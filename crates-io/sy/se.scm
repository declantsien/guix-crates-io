(define-module (crates-io sy se) #:use-module (crates-io))

(define-public crate-syserve-0.1 (crate (name "syserve") (vers "0.1.0") (deps (list (crate-dep (name "actix-files") (req "^0.5.0") (default-features #t) (kind 0)) (crate-dep (name "actix-web") (req "^3.3.2") (default-features #t) (kind 0)) (crate-dep (name "env_logger") (req "^0.8.3") (default-features #t) (kind 0)))) (hash "0vh3jp7zhqa6ndmyy8mxag89icsa87657fsmkqj0pd5wl6x4xhzr")))

(define-public crate-syserve-0.2 (crate (name "syserve") (vers "0.2.0") (deps (list (crate-dep (name "argparse") (req "^0.2.2") (default-features #t) (kind 0)) (crate-dep (name "env_logger") (req "^0.8.3") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1") (features (quote ("full"))) (default-features #t) (kind 0)) (crate-dep (name "warp") (req "^0.3.0") (default-features #t) (kind 0)))) (hash "01qzkkn29r6jr6v3860r9lrrh5rx206frfywk66fy0v003xv655f")))

(define-public crate-syserve-0.2 (crate (name "syserve") (vers "0.2.1") (deps (list (crate-dep (name "argparse") (req "^0.2.2") (default-features #t) (kind 0)) (crate-dep (name "env_logger") (req "^0.8.3") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1") (features (quote ("full"))) (default-features #t) (kind 0)) (crate-dep (name "warp") (req "^0.3.0") (default-features #t) (kind 0)))) (hash "18jjjra2skmgncs2apawbbf2y82iazxmci6f283ilyrv882qcjmm")))

(define-public crate-syserve-0.3 (crate (name "syserve") (vers "0.3.0") (deps (list (crate-dep (name "argparse") (req "^0.2.2") (default-features #t) (kind 0)) (crate-dep (name "env_logger") (req "^0.8.3") (default-features #t) (kind 0)) (crate-dep (name "httparse") (req "^1.3.6") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.14") (default-features #t) (kind 0)) (crate-dep (name "threadpool") (req "^1.8.1") (default-features #t) (kind 0)))) (hash "0ga1kc66ba2h2nq2zwnq68d0k5yfh44h3iz1wislw2p1fx5af27l")))

(define-public crate-sysexit-0.1 (crate (name "sysexit") (vers "0.1.0") (deps (list (crate-dep (name "libc") (req "^0.2") (kind 0)))) (hash "1nh1ypp5xgfgzpz94zrgz8mcql3fsiw14ccnl5rpnm0jilw4gcvh")))

(define-public crate-sysexit-0.1 (crate (name "sysexit") (vers "0.1.1") (deps (list (crate-dep (name "libc") (req "^0.2") (kind 0)))) (hash "0p4874nsc8bwqbiia7v2gas3c38r4m39fg8a8r0ipnq2n3qc9ln5")))

(define-public crate-sysexit-0.2 (crate (name "sysexit") (vers "0.2.0") (deps (list (crate-dep (name "libc") (req "^0.2") (kind 0)))) (hash "1ad6i92nxzhqv0s07am350cr4ngsksbmfswdkan3kybf1sxr2sd2")))

(define-public crate-sysexits-0.1 (crate (name "sysexits") (vers "0.1.0") (hash "127p5sn5s6sjr1b411v7jvwrv463b7jhcdbdl2vqnb2bqlwxz4y0") (rust-version "1.61.0")))

(define-public crate-sysexits-0.1 (crate (name "sysexits") (vers "0.1.1") (hash "0pa2gqcn13q1yw6425wlacnd55s1bkdcy60ll9ysfqs0ilkwh1pc") (rust-version "1.61.0")))

(define-public crate-sysexits-0.2 (crate (name "sysexits") (vers "0.2.0") (hash "0yincvc9ma60kzqqc4sjv1i8lxc473hqziv871lbqbqrw6bc2nlj") (rust-version "1.61.0")))

(define-public crate-sysexits-0.2 (crate (name "sysexits") (vers "0.2.1") (hash "1s5lvmzvy9q7d8wh91ig5lfbz0w6i3mhad0p3rw67cbkf0a3qpwx") (rust-version "1.61.0")))

(define-public crate-sysexits-0.2 (crate (name "sysexits") (vers "0.2.2") (hash "1j69s4hdskxzh349qpsnqpvmjhiifbyrja3y90krxrvp7l0kz47j") (rust-version "1.61.0")))

(define-public crate-sysexits-0.3 (crate (name "sysexits") (vers "0.3.0") (hash "0adnw8hscy8jylkpkirjmglnnhflbwkn1nd21512l2kdckm4vhjp") (rust-version "1.61.0")))

(define-public crate-sysexits-0.3 (crate (name "sysexits") (vers "0.3.1") (hash "0kb46filxgg44wnlb90xhq3kvzspczlrh0aywb7jjin12y4r1fvh") (rust-version "1.61.0")))

(define-public crate-sysexits-0.3 (crate (name "sysexits") (vers "0.3.2") (hash "183l4vkq507l4ra2852ka933lj938jyfnirjhrgcggilqy192mn1") (rust-version "1.61.0")))

(define-public crate-sysexits-0.3 (crate (name "sysexits") (vers "0.3.3") (hash "0hyf9a2wrw8idqx7s8svbjran9lf32gck0fidn3043m90zlmylrn") (rust-version "1.61.0")))

(define-public crate-sysexits-0.3 (crate (name "sysexits") (vers "0.3.4") (hash "0wpsvi1xjbilx2y5qm07pasczq6grq31fa3fwxwfqmfw7yf6qfwi") (rust-version "1.61.0")))

(define-public crate-sysexits-0.4 (crate (name "sysexits") (vers "0.4.0") (hash "0gblrxdny2s2mm6dnmrdskh0bc2ymid8dgkgjiyyq7r7rzsfzm5w") (features (quote (("std") ("default" "std")))) (rust-version "1.61.0")))

(define-public crate-sysexits-0.4 (crate (name "sysexits") (vers "0.4.1") (hash "08p8ns7wsvp1jdj7kzz9c1qd7iagk32s6rzl1lliwmg6wrl40ay0") (features (quote (("std") ("default" "std")))) (rust-version "1.61.0")))

(define-public crate-sysexits-0.5 (crate (name "sysexits") (vers "0.5.0") (hash "1rb4yyx602rqaaq25nq45r6d987wi7dw0zpg6008cwlgnhfnqz0g") (features (quote (("std") ("default" "std")))) (rust-version "1.61.0")))

(define-public crate-sysexits-0.6 (crate (name "sysexits") (vers "0.6.0") (hash "17s22c8d425khbxfi2ia6z6qfaibjyy4saqcnps97c49dihwkd92") (features (quote (("std") ("default" "std")))) (rust-version "1.61.0")))

(define-public crate-sysexits-0.6 (crate (name "sysexits") (vers "0.6.1") (hash "190130yq60wqii72wmraxzc8j1gfg36wdvkwk2was7x3f8665myr") (features (quote (("std") ("default" "std")))) (rust-version "1.61.0")))

(define-public crate-sysexits-0.6 (crate (name "sysexits") (vers "0.6.2") (hash "0s3lzb9z6yx0lh9j3n7q9jw0pa0382fqb8k51jpmgq47rwiwrbk4") (features (quote (("std") ("default" "std")))) (rust-version "1.61.0")))

(define-public crate-sysexits-0.6 (crate (name "sysexits") (vers "0.6.3") (hash "0dxf46i79d8if9qi0d8ic92xv4b14y4ah3401yrd3bcgsqbc0gi8") (features (quote (("std") ("default" "std")))) (rust-version "1.61.0")))

(define-public crate-sysexits-0.7 (crate (name "sysexits") (vers "0.7.0") (hash "0mwagmk3j2056ga8bm1fb1lfix2yj5xn3p5sjx1bkr0xgmgpwjy6") (features (quote (("std") ("default" "std")))) (yanked #t) (rust-version "1.61.0")))

(define-public crate-sysexits-0.7 (crate (name "sysexits") (vers "0.7.1") (hash "1zq597v05lwyp8lrjqjbspjy78qg382fi7mnl4r3ns0rzc76s96l") (features (quote (("std") ("default" "std")))) (rust-version "1.61.0")))

(define-public crate-sysexits-0.7 (crate (name "sysexits") (vers "0.7.2") (hash "1mm3d4i3ry56j93zaicxhx0hsqv73z0gvx6rkvgr4fck9m2pqxpd") (features (quote (("std") ("default" "std")))) (rust-version "1.61.0")))

(define-public crate-sysexits-0.7 (crate (name "sysexits") (vers "0.7.3") (hash "17a66dwwk4apnrc4rlv9vrz2y2khxd1f0zxyhsb52g35cnck49b4") (features (quote (("std") ("nightly" "extended_io_error") ("extended_io_error" "std") ("default" "std")))) (rust-version "1.61.0")))

(define-public crate-sysexits-0.7 (crate (name "sysexits") (vers "0.7.4") (hash "06cjkzcm2bxsi8jxwhwg35abwafqxisprd52mn4mp5vzy0bf6kia") (features (quote (("std") ("nightly" "extended_io_error") ("extended_io_error" "std") ("default" "std")))) (rust-version "1.61.0")))

(define-public crate-sysexits-0.7 (crate (name "sysexits") (vers "0.7.5") (hash "04vcdsk460x598z4w5f7rvzwbwn3lw1rkbyzskcw6r0fnjj7yn1m") (features (quote (("std") ("nightly" "extended_io_error") ("extended_io_error" "std") ("default" "std")))) (rust-version "1.61.0")))

(define-public crate-sysexits-0.7 (crate (name "sysexits") (vers "0.7.6") (hash "1x8jbmsncg1wgqfyhpca04r58bz4srzqcsf0iiaqa0w9r50ljwhy") (features (quote (("std") ("nightly" "extended_io_error") ("extended_io_error" "std") ("default" "std")))) (rust-version "1.61.0")))

(define-public crate-sysexits-0.7 (crate (name "sysexits") (vers "0.7.7") (hash "0mngn6dxzf53m73hrgqih01556d0w8cgxxw27kiyf44g9yzhwgc6") (features (quote (("std") ("nightly" "extended_io_error") ("extended_io_error" "std") ("default" "std")))) (rust-version "1.61.0")))

(define-public crate-sysexits-0.7 (crate (name "sysexits") (vers "0.7.8") (hash "1bvrfyhi8qfckq404l3mmifxxgkq17qma4jqaq9l4rsmy9r421lm") (features (quote (("std") ("nightly" "extended_io_error") ("extended_io_error" "std") ("default" "std")))) (rust-version "1.61.0")))

(define-public crate-sysexits-0.7 (crate (name "sysexits") (vers "0.7.9") (hash "0pcj2kvn64bs0061rv0r43sfyjbqsi78zii3x6lqahpkikhcfwhf") (features (quote (("std") ("nightly" "extended_io_error") ("extended_io_error" "std") ("default" "std")))) (yanked #t) (rust-version "1.61.0")))

(define-public crate-sysexits-0.7 (crate (name "sysexits") (vers "0.7.10") (hash "1rp9php91drmqq2dbnig77g580il341ciyhzn1wcxm6fqsndi5q1") (features (quote (("std") ("nightly" "extended_io_error") ("extended_io_error" "std") ("default" "std")))) (rust-version "1.61.0")))

(define-public crate-sysexits-0.7 (crate (name "sysexits") (vers "0.7.11") (hash "0wpr9kvv14srxj5x056235c0fwbwmyiwm2j6aa4i43bx04gq2wqh") (features (quote (("std") ("nightly" "extended_io_error") ("extended_io_error" "std") ("default" "std")))) (rust-version "1.61.0")))

(define-public crate-sysexits-0.7 (crate (name "sysexits") (vers "0.7.12") (hash "1jvwqspw3s1cmvs2pi33zpqp3kj6win54scvvl2vzn0q7nkpi4bx") (features (quote (("std") ("nightly" "extended_io_error") ("extended_io_error" "std") ("default" "std")))) (rust-version "1.61.0")))

(define-public crate-sysexits-0.7 (crate (name "sysexits") (vers "0.7.13") (hash "1gn2dficr7p4ip33y321spk4vzgkzwzn4v354w9g3vz3pjrk7sb2") (features (quote (("std") ("nightly" "extended_io_error") ("extended_io_error" "std") ("default" "std")))) (rust-version "1.61.0")))

(define-public crate-sysexits-0.7 (crate (name "sysexits") (vers "0.7.14") (hash "10j2y6hzi349v56qmzizzr09qf4hfaiyx5jcqfk953ywliw6rf5m") (features (quote (("std") ("nightly" "extended_io_error") ("extended_io_error" "std") ("default" "std")))) (rust-version "1.61.0")))

