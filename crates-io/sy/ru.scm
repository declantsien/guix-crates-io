(define-module (crates-io sy ru) #:use-module (crates-io))

(define-public crate-syrup-0.1 (crate (name "syrup") (vers "0.1.0") (deps (list (crate-dep (name "pancurses") (req "^0.15.0") (default-features #t) (kind 0)))) (hash "0la0ys6zac91i62g4l2w6hm17k67xp0iwhgh65ysn6qspcwcxihi")))

(define-public crate-syrup-0.2 (crate (name "syrup") (vers "0.2.0") (deps (list (crate-dep (name "pancurses") (req "^0.16.0") (default-features #t) (kind 0)) (crate-dep (name "textwrap") (req "^0.10") (kind 0)))) (hash "1f1npbzcp3vv58asv59352758k21bbl4gha6kv9bpp3xhd06w2xj")))

(define-public crate-syrup-cpi-0.1 (crate (name "syrup-cpi") (vers "0.1.0") (deps (list (crate-dep (name "anchor-gen") (req "^0.1.2") (default-features #t) (kind 0)) (crate-dep (name "anchor-lang") (req ">=0.20") (default-features #t) (kind 0)))) (hash "0x9gmj8fhyz1nc6ml69nj7yxh9kc2jvapyvdkndlac9ynxs35slx") (features (quote (("no-log-ix-name") ("no-idl") ("no-entrypoint") ("default" "cpi") ("cpi" "no-entrypoint"))))))

(define-public crate-syrup-cpi-0.2 (crate (name "syrup-cpi") (vers "0.2.0") (deps (list (crate-dep (name "anchor-gen") (req "^0.1.2") (default-features #t) (kind 0)) (crate-dep (name "anchor-lang") (req ">=0.20") (default-features #t) (kind 0)))) (hash "0v9y4n7p0zka3mkp80x5fkhwjgb1lrnz8jv11n3ig675j4d7g9yv") (features (quote (("no-log-ix-name") ("no-idl") ("no-entrypoint") ("default" "cpi") ("cpi" "no-entrypoint"))))))

