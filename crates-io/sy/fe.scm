(define-module (crates-io sy fe) #:use-module (crates-io))

(define-public crate-syfetch-0.1 (crate (name "syfetch") (vers "0.1.0") (hash "04g7rawqaaflh8hakvkhbqxzahw23bk01p8vkhqn2cv5bjh9kyv4")))

(define-public crate-syfetch-0.1 (crate (name "syfetch") (vers "0.1.1") (deps (list (crate-dep (name "colored") (req "^2.0.0") (default-features #t) (kind 0)))) (hash "0i5dzf8009b1nj5yis5c2a28nqmnchlwjm5xqhz10w6d6z3vxwmw")))

(define-public crate-syfetch-0.1 (crate (name "syfetch") (vers "0.1.2") (deps (list (crate-dep (name "colored") (req "^2.0.0") (default-features #t) (kind 0)))) (hash "1mqbsivcrrf535b0gxv3vbzc33bmw79mzy7n1xqcpn0mkznwifmm")))

