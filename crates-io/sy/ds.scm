(define-module (crates-io sy ds) #:use-module (crates-io))

(define-public crate-syds-0.1 (crate (name "syds") (vers "0.1.0") (deps (list (crate-dep (name "notify") (req "^4.0.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.3.13") (default-features #t) (kind 0)))) (hash "1w8pmh2ns2lyh3hqd96fzm99whpvpdgl0alfj3figp0nvvr5ncy4")))

(define-public crate-syds-0.1 (crate (name "syds") (vers "0.1.1") (deps (list (crate-dep (name "notify") (req "^4.0.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.3.13") (default-features #t) (kind 0)))) (hash "0ghzxca32k5w1xjcaxyrsjn9sjmw82zzmbnaa5c7cyicclcaksva")))

(define-public crate-syds-0.1 (crate (name "syds") (vers "0.1.2") (deps (list (crate-dep (name "notify") (req "^4.0.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.3.13") (default-features #t) (kind 0)))) (hash "0dnr196yn4nnnl9wdcfbyaypnb9jigl6v14v82adgbq6k05wfldy")))

(define-public crate-syds-0.2 (crate (name "syds") (vers "0.2.0") (deps (list (crate-dep (name "notify") (req "^4.0.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.3.13") (default-features #t) (kind 0)))) (hash "08qp5ckscjgf8p1rfwfckhkya9qw113s7nh4hrk4fv9vqknavwzi")))

(define-public crate-syds-0.2 (crate (name "syds") (vers "0.2.1") (deps (list (crate-dep (name "notify") (req "^4.0.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.3.13") (default-features #t) (kind 0)))) (hash "0cbv8zv6c7vid6sqwhcqvm367h9vmasifn3p1hzkg0aqmdjrbgx3")))

