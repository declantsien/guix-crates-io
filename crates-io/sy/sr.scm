(define-module (crates-io sy sr) #:use-module (crates-io))

(define-public crate-sysrepo-0.1 (crate (name "sysrepo") (vers "0.1.0") (deps (list (crate-dep (name "bindgen") (req "^0.54.0") (default-features #t) (kind 1)))) (hash "07kvvaqhmg5vh8dfsanh6z6lzx1gqv8awwrbgnxln6q87xspqkvr")))

(define-public crate-sysrepo-0.2 (crate (name "sysrepo") (vers "0.2.0") (deps (list (crate-dep (name "bindgen") (req "^0.54.0") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2.70") (default-features #t) (kind 0)))) (hash "05lywyjn4ihsh69ab3p9ss822dsjb14iqc43nmvkbrhiz7d6acfb")))

(define-public crate-sysrepo-0.3 (crate (name "sysrepo") (vers "0.3.0") (deps (list (crate-dep (name "bindgen") (req "^0.59.2") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2.121") (default-features #t) (kind 0)))) (hash "1mqzs6rvr047gymrcf4v5v2x0zp7f70s5cm999naz673w3m7ibvf")))

(define-public crate-sysrepo-0.4 (crate (name "sysrepo") (vers "0.4.0") (deps (list (crate-dep (name "bindgen") (req "^0.59.2") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2.121") (default-features #t) (kind 0)) (crate-dep (name "libyang2-sys") (req "^0.6") (default-features #t) (kind 0)) (crate-dep (name "yang2") (req "^0.7") (default-features #t) (kind 0)))) (hash "1jlybl4n026h3gmqq9rnxb92b25x7qjmlb3w387gyyliajmzwq96")))

(define-public crate-sysreq-0.1 (crate (name "sysreq") (vers "0.1.0") (deps (list (crate-dep (name "hyper") (req "^0") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0") (features (quote ("blocking" "rustls-tls-native-roots"))) (kind 2)) (crate-dep (name "thiserror") (req "^1") (default-features #t) (kind 0)))) (hash "1gnsj0jxnwcfb5cr44kx8qgkbn0zm5890hzy8cqr3by72sys0flc") (yanked #t)))

(define-public crate-sysreq-0.1 (crate (name "sysreq") (vers "0.1.1") (deps (list (crate-dep (name "difference") (req "^2") (default-features #t) (kind 2)) (crate-dep (name "lazy_static") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0") (features (quote ("blocking" "rustls-tls-native-roots"))) (kind 2)) (crate-dep (name "thiserror") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "url") (req "^2") (default-features #t) (kind 0)))) (hash "0383016108clm1z1nd6gml9m80jkqj66qz3kdicnlkwwyp2vzyz0")))

(define-public crate-sysreq-0.1 (crate (name "sysreq") (vers "0.1.2") (deps (list (crate-dep (name "difference") (req "^2") (default-features #t) (kind 2)) (crate-dep (name "lazy_static") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0") (features (quote ("blocking" "rustls-tls-native-roots"))) (kind 2)) (crate-dep (name "thiserror") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "url") (req "^2") (default-features #t) (kind 0)))) (hash "0imijhrgdgf2m155miaqza896c4iyhwzb8km7qgq75z2k0fb2mqf")))

(define-public crate-sysreq-0.1 (crate (name "sysreq") (vers "0.1.3") (deps (list (crate-dep (name "difference") (req "^2") (default-features #t) (kind 2)) (crate-dep (name "lazy_static") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0") (features (quote ("blocking" "rustls-tls-native-roots"))) (kind 2)) (crate-dep (name "thiserror") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "url") (req "^2") (default-features #t) (kind 0)))) (hash "19c1yy5z15msxw81zry3il2r1d2c2nsc4lmnwh45bh1ddgcax0lm")))

(define-public crate-sysreq-0.1 (crate (name "sysreq") (vers "0.1.4") (deps (list (crate-dep (name "difference") (req "^2") (default-features #t) (kind 2)) (crate-dep (name "lazy_static") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0") (features (quote ("blocking" "rustls-tls-native-roots"))) (kind 2)) (crate-dep (name "url") (req "^2") (default-features #t) (kind 0)))) (hash "1hyfc4p2mx74gr047yl5j81m6bf249wsjspdxcpj4x2qn6vzfz0r")))

(define-public crate-sysreq-0.1 (crate (name "sysreq") (vers "0.1.5") (deps (list (crate-dep (name "difference") (req "^2") (default-features #t) (kind 2)) (crate-dep (name "reqwest") (req "^0") (features (quote ("blocking" "rustls-tls-native-roots"))) (kind 2)) (crate-dep (name "url") (req "^2") (default-features #t) (kind 0)))) (hash "11pkwqglrd63zabl22fxb9x3l4qh6wqf4925z13vdr42bjqn3yir")))

(define-public crate-sysreq-0.1 (crate (name "sysreq") (vers "0.1.6") (deps (list (crate-dep (name "difference") (req "^2") (default-features #t) (kind 2)) (crate-dep (name "reqwest") (req "^0") (features (quote ("blocking" "rustls-tls-native-roots"))) (kind 2)) (crate-dep (name "url") (req "^2") (default-features #t) (kind 0)))) (hash "046kh743v19gv5s5acilc7c3wv5nrhdlzmb9pgdjyzqgx2iah468")))

