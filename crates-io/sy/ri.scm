(define-module (crates-io sy ri) #:use-module (crates-io))

(define-public crate-syringe-0.1 (crate (name "syringe") (vers "0.1.0") (deps (list (crate-dep (name "dunce") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.3.20") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "winapi") (req "^0.3") (features (quote ("ntdef" "handleapi" "errhandlingapi" "tlhelp32" "processthreadsapi" "securitybaseapi" "winbase" "handleapi" "libloaderapi" "memoryapi"))) (default-features #t) (kind 0)))) (hash "0404dacjjcawvl40vsn9d8bs4rkj94ddfgrwid2qx665f5zr8ln3")))

(define-public crate-syringe-di-0.0.1 (crate (name "syringe-di") (vers "0.0.1") (deps (list (crate-dep (name "frunk") (req "^0.4.0") (default-features #t) (kind 0)) (crate-dep (name "once_cell") (req "^1.14.0") (default-features #t) (kind 0)))) (hash "0wh2iwxan4vaar681qcfaqf5v4imb46px3m9yi7lgm1s4bqbdfpp")))

(define-public crate-syringe-di-0.0.2 (crate (name "syringe-di") (vers "0.0.2") (deps (list (crate-dep (name "frunk") (req "^0.4.0") (default-features #t) (kind 0)) (crate-dep (name "once_cell") (req "^1.14.0") (default-features #t) (kind 0)))) (hash "0hi1v58bv5wj9x6q24i9l028cysafv80mcavxkx816x5qx03s0qb")))

(define-public crate-syringe-di-derive-0.0.1 (crate (name "syringe-di-derive") (vers "0.0.1") (hash "0qyiaakh7k1ckfdhxxi25gp3vinsn9m4fq2xhgpj68vg36qk5k3q")))

(define-public crate-syringe-di-derive-0.0.2 (crate (name "syringe-di-derive") (vers "0.0.2") (hash "132irqg2ih21h353nhqxxlrhj8a7vw0zz8pwrms48llv0aq0crzp")))

