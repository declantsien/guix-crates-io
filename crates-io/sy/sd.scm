(define-module (crates-io sy sd) #:use-module (crates-io))

(define-public crate-sysdir-1 (crate (name "sysdir") (vers "1.0.0") (deps (list (crate-dep (name "version-sync") (req "^0.9.4") (features (quote ("markdown_deps_updated" "html_root_url_updated"))) (kind 2)))) (hash "1wvznnr57m0dbc509jw2mcizgafp4q3aj7vmz4hy4k6ja410rj52") (rust-version "1.64.0")))

(define-public crate-sysdir-1 (crate (name "sysdir") (vers "1.1.0") (deps (list (crate-dep (name "version-sync") (req "^0.9.4") (features (quote ("markdown_deps_updated" "html_root_url_updated"))) (kind 2)))) (hash "19j6k76mplhk1r001w76z2icxnx11gi2ymbi50mylz66iyahv4np") (rust-version "1.64.0")))

(define-public crate-sysdir-1 (crate (name "sysdir") (vers "1.2.0") (deps (list (crate-dep (name "version-sync") (req "^0.9.4") (features (quote ("markdown_deps_updated" "html_root_url_updated"))) (kind 2)))) (hash "1pwlmcywa1qx42jdha26sbm4vjb6w3dqfcwz1cyfpici2w5qxhak") (rust-version "1.64.0")))

(define-public crate-sysdir-1 (crate (name "sysdir") (vers "1.2.1") (deps (list (crate-dep (name "version-sync") (req "^0.9.4") (features (quote ("markdown_deps_updated" "html_root_url_updated"))) (kind 2)))) (hash "148839862fldr6vlr5w22dx5k985mgkhvg4ikrjc04phz9j7nk25") (rust-version "1.64.0")))

(define-public crate-sysdir-1 (crate (name "sysdir") (vers "1.2.2") (deps (list (crate-dep (name "version-sync") (req "^0.9.4") (features (quote ("markdown_deps_updated" "html_root_url_updated"))) (kind 2)))) (hash "1w1352rva864zgzqa77kv2vqd29f4fa3j3z1la52lh114swrhip1") (rust-version "1.64.0")))

(define-public crate-sysdns-0.1 (crate (name "sysdns") (vers "0.1.0") (deps (list (crate-dep (name "interfaces") (req "^0.0.8") (default-features #t) (target "cfg(target_os = \"macos\")") (kind 0)) (crate-dep (name "thiserror") (req "^1") (default-features #t) (kind 0)))) (hash "17f3r2np3x0mndvam6xanwj8a94905hwk30cf1hab8x2zxpi7rv1")))

(define-public crate-sysdo-0.1 (crate (name "sysdo") (vers "0.1.0") (hash "1klwaqp7yn8qnv7fc800hnm8hc1ybmrp1y23ql56d6ljyc3pm4sv")))

(define-public crate-sysdump-0.1 (crate (name "sysdump") (vers "0.1.0") (deps (list (crate-dep (name "default-net") (req "^0.11.0") (default-features #t) (kind 0)) (crate-dep (name "directories") (req "^4.0.1") (default-features #t) (kind 0)) (crate-dep (name "netstat2") (req "^0.9.1") (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.11.11") (features (quote ("blocking" "json" "socks"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "sysinfo") (req "^0.26.2") (default-features #t) (kind 0)) (crate-dep (name "term-table") (req "^1.3.2") (default-features #t) (kind 0)))) (hash "0qsr59bjkzq7d4d5ag52i8iq7fq09ci4x0jangcmkzkigdh7n7df")))

