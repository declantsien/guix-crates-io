(define-module (crates-io sy ro) #:use-module (crates-io))

(define-public crate-syron-0.1 (crate (name "syron") (vers "0.1.0") (deps (list (crate-dep (name "ndarray") (req "^0.15.0") (default-features #t) (kind 0)) (crate-dep (name "ndarray-rand") (req "^0.14.0") (default-features #t) (kind 0)))) (hash "166c3wvac553wpq00g3kr7zalqy4442njy2zx1lblda2x695dw32")))

(define-public crate-syron-0.1 (crate (name "syron") (vers "0.1.1") (deps (list (crate-dep (name "ndarray") (req "^0.15.0") (default-features #t) (kind 0)) (crate-dep (name "ndarray-rand") (req "^0.14.0") (default-features #t) (kind 0)))) (hash "0rja43cf14y29z74h4z0w3gbgi5qa02y3szimkn0gpqxhq84fp71")))

