(define-module (crates-io sy ue) #:use-module (crates-io))

(define-public crate-syue-0.1 (crate (name "syue") (vers "0.1.0") (deps (list (crate-dep (name "regex") (req "^1.10.3") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.114") (default-features #t) (kind 0)))) (hash "1bg3qsairg4ls96il3dfm0f6fvac9aaklgbi5gibvk34affswf6g") (yanked #t)))

(define-public crate-syue-0.1 (crate (name "syue") (vers "0.1.1") (deps (list (crate-dep (name "regex") (req "^1.10.3") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.114") (default-features #t) (kind 0)))) (hash "0jfv2h6zwxmhy0411zz6zw4msknnaizz682xxjdgy2mv78s16idi") (yanked #t)))

(define-public crate-syue-0.1 (crate (name "syue") (vers "0.1.2") (deps (list (crate-dep (name "regex") (req "^1.10.3") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.114") (default-features #t) (kind 0)))) (hash "180i6fyqj5z4hnip2qvkfsdh1pfwxa05gc4n2dg6xkfl3zcgib1h")))

