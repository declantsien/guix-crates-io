(define-module (crates-io sy mm) #:use-module (crates-io))

(define-public crate-symm_impl-0.1 (crate (name "symm_impl") (vers "0.1.0") (deps (list (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (features (quote ("full" "extra-traits"))) (default-features #t) (kind 0)) (crate-dep (name "trybuild") (req "^1.0") (default-features #t) (kind 2)))) (hash "0l79zzivqj6zhy8rjxc5nfar66xpa8iyrjvnbd1a6nfz0havahk1")))

(define-public crate-symm_impl-0.1 (crate (name "symm_impl") (vers "0.1.1") (deps (list (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (features (quote ("full" "extra-traits"))) (default-features #t) (kind 0)) (crate-dep (name "trybuild") (req "^1.0") (default-features #t) (kind 2)))) (hash "02kqfgbalp55ynnvc50c24aak7akfzdqzi1h7jacx3fhdbx9b34i")))

(define-public crate-symm_impl-0.1 (crate (name "symm_impl") (vers "0.1.2") (deps (list (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (features (quote ("full" "extra-traits"))) (default-features #t) (kind 0)) (crate-dep (name "trybuild") (req "^1.0") (default-features #t) (kind 2)))) (hash "0k5165889vnb6dy4h829jv7cs06yhbd71ippf2g39djx3sd7g3lk")))

(define-public crate-symmetric-interaction-calculus-0.1 (crate (name "symmetric-interaction-calculus") (vers "0.1.0") (hash "0542dbpfy1gj1jdhyvxscsj5aya1q3x7p0znzc5vmvvdan16qg99")))

(define-public crate-symmetric-interaction-calculus-0.1 (crate (name "symmetric-interaction-calculus") (vers "0.1.1") (hash "0hcghdyl5aw9nsk6wykgf9pq7k6hi1l9iqw7068srcfz5sfcnnk4")))

(define-public crate-symmetric-interaction-calculus-0.1 (crate (name "symmetric-interaction-calculus") (vers "0.1.2") (hash "0dnzwx01biwkh9an626nhr3zy8m1qj5hxiz22lf7k97hwajvfi9p")))

(define-public crate-symmetric-interaction-calculus-0.1 (crate (name "symmetric-interaction-calculus") (vers "0.1.3") (hash "0ysmx8j6i5l1k5fnnf8k916jqvyc2ad4r4iyh05lsk975mj5k75b")))

(define-public crate-symmetric-interaction-calculus-0.1 (crate (name "symmetric-interaction-calculus") (vers "0.1.4") (hash "0qjyw3wwr13fga7p9ypilvd0djc6b6nzp96l2pj8r1wr8kf1ynqx")))

(define-public crate-symmetric-interaction-calculus-0.1 (crate (name "symmetric-interaction-calculus") (vers "0.1.5") (hash "08mr80baq1xjysn9hayis3jc7dii6bn38y4p73x4dql66a23x8z0")))

(define-public crate-symmetric-interaction-calculus-0.1 (crate (name "symmetric-interaction-calculus") (vers "0.1.6") (deps (list (crate-dep (name "clap") (req "^2.26.0") (default-features #t) (kind 0)))) (hash "04w97hcg2hb3lf9jj64b9lj751jf5gvvqqsbv5n46wk7r362ij1d")))

(define-public crate-symmetric-interaction-calculus-0.1 (crate (name "symmetric-interaction-calculus") (vers "0.1.7") (deps (list (crate-dep (name "clap") (req "^2.26.0") (default-features #t) (kind 0)))) (hash "1z4wh4a3q980chykr5nhdj6vqash5yh3gllc568shvcnbkx4k6z0")))

(define-public crate-symmetric-interaction-calculus-0.1 (crate (name "symmetric-interaction-calculus") (vers "0.1.8") (deps (list (crate-dep (name "clap") (req "^2.26.0") (default-features #t) (kind 0)))) (hash "0g8qlx88r4gsm2w987wh0a455inzaqa8raqjgxhwb3yz4xw8z0nd")))

(define-public crate-symmetric-shadowcasting-0.1 (crate (name "symmetric-shadowcasting") (vers "0.1.0") (deps (list (crate-dep (name "num-rational") (req "^0.3") (kind 0)))) (hash "0wn0rwf80lppzqd70vn771pnkj03c247dj0cndvxcmzz882vfi8p")))

(define-public crate-symmetric-shadowcasting-0.1 (crate (name "symmetric-shadowcasting") (vers "0.1.1") (deps (list (crate-dep (name "num-rational") (req "^0.3") (kind 0)))) (hash "18m46yhlpxnqgglmfyiyr31zx4yqqi8kdy8gzg8xhlpny9h5d3sv")))

(define-public crate-symmetric-shadowcasting-0.2 (crate (name "symmetric-shadowcasting") (vers "0.2.0") (deps (list (crate-dep (name "num-rational") (req "^0.3") (kind 0)))) (hash "1j465vfihvx2y89w8gdz9mkrdhb89g659pjcwhglknq57ca2xq5k")))

(define-public crate-symmetrical-spork-0.1 (crate (name "symmetrical-spork") (vers "0.1.0") (hash "1yybpg5ng7jsmvwrwa387f7pjxr5qmhijvypq3x2m9r0fblyk2xc") (features (quote (("two") ("one"))))))

(define-public crate-symmetrical-spork-0.1 (crate (name "symmetrical-spork") (vers "0.1.1") (hash "1z1n6samf7ql7f8jzkb15mjbk53khkq0z5m9hscr05snwk5vgmqh") (features (quote (("two") ("one"))))))

(define-public crate-symmetrical-spork-0.1 (crate (name "symmetrical-spork") (vers "0.1.2") (hash "0yza2hhb2mr46ij6gr0j4vymci4pd13lvx79v6p7v9fkrw9myikz") (features (quote (("two") ("one"))))))

