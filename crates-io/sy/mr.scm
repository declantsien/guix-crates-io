(define-module (crates-io sy mr) #:use-module (crates-io))

(define-public crate-symr-0.0.0 (crate (name "symr") (vers "0.0.0") (hash "0vq6fifnxgy5ic7g6pgc7j4r6byd1s1ll5is735jydn7ij1f8ghj")))

(define-public crate-symrs-0.1 (crate (name "symrs") (vers "0.1.0") (hash "0dphj035bvckz1rkxagsq2qh5n2sqvrigdd3kvsq0jgsvqkmmrhw")))

