(define-module (crates-io sy un) #:use-module (crates-io))

(define-public crate-syunit-0.1 (crate (name "syunit") (vers "0.1.0") (deps (list (crate-dep (name "serde") (req "^1.0.196") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "1vjfxxgsrbc74g08y0cmyqa9jl35hhal7b0lk3qpj8nmyhg64r14")))

(define-public crate-syunit-0.1 (crate (name "syunit") (vers "0.1.1") (deps (list (crate-dep (name "serde") (req "^1.0.196") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)))) (hash "1kcmw4h8xjxwkwm6pd8c1x1wgw41xicjbjvixsry0hxhvzybj35s") (features (quote (("default" "serde")))) (v 2) (features2 (quote (("serde" "dep:serde"))))))

(define-public crate-syunit-0.1 (crate (name "syunit") (vers "0.1.2") (deps (list (crate-dep (name "serde") (req "^1.0.196") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)))) (hash "011hq39mb6m78siq0dz0jzi4psxm3jc9l0dhd9dynzs7kz1n59qj") (features (quote (("default" "serde")))) (v 2) (features2 (quote (("serde" "dep:serde"))))))

(define-public crate-syunit-0.1 (crate (name "syunit") (vers "0.1.3") (deps (list (crate-dep (name "serde") (req "^1.0.196") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)))) (hash "1cp8x1sz8ixc395bs417vnb7bfjvwjdpkwc3ljzqfc5ig9m5zp4x") (features (quote (("default" "serde")))) (v 2) (features2 (quote (("serde" "dep:serde"))))))

(define-public crate-syunit-0.1 (crate (name "syunit") (vers "0.1.4") (deps (list (crate-dep (name "serde") (req "^1.0.196") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)))) (hash "0frbaib5hd3x7fjkp8xcdj1cs5dbqjlvqgan1ygv7nbkvw2xk031") (features (quote (("default" "serde")))) (v 2) (features2 (quote (("serde" "dep:serde"))))))

(define-public crate-syunit-0.1 (crate (name "syunit") (vers "0.1.5") (deps (list (crate-dep (name "serde") (req "^1.0.196") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)))) (hash "1lagcdxjrn1h26rk0234zirf226qj3bvy4qbj0hwhz14cxc75wwm") (features (quote (("default" "serde")))) (v 2) (features2 (quote (("serde" "dep:serde"))))))

(define-public crate-syunit-0.1 (crate (name "syunit") (vers "0.1.6") (deps (list (crate-dep (name "serde") (req "^1.0.196") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)))) (hash "102x3gbkgiirqy3md36igqqxxmfp2wz04rbi1yyaliylx2qd7jgk") (features (quote (("default" "serde")))) (v 2) (features2 (quote (("serde" "dep:serde"))))))

(define-public crate-syunit-0.2 (crate (name "syunit") (vers "0.2.0") (deps (list (crate-dep (name "serde") (req "^1.0.196") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)))) (hash "1di8sblf4hb4vydxw63jfr3bjkpnhjr2v942py4cgfp0xm8f1bz6") (features (quote (("default" "serde")))) (v 2) (features2 (quote (("serde" "dep:serde"))))))

(define-public crate-syunit-0.2 (crate (name "syunit") (vers "0.2.1") (deps (list (crate-dep (name "serde") (req "^1.0.196") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)))) (hash "0hpzqwxilp7ibjclc2ybliwfglh8qiwbvbsrraws8w2r05874fl5") (features (quote (("default" "serde")))) (v 2) (features2 (quote (("serde" "dep:serde"))))))

