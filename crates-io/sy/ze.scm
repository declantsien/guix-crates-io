(define-module (crates-io sy ze) #:use-module (crates-io))

(define-public crate-syze-0.1 (crate (name "syze") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^4.2.7") (default-features #t) (kind 0)) (crate-dep (name "colored") (req "^2.0.0") (default-features #t) (kind 0)))) (hash "14ywgd2jhlr3q0vryk8mflw3kxdr6b3vsw8k9gzgb3hr4hvk4lc7")))

(define-public crate-syze-0.1 (crate (name "syze") (vers "0.1.27") (deps (list (crate-dep (name "clap") (req "^4.2.7") (default-features #t) (kind 0)) (crate-dep (name "colored") (req "^2.0.0") (default-features #t) (kind 0)))) (hash "1a2bmw4zn3z5pz0lapadn8hh1g82ldyb49h3h9n62knv0kii7x94")))

