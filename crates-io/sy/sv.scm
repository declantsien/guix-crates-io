(define-module (crates-io sy sv) #:use-module (crates-io))

(define-public crate-sysvmq-0.1 (crate (name "sysvmq") (vers "0.1.0") (deps (list (crate-dep (name "libc") (req "^0.2.98") (default-features #t) (kind 0)) (crate-dep (name "nix") (req "^0.27.1") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.26") (default-features #t) (kind 0)))) (hash "09wd1ciic3sskaf3m2z1ypkb06jpar28y3j5m71ac0ww4z1b28wx")))

(define-public crate-sysvmq-0.1 (crate (name "sysvmq") (vers "0.1.1") (deps (list (crate-dep (name "libc") (req "^0.2.98") (default-features #t) (kind 0)) (crate-dep (name "nix") (req "^0.27.1") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.26") (default-features #t) (kind 0)))) (hash "0yrm881ajbr257sdns1zp8i285wq1z65zk13y6llim1krzag8yz1")))

