(define-module (crates-io qu te) #:use-module (crates-io))

(define-public crate-qute-0.0.0 (crate (name "qute") (vers "0.0.0") (hash "1f3cdfhan3drz8qwp40md849dqcyx2a0ss6mscgil5mjgvp8vasx")))

(define-public crate-qute-0.0.1 (crate (name "qute") (vers "0.0.1") (hash "18b0ci4kkdkzp0r2gr4qzlhpylysxw89b0s49idm6dbgi9l6j8qw")))

(define-public crate-qute-0.0.2 (crate (name "qute") (vers "0.0.2") (hash "1x96296ciq9wg8asafq71rcz4hkgwck6xz2ad6nxjni0a6r272rb")))

(define-public crate-qute-0.0.3 (crate (name "qute") (vers "0.0.3") (hash "1fih3sm71vzdgjbq362asns9lsrkd3lbb41i7zph4g4s9y9f3hn3")))

(define-public crate-qute-0.0.4 (crate (name "qute") (vers "0.0.4") (hash "0jl4i4c0hgl6dwwwgqcrvnhxbdgmhln142b5al9fbizzva8nfpl1")))

(define-public crate-qute-0.0.5 (crate (name "qute") (vers "0.0.5") (hash "1ilf933n0kf0x3rpl5in219cwcxn2zm3waa16gzmr6jh9vzjp0vc")))

(define-public crate-qute-0.0.6 (crate (name "qute") (vers "0.0.6") (hash "1nry3ksv07kj377wwiw750a8my5wp0lbvd3wg3w3iv4k8iwqjrm0")))

(define-public crate-qute-0.0.7 (crate (name "qute") (vers "0.0.7") (hash "0b0n09iysk682hk1wy95q8pxicc8pldgsabg3r6lip8ydacwgz34")))

(define-public crate-qute-0.0.8 (crate (name "qute") (vers "0.0.8") (hash "0ki3hwhblycmgvb4nx1grbzk8gfdpv4xgibayiv88ncjcsmnakkd")))

(define-public crate-qute-0.0.9 (crate (name "qute") (vers "0.0.9") (hash "1aq1dr5j2j45zbxv9g2xzphsc50x7x4ayr9bfxj6ggykca04nfzv")))

(define-public crate-qute-0.0.10 (crate (name "qute") (vers "0.0.10") (hash "06wlc48ykp53h6gjbckrzb3kzb9nnbw18m4szm9rvjcsgrqpx2f5")))

(define-public crate-qute-0.0.11 (crate (name "qute") (vers "0.0.11") (hash "16qq59lpkgm0a3af71ajk4d5xwspxmdggm6kg7lsikmxz9v2xl3a")))

(define-public crate-qute-0.0.12 (crate (name "qute") (vers "0.0.12") (hash "1licih9ab1mgs93dbjyqqfl2rakbnx8qcbp8kyjrbm0p8bwq1pa1")))

(define-public crate-qute-0.0.13 (crate (name "qute") (vers "0.0.13") (hash "1cab4py22a6pa7h548x57zd3gi1b76fx45r0hl2mf9946js69vvq")))

(define-public crate-qutee-0.1 (crate (name "qutee") (vers "0.1.0") (deps (list (crate-dep (name "num-traits") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "test-case") (req "^3.0") (default-features #t) (kind 2)))) (hash "1lbnn435mpjay3jbhw7vk974izmmxdqyk8qi5ingf8xkvvb8lb6l")))

(define-public crate-qutee-0.1 (crate (name "qutee") (vers "0.1.1") (deps (list (crate-dep (name "criterion") (req "^0.5.1") (features (quote ("html_reports"))) (default-features #t) (kind 2)) (crate-dep (name "num-traits") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "test-case") (req "^3.0") (default-features #t) (kind 2)))) (hash "0wn7gsxnsdsydq9xib2g4iywxzwwg8xxc0v90l7qi7hqysyf7bj5")))

(define-public crate-qutee-0.2 (crate (name "qutee") (vers "0.2.0") (deps (list (crate-dep (name "criterion") (req "^0.5.1") (features (quote ("html_reports"))) (default-features #t) (kind 2)) (crate-dep (name "num-traits") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "test-case") (req "^3.0") (default-features #t) (kind 2)))) (hash "19qinmhx1019cijnqc59i2m46r39jyih02r4bj06m9a74fkca4m7")))

(define-public crate-qutee-0.2 (crate (name "qutee") (vers "0.2.1") (deps (list (crate-dep (name "criterion") (req "^0.5.1") (features (quote ("html_reports"))) (default-features #t) (kind 2)) (crate-dep (name "num-traits") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 2)) (crate-dep (name "test-case") (req "^3.0") (default-features #t) (kind 2)))) (hash "1szmqfrqb0ry3z8skkbvbd4a3amw2n15yhfpkr5na3qsfz5vp92w")))

(define-public crate-qutescript-0.1 (crate (name "qutescript") (vers "0.1.0") (hash "1an5bxrzsdgjl9zhzdyn2v63w1cgdiawvgn46pdszfx422y09irm")))

(define-public crate-qutex-0.0.1 (crate (name "qutex") (vers "0.0.1") (deps (list (crate-dep (name "crossbeam") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "futures") (req "^0.1") (default-features #t) (kind 0)))) (hash "03y7da9k4kcv4zgji4a6l4drrfay1lrvh4lxry5z84pg7qrdnx3x")))

(define-public crate-qutex-0.0.2 (crate (name "qutex") (vers "0.0.2") (deps (list (crate-dep (name "crossbeam") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "futures") (req "^0.1") (default-features #t) (kind 0)))) (hash "0m02k3ypmn33bipihv2cavj4hglylq91v8xvyganvkmivjy8n5x0")))

(define-public crate-qutex-0.0.3 (crate (name "qutex") (vers "0.0.3") (deps (list (crate-dep (name "crossbeam") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "futures") (req "^0.1") (default-features #t) (kind 0)))) (hash "1bqy12s8g1j75rdb85iga6z10d9360vfwv53ada7zb47hj0yivhl")))

(define-public crate-qutex-0.0.4 (crate (name "qutex") (vers "0.0.4") (deps (list (crate-dep (name "crossbeam") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "futures") (req "^0.1") (default-features #t) (kind 0)))) (hash "1i3pqcya1qh2bqc71nh7yqqcn6y9vld295zqq8iybh61wcd8a9kg")))

(define-public crate-qutex-0.0.5 (crate (name "qutex") (vers "0.0.5") (deps (list (crate-dep (name "crossbeam") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "futures") (req "^0.1") (default-features #t) (kind 0)))) (hash "1ivdc3hgvwjl53r7z2lbczvxifb0hrm9zrkqh2kpq2vwizkjaxmg")))

(define-public crate-qutex-0.0.6 (crate (name "qutex") (vers "0.0.6") (deps (list (crate-dep (name "crossbeam") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "futures") (req "^0.1") (default-features #t) (kind 0)))) (hash "0r066hq1lqv5mmnb0plq7nd06bnfmda3lnjipkm6q4hn6xvq6bfk")))

(define-public crate-qutex-0.0.7 (crate (name "qutex") (vers "0.0.7") (deps (list (crate-dep (name "crossbeam") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "futures") (req "^0.1") (default-features #t) (kind 0)))) (hash "15y47iljig28chbgzxkyi3cqr6lf2njc7i1bmhrn73bqp15al5k3")))

(define-public crate-qutex-0.0.8 (crate (name "qutex") (vers "0.0.8") (deps (list (crate-dep (name "crossbeam") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "futures") (req "^0.1") (default-features #t) (kind 0)))) (hash "0lrayh86zpz5w9ir80g5m09d6p5pgh0la826cwxlpp9f8w57n70h")))

(define-public crate-qutex-0.0.9 (crate (name "qutex") (vers "0.0.9") (deps (list (crate-dep (name "crossbeam") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "futures") (req "^0.1") (default-features #t) (kind 0)))) (hash "1c4y5v8igdzzf26vwv4dh10iwnriqiwcmr4q7b47bsgwnpbc0haq")))

(define-public crate-qutex-0.1 (crate (name "qutex") (vers "0.1.0") (deps (list (crate-dep (name "crossbeam") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "futures") (req "^0.1") (default-features #t) (kind 0)))) (hash "05r3317nqyvcmisvgccn6ryw055jadqcli11a36drhm14c5j8vx4")))

(define-public crate-qutex-0.1 (crate (name "qutex") (vers "0.1.1") (deps (list (crate-dep (name "crossbeam") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "futures") (req "^0.1") (default-features #t) (kind 0)))) (hash "0mrikgk8k1xy3mvyim46ricgzjaa6jx8823x8y6islfxi7s1fqwh")))

(define-public crate-qutex-0.2 (crate (name "qutex") (vers "0.2.0") (deps (list (crate-dep (name "crossbeam") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "futures") (req "^0.1") (default-features #t) (kind 0)))) (hash "0f46gdfm67kvj417saxk5sy37n4hl9fhlmv2v22si2baf8hpfcrw")))

(define-public crate-qutex-0.2 (crate (name "qutex") (vers "0.2.1") (deps (list (crate-dep (name "crossbeam") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "futures") (req "^0.1") (default-features #t) (kind 0)))) (hash "1rzhyxaszb47p4n3pjp88npigywlgisnh3lg3ssyin67x6agwssx")))

(define-public crate-qutex-0.2 (crate (name "qutex") (vers "0.2.2") (deps (list (crate-dep (name "crossbeam") (req "^0.7") (default-features #t) (kind 0)) (crate-dep (name "futures") (req "^0.1") (default-features #t) (kind 0)))) (hash "1xfgxfzqb15l8cnlr425al6dqny2w6gl304m4zahlcwlaqcsjwmm")))

(define-public crate-qutex-0.2 (crate (name "qutex") (vers "0.2.3") (deps (list (crate-dep (name "crossbeam") (req "^0.7") (default-features #t) (kind 0)) (crate-dep (name "futures") (req "^0.1") (default-features #t) (kind 0)))) (hash "188by93n3k5scyy6ng3ccv8ssfsqk53imgi5qbj26b4zzgwjahq8")))

(define-public crate-qutex-0.2 (crate (name "qutex") (vers "0.2.4") (deps (list (crate-dep (name "crossbeam") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "futures") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "futures03") (req "^0.3.0-alpha.14") (features (quote ("compat"))) (optional #t) (default-features #t) (kind 0) (package "futures-preview")))) (hash "0gw8164yaacz9hxjc5cbc3d8vbbpj0inn2j5z6bc2wyplcdsb96d") (features (quote (("default") ("async_await" "futures03"))))))

(define-public crate-qutex-0.2 (crate (name "qutex") (vers "0.2.5") (deps (list (crate-dep (name "crossbeam") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "futures") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "futures03") (req "^0.3.0-alpha.14") (features (quote ("compat"))) (optional #t) (default-features #t) (kind 0) (package "futures-preview")))) (hash "1kg283sbyp4qsd9sc4npyrn5gfy5x3m5z0c7zf03wkwnqxhz7fvk") (features (quote (("default") ("async_await" "futures03"))))))

(define-public crate-qutex-0.2 (crate (name "qutex") (vers "0.2.6") (deps (list (crate-dep (name "crossbeam") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "futures") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "futures03") (req "^0.3.0-alpha.14") (features (quote ("compat"))) (optional #t) (default-features #t) (kind 0) (package "futures-preview")))) (hash "0mqp7a3yrr9cicww5k89i2qcvx8yw1lxqgq3cb5f7c6qwww84xqi") (features (quote (("default") ("async_await" "futures03"))))))

