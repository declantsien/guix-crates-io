(define-module (crates-io qu at) #:use-module (crates-io))

(define-public crate-quat-0.1 (crate (name "quat") (vers "0.1.0") (deps (list (crate-dep (name "number_traits") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "vec4") (req "^0.1") (default-features #t) (kind 0)))) (hash "0fs40kx4fjlk472kcaw9jxzxrf1fk4ib1sl96akif2hn1hpii4fh")))

(define-public crate-quat-0.1 (crate (name "quat") (vers "0.1.1") (deps (list (crate-dep (name "number_traits") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "vec4") (req "^0.1") (default-features #t) (kind 0)))) (hash "1jw3qfxn3n7c4k90h6v99b4s6wyfvjzf07nhjy42j3h0dsh8spp6")))

(define-public crate-quat-0.1 (crate (name "quat") (vers "0.1.2") (deps (list (crate-dep (name "number_traits") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "vec4") (req "^0.1") (default-features #t) (kind 0)))) (hash "1zl583nihi6j3ff6mx5cynlcbq09y2xfpn96nfa11jxc682xnlxr")))

(define-public crate-quat-0.2 (crate (name "quat") (vers "0.2.0") (deps (list (crate-dep (name "cast_trait") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "vec4") (req "^0.2") (default-features #t) (kind 0)))) (hash "1lpn08blsdch3d04yf7gay4nbaifb054mgrlfqfj4hbai6dq3mjc")))

(define-public crate-quat-0.2 (crate (name "quat") (vers "0.2.1") (deps (list (crate-dep (name "cast_trait") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "vec4") (req "^0.2") (default-features #t) (kind 0)))) (hash "1lp2xl2v87arp3xfjyn1sy8ig336j44ya1c1k87wspc9gd5c7ams")))

(define-public crate-quaternion-0.0.0 (crate (name "quaternion") (vers "0.0.0") (deps (list (crate-dep (name "vecmath") (req "^0.0.3") (default-features #t) (kind 0)))) (hash "13c1flpsw9zwr4rhnnns8kcrlmf8f7k4cv8fvvwj862skn8h5nc8")))

(define-public crate-quaternion-0.0.1 (crate (name "quaternion") (vers "0.0.1") (deps (list (crate-dep (name "vecmath") (req "^0.0.3") (default-features #t) (kind 0)))) (hash "0dgvxs8shgvx0yci02mxqh1ilfa4i3f980ywmbg6lh92qg2fj3j3")))

(define-public crate-quaternion-0.0.2 (crate (name "quaternion") (vers "0.0.2") (deps (list (crate-dep (name "vecmath") (req "^0.0.5") (default-features #t) (kind 0)))) (hash "0lh1jla1qblswcizyrf0zjc492dn56bxpxh2jf3mrs4b13m2niw0")))

(define-public crate-quaternion-0.0.5 (crate (name "quaternion") (vers "0.0.5") (deps (list (crate-dep (name "vecmath") (req "^0.0.5") (default-features #t) (kind 0)))) (hash "1snmvjqfl5vmssqzkhwf5s8vmqxz0qai5kii10y67x08q4szc8sb")))

(define-public crate-quaternion-0.0.6 (crate (name "quaternion") (vers "0.0.6") (deps (list (crate-dep (name "vecmath") (req "^0.0.22") (default-features #t) (kind 0)))) (hash "1kfjcz6g669m4js26dwwia22r88q0hnkldyn04ihk9d370n73f20")))

(define-public crate-quaternion-0.0.7 (crate (name "quaternion") (vers "0.0.7") (deps (list (crate-dep (name "vecmath") (req "^0.0.23") (default-features #t) (kind 0)))) (hash "10sj4ipsl9gq65f4y70h956c7d00r2mw7wgilcpic5874b6b77rv")))

(define-public crate-quaternion-0.1 (crate (name "quaternion") (vers "0.1.0") (deps (list (crate-dep (name "vecmath") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "1d7q8ns95cybb27xcgz8yilss675hpbcbw5305k0mr6ijajli8jj")))

(define-public crate-quaternion-0.2 (crate (name "quaternion") (vers "0.2.0") (deps (list (crate-dep (name "vecmath") (req "^0.2.0") (default-features #t) (kind 0)))) (hash "11s0y0370krddgl3f2999g85nwkacxwaa0qdsqa0vc64v34xgaw0")))

(define-public crate-quaternion-0.2 (crate (name "quaternion") (vers "0.2.1") (deps (list (crate-dep (name "vecmath") (req "^0.2.0") (default-features #t) (kind 0)))) (hash "0k5y57ykc4d9gs38wqdwmmds4avd7gjrfjabm27xb18b272syv6g")))

(define-public crate-quaternion-0.3 (crate (name "quaternion") (vers "0.3.0") (deps (list (crate-dep (name "vecmath") (req "^0.3.0") (default-features #t) (kind 0)))) (hash "0nbq4yqz5p73cc9i6h7ax3gxpp74yyphpbg8a93b4q32i6n0gdg1")))

(define-public crate-quaternion-0.3 (crate (name "quaternion") (vers "0.3.1") (deps (list (crate-dep (name "vecmath") (req "^0.3.0") (default-features #t) (kind 0)))) (hash "0xg66jyp83xawdsk7i52k40id0w14ffzcm9rwl060vkpw9l82ngm")))

(define-public crate-quaternion-0.4 (crate (name "quaternion") (vers "0.4.0") (deps (list (crate-dep (name "vecmath") (req "^1.0.0") (default-features #t) (kind 0)))) (hash "19prb9xdml2j9amz3nd4x3a3xdhivz6f0p4w2fxbdnzm864dk74y")))

(define-public crate-quaternion-0.4 (crate (name "quaternion") (vers "0.4.1") (deps (list (crate-dep (name "vecmath") (req "^1.0.0") (default-features #t) (kind 0)))) (hash "05n2v25v3w4jhv3hfadmk5vgrndrbhlp865a4n2kci8ldlil7109")))

(define-public crate-quaternion-1 (crate (name "quaternion") (vers "1.0.0") (deps (list (crate-dep (name "vecmath") (req "^1.0.0") (default-features #t) (kind 0)))) (hash "1cy06x3sysk953pfj6c3xp5xfayz4czvf663ip4pckrxrysgsa7j")))

(define-public crate-quaternion-core-0.1 (crate (name "quaternion-core") (vers "0.1.0") (deps (list (crate-dep (name "autocfg") (req "^1") (default-features #t) (kind 1)) (crate-dep (name "num-traits") (req "^0.2.15") (kind 0)))) (hash "09clgij883f3zjvc6bbnh9kgz27ljqrmqhrrpi8hlf1mz47izgbi") (features (quote (("std" "num-traits/std") ("simd") ("libm" "num-traits/libm") ("fma") ("default" "std"))))))

(define-public crate-quaternion-core-0.2 (crate (name "quaternion-core") (vers "0.2.0") (deps (list (crate-dep (name "autocfg") (req "^1") (default-features #t) (kind 1)) (crate-dep (name "num-traits") (req "^0.2.15") (kind 0)))) (hash "14dw6wm5z7d4zjx7vd92484837mrv1205xl98v2a7iz3n6r372j2") (features (quote (("std" "num-traits/std") ("simd") ("libm" "num-traits/libm") ("fma") ("default" "std"))))))

(define-public crate-quaternion-core-0.2 (crate (name "quaternion-core") (vers "0.2.1") (deps (list (crate-dep (name "autocfg") (req "^1") (default-features #t) (kind 1)) (crate-dep (name "num-traits") (req "^0.2.15") (kind 0)))) (hash "0f4fav3q0n8m90q0accpf5dcfw6rdb18l0ahybvricz73f7hrwll") (features (quote (("std" "num-traits/std") ("simd") ("libm" "num-traits/libm") ("fma") ("default" "std"))))))

(define-public crate-quaternion-core-0.3 (crate (name "quaternion-core") (vers "0.3.0") (deps (list (crate-dep (name "autocfg") (req "^1") (default-features #t) (kind 1)) (crate-dep (name "num-traits") (req "^0.2.15") (kind 0)))) (hash "0n3yl1isvdkz4sl41ndj7rz94hci9jnzvdj4sha90piv7jyfxf5z") (features (quote (("std" "num-traits/std") ("libm" "num-traits/libm") ("fma") ("default" "std"))))))

(define-public crate-quaternion-core-0.3 (crate (name "quaternion-core") (vers "0.3.1") (deps (list (crate-dep (name "autocfg") (req "^1") (default-features #t) (kind 1)) (crate-dep (name "num-traits") (req "^0.2.15") (kind 0)))) (hash "04vax1svsbyrifqks1hxrjy350srg7ax5219ld6zw0pp31mvmziy") (features (quote (("std" "num-traits/std") ("libm" "num-traits/libm") ("fma") ("default" "std"))))))

(define-public crate-quaternion-core-0.3 (crate (name "quaternion-core") (vers "0.3.2") (deps (list (crate-dep (name "autocfg") (req "^1") (default-features #t) (kind 1)) (crate-dep (name "num-traits") (req "^0.2.15") (kind 0)))) (hash "1i9jr4yj03b40djkwv3q46mivfk3wqhghrv0z3qqw2kfb3cjcv2k") (features (quote (("std" "num-traits/std") ("libm" "num-traits/libm") ("fma") ("default" "std"))))))

(define-public crate-quaternion-core-0.3 (crate (name "quaternion-core") (vers "0.3.3") (deps (list (crate-dep (name "autocfg") (req "^1") (default-features #t) (kind 1)) (crate-dep (name "num-traits") (req "^0.2.15") (kind 0)))) (hash "1xnqz7akfxkp2cc4l05v0d555yx0xbi5njm3ysn1bgray2n0zl4f") (features (quote (("std" "num-traits/std") ("libm" "num-traits/libm") ("fma") ("default" "std"))))))

(define-public crate-quaternion-core-0.3 (crate (name "quaternion-core") (vers "0.3.4") (deps (list (crate-dep (name "num-traits") (req "^0.2.15") (kind 0)))) (hash "0k9z5n8srv4s0nx3vrwm9496pv3hn8wp2zi91q8jz2f1j4q7zlp4") (features (quote (("std" "num-traits/std") ("libm" "num-traits/libm") ("fma") ("default" "std"))))))

(define-public crate-quaternion-core-0.3 (crate (name "quaternion-core") (vers "0.3.5") (deps (list (crate-dep (name "num-traits") (req "^0.2.15") (kind 0)))) (hash "1syismbzr36bwdqyhlv69aal938f03l082gkz82shvn80v7l8lcv") (features (quote (("std" "num-traits/std") ("libm" "num-traits/libm") ("fma") ("default" "std"))))))

(define-public crate-quaternion-core-0.4 (crate (name "quaternion-core") (vers "0.4.0") (deps (list (crate-dep (name "num-traits") (req "^0.2.15") (kind 0)))) (hash "0hmiygsii334gwnp33n8bh252y1r3shv1rgjqsy2qxi7g2nlxnmb") (features (quote (("std" "num-traits/std") ("norm-sqrt") ("libm" "num-traits/libm") ("fma") ("default" "std")))) (yanked #t)))

(define-public crate-quaternion-core-0.4 (crate (name "quaternion-core") (vers "0.4.1") (deps (list (crate-dep (name "num-traits") (req "^0.2.15") (kind 0)))) (hash "1bbzdxnqk2pcz1yr0p2fhr7wan9b0jzvjm85qwij3162mzyy8vh4") (features (quote (("std" "num-traits/std") ("norm-sqrt") ("libm" "num-traits/libm") ("fma") ("default" "std")))) (yanked #t)))

(define-public crate-quaternion-core-0.4 (crate (name "quaternion-core") (vers "0.4.2") (deps (list (crate-dep (name "num-traits") (req "^0.2.15") (kind 0)))) (hash "0s9wp8fg8bdz6584avffdlv98qkk4s3f9by497war30g8xwxmsk5") (features (quote (("std" "num-traits/std") ("norm-sqrt") ("libm" "num-traits/libm") ("fma") ("default" "std"))))))

(define-public crate-quaternion-core-0.4 (crate (name "quaternion-core") (vers "0.4.3") (deps (list (crate-dep (name "num-traits") (req "^0.2.17") (kind 0)))) (hash "02dry8bwhbmm1rqrxgqkiyss8bm2vgisxp3i6zmy1bbscnkmgfpr") (features (quote (("std" "num-traits/std") ("norm-sqrt") ("libm" "num-traits/libm") ("fma") ("default" "std"))))))

(define-public crate-quaternion-core-0.5 (crate (name "quaternion-core") (vers "0.5.0") (deps (list (crate-dep (name "num-traits") (req "^0.2.17") (kind 0)) (crate-dep (name "serde") (req "^1.0.193") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)))) (hash "0k8yj588wz10li3pbwz45ng0l50kjkhxbsk6rrjsy78ym7scqs4g") (features (quote (("std" "num-traits/std") ("serde-serialize" "serde") ("norm-sqrt") ("libm" "num-traits/libm") ("fma") ("default" "std"))))))

(define-public crate-quaternion-matrix-0.0.1 (crate (name "quaternion-matrix") (vers "0.0.1") (deps (list (crate-dep (name "num") (req "^0.4") (default-features #t) (kind 0)))) (hash "184gimd45r8r6xix6zw9rhqm9c5aaklhbvk7z3s03qlzfl5c631j") (yanked #t)))

(define-public crate-quaternion-matrix-0.0.2 (crate (name "quaternion-matrix") (vers "0.0.2") (deps (list (crate-dep (name "num") (req "^0.4") (default-features #t) (kind 0)))) (hash "03zqb8f13hnv95ddb0sg0f378irsx3kid2pzhha8qcla4zypzhcb") (yanked #t)))

(define-public crate-quaternion-matrix-0.0.3 (crate (name "quaternion-matrix") (vers "0.0.3") (deps (list (crate-dep (name "num") (req "^0.4") (default-features #t) (kind 0)))) (hash "115p09hj7p3mnycx3hwdsypj2ggmknrhds8ikfzv1ywzwxm77wr6") (yanked #t)))

(define-public crate-quaternion-matrix-0.0.4 (crate (name "quaternion-matrix") (vers "0.0.4") (deps (list (crate-dep (name "num") (req "^0.4") (default-features #t) (kind 0)))) (hash "0cz73vg84b6xnnxdq3rky911cn913dzabf7h6fjblz9v5d3jlb6l") (yanked #t)))

(define-public crate-quaternion-matrix-0.0.5 (crate (name "quaternion-matrix") (vers "0.0.5") (deps (list (crate-dep (name "num") (req "^0.4") (default-features #t) (kind 0)))) (hash "1yg7q4vfzfzbipp8p9zz0vv9skz6w9bv8r99njn4pbbbryx8lsi2") (yanked #t)))

(define-public crate-quaternion-matrix-0.0.6 (crate (name "quaternion-matrix") (vers "0.0.6") (deps (list (crate-dep (name "num") (req "^0.4") (default-features #t) (kind 0)))) (hash "12lqxdi1q5fhaavydj150a2lxr80qizllgvx2bqrr2392lf943k6") (yanked #t)))

(define-public crate-quaternion-matrix-0.0.7 (crate (name "quaternion-matrix") (vers "0.0.7") (deps (list (crate-dep (name "num") (req "^0.4") (default-features #t) (kind 0)))) (hash "15b7mqqsvvdfp2d5m3n25zrlcdvsic8rwz6y126gy082rnz5fbg9") (yanked #t)))

(define-public crate-quaternion-matrix-0.0.8 (crate (name "quaternion-matrix") (vers "0.0.8") (deps (list (crate-dep (name "num") (req "^0.4") (default-features #t) (kind 0)))) (hash "0zgn2jfp876f5nl7q88nf2ni5vbxwfps4hn38c0v8cncwqclpysl") (yanked #t)))

(define-public crate-quaternion-matrix-0.1 (crate (name "quaternion-matrix") (vers "0.1.1") (deps (list (crate-dep (name "num") (req "^0.4") (default-features #t) (kind 0)))) (hash "1js1hi3hzgnfalhxpbhgawvqxngqmrj0r667fkv19n62y6agy9vh") (yanked #t)))

(define-public crate-quaternion-matrix-0.1 (crate (name "quaternion-matrix") (vers "0.1.2") (deps (list (crate-dep (name "num") (req "^0.4") (default-features #t) (kind 0)))) (hash "0j1r4p1jbfi0gyizqaxrs6l0wdhzif362cnmgcj4m9mja1b6bsbc") (yanked #t)))

(define-public crate-quaternion-matrix-0.1 (crate (name "quaternion-matrix") (vers "0.1.3") (deps (list (crate-dep (name "num") (req "^0.4") (default-features #t) (kind 0)))) (hash "1rhnq3yblr5h9pnnwf6iqabfi3mjg5wgpgr9icsg8npahs4x02x0") (yanked #t)))

(define-public crate-quaternion-matrix-0.1 (crate (name "quaternion-matrix") (vers "0.1.4") (deps (list (crate-dep (name "num") (req "^0.4") (default-features #t) (kind 0)))) (hash "1spv13cjl5r19saag72vfa16zf8d6vivs35c4rbj0w5hczdp358m")))

(define-public crate-quaternion-wrapper-0.1 (crate (name "quaternion-wrapper") (vers "0.1.0") (deps (list (crate-dep (name "autocfg") (req "^1") (default-features #t) (kind 1)) (crate-dep (name "num-traits") (req "^0.2") (kind 0)) (crate-dep (name "quaternion-core") (req "^0.1.0") (kind 0)))) (hash "05chj6vcm2yg1n9i0fq3b3x2jz27vj6h0m9n7q3h8jf75kxb7gz7") (features (quote (("std" "quaternion-core/std" "num-traits/std") ("simd" "quaternion-core/simd") ("libm" "quaternion-core/libm" "num-traits/libm") ("fma" "quaternion-core/fma") ("default" "std"))))))

(define-public crate-quaternion-wrapper-0.1 (crate (name "quaternion-wrapper") (vers "0.1.1") (deps (list (crate-dep (name "autocfg") (req "^1") (default-features #t) (kind 1)) (crate-dep (name "num-traits") (req "^0.2") (kind 0)) (crate-dep (name "quaternion-core") (req "^0.1.0") (kind 0)))) (hash "19fb37rr9yxdymqsfkmhvrm4dnw03n0w9hb19x42whba637ff989") (features (quote (("std" "quaternion-core/std" "num-traits/std") ("simd" "quaternion-core/simd") ("libm" "quaternion-core/libm" "num-traits/libm") ("fma" "quaternion-core/fma") ("default" "std"))))))

(define-public crate-quaternion-wrapper-0.2 (crate (name "quaternion-wrapper") (vers "0.2.0") (deps (list (crate-dep (name "autocfg") (req "^1") (default-features #t) (kind 1)) (crate-dep (name "num-traits") (req "^0.2.15") (kind 0)) (crate-dep (name "quaternion-core") (req "^0.3.0") (kind 0)))) (hash "0n7kj6s8gic7cxi9f6d00079wv00lxs48514lrg9rq8ybbmzzx30") (features (quote (("std" "quaternion-core/std" "num-traits/std") ("libm" "quaternion-core/libm" "num-traits/libm") ("fma" "quaternion-core/fma") ("default" "std"))))))

(define-public crate-quaternion-wrapper-0.3 (crate (name "quaternion-wrapper") (vers "0.3.0") (deps (list (crate-dep (name "num-traits") (req "^0.2.15") (kind 0)) (crate-dep (name "quaternion-core") (req "^0.4.2") (kind 0)))) (hash "1h6r93nzr2d1lvl909jg42z0mzp8qhi1khr43q8656c9fjvvdya5") (features (quote (("std" "quaternion-core/std" "num-traits/std") ("norm-sqrt" "quaternion-core/norm-sqrt") ("libm" "quaternion-core/libm" "num-traits/libm") ("fma" "quaternion-core/fma") ("default" "std"))))))

(define-public crate-quaternion_averager-0.1 (crate (name "quaternion_averager") (vers "0.1.0") (deps (list (crate-dep (name "approx") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "nalgebra") (req "^0.18") (default-features #t) (kind 0)))) (hash "12qxh7kmj16gr25dmnqg7ak8klvinfwmk35g4j8w36gblld6mvd1")))

(define-public crate-quaternion_averager-0.1 (crate (name "quaternion_averager") (vers "0.1.1") (deps (list (crate-dep (name "approx") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "nalgebra") (req "^0.18") (default-features #t) (kind 0)))) (hash "1w1d9zmylyidd2sxrzbhzlmznn8cj5vni8pir8pyjy8hgfjnjcrc")))

(define-public crate-quaternion_averager-0.1 (crate (name "quaternion_averager") (vers "0.1.2") (deps (list (crate-dep (name "approx") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "nalgebra") (req "^0.19") (default-features #t) (kind 0)))) (hash "0sd6j7cn46j8mcfhczb2kw7b65shyild1bg0cmc11jb0h79vp6rn")))

(define-public crate-quaternion_averager-0.1 (crate (name "quaternion_averager") (vers "0.1.3") (deps (list (crate-dep (name "approx") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "nalgebra") (req "^0.19") (default-features #t) (kind 0)))) (hash "1g5215miffwygpnvk1fvylpgwm5nwzafjm62zaix91fcz8j4jn92")))

(define-public crate-quaternions-0.5 (crate (name "quaternions") (vers "0.5.0-a1") (deps (list (crate-dep (name "num-traits") (req "^0.2") (default-features #t) (kind 0)))) (hash "0jj581bc62x5fg3sln26kc4lmr9is6knmrnqn6nfig38jiw9hr1v")))

(define-public crate-quaternions-0.5 (crate (name "quaternions") (vers "0.5.0-a2") (deps (list (crate-dep (name "num-traits") (req "^0.2") (default-features #t) (kind 0)))) (hash "0aa04mkjqvdgy5mqpb924qqhcv1d2107hczq0kdbihkk5fdyngc7")))

(define-public crate-quatrain-0.6 (crate (name "quatrain") (vers "0.6.0") (deps (list (crate-dep (name "anyhow") (req "^1.0.31") (default-features #t) (kind 0)) (crate-dep (name "auto_enums") (req "^0.7.5") (default-features #t) (kind 0)) (crate-dep (name "chrono") (req "^0.4.11") (default-features #t) (kind 0)) (crate-dep (name "derive-new") (req "^0.5.8") (default-features #t) (kind 0)) (crate-dep (name "dirs") (req "^2.0.2") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "getopts") (req "^0.2.21") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "katex") (req "^0.2.1") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.4.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "memchr") (req "^2.3.3") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.111") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_yaml") (req "^0.8.12") (default-features #t) (kind 0)) (crate-dep (name "tera") (req "^1.3.0") (optional #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.19") (default-features #t) (kind 0)) (crate-dep (name "unicase") (req "^2.6.0") (default-features #t) (kind 0)))) (hash "0zz094ddhbwrsg3jfa3syv8gfbsb2bvv78i0kgihrxb043j296sm") (features (quote (("quatrain" "tera" "getopts" "lazy_static" "dirs"))))))

(define-public crate-quatre-0.0.4 (crate (name "quatre") (vers "0.0.4-quatre") (hash "1hiw8fzc8yqf7r0y7xis97nz9rrk3j0fjza1gqa6ixsm46982imx") (yanked #t)))

