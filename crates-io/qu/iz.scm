(define-module (crates-io qu iz) #:use-module (crates-io))

(define-public crate-quiz-0.0.1 (crate (name "quiz") (vers "0.0.1") (deps (list (crate-dep (name "figures-rs") (req ">=0.0.1, <0.0.2") (default-features #t) (kind 0)) (crate-dep (name "termion") (req ">=1.5.5, <2.0.0") (default-features #t) (kind 0)))) (hash "1wi0q5y695fqi6k1g9g0mvb9xnlz5790cpdrdvp5sm5369zvmh82")))

(define-public crate-quiz-0.0.2 (crate (name "quiz") (vers "0.0.2") (deps (list (crate-dep (name "figures-rs") (req ">=0.0.1, <0.0.2") (default-features #t) (kind 0)) (crate-dep (name "termion") (req ">=1.5.5, <2.0.0") (default-features #t) (kind 0)))) (hash "04ksk5mjfw2qbiw55dys7qnywxg1amn582cjs0z5c3r2p9p9p3wf")))

(define-public crate-quiz-0.0.3 (crate (name "quiz") (vers "0.0.3") (deps (list (crate-dep (name "figures-rs") (req ">=0.0.1, <0.0.2") (default-features #t) (kind 0)) (crate-dep (name "termion") (req ">=1.5.5, <2.0.0") (default-features #t) (kind 0)))) (hash "1zf7ka4qz2nd306nl1vhs75zyzjbsm88mazqwvpyzj50hkfp2qap")))

(define-public crate-quizas-0.1 (crate (name "quizas") (vers "0.1.0") (hash "0j85if73zz6x17sqrqys0swmk79w66gawjcl6dgvy2hwdfff46px")))

(define-public crate-quizas-0.1 (crate (name "quizas") (vers "0.1.1") (hash "0skw5z2jnq7a73nbm28rw0rpbjm2zjalqvps6h09gb573wrfad21")))

(define-public crate-quizas-0.1 (crate (name "quizas") (vers "0.1.2") (hash "1ds1k6pkjz4bycw687ydm66g5wc7szsrifidvgdrdc23f2alfgjj")))

(define-public crate-quizas-0.1 (crate (name "quizas") (vers "0.1.2-1") (hash "1wp1q7ws1z1wwxdbz3dpigqnbliwh91k8203h23483w7vya62idr")))

(define-public crate-quizas-0.1 (crate (name "quizas") (vers "0.1.3") (hash "1vqs6ln8wcgw3m30j3rn41s7493rfz9id301xl6kgxfzssqmj7km")))

(define-public crate-quizas-0.1 (crate (name "quizas") (vers "0.1.4") (hash "117hz36rv5jccffk9lnmw2ah2b6c0jpxxyx8shmy5b4x7vqy24q0")))

(define-public crate-quizas-0.1 (crate (name "quizas") (vers "0.1.5") (hash "1jxizl15piy81dg86bmrc9gp9lk5f4i0i4kziy1fgv8lfgxnd7p0")))

(define-public crate-quizas-0.1 (crate (name "quizas") (vers "0.1.6") (hash "11vdqa3hfy3v03l5023xq7aqqaiwa12wlr4jqq64l1mlpgs2bn4d")))

(define-public crate-quizas-0.1 (crate (name "quizas") (vers "0.1.7") (hash "0120j52s45a6kynajv5n21jxbggq8x04ns4llyf9b0v10507shh1")))

(define-public crate-quizas-0.1 (crate (name "quizas") (vers "0.1.8") (hash "0s47h6s1g0xmli0n9fffmcndmsdy70wm5dj2b9w98fk85ax8vi63")))

(define-public crate-quizas-0.1 (crate (name "quizas") (vers "0.1.9") (hash "0g9jw9xic7dy8pl8kfikx155a86yy2hdkib89348a1srfrn9m0z5")))

(define-public crate-quizer-1 (crate (name "quizer") (vers "1.0.0") (deps (list (crate-dep (name "bincode") (req "^1.3.3") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.152") (features (quote ("serde_derive"))) (default-features #t) (kind 0)))) (hash "1zai94cl6vywsjpim15lfjsva2jb09skzr0hczgyd8d9ny8kd963") (yanked #t)))

(define-public crate-quizler-0.0.0 (crate (name "quizler") (vers "0.0.0") (deps (list (crate-dep (name "actix") (req "^0.13") (default-features #t) (kind 0)) (crate-dep (name "actix-rt") (req "^2.8.0") (default-features #t) (kind 0)) (crate-dep (name "actix-web") (req "^4") (default-features #t) (kind 0)) (crate-dep (name "actix-web-actors") (req "^4") (default-features #t) (kind 0)) (crate-dep (name "dotenvy") (req "^0.15") (default-features #t) (kind 0)) (crate-dep (name "env_logger") (req "^0.10") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "rand_core") (req "^0.6.4") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1") (features (quote ("derive" "rc"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "uuid") (req "^1.3.2") (features (quote ("v4" "fast-rng" "macro-diagnostics" "serde"))) (default-features #t) (kind 0)))) (hash "0r3kj3bk0l7acir611cy2ypsjlaqpwl1l1nc00hyr15axwr5wv6c")))

(define-public crate-quizler-0.1 (crate (name "quizler") (vers "0.1.0-alpha") (deps (list (crate-dep (name "actix") (req "^0.13") (default-features #t) (kind 0)) (crate-dep (name "actix-cors") (req "^0.6.4") (default-features #t) (kind 0)) (crate-dep (name "actix-multipart") (req "^0.6") (default-features #t) (kind 0)) (crate-dep (name "actix-web") (req "^4") (default-features #t) (kind 0)) (crate-dep (name "actix-web-actors") (req "^4") (default-features #t) (kind 0)) (crate-dep (name "bytes") (req "^1.4") (default-features #t) (kind 0)) (crate-dep (name "dotenvy") (req "^0.15") (default-features #t) (kind 0)) (crate-dep (name "embeddy") (req "^0.1.1") (default-features #t) (kind 0)) (crate-dep (name "env_logger") (req "^0.10") (default-features #t) (kind 0)) (crate-dep (name "futures-util") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "mime") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "rand_core") (req "^0.6") (default-features #t) (kind 0)) (crate-dep (name "rustrict") (req "^0.7.6") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1") (features (quote ("derive" "rc"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "uuid") (req "^1.3.2") (features (quote ("v4" "fast-rng" "macro-diagnostics" "serde"))) (default-features #t) (kind 0)))) (hash "1rhh58sdzyfain2qndv0lf20h8g3r0qiaxx3wh1djg58w3q8p5h1")))

(define-public crate-quizzer-1 (crate (name "quizzer") (vers "1.0.0") (deps (list (crate-dep (name "bincode") (req "^1.3.3") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.152") (features (quote ("serde_derive"))) (default-features #t) (kind 0)))) (hash "17k7z95rbbbs30ys0f1np3162g003hh376bxj26ydgyj4w30cnqs")))

(define-public crate-quizzer-1 (crate (name "quizzer") (vers "1.0.1") (deps (list (crate-dep (name "bincode") (req "^1.3.3") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.152") (features (quote ("serde_derive"))) (default-features #t) (kind 0)))) (hash "0cqmqg8a9rfbbpr0v5fd4ga11l11mq6az81zss45ngb6sj0lv758")))

(define-public crate-quizzer-1 (crate (name "quizzer") (vers "1.0.2") (deps (list (crate-dep (name "bincode") (req "^1.3.3") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.152") (features (quote ("serde_derive"))) (default-features #t) (kind 0)))) (hash "0vcwh863h1lyk5crdjykw0hywbycsjnrbb4vbmcbniqn2jk2mabj")))

(define-public crate-quizzer-1 (crate (name "quizzer") (vers "1.1.2") (deps (list (crate-dep (name "bincode") (req "^1.3.3") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "cfg-if") (req "^1.0.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.152") (features (quote ("serde_derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.92") (optional #t) (default-features #t) (kind 0)))) (hash "1ysyr7sqa1n0fip5pnyy5c9pvm4g5g47qq5vyl7j91k2fpslibgz") (v 2) (features2 (quote (("json" "dep:serde_json") ("default" "dep:bincode"))))))

