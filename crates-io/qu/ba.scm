(define-module (crates-io qu ba) #:use-module (crates-io))

(define-public crate-qubaitian_hello_cargo-0.1 (crate (name "qubaitian_hello_cargo") (vers "0.1.0") (hash "0n8f3d4mvbcidlkavrmbnlg4g3jdm7wljpfyldapizzh2hnnr5r4")))

(define-public crate-qubaitian_minigrep-0.1 (crate (name "qubaitian_minigrep") (vers "0.1.0") (hash "0ncgbrw8hw1gz9pc7vfp8f5462x991gzx1j60d29x9n9m711r1p9")))

