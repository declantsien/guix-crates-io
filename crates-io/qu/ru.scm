(define-module (crates-io qu ru) #:use-module (crates-io))

(define-public crate-qurust-0.1 (crate (name "qurust") (vers "0.1.0") (deps (list (crate-dep (name "version-sync") (req "^0.9.5") (default-features #t) (kind 2)))) (hash "0swcgwmlw6fcbfshyj57f1bb12yb1yfilp02ql1b8cydva943cqp")))

(define-public crate-qurust-0.2 (crate (name "qurust") (vers "0.2.0") (deps (list (crate-dep (name "antlr-rust") (req "^0.3.0-beta") (default-features #t) (kind 0)) (crate-dep (name "indent") (req "^0.1.1") (default-features #t) (kind 0)) (crate-dep (name "indoc") (req "^2.0.5") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.10.4") (default-features #t) (kind 0)) (crate-dep (name "version-sync") (req "^0.9.5") (default-features #t) (kind 2)))) (hash "0d5inqwm7sia9k0zzjg0r9s3bkz7yyljf090wv38wdw22p5fmgcd")))

