(define-module (crates-io qu ri) #:use-module (crates-io))

(define-public crate-quriust-0.1 (crate (name "quriust") (vers "0.1.0") (deps (list (crate-dep (name "num-complex") (req "^0.4.4") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)))) (hash "105hfs7z6fshy1prasf4q9y2161cs2yyza4wlzcgg58j0cyj765h")))

(define-public crate-quriust-0.1 (crate (name "quriust") (vers "0.1.2") (deps (list (crate-dep (name "nalgebra") (req "^0.32.3") (default-features #t) (kind 0)) (crate-dep (name "num-complex") (req "^0.4.4") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)))) (hash "1pfcmxc770f4qidrc53dkgzm19abwbiizwikpzqga32a9qp5zzqy")))

(define-public crate-quriust-0.1 (crate (name "quriust") (vers "0.1.3") (deps (list (crate-dep (name "nalgebra") (req "^0.32.3") (default-features #t) (kind 0)) (crate-dep (name "num-complex") (req "^0.4.4") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)))) (hash "147ibnqz0qhfmird43x70jh4jy2sw25vhl9sjajfqahk0xap3r69")))

(define-public crate-quriust-0.1 (crate (name "quriust") (vers "0.1.4") (deps (list (crate-dep (name "nalgebra") (req "^0.32.3") (default-features #t) (kind 0)) (crate-dep (name "num-complex") (req "^0.4.4") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)))) (hash "0j68p16z22awg0cs69hi9zjij4b6s4plm6is5skidjxwhn93m3jv")))

(define-public crate-quriust-0.1 (crate (name "quriust") (vers "0.1.5") (deps (list (crate-dep (name "nalgebra") (req "^0.32.3") (default-features #t) (kind 0)) (crate-dep (name "num-complex") (req "^0.4.4") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)))) (hash "1idmrawca91zq43ghxxkk492ir6ljsdzz51cij5q9yz8sdabfx2h")))

(define-public crate-quriust-0.1 (crate (name "quriust") (vers "0.1.6") (deps (list (crate-dep (name "nalgebra") (req "^0.32.3") (default-features #t) (kind 0)) (crate-dep (name "num-complex") (req "^0.4.4") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)))) (hash "18lag84fwml2vcsxsmy1fqv1hxz7j7gj854632khwxkc27mncnf5")))

(define-public crate-quriust-0.1 (crate (name "quriust") (vers "0.1.7") (deps (list (crate-dep (name "nalgebra") (req "^0.32.3") (default-features #t) (kind 0)) (crate-dep (name "num-complex") (req "^0.4.4") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)))) (hash "1xnvwchmkz3clww7j7z24fyi0x0pijf5mlm7a1rcxyysn3zwhims")))

(define-public crate-quriust-0.1 (crate (name "quriust") (vers "0.1.8") (deps (list (crate-dep (name "nalgebra") (req "^0.32.3") (default-features #t) (kind 0)) (crate-dep (name "num-complex") (req "^0.4.4") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)))) (hash "1qfnm53zxxpdv77i246anqabsgr9ad2ss3na5nc3zngbi6h76hri")))

(define-public crate-quriust-0.1 (crate (name "quriust") (vers "0.1.9") (deps (list (crate-dep (name "nalgebra") (req "^0.32.3") (default-features #t) (kind 0)) (crate-dep (name "num-complex") (req "^0.4.4") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)))) (hash "05wmd9201l9cvb15d3dh0j9rdrd0fl6lwlyfdccmprrcaprmcg81")))

(define-public crate-quriust-0.2 (crate (name "quriust") (vers "0.2.0") (deps (list (crate-dep (name "nalgebra") (req "^0.32.3") (default-features #t) (kind 0)) (crate-dep (name "num-complex") (req "^0.4.4") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)))) (hash "048rwd560hdcmbdcrgky7c8bz48z2r82ic09x9w5i2kxbdc2jjcq")))

