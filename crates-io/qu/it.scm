(define-module (crates-io qu it) #:use-module (crates-io))

(define-public crate-quit-0.1 (crate (name "quit") (vers "0.1.0") (deps (list (crate-dep (name "lazy_static") (req "^1.4") (default-features #t) (kind 2)) (crate-dep (name "quit_macros") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "1w0pws2h7xdlaqs20m69q2j92qp5qaqvs4gvhwc7cb95y9wc32n2")))

(define-public crate-quit-0.2 (crate (name "quit") (vers "0.2.0") (deps (list (crate-dep (name "lazy_static") (req "^1.4") (default-features #t) (kind 2)) (crate-dep (name "quit_macros") (req "^0.2.0") (default-features #t) (kind 0)))) (hash "16rsl1nsw14zi24m6spzbcymp03fcl881qq7dcqkdagi47kx7607")))

(define-public crate-quit-0.2 (crate (name "quit") (vers "0.2.1") (deps (list (crate-dep (name "lazy_static") (req "^1.4") (default-features #t) (kind 2)) (crate-dep (name "quit_macros") (req "= 0.2.1") (default-features #t) (kind 0)))) (hash "09r7gf890lrg0a7zqwd1l11cw2x85jl7azd34y01nphq8qygqh7d")))

(define-public crate-quit-1 (crate (name "quit") (vers "1.0.0") (deps (list (crate-dep (name "lazy_static") (req "^1.4") (default-features #t) (kind 2)) (crate-dep (name "quit_macros") (req "= 1.0.0") (default-features #t) (kind 0)))) (hash "0j8fxsisnz6igjsib5zqrym0qg462vqba0fy4n9kqaq74yqv2n3j")))

(define-public crate-quit-1 (crate (name "quit") (vers "1.1.0") (deps (list (crate-dep (name "lazy_static") (req "^1.4") (default-features #t) (kind 2)) (crate-dep (name "quit_macros") (req "=1.1.0") (default-features #t) (kind 0)))) (hash "1b5jg3hnzdbwji7cncp09hhbkv6lamfrgc90ryi1ym3cc0aan0yb")))

(define-public crate-quit-1 (crate (name "quit") (vers "1.1.1") (deps (list (crate-dep (name "lazy_static") (req "^1.4") (default-features #t) (kind 2)) (crate-dep (name "quit_macros") (req "=1.1.1") (default-features #t) (kind 0)))) (hash "0j401dzrzmn9h292xb11gzwmmh8xvs4blphl11qf2gy78zxv3n65")))

(define-public crate-quit-1 (crate (name "quit") (vers "1.1.2") (deps (list (crate-dep (name "lazy_static") (req "^1.4") (default-features #t) (kind 2)) (crate-dep (name "quit_macros") (req "=1.1.2") (default-features #t) (kind 0)))) (hash "0mh7hhhps8pn6s6lv0sya3vk76rb4kvblq5f6nxm5715lhvbpx4l")))

(define-public crate-quit-1 (crate (name "quit") (vers "1.1.3") (deps (list (crate-dep (name "lazy_static") (req "^1.4") (default-features #t) (kind 2)) (crate-dep (name "quit_macros") (req "=1.1.3") (default-features #t) (kind 0)))) (hash "1fvx1qwb4ijsvfprj65ha416jviq086bdks0j8pnij2qalimdzfb")))

(define-public crate-quit-1 (crate (name "quit") (vers "1.1.4") (deps (list (crate-dep (name "lazy_static") (req "^1.4") (default-features #t) (kind 2)) (crate-dep (name "quit_macros") (req "=1.1.4") (default-features #t) (kind 0)))) (hash "17g4d23yhvp3y2fdn4j0wzn0lvzda0qpxygsgxc5h2znhvy19lxs")))

(define-public crate-quit-1 (crate (name "quit") (vers "1.2.0") (deps (list (crate-dep (name "quit_macros") (req "=1.2.0") (default-features #t) (kind 0)))) (hash "0482h1jg745g29nmny7mys2xnglk0qm7792s1i8hz4fbfvvlq265") (rust-version "1.60.0")))

(define-public crate-quit-2 (crate (name "quit") (vers "2.0.0") (deps (list (crate-dep (name "quit_macros") (req "=2.0.0") (default-features #t) (kind 0)))) (hash "16v6imwrw0niqf925zrxcc7a2q3ca0scy677j1mvv09x4c4cd460") (features (quote (("__unstable_tests")))) (rust-version "1.64.0")))

(define-public crate-quit_macros-0.1 (crate (name "quit_macros") (vers "0.1.0") (deps (list (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "15rivqi8q8h4rpnw10v81s5j32bqk7f6zcl3qdnkr04sav4yhcq0")))

(define-public crate-quit_macros-0.2 (crate (name "quit_macros") (vers "0.2.0") (deps (list (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "0zyfmcsb2lgjimzv0lvhvnl9r3j9nicdqffrip2n4a9cppl2km3r")))

(define-public crate-quit_macros-0.2 (crate (name "quit_macros") (vers "0.2.1") (deps (list (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "18gvz82asx10jqmyzrkgqlavirf6lv35rjkl1si43dp7jlfx4zb9")))

(define-public crate-quit_macros-1 (crate (name "quit_macros") (vers "1.0.0") (deps (list (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "0dqnk84krc3q5wxvp8qiys1qbiagyxawh3m04vwg2lzzkw6pn10w")))

(define-public crate-quit_macros-1 (crate (name "quit_macros") (vers "1.1.0") (hash "0alhvaq4f84fn0ys1j61pn88d1aaq9zx4paiqlqpm6mmirn91qyw")))

(define-public crate-quit_macros-1 (crate (name "quit_macros") (vers "1.1.1") (hash "1zyqd56fg7ydwy9fyx1a1w37wpp2hrfb11wk4av8r0834awdhqk7")))

(define-public crate-quit_macros-1 (crate (name "quit_macros") (vers "1.1.2") (hash "0khvdv0h48hd43bflsvqf21z438vzyssgx5bmah9gbwg2gj26sgz")))

(define-public crate-quit_macros-1 (crate (name "quit_macros") (vers "1.1.3") (hash "1g3zbjjxlfqrjfxz2jxgmq5wb03lsk28rqarz7yp15hnny24vsr6")))

(define-public crate-quit_macros-1 (crate (name "quit_macros") (vers "1.1.4") (hash "1bhvg3z60r4msg0x1d2sai7578sypacrij3906f17qyw8xmn24d4")))

(define-public crate-quit_macros-1 (crate (name "quit_macros") (vers "1.2.0") (hash "0hj2c9hbymjkrj93ash4zhsa63f7mx0hjpg8zvp4iqvz5krz877g") (rust-version "1.60.0")))

(define-public crate-quit_macros-2 (crate (name "quit_macros") (vers "2.0.0") (hash "0r3myfghk6rwgbgk42yyx87c7j8f6vxm4yfiy9xas22xvnh2fjqx") (rust-version "1.64.0")))

(define-public crate-quitch-0.0.2 (crate (name "quitch") (vers "0.0.2") (deps (list (crate-dep (name "anyhow") (req "^1.0.81") (features (quote ("backtrace"))) (default-features #t) (kind 0)) (crate-dep (name "base16ct") (req "^0.2.0") (features (quote ("alloc"))) (default-features #t) (kind 0)) (crate-dep (name "chrono") (req "^0.4.35") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^4.5.2") (features (quote ("unicode" "wrap_help" "derive"))) (default-features #t) (kind 0)) (crate-dep (name "futures") (req "^0.3.30") (default-features #t) (kind 0)) (crate-dep (name "indexmap") (req "^2.2.5") (default-features #t) (kind 0)) (crate-dep (name "itertools") (req "^0.12.1") (default-features #t) (kind 0)) (crate-dep (name "sha1") (req "^0.10.6") (default-features #t) (kind 0)) (crate-dep (name "sqlx") (req "^0.7.4") (features (quote ("macros" "mysql" "chrono" "runtime-tokio" "tls-rustls"))) (kind 0)) (crate-dep (name "tokio") (req "^1.36.0") (features (quote ("macros" "rt-multi-thread"))) (default-features #t) (kind 0)) (crate-dep (name "url") (req "^2.5.0") (default-features #t) (kind 0)))) (hash "1v54wpylpz525a3pxqcgahl7ivvk41mpwxyi105livzc441yrmm4")))

(define-public crate-quitch-0.0.3 (crate (name "quitch") (vers "0.0.3") (deps (list (crate-dep (name "anyhow") (req "^1.0.81") (features (quote ("backtrace"))) (default-features #t) (kind 0)) (crate-dep (name "base16ct") (req "^0.2.0") (features (quote ("alloc"))) (default-features #t) (kind 0)) (crate-dep (name "chrono") (req "^0.4.35") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^4.5.2") (features (quote ("unicode" "wrap_help" "derive"))) (default-features #t) (kind 0)) (crate-dep (name "futures") (req "^0.3.30") (default-features #t) (kind 0)) (crate-dep (name "indexmap") (req "^2.2.5") (default-features #t) (kind 0)) (crate-dep (name "itertools") (req "^0.12.1") (default-features #t) (kind 0)) (crate-dep (name "sha1") (req "^0.10.6") (default-features #t) (kind 0)) (crate-dep (name "sqlx") (req "^0.7.4") (features (quote ("macros" "mysql" "chrono" "runtime-tokio" "tls-rustls"))) (kind 0)) (crate-dep (name "tokio") (req "^1.36.0") (features (quote ("macros" "rt-multi-thread"))) (default-features #t) (kind 0)) (crate-dep (name "url") (req "^2.5.0") (default-features #t) (kind 0)))) (hash "1jpqglv7avvbmfllnk9an4c2i9dsgjdnfxb8vvscamdlz5bj7mi3")))

(define-public crate-quitch-0.0.4 (crate (name "quitch") (vers "0.0.4") (deps (list (crate-dep (name "anyhow") (req "^1.0.81") (features (quote ("backtrace"))) (default-features #t) (kind 0)) (crate-dep (name "base16ct") (req "^0.2.0") (features (quote ("alloc"))) (default-features #t) (kind 0)) (crate-dep (name "chrono") (req "^0.4.35") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^4.5.2") (features (quote ("unicode" "wrap_help" "derive"))) (default-features #t) (kind 0)) (crate-dep (name "futures") (req "^0.3.30") (default-features #t) (kind 0)) (crate-dep (name "indexmap") (req "^2.2.5") (default-features #t) (kind 0)) (crate-dep (name "itertools") (req "^0.12.1") (default-features #t) (kind 0)) (crate-dep (name "sha1") (req "^0.10.6") (default-features #t) (kind 0)) (crate-dep (name "sqlx") (req "^0.7.4") (features (quote ("macros" "mysql" "chrono" "runtime-tokio" "tls-rustls"))) (kind 0)) (crate-dep (name "tokio") (req "^1.36.0") (features (quote ("macros" "rt-multi-thread"))) (default-features #t) (kind 0)) (crate-dep (name "url") (req "^2.5.0") (default-features #t) (kind 0)))) (hash "0cnnf9cc3j3ya8bq5qw7flgma78v88knwpv1r87jcvk9d8nvczvy")))

(define-public crate-quitters-0.1 (crate (name "quitters") (vers "0.1.0") (deps (list (crate-dep (name "once_cell") (req "^1.15.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.6.0") (features (quote ("std" "perf"))) (kind 0)) (crate-dep (name "semver") (req "^1.0.14") (default-features #t) (kind 0)))) (hash "055hdmm9b78jpdirsbfl7bi2zi1zjwiskjxma2r1a8adv3kwv378")))

