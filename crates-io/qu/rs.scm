(define-module (crates-io qu rs) #:use-module (crates-io))

(define-public crate-qurs-0.1 (crate (name "qurs") (vers "0.1.0") (hash "1h4sglxm5zfym5b24nwnbw9cz80xfwi7yqas9l9qvdiwvysdf44y")))

(define-public crate-qurs-0.1 (crate (name "qurs") (vers "0.1.1") (deps (list (crate-dep (name "bindgen") (req "^0.60.1") (default-features #t) (kind 1)) (crate-dep (name "cc") (req "^1.0") (features (quote ("parallel"))) (default-features #t) (kind 1)) (crate-dep (name "num") (req "^0.4.0") (default-features #t) (kind 0)) (crate-dep (name "ordered-float") (req "^3.0.0") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)) (crate-dep (name "superslice") (req "^1.0.0") (default-features #t) (kind 0)))) (hash "01cfcx6hnm5kaykjp2v27nn8811rdw9z5vw66v57a1hdv66d3dav")))

(define-public crate-qurs-0.2 (crate (name "qurs") (vers "0.2.0") (deps (list (crate-dep (name "bindgen") (req "^0.60.1") (default-features #t) (kind 1)) (crate-dep (name "cc") (req "^1.0") (features (quote ("parallel"))) (default-features #t) (kind 1)) (crate-dep (name "num") (req "^0.4.0") (default-features #t) (kind 0)) (crate-dep (name "ordered-float") (req "^3.0.0") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)) (crate-dep (name "superslice") (req "^1.0.0") (default-features #t) (kind 0)))) (hash "126ii810g1w6a4diw2w4hklx53iq94r1fmpldwpvy50qyspb8asp")))

