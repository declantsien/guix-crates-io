(define-module (crates-io qu et) #:use-module (crates-io))

(define-public crate-quetta-0.1 (crate (name "quetta") (vers "0.1.0") (hash "0xps6bf11qwddbkrg5mwr428jcyh8d32k3jiigj4lm7r431048i6")))

(define-public crate-quetzal-0.1 (crate (name "quetzal") (vers "0.1.0") (hash "0y70kqw1fh9qnljv4jpr5714xdab91hpsfbirw7gnwbibbrbyzg8")))

