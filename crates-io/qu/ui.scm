(define-module (crates-io qu ui) #:use-module (crates-io))

(define-public crate-quuid-0.1 (crate (name "quuid") (vers "0.1.0") (deps (list (crate-dep (name "clipboard") (req "^0.5.0") (default-features #t) (kind 0)) (crate-dep (name "uuid") (req "^0.7") (features (quote ("v4"))) (default-features #t) (kind 0)))) (hash "14hvpml5zcyh6s0gfy297w12zrryji96f7czhgf4hwggsw3kpnxc")))

(define-public crate-quuidy-0.1 (crate (name "quuidy") (vers "0.1.0") (deps (list (crate-dep (name "uuid") (req "^1.4.1") (features (quote ("v4" "fast-rng"))) (default-features #t) (kind 0)))) (hash "0y1vc7la2by6yhxmrs0xh3rkrbvsil13qq4i2myj1209xxpjzrqd")))

(define-public crate-quuidy-0.1 (crate (name "quuidy") (vers "0.1.1") (deps (list (crate-dep (name "clap") (req "^4.4.6") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "uuid") (req "^1.4.1") (features (quote ("v4" "fast-rng"))) (default-features #t) (kind 0)))) (hash "002isjqm7g2mwwkyli4w74g63y2nbfiv9idjjlpkq37h2zgs8rhh")))

(define-public crate-quuidy-0.1 (crate (name "quuidy") (vers "0.1.2") (deps (list (crate-dep (name "clap") (req "^4.4.6") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "uuid") (req "^1.7.0") (features (quote ("v4" "v7" "fast-rng"))) (default-features #t) (kind 0)))) (hash "1f115q0jp8wdxij9kiy26hmhbz4ibjv8vpjbgvk6hxnbq8iw9sx3")))

