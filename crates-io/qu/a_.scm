(define-module (crates-io qu a_) #:use-module (crates-io))

(define-public crate-qua_format-0.1 (crate (name "qua_format") (vers "0.1.0") (deps (list (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_yaml") (req "^0.8") (default-features #t) (kind 0)))) (hash "0vh151n4z4cg9lymg9nw9kfpizxj1lnilj30q17wkkn45lbvjdad")))

(define-public crate-qua_format-0.1 (crate (name "qua_format") (vers "0.1.1") (deps (list (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_yaml") (req "^0.8") (default-features #t) (kind 0)))) (hash "0xbcw8k9z1s6qbj10rp0zv970id7qx1dr3833diwfcv46ksmc496")))

(define-public crate-qua_format-0.1 (crate (name "qua_format") (vers "0.1.2") (deps (list (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_yaml") (req "^0.8") (default-features #t) (kind 0)))) (hash "0wn804f579891id7mhmpnrrqgn8rjxwmbcff4wam9f1ivjays85f")))

(define-public crate-qua_format-0.1 (crate (name "qua_format") (vers "0.1.3") (deps (list (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_yaml") (req "^0.8") (default-features #t) (kind 0)))) (hash "0bny52v611fblf00q4hps9skvlspnfwbvry03msc5y2wb94lpykl")))

(define-public crate-qua_format-0.1 (crate (name "qua_format") (vers "0.1.4") (deps (list (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_repr") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "serde_yaml") (req "^0.8") (default-features #t) (kind 0)))) (hash "1klaskf1p7zhyv0g09dcd0dvwh5rv2hxfhdzy87cpjnq00qkq3pl")))

(define-public crate-qua_format-0.1 (crate (name "qua_format") (vers "0.1.5") (deps (list (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_repr") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "serde_yaml") (req "^0.8") (default-features #t) (kind 0)))) (hash "0c6hf8dqgbfi2kf3g7hmabdig4190g0rapjila423qrv1xhd3avh")))

(define-public crate-qua_format-0.1 (crate (name "qua_format") (vers "0.1.6") (deps (list (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_repr") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "serde_yaml") (req "^0.8") (default-features #t) (kind 0)))) (hash "0n7816r9ayy9p29h8psk0j2nr8y0zxpz776i1gkp5qa3y8zwpyns")))

(define-public crate-qua_format-0.1 (crate (name "qua_format") (vers "0.1.7") (deps (list (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_repr") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "serde_yaml") (req "^0.8") (default-features #t) (kind 0)))) (hash "0fd1g8gp09f5w840g7lvlx91iqg4kbn55zjfyshrjgmy47dzznnq")))

