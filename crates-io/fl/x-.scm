(define-module (crates-io fl x-) #:use-module (crates-io))

(define-public crate-flx-rs-0.1 (crate (name "flx-rs") (vers "0.1.0") (hash "111372lwbh21phzdnv43a29ydhva5zf9k2dbifw52lx2mbmfd1dv")))

(define-public crate-flx-rs-0.1 (crate (name "flx-rs") (vers "0.1.1") (hash "0nzzi1qp0pvkp5j1h301p84v13ivdxwnglv4v6whmbs0bf843187")))

(define-public crate-flx-rs-0.1 (crate (name "flx-rs") (vers "0.1.2") (hash "12gjsm3dwsn0d717hkm0ijla11sb2yqdpa4qkb6q2rylwczzz2yx")))

(define-public crate-flx-rs-0.1 (crate (name "flx-rs") (vers "0.1.3") (hash "1qp1jmzpvqc9k6anjp5ik9dcs1is42iskl77mlz6d1lxhig0cfx3")))

(define-public crate-flx-rs-0.1 (crate (name "flx-rs") (vers "0.1.4") (hash "1sl8jjlxrm9kp9f4az3gs93v9afymhzry48b7qijy9kz3cz4abb3")))

(define-public crate-flx-rs-0.1 (crate (name "flx-rs") (vers "0.1.5") (hash "0b9hnpm33nj1d6h3m96x452c4m64yria6rrzccbsx7frs3sl60wv")))

(define-public crate-flx-rs-0.1 (crate (name "flx-rs") (vers "0.1.6") (hash "1ixwz7xfx1ii35kawnizg9hy19q09gx7wxayfg961vcs37la381y")))

(define-public crate-flx-rs-0.2 (crate (name "flx-rs") (vers "0.2.0") (hash "13jgmi1a44wrkw0dwkba0f2whyz0l1fr9wlfg8wc0cf90r0281i1")))

