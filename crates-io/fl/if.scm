(define-module (crates-io fl if) #:use-module (crates-io))

(define-public crate-flif-0.0.0 (crate (name "flif") (vers "0.0.0") (hash "0a5sh5c6w9y38sqab0681mcljjwg1d38q3smhwjbapv2r1d3plsl") (yanked #t)))

(define-public crate-flif-0.1 (crate (name "flif") (vers "0.1.0") (deps (list (crate-dep (name "inflate") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.1.40") (default-features #t) (kind 0)) (crate-dep (name "png") (req "^0.11.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.1.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "structopt-derive") (req "^0.1.0") (optional #t) (default-features #t) (kind 0)))) (hash "02d9w0qdqd1gmhw9qq7c2fxl64rcdz2y60crb4dsqmagrpg5rq7y") (features (quote (("binary" "structopt" "structopt-derive" "png"))))))

(define-public crate-flif-0.2 (crate (name "flif") (vers "0.2.0") (deps (list (crate-dep (name "fnv") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "inflate") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "png") (req "^0.12") (default-features #t) (kind 2)))) (hash "02dd7391z0nirnacmvv6f378rs4pfgcldjqs2wk6lklgkpfzlxwr")))

(define-public crate-flif-0.3 (crate (name "flif") (vers "0.3.0") (deps (list (crate-dep (name "fnv") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "inflate") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "png") (req "^0.12") (default-features #t) (kind 2)))) (hash "0g4rmh97yqbzkkbd5igsrfn3m551272riyq19n544vvkj9jrq289")))

(define-public crate-flif-0.4 (crate (name "flif") (vers "0.4.0") (deps (list (crate-dep (name "inflate") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "png") (req "^0.13") (default-features #t) (kind 2)))) (hash "15jmwsxpzjc9a24p17211sji48ns8fdgbzsh87vapq6kvhi3zvnv") (yanked #t)))

(define-public crate-flif-0.4 (crate (name "flif") (vers "0.4.1") (deps (list (crate-dep (name "inflate") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "png") (req "^0.13") (default-features #t) (kind 2)))) (hash "034lzfamlkkxllmz436l1vv41jl5k9ls1hh9ykfrmx13n3ng8c9v")))

