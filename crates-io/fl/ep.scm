(define-module (crates-io fl ep) #:use-module (crates-io))

(define-public crate-flep-0.1 (crate (name "flep") (vers "0.1.0") (deps (list (crate-dep (name "flep_protocol") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "188a9xy5xjacd82zvhnsp67xi2xswdgi66f3x5971bm7pxm6fn1j")))

(define-public crate-flep-0.1 (crate (name "flep") (vers "0.1.1") (deps (list (crate-dep (name "flep_protocol") (req "^0.1.1") (default-features #t) (kind 0)) (crate-dep (name "mio") (req "^0.6.0") (default-features #t) (kind 0)) (crate-dep (name "uuid") (req "^0.3") (features (quote ("v4"))) (default-features #t) (kind 0)))) (hash "00rp90iwg84lvacn2sg899wf68c6p1gwfhr2l79rbafand87wncv")))

(define-public crate-flep-0.1 (crate (name "flep") (vers "0.1.2") (deps (list (crate-dep (name "flep_protocol") (req "^0.1.2") (default-features #t) (kind 0)) (crate-dep (name "mio") (req "^0.6") (default-features #t) (kind 0)) (crate-dep (name "uuid") (req "^0.5") (features (quote ("v4"))) (default-features #t) (kind 0)))) (hash "1p2wbfw9xacp6zks00hb88wf1vy1z3vbapl05d2shx0hxv8fpfzl")))

(define-public crate-flep-0.2 (crate (name "flep") (vers "0.2.0") (deps (list (crate-dep (name "error-chain") (req "^0.10") (default-features #t) (kind 0)) (crate-dep (name "flep_protocol") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "mio") (req "^0.6") (default-features #t) (kind 0)) (crate-dep (name "uuid") (req "^0.5") (features (quote ("v4"))) (default-features #t) (kind 0)))) (hash "1mkp4zxy5bqahfyxxgv1l5gcmazgk1nappfbwjb9c9331pjzvjdn")))

(define-public crate-flep_protocol-0.1 (crate (name "flep_protocol") (vers "0.1.0") (deps (list (crate-dep (name "byteorder") (req "^0.5") (default-features #t) (kind 0)) (crate-dep (name "itertools") (req "^0.5") (default-features #t) (kind 0)))) (hash "1bkg0qghlhpsw357633nriafvb142a3p5nk5v4mps76xxj0yz3id")))

(define-public crate-flep_protocol-0.1 (crate (name "flep_protocol") (vers "0.1.1") (deps (list (crate-dep (name "byteorder") (req "^0.5") (default-features #t) (kind 0)) (crate-dep (name "itertools") (req "^0.5") (default-features #t) (kind 0)) (crate-dep (name "rfc1700") (req "^0.1") (default-features #t) (kind 0)))) (hash "1rcl7xxhgc9n7xjd7xll1bfibyczzcqsf2sgspvd2z092gxlvdkb")))

(define-public crate-flep_protocol-0.1 (crate (name "flep_protocol") (vers "0.1.2") (deps (list (crate-dep (name "byteorder") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "itertools") (req "^0.6") (default-features #t) (kind 0)) (crate-dep (name "rfc1700") (req "^0.1") (default-features #t) (kind 0)))) (hash "13y65zf74l8hqy73iqn71j71wifb4lcdr3h4333i39v9fvg1f3kp")))

(define-public crate-flep_protocol-0.2 (crate (name "flep_protocol") (vers "0.2.0") (deps (list (crate-dep (name "byteorder") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "error-chain") (req "^0.10") (default-features #t) (kind 0)) (crate-dep (name "itertools") (req "^0.6") (default-features #t) (kind 0)) (crate-dep (name "rfc1700") (req "^0.1") (default-features #t) (kind 0)))) (hash "171fh2z7l2g51x90q3wrlid1z305ppgk7rb2yxgcnf6w5g0pv65f")))

