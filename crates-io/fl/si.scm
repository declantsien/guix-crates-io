(define-module (crates-io fl si) #:use-module (crates-io))

(define-public crate-flsignal-0.5 (crate (name "flsignal") (vers "0.5.0") (deps (list (crate-dep (name "env_logger") (req "^0.9") (default-features #t) (kind 0)) (crate-dep (name "flnet") (req "^0.5") (default-features #t) (kind 0)) (crate-dep (name "flnet-libc") (req "^0.5") (default-features #t) (kind 0)) (crate-dep (name "flutils") (req "^0.5") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "0n1smcx34pk2427vnasl8b4n2l2l7h1bgry06dam2ckmnnsibb43")))

