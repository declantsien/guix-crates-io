(define-module (crates-io fl dt) #:use-module (crates-io))

(define-public crate-fldtools-0.1 (crate (name "fldtools") (vers "0.1.1") (deps (list (crate-dep (name "byteorder") (req "^1.1.0") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^2.24.2") (default-features #t) (kind 0)) (crate-dep (name "itertools") (req "^0.6.1") (default-features #t) (kind 0)))) (hash "1jmvdzwg6cgz8lg16ygahxcj5q4zix6fq07cyg6bdm985cvifh1s")))

