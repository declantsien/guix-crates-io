(define-module (crates-io fl oa) #:use-module (crates-io))

(define-public crate-floader-0.1 (crate (name "floader") (vers "0.1.0") (deps (list (crate-dep (name "bootloader") (req "^0.11.4") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^4.4.8") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "ovmf-prebuilt") (req "^0.1.0-alpha.1") (default-features #t) (kind 0)))) (hash "1f39j6isn9dqzz42c7h49sdg8vwzd0knccnhrn620zrghjf9qixa") (yanked #t)))

(define-public crate-floader-0.1 (crate (name "floader") (vers "0.1.1") (deps (list (crate-dep (name "bootloader") (req "^0.11.4") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^4.4.8") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "ovmf-prebuilt") (req "^0.1.0-alpha.1") (default-features #t) (kind 0)))) (hash "0ihq2d22g5yj0hz2niyc1m1ja3bj3byc28mwshhq67cd5l4mcw6s") (yanked #t)))

(define-public crate-floaout-0.1 (crate (name "floaout") (vers "0.1.0") (hash "1g4dyqq5xbf6fcb2b08j4birkr2fi8z0il6x0m3hmq6mql6w4mv2")))

(define-public crate-floaout-0.2 (crate (name "floaout") (vers "0.2.0") (deps (list (crate-dep (name "mpl") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "mycrc") (req "^0.3") (default-features #t) (kind 0)))) (hash "15clkcsl2dhh0b3xlvaq4vyfvib3rhl23sznspyybzzbyglnnnw6")))

(define-public crate-floaout-0.2 (crate (name "floaout") (vers "0.2.1") (deps (list (crate-dep (name "mpl") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "mpl-macro") (req "^0.1.2") (default-features #t) (kind 0)) (crate-dep (name "mycrc") (req "^0.3") (default-features #t) (kind 0)))) (hash "0gdqg3j5c219wsfx8swd03ddh9wzk5y5mah02swni5lz2jc3v0ba")))

(define-public crate-floaout-0.2 (crate (name "floaout") (vers "0.2.2") (deps (list (crate-dep (name "mpl") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "mpl-macro") (req "^0.1.2") (default-features #t) (kind 0)) (crate-dep (name "mycrc") (req "^0.3") (default-features #t) (kind 0)))) (hash "0cilr82z3x7ivsxfyckb67bdvvzsf8idzlfbq6kf3sqdbbmvgfwv")))

(define-public crate-float-0.1 (crate (name "float") (vers "0.1.0") (hash "1437mnsy030f6jh5m96pgw3gkkadwyds87gkyw1fp2c9a31bn75y")))

(define-public crate-float-0.1 (crate (name "float") (vers "0.1.1") (hash "1plrf3qp7wy2a8apm6yn9n11bbf9ys5l6lx44pggbf122x0g9rsj")))

(define-public crate-float-0.1 (crate (name "float") (vers "0.1.2") (hash "0zxld6pm58clv04gxr4clq48vyvwqci7csqd7kfvkdv6h2d3qlq2")))

(define-public crate-float-cmp-0.0.1 (crate (name "float-cmp") (vers "0.0.1") (hash "11dl93l6gg458cwsddnnxkbgy479cy1xyw0h26096x2v4bpmfdck")))

(define-public crate-float-cmp-0.0.2 (crate (name "float-cmp") (vers "0.0.2") (hash "04nvhbg94y70qh7npjx39dk029y2axq4i0si1f80ya7amk7xiqkk")))

(define-public crate-float-cmp-0.0.3 (crate (name "float-cmp") (vers "0.0.3") (hash "1f8brqzljy71gdk5xfyvp999p11l6pjqahqb9w4kijph635fsx5q")))

(define-public crate-float-cmp-0.1 (crate (name "float-cmp") (vers "0.1.0") (deps (list (crate-dep (name "num") (req "*") (default-features #t) (kind 0)))) (hash "0njxrnyz3y8645m062vfwqa8myfnaayl20a9zvj61wabwrsd6zjy")))

(define-public crate-float-cmp-0.1 (crate (name "float-cmp") (vers "0.1.1") (hash "0k5z552xz3ii580f83kg1bdc4f07jfmjap1v00zg8vdin0blma51")))

(define-public crate-float-cmp-0.1 (crate (name "float-cmp") (vers "0.1.2") (hash "09i9s21c0075jk2rw92apaynwhy1gy8q2y7bw82v7nqhqnrz6cfw")))

(define-public crate-float-cmp-0.1 (crate (name "float-cmp") (vers "0.1.3") (hash "0d3wiwik2x5gmwg772yxx2nwkjbz3zikk36wi1s2p6kcfhsllnxh")))

(define-public crate-float-cmp-0.2 (crate (name "float-cmp") (vers "0.2.1") (hash "0c0sjjmn59a7gw8623asg7051wk4409kd1gcxdwhnrb1vr3alpim")))

(define-public crate-float-cmp-0.2 (crate (name "float-cmp") (vers "0.2.2") (deps (list (crate-dep (name "num") (req "^0.1") (default-features #t) (kind 0)))) (hash "0wlxngrxnbgpm44nj46l4p8r013fgrq83jlnzmi91mn05p33pk44")))

(define-public crate-float-cmp-0.2 (crate (name "float-cmp") (vers "0.2.3") (deps (list (crate-dep (name "num-traits") (req "^0.1") (kind 0)))) (hash "04f4crd9fmi808cxzwpmisj46lk9bc9xjrhnn9p389q5r0x9sp5f")))

(define-public crate-float-cmp-0.2 (crate (name "float-cmp") (vers "0.2.4") (deps (list (crate-dep (name "num") (req "^0.1") (kind 0)))) (hash "05pmdbw3xw56qs2zsw2z87rysq1qb2isj5nmhakg8p82mwj6pf2s")))

(define-public crate-float-cmp-0.2 (crate (name "float-cmp") (vers "0.2.5") (deps (list (crate-dep (name "num") (req "^0.1") (kind 0)))) (hash "1w1il86960zsnk6cwbs2c3s2ncnjjswk7pfa717z963dxx1pl488")))

(define-public crate-float-cmp-0.3 (crate (name "float-cmp") (vers "0.3.0") (deps (list (crate-dep (name "num") (req "^0.1") (kind 0)))) (hash "1c0hmj46xma5aysz0qb49padhc26aw875whx6q6rglsj5dqpds1b")))

(define-public crate-float-cmp-0.4 (crate (name "float-cmp") (vers "0.4.0") (deps (list (crate-dep (name "num-traits") (req "^0.2") (kind 0)))) (hash "0036jb8ry4h83n319jb20b5yvyfyq8mx8dkxnyjm22nq8fl8yjhk")))

(define-public crate-float-cmp-0.4 (crate (name "float-cmp") (vers "0.4.1") (deps (list (crate-dep (name "num-traits") (req "^0.2") (optional #t) (kind 0)))) (hash "0qmlz3hnal801gdjgf21xm4z65d6m004ccaq883klym2yq2ryiw1") (features (quote (("default" "num-traits")))) (yanked #t)))

(define-public crate-float-cmp-0.5 (crate (name "float-cmp") (vers "0.5.0") (deps (list (crate-dep (name "num-traits") (req "^0.2") (optional #t) (kind 0)))) (hash "0370hkwwlnvd2kqz650yhgy3qyv2pnf43n9kbwi50d0qjiikmbhp") (features (quote (("default" "num-traits"))))))

(define-public crate-float-cmp-0.5 (crate (name "float-cmp") (vers "0.5.1") (deps (list (crate-dep (name "num-traits") (req "^0.2") (optional #t) (kind 0)))) (hash "0kan284ij0nn6nc93524y1r5bsmpch95fiwz6p6hlfpqiqmrki7h") (features (quote (("default" "num-traits"))))))

(define-public crate-float-cmp-0.5 (crate (name "float-cmp") (vers "0.5.2") (deps (list (crate-dep (name "num-traits") (req "^0.2") (optional #t) (kind 0)))) (hash "0pjvnhmph9ij96c45nfqhb2352bfdv6lz7yhva2810d297jfxx3y") (features (quote (("default" "num-traits"))))))

(define-public crate-float-cmp-0.5 (crate (name "float-cmp") (vers "0.5.3") (deps (list (crate-dep (name "num-traits") (req "^0.2") (optional #t) (kind 0)))) (hash "03hmx3n48hjm0x1ig84n1j87kzp75lzr6cj1sgi6a6pykgn4n8km") (features (quote (("default" "num-traits"))))))

(define-public crate-float-cmp-0.6 (crate (name "float-cmp") (vers "0.6.0") (deps (list (crate-dep (name "num-traits") (req "^0.2") (optional #t) (kind 0)))) (hash "0zb1lv3ga18vsnpjjdg87yazbzvmfwwllj3aiid8660rp3qw8qns") (features (quote (("default" "num-traits"))))))

(define-public crate-float-cmp-0.7 (crate (name "float-cmp") (vers "0.7.0") (deps (list (crate-dep (name "num-traits") (req "^0.2") (optional #t) (kind 0)))) (hash "0s4fmjzpdg7459i7xsx88jrzd31ssa6zrhg5wx3v0m6j0gx1p6j9") (features (quote (("default" "num-traits"))))))

(define-public crate-float-cmp-0.8 (crate (name "float-cmp") (vers "0.8.0") (deps (list (crate-dep (name "num-traits") (req "^0.2") (optional #t) (kind 0)))) (hash "1i56hnzjn5pmrcm47fwkmfxiihk7wz5vvcgpb0kpfhzkqi57y9p1") (features (quote (("std") ("ratio" "num-traits") ("default" "ratio"))))))

(define-public crate-float-cmp-0.9 (crate (name "float-cmp") (vers "0.9.0") (deps (list (crate-dep (name "num-traits") (req "^0.2.1") (optional #t) (kind 0)))) (hash "1i799ksbq7fj9rm9m82g1yqgm6xi3jnrmylddmqknmksajylpplq") (features (quote (("std") ("ratio" "num-traits") ("default" "ratio"))))))

(define-public crate-float-dbg-0.1 (crate (name "float-dbg") (vers "0.1.0") (hash "0zvp8lxapghfwdcqx94lj59jlz08bgj6rii7x53z3adcf20x7s8x")))

(define-public crate-float-dbg-0.1 (crate (name "float-dbg") (vers "0.1.1") (hash "1w2fr9yd42wgl6hzk0ijb96sdcpa96n8w02m8gsf6hc28cx5kljz")))

(define-public crate-float-dbg-0.2 (crate (name "float-dbg") (vers "0.2.0") (hash "1vmingxnyjwlvy4sq704wmjj6zmz5qcif0djgvp3vlz1dqbn5rpy")))

(define-public crate-float-dbg-0.3 (crate (name "float-dbg") (vers "0.3.0") (deps (list (crate-dep (name "az") (req "^1.2.0") (default-features #t) (kind 0)))) (hash "0sqmdr2vgbdp75vjzpar8cchgk3vcfjmny8bba37qpfbphgw7p9l")))

(define-public crate-float-dbg-0.3 (crate (name "float-dbg") (vers "0.3.1") (deps (list (crate-dep (name "az") (req "^1.2.0") (default-features #t) (kind 0)))) (hash "0acgv654zx576abh3kf3b1x9vzvvnz06w0xkrlcyhxkfjsxlrv53")))

(define-public crate-float-dbg-0.3 (crate (name "float-dbg") (vers "0.3.2") (deps (list (crate-dep (name "az") (req "^1.2.0") (default-features #t) (kind 0)))) (hash "1w4abkvw9sl2rr9a9w0gv1yswzkm24dnmpfbg5x3i6309as7h2si")))

(define-public crate-float-dbg-0.4 (crate (name "float-dbg") (vers "0.4.0-alpha.1") (deps (list (crate-dep (name "az") (req "^1.2.0") (default-features #t) (kind 0)))) (hash "0ph5abwsy5r4w1h427p20161cnskv61b38r63z61nv3wr0cipmwv")))

(define-public crate-float-derive-0.1 (crate (name "float-derive") (vers "0.1.0") (deps (list (crate-dep (name "float-derive-macros") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2.15") (default-features #t) (kind 0)) (crate-dep (name "ordered-float") (req "^3.3.0") (default-features #t) (kind 0)))) (hash "1rflpznvqsgq0ij21zlidx3irrvx277vlfm95f0x37kdd1nx0vkx")))

(define-public crate-float-derive-macros-0.1 (crate (name "float-derive-macros") (vers "0.1.0") (deps (list (crate-dep (name "proc-macro2") (req "^1.0.47") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.21") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.103") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "0r5sswvc7kzppgyac45028ilb0q2bx8829cqmvlkv9zwygxi81px")))

(define-public crate-float-format-0.1 (crate (name "float-format") (vers "0.1.0") (deps (list (crate-dep (name "bitvec") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0") (default-features #t) (kind 0)))) (hash "040pivcc9ljcljd8kncpv8hnnqakcaw182f34h729cy31anr6ilm")))

(define-public crate-float-format-0.1 (crate (name "float-format") (vers "0.1.1") (deps (list (crate-dep (name "bitvec") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "fraction") (req "^0.10") (features (quote ("with-bigint"))) (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0") (default-features #t) (kind 0)))) (hash "1xjmj14iv88ial5mp8kxhvragw44yk7ypichvxinr31xk1gfx1mx")))

(define-public crate-float-format-0.1 (crate (name "float-format") (vers "0.1.2") (deps (list (crate-dep (name "bitvec") (req "^1.0.0") (default-features #t) (kind 0)) (crate-dep (name "derivative") (req "^2.2.0") (default-features #t) (kind 0)) (crate-dep (name "fraction") (req "^0.10.0") (features (quote ("with-bigint"))) (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2.15") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.31") (default-features #t) (kind 0)))) (hash "0j602mz2rv78yf7d42xvyvdxn8bggm2rk0vrr1ibpmil1sl579fj")))

(define-public crate-float-format-0.1 (crate (name "float-format") (vers "0.1.3") (deps (list (crate-dep (name "bitvec") (req "^1.0.0") (default-features #t) (kind 0)) (crate-dep (name "derivative") (req "^2.2.0") (default-features #t) (kind 0)) (crate-dep (name "fraction") (req "^0.10.0") (features (quote ("with-bigint"))) (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2.15") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.31") (default-features #t) (kind 0)))) (hash "16qsvf0hxicw3kn9l03851sdyfmkw1zdprnxj268lrshp5c0war3")))

(define-public crate-float-hash-of-0.1 (crate (name "float-hash-of") (vers "0.1.0") (deps (list (crate-dep (name "hash-of") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "0ijggw0lga7ndzmpzdcrdrdcrnx6mm3i45d6c6khq1my84rkvndv")))

(define-public crate-float-lerp-0.1 (crate (name "float-lerp") (vers "0.1.0") (deps (list (crate-dep (name "num-traits") (req "^0.2") (default-features #t) (kind 0)))) (hash "01q15yijv05pijzzbgv8i1na7zkbrnkjmik14hpp541wgip3lh8q")))

(define-public crate-float-lerp-0.1 (crate (name "float-lerp") (vers "0.1.1") (deps (list (crate-dep (name "num-traits") (req "^0.2") (default-features #t) (kind 0)))) (hash "0kmjay6iaagjkh8mya18i4r4wbv6xxc2yw31d6z8qww0zv5cg6bl")))

(define-public crate-float-lerp-0.2 (crate (name "float-lerp") (vers "0.2.0") (deps (list (crate-dep (name "num-traits") (req "^0.2") (default-features #t) (kind 0)))) (hash "1z0vl0fkcicjrd54xw3bx98k5gbsb8k7mqfiix8q7wz3vnvsq4s2")))

(define-public crate-float-ord-0.1 (crate (name "float-ord") (vers "0.1.0") (deps (list (crate-dep (name "pdqsort") (req "^1.0.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.3") (default-features #t) (kind 2)))) (hash "1m6xycq0k73zps3b0a4f1z71ax092c17bxrdr1lw3jzkhh6b4062")))

(define-public crate-float-ord-0.1 (crate (name "float-ord") (vers "0.1.1") (deps (list (crate-dep (name "pdqsort") (req "^1.0.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.3") (default-features #t) (kind 2)))) (hash "142sh8hj7jdaq5mg69pl3v3p4wfn9mnh2da52aizz42ip0szkqs7")))

(define-public crate-float-ord-0.1 (crate (name "float-ord") (vers "0.1.2") (deps (list (crate-dep (name "pdqsort") (req "^1.0.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.3") (default-features #t) (kind 2)))) (hash "1k1myq4npsadpvlq5za3lsjirs0p4z63ydc7v8ww5zd9cma6ri7q")))

(define-public crate-float-ord-0.2 (crate (name "float-ord") (vers "0.2.0") (deps (list (crate-dep (name "rand") (req "^0.3") (default-features #t) (kind 2)))) (hash "0kin50365sr3spnbscq43lksymybi99ai9rkqdw90m6vixhlibbv")))

(define-public crate-float-ord-0.3 (crate (name "float-ord") (vers "0.3.0") (deps (list (crate-dep (name "rand") (req "^0.8") (default-features #t) (kind 2)))) (hash "05vm3vh0xp8kr5kzp1fihk5wjz60sirs89n1cajfcpr6vc0n5zy1")))

(define-public crate-float-ord-0.3 (crate (name "float-ord") (vers "0.3.1") (deps (list (crate-dep (name "rand") (req "^0.8") (default-features #t) (kind 2)))) (hash "0ixmh86lh4vkn8d6q4b306n2hnbvljpnmngnb8f3zpdjs1q031k4")))

(define-public crate-float-ord-0.3 (crate (name "float-ord") (vers "0.3.2") (deps (list (crate-dep (name "rand") (req "^0.8") (default-features #t) (kind 2)))) (hash "0gbvx6dzz8xvj38bj02cajpqd9p5syxjx9jyqpj8414amr4izs4c")))

(define-public crate-float-pretty-print-0.1 (crate (name "float-pretty-print") (vers "0.1.0") (hash "109y42xy2qi1ld8zhi6x9m7fnlm6fq55krgpwrbwzp216plyllaj")))

(define-public crate-float-pretty-print-0.1 (crate (name "float-pretty-print") (vers "0.1.1") (hash "0cd2kiv01aprspl7jrzpan6y3r9pfn117lqri8048qkm1b2wvqfa")))

(define-public crate-float-quickly-0.1 (crate (name "float-quickly") (vers "0.1.0") (deps (list (crate-dep (name "criterion") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "libm") (req "^0.2.2") (default-features #t) (kind 2)) (crate-dep (name "paste") (req "^1.0.7") (default-features #t) (kind 2)))) (hash "0a9nk29sswi54nf147yyk90x2dg5bxqbfxq9kpz18rfaan8kf8wq") (features (quote (("external_doc"))))))

(define-public crate-float-traits-0.0.2 (crate (name "float-traits") (vers "0.0.2") (deps (list (crate-dep (name "num-integer") (req "^0.1.32") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.1.40") (default-features #t) (kind 0)))) (hash "0mvp1vzlapcblchp2hxxrgq5xh4qf0ga6jz6wlnq5fkc9pia4p28")))

(define-public crate-float-traits-0.0.3 (crate (name "float-traits") (vers "0.0.3") (deps (list (crate-dep (name "num-integer") (req "^0.1.32") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.1.40") (default-features #t) (kind 0)))) (hash "1wp7mq706n4q3kblld00ibc1rcpmn6xj7qj5bcq5y02k3r6r7lcl")))

(define-public crate-float01-0.0.0 (crate (name "float01") (vers "0.0.0") (deps (list (crate-dep (name "num-traits") (req "^0.2.15") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.158") (optional #t) (default-features #t) (kind 0)))) (hash "0zzr7f67y9jsn6wkgsl2x7ld94jmj9m33cjsr0glvjzhl2ma873q")))

(define-public crate-float01-0.1 (crate (name "float01") (vers "0.1.0") (deps (list (crate-dep (name "num-traits") (req "^0.2.15") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.158") (optional #t) (default-features #t) (kind 0)))) (hash "1rc8sqxqv91vb9ydjb85m1skd289mkadbdz6j0j0srp01v5bwfm9")))

(define-public crate-float_approx_math-0.1 (crate (name "float_approx_math") (vers "0.1.0") (deps (list (crate-dep (name "array_trait") (req "^0.5.15") (default-features #t) (kind 0)) (crate-dep (name "linspace") (req "^0.1.0") (default-features #t) (kind 2)) (crate-dep (name "moddef") (req "^0.2.2") (default-features #t) (kind 0)) (crate-dep (name "plotters") (req "^0.3.5") (default-features #t) (kind 2)) (crate-dep (name "polynomial_ops") (req "^0.1.2") (default-features #t) (kind 0)))) (hash "0s7dvla3i33q1j0z8vmxkvq1pvljldnc3l72xpfwfbiihsapvwxz")))

(define-public crate-float_approx_math-0.1 (crate (name "float_approx_math") (vers "0.1.1") (deps (list (crate-dep (name "array_trait") (req "^0.6.11") (default-features #t) (kind 0)) (crate-dep (name "linspace") (req "^0.1.0") (default-features #t) (kind 2)) (crate-dep (name "moddef") (req "^0.2.2") (default-features #t) (kind 0)) (crate-dep (name "plotters") (req "^0.3.5") (default-features #t) (kind 2)) (crate-dep (name "polynomial_ops") (req "^0.1.2") (default-features #t) (kind 0)))) (hash "10vr6q1ih80rng2gabf3wqxxydm1ak11hl8jx5gly9hm7w76grbh")))

(define-public crate-float_approx_math-0.1 (crate (name "float_approx_math") (vers "0.1.2") (deps (list (crate-dep (name "array_trait") (req "^0.6.11") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "array_trait") (req "^0.6.11") (default-features #t) (kind 2)) (crate-dep (name "linspace") (req "^0.1.0") (default-features #t) (kind 2)) (crate-dep (name "moddef") (req "^0.2.2") (default-features #t) (kind 0)) (crate-dep (name "plotters") (req "^0.3.5") (default-features #t) (kind 2)) (crate-dep (name "polynomial_ops") (req "^0.1.2") (optional #t) (default-features #t) (kind 0)))) (hash "196265kr2w3dcrkw9in6pjpyba2zh8n3n5nxmr39laf663zp5vkm") (features (quote (("sqrt") ("no_array_trait" "sqrt") ("default" "sin" "sqrt")))) (v 2) (features2 (quote (("sin" "dep:polynomial_ops" "dep:array_trait"))))))

(define-public crate-float_approx_math-0.1 (crate (name "float_approx_math") (vers "0.1.3") (deps (list (crate-dep (name "array_trait") (req "^0.6.15") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "moddef") (req "^0.2.2") (default-features #t) (kind 0)) (crate-dep (name "polynomial_ops") (req "^0.1.3") (optional #t) (default-features #t) (kind 0)))) (hash "1ccfwyqildhf9j3wrihm9a5678l3jbfaa7130x3fcmsgg58ljby4") (features (quote (("sqrt") ("no_array_trait" "sqrt") ("default" "sin" "sqrt")))) (v 2) (features2 (quote (("sin" "dep:polynomial_ops" "dep:array_trait"))))))

(define-public crate-float_approx_math-0.1 (crate (name "float_approx_math") (vers "0.1.4") (deps (list (crate-dep (name "array_trait") (req "^0.6.16") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "moddef") (req "^0.2.2") (default-features #t) (kind 0)) (crate-dep (name "polynomial_ops") (req "^0.1.4") (optional #t) (default-features #t) (kind 0)))) (hash "1ynyid4d81pl19l5rgi3a9xpy2zr428kfyrsnmm21vrhy7jgx8m7") (features (quote (("sqrt") ("no_array_trait" "sqrt") ("default" "sin" "sqrt")))) (v 2) (features2 (quote (("sin" "dep:polynomial_ops" "dep:array_trait"))))))

(define-public crate-float_approx_math-0.1 (crate (name "float_approx_math") (vers "0.1.5") (deps (list (crate-dep (name "array_trait") (req "^0.6.17") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "array_trait") (req "^0.6.17") (default-features #t) (kind 2)) (crate-dep (name "linspace") (req "^0.1.0") (default-features #t) (kind 2)) (crate-dep (name "moddef") (req "^0.2.2") (default-features #t) (kind 0)) (crate-dep (name "plotters") (req "^0.3.5") (default-features #t) (kind 2)) (crate-dep (name "polynomial_ops") (req "^0.1.5") (optional #t) (default-features #t) (kind 0)))) (hash "0yp7g87p9q84s5ivlm1fwnkvmnca07wjs4zqdmkdkn931cws7z52") (features (quote (("sqrt") ("no_array_trait" "sqrt") ("default" "sin" "sqrt")))) (v 2) (features2 (quote (("sin" "dep:polynomial_ops" "dep:array_trait"))))))

(define-public crate-float_approx_math-0.1 (crate (name "float_approx_math") (vers "0.1.6") (deps (list (crate-dep (name "array_trait") (req "^0.6.17") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "array_trait") (req "^0.6.17") (default-features #t) (kind 2)) (crate-dep (name "linspace") (req "^0.1.0") (default-features #t) (kind 2)) (crate-dep (name "moddef") (req "^0.2.2") (default-features #t) (kind 0)) (crate-dep (name "plotters") (req "^0.3.5") (default-features #t) (kind 2)) (crate-dep (name "polynomial_ops") (req "^0.1.5") (optional #t) (default-features #t) (kind 0)))) (hash "1lr4fz54pknv17y60yycmlbnilb7lr4zsj0wrck9pm6qdkhqp821") (features (quote (("sqrt") ("no_array_trait" "sqrt") ("default" "sin" "sqrt")))) (v 2) (features2 (quote (("sin" "dep:polynomial_ops" "dep:array_trait"))))))

(define-public crate-float_approx_math-0.1 (crate (name "float_approx_math") (vers "0.1.7") (deps (list (crate-dep (name "array_trait") (req "^0.6.18") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "array_trait") (req "^0.6.18") (default-features #t) (kind 2)) (crate-dep (name "fn_zip") (req "^0.1.2") (default-features #t) (kind 0)) (crate-dep (name "linspace") (req "^0.1.0") (default-features #t) (kind 2)) (crate-dep (name "moddef") (req "^0.2.2") (default-features #t) (kind 0)) (crate-dep (name "plotters") (req "^0.3.5") (default-features #t) (kind 2)) (crate-dep (name "polynomial_ops") (req "^0.1.6") (optional #t) (default-features #t) (kind 0)))) (hash "0k2bcncgjc07xvw2q4bspxgil4fy16m1byrqbl14ihh93pzw4fbf") (features (quote (("sqrt") ("no_array_trait" "sqrt") ("default" "sin" "sqrt")))) (v 2) (features2 (quote (("sin" "dep:polynomial_ops" "dep:array_trait"))))))

(define-public crate-float_approx_math-0.1 (crate (name "float_approx_math") (vers "0.1.8") (deps (list (crate-dep (name "array__ops") (req "^0.1.1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "array__ops") (req "^0.1.1") (default-features #t) (kind 2)) (crate-dep (name "fn_zip") (req "^0.1.2") (default-features #t) (kind 0)) (crate-dep (name "linspace") (req "^0.1.1") (default-features #t) (kind 2)) (crate-dep (name "moddef") (req "^0.2.2") (default-features #t) (kind 0)) (crate-dep (name "plotters") (req "^0.3.5") (default-features #t) (kind 2)) (crate-dep (name "polynomial_ops") (req "^0.1.7") (optional #t) (default-features #t) (kind 0)))) (hash "1zmgdvallpy6qj3jcirz4p26ca7klcg37rrgivn7b5r2vpm9jmc3") (features (quote (("sqrt") ("no_array_trait" "sqrt") ("default" "sin" "sqrt")))) (v 2) (features2 (quote (("sin" "dep:polynomial_ops" "dep:array__ops"))))))

(define-public crate-float_approx_math-0.1 (crate (name "float_approx_math") (vers "0.1.9") (deps (list (crate-dep (name "array__ops") (req "^0.1.2") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "array__ops") (req "^0.1.1") (default-features #t) (kind 2)) (crate-dep (name "fn_zip") (req "^0.1.2") (default-features #t) (kind 0)) (crate-dep (name "linspace") (req "^0.1.1") (default-features #t) (kind 2)) (crate-dep (name "moddef") (req "^0.2.2") (default-features #t) (kind 0)) (crate-dep (name "plotters") (req "^0.3.5") (default-features #t) (kind 2)) (crate-dep (name "polynomial_ops") (req "^0.1.8") (optional #t) (default-features #t) (kind 0)))) (hash "0rnnsll23jg7pd0bnqc9yc9k84892alirrq2mvl8hkw6gzngmdjl") (features (quote (("sqrt") ("no_array_trait" "sqrt") ("default" "sin" "sqrt")))) (v 2) (features2 (quote (("sin" "dep:polynomial_ops" "dep:array__ops"))))))

(define-public crate-float_duration-0.1 (crate (name "float_duration") (vers "0.1.0") (deps (list (crate-dep (name "approx") (req "^0.1.1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "chrono") (req "^0.3.1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "time") (req "^0.1.37") (optional #t) (default-features #t) (kind 0)))) (hash "0168av7ar72va6in50yv1kivx9grwrjs8lk9liwxh0rikha6vs1z") (features (quote (("default" "chrono" "time" "approx") ("all" "chrono" "time" "approx"))))))

(define-public crate-float_duration-0.2 (crate (name "float_duration") (vers "0.2.0") (deps (list (crate-dep (name "approx") (req "^0.1.1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "chrono") (req "^0.3.1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde_test") (req "^1.0") (default-features #t) (kind 2)) (crate-dep (name "time") (req "^0.1.37") (optional #t) (default-features #t) (kind 0)))) (hash "0dj9fpp1p8n6rw827kbdylij2d9bcf0xmsxxgl4481935ddczisf") (features (quote (("default" "chrono" "time" "approx" "serde") ("all" "chrono" "time" "approx" "serde"))))))

(define-public crate-float_duration-0.3 (crate (name "float_duration") (vers "0.3.0") (deps (list (crate-dep (name "approx") (req "^0.1.1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "chrono") (req "^0.3.1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde_test") (req "^1.0") (default-features #t) (kind 2)) (crate-dep (name "time") (req "^0.1.37") (optional #t) (default-features #t) (kind 0)))) (hash "1km7kncmby03c583yxzw3qls1qyn3h25a4vlh0fijp790hp0lhnb") (features (quote (("default" "chrono" "time" "approx" "serde") ("all" "chrono" "time" "approx" "serde"))))))

(define-public crate-float_duration-0.3 (crate (name "float_duration") (vers "0.3.1") (deps (list (crate-dep (name "approx") (req "^0.1.1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "chrono") (req "^0.3.1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde_test") (req "^1.0") (default-features #t) (kind 2)) (crate-dep (name "time") (req "^0.1.37") (optional #t) (default-features #t) (kind 0)))) (hash "1by10yk6v7kfrz2fafkfc065s7np2l1b3fgmydlkgrwa5qc4pmhp") (features (quote (("nightly") ("default" "chrono" "time" "approx" "serde") ("all" "chrono" "time" "approx" "serde"))))))

(define-public crate-float_duration-0.3 (crate (name "float_duration") (vers "0.3.2") (deps (list (crate-dep (name "approx") (req "^0.1.1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "chrono") (req "^0.3.1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde_test") (req "^1.0") (default-features #t) (kind 2)) (crate-dep (name "time") (req "^0.1.37") (optional #t) (default-features #t) (kind 0)))) (hash "1hgkqf4lvavzdvcf9ydnlkwyk0vd7wakjjr4jmb2cdw7hxyx9kir") (features (quote (("nightly") ("default" "chrono" "time" "approx" "serde") ("all" "chrono" "time" "approx" "serde"))))))

(define-public crate-float_duration-0.3 (crate (name "float_duration") (vers "0.3.3") (deps (list (crate-dep (name "approx") (req "^0.1.1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "chrono") (req "^0.4.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde_test") (req "^1.0") (default-features #t) (kind 2)) (crate-dep (name "time") (req "^0.1.37") (optional #t) (default-features #t) (kind 0)))) (hash "03j82y9484az37vhm3vayg7s1rw378hfd3yq4w97z3xgh8qqxvaa") (features (quote (("nightly") ("default" "chrono" "time" "approx" "serde") ("all" "chrono" "time" "approx" "serde"))))))

(define-public crate-float_eq-0.1 (crate (name "float_eq") (vers "0.1.0") (hash "01d3g0qaqlrls2vmiim0rqrbnwirg27z3d6a38ql6wxy86f0g3x3")))

(define-public crate-float_eq-0.1 (crate (name "float_eq") (vers "0.1.1") (hash "00dkb3mijnvr37mbzxwz0p31m9dqkwfhyhwkfdl5ifmnnxy41s5f")))

(define-public crate-float_eq-0.1 (crate (name "float_eq") (vers "0.1.2") (hash "09cn5s3gxng6hinxjrzw5a3m6bfmz8hqz961zix9lpq03n6nyp63") (features (quote (("std") ("default" "std"))))))

(define-public crate-float_eq-0.1 (crate (name "float_eq") (vers "0.1.3") (hash "1pbgag1mkhkss9agbjvkhla15q6vmm8wyqk6i3amlq4z0kydkjas") (features (quote (("std") ("default" "std"))))))

(define-public crate-float_eq-0.2 (crate (name "float_eq") (vers "0.2.0") (deps (list (crate-dep (name "num-complex") (req "^0.2") (optional #t) (default-features #t) (kind 0)))) (hash "0rxxxkm1vcpk71xlrfbnglnvrf76yabpph7kqa8z4yhhq5rscx56") (features (quote (("std") ("num" "num-complex") ("default" "std"))))))

(define-public crate-float_eq-0.3 (crate (name "float_eq") (vers "0.3.0") (deps (list (crate-dep (name "num-complex") (req "^0.2") (optional #t) (default-features #t) (kind 0)))) (hash "1s8kqcncj2f6j1zr97s9163axkq3r374s6gb4ja069rmy3fb2zaw") (features (quote (("std") ("num" "num-complex") ("default" "std"))))))

(define-public crate-float_eq-0.3 (crate (name "float_eq") (vers "0.3.1") (deps (list (crate-dep (name "num-complex") (req "^0.2") (optional #t) (default-features #t) (kind 0)))) (hash "1pnj6s17nma6l1ikkij24syq1b4c0f03s3djjfbb5frmvk57irf7") (features (quote (("std") ("num" "num-complex") ("default" "std"))))))

(define-public crate-float_eq-0.4 (crate (name "float_eq") (vers "0.4.0") (deps (list (crate-dep (name "float_eq_derive") (req "=0.4.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "num-complex") (req "^0.2") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "trybuild") (req "^1") (default-features #t) (kind 2)))) (hash "16q4rr33kjc7ws01c32qlkzd1in7jafs91gvrl0y6l9bqv5faana") (features (quote (("std") ("num" "num-complex") ("derive" "float_eq_derive") ("default" "std"))))))

(define-public crate-float_eq-0.4 (crate (name "float_eq") (vers "0.4.1") (deps (list (crate-dep (name "float_eq_derive") (req "=0.4.1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "num-complex") (req "^0.2") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "trybuild") (req "^1") (default-features #t) (kind 2)))) (hash "1icywlrkkw4c85f96699h1nz0z257k92n5lykfplvx27bki932k3") (features (quote (("std") ("num" "num-complex") ("derive" "float_eq_derive") ("default" "std"))))))

(define-public crate-float_eq-0.5 (crate (name "float_eq") (vers "0.5.0") (deps (list (crate-dep (name "float_eq_derive") (req "=0.5.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "num-complex") (req "^0.3") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "trybuild") (req "^1") (default-features #t) (kind 2)))) (hash "1wssi0wkaswbac65m7919scgzx265w4lqswiz520bp1w5y8bc8zv") (features (quote (("std") ("num" "num-complex") ("derive" "float_eq_derive") ("default" "std"))))))

(define-public crate-float_eq-0.6 (crate (name "float_eq") (vers "0.6.0") (deps (list (crate-dep (name "float_eq_derive") (req "=0.6.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "num-complex") (req "^0.4") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "trybuild") (req "^1") (default-features #t) (kind 2)))) (hash "1fab45xkzhb2pppbnyi20x90nh4fmpyig17a0arsnm7ghdi0b3sc") (features (quote (("std") ("num" "num-complex") ("derive" "float_eq_derive") ("default" "std"))))))

(define-public crate-float_eq-0.6 (crate (name "float_eq") (vers "0.6.1") (deps (list (crate-dep (name "float_eq_derive") (req "=0.6.1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "num-complex") (req "^0.4") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "trybuild") (req "^1") (default-features #t) (kind 2)))) (hash "0biga4dqd8q2zrg4rc75qk3yz3y04xgxwpkkyli0kbzcixr4lcxa") (features (quote (("std") ("num" "num-complex") ("derive" "float_eq_derive") ("default" "std"))))))

(define-public crate-float_eq-0.7 (crate (name "float_eq") (vers "0.7.0") (deps (list (crate-dep (name "float_eq_derive") (req "=0.7.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "num-complex") (req "^0.4") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "trybuild") (req "^1") (default-features #t) (kind 2)))) (hash "0g82c7djwz0dlyg8ywy3b3rygbdwcbgllmrwwql7i6jgx6ck9mf1") (features (quote (("std") ("num" "num-complex") ("derive" "float_eq_derive") ("default" "std"))))))

(define-public crate-float_eq-1 (crate (name "float_eq") (vers "1.0.0") (deps (list (crate-dep (name "float_eq_derive") (req "=1.0.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "num-complex") (req "^0.4") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "trybuild") (req "^1") (default-features #t) (kind 2)))) (hash "0n6cabjig66jqjcycgrsqfbcis7vrjdqfa3x93b03djsqdln96xm") (features (quote (("std") ("num" "num-complex") ("derive" "float_eq_derive") ("default" "std"))))))

(define-public crate-float_eq-1 (crate (name "float_eq") (vers "1.0.1") (deps (list (crate-dep (name "float_eq_derive") (req "=1.0.1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "num-complex") (req "^0.4") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "trybuild") (req "^1") (default-features #t) (kind 2)))) (hash "0lxqxkvdy5zh3qsksavpcazg57cbyjy9p54m16x13bfq8lqhxa18") (features (quote (("std") ("num" "num-complex") ("derive" "float_eq_derive") ("default" "std"))))))

(define-public crate-float_eq_derive-0.4 (crate (name "float_eq_derive") (vers "0.4.0") (deps (list (crate-dep (name "proc-macro2") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1") (default-features #t) (kind 0)))) (hash "05xnapw7lwv0pfcldcxprr8lsq5pcfyh64xnr389k0d5k9zl8anb")))

(define-public crate-float_eq_derive-0.4 (crate (name "float_eq_derive") (vers "0.4.1") (deps (list (crate-dep (name "proc-macro2") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "1mvdpmp74fcf3jkrvbvcva135q70whzpprj5vhwx9jkpzkfb3015")))

(define-public crate-float_eq_derive-0.5 (crate (name "float_eq_derive") (vers "0.5.0") (deps (list (crate-dep (name "proc-macro2") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "0yjz17ba3gbv6s92mwr2664hdd83ha7snq6zkfwmcv5v64yl5v72")))

(define-public crate-float_eq_derive-0.6 (crate (name "float_eq_derive") (vers "0.6.0") (deps (list (crate-dep (name "proc-macro2") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "1g3cjq3l1rljxiijxb9ys32ghzsxfqfwlrn12qmxcddllj553bqn")))

(define-public crate-float_eq_derive-0.6 (crate (name "float_eq_derive") (vers "0.6.1") (deps (list (crate-dep (name "proc-macro2") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "03na88zbdmk7yic1s1xggrvfbz51vpmcp58wc7g5v1vxqwgrvc1i")))

(define-public crate-float_eq_derive-0.7 (crate (name "float_eq_derive") (vers "0.7.0") (deps (list (crate-dep (name "proc-macro2") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1") (default-features #t) (kind 0)))) (hash "1dn4db2zl65l4w46bkapxnzx383ka8h23656xjdipi8x3w8i4xjv")))

(define-public crate-float_eq_derive-1 (crate (name "float_eq_derive") (vers "1.0.0") (deps (list (crate-dep (name "proc-macro2") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1") (default-features #t) (kind 0)))) (hash "1ffpf0vn6kmrr232yqggcgk1fxwwxr9zjm6bwvcgawhgpzidf4rb")))

(define-public crate-float_eq_derive-1 (crate (name "float_eq_derive") (vers "1.0.1") (deps (list (crate-dep (name "proc-macro2") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1") (default-features #t) (kind 0)))) (hash "0mkg635lmr0nwjlxz6f5k8g60hrd800i061hrlmpvhdkl96d74j2")))

(define-public crate-float_extras-0.1 (crate (name "float_extras") (vers "0.1.0") (deps (list (crate-dep (name "libc") (req "^0.2.22") (default-features #t) (kind 0)))) (hash "15q6vy9wvjw26bypk01szrmaxd9slfk0zrf4mqf0q8654fan5v5p")))

(define-public crate-float_extras-0.1 (crate (name "float_extras") (vers "0.1.1") (deps (list (crate-dep (name "libc") (req "^0.2.22") (default-features #t) (kind 0)))) (hash "14wdlk28ylx50wddq9asnjmz3qx8p5dx3yzsw56xvvfxyg4ncxqw")))

(define-public crate-float_extras-0.1 (crate (name "float_extras") (vers "0.1.2") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "0p2gwgp7q7py3jnp30xvsqfc0sjkm696m66x5xza2gh60q66bcw5")))

(define-public crate-float_extras-0.1 (crate (name "float_extras") (vers "0.1.3") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "13y4i5nya3wh9f3pnbg7ymb18mxyjh4r72ljmwxqxmlgby0nxv1v")))

(define-public crate-float_extras-0.1 (crate (name "float_extras") (vers "0.1.4") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "1pplzdnvwqr7x88k9pp2rh814b19m61hhmbwqy5bdsbiwxxh5la0")))

(define-public crate-float_extras-0.1 (crate (name "float_extras") (vers "0.1.5") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "0n8bbmhpmr9r8zvdyl45frywn62khgz3anwcjl4rxbhw0bm7788a")))

(define-public crate-float_extras-0.1 (crate (name "float_extras") (vers "0.1.6") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "0ymb731vf7qdvw20spxavy1a8k8f9fb6v8ziamck38lyckw70axj")))

(define-public crate-float_fast_print-0.1 (crate (name "float_fast_print") (vers "0.1.0") (hash "1hbfvq6d62dgaqa1y8f2pliya3n975r341dld7d24dilqqb7qbb4") (yanked #t)))

(define-public crate-float_fast_print-0.1 (crate (name "float_fast_print") (vers "0.1.1") (hash "14x3ks2jfzf18pcic52gkr8d3fn3ypxma7ka3qgvf5k3lfggv600") (yanked #t)))

(define-public crate-float_fast_print-0.1 (crate (name "float_fast_print") (vers "0.1.2") (deps (list (crate-dep (name "rand") (req "^0.5.4") (default-features #t) (kind 2)))) (hash "00dd0cp7iq0bjnyqfrh2bba0g4w1drm10l1h8pr1qcy739g1vjd5")))

(define-public crate-float_math-0.0.1 (crate (name "float_math") (vers "0.0.1") (hash "0312vlda1ghcgi84rfq47z1w1ww6wnrzk9zvsqmvnpzlf4mifabs") (features (quote (("portable_simd") ("core_intrinsics"))))))

(define-public crate-float_math-0.0.2 (crate (name "float_math") (vers "0.0.2") (deps (list (crate-dep (name "bytemuck") (req "^1.7.3") (features (quote ("nightly_portable_simd"))) (default-features #t) (kind 0)) (crate-dep (name "libm") (req "^0.2") (default-features #t) (kind 2)))) (hash "10q7ib546q2zql2d14w4g54dkhg0ym07zjcbc5105nxdsgl1ys6g") (features (quote (("portable_simd") ("core_intrinsics"))))))

(define-public crate-float_math-0.0.3 (crate (name "float_math") (vers "0.0.3") (deps (list (crate-dep (name "bytemuck") (req "^1.7.3") (features (quote ("nightly_portable_simd"))) (default-features #t) (kind 0)) (crate-dep (name "libm") (req "^0.2") (default-features #t) (kind 2)))) (hash "0k1bf8k2p8xyb6mxvpxkxzbc6v295ssr9pi86a93jy8lzhxidv2l") (features (quote (("portable_simd") ("core_intrinsics"))))))

(define-public crate-float_next_after-0.1 (crate (name "float_next_after") (vers "0.1.0") (deps (list (crate-dep (name "num-traits") (req "^0.2.11") (default-features #t) (kind 0)))) (hash "0byk7hl1sygdf4c3hxxi5a0wvyfbpzy23dl948kiyzpq1017sny9")))

(define-public crate-float_next_after-0.1 (crate (name "float_next_after") (vers "0.1.1") (deps (list (crate-dep (name "num-traits") (req "^0.2.11") (default-features #t) (kind 0)))) (hash "15v6qfbl1w8d410sfi5gxrp8sfbr28p4vih3apry7bp5pmrmxz7y")))

(define-public crate-float_next_after-0.1 (crate (name "float_next_after") (vers "0.1.2") (deps (list (crate-dep (name "num-traits") (req "^0.2.11") (default-features #t) (kind 0)))) (hash "15qawjmb4kcpcwwgglqyzvf5vsh62rc4765qlwzcfxqzdvrgi8cj")))

(define-public crate-float_next_after-0.1 (crate (name "float_next_after") (vers "0.1.3") (deps (list (crate-dep (name "num-traits") (req "^0.2.11") (default-features #t) (kind 0)))) (hash "1jx66gr7qw6x45v8qkvzdg7s884gz05v5ab2cwxqaxwnxm1gbllv")))

(define-public crate-float_next_after-0.1 (crate (name "float_next_after") (vers "0.1.4") (deps (list (crate-dep (name "num-traits") (req "^0.2.11") (default-features #t) (kind 0)))) (hash "0w575zivrsxk5fjcmnjajfchgjqng3lajnrswbjkpww26yszv5c8")))

(define-public crate-float_next_after-0.1 (crate (name "float_next_after") (vers "0.1.5") (deps (list (crate-dep (name "num-traits") (req "^0.2.11") (default-features #t) (kind 0)))) (hash "0cnn60pslh0gilpy2jr7qpqk22a6lmsdz847988bg1krhg2i5ijg")))

(define-public crate-float_next_after-1 (crate (name "float_next_after") (vers "1.0.0") (deps (list (crate-dep (name "criterion") (req "^0.4.0") (features (quote ("html_reports"))) (default-features #t) (kind 2)) (crate-dep (name "fastrand") (req "^1.8.0") (default-features #t) (kind 2)))) (hash "1s7ikn69b394frihag05b0qcw9i9y04qanlhp5c8sjrw70bcrxwb")))

(define-public crate-float_to_int-0.1 (crate (name "float_to_int") (vers "0.1.0") (deps (list (crate-dep (name "cfg-if") (req "^1.0.0") (default-features #t) (kind 0)) (crate-dep (name "half") (req "^2.1.0") (features (quote ("num-traits"))) (optional #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2.15") (optional #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2.15") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 2)))) (hash "0bj8kcs5w68bffq0dmjp1r57jriihka98hpsj81zmvh8a1a60xk1") (features (quote (("default" "std")))) (v 2) (features2 (quote (("std" "half?/std" "num-traits?/std") ("nightly" "half?/use-intrinsics") ("half" "dep:half" "dep:num-traits"))))))

(define-public crate-floatconv-0.1 (crate (name "floatconv") (vers "0.1.0") (hash "17mx8afl91dqb8sp1273xpz0h0fsviijzxnmm929p7pf0gacw7br")))

(define-public crate-floatconv-0.2 (crate (name "floatconv") (vers "0.2.0") (hash "16f1266z3ykp8hjrcslags49sixphph88skykw04y4hm62ll4y5h")))

(define-public crate-floatconv-0.2 (crate (name "floatconv") (vers "0.2.1") (hash "1ahn5ddwb2f8mp5kdr5wvw5xzi79kbw5k3rzfxy670zmqds8a11r")))

(define-public crate-floatconv-0.2 (crate (name "floatconv") (vers "0.2.2") (hash "1mmxymafsb1nap255k7rjf098bzci06nv32667wvyszq6r2byzz5") (yanked #t)))

(define-public crate-floatconv-0.2 (crate (name "floatconv") (vers "0.2.3") (hash "0z4yrd6jh6f8ww85i2408h850pwhla38mirzsr1pnbxfywdgavng")))

(define-public crate-floatconv-0.2 (crate (name "floatconv") (vers "0.2.4") (hash "1f47gzshpwwwayxwkqrf8ml6crbqqyjssl26xa8fwiz2ls2fmbgi")))

(define-public crate-floatconv-0.2 (crate (name "floatconv") (vers "0.2.5") (hash "0p2gc7pbicfcs7cvsh31dwjh1pmvf3y04s4zpw1w48r2i191hham")))

(define-public crate-floatconv-0.2 (crate (name "floatconv") (vers "0.2.6") (hash "02l0bi652ykmf5grndxpzw0b25gh1bcydr46k3ipqn7dy26vv0wf")))

(define-public crate-floatconv-0.2 (crate (name "floatconv") (vers "0.2.7") (hash "0ph1bmmr0n15vd5yykxghgjywg5z75kn31bn2qr9bvjn3s7ylryj")))

(define-public crate-floatconv-0.2 (crate (name "floatconv") (vers "0.2.8") (hash "0mgfpr8n5swi302a8byl9hyqywqkghhfvx63wi70r9y0a5qdp3lz")))

(define-public crate-floatd-0.1 (crate (name "floatd") (vers "0.1.0") (deps (list (crate-dep (name "num-traits") (req "^0.2.18") (features (quote ("libm"))) (kind 0)))) (hash "08gk1sgbbrvh7h12xvlc3qjhkr5pvr45l1fknwvv64s4ml57wa1l") (features (quote (("std" "num-traits/std"))))))

(define-public crate-floaters-0.1 (crate (name "floaters") (vers "0.1.0") (deps (list (crate-dep (name "getrandom") (req "^0.2.10") (features (quote ("std"))) (default-features #t) (kind 0)))) (hash "1slwcjbbyq3sgykpzb13kn8i34w9yprgc7ndcv0bahpln674ydwi")))

(define-public crate-floating-cat-0.1 (crate (name "floating-cat") (vers "0.1.0") (deps (list (crate-dep (name "num") (req "^0.4.0") (default-features #t) (kind 0)))) (hash "1kgsylh2c2mvcfx438444bi3526gfi0xh2cr0f6k2jp4dqwaksml")))

(define-public crate-floating-distance-0.1 (crate (name "floating-distance") (vers "0.1.0") (deps (list (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 2)))) (hash "0hzn4mk1scbs4mrdph1gg29bzlf2d4s8adyalflkskfb21cyk9dx") (rust-version "1.60.0")))

(define-public crate-floating-distance-0.2 (crate (name "floating-distance") (vers "0.2.0") (deps (list (crate-dep (name "approx") (req "^0.5.1") (default-features #t) (kind 2)) (crate-dep (name "cfg-if") (req "^1.0.0") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2.16") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 2)))) (hash "0nb3ckai248lv8aw4lnj7hplb8jpdxm18gy9jmhgmbrqmyxpyh2c") (features (quote (("simd")))) (rust-version "1.60.0")))

(define-public crate-floating-distance-0.3 (crate (name "floating-distance") (vers "0.3.1") (deps (list (crate-dep (name "approx") (req "^0.5.1") (default-features #t) (kind 2)) (crate-dep (name "cfg-if") (req "^1.0.0") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2.16") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 2)))) (hash "0s17lxfvp7xz04bcd1qcpkf695jp7f0rzygpipg71wc4anpxbm82") (features (quote (("simd")))) (rust-version "1.60.0")))

(define-public crate-floating-duration-0.1 (crate (name "floating-duration") (vers "0.1.0") (hash "10spp9ri23pzijir7fi16dcgc6zliai2z8ldg3ym1l8r3mgzk8f3")))

(define-public crate-floating-duration-0.1 (crate (name "floating-duration") (vers "0.1.1") (hash "15r6ffq48ia00xf7hqqawil83np06nydksimj7igb1n39nb4dh9g")))

(define-public crate-floating-duration-0.1 (crate (name "floating-duration") (vers "0.1.2") (hash "08nz3yk587zn8pi4c8ywa1ffqv5cmyddqyg87c583kfvv5qhpimj")))

(define-public crate-floating-ui-0.0.1 (crate (name "floating-ui") (vers "0.0.1") (deps (list (crate-dep (name "js-sys") (req "^0.3.69") (default-features #t) (kind 0)) (crate-dep (name "paste") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "static_assertions") (req "^1.1.0") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "wasm-bindgen") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "web-sys") (req "^0.3") (features (quote ("HtmlDocument" "HtmlElement" "CssStyleDeclaration"))) (default-features #t) (kind 0)))) (hash "0xcj61mrkhqwplz23kv67mpab05ynnk3yw9rcx6kki87385rfpj4")))

(define-public crate-floating-ui-core-0.0.1 (crate (name "floating-ui-core") (vers "0.0.1") (deps (list (crate-dep (name "floating-ui-utils") (req "^0.0.1") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.197") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.114") (default-features #t) (kind 0)))) (hash "13czsjwci62xhgg2za06sfzpbapjnmcf72b3pqy0fqgs340s0592")))

(define-public crate-floating-ui-core-0.0.2 (crate (name "floating-ui-core") (vers "0.0.2") (deps (list (crate-dep (name "floating-ui-utils") (req "^0.0.2") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.197") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.114") (default-features #t) (kind 0)))) (hash "1ghgs7kvzgic9n96y6kxkvlfmpd16jf3fxnzml6qabwbpwbz3dlw")))

(define-public crate-floating-ui-core-0.0.3 (crate (name "floating-ui-core") (vers "0.0.3") (deps (list (crate-dep (name "dyn-clone") (req "^1.0.17") (default-features #t) (kind 0)) (crate-dep (name "floating-ui-utils") (req "^0.0.3") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.197") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.114") (default-features #t) (kind 0)))) (hash "047d0k39chiz4aqfkfsmvcl9n5g1zq9zc4xnfrzs3x0fvbl1n3f1")))

(define-public crate-floating-ui-core-0.0.4 (crate (name "floating-ui-core") (vers "0.0.4") (deps (list (crate-dep (name "dyn-clone") (req "^1.0.17") (default-features #t) (kind 0)) (crate-dep (name "floating-ui-utils") (req "^0.0.4") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.197") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.114") (default-features #t) (kind 0)))) (hash "00lkx28s03s9b2pasvzsaxpi05g29pql55md3kgbbyrv0w79fbqj")))

(define-public crate-floating-ui-core-0.0.5 (crate (name "floating-ui-core") (vers "0.0.5") (deps (list (crate-dep (name "dyn-clone") (req "^1.0.17") (default-features #t) (kind 0)) (crate-dep (name "floating-ui-utils") (req "^0.0.5") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.197") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.114") (default-features #t) (kind 0)))) (hash "0vjc00c4n7b5nlbw0a7n6ygswq8j9z96x058sdb0yhfgbw67nc3h")))

(define-public crate-floating-ui-core-0.0.6 (crate (name "floating-ui-core") (vers "0.0.6") (deps (list (crate-dep (name "dyn-clone") (req "^1.0.17") (default-features #t) (kind 0)) (crate-dep (name "floating-ui-utils") (req "^0.0.6") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.197") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.114") (default-features #t) (kind 0)))) (hash "1k9786k4gxysi5zi19dg75gmp2fihqqbr6jl9svv5irx4b3wz6zz")))

(define-public crate-floating-ui-core-0.0.7 (crate (name "floating-ui-core") (vers "0.0.7") (deps (list (crate-dep (name "dyn-clone") (req "^1.0.17") (default-features #t) (kind 0)) (crate-dep (name "floating-ui-utils") (req "^0.0.7") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.197") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.114") (default-features #t) (kind 0)))) (hash "1j42gi8v4isvs5lmgn6694qchd5b1hnf3xi61w21g3qgh02jd3gs")))

(define-public crate-floating-ui-core-0.0.8 (crate (name "floating-ui-core") (vers "0.0.8") (deps (list (crate-dep (name "dyn-clone") (req "^1.0.17") (default-features #t) (kind 0)) (crate-dep (name "floating-ui-utils") (req "^0.0.8") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.197") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.114") (default-features #t) (kind 0)))) (hash "0wi6imls6bl9s3jdydr7mwa636ys3y3xd5rvmzki8ch39yd22688")))

(define-public crate-floating-ui-dom-0.0.1 (crate (name "floating-ui-dom") (vers "0.0.1") (deps (list (crate-dep (name "floating-ui-core") (req "^0.0.1") (default-features #t) (kind 0)) (crate-dep (name "floating-ui-utils") (req "^0.0.1") (features (quote ("dom"))) (default-features #t) (kind 0)) (crate-dep (name "web-sys") (req "^0.3.69") (features (quote ("css" "CssStyleDeclaration" "Document" "DomRect" "DomRectList" "Element" "HtmlElement" "HtmlSlotElement" "Node" "ShadowRoot" "Window"))) (default-features #t) (kind 0)))) (hash "173c250gq0zp8pnd5vgmk30r613wzrmzwm4mhj133abghbdjqg9g")))

(define-public crate-floating-ui-dom-0.0.2 (crate (name "floating-ui-dom") (vers "0.0.2") (deps (list (crate-dep (name "floating-ui-core") (req "^0.0.2") (default-features #t) (kind 0)) (crate-dep (name "floating-ui-utils") (req "^0.0.2") (features (quote ("dom"))) (default-features #t) (kind 0)) (crate-dep (name "web-sys") (req "^0.3.69") (features (quote ("css" "CssStyleDeclaration" "Document" "DomRect" "DomRectList" "Element" "HtmlElement" "HtmlSlotElement" "Node" "ShadowRoot" "Window"))) (default-features #t) (kind 0)))) (hash "0xrx8xwapnwwkr9r4whd6lg2qbnw0m1rc4i3qgr38bwmcpn10mgr")))

(define-public crate-floating-ui-dom-0.0.3 (crate (name "floating-ui-dom") (vers "0.0.3") (deps (list (crate-dep (name "dyn-clone") (req "^1.0.17") (default-features #t) (kind 0)) (crate-dep (name "floating-ui-core") (req "^0.0.3") (default-features #t) (kind 0)) (crate-dep (name "floating-ui-utils") (req "^0.0.3") (features (quote ("dom"))) (default-features #t) (kind 0)) (crate-dep (name "web-sys") (req "^0.3.69") (features (quote ("css" "AddEventListenerOptions" "CssStyleDeclaration" "Document" "DomRect" "DomRectList" "Element" "Event" "EventTarget" "HtmlElement" "HtmlSlotElement" "Node" "ResizeObserver" "ResizeObserverEntry" "ShadowRoot" "Window"))) (default-features #t) (kind 0)))) (hash "1yvms7discyp8pj9sm29d2hqranr8ch1k8ivdx1z6ii7194wkmza")))

(define-public crate-floating-ui-dom-0.0.4 (crate (name "floating-ui-dom") (vers "0.0.4") (deps (list (crate-dep (name "dyn-clone") (req "^1.0.17") (default-features #t) (kind 0)) (crate-dep (name "floating-ui-core") (req "^0.0.4") (default-features #t) (kind 0)) (crate-dep (name "floating-ui-utils") (req "^0.0.4") (features (quote ("dom"))) (default-features #t) (kind 0)) (crate-dep (name "web-sys") (req "^0.3.69") (features (quote ("css" "AddEventListenerOptions" "CssStyleDeclaration" "Document" "DomRect" "DomRectList" "Element" "Event" "EventTarget" "HtmlElement" "HtmlSlotElement" "Node" "ResizeObserver" "ResizeObserverEntry" "ShadowRoot" "Window"))) (default-features #t) (kind 0)))) (hash "08kwz197p30dzg87mqrkwgphrfkargjfnvqf8ca818y82drf8dqd")))

(define-public crate-floating-ui-dom-0.0.5 (crate (name "floating-ui-dom") (vers "0.0.5") (deps (list (crate-dep (name "dyn-clone") (req "^1.0.17") (default-features #t) (kind 0)) (crate-dep (name "floating-ui-core") (req "^0.0.5") (default-features #t) (kind 0)) (crate-dep (name "floating-ui-utils") (req "^0.0.5") (features (quote ("dom"))) (default-features #t) (kind 0)) (crate-dep (name "web-sys") (req "^0.3.69") (features (quote ("css" "AddEventListenerOptions" "CssStyleDeclaration" "Document" "DomRect" "DomRectList" "Element" "Event" "EventTarget" "HtmlElement" "HtmlSlotElement" "IntersectionObserver" "IntersectionObserverEntry" "IntersectionObserverInit" "Node" "ResizeObserver" "ResizeObserverEntry" "Selection" "ShadowRoot" "Window"))) (default-features #t) (kind 0)))) (hash "1x08vfdwshg20qzq69aa3w0l0cw8ck4m18x0mlif1w78n1a89zcd")))

(define-public crate-floating-ui-dom-0.0.6 (crate (name "floating-ui-dom") (vers "0.0.6") (deps (list (crate-dep (name "dyn-clone") (req "^1.0.17") (default-features #t) (kind 0)) (crate-dep (name "floating-ui-core") (req "^0.0.6") (default-features #t) (kind 0)) (crate-dep (name "floating-ui-utils") (req "^0.0.6") (features (quote ("dom"))) (default-features #t) (kind 0)) (crate-dep (name "web-sys") (req "^0.3.69") (features (quote ("css" "AddEventListenerOptions" "CssStyleDeclaration" "Document" "DomRect" "DomRectList" "Element" "Event" "EventTarget" "HtmlElement" "HtmlSlotElement" "IntersectionObserver" "IntersectionObserverEntry" "IntersectionObserverInit" "Node" "ResizeObserver" "ResizeObserverEntry" "Selection" "ShadowRoot" "Window"))) (default-features #t) (kind 0)))) (hash "0lpf74qh8rq49jrbd4akjzxdd2zq3lkk8r6lmmi770b6nnvnc1rb")))

(define-public crate-floating-ui-dom-0.0.7 (crate (name "floating-ui-dom") (vers "0.0.7") (deps (list (crate-dep (name "dyn-clone") (req "^1.0.17") (default-features #t) (kind 0)) (crate-dep (name "floating-ui-core") (req "^0.0.7") (default-features #t) (kind 0)) (crate-dep (name "floating-ui-utils") (req "^0.0.7") (features (quote ("dom"))) (default-features #t) (kind 0)) (crate-dep (name "web-sys") (req "^0.3.69") (features (quote ("css" "AddEventListenerOptions" "CssStyleDeclaration" "Document" "DomRect" "DomRectList" "Element" "Event" "EventTarget" "HtmlElement" "HtmlSlotElement" "IntersectionObserver" "IntersectionObserverEntry" "IntersectionObserverInit" "Node" "ResizeObserver" "ResizeObserverEntry" "Selection" "ShadowRoot" "Window"))) (default-features #t) (kind 0)))) (hash "0x54i4skmbb32svmidvdy4r373x0sdxxw5yw8bkpavp1lqfh87b2")))

(define-public crate-floating-ui-dom-0.0.8 (crate (name "floating-ui-dom") (vers "0.0.8") (deps (list (crate-dep (name "dyn-clone") (req "^1.0.17") (default-features #t) (kind 0)) (crate-dep (name "floating-ui-core") (req "^0.0.8") (default-features #t) (kind 0)) (crate-dep (name "floating-ui-utils") (req "^0.0.8") (features (quote ("dom"))) (default-features #t) (kind 0)) (crate-dep (name "web-sys") (req "^0.3.69") (features (quote ("css" "AddEventListenerOptions" "CssStyleDeclaration" "Document" "DomRect" "DomRectList" "Element" "Event" "EventTarget" "HtmlElement" "HtmlSlotElement" "IntersectionObserver" "IntersectionObserverEntry" "IntersectionObserverInit" "Node" "ResizeObserver" "ResizeObserverEntry" "Selection" "ShadowRoot" "Window"))) (default-features #t) (kind 0)))) (hash "1ywbas0ckqjdkdyqkanhlw1xwgbynpi862d57j4yzqzy7g2chy66")))

(define-public crate-floating-ui-leptos-0.0.3 (crate (name "floating-ui-leptos") (vers "0.0.3") (deps (list (crate-dep (name "cfg-if") (req "^1.0.0") (default-features #t) (kind 0)) (crate-dep (name "floating-ui-dom") (req "^0.0.3") (default-features #t) (kind 0)) (crate-dep (name "leptos") (req "^0.6.9") (features (quote ("nightly"))) (default-features #t) (kind 0)) (crate-dep (name "wasm-bindgen-test") (req "^0.3.42") (default-features #t) (kind 2)) (crate-dep (name "web-sys") (req "^0.3.69") (features (quote ("css" "AddEventListenerOptions" "CssStyleDeclaration" "Document" "DomRect" "DomRectList" "Element" "Event" "EventTarget" "HtmlElement" "HtmlSlotElement" "Node" "ResizeObserver" "ResizeObserverEntry" "ShadowRoot" "Window"))) (default-features #t) (kind 0)))) (hash "0yfw55zmc62i654jdjgk58b88vkbbzf6s2qw1qdb9fmdxvqdhhpd")))

(define-public crate-floating-ui-leptos-0.0.4 (crate (name "floating-ui-leptos") (vers "0.0.4") (deps (list (crate-dep (name "cfg-if") (req "^1.0.0") (default-features #t) (kind 0)) (crate-dep (name "floating-ui-dom") (req "^0.0.4") (default-features #t) (kind 0)) (crate-dep (name "leptos") (req "^0.6.9") (features (quote ("nightly"))) (default-features #t) (kind 0)) (crate-dep (name "wasm-bindgen-test") (req "^0.3.42") (default-features #t) (kind 2)) (crate-dep (name "web-sys") (req "^0.3.69") (features (quote ("css" "AddEventListenerOptions" "CssStyleDeclaration" "Document" "DomRect" "DomRectList" "Element" "Event" "EventTarget" "HtmlElement" "HtmlSlotElement" "Node" "ResizeObserver" "ResizeObserverEntry" "ShadowRoot" "Window"))) (default-features #t) (kind 0)))) (hash "1rfz1kh97jr1280jsb3zmm179hgqgxh8ir28c3rfrlyp35l3vby8")))

(define-public crate-floating-ui-leptos-0.0.5 (crate (name "floating-ui-leptos") (vers "0.0.5") (deps (list (crate-dep (name "cfg-if") (req "^1.0.0") (default-features #t) (kind 0)) (crate-dep (name "floating-ui-dom") (req "^0.0.5") (default-features #t) (kind 0)) (crate-dep (name "leptos") (req "^0.6.9") (features (quote ("nightly"))) (default-features #t) (kind 0)) (crate-dep (name "paste") (req "^1.0.0") (default-features #t) (kind 0)) (crate-dep (name "wasm-bindgen-test") (req "^0.3.42") (default-features #t) (kind 2)) (crate-dep (name "web-sys") (req "^0.3.69") (features (quote ("css" "AddEventListenerOptions" "CssStyleDeclaration" "Document" "DomRect" "DomRectList" "Element" "Event" "EventTarget" "HtmlElement" "HtmlSlotElement" "IntersectionObserver" "IntersectionObserverEntry" "IntersectionObserverInit" "Node" "ResizeObserver" "ResizeObserverEntry" "Selection" "ShadowRoot" "Window"))) (default-features #t) (kind 0)))) (hash "0lvky7m7hyaq311qrnkpq550lx7563wr71cgfn4s762a4w4l6qfk")))

(define-public crate-floating-ui-leptos-0.0.6 (crate (name "floating-ui-leptos") (vers "0.0.6") (deps (list (crate-dep (name "cfg-if") (req "^1.0.0") (default-features #t) (kind 0)) (crate-dep (name "floating-ui-dom") (req "^0.0.6") (default-features #t) (kind 0)) (crate-dep (name "leptos") (req "^0.6.9") (features (quote ("nightly"))) (default-features #t) (kind 0)) (crate-dep (name "paste") (req "^1.0.0") (default-features #t) (kind 0)) (crate-dep (name "wasm-bindgen-test") (req "^0.3.42") (default-features #t) (kind 2)) (crate-dep (name "web-sys") (req "^0.3.69") (features (quote ("css" "AddEventListenerOptions" "CssStyleDeclaration" "Document" "DomRect" "DomRectList" "Element" "Event" "EventTarget" "HtmlElement" "HtmlSlotElement" "IntersectionObserver" "IntersectionObserverEntry" "IntersectionObserverInit" "Node" "ResizeObserver" "ResizeObserverEntry" "Selection" "ShadowRoot" "Window"))) (default-features #t) (kind 0)))) (hash "1gnvlxi22lcg1dm092lx5prmk1g0gl1jkrf9k80c11nsb531310q")))

(define-public crate-floating-ui-leptos-0.0.7 (crate (name "floating-ui-leptos") (vers "0.0.7") (deps (list (crate-dep (name "cfg-if") (req "^1.0.0") (default-features #t) (kind 0)) (crate-dep (name "floating-ui-dom") (req "^0.0.7") (default-features #t) (kind 0)) (crate-dep (name "leptos") (req "^0.6.9") (features (quote ("nightly"))) (default-features #t) (kind 0)) (crate-dep (name "paste") (req "^1.0.0") (default-features #t) (kind 0)) (crate-dep (name "wasm-bindgen-test") (req "^0.3.42") (default-features #t) (kind 2)) (crate-dep (name "web-sys") (req "^0.3.69") (features (quote ("css" "AddEventListenerOptions" "CssStyleDeclaration" "Document" "DomRect" "DomRectList" "Element" "Event" "EventTarget" "HtmlElement" "HtmlSlotElement" "IntersectionObserver" "IntersectionObserverEntry" "IntersectionObserverInit" "Node" "ResizeObserver" "ResizeObserverEntry" "Selection" "ShadowRoot" "Window"))) (default-features #t) (kind 0)))) (hash "0x0qp603llcjm0lrwi8lqddhkzkd6pmi6nl5dhrhn0q9fn93hf9k")))

(define-public crate-floating-ui-leptos-0.0.8 (crate (name "floating-ui-leptos") (vers "0.0.8") (deps (list (crate-dep (name "cfg-if") (req "^1.0.0") (default-features #t) (kind 0)) (crate-dep (name "floating-ui-dom") (req "^0.0.8") (default-features #t) (kind 0)) (crate-dep (name "leptos") (req "^0.6.9") (features (quote ("nightly"))) (default-features #t) (kind 0)) (crate-dep (name "paste") (req "^1.0.0") (default-features #t) (kind 0)) (crate-dep (name "wasm-bindgen-test") (req "^0.3.42") (default-features #t) (kind 2)) (crate-dep (name "web-sys") (req "^0.3.69") (features (quote ("css" "AddEventListenerOptions" "CssStyleDeclaration" "Document" "DomRect" "DomRectList" "Element" "Event" "EventTarget" "HtmlElement" "HtmlSlotElement" "IntersectionObserver" "IntersectionObserverEntry" "IntersectionObserverInit" "Node" "ResizeObserver" "ResizeObserverEntry" "Selection" "ShadowRoot" "Window"))) (default-features #t) (kind 0)))) (hash "1fywl7rl6l7sacvsjv9zjqdn7jl1q0wd88510s2hb3ffn1j23xyj")))

(define-public crate-floating-ui-utils-0.0.1 (crate (name "floating-ui-utils") (vers "0.0.1") (deps (list (crate-dep (name "serde") (req "^1.0.197") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "web-sys") (req "^0.3.69") (features (quote ("css" "CssStyleDeclaration" "Document" "DomRect" "DomRectList" "Element" "HtmlElement" "HtmlSlotElement" "Node" "ShadowRoot" "Window"))) (optional #t) (default-features #t) (kind 0)))) (hash "06k5y2f1n5pa1kqm67kam4k76pqahhbs8d9avrh50spi8gd3gknl") (features (quote (("default")))) (v 2) (features2 (quote (("dom" "dep:web-sys"))))))

(define-public crate-floating-ui-utils-0.0.2 (crate (name "floating-ui-utils") (vers "0.0.2") (deps (list (crate-dep (name "serde") (req "^1.0.197") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "web-sys") (req "^0.3.69") (features (quote ("css" "CssStyleDeclaration" "Document" "DomRect" "DomRectList" "Element" "HtmlElement" "HtmlSlotElement" "Node" "ShadowRoot" "Window"))) (optional #t) (default-features #t) (kind 0)))) (hash "0bv3d1z56xli25p8l2ix935z2kmwii9zxg6gjng290wf9fjjzsac") (features (quote (("default")))) (v 2) (features2 (quote (("dom" "dep:web-sys"))))))

(define-public crate-floating-ui-utils-0.0.3 (crate (name "floating-ui-utils") (vers "0.0.3") (deps (list (crate-dep (name "dyn-clone") (req "^1.0.17") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.197") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "web-sys") (req "^0.3.69") (features (quote ("css" "AddEventListenerOptions" "CssStyleDeclaration" "Document" "DomRect" "DomRectList" "Element" "Event" "EventTarget" "HtmlElement" "HtmlSlotElement" "Node" "ResizeObserver" "ResizeObserverEntry" "ShadowRoot" "Window"))) (optional #t) (default-features #t) (kind 0)))) (hash "1chpi553rzhqhhjiwbvfm3rbvgyah3raywm29sj00qcr4vr97qbq") (features (quote (("default")))) (v 2) (features2 (quote (("dom" "dep:web-sys"))))))

(define-public crate-floating-ui-utils-0.0.4 (crate (name "floating-ui-utils") (vers "0.0.4") (deps (list (crate-dep (name "dyn-clone") (req "^1.0.17") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.197") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "web-sys") (req "^0.3.69") (features (quote ("css" "AddEventListenerOptions" "CssStyleDeclaration" "Document" "DomRect" "DomRectList" "Element" "Event" "EventTarget" "HtmlElement" "HtmlSlotElement" "Node" "ResizeObserver" "ResizeObserverEntry" "ShadowRoot" "Window"))) (optional #t) (default-features #t) (kind 0)))) (hash "1myraxja7ah1qq4rjxnr2r2ssb3j3brlhnc0hv76clwxgjmgig5j") (features (quote (("default")))) (v 2) (features2 (quote (("dom" "dep:web-sys"))))))

(define-public crate-floating-ui-utils-0.0.5 (crate (name "floating-ui-utils") (vers "0.0.5") (deps (list (crate-dep (name "cfg-if") (req "^1.0.0") (default-features #t) (kind 0)) (crate-dep (name "dyn-clone") (req "^1.0.17") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.197") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "web-sys") (req "^0.3.69") (features (quote ("css" "AddEventListenerOptions" "CssStyleDeclaration" "Document" "DomRect" "DomRectList" "Element" "Event" "EventTarget" "HtmlElement" "HtmlSlotElement" "IntersectionObserver" "IntersectionObserverEntry" "IntersectionObserverInit" "Node" "ResizeObserver" "ResizeObserverEntry" "Selection" "ShadowRoot" "Window"))) (optional #t) (default-features #t) (kind 0)))) (hash "1v5y60n9yd5qa5yy807mvnrbjgd3wsxd989wzz96p1c4j80ji7z8") (features (quote (("default")))) (v 2) (features2 (quote (("dom" "dep:web-sys"))))))

(define-public crate-floating-ui-utils-0.0.6 (crate (name "floating-ui-utils") (vers "0.0.6") (deps (list (crate-dep (name "cfg-if") (req "^1.0.0") (default-features #t) (kind 0)) (crate-dep (name "dyn-clone") (req "^1.0.17") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.197") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "web-sys") (req "^0.3.69") (features (quote ("css" "AddEventListenerOptions" "CssStyleDeclaration" "Document" "DomRect" "DomRectList" "Element" "Event" "EventTarget" "HtmlElement" "HtmlSlotElement" "IntersectionObserver" "IntersectionObserverEntry" "IntersectionObserverInit" "Node" "ResizeObserver" "ResizeObserverEntry" "Selection" "ShadowRoot" "Window"))) (optional #t) (default-features #t) (kind 0)))) (hash "10az2k625zs0j85g55qxwhgw068bn61bqpvqhkpszq2rxrdi141l") (features (quote (("default")))) (v 2) (features2 (quote (("dom" "dep:web-sys"))))))

(define-public crate-floating-ui-utils-0.0.7 (crate (name "floating-ui-utils") (vers "0.0.7") (deps (list (crate-dep (name "cfg-if") (req "^1.0.0") (default-features #t) (kind 0)) (crate-dep (name "dyn-clone") (req "^1.0.17") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.197") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "web-sys") (req "^0.3.69") (features (quote ("css" "AddEventListenerOptions" "CssStyleDeclaration" "Document" "DomRect" "DomRectList" "Element" "Event" "EventTarget" "HtmlElement" "HtmlSlotElement" "IntersectionObserver" "IntersectionObserverEntry" "IntersectionObserverInit" "Node" "ResizeObserver" "ResizeObserverEntry" "Selection" "ShadowRoot" "Window"))) (optional #t) (default-features #t) (kind 0)))) (hash "18a85c55vk1ignqbmrw1niy6mlf8192ysqnhqb13an8w4396f6jd") (features (quote (("default")))) (v 2) (features2 (quote (("dom" "dep:web-sys"))))))

(define-public crate-floating-ui-utils-0.0.8 (crate (name "floating-ui-utils") (vers "0.0.8") (deps (list (crate-dep (name "cfg-if") (req "^1.0.0") (default-features #t) (kind 0)) (crate-dep (name "dyn-clone") (req "^1.0.17") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.197") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "web-sys") (req "^0.3.69") (features (quote ("css" "AddEventListenerOptions" "CssStyleDeclaration" "Document" "DomRect" "DomRectList" "Element" "Event" "EventTarget" "HtmlElement" "HtmlSlotElement" "IntersectionObserver" "IntersectionObserverEntry" "IntersectionObserverInit" "Node" "ResizeObserver" "ResizeObserverEntry" "Selection" "ShadowRoot" "Window"))) (optional #t) (default-features #t) (kind 0)))) (hash "0cix702ygwc15p3yw0sw26zkp7rr9ahl8h07gbq2j6nydkqm2hdh") (features (quote (("default")))) (v 2) (features2 (quote (("dom" "dep:web-sys"))))))

(define-public crate-floating_bar-0.1 (crate (name "floating_bar") (vers "0.1.0") (deps (list (crate-dep (name "gcd") (req "^1.2.0") (default-features #t) (kind 0)))) (hash "11vfb5ldiphzdrvn4gb3c92p45a7cmma8hkqknqm6wrr1m32d357")))

(define-public crate-floating_bar-0.1 (crate (name "floating_bar") (vers "0.1.1") (deps (list (crate-dep (name "gcd") (req "^1.2.0") (default-features #t) (kind 0)))) (hash "14njjd2c2nmilg415qg3fz567qds18vjsdm0j0zd2sn0ivyx834q")))

(define-public crate-floating_bar-0.1 (crate (name "floating_bar") (vers "0.1.2") (deps (list (crate-dep (name "gcd") (req "^1.2.0") (default-features #t) (kind 0)))) (hash "16h7bcgm0wj07anzmac50mwkc40fpdcbdnb3pk255c7dcjsz0vdm")))

(define-public crate-floating_bar-0.1 (crate (name "floating_bar") (vers "0.1.3") (deps (list (crate-dep (name "gcd") (req "^1.2.0") (default-features #t) (kind 0)) (crate-dep (name "integer-sqrt") (req "^0.1.2") (default-features #t) (kind 0)))) (hash "19csfzjsz3k194ys1g3ki827w9jdw9li0xqkglpfwjs5q6z6l1vk")))

(define-public crate-floating_bar-0.2 (crate (name "floating_bar") (vers "0.2.0") (deps (list (crate-dep (name "gcd") (req "^1.2.0") (default-features #t) (kind 0)) (crate-dep (name "integer-sqrt") (req "^0.1.2") (default-features #t) (kind 0)))) (hash "0vfisyfb670yi5972wqc1r8a3xxkhizc7q08sw5h4vyd1r45xpv1")))

(define-public crate-floating_bar-0.2 (crate (name "floating_bar") (vers "0.2.1") (deps (list (crate-dep (name "gcd") (req "^2.0.1") (default-features #t) (kind 0)) (crate-dep (name "integer-sqrt") (req "^0.1.3") (default-features #t) (kind 0)))) (hash "14y7870wi3z6ga48ckm4whnil9ji5q79rj37g93fv34xxh48ipmm")))

(define-public crate-floating_bar-0.2 (crate (name "floating_bar") (vers "0.2.2") (deps (list (crate-dep (name "gcd") (req "^2.0.1") (default-features #t) (kind 0)) (crate-dep (name "integer-sqrt") (req "^0.1.3") (default-features #t) (kind 0)))) (hash "0rbmflmphsda3sahsd6zdyi9blbdx5yma74b3c6iwavx7fbnnxn6") (features (quote (("bench")))) (yanked #t)))

(define-public crate-floating_bar-0.2 (crate (name "floating_bar") (vers "0.2.3") (deps (list (crate-dep (name "gcd") (req "^2.0.1") (default-features #t) (kind 0)) (crate-dep (name "integer-sqrt") (req "^0.1.3") (default-features #t) (kind 0)))) (hash "0dcxmv7y8plx1k0d9018k3pndipv1c6n776rypbz5nkzhvb4xc7h") (features (quote (("bench"))))))

(define-public crate-floating_bar-0.2 (crate (name "floating_bar") (vers "0.2.4") (deps (list (crate-dep (name "gcd") (req "^2.0.1") (default-features #t) (kind 0)) (crate-dep (name "integer-sqrt") (req "^0.1.3") (default-features #t) (kind 0)))) (hash "1fmzq0aaj11v1c53768w7mf1y37i7f2bk4lis7p1s6j10zmc6kbx") (features (quote (("denormals") ("bench"))))))

(define-public crate-floating_bar-0.3 (crate (name "floating_bar") (vers "0.3.0") (deps (list (crate-dep (name "criterion") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "gcd") (req "^2.0.1") (default-features #t) (kind 0)) (crate-dep (name "integer-sqrt") (req "^0.1.3") (default-features #t) (kind 0)))) (hash "09gq0jnzk5ww1fx571ir4v8s0kq5qshi22fiaayvkcab7m9gcr3b") (features (quote (("denormals") ("bench"))))))

(define-public crate-floating_bar-0.4 (crate (name "floating_bar") (vers "0.4.0") (deps (list (crate-dep (name "criterion") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "gcd") (req "^2.0.1") (default-features #t) (kind 0)))) (hash "14c5vry83lr321x0rz0sf91mnf2wd4203dl71mj8nkb02z2agmf9") (features (quote (("std") ("default" "std") ("bench"))))))

(define-public crate-floatpack-0.1 (crate (name "floatpack") (vers "0.1.0") (deps (list (crate-dep (name "bincode") (req "^1") (default-features #t) (kind 2)) (crate-dep (name "bitpacking") (req "^0.8.4") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.4") (default-features #t) (kind 2)) (crate-dep (name "rust_decimal") (req "^1.17.0") (default-features #t) (kind 0)) (crate-dep (name "rust_decimal_macros") (req "^1.17") (default-features #t) (kind 2)) (crate-dep (name "serde") (req "^1") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "034s0imlib4c0x9as4j3kc86s1qjvyk16ly9z1pd7r8vsi5mh3bl")))

(define-public crate-floaty-0.1 (crate (name "floaty") (vers "0.1.0") (deps (list (crate-dep (name "cast") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "0pkrrgpj63q5skvybqm3l1sdi7bafa1wypjmj3s87d4x9a65bh4y") (features (quote (("unstable"))))))

