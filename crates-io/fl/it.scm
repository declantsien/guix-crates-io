(define-module (crates-io fl it) #:use-module (crates-io))

(define-public crate-flit-0.1 (crate (name "flit") (vers "0.1.0") (deps (list (crate-dep (name "bitvec") (req "^0.10") (default-features #t) (kind 0)) (crate-dep (name "twox-hash") (req "^1.1") (default-features #t) (kind 0)))) (hash "1s1ic3pmaywv6cz1xadj7hc6nh4gl5z489cgyn986vcijvgvsbrh")))

(define-public crate-flit-0.1 (crate (name "flit") (vers "0.1.1") (deps (list (crate-dep (name "bitvec") (req "^0.10") (default-features #t) (kind 0)) (crate-dep (name "criterion") (req "^0.2") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.6") (default-features #t) (kind 2)) (crate-dep (name "twox-hash") (req "^1.1") (default-features #t) (kind 0)))) (hash "1jhz664c9yawwnk7hdrwvwb18x519cqkxja0jiv1d8d7bhkqnrwy")))

(define-public crate-flit-0.1 (crate (name "flit") (vers "0.1.2") (deps (list (crate-dep (name "bitvec") (req "^0.17.4") (default-features #t) (kind 0)) (crate-dep (name "criterion") (req "^0.3.2") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.7.3") (default-features #t) (kind 2)) (crate-dep (name "twox-hash") (req "^1.5.0") (default-features #t) (kind 0)))) (hash "06a6bbf7585152pmakr1cibqzggp2h4gzq1is3wjsqhpj7p5qf82")))

(define-public crate-flite-0.0.1 (crate (name "flite") (vers "0.0.1") (deps (list (crate-dep (name "flite-sys") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1") (default-features #t) (kind 0)))) (hash "1mjz5dcp9hw5q2gqv0x7b2lz5alzgkxbhriyvhj0jvb7h56az8hl")))

(define-public crate-flite-0.0.2 (crate (name "flite") (vers "0.0.2") (deps (list (crate-dep (name "flite-sys") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1") (default-features #t) (kind 0)))) (hash "1myb563xjg8f4gp9nkjdxxcw663zvc1953i6v3s444h6p8fj495k")))

(define-public crate-flite-0.0.3 (crate (name "flite") (vers "0.0.3") (deps (list (crate-dep (name "flite-sys") (req "^0.1") (default-features #t) (kind 0)))) (hash "1d8vdxvjf4kli9iq8189a28gd49v3dfhrhqa7ra7w4mj5hihd0gr")))

(define-public crate-flite-sys-0.1 (crate (name "flite-sys") (vers "0.1.0") (hash "1gnpbinik716ypgx5a17j3c0p8vmnsriqhcx8487nj6272dg11hp")))

(define-public crate-flite-sys-0.1 (crate (name "flite-sys") (vers "0.1.1") (hash "1f5nji6w6rwsbdfq54xg8s768g5vmjjirav009lkm7g7j2kqwp36")))

(define-public crate-flite-sys-0.1 (crate (name "flite-sys") (vers "0.1.2") (hash "12g9rmnvvkgk26w8f6f97799x0k5rvn0sbn322djpyzz4p0j6nmp")))

(define-public crate-fliters-0.1 (crate (name "fliters") (vers "0.1.0") (deps (list (crate-dep (name "cc") (req "^1.0.83") (default-features #t) (kind 1)) (crate-dep (name "cstr") (req "^0.2.11") (default-features #t) (kind 0)) (crate-dep (name "hound") (req "^3.5") (default-features #t) (kind 0)) (crate-dep (name "rodio") (req "^0.17.1") (features (quote ("wav"))) (kind 0)))) (hash "093q1nd9g4lkvm2qasqzlpvgk3qb0m66y447c5xpmjf603fw592k")))

(define-public crate-fliters-0.1 (crate (name "fliters") (vers "0.1.1") (deps (list (crate-dep (name "cc") (req "^1.0.83") (default-features #t) (kind 1)) (crate-dep (name "cstr") (req "^0.2.11") (default-features #t) (kind 0)) (crate-dep (name "hound") (req "^3.5") (default-features #t) (kind 0)) (crate-dep (name "rodio") (req "^0.17.1") (features (quote ("wav"))) (kind 0)))) (hash "1j6q52n65mlg569l9sxkw4g80yspl9bna8kljn3nv1xf9xjsiab7") (yanked #t)))

(define-public crate-fliters-0.1 (crate (name "fliters") (vers "0.1.2") (deps (list (crate-dep (name "cc") (req "^1.0.83") (default-features #t) (kind 1)) (crate-dep (name "cstr") (req "^0.2.11") (default-features #t) (kind 0)) (crate-dep (name "hound") (req "^3.5") (default-features #t) (kind 0)) (crate-dep (name "rodio") (req "^0.17.1") (features (quote ("wav"))) (kind 0)))) (hash "0jnazyh1vkmkkr9hmpvm410xjjgr96x1mh37lam0mbrkgfrw6xgk") (yanked #t)))

(define-public crate-fliters-0.1 (crate (name "fliters") (vers "0.1.3") (deps (list (crate-dep (name "cc") (req "^1.0.83") (default-features #t) (kind 1)) (crate-dep (name "cstr") (req "^0.2.11") (default-features #t) (kind 0)) (crate-dep (name "hound") (req "^3.5") (default-features #t) (kind 0)) (crate-dep (name "rodio") (req "^0.17.1") (features (quote ("wav"))) (kind 0)))) (hash "1nd1m8176xjiqbhs96syjr188chxfxl6q1qgmyznm1kiczpf61jf") (yanked #t)))

(define-public crate-fliters-0.1 (crate (name "fliters") (vers "0.1.4") (deps (list (crate-dep (name "cc") (req "^1.0.83") (default-features #t) (kind 1)) (crate-dep (name "cstr") (req "^0.2.11") (default-features #t) (kind 0)) (crate-dep (name "hound") (req "^3.5") (default-features #t) (kind 0)) (crate-dep (name "rodio") (req "^0.17.1") (features (quote ("wav"))) (kind 0)))) (hash "19ap1xms7qg810l5n5mm86v0mi296yid9n7c8pal55az80vb06zl") (yanked #t)))

(define-public crate-fliters-0.1 (crate (name "fliters") (vers "0.1.5") (deps (list (crate-dep (name "cc") (req "^1.0.83") (default-features #t) (kind 1)) (crate-dep (name "cstr") (req "^0.2.11") (default-features #t) (kind 0)) (crate-dep (name "hound") (req "^3.5") (default-features #t) (kind 0)) (crate-dep (name "rodio") (req "^0.17.1") (features (quote ("wav"))) (kind 0)))) (hash "07s0432v7mn3x2fc1lkbd5df86c9bmfivasnjdzz4wa80g6984ry") (yanked #t)))

(define-public crate-fliters-0.1 (crate (name "fliters") (vers "0.1.7") (deps (list (crate-dep (name "cc") (req "^1.0.83") (default-features #t) (kind 1)) (crate-dep (name "cstr") (req "^0.2.11") (default-features #t) (kind 0)) (crate-dep (name "hound") (req "^3.5") (default-features #t) (kind 0)) (crate-dep (name "rodio") (req "^0.17.1") (features (quote ("wav"))) (kind 0)))) (hash "1wh7iqfn854hbwihzxwz0c3xz932yyvf96amcniy9gzyfsqzn5v1")))

