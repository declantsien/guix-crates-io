(define-module (crates-io fl an) #:use-module (crates-io))

(define-public crate-flandres-0.1 (crate (name "flandres") (vers "0.1.0") (deps (list (crate-dep (name "fltk") (req "^0.9") (default-features #t) (kind 0)))) (hash "0bar04920cravj6has8qmcakbck8091accqj1zprrbppv7b2pf6v")))

(define-public crate-flandres-0.2 (crate (name "flandres") (vers "0.2.0") (deps (list (crate-dep (name "fltk") (req "^0.14") (default-features #t) (kind 0)))) (hash "193blfl7543azzrkl0wsgphdfq2b6v9am62in37lzz7gdsf7y6a0")))

(define-public crate-flandres-0.2 (crate (name "flandres") (vers "0.2.1") (deps (list (crate-dep (name "fltk") (req "^0.15") (default-features #t) (kind 0)))) (hash "09svbc964ndq95p29zxpkiqvfr2rc27650zxmvri31x4k0z7vxaj")))

(define-public crate-flandres-0.3 (crate (name "flandres") (vers "0.3.0") (deps (list (crate-dep (name "fltk") (req "^1") (default-features #t) (kind 0)))) (hash "0n61sn7n4g8vxa0by71342zjq9ymv7szz4fn8c6d701hmrhhddrd")))

(define-public crate-flandres-0.3 (crate (name "flandres") (vers "0.3.1") (deps (list (crate-dep (name "fltk") (req "^1") (default-features #t) (kind 0)))) (hash "0kga45l6a60cxaklnbg2zw0pfbscplnrzn97p98bmlv4ykr0g9mq")))

(define-public crate-flange-0.0.0 (crate (name "flange") (vers "0.0.0") (hash "127xgc2cfi4nqnzjbycj8388064xk40v1lf6lnp8j3mc8281yv55")))

(define-public crate-flange-flat-tree-0.1 (crate (name "flange-flat-tree") (vers "0.1.0") (hash "1ggbak8lp2jvibki6asrdhx6r4dpxqp3fb5ls2fg86sxqgskmj19")))

(define-public crate-flange-flat-tree-0.1 (crate (name "flange-flat-tree") (vers "0.1.1") (hash "1mn2mi57q55x4hc3bd49ips2wh9g49690m4mfg2r9jr3lvhpnnk0")))

(define-public crate-flange-flat-tree-0.1 (crate (name "flange-flat-tree") (vers "0.1.2") (hash "0s49w38hhrpmam51llk1spaxm1ns6sdl5ymymj65xvjjjd2l88pj")))

(define-public crate-flange-flat-tree-0.1 (crate (name "flange-flat-tree") (vers "0.1.3") (hash "1d619hsf7nxadj9xp0ic63vdkq5ghjn7jawldfz35pa8rz2icfia")))

(define-public crate-flange-flat-tree-0.2 (crate (name "flange-flat-tree") (vers "0.2.0") (hash "1w661igd51q10ssw8n41w6bwrkv4ph1m36fch20yljrmd920srar")))

(define-public crate-flange-flat-tree-0.2 (crate (name "flange-flat-tree") (vers "0.2.1") (hash "0pxmla291rpqhr1dcv6jl99aidapliwjpxpzvcdsvllf9ncr7i2v")))

(define-public crate-flange-flat-tree-0.2 (crate (name "flange-flat-tree") (vers "0.2.2") (deps (list (crate-dep (name "cargo-readme") (req "^3") (default-features #t) (kind 2)))) (hash "0jq191rwz6cybqkvpyx74rj7wiln4jq4lzd8gvaiwpypr4q7n3hh")))

(define-public crate-flanker-0.1 (crate (name "flanker") (vers "0.1.0") (hash "1c2p22avms53h0licjly2hdhp9bszg2blbsq0n14w46xc6nf92cr")))

(define-public crate-flanker-0.2 (crate (name "flanker") (vers "0.2.0") (hash "1qc7ks3i18hbjsy4pccdn2jfnw03hlfszyk98dgb4m9780yhvgci")))

(define-public crate-flanker-0.2 (crate (name "flanker") (vers "0.2.1") (hash "0wswkr2v97zsl7895ywpnl2sc509vzrpgfnw6bchrzrizhrcr9pl")))

(define-public crate-flanker-0.3 (crate (name "flanker") (vers "0.3.0") (hash "0fprkpyfm471qbcy31wz24his39gmj8hz5daaxh6r77si4dc7wcd")))

(define-public crate-flanker-0.4 (crate (name "flanker") (vers "0.4.0") (hash "0vc6qslprzyan7km532pk0akk587hgm7jy46j21zswz35i0m1cmc")))

(define-public crate-flanker-0.5 (crate (name "flanker") (vers "0.5.0") (hash "0bgpaiikfib3ljwk6rvk5b64yi8x5rrrmkzlzijgpr71jk7365lk")))

(define-public crate-flanker-assert-float-0.2 (crate (name "flanker-assert-float") (vers "0.2.0") (hash "14ai1ckadz81gdrshycgc5xcf1z1vkgkjslnn95q8v4naqpwagh6")))

(define-public crate-flanker-assert-float-0.2 (crate (name "flanker-assert-float") (vers "0.2.1") (hash "018i5j1xzbphgfc7d7i5py09syn8fnl5nmxl7j1q835k6fk13nvd")))

(define-public crate-flanker-assert-float-0.3 (crate (name "flanker-assert-float") (vers "0.3.0") (hash "1ilbjs3wky4gg1fvhvl2qjmaxwqsz0sryvy7d5z52ba1dksagl1k")))

(define-public crate-flanker-assert-float-0.4 (crate (name "flanker-assert-float") (vers "0.4.0") (hash "1d93ccakdkn943gzcr1fhbjl33ln0kvr9dvpdxk8ixc31qkp2c5b")))

(define-public crate-flanker-assert-float-0.5 (crate (name "flanker-assert-float") (vers "0.5.0") (hash "1fi8la0ynqfjn7sjn0qvd4cj35lnwpr3m5krnjb4jch0xpx3h5ab")))

(define-public crate-flanker-assert-str-0.3 (crate (name "flanker-assert-str") (vers "0.3.0") (hash "0cjxrv71c5gmrfsfx2g91gc6z52w2w2vs5hd2mvykla5avr72rg3")))

(define-public crate-flanker-assert-str-0.4 (crate (name "flanker-assert-str") (vers "0.4.0") (hash "1rh55q1ws1q6nsvqk7spc0bjpngw46cw7sk0x6ggnh42g083296h")))

(define-public crate-flanker-assert-str-0.5 (crate (name "flanker-assert-str") (vers "0.5.0") (hash "0l750rf3xf110yablaryf5vj4hjgaid4lrvm6j6j9ikirbazsws3")))

(define-public crate-flanker-temp-0.1 (crate (name "flanker-temp") (vers "0.1.0") (deps (list (crate-dep (name "tinyrand") (req "^0.5.0") (default-features #t) (kind 0)) (crate-dep (name "tinyrand-std") (req "^0.5.0") (default-features #t) (kind 0)))) (hash "1azdxy2vzf4n726lpz6r6bw0nijx7xjj2lv4sicf45rsfb33s7yy")))

(define-public crate-flanker-temp-0.2 (crate (name "flanker-temp") (vers "0.2.0") (deps (list (crate-dep (name "tinyrand") (req "^0.5.0") (default-features #t) (kind 0)) (crate-dep (name "tinyrand-std") (req "^0.5.0") (default-features #t) (kind 0)))) (hash "03srj6avan11dqz7fw24fqsaipchfvqypi8xp1vwy2v067s3ivxx")))

(define-public crate-flanker-temp-0.2 (crate (name "flanker-temp") (vers "0.2.1") (deps (list (crate-dep (name "tinyrand") (req "^0.5.0") (default-features #t) (kind 0)) (crate-dep (name "tinyrand-std") (req "^0.5.0") (default-features #t) (kind 0)))) (hash "1560i19v68l9j3cfmj3ssih4zlw6jdiaa10wscccj8cls01m6cnh")))

(define-public crate-flanker-temp-0.3 (crate (name "flanker-temp") (vers "0.3.0") (deps (list (crate-dep (name "tinyrand") (req "^0.5.0") (default-features #t) (kind 0)) (crate-dep (name "tinyrand-std") (req "^0.5.0") (default-features #t) (kind 0)))) (hash "0z87xqc9f9wic8g8n8dd7nfv327nl7y6br74ahcswp3m5f6qkyhn")))

(define-public crate-flanker-temp-0.4 (crate (name "flanker-temp") (vers "0.4.0") (deps (list (crate-dep (name "thread-id") (req "^4.0.0") (default-features #t) (kind 0)) (crate-dep (name "tinyrand") (req "^0.5.0") (default-features #t) (kind 0)) (crate-dep (name "tinyrand-std") (req "^0.5.0") (default-features #t) (kind 0)))) (hash "1g0bgihx0dkx5ilsd17m3nbldg28mkr28a1hqv9ic3brxbanxg9y")))

(define-public crate-flanker-temp-0.5 (crate (name "flanker-temp") (vers "0.5.0") (deps (list (crate-dep (name "thread-id") (req "^4.0.0") (default-features #t) (kind 0)) (crate-dep (name "tinyrand") (req "^0.5.0") (default-features #t) (kind 0)) (crate-dep (name "tinyrand-std") (req "^0.5.0") (default-features #t) (kind 0)))) (hash "03rbp6klw4ay1g26vnmy8dadlqnmjvzxqsdl3fw12x7j467qha0l")))

(define-public crate-flann-0.0.1 (crate (name "flann") (vers "0.0.1") (deps (list (crate-dep (name "flann-sys") (req "^0.0.1") (default-features #t) (kind 0)) (crate-dep (name "generic-array") (req "^0.9.0") (default-features #t) (kind 0)))) (hash "0x8smq8yq2bz94lhsrk1m2wsfxkfmckw5i3fk6yzx4p6zs4lzzzc")))

(define-public crate-flann-0.0.2 (crate (name "flann") (vers "0.0.2") (deps (list (crate-dep (name "flann-sys") (req "^0.0.1") (default-features #t) (kind 0)) (crate-dep (name "generic-array") (req "^0.9.0") (default-features #t) (kind 0)))) (hash "11pclib1mmavb482rr1jg2i04cp9spsq08wn56xyxj9mf7sl734p")))

(define-public crate-flann-0.0.4 (crate (name "flann") (vers "0.0.4") (deps (list (crate-dep (name "flann-sys") (req "^0.0.4") (default-features #t) (kind 0)) (crate-dep (name "generic-array") (req "^0.9.0") (default-features #t) (kind 0)))) (hash "1qvrb2ac9y6fbsinzfpb502181m0vcq8wxclx8aykvqw7kv7mvvr")))

(define-public crate-flann-0.0.5 (crate (name "flann") (vers "0.0.5") (deps (list (crate-dep (name "flann-sys") (req "^0.0.4") (default-features #t) (kind 0)) (crate-dep (name "generic-array") (req "^0.9.0") (default-features #t) (kind 0)) (crate-dep (name "itertools") (req "^0.7.6") (default-features #t) (kind 0)))) (hash "10141pm9if2vpglbk4a71h45mk4m4fpc4qsvrxnpdhnqbi461qkk")))

(define-public crate-flann-0.0.6 (crate (name "flann") (vers "0.0.6") (deps (list (crate-dep (name "flann-sys") (req "^0.0.4") (default-features #t) (kind 0)) (crate-dep (name "generic-array") (req "^0.9.0") (default-features #t) (kind 0)) (crate-dep (name "itertools") (req "^0.7.6") (default-features #t) (kind 0)))) (hash "15330kdm7q6a0whwzdgkshh4xmlva4g15rmdy94fr8d5d0px6z86")))

(define-public crate-flann-0.1 (crate (name "flann") (vers "0.1.0") (deps (list (crate-dep (name "assert_approx_eq") (req "^1.1.0") (default-features #t) (kind 2)) (crate-dep (name "failure") (req "^0.1.5") (default-features #t) (kind 0)) (crate-dep (name "flann-sys") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "generic-array") (req "^0.12.0") (default-features #t) (kind 0)) (crate-dep (name "itertools") (req "^0.8.0") (default-features #t) (kind 0)))) (hash "1wibb0fnfxliv323842lnm7rq57jfqihkr8v7fd08vd211cs3j2l")))

(define-public crate-flann-sys-0.0.1 (crate (name "flann-sys") (vers "0.0.1") (deps (list (crate-dep (name "bindgen") (req "^0.32.1") (default-features #t) (kind 1)) (crate-dep (name "cc") (req "^1.0.3") (default-features #t) (kind 1)))) (hash "13lfnzm83jks2zgfpn0kl62sn84m5sjcjb9kh6ax2bnl5pvgsaw2")))

(define-public crate-flann-sys-0.0.3 (crate (name "flann-sys") (vers "0.0.3") (deps (list (crate-dep (name "bindgen") (req "^0.32.1") (default-features #t) (kind 1)) (crate-dep (name "cc") (req "^1.0.3") (default-features #t) (kind 1)) (crate-dep (name "pkg-config") (req "^0.3.9") (default-features #t) (kind 1)))) (hash "07lj8765dl8n9klklfisr1h9lcrlanh9fvm401yfj927yimykf09")))

(define-public crate-flann-sys-0.0.4 (crate (name "flann-sys") (vers "0.0.4") (deps (list (crate-dep (name "bindgen") (req "^0.32.1") (default-features #t) (kind 1)) (crate-dep (name "cc") (req "^1.0.3") (default-features #t) (kind 1)) (crate-dep (name "pkg-config") (req "^0.3.9") (default-features #t) (kind 1)))) (hash "0b180bqkv7np2sx9870i8617l3kg1zkn8iicaw55sxxbzpmfw1w0")))

(define-public crate-flann-sys-0.1 (crate (name "flann-sys") (vers "0.1.0") (deps (list (crate-dep (name "bindgen") (req "^0.32.1") (default-features #t) (kind 1)) (crate-dep (name "cmake") (req "^0.1.35") (default-features #t) (kind 1)))) (hash "0h851azkcj65p2g59rspkxg2vjj2rrclby3nbyzlya8c9i2gkv1x")))

(define-public crate-flanterm_bindings-1 (crate (name "flanterm_bindings") (vers "1.0.0") (deps (list (crate-dep (name "bindgen") (req "^0.69.4") (default-features #t) (kind 1)) (crate-dep (name "cc") (req "^1.0.94") (default-features #t) (kind 1)))) (hash "05lci8gzp6vjcv0lgr2d2r60aw17a1s1bdcp2rs4nsfx07mxiq0d") (yanked #t)))

(define-public crate-flanterm_bindings-1 (crate (name "flanterm_bindings") (vers "1.0.1") (deps (list (crate-dep (name "bindgen") (req "^0.69.4") (default-features #t) (kind 1)))) (hash "1r9xyjzamha6qy1m6j8bkfgza30703qwz8l0b5q67i490hvd41kn") (yanked #t)))

(define-public crate-flanterm_bindings-0.1 (crate (name "flanterm_bindings") (vers "0.1.0") (deps (list (crate-dep (name "bindgen") (req "^0.69.4") (default-features #t) (kind 1)) (crate-dep (name "cc") (req "^1.0.94") (default-features #t) (kind 1)))) (hash "1g5yyjhnrgrh44wy2ijqcxjnjjl0vxxyl8p49v8ky6sibjdrd3mc") (yanked #t)))

(define-public crate-flanterm_bindings-1 (crate (name "flanterm_bindings") (vers "1.0.2") (deps (list (crate-dep (name "bindgen") (req "^0.69.4") (default-features #t) (kind 1)) (crate-dep (name "cc") (req "^1.0.94") (default-features #t) (kind 1)))) (hash "131hg63kx64gzqkz73sg208xmnafzdnm9an83nwfrpa957w5ixsz")))

(define-public crate-flanterm_bindings-1 (crate (name "flanterm_bindings") (vers "1.0.3") (deps (list (crate-dep (name "bindgen") (req "^0.69.4") (default-features #t) (kind 1)) (crate-dep (name "cc") (req "^1.0.94") (default-features #t) (kind 1)))) (hash "1g7pskpchj0dcl1gk3dn5vjsn676ywkjcpzf8nwz08b5h4qqbnrg")))

(define-public crate-flanterm_bindings-1 (crate (name "flanterm_bindings") (vers "1.0.4") (deps (list (crate-dep (name "bindgen") (req "^0.69.4") (default-features #t) (kind 1)) (crate-dep (name "cc") (req "^1.0.94") (default-features #t) (kind 1)))) (hash "19wym323rfm3ik1cz4i0x6vbqj9whx91zdx34a4yq20mdy3ysisk")))

(define-public crate-flanterm_bindings-1 (crate (name "flanterm_bindings") (vers "1.0.5") (deps (list (crate-dep (name "bindgen") (req "^0.69.4") (default-features #t) (kind 1)) (crate-dep (name "cc") (req "^1.0.94") (default-features #t) (kind 1)))) (hash "0gxdcbb1pyp1vxzf3djwsplsdj76rqyd5q7xqw7560izfs4jxybw")))

