(define-module (crates-io fl -w) #:use-module (crates-io))

(define-public crate-fl-wasm-rs-0.1 (crate (name "fl-wasm-rs") (vers "0.1.0") (deps (list (crate-dep (name "cfg-if") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "fl-wasm-rs-macros") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.85") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0") (default-features #t) (kind 0)))) (hash "00z2lhy5dqgcasf228l5xik1i2xk2ydd126fbzq71vzb59hvcp7n") (rust-version "1.65")))

(define-public crate-fl-wasm-rs-0.1 (crate (name "fl-wasm-rs") (vers "0.1.1") (deps (list (crate-dep (name "cfg-if") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "fl-wasm-rs-macros") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.85") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0") (default-features #t) (kind 0)))) (hash "1grba551z67n9rg1b0gl1f94jj6k8ifhbgmwwin4whnaxlwdj8ys") (rust-version "1.65")))

(define-public crate-fl-wasm-rs-macros-0.1 (crate (name "fl-wasm-rs-macros") (vers "0.1.0") (deps (list (crate-dep (name "proc-macro-error") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)))) (hash "06xnis1ix6g0r1i43alxbnbm257z3h4xgrirky921lwmvpx8ip4j") (rust-version "1.65")))

