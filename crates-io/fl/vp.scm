(define-module (crates-io fl vp) #:use-module (crates-io))

(define-public crate-flvparse-0.1 (crate (name "flvparse") (vers "0.1.0") (deps (list (crate-dep (name "nom") (req "^6.1") (kind 0)))) (hash "1x34gb5gzsr8pdgnf2f4iwas3d5ydfw2dg74bmwqqday8a3qqx3i") (features (quote (("std" "nom/std") ("default" "std") ("alloc" "nom/alloc"))))))

(define-public crate-flvparser-0.0.1 (crate (name "flvparser") (vers "0.0.1") (deps (list (crate-dep (name "byteorder") (req "^1.4.2") (default-features #t) (kind 0)) (crate-dep (name "bytes") (req "^1.0.0") (default-features #t) (kind 0)) (crate-dep (name "failure") (req "^0.1.1") (default-features #t) (kind 0)) (crate-dep (name "hmac") (req "^0.6.2") (default-features #t) (kind 0)) (crate-dep (name "networkio") (req "^0.1.23") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "sha2") (req "^0.7.1") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.4.0") (features (quote ("full"))) (kind 0)) (crate-dep (name "tokio-util") (req "^0.6.5") (features (quote ("codec"))) (default-features #t) (kind 0)))) (hash "1nr6p3mpsrw6wx847irl0mdp3j0b0f4cbkavsz0i5jj940wsjpk7")))

