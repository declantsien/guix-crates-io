(define-module (crates-io fl or) #:use-module (crates-io))

(define-public crate-flora-0.0.0 (crate (name "flora") (vers "0.0.0") (hash "14flivs631xg1wlkbsg5rkp6lrmaqgjp413rbssd289xnxd7drvd")))

(define-public crate-floral-0.0.0 (crate (name "floral") (vers "0.0.0") (hash "1vap2qfd3pk6zb87knawh4d0nvvvibchd5hyx7fpkrwblfzbmhnn")))

(define-public crate-floral_barrel-0.1 (crate (name "floral_barrel") (vers "0.1.0") (hash "1mj7smd2kn9wn18b6588jr5b344hikl2zahdd3rxzaybxl5cqgwa")))

(define-public crate-floral_barrel-0.2 (crate (name "floral_barrel") (vers "0.2.0") (deps (list (crate-dep (name "chrono") (req "^0.4.28") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^4.4.1") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "dirs") (req "^5.0.1") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.188") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.105") (default-features #t) (kind 0)))) (hash "130kch3l9q8pw455mpqlfgcysfsf7z0kdpjc1akddfbz90ap3hxf")))

(define-public crate-floral_barrel-0.2 (crate (name "floral_barrel") (vers "0.2.1") (deps (list (crate-dep (name "chrono") (req "^0.4.28") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^4.4.1") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "dirs") (req "^5.0.1") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.188") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.105") (default-features #t) (kind 0)))) (hash "1gpifyv6p1dm4xzi9xcf4bsb4v5hafpdffi1xk7l43ik3dc54hiw")))

(define-public crate-floread-0.0.0 (crate (name "floread") (vers "0.0.0") (hash "0l0rl2rk58gi894744qkifanx61r4g4y2gdl1f758m5n0fkzzk0j")))

