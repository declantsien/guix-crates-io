(define-module (crates-io fl ec) #:use-module (crates-io))

(define-public crate-flecha-0.0.0 (crate (name "flecha") (vers "0.0.0") (hash "1s231qbm3vnfigh1rfipccwri7ih3nh4xqc5qcddd5if5x0wvbb0")))

(define-public crate-fleck-0.1 (crate (name "fleck") (vers "0.1.0") (deps (list (crate-dep (name "bencher") (req "^0.1.5") (default-features #t) (kind 2)))) (hash "0brk5ib1cai0chplrrvmym5kiza87g36b31i5sy5s6r1f1zvg1kd")))

(define-public crate-fleck-0.1 (crate (name "fleck") (vers "0.1.1") (deps (list (crate-dep (name "bencher") (req "^0.1.5") (default-features #t) (kind 2)))) (hash "148blszlwn2vz7p4jz9bprs38ykkk99ldv25v026b6pzha2naqfc")))

(define-public crate-fleck-0.2 (crate (name "fleck") (vers "0.2.0") (deps (list (crate-dep (name "bencher") (req "^0.1.5") (default-features #t) (kind 2)))) (hash "095vk37vx03bcn0lcym6qcr4klpbq4fwlqqvf1qrcry9gvrhc3pp") (features (quote (("no_std"))))))

(define-public crate-flecs-0.1 (crate (name "flecs") (vers "0.1.0") (deps (list (crate-dep (name "bindgen") (req "^0.59.1") (default-features #t) (kind 1)) (crate-dep (name "cc") (req "^1.0.70") (default-features #t) (kind 1)) (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)))) (hash "1cflczxlqd97pa5qlry1bad31xljyjinnad5ssvk3gjqbks62vgl")))

(define-public crate-flecs-0.1 (crate (name "flecs") (vers "0.1.1") (deps (list (crate-dep (name "bindgen") (req "^0.59.1") (default-features #t) (kind 1)) (crate-dep (name "cc") (req "^1.0.70") (default-features #t) (kind 1)) (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)))) (hash "1ikmi7dwimaaw78cd0cyvhnyk1nn82zswjc7clhqs9nwkf6ys295")))

(define-public crate-flecs-0.1 (crate (name "flecs") (vers "0.1.2") (deps (list (crate-dep (name "bevy_ecs") (req "^0.10.1") (default-features #t) (kind 2)) (crate-dep (name "bindgen") (req "^0.65.1") (default-features #t) (kind 1)) (crate-dep (name "cc") (req "^1.0.70") (default-features #t) (kind 1)) (crate-dep (name "criterion") (req "^0.4") (default-features #t) (kind 2)) (crate-dep (name "flax") (req "^0.4.0") (default-features #t) (kind 2)) (crate-dep (name "hecs") (req "^0.10.3") (default-features #t) (kind 2)) (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)))) (hash "16p483791rlgrksr6yynwd8jwzapb1zhjqmil4jn77p5kkjrg4pf")))

(define-public crate-flecs-0.1 (crate (name "flecs") (vers "0.1.3") (deps (list (crate-dep (name "bevy_ecs") (req "^0.10.1") (default-features #t) (kind 2)) (crate-dep (name "bindgen") (req "^0.65.1") (default-features #t) (kind 1)) (crate-dep (name "cc") (req "^1.0.70") (default-features #t) (kind 1)) (crate-dep (name "criterion") (req "^0.4") (default-features #t) (kind 2)) (crate-dep (name "flax") (req "^0.4.0") (default-features #t) (kind 2)) (crate-dep (name "hecs") (req "^0.10.3") (default-features #t) (kind 2)) (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)))) (hash "1xnh339xhfmx1d84ka4bcbcbqcdlx74mxbkaxpizmy45ma2jz0i5")))

(define-public crate-flecs-0.1 (crate (name "flecs") (vers "0.1.4") (deps (list (crate-dep (name "bevy_ecs") (req "^0.10.1") (default-features #t) (kind 2)) (crate-dep (name "criterion") (req "^0.5") (default-features #t) (kind 2)) (crate-dep (name "flax") (req "^0.4.0") (default-features #t) (kind 2)) (crate-dep (name "flecs-sys") (req "^0.1.4") (default-features #t) (kind 0)) (crate-dep (name "hecs") (req "^0.10.3") (default-features #t) (kind 2)) (crate-dep (name "once_cell") (req "^1.18") (default-features #t) (kind 0)))) (hash "1fjpfbashkj7qhawbd84q0kqrkl3q5ajywv7rah2hbczfld8zaxp") (features (quote (("export_bindings" "flecs-sys/export_bindings")))) (rust-version "1.68")))

(define-public crate-flecs-0.1 (crate (name "flecs") (vers "0.1.5") (deps (list (crate-dep (name "bevy_ecs") (req "^0.11.3") (default-features #t) (kind 2)) (crate-dep (name "criterion") (req "^0.5") (default-features #t) (kind 2)) (crate-dep (name "flax") (req "^0.5.0") (default-features #t) (kind 2)) (crate-dep (name "flecs-sys") (req "^0.1.5") (default-features #t) (kind 0)) (crate-dep (name "hecs") (req "^0.10.3") (default-features #t) (kind 2)) (crate-dep (name "once_cell") (req "^1.18") (default-features #t) (kind 0)))) (hash "14b2z1922pk3hsszzws0q6y5958hz378gx7g9265iyx07bm8cc2c") (features (quote (("export_bindings" "flecs-sys/export_bindings")))) (rust-version "1.68")))

(define-public crate-flecs-sys-0.1 (crate (name "flecs-sys") (vers "0.1.4") (deps (list (crate-dep (name "bindgen") (req "^0.66") (default-features #t) (kind 1)) (crate-dep (name "cc") (req "^1") (default-features #t) (kind 1)) (crate-dep (name "once_cell") (req "^1.18") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.8") (default-features #t) (kind 1)))) (hash "1y5qcr7h8vcx62aiv8d1kk3cpvdwchpsk18j2ghrkxxmklbv7xds") (features (quote (("export_bindings")))) (links "flecs") (rust-version "1.68")))

(define-public crate-flecs-sys-0.1 (crate (name "flecs-sys") (vers "0.1.5") (deps (list (crate-dep (name "bindgen") (req "^0.66") (default-features #t) (kind 1)) (crate-dep (name "cc") (req "^1") (default-features #t) (kind 1)) (crate-dep (name "once_cell") (req "^1.18") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.8") (default-features #t) (kind 1)))) (hash "0aqaidp8qczjc0qqmalcnqxvn2rkxd3hn252b1jriarqbsgmpcdm") (features (quote (("export_bindings")))) (links "flecs") (rust-version "1.68")))

(define-public crate-flecs_ecs-0.0.1 (crate (name "flecs_ecs") (vers "0.0.1") (hash "1b8jbsnjdgnl6bvnxv6y931y3g7jddhfqf4619qvdqwfmddli0bd") (yanked #t)))

