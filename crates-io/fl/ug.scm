(define-module (crates-io fl ug) #:use-module (crates-io))

(define-public crate-flugrost-0.1 (crate (name "flugrost") (vers "0.1.0") (hash "10zv2anf3rcl1qsjx65yd2ayisk3ikjw7133ngcddl6j6hdbcmr2") (yanked #t)))

(define-public crate-flugrost-0.0.1 (crate (name "flugrost") (vers "0.0.1") (deps (list (crate-dep (name "byteorder") (req "^1.2.2") (default-features #t) (kind 0)) (crate-dep (name "rmp-serde") (req "^0.13.7") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (default-features #t) (kind 0)))) (hash "1kmz36v8j61jjbgyp9yw4mbpaf1ca9s8hs9cal33i2nx8wl6jdfg")))

