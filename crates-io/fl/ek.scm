(define-module (crates-io fl ek) #:use-module (crates-io))

(define-public crate-flek-1 (crate (name "flek") (vers "1.4.0") (deps (list (crate-dep (name "cliply") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "coutils") (req "^1.4.0") (default-features #t) (kind 0)))) (hash "1mbd2ar383q7x6izd29q292gfq5srgvq19kbsrjvwga5knkgpy03")))

(define-public crate-flek-1 (crate (name "flek") (vers "1.5.0") (deps (list (crate-dep (name "cliply") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "coutils") (req "^1.5.0") (default-features #t) (kind 0)))) (hash "09ayg9axdm8fdw9ch3z9l08zvh6c62xgm3h2vpp7kvrj7jdnnwj9")))

(define-public crate-flek-1 (crate (name "flek") (vers "1.6.0") (deps (list (crate-dep (name "cliply") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "coutils") (req "^1.5.0") (default-features #t) (kind 0)))) (hash "0py10p817kjjn1kj9j23l5w8b4zk1p2qzg9l3x0k9qs09rcgd7h6")))

(define-public crate-flek-1 (crate (name "flek") (vers "1.7.0") (deps (list (crate-dep (name "cliply") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "coutils") (req "^1.5.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "sha256") (req "^1.3.0") (default-features #t) (kind 0)))) (hash "19b2m93bg0wh7d2cg6zvlfvf7pwn47rz25fvnh2cjzx1hvr8j4x1")))

(define-public crate-flek-1 (crate (name "flek") (vers "1.8.0") (deps (list (crate-dep (name "cliply") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "coutils") (req "^1.5.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "sha256") (req "^1.3.0") (default-features #t) (kind 0)))) (hash "0nzxs9xd00s7q13ivdba7pa99b1fngh57252lnkphnibpz7lswvf")))

(define-public crate-flek-1 (crate (name "flek") (vers "1.9.0") (deps (list (crate-dep (name "cliply") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "coutils") (req "^1.5.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "sha256") (req "^1.3.0") (default-features #t) (kind 0)))) (hash "1x177lxma2pljkz4yq1kyz92y5mi7gds6n1wygqrz3qyhyxz5y4b")))

