(define-module (crates-io fl ps) #:use-module (crates-io))

(define-public crate-flpsecret-2 (crate (name "flpsecret") (vers "2.0.0") (deps (list (crate-dep (name "clap") (req "^3.0.0-beta.2") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.7") (default-features #t) (kind 0)))) (hash "0w15wrfpy1p75s4nxhq6ywzb25fp4mhyxad2d3n7am726m4llfvm")))

