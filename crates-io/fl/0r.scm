(define-module (crates-io fl #{0r}#) #:use-module (crates-io))

(define-public crate-fl0rian_s_lesson_grrs-0.1 (crate (name "fl0rian_s_lesson_grrs") (vers "0.1.0") (deps (list (crate-dep (name "anyhow") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "assert_cmd") (req "^2") (default-features #t) (kind 2)) (crate-dep (name "predicates") (req "^2") (default-features #t) (kind 2)) (crate-dep (name "structopt") (req "^0.3.25") (default-features #t) (kind 0)) (crate-dep (name "tempfile") (req "^3") (default-features #t) (kind 2)))) (hash "1kcmjsqq26b28r6klabbm4pz0n94ajxcgigyh1w5ym6hbnmann3a")))

