(define-module (crates-io fl ea) #:use-module (crates-io))

(define-public crate-fleabit-0.1 (crate (name "fleabit") (vers "0.1.0") (deps (list (crate-dep (name "bitvec") (req "^1.0.1") (default-features #t) (kind 0)))) (hash "0yjswvq90ml5l57j6dvk9vcaw9cf56cic65q6vxlg274lzmpskci") (yanked #t)))

(define-public crate-fleabit-0.1 (crate (name "fleabit") (vers "0.1.1") (deps (list (crate-dep (name "bitvec") (req "^1.0.1") (default-features #t) (kind 0)))) (hash "0yc6kfkxxbf6n017j2ppkcq3z9ha2f0klq2gqwaqb5brjqv211c2") (yanked #t)))

(define-public crate-fleabit-0.1 (crate (name "fleabit") (vers "0.1.2") (deps (list (crate-dep (name "bitvec") (req "^1.0.1") (default-features #t) (kind 0)))) (hash "1cd50i52qy3la07cdyib399x40cq47yyfz5nfrj4b0nz6kl6z3vm") (yanked #t)))

