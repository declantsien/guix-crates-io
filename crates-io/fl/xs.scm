(define-module (crates-io fl xs) #:use-module (crates-io))

(define-public crate-flxsdk_localization-1 (crate (name "flxsdk_localization") (vers "1.0.0") (deps (list (crate-dep (name "language-objects") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "message-locator") (req "^1") (default-features #t) (kind 0)))) (hash "14i53zx8yxn1fa9vkzyjh0vmxwxyrijzw15giad93sb78xlnac07") (yanked #t)))

