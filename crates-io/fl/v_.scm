(define-module (crates-io fl v_) #:use-module (crates-io))

(define-public crate-flv_codec-0.0.1 (crate (name "flv_codec") (vers "0.0.1") (deps (list (crate-dep (name "bytecodec") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "trackable") (req "^0.2") (default-features #t) (kind 0)))) (hash "1pzss0fky0m40aqlp44z2psvz5zdbvm8587kxcl85iy6xxrk74hp")))

(define-public crate-flv_codec-0.0.2 (crate (name "flv_codec") (vers "0.0.2") (deps (list (crate-dep (name "bytecodec") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "trackable") (req "^0.2") (default-features #t) (kind 0)))) (hash "11cgzyhh8lg6gvyss46w9mg1ym4zfhmfi9r8719ag60p8bwim63h")))

(define-public crate-flv_codec-0.1 (crate (name "flv_codec") (vers "0.1.0") (deps (list (crate-dep (name "bytecodec") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "trackable") (req "^0.2") (default-features #t) (kind 0)))) (hash "0f760jhgi4d7gdyjs4zz1hsnkkql7kbmh0wnrqgl4sz0n631ck6w")))

