(define-module (crates-io fl et) #:use-module (crates-io))

(define-public crate-flet-0.0.0 (crate (name "flet") (vers "0.0.0") (hash "0fmgkbvxgb5x5a2wj0dix4askrccxhi69w2gly7hf354rqn2yiw8")))

(define-public crate-fletcher-0.0.1 (crate (name "fletcher") (vers "0.0.1") (hash "06762m1apnp27k0njfwjnymrsx7k3mg50kwd3pkkid46scp7pbvw")))

(define-public crate-fletcher-0.1 (crate (name "fletcher") (vers "0.1.0") (deps (list (crate-dep (name "byteorder") (req "^1.3") (default-features #t) (kind 2)))) (hash "04b6phl9asd102na3f74i3bac502w17wilpjlg2465ax0c55svy7")))

(define-public crate-fletcher-0.2 (crate (name "fletcher") (vers "0.2.0") (deps (list (crate-dep (name "byteorder") (req "^1.3") (default-features #t) (kind 2)))) (hash "1wq9zcnd3zmq038bdicadhfc8gbgdr9q3w1iqphzadgqv3c785lj")))

(define-public crate-fletcher-0.3 (crate (name "fletcher") (vers "0.3.0") (deps (list (crate-dep (name "byteorder") (req "^1.3") (default-features #t) (kind 2)))) (hash "0w0h31kqwz0n3ffizysmxzw3yysy7dnkdw28h2xd3r7j4i8svdkq")))

(define-public crate-fletcher-simd-0.1 (crate (name "fletcher-simd") (vers "0.1.0") (deps (list (crate-dep (name "byteorder") (req "^1.4") (default-features #t) (kind 2)) (crate-dep (name "multiversion") (req "^0.6") (default-features #t) (kind 0)) (crate-dep (name "num") (req "^0.4") (kind 0)))) (hash "1bhkajh652w2qx3lk63m49k515dkckzba6vw4911dk019k3qz1qc")))

(define-public crate-fletcher-simd-0.1 (crate (name "fletcher-simd") (vers "0.1.1") (deps (list (crate-dep (name "byteorder") (req "^1.4") (default-features #t) (kind 2)) (crate-dep (name "multiversion") (req "^0.6") (default-features #t) (kind 0)) (crate-dep (name "num") (req "^0.4") (kind 0)) (crate-dep (name "rand") (req "^0.8") (default-features #t) (kind 2)))) (hash "0vqjfayp9s72jkgw5vsfy2s41i9lgdqp7m14nyvch8w73pf9gqah")))

(define-public crate-fletcher-simd-0.2 (crate (name "fletcher-simd") (vers "0.2.0") (deps (list (crate-dep (name "byteorder") (req "^1.4") (default-features #t) (kind 2)) (crate-dep (name "multiversion") (req "^0.6") (default-features #t) (kind 0)) (crate-dep (name "num") (req "^0.4") (kind 0)) (crate-dep (name "rand") (req "^0.8") (default-features #t) (kind 2)))) (hash "0cizg22kfi76ql3k97hkyczjf68rz2n2hdv0988c6gcqjk96irhp")))

(define-public crate-fletcher-simd-0.3 (crate (name "fletcher-simd") (vers "0.3.0") (deps (list (crate-dep (name "byteorder") (req "^1.4") (default-features #t) (kind 2)) (crate-dep (name "multiversion") (req "^0.6") (kind 0)) (crate-dep (name "num") (req "^0.4") (kind 0)) (crate-dep (name "rand") (req "^0.8") (default-features #t) (kind 2)))) (hash "10xmzxkfnwgn9d5g9a8i9xzbfbbqisxjzih4nzcddqgx34icxcyp") (features (quote (("runtime_dispatch" "multiversion/std") ("default" "runtime_dispatch"))))))

(define-public crate-fletcher-simd-0.4 (crate (name "fletcher-simd") (vers "0.4.0") (deps (list (crate-dep (name "byteorder") (req "^1.4") (default-features #t) (kind 2)) (crate-dep (name "multiversion") (req "^0.6") (kind 0)) (crate-dep (name "num") (req "^0.4") (kind 0)) (crate-dep (name "rand") (req "^0.8") (default-features #t) (kind 2)))) (hash "1q0zh4y5z1ggwdvjkzgqcnj47n070fj7ny72wnjzhgb6dr0adw02") (features (quote (("runtime_dispatch" "multiversion/std") ("default" "runtime_dispatch"))))))

