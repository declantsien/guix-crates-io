(define-module (crates-io fl #{2r}#) #:use-module (crates-io))

(define-public crate-fl2rust-0.1 (crate (name "fl2rust") (vers "0.1.0") (hash "05ipyk8xsdx4c1xm0k3ipjdc5b9skkbkgdh9kaxxf6ab80bq9s6c")))

(define-public crate-fl2rust-0.1 (crate (name "fl2rust") (vers "0.1.1") (hash "1ylgzp4qbll5akzq7i39bz03m3yp409124pkn5fqvyjxsifmh7r6")))

(define-public crate-fl2rust-0.1 (crate (name "fl2rust") (vers "0.1.2") (hash "1fnqj8h2l86hrxkisl4ksq4yma4i482d5gw666bdk14f78araij4")))

(define-public crate-fl2rust-0.1 (crate (name "fl2rust") (vers "0.1.5") (hash "1cywf3xj5iqa60j5pvdsln80dgs9x5gzsrpf2qarndimki7dgwgl")))

(define-public crate-fl2rust-0.1 (crate (name "fl2rust") (vers "0.1.6") (hash "080i55nz4kal0xrc7hgpqd6lw27l91lg7ry6fvhl3drxvxh76m0l")))

(define-public crate-fl2rust-0.1 (crate (name "fl2rust") (vers "0.1.7") (hash "10fd2szvpcw0cr60ywvhwykcr672a4naa2rq7hh5fxpjcg749lvc")))

(define-public crate-fl2rust-0.1 (crate (name "fl2rust") (vers "0.1.8") (hash "0bnayx9w8x5bql0jzbjhmgy1fc4x19xaw1v2jal2qz1q23frhrzn")))

(define-public crate-fl2rust-0.1 (crate (name "fl2rust") (vers "0.1.9") (hash "1b6w8clf0g1phg40f3fyznd73awpfcz1i48psa631raxfvn75xrl")))

(define-public crate-fl2rust-0.1 (crate (name "fl2rust") (vers "0.1.10") (hash "08hpa0rjrfn20n3pf2656s6a0nnwddxwix73wfl1k9xr8j9phwik")))

(define-public crate-fl2rust-0.1 (crate (name "fl2rust") (vers "0.1.11") (hash "0w11bv4vkxlj6vwakhy4qknyp2aywvld9lpkjn0ararz4z47w6qa")))

(define-public crate-fl2rust-0.1 (crate (name "fl2rust") (vers "0.1.12") (hash "0xnw737dk3y172siwlw3vzwrhzr97bl0fkk9qn5xhrhbxv34ziz8")))

(define-public crate-fl2rust-0.1 (crate (name "fl2rust") (vers "0.1.13") (hash "0is8w6313xbqhm6p82s6hphz5cm8a3k9n4d8p2437z5xcx8gj4zk")))

(define-public crate-fl2rust-0.1 (crate (name "fl2rust") (vers "0.1.14") (hash "15sgysravds9p97a8lkbpzqgl41b4am9d372cws9lrhldmmvnzhr")))

(define-public crate-fl2rust-0.2 (crate (name "fl2rust") (vers "0.2.0") (hash "0pmmj30ibsy894di0xi518n4g4gb3s4pwv3njr1rfz3cqh45z7r7")))

(define-public crate-fl2rust-0.2 (crate (name "fl2rust") (vers "0.2.1") (hash "0vkd6bx86ppfqrv6ig5v3dyrd9x7g4h68g22y1npbl7q8d8f1b67")))

(define-public crate-fl2rust-0.2 (crate (name "fl2rust") (vers "0.2.2") (hash "17k70g1s4fd3ln3k6jrja0mknrpkc4kp87mib9d614wlxnf5vpql")))

(define-public crate-fl2rust-0.2 (crate (name "fl2rust") (vers "0.2.3") (hash "04sfin2w0qy4xr5zllhwffwinzmbfnm1zrin73snj3ajjm3wdqhz")))

(define-public crate-fl2rust-0.2 (crate (name "fl2rust") (vers "0.2.4") (hash "0b3bva13d6k69bxyiwkfpaz1n9l5asw67rn0rmiirs7ns8sqs3kj")))

(define-public crate-fl2rust-0.2 (crate (name "fl2rust") (vers "0.2.5") (hash "00lps74qf45p89n3ksk2z4pk7nyk0450hrnhipx8n4wpfbfwb1xl")))

(define-public crate-fl2rust-0.2 (crate (name "fl2rust") (vers "0.2.6") (hash "1q10vvbpsmwzh0cn578k450d2j0g1g9l8vnwkcqz3madxglj5mli")))

(define-public crate-fl2rust-0.2 (crate (name "fl2rust") (vers "0.2.7") (hash "0mpc6lsxs6zf9lpvx479pkg9dmid8akazmhp3yc0rh6818r9apg6")))

(define-public crate-fl2rust-0.2 (crate (name "fl2rust") (vers "0.2.8") (hash "1sjkcxf8gxa3ny1yz6777f6pa312pw83ahdvm6dyqkw33wlvy6sk")))

(define-public crate-fl2rust-0.2 (crate (name "fl2rust") (vers "0.2.9") (hash "1vig1b9fmv8pnkc2a0xwwwrkmnn8hbnm6zslbikhfpz70sx769pp")))

(define-public crate-fl2rust-0.3 (crate (name "fl2rust") (vers "0.3.0") (hash "1qkjias50yyhkan4d2hd54rx88bv1017w71r2dgl7y7hwrcisfvi")))

(define-public crate-fl2rust-0.3 (crate (name "fl2rust") (vers "0.3.1") (deps (list (crate-dep (name "fltk") (req "^0.14") (default-features #t) (kind 2)))) (hash "0nwwwmr1sj7x587cvk0l9hbbldfr1l6v2knr0fngg0mzb5p792ib")))

(define-public crate-fl2rust-0.3 (crate (name "fl2rust") (vers "0.3.2") (deps (list (crate-dep (name "fltk") (req "^0.15") (default-features #t) (kind 2)))) (hash "0m1rbaf22r1ahbgsnlc18581kypd17zdw8637w1y48k7sjczp4fk")))

(define-public crate-fl2rust-0.4 (crate (name "fl2rust") (vers "0.4.0") (deps (list (crate-dep (name "fltk") (req "^1") (default-features #t) (kind 2)))) (hash "1apfpbi3vlm2ygpqpmliimb9z3db4vzn8d52hllm4fk2d3c9z3fk")))

(define-public crate-fl2rust-0.4 (crate (name "fl2rust") (vers "0.4.1") (deps (list (crate-dep (name "fltk") (req "^1") (default-features #t) (kind 2)))) (hash "1037ik8zsiijqhryrgl3rmzfiyksqhfsvldr92pyh7i532pibaij")))

(define-public crate-fl2rust-0.4 (crate (name "fl2rust") (vers "0.4.2") (deps (list (crate-dep (name "fltk") (req "^1") (default-features #t) (kind 2)))) (hash "001vlhxh5sff498c821ihzdj67ghfr4l46lmz0v6rjn6xf0dqrgk")))

(define-public crate-fl2rust-0.4 (crate (name "fl2rust") (vers "0.4.3") (deps (list (crate-dep (name "fltk") (req "^1") (default-features #t) (kind 2)))) (hash "1kllj71f5xwah9d58xx3b2z9pcpwhf93bfhgg3a5n6p40sa9iafq")))

(define-public crate-fl2rust-0.4 (crate (name "fl2rust") (vers "0.4.4") (deps (list (crate-dep (name "fltk") (req "^1") (default-features #t) (kind 2)))) (hash "05f2pbpb65ag9ghnfpssy9q4c0gnsb85wkmccqgfg8cvjhv33xv3")))

(define-public crate-fl2rust-0.4 (crate (name "fl2rust") (vers "0.4.5") (deps (list (crate-dep (name "fltk") (req "^1") (default-features #t) (kind 2)))) (hash "16fls3k19zqap2fx0k8zdj1rms1dm2l3vi9y7lf0mylm61mggmfn")))

(define-public crate-fl2rust-0.4 (crate (name "fl2rust") (vers "0.4.6") (deps (list (crate-dep (name "fltk") (req "^1") (default-features #t) (kind 2)))) (hash "1sws0wa8qwwl4qjswg3qribank4z6izqk7jfd2l7klh3dm6y0vyf")))

(define-public crate-fl2rust-0.4 (crate (name "fl2rust") (vers "0.4.7") (deps (list (crate-dep (name "fltk") (req "^1") (default-features #t) (kind 2)))) (hash "09nb7ay4ksv1nmrmmqf0bwl580qpk84bjb7x0kmaw6c8n62bqyxi")))

(define-public crate-fl2rust-0.4 (crate (name "fl2rust") (vers "0.4.8") (deps (list (crate-dep (name "fltk") (req "^1") (default-features #t) (kind 2)))) (hash "1rw6mwx93f4zwc712g2x8shm10lxhwwhj216kxslg8x472hk7lyh")))

(define-public crate-fl2rust-0.4 (crate (name "fl2rust") (vers "0.4.9") (deps (list (crate-dep (name "fltk") (req "^1") (default-features #t) (kind 2)))) (hash "16285g3zchb19sbi451fpjirjpxp53b3aa1acq8gil3gb8fppjj3")))

(define-public crate-fl2rust-0.4 (crate (name "fl2rust") (vers "0.4.10") (deps (list (crate-dep (name "fltk") (req "^1") (default-features #t) (kind 2)))) (hash "121563y99j7dv0d0kq9ncdhncqnx0yc6fixljw38ljxhc6paxs5m")))

(define-public crate-fl2rust-0.4 (crate (name "fl2rust") (vers "0.4.11") (deps (list (crate-dep (name "fltk") (req "^1") (default-features #t) (kind 2)))) (hash "0cn25zn4yphfm1zzhqs1cq6zvym06f5zd4r9vbznrqaikyk55kw2")))

(define-public crate-fl2rust-0.4 (crate (name "fl2rust") (vers "0.4.12") (deps (list (crate-dep (name "fltk") (req "^1") (default-features #t) (kind 2)))) (hash "0c8xl2h0m5rsr94dfjbipq2z12p5warq8pwrh73jh8g95rc16phl")))

(define-public crate-fl2rust-0.4 (crate (name "fl2rust") (vers "0.4.13") (deps (list (crate-dep (name "fltk") (req "^1") (default-features #t) (kind 2)) (crate-dep (name "fltk-form") (req "^0.1") (default-features #t) (kind 2)) (crate-dep (name "fltk-form-derive") (req "^0.1") (default-features #t) (kind 2)))) (hash "1b04s7f09njjcbasyinlkm65f6wvjz7ww9j9y77zfw3nf8ryr4px")))

(define-public crate-fl2rust-0.4 (crate (name "fl2rust") (vers "0.4.14") (deps (list (crate-dep (name "fltk") (req "^1") (default-features #t) (kind 2)) (crate-dep (name "fltk-form") (req "^0.1") (default-features #t) (kind 2)) (crate-dep (name "fltk-form-derive") (req "^0.1") (default-features #t) (kind 2)))) (hash "1y00r382z9lypnn83y6d69z8sjy3bccmw5k8n9bmrjcfv2ad0z5i")))

(define-public crate-fl2rust-0.4 (crate (name "fl2rust") (vers "0.4.15") (deps (list (crate-dep (name "fltk") (req "^1") (default-features #t) (kind 2)) (crate-dep (name "fltk-form") (req "^0.1") (default-features #t) (kind 2)) (crate-dep (name "fltk-form-derive") (req "^0.1") (default-features #t) (kind 2)))) (hash "0m4d4dbvzshanx9kl2icjjkvk5crv1k60fsf8lgd5inyhr2m6shb")))

(define-public crate-fl2rust-0.4 (crate (name "fl2rust") (vers "0.4.16") (deps (list (crate-dep (name "fltk") (req "^1") (default-features #t) (kind 2)) (crate-dep (name "fltk-form") (req "^0.1") (default-features #t) (kind 2)) (crate-dep (name "fltk-form-derive") (req "^0.1") (default-features #t) (kind 2)))) (hash "1vmnnmhpxxxxl1s91pmwwd5fqca15gnzv0m17689x5yz21i867vz")))

(define-public crate-fl2rust-0.4 (crate (name "fl2rust") (vers "0.4.17") (deps (list (crate-dep (name "fltk") (req "^1") (default-features #t) (kind 2)) (crate-dep (name "fltk-form") (req "^0.1") (default-features #t) (kind 2)) (crate-dep (name "fltk-form-derive") (req "^0.1") (default-features #t) (kind 2)))) (hash "043f677s73bvfrjgyq2hshazz9qf8xs41bahvlm6pcgga02531wf")))

(define-public crate-fl2rust-0.4 (crate (name "fl2rust") (vers "0.4.18") (deps (list (crate-dep (name "fltk") (req "^1") (default-features #t) (kind 2)) (crate-dep (name "fltk-form") (req "^0.1") (default-features #t) (kind 2)) (crate-dep (name "fltk-form-derive") (req "^0.1") (default-features #t) (kind 2)))) (hash "1lc9zjnkl01mp38yybi8vgy2mkcszzyxb27bph11b0z4kw9r32qy")))

(define-public crate-fl2rust-0.4 (crate (name "fl2rust") (vers "0.4.19") (deps (list (crate-dep (name "fltk") (req "^1") (default-features #t) (kind 2)) (crate-dep (name "fltk-form") (req "^0.1") (default-features #t) (kind 2)) (crate-dep (name "fltk-form-derive") (req "^0.1") (default-features #t) (kind 2)))) (hash "16c05xzw1mx1mhrag3mmfq10b93v2mzib6wkq7ib39ail4bf5bl2")))

(define-public crate-fl2rust-0.5 (crate (name "fl2rust") (vers "0.5.0") (deps (list (crate-dep (name "fluid-parser") (req "^0.1.6") (default-features #t) (kind 0)))) (hash "1j0sd26n822r8w2l06348f32jqjnqxa37fc8bx7ksssb6n6z1m0j") (yanked #t) (rust-version "1.63")))

(define-public crate-fl2rust-0.5 (crate (name "fl2rust") (vers "0.5.1") (deps (list (crate-dep (name "fluid-parser") (req "^0.1.6") (default-features #t) (kind 0)))) (hash "0384cd2mx5ylk1rjgpyhnhw7v7v2gah737a1yfkiazr9qgs5cnnd") (yanked #t) (rust-version "1.63")))

(define-public crate-fl2rust-0.5 (crate (name "fl2rust") (vers "0.5.2") (deps (list (crate-dep (name "fluid-parser") (req "^0.1.6") (default-features #t) (kind 0)))) (hash "1plr9r1gvqk4zqiq2hr1ia7vm5zw5rw62hb1qmdrlhm8i3nbkx41") (yanked #t) (rust-version "1.63")))

(define-public crate-fl2rust-0.5 (crate (name "fl2rust") (vers "0.5.3") (deps (list (crate-dep (name "fluid-parser") (req "^0.1.6") (default-features #t) (kind 0)))) (hash "0ciwc82irck97my3k8v8cxvnixkxmcvjz3qra415la5ba8s6sihy") (yanked #t) (rust-version "1.63")))

(define-public crate-fl2rust-0.5 (crate (name "fl2rust") (vers "0.5.4") (deps (list (crate-dep (name "fluid-parser") (req "^0.1.6") (default-features #t) (kind 0)))) (hash "0mnmbhybw2pjj11gh8zgafxvvvjqjf5jc8n64kfrxpn37s3jssxs") (yanked #t) (rust-version "1.63")))

(define-public crate-fl2rust-0.5 (crate (name "fl2rust") (vers "0.5.5") (deps (list (crate-dep (name "fluid-parser") (req "^0.1.6") (default-features #t) (kind 0)))) (hash "1pkyjb8klsz4f9g9kbs97q9sicwnmdrx6sdcgh9y9d47gd56f5aj") (yanked #t) (rust-version "1.63")))

(define-public crate-fl2rust-0.5 (crate (name "fl2rust") (vers "0.5.6") (deps (list (crate-dep (name "fluid-parser") (req "^0.1.6") (default-features #t) (kind 0)))) (hash "0i1c1a6hzxa0jlri1sk4939ki99jwd7db5gkcsbfyvx5vzrix040") (rust-version "1.63")))

(define-public crate-fl2rust-0.5 (crate (name "fl2rust") (vers "0.5.7") (deps (list (crate-dep (name "fluid-parser") (req "^0.1.9") (default-features #t) (kind 0)))) (hash "0l2pvg2vwf04i0xhg6h1y52hmj368c6mwxrcnlcq3g7wjc8qrvdn") (rust-version "1.63")))

(define-public crate-fl2rust-0.5 (crate (name "fl2rust") (vers "0.5.8") (deps (list (crate-dep (name "fluid-parser") (req "^0.1.9") (default-features #t) (kind 0)))) (hash "1342iwvlbvmwaqh8a0053i47jqc9hcqd5d73xfzqqas7fn14mgs5") (rust-version "1.63")))

(define-public crate-fl2rust-0.5 (crate (name "fl2rust") (vers "0.5.9") (deps (list (crate-dep (name "fluid-parser") (req "^0.1.9") (default-features #t) (kind 0)))) (hash "0jz0jwshmih5hzqh6bm8ib0jxcza5bfqbq3r6a1ncljyimhyp7kp") (rust-version "1.63")))

(define-public crate-fl2rust-0.5 (crate (name "fl2rust") (vers "0.5.10") (deps (list (crate-dep (name "fluid-parser") (req "^0.1.10") (default-features #t) (kind 0)))) (hash "1pwj8cv4dbcq3wb5ralvyv8cv9adi226px50g3ncw2bi9lwl62mk") (rust-version "1.63")))

(define-public crate-fl2rust-0.5 (crate (name "fl2rust") (vers "0.5.11") (deps (list (crate-dep (name "fluid-parser") (req "^0.1.12") (default-features #t) (kind 0)))) (hash "0wklzhj8rciwvpal9c2bvkb44lpraivj4fhviiwkvhpmgs6krclf") (rust-version "1.63")))

(define-public crate-fl2rust-0.5 (crate (name "fl2rust") (vers "0.5.12") (deps (list (crate-dep (name "fluid-parser") (req "^0.1.12") (default-features #t) (kind 0)))) (hash "0fvzvsgyv7l9hpbn6q7ar5cjv6mgbl2sr79acp3jw48k5gavp388") (rust-version "1.63")))

(define-public crate-fl2rust-0.5 (crate (name "fl2rust") (vers "0.5.13") (deps (list (crate-dep (name "fluid-parser") (req "^0.1.12") (default-features #t) (kind 0)))) (hash "0ssprb20scc8869k3ffrxbq9wx8y1y7zqgxmvl32js351vyva0vz") (rust-version "1.63")))

(define-public crate-fl2rust-0.5 (crate (name "fl2rust") (vers "0.5.14") (deps (list (crate-dep (name "fluid-parser") (req "^0.1.14") (default-features #t) (kind 0)))) (hash "14hdym18mgxr478m95x0pg4608dqq8y82g7x4syrfnj815dwf82f") (rust-version "1.63")))

(define-public crate-fl2rust-0.5 (crate (name "fl2rust") (vers "0.5.15") (deps (list (crate-dep (name "fluid-parser") (req "^0.1.14") (default-features #t) (kind 0)))) (hash "1nii2kpxi9wwmjrckfzb8v0apb56h9d4zpz9higs9hyv4i4nlj2v") (rust-version "1.63")))

(define-public crate-fl2rust-0.5 (crate (name "fl2rust") (vers "0.5.16") (deps (list (crate-dep (name "fluid-parser") (req "^0.1.14") (default-features #t) (kind 0)))) (hash "10641k778gql4zsfhr6pks90pns4l1fs2vmv4dl64mdsqsah43y1") (rust-version "1.63")))

(define-public crate-fl2rust-0.5 (crate (name "fl2rust") (vers "0.5.17") (deps (list (crate-dep (name "fluid-parser") (req "^0.1.14") (default-features #t) (kind 0)))) (hash "03qx0hb180wwhz7zy5gqvrk1l5ag5naahxvbg3dk86pw2m7dbx5p") (rust-version "1.63")))

(define-public crate-fl2rust-0.5 (crate (name "fl2rust") (vers "0.5.18") (deps (list (crate-dep (name "fluid-parser") (req "^0.1.14") (default-features #t) (kind 0)))) (hash "0vn0hcgkk3987522vhrpvp1a0863mhlgqphclk1lpynqsv87skyc") (rust-version "1.63")))

(define-public crate-fl2rust-0.5 (crate (name "fl2rust") (vers "0.5.19") (deps (list (crate-dep (name "fluid-parser") (req "^0.1.15") (default-features #t) (kind 0)))) (hash "0psr015kk8rf8a2vl6pqp89z8nyibw4a1biw9y2yww1fmq294rzn") (rust-version "1.63")))

(define-public crate-fl2rust-macro-0.5 (crate (name "fl2rust-macro") (vers "0.5.5") (deps (list (crate-dep (name "fl2rust") (req "^0.5") (default-features #t) (kind 0)) (crate-dep (name "fluid-parser") (req "^0.1") (default-features #t) (kind 0)))) (hash "0hmrv9v7d2xc6pi60jv9j2cpsaj1y896bcpz1vvc85cd0ma5lh2v") (rust-version "1.63")))

(define-public crate-fl2rust-macro-0.5 (crate (name "fl2rust-macro") (vers "0.5.6") (deps (list (crate-dep (name "fl2rust") (req "^0.5") (default-features #t) (kind 0)) (crate-dep (name "fluid-parser") (req "^0.1") (default-features #t) (kind 0)))) (hash "0mczk4hj65m33grlgisw14vr8mw73zsjf2fzs4khwbl5hw002cjj") (rust-version "1.63")))

(define-public crate-fl2rust-macro-0.5 (crate (name "fl2rust-macro") (vers "0.5.7") (deps (list (crate-dep (name "fl2rust") (req "^0.5.7") (default-features #t) (kind 0)) (crate-dep (name "fluid-parser") (req "^0.1.9") (default-features #t) (kind 0)))) (hash "05yxskmiyy7kwg4ld9wq6kj5ml8b55pwz50qnf015ciyw7y9ic5h") (rust-version "1.63")))

(define-public crate-fl2rust-macro-0.5 (crate (name "fl2rust-macro") (vers "0.5.8") (deps (list (crate-dep (name "fl2rust") (req "^0.5.8") (default-features #t) (kind 0)) (crate-dep (name "fluid-parser") (req "^0.1.9") (default-features #t) (kind 0)))) (hash "0wwz2cgcjkqxk6dijw0acr7l1r5asm8ayfgca1dj36fw4a4fk70q") (rust-version "1.63")))

(define-public crate-fl2rust-macro-0.5 (crate (name "fl2rust-macro") (vers "0.5.10") (deps (list (crate-dep (name "fl2rust") (req "^0.5.10") (default-features #t) (kind 0)) (crate-dep (name "fluid-parser") (req "^0.1.10") (default-features #t) (kind 0)))) (hash "1dk7c8fpkc2m84zmf4zhch66lzqw4a0325jk6cmg9gdl21lli6xw") (rust-version "1.63")))

(define-public crate-fl2rust-macro-0.5 (crate (name "fl2rust-macro") (vers "0.5.11") (deps (list (crate-dep (name "fl2rust") (req "^0.5.11") (default-features #t) (kind 0)) (crate-dep (name "fluid-parser") (req "^0.1.12") (default-features #t) (kind 0)))) (hash "0b09vbgddfbc338v9g889ps5bhmvj5sah48j3hsl6w9gv249b3x9") (rust-version "1.63")))

(define-public crate-fl2rust-macro-0.5 (crate (name "fl2rust-macro") (vers "0.5.12") (deps (list (crate-dep (name "fl2rust") (req "^0.5.12") (default-features #t) (kind 0)) (crate-dep (name "fluid-parser") (req "^0.1.12") (default-features #t) (kind 0)))) (hash "1m97npnpyjh7qcwpiynqa9zwb7afg3qzylsgas1jrbinncl0adkv") (rust-version "1.63")))

(define-public crate-fl2rust-macro-0.5 (crate (name "fl2rust-macro") (vers "0.5.13") (deps (list (crate-dep (name "fl2rust") (req "^0.5.13") (default-features #t) (kind 0)) (crate-dep (name "fluid-parser") (req "^0.1.12") (default-features #t) (kind 0)))) (hash "1k1yhqqi90rm660cjfrgz22mifyddkc42k1qrhpcr0d3nv4bv094") (rust-version "1.63")))

(define-public crate-fl2rust-macro-0.5 (crate (name "fl2rust-macro") (vers "0.5.14") (deps (list (crate-dep (name "fl2rust") (req "^0.5.14") (default-features #t) (kind 0)) (crate-dep (name "fluid-parser") (req "^0.1.14") (default-features #t) (kind 0)))) (hash "1l189ryr0iq83pr3rw83cl2llywsvl6a4jzzd5drrwv19vxjxx9i") (rust-version "1.63")))

(define-public crate-fl2rust-macro-0.5 (crate (name "fl2rust-macro") (vers "0.5.15") (deps (list (crate-dep (name "fl2rust") (req "^0.5.15") (default-features #t) (kind 0)) (crate-dep (name "fluid-parser") (req "^0.1.14") (default-features #t) (kind 0)))) (hash "0ihqck0h3x2as9dmfb21kdzsp2njkk4sdzch16i1r21176pwgig1") (rust-version "1.63")))

(define-public crate-fl2rust-macro-0.5 (crate (name "fl2rust-macro") (vers "0.5.16") (deps (list (crate-dep (name "fl2rust") (req "^0.5.16") (default-features #t) (kind 0)) (crate-dep (name "fluid-parser") (req "^0.1.14") (default-features #t) (kind 0)))) (hash "192dk5cif8l28cpra95lwdqmkzi6j7hzlhni6v7iqwgmcqjdllxg") (rust-version "1.63")))

(define-public crate-fl2rust-macro-0.5 (crate (name "fl2rust-macro") (vers "0.5.17") (deps (list (crate-dep (name "fl2rust") (req "^0.5.17") (default-features #t) (kind 0)) (crate-dep (name "fluid-parser") (req "^0.1.14") (default-features #t) (kind 0)))) (hash "12iljp75ipxfaviawjglbm61kl23zwj0k7h7bgv3vkqrcdx8q3fs") (rust-version "1.63")))

(define-public crate-fl2rust-macro-0.5 (crate (name "fl2rust-macro") (vers "0.5.18") (deps (list (crate-dep (name "fl2rust") (req "^0.5.18") (default-features #t) (kind 0)) (crate-dep (name "fluid-parser") (req "^0.1.14") (default-features #t) (kind 0)))) (hash "1i92h3q2ix5y848xhcnc6gc32ibw3igjk6pzkzzrvjw3pabgdgd9") (rust-version "1.63")))

(define-public crate-fl2rust-macro-0.5 (crate (name "fl2rust-macro") (vers "0.5.19") (deps (list (crate-dep (name "fl2rust") (req "^0.5.19") (default-features #t) (kind 0)) (crate-dep (name "fluid-parser") (req "^0.1.15") (default-features #t) (kind 0)))) (hash "13476kjg81jkpn3cp0synzr1gf87m8d1brkzjkfd7mqsrrnr5ln9") (rust-version "1.63")))

