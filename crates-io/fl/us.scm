(define-module (crates-io fl us) #:use-module (crates-io))

(define-public crate-fluse-0.1 (crate (name "fluse") (vers "0.1.0") (hash "1g1fwmycvh06528d3m1wc9q8q9cq5bmvfjgxm1dx03mfqv9nifgl")))

(define-public crate-fluse-0.1 (crate (name "fluse") (vers "0.1.2") (hash "02ghb5iijhl3pafihd3nvr4aas7b6alw4hgxr7ijdnpzniqnn6j3")))

(define-public crate-fluse-0.1 (crate (name "fluse") (vers "0.1.3") (hash "0ljj8spad8pmhszh0zdsgqfs9spdk2zs5fpbbn6q4pcfiww1cwlh")))

(define-public crate-fluse-0.1 (crate (name "fluse") (vers "0.1.4") (hash "15ydx83k1dqvhpdn9vw7gl246myz6da0bp9j9js3mml96iixlib8")))

(define-public crate-flush-0.1 (crate (name "flush") (vers "0.1.0") (deps (list (crate-dep (name "nix") (req "^0.11.0") (default-features #t) (kind 0)))) (hash "00fd52v81h1rzyirn5nsr5w6a017ma4d4p5djn64wnv2z8s629bl")))

(define-public crate-fluss-0.1 (crate (name "fluss") (vers "0.1.0") (deps (list (crate-dep (name "async-std") (req "^0.99") (default-features #t) (kind 2)) (crate-dep (name "futures-preview") (req "^0.3.0-alpha.17") (default-features #t) (kind 0)) (crate-dep (name "pin-utils") (req "^0.1.0-alpha.4") (default-features #t) (kind 0)) (crate-dep (name "slab") (req "^0.4.2") (default-features #t) (kind 0)))) (hash "0pvk0c0d1a6bn7lffi5vqb4zdabxx5r63hhxg725lhm79rkzj9i7")))

(define-public crate-fluss-0.1 (crate (name "fluss") (vers "0.1.1-alpha.1") (deps (list (crate-dep (name "async-std") (req "^0.99") (default-features #t) (kind 2)) (crate-dep (name "futures-preview") (req "^0.3.0-alpha.17") (default-features #t) (kind 0)) (crate-dep (name "pin-utils") (req "^0.1.0-alpha.4") (default-features #t) (kind 0)) (crate-dep (name "slab") (req "^0.4.2") (default-features #t) (kind 0)))) (hash "1r02p7g61g54wnihnwzc7z2n05iky100mj58695hx699j7j5b626")))

(define-public crate-flussab-0.1 (crate (name "flussab") (vers "0.1.0") (deps (list (crate-dep (name "num-traits") (req "^0.2.14") (default-features #t) (kind 0)))) (hash "060d0qmpgzlp3avh4j2rcg0cyzpsfds9cqkxmz8nlmivk0b4hlr0")))

(define-public crate-flussab-0.3 (crate (name "flussab") (vers "0.3.0") (deps (list (crate-dep (name "itoap") (req "^1.0.1") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2.14") (default-features #t) (kind 0)))) (hash "0hw810br6sl68gqbdhi341p10jvjla9ym9n4c5xkvqapzbidx49s")))

(define-public crate-flussab-0.3 (crate (name "flussab") (vers "0.3.1") (deps (list (crate-dep (name "itoap") (req "^1.0.1") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2.17") (default-features #t) (kind 0)))) (hash "0bxih9ydvk4dlibgqdmk19hrslwwkb9jsa11p9wls7ma867nvm7w")))

(define-public crate-flussab-aiger-0.1 (crate (name "flussab-aiger") (vers "0.1.0") (deps (list (crate-dep (name "flussab") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2.17") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.50") (default-features #t) (kind 0)) (crate-dep (name "zwohash") (req "^0.1.2") (default-features #t) (kind 0)))) (hash "1ll6g0aqn9q7nb2vlk32bplqpygfnbcs9jdkx1i325nhf2ckm2rp")))

(define-public crate-flussab-cnf-0.1 (crate (name "flussab-cnf") (vers "0.1.0") (deps (list (crate-dep (name "flussab") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "itoa") (req "^0.4.7") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.24") (default-features #t) (kind 0)))) (hash "0isqp74q05kx3gggr0slf7fm71micgi3ydbrhrb2m3d1jls661yl")))

(define-public crate-flussab-cnf-0.1 (crate (name "flussab-cnf") (vers "0.1.1") (deps (list (crate-dep (name "flussab") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "itoa") (req "^0.4.7") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.24") (default-features #t) (kind 0)))) (hash "0qf42irk22xqximf4yhixlzlpija17ci7crl1bnch1i84lr3bdwx")))

(define-public crate-flussab-cnf-0.2 (crate (name "flussab-cnf") (vers "0.2.0") (deps (list (crate-dep (name "flussab") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "itoa") (req "^0.4.7") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2.14") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.24") (default-features #t) (kind 0)))) (hash "146pvaa2ys7lpdncl60azb6kprf04g0a313fhp83mbm2l0vrbxcs")))

(define-public crate-flussab-cnf-0.3 (crate (name "flussab-cnf") (vers "0.3.0") (deps (list (crate-dep (name "assert_matches") (req "^1.5.0") (default-features #t) (kind 2)) (crate-dep (name "flussab") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2.14") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.30") (default-features #t) (kind 0)))) (hash "0csjx0c93hxap0fz4l8fmimid8n2gaaq978wa6px0r61pmwhdm64")))

(define-public crate-flussab-cnf-0.3 (crate (name "flussab-cnf") (vers "0.3.1") (deps (list (crate-dep (name "assert_matches") (req "^1.5.0") (default-features #t) (kind 2)) (crate-dep (name "flussab") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2.17") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.50") (default-features #t) (kind 0)))) (hash "1w2jcghqij9gi1qqr4pk580vydffy1bg0jgi95i3h33lbn3hvpd1")))

