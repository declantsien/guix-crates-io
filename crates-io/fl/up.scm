(define-module (crates-io fl up) #:use-module (crates-io))

(define-public crate-flupp-0.1 (crate (name "flupp") (vers "0.1.0") (deps (list (crate-dep (name "encoding_rs") (req "^0.8.32") (default-features #t) (kind 0)) (crate-dep (name "insta") (req "=1.29.0") (default-features #t) (kind 2)) (crate-dep (name "thiserror") (req "^1.0.40") (default-features #t) (kind 0)) (crate-dep (name "time") (req "^0.3.21") (default-features #t) (kind 0)))) (hash "1y4axbc5yd31ixf1v4qlgzy224w0x1rjq1pqccz8migy4ds1313c")))

(define-public crate-flupp-0.1 (crate (name "flupp") (vers "0.1.1") (deps (list (crate-dep (name "encoding_rs") (req "^0.8.32") (default-features #t) (kind 0)) (crate-dep (name "insta") (req "=1.29.0") (default-features #t) (kind 2)) (crate-dep (name "thiserror") (req "^1.0.40") (default-features #t) (kind 0)) (crate-dep (name "time") (req "^0.3.21") (default-features #t) (kind 0)))) (hash "0y3vrqsi0ivqfd6fmp8yzi4d88msq23vxqjjz1vjvzly12kp8576")))

(define-public crate-flupp-0.1 (crate (name "flupp") (vers "0.1.2") (deps (list (crate-dep (name "encoding_rs") (req "^0.8.32") (default-features #t) (kind 0)) (crate-dep (name "insta") (req "=1.31.0") (default-features #t) (kind 2)) (crate-dep (name "thiserror") (req "^1.0.44") (default-features #t) (kind 0)) (crate-dep (name "time") (req "^0.3.23") (default-features #t) (kind 0)))) (hash "0m3linf8810j4nb4k68ld9jdl3df36765dz1ds2s34j3yi83yc1b")))

(define-public crate-flupp-0.1 (crate (name "flupp") (vers "0.1.3") (deps (list (crate-dep (name "encoding_rs") (req "^0.8.32") (default-features #t) (kind 0)) (crate-dep (name "insta") (req "=1.31.0") (default-features #t) (kind 2)) (crate-dep (name "thiserror") (req "^1.0.44") (default-features #t) (kind 0)) (crate-dep (name "time") (req "^0.3.23") (default-features #t) (kind 0)))) (hash "02smpd8hjqc8j7qb2yh6r9hbkqifi76l6726yhxr7633zr7a9i9h")))

