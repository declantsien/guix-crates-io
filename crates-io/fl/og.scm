(define-module (crates-io fl og) #:use-module (crates-io))

(define-public crate-flog-0.1 (crate (name "flog") (vers "0.1.0") (deps (list (crate-dep (name "bincode") (req "^1.3.1") (default-features #t) (kind 2)) (crate-dep (name "criterion") (req "^0.3.3") (default-features #t) (kind 2)) (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.115") (default-features #t) (kind 2)) (crate-dep (name "sled") (req "^0.34.3") (default-features #t) (kind 2)))) (hash "057wzr8jn3lr96qqq95bmhxj1pqv3lw43v83vymk514j2lp5w3aj")))

(define-public crate-flog-0.1 (crate (name "flog") (vers "0.1.1") (deps (list (crate-dep (name "criterion") (req "^0.3.3") (default-features #t) (kind 2)) (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "pprof") (req "^0.3.18") (features (quote ("flamegraph"))) (default-features #t) (kind 0)))) (hash "1nvbvfqsfg3ll8qsla2kxkf44lhg7csq5skq43ikr5r3q0z8fcx6")))

(define-public crate-flog-0.1 (crate (name "flog") (vers "0.1.2") (deps (list (crate-dep (name "criterion") (req "^0.3.3") (default-features #t) (kind 2)) (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "pprof") (req "^0.3.18") (features (quote ("flamegraph"))) (default-features #t) (kind 0)))) (hash "1bmgvxzhc082bim75fwmchq8kk307ixwww5nnrp0prrdbqs79nwj")))

