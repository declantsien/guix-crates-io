(define-module (crates-io fl ol) #:use-module (crates-io))

(define-public crate-flololoat-0.1 (crate (name "flololoat") (vers "0.1.0") (deps (list (crate-dep (name "proc-macro2") (req "^1.0.69") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.33") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2.0.38") (features (quote ("visit-mut"))) (default-features #t) (kind 0)))) (hash "13wb81knl7ir42n9zmnkb9jhb4jh9fc1nh9c6kifcgd3qdgfambd")))

