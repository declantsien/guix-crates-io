(define-module (crates-io fl y_) #:use-module (crates-io))

(define-public crate-fly_bevy-0.1 (crate (name "fly_bevy") (vers "0.1.0") (deps (list (crate-dep (name "bevy") (req "^0.10.1") (default-features #t) (kind 0)))) (hash "1dm7f93hp8qrcphkr4a642rd4ikxyr6394q4zvn6ngif5sqhng2j")))

(define-public crate-fly_bevy-0.10 (crate (name "fly_bevy") (vers "0.10.2") (deps (list (crate-dep (name "bevy") (req "^0.10.1") (default-features #t) (kind 0)))) (hash "0bx1fj9wm8v85mh4rn956g7y24xfwj2j5fq9262dsqsgchlspbky")))

(define-public crate-fly_catcher-0.1 (crate (name "fly_catcher") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^2.33.3") (default-features #t) (kind 0)) (crate-dep (name "hyper") (req "^0.13") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^0.2") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "1ly6sff145nppn23d35qxv5x6sr9rkhjsgqq5l7vrv37bpligm6q") (yanked #t)))

