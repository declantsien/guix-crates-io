(define-module (crates-io fl b-) #:use-module (crates-io))

(define-public crate-flb-plugin-0.1 (crate (name "flb-plugin") (vers "0.1.0") (deps (list (crate-dep (name "flb-plugin-sys") (req "^0.1") (default-features #t) (kind 0)))) (hash "1xkvn5zrccc8d9jl9hpwpwkycvdsj17zs6i4g8698vbgq55lsfka")))

(define-public crate-flb-plugin-0.1 (crate (name "flb-plugin") (vers "0.1.1") (deps (list (crate-dep (name "flb-plugin-sys") (req "^0.1") (default-features #t) (kind 0)))) (hash "1pwzz05s2iwn9ghmysk7bp3iqi8hv1y314225c38hyj7pamqxdq6")))

(define-public crate-flb-plugin-sys-0.1 (crate (name "flb-plugin-sys") (vers "0.1.0") (deps (list (crate-dep (name "bindgen") (req "^0.60") (default-features #t) (kind 1)))) (hash "1kp0asl0yylsykps87arl08dyz1infbkgcf7shl5h04abxbx5x7q")))

