(define-module (crates-io fl ot) #:use-module (crates-io))

(define-public crate-flot-0.1 (crate (name "flot") (vers "0.1.2") (deps (list (crate-dep (name "json") (req "^0.11.8") (default-features #t) (kind 0)) (crate-dep (name "typed-arena") (req "^1.3.0") (default-features #t) (kind 0)))) (hash "02l1lardwi0kg3flmh5zzsg13ihjrvw5w6c4azzli94hw9s2m138")))

(define-public crate-flot-0.1 (crate (name "flot") (vers "0.1.3") (deps (list (crate-dep (name "json") (req "^0.11.8") (default-features #t) (kind 0)) (crate-dep (name "typed-arena") (req "^1.3.0") (default-features #t) (kind 0)))) (hash "0c08apyfsmfaqn5wja946ssha9rn9c6qszsjjhr7scf6shg9mxx3")))

(define-public crate-flotsam-0.1 (crate (name "flotsam") (vers "0.1.0") (hash "0j0dv2i612shz7lq27idakrvhmvc4y6j966yv4y8a5wqdadzv3qp")))

