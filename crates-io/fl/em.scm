(define-module (crates-io fl em) #:use-module (crates-io))

(define-public crate-flem-0.5 (crate (name "flem") (vers "0.5.0") (deps (list (crate-dep (name "heapless") (req "^0.7") (default-features #t) (kind 2)))) (hash "1mhc4g891sb1vq15mw3pqbyprcxbk2xyd7zji4w1hiwvxlknwqbp")))

(define-public crate-flem-0.5 (crate (name "flem") (vers "0.5.1") (deps (list (crate-dep (name "heapless") (req "^0.7") (default-features #t) (kind 2)))) (hash "11wxcz9dh56nv6vr8mrav7hxa3hdlf7am9ri52p019y9lphrbhj4")))

(define-public crate-flem-0.6 (crate (name "flem") (vers "0.6.0") (deps (list (crate-dep (name "heapless") (req "^0.7") (default-features #t) (kind 2)))) (hash "0prk9s0plw9lfs3hb5v03lgphppvpnmnxbv0yfyhc06krjhpjhmf")))

(define-public crate-flem-0.6 (crate (name "flem") (vers "0.6.1") (deps (list (crate-dep (name "heapless") (req "^0.7") (default-features #t) (kind 2)))) (hash "1nndcgzpxnk8jjssbv15yphbxal4w4p7vy2z8swzs36683n0x49n")))

(define-public crate-flem-0.6 (crate (name "flem") (vers "0.6.2") (deps (list (crate-dep (name "heapless") (req "^0.7") (default-features #t) (kind 2)))) (hash "03mvhyz9vw443m43whhz8ivpcbsl85z988hlsn5hklpkp2xb6nqa") (features (quote (("std") ("default"))))))

(define-public crate-flem-serial-rs-0.2 (crate (name "flem-serial-rs") (vers "0.2.0") (deps (list (crate-dep (name "flem") (req "^0.5") (default-features #t) (kind 0)) (crate-dep (name "serialport") (req "^4.2") (default-features #t) (kind 0)))) (hash "0qpwlmnfmii1f7l5q8zr2ykfh6p3h4ipbfw6p0g4npzr8vd657dc")))

(define-public crate-flem-serial-rs-0.2 (crate (name "flem-serial-rs") (vers "0.2.1") (deps (list (crate-dep (name "flem") (req "^0.5") (default-features #t) (kind 0)) (crate-dep (name "serialport") (req "^4.2") (default-features #t) (kind 0)))) (hash "12pvy5wh63ylk9ps8x1vr7yckafgc0vywyp07wir39jznyph33hw")))

(define-public crate-flem-serial-rs-0.2 (crate (name "flem-serial-rs") (vers "0.2.2") (deps (list (crate-dep (name "flem") (req "^0.6") (default-features #t) (kind 0)) (crate-dep (name "serialport") (req "^4.2") (default-features #t) (kind 0)))) (hash "189gqb33zh52haxi9dw0nkj8n3xyz4s9345y4v5ffmvj3lsm324m")))

(define-public crate-flem-serial-rs-0.3 (crate (name "flem-serial-rs") (vers "0.3.0") (deps (list (crate-dep (name "flem") (req "^0.6") (features (quote ("std"))) (default-features #t) (kind 0)) (crate-dep (name "serialport") (req "^4.2") (default-features #t) (kind 0)))) (hash "1f3h8dg5iza8749x9syjalzgbf1i23781bqa34rx7q7741mmhd6c")))

(define-public crate-flem-serial-rs-0.3 (crate (name "flem-serial-rs") (vers "0.3.1") (deps (list (crate-dep (name "flem") (req "^0.6") (features (quote ("std"))) (default-features #t) (kind 0)) (crate-dep (name "serialport") (req "^4.2") (default-features #t) (kind 0)))) (hash "1q14z9l39ci1713k6axydhgk058rsfgz8cb57g21b87b6ba445v6")))

(define-public crate-flem-serial-rs-0.3 (crate (name "flem-serial-rs") (vers "0.3.2") (deps (list (crate-dep (name "flem") (req "^0.6") (features (quote ("std"))) (default-features #t) (kind 0)) (crate-dep (name "serialport") (req "^4.2") (default-features #t) (kind 0)))) (hash "1jnq73qyq7p7miixgx86rrcy1kn8ar1a67223aag4mf1r1bpmhrw")))

(define-public crate-flemish-0.1 (crate (name "flemish") (vers "0.1.0") (deps (list (crate-dep (name "fltk") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "fltk-flex") (req "^0.1") (default-features #t) (kind 0)))) (hash "04zw00gli3z76fnspy9d4kyjdgvmibriqdp1m70wj8zn09n8hcb8")))

(define-public crate-flemish-0.1 (crate (name "flemish") (vers "0.1.1") (deps (list (crate-dep (name "fltk") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "fltk-flex") (req "^0.1") (default-features #t) (kind 0)))) (hash "1rlspnazvbg3kbj2520lm2mq57rcgmqd06m7smjca85d273s4kkm")))

(define-public crate-flemish-0.1 (crate (name "flemish") (vers "0.1.2") (deps (list (crate-dep (name "fltk") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "fltk-flex") (req "^0.1") (default-features #t) (kind 0)))) (hash "1pibwa704z11wgzz06jjcw3pn2d5bk4i2zd2q2ql35bf0fissfqh")))

(define-public crate-flemish-0.1 (crate (name "flemish") (vers "0.1.3") (deps (list (crate-dep (name "fltk") (req "^1") (features (quote ("enable-glwindow"))) (default-features #t) (kind 0)) (crate-dep (name "fltk-flex") (req "^0.1.3") (default-features #t) (kind 0)) (crate-dep (name "fltk-theme") (req "^0.1.8") (default-features #t) (kind 0)))) (hash "0i25nfgch7b50i65i2gnv3h58nihl6n9kb8r6wkacwx3km041chz")))

(define-public crate-flemish-0.2 (crate (name "flemish") (vers "0.2.0") (deps (list (crate-dep (name "fltk") (req "^1.2.7") (default-features #t) (kind 0)) (crate-dep (name "fltk-theme") (req "^0.4") (default-features #t) (kind 0)))) (hash "0brj9myhj6jwq8v628ykzp4k53x01k9y18abq4739gc5dwirdhy6")))

(define-public crate-flemish-0.2 (crate (name "flemish") (vers "0.2.1") (deps (list (crate-dep (name "fltk") (req "^1.2.7") (default-features #t) (kind 0)) (crate-dep (name "fltk-theme") (req "^0.4") (default-features #t) (kind 0)))) (hash "053l5kmw7ldarqsf7x0nxbiw0w6809wln0jq0xj264wwfzrqw6vv")))

(define-public crate-flemish-0.3 (crate (name "flemish") (vers "0.3.0") (deps (list (crate-dep (name "fltk") (req "^1.2.7") (default-features #t) (kind 0)) (crate-dep (name "fltk-theme") (req "^0.4") (default-features #t) (kind 0)))) (hash "0bsx1ydxf70srf9z6vvcnss5c24kb6rlm6my99pz33mryacwx9wa")))

(define-public crate-flemish-0.3 (crate (name "flemish") (vers "0.3.1") (deps (list (crate-dep (name "fltk") (req "^1.3.14") (default-features #t) (kind 0)) (crate-dep (name "fltk-theme") (req "^0.4") (default-features #t) (kind 0)))) (hash "1sjj8lihwzk4n80yvdvazskh78qhwzaz0rxplb1is76gm6d3yhzs")))

(define-public crate-flemish-0.4 (crate (name "flemish") (vers "0.4.0") (deps (list (crate-dep (name "fltk") (req "^1.4") (default-features #t) (kind 0)) (crate-dep (name "fltk-theme") (req "^0.7") (default-features #t) (kind 0)))) (hash "1i39fpmn4ixgszh4fnfhc2i4bpa6yp232qk9i3vccbnfay2xnljn")))

(define-public crate-flemish-0.4 (crate (name "flemish") (vers "0.4.1") (deps (list (crate-dep (name "fltk") (req "^1.4") (default-features #t) (kind 0)) (crate-dep (name "fltk-theme") (req "^0.7") (default-features #t) (kind 0)))) (hash "10w0p2lf75iazy8z988zf2ssf3b3ifnl950i3nkjd51mq01nv5ki")))

(define-public crate-flemish-0.4 (crate (name "flemish") (vers "0.4.2") (deps (list (crate-dep (name "fltk") (req "^1.4") (default-features #t) (kind 0)) (crate-dep (name "fltk-theme") (req "^0.7") (default-features #t) (kind 0)))) (hash "0a63f8n76qibpqwy5bgpgvznis2l5xjf1m13an819ll4h7y5z3v0")))

(define-public crate-flemish-0.4 (crate (name "flemish") (vers "0.4.3") (deps (list (crate-dep (name "fltk") (req "^1.4") (default-features #t) (kind 0)) (crate-dep (name "fltk-theme") (req "^0.7") (default-features #t) (kind 0)))) (hash "1ainfyjwk0p0rv98brfg159wwl8x0rv4afv542w4dp6dbm9fwa3z")))

(define-public crate-flemish-0.5 (crate (name "flemish") (vers "0.5.0") (deps (list (crate-dep (name "fltk") (req "^1.4") (default-features #t) (kind 0)) (crate-dep (name "fltk-theme") (req "^0.7") (default-features #t) (kind 0)))) (hash "1w3bc0jcra6xqikwv0ykldvvkmy1raamkd215amwbmjwp19ic2lc")))

(define-public crate-flemish-0.5 (crate (name "flemish") (vers "0.5.1") (deps (list (crate-dep (name "fltk") (req "^1.4") (default-features #t) (kind 0)) (crate-dep (name "fltk-theme") (req "^0.7") (default-features #t) (kind 0)))) (hash "0csbv9d4nl340x4ajapg48kywfr76cf409pp29i6pr2kc0xc7d5v")))

