(define-module (crates-io fl ou) #:use-module (crates-io))

(define-public crate-flou-0.1 (crate (name "flou") (vers "0.1.0") (deps (list (crate-dep (name "nom") (req "^7.0.0") (default-features #t) (kind 0)) (crate-dep (name "nom-supreme") (req "^0.6.0") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2.14") (default-features #t) (kind 0)) (crate-dep (name "pretty_assertions") (req "^1.0.0") (default-features #t) (kind 2)))) (hash "0dk06crgvip51qn5w42j42gp0pp0in7vwa0ncmhanifsnqwzxf1c")))

(define-public crate-flou_cli-0.1 (crate (name "flou_cli") (vers "0.1.0") (deps (list (crate-dep (name "flou") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.3.25") (default-features #t) (kind 0)))) (hash "0h2id2p6qwdcj9qmwmbgk3g0kjywkkwnpp0c2qrhkmbm0h241n6q")))

(define-public crate-floui-0.1 (crate (name "floui") (vers "0.1.0") (deps (list (crate-dep (name "floui-sys") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.4") (default-features #t) (kind 0)))) (hash "02217jlbv5jqqv670jz8kjsh3clqi23bfh7y1mh0ck8h0jr9y5py")))

(define-public crate-floui-0.1 (crate (name "floui") (vers "0.1.1") (deps (list (crate-dep (name "floui-sys") (req "=0.1.2") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.4") (default-features #t) (kind 0)))) (hash "16bjzxmgszi883xbcya0z1c6mm18ybv90wph4fsg1xj0s8wiph3v") (features (quote (("ios-webview" "floui-sys/ios-webview"))))))

(define-public crate-floui-0.1 (crate (name "floui") (vers "0.1.2") (deps (list (crate-dep (name "floui-sys") (req "=0.1.2") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.4") (default-features #t) (kind 0)))) (hash "046rnyw6ql769af05v2a7adhgaci6s2qvsdixs5817k26qb4wgc6") (features (quote (("ios-webview" "floui-sys/ios-webview"))))))

(define-public crate-floui-0.1 (crate (name "floui") (vers "0.1.3") (deps (list (crate-dep (name "floui-sys") (req "=0.1.2") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.4") (default-features #t) (kind 0)))) (hash "05jg0s3xcd0pmm2w14irwspn9hdlrfm4ynp52c3cshhfnl2c0apj") (features (quote (("ios-webview" "floui-sys/ios-webview"))))))

(define-public crate-floui-0.1 (crate (name "floui") (vers "0.1.4") (deps (list (crate-dep (name "floui-sys") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.4") (default-features #t) (kind 0)))) (hash "0ns5k5v5nzbn3gdrqwf7g53zdsh7wcwcd2xgza9c98aj5kik9jy9") (features (quote (("ios-webview" "floui-sys/ios-webview"))))))

(define-public crate-floui-0.1 (crate (name "floui") (vers "0.1.5") (deps (list (crate-dep (name "floui-sys") (req "=0.1.4") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.4") (default-features #t) (kind 0)))) (hash "1yi4mfhf40kb6ynbhvvg2wkzrzwbz2j8nqhdx0s1z81zzvdg9vpz") (features (quote (("ios-webview" "floui-sys/ios-webview"))))))

(define-public crate-floui-sys-0.1 (crate (name "floui-sys") (vers "0.1.0") (deps (list (crate-dep (name "cc") (req "^1") (default-features #t) (target "cfg(not(android))") (kind 1)))) (hash "16a4zzv89lrmk8003avcawfbs4444kf3ma84hq8rn9m7az4xzx6p")))

(define-public crate-floui-sys-0.1 (crate (name "floui-sys") (vers "0.1.1") (deps (list (crate-dep (name "cc") (req "^1") (default-features #t) (target "cfg(not(android))") (kind 1)))) (hash "1i8shdywfkms8x5qhai415bg7z5jkhw5xz9zp8d9ww44g6hi9ql8")))

(define-public crate-floui-sys-0.1 (crate (name "floui-sys") (vers "0.1.2") (deps (list (crate-dep (name "cc") (req "^1") (default-features #t) (target "cfg(not(android))") (kind 1)))) (hash "1mvqlh9inq7bvm9jcks5xbadkdi0q2bd869gr8k44y7hinrr1952") (features (quote (("ios-webview"))))))

(define-public crate-floui-sys-0.1 (crate (name "floui-sys") (vers "0.1.3") (deps (list (crate-dep (name "cc") (req "^1") (default-features #t) (target "cfg(not(android))") (kind 1)))) (hash "0nnfvnj2dfp0gblzrh54ghxcrj90cw97lwbaaxnq5ws08940xfgy") (features (quote (("ios-webview"))))))

(define-public crate-floui-sys-0.1 (crate (name "floui-sys") (vers "0.1.4") (deps (list (crate-dep (name "cc") (req "^1") (default-features #t) (target "cfg(not(android))") (kind 1)))) (hash "1yi82jzb8fxs209p20m2yr4rcjfm5k4y2ay0ild61jjzfaa6rv4x") (features (quote (("ios-webview"))))))

(define-public crate-flounder-0.0.0 (crate (name "flounder") (vers "0.0.0") (hash "0xdf5q16pvq4468xacz2y1y2siz7g59qlsbya9ynfaqcc8a677i1")))

(define-public crate-flour-1 (crate (name "flour") (vers "1.0.0") (deps (list (crate-dep (name "bytestream") (req "^0.4.0") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^3.1.6") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.136") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0.136") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.79") (default-features #t) (kind 0)))) (hash "1chnxi547gr4zyabd5r66cby3gx0xpxmkfqksj1pipyz2k3inz50")))

(define-public crate-flour-2 (crate (name "flour") (vers "2.0.0-pre1") (deps (list (crate-dep (name "bytestream") (req "^0.4.0") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^3.1.6") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "encoding_rs") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "semver") (req "^1.0.7") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.136") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde-semver") (req "^0.2.1") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.79") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0") (default-features #t) (kind 0)))) (hash "0h3hmj7v0f4h4zjpvdd148ibjmw41wblvp7anqnin88hdnhqy8wy")))

(define-public crate-flour-2 (crate (name "flour") (vers "2.0.0") (deps (list (crate-dep (name "bytestream") (req "^0.4.0") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^3.1.6") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "encoding_rs") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "json_comments") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "semver") (req "^1.0.7") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.136") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde-semver") (req "^0.2.1") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.79") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0") (default-features #t) (kind 0)))) (hash "0rw45sdk8bkzbncimh20rkhxxz7xd42hlzgcf5rbfjp9m67w11va") (features (quote (("modder_qol") ("default" "modder_qol")))) (v 2) (features2 (quote (("clap" "dep:clap"))))))

