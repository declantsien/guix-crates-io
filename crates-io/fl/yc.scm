(define-module (crates-io fl yc) #:use-module (crates-io))

(define-public crate-flycatcher-0.1 (crate (name "flycatcher") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^2.33.3") (default-features #t) (kind 0)) (crate-dep (name "flycatcher-diagnostic") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "flycatcher-parser") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "13hi2bbwymmwqz3b5i98qz2q22fngz6h25y5r2b5fzk52cdjvb7x")))

(define-public crate-flycatcher-0.1 (crate (name "flycatcher") (vers "0.1.1") (deps (list (crate-dep (name "clap") (req "^2.33.3") (default-features #t) (kind 0)) (crate-dep (name "flycatcher-diagnostic") (req "^0.1.1") (default-features #t) (kind 0)) (crate-dep (name "flycatcher-parser") (req "^0.1.1") (default-features #t) (kind 0)) (crate-dep (name "flycatcherc") (req "^0.1.1") (default-features #t) (kind 0)) (crate-dep (name "flycatcherc-clif") (req "^0.1.1") (default-features #t) (kind 0)) (crate-dep (name "flycatcherc-link") (req "^0.1.1") (default-features #t) (kind 0)))) (hash "1hwpxazs00hfc92awy3a6sjmm1x9skdawbjjrblkrfp78ym5r6wn")))

(define-public crate-flycatcher-diagnostic-0.1 (crate (name "flycatcher-diagnostic") (vers "0.1.0") (deps (list (crate-dep (name "codespan-reporting") (req "^0.11.1") (default-features #t) (kind 0)))) (hash "1z4ngkhl7bpaia9lm9qlkdaqg0y2qrf2yvipvvmdgw5vx0v6qswi")))

(define-public crate-flycatcher-diagnostic-0.1 (crate (name "flycatcher-diagnostic") (vers "0.1.1") (deps (list (crate-dep (name "codespan-reporting") (req "^0.11.1") (default-features #t) (kind 0)))) (hash "0m7g6f9ri9yhy4izkn1czyi30kr1cjsrcjzkpv4dr5cc15gbm77v")))

(define-public crate-flycatcher-lexer-0.1 (crate (name "flycatcher-lexer") (vers "0.1.0") (deps (list (crate-dep (name "logos") (req "^0.12.0") (default-features #t) (kind 0)))) (hash "1gbvx1vrrpqwkz2jy466x3qi4kfkmhwvfrygxvgqkjvp0zfq2asn")))

(define-public crate-flycatcher-lexer-0.1 (crate (name "flycatcher-lexer") (vers "0.1.1") (deps (list (crate-dep (name "logos") (req "^0.12.0") (default-features #t) (kind 0)))) (hash "0wnp0iccwr3gyq305fzajws02sqbp5xfr6lkl37b5yvi1cz2a5bw")))

(define-public crate-flycatcher-parser-0.1 (crate (name "flycatcher-parser") (vers "0.1.0") (deps (list (crate-dep (name "codespan-reporting") (req "^0.11.1") (default-features #t) (kind 0)) (crate-dep (name "flycatcher-lexer") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "0izddh0w2fxrg6dg8iyi9b0digfikrd0541kl5awc74lq0f0aq92")))

(define-public crate-flycatcher-parser-0.1 (crate (name "flycatcher-parser") (vers "0.1.1") (deps (list (crate-dep (name "codespan-reporting") (req "^0.11.1") (default-features #t) (kind 0)) (crate-dep (name "flycatcher-lexer") (req "^0.1.1") (default-features #t) (kind 0)))) (hash "1c133y9bgkyyazr06vdgjx9l7jyjixaba70jjkadjl5zvdjm0f58")))

(define-public crate-flycatcherc-0.1 (crate (name "flycatcherc") (vers "0.1.1") (deps (list (crate-dep (name "flycatcher-diagnostic") (req "^0.1.1") (default-features #t) (kind 0)) (crate-dep (name "flycatcher-parser") (req "^0.1.1") (default-features #t) (kind 0)))) (hash "1zqbq5hfk42d08gmn54k0bx1mbqprzkp2hzd675539qnaax4f9cs")))

(define-public crate-flycatcherc-clif-0.1 (crate (name "flycatcherc-clif") (vers "0.1.1") (deps (list (crate-dep (name "cranelift") (req "^0.76.0") (default-features #t) (kind 0)) (crate-dep (name "cranelift-codegen") (req "^0.76.0") (default-features #t) (kind 0)) (crate-dep (name "cranelift-frontend") (req "^0.76.0") (default-features #t) (kind 0)) (crate-dep (name "cranelift-module") (req "^0.76.0") (default-features #t) (kind 0)) (crate-dep (name "cranelift-object") (req "^0.76.0") (default-features #t) (kind 0)) (crate-dep (name "flycatcherc") (req "^0.1.1") (default-features #t) (kind 0)) (crate-dep (name "target-lexicon") (req "^0.12") (default-features #t) (kind 0)))) (hash "151g9nd9v5wp1fx007h977axj8babi5fgk73jfdmah2vcmdyjx5j")))

(define-public crate-flycatcherc-link-0.1 (crate (name "flycatcherc-link") (vers "0.1.1") (hash "10r53a5qx03dq3jxg4kamxcbk5z5w9sxrrzmpkbihm1fmpzy38hp") (features (quote (("gcc") ("default" "gcc"))))))

