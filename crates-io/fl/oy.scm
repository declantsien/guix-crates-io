(define-module (crates-io fl oy) #:use-module (crates-io))

(define-public crate-floyd-warshall-0.0.0 (crate (name "floyd-warshall") (vers "0.0.0") (deps (list (crate-dep (name "petgraph") (req "^0.4.10") (default-features #t) (kind 0)))) (hash "132939vmysav4mc523m8dd8r7bdi7747la727lbvwrk8hk6760xi")))

(define-public crate-floyd-warshall-0.0.1 (crate (name "floyd-warshall") (vers "0.0.1") (deps (list (crate-dep (name "petgraph") (req "^0.4.10") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.3.17") (default-features #t) (kind 2)) (crate-dep (name "text_io") (req "^0.1.6") (default-features #t) (kind 2)))) (hash "1plhay2i0ysq660zlws6hp4f1skq6jprpgz8s09rsghjyv9p996j")))

(define-public crate-floyd-warshall-0.0.2 (crate (name "floyd-warshall") (vers "0.0.2") (deps (list (crate-dep (name "petgraph") (req "^0.4.10") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.3.17") (default-features #t) (kind 2)) (crate-dep (name "text_io") (req "^0.1.6") (default-features #t) (kind 2)))) (hash "102yfmpm9q87n7jwgb1sg10mi621wi1jr8jr6jvn6igjim7i0xdq")))

(define-public crate-floyd-warshall-0.0.3 (crate (name "floyd-warshall") (vers "0.0.3") (deps (list (crate-dep (name "petgraph") (req "^0.4.10") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.3.17") (default-features #t) (kind 2)) (crate-dep (name "text_io") (req "^0.1.6") (default-features #t) (kind 2)))) (hash "1ad8gzvcl9i20k051nj9hbag5k4dmwc2wxdw0bqzf3h5zlc5yhx6")))

(define-public crate-floyd-warshall-alg-0.1 (crate (name "floyd-warshall-alg") (vers "0.1.2") (deps (list (crate-dep (name "num-traits") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "safe-graph") (req "^0.1.4") (default-features #t) (kind 0)))) (hash "0h97g46iy2qfppx8wqjz6x44l4a7x6h5da8rg8m3y8j6p4pjszwm")))

(define-public crate-floyd-warshall-alg-0.1 (crate (name "floyd-warshall-alg") (vers "0.1.3") (deps (list (crate-dep (name "num-traits") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "safe-graph") (req "^0.1.4") (default-features #t) (kind 0)))) (hash "1a4zrqw7rksjdwnjbk8p57l2145sy8byczl17gzqm4841aq5xby7")))

(define-public crate-floydrivest-0.1 (crate (name "floydrivest") (vers "0.1.0") (hash "0j495500acbp5fmmg1lydi6mi6qy3nbb90c9bc8gnzdxj51gp9h4")))

(define-public crate-floydrivest-0.1 (crate (name "floydrivest") (vers "0.1.1") (hash "1q9crzx560jb2cxd2z8l1bj98jzw64kfgl6kp05r20ndjiw64pj1")))

(define-public crate-floydrivest-0.1 (crate (name "floydrivest") (vers "0.1.2") (hash "0y7l8cyfi46jj2ybbmz9kapdjwdx08fc1cyskkacbfnjb50qc03q")))

(define-public crate-floydrivest-0.1 (crate (name "floydrivest") (vers "0.1.3") (hash "0iy830vr2kaww23xs4qk47nsq9c50ydfbmjb0rf2i597pkhr2vnz") (yanked #t)))

(define-public crate-floydrivest-0.2 (crate (name "floydrivest") (vers "0.2.0") (hash "0v9gsmnh6a9m90p04mrr10nncnjy45ixh5acga9znmf05py65ms3") (yanked #t)))

(define-public crate-floydrivest-0.2 (crate (name "floydrivest") (vers "0.2.1") (hash "12sk5x5z9amx3hrrwghci5mcdda922ahqfk1zzgpr9m5110w0dp8")))

(define-public crate-floydrivest-0.2 (crate (name "floydrivest") (vers "0.2.2") (deps (list (crate-dep (name "criterion") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "kth") (req "^0.1.0") (default-features #t) (kind 2)) (crate-dep (name "order-stat") (req "^0.1.3") (default-features #t) (kind 2)) (crate-dep (name "pdqselect") (req "^0.1.0") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.7.3") (default-features #t) (kind 2)))) (hash "1gy8w1pd9mxrzgvkfq3br58b2vhfc75k1s5mr8kqr9dxdz9dwcwm")))

(define-public crate-floydrivest-0.2 (crate (name "floydrivest") (vers "0.2.3") (deps (list (crate-dep (name "criterion") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "kth") (req "^0.1.0") (default-features #t) (kind 2)) (crate-dep (name "order-stat") (req "^0.1.3") (default-features #t) (kind 2)) (crate-dep (name "pdqselect") (req "^0.1.0") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.7.3") (default-features #t) (kind 2)))) (hash "11k77mcvysv29hcg2yhjljjwp7hr7bddhszqsj21vfz24yv7r0aj")))

(define-public crate-floydrivest-0.2 (crate (name "floydrivest") (vers "0.2.4") (deps (list (crate-dep (name "criterion") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "kth") (req "^0.1.0") (default-features #t) (kind 2)) (crate-dep (name "order-stat") (req "^0.1.3") (default-features #t) (kind 2)) (crate-dep (name "pdqselect") (req "^0.1.0") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.7.3") (default-features #t) (kind 2)))) (hash "02hfs35s4as5z9zi00pzxlc82vmqjb2xlkgbnwqbg841dr1bcf3n")))

