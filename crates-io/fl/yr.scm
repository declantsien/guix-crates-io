(define-module (crates-io fl yr) #:use-module (crates-io))

(define-public crate-flyr-0.4 (crate (name "flyr") (vers "0.4.0") (deps (list (crate-dep (name "binread") (req "^1.1.1") (default-features #t) (kind 0)) (crate-dep (name "image") (req "^0.23.1") (features (quote ("png"))) (kind 0)) (crate-dep (name "ndarray") (req "^0.13.0") (default-features #t) (kind 0)))) (hash "10fflwaickdw4gwv1b6fg24qsmkl910bvl7bq45zxawx3w1hziwc")))

(define-public crate-flyr-0.4 (crate (name "flyr") (vers "0.4.1") (deps (list (crate-dep (name "binread") (req "^1.1.1") (default-features #t) (kind 0)) (crate-dep (name "image") (req "^0.23.1") (features (quote ("png"))) (kind 0)) (crate-dep (name "ndarray") (req "^0.13.0") (default-features #t) (kind 0)))) (hash "0cifdz7c741rb9yly8v72kwixl2wz3c8hsv04pmyd43w2nrqqm9v")))

(define-public crate-flyr-0.4 (crate (name "flyr") (vers "0.4.2") (deps (list (crate-dep (name "binread") (req "^1.1.1") (default-features #t) (kind 0)) (crate-dep (name "image") (req "^0.23.1") (features (quote ("png"))) (kind 0)) (crate-dep (name "ndarray") (req "^0.13.0") (default-features #t) (kind 0)))) (hash "1q0d6r3f5yaj78g8ywfwrbcpl60zyba2wcf7pxa9j29jxdnnxiqw")))

(define-public crate-flyr-0.5 (crate (name "flyr") (vers "0.5.0") (deps (list (crate-dep (name "binread") (req "^2.2.0") (default-features #t) (kind 0)) (crate-dep (name "image") (req "^0.23.1") (features (quote ("png" "jpeg"))) (kind 0)) (crate-dep (name "ndarray") (req "^0.15.4") (default-features #t) (kind 0)))) (hash "1cp09f0938fdr65x69w1705mqjfwn7mrr48w3j9rdj7bjdxppg6x")))

