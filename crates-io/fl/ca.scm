(define-module (crates-io fl ca) #:use-module (crates-io))

(define-public crate-flcard-1 (crate (name "flcard") (vers "1.0.1") (deps (list (crate-dep (name "anyhow") (req "^1.0.70") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^4.2.1") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "clap_complete") (req "^4.2.0") (default-features #t) (kind 0)) (crate-dep (name "colored") (req "^2.0.0") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)))) (hash "19xb27imvvf326yvdascxl8bn1y18fdh5435gp4v2i7hwhsj0fgw")))

