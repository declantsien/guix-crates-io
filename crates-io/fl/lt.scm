(define-module (crates-io fl lt) #:use-module (crates-io))

(define-public crate-flltr-0.2 (crate (name "flltr") (vers "0.2.1") (deps (list (crate-dep (name "colored") (req "^2.0.0") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.3.26") (kind 0)))) (hash "03nflvw68jy8icaf4w4w32zbwyrrxmvvkak0nxblqs4qcldnpk9n")))

