(define-module (crates-io i- ma) #:use-module (crates-io))

(define-public crate-i-macros-1 (crate (name "i-macros") (vers "1.0.0-beta.4") (deps (list (crate-dep (name "i-codegen") (req "^1.0.0-beta.3") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "05y4jr24ivl0nlm1i63vmcipb9p76zxlw290z9cxv9m3dwk8js16") (features (quote (("custom-protocol"))))))

