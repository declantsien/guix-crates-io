(define-module (crates-io i- lo) #:use-module (crates-io))

(define-public crate-i-love-jesus-0.1 (crate (name "i-love-jesus") (vers "0.1.0") (deps (list (crate-dep (name "diesel") (req "^2.1") (features (quote ("postgres_backend"))) (default-features #t) (kind 0)) (crate-dep (name "i-love-jesus-macros") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "116ccckdzgrgk1vwcsx976dizx8maa9clv6rav62j58y53in1yir")))

(define-public crate-i-love-jesus-macros-0.1 (crate (name "i-love-jesus-macros") (vers "0.1.0") (deps (list (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2.0") (default-features #t) (kind 0)))) (hash "08h3vprmclip98rx0bxa69irdm8d5nm4by0j7102kf7rhfgjf5c2")))

