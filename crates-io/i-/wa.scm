(define-module (crates-io i- wa) #:use-module (crates-io))

(define-public crate-i-wanna-build-0.1 (crate (name "i-wanna-build") (vers "0.1.0") (deps (list (crate-dep (name "quick-xml") (req "^0.31.0") (features (quote ("serialize"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.192") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "xml-rs") (req "^0.8.19") (default-features #t) (kind 0)))) (hash "0x2ny9f5fnfgzxll5z0c94xd3rc0p3awkz9q90j2cxbll06b0x23")))

