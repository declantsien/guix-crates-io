(define-module (crates-io i- kd) #:use-module (crates-io))

(define-public crate-i-kdl-1 (crate (name "i-kdl") (vers "1.0.0") (deps (list (crate-dep (name "kdl") (req "^1.0.0") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.22") (default-features #t) (kind 0)))) (hash "00mqfkg05j222cmnn93y8qzl3zn4i0616y8p3cipca3jyw77ggg3")))

