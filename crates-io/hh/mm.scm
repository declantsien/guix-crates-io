(define-module (crates-io hh mm) #:use-module (crates-io))

(define-public crate-hhmmss-0.1 (crate (name "hhmmss") (vers "0.1.0") (deps (list (crate-dep (name "chrono") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "time") (req "^0.2") (default-features #t) (kind 0)))) (hash "0qk0d9l3gv8vhlf1ml9ixa8pjxhr0ij0hqd613qixc3cj78ag8qi")))

