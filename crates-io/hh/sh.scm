(define-module (crates-io hh sh) #:use-module (crates-io))

(define-public crate-hhsh-0.1 (crate (name "hhsh") (vers "0.1.0") (deps (list (crate-dep (name "cli-table") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "futures") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "surf") (req "^2.2.0") (default-features #t) (kind 0)))) (hash "0y6wrh6b2hxpav8rvxgiyjka8qqc1lclsa6y2427wb9y18k4sx6i")))

