(define-module (crates-io hh -h) #:use-module (crates-io))

(define-public crate-hh-highligh-0.1 (crate (name "hh-highligh") (vers "0.1.0") (deps (list (crate-dep (name "colored") (req "^2.1.0") (default-features #t) (kind 0)) (crate-dep (name "env_logger") (req "^0.11.1") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.20") (default-features #t) (kind 0)))) (hash "0armsfq7z0z26gq4bykr28ylp1lzccj0v9by50wg8kydf8ff65sm")))

(define-public crate-hh-highlight-0.1 (crate (name "hh-highlight") (vers "0.1.0") (deps (list (crate-dep (name "colored") (req "^2.1.0") (default-features #t) (kind 0)) (crate-dep (name "env_logger") (req "^0.11.1") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.20") (default-features #t) (kind 0)))) (hash "0mkrb2sfpmj4km8ng7xqzn9375vvk2v5ai2r9i87mnfg2q17sj56")))

