(define-module (crates-io r9 ir) #:use-module (crates-io))

(define-public crate-r9ir-0.1 (crate (name "r9ir") (vers "0.1.1") (hash "0wbs8cpsgjy5dkv9931ym5p0npdf4fq14dg4qw6mymgwsfmg8w1a") (yanked #t)))

(define-public crate-r9ir-1 (crate (name "r9ir") (vers "1.0.0") (hash "05wpfsx9qkyw34q0h4mz4l16apl65cwc0w6syc6f1bs2bzw6qv6w")))

