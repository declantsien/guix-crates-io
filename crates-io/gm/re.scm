(define-module (crates-io gm re) #:use-module (crates-io))

(define-public crate-gmres-0.1 (crate (name "gmres") (vers "0.1.0") (deps (list (crate-dep (name "rsparse") (req "^0.2.3") (default-features #t) (kind 0)))) (hash "01aygrs87b54vz7fjrp6m9pai52b1jx1r2nhj4pczizqq3vfsqnv")))

(define-public crate-gmres-0.1 (crate (name "gmres") (vers "0.1.1") (deps (list (crate-dep (name "rsparse") (req "^0.2.3") (default-features #t) (kind 0)))) (hash "17rvxrsbp6lxcqw9cj8knnl2qc0shdcyiv1s7g6kjrasjpk84dzf")))

(define-public crate-gmres-0.1 (crate (name "gmres") (vers "0.1.2") (deps (list (crate-dep (name "rsparse") (req "^0.2.3") (default-features #t) (kind 0)))) (hash "17df2gs133sfvi9aav0gb7z5qwvwj99bc1l4dmx3m21pd0g22ngj")))

(define-public crate-gmres-1 (crate (name "gmres") (vers "1.0.0") (deps (list (crate-dep (name "rsparse") (req "^1.0.0") (default-features #t) (kind 0)))) (hash "1wfqlffm23zry7mzwqlhhzhmal2n8dxsaxz39dfrq80m6kv9ihpz")))

