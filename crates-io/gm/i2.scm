(define-module (crates-io gm i2) #:use-module (crates-io))

(define-public crate-gmi2html-0.1 (crate (name "gmi2html") (vers "0.1.6") (deps (list (crate-dep (name "criterion") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "url") (req "^2.1.1") (default-features #t) (kind 0)))) (hash "05q4dl65j31knlqz499v5sz5x0qpd76x4qkpybk6ww3941sx9fl1")))

(define-public crate-gmi2html-0.1 (crate (name "gmi2html") (vers "0.1.7") (deps (list (crate-dep (name "url") (req "^2.2.2") (default-features #t) (kind 0)))) (hash "0alr08clf89ylan4f10qrhvlvr4vdd836c1pfq2g1k3d68bq3vni")))

(define-public crate-gmi2html-0.1 (crate (name "gmi2html") (vers "0.1.8") (deps (list (crate-dep (name "url") (req "^2.2.2") (default-features #t) (kind 0)))) (hash "0p9vxk6vc0l0aj0dbdb4n1vd5s3pg69bnx5r4d19ss70k7pjzyip")))

