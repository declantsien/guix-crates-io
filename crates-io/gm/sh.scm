(define-module (crates-io gm sh) #:use-module (crates-io))

(define-public crate-gmsh-sys-0.1 (crate (name "gmsh-sys") (vers "0.1.0") (deps (list (crate-dep (name "pkg-config") (req "^0.3.16") (default-features #t) (kind 1)))) (hash "1931nzlclpdxy9k8qs5nyf6mdbd29z04sv73rzi76b8n43iq7c6x") (links "gmsh")))

(define-public crate-gmsh-sys-0.1 (crate (name "gmsh-sys") (vers "0.1.1") (deps (list (crate-dep (name "pkg-config") (req "^0.3.16") (default-features #t) (kind 1)))) (hash "13mkcq4ckpkz3rm6cs7xr5z3rry7lisjv2zbxy8mkv4x9prp3dvr") (links "gmsh")))

(define-public crate-gmsh-sys-0.1 (crate (name "gmsh-sys") (vers "0.1.2") (deps (list (crate-dep (name "pkg-config") (req "^0.3.16") (default-features #t) (kind 1)))) (hash "0rq7xvmyf4cvfq58wpv2gib0cwx4b1qgadlnny8lvwrm9yivnd26") (links "gmsh")))

