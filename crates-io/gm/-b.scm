(define-module (crates-io gm -b) #:use-module (crates-io))

(define-public crate-gm-boilerplate-0.1 (crate (name "gm-boilerplate") (vers "0.1.0") (deps (list (crate-dep (name "futures") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "glitch-in-the-matrix") (req "^0.13") (default-features #t) (kind 0)) (crate-dep (name "tokio-core") (req "^0.1") (default-features #t) (kind 0)))) (hash "0apf4ip7jvr5w7hchr6hjiqnki5mngpwfnilbn2sxxqarfycwh6v")))

