(define-module (crates-io gm ag) #:use-module (crates-io))

(define-public crate-gmagick-0.1 (crate (name "gmagick") (vers "0.1.1") (deps (list (crate-dep (name "bindgen") (req "^0.30") (default-features #t) (kind 1)) (crate-dep (name "failure") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "failure_derive") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2.33") (default-features #t) (kind 0)) (crate-dep (name "pkg-config") (req "^0.3.9") (default-features #t) (kind 1)))) (hash "0ll9ajvmnclr2cqb3kkfkj029wzcb55qnqafqliicjq7id5ckdzl")))

