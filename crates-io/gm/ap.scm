(define-module (crates-io gm ap) #:use-module (crates-io))

(define-public crate-gmaps-static-0.0.1 (crate (name "gmaps-static") (vers "0.0.1") (deps (list (crate-dep (name "base64") (req "^0.13.0") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "querystring") (req "^1.1.0") (default-features #t) (kind 2)) (crate-dep (name "ring") (req "^0.16.20") (default-features #t) (kind 0)) (crate-dep (name "url") (req "^2.2.2") (default-features #t) (kind 0)))) (hash "157ahj4nyvrjy6418piaq6nvwx5sq7j7h85v7ysrpa40fig2pnc9")))

