(define-module (crates-io gm ac) #:use-module (crates-io))

(define-public crate-gmac-0.0.0 (crate (name "gmac") (vers "0.0.0") (hash "0gqkd4dnnak96zskmz7xhpwxjp0hl9gzf4axpcsnidjhigwlrni8")))

(define-public crate-gmacro-0.0.0 (crate (name "gmacro") (vers "0.0.0") (hash "070snmqq6schmzvrl5z3rqhxwh1fpdfq6lv9lfn8ss9g7rb9sx2a") (yanked #t)))

