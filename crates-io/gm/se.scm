(define-module (crates-io gm se) #:use-module (crates-io))

(define-public crate-gmserverplugin-1 (crate (name "gmserverplugin") (vers "1.0.0") (deps (list (crate-dep (name "gmod") (req "^8") (features (quote ("hax"))) (kind 0)))) (hash "0dmkchxxzs2ibn79rlpk8mh46cpmgzlh1jdk830wj2kxhv7ca7a8")))

(define-public crate-gmserverplugin-2 (crate (name "gmserverplugin") (vers "2.0.0") (deps (list (crate-dep (name "gmod") (req "^12.0.1") (features (quote ("hax"))) (kind 0)))) (hash "0a5qkcv42ds1jymgnzmw2xrsh32g9j0qmv40p72fhvhxy87bs7v7")))

(define-public crate-gmserverplugin-2 (crate (name "gmserverplugin") (vers "2.0.1") (deps (list (crate-dep (name "gmod") (req "^14.0.1") (features (quote ("hax"))) (kind 0)))) (hash "1f9b6p62hy9jmsxi1pm22b5j4rwrzn9938q1pibpl0i89hc8yq00")))

