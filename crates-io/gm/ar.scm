(define-module (crates-io gm ar) #:use-module (crates-io))

(define-public crate-gmarkov-0.0.1 (crate (name "gmarkov") (vers "0.0.1") (hash "14q1dmgscw1zpfw4jsfipmbcr5fddrg6v1dx8j70by7a2phm2rvv")))

(define-public crate-gmarkov-1 (crate (name "gmarkov") (vers "1.0.0") (deps (list (crate-dep (name "clap") (req "^2.33.0") (default-features #t) (kind 0)) (crate-dep (name "gmarkov-lib") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "0nxcapp1qpfkxfibv0143sbng177amfhr8s4475yhz1h656gc2yl")))

(define-public crate-gmarkov-lib-0.1 (crate (name "gmarkov-lib") (vers "0.1.0") (deps (list (crate-dep (name "rand") (req "^0.6.5") (default-features #t) (kind 0)))) (hash "0cjizcf35ricci86a9d6b1s9hn4xaf4rfiyndw28s4g57qnz121g")))

(define-public crate-gmarkov-lib-0.1 (crate (name "gmarkov-lib") (vers "0.1.1") (deps (list (crate-dep (name "rand") (req "^0.6.5") (default-features #t) (kind 0)))) (hash "1k26jay65dhp4wwfmjrykij251cx618xmhqgrgvqvl1dpqmyhr7k")))

