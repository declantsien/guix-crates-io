(define-module (crates-io gm -f) #:use-module (crates-io))

(define-public crate-gm-ffi-0.1 (crate (name "gm-ffi") (vers "0.1.0") (deps (list (crate-dep (name "cstr_core") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "cty") (req "^0.2") (default-features #t) (kind 0)))) (hash "1pkpqfw0yl6k0lw29zfv33wslv6sqbn4wsmcvy0lm5zlapxkghhg")))

(define-public crate-gm-ffi-0.2 (crate (name "gm-ffi") (vers "0.2.0") (hash "04ichgr8xfh32w69a9h6fnhpby8ck2dv100b9rd683z7zpzqqr87")))

(define-public crate-gm-ffi-0.2 (crate (name "gm-ffi") (vers "0.2.1") (deps (list (crate-dep (name "interprocess") (req "^1.2.1") (default-features #t) (target "cfg(not(windows))") (kind 0)) (crate-dep (name "once_cell") (req "^1.17.1") (default-features #t) (target "cfg(not(windows))") (kind 0)) (crate-dep (name "parking_lot") (req "^0.12.1") (default-features #t) (target "cfg(not(windows))") (kind 0)))) (hash "13dfivgxv6awc1qyakvbjbp2ay8lyf4r12x1ykcrlzcqwc2pxsnp")))

(define-public crate-gm-ffi-0.2 (crate (name "gm-ffi") (vers "0.2.2") (deps (list (crate-dep (name "interprocess") (req "^1.2.1") (default-features #t) (target "cfg(not(windows))") (kind 0)) (crate-dep (name "once_cell") (req "^1.17.1") (default-features #t) (target "cfg(not(windows))") (kind 0)) (crate-dep (name "parking_lot") (req "^0.12.1") (default-features #t) (target "cfg(not(windows))") (kind 0)))) (hash "1bvr180aajpr47fl7rrppq8v2bpmn1342lj2cvwz4kf7y9lwg01d")))

