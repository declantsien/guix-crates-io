(define-module (crates-io gm pr) #:use-module (crates-io))

(define-public crate-gmprs-0.1 (crate (name "gmprs") (vers "0.1.0") (deps (list (crate-dep (name "bindgen") (req "^0.69.1") (default-features #t) (kind 1)))) (hash "055arl6hdzg4g646micp9ap3jmynnj0n50mlwsgnpg8q5lgw1lrw") (yanked #t)))

(define-public crate-gmprs-0.1 (crate (name "gmprs") (vers "0.1.1") (deps (list (crate-dep (name "bindgen") (req "^0.69.1") (default-features #t) (kind 1)))) (hash "03n7pwcagzqlzjck2h66fvs971l5jqcz54s9jcglcv8cwa4k1rpq") (yanked #t)))

