(define-module (crates-io gm -t) #:use-module (crates-io))

(define-public crate-gm-types-0.1 (crate (name "gm-types") (vers "0.1.0") (deps (list (crate-dep (name "error-chain") (req "^0.11.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.19") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0.19") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.6") (default-features #t) (kind 0)))) (hash "1jn2167wwvsrj2qwscrsbiggz0mn0q4gvr72mwybzr470ic4wpy7")))

(define-public crate-gm-types-0.2 (crate (name "gm-types") (vers "0.2.0") (deps (list (crate-dep (name "error-chain") (req "^0.11.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.19") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0.19") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.6") (default-features #t) (kind 0)))) (hash "1q311piqdwhn427w58l9fhmgyqx072icn7brafaz4psac1fhbz6j")))

(define-public crate-gm-types-0.3 (crate (name "gm-types") (vers "0.3.0") (deps (list (crate-dep (name "error-chain") (req "^0.11") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)))) (hash "01ljlglmb1p1q898xrnl4xlk4ahqy1nzsazccv32548svj9566ay")))

(define-public crate-gm-types-0.4 (crate (name "gm-types") (vers "0.4.0-beta1") (deps (list (crate-dep (name "error-chain") (req "^0.12") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)))) (hash "18vd8vj50rl2iv4cvimsxabr2g5sy33jay72d7xvxmqw3six8cbn")))

(define-public crate-gm-types-0.4 (crate (name "gm-types") (vers "0.4.0-pre") (deps (list (crate-dep (name "error-chain") (req "^0.12") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)))) (hash "05df1fwx588jw2aj1rx6fflp1s5x73m7rjhxdvw746bpd2lnrcrn")))

