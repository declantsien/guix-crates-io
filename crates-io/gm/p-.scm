(define-module (crates-io gm p-) #:use-module (crates-io))

(define-public crate-gmp-mpfr-0.1 (crate (name "gmp-mpfr") (vers "0.1.0") (deps (list (crate-dep (name "gmp-mpfr-sys") (req "^0.2") (default-features #t) (kind 0)))) (hash "0p2l784sq97pifj0zzyqbxj7scjzd73lj4g22k0w3lrykalxnzwk")))

(define-public crate-gmp-mpfr-0.1 (crate (name "gmp-mpfr") (vers "0.1.1") (deps (list (crate-dep (name "gmp-mpfr-sys") (req "^0.2") (default-features #t) (kind 0)))) (hash "17rlcwrgf5fqxfdpx2midfv5ssjjqydm6jds031f6mpkigylg9m6")))

(define-public crate-gmp-mpfr-0.2 (crate (name "gmp-mpfr") (vers "0.2.0") (deps (list (crate-dep (name "gmp-mpfr-sys") (req "^0.2") (default-features #t) (kind 0)))) (hash "1x7k2v156zqlpgd1gfchv224q7vj1j6ay1icn1pg3bz7p2y3g9xn")))

(define-public crate-gmp-mpfr-0.3 (crate (name "gmp-mpfr") (vers "0.3.0") (deps (list (crate-dep (name "gmp-mpfr-sys") (req "^0.2") (default-features #t) (kind 0)))) (hash "19z80jf7q1g55wdnpa71hb35cgsi37g9gm6w9mv11i6qam3cchvg")))

(define-public crate-gmp-mpfr-0.3 (crate (name "gmp-mpfr") (vers "0.3.1") (deps (list (crate-dep (name "gmp-mpfr-sys") (req "^0.2") (default-features #t) (kind 0)))) (hash "1mbqnsnmn1ci1nikwp0ygv3f43pb8hw4psir05xin7n2m4ca3r67")))

(define-public crate-gmp-mpfr-0.3 (crate (name "gmp-mpfr") (vers "0.3.2") (deps (list (crate-dep (name "gmp-mpfr-sys") (req "^0.2") (default-features #t) (kind 0)))) (hash "1c8hlbdr60ypyka0g69sx2kdfazi348h99hrk0p76maj79gb4i6p")))

(define-public crate-gmp-mpfr-0.4 (crate (name "gmp-mpfr") (vers "0.4.0") (deps (list (crate-dep (name "gmp-mpfr-sys") (req "^0.3") (default-features #t) (kind 0)))) (hash "1vkfca5j1n4lj9r4bwp9k6mgqvzbsq9k9n1j3zp11fs23bjqd2iw")))

(define-public crate-gmp-mpfr-0.4 (crate (name "gmp-mpfr") (vers "0.4.1") (deps (list (crate-dep (name "gmp-mpfr-sys") (req "^0.3") (default-features #t) (kind 0)))) (hash "0ck8jlj1piwzcx4xr0z51pdl8hcnckpybb54vwb2wwahx4zarg0a")))

(define-public crate-gmp-mpfr-0.5 (crate (name "gmp-mpfr") (vers "0.5.0") (hash "0y7469bjpms0y8yagqgcs9rnw8396vqx5s169mks7haszppmb0gc")))

(define-public crate-gmp-mpfr-0.5 (crate (name "gmp-mpfr") (vers "0.5.1") (hash "1f1jpry3cx1w6933y5q203ls8jd4d2c221cm76nk8qgadja27bws")))

(define-public crate-gmp-mpfr-0.5 (crate (name "gmp-mpfr") (vers "0.5.2") (hash "1fm9hxa767xjrqic7z735zrp4mrx7jdd57qwif9423vx5968hz54")))

(define-public crate-gmp-mpfr-sys-0.1 (crate (name "gmp-mpfr-sys") (vers "0.1.0") (hash "1mahhqb0yj5ahvihcifx2cr152bkyv5y2rh5ixz9xpxsjav1n41w")))

(define-public crate-gmp-mpfr-sys-0.2 (crate (name "gmp-mpfr-sys") (vers "0.2.0") (hash "1qq82vn2236v2h6d0vw371jkcbpd6jyym3y0b0lasdriblqyqkc3")))

(define-public crate-gmp-mpfr-sys-0.2 (crate (name "gmp-mpfr-sys") (vers "0.2.1") (hash "1ih606sndqf4nx5y30f2kvxh2mn3j3i6bi315pqrpb72j50814jm")))

(define-public crate-gmp-mpfr-sys-0.2 (crate (name "gmp-mpfr-sys") (vers "0.2.2") (hash "10hpy5jlw34fx3li9q9rk4s8cf4lgi66vgl54r29b0n1hk3hx620")))

(define-public crate-gmp-mpfr-sys-0.2 (crate (name "gmp-mpfr-sys") (vers "0.2.3") (hash "0i4ya6bk1rd0ik72z0m57hppmlczbwh0wwqwwqcmyl0p7wgj6xa3")))

(define-public crate-gmp-mpfr-sys-0.3 (crate (name "gmp-mpfr-sys") (vers "0.3.0") (hash "1986hbsgj5y2p5f83ckpzwgy4xjyg9a495ki5wfw5icgxz6h1n8w")))

(define-public crate-gmp-mpfr-sys-0.3 (crate (name "gmp-mpfr-sys") (vers "0.3.1") (hash "09m3abjqxkv4wng8xawv718lilyjnk7wcp2ashjb0ifdmxfsizdh")))

(define-public crate-gmp-mpfr-sys-0.3 (crate (name "gmp-mpfr-sys") (vers "0.3.2") (hash "1l8s04dskdydgldvic7r2l412rf0v2z3r4k6ys85kgz4bxf0m1ca")))

(define-public crate-gmp-mpfr-sys-0.4 (crate (name "gmp-mpfr-sys") (vers "0.4.0") (hash "1dyd0jnb6cx3ka64vnr5z6yng6qds51m2w52q06yq7lbhzqimygw")))

(define-public crate-gmp-mpfr-sys-0.5 (crate (name "gmp-mpfr-sys") (vers "0.5.0") (hash "05ha2jmv0j44vb79i9pi4ykw2frfwhmqipdfx3w3bzifl2jgg8ld")))

(define-public crate-gmp-mpfr-sys-0.5 (crate (name "gmp-mpfr-sys") (vers "0.5.1") (hash "189mj2cxh803hkqxm4lvsic3741y1sgjx8xddrmb4lypd7va7jnn")))

(define-public crate-gmp-mpfr-sys-0.5 (crate (name "gmp-mpfr-sys") (vers "0.5.2") (hash "0pq5lcqrz00s7r84yfqa25bgild37z8jp01cfgjsm4ch578b2a0s")))

(define-public crate-gmp-mpfr-sys-0.6 (crate (name "gmp-mpfr-sys") (vers "0.6.0") (hash "1n9fyqmsfdh3xl2xvz8q7z70g4bqi20qajqj4n9fqcrq1s7qp1lq")))

(define-public crate-gmp-mpfr-sys-1 (crate (name "gmp-mpfr-sys") (vers "1.0.0") (hash "1388zr91mbq0pi2pbcfjfpda4x74v4jgd75r2gnajx17c3751zlk")))

(define-public crate-gmp-mpfr-sys-1 (crate (name "gmp-mpfr-sys") (vers "1.0.1") (hash "1k4jggdyb8a57i2vbzhjpqa7qkdhxf7qpznms24drizqzwwrh5pi")))

(define-public crate-gmp-mpfr-sys-1 (crate (name "gmp-mpfr-sys") (vers "1.0.2") (hash "0120yqlhl05dxb7pywifdckqkbvs7ylpr8db8pd7lhhk160bqvgb") (features (quote (("mpfr") ("mpc" "mpfr") ("default" "mpfr" "mpc"))))))

(define-public crate-gmp-mpfr-sys-1 (crate (name "gmp-mpfr-sys") (vers "1.0.3") (hash "02cgyknvx2zngzk33x2426hh9n2k1lr4y0w4dkmlff7kn94j23qg") (features (quote (("mpfr") ("mpc" "mpfr") ("default" "mpfr" "mpc"))))))

(define-public crate-gmp-mpfr-sys-1 (crate (name "gmp-mpfr-sys") (vers "1.0.4") (hash "08f6i5apkjdm0ajynrqcys74c7mhl9fsnwrzhkyyx9kq556mc7h3") (features (quote (("mpfr") ("mpc" "mpfr") ("default" "mpfr" "mpc") ("ctest") ("cnotest"))))))

(define-public crate-gmp-mpfr-sys-1 (crate (name "gmp-mpfr-sys") (vers "1.0.5") (hash "04r14x514khwb36fk855jkrp67n9fi8dcr51r8fmfdqv7yq5hbbp") (features (quote (("mpfr") ("mpc" "mpfr") ("default" "mpfr" "mpc") ("ctest") ("cnotest"))))))

(define-public crate-gmp-mpfr-sys-1 (crate (name "gmp-mpfr-sys") (vers "1.0.6") (hash "1brj7qpdlpnpa8085197asp60hkgh5ag7lgzybm696nbyyvj71gg") (features (quote (("mpfr") ("mpc" "mpfr") ("default" "mpfr" "mpc") ("ctest") ("cnotest"))))))

(define-public crate-gmp-mpfr-sys-1 (crate (name "gmp-mpfr-sys") (vers "1.0.7") (hash "19w8nq6n4ywr0xqs4p955qrmq0zn02ph30yvsfcag0spy2vh82g7") (features (quote (("mpfr") ("mpc" "mpfr") ("default" "mpfr" "mpc") ("ctest") ("cnotest"))))))

(define-public crate-gmp-mpfr-sys-1 (crate (name "gmp-mpfr-sys") (vers "1.0.8") (hash "1zh6vkg60gaibjiz1qhzn7ij8ld6xcw4cya8b6nys2xjgkq5hdkz") (features (quote (("mpfr") ("mpc" "mpfr") ("default" "mpfr" "mpc") ("ctest") ("cnotest"))))))

(define-public crate-gmp-mpfr-sys-1 (crate (name "gmp-mpfr-sys") (vers "1.1.0") (hash "1hnja1nvwmsd6qlckbki473iwc6r2adwrvxyc7ll1xr8kr95vpzs") (features (quote (("mpfr") ("mpc" "mpfr") ("default" "mpfr" "mpc") ("ctest") ("cnotest"))))))

(define-public crate-gmp-mpfr-sys-1 (crate (name "gmp-mpfr-sys") (vers "1.1.1") (hash "02jr04bydkk1zk0scn1y65qxrfiq0fxy8ch93v9bvn5wqgqqd1n0") (features (quote (("mpfr") ("mpc" "mpfr") ("default" "mpfr" "mpc") ("ctest") ("cnotest"))))))

(define-public crate-gmp-mpfr-sys-1 (crate (name "gmp-mpfr-sys") (vers "1.1.2") (hash "1p4gnn6b62gay2livdlb06l9sk2i7j9np3vkaygyprl5sphzla0j") (features (quote (("mpfr") ("mpc" "mpfr") ("default" "mpfr" "mpc") ("ctest") ("cnotest"))))))

(define-public crate-gmp-mpfr-sys-1 (crate (name "gmp-mpfr-sys") (vers "1.1.3") (hash "1xa6amh1024m4lk1xlgdb9f7nr3p8wmcf453p9pxh3a2164d3g27") (features (quote (("mpfr") ("mpc" "mpfr") ("default" "mpfr" "mpc") ("ctest") ("cnotest"))))))

(define-public crate-gmp-mpfr-sys-1 (crate (name "gmp-mpfr-sys") (vers "1.1.4") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "0hls17dl4n674w08pi0x375p2ymhhl719aafsxkmiwinhhr2ywxr") (features (quote (("mpfr") ("mpc" "mpfr") ("default" "mpfr" "mpc") ("ctest") ("cnotest"))))))

(define-public crate-gmp-mpfr-sys-1 (crate (name "gmp-mpfr-sys") (vers "1.1.5") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "0z3rqyshh49rj9vkqwwx4dn2adb6xjhyls79f17nr2hz9mlycpg5") (features (quote (("mpfr") ("mpc" "mpfr") ("default" "mpfr" "mpc") ("ctest") ("cnotest"))))))

(define-public crate-gmp-mpfr-sys-1 (crate (name "gmp-mpfr-sys") (vers "1.1.6") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "1ajh6i7c45wgsgvsrgfbr7wfmg4l6bm491bk9b6wmlcvl5c8n5fj") (features (quote (("mpfr") ("mpc" "mpfr") ("default" "mpfr" "mpc") ("ctest") ("cnotest")))) (links "gmp")))

(define-public crate-gmp-mpfr-sys-1 (crate (name "gmp-mpfr-sys") (vers "1.1.7") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "1q2xhyfxlljr9bqz9zgvqc92k8a5qr8ipw9pr6c9jhcf76xvcza8") (features (quote (("mpfr") ("mpc" "mpfr") ("default" "mpfr" "mpc") ("ctest") ("cnotest")))) (links "gmp")))

(define-public crate-gmp-mpfr-sys-1 (crate (name "gmp-mpfr-sys") (vers "1.1.8") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "10032sh5y41p0cfyqqzsajn74ka4jbb3256hcf1lfrjfzf314xqj") (features (quote (("mpfr") ("mpc" "mpfr") ("default" "mpfr" "mpc") ("ctest") ("cnotest")))) (links "gmp")))

(define-public crate-gmp-mpfr-sys-1 (crate (name "gmp-mpfr-sys") (vers "1.1.9") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "0ckapkjcc7gpfpg1ka7hkcv18pbynr68xl1mz8qsrbyc63qk9ha4") (features (quote (("mpfr") ("mpc" "mpfr") ("default" "mpfr" "mpc") ("ctest") ("cnotest")))) (links "gmp")))

(define-public crate-gmp-mpfr-sys-1 (crate (name "gmp-mpfr-sys") (vers "1.1.10") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "0gm2nvbgnvp0vf2gkvl77bw0j82z7qzac1zgzb878sxadknsqc3l") (features (quote (("mpfr") ("mpc" "mpfr") ("default" "mpfr" "mpc") ("ctest") ("cnotest") ("cnodelete")))) (links "gmp")))

(define-public crate-gmp-mpfr-sys-1 (crate (name "gmp-mpfr-sys") (vers "1.1.11") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "13n16zgkj969p85mb8n21kgisqdhpw8psag39d9c8b40bhl1qrhx") (features (quote (("mpfr") ("mpc" "mpfr") ("fail-on-warnings") ("default" "mpfr" "mpc") ("ctest") ("cnotest") ("cnodelete")))) (links "gmp")))

(define-public crate-gmp-mpfr-sys-1 (crate (name "gmp-mpfr-sys") (vers "1.1.12") (deps (list (crate-dep (name "dirs") (req "~1.0") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "0q0adc8w3x8nvh7wigxk2hbj9la8p3mhg51rkhkf396yvcs752lq") (features (quote (("mpfr") ("mpc" "mpfr") ("fail-on-warnings") ("default" "mpfr" "mpc") ("ctest") ("cnotest") ("cnodelete")))) (links "gmp")))

(define-public crate-gmp-mpfr-sys-1 (crate (name "gmp-mpfr-sys") (vers "1.1.13") (deps (list (crate-dep (name "dirs") (req "~1.0") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "11x34a37q0qfj58pqai66qwmc0n8f8p0hfwzr8aww9h96710vgc1") (features (quote (("mpfr") ("mpc" "mpfr") ("fail-on-warnings") ("default" "mpfr" "mpc") ("ctest") ("cnotest") ("cnodelete")))) (links "gmp")))

(define-public crate-gmp-mpfr-sys-1 (crate (name "gmp-mpfr-sys") (vers "1.1.14") (deps (list (crate-dep (name "dirs") (req "^2.0") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "1ysm543ndl05844wc44yy39214c35v22aj2185ggmviza7bjqnbj") (features (quote (("mpfr") ("mpc" "mpfr") ("fail-on-warnings") ("default" "mpfr" "mpc") ("ctest") ("cnotest") ("cnodelete")))) (links "gmp")))

(define-public crate-gmp-mpfr-sys-1 (crate (name "gmp-mpfr-sys") (vers "1.2.0") (deps (list (crate-dep (name "dirs") (req "^2.0") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2.44") (kind 0)))) (hash "0b3hahv63fzabbisar999s5l53zmqkrv7klfip8kanyiym3j7jsa") (features (quote (("use-system-libs") ("mpfr") ("mpc" "mpfr") ("fail-on-warnings") ("default" "mpfr" "mpc") ("cnodelete")))) (links "gmp")))

(define-public crate-gmp-mpfr-sys-1 (crate (name "gmp-mpfr-sys") (vers "1.2.1") (deps (list (crate-dep (name "dirs") (req "^2.0") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2.44") (kind 0)))) (hash "1lriv7qf9a354y7w092iwdw591fq5aq1zs8wsqaik0sx7z18cblk") (features (quote (("use-system-libs") ("mpfr") ("mpc" "mpfr") ("fail-on-warnings") ("default" "mpfr" "mpc") ("cnodelete")))) (links "gmp")))

(define-public crate-gmp-mpfr-sys-1 (crate (name "gmp-mpfr-sys") (vers "1.2.2") (deps (list (crate-dep (name "libc") (req "^0.2.44") (kind 0)) (crate-dep (name "winapi") (req "^0.3") (features (quote ("combaseapi" "knownfolders" "shlobj" "winbase" "winerror"))) (default-features #t) (target "cfg(target_os = \"windows\")") (kind 1)))) (hash "16z8iixz4j8crl8qwahnwwy8x2ys9f364gs67ml83w4xrw2zimv3") (features (quote (("use-system-libs") ("mpfr") ("mpc" "mpfr") ("fail-on-warnings") ("default" "mpfr" "mpc") ("cnodelete")))) (links "gmp")))

(define-public crate-gmp-mpfr-sys-1 (crate (name "gmp-mpfr-sys") (vers "1.2.3") (deps (list (crate-dep (name "libc") (req "^0.2.44") (kind 0)) (crate-dep (name "winapi") (req "^0.3") (features (quote ("combaseapi" "knownfolders" "shlobj" "winbase" "winerror"))) (default-features #t) (target "cfg(target_os = \"windows\")") (kind 1)))) (hash "06rvnn91dyrj5vn9r5n1dichg1dbbbk2dpmrciqvx80niz3cq11w") (features (quote (("use-system-libs") ("mpfr") ("mpc" "mpfr") ("force-cross") ("fail-on-warnings") ("default" "mpfr" "mpc") ("cnodelete")))) (links "gmp")))

(define-public crate-gmp-mpfr-sys-1 (crate (name "gmp-mpfr-sys") (vers "1.2.4") (deps (list (crate-dep (name "libc") (req "^0.2.44") (kind 0)) (crate-dep (name "winapi") (req "^0.3") (features (quote ("combaseapi" "knownfolders" "shlobj" "winbase" "winerror"))) (default-features #t) (target "cfg(target_os = \"windows\")") (kind 1)))) (hash "0kpbdvs0dkcdj641av759z56as83zmk1klcxiajbfmy3iw1dm8z2") (features (quote (("use-system-libs") ("mpfr") ("mpc" "mpfr") ("force-cross") ("fail-on-warnings") ("default" "mpfr" "mpc") ("cnodelete")))) (links "gmp")))

(define-public crate-gmp-mpfr-sys-1 (crate (name "gmp-mpfr-sys") (vers "1.3.0") (deps (list (crate-dep (name "libc") (req "^0.2.44") (kind 0)) (crate-dep (name "winapi") (req "^0.3") (features (quote ("combaseapi" "knownfolders" "shlobj" "winbase" "winerror"))) (default-features #t) (target "cfg(target_os = \"windows\")") (kind 1)))) (hash "1kgva9ij8lppv40zch9rgwxqbx4j5yykhdmf8lxqsvfa2ya9z94h") (features (quote (("use-system-libs") ("mpfr") ("mpc" "mpfr") ("force-cross") ("fail-on-warnings") ("default" "mpfr" "mpc") ("cnodelete")))) (links "gmp")))

(define-public crate-gmp-mpfr-sys-1 (crate (name "gmp-mpfr-sys") (vers "1.3.1") (deps (list (crate-dep (name "libc") (req "^0.2.44") (kind 0)) (crate-dep (name "winapi") (req "^0.3") (features (quote ("combaseapi" "knownfolders" "shlobj" "winbase" "winerror"))) (default-features #t) (target "cfg(target_os = \"windows\")") (kind 1)))) (hash "07pcdxdqj2blj930b1v6i3i41wnmr745cwf7rwqc1zb5yy8vc6xc") (features (quote (("use-system-libs") ("mpfr") ("mpc" "mpfr") ("force-cross") ("fail-on-warnings") ("default" "mpfr" "mpc") ("cnodelete")))) (links "gmp")))

(define-public crate-gmp-mpfr-sys-1 (crate (name "gmp-mpfr-sys") (vers "1.4.0") (deps (list (crate-dep (name "libc") (req "^0.2.44") (kind 0)) (crate-dep (name "winapi") (req "^0.3") (features (quote ("combaseapi" "knownfolders" "shlobj" "winbase" "winerror"))) (default-features #t) (target "cfg(target_os = \"windows\")") (kind 1)))) (hash "024m2mdh9ahq8hamdvsb6fhkng9639xv3vjdass05fczxj2qwkmd") (features (quote (("use-system-libs") ("mpfr") ("mpc" "mpfr") ("force-cross") ("fail-on-warnings") ("default" "mpfr" "mpc") ("cnodelete")))) (links "gmp")))

(define-public crate-gmp-mpfr-sys-1 (crate (name "gmp-mpfr-sys") (vers "1.4.1") (deps (list (crate-dep (name "libc") (req "^0.2.44") (kind 0)) (crate-dep (name "winapi") (req "^0.3") (features (quote ("combaseapi" "knownfolders" "shlobj" "winbase" "winerror"))) (default-features #t) (target "cfg(target_os = \"windows\")") (kind 1)))) (hash "1425q4fykrqfbjldhw02s1iax4sryxxfqjkzzasq8hc7y3hcmwsb") (features (quote (("use-system-libs") ("mpfr") ("mpc" "mpfr") ("force-cross") ("fail-on-warnings") ("default" "mpfr" "mpc") ("cnodelete") ("c-no-tests")))) (links "gmp")))

(define-public crate-gmp-mpfr-sys-1 (crate (name "gmp-mpfr-sys") (vers "1.4.2") (deps (list (crate-dep (name "libc") (req "^0.2.44") (kind 0)) (crate-dep (name "winapi") (req "^0.3") (features (quote ("combaseapi" "knownfolders" "shlobj" "winbase" "winerror"))) (default-features #t) (target "cfg(target_os = \"windows\")") (kind 1)))) (hash "0vpiyx6qizqxlq8mb7ch6dz91bj0s8761vgxn4hk10s9klrxnzx5") (features (quote (("use-system-libs") ("mpfr") ("mpc" "mpfr") ("force-cross") ("fail-on-warnings") ("default" "mpfr" "mpc") ("cnodelete") ("c-no-tests")))) (links "gmp")))

(define-public crate-gmp-mpfr-sys-1 (crate (name "gmp-mpfr-sys") (vers "1.4.3") (deps (list (crate-dep (name "libc") (req "^0.2.44") (kind 0)) (crate-dep (name "winapi") (req "^0.3") (features (quote ("combaseapi" "knownfolders" "shlobj" "winbase" "winerror"))) (default-features #t) (target "cfg(target_os = \"windows\")") (kind 1)))) (hash "1n0big4im2ndcck7430hlgzxq3clk1lh6aqv0dnw0ai808s83s66") (features (quote (("use-system-libs") ("mpfr") ("mpc" "mpfr") ("force-cross") ("fail-on-warnings") ("default" "mpfr" "mpc") ("cnodelete") ("c-no-tests")))) (links "gmp")))

(define-public crate-gmp-mpfr-sys-1 (crate (name "gmp-mpfr-sys") (vers "1.4.4") (deps (list (crate-dep (name "libc") (req "^0.2.44") (kind 0)) (crate-dep (name "winapi") (req "^0.3") (features (quote ("combaseapi" "knownfolders" "shlobj" "winbase" "winerror"))) (default-features #t) (target "cfg(target_os = \"windows\")") (kind 1)))) (hash "1k4dny6kl8g0jdsh3ihndhc8p1c3qqbhk59vnp2vay13py5cyjgd") (features (quote (("use-system-libs") ("mpfr") ("mpc" "mpfr") ("force-cross") ("fail-on-warnings") ("default" "mpfr" "mpc") ("cnodelete") ("c-no-tests")))) (links "gmp")))

(define-public crate-gmp-mpfr-sys-1 (crate (name "gmp-mpfr-sys") (vers "1.4.5") (deps (list (crate-dep (name "libc") (req "^0.2.44") (kind 0)) (crate-dep (name "winapi") (req "^0.3") (features (quote ("combaseapi" "knownfolders" "shlobj" "winbase" "winerror"))) (default-features #t) (target "cfg(target_os = \"windows\")") (kind 1)))) (hash "0r00n7fbc99pnbr9qw1dkm4xdncq1a4mxb6qz175h79bg52h1rqq") (features (quote (("use-system-libs") ("mpfr") ("mpc" "mpfr") ("force-cross") ("fail-on-warnings") ("default" "mpfr" "mpc") ("cnodelete") ("c-no-tests")))) (links "gmp")))

(define-public crate-gmp-mpfr-sys-1 (crate (name "gmp-mpfr-sys") (vers "1.4.6") (deps (list (crate-dep (name "libc") (req "^0.2.44") (kind 0)) (crate-dep (name "winapi") (req "^0.3") (features (quote ("combaseapi" "knownfolders" "shlobj" "winbase" "winerror"))) (default-features #t) (target "cfg(target_os = \"windows\")") (kind 1)))) (hash "06likcvshw0cxpgqqqf9azk9c97hhj3w5irn7bvpz1j3ajsrx40w") (features (quote (("use-system-libs") ("mpfr") ("mpc" "mpfr") ("force-cross") ("fail-on-warnings") ("default" "mpfr" "mpc") ("cnodelete") ("c-no-tests")))) (links "gmp")))

(define-public crate-gmp-mpfr-sys-1 (crate (name "gmp-mpfr-sys") (vers "1.4.7") (deps (list (crate-dep (name "libc") (req "^0.2.44") (kind 0)) (crate-dep (name "winapi") (req "^0.3") (features (quote ("combaseapi" "knownfolders" "shlobj" "winbase" "winerror"))) (default-features #t) (target "cfg(target_os = \"windows\")") (kind 1)))) (hash "1ysvdf352vcnb5ygmbwf5pkndqb0p6clmz0nqkf3nmz9ghssfim1") (features (quote (("use-system-libs") ("mpfr") ("mpc" "mpfr") ("force-cross") ("fail-on-warnings") ("default" "mpfr" "mpc") ("cnodelete") ("c-no-tests")))) (links "gmp")))

(define-public crate-gmp-mpfr-sys-1 (crate (name "gmp-mpfr-sys") (vers "1.4.8") (deps (list (crate-dep (name "libc") (req "^0.2.44") (kind 0)) (crate-dep (name "winapi") (req "^0.3") (features (quote ("combaseapi" "knownfolders" "shlobj" "winbase" "winerror"))) (default-features #t) (target "cfg(target_os = \"windows\")") (kind 1)))) (hash "0vihd6440ml1zckap9d404jhp6zglv26h5la94l10lb5z470pl5k") (features (quote (("use-system-libs") ("mpfr") ("mpc" "mpfr") ("force-cross") ("fail-on-warnings") ("default" "mpfr" "mpc") ("cnodelete") ("c-no-tests")))) (links "gmp")))

(define-public crate-gmp-mpfr-sys-1 (crate (name "gmp-mpfr-sys") (vers "1.4.9") (deps (list (crate-dep (name "libc") (req "^0.2.44") (kind 0)) (crate-dep (name "winapi") (req "^0.3") (features (quote ("combaseapi" "knownfolders" "shlobj" "winbase" "winerror"))) (default-features #t) (target "cfg(target_os = \"windows\")") (kind 1)))) (hash "1hfayswhlqpjyvprr4avhv2wq2fk5hqx3bvkvgdxhvk61q8p2xid") (features (quote (("use-system-libs") ("mpfr") ("mpc" "mpfr") ("force-cross") ("fail-on-warnings") ("default" "mpfr" "mpc") ("cnodelete") ("c-no-tests")))) (links "gmp")))

(define-public crate-gmp-mpfr-sys-1 (crate (name "gmp-mpfr-sys") (vers "1.4.10") (deps (list (crate-dep (name "libc") (req "^0.2.44") (kind 0)) (crate-dep (name "winapi") (req "^0.3") (features (quote ("combaseapi" "knownfolders" "shlobj" "winbase" "winerror"))) (default-features #t) (target "cfg(target_os = \"windows\")") (kind 1)))) (hash "06cx1ybwihm9frj2x7sccqpjim0ixxbyg1xax4ig2xbcvgd44gza") (features (quote (("use-system-libs") ("mpfr") ("mpc" "mpfr") ("force-cross") ("fail-on-warnings") ("default" "mpfr" "mpc") ("cnodelete") ("c-no-tests")))) (links "gmp")))

(define-public crate-gmp-mpfr-sys-1 (crate (name "gmp-mpfr-sys") (vers "1.4.11") (deps (list (crate-dep (name "libc") (req "^0.2.44") (kind 0)) (crate-dep (name "winapi") (req "^0.3") (features (quote ("combaseapi" "knownfolders" "shlobj" "winbase" "winerror"))) (default-features #t) (target "cfg(target_os = \"windows\")") (kind 1)))) (hash "026nhirq53w14whnrhf2bacnj2nz5yvw0sgqhh9g8wgkxmq5p401") (features (quote (("use-system-libs") ("mpfr") ("mpc" "mpfr") ("force-cross") ("fail-on-warnings") ("default" "mpfr" "mpc") ("cnodelete") ("c-no-tests")))) (links "gmp") (rust-version "1.37")))

(define-public crate-gmp-mpfr-sys-1 (crate (name "gmp-mpfr-sys") (vers "1.4.12") (deps (list (crate-dep (name "libc") (req "^0.2.44") (kind 0)) (crate-dep (name "winapi") (req "^0.3") (features (quote ("combaseapi" "knownfolders" "shlobj" "winbase" "winerror"))) (default-features #t) (target "cfg(target_os = \"windows\")") (kind 1)))) (hash "0i8hiy712afmf048vbsrdz42y7vm7gk89dckvdrci5g1qshvzb9c") (features (quote (("use-system-libs") ("mpfr") ("mpc" "mpfr") ("force-cross") ("fail-on-warnings") ("default" "mpfr" "mpc") ("cnodelete") ("c-no-tests")))) (links "gmp") (rust-version "1.37")))

(define-public crate-gmp-mpfr-sys-1 (crate (name "gmp-mpfr-sys") (vers "1.5.0") (deps (list (crate-dep (name "libc") (req "^0.2.44") (kind 0)) (crate-dep (name "windows-sys") (req "^0.42") (features (quote ("Win32_Foundation" "Win32_Globalization" "Win32_System_Com" "Win32_UI_Shell"))) (default-features #t) (target "cfg(target_os = \"windows\")") (kind 1)))) (hash "1xhcphky9fsj1s8a25500sivjndbbbw5pp67hxwhb8v8wpj105vm") (features (quote (("use-system-libs") ("mpfr") ("mpc" "mpfr") ("force-cross") ("fail-on-warnings") ("default" "mpfr" "mpc") ("cnodelete") ("c-no-tests")))) (links "gmp") (rust-version "1.65")))

(define-public crate-gmp-mpfr-sys-1 (crate (name "gmp-mpfr-sys") (vers "1.4.13") (deps (list (crate-dep (name "libc") (req "^0.2.44") (kind 0)) (crate-dep (name "winapi") (req "^0.3") (features (quote ("combaseapi" "knownfolders" "shlobj" "winbase" "winerror"))) (default-features #t) (target "cfg(target_os = \"windows\")") (kind 1)))) (hash "1mxm72rmnvc3sgh8s3nii5kxas50za64dmxwqlw7vq768f3ja7bq") (features (quote (("use-system-libs") ("mpfr") ("mpc" "mpfr") ("force-cross") ("fail-on-warnings") ("default" "mpfr" "mpc") ("cnodelete") ("c-no-tests")))) (links "gmp") (rust-version "1.37")))

(define-public crate-gmp-mpfr-sys-1 (crate (name "gmp-mpfr-sys") (vers "1.5.1") (deps (list (crate-dep (name "libc") (req "^0.2.44") (kind 0)) (crate-dep (name "windows-sys") (req "^0.42") (features (quote ("Win32_Foundation" "Win32_Globalization" "Win32_System_Com" "Win32_UI_Shell"))) (default-features #t) (target "cfg(target_os = \"windows\")") (kind 1)))) (hash "0b364vbmgssb8r7iybx1zafrfivnjf57vjn56fxjbyr65h4nha53") (features (quote (("use-system-libs") ("mpfr") ("mpc" "mpfr") ("force-cross") ("fail-on-warnings") ("default" "mpfr" "mpc") ("cnodelete") ("c-no-tests")))) (links "gmp") (rust-version "1.65")))

(define-public crate-gmp-mpfr-sys-1 (crate (name "gmp-mpfr-sys") (vers "1.5.2") (deps (list (crate-dep (name "libc") (req "^0.2.44") (kind 0)) (crate-dep (name "windows-sys") (req "^0.42") (features (quote ("Win32_Foundation" "Win32_Globalization" "Win32_System_Com" "Win32_UI_Shell"))) (default-features #t) (target "cfg(target_os = \"windows\")") (kind 1)))) (hash "0pxpz1z47rharmdhppmqlpn7vaa455zligzr5sfcxa7zw9ih0mjv") (features (quote (("use-system-libs") ("mpfr") ("mpc" "mpfr") ("force-cross") ("fail-on-warnings") ("default" "mpfr" "mpc") ("cnodelete") ("c-no-tests")))) (links "gmp") (rust-version "1.65")))

(define-public crate-gmp-mpfr-sys-1 (crate (name "gmp-mpfr-sys") (vers "1.5.3") (deps (list (crate-dep (name "libc") (req "^0.2.44") (kind 0)) (crate-dep (name "windows-sys") (req "^0.42") (features (quote ("Win32_Foundation" "Win32_Globalization" "Win32_System_Com" "Win32_UI_Shell"))) (default-features #t) (target "cfg(target_os = \"windows\")") (kind 1)))) (hash "0pd9zrq76hbx3rkma29q4knslz5v3v3ijfay94dn4jkfs4lvrshk") (features (quote (("use-system-libs") ("mpfr") ("mpc" "mpfr") ("force-cross") ("fail-on-warnings") ("default" "mpfr" "mpc") ("cnodelete") ("c-no-tests")))) (links "gmp") (rust-version "1.65")))

(define-public crate-gmp-mpfr-sys-1 (crate (name "gmp-mpfr-sys") (vers "1.6.0") (deps (list (crate-dep (name "libc") (req "^0.2.44") (kind 0)) (crate-dep (name "windows-sys") (req "^0.42") (features (quote ("Win32_Foundation" "Win32_Globalization" "Win32_System_Com" "Win32_UI_Shell"))) (default-features #t) (target "cfg(target_os = \"windows\")") (kind 1)))) (hash "0rl4qrcqab701y78x6jqw8s9zlss0rxlw4br0dz8ha1xnd0hy0c7") (features (quote (("use-system-libs") ("mpfr") ("mpc" "mpfr") ("force-cross") ("fail-on-warnings") ("default" "mpfr" "mpc") ("cnodelete") ("c-no-tests")))) (links "gmp") (rust-version "1.65")))

(define-public crate-gmp-mpfr-sys-1 (crate (name "gmp-mpfr-sys") (vers "1.6.1") (deps (list (crate-dep (name "libc") (req "^0.2.44") (kind 0)) (crate-dep (name "windows-sys") (req "^0.42") (features (quote ("Win32_Foundation" "Win32_Globalization" "Win32_System_Com" "Win32_UI_Shell"))) (default-features #t) (target "cfg(target_os = \"windows\")") (kind 1)))) (hash "06vj8lli8l9mxxak7icn8kgy9rj817b1ssbf4viqgzi9iiywdi8r") (features (quote (("use-system-libs") ("mpfr") ("mpc" "mpfr") ("force-cross") ("fail-on-warnings") ("default" "mpfr" "mpc") ("cnodelete") ("c-no-tests")))) (links "gmp") (rust-version "1.65")))

(define-public crate-gmp-mpfr-sys-1 (crate (name "gmp-mpfr-sys") (vers "1.6.2") (deps (list (crate-dep (name "libc") (req "^0.2.44") (kind 0)) (crate-dep (name "windows-sys") (req "^0.52") (features (quote ("Win32_Foundation" "Win32_Globalization" "Win32_System_Com" "Win32_UI_Shell"))) (default-features #t) (target "cfg(target_os = \"windows\")") (kind 1)))) (hash "1qq85s45m5pdyqrhbf9gsdlzra4gxqdbj0wwwkmimx79rg1nqain") (features (quote (("use-system-libs") ("mpfr") ("mpc" "mpfr") ("force-cross") ("fail-on-warnings") ("default" "mpfr" "mpc") ("cnodelete") ("c-no-tests")))) (links "gmp") (rust-version "1.65")))

(define-public crate-gmp-sys-0.0.1 (crate (name "gmp-sys") (vers "0.0.1") (hash "0ips8n7agwjmd6wbgk9dvvmlk64l15bc93cb1373pif766g1ghlj")))

(define-public crate-gmp-sys-0.0.2 (crate (name "gmp-sys") (vers "0.0.2") (hash "1s5i5bzbcvr1vgjbv4fwd8x1k7kdbvj9lnprpdrw13il2rpic1y6")))

(define-public crate-gmp-sys-0.0.3 (crate (name "gmp-sys") (vers "0.0.3") (hash "0jqia0rwjpkkm7rhiyz2yi26xc4dmzadryjfdnbvxxhwxsv9qiya")))

(define-public crate-gmp-sys-0.0.4 (crate (name "gmp-sys") (vers "0.0.4") (hash "1hpiac9438g1g216gjh9a76ha3rjrwm9xfyaikxhqsdplkrawclq")))

(define-public crate-gmp-sys-0.0.5 (crate (name "gmp-sys") (vers "0.0.5") (hash "0kfmv4a1cxx1y2bgds5pbw56rs6wdvpmsfik0pzznc8xkbgqd2yb")))

(define-public crate-gmp-sys-0.0.6 (crate (name "gmp-sys") (vers "0.0.6") (hash "16anvs44pwck7f6vv9m8fpglhs5y87va3xz9bk8zsz9mx82fdy2c")))

(define-public crate-gmp-sys-0.0.7 (crate (name "gmp-sys") (vers "0.0.7") (hash "1s9khrlxnvqhcn2cfyk8pv20i1l0sfp28drc8b0d9xnfwh79ddnx")))

(define-public crate-gmp-sys-0.1 (crate (name "gmp-sys") (vers "0.1.0") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "1g42fvhbfgjzwylk3h0mw98jj7sla5wn53vbgrglaaspx03h0rg5")))

