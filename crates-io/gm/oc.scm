(define-module (crates-io gm oc) #:use-module (crates-io))

(define-public crate-gmocli-1 (crate (name "gmocli") (vers "1.0.0-alpha.1") (deps (list (crate-dep (name "json") (req "^0.12.4") (default-features #t) (kind 1)))) (hash "1k93bilzlrg0nly879rk5izqmf87sqzsja40055ij353d49f49n3")))

(define-public crate-gmocli-1 (crate (name "gmocli") (vers "1.0.0") (deps (list (crate-dep (name "json") (req "^0.12.4") (default-features #t) (kind 1)))) (hash "02a4smiz02j5k01hbss4wybs7hy93afprni5syjvgl631plk703c")))

(define-public crate-gmocli-1 (crate (name "gmocli") (vers "1.1.0") (deps (list (crate-dep (name "json") (req "^0.12.4") (default-features #t) (kind 1)))) (hash "0flj7brzazsis2pskhgg47v3fx6qwshib4m3z72r91nc17jq2bs8")))

