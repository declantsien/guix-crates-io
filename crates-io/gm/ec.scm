(define-module (crates-io gm ec) #:use-module (crates-io))

(define-public crate-gmec-0.0.1 (crate (name "gmec") (vers "0.0.1") (hash "0fmwl8i6kjiw1g5bglfk93ggqdb53qbgjziybj2x00i4hjap466p")))

(define-public crate-gmec-0.0.2 (crate (name "gmec") (vers "0.0.2") (hash "1sbp85mdl9gx3db8kvaad2fhy2rn7jszfmiqw73xcsxvw617a7jb")))

(define-public crate-gmec-0.0.3 (crate (name "gmec") (vers "0.0.3") (hash "05vx6k61g04v1if24khrbyr5rv6km2fggwg9hif4q0vrpaa3bs4i")))

