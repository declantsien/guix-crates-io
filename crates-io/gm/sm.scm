(define-module (crates-io gm sm) #:use-module (crates-io))

(define-public crate-gmsm-0.0.1 (crate (name "gmsm") (vers "0.0.1") (hash "15021qdm9dm4q8djxci51lng5j5z66iyvmyj5nq3kqpcaljzckg9")))

(define-public crate-gmsm-0.0.2 (crate (name "gmsm") (vers "0.0.2") (deps (list (crate-dep (name "hex") (req "^0.3") (default-features #t) (kind 0)))) (hash "1xhpcs06m2g7zs7n4z68dz9mqxmi3y4lf8j8p4950rfhf5jxr1j5")))

(define-public crate-gmsm-0.0.3 (crate (name "gmsm") (vers "0.0.3") (deps (list (crate-dep (name "hex") (req "^0.3") (default-features #t) (kind 0)))) (hash "11i28j1scflgxfvicrgwwc2694w2da573w0w24n5gsdjassrjyl3")))

(define-public crate-gmsm-0.0.4 (crate (name "gmsm") (vers "0.0.4") (deps (list (crate-dep (name "hex") (req "^0.3") (default-features #t) (kind 0)))) (hash "1bp70wk5hbyzcs0npqglxqdgda8k6jc6i079rmjkr1pgdhnj5knd")))

(define-public crate-gmsm-0.0.5 (crate (name "gmsm") (vers "0.0.5") (deps (list (crate-dep (name "hex") (req "^0.3") (default-features #t) (kind 0)))) (hash "1bkxbwaq6ysdfl9w1b3h0g7abn7ysz3c38vph71yhah68c2gf15w")))

(define-public crate-gmsm-0.1 (crate (name "gmsm") (vers "0.1.0") (deps (list (crate-dep (name "hex") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "num") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "num-bigint-dig") (req "^0.7.0") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.4") (default-features #t) (kind 0)))) (hash "0y3fb8pvyxiz2py8j44aywhbq0vvqf6a56jcinczrs5l1sh5g70p")))

