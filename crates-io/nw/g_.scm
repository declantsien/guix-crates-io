(define-module (crates-io nw g_) #:use-module (crates-io))

(define-public crate-nwg_ui-1 (crate (name "nwg_ui") (vers "1.0.0") (deps (list (crate-dep (name "nwg") (req "^1.0.12") (features (quote ("all" "flexbox"))) (default-features #t) (kind 0) (package "native-windows-gui")) (crate-dep (name "winapi") (req "^0.3.9") (default-features #t) (kind 0)))) (hash "0yf55cnasjmcsgn2q6i8m81mj33ppmg1l8lnxsjcwp3c9ll576dd")))

(define-public crate-nwg_ui-1 (crate (name "nwg_ui") (vers "1.0.1") (deps (list (crate-dep (name "nwg") (req "^1.0.12") (features (quote ("all" "flexbox"))) (default-features #t) (kind 0) (package "native-windows-gui")) (crate-dep (name "winapi") (req "^0.3.9") (default-features #t) (kind 0)))) (hash "10zxm2rjqs62yz9rs163x5nddqzk1r06v0g3c1wjs4jldhc1049i")))

