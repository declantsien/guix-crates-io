(define-module (crates-io nw l-) #:use-module (crates-io))

(define-public crate-nwl-protobufs-rust-0.1 (crate (name "nwl-protobufs-rust") (vers "0.1.0") (deps (list (crate-dep (name "prost") (req "^0.10.3") (default-features #t) (kind 0)) (crate-dep (name "prost-types") (req "^0.10.1") (default-features #t) (kind 0)) (crate-dep (name "tonic") (req "^0.7.2") (default-features #t) (kind 0)) (crate-dep (name "tonic-build") (req "^0.7.2") (default-features #t) (kind 1)))) (hash "14rjc1avqd0v27x7gwxd3iqjiv293r820q30bkz7wlzc23s53m0z") (yanked #t)))

