(define-module (crates-io nw -b) #:use-module (crates-io))

(define-public crate-nw-board-support-0.0.0 (crate (name "nw-board-support") (vers "0.0.0") (hash "0v1bdhbgb4yb69gpllsmzc3hz5na1rk6kfwi3rbc37mzbdi4mbjz")))

(define-public crate-nw-bootloader-0.0.0 (crate (name "nw-bootloader") (vers "0.0.0") (hash "1xz40hawy4mynl4flhqi26906pcvsy7sjvpchcs1kz1wd2hqjmhd")))

