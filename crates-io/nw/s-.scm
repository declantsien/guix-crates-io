(define-module (crates-io nw s-) #:use-module (crates-io))

(define-public crate-nws-forecast-zones-0.1 (crate (name "nws-forecast-zones") (vers "0.1.0") (hash "1gv5mvf439aaicc8d502bk2mzxrlkzh6xr6mlgcvj5g818nmdx5l")))

(define-public crate-nws-forecast-zones-0.1 (crate (name "nws-forecast-zones") (vers "0.1.1") (hash "1pxhaxrvkzapn3p0d54i91fi79jsbfipixv55c5m1lqc6pv04345")))

(define-public crate-nws-forecast-zones-0.2 (crate (name "nws-forecast-zones") (vers "0.2.0") (hash "00blkznmgli9d1hmfjj7qwyx1dz9bw6hvjchfl5b8jljfsc1why3")))

(define-public crate-nws-forecast-zones-0.3 (crate (name "nws-forecast-zones") (vers "0.3.0") (hash "13n1vga9vmwyxff0y7cqbmzv4n762z4m7sd6f4hc4x0vdgkj6ksl")))

(define-public crate-nws-product-list-0.1 (crate (name "nws-product-list") (vers "0.1.0") (hash "1dxmx00hipbn5iwpzab1bcdp8ydr7y4l2l09zx9j4gc2d1lxg36p")))

(define-public crate-nws-rs-0.0.1 (crate (name "nws-rs") (vers "0.0.1") (deps (list (crate-dep (name "anyhow") (req "^1.0.32") (default-features #t) (kind 0)) (crate-dep (name "http") (req "^0.2.1") (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.10.8") (features (quote ("blocking" "json"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.116") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.57") (default-features #t) (kind 0)))) (hash "16fda4ba0cwww4h7k963rp723hvmffd0gh7774bfcad6yyqrrysj")))

