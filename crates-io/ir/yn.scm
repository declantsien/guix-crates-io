(define-module (crates-io ir yn) #:use-module (crates-io))

(define-public crate-iryna-0.1 (crate (name "iryna") (vers "0.1.0") (deps (list (crate-dep (name "concurrent-hashmap") (req "^0.2.1") (default-features #t) (kind 0)) (crate-dep (name "mio") (req "^0.6") (default-features #t) (kind 0)))) (hash "09b8c1a5h6gpy63j64bpdvzncayz43n7r8afyivp7ngm5h97ylx0") (yanked #t)))

(define-public crate-iryna-0.1 (crate (name "iryna") (vers "0.1.1") (deps (list (crate-dep (name "concurrent-hashmap") (req "^0.2.1") (default-features #t) (kind 0)) (crate-dep (name "mio") (req "^0.6") (default-features #t) (kind 0)))) (hash "1qzcz5nkx8wwi88p3452c2dkwklk4z2mc3fwxg35cxipa923cf0y") (yanked #t)))

(define-public crate-iryna-0.1 (crate (name "iryna") (vers "0.1.2") (deps (list (crate-dep (name "concurrent-hashmap") (req "^0.2.1") (default-features #t) (kind 0)) (crate-dep (name "mio") (req "^0.6") (default-features #t) (kind 0)))) (hash "1wxlv60g1wxwf3s23734h4xra0n71ngv26fk5zwqnh9y0ni12v5h") (yanked #t)))

(define-public crate-iryna-0.1 (crate (name "iryna") (vers "0.1.3") (deps (list (crate-dep (name "concurrent-hashmap") (req "^0.2.1") (default-features #t) (kind 0)) (crate-dep (name "mio") (req "^0.6") (default-features #t) (kind 0)))) (hash "10vicjp2lhk7d9q1x3gl0yrjagvmn2h8z257662z5vjxlahpa70w") (yanked #t)))

(define-public crate-iryna-0.1 (crate (name "iryna") (vers "0.1.4") (deps (list (crate-dep (name "chashmap") (req "^2.2.0") (default-features #t) (kind 0)) (crate-dep (name "mio") (req "^0.6") (default-features #t) (kind 0)))) (hash "1k9d5fmm0z2qaj86m9ix8agdcc3fcxjr5rcz80764yzaw2f3gigc")))

