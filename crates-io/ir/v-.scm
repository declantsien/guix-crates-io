(define-module (crates-io ir v-) #:use-module (crates-io))

(define-public crate-irv-loader-0.0.1 (crate (name "irv-loader") (vers "0.0.1") (deps (list (crate-dep (name "goblin") (req "^0.5") (default-features #t) (kind 0)) (crate-dep (name "irv-traits") (req "^0.0.1") (default-features #t) (kind 0)))) (hash "1lk4g42g7k45jzchbphv4yxll6raivwbvy6md487giw2jwaq90k9")))

(define-public crate-irv-traits-0.0.1 (crate (name "irv-traits") (vers "0.0.1") (hash "1hi9gig1x8phq99l6z3aq4vbz221y5j0rqk7qjh00pwhhn6zrlq3")))

