(define-module (crates-io ir cp) #:use-module (crates-io))

(define-public crate-ircparser-0.1 (crate (name "ircparser") (vers "0.1.0") (deps (list (crate-dep (name "collection_macros") (req "^0.2.0") (default-features #t) (kind 2)))) (hash "0wicyh70dgr2279v9dlpx89h0x61vwbvfhggh70mq7s12nkzkydn")))

(define-public crate-ircparser-0.2 (crate (name "ircparser") (vers "0.2.0") (deps (list (crate-dep (name "collection_macros") (req "^0.2.0") (default-features #t) (kind 2)))) (hash "0qywy9j06d6rj6z32xkys8h1yjz3pg77g90m8rdlqg4ymvnd9ykg")))

(define-public crate-ircparser-0.2 (crate (name "ircparser") (vers "0.2.1") (deps (list (crate-dep (name "collection_macros") (req "^0.2.0") (default-features #t) (kind 2)))) (hash "15ajg89rp725xwpvrlk1la2w30f0aqpcn7l99y5na5iw1m8g7dxm")))

(define-public crate-ircparser-vanten-0.2 (crate (name "ircparser-vanten") (vers "0.2.1") (deps (list (crate-dep (name "collection_macros") (req "^0.2.0") (default-features #t) (kind 2)))) (hash "0abcbx3037xdybhyhn0wnqwz0qy0mrvbppgm17r131z380xq5qbv")))

(define-public crate-ircprobe-0.1 (crate (name "ircprobe") (vers "0.1.0") (deps (list (crate-dep (name "anyhow") (req "^1.0.70") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^4.2.2") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "futures") (req "^0.3.28") (default-features #t) (kind 0)) (crate-dep (name "rustyline-async") (req "^0.3.1") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.27.0") (features (quote ("rt-multi-thread" "macros" "net"))) (default-features #t) (kind 0)) (crate-dep (name "tokio-util") (req "^0.7.7") (features (quote ("codec"))) (default-features #t) (kind 0)))) (hash "1zhibnnfr43vq6c6bf0669hv91mqdq7p905vqn3fv1nv0vvfp29h")))

(define-public crate-ircprobe-0.1 (crate (name "ircprobe") (vers "0.1.1") (deps (list (crate-dep (name "anyhow") (req "^1.0.70") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^4.2.2") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "futures") (req "^0.3.28") (default-features #t) (kind 0)) (crate-dep (name "rustyline-async") (req "^0.3.1") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.27.0") (features (quote ("rt-multi-thread" "macros" "net"))) (default-features #t) (kind 0)) (crate-dep (name "tokio-util") (req "^0.7.7") (features (quote ("codec"))) (default-features #t) (kind 0)))) (hash "14d42f1jpb55m5d83f8a7lyk6fi20c7c15mghn9y3myqgxf0xpnc")))

