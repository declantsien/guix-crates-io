(define-module (crates-io ir ea) #:use-module (crates-io))

(define-public crate-ireal-parser-0.1 (crate (name "ireal-parser") (vers "0.1.0") (deps (list (crate-dep (name "regex") (req "^1.7.3") (default-features #t) (kind 0)))) (hash "140zr2gm6rg8snbf78sayz74r5pn07bzryyhvy89ddykhw35hd9a")))

