(define-module (crates-io ir ap) #:use-module (crates-io))

(define-public crate-irapt-0.1 (crate (name "irapt") (vers "0.1.0") (deps (list (crate-dep (name "csv") (req "^1.1") (default-features #t) (kind 2)) (crate-dep (name "itertools") (req "^0.10") (kind 0)) (crate-dep (name "matches") (req "^0.1") (default-features #t) (kind 2)) (crate-dep (name "num") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "rustfft") (req "^6.0") (default-features #t) (kind 0)))) (hash "1h3vmiqxvfrpw885dqsd5wm5wgkiki5rnyas9sy4rvh2rg004y32")))

(define-public crate-irapt-0.1 (crate (name "irapt") (vers "0.1.1") (deps (list (crate-dep (name "csv") (req "^1.1") (default-features #t) (kind 2)) (crate-dep (name "itertools") (req "^0.10") (kind 0)) (crate-dep (name "matches") (req "^0.1") (default-features #t) (kind 2)) (crate-dep (name "num") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "rustfft") (req "^6.0") (default-features #t) (kind 0)))) (hash "1g62icbx2wkw0d4k5162rj0n5rx2r7fqgi92ywivzamwa0rpzwiy")))

(define-public crate-irapt-0.2 (crate (name "irapt") (vers "0.2.0") (deps (list (crate-dep (name "csv") (req "^1.1") (default-features #t) (kind 2)) (crate-dep (name "itertools") (req "^0.10") (kind 0)) (crate-dep (name "matches") (req "^0.1") (default-features #t) (kind 2)) (crate-dep (name "num") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "rustfft") (req "^6.0") (default-features #t) (kind 0)))) (hash "1ivhn7s4kyyh8vns2gx8mp8q7wnkbskqs67iig1n598wvnxrvc27")))

