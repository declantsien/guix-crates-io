(define-module (crates-io ir ks) #:use-module (crates-io))

(define-public crate-irksome-0.0.1 (crate (name "irksome") (vers "0.0.1") (deps (list (crate-dep (name "chrono") (req "^0.4.0") (default-features #t) (kind 0)))) (hash "0aw93v05n01hx8d9j3cgv9s3m1bfhys0yw294yzra0vcv42klrgv")))

