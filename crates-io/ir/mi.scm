(define-module (crates-io ir mi) #:use-module (crates-io))

(define-public crate-irmin-0.3 (crate (name "irmin") (vers "0.3.1-beta.0") (deps (list (crate-dep (name "bindgen") (req "^0.59") (default-features #t) (kind 1)) (crate-dep (name "serde") (req "^1") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1") (default-features #t) (kind 0)))) (hash "1b1zwhy0vq2zbbnbgxml20ivs41qgzl5fni9jm4jvlc833fbj93v") (features (quote (("docs"))))))

(define-public crate-irmin-0.3 (crate (name "irmin") (vers "0.3.3") (deps (list (crate-dep (name "bindgen") (req "^0.59") (default-features #t) (kind 1)) (crate-dep (name "serde") (req "^1") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1") (default-features #t) (kind 0)))) (hash "1lishhg4hsqvs1am5syq7qqd40lf8c5n8ha7wp7x6kwc0yvmzm7f") (features (quote (("docs"))))))

