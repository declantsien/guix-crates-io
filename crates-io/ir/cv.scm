(define-module (crates-io ir cv) #:use-module (crates-io))

(define-public crate-ircv3-0.0.1 (crate (name "ircv3") (vers "0.0.1") (hash "0jw2x7cm60gdvd4mz05xz4p1q6sfb48v5xzz5h6qlyiyf2vx8hy2")))

(define-public crate-ircv3_parse-0.0.2 (crate (name "ircv3_parse") (vers "0.0.2") (deps (list (crate-dep (name "ircv3_tags") (req "^0.0.2") (default-features #t) (kind 0)) (crate-dep (name "nom") (req "^7.1.3") (default-features #t) (kind 0)) (crate-dep (name "pretty_assertions") (req "^1.4.0") (default-features #t) (kind 2)))) (hash "1vz8rsinnxbb5wf1df29s7kl9aqijvsij694fk5fqrjdj544zgv2")))

(define-public crate-ircv3_parse-0.0.3 (crate (name "ircv3_parse") (vers "0.0.3") (deps (list (crate-dep (name "ircv3_tags") (req "^0.0.4") (default-features #t) (kind 0)) (crate-dep (name "nom") (req "^7.1.3") (default-features #t) (kind 0)) (crate-dep (name "pretty_assertions") (req "^1.4.0") (default-features #t) (kind 2)))) (hash "1pqfxkbh7smsd0g0mzyb4h55x61amikkqzl6rxgijr2l7p4nqkds")))

(define-public crate-ircv3_parse-0.0.4 (crate (name "ircv3_parse") (vers "0.0.4") (deps (list (crate-dep (name "ircv3_tags") (req "^0.0.4") (default-features #t) (kind 0)) (crate-dep (name "nom") (req "^7.1.3") (default-features #t) (kind 0)) (crate-dep (name "pretty_assertions") (req "^1.4.0") (default-features #t) (kind 2)))) (hash "0m4yvcyp06i5ngwzcxh00qciq6qkfzk4dykbqd59qslbb12vzrjs")))

(define-public crate-ircv3_parse-0.0.5 (crate (name "ircv3_parse") (vers "0.0.5") (deps (list (crate-dep (name "ircv3_tags") (req "^0.0.4") (default-features #t) (kind 0)) (crate-dep (name "nom") (req "^7.1.3") (default-features #t) (kind 0)) (crate-dep (name "pretty_assertions") (req "^1.4.0") (default-features #t) (kind 2)))) (hash "0b2ijwfp6lircyqzapr369438as3l0yk8pdpaacy6rf34kf6l74g")))

(define-public crate-ircv3_parse-0.0.6 (crate (name "ircv3_parse") (vers "0.0.6") (deps (list (crate-dep (name "ircv3_tags") (req "^0.0.4") (default-features #t) (kind 0)) (crate-dep (name "nom") (req "^7.1.3") (default-features #t) (kind 0)) (crate-dep (name "pretty_assertions") (req "^1.4.0") (default-features #t) (kind 2)))) (hash "1wzqsp11c7qyvlx3nv3aj867zhqny6rsbdxp5ypn5kw0wc6sfbr6")))

(define-public crate-ircv3_parse-0.0.7 (crate (name "ircv3_parse") (vers "0.0.7") (deps (list (crate-dep (name "ircv3_tags") (req "^0.0.4") (default-features #t) (kind 0)) (crate-dep (name "nom") (req "^7.1.3") (default-features #t) (kind 0)) (crate-dep (name "pretty_assertions") (req "^1.4.0") (default-features #t) (kind 2)))) (hash "1yvs0mgfkk6agc88l6wg7yk9x5qq2mz373rl84kmy4qxv0zphzfp")))

(define-public crate-ircv3_parse-0.0.8 (crate (name "ircv3_parse") (vers "0.0.8") (deps (list (crate-dep (name "ircv3_tags") (req "^0.0.6") (default-features #t) (kind 0)) (crate-dep (name "nom") (req "^7.1.3") (default-features #t) (kind 0)) (crate-dep (name "pretty_assertions") (req "^1.4.0") (default-features #t) (kind 2)))) (hash "0z58wiwg0qi265f1aqcriprvkjr4pm86ppv0nhsakk3drjj48mqd")))

(define-public crate-ircv3_parse-0.1 (crate (name "ircv3_parse") (vers "0.1.0") (deps (list (crate-dep (name "ircv3_tags") (req "^0.1.1") (default-features #t) (kind 0)) (crate-dep (name "nom") (req "^7.1.3") (default-features #t) (kind 0)) (crate-dep (name "pretty_assertions") (req "^1.4.0") (default-features #t) (kind 2)))) (hash "064iw213v9gk9zy6dy6g47qa6hbr27421gd3j1bdfzb7fyxy5dqa")))

(define-public crate-ircv3_parse-0.1 (crate (name "ircv3_parse") (vers "0.1.1") (deps (list (crate-dep (name "ircv3_tags") (req "^0.1.1") (default-features #t) (kind 0)) (crate-dep (name "nom") (req "^7.1.3") (default-features #t) (kind 0)) (crate-dep (name "pretty_assertions") (req "^1.4.0") (default-features #t) (kind 2)))) (hash "18r71i4csjqzgcqg4xgarj53m0qzis71nqd2abahk77sfi0c9waz")))

(define-public crate-ircv3_parse-0.1 (crate (name "ircv3_parse") (vers "0.1.2") (deps (list (crate-dep (name "ircv3_tags") (req "^0.1.1") (default-features #t) (kind 0)) (crate-dep (name "nom") (req "^7.1.3") (default-features #t) (kind 0)) (crate-dep (name "pretty_assertions") (req "^1.4.0") (default-features #t) (kind 2)))) (hash "11qqi9yfhnyv66amw2i6idi4a18g67yqlmj2sprw9qska7557kvl")))

(define-public crate-ircv3_parse-0.1 (crate (name "ircv3_parse") (vers "0.1.3") (deps (list (crate-dep (name "ircv3_tags") (req "^0.1.2") (default-features #t) (kind 0)) (crate-dep (name "nom") (req "^7.1.3") (default-features #t) (kind 0)) (crate-dep (name "pretty_assertions") (req "^1.4.0") (default-features #t) (kind 2)))) (hash "1fwj1kkm68mjbamwv0qihmqsv97mpjzv69mrnv7p5xy9w7b021la")))

(define-public crate-ircv3_parse-0.1 (crate (name "ircv3_parse") (vers "0.1.4") (deps (list (crate-dep (name "ircv3_tags") (req "^0.1.3") (default-features #t) (kind 0)) (crate-dep (name "nom") (req "^7.1.3") (default-features #t) (kind 0)) (crate-dep (name "pretty_assertions") (req "^1.4.0") (default-features #t) (kind 2)))) (hash "15n35mb1d09wiz2xs7zkq8wv1kqz40mm82sv6jd154fxk9gs9spw")))

(define-public crate-ircv3_tags-0.0.2 (crate (name "ircv3_tags") (vers "0.0.2") (deps (list (crate-dep (name "nom") (req "^7.1.3") (default-features #t) (kind 0)) (crate-dep (name "pretty_assertions") (req "^1.4.0") (default-features #t) (kind 2)))) (hash "0xgh66dd17rhav1bmjxzs3qzf6a2qjfbmfh5hglkk72jhfg2awqz")))

(define-public crate-ircv3_tags-0.0.3 (crate (name "ircv3_tags") (vers "0.0.3") (deps (list (crate-dep (name "nom") (req "^7.1.3") (default-features #t) (kind 0)) (crate-dep (name "pretty_assertions") (req "^1.4.0") (default-features #t) (kind 2)))) (hash "03ia03vy87nj66nmgb2awycjipmzcxafg1nq3fblxbbaq8rfm1cb")))

(define-public crate-ircv3_tags-0.0.4 (crate (name "ircv3_tags") (vers "0.0.4") (deps (list (crate-dep (name "nom") (req "^7.1.3") (default-features #t) (kind 0)) (crate-dep (name "pretty_assertions") (req "^1.4.0") (default-features #t) (kind 2)))) (hash "1mz0gxlhqa3xdhamary4nlp18i9wpmsbpm61fkpw4i2savr6jpdp")))

(define-public crate-ircv3_tags-0.0.5 (crate (name "ircv3_tags") (vers "0.0.5") (deps (list (crate-dep (name "nom") (req "^7.1.3") (default-features #t) (kind 0)) (crate-dep (name "pretty_assertions") (req "^1.4.0") (default-features #t) (kind 2)))) (hash "0xq132k85kyj3nagig8x0xw3aglfhnj1xvq6drdwgfcjgxbqyy8a")))

(define-public crate-ircv3_tags-0.0.6 (crate (name "ircv3_tags") (vers "0.0.6") (deps (list (crate-dep (name "nom") (req "^7.1.3") (default-features #t) (kind 0)) (crate-dep (name "pretty_assertions") (req "^1.4.0") (default-features #t) (kind 2)))) (hash "02vci3llkaqhawmxdrfsbz519yvl7nqc2nx1ca6fvayhs0jg94jd")))

(define-public crate-ircv3_tags-0.1 (crate (name "ircv3_tags") (vers "0.1.0") (deps (list (crate-dep (name "nom") (req "^7.1.3") (default-features #t) (kind 0)) (crate-dep (name "pretty_assertions") (req "^1.4.0") (default-features #t) (kind 2)))) (hash "00q0jxl198rimkn2a0izdsdc0jc89v2kc2nyl0afl1k13x5ssd8r")))

(define-public crate-ircv3_tags-0.1 (crate (name "ircv3_tags") (vers "0.1.1") (deps (list (crate-dep (name "nom") (req "^7.1.3") (default-features #t) (kind 0)) (crate-dep (name "pretty_assertions") (req "^1.4.0") (default-features #t) (kind 2)))) (hash "1znqm9sy6rcq1zrz5lws77indnd069j1kck46p48q5cplpxjj6kd")))

(define-public crate-ircv3_tags-0.1 (crate (name "ircv3_tags") (vers "0.1.2") (deps (list (crate-dep (name "nom") (req "^7.1.3") (default-features #t) (kind 0)))) (hash "1dv096ycd31qyvim1fl4fgrakl3ar9a1wx2h0g83si18iqys2qrr")))

(define-public crate-ircv3_tags-0.1 (crate (name "ircv3_tags") (vers "0.1.3") (deps (list (crate-dep (name "nom") (req "^7.1.3") (default-features #t) (kind 0)))) (hash "0shsn4fg62afbq2fih9ysgh0awdwsz3x2sik7ww5rqv6v6rcchhy")))

