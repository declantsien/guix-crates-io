(define-module (crates-io ir eg) #:use-module (crates-io))

(define-public crate-iregexp-0.1 (crate (name "iregexp") (vers "0.1.0") (deps (list (crate-dep (name "pest") (req "^2.7") (default-features #t) (kind 0)) (crate-dep (name "pest_derive") (req "^2.7") (default-features #t) (kind 0)))) (hash "1i3nw8bw1d6yjjix1m7m9ra8p52x4s9gxsp2h58d35fyfk1vhwy0")))

(define-public crate-iregexp-0.1 (crate (name "iregexp") (vers "0.1.1") (deps (list (crate-dep (name "pest") (req "^2.7") (default-features #t) (kind 0)) (crate-dep (name "pest_derive") (req "^2.7") (default-features #t) (kind 0)))) (hash "0k57wcn2607qx9gngz31sm4034qr9mm8zhb00ljf7ry4k8y55151")))

