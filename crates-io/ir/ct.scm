(define-module (crates-io ir ct) #:use-module (crates-io))

(define-public crate-irctokens-0.1 (crate (name "irctokens") (vers "0.1.0") (deps (list (crate-dep (name "criterion") (req "^0.4.0") (default-features #t) (kind 2)))) (hash "0sn64icxiy2mhfg56qz33gmghkkdr5gccdpxy44gfh7j80bfbz91")))

(define-public crate-irctokens-0.1 (crate (name "irctokens") (vers "0.1.1") (deps (list (crate-dep (name "criterion") (req "^0.4.0") (default-features #t) (kind 2)))) (hash "04hf9mfnlqf08gx1pxgpnh8lm5pdi9vbdryfjvyjcx5hhlcmmy9g")))

(define-public crate-irctokens-0.1 (crate (name "irctokens") (vers "0.1.2") (deps (list (crate-dep (name "criterion") (req "^0.4.0") (default-features #t) (kind 2)))) (hash "0kxxbl4aaacdw86pg77cd9cgqr454m5hvbxvls6wylsk7h8iy388")))

(define-public crate-irctokens-0.1 (crate (name "irctokens") (vers "0.1.3") (deps (list (crate-dep (name "criterion") (req "^0.4.0") (default-features #t) (kind 2)))) (hash "0q3p263n699qkvx7w5wx6yw7w3n2kwbwsm50m2ddqbjzqkhxwsyi")))

