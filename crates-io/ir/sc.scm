(define-module (crates-io ir sc) #:use-module (crates-io))

(define-public crate-irsc-0.0.1 (crate (name "irsc") (vers "0.0.1") (hash "049fl8rndh4wwcf27qpcyhsdsdwskzd4krg71kzj9042kw84jrsd")))

(define-public crate-irsc-0.0.2 (crate (name "irsc") (vers "0.0.2") (hash "0k4l8l4zrknfdqnvf3ki64xq158bxp397a5n3jw4yww1q6np6jz1")))

(define-public crate-irsc-0.0.3 (crate (name "irsc") (vers "0.0.3") (hash "0lwsimwmr73z7l9yz2h9327cq4ycnc7yf7gqdc48wv1rfh96p25l")))

(define-public crate-irsc-0.1 (crate (name "irsc") (vers "0.1.0") (deps (list (crate-dep (name "env_logger") (req "*") (default-features #t) (kind 2)) (crate-dep (name "log") (req "*") (default-features #t) (kind 0)) (crate-dep (name "openssl") (req "*") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "regex") (req "*") (default-features #t) (kind 0)) (crate-dep (name "regex_macros") (req "*") (default-features #t) (kind 0)))) (hash "0yiixwxzvrdkq0s09lacmnmc0m0lsflypapbh98lc55jgkvpys3x") (features (quote (("ssl" "openssl"))))))

(define-public crate-irsc-0.1 (crate (name "irsc") (vers "0.1.1") (deps (list (crate-dep (name "env_logger") (req "*") (default-features #t) (kind 2)) (crate-dep (name "log") (req "*") (default-features #t) (kind 0)) (crate-dep (name "openssl") (req "*") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "regex") (req "*") (default-features #t) (kind 0)) (crate-dep (name "regex_macros") (req "*") (default-features #t) (kind 0)))) (hash "1cvkyiydq27nhsil446gw3cninwhwrdvmdhv2r1ilwwx8pllcv9s") (features (quote (("ssl" "openssl"))))))

(define-public crate-irsc-0.2 (crate (name "irsc") (vers "0.2.0") (deps (list (crate-dep (name "carboxyl") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "clippy") (req "*") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "env_logger") (req "*") (default-features #t) (kind 2)) (crate-dep (name "log") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "openssl") (req "^0.6") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "regex_macros") (req "^0.1") (default-features #t) (kind 0)))) (hash "19l83gfdsnnlj844x1hc203lpb5vdccdy8x9l93vc69djamwl499") (features (quote (("lints" "clippy")))) (yanked #t)))

