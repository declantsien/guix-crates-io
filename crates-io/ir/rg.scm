(define-module (crates-io ir rg) #:use-module (crates-io))

(define-public crate-irrgarten-0.1 (crate (name "irrgarten") (vers "0.1.0") (deps (list (crate-dep (name "rand") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0") (default-features #t) (kind 0)))) (hash "0v4k15sv745z090mval4nif09rq4bk3vvyqcvg1zh13kmav2bdgw")))

(define-public crate-irrgarten-0.1 (crate (name "irrgarten") (vers "0.1.1") (deps (list (crate-dep (name "rand") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0") (default-features #t) (kind 0)))) (hash "1aj3zrrff8vq8spr2dg7jm82mwg484gqs2kjrsswir6g0q8g9jfs")))

