(define-module (crates-io ir o-) #:use-module (crates-io))

(define-public crate-iro-cli-0.2 (crate (name "iro-cli") (vers "0.2.1") (deps (list (crate-dep (name "colorconv") (req "^0.1.1") (default-features #t) (kind 0)) (crate-dep (name "colored") (req "^2.0.0") (default-features #t) (kind 0)) (crate-dep (name "env_logger") (req "^0.9.1") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.17") (default-features #t) (kind 0)))) (hash "0zr2d0xyna4fmmzw5iqnrk9nahjgb07fi6q8c16lq34hl9ay7540")))

