(define-module (crates-io ir re) #:use-module (crates-io))

(define-public crate-irrefutable-0.1 (crate (name "irrefutable") (vers "0.1.0") (deps (list (crate-dep (name "proc-macro2") (req "^1.0.10") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.3") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.17") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "1xyxvykiap7bllsssb7vg2lgyyzg1mxg7yyimjl5sir3yzxxf2pa")))

(define-public crate-irrefutable-0.1 (crate (name "irrefutable") (vers "0.1.1") (deps (list (crate-dep (name "proc-macro2") (req "^1.0.10") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.3") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.17") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "0kq6bndbw07sswf390c7x37yy0qnq0r5p4nd32ihi0x40cvj48j1")))

(define-public crate-irrefutable-0.1 (crate (name "irrefutable") (vers "0.1.2") (deps (list (crate-dep (name "proc-macro2") (req "^1.0.10") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.3") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.18") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "1ja67bkfz3h29qpx63s97qgmpwij46n3zlh5r1bm921mpnmp4pl4")))

(define-public crate-irrefutable-0.1 (crate (name "irrefutable") (vers "0.1.3") (deps (list (crate-dep (name "proc-macro2") (req "^1.0.18") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.7") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.30") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "0zsh3y5p2ygzms26gvsmc38fddxrl4l0b30y05si6jxyi69f6f8x")))

(define-public crate-irregex-0.1 (crate (name "irregex") (vers "0.1.0") (deps (list (crate-dep (name "nom") (req "^7.1.3") (default-features #t) (kind 0)))) (hash "0q2qhvxcvaknqws77ddc5xgyh5camhxcx9lx3rp710g5qxw6wzml")))

(define-public crate-irrelevant-0.1 (crate (name "irrelevant") (vers "0.1.0") (hash "1sl7w5k6lsari47b8z0sw02m3ic3sbhggzicnrxylbfg14znlwvc")))

