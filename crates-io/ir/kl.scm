(define-module (crates-io ir kl) #:use-module (crates-io))

(define-public crate-irkle-0.1 (crate (name "irkle") (vers "0.1.0-pre.1") (deps (list (crate-dep (name "blake3") (req "^0.3.7") (default-features #t) (kind 0)))) (hash "0s282k6gkx9nraxs90089nyvvig51kwcakzrwpglypjd2wv4n90l")))

