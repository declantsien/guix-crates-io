(define-module (crates-io ir cl) #:use-module (crates-io))

(define-public crate-irclap-0.1 (crate (name "irclap") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^2.30") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^2.30") (features (quote ("yaml"))) (default-features #t) (kind 2)) (crate-dep (name "futures") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "irc") (req "^0.13") (default-features #t) (kind 0)) (crate-dep (name "tokio-core") (req "^0.1") (default-features #t) (kind 0)))) (hash "1m0wl26giqrwpj49xard7mbk9vlvm3319mvi3759gdyk3kz18ilf")))

