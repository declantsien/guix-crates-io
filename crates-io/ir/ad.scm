(define-module (crates-io ir ad) #:use-module (crates-io))

(define-public crate-iradix-0.0.0 (crate (name "iradix") (vers "0.0.0") (deps (list (crate-dep (name "criterion") (req "^0.5") (default-features #t) (kind 2)) (crate-dep (name "tempfile") (req "^3") (default-features #t) (kind 2)))) (hash "0h93jw1q7rvh5w6smcgb4wj1mi329jdy2yc4xd2nihswqyi7ik3z") (features (quote (("std") ("default")))) (yanked #t) (rust-version "1.73")))

(define-public crate-iradix-0.0.1 (crate (name "iradix") (vers "0.0.1") (deps (list (crate-dep (name "criterion") (req "^0.5") (default-features #t) (kind 2)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (optional #t) (kind 0)) (crate-dep (name "tempfile") (req "^3") (default-features #t) (kind 2)))) (hash "0b7yqmvkaiysyl9l3r7g9bbz8i3bfj797avxm11qy2z8kgnw718s") (features (quote (("default")))) (v 2) (features2 (quote (("std" "serde?/default")))) (rust-version "1.73")))

