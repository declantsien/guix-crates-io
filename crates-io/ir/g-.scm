(define-module (crates-io ir g-) #:use-module (crates-io))

(define-public crate-irg-kvariants-0.1 (crate (name "irg-kvariants") (vers "0.1.0") (deps (list (crate-dep (name "csv") (req "^1.1.6") (default-features #t) (kind 0)) (crate-dep (name "once_cell") (req "^1.16.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.148") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "csv") (req "^1.1.6") (default-features #t) (kind 1)) (crate-dep (name "serde") (req "^1.0.148") (features (quote ("derive"))) (default-features #t) (kind 1)))) (hash "0y6z9w8884hvnf67fa8h28yfwrc8h0kb94zwvgwrqqk3hcli8cn7")))

