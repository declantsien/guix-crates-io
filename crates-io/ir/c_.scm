(define-module (crates-io ir c_) #:use-module (crates-io))

(define-public crate-irc_message-0.0.1 (crate (name "irc_message") (vers "0.0.1") (hash "1yiw65y2z2y2vqbs3qpx0afj2c4y8vc261kmqqhcj15jp0iw1nb8")))

(define-public crate-irc_message-0.0.2 (crate (name "irc_message") (vers "0.0.2") (hash "0zjbj3qfwak2cxw687hqwvkxhh3bzk4pmkpikfrra383jwl807db")))

(define-public crate-irc_message-0.0.3 (crate (name "irc_message") (vers "0.0.3") (hash "0nraz2f3kg5z44b52r0gmj734jyh47xwx9ygk3akkmii3ysvn2vj")))

(define-public crate-irc_message-0.0.4 (crate (name "irc_message") (vers "0.0.4") (hash "0jdgr2168wz0j2v3vr2q0bpvl76dvnmr7lyck19b9ygllkwa0jfv")))

(define-public crate-irc_message-0.0.5 (crate (name "irc_message") (vers "0.0.5") (hash "1x7ila2h4lmvyj0ipwajhdn790xx2c4xhkq3mvh53rxnvbp36fqw")))

(define-public crate-irc_message-1 (crate (name "irc_message") (vers "1.0.0") (hash "0027kbc30khzbikg10f22xkck1c7kkw4ga7djq8bwmlq51hrmzzc")))

