(define-module (crates-io ir cs) #:use-module (crates-io))

(define-public crate-ircsim-0.1 (crate (name "ircsim") (vers "0.1.0") (deps (list (crate-dep (name "clearscreen") (req "^1.0.10") (default-features #t) (kind 0)) (crate-dep (name "colored") (req "^2.0.0") (default-features #t) (kind 0)) (crate-dep (name "crossterm") (req "^0.23.2") (default-features #t) (kind 0)))) (hash "0ibiv6ir92f7v897ywyavvh7cz6j410ayprfvhlh90xq069ppbfb")))

(define-public crate-ircsim-0.1 (crate (name "ircsim") (vers "0.1.1") (deps (list (crate-dep (name "clearscreen") (req "^1.0.10") (default-features #t) (kind 0)) (crate-dep (name "colored") (req "^2.0.0") (default-features #t) (kind 0)) (crate-dep (name "crossterm") (req "^0.23.2") (default-features #t) (kind 0)))) (hash "1gc34l76xlnqq3xrfmbvrry3715gs3p84q4k6dmqwsp869idivnz")))

(define-public crate-ircsim-0.1 (crate (name "ircsim") (vers "0.1.2") (deps (list (crate-dep (name "clearscreen") (req "^1.0.10") (default-features #t) (kind 0)) (crate-dep (name "colored") (req "^2.0.0") (default-features #t) (kind 0)) (crate-dep (name "crossterm") (req "^0.23.2") (default-features #t) (kind 0)))) (hash "0wd8b8a9zrj31zbvwwg38gfhv9rxhrky1ffvjjy74hgz9xi9pnq0")))

(define-public crate-ircsim-0.1 (crate (name "ircsim") (vers "0.1.3") (deps (list (crate-dep (name "clearscreen") (req "^1.0.10") (default-features #t) (kind 0)) (crate-dep (name "colored") (req "^2.0.0") (default-features #t) (kind 0)) (crate-dep (name "crossterm") (req "^0.23.2") (default-features #t) (kind 0)))) (hash "0fl9a46s7n88015mpc9gmy7ahwpz9nhwmpdkss6nqz126ba3cxfi")))

(define-public crate-ircsim-0.1 (crate (name "ircsim") (vers "0.1.4") (deps (list (crate-dep (name "clearscreen") (req "^1.0.10") (default-features #t) (kind 0)) (crate-dep (name "colored") (req "^2.0.0") (default-features #t) (kind 0)) (crate-dep (name "crossterm") (req "^0.23.2") (default-features #t) (kind 0)))) (hash "0mk47yfn517apnh4d0zd52m7i0h3ayyb2g2d63jhy2hli87iyzfd")))

(define-public crate-ircsim-0.1 (crate (name "ircsim") (vers "0.1.5") (deps (list (crate-dep (name "clearscreen") (req "^1.0.10") (default-features #t) (kind 0)) (crate-dep (name "colored") (req "^2.0.0") (default-features #t) (kind 0)) (crate-dep (name "crossterm") (req "^0.23.2") (default-features #t) (kind 0)))) (hash "0i15hwy7rs13m6fq6falsghgyrhzvx51kx15cqf0qkkxjs04wfar")))

