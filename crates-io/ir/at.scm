(define-module (crates-io ir at) #:use-module (crates-io))

(define-public crate-irating-0.1 (crate (name "irating") (vers "0.1.0") (deps (list (crate-dep (name "insta") (req "=1.34.0") (default-features #t) (kind 2)))) (hash "0ijrg2ilh8va45j8fk0k3mvlfrwhzqq4jycdzdyva2s4x3xvkysb")))

