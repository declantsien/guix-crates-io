(define-module (crates-io ir c3) #:use-module (crates-io))

(define-public crate-irc3-0.1 (crate (name "irc3") (vers "0.1.0") (deps (list (crate-dep (name "async-std") (req "^1.5") (default-features #t) (kind 0)) (crate-dep (name "futures") (req "^0.3") (default-features #t) (kind 0)))) (hash "08hzln4scxy92flb6fgp1prsxi2i7n63z90333pqmhwjh1apk9pi")))

(define-public crate-irc3-0.1 (crate (name "irc3") (vers "0.1.1") (deps (list (crate-dep (name "async-std") (req "^1.5") (default-features #t) (kind 0)) (crate-dep (name "futures") (req "^0.3") (default-features #t) (kind 0)))) (hash "1ii1nr3s0a1irsv4sis34dq4pvbli314lyf429zmhslhnbay7sgc")))

(define-public crate-irc3-0.2 (crate (name "irc3") (vers "0.2.0") (deps (list (crate-dep (name "async-std") (req "^1.5") (default-features #t) (kind 0)) (crate-dep (name "futures") (req "^0.3") (default-features #t) (kind 0)))) (hash "1i8vp8xwda3541778c64qa5hb3h7isbwkq7mpcm953dvkxmhnvkh")))

(define-public crate-irc3-0.2 (crate (name "irc3") (vers "0.2.1") (deps (list (crate-dep (name "async-std") (req "^1.5") (default-features #t) (kind 0)) (crate-dep (name "futures") (req "^0.3") (default-features #t) (kind 0)))) (hash "0lbphb2ih10pgb3hl6rs54vqjrgkmaqsgflxnplfy2j3x3mkijl7")))

(define-public crate-irc3-0.2 (crate (name "irc3") (vers "0.2.2") (deps (list (crate-dep (name "async-std") (req "^1.5") (default-features #t) (kind 0)) (crate-dep (name "futures") (req "^0.3") (default-features #t) (kind 0)))) (hash "1bif4017ycy84rhnym3mqw6ib9q43djz3rcf9dhshgdpxskgxhqn")))

(define-public crate-irc3-0.3 (crate (name "irc3") (vers "0.3.0") (deps (list (crate-dep (name "async-std") (req "^1.5") (default-features #t) (kind 0)) (crate-dep (name "async-tls") (req "^0.6") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "futures") (req "^0.3") (default-features #t) (kind 0)))) (hash "1ib2xaln9i6gbbjc4adbwf53lza14nf8474l1qf2inb0r8xgqx32") (features (quote (("tls" "async-tls") ("default"))))))

(define-public crate-irc3-sdk-0.0.1 (crate (name "irc3-sdk") (vers "0.0.1") (hash "00xr9d5fcy1gd9r5y96kr4pjx7l0c8kgg806f2dmapgi0jhq1r7p")))

