(define-module (crates-io dg l-) #:use-module (crates-io))

(define-public crate-dgl-gl-0.3 (crate (name "dgl-gl") (vers "0.3.0") (deps (list (crate-dep (name "gl_generator") (req "^0.5.3") (default-features #t) (kind 1)))) (hash "10c0cvp7lgrxykh65rcl45ypzmmsrypm1l2dqrjiqinvhblq1qvs") (yanked #t)))

