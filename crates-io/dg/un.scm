(define-module (crates-io dg un) #:use-module (crates-io))

(define-public crate-dgunther2001-linked_list-0.1 (crate (name "dgunther2001-linked_list") (vers "0.1.0") (hash "0cj1w4cx7khxzn7j785vxbnyawazww7hxzmkbzd82fcnwzdm562k")))

(define-public crate-dgunther2001-linked_list-1 (crate (name "dgunther2001-linked_list") (vers "1.0.6") (hash "00xm1z2bvrzmc9m76lha0vjdcgi977yzfcc6k0g33ykic7g6wh6d")))

(define-public crate-dgunther2001-linked_list-1 (crate (name "dgunther2001-linked_list") (vers "1.0.7") (hash "0w3nrawqyqf1ik2pkkxm0psxiwsdnm6dhpx5g1sxavf9kgvzxmyp")))

(define-public crate-dgunther2001-linked_list-1 (crate (name "dgunther2001-linked_list") (vers "1.0.8") (hash "0f0qzph1651fv5vxxz90dx08swxzhny25vhc71gc8dnh9ivpfc36")))

