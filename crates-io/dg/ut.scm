(define-module (crates-io dg ut) #:use-module (crates-io))

(define-public crate-dgut-reqwest-0.1 (crate (name "dgut-reqwest") (vers "0.1.0") (deps (list (crate-dep (name "reqwest") (req "^0.11") (features (quote ("json" "blocking"))) (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "0vbkp4mdxpm525blns9d2z2a0pr4qwkivf3i45l4cdphkn1h6l7c") (yanked #t)))

(define-public crate-dgut-reqwest-0.1 (crate (name "dgut-reqwest") (vers "0.1.1") (deps (list (crate-dep (name "reqwest") (req "^0.11") (features (quote ("json" "blocking"))) (default-features #t) (kind 0)))) (hash "1nlriv0866kksldml96hb3w75nq5ypzs8dqn8axd6ysm5dy63bkl") (yanked #t)))

