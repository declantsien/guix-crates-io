(define-module (crates-io dg #{65}#) #:use-module (crates-io))

(define-public crate-dg6502-0.1 (crate (name "dg6502") (vers "0.1.0") (deps (list (crate-dep (name "crossterm") (req "^0.26.1") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.147") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.88") (default-features #t) (kind 0)))) (hash "03vhzm2jkyz7897pn2ykid0dgm2y316jkaaqij42y24hz7pdnn59")))

