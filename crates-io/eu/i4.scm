(define-module (crates-io eu i4) #:use-module (crates-io))

(define-public crate-eui48-0.1 (crate (name "eui48") (vers "0.1.0") (deps (list (crate-dep (name "rustc-serialize") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^0.6") (optional #t) (default-features #t) (kind 0)))) (hash "1ayw4vpkjg974bqbixhc8l1fcdd473hc01vsjm5mwg69nb2y48km")))

(define-public crate-eui48-0.3 (crate (name "eui48") (vers "0.3.0") (deps (list (crate-dep (name "rustc-serialize") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "~1") (optional #t) (default-features #t) (kind 0)))) (hash "1ng06z6v5zj3bh905v082n5kdr5y3civsy2dvcxx4p3n8mkxxf62")))

(define-public crate-eui48-0.3 (crate (name "eui48") (vers "0.3.1") (deps (list (crate-dep (name "rustc-serialize") (req "^0.3.24") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.27") (optional #t) (default-features #t) (kind 0)))) (hash "015wn3hsf9ng85vgja4vagplkdspkh1sigcw86a5wzaqwv7fwf9j")))

(define-public crate-eui48-0.3 (crate (name "eui48") (vers "0.3.2") (deps (list (crate-dep (name "rustc-serialize") (req "^0.3.24") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.27") (optional #t) (default-features #t) (kind 0)))) (hash "0mmdhczfdxwv5v5h90ydqkx0mdqiv0h2clshm2cm4qlwp0gacw29")))

(define-public crate-eui48-0.4 (crate (name "eui48") (vers "0.4.0") (deps (list (crate-dep (name "rustc-serialize") (req "^0.3.24") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.80") (optional #t) (default-features #t) (kind 0)))) (hash "0xp44gnx80zcb6dz4msrjgzwmix9fyxl1vkvahncpwjdfj6h2lax")))

(define-public crate-eui48-0.4 (crate (name "eui48") (vers "0.4.1") (deps (list (crate-dep (name "rustc-serialize") (req "^0.3.24") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.80") (optional #t) (default-features #t) (kind 0)))) (hash "13hillaards606s515yhmbcq2fkprrswdnif0zdsl5nw2fqsv7b4")))

(define-public crate-eui48-0.4 (crate (name "eui48") (vers "0.4.2") (deps (list (crate-dep (name "rustc-serialize") (req "^0.3.24") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.80") (optional #t) (default-features #t) (kind 0)))) (hash "0sx8pwvbwvgmi989wpczx3mw9fk0f13z1a14hmgnr14g2x6k5z1z")))

(define-public crate-eui48-0.4 (crate (name "eui48") (vers "0.4.3") (deps (list (crate-dep (name "rustc-serialize") (req "^0.3.24") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.80") (optional #t) (default-features #t) (kind 0)))) (hash "0kg6hlxmcj7j1afkax6a5c9ma3phniplhscasqfmwxjp38ajgq06")))

(define-public crate-eui48-0.4 (crate (name "eui48") (vers "0.4.4") (deps (list (crate-dep (name "rustc-serialize") (req "^0.3.24") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.80") (optional #t) (default-features #t) (kind 0)))) (hash "0vryczmj5irhc2lin4glnhrygkvddyzajl308vkxvfs2ln1yqp5x")))

(define-public crate-eui48-0.4 (crate (name "eui48") (vers "0.4.5") (deps (list (crate-dep (name "rustc-serialize") (req "^0.3.24") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.80") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.37") (optional #t) (default-features #t) (kind 0)))) (hash "1qb32nxrmjx84878yp887rn9b8ny35gfnd29zl11lch6h49q39c1")))

(define-public crate-eui48-0.4 (crate (name "eui48") (vers "0.4.6") (deps (list (crate-dep (name "rustc-serialize") (req "^0.3.24") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.80") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.37") (optional #t) (default-features #t) (kind 0)))) (hash "0sqbmcnvilanzjagknmpf85pnji2b9hn2pqzd5rygrfkwikghk4c") (features (quote (("disp_hexstring") ("default"))))))

(define-public crate-eui48-0.5 (crate (name "eui48") (vers "0.5.0") (deps (list (crate-dep (name "rustc-serialize") (req "^0.3.24") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.106") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.51") (optional #t) (default-features #t) (kind 0)))) (hash "1pdrcqz5917dy7rq2i4i6d42m5dk3rfhb5b0mfq7yvkhljirw8wx") (features (quote (("disp_hexstring") ("default" "rustc-serialize"))))))

(define-public crate-eui48-1 (crate (name "eui48") (vers "1.0.0") (deps (list (crate-dep (name "regex") (req "^1.3.7") (default-features #t) (kind 0)) (crate-dep (name "rustc-serialize") (req "^0.3.24") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.106") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.51") (optional #t) (default-features #t) (kind 0)))) (hash "1xqbs1jl17zv1xc9w9393rywncpyy66vlrwdpzzm7875m47b3cp3") (features (quote (("disp_hexstring") ("default" "rustc-serialize"))))))

(define-public crate-eui48-1 (crate (name "eui48") (vers "1.0.1") (deps (list (crate-dep (name "regex") (req "^1.3.9") (default-features #t) (kind 0)) (crate-dep (name "rustc-serialize") (req "^0.3.24") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.114") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.56") (optional #t) (default-features #t) (kind 0)))) (hash "0lliq2ssnj9l8x6whl6rmdr5y3gw1l9kzqy2r5ba58qayd7j28x9") (features (quote (("disp_hexstring") ("default" "rustc-serialize"))))))

(define-public crate-eui48-0.5 (crate (name "eui48") (vers "0.5.1") (deps (list (crate-dep (name "rustc-serialize") (req "^0.3.24") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.114") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.56") (optional #t) (default-features #t) (kind 0)))) (hash "19yfy6yphbvlh70qjclf68nrsi9yf5w407i5612mh44davrpkn5f") (features (quote (("disp_hexstring") ("default" "rustc-serialize"))))))

(define-public crate-eui48-1 (crate (name "eui48") (vers "1.1.0") (deps (list (crate-dep (name "bincode") (req "^1.3.1") (default-features #t) (kind 2)) (crate-dep (name "regex") (req "^1.3.9") (default-features #t) (kind 0)) (crate-dep (name "rustc-serialize") (req "^0.3.24") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.114") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.56") (optional #t) (default-features #t) (kind 0)))) (hash "00cpf25kc3mxhqnahm0bw9xl19gr2pzc5g84dvkc4mwdbsn1hx48") (features (quote (("serde_bytes" "serde") ("disp_hexstring") ("default" "rustc-serialize"))))))

