(define-module (crates-io eu _v) #:use-module (crates-io))

(define-public crate-eu_vat_id-0.1 (crate (name "eu_vat_id") (vers "0.1.0") (deps (list (crate-dep (name "failure") (req "^0.1.1") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.0.2") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^0.2.11") (default-features #t) (kind 0)))) (hash "1d51yh3ps596cmkcvcibybyd5i0lk9ab36psw8jhrfz826k1ajgl")))

