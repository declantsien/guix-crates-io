(define-module (crates-io eu ro) #:use-module (crates-io))

(define-public crate-euroc-0.1 (crate (name "euroc") (vers "0.1.0") (deps (list (crate-dep (name "anyhow") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "csv") (req "^1.1") (default-features #t) (kind 0)) (crate-dep (name "image") (req "^0.23") (default-features #t) (kind 0)) (crate-dep (name "nalgebra") (req "^0.29") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.130") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "yaml-rust") (req "^0.4") (default-features #t) (kind 0)))) (hash "1rn8d8kw5fi5qs4n2mwkd40qq4jqlz2rb1byvpk4lcxz8s9zj3pa")))

(define-public crate-europa-0.0.1 (crate (name "europa") (vers "0.0.1") (hash "0zzz29w5q5803vnbg2jvnybk2gkx3zv6kqpgdi6h6j06yf04yc27")))

(define-public crate-europa_net-0.1 (crate (name "europa_net") (vers "0.1.0") (hash "00n0ig9z70a02iaax8rnp7cx7hj3mg9622gh935ci8r07xaxwplx")))

(define-public crate-europa_net-0.1 (crate (name "europa_net") (vers "0.1.1") (hash "1bf7hf3kqw7xvy5w8iv7r9qk8p8nw26g5x8hca7p7x5q7sxawl44")))

(define-public crate-europe-elects-csv-0.0.1 (crate (name "europe-elects-csv") (vers "0.0.1") (deps (list (crate-dep (name "chrono") (req "^0.4.37") (features (quote ("serde"))) (default-features #t) (kind 0)) (crate-dep (name "csv") (req "^1.3.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.197") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.58") (default-features #t) (kind 0)))) (hash "0fpk4gsrl4qy7l1w3lch6rnmy48rpfjvlf7jalqjkxy25k0v1h1k")))

(define-public crate-eurorack-oxide-utils-0.1 (crate (name "eurorack-oxide-utils") (vers "0.1.0") (hash "16ycdxwgg5g8pkwfv15vf29ncn0x5mbshx31imv2sf97i51a4z45")))

(define-public crate-eurorack-oxide-utils-0.1 (crate (name "eurorack-oxide-utils") (vers "0.1.1") (hash "1hkjqd65d9hbgnxhwwjcfnk1m907pamdlifz9qap7smxzl04xzai")))

(define-public crate-eurorack-oxide-utils-0.1 (crate (name "eurorack-oxide-utils") (vers "0.1.2") (deps (list (crate-dep (name "micromath") (req "^1.0.0") (default-features #t) (kind 0)))) (hash "0g743bjqv2wk3agaj2pd3xrvb2zb189sh93ngq7cxzi74ikf43bs")))

(define-public crate-eurorack-oxide-utils-0.1 (crate (name "eurorack-oxide-utils") (vers "0.1.3") (deps (list (crate-dep (name "micromath") (req "^1.0.0") (default-features #t) (kind 0)))) (hash "1jdghir1rqdagnp5w9z8d5s9bbhcpkh3dj2y08pzjw63m20bf12b")))

(define-public crate-eurorack-oxide-utils-0.1 (crate (name "eurorack-oxide-utils") (vers "0.1.4") (deps (list (crate-dep (name "micromath") (req "^1.0.0") (default-features #t) (kind 0)))) (hash "0lm4lbzj7lqqyk7hp1644n68nvzcyw6dijlc4439q0cmn3ly3rk1")))

