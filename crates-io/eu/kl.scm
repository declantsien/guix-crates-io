(define-module (crates-io eu kl) #:use-module (crates-io))

(define-public crate-euklid-0.0.4 (crate (name "euklid") (vers "0.0.4") (deps (list (crate-dep (name "quickcheck") (req "^1.0") (default-features #t) (kind 2)) (crate-dep (name "quickcheck_macros") (req "^1.0") (default-features #t) (kind 2)))) (hash "1k04j7ylzhgsqq3mrmly5z87zzr5qimxvl1y98zqpz7rn00kgqyr")))

(define-public crate-euklidische_algo-0.1 (crate (name "euklidische_algo") (vers "0.1.0") (hash "09nyifakwwpmqvxsi1f3k2kng8gfqwrad2zp24gf2xdhlcxiphxi") (yanked #t)))

(define-public crate-euklidische_algo-0.1 (crate (name "euklidische_algo") (vers "0.1.1") (hash "1sgx372m7a9bavzyvy38gx7lpfhj5ihjci0rizycl75bwfhhyigl") (yanked #t)))

