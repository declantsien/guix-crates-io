(define-module (crates-io eu no) #:use-module (crates-io))

(define-public crate-eunomia-rs-0.1 (crate (name "eunomia-rs") (vers "0.1.0") (deps (list (crate-dep (name "bindgen") (req "^0.60.1") (default-features #t) (kind 1)) (crate-dep (name "link-cplusplus") (req "^1.0") (default-features #t) (kind 0)))) (hash "0vzmp1k9rzz8qdzpfwhxiziarixcwb7344cylhkbhy1qfic2a9zk")))

(define-public crate-eunomia-rs-0.1 (crate (name "eunomia-rs") (vers "0.1.1") (deps (list (crate-dep (name "anyhow") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "fastrand") (req "^1.6.0") (default-features #t) (kind 0)) (crate-dep (name "home") (req "^0.5.4") (default-features #t) (kind 0)))) (hash "1qvcxfcwpxh66ap5193r89n27s60kyr0v84ji136qkh281xcqhjk")))

