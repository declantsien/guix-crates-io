(define-module (crates-io eu c_) #:use-module (crates-io))

(define-public crate-euc_lib-0.1 (crate (name "euc_lib") (vers "0.1.0") (hash "0i2hf2l7bav3zwj98jq1zp5zc4dixvh27gcw8n46460dk4nhm7rf") (yanked #t)))

(define-public crate-euc_lib-0.1 (crate (name "euc_lib") (vers "0.1.1") (hash "042wma9kg0cjvf0rrss65lfgnw0rn3z7p86b0hwf63bz00ahqqm8") (yanked #t)))

(define-public crate-euc_lib-0.1 (crate (name "euc_lib") (vers "0.1.2") (hash "0rchw0irnbp7afqasymhbnkx6rp9w4p83ccfsx3w7vf72szp1m3p") (yanked #t)))

(define-public crate-euc_lib-0.1 (crate (name "euc_lib") (vers "0.1.3") (hash "098ggxkqqgqvl3kji0h704zjrskly986m2rmvwpwj82qqdhdnki5") (yanked #t)))

(define-public crate-euc_lib-0.1 (crate (name "euc_lib") (vers "0.1.4") (hash "1qcpmxizl4fd08jzxwbrm1b30mspvlijmk85bhqymhpx0zp8wfx0") (yanked #t)))

(define-public crate-euc_lib-0.1 (crate (name "euc_lib") (vers "0.1.5") (hash "1ma2wq2qsl2xjwy4vkb6sakn9cz705dpcpb6sr6050m4p4aj5iy0") (yanked #t)))

(define-public crate-euc_lib-0.1 (crate (name "euc_lib") (vers "0.1.6") (hash "0jdc15b6kynz59q2i7yxl6xhfbdjvmz4pqz8fc1fh0b9svpffkjx")))

(define-public crate-euc_lib-0.1 (crate (name "euc_lib") (vers "0.1.7-rc1") (hash "0npg175s8llmwp56g1i5crrr7vx655rwvcffba3g7mr2lw924lmn") (yanked #t)))

(define-public crate-euc_lib-0.1 (crate (name "euc_lib") (vers "0.1.7-rc2") (hash "1i25syn52cbbx3npvb1k3zgyvnx6aql99aj333fa3vdz6achpi9s") (yanked #t)))

(define-public crate-euc_lib-0.1 (crate (name "euc_lib") (vers "0.1.7") (hash "13ziij2h73vzki9km80j8q8qa0ag4dm0qain81gkw414fbsrwgaa") (yanked #t)))

(define-public crate-euc_lib-0.1 (crate (name "euc_lib") (vers "0.1.8-rc1") (hash "1mgsp182ac103ribjjhpp5w14hdmbvd9gn660fz9mhg851ra30zv") (yanked #t)))

(define-public crate-euc_lib-0.1 (crate (name "euc_lib") (vers "0.1.8") (hash "1q893vc969sf7yp9lad80p7pvrbvm1j3i0yx0a09qp062lmh3x2b") (yanked #t)))

(define-public crate-euc_lib-0.1 (crate (name "euc_lib") (vers "0.1.9") (hash "0mqv0lscd3r570s1izm4z8w0sgprlhpf7da5f708axjm37w5s02g")))

(define-public crate-euc_lib-0.2 (crate (name "euc_lib") (vers "0.2.1") (hash "0496j5bdwnayzfwwj1br3pwn9azmpaza7l8hgjih4khsjdcwkk6v") (yanked #t)))

(define-public crate-euc_lib-0.2 (crate (name "euc_lib") (vers "0.2.2") (hash "1fhhiwjaywqqgbiz39rfaa5xr5w9q0a90f2b4p7d42m2pa4slkyi") (yanked #t)))

(define-public crate-euc_lib-0.2 (crate (name "euc_lib") (vers "0.2.3") (hash "1yaf6w2ivkqck834fhw8xv7w4816k1l40hrqlrxcc3h81a1436h7") (yanked #t)))

(define-public crate-euc_lib-0.2 (crate (name "euc_lib") (vers "0.2.4") (hash "0a33wh1qy03rjym0rxkblnmk6vi1qrcc7yvd91qp47awkkcf0096") (yanked #t)))

(define-public crate-euc_lib-0.2 (crate (name "euc_lib") (vers "0.2.5") (hash "0iw1gy5ffqsj4w457z285z275xj8q77f139jk2lnchzs5b0is6l6") (yanked #t)))

(define-public crate-euc_lib-0.2 (crate (name "euc_lib") (vers "0.2.6") (hash "1r11q8gdvwjal000vqxkp0q1y65r6vqwcydxi4ni1nhbyqvm0k25")))

(define-public crate-euc_lib-0.3 (crate (name "euc_lib") (vers "0.3.0") (deps (list (crate-dep (name "num") (req "^0.4.0") (default-features #t) (kind 0)))) (hash "1d0jmhl6lkh56w64nmg01dzv5p10ab6a02kzmglk7k0qn5sba3qg") (yanked #t)))

(define-public crate-euc_lib-0.3 (crate (name "euc_lib") (vers "0.3.1") (deps (list (crate-dep (name "num") (req "^0.4.0") (default-features #t) (kind 0)))) (hash "1irxbvbwd4bywlyxvfsaa6iyrhq70yd6gkr3ac26pn176yl6ichs") (yanked #t)))

(define-public crate-euc_lib-0.3 (crate (name "euc_lib") (vers "0.3.2") (deps (list (crate-dep (name "num") (req "^0.4.0") (default-features #t) (kind 0)))) (hash "04vai89y49pv1rpqbphyd4z38j66scdfxb9nllsb25lsh07vgvkp")))

(define-public crate-euc_lib-0.4 (crate (name "euc_lib") (vers "0.4.0") (deps (list (crate-dep (name "num") (req "^0.4.0") (default-features #t) (kind 0)))) (hash "061xrdwwpny90ikllcr3ifk5vm7sa1xc2wjjl7gr597sh6nhyj1w")))

