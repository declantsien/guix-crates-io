(define-module (crates-io eu id) #:use-module (crates-io))

(define-public crate-euid-0.1 (crate (name "euid") (vers "0.1.0") (deps (list (crate-dep (name "criterion") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "getrandom") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 2)))) (hash "1radgpzxbrgi8mnq6hrjzw8a2jqlpsysrcdpyh19v4jq8ykqf0cx") (features (quote (("default" "checkmod_128") ("checkmod_64") ("checkmod_128"))))))

(define-public crate-euid-0.1 (crate (name "euid") (vers "0.1.1") (deps (list (crate-dep (name "criterion") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "getrandom") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 2)))) (hash "0pzzfvpcihy17yv1nr2j3vvd2gmwbbvssc9fmrxaf6hrh3jnnhax") (features (quote (("std") ("default" "std"))))))

(define-public crate-euid-0.1 (crate (name "euid") (vers "0.1.2") (deps (list (crate-dep (name "criterion") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "getrandom") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 2)))) (hash "1avsq13908fr1qyf0iywp0y2l8qvxlv8xfzfa39qvakn1j1q0f6s") (features (quote (("std") ("euid_64") ("default" "std"))))))

(define-public crate-euid-0.1 (crate (name "euid") (vers "0.1.3") (deps (list (crate-dep (name "criterion") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "getrandom") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 2)))) (hash "1c3p4ymb066d1bw44ba72kzxla0kys0k500kq5fyi6y41di64i36") (features (quote (("std") ("euid_64") ("default" "std"))))))

(define-public crate-euid-0.1 (crate (name "euid") (vers "0.1.4") (deps (list (crate-dep (name "criterion") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "getrandom") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 2)))) (hash "0z8qbkn6swrs2kvq70sj0k1l3ymjnkqj67w51hcbwvmarpn6c3zq") (features (quote (("std") ("euid_64") ("default" "std"))))))

(define-public crate-euid-0.1 (crate (name "euid") (vers "0.1.5") (deps (list (crate-dep (name "criterion") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "getrandom") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 2)))) (hash "0p43m900qpp05zx5p0zpaq0ndgh2j4gaw9d4xm2xc7dmhmjifzq9") (features (quote (("std") ("non_binary") ("euid_64") ("default" "std"))))))

(define-public crate-euid-0.1 (crate (name "euid") (vers "0.1.6") (deps (list (crate-dep (name "criterion") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "getrandom") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 2)))) (hash "17v4588qc3rxnm0pyhd7mjvcdgkdgb9hp56891c7mg29vndva71c") (features (quote (("std") ("non_binary") ("euid_64") ("default" "std"))))))

(define-public crate-euid-0.1 (crate (name "euid") (vers "0.1.7") (deps (list (crate-dep (name "criterion") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "getrandom") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 2)))) (hash "036jkla0h7gynlyay6s25qx4hsqz3r98vy4kjhj1cfkpc81q0ljm") (features (quote (("std") ("non_binary") ("euid_64") ("default" "std"))))))

(define-public crate-euid-toys-0.1 (crate (name "euid-toys") (vers "0.1.0") (deps (list (crate-dep (name "euid") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "0kyl36kyk8081fhj4pv1xl1ryh9ihckx8fpcii2vzvh6zrf5hq0j")))

(define-public crate-euid-toys-0.1 (crate (name "euid-toys") (vers "0.1.3") (deps (list (crate-dep (name "euid") (req "^0.1.3") (default-features #t) (kind 0)))) (hash "0rzkmb4psg06byd9fq6gd2dy7rnfnpsq1dcl64316msx5q9v7hq6")))

(define-public crate-euid-toys-0.1 (crate (name "euid-toys") (vers "0.1.5") (deps (list (crate-dep (name "euid") (req "^0.1.5") (default-features #t) (kind 0)))) (hash "1mlh9qhdsklbm1w53zxihs99bc9kfh2p6a66sqf0xq3a4ndbbndm")))

(define-public crate-euid-toys-0.1 (crate (name "euid-toys") (vers "0.1.6") (deps (list (crate-dep (name "euid") (req "^0.1.6") (default-features #t) (kind 0)))) (hash "00vkl20llnkf2s3s3w1gp6gk12qkayv726mwgdyf342am0xiln1d")))

(define-public crate-euid-toys-0.1 (crate (name "euid-toys") (vers "0.1.7") (deps (list (crate-dep (name "euid") (req "^0.1.7") (default-features #t) (kind 0)))) (hash "128l1l0h1rp44s56bwjg5kw354m3sg9w8nm6mql15wh4n961hmvf")))

