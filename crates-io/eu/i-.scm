(define-module (crates-io eu i-) #:use-module (crates-io))

(define-public crate-eui-no-std-0.1 (crate (name "eui-no-std") (vers "0.1.0") (deps (list (crate-dep (name "heapless") (req "^0.5") (default-features #t) (kind 0)))) (hash "069msk4rffhws13298scqqg4b0qr31qdndwv7q1vzcca6cp34hv4")))

(define-public crate-eui-no-std-0.2 (crate (name "eui-no-std") (vers "0.2.0") (deps (list (crate-dep (name "heapless") (req "^0.5") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (optional #t) (kind 0)) (crate-dep (name "serde_test") (req "^1.0") (default-features #t) (kind 2)))) (hash "01vra8akmzyk1ifqva07948kmb74895jc99yzg2xsj0ckvvfm5hx")))

(define-public crate-eui-no-std-0.2 (crate (name "eui-no-std") (vers "0.2.1") (deps (list (crate-dep (name "heapless") (req "^0.5") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (optional #t) (kind 0)) (crate-dep (name "serde_test") (req "^1.0") (default-features #t) (kind 2)))) (hash "1m49qkdwp69vbi8wyx60zgmwgniq4fw2i1s92isd7kf8hziq1dv4")))

(define-public crate-eui-no-std-0.2 (crate (name "eui-no-std") (vers "0.2.2") (deps (list (crate-dep (name "heapless") (req "^0.5") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (optional #t) (kind 0)) (crate-dep (name "serde_test") (req "^1.0") (default-features #t) (kind 2)))) (hash "1fgy90vfhxs3qzvfcgdlwqn4vxnhcpmx1llcwvfiz21bvqr44224")))

(define-public crate-eui-no-std-0.2 (crate (name "eui-no-std") (vers "0.2.3") (deps (list (crate-dep (name "hash32") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "hash32-derive") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "heapless") (req "^0.5") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (optional #t) (kind 0)) (crate-dep (name "serde_test") (req "^1.0") (default-features #t) (kind 2)))) (hash "1ganmbr10gi1h4ndjxkbalscywa8fpxrm9blksaqqsl767zrw029")))

(define-public crate-eui-no-std-0.2 (crate (name "eui-no-std") (vers "0.2.4") (deps (list (crate-dep (name "hash32") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "hash32-derive") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "heapless") (req "^0.5") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (optional #t) (kind 0)) (crate-dep (name "serde_test") (req "^1.0") (default-features #t) (kind 2)))) (hash "0clcjamlfrfpvrbpsj8q4m247hcl75q7ywhbgkl7xdjlwdk8bh4p")))

(define-public crate-eui-no-std-0.3 (crate (name "eui-no-std") (vers "0.3.0") (deps (list (crate-dep (name "hash32") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "hash32-derive") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "heapless") (req "^0.5") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (optional #t) (kind 0)) (crate-dep (name "serde_test") (req "^1.0") (default-features #t) (kind 2)))) (hash "0phffqqr5mxbqx6589x882dk5p64y1srrgvr3yhc336rrpj268iy")))

(define-public crate-eui-no-std-0.3 (crate (name "eui-no-std") (vers "0.3.1") (deps (list (crate-dep (name "hash32") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "hash32-derive") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "heapless") (req "^0.5") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (optional #t) (kind 0)) (crate-dep (name "serde_test") (req "^1.0") (default-features #t) (kind 2)))) (hash "0rxb52b1x7r5z7lhrvvlrhbif6bgfj6hk31irycfqlfz6xymg7ag")))

