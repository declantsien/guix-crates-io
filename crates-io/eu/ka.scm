(define-module (crates-io eu ka) #:use-module (crates-io))

(define-public crate-euka-0.0.1 (crate (name "euka") (vers "0.0.1") (deps (list (crate-dep (name "mohan") (req "^0.0.24") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "1p4brij30indhmj9nkihswin3dxk86p8kaf8ywncsl684x7i0ydm")))

