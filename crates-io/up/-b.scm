(define-module (crates-io up -b) #:use-module (crates-io))

(define-public crate-up-bank-api-0.1 (crate (name "up-bank-api") (vers "0.1.0") (deps (list (crate-dep (name "restson") (req "^1.0.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.130") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0.130") (default-features #t) (kind 0)))) (hash "00hswl871sadffnfybww7x92azzjrcvm64y9rl0zwz0fng221ly4")))

(define-public crate-up-bank-api-0.1 (crate (name "up-bank-api") (vers "0.1.1") (deps (list (crate-dep (name "restson") (req "^1.0.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.130") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0.130") (default-features #t) (kind 0)))) (hash "0cnpxyykqqpqf8nxh3v1yp4bgjqs7883w6c4f1p6fagkn6zp5v7k")))

(define-public crate-up-bank-api-0.1 (crate (name "up-bank-api") (vers "0.1.2") (deps (list (crate-dep (name "restson") (req "^1.0.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.130") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0.130") (default-features #t) (kind 0)) (crate-dep (name "toml") (req "^0.5.8") (default-features #t) (kind 2)))) (hash "1mljnzgj7r0vgmqn7slc68zix04y5g73zvv7q7s3jzgsqyniqwqb")))

