(define-module (crates-io up c-) #:use-module (crates-io))

(define-public crate-upc-checker-0.1 (crate (name "upc-checker") (vers "0.1.1") (hash "01n0i4v26ldcphyyk34pqlilz4m6b5589k1fgpfnpfhb7mbwrq0x")))

(define-public crate-upc-checker-0.1 (crate (name "upc-checker") (vers "0.1.2") (hash "1gq4bw1lzhqrs362n2py3g3jrl86rkcbxpbsqpx53nk3ci41ap9s")))

(define-public crate-upc-checker-0.1 (crate (name "upc-checker") (vers "0.1.4") (hash "1mh7fr4iqmiilh78sihlpb63j92dg6j918g9bvk602is6810ziln")))

(define-public crate-upc-checker-0.1 (crate (name "upc-checker") (vers "0.1.5") (hash "16b8x6qwh320szd8sydapchcblj78qigbkgp9akixvmfjqgnr1rh")))

