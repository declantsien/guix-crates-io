(define-module (crates-io up sp) #:use-module (crates-io))

(define-public crate-upspin-0.0.1 (crate (name "upspin") (vers "0.0.1-pre") (deps (list (crate-dep (name "failure") (req "^0.1.1") (default-features #t) (kind 0)))) (hash "1psmsgg49g9v471grgbd4kirys9886syrpjxzqck31y63lz5nasf")))

