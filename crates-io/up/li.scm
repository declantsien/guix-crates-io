(define-module (crates-io up li) #:use-module (crates-io))

(define-public crate-uplift-cli-0.1 (crate (name "uplift-cli") (vers "0.1.0") (deps (list (crate-dep (name "anyhow") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "btleplug") (req "^0.9") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^3.1") (features (quote ("derive" "env"))) (default-features #t) (kind 0)) (crate-dep (name "env_logger") (req "^0.9") (default-features #t) (kind 0)) (crate-dep (name "futures") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.4") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.17") (features (quote ("macros" "time" "rt-multi-thread"))) (default-features #t) (kind 0)) (crate-dep (name "uuid") (req "^0.8") (default-features #t) (kind 0)))) (hash "0lfwpfqnpkr79whpwd9199k1040wwkif7g5hqgxxpyyrpjdbs7xh")))

(define-public crate-uplifter-0.1 (crate (name "uplifter") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^4") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.11") (features (quote ("json"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "17i9n8bmdci9n2ga70797jnxhbnms2iflxj81cj0f6904bxb301m") (rust-version "1.57.0")))

(define-public crate-uplink-0.0.1 (crate (name "uplink") (vers "0.0.1") (hash "03i6qraw51l384khx628p1bmnmx0lxwgq04axcir37p3r2006sph")))

(define-public crate-uplink-0.0.2 (crate (name "uplink") (vers "0.0.2") (hash "0lw1sdbg354w06hgffzhyjp235n5snqjvmk0s4rqdf91pimp1r3z")))

(define-public crate-uplink-0.1 (crate (name "uplink") (vers "0.1.0") (deps (list (crate-dep (name "uplink-sys") (req "^0.5.0") (default-features #t) (kind 0)))) (hash "0y9lss5cfg40c6nkyvqbmjcp2plnk0s2xf142d143bfy76ban32l")))

(define-public crate-uplink-0.1 (crate (name "uplink") (vers "0.1.1") (deps (list (crate-dep (name "uplink-sys") (req "^0.5.1") (default-features #t) (kind 0)))) (hash "0a001cq9h91823jw05syzbjdvbqywmc9rd5mmbqcp6qp6q4d30ml")))

(define-public crate-uplink-0.2 (crate (name "uplink") (vers "0.2.0") (deps (list (crate-dep (name "uplink-sys") (req "^0.5.2") (default-features #t) (kind 0)))) (hash "0vwcq3xw26q3i706qpk2gpimkabsym75brlbpmsqsh4piwg7vv5l")))

(define-public crate-uplink-0.3 (crate (name "uplink") (vers "0.3.0") (deps (list (crate-dep (name "uplink-sys") (req "^0.5.2") (default-features #t) (kind 0)))) (hash "07yssvwyvq1ljvnc4kd4813g4m9h1x090hrcifkpiyfirq1540zk")))

(define-public crate-uplink-0.4 (crate (name "uplink") (vers "0.4.0") (deps (list (crate-dep (name "uplink-sys") (req "^0.5.2") (default-features #t) (kind 0)))) (hash "02nw4rmz6gwbpnkzcpagm73h0mpi7mcnfdsw7s321xv8h7q4i8v5")))

(define-public crate-uplink-0.5 (crate (name "uplink") (vers "0.5.0") (deps (list (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 2)) (crate-dep (name "uplink-sys") (req "^0.5.2") (default-features #t) (kind 0)))) (hash "1j00d2cps1s61dmj0f757j7sxi399y5jgfijn10pgx3qdmnwjz3v")))

(define-public crate-uplink-0.6 (crate (name "uplink") (vers "0.6.0") (deps (list (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 2)) (crate-dep (name "uplink-sys") (req "^0.5.2") (default-features #t) (kind 0)))) (hash "0d2dlcdg9b3qycw65rgi800b8jmm7xn8wxlhxq5pmah70xcd15zc")))

(define-public crate-uplink-0.7 (crate (name "uplink") (vers "0.7.0") (deps (list (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 2)) (crate-dep (name "uplink-sys") (req "^0.6.0") (default-features #t) (kind 0)))) (hash "0bij9yjr6abqmx9w7s832n60vgiq1s1k774h6rrdq15zd93rjbyr")))

(define-public crate-uplink-0.8 (crate (name "uplink") (vers "0.8.0") (deps (list (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 2)) (crate-dep (name "uplink-sys") (req "^0.6.0") (default-features #t) (kind 0)))) (hash "0kbyix8wl1hw5m6i14r8h27n1pdf7ma992kb02l1gsv31dxx9lgw")))

(define-public crate-uplink-0.8 (crate (name "uplink") (vers "0.8.1") (deps (list (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 2)) (crate-dep (name "uplink-sys") (req "^0.6.1") (default-features #t) (kind 0)))) (hash "03cf8pnsn9x2j39kk6rh7xq21irg756y98jgjfra4068acm56ppz")))

(define-public crate-uplink-sys-0.1 (crate (name "uplink-sys") (vers "0.1.0") (deps (list (crate-dep (name "bindgen") (req "^0.58.1") (default-features #t) (kind 1)))) (hash "16ba32if6b41cwn0qb88cy848d4sm2d9b69wg218429r0nwa244m")))

(define-public crate-uplink-sys-0.1 (crate (name "uplink-sys") (vers "0.1.1") (deps (list (crate-dep (name "bindgen") (req "^0.58.1") (default-features #t) (kind 1)))) (hash "0jgsz61f8cd5l19gi5n3yq7fykm6rsikczzmyry7a4mahq97lpv5")))

(define-public crate-uplink-sys-0.2 (crate (name "uplink-sys") (vers "0.2.0") (deps (list (crate-dep (name "bindgen") (req "^0.59.1") (default-features #t) (kind 1)))) (hash "1ax0w83lc58pr0n52r2jd7kb88sznv632n22cbn7hsnd5s639xxf") (links "uplink")))

(define-public crate-uplink-sys-0.3 (crate (name "uplink-sys") (vers "0.3.0") (deps (list (crate-dep (name "bindgen") (req "^0.59.2") (default-features #t) (kind 1)))) (hash "0lvrr2cyx1dpw57wkm80aqsf9hx3d4zj8396qqjfp8an07hkc6i8") (links "uplink")))

(define-public crate-uplink-sys-0.4 (crate (name "uplink-sys") (vers "0.4.0") (deps (list (crate-dep (name "bindgen") (req "^0.59.2") (default-features #t) (kind 1)))) (hash "1x4lrflny02f1p59s5n6wy82ijlh9xxn22hk33q3pmjbmhx9zf54") (links "uplink")))

(define-public crate-uplink-sys-0.5 (crate (name "uplink-sys") (vers "0.5.0") (deps (list (crate-dep (name "bindgen") (req "^0.59.2") (default-features #t) (kind 1)))) (hash "1mplqxg61asgfsbn5r83njb9g6bvd2s2xcgf1iw4h7g1wprqrnxp") (links "uplink")))

(define-public crate-uplink-sys-0.5 (crate (name "uplink-sys") (vers "0.5.1") (deps (list (crate-dep (name "bindgen") (req "^0.59.2") (default-features #t) (kind 1)))) (hash "0dz3i4vnqhhb21ypdhnibhyfnwx9hzrzdzg8xp96im4g4a0xsdwh") (links "uplink")))

(define-public crate-uplink-sys-0.5 (crate (name "uplink-sys") (vers "0.5.2") (deps (list (crate-dep (name "bindgen") (req "^0.59.2") (default-features #t) (kind 1)))) (hash "0ar8s31q9bvbvpxxmzv8j4g7y5inww590zj1aibh6n33fkpmdprk") (links "uplink")))

(define-public crate-uplink-sys-0.6 (crate (name "uplink-sys") (vers "0.6.0") (deps (list (crate-dep (name "bindgen") (req "^0.68.1") (default-features #t) (kind 1)))) (hash "02zci8jz3s9x7782mhdfxbwbza4rxnfg45fgh7q12brrimvas6xh") (links "uplink")))

(define-public crate-uplink-sys-0.6 (crate (name "uplink-sys") (vers "0.6.1") (deps (list (crate-dep (name "bindgen") (req "^0.68.1") (default-features #t) (kind 1)))) (hash "0lnzbs6rpaj6kl5yr6jammp2lhrdaw2cldk830f9i4wdj28vfdb8") (links "uplink")))

