(define-module (crates-io up -a) #:use-module (crates-io))

(define-public crate-up-api-0.1 (crate (name "up-api") (vers "0.1.0") (deps (list (crate-dep (name "reqwest") (req "^0.11.11") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "url") (req "^2.2.2") (default-features #t) (kind 0)))) (hash "00sflbv5iwhfnnvgp1pg1p5mkhwc9a9d1rqk650jpqkqif7k0nc8")))

(define-public crate-up-api-0.1 (crate (name "up-api") (vers "0.1.1") (deps (list (crate-dep (name "reqwest") (req "^0.11.11") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "url") (req "^2.2.2") (default-features #t) (kind 0)))) (hash "12bfbr8dvvr188cri3pc616xp9q02finpsshsng3ipgl9aclrng9")))

(define-public crate-up-api-0.1 (crate (name "up-api") (vers "0.1.2") (deps (list (crate-dep (name "reqwest") (req "^0.11.11") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "url") (req "^2.2.2") (default-features #t) (kind 0)))) (hash "1l6a27ymfzqd72l58p8v6087skxl63rgr1kbvx7k4a9m1izj24nd")))

(define-public crate-up-api-0.1 (crate (name "up-api") (vers "0.1.3") (deps (list (crate-dep (name "reqwest") (req "^0.11.11") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "url") (req "^2.2.2") (default-features #t) (kind 0)))) (hash "1a016k9b5cc9lzird9185vx5hp0nhdq6qf2bmb3kwyn6frylk1bn")))

