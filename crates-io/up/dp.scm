(define-module (crates-io up dp) #:use-module (crates-io))

(define-public crate-updpkg-0.0.0 (crate (name "updpkg") (vers "0.0.0") (deps (list (crate-dep (name "clap") (req "^4") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "env_logger") (req "^0") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0") (default-features #t) (kind 0)))) (hash "05dyr2d7rwghws84zm6r7zqh6bvbal7fd3xjshyl7w146g2jd01c") (rust-version "1.70.0")))

(define-public crate-updpkg-0.1 (crate (name "updpkg") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^4") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "env_logger") (req "^0") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0") (default-features #t) (kind 0)))) (hash "0i7fp418yc90qpmxp5v00c5a5xqnj9bci2r7lsrl2rqq8xd1ma3q") (rust-version "1.70.0")))

(define-public crate-updpkg-0.2 (crate (name "updpkg") (vers "0.2.0") (deps (list (crate-dep (name "clap") (req "^4") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "env_logger") (req "^0") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0") (default-features #t) (kind 0)))) (hash "1ih53dixxkx3pgc8vwvslfyvg2xvda7y3n15pg7zqa3kslwdz4gk") (features (quote (("sd") ("default" "sd")))) (rust-version "1.70.0")))

(define-public crate-updpkg-0.2 (crate (name "updpkg") (vers "0.2.1") (deps (list (crate-dep (name "clap") (req "^4") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "env_logger") (req "^0") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0") (default-features #t) (kind 0)))) (hash "0q383078rvy2svg09vwlwxf3jz9jppig4d4hdzny6wgy52ac11x0") (features (quote (("sd") ("default" "sd")))) (rust-version "1.70.0")))

(define-public crate-updpkg-0.2 (crate (name "updpkg") (vers "0.2.2") (deps (list (crate-dep (name "clap") (req "^4") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "env_logger") (req "^0") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0") (default-features #t) (kind 0)))) (hash "0xkvlqvbkdqs702qyp73s4yaivan6sajyiwwkhzpv9qvxz8yddn8") (features (quote (("sd") ("default" "sd")))) (rust-version "1.70.0")))

