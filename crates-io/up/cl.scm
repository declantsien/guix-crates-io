(define-module (crates-io up cl) #:use-module (crates-io))

(define-public crate-upcloud-0.0.1 (crate (name "upcloud") (vers "0.0.1") (hash "14si8h2ff9rd4kvbc1aivf0mq070dihkwplmlb8c5mfy0nxxfj1w")))

(define-public crate-upcloud-rs-0.1 (crate (name "upcloud-rs") (vers "0.1.0") (deps (list (crate-dep (name "async-std") (req "^1.12.0") (features (quote ("attributes" "tokio1"))) (default-features #t) (kind 2)) (crate-dep (name "reqwest") (req "^0.11.22") (features (quote ("blocking" "json"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.189") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "1q5n8f6pcfm9ihiv6b5xzpr475b4n6ac15b2gkqdnmlybc2iw0y1") (features (quote (("rustls" "reqwest/rustls-tls") ("nativetls" "reqwest/default-tls") ("gzip" "reqwest/gzip") ("deflate" "reqwest/deflate") ("default-rustls" "rustls") ("default" "nativetls") ("brotli" "reqwest/brotli") ("blocking" "reqwest/blocking"))))))

(define-public crate-upcloud-rs-0.1 (crate (name "upcloud-rs") (vers "0.1.1") (deps (list (crate-dep (name "async-std") (req "^1.12.0") (features (quote ("attributes" "tokio1"))) (default-features #t) (kind 2)) (crate-dep (name "reqwest") (req "^0.11.22") (features (quote ("blocking" "json"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.189") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "0jbn997rg6zx14c6pbra4xd1p85h52nci13g8fn4rqw14kfsnyi4") (features (quote (("rustls" "reqwest/rustls-tls") ("nativetls" "reqwest/default-tls") ("gzip" "reqwest/gzip") ("deflate" "reqwest/deflate") ("default-rustls" "rustls") ("default" "nativetls") ("brotli" "reqwest/brotli") ("blocking" "reqwest/blocking"))))))

(define-public crate-upcloud-rs-0.1 (crate (name "upcloud-rs") (vers "0.1.2") (deps (list (crate-dep (name "async-std") (req "^1.12.0") (features (quote ("attributes" "tokio1"))) (default-features #t) (kind 2)) (crate-dep (name "reqwest") (req "^0.11.22") (features (quote ("blocking" "json"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.189") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.107") (default-features #t) (kind 0)))) (hash "0vry58cr26g8h7b8m4dlfplkdk44w85cskgbd6m68r3cmxzfzg37") (features (quote (("rustls" "reqwest/rustls-tls") ("nativetls" "reqwest/default-tls") ("gzip" "reqwest/gzip") ("deflate" "reqwest/deflate") ("default-rustls" "rustls") ("default" "nativetls") ("brotli" "reqwest/brotli") ("blocking" "reqwest/blocking"))))))

(define-public crate-upcloud-rs-0.1 (crate (name "upcloud-rs") (vers "0.1.3") (deps (list (crate-dep (name "async-std") (req "^1.12.0") (features (quote ("attributes" "tokio1"))) (default-features #t) (kind 2)) (crate-dep (name "reqwest") (req "^0.11.22") (features (quote ("blocking" "json"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.190") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.107") (default-features #t) (kind 0)))) (hash "1jkp33nyjmnaf3m8hkngh4p15p1danadsii4cdbrib75ndf0q2gh") (features (quote (("rustls" "reqwest/rustls-tls") ("nativetls" "reqwest/default-tls") ("gzip" "reqwest/gzip") ("deflate" "reqwest/deflate") ("default-rustls" "rustls") ("default" "nativetls") ("brotli" "reqwest/brotli") ("blocking" "reqwest/blocking"))))))

(define-public crate-upcloud-rs-0.1 (crate (name "upcloud-rs") (vers "0.1.4") (deps (list (crate-dep (name "async-std") (req "^1.12.0") (features (quote ("attributes" "tokio1"))) (default-features #t) (kind 2)) (crate-dep (name "reqwest") (req "^0.11.22") (features (quote ("blocking" "json"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.190") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.107") (default-features #t) (kind 0)))) (hash "1m7clpsi02wr0v4s7w35rmy17hs9jknifsdccqyqqsra08s5rjlq") (features (quote (("rustls" "reqwest/rustls-tls") ("nativetls" "reqwest/default-tls") ("gzip" "reqwest/gzip") ("deflate" "reqwest/deflate") ("default-rustls" "rustls") ("default" "nativetls") ("brotli" "reqwest/brotli") ("blocking" "reqwest/blocking"))))))

