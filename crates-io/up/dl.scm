(define-module (crates-io up dl) #:use-module (crates-io))

(define-public crate-updlockfiles-0.1 (crate (name "updlockfiles") (vers "0.1.0") (deps (list (crate-dep (name "anyhow") (req "^1.0.65") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^4.0.15") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "env_logger") (req "^0.9.1") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.17") (default-features #t) (kind 0)) (crate-dep (name "tempdir") (req "^0.3.7") (default-features #t) (kind 0)))) (hash "0adgnpj7bxl70964wgzqnz0c14by3gxfvcahfrjdvagmpwljzdqq")))

