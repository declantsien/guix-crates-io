(define-module (crates-io up db) #:use-module (crates-io))

(define-public crate-updb-0.1 (crate (name "updb") (vers "0.1.0") (deps (list (crate-dep (name "arc-swap") (req "^1.6.0") (default-features #t) (kind 0)) (crate-dep (name "imbl") (req "^2.0.0") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.39") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.26.0") (features (quote ("sync"))) (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.26.0") (features (quote ("rt" "macros"))) (default-features #t) (kind 2)))) (hash "0dw4s9xiz0zj37brnkymav5mmrahh0v5hbawglsvx88a4a36b3sl")))

