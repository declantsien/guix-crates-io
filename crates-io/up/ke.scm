(define-module (crates-io up ke) #:use-module (crates-io))

(define-public crate-upkeep-1 (crate (name "upkeep") (vers "1.0.0") (deps (list (crate-dep (name "chrono") (req "^0.4.35") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^4.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "colored") (req "^2.1.0") (default-features #t) (kind 0)) (crate-dep (name "dirs") (req "^5.0.1") (default-features #t) (kind 0)) (crate-dep (name "rusqlite") (req "^0.31.0") (features (quote ("bundled"))) (default-features #t) (kind 0)))) (hash "1ycydsbvp8320dcqkmipxyq7xwhqhhn57vr2m2hpi9d7kf94mc9x")))

