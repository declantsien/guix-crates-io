(define-module (crates-io up m_) #:use-module (crates-io))

(define-public crate-upm_lib-0.2 (crate (name "upm_lib") (vers "0.2.0") (deps (list (crate-dep (name "failure") (req "^0.1.1") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "toml") (req "^0.4.5") (default-features #t) (kind 0)))) (hash "0cg4208280b7s5p1jvds41m0xrk36557s2v99zl4vg8bi704asww")))

(define-public crate-upm_lib-0.3 (crate (name "upm_lib") (vers "0.3.0") (deps (list (crate-dep (name "failure") (req "^0.1.1") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "toml") (req "^0.4.5") (default-features #t) (kind 0)))) (hash "0a74ca6rvrh02f8fhiv418xjxxd68d1nfmy3r0rsibcxcs09w9ld")))

