(define-module (crates-io up l-) #:use-module (crates-io))

(define-public crate-upl-delegation-manager-0.1 (crate (name "upl-delegation-manager") (vers "0.1.0") (deps (list (crate-dep (name "anchor-lang") (req "^0.26.0") (default-features #t) (kind 0)))) (hash "19y44anlg7ihy9b03n71cmgz0rgswgqqnyjgmsqaffgy9nysz8px") (features (quote (("no-log-ix-name") ("no-idl") ("no-entrypoint") ("default") ("cpi" "no-entrypoint"))))))

(define-public crate-upl-delegation-manager-0.1 (crate (name "upl-delegation-manager") (vers "0.1.1") (deps (list (crate-dep (name "anchor-lang") (req "^0.26.0") (default-features #t) (kind 0)))) (hash "0kqsqg4h77azfa7jc5l11bvxna0ah652i8pqh6jzw1hz4rmffywh") (features (quote (("no-log-ix-name") ("no-idl") ("no-entrypoint") ("default") ("cpi" "no-entrypoint"))))))

