(define-module (crates-io tg -s) #:use-module (crates-io))

(define-public crate-tg-sys-0.1 (crate (name "tg-sys") (vers "0.1.0") (deps (list (crate-dep (name "cc") (req "^1.0.83") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2.150") (default-features #t) (kind 0)))) (hash "1svbr86qbj4i7krq5ax0nqbkxm4zmsq1qc32nm9vmyw3xmscfwms") (links "tg")))

(define-public crate-tg-sys-0.1 (crate (name "tg-sys") (vers "0.1.1") (deps (list (crate-dep (name "cc") (req "^1.0.83") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2.150") (default-features #t) (kind 0)))) (hash "089qam150p796fkrapynyaalnjz9msk8jsgqiqhnlac4h9miyz84") (features (quote (("default" "atomics") ("atomics")))) (links "tg")))

