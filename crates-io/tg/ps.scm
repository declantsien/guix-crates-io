(define-module (crates-io tg ps) #:use-module (crates-io))

(define-public crate-tgps-0.0.0 (crate (name "tgps") (vers "0.0.0") (hash "158rxyp6ywal5x3cm4c6bdbjpp2p3w8132sdbimmrssk06ad5cng") (yanked #t)))

(define-public crate-tgps-0.0.1 (crate (name "tgps") (vers "0.0.1") (hash "1n9y9r14qnq3201fpq135q3amwlvl95lxlj91415dknj9qzcrfp8")))

