(define-module (crates-io tg -a) #:use-module (crates-io))

(define-public crate-tg-api-0.2 (crate (name "tg-api") (vers "0.2.0") (deps (list (crate-dep (name "reqwest") (req "^0.11.14") (features (quote ("json" "multipart" "blocking"))) (default-features #t) (kind 0)))) (hash "0jqzbv8skpkn5sv7lszmg67kc7v6ravdskh5gkxx92a7n0y0r4jy")))

(define-public crate-tg-api-0.2 (crate (name "tg-api") (vers "0.2.1") (deps (list (crate-dep (name "reqwest") (req "^0.11.14") (features (quote ("json" "multipart" "blocking"))) (default-features #t) (kind 0)))) (hash "0aprc8prq78knk8k1fmf3vc6b6gi5h614750izv6vfhw9jgha1s7")))

(define-public crate-tg-api-0.3 (crate (name "tg-api") (vers "0.3.0") (deps (list (crate-dep (name "reqwest") (req "^0.11.14") (features (quote ("json" "multipart" "blocking"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.91") (default-features #t) (kind 0)))) (hash "0wv8rnzrn1whfgsmp4fpqkblmixdb35r3h1mz2ly1g700v5x08ar")))

(define-public crate-tg-api-0.3 (crate (name "tg-api") (vers "0.3.1") (deps (list (crate-dep (name "reqwest") (req "^0.11.14") (features (quote ("json" "multipart" "blocking"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.91") (default-features #t) (kind 0)))) (hash "0hx328np1kbsbw9j6pvkhfavfvl15z23i0vh4ckl94dxvnfq39m1")))

(define-public crate-tg-api-0.3 (crate (name "tg-api") (vers "0.3.2") (deps (list (crate-dep (name "reqwest") (req "^0.11.14") (features (quote ("json" "multipart" "blocking"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.91") (default-features #t) (kind 0)))) (hash "1il6vnwgd6nqcqlfqwg6x9c8zawnwg4bd1i8zs39sh734q3hhymv")))

(define-public crate-tg-api-0.4 (crate (name "tg-api") (vers "0.4.0") (deps (list (crate-dep (name "reqwest") (req "^0.11.14") (features (quote ("json" "multipart" "blocking"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.91") (default-features #t) (kind 0)))) (hash "1fi70vakcgzxrb5mjrk3n7hxfwg840cqgdm8d4pr0cga55f5fz3n")))

(define-public crate-tg-api-0.4 (crate (name "tg-api") (vers "0.4.1") (deps (list (crate-dep (name "reqwest") (req "^0.11.14") (features (quote ("json" "multipart" "blocking"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.91") (default-features #t) (kind 0)))) (hash "15j1l25dxfm6ywy45wvm8m9xz7dh7bzlkgfmlrh8is7fsl90smzk")))

(define-public crate-tg-api-0.4 (crate (name "tg-api") (vers "0.4.2") (deps (list (crate-dep (name "reqwest") (req "^0.11.14") (features (quote ("json" "multipart" "blocking"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.91") (default-features #t) (kind 0)))) (hash "1cqgqn1aannzsfhmppavcv2knyiyphwds8w39jimxv026wwmp6b2")))

(define-public crate-tg-api-0.4 (crate (name "tg-api") (vers "0.4.3") (deps (list (crate-dep (name "reqwest") (req "^0.11.14") (features (quote ("json" "multipart" "blocking"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.91") (default-features #t) (kind 0)))) (hash "0p101sba34d9gdz1cbbchnqwhdh7rij25y4d4576k6jrachd3g74")))

