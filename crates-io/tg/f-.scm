(define-module (crates-io tg f-) #:use-module (crates-io))

(define-public crate-tgf-macros-0.0.2 (crate (name "tgf-macros") (vers "0.0.2") (deps (list (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2.0") (features (quote ("extra-traits" "full"))) (default-features #t) (kind 0)))) (hash "10xhz0jdwarlzwqf5lrkscwz998qy79s83yfk8cs34sc9yw1pm4m")))

