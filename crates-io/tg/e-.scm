(define-module (crates-io tg e-) #:use-module (crates-io))

(define-public crate-tge-ext-0.0.0 (crate (name "tge-ext") (vers "0.0.0") (hash "1dqv9g8dhb9c9zfz5ybjy5pj68fjrx5hk2r7mq2s7c6ff6cn2iib")))

(define-public crate-tge-scene-0.0.0 (crate (name "tge-scene") (vers "0.0.0") (hash "1fjxp8crc8l3xpnaja52wj9fa4i35n7yzhsphlx6lagfvd4ri6gp")))

(define-public crate-tge-ui-0.0.0 (crate (name "tge-ui") (vers "0.0.0") (hash "03fxq1qmm6zbcjsjiydyk4n08zds8ly6z78pmg4cqzqxvhi20s1v")))

