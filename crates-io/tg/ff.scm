(define-module (crates-io tg ff) #:use-module (crates-io))

(define-public crate-tgff-0.0.1 (crate (name "tgff") (vers "0.0.1") (deps (list (crate-dep (name "assert") (req "^0.0.1") (default-features #t) (kind 0)))) (hash "0pa7ymsmyqnybybbykbnhg6bjmf83ia1rri34kgi91jxp60gy1n0")))

(define-public crate-tgff-0.0.2 (crate (name "tgff") (vers "0.0.2") (deps (list (crate-dep (name "assert") (req "^0.0.1") (default-features #t) (kind 2)))) (hash "0kf8hbqlixs07vfmr5ipwa37hzxx7wgxg76gnbd6y76j95skizkn")))

(define-public crate-tgff-0.0.3 (crate (name "tgff") (vers "0.0.3") (deps (list (crate-dep (name "assert") (req "^0.0.1") (default-features #t) (kind 2)))) (hash "1jjpka04b9n9csw7338izcb85phzvwp62h12fjaidrl87rg208a1")))

(define-public crate-tgff-0.0.5 (crate (name "tgff") (vers "0.0.5") (deps (list (crate-dep (name "assert") (req "^0.0.2") (default-features #t) (kind 2)))) (hash "1bilhm5xx579pn33zs7fiq4x1a75ck19vnm6xwjk061w1r6dxjc2")))

(define-public crate-tgff-0.0.6 (crate (name "tgff") (vers "0.0.6") (deps (list (crate-dep (name "assert") (req "^0.0.2") (default-features #t) (kind 2)))) (hash "032qvlql5lv5zji77zr7655b272l85vgc8y7qzvywkl8va47dh3z")))

(define-public crate-tgff-0.0.7 (crate (name "tgff") (vers "0.0.7") (deps (list (crate-dep (name "assert") (req "^0.0.2") (default-features #t) (kind 2)))) (hash "1z9x6acpxjpi339g659c2z6j9isxjlhbjvkfmlksc7rpxxfaz4f9")))

(define-public crate-tgff-0.0.8 (crate (name "tgff") (vers "0.0.8") (deps (list (crate-dep (name "assert") (req "^0.0.3") (default-features #t) (kind 2)))) (hash "19adrgm014xh61n84gjmzk7app01066j51yvm4g4n1y2ri1d2922")))

(define-public crate-tgff-0.0.9 (crate (name "tgff") (vers "0.0.9") (deps (list (crate-dep (name "assert") (req "^0.0.4") (default-features #t) (kind 2)))) (hash "0c4k5vgchnsw6vz4v0rkbig4baravzpam0ymk09a1cqjgbvd8ayj")))

(define-public crate-tgff-0.1 (crate (name "tgff") (vers "0.1.0") (deps (list (crate-dep (name "assert") (req "^0.1") (default-features #t) (kind 2)))) (hash "1yhsaydsb5vqlg9aafpk9ini58wyji7jfx6xklg8cjydas7z9nxr")))

(define-public crate-tgff-0.1 (crate (name "tgff") (vers "0.1.1") (deps (list (crate-dep (name "assert") (req "^0.1") (default-features #t) (kind 2)))) (hash "1rfpjgpc21mg91b9sbpgcg4qa83xhy654l4m81zklpdlmgfc3480")))

(define-public crate-tgff-0.1 (crate (name "tgff") (vers "0.1.2") (deps (list (crate-dep (name "assert") (req "^0.1") (default-features #t) (kind 2)))) (hash "15ijfxrs5czwf565p56gdzwag8b1b7ka9r94r96dq40944sjwrwf")))

(define-public crate-tgff-0.1 (crate (name "tgff") (vers "0.1.3") (deps (list (crate-dep (name "assert") (req "^0.1") (default-features #t) (kind 2)))) (hash "0yw2gcx92md8ilrraxsrv1hl7jn2dlpa5wapl0rhrd6rmpiw8vq3")))

(define-public crate-tgff-0.1 (crate (name "tgff") (vers "0.1.4") (deps (list (crate-dep (name "assert") (req "^0.1") (default-features #t) (kind 2)))) (hash "1hbax5wzid7ivjjbldsx3c2v9b3mjhyrhh54b2knn8zn8vkl2zqi")))

(define-public crate-tgff-0.1 (crate (name "tgff") (vers "0.1.5") (deps (list (crate-dep (name "assert") (req "^0.1") (default-features #t) (kind 2)))) (hash "1qx902pmdn3v1p6l6h46v1jp30l9gqaf1jnsd18v7kdim8lykjap")))

(define-public crate-tgff-0.1 (crate (name "tgff") (vers "0.1.6") (deps (list (crate-dep (name "assert") (req "^0.1") (default-features #t) (kind 2)))) (hash "1jwmzhv6xgr4s871j6h7lysb2df62isnlrwi0rdbv8sv6sp5fj02")))

(define-public crate-tgff-0.1 (crate (name "tgff") (vers "0.1.7") (deps (list (crate-dep (name "assert") (req "^0.1") (default-features #t) (kind 2)))) (hash "1hn8gm4kx3dgrwis7lkcail62v10jpmfp440150ja62dsrzfpgfx")))

(define-public crate-tgff-0.1 (crate (name "tgff") (vers "0.1.8") (deps (list (crate-dep (name "assert") (req "^0.1") (default-features #t) (kind 2)))) (hash "0055akv8xvslsdy4wqlhpnng4i8wmjzkcq9fghypg6j929baq7qm")))

(define-public crate-tgff-0.1 (crate (name "tgff") (vers "0.1.9") (deps (list (crate-dep (name "assert") (req "*") (default-features #t) (kind 2)) (crate-dep (name "num") (req "*") (default-features #t) (kind 0)))) (hash "0c60jd0fpnsrmx2jwag21cbhnzsyvw7ck9kx4rjrwybyks8pibvk")))

(define-public crate-tgff-0.1 (crate (name "tgff") (vers "0.1.10") (deps (list (crate-dep (name "assert") (req "*") (default-features #t) (kind 2)))) (hash "1p5z17jlzhbw92h45pzi059sg5f35c9i0s8rqw3bcvv886m68cn3")))

