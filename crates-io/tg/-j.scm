(define-module (crates-io tg -j) #:use-module (crates-io))

(define-public crate-tg-join-leave-bot-0.1 (crate (name "tg-join-leave-bot") (vers "0.1.0") (deps (list (crate-dep (name "futures") (req "^0.3.21") (default-features #t) (kind 0)) (crate-dep (name "pw-telegram-bot-fork") (req "^0.9.0") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.17.0") (features (quote ("macros" "rt" "rt-multi-thread"))) (default-features #t) (kind 0)))) (hash "0vf9lpfpphhz4ahph9k8q417ibg005ykvbd5mmwj15j5ivcbjckz")))

