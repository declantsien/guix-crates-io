(define-module (crates-io tg -t) #:use-module (crates-io))

(define-public crate-tg-test-utils-0.5 (crate (name "tg-test-utils") (vers "0.5.3") (deps (list (crate-dep (name "cosmwasm-std") (req "^1.0.0-beta") (default-features #t) (kind 0)) (crate-dep (name "tg-voting-contract") (req "^0.5.3") (default-features #t) (kind 0)))) (hash "0dxlabva3pc10hp7mk62avnm2yjk19zkpyrzcvz3b6j0iav746iy")))

(define-public crate-tg-test-utils-0.5 (crate (name "tg-test-utils") (vers "0.5.3-2") (deps (list (crate-dep (name "cosmwasm-std") (req "^1.0.0-beta") (default-features #t) (kind 0)) (crate-dep (name "tg-voting-contract") (req "^0.5.3-2") (default-features #t) (kind 0)))) (hash "1ayikbbd985awgl4f25nvk7qzn87bh7rddpjxvn8zn15d81bsjr7")))

(define-public crate-tg-test-utils-0.5 (crate (name "tg-test-utils") (vers "0.5.4") (deps (list (crate-dep (name "cosmwasm-std") (req "^1.0.0-beta") (default-features #t) (kind 0)) (crate-dep (name "tg-voting-contract") (req "^0.5.4") (default-features #t) (kind 0)))) (hash "1pkw8ybv9n5as5jlwp26pgr9a7gs5g37jh7s4468af5lgshdy62i")))

(define-public crate-tg-test-utils-0.5 (crate (name "tg-test-utils") (vers "0.5.5") (deps (list (crate-dep (name "cosmwasm-std") (req "^1.0.0-beta") (default-features #t) (kind 0)) (crate-dep (name "tg-voting-contract") (req "^0.5.5") (default-features #t) (kind 0)))) (hash "0v75a5k6gjcq0svibmnigqd34inlfd3nl78a0m9gs2i7lg9gx7di")))

(define-public crate-tg-test-utils-0.6 (crate (name "tg-test-utils") (vers "0.6.0-alpha1") (deps (list (crate-dep (name "cosmwasm-std") (req "^1.0.0-beta") (default-features #t) (kind 0)) (crate-dep (name "tg-voting-contract") (req "^0.6.0-alpha1") (default-features #t) (kind 0)))) (hash "1ipxf720n6rf5yx0mp1qzv3jl9qi88lq3y7zdwg9jr4qrc6czkxd")))

(define-public crate-tg-test-utils-0.6 (crate (name "tg-test-utils") (vers "0.6.0-beta1") (deps (list (crate-dep (name "cosmwasm-std") (req "^1.0.0-beta") (default-features #t) (kind 0)) (crate-dep (name "tg-voting-contract") (req "^0.6.0-beta1") (default-features #t) (kind 0)))) (hash "14hln1f1a5wkzrrqq04k06jsxf2f7nnh9mz6qzfannf42bzn98ap")))

(define-public crate-tg-test-utils-0.6 (crate (name "tg-test-utils") (vers "0.6.0-rc1") (deps (list (crate-dep (name "cosmwasm-std") (req "^1.0.0-beta") (default-features #t) (kind 0)) (crate-dep (name "tg-voting-contract") (req "^0.6.0-rc1") (default-features #t) (kind 0)))) (hash "0pava4csqjhbpbanb6jhncl2cw1r69qm896j2iqji6kzbna1z1gx")))

(define-public crate-tg-test-utils-0.6 (crate (name "tg-test-utils") (vers "0.6.0-rc2") (deps (list (crate-dep (name "cosmwasm-std") (req "^1.0.0-beta") (default-features #t) (kind 0)) (crate-dep (name "tg-voting-contract") (req "^0.6.0-rc2") (default-features #t) (kind 0)))) (hash "0wkpdd29wjspd995cznmb741v7z85v0xgzwwai9nqd71zymwj0y3")))

(define-public crate-tg-test-utils-0.6 (crate (name "tg-test-utils") (vers "0.6.0") (deps (list (crate-dep (name "cosmwasm-std") (req "^1.0.0-beta5") (default-features #t) (kind 0)) (crate-dep (name "tg-voting-contract") (req "^0.6.0") (default-features #t) (kind 0)))) (hash "0jx57cwpalr1aawyg8ks8s6jnvapyx61qz2ckq37xxb2vmij2b1h")))

(define-public crate-tg-test-utils-0.6 (crate (name "tg-test-utils") (vers "0.6.1") (deps (list (crate-dep (name "cosmwasm-std") (req "^1.0.0-beta5") (default-features #t) (kind 0)) (crate-dep (name "tg-voting-contract") (req "^0.6.1") (default-features #t) (kind 0)))) (hash "0n84lngz2azkvcb176jdwsjl7q4i43fd4j4cgcplw8vnkliw1q1f")))

(define-public crate-tg-test-utils-0.6 (crate (name "tg-test-utils") (vers "0.6.2") (deps (list (crate-dep (name "cosmwasm-std") (req "^1.0.0-beta5") (default-features #t) (kind 0)) (crate-dep (name "tg-voting-contract") (req "^0.6.2") (default-features #t) (kind 0)))) (hash "18hmi6dh38k568wnibi6m07ajixdmzkcjfxg50pmrph3fnbcm8sp")))

(define-public crate-tg-test-utils-0.7 (crate (name "tg-test-utils") (vers "0.7.0-alpha1") (deps (list (crate-dep (name "cosmwasm-std") (req "^1.0.0-beta5") (default-features #t) (kind 0)) (crate-dep (name "tg-voting-contract") (req "^0.7.0-alpha1") (default-features #t) (kind 0)))) (hash "16ikhy872jnygd9dpxm0c6acxlayi7lnnnqmmiz0i32xxi5rabz8")))

(define-public crate-tg-test-utils-0.7 (crate (name "tg-test-utils") (vers "0.7.0-alpha2") (deps (list (crate-dep (name "cosmwasm-std") (req "^1.0.0-beta5") (default-features #t) (kind 0)) (crate-dep (name "tg-voting-contract") (req "^0.7.0-alpha2") (default-features #t) (kind 0)))) (hash "0x0gif39m15nxhbzzib6l0dzxjyp6wj6skvzavfgc4jlg2lcz0xh")))

(define-public crate-tg-test-utils-0.8 (crate (name "tg-test-utils") (vers "0.8.0") (deps (list (crate-dep (name "cosmwasm-std") (req "^1.0.0-beta6") (default-features #t) (kind 0)) (crate-dep (name "tg-voting-contract") (req "^0.8.0") (default-features #t) (kind 0)))) (hash "0grhc1wfch90p1gcxpbrf6fqh6pz1khmkwcr4kisjn3rz5983532")))

(define-public crate-tg-test-utils-0.8 (crate (name "tg-test-utils") (vers "0.8.1") (deps (list (crate-dep (name "cosmwasm-std") (req "^1.0.0-beta6") (default-features #t) (kind 0)) (crate-dep (name "tg-voting-contract") (req "^0.8.1") (default-features #t) (kind 0)))) (hash "1qcg0696d646v1wmmd6zkpfm4nf6dw3129n9iah1avrm4agqsymw")))

(define-public crate-tg-test-utils-0.9 (crate (name "tg-test-utils") (vers "0.9.0") (deps (list (crate-dep (name "cosmwasm-std") (req "^1.0.0-beta8") (default-features #t) (kind 0)) (crate-dep (name "tg-voting-contract") (req "^0.9.0") (default-features #t) (kind 0)))) (hash "0yr68b1f42ajx06q7pg33h3hmdrmgych4bg83dyym52adpjsrfg6")))

(define-public crate-tg-test-utils-0.10 (crate (name "tg-test-utils") (vers "0.10.0") (deps (list (crate-dep (name "cosmwasm-std") (req "^1.0.0") (default-features #t) (kind 0)) (crate-dep (name "tg-voting-contract") (req "^0.10.0") (default-features #t) (kind 0)))) (hash "1agsgvvq06776xyszrwi0lcvik5wasl5cxqgshb34zzzw0bh8aib")))

(define-public crate-tg-test-utils-0.11 (crate (name "tg-test-utils") (vers "0.11.0") (deps (list (crate-dep (name "cosmwasm-std") (req "^1.0.0") (default-features #t) (kind 0)) (crate-dep (name "tg-voting-contract") (req "^0.11.0") (default-features #t) (kind 0)))) (hash "0xjk0i8qi5lqsrbh78gjncacbihr1s5lg7r5m8x8y01yf0ihxvrg")))

(define-public crate-tg-test-utils-0.12 (crate (name "tg-test-utils") (vers "0.12.0") (deps (list (crate-dep (name "cosmwasm-std") (req "^1.0.0") (default-features #t) (kind 0)) (crate-dep (name "tg-voting-contract") (req "^0.12.0") (default-features #t) (kind 0)))) (hash "0xcnkbnd1ynmx7pck8dsjqfl1342wi2j3zsgxd4s0g82gz8biikv")))

(define-public crate-tg-test-utils-0.13 (crate (name "tg-test-utils") (vers "0.13.0") (deps (list (crate-dep (name "cosmwasm-std") (req "^1.0.0") (default-features #t) (kind 0)) (crate-dep (name "tg-voting-contract") (req "^0.13.0") (default-features #t) (kind 0)))) (hash "0swdkdhfk9aqw6zx2paz49xamnxm7q9v2f6n84vvqn6ybfgnbfb5")))

(define-public crate-tg-test-utils-0.14 (crate (name "tg-test-utils") (vers "0.14.0") (deps (list (crate-dep (name "cosmwasm-std") (req "^1.0.0") (default-features #t) (kind 0)) (crate-dep (name "tg-voting-contract") (req "^0.14.0") (default-features #t) (kind 0)))) (hash "0w8610rnywhv2bxhsf8ixmvpd1mh7hif7r0xnkqfpy0w6m15228n")))

(define-public crate-tg-test-utils-0.15 (crate (name "tg-test-utils") (vers "0.15.0") (deps (list (crate-dep (name "cosmwasm-std") (req "^1.1.0") (default-features #t) (kind 0)) (crate-dep (name "tg-voting-contract") (req "^0.15.0") (default-features #t) (kind 0)))) (hash "0ancg25mv1mcjrfwxvjvii8r1i4a1nzjykzy3sgy5iqcn8s4bjam")))

(define-public crate-tg-test-utils-0.15 (crate (name "tg-test-utils") (vers "0.15.1") (deps (list (crate-dep (name "cosmwasm-std") (req "^1.1.0") (default-features #t) (kind 0)) (crate-dep (name "tg-voting-contract") (req "^0.15.1") (default-features #t) (kind 0)))) (hash "1p5s96vr1f75wmz3hz5vczapa756zxn04y7642fc5rapyi9v7kcx")))

(define-public crate-tg-test-utils-0.16 (crate (name "tg-test-utils") (vers "0.16.0") (deps (list (crate-dep (name "cosmwasm-std") (req "^1.1.9") (default-features #t) (kind 0)) (crate-dep (name "tg-voting-contract") (req "^0.16.0") (default-features #t) (kind 0)))) (hash "0z4da637xmf27387nlwcn0q54lflpippq04p8zmj2rciik0wppqn")))

(define-public crate-tg-test-utils-0.17 (crate (name "tg-test-utils") (vers "0.17.0") (deps (list (crate-dep (name "cosmwasm-std") (req "^1.1.9") (default-features #t) (kind 0)) (crate-dep (name "tg-voting-contract") (req "^0.17.0") (default-features #t) (kind 0)))) (hash "1wkgvqxvb8pdg0q58ginqv816ml6aqrbxnj18ksy4y0ki6nvqahs")))

(define-public crate-tg-test-utils-0.17 (crate (name "tg-test-utils") (vers "0.17.1") (deps (list (crate-dep (name "cosmwasm-std") (req "^1.1.9") (default-features #t) (kind 0)) (crate-dep (name "tg-voting-contract") (req "^0.17.1") (default-features #t) (kind 0)))) (hash "1gzxx3p0frxb303f2665h1fhmn97fwfb26w4d1bx17wazbhqz69b")))

