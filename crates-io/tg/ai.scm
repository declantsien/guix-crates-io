(define-module (crates-io tg ai) #:use-module (crates-io))

(define-public crate-tgaimage-0.1 (crate (name "tgaimage") (vers "0.1.0") (hash "0h4dy38s41m92zc6casjry83j7h37wzn78sa2lzp80if89j6w08v")))

(define-public crate-tgaimage-0.1 (crate (name "tgaimage") (vers "0.1.1") (hash "16qd2bsijv4h89y7r5nb6zqvhiqjypd6ljxk30bcm70rjpy3cjmm")))

(define-public crate-tgaimage-sys-0.1 (crate (name "tgaimage-sys") (vers "0.1.0") (deps (list (crate-dep (name "bindgen") (req "^0.26") (default-features #t) (kind 1)))) (hash "0dnj1v87f3wcrk1n175rispq2xnp1xj8prgpglin4w9v4p4jx70j")))

