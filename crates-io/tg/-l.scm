(define-module (crates-io tg -l) #:use-module (crates-io))

(define-public crate-tg-labstatus-0.1 (crate (name "tg-labstatus") (vers "0.1.0") (deps (list (crate-dep (name "hyper") (req "^0.9.10") (default-features #t) (kind 0)) (crate-dep (name "telegram-bot") (req "^0.4.2") (default-features #t) (kind 0)))) (hash "15npws7443w7p24m1l0s7a20hmilkz9283d6140kcj5li8ngwgc6")))

