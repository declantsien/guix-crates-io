(define-module (crates-io tg op) #:use-module (crates-io))

(define-public crate-tgopher-0.1 (crate (name "tgopher") (vers "0.1.0") (deps (list (crate-dep (name "termion") (req "^1.5.6") (default-features #t) (kind 0)))) (hash "1gqka0wx50pa099hmxhqz9046jan1hdqxkja05hckpkwv7g313nz") (yanked #t)))

(define-public crate-tgopher-0.1 (crate (name "tgopher") (vers "0.1.1") (deps (list (crate-dep (name "termion") (req "^1.5.6") (default-features #t) (kind 0)))) (hash "1wnq7jxbfgx5rpj40mm2fb5hqkd8y0zjy8xmzpy1c8zsmqsci298")))

