(define-module (crates-io ml sa) #:use-module (crates-io))

(define-public crate-mlsag-0.1 (crate (name "mlsag") (vers "0.1.0") (deps (list (crate-dep (name "curve25519-dalek") (req "^1") (features (quote ("nightly"))) (default-features #t) (kind 0)) (crate-dep (name "merlin") (req "^1.1") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.6") (default-features #t) (kind 0)) (crate-dep (name "sha2") (req "^0.8") (default-features #t) (kind 0)))) (hash "02f10awzk188r7zcvqz9fi9f0y4rcn5zpq6w0xrkr2gqvnrf22jm")))

(define-public crate-mlsag-0.2 (crate (name "mlsag") (vers "0.2.0") (deps (list (crate-dep (name "curve25519-dalek") (req "^1") (features (quote ("nightly"))) (default-features #t) (kind 0)) (crate-dep (name "merlin") (req "^1.1") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.6") (default-features #t) (kind 0)) (crate-dep (name "sha2") (req "^0.8") (default-features #t) (kind 0)))) (hash "0zbp9d9g7scijkf6wb4cmqa4fmmn9h22s63m81711glikxnzrr6l")))

(define-public crate-mlsag-0.3 (crate (name "mlsag") (vers "0.3.0") (deps (list (crate-dep (name "curve25519-dalek") (req "^1") (features (quote ("nightly"))) (default-features #t) (kind 0)) (crate-dep (name "merlin") (req "^1.1") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.6") (default-features #t) (kind 0)) (crate-dep (name "sha2") (req "^0.8") (default-features #t) (kind 0)))) (hash "1wg3zwgkhpnagpxzs36qwc2jrnx0wnwwbc1q269r0lsrh99b13pa")))

