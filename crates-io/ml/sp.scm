(define-module (crates-io ml sp) #:use-module (crates-io))

(define-public crate-mlsp-0.1 (crate (name "mlsp") (vers "0.1.0") (deps (list (crate-dep (name "rand") (req "^0.8.4") (default-features #t) (kind 2)))) (hash "1gj6kwpmmfhvr5jcz1rplms29d723hcsw7432mn6rjr85klpp91k")))

(define-public crate-mlsp-0.1 (crate (name "mlsp") (vers "0.1.1") (deps (list (crate-dep (name "rand") (req "^0.8.4") (default-features #t) (kind 2)))) (hash "1k2nphv957aibp3ws3sl8jhibkcx52gfcr75y8hwliqg131s6n27")))

(define-public crate-mlsp-0.2 (crate (name "mlsp") (vers "0.2.0") (deps (list (crate-dep (name "rand") (req "^0.8.4") (default-features #t) (kind 2)))) (hash "1c28jmkq4lx572rqniyw0fij0qvvav8q47kfdmhvmcgkb8kqyvcz")))

