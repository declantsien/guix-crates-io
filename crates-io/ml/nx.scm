(define-module (crates-io ml nx) #:use-module (crates-io))

(define-public crate-mlnx-ofed-libibverbs-sys-0.0.0 (crate (name "mlnx-ofed-libibverbs-sys") (vers "0.0.0") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (target "cfg(target_os = \"linux\")") (kind 0)))) (hash "0r81g396z4ayrkph4y2pkblq08h10r0pf0d5wswr95y2jyycsbnd")))

(define-public crate-mlnx-ofed-libibverbs-sys-0.0.1 (crate (name "mlnx-ofed-libibverbs-sys") (vers "0.0.1") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (target "cfg(target_os = \"linux\")") (kind 0)))) (hash "0lhji96llzsnfr28vix5c70fibf72kar08gxqn7gasfvd1hgzsip")))

(define-public crate-mlnx-ofed-libibverbs-sys-0.0.2 (crate (name "mlnx-ofed-libibverbs-sys") (vers "0.0.2") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (target "cfg(target_os = \"linux\")") (kind 0)))) (hash "03w3g4i3hxajd9sf83kmb9gdfh0acwvz42jy0n1cdlkj975dn88d")))

(define-public crate-mlnx-ofed-libibverbs-sys-0.0.3 (crate (name "mlnx-ofed-libibverbs-sys") (vers "0.0.3") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (target "cfg(target_os = \"linux\")") (kind 0)))) (hash "0i90a0idnd8zbyhf4qk3mg75ipf224m0l2qr37ig3bcbbg5xbhch")))

(define-public crate-mlnx-ofed-libibverbs-sys-0.0.4 (crate (name "mlnx-ofed-libibverbs-sys") (vers "0.0.4") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (target "cfg(target_os = \"linux\")") (kind 0)))) (hash "15iimy4xqd41jskb783r2sc56a6mq22plmn49bhgr1g2mnj7zw81")))

(define-public crate-mlnx-ofed-libibverbs-sys-0.0.5 (crate (name "mlnx-ofed-libibverbs-sys") (vers "0.0.5") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (target "cfg(target_os = \"linux\")") (kind 0)))) (hash "089c2mjl6p7pzs4yxydd375zdjaz03a9gras424qs7cvzn1q94pw")))

(define-public crate-mlnx-ofed-libibverbs-sys-0.0.6 (crate (name "mlnx-ofed-libibverbs-sys") (vers "0.0.6") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (target "cfg(target_os = \"linux\")") (kind 0)))) (hash "0ysympapr5cxsbnrbxdqcgxxalbd9k6aipmr0zfg2nd505fqws48")))

(define-public crate-mlnx-ofed-libmlx4-sys-0.0.7 (crate (name "mlnx-ofed-libmlx4-sys") (vers "0.0.7") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (target "cfg(target_os = \"linux\")") (kind 0)) (crate-dep (name "libnuma-sys") (req "^0.0.9") (default-features #t) (kind 0)) (crate-dep (name "mlnx-ofed-libibverbs-sys") (req "^0.0.6") (default-features #t) (kind 0)))) (hash "0ji1hcdlxm3b9gsyhmdqsddifx9rsfrqrmpk8nzi0vq34imncj6y")))

(define-public crate-mlnx-ofed-libmlx4-sys-0.0.8 (crate (name "mlnx-ofed-libmlx4-sys") (vers "0.0.8") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (target "cfg(target_os = \"linux\")") (kind 0)) (crate-dep (name "libnuma-sys") (req "^0.0.9") (default-features #t) (kind 0)) (crate-dep (name "mlnx-ofed-libibverbs-sys") (req "^0.0.6") (default-features #t) (kind 0)))) (hash "0qnc17g4i9d6y8s6vkx20nn5gsmkx62h8hw1c779xgxglis4hkl4")))

(define-public crate-mlnx-ofed-libmlx4-sys-0.0.9 (crate (name "mlnx-ofed-libmlx4-sys") (vers "0.0.9") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (target "cfg(target_os = \"linux\")") (kind 0)) (crate-dep (name "libnuma-sys") (req "^0.0.9") (default-features #t) (kind 0)) (crate-dep (name "mlnx-ofed-libibverbs-sys") (req "^0.0.6") (default-features #t) (kind 0)))) (hash "19vcvwl1y0zynh2qx7qrlnm2914d1ww5zj81n1jfcaanvrxybnl5")))

(define-public crate-mlnx-ofed-libmlx4-sys-0.0.10 (crate (name "mlnx-ofed-libmlx4-sys") (vers "0.0.10") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (target "cfg(target_os = \"linux\")") (kind 0)) (crate-dep (name "libnuma-sys") (req "^0.0.9") (default-features #t) (kind 0)) (crate-dep (name "mlnx-ofed-libibverbs-sys") (req "^0.0.6") (default-features #t) (kind 0)))) (hash "1gymhpnp040j2lplssf5fimd70hs9fng8wx426zqws72s727l46h")))

(define-public crate-mlnx-ofed-libmlx5-sys-0.0.3 (crate (name "mlnx-ofed-libmlx5-sys") (vers "0.0.3") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (target "cfg(target_os = \"linux\")") (kind 0)) (crate-dep (name "libnuma-sys") (req "^0.0.8") (default-features #t) (kind 0)) (crate-dep (name "mlnx-ofed-libibverbs-sys") (req "^0.0.3") (default-features #t) (kind 0)))) (hash "059xa76qvs07arnvbbd13haw5spfaakbx0kn7am4yjyyxgfl5avs")))

(define-public crate-mlnx-ofed-libmlx5-sys-0.0.4 (crate (name "mlnx-ofed-libmlx5-sys") (vers "0.0.4") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (target "cfg(target_os = \"linux\")") (kind 0)) (crate-dep (name "libnuma-sys") (req "^0.0.8") (default-features #t) (kind 0)) (crate-dep (name "mlnx-ofed-libibverbs-sys") (req "^0.0.4") (default-features #t) (kind 0)))) (hash "01p55kmsdg5nb9za4zps8vl68rdvzryf5q9aj4v3lvldqd96z8gz")))

(define-public crate-mlnx-ofed-libmlx5-sys-0.0.5 (crate (name "mlnx-ofed-libmlx5-sys") (vers "0.0.5") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (target "cfg(target_os = \"linux\")") (kind 0)) (crate-dep (name "libnuma-sys") (req "^0.0.8") (default-features #t) (kind 0)) (crate-dep (name "mlnx-ofed-libibverbs-sys") (req "^0.0.5") (default-features #t) (kind 0)))) (hash "0x3769jg2chg5xs4bahzbvc24him32pfcnf1351gl3fnnbyai02a")))

(define-public crate-mlnx-ofed-libmlx5-sys-0.0.6 (crate (name "mlnx-ofed-libmlx5-sys") (vers "0.0.6") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (target "cfg(target_os = \"linux\")") (kind 0)) (crate-dep (name "libnuma-sys") (req "^0.0.9") (default-features #t) (kind 0)) (crate-dep (name "mlnx-ofed-libibverbs-sys") (req "^0.0.6") (default-features #t) (kind 0)))) (hash "0wi32d6c3cf479w6lyfwk7yfilnfcygkwlhv56jwmbdrdiai0qs4")))

(define-public crate-mlnx-ofed-libmlx5-sys-0.0.7 (crate (name "mlnx-ofed-libmlx5-sys") (vers "0.0.7") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (target "cfg(target_os = \"linux\")") (kind 0)) (crate-dep (name "libnuma-sys") (req "^0.0.9") (default-features #t) (kind 0)) (crate-dep (name "mlnx-ofed-libibverbs-sys") (req "^0.0.6") (default-features #t) (kind 0)))) (hash "0j54g1yqly11nb4acm9j83awj2dqp3g8d02ywzn5xyjkpz2m683f")))

(define-public crate-mlnx-ofed-librdmacm-sys-0.0.4 (crate (name "mlnx-ofed-librdmacm-sys") (vers "0.0.4") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (target "cfg(target_os = \"linux\")") (kind 0)) (crate-dep (name "mlnx-ofed-libibverbs-sys") (req "^0.0.4") (default-features #t) (kind 0)))) (hash "0v8y6r61wxcssbsrdxh3kl8064bs4k8yv431igakarg9ajhi2sac")))

(define-public crate-mlnx-ofed-librdmacm-sys-0.0.5 (crate (name "mlnx-ofed-librdmacm-sys") (vers "0.0.5") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (target "cfg(target_os = \"linux\")") (kind 0)) (crate-dep (name "mlnx-ofed-libibverbs-sys") (req "^0.0.5") (default-features #t) (kind 0)))) (hash "0slf73hpsgi89fdi3qnnw73ids6p3606rd9i26shg82176mn57y4")))

(define-public crate-mlnx-ofed-librdmacm-sys-0.0.6 (crate (name "mlnx-ofed-librdmacm-sys") (vers "0.0.6") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (target "cfg(target_os = \"linux\")") (kind 0)) (crate-dep (name "mlnx-ofed-libibverbs-sys") (req "^0.0.6") (default-features #t) (kind 0)))) (hash "0hs12zpvxlrgcv8bq2rvc16sk08di2lzd6xwj01gacmgvq76hxbd")))

