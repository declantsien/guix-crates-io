(define-module (crates-io ml og) #:use-module (crates-io))

(define-public crate-mlog-0.0.1 (crate (name "mlog") (vers "0.0.1") (hash "1ys4bm4ijkybwp0hk11k0nkknhws3hckh8jkwd88dhgb7p8yw4yr") (yanked #t)))

(define-public crate-mlog-0.1 (crate (name "mlog") (vers "0.1.0") (deps (list (crate-dep (name "log") (req "^0.4.6") (features (quote ("std"))) (default-features #t) (kind 0)))) (hash "1qv3y88zrbvxc134mjg1di5qkyldwbkj1dshw7bm7z3rzgqhsgpg")))

(define-public crate-mlog-0.2 (crate (name "mlog") (vers "0.2.0") (deps (list (crate-dep (name "log") (req "^0.4.6") (features (quote ("std"))) (default-features #t) (kind 0)))) (hash "0z9ab7y82k4g810wxiyyhcpk984mnn6c8s48fglc2mqj5nf2dlpj")))

(define-public crate-mlog-0.2 (crate (name "mlog") (vers "0.2.1") (deps (list (crate-dep (name "log") (req "^0.4.6") (features (quote ("std"))) (default-features #t) (kind 0)))) (hash "01xhc1bmdp20bjg2arcr6bds15vjah7dfqk86dc34a7vs1i51m8f")))

(define-public crate-mlog-0.3 (crate (name "mlog") (vers "0.3.0") (deps (list (crate-dep (name "hashbrown") (req "^0.1.8") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.6") (features (quote ("std"))) (default-features #t) (kind 0)))) (hash "1y5nyl6m1w6i12wc91s7yva08h1fgcmjpdj8sdr9insk5vcgky85")))

(define-public crate-mlog-0.3 (crate (name "mlog") (vers "0.3.1") (deps (list (crate-dep (name "hashbrown") (req "^0.1.8") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.6") (features (quote ("std"))) (default-features #t) (kind 0)))) (hash "1lcq6xzbv2d65djy99p9wjbw7an57rdw7mv48cqq431bxw6rgkj5")))

