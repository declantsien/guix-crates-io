(define-module (crates-io ml o-) #:use-module (crates-io))

(define-public crate-mlo-chacha20-rs-0.1 (crate (name "mlo-chacha20-rs") (vers "0.1.0") (hash "1riwqdwpxg3f2kdj2p4ilkmgss5hmgjz5szwv1jx0ihx9y29g32l")))

(define-public crate-mlo-chacha20-rs-0.1 (crate (name "mlo-chacha20-rs") (vers "0.1.1") (hash "1syykr68n8f368zqbrbmgp1b15kywzq0m5gkv5g28lzim4fnccs4")))

(define-public crate-mlo-chacha20-rs-0.1 (crate (name "mlo-chacha20-rs") (vers "0.1.2") (hash "1xiifyl2xyrwk00nknnw6bfhfsjdm01ld5s1ls6lp4dpv6ppxz2k")))

