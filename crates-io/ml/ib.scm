(define-module (crates-io ml ib) #:use-module (crates-io))

(define-public crate-mlib-0.1 (crate (name "mlib") (vers "0.1.0") (deps (list (crate-dep (name "rand") (req "^0.8") (default-features #t) (kind 0)))) (hash "11zsh7fl7b798j75kpgp03b5x56dmvnng9m5z18mn1d2n8zhcw52")))

(define-public crate-mlib-0.1 (crate (name "mlib") (vers "0.1.1") (deps (list (crate-dep (name "rand") (req "^0.8") (default-features #t) (kind 0)))) (hash "1qpkvai8gn3mm99fyr2lkp4kkkygvdc7nisavhrixm25qn0yaxl4")))

(define-public crate-mlib-0.1 (crate (name "mlib") (vers "0.1.2") (deps (list (crate-dep (name "rand") (req "^0.8") (default-features #t) (kind 0)))) (hash "04771p15pbqqz6c8nw1qmjwq9j026jxzy2sama86pymk6k67rjql")))

