(define-module (crates-io ml ir) #:use-module (crates-io))

(define-public crate-mlir-0.0.1 (crate (name "mlir") (vers "0.0.1") (hash "0gw96xsa1yz4q207h5vwx06w7jcr22yaknghkfc227nbmrbsz39h")))

(define-public crate-mlir-sys-0.1 (crate (name "mlir-sys") (vers "0.1.0") (deps (list (crate-dep (name "bindgen") (req "^0.59.1") (default-features #t) (kind 1)))) (hash "0xj9s3az2dqgj5mrknwpkr2kp26sbmxaln00rsswdqy3dln5syjj") (links "MLIR")))

(define-public crate-mlir-sys-0.1 (crate (name "mlir-sys") (vers "0.1.1") (deps (list (crate-dep (name "bindgen") (req "^0.60.1") (default-features #t) (kind 1)))) (hash "1y1z7di5s4wbinp55p4mxh0lxpxsxw8r594lwm54ppgnyhdij7xb") (links "MLIR")))

(define-public crate-mlir-sys-0.1 (crate (name "mlir-sys") (vers "0.1.2") (deps (list (crate-dep (name "bindgen") (req "^0.60.1") (default-features #t) (kind 1)))) (hash "1nnhf55gn9nnaxjrdhdc034mfpq13m22ky97m8w4dlpnggihh846") (links "MLIR")))

(define-public crate-mlir-sys-0.1 (crate (name "mlir-sys") (vers "0.1.3") (deps (list (crate-dep (name "bindgen") (req "^0.60.1") (default-features #t) (kind 1)))) (hash "15n8ll2an6pma6l4jqiqxshd1wlq2g36s1gpqihchnv8l33501xa") (links "MLIR")))

(define-public crate-mlir-sys-0.1 (crate (name "mlir-sys") (vers "0.1.4") (deps (list (crate-dep (name "bindgen") (req "^0.60.1") (default-features #t) (kind 1)))) (hash "1wnxxjsdly596h8kbf6wj2xnbgg7i9aq1d4ksnrj7f0iipllc2ki") (links "MLIR")))

(define-public crate-mlir-sys-0.2 (crate (name "mlir-sys") (vers "0.2.0") (deps (list (crate-dep (name "bindgen") (req "^0.65.1") (default-features #t) (kind 1)))) (hash "1cwkl5kg4c05d3iy7li9h9k2zsyj9wp74rv3bam9xxfmi3i2n9n1") (links "MLIR")))

(define-public crate-mlir-sys-0.2 (crate (name "mlir-sys") (vers "0.2.1") (deps (list (crate-dep (name "bindgen") (req "^0.68.1") (default-features #t) (kind 1)))) (hash "16k9yq55iyd4k5s3sl0kgcg1k2cv1crkixb0j3ymj9zdj59rmqfm") (links "MLIR")))

(define-public crate-mlir-sys-0.2 (crate (name "mlir-sys") (vers "0.2.2") (deps (list (crate-dep (name "bindgen") (req "^0.69.4") (default-features #t) (kind 1)))) (hash "0zqvy65dnj172cjx6s8rsi2h1r10vp5bhpafbk6mkj9vlq35h6qv") (links "MLIR")))

