(define-module (crates-io ml _m) #:use-module (crates-io))

(define-public crate-ml_monitoring-0.1 (crate (name "ml_monitoring") (vers "0.1.0") (deps (list (crate-dep (name "approx") (req "^0.3.1") (default-features #t) (kind 2)) (crate-dep (name "kolmogorov_smirnov") (req "^1.1.0") (default-features #t) (kind 0)) (crate-dep (name "slice-deque") (req "^0.1.16") (default-features #t) (kind 0)))) (hash "1b8wdcmdlahldzj9nkvlfkxvx8ddymr812ky1q190wjz3xy7li5i")))

