(define-module (crates-io ml -d) #:use-module (crates-io))

(define-public crate-ml-distance-0.1 (crate (name "ml-distance") (vers "0.1.0") (deps (list (crate-dep (name "approx") (req "^0.5.1") (default-features #t) (kind 0)))) (hash "1n94zhadw6cvlync8fvsy4b8iyx4l5dn9c9jfcc5ya26g9q8ic9k")))

(define-public crate-ml-distance-0.2 (crate (name "ml-distance") (vers "0.2.0") (deps (list (crate-dep (name "approx") (req "^0.5.1") (default-features #t) (kind 0)))) (hash "15l56vvdxl0xsklf2har0lhwd77n0g1jivx6hmbzm2snbd99v400")))

(define-public crate-ml-distance-0.2 (crate (name "ml-distance") (vers "0.2.1") (deps (list (crate-dep (name "approx") (req "^0.5.1") (default-features #t) (kind 0)))) (hash "1817wk910y6ymrsn56pnfnawh6fnw18jmphg4hwpj5mnmgc3b1qm")))

(define-public crate-ml-distance-0.2 (crate (name "ml-distance") (vers "0.2.3") (deps (list (crate-dep (name "approx") (req "^0.5.1") (default-features #t) (kind 0)))) (hash "1xz5kr74a4z43yph0kqh9k0dp0d4zx90pb66k931pb98frg93lsk")))

(define-public crate-ml-distance-1 (crate (name "ml-distance") (vers "1.0.0") (deps (list (crate-dep (name "approx") (req "^0.5.1") (default-features #t) (kind 0)))) (hash "00w04p0gkb62czg48b6y7vvghhygbg7hkg31749v2fvjwm6ndn0r")))

(define-public crate-ml-distance-1 (crate (name "ml-distance") (vers "1.0.1") (deps (list (crate-dep (name "approx") (req "^0.5.1") (default-features #t) (kind 0)))) (hash "0hx44irrcs9fm8q8sssc50in1lzpgh23yr0jca48cifdxhrdjhqw")))

(define-public crate-ml-downloader-0.1 (crate (name "ml-downloader") (vers "0.1.0") (deps (list (crate-dep (name "bytes") (req "^1.1.0") (default-features #t) (kind 0)) (crate-dep (name "digest") (req "^0.10.3") (default-features #t) (kind 0)) (crate-dep (name "fastrand") (req "^1.7.0") (default-features #t) (kind 0)) (crate-dep (name "hex") (req "^0.4.3") (default-features #t) (kind 0)) (crate-dep (name "readme-rustdocifier") (req "^0.1.1") (default-features #t) (kind 1)) (crate-dep (name "reqwest") (req "^0.11.9") (features (quote ("blocking"))) (default-features #t) (kind 0)) (crate-dep (name "sha2") (req "^0.10.2") (default-features #t) (kind 2)))) (hash "1f22pr3rkkpvxww4zly88fxhg2x0czwi1fcimzm21qlmayddn7v1")))

(define-public crate-ml-downloader-0.1 (crate (name "ml-downloader") (vers "0.1.1") (deps (list (crate-dep (name "bytes") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "digest") (req "^0.10.7") (default-features #t) (kind 0)) (crate-dep (name "fastrand") (req "^2.0.0") (default-features #t) (kind 0)) (crate-dep (name "hex") (req "^0.4.3") (default-features #t) (kind 0)) (crate-dep (name "readme-rustdocifier") (req "^0.1.1") (default-features #t) (kind 1)) (crate-dep (name "reqwest") (req "^0.11.18") (features (quote ("blocking"))) (default-features #t) (kind 0)) (crate-dep (name "sha2") (req "^0.10.7") (default-features #t) (kind 2)))) (hash "11xql3rcywi07khy3yxky9jsqdg8aqa54xr49hyrhgzk88gfzbr5")))

(define-public crate-ml-dsa-0.0.1 (crate (name "ml-dsa") (vers "0.0.1") (hash "0pxs4ll31ljipyslqkgva8mbrdfrq0nmygjhi1c88v5kvrfcycds")))

(define-public crate-ml-dsa-rs-0.1 (crate (name "ml-dsa-rs") (vers "0.1.0") (hash "1sbclas0y17c3vac7x0v47lmhp1snpyn1p0d0h9jp5s7w3fmk95b")))

(define-public crate-ml-dsa-rs-0.1 (crate (name "ml-dsa-rs") (vers "0.1.1") (hash "0i01qavi4mb432pys9jqiqcdzpfq1qx9b0imx94n5abbg2b6lhn9")))

