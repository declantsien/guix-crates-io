(define-module (crates-io ml -p) #:use-module (crates-io))

(define-public crate-ml-progress-0.1 (crate (name "ml-progress") (vers "0.1.0") (deps (list (crate-dep (name "parking_lot") (req "^0.12.0") (default-features #t) (kind 0)) (crate-dep (name "readme-rustdocifier") (req "^0.1.1") (default-features #t) (kind 1)) (crate-dep (name "terminal_size") (req "^0.1.17") (default-features #t) (kind 0)))) (hash "04xw87gx9i4ln4an9gz3in2pgvlcp8p4i4nrb8sk4gii3np9kzkq")))

