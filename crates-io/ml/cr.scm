(define-module (crates-io ml cr) #:use-module (crates-io))

(define-public crate-mlcr-0.1 (crate (name "mlcr") (vers "0.1.0") (deps (list (crate-dep (name "nn") (req "^0.1") (default-features #t) (kind 0)))) (hash "1si9x1nx7a555yqd3kyr4i0aihm15wx3j47j684x5rar330b44r9")))

(define-public crate-mlcr-0.2 (crate (name "mlcr") (vers "0.2.0") (deps (list (crate-dep (name "crossbeam") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "nn") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "parking_lot") (req "^0.3") (default-features #t) (kind 0)))) (hash "02agpzii3paplxahah9hwm0b29ipqqcdnlmx0vyzp1cskcppzdzk")))

