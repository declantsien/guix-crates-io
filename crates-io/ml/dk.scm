(define-module (crates-io ml dk) #:use-module (crates-io))

(define-public crate-mldkyt-uwurandom-rs-0.1 (crate (name "mldkyt-uwurandom-rs") (vers "0.1.0") (deps (list (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)))) (hash "1j65j7jgvx7301cjjka9px752h0s8pxicrg8jmq10h747gy7ivim")))

(define-public crate-mldkyt-uwurandom-rs-0.1 (crate (name "mldkyt-uwurandom-rs") (vers "0.1.1") (deps (list (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)))) (hash "1lmyg4k9vwgf5819hiy8shb5dprj1mbpzq4qjlh33kk3sb59id43")))

