(define-module (crates-io ml t-) #:use-module (crates-io))

(define-public crate-mlt-sys-0.1 (crate (name "mlt-sys") (vers "0.1.0") (hash "0g2m9cvzwf5jzbxk47zx6vilqsys4jc9acm14ks04r640nxdm3f7") (links "mlt")))

(define-public crate-mlt-sys-0.1 (crate (name "mlt-sys") (vers "0.1.1") (deps (list (crate-dep (name "os_type") (req "^2.0.0") (default-features #t) (kind 1)))) (hash "01ymy8mxha6nqpyr6pi5sb2k50ynmkp9icrghkicv9c9ly7vcfyk") (links "mlt")))

