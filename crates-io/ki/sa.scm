(define-module (crates-io ki sa) #:use-module (crates-io))

(define-public crate-kisaseed-0.1 (crate (name "kisaseed") (vers "0.1.0") (deps (list (crate-dep (name "cipher") (req "^0.4.4") (default-features #t) (kind 0)) (crate-dep (name "zeroize") (req "^1.6.0") (optional #t) (default-features #t) (kind 0)))) (hash "13nyyna4z8i9s0hi57id3mxvw58jr7q1sng1pvfyk7a9zf75wsca") (v 2) (features2 (quote (("zeroize" "dep:zeroize")))) (rust-version "1.56")))

(define-public crate-kisaseed-0.1 (crate (name "kisaseed") (vers "0.1.1") (deps (list (crate-dep (name "cipher") (req "^0.4.4") (default-features #t) (kind 0)) (crate-dep (name "cipher") (req "^0.4.4") (features (quote ("dev"))) (default-features #t) (kind 2)))) (hash "0l5zfhsynxg67mvmnw5h908nbgzq813w4vx7nbiiin2bp062nx79") (features (quote (("zeroize" "cipher/zeroize")))) (rust-version "1.56")))

(define-public crate-kisaseed-0.1 (crate (name "kisaseed") (vers "0.1.2") (deps (list (crate-dep (name "cipher") (req "^0.4.4") (default-features #t) (kind 0)) (crate-dep (name "cipher") (req "^0.4.4") (features (quote ("dev"))) (default-features #t) (kind 2)))) (hash "1x1vb4qxjf26iy0h7mrbc92s62wkj65lgycwvv6ywm53w3a303vh") (features (quote (("zeroize" "cipher/zeroize")))) (rust-version "1.56")))

(define-public crate-kisaseed-0.1 (crate (name "kisaseed") (vers "0.1.3") (deps (list (crate-dep (name "cipher") (req "^0.4.4") (default-features #t) (kind 0)) (crate-dep (name "cipher") (req "^0.4.4") (features (quote ("dev"))) (default-features #t) (kind 2)))) (hash "1pscsknsrvwn2z7gvlg1sqs4p5mkppbmbf0lcspg9d5cizfrn6wm") (features (quote (("zeroize" "cipher/zeroize")))) (rust-version "1.56")))

