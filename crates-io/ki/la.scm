(define-module (crates-io ki la) #:use-module (crates-io))

(define-public crate-kilac-0.1 (crate (name "kilac") (vers "0.1.0") (hash "1f890rcc8c4cbh6i2nbg5xbvirdkksglbcv3wyqammmnp0srzrnq")))

(define-public crate-kilac-0.1 (crate (name "kilac") (vers "0.1.1") (hash "1r9cjz2flbql7s8kb5mbrc099l7vss1cbmfh906sjzlpkjbnyyai")))

(define-public crate-kilac-0.1 (crate (name "kilac") (vers "0.1.2") (deps (list (crate-dep (name "pretty_assertions") (req "^0.5.0") (default-features #t) (kind 2)))) (hash "11315c5fqm01mpysdbl7gwh744gpb59y2v7sz5glxxzic8b7ivbz")))

(define-public crate-kilac-0.1 (crate (name "kilac") (vers "0.1.3") (deps (list (crate-dep (name "pretty_assertions") (req "^0.5.0") (default-features #t) (kind 2)))) (hash "00ccqlsbi0vpdmn9mjq8d81lfjxfvxgnnqqbfih6hc6av1npddbl")))

(define-public crate-kilac-0.2 (crate (name "kilac") (vers "0.2.0") (deps (list (crate-dep (name "criterion") (req "^0.2.0") (default-features #t) (kind 2)) (crate-dep (name "pretty_assertions") (req "^0.5.0") (default-features #t) (kind 2)))) (hash "0jhw8wymnphp23m9wjg73055vqmaqsqy7ysabsj9pnqamw7x85a1")))

