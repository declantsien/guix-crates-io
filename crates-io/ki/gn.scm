(define-module (crates-io ki gn) #:use-module (crates-io))

(define-public crate-kignore-0.1 (crate (name "kignore") (vers "0.1.0") (deps (list (crate-dep (name "eyre") (req "^0.6.8") (default-features #t) (kind 0)) (crate-dep (name "gitignore_inner") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "0miwfc9kfbnf0lvihwq238si10zjdqa1npvq1wwd2nkpchzvhr8z")))

(define-public crate-kignore-0.1 (crate (name "kignore") (vers "0.1.1") (deps (list (crate-dep (name "eyre") (req "^0.6.8") (default-features #t) (kind 0)) (crate-dep (name "gitignore_inner") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "09mgvbm66xq051jsw9k54safxnfm4h1mkcanc9lhpnx5hybvxrnx")))

