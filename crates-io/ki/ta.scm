(define-module (crates-io ki ta) #:use-module (crates-io))

(define-public crate-kitamura-0.1 (crate (name "kitamura") (vers "0.1.0") (deps (list (crate-dep (name "serde") (req "^1") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1") (default-features #t) (kind 0)))) (hash "00n8xmcg7zzcdcqnp4anikhgalc146zv2sp8j3mkh3n6zmhwidkq")))

(define-public crate-kitamura-0.1 (crate (name "kitamura") (vers "0.1.1") (deps (list (crate-dep (name "serde") (req "^1") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1") (default-features #t) (kind 0)))) (hash "11lqwdvx6n72al2ky189cv0fzlbnvpihmm3fx2ifk3vipbhsch69")))

(define-public crate-kitamura-0.1 (crate (name "kitamura") (vers "0.1.2") (deps (list (crate-dep (name "serde") (req "^1") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1") (default-features #t) (kind 0)))) (hash "0s5f5qwd382km6bzfdd07ryd9isba61mqrh8jwkqyfw4h2z3by15")))

(define-public crate-kitamura-0.1 (crate (name "kitamura") (vers "0.1.4") (deps (list (crate-dep (name "serde") (req "^1") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1") (default-features #t) (kind 0)))) (hash "1wl0zq7r6n5pmp35d1zzzzqy4ivk6idiv9lja9yr7z4qvlfv3jgr")))

(define-public crate-kitamura-0.1 (crate (name "kitamura") (vers "0.1.5") (deps (list (crate-dep (name "serde") (req "^1") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1") (default-features #t) (kind 0)))) (hash "0cp8dfzq1x8yzr812jzyrjxs53vp7psm7bsqfmc0zgksx374p780")))

(define-public crate-kitamura-0.1 (crate (name "kitamura") (vers "0.1.6") (deps (list (crate-dep (name "serde") (req "^1") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1") (default-features #t) (kind 0)))) (hash "1xhfpp3riiaaj3vsdzvkific625yiv8jnwy5jqq95n978grgvdk5")))

(define-public crate-kitamura-0.1 (crate (name "kitamura") (vers "0.1.7") (deps (list (crate-dep (name "serde") (req "^1") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1") (default-features #t) (kind 0)))) (hash "10jzz9clp4vrhcynhy6qihy4lk8rsaqv322vzl0p2i7w6s3vk5gs")))

(define-public crate-kitamura-0.2 (crate (name "kitamura") (vers "0.2.0") (deps (list (crate-dep (name "serde") (req "^1") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1") (default-features #t) (kind 0)))) (hash "05vlb2bbbwx19yif46aai3m229mrc7xxbqhj23x4pnggxpjls800")))

