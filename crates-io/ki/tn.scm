(define-module (crates-io ki tn) #:use-module (crates-io))

(define-public crate-kitn-0.0.0 (crate (name "kitn") (vers "0.0.0") (hash "1bc2640shcgd0hvq33qgcngx81cn1fyh49xwpjlwardda11gj6vs")))

(define-public crate-kitn_python-0.0.0 (crate (name "kitn_python") (vers "0.0.0") (deps (list (crate-dep (name "kitn") (req "^0.0.0") (default-features #t) (kind 0)) (crate-dep (name "pyo3") (req "^0.20") (features (quote ("abi3-py37" "extension-module"))) (default-features #t) (kind 0)))) (hash "1xfibq5nl76c50ywaqw78yp403yd681c1a493gi1f47sjf87q2ym")))

(define-public crate-kitn_python-0.0.1 (crate (name "kitn_python") (vers "0.0.1") (hash "1lailw8v0vcmq0zm4mz63iwsaw68k61cflwfd89ynxd06ijfj948")))

