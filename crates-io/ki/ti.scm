(define-module (crates-io ki ti) #:use-module (crates-io))

(define-public crate-kitin-0.1 (crate (name "kitin") (vers "0.1.0") (hash "1ybrx260r1787jpmflrmvz3dnhwndm5sd98xwpr2645zgpmb5f4c")))

(define-public crate-kitin-0.1 (crate (name "kitin") (vers "0.1.1") (deps (list (crate-dep (name "clap") (req "^3.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)))) (hash "0sf3c1mlbhh3silvllwlv25wiksfng70mrlnr2c3f8smw3chbh3v")))

(define-public crate-kitin-0.1 (crate (name "kitin") (vers "0.1.2") (deps (list (crate-dep (name "clap") (req "^3.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_yaml") (req "^0.8.25") (default-features #t) (kind 0)))) (hash "0fzjbn7lirkd9jqgnl0d1js7pg82k9ppqmmlngacfak8ar1vbps5")))

