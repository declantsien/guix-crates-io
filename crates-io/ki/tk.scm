(define-module (crates-io ki tk) #:use-module (crates-io))

(define-public crate-kitkat-0.1 (crate (name "kitkat") (vers "0.1.0") (hash "12j0f3hvvnja59ijjm7f3rx4ig9zjyp7xdv3wg54vfc290byg0nx") (yanked #t)))

(define-public crate-kitkat-0.1 (crate (name "kitkat") (vers "0.1.1") (hash "1b7m4j1f7j9r7xmm8ggxyh5d4ifiar0v8gr5k1xkh80znbhw09wa") (yanked #t)))

(define-public crate-kitkat-1 (crate (name "kitkat") (vers "1.0.0") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "minifb") (req "^0.19.1") (default-features #t) (kind 0)))) (hash "1dii0sjn3993k7ba39s173l1l4rbxhzi126wrnw6yla1fsryc8m2")))

(define-public crate-kitkat-1 (crate (name "kitkat") (vers "1.1.0") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "minifb") (req "^0.19.1") (default-features #t) (kind 0)))) (hash "06rrajx9dfchi0r17k2lyn69qck5x9fckdy0g8w375lpynm109sd")))

(define-public crate-kitkat-1 (crate (name "kitkat") (vers "1.1.1") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "minifb") (req "^0.19.1") (default-features #t) (kind 0)))) (hash "0lyhgi23sncazizg6g1xp5gg2pypk56s3w72vqr5hgzfqmh85svh")))

(define-public crate-kitkat-1 (crate (name "kitkat") (vers "1.1.2") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "minifb") (req "^0.19") (default-features #t) (kind 0)))) (hash "1r7vkils10lfvia3g5vnd58qffval61vc5j59fyxwdjq28ik4vps")))

(define-public crate-kitkat-1 (crate (name "kitkat") (vers "1.1.3") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "minifb") (req "^0.19") (default-features #t) (kind 0)))) (hash "0qsg7bavg14whhws6wgjj9wchkia7x6b8wvhm1siz2x35hc0q1wb")))

