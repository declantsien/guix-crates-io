(define-module (crates-io ki it) #:use-module (crates-io))

(define-public crate-kiiterm-0.0.0 (crate (name "kiiterm") (vers "0.0.0") (deps (list (crate-dep (name "kii") (req "^0.0.1") (default-features #t) (kind 0)) (crate-dep (name "termion") (req "^1.5.6") (default-features #t) (kind 0)))) (hash "08mziad2bh5zgrnxzhnvckzs8z25722q8dvm4sbv3db8vwahyznj")))

