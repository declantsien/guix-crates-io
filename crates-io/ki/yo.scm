(define-module (crates-io ki yo) #:use-module (crates-io))

(define-public crate-kiyomi-0.1 (crate (name "kiyomi") (vers "0.1.0") (hash "1a1x6hmxxmdima5xzbyjynwsp484xcssl3cga2m3llp3y5lci7a3")))

(define-public crate-kiyomi_ecs-0.1 (crate (name "kiyomi_ecs") (vers "0.1.0") (hash "1bn2cfr4sfqljaabgz1wdj7zrqgy7laikr3gffc13baymlvyqd9x")))

