(define-module (crates-io ki zu) #:use-module (crates-io))

(define-public crate-kizuna-0.1 (crate (name "kizuna") (vers "0.1.0") (deps (list (crate-dep (name "tokio") (req "^1.27.0") (features (quote ("rt" "rt-multi-thread" "macros"))) (default-features #t) (kind 2)))) (hash "0jkr193rhl9nxgqhhp825jxbnsa7sn0iqw7wlnmhspy3cf9rn66j")))

