(define-module (crates-io ki ru) #:use-module (crates-io))

(define-public crate-KiruganCargoTest-0.1 (crate (name "KiruganCargoTest") (vers "0.1.0") (hash "0hm7pliqx77yijd63r4qg05g1gln1hwg39dq1f9xprbl01vzdykc")))

(define-public crate-kirunadb-0.0.1 (crate (name "kirunadb") (vers "0.0.1") (deps (list (crate-dep (name "cap-std") (req "^1.0.1") (default-features #t) (kind 0)) (crate-dep (name "cxx") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "cxx-build") (req "^1.0") (default-features #t) (kind 1)) (crate-dep (name "tempfile") (req "^3.3.0") (default-features #t) (kind 0)))) (hash "1k1i70vsqj9srh72nj8b8jkxgh5a9wrknzbn2mpzh8skkjmanbj8")))

