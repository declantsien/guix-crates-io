(define-module (crates-io ki lt) #:use-module (crates-io))

(define-public crate-kilt-api-client-1 (crate (name "kilt-api-client") (vers "1.6.2") (deps (list (crate-dep (name "codec") (req "^3") (features (quote ("derive" "full" "bit-vec"))) (kind 0) (package "parity-scale-codec")) (crate-dep (name "subxt") (req "^0.21") (default-features #t) (kind 0)))) (hash "17dslk7lb2cw40d3k2jm3gx88rwvcib7xx0x3dnij000azgl3d89")))

