(define-module (crates-io ki je) #:use-module (crates-io))

(define-public crate-kijetesantakaluotokieni-1 (crate (name "kijetesantakaluotokieni") (vers "1.0.0") (deps (list (crate-dep (name "clap") (req "^3.1.12") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "voca_rs") (req "^1.14.0") (default-features #t) (kind 0)))) (hash "13fm9gp7xxl2isnb3cqc8xhhfvbdmq7hbnhc136x29icdkv5n1ki")))

(define-public crate-kijetesantakaluotokieni-1 (crate (name "kijetesantakaluotokieni") (vers "1.0.1") (deps (list (crate-dep (name "clap") (req "^3.1.12") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "voca_rs") (req "^1.14.0") (default-features #t) (kind 0)))) (hash "0qwhqfnmijfbq45lc02drrhn5n3p9z43s96fiqz8piq1n3knpx5d")))

