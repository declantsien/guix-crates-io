(define-module (crates-io ki ri) #:use-module (crates-io))

(define-public crate-kiri-0.1 (crate (name "kiri") (vers "0.1.0") (deps (list (crate-dep (name "evdev") (req "^0.12.1") (default-features #t) (kind 0)) (crate-dep (name "evdev-keys") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2.138") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.17") (default-features #t) (kind 0)) (crate-dep (name "rustc-hash") (req "^1.1.0") (default-features #t) (kind 0)))) (hash "0pb6dnlj4j89i1zfws2kc2mjlrv4k3jyjgdlgc9n1z90hgb0rw72")))

(define-public crate-kiri-0.2 (crate (name "kiri") (vers "0.2.0") (deps (list (crate-dep (name "evdev") (req "^0.12.1") (default-features #t) (kind 0)) (crate-dep (name "evdev-keys") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2.138") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.17") (default-features #t) (kind 0)) (crate-dep (name "rustc-hash") (req "^1.1.0") (default-features #t) (kind 0)))) (hash "130q5s6s44jw7dhzqc2iv0zz5vrw583921dmj30sqwyiaf9abcan")))

(define-public crate-kiri-0.3 (crate (name "kiri") (vers "0.3.0") (deps (list (crate-dep (name "evdev") (req "^0.12.1") (default-features #t) (kind 0)) (crate-dep (name "evdev-keys") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2.138") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.17") (default-features #t) (kind 0)) (crate-dep (name "rustc-hash") (req "^1.1.0") (default-features #t) (kind 0)))) (hash "1s9d000gl002kzwhgk31chfanmcjjvzds838y8jdwqxbxfagzzcp")))

(define-public crate-kirigami-0.1 (crate (name "kirigami") (vers "0.1.0") (deps (list (crate-dep (name "kde_frameworks") (req "^0.1") (default-features #t) (kind 1)))) (hash "15w8dljzm7ci7bpl01zly7swgqns6w3jsr5z4sg1irzzgx4grqwn") (links "Kirigami2")))

(define-public crate-kirin-0.1 (crate (name "kirin") (vers "0.1.0") (hash "196wxhdll1rj1hfhfvbgcsc5rhin9bglwrjwqzjjx6n040i84cfk")))

(define-public crate-kirino-0.1 (crate (name "kirino") (vers "0.1.0") (hash "0vrqgcjbcz96yk78n65blylakh1ww8k0xbg9m4fx0l4gzlln12q3")))

(define-public crate-kiririn-0.1 (crate (name "kiririn") (vers "0.1.0") (hash "0n2q97sin6vqsl3r6g0zs2xa2g9m5c3y2pdsmbqf49fv3bjx9zl7")))

