(define-module (crates-io ki nc) #:use-module (crates-io))

(define-public crate-kincaid-0.1 (crate (name "kincaid") (vers "0.1.0") (hash "1pr4l5c2fsp2j68wn85ixfh40nmmvciyxjgsh7cprkrq5ph06wwc")))

(define-public crate-kincaid-0.2 (crate (name "kincaid") (vers "0.2.0") (deps (list (crate-dep (name "lazy_static") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)))) (hash "12qz69nj7ffvm8yb68742jssfv6wy9dnahaa12473kw9kpibag3m")))

(define-public crate-kincaid-0.2 (crate (name "kincaid") (vers "0.2.2") (deps (list (crate-dep (name "lazy_static") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)))) (hash "0asq6qq9dm49hcmlnsh3g556cpp1jyjab9km39qwdb0r2hhzwxq2")))

(define-public crate-kincaid-0.2 (crate (name "kincaid") (vers "0.2.4") (deps (list (crate-dep (name "console_error_panic_hook") (req "^0.1.6") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "wasm-bindgen") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "wasm-bindgen-test") (req "^0.3.13") (default-features #t) (kind 2)) (crate-dep (name "wee_alloc") (req "^0.4.5") (optional #t) (default-features #t) (kind 0)))) (hash "0231kvi96vp1nb8mr5b0jwr4x4znfbcdbd0n3pd28ldic5n3lzc2") (features (quote (("default" "console_error_panic_hook"))))))

