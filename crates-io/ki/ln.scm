(define-module (crates-io ki ln) #:use-module (crates-io))

(define-public crate-kiln-0.1 (crate (name "kiln") (vers "0.1.0") (hash "0lvnkhrwqhvvka5p1blp99qjs5bri9xn40w56b59cwmyz9xxdfvg")))

(define-public crate-kiln-0.1 (crate (name "kiln") (vers "0.1.1") (hash "0q0di4686hxab944fz3g6rdib263jwp1rrap287mgrmhh2qwkmd6")))

(define-public crate-kiln-0.1 (crate (name "kiln") (vers "0.1.2") (hash "1bn37icqhsz7iaw57frvg4bxabqbf7i83fywlnbs1bi5jqfjwa7i")))

(define-public crate-kiln-0.1 (crate (name "kiln") (vers "0.1.3") (deps (list (crate-dep (name "uuid") (req "^0.7") (features (quote ("v4"))) (default-features #t) (kind 0)))) (hash "04x9idpwn84n8aj1hdp87k036j71358naqjjmdpyijhrchg1zdf5")))

(define-public crate-kiln-0.2 (crate (name "kiln") (vers "0.2.0") (deps (list (crate-dep (name "uuid") (req "^0.7") (features (quote ("v4"))) (default-features #t) (kind 0)))) (hash "1mx757aahz7c2la9bw1kifyl63197gasgfr2ss2jsgylbw70qd6a")))

