(define-module (crates-io ki pp) #:use-module (crates-io))

(define-public crate-kippschalter-0.1 (crate (name "kippschalter") (vers "0.1.0") (deps (list (crate-dep (name "i3ipc") (req "^0.10.1") (features (quote ("i3-4-14"))) (default-features #t) (kind 0)))) (hash "19wd4vyg492kr6ivl39i8kd3r51ci08vx67pwfiviyhnbhc0arjh") (yanked #t)))

(define-public crate-kippschalter-0.2 (crate (name "kippschalter") (vers "0.2.1") (deps (list (crate-dep (name "env_logger") (req "^0.7.1") (default-features #t) (kind 0)) (crate-dep (name "i3ipc") (req "^0.10.1") (features (quote ("i3-4-14"))) (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)))) (hash "0k1cm65llbbilc2i8l6v1fxlp3d7k0k5lrkqfb6bmiqmgzgx20kj") (yanked #t)))

