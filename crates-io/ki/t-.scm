(define-module (crates-io ki t-) #:use-module (crates-io))

(define-public crate-kit-ais-dataset-0.1 (crate (name "kit-ais-dataset") (vers "0.1.0") (deps (list (crate-dep (name "anyhow") (req "^1.0.44") (default-features #t) (kind 0)) (crate-dep (name "chrono") (req "^0.4.19") (default-features #t) (kind 0)) (crate-dep (name "noisy_float") (req "^0.2.0") (features (quote ("serde"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.130") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde-xml-rs") (req "^0.5.0") (default-features #t) (kind 0)))) (hash "1xmkyazbf8grcm9rdfjb8aj1qn285p3vazbfdyz7hkyr60cizmv5")))

