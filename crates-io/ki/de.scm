(define-module (crates-io ki de) #:use-module (crates-io))

(define-public crate-kidex-common-0.1 (crate (name "kidex-common") (vers "0.1.0") (deps (list (crate-dep (name "serde") (req "^1.0.152") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.93") (optional #t) (default-features #t) (kind 0)))) (hash "0fg5jx533k1whkx1wfnlhysplzqqw1qc6743arx29cx0kv9n3lfr") (v 2) (features2 (quote (("util" "dep:serde_json"))))))

