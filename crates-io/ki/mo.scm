(define-module (crates-io ki mo) #:use-module (crates-io))

(define-public crate-kimono-0.0.0 (crate (name "kimono") (vers "0.0.0") (deps (list (crate-dep (name "stretch") (req "^0.3.2") (default-features #t) (kind 0)))) (hash "14smxg5j4h0k7jds5257c13a0r9lb2zjnyjgngh8bkj3wpvcj3qk")))

(define-public crate-kimono-0.0.1 (crate (name "kimono") (vers "0.0.1") (deps (list (crate-dep (name "ansi-escapes") (req "^0.1.1") (default-features #t) (kind 0)) (crate-dep (name "colored") (req "^2.0.0") (default-features #t) (kind 0)) (crate-dep (name "unicode-width") (req "^0.1.9") (default-features #t) (kind 0)))) (hash "1mj7c30ddc1n7p1hvssi0j9r65ppa10605vjafxdmam80w8g3d38")))

(define-public crate-kimono-0.0.2 (crate (name "kimono") (vers "0.0.2") (deps (list (crate-dep (name "ansi-escapes") (req "^0.1.1") (default-features #t) (kind 0)) (crate-dep (name "colored") (req "^2.0.0") (default-features #t) (kind 0)) (crate-dep (name "unicode-width") (req "^0.1.9") (default-features #t) (kind 0)))) (hash "1frm38m1jh88r8560grl4f5s4z89nypidknaqpzcrd1zqap1h8ap")))

(define-public crate-kimono-0.0.3 (crate (name "kimono") (vers "0.0.3") (deps (list (crate-dep (name "ansi-escapes") (req "^0.1.1") (default-features #t) (kind 0)) (crate-dep (name "colored") (req "^2.0.0") (default-features #t) (kind 0)) (crate-dep (name "unicode-width") (req "^0.1.9") (default-features #t) (kind 0)))) (hash "0yhflbgbjc35y5asw0lzrcmrqb84r8va041sp6jfc2r2c11a38wi")))

(define-public crate-kimono-0.0.4 (crate (name "kimono") (vers "0.0.4") (deps (list (crate-dep (name "ansi-escapes") (req "^0.1.1") (default-features #t) (kind 0)) (crate-dep (name "colored") (req "^2.0.0") (default-features #t) (kind 0)) (crate-dep (name "unicode-width") (req "^0.1.9") (default-features #t) (kind 0)))) (hash "13a6dyfk3i6fc7ya6cclgg8rhf5cisshamk0fz96lldbgi69j7v9")))

(define-public crate-kimono-0.0.5 (crate (name "kimono") (vers "0.0.5") (deps (list (crate-dep (name "ansi-escapes") (req "^0.1.1") (default-features #t) (kind 0)) (crate-dep (name "colored") (req "^2.0.0") (default-features #t) (kind 0)) (crate-dep (name "unicode-width") (req "^0.1.9") (default-features #t) (kind 0)))) (hash "0xjvqk8pmfrqqy7b4fy4w8nfq23dvp79r5czs58nv7gxhgxb7rag")))

(define-public crate-kimono-0.0.6 (crate (name "kimono") (vers "0.0.6") (deps (list (crate-dep (name "ansi-escapes") (req "^0.1.1") (default-features #t) (kind 0)) (crate-dep (name "colored") (req "^2.0.0") (default-features #t) (kind 0)) (crate-dep (name "unicode-width") (req "^0.1.9") (default-features #t) (kind 0)))) (hash "0vcz54wyiqf70q4rjj73mhccnk4qlg8pfm6mlbb85r65fz1bvvwz")))

(define-public crate-kimono-0.0.7 (crate (name "kimono") (vers "0.0.7") (deps (list (crate-dep (name "ansi-escapes") (req "^0.1.1") (default-features #t) (kind 0)) (crate-dep (name "colored") (req "^2.0.0") (default-features #t) (kind 0)) (crate-dep (name "unicode-width") (req "^0.1.9") (default-features #t) (kind 0)))) (hash "0138qgf3w1aww6x9ni7kr4356wqi2zd1bfv5z5issaz71m87rpc0")))

(define-public crate-kimono-0.1 (crate (name "kimono") (vers "0.1.0") (deps (list (crate-dep (name "ansi-escapes") (req "^0.1.1") (default-features #t) (kind 0)) (crate-dep (name "colored") (req "^2.0.0") (default-features #t) (kind 0)) (crate-dep (name "unicode-width") (req "^0.1.9") (default-features #t) (kind 0)))) (hash "0jxdix1cmqzc63llvina6k4i9aycwpdrd0a7wrvnhjd3z0s80d08")))

(define-public crate-kimono-0.1 (crate (name "kimono") (vers "0.1.1") (deps (list (crate-dep (name "ansi-escapes") (req "^0.1.1") (default-features #t) (kind 0)) (crate-dep (name "colored") (req "^2.0.0") (default-features #t) (kind 0)) (crate-dep (name "unicode-width") (req "^0.1.9") (default-features #t) (kind 0)))) (hash "0hgfxdaz2pnjfg6gk9dk1rdlrdhhjxz7pdazcwdmbh01l1pj75gl")))

(define-public crate-kimono-0.1 (crate (name "kimono") (vers "0.1.2") (deps (list (crate-dep (name "ansi-escapes") (req "^0.1.1") (default-features #t) (kind 0)) (crate-dep (name "colored") (req "^2.0.0") (default-features #t) (kind 0)) (crate-dep (name "unicode-width") (req "^0.1.9") (default-features #t) (kind 0)))) (hash "0nqyb9xkc7jvm1whv43kyl5lwv2dzmws68d0s7gwlql0d7h0qz8c")))

(define-public crate-kimono-0.1 (crate (name "kimono") (vers "0.1.3") (deps (list (crate-dep (name "ansi-escapes") (req "^0.1.1") (default-features #t) (kind 0)) (crate-dep (name "colored") (req "^2.0.0") (default-features #t) (kind 0)) (crate-dep (name "ukiyoe") (req "^0") (default-features #t) (kind 2)) (crate-dep (name "unicode-width") (req "^0.1.9") (default-features #t) (kind 0)))) (hash "0jhk68dfx21s57d3sji7yr7kjzlmbv36s0h4kdqs3xw716i915y2")))

