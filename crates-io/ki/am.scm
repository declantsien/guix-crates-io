(define-module (crates-io ki am) #:use-module (crates-io))

(define-public crate-kiam-0.1 (crate (name "kiam") (vers "0.1.0") (hash "1qjwx6zggvfrw27qh4cq16grcq28p1agi2i9r8msrcwigaq35v2g")))

(define-public crate-kiam-0.1 (crate (name "kiam") (vers "0.1.1") (hash "1jzx71gn8p9c9hi23wy9mlqq96gcdprl456xvvlya6p4hljc63yb")))

(define-public crate-kiama-0.1 (crate (name "kiama") (vers "0.1.0") (hash "0x91wk152j6nwvk6bczy1z92s891cvlrnjm0ybs73amnxblvjcvk") (yanked #t) (rust-version "1.77.0")))

(define-public crate-kiama-0.0.0 (crate (name "kiama") (vers "0.0.0") (hash "1572b72jmnbp3af7ixwgrzjscg7xn1wgvv4gyx8nmccdk24sgz2r") (rust-version "1.77.0")))

