(define-module (crates-io ki ok) #:use-module (crates-io))

(define-public crate-kioku-0.1 (crate (name "kioku") (vers "0.1.0") (hash "1w886mracxmkwfxslbl7480am3216anbbp1mip2jp09vy5yrk9rn")))

(define-public crate-kioku-0.2 (crate (name "kioku") (vers "0.2.0") (hash "1n1y7zpj7zb8wjw7amqszlbb9h3fmamicdag1lgwv2rcm7ff8mkr")))

(define-public crate-kioku-0.3 (crate (name "kioku") (vers "0.3.0") (hash "09nyvq68xsnrmkd2z26ph1ksdr78gfxzk95gfnx63kwc08kpn89h")))

(define-public crate-kioku-0.3 (crate (name "kioku") (vers "0.3.1") (hash "16a970h8bvy1bm5m76y9iwsnbbr7yrak06lk8xr4d8pjzlwp55kd")))

