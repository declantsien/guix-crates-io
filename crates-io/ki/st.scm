(define-module (crates-io ki st) #:use-module (crates-io))

(define-public crate-kistro-0.1 (crate (name "kistro") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^4.3.21") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "1ivv9zj2dpy2nl4grrsc4r6rnvgi11h4alid4jdkg5p3qffqa6zk")))

(define-public crate-kistro-0.1 (crate (name "kistro") (vers "0.1.1") (deps (list (crate-dep (name "clap") (req "^4.3.21") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "14kyd41xfpy0db7brrkvn4dnfkdkyjlfr8pxcymh14jq5lvfixnr")))

