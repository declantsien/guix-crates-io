(define-module (crates-io jo ss) #:use-module (crates-io))

(define-public crate-joss-0.0.0 (crate (name "joss") (vers "0.0.0") (hash "0nqz4s8azcyp177g2dvclq94iyv0nq4isqv6ijv8qw5v6shpws6j")))

(define-public crate-joss-0.0.1 (crate (name "joss") (vers "0.0.1") (deps (list (crate-dep (name "cstring") (req "^0.0.2") (default-features #t) (kind 0)))) (hash "01i9zwl241qyaqimwhsgrf6vnz9hs0jas4phskw2mad4shq7pa4r")))

(define-public crate-joss-0.0.2 (crate (name "joss") (vers "0.0.2") (deps (list (crate-dep (name "cstring") (req "^0.0.2") (default-features #t) (kind 0)))) (hash "11g937xhnzw23i814xzpal31rihipc8lbcl5j71kb2nj6x0y1j9p")))

