(define-module (crates-io jo ps) #:use-module (crates-io))

(define-public crate-jops-0.1 (crate (name "jops") (vers "0.1.0") (deps (list (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)))) (hash "0mj1qga5kl7f7wq98k9j78fnkcq2bf0whshjn1fbsjscfzm1d6sz")))

(define-public crate-jops-0.1 (crate (name "jops") (vers "0.1.1") (deps (list (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)))) (hash "1535ij34ckklm4pyvzg5yc3kw4n4lxdy7xc3jlna4vxvkwrhq2mx")))

(define-public crate-jops-0.2 (crate (name "jops") (vers "0.2.0") (deps (list (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)))) (hash "0svzka2q4jclwihipcgskprrg2v6gvz48zq296w8g61ld1jd23g6")))

