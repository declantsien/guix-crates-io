(define-module (crates-io jo z-) #:use-module (crates-io))

(define-public crate-joz-logger-0.1 (crate (name "joz-logger") (vers "0.1.0") (deps (list (crate-dep (name "chrono") (req "^0.4.26") (default-features #t) (kind 0)) (crate-dep (name "colored") (req "^2.0") (default-features #t) (kind 0)))) (hash "0pcgv16jk7xrmlaz5xr0bqj1zdg7b0ibfh8aricl16v9j78nnp1y")))

(define-public crate-joz-logger-0.1 (crate (name "joz-logger") (vers "0.1.1") (deps (list (crate-dep (name "chrono") (req "^0.4.26") (default-features #t) (kind 0)) (crate-dep (name "colored") (req "^2.0") (default-features #t) (kind 0)))) (hash "09hmiq71ba5hhkgfcnjy4jd5hcgz7039482kvcn3a8d9dzb1pz9r")))

(define-public crate-joz-logger-0.2 (crate (name "joz-logger") (vers "0.2.0") (deps (list (crate-dep (name "chrono") (req "^0.4.26") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "1c93gccdd2gnrwsjx636fcb58bazn3f501c8pkj4r8sw916q7vdq")))

(define-public crate-joz-logger-0.2 (crate (name "joz-logger") (vers "0.2.1") (deps (list (crate-dep (name "chrono") (req "^0.4.26") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "18ps1f5mb5ffcg43zblz4mcb79y9f4360n334pw5dn1l5vlrcvgl")))

(define-public crate-joz-logger-0.2 (crate (name "joz-logger") (vers "0.2.2") (deps (list (crate-dep (name "chrono") (req "^0.4.26") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "0883qq9xdp287jqxbmfqq04485drj372rvby4873pkz8d863fdl0")))

