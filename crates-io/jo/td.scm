(define-module (crates-io jo td) #:use-module (crates-io))

(define-public crate-jotdown-0.1 (crate (name "jotdown") (vers "0.1.0") (hash "10a2z3w0210vczdjvx0pglq64fk59fsz85ngj9xc1gll0g0dy73r") (features (quote (("suite_bench") ("suite") ("html") ("deterministic") ("default" "html"))))))

(define-public crate-jotdown-0.2 (crate (name "jotdown") (vers "0.2.0") (hash "1647hvkqqrf8jmjrrny0962ff5q7l17n6kciscaxbinzczggfjvq") (features (quote (("suite_bench") ("suite") ("html") ("deterministic") ("default" "html"))))))

(define-public crate-jotdown-0.2 (crate (name "jotdown") (vers "0.2.1") (hash "1wqnk4hn596kkrn1zk99zs1dngkz5iidb00ly11637c9pmhslcp2") (features (quote (("suite_bench") ("suite") ("html") ("deterministic") ("default" "html"))))))

(define-public crate-jotdown-0.3 (crate (name "jotdown") (vers "0.3.0") (hash "09dfrmqlxrag4iy4lmrgzidxr83kdpb5r9rc27f655g6fs8f4cxy") (features (quote (("html") ("deterministic") ("default" "html"))))))

(define-public crate-jotdown-0.3 (crate (name "jotdown") (vers "0.3.1") (hash "0qv0vxpdr92dl0zd820fwz3izk40x2dvm2bg4m2wx745hdqvhf2r") (features (quote (("html") ("deterministic") ("default" "html"))))))

(define-public crate-jotdown-0.3 (crate (name "jotdown") (vers "0.3.2") (hash "15n928lkbfj06gayh49nswcnkxgkr9j33jc2398czlk54qgbihfk") (features (quote (("html") ("deterministic") ("default" "html"))))))

(define-public crate-jotdown-0.4 (crate (name "jotdown") (vers "0.4.0") (hash "00zw3xmqzv56hry56l07xr70mwy3bqa2bbz4kbxrbmghjd3l2j6w") (features (quote (("html") ("deterministic") ("default" "html")))) (rust-version "1.56")))

