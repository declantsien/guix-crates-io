(define-module (crates-io jo jo) #:use-module (crates-io))

(define-public crate-jojo-0.1 (crate (name "jojo") (vers "0.1.0") (hash "1az6gibvs7wwk45ngvkm8z6yjvi2gkmf5a9j37rzbrhpdrki9ggi") (yanked #t)))

(define-public crate-jojo-0.1 (crate (name "jojo") (vers "0.1.1") (deps (list (crate-dep (name "dic") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "0q38bbs8g0cbhmv7sz71i11ay2j1zwwcqs5s3zv6fq0v2kkxv54g") (yanked #t)))

(define-public crate-jojo-0.1 (crate (name "jojo") (vers "0.1.2") (deps (list (crate-dep (name "dic") (req "^0.1.3") (default-features #t) (kind 0)))) (hash "12y2xpcs0kqs2rx4krp0rmnksa6fikywvkpzxqr64ahmfi2zrzr1") (yanked #t)))

(define-public crate-jojo-0.1 (crate (name "jojo") (vers "0.1.3") (deps (list (crate-dep (name "dic") (req "^0.1.3") (default-features #t) (kind 0)))) (hash "1i9b9dah9kwk0gyk283xgam4jixsaj4317f0wdkmpnid2g3q6d8p") (yanked #t)))

(define-public crate-jojo-0.1 (crate (name "jojo") (vers "0.1.4") (deps (list (crate-dep (name "dic") (req "^0.1.4") (default-features #t) (kind 0)))) (hash "1r0gvwra160x9zz93a7mlwqh8fys8j944iw3lhhc7jpkxw7bf2ma") (yanked #t)))

(define-public crate-jojo-0.1 (crate (name "jojo") (vers "0.1.6") (deps (list (crate-dep (name "clap") (req "^2") (default-features #t) (kind 0)) (crate-dep (name "dic") (req "^0.1.4") (default-features #t) (kind 0)) (crate-dep (name "org-tangle") (req "^0.1.0") (default-features #t) (kind 1)) (crate-dep (name "uuid") (req "^0.7") (features (quote ("v4"))) (default-features #t) (kind 0)))) (hash "10z79kcwh6w54kr52pl8gfwcc3ahcqcw3ns19nshw6pci2li541q") (yanked #t)))

(define-public crate-jojo-0.1 (crate (name "jojo") (vers "0.1.7") (deps (list (crate-dep (name "clap") (req "^2") (default-features #t) (kind 0)) (crate-dep (name "dic") (req "^0.1.4") (default-features #t) (kind 0)) (crate-dep (name "linefeed") (req "^0.5") (default-features #t) (kind 0)) (crate-dep (name "org-tangle") (req "^0.1.0") (default-features #t) (kind 1)) (crate-dep (name "uuid") (req "^0.7") (features (quote ("v4"))) (default-features #t) (kind 0)))) (hash "0cg6aya51mwm02c3nwah9gglyg9iw2wm96pcg6cpy9qq8islbpzp") (yanked #t)))

(define-public crate-jojo-0.1 (crate (name "jojo") (vers "0.1.8") (deps (list (crate-dep (name "clap") (req "^2") (default-features #t) (kind 0)) (crate-dep (name "dic") (req "^0.1.4") (default-features #t) (kind 0)) (crate-dep (name "linefeed") (req "^0.5") (default-features #t) (kind 0)) (crate-dep (name "org-tangle") (req "^0.1.0") (default-features #t) (kind 1)) (crate-dep (name "uuid") (req "^0.7") (features (quote ("v4"))) (default-features #t) (kind 0)))) (hash "00mvlalaqb678c2b6aqpnzzqgipblvy9fs6dbvnwp2hjrgzrr103") (yanked #t)))

(define-public crate-jojo-0.1 (crate (name "jojo") (vers "0.1.9") (deps (list (crate-dep (name "clap") (req "^2") (default-features #t) (kind 0)) (crate-dep (name "dic") (req "^0.1.5") (default-features #t) (kind 0)) (crate-dep (name "linefeed") (req "^0.5") (default-features #t) (kind 0)) (crate-dep (name "org-tangle") (req "^0.1.0") (default-features #t) (kind 1)) (crate-dep (name "uuid") (req "^0.7") (features (quote ("v4"))) (default-features #t) (kind 0)))) (hash "00crqzrr0bp62mgwn1fjllg9abpb5rn6zpj6rkfd6mjsbsj3r255") (yanked #t)))

(define-public crate-jojo-0.1 (crate (name "jojo") (vers "0.1.10") (deps (list (crate-dep (name "clap") (req "^2") (default-features #t) (kind 0)) (crate-dep (name "dic") (req "^0.1.5") (default-features #t) (kind 0)) (crate-dep (name "linefeed") (req "^0.5") (default-features #t) (kind 0)) (crate-dep (name "org-tangle") (req "^0.1") (default-features #t) (kind 1)) (crate-dep (name "uuid") (req "^0.7") (features (quote ("v4"))) (default-features #t) (kind 0)))) (hash "014hz4xxbxi6mz2rbwmis653qdgs22yf8lqzw7lj01m9dgj4809c") (yanked #t)))

(define-public crate-jojo-0.1 (crate (name "jojo") (vers "0.1.11") (deps (list (crate-dep (name "clap") (req "^2") (default-features #t) (kind 0)) (crate-dep (name "dic") (req "^0.1.5") (default-features #t) (kind 0)) (crate-dep (name "linefeed") (req "^0.5") (default-features #t) (kind 0)) (crate-dep (name "org-tangle") (req "^0.1") (default-features #t) (kind 1)) (crate-dep (name "uuid") (req "^0.7") (features (quote ("v4"))) (default-features #t) (kind 0)))) (hash "0vhds2zxvw7p4cffy3c4dy4p413hp986mmv47b8y39jsaa42rsz9") (yanked #t)))

(define-public crate-jojo-0.1 (crate (name "jojo") (vers "0.1.12") (deps (list (crate-dep (name "clap") (req "^2") (default-features #t) (kind 0)) (crate-dep (name "dic") (req "^0.1.5") (default-features #t) (kind 0)) (crate-dep (name "linefeed") (req "^0.5") (default-features #t) (kind 0)) (crate-dep (name "org-tangle") (req "^0.1") (default-features #t) (kind 1)) (crate-dep (name "uuid") (req "^0.7") (features (quote ("v4"))) (default-features #t) (kind 0)))) (hash "1ygfwyhyr5l59nszfyfv8vjk3b6w0zy8nmqa676f8x1v8ji2dsis") (yanked #t)))

(define-public crate-jojo-0.1 (crate (name "jojo") (vers "0.1.13") (deps (list (crate-dep (name "clap") (req "^2") (default-features #t) (kind 0)) (crate-dep (name "jojo-core") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "linefeed") (req "^0.5") (default-features #t) (kind 0)) (crate-dep (name "org-tangle") (req "^0.1") (default-features #t) (kind 1)))) (hash "0r38s0zjg3q4d8xvi0xglb7d9khy3s7z8kspz5hgaj7gygxz3h88") (yanked #t)))

(define-public crate-jojo-0.1 (crate (name "jojo") (vers "0.1.14") (deps (list (crate-dep (name "clap") (req "^2") (default-features #t) (kind 0)) (crate-dep (name "jojo-core") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "linefeed") (req "^0.5") (default-features #t) (kind 0)) (crate-dep (name "org-tangle") (req "^0.1") (default-features #t) (kind 1)))) (hash "1y5akc30xjbslckhp6yng0b3n1nhhpi9l4305l0w9x0hnwab382c") (yanked #t)))

(define-public crate-jojo-core-0.1 (crate (name "jojo-core") (vers "0.1.0") (deps (list (crate-dep (name "dic") (req "^0.1.5") (default-features #t) (kind 0)) (crate-dep (name "org-tangle") (req "^0.1") (default-features #t) (kind 1)) (crate-dep (name "uuid") (req "^0.7") (features (quote ("v4"))) (default-features #t) (kind 0)))) (hash "13sl6lz497x3swj2bjidi2zx3pxyxnar8xkqi9hs2bvhg38jf2rb") (yanked #t)))

(define-public crate-jojo-web-0.1 (crate (name "jojo-web") (vers "0.1.0") (deps (list (crate-dep (name "jojo-core") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "org-tangle") (req "^0.1") (default-features #t) (kind 1)))) (hash "0z7cl0bb580mqksmmay1bz93y2g1h8ly6py4kd2x0hmid786hzas") (yanked #t)))

(define-public crate-jojorss-0.1 (crate (name "jojorss") (vers "0.1.0") (deps (list (crate-dep (name "dirs-next") (req "^2") (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.11") (features (quote ("blocking"))) (default-features #t) (kind 0)) (crate-dep (name "rss") (req "^1") (default-features #t) (kind 0)))) (hash "01phj1billxichmnxzs182qzp1824531x7a539hij3bj3gkmaprl")))

(define-public crate-jojorss-0.1 (crate (name "jojorss") (vers "0.1.1") (deps (list (crate-dep (name "dirs-next") (req "^2") (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.11") (features (quote ("blocking"))) (default-features #t) (kind 0)) (crate-dep (name "rss") (req "^1") (default-features #t) (kind 0)))) (hash "0qmy9za23cq8cy8zzmxq47s62j6jc17ly8wppbh39sm5rx4bsw8n")))

(define-public crate-jojorss-0.1 (crate (name "jojorss") (vers "0.1.2") (deps (list (crate-dep (name "dirs-next") (req "^2.0") (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.11") (features (quote ("blocking"))) (default-features #t) (kind 0)) (crate-dep (name "rss") (req "^1.10") (default-features #t) (kind 0)))) (hash "0wq1qfqpbdgws0klwzmgzl17yghpj8yra3xkl28wc4ayswg2lf0x")))

(define-public crate-jojorss-0.1 (crate (name "jojorss") (vers "0.1.3") (deps (list (crate-dep (name "dirs-next") (req "^2.0") (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.11") (features (quote ("blocking"))) (default-features #t) (kind 0)) (crate-dep (name "rss") (req "^1.10") (default-features #t) (kind 0)))) (hash "05rmv4pbrmp7mcnmvlcq2jkmr6kjxk26zl94zfak7pqysc2hdbvy")))

