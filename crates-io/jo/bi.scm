(define-module (crates-io jo bi) #:use-module (crates-io))

(define-public crate-jobicolet-0.1 (crate (name "jobicolet") (vers "0.1.0") (deps (list (crate-dep (name "wee_alloc") (req "^0.4.5") (default-features #t) (kind 0)))) (hash "1d31j2hrc50pq0h8zn6zrpmmp6fxf0jnvzss8ra74yrjlk69p8i2")))

(define-public crate-jobicolet-0.1 (crate (name "jobicolet") (vers "0.1.1") (deps (list (crate-dep (name "wee_alloc") (req "^0.4.5") (default-features #t) (kind 0)))) (hash "1jxapda2m38jzxnybyiiahvhxdx32cgz78hawiraqlq4a2bkw82w")))

(define-public crate-jobicolet-0.1 (crate (name "jobicolet") (vers "0.1.2") (deps (list (crate-dep (name "wee_alloc") (req "^0.4.5") (default-features #t) (kind 0)))) (hash "106p02ybxjsky1qf7dprvfdikfckml9mrzqr5lw5g8j7ywiwfqi9")))

