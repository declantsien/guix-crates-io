(define-module (crates-io jo ck) #:use-module (crates-io))

(define-public crate-jockey-0.0.0 (crate (name "jockey") (vers "0.0.0") (hash "17r9sasyp383rb0z2ag2ywkxaksq4kig5by36mi356yh0d19gjjq") (yanked #t)))

(define-public crate-jockey-0.1 (crate (name "jockey") (vers "0.1.0") (hash "1v3xnazbhz59gdvkmfrdgmr68js3z164s6qhsd6h2avrzx20pmhh")))

(define-public crate-jockey-0.2 (crate (name "jockey") (vers "0.2.0") (hash "11qc6g0kpkcn5vkffyfil1wf1z8axv1p0imzd7ws9a5bp7nyizfq")))

(define-public crate-jockey-0.2 (crate (name "jockey") (vers "0.2.1") (hash "130a3awrxjxn8vfhrmy97d7c14gglil5fk1prn8xgfg80iyh8jm5")))

(define-public crate-jockey-0.3 (crate (name "jockey") (vers "0.3.0") (deps (list (crate-dep (name "jockey_derive") (req "^0") (default-features #t) (kind 2)))) (hash "0ri7mbjscna1nqi3z5yny8m1cxx5sk533i23pwfr7n0k4sx0fb4f")))

(define-public crate-jockey_derive-0.0.0 (crate (name "jockey_derive") (vers "0.0.0") (hash "0206vs3y7v2bnhrn4m6ih0aylav9bn9dsfm0ks2ckvdym2nrsars") (yanked #t)))

(define-public crate-jockey_derive-0.1 (crate (name "jockey_derive") (vers "0.1.0") (deps (list (crate-dep (name "proc-macro2") (req "^0.4.24") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^0.6.10") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^0.15.22") (default-features #t) (kind 0)))) (hash "0kkm07ahpfdzndxnd4gm4kv1hr94i65nk4043bw1r066caxzjrc5")))

(define-public crate-jockey_derive-0.2 (crate (name "jockey_derive") (vers "0.2.0") (deps (list (crate-dep (name "proc-macro2") (req "^0.4.24") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^0.6.10") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^0.15.22") (default-features #t) (kind 0)))) (hash "1zrcjgknpzm5sr6nkqksxgwsk85cc2jcc8amxxbfcjzjad3kpza1")))

(define-public crate-jockey_derive-0.2 (crate (name "jockey_derive") (vers "0.2.1") (deps (list (crate-dep (name "proc-macro2") (req "^0.4.24") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^0.6.10") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^0.15.22") (default-features #t) (kind 0)))) (hash "1brbcmfbl7jmr8ycmci88q963hasz5a8bahp0dza9322z3p0vqmd")))

(define-public crate-jockey_derive-0.3 (crate (name "jockey_derive") (vers "0.3.0") (deps (list (crate-dep (name "jockey") (req "^0") (default-features #t) (kind 2)) (crate-dep (name "proc-macro2") (req "^0.4.24") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^0.6.10") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^0.15.22") (default-features #t) (kind 0)))) (hash "1z3fi0fczvfg28318ac25llg9lsk63cv4kqq854hy4n7hmvkk83d")))

