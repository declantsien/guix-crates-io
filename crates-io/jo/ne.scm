(define-module (crates-io jo ne) #:use-module (crates-io))

(define-public crate-jones-0.1 (crate (name "jones") (vers "0.1.0") (deps (list (crate-dep (name "ansi_term") (req "^0.12.1") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.5.4") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.3.13") (default-features #t) (kind 0)))) (hash "00gl5by12wzj5gxz9lcms7sd6gfq171gp9c6hdps86zs3vgbp4wa")))

(define-public crate-jones-0.1 (crate (name "jones") (vers "0.1.1") (deps (list (crate-dep (name "ansi_term") (req "^0.12.1") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.5.4") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.3.13") (default-features #t) (kind 0)))) (hash "0f1j6pic3q2s49rpi7kz6g1p3imijaj3jp7vldgdzc5rrkqsn4r9")))

(define-public crate-jones-0.1 (crate (name "jones") (vers "0.1.3") (deps (list (crate-dep (name "ansi_term") (req "^0.12.1") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.5.4") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.3.13") (default-features #t) (kind 0)))) (hash "0crcsklwncywj8zfyi3g27szajnncmqsysz26b127scn5wb0bivf")))

(define-public crate-jones-0.1 (crate (name "jones") (vers "0.1.4") (deps (list (crate-dep (name "ansi_term") (req "^0.12.1") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.5.4") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.3.13") (default-features #t) (kind 0)))) (hash "00bc1qbkbw9cl17v1q1r7j75gry1l22ph73hbgggnvpgiz7iqch4")))

(define-public crate-jones-0.2 (crate (name "jones") (vers "0.2.0") (deps (list (crate-dep (name "ansi_term") (req "^0.12.1") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.5.5") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.3.13") (default-features #t) (kind 0)) (crate-dep (name "walkdir") (req "^2.3.1") (default-features #t) (kind 0)))) (hash "0ml2rg56l1pg7wivzcigrmalny3vi3b4x76v28m7yvzkcbqzhc57")))

