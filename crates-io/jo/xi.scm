(define-module (crates-io jo xi) #:use-module (crates-io))

(define-public crate-joxide-0.0.1 (crate (name "joxide") (vers "0.0.1") (deps (list (crate-dep (name "argh") (req "^0.1.9") (default-features #t) (kind 0)))) (hash "1960alwa4griyyqwajkmiv1g2wf7kqvlzy7yinbb3x4id7qbw025")))

(define-public crate-joxide-0.0.2 (crate (name "joxide") (vers "0.0.2") (deps (list (crate-dep (name "argh") (req "^0.1.9") (default-features #t) (kind 0)))) (hash "120rkl01a7wyb1rnlmwznxdwhq5g1c7mqfq83rjj11ql1d4r85zw")))

(define-public crate-joxide-0.0.3 (crate (name "joxide") (vers "0.0.3") (deps (list (crate-dep (name "argh") (req "^0.1.9") (default-features #t) (kind 0)))) (hash "16wldi2gp1fyf6dkflddp07lb3qgb29hsks0zvkhqhicxkp1nrln")))

(define-public crate-joxide-0.1 (crate (name "joxide") (vers "0.1.3") (deps (list (crate-dep (name "argh") (req "^0.1.9") (default-features #t) (kind 0)) (crate-dep (name "glob") (req "^0.3.1") (default-features #t) (kind 0)))) (hash "1hxq5bbb2z36kizx88wx1zr19nag68s0c1wzd96mzinpl8cyrq3d")))

