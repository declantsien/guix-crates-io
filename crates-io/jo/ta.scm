(define-module (crates-io jo ta) #:use-module (crates-io))

(define-public crate-jotai-0.0.0 (crate (name "jotai") (vers "0.0.0") (hash "0v7336f0a2vqyv8ic6hw46pygg6r7fihyykmhsjhznp3lvkrdm7k")))

(define-public crate-jotaro-0.1 (crate (name "jotaro") (vers "0.1.0") (deps (list (crate-dep (name "jotaro-sys") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0") (default-features #t) (kind 0)))) (hash "0w8zjkw1wh4c6z1jpmqy7d4fqc8xiw59l6q424pmnwnm0fppn4y0")))

(define-public crate-jotaro-sys-0.1 (crate (name "jotaro-sys") (vers "0.1.0") (deps (list (crate-dep (name "bindgen") (req "^0.59") (default-features #t) (kind 1)) (crate-dep (name "cmake") (req "^0.1") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "pkg-config") (req "^0.3") (default-features #t) (kind 1)))) (hash "0gsw8dpgnvdiy22qr428qw7k2qa6rjd6b4f3v0mjda0w86wspi1q")))

(define-public crate-jotaro-sys-0.1 (crate (name "jotaro-sys") (vers "0.1.1") (deps (list (crate-dep (name "bindgen") (req "^0.59") (default-features #t) (kind 1)) (crate-dep (name "cmake") (req "^0.1") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "pkg-config") (req "^0.3") (default-features #t) (kind 1)))) (hash "0xfiqzp5m9cbspc49f7kijx8bhiw4l93x5mkxzlbm2njl7r9zcpw")))

(define-public crate-jotaro-sys-0.1 (crate (name "jotaro-sys") (vers "0.1.2") (deps (list (crate-dep (name "bindgen") (req "^0.59") (default-features #t) (kind 1)) (crate-dep (name "cmake") (req "^0.1") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "pkg-config") (req "^0.3") (default-features #t) (kind 1)))) (hash "0zbrpsw9636fsdxr30iyw2g0jn3srala45wsdl085jn6v2fgz5c4")))

(define-public crate-jotaro-sys-0.1 (crate (name "jotaro-sys") (vers "0.1.3") (deps (list (crate-dep (name "bindgen") (req "^0.59") (default-features #t) (kind 1)) (crate-dep (name "cmake") (req "^0.1") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "openssl-sys") (req "^0.9.99") (features (quote ("vendored"))) (default-features #t) (kind 0)))) (hash "04wqmi3gkwvznxk3385vcl2v9gkbmc6sd23y3xzr7w8hsn5h4ar6")))

