(define-module (crates-io jo ts) #:use-module (crates-io))

(define-public crate-jotspot-0.1 (crate (name "jotspot") (vers "0.1.0") (deps (list (crate-dep (name "cursive") (req "^0.20.0") (default-features #t) (kind 0)))) (hash "1liz8bk8b5m4y0d2qdlgcf8dndknk1f7d5das8vh8v0zlqnfvrq9")))

