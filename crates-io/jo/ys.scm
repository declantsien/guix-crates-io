(define-module (crates-io jo ys) #:use-module (crates-io))

(define-public crate-joysale-0.1 (crate (name "joysale") (vers "0.1.0") (hash "0hn9m2a82iyfhh137ma8d69jr80i6v5iy3vhbpmnkfxni27hs8q6")))

(define-public crate-joystick-0.0.0 (crate (name "joystick") (vers "0.0.0") (hash "10rmci12crwfs1927i6a6h0p4aysxrcmbxb0qfjn81w5qiyimdf0")))

