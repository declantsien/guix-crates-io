(define-module (crates-io jo es) #:use-module (crates-io))

(define-public crate-joestar-0.1 (crate (name "joestar") (vers "0.1.0") (deps (list (crate-dep (name "log") (req "^0.4.17") (default-features #t) (kind 0)) (crate-dep (name "wry") (req "^0.27.0") (default-features #t) (kind 0)))) (hash "04s8s90nalb3g3m215xswl2a78nhzv8qidhz2g42bqh93vdyqp77") (yanked #t)))

(define-public crate-joestar-0.1 (crate (name "joestar") (vers "0.1.1") (deps (list (crate-dep (name "log") (req "^0.4.17") (default-features #t) (kind 0)) (crate-dep (name "wry") (req "^0.27.0") (default-features #t) (kind 0)))) (hash "11a68nfpq2scp7f9bj4zpiqsicq91zw0ljpwj60h9x5laxmy524c") (yanked #t)))

(define-public crate-joestar-0.1 (crate (name "joestar") (vers "0.1.2") (deps (list (crate-dep (name "log") (req "^0.4.17") (default-features #t) (kind 0)) (crate-dep (name "wry") (req "^0.27.0") (default-features #t) (kind 0)))) (hash "0g0i820h65pr1krzh4rpmlyiz8ia0ikr4l9xlv41byif5l1g7921") (yanked #t)))

(define-public crate-joestar-html-0.1 (crate (name "joestar-html") (vers "0.1.0") (deps (list (crate-dep (name "joestar") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "0h5mxbhy7f5pdz5k2izzw1dbsb0dlr1x8gcbh1q3xpwaqi51cq60") (yanked #t)))

(define-public crate-joestar-html-0.1 (crate (name "joestar-html") (vers "0.1.1") (deps (list (crate-dep (name "joestar") (req "^0.1.1") (default-features #t) (kind 0)))) (hash "1p7dbg1yg2vyqx6pfac52pbd5avgxcihn4rpm0vl1msblzjpqy7d") (yanked #t)))

(define-public crate-joestar-html-0.1 (crate (name "joestar-html") (vers "0.1.2") (deps (list (crate-dep (name "joestar") (req "^0.1.2") (default-features #t) (kind 0)))) (hash "1sqhbiziizj8pl8f6jj2zya1p586gr184y2sy7lb965rlvg957g8") (yanked #t)))

(define-public crate-joestar-html-0.1 (crate (name "joestar-html") (vers "0.1.3") (deps (list (crate-dep (name "joestar") (req "^0.1.2") (default-features #t) (kind 0)))) (hash "0svk075mnyravbn5bghhg1j9v8j0cr40sb2jny6wylznyp3skvxd") (yanked #t)))

