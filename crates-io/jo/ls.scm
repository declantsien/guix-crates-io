(define-module (crates-io jo ls) #:use-module (crates-io))

(define-public crate-jolse-0.1 (crate (name "jolse") (vers "0.1.4") (deps (list (crate-dep (name "zstd") (req "^0.13.1") (default-features #t) (kind 0)))) (hash "1pfn59799vgg5l5jp8rdv7s1s8437y6zhcdsd5a4547kw6f4w8sw") (yanked #t) (rust-version "1.77.2")))

(define-public crate-jolse-0.1 (crate (name "jolse") (vers "0.1.0") (deps (list (crate-dep (name "zstd") (req "^0.13.1") (default-features #t) (kind 0)))) (hash "0s24irbvpm6lj2bf3caq85427m4zd887dk8w422n19w91psjkpdx") (rust-version "1.77.2")))

