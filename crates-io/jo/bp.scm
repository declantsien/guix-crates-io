(define-module (crates-io jo bp) #:use-module (crates-io))

(define-public crate-jobpool-0.1 (crate (name "jobpool") (vers "0.1.0") (hash "0161d9h23n16c3khzmxn3an06xpb6jm965vrddvma3y7abgfz56m")))

(define-public crate-jobpool-0.1 (crate (name "jobpool") (vers "0.1.1") (hash "0flvv90m47hfn7lwqhplqc4sri6fb5w1ilf2yhdz3f11gg663igl")))

(define-public crate-jobpool-0.1 (crate (name "jobpool") (vers "0.1.2") (hash "1gf64qvfmncy94x5cjs5jyhk2d687v7f7m7c9nj9gfx90naab8cz")))

(define-public crate-jobpool-0.1 (crate (name "jobpool") (vers "0.1.3") (hash "14i651819i01p28042iv5mf0m9v82n8qr1ma4nk1prjy6n5d096j")))

(define-public crate-jobpool-0.1 (crate (name "jobpool") (vers "0.1.4") (hash "1fxw1zlc5zvzz0j3wxbi2hgs7w6qy0df8l0wmj8ldraspiahqbm1")))

(define-public crate-jobpool-0.1 (crate (name "jobpool") (vers "0.1.5") (hash "00496h63hyjqz21r80sxcim62rkwd4rr6xh9w9q4zw5hys0l5hr8")))

(define-public crate-jobpool-0.1 (crate (name "jobpool") (vers "0.1.6") (hash "149vbvxcxghq596fskr7hvyd2i77bad9a73rx62r5f1lh8rvd1xz")))

(define-public crate-jobpool-0.1 (crate (name "jobpool") (vers "0.1.7") (hash "1y33wk4crj22rjh9dw39p0gy870myjqj21l0r6daxknxwm2ilvbr")))

(define-public crate-jobpool-0.1 (crate (name "jobpool") (vers "0.1.8") (hash "0pd5rgnvpfv14sy363phim8w4wa5mabjgl9w0xnbg1api55kz4lz")))

(define-public crate-jobpool-0.1 (crate (name "jobpool") (vers "0.1.9") (hash "14vihfy3f66g15wcn0v4p4zi0isj4m2327wra04388cvgrpigxz0")))

(define-public crate-jobpool-0.1 (crate (name "jobpool") (vers "0.1.10") (hash "0bfnasd9iajw0jlfag06i1wymvsq4sqw50vg026yh30n9s0svhjc")))

(define-public crate-jobpool-0.2 (crate (name "jobpool") (vers "0.2.0") (hash "0kl27yb95xdp49f7mgrg81dpv3wazvv1mb9dhkl2vx1x9siz2q5f")))

(define-public crate-jobpool-0.2 (crate (name "jobpool") (vers "0.2.1") (hash "00d1lhhblhgqnmzwizdwydzg2kswvqf643lvgxxmdc7hhyzanank")))

(define-public crate-jobpool-0.2 (crate (name "jobpool") (vers "0.2.2") (hash "1apj2kr0srnv38ip0qzhqzsa7n3id9l9bzch4dqjd48455slfbhv")))

(define-public crate-jobpool-0.2 (crate (name "jobpool") (vers "0.2.3") (hash "0md0c1r48rwk364v9vr8ifrdjn0hy33fvsz8ijbdv5v32dxgd20g")))

(define-public crate-jobpool-0.2 (crate (name "jobpool") (vers "0.2.4") (hash "12jbmgrhhl4ql9krragrbirh8rjc0vlxzk4nfpn441dqz85n1lhw")))

(define-public crate-jobpool-0.2 (crate (name "jobpool") (vers "0.2.5") (hash "1h68yf6m3l226z341v095dinms39hd5dlazr1s2lq9ghyhq0zs88")))

(define-public crate-jobpool-0.3 (crate (name "jobpool") (vers "0.3.0") (hash "0csmwspmn8wll0ppm9cpf0v4a9q4mc97gfvaqjl9scjxgf56lcs5")))

(define-public crate-jobpool-0.3 (crate (name "jobpool") (vers "0.3.1") (hash "1xlq3fyc8p5z3yqiwn9jj9lmg2bdgqndcwihxgv3nrc94m9jgs19")))

(define-public crate-jobpool-0.3 (crate (name "jobpool") (vers "0.3.2") (hash "0fzjfp3sfbhy61q9bnsjc85h8g91vzfyqjlxz45m4n1rz88xy147")))

(define-public crate-jobpool-0.3 (crate (name "jobpool") (vers "0.3.3") (hash "1ivw3gprw2d014wfnchk1n9q9rwr5r63jk9pwqgkkax84q79p5cc")))

(define-public crate-jobpool-0.3 (crate (name "jobpool") (vers "0.3.4") (hash "1sd5qx1hcbzhc6yvxic94p0azdb0g05k4sxhgfn22jpzc1hryzva")))

(define-public crate-jobpool-0.3 (crate (name "jobpool") (vers "0.3.5") (hash "1m82qr87cldvz4vi8d12ahkmladd5lvngkh4yclgg8x1qni07h29")))

(define-public crate-jobpool-0.3 (crate (name "jobpool") (vers "0.3.6") (hash "035d24nqb7k82phva28gfm1vwhmbxzp5wa52fvwlc11m9dhfzjzz")))

(define-public crate-jobpool-0.3 (crate (name "jobpool") (vers "0.3.7") (hash "1afa2sxrs0p2gl582w806pb262ycn1ns79slx548mbyg7g9zg8b8")))

(define-public crate-jobpool-0.3 (crate (name "jobpool") (vers "0.3.8") (hash "0mcq50d8bqkjiw0rza2hjb3gyakvsa64s41wgjmf1k0s4x7bk8s1")))

