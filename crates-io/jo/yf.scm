(define-module (crates-io jo yf) #:use-module (crates-io))

(define-public crate-joyful_minigrep-0.1 (crate (name "joyful_minigrep") (vers "0.1.0") (deps (list (crate-dep (name "colored") (req "^2") (default-features #t) (kind 0)) (crate-dep (name "criterion") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "term") (req "^0.7.0") (default-features #t) (kind 0)))) (hash "1mgpcwxmj44kcxx5f1p894dziwhdn5rp8zdq32wxf3wddxvn8jg6")))

(define-public crate-joyful_minigrep-0.1 (crate (name "joyful_minigrep") (vers "0.1.1") (deps (list (crate-dep (name "colored") (req "^2") (default-features #t) (kind 0)) (crate-dep (name "criterion") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "term") (req "^0.7.0") (default-features #t) (kind 0)))) (hash "0kcirq9vv49fh5ppwx5a6g039xgilpcprydk7ii3vzp7h25agij5")))

(define-public crate-joyful_minigrep-0.1 (crate (name "joyful_minigrep") (vers "0.1.2") (deps (list (crate-dep (name "colored") (req "^2") (default-features #t) (kind 0)) (crate-dep (name "criterion") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "term") (req "^0.7.0") (default-features #t) (kind 0)))) (hash "0gsphij65yppl28lwswzg3j7hmr7vn50idvk01z7v7055a8zjzm4")))

(define-public crate-joyful_minigrep-0.1 (crate (name "joyful_minigrep") (vers "0.1.3") (deps (list (crate-dep (name "colored") (req "^2") (default-features #t) (kind 0)) (crate-dep (name "criterion") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "term") (req "^0.7.0") (default-features #t) (kind 0)))) (hash "0xw0qd7h8s48p1ig85srdv1cn0lajk8hrrrnibnd1ddv0jxiz561")))

