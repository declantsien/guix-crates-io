(define-module (crates-io jo lt) #:use-module (crates-io))

(define-public crate-jolt-0.1 (crate (name "jolt") (vers "0.1.0") (hash "09p22cr0dr7474p8m71bjpb2wrq7hn98w55ad8pn1jmlww65r7d6")))

(define-public crate-jolt-physics-0.1 (crate (name "jolt-physics") (vers "0.1.0") (deps (list (crate-dep (name "jolt-sys") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "0bl59x5rndf3r331dmppy78c2smxrr3r3smxbr1v8id9yg8b8i66") (yanked #t)))

(define-public crate-jolt-physics-0.1 (crate (name "jolt-physics") (vers "0.1.1") (deps (list (crate-dep (name "jolt-sys") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "1569jwwq6wa8hr1ys4bindy1d8hr0m3qxqsv3gmggm95wc3d4ny8") (yanked #t)))

(define-public crate-jolt-physics-0.1 (crate (name "jolt-physics") (vers "0.1.2") (deps (list (crate-dep (name "jolt-sys") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "0klsdnrmar5c8p9z4xfcnaca3kkv3cnckpzkr00w0my4y43ldw45") (yanked #t)))

(define-public crate-jolt-physics-0.1 (crate (name "jolt-physics") (vers "0.1.3") (deps (list (crate-dep (name "jolt-sys") (req "^0.1.2") (default-features #t) (kind 0)))) (hash "04gi64kswyfx291x3djcdm3kqgla1z6yvql5468zd61n2mp8r069") (yanked #t)))

(define-public crate-jolt-physics-0.1 (crate (name "jolt-physics") (vers "0.1.5") (deps (list (crate-dep (name "jolt-sys") (req "^0.1.5") (default-features #t) (kind 0)))) (hash "0gr8cdlm8dcng5nx1fannpqswrdlmiar8fhs2jlf3clcjwz8h86r")))

(define-public crate-jolt-physics-0.1 (crate (name "jolt-physics") (vers "0.1.4") (deps (list (crate-dep (name "jolt-sys") (req "^0.1.4") (default-features #t) (kind 0)))) (hash "1yrk55x9sszwqh7y66k7q96fpfh8gvv2g4y555ss5h456hiblq49")))

(define-public crate-jolt-sys-0.1 (crate (name "jolt-sys") (vers "0.1.0") (hash "1p60y5ch4b2mmymdyi46zv8pk6p7qpscd2xhh9hzlbjp68lx5vgx") (yanked #t)))

(define-public crate-jolt-sys-0.1 (crate (name "jolt-sys") (vers "0.1.2") (deps (list (crate-dep (name "bindgen") (req "^0.59.2") (default-features #t) (kind 1)) (crate-dep (name "cmake") (req "^0.1.48") (default-features #t) (kind 1)) (crate-dep (name "normpath") (req "^0.3") (default-features #t) (kind 1)))) (hash "18mdk1pqw7gdq1b96mm3d7ipfhmfk4yrz6f2n8r2xiwwvbgqbd9n") (features (quote (("default")))) (yanked #t) (links "jolt-wrapper")))

(define-public crate-jolt-sys-0.1 (crate (name "jolt-sys") (vers "0.1.5") (deps (list (crate-dep (name "bindgen") (req "^0.59.2") (default-features #t) (kind 1)) (crate-dep (name "cmake") (req "^0.1.48") (default-features #t) (kind 1)) (crate-dep (name "normpath") (req "^0.3") (default-features #t) (kind 1)))) (hash "0z2bp1b8x9467zk5kxkfnvv8sbwq5q4ikh07d06sdl5j7g2pzasz") (features (quote (("default")))) (links "jolt-wrapper")))

(define-public crate-joltc-sys-0.1 (crate (name "joltc-sys") (vers "0.1.0+Jolt-5.0.0") (deps (list (crate-dep (name "anyhow") (req "^1.0.69") (default-features #t) (kind 1)) (crate-dep (name "bindgen") (req "^0.69.1") (default-features #t) (kind 1)) (crate-dep (name "cc") (req "^1.0.94") (features (quote ("parallel"))) (default-features #t) (kind 1)) (crate-dep (name "cmake") (req "^0.1.50") (default-features #t) (kind 1)) (crate-dep (name "walkdir") (req "^2.5.0") (default-features #t) (kind 1)))) (hash "0g10kddlksjkfyqzqxacn4722pc510chqzjln586jh9mvxvx5zgv") (features (quote (("object-layer-u32") ("double-precision") ("default"))))))

(define-public crate-joltc-sys-0.1 (crate (name "joltc-sys") (vers "0.1.1+Jolt-5.0.0") (deps (list (crate-dep (name "anyhow") (req "^1.0.69") (default-features #t) (kind 1)) (crate-dep (name "bindgen") (req "^0.69.1") (default-features #t) (kind 1)) (crate-dep (name "cc") (req "^1.0.94") (features (quote ("parallel"))) (default-features #t) (kind 1)) (crate-dep (name "cmake") (req "^0.1.50") (default-features #t) (kind 1)) (crate-dep (name "walkdir") (req "^2.5.0") (default-features #t) (kind 1)))) (hash "1a3xpl1jg0grgx8bb6vahbdai6zriv2lcf68lr0x0ps3mnawch34") (features (quote (("object-layer-u32") ("double-precision") ("default"))))))

(define-public crate-joltc-sys-0.2 (crate (name "joltc-sys") (vers "0.2.0+Jolt-5.0.0") (deps (list (crate-dep (name "anyhow") (req "^1.0.69") (default-features #t) (kind 1)) (crate-dep (name "bindgen") (req "^0.69.1") (default-features #t) (kind 1)) (crate-dep (name "cc") (req "^1.0.94") (features (quote ("parallel"))) (default-features #t) (kind 1)) (crate-dep (name "cmake") (req "^0.1.50") (default-features #t) (kind 1)) (crate-dep (name "walkdir") (req "^2.5.0") (default-features #t) (kind 1)))) (hash "0h0ri02lkdhibk6lba84l54hw5nmin6fvap37w2z203a6gcfzqvi") (features (quote (("object-layer-u32") ("double-precision") ("default"))))))

(define-public crate-joltc-sys-0.3 (crate (name "joltc-sys") (vers "0.3.0+Jolt-5.0.0") (deps (list (crate-dep (name "anyhow") (req "^1.0.69") (default-features #t) (kind 1)) (crate-dep (name "bindgen") (req "^0.69.1") (default-features #t) (kind 1)) (crate-dep (name "cc") (req "^1.0.94") (features (quote ("parallel"))) (default-features #t) (kind 1)) (crate-dep (name "cmake") (req "^0.1.50") (default-features #t) (kind 1)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 2)) (crate-dep (name "walkdir") (req "^2.5.0") (default-features #t) (kind 1)))) (hash "10c6mhv0jnr1bpjd8ikpl45b7fld9jq07m0p77iblvmv6b9k3wii") (features (quote (("object-layer-u32") ("double-precision") ("default"))))))

(define-public crate-joltc-sys-0.3 (crate (name "joltc-sys") (vers "0.3.1+Jolt-5.0.0") (deps (list (crate-dep (name "anyhow") (req "^1.0.69") (default-features #t) (kind 1)) (crate-dep (name "bindgen") (req "^0.69.1") (default-features #t) (kind 1)) (crate-dep (name "cc") (req "^1.0.94") (features (quote ("parallel"))) (default-features #t) (kind 1)) (crate-dep (name "cmake") (req "^0.1.50") (default-features #t) (kind 1)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 2)) (crate-dep (name "walkdir") (req "^2.5.0") (default-features #t) (kind 1)))) (hash "15xjqj1ialdvwp9lg9ifznafijp8fsk25z47wqa842hwwma26vw4") (features (quote (("object-layer-u32") ("double-precision") ("default"))))))

