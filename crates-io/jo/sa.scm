(define-module (crates-io jo sa) #:use-module (crates-io))

(define-public crate-josa-0.1 (crate (name "josa") (vers "0.1.0") (deps (list (crate-dep (name "hangul") (req "^0.1.2") (default-features #t) (kind 0)))) (hash "1qn9svf1bshwh7h8g27prbw1d0q74dmn7h1axs9808c4pp0994sw")))

(define-public crate-josa-0.1 (crate (name "josa") (vers "0.1.1") (deps (list (crate-dep (name "hangul") (req "^0.1.3") (default-features #t) (kind 0)))) (hash "16n243avpsk7mylra8vfm49amphm7m71cjfvbi3fg55cy49myq4b")))

(define-public crate-josa-0.1 (crate (name "josa") (vers "0.1.2") (deps (list (crate-dep (name "hangul") (req "^0.1.3") (default-features #t) (kind 0)))) (hash "0jfr3fq5x51lghd7q7x7m5nsnalsvcnsqjcvfcld7a11j9g115wb")))

