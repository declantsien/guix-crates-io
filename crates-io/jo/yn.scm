(define-module (crates-io jo yn) #:use-module (crates-io))

(define-public crate-joyn-0.1 (crate (name "joyn") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^4") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "assert_cmd") (req "^2") (default-features #t) (kind 2)) (crate-dep (name "predicates") (req "^2") (default-features #t) (kind 2)))) (hash "00h49g3wbj8bb1ga5yh4hacixksxynarc1wzpnvshs6g905z6k23")))

(define-public crate-joyn-0.1 (crate (name "joyn") (vers "0.1.10") (deps (list (crate-dep (name "clap") (req "^4") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "assert_cmd") (req "^2") (default-features #t) (kind 2)) (crate-dep (name "predicates") (req "^2") (default-features #t) (kind 2)))) (hash "0i5k5xgi1s5m63wq6wjsr2f7z62spsbpbh7wpvjy0milxks9pdns") (yanked #t)))

(define-public crate-joyn-0.1 (crate (name "joyn") (vers "0.1.1") (deps (list (crate-dep (name "clap") (req "^4") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "assert_cmd") (req "^2") (default-features #t) (kind 2)) (crate-dep (name "predicates") (req "^2") (default-features #t) (kind 2)))) (hash "0jx69r413c7z7c8972jdwr4w7rijazrnw8j3nz4v34bhrsgbajm8")))

(define-public crate-joyn-0.2 (crate (name "joyn") (vers "0.2.0") (deps (list (crate-dep (name "clap") (req "^4") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "assert_cmd") (req "^2") (default-features #t) (kind 2)) (crate-dep (name "predicates") (req "^2") (default-features #t) (kind 2)))) (hash "0bs4j3zpybq6k4xs9dnrs0jagqi7rbw2xd487ajzihbin515d6fn")))

(define-public crate-joyn-0.2 (crate (name "joyn") (vers "0.2.2") (deps (list (crate-dep (name "clap") (req "^4") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "assert_cmd") (req "^2") (default-features #t) (kind 2)) (crate-dep (name "predicates") (req "^2") (default-features #t) (kind 2)))) (hash "0n4x2zhqrzsiss7r5l92mjxh4j6lhfrfhj93pgkm5sw86wghf0dm")))

(define-public crate-joyn-0.2 (crate (name "joyn") (vers "0.2.3") (deps (list (crate-dep (name "clap") (req "^4") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "assert_cmd") (req "^2") (default-features #t) (kind 2)) (crate-dep (name "predicates") (req "^2") (default-features #t) (kind 2)))) (hash "04389ady6bhzzrdljrbgz92v558g101df6flarm0688vrc0r7m4i") (yanked #t)))

(define-public crate-joyn-0.2 (crate (name "joyn") (vers "0.2.4") (deps (list (crate-dep (name "clap") (req "^4") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "assert_cmd") (req "^2") (default-features #t) (kind 2)) (crate-dep (name "predicates") (req "^2") (default-features #t) (kind 2)))) (hash "1x5v9p0k8kifc0spl3ljng7541sini7j3rk1qvkgj09f0xyqizss")))

(define-public crate-joyn-0.4 (crate (name "joyn") (vers "0.4.0") (deps (list (crate-dep (name "clap") (req "^4") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "assert_cmd") (req "^2") (default-features #t) (kind 2)) (crate-dep (name "predicates") (req "^2") (default-features #t) (kind 2)))) (hash "0ngncwzyk03clc2gp2hrprkccgq43z3q48lp06jvhsk3n508wi3w")))

