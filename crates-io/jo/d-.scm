(define-module (crates-io jo d-) #:use-module (crates-io))

(define-public crate-jod-thread-0.1 (crate (name "jod-thread") (vers "0.1.0") (hash "1zz4zkyllanq84synwkw8nncg0jrx89rwk8fka1ap3xqfcgs2lig")))

(define-public crate-jod-thread-0.1 (crate (name "jod-thread") (vers "0.1.1") (hash "0jdvk3mvzdggjrqmjdhq1h6lp14da6kapjpbrnkn9rf3f9i6a8j0")))

(define-public crate-jod-thread-0.1 (crate (name "jod-thread") (vers "0.1.2") (hash "1bj7g6l59ybcf33znf80ccqbxvs1cmd8ynd4m8h7ywdqk473c8wb")))

