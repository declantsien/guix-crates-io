(define-module (crates-io jo rd) #:use-module (crates-io))

(define-public crate-jord-0.1 (crate (name "jord") (vers "0.1.0") (hash "0gqac2vrq81mlpi4xmpr3873k2sl7rxzxjc06jx5k1rlj93mj5vb")))

(define-public crate-jord-0.2 (crate (name "jord") (vers "0.2.0") (deps (list (crate-dep (name "criterion") (req "^0.5.1") (features (quote ("html_reports"))) (default-features #t) (kind 2)))) (hash "1bkzw2w9xp05g34gjyqs7gi8dgk0nr726xcnw8v9hsjm4rb9m99a")))

(define-public crate-jord-0.3 (crate (name "jord") (vers "0.3.0") (deps (list (crate-dep (name "criterion") (req "^0.5.1") (features (quote ("html_reports"))) (default-features #t) (kind 2)))) (hash "04h6mnvxwd9535vimx6qkigx0rh99k2kfaa2wl20ldh48mgyr1af") (rust-version "1.65")))

(define-public crate-jord-0.4 (crate (name "jord") (vers "0.4.0") (deps (list (crate-dep (name "criterion") (req "^0.5.1") (features (quote ("html_reports"))) (default-features #t) (kind 2)))) (hash "12mpz9nykqsajlk83ax6wc8l93l1ikj2y5himpqhnn3yisgn7h6g") (rust-version "1.65")))

(define-public crate-jord-0.5 (crate (name "jord") (vers "0.5.0") (deps (list (crate-dep (name "criterion") (req "^0.5.1") (features (quote ("html_reports"))) (default-features #t) (kind 2)))) (hash "0idlr5yas2icisvadn6aivzarjq1slycc51lxs0bvggh0srw6iin") (rust-version "1.65")))

(define-public crate-jord-0.6 (crate (name "jord") (vers "0.6.0") (deps (list (crate-dep (name "criterion") (req "^0.5.1") (features (quote ("html_reports"))) (default-features #t) (kind 2)))) (hash "0n5mn175792qsmlrfp2jjc78ddbsx27b0linvwwpk1jrlbi9fhnb") (rust-version "1.65")))

(define-public crate-jord-0.7 (crate (name "jord") (vers "0.7.0") (deps (list (crate-dep (name "criterion") (req "^0.5.1") (features (quote ("html_reports"))) (default-features #t) (kind 2)))) (hash "06dn3nwi1ymxxd1bvrvza736aiv57sfpfhhpkaa9n51kn3l4qpni") (rust-version "1.65")))

(define-public crate-jord-0.8 (crate (name "jord") (vers "0.8.0") (deps (list (crate-dep (name "criterion") (req "^0.5.1") (features (quote ("html_reports"))) (default-features #t) (kind 2)))) (hash "1mg1x4vzgdz4l1qlwkkmrpmb724lfy55pv77why3bx2ym2pf3mxm") (rust-version "1.65")))

(define-public crate-jord-0.9 (crate (name "jord") (vers "0.9.0") (deps (list (crate-dep (name "criterion") (req "^0.5.1") (features (quote ("html_reports"))) (default-features #t) (kind 2)))) (hash "0827idhy7bx7yv55zlx6wg0cd6swy4kfzpmc8ii4llk0ikpmq9p2") (rust-version "1.65")))

(define-public crate-jord-0.10 (crate (name "jord") (vers "0.10.0") (deps (list (crate-dep (name "criterion") (req "^0.5.1") (features (quote ("html_reports"))) (default-features #t) (kind 2)))) (hash "0ampmq7wkxic0jb0bg05bvw3vm2wr1kzzy468vr6n96y1fs0r907") (rust-version "1.65")))

(define-public crate-jord-0.11 (crate (name "jord") (vers "0.11.0") (deps (list (crate-dep (name "criterion") (req "^0.5.1") (features (quote ("html_reports"))) (default-features #t) (kind 2)))) (hash "01q80p20z739ikc8mx8rc3j00wxv6l8iwkklf55gzhx8qdls0irc") (rust-version "1.65")))

(define-public crate-jord-0.12 (crate (name "jord") (vers "0.12.0") (deps (list (crate-dep (name "criterion") (req "^0.5.1") (features (quote ("html_reports"))) (default-features #t) (kind 2)))) (hash "0if0rm42a19r2ald2383x9850rl3cavsxjf8rd670zx3cikzvxg7") (rust-version "1.65")))

(define-public crate-jord-0.13 (crate (name "jord") (vers "0.13.0") (deps (list (crate-dep (name "criterion") (req "^0.5.1") (features (quote ("html_reports"))) (default-features #t) (kind 2)))) (hash "0x5xrgijb2yhln79cghxfd5hy69ixlgskb0nhb3jw89x8ddq07ra") (rust-version "1.65")))

(define-public crate-jord-0.14 (crate (name "jord") (vers "0.14.0") (deps (list (crate-dep (name "criterion") (req "^0.5.1") (features (quote ("html_reports"))) (default-features #t) (kind 2)))) (hash "1a2dgp58hi23f2nii0acjn2dhyjrz4r6b0sz5778izf126mr3y10") (rust-version "1.65")))

(define-public crate-jordan4ibanez_perlin_rust-0.0.1 (crate (name "jordan4ibanez_perlin_rust") (vers "0.0.1") (hash "01z3zn6h0xc6rw230mr7dngqmc7kasvv7lwjpnwldaij235zymmz")))

(define-public crate-jordin-0.1 (crate (name "jordin") (vers "0.1.0") (hash "0y1w14rcwlv64vs8p17kisrr3j7p65a0jkrp58hwfx6b6hfwg4ql")))

