(define-module (crates-io ji mu) #:use-module (crates-io))

(define-public crate-jimu-framework-0.1 (crate (name "jimu-framework") (vers "0.1.0") (hash "1xbadfcdcm5lrwrj01aca8civ5avs4xjy0kmbnsw4mdd8ci227lf")))

(define-public crate-jimu-framework-0.1 (crate (name "jimu-framework") (vers "0.1.1") (hash "0adx4lj12na4v0f6v1k7rxwr0pq90wcs1dlf2znl3ysvy3ngbm8w")))

