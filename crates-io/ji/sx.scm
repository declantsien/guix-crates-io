(define-module (crates-io ji sx) #:use-module (crates-io))

(define-public crate-jisx0401-0.1 (crate (name "jisx0401") (vers "0.1.0-alpha.2") (hash "0cgn9mslcnn66rrlz712sf3f3f145hnshr43kkhii8rbdmw5xk3h")))

(define-public crate-jisx0401-0.1 (crate (name "jisx0401") (vers "0.1.0-alpha.3") (hash "0ldryrwr2j37k7bqkdbgi3mb2x8g1ikskfsajh1fqw36v5gy7aih")))

(define-public crate-jisx0401-0.1 (crate (name "jisx0401") (vers "0.1.0-beta.1") (hash "12n365am1grb4glryhn17qhaiwsmr5bkg62wbdh8m0xhfplfnyxd")))

(define-public crate-jisx0401-0.1 (crate (name "jisx0401") (vers "0.1.0-beta.2") (hash "10113m8yrxmmv85rrri1m93qrwbm38qfr3lfsmlrw95j2aww6zhy")))

