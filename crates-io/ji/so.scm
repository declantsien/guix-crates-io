(define-module (crates-io ji so) #:use-module (crates-io))

(define-public crate-jisort-0.0.4 (crate (name "jisort") (vers "0.0.4") (deps (list (crate-dep (name "argh") (req "^0.1.10") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "ignore") (req "^0.4.20") (default-features #t) (kind 0)) (crate-dep (name "nom") (req "^7.1.3") (default-features #t) (kind 0)))) (hash "095jy9qr74khy87phhi8yp922f25rj3zma833rf4q590cy56vrpr") (features (quote (("default" "argh"))))))

