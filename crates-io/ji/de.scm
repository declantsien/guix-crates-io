(define-module (crates-io ji de) #:use-module (crates-io))

(define-public crate-jiden-0.1 (crate (name "jiden") (vers "0.1.0") (deps (list (crate-dep (name "serde") (req "^1.0.197") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.114") (default-features #t) (kind 0)))) (hash "1qz2i3gqb3xryhmn5bavfa1i150a1mizgy4s4n3qa5dbifd2i6lq")))

(define-public crate-jiden-0.1 (crate (name "jiden") (vers "0.1.1") (deps (list (crate-dep (name "serde") (req "^1.0.197") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.114") (default-features #t) (kind 0)))) (hash "1jpvv7fvh61vpqlhdb3bk0j1w5wfl19xpvp7207whfin3ppix3ya")))

