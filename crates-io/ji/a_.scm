(define-module (crates-io ji a_) #:use-module (crates-io))

(define-public crate-jia_pipe_viewer-0.1 (crate (name "jia_pipe_viewer") (vers "0.1.2") (deps (list (crate-dep (name "clap") (req "^2.33.0") (default-features #t) (kind 0)) (crate-dep (name "crossbeam") (req "^0.7.3") (default-features #t) (kind 0)) (crate-dep (name "crossterm") (req "^0.14.2") (default-features #t) (kind 0)))) (hash "1rdsc78lc2d3h8aks96z1pxkrnbljhgh8nd5sr2gzn5zjk832sxf")))

(define-public crate-jia_pipe_viewer-0.1 (crate (name "jia_pipe_viewer") (vers "0.1.3") (deps (list (crate-dep (name "clap") (req "^2.33.0") (default-features #t) (kind 0)) (crate-dep (name "crossbeam") (req "^0.7.3") (default-features #t) (kind 0)) (crate-dep (name "crossterm") (req "^0.14.2") (default-features #t) (kind 0)))) (hash "0xlasdzda03fllp8c6gwk5735k505vp1iarnm9g2yx5j8nppjxsf")))

(define-public crate-jia_pipe_viewer-0.1 (crate (name "jia_pipe_viewer") (vers "0.1.4") (deps (list (crate-dep (name "clap") (req "^2.33.0") (default-features #t) (kind 0)) (crate-dep (name "crossbeam") (req "^0.7.3") (default-features #t) (kind 0)) (crate-dep (name "crossterm") (req "^0.14.2") (default-features #t) (kind 0)))) (hash "0mzxdf76nsmixc0z3bjz9b4c83hqqk9j5sa2hvz9k17q70dp1l14")))

