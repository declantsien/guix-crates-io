(define-module (crates-io ji ms) #:use-module (crates-io))

(define-public crate-jims-new-rust-cli-test-0.1 (crate (name "jims-new-rust-cli-test") (vers "0.1.3") (hash "0h1w0xfyn77hbyn5l5c0jid2wycwy2rqh7wmlqq8ivxwfcasl45r")))

(define-public crate-jims-registry-readme-0.0.1 (crate (name "jims-registry-readme") (vers "0.0.1") (hash "15a0mglbjhdahza1s7sc87pkhn62dxs0bb4hwdx0fiv78a7spmzj")))

(define-public crate-jims-rust-cli-test-0.1 (crate (name "jims-rust-cli-test") (vers "0.1.2") (hash "0n0xbz2mbfsm726la58r9ql7vlhyis0abk4qax3wv0mzb8pwwrfc")))

(define-public crate-jims-rust-cli-test-0.1 (crate (name "jims-rust-cli-test") (vers "0.1.3") (hash "11kxpp1mzz06synvky5zi85rvn3dfxnd9c5sy2dgywi9qa8p1z75")))

