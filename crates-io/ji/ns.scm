(define-module (crates-io ji ns) #:use-module (crates-io))

(define-public crate-jinshu-0.1 (crate (name "jinshu") (vers "0.1.0") (hash "0svjza2338ypnqpa3179wfdz1hspknd0wf4lmicngjqq9wwcslj5")))

(define-public crate-jinshu-admin-0.0.1 (crate (name "jinshu-admin") (vers "0.0.1") (hash "0zq94431jlzvb6pgvz0934nc0b72182hb47nv59yh7633lalqrd5")))

(define-public crate-jinshu-api-0.0.1 (crate (name "jinshu-api") (vers "0.0.1") (hash "01a4shwpi92zqn3avzh4bx0kl82fsjfdbbfphfc1w7px16ldfy9c")))

(define-public crate-jinshu-authorizer-0.0.1 (crate (name "jinshu-authorizer") (vers "0.0.1") (hash "013i8rs0f34slknw9rqlcdjy1rb3zg27rcmgaik7w6fc69x17sbi")))

(define-public crate-jinshu-chat-0.0.1 (crate (name "jinshu-chat") (vers "0.0.1") (hash "1qbzr5ny1q1hw6xkfqy67malmd2i82nkqlb1q93gzzfw6z14jmy4")))

(define-public crate-jinshu-cli-0.0.1 (crate (name "jinshu-cli") (vers "0.0.1") (hash "17cwm86yd108mwgg7m4bg359n9yaxd73c6ycw51z4asvsdzxxn58")))

(define-public crate-jinshu-comet-0.0.1 (crate (name "jinshu-comet") (vers "0.0.1") (hash "009664ds263r23wikwihgz349cn1hwk2zw7w0jqm5yjx5mzd2wyl")))

(define-public crate-jinshu-common-0.0.1 (crate (name "jinshu-common") (vers "0.0.1") (hash "0ivkd63qa6l5zm4i57sx7d0x84dwh1sgqvr6lvg5fk8853yyj3vh")))

(define-public crate-jinshu-distributor-0.0.1 (crate (name "jinshu-distributor") (vers "0.0.1") (hash "0xs80y0p1z0h34x8xdiyimhs2mxzyvk9z8kwrvkg4iq759jlhh25")))

(define-public crate-jinshu-file-0.0.1 (crate (name "jinshu-file") (vers "0.0.1") (hash "12wyhv3nly1qmh7nqi3dzfqpqm4mnd2vvd6nh1prjprj9r2wx9nq")))

(define-public crate-jinshu-friend-0.0.1 (crate (name "jinshu-friend") (vers "0.0.1") (hash "0kgzx1c78nc2r4h8kf19pr5rnhcb1m6vjydifls3m4xc0n3c4h6b")))

(define-public crate-jinshu-gateway-0.0.1 (crate (name "jinshu-gateway") (vers "0.0.1") (hash "079j8p5qa3iylc6kg87rfy1fh0cl5wfc3y36b9c1s1c6ndshppqs")))

(define-public crate-jinshu-group-0.0.1 (crate (name "jinshu-group") (vers "0.0.1") (hash "0j0mjibw4mrznha7k9x8jhs4yf6kq6ayfny7iyyh04svpap2bmbs")))

(define-public crate-jinshu-model-0.1 (crate (name "jinshu-model") (vers "0.1.0") (hash "0ipiw85r0qdpdf5qgqmwk8y2r99j1dwmz5i1n39jb0a690m4w7rj")))

(define-public crate-jinshu-protocol-0.0.1 (crate (name "jinshu-protocol") (vers "0.0.1") (hash "0694h1brxm4ks32xcizjqcikks9s36y49b2w1lkikxl65g2ll4jh")))

(define-public crate-jinshu-pusher-0.0.1 (crate (name "jinshu-pusher") (vers "0.0.1") (hash "1ahf4cbmrm7agbp5k406ffpavn79b4a6b53qg9vvlhmhyk4jfba2")))

(define-public crate-jinshu-receiver-0.0.1 (crate (name "jinshu-receiver") (vers "0.0.1") (hash "0h2b98nnww35s9cbnsbyzq02lxjxs13597rcrd5bgswra7a6kj1p")))

(define-public crate-jinshu-recorder-0.0.1 (crate (name "jinshu-recorder") (vers "0.0.1") (hash "1jb578za761n2sxdcl49i21ii2z2k357gk1xzvssc4icjvq2pw7j")))

(define-public crate-jinshu-rpc-0.0.1 (crate (name "jinshu-rpc") (vers "0.0.1") (hash "1mbj17107bspvd55clfscpnq7i266ii6w670g8ip4d948qxbjdnw")))

(define-public crate-jinshu-sdk-0.0.1 (crate (name "jinshu-sdk") (vers "0.0.1") (hash "1yh18szm04yvhx58hcsnhzisnd006355r1f84maadd3bhhqgmy6b")))

(define-public crate-jinshu-sfu-0.0.1 (crate (name "jinshu-sfu") (vers "0.0.1") (hash "1jwp2v92l43aiqn35l4psgpinbgiv9vlipkbrw2aqdlzm1w2a2cw")))

(define-public crate-jinshu-storage-0.0.1 (crate (name "jinshu-storage") (vers "0.0.1") (hash "0cg2qakjs0h60yqkmap3qa7lkckv3y02i5ywwg4riimis258lxna")))

(define-public crate-jinshu-timer-0.0.1 (crate (name "jinshu-timer") (vers "0.0.1") (hash "1zsrs92b2gyi78wy9qn73yh8aspp8il3xwd2hhy2l0132ji39cyz")))

(define-public crate-jinshu-transcoder-0.0.1 (crate (name "jinshu-transcoder") (vers "0.0.1") (hash "18agjg58g1990cjb1pihvjxxy83r9gb3b7mrqp7ysfj1zp3hk11b")))

(define-public crate-jinshu-utils-0.0.1 (crate (name "jinshu-utils") (vers "0.0.1") (hash "0hb9i3c1gld09j1fhirkkpmy6sfacy2z8py1j7jiydafmf9k1zwf")))

