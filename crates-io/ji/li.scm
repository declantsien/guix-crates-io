(define-module (crates-io ji li) #:use-module (crates-io))

(define-public crate-jili-0.0.0 (crate (name "jili") (vers "0.0.0") (hash "1vc66j2fif2zfi0is05sdbrl6map42z9p7r87c3p8kl1914xrj10")))

(define-public crate-jili-0.0.1 (crate (name "jili") (vers "0.0.1") (hash "0wi4irjgwxs3xvq7s9vffxm518izglkr2r2l78n4qm48ilmplxsf")))

