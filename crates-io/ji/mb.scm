(define-module (crates-io ji mb) #:use-module (crates-io))

(define-public crate-jimberlage_jira_client-1 (crate (name "jimberlage_jira_client") (vers "1.0.0") (deps (list (crate-dep (name "base64") (req "^0.21") (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.11") (features (quote ("blocking" "json"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)))) (hash "1vmhivlmj3lrmcpn73ql2svjbms72p2afppiiz6cq09spc2lh8w9")))

(define-public crate-jimberlage_jira_client-1 (crate (name "jimberlage_jira_client") (vers "1.2.0") (deps (list (crate-dep (name "base64") (req "^0.21") (default-features #t) (kind 0)) (crate-dep (name "chrono") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.11") (features (quote ("blocking" "json"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)))) (hash "0idav13bqyscm3vm0pdj8n9bbc98ph2mppr3hrrm3lgc88rghlvb")))

(define-public crate-jimberlage_jira_client-1 (crate (name "jimberlage_jira_client") (vers "1.3.0") (deps (list (crate-dep (name "base64") (req "^0.21") (default-features #t) (kind 0)) (crate-dep (name "chrono") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.11") (features (quote ("blocking" "json"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)))) (hash "0sq674d122g59f0qdprdmpwf29fwcw22h168y1hsq4mv4vkjgc24")))

(define-public crate-jimblandy-yank-test-0.1 (crate (name "jimblandy-yank-test") (vers "0.1.0") (hash "1mgmsmnla8wl513mi26xwzq0w05crqbhk5rpcwhhqjmsx69q94dx")))

(define-public crate-jimblandy-yank-test-0.1 (crate (name "jimblandy-yank-test") (vers "0.1.1") (hash "1gl55xcp0sgjwyd3yv1mdj25rlk92mn626m44bxklzqh2pbyzf9w") (yanked #t)))

(define-public crate-jimblandy-yank-test-0.1 (crate (name "jimblandy-yank-test") (vers "0.1.2") (hash "0kcx71cm8s1kcf4i14fdwp9l4iwy1mi6bhwcf48d666mn5664w7q") (yanked #t)))

(define-public crate-jimblandy-yank-test2-0.1 (crate (name "jimblandy-yank-test2") (vers "0.1.0") (deps (list (crate-dep (name "jimblandy-yank-test") (req "^0.1.1") (default-features #t) (kind 0)))) (hash "1a7bmj8nn1r6jrh643wr73bl5d3dhwk9wn9r86fdlvi6lclvb2p9")))

(define-public crate-jimblandy-yank-test2-0.1 (crate (name "jimblandy-yank-test2") (vers "0.1.2") (deps (list (crate-dep (name "jimblandy-yank-test") (req "^0.1.2") (default-features #t) (kind 0)))) (hash "05zcgij2yncxg0mgnlblh035rq4lx89sswimkd3i2j8qxi6ay9dh")))

(define-public crate-jimbo-0.1 (crate (name "jimbo") (vers "0.1.0") (hash "17cgrwdwkb8awp26v4x7bdj1x5xxzzylv7gafn2r8szhai5a1fpc")))

