(define-module (crates-io ji tt) #:use-module (crates-io))

(define-public crate-jitterentropy-sys-0.1 (crate (name "jitterentropy-sys") (vers "0.1.0") (deps (list (crate-dep (name "bindgen") (req "^0.53") (default-features #t) (kind 1)) (crate-dep (name "make-cmd") (req "^0.1.0") (default-features #t) (kind 1)))) (hash "14ngh5g5c55w70wckqs56sbs0qzkx5mb5qsxfzlnrbj88lgybhwl")))

(define-public crate-jittr-0.1 (crate (name "jittr") (vers "0.1.0") (deps (list (crate-dep (name "futures") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "futures-timer") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4") (optional #t) (default-features #t) (kind 0)))) (hash "1wbfidv166fwxwrq2zpicsk5r0ap4hlxm40l1ns37jck2ap4l109") (features (quote (("default" "log"))))))

(define-public crate-jittr-0.1 (crate (name "jittr") (vers "0.1.1") (deps (list (crate-dep (name "futures") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "futures-timer") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4") (optional #t) (default-features #t) (kind 0)))) (hash "0g1a9p22ixzk8wnpz17xzkg6s16s25xyg1zz1g0zfnkc93wsdrjz") (features (quote (("default" "log"))))))

(define-public crate-jittr-0.2 (crate (name "jittr") (vers "0.2.0") (deps (list (crate-dep (name "futures") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "futures-timer") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4") (optional #t) (default-features #t) (kind 0)))) (hash "0gbj0h17kvskk0x67x8gqshi84ik948pgak9ib27rl2ffp355mas") (features (quote (("default" "log"))))))

