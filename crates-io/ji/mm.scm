(define-module (crates-io ji mm) #:use-module (crates-io))

(define-public crate-jimmy-0.9 (crate (name "jimmy") (vers "0.9.0") (deps (list (crate-dep (name "clap") (req "^3.0.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.5.4") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.133") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_yaml") (req "^0.8.23") (default-features #t) (kind 0)))) (hash "1vpzd6gg9ficgn69wsn1zqy6a7myzla3wpdzy3rzw6a105ayp0r7")))

(define-public crate-jimmy-0.10 (crate (name "jimmy") (vers "0.10.0") (deps (list (crate-dep (name "clap") (req "^3.0.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.5.4") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.133") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_yaml") (req "^0.8.23") (default-features #t) (kind 0)))) (hash "0n9q1bl4yzppr634ixf3bf35z96hhqwn9cc7a8waklzja3yyjkyc")))

