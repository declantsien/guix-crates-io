(define-module (crates-io ji gg) #:use-module (crates-io))

(define-public crate-jiggle-0.1 (crate (name "jiggle") (vers "0.1.0") (deps (list (crate-dep (name "env_logger") (req "^0.9.0") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.16") (default-features #t) (kind 0)) (crate-dep (name "mouse-rs") (req "^0.4.2") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.3.26") (default-features #t) (kind 0)))) (hash "1wj2f60iqnhsdhnl7vxlv1ak8zqkkq345fvd2sixxpmaiksgl1py")))

