(define-module (crates-io ji np) #:use-module (crates-io))

(define-public crate-jinpeng_test-0.1 (crate (name "jinpeng_test") (vers "0.1.0") (hash "1wrjl16x9bsxy3cbzmkkskcphq1k95zc5q0q3xgkqhvny2nia0ah")))

(define-public crate-jinpyok_input-0.1 (crate (name "jinpyok_input") (vers "0.1.0") (hash "1m5qdpg4dh6jbav8nbd5nnxvh9p539i9q52jf0b6nzddc7f8blwg") (yanked #t)))

(define-public crate-jinpyok_input-0.1 (crate (name "jinpyok_input") (vers "0.1.1") (hash "0c01rb0app6pm3bz3bvzdx6a4qcar57y3ha6xx1pig9yxwxnz04k") (yanked #t)))

(define-public crate-jinpyok_input-0.1 (crate (name "jinpyok_input") (vers "0.1.2") (hash "0g9zrsppks892ms1wjxi1qn4wjgcj0djvd59iiawcazam13dciva") (yanked #t)))

(define-public crate-jinpyok_input-0.1 (crate (name "jinpyok_input") (vers "0.1.3") (hash "1j10p62pfs3d5c8g7qalsj6f4vc2rq5hbpwhaizmm5d10107zsbi")))

