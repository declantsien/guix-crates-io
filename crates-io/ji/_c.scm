(define-module (crates-io ji _c) #:use-module (crates-io))

(define-public crate-ji_cloud_shared-0.1 (crate (name "ji_cloud_shared") (vers "0.1.0") (deps (list (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_repr") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "strum") (req "^0.18") (default-features #t) (kind 0)) (crate-dep (name "strum_macros") (req "^0.18") (default-features #t) (kind 0)))) (hash "17csz0y3wmkjhw2fhjd70fx49wzp7kc3wd5bi9kwzl5p4ywh8mzx") (features (quote (("frontend") ("backend"))))))

