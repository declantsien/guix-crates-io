(define-module (crates-io ji sp) #:use-module (crates-io))

(define-public crate-jisp_sha2-0.1 (crate (name "jisp_sha2") (vers "0.1.0") (deps (list (crate-dep (name "crypto-bigint") (req "^0.5.5") (default-features #t) (kind 0)))) (hash "1dldvxlvyibx9021rfwdad64xak0br82ig9c8zrqn7ak01shvj04")))

(define-public crate-jisp_sha2-0.2 (crate (name "jisp_sha2") (vers "0.2.0") (deps (list (crate-dep (name "crypto-bigint") (req "^0.5.5") (default-features #t) (kind 0)))) (hash "1afrvpic35lmfvvlgxxsyp554x6m9mr149axyragamzvaybiny2p")))

(define-public crate-jisp_sha2-0.2 (crate (name "jisp_sha2") (vers "0.2.1") (deps (list (crate-dep (name "crypto-bigint") (req "^0.5.5") (default-features #t) (kind 0)))) (hash "15f3sgz3ms1ibhrqxwxj6rqmw9rny7b642zpfsl5cikabp4351w8")))

(define-public crate-jisp_sha2-0.2 (crate (name "jisp_sha2") (vers "0.2.2") (deps (list (crate-dep (name "crypto-bigint") (req "^0.5.5") (default-features #t) (kind 0)))) (hash "1sangxplqfmf8k8shqmyljl8dqrvdmkl7nycxcw7hbk31xpl7gxg")))

(define-public crate-jisp_sha2-0.2 (crate (name "jisp_sha2") (vers "0.2.3") (deps (list (crate-dep (name "crypto-bigint") (req "^0.5.5") (default-features #t) (kind 0)))) (hash "193qd4kqbh6djnzxk8vr524z2i2vk4pik3xsf6xg17w2mr1v8jhz")))

(define-public crate-jisp_sha3-0.1 (crate (name "jisp_sha3") (vers "0.1.0") (hash "1c86czmj31ngasp04a837gadjjsk50nh4ddsbrksn76j6j4vbdlb")))

(define-public crate-jisp_sha3-0.1 (crate (name "jisp_sha3") (vers "0.1.1") (hash "1y80xnvjr2qgw8xbfismlcdwk5p06rzrm3dlrg4fqyia8kp92627")))

