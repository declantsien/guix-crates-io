(define-module (crates-io ji d-) #:use-module (crates-io))

(define-public crate-jid-gst-meet-0.9 (crate (name "jid-gst-meet") (vers "0.9.2") (deps (list (crate-dep (name "minidom-gst-meet") (req "^0.13") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)))) (hash "0dp98l09rj3df5s92qlp03b555zrsgmpinj0m8rids8ha541c31q")))

(define-public crate-jid-gst-meet-0.9 (crate (name "jid-gst-meet") (vers "0.9.3") (deps (list (crate-dep (name "minidom") (req "^0.13") (optional #t) (default-features #t) (kind 0) (package "minidom-gst-meet")) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)))) (hash "0wssyry9dxms4qxz2kmrazycww49mg4wzgbhw1nmwlwda5dr7q0r")))

