(define-module (crates-io ji t_) #:use-module (crates-io))

(define-public crate-jit_macros-0.0.1 (crate (name "jit_macros") (vers "0.0.1") (deps (list (crate-dep (name "jit") (req "*") (default-features #t) (kind 0)))) (hash "0xgmwnq2n1cd2qm53rkmnrhfh69a48hzs0fjdfnp756bgvcxzcqj")))

(define-public crate-jit_macros-0.0.2 (crate (name "jit_macros") (vers "0.0.2") (hash "1gnxg0gkclcka0hibdi90f6nq3iqjk48y6n3f4bjj94lj8pzrrga")))

(define-public crate-jit_macros-0.0.3 (crate (name "jit_macros") (vers "0.0.3") (hash "0qwlgdgjdsd85ikz3ssdcj54yhgnpwcd94kgcc5mvsimvdvayiwn")))

(define-public crate-jit_macros-0.0.4 (crate (name "jit_macros") (vers "0.0.4") (hash "19nharidwwi6v912fv78vgw805lw644wn874rj8jdqphnh7p3xis")))

(define-public crate-jit_macros-0.0.5 (crate (name "jit_macros") (vers "0.0.5") (hash "02vrl8yhlh7isflc1f7g9v3s750fcss8nrm94m76d2w57gdmml1q")))

(define-public crate-jit_macros-0.0.6 (crate (name "jit_macros") (vers "0.0.6") (deps (list (crate-dep (name "matches") (req "*") (default-features #t) (kind 0)))) (hash "1sgb439rfgwyg6w9a2yz6hngc5lpnsw4mfv1gf5m6886pyyfgcri")))

(define-public crate-jit_macros-0.0.7 (crate (name "jit_macros") (vers "0.0.7") (deps (list (crate-dep (name "matches") (req "*") (default-features #t) (kind 0)))) (hash "1fj90z56dfrh2p27wp1q3mxlk80nb4ddcfn0fcm87prs92l2grkq")))

(define-public crate-jit_macros-0.0.8 (crate (name "jit_macros") (vers "0.0.8") (deps (list (crate-dep (name "matches") (req "*") (default-features #t) (kind 0)))) (hash "0xmwfbqh6k4zx4g1n5vc9hmm7k8dqhs3wq6vdqa073dwavnsc385")))

(define-public crate-jit_macros-0.0.9 (crate (name "jit_macros") (vers "0.0.9") (deps (list (crate-dep (name "matches") (req "*") (default-features #t) (kind 0)))) (hash "1rh85mriflh564gn9dn2vi2mq5m0j56i3n5k4kk6my6mql6nmd85")))

(define-public crate-jit_rs-0.1 (crate (name "jit_rs") (vers "0.1.0") (hash "1dr0wl756k9zy2n8d1q4q9pinchy76cmq80mwydqfddc0vkb9ick")))

(define-public crate-jit_rs-0.1 (crate (name "jit_rs") (vers "0.1.1") (deps (list (crate-dep (name "jit_rs-sys") (req "^0.1.1") (default-features #t) (kind 0)))) (hash "0krrkcyffp7lym07xayks85kg85116kbk18yfrvik9p4hsizff6r")))

(define-public crate-jit_rs-0.1 (crate (name "jit_rs") (vers "0.1.2") (deps (list (crate-dep (name "jit_rs-sys") (req "^0.1.2") (default-features #t) (kind 0)))) (hash "1z3x9yb7g9jzxc9m39460fi1yljg4gs029a9njs68cvvi6clsfxf")))

(define-public crate-jit_rs-0.1 (crate (name "jit_rs") (vers "0.1.3") (deps (list (crate-dep (name "jit_rs-sys") (req "^0.1.3") (default-features #t) (kind 0)))) (hash "0bm9l89cyhxjkvx34ba337md0v0kx5lgihwj4abf2jpza9zhykpg")))

(define-public crate-jit_rs-sys-0.1 (crate (name "jit_rs-sys") (vers "0.1.1") (deps (list (crate-dep (name "windows") (req "^0.13.0") (default-features #t) (kind 0)) (crate-dep (name "windows") (req "^0.13.0") (default-features #t) (kind 1)))) (hash "0rm2jzrlb75x7iqp2myhfj2kih9jj0xrl6gl9bg530qdlbd24rly")))

(define-public crate-jit_rs-sys-0.1 (crate (name "jit_rs-sys") (vers "0.1.2") (deps (list (crate-dep (name "windows") (req "^0.13.0") (default-features #t) (kind 0)) (crate-dep (name "windows") (req "^0.13.0") (default-features #t) (kind 1)))) (hash "0dzdimaj5m2d4vllvd5qqwxj8r302d4c8cdpc2snrvnn8g31qyg1")))

(define-public crate-jit_rs-sys-0.1 (crate (name "jit_rs-sys") (vers "0.1.3") (deps (list (crate-dep (name "windows") (req "^0.13.0") (default-features #t) (kind 0)) (crate-dep (name "windows") (req "^0.13.0") (default-features #t) (kind 1)))) (hash "0kikahbv0kzrcpb3yffqrxcjckzy3p273cr6p3z06d7fmnibdnbr")))

