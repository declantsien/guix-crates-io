(define-module (crates-io ji tr) #:use-module (crates-io))

(define-public crate-jitRegistry-0.0.1 (crate (name "jitRegistry") (vers "0.0.1") (deps (list (crate-dep (name "oci-image-spec") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.3.26") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.17.0") (default-features #t) (kind 0)))) (hash "03kdqin169gfwnbd0yxjk1ax0m0dj4mn14w4l9zivr6bwbr20zdy")))

