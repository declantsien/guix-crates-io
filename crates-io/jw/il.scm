(define-module (crates-io jw il) #:use-module (crates-io))

(define-public crate-jwilm-xdo-0.1 (crate (name "jwilm-xdo") (vers "0.1.0") (deps (list (crate-dep (name "foreign-types") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2.20") (default-features #t) (kind 0)) (crate-dep (name "libxdo-sys") (req "^0.11") (default-features #t) (kind 0)) (crate-dep (name "x11") (req "^2.1.0") (features (quote ("xlib"))) (default-features #t) (kind 0)))) (hash "1ldvn8a52bgi3ibw202ynw0rydy3ppqv4q99lxbvgyr4n3dj7lil")))

(define-public crate-jwilm-xdo-0.1 (crate (name "jwilm-xdo") (vers "0.1.1") (deps (list (crate-dep (name "foreign-types") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2.20") (default-features #t) (kind 0)) (crate-dep (name "libxdo-sys") (req "^0.11") (default-features #t) (kind 0)) (crate-dep (name "x11") (req "^2.1.0") (features (quote ("xlib"))) (default-features #t) (kind 0)))) (hash "0ml79xabm6f428gny1mxi6j3zjlmf14kz5vnk6fiva8x3481h5md")))

(define-public crate-jwilm-xdo-0.1 (crate (name "jwilm-xdo") (vers "0.1.2") (deps (list (crate-dep (name "foreign-types") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2.20") (default-features #t) (kind 0)) (crate-dep (name "libxdo-sys") (req "^0.11") (default-features #t) (kind 0)) (crate-dep (name "x11") (req "^2.1.0") (features (quote ("xlib"))) (default-features #t) (kind 0)))) (hash "0zl7sg6bs3k2nsxam8k46nydwv1nk6m72zq46n1sb8g04dbsgq6q")))

