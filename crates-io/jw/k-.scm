(define-module (crates-io jw k-) #:use-module (crates-io))

(define-public crate-jwk-set-0.0.0 (crate (name "jwk-set") (vers "0.0.0") (hash "1kv76vkggl6c37h2qkrdx4xzwjwd7prqzxw0949h33704cm6y7kc")))

(define-public crate-jwk-set-0.1 (crate (name "jwk-set") (vers "0.1.0") (deps (list (crate-dep (name "jsonwebkey") (req "^0.3") (kind 0)) (crate-dep (name "jsonwebtoken") (req "^7.2") (optional #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("std" "derive"))) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (features (quote ("std"))) (optional #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (features (quote ("std"))) (kind 2)))) (hash "1lqnk1ivfgw52rgw72jhrlczy304c8pfszlspjwqvcw6wm3f0080") (features (quote (("with-decrypt" "jsonwebkey/jwt-convert" "jsonwebtoken") ("default" "with-decrypt") ("_priv_with_serde_json" "serde_json"))))))

(define-public crate-jwk-set-0.1 (crate (name "jwk-set") (vers "0.1.1") (deps (list (crate-dep (name "jsonwebkey") (req "^0.3.5") (kind 0)) (crate-dep (name "jsonwebtoken") (req "^8.0") (optional #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("std" "derive"))) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (features (quote ("std"))) (optional #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (features (quote ("std"))) (kind 2)))) (hash "1y8d2n9wapnhkbi5p0i03sx5gqiavicvn93g54ywfn5599aa0dpi") (features (quote (("with-decrypt" "jsonwebkey/jwt-convert" "jsonwebtoken") ("default" "with-decrypt") ("_priv_with_serde_json" "serde_json"))))))

