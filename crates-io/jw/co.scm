(define-module (crates-io jw co) #:use-module (crates-io))

(define-public crate-jwconv-0.0.1 (crate (name "jwconv") (vers "0.0.1") (deps (list (crate-dep (name "lazy_static") (req "^0.2.2") (default-features #t) (kind 0)))) (hash "0fr3kvjlfcmic1m6zlbj5pwr31yx4psj5f2zjk99xksbzdhfhp9x")))

(define-public crate-jwconv-cli-0.0.1 (crate (name "jwconv-cli") (vers "0.0.1") (deps (list (crate-dep (name "clap") (req "^2.20.0") (features (quote ("yaml"))) (default-features #t) (kind 0)) (crate-dep (name "jwconv") (req "^0.0.1") (default-features #t) (kind 0)))) (hash "0mpmxyash16lm4sxcp1zfdc5si7ws99z0ksjrwbrxiaaxlsm5vid")))

(define-public crate-jwconv-ffi-0.0.1 (crate (name "jwconv-ffi") (vers "0.0.1") (deps (list (crate-dep (name "jwconv") (req "^0.0.1") (default-features #t) (kind 0)))) (hash "11f5zlhg3k4lmnr7xa8chyzhyhlhc7r2fy0n4mccyfhaldmlb24i")))

