(define-module (crates-io pg c-) #:use-module (crates-io))

(define-public crate-pgc-derive-0.1 (crate (name "pgc-derive") (vers "0.1.0") (deps (list (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "synstructure") (req "^0.12") (default-features #t) (kind 0)))) (hash "1mi5prdf5mafisky6sz6fkhl2m0z2s3rmkklvf0vf1nxggad7w58")))

(define-public crate-pgc-derive-0.1 (crate (name "pgc-derive") (vers "0.1.1") (deps (list (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "synstructure") (req "^0.12") (default-features #t) (kind 0)))) (hash "12k52q8q08hmmz15iyr0f049lmaawwr2cqmz8jv6sinlsmpl9az3")))

