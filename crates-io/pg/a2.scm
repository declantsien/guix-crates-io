(define-module (crates-io pg a2) #:use-module (crates-io))

(define-public crate-pga2d-0.0.1 (crate (name "pga2d") (vers "0.0.1") (hash "1sgzyghfhi3bhyr1677ha4iyycc22sq7sj956mgkiih97jfc5qcs")))

(define-public crate-pga2d-0.0.2 (crate (name "pga2d") (vers "0.0.2") (hash "0kkjrrsk9fzk6lmh242hhd3gckphm87sfb622g0ghd1l9xnj2y5a")))

