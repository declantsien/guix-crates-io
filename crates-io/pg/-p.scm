(define-module (crates-io pg -p) #:use-module (crates-io))

(define-public crate-pg-pool-safe-query-0.1 (crate (name "pg-pool-safe-query") (vers "0.1.0") (deps (list (crate-dep (name "pg_query") (req "^0.7") (default-features #t) (kind 0)))) (hash "0qjp8b8a0g0r5xrbfrg45l5rjag45a5y1rlwi8w7v3sl5yjk64fa")))

