(define-module (crates-io pg _i) #:use-module (crates-io))

(define-public crate-pg_interval-0.0.1 (crate (name "pg_interval") (vers "0.0.1") (deps (list (crate-dep (name "byteorder") (req "^1.2.4") (default-features #t) (kind 0)) (crate-dep (name "postgres") (req "^0.15") (optional #t) (default-features #t) (kind 0)))) (hash "0dam285wpcjvl71fvghx3588f7sxmx8149jgly7fqvrpdfy7wyvm") (features (quote (("default" "postgres"))))))

(define-public crate-pg_interval-0.1 (crate (name "pg_interval") (vers "0.1.0") (deps (list (crate-dep (name "byteorder") (req "^1.2.4") (default-features #t) (kind 0)) (crate-dep (name "postgres") (req "^0.15") (optional #t) (default-features #t) (kind 0)))) (hash "0slarqvhf6glzwpff1b0a1b07az5865aq5vmyi90k3dzzsdzddca") (features (quote (("default" "postgres"))))))

(define-public crate-pg_interval-0.1 (crate (name "pg_interval") (vers "0.1.1") (deps (list (crate-dep (name "byteorder") (req "^1.2.4") (default-features #t) (kind 0)) (crate-dep (name "postgres") (req "^0.15") (optional #t) (default-features #t) (kind 0)))) (hash "18wx0w5zc5v3zbny2k39b1pfgyaszgjyhi723wh106sbsw89f6y5") (features (quote (("default" "postgres"))))))

(define-public crate-pg_interval-0.2 (crate (name "pg_interval") (vers "0.2.0") (deps (list (crate-dep (name "byteorder") (req "^1.2.4") (default-features #t) (kind 0)) (crate-dep (name "postgres") (req "^0.15") (optional #t) (default-features #t) (kind 0)))) (hash "1awiqkvg7wyzz3rmjrjcjja8rwwn0h3g3bhw6mh845djjp7wz05b") (features (quote (("default" "postgres"))))))

(define-public crate-pg_interval-0.3 (crate (name "pg_interval") (vers "0.3.0") (deps (list (crate-dep (name "byteorder") (req "^1.2.4") (default-features #t) (kind 0)) (crate-dep (name "chrono") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "postgres") (req "^0.15") (optional #t) (default-features #t) (kind 0)))) (hash "00jqwhhp165kh4z8xxm5cgr323qrng2hxsb3p22s54ac9c58fig4") (features (quote (("default" "postgres"))))))

(define-public crate-pg_interval-0.4 (crate (name "pg_interval") (vers "0.4.1") (deps (list (crate-dep (name "bytes") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "chrono") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "postgres-types") (req "^0.2") (optional #t) (default-features #t) (kind 0)))) (hash "12mryplx59hfx26pc5y8shs045s5ff9pjyn93kpaamwccnylsda7") (features (quote (("postgres" "postgres-types") ("default" "postgres"))))))

(define-public crate-pg_interval-0.4 (crate (name "pg_interval") (vers "0.4.2") (deps (list (crate-dep (name "bytes") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "chrono") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "postgres-types") (req "^0.2") (optional #t) (default-features #t) (kind 0)))) (hash "1whds37v95l3awjcf46kg0l64ypiivnwnrghiq24na2y8q5n8ipy") (features (quote (("postgres" "postgres-types") ("default" "postgres"))))))

