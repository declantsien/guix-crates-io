(define-module (crates-io pg sq) #:use-module (crates-io))

(define-public crate-pgsql_builder-0.0.1 (crate (name "pgsql_builder") (vers "0.0.1") (deps (list (crate-dep (name "itertools") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "modifier") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "postgres") (req "^0.11") (default-features #t) (kind 0)))) (hash "1ngad1qggc9djvv03dhl2sqcc1q12a9mrh5sx5yzn2a70nh06l9g")))

