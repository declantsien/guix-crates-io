(define-module (crates-io pg ma) #:use-module (crates-io))

(define-public crate-pgmacro-0.1 (crate (name "pgmacro") (vers "0.1.0") (deps (list (crate-dep (name "proc-macro2") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.10") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.5.4") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.80") (default-features #t) (kind 0)))) (hash "0r6l49d635c5hqmdgqs1hwvws3yrqjpckrkh1lsa65qm41z28z2a")))

(define-public crate-pgmacro-0.1 (crate (name "pgmacro") (vers "0.1.1") (deps (list (crate-dep (name "proc-macro2") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.10") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.5.4") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1") (default-features #t) (kind 0)))) (hash "0msdskk0is897gx3qqycmndgzqjhy6xdq8ag75baiqgf6af29m07")))

(define-public crate-pgmacro-0.1 (crate (name "pgmacro") (vers "0.1.2") (deps (list (crate-dep (name "proc-macro2") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.10") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.5.4") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1") (default-features #t) (kind 0)))) (hash "00iqvi871h40bgnhfhg78jc3lgq0qm78jz4a5hkq7xn3177xz2np")))

(define-public crate-pgmacro-0.1 (crate (name "pgmacro") (vers "0.1.3") (deps (list (crate-dep (name "proc-macro2") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.10") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.5.4") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1") (default-features #t) (kind 0)))) (hash "0xfhl91arnbbgp0nzd4r04nn2rh5579h3zp75718n4al2lxpqb6c")))

(define-public crate-pgmacro-0.1 (crate (name "pgmacro") (vers "0.1.4") (deps (list (crate-dep (name "proc-macro2") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.10") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.5.4") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1") (default-features #t) (kind 0)))) (hash "1sm0rfkkd7lc1hl70pa2wdyy2v6qlnvrxzl09snnwdd8qqmyy3xl")))

(define-public crate-pgmacro-0.1 (crate (name "pgmacro") (vers "0.1.5") (deps (list (crate-dep (name "proc-macro2") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.10") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.5.4") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1") (default-features #t) (kind 0)))) (hash "1w0knxfvjyxzy66i5i4j1hk63fhiga3gjn3nd75f5v25admg3253")))

(define-public crate-pgmacro-0.1 (crate (name "pgmacro") (vers "0.1.6") (deps (list (crate-dep (name "proc-macro2") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.10") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.5.4") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1") (default-features #t) (kind 0)))) (hash "02zkrr9fmbjdyqd6fm3hrs8hw9zhxvqf6kmpks7xq8x2hywilddg")))

(define-public crate-pgmacro-0.1 (crate (name "pgmacro") (vers "0.1.7") (deps (list (crate-dep (name "proc-macro2") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.10") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.5.4") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1") (default-features #t) (kind 0)))) (hash "1jqsy034hywhz5xmkzwvraz1ch1ab39f3dz2f6hy8b63xywhzkvv")))

(define-public crate-pgmacro-0.1 (crate (name "pgmacro") (vers "0.1.9") (deps (list (crate-dep (name "proc-macro2") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.10") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1") (default-features #t) (kind 0)))) (hash "1zipxy3phq41s507flymiyn7qsfk8j02a74nqfhlyqli47vayw92")))

(define-public crate-pgmacro-0.1 (crate (name "pgmacro") (vers "0.1.10") (deps (list (crate-dep (name "proc-macro2") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.10") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1") (default-features #t) (kind 0)))) (hash "0gh99rcvy56ml1iv678ci1r56q1rmddasqkk7i1fxg99l8f18dj8")))

(define-public crate-pgmacro-0.1 (crate (name "pgmacro") (vers "0.1.12") (deps (list (crate-dep (name "proc-macro2") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.10") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1") (default-features #t) (kind 0)))) (hash "01184v15jr31df7qv6n1pk8sddwzb4jxsqsi212xlv586z2fjhh0")))

(define-public crate-pgmacro-0.1 (crate (name "pgmacro") (vers "0.1.13") (deps (list (crate-dep (name "proc-macro2") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.10") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1") (default-features #t) (kind 0)))) (hash "135lidc8q9ld3p53vgywpsim1wqgz3wjscidysdlqrllm0njy9x8")))

