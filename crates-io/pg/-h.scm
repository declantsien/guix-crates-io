(define-module (crates-io pg -h) #:use-module (crates-io))

(define-public crate-pg-helper-0.1 (crate (name "pg-helper") (vers "0.1.0") (deps (list (crate-dep (name "postgres-types") (req "^0.2.4") (default-features #t) (kind 0)))) (hash "1ks3gdxplaxa4555rbxhqg6rp7q3bljwzcp7wxgh530awh0wlhv8")))

(define-public crate-pg-helper-0.2 (crate (name "pg-helper") (vers "0.2.0") (deps (list (crate-dep (name "postgres-types") (req "^0.2.4") (default-features #t) (kind 0)))) (hash "02zfjx9wa5b6r55d42p4al0qr706z2kkv07qq0np9ck0wjvcjdak")))

(define-public crate-pg-helper-0.2 (crate (name "pg-helper") (vers "0.2.3") (deps (list (crate-dep (name "postgres-types") (req "^0.2.4") (default-features #t) (kind 0)))) (hash "1n4143ggkcq1b9q3n4dpl07n3gm766w2iq5w8dzaa2fj0wx524ca")))

