(define-module (crates-io pg r2) #:use-module (crates-io))

(define-public crate-pgr2junit-0.1 (crate (name "pgr2junit") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^3") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "patch") (req "^0") (default-features #t) (kind 0)) (crate-dep (name "quick-junit") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "scanf") (req "^1") (default-features #t) (kind 0)))) (hash "14k3dmajk8id7sw76ardqb1f532ka5axfzzhmadm0m65bz63pzkf")))

(define-public crate-pgr2junit-0.0.1 (crate (name "pgr2junit") (vers "0.0.1") (deps (list (crate-dep (name "clap") (req "^3") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "patch") (req "^0") (default-features #t) (kind 0)) (crate-dep (name "quick-junit") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "scanf") (req "^1") (default-features #t) (kind 0)))) (hash "0mn6i1cdkqkbrc60isjvhx9s4f23lw69wlaq7ax40xmbhxrvwp7g")))

(define-public crate-pgr2junit-0.1 (crate (name "pgr2junit") (vers "0.1.1") (deps (list (crate-dep (name "clap") (req "^3") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "patch") (req "^0") (default-features #t) (kind 0)) (crate-dep (name "quick-junit") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "scanf") (req "^1") (default-features #t) (kind 0)))) (hash "03wg480cbdknz6ngzgm6php22y92ch6q0ky89kyg6spb0myx3kvv")))

