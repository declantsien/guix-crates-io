(define-module (crates-io pg _r) #:use-module (crates-io))

(define-public crate-pg_rnd_numeric-1 (crate (name "pg_rnd_numeric") (vers "1.0.0") (deps (list (crate-dep (name "pgx") (req "^0.6.1") (default-features #t) (kind 0)) (crate-dep (name "pgx-tests") (req "^0.6.1") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.8.4") (default-features #t) (kind 0)))) (hash "0ws5djldwwnw7n17ggc1j12rfjw2fy5rmmva0lmimm596khc4qjh") (features (quote (("pg_test") ("pg15" "pgx/pg15" "pgx-tests/pg15") ("pg14" "pgx/pg14" "pgx-tests/pg14") ("pg13" "pgx/pg13" "pgx-tests/pg13") ("default" "pg14"))))))

