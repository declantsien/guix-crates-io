(define-module (crates-io pg ar) #:use-module (crates-io))

(define-public crate-pgarchive-0.0.0 (crate (name "pgarchive") (vers "0.0.0") (deps (list (crate-dep (name "chrono") (req "^0.4.30") (default-features #t) (kind 0)) (crate-dep (name "flate2") (req "^1.0.27") (default-features #t) (kind 0)) (crate-dep (name "hex-literal") (req "^0.4.1") (default-features #t) (kind 2)))) (hash "1v2aghn09nzqx8accc05ndj04cq0dy1bgmk1ci5vqp6cci8j5b84")))

(define-public crate-pgarchive-0.0.1 (crate (name "pgarchive") (vers "0.0.1") (deps (list (crate-dep (name "chrono") (req "^0.4.30") (default-features #t) (kind 0)) (crate-dep (name "flate2") (req "^1.0.27") (default-features #t) (kind 0)) (crate-dep (name "hex-literal") (req "^0.4.1") (default-features #t) (kind 2)))) (hash "06f5wancxhbw8h62agh7qjhwd3isx9ljzqza3nmvlimpj1xzf3ms")))

(define-public crate-pgarchive-0.2 (crate (name "pgarchive") (vers "0.2.0") (deps (list (crate-dep (name "chrono") (req "^0.4.30") (default-features #t) (kind 0)) (crate-dep (name "flate2") (req "^1.0.27") (default-features #t) (kind 0)) (crate-dep (name "hex-literal") (req "^0.4.1") (default-features #t) (kind 2)))) (hash "18i87cn020a13cc09yci2jlw62jyfihbcdqs4z11lidhvkqm7bbr")))

(define-public crate-pgarchive-0.3 (crate (name "pgarchive") (vers "0.3.0") (deps (list (crate-dep (name "chrono") (req "^0.4.30") (default-features #t) (kind 0)) (crate-dep (name "flate2") (req "^1.0.27") (default-features #t) (kind 0)) (crate-dep (name "hex-literal") (req "^0.4.1") (default-features #t) (kind 2)) (crate-dep (name "thiserror") (req "^1.0.49") (default-features #t) (kind 0)))) (hash "0ss9gds5mva4425vxagfj61a4f916rr6ab8fpq1n5bc2sdgvyriv")))

