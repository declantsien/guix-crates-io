(define-module (crates-io pg ru) #:use-module (crates-io))

(define-public crate-pgrustxn-0.0.7 (crate (name "pgrustxn") (vers "0.0.7") (deps (list (crate-dep (name "rustfmt") (req "^0.4") (default-features #t) (kind 2)))) (hash "1rjyfdap4565j35x5dvxca7vysxq91rzjhy485jq7jyqi6j5bic3") (yanked #t)))

(define-public crate-pgrustxn-sys-0.0.7 (crate (name "pgrustxn-sys") (vers "0.0.7") (deps (list (crate-dep (name "rustfmt") (req "^0.4") (default-features #t) (kind 2)))) (hash "11pcfj3ca8yggp9nbfw0sr0gnsck62nq2adzcy00hlcjafxps7ij") (yanked #t)))

(define-public crate-pgrustxn-sys-0.0.8 (crate (name "pgrustxn-sys") (vers "0.0.8") (deps (list (crate-dep (name "rustfmt") (req "^0.4") (default-features #t) (kind 2)))) (hash "0n3l1jvi065d5zsvyclqbyrkkhlxzm6dzp24zx9r5gn1s6waa94d") (yanked #t)))

