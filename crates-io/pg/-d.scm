(define-module (crates-io pg -d) #:use-module (crates-io))

(define-public crate-pg-db-idle-agent-0.0.1 (crate (name "pg-db-idle-agent") (vers "0.0.1") (hash "00i1qhaqhhnn8xf98ydgnjx8rfcm0jip1mkdsycbv9il9kk1f4h2") (yanked #t)))

(define-public crate-pg-db-idle-agent-0.0.2 (crate (name "pg-db-idle-agent") (vers "0.0.2") (hash "0jr651qb7rpnwqpd42ynv4yqx0wagw3fzpwklrmabq9xwdln26ah") (yanked #t)))

(define-public crate-pg-db-idle-agent-0.0.3 (crate (name "pg-db-idle-agent") (vers "0.0.3") (deps (list (crate-dep (name "serial_test") (req "^3.1.1") (default-features #t) (kind 2)) (crate-dep (name "sqlx") (req "^0.7.4") (features (quote ("postgres" "runtime-tokio-rustls"))) (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.35.1") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "0p365c59hnmg4npq7z3xxb3kmgljr05gy679pa6qmrd88zagfgp4") (yanked #t)))

(define-public crate-pg-db-idle-agent-0.0.4 (crate (name "pg-db-idle-agent") (vers "0.0.4") (deps (list (crate-dep (name "serial_test") (req "^3.1.1") (default-features #t) (kind 2)) (crate-dep (name "sqlx") (req "^0.7.4") (features (quote ("postgres" "runtime-tokio-rustls"))) (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.35.1") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "0z5g9fgyrmkx453k5rg7n7yd1a3k22mssidw23vig36kxrkkxy71") (yanked #t)))

(define-public crate-pg-db-idle-agent-async-0.1 (crate (name "pg-db-idle-agent-async") (vers "0.1.0") (deps (list (crate-dep (name "serial_test") (req "^3.1.1") (default-features #t) (kind 2)) (crate-dep (name "sqlx") (req "^0.7.4") (features (quote ("postgres" "runtime-tokio-rustls"))) (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.35.1") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "1anfiyizs12c55icd2zal5d11r6452sqny1wzk54s3fgkrxk9i87")))

