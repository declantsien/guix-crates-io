(define-module (crates-io pg ty) #:use-module (crates-io))

(define-public crate-pgtypes-0.1 (crate (name "pgtypes") (vers "0.1.0") (deps (list (crate-dep (name "bindgen") (req "^0.25.0") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0") (default-features #t) (kind 0)))) (hash "0k1g2cw1g1zm5rxqnbaxmfwg10iigc438qih3bpnn6rysldnpfp9")))

