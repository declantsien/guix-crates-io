(define-module (crates-io pg bu) #:use-module (crates-io))

(define-public crate-pgburst-0.1 (crate (name "pgburst") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^4.4.11") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "colorize") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "notify") (req "^6.1.1") (default-features #t) (kind 0)) (crate-dep (name "postgres") (req "^0.19.7") (default-features #t) (kind 0)))) (hash "0kkmnhbgsd1rdf9gx2w00yccx247k5vz6p2gbwlgqh10gn8z41pv")))

(define-public crate-pgburst-0.1 (crate (name "pgburst") (vers "0.1.2") (deps (list (crate-dep (name "clap") (req "^4.4.11") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "colorize") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "notify") (req "^6.1.1") (default-features #t) (kind 0)) (crate-dep (name "postgres") (req "^0.19.7") (default-features #t) (kind 0)))) (hash "07askmammj1jmh21y4r0cb69ibshfpmznqqvmiy20kv3r2rprxxv")))

(define-public crate-pgburst-0.2 (crate (name "pgburst") (vers "0.2.0") (deps (list (crate-dep (name "clap") (req "^4.4.11") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "colorize") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "notify") (req "^6.1.1") (default-features #t) (kind 0)) (crate-dep (name "postgres") (req "^0.19.7") (default-features #t) (kind 0)))) (hash "0kxf0zjgk8sazsbrwb2pb0c0lmn0bysmwvri1v1x5g194andxx6x")))

(define-public crate-pgburst-0.2 (crate (name "pgburst") (vers "0.2.1") (deps (list (crate-dep (name "clap") (req "^4.4.11") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "colorize") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "notify") (req "^6.1.1") (default-features #t) (kind 0)) (crate-dep (name "postgres") (req "^0.19.7") (default-features #t) (kind 0)))) (hash "13r1xxv52wqadmvw4jnkz68n3996rmnwszv0jnq86fafln8iia8i")))

