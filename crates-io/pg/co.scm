(define-module (crates-io pg co) #:use-module (crates-io))

(define-public crate-pgcopy-0.0.1 (crate (name "pgcopy") (vers "0.0.1") (deps (list (crate-dep (name "byteorder") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "chrono") (req "^0.4") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "uuid") (req "^0.7") (optional #t) (default-features #t) (kind 0)))) (hash "1mah6cf3mv89xwn91fiks3zixi1s7ylf3ng32y3bikjwbznlnj76") (features (quote (("with-uuid" "uuid") ("with-chrono" "chrono") ("default") ("all" "with-uuid" "with-chrono"))))))

(define-public crate-pgcopy-0.0.2 (crate (name "pgcopy") (vers "0.0.2") (deps (list (crate-dep (name "byteorder") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "chrono") (req "^0.4") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "eui48") (req "^0.4") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "uuid") (req "^0.7") (optional #t) (default-features #t) (kind 0)))) (hash "0rpkxi0hnv27pmfs4j1bhn70jyadzhn18gwa5garaxnp9h8jjb96") (features (quote (("with-uuid" "uuid") ("with-eui48" "eui48") ("with-chrono" "chrono") ("default") ("all" "with-uuid" "with-chrono" "with-eui48"))))))

