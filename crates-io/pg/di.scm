(define-module (crates-io pg di) #:use-module (crates-io))

(define-public crate-pgdiff-0.1 (crate (name "pgdiff") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^4.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "derive-deref-rs") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "elephantry") (req "^3.1") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "console") (req "^0.15") (default-features #t) (kind 2)) (crate-dep (name "similar") (req "^2.2") (default-features #t) (kind 2)))) (hash "0w8mcfqsmsdqbm1pgww2607qbrivsybahysby1xzv3n3n79nks8w")))

