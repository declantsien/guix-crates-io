(define-module (crates-io pg da) #:use-module (crates-io))

(define-public crate-pgdatetime-0.1 (crate (name "pgdatetime") (vers "0.1.0") (deps (list (crate-dep (name "criterion") (req "^0.3") (default-features #t) (kind 2)))) (hash "1szrzb9g3vkz64qkdn4z3vvylq5xr23pj8h2azhypswyzdb887ap")))

(define-public crate-pgdatetime-0.2 (crate (name "pgdatetime") (vers "0.2.0") (deps (list (crate-dep (name "bincode") (req "^1.3") (default-features #t) (kind 2)) (crate-dep (name "criterion") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "serde") (req "^1.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 2)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 2)))) (hash "0hwbcbq4qd4s30x0xlmsws3jxwjcmq76rj9rmsha191cfwwpnnaa")))

(define-public crate-pgdatetime-0.3 (crate (name "pgdatetime") (vers "0.3.0") (deps (list (crate-dep (name "bincode") (req "^1.3") (default-features #t) (kind 2)) (crate-dep (name "criterion") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "serde") (req "^1.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 2)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 2)))) (hash "1kpcjyv4igmfwv63mpsgih6ffn8zm6czx2zbmfn1pqjbskxmh8ma")))

