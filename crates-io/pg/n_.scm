(define-module (crates-io pg n_) #:use-module (crates-io))

(define-public crate-pgn_filter-1 (crate (name "pgn_filter") (vers "1.0.0") (deps (list (crate-dep (name "gecliht") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.4.2") (default-features #t) (kind 0)))) (hash "00r14d87p7va9f0y0dpx295gxb1f4vi8k6lzadm1dln132747ax4")))

(define-public crate-pgn_filter-1 (crate (name "pgn_filter") (vers "1.1.0") (deps (list (crate-dep (name "regex") (req "^1.4.2") (default-features #t) (kind 0)) (crate-dep (name "textwrap") (req "^0.16.0") (default-features #t) (kind 0)))) (hash "1jn4yaqpfl0cfvql4gc21x1mwg7iklbh552268a05446hlk8dq40")))

(define-public crate-pgn_parser-0.2 (crate (name "pgn_parser") (vers "0.2.1") (deps (list (crate-dep (name "anyhow") (req "^1.0.75") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^4.4.8") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "pest") (req "^2.7.5") (default-features #t) (kind 0)) (crate-dep (name "pest_derive") (req "^2.7.5") (default-features #t) (kind 0)) (crate-dep (name "pest_generator") (req "^2.7.5") (default-features #t) (kind 0)))) (hash "18rnw2cdjysmf59n548cpzqadaldnbj35213dcf2irrlpb7qk56j")))

(define-public crate-pgn_parser-0.2 (crate (name "pgn_parser") (vers "0.2.2") (deps (list (crate-dep (name "anyhow") (req "^1.0.75") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^4.4.8") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "pest") (req "^2.7.5") (default-features #t) (kind 0)) (crate-dep (name "pest_derive") (req "^2.7.5") (default-features #t) (kind 0)) (crate-dep (name "pest_generator") (req "^2.7.5") (default-features #t) (kind 0)))) (hash "1d5xzgdkwphgqjqzzdqyi3r3kh5rwygcb595djlz3fk7ivvyzm62")))

