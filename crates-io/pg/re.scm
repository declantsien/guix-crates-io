(define-module (crates-io pg re) #:use-module (crates-io))

(define-public crate-pgrep-0.1 (crate (name "pgrep") (vers "0.1.0") (deps (list (crate-dep (name "colored") (req "^2.0.0") (default-features #t) (kind 0)))) (hash "1d1crv7zgrs5w3a7ldifp9ss1w0m6qramanbgfrzzaldjdr9i1mi") (yanked #t)))

