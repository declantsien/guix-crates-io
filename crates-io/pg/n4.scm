(define-module (crates-io pg n4) #:use-module (crates-io))

(define-public crate-pgn4-0.1 (crate (name "pgn4") (vers "0.1.0") (deps (list (crate-dep (name "thiserror") (req "^1.0") (default-features #t) (kind 0)))) (hash "0iwv96g2j9hnsayyzkrc63prwr0zmqciry3k8nqlh51asjvrc2qg")))

(define-public crate-pgn4-0.2 (crate (name "pgn4") (vers "0.2.0") (deps (list (crate-dep (name "fen4") (req "^0.4.0") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0") (default-features #t) (kind 0)))) (hash "14vjf7hjyk9dp7w18c30ppg97dn2l9smblzzlqv9nsrxwzjihlq0")))

(define-public crate-pgn4-0.3 (crate (name "pgn4") (vers "0.3.0") (deps (list (crate-dep (name "fen4") (req "^0.5.0") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0") (default-features #t) (kind 0)))) (hash "07zscrq7vjm1mmw6xb4ik9c01kb000pnyxn4hljh3c13wgm45nvr")))

(define-public crate-pgn4-0.3 (crate (name "pgn4") (vers "0.3.1") (deps (list (crate-dep (name "fen4") (req ">=0.4, <0.6") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0") (default-features #t) (kind 0)))) (hash "0jfkh8yyvk7wljbwjdbrsxg6y388ppb3rw32wbz9g2vha0ls8lay")))

(define-public crate-pgn4-0.3 (crate (name "pgn4") (vers "0.3.2") (deps (list (crate-dep (name "fen4") (req ">=0.4, <0.7") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0") (default-features #t) (kind 0)))) (hash "1jvs89nhkldar4wwwlfpm1bz89gzpgbys4n96ypf0iiwbrirwnjk")))

(define-public crate-pgn4-0.3 (crate (name "pgn4") (vers "0.3.3") (deps (list (crate-dep (name "fen4") (req ">=0.4, <0.8") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0") (default-features #t) (kind 0)))) (hash "0812yxqmggn1mvxlyg20zrzppb1280jji6hndmmwlr6cv8zbqf8k")))

