(define-module (crates-io pg en) #:use-module (crates-io))

(define-public crate-pgen-0.2 (crate (name "pgen") (vers "0.2.1") (deps (list (crate-dep (name "clap") (req "~2.29.1") (features (quote ("yaml"))) (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.4") (default-features #t) (kind 0)))) (hash "1483g8pf9ihl1bmmq2n5y1xs1vp0xwkg9hr0wrb1y5f7irq5mki8")))

(define-public crate-pgen-0.2 (crate (name "pgen") (vers "0.2.2") (deps (list (crate-dep (name "clap") (req "~2.29.1") (features (quote ("yaml"))) (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.4") (default-features #t) (kind 0)))) (hash "0rwnyifd5hbyr245p8aw1n90zr4yhrikxyi0kp4kwlc7jlgajwzg")))

(define-public crate-pgen-0.2 (crate (name "pgen") (vers "0.2.3") (deps (list (crate-dep (name "clap") (req "~2.29.1") (features (quote ("yaml"))) (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.4") (default-features #t) (kind 0)))) (hash "1ymi0v3xnm8ksz5z3pbl7vy70h7y04xcxzvphwx1c8c4zhhgydbz")))

(define-public crate-pgen-0.2 (crate (name "pgen") (vers "0.2.4") (deps (list (crate-dep (name "clap") (req "~2.29.1") (features (quote ("yaml"))) (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.4") (default-features #t) (kind 0)))) (hash "089mjfz097nk177xq43fhknbq4wydnd1qq6nq07zavya2i8i4zv4")))

(define-public crate-pgen-0.2 (crate (name "pgen") (vers "0.2.5") (deps (list (crate-dep (name "clap") (req "~2.29.1") (features (quote ("yaml"))) (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.4") (default-features #t) (kind 0)))) (hash "0qvrj26yz17xdv3iv6pwzhh6sg7a4qyaglvyqdpc3macgmv889bn")))

(define-public crate-pgen-0.3 (crate (name "pgen") (vers "0.3.0") (deps (list (crate-dep (name "clap") (req "~2.29.1") (features (quote ("yaml"))) (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.4") (default-features #t) (kind 0)))) (hash "1gk4gcz2q93mqrkiz0kwg6qyw6ax0rq2p8b9ziq97fsx8i4gs6xn")))

(define-public crate-pgen-0.4 (crate (name "pgen") (vers "0.4.0") (deps (list (crate-dep (name "clap") (req "~2.29.1") (features (quote ("yaml"))) (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.4") (default-features #t) (kind 0)))) (hash "0l41q64a6l8xslxyk4g3mqbz3l38z8m81bqpd6afaarc012j4mw3")))

(define-public crate-pgen-1 (crate (name "pgen") (vers "1.0.0") (deps (list (crate-dep (name "clap") (req "~2.29.1") (features (quote ("yaml"))) (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.4") (default-features #t) (kind 0)))) (hash "1icwjbwmwc2kwrjwswfgy35axqh2a8m72abxd7m24w6m0a044nz8")))

(define-public crate-pgen-1 (crate (name "pgen") (vers "1.0.1") (deps (list (crate-dep (name "clap") (req "~2.29.1") (features (quote ("yaml"))) (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.4") (default-features #t) (kind 0)))) (hash "0rg6dxnrm90cyw8dzf0xp8kflqiyr64yhmwpkf8zxgkjya6yir63")))

(define-public crate-pgen-1 (crate (name "pgen") (vers "1.0.2") (deps (list (crate-dep (name "clap") (req "~2.29.1") (features (quote ("yaml"))) (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.4") (default-features #t) (kind 0)))) (hash "0klwzf5a8cdl8a91cmvb6ipl8kflv31n9n3hz8hylljp8ly88lv0")))

(define-public crate-pgen-1 (crate (name "pgen") (vers "1.0.3") (deps (list (crate-dep (name "clap") (req "~2.29.1") (features (quote ("yaml"))) (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.4") (default-features #t) (kind 0)))) (hash "1m149bxffz2pv11f5py6f28gkv173v2p8w3mzfn3dw6km0zvp8f1")))

(define-public crate-pgen-1 (crate (name "pgen") (vers "1.0.4") (deps (list (crate-dep (name "clap") (req "~2.29.1") (features (quote ("yaml"))) (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.4") (default-features #t) (kind 0)))) (hash "1mnw2yzv433331m5adrllvk4h4bb71skksx17kl74q2wqbawbwf4")))

(define-public crate-pgen-1 (crate (name "pgen") (vers "1.0.5") (deps (list (crate-dep (name "clap") (req "^2.33.0") (features (quote ("yaml"))) (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.6.5") (default-features #t) (kind 0)))) (hash "1hn3xmy955wczh9sq30c4npf31234dqisvlg3jrafwqhz6dg9pk1")))

(define-public crate-pgen-1 (crate (name "pgen") (vers "1.1.0") (deps (list (crate-dep (name "clap") (req "^2.33.0") (features (quote ("yaml"))) (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.7.2") (default-features #t) (kind 0)))) (hash "0zl1ak8q7s2d0vhxqwsmgvjbqm6118m02z8agyc3659w0hdjgg9w")))

(define-public crate-pgen-1 (crate (name "pgen") (vers "1.1.1") (deps (list (crate-dep (name "clap") (req "^2.33.0") (features (quote ("yaml"))) (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.7.2") (default-features #t) (kind 0)))) (hash "0c8cqljx95pp2d47xni824b4ph4m4dngw2ahkn2dvmrzmd7vji7c")))

(define-public crate-pgen-1 (crate (name "pgen") (vers "1.1.2") (deps (list (crate-dep (name "clap") (req "^2.33.0") (features (quote ("yaml"))) (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.7.2") (default-features #t) (kind 0)))) (hash "0pgc26isqi71p1ih94vbnhj910lsyg247dsmkyvykv0kkcax5m51")))

(define-public crate-pgen-1 (crate (name "pgen") (vers "1.1.3") (deps (list (crate-dep (name "clap") (req "^2.33.0") (features (quote ("yaml"))) (kind 0)) (crate-dep (name "rand") (req "^0.7.2") (default-features #t) (kind 0)))) (hash "1xf788zjbj4s8s0awmsk8z56ll119cmymn2md5y4gfixvqd3zyv2")))

(define-public crate-pgen-1 (crate (name "pgen") (vers "1.1.4") (deps (list (crate-dep (name "clap") (req "^4.1") (features (quote ("std" "derive" "help" "usage" "error-context"))) (kind 0)) (crate-dep (name "rand") (req "^0.8") (default-features #t) (kind 0)))) (hash "106h49vqvbn7jmh95ryifwwwf17sj9zc1pjy1yvx0xv6fsirhcxw")))

(define-public crate-pgen-1 (crate (name "pgen") (vers "1.2.0") (deps (list (crate-dep (name "clap") (req "^4.1") (features (quote ("std" "derive" "help" "usage" "error-context"))) (kind 0)) (crate-dep (name "rand") (req "^0.8") (default-features #t) (kind 0)))) (hash "1zqwvvdyf8hdaw1vwbqkqcprrvb3p8k2c787iwzf7jjch6sggdhp")))

(define-public crate-pgen-1 (crate (name "pgen") (vers "1.3.0") (deps (list (crate-dep (name "clap") (req "^4.1") (features (quote ("std" "derive" "help" "usage" "error-context"))) (kind 0)) (crate-dep (name "rand") (req "^0.8") (default-features #t) (kind 0)))) (hash "1p7xzp11qv01yq8d4kw59s1iy985ajkn7css8xsxd1jaq2jl533s")))

(define-public crate-pgen-2 (crate (name "pgen") (vers "2.0.0-alpha.1") (deps (list (crate-dep (name "bip39-lexical-data") (req "^1.0.0-rc.1") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^4.5.4") (features (quote ("std" "derive" "help" "usage" "error-context"))) (kind 0)) (crate-dep (name "eff-lexical-data") (req "^1.0.0-rc.1") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)))) (hash "10wa1fimrn7gjlr6idw40a26liy29b3x1868f9fpzlwnkv71y01j")))

(define-public crate-pgen-2 (crate (name "pgen") (vers "2.0.0") (deps (list (crate-dep (name "bip39-lexical-data") (req "^1.0.0") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^4.5.4") (features (quote ("std" "derive" "help" "usage" "error-context"))) (kind 0)) (crate-dep (name "eff-lexical-data") (req "^1.0.0") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)))) (hash "1qyjlvzqxgkkzsz3qjclb60ls60nywvwq2vxz020lb2rndjkbgq2")))

