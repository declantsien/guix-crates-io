(define-module (crates-io pg id) #:use-module (crates-io))

(define-public crate-pgident-0.0.1 (crate (name "pgident") (vers "0.0.1") (hash "1wpph6rnd1052irqv4d7nwxnka6lv44nlkyvc006wcrmsrw9g8w7")))

(define-public crate-pgident-0.0.2 (crate (name "pgident") (vers "0.0.2") (deps (list (crate-dep (name "thiserror") (req "^1.0") (default-features #t) (kind 0)))) (hash "12m3626llmgjk115h2k8xxhlalgrczz9p1plkx6xs0l07gdp8ml9")))

(define-public crate-pgident-0.0.3 (crate (name "pgident") (vers "0.0.3") (deps (list (crate-dep (name "thiserror") (req "^1.0") (default-features #t) (kind 0)))) (hash "08cbdznhj5jn84ag4k8jrnjr6i3swnxi044c6a4bac1s6qrgjyz0")))

