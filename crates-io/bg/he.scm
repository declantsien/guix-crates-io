(define-module (crates-io bg he) #:use-module (crates-io))

(define-public crate-bghelper-2 (crate (name "bghelper") (vers "2.0.0") (deps (list (crate-dep (name "clap") (req "^4.1.8") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "colored") (req "^2.0.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.7.1") (default-features #t) (kind 0)))) (hash "0iavgyhcwy394n9hc01077gmlgkx40jklmy6my8lf6bhmxqv9bhl")))

