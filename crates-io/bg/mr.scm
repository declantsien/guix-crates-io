(define-module (crates-io bg mr) #:use-module (crates-io))

(define-public crate-bgmrank-cli-0.1 (crate (name "bgmrank-cli") (vers "0.1.0") (deps (list (crate-dep (name "enum-set") (req "^0.0.6") (default-features #t) (kind 0)) (crate-dep (name "getopts") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "libbgmrank") (req "^0.0.1") (default-features #t) (kind 0)))) (hash "1i8i6g5q5zddmb48xr8yi0v2gkyv6lrbb9a9gjw6rbd40zrnm0sq") (yanked #t)))

