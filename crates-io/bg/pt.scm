(define-module (crates-io bg pt) #:use-module (crates-io))

(define-public crate-bgptools-0.0.1 (crate (name "bgptools") (vers "0.0.1") (hash "0fh9c3yxggxqyd0fclbh3qwpczll0337liwx1w9bsgivkhhav04s")))

(define-public crate-bgptools-0.0.2 (crate (name "bgptools") (vers "0.0.2") (deps (list (crate-dep (name "structopt") (req "^0.3") (default-features #t) (kind 0)))) (hash "162rw2alhirwfw5ql2r4q7h186jykr303vrz094w40lnw2513zz8")))

(define-public crate-bgptools-0.0.3 (crate (name "bgptools") (vers "0.0.3") (deps (list (crate-dep (name "structopt") (req "^0.3") (default-features #t) (kind 0)))) (hash "0vdsb88dnvlmndsn1m0sj6m8cdmlnzi46in03zql2hyhj52yj6qj")))

(define-public crate-bgptools-0.1 (crate (name "bgptools") (vers "0.1.0") (deps (list (crate-dep (name "mrt") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.3") (default-features #t) (kind 0)))) (hash "1p1g2bgz0fgr738chba26ivx45scnpi8nj5vx7m3q10dpkx8k3bp")))

(define-public crate-bgptools-0.1 (crate (name "bgptools") (vers "0.1.1") (deps (list (crate-dep (name "mrt") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.3") (default-features #t) (kind 0)))) (hash "17zxaqsxvgpmgnq2l08rdrnn39s98mar57sfcyrklzgwm20pxsbb")))

