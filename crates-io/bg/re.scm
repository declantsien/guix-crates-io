(define-module (crates-io bg re) #:use-module (crates-io))

(define-public crate-bgrep-0.1 (crate (name "bgrep") (vers "0.1.0") (deps (list (crate-dep (name "getopts") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)))) (hash "1zf0n7kr7wp9rkvcplcwwdfz3ixcx6ci4jv19ykm6cjwlqfwi08n")))

(define-public crate-bgrep-1 (crate (name "bgrep") (vers "1.0.0") (deps (list (crate-dep (name "clap") (req "^2.32") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)))) (hash "0mpxlzrvsyx2gss3b3cfbi9j7hgz2rs0c7xdyy9pdh2ygal6yina")))

