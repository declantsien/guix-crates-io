(define-module (crates-io bg zf) #:use-module (crates-io))

(define-public crate-bgzf-0.2 (crate (name "bgzf") (vers "0.2.0") (deps (list (crate-dep (name "byteorder") (req "^1.4.3") (default-features #t) (kind 0)) (crate-dep (name "bytes") (req "^1.1.0") (default-features #t) (kind 0)) (crate-dep (name "libdeflater") (req "^0.7.5") (default-features #t) (kind 0)) (crate-dep (name "proptest") (req "^1.0.0") (default-features #t) (kind 2)) (crate-dep (name "tempfile") (req "^3.2.0") (default-features #t) (kind 2)) (crate-dep (name "thiserror") (req "^1.0.30") (default-features #t) (kind 0)))) (hash "16j8anmfnk1bbwimwcggdq11qaynmc6g9zb9kjb95b0hnnnlj23r")))

(define-public crate-bgzf_rust_reader-0.1 (crate (name "bgzf_rust_reader") (vers "0.1.0") (deps (list (crate-dep (name "libdeflater") (req "^0.7.3") (default-features #t) (kind 0)) (crate-dep (name "positioned-io") (req "^0.2.2") (default-features #t) (kind 0)))) (hash "0220q94vqqi9396sghxc58fdggfprm1q891zsxk4ql78pbh8r7gb")))

(define-public crate-bgzf_rust_reader-0.2 (crate (name "bgzf_rust_reader") (vers "0.2.0") (deps (list (crate-dep (name "libdeflater") (req "^0.7.3") (default-features #t) (kind 0)) (crate-dep (name "positioned-io") (req "^0.2.2") (default-features #t) (kind 0)))) (hash "16k87wvxc7lpfpf0lv24c56120j77s8zia7h92365mpszx7vjrh2")))

(define-public crate-bgzf_rust_reader-0.2 (crate (name "bgzf_rust_reader") (vers "0.2.1") (deps (list (crate-dep (name "libdeflater") (req "^0.7.3") (default-features #t) (kind 0)) (crate-dep (name "positioned-io") (req "^0.2.2") (default-features #t) (kind 0)))) (hash "17vqrgrdfa1a15vdzq53jd59fcmmwjg0lxj53ks5b2835n63bj43")))

