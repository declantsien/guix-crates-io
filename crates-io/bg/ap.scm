(define-module (crates-io bg ap) #:use-module (crates-io))

(define-public crate-bgapi-0.0.0 (crate (name "bgapi") (vers "0.0.0") (hash "0npjzrv0vw6ppbqfq3qcywhbjynvs5jx29gzhc0lcz9143c41rg8")))

(define-public crate-bgapi-0.0.1 (crate (name "bgapi") (vers "0.0.1") (deps (list (crate-dep (name "bytes") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "num-derive") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2") (default-features #t) (kind 0)))) (hash "10kciivq9b93wzks82ppss7m816ns7kx5c66vkjsd81pfmplsaap")))

(define-public crate-bgapi-0.0.2 (crate (name "bgapi") (vers "0.0.2") (deps (list (crate-dep (name "bytes") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "num-derive") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "spmc") (req "^0.2.2") (default-features #t) (kind 0)))) (hash "11w6zgi3swfcpdb8dm4naw8wm9zxjirplbnlnbzg5qdjjni0rm2a")))

(define-public crate-bgapi-0.0.3 (crate (name "bgapi") (vers "0.0.3") (deps (list (crate-dep (name "bytes") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "num-derive") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "spmc") (req "^0.2.2") (default-features #t) (kind 0)))) (hash "16y0k92a8d1y6x9iy6vmwai413lqws2rnqfamg0zx8j1fmwyvx89")))

(define-public crate-bgapi-0.0.4 (crate (name "bgapi") (vers "0.0.4") (deps (list (crate-dep (name "bytes") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "num-derive") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "spmc") (req "^0.2.2") (default-features #t) (kind 0)))) (hash "10kc46k304giqpyrw8fzrjh873dz1y4h3h5977295qpxdps360wf")))

(define-public crate-bgapi-0.0.5 (crate (name "bgapi") (vers "0.0.5") (deps (list (crate-dep (name "bytes") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "num-derive") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "spmc") (req "^0.2.2") (default-features #t) (kind 0)))) (hash "0clrp1ya7vg04cm3lkqgnixv30pi3ak6ri80glk2dy9b2gp9g2fz")))

(define-public crate-bgapi-0.0.6 (crate (name "bgapi") (vers "0.0.6") (deps (list (crate-dep (name "bytes") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "num-derive") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "spmc") (req "^0.2.2") (default-features #t) (kind 0)))) (hash "0vnhd9rwc1jxm9w08nkrdnbmwydcaifg148l3bwlxb7g9vlrvnc7")))

(define-public crate-bgapi-0.0.7 (crate (name "bgapi") (vers "0.0.7") (deps (list (crate-dep (name "bytes") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "num-derive") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "spmc") (req "^0.2.2") (default-features #t) (kind 0)))) (hash "00q6p21w7hcjx241f9j7xs4dqk1alrvs0v18vd3iw4fyfp5cnj70")))

(define-public crate-bgapi-0.0.8 (crate (name "bgapi") (vers "0.0.8") (deps (list (crate-dep (name "bytes") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "num-derive") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "spmc") (req "^0.2.2") (default-features #t) (kind 0)))) (hash "0jr4j4lm6jaglg847px5449ywyq7ffi3wxsvbvibgn51vm6nbd51")))

(define-public crate-bgapi-0.0.9 (crate (name "bgapi") (vers "0.0.9") (deps (list (crate-dep (name "bytes") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "num-derive") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "spmc") (req "^0.2.2") (default-features #t) (kind 0)))) (hash "0a8h7xpf7nld4c2kqll93mrq02sw9z0kvqqxz4wra77wksjgjqxp")))

(define-public crate-bgapi-0.0.10 (crate (name "bgapi") (vers "0.0.10") (deps (list (crate-dep (name "bytes") (req "^0.4.12") (default-features #t) (kind 0)) (crate-dep (name "num-derive") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "spmc") (req "^0.2.2") (default-features #t) (kind 0)))) (hash "02mx7z32l2pi4f257i3v2xrjxnih9g96fgb4kn5dq7qn8ldlpv43")))

