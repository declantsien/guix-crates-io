(define-module (crates-io bg jk) #:use-module (crates-io))

(define-public crate-bgjk-0.1 (crate (name "bgjk") (vers "0.1.0") (deps (list (crate-dep (name "clippy") (req "^0.0") (optional #t) (default-features #t) (kind 0)))) (hash "0rg4045bkgrzqggdrnkfm3z5a2p0m7pbi9cl4b5mkjjkaf6n7b67") (features (quote (("dev" "clippy") ("default"))))))

(define-public crate-bgjk-1 (crate (name "bgjk") (vers "1.0.0") (deps (list (crate-dep (name "clippy") (req "^0.0") (optional #t) (default-features #t) (kind 0)))) (hash "02wfrc5fhli4cnyvqm3si20xnmnl26a9pq09xlja1jgnpxqkgryw") (features (quote (("dev" "clippy") ("default"))))))

(define-public crate-bgjk-1 (crate (name "bgjk") (vers "1.0.1") (deps (list (crate-dep (name "clippy") (req "^0.0") (optional #t) (default-features #t) (kind 0)))) (hash "1jg78jjgvkfvcml004r1c7xzyd8i4qjlpqpa59vwd0p93lg308q4") (features (quote (("dev" "clippy") ("default"))))))

