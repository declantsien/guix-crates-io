(define-module (crates-io kr af) #:use-module (crates-io))

(define-public crate-kraft-0.0.0 (crate (name "kraft") (vers "0.0.0") (hash "13502qxcyzqh96dlp7d0gprfs0d8ii38jhh63j4901b23q5zrdmw")))

(define-public crate-krafter-0.0.0 (crate (name "krafter") (vers "0.0.0") (hash "14chzw7p6raa0cgfslbh4khmb152dq599rvn3cwh796a9v4fy3d8") (yanked #t)))

(define-public crate-krafter-0.0.0 (crate (name "krafter") (vers "0.0.0-ph") (hash "184zl72zxbk2fffhysrvasc2g8vc33pyfj9z3rd160iy0jf3vszd") (yanked #t)))

(define-public crate-kraftwerk-0.0.0 (crate (name "kraftwerk") (vers "0.0.0") (hash "06zdlqmlh1ns4lxy5mrrasbf8iakli986q72jrmbdk97ji5x1s89") (yanked #t)))

