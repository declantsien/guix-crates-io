(define-module (crates-io kr ad) #:use-module (crates-io))

(define-public crate-kradical_converter-0.1 (crate (name "kradical_converter") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^3.0.0-beta.2") (default-features #t) (kind 0)) (crate-dep (name "kradical_parsing") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1") (default-features #t) (kind 0)))) (hash "02wn98a0r86mr1hmgk2d9q4b7w9cps105chq9116ihn46c2iyzx4")))

(define-public crate-kradical_jis-0.1 (crate (name "kradical_jis") (vers "0.1.0") (hash "06cc83jmhzx3788gf4b2xzjyz6143ccff48x6xcycamfjx6xxqq2")))

(define-public crate-kradical_parsing-0.1 (crate (name "kradical_parsing") (vers "0.1.0") (deps (list (crate-dep (name "encoding") (req "^0") (default-features #t) (kind 0)) (crate-dep (name "kradical_jis") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "nom") (req "^6") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "unicode-segmentation") (req "^1") (default-features #t) (kind 0)))) (hash "11q0dcix2817m4y5iinpiz2h0sp54ii0bxd09fp8yj0sayz2vrnp")))

(define-public crate-kradical_static-0.1 (crate (name "kradical_static") (vers "0.1.0") (hash "0rnkbszgvnzy203pj855787bg5wh1q58srr23b70469c71k712a5")))

(define-public crate-kradical_static-0.2 (crate (name "kradical_static") (vers "0.2.0") (hash "1xp9jdlglv6rlqz3wv7gjhx2jifriqady7x4fg223r6gak30xlcc")))

