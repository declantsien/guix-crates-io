(define-module (crates-io kr ip) #:use-module (crates-io))

(define-public crate-kripher-0.4 (crate (name "kripher") (vers "0.4.2") (hash "0i2id1mvr7vbcashqz1zbqhii5r2x99zbgz9qg03wx527my4njl1")))

(define-public crate-kripher-0.6 (crate (name "kripher") (vers "0.6.0") (hash "0kkzkn507bx8k7mxbqp2lrkw0ahda2qs3gxi5mbnjxppg3ggcj8a")))

(define-public crate-kripher-0.6 (crate (name "kripher") (vers "0.6.1") (hash "0yq7s9jshwhdji7xbdn769pfpkjf9c7jgdwvzznx7d40ka56p4wa")))

(define-public crate-kripher-0.8 (crate (name "kripher") (vers "0.8.0") (hash "13mjlxbq7fx5cxn2v2vw511xf26ck82kzj4z33s1imj4ssyh7fh7")))

(define-public crate-kripke-machine-0.1 (crate (name "kripke-machine") (vers "0.1.0") (hash "1wfhg0d4626mw2bhw6lg21ziq150kdpw1js67kflsx1cap33ks34")))

