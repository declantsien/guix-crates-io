(define-module (crates-io kr h_) #:use-module (crates-io))

(define-public crate-krh_earcut-0.1 (crate (name "krh_earcut") (vers "0.1.0") (deps (list (crate-dep (name "glam") (req "^0.27.0") (kind 0)) (crate-dep (name "macroquad") (req "^0.4.5") (default-features #t) (kind 2)))) (hash "0ak5iab95m6r4qxs7lfypgs1gnrrchkcm0pzrqd5wjqm5rfyxdgx") (features (quote (("std" "glam/std") ("libm" "glam/libm") ("default" "std"))))))

