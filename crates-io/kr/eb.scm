(define-module (crates-io kr eb) #:use-module (crates-io))

(define-public crate-krebs-0.1 (crate (name "krebs") (vers "0.1.0") (deps (list (crate-dep (name "futures") (req "^0.1.14") (default-features #t) (kind 0)) (crate-dep (name "hyper") (req "^0.11.2") (default-features #t) (kind 0)))) (hash "0nnkdklfldjymzxlmh80p0cmydd0ak4fngqlc31fccxb6hl46q38")))

