(define-module (crates-io kr ed) #:use-module (crates-io))

(define-public crate-kreddit-0.0.0 (crate (name "kreddit") (vers "0.0.0") (hash "04iiyy74x1d141g04w3d2iv3d4585z9v0xgkq9yrjnxnjx414g7i")))

(define-public crate-kredis-0.1 (crate (name "kredis") (vers "0.1.0") (deps (list (crate-dep (name "anyhow") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "kresp") (req "^0.1.1") (default-features #t) (kind 0)) (crate-dep (name "multistream") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.17.0") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "1yg421wbsnb69qcdwfazxaghaaka44wmwzynxv2hrlbc84fclc2j")))

(define-public crate-kredo-0.0.0 (crate (name "kredo") (vers "0.0.0") (hash "1xcddwax9xq8lgxx13yyaaxi5kkym0v3yx2bzyj2aig4q08whw07")))

