(define-module (crates-io kr b5) #:use-module (crates-io))

(define-public crate-krb5-src-0.1 (crate (name "krb5-src") (vers "0.1.0+1.18.1") (deps (list (crate-dep (name "duct") (req "^0.13.3") (default-features #t) (kind 1)) (crate-dep (name "openssl-sys") (req "^0.9.55") (default-features #t) (kind 0)))) (hash "0vd8d3x42q0xjghn5fmriqn4bs4c2lb9yix1dh7zgzd19dxziqj6") (features (quote (("openssl-vendored" "openssl-sys/vendored"))))))

(define-public crate-krb5-src-0.1 (crate (name "krb5-src") (vers "0.1.1+1.18.1") (deps (list (crate-dep (name "duct") (req "^0.13.3") (default-features #t) (kind 1)) (crate-dep (name "openssl-sys") (req "^0.9.55") (default-features #t) (kind 0)))) (hash "1dzf4h223j8bgqdjl7097722frc178ph44g3qk0skwv8a3flv151") (features (quote (("openssl-vendored" "openssl-sys/vendored"))))))

(define-public crate-krb5-src-0.2 (crate (name "krb5-src") (vers "0.2.0+1.18.1") (deps (list (crate-dep (name "duct") (req "^0.13.3") (default-features #t) (kind 1)) (crate-dep (name "openssl-sys") (req "^0.9.55") (default-features #t) (kind 0)))) (hash "1f4ka0kzzdyqrars423vq95rnsz5vhsj6aj0s2v7wzji38988j97") (features (quote (("openssl-vendored" "openssl-sys/vendored")))) (links "krb5-src")))

(define-public crate-krb5-src-0.2 (crate (name "krb5-src") (vers "0.2.1+1.18.1") (deps (list (crate-dep (name "duct") (req "^0.13.3") (default-features #t) (kind 1)) (crate-dep (name "openssl-sys") (req "^0.9.55") (default-features #t) (kind 0)))) (hash "1a5l2fvlx4jm5wgzydjiip3cl5gaqrimjhr21w1jp1dpd6r9y7lj") (features (quote (("openssl-vendored" "openssl-sys/vendored") ("binaries")))) (links "krb5-src")))

(define-public crate-krb5-src-0.2 (crate (name "krb5-src") (vers "0.2.2+1.18.1") (deps (list (crate-dep (name "duct") (req "^0.13.3") (default-features #t) (kind 1)) (crate-dep (name "openssl-sys") (req "^0.9.55") (default-features #t) (kind 0)))) (hash "0sq5iynlpdr9xyb7kr1l2saxcivvlpcxnas244sgw8jpf3rwj4m6") (features (quote (("openssl-vendored" "openssl-sys/vendored") ("binaries")))) (links "krb5-src")))

(define-public crate-krb5-src-0.2 (crate (name "krb5-src") (vers "0.2.3+1.18.2") (deps (list (crate-dep (name "duct") (req "^0.13.3") (default-features #t) (kind 1)) (crate-dep (name "openssl-sys") (req "^0.9.55") (default-features #t) (kind 0)))) (hash "08lndpb47693qfshysy06d8vskp0w5ig1ix4pafyz4kc9x0f7i8w") (features (quote (("openssl-vendored" "openssl-sys/vendored") ("binaries")))) (links "krb5-src")))

(define-public crate-krb5-src-0.2 (crate (name "krb5-src") (vers "0.2.4+1.18.2") (deps (list (crate-dep (name "duct") (req "^0.13.3") (default-features #t) (kind 1)) (crate-dep (name "openssl-sys") (req "^0.9.55") (default-features #t) (kind 0)))) (hash "109d8rkv13slm26iw981jgda1a2kc0xzrj5gdmj43jkh7dy2n750") (features (quote (("openssl-vendored" "openssl-sys/vendored") ("nls") ("binaries")))) (links "krb5-src")))

(define-public crate-krb5-src-0.3 (crate (name "krb5-src") (vers "0.3.0+1.19.2") (deps (list (crate-dep (name "duct") (req "^0.13.3") (default-features #t) (kind 1)) (crate-dep (name "openssl-sys") (req "^0.9.55") (default-features #t) (kind 0)))) (hash "1df9x9r9lwbc7pcczzm5mps2f6c5gvb5vsqmqwwybgv1rvb1b805") (features (quote (("openssl-vendored" "openssl-sys/vendored") ("nls") ("binaries")))) (links "krb5-src")))

(define-public crate-krb5-src-0.3 (crate (name "krb5-src") (vers "0.3.1+1.19.2") (deps (list (crate-dep (name "duct") (req "^0.13.3") (default-features #t) (kind 1)) (crate-dep (name "openssl-sys") (req "^0.9.55") (default-features #t) (kind 0)))) (hash "1jpa84y3zyx23mlhx6spl1s8kqs8n2rjircvsj2xhwn2fpk5pzjp") (features (quote (("openssl-vendored" "openssl-sys/vendored") ("nls") ("binaries")))) (links "krb5-src")))

(define-public crate-krb5-src-0.3 (crate (name "krb5-src") (vers "0.3.2+1.19.2") (deps (list (crate-dep (name "duct") (req "^0.13.3") (default-features #t) (kind 1)) (crate-dep (name "openssl-sys") (req "^0.9.55") (optional #t) (default-features #t) (kind 0)))) (hash "1ys8ksxjm5w93q4bqfzns1xp9szjjh908drqg71qpm1mfxz3pka4") (features (quote (("openssl-vendored" "openssl-sys/vendored") ("nls") ("binaries")))) (links "krb5-src")))

(define-public crate-krb5-src-0.3 (crate (name "krb5-src") (vers "0.3.3+1.19.2") (deps (list (crate-dep (name "duct") (req "^0.13.3") (default-features #t) (kind 1)) (crate-dep (name "openssl-sys") (req "^0.9.55") (optional #t) (default-features #t) (kind 0)))) (hash "1bvkvxyb66y8yii3v5qyl5mkp7n3942qp3cnd9dzhd3k1ciswp9g") (features (quote (("openssl-vendored" "openssl-sys/vendored") ("nls") ("binaries")))) (links "krb5-src")))

(define-public crate-krb5-sys-0.1 (crate (name "krb5-sys") (vers "0.1.0") (hash "01m1hcm5c5nkj44xxrqbqbdms8sp5nc5ilqd5bkf8ljfbq1sl9r2") (features (quote (("krb5_old_crypto") ("krb5_deprecated") ("default"))))))

(define-public crate-krb5-sys-0.1 (crate (name "krb5-sys") (vers "0.1.1") (hash "02s4775wg0bi0y50bsidy6bpd40xmrhn5hlr1s8frfqzgkbamn91") (features (quote (("krb5_old_crypto") ("krb5_deprecated") ("default"))))))

(define-public crate-krb5-sys-0.1 (crate (name "krb5-sys") (vers "0.1.2") (hash "0kpvhngmn54kqnj3zkxbkbnc039v6jc01yl1jvzgm6v1qf8rnz7y") (features (quote (("krb5_old_crypto") ("krb5_deprecated") ("default"))))))

(define-public crate-krb5-sys-0.1 (crate (name "krb5-sys") (vers "0.1.3") (hash "10jlclw4l702k52dmrhx6gr1zlbx5z2790wf9n233i6mbch01aii") (features (quote (("krb5_old_crypto") ("krb5_deprecated") ("default"))))))

(define-public crate-krb5-sys-0.1 (crate (name "krb5-sys") (vers "0.1.4") (hash "1ifp4cgxwbs9fj5pq0vbkb71r90iq1kcc05lszqqlh704rx9dr8a") (features (quote (("krb5_old_crypto") ("krb5_deprecated") ("default"))))))

(define-public crate-krb5-sys-0.2 (crate (name "krb5-sys") (vers "0.2.0") (hash "1g9bl57pq9q8ka34wi0dlfhw922jqnzxh9ygpb6wdlf34cvizm6l") (features (quote (("krb5_old_crypto") ("krb5_deprecated") ("default"))))))

(define-public crate-krb5-sys-0.3 (crate (name "krb5-sys") (vers "0.3.0") (hash "07vpkgx96cq7a3mgk5p5z37skllrc3rp910qd8rji6rc4dld9k32") (features (quote (("krb5_old_crypto") ("krb5_deprecated") ("default"))))))

