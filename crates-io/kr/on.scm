(define-module (crates-io kr on) #:use-module (crates-io))

(define-public crate-kron-0.1 (crate (name "kron") (vers "0.1.0") (hash "1cj1ly3cz24b4fbham6x2v3q0k6vyf7jcf2ar4iknax7j2fa5bvk")))

(define-public crate-kron-1 (crate (name "kron") (vers "1.0.0") (deps (list (crate-dep (name "anyhow") (req "^1.0.71") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "time") (req "^0.3.21") (features (quote ("formatting"))) (default-features #t) (kind 0)))) (hash "16xhpid4z6q8rhriw628lizza3w5ihrha9dhy27wwjkmql6idzb2")))

(define-public crate-kron-1 (crate (name "kron") (vers "1.1.0") (deps (list (crate-dep (name "anyhow") (req "^1.0.71") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "time") (req "^0.3.21") (features (quote ("formatting"))) (default-features #t) (kind 0)))) (hash "1pa0mcdpr8np6d55ibp4llibwizrms3nrv5dcq1znrwvc9irky3c")))

(define-public crate-kron-2 (crate (name "kron") (vers "2.0.0") (deps (list (crate-dep (name "assert_cmd") (req "^2.0.11") (default-features #t) (kind 2)) (crate-dep (name "clap") (req "^4.3.0") (features (quote ("derive" "wrap_help"))) (default-features #t) (kind 0)) (crate-dep (name "kron-lib") (req "^2.0.0") (default-features #t) (kind 0)))) (hash "1mghdyjb8a7lchspyzw4wwfnp4zcss0vdjx8izp7nhlyhj6naq7a")))

(define-public crate-kron-2 (crate (name "kron") (vers "2.1.0") (deps (list (crate-dep (name "anyhow") (req "^1.0.71") (default-features #t) (kind 0)) (crate-dep (name "assert_cmd") (req "^2.0.11") (default-features #t) (kind 2)) (crate-dep (name "clap") (req "^4.3.0") (features (quote ("derive" "wrap_help"))) (default-features #t) (kind 0)) (crate-dep (name "kron-lib") (req "^2.1.0") (default-features #t) (kind 0)) (crate-dep (name "pager") (req "^0.16.1") (default-features #t) (kind 0)))) (hash "05mspbwlwf00v6lhsspcrd1qc77gppwc4ij8kc7qds0hjzawn39r")))

(define-public crate-kron-lib-2 (crate (name "kron-lib") (vers "2.0.0") (deps (list (crate-dep (name "anyhow") (req "^1.0.71") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "time") (req "^0.3.21") (features (quote ("formatting"))) (default-features #t) (kind 0)))) (hash "19wkcffx2f9izr7w85ipw0gjl8fx04ky6rmwwdx2x84g5gpg5f07")))

(define-public crate-kron-lib-2 (crate (name "kron-lib") (vers "2.1.0") (deps (list (crate-dep (name "anyhow") (req "^1.0.71") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "time") (req "^0.3.21") (features (quote ("formatting"))) (default-features #t) (kind 0)))) (hash "1qn4ll05c4csv125785w705lyvdyp3qaiyv48iadxk46n8i9b1lw")))

(define-public crate-krone-0.1 (crate (name "krone") (vers "0.1.0") (deps (list (crate-dep (name "dirs") (req "^5.0.1") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "toml") (req "^0.7.3") (features (quote ("parse" "display"))) (kind 0)) (crate-dep (name "uuid") (req "^1.3.2") (features (quote ("v4" "serde"))) (default-features #t) (kind 0)))) (hash "093rpqwwqlghv35grby36ckr7qiarkjdj3jz9p7pikwjdgvl8l4q")))

(define-public crate-krone-0.1 (crate (name "krone") (vers "0.1.1") (deps (list (crate-dep (name "dirs") (req "^5.0.1") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "toml") (req "^0.7.3") (features (quote ("parse" "display"))) (kind 0)) (crate-dep (name "uuid") (req "^1.3.2") (features (quote ("v4" "serde"))) (default-features #t) (kind 0)))) (hash "0ka91v23wjf8gg3bm1w55qwl62xgq303w00avwd3vvc5lczb1fb2")))

(define-public crate-krone-0.1 (crate (name "krone") (vers "0.1.2") (deps (list (crate-dep (name "chrono") (req "^0.4.26") (default-features #t) (kind 0)) (crate-dep (name "dirs") (req "^5.0.1") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "toml") (req "^0.7.3") (features (quote ("parse" "display"))) (kind 0)) (crate-dep (name "uuid") (req "^1.3.2") (features (quote ("v4" "serde"))) (default-features #t) (kind 0)))) (hash "11x4ycwk376vgmjs2hwm9fk4k99lkqjq6ck0nqb5fqr16g6f4a4q")))

(define-public crate-kronecker-0.1 (crate (name "kronecker") (vers "0.1.0") (hash "0inwa1dhqa54si7ch898hrxrx5yiz0490gggyxc1xywjcl077m6s")))

(define-public crate-kronos-0.0.1 (crate (name "kronos") (vers "0.0.1") (deps (list (crate-dep (name "chrono") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "earlgrey") (req "^0.0.4") (default-features #t) (kind 2)) (crate-dep (name "lexers") (req "^0.0.4") (default-features #t) (kind 2)))) (hash "0p0ap1imvcyc3ily20zxb9g6vwffpyz9zpxib71pbvdarl70ks50")))

(define-public crate-kronos-0.0.2 (crate (name "kronos") (vers "0.0.2") (deps (list (crate-dep (name "chrono") (req "^0.2") (default-features #t) (kind 0)))) (hash "1x7ngmr9akqfdmkfp6i5r5k8yr7dpdqn6h2gwca75gaambh4j981")))

(define-public crate-kronos-0.0.4 (crate (name "kronos") (vers "0.0.4") (deps (list (crate-dep (name "chrono") (req "^0.3") (default-features #t) (kind 0)))) (hash "1l5dv729r799n2mwyjj8w79msn31bf03rw7vfa0qi5qnfjrnmlc9")))

(define-public crate-kronos-0.0.5 (crate (name "kronos") (vers "0.0.5") (deps (list (crate-dep (name "chrono") (req "^0.4") (default-features #t) (kind 0)))) (hash "031ha4r797dq6k0jinilap454b7sxz0zs5blgw3zs4kq9rcn3sk5")))

(define-public crate-kronos-0.1 (crate (name "kronos") (vers "0.1.0") (deps (list (crate-dep (name "chrono") (req "^0.4") (default-features #t) (kind 0)))) (hash "1v38midcg6yv9wd1axb62966m3zbq7qi21gybf24crh6a0pglfz6")))

(define-public crate-kronos-0.1 (crate (name "kronos") (vers "0.1.1") (deps (list (crate-dep (name "chrono") (req "^0.4") (default-features #t) (kind 0)))) (hash "0p73j1sj4j36wpnyj4k750bz0kg2shngdbxwnz442d2br9cc8f9b")))

(define-public crate-kronos-0.1 (crate (name "kronos") (vers "0.1.2") (deps (list (crate-dep (name "chrono") (req "^0.4") (default-features #t) (kind 0)))) (hash "0c2x85j06s1z5r1jk856fvqizxmp3jriwyph20g8crw0ylrvbymp")))

(define-public crate-kronos-0.1 (crate (name "kronos") (vers "0.1.3") (deps (list (crate-dep (name "chrono") (req "^0.4") (default-features #t) (kind 0)))) (hash "1mwfbxr0s1nmvg55mg2w9jh37ikglrca29vpa292q51l6jf0ylw6")))

(define-public crate-kronos-0.1 (crate (name "kronos") (vers "0.1.4") (deps (list (crate-dep (name "chrono") (req "^0.4") (default-features #t) (kind 0)))) (hash "0l28ap8z6c0gddad2hyx87n1z9rjdsyvra7x5q2lagi60nnnnr58")))

(define-public crate-kronos-0.1 (crate (name "kronos") (vers "0.1.5") (deps (list (crate-dep (name "chrono") (req "^0.4") (default-features #t) (kind 0)))) (hash "095xidgwj26vq26csq0z7r0ya863xzmrbibki2bhlpx4qr45kmvl")))

