(define-module (crates-io kr am) #:use-module (crates-io))

(define-public crate-kramer-0.1 (crate (name "kramer") (vers "0.1.0") (deps (list (crate-dep (name "async-std") (req "^0.99") (optional #t) (default-features #t) (kind 0)))) (hash "1vs7wddci4ya1fqsnmnrnsrj332q7v6z36symq7wd0425d2fswng") (features (quote (("kramer-io" "async-std"))))))

(define-public crate-kramer-0.2 (crate (name "kramer") (vers "0.2.0") (deps (list (crate-dep (name "async-std") (req "^0.99") (optional #t) (default-features #t) (kind 0)))) (hash "0ah1ni2dy51zh4bx7sdkxpa09ngkflmww1varcmgjsnwis3i7p1c") (features (quote (("kramer-async" "async-std"))))))

(define-public crate-kramer-0.2 (crate (name "kramer") (vers "0.2.1") (deps (list (crate-dep (name "async-std") (req "^0.99") (optional #t) (default-features #t) (kind 0)))) (hash "1nvn6zrhi4fhqzl7lci921xqsh4s7a63n7gm77ljhj2l7fb72qmi") (features (quote (("kramer-async" "async-std"))))))

(define-public crate-kramer-0.3 (crate (name "kramer") (vers "0.3.0") (deps (list (crate-dep (name "async-std") (req "^0.99") (optional #t) (default-features #t) (kind 0)))) (hash "1s3sals2nvpp51rqpp8qr3jn8ydhbfg9qpyc52nj52rd327pmfyw") (features (quote (("kramer-async" "async-std"))))))

(define-public crate-kramer-1 (crate (name "kramer") (vers "1.0.0") (deps (list (crate-dep (name "async-std") (req "^1.0") (optional #t) (default-features #t) (kind 0)))) (hash "1vbw8mzpbb5iyv8fdqxzzvkgapy5hd5sbjlc8c9bh75zahp5k41v") (features (quote (("kramer-async" "async-std"))))))

(define-public crate-kramer-1 (crate (name "kramer") (vers "1.0.1") (deps (list (crate-dep (name "async-std") (req "^1.0") (optional #t) (default-features #t) (kind 0)))) (hash "0gd7xb62w05zd7kckwxmrh3ggdcvggj5ijzd4kzmz6vslf87jnmk") (features (quote (("kramer-async" "async-std"))))))

(define-public crate-kramer-1 (crate (name "kramer") (vers "1.1.0") (deps (list (crate-dep (name "async-std") (req "^1.0") (optional #t) (default-features #t) (kind 0)))) (hash "053r87spm9way30d2s83wz8x7swfqmgs7lad78q769j8vqqb4s58") (features (quote (("kramer-async" "async-std"))))))

(define-public crate-kramer-1 (crate (name "kramer") (vers "1.2.0") (deps (list (crate-dep (name "async-std") (req "^1.0") (optional #t) (default-features #t) (kind 0)))) (hash "0zfc8c9zz1myicxxcqcrh8sz81m69515159yhw9zhinnd5ch9sqk") (features (quote (("kramer-async" "async-std") ("acl"))))))

(define-public crate-kramer-1 (crate (name "kramer") (vers "1.3.0") (deps (list (crate-dep (name "async-std") (req "^1.0") (optional #t) (default-features #t) (kind 0)))) (hash "0i4dmp9ddqh37ysnp38a9jg44l9gpn3cj6vf1ja3d3p897jccrmn") (features (quote (("kramer-async" "async-std") ("acl"))))))

(define-public crate-kramer-1 (crate (name "kramer") (vers "1.3.1") (deps (list (crate-dep (name "async-std") (req "^1.0") (optional #t) (default-features #t) (kind 0)))) (hash "1jj1jzp3hp99cwflawwakf1hbqwz4zc272j8ilx9wln6811fan1i") (features (quote (("kramer-async" "async-std") ("acl"))))))

(define-public crate-kramer-1 (crate (name "kramer") (vers "1.3.2") (deps (list (crate-dep (name "async-std") (req "^1.0") (optional #t) (default-features #t) (kind 0)))) (hash "1jhfjyzjlmnqwxnvq624yzy90db9hxx9ip4rpaknalc6v47nwpmm") (features (quote (("kramer-async" "async-std") ("acl"))))))

(define-public crate-kramer-2 (crate (name "kramer") (vers "2.0.0") (deps (list (crate-dep (name "async-std") (req "^1.0") (optional #t) (default-features #t) (kind 0)))) (hash "1gflzm3rws451m9mjx575a44g62vjdd7isw4krkg7qnrh782kn24") (features (quote (("kramer-async" "async-std") ("acl"))))))

(define-public crate-kramer-2 (crate (name "kramer") (vers "2.0.1") (deps (list (crate-dep (name "async-std") (req "^1.0") (optional #t) (default-features #t) (kind 0)))) (hash "0yrl7xq8m3q1fv9dvx607pk9h4b5qv5pqycdhxlm8ifyds7wnvrz") (features (quote (("kramer-async" "async-std") ("acl"))))))

(define-public crate-kramer-2 (crate (name "kramer") (vers "2.1.0") (deps (list (crate-dep (name "async-std") (req "^1.0") (optional #t) (default-features #t) (kind 0)))) (hash "1849778wxa6wcdj8f672dnjfs1fi0p5g71iysbi7wdkv0rxap86v") (features (quote (("kramer-async" "async-std") ("acl"))))))

(define-public crate-kramer-3 (crate (name "kramer") (vers "3.0.0") (deps (list (crate-dep (name "async-std") (req "^1.0") (optional #t) (default-features #t) (kind 0)))) (hash "082wvcg2n1mlzfqxfw0njy2zrvy23d9nbxina7wz1hq4jmxrf8cl") (features (quote (("kramer-async-read" "kramer-async") ("kramer-async" "async-std") ("acl"))))))

(define-public crate-kramqpus-0.1 (crate (name "kramqpus") (vers "0.1.0") (hash "13vmq49yv3hi3xx609mk46pvbyqkib4cyk8h0cvyhga61gbjw265")))

