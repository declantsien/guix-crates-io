(define-module (crates-io kr cl) #:use-module (crates-io))

(define-public crate-krcli-0.1 (crate (name "krcli") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^2.33.3") (default-features #t) (kind 0)) (crate-dep (name "exitcode") (req "^1.1.2") (default-features #t) (kind 0)) (crate-dep (name "keyring") (req "^0.10.1") (default-features #t) (kind 0)))) (hash "0nvapf5g50m9brha1kq75j80djz2ks4s3rw6vx47mkbq142j32ys")))

(define-public crate-krcli-0.1 (crate (name "krcli") (vers "0.1.1") (deps (list (crate-dep (name "clap") (req "^2.33.3") (default-features #t) (kind 0)) (crate-dep (name "exitcode") (req "^1.1.2") (default-features #t) (kind 0)) (crate-dep (name "keyring") (req "^0.10.1") (default-features #t) (kind 0)))) (hash "1sfzcn2rhbf1w2iybz56r0i358g6hsk4pf2bi8c87q7bg4q7fxml")))

