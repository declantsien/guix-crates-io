(define-module (crates-io kr ep) #:use-module (crates-io))

(define-public crate-krep-0.1 (crate (name "krep") (vers "0.1.0") (deps (list (crate-dep (name "doe") (req "^0.1.32") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.4") (default-features #t) (kind 0)))) (hash "146wvdhf1b3xph7icjch4zm29pb91jpi6i8ywbfzhhpx1g8p4p1y") (yanked #t)))

(define-public crate-krep-0.1 (crate (name "krep") (vers "0.1.1") (deps (list (crate-dep (name "doe") (req "^0.1.32") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.4") (default-features #t) (kind 0)))) (hash "1m0pyims0y0bc3p775vbrryy4hhjaiwi6im5pxfldxhgajpkg5vh")))

(define-public crate-krep-1 (crate (name "krep") (vers "1.0.0") (deps (list (crate-dep (name "doe") (req "^0.1.32") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.4") (default-features #t) (kind 0)))) (hash "1wfxzz4q9jwhg96jc9nr5ha9q88pgpl4016yv64nvlq7b1m8apmn") (yanked #t)))

(define-public crate-krep-2 (crate (name "krep") (vers "2.0.0") (deps (list (crate-dep (name "doe") (req "^0.1.32") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.4") (default-features #t) (kind 0)))) (hash "13d69hma7jc7mwna5yfzqblf6l05amgn0vql6cdbpmvp2sxc9c1g") (yanked #t)))

(define-public crate-krep-3 (crate (name "krep") (vers "3.0.0") (deps (list (crate-dep (name "doe") (req "^0.1.32") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.4") (default-features #t) (kind 0)))) (hash "1ff6raibs0dkw4bcqqdmi2nvq1gr6iiqfbi8l6fyh49rhnk5i2g5") (yanked #t)))

