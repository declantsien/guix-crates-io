(define-module (crates-io kr yp) #:use-module (crates-io))

(define-public crate-kryp-0.0.0 (crate (name "kryp") (vers "0.0.0") (hash "07kq3gdjvk7v9ln9mqc4r901rlfpplsx16a6r1nszxc6h9bwm16b")))

(define-public crate-krypt-0.1 (crate (name "krypt") (vers "0.1.0") (deps (list (crate-dep (name "atty") (req "^0.2.14") (default-features #t) (kind 0)) (crate-dep (name "blake3") (req "^0.1.3") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^2.33.0") (default-features #t) (kind 0)) (crate-dep (name "data-encoding") (req "^2.1.2") (default-features #t) (kind 0)) (crate-dep (name "digest") (req "^0.8.1") (default-features #t) (kind 0)) (crate-dep (name "hex") (req "^0.4.0") (default-features #t) (kind 0)) (crate-dep (name "hexyl") (req "^0.6.0") (default-features #t) (kind 0)) (crate-dep (name "md-5") (req "^0.8.0") (default-features #t) (kind 0)) (crate-dep (name "md2") (req "^0.8.0") (default-features #t) (kind 0)) (crate-dep (name "md4") (req "^0.8.0") (default-features #t) (kind 0)) (crate-dep (name "sha-1") (req "^0.8.2") (default-features #t) (kind 0)) (crate-dep (name "sha2") (req "^0.8.1") (default-features #t) (kind 0)) (crate-dep (name "sha3") (req "^0.8.2") (default-features #t) (kind 0)) (crate-dep (name "streebog") (req "^0.8.0") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.3.9") (default-features #t) (kind 0)))) (hash "0lxfc6xc2bjhn3ng0qrshxhavgw299a0yc1w6fshyp9hf64zigaz")))

(define-public crate-krypto-0.0.0 (crate (name "krypto") (vers "0.0.0") (deps (list (crate-dep (name "abscissa") (req "^0") (default-features #t) (kind 0)))) (hash "0n7izh7291czj0sw918k2yc6aa5iszy2gw2mfw5j1a1kd9yciv9a") (yanked #t)))

(define-public crate-krypton-0.1 (crate (name "krypton") (vers "0.1.0") (hash "0khhw65dmj3h2lfh215w5sjl30av3gy1nnilgl6rli9028mswr1a")))

(define-public crate-krypton-0.1 (crate (name "krypton") (vers "0.1.0-rc.0.1") (deps (list (crate-dep (name "rand") (req "^0.6") (default-features #t) (kind 0)))) (hash "0mw13y5d48hmd67yy6y1xjwca06dajmi1810mlk9h415kjynwaw2")))

(define-public crate-kryptonite-0.1 (crate (name "kryptonite") (vers "0.1.0") (hash "1qnnhizklyrgba1m7dabmwygsnls8q02l7ys16z6vyvvjfzkzqvn")))

(define-public crate-kryptor-0.1 (crate (name "kryptor") (vers "0.1.0") (deps (list (crate-dep (name "assert_cmd") (req "^2") (default-features #t) (kind 2)) (crate-dep (name "base32") (req "^0.4.0") (default-features #t) (kind 0)) (crate-dep (name "base64") (req "^0.13") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^2.33") (default-features #t) (kind 0)) (crate-dep (name "predicates") (req "^2") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "sha2") (req "^0.10.8") (default-features #t) (kind 0)))) (hash "0vb73gl06ln5imjpz4wsz4r7x66cxzxcj2b3firhrfryzzl1sf16")))

(define-public crate-kryptor-0.1 (crate (name "kryptor") (vers "0.1.1") (deps (list (crate-dep (name "assert_cmd") (req "^2") (default-features #t) (kind 2)) (crate-dep (name "base32") (req "^0.4.0") (default-features #t) (kind 0)) (crate-dep (name "base64") (req "^0.13") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^2.33") (default-features #t) (kind 0)) (crate-dep (name "predicates") (req "^2") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "sha2") (req "^0.10.8") (default-features #t) (kind 0)))) (hash "0m9g3zhbxmk3f31r4q8k6jxl459vb5wzvf5kxlj11qy2sxpxlis0")))

(define-public crate-kryptor-0.1 (crate (name "kryptor") (vers "0.1.2") (deps (list (crate-dep (name "assert_cmd") (req "^2") (default-features #t) (kind 2)) (crate-dep (name "base32") (req "^0.4.0") (default-features #t) (kind 0)) (crate-dep (name "base64") (req "^0.13") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^2.33") (default-features #t) (kind 0)) (crate-dep (name "predicates") (req "^2") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "sha2") (req "^0.10.8") (default-features #t) (kind 0)))) (hash "0p764cdv96rrwhs0xsmcaf52n8m2xwdilm76nvahgcy3w89f0db7")))

(define-public crate-kryptor-0.1 (crate (name "kryptor") (vers "0.1.3") (deps (list (crate-dep (name "assert_cmd") (req "^2") (default-features #t) (kind 2)) (crate-dep (name "base32") (req "^0.4.0") (default-features #t) (kind 0)) (crate-dep (name "base64") (req "^0.13") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^2.33") (default-features #t) (kind 0)) (crate-dep (name "predicates") (req "^2") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "sha2") (req "^0.10.8") (default-features #t) (kind 0)))) (hash "03wyaqhp5vkyjhj8b9vh2r83y8wx5g6fmlfkbnnp3jys6b11wz29")))

(define-public crate-kryptos-0.1 (crate (name "kryptos") (vers "0.1.2") (hash "1755wvig8d7pv5hb1f3jrpfh359i2sc80jp9bgrjiqafcrxh0vws")))

(define-public crate-kryptos-0.3 (crate (name "kryptos") (vers "0.3.0") (hash "1xq08za2v893c02h121hszm75hsjchcqym2qr1k4anha04hhvjvm")))

(define-public crate-kryptos-0.4 (crate (name "kryptos") (vers "0.4.0") (hash "1bjbii5vb3r4cmv1w4w2x2i15v8fj41dylzd46972rmzlxb91yr8")))

(define-public crate-kryptos-0.4 (crate (name "kryptos") (vers "0.4.1") (hash "00jv5ja7xfahj3k38k4m3kjmg6wyqjqmsk5k64wh4hpda12s014i")))

(define-public crate-kryptos-0.5 (crate (name "kryptos") (vers "0.5.0") (hash "097g7n3ac860h56r6vnhgi9gry8sjh3hnclrwry4nhgdgylq3y0c")))

(define-public crate-kryptos-0.6 (crate (name "kryptos") (vers "0.6.0") (deps (list (crate-dep (name "regex") (req "^0.2.6") (default-features #t) (kind 0)))) (hash "1c2sfcmayyz628ws58r7njb7211hdzk88p1hpn253vbf1gs18rah")))

(define-public crate-kryptos-0.6 (crate (name "kryptos") (vers "0.6.1") (deps (list (crate-dep (name "regex") (req "^1.0.0") (default-features #t) (kind 0)))) (hash "0i1dm9lwashmk1glg1f1nh9wb47x75j0xrssp7sglpnzjr840mv8")))

(define-public crate-kryptos-0.6 (crate (name "kryptos") (vers "0.6.2") (deps (list (crate-dep (name "regex") (req "^1.5.4") (default-features #t) (kind 0)))) (hash "09lh701dh7dc04kbsfkmkr1rgh73d4kal20f0jbswx87alpwzvw0")))

(define-public crate-kryptos-0.6 (crate (name "kryptos") (vers "0.6.3") (deps (list (crate-dep (name "regex") (req "^1.5.4") (default-features #t) (kind 0)))) (hash "13f53n521dx007a70bmq1y0psphcmahhqyaj8bh0li641635cb49")))

