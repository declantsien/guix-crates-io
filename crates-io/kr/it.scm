(define-module (crates-io kr it) #:use-module (crates-io))

(define-public crate-krita-0.1 (crate (name "krita") (vers "0.1.0") (deps (list (crate-dep (name "lexical-core") (req "^0.7") (default-features #t) (kind 0)) (crate-dep (name "lzf") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "nom") (req "^5") (features (quote ("std"))) (kind 0)) (crate-dep (name "png") (req "^0.16") (default-features #t) (kind 2)))) (hash "0gr8v051yq6sggvq1mnyfif4db21yazzzhy6px3q67kkynwa71vm")))

(define-public crate-krita-0.2 (crate (name "krita") (vers "0.2.0") (deps (list (crate-dep (name "lexical-core") (req "^0.7") (default-features #t) (kind 0)) (crate-dep (name "lzf") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "minidom") (req "^0.12") (default-features #t) (kind 0)) (crate-dep (name "nom") (req "^5") (features (quote ("std"))) (kind 0)) (crate-dep (name "png") (req "^0.16") (default-features #t) (kind 2)) (crate-dep (name "zip") (req "^0.5") (default-features #t) (kind 0)))) (hash "0c3si0ij3xpnm3fi92rhb5887yygqhw47rmwlc7nxz0dg5c3xpag")))

(define-public crate-krita-0.2 (crate (name "krita") (vers "0.2.1") (deps (list (crate-dep (name "lexical-core") (req "^0.7") (default-features #t) (kind 0)) (crate-dep (name "lzf") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "minidom") (req "^0.12") (default-features #t) (kind 0)) (crate-dep (name "nom") (req "^5") (features (quote ("std"))) (kind 0)) (crate-dep (name "png") (req "^0.16") (default-features #t) (kind 2)) (crate-dep (name "zip") (req "^0.5") (default-features #t) (kind 0)))) (hash "1aj77g9fzj230g89w9z44bx6fnfa6q6hq3srb77izzvd7kmb7hvf")))

(define-public crate-krita-to-svg-0.2 (crate (name "krita-to-svg") (vers "0.2.0") (deps (list (crate-dep (name "base64") (req "^0.12") (default-features #t) (kind 0)) (crate-dep (name "krita") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "minidom") (req "^0.12") (default-features #t) (kind 0)) (crate-dep (name "png") (req "^0.16") (default-features #t) (kind 0)))) (hash "09bc0b338zp0ssyjiz73c94ak6km30423zqp8x07l8jxjhjs8ac2")))

(define-public crate-krita-to-svg-0.2 (crate (name "krita-to-svg") (vers "0.2.1") (deps (list (crate-dep (name "base64") (req "^0.12") (default-features #t) (kind 0)) (crate-dep (name "krita") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "minidom") (req "^0.12") (default-features #t) (kind 0)) (crate-dep (name "png") (req "^0.16") (default-features #t) (kind 0)))) (hash "0v2h862iv1vs41r6g3nvxgwgld9ww5ds7fjjgs6jc94n2bp132rn")))

(define-public crate-krita-to-svg-0.2 (crate (name "krita-to-svg") (vers "0.2.2") (deps (list (crate-dep (name "base64") (req "^0.12") (default-features #t) (kind 0)) (crate-dep (name "krita") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "minidom") (req "^0.12") (default-features #t) (kind 0)) (crate-dep (name "png") (req "^0.16") (default-features #t) (kind 0)))) (hash "1hj8fnyb6j5m9g32chdldp0qxxwyjd16qrwrm8zbkb6wi1hwcrdd")))

(define-public crate-krittapong-0.0.1 (crate (name "krittapong") (vers "0.0.1") (hash "1jyzd3v3i49xm9amwv5v0y6lcdbhpa67mn4f8xzmjzsvrl6iv83j")))

