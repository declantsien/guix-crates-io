(define-module (crates-io kr en) #:use-module (crates-io))

(define-public crate-krenz-1 (crate (name "krenz") (vers "1.0.0") (deps (list (crate-dep (name "color-eyre") (req "^0.6.2") (default-features #t) (kind 0)))) (hash "0rh51k0465dil1zxgxmvlih67fxz9hdmrgnca602fxk7h4ghyxz6")))

(define-public crate-krenz-1 (crate (name "krenz") (vers "1.0.1") (deps (list (crate-dep (name "color-eyre") (req "^0.6.2") (default-features #t) (kind 0)))) (hash "15ag2ng0ajmm6lpaazqh5q5b58h5mrm5pb5abl625mxz23hdhvrh")))

(define-public crate-krenz-1 (crate (name "krenz") (vers "1.0.2") (deps (list (crate-dep (name "color-eyre") (req "^0.6.2") (default-features #t) (kind 0)))) (hash "0nyrcqqdpbqa7pw0gib4rpj9jbs6ym3s849mr10azgiqz5ip4nms")))

(define-public crate-krenz-1 (crate (name "krenz") (vers "1.0.3") (deps (list (crate-dep (name "color-eyre") (req "^0.6") (default-features #t) (kind 0)))) (hash "0w9mzf2fz99kqhis97cyzl2hqq4kanqkhck9wg3sp1k65i2qx5jq")))

