(define-module (crates-io qh as) #:use-module (crates-io))

(define-public crate-qhashfilefuture-0.9 (crate (name "qhashfilefuture") (vers "0.9.0") (deps (list (crate-dep (name "hex") (req "^0.4.2") (default-features #t) (kind 0)) (crate-dep (name "sha2") (req "^0.9.0") (default-features #t) (kind 0)))) (hash "1sj29jwpm52cqcm89j8apikbc68ja1xrl4gj6w5yhvib7plzayxk")))

(define-public crate-qhashfilefuture-0.9 (crate (name "qhashfilefuture") (vers "0.9.1") (deps (list (crate-dep (name "hex") (req "^0.4.2") (default-features #t) (kind 0)) (crate-dep (name "sha2") (req "^0.9.0") (default-features #t) (kind 0)))) (hash "000wiisv3hfpq2nyyj7447sikb120s6p7s08cszcj9nx5kz5nhwf")))

