(define-module (crates-io qh ul) #:use-module (crates-io))

(define-public crate-qhull-0.1 (crate (name "qhull") (vers "0.1.0") (deps (list (crate-dep (name "qhull-sys") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "0c0cmv2p2agyrmh5q1brxs7nkpcnrcmng4d9pa77fbbqrfm4k87l")))

(define-public crate-qhull-0.1 (crate (name "qhull") (vers "0.1.1") (deps (list (crate-dep (name "qhull-sys") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "0wch17vm5hn4lknl74yhah9m8lkizxpprlrnp8i20wm48f5np87q")))

(define-public crate-qhull-0.2 (crate (name "qhull") (vers "0.2.0") (deps (list (crate-dep (name "qhull-sys") (req "^0.2") (default-features #t) (kind 0)))) (hash "1sg29mcznjpcs3jlivws0hd4kqscbwcgnmqdzpq19cdznncv4w7z")))

(define-public crate-qhull-0.3 (crate (name "qhull") (vers "0.3.0") (deps (list (crate-dep (name "qhull-sys") (req "^0.3") (features (quote ("include-programs"))) (default-features #t) (kind 0)))) (hash "0qfmyiwca3pk4r8gh5b6cbkbi5kzssfrp7ql8cfrh1k7pcc5m4lv")))

(define-public crate-qhull-0.3 (crate (name "qhull") (vers "0.3.1") (deps (list (crate-dep (name "qhull-sys") (req "^0.3") (features (quote ("include-programs"))) (default-features #t) (kind 0)))) (hash "0vxhlr1q4i787zrkjnn0j1ipl751r8i32mwfmgxrn9pmjm0y6r8m")))

(define-public crate-qhull-sys-0.1 (crate (name "qhull-sys") (vers "0.1.0") (deps (list (crate-dep (name "bindgen") (req "^0.69.4") (default-features #t) (kind 1)) (crate-dep (name "cc") (req "^1.0.83") (default-features #t) (kind 1)))) (hash "18a0160bl1zncd8jbav787ip1ci30bp50k887lmfl5qynbmrlhz6") (features (quote (("default") ("all-headers"))))))

(define-public crate-qhull-sys-0.1 (crate (name "qhull-sys") (vers "0.1.1") (deps (list (crate-dep (name "bindgen") (req "^0.69.4") (default-features #t) (kind 1)) (crate-dep (name "cc") (req "^1.0.83") (default-features #t) (kind 1)))) (hash "1cnh1yzk8kayzb3y7mh7i9xg9pvxk8kk8jk4z2w3zkd861r2smnb") (features (quote (("default") ("all-headers"))))))

(define-public crate-qhull-sys-0.2 (crate (name "qhull-sys") (vers "0.2.0") (deps (list (crate-dep (name "bindgen") (req "^0.69") (default-features #t) (kind 1)) (crate-dep (name "cc") (req "^1.0") (default-features #t) (kind 1)))) (hash "1dq7a7fykji03ic2x4hg0cbx03vs6gm5kxfxb31cy3wvycy4a7cd") (features (quote (("default") ("all-headers"))))))

(define-public crate-qhull-sys-0.3 (crate (name "qhull-sys") (vers "0.3.0") (deps (list (crate-dep (name "bindgen") (req "^0.69") (default-features #t) (kind 1)) (crate-dep (name "cc") (req "^1.0") (default-features #t) (kind 1)))) (hash "0vks70b9cs1zpalqngcl9z58897yrx2zymm1hqir7xm479zcm3x4") (features (quote (("include-programs") ("default") ("all-headers"))))))

(define-public crate-qhull-sys-0.3 (crate (name "qhull-sys") (vers "0.3.1") (deps (list (crate-dep (name "bindgen") (req "^0.69") (default-features #t) (kind 1)) (crate-dep (name "cc") (req "^1.0") (default-features #t) (kind 1)))) (hash "0cr19hpy2ffafbb39p4pqj6xy9d8n2mm86ccbdc09izscxz43qaf") (features (quote (("include-programs") ("default") ("all-headers"))))))

