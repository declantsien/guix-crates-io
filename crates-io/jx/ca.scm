(define-module (crates-io jx ca) #:use-module (crates-io))

(define-public crate-jxcape-0.1 (crate (name "jxcape") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^4.4.8") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.108") (default-features #t) (kind 0)))) (hash "0fjgnckk683m5aqn2xr0zkxl0spc0ca30nk220yynfa76pfrfs6p")))

(define-public crate-jxcape-0.2 (crate (name "jxcape") (vers "0.2.0") (deps (list (crate-dep (name "clap") (req "^4.4.8") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.108") (default-features #t) (kind 0)))) (hash "0vydb9c3fyikp74y80949vsrj098sf4i0ghiz5dq5h12gg3kccpv")))

