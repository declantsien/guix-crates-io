(define-module (crates-io jx pa) #:use-module (crates-io))

(define-public crate-jxpand-0.1 (crate (name "jxpand") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^4.4.10") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.108") (default-features #t) (kind 0)))) (hash "1kg3ir9hrfpzmscbfw96irami3vc2rr0kddyd051nimhfwl3xd1j")))

