(define-module (crates-io jx on) #:use-module (crates-io))

(define-public crate-jxon-0.1 (crate (name "jxon") (vers "0.1.0") (deps (list (crate-dep (name "quick-xml") (req "^0.17") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (features (quote ("preserve_order"))) (default-features #t) (kind 0)))) (hash "1y08d96d7c5drl1jkrf39bk0hh3w5k3jd95y33xqc005m2lhxafg")))

(define-public crate-jxon-0.2 (crate (name "jxon") (vers "0.2.0") (deps (list (crate-dep (name "quick-xml") (req "^0.17") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (features (quote ("preserve_order"))) (default-features #t) (kind 0)))) (hash "0g1hiv72m281092v2k4vxk0sl946ylywlvv6w365pp07i0rz71gl")))

(define-public crate-jxon-0.3 (crate (name "jxon") (vers "0.3.0") (deps (list (crate-dep (name "quick-xml") (req "^0.17") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (features (quote ("preserve_order"))) (default-features #t) (kind 0)))) (hash "19rv57pvlr4619n2gq4wh5j3vxaf7x9f612ni48chryd9hijq2l1")))

(define-public crate-jxon-0.4 (crate (name "jxon") (vers "0.4.0") (deps (list (crate-dep (name "quick-xml") (req "^0.17") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (features (quote ("preserve_order"))) (default-features #t) (kind 0)))) (hash "1k0smyz5cx3s1kl1cr42v6z38wlbdl4gz3lzxz8h1l1d9lqmq5ab")))

(define-public crate-jxon-0.5 (crate (name "jxon") (vers "0.5.0") (deps (list (crate-dep (name "quick-xml") (req "^0.17") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (features (quote ("preserve_order"))) (default-features #t) (kind 0)))) (hash "0y8fcfa72n5bc135bw28i7l8yzbk91kcx3y2g13b88zp4avqln9s")))

(define-public crate-jxon-0.7 (crate (name "jxon") (vers "0.7.0") (deps (list (crate-dep (name "quick-xml") (req "^0.17") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (features (quote ("preserve_order"))) (default-features #t) (kind 0)))) (hash "1mpcbwhz0p23h3kqx69d84mpbifxcbxicm4qlk047mhrqcgwza54")))

(define-public crate-jxon-0.7 (crate (name "jxon") (vers "0.7.1") (deps (list (crate-dep (name "quick-xml") (req "^0.22") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (features (quote ("preserve_order"))) (default-features #t) (kind 0)))) (hash "1kvlbb8z9sxszk2zp07j4bkiv110mlgx30i0d8mhqi94xhh0q6qy")))

(define-public crate-jxon-0.8 (crate (name "jxon") (vers "0.8.0") (deps (list (crate-dep (name "quick-xml") (req "^0.22") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (features (quote ("preserve_order"))) (default-features #t) (kind 0)))) (hash "002c74zpjayp3x9s4yicphv0prs9b6xbx7wcgi58m5c50mck43rl")))

(define-public crate-jxon-0.9 (crate (name "jxon") (vers "0.9.0") (deps (list (crate-dep (name "quick-xml") (req "^0.22") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (features (quote ("preserve_order"))) (default-features #t) (kind 0)))) (hash "1m4pz0njcjdz2wcjfq58lwk69qr2gxfyg8w1dmayjf1mvi0ivda6")))

