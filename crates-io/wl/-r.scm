(define-module (crates-io wl -r) #:use-module (crates-io))

(define-public crate-wl-realtime-ogd-0.1 (crate (name "wl-realtime-ogd") (vers "0.1.0") (deps (list (crate-dep (name "chrono") (req "^0.4.24") (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.11.16") (features (quote ("json"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.160") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.96") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "1qjyd96ff9qxqm1w1a4fvsd0wl4p0ql4vf0g9wikc3fz68alvzv0")))

