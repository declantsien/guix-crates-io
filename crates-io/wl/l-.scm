(define-module (crates-io wl l-) #:use-module (crates-io))

(define-public crate-wll-macros-0.1 (crate (name "wll-macros") (vers "0.1.0") (deps (list (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (features (quote ("full" "extra-traits"))) (default-features #t) (kind 0)))) (hash "0w8fmgajv2ndx6fk5hjc9kykkbmk456m8wgmbh2x5fjbxqn5bdfh")))

(define-public crate-wll-sys-0.1 (crate (name "wll-sys") (vers "0.1.0") (deps (list (crate-dep (name "bindgen") (req "^0.54") (default-features #t) (kind 1)))) (hash "1dq38ag9q5p8shwhvj1hz0sng3bnqjjmc8p1js1f6j6d8qw3gy4j") (features (quote (("auto-link"))))))

