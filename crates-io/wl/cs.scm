(define-module (crates-io wl cs) #:use-module (crates-io))

(define-public crate-wlcs-0.1 (crate (name "wlcs") (vers "0.1.0") (deps (list (crate-dep (name "container_of") (req "^0.5.1") (default-features #t) (kind 0)) (crate-dep (name "memoffset") (req "^0.9.0") (default-features #t) (kind 0)) (crate-dep (name "nix") (req "^0.27.1") (features (quote ("signal"))) (kind 0)) (crate-dep (name "wayland-sys") (req "^0.31.1") (features (quote ("client" "server"))) (default-features #t) (kind 0)))) (hash "17k0nwn3f2z71rncb8glb4x15m5zmcbklnk71hpv739nrq2w769d") (rust-version "1.56.1")))

