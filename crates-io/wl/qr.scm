(define-module (crates-io wl qr) #:use-module (crates-io))

(define-public crate-wlqrkrhtlvek-0.1 (crate (name "wlqrkrhtlvek") (vers "0.1.0") (deps (list (crate-dep (name "chrono") (req "^0.4") (default-features #t) (kind 0)))) (hash "1h9chi68wf0fy3mxq1jvxvad0ppfdrkh2z9p0zabrphajjc6chva")))

(define-public crate-wlqrkrhtlvek-0.1 (crate (name "wlqrkrhtlvek") (vers "0.1.1") (deps (list (crate-dep (name "chrono") (req "^0.4") (default-features #t) (kind 0)))) (hash "004x4niwx87ymcv2wrwkidqp8vmjj7k5p8ijj7lxcnl5lamvp7c8")))

(define-public crate-wlqrkrhtlvek-0.1 (crate (name "wlqrkrhtlvek") (vers "0.1.2") (deps (list (crate-dep (name "chrono") (req "^0.4") (default-features #t) (kind 0)))) (hash "1hjzgnzc18smgilf0s1yxdv3239r3px5caq14v1hd591val02skn")))

