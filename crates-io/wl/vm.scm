(define-module (crates-io wl vm) #:use-module (crates-io))

(define-public crate-wlvm-0.1 (crate (name "wlvm") (vers "0.1.0") (hash "0lk4qw36xy4xnzihvwaymg0aaar80x73xrrnkn96hmm6n8kpyicr")))

(define-public crate-wlvm-0.2 (crate (name "wlvm") (vers "0.2.0") (hash "12s3jznjrjqjpzf4ix1n6hfzaabgjqg0nm5sfzlkxn1qxprlqmj1")))

(define-public crate-wlvm-0.3 (crate (name "wlvm") (vers "0.3.0") (hash "19ndm3qjbv7ja8gjl6mv5pcl5hjjbkqirsc06ir9xjnzxc7ds7pb")))

(define-public crate-wlvm-0.3 (crate (name "wlvm") (vers "0.3.1") (hash "1vla579m5a3ncnrvi8fwklgy9vx7b9f2fcz3lwddxcb8rihcn5i2")))

(define-public crate-wlvm-0.3 (crate (name "wlvm") (vers "0.3.2") (hash "0n71cqar09cf6d8sicg2ymzppq6s7z7a6hwg54li982gqbjgqa5d")))

(define-public crate-wlvm-0.3 (crate (name "wlvm") (vers "0.3.3") (hash "1yq3zkb4gmnjdkij76srqj3kdc64y3whb29haabhk0dqagbzp79a")))

(define-public crate-wlvm-0.3 (crate (name "wlvm") (vers "0.3.4") (hash "149qk49wjfsbz2szxb6d2m73vf8nkp05yxl0a2l3s0aydwnj4nfr")))

(define-public crate-wlvm-0.3 (crate (name "wlvm") (vers "0.3.5") (hash "0466isr8swh3lkbhg798wlhsh4ycjhivagpdp15a2gacwg16n8yi")))

(define-public crate-wlvm-0.3 (crate (name "wlvm") (vers "0.3.6") (hash "0i72wazvyw4xsn9yqfncg39qbrdqfpncg19nhdsl0rb0k6pc8iim")))

(define-public crate-wlvm-0.3 (crate (name "wlvm") (vers "0.3.7") (hash "0pdwjwmiky7d79mzh8yjf7lz3zz30cmd9naxwyvxks44wkf2xff8")))

(define-public crate-wlvm-0.3 (crate (name "wlvm") (vers "0.3.9") (hash "03c6cx7gk359xpsali77svyvxcvs9jp2gx5sfzh0awxx7kccizl9")))

(define-public crate-wlvm-0.3 (crate (name "wlvm") (vers "0.3.10") (hash "0psghyd0zv2ydw9l4lcmc9bfd0270ln2m3zc9zi8ijw8q2bpn15y")))

(define-public crate-wlvm-0.4 (crate (name "wlvm") (vers "0.4.0") (hash "0nmy64plhk045gvvl2qpb5zgm1k7js3v1mwizw7xdm37jfpv499v")))

