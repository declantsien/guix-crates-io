(define-module (crates-io wl -t) #:use-module (crates-io))

(define-public crate-wl-tools-0.0.0 (crate (name "wl-tools") (vers "0.0.0-alpha.1") (deps (list (crate-dep (name "test-case") (req "^2") (kind 2)))) (hash "0kqn34x0dpll18fwwmm6xz24rcihs1s1cfg8sizkymjlyh9j0cqg") (features (quote (("default"))))))

(define-public crate-wl-tools-0.0.0 (crate (name "wl-tools") (vers "0.0.0-alpha.2") (deps (list (crate-dep (name "test-case") (req "^2") (kind 2)))) (hash "0ls9c551g3yrqxcdqzhm1wpcgg63l781xnq07r96ajabkbdpklzy") (features (quote (("default"))))))

(define-public crate-wl-tools-0.0.0 (crate (name "wl-tools") (vers "0.0.0-alpha.3") (deps (list (crate-dep (name "test-case") (req "^2") (kind 2)))) (hash "1nzgjyz9nji7ij02fcw87absc1fnpzxwkqbaasczpv9ffs8iv1ri") (features (quote (("default"))))))

