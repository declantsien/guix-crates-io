(define-module (crates-io do tv) #:use-module (crates-io))

(define-public crate-dotvanity-0.1 (crate (name "dotvanity") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^2.33.1") (default-features #t) (kind 0)) (crate-dep (name "data-encoding") (req "^2.2.0") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.7.3") (default-features #t) (kind 0)) (crate-dep (name "schnorrkel") (req "^0.9.1") (default-features #t) (kind 0)) (crate-dep (name "sp-core") (req "^2.0.0-alpha.8") (default-features #t) (kind 0)))) (hash "163rmwki0kr4f8qx6kjnwqwbfrxcpknp5kbs2sqizsjyjj7n80xy") (yanked #t)))

(define-public crate-dotvanity-0.1 (crate (name "dotvanity") (vers "0.1.2") (deps (list (crate-dep (name "clap") (req "^2.33.1") (default-features #t) (kind 0)) (crate-dep (name "data-encoding") (req "^2.2.0") (default-features #t) (kind 0)) (crate-dep (name "sp-core") (req "^2.0.0-alpha.8") (default-features #t) (kind 0)) (crate-dep (name "tiny-bip39") (req "^0.7.0") (default-features #t) (kind 0)))) (hash "0zh36nr1j6salasfcljm010pwvg79mbav0nqqdgjd15z074i4wxg")))

(define-public crate-dotvanity-0.2 (crate (name "dotvanity") (vers "0.2.0") (deps (list (crate-dep (name "clap") (req "^2.33.1") (default-features #t) (kind 0)) (crate-dep (name "data-encoding") (req "^2.2.0") (default-features #t) (kind 0)) (crate-dep (name "sp-core") (req "^2.0.0-alpha.8") (default-features #t) (kind 0)))) (hash "1xr3xry612m187x1lw8mqakcgghkbw66pbh540hfmr9l3y4lsphh")))

(define-public crate-dotvanity-0.2 (crate (name "dotvanity") (vers "0.2.1") (deps (list (crate-dep (name "clap") (req "^2.33.1") (default-features #t) (kind 0)) (crate-dep (name "data-encoding") (req "^2.2.0") (default-features #t) (kind 0)) (crate-dep (name "sp-core") (req "^2.0.0-alpha.8") (default-features #t) (kind 0)))) (hash "0556g59i6lc0zzl6xpdgnvqk5x8cr1wi0ri94k6wzhrnzf1z2ds2")))

(define-public crate-dotvanity-0.2 (crate (name "dotvanity") (vers "0.2.2") (deps (list (crate-dep (name "clap") (req "^2.33.1") (default-features #t) (kind 0)) (crate-dep (name "data-encoding") (req "^2.2.0") (default-features #t) (kind 0)) (crate-dep (name "sp-core") (req "^2.0.0-alpha.8") (default-features #t) (kind 0)))) (hash "0ibhcg57dfdcbsdwfcd8sy995h8mmv49jmk4f2yfr0nz8dn21cbd")))

(define-public crate-dotvanity-0.2 (crate (name "dotvanity") (vers "0.2.6") (deps (list (crate-dep (name "clap") (req "^2.33.1") (default-features #t) (kind 0)) (crate-dep (name "data-encoding") (req "^2.2.0") (default-features #t) (kind 0)) (crate-dep (name "sp-core") (req "^2.0.0-alpha.8") (default-features #t) (kind 0)))) (hash "1kixmziwd5zz7nb6qaj2h06x09dzllnhl0wljggaxg83d22y5q7a")))

(define-public crate-dotvanity-0.2 (crate (name "dotvanity") (vers "0.2.7") (deps (list (crate-dep (name "clap") (req "^2.33.1") (default-features #t) (kind 0)) (crate-dep (name "data-encoding") (req "^2.2.0") (default-features #t) (kind 0)) (crate-dep (name "sp-core") (req "^2.0.0-alpha.8") (default-features #t) (kind 0)))) (hash "15cn9alva1xas1pksvd3f8shd11xlwgls9q2s1368f0aq3f6am41")))

(define-public crate-dotvanity-1 (crate (name "dotvanity") (vers "1.0.0") (deps (list (crate-dep (name "clap") (req "^2.33.1") (default-features #t) (kind 0)) (crate-dep (name "data-encoding") (req "^2.2.1") (default-features #t) (kind 0)) (crate-dep (name "sp-core") (req "^2.0.0-rc3") (default-features #t) (kind 0)))) (hash "1dm1amb5pi4lxrc51a1kam4kk54q59r44lqrcb9xh4ibxspj2nc9")))

