(define-module (crates-io do cm) #:use-module (crates-io))

(define-public crate-docmatic-0.1 (crate (name "docmatic") (vers "0.1.1") (deps (list (crate-dep (name "glob") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "which") (req "^2.0") (default-features #t) (kind 0)))) (hash "090hc4rarfb5wyi1yia52s1m8yz24i15zidqb0sps729i4wmqic5")))

(define-public crate-docmatic-0.1 (crate (name "docmatic") (vers "0.1.2") (deps (list (crate-dep (name "which") (req "^2.0") (default-features #t) (kind 0)))) (hash "1hx85n266lxswqxrbbinqlhi1qcnjgd4cc7v42abg72kmz7fnn4d")))

