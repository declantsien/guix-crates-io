(define-module (crates-io do yo) #:use-module (crates-io))

(define-public crate-doyoumarkdown-0.0.1 (crate (name "doyoumarkdown") (vers "0.0.1") (deps (list (crate-dep (name "anyhow") (req "^1.0.52") (default-features #t) (kind 0)) (crate-dep (name "nom") (req "^7.1.0") (default-features #t) (kind 0)) (crate-dep (name "nom_locate") (req "^4.0.0") (default-features #t) (kind 0)))) (hash "0q7d7p6hg2w2lj9ag7ljqnc6gfh1f0xi315avay140pjzpmbcr99")))

(define-public crate-doyoumarkdown-0.0.2 (crate (name "doyoumarkdown") (vers "0.0.2") (deps (list (crate-dep (name "anyhow") (req "^1.0.52") (default-features #t) (kind 0)) (crate-dep (name "nom") (req "^7.1.0") (default-features #t) (kind 0)) (crate-dep (name "nom_locate") (req "^4.0.0") (default-features #t) (kind 0)))) (hash "1nq0y00asanwcwnlpwgqyvccqdsk5zvpsydxnx4xvrdcicd1ykg8")))

