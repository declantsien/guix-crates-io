(define-module (crates-io do gs) #:use-module (crates-io))

(define-public crate-dogs-1 (crate (name "dogs") (vers "1.0.0") (deps (list (crate-dep (name "fxhash") (req "^0.2.1") (default-features #t) (kind 0)) (crate-dep (name "human_format") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "min-max-heap") (req "^1.3.0") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.7.3") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.114") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.57") (default-features #t) (kind 0)))) (hash "0i3qawvxb0n852d90daqjphndww76x4babylgf83v8cqdkyjfvhv")))

(define-public crate-dogs-1 (crate (name "dogs") (vers "1.1.0") (deps (list (crate-dep (name "fxhash") (req "^0.2.1") (default-features #t) (kind 0)) (crate-dep (name "human_format") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "min-max-heap") (req "^1.3.0") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.7.3") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.114") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.57") (default-features #t) (kind 0)))) (hash "16jsizhndanv02c042c0ndm8nn8fbadr062bz7vv9sq59mabqcwb")))

(define-public crate-dogs-1 (crate (name "dogs") (vers "1.2.0") (deps (list (crate-dep (name "bit-set") (req "^0.5.2") (default-features #t) (kind 0)) (crate-dep (name "criterion") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "fxhash") (req "^0.2.1") (default-features #t) (kind 0)) (crate-dep (name "human_format") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "min-max-heap") (req "^1.3.0") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.3") (default-features #t) (kind 0)) (crate-dep (name "rl-bandit") (req "^1.0.2") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.114") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.57") (default-features #t) (kind 0)))) (hash "02264bca0jsgb33q3xppw4k4r1ds5inzz79mdpi89iy8rxqz34sf")))

(define-public crate-dogs-1 (crate (name "dogs") (vers "1.3.0") (deps (list (crate-dep (name "bit-set") (req "^0.5.2") (default-features #t) (kind 0)) (crate-dep (name "criterion") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "fxhash") (req "^0.2.1") (default-features #t) (kind 0)) (crate-dep (name "human_format") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "min-max-heap") (req "^1.3.0") (default-features #t) (kind 0)) (crate-dep (name "ordered-float") (req "^2.0.0") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.3") (default-features #t) (kind 0)) (crate-dep (name "rl-bandit") (req "^1.0.2") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.114") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.57") (default-features #t) (kind 0)))) (hash "18nb7j1y0bc27wrm6hd66s9s7yg8q92z66mki82fj2vsba4q00l0")))

(define-public crate-dogsay-0.1 (crate (name "dogsay") (vers "0.1.0") (deps (list (crate-dep (name "assert_cmd") (req "^2.0.4") (default-features #t) (kind 0)) (crate-dep (name "colored") (req "^1.7.0") (default-features #t) (kind 0)) (crate-dep (name "exitfailure") (req "^0.5.1") (default-features #t) (kind 0)) (crate-dep (name "failure") (req "^0.1.5") (default-features #t) (kind 0)) (crate-dep (name "predicates") (req "^1.0.0") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.2.15") (default-features #t) (kind 0)))) (hash "04yss7zi6cl2sdjbalg86nrvh2s5gc8armdvsl4xgjs9a9pag83f")))

(define-public crate-dogstatsd-0.1 (crate (name "dogstatsd") (vers "0.1.0") (deps (list (crate-dep (name "chrono") (req "^0.2") (default-features #t) (kind 0)))) (hash "0lf5m32a0h0k8r39zifg1744gk6i6sw1w8ggy0s82sapdh6x3k1y")))

(define-public crate-dogstatsd-0.1 (crate (name "dogstatsd") (vers "0.1.1") (deps (list (crate-dep (name "chrono") (req "^0.2") (default-features #t) (kind 0)))) (hash "0khv55i811nzfkchdf2zmnnf2bkflri0h6irr2qgwvmp48xf8rsj")))

(define-public crate-dogstatsd-0.2 (crate (name "dogstatsd") (vers "0.2.0") (deps (list (crate-dep (name "chrono") (req "^0.2") (default-features #t) (kind 0)))) (hash "04sxqjw4i9phqh21v47vbsy35skmjyrvy8vlp3i8cwg5l6gg7pkr") (features (quote (("unstable"))))))

(define-public crate-dogstatsd-0.3 (crate (name "dogstatsd") (vers "0.3.0") (deps (list (crate-dep (name "chrono") (req "^0.2") (default-features #t) (kind 0)))) (hash "0awmcxkc6n5bi384wjzigkbmgqkflfzysl9i1apwszdzslicqsyg") (features (quote (("unstable"))))))

(define-public crate-dogstatsd-0.3 (crate (name "dogstatsd") (vers "0.3.1") (deps (list (crate-dep (name "chrono") (req "^0.2") (default-features #t) (kind 0)))) (hash "0ax2i18ia1bgssl66hdwa0lxd113azr8n86mr1sy4z0ysmk9g8c8") (features (quote (("unstable"))))))

(define-public crate-dogstatsd-0.4 (crate (name "dogstatsd") (vers "0.4.0") (deps (list (crate-dep (name "chrono") (req "^0.4") (default-features #t) (kind 0)))) (hash "195hr95d9cf55172994b5wrvwxnfvl686krfdga5gn6k5av0vr7k") (features (quote (("unstable"))))))

(define-public crate-dogstatsd-0.4 (crate (name "dogstatsd") (vers "0.4.1") (deps (list (crate-dep (name "chrono") (req "^0.4") (default-features #t) (kind 0)))) (hash "0p1v9gs2549zw5lzrjp3yy5fmn23p5c33v9r878dqnypmahligpc") (features (quote (("unstable"))))))

(define-public crate-dogstatsd-0.5 (crate (name "dogstatsd") (vers "0.5.0") (deps (list (crate-dep (name "chrono") (req "^0.4") (default-features #t) (kind 0)))) (hash "1yjww2hygahavi82m2yws6pdz5bjpp391ms1vkws3752dx3mci88") (features (quote (("unstable"))))))

(define-public crate-dogstatsd-0.6 (crate (name "dogstatsd") (vers "0.6.0") (deps (list (crate-dep (name "chrono") (req "^0.4") (default-features #t) (kind 0)))) (hash "1bs6yr2gfqxk49n80n092kz35mpzd830an55i6zmfsdd1c0cldsq") (features (quote (("unstable"))))))

(define-public crate-dogstatsd-0.6 (crate (name "dogstatsd") (vers "0.6.1") (deps (list (crate-dep (name "chrono") (req "^0.4") (default-features #t) (kind 0)))) (hash "1cd06zm9zrmm40ws122v069r3cv9hn37br4m87zyai3j5ldknbrr") (features (quote (("unstable"))))))

(define-public crate-dogstatsd-0.6 (crate (name "dogstatsd") (vers "0.6.2") (deps (list (crate-dep (name "chrono") (req "^0.4") (default-features #t) (kind 0)))) (hash "1lq8dhsbmi8w2jrlixbhj33gy5sqa51sr60qfb75gach51dar4ih") (features (quote (("unstable"))))))

(define-public crate-dogstatsd-0.7 (crate (name "dogstatsd") (vers "0.7.0") (deps (list (crate-dep (name "chrono") (req "^0.4") (default-features #t) (kind 0)))) (hash "0q8jdypsm0dpvbizpad5hfqicr9bmq29wym54jahjh5yvn7nppvn") (features (quote (("unstable"))))))

(define-public crate-dogstatsd-0.7 (crate (name "dogstatsd") (vers "0.7.1") (deps (list (crate-dep (name "chrono") (req "^0.4") (features (quote ("clock"))) (kind 0)))) (hash "0ggr4mpgyanqd4hxwxrm17m71nnm099x6wygh40ac3yzkr77bg1c") (features (quote (("unstable"))))))

(define-public crate-dogstatsd-0.8 (crate (name "dogstatsd") (vers "0.8.0") (deps (list (crate-dep (name "chrono") (req "^0.4") (features (quote ("clock"))) (kind 0)))) (hash "19hgnhywk2hvn112x6hd6zn5nkhjsna984wlkdjl5fsbpbnsb0n4") (features (quote (("unstable"))))))

(define-public crate-dogstatsd-0.8 (crate (name "dogstatsd") (vers "0.8.1") (deps (list (crate-dep (name "chrono") (req "^0.4") (features (quote ("clock"))) (kind 0)))) (hash "054fyjsaal46g47wd9al87x3kl4p0ppj1y1dbppgr91j752d22a6") (features (quote (("unstable"))))))

(define-public crate-dogstatsd-0.9 (crate (name "dogstatsd") (vers "0.9.0") (deps (list (crate-dep (name "chrono") (req "^0.4") (features (quote ("clock"))) (kind 0)))) (hash "0gy4pz57wn24vckf70z1af55jazpz4ciih85b6xhk8awg060p3s5") (features (quote (("unstable"))))))

(define-public crate-dogstatsd-0.10 (crate (name "dogstatsd") (vers "0.10.0") (deps (list (crate-dep (name "chrono") (req "^0.4") (features (quote ("clock"))) (kind 0)) (crate-dep (name "retry") (req "^2.0.0") (default-features #t) (kind 0)))) (hash "0cp7k6s3qx100ah5wlqn95rhc93vlm6jmfyy32mlc0znqfb25qhx") (features (quote (("unstable"))))))

(define-public crate-dogstatsd-0.11 (crate (name "dogstatsd") (vers "0.11.0") (deps (list (crate-dep (name "chrono") (req "^0.4") (features (quote ("clock"))) (kind 0)) (crate-dep (name "retry") (req "^2.0.0") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.32") (features (quote ("full"))) (default-features #t) (kind 2)))) (hash "0hqihysrsnw4x4rq66rxwz8dbhqwavsbih3f7agm8sr8k3vm7ygw") (features (quote (("unstable"))))))

(define-public crate-dogstatsd-0.11 (crate (name "dogstatsd") (vers "0.11.1") (deps (list (crate-dep (name "chrono") (req "^0.4") (features (quote ("clock"))) (kind 0)) (crate-dep (name "retry") (req "^2.0.0") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.32") (features (quote ("full"))) (default-features #t) (kind 2)))) (hash "1xqcbmgkgniqichj5ql29lnrgjdyskddp38j4pvfqxsygp0jbjqh") (features (quote (("unstable"))))))

(define-public crate-dogstatsd-tokio-client-0.0.0 (crate (name "dogstatsd-tokio-client") (vers "0.0.0") (hash "0ia3ip5xvzfp2kzvlpw01dyp3mhxap4q1n6s73smgakm3w1zhl3r")))

