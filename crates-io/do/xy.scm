(define-module (crates-io do xy) #:use-module (crates-io))

(define-public crate-doxygen-rs-0.2 (crate (name "doxygen-rs") (vers "0.2.2") (deps (list (crate-dep (name "criterion") (req "^0.4") (default-features #t) (kind 2)) (crate-dep (name "phf") (req "^0.11") (features (quote ("macros"))) (default-features #t) (kind 0)))) (hash "01bm7s8g334ildrq5xzflwhgbf72plzf59pgwwa89fwv1km71xmz")))

(define-public crate-doxygen-rs-0.3 (crate (name "doxygen-rs") (vers "0.3.0") (deps (list (crate-dep (name "criterion") (req "^0.4") (default-features #t) (kind 2)) (crate-dep (name "phf") (req "^0.11") (features (quote ("macros"))) (default-features #t) (kind 0)))) (hash "1fvf9wx8prrcwl82y126zn7f3ipf9dldkn1isw6q6xr6iq8ny7d4")))

(define-public crate-doxygen-rs-0.3 (crate (name "doxygen-rs") (vers "0.3.1") (deps (list (crate-dep (name "criterion") (req "^0.4") (default-features #t) (kind 2)) (crate-dep (name "phf") (req "^0.11") (features (quote ("macros"))) (default-features #t) (kind 0)))) (hash "0d0din2qf8rsz6sj7ppavdv62smb63cc4h8hmgh625bc4p76qy82")))

(define-public crate-doxygen-rs-0.4 (crate (name "doxygen-rs") (vers "0.4.0") (deps (list (crate-dep (name "phf") (req "^0.11") (features (quote ("macros"))) (default-features #t) (kind 0)))) (hash "0mrqlf99gkcmsqcr4sfxx6qxci8i42jk5c0823kld9c4xpkv407i")))

(define-public crate-doxygen-rs-0.4 (crate (name "doxygen-rs") (vers "0.4.1") (deps (list (crate-dep (name "phf") (req "^0.11") (features (quote ("macros"))) (default-features #t) (kind 0)))) (hash "146xg2nx6pikfnywdi7jgvq68jzmqw8akmq7hlaakiwcpgh4lp7f")))

(define-public crate-doxygen-rs-0.4 (crate (name "doxygen-rs") (vers "0.4.2") (deps (list (crate-dep (name "phf") (req "^0.11") (features (quote ("macros"))) (default-features #t) (kind 0)))) (hash "1n872n45ivhj3pqfwhbi7cvx00rn76a72x368ricykfkh33nwns1")))

