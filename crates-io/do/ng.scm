(define-module (crates-io do ng) #:use-module (crates-io))

(define-public crate-dong-1 (crate (name "dong") (vers "1.0.0") (deps (list (crate-dep (name "rodio") (req "^0.17.1") (default-features #t) (kind 0)))) (hash "13xla353lrvdgrpnwkmk7qyrc07v1zdkwmv5qwnfzs8fwhbd6f0b")))

(define-public crate-dong-1 (crate (name "dong") (vers "1.0.1") (deps (list (crate-dep (name "rodio") (req "^0.17.1") (default-features #t) (kind 0)))) (hash "06m86iz9ysvkw2x9l6ldxgfwxfz4wvqwds8yqy07d9v3qslbsfk4")))

(define-public crate-dong-1 (crate (name "dong") (vers "1.0.2") (deps (list (crate-dep (name "rodio") (req "^0.17.1") (default-features #t) (kind 0)))) (hash "1myhdlq1kn406g0k99jz749hcrrnwcjxbqzks18pmbhziy0q15ba")))

(define-public crate-dong-1 (crate (name "dong") (vers "1.0.4") (deps (list (crate-dep (name "rodio") (req "^0.17.1") (default-features #t) (kind 0)))) (hash "0d6iwplrr1bdklgck9bn7j9xbyf97r3fwcp0xgcnfqida8kr51jc")))

(define-public crate-dong-1 (crate (name "dong") (vers "1.0.5") (deps (list (crate-dep (name "rodio") (req "^0.17.1") (default-features #t) (kind 0)))) (hash "1kww76rzv23xd69798578mr94zyryrhi7wgs3fz35b50k61795d7")))

(define-public crate-dong1e-grep-0.1 (crate (name "dong1e-grep") (vers "0.1.0") (hash "1wv4fqnssx8f04vdh656bq6d7qdlb2hrfhqb4h76ri0gh251530d")))

(define-public crate-dong_crate-0.1 (crate (name "dong_crate") (vers "0.1.1") (hash "02bmvw4wabmxwqvj8hymkygs82bk050wmpxjnaz6bzccrgb20p02")))

(define-public crate-dongi-0.1 (crate (name "dongi") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^4.0.32") (features (quote ("derive" "unicode"))) (default-features #t) (kind 0)))) (hash "1i2si34ri6ijnp3d6n28x2rs1hg152fnbsx9wy4qb4wm8rdwmndz")))

(define-public crate-dongsong-first-crate-test-0.1 (crate (name "dongsong-first-crate-test") (vers "0.1.0") (hash "0f16lhpwcr43by0h8yy1qak7y2jvq7sji4c5gdqqz2ks1q9s6pkp")))

