(define-module (crates-io do nt) #:use-module (crates-io))

(define-public crate-dont-0.1 (crate (name "dont") (vers "0.1.0") (deps (list (crate-dep (name "cfg-if") (req "^1.0.0") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^3.1.6") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "mockall") (req "^0.11.0") (default-features #t) (kind 2)) (crate-dep (name "which") (req "^4.2.4") (default-features #t) (kind 0)))) (hash "1asd121ynd7wnhnm2n7fj976w29rcbak0npk7rly0xhjn6zpwrcw")))

(define-public crate-dont-expand-0.1 (crate (name "dont-expand") (vers "0.1.0") (deps (list (crate-dep (name "paste") (req "^1") (default-features #t) (kind 0)))) (hash "193k8iqz7x6dw1iinwr1dczfp9hc3np9254x1kzzyxs1ds9nmald")))

(define-public crate-dont_disappear-0.0.1 (crate (name "dont_disappear") (vers "0.0.1") (hash "1ynrk61pf1z9cp7jzcwy1cmzmn84ljcy20msnxks0xp2hac5i99r")))

(define-public crate-dont_disappear-0.0.2 (crate (name "dont_disappear") (vers "0.0.2") (hash "0pszvxmfghs27znq484hr0hlmf7q8m1fqc2imzbr4pm7qnjqy8q1")))

(define-public crate-dont_disappear-0.0.3 (crate (name "dont_disappear") (vers "0.0.3") (hash "0xddvxm17cp5bqvrdw317zlyqg0la8p34vhyid4mmrxg7wf9475n")))

(define-public crate-dont_disappear-0.0.4 (crate (name "dont_disappear") (vers "0.0.4") (hash "1bdjvi4fvl6caglwlgqj1iz9f27xw3sirjx99imzf9x2zfmc9a44")))

(define-public crate-dont_disappear-0.0.5 (crate (name "dont_disappear") (vers "0.0.5") (hash "19dpbvwlw0gz9jwnj50d08gbqzwz85kqa032wm3ly4xjb64cgrdw")))

(define-public crate-dont_disappear-0.0.6 (crate (name "dont_disappear") (vers "0.0.6") (hash "1n2nxl68hg77kl2xx2ganxz3inxichnkfkbqrlyhkimmnm8sghc2")))

(define-public crate-dont_disappear-0.0.8 (crate (name "dont_disappear") (vers "0.0.8") (hash "11zx24yg02m050bz74g44g2swmgaqq49zm3a9ia4lb3krzmy4vag")))

(define-public crate-dont_disappear-0.0.9 (crate (name "dont_disappear") (vers "0.0.9") (hash "1qxfn8dklw18vn6r9s5335hrl9d2mz6svzs36dz2xpd2mw30vpvi")))

(define-public crate-dont_disappear-0.0.10 (crate (name "dont_disappear") (vers "0.0.10") (hash "0zx7dxsa18ha7n4br8k4yshvbyx41r16a0nqyah9vp8hx3lzzird")))

(define-public crate-dont_disappear-0.0.7 (crate (name "dont_disappear") (vers "0.0.7") (hash "1dvajv7s2gf61bwfq375r1k3g2z33hfz1nwia6k05m48kphr1hr7")))

(define-public crate-dont_disappear-1 (crate (name "dont_disappear") (vers "1.0.0") (hash "03bnsxdxn95az566qw346g3nh9p4bzpiv9kj1skdsd6z2qwqr5mi")))

(define-public crate-dont_disappear-1 (crate (name "dont_disappear") (vers "1.0.1") (hash "1mih6s93qs1bh0c9sb6hfdqfcs4mnf3jsrv8b6gdfc6z5gxrgp8x")))

(define-public crate-dont_disappear-2 (crate (name "dont_disappear") (vers "2.0.0") (deps (list (crate-dep (name "crossterm") (req "^0.4") (default-features #t) (kind 0)))) (hash "1wq1f15qs5ns1xbm8qfvc7j2d47ix62vxs28hjjdppp22kwmkv97")))

(define-public crate-dont_disappear-2 (crate (name "dont_disappear") (vers "2.0.1") (deps (list (crate-dep (name "crossterm") (req "^0.4") (default-features #t) (kind 0)))) (hash "1swyqz8qbdk2js80jl0ih44y3xmqb5byrwi3v851yhkzr33k2nif")))

(define-public crate-dont_disappear-2 (crate (name "dont_disappear") (vers "2.1.0") (deps (list (crate-dep (name "crossterm") (req "^0.4") (default-features #t) (kind 0)))) (hash "0vaknal83kjjm0jmnlnjg4pai98s5nzsx7pmwq0ii0pk6pzpcj7l")))

(define-public crate-dont_disappear-2 (crate (name "dont_disappear") (vers "2.1.3") (deps (list (crate-dep (name "crossterm") (req "^0.4") (default-features #t) (kind 0)))) (hash "1k6s0njldqwm73g6bk95j0vma3fbx7g87pqpagcpv6vkysij5gxf")))

(define-public crate-dont_disappear-2 (crate (name "dont_disappear") (vers "2.1.4") (deps (list (crate-dep (name "crossterm") (req "^0.4") (default-features #t) (kind 0)))) (hash "0868j83m1ni1bwym5kc6cva2d4xd6ss74zkicz4hf1g1ckd26s4g")))

(define-public crate-dont_disappear-2 (crate (name "dont_disappear") (vers "2.1.5") (deps (list (crate-dep (name "crossterm") (req "^0.4") (default-features #t) (kind 0)))) (hash "1dmf97yb68r8idm65205xklxjki46rgfi400y56wbzaw82k41ncy")))

(define-public crate-dont_disappear-2 (crate (name "dont_disappear") (vers "2.1.6") (deps (list (crate-dep (name "crossterm") (req "^0.4") (default-features #t) (kind 0)))) (hash "0abn52rxf70jizmg86cgswwzxdpc18mbw6xrlrir4n65wkx74z3b")))

(define-public crate-dont_disappear-2 (crate (name "dont_disappear") (vers "2.1.8") (deps (list (crate-dep (name "crossterm") (req "^0.4") (default-features #t) (kind 0)))) (hash "1x6crpjqyiardfw3m42b7whb6ivf96kbsxmm648g227l651ar37r")))

(define-public crate-dont_disappear-2 (crate (name "dont_disappear") (vers "2.1.7") (deps (list (crate-dep (name "crossterm") (req "^0.4") (default-features #t) (kind 0)))) (hash "15bsyg7lrpbqj9yhq1wql8xd8w380xq5ccxvhdq5rz2dhhmsn2lx")))

(define-public crate-dont_disappear-2 (crate (name "dont_disappear") (vers "2.1.10") (deps (list (crate-dep (name "crossterm") (req "^0.4") (default-features #t) (kind 0)))) (hash "15nl9j7c0liypibfw06jw9d29986pc8al9qh43238x1syr5b22ip")))

(define-public crate-dont_disappear-2 (crate (name "dont_disappear") (vers "2.1.11") (deps (list (crate-dep (name "crossterm") (req "^0.4") (default-features #t) (kind 0)))) (hash "02xskamlal6al017v3wnkicl27jdpyc6nxsi2r0zg153cfzbrpxx")))

(define-public crate-dont_disappear-2 (crate (name "dont_disappear") (vers "2.1.12") (deps (list (crate-dep (name "crossterm") (req "^0.4") (default-features #t) (kind 0)))) (hash "15s1lj609kg1bbhv7s4pa7j4w5vi246rb27n8i8a2bn3qdprr9lh")))

(define-public crate-dont_disappear-2 (crate (name "dont_disappear") (vers "2.2.0") (deps (list (crate-dep (name "crossterm") (req "^0.4") (default-features #t) (kind 0)))) (hash "1g3z1jzmyfcgxdvxkrv642473km21ykygvdrij3xd4vkc2nh39hf")))

(define-public crate-dont_disappear-2 (crate (name "dont_disappear") (vers "2.2.1") (deps (list (crate-dep (name "crossterm") (req "^0.5") (default-features #t) (kind 0)))) (hash "19hgbk6wbn6bn2dzk4psvlaz4svss10rp4kp48yn4n68j5cjxny9")))

(define-public crate-dont_disappear-3 (crate (name "dont_disappear") (vers "3.0.0") (deps (list (crate-dep (name "crossterm") (req "^0.5") (default-features #t) (kind 0)))) (hash "11mrxj1w96rf9wqjbwwkgprafr7ja3zbxwcq6c5di48dyrafbcya")))

(define-public crate-dont_disappear-3 (crate (name "dont_disappear") (vers "3.0.1") (deps (list (crate-dep (name "crossterm") (req "^0.8") (default-features #t) (kind 0)))) (hash "1dn1zzlhi3bsh78siflr86a78cbjfpjp9bhy0miidwrkb42vpll0")))

(define-public crate-dont_panic-0.1 (crate (name "dont_panic") (vers "0.1.0") (hash "1ycbldfm0ya5f1dps383bci7r9m8zf5ldykyq110byglc998nyxj") (features (quote (("panic") ("default"))))))

(define-public crate-dont_panic_slice-0.1 (crate (name "dont_panic_slice") (vers "0.1.0") (deps (list (crate-dep (name "dont_panic") (req "^0.1") (default-features #t) (kind 0)))) (hash "1w4wc7s2jq9ixs0vs8gnd5sy49wcfql3gn9nfbnf75hi5zbmpvj0") (features (quote (("panic" "dont_panic/panic") ("default"))))))

(define-public crate-dontfrag-1 (crate (name "dontfrag") (vers "1.0.0") (deps (list (crate-dep (name "cfg-if") (req "^1.0.0") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2.150") (default-features #t) (target "cfg(unix)") (kind 0)) (crate-dep (name "tokio") (req "^1.35.0") (features (quote ("net"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "windows-sys") (req "^0.52.0") (features (quote ("Win32_Networking_WinSock"))) (default-features #t) (target "cfg(windows)") (kind 0)))) (hash "1bg4vc9f0nyr63a0vam38i5s5z9l7afs3rhclbrdadlwr0bfqhrl")))

(define-public crate-dontfrag-1 (crate (name "dontfrag") (vers "1.0.1") (deps (list (crate-dep (name "cfg-if") (req "^1.0.0") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2.150") (default-features #t) (target "cfg(unix)") (kind 0)) (crate-dep (name "tokio") (req "^1.35.0") (features (quote ("net"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "windows-sys") (req "^0.52.0") (features (quote ("Win32_Networking_WinSock"))) (default-features #t) (target "cfg(windows)") (kind 0)))) (hash "15sk2psihki9nhp2llw5qlq8fbvsl0kskzjd3d3vl9cbbagr85y1")))

(define-public crate-dontpanic-0.1 (crate (name "dontpanic") (vers "0.1.0") (hash "08nk5c097kzcm2pjlffx2gnh4kc4yrvgp1ii3qmwpsb7dzr3x29g")))

(define-public crate-dontpayfull-0.0.0 (crate (name "dontpayfull") (vers "0.0.0") (hash "0m1i0vb8an230civnmf3qsfn4dzsl6wgk2mv76588bj3fwymp7cl")))

(define-public crate-dontreaddocs-0.1 (crate (name "dontreaddocs") (vers "0.1.0") (hash "15fs0g618vswwwpka7gpa769ig5w0diwckqlrwzs1s6cw4a1hrdd")))

(define-public crate-dontreaddocs-0.1 (crate (name "dontreaddocs") (vers "0.1.1") (hash "15kyxpmwcxzvwxha97zgn59l38yqji8y8hgg81y2y351kx8a26z1")))

(define-public crate-dontshare-1 (crate (name "dontshare") (vers "1.0.0") (hash "097hk7kc0mq143zph99nq90kkbz0g608z18s8aqxvlcafqb6sgca") (features (quote (("nightly"))))))

(define-public crate-dontshare-1 (crate (name "dontshare") (vers "1.1.0") (hash "0bkx6bp4rfsngsjn1w8g8w3hm3f8w1fxdzkpv6vqk06yz9nd02cq") (features (quote (("nightly"))))))

(define-public crate-dontshare-1 (crate (name "dontshare") (vers "1.2.0") (hash "1p7acrs26mr95y6l3km5a4qszxy01108djxg97klc4m9s1jyy7zl") (features (quote (("nightly"))))))

(define-public crate-dontuse-this-001-0.1 (crate (name "dontuse-this-001") (vers "0.1.0") (hash "185xhv7pmdw8p87zzcgqdzbcjhnscl0a49msaqx23q941y0zc7rq") (yanked #t)))

(define-public crate-dontuse-this-001-0.1 (crate (name "dontuse-this-001") (vers "0.1.1") (hash "0d8ld3jvzidxjd3nkhpl0xjyrc451q7cf1kfy9n21phlgaxzpsdw")))

(define-public crate-dontuse-this-002-0.1 (crate (name "dontuse-this-002") (vers "0.1.0") (hash "01s270gi53y9kh3wgm1nxnxd85aqm69vszir7991d6cbvrsn0dfl")))

(define-public crate-dontuse-this-002-0.1 (crate (name "dontuse-this-002") (vers "0.1.1") (hash "14r9in62xn6wkx0lr6wvcir39j222ybigwh8gwi7l9v5kybnsyn7")))

