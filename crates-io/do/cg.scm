(define-module (crates-io do cg) #:use-module (crates-io))

(define-public crate-docgen-0.1 (crate (name "docgen") (vers "0.1.0") (hash "1vcr8201s5nh72wp0yz0j0bjhfbfhc469kzvrhjs0ps0dc1s8zx4")))

(define-public crate-docgen-0.1 (crate (name "docgen") (vers "0.1.1") (hash "1q8wpl461qd4jmc0bm78x8gpg2zapnc5zzw8bydh5nvbz4zpgzcc")))

(define-public crate-docgen-0.1 (crate (name "docgen") (vers "0.1.2") (hash "0cghcg9jliskhvqqx63y4fm5648wigrds3a10br6g898cfgg7f16")))

