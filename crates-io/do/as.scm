(define-module (crates-io do as) #:use-module (crates-io))

(define-public crate-doas-rs-0.1 (crate (name "doas-rs") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^4.4.6") (features (quote ("derive" "cargo"))) (default-features #t) (kind 0)) (crate-dep (name "color-eyre") (req "^0.6.2") (default-features #t) (kind 0)) (crate-dep (name "human-panic") (req "^1.2.2") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2.148") (default-features #t) (kind 0)) (crate-dep (name "pam") (req "^0.8.0") (default-features #t) (kind 0)) (crate-dep (name "whoami") (req "^1.4.1") (default-features #t) (kind 0)))) (hash "1iic30kw9xwm7543yps1ds6h90y8gb8zlqnf0nbpqbdzdc3m2v7v")))

