(define-module (crates-io do od) #:use-module (crates-io))

(define-public crate-doodle-0.1 (crate (name "doodle") (vers "0.1.0") (deps (list (crate-dep (name "doodle_derive") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)))) (hash "1swpifzl8f0ggsnag1h7qrc7ihcaq4jkj3ipk5vvvvzif7dmfg9w")))

(define-public crate-doodle-0.1 (crate (name "doodle") (vers "0.1.1") (deps (list (crate-dep (name "doodle_derive") (req "^0.1.1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)))) (hash "0kv2m20g8abqlw1sbcqqy111p29ipq315w8j7dln2mzzd0phhavy") (features (quote (("derive" "doodle_derive"))))))

(define-public crate-doodle-0.1 (crate (name "doodle") (vers "0.1.2") (deps (list (crate-dep (name "doodle_derive") (req "^0.1.2") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)))) (hash "01nhf5cskbnqvvc18d7mfic39rj36r845zc9544a2zfwr9cq02md") (features (quote (("derive" "doodle_derive"))))))

(define-public crate-doodle-0.2 (crate (name "doodle") (vers "0.2.0") (deps (list (crate-dep (name "doodle_derive") (req "^0.2.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)))) (hash "0nf1xddv80d9af4arkh0dawirahxg22kzbvpl6zkvpg4kg4kax53") (features (quote (("derive" "doodle_derive"))))))

(define-public crate-doodle_derive-0.1 (crate (name "doodle_derive") (vers "0.1.0") (deps (list (crate-dep (name "quote") (req "^0.6") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^0.15") (default-features #t) (kind 0)))) (hash "05x54an9g9r9vxcsyr6ahn9wqqymyva361h4b7jifrjgvkq85n41")))

(define-public crate-doodle_derive-0.1 (crate (name "doodle_derive") (vers "0.1.1") (deps (list (crate-dep (name "quote") (req "^0.6") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^0.15") (default-features #t) (kind 0)))) (hash "1kxwh3swc8fm1qqzjxlbgaa313l3f9dsipr3kh587wj81ppssj6m")))

(define-public crate-doodle_derive-0.1 (crate (name "doodle_derive") (vers "0.1.2") (deps (list (crate-dep (name "quote") (req "^0.6") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^0.15") (default-features #t) (kind 0)))) (hash "05qn616gqhxg3d9ald82npcxdxcfx6f88mizlmp18i7lg9sfp039")))

(define-public crate-doodle_derive-0.2 (crate (name "doodle_derive") (vers "0.2.0") (deps (list (crate-dep (name "proc-macro2") (req "^0.4.27") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^0.6") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^0.15") (default-features #t) (kind 0)))) (hash "1zapgd3c2j3f8qjbzp0dw2r4aasrjadk67b65g03jj8h59z9cg60")))

