(define-module (crates-io do ml) #:use-module (crates-io))

(define-public crate-domlist-1 (crate (name "domlist") (vers "1.6.0") (deps (list (crate-dep (name "clap") (req "^4.3.4") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "prettytable-rs") (req "^0.10") (default-features #t) (kind 0)) (crate-dep (name "ssh2") (req "^0.9.4") (default-features #t) (kind 0)))) (hash "09hrxyivm8mmrjd3ywhr8a5fsq42cfsa4dbsnrr61g3r7z7kl8zb")))

(define-public crate-domlist-1 (crate (name "domlist") (vers "1.6.1") (deps (list (crate-dep (name "clap") (req "^4.3.4") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "prettytable-rs") (req "^0.10") (default-features #t) (kind 0)) (crate-dep (name "ssh2") (req "^0.9.4") (default-features #t) (kind 0)))) (hash "02y0bas7p63h94sx0j4gargqin42ccz9w0ff07mv8h5v4bnbxczs")))

(define-public crate-domlist-1 (crate (name "domlist") (vers "1.6.2") (deps (list (crate-dep (name "clap") (req "^4.3.4") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "prettytable-rs") (req "^0.10") (default-features #t) (kind 0)) (crate-dep (name "ssh2") (req "^0.9.4") (default-features #t) (kind 0)))) (hash "1mcgi2gy8wygjcfx89552qkc2svgg0r8lhzr7da6s1pz88p38qsg")))

(define-public crate-domlist-1 (crate (name "domlist") (vers "1.6.3") (deps (list (crate-dep (name "clap") (req "^4.3.4") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "prettytable-rs") (req "^0.10") (default-features #t) (kind 0)) (crate-dep (name "ssh2") (req "^0.9.4") (default-features #t) (kind 0)))) (hash "1xa7niyrkwxy4qfzsz6m57plb19r64kcx3dc1avqfr1srgi44v1k")))

(define-public crate-domlist-1 (crate (name "domlist") (vers "1.7.0") (deps (list (crate-dep (name "clap") (req "^4.3.4") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "prettytable-rs") (req "^0.10") (default-features #t) (kind 0)) (crate-dep (name "ssh2") (req "^0.9.4") (default-features #t) (kind 0)))) (hash "1k5jgg82g4nlhi8zw1kl1cz32p212a02bsbfdi4wjblcp5c0sgrh")))

(define-public crate-domlist-1 (crate (name "domlist") (vers "1.7.1") (deps (list (crate-dep (name "clap") (req "^4.3.4") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "prettytable-rs") (req "^0.10") (default-features #t) (kind 0)) (crate-dep (name "ssh2") (req "^0.9.4") (default-features #t) (kind 0)))) (hash "03qblbyll45310bjgkrcqi68qwgx5mg2hmb118gvfrkfgnxwzkzd")))

(define-public crate-domlist-1 (crate (name "domlist") (vers "1.7.2") (deps (list (crate-dep (name "clap") (req "^4.3.4") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "prettytable-rs") (req "^0.10") (default-features #t) (kind 0)) (crate-dep (name "ssh2") (req "^0.9.4") (default-features #t) (kind 0)))) (hash "1wckkh0jpyzfmax4lb8qpsyi730g4rkgilql75admjbvxzlslkhq")))

(define-public crate-domlist-1 (crate (name "domlist") (vers "1.7.3") (deps (list (crate-dep (name "clap") (req "^4.3.4") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "prettytable-rs") (req "^0.10") (default-features #t) (kind 0)) (crate-dep (name "ssh2") (req "^0.9.4") (default-features #t) (kind 0)))) (hash "0z4ywszhj3j82gzslfsbxnm24lfg9vkagzmymn4c89mw5pvhw46h")))

(define-public crate-domlist-1 (crate (name "domlist") (vers "1.7.4") (deps (list (crate-dep (name "clap") (req "^4.3.4") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "prettytable-rs") (req "^0.10") (default-features #t) (kind 0)) (crate-dep (name "ssh2") (req "^0.9.4") (default-features #t) (kind 0)))) (hash "0lmlsjpvns39qhn5kk1f01p6wi75k7zvawd7ynxzcqnhb3pcy0qs")))

(define-public crate-domlist-1 (crate (name "domlist") (vers "1.7.5") (deps (list (crate-dep (name "clap") (req "^4.3.4") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "prettytable-rs") (req "^0.10") (default-features #t) (kind 0)) (crate-dep (name "ssh2") (req "^0.9.4") (default-features #t) (kind 0)))) (hash "0m49rkw8zwxb06ljkhfip67j6p48d7rrj5kwbwpzxk3g1gfxvj28")))

