(define-module (crates-io do op) #:use-module (crates-io))

(define-public crate-doop-0.0.0 (crate (name "doop") (vers "0.0.0-0.0.0-0.0.0") (hash "1c8di3yzvpl1a30hldd58ls7kwqyzcrwjk55mav0asrdijyy8hwh") (yanked #t)))

(define-public crate-doop-0.0.0 (crate (name "doop") (vers "0.0.0--") (hash "169pyyd5mq3r99w5i3rhran6zz9c9695910drn4dr9r848kdj6b1") (yanked #t)))

