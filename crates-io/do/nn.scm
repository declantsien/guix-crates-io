(define-module (crates-io do nn) #:use-module (crates-io))

(define-public crate-donnager-0.1 (crate (name "donnager") (vers "0.1.0") (deps (list (crate-dep (name "chrono") (req "^0.4.23") (default-features #t) (kind 0)) (crate-dep (name "nalgebra") (req "^0.31.4") (default-features #t) (kind 0)) (crate-dep (name "plotters") (req "^0.3.4") (default-features #t) (kind 0)))) (hash "04lgqq3npwmkzxv2j2yzwfamwb2b8jkgqgzv92jvk1pa36d8zhli")))

(define-public crate-donnager-0.1 (crate (name "donnager") (vers "0.1.1") (deps (list (crate-dep (name "chrono") (req "^0.4.23") (default-features #t) (kind 0)) (crate-dep (name "nalgebra") (req "^0.31.4") (default-features #t) (kind 0)) (crate-dep (name "plotters") (req "^0.3.4") (default-features #t) (kind 0)))) (hash "1km501nzsd1fac20pab4g7yzh9qc33pvy0i1pmsad1lcg8gg4lv2")))

(define-public crate-donnager-0.1 (crate (name "donnager") (vers "0.1.2") (deps (list (crate-dep (name "chrono") (req "^0.4.23") (default-features #t) (kind 0)) (crate-dep (name "nalgebra") (req "^0.31.4") (default-features #t) (kind 0)) (crate-dep (name "plotters") (req "^0.3.4") (default-features #t) (kind 0)))) (hash "0hb15j0yxia2wvm800w9ngclbjk8v2l24b71frvfhnvy118qvfm6")))

