(define-module (crates-io do se) #:use-module (crates-io))

(define-public crate-dose-0.1 (crate (name "dose") (vers "0.1.0") (deps (list (crate-dep (name "dose-derive") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "fake") (req "^2.4") (default-features #t) (kind 2)) (crate-dep (name "log") (req "^0.4.0") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8") (default-features #t) (kind 2)))) (hash "09gzywp756scvw3nhpmj47153ff25qpkr597sqwmfq9p97ggxzy2")))

(define-public crate-dose-0.1 (crate (name "dose") (vers "0.1.1") (deps (list (crate-dep (name "dose-derive") (req "^0.1.1") (default-features #t) (kind 0)) (crate-dep (name "fake") (req "^2.4") (default-features #t) (kind 2)) (crate-dep (name "log") (req "^0.4.0") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8") (default-features #t) (kind 2)))) (hash "1n8ihqh0c2fvm3r28fcvrlf1rxq3n8y4jg527ybcn1nfv72scw8v")))

(define-public crate-dose-0.1 (crate (name "dose") (vers "0.1.2") (deps (list (crate-dep (name "dose-derive") (req "^0.1.2") (default-features #t) (kind 0)) (crate-dep (name "fake") (req "^2.4") (default-features #t) (kind 2)) (crate-dep (name "log") (req "^0.4.0") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8") (default-features #t) (kind 2)))) (hash "1xbwngpkqch7pmmyvs8yqsg4qbph3andrrni1j9r1l3318lvjpax")))

(define-public crate-dose-0.1 (crate (name "dose") (vers "0.1.3") (deps (list (crate-dep (name "dose-derive") (req "^0.1.3") (default-features #t) (kind 0)) (crate-dep (name "fake") (req "^2.4") (default-features #t) (kind 2)) (crate-dep (name "log") (req "^0.4.0") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8") (default-features #t) (kind 2)))) (hash "1ra53jbmg25agxynvyiyw46npcgd3zvbbgwsjrm9apb9p8k8a7j6")))

(define-public crate-dose-derive-0.1 (crate (name "dose-derive") (vers "0.1.0") (deps (list (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "1wla2mx7y9ldfc6726x4kzcfand04zxsx8bwwmqr0m8bgp6z547s")))

(define-public crate-dose-derive-0.1 (crate (name "dose-derive") (vers "0.1.1") (deps (list (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "05jwfbsppwcs1ysr5l5m6d6d60m78k037qwi698g8nb0h2m8nfgr")))

(define-public crate-dose-derive-0.1 (crate (name "dose-derive") (vers "0.1.2") (deps (list (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "0j97387npm4fdw6w8dia6nfp26f1snr11023zqjk4599ki61r9v9")))

(define-public crate-dose-derive-0.1 (crate (name "dose-derive") (vers "0.1.3") (deps (list (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "1v0cc51azmnl3vypax265w9n3vk6p1na1f4w85rr796ppcbrsrdy")))

(define-public crate-dose2gmsh-0.1 (crate (name "dose2gmsh") (vers "0.1.0") (deps (list (crate-dep (name "itertools") (req "^0.8.2") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.3.8") (default-features #t) (kind 0)))) (hash "1rsi5nr5mkw7v6vpvqmp3s5n81vrpr7gcl44pk90raz723126pi8")))

(define-public crate-dose2gmsh-1 (crate (name "dose2gmsh") (vers "1.0.0") (deps (list (crate-dep (name "approx") (req "^0.3.2") (default-features #t) (kind 2)) (crate-dep (name "csv") (req "^1.1.3") (default-features #t) (kind 2)) (crate-dep (name "itertools") (req "^0.8.2") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.3.8") (default-features #t) (kind 0)))) (hash "1kca9y7rcafn6mscr9j7hmph4lw9w508rh1djz1q75y747pcxs1c")))

(define-public crate-dose2gmsh-1 (crate (name "dose2gmsh") (vers "1.0.1") (deps (list (crate-dep (name "approx") (req "^0.3.2") (default-features #t) (kind 2)) (crate-dep (name "csv") (req "^1.1.3") (default-features #t) (kind 2)) (crate-dep (name "itertools") (req "^0.8.2") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.3.8") (default-features #t) (kind 0)))) (hash "0f96206c3yscrc1hsn9v1rg5prph3ly2an3rkzkg4i9zdrl99zs3")))

