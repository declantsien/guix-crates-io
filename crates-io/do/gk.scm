(define-module (crates-io do gk) #:use-module (crates-io))

(define-public crate-dogkv-0.1 (crate (name "dogkv") (vers "0.1.0") (deps (list (crate-dep (name "assert_cmd") (req "^0.11.0") (default-features #t) (kind 2)) (crate-dep (name "clap") (req "^4.0.18") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "predicates") (req "^1.0.0") (default-features #t) (kind 2)))) (hash "1xdk42zxpgri9z1imz7bb3ixq32fsiqkb7vvygbv9jzw7lny0wcn")))

