(define-module (crates-io do co) #:use-module (crates-io))

(define-public crate-docomo-api-0.1 (crate (name "docomo-api") (vers "0.1.0") (deps (list (crate-dep (name "hyper") (req "^0.9.10") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^0.8.2") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^0.8.1") (default-features #t) (kind 0)) (crate-dep (name "serde_macros") (req "^0.8.2") (default-features #t) (kind 0)))) (hash "0vnrbwzwjqhcx1kkgas1gqzsgfjjlx0mabh1dwpsy653ixhqbd37")))

(define-public crate-docopt-0.6 (crate (name "docopt") (vers "0.6.8") (hash "11zjyn53qh5qwpznsmpr2a0j64plya6fnki7w5is4gpzhlsfaajq")))

(define-public crate-docopt-0.6 (crate (name "docopt") (vers "0.6.9") (hash "0kyba7r6zhp8bspbgzsv63xc3y0kk9jjsl9gdizg8v1fhzmc4s0v")))

(define-public crate-docopt-0.6 (crate (name "docopt") (vers "0.6.10") (hash "1lnzlijggk02xinqkvgn0mwa681i42ifjishrbmn366v392hf5ql")))

(define-public crate-docopt-0.6 (crate (name "docopt") (vers "0.6.11") (hash "0qx1p9qyhfqrv48h0z5lcnmcqwkjxp7x9x38sd3m08sfjayk1q3z")))

(define-public crate-docopt-0.6 (crate (name "docopt") (vers "0.6.12") (hash "02m5q4g031rya7hm84wvnri7p6721h1cqacg5sy2ksk4jxkmxj4p")))

(define-public crate-docopt-0.6 (crate (name "docopt") (vers "0.6.13") (hash "1hms7kc35x0980g6xx5pkdxf600n3h02qx4cvddlpmhynlgcib1r")))

(define-public crate-docopt-0.6 (crate (name "docopt") (vers "0.6.14") (hash "13l041yassyswr8lghg5sk6q8bzr52wimk7j1qn04635vh390akd")))

(define-public crate-docopt-0.6 (crate (name "docopt") (vers "0.6.15") (hash "0dsapxzvby7k7ypp3a3hjbnbis8bkz2597my1hl96srkmwymiz9k")))

(define-public crate-docopt-0.6 (crate (name "docopt") (vers "0.6.16") (hash "0ajsa7daqnxd39g8pf5ygdnv5ympwwi5y52kzzfskajv61rcsy3h")))

(define-public crate-docopt-0.6 (crate (name "docopt") (vers "0.6.17") (hash "0lsw2xa3qxm8qsg474k1vxwr08x8gjja3gds9ad03kqy2ld90g06")))

(define-public crate-docopt-0.6 (crate (name "docopt") (vers "0.6.18") (deps (list (crate-dep (name "regex") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "1n6ry43lprc42v81w4ibsj9b12rqm0l5l3a0fprk9bihzbr185qs")))

(define-public crate-docopt-0.6 (crate (name "docopt") (vers "0.6.19") (deps (list (crate-dep (name "regex") (req "*") (default-features #t) (kind 0)) (crate-dep (name "rustc-serialize") (req "*") (default-features #t) (kind 0)))) (hash "0jr10vw4qh6cm85b2xbk58q6macy33cypiq3ln6bdpxmqqbhbm71")))

(define-public crate-docopt-0.6 (crate (name "docopt") (vers "0.6.20") (deps (list (crate-dep (name "regex") (req "*") (default-features #t) (kind 0)) (crate-dep (name "rustc-serialize") (req "*") (default-features #t) (kind 0)))) (hash "1ch715s9h38n7x9x3lsc54824h668fxbcfngrsbgraaghqq3wk9k")))

(define-public crate-docopt-0.6 (crate (name "docopt") (vers "0.6.21") (deps (list (crate-dep (name "regex") (req "*") (default-features #t) (kind 0)) (crate-dep (name "rustc-serialize") (req "*") (default-features #t) (kind 0)))) (hash "1837gvnxp4rl07wbj6h3mxhwyrf5hyp9ij3344ir7knyhvh6v661")))

(define-public crate-docopt-0.6 (crate (name "docopt") (vers "0.6.22") (deps (list (crate-dep (name "regex") (req "*") (default-features #t) (kind 0)) (crate-dep (name "rustc-serialize") (req "*") (default-features #t) (kind 0)))) (hash "1a16pd13kxfq8mlxz7jxi9j539s3sb8kirnqgd7kx5njpzy3q03x")))

(define-public crate-docopt-0.6 (crate (name "docopt") (vers "0.6.23") (deps (list (crate-dep (name "regex") (req "*") (default-features #t) (kind 0)) (crate-dep (name "rustc-serialize") (req "*") (default-features #t) (kind 0)))) (hash "1llkqj09qcf3hgf5j539z4i28vibr5qgg8yfmc2mpnas3xaz3hwp")))

(define-public crate-docopt-0.6 (crate (name "docopt") (vers "0.6.24") (deps (list (crate-dep (name "regex") (req "*") (default-features #t) (kind 0)) (crate-dep (name "rustc-serialize") (req "*") (default-features #t) (kind 0)))) (hash "0640wry3sk74v5zvl511jhmysjdm49dw68g7n5dg6lkvlwvnqaqf")))

(define-public crate-docopt-0.6 (crate (name "docopt") (vers "0.6.25") (deps (list (crate-dep (name "regex") (req "*") (default-features #t) (kind 0)) (crate-dep (name "rustc-serialize") (req "*") (default-features #t) (kind 0)))) (hash "16gwbrmvxdg733mr9gjg5v3a211v7ayp26mpynjp55saskxx34jj")))

(define-public crate-docopt-0.6 (crate (name "docopt") (vers "0.6.26") (deps (list (crate-dep (name "regex") (req "*") (default-features #t) (kind 0)) (crate-dep (name "rustc-serialize") (req "*") (default-features #t) (kind 0)))) (hash "1nibfvshj1fg88l9qshh39fngq1arv8k73vfm7v33p75mhl9rqnh")))

(define-public crate-docopt-0.6 (crate (name "docopt") (vers "0.6.27") (deps (list (crate-dep (name "regex") (req "*") (default-features #t) (kind 0)) (crate-dep (name "rustc-serialize") (req "*") (default-features #t) (kind 0)))) (hash "1lx3gh1lf43cwy37lkc3ncmshk5v1nf97f9lpyx8w6hlrx20x9r1")))

(define-public crate-docopt-0.6 (crate (name "docopt") (vers "0.6.28") (deps (list (crate-dep (name "regex") (req "*") (default-features #t) (kind 0)) (crate-dep (name "rustc-serialize") (req "*") (default-features #t) (kind 0)))) (hash "0cx50c4fq6hryq9ky2d7ccwklis3n04y4knj4r7mdchfm5ld4qqy")))

(define-public crate-docopt-0.6 (crate (name "docopt") (vers "0.6.29") (deps (list (crate-dep (name "regex") (req "*") (default-features #t) (kind 0)) (crate-dep (name "rustc-serialize") (req "*") (default-features #t) (kind 0)))) (hash "1qkzdbg56bxpwsrdqswkmhjw7svvbcqp9skscpchvj1g16sx7wjb")))

(define-public crate-docopt-0.6 (crate (name "docopt") (vers "0.6.30") (deps (list (crate-dep (name "regex") (req "*") (default-features #t) (kind 0)) (crate-dep (name "rustc-serialize") (req "*") (default-features #t) (kind 0)))) (hash "1c9lx617d641spb6n6crfwwhpvnwybp1zgqsjw5kx4n5p29ybkhy")))

(define-public crate-docopt-0.6 (crate (name "docopt") (vers "0.6.31") (deps (list (crate-dep (name "regex") (req "*") (default-features #t) (kind 0)) (crate-dep (name "rustc-serialize") (req "*") (default-features #t) (kind 0)))) (hash "1s9fmwvrpgxsci4mjv02l55p4xyjw19yqkrfz968ma2b1dabh7qg")))

(define-public crate-docopt-0.6 (crate (name "docopt") (vers "0.6.32") (deps (list (crate-dep (name "regex") (req "*") (default-features #t) (kind 0)) (crate-dep (name "rustc-serialize") (req "*") (default-features #t) (kind 0)))) (hash "05scy7i1ca068ab3kraz6488kci4ggzq7j4b192inpj4hywyr97b")))

(define-public crate-docopt-0.6 (crate (name "docopt") (vers "0.6.33") (deps (list (crate-dep (name "regex") (req "*") (default-features #t) (kind 0)) (crate-dep (name "rustc-serialize") (req "*") (default-features #t) (kind 0)))) (hash "1mc26vzvrx6x0jn7jq5940zplj44b86smyh6y2n4864rg30r3b2z")))

(define-public crate-docopt-0.6 (crate (name "docopt") (vers "0.6.34") (deps (list (crate-dep (name "regex") (req "*") (default-features #t) (kind 0)) (crate-dep (name "rustc-serialize") (req "*") (default-features #t) (kind 0)))) (hash "0230qpfg515da26nn512mdc3mk8sdfwr5i6cy00c4ld63a03r6xn")))

(define-public crate-docopt-0.6 (crate (name "docopt") (vers "0.6.35") (deps (list (crate-dep (name "regex") (req "*") (default-features #t) (kind 0)) (crate-dep (name "rustc-serialize") (req "*") (default-features #t) (kind 0)))) (hash "0a09ggp52lbz4gwfw97z7bljbaq5x2wy4rzd746zgh2c7ad33l3i")))

(define-public crate-docopt-0.6 (crate (name "docopt") (vers "0.6.36") (deps (list (crate-dep (name "regex") (req "*") (default-features #t) (kind 0)) (crate-dep (name "rustc-serialize") (req "*") (default-features #t) (kind 0)))) (hash "1lw88w1281ig8r86g9pa4aprcafgl0qmvgnq2j41vg2r7i8h3vsr")))

(define-public crate-docopt-0.6 (crate (name "docopt") (vers "0.6.37") (deps (list (crate-dep (name "regex") (req "*") (default-features #t) (kind 0)) (crate-dep (name "rustc-serialize") (req "*") (default-features #t) (kind 0)))) (hash "0xbxja7mbczx8lsqa1fl6j03nwymq82p4r3hp5ylzhxf7nhqmic1")))

(define-public crate-docopt-0.6 (crate (name "docopt") (vers "0.6.38") (deps (list (crate-dep (name "regex") (req "*") (default-features #t) (kind 0)) (crate-dep (name "rustc-serialize") (req "*") (default-features #t) (kind 0)))) (hash "0ybqg2qdhaxh2wsq102p8c6dzb9n9dv4x5vawikmh2j6ym61jjrv")))

(define-public crate-docopt-0.6 (crate (name "docopt") (vers "0.6.39") (deps (list (crate-dep (name "regex") (req "*") (default-features #t) (kind 0)) (crate-dep (name "rustc-serialize") (req "*") (default-features #t) (kind 0)))) (hash "0ymplrwnz2rvf6yxg21i9xq8jgnnh2pdsl9znxmn38m329qzhmh0")))

(define-public crate-docopt-0.6 (crate (name "docopt") (vers "0.6.40") (deps (list (crate-dep (name "libc") (req "*") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "*") (default-features #t) (kind 0)) (crate-dep (name "rustc-serialize") (req "*") (default-features #t) (kind 0)))) (hash "1lg3gji7d6vi1cyn7aprq4vk3nxqsy7pnghhkirhwamasw8yxi1q")))

(define-public crate-docopt-0.6 (crate (name "docopt") (vers "0.6.41") (deps (list (crate-dep (name "libc") (req "*") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "*") (default-features #t) (kind 0)) (crate-dep (name "rustc-serialize") (req "^0.2.0") (default-features #t) (kind 0)))) (hash "1paskvdx4hg5wz3sxgnzzf4h4vr2895p2j09q8rx4i60idslzvp2")))

(define-public crate-docopt-0.6 (crate (name "docopt") (vers "0.6.42") (deps (list (crate-dep (name "libc") (req "*") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "*") (default-features #t) (kind 0)) (crate-dep (name "rustc-serialize") (req "^0.2.0") (default-features #t) (kind 0)))) (hash "1irg693wjzyip436p24z4v43vp24d5k8xhnhkqd651j8r7rjcl3f")))

(define-public crate-docopt-0.6 (crate (name "docopt") (vers "0.6.43") (deps (list (crate-dep (name "libc") (req "*") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "*") (default-features #t) (kind 0)) (crate-dep (name "rustc-serialize") (req "^0.3") (default-features #t) (kind 0)))) (hash "0pkbhh9d0sw3pp1c1pjhj4f3mkpf4vrii5d6kjy7lvcrw5n29bmb")))

(define-public crate-docopt-0.6 (crate (name "docopt") (vers "0.6.44") (deps (list (crate-dep (name "libc") (req "*") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "*") (default-features #t) (kind 0)) (crate-dep (name "rustc-serialize") (req "^0.3") (default-features #t) (kind 0)))) (hash "1i9wi7ig8a7qb1k01zk6g2plp20gzsi454jjg9x5ddnrmxaq7r0l")))

(define-public crate-docopt-0.6 (crate (name "docopt") (vers "0.6.45") (deps (list (crate-dep (name "libc") (req "*") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "*") (default-features #t) (kind 0)) (crate-dep (name "rustc-serialize") (req "^0.3") (default-features #t) (kind 0)))) (hash "1ckiyvf0nxags9kdkk5cz5r3vs177qnz1w0v47z126j3c7vqv7hs")))

(define-public crate-docopt-0.6 (crate (name "docopt") (vers "0.6.47") (deps (list (crate-dep (name "libc") (req "*") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "*") (default-features #t) (kind 0)) (crate-dep (name "rustc-serialize") (req "^0.3") (default-features #t) (kind 0)))) (hash "1c0m6iq00i6drh2fxsr5fgg9z1yy1igvvpv83j8q1rz40irk1rx4")))

(define-public crate-docopt-0.6 (crate (name "docopt") (vers "0.6.49") (deps (list (crate-dep (name "libc") (req "*") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "*") (default-features #t) (kind 0)) (crate-dep (name "rustc-serialize") (req "^0.3") (default-features #t) (kind 0)))) (hash "1n62xwn6g1zd3gg7lbb1k18gw1r9klbn07cpzn9dk8y5fm9fyn13")))

(define-public crate-docopt-0.6 (crate (name "docopt") (vers "0.6.50") (deps (list (crate-dep (name "libc") (req "*") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "*") (default-features #t) (kind 0)) (crate-dep (name "rustc-serialize") (req "^0.3") (default-features #t) (kind 0)))) (hash "16cjr71fddix0rrmw0j0i406i6bpi06y414mjvdr1ggsrh49h4bz")))

(define-public crate-docopt-0.6 (crate (name "docopt") (vers "0.6.51") (deps (list (crate-dep (name "libc") (req "*") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "*") (default-features #t) (kind 0)) (crate-dep (name "rustc-serialize") (req "^0.3") (default-features #t) (kind 0)))) (hash "0qza44b8kbl4ki1zyqm66gly0slsags444af2y8qdxn9pjq1mbzw")))

(define-public crate-docopt-0.6 (crate (name "docopt") (vers "0.6.52") (deps (list (crate-dep (name "libc") (req "*") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "*") (default-features #t) (kind 0)) (crate-dep (name "rustc-serialize") (req "^0.3") (default-features #t) (kind 0)))) (hash "14w57mw8bjpgn8ngpnayd548pnzmp39w67mbr20cgk2ryhddf9a3")))

(define-public crate-docopt-0.6 (crate (name "docopt") (vers "0.6.53") (deps (list (crate-dep (name "libc") (req "*") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "*") (default-features #t) (kind 0)) (crate-dep (name "rustc-serialize") (req "^0.3") (default-features #t) (kind 0)))) (hash "0vczvj4kd5bibpxwknihs25chp0kc2g7q1m6402yrbxxawgayzkr")))

(define-public crate-docopt-0.6 (crate (name "docopt") (vers "0.6.54") (deps (list (crate-dep (name "libc") (req "*") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "*") (default-features #t) (kind 0)) (crate-dep (name "rustc-serialize") (req "^0.3") (default-features #t) (kind 0)))) (hash "1xmy71sh181mqphbdhl99s0mjbzz132xz90rs9asml794scsx975")))

(define-public crate-docopt-0.6 (crate (name "docopt") (vers "0.6.55") (deps (list (crate-dep (name "libc") (req "*") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "*") (default-features #t) (kind 0)) (crate-dep (name "rustc-serialize") (req "^0.3") (default-features #t) (kind 0)))) (hash "0infpz3v7il312g1m4va1lxbpih8qypslllsra3q6mrkdndvj9vp")))

(define-public crate-docopt-0.6 (crate (name "docopt") (vers "0.6.56") (deps (list (crate-dep (name "libc") (req "*") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "*") (default-features #t) (kind 0)) (crate-dep (name "rustc-serialize") (req "^0.3") (default-features #t) (kind 0)))) (hash "0nl7bwpdzbncnda5yhqzmjawwv9hng808vnjq44rv20mrjgvr4vw")))

(define-public crate-docopt-0.6 (crate (name "docopt") (vers "0.6.57") (deps (list (crate-dep (name "libc") (req "*") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "*") (default-features #t) (kind 0)) (crate-dep (name "rustc-serialize") (req "^0.3") (default-features #t) (kind 0)))) (hash "04gigb422gm1vjhhxx7rh5bg0amldminscmypqiz0m1f6pk33ygg")))

(define-public crate-docopt-0.6 (crate (name "docopt") (vers "0.6.58") (deps (list (crate-dep (name "libc") (req "*") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "*") (default-features #t) (kind 0)) (crate-dep (name "rustc-serialize") (req "^0.3") (default-features #t) (kind 0)))) (hash "0dly2wj6572jr86y2m8wkswzddsffiqsw987vsraikrrbk9vi8wa")))

(define-public crate-docopt-0.6 (crate (name "docopt") (vers "0.6.59") (deps (list (crate-dep (name "regex") (req "*") (default-features #t) (kind 0)) (crate-dep (name "rustc-serialize") (req "^0.3") (default-features #t) (kind 0)))) (hash "0ixyf8vqb6nfw4gwyrqym1143rigp2d4y0yjs4lrw1q3wnclwxd4")))

(define-public crate-docopt-0.6 (crate (name "docopt") (vers "0.6.60") (deps (list (crate-dep (name "regex") (req "*") (default-features #t) (kind 0)) (crate-dep (name "rustc-serialize") (req "^0.3") (default-features #t) (kind 0)))) (hash "0r6xav7f4c62vfflwcbnfnwswvbdh0spc2mkpim75ndp1jb72nqq")))

(define-public crate-docopt-0.6 (crate (name "docopt") (vers "0.6.61") (deps (list (crate-dep (name "regex") (req "*") (default-features #t) (kind 0)) (crate-dep (name "rustc-serialize") (req "^0.3") (default-features #t) (kind 0)))) (hash "03gjqc5477rw6aj0zi23zpfh3870jb6afrg8dmwq4s6a5krsq699")))

(define-public crate-docopt-0.6 (crate (name "docopt") (vers "0.6.62") (deps (list (crate-dep (name "regex") (req "*") (default-features #t) (kind 0)) (crate-dep (name "rustc-serialize") (req "^0.3") (default-features #t) (kind 0)))) (hash "0p20p7zxcwl5j8fa3mlrljfl4pr7ngvc6c2qa1290qks49qrx26g")))

(define-public crate-docopt-0.6 (crate (name "docopt") (vers "0.6.63") (deps (list (crate-dep (name "regex") (req "*") (default-features #t) (kind 0)) (crate-dep (name "rustc-serialize") (req "^0.3") (default-features #t) (kind 0)))) (hash "0ydmlyj9av3xkkabkvnrm80jciyjdljm0dzz4fs2fmsk5zgq5d6h")))

(define-public crate-docopt-0.6 (crate (name "docopt") (vers "0.6.64") (deps (list (crate-dep (name "regex") (req "*") (default-features #t) (kind 0)) (crate-dep (name "rustc-serialize") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "strsim") (req "^0.3") (default-features #t) (kind 0)))) (hash "1mfi6pmg7fq7iv3l3jq3daybsgyv71m0szgsybg68ig9z4kjlg42")))

(define-public crate-docopt-0.6 (crate (name "docopt") (vers "0.6.65") (deps (list (crate-dep (name "regex") (req "*") (default-features #t) (kind 0)) (crate-dep (name "rustc-serialize") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "strsim") (req "^0.3") (default-features #t) (kind 0)))) (hash "1392f9znf2nmp3cqw8wxhdql6kjcy6ik2c4y9hg00bnszkj6cx87")))

(define-public crate-docopt-0.6 (crate (name "docopt") (vers "0.6.66") (deps (list (crate-dep (name "regex") (req "*") (default-features #t) (kind 0)) (crate-dep (name "rustc-serialize") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "strsim") (req "^0.3") (default-features #t) (kind 0)))) (hash "0rbsxz0qx2k4b2sj5knn1dqj3bmdv3bwl6w6ys5h77sw0rpa654l")))

(define-public crate-docopt-0.6 (crate (name "docopt") (vers "0.6.67") (deps (list (crate-dep (name "regex") (req "*") (default-features #t) (kind 0)) (crate-dep (name "rustc-serialize") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "strsim") (req "^0.3") (default-features #t) (kind 0)))) (hash "0lxj9l91ir9xy4b1gvy2c01pia4zida0wjskpl6iw4a5pa6gh0as")))

(define-public crate-docopt-0.6 (crate (name "docopt") (vers "0.6.68") (deps (list (crate-dep (name "regex") (req "*") (default-features #t) (kind 0)) (crate-dep (name "rustc-serialize") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "strsim") (req "^0.3") (default-features #t) (kind 0)))) (hash "0abwndljid4dlm4nmiyvhcn810pmzcyqdihlvc9nrc9a2w6y30qp")))

(define-public crate-docopt-0.6 (crate (name "docopt") (vers "0.6.69") (deps (list (crate-dep (name "regex") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "rustc-serialize") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "strsim") (req "^0.3") (default-features #t) (kind 0)))) (hash "0agpxw12il3py3llmbhrzb40391pq4xavml5lnqn5aj3s4wmmwh2")))

(define-public crate-docopt-0.6 (crate (name "docopt") (vers "0.6.70") (deps (list (crate-dep (name "regex") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "rustc-serialize") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "strsim") (req "^0.3") (default-features #t) (kind 0)))) (hash "1kyawdlypz89pv7gs19lsyabd5a7300sp51r570x2ffc2ib51l2b")))

(define-public crate-docopt-0.6 (crate (name "docopt") (vers "0.6.71") (deps (list (crate-dep (name "regex") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "rustc-serialize") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "strsim") (req "^0.3") (default-features #t) (kind 0)))) (hash "12m8vh58ca1sna3w1ssqq1xvzhj3lc6jizwn64hvldc39nm3592n")))

(define-public crate-docopt-0.6 (crate (name "docopt") (vers "0.6.72") (deps (list (crate-dep (name "regex") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "rustc-serialize") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "strsim") (req "^0.3") (default-features #t) (kind 0)))) (hash "0ncd7bx0d6wmsdvmjbyzk0g16agyppl4rg7m3cwdjhg4m9gqnja9")))

(define-public crate-docopt-0.6 (crate (name "docopt") (vers "0.6.73") (deps (list (crate-dep (name "regex") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "rustc-serialize") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "strsim") (req "^0.3") (default-features #t) (kind 0)))) (hash "1hmldn0h80z6j23rp1qkc6gzbky7r3l89ddr55xbz98igyzvv318")))

(define-public crate-docopt-0.6 (crate (name "docopt") (vers "0.6.74") (deps (list (crate-dep (name "regex") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "rustc-serialize") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "strsim") (req "^0.3") (default-features #t) (kind 0)))) (hash "02x8i7g3kzzwgfa3xixd2cf4v2rnb318d237sgwaxii1gb4d29ag")))

(define-public crate-docopt-0.6 (crate (name "docopt") (vers "0.6.75") (deps (list (crate-dep (name "regex") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "rustc-serialize") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "strsim") (req "^0.3") (default-features #t) (kind 0)))) (hash "0z5cjr7n1bhmb8lh0h57ac8lchzgcrvid1cmnp0md4p4cn7d3mv2")))

(define-public crate-docopt-0.6 (crate (name "docopt") (vers "0.6.76") (deps (list (crate-dep (name "regex") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "rustc-serialize") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "strsim") (req "^0.3") (default-features #t) (kind 0)))) (hash "11c5qi9qz8l8fdhlkglb2bh9y3j1p1dbhy1cdr16bbgrgl53cigr")))

(define-public crate-docopt-0.6 (crate (name "docopt") (vers "0.6.77") (deps (list (crate-dep (name "regex") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "rustc-serialize") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "strsim") (req "^0.3") (default-features #t) (kind 0)))) (hash "01d72pbk8as6xdvc7ia6d6sl14zh94if1jbv0lvrnhh2ml3rdmg4")))

(define-public crate-docopt-0.6 (crate (name "docopt") (vers "0.6.78") (deps (list (crate-dep (name "regex") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "rustc-serialize") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "strsim") (req "^0.3") (default-features #t) (kind 0)))) (hash "0mb16vdfnragxahcj42kjhlv0m7qhf48723qw6imaglkpfjjqyam")))

(define-public crate-docopt-0.6 (crate (name "docopt") (vers "0.6.80") (deps (list (crate-dep (name "regex") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "rustc-serialize") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "strsim") (req "^0.3") (default-features #t) (kind 0)))) (hash "0xkswpzm9r3kwjjc6a3lqa0mi27y6ajbliqxl6jwca08rssarh2c")))

(define-public crate-docopt-0.6 (crate (name "docopt") (vers "0.6.81") (deps (list (crate-dep (name "regex") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "rustc-serialize") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "strsim") (req "^0.3") (default-features #t) (kind 0)))) (hash "0a9qnz2lj3ifn36npm9i301vrcjrav0n5sr6r0k3z3bdci85qfrj")))

(define-public crate-docopt-0.6 (crate (name "docopt") (vers "0.6.82") (deps (list (crate-dep (name "lazy_static") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "rustc-serialize") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "strsim") (req "^0.3") (default-features #t) (kind 0)))) (hash "0zfkn9fl5xk81qhhi6y9kdgnj2yyk48a1m3arzf4brdljdh0284g")))

(define-public crate-docopt-0.6 (crate (name "docopt") (vers "0.6.83") (deps (list (crate-dep (name "lazy_static") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "rustc-serialize") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "strsim") (req "^0.5") (default-features #t) (kind 0)))) (hash "0fwc1k0qgzh0yk28v0qhpq61jfmp6ljprm1p1i0n38r3g03wchpw")))

(define-public crate-docopt-0.6 (crate (name "docopt") (vers "0.6.84") (deps (list (crate-dep (name "lazy_static") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "rustc-serialize") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "strsim") (req "^0.5") (default-features #t) (kind 0)))) (hash "0p0yygfgz66w7nynggpm4cmd38xlkvyplmi2ii6z86cpin2lxgaw")))

(define-public crate-docopt-0.6 (crate (name "docopt") (vers "0.6.85") (deps (list (crate-dep (name "lazy_static") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "rustc-serialize") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "strsim") (req "^0.5") (default-features #t) (kind 0)))) (hash "1y8bgxpdbf1ij3j3vwwh3lacgf5rhmwy2f3j1rbwa8a0cy1xg20v")))

(define-public crate-docopt-0.6 (crate (name "docopt") (vers "0.6.86") (deps (list (crate-dep (name "lazy_static") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "rustc-serialize") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "strsim") (req "^0.5") (default-features #t) (kind 0)))) (hash "1nf4f4zf5yk0d0l4kl7hkii4na22fhn0l2hgfb46yzv08l2g6zja")))

(define-public crate-docopt-0.7 (crate (name "docopt") (vers "0.7.0") (deps (list (crate-dep (name "lazy_static") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "rustc-serialize") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "strsim") (req "^0.6") (default-features #t) (kind 0)))) (hash "1n6gbhsks2w9y0b4bwqyawh4ghbkka09w6pjcrq9i1sd51pflcmb")))

(define-public crate-docopt-0.8 (crate (name "docopt") (vers "0.8.0") (deps (list (crate-dep (name "lazy_static") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "strsim") (req "^0.6") (default-features #t) (kind 0)))) (hash "0h5lqm47vp5khvch9p9jr3sixxgbwf93jdb23z3cawm7x3p0ir33")))

(define-public crate-docopt-0.8 (crate (name "docopt") (vers "0.8.1") (deps (list (crate-dep (name "lazy_static") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "strsim") (req "^0.6") (default-features #t) (kind 0)))) (hash "0ykq95xrg8l0c0n37q13rrm6rji8x0yw8569zi25aglbixqr6nrv")))

(define-public crate-docopt-0.8 (crate (name "docopt") (vers "0.8.2") (deps (list (crate-dep (name "lazy_static") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "strsim") (req "^0.6") (default-features #t) (kind 0)))) (hash "128qsa5q692y95wixizmplhvh0c201a8nixjp2xq5sbk10lgbxml") (yanked #t)))

(define-public crate-docopt-0.8 (crate (name "docopt") (vers "0.8.3") (deps (list (crate-dep (name "lazy_static") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "strsim") (req "^0.6") (default-features #t) (kind 0)))) (hash "0jha611mffc2qnxvdl3pmglz07akl99lk1vihhb3nl1cd69x7b6q")))

(define-public crate-docopt-0.9 (crate (name "docopt") (vers "0.9.0") (deps (list (crate-dep (name "lazy_static") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "strsim") (req "^0.7") (default-features #t) (kind 0)))) (hash "1zzqvk7wfzgqdgvqng6wnzgv9rrhpprpiwg9khy5qqchlm4fi8m7")))

(define-public crate-docopt-1 (crate (name "docopt") (vers "1.0.0") (deps (list (crate-dep (name "lazy_static") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "strsim") (req "^0.7") (default-features #t) (kind 0)))) (hash "0aqybssp3krn7b0f6sh90l5zr437nkrghp2psgxzzikgqd8bfzz6")))

(define-public crate-docopt-1 (crate (name "docopt") (vers "1.0.1") (deps (list (crate-dep (name "lazy_static") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.0.3") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "strsim") (req "^0.7") (default-features #t) (kind 0)))) (hash "1h1s3lg61yivsyqrsll0fwj0zfjmkzbryh2bq7napanzf3gr436n")))

(define-public crate-docopt-1 (crate (name "docopt") (vers "1.0.2") (deps (list (crate-dep (name "lazy_static") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.0.3") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "strsim") (req "^0.7") (default-features #t) (kind 0)))) (hash "09djlzwkdmmic3jfinr29v25v0x8m6b8fchyzh3p4nwvaz10cafv")))

(define-public crate-docopt-1 (crate (name "docopt") (vers "1.1.0") (deps (list (crate-dep (name "lazy_static") (req "^1.3") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.1.5") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "strsim") (req "^0.9") (default-features #t) (kind 0)))) (hash "0s9rcpmnnivs502q69lc1h1wrwapkq09ikgbfbgqf31idmc5llkz")))

(define-public crate-docopt-1 (crate (name "docopt") (vers "1.1.1") (deps (list (crate-dep (name "lazy_static") (req "^1.3") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.4.0") (features (quote ("std" "unicode"))) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "strsim") (req "^0.10") (default-features #t) (kind 0)))) (hash "07x5g52n6fzilxhk5220caznkvdnzzvahlzrzkmgj8y88sc12gvz")))

(define-public crate-docopt_macros-0.6 (crate (name "docopt_macros") (vers "0.6.8") (deps (list (crate-dep (name "docopt") (req "~0.6.8") (default-features #t) (kind 0)))) (hash "02v68hdi05cshlg58spjr7b19kvw026dryjjzy1jk0facmh8bijx")))

(define-public crate-docopt_macros-0.6 (crate (name "docopt_macros") (vers "0.6.10") (deps (list (crate-dep (name "docopt") (req "^0.6.10") (default-features #t) (kind 0)))) (hash "0d3cr1w94b467c1gdgasrw6iriqmpcq18972wlmdr61dsx4p7nbf")))

(define-public crate-docopt_macros-0.6 (crate (name "docopt_macros") (vers "0.6.11") (deps (list (crate-dep (name "docopt") (req "^0.6.11") (default-features #t) (kind 0)))) (hash "1s58yzfh83irfrjl4ynddiw54c648yqgsvvwsjwbl23a644c242m")))

(define-public crate-docopt_macros-0.6 (crate (name "docopt_macros") (vers "0.6.12") (deps (list (crate-dep (name "docopt") (req "^0.6.12") (default-features #t) (kind 0)))) (hash "00j8c59j92n6wmh0k6v9f7yiqnvj629n8k2rjkv3rjggknn3rnxd")))

(define-public crate-docopt_macros-0.6 (crate (name "docopt_macros") (vers "0.6.13") (deps (list (crate-dep (name "docopt") (req "^0.6.13") (default-features #t) (kind 0)))) (hash "02xj7mfsj04y5nr31zp5aqhvs53f8p01d02cr02f4kig3ic9s1n8")))

(define-public crate-docopt_macros-0.6 (crate (name "docopt_macros") (vers "0.6.14") (deps (list (crate-dep (name "docopt") (req "^0.6.14") (default-features #t) (kind 0)))) (hash "1xygvs4lwm1j20nrnyqw53f0c75amvi26zfw4yxnpj09kasdcbvn")))

(define-public crate-docopt_macros-0.6 (crate (name "docopt_macros") (vers "0.6.15") (deps (list (crate-dep (name "docopt") (req "^0.6.15") (default-features #t) (kind 0)))) (hash "00sg289lqdsbqrh8lbdjxpji61y8v09p4n97nz5xl3wll7qnbzas")))

(define-public crate-docopt_macros-0.6 (crate (name "docopt_macros") (vers "0.6.16") (deps (list (crate-dep (name "docopt") (req "^0.6.16") (default-features #t) (kind 0)))) (hash "05ycnw5c5rnil0nwd9l1hq39b0xckvkjdkwkq0s1iaqw13ah76x1")))

(define-public crate-docopt_macros-0.6 (crate (name "docopt_macros") (vers "0.6.17") (deps (list (crate-dep (name "docopt") (req "^0.6.17") (default-features #t) (kind 0)))) (hash "1il9rd0avijlcwgszl18s7abdhf0c9rnzj7l8n154yqzc4f9zars")))

(define-public crate-docopt_macros-0.6 (crate (name "docopt_macros") (vers "0.6.18") (deps (list (crate-dep (name "docopt") (req "^0.6.18") (default-features #t) (kind 0)))) (hash "0s6rmkhr9aaihiyy8fql9yg83x2dv29kqi21b4336mpw5gsvi1dv")))

(define-public crate-docopt_macros-0.6 (crate (name "docopt_macros") (vers "0.6.19") (deps (list (crate-dep (name "docopt") (req "^0.6.19") (default-features #t) (kind 0)))) (hash "10h1bqvdv5147pw60q87gah8ny7q3vrxnglljy4h8whgc7gn51cb")))

(define-public crate-docopt_macros-0.6 (crate (name "docopt_macros") (vers "0.6.20") (deps (list (crate-dep (name "docopt") (req "^0.6.19") (default-features #t) (kind 0)))) (hash "14v7pr83qf8wg0ymy7indba0pk8sglq2ikfvbzb45xpsikjn9kj9")))

(define-public crate-docopt_macros-0.6 (crate (name "docopt_macros") (vers "0.6.21") (deps (list (crate-dep (name "docopt") (req "^0.6.19") (default-features #t) (kind 0)))) (hash "08l8hjyidbyrq9wdi0qjpbcaldsc4jhfkj40dqlr3ad1dlzsa8gd")))

(define-public crate-docopt_macros-0.6 (crate (name "docopt_macros") (vers "0.6.22") (deps (list (crate-dep (name "docopt") (req "^0.6.19") (default-features #t) (kind 0)))) (hash "1pc2gma5mj2akp0jlrika611rmnzsclwb9plxp9m1k048mf13vpy")))

(define-public crate-docopt_macros-0.6 (crate (name "docopt_macros") (vers "0.6.23") (deps (list (crate-dep (name "docopt") (req "^0.6.19") (default-features #t) (kind 0)))) (hash "1rg4fs561n1p4xszarci0cmn57nqlqc9f07y3db267b9nsqk0ywg")))

(define-public crate-docopt_macros-0.6 (crate (name "docopt_macros") (vers "0.6.24") (deps (list (crate-dep (name "docopt") (req "^0.6.19") (default-features #t) (kind 0)))) (hash "1mamlb39c4857zd6y2lnf53gz2hhlhl6rdn2av7y6yq6nds668g7")))

(define-public crate-docopt_macros-0.6 (crate (name "docopt_macros") (vers "0.6.25") (deps (list (crate-dep (name "docopt") (req "^0.6.19") (default-features #t) (kind 0)))) (hash "1br0lj42rzp88jfn4xn3vzhh9y3q87y3z9ix6indbr3l4sp6vbfb")))

(define-public crate-docopt_macros-0.6 (crate (name "docopt_macros") (vers "0.6.26") (deps (list (crate-dep (name "docopt") (req "^0.6.19") (default-features #t) (kind 0)))) (hash "1ixg6wpd5d6hrh9vyhlp0d1gnbxpybx1nk198dhwrxm4mjbw6bls")))

(define-public crate-docopt_macros-0.6 (crate (name "docopt_macros") (vers "0.6.27") (deps (list (crate-dep (name "docopt") (req "^0.6.19") (default-features #t) (kind 0)))) (hash "01xgdzwsaibfgvl1311hz8fiwsr3f8g3hhz1ix9zzjlfm6frsdsl")))

(define-public crate-docopt_macros-0.6 (crate (name "docopt_macros") (vers "0.6.28") (deps (list (crate-dep (name "docopt") (req "*") (default-features #t) (kind 0)))) (hash "0gz7ps19x5my0vqxhd4qpiybl8ppaj8vasmpk65wdnf996m909xf")))

(define-public crate-docopt_macros-0.6 (crate (name "docopt_macros") (vers "0.6.29") (deps (list (crate-dep (name "docopt") (req "*") (default-features #t) (kind 0)) (crate-dep (name "rustc-serialize") (req "*") (default-features #t) (kind 2)))) (hash "1rrlgzgxsn1b6c4fqlzqzynkyswrpz900r1f67ph66ksy5w17nf2")))

(define-public crate-docopt_macros-0.6 (crate (name "docopt_macros") (vers "0.6.30") (deps (list (crate-dep (name "docopt") (req "*") (default-features #t) (kind 0)) (crate-dep (name "rustc-serialize") (req "*") (default-features #t) (kind 2)))) (hash "0f8ajxghmdrgll8nq30q4akbn8hj5fzhlpa89g66jkg5za9c2aqa")))

(define-public crate-docopt_macros-0.6 (crate (name "docopt_macros") (vers "0.6.31") (deps (list (crate-dep (name "docopt") (req "*") (default-features #t) (kind 0)) (crate-dep (name "rustc-serialize") (req "*") (default-features #t) (kind 2)))) (hash "0rpj2bjhmgfjl0ysk5wr6hdf1ldrzh1pknsadzsywa0z347z825g")))

(define-public crate-docopt_macros-0.6 (crate (name "docopt_macros") (vers "0.6.32") (deps (list (crate-dep (name "docopt") (req "*") (default-features #t) (kind 0)) (crate-dep (name "rustc-serialize") (req "*") (default-features #t) (kind 2)))) (hash "1vz7y00d24km05qr6p0pa9w2hjh6nka91z90aclclzj2v9910wpw")))

(define-public crate-docopt_macros-0.6 (crate (name "docopt_macros") (vers "0.6.33") (deps (list (crate-dep (name "docopt") (req "*") (default-features #t) (kind 0)) (crate-dep (name "rustc-serialize") (req "*") (default-features #t) (kind 2)))) (hash "0571bfdys9527xxm8j6dw2f72sihbpnizz2zsi3rn96fkpjsi4z7")))

(define-public crate-docopt_macros-0.6 (crate (name "docopt_macros") (vers "0.6.34") (deps (list (crate-dep (name "docopt") (req "*") (default-features #t) (kind 0)) (crate-dep (name "rustc-serialize") (req "*") (default-features #t) (kind 2)))) (hash "1fahn8yzh430zy2aw0d4hd8jf3672lh919xhqxnwali3vgbai9gh")))

(define-public crate-docopt_macros-0.6 (crate (name "docopt_macros") (vers "0.6.35") (deps (list (crate-dep (name "docopt") (req "*") (default-features #t) (kind 0)) (crate-dep (name "rustc-serialize") (req "*") (default-features #t) (kind 2)))) (hash "030c52psz69hq3r4s7hcrpwvnrax8f423h2s8alvdd1z9hxgqdqy")))

(define-public crate-docopt_macros-0.6 (crate (name "docopt_macros") (vers "0.6.36") (deps (list (crate-dep (name "docopt") (req "*") (default-features #t) (kind 0)) (crate-dep (name "rustc-serialize") (req "*") (default-features #t) (kind 2)))) (hash "0x8wp3wvp360hfg9grjpmn3gjb1b708w03y6aqk1l66x5pwy7jn0")))

(define-public crate-docopt_macros-0.6 (crate (name "docopt_macros") (vers "0.6.37") (deps (list (crate-dep (name "docopt") (req "*") (default-features #t) (kind 0)) (crate-dep (name "rustc-serialize") (req "*") (default-features #t) (kind 2)))) (hash "1iacmp88j60355piafxzi5xrsq2jk0qz77jv9dwhnwi8v10xdk6c")))

(define-public crate-docopt_macros-0.6 (crate (name "docopt_macros") (vers "0.6.38") (deps (list (crate-dep (name "docopt") (req "*") (default-features #t) (kind 0)) (crate-dep (name "rustc-serialize") (req "*") (default-features #t) (kind 2)))) (hash "04pb51awf0qfdikbhp5fqajln4nsg211w4lckl9l7pidxrqks6r5")))

(define-public crate-docopt_macros-0.6 (crate (name "docopt_macros") (vers "0.6.39") (deps (list (crate-dep (name "docopt") (req "*") (default-features #t) (kind 0)) (crate-dep (name "rustc-serialize") (req "*") (default-features #t) (kind 2)))) (hash "1g8m2alxwq9b3l5qsr204xsifr1244ppmi0q996z3knyapchx04k")))

(define-public crate-docopt_macros-0.6 (crate (name "docopt_macros") (vers "0.6.40") (deps (list (crate-dep (name "docopt") (req "*") (default-features #t) (kind 0)) (crate-dep (name "rustc-serialize") (req "*") (default-features #t) (kind 2)))) (hash "0mmdbbk6rymlafnxvjmm13v63gjpjkhnp0z8cwbvfw760qz22h5i")))

(define-public crate-docopt_macros-0.6 (crate (name "docopt_macros") (vers "0.6.41") (deps (list (crate-dep (name "docopt") (req "*") (default-features #t) (kind 0)) (crate-dep (name "rustc-serialize") (req "*") (default-features #t) (kind 2)))) (hash "01xldgdh38gk7ckmxfkklzsniv8nr419zi7h62fmzpyl1gi2mhqm")))

(define-public crate-docopt_macros-0.6 (crate (name "docopt_macros") (vers "0.6.42") (deps (list (crate-dep (name "docopt") (req "*") (default-features #t) (kind 0)) (crate-dep (name "rustc-serialize") (req "^0.2.0") (default-features #t) (kind 2)))) (hash "07a7miff8m8b27adwmg2lpl9lx0dr6qs6p6x5c0jvp25k1q00wfq")))

(define-public crate-docopt_macros-0.6 (crate (name "docopt_macros") (vers "0.6.43") (deps (list (crate-dep (name "docopt") (req "*") (default-features #t) (kind 0)) (crate-dep (name "rustc-serialize") (req "^0.3") (default-features #t) (kind 2)))) (hash "1qq1a18phws9vsvh54nzjh0kk532c4w959qxf3a4cxvqp9s4s894")))

(define-public crate-docopt_macros-0.6 (crate (name "docopt_macros") (vers "0.6.44") (deps (list (crate-dep (name "docopt") (req "*") (default-features #t) (kind 0)) (crate-dep (name "rustc-serialize") (req "^0.3") (default-features #t) (kind 2)))) (hash "0shzggchlnrl8i559xamyjb6gxb7sbq7lvdjgqnb24qh4hq0grj2")))

(define-public crate-docopt_macros-0.6 (crate (name "docopt_macros") (vers "0.6.45") (deps (list (crate-dep (name "docopt") (req "*") (default-features #t) (kind 0)) (crate-dep (name "rustc-serialize") (req "^0.3") (default-features #t) (kind 2)))) (hash "0lc23s8w5p6r5g734r9059v43x8zrdkgwcnsl03zxpwnn49zhpb9")))

(define-public crate-docopt_macros-0.6 (crate (name "docopt_macros") (vers "0.6.47") (deps (list (crate-dep (name "docopt") (req "*") (default-features #t) (kind 0)) (crate-dep (name "rustc-serialize") (req "^0.3") (default-features #t) (kind 2)))) (hash "0hd2amz55hjrh0i2vy97ck3sfc26769slv5cx3r8mfxp4dxff4cg")))

(define-public crate-docopt_macros-0.6 (crate (name "docopt_macros") (vers "0.6.49") (deps (list (crate-dep (name "docopt") (req "*") (default-features #t) (kind 0)) (crate-dep (name "rustc-serialize") (req "^0.3") (default-features #t) (kind 2)))) (hash "099286vabhy24piqv0za1fjxmrqrmj076756pszlvkimxcjlsbjv")))

(define-public crate-docopt_macros-0.6 (crate (name "docopt_macros") (vers "0.6.50") (deps (list (crate-dep (name "docopt") (req "*") (default-features #t) (kind 0)) (crate-dep (name "rustc-serialize") (req "^0.3") (default-features #t) (kind 2)))) (hash "0c0vd7b53i80jxx37xypirrzwnd0zck41wpb2ly80kqw92h5x9nl")))

(define-public crate-docopt_macros-0.6 (crate (name "docopt_macros") (vers "0.6.51") (deps (list (crate-dep (name "docopt") (req "*") (default-features #t) (kind 0)) (crate-dep (name "rustc-serialize") (req "^0.3") (default-features #t) (kind 2)))) (hash "1izr86qqzwnphhi8zsicz4jzrcvdg7s7c1wy1qn7yvisy82jgsba")))

(define-public crate-docopt_macros-0.6 (crate (name "docopt_macros") (vers "0.6.52") (deps (list (crate-dep (name "docopt") (req "*") (default-features #t) (kind 0)) (crate-dep (name "rustc-serialize") (req "^0.3") (default-features #t) (kind 2)))) (hash "1ln43b62rls4rhpqcnac28kcq7ravx5nxd0qimliy81l69kzg4sn")))

(define-public crate-docopt_macros-0.6 (crate (name "docopt_macros") (vers "0.6.53") (deps (list (crate-dep (name "docopt") (req "*") (default-features #t) (kind 0)) (crate-dep (name "rustc-serialize") (req "^0.3") (default-features #t) (kind 2)))) (hash "1k6cjma5iqiv4627n9l0wj0jm146cxzvhr5bcigh52nap4dnilcv")))

(define-public crate-docopt_macros-0.6 (crate (name "docopt_macros") (vers "0.6.54") (deps (list (crate-dep (name "docopt") (req "*") (default-features #t) (kind 0)) (crate-dep (name "rustc-serialize") (req "^0.3") (default-features #t) (kind 2)))) (hash "1axqhh1zlqi9ymp39lc9gsngikfc1y5k8qd11zlg04047immpl5h")))

(define-public crate-docopt_macros-0.6 (crate (name "docopt_macros") (vers "0.6.55") (deps (list (crate-dep (name "docopt") (req "*") (default-features #t) (kind 0)) (crate-dep (name "rustc-serialize") (req "^0.3") (default-features #t) (kind 2)))) (hash "04n3mylvcf2a69idpnzvic4d34mr49i6f1ayw1w0mh6wq6rj2bxc")))

(define-public crate-docopt_macros-0.6 (crate (name "docopt_macros") (vers "0.6.56") (deps (list (crate-dep (name "docopt") (req "*") (default-features #t) (kind 0)) (crate-dep (name "rustc-serialize") (req "^0.3") (default-features #t) (kind 2)))) (hash "12w2cbgj85xb511xfy235wjkvpp3lyh0y6cp1sqjpyhm7bljvhcn")))

(define-public crate-docopt_macros-0.6 (crate (name "docopt_macros") (vers "0.6.57") (deps (list (crate-dep (name "docopt") (req "*") (default-features #t) (kind 0)) (crate-dep (name "rustc-serialize") (req "^0.3") (default-features #t) (kind 2)))) (hash "0a9i1vh58wgj8s1zyir8kkpmn8vfzdp92bqbla1vdvhi5njlbpvw")))

(define-public crate-docopt_macros-0.6 (crate (name "docopt_macros") (vers "0.6.58") (deps (list (crate-dep (name "docopt") (req "*") (default-features #t) (kind 0)) (crate-dep (name "rustc-serialize") (req "^0.3") (default-features #t) (kind 2)))) (hash "00cqp4f4z4c54cfsb1vgb7h3hvkmx9jqj9795f9zjmmxl2ddjydb")))

(define-public crate-docopt_macros-0.6 (crate (name "docopt_macros") (vers "0.6.59") (deps (list (crate-dep (name "docopt") (req "*") (default-features #t) (kind 0)) (crate-dep (name "rustc-serialize") (req "^0.3") (default-features #t) (kind 2)))) (hash "0k04399gv87l6bplbm3dwiksxalcgd02jxvgj12lq4s0czz1fq6d")))

(define-public crate-docopt_macros-0.6 (crate (name "docopt_macros") (vers "0.6.60") (deps (list (crate-dep (name "docopt") (req "*") (default-features #t) (kind 0)) (crate-dep (name "rustc-serialize") (req "^0.3") (default-features #t) (kind 2)))) (hash "1lligzwgjsx3whqzampik4qqags7nqi4678j8jg2bh9s52n3gkiv")))

(define-public crate-docopt_macros-0.6 (crate (name "docopt_macros") (vers "0.6.62") (deps (list (crate-dep (name "docopt") (req "*") (default-features #t) (kind 0)) (crate-dep (name "rustc-serialize") (req "^0.3") (default-features #t) (kind 2)))) (hash "1gj3d4j96zrckvmslm8j64mamgpnay97kx0igjpmw7nadisz72n9")))

(define-public crate-docopt_macros-0.6 (crate (name "docopt_macros") (vers "0.6.63") (deps (list (crate-dep (name "docopt") (req "*") (default-features #t) (kind 0)) (crate-dep (name "rustc-serialize") (req "^0.3") (default-features #t) (kind 2)))) (hash "0x9a9qs6zmqhal6d79hrx6hf0x6bf4q662x9rcm25d75q1xrvvv7")))

(define-public crate-docopt_macros-0.6 (crate (name "docopt_macros") (vers "0.6.64") (deps (list (crate-dep (name "docopt") (req "*") (default-features #t) (kind 0)) (crate-dep (name "rustc-serialize") (req "^0.3") (default-features #t) (kind 2)))) (hash "1lwj76gxkpa7ymzvcrzssfp2mdrcqv89c7d3fiab0g6vzhwsiq6f")))

(define-public crate-docopt_macros-0.6 (crate (name "docopt_macros") (vers "0.6.65") (deps (list (crate-dep (name "docopt") (req "*") (default-features #t) (kind 0)) (crate-dep (name "rustc-serialize") (req "^0.3") (default-features #t) (kind 2)))) (hash "1zcbdzq0lgz5962hywrqr95z79gfq6j776a97gdz2g7cbfpbgydf")))

(define-public crate-docopt_macros-0.6 (crate (name "docopt_macros") (vers "0.6.66") (deps (list (crate-dep (name "docopt") (req "*") (default-features #t) (kind 0)) (crate-dep (name "rustc-serialize") (req "^0.3") (default-features #t) (kind 2)))) (hash "1w8mlzag1blnpx16h3cfriqm7d3r71h2cyzj4b2gkqqrpv7gczc1")))

(define-public crate-docopt_macros-0.6 (crate (name "docopt_macros") (vers "0.6.67") (deps (list (crate-dep (name "docopt") (req "*") (default-features #t) (kind 0)) (crate-dep (name "rustc-serialize") (req "^0.3") (default-features #t) (kind 2)))) (hash "06nbxnrfmzsma89sr0km3m2k3yzfhahxrgs100ya1cka6p4sb9q9")))

(define-public crate-docopt_macros-0.6 (crate (name "docopt_macros") (vers "0.6.68") (deps (list (crate-dep (name "docopt") (req "*") (default-features #t) (kind 0)) (crate-dep (name "rustc-serialize") (req "^0.3") (default-features #t) (kind 2)))) (hash "0qcvwsknnr2h6nx7m33a0xhprga2ygbcx7058zw3shkynq4y2bxf")))

(define-public crate-docopt_macros-0.6 (crate (name "docopt_macros") (vers "0.6.69") (deps (list (crate-dep (name "docopt") (req "*") (default-features #t) (kind 0)) (crate-dep (name "rustc-serialize") (req "^0.3") (default-features #t) (kind 2)))) (hash "0n6nbl49vyjv1apy5bzs6d4afipzz4wq6hksg4813pvx65m93hms")))

(define-public crate-docopt_macros-0.6 (crate (name "docopt_macros") (vers "0.6.70") (deps (list (crate-dep (name "docopt") (req "*") (default-features #t) (kind 0)) (crate-dep (name "rustc-serialize") (req "^0.3") (default-features #t) (kind 2)))) (hash "1z7xdbvbnxl29bqna09pck3bh9pl36sgz6s0lrgj42fa5vzwdr0y")))

(define-public crate-docopt_macros-0.6 (crate (name "docopt_macros") (vers "0.6.71") (deps (list (crate-dep (name "docopt") (req "*") (default-features #t) (kind 0)) (crate-dep (name "rustc-serialize") (req "^0.3") (default-features #t) (kind 2)))) (hash "0lzxvzbmm0nyq0qkilgjzm6zdl452mwnrm5634jc9prlrjdf5k5s")))

(define-public crate-docopt_macros-0.6 (crate (name "docopt_macros") (vers "0.6.72") (deps (list (crate-dep (name "docopt") (req "^0.6") (default-features #t) (kind 0)) (crate-dep (name "rustc-serialize") (req "^0.3") (default-features #t) (kind 2)))) (hash "1646sh6mkz9dafn7pzw3hzml5mpjbwj3qas3v2cwz050h4l889p8")))

(define-public crate-docopt_macros-0.6 (crate (name "docopt_macros") (vers "0.6.73") (deps (list (crate-dep (name "docopt") (req "^0.6") (default-features #t) (kind 0)) (crate-dep (name "rustc-serialize") (req "^0.3") (default-features #t) (kind 2)))) (hash "0ck88rvimqyml1kpzk340za7485m0viks4waff9p2ac8fkhb0asd")))

(define-public crate-docopt_macros-0.6 (crate (name "docopt_macros") (vers "0.6.74") (deps (list (crate-dep (name "docopt") (req "^0.6") (default-features #t) (kind 0)) (crate-dep (name "rustc-serialize") (req "^0.3") (default-features #t) (kind 2)))) (hash "18pdk4b5c7ywqil9r37njh17hbp4jv6fkm6xhclglsl5v819sf1n")))

(define-public crate-docopt_macros-0.6 (crate (name "docopt_macros") (vers "0.6.75") (deps (list (crate-dep (name "docopt") (req "^0.6") (default-features #t) (kind 0)) (crate-dep (name "rustc-serialize") (req "^0.3") (default-features #t) (kind 2)))) (hash "1hl3b2rivynk8m90a6aj51gzy5643k54jbbbp2rn5xl77gf3jlz1")))

(define-public crate-docopt_macros-0.6 (crate (name "docopt_macros") (vers "0.6.76") (deps (list (crate-dep (name "docopt") (req "^0.6") (default-features #t) (kind 0)) (crate-dep (name "rustc-serialize") (req "^0.3") (default-features #t) (kind 2)))) (hash "0hcyqf983pl5cwn9g7zr7qz4rflhw1aqlc8hmh8mybxd9kaz0zp2")))

(define-public crate-docopt_macros-0.6 (crate (name "docopt_macros") (vers "0.6.78") (deps (list (crate-dep (name "docopt") (req "^0.6") (default-features #t) (kind 0)) (crate-dep (name "rustc-serialize") (req "^0.3") (default-features #t) (kind 2)))) (hash "0ly7zdjcqh298pkr192pn3zmlqfhjzqz2v0qy5i88h0jvfq0bcrh")))

(define-public crate-docopt_macros-0.6 (crate (name "docopt_macros") (vers "0.6.79") (deps (list (crate-dep (name "docopt") (req "^0.6") (default-features #t) (kind 0)) (crate-dep (name "rustc-serialize") (req "^0.3") (default-features #t) (kind 2)))) (hash "0vl6640wxjjyfffg1qlk7fg4b2q9r12ybja6ribgxlyl418b64hj")))

(define-public crate-docopt_macros-0.6 (crate (name "docopt_macros") (vers "0.6.80") (deps (list (crate-dep (name "docopt") (req "^0.6") (default-features #t) (kind 0)) (crate-dep (name "rustc-serialize") (req "^0.3") (default-features #t) (kind 2)))) (hash "003xm7simifmj8c3n3xbi9sii3ng6vv6zkjqww1maasmvjsr2qbc")))

(define-public crate-docopt_macros-0.6 (crate (name "docopt_macros") (vers "0.6.81") (deps (list (crate-dep (name "docopt") (req "^0.6") (default-features #t) (kind 0)) (crate-dep (name "rustc-serialize") (req "^0.3") (default-features #t) (kind 2)))) (hash "1iak7dkjrrg0vjylapxk4ri778kp074g8a794j0nyk6qkfndjsy6")))

(define-public crate-docopt_macros-0.6 (crate (name "docopt_macros") (vers "0.6.82") (deps (list (crate-dep (name "docopt") (req "^0.6") (default-features #t) (kind 0)) (crate-dep (name "rustc-serialize") (req "^0.3") (default-features #t) (kind 2)))) (hash "150kkxx4hca6g448qqc0jkqhhbwgm1maanik8qx5p3bap6y1bf4c")))

(define-public crate-docopt_macros-0.6 (crate (name "docopt_macros") (vers "0.6.83") (deps (list (crate-dep (name "docopt") (req "^0.6.80") (default-features #t) (kind 0)) (crate-dep (name "rustc-serialize") (req "^0.3") (default-features #t) (kind 2)))) (hash "15cw7j564g2cmwdiay5bcn7n9gdhvbdi2qrm7zpw4dhqrfmf50xx")))

(define-public crate-docopt_macros-0.6 (crate (name "docopt_macros") (vers "0.6.84") (deps (list (crate-dep (name "docopt") (req "^0.6.80") (default-features #t) (kind 0)) (crate-dep (name "rustc-serialize") (req "^0.3") (default-features #t) (kind 2)))) (hash "06hb8l7f6layvpwpizlhy96rnai0bjrij1vlr5rpiyfvb0vrc1qg")))

(define-public crate-docopt_macros-0.6 (crate (name "docopt_macros") (vers "0.6.85") (deps (list (crate-dep (name "docopt") (req "^0.6.80") (default-features #t) (kind 0)) (crate-dep (name "rustc-serialize") (req "^0.3") (default-features #t) (kind 2)))) (hash "0h6q8qmdz30ynd4g8602z9lws0j5nqc6rk2j0cdc9ghb48qamk18")))

(define-public crate-docopt_macros-0.6 (crate (name "docopt_macros") (vers "0.6.86") (deps (list (crate-dep (name "docopt") (req "^0.6.80") (default-features #t) (kind 0)) (crate-dep (name "rustc-serialize") (req "^0.3") (default-features #t) (kind 2)))) (hash "1n9a524n3mdxab9dfq7ycmqxf93qgkl5acxp1ihclj05wm1x31n4")))

(define-public crate-docopt_macros-0.6 (crate (name "docopt_macros") (vers "0.6.87") (deps (list (crate-dep (name "docopt") (req "^0.6.80") (default-features #t) (kind 0)) (crate-dep (name "rustc-serialize") (req "^0.3") (default-features #t) (kind 2)))) (hash "136kmwpf562s006vr9prmw0y1kl7wgf0v05rrvi7cjz8viinz5c2")))

(define-public crate-docopt_macros-0.6 (crate (name "docopt_macros") (vers "0.6.88") (deps (list (crate-dep (name "docopt") (req "^0.6.80") (default-features #t) (kind 0)) (crate-dep (name "rustc-serialize") (req "^0.3") (default-features #t) (kind 2)))) (hash "0z9j13312ic8pssmi42xy93zv9ns2ihzll91x6vv5rn9cxgsrg5p")))

(define-public crate-docopt_macros-0.7 (crate (name "docopt_macros") (vers "0.7.0") (deps (list (crate-dep (name "docopt") (req "^0.7.0") (default-features #t) (kind 0)) (crate-dep (name "rustc-serialize") (req "^0.3") (default-features #t) (kind 2)))) (hash "0bq1rnjr8qxa5gqrwn19y86cqh5clkd4kgrdhp4jcf8dmf49nhxc")))

(define-public crate-docopt_macros-0.8 (crate (name "docopt_macros") (vers "0.8.0") (deps (list (crate-dep (name "docopt") (req "^0.8.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (default-features #t) (kind 2)) (crate-dep (name "serde_derive") (req "^1.0") (default-features #t) (kind 2)))) (hash "0ad9nfh5knllhnzs99y4gzgx2ww0c16dpvnqbg2ichg2vkpc33k7")))

(define-public crate-docopt_macros-0.8 (crate (name "docopt_macros") (vers "0.8.1") (deps (list (crate-dep (name "docopt") (req "^0.8.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (default-features #t) (kind 2)) (crate-dep (name "serde_derive") (req "^1.0") (default-features #t) (kind 2)))) (hash "055nwh0b8c2biml9pckl5dgl6fsff9wk83adwjj8kblr8as0145v")))

(define-public crate-docopticon-0.1 (crate (name "docopticon") (vers "0.1.0") (deps (list (crate-dep (name "assert_fs") (req "^1.1.0") (default-features #t) (kind 2)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)))) (hash "1cbdld6qxipr2sya2266nckzjvaa95svc9pbqh7b07rmsrjc0ad6") (features (quote (("std" "serde")))) (v 2) (features2 (quote (("serde" "dep:serde"))))))

(define-public crate-docopticon-0.1 (crate (name "docopticon") (vers "0.1.1") (deps (list (crate-dep (name "assert_fs") (req "^1.1.0") (default-features #t) (kind 2)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)))) (hash "1x1b445xvv99fxrvlsw85w744hhpxmzq6wb824mpmx6nfds8vy05") (features (quote (("std" "serde")))) (v 2) (features2 (quote (("serde" "dep:serde"))))))

(define-public crate-docopticon-0.1 (crate (name "docopticon") (vers "0.1.2") (deps (list (crate-dep (name "assert_fs") (req "^1.1.0") (default-features #t) (kind 2)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)))) (hash "1443n2q2k2nm5hw9lz272rvnd2rajk9axh7knh3in8m2hvnpdrwl") (features (quote (("std" "serde")))) (v 2) (features2 (quote (("serde" "dep:serde"))))))

