(define-module (crates-io do nh) #:use-module (crates-io))

(define-public crate-donhcd-sentry-0.1 (crate (name "donhcd-sentry") (vers "0.1.5") (deps (list (crate-dep (name "chrono") (req "^0.2.22") (default-features #t) (kind 0)) (crate-dep (name "hyper") (req "^0.9.5") (default-features #t) (kind 0)) (crate-dep (name "time") (req "^0.1.35") (default-features #t) (kind 0)))) (hash "145m09fcbk74izp2jp9v968m35xanhsa0hklpwpwgc93253gcgya")))

