(define-module (crates-io do -a) #:use-module (crates-io))

(define-public crate-do-app-0.1 (crate (name "do-app") (vers "0.1.0") (deps (list (crate-dep (name "console") (req "^0.14") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_yaml") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0") (default-features #t) (kind 0)))) (hash "04hqqsx0dk1vqca18gmicn8ya6nkykpgfc1h63x2gd56zidz6hfb")))

(define-public crate-do-async-0.0.0 (crate (name "do-async") (vers "0.0.0") (hash "096nh58bmxnmd49av4m2cr0r8grssyhy7h979nkkb44q3h1pn9ci")))

