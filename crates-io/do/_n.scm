(define-module (crates-io do _n) #:use-module (crates-io))

(define-public crate-do_nothing-0.1 (crate (name "do_nothing") (vers "0.1.0") (hash "1wi0wwwl45s2hcivzak0jjdkdr90kf2miq1zxbd5hijc9qhms13z")))

(define-public crate-do_nothing-0.1 (crate (name "do_nothing") (vers "0.1.1") (hash "1pa8jm1nbs8bapmc9n9711xvld35i6hfryx2p5j8rnglllfjns0n")))

(define-public crate-do_nothing-0.1 (crate (name "do_nothing") (vers "0.1.2") (hash "158r110kl95shw9xa762w4cviw7v4n9qy3qg901y2mf6gjq1k7sf")))

