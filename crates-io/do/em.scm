(define-module (crates-io do em) #:use-module (crates-io))

(define-public crate-doem-math-1 (crate (name "doem-math") (vers "1.0.0") (deps (list (crate-dep (name "staticvec") (req "^0.8.0") (default-features #t) (kind 0)))) (hash "0z0xcf94k81qng7wxbf5k9mdfmdb70f5y6caibgimxn4kyk8v4k5")))

(define-public crate-doem-math-1 (crate (name "doem-math") (vers "1.0.1") (deps (list (crate-dep (name "staticvec") (req "^0.8.0") (default-features #t) (kind 0)))) (hash "05vbz6s7d2irrn2sdgnw4ncsc2pbscv2b4czl6ac7s8v9v5vs8fr")))

