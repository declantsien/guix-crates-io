(define-module (crates-io do mt) #:use-module (crates-io))

(define-public crate-domtree-0.1 (crate (name "domtree") (vers "0.1.0") (deps (list (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 2)))) (hash "1sm9bhcz5hgw1nr7m2saaj3ksd1lwa17cjlzp5i4jplmv3r6vrp5")))

(define-public crate-domtree-0.2 (crate (name "domtree") (vers "0.2.0") (deps (list (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 2)))) (hash "0p81rdqvj1i0mqbxdk1rwidcx3al23hvlzzj2rkmh5xl58wwskr2") (features (quote (("materialized_idf"))))))

