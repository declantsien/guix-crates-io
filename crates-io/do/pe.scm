(define-module (crates-io do pe) #:use-module (crates-io))

(define-public crate-dope-0.1 (crate (name "dope") (vers "0.1.0") (deps (list (crate-dep (name "chrono") (req "^0") (default-features #t) (kind 0)) (crate-dep (name "failure") (req "^0") (default-features #t) (kind 0)) (crate-dep (name "futures-preview") (req "^0.3.0-alpha") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0") (default-features #t) (kind 0)) (crate-dep (name "slab") (req "^0") (default-features #t) (kind 0)))) (hash "1s94jka8l2r45hib2my0bnvap9szk9spvcv2wcvrg172d77knpkb")))

