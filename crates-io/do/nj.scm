(define-module (crates-io do nj) #:use-module (crates-io))

(define-public crate-donjo-0.1 (crate (name "donjo") (vers "0.1.0") (deps (list (crate-dep (name "colored") (req "^1.8") (default-features #t) (kind 0)) (crate-dep (name "dirs") (req "^2.0.1") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.2.18") (default-features #t) (kind 0)))) (hash "0k9p37vv283p26l36n5bsmaz1xx0ndfyy5ls6p1rlhhv8qrn6iq9")))

