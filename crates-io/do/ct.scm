(define-module (crates-io do ct) #:use-module (crates-io))

(define-public crate-doctest-file-1 (crate (name "doctest-file") (vers "1.0.0") (hash "0qkmnrsx2kszm58wxyry63bs35msj9chdb6jlh54a8cdwaiizj5a") (rust-version "1.65")))

(define-public crate-doctest_aksjdlasjkdlajskdjasld-0.1 (crate (name "doctest_aksjdlasjkdlajskdjasld") (vers "0.1.0") (hash "1iwyqy1lrbb13p42r0vcy1d0qf325jn99zil9avk31bhm9382zk6") (yanked #t)))

(define-public crate-doctor-0.1 (crate (name "doctor") (vers "0.1.0") (deps (list (crate-dep (name "nom") (req "^6.0.1") (default-features #t) (kind 0)))) (hash "01214lz8swxkhis7kk9nj5kg42l0i24jvzql76y28pscfxan1mpl")))

(define-public crate-doctor-0.2 (crate (name "doctor") (vers "0.2.0") (deps (list (crate-dep (name "nom") (req "^6.0.1") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)))) (hash "16y9526mm8ni5cb39brcgndvs2qxsxjqzvbn0jq2m7s0rf6lh65x")))

(define-public crate-doctor-0.2 (crate (name "doctor") (vers "0.2.1") (deps (list (crate-dep (name "nom") (req "^6.0.1") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)))) (hash "0s17plirzay6rm9bawjyvg67dy6kghs5smi8bvqp112n1jz4p96k")))

(define-public crate-doctor-0.2 (crate (name "doctor") (vers "0.2.2") (deps (list (crate-dep (name "nom") (req "^6.0.1") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "version-sync") (req "^0.9") (default-features #t) (kind 2)))) (hash "1j8aq3092w3zbxymvac5787gp8rp92n6mgb2g4mkhgx6m9w2j2nw")))

(define-public crate-doctor-0.3 (crate (name "doctor") (vers "0.3.0") (deps (list (crate-dep (name "doc-comment") (req "^0.3.3") (default-features #t) (kind 2)) (crate-dep (name "nom") (req "^6.0.1") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "version-sync") (req "^0.9") (default-features #t) (kind 2)))) (hash "0yxqym40v4grc0zn1nwcz6fgp7vjdjjq84lf3saybv3i2mm05b5q")))

(define-public crate-doctor-0.3 (crate (name "doctor") (vers "0.3.1") (deps (list (crate-dep (name "doc-comment") (req "^0.3.3") (default-features #t) (kind 2)) (crate-dep (name "nom") (req "^6.0.1") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "version-sync") (req "^0.9") (default-features #t) (kind 2)))) (hash "13g9mni9q242kbajx8k7nsqybwr1nh2ywgb69jj5s64dnbrbccyv")))

(define-public crate-doctor-0.3 (crate (name "doctor") (vers "0.3.2") (deps (list (crate-dep (name "nom") (req "^6.0.1") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "version-sync") (req "^0.9") (default-features #t) (kind 2)))) (hash "0qcfzfqa92kax6sna846n8624ds499hj9gkydcha284wv890khp1")))

(define-public crate-doctor-0.3 (crate (name "doctor") (vers "0.3.3") (deps (list (crate-dep (name "nom") (req "^6.0.1") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "version-sync") (req "^0.9") (default-features #t) (kind 2)))) (hash "1hfrbdg0q2kycdbg8vzvnkj2fz3c0fzk07fv1ncvfx6xjl0q7mb6")))

(define-public crate-doctor-0.3 (crate (name "doctor") (vers "0.3.4") (deps (list (crate-dep (name "nom") (req "^6.0.1") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)))) (hash "0k8x3rsm31ldv6favdqplsrirh9qsz3aqzryd9b4gd5l6ab81z92")))

(define-public crate-doctor-diff-cli-0.1 (crate (name "doctor-diff-cli") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^2.33") (default-features #t) (kind 0)) (crate-dep (name "doctor-diff-core") (req "^0.1") (default-features #t) (kind 0)))) (hash "0jvc0v2l4am0b9awx9imwv1y69rkbjb67hwvyn5wzq2csznsir1d")))

(define-public crate-doctor-diff-core-0.1 (crate (name "doctor-diff-core") (vers "0.1.0") (deps (list (crate-dep (name "ring") (req "^0.16") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "zip") (req "^0.5") (default-features #t) (kind 0)))) (hash "007cv3fdc8s6jwigfldjwdq205dml5aagr9zhsqb1q81y4yf7jr8")))

(define-public crate-doctor-syn-0.1 (crate (name "doctor-syn") (vers "0.1.0") (deps (list (crate-dep (name "num-traits") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (features (quote ("full" "extra-traits"))) (default-features #t) (kind 0)))) (hash "1l4v2z2km4ncajbznq3k3bwz07d2p407igddia7pyxmfhf06l1lh")))

