(define-module (crates-io do _s) #:use-module (crates-io))

(define-public crate-do_syscall-1 (crate (name "do_syscall") (vers "1.0.0") (hash "0ghd6bc2h3d0avknzwdpyx4l1rym5qlgfd3cq6q91zcp5z8igmnv") (yanked #t)))

(define-public crate-do_syscall-1 (crate (name "do_syscall") (vers "1.0.1") (hash "18ya1g2zhj9al1fmkm0df9zn99mygf8z8fnv7jxm4228f2zi837f") (yanked #t)))

(define-public crate-do_syscall-1 (crate (name "do_syscall") (vers "1.0.2") (hash "1dxjlkdn3640vbjf7a5br42mb0b7r4lji5ldr4snjy2dn6k0bil7")))

