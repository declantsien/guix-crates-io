(define-module (crates-io do ri) #:use-module (crates-io))

(define-public crate-dorian-0.1 (crate (name "dorian") (vers "0.1.0") (deps (list (crate-dep (name "llvm-sys") (req "^120.1.0") (default-features #t) (kind 0)))) (hash "0ryp4694jpd34syg6pp3ww1ka8664nqa8ifi9brd8lc9hxi48kv0")))

(define-public crate-dorian-0.1 (crate (name "dorian") (vers "0.1.1") (deps (list (crate-dep (name "llvm-sys") (req "^120.1.0") (default-features #t) (kind 0)))) (hash "1fkdmwpl2i50jgyk8x4y4css9aaxgrzfmkk1acmc5cys078cnchp")))

(define-public crate-dorian-0.2 (crate (name "dorian") (vers "0.2.0") (deps (list (crate-dep (name "llvm-sys") (req "^140.0.2") (default-features #t) (kind 0)))) (hash "1xsh2yy0dxmfi7a22dkqbm58mlwwky0x5qw1rc5dghq1ih5n9ggg")))

(define-public crate-dorian-0.2 (crate (name "dorian") (vers "0.2.1") (deps (list (crate-dep (name "llvm-sys") (req "^140.0.2") (default-features #t) (kind 0)))) (hash "04m9lam6ckifq8ikj2p3a7vjl1gm5ysy9kx88lnyj0xnp92chg93")))

(define-public crate-doric-0.1 (crate (name "doric") (vers "0.1.0") (deps (list (crate-dep (name "arc-swap") (req "^1.2") (default-features #t) (kind 0)) (crate-dep (name "chrono") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "crossbeam-queue") (req "^0.3.1") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.8") (features (quote ("std"))) (default-features #t) (kind 0)))) (hash "0fqzrn1y5zvvi18xibgfb5lqk2rffp3bsbbqy64kksw3937xbdri")))

(define-public crate-doris-0.0.0 (crate (name "doris") (vers "0.0.0") (hash "1dgd9g5wpl5ppfxbljh03pyihqjh1344z6c41riwn7vmmkf520pd")))

