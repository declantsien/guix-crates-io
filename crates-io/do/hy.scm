(define-module (crates-io do hy) #:use-module (crates-io))

(define-public crate-dohy-0.1 (crate (name "dohy") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^4.1.6") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "dothyphen") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "1wgjh00sshsk1zbqjx0l36hlv9v0pk8ggrv74j5nn91hgfzgl6h1")))

(define-public crate-dohy-0.2 (crate (name "dohy") (vers "0.2.0") (deps (list (crate-dep (name "clap") (req "^4.1.6") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "dothyphen") (req "^0.2.0") (default-features #t) (kind 0)))) (hash "0ir1f0c5cadczqara5c0k2vcyynhncs75lnq7pfcmzmffq25hqvb")))

(define-public crate-dohy-0.2 (crate (name "dohy") (vers "0.2.1") (deps (list (crate-dep (name "clap") (req "^4.1.6") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "dothyphen") (req "^0.2.1") (default-features #t) (kind 0)))) (hash "1vfkw0357bvmajqsy8pl7dr1wl2r3z4vg3hgibs8c57b4hjgyx88")))

(define-public crate-dohy-0.2 (crate (name "dohy") (vers "0.2.2") (deps (list (crate-dep (name "clap") (req "^4.4") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "dothyphen") (req "^0.2") (default-features #t) (kind 0)))) (hash "03b6ln08kirakfsb2alm76pdd99ciycfcyaz5dl2pm4bya1qxkfh")))

(define-public crate-dohy-1 (crate (name "dohy") (vers "1.0.0") (deps (list (crate-dep (name "clap") (req "^4.4") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "dothyphen") (req "^1.0.0") (default-features #t) (kind 0)))) (hash "1wbvi3fgl064xgg7p0h1kna78a7kh7vx40javim6xr98fi7svilj")))

