(define-module (crates-io do tz) #:use-module (crates-io))

(define-public crate-dotz-1 (crate (name "dotz") (vers "1.0.0") (deps (list (crate-dep (name "assert_cli") (req "^0.6.3") (default-features #t) (kind 2)))) (hash "1hgx60pm0dcia7kcfb2jhw2mzhb8xhhqdnvaxzcrq3kh9pl4ck4a")))

(define-public crate-dotz-1 (crate (name "dotz") (vers "1.0.1") (deps (list (crate-dep (name "assert_cli") (req "^0.6.3") (default-features #t) (kind 2)))) (hash "0swq050n34x3x4hd0d54hk0g08rjnhxhc9kcvc28a37knpndxikj")))

(define-public crate-dotz-1 (crate (name "dotz") (vers "1.0.2") (deps (list (crate-dep (name "assert_cli") (req "^0.6.3") (default-features #t) (kind 2)))) (hash "1h8y1i1yvjxq6asqf2glnxl1sqhx60c1kc4nmrj4ryy4h077ll39")))

(define-public crate-dotz-1 (crate (name "dotz") (vers "1.0.3") (deps (list (crate-dep (name "assert_cli") (req "^0.6.3") (default-features #t) (kind 2)))) (hash "052mmwn2m1zdpjf6nm5ldz7xlyll50af2i6iqf7zc6ybx5qcbnyp")))

