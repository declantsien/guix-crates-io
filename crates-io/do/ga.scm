(define-module (crates-io do ga) #:use-module (crates-io))

(define-public crate-doga-add-numbers-0.1 (crate (name "doga-add-numbers") (vers "0.1.0") (hash "0sjr7gj2l88d1w1m0bzriv08qpxddx03vxn1c4w81srj1kkjbd24")))

(define-public crate-doga-english-greetings-0.1 (crate (name "doga-english-greetings") (vers "0.1.0") (deps (list (crate-dep (name "fastrand") (req "^1.8.0") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.32") (default-features #t) (kind 0)))) (hash "1zz6d19pd6fjn031pdhr952iq0agg87mxhlrhlznndlffwfy4wrb")))

(define-public crate-doga-hello-world-0.1 (crate (name "doga-hello-world") (vers "0.1.0") (hash "1hnl4qazmw3v963q8zks54pw25ldz3300p3bvb7fsd2szllm37wi")))

(define-public crate-doga-multilingual-greetings-0.1 (crate (name "doga-multilingual-greetings") (vers "0.1.0") (deps (list (crate-dep (name "doga-reverse-string") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "fastrand") (req "^1.8.0") (default-features #t) (kind 0)))) (hash "0xrw7z8zj311f5gr56jlia1hq1w6ydxlyg6algn308fclm69kb3j")))

(define-public crate-doga-multilingual-greetings-0.1 (crate (name "doga-multilingual-greetings") (vers "0.1.1") (deps (list (crate-dep (name "doga-reverse-string") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "fastrand") (req "^1.8.0") (default-features #t) (kind 0)))) (hash "0kyv2lfhr5rmnk7cmk312dnbb74maja79cwnf3406y5anz30aqsb")))

(define-public crate-doga-multilingual-greetings-0.1 (crate (name "doga-multilingual-greetings") (vers "0.1.2") (deps (list (crate-dep (name "doga-reverse-string") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "fastrand") (req "^1.8.0") (default-features #t) (kind 0)))) (hash "1jbpjaysj5y19s5qn9vm6gwmj0rkvqzn1385gypspx4d0k173kgi")))

(define-public crate-doga-reverse-string-0.1 (crate (name "doga-reverse-string") (vers "0.1.0") (deps (list (crate-dep (name "unicode-segmentation") (req "^1.9.0") (default-features #t) (kind 0)))) (hash "0l3m48yadp1iyf64ipr737z3ybvsdzfqan1fxvdzly4m9ayc31zw")))

(define-public crate-dogapi-1 (crate (name "dogapi") (vers "1.0.0") (deps (list (crate-dep (name "reqwest") (req "^0.11") (features (quote ("blocking"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.0") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0.0") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.0") (default-features #t) (kind 0)))) (hash "0r5psv0rd3wyk3i9b2jkq3m2gbg05c40rrw4lpdcfvy6j6m8i1pm")))

