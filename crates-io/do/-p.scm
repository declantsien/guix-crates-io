(define-module (crates-io do -p) #:use-module (crates-io))

(define-public crate-do-paginate-0.1 (crate (name "do-paginate") (vers "0.1.0") (hash "1vaq8b1mz66g382z5v68sgvry55p0ydylv2a4k137qmh7h4lv9xv")))

(define-public crate-do-proxy-0.1 (crate (name "do-proxy") (vers "0.1.0") (deps (list (crate-dep (name "async-trait") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "paste") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive" "derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "worker") (req "^0.0.12") (default-features #t) (kind 0)))) (hash "03pngpk2v7jm3llp3shv0az2bb8n3dnjdfs8b0nhk3bq78iskkra")))

(define-public crate-do-proxy-0.1 (crate (name "do-proxy") (vers "0.1.1") (deps (list (crate-dep (name "async-trait") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "paste") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive" "derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "worker") (req "^0.0.12") (default-features #t) (kind 0)))) (hash "1kznw9q0w3436ipfd6ax7rk5zwk3cp2kd1gkqhg79m9fbkn8rjzl")))

