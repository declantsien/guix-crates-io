(define-module (crates-io do dg) #:use-module (crates-io))

(define-public crate-dodge-0.0.0 (crate (name "dodge") (vers "0.0.0") (hash "07pdfbdpqz0y6jp8hb9sw52wr1n5xmx16qjb4xrradx6sq6ma6pi")))

(define-public crate-dodgems-0.1 (crate (name "dodgems") (vers "0.1.0") (hash "0y6b2amn9q45w8cfzq1vgbjjk6anyvk7bjshjbd68x055v4zmrv2") (features (quote (("default" "alloc") ("alloc")))) (yanked #t)))

(define-public crate-dodgems-0.1 (crate (name "dodgems") (vers "0.1.1") (hash "1nwjq9pzhfzl2174m9jr6yx7ad7mvlpsm2pirhpwvppafg11nckz") (features (quote (("default" "alloc") ("alloc"))))))

(define-public crate-dodgy-1 (crate (name "dodgy") (vers "1.0.0") (deps (list (crate-dep (name "glam") (req "^0.23.0") (default-features #t) (kind 0)))) (hash "01hmmjd1vc1khbh7cnxl6yr63591cgqvplwjkwklkdnsxikzcwf8") (yanked #t)))

(define-public crate-dodgy-1 (crate (name "dodgy") (vers "1.1.0") (deps (list (crate-dep (name "glam") (req "^0.24.0") (default-features #t) (kind 0)))) (hash "1fpr2icy9p0m41g238lmqly7gl6n0q1jdi4p9l6cn6y7qda3cc4h") (yanked #t)))

(define-public crate-dodgy-1 (crate (name "dodgy") (vers "1.1.1") (deps (list (crate-dep (name "glam") (req "^0.24.0") (default-features #t) (kind 0)))) (hash "0v4nlaam6mgzx8k27cd4ydgcdzpdcr7vgxvmac18if4rbla0dlm9") (yanked #t)))

(define-public crate-dodgy-1 (crate (name "dodgy") (vers "1.2.0") (deps (list (crate-dep (name "glam") (req "^0.24.0") (default-features #t) (kind 0)))) (hash "1pj0ab3cfzbr2whzc9a2cx0xvcvbhmy7zl8a18siwlfkrfzwmapk") (yanked #t)))

(define-public crate-dodgy-1 (crate (name "dodgy") (vers "1.2.1") (deps (list (crate-dep (name "glam") (req "^0.24.0") (default-features #t) (kind 0)))) (hash "01pss58pqsng4q9zla65zmcdik3rj538ycqv5qrhw6f15rsqr5ds") (yanked #t)))

(define-public crate-dodgy-1 (crate (name "dodgy") (vers "1.2.2") (deps (list (crate-dep (name "glam") (req "^0.24.0") (default-features #t) (kind 0)))) (hash "0z86wlxvaxiv0zlnbglb9ra88qv1mwlbr97l7d3cn4bx7v5gakb1") (yanked #t)))

(define-public crate-dodgy-1 (crate (name "dodgy") (vers "1.3.0") (deps (list (crate-dep (name "glam") (req "^0.25.0") (default-features #t) (kind 0)))) (hash "1jal1r7jacnvs9a8mql472vc9hyhzbqv4n218jfzplh8hay8v2ds") (yanked #t)))

(define-public crate-dodgy-0.3 (crate (name "dodgy") (vers "0.3.0") (deps (list (crate-dep (name "glam") (req "^0.25.0") (default-features #t) (kind 0)))) (hash "197fqavxvwv45sra18z90svlawggf2zjjk4ymi132719y1anp77d")))

(define-public crate-dodgy_2d-0.4 (crate (name "dodgy_2d") (vers "0.4.0") (deps (list (crate-dep (name "glam") (req "^0.25.0") (default-features #t) (kind 0)))) (hash "1wdc9mps589is9rsmq38mfn07r3lnyp8adxql4pp1nqbmxxik3m6")))

(define-public crate-dodgy_3d-0.4 (crate (name "dodgy_3d") (vers "0.4.0") (deps (list (crate-dep (name "glam") (req "^0.25.0") (default-features #t) (kind 0)))) (hash "10gf5nv4n92ly0cl2ksichwvdc9jw9nkp7hskig66np1y88mjnnx")))

