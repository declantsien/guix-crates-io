(define-module (crates-io do li) #:use-module (crates-io))

(define-public crate-dolibarr-0.1 (crate (name "dolibarr") (vers "0.1.0") (deps (list (crate-dep (name "reqwest") (req "^0.11") (features (quote ("json" "blocking" "cookies"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)))) (hash "1q471gs4hsfqrkfiwhxskw1535szq27353yk836cwi98fxnakacs")))

