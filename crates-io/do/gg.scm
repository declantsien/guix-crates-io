(define-module (crates-io do gg) #:use-module (crates-io))

(define-public crate-dogg-0.1 (crate (name "dogg") (vers "0.1.0") (deps (list (crate-dep (name "chrono") (req "^0.4.26") (default-features #t) (kind 0)) (crate-dep (name "csv") (req "^1.2.2") (default-features #t) (kind 0)) (crate-dep (name "rustyline") (req "^11.0.0") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.97") (default-features #t) (kind 0)) (crate-dep (name "strsim") (req "^0.10.0") (default-features #t) (kind 0)))) (hash "0cgd95i9hgv4bj7217mp2w6qhlvzfhndqxn7jjaigpzfj444009a")))

(define-public crate-dogg-0.1 (crate (name "dogg") (vers "0.1.1") (deps (list (crate-dep (name "chrono") (req "^0.4.26") (default-features #t) (kind 0)) (crate-dep (name "csv") (req "^1.2.2") (default-features #t) (kind 0)) (crate-dep (name "rustyline") (req "^11.0.0") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.97") (default-features #t) (kind 0)) (crate-dep (name "strsim") (req "^0.10.0") (default-features #t) (kind 0)))) (hash "0v30zzcwsxpihxyh4rkly8r6fnpgy7b3glf6spda3l9s1yc2l80s")))

(define-public crate-dogg-0.1 (crate (name "dogg") (vers "0.1.2") (deps (list (crate-dep (name "chrono") (req "^0.4.26") (default-features #t) (kind 0)) (crate-dep (name "csv") (req "^1.2.2") (default-features #t) (kind 0)) (crate-dep (name "rustyline") (req "^11.0.0") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.97") (default-features #t) (kind 0)) (crate-dep (name "strsim") (req "^0.10.0") (default-features #t) (kind 0)))) (hash "1ifa5ymr5lw5vf0yz80vxkxvb6jdrqkdinlvkzpnsk2fxdyp57zh")))

(define-public crate-dogg-0.1 (crate (name "dogg") (vers "0.1.3") (deps (list (crate-dep (name "chrono") (req "^0.4.26") (default-features #t) (kind 0)) (crate-dep (name "csv") (req "^1.2.2") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.97") (default-features #t) (kind 0)) (crate-dep (name "strsim") (req "^0.10.0") (default-features #t) (kind 0)))) (hash "13dw5qs7rd62945jigvsz8yif9z5y0g04q88aww7q44ybqz1d5gb")))

(define-public crate-dogged-0.1 (crate (name "dogged") (vers "0.1.0") (hash "1zx6nc1jwd9fsa9ld41cicr7j8r9c7f8l7aiw1w18z76xkby8l26")))

(define-public crate-dogged-0.2 (crate (name "dogged") (vers "0.2.0") (deps (list (crate-dep (name "rand") (req "^0.3.14") (default-features #t) (kind 2)))) (hash "0yk5l6qqidl5y935x15gi9kkd6niim1wb64r1l7kdzl9jw8dyf16")))

(define-public crate-doggo-0.1 (crate (name "doggo") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^2.20.4") (features (quote ("yaml"))) (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.3.0") (default-features #t) (kind 0)))) (hash "0n9hfpasxrvilb1mpm2qjkllwrv1642wjnrsw9h5fgsk1xlsaqcs")))

(define-public crate-doggo-0.1 (crate (name "doggo") (vers "0.1.1") (deps (list (crate-dep (name "chrono") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^2.20.4") (features (quote ("yaml"))) (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.3.0") (default-features #t) (kind 0)))) (hash "0ivq5yrz4mbncayb6iqqnyhns8i1qqykc65cyzin5zr49f99y5v3")))

(define-public crate-doggo-0.1 (crate (name "doggo") (vers "0.1.2") (deps (list (crate-dep (name "chrono") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^2.20.4") (features (quote ("yaml"))) (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.3.0") (default-features #t) (kind 0)))) (hash "0d1q6bmqnwj3lzrch3w4i4ih7i713rf2wyz7z3r5lkrnvhs5rwfy")))

(define-public crate-doggo-0.1 (crate (name "doggo") (vers "0.1.3") (deps (list (crate-dep (name "chrono") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^2.20.4") (features (quote ("yaml"))) (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.3.0") (default-features #t) (kind 0)))) (hash "15rgskh6i0rakff0w4qyl2n0fp7mcgyfc8m7b0w6ahzp2zvjaq5z")))

(define-public crate-doggo-0.1 (crate (name "doggo") (vers "0.1.4") (deps (list (crate-dep (name "chrono") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^2.20.4") (features (quote ("yaml"))) (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.3.0") (default-features #t) (kind 0)))) (hash "1qgq1mbcnkxa35l3h8m09qhxcpc0689dcndxdbg82hymbsj1n5y1")))

(define-public crate-doggo-0.1 (crate (name "doggo") (vers "0.1.5") (deps (list (crate-dep (name "chrono") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^2.20.4") (features (quote ("yaml"))) (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.3.0") (default-features #t) (kind 0)))) (hash "0i62vs8gzb9zl8xcz0c90bxsm4vhqi0yg8z1qfarp386isgndcli")))

(define-public crate-doggo-0.1 (crate (name "doggo") (vers "0.1.6") (deps (list (crate-dep (name "chrono") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^2.20.4") (features (quote ("yaml"))) (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.3.0") (default-features #t) (kind 0)))) (hash "1f89450rwwpsjcja7vzlx5r5ak5yhf6kaa1my8naizznpdbrlqyl")))

(define-public crate-doggo-0.1 (crate (name "doggo") (vers "0.1.7") (deps (list (crate-dep (name "chrono") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^2.20.4") (features (quote ("yaml"))) (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.3.0") (default-features #t) (kind 0)))) (hash "07lrpv7vg89jn4dzvy0shccsk93riyj1mapfh6svhxzw9v95ra49")))

(define-public crate-doggo-0.1 (crate (name "doggo") (vers "0.1.8") (deps (list (crate-dep (name "chrono") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^2.20.4") (features (quote ("yaml"))) (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.3.0") (default-features #t) (kind 0)))) (hash "1iap5b9i2cvnv64gb4a3v62qxjzpm6fakh3aa0ic7dy5aqzjmkj6")))

(define-public crate-doggo-0.1 (crate (name "doggo") (vers "0.1.10") (deps (list (crate-dep (name "chrono") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^2.20.4") (features (quote ("yaml"))) (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.3.0") (default-features #t) (kind 0)))) (hash "05vys4h9n2avmfzxz4sjb0mcd16scsz1wp32wr0cqrp3lh1ms4zl")))

(define-public crate-doggo-0.1 (crate (name "doggo") (vers "0.1.11") (deps (list (crate-dep (name "chrono") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^2.20") (features (quote ("yaml"))) (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.3") (default-features #t) (kind 0)))) (hash "13r7dsywwr1wd7p6bvg4i1b9bxcmz4yjs6hcb4y2y2yawy1fxrri")))

(define-public crate-doggo-0.1 (crate (name "doggo") (vers "0.1.13") (deps (list (crate-dep (name "chrono") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^2.20") (features (quote ("yaml"))) (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.3") (default-features #t) (kind 0)))) (hash "1rj74n2h6b637viqrp83jvpw8jxhlylx2zgm6m01f41fm1rn0rl0")))

(define-public crate-doggo-0.1 (crate (name "doggo") (vers "0.1.12-pre") (deps (list (crate-dep (name "chrono") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^2.31") (features (quote ("yaml"))) (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.5") (default-features #t) (kind 0)))) (hash "0jg3rrx4628chkfghi4sy2hw3w398bjm0bw3cavx9r2cwrnxa5hr")))

(define-public crate-doggo-0.1 (crate (name "doggo") (vers "0.1.14") (deps (list (crate-dep (name "chrono") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^2.31") (features (quote ("yaml"))) (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.5") (default-features #t) (kind 0)))) (hash "13xpbas7gv4fbn6j97wsvgswhrv0lw5z77dzvdw5g5icm9b09cq5")))

(define-public crate-doggo-0.1 (crate (name "doggo") (vers "0.1.16") (deps (list (crate-dep (name "chrono") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^2.33") (features (quote ("yaml"))) (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.7") (default-features #t) (kind 0)))) (hash "0970h6kakh750dg9r66fb3lmkcchki95xjrn2zy7nlw86npk7d0q")))

