(define-module (crates-io do _u) #:use-module (crates-io))

(define-public crate-do_username-0.1 (crate (name "do_username") (vers "0.1.0") (deps (list (crate-dep (name "rand") (req "^0.7.3") (default-features #t) (kind 0)))) (hash "14barcsv0lg5lf4mpysqpc6prcz8b08gwmjfwsncsxzmp6m2dfm9")))

(define-public crate-do_username-1 (crate (name "do_username") (vers "1.0.0") (deps (list (crate-dep (name "lazy_static") (req "^1.0.1") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.7.3") (default-features #t) (kind 0)))) (hash "08vncmls8sghnrkazwbspc9cqjdvxwwlb5npsar1w8wa38z9a2hp")))

(define-public crate-do_util-0.1 (crate (name "do_util") (vers "0.1.0") (hash "1k1craii62ilcdf6d3klyczgy6pq240wxgk4zvb6v3lc6kqfx55h")))

