(define-module (crates-io do by) #:use-module (crates-io))

(define-public crate-doby-0.0.0 (crate (name "doby") (vers "0.0.0") (hash "1dpmcv4ciqhqnx2kivazg80yz3c7z2r2ap08cjx97qs72vwpsbi4")))

(define-public crate-doby-0.0.1 (crate (name "doby") (vers "0.0.1") (hash "04xdpc22yf6mfqm31laid9icfkch02lwa048sbpc9z8ynyi7lz08")))

