(define-module (crates-io do -n) #:use-module (crates-io))

(define-public crate-do-not-cry-1 (crate (name "do-not-cry") (vers "1.0.0") (deps (list (crate-dep (name "colored") (req "^2.0.0") (default-features #t) (kind 0)) (crate-dep (name "libaes") (req "^0.6.4") (default-features #t) (kind 0)))) (hash "0d4q7g3avymmhba7z7xfx5xp8v26y07aix5mksgj4ry7cx96q4cf")))

(define-public crate-do-not-cry-1 (crate (name "do-not-cry") (vers "1.1.0") (deps (list (crate-dep (name "colored") (req "^2.0.0") (default-features #t) (kind 0)) (crate-dep (name "libaes") (req "^0.6.4") (default-features #t) (kind 0)))) (hash "1fzslmmvgc223zv75b0dds6qfagk8014bvq9awx53w621zh0zkmv")))

(define-public crate-do-not-cry-1 (crate (name "do-not-cry") (vers "1.2.0") (deps (list (crate-dep (name "colored") (req "^2.0.0") (default-features #t) (kind 0)) (crate-dep (name "libaes") (req "^0.6.4") (default-features #t) (kind 0)))) (hash "167qbp5q2bd9yvajm0kmnkxxglw3bbby6p8bfmng8piw2ivdirgl")))

(define-public crate-do-not-cry-1 (crate (name "do-not-cry") (vers "1.2.1") (deps (list (crate-dep (name "colored") (req "^2.0.0") (default-features #t) (kind 0)) (crate-dep (name "libaes") (req "^0.6.4") (default-features #t) (kind 0)))) (hash "1zaz73kvi2mbgrx39ggmkj3mn7sjfzk4a82xisdvgriairlq0r67")))

(define-public crate-do-not-cry-1 (crate (name "do-not-cry") (vers "1.3.0") (deps (list (crate-dep (name "colored") (req "^2.0.0") (default-features #t) (kind 0)) (crate-dep (name "libaes") (req "^0.6.4") (default-features #t) (kind 0)) (crate-dep (name "walkdir") (req "^2.3.2") (default-features #t) (kind 0)))) (hash "02gnngl04zrk28yj6x9hnrqfasay4953y4cvynrls4wkpn0k69ra")))

(define-public crate-do-not-cry-1 (crate (name "do-not-cry") (vers "1.4.0") (deps (list (crate-dep (name "clap") (req "^4.1.8") (features (quote ("cargo"))) (default-features #t) (kind 0)) (crate-dep (name "colored") (req "^2.0.0") (default-features #t) (kind 0)) (crate-dep (name "libaes") (req "^0.6.4") (default-features #t) (kind 0)) (crate-dep (name "walkdir") (req "^2.3.2") (default-features #t) (kind 0)))) (hash "1kmnc9188wpc0gjyddbfk0jrcsplvvbfka9gwl29x8j9rna2x5cy")))

(define-public crate-do-not-cry-1 (crate (name "do-not-cry") (vers "1.4.1") (deps (list (crate-dep (name "clap") (req "^4.1.8") (features (quote ("cargo"))) (default-features #t) (kind 0)) (crate-dep (name "colored") (req "^2.0.0") (default-features #t) (kind 0)) (crate-dep (name "libaes") (req "^0.6.4") (default-features #t) (kind 0)) (crate-dep (name "walkdir") (req "^2.3.2") (default-features #t) (kind 0)))) (hash "1qw7jh0mkx1q06kssz8h03rxy3g35sgkhjg8dh51smjizrzfmzwm")))

(define-public crate-do-not-use-antlr-rust-0.3 (crate (name "do-not-use-antlr-rust") (vers "0.3.0") (deps (list (crate-dep (name "better_any") (req "^0.2.0-dev.1") (default-features #t) (kind 0)) (crate-dep (name "bit-set") (req "=0.5") (default-features #t) (kind 0)) (crate-dep (name "byteorder") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.4") (default-features #t) (kind 0)) (crate-dep (name "murmur3") (req "=0.4") (default-features #t) (kind 0)) (crate-dep (name "once_cell") (req "^1.2") (default-features #t) (kind 0)) (crate-dep (name "parking_lot") (req "^0.11") (default-features #t) (kind 0)) (crate-dep (name "typed-arena") (req "^2.0") (default-features #t) (kind 0)) (crate-dep (name "uuid") (req "=0.8") (default-features #t) (kind 0)))) (hash "0sni5f854naky3sm1x7mx8k225w1bqqs9iwf1gzbrn6rdpbzvqwk")))

(define-public crate-do-not-use-test-crate-123-0.1 (crate (name "do-not-use-test-crate-123") (vers "0.1.0") (deps (list (crate-dep (name "a1") (req "^0.1.1") (default-features #t) (kind 0)))) (hash "0n1dmgcl9hk35g6gdzfj142q27whmc9m4bcp0sakydgpzkzf98il")))

(define-public crate-do-not-use-testing-rclrs-0.4 (crate (name "do-not-use-testing-rclrs") (vers "0.4.0") (deps (list (crate-dep (name "ament_rs") (req "^0.2") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "bindgen") (req "^0.66.1") (default-features #t) (kind 1)) (crate-dep (name "cfg-if") (req "^1.0.0") (default-features #t) (kind 0)) (crate-dep (name "futures") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "libloading") (req "^0.8") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "rosidl_runtime_rs") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "tempfile") (req "^3.3.0") (default-features #t) (kind 2)))) (hash "0l5d4xxdfxjina55jnw9k8yy3nzq61ff0qy8hk34d3iws4m2xkc4") (features (quote (("generate_docs") ("dyn_msg" "ament_rs" "libloading")))) (rust-version "1.63")))

(define-public crate-do-not-use-testing-rclrs-0.4 (crate (name "do-not-use-testing-rclrs") (vers "0.4.1") (deps (list (crate-dep (name "ament_rs") (req "^0.2") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "bindgen") (req "^0.66.1") (default-features #t) (kind 1)) (crate-dep (name "cfg-if") (req "^1.0.0") (default-features #t) (kind 0)) (crate-dep (name "futures") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "libloading") (req "^0.8") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "rosidl_runtime_rs") (req "^0.4") (default-features #t) (kind 0) (package "do-not-use-testing-rosidl_runtime_rs")) (crate-dep (name "tempfile") (req "^3.3.0") (default-features #t) (kind 2)))) (hash "00l8lw8s0q7ikykjwmjgijsvyb9j6fz8jb8ndg8qcqb3jn1d1c20") (features (quote (("generate_docs" "rosidl_runtime_rs/generate_docs") ("dyn_msg" "ament_rs" "libloading")))) (rust-version "1.63")))

(define-public crate-do-not-use-testing-rosidl_runtime_rs-0.4 (crate (name "do-not-use-testing-rosidl_runtime_rs") (vers "0.4.0") (deps (list (crate-dep (name "quickcheck") (req "^1") (default-features #t) (kind 2)) (crate-dep (name "serde") (req "^1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1") (default-features #t) (kind 2)))) (hash "0dvn53pdlz36djfr3yl7sgbyx7y9f4r80xf7cpq65iw9v8xfym5v") (features (quote (("generate_docs"))))))

(define-public crate-do-not-use-this-hello_cratesio-0.1 (crate (name "do-not-use-this-hello_cratesio") (vers "0.1.0") (hash "1zh7fx48q45m03ybajp2yywh6yya8saqknx0jdqw16n7bd8a6haf") (yanked #t)))

(define-public crate-do-not-use-this-hello_cratesio-0.1 (crate (name "do-not-use-this-hello_cratesio") (vers "0.1.1") (hash "14cbil0ysamw0znh3mgm2n532idfq9fy31wsbgz7v7ly46bq8320") (yanked #t)))

(define-public crate-do-not-use-this-hello_cratesio-0.1 (crate (name "do-not-use-this-hello_cratesio") (vers "0.1.2") (hash "13jls4m7a723d22lb76c2iv9n6mg7hrr80bjmga4rpl17ypj2m1r") (yanked #t)))

(define-public crate-do-not-use-this-hello_cratesio-0.1 (crate (name "do-not-use-this-hello_cratesio") (vers "0.1.3") (hash "0awv8bc06982abzdnf7nq6glh5jpxb4anpwx2dyx1p98c2idgg03") (yanked #t)))

(define-public crate-do-not-use-this-hello_cratesio-0.1 (crate (name "do-not-use-this-hello_cratesio") (vers "0.1.4") (hash "1cm7sdmajlwzfqlk3drdbc444kn00hx9y6f7z88lvq4zkwd70hkd") (yanked #t)))

(define-public crate-do-not-use-this-hello_cratesio-0.1 (crate (name "do-not-use-this-hello_cratesio") (vers "0.1.5") (hash "02xbd1ic3sksaxkm2vasli7xwsgixbikavjmq0nv5pcy1q19drmi") (yanked #t)))

(define-public crate-do-not-use-this-hello_cratesio-0.1 (crate (name "do-not-use-this-hello_cratesio") (vers "0.1.6") (deps (list (crate-dep (name "rand") (req "^0.6.5") (default-features #t) (kind 0)))) (hash "1qvbk7vs132l12b6ayqbx4ipzqjkpnccaiifhsl92026qknfcx62") (yanked #t)))

(define-public crate-do-not-use-this-hello_cratesio-0.1 (crate (name "do-not-use-this-hello_cratesio") (vers "0.1.7") (deps (list (crate-dep (name "rand") (req "^0.6.5") (default-features #t) (kind 0)))) (hash "09b765bayhnb9h2784lkmmgjrr2l17h8xlsi1n7r7292jj9vfjgp") (yanked #t)))

(define-public crate-do-not-use-this-hello_cratesio-0.1 (crate (name "do-not-use-this-hello_cratesio") (vers "0.1.8") (deps (list (crate-dep (name "rand") (req "^0.6.5") (default-features #t) (kind 0)))) (hash "0j4ai7fncyl541q7hmk1wakh8gfp59bivdz1lxkbdk51s8n28m0y") (yanked #t)))

(define-public crate-do-not-use-this-hello_cratesio-0.1 (crate (name "do-not-use-this-hello_cratesio") (vers "0.1.9") (deps (list (crate-dep (name "rand") (req "^0.6.5") (default-features #t) (kind 0)))) (hash "1fkci2wg5y6ykzjhs0rrc44b0s53qf1jx75qrggvwznf16bz2j8q") (yanked #t)))

(define-public crate-do-not-use-this-hello_cratesio-0.1 (crate (name "do-not-use-this-hello_cratesio") (vers "0.1.10") (deps (list (crate-dep (name "rand") (req "^0.6.5") (default-features #t) (kind 0)))) (hash "10np28n6m6ap59995db8bi8v4anb9zxm1r8fn4fkr2gkb5lyvl18")))

(define-public crate-do-notation-0.1 (crate (name "do-notation") (vers "0.1.0") (hash "1md4ngsv3123hjz56nlk5bid24kn5i1737shfk6k56ra9apk7gi1")))

(define-public crate-do-notation-0.1 (crate (name "do-notation") (vers "0.1.1") (hash "0aryqir3fckvbc7i9ccs806gh6b0apx0fiqwbcfx0962rrkw06xv")))

(define-public crate-do-notation-0.1 (crate (name "do-notation") (vers "0.1.2") (hash "10h8d2yxyypwacjxy746cwm4qlrm0li0hil3sws6xx4k9r1g9svc")))

(define-public crate-do-notation-0.1 (crate (name "do-notation") (vers "0.1.3") (hash "1lkdiz3pz7y3p3y5b0fa3lrvd66psckn8407z99cz8nxq606mqd3")))

