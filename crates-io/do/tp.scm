(define-module (crates-io do tp) #:use-module (crates-io))

(define-public crate-dotplot-0.1 (crate (name "dotplot") (vers "0.1.0") (deps (list (crate-dep (name "docopt") (req "^0.6.78") (default-features #t) (kind 0)))) (hash "1yhfzbzdk9nppjvzfab8h5ymzrna3l123knwfc9116x7q627jc7q")))

(define-public crate-dotproperties-0.1 (crate (name "dotproperties") (vers "0.1.0") (deps (list (crate-dep (name "nom") (req "^3.1.0") (default-features #t) (kind 0)))) (hash "1dqgsqbsbk6h5ncvf5kjgrzg4bbrjyh6pqvs5qv605rgr3gfrssr")))

