(define-module (crates-io do ny) #:use-module (crates-io))

(define-public crate-donyeh-0.1 (crate (name "donyeh") (vers "0.1.0") (deps (list (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)))) (hash "0hqf2khsn4qk6vfwv7rwssi480max8q8fscsv7i8kpf9wk03jrrk")))

(define-public crate-donyeh-0.1 (crate (name "donyeh") (vers "0.1.1") (deps (list (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)))) (hash "19971c88npgacvrlgwss6mharspgwy0qi7zkvjyzx7g2j7b1n63y")))

