(define-module (crates-io do me) #:use-module (crates-io))

(define-public crate-dome-0.0.1 (crate (name "dome") (vers "0.0.1") (hash "153jpbzfckmvavwfs3r54qsf4lfphgm7ain547lyy110jvsx24q9") (yanked #t)))

(define-public crate-dome_cloomnik-0.1 (crate (name "dome_cloomnik") (vers "0.1.0") (deps (list (crate-dep (name "atoi") (req "^0.4") (default-features #t) (kind 2)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "025nqp7a16jvx4qzbg42amya6jfqflza0vnkpxcf8g0grdqlyspy")))

(define-public crate-dome_cloomnik-0.1 (crate (name "dome_cloomnik") (vers "0.1.1") (deps (list (crate-dep (name "atoi") (req "^0.4") (default-features #t) (kind 2)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "0psm2n0q9848rd1gjng72209jr9lpbvwyk086way9li5dcmmkvfi")))

(define-public crate-dome_cloomnik-0.1 (crate (name "dome_cloomnik") (vers "0.1.2") (deps (list (crate-dep (name "atoi") (req "^0.4") (default-features #t) (kind 2)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "0g6d6ffg1m4fs1i2w0f2348a5srv7qayhr3m5ci8i76sn3s6shym")))

(define-public crate-dome_cloomnik-0.1 (crate (name "dome_cloomnik") (vers "0.1.3") (deps (list (crate-dep (name "atoi") (req "^0.4") (default-features #t) (kind 2)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "0h6j6wvfm6fxw359kzwphink926iqn5r6hnxfrqli4fbrk070byc")))

(define-public crate-dome_cloomnik-0.1 (crate (name "dome_cloomnik") (vers "0.1.4") (deps (list (crate-dep (name "atoi") (req "^0.4") (default-features #t) (kind 2)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "0g46pfiz6qa92flplpnl18v1jl329v6fi8bhidwx59giz91xcqjx")))

(define-public crate-dome_cloomnik-0.1 (crate (name "dome_cloomnik") (vers "0.1.5") (deps (list (crate-dep (name "atoi") (req "^0.4") (default-features #t) (kind 2)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "11l011nj54rg2h1zsgb76ghvdqhpzi2944lyhw965f3i577p8r4q")))

(define-public crate-dome_cloomnik-0.1 (crate (name "dome_cloomnik") (vers "0.1.6") (deps (list (crate-dep (name "atoi") (req "^0.4") (default-features #t) (kind 2)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "0m528nsmrkarb16mlilhylb2z2y39d6nl9xxx9nyw1cwhbgkvq4g")))

(define-public crate-dome_cloomnik-0.1 (crate (name "dome_cloomnik") (vers "0.1.7") (deps (list (crate-dep (name "anyhow") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "atoi") (req "^0.4") (default-features #t) (kind 2)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0") (default-features #t) (kind 0)))) (hash "0bcqncgbghmq7amalipyfgsipbhk3dmwx5wk5x0kn9m9dzny3fdw")))

(define-public crate-dome_cloomnik-0.1 (crate (name "dome_cloomnik") (vers "0.1.8") (deps (list (crate-dep (name "anyhow") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "atoi") (req "^0.4") (default-features #t) (kind 2)) (crate-dep (name "backtrace") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0") (default-features #t) (kind 0)))) (hash "137f6rvfi5ywkb9fy8xq8qva5agc4pfp0w2zzy5c27dfd65n3rni")))

(define-public crate-dome_cloomnik-0.1 (crate (name "dome_cloomnik") (vers "0.1.9") (deps (list (crate-dep (name "anyhow") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "atoi") (req "^0.4") (default-features #t) (kind 2)) (crate-dep (name "backtrace") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0") (default-features #t) (kind 0)))) (hash "0yk08cq764hbpbqvd8vlhxkf6xakp911y5nqw1i31fx4aspbrb26")))

(define-public crate-dome_cloomnik-0.1 (crate (name "dome_cloomnik") (vers "0.1.10") (deps (list (crate-dep (name "anyhow") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "atoi") (req "^0.4") (default-features #t) (kind 2)) (crate-dep (name "backtrace") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0") (default-features #t) (kind 0)))) (hash "16hnpssd61k5iikl72j3i3barm8m0praywjn093b1kl8jimwiba5")))

(define-public crate-dome_cloomnik-0.1 (crate (name "dome_cloomnik") (vers "0.1.11") (deps (list (crate-dep (name "anyhow") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "atoi") (req "^0.4") (default-features #t) (kind 2)) (crate-dep (name "backtrace") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0") (default-features #t) (kind 0)))) (hash "14vbq016v44yxxxnpra5wrgiqj6if3p0prjx515cjyw3l15263wc")))

(define-public crate-dome_cloomnik-0.1 (crate (name "dome_cloomnik") (vers "0.1.12") (deps (list (crate-dep (name "anyhow") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "atoi") (req "^0.4") (default-features #t) (kind 2)) (crate-dep (name "backtrace") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0") (default-features #t) (kind 0)))) (hash "07wcd67y03gvjs3nljg3gqadxmci78y3lj5mkc7w8ma3nfay1sm4")))

