(define-module (crates-io do cr) #:use-module (crates-io))

(define-public crate-docrs-0.1 (crate (name "docrs") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^4.5.4") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.10.4") (default-features #t) (kind 0)) (crate-dep (name "webbrowser") (req "^0.8.14") (default-features #t) (kind 0)))) (hash "0sp6556y12ka2dr2ly9fcqlxfdj81prb5n9iwq7h6vh8zvc0cbmr") (rust-version "1.74.0")))

(define-public crate-docrypto-0.1 (crate (name "docrypto") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^2") (default-features #t) (kind 0)) (crate-dep (name "rust-crypto") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "scoped_threadpool") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "time") (req "^0.1") (default-features #t) (kind 0)))) (hash "0vdw4aabmw7arrd3dkfsx0d2gldccv5y0xzivdlm3ng6jcz6wfh8")))

(define-public crate-docrypto-0.1 (crate (name "docrypto") (vers "0.1.1") (deps (list (crate-dep (name "clap") (req "^2") (default-features #t) (kind 0)) (crate-dep (name "rust-crypto") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "scoped_threadpool") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "time") (req "^0.1") (default-features #t) (kind 0)))) (hash "06i2v0d2w4hivmwqh7yx3jsa041g4vibvsz4p59pf7viz0kbn0rg")))

