(define-module (crates-io do cp) #:use-module (crates-io))

(define-public crate-docpars-0.2 (crate (name "docpars") (vers "0.2.0") (deps (list (crate-dep (name "docopt") (req "^1.1.0") (default-features #t) (kind 0)))) (hash "0fa81zjjbqcbp2z8z2ybg8flm6l5nnarrqcz69l52nnirr04ccq2")))

