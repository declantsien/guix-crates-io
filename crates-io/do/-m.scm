(define-module (crates-io do -m) #:use-module (crates-io))

(define-public crate-do-meow-0.1 (crate (name "do-meow") (vers "0.1.0") (deps (list (crate-dep (name "inline_colorization") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "open") (req "^5.0") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.34") (features (quote ("macros" "rt-multi-thread" "time"))) (default-features #t) (kind 0)))) (hash "02nja77039y9kn8i43azbskdbl38gqyxnxwsc2xww3s976kxgpwr")))

(define-public crate-do-meow-0.1 (crate (name "do-meow") (vers "0.1.1") (deps (list (crate-dep (name "inline_colorization") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "open") (req "^5.0") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.34") (features (quote ("macros" "rt-multi-thread" "time"))) (default-features #t) (kind 0)))) (hash "1x7j1gmfvagn5n70ngh349k2y2ki3wdxyn606gf6vllp6jzjdcn0")))

(define-public crate-do-meow-0.2 (crate (name "do-meow") (vers "0.2.0") (deps (list (crate-dep (name "colored") (req "^2.0") (default-features #t) (kind 0)) (crate-dep (name "enable-ansi-support") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "open") (req "^5.0") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.34") (features (quote ("macros" "rt-multi-thread" "time"))) (default-features #t) (kind 0)))) (hash "0a44wv3f999sazxzj8gcq4cvn30k528vy1jjg8cd6l25imzdwxjr")))

