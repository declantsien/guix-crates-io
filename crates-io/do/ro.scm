(define-module (crates-io do ro) #:use-module (crates-io))

(define-public crate-doro-0.1 (crate (name "doro") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^4.2.4") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "colored") (req "^2.0.0") (default-features #t) (kind 0)))) (hash "16kkvw08h3wg2ppmdzdzhn7h7yra3a15li2373065jpzglriwl5x")))

(define-public crate-doro-0.2 (crate (name "doro") (vers "0.2.0") (deps (list (crate-dep (name "clap") (req "^4.2.4") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "colored") (req "^2.0.0") (default-features #t) (kind 0)))) (hash "12k2h4v7ljigclv44xwd9hbz7my5jlrgwlxl9srgmxz8n4r9f9iv")))

(define-public crate-doro-0.2 (crate (name "doro") (vers "0.2.1") (deps (list (crate-dep (name "clap") (req "^4.2.4") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "colored") (req "^2.0.0") (default-features #t) (kind 0)))) (hash "1mgg0pcxbk7rfflp3jhzh4ls0496hpn9lhqvzihv02j7y37qj46d")))

(define-public crate-doroot-0.1 (crate (name "doroot") (vers "0.1.0") (hash "1bh17gly28ipcyhg6064qssmcjyxyklwxrf0pxzsw4nc9fm23gna")))

(define-public crate-dorothy-ssr-0.1 (crate (name "dorothy-ssr") (vers "0.1.0") (deps (list (crate-dep (name "once_cell") (req "^1.17.1") (default-features #t) (kind 0)) (crate-dep (name "paste") (req "^1.0.12") (default-features #t) (kind 0)))) (hash "00wmwk2il2h2y3igzlh6ilj5d6hfs1mqfw68jbh29w45q4fnwwqp")))

(define-public crate-dorothy-ssr-0.1 (crate (name "dorothy-ssr") (vers "0.1.2") (deps (list (crate-dep (name "once_cell") (req "^1.17.1") (default-features #t) (kind 0)) (crate-dep (name "paste") (req "^1.0.12") (default-features #t) (kind 0)))) (hash "1if5f3xynwc1sjvq68nimi7vik3axk4iabcqhss0j204ispdzpn6")))

(define-public crate-dorothy-ssr-0.1 (crate (name "dorothy-ssr") (vers "0.1.3") (deps (list (crate-dep (name "once_cell") (req "^1.17.1") (default-features #t) (kind 0)) (crate-dep (name "paste") (req "^1.0.12") (default-features #t) (kind 0)))) (hash "0n5wxd65xgjzz9sfbx5rm4r65lksa4mqny17g54jdp9lzk0zp1vg")))

(define-public crate-dorothy-ssr-0.1 (crate (name "dorothy-ssr") (vers "0.1.4") (deps (list (crate-dep (name "once_cell") (req "^1.17.1") (default-features #t) (kind 0)) (crate-dep (name "paste") (req "^1.0.12") (default-features #t) (kind 0)))) (hash "0nwfzdgw9nqls8k4hqnj3g0fl1y5kx3zk4ksqj1f6fkb5p3wwr00")))

