(define-module (crates-io do c_) #:use-module (crates-io))

(define-public crate-doc_9-0.1 (crate (name "doc_9") (vers "0.1.0") (hash "09d81h00slp7bmjqifwvkl8f2lxlf7l0vzsm9f757bp3nkg8c84d")))

(define-public crate-doc_9_testing_12345-0.1 (crate (name "doc_9_testing_12345") (vers "0.1.0") (hash "166zz4fhw96i0xb85f58qlk7viysiqqwnni80vnjyjr8gfh05c3f")))

(define-public crate-doc_9_testing_123456789-0.1 (crate (name "doc_9_testing_123456789") (vers "0.1.0") (hash "16jfsfhhzqiyhj9cs00c4z4jnhfx2rn6p46d2x6ax78a21fgak7j")))

(define-public crate-doc_consts-0.1 (crate (name "doc_consts") (vers "0.1.0") (deps (list (crate-dep (name "syn") (req "^2.0") (default-features #t) (kind 0)))) (hash "1mvwbrq9bqj3xi72z2q0qdxrj8bcz4j5wk6gff13apaqiq1v9l1x")))

(define-public crate-doc_consts-0.2 (crate (name "doc_consts") (vers "0.2.0") (deps (list (crate-dep (name "doc_consts_derive") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "0ik4gm9pw0jns2rx91pz2378znyw3528xwqrmlz528x464x9spyc")))

(define-public crate-doc_consts_derive-0.1 (crate (name "doc_consts_derive") (vers "0.1.0") (deps (list (crate-dep (name "syn") (req "^2.0") (default-features #t) (kind 0)))) (hash "0w958b9c6pdjfmdhysmz9abc8cn88na0bi6ib5yjvlyxniz1y6qr")))

(define-public crate-doc_file-0.0.1 (crate (name "doc_file") (vers "0.0.1") (hash "1418dc01mp3p6xn50ljssizm3x0nk5zbskn6hxl2an1sd90yg3fk")))

(define-public crate-doc_file-0.0.2 (crate (name "doc_file") (vers "0.0.2") (hash "10l73s4if1n729rmm7ydz7hxdg1ac8d02fpbqahyr6zm0gpb40im")))

(define-public crate-doc_file-0.1 (crate (name "doc_file") (vers "0.1.0") (hash "11y7wc89d49nflz05m8wijb1786026pwb2505ihnmzrl5icsiwcb")))

(define-public crate-doc_file-0.2 (crate (name "doc_file") (vers "0.2.0") (hash "01nycqs3wivgpc2z421ff344n1rpnnczb4jcnmpxk0cbg0prhd5y")))

(define-public crate-doc_info_dl-0.1 (crate (name "doc_info_dl") (vers "0.1.0") (hash "1iqai3sjf4z6lmpp3gkc1syr2qvch21yz9rrf0lnkk4c0wirjd1r")))

(define-public crate-doc_item-0.1 (crate (name "doc_item") (vers "0.1.0") (deps (list (crate-dep (name "darling") (req "^0.12.2") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.60") (features (quote ("derive" "parsing" "proc-macro"))) (kind 0)) (crate-dep (name "uuid") (req "^0.8.2") (features (quote ("v4"))) (kind 0)))) (hash "027yx0kvq237bh8mkan7rqakbjnisfq0kmd3g2xv7ayj1kl7araw")))

(define-public crate-doc_item-0.1 (crate (name "doc_item") (vers "0.1.1") (deps (list (crate-dep (name "darling") (req "^0.12.2") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.60") (features (quote ("derive" "parsing" "proc-macro"))) (kind 0)) (crate-dep (name "uuid") (req "^0.8.2") (features (quote ("v4"))) (kind 0)))) (hash "063bmc5lp7m4faf21nj3hzv05zqk4pmzq7jk2s7w68b00g5kp1zi")))

(define-public crate-doc_item-0.2 (crate (name "doc_item") (vers "0.2.0") (deps (list (crate-dep (name "darling") (req "^0.12.2") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.68") (features (quote ("derive" "parsing" "proc-macro"))) (kind 0)) (crate-dep (name "uuid") (req "^0.8.2") (features (quote ("v4"))) (kind 0)))) (hash "05p5v64km1x90xg4323znm82pigh8f9mp5h68gam3y4inv3w14ar") (yanked #t)))

(define-public crate-doc_item-0.2 (crate (name "doc_item") (vers "0.2.1") (deps (list (crate-dep (name "darling") (req "^0.12.2") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.68") (features (quote ("derive" "parsing" "proc-macro"))) (kind 0)) (crate-dep (name "uuid") (req "^0.8.2") (features (quote ("v4"))) (kind 0)))) (hash "1223v144gg7gzpyvj9m0b1xgm02plq2q8fg2pfzd0hgn7ba8bvg9") (yanked #t)))

(define-public crate-doc_item-0.2 (crate (name "doc_item") (vers "0.2.2") (deps (list (crate-dep (name "darling") (req "^0.12.2") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.68") (features (quote ("derive" "parsing" "proc-macro"))) (kind 0)) (crate-dep (name "uuid") (req "^0.8.2") (features (quote ("v4"))) (kind 0)))) (hash "0n011rh0w42x9znsphvxq2qvq6ncjgi5yf28m93iyaqycf1j85np")))

(define-public crate-doc_item-0.2 (crate (name "doc_item") (vers "0.2.3") (deps (list (crate-dep (name "darling") (req "^0.12.2") (default-features #t) (kind 0)) (crate-dep (name "once_cell") (req "^1.7.2") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.68") (features (quote ("derive" "parsing" "proc-macro"))) (kind 0)))) (hash "1x4647yfy624wi1ran5j39rfbc4909vv86p3s2dwb059jhls5j34")))

(define-public crate-doc_item-0.2 (crate (name "doc_item") (vers "0.2.4") (deps (list (crate-dep (name "darling") (req "^0.12.2") (default-features #t) (kind 0)) (crate-dep (name "substring") (req "^1.4.5") (default-features #t) (kind 2)) (crate-dep (name "syn") (req "^1.0.68") (features (quote ("derive" "parsing" "proc-macro"))) (kind 0)) (crate-dep (name "thirtyfour_sync") (req "^0.22.2") (default-features #t) (kind 2)) (crate-dep (name "uuid") (req "^0.8.2") (features (quote ("v4"))) (kind 0)))) (hash "17d44c28lc90sw3arb0g9ivn46d1330kldg93ydlcljr8c5h1dfp") (features (quote (("frontend_test"))))))

(define-public crate-doc_item-0.2 (crate (name "doc_item") (vers "0.2.5") (deps (list (crate-dep (name "darling") (req "^0.12.3") (default-features #t) (kind 0)) (crate-dep (name "indoc") (req "^1.0.3") (default-features #t) (kind 2)) (crate-dep (name "rustversion") (req "^1.0.4") (default-features #t) (kind 2)) (crate-dep (name "substring") (req "^1.4.5") (default-features #t) (kind 2)) (crate-dep (name "syn") (req "^1.0.69") (features (quote ("derive" "parsing" "proc-macro"))) (kind 0)) (crate-dep (name "thirtyfour_sync") (req "^0.23.0") (default-features #t) (kind 2)) (crate-dep (name "trybuild") (req "^1.0.42") (default-features #t) (kind 2)) (crate-dep (name "uuid") (req "^0.8.2") (features (quote ("v4"))) (kind 0)))) (hash "0783h7dzd1hxwhqd48zfx0qjdm2kjcqzn1c57g766v2xxawps0x7") (features (quote (("frontend_test"))))))

(define-public crate-doc_item-0.3 (crate (name "doc_item") (vers "0.3.0") (deps (list (crate-dep (name "darling") (req "^0.13.1") (default-features #t) (kind 0)) (crate-dep (name "rustversion") (req "^1.0.6") (default-features #t) (kind 2)) (crate-dep (name "syn") (req "^1.0.89") (features (quote ("derive" "parsing" "proc-macro"))) (kind 0)) (crate-dep (name "thirtyfour_sync") (req "^0.27.1") (default-features #t) (kind 2)) (crate-dep (name "trybuild") (req "^1.0.56") (default-features #t) (kind 2)))) (hash "0xslrqb9mwmzprk96ngnyjiv3qf7zpy65v543vx03jydj1j2yfsq")))

(define-public crate-doc_link-0.1 (crate (name "doc_link") (vers "0.1.0") (deps (list (crate-dep (name "Inflector") (req "^0.11.4") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^2.33.0") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^0.4.27") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^0.6.11") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.92") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0.92") (default-features #t) (kind 0)) (crate-dep (name "serde_yaml") (req "^0.8.9") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^0.15.43") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "1z3bsirhlhjlhshhn3718in3v1fsvqys0s7b6qr5ibq7m025rx81")))

(define-public crate-doc_test_hick-0.1 (crate (name "doc_test_hick") (vers "0.1.0") (hash "18pbwzn41lwxn6vr9kc5638vw8hw2lk9sg8b9cjkrs43vblg54sp")))

