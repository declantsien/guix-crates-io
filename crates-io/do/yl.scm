(define-module (crates-io do yl) #:use-module (crates-io))

(define-public crate-doyle-0.0.1 (crate (name "doyle") (vers "0.0.1") (deps (list (crate-dep (name "colored") (req "^2.0.0") (default-features #t) (kind 0)) (crate-dep (name "rayon") (req "^1.5.1") (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.11.4") (features (quote ("blocking"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)))) (hash "1ri5k2ip44jnf0322jp5ifzg1522xvpjxykjn0212vb4q14gb7py")))

