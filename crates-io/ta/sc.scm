(define-module (crates-io ta sc) #:use-module (crates-io))

(define-public crate-taschenrechner-0.2 (crate (name "taschenrechner") (vers "0.2.2") (hash "0p89wlqz4dl6d6gbplykmhcxvlnqq6h8gm31fr9y3g4ldkvhmx12") (yanked #t)))

(define-public crate-taschenrechner-0.2 (crate (name "taschenrechner") (vers "0.2.3") (hash "1sva09gqz1q994rcj804n1v0hybsp74vf6nvqxhy396ay8r04fhl") (yanked #t)))

(define-public crate-taschenrechner-0.2 (crate (name "taschenrechner") (vers "0.2.4") (hash "17a0j47fa1659ziqs98xdm1r41j5cdshggv6ih22ywsmmh9gldlm") (yanked #t)))

(define-public crate-taschenrechner-0.2 (crate (name "taschenrechner") (vers "0.2.5") (hash "1xk3wxw3cmgvfc5x8hw9i9h596w9zqp2s9pcnzwg8szl56vijagd") (yanked #t)))

