(define-module (crates-io ta i6) #:use-module (crates-io))

(define-public crate-tai64-0.1 (crate (name "tai64") (vers "0.1.0") (deps (list (crate-dep (name "byteorder") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "chrono") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "quickcheck") (req "^0.4") (default-features #t) (kind 2)))) (hash "0w36ggqs3w0n6wbd41s2hdb4fka3lfgl65w0qqfybaskv9gkvz9w")))

(define-public crate-tai64-0.2 (crate (name "tai64") (vers "0.2.0") (deps (list (crate-dep (name "byteorder") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "chrono") (req "^0.4") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "quickcheck") (req "^0.6") (default-features #t) (kind 2)))) (hash "0jzj2h9qla6biqvcmqrbi3vpyl0l1m2xv22pfymqppc0w6dw3ryc") (features (quote (("default"))))))

(define-public crate-tai64-0.2 (crate (name "tai64") (vers "0.2.1") (deps (list (crate-dep (name "byteorder") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "chrono") (req "^0.4") (optional #t) (kind 0)) (crate-dep (name "quickcheck") (req "^0.6") (default-features #t) (kind 2)))) (hash "10b1m28faqnfci9qsswlv6hfgzx6mdf4iq32vln6s39hfwnvv339") (features (quote (("default"))))))

(define-public crate-tai64-0.2 (crate (name "tai64") (vers "0.2.2") (deps (list (crate-dep (name "byteorder") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "chrono") (req "^0.4") (optional #t) (kind 0)) (crate-dep (name "quickcheck") (req "^0.6") (default-features #t) (kind 2)))) (hash "0lkyzywvf1acyg4zn8j9lsj8qh089xaj2winip0zcbifvwbh40ih") (features (quote (("default"))))))

(define-public crate-tai64-0.2 (crate (name "tai64") (vers "0.2.3") (deps (list (crate-dep (name "byteorder") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "chrono") (req "^0.4") (optional #t) (kind 0)) (crate-dep (name "quickcheck") (req "^0.6") (default-features #t) (kind 2)))) (hash "02nzfjb1hv1950f3lr71d9v7nwa6h64ljk14lf2hxngb366nayl6") (features (quote (("default"))))))

(define-public crate-tai64-1 (crate (name "tai64") (vers "1.0.0") (deps (list (crate-dep (name "byteorder") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "chrono") (req "^0.4") (optional #t) (kind 0)) (crate-dep (name "quickcheck") (req "^0.6") (default-features #t) (kind 2)))) (hash "00b9ignld2xwzbz2gzd3j6b5s5iaj55q1nhkh3cmy1cgnf29rixf") (features (quote (("default"))))))

(define-public crate-tai64-2 (crate (name "tai64") (vers "2.0.0") (deps (list (crate-dep (name "chrono") (req "^0.4") (optional #t) (kind 0)) (crate-dep (name "failure") (req "^0.1") (features (quote ("failure_derive"))) (kind 0)) (crate-dep (name "quickcheck") (req "^0.8") (default-features #t) (kind 2)))) (hash "0ic1a2qljdc2x64cknbrfxq8jwwv6k4a4nzn25bxk9w6nk3q9scd") (features (quote (("std") ("default" "std")))) (yanked #t)))

(define-public crate-tai64-2 (crate (name "tai64") (vers "2.0.1") (deps (list (crate-dep (name "chrono") (req "^0.4") (optional #t) (kind 0)) (crate-dep (name "failure") (req "^0.1") (features (quote ("failure_derive"))) (kind 0)) (crate-dep (name "quickcheck") (req "^0.8") (default-features #t) (kind 2)))) (hash "0hh9ddqq8jrx9mlpil6d1xdsnc93nc4acqwhd61bqrrz3fkjli2z") (features (quote (("std") ("default" "std"))))))

(define-public crate-tai64-3 (crate (name "tai64") (vers "3.0.0") (deps (list (crate-dep (name "chrono") (req "^0.4") (optional #t) (kind 0)) (crate-dep (name "quickcheck") (req "^0.8") (default-features #t) (kind 2)))) (hash "01z2m69a08miflrnp9fqd03nffz9i4l8xz9s1pj2yhdib6j2izxz") (features (quote (("std") ("default" "std"))))))

(define-public crate-tai64-3 (crate (name "tai64") (vers "3.1.0") (deps (list (crate-dep (name "chrono") (req "^0.4") (optional #t) (kind 0)) (crate-dep (name "quickcheck") (req "^0.8") (default-features #t) (kind 2)) (crate-dep (name "serde") (req "^1") (optional #t) (kind 0)))) (hash "10kb8lbwspbwrbqayscraz62ywbkgqj36rp81a46i09b7nf2h520") (features (quote (("std") ("default" "std"))))))

(define-public crate-tai64-4 (crate (name "tai64") (vers "4.0.0") (deps (list (crate-dep (name "serde") (req "^1") (optional #t) (kind 0)) (crate-dep (name "zeroize") (req "^1.1") (optional #t) (kind 0)))) (hash "1p597n6rnl6d2s065rinkyb86wr8z3kzayhzdhp17x1521102x7d") (features (quote (("std") ("default" "std")))) (rust-version "1.56")))

