(define-module (crates-io ta be) #:use-module (crates-io))

(define-public crate-tabelog_searcher-0.1 (crate (name "tabelog_searcher") (vers "0.1.0") (deps (list (crate-dep (name "chrono") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "hyper") (req "^0.10") (default-features #t) (kind 0)) (crate-dep (name "hyper-native-tls") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "rustc-serialize") (req "^0.3") (default-features #t) (kind 0)))) (hash "1ykaqhwpapd13bcqib17abgwsjd5hjqsjpsa46pycf21xq03bx4h")))

