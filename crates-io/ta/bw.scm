(define-module (crates-io ta bw) #:use-module (crates-io))

(define-public crate-tabwriter-0.1 (crate (name "tabwriter") (vers "0.1.1") (deps (list (crate-dep (name "docopt") (req "~0.6.8") (default-features #t) (kind 0)))) (hash "17h97rgl6yfpwhv0w9s2qxv9yc0ycjvpva6i366lylfhldiv2mm5")))

(define-public crate-tabwriter-0.1 (crate (name "tabwriter") (vers "0.1.2") (deps (list (crate-dep (name "docopt") (req "~0.6.8") (default-features #t) (kind 0)))) (hash "10y6ki7sj151i1r8xawjmjwpf9yy53fcgfciirds8hq3isdvxb1f")))

(define-public crate-tabwriter-0.1 (crate (name "tabwriter") (vers "0.1.3") (deps (list (crate-dep (name "docopt") (req "~0.6.8") (default-features #t) (kind 0)))) (hash "1p3s05wsmm6ysgci2szas52svi7r7fi5dl85zk9jizqinqj8ai1j")))

(define-public crate-tabwriter-0.1 (crate (name "tabwriter") (vers "0.1.5") (deps (list (crate-dep (name "docopt") (req "*") (default-features #t) (kind 0)))) (hash "0jl1h77pf7aa0yd3q2g416b43z6xpz5dmygw65qjsb5r8r75l96d")))

(define-public crate-tabwriter-0.1 (crate (name "tabwriter") (vers "0.1.6") (deps (list (crate-dep (name "docopt") (req "*") (default-features #t) (kind 0)))) (hash "12pwzii9h2c3g209bs52bj4cjbxn1nmjpngcn6hcybci033hc864")))

(define-public crate-tabwriter-0.1 (crate (name "tabwriter") (vers "0.1.8") (deps (list (crate-dep (name "docopt") (req "*") (default-features #t) (kind 0)))) (hash "0kls1ifx8jmi7pw7kncqgkjf6fs89hp3lkw15i2fz3f4vi1ivx05")))

(define-public crate-tabwriter-0.1 (crate (name "tabwriter") (vers "0.1.9") (deps (list (crate-dep (name "docopt") (req "*") (default-features #t) (kind 0)))) (hash "0cfq5gj8csl5xwy4j6lpcxinjspp71k516b01qls0f3qgq7x22bf")))

(define-public crate-tabwriter-0.1 (crate (name "tabwriter") (vers "0.1.10") (deps (list (crate-dep (name "docopt") (req "*") (default-features #t) (kind 0)))) (hash "1m8iwb441gmisq6z6zmx6n8chmdhqz7rjgmn0b5gyc5q0iy23gkq")))

(define-public crate-tabwriter-0.1 (crate (name "tabwriter") (vers "0.1.11") (deps (list (crate-dep (name "docopt") (req "*") (default-features #t) (kind 0)) (crate-dep (name "rustc-serialize") (req "*") (default-features #t) (kind 0)))) (hash "0fw8i17xmiq96kljc4mvbfbbc4mhr7injv9h3a1j3snz7jd9v4a3")))

(define-public crate-tabwriter-0.1 (crate (name "tabwriter") (vers "0.1.12") (deps (list (crate-dep (name "docopt") (req "*") (default-features #t) (kind 0)) (crate-dep (name "rustc-serialize") (req "*") (default-features #t) (kind 0)))) (hash "0cdfyjddsb9vvyyx0dba3xxzargpgqmmnyaciz8mza0ci1sbj7qb")))

(define-public crate-tabwriter-0.1 (crate (name "tabwriter") (vers "0.1.13") (deps (list (crate-dep (name "docopt") (req "*") (default-features #t) (kind 0)) (crate-dep (name "rustc-serialize") (req "*") (default-features #t) (kind 0)))) (hash "03sw1q2hr3lqhsvxsskz71zs4m0ppd5i65jvldm0il12bva697v5")))

(define-public crate-tabwriter-0.1 (crate (name "tabwriter") (vers "0.1.14") (deps (list (crate-dep (name "docopt") (req "*") (default-features #t) (kind 0)) (crate-dep (name "rustc-serialize") (req "*") (default-features #t) (kind 0)))) (hash "0vx29x3b2w8d42f8gpz2g1s7m7yrshvx8xhlgp485y56ppqfsfgr")))

(define-public crate-tabwriter-0.1 (crate (name "tabwriter") (vers "0.1.15") (deps (list (crate-dep (name "docopt") (req "*") (default-features #t) (kind 0)) (crate-dep (name "rustc-serialize") (req "*") (default-features #t) (kind 0)))) (hash "0f5m9hjby2n80r8c0vi5m883crjgyhbmszdfdrkrwsbfrfb74822")))

(define-public crate-tabwriter-0.1 (crate (name "tabwriter") (vers "0.1.16") (deps (list (crate-dep (name "docopt") (req "*") (default-features #t) (kind 0)) (crate-dep (name "rustc-serialize") (req "*") (default-features #t) (kind 0)))) (hash "1sszl7jlsg8ryl9mji9czcdzsvqr1g3kwny4kh95pz6ncwb3q73h")))

(define-public crate-tabwriter-0.1 (crate (name "tabwriter") (vers "0.1.17") (deps (list (crate-dep (name "docopt") (req "*") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "*") (default-features #t) (kind 0)) (crate-dep (name "rustc-serialize") (req "*") (default-features #t) (kind 0)))) (hash "0w850610ls8a1rgxpw3f8zwbay7g90csmn1gml54w3yi0gj9xlcx")))

(define-public crate-tabwriter-0.1 (crate (name "tabwriter") (vers "0.1.18") (deps (list (crate-dep (name "docopt") (req "*") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "*") (default-features #t) (kind 0)) (crate-dep (name "rustc-serialize") (req "*") (default-features #t) (kind 0)))) (hash "14wsl7hafvb4c0nll90gz1pdx021l2qyss1jzc5kmymscz7ksaiv")))

(define-public crate-tabwriter-0.1 (crate (name "tabwriter") (vers "0.1.19") (deps (list (crate-dep (name "docopt") (req "*") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "*") (default-features #t) (kind 0)) (crate-dep (name "rustc-serialize") (req "*") (default-features #t) (kind 0)))) (hash "05nbgwg2bwyhs5jd84x2snafjhcwyvf9w5dk2hcqv462ppipl43s")))

(define-public crate-tabwriter-0.1 (crate (name "tabwriter") (vers "0.1.20") (deps (list (crate-dep (name "docopt") (req "*") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "*") (default-features #t) (kind 0)) (crate-dep (name "rustc-serialize") (req "*") (default-features #t) (kind 0)))) (hash "0g22crjfrgll288b7ad816yjm5cxd152cnv7779rj30fh49dlvky")))

(define-public crate-tabwriter-0.1 (crate (name "tabwriter") (vers "0.1.21") (deps (list (crate-dep (name "docopt") (req "*") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "*") (default-features #t) (kind 0)) (crate-dep (name "rustc-serialize") (req "*") (default-features #t) (kind 0)))) (hash "16k90fdk25yhs8p1d91glqlqqxqv25gr89ndffsb7isl4r1r0ph0")))

(define-public crate-tabwriter-0.1 (crate (name "tabwriter") (vers "0.1.22") (deps (list (crate-dep (name "docopt") (req "*") (default-features #t) (kind 0)) (crate-dep (name "rustc-serialize") (req "*") (default-features #t) (kind 0)))) (hash "1f8ffrpz8w6i9848qsm4vwm11y1j8sqb4cd4jkgan96wkpxqm75x")))

(define-public crate-tabwriter-0.1 (crate (name "tabwriter") (vers "0.1.23") (deps (list (crate-dep (name "unicode-width") (req "*") (default-features #t) (kind 0)))) (hash "12pvspdfdx1rni8z1vywnfvrfw8qq3vcbxzynsn1nyzbjcy8p7p2")))

(define-public crate-tabwriter-0.1 (crate (name "tabwriter") (vers "0.1.24") (deps (list (crate-dep (name "unicode-width") (req "^0.1") (default-features #t) (kind 0)))) (hash "1ffv3xxshs1ga7cz0w4rsn0nnw53n6znvbhacz9cjkby40jkj9d3")))

(define-public crate-tabwriter-0.1 (crate (name "tabwriter") (vers "0.1.25") (deps (list (crate-dep (name "ansi_term") (req "^0.7") (default-features #t) (kind 2)) (crate-dep (name "lazy_static") (req "^0.1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^0.1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "unicode-width") (req "^0.1") (default-features #t) (kind 0)))) (hash "0fzd4iwfg2ddjwgzz4bnyr1h25jzbf6g03mhyigdb498v9izbnw5") (features (quote (("default") ("ansi_formatting" "regex" "lazy_static"))))))

(define-public crate-tabwriter-1 (crate (name "tabwriter") (vers "1.0.0") (deps (list (crate-dep (name "ansi_term") (req "^0.7") (default-features #t) (kind 2)) (crate-dep (name "lazy_static") (req "^0.2") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^0.1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "unicode-width") (req "^0.1") (default-features #t) (kind 0)))) (hash "0lvi8pkqljdibr4w187dkmsls0x02x729bgkgkh5mqrnw8jp04ih") (features (quote (("default") ("ansi_formatting" "regex" "lazy_static")))) (yanked #t)))

(define-public crate-tabwriter-1 (crate (name "tabwriter") (vers "1.0.1") (deps (list (crate-dep (name "ansi_term") (req "^0.7") (default-features #t) (kind 2)) (crate-dep (name "lazy_static") (req "^0.2") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^0.1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "unicode-width") (req "^0.1") (default-features #t) (kind 0)))) (hash "08wdvp6mgvs44a915pvqk2a0np9adwxzd420g1504rq1za03f95j") (features (quote (("default") ("ansi_formatting" "regex" "lazy_static")))) (yanked #t)))

(define-public crate-tabwriter-1 (crate (name "tabwriter") (vers "1.0.2") (deps (list (crate-dep (name "ansi_term") (req "^0.7") (default-features #t) (kind 2)) (crate-dep (name "lazy_static") (req "^0.2") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^0.1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "unicode-width") (req "^0.1") (default-features #t) (kind 0)))) (hash "0h4k749j4j88wag0md3dzqygz3xfbsbyq6fzzhjfw3s8y9xjgb8y") (features (quote (("default") ("ansi_formatting" "regex" "lazy_static"))))))

(define-public crate-tabwriter-1 (crate (name "tabwriter") (vers "1.0.3") (deps (list (crate-dep (name "ansi_term") (req "^0.7") (default-features #t) (kind 2)) (crate-dep (name "lazy_static") (req "^0.2") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^0.2.1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "unicode-width") (req "^0.1") (default-features #t) (kind 0)))) (hash "0piq2b5svgrpi53x2dijvwi607rdvdnx3gx9r4nyp8n05cb10y1v") (features (quote (("default") ("ansi_formatting" "regex" "lazy_static"))))))

(define-public crate-tabwriter-1 (crate (name "tabwriter") (vers "1.0.4") (deps (list (crate-dep (name "lazy_static") (req "^1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^0.2.1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "unicode-width") (req "^0.1") (default-features #t) (kind 0)))) (hash "1pf4xaw5qrg5qr94jbjd47yg10i1kslb12jnwh9x2w9a3v3rmasn") (features (quote (("default") ("ansi_formatting" "regex" "lazy_static"))))))

(define-public crate-tabwriter-1 (crate (name "tabwriter") (vers "1.1.0") (deps (list (crate-dep (name "lazy_static") (req "^1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "unicode-width") (req "^0.1") (default-features #t) (kind 0)))) (hash "1fsfri1alvmxd66jklj3wiscp7qldql2lwarmm64jlcy2jly6a4i") (features (quote (("default") ("ansi_formatting" "regex" "lazy_static"))))))

(define-public crate-tabwriter-1 (crate (name "tabwriter") (vers "1.2.0") (deps (list (crate-dep (name "lazy_static") (req "^1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.3") (features (quote ("std"))) (optional #t) (kind 0)) (crate-dep (name "unicode-width") (req "^0.1") (default-features #t) (kind 0)))) (hash "13hjd6h19pr3wp2kmr1579cgg7zybrrprv87dh5afdh4199vambn") (features (quote (("default") ("ansi_formatting" "regex" "lazy_static"))))))

(define-public crate-tabwriter-1 (crate (name "tabwriter") (vers "1.2.1") (deps (list (crate-dep (name "lazy_static") (req "^1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.3") (features (quote ("std"))) (optional #t) (kind 0)) (crate-dep (name "unicode-width") (req "^0.1") (default-features #t) (kind 0)))) (hash "048i0mj3b07zlry9m5fl706y5bzdzgrswymqn32drakzk7y5q81n") (features (quote (("default") ("ansi_formatting" "regex" "lazy_static"))))))

(define-public crate-tabwriter-1 (crate (name "tabwriter") (vers "1.3.0") (deps (list (crate-dep (name "unicode-width") (req "^0.1.10") (default-features #t) (kind 0)))) (hash "1wgf6ajrq3ryrvmps2c8k2aizk8lwdmdi5gyjlq1lra1wqz1gq88") (features (quote (("default") ("ansi_formatting"))))))

(define-public crate-tabwriter-1 (crate (name "tabwriter") (vers "1.4.0") (deps (list (crate-dep (name "unicode-width") (req "^0.1.10") (default-features #t) (kind 0)))) (hash "1xp5j7v8jsk92zcmiyh4ya9akhrchjvc595vwcvxrxk49wn2h9x3") (features (quote (("default") ("ansi_formatting"))))))

(define-public crate-tabwriter-bin-0.1 (crate (name "tabwriter-bin") (vers "0.1.1") (deps (list (crate-dep (name "docopt") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "tabwriter") (req "^1") (default-features #t) (kind 0)))) (hash "1sk2rnmdy6lk0jrcgzlf7hnwj77is4694piw3rc214b5b2hwwb53")))

(define-public crate-tabwriter-bin-0.2 (crate (name "tabwriter-bin") (vers "0.2.0") (deps (list (crate-dep (name "docopt") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "tabwriter") (req "^1") (default-features #t) (kind 0)))) (hash "19dx7h7d7mgyhpr4claf1fn68ijn337q3r19b1ymfkfh1d05qdxb")))

(define-public crate-tabwriter-bin-0.2 (crate (name "tabwriter-bin") (vers "0.2.1") (deps (list (crate-dep (name "docopt") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "tabwriter") (req "^1") (default-features #t) (kind 0)))) (hash "0w6g1p66jfjpcghz6yn6wwjbgwbvlm9zar4qf3z4z7xfryybgcdh")))

