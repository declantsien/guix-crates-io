(define-module (crates-io ta ku) #:use-module (crates-io))

(define-public crate-takumi_okamoto-0.1 (crate (name "takumi_okamoto") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^3.0.0-beta.2") (default-features #t) (kind 0)) (crate-dep (name "webbrowser") (req "^0.5.5") (default-features #t) (kind 0)))) (hash "1pdwhdnb92qy2i69xiyz0ms1vccamwrslsm6n42hrcnmg7pm5jn1") (yanked #t)))

(define-public crate-takumi_okamoto-0.2 (crate (name "takumi_okamoto") (vers "0.2.0") (deps (list (crate-dep (name "clap") (req "^3.0.0-beta.2") (default-features #t) (kind 0)) (crate-dep (name "webbrowser") (req "^0.5.5") (default-features #t) (kind 0)))) (hash "0ymwh0d6dpw0cfpanhj1nbi7qgm23csmazwg3i91885q5115jc9b")))

(define-public crate-takumi_okamoto-0.3 (crate (name "takumi_okamoto") (vers "0.3.0") (deps (list (crate-dep (name "clap") (req "^3.2.16") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "webbrowser") (req "^0.7.1") (default-features #t) (kind 0)))) (hash "1wf2m5vkl1dj9jrslx2aspszgllgqpsljnw8jdqcngyrrwzq1s1a")))

(define-public crate-takuzu-0.2 (crate (name "takuzu") (vers "0.2.0") (deps (list (crate-dep (name "libc") (req "^0.1") (default-features #t) (kind 0)))) (hash "1dadj0zs0836layd7lzbn489gq06rwaav2vms85gb9s918fnqk7j")))

(define-public crate-takuzu-0.2 (crate (name "takuzu") (vers "0.2.1") (deps (list (crate-dep (name "libc") (req "^0.1") (default-features #t) (kind 0)))) (hash "0qr6h13ahl6hp6qcjc9dj5bkfvsywk4ppbwbxmcbb11hmrqi3jc8")))

(define-public crate-takuzu-0.2 (crate (name "takuzu") (vers "0.2.2") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "0ygsp1fdyjbjc9x95m16ha9jffwx270dn1d4vgy9jvqk7f28yfzv")))

(define-public crate-takuzu-0.3 (crate (name "takuzu") (vers "0.3.0") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "1p8w9lz4qphpmj6h80sfpcxgamw0wflz9ycwy8hgi5dnxrcfh4ah")))

(define-public crate-takuzu-0.3 (crate (name "takuzu") (vers "0.3.1") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "06fn6s2dzizc7x3lg0wygcyws9h1lrvsmvswvp4ng6sn6vrxsmff")))

(define-public crate-takuzu-0.3 (crate (name "takuzu") (vers "0.3.2") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "0n550wry1ggva9yx1pn5yvj6iyl6fbvqpwqxhf4y764523vjp5kg")))

(define-public crate-takuzu-0.3 (crate (name "takuzu") (vers "0.3.3") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "1lnnidyn406kzdvrkdv0g84cxvlfh71z4ql3pdw5x4b29gyibywf")))

(define-public crate-takuzu-0.4 (crate (name "takuzu") (vers "0.4.0") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "167irlmmgql9hlp21lgw9z0yh407105a24vv0ynjaqnkfhvhzglk")))

(define-public crate-takuzu-0.5 (crate (name "takuzu") (vers "0.5.0") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "0cyvc1fivhbxps60fkjlpxinl315yr2m3cmahqjpf36zangxw9c4")))

(define-public crate-takuzu-0.5 (crate (name "takuzu") (vers "0.5.1") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "1rfdccx89xvlbh604am18vk0n41h8f0s24jhgsxj96ar73aw1d4q")))

(define-public crate-takuzu-0.6 (crate (name "takuzu") (vers "0.6.0") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "05n3d9yvs9mjlhnf3mqblczlysjh5c5fxqmjivyr766xvcqrjzav")))

(define-public crate-takuzu-1 (crate (name "takuzu") (vers "1.0.0") (deps (list (crate-dep (name "anyhow") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.4") (default-features #t) (kind 2)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "06apnrv12xih05b95cd4gxzvppcp9z8m2ki0k77v8fs4z67fgvwf")))

(define-public crate-takuzu-1 (crate (name "takuzu") (vers "1.0.1") (deps (list (crate-dep (name "anyhow") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.4") (default-features #t) (kind 2)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "0s94kq12m3yh2ghvlswcfbiffbyvdsrh0w7wd934sz52h8601q7c")))

(define-public crate-takuzu-1 (crate (name "takuzu") (vers "1.1.0") (deps (list (crate-dep (name "anyhow") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.4") (default-features #t) (kind 2)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "08w0j3r2xhfn4sifg9jcnq0dlgh3n2s8184p87kpzw232m58frgy")))

(define-public crate-takuzu-1 (crate (name "takuzu") (vers "1.1.1") (deps (list (crate-dep (name "anyhow") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.4") (default-features #t) (kind 2)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "17g8xzs0l90yyqmgc1nqiwrlnhzb3nnkyafmxkj9hd39m1nbgh4r")))

