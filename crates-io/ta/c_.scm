(define-module (crates-io ta c_) #:use-module (crates-io))

(define-public crate-tac_cart-0.1 (crate (name "tac_cart") (vers "0.1.0") (deps (list (crate-dep (name "binread") (req "^2.2.0") (default-features #t) (kind 0)) (crate-dep (name "modular-bitfield") (req "^0.11.2") (default-features #t) (kind 0)))) (hash "0g2fh5w3g11447ralcnfhda2g414l0wzj5wji62pnl8cxkrr7l9i")))

(define-public crate-tac_cart-0.2 (crate (name "tac_cart") (vers "0.2.0") (deps (list (crate-dep (name "binread") (req "^2.2.0") (default-features #t) (kind 0)) (crate-dep (name "binwrite") (req "^0.2.1") (default-features #t) (kind 0)) (crate-dep (name "modular-bitfield") (req "^0.11.2") (default-features #t) (kind 0)))) (hash "0fy60115r0lgml3i7sblk378jc0br2nq2mpy6h124qidwlyghhk1")))

