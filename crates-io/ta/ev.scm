(define-module (crates-io ta ev) #:use-module (crates-io))

(define-public crate-taevus-1 (crate (name "taevus") (vers "1.0.0") (deps (list (crate-dep (name "chrono") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "~2.27.0") (default-features #t) (kind 0)))) (hash "0vrh4dv8zvx918llk12zqbsvd2m01g8wfp7li88gb21bxziwampw") (yanked #t)))

