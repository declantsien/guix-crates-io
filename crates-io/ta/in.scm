(define-module (crates-io ta in) #:use-module (crates-io))

(define-public crate-tain-0.1 (crate (name "tain") (vers "0.1.0") (hash "13iavfza3x4w3rm74gjp05xhxgryyd6laf53azgqql1nx6b9bhs1")))

(define-public crate-tain-0.2 (crate (name "tain") (vers "0.2.0") (deps (list (crate-dep (name "fake") (req "^2.9.2") (default-features #t) (kind 0)) (crate-dep (name "testcontainers") (req "^0.15") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.37") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "101mlswv4gcq5xhsghvia9z4gnpxgkllvpf5pwd8n2rkn230sv8q")))

