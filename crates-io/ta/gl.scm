(define-module (crates-io ta gl) #:use-module (crates-io))

(define-public crate-taglib-0.1 (crate (name "taglib") (vers "0.1.0") (deps (list (crate-dep (name "libc") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "pkg-config") (req "^0.3") (optional #t) (default-features #t) (kind 1)))) (hash "0w4ykpkkmnr91wwbw5fx0bdnw3kfc91b211da2sckmv0cp8c2v8x") (features (quote (("use-pkgconfig" "pkg-config") ("default"))))))

(define-public crate-taglib-0.2 (crate (name "taglib") (vers "0.2.0") (deps (list (crate-dep (name "libc") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "taglib-sys") (req "^0.2.0") (default-features #t) (kind 0)))) (hash "041fjwg9h1sqh73khw0nm744kibdlpzfdlq9hxcrz35h244b34lw") (features (quote (("use-pkgconfig" "taglib-sys/use-pkgconfig") ("default"))))))

(define-public crate-taglib-1 (crate (name "taglib") (vers "1.0.0") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "taglib-sys") (req "^1.0.0") (default-features #t) (kind 0)))) (hash "04r0mm35nr9pjjk27c59ysx8bgjbas5ag7j5xpzfyva2gvrmx7xq") (features (quote (("use-pkgconfig" "taglib-sys/use-pkgconfig") ("default"))))))

(define-public crate-taglib-sys-0.2 (crate (name "taglib-sys") (vers "0.2.0") (deps (list (crate-dep (name "libc") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "pkg-config") (req "^0.3") (optional #t) (default-features #t) (kind 1)))) (hash "09lfr58p863ddl6c1b6mvqkvllhnx200d16xzxflrpvvn0ddczvy") (features (quote (("use-pkgconfig" "pkg-config") ("default"))))))

(define-public crate-taglib-sys-1 (crate (name "taglib-sys") (vers "1.0.0") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "pkg-config") (req "^0.3") (optional #t) (default-features #t) (kind 1)))) (hash "0kia1mlg7jvdx3b43fynsvjr58g3x3w4qs8nxb6fiw83lqifl84y") (features (quote (("use-pkgconfig" "pkg-config") ("default")))) (links "tag_c")))

