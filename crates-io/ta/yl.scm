(define-module (crates-io ta yl) #:use-module (crates-io))

(define-public crate-taylor-0.0.1 (crate (name "taylor") (vers "0.0.1") (deps (list (crate-dep (name "math-ast") (req "^0.2.0") (default-features #t) (kind 0)))) (hash "00kjpn718knhif5l0cwf16v21m03sx09wzl72alkv28prvxcgb0x") (features (quote (("default"))))))

