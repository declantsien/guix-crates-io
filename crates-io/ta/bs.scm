(define-module (crates-io ta bs) #:use-module (crates-io))

(define-public crate-tabs-0.1 (crate (name "tabs") (vers "0.1.0") (deps (list (crate-dep (name "futures") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "1cb4d7nmqv29adv3044axp3r8i63x54frmlvr8wgbyb6ng4a3jil")))

(define-public crate-tabs-0.1 (crate (name "tabs") (vers "0.1.1") (deps (list (crate-dep (name "futures") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "1ic263h53klz86qjwyp3vwv4dp43qqyfdj6fprnnav6ixqlqf5gm")))

(define-public crate-tabset-0.1 (crate (name "tabset") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^2.33") (features (quote ("color"))) (default-features #t) (kind 0)) (crate-dep (name "colored") (req "^2.0.0") (default-features #t) (kind 0)) (crate-dep (name "directories") (req "^4.0.1") (default-features #t) (kind 0)))) (hash "0hgaxy1ciksmd4r4jp0hh9d856r7da33inz9pb8pg5g3bq0hza15")))

