(define-module (crates-io ta rc) #:use-module (crates-io))

(define-public crate-tarc-0.1 (crate (name "tarc") (vers "0.1.0") (hash "027v2pvb79mqk5fxrldw4csvi4wrllvzn0zm336s1wp7qraawb6x") (yanked #t)))

(define-public crate-tarc-0.1 (crate (name "tarc") (vers "0.1.1") (hash "1hv5dz5vf41db9bidqaw4mpa3w1wcj3791nyg6clb4l8r9s2cl7c")))

(define-public crate-tarc-0.1 (crate (name "tarc") (vers "0.1.2") (hash "1xc352vr8b02rzk3xaajzr28aagf571c19jbqpj905jms0ha5kdn") (features (quote (("std") ("default" "std"))))))

(define-public crate-tarc-0.1 (crate (name "tarc") (vers "0.1.3") (hash "18libz04i4mgnhazfci200aw6iqsk5xmp6iw4pz4pk43gas2bwc7") (features (quote (("std") ("default" "std"))))))

(define-public crate-tarc-0.1 (crate (name "tarc") (vers "0.1.4") (hash "071fxvs8g0rxsdzw1fclqxhy01g31qqw5q6s5bglg3vi6k6rgnl8") (features (quote (("std") ("default" "std"))))))

(define-public crate-tarch-0.1 (crate (name "tarch") (vers "0.1.0") (hash "1b8jm03xv4ffwpf3sq2n30smv3dci2x7wxgpxv01b3vd1qbbnvkl") (yanked #t)))

