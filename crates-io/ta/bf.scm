(define-module (crates-io ta bf) #:use-module (crates-io))

(define-public crate-tabfile-0.2 (crate (name "tabfile") (vers "0.2.0") (deps (list (crate-dep (name "tempfile") (req "^3") (default-features #t) (kind 2)))) (hash "199ybm0q3qjzbvg1wha2lgcv1z4frdm1i8dshfcgxadjgr64a3zb")))

(define-public crate-tabfile-0.2 (crate (name "tabfile") (vers "0.2.1") (deps (list (crate-dep (name "tempfile") (req "^3") (default-features #t) (kind 2)))) (hash "0dppgwpw3x30a8chv6jqwqlvabr8apqry5gzf7zghq044w0qgy31")))

