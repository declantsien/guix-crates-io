(define-module (crates-io ta bu) #:use-module (crates-io))

(define-public crate-tabu-0.1 (crate (name "tabu") (vers "0.1.0") (deps (list (crate-dep (name "hashbrown") (req "^0.13.1") (default-features #t) (kind 0)) (crate-dep (name "itertools") (req "^0.10.5") (default-features #t) (kind 0)) (crate-dep (name "ordered-float") (req "^3.4.0") (default-features #t) (kind 0)))) (hash "13g1xyykqzrnxkck8v4vhgwvpiv25asxvc661a2imhr0bn3qyp37")))

(define-public crate-tabu-0.1 (crate (name "tabu") (vers "0.1.1") (deps (list (crate-dep (name "hashbrown") (req "^0.13.1") (default-features #t) (kind 0)) (crate-dep (name "itertools") (req "^0.10.5") (default-features #t) (kind 0)) (crate-dep (name "ordered-float") (req "^3.4.0") (default-features #t) (kind 0)))) (hash "1hpsf3vx2l5ma5p4rznw2mz4g09mj1cbw6zqdmfc1r3x2w7syrcg")))

(define-public crate-tabula-0.1 (crate (name "tabula") (vers "0.1.0") (deps (list (crate-dep (name "anyhow") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "jni") (req "^0.19") (features (quote ("invocation"))) (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "1pi0khyz6d41bc0sz3fkplh27bm8z2qrbhsklbbmpmjdf8yvyl9q")))

(define-public crate-tabula-0.1 (crate (name "tabula") (vers "0.1.1") (deps (list (crate-dep (name "anyhow") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "jni") (req "^0.19") (features (quote ("invocation"))) (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "0a7kbswkq1895s8y5bknsmfabxln95nyl8g1gh8b889zp71dxkrb")))

(define-public crate-tabular-0.1 (crate (name "tabular") (vers "0.1.0") (deps (list (crate-dep (name "unicode-width") (req "^0.1.5") (optional #t) (default-features #t) (kind 0)))) (hash "0igyi4mxnfxhvn742p9ljr1la51f4znwc5jd13vw740fsbs1kr89") (features (quote (("default" "unicode-width"))))))

(define-public crate-tabular-0.1 (crate (name "tabular") (vers "0.1.1") (deps (list (crate-dep (name "unicode-width") (req "^0.1.5") (optional #t) (default-features #t) (kind 0)))) (hash "1pmjzspi0m5c6g5z2ar6kcym8hv25czgyq19s64l74vm350xp986") (features (quote (("default" "unicode-width"))))))

(define-public crate-tabular-0.1 (crate (name "tabular") (vers "0.1.2") (deps (list (crate-dep (name "unicode-width") (req "^0.1.5") (optional #t) (default-features #t) (kind 0)))) (hash "02hfch6d214p3rp31rrid1h0l1y6lq8ddrmz79qdscvdjm7h6vgx") (features (quote (("default" "unicode-width"))))))

(define-public crate-tabular-0.1 (crate (name "tabular") (vers "0.1.3") (deps (list (crate-dep (name "unicode-width") (req "^0.1.5") (optional #t) (default-features #t) (kind 0)))) (hash "1wcd3rfivkmmhqmbwsmwclhmc8bkqins8r0kvvgjzf9z0fl1r00f") (features (quote (("default" "unicode-width")))) (yanked #t)))

(define-public crate-tabular-0.1 (crate (name "tabular") (vers "0.1.4") (deps (list (crate-dep (name "unicode-width") (req "^0.1.5") (optional #t) (default-features #t) (kind 0)))) (hash "0fsci58lxkklc1gdd87y08dan7i4kn36nnq6fi5fdvyw0bp5pqz7") (features (quote (("default" "unicode-width"))))))

(define-public crate-tabular-0.2 (crate (name "tabular") (vers "0.2.0") (deps (list (crate-dep (name "colored") (req "^2.0.0") (default-features #t) (kind 2)) (crate-dep (name "strip-ansi-escapes") (req "^0.1.1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "unicode-width") (req "^0.1.5") (optional #t) (default-features #t) (kind 0)))) (hash "1ld3j7zx5ri87wf379n9mhdqgn6wibbfj3gr7nbs3027a4n8i8nr") (features (quote (("default" "unicode-width") ("ansi-cell" "strip-ansi-escapes"))))))

(define-public crate-tabular2-0.1 (crate (name "tabular2") (vers "0.1.0") (deps (list (crate-dep (name "strip-ansi-escapes") (req "^0.1.1") (default-features #t) (kind 0)) (crate-dep (name "unicode-width") (req "^0.1.10") (default-features #t) (kind 0)))) (hash "06h7yqm3fz8d1c06r49lsr3qw0pbd1kvvcyzw3gx3fma664q6qz1")))

(define-public crate-tabular2-1 (crate (name "tabular2") (vers "1.0.0") (deps (list (crate-dep (name "strip-ansi-escapes") (req "^0.1.1") (default-features #t) (kind 0)) (crate-dep (name "unicode-width") (req "^0.1.10") (default-features #t) (kind 0)))) (hash "17qc484iiaawrz7rb0i24lsp5jx0hlsbzgknhd9syb9wkfs64wi3")))

(define-public crate-tabulate-1 (crate (name "tabulate") (vers "1.0.0") (deps (list (crate-dep (name "clap") (req "^2.23.0") (default-features #t) (kind 0)) (crate-dep (name "combine") (req "^3.2.0") (default-features #t) (kind 0)) (crate-dep (name "error-chain") (req "^0.11.0") (default-features #t) (kind 0)))) (hash "157jwji578gia8143rni795k04bagfd0ffxlq90i4vai806xic37")))

(define-public crate-tabulate-1 (crate (name "tabulate") (vers "1.0.1") (deps (list (crate-dep (name "clap") (req "^2.23.0") (features (quote ("wrap_help"))) (default-features #t) (kind 0)) (crate-dep (name "combine") (req "^3.2.0") (default-features #t) (kind 0)) (crate-dep (name "error-chain") (req "^0.11.0") (default-features #t) (kind 0)))) (hash "1ccg6sfxl67jnbhlx0spm5adp93dann9aspqdmnc7h26ghvdr29a")))

(define-public crate-tabulate-1 (crate (name "tabulate") (vers "1.0.2") (deps (list (crate-dep (name "clap") (req "^2.23.0") (features (quote ("wrap_help"))) (default-features #t) (kind 0)) (crate-dep (name "combine") (req "^3.2.0") (default-features #t) (kind 0)) (crate-dep (name "error-chain") (req "^0.11.0") (default-features #t) (kind 0)))) (hash "00kbsc0p6n77448arld6vk0plpniywlx8zdjxzkxv24xzpa36k7j")))

(define-public crate-tabulate-1 (crate (name "tabulate") (vers "1.1.1") (deps (list (crate-dep (name "clap") (req "^2.23.0") (features (quote ("wrap_help"))) (default-features #t) (kind 0)) (crate-dep (name "combine") (req "^4.0.0") (default-features #t) (kind 0)) (crate-dep (name "error-chain") (req "^0.12.0") (default-features #t) (kind 0)))) (hash "17qily3xf3nszib0qasfjj34276r8sw3q103w69qwab0h35qj29f")))

(define-public crate-tabulate-1 (crate (name "tabulate") (vers "1.2.0") (deps (list (crate-dep (name "clap") (req "^4") (features (quote ("wrap_help" "derive" "cargo" "string"))) (default-features #t) (kind 0)) (crate-dep (name "combine") (req "^4.0.0") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0") (default-features #t) (kind 0)))) (hash "0d5cwbdcimrs04hc31z35lh3455ncvad5x6d1k4y1iw54mk5i6bz")))

(define-public crate-tabulate-rs-0.1 (crate (name "tabulate-rs") (vers "0.1.0") (hash "0jsjk207a04mwfnp05xh6q7idnww2da4z1g954s2fjs43m4vv2wd")))

