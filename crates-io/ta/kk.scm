(define-module (crates-io ta kk) #:use-module (crates-io))

(define-public crate-takkerus-0.2 (crate (name "takkerus") (vers "0.2.0") (deps (list (crate-dep (name "getopts") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "time") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "zero_sum") (req "^1.1") (features (quote ("with_tak"))) (default-features #t) (kind 0)))) (hash "0yhdhim7i6y3jj7ic8yqw2akansrwz813rw76x1l55015c50v9hf") (yanked #t)))

(define-public crate-takkerus-0.3 (crate (name "takkerus") (vers "0.3.0") (deps (list (crate-dep (name "getopts") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "time") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "zero_sum") (req "^1.2") (features (quote ("with_tak"))) (default-features #t) (kind 0)))) (hash "194q71zdhiswhpgdwfg5n8njs7r37lj6kw656p8c7in5l8wk5xwd") (yanked #t)))

