(define-module (crates-io ta gi) #:use-module (crates-io))

(define-public crate-taginfo-0.1 (crate (name "taginfo") (vers "0.1.0") (deps (list (crate-dep (name "lazy_static") (req "^1.3.0") (default-features #t) (kind 0)))) (hash "0lbkgk1xkimcy83a2ghbjnrpzyx71wawb9rwrii50gl3sld6q42b")))

