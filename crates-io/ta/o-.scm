(define-module (crates-io ta o-) #:use-module (crates-io))

(define-public crate-tao-core-video-sys-0.2 (crate (name "tao-core-video-sys") (vers "0.2.0") (deps (list (crate-dep (name "cfg-if") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "core-foundation-sys") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "metal") (req "^0.23") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "objc") (req "^0.2") (default-features #t) (kind 0)))) (hash "1xlqdcp7a5hni0jwc7yn7p4mij68f86ffv0cfb8did4w53mm0517") (features (quote (("opengl") ("io_suface") ("display_link" "opengl") ("direct3d") ("default") ("all" "display_link" "direct3d" "io_suface" "opengl"))))))

(define-public crate-tao-log-0.1 (crate (name "tao-log") (vers "0.1.0") (deps (list (crate-dep (name "log") (req ">= 0.4.6, < 0.5") (default-features #t) (kind 0)))) (hash "0ld6iczda0fri4178qd426hszfdfgk0jf5wcn0hnahz9xkz6ph5f")))

(define-public crate-tao-log-0.2 (crate (name "tao-log") (vers "0.2.0") (deps (list (crate-dep (name "log") (req ">= 0.4.6, < 0.5") (default-features #t) (kind 0)) (crate-dep (name "parking_lot") (req ">= 0.7.1, < 0.8") (default-features #t) (kind 2)))) (hash "17ldbzj64a1mcsbj62cw04r76id24g7kqwmm0rlpi3m4szsh70cr") (features (quote (("std" "log/std"))))))

(define-public crate-tao-log-0.2 (crate (name "tao-log") (vers "0.2.1") (deps (list (crate-dep (name "log") (req ">= 0.4.6, < 0.5") (default-features #t) (kind 0)) (crate-dep (name "parking_lot") (req ">= 0.9.0, < 0.10") (default-features #t) (kind 2)))) (hash "1i5frskp3fs58mak2cfd0swq73j0drlc782dkz13fkx8qrp3mid6") (features (quote (("std" "log/std"))))))

(define-public crate-tao-log-1 (crate (name "tao-log") (vers "1.0.0") (deps (list (crate-dep (name "log") (req ">= 0.4.6, < 0.5") (default-features #t) (kind 0)) (crate-dep (name "parking_lot") (req ">= 0.9.0, < 0.10") (default-features #t) (kind 2)))) (hash "1g8chzfr3pd2ywkk6g1rgq03s0ccz3fx642w7g0d5frz2dvnh6x8") (features (quote (("std" "log/std"))))))

(define-public crate-tao-log-1 (crate (name "tao-log") (vers "1.0.1") (deps (list (crate-dep (name "log") (req ">=0.4.6, <0.5") (default-features #t) (kind 0)) (crate-dep (name "parking_lot") (req ">=0.9.0, <0.10") (default-features #t) (kind 2)))) (hash "1l3r0hc61cj41v69ic39h1rz88n1vk6dfznqcdjq4kxmxg3p5nqv") (features (quote (("std" "log/std"))))))

(define-public crate-tao-macros-0.1 (crate (name "tao-macros") (vers "0.1.0") (deps (list (crate-dep (name "proc-macro2") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "02gx598zsgfnpjrmwa3vnl77ahp25bcq6lbir3zkjnnl8n1csvrv") (features (quote (("tray") ("default")))) (rust-version "1.56")))

(define-public crate-tao-macros-0.1 (crate (name "tao-macros") (vers "0.1.1") (deps (list (crate-dep (name "proc-macro2") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "0idlb50wnfgx7cwyjv5fvfbbkgz7qzzvv2a54dc4clpbqnya89rv") (features (quote (("tray") ("default")))) (rust-version "1.56")))

(define-public crate-tao-macros-0.1 (crate (name "tao-macros") (vers "0.1.2") (deps (list (crate-dep (name "proc-macro2") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "1hhqgmr9yw60nwmw8xwxh44w2h2qz18nhvhkkdk8n5axa214a4gc") (features (quote (("tray") ("default")))) (rust-version "1.56")))

(define-public crate-tao-of-rust-0.1 (crate (name "tao-of-rust") (vers "0.1.0") (deps (list (crate-dep (name "csv_challenge") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "failures_crate") (req "^0.1") (default-features #t) (kind 0)))) (hash "1xijhiqwccj4yyny2nn12n12fggxq5f8spvjcr1x5474dlvbzvp0")))

(define-public crate-tao-of-rust-1 (crate (name "tao-of-rust") (vers "1.0.0") (deps (list (crate-dep (name "csv_challenge") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "failures_crate") (req "^0.1") (default-features #t) (kind 0)))) (hash "0yp0n9ljxkzgqfjcv9apvzakv0p8ix305004hici60vv1ajyvxz6")))

(define-public crate-tao-of-rust-1 (crate (name "tao-of-rust") (vers "1.0.1") (deps (list (crate-dep (name "csv_challenge") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "failures_crate") (req "^0.1") (default-features #t) (kind 0)))) (hash "0i83lg5r336w00fvakpvkbvv955wnbsblalbqaz56ys2mjj7z90a")))

(define-public crate-tao-of-rust-1 (crate (name "tao-of-rust") (vers "1.0.2") (deps (list (crate-dep (name "csv_challenge") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "failures_crate") (req "^0.1") (default-features #t) (kind 0)))) (hash "1my8xpnlwvlyfchi2sfr609f3wfcn1qwvssa7ndn34nf73p7imbh")))

