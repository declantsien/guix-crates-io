(define-module (crates-io ta gu) #:use-module (crates-io))

(define-public crate-tagu-0.1 (crate (name "tagu") (vers "0.1.0") (hash "1kyzmjz9xsz8gnj3qbcw1n4ag3p77an8klsn1gkzqzsjc2s2y9mi") (yanked #t)))

(define-public crate-tagu-0.1 (crate (name "tagu") (vers "0.1.1") (hash "1nr433vwdi1n0w637rsrvj1m9f7hk87ylsp4z483wq9z9f5h3jr4") (yanked #t)))

(define-public crate-tagu-0.1 (crate (name "tagu") (vers "0.1.2") (hash "0xcm4fzaxbk885qb77fsa2lv8ry8rvll8mmgnzyqznhqk3nwjm70") (yanked #t)))

(define-public crate-tagu-0.1 (crate (name "tagu") (vers "0.1.3") (hash "02kbi7bysczsaf36294h1414kj6hrm9c1ip8v8bikrhw0147ppv7") (yanked #t)))

(define-public crate-tagu-0.1 (crate (name "tagu") (vers "0.1.4") (hash "1nng1rqv1iybrnzgkfqq663g08fzac247xcyyxmz0qf77zm057i4") (yanked #t)))

(define-public crate-tagu-0.1 (crate (name "tagu") (vers "0.1.5") (hash "02s33gm0wfd4952xi501xwhy4d9l3k11ay50rvlc93bc8lqzy1hz") (yanked #t)))

(define-public crate-tagu-0.1 (crate (name "tagu") (vers "0.1.6") (hash "0sjpvqv29bpnf4lzmydwl2y71s5nw5p6jgfa3z99xfhgs836pnzd")))

(define-public crate-tagua-llvm-0.0.1 (crate (name "tagua-llvm") (vers "0.0.1") (deps (list (crate-dep (name "libc") (req "~0.2.7") (default-features #t) (kind 0)) (crate-dep (name "llvm-sys") (req "~0.3.0") (default-features #t) (kind 0)))) (hash "0p04hkdybvc4jppx1pngi7lysclccxrzbh00b6c4w4mac51jspl2")))

(define-public crate-tagua-llvm-0.1 (crate (name "tagua-llvm") (vers "0.1.0") (deps (list (crate-dep (name "libc") (req "~0.2.7") (default-features #t) (kind 0)) (crate-dep (name "llvm-sys") (req "~0.3.0") (default-features #t) (kind 0)))) (hash "1vrg33px8fwsxgvw52g0bzz4vzg57yx747v7h1xzd015prfzhqsf")))

(define-public crate-tagua-parser-0.1 (crate (name "tagua-parser") (vers "0.1.0") (deps (list (crate-dep (name "lazy_static") (req "~0.1") (default-features #t) (kind 0)) (crate-dep (name "nom") (req "^1.2.3") (features (quote ("regexp" "regexp_macros"))) (default-features #t) (kind 0)) (crate-dep (name "quickcheck") (req "~0.3") (default-features #t) (kind 2)) (crate-dep (name "regex") (req "~0.1") (default-features #t) (kind 0)))) (hash "0plhm6zp2dvc95n8s4wd5l8532vh3ak39dqf0s10yi69hyrqdcfh")))

