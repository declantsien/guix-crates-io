(define-module (crates-io ta ky) #:use-module (crates-io))

(define-public crate-takyon-0.1 (crate (name "takyon") (vers "0.1.0") (deps (list (crate-dep (name "io-uring") (req "^0.6.2") (default-features #t) (target "cfg(target_os = \"linux\")") (kind 0)) (crate-dep (name "libc") (req "^0.2.149") (default-features #t) (target "cfg(target_os = \"linux\")") (kind 0)) (crate-dep (name "nohash") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.49") (default-features #t) (kind 0)))) (hash "0ggm8lk823pbsadisg2fsgn7bvqiqkrhnl6zvxwpj1m3g3zpr0hz")))

(define-public crate-takyon-0.2 (crate (name "takyon") (vers "0.2.0") (deps (list (crate-dep (name "io-uring") (req "^0.6.2") (default-features #t) (target "cfg(target_os = \"linux\")") (kind 0)) (crate-dep (name "libc") (req "^0.2.149") (default-features #t) (target "cfg(target_os = \"linux\")") (kind 0)) (crate-dep (name "nohash") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "pin-project-lite") (req "^0.2.13") (default-features #t) (kind 0)))) (hash "1vsph6g4s9wbs91ak1czg7q9svgabbfd354w86m4sq5h6wccadf3")))

