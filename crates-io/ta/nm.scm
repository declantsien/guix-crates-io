(define-module (crates-io ta nm) #:use-module (crates-io))

(define-public crate-tanmatsu-0.1 (crate (name "tanmatsu") (vers "0.1.0") (deps (list (crate-dep (name "crossterm") (req "^0.19") (default-features #t) (target "cfg(not(target = \"redox\"))") (kind 0)) (crate-dep (name "termion") (req "^1.5") (default-features #t) (target "cfg(target = \"redox\")") (kind 0)))) (hash "1nwmz95xz8isssks01i2wc8sn9j9wljh9w70wm3hrb58cg015cfr")))

(define-public crate-tanmatsu-0.1 (crate (name "tanmatsu") (vers "0.1.1") (deps (list (crate-dep (name "crossterm") (req "^0.19") (default-features #t) (target "cfg(not(target = \"redox\"))") (kind 0)) (crate-dep (name "termion") (req "^1.5") (default-features #t) (target "cfg(target = \"redox\")") (kind 0)))) (hash "0jzm02akfs7xk8md2ckfwmmja8g6bs5ljdc596bc902f8d25w1zg")))

(define-public crate-tanmatsu-0.2 (crate (name "tanmatsu") (vers "0.2.0") (deps (list (crate-dep (name "crossterm") (req "^0.19") (default-features #t) (target "cfg(not(target = \"redox\"))") (kind 0)) (crate-dep (name "termion") (req "^1.5") (default-features #t) (target "cfg(target = \"redox\")") (kind 0)))) (hash "0vl4vpgf9lvbx077b9wykr10k5v2kdjl7lswcbmr2nb3f8p2cvp6")))

(define-public crate-tanmatsu-0.2 (crate (name "tanmatsu") (vers "0.2.5") (deps (list (crate-dep (name "crossterm") (req "^0.19") (default-features #t) (target "cfg(not(target = \"redox\"))") (kind 0)) (crate-dep (name "termion") (req "^1.5") (default-features #t) (target "cfg(target = \"redox\")") (kind 0)))) (hash "1pwj0sgjrxbbrl7lb9pjr4r800z8f47hbdmq6csk3x8f7bkg0jb3")))

(define-public crate-tanmatsu-0.3 (crate (name "tanmatsu") (vers "0.3.0") (deps (list (crate-dep (name "crossterm") (req "^0.19") (default-features #t) (target "cfg(not(target = \"redox\"))") (kind 0)) (crate-dep (name "termion") (req "^1.5") (default-features #t) (target "cfg(target = \"redox\")") (kind 0)))) (hash "18f8k13c4r35bqrmxyawdhrx9bcric0rz45wwv71x9j3rfa91sq0")))

(define-public crate-tanmatsu-0.4 (crate (name "tanmatsu") (vers "0.4.0") (deps (list (crate-dep (name "crossterm") (req "^0.19") (default-features #t) (target "cfg(not(target = \"redox\"))") (kind 0)) (crate-dep (name "termion") (req "^1.5") (default-features #t) (target "cfg(target = \"redox\")") (kind 0)))) (hash "1kzlxp5bry8hyk1ycgralwcclk7334xca9gp1dj8ldsc84awqqyq")))

(define-public crate-tanmatsu-0.5 (crate (name "tanmatsu") (vers "0.5.0") (deps (list (crate-dep (name "crossterm") (req "^0.20") (default-features #t) (target "cfg(not(target = \"redox\"))") (kind 0)) (crate-dep (name "termion") (req "^1.5.6") (default-features #t) (target "cfg(target = \"redox\")") (kind 0)))) (hash "0nnydhm4yczk2k1g94dy1kviin41760q81sxynyylmsdlmfbz8m1")))

(define-public crate-tanmatsu-0.6 (crate (name "tanmatsu") (vers "0.6.0") (deps (list (crate-dep (name "crossterm") (req "^0.20") (default-features #t) (target "cfg(not(target = \"redox\"))") (kind 0)) (crate-dep (name "termion") (req "^1.5.6") (default-features #t) (target "cfg(target = \"redox\")") (kind 0)))) (hash "1zblvfx53v8c5chdi3828jbb6jzd6l8i3rcgq5akfj7dxclfpxr9")))

(define-public crate-tanmatsu-0.6 (crate (name "tanmatsu") (vers "0.6.1") (deps (list (crate-dep (name "crossterm") (req "^0.20") (default-features #t) (target "cfg(not(target = \"redox\"))") (kind 0)) (crate-dep (name "termion") (req "^1.5.6") (default-features #t) (target "cfg(target = \"redox\")") (kind 0)))) (hash "14mawqnn2f8y3z4fhy47s6qng803wck6qyxq82vfvmylndpc4psp")))

(define-public crate-tanmatsu-0.6 (crate (name "tanmatsu") (vers "0.6.2") (deps (list (crate-dep (name "crossterm") (req "^0.20") (default-features #t) (target "cfg(not(target = \"redox\"))") (kind 0)) (crate-dep (name "termion") (req "^1.5.6") (default-features #t) (target "cfg(target = \"redox\")") (kind 0)))) (hash "1wck3lbx436xaprcvxpr3zk2pflpcm76ajawn8vpq65ipib9rd71")))

(define-public crate-tanmatsu-0.6 (crate (name "tanmatsu") (vers "0.6.3") (deps (list (crate-dep (name "crossterm") (req "^0.20") (default-features #t) (target "cfg(not(target = \"redox\"))") (kind 0)) (crate-dep (name "termion") (req "^1.5.6") (default-features #t) (target "cfg(target = \"redox\")") (kind 0)))) (hash "16nd6n03w30k5m9szicw9xk8i4m7xf87gdkhrg7s188ma04dvg5b")))

(define-public crate-tanmatsu-0.6 (crate (name "tanmatsu") (vers "0.6.4") (deps (list (crate-dep (name "crossterm") (req "^0.20") (default-features #t) (target "cfg(not(target = \"redox\"))") (kind 0)) (crate-dep (name "termion") (req "^1.5.6") (default-features #t) (target "cfg(target = \"redox\")") (kind 0)))) (hash "1lajcr67gfqi09qjdgrq236b82fvsdd48zhfwldlxx4ki5frcy3g")))

