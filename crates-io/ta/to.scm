(define-module (crates-io ta to) #:use-module (crates-io))

(define-public crate-tato-0.1 (crate (name "tato") (vers "0.1.0") (deps (list (crate-dep (name "libm") (req "^0.2.8") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2.18") (default-features #t) (kind 0)) (crate-dep (name "slotmap") (req "^1.0.7") (kind 0)))) (hash "0gf03qaxfjg0a7xbcq5zqh038l0mlbjbaca8ww9i32i9njaxg4sz") (features (quote (("std"))))))

(define-public crate-tato-0.1 (crate (name "tato") (vers "0.1.1") (deps (list (crate-dep (name "libm") (req "^0.2.8") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2.18") (default-features #t) (kind 0)) (crate-dep (name "slotmap") (req "^1.0.7") (kind 0)))) (hash "0awdpqva5p1dqawqxlvq6rd63cqrp0ph34brv8fzdn9v4n8p84j2") (features (quote (("std"))))))

(define-public crate-tato-0.1 (crate (name "tato") (vers "0.1.3") (deps (list (crate-dep (name "libm") (req "^0.2.8") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2.18") (default-features #t) (kind 0)) (crate-dep (name "slotmap") (req "^1.0.7") (kind 0)))) (hash "0cf8m7kf2xyn82aqnj5iz91bpyy7k1rc1x1w61c9bn41fvlhyy43") (features (quote (("std"))))))

(define-public crate-tato-0.1 (crate (name "tato") (vers "0.1.4") (deps (list (crate-dep (name "libm") (req "^0.2.8") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2.18") (default-features #t) (kind 0)) (crate-dep (name "slotmap") (req "^1.0.7") (kind 0)))) (hash "1284xpcf2mixj2hmkxw1mm7g0i7zc1rlhcl3dpg2674ivsz7clqk") (features (quote (("std"))))))

(define-public crate-tato_pipe-0.1 (crate (name "tato_pipe") (vers "0.1.0") (deps (list (crate-dep (name "image") (req "^0.24.7") (default-features #t) (kind 0)) (crate-dep (name "tato") (req "^0.1.1") (features (quote ("std"))) (default-features #t) (kind 0)))) (hash "16ldjihjxyz79973hpdrdbx2p0ila674lmjpw13wk4riia0dllwg")))

(define-public crate-tato_pipe-0.1 (crate (name "tato_pipe") (vers "0.1.1") (deps (list (crate-dep (name "image") (req "^0.24.7") (default-features #t) (kind 0)) (crate-dep (name "tato") (req "^0.1.1") (features (quote ("std"))) (default-features #t) (kind 0)))) (hash "04ih0nzn103g6w82gql0f2p9g9ajxcxq9qp364cp0h9gxk8bspmw")))

(define-public crate-tato_pipe-0.1 (crate (name "tato_pipe") (vers "0.1.2") (deps (list (crate-dep (name "image") (req "^0.24.7") (default-features #t) (kind 0)) (crate-dep (name "tato") (req "^0.1.1") (features (quote ("std"))) (default-features #t) (kind 0)))) (hash "0xkjllxf1cpj2bghcvzw3yqqwms1j8bpprqsiry98b3crs1q7sid")))

