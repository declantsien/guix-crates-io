(define-module (crates-io ta gp) #:use-module (crates-io))

(define-public crate-tagparser-0.1 (crate (name "tagparser") (vers "0.1.0") (deps (list (crate-dep (name "regex") (req "^1.10.2") (default-features #t) (kind 0)))) (hash "1fs8r4hb3fwnrggwgnsb2bvxn1yirx9i7kb1bzb3phz2jzrhzai6") (rust-version "1.75.0")))

(define-public crate-tagparser-0.2 (crate (name "tagparser") (vers "0.2.0") (deps (list (crate-dep (name "regex") (req "^1.10.2") (default-features #t) (kind 0)))) (hash "1r2c32b5lqncd4k3rqk50qaf30hxzm8mr7np37gavn1jhzbk2ghi") (rust-version "1.72.0")))

(define-public crate-tagptr-0.2 (crate (name "tagptr") (vers "0.2.0") (hash "05r4mwvlsclx1ayj65hpzjv3dn4wpi8j4xm695vydccf9k7r683v")))

