(define-module (crates-io ta lr) #:use-module (crates-io))

(define-public crate-talrost-0.1 (crate (name "talrost") (vers "0.1.0") (hash "009ls05jfwn8cm1f6wpj4q67mmxx9kdc9pxfjk1f83y8dg1qjx7z")))

(define-public crate-talrost-0.1 (crate (name "talrost") (vers "0.1.1") (hash "030a8zcvsrq69vmlh1piymlsmk31sd0hdl06znc39a2ybpxrr49f")))

(define-public crate-talrost-0.1 (crate (name "talrost") (vers "0.1.2") (hash "0ppacjzs3n1x77nn9xkdh9srf2d7lwin09kimn97q80i4ldnzkkm")))

