(define-module (crates-io ta na) #:use-module (crates-io))

(define-public crate-tanaka-0.1 (crate (name "tanaka") (vers "0.1.0") (deps (list (crate-dep (name "document-features") (req "^0.2.7") (default-features #t) (kind 0)) (crate-dep (name "lazy-regex") (req "^3.1.0") (default-features #t) (kind 0)) (crate-dep (name "miette") (req "^5.10.0") (default-features #t) (kind 0)) (crate-dep (name "xz-decom") (req "^0.2.0") (default-features #t) (kind 1)))) (hash "0b4qw2vyw53r11is025zlswrxil4bw13vpiy0c5hb52m90pkiww3") (features (quote (("include_subset") ("include") ("default" "include" "include_subset"))))))

