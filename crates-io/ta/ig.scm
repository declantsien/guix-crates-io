(define-module (crates-io ta ig) #:use-module (crates-io))

(define-public crate-taiga-0.0.1 (crate (name "taiga") (vers "0.0.1") (deps (list (crate-dep (name "hyper") (req "^0.6.14") (default-features #t) (kind 0)) (crate-dep (name "rustc-serialize") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "time") (req "^0.1") (default-features #t) (kind 0)))) (hash "1zrncjxgfx00hdf8kx404xkiq5igaaf07msl7a11jmqp7y2mg4j9")))

(define-public crate-taiga-0.0.2 (crate (name "taiga") (vers "0.0.2") (deps (list (crate-dep (name "hyper") (req "^0.6.14") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "*") (default-features #t) (kind 0)) (crate-dep (name "serde_codegen") (req "*") (optional #t) (default-features #t) (kind 1)) (crate-dep (name "serde_json") (req "*") (default-features #t) (kind 0)) (crate-dep (name "serde_macros") (req "*") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "syntex") (req "*") (default-features #t) (kind 1)) (crate-dep (name "time") (req "^0.1") (default-features #t) (kind 0)))) (hash "1a7n5rnbzlzrbln3qi9ssfhkgmzzdclj0746zlqdcrn651ycck4y") (features (quote (("nightly" "serde_macros") ("default" "serde_codegen"))))))

