(define-module (crates-io ta r-) #:use-module (crates-io))

(define-public crate-tar-no-std-0.1 (crate (name "tar-no-std") (vers "0.1.0") (deps (list (crate-dep (name "arrayvec") (req "^0.7") (kind 0)) (crate-dep (name "bitflags") (req "^1.3") (default-features #t) (kind 0)) (crate-dep (name "env_logger") (req "^0.9") (default-features #t) (kind 2)) (crate-dep (name "log") (req "^0.4") (kind 0)))) (hash "1x9v85s7s8ynf4hhwjibvn6g6cpvr978c5g5vnzr3cyxpd9j4zlb") (yanked #t)))

(define-public crate-tar-no-std-0.1 (crate (name "tar-no-std") (vers "0.1.1") (deps (list (crate-dep (name "arrayvec") (req "^0.7") (kind 0)) (crate-dep (name "bitflags") (req "^1.3") (default-features #t) (kind 0)) (crate-dep (name "env_logger") (req "^0.9") (default-features #t) (kind 2)) (crate-dep (name "log") (req "^0.4") (kind 0)))) (hash "1qdan6ban8l18qczkblw3d9gxf8yy5c2yh60nl12wqb3a70myika") (yanked #t)))

(define-public crate-tar-no-std-0.1 (crate (name "tar-no-std") (vers "0.1.2") (deps (list (crate-dep (name "arrayvec") (req "^0.7") (kind 0)) (crate-dep (name "bitflags") (req "^1.3") (default-features #t) (kind 0)) (crate-dep (name "env_logger") (req "^0.9") (default-features #t) (kind 2)) (crate-dep (name "log") (req "^0.4") (kind 0)))) (hash "0a14iadsv80ks9p8n5m46r65waccnzwnmajspbiy2r1gqjfywhdi")))

(define-public crate-tar-no-std-0.1 (crate (name "tar-no-std") (vers "0.1.3") (deps (list (crate-dep (name "arrayvec") (req "^0.7") (kind 0)) (crate-dep (name "bitflags") (req "^1.3") (default-features #t) (kind 0)) (crate-dep (name "env_logger") (req "^0.9") (default-features #t) (kind 2)) (crate-dep (name "log") (req "^0.4") (kind 0)))) (hash "1wb856jjp0qw39pz1qannka8azpd0wvbgni0r2ygl0ckjvlqp7bp")))

(define-public crate-tar-no-std-0.1 (crate (name "tar-no-std") (vers "0.1.4") (deps (list (crate-dep (name "arrayvec") (req "^0.7") (kind 0)) (crate-dep (name "bitflags") (req "^1.3") (default-features #t) (kind 0)) (crate-dep (name "env_logger") (req "^0.9") (default-features #t) (kind 2)) (crate-dep (name "log") (req "^0.4") (kind 0)))) (hash "0s2hfpm0sycgz86nl7f4n5npz9k1ip78fbs2mb4pk57j5a8i37d8")))

(define-public crate-tar-no-std-0.1 (crate (name "tar-no-std") (vers "0.1.5") (deps (list (crate-dep (name "arrayvec") (req "^0.7") (kind 0)) (crate-dep (name "bitflags") (req "^1.3") (default-features #t) (kind 0)) (crate-dep (name "env_logger") (req "^0.9") (default-features #t) (kind 2)) (crate-dep (name "log") (req "^0.4") (kind 0)))) (hash "1mq17ifj20gdjz4irgvvf4i87cljw5rvgjgyhrwkg486xh9263nm") (features (quote (("default") ("alloc") ("all" "alloc"))))))

(define-public crate-tar-no-std-0.1 (crate (name "tar-no-std") (vers "0.1.6") (deps (list (crate-dep (name "arrayvec") (req "^0.7") (kind 0)) (crate-dep (name "bitflags") (req "^1.3") (default-features #t) (kind 0)) (crate-dep (name "env_logger") (req "^0.9") (default-features #t) (kind 2)) (crate-dep (name "log") (req "^0.4") (kind 0)))) (hash "0ly59qzlw2jh8n8j2x4yd87x3w6v5s3wxrslkzr4fnwna6sr23hq") (features (quote (("default") ("alloc") ("all" "alloc"))))))

(define-public crate-tar-no-std-0.1 (crate (name "tar-no-std") (vers "0.1.7") (deps (list (crate-dep (name "arrayvec") (req "^0.7") (kind 0)) (crate-dep (name "bitflags") (req "^1.3") (default-features #t) (kind 0)) (crate-dep (name "env_logger") (req "^0.9") (default-features #t) (kind 2)) (crate-dep (name "log") (req "^0.4") (kind 0)))) (hash "0sl36qggnz6f1zqfhifflk3px93rrlf8dgpwwg1z27mbaq499rgc") (features (quote (("default") ("alloc") ("all" "alloc"))))))

(define-public crate-tar-no-std-0.1 (crate (name "tar-no-std") (vers "0.1.8") (deps (list (crate-dep (name "arrayvec") (req "^0.7") (kind 0)) (crate-dep (name "bitflags") (req "^1.3") (default-features #t) (kind 0)) (crate-dep (name "env_logger") (req "^0.9") (default-features #t) (kind 2)) (crate-dep (name "log") (req "^0.4") (kind 0)))) (hash "1d7gwigjmh3hf8lbbl5zsadczgsqfq1yh466rrkdl88qjphvg8ns") (features (quote (("default") ("alloc") ("all" "alloc"))))))

(define-public crate-tar-no-std-0.2 (crate (name "tar-no-std") (vers "0.2.0") (deps (list (crate-dep (name "arrayvec") (req "^0.7") (kind 0)) (crate-dep (name "bitflags") (req "^2.0") (default-features #t) (kind 0)) (crate-dep (name "env_logger") (req "^0.10") (default-features #t) (kind 2)) (crate-dep (name "log") (req "^0.4") (kind 0)))) (hash "1i9zdwkjfa7qpvs647qir9xcd0492iwjkf50skvax9xd1rsqhxnq") (features (quote (("default") ("alloc"))))))

(define-public crate-tar-no-std-0.3 (crate (name "tar-no-std") (vers "0.3.0") (deps (list (crate-dep (name "bitflags") (req "^2.5") (default-features #t) (kind 0)) (crate-dep (name "env_logger") (req "^0.11") (default-features #t) (kind 2)) (crate-dep (name "log") (req "^0.4") (kind 0)) (crate-dep (name "memchr") (req "^2.7") (kind 0)) (crate-dep (name "num-traits") (req "~0.2") (kind 0)))) (hash "0h9hf8crwn28vfmkjz35rc2ar7bcbr3l8vd3m5h0dj9y5r3yl44a") (features (quote (("unstable") ("default") ("alloc")))) (rust-version "1.76.0")))

(define-public crate-tar-no-std-0.3 (crate (name "tar-no-std") (vers "0.3.1") (deps (list (crate-dep (name "bitflags") (req "^2.5") (default-features #t) (kind 0)) (crate-dep (name "env_logger") (req "^0.11") (default-features #t) (kind 2)) (crate-dep (name "log") (req "^0.4") (kind 0)) (crate-dep (name "memchr") (req "^2.7") (kind 0)) (crate-dep (name "num-traits") (req "~0.2") (kind 0)))) (hash "1im22jzl4qlx3dzbq0cc8d4df6l88ciqigy75isiadh3xs87g2ad") (features (quote (("unstable") ("default") ("alloc")))) (rust-version "1.76.0")))

(define-public crate-tar-parser-0.0.1 (crate (name "tar-parser") (vers "0.0.1") (deps (list (crate-dep (name "nom") (req "~0.3.2") (default-features #t) (kind 0)))) (hash "0jq2l5c1kls40mhw9girr41yxg9zbai3sp8q93ibmqav6hx5jcmm")))

(define-public crate-tar-parser-0.1 (crate (name "tar-parser") (vers "0.1.0") (deps (list (crate-dep (name "nom") (req "~0.3.4") (default-features #t) (kind 0)))) (hash "0knp0z1ldk3rfjrywnvsgp0sfi7kkmm1r29ff7mfrn9a2982qi7q")))

(define-public crate-tar-parser-0.2 (crate (name "tar-parser") (vers "0.2.0") (deps (list (crate-dep (name "nom") (req "~1.0.0") (default-features #t) (kind 0)))) (hash "0ydgi3awld56n6d1l7z1nf2plwq6iw36yxzb07k4hch2wxh3mfzv")))

(define-public crate-tar-parser-0.2 (crate (name "tar-parser") (vers "0.2.1") (deps (list (crate-dep (name "nom") (req "^1.1.0") (default-features #t) (kind 0)))) (hash "183xp3y6mgp7kw2jh1mfsgxpjl5v13b1w85rhr2adg3sf0zj3knx")))

(define-public crate-tar-parser-0.3 (crate (name "tar-parser") (vers "0.3.0") (deps (list (crate-dep (name "nom") (req "^1.2.4") (default-features #t) (kind 0)))) (hash "0yp4a07hk8iz18a3silwzs6xxg3ls7m6l8wd52hyaw19wrbwr1id")))

(define-public crate-tar-parser-0.3 (crate (name "tar-parser") (vers "0.3.1") (deps (list (crate-dep (name "nom") (req "^1.2.4") (default-features #t) (kind 0)))) (hash "0180wchga7nywqgf87897r734alywip8p2c6d37325rs7nyrxv98")))

(define-public crate-tar-parser-0.4 (crate (name "tar-parser") (vers "0.4.0") (deps (list (crate-dep (name "nom") (req "^2.0.0") (default-features #t) (kind 0)))) (hash "0jls1awzq292qpa53lpiqjqsk72bplkazy108yzw5c63dim0wj87")))

(define-public crate-tar-parser-0.4 (crate (name "tar-parser") (vers "0.4.1") (deps (list (crate-dep (name "nom") (req "^2.0.1") (default-features #t) (kind 0)))) (hash "16ff7b8s7wh28cxndl8jp0ip4aram60jpshasnv01x4nr6mjpr41") (yanked #t)))

(define-public crate-tar-parser-0.4 (crate (name "tar-parser") (vers "0.4.2") (deps (list (crate-dep (name "nom") (req "^2.0.1") (default-features #t) (kind 0)))) (hash "17s1naqdjbcchbsd9cslcy7rpxwas2m1r1dswhbmi6z286kwdxhi")))

(define-public crate-tar-parser-0.4 (crate (name "tar-parser") (vers "0.4.3") (deps (list (crate-dep (name "nom") (req "^2.1.0") (default-features #t) (kind 0)))) (hash "0rnzbrlcngfsi58abgrbsjd8igw4vd4jh6ygi0c4x6v5jkapxw6c")))

(define-public crate-tar-parser-0.5 (crate (name "tar-parser") (vers "0.5.0") (deps (list (crate-dep (name "nom") (req "^2.1.0") (default-features #t) (kind 0)))) (hash "1krb919d2ag20i3hbf95hn7p389dx861v11xkzk6vv09lz1138sj")))

(define-public crate-tar-parser-0.6 (crate (name "tar-parser") (vers "0.6.0") (deps (list (crate-dep (name "nom") (req "^4.2") (default-features #t) (kind 0)))) (hash "1ig2ks0wl8m1czspi7w6xndg054fag92mkivzwri8ar2mgxlxckh")))

(define-public crate-tar-parser2-0.7 (crate (name "tar-parser2") (vers "0.7.0") (deps (list (crate-dep (name "nom") (req "^7") (default-features #t) (kind 0)) (crate-dep (name "tar") (req "^0.4") (default-features #t) (kind 2)) (crate-dep (name "tempfile") (req "^3.3") (default-features #t) (kind 2)))) (hash "1a9vabfi77pv5h4jkdmwsq9p3h8agkp2yrxq1550bic50hdvqx3n")))

(define-public crate-tar-parser2-0.7 (crate (name "tar-parser2") (vers "0.7.1") (deps (list (crate-dep (name "nom") (req "^7") (default-features #t) (kind 0)) (crate-dep (name "tar") (req "^0.4") (default-features #t) (kind 2)) (crate-dep (name "tempfile") (req "^3.3") (default-features #t) (kind 2)))) (hash "1r841mbiy1cipjq3a9wnsh5j350fv6f5z4284c8lpnkgzb3dmfpz")))

(define-public crate-tar-parser2-0.8 (crate (name "tar-parser2") (vers "0.8.0") (deps (list (crate-dep (name "nom") (req "^7") (default-features #t) (kind 0)) (crate-dep (name "tar") (req "^0.4") (default-features #t) (kind 2)) (crate-dep (name "tempfile") (req "^3.4") (default-features #t) (kind 2)))) (hash "1gkdskx24fmqkk2y6axvlddg3vr2psvg2lj9gm3nzcsnp9fsmlya")))

(define-public crate-tar-parser2-0.9 (crate (name "tar-parser2") (vers "0.9.0") (deps (list (crate-dep (name "nom") (req "^7") (default-features #t) (kind 0)) (crate-dep (name "tar") (req "^0.4") (default-features #t) (kind 2)) (crate-dep (name "tempfile") (req "^3.4") (default-features #t) (kind 2)))) (hash "08madp75rcb7ls39ssghpf72qgxmi110szimgvp8l601pyx7n3kz")))

(define-public crate-tar-parser2-0.9 (crate (name "tar-parser2") (vers "0.9.1") (deps (list (crate-dep (name "nom") (req "^7") (default-features #t) (kind 0)) (crate-dep (name "tar") (req "^0.4") (default-features #t) (kind 2)) (crate-dep (name "tempfile") (req "^3.4") (default-features #t) (kind 2)))) (hash "0bc8xj9hs6b0m0921f3gh4zpjyyj5ncx8sd09cpbjvpiks6g8qkj")))

(define-public crate-tar-rsl-0.1 (crate (name "tar-rsl") (vers "0.1.0") (deps (list (crate-dep (name "filetime") (req "^0.2.8") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (target "cfg(unix)") (kind 0)) (crate-dep (name "tempfile") (req "^3") (default-features #t) (kind 2)) (crate-dep (name "xattr") (req "^1.0") (optional #t) (default-features #t) (target "cfg(unix)") (kind 0)))) (hash "0zdljj4rkmmnxfkp0s31951fxsmy3s09md26y46i1nzb8nvh7q1p") (features (quote (("default" "xattr"))))))

(define-public crate-tar-rsl-0.1 (crate (name "tar-rsl") (vers "0.1.1") (deps (list (crate-dep (name "filetime") (req "^0.2.8") (default-features #t) (kind 0)) (crate-dep (name "jdks") (req "^0.1.3") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (target "cfg(unix)") (kind 0)) (crate-dep (name "tempfile") (req "^3") (default-features #t) (kind 2)) (crate-dep (name "xattr") (req "^1.0") (optional #t) (default-features #t) (target "cfg(unix)") (kind 0)))) (hash "0nfzb8axvzf8wxrwl31rq6kzlvyyf45hcddizwcsvddqr55shvhj") (features (quote (("default" "xattr"))))))

(define-public crate-tar-sys-0.2 (crate (name "tar-sys") (vers "0.2.0") (deps (list (crate-dep (name "libc") (req "*") (default-features #t) (kind 0)))) (hash "1bg8jgsvv7v4qzdk1jw53jy8q8h2wxgcgg93xw4gr6kd4zjsp536")))

(define-public crate-tar-sys-0.2 (crate (name "tar-sys") (vers "0.2.1") (deps (list (crate-dep (name "libc") (req "*") (default-features #t) (kind 0)))) (hash "1l15f11px1l0c6d98v1k98qyk7g3r9hcpb78mvz09myg29735wxd")))

(define-public crate-tar-sys-0.2 (crate (name "tar-sys") (vers "0.2.2") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "0rlpiahq301llwcpybfv5n7x5l8y1p3rahk1fmk1rgns6acjfhvg")))

(define-public crate-tar-sys-0.3 (crate (name "tar-sys") (vers "0.3.0") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "1xn1c2dq6g1xqfnwnfsggcwws02rvl2n9v6rjzvy87cz3503175l")))

(define-public crate-tar-wasi-0.4 (crate (name "tar-wasi") (vers "0.4.37") (deps (list (crate-dep (name "filetime") (req "^0.2.8") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (target "cfg(unix)") (kind 0)) (crate-dep (name "rand") (req "^0.8") (default-features #t) (kind 2)) (crate-dep (name "tempfile") (req "^3") (default-features #t) (kind 2)) (crate-dep (name "xattr") (req "^0.2") (optional #t) (default-features #t) (target "cfg(unix)") (kind 0)))) (hash "1lwfs5dg5rdkmz6hag5vyllhaf7vi6474fggw3yc1mk942swkj1x") (features (quote (("default" "xattr"))))))

(define-public crate-tar-wasi-0.4 (crate (name "tar-wasi") (vers "0.4.38") (deps (list (crate-dep (name "filetime") (req "^0.2.8") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (target "cfg(unix)") (kind 0)) (crate-dep (name "rand") (req "^0.8") (default-features #t) (kind 2)) (crate-dep (name "tempfile") (req "^3") (default-features #t) (kind 2)) (crate-dep (name "xattr") (req "^0.2") (optional #t) (default-features #t) (target "cfg(unix)") (kind 0)))) (hash "15w85zxyd3hmrcbhpr3ciz1j0iv5i2wzb2w4y3a5pwcgm8wxwgvw") (features (quote (("default" "xattr"))))))

