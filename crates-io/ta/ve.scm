(define-module (crates-io ta ve) #:use-module (crates-io))

(define-public crate-tavern-0.0.0 (crate (name "tavern") (vers "0.0.0") (hash "0gnyw2m25kcgnbqj5sylp6vir2f22rcnlxipjhml7sivscccfpk4") (yanked #t) (rust-version "1.56")))

(define-public crate-tavern-0.0.1 (crate (name "tavern") (vers "0.0.1") (hash "12mz9s0l25cyavgfix3y8hckkapqppr5wxmp76nwbhxphsr2xj6r") (yanked #t) (rust-version "1.56")))

(define-public crate-tavern_activity-0.0.0 (crate (name "tavern_activity") (vers "0.0.0") (hash "04zc5iyrcbq288x1vn4z0vpmpjy89jwyy62g9mlwqbrilvf0gak0") (yanked #t) (rust-version "1.56")))

(define-public crate-tavern_client-0.0.0 (crate (name "tavern_client") (vers "0.0.0") (hash "0bplf6jnyajss2xgphw6qcr8ijklzrgksf4q1qcg7w7rvrvn7qaa") (yanked #t) (rust-version "1.56")))

(define-public crate-tavern_server-0.0.0 (crate (name "tavern_server") (vers "0.0.0") (hash "0zcsj2b3zj5cn160mh3g5rgqs0niwdmx7scc472k5jwn3a17gd8c") (yanked #t) (rust-version "1.56")))

