(define-module (crates-io ta c-) #:use-module (crates-io))

(define-public crate-tac-k-0.1 (crate (name "tac-k") (vers "0.1.0") (deps (list (crate-dep (name "memmap2") (req "^0.9") (default-features #t) (kind 0)))) (hash "06yaabd3nhr5vkrfab2k4f30qmskpifsb4d7d19b2fzmsd0xxkmv") (rust-version "1.70")))

(define-public crate-tac-k-0.1 (crate (name "tac-k") (vers "0.1.1") (deps (list (crate-dep (name "memmap2") (req "^0.9") (default-features #t) (kind 0)))) (hash "0z40anr8s46fzj1a29lxcfad2bf8rhcqw186krv5hib6mhjjjq3h") (rust-version "1.70")))

(define-public crate-tac-k-0.1 (crate (name "tac-k") (vers "0.1.2") (deps (list (crate-dep (name "memmap2") (req "^0.9") (default-features #t) (kind 0)))) (hash "1da2f4dkws4l1y6adf1pkwj2h2z7wx35lszdl3hc51sdqfhicxw2") (yanked #t) (rust-version "1.70")))

(define-public crate-tac-k-0.1 (crate (name "tac-k") (vers "0.1.3") (deps (list (crate-dep (name "memmap2") (req "^0.9") (default-features #t) (kind 0)))) (hash "1x8vms7iv9inm51lh81rq2c075yvz2ibapka7g1l66hmspgmws98") (rust-version "1.70")))

(define-public crate-tac-k-0.2 (crate (name "tac-k") (vers "0.2.0") (deps (list (crate-dep (name "memmap2") (req "^0.9") (default-features #t) (kind 0)))) (hash "0wccwv8290anzwzlmb73gr71d0yizw68b9bqzwzdmxv312h90dja") (rust-version "1.70")))

(define-public crate-tac-k-0.2 (crate (name "tac-k") (vers "0.2.1") (deps (list (crate-dep (name "memmap2") (req "^0.9") (default-features #t) (kind 0)))) (hash "03fsmvf4hv1y4jmnzjiims39x3cf9mp8s7lgxa5gqi8az3iprysn") (rust-version "1.70")))

(define-public crate-tac-k-0.3 (crate (name "tac-k") (vers "0.3.0") (deps (list (crate-dep (name "clap") (req "~4.4") (features (quote ("std" "help" "usage" "cargo"))) (kind 0)) (crate-dep (name "memmap2") (req "^0.9") (default-features #t) (kind 0)))) (hash "18z6h7kji316nxpwi0x06madv6v63askk2dhv1w2xv68vl578zhh") (rust-version "1.70")))

(define-public crate-tac-k-0.3 (crate (name "tac-k") (vers "0.3.1") (deps (list (crate-dep (name "clap") (req "~4.4") (features (quote ("std" "help" "usage" "cargo"))) (kind 0)) (crate-dep (name "memmap2") (req "^0.9") (default-features #t) (kind 0)))) (hash "0xbaycd98d0wa9165mrj6qiq8jv2d4006z12amglj2njvypyklck") (rust-version "1.70")))

