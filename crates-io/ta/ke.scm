(define-module (crates-io ta ke) #:use-module (crates-io))

(define-public crate-take-0.1 (crate (name "take") (vers "0.1.0") (hash "1i8p579k9kq21k7pcm4yzbc12xpshl39jfa5c1j6pxf1ia6qcmxi")))

(define-public crate-take-breath-0.1 (crate (name "take-breath") (vers "0.1.0") (deps (list (crate-dep (name "notify-rust") (req "^4.5.2") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.9.0") (features (quote ("full" "time"))) (default-features #t) (kind 0)) (crate-dep (name "user-idle") (req "^0.5.0") (default-features #t) (kind 0)))) (hash "1198n2s1fm5mrnf76npckcw3px50wpqkv2yl6wvsi41y82s0xn9k")))

(define-public crate-take-breath-0.1 (crate (name "take-breath") (vers "0.1.1") (deps (list (crate-dep (name "notify-rust") (req "^4.5.2") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.9.0") (features (quote ("full" "time"))) (default-features #t) (kind 0)) (crate-dep (name "user-idle") (req "^0.5.0") (default-features #t) (kind 0)))) (hash "1sll6lzxc9jhk3zaipdp5fllp1454s42d3n7mix66bz8h2jkybgy")))

(define-public crate-take-breath-0.1 (crate (name "take-breath") (vers "0.1.2") (deps (list (crate-dep (name "notify-rust") (req "^4.5.2") (default-features #t) (kind 0)) (crate-dep (name "user-idle") (req "^0.5.0") (default-features #t) (kind 0)))) (hash "07wxy9n7r7rdbz13zij90bzbx612ywpvyih341pj5gnpxpji4m6b")))

(define-public crate-take-breath-0.1 (crate (name "take-breath") (vers "0.1.3") (deps (list (crate-dep (name "dirs") (req "^3.0.2") (default-features #t) (kind 0)) (crate-dep (name "humantime") (req "^2.1.0") (default-features #t) (kind 0)) (crate-dep (name "humantime-serde") (req "^1.0.1") (default-features #t) (kind 0)) (crate-dep (name "notify-rust") (req "^4.5.2") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.127") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "toml") (req "^0.5.8") (default-features #t) (kind 0)) (crate-dep (name "user-idle") (req "^0.5.0") (default-features #t) (kind 0)))) (hash "08jh8z8nqqss1jymp6hcq37ral10bl2g30nx7klh4iyk7jlxwb0v")))

(define-public crate-take-breath-0.1 (crate (name "take-breath") (vers "0.1.4") (deps (list (crate-dep (name "dirs") (req "^3.0.2") (default-features #t) (kind 0)) (crate-dep (name "humantime") (req "^2.1.0") (default-features #t) (kind 0)) (crate-dep (name "humantime-serde") (req "^1.0.1") (default-features #t) (kind 0)) (crate-dep (name "notify-rust") (req "^4.5.2") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.127") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "toml") (req "^0.5.8") (default-features #t) (kind 0)) (crate-dep (name "user-idle") (req "^0.5.0") (default-features #t) (kind 0)))) (hash "07h31p86wr1gmr9b5adw02vv5472dh5bg3ns0zz7dn66a6fzlwsd")))

(define-public crate-take-breath-0.1 (crate (name "take-breath") (vers "0.1.5") (deps (list (crate-dep (name "clap") (req "^2.33.3") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "dirs") (req "^3.0.2") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "humantime-serde") (req "^1.0.1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "notify-rust") (req "^4.5.2") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.127") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "toml") (req "^0.5.8") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "user-idle") (req "^0.5.0") (default-features #t) (kind 0)))) (hash "1bn57q3zmdqfjsw5ri0hnmb0qmd5q94pdhf9450cn48sg8kba45z") (features (quote (("notify" "notify-rust") ("default" "notify" "config" "cli") ("config" "serde/derive" "humantime-serde" "toml" "dirs") ("cli" "clap"))))))

(define-public crate-take-cell-option-0.1 (crate (name "take-cell-option") (vers "0.1.0") (hash "0jza3jdcp6fjhcvr4nkar2wvb837s3sx4wxs0g518y3jady68np9")))

(define-public crate-take-cell-option-0.1 (crate (name "take-cell-option") (vers "0.1.1") (hash "0dq4cqigylv0kfvlh4yv65nk5war68kyhyw9nmndi2dx7hmxzczv")))

(define-public crate-take-cell-option-0.1 (crate (name "take-cell-option") (vers "0.1.2") (hash "1jd7q6pmgdj5a4445rlh7mbrc3679z73xzlhx3p0gf06maiahacm")))

(define-public crate-take-if-0.1 (crate (name "take-if") (vers "0.1.0") (hash "069ydrzbgs7k4194gqqzvxslniqwla7ysnjcw99sfwl9p5mwm67w")))

(define-public crate-take-if-1 (crate (name "take-if") (vers "1.0.0") (hash "1w69z6f691ddfzf2kl00sifrp69gfvy3s1i44s9rsn1ykdfa9mcd")))

(define-public crate-take-me-over-0.1 (crate (name "take-me-over") (vers "0.1.0") (hash "1im8w8sq2rpzbaas9ny5q8appqrjd250va12j9yl8gh6d0nnhjd1")))

(define-public crate-take-some-0.1 (crate (name "take-some") (vers "0.1.0") (deps (list (crate-dep (name "indexmap") (req "^1.0") (default-features #t) (kind 2)))) (hash "1i6drbd5jrwy6sv38xzbvd4zyc74lyszsjs8iqrgr19ygm83yg7j")))

(define-public crate-take-some-0.1 (crate (name "take-some") (vers "0.1.1") (deps (list (crate-dep (name "indexmap") (req "^1.0") (default-features #t) (kind 2)))) (hash "13csif3gggfq33cifmc6lsnjs873n07dz9441s4idsj5xxj0w7hi")))

(define-public crate-take-some-0.1 (crate (name "take-some") (vers "0.1.2") (deps (list (crate-dep (name "indexmap") (req "^1.0") (default-features #t) (kind 2)))) (hash "02y09p6r1r2r1j9ayjdzwy92rgkjyzf7vp3ams9wjlgcs4h5k5l5")))

(define-public crate-take-static-0.1 (crate (name "take-static") (vers "0.1.0") (deps (list (crate-dep (name "call-once") (req "^0.1") (default-features #t) (kind 0)))) (hash "1pibsdw8psdr1665lm3f9aq1ixbf4jw7kxc4miiq6pxl478jyypq")))

(define-public crate-take-static-0.1 (crate (name "take-static") (vers "0.1.1") (deps (list (crate-dep (name "call-once") (req "^0.1") (default-features #t) (kind 0)))) (hash "01h9jvsiswa8fmsrq2ysrwvj850vxlvl8gy6p85m8s33wxvkgs6f")))

(define-public crate-take-static-0.1 (crate (name "take-static") (vers "0.1.2") (deps (list (crate-dep (name "call-once") (req "^0.1") (default-features #t) (kind 0)))) (hash "00gyqi0m1igfnxh043dhi2zvv4zwp72pajz1ymfxn7wnh05fnryg")))

(define-public crate-take-until-0.1 (crate (name "take-until") (vers "0.1.0") (hash "0fzy344njqfgc6mi2jlji7h70d163hyd777m989qqyh6k22pvqdl")))

(define-public crate-take-until-0.2 (crate (name "take-until") (vers "0.2.0") (hash "1fqbg8pk6yl03k9pr411h6fvj8ygknx42w3bwv0khyx6vyh6znwb")))

(define-public crate-take_mut-0.1 (crate (name "take_mut") (vers "0.1.0") (hash "0ph5afdz81zch4nwkyql0aiawgfg5vdkavx381nlzbqz9dbhmy6p")))

(define-public crate-take_mut-0.1 (crate (name "take_mut") (vers "0.1.1") (hash "17w06a8qk2kpnfk3m3r2b3vhy3ycvd1iih83nm11sakl1bdxag3i")))

(define-public crate-take_mut-0.1 (crate (name "take_mut") (vers "0.1.2") (hash "192csxl45y11skqix6zd66vssjjqqcg368lmhxsw9ks5c1h1vs27") (yanked #t)))

(define-public crate-take_mut-0.1 (crate (name "take_mut") (vers "0.1.3") (hash "02112b0sql9j016yb9p52vq0bglv70ch2z5jp3yf2x8diaqwx1kr")))

(define-public crate-take_mut-0.2 (crate (name "take_mut") (vers "0.2.0") (hash "1iglhzj5g9d5fm0hj3g43n0gfg48adr0x2lfffssxx2d2yhi1fah")))

(define-public crate-take_mut-0.2 (crate (name "take_mut") (vers "0.2.1") (hash "14f7n3qk0z85w5bm0p59xcmkphs5vhiyn9a1s03zvr9kqcpj94yb")))

(define-public crate-take_mut-0.2 (crate (name "take_mut") (vers "0.2.2") (hash "0q2d7w6nd5bl7bay5csq065sjg8fw0jcx6hl1983cpzf25fh0r7p")))

(define-public crate-take_ref-0.1 (crate (name "take_ref") (vers "0.1.0") (hash "0ls3zxbr240rpnk2mfiiq1sn4vlva9y3cr4lkqg4cn7cy808d4ll")))

(define-public crate-take_ref-0.2 (crate (name "take_ref") (vers "0.2.0") (hash "0bkxc406jxfca8mabwjdk9r52wrqvbim0a85rclgbppa6wl70cin") (features (quote (("use_std") ("default" "use_std"))))))

(define-public crate-take_while_with_failure-0.1 (crate (name "take_while_with_failure") (vers "0.1.0") (hash "1x7rmm7drra3hidban17x4yha56fhzp4nzdsvkw9dq2csfrfiimp")))

(define-public crate-takeable-0.1 (crate (name "takeable") (vers "0.1.0") (deps (list (crate-dep (name "unreachable") (req "^1.0") (optional #t) (default-features #t) (kind 0)))) (hash "0mr53igsjcmyg7h98dqg7jwkg9qyr5nivwqpn2h6ljc7kgfs8m25") (features (quote (("microoptimization" "unreachable") ("default" "microoptimization"))))))

(define-public crate-takeable-0.2 (crate (name "takeable") (vers "0.2.0") (hash "0igcjinwdqxaswrgf3mxzfzxp2fmi3z3m3nnb0mrb8hq8i0g9b37")))

(define-public crate-takeable-0.2 (crate (name "takeable") (vers "0.2.1") (hash "1ac8xmrkwfb7wkhxf6zh582yj5hxjdckbz054rwdmpwjy08fhiwm")))

(define-public crate-takeable-0.2 (crate (name "takeable") (vers "0.2.2") (hash "0xwllv6z39a0wn1r01agvp82p7kc1ig8bgsf6r0j09kj2z813zky")))

(define-public crate-takeable-option-0.1 (crate (name "takeable-option") (vers "0.1.0") (hash "1llccmrg920fvgrdk4lv3w3id7vl10fg5pwxn1b928knzk9ym3g5")))

(define-public crate-takeable-option-0.2 (crate (name "takeable-option") (vers "0.2.0") (hash "12hnx4012ykjq1f2sabimgkpsra3sx2g4syvz5cgm1c3vvlshr8d")))

(define-public crate-takeable-option-0.3 (crate (name "takeable-option") (vers "0.3.0") (hash "0g7x9vvg0pfdhccqq1y438nrg1zf9lpra1284m51gzd8rjrwaiwv")))

(define-public crate-takeable-option-0.4 (crate (name "takeable-option") (vers "0.4.0") (hash "0hvd6vk4ksgg2y99498jw52ric4lxm0i6ygpzqm95gdrhvsxyynp")))

(define-public crate-takeable-option-0.5 (crate (name "takeable-option") (vers "0.5.0") (hash "182axkm8pq7cynsfn65ar817mmdhayrjmbl371yqp8zyzhr8kbin")))

(define-public crate-takecell-0.1 (crate (name "takecell") (vers "0.1.0") (deps (list (crate-dep (name "once_cell") (req "^1.8.0") (default-features #t) (kind 2)))) (hash "0qr8s2vydcpmw1rim9di93yk4pgsrs5kncmvzzj4xmq7q2rpaxyh")))

(define-public crate-takecell-0.1 (crate (name "takecell") (vers "0.1.1") (deps (list (crate-dep (name "once_cell") (req "^1.8.0") (default-features #t) (kind 2)))) (hash "0gpzcnazzwl6mq7xzbk9jf5zd0959h6310ls1ibapp3ccwwl7wr0")))

(define-public crate-taken-0.1 (crate (name "taken") (vers "0.1.0") (hash "0zsphfwhs6hbakyyx8i9mlmdq8b2d8l6v18dvg0xrsn76lj461qb")))

(define-public crate-taken-0.1 (crate (name "taken") (vers "0.1.1") (hash "1r3ddqc4pq67va9hav03xyp667aj8qgzhh2ywnj10gyhmbz4vrc4")))

(define-public crate-takeoff-0.1 (crate (name "takeoff") (vers "0.1.0") (deps (list (crate-dep (name "anyhow") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "glob") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "grass") (req "^0.10") (default-features #t) (kind 0)) (crate-dep (name "pulldown-cmark") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syntect") (req "^4.4") (default-features #t) (kind 0)) (crate-dep (name "tera") (req "^1.5") (default-features #t) (kind 0)) (crate-dep (name "toml") (req "^0.5") (default-features #t) (kind 0)) (crate-dep (name "tracing") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "tracing-subscriber") (req "^0.2") (default-features #t) (kind 2)) (crate-dep (name "walkdir") (req "^2.3") (default-features #t) (kind 0)))) (hash "14rs5hf5n6k1wigzqgac0y501zb7f21awsw4517db9l673wicr9p") (yanked #t)))

(define-public crate-takeoff-0.1 (crate (name "takeoff") (vers "0.1.1") (deps (list (crate-dep (name "anyhow") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "glob") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "grass") (req "^0.10") (default-features #t) (kind 0)) (crate-dep (name "pulldown-cmark") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syntect") (req "^4.4") (default-features #t) (kind 0)) (crate-dep (name "tera") (req "^1.5") (default-features #t) (kind 0)) (crate-dep (name "toml") (req "^0.5") (default-features #t) (kind 0)) (crate-dep (name "tracing") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "tracing-subscriber") (req "^0.2") (default-features #t) (kind 2)) (crate-dep (name "walkdir") (req "^2.3") (default-features #t) (kind 0)))) (hash "0db70gvf7l7zhbq6bxhknfgwkm96h85c60zgnbxha00xxpwip014") (yanked #t)))

(define-public crate-takeoff_cli-0.1 (crate (name "takeoff_cli") (vers "0.1.1") (deps (list (crate-dep (name "anyhow") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^2.33") (default-features #t) (kind 0)) (crate-dep (name "takeoff") (req "^0.1.1") (default-features #t) (kind 0)) (crate-dep (name "tracing-subscriber") (req "^0.2") (default-features #t) (kind 0)))) (hash "1inpwswjyrlibl36b6l1w8blbndwyp5m21djalvki5q42al3dv00") (yanked #t)))

(define-public crate-takeout-metadata-0.1 (crate (name "takeout-metadata") (vers "0.1.0") (deps (list (crate-dep (name "chrono") (req "^0.4.38") (features (quote ("serde"))) (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^4.5.4") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.198") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.116") (default-features #t) (kind 0)))) (hash "08i78j0mr8i82fi9bbqdpikwy5dwhj8h23wxwgabkpsw4qigrpkl") (rust-version "1.70")))

(define-public crate-takeout-metadata-0.1 (crate (name "takeout-metadata") (vers "0.1.1") (deps (list (crate-dep (name "chrono") (req "^0.4.38") (features (quote ("serde"))) (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^4.5.4") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.198") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.116") (default-features #t) (kind 0)))) (hash "10nvjfi50rgqlfhdg609jpzljzqgidykc774w9d6mfnzsds2bv2i") (rust-version "1.70")))

(define-public crate-taker-0.1 (crate (name "taker") (vers "0.1.0") (hash "1p4xic2fcyhpxd0hrbsiypzy95yv2094pdp66nzlbv7nxjakyd0k")))

(define-public crate-takes-0.1 (crate (name "takes") (vers "0.1.0") (hash "1wc8cn1fksxxz13cxfq94866yn553f1v8xfcg1jakqzqz0abjr0m")))

