(define-module (crates-io ta dm) #:use-module (crates-io))

(define-public crate-tadm-0.1 (crate (name "tadm") (vers "0.1.0") (hash "0x9iwwv2z8ply5n9h2j5xkfrkxdmic9a653wdbklbipv669l5v5p") (rust-version "1.58")))

(define-public crate-tadm-0.1 (crate (name "tadm") (vers "0.1.1") (hash "080k7d6cldcwdvm810bnsaqmxhrq0pxygnnhs1p9pp5vmwbivrla") (rust-version "1.58")))

