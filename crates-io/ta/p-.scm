(define-module (crates-io ta p-) #:use-module (crates-io))

(define-public crate-tap-consooomer-0.1 (crate (name "tap-consooomer") (vers "0.1.0") (deps (list (crate-dep (name "anyhow") (req "^1.0.65") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^4.1.1") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "pest") (req "^2.3.1") (default-features #t) (kind 0)) (crate-dep (name "pest_derive") (req "^2.3.1") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.144") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.85") (default-features #t) (kind 0)))) (hash "1x6ca77nskl06nhgmcb4ns5lx93hbahw58774ifzg9ym4384f836") (rust-version "1.64.0")))

(define-public crate-tap-harness-0.1 (crate (name "tap-harness") (vers "0.1.0") (deps (list (crate-dep (name "anyhow") (req "^1.0") (default-features #t) (kind 2)) (crate-dep (name "testanything") (req "^0.4") (default-features #t) (kind 0)))) (hash "0vwl9w47nnf5wiff6p02jpcfcwnp7pvkfb748i277kpdb7wmjg0v")))

(define-public crate-tap-harness-0.2 (crate (name "tap-harness") (vers "0.2.0") (deps (list (crate-dep (name "anyhow") (req "^1.0") (features (quote ("std"))) (kind 2)) (crate-dep (name "testanything") (req "^0.4") (features (quote ("std"))) (kind 0)))) (hash "0p95l62w08plwrbc6afsg530rqxlpk01xpp678iz4k7kpr7q4vsv")))

(define-public crate-tap-harness-0.3 (crate (name "tap-harness") (vers "0.3.0") (deps (list (crate-dep (name "anyhow") (req "^1.0") (features (quote ("std"))) (kind 2)) (crate-dep (name "testanything") (req "^0.4") (features (quote ("std"))) (kind 0)))) (hash "13bn7gbsi517pc6x9d1vqww5xkfmadr7lliyzc5wdd9lwr0i593h")))

(define-public crate-tap-reader-1 (crate (name "tap-reader") (vers "1.0.0") (deps (list (crate-dep (name "serde") (req "^1") (default-features #t) (kind 2)) (crate-dep (name "serde_derive") (req "^1") (default-features #t) (kind 2)) (crate-dep (name "serde_json") (req "^1") (default-features #t) (kind 2)))) (hash "0sgy8b64v9sfh26vcrzgq38mxzsjbnapipgqs1fl2qxf4lnqsyv9")))

(define-public crate-tap-reader-1 (crate (name "tap-reader") (vers "1.0.1") (deps (list (crate-dep (name "serde") (req "^1") (default-features #t) (kind 2)) (crate-dep (name "serde_derive") (req "^1") (default-features #t) (kind 2)) (crate-dep (name "serde_json") (req "^1") (default-features #t) (kind 2)))) (hash "1wxnxzmszmg8lqc3cmzzpilp7w2r1ir7k9lvaba8qnj13lqgq4wg")))

(define-public crate-tap-trait-0.0.0 (crate (name "tap-trait") (vers "0.0.0") (deps (list (crate-dep (name "pipe-trait") (req "^0.3.2") (default-features #t) (kind 2)))) (hash "1c2crwpkh4dnwrrv1kw7v1iqf3i0s511g56wyjlhaqrdcq2fwd7i")))

(define-public crate-tap-trait-1 (crate (name "tap-trait") (vers "1.0.0") (deps (list (crate-dep (name "pipe-trait") (req "^0.3.2") (default-features #t) (kind 2)))) (hash "0pb7ph9l6a3xzy866qa4ybg8mxjfblinjy1mgm941n10pkc1c2cb")))

(define-public crate-tap-windows-0.1 (crate (name "tap-windows") (vers "0.1.0") (deps (list (crate-dep (name "scopeguard") (req "^1.1") (default-features #t) (kind 0)) (crate-dep (name "winapi") (req "^0.3") (features (quote ("errhandlingapi" "combaseapi" "ioapiset" "winioctl" "setupapi" "synchapi" "netioapi" "fileapi"))) (default-features #t) (kind 0)) (crate-dep (name "winreg") (req "^0.7") (default-features #t) (kind 0)))) (hash "0kaghddbddpji5iy7dvxgji38yg80znwir9axz5fqkalxhmpl8mx") (yanked #t)))

(define-public crate-tap-windows-0.1 (crate (name "tap-windows") (vers "0.1.1") (deps (list (crate-dep (name "scopeguard") (req "^1.1") (default-features #t) (kind 0)) (crate-dep (name "winapi") (req "^0.3") (features (quote ("errhandlingapi" "combaseapi" "ioapiset" "winioctl" "setupapi" "synchapi" "netioapi" "fileapi"))) (default-features #t) (kind 0)) (crate-dep (name "winreg") (req "^0.7") (default-features #t) (kind 0)))) (hash "1ghs65lw5v8avc3sq65r4php2ki5h78kl2pnazqhi4f4d94j487n") (yanked #t)))

(define-public crate-tap-windows-0.1 (crate (name "tap-windows") (vers "0.1.2") (deps (list (crate-dep (name "scopeguard") (req "^1.1") (default-features #t) (kind 0)) (crate-dep (name "winapi") (req "^0.3") (features (quote ("errhandlingapi" "combaseapi" "ioapiset" "winioctl" "setupapi" "synchapi" "netioapi" "fileapi"))) (default-features #t) (kind 0)) (crate-dep (name "winreg") (req "^0.7") (default-features #t) (kind 0)))) (hash "1jyb9lcvs6qjixknn76frx02zbmb3wpp56pkmpp9v1ais7vagj0d")))

