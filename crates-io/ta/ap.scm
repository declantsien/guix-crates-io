(define-module (crates-io ta ap) #:use-module (crates-io))

(define-public crate-taap-0.1 (crate (name "taap") (vers "0.1.0") (hash "0cb2iibjd8ahx0flm95rgz61203cxysi9kzsi9mkl1wjk7j8npkf")))

(define-public crate-taap-0.1 (crate (name "taap") (vers "0.1.1") (hash "00b67m126aybv4byayincrx8xa1g7kf9m7xck3wjm9fvc6zsi565")))

(define-public crate-taap-0.1 (crate (name "taap") (vers "0.1.2") (hash "0p969yrzavi0kprfyal6f9fjkb77hqdr6xq74bxyxviapwhk6zvi") (yanked #t)))

(define-public crate-taap-0.1 (crate (name "taap") (vers "0.1.3") (hash "074ig3hjifcgbc41flyblp0wy77qsp53iqxjbgw0f5217pkhmac2")))

(define-public crate-taap-0.1 (crate (name "taap") (vers "0.1.4") (hash "0lyj054i2w8ddmlga97lbzvy9clapnvjs29krxbmmnn1q7bh9jaa")))

