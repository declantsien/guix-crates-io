(define-module (crates-io ta nb) #:use-module (crates-io))

(define-public crate-tanban-0.1 (crate (name "tanban") (vers "0.1.0") (deps (list (crate-dep (name "tui") (req "^0.19.0") (default-features #t) (kind 0)))) (hash "0rhdp83fdsb35vmw20xcwsicx0shv7pzl1yig4jl6wlxzcjdzzzk") (yanked #t)))

(define-public crate-tanban-0.1 (crate (name "tanban") (vers "0.1.1") (deps (list (crate-dep (name "chrono") (req "^0.4.23") (default-features #t) (kind 0)) (crate-dep (name "crossterm") (req "^0.25.0") (default-features #t) (kind 0)) (crate-dep (name "tui") (req "^0.19.0") (default-features #t) (kind 0)))) (hash "0gp5ccalqxsfslq4fx2p98z70zvvlg17clhr5pj8dck3d4wb14xz") (yanked #t)))

(define-public crate-tanban-0.1 (crate (name "tanban") (vers "0.1.2") (deps (list (crate-dep (name "cursive") (req "^0.20.0") (features (quote ("toml"))) (default-features #t) (kind 0)))) (hash "0lf47drx2n1wq6xcw98rrqq324yzwc8m77y89ywfh23wi0i90nlk") (yanked #t)))

(define-public crate-tanban-0.1 (crate (name "tanban") (vers "0.1.3") (deps (list (crate-dep (name "cursive") (req "^0.20.0") (features (quote ("toml"))) (default-features #t) (kind 0)))) (hash "1gwg34yd9dprsnvfy3krbahjwl0vswc1b3byyh48ffr2fcagc4j2") (yanked #t)))

