(define-module (crates-io ta pe) #:use-module (crates-io))

(define-public crate-tape-0.0.1 (crate (name "tape") (vers "0.0.1") (deps (list (crate-dep (name "libtar-sys") (req "^0.0.1") (default-features #t) (kind 0)))) (hash "1wwl2vnn2r608759ckg1hynm60mymg5p7iyp24k3glbmrpcdwlwv")))

(define-public crate-tape-0.0.2 (crate (name "tape") (vers "0.0.2") (deps (list (crate-dep (name "libtar-sys") (req "^0.0.3") (default-features #t) (kind 0)))) (hash "0p4pax7gn8lz9hwxswp28543m33j8i1ydd3hz4bhs3bgv6d3s2vk")))

(define-public crate-tape-0.0.3 (crate (name "tape") (vers "0.0.3") (deps (list (crate-dep (name "libtar-sys") (req "^0.0.4") (default-features #t) (kind 0)))) (hash "0gnj6s590d7sw1pd7rhyv5sfl68ky6n64zghj0k28jkba1njcvki")))

(define-public crate-tape-0.0.4 (crate (name "tape") (vers "0.0.4") (deps (list (crate-dep (name "libtar-sys") (req "^0.0.4") (default-features #t) (kind 0)))) (hash "1582kc4z9cm1y4bwg2vl36rmvl59wsc8cliq1jwi722gfzi60w2p")))

(define-public crate-tape-0.0.5 (crate (name "tape") (vers "0.0.5") (deps (list (crate-dep (name "libtar-sys") (req "^0.0.5") (default-features #t) (kind 0)))) (hash "187n67hfb9zcxh49azfb8khlkyv2y35j6rhr3bry2bc4ng8x6xqb")))

(define-public crate-tape-0.0.6 (crate (name "tape") (vers "0.0.6") (deps (list (crate-dep (name "libtar-sys") (req "^0.0.6") (default-features #t) (kind 0)))) (hash "0350211iyxl96ncnv6n920k884vynwq68hx4xqyx73nll72fym93")))

(define-public crate-tape-0.0.7 (crate (name "tape") (vers "0.0.7") (deps (list (crate-dep (name "libtar-sys") (req "^0.0.7") (default-features #t) (kind 0)))) (hash "1ydkbxjmfsaijgp2vmaw7ifjcnb0jjcfvrbk3c3j4hzr3lj3hflk")))

(define-public crate-tape-0.0.8 (crate (name "tape") (vers "0.0.8") (deps (list (crate-dep (name "libtar-sys") (req "^0.0.8") (default-features #t) (kind 0)))) (hash "0iw8yx8krr3bldp2f4ylh6w1ncjbrgmp39g33bmy6py9pyxgzfqk")))

(define-public crate-tape-0.0.9 (crate (name "tape") (vers "0.0.9") (deps (list (crate-dep (name "libtar-sys") (req "^0.0.9") (default-features #t) (kind 0)))) (hash "1a755gnwfjwz8skd5ynvi8ih1w45yl4rhqfgvavyy2a2c9na6bqb")))

(define-public crate-tape-0.1 (crate (name "tape") (vers "0.1.0") (deps (list (crate-dep (name "libtar-sys") (req "^0.1") (default-features #t) (kind 0)))) (hash "1f4x057g4mcgsfr4jdk4fa12lrh6dmygyr8l07fzmxbbxsipq0cx")))

(define-public crate-tape-0.1 (crate (name "tape") (vers "0.1.1") (deps (list (crate-dep (name "libtar-sys") (req "^0.1") (default-features #t) (kind 0)))) (hash "0ca0r2j2sqmfnybi6dh0jdjsidwx71lb365cdcg5qc7pllzdpzb1")))

(define-public crate-tape-0.1 (crate (name "tape") (vers "0.1.2") (deps (list (crate-dep (name "libtar-sys") (req "^0.1") (default-features #t) (kind 0)))) (hash "09ikjvljkrkmz5hbvai22ipkd9shza26797f1ghishyj10l3d9ab")))

(define-public crate-tape-0.1 (crate (name "tape") (vers "0.1.3") (deps (list (crate-dep (name "libtar-sys") (req "^0.1") (default-features #t) (kind 0)))) (hash "1ql2km69534i30hf545wzwplg9wrwrdjiji1nb7sdwsycqb9vl0h")))

(define-public crate-tape-0.1 (crate (name "tape") (vers "0.1.4") (deps (list (crate-dep (name "libtar-sys") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "temporary") (req "*") (default-features #t) (kind 2)))) (hash "082zvd6s4h743lm7nvcwwc4kyc2s1irlmza6gpnvziz1iz7ng2gd")))

(define-public crate-tape-0.1 (crate (name "tape") (vers "0.1.5") (deps (list (crate-dep (name "libtar-sys") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "temporary") (req "*") (default-features #t) (kind 2)))) (hash "08gs24l5wlz8rpxw10qx19n3li7v2j8f2f6h1czymv8jn1cpxs0l")))

(define-public crate-tape-0.1 (crate (name "tape") (vers "0.1.6") (deps (list (crate-dep (name "libtar-sys") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "temporary") (req "*") (default-features #t) (kind 2)))) (hash "08jgn4r4pb2dl7h4wxng512jjhrwzqnp82zl7j7940ar5h2mzv1g")))

(define-public crate-tape-0.1 (crate (name "tape") (vers "0.1.7") (deps (list (crate-dep (name "libtar-sys") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "temporary") (req "*") (default-features #t) (kind 2)))) (hash "0dwmfkv2gpy4lvaz2id7lylzhj5ak5nhhmsnz54pvrq845kgdliy")))

(define-public crate-tape-0.1 (crate (name "tape") (vers "0.1.8") (deps (list (crate-dep (name "libc") (req "*") (default-features #t) (kind 0)) (crate-dep (name "libtar-sys") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "temporary") (req "*") (default-features #t) (kind 2)))) (hash "189lmmlqpw0b5nfv3zkjj1027ndi4wf3kfjahzr47x1sz8zpqy86")))

(define-public crate-tape-0.2 (crate (name "tape") (vers "0.2.0") (deps (list (crate-dep (name "libc") (req "*") (default-features #t) (kind 0)) (crate-dep (name "libtar-sys") (req "*") (default-features #t) (kind 0)) (crate-dep (name "temporary") (req "*") (default-features #t) (kind 2)))) (hash "0yz8wq9hkls44dxg2m114rhv28104pd49mpwvc6avilgpisiy7rv")))

(define-public crate-tape-0.2 (crate (name "tape") (vers "0.2.1") (deps (list (crate-dep (name "libc") (req "*") (default-features #t) (kind 0)) (crate-dep (name "tar-sys") (req "*") (default-features #t) (kind 0)) (crate-dep (name "temporary") (req "*") (default-features #t) (kind 2)))) (hash "067isdcr1vwzl07fwjxldyja5gpdf81ivcifffrnj5242w12d79r")))

(define-public crate-tape-0.3 (crate (name "tape") (vers "0.3.0") (deps (list (crate-dep (name "lazy_static") (req "*") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "*") (default-features #t) (kind 0)) (crate-dep (name "tar-sys") (req "*") (default-features #t) (kind 0)) (crate-dep (name "temporary") (req "*") (default-features #t) (kind 2)))) (hash "0bisvzm9hb1vkzxacr392560i6avg8j76r1vxrl4av6c6lc5qz98")))

(define-public crate-tape-0.4 (crate (name "tape") (vers "0.4.0") (deps (list (crate-dep (name "lazy_static") (req "*") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "*") (default-features #t) (kind 0)) (crate-dep (name "tar-sys") (req "*") (default-features #t) (kind 0)) (crate-dep (name "temporary") (req "*") (default-features #t) (kind 2)))) (hash "1hwy4j8bn21s8j5si00r8hp4k5lmcf3bc67wd88x4v15sckqlp38")))

(define-public crate-tape-0.4 (crate (name "tape") (vers "0.4.1") (deps (list (crate-dep (name "lazy_static") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "tar-sys") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "temporary") (req "^0.5") (default-features #t) (kind 2)))) (hash "04ccf1k0zlj725gxqfnv8dvcbmsgf89gi84x17xgvb9kf4dmrbya")))

(define-public crate-tape-0.5 (crate (name "tape") (vers "0.5.0") (deps (list (crate-dep (name "lazy_static") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "tar-sys") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "temporary") (req "^0.6") (default-features #t) (kind 2)))) (hash "1z2kh3321bsp9w5wrbg1px1llm6x4x7nj2d30rx59kzpil8hspq6")))

(define-public crate-tape-0.5 (crate (name "tape") (vers "0.5.1") (deps (list (crate-dep (name "lazy_static") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "tar-sys") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "temporary") (req "^0.6") (default-features #t) (kind 2)))) (hash "04s3nmgyq4wnyjasjvzr2wxshydx31alkdw8ld386x3aacv2xxd7")))

(define-public crate-tapedeck-0.1 (crate (name "tapedeck") (vers "0.1.0") (deps (list (crate-dep (name "anyhow") (req "^1.0.38") (default-features #t) (kind 0)) (crate-dep (name "bytes") (req "^0.6.0") (default-features #t) (kind 0)) (crate-dep (name "chrono") (req "^0.4.19") (default-features #t) (kind 0)) (crate-dep (name "crossterm") (req "^0.18.2") (features (quote ("event-stream"))) (default-features #t) (kind 0)) (crate-dep (name "futures") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2.81") (default-features #t) (kind 0)) (crate-dep (name "libpulse-binding") (req "^2.23.0") (default-features #t) (kind 0)) (crate-dep (name "libpulse-simple-binding") (req "^2.23.0") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.14") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.11") (default-features #t) (kind 2)) (crate-dep (name "mime_guess") (req "^2.0.3") (default-features #t) (kind 0)) (crate-dep (name "pest") (req "^2.1.3") (default-features #t) (kind 0)) (crate-dep (name "pest_derive") (req "^2.1.0") (default-features #t) (kind 0)) (crate-dep (name "pls") (req "^0.2.2") (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.11.1") (default-features #t) (kind 0)) (crate-dep (name "rusqlite") (req "^0.25.3") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.3.20") (default-features #t) (kind 0)) (crate-dep (name "tempfile") (req "^3.2.0") (default-features #t) (kind 2)) (crate-dep (name "tokio") (req "^1.4") (features (quote ("full"))) (default-features #t) (kind 0)) (crate-dep (name "tokio-stream") (req "^0.1.4") (default-features #t) (kind 0)) (crate-dep (name "tracing") (req "^0.1.22") (features (quote ("log"))) (default-features #t) (kind 0)) (crate-dep (name "tracing-appender") (req "^0.1.2") (default-features #t) (kind 0)) (crate-dep (name "tracing-log") (req "^0.1.1") (default-features #t) (kind 0)) (crate-dep (name "tracing-subscriber") (req "^0.2.15") (default-features #t) (kind 0)) (crate-dep (name "unicode-width") (req "^0.1.8") (default-features #t) (kind 0)) (crate-dep (name "url") (req "^2.2.0") (default-features #t) (kind 0)) (crate-dep (name "walkdir") (req "^2.3.1") (default-features #t) (kind 0)))) (hash "1v1f240z703xx27sga5p9aa8i2h5dywb66bqvakdgf07dw5hdw87")))

(define-public crate-taper-0.1 (crate (name "taper") (vers "0.1.0") (deps (list (crate-dep (name "bincode") (req "^1.3") (default-features #t) (kind 0)) (crate-dep (name "flume") (req "^0.10") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (kind 0)) (crate-dep (name "tokio") (req "^1") (features (quote ("rt" "net" "io-util"))) (optional #t) (default-features #t) (kind 0)))) (hash "0p0yr1m0iazfq1qwz2mrvz9ji8q4jszsqbqqs98s1kqplss8sdl3") (features (quote (("default") ("async" "tokio")))) (yanked #t)))

(define-public crate-taper-0.2 (crate (name "taper") (vers "0.2.0") (deps (list (crate-dep (name "bincode") (req "^1.3") (default-features #t) (kind 0)) (crate-dep (name "flume") (req "^0.10") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (kind 0)) (crate-dep (name "tokio") (req "^1") (features (quote ("rt" "net" "io-util"))) (optional #t) (default-features #t) (kind 0)))) (hash "19giajngfn96zwfyb92n5xc9rl2yfhxkvaf19w81p9fvpxi09vw0") (features (quote (("default") ("async" "tokio")))) (yanked #t)))

(define-public crate-taper-0.2 (crate (name "taper") (vers "0.2.1") (deps (list (crate-dep (name "bincode") (req "^1.3") (default-features #t) (kind 0)) (crate-dep (name "flume") (req "^0.10") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (kind 0)) (crate-dep (name "tokio") (req "^1") (features (quote ("rt" "net" "io-util"))) (optional #t) (default-features #t) (kind 0)))) (hash "012jlcc3rb3xkh4pf4sp0xvp54xjxlf97a83g1880iywcvfvxb5n") (features (quote (("default") ("async" "tokio")))) (yanked #t)))

(define-public crate-taper-0.2 (crate (name "taper") (vers "0.2.2") (deps (list (crate-dep (name "bincode") (req "^1.3") (default-features #t) (kind 0)) (crate-dep (name "flume") (req "^0.10") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (kind 0)) (crate-dep (name "tokio") (req "^1") (features (quote ("rt" "net" "io-util"))) (optional #t) (default-features #t) (kind 0)))) (hash "1xaqxq1k44ax86mgcarmxg983hi05ig600grbm160pxpp5a9v5hy") (features (quote (("default") ("async" "tokio")))) (yanked #t)))

(define-public crate-taper-0.2 (crate (name "taper") (vers "0.2.3") (deps (list (crate-dep (name "bincode") (req "^1.3") (default-features #t) (kind 0)) (crate-dep (name "flume") (req "^0.10") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (kind 0)) (crate-dep (name "tokio") (req "^1") (features (quote ("rt" "net" "io-util"))) (optional #t) (default-features #t) (kind 0)))) (hash "0g2rkj3v30p9856zwq9skk6i5gkirsslsrbhlpdvhr54c5wz2x2a") (features (quote (("default") ("async" "tokio")))) (yanked #t)))

(define-public crate-taper-0.2 (crate (name "taper") (vers "0.2.4") (deps (list (crate-dep (name "bincode") (req "^1.3") (default-features #t) (kind 0)) (crate-dep (name "flume") (req "^0.10") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (kind 0)) (crate-dep (name "tokio") (req "^1") (features (quote ("rt" "net" "io-util"))) (optional #t) (default-features #t) (kind 0)))) (hash "0ly5cbmfbn21ij726ls6w06z92g152js78h6rq3v9xmhbh06krm9") (features (quote (("default") ("async" "tokio")))) (yanked #t)))

(define-public crate-tapestry-0.1 (crate (name "tapestry") (vers "0.1.0") (deps (list (crate-dep (name "rand") (req "^0.8.3") (default-features #t) (kind 0)))) (hash "069rl0z4wq4f6aqqn06x0xr8niv497pjlz04kqm03z3f16rarb1d")))

