(define-module (crates-io ta li) #:use-module (crates-io))

(define-public crate-talib-0.1 (crate (name "talib") (vers "0.1.0") (deps (list (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "talib-sys") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "17qbx38h8lacq86iyrzs3zk181k75zvn66p2z13rc40nkdmqair1")))

(define-public crate-talib-sys-0.1 (crate (name "talib-sys") (vers "0.1.0") (deps (list (crate-dep (name "bindgen") (req "^0.69.1") (default-features #t) (kind 1)) (crate-dep (name "flate2") (req "^1.0.26") (default-features #t) (kind 1)) (crate-dep (name "fs_extra") (req "^1.3.0") (default-features #t) (kind 1)) (crate-dep (name "openssl") (req "^0.10") (features (quote ("vendored"))) (default-features #t) (kind 1)) (crate-dep (name "reqwest") (req "^0.11") (features (quote ("blocking"))) (default-features #t) (kind 1)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "tar") (req "^0.4.38") (default-features #t) (kind 1)) (crate-dep (name "zip-extract") (req "^0.1.2") (default-features #t) (kind 1)))) (hash "16wbbvad559glnyc4mjrcahj2fm8zb0wpczjlpy1wr0n5hv710z6")))

(define-public crate-talipp-0.1 (crate (name "talipp") (vers "0.1.0") (hash "0l5lqwjkqygjy7ajysgm916zgc550jbqiiv4qpxmnp350vmhmwrr")))

(define-public crate-talisman-0.0.0 (crate (name "talisman") (vers "0.0.0") (hash "07rkscmmxs6dv17w0636jrqwa6x4vh9py8dykfp7gxd5g8qg7wzn")))

