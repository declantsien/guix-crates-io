(define-module (crates-io ta vo) #:use-module (crates-io))

(define-public crate-tavol-0.1 (crate (name "tavol") (vers "0.1.0") (deps (list (crate-dep (name "aes") (req "^0.8.2") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^4.2.7") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "error-stack") (req "^0.3.1") (default-features #t) (kind 0)) (crate-dep (name "sha2") (req "^0.10.6") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.40") (default-features #t) (kind 0)))) (hash "0icp008xk81ka14j80ps8gjfiwinb26ybjb883jppav6b74b1x5m") (yanked #t)))

