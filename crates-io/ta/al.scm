(define-module (crates-io ta al) #:use-module (crates-io))

(define-public crate-taalika-0.1 (crate (name "taalika") (vers "0.1.5") (deps (list (crate-dep (name "colored") (req "^2.0.0") (default-features #t) (kind 2)) (crate-dep (name "strip-ansi-escapes") (req "^0.1.1") (default-features #t) (kind 0)) (crate-dep (name "unicode-width") (req "^0.1.5") (optional #t) (default-features #t) (kind 0)))) (hash "1v3hr78xpldv87vmw4x8z29nwrs7hbndxi5n4s38sksx8xy661cr") (features (quote (("default" "unicode-width")))) (yanked #t)))

(define-public crate-taalika-0.1 (crate (name "taalika") (vers "0.1.6") (deps (list (crate-dep (name "colored") (req "^2.0.0") (default-features #t) (kind 2)) (crate-dep (name "strip-ansi-escapes") (req "^0.1.1") (default-features #t) (kind 0)) (crate-dep (name "unicode-width") (req "^0.1.5") (optional #t) (default-features #t) (kind 0)))) (hash "0rjhwj3cbw2903h3c20w57fsfs08p4h2mvqnzn7sqz4g8jicd183") (features (quote (("default" "unicode-width")))) (yanked #t)))

