(define-module (crates-io ta iz) #:use-module (crates-io))

(define-public crate-taizen-0.1 (crate (name "taizen") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^2.32.0") (default-features #t) (kind 0)) (crate-dep (name "cursive") (req "^0.9.2") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.0.2") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.9.5") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.33") (default-features #t) (kind 0)) (crate-dep (name "urlencoding") (req "^1.0.0") (default-features #t) (kind 0)))) (hash "0sskznsp9sgv6xx6d97nm4kxss3aq3gc0vx29j40xai5bkk8g7kb")))

