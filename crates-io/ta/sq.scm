(define-module (crates-io ta sq) #:use-module (crates-io))

(define-public crate-tasque-0.0.1 (crate (name "tasque") (vers "0.0.1") (deps (list (crate-dep (name "num_cpus") (req "^1") (default-features #t) (kind 0)))) (hash "14g051pjy01nsns4xf6221ljxjkzgayyig12y79sppv9jdfjkw10")))

(define-public crate-tasque-0.1 (crate (name "tasque") (vers "0.1.0") (deps (list (crate-dep (name "num_cpus") (req "^1") (default-features #t) (kind 0)))) (hash "1b3mmhpd8k39gyns37ks33f9rly4gqybzbnjgdiyprz78dm3kx0z")))

(define-public crate-tasque-0.1 (crate (name "tasque") (vers "0.1.1") (deps (list (crate-dep (name "num_cpus") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "prometrics") (req "^0.1") (default-features #t) (kind 0)))) (hash "064z2ryg05cva840f3l6vzp20k9lnfmqkvw7vzbqdpihv4zdia06")))

(define-public crate-tasque-0.1 (crate (name "tasque") (vers "0.1.2") (deps (list (crate-dep (name "num_cpus") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "prometrics") (req "^0.1") (default-features #t) (kind 0)))) (hash "0l7vb3k2sj8jwihqcqnyfan90mwyg71npyfssc3l057afa7rp8lp")))

(define-public crate-tasque-0.1 (crate (name "tasque") (vers "0.1.3") (deps (list (crate-dep (name "num_cpus") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "prometrics") (req "^0.1") (default-features #t) (kind 0)))) (hash "1qjhz9n4ih6aida9ifahw4ljaqzkryfxjfhanzinhv5pwqarl5sh")))

(define-public crate-tasque-0.1 (crate (name "tasque") (vers "0.1.4") (deps (list (crate-dep (name "num_cpus") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "prometrics") (req "^0.1") (default-features #t) (kind 0)))) (hash "038d98i5qkswmfvac0331528mf40xsfak5w8i3z57xwnl3c5sicf")))

(define-public crate-tasque-0.1 (crate (name "tasque") (vers "0.1.5") (deps (list (crate-dep (name "num_cpus") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "prometrics") (req "^0.1") (default-features #t) (kind 0)))) (hash "0lspflkx562jd5s19hwa10i7frdaqc4ipdpj5ivixcgbs40m029q")))

