(define-module (crates-io ta g_) #:use-module (crates-io))

(define-public crate-tag_nominatim-0.1 (crate (name "tag_nominatim") (vers "0.1.0") (deps (list (crate-dep (name "reqwest") (req "^0.11") (features (quote ("json"))) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.20") (features (quote ("full"))) (default-features #t) (kind 2)) (crate-dep (name "tokio-test") (req "^0.4.3") (default-features #t) (kind 2)) (crate-dep (name "url") (req "^2.4") (default-features #t) (kind 0)))) (hash "114lp3ig6xkzc06yraiky0sdkpka7a8vy8ki5ksr2zqjmkjlckgs") (features (quote (("rustls" "reqwest/rustls-tls") ("default" "reqwest/default-tls"))))))

(define-public crate-tag_password-0.1 (crate (name "tag_password") (vers "0.1.0") (deps (list (crate-dep (name "argon2") (req "^0.5.2") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "rand_core") (req "^0.6.4") (features (quote ("getrandom"))) (optional #t) (default-features #t) (kind 0)))) (hash "0388l5z55s2s76rdkjdz1v8nrfw2x6i7ljrgbl64i5vzlk84y5v5") (features (quote (("default" "argon2")))) (v 2) (features2 (quote (("argon2" "dep:argon2" "dep:rand_core"))))))

(define-public crate-tag_password-0.1 (crate (name "tag_password") (vers "0.1.1") (deps (list (crate-dep (name "argon2") (req "^0.5.2") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "rand_core") (req "^0.6.4") (features (quote ("getrandom"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.193") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)))) (hash "1h6p5bzgk9ay6bpfjy0vwd3vxp9rd4ni8h3jfg4rmp2llmz6cyqq") (features (quote (("default" "argon2" "serde")))) (v 2) (features2 (quote (("serde" "dep:serde") ("argon2" "dep:argon2" "dep:rand_core"))))))

(define-public crate-tag_password-0.1 (crate (name "tag_password") (vers "0.1.2") (deps (list (crate-dep (name "argon2") (req "^0.5.2") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "rand_core") (req "^0.6.4") (features (quote ("getrandom"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.193") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)))) (hash "0rb13094cl739xzsh6l2w7m5fjdj7dsj6dmls4700p3pr4vp0lj8") (features (quote (("default" "argon2" "serde")))) (v 2) (features2 (quote (("serde" "dep:serde") ("argon2" "dep:argon2" "dep:rand_core"))))))

(define-public crate-tag_password-0.1 (crate (name "tag_password") (vers "0.1.3-pre1") (deps (list (crate-dep (name "argon2") (req "^0.5.2") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "async-graphql") (req "^6.0.11") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "rand_core") (req "^0.6.4") (features (quote ("getrandom"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.193") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)))) (hash "10y547c906j99f87cyy1bjcx297ra20cl5j5c9wagsnzcjj7maz7") (features (quote (("default" "argon2" "serde")))) (v 2) (features2 (quote (("serde" "dep:serde") ("graphql" "dep:async-graphql") ("argon2" "dep:argon2" "dep:rand_core"))))))

(define-public crate-tag_password-0.1 (crate (name "tag_password") (vers "0.1.3-pre2") (deps (list (crate-dep (name "argon2") (req "^0.5.2") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "async-graphql") (req "^6.0.11") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "rand_core") (req "^0.6.4") (features (quote ("getrandom"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.193") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)))) (hash "193381jhxhw2g1qrzbbgy1qry52syd0xyms467f2hpvjv4531ljb") (features (quote (("default" "argon2" "serde")))) (v 2) (features2 (quote (("serde" "dep:serde") ("graphql" "dep:async-graphql") ("argon2" "dep:argon2" "dep:rand_core"))))))

(define-public crate-tag_password-0.1 (crate (name "tag_password") (vers "0.1.3-pre3") (deps (list (crate-dep (name "argon2") (req "^0.5.2") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "async-graphql") (req "^6.0.11") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "rand_core") (req "^0.6.4") (features (quote ("getrandom"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.193") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)))) (hash "1knaqrh7aapzl2zfzvz0l4zjhpz3s4z0p0s60rknrlqhna4lyxvj") (features (quote (("default" "argon2" "serde")))) (v 2) (features2 (quote (("serde" "dep:serde") ("graphql" "dep:async-graphql") ("argon2" "dep:argon2" "dep:rand_core"))))))

(define-public crate-tag_password-0.1 (crate (name "tag_password") (vers "0.1.3") (deps (list (crate-dep (name "argon2") (req "^0.5.2") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "async-graphql") (req "^6.0.11") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "rand_core") (req "^0.6.4") (features (quote ("getrandom"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.193") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)))) (hash "00lkzmyn5zv0fb1jmfbv5a1snff2g9mwxzx0s9hxhjrqmn63rk1v") (features (quote (("default" "argon2" "serde")))) (v 2) (features2 (quote (("serde" "dep:serde") ("graphql" "dep:async-graphql") ("argon2" "dep:argon2" "dep:rand_core"))))))

(define-public crate-tag_safe-0.0.1 (crate (name "tag_safe") (vers "0.0.1") (deps (list (crate-dep (name "compiletest_rs") (req "*") (default-features #t) (kind 2)))) (hash "0xaqm5aihi8lwlmkqqsmpk8xx7k17icdh6if9ccilysq3jqic457")))

(define-public crate-tag_safe-0.0.3 (crate (name "tag_safe") (vers "0.0.3") (deps (list (crate-dep (name "compiletest_rs") (req "*") (default-features #t) (kind 2)))) (hash "1h2m2xma6y9280zbv7b207lxm6290hnl9yrkmrrpmdlwdfbcj441")))

(define-public crate-tag_safe-0.0.4 (crate (name "tag_safe") (vers "0.0.4") (deps (list (crate-dep (name "compiletest_rs") (req "*") (default-features #t) (kind 2)))) (hash "17as0q3spr0app1l6g4vrh08k2r1p06ww91p7121j0hmkd5q1dal")))

(define-public crate-tag_safe-0.0.5 (crate (name "tag_safe") (vers "0.0.5") (deps (list (crate-dep (name "compiletest_rs") (req "*") (default-features #t) (kind 2)))) (hash "0xajdx3j00qx6i9x3wlhl6asb7aqv47jnnlsfv3ba5jrxj0arsiy")))

(define-public crate-tag_safe-0.0.6 (crate (name "tag_safe") (vers "0.0.6") (deps (list (crate-dep (name "compiletest_rs") (req "^0.0") (default-features #t) (kind 2)))) (hash "1b3iqsvqg7gb1v6qwajqyiyh0860rgzsy7hvvjgc5bcqhphx5pv3")))

(define-public crate-tag_safe-0.0.7 (crate (name "tag_safe") (vers "0.0.7") (deps (list (crate-dep (name "compiletest_rs") (req "^0.1") (default-features #t) (kind 2)))) (hash "1mvfvcsvdlws280v6b4846dphdixl1lmbx779aw8bqaqamfc6hk3")))

(define-public crate-tag_safe-0.0.8 (crate (name "tag_safe") (vers "0.0.8") (deps (list (crate-dep (name "compiletest_rs") (req "^0.1") (default-features #t) (kind 2)))) (hash "013cda272yk5gvshw2dnidldmrfsl1wjndqkcazhfhnkv146fqpf")))

(define-public crate-tag_safe-0.1 (crate (name "tag_safe") (vers "0.1.0") (deps (list (crate-dep (name "compiletest_rs") (req "^0.1") (default-features #t) (kind 2)))) (hash "0pg4v7drmgrn9cx2rqgb683shl725nf1l71q6kiwvl4njbchfiir")))

(define-public crate-tag_safe-0.1 (crate (name "tag_safe") (vers "0.1.1") (deps (list (crate-dep (name "compiletest_rs") (req "^0.2") (default-features #t) (kind 2)))) (hash "1knqqlpj411kxdqf7k2b164d0ipvhgvwdbm01pvg6b6qfpdlb9jb")))

(define-public crate-tag_safe-0.2 (crate (name "tag_safe") (vers "0.2.0") (deps (list (crate-dep (name "compiletest_rs") (req "^0.2") (default-features #t) (kind 2)) (crate-dep (name "lazy_static") (req "^0.2.2") (default-features #t) (kind 0)))) (hash "1wlpfwhmlwcpp3r89pdy56qr9pv65i4kcnks2dkqcfq6hxm8hq81")))

(define-public crate-tag_safe-0.2 (crate (name "tag_safe") (vers "0.2.1") (deps (list (crate-dep (name "compiletest_rs") (req "^0.2") (default-features #t) (kind 2)) (crate-dep (name "lazy_static") (req "^0.2.2") (default-features #t) (kind 0)))) (hash "0skzcnpbyhk7bz0hhgisivl03a9hg48y011vsvydmzxz8d3vc5qr")))

(define-public crate-tag_safe-0.2 (crate (name "tag_safe") (vers "0.2.2") (deps (list (crate-dep (name "compiletest_rs") (req "^0.2") (default-features #t) (kind 2)) (crate-dep (name "lazy_static") (req "^0.2.2") (default-features #t) (kind 0)))) (hash "0cajks3wazgl0y3ijlffj5haci7752jiccihvrkrrdb1qf84za9y")))

(define-public crate-tag_safe-0.2 (crate (name "tag_safe") (vers "0.2.3") (deps (list (crate-dep (name "compiletest_rs") (req "^0.2") (default-features #t) (kind 2)) (crate-dep (name "lazy_static") (req "^0.2.2") (default-features #t) (kind 0)))) (hash "1vhl1y4im3ddvp6glwyygw7zd3ssrrsh33kxccfv42zn3z16jz0w")))

(define-public crate-tag_safe-0.2 (crate (name "tag_safe") (vers "0.2.4") (deps (list (crate-dep (name "compiletest_rs") (req "^0.2") (default-features #t) (kind 2)) (crate-dep (name "lazy_static") (req "^0.2.2") (default-features #t) (kind 0)))) (hash "0lpldbbszfw81vk48m4kwlgzj5vlhxx3fqvg2gzhxbyaa9f8b1gx")))

(define-public crate-tag_safe-0.2 (crate (name "tag_safe") (vers "0.2.5") (deps (list (crate-dep (name "compiletest_rs") (req "^0.2") (default-features #t) (kind 2)) (crate-dep (name "lazy_static") (req "^0.2.2") (default-features #t) (kind 0)))) (hash "1wlrsciyxz5w4gs7a6idi77n4acr7v8vqqlzaigv5ywr6pwxr3kg")))

(define-public crate-tag_safe-0.2 (crate (name "tag_safe") (vers "0.2.6") (deps (list (crate-dep (name "compiletest_rs") (req "^0.2") (default-features #t) (kind 2)) (crate-dep (name "lazy_static") (req "^0.2.2") (default-features #t) (kind 0)))) (hash "01rm5y64zcsrzhnrv7zkfbj7bgd73g67n8qn1lnd5pfqbdh0gk74")))

(define-public crate-tag_safe-0.2 (crate (name "tag_safe") (vers "0.2.7") (deps (list (crate-dep (name "compiletest_rs") (req "^0.2") (default-features #t) (kind 2)) (crate-dep (name "lazy_static") (req "^0.2.2") (default-features #t) (kind 0)))) (hash "1z7i720wh7pjnlzj3765an26j7zgrdx23igky51d66hhrmbxisdr")))

(define-public crate-tag_safe-0.2 (crate (name "tag_safe") (vers "0.2.8") (deps (list (crate-dep (name "compiletest_rs") (req "^0.2") (default-features #t) (kind 2)) (crate-dep (name "lazy_static") (req "^0.2.2") (default-features #t) (kind 0)))) (hash "0yrb46ks70kv74rp87w6v3gxd94b7s3jpzn281x0q518m0m5y4iv")))

(define-public crate-tag_safe-0.2 (crate (name "tag_safe") (vers "0.2.11") (deps (list (crate-dep (name "compiletest_rs") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "lazy_static") (req "^0.2.2") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.3") (default-features #t) (kind 0)))) (hash "020q7ypfkc2qngrqcb52j7kl87wv1h1dpyg0x0iblls3w29bqsr9")))

(define-public crate-tag_safe-0.2 (crate (name "tag_safe") (vers "0.2.12") (deps (list (crate-dep (name "compiletest_rs") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "lazy_static") (req "^0.2.2") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.3") (default-features #t) (kind 0)))) (hash "1rvcr9d82zqy30njy3z0nixyb6pj7h0q5nxlhh7ds703m3nf4yig")))

(define-public crate-tag_safe-0.2 (crate (name "tag_safe") (vers "0.2.13") (deps (list (crate-dep (name "compiletest_rs") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "lazy_static") (req "^0.2.2") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.3") (default-features #t) (kind 0)))) (hash "0nxydj3kq7vsqkmpbzfxsbgfj0jnjhxmdjcak38kj29gf2gh17sx")))

(define-public crate-tag_safe-0.2 (crate (name "tag_safe") (vers "0.2.14") (deps (list (crate-dep (name "compiletest_rs") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "lazy_static") (req "^0.2.2") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.3") (default-features #t) (kind 0)))) (hash "0gsal82gv2l809pwpalfk61s817bgd1qkc32mlf57q8a1qd2y162")))

(define-public crate-tag_safe-0.2 (crate (name "tag_safe") (vers "0.2.15") (deps (list (crate-dep (name "compiletest_rs") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "lazy_static") (req "^0.2.2") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.3") (default-features #t) (kind 0)))) (hash "0269pz1djdmfqd0gfj25p8xry523h9pxbhqw78qr3wwjcn96wdhw")))

(define-public crate-tag_safe-0.2 (crate (name "tag_safe") (vers "0.2.16") (deps (list (crate-dep (name "compiletest_rs") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "lazy_static") (req "^0.2.2") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.3") (default-features #t) (kind 0)))) (hash "0dkf3yh7h2bni2vncxf08xicrs7c85k0rw62ssbrd7dy7pwfcn9q")))

(define-public crate-tag_safe-0.2 (crate (name "tag_safe") (vers "0.2.17") (deps (list (crate-dep (name "compiletest_rs") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "lazy_static") (req "^0.2.2") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.3") (default-features #t) (kind 0)))) (hash "10fwb1rjil9nlb1wgr4h8j85n79l3h7ysxy2h94vm8fpl2dnlpka")))

(define-public crate-tag_safe-0.2 (crate (name "tag_safe") (vers "0.2.18") (deps (list (crate-dep (name "compiletest_rs") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "lazy_static") (req "^0.2.2") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.3") (default-features #t) (kind 0)))) (hash "1dmvd2v4n2b9pfj51ljlk8rhh3z2if5qfjkirm6gzvc7ska2h7by")))

(define-public crate-tag_safe-0.2 (crate (name "tag_safe") (vers "0.2.19") (deps (list (crate-dep (name "compiletest_rs") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "lazy_static") (req "^0.2.2") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.3") (default-features #t) (kind 0)))) (hash "1076bndqqsasjapf5h81xjq9wzd101m49x3cxzs0slm0dqbpf4gi")))

(define-public crate-tag_safe-0.2 (crate (name "tag_safe") (vers "0.2.20") (deps (list (crate-dep (name "compiletest_rs") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "lazy_static") (req "^0.2.2") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.3") (default-features #t) (kind 0)))) (hash "0n76vfhrscjlv6xw6fvz2p0blkq0x9jvx1sa3zhkm30n774avsv7")))

(define-public crate-tag_safe-0.2 (crate (name "tag_safe") (vers "0.2.21") (deps (list (crate-dep (name "compiletest_rs") (req "^0.3") (features (quote ("stable"))) (kind 2)) (crate-dep (name "lazy_static") (req "^0.2.2") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.3") (default-features #t) (kind 0)))) (hash "1zb68iqnz09c0q4kha9qdkibx36bja0a6fnz5wdy3w1x38xfkdgb")))

(define-public crate-tag_safe-0.2 (crate (name "tag_safe") (vers "0.2.22") (deps (list (crate-dep (name "compiletest_rs") (req "^0.3") (features (quote ("stable"))) (kind 2)) (crate-dep (name "lazy_static") (req "^0.2.2") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.3") (default-features #t) (kind 0)))) (hash "161m6mjhq5qlyb6xjz90s6b9dmdwp1nz7iiv3cbhk3hkvxzk6li0")))

(define-public crate-tag_safe-0.2 (crate (name "tag_safe") (vers "0.2.23") (deps (list (crate-dep (name "compiletest_rs") (req "^0.3") (features (quote ("stable"))) (kind 2)) (crate-dep (name "lazy_static") (req "^0.2.2") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.3") (default-features #t) (kind 0)))) (hash "199h0r2siw1zbb3i4vbmfk0180wwxg9d61mvbd4h0wg9jwq1ahpi")))

(define-public crate-tag_safe-0.2 (crate (name "tag_safe") (vers "0.2.24") (deps (list (crate-dep (name "compiletest_rs") (req "^0.3") (features (quote ("stable"))) (kind 2)) (crate-dep (name "lazy_static") (req "^0.2.2") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.3") (default-features #t) (kind 0)))) (hash "02bj9nb12hszmx6fx0g4001p3yky916icrikrg77idbly6j74f3r")))

(define-public crate-tag_safe-0.2 (crate (name "tag_safe") (vers "0.2.25") (deps (list (crate-dep (name "compiletest_rs") (req "^0.3") (features (quote ("stable"))) (kind 2)) (crate-dep (name "lazy_static") (req "^0.2.2") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.3") (default-features #t) (kind 0)))) (hash "1c3ndi6cip2bba2m62vr1az50nny1biwjaxwjcrg9sdravykb4dm")))

