(define-module (crates-io ta ic) #:use-module (crates-io))

(define-public crate-taichi-0.0.1 (crate (name "taichi") (vers "0.0.1") (hash "1hcpg366zav5lw8rxa4j1zj7zqzwj8smzdiwbc4a4ypplbkx3719")))

(define-public crate-taichi-runtime-0.0.1 (crate (name "taichi-runtime") (vers "0.0.1+1.3.0") (deps (list (crate-dep (name "bitflags") (req "^1.3") (default-features #t) (kind 0)) (crate-dep (name "taichi-sys") (req "^0.0.3") (default-features #t) (kind 0)))) (hash "0drkj0pfxi86bn9yp8y2f4ir0cp914jlq1fph2fh0c6aic8am6pk")))

(define-public crate-taichi-runtime-0.0.2 (crate (name "taichi-runtime") (vers "0.0.2+1.3.0") (deps (list (crate-dep (name "bitflags") (req "^1.3") (default-features #t) (kind 0)) (crate-dep (name "taichi-sys") (req "^0.0.4") (default-features #t) (kind 0)))) (hash "17mvg919i92xs2jhhgggpx5xx4n17wfl4ypsbi58k8r0r2js14mm")))

(define-public crate-taichi-runtime-0.0.3 (crate (name "taichi-runtime") (vers "0.0.3+1.3.0") (deps (list (crate-dep (name "bitflags") (req "^1.3") (default-features #t) (kind 0)) (crate-dep (name "taichi-sys") (req "^0.0.5") (default-features #t) (kind 0)))) (hash "07ynyi2jcji411mjd4pxlds9ibpvrvd4q68xg7incp8w26m6bvy1")))

(define-public crate-taichi-runtime-0.0.4 (crate (name "taichi-runtime") (vers "0.0.4+1.3.0") (deps (list (crate-dep (name "bitflags") (req "^1.3") (default-features #t) (kind 0)) (crate-dep (name "taichi-sys") (req "^0.0.5") (default-features #t) (kind 0)))) (hash "0iqlhvnmnc05h0frjmh78rj05x505j8zl46gsswn1xpk83dlqbwm")))

(define-public crate-taichi-runtime-0.0.5 (crate (name "taichi-runtime") (vers "0.0.5+1.3.0") (deps (list (crate-dep (name "bitflags") (req "^1.3") (default-features #t) (kind 0)) (crate-dep (name "taichi-sys") (req "^0.0.5") (default-features #t) (kind 0)))) (hash "12d66ifpv6f5ngx4ikwh155r4cifn0d6hzvca76jm2xahhj3b5c4")))

(define-public crate-taichi-runtime-0.0.7 (crate (name "taichi-runtime") (vers "0.0.7+1.3.0") (deps (list (crate-dep (name "bitflags") (req "^1.3") (default-features #t) (kind 0)) (crate-dep (name "taichi-sys") (req "^0.0.8") (default-features #t) (kind 0)))) (hash "1jq4xnci7d5wgnk68cjaii4av77xngpcfn4da4gwmbs04q5y08wb")))

(define-public crate-taichi-runtime-0.0.9 (crate (name "taichi-runtime") (vers "0.0.9+1.6.0") (deps (list (crate-dep (name "bitflags") (req "^1.3") (default-features #t) (kind 0)) (crate-dep (name "taichi-sys") (req "^0.0.9") (default-features #t) (kind 0)))) (hash "0yr6vv8vrpqj06cbqqph27iqnkfhiykhzl7y78y38izrnps06zdk")))

(define-public crate-taichi-sys-0.0.1 (crate (name "taichi-sys") (vers "0.0.1+1.3.0") (deps (list (crate-dep (name "bitflags") (req "^1.3") (default-features #t) (kind 0)) (crate-dep (name "dotenvy") (req "^0.15") (default-features #t) (kind 1)))) (hash "0kif0dyp22yqwh7l8zrrl8azzqahw9dh976i1yg70pv7z5gpbr7w") (links "taichi_c_api")))

(define-public crate-taichi-sys-0.0.2 (crate (name "taichi-sys") (vers "0.0.2+1.3.0") (deps (list (crate-dep (name "bitflags") (req "^1.3") (default-features #t) (kind 0)) (crate-dep (name "dotenvy") (req "^0.15") (default-features #t) (kind 1)))) (hash "10sg9500i24aky3056bkac7lja4kkjzr10z7ps8qrszxmij9ky6n") (links "taichi_c_api")))

(define-public crate-taichi-sys-0.0.3 (crate (name "taichi-sys") (vers "0.0.3+1.3.0") (deps (list (crate-dep (name "bitflags") (req "^1.3") (default-features #t) (kind 0)) (crate-dep (name "dotenvy") (req "^0.15") (default-features #t) (kind 1)))) (hash "1l3cbdk6038pb5dzvsahacbipv568gjgm1x1a00ij6wz2hj8vn03") (links "taichi_c_api")))

(define-public crate-taichi-sys-0.0.4 (crate (name "taichi-sys") (vers "0.0.4+1.3.0") (deps (list (crate-dep (name "bitflags") (req "^1.3") (default-features #t) (kind 0)) (crate-dep (name "dotenvy") (req "^0.15") (default-features #t) (kind 1)))) (hash "0r8pf7i5bh5x4avr45arwgyy4839crvgl2yhhqyvzdrna4jji7rc") (links "taichi_c_api")))

(define-public crate-taichi-sys-0.0.5 (crate (name "taichi-sys") (vers "0.0.5+1.3.0") (deps (list (crate-dep (name "bitflags") (req "^1.3") (default-features #t) (kind 0)) (crate-dep (name "dotenvy") (req "^0.15") (default-features #t) (kind 1)))) (hash "0p2gl9byh5qrf68cifcpfpwjw3wgyvkhf5x8ly7irs3vdb9rjfqm") (links "taichi_c_api")))

(define-public crate-taichi-sys-0.0.6 (crate (name "taichi-sys") (vers "0.0.6+1.3.0") (deps (list (crate-dep (name "bitflags") (req "^1.3") (default-features #t) (kind 0)) (crate-dep (name "dotenvy") (req "^0.15") (default-features #t) (kind 1)))) (hash "0jykq9hl0ny0ys4xa44sv85xr2vs3y8yd50gsqz7jk1as3vyd0a1") (links "taichi_c_api")))

(define-public crate-taichi-sys-0.0.7 (crate (name "taichi-sys") (vers "0.0.7+1.3.0") (deps (list (crate-dep (name "bitflags") (req "^1.3") (default-features #t) (kind 0)) (crate-dep (name "dotenvy") (req "^0.15") (default-features #t) (kind 1)))) (hash "13pq3ziqc78c3myy0cds3nsf71gylxql7j8hbx2hrm8m79mbdykn") (links "taichi_c_api")))

(define-public crate-taichi-sys-0.0.8 (crate (name "taichi-sys") (vers "0.0.8+1.3.0") (deps (list (crate-dep (name "bitflags") (req "^1.3") (default-features #t) (kind 0)) (crate-dep (name "dotenvy") (req "^0.15") (default-features #t) (kind 1)))) (hash "0bsi22p7q9f3l4m4kajba7hriji5izpbhjcglby6l49899529zrf") (links "taichi_c_api")))

(define-public crate-taichi-sys-0.0.9 (crate (name "taichi-sys") (vers "0.0.9+1.6.0") (deps (list (crate-dep (name "bitflags") (req "^1.3") (default-features #t) (kind 0)) (crate-dep (name "dotenvy") (req "^0.15") (default-features #t) (kind 1)))) (hash "0l8j31djj6pxklajvsp0imqqpkcbsfyac54vxazrllirwab1qp0k") (links "taichi_c_api")))

