(define-module (crates-io ta ki) #:use-module (crates-io))

(define-public crate-taki-cli-0.1 (crate (name "taki-cli") (vers "0.1.0") (deps (list (crate-dep (name "anyhow") (req "^1.0.79") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^4.4.18") (default-features #t) (kind 0)))) (hash "1dvkz8zmhabfyfg6bcyz458amrqhihbbfkbqilxc8a9i87pirqsh")))

(define-public crate-taki-cli-0.1 (crate (name "taki-cli") (vers "0.1.1") (deps (list (crate-dep (name "anyhow") (req "^1.0.79") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^4.4.18") (default-features #t) (kind 0)))) (hash "1zyh1a9jv9c4p01r983ajm5q92pkfb2chj4xxjnhi6avkz94g757")))

