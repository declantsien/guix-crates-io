(define-module (crates-io ta kp) #:use-module (crates-io))

(define-public crate-takparse-0.1 (crate (name "takparse") (vers "0.1.0") (hash "0i7axksh408l6k1vkdygxfvzc70hlrf0r065wj37vmbwibbyqdnf")))

(define-public crate-takparse-0.2 (crate (name "takparse") (vers "0.2.0") (hash "0j7aichb5n0ilii2xwg5gj601296js5905cqq6l1qh330iaqk2zz")))

(define-public crate-takparse-0.2 (crate (name "takparse") (vers "0.2.1") (hash "1acmm22nb7rk8h5g85g7k9f4ww2z23g1l08v0j19wyls7ss37k8w")))

(define-public crate-takparse-0.3 (crate (name "takparse") (vers "0.3.0") (hash "1m79vnjgds4cnqhllmscz0zqv8jazn2s4i2bp7i0n9245kl4js56")))

(define-public crate-takparse-0.4 (crate (name "takparse") (vers "0.4.0") (hash "0y31s546pjn6lmzl32knf40pqfvigd7i4hiy9g1yfgjisv4k8f3f")))

(define-public crate-takparse-0.5 (crate (name "takparse") (vers "0.5.0") (hash "0fpwfa9x52fjk4q3bwh8rgam24s05kwb3z71lj8zq7l8w7wsgp3h")))

(define-public crate-takparse-0.5 (crate (name "takparse") (vers "0.5.1") (hash "0w48rx83f6229i9gd3128cm1kr5ncdc4bdmdzb9lflm4z5sn5ry1")))

(define-public crate-takparse-0.5 (crate (name "takparse") (vers "0.5.2") (hash "1v8s0mx79431p83hkgypd7dcmhsikb95g3qpw6wqxwnq8x44zn6p")))

(define-public crate-takparse-0.5 (crate (name "takparse") (vers "0.5.3") (hash "0gvh2q2q6g27cyr6gmgq60c7h7zm2v5cmc0vibyh7k17kjg4q7mb")))

(define-public crate-takparse-0.5 (crate (name "takparse") (vers "0.5.4") (hash "1kbwnks14clfxxljdm35x1g7n5vhzdqm2kdfk8n9fj3rxziary3y")))

(define-public crate-takparse-0.5 (crate (name "takparse") (vers "0.5.5") (hash "0ij81y5ysb92jjvbzms1kpd1i4w0blxb02rynsz8b59jd9ighsv1")))

(define-public crate-takparse-0.6 (crate (name "takparse") (vers "0.6.0") (deps (list (crate-dep (name "chrono") (req "^0.4.26") (default-features #t) (kind 0)))) (hash "0xvn3x3bs5yc0vlxfghsq9nvcz7hvarfdraz7jb6ppwcs54f83ji") (rust-version "1.65")))

