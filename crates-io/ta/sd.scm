(define-module (crates-io ta sd) #:use-module (crates-io))

(define-public crate-tasd-0.1 (crate (name "tasd") (vers "0.1.0") (deps (list (crate-dep (name "chrono") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "crossterm") (req "^0.25") (default-features #t) (kind 0)) (crate-dep (name "dyn-clone") (req "^1") (default-features #t) (kind 0)))) (hash "0n1frjb7p49xxgydxmg9cyzhib6db0m9lw9k2kgb15hg4phqrfyg")))

(define-public crate-tasd-0.1 (crate (name "tasd") (vers "0.1.1") (deps (list (crate-dep (name "chrono") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "crossterm") (req "^0.25") (default-features #t) (kind 0)) (crate-dep (name "dyn-clone") (req "^1") (default-features #t) (kind 0)))) (hash "08623nipx25p32j4kgw87w0qi6dbmxskc820c6l6r41c7rg3ja9k")))

(define-public crate-tasd-0.2 (crate (name "tasd") (vers "0.2.0") (deps (list (crate-dep (name "strum_macros") (req "^0.25") (default-features #t) (kind 0)))) (hash "17nv6r6cass6i2x70q3rimzv3mli328sz9mirkp6hyxx63pbjwbk")))

(define-public crate-tasd-0.2 (crate (name "tasd") (vers "0.2.1") (deps (list (crate-dep (name "strum_macros") (req "^0.25") (default-features #t) (kind 0)))) (hash "0afwzhnifnqp8f07y3ix14d41h0db9v6n845p26cgpc0q1gl01xr")))

(define-public crate-tasd-0.3 (crate (name "tasd") (vers "0.3.0") (deps (list (crate-dep (name "strum_macros") (req "^0.25") (default-features #t) (kind 0)))) (hash "085fbflkjx52xjydwhl4f9nsc0481jqp9j5iarvyv1q75xns73hp")))

(define-public crate-tasd-0.4 (crate (name "tasd") (vers "0.4.0") (deps (list (crate-dep (name "strum") (req "^0.25") (default-features #t) (kind 0)) (crate-dep (name "strum_macros") (req "^0.25") (default-features #t) (kind 0)))) (hash "18a0c36kcsh909ybw9nrgkx1ydxm51baknlqa8id83p0jz6zf2aw")))

