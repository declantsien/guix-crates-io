(define-module (crates-io ta g-) #:use-module (crates-io))

(define-public crate-tag-helper-0.2 (crate (name "tag-helper") (vers "0.2.2") (deps (list (crate-dep (name "git2") (req "^0.6") (default-features #t) (kind 0)) (crate-dep (name "semver") (req "^0.7") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "structopt-derive") (req "^0.1") (default-features #t) (kind 0)))) (hash "060qps8kfz97q3znijv9nyp77gl2wq8a12xvzfz8la9ym01h5rwv")))

(define-public crate-tag-helper-0.2 (crate (name "tag-helper") (vers "0.2.4") (deps (list (crate-dep (name "git2") (req "^0.6") (default-features #t) (kind 0)) (crate-dep (name "semver") (req "^0.7") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "structopt-derive") (req "^0.1") (default-features #t) (kind 0)))) (hash "02inr8rdmxiffh2v5lf78mk4fsvxqrv4w6i8vy855swg2n4s3l9s")))

(define-public crate-tag-helper-0.3 (crate (name "tag-helper") (vers "0.3.0") (deps (list (crate-dep (name "git2") (req "^0.7") (default-features #t) (kind 0)) (crate-dep (name "semver") (req "^0.9") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.2") (default-features #t) (kind 0)))) (hash "16r0jh155dsk3z55kk7vrw4hyryfgan2116dd5m0s9hahnaq20la")))

(define-public crate-tag-helper-0.3 (crate (name "tag-helper") (vers "0.3.1") (deps (list (crate-dep (name "git2") (req "^0.7") (default-features #t) (kind 0)) (crate-dep (name "semver") (req "^0.9") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.2") (default-features #t) (kind 0)))) (hash "0kqnl1w3yp2vfpgv3rp9wb42xpli2x418jfngnhzbj2jc57r56bp")))

(define-public crate-tag-helper-0.3 (crate (name "tag-helper") (vers "0.3.2") (deps (list (crate-dep (name "git2") (req "^0.13") (default-features #t) (kind 0)) (crate-dep (name "semver") (req "^0.11") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.3") (default-features #t) (kind 0)))) (hash "06pj86df805dwvwzpxgak24afvjjvgrsrdj21p1mkalj0iyclffy")))

(define-public crate-tag-helper-0.4 (crate (name "tag-helper") (vers "0.4.0") (deps (list (crate-dep (name "anyhow") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "git2") (req "^0.13") (default-features #t) (kind 0)) (crate-dep (name "semver") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.3") (default-features #t) (kind 0)))) (hash "13l3qbaai7mdnk7s9mjmsw12f33g7bzwphi02qscf0px5sxwbd7f")))

(define-public crate-tag-helper-0.4 (crate (name "tag-helper") (vers "0.4.1") (deps (list (crate-dep (name "anyhow") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "git2") (req "^0.13") (default-features #t) (kind 0)) (crate-dep (name "semver") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.3") (default-features #t) (kind 0)))) (hash "04mwpp4jq65h5icr7g1xzb95lh2nb5r9pzqs32jshzzhax90wf11")))

(define-public crate-tag-helper-0.5 (crate (name "tag-helper") (vers "0.5.0") (deps (list (crate-dep (name "anyhow") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^4") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "git2") (req "^0.16") (default-features #t) (kind 0)) (crate-dep (name "semver") (req "^1") (default-features #t) (kind 0)))) (hash "19myz4cvim7w5vpjirhxrx1qqclnsnsvvfzv4dk7dggyqfxp47p7")))

(define-public crate-tag-helper-0.5 (crate (name "tag-helper") (vers "0.5.1") (deps (list (crate-dep (name "anyhow") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^4") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "git2") (req "^0.16") (default-features #t) (kind 0)) (crate-dep (name "semver") (req "^1") (default-features #t) (kind 0)))) (hash "1ribvhv0590ig6w7ylk7j15sq3fa5s8iidplck6cxbjfzw2d2kzk")))

(define-public crate-tag-vec-0.0.1 (crate (name "tag-vec") (vers "0.0.1") (hash "03pic39cchwcj2zyljp9ahd9mcn6kapp9jv8b4fbpixx0lip1kx4")))

(define-public crate-tag-vec-0.0.2 (crate (name "tag-vec") (vers "0.0.2") (hash "1q4ilbcm84bkkw2zcs8dwf6s8bx94cs1v8x15n5h83limq7s1lxl")))

