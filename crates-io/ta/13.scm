(define-module (crates-io ta #{13}#) #:use-module (crates-io))

(define-public crate-ta1394-avc-audio-0.1 (crate (name "ta1394-avc-audio") (vers "0.1.0") (deps (list (crate-dep (name "ta1394-avc-general") (req "^0.1") (default-features #t) (kind 0)))) (hash "0xhmlbzqf719j1j09qpz4n1l1q8jk81cam652jr27lcqk95hss1s")))

(define-public crate-ta1394-avc-audio-0.2 (crate (name "ta1394-avc-audio") (vers "0.2.0") (deps (list (crate-dep (name "ta1394-avc-general") (req "^0.2") (default-features #t) (kind 0)))) (hash "03p39szijfsv0i896dnirczfv9xs75jnzxl5h1fad272g6i69qzg")))

(define-public crate-ta1394-avc-ccm-0.1 (crate (name "ta1394-avc-ccm") (vers "0.1.0") (deps (list (crate-dep (name "ta1394-avc-general") (req "^0.1") (default-features #t) (kind 0)))) (hash "0zq1rd0i8ifqzkh0rxrsjjl3wg9lkp6avkk3x6lzyvb52qbqrjal")))

(define-public crate-ta1394-avc-ccm-0.2 (crate (name "ta1394-avc-ccm") (vers "0.2.0") (deps (list (crate-dep (name "ta1394-avc-general") (req "^0.2") (default-features #t) (kind 0)))) (hash "05fn0lgy0fyraad686vpa5bzbl63inlrhrs71f7fvf1qfqrh4nyv")))

(define-public crate-ta1394-avc-general-0.1 (crate (name "ta1394-avc-general") (vers "0.1.0") (deps (list (crate-dep (name "ieee1212-config-rom") (req "^0.1") (default-features #t) (kind 0)))) (hash "1hbbvj7fhl5zpcpr7b9dx51imbscl6cjr1wfgihc108g4w05chm2")))

(define-public crate-ta1394-avc-general-0.2 (crate (name "ta1394-avc-general") (vers "0.2.0") (deps (list (crate-dep (name "ieee1212-config-rom") (req "^0.1") (default-features #t) (kind 0)))) (hash "0jy2wx0dp3fdx4yfxjx74idw9iw9vkq9qpd77r62krgp8sv75pwz")))

(define-public crate-ta1394-avc-stream-format-0.1 (crate (name "ta1394-avc-stream-format") (vers "0.1.0") (deps (list (crate-dep (name "ta1394-avc-general") (req "^0.1") (default-features #t) (kind 0)))) (hash "11d8ildg4drl53vnzgzc4wcifyvqrxwy66cdrjg626g7c3vlkzcf")))

(define-public crate-ta1394-avc-stream-format-0.2 (crate (name "ta1394-avc-stream-format") (vers "0.2.0") (deps (list (crate-dep (name "ta1394-avc-general") (req "^0.2") (default-features #t) (kind 0)))) (hash "0aj2gzpj3r4gyd8793jmh0xxbksrf2hin051d6sg2bscw5g5jzqz")))

