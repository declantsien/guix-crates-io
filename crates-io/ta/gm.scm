(define-module (crates-io ta gm) #:use-module (crates-io))

(define-public crate-tagmap-0.0.1 (crate (name "tagmap") (vers "0.0.1") (deps (list (crate-dep (name "eva-common") (req "^0.3.1") (default-features #t) (kind 0)) (crate-dep (name "uuid") (req "^1.4.0") (default-features #t) (kind 0)))) (hash "0x6ks85ra0c583ca5hqdf8dw4s04kfzj8kf5wn96n1firnjgpq4r")))

(define-public crate-tagmap-0.0.2 (crate (name "tagmap") (vers "0.0.2") (deps (list (crate-dep (name "eva-common") (req "^0.3.1") (default-features #t) (kind 0)) (crate-dep (name "uuid") (req "^1.4.0") (default-features #t) (kind 0)))) (hash "0r25jcd3j36nz22arijk6bsim2w9qjia2x18z3ndmc0lhmlcfzsq")))

(define-public crate-tagmap-0.0.3 (crate (name "tagmap") (vers "0.0.3") (deps (list (crate-dep (name "eva-common") (req "^0.3.1") (default-features #t) (kind 0)) (crate-dep (name "uuid") (req "^1.4.0") (default-features #t) (kind 0)))) (hash "1jil3nn46njabp06mhfzr24bzrwz03akcaszk9ykafnfmcnpgizn")))

(define-public crate-tagmap-0.0.4 (crate (name "tagmap") (vers "0.0.4") (deps (list (crate-dep (name "eva-common") (req "^0.3.1") (default-features #t) (kind 0)) (crate-dep (name "uuid") (req "^1.4.0") (default-features #t) (kind 0)))) (hash "085xcajvlinhd5z2qwcj65znjsc1xnmppm9cicdk2mpz1vrv9qgf")))

(define-public crate-tagmap-0.0.5 (crate (name "tagmap") (vers "0.0.5") (deps (list (crate-dep (name "eva-common") (req "^0.3.1") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.164") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "0ddxx98rxilbbxg6dycvy4k4bzw0g84r6h51h2p8q0rxzfvqhrbr")))

(define-public crate-tagmap-0.0.6 (crate (name "tagmap") (vers "0.0.6") (deps (list (crate-dep (name "eva-common") (req "^0.3.1") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.164") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "1n87b162gpyil14cpskk0wqcl9qwyqjpvhx90jz02ga0qlddmj9r")))

(define-public crate-tagmap-0.0.7 (crate (name "tagmap") (vers "0.0.7") (deps (list (crate-dep (name "eva-common") (req "^0.3.1") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.164") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "0hvm1w88d7d10mfqqm2cv3dm830v1x9rd1a0vavc1b41n02n1qw6")))

(define-public crate-tagmap-0.0.8 (crate (name "tagmap") (vers "0.0.8") (deps (list (crate-dep (name "eva-common") (req "^0.3.1") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.164") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "1vyzv3zw2ajja40q0lgw2gi2sdai7iv7r8n9iivz3lygd4r2fwr9")))

(define-public crate-tagmap-0.0.9 (crate (name "tagmap") (vers "0.0.9") (deps (list (crate-dep (name "eva-common") (req "^0.3.1") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.164") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "0znyfyysmrv1mm1p99p7sr8ahdvnzzpnvz49x045gq18l9g819n2")))

