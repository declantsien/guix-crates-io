(define-module (crates-io ta rr) #:use-module (crates-io))

(define-public crate-tarr-0.1 (crate (name "tarr") (vers "0.1.0") (deps (list (crate-dep (name "anyhow") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "argh") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "fehler") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "humansize") (req "^1.1") (default-features #t) (kind 0)) (crate-dep (name "tar") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "tempfile") (req "^3.1") (default-features #t) (kind 0)) (crate-dep (name "unicode-width") (req "^0.1") (default-features #t) (kind 0)))) (hash "0nirh6n1m174fzf4ajg2p2h4prfqk76z1rras8f2hflhc22i8awp")))

(define-public crate-tarr-0.1 (crate (name "tarr") (vers "0.1.2") (deps (list (crate-dep (name "anyhow") (req "^1.0.47") (features (quote ("std"))) (kind 0)) (crate-dep (name "argh") (req "^0.1.6") (kind 0)) (crate-dep (name "fehler") (req "^1.0.0") (kind 0)) (crate-dep (name "humansize") (req "^1.1.1") (kind 0)) (crate-dep (name "tar") (req "^0.4.37") (kind 0)) (crate-dep (name "tempfile") (req "^3.2.0") (kind 0)) (crate-dep (name "unicode-width") (req "^0.1.9") (kind 0)))) (hash "15ihqq6cnyg4d44x7mvkm999glp0ly42yig4dcclmhmwakwm27ir")))

(define-public crate-tarragon-0.0.0 (crate (name "tarragon") (vers "0.0.0") (hash "0lpfrm0vskb1bsfz6r81j2wnya5sfiglkl7dklh2h895dnhnsxx9")))

(define-public crate-tarrasque-0.1 (crate (name "tarrasque") (vers "0.1.0") (deps (list (crate-dep (name "clippy") (req "0.0.*") (optional #t) (default-features #t) (kind 0)))) (hash "0gm3mlf26ldc6hs6ah3izlr5xvaw86cmz7zg9nlcc8l6z006343m")))

(define-public crate-tarrasque-0.2 (crate (name "tarrasque") (vers "0.2.0") (deps (list (crate-dep (name "clippy") (req "0.0.*") (optional #t) (default-features #t) (kind 0)))) (hash "086zfalqbn4wfs6jabnd9y6cvf9sx72sw9fclmxw1hdz83imx1yf")))

(define-public crate-tarrasque-0.3 (crate (name "tarrasque") (vers "0.3.0") (deps (list (crate-dep (name "clippy") (req "0.0.*") (optional #t) (default-features #t) (kind 0)))) (hash "0bphnxvwcsaby846adjwzc5lyxni4vhhxshilzz0dvpvnksc08xh")))

(define-public crate-tarrasque-0.4 (crate (name "tarrasque") (vers "0.4.0") (deps (list (crate-dep (name "clippy") (req "0.0.*") (optional #t) (default-features #t) (kind 0)))) (hash "1pmvgv807q84s5n20wmn7vz7awvn2dsgnc82s5j77zyy2qdcfr0h")))

(define-public crate-tarrasque-0.5 (crate (name "tarrasque") (vers "0.5.0") (hash "0vxk1xqmj5pr3yf1w4l6jgbjby52hhc2njqg8dhl11dp0r6kwgwx")))

(define-public crate-tarrasque-0.6 (crate (name "tarrasque") (vers "0.6.0") (deps (list (crate-dep (name "tarrasque-macro") (req "^0.6.0") (default-features #t) (kind 0)))) (hash "0hq38b7m9aipdiirfagmcracqbwnw9c4hhkxzfxqs7zmcw0j7wab")))

(define-public crate-tarrasque-0.7 (crate (name "tarrasque") (vers "0.7.0") (deps (list (crate-dep (name "tarrasque-macro") (req "^0.7.0") (default-features #t) (kind 0)))) (hash "1b7wj5asll1l7wpcwkc27pji1q9vzyk4wnajczgxlvk0b7m9mzz9")))

(define-public crate-tarrasque-0.8 (crate (name "tarrasque") (vers "0.8.0") (deps (list (crate-dep (name "microbench") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "tarrasque-macro") (req "^0.8.0") (default-features #t) (kind 0)))) (hash "0wc33192ry6njk9fwm63zy2k4vz2d6f39n645nfh0a46b8md1141")))

(define-public crate-tarrasque-0.9 (crate (name "tarrasque") (vers "0.9.0") (deps (list (crate-dep (name "microbench") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "tarrasque-macro") (req "^0.9.0") (default-features #t) (kind 0)))) (hash "12hg5dyxq70749wcjr376gk59f5mzc28nn0nxailxffp4s47yhq5")))

(define-public crate-tarrasque-0.10 (crate (name "tarrasque") (vers "0.10.0") (deps (list (crate-dep (name "microbench") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "tarrasque-macro") (req "^0.10.0") (default-features #t) (kind 0)))) (hash "04lyk8xil39fdhwchkxidw31v970snsx5xnnqj2dnbwv9n1aakkv")))

(define-public crate-tarrasque-macro-0.6 (crate (name "tarrasque-macro") (vers "0.6.0") (deps (list (crate-dep (name "proc-macro2") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^0.6") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^0.15") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "1qrwvp80vrmhgnc708ly1nbnc7q1i8lv5nh92vr8dgz59j4w1skw")))

(define-public crate-tarrasque-macro-0.7 (crate (name "tarrasque-macro") (vers "0.7.0") (deps (list (crate-dep (name "proc-macro2") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^0.6") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^0.15") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "05xpsiwjz4z3m2b0dhd1234bgj5jkxvcy6rssg44mdh031164dcg")))

(define-public crate-tarrasque-macro-0.8 (crate (name "tarrasque-macro") (vers "0.8.0") (deps (list (crate-dep (name "proc-macro2") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^0.6") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^0.15") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "06rkvcm2y0y0wpz8gliy66l7vf763q4f0a195by9q2lz1fjg4gxw")))

(define-public crate-tarrasque-macro-0.9 (crate (name "tarrasque-macro") (vers "0.9.0") (deps (list (crate-dep (name "proc-macro2") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^0.6") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^0.15") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "08z8m1y2005ywhszq8z69yz0m94w225d1756dqvynmdp9fn5nhcy")))

(define-public crate-tarrasque-macro-0.10 (crate (name "tarrasque-macro") (vers "0.10.0") (deps (list (crate-dep (name "proc-macro2") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^0.6") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^0.15") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "1ynkv76vmynrfdk36bimw1j88iavmd0qlyanan0y7cy58yxzd31r")))

