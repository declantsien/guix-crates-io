(define-module (crates-io ta tk) #:use-module (crates-io))

(define-public crate-tatk-0.2 (crate (name "tatk") (vers "0.2.0") (hash "0kmlkx3nr4sg0yc75y749np74c5nh1idnm9ql27qgl55g4klrn19") (features (quote (("test-data") ("full" "test-data") ("f32") ("default"))))))

(define-public crate-tatk-0.2 (crate (name "tatk") (vers "0.2.1") (hash "1al2w3rv6g0dckra4b72rk8ip1q6i6l87qvsqaqsi9r09wzz8jq8") (features (quote (("test-data") ("full" "test-data") ("f32") ("default"))))))

(define-public crate-tatk-0.2 (crate (name "tatk") (vers "0.2.3") (hash "17hxpwmxvw1l75dqvvlhnz8sb6kd01jn4ylakw4k2bqh6084jh44") (features (quote (("test-data") ("full" "test-data") ("f32") ("default"))))))

(define-public crate-tatk-0.2 (crate (name "tatk") (vers "0.2.4") (hash "168fiflbhhgbhnzhym8c8cnzw3f5j3pr0zlglk0nz9h5w667w9fj") (features (quote (("test-data") ("full" "test-data") ("f32") ("default"))))))

(define-public crate-tatk-0.3 (crate (name "tatk") (vers "0.3.0") (deps (list (crate-dep (name "tatk_derive") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "0qy5gwr8rhhxx1l3grz5c2fjmpwry22sjy1vaq157kvzqz2h2iph") (features (quote (("test-data") ("full" "test-data") ("f32") ("default"))))))

(define-public crate-tatk_derive-0.1 (crate (name "tatk_derive") (vers "0.1.0") (deps (list (crate-dep (name "quote") (req "^1.0.33") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2.0.38") (default-features #t) (kind 0)))) (hash "08cg2777rm8vylnrsdp0j737d2y1srq7jc8iqpghchac5mj53ghs")))

