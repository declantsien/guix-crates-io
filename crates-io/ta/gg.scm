(define-module (crates-io ta gg) #:use-module (crates-io))

(define-public crate-tagga-0.0.0 (crate (name "tagga") (vers "0.0.0") (hash "1y8l4imwcvb0nf1r8f9qhdhwmz1l7l2k3l79hkpvvfyncyc7w72i")))

(define-public crate-tagged-0.1 (crate (name "tagged") (vers "0.1.0") (hash "0paqg0mmb195yvid4zxc9qrsdpl8d4nw6kp9jf7r7k5av6krn37j")))

(define-public crate-tagged-0.1 (crate (name "tagged") (vers "0.1.1") (hash "022n1jhkvh7c13w66iyw1gj80g98b2lzvhrns2psc58hyfs7c8av")))

(define-public crate-tagged-base64-0.3 (crate (name "tagged-base64") (vers "0.3.2") (deps (list (crate-dep (name "ark-serialize") (req "^0.4.0") (features (quote ("derive"))) (optional #t) (kind 0)) (crate-dep (name "ark-std") (req "^0.4.0") (kind 0)) (crate-dep (name "base64") (req "^0.13.1") (default-features #t) (kind 0)) (crate-dep (name "bincode") (req "^1.3") (default-features #t) (kind 2)) (crate-dep (name "clap") (req "^4.0") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "console_error_panic_hook") (req "^0.1.7") (optional #t) (default-features #t) (target "cfg(target_arch = \"wasm32\")") (kind 0)) (crate-dep (name "crc-any") (req "^2.4.1") (kind 0)) (crate-dep (name "getrandom") (req "^0.2") (features (quote ("js"))) (default-features #t) (kind 2)) (crate-dep (name "quickcheck") (req "^1.0") (default-features #t) (kind 2)) (crate-dep (name "quickcheck_macros") (req "^1.0") (default-features #t) (kind 2)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 2)) (crate-dep (name "snafu") (req "^0.7") (features (quote ("backtraces"))) (default-features #t) (kind 0)) (crate-dep (name "tagged-base64-macros") (req "^0.3.2") (kind 0)) (crate-dep (name "wasm-bindgen") (req "^0.2.78") (features (quote ("serde-serialize"))) (optional #t) (default-features #t) (target "cfg(target_arch = \"wasm32\")") (kind 0)) (crate-dep (name "wasm-bindgen-test") (req "^0.3.28") (default-features #t) (kind 2)) (crate-dep (name "web-sys") (req "^0.3.49") (features (quote ("console" "Headers" "RequestInit" "RequestMode" "Request" "Response" "Window"))) (optional #t) (default-features #t) (target "cfg(target_arch = \"wasm32\")") (kind 0)))) (hash "0m7m1yjqyknxxsg0fssrfdsgymsq29fwbqsmbi4bfx22rni790mg") (features (quote (("default" "ark-serialize" "serde" "wasm-bindgen")))) (v 2) (features2 (quote (("wasm-debug" "dep:console_error_panic_hook") ("wasm-bindgen" "dep:wasm-bindgen") ("serde" "dep:serde" "tagged-base64-macros/serde") ("build-cli" "dep:clap") ("ark-serialize" "dep:ark-serialize"))))))

(define-public crate-tagged-base64-0.3 (crate (name "tagged-base64") (vers "0.3.3") (deps (list (crate-dep (name "ark-serialize") (req "^0.4.0") (features (quote ("derive"))) (optional #t) (kind 0)) (crate-dep (name "ark-std") (req "^0.4.0") (kind 0)) (crate-dep (name "base64") (req "^0.13.1") (default-features #t) (kind 0)) (crate-dep (name "bincode") (req "^1.3") (default-features #t) (kind 2)) (crate-dep (name "clap") (req "^4.0") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "console_error_panic_hook") (req "^0.1.7") (optional #t) (default-features #t) (target "cfg(target_arch = \"wasm32\")") (kind 0)) (crate-dep (name "crc-any") (req "^2.4.1") (kind 0)) (crate-dep (name "getrandom") (req "^0.2") (features (quote ("js"))) (default-features #t) (kind 2)) (crate-dep (name "quickcheck") (req "^1.0") (default-features #t) (kind 2)) (crate-dep (name "quickcheck_macros") (req "^1.0") (default-features #t) (kind 2)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 2)) (crate-dep (name "snafu") (req "^0.7") (features (quote ("backtraces"))) (default-features #t) (kind 0)) (crate-dep (name "tagged-base64-macros") (req "^0.3.3") (kind 0)) (crate-dep (name "wasm-bindgen") (req "^0.2.78") (features (quote ("serde-serialize"))) (optional #t) (default-features #t) (target "cfg(target_arch = \"wasm32\")") (kind 0)) (crate-dep (name "wasm-bindgen-test") (req "^0.3.28") (default-features #t) (kind 2)) (crate-dep (name "web-sys") (req "^0.3.49") (features (quote ("console" "Headers" "RequestInit" "RequestMode" "Request" "Response" "Window"))) (optional #t) (default-features #t) (target "cfg(target_arch = \"wasm32\")") (kind 0)))) (hash "0c77wsbf6nw51xibvy6cmm05gccgy84ck719k43qb8z3h579s62h") (features (quote (("default" "ark-serialize" "serde" "wasm-bindgen")))) (v 2) (features2 (quote (("wasm-debug" "dep:console_error_panic_hook") ("wasm-bindgen" "dep:wasm-bindgen") ("serde" "dep:serde" "tagged-base64-macros/serde") ("build-cli" "dep:clap") ("ark-serialize" "dep:ark-serialize"))))))

(define-public crate-tagged-base64-0.4 (crate (name "tagged-base64") (vers "0.4.0") (deps (list (crate-dep (name "ark-bls12-381") (req "^0.4") (default-features #t) (kind 2)) (crate-dep (name "ark-serialize") (req "^0.4") (features (quote ("derive"))) (optional #t) (kind 0)) (crate-dep (name "ark-std") (req "^0.4") (kind 0)) (crate-dep (name "base64") (req "^0.22") (default-features #t) (kind 0)) (crate-dep (name "bincode") (req "^1.3") (default-features #t) (kind 2)) (crate-dep (name "clap") (req "^4.0") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "console_error_panic_hook") (req "^0.1.7") (optional #t) (default-features #t) (target "cfg(target_arch = \"wasm32\")") (kind 0)) (crate-dep (name "crc-any") (req "^2.4.1") (kind 0)) (crate-dep (name "getrandom") (req "^0.2") (features (quote ("js"))) (default-features #t) (kind 2)) (crate-dep (name "quickcheck") (req "^1.0") (default-features #t) (kind 2)) (crate-dep (name "quickcheck_macros") (req "^1.0") (default-features #t) (kind 2)) (crate-dep (name "rand_chacha") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 2)) (crate-dep (name "snafu") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "tagged-base64-macros") (req "^0.4.0") (kind 0)) (crate-dep (name "wasm-bindgen") (req "^0.2.78") (features (quote ("serde-serialize"))) (optional #t) (default-features #t) (target "cfg(target_arch = \"wasm32\")") (kind 0)) (crate-dep (name "wasm-bindgen-test") (req "^0.3.28") (default-features #t) (kind 2)) (crate-dep (name "web-sys") (req "^0.3.49") (features (quote ("console" "Headers" "RequestInit" "RequestMode" "Request" "Response" "Window"))) (optional #t) (default-features #t) (target "cfg(target_arch = \"wasm32\")") (kind 0)))) (hash "1wai2nv718ig07iyy9ar2jm7ywhlz8xl1kgqqv93ynj0vgqvnx3v") (features (quote (("default" "ark-serialize" "serde" "wasm-bindgen")))) (v 2) (features2 (quote (("wasm-debug" "dep:console_error_panic_hook") ("wasm-bindgen" "dep:wasm-bindgen") ("serde" "dep:serde" "tagged-base64-macros/serde") ("build-cli" "dep:clap") ("ark-serialize" "dep:ark-serialize"))))))

(define-public crate-tagged-base64-macros-0.3 (crate (name "tagged-base64-macros") (vers "0.3.2") (deps (list (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.109") (features (quote ("full" "extra-traits"))) (default-features #t) (kind 0)))) (hash "09i8vsk2798ylsksvsflhhic4i9jgj2jbl6qnji9grkcyc13ck7q") (features (quote (("serde") ("default" "serde"))))))

(define-public crate-tagged-base64-macros-0.3 (crate (name "tagged-base64-macros") (vers "0.3.3") (deps (list (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.109") (features (quote ("full" "extra-traits"))) (default-features #t) (kind 0)))) (hash "12bm1z4dxx6fygzlir4whcw5lb84v0mf4b0qd7cs6vhbyjb6lqvi") (features (quote (("serde") ("default" "serde"))))))

(define-public crate-tagged-base64-macros-0.4 (crate (name "tagged-base64-macros") (vers "0.4.0") (deps (list (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.109") (features (quote ("full" "extra-traits"))) (default-features #t) (kind 0)))) (hash "1b45mr48ym9w2qwhjmxsap27ykr6qcz1fshryz9gm6082p0a7pmw") (features (quote (("serde") ("default" "serde"))))))

(define-public crate-tagged-box-0.1 (crate (name "tagged-box") (vers "0.1.0") (hash "16mhmh70ckhqmnavrwxfkg75lyrcdw4c8lyqvnsmfd01f5z82k7w") (features (quote (("default" "48bits") ("57bits") ("56bits") ("55bits") ("54bits") ("53bits") ("52bits") ("51bits") ("50bits") ("49bits") ("48bits"))))))

(define-public crate-tagged-box-0.1 (crate (name "tagged-box") (vers "0.1.1") (hash "1h4dbpbcj8n5iw0nk4bnca47b6b8xhivnrgxzmnmhkbfkdrlp77y") (features (quote (("default" "48bits") ("57bits") ("56bits") ("55bits") ("54bits") ("53bits") ("52bits") ("51bits") ("50bits") ("49bits") ("48bits"))))))

(define-public crate-tagged-channels-0.0.1 (crate (name "tagged-channels") (vers "0.0.1") (deps (list (crate-dep (name "parking_lot") (req "^0.12") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1") (features (quote ("sync"))) (default-features #t) (kind 0)) (crate-dep (name "async-stream") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "axum") (req "^0.6") (features (quote ("ws"))) (default-features #t) (kind 2)) (crate-dep (name "futures") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "rusty-hook") (req "^0.11") (default-features #t) (kind 2)) (crate-dep (name "serde") (req "^1") (features (quote ("derive" "rc"))) (default-features #t) (kind 2)) (crate-dep (name "serde_json") (req "^1") (default-features #t) (kind 2)) (crate-dep (name "tokio") (req "^1") (features (quote ("macros" "rt-multi-thread"))) (default-features #t) (kind 2)) (crate-dep (name "tokio-test") (req "^0.4") (default-features #t) (kind 2)))) (hash "154va7lmp35494vmqqani1rrbzindgc75jjaqjk6jp0cmcm2hpb5")))

(define-public crate-tagged-hybrid-0.1 (crate (name "tagged-hybrid") (vers "0.1.0") (deps (list (crate-dep (name "heck") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "itertools") (req "^0.10") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "smallvec") (req "^1.9") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "tap") (req "^1.0") (default-features #t) (kind 0)))) (hash "1g26qinfb388rqn1dxqw3fvip49nywpwyfim6kdp7mr13m1gx4yb")))

(define-public crate-tagged-pointer-0.1 (crate (name "tagged-pointer") (vers "0.1.0") (deps (list (crate-dep (name "typenum") (req "^1.13") (default-features #t) (kind 0)))) (hash "0sw05yv4r3ws0yv1asnhgbl15bpn87n5ynnwszl7niscrf90idgn") (features (quote (("fallback"))))))

(define-public crate-tagged-pointer-0.1 (crate (name "tagged-pointer") (vers "0.1.1") (deps (list (crate-dep (name "typenum") (req "^1.13") (default-features #t) (kind 0)))) (hash "0dg825nhc4hrvz53pnilcbq679arvxx17q13ipb7lf8cqg3d96c2") (features (quote (("fallback"))))))

(define-public crate-tagged-pointer-0.1 (crate (name "tagged-pointer") (vers "0.1.2") (deps (list (crate-dep (name "typenum") (req "^1.13") (default-features #t) (kind 0)))) (hash "135a0dha8sb40ypncsb0dz4r0grdj08303np9s5ncbhjwwwa2g30") (features (quote (("fallback"))))))

(define-public crate-tagged-pointer-0.1 (crate (name "tagged-pointer") (vers "0.1.3") (deps (list (crate-dep (name "typenum") (req "^1.13") (default-features #t) (kind 0)))) (hash "1jnx6h6y9cwk3vvvf6kr9n17n4g0rv280iy3z54mpric4i4ap9w8") (features (quote (("fallback"))))))

(define-public crate-tagged-pointer-0.2 (crate (name "tagged-pointer") (vers "0.2.0") (hash "1qay7qbx2x3khn52kpv2g5sxrjmb1k16qq51vq52f7qgmrfx237a") (features (quote (("fallback"))))))

(define-public crate-tagged-pointer-0.2 (crate (name "tagged-pointer") (vers "0.2.1") (hash "0a3mx45g2g8blnrfl20lbr68fqd8ybkyc26aizyf0whscq90v6bq") (features (quote (("fallback"))))))

(define-public crate-tagged-pointer-0.2 (crate (name "tagged-pointer") (vers "0.2.2") (hash "0bcx3y3rwxslnvg283pmycd3fhn582iw76x3473v060ljbvrmfl6") (features (quote (("fallback"))))))

(define-public crate-tagged-pointer-0.2 (crate (name "tagged-pointer") (vers "0.2.3") (hash "05ff60453a94ln9yahypyarrlkcr5nkj9d7ss1j7ll10d6j2x7d0") (features (quote (("fallback")))) (rust-version "1.51")))

(define-public crate-tagged-pointer-0.2 (crate (name "tagged-pointer") (vers "0.2.4") (hash "0ha8138v8cskj21rv04f9jdrphl4jld6lk1c3j2rsk78ar66gbf4") (features (quote (("fallback")))) (rust-version "1.51")))

(define-public crate-tagged-pointer-0.2 (crate (name "tagged-pointer") (vers "0.2.5") (hash "1h40i0ww57665gkrlcgm1njvnrvmjczaryg214z14jrlm5mpyqsp") (features (quote (("fallback")))) (rust-version "1.51")))

(define-public crate-tagged-pointer-0.2 (crate (name "tagged-pointer") (vers "0.2.6") (hash "0mp6iiqqbh4372pj5zsi18y7rg6gdpvlbv6jjlakcnszb5kn3kmk") (features (quote (("fallback")))) (rust-version "1.51")))

(define-public crate-tagged-pointer-0.2 (crate (name "tagged-pointer") (vers "0.2.7") (deps (list (crate-dep (name "compiletest_rs") (req "^0.10") (optional #t) (default-features #t) (kind 0)))) (hash "0bpimmrkci67c62p795a5p5wq3g9ggdhx5zrqwfvf5jlzdssgbi1") (features (quote (("fallback")))) (rust-version "1.51")))

(define-public crate-tagged-pointer-as-enum-1 (crate (name "tagged-pointer-as-enum") (vers "1.0.0") (deps (list (crate-dep (name "criterion") (req "^0.3") (optional #t) (default-features #t) (kind 0)))) (hash "1phqxl8sr92mr384km3csbz6gafwqv4qj6aar51gj71v721z1s9y") (features (quote (("bench" "criterion"))))))

(define-public crate-tagged-rendezvous-0.1 (crate (name "tagged-rendezvous") (vers "0.1.0") (deps (list (crate-dep (name "dashmap") (req "^4") (default-features #t) (kind 0)) (crate-dep (name "murmur3") (req "^0.5") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "rayon_crate") (req "^1") (optional #t) (default-features #t) (kind 0) (package "rayon")))) (hash "1v9d7gcqyvyfjmmlszxzxxx7hl9qvpdcjb66r6s104phkimq8yw1") (features (quote (("rayon" "rayon_crate" "dashmap/rayon"))))))

(define-public crate-tagged-rendezvous-0.1 (crate (name "tagged-rendezvous") (vers "0.1.1") (deps (list (crate-dep (name "dashmap") (req "^5") (default-features #t) (kind 0)) (crate-dep (name "murmur3") (req "^0.5") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "rayon_crate") (req "^1") (optional #t) (default-features #t) (kind 0) (package "rayon")))) (hash "19as1hgvpz0kv4pv6ppf358qbww82krbs8ivd9h5dv6mp7m690f8") (features (quote (("rayon" "rayon_crate" "dashmap/rayon"))))))

(define-public crate-tagged-serde-0.1 (crate (name "tagged-serde") (vers "0.1.0") (deps (list (crate-dep (name "proc-macro2") (req "^1.0.56") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.26") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2.0.15") (features (quote ("full" "extra-traits"))) (default-features #t) (kind 0)))) (hash "0yj6i559x6ijbiari63351riwb6313xc4zdzsbnjd7sbhq7xj9qg")))

(define-public crate-tagged-tree-0.1 (crate (name "tagged-tree") (vers "0.1.0") (deps (list (crate-dep (name "doc-comment") (req "^0.3.3") (default-features #t) (kind 2)) (crate-dep (name "duplicate") (req "^0.3.0") (default-features #t) (kind 2)) (crate-dep (name "mockall") (req "^0.10.2") (default-features #t) (kind 2)))) (hash "0pjrjq502955kg8nd96qn20gn9d91azp7vr587g5vv12vayg9bln")))

(define-public crate-tagged-tree-0.2 (crate (name "tagged-tree") (vers "0.2.0") (deps (list (crate-dep (name "doc-comment") (req "^0.3.3") (default-features #t) (kind 2)) (crate-dep (name "duplicate") (req "^0.3.0") (default-features #t) (kind 2)) (crate-dep (name "mockall") (req "^0.10.2") (default-features #t) (kind 2)))) (hash "0sp8c3xp3dvxmq1z8i963hkbwcbv38b3i2zz3h0pyb3h8hxxbqjl")))

(define-public crate-tagged-tree-0.3 (crate (name "tagged-tree") (vers "0.3.0") (deps (list (crate-dep (name "doc-comment") (req "^0.3.3") (default-features #t) (kind 2)) (crate-dep (name "duplicate") (req "^0.3.0") (default-features #t) (kind 2)) (crate-dep (name "mockall") (req "^0.10.2") (default-features #t) (kind 2)))) (hash "1g0b4d5rfwqwymhili1rfvsdi7hsc6acs2vdz1f5zan4rh4xvba1")))

(define-public crate-tagged-tree-0.4 (crate (name "tagged-tree") (vers "0.4.0") (deps (list (crate-dep (name "doc-comment") (req "^0.3.3") (default-features #t) (kind 2)) (crate-dep (name "duplicate") (req "^0.3.0") (default-features #t) (kind 2)) (crate-dep (name "mockall") (req "^0.10.2") (default-features #t) (kind 2)) (crate-dep (name "serde") (req "^1.0.130") (features (quote ("std" "derive"))) (optional #t) (kind 0)))) (hash "0fd5x5x0ddh3611smllwr8pps5nmkn7zb1p65p0d177zgyrg0w8h")))

(define-public crate-tagged-union-0.0.1 (crate (name "tagged-union") (vers "0.0.1") (deps (list (crate-dep (name "tagged-union-macro") (req "^0.0.1") (default-features #t) (kind 0)))) (hash "0kqma5zha06zdnilvjvjr4gl28ic5xjhwpnnsp3gpl0ph6fwyv96")))

(define-public crate-tagged-union-macro-0.0.1 (crate (name "tagged-union-macro") (vers "0.0.1") (deps (list (crate-dep (name "Inflector") (req "^0.11.4") (kind 0)) (crate-dep (name "proc-macro2") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2") (features (quote ("derive" "full" "extra-traits"))) (default-features #t) (kind 0)))) (hash "15hzkyg0pjzv3640qad9pvlingaq6zd6cqq81chs0r9hvjhf4kzb")))

(define-public crate-tagged_bytes-0.0.1 (crate (name "tagged_bytes") (vers "0.0.1") (hash "1m8nyca9lfrxnkmdrw88mvzb285gdb83qykr8k4y1k8njcnavaq9") (features (quote (("alloc"))))))

(define-public crate-tagged_cell-0.1 (crate (name "tagged_cell") (vers "0.1.0") (hash "0bpn82shnf82ck9ffqpmbb4vhsj9blncs5xxczb45qd4kcsvb80f")))

(define-public crate-tagged_cell-0.1 (crate (name "tagged_cell") (vers "0.1.1") (hash "1v8jg1zzcidllvxbbxi5a4ibxf1fa5ycbn8hccgynhzqwpv567ca")))

(define-public crate-tagged_cell-0.1 (crate (name "tagged_cell") (vers "0.1.2") (hash "1jf5kavssxrva5cq3dy8bsh75m1akb4cbjkjxzjwajzxj710r2kw")))

(define-public crate-tagged_cell-0.1 (crate (name "tagged_cell") (vers "0.1.3") (hash "1nr9r2h26rlhiaja3252y98lhh4cvxmzw7dn0k84482gm661dz2w")))

(define-public crate-tagged_ptr-0.1 (crate (name "tagged_ptr") (vers "0.1.0") (hash "0bnqrdmxdaqz0j5bmmr9g8hzyxnaqr29zb32iwf2l2wgsvk63j4i")))

(define-public crate-tagged_ufs-0.1 (crate (name "tagged_ufs") (vers "0.1.0") (deps (list (crate-dep (name "ahash") (req "^0.8.3") (default-features #t) (kind 0)) (crate-dep (name "anyhow") (req "^1.0.75") (default-features #t) (kind 0)) (crate-dep (name "criterion") (req "^0.5.1") (default-features #t) (kind 2)) (crate-dep (name "quickcheck") (req "^1.0.3") (default-features #t) (kind 2)) (crate-dep (name "quickcheck_macros") (req "^1.0.0") (default-features #t) (kind 2)))) (hash "1bxrh82w53r1z5qq3hyxfkazki3rajr85n73iishl650r23sm76k")))

(define-public crate-tagger-0.1 (crate (name "tagger") (vers "0.1.0") (hash "04mw1nh0l2ci2dwymfy7hn7prm6m9dacrvw8fwysszi57b5gqq2c")))

(define-public crate-tagger-0.2 (crate (name "tagger") (vers "0.2.0") (hash "1bipkj4pcdva7ma7x9jn08lm1p914716za9hhbqpqbszdbw5bd6g")))

(define-public crate-tagger-0.2 (crate (name "tagger") (vers "0.2.1") (hash "1k0daq8kn5cjjxlplgvqw91ngqd3vk7gpippsx5smm2rl1wbf2x0")))

(define-public crate-tagger-0.3 (crate (name "tagger") (vers "0.3.0") (hash "02k1djb316wdc98d8y4dd8k056mr8z61rnz9fwi9f129lbzwvzmd")))

(define-public crate-tagger-0.3 (crate (name "tagger") (vers "0.3.1") (hash "1gbizmai0myvlqqwajz5g52s9akipyl0m9f5ri72582r6mwdhmvh")))

(define-public crate-tagger-0.3 (crate (name "tagger") (vers "0.3.2") (hash "0c4jsxpzlvww7ajgbsp8lh07dfrp85i3ahkw88dw6fadx1y3ald5")))

(define-public crate-tagger-0.4 (crate (name "tagger") (vers "0.4.0") (hash "0b5sq5zdvqlrqvc92kvpy058z6hyhrxivh35vgj4fvyq5pxb5ija")))

(define-public crate-tagger-0.4 (crate (name "tagger") (vers "0.4.1") (hash "03j1lnkk4yi7nhdsyfw37ghh7vj07gkcyxy2s7dpw58b15k1wbl1")))

(define-public crate-tagger-0.4 (crate (name "tagger") (vers "0.4.2") (hash "1w6pmm28imn9mjxj9z1nyjrrza9sgyv2vphk4rbgd31wh4yal1s4")))

(define-public crate-tagger-0.4 (crate (name "tagger") (vers "0.4.3") (hash "13qdh2p1w27bzym7z584s9rp2l5imszbcvbrciqb3bahb7mn694n")))

(define-public crate-tagger-0.4 (crate (name "tagger") (vers "0.4.5") (hash "0g6nlrsgjib3kl359rrc3lvsk4q4fmaycq9rinak53pns37xi30z")))

(define-public crate-tagger-0.4 (crate (name "tagger") (vers "0.4.6") (hash "10db871ivm8xzg01f3pzm52ija8mjn7wddb2r2skx4nx3b57z8dd")))

(define-public crate-tagger-0.5 (crate (name "tagger") (vers "0.5.0") (hash "0mfr5n3xzybb0rk9qc854xq9b6apvh5w9mia6h96m0zj9dfcq4xx")))

(define-public crate-tagger-0.6 (crate (name "tagger") (vers "0.6.0") (hash "1bv1amsn1pfsxz5l4237yzc3gdz9xc8rq0lz1wx4zgaln09mirx7")))

(define-public crate-tagger-0.6 (crate (name "tagger") (vers "0.6.1") (hash "17sdp7b0g6q5zdm3sy7ycfy7cc81y94manyn7hssn7s3r2hh3xll")))

(define-public crate-tagger-0.6 (crate (name "tagger") (vers "0.6.2") (hash "0m6bwa81kzq7sxy9as8j4grrj1a7nd4hgw6lvw6j5grv4bg6bsm6")))

(define-public crate-tagger-0.7 (crate (name "tagger") (vers "0.7.0") (hash "16lc2y1xh9nnn58y5yx95bfzlyk1praik9p6yikjvnn81fl29wjs")))

(define-public crate-tagger-0.7 (crate (name "tagger") (vers "0.7.1") (hash "07h2vbi56x8f2hj97yis3x7sa3q25fv05l16jjihi546j3fd3kqa")))

(define-public crate-tagger-1 (crate (name "tagger") (vers "1.0.0") (hash "1rwpmw56yhkarldgj0r5vjqvqw03dbny3g62pssdd85idz18kmiz")))

(define-public crate-tagger-1 (crate (name "tagger") (vers "1.0.1") (hash "1bd97p2a0y304324r9w03vqig23wwx8w4aj6d3l3aszg5631alv5")))

(define-public crate-tagger-2 (crate (name "tagger") (vers "2.0.0") (hash "00ba7idndmdshnvy3rsjxwzs3k695nx7vpkfsrhr78m3b0qysysw") (yanked #t)))

(define-public crate-tagger-2 (crate (name "tagger") (vers "2.0.1") (hash "0kxqa8wfh7rh2y5czy1fqr8zpvgdc1bccw7msh5d5p2c7caw44i1") (yanked #t)))

(define-public crate-tagger-2 (crate (name "tagger") (vers "2.1.0") (hash "1zpl51886ak0l0cc3pxajvia4qrj0h90dimp0hf2w864vdyq0h0s") (yanked #t)))

(define-public crate-tagger-2 (crate (name "tagger") (vers "2.1.1") (hash "0dn6wrk72pz6mpgdhy4ymqq43wmpvh914nvqjhjpygzlvsfa1vf8") (yanked #t)))

(define-public crate-tagger-2 (crate (name "tagger") (vers "2.1.2") (hash "10za5w5md0z2cik2wf8h0292zyybgglfwprqh4q22n041r8sl92d")))

(define-public crate-tagger-2 (crate (name "tagger") (vers "2.2.0") (hash "08c3jrq7zd1s598lzf246kjqj7jynr0wjaqyxaha18vqsw759kyh")))

(define-public crate-tagger-2 (crate (name "tagger") (vers "2.2.1") (hash "0k82kfbj1dxnxcx1xcvhnzfi82n4m6ik6pybj32wh7l91lx871nj")))

(define-public crate-tagger-3 (crate (name "tagger") (vers "3.0.0") (hash "0n06q5z1yyzd0jj9wmjm3k7bskavzvjh5xsyilyacgav891hzbia") (yanked #t)))

(define-public crate-tagger-3 (crate (name "tagger") (vers "3.0.1") (hash "1wwcfs01lqis8m1a499sk4g39vcfrizk3vb1a6gg5x4sf6jl6zyb") (yanked #t)))

(define-public crate-tagger-3 (crate (name "tagger") (vers "3.0.2") (hash "1gmffhws6vyrb07k5wd0q2smh9zx6gc1j364scndmqkp0zbn8vcf") (yanked #t)))

(define-public crate-tagger-3 (crate (name "tagger") (vers "3.0.4") (hash "0zp8klvvm4gr780gl0php0ncbjng3dr5iz08qfkzalzrc8c7dm00") (yanked #t)))

(define-public crate-tagger-3 (crate (name "tagger") (vers "3.0.5") (hash "1k9b165a79jw7l5i567iricnvw81xbh50i0am3hz72baazvzpjqn") (yanked #t)))

(define-public crate-tagger-3 (crate (name "tagger") (vers "3.0.6") (hash "17g0jbmyiwy1fnkx1dpf0nxdsddqcy61r021i94p6qx7qz086dzz") (yanked #t)))

(define-public crate-tagger-3 (crate (name "tagger") (vers "3.1.0") (hash "0ahy8m9nnj8srvx13i5l3hsqr9q4n7r891bxaxnvkb1pkllagvvr") (yanked #t)))

(define-public crate-tagger-3 (crate (name "tagger") (vers "3.1.1") (hash "0sgvcif0qzchb9v678gbryhblp4zwvy8nvvw4f43iw6qsp8q7fcj") (yanked #t)))

(define-public crate-tagger-3 (crate (name "tagger") (vers "3.1.2") (hash "0w45vl3c5174n2n4kn2nwp6r6qnwcvq8532xnk6s65a3vivim5v0")))

(define-public crate-tagger-3 (crate (name "tagger") (vers "3.2.0") (hash "1anms5pi0m2xjd2wiff9fxahsygfpci0s5nqvi6n4hxvm8yhidry")))

(define-public crate-tagger-3 (crate (name "tagger") (vers "3.2.1") (hash "0kyja69b9spzc6mc28vclgv0yz13mlskwms97w2rnl3d34kfccy9")))

(define-public crate-tagger-3 (crate (name "tagger") (vers "3.3.0") (hash "0riy8ykkrmghq963zzk2vzi8idszq1nplpd6vrhaj2s9ixcbbgby") (yanked #t)))

(define-public crate-tagger-3 (crate (name "tagger") (vers "3.3.1") (hash "117c2qc5sk76825pvmqpv2x76yc1brlcnw33sfy69zwg57whd4rs") (yanked #t)))

(define-public crate-tagger-3 (crate (name "tagger") (vers "3.3.2") (hash "04zbvvixxc9j3yswvh12lzqwi5j14ay4yrcqlp3m3nxi10caagfv") (yanked #t)))

(define-public crate-tagger-4 (crate (name "tagger") (vers "4.0.0") (hash "03a5grrgsxkwqr9m9jxyq5818xri4fcir9sh6ccxa5iv0h48n98z") (yanked #t)))

(define-public crate-tagger-4 (crate (name "tagger") (vers "4.0.1") (hash "0k55209nzmsjqw64vgfavi97cvgyg5k1m36yv4nrv6r7npjkfrbv") (yanked #t)))

(define-public crate-tagger-4 (crate (name "tagger") (vers "4.1.0") (hash "1d56pp5z14csqaiybqfacs5mdj0hha099cfrhy82sq1rzcz78g30") (yanked #t)))

(define-public crate-tagger-4 (crate (name "tagger") (vers "4.2.0") (hash "1gq6vykl8idix7hm31335y1jsr2yv3vjgjq4blgjl7agk3wa34wi")))

(define-public crate-tagger-4 (crate (name "tagger") (vers "4.2.1") (hash "0pl3gc1hgznl7hxnyakb6s3fv5qhsr7r1aqflfjwjxp5pb0qsjvq")))

(define-public crate-tagger-4 (crate (name "tagger") (vers "4.3.1") (hash "0x20zwxj1nhrq2fd3k1zq9x7jbsv2gc2dz5hpc1rzcv19cm92la8")))

(define-public crate-tagger-4 (crate (name "tagger") (vers "4.3.2") (hash "1i6mai98gvlp4z3fzv062zxazigacs7nsgdbghwkxdgmf3w06msp")))

(define-public crate-tagger-4 (crate (name "tagger") (vers "4.3.3") (hash "08dj0hg547avw32gcqm1gdfflwwyxwf27v106shs62yjgpy7ipbp")))

(define-public crate-tagger-4 (crate (name "tagger") (vers "4.3.4") (hash "0qsbsfml1adidq85x3ilaqba6xavy65ryvi8s16aw7axcifnzaka")))

(define-public crate-tagger-4 (crate (name "tagger") (vers "4.3.5") (hash "1if3chr1nhsdd6z09vi1x3cpxcrkjfi676z4n438b6nysrj9yk09")))

(define-public crate-taggie-0.1 (crate (name "taggie") (vers "0.1.0") (deps (list (crate-dep (name "fastrand") (req "^1.4.0") (default-features #t) (kind 2)) (crate-dep (name "taglib") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "tempfile") (req "^3.0.4") (default-features #t) (kind 0)))) (hash "0y6f1906brfgnhk1i78ddy0zlmyl02z87cx98f80y2lq403qrc0m")))

