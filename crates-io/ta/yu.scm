(define-module (crates-io ta yu) #:use-module (crates-io))

(define-public crate-tayum-0.1 (crate (name "tayum") (vers "0.1.0") (deps (list (crate-dep (name "hashbrown") (req "^0.14.3") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)))) (hash "1w20bn1d7f231bycpqc8ik59fs7240pplb3a5ayd0brklazm0bii")))

(define-public crate-tayum-cli-0.1 (crate (name "tayum-cli") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^4.4.12") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "tayum") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "0lrwa8ncpzyzzqwjbwqqnba4d5n6hadgk4hr6wxk901z3nx84c6v")))

