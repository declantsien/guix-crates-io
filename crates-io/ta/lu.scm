(define-module (crates-io ta lu) #:use-module (crates-io))

(define-public crate-talus-0.1 (crate (name "talus") (vers "0.1.0") (deps (list (crate-dep (name "cpython") (req "^0.4") (features (quote ("extension-module"))) (default-features #t) (kind 0)) (crate-dep (name "csv") (req "^1.1") (default-features #t) (kind 0)) (crate-dep (name "itertools") (req "^0.8.2") (default-features #t) (kind 0)) (crate-dep (name "kdtree") (req "^0.5.1") (default-features #t) (kind 0)) (crate-dep (name "petgraph") (req "^0.5.0") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.7.3") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0") (default-features #t) (kind 0)))) (hash "0if949im5fr1qd7h6rfbamjb92ykmlrrx2jw6hl6qx4wdzy8pjim")))

