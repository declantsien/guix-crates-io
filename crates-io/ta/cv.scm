(define-module (crates-io ta cv) #:use-module (crates-io))

(define-public crate-tacview-0.1 (crate (name "tacview") (vers "0.1.0") (deps (list (crate-dep (name "thiserror") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "zip") (req "^0.5") (features (quote ("deflate"))) (kind 0)))) (hash "1dq7lnv3wmhfazgvjlal0xysdjs2m6kq1kp4jpwmrsqj857i51zz")))

(define-public crate-tacview-0.1 (crate (name "tacview") (vers "0.1.1") (deps (list (crate-dep (name "thiserror") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "zip") (req "^0.5") (features (quote ("deflate"))) (kind 0)))) (hash "1a4q50s6fw4j1mcd8rx61d205hb0rzcgc6pj1zyaxw2y8nz1krkg")))

(define-public crate-tacview-0.1 (crate (name "tacview") (vers "0.1.2") (deps (list (crate-dep (name "thiserror") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "zip") (req "^0.5") (features (quote ("deflate"))) (kind 0)))) (hash "04mqx9xxmlvsya3afndxs03gsgg5dpvljmy0q795n9zzs6a3klpz")))

(define-public crate-tacview-0.1 (crate (name "tacview") (vers "0.1.3") (deps (list (crate-dep (name "thiserror") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "zip") (req "^0.6") (features (quote ("deflate"))) (kind 0)))) (hash "034zc6yn9ydm37wxb2dqn678xcny9bbka9a91pd6d4naza99frxr")))

(define-public crate-tacview-0.1 (crate (name "tacview") (vers "0.1.4") (deps (list (crate-dep (name "thiserror") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "zip") (req "^0.6") (features (quote ("deflate"))) (kind 0)))) (hash "10jxij0fiz5i1ypkrfrbhrf2sk8gg0szkyvbxnkr9d0giln7pb7l")))

(define-public crate-tacview-0.1 (crate (name "tacview") (vers "0.1.5") (deps (list (crate-dep (name "thiserror") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "zip") (req "^0.6") (features (quote ("deflate"))) (kind 0)))) (hash "1sbda7qcljrhzjl697awc3v6gd2fgmh1iri3wwb6qslq7lf1f7sx")))

