(define-module (crates-io ta fk) #:use-module (crates-io))

(define-public crate-tafkars-0.1 (crate (name "tafkars") (vers "0.1.0") (deps (list (crate-dep (name "serde") (req "^1.0.163") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.96") (default-features #t) (kind 0)))) (hash "0fp37lmm5wlk247fnrp2l3zzd45d71z0fx5q4wvaaplj6vhyrcbf")))

(define-public crate-tafkars-lemmy-0.1 (crate (name "tafkars-lemmy") (vers "0.1.0") (deps (list (crate-dep (name "actix-web") (req "^4.3.1") (default-features #t) (kind 0)) (crate-dep (name "env_logger") (req "^0.10.0") (default-features #t) (kind 0)) (crate-dep (name "lemmy_api_common") (req "^0.17.3") (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.11.18") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.163") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.96") (default-features #t) (kind 0)) (crate-dep (name "tafkars") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.40") (default-features #t) (kind 0)))) (hash "080sdii3lbx58kq0xkgxlh8sl0vd7p8ql7wddmq6cvy653pvx04d")))

