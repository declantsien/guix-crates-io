(define-module (crates-io ta gn) #:use-module (crates-io))

(define-public crate-tagname-0.1 (crate (name "tagname") (vers "0.1.0") (deps (list (crate-dep (name "tagname_derive") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "trybuild") (req "^1.0.71") (default-features #t) (kind 2)))) (hash "16vq3ph88v1y2ff8fs21q38va7w4q3glxpa89rfb3w9wb0av9szx") (yanked #t)))

(define-public crate-tagname-0.1 (crate (name "tagname") (vers "0.1.1") (deps (list (crate-dep (name "tagname_derive") (req "^0.1.1") (default-features #t) (kind 0)) (crate-dep (name "trybuild") (req "^1.0.71") (default-features #t) (kind 2)))) (hash "08gnk0k9cmwbxvxrw62layhcgabmhliqww6550jp580n1j6j48v6")))

(define-public crate-tagname-0.2 (crate (name "tagname") (vers "0.2.0") (deps (list (crate-dep (name "tagname_derive") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "trybuild") (req "^1.0.71") (default-features #t) (kind 2)))) (hash "1nbdc4r3clv7v87zf02l7q28y691r7l861mb585widsllwgkr6qf")))

(define-public crate-tagname-0.2 (crate (name "tagname") (vers "0.2.1") (deps (list (crate-dep (name "tagname_derive") (req "^0.2.1") (default-features #t) (kind 0)) (crate-dep (name "trybuild") (req "^1.0.71") (default-features #t) (kind 2)))) (hash "1bzysmph8r6f9wlzib0d5nispakb1vg6lfwiwzf6w72w181aygd5")))

(define-public crate-tagname-0.3 (crate (name "tagname") (vers "0.3.0") (deps (list (crate-dep (name "tagname_derive") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "trybuild") (req "^1.0.71") (default-features #t) (kind 2)))) (hash "1l9lcfrbzjvyvq31yxlbadqwpq9rjjxql96hsgsgx9h84zdln7fs")))

(define-public crate-tagname-0.3 (crate (name "tagname") (vers "0.3.1") (deps (list (crate-dep (name "tagname_derive") (req "^0.3.1") (default-features #t) (kind 0)) (crate-dep (name "trybuild") (req "^1.0.71") (default-features #t) (kind 2)))) (hash "16nxighrl2x0jc9xqkbzf0kw004hzyicigpw75a46bl32hapjvc2")))

(define-public crate-tagname_derive-0.1 (crate (name "tagname_derive") (vers "0.1.0") (deps (list (crate-dep (name "proc-macro2") (req "^1.0.47") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (default-features #t) (kind 0)))) (hash "0y4l36055r9bvsm758w7c7874j6jgrsw86fjhicn34fcd38n58j3") (yanked #t)))

(define-public crate-tagname_derive-0.1 (crate (name "tagname_derive") (vers "0.1.1") (deps (list (crate-dep (name "proc-macro2") (req "^1.0.47") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (default-features #t) (kind 0)))) (hash "02hf2kgzglxyryfslf63szdydx65qjpd8mdrcb7j5glfgnm3zqh6")))

(define-public crate-tagname_derive-0.2 (crate (name "tagname_derive") (vers "0.2.0") (deps (list (crate-dep (name "proc-macro2") (req "^1.0.47") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "0gyak8dvxcmxr9p6mppzj56c5db0pl2dp7za6fghwdsw9jpaikjf")))

(define-public crate-tagname_derive-0.2 (crate (name "tagname_derive") (vers "0.2.1") (deps (list (crate-dep (name "proc-macro2") (req "^1.0.47") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "0yzvc806429i3r28d582qlc17k5f495i6v3h1vwadbzshc7c5h8a")))

(define-public crate-tagname_derive-0.3 (crate (name "tagname_derive") (vers "0.3.0") (deps (list (crate-dep (name "proc-macro2") (req "^1.0.47") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "1bf3w16kzvz6zpbmi8nsijgd1ajxdhhsd6lv118xks1gqffmg3n8")))

(define-public crate-tagname_derive-0.3 (crate (name "tagname_derive") (vers "0.3.1") (deps (list (crate-dep (name "proc-macro2") (req "^1.0.47") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "0almvx52g5f7ifwr1rzw28zkyyvgpdh3pm8zjk2si523nvrd1mf6")))

