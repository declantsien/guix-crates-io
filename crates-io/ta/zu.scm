(define-module (crates-io ta zu) #:use-module (crates-io))

(define-public crate-tazui-0.1 (crate (name "tazui") (vers "0.1.0") (deps (list (crate-dep (name "crossterm") (req "^0.26.0") (default-features #t) (kind 0)) (crate-dep (name "taz") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "0yp9mq1wb70klnv4g9mb8k9m7hrxp1kqbsk9npnbm7mk9i2nswin")))

(define-public crate-tazui-0.1 (crate (name "tazui") (vers "0.1.1") (deps (list (crate-dep (name "crossterm") (req "^0.26.0") (default-features #t) (kind 0)) (crate-dep (name "taz") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "1grsp7sl88j9w902xd7193x16bk01clb0llcld4bws2rlixw6g55")))

