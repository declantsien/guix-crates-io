(define-module (crates-io ta us) #:use-module (crates-io))

(define-public crate-taus-0.1 (crate (name "taus") (vers "0.1.0") (hash "19knp93vmd2sd1glpy8j9gplzj0irhqb6bk88hwnx3p1992acch6")))

(define-public crate-taus-cli-0.1 (crate (name "taus-cli") (vers "0.1.0") (hash "0x4qdps6idqz3qg8nl5gq4pjz2dlqnllj1zyga2xvmkp3rmdpxn6")))

