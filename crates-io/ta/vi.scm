(define-module (crates-io ta vi) #:use-module (crates-io))

(define-public crate-tavi-0.0.1 (crate (name "tavi") (vers "0.0.1") (hash "04w4qz2pmvrlq5w2nmjlcjmjhc897mz33z7cd5nm4dljmh25ag74") (yanked #t)))

(define-public crate-tavily-0.1 (crate (name "tavily") (vers "0.1.0") (hash "124zpm99zsr164dpl2218s43amjnl7h52ii0wsvvz59bx97h664x")))

(define-public crate-tavily-0.2 (crate (name "tavily") (vers "0.2.0") (deps (list (crate-dep (name "reqwest") (req "^0.12.4") (features (quote ("json"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.198") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "1ms80cqyysz7xj66359i2gbyas4m7aba71f4vm8n34r0zc598jin")))

(define-public crate-tavily-0.2 (crate (name "tavily") (vers "0.2.1") (deps (list (crate-dep (name "reqwest") (req "^0.12.4") (features (quote ("json"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.198") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "04v54wbqm98gxiyz4a96042dxm58wzd3vk1kv9kcpvg1cssjsp1f")))

(define-public crate-tavily-0.2 (crate (name "tavily") (vers "0.2.2") (deps (list (crate-dep (name "reqwest") (req "^0.12.4") (features (quote ("json"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.198") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "0fzwqykar5f7yxgd670fm85ab66fmd47bjm9qwqrn6398wwp346g")))

(define-public crate-tavily-1 (crate (name "tavily") (vers "1.0.0") (deps (list (crate-dep (name "reqwest") (req "^0.12.4") (features (quote ("json"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.198") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "1bjfzis5w0jqslwmlflkqd35p248ln05pxkv538g5x8szmay1655")))

