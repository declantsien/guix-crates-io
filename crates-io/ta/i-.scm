(define-module (crates-io ta i-) #:use-module (crates-io))

(define-public crate-tai-lib-0.0.0 (crate (name "tai-lib") (vers "0.0.0") (hash "00vqy54yhyjn17rw2fc9h1n3lmw6b264qssv7fqzapb74i34b3s9")))

(define-public crate-tai-rs-0.1 (crate (name "tai-rs") (vers "0.1.0") (hash "1g9mwn576bnv2wk8qwyvs88m24dbq19g6lnlbl0z2n4cmr9s1dgb")))

(define-public crate-tai-time-0.1 (crate (name "tai-time") (vers "0.1.0") (deps (list (crate-dep (name "chrono") (req "^0.4.31") (optional #t) (kind 0)) (crate-dep (name "serde") (req "^1") (features (quote ("derive"))) (optional #t) (kind 0)))) (hash "043a1xmn8ialxzfx5l3zvfmw1k1m4id39g1mvhml2awb2368jj26") (features (quote (("std") ("default" "std")))) (v 2) (features2 (quote (("serde" "dep:serde") ("chrono" "dep:chrono")))) (rust-version "1.64")))

(define-public crate-tai-time-0.2 (crate (name "tai-time") (vers "0.2.0") (deps (list (crate-dep (name "chrono") (req "^0.4.31") (optional #t) (kind 0)) (crate-dep (name "libc") (req "^0.2.68") (optional #t) (kind 0)) (crate-dep (name "serde") (req "^1") (features (quote ("derive"))) (optional #t) (kind 0)))) (hash "0py3l1cd0pn3cl07jdjs8hyrhc8sj9yza7kfx7cbw2dlgyqbkvcj") (features (quote (("std") ("default" "std")))) (v 2) (features2 (quote (("tai_clock" "dep:libc") ("serde" "dep:serde") ("chrono" "dep:chrono")))) (rust-version "1.64")))

(define-public crate-tai-time-0.2 (crate (name "tai-time") (vers "0.2.1") (deps (list (crate-dep (name "chrono") (req "^0.4.31") (optional #t) (kind 0)) (crate-dep (name "libc") (req "^0.2.68") (optional #t) (kind 0)) (crate-dep (name "serde") (req "^1") (features (quote ("derive"))) (optional #t) (kind 0)))) (hash "155kabqbdcch25rvpvgbkzxsaqyg19hv19w08fn2sczkb713kwar") (features (quote (("std") ("default" "std")))) (v 2) (features2 (quote (("tai_clock" "dep:libc") ("serde" "dep:serde") ("chrono" "dep:chrono")))) (rust-version "1.64")))

(define-public crate-tai-time-0.2 (crate (name "tai-time") (vers "0.2.2") (deps (list (crate-dep (name "chrono") (req "^0.4.31") (optional #t) (kind 0)) (crate-dep (name "libc") (req "^0.2.68") (optional #t) (kind 0)) (crate-dep (name "serde") (req "^1") (features (quote ("derive"))) (optional #t) (kind 0)))) (hash "17ka8yfx8k04vmx7c9vf3ml4f3yy4d7m26s8rl7s69wzw9dapq64") (features (quote (("std") ("default" "std")))) (v 2) (features2 (quote (("tai_clock" "dep:libc") ("serde" "dep:serde") ("chrono" "dep:chrono")))) (rust-version "1.64")))

(define-public crate-tai-time-0.3 (crate (name "tai-time") (vers "0.3.0") (deps (list (crate-dep (name "chrono") (req "^0.4.31") (optional #t) (kind 0)) (crate-dep (name "nix") (req "^0.26") (features (quote ("time"))) (optional #t) (kind 0)) (crate-dep (name "serde") (req "^1") (features (quote ("derive"))) (optional #t) (kind 0)))) (hash "1kwf406d1f20jy32nlz2xzp76v0r76wsz75r8svpwlzb7ybv1ngm") (features (quote (("std") ("default" "std")))) (v 2) (features2 (quote (("tai_clock" "dep:nix") ("serde" "dep:serde") ("chrono" "dep:chrono")))) (rust-version "1.64")))

(define-public crate-tai-time-0.3 (crate (name "tai-time") (vers "0.3.1") (deps (list (crate-dep (name "chrono") (req "^0.4.31") (optional #t) (kind 0)) (crate-dep (name "nix") (req "^0.26") (features (quote ("time"))) (optional #t) (kind 0)) (crate-dep (name "serde") (req "^1") (features (quote ("derive"))) (optional #t) (kind 0)))) (hash "1mab215vkglswxbd43z2h9hivwrg62kjjai6v4ssfbnkz3w53s4w") (features (quote (("std") ("default" "std")))) (v 2) (features2 (quote (("tai_clock" "dep:nix") ("serde" "dep:serde") ("chrono" "dep:chrono")))) (rust-version "1.64")))

(define-public crate-tai-util-0.0.0 (crate (name "tai-util") (vers "0.0.0") (hash "0jc7qiirw492hrnx0ngpcd42h4xhq3vp82hw8lwkzgi8jpck8wzc")))

