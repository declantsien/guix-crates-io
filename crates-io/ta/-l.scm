(define-module (crates-io ta -l) #:use-module (crates-io))

(define-public crate-ta-lib-0.1 (crate (name "ta-lib") (vers "0.1.0") (deps (list (crate-dep (name "concat-idents") (req "^1.1.3") (default-features #t) (kind 0)) (crate-dep (name "ta-lib-sys") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "0q5g49xvsa6288gmp6zrjnz0avmsvfkd54j6ac2asm4f9wsrv9rj") (features (quote (("use_system_lib" "ta-lib-sys/use_system_lib"))))))

(define-public crate-ta-lib-0.1 (crate (name "ta-lib") (vers "0.1.1") (deps (list (crate-dep (name "concat-idents") (req "^1.1.3") (default-features #t) (kind 0)) (crate-dep (name "ta-lib-sys") (req "^0.1.1") (default-features #t) (kind 0)))) (hash "0zcpg1rqhb4dyw40aapabvm1dyw5r5d4sabha8s1jad3kvb3yqb3") (features (quote (("use_system_lib" "ta-lib-sys/use_system_lib"))))))

(define-public crate-ta-lib-0.1 (crate (name "ta-lib") (vers "0.1.2") (deps (list (crate-dep (name "concat-idents") (req "^1.1.4") (default-features #t) (kind 0)) (crate-dep (name "ta-lib-sys") (req "^0.1.2") (default-features #t) (kind 0)))) (hash "0njkdg2ay8rfjvsagwx86pchbza2nkqafx3gqyqa9wlnjq1p80ps") (features (quote (("use_system_lib" "ta-lib-sys/use_system_lib"))))))

(define-public crate-ta-lib-sys-0.1 (crate (name "ta-lib-sys") (vers "0.1.0") (deps (list (crate-dep (name "bindgen") (req "^0.59.1") (default-features #t) (kind 1)))) (hash "00a5p7hcww4wgrxlyjghi2khprcwbp0zhmllgqr3xwfa605idwza") (features (quote (("use_system_lib"))))))

(define-public crate-ta-lib-sys-0.1 (crate (name "ta-lib-sys") (vers "0.1.1") (deps (list (crate-dep (name "bindgen") (req "^0.59.1") (default-features #t) (kind 1)))) (hash "0k8crn2q8ra85byajrlipmrkxdal8g7hgf252iwyk8zdy6zhxzyd") (features (quote (("use_system_lib"))))))

(define-public crate-ta-lib-sys-0.1 (crate (name "ta-lib-sys") (vers "0.1.2") (deps (list (crate-dep (name "bindgen") (req "^0.63.0") (default-features #t) (kind 1)))) (hash "0s4a60nmvcbffz8a9jrpz3zzcm2mq38ps1axml4vvdmyc63mz4w1") (features (quote (("use_system_lib"))))))

(define-public crate-ta-lib-wrapper-0.1 (crate (name "ta-lib-wrapper") (vers "0.1.0") (deps (list (crate-dep (name "bindgen") (req "^0.26.3") (default-features #t) (kind 1)))) (hash "1nnz457745wkayndnanprhg4mw5797pb198vzs0bg1rf2zqinsvc")))

(define-public crate-ta-lib-wrapper-0.1 (crate (name "ta-lib-wrapper") (vers "0.1.1") (deps (list (crate-dep (name "bindgen") (req "^0.26.3") (default-features #t) (kind 1)))) (hash "00yhklmndg0x9z5mk66cjixfg6y7pphh9x6rnbylz66fgnz4qj0k")))

(define-public crate-ta-lib-wrapper-0.2 (crate (name "ta-lib-wrapper") (vers "0.2.0") (deps (list (crate-dep (name "bindgen") (req "^0.51.1") (default-features #t) (kind 1)))) (hash "19wg6yqmk03rkkpip8lmj9fvr932s2jw5vxwgqx4a25mi9fxwgmf")))

