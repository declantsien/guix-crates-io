(define-module (crates-io ta ub) #:use-module (crates-io))

(define-public crate-taubyte-sdk-0.1 (crate (name "taubyte-sdk") (vers "0.1.4") (deps (list (crate-dep (name "bytes") (req "^1.3.0") (default-features #t) (kind 0)) (crate-dep (name "cid") (req "^0.7.0") (default-features #t) (kind 0)) (crate-dep (name "http") (req "^0.2") (default-features #t) (kind 0)))) (hash "17qsy0a7hpnq9408jw47ppywf9bxlzdzy53kqrmgs4y9hn7dmqsi")))

(define-public crate-taubyte-sdk-0.1 (crate (name "taubyte-sdk") (vers "0.1.5") (deps (list (crate-dep (name "bytes") (req "^1.3.0") (default-features #t) (kind 0)) (crate-dep (name "cid") (req "^0.7.0") (default-features #t) (kind 0)) (crate-dep (name "http") (req "^0.2") (default-features #t) (kind 0)))) (hash "10296qh9y5wkb7sf2fw14kw854458sm0qfiww1rzp3dynznzyj7f")))

(define-public crate-taubyte-sdk-0.1 (crate (name "taubyte-sdk") (vers "0.1.2") (deps (list (crate-dep (name "bytes") (req "^1.3.0") (default-features #t) (kind 0)) (crate-dep (name "cid") (req "^0.7.0") (default-features #t) (kind 0)) (crate-dep (name "http") (req "^0.2") (default-features #t) (kind 0)))) (hash "1wvp3ljz3n1sg962f6qj0jy1r2fk8rp1kxbvxjyp9hvzpk5b55mq")))

(define-public crate-taubyte-sdk-0.1 (crate (name "taubyte-sdk") (vers "0.1.6") (deps (list (crate-dep (name "bytes") (req "^1.3.0") (default-features #t) (kind 0)) (crate-dep (name "cid") (req "^0.7.0") (default-features #t) (kind 0)) (crate-dep (name "http") (req "^0.2") (default-features #t) (kind 0)))) (hash "0cnpvrcl7fzqzx8n14rsxqdckh49kfxhbwq861cdcz0a4sx80ncv")))

