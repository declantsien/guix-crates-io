(define-module (crates-io ta hi) #:use-module (crates-io))

(define-public crate-tahir-hr-0.1 (crate (name "tahir-hr") (vers "0.1.0") (deps (list (crate-dep (name "sankaku") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "tahir_hub") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "05ykaqw24nahqgzh6lrckdbk9n1s717v5j0x3r0svhr47n18x6c0")))

(define-public crate-tahir_hub-0.1 (crate (name "tahir_hub") (vers "0.1.0") (deps (list (crate-dep (name "cfg-if") (req "^1.0.0") (default-features #t) (kind 0)) (crate-dep (name "sankaku") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "0wbh9wv7wv36i7qccxqzp2ypayzp3z9jipymq60ngxy4djq1xk4k")))

