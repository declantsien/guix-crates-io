(define-module (crates-io ta mp) #:use-module (crates-io))

(define-public crate-tampon-1 (crate (name "tampon") (vers "1.0.0") (deps (list (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)))) (hash "0grfn2vhqsqzabzjxabjbnas72qjn9sqn1jkk53aqzsic7vhj5ya")))

