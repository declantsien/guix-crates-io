(define-module (crates-io d2 _i) #:use-module (crates-io))

(define-public crate-d2_iterators-0.1 (crate (name "d2_iterators") (vers "0.1.0") (deps (list (crate-dep (name "itertools") (req "^0.8.0") (default-features #t) (kind 0)))) (hash "1z9fj2rkh99dsb6phsys7i12l7y42p8s3m8p1avxq6vzginh07bs")))

(define-public crate-d2_iterators_in_rust-0.1 (crate (name "d2_iterators_in_rust") (vers "0.1.0") (deps (list (crate-dep (name "itertools") (req "^0.8.0") (default-features #t) (kind 0)))) (hash "02rxqhjyhm21h6p8kzgzj1j1j9yg5amcfj8a4d402vsc3lcqj7m5")))

(define-public crate-d2_iterators_js-0.1 (crate (name "d2_iterators_js") (vers "0.1.0") (deps (list (crate-dep (name "itertools") (req "^0.9.0") (default-features #t) (kind 0)))) (hash "0sy7jfdmr8ibd2mxxkvbp8ddjfp6mma1ahlapaxbwm7d6qnjcqhq")))

(define-public crate-d2_iterators_rustc-0.1 (crate (name "d2_iterators_rustc") (vers "0.1.0") (deps (list (crate-dep (name "itertools") (req "^0.8.2") (default-features #t) (kind 0)))) (hash "01hyzsz40q09w7cafb36gg745wb7lfjqy0g0j21r24k0xiha1x9s") (yanked #t)))

(define-public crate-d2_iterators_rychan14-0.1 (crate (name "d2_iterators_rychan14") (vers "0.1.0") (deps (list (crate-dep (name "itertools") (req "^0.9.0") (default-features #t) (kind 0)))) (hash "1m0wvb7xvy7xlqhqag8z35xzd82hi80rv0wwz5gnq9q3jdilc8hg")))

(define-public crate-d2_iterators_smc-0.1 (crate (name "d2_iterators_smc") (vers "0.1.0") (deps (list (crate-dep (name "itertools") (req "^0.8.0") (default-features #t) (kind 0)))) (hash "0dlx0hq7dym9kdkimqhljmjfx5hf0a3rvxa876ih5annqsffcz8n")))

(define-public crate-d2_iterators_smc-0.1 (crate (name "d2_iterators_smc") (vers "0.1.1") (deps (list (crate-dep (name "itertools") (req "^0.8.0") (default-features #t) (kind 0)))) (hash "05ldjfsic8ic1w2pn2vyxgz2b0myix2v6wdgfllszrxnjw3r3i02")))

