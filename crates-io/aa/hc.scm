(define-module (crates-io aa hc) #:use-module (crates-io))

(define-public crate-aahc-0.1 (crate (name "aahc") (vers "0.1.0") (deps (list (crate-dep (name "async-compat") (req "^0.1") (default-features #t) (kind 2)) (crate-dep (name "futures-core") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "futures-executor") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "futures-io") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "httparse") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^0.2") (features (quote ("dns" "io-util" "rt-core" "tcp"))) (default-features #t) (kind 2)))) (hash "095pcsb328d92icjz2a66fiwv48zr9i15ix437mk839gk07ncqfj") (features (quote (("detailed-errors") ("default" "detailed-errors"))))))

