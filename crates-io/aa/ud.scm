(define-module (crates-io aa ud) #:use-module (crates-io))

(define-public crate-aaudio-0.1 (crate (name "aaudio") (vers "0.1.0") (deps (list (crate-dep (name "aaudio-sys") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "1kvjgq3b91x3138382jwp4c9w2jqffsdbz7k8ppkqx4x6whlybbk") (yanked #t)))

(define-public crate-aaudio-0.1 (crate (name "aaudio") (vers "0.1.1") (deps (list (crate-dep (name "aaudio-sys") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "1a480scwfvjfm9kk2nyq7hs9z5k67i1m787bxyrf609kask7nvkl")))

(define-public crate-aaudio-sys-0.1 (crate (name "aaudio-sys") (vers "0.1.0") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "15j5793313lhd0dz56igp8wqqjcnbspkdmvn7d7g2sw8cvwanwy0")))

