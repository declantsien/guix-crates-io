(define-module (crates-io aa -c) #:use-module (crates-io))

(define-public crate-aa-colour-0.1 (crate (name "aa-colour") (vers "0.1.1") (deps (list (crate-dep (name "displaydoc") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0") (default-features #t) (kind 0)))) (hash "14d7mipfkv3jn1zh6s429852nis23c7abz1sh20p9flvlrgn6sdq")))

