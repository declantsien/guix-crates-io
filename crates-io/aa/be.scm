(define-module (crates-io aa be) #:use-module (crates-io))

(define-public crate-aabel-bloom-rs-0.1 (crate (name "aabel-bloom-rs") (vers "0.1.0") (deps (list (crate-dep (name "aabel-multihash-rs") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "bitvec") (req "^1.0") (default-features #t) (kind 0)))) (hash "0mw1dpfqx51zj7shhcdway19sbvvm379hhjjf93a01za7izgijym")))

(define-public crate-aabel-bloom-rs-0.1 (crate (name "aabel-bloom-rs") (vers "0.1.1") (deps (list (crate-dep (name "aabel-multihash-rs") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "bitvec") (req "^1.0") (default-features #t) (kind 0)))) (hash "0rbh4bhap6l82x2wmkgsi8x6qma5k86p6ww6k0inq7ygzva5hzbr")))

(define-public crate-aabel-hashmap-rs-0.1 (crate (name "aabel-hashmap-rs") (vers "0.1.0") (hash "1mb2fnydwx97dn0jw5c1jrpcv9kvn97ivj5c9m38rx3mq4i7kh6l")))

(define-public crate-aabel-identifier-rs-0.1 (crate (name "aabel-identifier-rs") (vers "0.1.0") (hash "0kqd01iishdx1ffixng5rbxf58nb7czygnk80520ivfx63wz06fb")))

(define-public crate-aabel-identifier-rs-0.1 (crate (name "aabel-identifier-rs") (vers "0.1.1") (hash "1b0hapygc3al7dbmhpgbd47zq4cfh7jj02dy5kb2r8w47dcq6v4y")))

(define-public crate-aabel-identifier-rs-0.1 (crate (name "aabel-identifier-rs") (vers "0.1.2") (hash "176qdk3pxadc4vs1g3d95303d79vc0xlc5xgx27r3hfdlgn35c98")))

(define-public crate-aabel-multihash-rs-0.1 (crate (name "aabel-multihash-rs") (vers "0.1.0") (deps (list (crate-dep (name "rand") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "siphasher") (req "^1.0") (default-features #t) (kind 0)))) (hash "0d6qwlh1lgz1npfjaqwid98q5wxh3nhhil7mqir2n3jl5kh3ip05")))

(define-public crate-aabel-multihash-rs-0.1 (crate (name "aabel-multihash-rs") (vers "0.1.1") (deps (list (crate-dep (name "rand") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "siphasher") (req "^1.0") (default-features #t) (kind 0)))) (hash "11wvvgcaac302mfj2zc2and99pc8q3pbdrkh6y82y7vyyxz8q3gg")))

(define-public crate-aabel-multihash-rs-0.1 (crate (name "aabel-multihash-rs") (vers "0.1.2") (deps (list (crate-dep (name "rand") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "siphasher") (req "^1.0") (default-features #t) (kind 0)))) (hash "0k0wklv301wi8avck0x1vy3jsv3r4lzjfj02cyrms3slyjvv9bm2")))

