(define-module (crates-io aa rc) #:use-module (crates-io))

(define-public crate-aarc-0.0.1 (crate (name "aarc") (vers "0.0.1") (hash "1ihxciqs5zg64hc7bl5ylb0zgqyn2i3g5nfl6fipzwy846db8cga") (yanked #t)))

(define-public crate-aarc-0.1 (crate (name "aarc") (vers "0.1.0") (deps (list (crate-dep (name "rand") (req "^0.8") (default-features #t) (kind 2)))) (hash "1inris25fjbpxn0qx0jilj2vfhq5kc76disl454bm3180lm9daqc")))

(define-public crate-aarc-0.2 (crate (name "aarc") (vers "0.2.0") (deps (list (crate-dep (name "rand") (req "^0.8") (default-features #t) (kind 2)))) (hash "1yy9pxk2cvjn8zbymrwk4qdgbfcmrrwim2lbgqzbld2v60hp2wld")))

(define-public crate-aarc-0.2 (crate (name "aarc") (vers "0.2.1") (deps (list (crate-dep (name "rand") (req "^0.8") (default-features #t) (kind 2)))) (hash "1nq10712w1srs0vx3ga1gkyg6fis3ql459r5pdh9qcnl8v9zxq5g")))

(define-public crate-aarch64-0.0.1 (crate (name "aarch64") (vers "0.0.1") (hash "1c9q5xz8sp7dyls6j740mih3hhsknh6s8maaq5ga5zyzlkvzcxcf") (yanked #t)))

(define-public crate-aarch64-0.0.2 (crate (name "aarch64") (vers "0.0.2") (hash "1hk68i0q7yr71j99snsys24r0v2cij8xnsnzi4xjkqr1kzlfqfbb") (yanked #t)))

(define-public crate-aarch64-0.0.3 (crate (name "aarch64") (vers "0.0.3") (hash "0wffg5vha9268iznjj9b3d1z7aahk3qwkm6223yp6km6wlh677vl")))

(define-public crate-aarch64-0.0.4 (crate (name "aarch64") (vers "0.0.4") (deps (list (crate-dep (name "cortex-a") (req "^3.0.4") (default-features #t) (kind 0)))) (hash "01lndkv4cz5c34h4dbfrrgsdd4llm0r7zif5yf9pbnibqr6ahnag")))

(define-public crate-aarch64-0.0.5 (crate (name "aarch64") (vers "0.0.5") (deps (list (crate-dep (name "cortex-a") (req "^5.0") (default-features #t) (kind 0)))) (hash "0xyyk99jlrq40zkzg17153d7a30r9g1y1z6i1xrmzz0bawm3aw32")))

(define-public crate-aarch64-0.0.6 (crate (name "aarch64") (vers "0.0.6") (deps (list (crate-dep (name "cortex-a") (req "^6.1") (default-features #t) (kind 0)))) (hash "1jv40z7hwasfxfpwl23jw0v42z2chyh3brm9gf0fap6zb8x9rdag")))

(define-public crate-aarch64-0.0.7 (crate (name "aarch64") (vers "0.0.7") (deps (list (crate-dep (name "cortex-a") (req "^7") (default-features #t) (kind 0)))) (hash "1cvsazqp27s5hdr2ii1ybr1y84wr9z7rpcw1l1n4zr4wi4qxyj4x")))

(define-public crate-aarch64-0.0.9 (crate (name "aarch64") (vers "0.0.9") (deps (list (crate-dep (name "cortex-a") (req "^8.1") (default-features #t) (kind 0)))) (hash "0gzr0xyvdk3hi1r67cvrs9svli2307dhflmg0gmbizp35yvhs4mg")))

(define-public crate-aarch64-0.0.10 (crate (name "aarch64") (vers "0.0.10") (deps (list (crate-dep (name "aarch64-cpu") (req "^9.3") (default-features #t) (kind 0)) (crate-dep (name "tock-registers") (req "0.8.*") (kind 0)))) (hash "1brv0z478pxm5ymxgfczpq5639zb0xwqv91wr40s25ddkdpkd1dw")))

(define-public crate-aarch64-0.0.11 (crate (name "aarch64") (vers "0.0.11") (deps (list (crate-dep (name "aarch64-cpu") (req "^9.3") (default-features #t) (kind 0)) (crate-dep (name "tock-registers") (req "0.8.*") (kind 0)))) (hash "1yhvp5dd618k5pvcqmaz35s9z2pfjd4hknqicl0n2a2fidfk9pqa") (features (quote (("nightly") ("default" "nightly"))))))

(define-public crate-aarch64-arch-helpers-0.1 (crate (name "aarch64-arch-helpers") (vers "0.1.0") (hash "18llf07d5719bp3xznzn3vv52w7f2xy747jgsf6madr2shs2acss") (features (quote (("mmu_helpers") ("errata_a57_813419") ("default" "cache_helpers" "mmu_helpers") ("cache_helpers"))))))

(define-public crate-aarch64-arch-helpers-0.2 (crate (name "aarch64-arch-helpers") (vers "0.2.0") (hash "11wycbs3211yi8kib80pnpx8xv5fg4dbd37kjmkpkdv0hx2zlrla") (features (quote (("mmu_helpers") ("errata_a57_813419") ("default" "cache_helpers" "mmu_helpers") ("cache_helpers"))))))

(define-public crate-aarch64-arch-helpers-0.2 (crate (name "aarch64-arch-helpers") (vers "0.2.1") (hash "0ink5r746z6szajfcpqszlfms2kbzp7phd9m9wjzqb9950c0iims") (features (quote (("mmu_helpers") ("errata_a57_813419") ("default" "cache_helpers" "mmu_helpers") ("cache_helpers"))))))

(define-public crate-aarch64-cpu-9 (crate (name "aarch64-cpu") (vers "9.0.0") (deps (list (crate-dep (name "tock-registers") (req "0.8.*") (optional #t) (kind 0)))) (hash "09bj3g9plv13ihdxxq6cx809sbd91786i4i7g5a6lqmsan7bikis") (features (quote (("nightly" "tock-registers") ("default" "nightly"))))))

(define-public crate-aarch64-cpu-9 (crate (name "aarch64-cpu") (vers "9.1.0") (deps (list (crate-dep (name "tock-registers") (req "0.8.*") (kind 0)))) (hash "0n6j5is057acsphdp2n97hlp2yy3vhk6bj4bn9kbv6frpl1400h7")))

(define-public crate-aarch64-cpu-9 (crate (name "aarch64-cpu") (vers "9.2.0") (deps (list (crate-dep (name "tock-registers") (req "0.8.*") (kind 0)))) (hash "1hg9aq6lw989q91f1mdn4r66ypn4j87b01l6x1l3wb5d499jcy1l")))

(define-public crate-aarch64-cpu-9 (crate (name "aarch64-cpu") (vers "9.3.0") (deps (list (crate-dep (name "tock-registers") (req "0.8.*") (kind 0)))) (hash "0mabmn0x053xva8d9hc9v9kzz72k05dksv0lmc755mfxk5iv9jrp")))

(define-public crate-aarch64-cpu-9 (crate (name "aarch64-cpu") (vers "9.3.1") (deps (list (crate-dep (name "tock-registers") (req "0.8.*") (kind 0)))) (hash "04gl3apm1wlsw0ddzd0pv0brcqs3c8g3gsr3cn7vlr85srbiqwgb")))

(define-public crate-aarch64-cpu-9 (crate (name "aarch64-cpu") (vers "9.4.0") (deps (list (crate-dep (name "tock-registers") (req "0.8.*") (kind 0)))) (hash "11v2nbxgpcyi3b05dqnpcdfwrbjbg0m04a6pdlcwi7y1c55a0hmc")))

(define-public crate-aarch64-esr-decoder-0.1 (crate (name "aarch64-esr-decoder") (vers "0.1.0") (deps (list (crate-dep (name "bit_field") (req "^0.10.1") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.30") (default-features #t) (kind 0)))) (hash "050j1cm2zmgh5mknsdcdq6ikmw8ja2avkv9kvvnbja8zgn6fw702")))

(define-public crate-aarch64-esr-decoder-0.2 (crate (name "aarch64-esr-decoder") (vers "0.2.0") (deps (list (crate-dep (name "bit_field") (req "^0.10.1") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.30") (default-features #t) (kind 0)))) (hash "1m3lk5cyhg0zcqkpjvy0zsrra6nj67hx2y223i5h59dcxc24rwyh")))

(define-public crate-aarch64-esr-decoder-0.2 (crate (name "aarch64-esr-decoder") (vers "0.2.1") (deps (list (crate-dep (name "bit_field") (req "^0.10.1") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.30") (default-features #t) (kind 0)))) (hash "03b2m41z3gl1lz907x3f3vbxq99j50ygg501488in1if09mcagay")))

(define-public crate-aarch64-esr-decoder-0.2 (crate (name "aarch64-esr-decoder") (vers "0.2.2") (deps (list (crate-dep (name "bit_field") (req "^0.10.1") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.30") (default-features #t) (kind 0)))) (hash "1bv1qkxd3gjsbb3ia7hhncwlv46k6ifn321200br9h1jppr10fwk")))

(define-public crate-aarch64-esr-decoder-0.2 (crate (name "aarch64-esr-decoder") (vers "0.2.3") (deps (list (crate-dep (name "bit_field") (req "^0.10.1") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.30") (default-features #t) (kind 0)))) (hash "191kzxbwxk81rdbf6c32sll0j7vg51ivfmkb6hz8f1l7bypr0g9r")))

(define-public crate-aarch64-intrinsics-1 (crate (name "aarch64-intrinsics") (vers "1.0.0") (hash "12msjdcf16rh8ypkqnnlplia8icx1gilakv6s91n8xgdk7d1d2dy")))

(define-public crate-aarch64-paging-0.1 (crate (name "aarch64-paging") (vers "0.1.0") (deps (list (crate-dep (name "bitflags") (req "^1.3") (default-features #t) (kind 0)))) (hash "13in4kp8a8700b369v70p8b36zp0hh50cbsxm29cib7w8im7jj9v")))

(define-public crate-aarch64-paging-0.2 (crate (name "aarch64-paging") (vers "0.2.0") (deps (list (crate-dep (name "bitflags") (req "^1.3.2") (default-features #t) (kind 0)))) (hash "17glgmqwlj3nb991r3z2rn67qwv6qysy7ggbrnjrxxa5hny72wjm")))

(define-public crate-aarch64-paging-0.2 (crate (name "aarch64-paging") (vers "0.2.1") (deps (list (crate-dep (name "bitflags") (req "^1.3.2") (default-features #t) (kind 0)))) (hash "073ql5qcp5gyd8saf0k5zhayzxrcrf16cvp43qrxzibqyli02kzh")))

(define-public crate-aarch64-paging-0.3 (crate (name "aarch64-paging") (vers "0.3.0") (deps (list (crate-dep (name "bitflags") (req "^1.3.2") (default-features #t) (kind 0)))) (hash "10k2838rfim7082k8bbixrdj4y05px5jdssb4n8871y2zp88jzpb") (features (quote (("default" "alloc") ("alloc"))))))

(define-public crate-aarch64-paging-0.4 (crate (name "aarch64-paging") (vers "0.4.0") (deps (list (crate-dep (name "bitflags") (req "^2.0.2") (default-features #t) (kind 0)))) (hash "1ln1ff5y1xllqjkrzr2zkhxx2wiz5892yjx6cm3cxbrc45z7cci8") (features (quote (("default" "alloc") ("alloc"))))))

(define-public crate-aarch64-paging-0.4 (crate (name "aarch64-paging") (vers "0.4.1") (deps (list (crate-dep (name "bitflags") (req "^2.0.2") (default-features #t) (kind 0)))) (hash "16fk7pan3az55w0a0pw0jq23rdr0iblvxyfl1a5r4mkzy7wzkl6j") (features (quote (("default" "alloc") ("alloc"))))))

(define-public crate-aarch64-paging-0.5 (crate (name "aarch64-paging") (vers "0.5.0") (deps (list (crate-dep (name "bitflags") (req "^2.0.2") (default-features #t) (kind 0)))) (hash "0qdghln29w8n3x5svz1ljbzxjwa55phf4nixy7n68ah5a95gk5b2") (features (quote (("default" "alloc") ("alloc"))))))

(define-public crate-aarch64-std-0.1 (crate (name "aarch64-std") (vers "0.1.0") (deps (list (crate-dep (name "aarch64-cpu") (req "^9.2.0") (default-features #t) (kind 0)) (crate-dep (name "tock-registers") (req "^0.8.0") (default-features #t) (kind 0)))) (hash "1y4c2mqj31zincmpz43llfxs8inb2wky0816za6mkpg466g6fill") (features (quote (("default" "alloc") ("alloc"))))))

(define-public crate-aarch64-std-0.1 (crate (name "aarch64-std") (vers "0.1.1") (deps (list (crate-dep (name "aarch64-cpu") (req "^9.2.0") (default-features #t) (kind 0)) (crate-dep (name "tock-registers") (req "^0.8.0") (default-features #t) (kind 0)))) (hash "04sdvirly7z8jzzhw2p22lfa36f2lm1am5805mhvp766lh7lf8qk") (features (quote (("default" "alloc") ("alloc"))))))

(define-public crate-aarch64-std-0.1 (crate (name "aarch64-std") (vers "0.1.2") (deps (list (crate-dep (name "aarch64-cpu") (req "^9.2.0") (default-features #t) (kind 0)) (crate-dep (name "tock-registers") (req "^0.8.0") (default-features #t) (kind 0)))) (hash "1253xig1fmxwpvjsm74m5hbi1nii3xqir1xikvmbi7c1xf8cs7a6") (features (quote (("default" "alloc") ("alloc"))))))

(define-public crate-aarch64_define-0.1 (crate (name "aarch64_define") (vers "0.1.0") (hash "0lwimi6dm1414appsnwc2ij6xbzdpwvw8g9jlm6iph171i4z8izg") (rust-version "1.78")))

(define-public crate-aarch64_define-0.1 (crate (name "aarch64_define") (vers "0.1.1") (hash "0nc0p85lxrd1xbvbffwrrdsgjy645w5z8vbhrkijx44jra2h2hch") (rust-version "1.78")))

(define-public crate-aarch64_pgtable-0.1 (crate (name "aarch64_pgtable") (vers "0.1.0") (hash "0bnpz54pvpsskcj2c11ssj5zg6s8sjgv41c77vfkvipvslaqfsb0") (rust-version "1.78")))

(define-public crate-aarch64_ptregs-0.1 (crate (name "aarch64_ptregs") (vers "0.1.0") (hash "010k4bvj77sb5sqnq8bca9iwla0ny8mqwqydgzlv6xrcjgx20ra6") (rust-version "1.78")))

