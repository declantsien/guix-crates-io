(define-module (crates-io aa ti) #:use-module (crates-io))

(define-public crate-aati-0.10 (crate (name "aati") (vers "0.10.2") (deps (list (crate-dep (name "ascii") (req "^1.1.0") (default-features #t) (kind 0)) (crate-dep (name "colored") (req "^2.0.0") (default-features #t) (kind 0)) (crate-dep (name "dirs") (req "^5.0.0") (default-features #t) (kind 0)) (crate-dep (name "hex") (req "^0.4.3") (default-features #t) (kind 0)) (crate-dep (name "humansize") (req "^2.1.3") (default-features #t) (kind 0)) (crate-dep (name "lz4") (req "^1.24.0") (default-features #t) (kind 0)) (crate-dep (name "ring") (req "^0.16.20") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.159") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "tar") (req "^0.4.38") (default-features #t) (kind 0)) (crate-dep (name "tiny_http") (req "^0.12.0") (default-features #t) (kind 0)) (crate-dep (name "toml") (req "^0.7.3") (default-features #t) (kind 0)) (crate-dep (name "ureq") (req "^2.6.2") (default-features #t) (kind 0)))) (hash "154jq4p7w1qkz09r8nipfdhg7n6l0w206yc7lyjs8k3z6zbbwi9r")))

(define-public crate-aati-0.10 (crate (name "aati") (vers "0.10.3") (deps (list (crate-dep (name "ascii") (req "^1.1.0") (default-features #t) (kind 0)) (crate-dep (name "colored") (req "^2.0.0") (default-features #t) (kind 0)) (crate-dep (name "dirs") (req "^5.0.0") (default-features #t) (kind 0)) (crate-dep (name "hex") (req "^0.4.3") (default-features #t) (kind 0)) (crate-dep (name "humansize") (req "^2.1.3") (default-features #t) (kind 0)) (crate-dep (name "lz4") (req "^1.24.0") (default-features #t) (kind 0)) (crate-dep (name "ring") (req "^0.16.20") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.159") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "tar") (req "^0.4.38") (default-features #t) (kind 0)) (crate-dep (name "tiny_http") (req "^0.12.0") (default-features #t) (kind 0)) (crate-dep (name "toml") (req "^0.7.3") (default-features #t) (kind 0)) (crate-dep (name "ureq") (req "^2.6.2") (default-features #t) (kind 0)))) (hash "13zvrcip4ikp2d2ak2d2g4sd3i8awz0r9r5zjx0ny8dkcz1min8b")))

(define-public crate-aati-0.10 (crate (name "aati") (vers "0.10.4") (deps (list (crate-dep (name "ascii") (req "^1.1.0") (default-features #t) (kind 0)) (crate-dep (name "colored") (req "^2.0.0") (default-features #t) (kind 0)) (crate-dep (name "dirs") (req "^5.0.0") (default-features #t) (kind 0)) (crate-dep (name "hex") (req "^0.4.3") (default-features #t) (kind 0)) (crate-dep (name "humansize") (req "^2.1.3") (default-features #t) (kind 0)) (crate-dep (name "lz4") (req "^1.24.0") (default-features #t) (kind 0)) (crate-dep (name "ring") (req "^0.16.20") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.159") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "tar") (req "^0.4.38") (default-features #t) (kind 0)) (crate-dep (name "tiny_http") (req "^0.12.0") (default-features #t) (kind 0)) (crate-dep (name "toml") (req "^0.7.3") (default-features #t) (kind 0)) (crate-dep (name "ureq") (req "^2.6.2") (default-features #t) (kind 0)))) (hash "0r52m78f786gy59dmjkb6dc592shqwjpcm8szr2lfzjkjwi12dag")))

(define-public crate-aati-0.10 (crate (name "aati") (vers "0.10.5") (deps (list (crate-dep (name "ascii") (req "^1.1.0") (default-features #t) (kind 0)) (crate-dep (name "colored") (req "^2.0.0") (default-features #t) (kind 0)) (crate-dep (name "dirs") (req "^5.0.0") (default-features #t) (kind 0)) (crate-dep (name "hex") (req "^0.4.3") (default-features #t) (kind 0)) (crate-dep (name "humansize") (req "^2.1.3") (default-features #t) (kind 0)) (crate-dep (name "lz4") (req "^1.24.0") (default-features #t) (kind 0)) (crate-dep (name "ring") (req "^0.16.20") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.159") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "tar") (req "^0.4.38") (default-features #t) (kind 0)) (crate-dep (name "tiny_http") (req "^0.12.0") (default-features #t) (kind 0)) (crate-dep (name "toml") (req "^0.7.3") (default-features #t) (kind 0)) (crate-dep (name "ureq") (req "^2.6.2") (default-features #t) (kind 0)))) (hash "0v2xb0lvb5l4z2pb0rikykyv8lsawcvsig0qk195v7r55j3czpzv")))

(define-public crate-aati-0.10 (crate (name "aati") (vers "0.10.6") (deps (list (crate-dep (name "ascii") (req "^1.1.0") (default-features #t) (kind 0)) (crate-dep (name "colored") (req "^2.0.0") (default-features #t) (kind 0)) (crate-dep (name "dirs") (req "^5.0.0") (default-features #t) (kind 0)) (crate-dep (name "hex") (req "^0.4.3") (default-features #t) (kind 0)) (crate-dep (name "humansize") (req "^2.1.3") (default-features #t) (kind 0)) (crate-dep (name "lz4") (req "^1.24.0") (default-features #t) (kind 0)) (crate-dep (name "ring") (req "^0.16.20") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.159") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "tar") (req "^0.4.38") (default-features #t) (kind 0)) (crate-dep (name "tiny_http") (req "^0.12.0") (default-features #t) (kind 0)) (crate-dep (name "toml") (req "^0.7.3") (default-features #t) (kind 0)) (crate-dep (name "ureq") (req "^2.6.2") (default-features #t) (kind 0)))) (hash "0i27a87m3s3lddjnqv4z5dc64ai0dynkz6sm6ppxc9fr9fk7knr9")))

(define-public crate-aati-0.10 (crate (name "aati") (vers "0.10.7") (deps (list (crate-dep (name "ascii") (req "^1.1.0") (default-features #t) (kind 0)) (crate-dep (name "colored") (req "^2.0.0") (default-features #t) (kind 0)) (crate-dep (name "dirs") (req "^5.0.0") (default-features #t) (kind 0)) (crate-dep (name "hex") (req "^0.4.3") (default-features #t) (kind 0)) (crate-dep (name "humansize") (req "^2.1.3") (default-features #t) (kind 0)) (crate-dep (name "lz4") (req "^1.24.0") (default-features #t) (kind 0)) (crate-dep (name "ring") (req "^0.16.20") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.159") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "tar") (req "^0.4.38") (default-features #t) (kind 0)) (crate-dep (name "tiny_http") (req "^0.12.0") (default-features #t) (kind 0)) (crate-dep (name "toml") (req "^0.7.3") (default-features #t) (kind 0)) (crate-dep (name "ureq") (req "^2.6.2") (default-features #t) (kind 0)))) (hash "06r317yvld9nnk58806m2r8i5bwk8nkdn6d6743w79hq54wlxivl")))

(define-public crate-aati-0.10 (crate (name "aati") (vers "0.10.8") (deps (list (crate-dep (name "ascii") (req "^1.1.0") (default-features #t) (kind 0)) (crate-dep (name "colored") (req "^2.0.0") (default-features #t) (kind 0)) (crate-dep (name "dirs") (req "^5.0.0") (default-features #t) (kind 0)) (crate-dep (name "hex") (req "^0.4.3") (default-features #t) (kind 0)) (crate-dep (name "humansize") (req "^2.1.3") (default-features #t) (kind 0)) (crate-dep (name "lz4") (req "^1.24.0") (default-features #t) (kind 0)) (crate-dep (name "ring") (req "^0.16.20") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.159") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "tar") (req "^0.4.38") (default-features #t) (kind 0)) (crate-dep (name "tiny_http") (req "^0.12.0") (default-features #t) (kind 0)) (crate-dep (name "toml") (req "^0.7.3") (default-features #t) (kind 0)) (crate-dep (name "ureq") (req "^2.6.2") (default-features #t) (kind 0)))) (hash "0sqz692mb3fjxicxp92616rdksxg7b0hwkff843xcfq70byzacnz")))

(define-public crate-aati-0.10 (crate (name "aati") (vers "0.10.9") (deps (list (crate-dep (name "ascii") (req "^1.1.0") (default-features #t) (kind 0)) (crate-dep (name "colored") (req "^2.0.0") (default-features #t) (kind 0)) (crate-dep (name "dirs") (req "^5.0.0") (default-features #t) (kind 0)) (crate-dep (name "hex") (req "^0.4.3") (default-features #t) (kind 0)) (crate-dep (name "humansize") (req "^2.1.3") (default-features #t) (kind 0)) (crate-dep (name "lz4") (req "^1.24.0") (default-features #t) (kind 0)) (crate-dep (name "ring") (req "^0.16.20") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.159") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "tar") (req "^0.4.38") (default-features #t) (kind 0)) (crate-dep (name "tiny_http") (req "^0.12.0") (default-features #t) (kind 0)) (crate-dep (name "toml") (req "^0.7.3") (default-features #t) (kind 0)) (crate-dep (name "ureq") (req "^2.6.2") (default-features #t) (kind 0)))) (hash "0cn06gsj2nls741im9w2clyffjcq7xijljy02qn9r9cadsaa96c6")))

(define-public crate-aati-0.11 (crate (name "aati") (vers "0.11.0") (deps (list (crate-dep (name "ascii") (req "^1.1.0") (default-features #t) (kind 0)) (crate-dep (name "colored") (req "^2.0.0") (default-features #t) (kind 0)) (crate-dep (name "dirs") (req "^5.0.0") (default-features #t) (kind 0)) (crate-dep (name "hex") (req "^0.4.3") (default-features #t) (kind 0)) (crate-dep (name "humansize") (req "^2.1.3") (default-features #t) (kind 0)) (crate-dep (name "lz4") (req "^1.24.0") (default-features #t) (kind 0)) (crate-dep (name "ring") (req "^0.16.20") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.159") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "tar") (req "^0.4.38") (default-features #t) (kind 0)) (crate-dep (name "tiny_http") (req "^0.12.0") (default-features #t) (kind 0)) (crate-dep (name "toml") (req "^0.7.3") (default-features #t) (kind 0)) (crate-dep (name "ureq") (req "^2.6.2") (default-features #t) (kind 0)))) (hash "16q9m62jlxgczjq17dzn5n71gs92hjyk89lgg9pac53swk9lk1fp")))

(define-public crate-aati-0.11 (crate (name "aati") (vers "0.11.1") (deps (list (crate-dep (name "ascii") (req "^1.1.0") (default-features #t) (kind 0)) (crate-dep (name "colored") (req "^2.0.0") (default-features #t) (kind 0)) (crate-dep (name "dirs") (req "^5.0.0") (default-features #t) (kind 0)) (crate-dep (name "hex") (req "^0.4.3") (default-features #t) (kind 0)) (crate-dep (name "humansize") (req "^2.1.3") (default-features #t) (kind 0)) (crate-dep (name "lz4") (req "^1.24.0") (default-features #t) (kind 0)) (crate-dep (name "ring") (req "^0.16.20") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.159") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "tar") (req "^0.4.38") (default-features #t) (kind 0)) (crate-dep (name "tiny_http") (req "^0.12.0") (default-features #t) (kind 0)) (crate-dep (name "toml") (req "^0.7.3") (default-features #t) (kind 0)) (crate-dep (name "ureq") (req "^2.6.2") (default-features #t) (kind 0)))) (hash "1kpgfs2mgywaskzxgsxpymki0a864rdbjizfz8q009p7yzh9yrgi")))

(define-public crate-aati-0.11 (crate (name "aati") (vers "0.11.2") (deps (list (crate-dep (name "ascii") (req "^1.1.0") (default-features #t) (kind 0)) (crate-dep (name "colored") (req "^2.0.0") (default-features #t) (kind 0)) (crate-dep (name "dirs") (req "^5.0.0") (default-features #t) (kind 0)) (crate-dep (name "hex") (req "^0.4.3") (default-features #t) (kind 0)) (crate-dep (name "humansize") (req "^2.1.3") (default-features #t) (kind 0)) (crate-dep (name "lz4") (req "^1.24.0") (default-features #t) (kind 0)) (crate-dep (name "ring") (req "^0.16.20") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.159") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "tar") (req "^0.4.38") (default-features #t) (kind 0)) (crate-dep (name "tiny_http") (req "^0.12.0") (default-features #t) (kind 0)) (crate-dep (name "toml") (req "^0.7.3") (default-features #t) (kind 0)) (crate-dep (name "ureq") (req "^2.6.2") (default-features #t) (kind 0)))) (hash "0b55fsivpd0lr7xhm4wr3mixkqzcp3h5ng33hnnp38nznprlvb3l")))

(define-public crate-aati-0.11 (crate (name "aati") (vers "0.11.3") (deps (list (crate-dep (name "ascii") (req "^1.1.0") (default-features #t) (kind 0)) (crate-dep (name "colored") (req "^2.0.0") (default-features #t) (kind 0)) (crate-dep (name "dirs") (req "^5.0.0") (default-features #t) (kind 0)) (crate-dep (name "hex") (req "^0.4.3") (default-features #t) (kind 0)) (crate-dep (name "humansize") (req "^2.1.3") (default-features #t) (kind 0)) (crate-dep (name "lz4") (req "^1.24.0") (default-features #t) (kind 0)) (crate-dep (name "ring") (req "^0.16.20") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.159") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "tar") (req "^0.4.38") (default-features #t) (kind 0)) (crate-dep (name "tiny_http") (req "^0.12.0") (default-features #t) (kind 0)) (crate-dep (name "toml") (req "^0.7.3") (default-features #t) (kind 0)) (crate-dep (name "ureq") (req "^2.6.2") (default-features #t) (kind 0)))) (hash "1pwisap97mjhpxmffng7qjvv175fpkjwc8f4sayvxssgghz69zmd")))

(define-public crate-aati-0.11 (crate (name "aati") (vers "0.11.4") (deps (list (crate-dep (name "ascii") (req "^1.1.0") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^4.3.12") (features (quote ("string"))) (default-features #t) (kind 0)) (crate-dep (name "colored") (req "^2.0.0") (default-features #t) (kind 0)) (crate-dep (name "dirs") (req "^5.0.0") (default-features #t) (kind 0)) (crate-dep (name "hex") (req "^0.4.3") (default-features #t) (kind 0)) (crate-dep (name "humansize") (req "^2.1.3") (default-features #t) (kind 0)) (crate-dep (name "lz4") (req "^1.24.0") (default-features #t) (kind 0)) (crate-dep (name "ring") (req "^0.16.20") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.159") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "tar") (req "^0.4.38") (default-features #t) (kind 0)) (crate-dep (name "tiny_http") (req "^0.12.0") (default-features #t) (kind 0)) (crate-dep (name "toml") (req "^0.7.3") (default-features #t) (kind 0)) (crate-dep (name "ureq") (req "^2.6.2") (default-features #t) (kind 0)))) (hash "0xpj4rr71l7rjgcdkfy2nkfrrcm8zfgj4gcb9s1zd4y5mf1i64c0")))

(define-public crate-aati-0.11 (crate (name "aati") (vers "0.11.5") (deps (list (crate-dep (name "ascii") (req "^1.1.0") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^4.3.12") (features (quote ("string"))) (default-features #t) (kind 0)) (crate-dep (name "colored") (req "^2.0.0") (default-features #t) (kind 0)) (crate-dep (name "dirs") (req "^5.0.0") (default-features #t) (kind 0)) (crate-dep (name "hex") (req "^0.4.3") (default-features #t) (kind 0)) (crate-dep (name "humansize") (req "^2.1.3") (default-features #t) (kind 0)) (crate-dep (name "lz4") (req "^1.24.0") (default-features #t) (kind 0)) (crate-dep (name "ring") (req "^0.16.20") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.159") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "tar") (req "^0.4.38") (default-features #t) (kind 0)) (crate-dep (name "tiny_http") (req "^0.12.0") (default-features #t) (kind 0)) (crate-dep (name "toml") (req "^0.7.3") (default-features #t) (kind 0)) (crate-dep (name "ureq") (req "^2.6.2") (default-features #t) (kind 0)))) (hash "1x0ccpyr5bd2nw7qi399psd2hcwrkssdpxspgkd378y4rll7n38k")))

(define-public crate-aati-0.11 (crate (name "aati") (vers "0.11.6") (deps (list (crate-dep (name "ascii") (req "^1.1.0") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^4.3.12") (features (quote ("string"))) (default-features #t) (kind 0)) (crate-dep (name "colored") (req "^2.0.0") (default-features #t) (kind 0)) (crate-dep (name "dirs") (req "^5.0.0") (default-features #t) (kind 0)) (crate-dep (name "hex") (req "^0.4.3") (default-features #t) (kind 0)) (crate-dep (name "humansize") (req "^2.1.3") (default-features #t) (kind 0)) (crate-dep (name "lz4") (req "^1.24.0") (default-features #t) (kind 0)) (crate-dep (name "ring") (req "^0.16.20") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.159") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "tar") (req "^0.4.38") (default-features #t) (kind 0)) (crate-dep (name "tiny_http") (req "^0.12.0") (default-features #t) (kind 0)) (crate-dep (name "toml") (req "^0.7.3") (default-features #t) (kind 0)) (crate-dep (name "ureq") (req "^2.6.2") (default-features #t) (kind 0)))) (hash "0pjpdhz3ir02s44mlgwbdx8789jfp950ybmwrrbhjja2gda2badr")))

(define-public crate-aati-0.11 (crate (name "aati") (vers "0.11.7") (deps (list (crate-dep (name "ascii") (req "^1.1.0") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^4.3.12") (features (quote ("string"))) (default-features #t) (kind 0)) (crate-dep (name "clap_complete") (req "^4.3.2") (default-features #t) (kind 0)) (crate-dep (name "colored") (req "^2.0.0") (default-features #t) (kind 0)) (crate-dep (name "dirs") (req "^5.0.0") (default-features #t) (kind 0)) (crate-dep (name "hex") (req "^0.4.3") (default-features #t) (kind 0)) (crate-dep (name "humansize") (req "^2.1.3") (default-features #t) (kind 0)) (crate-dep (name "lz4") (req "^1.24.0") (default-features #t) (kind 0)) (crate-dep (name "ring") (req "^0.16.20") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.159") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "tar") (req "^0.4.38") (default-features #t) (kind 0)) (crate-dep (name "tiny_http") (req "^0.12.0") (default-features #t) (kind 0)) (crate-dep (name "toml") (req "^0.7.3") (default-features #t) (kind 0)) (crate-dep (name "ureq") (req "^2.6.2") (default-features #t) (kind 0)))) (hash "16bgwnj1736xwq4cqcvj1hwhxci6l7131y3rdnns70hqwpmpa95g")))

(define-public crate-aati-0.11 (crate (name "aati") (vers "0.11.8") (deps (list (crate-dep (name "ascii") (req "^1.1.0") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^4.3.12") (features (quote ("string"))) (default-features #t) (kind 0)) (crate-dep (name "clap_complete") (req "^4.3.2") (default-features #t) (kind 0)) (crate-dep (name "colored") (req "^2.0.0") (default-features #t) (kind 0)) (crate-dep (name "dirs") (req "^5.0.0") (default-features #t) (kind 0)) (crate-dep (name "hex") (req "^0.4.3") (default-features #t) (kind 0)) (crate-dep (name "humansize") (req "^2.1.3") (default-features #t) (kind 0)) (crate-dep (name "lz4") (req "^1.24.0") (default-features #t) (kind 0)) (crate-dep (name "ring") (req "^0.16.20") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.159") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "tar") (req "^0.4.38") (default-features #t) (kind 0)) (crate-dep (name "tiny_http") (req "^0.12.0") (default-features #t) (kind 0)) (crate-dep (name "toml") (req "^0.7.3") (default-features #t) (kind 0)) (crate-dep (name "ureq") (req "^2.6.2") (default-features #t) (kind 0)))) (hash "0av1zayagy26d2yx1m6l6w2wbxgq56dnj3271l1a1inc804cgvsc")))

(define-public crate-aati-0.12 (crate (name "aati") (vers "0.12.0") (deps (list (crate-dep (name "ascii") (req "^1.1.0") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^4.3.12") (features (quote ("string"))) (default-features #t) (kind 0)) (crate-dep (name "clap_complete") (req "^4.3.2") (default-features #t) (kind 0)) (crate-dep (name "colored") (req "^2.0.0") (default-features #t) (kind 0)) (crate-dep (name "dirs") (req "^5.0.0") (default-features #t) (kind 0)) (crate-dep (name "hex") (req "^0.4.3") (default-features #t) (kind 0)) (crate-dep (name "humansize") (req "^2.1.3") (default-features #t) (kind 0)) (crate-dep (name "lz4") (req "^1.24.0") (default-features #t) (kind 0)) (crate-dep (name "ring") (req "^0.16.20") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.159") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "tar") (req "^0.4.38") (default-features #t) (kind 0)) (crate-dep (name "tiny_http") (req "^0.12.0") (default-features #t) (kind 0)) (crate-dep (name "toml") (req "^0.7.3") (default-features #t) (kind 0)) (crate-dep (name "ureq") (req "^2.6.2") (default-features #t) (kind 0)))) (hash "0ymcmd2kmcci9m3840vg7xvjngi3cncxlrnsnjydsb6zz99j5sdv")))

(define-public crate-aati-0.12 (crate (name "aati") (vers "0.12.1") (deps (list (crate-dep (name "ascii") (req "^1.1.0") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^4.3.12") (features (quote ("string"))) (default-features #t) (kind 0)) (crate-dep (name "clap_complete") (req "^4.3.2") (default-features #t) (kind 0)) (crate-dep (name "colored") (req "^2.0.0") (default-features #t) (kind 0)) (crate-dep (name "dirs") (req "^5.0.0") (default-features #t) (kind 0)) (crate-dep (name "hex") (req "^0.4.3") (default-features #t) (kind 0)) (crate-dep (name "humansize") (req "^2.1.3") (default-features #t) (kind 0)) (crate-dep (name "lz4") (req "^1.24.0") (default-features #t) (kind 0)) (crate-dep (name "ring") (req "^0.16.20") (default-features #t) (kind 0)) (crate-dep (name "rustc_version") (req "^0.4.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.159") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "tar") (req "^0.4.38") (default-features #t) (kind 0)) (crate-dep (name "tiny_http") (req "^0.12.0") (default-features #t) (kind 0)) (crate-dep (name "toml") (req "^0.7.3") (default-features #t) (kind 0)) (crate-dep (name "ureq") (req "^2.6.2") (default-features #t) (kind 0)))) (hash "0900pp94cacfdi2h9j75qz901jc1cgxgnbvb0gpd3567ywnvf4iv")))

(define-public crate-aati-0.12 (crate (name "aati") (vers "0.12.2") (deps (list (crate-dep (name "ascii") (req "^1.1.0") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^4.3.12") (features (quote ("string"))) (default-features #t) (kind 0)) (crate-dep (name "clap_complete") (req "^4.3.2") (default-features #t) (kind 0)) (crate-dep (name "colored") (req "^2.0.0") (default-features #t) (kind 0)) (crate-dep (name "dirs") (req "^5.0.0") (default-features #t) (kind 0)) (crate-dep (name "hex") (req "^0.4.3") (default-features #t) (kind 0)) (crate-dep (name "humansize") (req "^2.1.3") (default-features #t) (kind 0)) (crate-dep (name "lz4") (req "^1.24.0") (default-features #t) (kind 0)) (crate-dep (name "ring") (req "^0.16.20") (default-features #t) (kind 0)) (crate-dep (name "rustc_version") (req "^0.4.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.159") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "tar") (req "^0.4.38") (default-features #t) (kind 0)) (crate-dep (name "tiny_http") (req "^0.12.0") (default-features #t) (kind 0)) (crate-dep (name "toml") (req "^0.7.3") (default-features #t) (kind 0)) (crate-dep (name "ureq") (req "^2.6.2") (default-features #t) (kind 0)))) (hash "1g93zzjyfzaz9lza1lys0zk2ch4vpjc8cnjw1hlfddc99slmvkn8")))

(define-public crate-aati_backend-0.1 (crate (name "aati_backend") (vers "0.1.0") (hash "0qri13faay43w3fjxjsw38c5kd0dxdmrx3jsc2jn74w2k21bkhc7")))

