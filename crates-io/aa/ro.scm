(define-module (crates-io aa ro) #:use-module (crates-io))

(define-public crate-aaron_art-0.1 (crate (name "aaron_art") (vers "0.1.0") (hash "0z54hd0k82yff7pwbyhx6d2a3mj80333vwd5jxxczyzbcfnl4nqg") (yanked #t)))

(define-public crate-aaron_art-0.1 (crate (name "aaron_art") (vers "0.1.1") (hash "0z55jblbg5qvklwp5wchxpi2xpc9jb5xdy7lv7vs704zx24z5z8f")))

(define-public crate-aaronia-rtsa-0.0.1 (crate (name "aaronia-rtsa") (vers "0.0.1") (deps (list (crate-dep (name "aaronia-rtsa-sys") (req "^0.0.1") (default-features #t) (kind 0)))) (hash "0pz4ixjfikpn4vfx2rw2vcfzwdwpg8q7scvlyvb7pqmb48064401")))

(define-public crate-aaronia-rtsa-0.0.3 (crate (name "aaronia-rtsa") (vers "0.0.3") (deps (list (crate-dep (name "aaronia-rtsa-sys") (req "^0.0.3") (default-features #t) (kind 0)) (crate-dep (name "gnuplot") (req "^0.0.37") (default-features #t) (kind 0)) (crate-dep (name "num-complex") (req "^0.4.2") (default-features #t) (kind 0)) (crate-dep (name "rustfft") (req "^6.1.0") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.38") (default-features #t) (kind 0)) (crate-dep (name "widestring") (req "^1.0.2") (default-features #t) (kind 0)))) (hash "1s90aqamrcy3dnz267z5w2mn62j1gf7jqs4vc33nnwjd8ajs4lvb")))

(define-public crate-aaronia-rtsa-0.0.4 (crate (name "aaronia-rtsa") (vers "0.0.4") (deps (list (crate-dep (name "aaronia-rtsa-sys") (req "^0.0.3") (default-features #t) (kind 0)) (crate-dep (name "gnuplot") (req "^0.0.37") (default-features #t) (kind 0)) (crate-dep (name "num-complex") (req "^0.4.2") (default-features #t) (kind 0)) (crate-dep (name "rustfft") (req "^6.1.0") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.38") (default-features #t) (kind 0)) (crate-dep (name "widestring") (req "^1.0.2") (default-features #t) (kind 0)))) (hash "0rrdkwadgx648ickk6nnhkxqjvzdpm7gbzrwca5nbqb3wn5rw1nq")))

(define-public crate-aaronia-rtsa-0.0.5 (crate (name "aaronia-rtsa") (vers "0.0.5") (deps (list (crate-dep (name "aaronia-rtsa-sys") (req "^0.0.4") (default-features #t) (kind 0)) (crate-dep (name "gnuplot") (req "^0.0.37") (default-features #t) (kind 2)) (crate-dep (name "num-complex") (req "^0.4.2") (default-features #t) (kind 0)) (crate-dep (name "rustfft") (req "^6.1.0") (default-features #t) (kind 2)) (crate-dep (name "thiserror") (req "^1.0.38") (default-features #t) (kind 0)) (crate-dep (name "widestring") (req "^1.0.2") (default-features #t) (kind 0)))) (hash "0d41izqgwrgy4xbwzzhp0k5hqa8jj1a12fnjvkdwh73kx1w2rdfp")))

(define-public crate-aaronia-rtsa-0.0.6 (crate (name "aaronia-rtsa") (vers "0.0.6") (deps (list (crate-dep (name "aaronia-rtsa-sys") (req "^0.0.4") (default-features #t) (kind 0)) (crate-dep (name "num-complex") (req "^0.4.2") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.38") (default-features #t) (kind 0)) (crate-dep (name "widestring") (req "^1.0.2") (default-features #t) (kind 0)) (crate-dep (name "gnuplot") (req "^0.0.37") (default-features #t) (kind 2)) (crate-dep (name "rustfft") (req "^6.1.0") (default-features #t) (kind 2)))) (hash "03jfxmzmffwdhw0z7qgdgi5anzfjz0jpwpw3c9dbnkm9zkqp4i6p")))

(define-public crate-aaronia-rtsa-sys-0.0.1 (crate (name "aaronia-rtsa-sys") (vers "0.0.1") (deps (list (crate-dep (name "bindgen") (req "^0.63") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "1q7936dv4q6gaxa1ik4gppapp55wf53z97h2fg370vdapk0agsml") (links "AaroniaRTSAAPI")))

(define-public crate-aaronia-rtsa-sys-0.0.3 (crate (name "aaronia-rtsa-sys") (vers "0.0.3") (deps (list (crate-dep (name "bindgen") (req "^0.63") (default-features #t) (kind 1)))) (hash "0b3457qbzjh8k4xvz8zyslqr5q46kpmhmbjyslypx4yfn2vgi9kv") (links "AaroniaRTSAAPI")))

(define-public crate-aaronia-rtsa-sys-0.0.4 (crate (name "aaronia-rtsa-sys") (vers "0.0.4") (deps (list (crate-dep (name "bindgen") (req "^0.63") (default-features #t) (kind 1)))) (hash "14d1nw052nbkj2pkqk6szdanpdkxqk0vqdrfyyg2lbx61ax2mc4h") (links "AaroniaRTSAAPI")))

