(define-module (crates-io aa a_) #:use-module (crates-io))

(define-public crate-aaa_csv_challenge-0.1 (crate (name "aaa_csv_challenge") (vers "0.1.0") (deps (list (crate-dep (name "failure") (req "^0.1.8") (default-features #t) (kind 0)) (crate-dep (name "failure_derive") (req "^0.1.8") (default-features #t) (kind 0)) (crate-dep (name "lazy-regex") (req "^2.2.1") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.5.4") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.3.23") (default-features #t) (kind 0)) (crate-dep (name "structopt-derive") (req "^0.4.16") (default-features #t) (kind 0)))) (hash "1bld5k30ms9vch0gc21rckk4ix7r8a5yhb52kxc0f43pai1p0apm")))

