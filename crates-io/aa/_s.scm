(define-module (crates-io aa _s) #:use-module (crates-io))

(define-public crate-aa_similarity-0.1 (crate (name "aa_similarity") (vers "0.1.0") (deps (list (crate-dep (name "aa-name") (req "^0.1.4") (default-features #t) (kind 0)) (crate-dep (name "aa-name") (req "^0.1.4") (default-features #t) (kind 1)) (crate-dep (name "heck") (req "^0.3.1") (default-features #t) (kind 1)) (crate-dep (name "nom") (req "^6.0") (default-features #t) (kind 1)))) (hash "1j8gkdysf2f5g7b37z76lad4xinr7pnbgnbac4hcahlgvd7ip6nw")))

