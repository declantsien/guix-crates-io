(define-module (crates-io aa ny) #:use-module (crates-io))

(define-public crate-aanyx-0.1 (crate (name "aanyx") (vers "0.1.0") (deps (list (crate-dep (name "libloading") (req "^0.8.0") (default-features #t) (kind 0)) (crate-dep (name "rustc_version") (req "^0.4.0") (default-features #t) (kind 1)))) (hash "0jxv9bnfwmf2w632gcc9n7z9gvvrj5a53bg8ma9xdjxgzibay5pj")))

(define-public crate-aanyx-0.2 (crate (name "aanyx") (vers "0.2.0") (deps (list (crate-dep (name "criterion") (req "^0.4.0") (features (quote ("html_reports"))) (default-features #t) (kind 2)) (crate-dep (name "libloading") (req "^0.8.0") (default-features #t) (kind 0)) (crate-dep (name "paste") (req "^1.0.12") (default-features #t) (kind 0)) (crate-dep (name "rustc_version") (req "^0.4.0") (default-features #t) (kind 1)))) (hash "18a5fnazifn54mm4p4732q347r0dn7lziw9nwmqvsjk97vnw387c")))

