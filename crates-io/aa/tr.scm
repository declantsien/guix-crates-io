(define-module (crates-io aa tr) #:use-module (crates-io))

(define-public crate-aatree-0.1 (crate (name "aatree") (vers "0.1.0") (deps (list (crate-dep (name "log") (req "^0.4.8") (optional #t) (default-features #t) (kind 0)))) (hash "06pdkkr11qnfssv9i74rp93fi33j1yrfn54knwrfm682ybnpcp90") (features (quote (("default" "log")))) (rust-version "1.50")))

(define-public crate-aatree-0.1 (crate (name "aatree") (vers "0.1.1") (deps (list (crate-dep (name "log") (req "^0.4.8") (optional #t) (default-features #t) (kind 0)))) (hash "1p3py9ap4dyjhix9cs73d68ny5gnpmbjrx6b23b73248clw4r4zs") (features (quote (("default" "log")))) (rust-version "1.56")))

(define-public crate-aatree-0.1 (crate (name "aatree") (vers "0.1.2") (deps (list (crate-dep (name "log") (req "^0.4.8") (optional #t) (default-features #t) (kind 0)))) (hash "0g2xz71qq1xawwcjk1nk7kfmy0liihxalchkfxk275lgicy9gg2q") (features (quote (("default" "log")))) (rust-version "1.56")))

(define-public crate-aatree-0.2 (crate (name "aatree") (vers "0.2.0") (hash "00jzk03p1ravx4d3zz8vr29y9nv4wbmdqix3wv9gn9fian1arvvd") (rust-version "1.56")))

(define-public crate-aatree-0.2 (crate (name "aatree") (vers "0.2.1") (hash "14p5al5nlivx4vpg377i5acwc3v248vz9568923r1h5h9d0gby3q") (rust-version "1.56")))

(define-public crate-aatree-0.2 (crate (name "aatree") (vers "0.2.2") (deps (list (crate-dep (name "document-features") (req "^0.2") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "openapi_type") (req "^0.4") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1") (optional #t) (kind 0)) (crate-dep (name "serde_json") (req "^1") (default-features #t) (kind 2)))) (hash "0xsk13fb0flph9icg0i2pbdwzpd1d4kfsdhpxj3z6if7262cpsax") (v 2) (features2 (quote (("serde" "dep:serde") ("openapi" "dep:openapi_type")))) (rust-version "1.60")))

