(define-module (crates-io aa n_) #:use-module (crates-io))

(define-public crate-aan_klib-0.1 (crate (name "aan_klib") (vers "0.1.0") (hash "190zv3zhy2jsig1sm7v0mx7w6lwm040fa22wpan9f6xzwkvjai56")))

(define-public crate-aan_klib-0.1 (crate (name "aan_klib") (vers "0.1.1") (hash "1y0mv7d0zawzl7ldqmjwwjxxa8bpw1bxcjhzvs8zzkkf0fa08h20")))

(define-public crate-aan_klib-0.1 (crate (name "aan_klib") (vers "0.1.2") (hash "044822p6bw4slpn272bvq61933fa5dvpgvdja6q4ixin45wr1zzv")))

