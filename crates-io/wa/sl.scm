(define-module (crates-io wa sl) #:use-module (crates-io))

(define-public crate-wasl-0.0.0 (crate (name "wasl") (vers "0.0.0") (deps (list (crate-dep (name "naga") (req "^0.5") (default-features #t) (kind 0)))) (hash "0hli20jhq4w8avgp076isq6plznp9ha4iz5m1y27klsl4ajyqfzp")))

