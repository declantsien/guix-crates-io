(define-module (crates-io wa r-) #:use-module (crates-io))

(define-public crate-war-cli-0.1 (crate (name "war-cli") (vers "0.1.0") (deps (list (crate-dep (name "failure") (req "^0.1.5") (default-features #t) (kind 0)) (crate-dep (name "notify") (req "^4.0.13") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.40") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.3.1") (default-features #t) (kind 0)) (crate-dep (name "war") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "01qxrl9mgisrr7hi3kz85rpp9fgms4n9slgx7jfhgaspw76pfjgb") (features (quote (("strict"))))))

(define-public crate-war-cli-0.1 (crate (name "war-cli") (vers "0.1.1") (deps (list (crate-dep (name "failure") (req "^0.1.5") (default-features #t) (kind 0)) (crate-dep (name "notify") (req "^4.0.13") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.40") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.3.1") (default-features #t) (kind 0)) (crate-dep (name "war") (req "^0.1.1") (default-features #t) (kind 0)))) (hash "1cykf4hfr2v1s7qkhq0n3lwky8mp7mh36xn1kk60x12gxs8bjf67") (features (quote (("strict"))))))

(define-public crate-war-cli-0.2 (crate (name "war-cli") (vers "0.2.0") (deps (list (crate-dep (name "failure") (req "^0.1.5") (default-features #t) (kind 0)) (crate-dep (name "notify") (req "^4.0.13") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.40") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.3.1") (default-features #t) (kind 0)) (crate-dep (name "war") (req "^0.2.0") (default-features #t) (kind 0)))) (hash "05ml40v9mygv7816p402hri5ji0fykrrlnbildsvp4nkidlvlzik") (features (quote (("strict"))))))

(define-public crate-war-rs-0.1 (crate (name "war-rs") (vers "0.1.0") (deps (list (crate-dep (name "ebur128") (req "^0.1.8") (default-features #t) (kind 0)) (crate-dep (name "wav") (req "^1.0.0") (default-features #t) (kind 0)))) (hash "1nh58kkf9zbsdcx3ziq7if2lzj0rchdr6p6m78hxrz1zdx4a4s78")))

(define-public crate-war-web-0.1 (crate (name "war-web") (vers "0.1.0") (hash "157mkak99mm24izvbp5hbb4igzqmnsqc1pw4fgmrl33zvwqnm194")))

(define-public crate-war-web-0.1 (crate (name "war-web") (vers "0.1.1") (hash "1kybclx9sr6a5yy67lww52bg9djchr4hgdagsa139n778zv2i7ay")))

