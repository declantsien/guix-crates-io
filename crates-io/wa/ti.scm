(define-module (crates-io wa ti) #:use-module (crates-io))

(define-public crate-watime-0.1 (crate (name "watime") (vers "0.1.0") (deps (list (crate-dep (name "chrono") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^3.0") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "1x805d5ly3hqqm3ys7mwdbdk9y9x2xzmcc407l38kj4r3dvdywl7")))

(define-public crate-watime-0.2 (crate (name "watime") (vers "0.2.0") (deps (list (crate-dep (name "chrono") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^3.0") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "126iiqpsws8p9gjp8ykcjp314n310g8k2rk4djp80qg60bdsip1q")))

(define-public crate-watime-0.2 (crate (name "watime") (vers "0.2.1") (deps (list (crate-dep (name "chrono") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^3.0") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "1rlyh93r2acpi7z6aqy9vj5miym3rkcga3m43kqrkhag4dwshiv6")))

(define-public crate-watime-0.2 (crate (name "watime") (vers "0.2.2") (deps (list (crate-dep (name "chrono") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^3.0") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "19w6bjbdyppn9vhvl741hsb51bdpdqzxlkb91h29m9q7zy3fcwh5")))

(define-public crate-watime-0.2 (crate (name "watime") (vers "0.2.3") (deps (list (crate-dep (name "chrono") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^3.0") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "10cqxdx1qvmqx0zwarvwr6x9bl4anx6a381x52nhbw9qbh538iql")))

