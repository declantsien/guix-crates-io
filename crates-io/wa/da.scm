(define-module (crates-io wa da) #:use-module (crates-io))

(define-public crate-wadachi-0.1 (crate (name "wadachi") (vers "0.1.0-beta.0") (deps (list (crate-dep (name "async-std") (req "^1.5.0") (features (quote ("attributes"))) (default-features #t) (kind 0)) (crate-dep (name "chrono") (req "^0.4.19") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.4.2") (default-features #t) (kind 0)) (crate-dep (name "scraper") (req "^0.12.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "surf") (req "^2.1.0") (default-features #t) (kind 0)))) (hash "1spkzmk6pxhz0pki0lr3zj7mbdqxrxqp89aymb9shid0f8kp3mkl")))

