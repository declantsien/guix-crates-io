(define-module (crates-io wa co) #:use-module (crates-io))

(define-public crate-wacom-sys-0.1 (crate (name "wacom-sys") (vers "0.1.0") (hash "0wnhwnhpidg38y0j03696kr1jm30vghgjsjckfj43x0d5cqk17hx")))

(define-public crate-wacomo-0.0.0 (crate (name "wacomo") (vers "0.0.0") (hash "0bj641alk6v29pqs4q0v954qsvsz2k31hgvwnkl31qadmkdvrfqx")))

