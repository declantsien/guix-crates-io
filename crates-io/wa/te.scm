(define-module (crates-io wa te) #:use-module (crates-io))

(define-public crate-water-0.0.1 (crate (name "water") (vers "0.0.1") (hash "1zkd7qkrnir921x9wg0qvri852vn1j1gpfa1skmri1i6s4fzhq6d") (yanked #t)))

(define-public crate-water-0.0.2 (crate (name "water") (vers "0.0.2") (hash "1xcpvn2fym5m9b8dvd71yqagiqjhamcxawc60qr734dcp3yzfnai") (yanked #t)))

(define-public crate-water-0.1 (crate (name "water") (vers "0.1.3") (hash "10vrxi47ngagmkd5c5406cgqc89adwmbh8n4bjd0xpkzaw4bsbzh") (yanked #t)))

(define-public crate-water-0.1 (crate (name "water") (vers "0.1.5") (hash "0ifn9imc4m9nqpxyx9dvppr0z87dqjm83f9rdd605gq57l1hwm8y") (yanked #t)))

(define-public crate-water-0.1 (crate (name "water") (vers "0.1.6-alpha") (hash "166hnaypjl0x0gzcpyafx16fcpcmy4frs0c5r1l3fafi0l6z4kzw") (yanked #t)))

(define-public crate-water-0.1 (crate (name "water") (vers "0.1.7-alpha") (hash "0dw3x20kd5xq00awkhsnszydw8n6xg2l3xybp465qrnqs1gyx6ym") (yanked #t)))

(define-public crate-water-0.2 (crate (name "water") (vers "0.2.8-alpha") (hash "00pppdvnh1b3zyffy5h8bxgxicb0c9hcifcz6fnbs401dsbgvsjj") (yanked #t)))

(define-public crate-water-0.3 (crate (name "water") (vers "0.3.9-alpha") (hash "1bagd9i8rh8bhbs4n06cz9kpgl8rs0qaabv5k2s3hax3shyj47dv") (yanked #t)))

(define-public crate-water-0.3 (crate (name "water") (vers "0.3.10-alpha") (hash "0jv0haql3k2648mr8jl17hwks289b0hvanbs9n37iw04gsah39h2") (yanked #t)))

(define-public crate-water-0.3 (crate (name "water") (vers "0.3.11-alpha") (hash "08wia11wc1kpajsn0ab6hb5f3ak24qkf92zk2wk9g3jcfqk8k7hy") (yanked #t)))

(define-public crate-water-0.4 (crate (name "water") (vers "0.4.12-alpha") (hash "1kg68krb9wdw8y1lxpxkhr2dm2q9v7ygv8kpnvr2s7sxhln8kdr8") (yanked #t)))

(define-public crate-water-0.4 (crate (name "water") (vers "0.4.13-alpha") (hash "1lsrcmiwbd2372r32d15a2msxi2hzr4li5fs9qfvvgfhl7ygsfb1") (yanked #t)))

(define-public crate-water-0.6 (crate (name "water") (vers "0.6.18-alpha") (hash "1r8vdd0iikpr224dhx1g3qy5ji1daq6qdr4fhdrcxjw8zznp14hz") (yanked #t)))

(define-public crate-water-0.6 (crate (name "water") (vers "0.6.19-alpha") (hash "1vvmp8i1c5n710v0dabvs8lp5qd7f0jma0dh7lk8qw8hs3yzf3hp") (yanked #t)))

(define-public crate-water-0.6 (crate (name "water") (vers "0.6.20-alpha") (hash "1gi9q49bx5p931qd9p2mxymf33ci1a40q4g9bdf3z467910ydsvd") (yanked #t)))

(define-public crate-water-0.6 (crate (name "water") (vers "0.6.21-alpha") (hash "1synlf2zj7gavpp4g9fl6mbv2zc4ypz2dardb18zm862rpj311y6")))

(define-public crate-water-0.7 (crate (name "water") (vers "0.7.22-alpha") (hash "1gj5jyyjg6p4c1g18gxrizwxdnbfayfqwrs3k1knb3z2zbk87z6j")))

(define-public crate-water-0.8 (crate (name "water") (vers "0.8.23-alpha") (hash "00j6ym99s95lrmv8jxqqz2j1ii0yqy0k01pmp0jsa9m4i2lgkb75")))

(define-public crate-water-0.10 (crate (name "water") (vers "0.10.25-alpha") (hash "0cvx8m859dgd9f51ky52l9zdrjwkc489jd6mr4a7yaqibab07633")))

(define-public crate-water-0.10 (crate (name "water") (vers "0.10.26-alpha") (deps (list (crate-dep (name "time") (req "*") (default-features #t) (kind 0)))) (hash "03b1l0hr5idykpf1jwny2a7cwb4grdbh0fxmhn0217cpb2gpfnmh")))

(define-public crate-water-0.11 (crate (name "water") (vers "0.11.27-alpha") (deps (list (crate-dep (name "time") (req "*") (default-features #t) (kind 0)))) (hash "1zsgd810xil4nqawx69ix3hz1yzixfga3swpimh5dwxald0wrz1r")))

(define-public crate-water-0.12 (crate (name "water") (vers "0.12.30-alpha") (deps (list (crate-dep (name "time") (req "*") (default-features #t) (kind 0)))) (hash "05f6xc8c1dzabrh8f8qvs0wh042bmzq8wrim707q2cknsqa65hgk")))

(define-public crate-water-0.13 (crate (name "water") (vers "0.13.31-alpha") (deps (list (crate-dep (name "time") (req "*") (default-features #t) (kind 0)))) (hash "1gag6im8vj0c81zk9ljmgncw5lszizswq8w6ggwhpmhdy87is37r")))

(define-public crate-water-0.14 (crate (name "water") (vers "0.14.33-alpha") (deps (list (crate-dep (name "time") (req "^0.1.5") (default-features #t) (kind 0)))) (hash "11nzk4m5ylwy8n39yf18f3s737gx6srzfvnfzq66bs34f1ym6z9a")))

(define-public crate-water-0.14 (crate (name "water") (vers "0.14.34-alpha") (deps (list (crate-dep (name "time") (req "^0.1.5") (default-features #t) (kind 0)))) (hash "1p3d611qcdj22bz0s21lqz7swzs5ablrjci8sjbr1qsg271rkdyj")))

(define-public crate-water-0.14 (crate (name "water") (vers "0.14.38-alpha") (deps (list (crate-dep (name "time") (req "*") (default-features #t) (kind 0)))) (hash "0w93hfb04sjfgjg78smlydldrb57m4glysvl7yazyqnw664n95x9")))

(define-public crate-water-0.14 (crate (name "water") (vers "0.14.39-alpha") (deps (list (crate-dep (name "time") (req "*") (default-features #t) (kind 0)))) (hash "14ppbwj35q8l33wgs6chv7kwxd71w6ixda0jlk3iv2h6s9c56ini")))

(define-public crate-water-0.14 (crate (name "water") (vers "0.14.41-alpha") (deps (list (crate-dep (name "time") (req "*") (default-features #t) (kind 0)))) (hash "1y0d3xxniw37bzh0j9l57sp2z8nwy0hzaaz55nkkrilq6v3n2gfk")))

(define-public crate-water-0.15 (crate (name "water") (vers "0.15.42-alpha") (deps (list (crate-dep (name "time") (req "*") (default-features #t) (kind 0)))) (hash "1szdpx13wmvwsgdb6wsqngjxxb99d6zsraixhsg3yv3234rfvrcr")))

(define-public crate-water-0.16 (crate (name "water") (vers "0.16.43-alpha") (deps (list (crate-dep (name "time") (req "*") (default-features #t) (kind 0)))) (hash "0sddlhkd9xrls8rhkci6a8hyqwxzv7zxgmr1y7nmqn61vlsn7cqb")))

(define-public crate-water-0.16 (crate (name "water") (vers "0.16.44-alpha") (deps (list (crate-dep (name "time") (req "*") (default-features #t) (kind 0)))) (hash "1swzpjq78ki0k4jzzp0gfvd3n0vzhp984g8j3mcn2qinh0r83vl4")))

(define-public crate-water-0.16 (crate (name "water") (vers "0.16.45-alpha") (deps (list (crate-dep (name "time") (req "*") (default-features #t) (kind 0)))) (hash "05g74s063npdpylr1g8y2qlwlbm437wbbrl6vviq9xzjr1zhz0aq")))

(define-public crate-water-simulation-0.1 (crate (name "water-simulation") (vers "0.1.1") (deps (list (crate-dep (name "clap") (req "^4.3.5") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "env_logger") (req "^0.10.0") (default-features #t) (kind 0)) (crate-dep (name "image") (req "^0.24.6") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.19") (default-features #t) (kind 0)))) (hash "06d0byw55rwwvh14fwv94m2wz1k8x7i1sfpgdjin31f66gyarcc0")))

(define-public crate-water-simulation-0.1 (crate (name "water-simulation") (vers "0.1.2") (deps (list (crate-dep (name "clap") (req "^4.3.5") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "env_logger") (req "^0.10.0") (default-features #t) (kind 0)) (crate-dep (name "image") (req "^0.24.6") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.19") (default-features #t) (kind 0)))) (hash "0cdssnlp1nc01b27bjr2azarrcz6wc515h67h5ifr4ylc3vvap62")))

(define-public crate-water_mon_app-1 (crate (name "water_mon_app") (vers "1.0.0") (deps (list (crate-dep (name "chrono") (req "^0.4.19") (default-features #t) (kind 0)) (crate-dep (name "local_ipaddress") (req "^0.1.3") (default-features #t) (kind 0)) (crate-dep (name "rocket") (req "^0.4.10") (default-features #t) (kind 0)) (crate-dep (name "rocket_contrib") (req "^0.4.10") (features (quote ("serve"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.118") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.60") (default-features #t) (kind 0)) (crate-dep (name "serialport") (req "^3.3.0") (default-features #t) (kind 0)))) (hash "1wh4paibzzlbi7yy431inp7zqzvvvhk9qagcy8in70yi03yanczv")))

(define-public crate-water_mon_app-1 (crate (name "water_mon_app") (vers "1.0.1") (deps (list (crate-dep (name "chrono") (req "^0.4.19") (default-features #t) (kind 0)) (crate-dep (name "local_ipaddress") (req "^0.1.3") (default-features #t) (kind 0)) (crate-dep (name "rocket") (req "^0.4.10") (default-features #t) (kind 0)) (crate-dep (name "rocket_contrib") (req "^0.4.10") (features (quote ("serve"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.118") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.60") (default-features #t) (kind 0)) (crate-dep (name "serialport") (req "^3.3.0") (default-features #t) (kind 0)))) (hash "1mqk8nv588rrnylfxq1bfykm57jbn769hnpzf3gw0bx3z8vn1d14")))

(define-public crate-waterbear-0.12 (crate (name "waterbear") (vers "0.12.0") (deps (list (crate-dep (name "atty") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^2.32") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "termcolor") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "unicode-segmentation") (req "^1.2.0") (default-features #t) (kind 0)) (crate-dep (name "unicode_categories") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "uuid") (req "^0.7") (features (quote ("v4"))) (default-features #t) (kind 0)) (crate-dep (name "waterbear-instruction-derive") (req "^0.1") (default-features #t) (kind 0)))) (hash "1ra8ba5gijv1rw7hj2a9fc647384712cx3ygs22hdh2p17qf0vf7")))

(define-public crate-waterbear-instruction-derive-0.1 (crate (name "waterbear-instruction-derive") (vers "0.1.0") (deps (list (crate-dep (name "lazy_static") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^0.11") (default-features #t) (kind 0)))) (hash "00kkpz5gm43i8kgk15g61ysxry8mqsappy61w3wrd39m3ss2knf9")))

(define-public crate-watercolor-0.1 (crate (name "watercolor") (vers "0.1.0") (hash "189bzmh8x2sx7k94w94bj1jqyjdfs1kjqccywxh5wc3wm7cpn3vx")))

(define-public crate-watercolor-0.2 (crate (name "watercolor") (vers "0.2.0") (hash "01p9hiam2j21s8x125fqnxfpyrpxbkh9ys9mv5aa3a0x01r4sp4m")))

(define-public crate-watercraft-0.1 (crate (name "watercraft") (vers "0.1.0") (deps (list (crate-dep (name "anyhow") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "byte-unit") (req "^4.0") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^4.0.29") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "dirs") (req "^4.0.0") (default-features #t) (kind 0)) (crate-dep (name "once_cell") (req "^1.16") (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.11") (features (quote ("json"))) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.23") (features (quote ("macros" "rt-multi-thread"))) (default-features #t) (kind 0)) (crate-dep (name "toml") (req "^0.5") (default-features #t) (kind 0)) (crate-dep (name "zip") (req "^0.6") (default-features #t) (kind 0)))) (hash "1y37pldg3nhsbk1qlzb8blqdj3hikwdn8f8cn6rz0vv9nyrilxdf") (features (quote (("rustls-tls" "reqwest/rustls-tls") ("native-tls" "reqwest/native-tls") ("default" "rustls-tls"))))))

(define-public crate-waterfall-0.1 (crate (name "waterfall") (vers "0.1.0") (hash "1ra9k1g3g80z8ny6nm8hisra9aj7irk9y7d9lg4iyvfyx9ld67ci")))

(define-public crate-waterfall-0.1 (crate (name "waterfall") (vers "0.1.1") (deps (list (crate-dep (name "heatmap") (req "^0.1.4") (default-features #t) (kind 0)) (crate-dep (name "hsl") (req "^0.1.1") (default-features #t) (kind 0)) (crate-dep (name "lodepng") (req "^0.5.1") (default-features #t) (kind 0)) (crate-dep (name "rusttype") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "0m52wvcm9wrw2ja693890bkfw25vjr0qdcsjzjh5wqp5h38ndk0c")))

(define-public crate-waterfall-0.1 (crate (name "waterfall") (vers "0.1.2") (deps (list (crate-dep (name "heatmap") (req "^0.1.5") (default-features #t) (kind 0)) (crate-dep (name "hsl") (req "^0.1.1") (default-features #t) (kind 0)) (crate-dep (name "lodepng") (req "^0.5.1") (default-features #t) (kind 0)) (crate-dep (name "rusttype") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "1kh236ywba64byg19wdxia3n1dcgbqqan7941j2hv902ybq2sg7c")))

(define-public crate-waterfall-0.1 (crate (name "waterfall") (vers "0.1.3") (deps (list (crate-dep (name "heatmap") (req "^0.1.6") (default-features #t) (kind 0)) (crate-dep (name "hsl") (req "^0.1.1") (default-features #t) (kind 0)) (crate-dep (name "lodepng") (req "^0.5.1") (default-features #t) (kind 0)) (crate-dep (name "rusttype") (req "^0.1.2") (default-features #t) (kind 0)))) (hash "0ndd2vkjg78hbdiyi1kx0vzd8p3ndyh93yihpz1yyp647inds8ak") (yanked #t)))

(define-public crate-waterfall-0.1 (crate (name "waterfall") (vers "0.1.4") (deps (list (crate-dep (name "heatmap") (req "^0.1.7") (default-features #t) (kind 0)) (crate-dep (name "hsl") (req "^0.1.1") (default-features #t) (kind 0)) (crate-dep (name "lodepng") (req "^0.5.1") (default-features #t) (kind 0)) (crate-dep (name "rusttype") (req "^0.1.2") (default-features #t) (kind 0)))) (hash "1d93w8mxdz8rcgwsqj9zsaf67710hhps5agn27xa1vgg706j2r83")))

(define-public crate-waterfall-0.2 (crate (name "waterfall") (vers "0.2.0") (deps (list (crate-dep (name "heatmap") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "hsl") (req "^0.1.1") (default-features #t) (kind 0)) (crate-dep (name "lodepng") (req "^0.5.3") (default-features #t) (kind 0)) (crate-dep (name "rusttype") (req "^0.1.2") (default-features #t) (kind 0)))) (hash "0ki3x2hndflmy7zxbsk0z74b7hpp5d9x6kijzx9vikdvcfw6z5cx")))

(define-public crate-waterfall-0.3 (crate (name "waterfall") (vers "0.3.0") (deps (list (crate-dep (name "heatmap") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "hsl") (req "^0.1.1") (default-features #t) (kind 0)) (crate-dep (name "lodepng") (req "^0.5.3") (default-features #t) (kind 0)) (crate-dep (name "rusttype") (req "^0.1.2") (default-features #t) (kind 0)))) (hash "1m5wcrwqqgmjx89g20nfzigrz8k48p58y6i6ikkjshbizp4ywkpf")))

(define-public crate-waterfall-0.4 (crate (name "waterfall") (vers "0.4.0") (deps (list (crate-dep (name "heatmap") (req "^0.4.0") (default-features #t) (kind 0)) (crate-dep (name "hsl") (req "^0.1.1") (default-features #t) (kind 0)) (crate-dep (name "lodepng") (req "^0.5.3") (default-features #t) (kind 0)) (crate-dep (name "rusttype") (req "^0.1.2") (default-features #t) (kind 0)))) (hash "0jjad2hw2pbbn6w5d7ahwxhpgljnmycqrkw5ncp84y3jpinkz15l")))

(define-public crate-waterfall-0.5 (crate (name "waterfall") (vers "0.5.0") (deps (list (crate-dep (name "heatmap") (req "^0.5.0") (default-features #t) (kind 0)) (crate-dep (name "hsl") (req "^0.1.1") (default-features #t) (kind 0)) (crate-dep (name "lodepng") (req "^0.5.3") (default-features #t) (kind 0)) (crate-dep (name "rusttype") (req "^0.1.2") (default-features #t) (kind 0)))) (hash "0p4hprx9s93z87i0ylm4xcxnaflj0bd6ic1dixlfca9cp7dlb2qq")))

(define-public crate-waterfall-0.6 (crate (name "waterfall") (vers "0.6.0") (deps (list (crate-dep (name "heatmap") (req "^0.6.0") (default-features #t) (kind 0)) (crate-dep (name "hsl") (req "^0.1.1") (default-features #t) (kind 0)) (crate-dep (name "lodepng") (req "^0.5.3") (default-features #t) (kind 0)) (crate-dep (name "rusttype") (req "^0.1.2") (default-features #t) (kind 0)))) (hash "1wk6sa2a417kggw5afjm2r85g4lhjd5nq0xf4ql38qh24lcfa9za")))

(define-public crate-waterfall-0.6 (crate (name "waterfall") (vers "0.6.1") (deps (list (crate-dep (name "heatmap") (req "^0.6.1") (default-features #t) (kind 0)) (crate-dep (name "hsl") (req "^0.1.1") (default-features #t) (kind 0)) (crate-dep (name "lodepng") (req "^0.5.3") (default-features #t) (kind 0)) (crate-dep (name "rusttype") (req "^0.1.2") (default-features #t) (kind 0)))) (hash "178nbglfb9ky15ms61hv2psiy36926a61ypkdsv8kcy8ix0sw6lp")))

(define-public crate-waterfall-0.6 (crate (name "waterfall") (vers "0.6.2") (deps (list (crate-dep (name "heatmap") (req "^0.6.2") (default-features #t) (kind 0)) (crate-dep (name "hsl") (req "^0.1.1") (default-features #t) (kind 0)) (crate-dep (name "lodepng") (req "^0.5.3") (default-features #t) (kind 0)) (crate-dep (name "rusttype") (req "^0.1.2") (default-features #t) (kind 0)))) (hash "179p95c5w0mkyhhrh4wi9243wzkgd70siqgqdlvpgl05z13rgsvq")))

(define-public crate-waterfall-0.6 (crate (name "waterfall") (vers "0.6.3") (deps (list (crate-dep (name "heatmap") (req "^0.6.2") (default-features #t) (kind 0)) (crate-dep (name "hsl") (req "^0.1.1") (default-features #t) (kind 0)) (crate-dep (name "lodepng") (req "^0.5.3") (default-features #t) (kind 0)) (crate-dep (name "rusttype") (req "^0.1.2") (default-features #t) (kind 0)))) (hash "17jmiqddf471n1n2v4hyzcaf4r4f7mfqzmnkydb3xqc0cdydzkc8")))

(define-public crate-waterfall-0.6 (crate (name "waterfall") (vers "0.6.4") (deps (list (crate-dep (name "heatmap") (req "^0.6.2") (default-features #t) (kind 0)) (crate-dep (name "hsl") (req "^0.1.1") (default-features #t) (kind 0)) (crate-dep (name "lodepng") (req "^0.5.3") (default-features #t) (kind 0)) (crate-dep (name "rusttype") (req "^0.1.2") (default-features #t) (kind 0)))) (hash "04xc8b5vhclnk202r4z2hrdiil125mw9y6rsf95xvn1yj94b2x96")))

(define-public crate-waterfall-0.6 (crate (name "waterfall") (vers "0.6.5") (deps (list (crate-dep (name "heatmap") (req "^0.6.2") (default-features #t) (kind 0)) (crate-dep (name "hsl") (req "^0.1.1") (default-features #t) (kind 0)) (crate-dep (name "lodepng") (req "^0.5.3") (default-features #t) (kind 0)) (crate-dep (name "rusttype") (req "^0.1.2") (default-features #t) (kind 0)))) (hash "0c7mjq8pja78f5fnr4c0wpv79kg760ayilnib0hbz9zzdbdcz1wd")))

(define-public crate-waterfall-0.6 (crate (name "waterfall") (vers "0.6.6") (deps (list (crate-dep (name "heatmap") (req "^0.6.2") (default-features #t) (kind 0)) (crate-dep (name "hsl") (req "^0.1.1") (default-features #t) (kind 0)) (crate-dep (name "lodepng") (req "^0.5.3") (default-features #t) (kind 0)) (crate-dep (name "rusttype") (req "^0.1.2") (default-features #t) (kind 0)))) (hash "095faqxcd68bl6m9i1nyz7gqav68cbi7x82mcszcakffvg7hmf5z")))

(define-public crate-waterfall-0.7 (crate (name "waterfall") (vers "0.7.0") (deps (list (crate-dep (name "heatmap") (req "^0.6.2") (default-features #t) (kind 0)) (crate-dep (name "hsl") (req "^0.1.1") (default-features #t) (kind 0)) (crate-dep (name "png") (req "^0.7.0") (default-features #t) (kind 0)) (crate-dep (name "rusttype") (req "^0.1.2") (default-features #t) (kind 0)))) (hash "0v82f9x02hdw0hy7iwgpabvy09c06cc7sdi8l6fr8rw1imgd6r6h")))

(define-public crate-waterfall-0.7 (crate (name "waterfall") (vers "0.7.1") (deps (list (crate-dep (name "heatmap") (req "^0.6.2") (default-features #t) (kind 0)) (crate-dep (name "hsl") (req "^0.1.1") (default-features #t) (kind 0)) (crate-dep (name "png") (req "^0.7.0") (default-features #t) (kind 0)) (crate-dep (name "rusttype") (req "^0.1.2") (default-features #t) (kind 0)))) (hash "0a870kbg894x6n2zcrzalxgk0v3jvb7iml6640l1a3djzqcjmzfx")))

(define-public crate-waterfall-0.8 (crate (name "waterfall") (vers "0.8.0") (deps (list (crate-dep (name "clocksource") (req "^0.6.0") (default-features #t) (kind 0)) (crate-dep (name "dejavu") (req "^2.37.0") (default-features #t) (kind 0)) (crate-dep (name "heatmap") (req "^0.7.0") (default-features #t) (kind 0)) (crate-dep (name "histogram") (req "^0.7.0") (default-features #t) (kind 0)) (crate-dep (name "image") (req "^0.24.3") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.17") (default-features #t) (kind 0)) (crate-dep (name "rusttype") (req "^0.9.2") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 2)) (crate-dep (name "rand_distr") (req "^0.4.3") (default-features #t) (kind 2)))) (hash "1zpl5r6mllnp5q6lrxflszm43s5hbfbw5d0h7gc12lm9zghzq0n5")))

(define-public crate-waterfall-0.8 (crate (name "waterfall") (vers "0.8.1") (deps (list (crate-dep (name "clocksource") (req "^0.6.0") (default-features #t) (kind 0)) (crate-dep (name "dejavu") (req "^2.37.0") (default-features #t) (kind 0)) (crate-dep (name "heatmap") (req "^0.7.1") (default-features #t) (kind 0)) (crate-dep (name "histogram") (req "^0.7.1") (default-features #t) (kind 0)) (crate-dep (name "image") (req "^0.24.3") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.17") (default-features #t) (kind 0)) (crate-dep (name "rusttype") (req "^0.9.2") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 2)) (crate-dep (name "rand_distr") (req "^0.4.3") (default-features #t) (kind 2)))) (hash "14j2yng6phdnh4gwfp8gkq45j3vwwhavi29ddbjp3wdkm24r5yjj")))

(define-public crate-wateringcan-0.1 (crate (name "wateringcan") (vers "0.1.0") (hash "0m91w205xa6f6dx8sksm28hhwxf9dmm0gc402s16s3jhfxkgyyfp")))

(define-public crate-wateringcan-0.1 (crate (name "wateringcan") (vers "0.1.1") (deps (list (crate-dep (name "seed") (req "^0.8") (default-features #t) (kind 0)))) (hash "19cgdw2dbfjx21gbhh0z7ppbcyz79n4xb7v7q6i30ypjj9nbwzg3")))

(define-public crate-waterkit-0.0.0 (crate (name "waterkit") (vers "0.0.0") (hash "0iayz1v9yyf6zb5ncc3j0kr5qcdffkklnq644s1k1yajlvb26b8j")))

(define-public crate-waterlogged-0.1 (crate (name "waterlogged") (vers "0.1.0") (deps (list (crate-dep (name "async-trait") (req "^0.1.41") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.11") (default-features #t) (kind 0)) (crate-dep (name "tide") (req "^0.15.0") (default-features #t) (kind 0)))) (hash "08yw3sz0sjln6f84qkm6cklh16pzcvhcbgjn2746pl5sz2d07xhz")))

(define-public crate-watermark-1 (crate (name "watermark") (vers "1.0.0") (deps (list (crate-dep (name "criterion") (req "^0.2") (default-features #t) (kind 2)) (crate-dep (name "num") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.7.0") (default-features #t) (kind 2)))) (hash "0y1g6sfvpc1jqi3zd3yw9fzylam4c7sj348chci63v9yq9ss984q")))

(define-public crate-watermark-1 (crate (name "watermark") (vers "1.0.1") (deps (list (crate-dep (name "criterion") (req "^0.2") (default-features #t) (kind 2)) (crate-dep (name "num") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.7.0") (default-features #t) (kind 2)))) (hash "020ya4157f03nhlsjywxi9dhg9gsaip22rdd8lxlrl2qvxvkxmkk")))

(define-public crate-watermark-1 (crate (name "watermark") (vers "1.0.2") (deps (list (crate-dep (name "criterion") (req "^0.2") (default-features #t) (kind 2)) (crate-dep (name "num") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.7.0") (default-features #t) (kind 2)))) (hash "1hbwpdqvlp9q3jmmrpgp3z5icfwwcs4xaan2033n1w0z7wmrnmip")))

(define-public crate-watermark-1 (crate (name "watermark") (vers "1.1.0") (deps (list (crate-dep (name "criterion") (req "^0.2") (default-features #t) (kind 2)) (crate-dep (name "num") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.7.0") (default-features #t) (kind 2)))) (hash "14cpxvvjhh2w1q7f1jj24v2829ijb99qamndgqxx08agjal8l9bd")))

(define-public crate-watermarker-0.3 (crate (name "watermarker") (vers "0.3.0") (deps (list (crate-dep (name "colored") (req "^2.0.0") (default-features #t) (kind 0)) (crate-dep (name "image") (req "^0.24.6") (default-features #t) (kind 0)) (crate-dep (name "indicatif") (req "^0.17.5") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.29.0") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "1sarr5nds15b512g45qpv2q40dk3wyv12fln4sqp1j5lx751yxnw")))

(define-public crate-watermill-0.1 (crate (name "watermill") (vers "0.1.0") (deps (list (crate-dep (name "num") (req "^0.4.0") (default-features #t) (kind 0)) (crate-dep (name "ordered-float") (req "^3.0") (features (quote ("serde"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)))) (hash "1prq0cijv1kywdngcsx1sjqa4kljvm3fddmqz763ps7rwafbk5rl")))

(define-public crate-watermill-0.1 (crate (name "watermill") (vers "0.1.1") (deps (list (crate-dep (name "num") (req "^0.4.0") (default-features #t) (kind 0)) (crate-dep (name "ordered-float") (req "^3.0") (features (quote ("serde"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)))) (hash "00n5rfni34jknp4p0vc5ycdb2497ycml2iliyczi0q4i57l98mdq")))

(define-public crate-waterrower-0.1 (crate (name "waterrower") (vers "0.1.0") (deps (list (crate-dep (name "bufstream") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "nix") (req "^0.7") (default-features #t) (kind 0)) (crate-dep (name "serial") (req "^0.3") (default-features #t) (kind 0)))) (hash "09dsqakzfzij3b99lja4r7mlf1lypgpjrdd2z6va79vk5r7rh3dg")))

(define-public crate-waterui-0.0.0 (crate (name "waterui") (vers "0.0.0") (hash "18pppf38lariydhjhwh4ldp7zjmsmsm5jksbv8prn2dyzfq851rk")))

(define-public crate-waterworks-1 (crate (name "waterworks") (vers "1.0.0") (hash "045gmj4p2by83bjy4a3kvxginvs4hxpb1xf132jzvrhbc9i5pmnn") (rust-version "1.75")))

