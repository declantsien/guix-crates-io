(define-module (crates-io wa ts) #:use-module (crates-io))

(define-public crate-watson-0.0.0 (crate (name "watson") (vers "0.0.0") (deps (list (crate-dep (name "executor") (req "^0") (default-features #t) (kind 0)) (crate-dep (name "malloc") (req "^0.0.1") (default-features #t) (kind 0)) (crate-dep (name "nom") (req "^5") (features (quote ("lexical"))) (kind 0)) (crate-dep (name "webassembly") (req "^0") (default-features #t) (kind 0)))) (hash "1kww8h51nnbzvivs4al1rpv3k87s3s9xik2da0wimy4by9nxsa9r")))

(define-public crate-watson-0.0.2 (crate (name "watson") (vers "0.0.2") (deps (list (crate-dep (name "nom") (req "^5.1.1") (features (quote ("alloc" "lexical"))) (kind 0)) (crate-dep (name "webassembly") (req "^0.3.1") (default-features #t) (kind 0)))) (hash "1fifgvrb06zx51q88vb1czc7aaw29jnr5q3lgw19vbhvvkxpjgf1")))

(define-public crate-watson-0.0.3 (crate (name "watson") (vers "0.0.3") (deps (list (crate-dep (name "webassembly") (req "^0.3.1") (default-features #t) (kind 0)))) (hash "1z4gd9va8pmn7whxmv1wvfsgm3xq8cqavb51yfgcb0a0h6f1rxlx")))

(define-public crate-watson-0.0.4 (crate (name "watson") (vers "0.0.4") (deps (list (crate-dep (name "webassembly") (req "^0.3.1") (default-features #t) (kind 0)))) (hash "0f2v2y1amazfgps4dxm03x7w3ggpwxjv96hwypd01vf1gsvqvzm4")))

(define-public crate-watson-0.0.5 (crate (name "watson") (vers "0.0.5") (deps (list (crate-dep (name "webassembly") (req "^0.3.1") (default-features #t) (kind 0)))) (hash "0668sfwn7csvpcrfb269yskx18walras3961r780r6d6d2qv538m")))

(define-public crate-watson-0.0.6 (crate (name "watson") (vers "0.0.6") (deps (list (crate-dep (name "webassembly") (req "^0.3.1") (default-features #t) (kind 0)))) (hash "0pphwf76iy37l19qzxnldjb5x26y3fdbfd5l62v0ji9dnlw2jv2q")))

(define-public crate-watson-0.0.7 (crate (name "watson") (vers "0.0.7") (deps (list (crate-dep (name "webassembly") (req "^0.3.1") (default-features #t) (kind 0)))) (hash "03qgfdcwmsc54wh2rrjxilm0fib3lwsf1gwwvd8yawy95jn4zq48")))

(define-public crate-watson-0.0.8 (crate (name "watson") (vers "0.0.8") (deps (list (crate-dep (name "webassembly") (req "^0.3.1") (default-features #t) (kind 0)))) (hash "09cygbkzg8d042aivd725pd54qmmh34bmqmwkc007f7hg2allpcb")))

(define-public crate-watson-0.0.9 (crate (name "watson") (vers "0.0.9") (deps (list (crate-dep (name "webassembly") (req "^0.3.1") (default-features #t) (kind 0)))) (hash "1whvd29pn5z4km3pw1a920mvnnl4n9ndjy9rw28m0g3gdjrcn61m")))

(define-public crate-watson-0.0.10 (crate (name "watson") (vers "0.0.10") (deps (list (crate-dep (name "webassembly") (req "^0.3.1") (default-features #t) (kind 0)))) (hash "12dkzkhx7vhgv1vxp4vq28nzm1mlbw83xacm1bw8zsv6bl7v568v")))

(define-public crate-watson-0.1 (crate (name "watson") (vers "0.1.0") (deps (list (crate-dep (name "webassembly") (req "^0.3.1") (default-features #t) (kind 0)))) (hash "0gxdzbi6wb38chbx22l0hhqipb0k4yvqcz85vfn87gjv5wf5msb0")))

(define-public crate-watson-0.2 (crate (name "watson") (vers "0.2.0") (deps (list (crate-dep (name "webassembly") (req "^0.5.0") (default-features #t) (kind 0)))) (hash "1qhffbw3mmvni2s9gsl5i5xparyw1qs9q61bjsysys99nfghmy55")))

(define-public crate-watson-0.3 (crate (name "watson") (vers "0.3.0") (deps (list (crate-dep (name "webassembly") (req "^0.5.0") (default-features #t) (kind 0)))) (hash "1affpk8ai94aswklb6gxx9abmsnz2zir9v8syh8awdlacs92dh0g")))

(define-public crate-watson-0.3 (crate (name "watson") (vers "0.3.1") (deps (list (crate-dep (name "serde") (req "^1.0") (features (quote ("alloc" "derive"))) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (features (quote ("alloc"))) (kind 0)) (crate-dep (name "webassembly") (req "^0.5.0") (default-features #t) (kind 0)))) (hash "0bdnxgl7833wqkmsx2biyn80ycp26lz3n384bx46vbg01yjzyh0z")))

(define-public crate-watson-0.3 (crate (name "watson") (vers "0.3.2") (deps (list (crate-dep (name "serde") (req "^1.0") (features (quote ("alloc" "derive"))) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (features (quote ("alloc"))) (kind 0)) (crate-dep (name "webassembly") (req "^0.5.0") (default-features #t) (kind 0)))) (hash "12xfba5s1w5vgv8ipxv123rj0x1j14v4zj4b58sbmhb32ml5xppv")))

(define-public crate-watson-0.3 (crate (name "watson") (vers "0.3.3") (deps (list (crate-dep (name "serde") (req "^1.0") (features (quote ("alloc" "derive"))) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (features (quote ("alloc"))) (kind 0)) (crate-dep (name "webassembly") (req "^0.5.0") (default-features #t) (kind 0)))) (hash "03i6zrmslw8dlz5027cfkhnfa49kkjgyxjl9shq1jfwgl8asf9sb")))

(define-public crate-watson-0.4 (crate (name "watson") (vers "0.4.0") (deps (list (crate-dep (name "serde") (req "^1.0") (features (quote ("alloc" "derive"))) (kind 0)) (crate-dep (name "webassembly") (req "^0.5.0") (default-features #t) (kind 0)))) (hash "0x7p7pzldvifkqlp0f9216rbg4lqiwylpllw556nrk3fkbkpccl0")))

(define-public crate-watson-0.4 (crate (name "watson") (vers "0.4.1") (deps (list (crate-dep (name "serde") (req "^1.0") (features (quote ("alloc" "derive"))) (kind 0)) (crate-dep (name "webassembly") (req "^0.5.0") (default-features #t) (kind 0)))) (hash "1vylclyf4xc7v7i15q267nhmx5a81cdr24bz9vqxn0n4g2npxshw")))

(define-public crate-watson-0.5 (crate (name "watson") (vers "0.5.0") (deps (list (crate-dep (name "serde") (req "^1.0") (features (quote ("alloc" "derive"))) (kind 0)) (crate-dep (name "webassembly") (req "^0.5.0") (default-features #t) (kind 0)))) (hash "06x8ni4hhqsrpw5x3j1yykivya13dffxj3lvnf3mr3a2047grnbg")))

(define-public crate-watson-0.6 (crate (name "watson") (vers "0.6.0") (deps (list (crate-dep (name "serde") (req "^1.0") (features (quote ("alloc" "derive"))) (kind 0)) (crate-dep (name "webassembly") (req "^0.5.0") (default-features #t) (kind 0)))) (hash "1pqvz0ida576plhx1spg5xjfk2i19npmcx3r30nhilrl7x5waw4s")))

(define-public crate-watson-0.7 (crate (name "watson") (vers "0.7.0") (deps (list (crate-dep (name "serde") (req "^1.0") (features (quote ("alloc" "derive"))) (kind 0)) (crate-dep (name "webassembly") (req "^0.5.0") (default-features #t) (kind 0)))) (hash "0q9gzf0mcpak2z7rnqrbdgn1zbj65y7a1n6i8sxbp0vb9x59g3wf")))

(define-public crate-watson-0.8 (crate (name "watson") (vers "0.8.0") (deps (list (crate-dep (name "serde") (req "^1.0") (features (quote ("alloc" "derive"))) (kind 0)) (crate-dep (name "webassembly") (req "^0.5.0") (default-features #t) (kind 0)))) (hash "0cdfxyn3a6ppnza9k57mk54lb52x3w9gq4kr8fl2slaph5ik2f6i")))

(define-public crate-watson-0.9 (crate (name "watson") (vers "0.9.0") (deps (list (crate-dep (name "serde") (req "^1.0.106") (features (quote ("alloc" "derive"))) (kind 0)) (crate-dep (name "webassembly") (req "^0.8.2") (default-features #t) (kind 0)))) (hash "1lzc9ssgxkifrxhh7kzjxf2a820p88rn252z8mpladnapw36w0rm")))

(define-public crate-watson-0.9 (crate (name "watson") (vers "0.9.1") (deps (list (crate-dep (name "serde") (req "^1.0.106") (features (quote ("alloc" "derive"))) (kind 0)) (crate-dep (name "webassembly") (req "^0.8.2") (default-features #t) (kind 0)))) (hash "02fv6zfj9yiwbh75nxjs9k4n6z533rkha147bl5lazjc2yb2a6jd")))

(define-public crate-watson-0.9 (crate (name "watson") (vers "0.9.2") (deps (list (crate-dep (name "serde") (req "^1.0.116") (features (quote ("alloc" "derive"))) (kind 0)) (crate-dep (name "spin") (req "^0.5.2") (default-features #t) (kind 0)) (crate-dep (name "webassembly") (req "^0.8.2") (default-features #t) (kind 0)))) (hash "1nvgrjcfd2rd7c4x2sy9l4zg6f6hg0v34c9rlfpp4k0s507ahqpg")))

(define-public crate-watson_rs-0.1 (crate (name "watson_rs") (vers "0.1.0") (deps (list (crate-dep (name "tempfile") (req "^3") (default-features #t) (kind 2)))) (hash "05ckq9igh9wal8vybydnk2aacjxzx88y5yw0qdbqb3wm68p2q5y1")))

