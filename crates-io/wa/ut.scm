(define-module (crates-io wa ut) #:use-module (crates-io))

(define-public crate-wautomata-0.1 (crate (name "wautomata") (vers "0.1.0") (deps (list (crate-dep (name "test_tools") (req "~0.1") (default-features #t) (kind 2)))) (hash "1zh9q9jfx6nkvf6v0fjscgqdb2981yr0cip69jzsw07bpyvkyjyb")))

(define-public crate-wautomata-0.1 (crate (name "wautomata") (vers "0.1.1") (deps (list (crate-dep (name "test_tools") (req "~0.1") (default-features #t) (kind 2)) (crate-dep (name "wtools") (req "~0.2") (default-features #t) (kind 0)))) (hash "1m9p8ca0ajk7ahqlinln64zgdi6a1mk3fncx2glfljy8r6k8k185")))

(define-public crate-wautomata-0.1 (crate (name "wautomata") (vers "0.1.2") (deps (list (crate-dep (name "automata_tools") (req "~0.1") (features (quote ("full"))) (default-features #t) (kind 0)) (crate-dep (name "test_tools") (req "~0.1") (default-features #t) (kind 2)) (crate-dep (name "wtools") (req "~0.2") (features (quote ("full"))) (default-features #t) (kind 2)))) (hash "120n71gqq994zpkq699381967axc5bwbdlcc91nw1rabx7143g7d") (features (quote (("use_std") ("use_alloc") ("full" "use_std") ("default" "use_std"))))))

