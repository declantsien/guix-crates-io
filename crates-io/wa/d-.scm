(define-module (crates-io wa d-) #:use-module (crates-io))

(define-public crate-wad-rs-0.1 (crate (name "wad-rs") (vers "0.1.0") (deps (list (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "rayon") (req "^1.3") (optional #t) (default-features #t) (kind 0)))) (hash "1qz263my24wmpi46zwddd00g3gbrxap2kj35j76jhzfkmv7ayk27") (features (quote (("parallel" "rayon")))) (yanked #t)))

(define-public crate-wad-rs-0.2 (crate (name "wad-rs") (vers "0.2.0") (deps (list (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "rayon") (req "^1.3") (optional #t) (default-features #t) (kind 0)))) (hash "12rhj6h05nvd49hls4d5rylxhnw6f045hl215n1ypndk1bkmxqzw") (features (quote (("parallel" "rayon")))) (yanked #t)))

