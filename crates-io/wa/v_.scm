(define-module (crates-io wa v_) #:use-module (crates-io))

(define-public crate-wav_concat-1 (crate (name "wav_concat") (vers "1.0.0") (hash "1l0mcqcij6s4ls5g5iya3i6knijyrl0iqwlpsxan97blsmhy92y8")))

(define-public crate-wav_concat-1 (crate (name "wav_concat") (vers "1.1.0") (hash "0rzh7z42yywcy1k38rhj7srws5i4k9pg5sd82aqjrmnf3gkld78k")))

(define-public crate-wav_inspect-0.1 (crate (name "wav_inspect") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^4.5.2") (features (quote ("derive" "cargo"))) (default-features #t) (kind 0)) (crate-dep (name "colored") (req "^2.1.0") (default-features #t) (kind 0)) (crate-dep (name "wavers") (req "^1.3.1") (features (quote ("colored"))) (default-features #t) (kind 0)))) (hash "1m2ks73jzpylr592zgk57wi0snpicbn73z01h725hh4bfvsp25hd")))

(define-public crate-wav_io-0.1 (crate (name "wav_io") (vers "0.1.0") (hash "14mkrbflhwxm9v41y3x6wnkx518d4v2v9c2w8fgirmf15y4f1yig")))

(define-public crate-wav_io-0.1 (crate (name "wav_io") (vers "0.1.1") (hash "1jfgz1mkpy99g6p9slr8871ymh0lbjcagjyf0l338hflpfpfia15")))

(define-public crate-wav_io-0.1 (crate (name "wav_io") (vers "0.1.2") (hash "0ip7gsz5jn5wfsnqwv5bzyfzrxjbnc1445xym5pcg7645x7z3v95")))

(define-public crate-wav_io-0.1 (crate (name "wav_io") (vers "0.1.3") (hash "185mkgmscm8i60j91vcqgd7k3scdi225nckplraf3nmwgcfzip0c")))

(define-public crate-wav_io-0.1 (crate (name "wav_io") (vers "0.1.4") (hash "122ls6agfzgr7f0xnnwyshx3j6plvxyrf5p929zs7n45q99l8mhy")))

(define-public crate-wav_io-0.1 (crate (name "wav_io") (vers "0.1.5") (hash "0nfar6p13ch6ylbazq46ma2mnpp2inzrn1h7c7f2j53fz2igxpdb")))

(define-public crate-wav_io-0.1 (crate (name "wav_io") (vers "0.1.6") (hash "0612d8jq90n83gjzbb23i9yrllzw9942li7znv210pvy354pzxqi")))

(define-public crate-wav_io-0.1 (crate (name "wav_io") (vers "0.1.7") (hash "04bmb14b30y61qn7flfq3mf2qv6p9hc72pbbnlf6x3vwg212xk7f")))

(define-public crate-wav_io-0.1 (crate (name "wav_io") (vers "0.1.8") (hash "1wk6h6556gsdipq7m9z057c7hvzmw2acl3dcrsff0fyn3yywfa1r")))

(define-public crate-wav_io-0.1 (crate (name "wav_io") (vers "0.1.10") (hash "0b9gxxds8km2l3275bdzfryvznb4n6nhisrcn3izclkfjv53v4rf")))

(define-public crate-wav_io-0.1 (crate (name "wav_io") (vers "0.1.11") (hash "10vihqhhp5idiln6xyiw2v1jpljfqd2982x14aafdpll24w8i5yb")))

(define-public crate-wav_io-0.1 (crate (name "wav_io") (vers "0.1.12") (hash "0s7l6lib1i486q5w378y3fbm3pyf5s24klfps31i25pc49n906v2")))

(define-public crate-wav_io-0.1 (crate (name "wav_io") (vers "0.1.13") (hash "1mljwv89cfh1hbkj9bwfwf0azrdahriji6vc28ic7whhky0047kp")))

(define-public crate-wav_io-0.1 (crate (name "wav_io") (vers "0.1.14") (hash "0cqxg944shb620y9hgrzk5c3kqfkpcfavfwllv5956ppz7zny8xk")))

(define-public crate-wav_reader-0.1 (crate (name "wav_reader") (vers "0.1.0") (hash "19psyqgpv1bnysma2b7cny5kj2s7pj91haz080s19gm6dy474j2s")))

(define-public crate-wav_reader-0.1 (crate (name "wav_reader") (vers "0.1.1") (hash "1dc6yqv8f5fwdp7xav3iy43zxrbbrw2xa5xknylq8hgawwgg2jfs")))

(define-public crate-wav_reader-0.1 (crate (name "wav_reader") (vers "0.1.2") (hash "16sklmcfjla0pdqk2jnxx3h562vdy7x4vw07k4855vf3plampkq2")))

(define-public crate-wav_reader-0.1 (crate (name "wav_reader") (vers "0.1.3") (hash "1d9wid00rnqgdqjhwixfds45w3yls00rlpgl1b6wgd7cyp9irs01")))

