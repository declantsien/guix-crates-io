(define-module (crates-io wa bt) #:use-module (crates-io))

(define-public crate-wabt-0.1 (crate (name "wabt") (vers "0.1.0") (deps (list (crate-dep (name "wabt-sys") (req "^0.1") (default-features #t) (kind 0)))) (hash "0knanlbhs3bmvpqfd0ka4njs2547wm47qy0nvcjdxf5fb06kb9v5")))

(define-public crate-wabt-0.1 (crate (name "wabt") (vers "0.1.1") (deps (list (crate-dep (name "wabt-sys") (req "^0.1.1") (default-features #t) (kind 0)))) (hash "0n2vc9wl3420d8dw80lp88lhc5kzkkj8gvd2hi7w85n80yzavb0z")))

(define-public crate-wabt-0.1 (crate (name "wabt") (vers "0.1.2") (deps (list (crate-dep (name "wabt-sys") (req "^0.1.1") (default-features #t) (kind 0)))) (hash "1gyapinqk1g289zkakf52x35phc8hp4wsq70j7yla9vwdgw443r3")))

(define-public crate-wabt-0.1 (crate (name "wabt") (vers "0.1.3") (deps (list (crate-dep (name "wabt-sys") (req "^0.1.1") (default-features #t) (kind 0)))) (hash "0pmjrl9prd2l9anh7qa0wliarccmfm9ynp95vr1ds4hbwk949nxf")))

(define-public crate-wabt-0.1 (crate (name "wabt") (vers "0.1.4") (deps (list (crate-dep (name "wabt-sys") (req "^0.1.2") (default-features #t) (kind 0)))) (hash "03sp367brrnvarj6ncpzbcsbc9gigzw0n98r325a2g7v008r7gxq")))

(define-public crate-wabt-0.1 (crate (name "wabt") (vers "0.1.5") (deps (list (crate-dep (name "wabt-sys") (req "^0.1.3") (default-features #t) (kind 0)))) (hash "0xma0chjnwrc1yylkhnw3l9sh6p903cr27zmfh9y1q2494h2jgs9")))

(define-public crate-wabt-0.1 (crate (name "wabt") (vers "0.1.6") (deps (list (crate-dep (name "wabt-sys") (req "^0.1.3") (default-features #t) (kind 0)))) (hash "0rkhdwacq69fvd8rpsikhn4ndrgp4r6n6dak5mgbn3d0c1svfzlg")))

(define-public crate-wabt-0.1 (crate (name "wabt") (vers "0.1.7") (deps (list (crate-dep (name "wabt-sys") (req "^0.1.3") (default-features #t) (kind 0)))) (hash "138bqpl3x0s5r32l3j0s1f1dh5xpanca8b34jfnc113484yhdiah")))

(define-public crate-wabt-0.1 (crate (name "wabt") (vers "0.1.8") (deps (list (crate-dep (name "serde") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "tempdir") (req "^0.3.6") (default-features #t) (kind 0)) (crate-dep (name "wabt-sys") (req "^0.1.4") (default-features #t) (kind 0)))) (hash "1rah0250c7bawqcmjfmjb27dsr6rb3b0vf912cqr17108iqrj0p9")))

(define-public crate-wabt-0.2 (crate (name "wabt") (vers "0.2.0") (deps (list (crate-dep (name "serde") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "tempdir") (req "^0.3.6") (default-features #t) (kind 0)) (crate-dep (name "wabt-sys") (req "^0.1.4") (default-features #t) (kind 0)))) (hash "0b1xxphwj2h2xbfzlarx67zwycm3mjwxq0bb0bdpy412h96mizqn")))

(define-public crate-wabt-0.2 (crate (name "wabt") (vers "0.2.1") (deps (list (crate-dep (name "serde") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "tempdir") (req "^0.3.6") (default-features #t) (kind 0)) (crate-dep (name "wabt-sys") (req "^0.1.5") (default-features #t) (kind 0)))) (hash "1x51cn8c5k34l5al436g294hrjn3jrancgpz5cb1jan1i8ga89g6") (yanked #t)))

(define-public crate-wabt-0.2 (crate (name "wabt") (vers "0.2.2") (deps (list (crate-dep (name "serde") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "tempdir") (req "^0.3.6") (default-features #t) (kind 0)) (crate-dep (name "wabt-sys") (req "^0.1.4") (default-features #t) (kind 0)))) (hash "0gfzr4rypddjxwll4k8d5rrifnhpvljxfngz63zlklcaq2iikqci")))

(define-public crate-wabt-0.2 (crate (name "wabt") (vers "0.2.3") (deps (list (crate-dep (name "serde") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "tempdir") (req "^0.3.6") (default-features #t) (kind 0)) (crate-dep (name "wabt-sys") (req "^0.1.4") (default-features #t) (kind 0)))) (hash "1a3ispbaaprr24l8277lbdn6i21bisa687svm59gvafxwp7dg5pr") (yanked #t)))

(define-public crate-wabt-0.3 (crate (name "wabt") (vers "0.3.0") (deps (list (crate-dep (name "serde") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "tempdir") (req "^0.3.6") (default-features #t) (kind 0)) (crate-dep (name "wabt-sys") (req "^0.1.4") (default-features #t) (kind 0)))) (hash "00d8ih9qv2cnq4v0b1jzz39vhpycgnzc8666zv7jimz5y2csw4k4")))

(define-public crate-wabt-0.4 (crate (name "wabt") (vers "0.4.0") (deps (list (crate-dep (name "serde") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "wabt-sys") (req "^0.2.0") (default-features #t) (kind 0)))) (hash "18hq7zlvz79xlidvadz18ahzqxhi3j4k7li4yc2jgkww4i1yaahq")))

(define-public crate-wabt-0.5 (crate (name "wabt") (vers "0.5.0") (deps (list (crate-dep (name "serde") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "wabt-sys") (req "^0.3.0") (default-features #t) (kind 0)))) (hash "0d47kfrv4jn6zrd7447s4xjlq139dslhrddgnlr1a7r0zd1pgcz9")))

(define-public crate-wabt-0.6 (crate (name "wabt") (vers "0.6.0") (deps (list (crate-dep (name "serde") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "wabt-sys") (req "^0.4.0") (default-features #t) (kind 0)))) (hash "0xas6g95jjfzml4k051fda81d9zgixjfxva5fl58r2gj6bwzls07")))

(define-public crate-wabt-0.7 (crate (name "wabt") (vers "0.7.0") (deps (list (crate-dep (name "serde") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "wabt-sys") (req "^0.5.0") (default-features #t) (kind 0)))) (hash "1lknfv6b747aaizm38jh6ip6kxxb60s7z9g5li8mpkcwa3f6x2lf")))

(define-public crate-wabt-0.7 (crate (name "wabt") (vers "0.7.1") (deps (list (crate-dep (name "serde") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "wabt-sys") (req "^0.5.1") (default-features #t) (kind 0)))) (hash "0qxp9hqqnv3ak9c9i0apizan209ydqzm4hbzf7zps1yn9d4v492v")))

(define-public crate-wabt-0.7 (crate (name "wabt") (vers "0.7.2") (deps (list (crate-dep (name "serde") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "wabt-sys") (req "^0.5.2") (default-features #t) (kind 0)))) (hash "19ciw4j755km7rrxfh3q80ihfh847bqmrbqz1hj5awj6gvwg1wcg")))

(define-public crate-wabt-0.7 (crate (name "wabt") (vers "0.7.3") (deps (list (crate-dep (name "serde") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "wabt-sys") (req "^0.5.3") (default-features #t) (kind 0)))) (hash "07x9wfs0l2pk84mfkmff329ixpcfqyi342bwv0ymyz646hcalih2")))

(define-public crate-wabt-0.7 (crate (name "wabt") (vers "0.7.4") (deps (list (crate-dep (name "serde") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "wabt-sys") (req "^0.5.3") (default-features #t) (kind 0)))) (hash "1p9ascb9bygxw6n18lq9pphm5ba4pw7n83z78xscr47312jn7r3l")))

(define-public crate-wabt-0.8 (crate (name "wabt") (vers "0.8.0") (deps (list (crate-dep (name "serde") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "wabt-sys") (req "^0.6.0") (default-features #t) (kind 0)))) (hash "0fr4nbdhk4pi5h8x2qgsqcmz8byvkpz47jrga8slhxvcyl66frgg")))

(define-public crate-wabt-0.9 (crate (name "wabt") (vers "0.9.0") (deps (list (crate-dep (name "serde") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "wabt-sys") (req "^0.6.1") (default-features #t) (kind 0)))) (hash "1hg1xhw82nqfp8q8d59nzw17mx0lnb65089w278cppchs50dsznh")))

(define-public crate-wabt-0.9 (crate (name "wabt") (vers "0.9.1") (deps (list (crate-dep (name "serde") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "wabt-sys") (req "^0.6.1") (default-features #t) (kind 0)))) (hash "1rr0370332d9fjsq0idgvmrs3261bahsifl0cbv2v92ck3bgbdcl")))

(define-public crate-wabt-0.9 (crate (name "wabt") (vers "0.9.2") (deps (list (crate-dep (name "serde") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "wabt-sys") (req "^0.7") (default-features #t) (kind 0)))) (hash "14v6lh2xcfj0glakdf7ri7agjra2b53ryq42d50pirf6hq95qp1w") (yanked #t)))

(define-public crate-wabt-0.10 (crate (name "wabt") (vers "0.10.0") (deps (list (crate-dep (name "serde") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "wabt-sys") (req "^0.8") (default-features #t) (kind 0)))) (hash "171vjvc9arnkd54a2ns55ww3jwm47bs7q46gpj9s50bcbqyzkgh0")))

(define-public crate-wabt-sys-0.1 (crate (name "wabt-sys") (vers "0.1.0") (deps (list (crate-dep (name "bindgen") (req "^0.30.0") (default-features #t) (kind 1)) (crate-dep (name "cc") (req "^1.0.4") (default-features #t) (kind 1)) (crate-dep (name "cmake") (req "^0.1.25") (default-features #t) (kind 1)))) (hash "0q95vgsrxsabxcrqkkcwb2aq84srxm34srnaj6h0gv3bvy971kc2")))

(define-public crate-wabt-sys-0.1 (crate (name "wabt-sys") (vers "0.1.1") (deps (list (crate-dep (name "cmake") (req "^0.1.25") (default-features #t) (kind 1)))) (hash "1ardhf3biacphfrisnkm9r4z3nzlc9fnr2r4ai8rgy21h01qzqag")))

(define-public crate-wabt-sys-0.1 (crate (name "wabt-sys") (vers "0.1.2") (deps (list (crate-dep (name "cmake") (req "^0.1.25") (default-features #t) (kind 1)))) (hash "0hrhgn68gbv5g48hyz5hzqfysivhqm9v41hpn8890x72hzb6kvcm")))

(define-public crate-wabt-sys-0.1 (crate (name "wabt-sys") (vers "0.1.3") (deps (list (crate-dep (name "cmake") (req "^0.1.25") (default-features #t) (kind 1)))) (hash "1mc138mkc92lsw3bz13052phdja0yn3j33hz89pphbnmsc8k0q35")))

(define-public crate-wabt-sys-0.1 (crate (name "wabt-sys") (vers "0.1.4") (deps (list (crate-dep (name "cc") (req "^1.0.4") (default-features #t) (kind 1)) (crate-dep (name "cmake") (req "^0.1.25") (default-features #t) (kind 1)))) (hash "0ab5bsgwvb1kvip7rk792kv8679mcsz99pvfk7k9p0ypdkcv2rzw")))

(define-public crate-wabt-sys-0.1 (crate (name "wabt-sys") (vers "0.1.5") (deps (list (crate-dep (name "cc") (req "^1.0.4") (default-features #t) (kind 1)) (crate-dep (name "cmake") (req "^0.1.25") (default-features #t) (kind 1)))) (hash "0dda4d0smadzikklvraijifsl877sbsw0bhgwqfl9pav3zwwv64j") (yanked #t)))

(define-public crate-wabt-sys-0.2 (crate (name "wabt-sys") (vers "0.2.0") (deps (list (crate-dep (name "cc") (req "^1.0.4") (default-features #t) (kind 1)) (crate-dep (name "cmake") (req "^0.1.25") (default-features #t) (kind 1)))) (hash "18vxspfgrn54zfsc6y0k2jvqqfy4m9jzbcj1i5hk4asajdmpr9wc")))

(define-public crate-wabt-sys-0.3 (crate (name "wabt-sys") (vers "0.3.0") (deps (list (crate-dep (name "cc") (req "^1.0.4") (default-features #t) (kind 1)) (crate-dep (name "cmake") (req "^0.1.25") (default-features #t) (kind 1)))) (hash "0w54am4y5rikjb47adrsr0m4mnbws749d1xswwz5hli3f78ch7ap")))

(define-public crate-wabt-sys-0.4 (crate (name "wabt-sys") (vers "0.4.0") (deps (list (crate-dep (name "cc") (req "^1.0.4") (default-features #t) (kind 1)) (crate-dep (name "cmake") (req "^0.1.32") (default-features #t) (kind 1)))) (hash "1p79cpv2786z22axv83ginxqys0y788kw7v1lmfk366nl27rmaja")))

(define-public crate-wabt-sys-0.5 (crate (name "wabt-sys") (vers "0.5.0") (deps (list (crate-dep (name "cc") (req "^1.0.4") (default-features #t) (kind 1)) (crate-dep (name "cmake") (req "^0.1.32") (default-features #t) (kind 1)))) (hash "1mkj5c7dsk4a398xfnrhyl4vvib9vliak56pk83j11kr54hcmb4z")))

(define-public crate-wabt-sys-0.5 (crate (name "wabt-sys") (vers "0.5.1") (deps (list (crate-dep (name "cc") (req "^1.0.4") (default-features #t) (kind 1)) (crate-dep (name "cmake") (req "^0.1.32") (default-features #t) (kind 1)))) (hash "1pbdkqzqch137rm28801n47cwiyas64hvln53p77q1icsbhyyahj")))

(define-public crate-wabt-sys-0.5 (crate (name "wabt-sys") (vers "0.5.2") (deps (list (crate-dep (name "cc") (req "^1.0.4") (default-features #t) (kind 1)) (crate-dep (name "cmake") (req "^0.1.32") (default-features #t) (kind 1)) (crate-dep (name "glob") (req "^0.2.11") (default-features #t) (target "cfg(windows)") (kind 1)))) (hash "18hz0l1lyqvn9pcdkxjar9rzx5vg1bjr3ncn5qakz4hazqmrij0c")))

(define-public crate-wabt-sys-0.5 (crate (name "wabt-sys") (vers "0.5.3") (deps (list (crate-dep (name "cc") (req "^1.0.4") (default-features #t) (kind 1)) (crate-dep (name "cmake") (req "^0.1.32") (default-features #t) (kind 1)) (crate-dep (name "glob") (req "^0.2.11") (default-features #t) (target "cfg(windows)") (kind 1)))) (hash "0lca9vgwzwc17j0px738wga7nxwbpjhxxq1h2xv68vh9c6xkc8s6")))

(define-public crate-wabt-sys-0.5 (crate (name "wabt-sys") (vers "0.5.4") (deps (list (crate-dep (name "cc") (req "^1.0.4") (default-features #t) (kind 1)) (crate-dep (name "cmake") (req "^0.1.32") (default-features #t) (kind 1)) (crate-dep (name "glob") (req "^0.2.11") (default-features #t) (target "cfg(windows)") (kind 1)))) (hash "0g3yxp3m047mwg6q8b1yfm0isripbqvifdsb226mk0lyf4jmn9m6")))

(define-public crate-wabt-sys-0.6 (crate (name "wabt-sys") (vers "0.6.0") (deps (list (crate-dep (name "cc") (req "^1.0.4") (default-features #t) (kind 1)) (crate-dep (name "cmake") (req "^0.1.32") (default-features #t) (kind 1)) (crate-dep (name "glob") (req "^0.2.11") (default-features #t) (target "cfg(windows)") (kind 1)))) (hash "0qjvg0mm0xa9ycm0fi7cny158hd4fs3x7mwg16k515irkdyisldd")))

(define-public crate-wabt-sys-0.6 (crate (name "wabt-sys") (vers "0.6.1") (deps (list (crate-dep (name "cc") (req "^1.0.4") (default-features #t) (kind 1)) (crate-dep (name "cmake") (req "^0.1.32") (default-features #t) (kind 1)) (crate-dep (name "glob") (req "^0.2.11") (default-features #t) (target "cfg(windows)") (kind 1)))) (hash "1apfcgz7yb0nn1ad9fy347dq7w7yczyfqg4jf55xn2hh44cchr5h")))

(define-public crate-wabt-sys-0.7 (crate (name "wabt-sys") (vers "0.7.0") (deps (list (crate-dep (name "cc") (req "^1.0.4") (default-features #t) (kind 1)) (crate-dep (name "cmake") (req "^0.1.32") (default-features #t) (kind 1)) (crate-dep (name "glob") (req "^0.2.11") (default-features #t) (target "cfg(windows)") (kind 1)))) (hash "021sw7271lr2wncm5n8jh6a6g666jaw3a25r7b0pvbbar4yiapdg")))

(define-public crate-wabt-sys-0.7 (crate (name "wabt-sys") (vers "0.7.1") (deps (list (crate-dep (name "cc") (req "^1.0.4") (default-features #t) (kind 1)) (crate-dep (name "cmake") (req "^0.1.32") (default-features #t) (kind 1)) (crate-dep (name "glob") (req "^0.2.11") (default-features #t) (target "cfg(windows)") (kind 1)))) (hash "114hiwxrv95jry981qf3z27p904qxqicmlx8szx9cp9ypcz09mr3")))

(define-public crate-wabt-sys-0.7 (crate (name "wabt-sys") (vers "0.7.2") (deps (list (crate-dep (name "cc") (req "^1.0.4") (default-features #t) (kind 1)) (crate-dep (name "cmake") (req "^0.1.32") (default-features #t) (kind 1)) (crate-dep (name "glob") (req "^0.2.11") (default-features #t) (target "cfg(windows)") (kind 1)))) (hash "1pv6wr6b6xnkqkpywr92vi8aj5n9rh0m7dpnwba1zf3yizwrbih1") (yanked #t)))

(define-public crate-wabt-sys-0.8 (crate (name "wabt-sys") (vers "0.8.0") (deps (list (crate-dep (name "cc") (req "^1.0.4") (default-features #t) (kind 1)) (crate-dep (name "cmake") (req "^0.1.32") (default-features #t) (kind 1)) (crate-dep (name "glob") (req "^0.2.11") (default-features #t) (target "cfg(windows)") (kind 1)))) (hash "14bp9j327i975rkvyrc5hks3s1063ig9ngkidsc1cgpnb4qh8khs")))

