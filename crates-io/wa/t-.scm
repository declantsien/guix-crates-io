(define-module (crates-io wa t-) #:use-module (crates-io))

(define-public crate-wat-ast-0.1 (crate (name "wat-ast") (vers "0.1.0") (hash "0dlw31268k8gq0n7hpl0xx8liqd1l80fsw11mr3a3mla1f2rqaqi")))

(define-public crate-wat-ast-0.2 (crate (name "wat-ast") (vers "0.2.0") (deps (list (crate-dep (name "wast") (req "^26.0") (kind 0)))) (hash "12d6i6k4y2d26lf37bwbrjbhswifsg2fwz8lxmxl099is2f4b97c")))

(define-public crate-wat-ast-0.2 (crate (name "wat-ast") (vers "0.2.1") (deps (list (crate-dep (name "wast") (req "^26.0") (kind 0)))) (hash "021dz3pvjjvp4q7i51g0xhg2cmzf7nmv8kxd7xllpjndh7xsvfc1")))

(define-public crate-wat-ast-0.2 (crate (name "wat-ast") (vers "0.2.2") (deps (list (crate-dep (name "wast") (req "^26.0") (kind 0)))) (hash "0x0nqdg82pbd0zx5rm2nxvv04q3gqiyjcznh778c5wl003jbapxj")))

(define-public crate-wat-ast-0.2 (crate (name "wat-ast") (vers "0.2.3") (deps (list (crate-dep (name "wast") (req "^26.0") (kind 0)))) (hash "1hg289r17ypgrfqpan770r6pnpy5cr53hqwnsy0h61p361vzi88m")))

(define-public crate-wat-ast-0.3 (crate (name "wat-ast") (vers "0.3.0") (deps (list (crate-dep (name "wast") (req "^26.0") (kind 0)))) (hash "074w26ywhgbjm10zd0qmf0rihm1wa7zhkr0i6r5pah8wiamg7w5n")))

(define-public crate-wat-ast-0.4 (crate (name "wat-ast") (vers "0.4.0") (deps (list (crate-dep (name "wast") (req "^26.0") (kind 0)))) (hash "0gp9dh1jkky0lcdcjaj9f2jrf11g6dk1z38ygnmb5mlg321aksz6")))

(define-public crate-wat-ast-0.5 (crate (name "wat-ast") (vers "0.5.0") (deps (list (crate-dep (name "wast") (req "^26.0") (kind 0)))) (hash "0c9maib4ik7gbclzwaclchsl7s0ll375yhjssy6n3fgj79hjp7lv")))

(define-public crate-wat-ast-0.5 (crate (name "wat-ast") (vers "0.5.1") (deps (list (crate-dep (name "wast") (req "^26.0") (kind 0)))) (hash "08rigcfcvpkzin8b7iw9iydzg34fs88ccr6kfn7x0f9yxcclqi86")))

(define-public crate-wat-ast-0.5 (crate (name "wat-ast") (vers "0.5.2") (deps (list (crate-dep (name "wast") (req ">=26.0.0, <27.0.0") (kind 0)))) (hash "0qx0z79mggs0xrymq6kfmz4wphqdcnbrkxcg9klmbk1f2y6jxsw5")))

(define-public crate-wat-ast-0.6 (crate (name "wat-ast") (vers "0.6.0") (deps (list (crate-dep (name "wast") (req ">=26.0.0, <27.0.0") (kind 0)))) (hash "0zlhjzqmrj6ha2gwd2xgz2lqsggj10aip6va9cn2rpmfaqq0a7jz")))

(define-public crate-wat-ast-0.7 (crate (name "wat-ast") (vers "0.7.0") (deps (list (crate-dep (name "wast") (req ">=26.0.0, <27.0.0") (kind 0)))) (hash "1rp08l4hy830cddvi4nqsm9ksajjp8ga9k8mkyfsvqcfcm4z3r09")))

(define-public crate-wat-ast-0.8 (crate (name "wat-ast") (vers "0.8.0") (deps (list (crate-dep (name "wast") (req ">=26.0.0, <27.0.0") (kind 0)))) (hash "1vr692mlwr4ixa35ysplhq9ipr6jarl9gvgkc95s8h8jwcj0n7jq")))

(define-public crate-wat-ast-0.8 (crate (name "wat-ast") (vers "0.8.1") (deps (list (crate-dep (name "wast") (req ">=26.0.0, <27.0.0") (kind 0)))) (hash "02r084z3fl872vv1ic63sa5jxadl2rlmps5pm4s7zlkqkr2kv3qy")))

(define-public crate-wat-ast-0.8 (crate (name "wat-ast") (vers "0.8.2") (deps (list (crate-dep (name "wast") (req ">=26.0.0, <27.0.0") (kind 0)))) (hash "0j85gyinpwgiczrrqsgdaqyj49f11mlgdm6iq0z4cnj3sziqxk3r")))

(define-public crate-wat-ast-0.9 (crate (name "wat-ast") (vers "0.9.0") (deps (list (crate-dep (name "wast") (req ">=26.0.0, <27.0.0") (kind 0)))) (hash "005w4b1mh1z6fc56riip2m69azjznnfyh2lvfkqf4f6v671k5c4z")))

(define-public crate-wat-ast-0.9 (crate (name "wat-ast") (vers "0.9.1") (deps (list (crate-dep (name "wast") (req ">=26.0.0, <27.0.0") (kind 0)))) (hash "1cjw8ckc1gz9amiyhxq0skv7lrdb6yy2d2pjd05m2ipflwn1cgyv")))

(define-public crate-wat-ast-0.10 (crate (name "wat-ast") (vers "0.10.0") (deps (list (crate-dep (name "wast") (req ">=26.0.0, <27.0.0") (kind 0)))) (hash "18si2kq9vjj9q4yy7psm0q7pqm5wa40gp5g56ydvq3gcn1b6y68j")))

(define-public crate-wat-ast-0.10 (crate (name "wat-ast") (vers "0.10.1") (deps (list (crate-dep (name "wast") (req ">=26.0.0, <27.0.0") (kind 0)))) (hash "1ik2p72bswhywg4zzr6k83pldb5328w2hbd2pyf8jcnn0hg26z4y")))

(define-public crate-wat-ast-0.11 (crate (name "wat-ast") (vers "0.11.0") (deps (list (crate-dep (name "wast") (req ">=26.0.0, <27.0.0") (kind 0)))) (hash "08hp19hq32h18qy4v53v6ff40fn6cdjc1in5yyn13kif7cg7l2ks")))

(define-public crate-wat-ast-0.12 (crate (name "wat-ast") (vers "0.12.0") (deps (list (crate-dep (name "wast") (req ">=26.0.0, <27.0.0") (kind 0)))) (hash "1vj1yq62q4c067d7xbsdx67h6vy79lc2kvnr4p4pz217v80mrv61")))

(define-public crate-wat-ast-0.13 (crate (name "wat-ast") (vers "0.13.0") (deps (list (crate-dep (name "wast") (req ">=26.0.0, <27.0.0") (kind 0)))) (hash "03idf87sr9n7mhs02w4wlmx72d62gh30c8pwq6crd72c37ywqmh4")))

(define-public crate-wat-ast-0.13 (crate (name "wat-ast") (vers "0.13.1") (deps (list (crate-dep (name "wast") (req ">=26.0.0, <27.0.0") (kind 0)))) (hash "08gvd0jhxpxj18kccq527d1w434xzlpkk849738il63kh4fwipgm")))

(define-public crate-wat-ast-0.13 (crate (name "wat-ast") (vers "0.13.2") (deps (list (crate-dep (name "wast") (req ">=26.0.0, <27.0.0") (kind 0)))) (hash "0hd250nm2nwpw30knr26k43nxa6h8qdxp1g8jqs1ffhlk9hlr19j")))

(define-public crate-wat-ast-0.14 (crate (name "wat-ast") (vers "0.14.0") (deps (list (crate-dep (name "wast") (req ">=26.0.0, <27.0.0") (kind 0)))) (hash "077biwsn38bc5r8mv7vs85pyw4jd1hgwcc6bp2qiqd9rznhhnz00")))

(define-public crate-wat-ast-0.14 (crate (name "wat-ast") (vers "0.14.1") (deps (list (crate-dep (name "wast") (req ">=26.0.0, <27.0.0") (kind 0)))) (hash "16mlmyfcrgnkag1gww5y4almgma4h34gfbjbdbjbiwwaf2av0yf0")))

(define-public crate-wat-ast-0.14 (crate (name "wat-ast") (vers "0.14.2") (deps (list (crate-dep (name "wast") (req ">=26.0.0, <27.0.0") (kind 0)))) (hash "0agi9x8crwvly887amva1mac86h3kwwrqx3hk9l627jjpxwibvhr")))

(define-public crate-wat-ast-0.14 (crate (name "wat-ast") (vers "0.14.3") (deps (list (crate-dep (name "wast") (req ">=26.0.0, <27.0.0") (kind 0)))) (hash "0ka14h5yzw91mfpw62j9d0glhhh03bvnyz2lgcsw86p477rg57bs")))

(define-public crate-wat-ast-0.14 (crate (name "wat-ast") (vers "0.14.4") (deps (list (crate-dep (name "wast") (req ">=26.0.0, <27.0.0") (kind 0)))) (hash "11kn0z59d14kr6zqpq8d4124bb55xxl9whqldlvc38yfaw6mrcp6")))

(define-public crate-wat-ast-0.14 (crate (name "wat-ast") (vers "0.14.5") (deps (list (crate-dep (name "wast") (req ">=26.0.0, <27.0.0") (kind 0)))) (hash "0rk7sk5kvcp6nfjyv4g2bf59mr29v4qvfs96si8s43658z87lcih")))

(define-public crate-wat-ast-0.14 (crate (name "wat-ast") (vers "0.14.6") (deps (list (crate-dep (name "wast") (req ">=26.0.0, <27.0.0") (kind 0)))) (hash "1sk05pvj38hbzm3sqhwjzrwvqzbjx6lvm49p8r7yrzv25saksi0h")))

(define-public crate-wat-ast-0.14 (crate (name "wat-ast") (vers "0.14.7") (deps (list (crate-dep (name "wast") (req ">=26.0.0, <27.0.0") (kind 0)))) (hash "02msjbjsjgl8x7mjmh66600sijgcyv6p9skqzwk4m5p0myagdfhz")))

(define-public crate-wat-ast-0.14 (crate (name "wat-ast") (vers "0.14.8") (deps (list (crate-dep (name "wast") (req ">=26.0.0, <27.0.0") (kind 0)))) (hash "18znkd5crdnsrw39z498s31hwrwc5kzmfhpm49m3y33p5g145w46")))

(define-public crate-wat-ast-0.14 (crate (name "wat-ast") (vers "0.14.9") (deps (list (crate-dep (name "wast") (req ">=26.0.0, <27.0.0") (kind 0)))) (hash "17w8rmgdjrja2ds03aa2k7rqp4f6cpdgyl4qmgkf21k3va7zi4pj")))

(define-public crate-wat-ast-0.14 (crate (name "wat-ast") (vers "0.14.10") (deps (list (crate-dep (name "wast") (req ">=26.0.0, <27.0.0") (kind 0)))) (hash "0shh02jfwgz11dggx736w8cm8ji4w4lhz8jvrgnphm3m1wn5wp0n")))

(define-public crate-wat-ast-0.15 (crate (name "wat-ast") (vers "0.15.0") (deps (list (crate-dep (name "wast") (req ">=26.0.0, <27.0.0") (kind 0)))) (hash "1jw63wzn4mz70gx9r5j344irp8xw99l7lygb3wdva5l51qsqn4am")))

(define-public crate-wat-ast-0.15 (crate (name "wat-ast") (vers "0.15.1") (deps (list (crate-dep (name "wast") (req ">=26.0.0, <27.0.0") (kind 0)))) (hash "15xrlkcgfid2hp3p7vgj2pfcy88zadp0b29w65dn2d5zk9lvs6r2")))

(define-public crate-wat-ast-0.15 (crate (name "wat-ast") (vers "0.15.2") (deps (list (crate-dep (name "wast") (req ">=26.0.0, <27.0.0") (kind 0)))) (hash "0mwbvs7ddzx4cxnz6pai15i2b48fipsq3xyy4birjqwxqaxkf1ck")))

(define-public crate-wat-ast-0.16 (crate (name "wat-ast") (vers "0.16.0") (deps (list (crate-dep (name "wast") (req ">=26.0.0, <27.0.0") (kind 0)))) (hash "13a6dbz7vjnbhkglms161im3ad2psqqjrm2w6vs7aimvmydxfv6d")))

(define-public crate-wat-ast-0.17 (crate (name "wat-ast") (vers "0.17.0") (deps (list (crate-dep (name "wast") (req ">=26.0.0, <27.0.0") (kind 0)))) (hash "0a7cpc1iizvgpyrn2h1m31gxbz7zcmn0dswmhhp1dldf115v09nb")))

(define-public crate-wat-ast-0.17 (crate (name "wat-ast") (vers "0.17.1") (deps (list (crate-dep (name "wast") (req ">=26.0.0, <27.0.0") (kind 0)))) (hash "06gnjp3w9rmd9mbnl2mzacv96yl9nlmkcjvs5hng0g8ivrwl0r7r")))

(define-public crate-wat-ast-0.17 (crate (name "wat-ast") (vers "0.17.2") (deps (list (crate-dep (name "wast") (req ">=26.0.0, <27.0.0") (kind 0)))) (hash "054bnppbkkw422qb9ys9yz2z2zwr23pg42s6z6pcn6xk9mb4h5gq")))

(define-public crate-wat-ast-0.18 (crate (name "wat-ast") (vers "0.18.0") (deps (list (crate-dep (name "wast") (req ">=26.0.0, <27.0.0") (kind 0)))) (hash "0r9pbykj1zjymzfpg4i4psq811fcna0m2bfja7n1cmdzbzj7yazy")))

(define-public crate-wat-ast-0.18 (crate (name "wat-ast") (vers "0.18.1") (deps (list (crate-dep (name "wast") (req ">=26.0.0, <27.0.0") (kind 0)))) (hash "10b03fvab7767cq73ppx6pp1ixxmn86vg6ddz9qc9n50vz71h688")))

(define-public crate-wat-ast-0.18 (crate (name "wat-ast") (vers "0.18.2") (deps (list (crate-dep (name "wast") (req ">=26.0.0, <27.0.0") (kind 0)))) (hash "01wlmhbh63mlzwy54jlyflv24pxlqw2wxyklajnn6lj4mh51l3d7")))

(define-public crate-wat-ast-0.18 (crate (name "wat-ast") (vers "0.18.3") (deps (list (crate-dep (name "wast") (req ">=26.0.0, <27.0.0") (kind 0)))) (hash "1ric60j8m9hrvx601bmwrq56awck18862g99dcriqaqk494qk61j")))

(define-public crate-wat-ast-0.19 (crate (name "wat-ast") (vers "0.19.0") (deps (list (crate-dep (name "wast") (req ">=26.0.0, <27.0.0") (kind 0)))) (hash "1h7qzqsb35050vg4srzw4s9ziqdn1w3rf2zx89nqpnjmv3ilydjc")))

(define-public crate-wat-ast-0.19 (crate (name "wat-ast") (vers "0.19.1") (deps (list (crate-dep (name "wast") (req ">=26.0.0, <27.0.0") (kind 0)))) (hash "136mw5q1gfgm1p470w3gcglzwlahbal003g2dzjhhgpzbc0y2cjw")))

(define-public crate-wat-ast-0.19 (crate (name "wat-ast") (vers "0.19.2") (deps (list (crate-dep (name "wast") (req ">=26.0.0, <27.0.0") (kind 0)))) (hash "1a4g6mvy9izd2m4kmkrzn01a2qxycmcmyg6r15nzznjha5b6wq0z")))

(define-public crate-wat-ast-0.19 (crate (name "wat-ast") (vers "0.19.3") (deps (list (crate-dep (name "wast") (req ">=26.0.0, <27.0.0") (kind 0)))) (hash "009gfsky2jbwkgj7ax5ik4jmll7mk0vzb9aypjvzfc3ci0bc81fb")))

(define-public crate-wat-ast-0.19 (crate (name "wat-ast") (vers "0.19.4") (deps (list (crate-dep (name "wast") (req ">=26.0.0, <27.0.0") (kind 0)))) (hash "0xf7xfpi4vpgr5zcfk8i522gfxdk7s01mmk1lma4r33j6h00yvhp")))

(define-public crate-wat-ast-0.19 (crate (name "wat-ast") (vers "0.19.5") (deps (list (crate-dep (name "wast") (req ">=26.0.0, <27.0.0") (kind 0)))) (hash "0rnkp69392ar98pbffj52q6islhlpp9251hvl33qv7bb6d38jahd")))

(define-public crate-wat-ast-0.19 (crate (name "wat-ast") (vers "0.19.6") (deps (list (crate-dep (name "wast") (req ">=26.0.0, <27.0.0") (kind 0)))) (hash "1cfg6284h8b3l4lshcj9b7w71c9db0gdms32lra4ibg2cpbkx21s")))

(define-public crate-wat-ast-0.19 (crate (name "wat-ast") (vers "0.19.7") (deps (list (crate-dep (name "wast") (req ">=26.0.0, <27.0.0") (kind 0)))) (hash "1x17wim479rilcq0ms7nj0f73laijlkl8rv5iw952jknn84329z9")))

(define-public crate-wat-ast-0.19 (crate (name "wat-ast") (vers "0.19.8") (deps (list (crate-dep (name "wast") (req ">=26.0.0, <27.0.0") (kind 0)))) (hash "1c2jdpbyling8g1lxjhh9b2sq8mv64yl7wrqsy9jhi5s60jwgjbz")))

(define-public crate-wat-ast-0.19 (crate (name "wat-ast") (vers "0.19.9") (deps (list (crate-dep (name "wast") (req ">=26.0.0, <27.0.0") (kind 0)))) (hash "076095jb0fbjl7rgxyzgv6bgf9inpggh7q0hqqva71ny3hmrpvvw")))

(define-public crate-wat-ast-0.19 (crate (name "wat-ast") (vers "0.19.10") (deps (list (crate-dep (name "wast") (req ">=26.0.0, <27.0.0") (kind 0)))) (hash "1bf5rnvm3y4k2bszh7a3sk38gbzcv027vi2gldgz5abrnjh3rrb0")))

(define-public crate-wat-ast-0.20 (crate (name "wat-ast") (vers "0.20.0") (deps (list (crate-dep (name "wast") (req "^26.0") (kind 0)))) (hash "0cljga2zrq6cr7j3g3r0xyyfvsbl8q0smzckp7x4myadn35n1q3g")))

(define-public crate-wat-ast-0.20 (crate (name "wat-ast") (vers "0.20.1") (deps (list (crate-dep (name "wast") (req "^26.0") (kind 0)))) (hash "17nh8ndlxkvjbhfv6cjw2nyfjkwh7chvkww42vvl5nh8kdx26822")))

(define-public crate-wat-ast-0.20 (crate (name "wat-ast") (vers "0.20.2") (deps (list (crate-dep (name "wast") (req "^26.0") (kind 0)))) (hash "13f6hq2xk6x7ds0h3f2i1di5dhhq8myfpam8419q4jrmxvd5iyb4")))

(define-public crate-wat-ast-0.20 (crate (name "wat-ast") (vers "0.20.3") (deps (list (crate-dep (name "wast") (req "^26.0") (kind 0)))) (hash "1d6kfm5wh23rxqi5zpk7ll9slkv5pmm5rgh3scdkd47ch0zw37b1")))

