(define-module (crates-io wa ss) #:use-module (crates-io))

(define-public crate-wasserglas-0.1 (crate (name "wasserglas") (vers "0.1.0") (deps (list (crate-dep (name "parking_lot") (req "^0.11") (default-features #t) (kind 0)))) (hash "0pvfgx9nxrxx5q81zxydcdmb715hw0myl2dzb9x7k9xsw4mqb0w3")))

(define-public crate-wassily-0.1 (crate (name "wassily") (vers "0.1.0") (deps (list (crate-dep (name "color-thief") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "image") (req "^0.24") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.18") (default-features #t) (kind 0)) (crate-dep (name "noise") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "num-complex") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "once_cell") (req "^1.17") (default-features #t) (kind 0)) (crate-dep (name "palette") (req "^0.7") (default-features #t) (kind 0)) (crate-dep (name "png") (req "^0.17") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (features (quote ("small_rng"))) (default-features #t) (kind 0)) (crate-dep (name "rand_distr") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "tiny-skia") (req "^0.11") (default-features #t) (kind 0)))) (hash "1faaksvmvni2348pi4s97p8g69xln4982nwgx2ianw7kz60ws6s0")))

(define-public crate-wassup-0.0.0 (crate (name "wassup") (vers "0.0.0-wrong-crate") (hash "1cw7xiis5dypkd513qhrb9s78d4r78mkscpbwa1b05jy36b0sg5n")))

