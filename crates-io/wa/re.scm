(define-module (crates-io wa re) #:use-module (crates-io))

(define-public crate-ware-0.1 (crate (name "ware") (vers "0.1.0") (hash "12fmm5lrj8iapd8a4g3yzj8j0az7jx2v8py6vmg02myl5gqwkfam")))

(define-public crate-ware-1 (crate (name "ware") (vers "1.0.0") (hash "1xnxi9micqjwr8rz5bq52i32gi4sjj210nkdzaqvn01cppiiwmg2")))

(define-public crate-ware-2 (crate (name "ware") (vers "2.0.0-rc1") (hash "1bjvj4ybkc6iyifvhskncc690dlk68pfcpv9669kh8nqd2d4l8ph")))

(define-public crate-ware-2 (crate (name "ware") (vers "2.0.0") (hash "0q7klx652wnfqh5g87gz7gwb24zzkla5wb0a96ly39n86vz39k1x")))

(define-public crate-ware-2 (crate (name "ware") (vers "2.0.1") (hash "0d69wndl9pg9p1jbbrzydf51f5s6k15lshdbrwg7qmw7b5p0nqhw")))

(define-public crate-ware-2 (crate (name "ware") (vers "2.0.2") (hash "0y3f8692j7g8mbrmak5vdzsidd9rkgykpmbwmg1li124iy6c7j7b")))

(define-public crate-warehouse-0.1 (crate (name "warehouse") (vers "0.1.0") (hash "1fzr731yqgfyix3nia3xs1kih8y2hck5y118gqgn9r46dvxgfcvg")))

(define-public crate-wareki-conv-0.1 (crate (name "wareki-conv") (vers "0.1.0") (deps (list (crate-dep (name "chrono") (req "^0.4.34") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.20") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.10.3") (default-features #t) (kind 0)) (crate-dep (name "unicode-jp") (req "^0.4.0") (default-features #t) (kind 0)))) (hash "0gsc4f5v31xan874l6kphm7rcm9c9zyhh2ckwfl0li22i077iar8")))

(define-public crate-wareki-conv-0.2 (crate (name "wareki-conv") (vers "0.2.0") (deps (list (crate-dep (name "chrono") (req "^0.4.34") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.20") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.10.3") (default-features #t) (kind 0)) (crate-dep (name "unicode-jp") (req "^0.4.0") (default-features #t) (kind 0)))) (hash "1g4nb7klva2gyc1p5mawm02gxi6k2n1l1m84107s3xchxw5am3xp")))

(define-public crate-wareki-conv-0.2 (crate (name "wareki-conv") (vers "0.2.1") (deps (list (crate-dep (name "chrono") (req "^0.4.34") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.20") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.10.3") (default-features #t) (kind 0)) (crate-dep (name "unicode-jp") (req "^0.4.0") (default-features #t) (kind 0)))) (hash "19anv8p5clk4pzc23r10jz8w2fhr4w40hq5dnqcz471z6j259lhf")))

