(define-module (crates-io wa ka) #:use-module (crates-io))

(define-public crate-waka-0.1 (crate (name "waka") (vers "0.1.0") (deps (list (crate-dep (name "base64") (req "^0.21.2") (default-features #t) (kind 0)) (crate-dep (name "dotenvy") (req "^0.15.7") (default-features #t) (kind 2)) (crate-dep (name "query-string-builder") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.11.18") (features (quote ("json"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.167") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.100") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.29.1") (features (quote ("full"))) (default-features #t) (kind 2)))) (hash "0b5jkk22xd3l8dyn1idzq7zi6150vlkwcbi8mzd235hypbl94z4d")))

(define-public crate-wakatime-lsp-0.0.0 (crate (name "wakatime-lsp") (vers "0.0.0") (hash "06sjrmc9fv9mid6v2w3h7ddxml6zx1w33zmcpyxk5lpk9ks7545p")))

