(define-module (crates-io wa ct) #:use-module (crates-io))

(define-public crate-wactor-0.1 (crate (name "wactor") (vers "0.1.0") (deps (list (crate-dep (name "lunatic") (req "^0.3.10") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.124") (default-features #t) (kind 0)))) (hash "1n4adkkyfi6jzrrvj4imzrjriy7646h5y6vbbnhbqzcn1qiswb70")))

(define-public crate-wactor-0.1 (crate (name "wactor") (vers "0.1.1") (deps (list (crate-dep (name "lunatic") (req "^0.3.10") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.124") (default-features #t) (kind 0)))) (hash "10qq4kic7zk5421v8iwil6n26bsnx62i5hw400gk51bb4z8pc8fw")))

