(define-module (crates-io wa vv) #:use-module (crates-io))

(define-public crate-wavv-0.1 (crate (name "wavv") (vers "0.1.0") (hash "0d8p90s08g00fy414nh3majm1lmrnf2awrbmy1dk8ga9542a3qbb")))

(define-public crate-wavv-0.1 (crate (name "wavv") (vers "0.1.1") (hash "1hgipsfxkbws1dcrraa8b63hi8s34h5p3gh30kkq6xcsgfismx02")))

(define-public crate-wavv-0.1 (crate (name "wavv") (vers "0.1.2") (hash "1s2an5lnhd5ywhn1iamm6ay8b0j7ix2x0qzp2i765ys205z9n606")))

(define-public crate-wavv-0.1 (crate (name "wavv") (vers "0.1.3") (hash "1mwggqkvcpf3riwj2xfrmxrw6xp23vgpsdyjrarc5mv0217yyqk5")))

(define-public crate-wavv-0.1 (crate (name "wavv") (vers "0.1.4") (hash "16gqli6nlfskqmnyp95crl7lhpgn22qm4rarldyzlc61kr8v99lp")))

