(define-module (crates-io wa vm) #:use-module (crates-io))

(define-public crate-wavm-0.1 (crate (name "wavm") (vers "0.1.0") (deps (list (crate-dep (name "anyhow") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "env_logger") (req "^0.7") (default-features #t) (kind 2)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "wavm-sys") (req "^0.1") (default-features #t) (kind 0)))) (hash "1y2b3wi0glh94bqkfyxgklgpl2ydwp8878arn4khjmigzzajxa2d")))

(define-public crate-wavm-cli-0.1 (crate (name "wavm-cli") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^2.33.3") (default-features #t) (kind 0)))) (hash "07wjnq95njbqr5vn1l8932padd5mk16jvqcm74k36yba9523abng")))

(define-public crate-wavm-sys-0.1 (crate (name "wavm-sys") (vers "0.1.0") (deps (list (crate-dep (name "bindgen") (req "^0.51") (default-features #t) (kind 1)) (crate-dep (name "cmake") (req "^0.1") (default-features #t) (kind 1)) (crate-dep (name "llvm-sys") (req "^80") (default-features #t) (kind 0)))) (hash "0xqh5qzc6xvq1jhlz5ia0xrnin0m52h0n5g6kbd9zwyl7axrgqs0")))

