(define-module (crates-io wa in) #:use-module (crates-io))

(define-public crate-wain-0.0.0 (crate (name "wain") (vers "0.0.0") (hash "05sj7ibjjylv38w0wfzd0968fcadygqnikxwhz5bbr0257r45a56")))

(define-public crate-wain-0.1 (crate (name "wain") (vers "0.1.0") (deps (list (crate-dep (name "wain-ast") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "wain-exec") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "wain-syntax-binary") (req "^0.1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "wain-syntax-text") (req "^0.1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "wain-validate") (req "^0.1") (default-features #t) (kind 0)))) (hash "0j2zrdbqwb9b6lcw9f9nj2rzkxc5y9xf5swbsk4cssjiagpxbyz8") (features (quote (("text" "wain-syntax-text") ("default" "binary" "text") ("binary" "wain-syntax-binary"))))))

(define-public crate-wain-0.1 (crate (name "wain") (vers "0.1.1") (deps (list (crate-dep (name "wain-ast") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "wain-exec") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "wain-syntax-binary") (req "^0.1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "wain-syntax-text") (req "^0.1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "wain-validate") (req "^0.1") (default-features #t) (kind 0)))) (hash "1vjsyg268g8zrqa5p5baw15qr6rfym7nng66nv65fzbvhnalx33v") (features (quote (("text" "wain-syntax-text") ("default" "binary" "text") ("binary" "wain-syntax-binary"))))))

(define-public crate-wain-0.1 (crate (name "wain") (vers "0.1.2") (deps (list (crate-dep (name "wain-ast") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "wain-exec") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "wain-syntax-binary") (req "^0.1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "wain-syntax-text") (req "^0.1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "wain-validate") (req "^0.1") (default-features #t) (kind 0)))) (hash "1hq92042jd4pmzgjzhxprx0592mak50kgfhsjr5q8h9m9sqc602i") (features (quote (("text" "wain-syntax-text") ("default" "binary" "text") ("binary" "wain-syntax-binary"))))))

(define-public crate-wain-0.1 (crate (name "wain") (vers "0.1.3") (deps (list (crate-dep (name "wain-ast") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "wain-exec") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "wain-syntax-binary") (req "^0.1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "wain-syntax-text") (req "^0.1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "wain-validate") (req "^0.1") (default-features #t) (kind 0)))) (hash "12g913p9p4mz92lky3rxw10k43swi4wisnga54zing8w9p89ykj3") (features (quote (("text" "wain-syntax-text") ("default" "binary" "text") ("binary" "wain-syntax-binary"))))))

(define-public crate-wain-0.1 (crate (name "wain") (vers "0.1.4") (deps (list (crate-dep (name "wain-ast") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "wain-exec") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "wain-syntax-binary") (req "^0.1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "wain-syntax-text") (req "^0.1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "wain-validate") (req "^0.1") (default-features #t) (kind 0)))) (hash "1055df2lmqcsykk5kxf4j22j8zr59jablwplwnbgp72jvgzf0fvl") (features (quote (("text" "wain-syntax-text") ("default" "binary" "text") ("binary" "wain-syntax-binary"))))))

(define-public crate-wain-0.1 (crate (name "wain") (vers "0.1.5") (deps (list (crate-dep (name "wain-ast") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "wain-exec") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "wain-syntax-binary") (req "^0.1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "wain-syntax-text") (req "^0.1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "wain-validate") (req "^0.1") (default-features #t) (kind 0)))) (hash "131d3fkx3s5s1gahxyiqcpbz4ym8g58zvcwy2r3b4ralfp3fzgji") (features (quote (("text" "wain-syntax-text") ("default" "binary" "text") ("binary" "wain-syntax-binary"))))))

(define-public crate-wain-0.1 (crate (name "wain") (vers "0.1.6") (deps (list (crate-dep (name "cargo-husky") (req "^1") (features (quote ("run-cargo-clippy" "run-cargo-fmt"))) (default-features #t) (kind 2)) (crate-dep (name "wain-ast") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "wain-exec") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "wain-syntax-binary") (req "^0.1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "wain-syntax-text") (req "^0.2") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "wain-validate") (req "^0.1") (default-features #t) (kind 0)))) (hash "19m5w3rnkfdjpysjgf65p0v1wp1cc7i27hjqy9206ghbddpr350n") (features (quote (("text" "wain-syntax-text") ("default" "binary" "text") ("binary" "wain-syntax-binary"))))))

(define-public crate-wain-0.1 (crate (name "wain") (vers "0.1.7") (deps (list (crate-dep (name "cargo-husky") (req "^1") (features (quote ("run-cargo-clippy" "run-cargo-fmt"))) (default-features #t) (kind 2)) (crate-dep (name "wain-ast") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "wain-exec") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "wain-syntax-binary") (req "^0.1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "wain-syntax-text") (req "^0.2") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "wain-validate") (req "^0.1") (default-features #t) (kind 0)))) (hash "08g1df1sipxvjy0000rmy8sg5kzyl3xhwqmvw87dymx8yhdhk666") (features (quote (("text" "wain-syntax-text") ("default" "binary" "text") ("binary" "wain-syntax-binary"))))))

(define-public crate-wain-ast-0.0.0 (crate (name "wain-ast") (vers "0.0.0") (hash "1z75kp4pqicp2b95szy1z8dlj1f5f9x7b8886r233s1g934258hs")))

(define-public crate-wain-ast-0.1 (crate (name "wain-ast") (vers "0.1.0") (hash "1m6m6ap34m637bsrh40kb8zh3wks3ph67zf6a23gzfnb82gn76fa")))

(define-public crate-wain-ast-0.1 (crate (name "wain-ast") (vers "0.1.1") (hash "1l205vd03188lbzq82xzvw5wmsfxh95zp84fz2qb99k8mf8rayly")))

(define-public crate-wain-ast-0.1 (crate (name "wain-ast") (vers "0.1.2") (hash "01qlc2rqhnamjcaqw73jkzlkz0vkimjbxwxga1i7bd2l85cdi5hr")))

(define-public crate-wain-ast-0.1 (crate (name "wain-ast") (vers "0.1.3") (hash "1ml35pii726qzbrzcjwv6gmrxl6m8p05ki5kp3inakgqh08aw7g2")))

(define-public crate-wain-ast-0.2 (crate (name "wain-ast") (vers "0.2.0") (hash "113fnza73mb5zkswb96q17k0fy6scm4j8zj1h159bp1fj6kj8a7p")))

(define-public crate-wain-ast-0.2 (crate (name "wain-ast") (vers "0.2.1") (hash "0k3pyf9dqzszpiskrla9lq1vr6fisbm50myv8g1v8n0gi849gh99")))

(define-public crate-wain-ast-0.2 (crate (name "wain-ast") (vers "0.2.2") (hash "0mv2n1qnq81nx7h6d29xrhq3r0c5bgbmzgwbmkajax3wpr5y53z6")))

(define-public crate-wain-exec-0.1 (crate (name "wain-exec") (vers "0.1.0") (deps (list (crate-dep (name "wain-ast") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "wain-syntax-text") (req "^0.1") (default-features #t) (kind 2)) (crate-dep (name "wain-validate") (req "^0.1") (default-features #t) (kind 2)))) (hash "09744n6n6j18r5vj4czsaa8sf8i2y3p6ql9zrflzpjfbmbfp5dqw")))

(define-public crate-wain-exec-0.1 (crate (name "wain-exec") (vers "0.1.1") (deps (list (crate-dep (name "wain-ast") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "wain-syntax-text") (req "^0.1") (default-features #t) (kind 2)) (crate-dep (name "wain-validate") (req "^0.1") (default-features #t) (kind 2)))) (hash "0vi8bkz7vll0pslp4h2rn5alxgdxs3f5lzdd529gcyf3cvll0a0m")))

(define-public crate-wain-exec-0.1 (crate (name "wain-exec") (vers "0.1.2") (deps (list (crate-dep (name "wain-ast") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "wain-syntax-text") (req "^0.1") (default-features #t) (kind 2)) (crate-dep (name "wain-validate") (req "^0.1") (default-features #t) (kind 2)))) (hash "055xfmi1hy6325c1vpz46gb8ybjs6y9cayasbm9j0j0ajkc45dnz")))

(define-public crate-wain-exec-0.2 (crate (name "wain-exec") (vers "0.2.0") (deps (list (crate-dep (name "wain-ast") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "wain-syntax-text") (req "^0.1") (default-features #t) (kind 2)) (crate-dep (name "wain-validate") (req "^0.1") (default-features #t) (kind 2)))) (hash "1cm5nzz1ci47ig3hvf9aq8w8275g4ly5vm07sg79rmfdbj5kns64")))

(define-public crate-wain-exec-0.2 (crate (name "wain-exec") (vers "0.2.1") (deps (list (crate-dep (name "wain-ast") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "wain-syntax-text") (req "^0.1") (default-features #t) (kind 2)) (crate-dep (name "wain-validate") (req "^0.1") (default-features #t) (kind 2)))) (hash "1r07q3p7nyfvqmva6jk80lypwpazx526klr7p17swnhr482gc8xi")))

(define-public crate-wain-exec-0.2 (crate (name "wain-exec") (vers "0.2.2") (deps (list (crate-dep (name "wain-ast") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "wain-syntax-text") (req "^0.1") (default-features #t) (kind 2)) (crate-dep (name "wain-validate") (req "^0.1") (default-features #t) (kind 2)))) (hash "0k23vz5h3axgfz4x5ngxs18ifbcir6r7vbp9gz2nmns8ccmxgyln")))

(define-public crate-wain-exec-0.3 (crate (name "wain-exec") (vers "0.3.0") (deps (list (crate-dep (name "wain-ast") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "wain-syntax-text") (req "^0.2") (default-features #t) (kind 2)) (crate-dep (name "wain-validate") (req "^0.1") (default-features #t) (kind 2)))) (hash "0c6dm92g5vljspdjs5ly4a34rfcr9x6ibd6fmbbbr953x74a9c6s")))

(define-public crate-wain-exec-0.3 (crate (name "wain-exec") (vers "0.3.1") (deps (list (crate-dep (name "wain-ast") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "wain-syntax-text") (req "^0.2") (default-features #t) (kind 2)) (crate-dep (name "wain-validate") (req "^0.1") (default-features #t) (kind 2)))) (hash "1916382s7xbry3xqzinzsai8rnkfyibw1zdxh7p6dhz4y29z2l36")))

(define-public crate-wain-syntax-binary-0.1 (crate (name "wain-syntax-binary") (vers "0.1.0") (deps (list (crate-dep (name "wain-ast") (req "^0.1") (default-features #t) (kind 0)))) (hash "12na4wx2iy7jh9rqkm2xsy4fx2gycprh2d7421scrlaky34by6im")))

(define-public crate-wain-syntax-binary-0.1 (crate (name "wain-syntax-binary") (vers "0.1.1") (deps (list (crate-dep (name "wain-ast") (req "^0.1") (default-features #t) (kind 0)))) (hash "04qc7kf6jqxq26999n9gpdfcsh1j8qf781rxbzsi3384gk5n7ggn")))

(define-public crate-wain-syntax-binary-0.1 (crate (name "wain-syntax-binary") (vers "0.1.2") (deps (list (crate-dep (name "wain-ast") (req "^0.1") (default-features #t) (kind 0)))) (hash "08mfcsml1rv90y612bsl88swrfkbs3dxf7qq9iznzrza309hgml5")))

(define-public crate-wain-syntax-binary-0.1 (crate (name "wain-syntax-binary") (vers "0.1.3") (deps (list (crate-dep (name "wain-ast") (req "^0.2") (default-features #t) (kind 0)))) (hash "1zb69aaxbyrgfp3jdz70w16s6mn12fgpkw0vblpamkvg1nzhr0a7")))

(define-public crate-wain-syntax-binary-0.1 (crate (name "wain-syntax-binary") (vers "0.1.4") (deps (list (crate-dep (name "wain-ast") (req "^0.2") (default-features #t) (kind 0)))) (hash "1z2z7q3fj1qjk5hma608sall39irv30flynp2xq52rlbax0wmyjn")))

(define-public crate-wain-syntax-binary-0.1 (crate (name "wain-syntax-binary") (vers "0.1.5") (deps (list (crate-dep (name "wain-ast") (req "^0.2") (default-features #t) (kind 0)))) (hash "1qc015b3nrf3nyxk8i87ygkpld602w6n6y4x0y0fx1974xjppyrq")))

(define-public crate-wain-syntax-text-0.1 (crate (name "wain-syntax-text") (vers "0.1.0") (deps (list (crate-dep (name "wain-ast") (req "^0.1") (default-features #t) (kind 0)))) (hash "0ziinay6qrf1ya5c9aqrwmnr7a56r9hhzgvh6vhasrna4x14madb")))

(define-public crate-wain-syntax-text-0.1 (crate (name "wain-syntax-text") (vers "0.1.1") (deps (list (crate-dep (name "wain-ast") (req "^0.1") (default-features #t) (kind 0)))) (hash "1lya3l30lc6y8vf5418n04ag7nwi9z91amly1rdc91q0sg9yaww5")))

(define-public crate-wain-syntax-text-0.1 (crate (name "wain-syntax-text") (vers "0.1.2") (deps (list (crate-dep (name "wain-ast") (req "^0.1") (default-features #t) (kind 0)))) (hash "04hwz7yxzybrlm88h268ipznhz29ikqlwla9373wzpk4dn36w951")))

(define-public crate-wain-syntax-text-0.2 (crate (name "wain-syntax-text") (vers "0.2.0") (deps (list (crate-dep (name "wain-ast") (req "^0.2") (default-features #t) (kind 0)))) (hash "0mm41227f065krrrw2b9ll047m89q3ckmgcar3vgh7grv9y0hy7k")))

(define-public crate-wain-syntax-text-0.2 (crate (name "wain-syntax-text") (vers "0.2.1") (deps (list (crate-dep (name "wain-ast") (req "^0.2") (default-features #t) (kind 0)))) (hash "06l8ls7mxlvdh4grkcgamih0i7qa05ljiryzh59bff35bvi7g4k9")))

(define-public crate-wain-validate-0.1 (crate (name "wain-validate") (vers "0.1.0") (deps (list (crate-dep (name "wain-ast") (req "^0.1") (default-features #t) (kind 0)))) (hash "03drb6r4ha97f3rfmm2v166zb1z16vbwznbkxbg5b874jixh5rwy")))

(define-public crate-wain-validate-0.1 (crate (name "wain-validate") (vers "0.1.1") (deps (list (crate-dep (name "wain-ast") (req "^0.1") (default-features #t) (kind 0)))) (hash "0rhxql7vrg13riy72aqmkcvl8iqb2069jddkilv8ha2wn5wn8rcl")))

(define-public crate-wain-validate-0.1 (crate (name "wain-validate") (vers "0.1.2") (deps (list (crate-dep (name "wain-ast") (req "^0.1") (default-features #t) (kind 0)))) (hash "0j43fwflxldg52bz9qr3m47lckclbkv4pwi0kljw9agkzxxb1d1q")))

(define-public crate-wain-validate-0.1 (crate (name "wain-validate") (vers "0.1.3") (deps (list (crate-dep (name "wain-ast") (req "^0.1") (default-features #t) (kind 0)))) (hash "0jsldadlfjk5ll9hrlvn0qkrbs8cv4193qdwf0r00hnkvgf1vlaz")))

(define-public crate-wain-validate-0.1 (crate (name "wain-validate") (vers "0.1.4") (deps (list (crate-dep (name "wain-ast") (req "^0.2") (default-features #t) (kind 0)))) (hash "1aza79a6g1hcnip0ijqz1dnia16nw7q8zvny6pcn1ks5mdhlf9c7")))

(define-public crate-wain-validate-0.1 (crate (name "wain-validate") (vers "0.1.5") (deps (list (crate-dep (name "wain-ast") (req "^0.2") (default-features #t) (kind 0)))) (hash "0f3lp5x2p64763czr704wr0yfr6x0gd78w3skyckx94wqlncrbqn")))

