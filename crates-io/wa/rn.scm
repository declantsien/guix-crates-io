(define-module (crates-io wa rn) #:use-module (crates-io))

(define-public crate-warn-0.1 (crate (name "warn") (vers "0.1.0") (deps (list (crate-dep (name "log") (req "^0.3.0") (default-features #t) (kind 0)))) (hash "1dyqb0r59yp16xh29czwflbpi4ibq3z4wlxdl0kgq3489n3l7ksq")))

(define-public crate-warn-0.1 (crate (name "warn") (vers "0.1.1") (deps (list (crate-dep (name "log") (req "^0.3.0") (default-features #t) (kind 0)))) (hash "1v9irifbb8lp0ab2a3jwyfyszh9r282d54i4b9xfjf485k3nj4kx")))

(define-public crate-warn-0.2 (crate (name "warn") (vers "0.2.0") (deps (list (crate-dep (name "log") (req "^0.3.0") (default-features #t) (kind 0)))) (hash "05wy0kp48qrvww9fhgphf0gsn1a3viqgz20aqqmq2cvk8s60i3fk")))

(define-public crate-warn-0.2 (crate (name "warn") (vers "0.2.1") (deps (list (crate-dep (name "log") (req "^0.3.0") (default-features #t) (kind 0)))) (hash "0qwxd98swwg382xdzbbm4py9ssx7jinkv11c16aj7908m18ss2wi")))

(define-public crate-warn-0.2 (crate (name "warn") (vers "0.2.2") (deps (list (crate-dep (name "log") (req "^0.3.0") (default-features #t) (kind 0)))) (hash "1jr2xnv9sziyiaypcs36wn10601480h7hvg9wkigvrsh503kh9jz")))

(define-public crate-warnalyzer-0.1 (crate (name "warnalyzer") (vers "0.1.0") (deps (list (crate-dep (name "chashmap") (req "^2.2") (default-features #t) (kind 0)) (crate-dep (name "intervaltree") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "pretty_env_logger") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^0.4") (features (quote ("span-locations"))) (default-features #t) (kind 0)) (crate-dep (name "rayon") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^0.15") (features (quote ("full" "extra-traits"))) (default-features #t) (kind 0)))) (hash "1v17iccrwyksl25qdzyb0kp76kxkw6lk23p38ybn2nh8wr3s4j8v")))

(define-public crate-warnalyzer-0.2 (crate (name "warnalyzer") (vers "0.2.0") (deps (list (crate-dep (name "chashmap") (req "^2.2") (default-features #t) (kind 0)) (crate-dep (name "intervaltree") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "pretty_env_logger") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1.0") (features (quote ("span-locations"))) (default-features #t) (kind 0)) (crate-dep (name "rayon") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (features (quote ("full" "extra-traits" "visit"))) (default-features #t) (kind 0)))) (hash "0ga12bv9g85wwhfaln71bhcds73frngz59mbj0y2cmvzja0d53fv")))

(define-public crate-warned-0.1 (crate (name "warned") (vers "0.1.0") (deps (list (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)))) (hash "0ng9npfv7dxyy75hh14bc7z2pva6m9ygkg97s1z7vp4g78y7ic9b")))

(define-public crate-warned-0.1 (crate (name "warned") (vers "0.1.1") (deps (list (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)))) (hash "19g3vpnjs9wp954r4jkz7i3w2rrhcwarz0fhac17rxnip805abd3")))

(define-public crate-warning-0.1 (crate (name "warning") (vers "0.1.0") (hash "0i932aaiw16xj5hj1i0fyrd5vra4k4v7a91bfq9crr15dzpnicrg")))

