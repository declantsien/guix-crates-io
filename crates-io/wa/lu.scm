(define-module (crates-io wa lu) #:use-module (crates-io))

(define-public crate-waluigi-0.1 (crate (name "waluigi") (vers "0.1.0") (hash "0aripd9bvmigmcy9b44fd41bqxw0rdbjqdrr4dzf0wd0yjr2nsfi")))

(define-public crate-walutomat-0.1 (crate (name "walutomat") (vers "0.1.0") (deps (list (crate-dep (name "hmac") (req "^0.7.0") (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.9") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "sha2") (req "^0.8.0") (default-features #t) (kind 0)) (crate-dep (name "uuid") (req "^0.7") (features (quote ("serde" "v4"))) (default-features #t) (kind 0)))) (hash "07l3yrm8rsi50knbzpp35rvvakvcch3n7998994rsd7iarg2zg3z")))

