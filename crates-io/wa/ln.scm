(define-module (crates-io wa ln) #:use-module (crates-io))

(define-public crate-walnut-0.1 (crate (name "walnut") (vers "0.1.0") (hash "07nfynfrv0ly4ci7k792n2zpnicaqxrj0mzpyjyp3g7cms2d8hyl") (yanked #t)))

(define-public crate-walnut-0.1 (crate (name "walnut") (vers "0.1.2") (hash "11pqh28wsisbjdp60j7il0q7r0d2qzxxa7wyxsyiqmhwm6mas50r") (yanked #t)))

(define-public crate-walnut-0.1 (crate (name "walnut") (vers "0.1.3") (hash "099b8arqs7cj8hvmlg419q59dr6zd0y9x9z6479z8p1k7h4g55ql") (yanked #t)))

(define-public crate-walnut-0.1 (crate (name "walnut") (vers "0.1.4") (hash "125mdcg99kp67c1mi5ny6klk3b7c8rjdjvf06sga6gjpvy4rirq5") (yanked #t)))

(define-public crate-walnut-0.1 (crate (name "walnut") (vers "0.1.5") (deps (list (crate-dep (name "anyhow") (req "^1.0.75") (default-features #t) (kind 0)) (crate-dep (name "bincode") (req "^1.3.3") (default-features #t) (kind 0)) (crate-dep (name "bitvec") (req "^1.0.1") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^4.4.8") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "crc32fast") (req "^1.2.0") (default-features #t) (kind 0)) (crate-dep (name "memmap2") (req "^0.9.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "18pky855954fb1pa7zf8mck4m8z7gp9k4x1378j3sczbv5jkqip1")))

