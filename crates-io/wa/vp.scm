(define-module (crates-io wa vp) #:use-module (crates-io))

(define-public crate-wavpack-0.1 (crate (name "wavpack") (vers "0.1.0") (deps (list (crate-dep (name "anyhow") (req "^1.0.43") (default-features #t) (kind 2)) (crate-dep (name "libc") (req "^0.2.101") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.29") (default-features #t) (kind 0)) (crate-dep (name "wavpack-sys") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "1i2lcv2h2f4lpn6qzw8szfl44hjaq4r658y8aia2f6v3lssrdmly")))

(define-public crate-wavpack-0.2 (crate (name "wavpack") (vers "0.2.0") (deps (list (crate-dep (name "anyhow") (req "^1.0.43") (default-features #t) (kind 2)) (crate-dep (name "ffi") (req "^0.1.0") (default-features #t) (kind 0) (package "wavpack-sys")) (crate-dep (name "thiserror") (req "^1.0.29") (default-features #t) (kind 0)))) (hash "0nnfdx5hq8bn3rbm3sh5kp74fbz71wa8a8izx9s290wbcn4kb61j")))

(define-public crate-wavpack-0.2 (crate (name "wavpack") (vers "0.2.1") (deps (list (crate-dep (name "anyhow") (req "^1.0.43") (default-features #t) (kind 2)) (crate-dep (name "ffi") (req "^0.1.1") (default-features #t) (kind 0) (package "wavpack-sys")) (crate-dep (name "thiserror") (req "^1.0.29") (default-features #t) (kind 0)))) (hash "1l2629p4nq7ihrwah1q015pfsp889mxdsdj8h71al5mf9pqa9w04")))

(define-public crate-wavpack-0.2 (crate (name "wavpack") (vers "0.2.2") (deps (list (crate-dep (name "anyhow") (req "^1.0.43") (default-features #t) (kind 2)) (crate-dep (name "ffi") (req "^0.1.1") (default-features #t) (kind 0) (package "wavpack-sys")) (crate-dep (name "thiserror") (req "^1.0.29") (default-features #t) (kind 0)))) (hash "0vfcyichb5hzcg06ngfmvx3l2jc5vr6d3zykbkh77k08gmrkyldb")))

(define-public crate-wavpack-0.2 (crate (name "wavpack") (vers "0.2.3") (deps (list (crate-dep (name "anyhow") (req "^1.0.43") (default-features #t) (kind 2)) (crate-dep (name "ffi") (req "^0.1.2") (default-features #t) (kind 0) (package "wavpack-sys")) (crate-dep (name "thiserror") (req "^1.0.29") (default-features #t) (kind 0)))) (hash "011nv6kv3wi6yg569zw1216k923gac2ib9qqpwwbag81y2dwg6py")))

(define-public crate-wavpack-0.2 (crate (name "wavpack") (vers "0.2.4") (deps (list (crate-dep (name "anyhow") (req "^1.0.43") (default-features #t) (kind 2)) (crate-dep (name "ffi") (req "^0.1.2") (default-features #t) (kind 0) (package "wavpack-sys")) (crate-dep (name "thiserror") (req "^1.0.29") (default-features #t) (kind 0)))) (hash "0r2k8qimal5sf6mk1p7kmc4jzhjhh2wr50lwrjpf2sbnpadl37v3")))

(define-public crate-wavpack-0.2 (crate (name "wavpack") (vers "0.2.5") (deps (list (crate-dep (name "anyhow") (req "^1.0.43") (default-features #t) (kind 2)) (crate-dep (name "ffi") (req "^0.1.3") (default-features #t) (kind 0) (package "wavpack-sys")) (crate-dep (name "thiserror") (req "^1.0.29") (default-features #t) (kind 0)))) (hash "0p81l8cvsriaxikd7gdn6cvp9ydnj9gdi5g0z204hxqd8r6whdxh")))

(define-public crate-wavpack-0.3 (crate (name "wavpack") (vers "0.3.0") (deps (list (crate-dep (name "anyhow") (req "^1.0.43") (default-features #t) (kind 2)) (crate-dep (name "ffi") (req "^0.1.4") (default-features #t) (kind 0) (package "wavpack-sys")) (crate-dep (name "thiserror") (req "^1.0.29") (default-features #t) (kind 0)))) (hash "1nx2yy7hlm450mdcvnhzc6ml892yww0bjz22xi3m14h546sqads1")))

(define-public crate-wavpack-0.4 (crate (name "wavpack") (vers "0.4.0") (deps (list (crate-dep (name "anyhow") (req "^1.0.43") (default-features #t) (kind 2)) (crate-dep (name "thiserror") (req "^1.0.29") (default-features #t) (kind 0)) (crate-dep (name "wavpack-sys") (req "^0.4.0") (default-features #t) (kind 0)))) (hash "0yhl446dqrw7p5fw78r135jhrvnrgklz7ibd5qf0lryv5w0nbp3y")))

(define-public crate-wavpack-sys-0.1 (crate (name "wavpack-sys") (vers "0.1.0") (deps (list (crate-dep (name "bindgen") (req "^0.59.1") (default-features #t) (kind 1)))) (hash "1zflxm397wn7xjhac7q9myfwqj22wsbgcmsnz9da61hzlf78f4yn")))

(define-public crate-wavpack-sys-0.1 (crate (name "wavpack-sys") (vers "0.1.1") (deps (list (crate-dep (name "bindgen") (req "^0.59.1") (default-features #t) (kind 1)) (crate-dep (name "cmake") (req "^0.1.45") (default-features #t) (kind 1)))) (hash "0zifsbam9s2z29m5rw4wrh8g40jwqgnp18p6nb527ic8lxsbiz2q")))

(define-public crate-wavpack-sys-0.1 (crate (name "wavpack-sys") (vers "0.1.2") (deps (list (crate-dep (name "bindgen") (req "^0.59.1") (default-features #t) (kind 1)) (crate-dep (name "cmake") (req "^0.1.45") (default-features #t) (kind 1)))) (hash "1f6w1av67ds9l8g6v2n432559mp22020ds94vm903h3y3rgac00k")))

(define-public crate-wavpack-sys-0.1 (crate (name "wavpack-sys") (vers "0.1.3") (deps (list (crate-dep (name "bindgen") (req "^0.59.1") (default-features #t) (kind 1)) (crate-dep (name "cmake") (req "^0.1.45") (default-features #t) (kind 1)))) (hash "1dwgam85yl2fk63f9fkhwzgrscak34c2dqfn08aplw62hckipwky")))

(define-public crate-wavpack-sys-0.1 (crate (name "wavpack-sys") (vers "0.1.4") (deps (list (crate-dep (name "bindgen") (req "^0.69.0") (default-features #t) (kind 1)) (crate-dep (name "cmake") (req "^0.1.45") (default-features #t) (kind 1)))) (hash "1l26ypgmn89mp897a4qanwfsys0px4jrjw2bnzfsdp7vd20h0fjq")))

(define-public crate-wavpack-sys-0.4 (crate (name "wavpack-sys") (vers "0.4.0") (deps (list (crate-dep (name "bindgen") (req "^0.69.0") (default-features #t) (kind 1)) (crate-dep (name "cmake") (req "^0.1.45") (default-features #t) (kind 1)))) (hash "0xwcwn8si2x81r49x95ascp44xl7ls6isff6wjg7yvkqlyz4xdpf")))

