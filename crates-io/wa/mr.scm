(define-module (crates-io wa mr) #:use-module (crates-io))

(define-public crate-wamr-sys-0.1 (crate (name "wamr-sys") (vers "0.1.0") (deps (list (crate-dep (name "bindgen") (req "^0.56") (default-features #t) (kind 1)) (crate-dep (name "cc") (req "^1.0") (default-features #t) (kind 1)) (crate-dep (name "cmake") (req "^0.1") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2.0") (default-features #t) (kind 0)))) (hash "1a7nbi8hfdx3i5v6sswdz4yvz6dm22l2m547bd505va9328mbjny") (links "iwasm")))

(define-public crate-wamr-sys-0.1 (crate (name "wamr-sys") (vers "0.1.1") (deps (list (crate-dep (name "bindgen") (req "^0.56") (default-features #t) (kind 1)) (crate-dep (name "cc") (req "^1.0") (default-features #t) (kind 1)) (crate-dep (name "cmake") (req "^0.1") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2.0") (default-features #t) (kind 0)))) (hash "1l210gcb4n17k8v3a47yk4lqlz7mrf66s3d27q05fyy8ng61k1bz") (links "iwasm")))

