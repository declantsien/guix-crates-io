(define-module (crates-io wa ng) #:use-module (crates-io))

(define-public crate-wang-0.0.1 (crate (name "wang") (vers "0.0.1") (hash "0r1ga9wsdir6xw5jw6k2cd81v6sfs79q7g93b8v31bdspwvkqyxc")))

(define-public crate-wangzy97lib-0.1 (crate (name "wangzy97lib") (vers "0.1.2") (hash "1jf9nxycadhsmn6a1h7r9ma50f7w0lrzicvwaaf1vxz895bdv3qk")))

(define-public crate-wangzy97lib-0.1 (crate (name "wangzy97lib") (vers "0.1.3") (hash "07k0wizm3qf0yi00yci2maymr4x0kp0yhq58gshhj2brk0rrsm7j")))

(define-public crate-wangzy97lib-0.1 (crate (name "wangzy97lib") (vers "0.1.4") (hash "0blv5w70q2iqmry7n3hdd8jjxpky2l5al8vdm7la3kj1q1j2rr41")))

(define-public crate-wangzy97lib-0.1 (crate (name "wangzy97lib") (vers "0.1.5") (hash "1a29nk2smplvzcyh6caf1y6bnixk80pjksd3d8vg1phc3zdxhx06")))

