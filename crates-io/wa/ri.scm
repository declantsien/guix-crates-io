(define-module (crates-io wa ri) #:use-module (crates-io))

(define-public crate-wari-0.0.1 (crate (name "wari") (vers "0.0.1") (deps (list (crate-dep (name "asm_riscv") (req "^0.1") (default-features #t) (kind 0)))) (hash "1h1rdp8g1sqq45g3zk572nbpbzvmp4c1fyvvxdyqhy92w26hwv0l")))

(define-public crate-wario-0.1 (crate (name "wario") (vers "0.1.0") (hash "149yfkavahk7yvpnql73xg1qa7z86n02mb80ka8sswy0i0dqvn8w") (yanked #t)))

(define-public crate-wario-0.1 (crate (name "wario") (vers "0.1.1") (hash "0d576bh6gaih1jlvxclb72dfh5rjcxvlxm2hjxnszw7vicmcb7ba")))

