(define-module (crates-io wa li) #:use-module (crates-io))

(define-public crate-walign-0.1 (crate (name "walign") (vers "0.1.0") (deps (list (crate-dep (name "structopt") (req "^0.3") (default-features #t) (kind 0)))) (hash "1xmxlsj8av83kdnnma17gpymfa9xs0q0ixsmspz4v464vnm1vm6r")))

(define-public crate-walign-0.1 (crate (name "walign") (vers "0.1.1") (deps (list (crate-dep (name "anyhow") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "byteorder") (req "^1.4") (default-features #t) (kind 0)) (crate-dep (name "ndarray") (req "^0.15") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.3") (default-features #t) (kind 0)))) (hash "0jw254hjm598js04516qvmi1ph7xa1zznycnzrsikck4n5m8ymbx")))

(define-public crate-walign-0.1 (crate (name "walign") (vers "0.1.2") (deps (list (crate-dep (name "anyhow") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "byteorder") (req "^1.4") (default-features #t) (kind 0)) (crate-dep (name "ndarray") (req "^0.15") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.3") (default-features #t) (kind 0)))) (hash "0pc492268n495p3avpd2w5wlq4wrvfqc9d3y3ibjydhifn8rdk99")))

(define-public crate-walign-0.1 (crate (name "walign") (vers "0.1.3") (deps (list (crate-dep (name "anyhow") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "byteorder") (req "^1.4") (default-features #t) (kind 0)) (crate-dep (name "ndarray") (req "^0.15") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.3") (default-features #t) (kind 0)))) (hash "1x9qyblly9l1qajjny1kvi39bdk46x5zir24xx7hdppwc007kcd8")))

(define-public crate-walign-0.1 (crate (name "walign") (vers "0.1.4") (deps (list (crate-dep (name "anyhow") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "byteorder") (req "^1.4") (default-features #t) (kind 0)) (crate-dep (name "ndarray") (req "^0.15") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.3") (default-features #t) (kind 0)))) (hash "1qp45ziy66yrzikn8yr2wq5yrbrazrvmd29gw97ka79fqdjmnw8z")))

(define-public crate-walign-0.1 (crate (name "walign") (vers "0.1.5") (deps (list (crate-dep (name "anyhow") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "byteorder") (req "^1.4") (default-features #t) (kind 0)) (crate-dep (name "ndarray") (req "^0.15") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.3") (default-features #t) (kind 0)))) (hash "0a084i78lvw8xky5sdk2nv627syyxmdg5s83c3d4cd2by2cmy79h")))

(define-public crate-walign-0.1 (crate (name "walign") (vers "0.1.6") (deps (list (crate-dep (name "byteorder") (req "^1.4") (default-features #t) (kind 0)) (crate-dep (name "ndarray") (req "^0.15") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.3") (default-features #t) (kind 0)))) (hash "1mffhgi4a1lbvgm46222srn4ipdffzwrib7hghwq0iv91jlx3xbk")))

