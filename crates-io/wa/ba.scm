(define-module (crates-io wa ba) #:use-module (crates-io))

(define-public crate-wabam-0.1 (crate (name "wabam") (vers "0.1.0") (hash "0m1abdmsy2rj4yiak7kj93l8q3k053amwgfl80q0kp1m35yx3h2k") (yanked #t)))

(define-public crate-wabam-0.1 (crate (name "wabam") (vers "0.1.1") (hash "10mj4sw9j9hwl8k8hpjspnyjg3jldlk3jxwj8zlxfidn6cb1g8m1") (yanked #t)))

(define-public crate-wabam-0.1 (crate (name "wabam") (vers "0.1.2") (hash "0clscl1ggsnzymzdpxp78xmx578svjc6zijirlrxzwwjaxla1a5f")))

(define-public crate-wabam-0.2 (crate (name "wabam") (vers "0.2.0") (hash "138igfnfv4mjk06kqxzykc758lj7q8pa40clp7cbb32w3kc6kvdw")))

(define-public crate-wabam-0.3 (crate (name "wabam") (vers "0.3.0") (hash "1a4j62hanrcgjia93d9l5zm2lk53r9igrldyvfikdjbldclgv9ig")))

