(define-module (crates-io wa yo) #:use-module (crates-io))

(define-public crate-wayout-1 (crate (name "wayout") (vers "1.0.0") (deps (list (crate-dep (name "clap") (req "^3.1.2") (default-features #t) (kind 0)) (crate-dep (name "smithay-client-toolkit") (req "^0.15.3") (default-features #t) (kind 0)))) (hash "1l5ii2ddvlx2nyrbjagf8f26v637x35f68vnvdpvqqmgg6f3v6wm")))

(define-public crate-wayout-1 (crate (name "wayout") (vers "1.1.2") (deps (list (crate-dep (name "clap") (req "^3.1.2") (default-features #t) (kind 0)) (crate-dep (name "wayland-client") (req "^0.29.4") (default-features #t) (kind 0)) (crate-dep (name "wayland-protocols") (req "^0.29.4") (features (quote ("unstable_protocols" "client"))) (default-features #t) (kind 0)))) (hash "0vcmmq5a075g2dc24asrx0bn1j60l5bbmid855mmpnssyh5qiizz")))

(define-public crate-wayout-1 (crate (name "wayout") (vers "1.1.3") (deps (list (crate-dep (name "clap") (req "^3.1.2") (default-features #t) (kind 0)) (crate-dep (name "wayland-client") (req "^0.29.4") (default-features #t) (kind 0)) (crate-dep (name "wayland-protocols") (req "^0.29.4") (features (quote ("unstable_protocols" "client"))) (default-features #t) (kind 0)))) (hash "10r4367xx9441mr8jx3sw96bv5mmzpjh98y8k090q5k6x2bwziz7")))

