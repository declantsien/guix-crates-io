(define-module (crates-io wa di) #:use-module (crates-io))

(define-public crate-wadi-0.0.1 (crate (name "wadi") (vers "0.0.1") (hash "1p4bnc7wwa675a00p5hrygddmbr7dxnqasj5cwxbsmcsv35w1xhg")))

(define-public crate-wadi-0.0.2 (crate (name "wadi") (vers "0.0.2") (deps (list (crate-dep (name "cstring") (req "^0.0") (default-features #t) (kind 0)))) (hash "1npzhq0ygx3dcggvqks0ayr3gfawws60i5lxkksyvpjz329j61c5")))

(define-public crate-wadi-0.0.3 (crate (name "wadi") (vers "0.0.3") (deps (list (crate-dep (name "cstring") (req "^0.0") (default-features #t) (kind 0)))) (hash "1jh6jrsh6rk4a6ljg5naxb91jbnk7jh1d34jqlqpbqfsibbwg2f0")))

(define-public crate-wadi-0.0.4 (crate (name "wadi") (vers "0.0.4") (deps (list (crate-dep (name "cstring") (req "^0.0") (default-features #t) (kind 0)))) (hash "1dvagcs5g0qj61kn1zs8k6a4008gcvcpg1v2pcxnkwy07d6dpjn3")))

