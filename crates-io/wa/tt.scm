(define-module (crates-io wa tt) #:use-module (crates-io))

(define-public crate-watt-0.0.0 (crate (name "watt") (vers "0.0.0") (hash "13lwzmdga5v280gmjyhh7nghnv94d5lgm9p3hjs75pr47caf35a1") (yanked #t)))

(define-public crate-watt-0.1 (crate (name "watt") (vers "0.1.0") (hash "02cab5drzvg0v25ksxagk3717h6psld7bqdryn6lj5jpdl34swp0")))

(define-public crate-watt-0.1 (crate (name "watt") (vers "0.1.1") (hash "1p29c8c7p4a1ywcx53s49n3f9h0vb2w50s2vwwmakmyzkacf188j")))

(define-public crate-watt-0.1 (crate (name "watt") (vers "0.1.2") (hash "00fh56zh69l8dk99z0y10v9q54n6b68yvn569kih988v5f45daca")))

(define-public crate-watt-0.1 (crate (name "watt") (vers "0.1.3") (hash "1zysinjrlqbbsh48fvszqqqn90n6lc3mafz3099x9kak91zkwnw8")))

(define-public crate-watt-0.2 (crate (name "watt") (vers "0.2.0") (hash "1p5hqjdzswsfmaz73x042ad82ka6dns2sw2q2vvva1nghyjp7yji")))

(define-public crate-watt-0.2 (crate (name "watt") (vers "0.2.1") (hash "0m553x759qq36hwkvpc07blf4jkh7fydfqzjn873n86k7xr7plwf")))

(define-public crate-watt-0.3 (crate (name "watt") (vers "0.3.0") (hash "1mr5p1rslpa9xwd4jv8yqzs8dz31ihipwyqdqc7lqqfhmhycfgxa")))

(define-public crate-watt-0.4 (crate (name "watt") (vers "0.4.0") (hash "0j7px01lh6aqyfrv6lglkp42x8bh4k4klva95xpq0k8rpracf9nn")))

(define-public crate-watt-0.4 (crate (name "watt") (vers "0.4.1") (hash "1kvv64kfzjhnlqgc0n6llh16r1281yisnz9cs03g9smkf3ry6vxy")))

(define-public crate-watt-0.4 (crate (name "watt") (vers "0.4.2") (hash "1q2zk6nhwqas7lj2iyxfczyfcgpnj5nfvrm3dryx17l94dy2kz3d")))

(define-public crate-watt-0.4 (crate (name "watt") (vers "0.4.3") (hash "1lj80ybnbw3sj9j3jca9snfn7pl6i4a2bl84zp1skvaa2ay0j7w0")))

(define-public crate-watt-0.4 (crate (name "watt") (vers "0.4.4") (hash "1ikw9lx19fh21qr0v8anpz3kczavz0b684k9bsa7015r8v0vll6s") (yanked #t)))

(define-public crate-watt-0.3 (crate (name "watt") (vers "0.3.1") (hash "1jyshr9qcbbr0b1vbvgw61ssz4nyxsdbxwf9rqjc02kqyzyg3z7d")))

(define-public crate-watt-0.4 (crate (name "watt") (vers "0.4.5") (hash "04v2khmyfhsbzawann4n8q8cggrz8hf8kxz7gqhnaaj2b47n81lp") (yanked #t)))

(define-public crate-watt-0.4 (crate (name "watt") (vers "0.4.6") (hash "1k4d2pskqqgyafzax5ly1pqfvaffq2rrd0xp8m1dlr0848dhpzhs")))

(define-public crate-watt-0.5 (crate (name "watt") (vers "0.5.0") (hash "12djvzyjbdw07yszyb69376x8dysfdq7prg85lkx6554pla27gxk")))

(define-public crate-watt-abi-0.0.0 (crate (name "watt-abi") (vers "0.0.0") (hash "0f82rrlinbcn5kgnpnfxs7adw92anhj9gk8d56lhhzq0p5j6wszz") (yanked #t)))

(define-public crate-watt-non-static-runtime-0.4 (crate (name "watt-non-static-runtime") (vers "0.4.0") (hash "1qfnqmi5fn5fkmvdp81nghfy6166jcsva3m6mlfks5gmzjixicig")))

(define-public crate-watt-runtime-0.0.0 (crate (name "watt-runtime") (vers "0.0.0") (hash "0dc2lahjvbnl3z3xj3bh4ikyxl2lajq0l01rxpy8j1kqzffkd5g2") (yanked #t)))

(define-public crate-wattbar-0.1 (crate (name "wattbar") (vers "0.1.0") (deps (list (crate-dep (name "anyhow") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "calloop") (req "^0.9.3") (default-features #t) (kind 0)) (crate-dep (name "palette") (req "^0.6.0") (default-features #t) (kind 0)) (crate-dep (name "smithay-client-toolkit") (req "^0.15") (features (quote ("calloop"))) (kind 0)) (crate-dep (name "upower_dbus") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "wayland-client") (req "^0.29") (default-features #t) (kind 0)) (crate-dep (name "wayland-protocols") (req "^0.29") (default-features #t) (kind 0)) (crate-dep (name "zbus") (req "^2.2.0") (default-features #t) (kind 0)))) (hash "1cz473rkr0w2m96ijdd622ccv33bwdy5p1as4rdqinvkqm8p9j4x")))

(define-public crate-wattle-0.0.1 (crate (name "wattle") (vers "0.0.1") (deps (list (crate-dep (name "url") (req "^2.4.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "warp") (req "^0.3.5") (optional #t) (default-features #t) (kind 0)))) (hash "1pnaci4nx6l3wfxd34c9q6iga3c1a9bs7sbgish1clnp8x4jb4p8") (features (quote (("web_warp" "warp" "url")))) (yanked #t)))

(define-public crate-wattle-0.0.2 (crate (name "wattle") (vers "0.0.2") (deps (list (crate-dep (name "calamine") (req "^0.21.2") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "mysql") (req "^24.0.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "url") (req "^2.4.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "warp") (req "^0.3.5") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "xlsxwriter") (req "^0.6.0") (optional #t) (default-features #t) (kind 0)))) (hash "06dxzsl0hyjbsns2296nwcc5wjyw4mkwv7pjv7ahlfdm0144bavb") (features (quote (("web_warp" "warp" "url") ("sql" "mysql") ("excel" "xlsxwriter" "calamine")))) (yanked #t)))

(define-public crate-wattle-0.0.4 (crate (name "wattle") (vers "0.0.4") (deps (list (crate-dep (name "mysql") (req "^24.0.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "url") (req "^2.4.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "warp") (req "^0.3.5") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "wattle-field") (req "^0.1.3") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "wattle-proc") (req "^0.1.3") (optional #t) (default-features #t) (kind 0)))) (hash "0g0zywzps58mj2s26rpajqpmkr4dfnhkl0y8xgjnp35z7770xaad") (features (quote (("web_warp" "warp" "url") ("sql" "mysql" "wattle-proc" "wattle-field")))) (yanked #t)))

(define-public crate-wattle-0.0.5 (crate (name "wattle") (vers "0.0.5") (deps (list (crate-dep (name "mysql") (req "^24.0.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "url") (req "^2.4.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "warp") (req "^0.3.5") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "wattle-proc") (req "^0.1.6") (optional #t) (default-features #t) (kind 0)))) (hash "0bmxnmjc5mdps43s1vl8ghc06jv2gzf8j1ds146zhfwzy5bf484c") (features (quote (("web_warp" "warp" "url") ("sql" "mysql" "wattle-proc")))) (yanked #t)))

(define-public crate-wattle-0.0.7 (crate (name "wattle") (vers "0.0.7") (deps (list (crate-dep (name "mysql") (req "^24.0.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "url") (req "^2.4.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "warp") (req "^0.3.5") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "wattle-proc") (req "^0.1.6") (optional #t) (default-features #t) (kind 0)))) (hash "10s8jwzc09wwiw7lpq2yr3a9n5y2yg4lsydf9dgmqpf4ggf1ndf2") (features (quote (("web_warp" "warp" "url") ("sql" "mysql" "wattle-proc")))) (yanked #t)))

(define-public crate-wattle-0.0.8 (crate (name "wattle") (vers "0.0.8") (deps (list (crate-dep (name "mysql") (req "^24.0.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "url") (req "^2.4.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "warp") (req "^0.3.5") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "wattle-proc") (req "^0.1.6") (optional #t) (default-features #t) (kind 0)))) (hash "0ar0gxfqliixgg00q96nz9l5l6581hhmbx67qydj6xirl4afqcyj") (features (quote (("web_warp" "warp" "url") ("sql" "mysql" "wattle-proc")))) (yanked #t)))

(define-public crate-wattle-0.0.9 (crate (name "wattle") (vers "0.0.9") (deps (list (crate-dep (name "mysql") (req "^24.0.0") (default-features #t) (kind 0)) (crate-dep (name "mysql_common") (req "^0.30.6") (default-features #t) (kind 0)))) (hash "1gj3wr5w2jka613cczp6n43zi25jwm690r5vsdrmma14i4qaia2i")))

(define-public crate-wattle-field-0.1 (crate (name "wattle-field") (vers "0.1.0") (hash "04wgq9r6bhbp9gld7qabcf47350n53nji0wni9il48ph87cpsc8w") (yanked #t)))

(define-public crate-wattle-field-0.1 (crate (name "wattle-field") (vers "0.1.1") (hash "0pqrl1x90a5dw26hs01cxyp30s7gxw1l46hkiqnqxnm5ahh20g9i") (yanked #t)))

(define-public crate-wattle-field-0.1 (crate (name "wattle-field") (vers "0.1.2") (hash "13kl38xydy1315mwdby5p0h93ki9p3xksfd9x30w2v8d50cpp1f5") (yanked #t)))

(define-public crate-wattle-field-0.1 (crate (name "wattle-field") (vers "0.1.3") (hash "1xbwh7rwv9yd0kr3wqv9fgjfkh7rb5agnlhbrgrdn8dkksl8jv2m") (yanked #t)))

(define-public crate-wattle-proc-0.1 (crate (name "wattle-proc") (vers "0.1.0") (deps (list (crate-dep (name "proc-macro2") (req "^1.0.66") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.32") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2.0.28") (default-features #t) (kind 0)) (crate-dep (name "wattle-field") (req "^0.1.1") (default-features #t) (kind 0)))) (hash "0y2d6b6i2095nqs0slhrp8kwczdacjpwg7s044lc7mkahbchb9xy")))

(define-public crate-wattle-proc-0.1 (crate (name "wattle-proc") (vers "0.1.1") (deps (list (crate-dep (name "proc-macro2") (req "^1.0.66") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.32") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2.0.28") (default-features #t) (kind 0)) (crate-dep (name "wattle-field") (req "^0.1.2") (default-features #t) (kind 0)))) (hash "195j09w0hx64lpwcdbhnrb14vyivjla9nsd592fkinszjph026kz")))

(define-public crate-wattle-proc-0.1 (crate (name "wattle-proc") (vers "0.1.2") (deps (list (crate-dep (name "proc-macro2") (req "^1.0.66") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.32") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2.0.28") (default-features #t) (kind 0)) (crate-dep (name "wattle-field") (req "^0.1.3") (default-features #t) (kind 0)))) (hash "19hmg3jr5c95q3b3gh03jm233v0pvgy5a6kqwnn5h1avh8z3bcmw") (yanked #t)))

(define-public crate-wattle-proc-0.1 (crate (name "wattle-proc") (vers "0.1.3") (deps (list (crate-dep (name "proc-macro2") (req "^1.0.66") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.32") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2.0.28") (default-features #t) (kind 0)) (crate-dep (name "wattle-field") (req "^0.1.3") (default-features #t) (kind 0)))) (hash "071hfdrgm0k3mz1mwkwzn4n5a0qyl0sda6x14510jazfga19yr8c")))

(define-public crate-wattle-proc-0.1 (crate (name "wattle-proc") (vers "0.1.4") (deps (list (crate-dep (name "proc-macro2") (req "^1.0.66") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.32") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2.0.28") (default-features #t) (kind 0)))) (hash "1580hwzgh384j9psrz7f7pjb1bvqfrs8nmnaryxlv1zclk5amaij")))

(define-public crate-wattle-proc-0.1 (crate (name "wattle-proc") (vers "0.1.5") (deps (list (crate-dep (name "proc-macro2") (req "^1.0.66") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.32") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2.0.28") (default-features #t) (kind 0)))) (hash "1k9p3pdh7ds069ysw4hr3d0k1am6f7kcnrdhb18lv00rzn34v5q9")))

(define-public crate-wattle-proc-0.1 (crate (name "wattle-proc") (vers "0.1.6") (deps (list (crate-dep (name "proc-macro2") (req "^1.0.66") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.32") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2.0.28") (default-features #t) (kind 0)))) (hash "1vbx23h7c03364d4v561kx97ypvvf91abwj844pjp7zykn7qrgk1")))

(define-public crate-watto-0.1 (crate (name "watto") (vers "0.1.0") (deps (list (crate-dep (name "leb128") (req "^0.2.5") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.33") (optional #t) (default-features #t) (kind 0)))) (hash "0awlpq4khplpxjlkyrdz14nwji8d4s1bnzh458l48wa1bqqvaik7") (features (quote (("writer" "std") ("strings" "std" "leb128" "thiserror") ("std"))))))

(define-public crate-wattpad-0.1 (crate (name "wattpad") (vers "0.1.0") (deps (list (crate-dep (name "anyhow") (req "^1.0.69") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.7.1") (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.11.14") (features (quote ("json" "cookies"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.152") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.93") (default-features #t) (kind 0)) (crate-dep (name "serde_repr") (req "^0.1.10") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.25.0") (features (quote ("full"))) (default-features #t) (kind 2)))) (hash "0il70gygglx80aqw5n0klbvdm6x4ijm9gyhzjz1nl93hwpjwyi65")))

(define-public crate-wattpad-0.1 (crate (name "wattpad") (vers "0.1.1") (deps (list (crate-dep (name "anyhow") (req "^1.0.69") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.7.1") (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.11.14") (features (quote ("json" "cookies"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.152") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.93") (default-features #t) (kind 0)) (crate-dep (name "serde_repr") (req "^0.1.10") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.25.0") (features (quote ("full"))) (default-features #t) (kind 2)))) (hash "0xrncj4malwpzcis3qnnkkjxp9fj8n1frjmqr4lh0y5xlzyrp331")))

(define-public crate-wattpad-0.1 (crate (name "wattpad") (vers "0.1.2") (deps (list (crate-dep (name "anyhow") (req "^1.0.69") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.7.1") (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.11.14") (features (quote ("json" "cookies" "gzip"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.152") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.93") (default-features #t) (kind 0)) (crate-dep (name "serde_repr") (req "^0.1.10") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.25.0") (features (quote ("full"))) (default-features #t) (kind 2)))) (hash "1kxmjh9gf0jfvi4580lxris501lg6ah2020qnjqdi5w6kjnyfp15")))

(define-public crate-wattpad-0.2 (crate (name "wattpad") (vers "0.2.0") (deps (list (crate-dep (name "anyhow") (req "^1.0.69") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.7.1") (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.11.14") (features (quote ("json" "cookies" "gzip"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.152") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.93") (default-features #t) (kind 0)) (crate-dep (name "serde_repr") (req "^0.1.10") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.25.0") (features (quote ("full"))) (default-features #t) (kind 2)))) (hash "0n8fynq32aixm6sr2nayma285377xbsnxbvzqm402947wyh4syc2")))

(define-public crate-wattpad-0.2 (crate (name "wattpad") (vers "0.2.1") (deps (list (crate-dep (name "anyhow") (req "^1.0.69") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.7.1") (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.11.14") (features (quote ("json" "cookies" "gzip"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.152") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.93") (default-features #t) (kind 0)) (crate-dep (name "serde_repr") (req "^0.1.10") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.25.0") (features (quote ("full"))) (default-features #t) (kind 2)))) (hash "16q7bqfxnn4xwlnaxbc8cicai3844w1i169khxb80mwvlsy7rbgd")))

(define-public crate-wattpad-0.2 (crate (name "wattpad") (vers "0.2.2") (deps (list (crate-dep (name "anyhow") (req "^1.0.69") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.7.1") (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.11.14") (features (quote ("json" "cookies" "gzip"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.152") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.93") (default-features #t) (kind 0)) (crate-dep (name "serde_repr") (req "^0.1.10") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.25.0") (features (quote ("full"))) (default-features #t) (kind 2)))) (hash "0j2zad0s2q4wjwmcpcd6zy3cidmy3k9wk3rvgkv887qppdrh7d6k")))

(define-public crate-wattpad-0.2 (crate (name "wattpad") (vers "0.2.3") (deps (list (crate-dep (name "anyhow") (req "^1.0.69") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.7.1") (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.11.14") (features (quote ("json" "cookies" "gzip"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.152") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.93") (default-features #t) (kind 0)) (crate-dep (name "serde_repr") (req "^0.1.10") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.25.0") (features (quote ("full"))) (default-features #t) (kind 2)))) (hash "1x3hg24gmkzksf78silvhgbcvn24cq3pgz09m1wa6h2sl32zjq96")))

(define-public crate-wattpad-0.2 (crate (name "wattpad") (vers "0.2.4") (deps (list (crate-dep (name "anyhow") (req "^1.0.69") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.7.1") (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.11.14") (features (quote ("json" "cookies" "gzip"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.152") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.93") (default-features #t) (kind 0)) (crate-dep (name "serde_repr") (req "^0.1.10") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.25.0") (features (quote ("full"))) (default-features #t) (kind 2)))) (hash "1vc1ch3bv9a6xw548896kq7ayh28as5fz7zh9aclna5r1n52j9lq")))

(define-public crate-wattpad-0.2 (crate (name "wattpad") (vers "0.2.5") (deps (list (crate-dep (name "anyhow") (req "^1.0.69") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.7.1") (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.11.14") (features (quote ("json" "cookies" "gzip"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.152") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.93") (default-features #t) (kind 0)) (crate-dep (name "serde_repr") (req "^0.1.10") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.25.0") (features (quote ("full"))) (default-features #t) (kind 2)))) (hash "0gsf5by4fbb46lfzvn71p3wdl66yfwb9i8yndmgghi32dypz60rx")))

(define-public crate-wattpad-0.3 (crate (name "wattpad") (vers "0.3.0") (deps (list (crate-dep (name "anyhow") (req "^1.0.69") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.7.1") (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.11.14") (features (quote ("json" "cookies" "gzip"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.152") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.93") (default-features #t) (kind 0)) (crate-dep (name "serde_repr") (req "^0.1.10") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.25.0") (features (quote ("full"))) (default-features #t) (kind 2)))) (hash "0m91nrd371gr5m2f5vi8xjqz8bqiwyd7z1vxqpi9w9gdzy3ri8b4")))

(define-public crate-wattpad-0.3 (crate (name "wattpad") (vers "0.3.1") (deps (list (crate-dep (name "anyhow") (req "^1.0.69") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.7.1") (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.11.14") (features (quote ("json" "cookies" "gzip"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.152") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.93") (default-features #t) (kind 0)) (crate-dep (name "serde_repr") (req "^0.1.10") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.25.0") (features (quote ("full"))) (default-features #t) (kind 2)))) (hash "19qkx0kiwnngirn62qnn8j01bcz942n2ygrn8ajnzrj55m0nrwy4")))

(define-public crate-wattpad-0.3 (crate (name "wattpad") (vers "0.3.2") (deps (list (crate-dep (name "anyhow") (req "^1.0.69") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.7.1") (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.11.14") (features (quote ("json" "cookies" "gzip"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.152") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.93") (default-features #t) (kind 0)) (crate-dep (name "serde_repr") (req "^0.1.10") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.25.0") (features (quote ("full"))) (default-features #t) (kind 2)))) (hash "1spbi4m7b1y7msgxdvvr7ww8fcfzymxanymmzqivdvjyvyfp9i6s")))

