(define-module (crates-io wa d_) #:use-module (crates-io))

(define-public crate-wad_goldsrc-0.1 (crate (name "wad_goldsrc") (vers "0.1.0") (deps (list (crate-dep (name "binrw") (req "^0.12.0") (default-features #t) (kind 0)) (crate-dep (name "com_goldsrc_formats") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "1idmzjv03jhy9caxvzs7m9wacdpczh1hghzx07gr6zpcma1kgi9w")))

