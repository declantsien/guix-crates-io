(define-module (crates-io wa -s) #:use-module (crates-io))

(define-public crate-wa-serde-derive-0.1 (crate (name "wa-serde-derive") (vers "0.1.101") (deps (list (crate-dep (name "watt") (req "^0.1") (default-features #t) (kind 0)))) (hash "0fdqaswca71w04sgrl8zxxk8nzs8njn8s38ywh41g1n9hdjvhm7s")))

(define-public crate-wa-serde-derive-0.1 (crate (name "wa-serde-derive") (vers "0.1.102") (deps (list (crate-dep (name "watt") (req "^0.3") (default-features #t) (kind 0)))) (hash "0ir17q20djgs5jhz2b99cd7kv513l80f7ahv4l4fjh3xkzcwf36v")))

(define-public crate-wa-serde-derive-0.1 (crate (name "wa-serde-derive") (vers "0.1.137") (deps (list (crate-dep (name "watt") (req "^0.4") (default-features #t) (kind 0)))) (hash "1mrrh60l9q5yldxgbzhhzsjs4prb0bm1yzzgix3q23w66bpqn6yz")))

