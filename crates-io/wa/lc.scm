(define-module (crates-io wa lc) #:use-module (crates-io))

(define-public crate-walc_model-0.1 (crate (name "walc_model") (vers "0.1.0") (deps (list (crate-dep (name "serde") (req "^1.0.197") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "0vpqw6ra1ibzx96mhy6wc4jmhsfizgsq4a7c24b0mrdfhhg0ccsm")))

(define-public crate-walc_model-0.1 (crate (name "walc_model") (vers "0.1.1") (deps (list (crate-dep (name "serde") (req "^1.0.197") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "1ka6cbmd39nkbbnk15fd2c3hdmxs2qmjr52bz2988pkjwz8ivgif")))

