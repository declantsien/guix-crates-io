(define-module (crates-io wa bd) #:use-module (crates-io))

(define-public crate-wabdavc-0.1 (crate (name "wabdavc") (vers "0.1.0") (deps (list (crate-dep (name "quick-xml") (req "^0.27.1") (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.11") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.23.0") (features (quote ("full"))) (default-features #t) (kind 2)))) (hash "1v056lqvnnyvnj8rp756m0kjzlapq6ik2kxyhnh1wrhxydlpx894") (yanked #t)))

