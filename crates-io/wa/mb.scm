(define-module (crates-io wa mb) #:use-module (crates-io))

(define-public crate-wambda-0.0.0 (crate (name "wambda") (vers "0.0.0") (hash "09p5945wkhqkkklbcyig2rvjbhjjdjjc3mrf020h0fyg3mhxhkqb")))

(define-public crate-wambo-0.1 (crate (name "wambo") (vers "0.1.0") (deps (list (crate-dep (name "regex") (req "^1.4.2") (default-features #t) (kind 0)))) (hash "0zhslihx79hj0ph4k90d8wnx1q15275iklgqpdxg22xsr4l6v2ww")))

(define-public crate-wambo-0.1 (crate (name "wambo") (vers "0.1.1") (deps (list (crate-dep (name "regex") (req "^1.4.2") (default-features #t) (kind 0)))) (hash "00zifa1jr2r50r4yxfg8h4dcnbd95sppp1fm16wr5k4ih76gyj74")))

(define-public crate-wambo-0.1 (crate (name "wambo") (vers "0.1.2") (deps (list (crate-dep (name "regex") (req "^1.4.2") (default-features #t) (kind 0)))) (hash "1qx6m3cvs6ryjshnzwds4ngkw43brgd3hs4sbhwcvkywm5h9m7yl")))

(define-public crate-wambo-0.1 (crate (name "wambo") (vers "0.1.3") (deps (list (crate-dep (name "regex") (req "^1.4.2") (default-features #t) (kind 0)))) (hash "1zqskbv8ni6z84k7m2x3i1fsnwsmwpr4zvkck90c2jkry1nrcvnv")))

(define-public crate-wambo-0.1 (crate (name "wambo") (vers "0.1.4") (deps (list (crate-dep (name "regex") (req "^1.4.2") (default-features #t) (kind 0)))) (hash "0ggarbnjwfj3dggnwvg654nixkfwpz6gqc71bjafs9jzkzadgfaf")))

(define-public crate-wambo-0.1 (crate (name "wambo") (vers "0.1.5") (deps (list (crate-dep (name "derive_more") (req "^0.99.11") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.4.2") (default-features #t) (kind 0)))) (hash "08q5vla70ywhhk7746xb1wz5w35bmnjszrdyyyynl4dinv7wkw82")))

(define-public crate-wambo-0.2 (crate (name "wambo") (vers "0.2.0") (deps (list (crate-dep (name "crossterm") (req "^0.18.2") (default-features #t) (kind 0)) (crate-dep (name "derive_more") (req "^0.99.11") (default-features #t) (kind 0)) (crate-dep (name "fraction_list_fmt_align") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.4.2") (default-features #t) (kind 0)))) (hash "1qqlx02n4zh9la5z5j71zwn4wdzlal6pkjr4pnj511gaarbla3d6")))

(define-public crate-wambo-0.2 (crate (name "wambo") (vers "0.2.1") (deps (list (crate-dep (name "crossterm") (req "^0.18.2") (default-features #t) (kind 0)) (crate-dep (name "derive_more") (req "^0.99.11") (default-features #t) (kind 0)) (crate-dep (name "fraction_list_fmt_align") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.4.2") (default-features #t) (kind 0)))) (hash "14nd3zrzl62ky9c16hm49gsi5wbzlvfyj5ppnm9jj5fx0nj7pcf1")))

(define-public crate-wambo-0.2 (crate (name "wambo") (vers "0.2.2") (deps (list (crate-dep (name "crossterm") (req "^0.18.2") (default-features #t) (kind 0)) (crate-dep (name "derive_more") (req "^0.99.11") (default-features #t) (kind 0)) (crate-dep (name "fraction_list_fmt_align") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.4.2") (default-features #t) (kind 0)))) (hash "0fmagiq5i36kykc3gd8mzi3009p9wgkxcs4wrfnbx0jgahdzgdnm")))

(define-public crate-wambo-0.2 (crate (name "wambo") (vers "0.2.3") (deps (list (crate-dep (name "crossterm") (req "^0.18.2") (default-features #t) (kind 0)) (crate-dep (name "derive_more") (req "^0.99.11") (default-features #t) (kind 0)) (crate-dep (name "fraction_list_fmt_align") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.4.2") (default-features #t) (kind 0)))) (hash "12rgyihappi8kr0sjpkvjns0zcsk8hg567c99mjs4xw4x6n0g4qc")))

(define-public crate-wambo-0.2 (crate (name "wambo") (vers "0.2.4") (deps (list (crate-dep (name "crossterm") (req "^0.18.2") (default-features #t) (kind 0)) (crate-dep (name "derive_more") (req "^0.99.11") (default-features #t) (kind 0)) (crate-dep (name "fraction_list_fmt_align") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.4.2") (default-features #t) (kind 0)))) (hash "0bdh96bizrw0i2c9pmwvfi67b2lm8dql1w67sdmlxwhkifs9kr2v")))

(define-public crate-wambo-0.2 (crate (name "wambo") (vers "0.2.5") (deps (list (crate-dep (name "crossterm") (req "^0.18.2") (default-features #t) (kind 0)) (crate-dep (name "derive_more") (req "^0.99.11") (default-features #t) (kind 0)) (crate-dep (name "fraction_list_fmt_align") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.4.2") (default-features #t) (kind 0)))) (hash "0b25crn90jrawgivmkx6a1mbks8y2dyn6dcv2xrbb0f8x855wcz1")))

(define-public crate-wambo-0.2 (crate (name "wambo") (vers "0.2.6") (deps (list (crate-dep (name "crossterm") (req "^0.18.2") (default-features #t) (kind 0)) (crate-dep (name "derive_more") (req "^0.99.11") (default-features #t) (kind 0)) (crate-dep (name "fraction_list_fmt_align") (req "^0.2.2") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.4.2") (default-features #t) (kind 0)))) (hash "0kvd005iapk7lw8h1sgrjjk1d02abncnrqjvhvhbawxlsax6x1gv") (yanked #t)))

(define-public crate-wambo-0.2 (crate (name "wambo") (vers "0.2.7") (deps (list (crate-dep (name "crossterm") (req "^0.18.2") (default-features #t) (kind 0)) (crate-dep (name "derive_more") (req "^0.99.11") (default-features #t) (kind 0)) (crate-dep (name "fraction_list_fmt_align") (req "^0.2.2") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.4.2") (default-features #t) (kind 0)))) (hash "1jdvvfjp1mhibwn9mwyxxf2xkzj5a6m5wqk4vvlhxxxph2frj1ly")))

(define-public crate-wambo-0.2 (crate (name "wambo") (vers "0.2.8") (deps (list (crate-dep (name "crossterm") (req "^0.18.2") (default-features #t) (kind 0)) (crate-dep (name "derive_more") (req "^0.99.11") (default-features #t) (kind 0)) (crate-dep (name "fraction_list_fmt_align") (req "^0.2.2") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.4.2") (default-features #t) (kind 0)))) (hash "0nadk2dcaa51p0dhly63bc7rfgbkq5g2krvmwl9jszrlcfr0aqb4")))

(define-public crate-wambo-0.2 (crate (name "wambo") (vers "0.2.9") (deps (list (crate-dep (name "crossterm") (req "^0.18.2") (default-features #t) (kind 0)) (crate-dep (name "derive_more") (req "^0.99.11") (default-features #t) (kind 0)) (crate-dep (name "fraction_list_fmt_align") (req "^0.2.3") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.4.2") (default-features #t) (kind 0)))) (hash "18zg4ry4nm83qx6vfhnfi1l2pjr5nvlzbqaq3k0l5cphbdww4cnf")))

(define-public crate-wambo-0.2 (crate (name "wambo") (vers "0.2.10") (deps (list (crate-dep (name "crossterm") (req "^0.19.0") (default-features #t) (kind 0)) (crate-dep (name "derive_more") (req "^0.99.11") (default-features #t) (kind 0)) (crate-dep (name "fraction_list_fmt_align") (req "^0.2.3") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.4.3") (default-features #t) (kind 0)))) (hash "1l9qdxn5b3wfqad836x5h2gfsc9vkrwmv0xs4djj2mxi3abzx978")))

(define-public crate-wambo-0.2 (crate (name "wambo") (vers "0.2.11") (deps (list (crate-dep (name "crossterm") (req "^0.19.0") (default-features #t) (kind 0)) (crate-dep (name "derive_more") (req "^0.99.11") (default-features #t) (kind 0)) (crate-dep (name "fraction_list_fmt_align") (req "^0.2.3") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.4.3") (default-features #t) (kind 0)))) (hash "18y0s1scbvsagrd14c3f8bx8gar888mdg44kwc8wphy3434k9grl")))

(define-public crate-wambo-0.2 (crate (name "wambo") (vers "0.2.12") (deps (list (crate-dep (name "crossterm") (req "^0.25") (default-features #t) (kind 0)) (crate-dep (name "derive_more") (req "^0.99.11") (features (quote ("display"))) (kind 0)) (crate-dep (name "fraction_list_fmt_align") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.6.0") (default-features #t) (kind 0)))) (hash "1ymyghgcj5rkhp3xbi0q3p4460xg9s6k3f1ld0vd50rbq8aa88lp")))

(define-public crate-wambo-0.3 (crate (name "wambo") (vers "0.3.0") (deps (list (crate-dep (name "crossterm") (req "^0.25") (default-features #t) (kind 0)) (crate-dep (name "derive_more") (req "^0.99") (features (quote ("display"))) (kind 0)) (crate-dep (name "fraction_list_fmt_align") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.6") (default-features #t) (kind 0)) (crate-dep (name "tui") (req "^0.19") (default-features #t) (kind 0)))) (hash "0199z91y7lqzmlwgs92zjghkz0hn0m1gw22982kahf7qg5rbp30q")))

(define-public crate-wambo-0.3 (crate (name "wambo") (vers "0.3.1") (deps (list (crate-dep (name "crossterm") (req "^0.25") (default-features #t) (kind 0)) (crate-dep (name "derive_more") (req "^0.99") (features (quote ("display"))) (kind 0)) (crate-dep (name "fraction_list_fmt_align") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.6") (default-features #t) (kind 0)) (crate-dep (name "tui") (req "^0.19") (default-features #t) (kind 0)))) (hash "0i97ipgkma0rcbgnh955m8mk5hm3bkfwgmflcra434s7habzd833")))

