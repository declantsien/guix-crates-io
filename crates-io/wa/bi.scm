(define-module (crates-io wa bi) #:use-module (crates-io))

(define-public crate-wabi-0.1 (crate (name "wabi") (vers "0.1.0") (hash "1g5f35wnixxf5swccbv01lvjlj398r2q8klx12s1ss5424hyihjm")))

(define-public crate-wabi-usd-0.1 (crate (name "wabi-usd") (vers "0.1.0") (hash "1rrzg14ajr8rrrxyw2cdpwy8xmxvi7qaqa43xm0h80lyi4f9awiw")))

(define-public crate-wabi-usd-0.1 (crate (name "wabi-usd") (vers "0.1.1") (hash "1hgj6d4zc9zvx0zass171g20kiy7a7yh129j36f464mfalqfd5hl")))

(define-public crate-wabisabi-0.0.0 (crate (name "wabisabi") (vers "0.0.0") (deps (list (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)))) (hash "1v9vxgxg1bifcajzjc8ma67cddwl6rzn4737vi45gsfgwl6m358j")))

(define-public crate-wabisabi-0.0.1 (crate (name "wabisabi") (vers "0.0.1") (deps (list (crate-dep (name "const-random") (req "^0.1.13") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)))) (hash "07qg9r248sj7pz009c75b9lxfnw2g3ay3m9qpjaamvgj1k0kgrva")))

