(define-module (crates-io a7 #{10}#) #:use-module (crates-io))

(define-public crate-a7105-0.1 (crate (name "a7105") (vers "0.1.0") (deps (list (crate-dep (name "defmt") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "embedded-hal") (req "^1.0.0-rc.1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "embedded-hal-async") (req "^1.0.0-rc.1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "maybe-async") (req "^0.2") (default-features #t) (kind 0)))) (hash "090ckqnmfb58xc4k4iqj3zmz92mz90wzcspd4g5mphjgnylyymx6") (features (quote (("default" "async") ("blocking" "embedded-hal" "maybe-async/is_sync") ("async" "embedded-hal-async"))))))

