(define-module (crates-io a7 #{93}#) #:use-module (crates-io))

(define-public crate-a793556702-0.1 (crate (name "a793556702") (vers "0.1.0") (hash "13xr7jidyfjpkg1qshcr9rwczbkim8y3bgb91jvwvxqfmk377i72")))

(define-public crate-a793556702-0.1 (crate (name "a793556702") (vers "0.1.1") (hash "1vfk11ncr6avbc5vb4iy98jq3ax756r8r8ry1p1va8w2s4ddghli")))

(define-public crate-a793556702-0.1 (crate (name "a793556702") (vers "0.1.2") (hash "16xks1bvfsk9j10x4h52pclma6pvjai62pa977mw57w0nldiync9")))

(define-public crate-a793556702-0.1 (crate (name "a793556702") (vers "0.1.3") (hash "1h08f3w406w3jjlsvqmraagd5zc9hd57y7lbxx7nidfby4w0zhv8")))

