(define-module (crates-io d4 -h) #:use-module (crates-io))

(define-public crate-d4-hts-0.1 (crate (name "d4-hts") (vers "0.1.1") (deps (list (crate-dep (name "bindgen") (req "^0.55.1") (default-features #t) (kind 1)) (crate-dep (name "pkg-config") (req "^0.3.18") (default-features #t) (kind 1)))) (hash "1fgy3ns4plp1f3xhz2bb61zk1ad7pspvha67zv6dn7828r2r4ikp")))

(define-public crate-d4-hts-0.1 (crate (name "d4-hts") (vers "0.1.2") (deps (list (crate-dep (name "bindgen") (req "^0.55.1") (default-features #t) (kind 1)) (crate-dep (name "pkg-config") (req "^0.3.18") (default-features #t) (kind 1)))) (hash "1j80jr8jkrxii9n570xm23vg4qs5ysd424d6nmjsw8kbgw8rl4q7")))

(define-public crate-d4-hts-0.1 (crate (name "d4-hts") (vers "0.1.8") (deps (list (crate-dep (name "bindgen") (req "^0.55.1") (default-features #t) (kind 1)) (crate-dep (name "pkg-config") (req "^0.3.18") (default-features #t) (kind 1)))) (hash "0fsi5kdwslbnq6n21x4w0ppfyxmvvcxm684gpcww7h3xd7n9kal0")))

(define-public crate-d4-hts-0.1 (crate (name "d4-hts") (vers "0.1.9") (deps (list (crate-dep (name "bindgen") (req "^0.55.1") (default-features #t) (kind 1)) (crate-dep (name "pkg-config") (req "^0.3.18") (default-features #t) (kind 1)))) (hash "1qvjsf2ivj0vv7645zv1wnn2j4qfdiaf705xvzjndvfjqnh0alss")))

(define-public crate-d4-hts-0.1 (crate (name "d4-hts") (vers "0.1.11") (deps (list (crate-dep (name "bindgen") (req "^0.55.1") (default-features #t) (kind 1)) (crate-dep (name "pkg-config") (req "^0.3.18") (default-features #t) (kind 1)))) (hash "1sy2y80x65w5x4kx5hqc50ahnhb1mgz0rh76ni2x6mfidrcd3b7r")))

(define-public crate-d4-hts-0.1 (crate (name "d4-hts") (vers "0.1.12") (deps (list (crate-dep (name "bindgen") (req "^0.55.1") (default-features #t) (kind 1)) (crate-dep (name "pkg-config") (req "^0.3.18") (default-features #t) (kind 1)))) (hash "1vs4mjp1131ksw7rr2ah9hhfyjnvviis67mz0czczsx56pac7k64")))

(define-public crate-d4-hts-0.1 (crate (name "d4-hts") (vers "0.1.13") (deps (list (crate-dep (name "bindgen") (req "^0.55.1") (default-features #t) (kind 1)) (crate-dep (name "pkg-config") (req "^0.3.18") (default-features #t) (kind 1)))) (hash "1ml3aiwj13gnn1rc6ngkwrdds6f6vgi4j2saz26sydrsc35knagy")))

(define-public crate-d4-hts-0.1 (crate (name "d4-hts") (vers "0.1.14") (deps (list (crate-dep (name "bindgen") (req "^0.55.1") (default-features #t) (kind 1)) (crate-dep (name "pkg-config") (req "^0.3.18") (default-features #t) (kind 1)))) (hash "03mg1br9gmpsjqnyb6c18b1j978zpz2bwr2wmk0512vjxkj6gbwc")))

(define-public crate-d4-hts-0.1 (crate (name "d4-hts") (vers "0.1.15") (deps (list (crate-dep (name "bindgen") (req "^0.55.1") (default-features #t) (kind 1)) (crate-dep (name "pkg-config") (req "^0.3.18") (default-features #t) (kind 1)))) (hash "1xmmwdc1f1wmdk8k34s052apv69nf8n7wrs4xfjsy62qx12m2shi")))

(define-public crate-d4-hts-0.1 (crate (name "d4-hts") (vers "0.1.16") (deps (list (crate-dep (name "bindgen") (req "^0.55.1") (default-features #t) (kind 1)) (crate-dep (name "pkg-config") (req "^0.3.18") (default-features #t) (kind 1)))) (hash "1hbk0rafmk8qiy6ry1wrs3ssyf5l4kgbx2bwv42pswmh5qs4x0yn")))

(define-public crate-d4-hts-0.1 (crate (name "d4-hts") (vers "0.1.17") (deps (list (crate-dep (name "bindgen") (req "^0.55.1") (default-features #t) (kind 1)) (crate-dep (name "pkg-config") (req "^0.3.18") (default-features #t) (kind 1)))) (hash "0qq1nyrk14y6nx1sl61hv7fpqy6w3f6xshijs5w3clnl2ppjclpf")))

(define-public crate-d4-hts-0.1 (crate (name "d4-hts") (vers "0.1.18") (deps (list (crate-dep (name "bindgen") (req "^0.55.1") (default-features #t) (kind 1)) (crate-dep (name "pkg-config") (req "^0.3.18") (default-features #t) (kind 1)))) (hash "1sw0a84gs41rzckkr9cvw8sm5pl2g77hv8p5i28r8ld1sj05n11z")))

(define-public crate-d4-hts-0.2 (crate (name "d4-hts") (vers "0.2.17") (deps (list (crate-dep (name "bindgen") (req "^0.55.1") (default-features #t) (kind 1)) (crate-dep (name "pkg-config") (req "^0.3.18") (default-features #t) (kind 1)))) (hash "0virpy8gjc1vv28rwh6fi5k1wr1p34hrvlfrmpqwicx0gj86kkli")))

(define-public crate-d4-hts-0.2 (crate (name "d4-hts") (vers "0.2.18") (deps (list (crate-dep (name "bindgen") (req "^0.55.1") (default-features #t) (kind 1)) (crate-dep (name "pkg-config") (req "^0.3.18") (default-features #t) (kind 1)))) (hash "12w03jywpi8rph7f4qah2826q5gn3n0sbc6l7h0ym7zipjm5l4gk")))

(define-public crate-d4-hts-0.3 (crate (name "d4-hts") (vers "0.3.0") (deps (list (crate-dep (name "bindgen") (req "^0.55.1") (default-features #t) (kind 1)) (crate-dep (name "pkg-config") (req "^0.3.18") (default-features #t) (kind 1)))) (hash "0pyqmjkfa9jc0y3ir2gpim45gr6qh2v8mlcdlja3ifrpcg5ifjfm")))

(define-public crate-d4-hts-0.3 (crate (name "d4-hts") (vers "0.3.1") (deps (list (crate-dep (name "bindgen") (req "^0.55.1") (default-features #t) (kind 1)) (crate-dep (name "pkg-config") (req "^0.3.18") (default-features #t) (kind 1)))) (hash "1fylg3j8aq6nb9bwf8rivzhp174abmg8ggndpdpkli24vn28aidc")))

(define-public crate-d4-hts-0.3 (crate (name "d4-hts") (vers "0.3.2") (deps (list (crate-dep (name "bindgen") (req "^0.55.1") (default-features #t) (kind 1)) (crate-dep (name "pkg-config") (req "^0.3.18") (default-features #t) (kind 1)))) (hash "1n40pn7rawcgjmhmxs4f893kkgww1v1314i2pvsi48c998ncvyj0")))

(define-public crate-d4-hts-0.3 (crate (name "d4-hts") (vers "0.3.3") (deps (list (crate-dep (name "bindgen") (req "^0.55.1") (default-features #t) (kind 1)) (crate-dep (name "pkg-config") (req "^0.3.18") (default-features #t) (kind 1)))) (hash "0cqd2yljw55fvf2v8xd94n1vwyyw9ni9dr9xk9a5pm3628l5zrch")))

(define-public crate-d4-hts-0.3 (crate (name "d4-hts") (vers "0.3.4") (deps (list (crate-dep (name "bindgen") (req "^0.55.1") (default-features #t) (kind 1)) (crate-dep (name "pkg-config") (req "^0.3.18") (default-features #t) (kind 1)))) (hash "1avaphpy92ay2hvvmnrdj31qhhivx7a7dfxbh2hkqql4rgma8a3q")))

(define-public crate-d4-hts-0.3 (crate (name "d4-hts") (vers "0.3.5") (deps (list (crate-dep (name "bindgen") (req "^0.55.1") (default-features #t) (kind 1)) (crate-dep (name "pkg-config") (req "^0.3.18") (default-features #t) (kind 1)))) (hash "132975rscs8wp2zzhrdvhwyvg1wcz07095ivl9b8rqdfcmsibrsj")))

(define-public crate-d4-hts-0.3 (crate (name "d4-hts") (vers "0.3.6") (deps (list (crate-dep (name "bindgen") (req "^0.55.1") (default-features #t) (kind 1)) (crate-dep (name "pkg-config") (req "^0.3.18") (default-features #t) (kind 1)))) (hash "1fy0pszq2ldhydr7blm4kzgzl642fsfsgwh14ixmag4pd5aaj8y1")))

(define-public crate-d4-hts-0.3 (crate (name "d4-hts") (vers "0.3.7") (deps (list (crate-dep (name "bindgen") (req "^0.55.1") (default-features #t) (kind 1)) (crate-dep (name "pkg-config") (req "^0.3.18") (default-features #t) (kind 1)))) (hash "15xkx38avn49vr8vl04wixajgjkfwqpjdsjha7c0vp600p0ww9i9")))

(define-public crate-d4-hts-0.3 (crate (name "d4-hts") (vers "0.3.9") (deps (list (crate-dep (name "bindgen") (req "^0.55.1") (default-features #t) (kind 1)) (crate-dep (name "pkg-config") (req "^0.3.18") (default-features #t) (kind 1)))) (hash "10fkksyqx9qnza7dbp02lhnzhq71g9mzb72s879h4lmmx1nczfaq")))

