(define-module (crates-io d4 -f) #:use-module (crates-io))

(define-public crate-d4-framefile-0.1 (crate (name "d4-framefile") (vers "0.1.0") (deps (list (crate-dep (name "flate2") (req "^1.0.13") (default-features #t) (kind 0)) (crate-dep (name "memmap") (req "^0.7.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.104") (default-features #t) (kind 0)))) (hash "081f221wr0zf0675wgxsaqkk0jbyf2frvi0rqs7fvfbgrx2bkdvi")))

(define-public crate-d4-framefile-0.1 (crate (name "d4-framefile") (vers "0.1.8") (deps (list (crate-dep (name "flate2") (req "^1.0.13") (default-features #t) (kind 0)) (crate-dep (name "memmap") (req "^0.7.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.104") (default-features #t) (kind 0)))) (hash "13lg3w306msvi35ci97i745af57h8pjj9qksinh53f2vy329sl52")))

(define-public crate-d4-framefile-0.1 (crate (name "d4-framefile") (vers "0.1.9") (deps (list (crate-dep (name "flate2") (req "^1.0.13") (default-features #t) (kind 0)) (crate-dep (name "memmap") (req "^0.7.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.104") (default-features #t) (kind 0)))) (hash "1y8qfqahd2sr86fk003g2h31y3rkggcg348zp5z6c53by0996qmv")))

(define-public crate-d4-framefile-0.1 (crate (name "d4-framefile") (vers "0.1.11") (deps (list (crate-dep (name "flate2") (req "^1.0.13") (default-features #t) (kind 0)) (crate-dep (name "memmap") (req "^0.7.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.104") (default-features #t) (kind 0)))) (hash "0myqqdshg94xmw1vadfcz9sj4kn5xkb6qr1q7wivgbbidx0284mz")))

(define-public crate-d4-framefile-0.1 (crate (name "d4-framefile") (vers "0.1.12") (deps (list (crate-dep (name "flate2") (req "^1.0.13") (default-features #t) (kind 0)) (crate-dep (name "memmap") (req "^0.7.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.104") (default-features #t) (kind 0)))) (hash "1w4cyqhzxzjvgyhi41zilbmdfvwkvd1vsw14gvlnma428h5xfb2m")))

(define-public crate-d4-framefile-0.1 (crate (name "d4-framefile") (vers "0.1.13") (deps (list (crate-dep (name "flate2") (req "^1.0.13") (default-features #t) (kind 0)) (crate-dep (name "memmap") (req "^0.7.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.104") (default-features #t) (kind 0)))) (hash "1wnm2s60vyfrjkpcbzfvwyxvbyjqnri7l47sdnf71pn0r21f3phl")))

(define-public crate-d4-framefile-0.1 (crate (name "d4-framefile") (vers "0.1.14") (deps (list (crate-dep (name "flate2") (req "^1.0.13") (default-features #t) (kind 0)) (crate-dep (name "memmap") (req "^0.7.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.104") (default-features #t) (kind 0)))) (hash "0p1ivxx24ja7qdi5crm456a0gjyl8qyp67ji09dh1zmbzqwx334c")))

(define-public crate-d4-framefile-0.1 (crate (name "d4-framefile") (vers "0.1.15") (deps (list (crate-dep (name "flate2") (req "^1.0.13") (default-features #t) (kind 0)) (crate-dep (name "memmap") (req "^0.7.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.104") (default-features #t) (kind 0)))) (hash "1v7bp5kpdsc24kjaflp4jagdlgwf080sf1b6xrdp3c4cyamd8xkl")))

(define-public crate-d4-framefile-0.1 (crate (name "d4-framefile") (vers "0.1.16") (deps (list (crate-dep (name "flate2") (req "^1.0.13") (default-features #t) (kind 0)) (crate-dep (name "memmap") (req "^0.7.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.104") (default-features #t) (kind 0)))) (hash "1xjfn9nwxyz8plbbkl31r7mi9vicqh788svv8a88aq4qghg36sn9")))

(define-public crate-d4-framefile-0.1 (crate (name "d4-framefile") (vers "0.1.17") (deps (list (crate-dep (name "flate2") (req "^1.0.13") (default-features #t) (kind 0)) (crate-dep (name "memmap") (req "^0.7.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.104") (default-features #t) (kind 0)))) (hash "1d5srizinrfl46y8h1syv879b8234a6h7wpyz00zkd8znwmx50kf")))

(define-public crate-d4-framefile-0.1 (crate (name "d4-framefile") (vers "0.1.18") (deps (list (crate-dep (name "flate2") (req "^1.0.13") (default-features #t) (kind 0)) (crate-dep (name "memmap") (req "^0.7.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.104") (default-features #t) (kind 0)))) (hash "0zimj2wcpd3n5h4l244h5zxrsp0y4kc96m2znxl927mg5cha7xnj")))

(define-public crate-d4-framefile-0.2 (crate (name "d4-framefile") (vers "0.2.17") (deps (list (crate-dep (name "flate2") (req "^1.0.13") (default-features #t) (kind 0)) (crate-dep (name "memmap") (req "^0.7.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.104") (default-features #t) (kind 0)))) (hash "12ys9d51vawnn72ibxyr3piikyr1r253iinzn76c0vn5knk61rzj")))

(define-public crate-d4-framefile-0.2 (crate (name "d4-framefile") (vers "0.2.18") (deps (list (crate-dep (name "flate2") (req "^1.0.13") (default-features #t) (kind 0)) (crate-dep (name "memmap") (req "^0.7.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.104") (default-features #t) (kind 0)))) (hash "131r7rckdlcvxdvrrsq59g81x6lnd70a63l018ic233j0zzj9xwz")))

(define-public crate-d4-framefile-0.3 (crate (name "d4-framefile") (vers "0.3.0") (deps (list (crate-dep (name "memmap") (req "^0.7.0") (optional #t) (default-features #t) (kind 0)))) (hash "1p8m42pb6x04c91sshkxpj3gqq6d9h2rdw7lvjdwmqfaxi1k95mm") (features (quote (("mapped_io" "memmap"))))))

(define-public crate-d4-framefile-0.3 (crate (name "d4-framefile") (vers "0.3.1") (deps (list (crate-dep (name "memmap") (req "^0.7.0") (optional #t) (default-features #t) (kind 0)))) (hash "10j6sh2b1613h0xyx3vnk34naf7l4x043ck1slqww1la796kd2b4") (features (quote (("mapped_io" "memmap"))))))

(define-public crate-d4-framefile-0.3 (crate (name "d4-framefile") (vers "0.3.2") (deps (list (crate-dep (name "memmap") (req "^0.7.0") (optional #t) (default-features #t) (kind 0)))) (hash "1gg5gwwmcfmbfzwj4in86d04wzi68ci156a5h13mfn0a8pxq46di") (features (quote (("mapped_io" "memmap"))))))

(define-public crate-d4-framefile-0.3 (crate (name "d4-framefile") (vers "0.3.3") (deps (list (crate-dep (name "memmap") (req "^0.7.0") (optional #t) (default-features #t) (kind 0)))) (hash "0fczj1zvf0p6p151cx9llmp3fgqz44cqygczncpkkpzkdbkgrq9c") (features (quote (("mapped_io" "memmap"))))))

(define-public crate-d4-framefile-0.3 (crate (name "d4-framefile") (vers "0.3.4") (deps (list (crate-dep (name "memmap") (req "^0.7.0") (optional #t) (default-features #t) (kind 0)))) (hash "0126m0kwzvdz8svkspkx1qzi0p1sd748j9rr9nkiykppn242gdy7") (features (quote (("mapped_io" "memmap"))))))

(define-public crate-d4-framefile-0.3 (crate (name "d4-framefile") (vers "0.3.5") (deps (list (crate-dep (name "memmap") (req "^0.7.0") (optional #t) (default-features #t) (kind 0)))) (hash "1sb6ra5fim1h4rln8h7znxjhlba5lhg26a041kryvhr9if1a837m") (features (quote (("mapped_io" "memmap"))))))

(define-public crate-d4-framefile-0.3 (crate (name "d4-framefile") (vers "0.3.6") (deps (list (crate-dep (name "memmap") (req "^0.7.0") (optional #t) (default-features #t) (kind 0)))) (hash "1sh30r9b69rqz0xk2qmdqxpdg599jvmg36187kxvrgy0nfmszjs5") (features (quote (("mapped_io" "memmap"))))))

(define-public crate-d4-framefile-0.3 (crate (name "d4-framefile") (vers "0.3.9") (deps (list (crate-dep (name "memmap") (req "^0.7.0") (optional #t) (default-features #t) (kind 0)))) (hash "1jn7lqjxg3zdypsff9c2cl19n38j5faqq6a7wq5bikynl4fmx5fw") (features (quote (("mapped_io" "memmap"))))))

