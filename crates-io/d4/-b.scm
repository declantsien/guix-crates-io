(define-module (crates-io d4 -b) #:use-module (crates-io))

(define-public crate-d4-bigwig-0.1 (crate (name "d4-bigwig") (vers "0.1.0") (deps (list (crate-dep (name "bindgen") (req "^0.55.1") (default-features #t) (kind 1)))) (hash "1adw32ad1gfgy4iax8c6h74z159nh7z55bgkh06zh4rd5al62ac1")))

(define-public crate-d4-bigwig-0.1 (crate (name "d4-bigwig") (vers "0.1.1") (deps (list (crate-dep (name "bindgen") (req "^0.55.1") (default-features #t) (kind 1)))) (hash "1ryw5k89chh7qvw0ggqar58078xbpgpwsv4s4vvk85vhzkmwjig7") (features (quote (("docs_build"))))))

(define-public crate-d4-bigwig-0.1 (crate (name "d4-bigwig") (vers "0.1.2") (deps (list (crate-dep (name "bindgen") (req "^0.55.1") (default-features #t) (kind 1)))) (hash "02ll5ymvc6j8044pqbyrsvajgbsqsj5836rwjfrdvfkcgv3ydxip")))

(define-public crate-d4-bigwig-0.1 (crate (name "d4-bigwig") (vers "0.1.8") (deps (list (crate-dep (name "bindgen") (req "^0.55.1") (default-features #t) (kind 1)))) (hash "0bi21b38qy6yp2a5s9a7i0as0j6yl141jj02qxj5x7ixl315szi5")))

(define-public crate-d4-bigwig-0.1 (crate (name "d4-bigwig") (vers "0.1.9") (deps (list (crate-dep (name "bindgen") (req "^0.55.1") (default-features #t) (kind 1)))) (hash "1kyvcxlhbjs6hwqdgjwj2ca61yswdr5c1pqb1ss5xkdxvjja75p2")))

(define-public crate-d4-bigwig-0.1 (crate (name "d4-bigwig") (vers "0.1.11") (deps (list (crate-dep (name "bindgen") (req "^0.55.1") (default-features #t) (kind 1)))) (hash "0r06p9n4dzjbsn5ys4z5g4a84sbz8kanwp8hzk5ddr29zgzrig6j")))

(define-public crate-d4-bigwig-0.1 (crate (name "d4-bigwig") (vers "0.1.12") (deps (list (crate-dep (name "bindgen") (req "^0.55.1") (default-features #t) (kind 1)))) (hash "1a0c7kymwknf0j1sxdxygdaq2i9cqhva7fd7wb1zc0sm50v8n53k")))

(define-public crate-d4-bigwig-0.1 (crate (name "d4-bigwig") (vers "0.1.13") (deps (list (crate-dep (name "bindgen") (req "^0.55.1") (default-features #t) (kind 1)))) (hash "0s9cn3rcvg8kfgslypfmppz787xj698jvrnj9ks3w6hfn4za2lbr")))

(define-public crate-d4-bigwig-0.1 (crate (name "d4-bigwig") (vers "0.1.14") (deps (list (crate-dep (name "bindgen") (req "^0.55.1") (default-features #t) (kind 1)))) (hash "035brhjh9rmwyhysccwararnh9bzqz1n8462lg8hxb94m9zwh2bw")))

(define-public crate-d4-bigwig-0.1 (crate (name "d4-bigwig") (vers "0.1.15") (deps (list (crate-dep (name "bindgen") (req "^0.55.1") (default-features #t) (kind 1)))) (hash "0y2q154l4snb7ml7bd09sa4s2746ghr51aqb34hkgbadwd4axwgj")))

(define-public crate-d4-bigwig-0.1 (crate (name "d4-bigwig") (vers "0.1.16") (deps (list (crate-dep (name "bindgen") (req "^0.55.1") (default-features #t) (kind 1)))) (hash "073g2yf9c72g55ily60brh6jixpc2cdpzis1l1ragx1xp27h9wpi")))

(define-public crate-d4-bigwig-0.1 (crate (name "d4-bigwig") (vers "0.1.17") (deps (list (crate-dep (name "bindgen") (req "^0.55.1") (default-features #t) (kind 1)))) (hash "1hv8n02jchj55n548hy5ikcv0j686kx8f5alxcbdg7chwyg36x4n")))

(define-public crate-d4-bigwig-0.1 (crate (name "d4-bigwig") (vers "0.1.18") (deps (list (crate-dep (name "bindgen") (req "^0.55.1") (default-features #t) (kind 1)))) (hash "0k78bn9h2hdkw15qsai51w47kw4aqqqmkwwr26m7z54zdifz5csi")))

(define-public crate-d4-bigwig-0.2 (crate (name "d4-bigwig") (vers "0.2.17") (deps (list (crate-dep (name "bindgen") (req "^0.55.1") (default-features #t) (kind 1)))) (hash "0ypg04qb4s0k0yx9shl7wwl7mw0l8ji8rgf3hadjjkdyfdjw7v16")))

(define-public crate-d4-bigwig-0.2 (crate (name "d4-bigwig") (vers "0.2.18") (deps (list (crate-dep (name "bindgen") (req "^0.55.1") (default-features #t) (kind 1)))) (hash "00h30kq0cn4g297dd9p7i80nh153qkxkk2s94za13arc2k8yr22v")))

(define-public crate-d4-bigwig-0.3 (crate (name "d4-bigwig") (vers "0.3.0") (deps (list (crate-dep (name "bindgen") (req "^0.55.1") (default-features #t) (kind 1)))) (hash "0dj8b3f1qs7imv2g1my4mbnq2rv27jz3bim1rldg5ziz32vspr6h")))

(define-public crate-d4-bigwig-0.3 (crate (name "d4-bigwig") (vers "0.3.1") (deps (list (crate-dep (name "bindgen") (req "^0.55.1") (default-features #t) (kind 1)))) (hash "00dffpihqxiahpw1lmw3xr0c0jhyxawqpyniawrz3s1q54w5wsh9")))

(define-public crate-d4-bigwig-0.3 (crate (name "d4-bigwig") (vers "0.3.2") (deps (list (crate-dep (name "bindgen") (req "^0.55.1") (default-features #t) (kind 1)))) (hash "1sjflwsdv7kbp149aby8ixi7j6kcr55qmglpvsblpc7wq14xlak8")))

(define-public crate-d4-bigwig-0.3 (crate (name "d4-bigwig") (vers "0.3.3") (deps (list (crate-dep (name "bindgen") (req "^0.55.1") (default-features #t) (kind 1)))) (hash "0zxzzjikmbgy597ma48ghqv5zxy08a8sc7h2sybrfhgzbmn338ak")))

(define-public crate-d4-bigwig-0.3 (crate (name "d4-bigwig") (vers "0.3.4") (deps (list (crate-dep (name "bindgen") (req "^0.55.1") (default-features #t) (kind 1)))) (hash "077yk3pq6s1b8i0947cbyqsn511y0y4jsys4pn7wfp631rp1rgkr")))

(define-public crate-d4-bigwig-0.3 (crate (name "d4-bigwig") (vers "0.3.5") (deps (list (crate-dep (name "bindgen") (req "^0.55.1") (default-features #t) (kind 1)))) (hash "13pmmqh4850djgl5d3wih5h3wygq6zwjli6bbck5m72g428fmvcx")))

(define-public crate-d4-bigwig-0.3 (crate (name "d4-bigwig") (vers "0.3.6") (deps (list (crate-dep (name "bindgen") (req "^0.55.1") (default-features #t) (kind 1)))) (hash "0n1pwbs220754q3q6crrjn0ycqab6vhhs35prlhxhyz93zd997fp")))

(define-public crate-d4-bigwig-0.3 (crate (name "d4-bigwig") (vers "0.3.9") (deps (list (crate-dep (name "bindgen") (req "^0.55.1") (default-features #t) (kind 1)))) (hash "1yrcwzc9gp24xba8vp535m151339pi5lal75lgzwaqjplplw6lvy")))

