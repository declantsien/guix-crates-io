(define-module (crates-io hl _c) #:use-module (crates-io))

(define-public crate-hl_core-0.1 (crate (name "hl_core") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^3.0.0-rc.7") (default-features #t) (kind 0)) (crate-dep (name "yaml-rust") (req "^0.4") (default-features #t) (kind 0)))) (hash "06nqdrc6xrfkn6yhjxk2s150p252h8ym5p1dxrjmpx92z64kflq0")))

(define-public crate-hl_core-0.1 (crate (name "hl_core") (vers "0.1.1") (deps (list (crate-dep (name "clap") (req "^3.0.0-rc.7") (default-features #t) (kind 0)) (crate-dep (name "yaml-rust") (req "^0.4") (default-features #t) (kind 0)))) (hash "1gxbxcw7r0p34mncgf9xzc2fi5cj2pgnj7mlqxwh51x571jqjzx7")))

(define-public crate-hl_core-0.1 (crate (name "hl_core") (vers "0.1.2") (hash "11g71jvqrjax4gdsska2wys1q82b6f79ycjh96qx0z4ync536cla")))

(define-public crate-hl_core-0.1 (crate (name "hl_core") (vers "0.1.3") (hash "0rm3d8r9ic8sqarxvniqjp4vqk49alx1ci4v3gdp60ih282jsp8i")))

