(define-module (crates-io hl is) #:use-module (crates-io))

(define-public crate-hlist-0.1 (crate (name "hlist") (vers "0.1.0") (hash "0f9d8dh2i67ph6hjapyhwiqcwc8ghmfc8hvwgksw5dsrsn13nf0d")))

(define-public crate-hlist-0.1 (crate (name "hlist") (vers "0.1.1") (deps (list (crate-dep (name "serde") (req "^1.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0") (optional #t) (default-features #t) (kind 0)))) (hash "0jx38zg31wn3hq0m30pd38sj865iihwlrnclf34wiwmy553d7m6k") (features (quote (("with_serde" "serde" "serde_derive") ("default" "with_serde"))))))

(define-public crate-hlist-0.1 (crate (name "hlist") (vers "0.1.2") (deps (list (crate-dep (name "serde") (req "^1.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0") (optional #t) (default-features #t) (kind 0)))) (hash "059vf9jjrl9akd9mvsdm674bdyvqc7vpjqb5k8yj37ywzmigkcrz") (features (quote (("with_serde" "serde" "serde_derive") ("default" "with_serde"))))))

(define-public crate-hlist-macro-0.1 (crate (name "hlist-macro") (vers "0.1.0") (hash "0xbsywgc6z0xx9d9ppavxnhxp71ns97aibl8h1fpaa5n5vfpbx1s")))

(define-public crate-hlist2-0.0.1 (crate (name "hlist2") (vers "0.0.1") (hash "105fxi5alvfih4wsi2qn66z7ncn465li10b4rjary5cbbfka6j8d")))

(define-public crate-hlist2-0.0.2 (crate (name "hlist2") (vers "0.0.2") (hash "0vyb7kqg0q7dxyil5y9ijfs6i7znfzqkb0pxrn98h6wdbl28nk60")))

(define-public crate-hlist2-0.0.3 (crate (name "hlist2") (vers "0.0.3") (hash "11afmkf87j78gh0d68dabgf4vfb78bcsaiiwyrxvscdcdc2y2rnv")))

(define-public crate-hlist2-0.0.4 (crate (name "hlist2") (vers "0.0.4") (hash "0f1ki5715j1dw74nghjsa426dxxmriig2gqadwxxzhzvl6s3ajhd") (yanked #t)))

(define-public crate-hlist2-0.0.5 (crate (name "hlist2") (vers "0.0.5") (hash "1swsp9qb504gd96ryrdpqrz96xdfi2rrhcwh5g85ldiyha5nyikn")))

(define-public crate-hlist2-0.0.6 (crate (name "hlist2") (vers "0.0.6") (hash "0ww0ixsq7f2732pzkifzvbmzqm7dbkqiy4z2cklpz4iy3aa4bcw2")))

(define-public crate-hlist2-0.0.7 (crate (name "hlist2") (vers "0.0.7") (hash "1815rwb6rnhvi48gwsl9k9xs6g4l1yx1krjxcq52xl5w3w51bfay") (yanked #t)))

(define-public crate-hlist2-0.0.8 (crate (name "hlist2") (vers "0.0.8") (hash "06az30ahmiakkbz4s1mlhf991xgcjddfixcyfjqfgbjg8a7nh11f")))

(define-public crate-hlist2-0.0.9 (crate (name "hlist2") (vers "0.0.9") (hash "1qg864dxig0wwhrwf5c06j83zdi7l4r8r68f56sw8snm4fmjz7sv")))

(define-public crate-hlist2-0.0.10 (crate (name "hlist2") (vers "0.0.10") (hash "1zvv9k1xc0bakxw5pv1zwylczdd887qjzq3w4kwywdl0a0r70bdw")))

(define-public crate-hlist2-0.0.11 (crate (name "hlist2") (vers "0.0.11") (hash "003vda4x5330gmi31ybgcm2dfnqcdjrr9w35i5nrzk89w0lj9gcx")))

(define-public crate-hlist2-0.0.12 (crate (name "hlist2") (vers "0.0.12") (hash "0ccnyhs0gdsxdrgh7h4jga42nl7iazvdpi7k6ad0pi7sjps5wvsl")))

(define-public crate-hlist2-0.0.13 (crate (name "hlist2") (vers "0.0.13") (hash "0zjjrach3bcf38f7qsbyycykz6q6z0l5pi9s88h7g16a21psh5dv")))

(define-public crate-hlist2-0.0.14 (crate (name "hlist2") (vers "0.0.14") (hash "08c2hiffhhmrgixq2mjdw2kr8kj5xaffacm86acc650d2xwmq4x3")))

