(define-module (crates-io hl ua) #:use-module (crates-io))

(define-public crate-hlua-0.1 (crate (name "hlua") (vers "0.1.0") (deps (list (crate-dep (name "libc") (req "*") (default-features #t) (kind 0)) (crate-dep (name "lua52-sys") (req "*") (default-features #t) (kind 0)))) (hash "08ry94yp9b8q11hrgdaar20nrhd1klbfbzbfdydh60hy3p6yzzpx")))

(define-public crate-hlua-0.1 (crate (name "hlua") (vers "0.1.1") (deps (list (crate-dep (name "libc") (req "*") (default-features #t) (kind 0)) (crate-dep (name "lua52-sys") (req "*") (default-features #t) (kind 0)))) (hash "0shdlvvffq90wh9hq5zyy6gl96y9lfrajvyrz9www7l6014mnph2")))

(define-public crate-hlua-0.1 (crate (name "hlua") (vers "0.1.2") (deps (list (crate-dep (name "libc") (req "*") (default-features #t) (kind 0)) (crate-dep (name "lua52-sys") (req "*") (default-features #t) (kind 0)))) (hash "0fyb5g3km6n2lnqxzv4jqpd26qdxl963p6lgi6gbdbjf0i9p3k6j")))

(define-public crate-hlua-0.1 (crate (name "hlua") (vers "0.1.3") (deps (list (crate-dep (name "libc") (req "*") (default-features #t) (kind 0)) (crate-dep (name "lua52-sys") (req "*") (default-features #t) (kind 0)))) (hash "0g281crpi4pcb4yd14a3zcyga2dy98x042f8937iln8yz3rcjkyf")))

(define-public crate-hlua-0.1 (crate (name "hlua") (vers "0.1.4") (deps (list (crate-dep (name "libc") (req "*") (default-features #t) (kind 0)) (crate-dep (name "lua52-sys") (req "*") (default-features #t) (kind 0)))) (hash "17505arv49ndkkp9bspx2pnnnlnm22gzv5f5my93zm6d45rbrda2")))

(define-public crate-hlua-0.1 (crate (name "hlua") (vers "0.1.5") (deps (list (crate-dep (name "libc") (req "*") (default-features #t) (kind 0)) (crate-dep (name "lua52-sys") (req "*") (default-features #t) (kind 0)))) (hash "0w919g0lnnajy5sfssgfwl70rh1dxpr22408mkcshak5ajgsif9b")))

(define-public crate-hlua-0.1 (crate (name "hlua") (vers "0.1.6") (deps (list (crate-dep (name "libc") (req "*") (default-features #t) (kind 0)) (crate-dep (name "lua52-sys") (req "*") (default-features #t) (kind 0)))) (hash "1j4p62mibgpz45w0d1n1gv2f260qccf6b250dw0v2y3qx9vrpyz9")))

(define-public crate-hlua-0.1 (crate (name "hlua") (vers "0.1.7") (deps (list (crate-dep (name "libc") (req "*") (default-features #t) (kind 0)) (crate-dep (name "lua52-sys") (req "*") (default-features #t) (kind 0)))) (hash "1v2ja35qrrw6xnl8spcjgysvim9qv67xn8qp6j00hyd7vwx9y8gr")))

(define-public crate-hlua-0.1 (crate (name "hlua") (vers "0.1.8") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "lua52-sys") (req "^0.0.4") (default-features #t) (kind 0)))) (hash "1irsj682zmppsy578kf48571c9sam44vkfsi14b86fv0vkcsi7an")))

(define-public crate-hlua-0.1 (crate (name "hlua") (vers "0.1.9") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "lua52-sys") (req "^0.0.4") (default-features #t) (kind 0)))) (hash "0fha54lfn6zxn5xs0n4flcbrly0izcz3fa4ags9530nf2cnqvyxv")))

(define-public crate-hlua-0.2 (crate (name "hlua") (vers "0.2.0") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "lua52-sys") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "07k4mmp3pzwn4rd891ch5907w2yxxgnml3xz0dnqv14i7cnnn8j8")))

(define-public crate-hlua-0.3 (crate (name "hlua") (vers "0.3.0") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "lua52-sys") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "04ayyiidpqh00vl8vzm846sr6yxnx8zby7zrc855ia3a05wblpl5")))

(define-public crate-hlua-0.3 (crate (name "hlua") (vers "0.3.1") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "lua52-sys") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "1sj4f48mqhr4i9d79a01rwbhlmd6xwiviyx08mm8hxwbds4ycfwl")))

(define-public crate-hlua-0.4 (crate (name "hlua") (vers "0.4.0") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "lua52-sys") (req "^0.1.1") (default-features #t) (kind 0)))) (hash "0abk6jpkdhvqzn8qk4b5pavs3mr3kd6hgn5hs3kvc03r2g75j2wr")))

(define-public crate-hlua-0.4 (crate (name "hlua") (vers "0.4.1") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "lua52-sys") (req "^0.1.1") (default-features #t) (kind 0)))) (hash "1r2lbn1f17pkf11m8pr6piwzzlgh2fnxkgdv4hyvhm9fzwgvg7gd")))

(define-public crate-hlua-0.4 (crate (name "hlua") (vers "0.4.2") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "lua52-sys") (req "^0.1.1") (default-features #t) (kind 0)))) (hash "0gd0r1j6h41kpbkj3xavpr51zafm5c3vl616iwd5hqcgpljbc5ci")))

(define-public crate-hlua-badtouch-0.4 (crate (name "hlua-badtouch") (vers "0.4.2") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "lua52-sys") (req "^0.1.1") (default-features #t) (kind 0)))) (hash "0xsrplzi9m2kb7yrk5zmf79qjqhx0jr0w25vh81xnnxb0q3qll6p")))

(define-public crate-hlua_master-0.1 (crate (name "hlua_master") (vers "0.1.8") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "lua52-sys") (req "^0.0.4") (default-features #t) (kind 0)))) (hash "1ki8g5qbs811vh2xbnk47b0bw2jdfllbyxciq729c80q330hza1a") (yanked #t)))

