(define-module (crates-io hl vm) #:use-module (crates-io))

(define-public crate-hlvm-0.4 (crate (name "hlvm") (vers "0.4.1") (deps (list (crate-dep (name "hlvm_runtime") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "1bn95m9rda9crjpam2ihl20704iw4ahhibn3gxv4vzpcpicljmz2")))

(define-public crate-hlvm-0.4 (crate (name "hlvm") (vers "0.4.2") (deps (list (crate-dep (name "hlvm_runtime") (req "^0.1.5") (default-features #t) (kind 0)))) (hash "0jiv4r5acqd5zh9dc7mvazyafc92r4v1k8jkq26hx4dqz3sw9wj5")))

(define-public crate-hlvm-0.5 (crate (name "hlvm") (vers "0.5.0") (deps (list (crate-dep (name "hlvm_runtime") (req "^0.2.0") (default-features #t) (kind 0)))) (hash "075wdl8vr98sqbk8mh9qwckds57mxxyyqx1dk1syhgg4bwi5fj87")))

(define-public crate-hlvm-0.5 (crate (name "hlvm") (vers "0.5.1") (deps (list (crate-dep (name "hlvm_runtime") (req "^0.2.1") (default-features #t) (kind 0)))) (hash "0mn5wakc77gcfyavv64x43n7chn7y9znjhi2fpqdmh7xv292gi5h")))

(define-public crate-hlvm-0.5 (crate (name "hlvm") (vers "0.5.2") (deps (list (crate-dep (name "hlvm_runtime") (req "^0.2.2") (default-features #t) (kind 0)))) (hash "08ghf59cxmjhcnq0rd3qhpsaimqwlfqn7av1hj6xx2xc8pl45bl0")))

(define-public crate-hlvm-0.5 (crate (name "hlvm") (vers "0.5.3") (deps (list (crate-dep (name "hlvm_runtime") (req "^0.2.2") (default-features #t) (kind 0)))) (hash "1h2ldq9l2r7k5gqhjxvkbb98p3f5aay53lqp2711n1i8f0pxcx95")))

(define-public crate-hlvm-0.6 (crate (name "hlvm") (vers "0.6.0") (deps (list (crate-dep (name "hlvm_runtime") (req "^0.3.0") (default-features #t) (kind 0)))) (hash "0646m54vsbcrnv1c3mk7r7hmi3w7by3qv8mfv442xlkmf0056c1a")))

(define-public crate-hlvm-0.7 (crate (name "hlvm") (vers "0.7.0") (deps (list (crate-dep (name "hlvm_runtime") (req "^0.4.0") (default-features #t) (kind 0)))) (hash "0rcna5dfvzvv5bxb7g0lisjy6xpz9rn708j5zq9sp2z1v4iqjh4s")))

(define-public crate-hlvm-0.7 (crate (name "hlvm") (vers "0.7.1") (deps (list (crate-dep (name "hlvm_runtime") (req "^0.4.0") (default-features #t) (kind 0)))) (hash "1kzy5zf8mkvirs9p1a4xy7xkv9q0q1fymkbmz1jxhjw3ykz2h5vz")))

(define-public crate-hlvm-0.7 (crate (name "hlvm") (vers "0.7.2") (deps (list (crate-dep (name "hlvm_runtime") (req "^0.4.1") (default-features #t) (kind 0)))) (hash "0p1ji71agzghr1kp5yys9mspwgwz81nc9l6mi7szachz44ai74fn")))

(define-public crate-hlvm-0.8 (crate (name "hlvm") (vers "0.8.0") (deps (list (crate-dep (name "hlvm_runtime") (req "^0.5.0") (default-features #t) (kind 0)))) (hash "0n4nffwbfm0yi9njb27sx3g24nzyi00rrj1h3795w1hnkjdhb4dm")))

(define-public crate-hlvm-0.9 (crate (name "hlvm") (vers "0.9.0") (deps (list (crate-dep (name "hlvm_runtime") (req "^0.6.0") (default-features #t) (kind 0)))) (hash "1l454wy1871ldznivz4pvr5zr51vm65jrhli6wgi0g3pz5xzrs21")))

(define-public crate-hlvm-0.10 (crate (name "hlvm") (vers "0.10.0") (deps (list (crate-dep (name "hlvm_runtime") (req "^0.7.0") (default-features #t) (kind 0)))) (hash "0c3ila5zywwjq0avgk3s5z4q1q44a5i5156a2s433p07hgpj085q")))

(define-public crate-hlvm_runtime-0.1 (crate (name "hlvm_runtime") (vers "0.1.1") (deps (list (crate-dep (name "bigdecimal") (req "^0.0.15") (default-features #t) (kind 0)))) (hash "1a1w54iwmfcv5wagppa0bwl0gmsdv6cngy9bxq29npaf472az8lr")))

(define-public crate-hlvm_runtime-0.1 (crate (name "hlvm_runtime") (vers "0.1.2") (deps (list (crate-dep (name "bigdecimal") (req "^0.0.15") (default-features #t) (kind 0)))) (hash "1k99qv47hcz982s6xq34pxr6cc0h0dj5h8kax1xsx8l556qkijna")))

(define-public crate-hlvm_runtime-0.1 (crate (name "hlvm_runtime") (vers "0.1.3") (deps (list (crate-dep (name "bigdecimal") (req "^0.0.15") (default-features #t) (kind 0)))) (hash "1wd94lxr7qbpazxsx4vkqwmw6lsb4hd0bq2xzk71dkibzasvm5lf")))

(define-public crate-hlvm_runtime-0.1 (crate (name "hlvm_runtime") (vers "0.1.5") (deps (list (crate-dep (name "bigdecimal") (req "^0.0.15") (default-features #t) (kind 0)))) (hash "05k8z3f1i0xdxgq20iwppd9j1alfgqbd7r6w92f8575k8wdx00rl")))

(define-public crate-hlvm_runtime-0.1 (crate (name "hlvm_runtime") (vers "0.1.6") (deps (list (crate-dep (name "bigdecimal") (req "^0.0.15") (default-features #t) (kind 0)))) (hash "01sy8spfr1p7zv1vp8306z3mnygq9a9ar33r86cmlcjz84bagdvv")))

(define-public crate-hlvm_runtime-0.2 (crate (name "hlvm_runtime") (vers "0.2.0") (deps (list (crate-dep (name "bigdecimal") (req "^0.0.15") (default-features #t) (kind 0)))) (hash "16ggm03009ijvnqwm5qpkiwwd6dy6sqxpv0p1vqb00n6p5sx4sc8")))

(define-public crate-hlvm_runtime-0.2 (crate (name "hlvm_runtime") (vers "0.2.1") (deps (list (crate-dep (name "bigdecimal") (req "^0.0.15") (default-features #t) (kind 0)))) (hash "1ml0zf49yr1bhmib15n0g99v5ws5lfczhxhmqslkbvdrblnl6ra8")))

(define-public crate-hlvm_runtime-0.2 (crate (name "hlvm_runtime") (vers "0.2.2") (deps (list (crate-dep (name "bigdecimal") (req "^0.0.15") (default-features #t) (kind 0)))) (hash "19m7n6pq4fymvl0i7kcp40b1nv0gs7g6437lzwz4767frcg32d50")))

(define-public crate-hlvm_runtime-0.3 (crate (name "hlvm_runtime") (vers "0.3.0") (deps (list (crate-dep (name "bigdecimal") (req "^0.0.15") (default-features #t) (kind 0)))) (hash "0c1nx1dfz197fhyvzrq49p83nfa18f5gq2zp9pq71r00091pn33c")))

(define-public crate-hlvm_runtime-0.4 (crate (name "hlvm_runtime") (vers "0.4.0") (deps (list (crate-dep (name "bigdecimal") (req "^0.0.15") (default-features #t) (kind 0)))) (hash "00g5xaf6ahniiiafg8r0nqwzqb3rdhx3ci76nwjr237ncr4rhdbd")))

(define-public crate-hlvm_runtime-0.4 (crate (name "hlvm_runtime") (vers "0.4.1") (deps (list (crate-dep (name "bigdecimal") (req "^0.0.15") (default-features #t) (kind 0)))) (hash "1w1fp6y47shn43kv1qw4aqv2r2d0wlh7b4kzs8ghb5fx9qf75js3")))

(define-public crate-hlvm_runtime-0.5 (crate (name "hlvm_runtime") (vers "0.5.0") (deps (list (crate-dep (name "decimal") (req "^2.0.4") (default-features #t) (kind 0)))) (hash "0yvv05c1xq89r0sn53q20mfrn94bs31cvazmkzzg9vj52fvmvcyj")))

(define-public crate-hlvm_runtime-0.6 (crate (name "hlvm_runtime") (vers "0.6.0") (deps (list (crate-dep (name "decimal") (req "^2.0.4") (default-features #t) (kind 0)))) (hash "1krlszd77a1r4qmn1l1j0lfqnq2rhz55ixf5879d8vw7fwn7ljxq")))

(define-public crate-hlvm_runtime-0.7 (crate (name "hlvm_runtime") (vers "0.7.0") (deps (list (crate-dep (name "decimal") (req "^2.0.4") (default-features #t) (kind 0)))) (hash "0sfmd8dr0rah9vnskwc9i62q82lph3ihq758504qa4zkfcdkic84")))

