(define-module (crates-io hl ad) #:use-module (crates-io))

(define-public crate-hladikes-hello-world-demo-crate-0.1 (crate (name "hladikes-hello-world-demo-crate") (vers "0.1.0") (hash "1d1hgnimn8r6bi75hj4802wrkjd4ybjzr4m0947fqfry5wk5q5wz")))

(define-public crate-hladikes_test_lib-0.1 (crate (name "hladikes_test_lib") (vers "0.1.0") (hash "005qq61ns6awcwnnhdc58cdg9bw5naj3d1692c5xv1lyx33v7jmh")))

(define-public crate-hladikes_test_lib-0.1 (crate (name "hladikes_test_lib") (vers "0.1.1") (hash "00cjnwv57mc2jrm4n23d5j894p78z94pn5pbfp9g4lvm3ah86jc1")))

(define-public crate-hladikes_test_lib-0.1 (crate (name "hladikes_test_lib") (vers "0.1.2") (hash "048g7iqxrdha8dbccsdh2qjiv4lwar9n8zkz25pgr8y0psbpdnbl")))

