(define-module (crates-io hl s-) #:use-module (crates-io))

(define-public crate-hls-playlist-0.1 (crate (name "hls-playlist") (vers "0.1.0") (deps (list (crate-dep (name "chrono") (req "^0.4.38") (default-features #t) (kind 0)) (crate-dep (name "hex") (req "^0.4.3") (default-features #t) (kind 0)) (crate-dep (name "rstest") (req "^0.19.0") (default-features #t) (kind 2)) (crate-dep (name "serde") (req "^1.0.201") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.117") (optional #t) (default-features #t) (kind 0)))) (hash "0ikrhsvghd170n1594ga7s3ppm5jky7xk9jpn3yq820jsdc1751s") (v 2) (features2 (quote (("steering-manifest" "dep:serde" "dep:serde_json"))))))

