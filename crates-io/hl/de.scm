(define-module (crates-io hl de) #:use-module (crates-io))

(define-public crate-hldemo-0.1 (crate (name "hldemo") (vers "0.1.0") (deps (list (crate-dep (name "error-chain") (req "^0.11") (default-features #t) (kind 2)) (crate-dep (name "memmap") (req "^0.5") (default-features #t) (kind 2)) (crate-dep (name "nom") (req "^3.2") (features (quote ("verbose-errors"))) (default-features #t) (kind 0)) (crate-dep (name "quick-error") (req "^1") (default-features #t) (kind 0)))) (hash "0bc9q30px0649ipv967mwbq8d78dhya2ga612y9ybdkna5066mds")))

(define-public crate-hldemo-0.2 (crate (name "hldemo") (vers "0.2.0") (deps (list (crate-dep (name "error-chain") (req "^0.11") (default-features #t) (kind 0)) (crate-dep (name "memmap") (req "^0.5") (default-features #t) (kind 2)) (crate-dep (name "nom") (req "^3.2") (features (quote ("verbose-errors"))) (default-features #t) (kind 0)) (crate-dep (name "quick-error") (req "^1") (default-features #t) (kind 0)))) (hash "0s178i904i7y72kriqdpfc86zd3iypfj06a5wpd9xxim0z32jh2x")))

(define-public crate-hldemo-0.3 (crate (name "hldemo") (vers "0.3.0") (deps (list (crate-dep (name "error-chain") (req "^0.11") (default-features #t) (kind 0)) (crate-dep (name "memmap") (req "^0.5") (default-features #t) (kind 2)) (crate-dep (name "nom") (req "^3.2") (features (quote ("verbose-errors"))) (default-features #t) (kind 0)) (crate-dep (name "quick-error") (req "^1") (default-features #t) (kind 0)))) (hash "10c7ld5amvxlidzskrrsfyzm7xn11klan7m1cypf64c71s408lhc")))

