(define-module (crates-io ag le) #:use-module (crates-io))

(define-public crate-aglet-0.1 (crate (name "aglet") (vers "0.1.0") (deps (list (crate-dep (name "bitflags") (req "^1.3.2") (default-features #t) (kind 0)))) (hash "1k7vncf86jhcx4f1w9mgsdk0ys0z94hqiwibd3d98frxhi1b76mc")))

(define-public crate-aglet-0.1 (crate (name "aglet") (vers "0.1.1") (deps (list (crate-dep (name "bitflags") (req "^1.3.2") (default-features #t) (kind 0)))) (hash "0r9dvwswqdsh2s07glsk3v9kcvbi28n1h8rirzd196fhmddk6fy1")))

(define-public crate-aglet-0.2 (crate (name "aglet") (vers "0.2.0") (deps (list (crate-dep (name "bitflags") (req "^1.3.2") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.145") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)))) (hash "0d84k7hp8fyzk9q5npwfn900z83xybg3nahzslzy0cg37p4ydy97") (features (quote (("default" "serde"))))))

(define-public crate-aglet-0.3 (crate (name "aglet") (vers "0.3.0") (deps (list (crate-dep (name "bitflags") (req "^1.3.2") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.145") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)))) (hash "0zzlkdsl89w86mc49h5yz9fshq6j4pcgc8ndays32hlh0jnbs4ni") (features (quote (("default" "serde"))))))

(define-public crate-aglet-0.3 (crate (name "aglet") (vers "0.3.1") (deps (list (crate-dep (name "bitflags") (req "^1.3.2") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.145") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)))) (hash "0xzqdb7nziggig6lj3b5pi5sj2w4rx22bmdrzdq7fykqq4flb00m") (features (quote (("default" "serde"))))))

(define-public crate-aglet-0.3 (crate (name "aglet") (vers "0.3.2") (deps (list (crate-dep (name "bitflags") (req "^1.3.2") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.145") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)))) (hash "0czrz3z364rjhhdcmafgv1frbjshgm011jq3wdp9pc1r03pskc5d") (features (quote (("default" "serde"))))))

(define-public crate-aglet-0.3 (crate (name "aglet") (vers "0.3.3") (deps (list (crate-dep (name "bitflags") (req "^1.3.2") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.145") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)))) (hash "0vmk2sb9n997sqap77y8hbslh21zwa6w9bkxznrrh02xgiyd3yca") (features (quote (("default" "serde"))))))

(define-public crate-aglet-0.3 (crate (name "aglet") (vers "0.3.4") (deps (list (crate-dep (name "bitflags") (req "^1.3.2") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.145") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)))) (hash "0infmn9dkclygkrlc2radx4hzhdribb6z4mhsfhd3adw82my7dis") (features (quote (("default" "serde"))))))

(define-public crate-aglet-0.4 (crate (name "aglet") (vers "0.4.0") (deps (list (crate-dep (name "enumflags2") (req "^0.7.5") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.145") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)))) (hash "02jamdyaxd35zdv5wzvhfwz8506dznbs79xvv12wpa9vsdmd1pbk") (features (quote (("default" "serde")))) (v 2) (features2 (quote (("serde" "dep:serde" "enumflags2/serde"))))))

(define-public crate-aglet-0.5 (crate (name "aglet") (vers "0.5.0") (deps (list (crate-dep (name "enumflags2") (req "^0.7.5") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.145") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)))) (hash "0db5cdrwcpca2yl2dhvy3jh0q8c1fvls36wcygcfzn52knfg11k3") (features (quote (("default" "serde")))) (v 2) (features2 (quote (("serde" "dep:serde" "enumflags2/serde"))))))

(define-public crate-aglet-0.5 (crate (name "aglet") (vers "0.5.1") (deps (list (crate-dep (name "enumflags2") (req "^0.7.5") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.145") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)))) (hash "0xymfbbnj8c73nnmjj6zaiv2bfx1l7qwsn3225zswpvvcd9r6j42") (features (quote (("default" "serde")))) (v 2) (features2 (quote (("serde" "dep:serde" "enumflags2/serde"))))))

(define-public crate-aglet-0.5 (crate (name "aglet") (vers "0.5.2") (deps (list (crate-dep (name "enumflags2") (req "^0.7.5") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.145") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)))) (hash "1m0rz8wkzfd95c4kk0wcab8qaq186as58i85bcplb6yr3bma6bg7") (features (quote (("default" "serde")))) (v 2) (features2 (quote (("serde" "dep:serde" "enumflags2/serde"))))))

