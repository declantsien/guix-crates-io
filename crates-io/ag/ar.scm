(define-module (crates-io ag ar) #:use-module (crates-io))

(define-public crate-agar-0.0.1 (crate (name "agar") (vers "0.0.1") (deps (list (crate-dep (name "lazy_static") (req ">=1.4.0, <2.0.0") (default-features #t) (kind 0)))) (hash "0r0n3nwx80yac583bplvmzhhd8b9dnqz5fm2757xs6gjyvm69fly")))

(define-public crate-agar-0.0.2 (crate (name "agar") (vers "0.0.2") (hash "02ag6h69dns5gvsn9mywd4gibm3z5bs7ng3mbvdd70fxfh6klpjm")))

