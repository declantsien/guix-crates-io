(define-module (crates-io ag ra) #:use-module (crates-io))

(define-public crate-agram-1 (crate (name "agram") (vers "1.0.0") (deps (list (crate-dep (name "lazy_static") (req "^1.4") (default-features #t) (kind 0)) (crate-dep (name "libflate") (req "^1.1") (default-features #t) (kind 0)) (crate-dep (name "libflate") (req "^1.1") (default-features #t) (kind 1)))) (hash "1px6116r3p2rk3s1yyb1cwzcai9qqy4yrm5lsimar8ia0sx0bd6k")))

(define-public crate-agram-1 (crate (name "agram") (vers "1.0.1") (deps (list (crate-dep (name "lazy_static") (req "^1.4") (default-features #t) (kind 0)) (crate-dep (name "libflate") (req "^1.1") (default-features #t) (kind 0)) (crate-dep (name "libflate") (req "^1.1") (default-features #t) (kind 1)))) (hash "05ibn0crq8bsqh2hx1mwq8i988l9pyhnm999rq2fh0lbn21pfa30")))

