(define-module (crates-io ag vt) #:use-module (crates-io))

(define-public crate-agvtf-1 (crate (name "agvtf") (vers "1.0.0") (deps (list (crate-dep (name "chrono") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "scraper") (req "^0.12") (default-features #t) (kind 0)) (crate-dep (name "ureq") (req "^2") (default-features #t) (kind 0)))) (hash "1dnfd13pcvrvh9dln9qc2cx85pbi86iqmqhlhrnax7jvs02wf0kg")))

(define-public crate-agvtf-1 (crate (name "agvtf") (vers "1.0.1") (deps (list (crate-dep (name "chrono") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "scraper") (req "^0.12") (default-features #t) (kind 0)) (crate-dep (name "ureq") (req "^2") (default-features #t) (kind 0)))) (hash "1ckb4jrl74q7hg3y39z1d23mj8912xc9mq6rn7jvlnlx1sa8xv9x")))

(define-public crate-agvtf-1 (crate (name "agvtf") (vers "1.0.2") (deps (list (crate-dep (name "chrono") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "scraper") (req "^0.12") (default-features #t) (kind 0)) (crate-dep (name "ureq") (req "^2") (default-features #t) (kind 0)))) (hash "0cw1146k9y7d62ylswvsj2hwipr5ph4gzcyws1ka1sqkndrqq7id")))

(define-public crate-agvtf-1 (crate (name "agvtf") (vers "1.0.3") (deps (list (crate-dep (name "chrono") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "scraper") (req "^0.12") (default-features #t) (kind 0)) (crate-dep (name "ureq") (req "^2") (kind 0)))) (hash "0hlsw1gfm12jbdlm0hnvs1jj3xvwjx3lg847i6av1dmhikxr3azc")))

(define-public crate-agvtf-1 (crate (name "agvtf") (vers "1.1.0") (deps (list (crate-dep (name "chrono") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "scraper") (req "^0.12") (default-features #t) (kind 0)) (crate-dep (name "ureq") (req "^2") (kind 0)))) (hash "0as5if0fxf73nyc15v039bcy4ld4pxbcgwkzs5rz3pl1zc6hyyff")))

(define-public crate-agvtf-1 (crate (name "agvtf") (vers "1.1.1") (deps (list (crate-dep (name "chrono") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "scraper") (req "^0.13") (default-features #t) (kind 0)) (crate-dep (name "ureq") (req "^2.5") (kind 0)))) (hash "1x1j0ad6mwa0kls5r3vgqpy690p01lp2xx7k8gh7rh0w8h2y9sva")))

(define-public crate-agvtf-1 (crate (name "agvtf") (vers "1.1.2") (deps (list (crate-dep (name "chrono") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "scraper") (req "^0.14") (default-features #t) (kind 0)) (crate-dep (name "ureq") (req "^2.5") (kind 0)))) (hash "0dz05cbh8mmhmldf16yji9vr91gvd0gflx1ysz1qybdx60jb0xi6")))

(define-public crate-agvtf-1 (crate (name "agvtf") (vers "1.1.3") (deps (list (crate-dep (name "chrono") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "scraper") (req "^0.15") (default-features #t) (kind 0)) (crate-dep (name "ureq") (req "^2.5") (kind 0)))) (hash "0hcbwyc10010ivwqcfcjyqdkcdbjxiqai4rh4yspkgn7mv5f20dm")))

(define-public crate-agvtf-1 (crate (name "agvtf") (vers "1.1.4") (deps (list (crate-dep (name "chrono") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "scraper") (req "^0.16") (default-features #t) (kind 0)) (crate-dep (name "ureq") (req "^2.5") (kind 0)))) (hash "009rbpb9wbckcminnfnrf7519vsnwwhnqz7q0d6nhr334d9ds2yn")))

(define-public crate-agvtf-1 (crate (name "agvtf") (vers "1.1.5") (deps (list (crate-dep (name "chrono") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "scraper") (req "^0.17") (default-features #t) (kind 0)) (crate-dep (name "ureq") (req "^2.5") (kind 0)))) (hash "1zb52vaj808z0licw8i37g7ibrxq046j8yh95rh69v46dca0j6yl")))

(define-public crate-agvtf-1 (crate (name "agvtf") (vers "1.1.6") (deps (list (crate-dep (name "chrono") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "scraper") (req "^0.18") (default-features #t) (kind 0)) (crate-dep (name "ureq") (req "^2.5") (kind 0)))) (hash "0zb07g36lnjk6znpyw5l1mx3ijzprr4d54yq3kgjfixwsngaa2l4")))

