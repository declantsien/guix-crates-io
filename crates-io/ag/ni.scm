(define-module (crates-io ag ni) #:use-module (crates-io))

(define-public crate-agni-0.1 (crate (name "agni") (vers "0.1.0") (hash "1v8dvflbwqpxc76din1ipakv687nlanjh88rnwc4ns8lp5qb1j8f") (yanked #t)))

(define-public crate-agni-0.1 (crate (name "agni") (vers "0.1.1") (deps (list (crate-dep (name "clap") (req "^2.32.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "num_cpus") (req "^1.8.0") (default-features #t) (kind 0)) (crate-dep (name "pest") (req "^2.0.2") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "pest_derive") (req "^2.0.1") (optional #t) (default-features #t) (kind 0)))) (hash "0c0vwv8cq44iw3vby4z73h2cry8bqd8w70lzk8nalb7zmnbjb90w") (features (quote (("io_pbrt" "pest" "pest_derive") ("default" "cli" "io_pbrt") ("cli" "clap"))))))

(define-public crate-agni-0.1 (crate (name "agni") (vers "0.1.2") (deps (list (crate-dep (name "clap") (req "^2.32.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "num_cpus") (req "^1.8.0") (default-features #t) (kind 0)) (crate-dep (name "pest") (req "^2.0.2") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "pest_derive") (req "^2.0.1") (optional #t) (default-features #t) (kind 0)))) (hash "118y0l9qxjjq1cfd7229k1cjb0775pgs525x9b60qy8z3i4j2adz") (features (quote (("io_pbrt" "pest" "pest_derive") ("default" "cli" "io_pbrt") ("cli" "clap"))))))

