(define-module (crates-io ag cw) #:use-module (crates-io))

(define-public crate-agcwd-0.1 (crate (name "agcwd") (vers "0.1.0") (deps (list (crate-dep (name "anyhow") (req "^1") (default-features #t) (kind 2)) (crate-dep (name "png") (req "^0.17") (default-features #t) (kind 2)) (crate-dep (name "structopt") (req "^0.3") (default-features #t) (kind 2)))) (hash "1qnghr9fcmv3mhxgvpn249wns2jbw2nsajyrb3niy027dk6kqyf9")))

(define-public crate-agcwd-0.2 (crate (name "agcwd") (vers "0.2.0") (deps (list (crate-dep (name "anyhow") (req "^1") (default-features #t) (kind 2)) (crate-dep (name "png") (req "^0.17") (default-features #t) (kind 2)) (crate-dep (name "structopt") (req "^0.3") (default-features #t) (kind 2)))) (hash "0r67mqfsgwa2axzi95frxydfis9v64r41n8pav3c0bk9sh8vcjgx")))

(define-public crate-agcwd-0.3 (crate (name "agcwd") (vers "0.3.0") (deps (list (crate-dep (name "anyhow") (req "^1") (default-features #t) (kind 2)) (crate-dep (name "png") (req "^0.17") (default-features #t) (kind 2)) (crate-dep (name "structopt") (req "^0.3") (default-features #t) (kind 2)))) (hash "0w6cl5h5vmphb84vvnr4by380gisfslwcgn2c3xhnf6jscqg6jw5")))

(define-public crate-agcwd-0.3 (crate (name "agcwd") (vers "0.3.1") (deps (list (crate-dep (name "anyhow") (req "^1") (default-features #t) (kind 2)) (crate-dep (name "png") (req "^0.17") (default-features #t) (kind 2)) (crate-dep (name "structopt") (req "^0.3") (default-features #t) (kind 2)))) (hash "075j57kfi7z1nsfbr10zqg6hcwis3ngih4nm48zf30ay4jl4r77a")))

