(define-module (crates-io ag g_) #:use-module (crates-io))

(define-public crate-agg_es_test_helpers-0.1 (crate (name "agg_es_test_helpers") (vers "0.1.0") (hash "0b8b382kzi19qhs2gdc1313hv8hky1glbf5pr6gg4h2h6qvzyn2r")))

(define-public crate-agg_es_testcase-0.1 (crate (name "agg_es_testcase") (vers "0.1.0") (hash "0hwg5hvz486m6gr9xilqyvbj2dah6a48yk2wmh2nclp5prc1bqik") (yanked #t)))

(define-public crate-agg_es_testcase-0.1 (crate (name "agg_es_testcase") (vers "0.1.1") (hash "1ql31ljh2ly1xygl4arpgl5nbyd13rhbbjrdl7dn6cpp205b27bc") (yanked #t)))

(define-public crate-agg_es_testhelper-0.1 (crate (name "agg_es_testhelper") (vers "0.1.2") (hash "1m4267ckgsbrkvh3l3h9mq56jrws4nm0793kh6k94qnjw1x5swkq") (yanked #t)))

(define-public crate-agg_es_testhelper-0.1 (crate (name "agg_es_testhelper") (vers "0.1.3") (hash "1ac2f2qh4645fglmx2z7kl3c57z9l0q09az35f3zm4xsrdjfh9a5") (yanked #t)))

