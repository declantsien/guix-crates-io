(define-module (crates-io ag _f) #:use-module (crates-io))

(define-public crate-ag_file_system_scanner-0.1 (crate (name "ag_file_system_scanner") (vers "0.1.0") (deps (list (crate-dep (name "chrono") (req "^0.4.23") (default-features #t) (kind 0)))) (hash "1cj5php2zazpnns5ax0xvpi220afy7yqh6k53qp18skjbcd7h7wk") (yanked #t)))

(define-public crate-ag_file_system_scanner-0.1 (crate (name "ag_file_system_scanner") (vers "0.1.1") (deps (list (crate-dep (name "chrono") (req "^0.4.23") (default-features #t) (kind 0)))) (hash "0pbgp68ygmnkfh1iimqsm8wpm51y8gc0g7alxlybwsmd5s865nzc")))

(define-public crate-ag_file_system_scanner-0.1 (crate (name "ag_file_system_scanner") (vers "0.1.2") (deps (list (crate-dep (name "chrono") (req "^0.4.23") (default-features #t) (kind 0)))) (hash "0awvm7miv42b8hn14kdwsgdclbkl3cgb3xci82gx5y66mx2h0642")))

(define-public crate-ag_file_system_scanner-0.1 (crate (name "ag_file_system_scanner") (vers "0.1.3") (deps (list (crate-dep (name "chrono") (req "^0.4.23") (default-features #t) (kind 0)))) (hash "0ik36gg7mdcvc06pzcjxhqsrhrgqx5kq4af35fq6vim01hcvgais")))

