(define-module (crates-io ag -l) #:use-module (crates-io))

(define-public crate-ag-lcd-0.1 (crate (name "ag-lcd") (vers "0.1.0") (deps (list (crate-dep (name "embedded-hal") (req "^0.2.3") (default-features #t) (kind 0)) (crate-dep (name "panic-halt") (req "^0.2.0") (default-features #t) (kind 2)) (crate-dep (name "ufmt") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "179r2lhbki62vbk92l0n0d62z08x8l7rbmln7z3v7gs1l6crd7pm")))

(define-public crate-ag-lcd-0.1 (crate (name "ag-lcd") (vers "0.1.1") (deps (list (crate-dep (name "embedded-hal") (req "^0.2.3") (default-features #t) (kind 0)) (crate-dep (name "panic-halt") (req "^0.2.0") (default-features #t) (kind 2)) (crate-dep (name "ufmt") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "00vgjv91yl1vn5bmkjdjhnr56na7rqrh8fn2v2fb3sw73mmiydi1")))

(define-public crate-ag-lcd-0.1 (crate (name "ag-lcd") (vers "0.1.2") (deps (list (crate-dep (name "embedded-hal") (req "^0.2.3") (default-features #t) (kind 0)) (crate-dep (name "panic-halt") (req "^0.2.0") (default-features #t) (kind 2)) (crate-dep (name "ufmt") (req "^0.1.0") (optional #t) (default-features #t) (kind 0)))) (hash "1wnnaf8pr5a04gppilckyv199qm7crl5b27xf8l33yjxbdgljwc5")))

(define-public crate-ag-lcd-0.2 (crate (name "ag-lcd") (vers "0.2.0") (deps (list (crate-dep (name "embedded-hal") (req "^0.2.3") (default-features #t) (kind 0)) (crate-dep (name "port-expander") (req "^0.3") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "shared-bus") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "ufmt") (req "^0.1.0") (optional #t) (default-features #t) (kind 0)))) (hash "1z6d0ksj737fyrzkn5cmzhnimy6jmh1mhpki5plghh56iwca6zzy") (features (quote (("i2c" "port-expander") ("avr-hal"))))))

