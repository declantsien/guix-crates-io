(define-module (crates-io ag a8) #:use-module (crates-io))

(define-public crate-aga8-0.1 (crate (name "aga8") (vers "0.1.0") (deps (list (crate-dep (name "criterion") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "winres") (req "^0.1") (default-features #t) (kind 1)))) (hash "05pvl1wb4dvq50g63a3qin9qxvdqqjkxgcn2j9r40cqz3a0cnanp") (yanked #t)))

(define-public crate-aga8-0.0.0 (crate (name "aga8") (vers "0.0.0") (deps (list (crate-dep (name "criterion") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "winres") (req "^0.1") (default-features #t) (kind 1)))) (hash "1wwqlkm1h06037ni20x3iasc86cw6y2gplsp516yf6f098pg80an")))

(define-public crate-aga8-0.1 (crate (name "aga8") (vers "0.1.1") (deps (list (crate-dep (name "criterion") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "winres") (req "^0.1") (default-features #t) (kind 1)))) (hash "0yqaqkiljfyk4jirmsdnbbdj180xf4f4fjb6in63g1h24l8czfxy") (features (quote (("extern"))))))

(define-public crate-aga8-0.1 (crate (name "aga8") (vers "0.1.2") (deps (list (crate-dep (name "criterion") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "winres") (req "^0.1") (default-features #t) (kind 1)))) (hash "18m3kpc01gvmy0zwx0f2hsd183adz5ihxpcvf32p628n1h7ycq18") (features (quote (("extern"))))))

(define-public crate-aga8-0.2 (crate (name "aga8") (vers "0.2.0") (deps (list (crate-dep (name "criterion") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "winres") (req "^0.1") (default-features #t) (kind 1)))) (hash "0dan8rg0yiii87ib9s0mmlgjl4w32h5ln7dj8lbsx6fgp82csh4j") (features (quote (("extern"))))))

(define-public crate-aga8-0.3 (crate (name "aga8") (vers "0.3.0") (deps (list (crate-dep (name "cbindgen") (req "^0.24.3") (default-features #t) (kind 1)) (crate-dep (name "criterion") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "winres") (req "^0.1") (default-features #t) (kind 1)))) (hash "0nsjkh7v7bl9m5dp64p0k69qhbf940i4hj5jkvwiwla463fm88g3") (features (quote (("extern"))))))

(define-public crate-aga8-0.4 (crate (name "aga8") (vers "0.4.0") (deps (list (crate-dep (name "cbindgen") (req "^0.24.3") (default-features #t) (kind 1)) (crate-dep (name "criterion") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 2)) (crate-dep (name "winres") (req "^0.1") (default-features #t) (kind 1)))) (hash "08ic1hq5jiw033chrwhpnlrjm8pdycxq2w0z3jzvk0ama2dlnmh7") (features (quote (("extern"))))))

