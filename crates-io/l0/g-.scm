(define-module (crates-io l0 g-) #:use-module (crates-io))

(define-public crate-l0g-macros-1 (crate (name "l0g-macros") (vers "1.0.0") (deps (list (crate-dep (name "proc-macro-error") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2") (features (quote ("derive" "parsing" "printing" "proc-macro"))) (kind 0)))) (hash "1sigpg2a405aqrl521zw3hm8vpzgbcyx5gkxc1mmndn1gfgarmgv") (features (quote (("log") ("defmt"))))))

