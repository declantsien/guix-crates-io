(define-module (crates-io fc s_) #:use-module (crates-io))

(define-public crate-fcs_rs-0.1 (crate (name "fcs_rs") (vers "0.1.0") (deps (list (crate-dep (name "byteorder") (req "^1.5.0") (default-features #t) (kind 0)) (crate-dep (name "polars") (req "^0.39.2") (features (quote ("lazy"))) (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.58") (default-features #t) (kind 0)))) (hash "0jkkcnlr0slxnc3bnncnms22hf0x2rjd9nml2dvj9nids1548z9w")))

