(define-module (crates-io fc nt) #:use-module (crates-io))

(define-public crate-fcnt-0.2 (crate (name "fcnt") (vers "0.2.0") (deps (list (crate-dep (name "clap") (req "^4.0.29") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "flume") (req "^0.10.14") (default-features #t) (kind 0)) (crate-dep (name "num_cpus") (req "^1.15.0") (default-features #t) (kind 0)))) (hash "1vz7j02d5gixr6rvkcmry7y1f56f8y7a4hm1kc81c3nq94irzb2r") (yanked #t)))

(define-public crate-fcnt-0.2 (crate (name "fcnt") (vers "0.2.1") (deps (list (crate-dep (name "clap") (req "^4.0.29") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "flume") (req "^0.10.14") (default-features #t) (kind 0)) (crate-dep (name "num_cpus") (req "^1.15.0") (default-features #t) (kind 0)))) (hash "1yilacr35dm7iplp1795fr2b6xrb841b7nlx50z8rr5kvhbcqjri") (yanked #t)))

(define-public crate-fcnt-0.2 (crate (name "fcnt") (vers "0.2.2") (deps (list (crate-dep (name "clap") (req "^4.0.29") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "flume") (req "^0.10.14") (default-features #t) (kind 0)) (crate-dep (name "num_cpus") (req "^1.15.0") (default-features #t) (kind 0)))) (hash "10b0009y83rivxlxc0h03758hrx09hi0z8h9fn455f9dmfx7bw6b") (yanked #t)))

(define-public crate-fcnt-0.2 (crate (name "fcnt") (vers "0.2.3") (deps (list (crate-dep (name "clap") (req "^4.0.29") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "flume") (req "^0.10.14") (default-features #t) (kind 0)) (crate-dep (name "num_cpus") (req "^1.15.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.7.1") (default-features #t) (kind 0)))) (hash "0rlzdyqs8m0r8n5sipx1ryaws71fgrvmnh4yaj7hrk3jj3w25br8")))

(define-public crate-fcnt-0.2 (crate (name "fcnt") (vers "0.2.5") (deps (list (crate-dep (name "clap") (req "^4.0.29") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "flume") (req "^0.10.14") (default-features #t) (kind 0)) (crate-dep (name "num_cpus") (req "^1.15.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.7.1") (default-features #t) (kind 0)))) (hash "12a56kib7i4gv6nyv3hxmjnf0ncm94c46rzpqi6y4jlnqxjilrp8")))

(define-public crate-fcnt-0.2 (crate (name "fcnt") (vers "0.2.6") (deps (list (crate-dep (name "clap") (req "^4.0.29") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "flume") (req "^0.10.14") (default-features #t) (kind 0)) (crate-dep (name "num_cpus") (req "^1.15.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.7.1") (default-features #t) (kind 0)))) (hash "05rn21ag2isrilandlby7913gamzn0qmiryi73q81w4zzv6wljzr")))

(define-public crate-fcnt-0.2 (crate (name "fcnt") (vers "0.2.7") (deps (list (crate-dep (name "clap") (req "^4.0.29") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "flume") (req "^0.10.14") (default-features #t) (kind 0)) (crate-dep (name "num_cpus") (req "^1.15.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.7.1") (default-features #t) (kind 0)))) (hash "09fs1blwkd11lkcgc30vd6yc3rx8bi712y9sdzsl50yb4j7az0fc")))

(define-public crate-fcntl-0.1 (crate (name "fcntl") (vers "0.1.0") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "11inzm32csz6zh2r6ar31f0ia0v7wvx2y36wds37s06sxs96wvw8")))

