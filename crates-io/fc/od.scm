(define-module (crates-io fc od) #:use-module (crates-io))

(define-public crate-fcode-1 (crate (name "fcode") (vers "1.0.0") (deps (list (crate-dep (name "bincode") (req "^1") (default-features #t) (kind 2)) (crate-dep (name "prost") (req "^0.7") (default-features #t) (kind 2)) (crate-dep (name "serde") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1") (features (quote ("derive"))) (default-features #t) (kind 2)) (crate-dep (name "serde_bytes") (req "^0.11") (default-features #t) (kind 2)) (crate-dep (name "serde_json") (req "^1") (default-features #t) (kind 2)) (crate-dep (name "thiserror") (req "^1") (default-features #t) (kind 0)))) (hash "06yhvxbx254vkj8h1yxi5hzn6x23w3y4v6dglw7s63h28zhqkbmx")))

