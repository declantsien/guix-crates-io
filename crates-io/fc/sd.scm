(define-module (crates-io fc sd) #:use-module (crates-io))

(define-public crate-fcsd-0.1 (crate (name "fcsd") (vers "0.1.0") (deps (list (crate-dep (name "byteorder") (req "^1.4.3") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.4") (default-features #t) (kind 2)) (crate-dep (name "rand_chacha") (req "^0.3.1") (default-features #t) (kind 2)))) (hash "1xssg2amdd71il0n6h13jyhg503ga2sr02njcj90gcwfzrih1xp9")))

(define-public crate-fcsd-0.1 (crate (name "fcsd") (vers "0.1.1") (deps (list (crate-dep (name "byteorder") (req "^1.4.3") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.4") (default-features #t) (kind 2)) (crate-dep (name "rand_chacha") (req "^0.3.1") (default-features #t) (kind 2)))) (hash "1gjg5zmbm66x14javsh8gzhw0xikhqm4crf2j78kinmqr0892v48")))

(define-public crate-fcsd-0.1 (crate (name "fcsd") (vers "0.1.2") (deps (list (crate-dep (name "byteorder") (req "^1.4.3") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.4") (default-features #t) (kind 2)) (crate-dep (name "rand_chacha") (req "^0.3.1") (default-features #t) (kind 2)))) (hash "1ic3l1nn31bbfxsnp5jihaxpv0xvw2v1mqkvahaqr83k0yk9ryz9")))

(define-public crate-fcsd-0.1 (crate (name "fcsd") (vers "0.1.3") (deps (list (crate-dep (name "byteorder") (req "^1.4.3") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.4") (default-features #t) (kind 2)) (crate-dep (name "rand_chacha") (req "^0.3.1") (default-features #t) (kind 2)))) (hash "14j6kjzdli664sb5mbzdjsjsp1rk0l1zmg40yqvnpxmjb0fbv4h8")))

(define-public crate-fcsd-0.2 (crate (name "fcsd") (vers "0.2.0") (deps (list (crate-dep (name "anyhow") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "byteorder") (req "^1.4.3") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.4") (default-features #t) (kind 2)) (crate-dep (name "rand_chacha") (req "^0.3.1") (default-features #t) (kind 2)))) (hash "07sv450zpdyj2afnazni6jr2w1fnxbmkj6zylfyn1zcv11cxg1wb")))

