(define-module (crates-io fc la) #:use-module (crates-io))

(define-public crate-fcla-0.1 (crate (name "fcla") (vers "0.1.0") (deps (list (crate-dep (name "fcla-macros") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "num") (req "^0.4.1") (default-features #t) (kind 0)))) (hash "04qmziii22ppccmh7092wzfb146v3mfx8ci46hy1cfhk8phbxwjz")))

(define-public crate-fcla-macros-0.1 (crate (name "fcla-macros") (vers "0.1.0") (deps (list (crate-dep (name "proc-macro2") (req "^1.0.78") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.35") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2.0.48") (features (quote ("full" "extra-traits"))) (default-features #t) (kind 0)))) (hash "0w1ky4hgbcpkzfc4sm3gvb1mh8qwsgklzl35cdfcjm7hf6cs1h3k")))

