(define-module (crates-io fc e_) #:use-module (crates-io))

(define-public crate-fce_cli-0.1 (crate (name "fce_cli") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^2.33.1") (default-features #t) (kind 0)) (crate-dep (name "exitfailure") (req "^0.5.1") (default-features #t) (kind 0)) (crate-dep (name "failure") (req "^0.1.5") (default-features #t) (kind 0)) (crate-dep (name "fce-wit-generator") (req "^0.1.1") (default-features #t) (kind 0)) (crate-dep (name "fce-wit-parser") (req "^0.1.1") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.114") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.56") (default-features #t) (kind 0)))) (hash "0prsw6dl987g14m4bqilwnh6ldxg9a4hlyfyk7wr15i0w2q2z0dd") (yanked #t)))

