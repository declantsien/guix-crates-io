(define-module (crates-io fc mp) #:use-module (crates-io))

(define-public crate-fcmp-0.2 (crate (name "fcmp") (vers "0.2.1") (deps (list (crate-dep (name "anyhow") (req "^1.0.53") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^3.1.0") (features (quote ("derive" "suggestions" "color"))) (default-features #t) (kind 0)) (crate-dep (name "colored") (req "^2.0.0") (default-features #t) (kind 0)) (crate-dep (name "pretty_assertions") (req "^1.1.0") (default-features #t) (kind 2)))) (hash "0ajcxar0yr95xy77d86zczdg2jv2izgsxsxh1pn9h1agxm2kjlzy") (features (quote (("default"))))))

(define-public crate-fcmp-0.3 (crate (name "fcmp") (vers "0.3.0") (deps (list (crate-dep (name "anyhow") (req "^1.0.53") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^3.1.0") (features (quote ("derive" "suggestions" "color"))) (default-features #t) (kind 0)) (crate-dep (name "colored") (req "^2.0.0") (default-features #t) (kind 0)) (crate-dep (name "pretty_assertions") (req "^1.1.0") (default-features #t) (kind 2)) (crate-dep (name "serde") (req "^1.0.136") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)))) (hash "1wp65nmnhpdrc2nzm2izjs9xbvm84pg1brhaj9ij7lvxsivs1fy2") (features (quote (("default"))))))

(define-public crate-fcmp-0.3 (crate (name "fcmp") (vers "0.3.1") (deps (list (crate-dep (name "anyhow") (req "^1.0.53") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^3.1.0") (features (quote ("derive" "suggestions" "color"))) (default-features #t) (kind 0)) (crate-dep (name "pretty_assertions") (req "^1.1.0") (default-features #t) (kind 2)) (crate-dep (name "serde") (req "^1.0.136") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)))) (hash "05j1riwqcwgc87wx4q25nn3wa65b471q1f40vzidakgxx8m1xhfb") (features (quote (("default"))))))

(define-public crate-fcmp-plus-plus-0.1 (crate (name "fcmp-plus-plus") (vers "0.1.0") (hash "1l4184kpp7cqkf8yr7frvhb670bfh3ck6qmr4kjdkp782il99qxa")))

