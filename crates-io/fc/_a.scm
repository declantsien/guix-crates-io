(define-module (crates-io fc _a) #:use-module (crates-io))

(define-public crate-fc_add-0.1 (crate (name "fc_add") (vers "0.1.0") (hash "1b8pdyxs4zzbdsqsxjdak52rhn1v453y7pd0qbw85pd6w02g5lp5") (yanked #t)))

(define-public crate-fc_add-0.2 (crate (name "fc_add") (vers "0.2.0") (hash "0fj82397933jxlkiq1h7pimyppwc7vq0cg2cq03a172wnnhl14vp")))

