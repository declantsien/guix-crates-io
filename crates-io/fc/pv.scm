(define-module (crates-io fc pv) #:use-module (crates-io))

(define-public crate-fcpv2-0.0.1 (crate (name "fcpv2") (vers "0.0.1") (deps (list (crate-dep (name "regex") (req "^1.4.3") (default-features #t) (kind 0)))) (hash "0q0n262hd5dh2zgl14ia806y8wxz9lanpfnwr8x6r3w5yqgal3a9")))

(define-public crate-fcpv2-0.0.2 (crate (name "fcpv2") (vers "0.0.2") (deps (list (crate-dep (name "regex") (req "^1.4.3") (default-features #t) (kind 0)))) (hash "1vzcz0i691dmymxmg7598xc8c1932sany4gq1cp29chywmay8yva")))

