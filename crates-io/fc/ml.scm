(define-module (crates-io fc ml) #:use-module (crates-io))

(define-public crate-fcmlib-0.1 (crate (name "fcmlib") (vers "0.1.0") (deps (list (crate-dep (name "bitflags") (req "^2.4.2") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.20") (default-features #t) (kind 0)) (crate-dep (name "nom") (req "^7.1.3") (default-features #t) (kind 0)))) (hash "10rzi0yymbg6lhgk0i9bm5qmhwnrk12wsfi3rf78gsfyv2bp2yq0")))

