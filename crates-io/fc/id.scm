(define-module (crates-io fc id) #:use-module (crates-io))

(define-public crate-fcidr-0.1 (crate (name "fcidr") (vers "0.1.0") (hash "1rabv0pmxl8b79cyyys28q0cq2dm2qriqi6w37qmkgbbvnhn0d63") (yanked #t)))

(define-public crate-fcidr-0.2 (crate (name "fcidr") (vers "0.2.0") (hash "1n4k6aqzx43jzhlb8a708421blwfy9q4scnv1d6f1l56cdl414yf") (yanked #t)))

(define-public crate-fcidr-0.3 (crate (name "fcidr") (vers "0.3.0") (hash "0wkff25h4lkiy5p91rknqrb8i76558bfmmgp1gb5wd0ff3wkwjr9") (yanked #t)))

(define-public crate-fcidr-0.4 (crate (name "fcidr") (vers "0.4.0") (hash "1jyrig8h3da94giawhasp2dxz11idjdv6dcajss8vnf3zpgbczwm") (yanked #t)))

(define-public crate-fcidr-0.5 (crate (name "fcidr") (vers "0.5.0") (deps (list (crate-dep (name "serde") (req "^1.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 2)))) (hash "0yp7q0jynw9hrw8pgsyhkkkc45kmnsyi0gk1k0l6sf4r35a0jjg6") (yanked #t)))

(define-public crate-fcidr-0.5 (crate (name "fcidr") (vers "0.5.1") (deps (list (crate-dep (name "serde") (req "^1.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 2)))) (hash "11jfrsv9jy1gi2wc31ypa787mj5hzksxs340aa1yk9y9c2ala51v") (yanked #t)))

(define-public crate-fcidr-0.6 (crate (name "fcidr") (vers "0.6.0") (deps (list (crate-dep (name "serde") (req "^1.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 2)))) (hash "1jidkq7mdszrdh62295dwqxlv0bvzarcbb4nk7h1ih7hxwg02kcx")))

(define-public crate-fcidr-0.7 (crate (name "fcidr") (vers "0.7.0") (deps (list (crate-dep (name "clap") (req "^4.3") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 2)))) (hash "0g463sbry0wfv19v4cyj5shbfvgfl58gc4bmw8887fijgpysvr0m") (rust-version "1.70.0")))

(define-public crate-fcidr-0.7 (crate (name "fcidr") (vers "0.7.1") (deps (list (crate-dep (name "clap") (req "^4.3") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 2)))) (hash "1gmv5vxjbj52hhmwsginrf3iqjazd0wi7qkn7rsym5p99bw5sdwj") (rust-version "1.70.0")))

(define-public crate-fcidr-0.7 (crate (name "fcidr") (vers "0.7.2") (deps (list (crate-dep (name "clap") (req "^4.3") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 2)))) (hash "1vc1wly8pc5qkxn4vpf6zpwdydyh893s5ckrr941jg6h7l0z3f0n") (rust-version "1.70.0")))

(define-public crate-fcidr-0.8 (crate (name "fcidr") (vers "0.8.0") (deps (list (crate-dep (name "clap") (req "^4.3") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 2)))) (hash "1asw56na5yw1rahwi9rh3wx14h8cvpc044wjxdw4pcprpvhrcgqz") (rust-version "1.70.0")))

(define-public crate-fcidr-0.8 (crate (name "fcidr") (vers "0.8.1") (deps (list (crate-dep (name "clap") (req "^4.3") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 2)))) (hash "02b5rrpd3nkfpl6vqlk0hxwx78qqwh577i08f2c5j9bygqwjvqdy") (rust-version "1.70.0")))

