(define-module (crates-io fc gi) #:use-module (crates-io))

(define-public crate-fcgi-0.0.1 (crate (name "fcgi") (vers "0.0.1") (hash "1s5521m8wvibpgivcdgddlsvsva3pazz183mshnzcj6l71dz0pc3") (yanked #t)))

(define-public crate-fcgi-0.0.2 (crate (name "fcgi") (vers "0.0.2") (hash "1m0627x3jq3j9pvri1ril4712nxhapb0n6j94p37wfsbddc470v9") (yanked #t)))

