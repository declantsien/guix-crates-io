(define-module (crates-io fc rl) #:use-module (crates-io))

(define-public crate-fcrlf-0.1 (crate (name "fcrlf") (vers "0.1.0") (deps (list (crate-dep (name "anyhow") (req "^1.0.38") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^2.33.3") (default-features #t) (kind 0)) (crate-dep (name "glob") (req "^0.3.0") (default-features #t) (kind 0)))) (hash "03p4zbkmj4zdhzqj795xkrycq3sbnfghbwlhmhvi6ngafzpi41mv")))

