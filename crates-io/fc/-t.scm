(define-module (crates-io fc -t) #:use-module (crates-io))

(define-public crate-fc-tools-flags-0.1 (crate (name "fc-tools-flags") (vers "0.1.0") (deps (list (crate-dep (name "ferrischat_common") (req "^0.2") (default-features #t) (kind 0)))) (hash "0g3z4iir3m6zx738rivyddjkviwxv2mgvfxkxxdbwskpl5rajkqf")))

(define-public crate-fc-tools-snowflakes-0.1 (crate (name "fc-tools-snowflakes") (vers "0.1.0") (deps (list (crate-dep (name "chrono") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "int-enum") (req "^0.4.0") (default-features #t) (kind 0)))) (hash "0skr6mdhcg1bi1ic2jjildfhw23yqfv14kq2pmkj3hhhinyc5p9l")))

(define-public crate-fc-tools-snowflakes-0.1 (crate (name "fc-tools-snowflakes") (vers "0.1.1") (deps (list (crate-dep (name "chrono") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "int-enum") (req "^0.4.0") (default-features #t) (kind 0)))) (hash "0plbsnm6wi4c7lv6ysz72f99qbxi2wp51s7i0275blwzdmny46nr")))

