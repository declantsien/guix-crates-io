(define-module (crates-io fc t-) #:use-module (crates-io))

(define-public crate-fct-vanitygen-0.1 (crate (name "fct-vanitygen") (vers "0.1.0") (deps (list (crate-dep (name "bs58") (req "^0.2.2") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^2.33.0") (default-features #t) (kind 0)) (crate-dep (name "ed25519-dalek") (req "^0.9.1") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.6") (default-features #t) (kind 0)) (crate-dep (name "rayon") (req "^1.0.3") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.1.6") (default-features #t) (kind 0)) (crate-dep (name "sha2") (req "^0.8.0") (default-features #t) (kind 0)))) (hash "0g5ndfyj3phq2zmir0fpb53qb2zbbgg1n648nhzxnw912r5zz4ym")))

