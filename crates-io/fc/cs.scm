(define-module (crates-io fc cs) #:use-module (crates-io))

(define-public crate-fccs-0.1 (crate (name "fccs") (vers "0.1.0") (hash "1wiggym8n1yz9f8rvm3i7gyfbmhdjy5hipmp9cpkmfnvprcrbmsm")))

(define-public crate-fccs-0.1 (crate (name "fccs") (vers "0.1.1") (hash "077324945s0paj4wa867znmipz0s6x9wqchaxmrwffbrwi1ax1s4")))

(define-public crate-fccs-0.1 (crate (name "fccs") (vers "0.1.2") (hash "1dj6g38zjmvvd48xpz49xjiph0wbpaaaz6jf9amnk7k0w4j7infz")))

(define-public crate-fccs-0.1 (crate (name "fccs") (vers "0.1.3") (hash "1jfkkjw6l1pkdnf4m9w4zv1zr8k5kwxqa12hlb5j1gmhkl1s4jcl")))

(define-public crate-fccs-0.1 (crate (name "fccs") (vers "0.1.4") (deps (list (crate-dep (name "windows") (req "^0.18.0") (default-features #t) (kind 0)) (crate-dep (name "windows") (req "^0.18.0") (default-features #t) (kind 1)))) (hash "1dkrgyxmm7jwc8daw8pyycwblrd9dz7a0i3zlq4319ly02a6k55b")))

(define-public crate-fccs-0.1 (crate (name "fccs") (vers "0.1.5") (deps (list (crate-dep (name "windows") (req "^0.18.0") (default-features #t) (kind 0)) (crate-dep (name "windows") (req "^0.18.0") (default-features #t) (kind 1)))) (hash "1wzj87lhkrsnyvf7kpsf3jwpyhwpvc3cvnk6pyj86aa7c6q0divb")))

