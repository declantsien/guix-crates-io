(define-module (crates-io fc p_) #:use-module (crates-io))

(define-public crate-fcp_cryptoauth-0.1 (crate (name "fcp_cryptoauth") (vers "0.1.0") (deps (list (crate-dep (name "byteorder") (req "~0.5.3") (default-features #t) (kind 0)) (crate-dep (name "hex") (req "~0.2.0") (default-features #t) (kind 0)) (crate-dep (name "rust_sodium") (req "~0.1.2") (default-features #t) (kind 0)))) (hash "09yf20m9dbr14ayzqv28jmf2zs9f8pfns8fmjj4lldppf7awi4nc")))

(define-public crate-fcp_cryptoauth-0.2 (crate (name "fcp_cryptoauth") (vers "0.2.0") (deps (list (crate-dep (name "byteorder") (req "^1.0.0") (default-features #t) (kind 0)) (crate-dep (name "hex") (req "~0.2.0") (default-features #t) (kind 0)) (crate-dep (name "rust_sodium") (req "~0.2.0") (default-features #t) (kind 0)))) (hash "09qdiqkqy0mqhcdi3bg37ml2p3bhqi9ajfz6ziwlrkfwri4vb4da")))

(define-public crate-fcp_cryptoauth-0.3 (crate (name "fcp_cryptoauth") (vers "0.3.0") (deps (list (crate-dep (name "byteorder") (req "^1.0.0") (default-features #t) (kind 0)) (crate-dep (name "hex") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "rust_sodium") (req "^0.2.0") (default-features #t) (kind 0)))) (hash "08pfviw7nm6fncmnpbcifs7wjgg3g31a867pywl7bbmj31339bdd")))

(define-public crate-fcp_cryptoauth-0.4 (crate (name "fcp_cryptoauth") (vers "0.4.0") (deps (list (crate-dep (name "byteorder") (req "^1.0.0") (default-features #t) (kind 0)) (crate-dep (name "hex") (req ">=0.3.0, <0.5.0") (default-features #t) (kind 0)) (crate-dep (name "rust_sodium") (req ">=0.3.0, <0.11.0") (default-features #t) (kind 0)))) (hash "00h7gpd7fyvwjv0m0wcgkji1cg7b5s37xnz5rc4xn1vlvpihh3n7")))

(define-public crate-fcp_switching-0.1 (crate (name "fcp_switching") (vers "0.1.0") (deps (list (crate-dep (name "byteorder") (req "^0.5.3") (default-features #t) (kind 0)) (crate-dep (name "fcp_cryptoauth") (req "^0.1.0") (default-features #t) (kind 2)) (crate-dep (name "hex") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.3.15") (default-features #t) (kind 2)) (crate-dep (name "simple_bencode") (req "^0.1.4") (default-features #t) (kind 0)))) (hash "0pgk09va09pl761kn0dvgwdlbqlvy3kbx8air7vwikwgqlabdawq")))

