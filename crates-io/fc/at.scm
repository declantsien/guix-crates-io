(define-module (crates-io fc at) #:use-module (crates-io))

(define-public crate-fcat-0.1 (crate (name "fcat") (vers "0.1.0") (deps (list (crate-dep (name "nix") (req "^0.9.0") (default-features #t) (kind 0)))) (hash "0bk37477r6dd9wdbsimdh7g268xiyhym55gp6diiamv2ka2rj4c4")))

