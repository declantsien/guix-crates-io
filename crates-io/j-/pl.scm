(define-module (crates-io j- pl) #:use-module (crates-io))

(define-public crate-j-pls-0.1 (crate (name "j-pls") (vers "0.1.0") (hash "039bkbdznv6861gl08vwivkv76nfkfsq9llk5c57g3c9535iy9pl")))

(define-public crate-j-pls-0.1 (crate (name "j-pls") (vers "0.1.1") (deps (list (crate-dep (name "owned_chars") (req "^0.3.2") (default-features #t) (kind 0)))) (hash "18x19kzwm4rwyk4j8sq6a974vab5j1kvaqd67w3xb873xss8w7xs")))

(define-public crate-j-pls-0.1 (crate (name "j-pls") (vers "0.1.2") (hash "1rn4kj58q5yya6f0i9br717mbyn2222ng2lcg7zx55gqfhvqk4wm")))

(define-public crate-j-pls-0.1 (crate (name "j-pls") (vers "0.1.3") (hash "1lywcg2ablvfr4ln8d0ribfksdsdmd9jfa9cs9smp79404b151bm")))

(define-public crate-j-pls-0.2 (crate (name "j-pls") (vers "0.2.0") (deps (list (crate-dep (name "colored") (req "^2.1.0") (default-features #t) (kind 0)) (crate-dep (name "global-static") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "xdg") (req "^2.5.2") (default-features #t) (kind 0)))) (hash "1ic7gc43n8jrpw30hkiv9jgxf3ivjx7mwwwny9yxyi7pv3wnl3wp")))

(define-public crate-j-pls-0.2 (crate (name "j-pls") (vers "0.2.1") (deps (list (crate-dep (name "colored") (req "^2.1.0") (default-features #t) (kind 0)) (crate-dep (name "global-static") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "xdg") (req "^2.5.2") (default-features #t) (kind 0)))) (hash "1hq4lz5ala00qsw0d6xm59z2b1my28nph93d5fgazn54jsci4r1z")))

(define-public crate-j-pls-0.2 (crate (name "j-pls") (vers "0.2.2") (deps (list (crate-dep (name "colored") (req "^2.1.0") (default-features #t) (kind 0)) (crate-dep (name "global-static") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "xdg") (req "^2.5.2") (default-features #t) (kind 0)))) (hash "1b4kkj5h5ch1y6k3551g6zi1q0vkv0zqv8ziqdlxslka7ma2ifk0")))

(define-public crate-j-pls-0.2 (crate (name "j-pls") (vers "0.2.3") (deps (list (crate-dep (name "colored") (req "^2.1.0") (default-features #t) (kind 0)) (crate-dep (name "global-static") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "xdg") (req "^2.5.2") (default-features #t) (kind 0)))) (hash "08jrd8q4z8jfhhlhzb1vdzf0gmfgjc8plvg6jwdqxsad518bdhva")))

(define-public crate-j-pls-0.3 (crate (name "j-pls") (vers "0.3.0") (deps (list (crate-dep (name "colored") (req "^2.1.0") (default-features #t) (kind 0)) (crate-dep (name "global-static") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "xdg") (req "^2.5.2") (default-features #t) (kind 0)))) (hash "19l7pi2nj6f5jb02wq2b9pgbcx4g7hj663cz9bra8phkxb4rgwgc")))

(define-public crate-j-pls-0.4 (crate (name "j-pls") (vers "0.4.0") (deps (list (crate-dep (name "colored") (req "^2.1.0") (default-features #t) (kind 0)) (crate-dep (name "global-static") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "xdg") (req "^2.5.2") (default-features #t) (kind 0)))) (hash "0gbfqlcd57ymzmh4n6f3k630jyj3zac28p74wh7r11vp3rylznbx")))

(define-public crate-j-pls-0.4 (crate (name "j-pls") (vers "0.4.1") (deps (list (crate-dep (name "colored") (req "^2.1.0") (default-features #t) (kind 0)) (crate-dep (name "global-static") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "xdg") (req "^2.5.2") (default-features #t) (kind 0)))) (hash "1xpp9sysqr87707izmmxcghs351b5b25v8fza3c757h9297wbdqk")))

(define-public crate-j-pls-0.4 (crate (name "j-pls") (vers "0.4.2") (deps (list (crate-dep (name "colored") (req "^2.1.0") (default-features #t) (kind 0)) (crate-dep (name "global-static") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "xdg") (req "^2.5.2") (default-features #t) (kind 0)))) (hash "0w9hk8x42c5za395yb13pjcq4jjn76yynq9x6p4b16c6v3yhk94b")))

