(define-module (crates-io j- ap) #:use-module (crates-io))

(define-public crate-j-api-0.1 (crate (name "j-api") (vers "0.1.0") (deps (list (crate-dep (name "j-api-derive") (req "^0.1.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)))) (hash "198ys70nc9az2wlih315cp2xm8nbqn41vlpabl6248ykyagmlb9f") (features (quote (("derive" "j-api-derive"))))))

(define-public crate-j-api-actix-0.1 (crate (name "j-api-actix") (vers "0.1.0") (deps (list (crate-dep (name "actix-web") (req "^2.0") (default-features #t) (kind 0)) (crate-dep (name "futures") (req "^0.3.4") (default-features #t) (kind 0)) (crate-dep (name "j-api") (req "^0.1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)))) (hash "18xd10fkg8z5sasprbbs84jas8cl1qfqpf6vn9c8jzbq1jv53iqc")))

(define-public crate-j-api-derive-0.1 (crate (name "j-api-derive") (vers "0.1.0") (deps (list (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (default-features #t) (kind 0)))) (hash "0nyw5vd26xahwzb08pdwqzjmsqv4bzyxxm16bd0ar2f1khh13cdx")))

