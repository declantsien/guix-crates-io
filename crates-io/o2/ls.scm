(define-module (crates-io o2 ls) #:use-module (crates-io))

(define-public crate-o2lsh-0.1 (crate (name "o2lsh") (vers "0.1.0") (deps (list (crate-dep (name "byteorder") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "ndarray") (req "^0.9.1") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "rayon") (req "^0.7") (default-features #t) (kind 0)) (crate-dep (name "revord") (req "^0.0.2") (default-features #t) (kind 0)))) (hash "1crvcdnxmldlpcqw0rna0gnr6l71qn56f658s1fdl7klppa79gm2")))

