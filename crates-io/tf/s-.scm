(define-module (crates-io tf s-) #:use-module (crates-io))

(define-public crate-tfs-fuse-sys-0.1 (crate (name "tfs-fuse-sys") (vers "0.1.0") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "pkg-config") (req "^0.3") (default-features #t) (kind 1)))) (hash "1g4mlv7idqv8frk42qs2i8fcpw9py9gwka2iv6hbx1jzjn994vkl")))

(define-public crate-tfs-fuse-sys-0.1 (crate (name "tfs-fuse-sys") (vers "0.1.1") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "pkg-config") (req "^0.3") (default-features #t) (kind 1)))) (hash "1gys22c33r5pabpva9320digib45wyaxllyb1iivm0r6akvkxkaw")))

