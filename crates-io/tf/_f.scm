(define-module (crates-io tf _f) #:use-module (crates-io))

(define-public crate-tf_filter-0.1 (crate (name "tf_filter") (vers "0.1.0") (deps (list (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "tf_observer") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "12fas8w6wm1aa3iwk6i9zrqg15qby5kh4p3m2x8pky8qx7m3qg8v")))

(define-public crate-tf_filter-0.1 (crate (name "tf_filter") (vers "0.1.1") (deps (list (crate-dep (name "tf_observer") (req "^0.1.1") (default-features #t) (kind 0)))) (hash "15zq8y8jd9b64gw7ql6sjja6lvmc6rxyxq5jy8bhv41wf0bghfb8")))

(define-public crate-tf_filter-0.1 (crate (name "tf_filter") (vers "0.1.2") (deps (list (crate-dep (name "tf_observer") (req "^0.1.2") (default-features #t) (kind 0)))) (hash "1mj5j9mdg5238cklklzv6s99l4vghfld8ajsgawzl0wr793vgdrl")))

(define-public crate-tf_filter-0.1 (crate (name "tf_filter") (vers "0.1.3") (deps (list (crate-dep (name "tf_observer") (req "^0.1.3") (default-features #t) (kind 0)))) (hash "1s8phnxypvy1wyp7l5zbxrrnqif8la4kasbs0yccahrxc6mlq8mm")))

