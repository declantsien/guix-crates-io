(define-module (crates-io tf pc) #:use-module (crates-io))

(define-public crate-tfpc-0.1 (crate (name "tfpc") (vers "0.1.0") (hash "16765cn6jvdq69fbbyyzabxrid3bsi8wmgai3lgmvy43ll94v5id")))

(define-public crate-tfpc-0.1 (crate (name "tfpc") (vers "0.1.1") (hash "1rq9vc7chip5iqyy5armk04m9kwdgm13i0bspwvnpwmwaizdzh2q")))

(define-public crate-tfpc-0.1 (crate (name "tfpc") (vers "0.1.2") (hash "14aa0v79llcbwm1mh07cy53safa34hvq775ab7gpiqjd48nkb6v0")))

(define-public crate-tfpc-0.1 (crate (name "tfpc") (vers "0.1.3") (hash "15vinpg050fagidsc1imzs0cwlsrk54xawalkkiwn9mdq07c199f")))

(define-public crate-tfpc-0.1 (crate (name "tfpc") (vers "0.1.4") (hash "0d6q9fc3ikk1prdydhl44l4xzn7kg5gsm472jjkzh9yrswrac31z")))

