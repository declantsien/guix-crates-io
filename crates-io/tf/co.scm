(define-module (crates-io tf co) #:use-module (crates-io))

(define-public crate-tfconfig-0.1 (crate (name "tfconfig") (vers "0.1.0") (deps (list (crate-dep (name "hcl-rs") (req "^0.14.4") (default-features #t) (kind 0)) (crate-dep (name "tempdir") (req "^0.3.7") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.41") (default-features #t) (kind 0)))) (hash "1l1s8qpcybsnlld8nmjys3nbgajz0af8q9sn8w3046sc03j5hm80")))

(define-public crate-tfconfig-0.2 (crate (name "tfconfig") (vers "0.2.0") (deps (list (crate-dep (name "hcl-rs") (req "^0.16.2") (default-features #t) (kind 0)) (crate-dep (name "tempdir") (req "^0.3.7") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.41") (default-features #t) (kind 0)))) (hash "1j7mxjj2j33ga5py0243b5klawgd4cwr31pbwz0w84y22xrgmcy9")))

(define-public crate-tfconfig-0.2 (crate (name "tfconfig") (vers "0.2.1") (deps (list (crate-dep (name "hcl-rs") (req "^0.16.2") (default-features #t) (kind 0)) (crate-dep (name "tempdir") (req "^0.3.7") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.41") (default-features #t) (kind 0)))) (hash "0xi5x2gr78g2fbdjss27wq3fmf0jzknnhks81ic8n6390nn2q05p")))

(define-public crate-tfconfig-0.2 (crate (name "tfconfig") (vers "0.2.2") (deps (list (crate-dep (name "hcl-rs") (req "^0.16.2") (default-features #t) (kind 0)) (crate-dep (name "tempdir") (req "^0.3.7") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.41") (default-features #t) (kind 0)))) (hash "0jrf7schf1lv410kj7b5gb592z70y08lggxxpqwbh8w4shmskrpc")))

(define-public crate-tfconfig-0.2 (crate (name "tfconfig") (vers "0.2.3") (deps (list (crate-dep (name "hcl-rs") (req "^0.18.0") (default-features #t) (kind 0)) (crate-dep (name "tempdir") (req "^0.3.7") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.41") (default-features #t) (kind 0)))) (hash "091x1flqcc7aqm097n0c5cavc333rx7n7f1si5iq0hkpbp89pvk4")))

