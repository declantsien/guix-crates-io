(define-module (crates-io tf _r) #:use-module (crates-io))

(define-public crate-tf_r2r-0.1 (crate (name "tf_r2r") (vers "0.1.0") (deps (list (crate-dep (name "futures") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "nalgebra") (req "^0.30") (default-features #t) (kind 0)) (crate-dep (name "r2r") (req "^0.8") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "10fp4gkjsyhdsv7ks3f5qlb0pp8nnxxly2hw98qgh6g9phz2ri4m") (features (quote (("ros2" "r2r"))))))

(define-public crate-tf_r2r-0.2 (crate (name "tf_r2r") (vers "0.2.0") (deps (list (crate-dep (name "futures") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "nalgebra") (req "^0.30") (default-features #t) (kind 0)) (crate-dep (name "r2r") (req "^0.9") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "0vsi2d47dz4237qn34a97gz10rlwxx24irkayrb0m03l4xnbyaf7") (features (quote (("ros2" "r2r"))))))

(define-public crate-tf_rosrust-0.0.1 (crate (name "tf_rosrust") (vers "0.0.1") (deps (list (crate-dep (name "nalgebra") (req "^0.26") (default-features #t) (kind 0)) (crate-dep (name "rosrust") (req "^0.9") (default-features #t) (kind 0)))) (hash "1y38dmfd69qkp75dpnqywd0dgm777765kfkibvwp14ip654j6kzv")))

(define-public crate-tf_rosrust-0.0.2 (crate (name "tf_rosrust") (vers "0.0.2") (deps (list (crate-dep (name "nalgebra") (req "^0.26") (default-features #t) (kind 0)) (crate-dep (name "rosrust") (req "^0.9") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0") (default-features #t) (kind 0)))) (hash "1g9hwql1hrh9dx17xb9hx9wi5rsq7bw5cxamcwz62qxdm40kjwin")))

(define-public crate-tf_rosrust-0.0.3 (crate (name "tf_rosrust") (vers "0.0.3") (deps (list (crate-dep (name "nalgebra") (req "^0.26") (default-features #t) (kind 0)) (crate-dep (name "rosrust") (req "^0.9") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0") (default-features #t) (kind 0)))) (hash "1nss8j1q3y6a0qm4mw2nv6nnngwqm0mgddibrifp55gj5v24b27j")))

(define-public crate-tf_rosrust-0.0.4 (crate (name "tf_rosrust") (vers "0.0.4") (deps (list (crate-dep (name "nalgebra") (req "^0.26") (default-features #t) (kind 0)) (crate-dep (name "rosrust") (req "^0.9") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0") (default-features #t) (kind 0)))) (hash "06zd7im6fh0m56cxnyr6n9zwvsmsyndsw6ji9f7siy15qjnvrpxl")))

(define-public crate-tf_rosrust-0.0.5 (crate (name "tf_rosrust") (vers "0.0.5") (deps (list (crate-dep (name "nalgebra") (req "^0.29") (default-features #t) (kind 0)) (crate-dep (name "rosrust") (req "^0.9") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0") (default-features #t) (kind 0)))) (hash "1p3brcqgbmk3c53fbxkiqlg3dy7xqi1vmlcsa5yviv3jrzbbwdn7")))

(define-public crate-tf_rosrust-0.1 (crate (name "tf_rosrust") (vers "0.1.0") (deps (list (crate-dep (name "nalgebra") (req "^0.29") (default-features #t) (kind 0)) (crate-dep (name "rosrust") (req "^0.9") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0") (default-features #t) (kind 0)))) (hash "1b7q6x2vi40i1lwpmxb5hcg29cy677fy026lw9n7ph9cmv8y1acy")))

