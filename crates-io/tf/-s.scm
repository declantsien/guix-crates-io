(define-module (crates-io tf -s) #:use-module (crates-io))

(define-public crate-tf-semver-1 (crate (name "tf-semver") (vers "1.0.17") (deps (list (crate-dep (name "serde") (req "^1.0") (optional #t) (kind 0)))) (hash "1qwf2kly5rq6qdnm5pqk7j5y1w0cmq61pz7xmwg5jza88988m032") (features (quote (("std") ("default" "std")))) (rust-version "1.31")))

