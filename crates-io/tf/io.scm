(define-module (crates-io tf io) #:use-module (crates-io))

(define-public crate-tfio-0.1 (crate (name "tfio") (vers "0.1.0") (deps (list (crate-dep (name "uuid") (req "^0.8") (features (quote ("v4"))) (default-features #t) (kind 0)))) (hash "0f34xzs0xjjl4zlx43599p62zn23jih2lci094d4sl364ds06d27")))

(define-public crate-tfio-0.1 (crate (name "tfio") (vers "0.1.1") (deps (list (crate-dep (name "uuid") (req "^0.8") (features (quote ("v4"))) (default-features #t) (kind 0)))) (hash "1sirsl7764zvmn56wzz0fml1yw9ngrph02x31pf8pzlcr132f5dj")))

(define-public crate-tfio-0.1 (crate (name "tfio") (vers "0.1.2") (deps (list (crate-dep (name "uuid") (req "^0.8") (features (quote ("v4"))) (default-features #t) (kind 0)))) (hash "1g4mmwa8b2m3ny7v12f7bhsnm227fb09b9dkllwk98yn69j3prls")))

(define-public crate-tfio-0.1 (crate (name "tfio") (vers "0.1.3") (deps (list (crate-dep (name "uuid") (req "^0.8") (features (quote ("v4"))) (default-features #t) (kind 0)))) (hash "0xzhvh3h4iagfqja6xl35fmrjqkxkb1cqmcpic76fhg3q5i5pkzl")))

(define-public crate-tfio-0.1 (crate (name "tfio") (vers "0.1.31") (deps (list (crate-dep (name "uuid") (req "^0.8") (features (quote ("v4"))) (default-features #t) (kind 0)))) (hash "0150g8ziwhawdpkrzqc6q3n16c4y7339k52a0kg9rrbxralpzg0x")))

(define-public crate-tfio-0.1 (crate (name "tfio") (vers "0.1.32") (deps (list (crate-dep (name "uuid") (req "^0.8") (features (quote ("v4"))) (default-features #t) (kind 0)))) (hash "1b7v8cw3lq3ggmznb2kvgffn2af5znfs8jyddkay6cff0768zf88")))

