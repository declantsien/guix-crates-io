(define-module (crates-io tf ir) #:use-module (crates-io))

(define-public crate-tfire-0.1 (crate (name "tfire") (vers "0.1.0") (deps (list (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)) (crate-dep (name "termion") (req "^1.5.6") (default-features #t) (kind 0)))) (hash "0fl2z9iklvrqvr7a8qi2gwwviarhy8vcjmmc7n9g7nlg1qcgpxd0")))

