(define-module (crates-io tf id) #:use-module (crates-io))

(define-public crate-tfidf-0.1 (crate (name "tfidf") (vers "0.1.0") (hash "0nbh3115p8z60vkyikh5l97bg5wb75536qxgi53142m5001syypj")))

(define-public crate-tfidf-0.2 (crate (name "tfidf") (vers "0.2.0") (hash "0n5mphv3w8dlhw8hbycfczfjydd0lvwldfix1684i4cix8yk6z2k")))

(define-public crate-tfidf-0.3 (crate (name "tfidf") (vers "0.3.0") (hash "0k7ik9x8m92yg4ddb4rg6r4sksc4hhfz2c17ckbkka43cl54gybb")))

(define-public crate-tfidf-summarizer-2 (crate (name "tfidf-summarizer") (vers "2.0.0") (deps (list (crate-dep (name "unicode-segmentation") (req "^1.8.0") (default-features #t) (kind 0)))) (hash "04a05yibgdx5bxfkiydlmnhqh1r3vlgjn854xhqbyxfj5n9sd880")))

(define-public crate-tfidf-text-summarizer-0.0.1 (crate (name "tfidf-text-summarizer") (vers "0.0.1") (deps (list (crate-dep (name "punkt") (req "^1.0.5") (default-features #t) (kind 0)))) (hash "008b1gjwpc27qgmghpr9b8wgp0wdab1qavhd7bj246rdqv3gwmmj")))

(define-public crate-tfidf-text-summarizer-0.0.2 (crate (name "tfidf-text-summarizer") (vers "0.0.2") (deps (list (crate-dep (name "jni") (req "^0.21.1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "punkt") (req "^1.0.5") (default-features #t) (kind 0)) (crate-dep (name "rayon") (req "^1.8") (default-features #t) (kind 0)))) (hash "0vl2lprsc1d67dvxk2g4r7ly9wk9d35n56i0bd3idd9qiklcx0mx") (v 2) (features2 (quote (("android" "dep:jni"))))))

(define-public crate-tfidf-text-summarizer-0.0.3 (crate (name "tfidf-text-summarizer") (vers "0.0.3") (deps (list (crate-dep (name "jni") (req "^0.21.1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "punkt") (req "^1.0.5") (default-features #t) (kind 0)) (crate-dep (name "rayon") (req "^1.8") (default-features #t) (kind 0)))) (hash "14z3x1icj3dcmzvc53c5zz5z89axy9wsbs5ycg9wz0c1ph612ai6") (v 2) (features2 (quote (("android" "dep:jni"))))))

