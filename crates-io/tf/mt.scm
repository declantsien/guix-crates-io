(define-module (crates-io tf mt) #:use-module (crates-io))

(define-public crate-tfmt-0.4 (crate (name "tfmt") (vers "0.4.0") (deps (list (crate-dep (name "heapless") (req "^0.8.0") (default-features #t) (kind 0)) (crate-dep (name "tfmt-macros") (req "^0.4.0") (default-features #t) (kind 0)))) (hash "1as4n26jpbswf06ckd5ypv2cxyk81gd4m2dd0vazz07jmkbd0879") (features (quote (("std"))))))

(define-public crate-tfmt-macros-0.4 (crate (name "tfmt-macros") (vers "0.4.0") (deps (list (crate-dep (name "proc-macro2") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "1vlwm545b92x974lv2kb65df99xr25n1xsnk0jcrhhs9z9cf8ri6")))

