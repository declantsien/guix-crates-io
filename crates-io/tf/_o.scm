(define-module (crates-io tf _o) #:use-module (crates-io))

(define-public crate-tf_observer-0.1 (crate (name "tf_observer") (vers "0.1.0") (deps (list (crate-dep (name "mockall") (req "^0.10.2") (default-features #t) (kind 2)) (crate-dep (name "mockito") (req "^0.30.0") (default-features #t) (kind 2)))) (hash "18z98f33499jbbn3knikzvmi2hkiaa68spaqmqnyiz0s5mhnr6m7")))

(define-public crate-tf_observer-0.1 (crate (name "tf_observer") (vers "0.1.1") (deps (list (crate-dep (name "mockall") (req "^0.10.2") (default-features #t) (kind 2)) (crate-dep (name "mockito") (req "^0.30.0") (default-features #t) (kind 2)))) (hash "03v25a1zgij59w7nkk5djyxhvg5l4lvn12bwh49xfpyda60my4v9")))

(define-public crate-tf_observer-0.1 (crate (name "tf_observer") (vers "0.1.2") (deps (list (crate-dep (name "mockall") (req "^0.11.0") (default-features #t) (kind 2)) (crate-dep (name "mockito") (req "^0.31.0") (default-features #t) (kind 2)))) (hash "1dx2wajl06w9pchzd11r7l5794ym6b0cir9zwc12c42d90cavdnl")))

(define-public crate-tf_observer-0.1 (crate (name "tf_observer") (vers "0.1.3") (deps (list (crate-dep (name "mockall") (req "^0.11.4") (default-features #t) (kind 2)) (crate-dep (name "mockito") (req "^0.31.1") (default-features #t) (kind 2)))) (hash "0hpbfx0dd4m2dc02d3yipfl4zn9nfy4kkfvpz3pipggc5pzh8hhw")))

