(define-module (crates-io tf le) #:use-module (crates-io))

(define-public crate-tfledge-0.0.0 (crate (name "tfledge") (vers "0.0.0") (deps (list (crate-dep (name "bindgen") (req "^0.69.4") (default-features #t) (kind 1)) (crate-dep (name "env_logger") (req "^0.11.3") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2.154") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.21") (default-features #t) (kind 0)) (crate-dep (name "rgb") (req "^0.8.37") (default-features #t) (kind 0)))) (hash "1rwzpbr1qvwrl18hik39m5vzijh8gjlha5kx5pfz85c3ibfrr8ix") (features (quote (("throttled") ("direct") ("default" "throttled") ("__bindgen"))))))

