(define-module (crates-io tf tp) #:use-module (crates-io))

(define-public crate-tftp-0.1 (crate (name "tftp") (vers "0.1.0") (deps (list (crate-dep (name "ascii") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "displaydoc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "enumn") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "expect-test") (req "^1") (default-features #t) (kind 2)))) (hash "17ahrq0a5jbc701rpw86mpx5k3xzp9f1b6wk66hybbllksn02ldb") (features (quote (("default" "alloc") ("alloc"))))))

(define-public crate-tftp-packet-0.1 (crate (name "tftp-packet") (vers "0.1.0") (deps (list (crate-dep (name "nom") (req "^7.1.1") (default-features #t) (kind 0)))) (hash "1is8qyg6rqydmdag0ljp9brm0yjqd3hzlh59a76k8clcb374d67a")))

(define-public crate-tftp-ro-0.1 (crate (name "tftp-ro") (vers "0.1.0") (deps (list (crate-dep (name "env_logger") (req "^0.7") (default-features #t) (kind 0)) (crate-dep (name "futures") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "simon") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^0.2") (features (quote ("macros" "fs" "udp" "stream" "io-util"))) (default-features #t) (kind 0)))) (hash "022psqfy3g6y0m745cs5mic4qz0b3l2vcgmpl1qn9f80lrpbvf6a")))

(define-public crate-tftp-ro-0.2 (crate (name "tftp-ro") (vers "0.2.0") (deps (list (crate-dep (name "env_logger") (req "^0.7") (default-features #t) (kind 0)) (crate-dep (name "futures") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "simon") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^0.2") (features (quote ("macros" "fs" "udp" "stream" "io-util"))) (default-features #t) (kind 0)))) (hash "1m1v5f2qmmkvznmd2qlyy2lsdq0v6racl5v5b6blza8hasbkiv15")))

(define-public crate-tftp-ro-0.2 (crate (name "tftp-ro") (vers "0.2.1") (deps (list (crate-dep (name "env_logger") (req "^0.7") (default-features #t) (kind 0)) (crate-dep (name "futures") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "simon") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^0.2") (features (quote ("macros" "fs" "udp" "stream" "io-util"))) (default-features #t) (kind 0)))) (hash "0dxn85qns6lqd3sdscydy9bqnqcx6v8cqa11cfsxmb8xmqab27gz")))

(define-public crate-tftp_client-0.1 (crate (name "tftp_client") (vers "0.1.0") (deps (list (crate-dep (name "byte-strings") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "paste") (req "^1") (default-features #t) (kind 2)) (crate-dep (name "thiserror") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "tracing") (req "^0.1") (default-features #t) (kind 0)))) (hash "0zrc806qy6zn2zvk6c6wwbbznh2wy00vw0z4ljl6v62wqd9ndkj5")))

(define-public crate-tftp_client-0.2 (crate (name "tftp_client") (vers "0.2.0") (deps (list (crate-dep (name "byte-strings") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "paste") (req "^1") (default-features #t) (kind 2)) (crate-dep (name "thiserror") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "tracing") (req "^0.1") (default-features #t) (kind 0)))) (hash "0dz27mfwwic6qfbjm1bpzcahsv7l2w313gp3isqyp2cxr2ya8vxh")))

(define-public crate-tftp_server-0.0.1 (crate (name "tftp_server") (vers "0.0.1") (deps (list (crate-dep (name "byteorder") (req "^0.5") (default-features #t) (kind 0)) (crate-dep (name "mio") (req "^0.6") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.3") (default-features #t) (kind 0)))) (hash "16n4w1gbx5ya1insafg15p39p14wf8v83c5h4h212f6rxfy97hca")))

(define-public crate-tftp_server-0.0.2 (crate (name "tftp_server") (vers "0.0.2") (deps (list (crate-dep (name "byteorder") (req "^0.5") (default-features #t) (kind 0)) (crate-dep (name "env_logger") (req "^0.3.5") (default-features #t) (kind 0)) (crate-dep (name "env_logger") (req "^0.3.5") (default-features #t) (kind 2)) (crate-dep (name "log") (req "^0.3.6") (default-features #t) (kind 0)) (crate-dep (name "mio") (req "^0.6") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.3") (default-features #t) (kind 0)))) (hash "0ki0m55f84620iwqzbjjs7qr1hlm269vdkkz6w4d34ds5hj63vh9")))

(define-public crate-tftp_server-0.0.3 (crate (name "tftp_server") (vers "0.0.3") (deps (list (crate-dep (name "byteorder") (req "^0.5") (default-features #t) (kind 0)) (crate-dep (name "env_logger") (req "^0.3.5") (default-features #t) (kind 0)) (crate-dep (name "env_logger") (req "^0.3.5") (default-features #t) (kind 2)) (crate-dep (name "getopts") (req "^0.2") (default-features #t) (kind 2)) (crate-dep (name "log") (req "^0.4.8") (default-features #t) (kind 0)) (crate-dep (name "mio") (req "^0.6") (default-features #t) (kind 0)) (crate-dep (name "mio-extras") (req "^2.0.5") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.3") (default-features #t) (kind 0)))) (hash "138gmin5gvspj4djrwpvlyz59yp26zm7ak0c3i5m3gv4mxkbiv7z")))

(define-public crate-tftpd-0.0.0 (crate (name "tftpd") (vers "0.0.0") (hash "1hrf2wjzi6wkfh3cxwnykm2450z4g7nq2zl73d86wi8pij8ckg0z") (yanked #t)))

(define-public crate-tftpd-0.1 (crate (name "tftpd") (vers "0.1.0") (hash "06bnhf317ma3rxvk34ky6npj9l40k38lrhq2wawr12mnwaiz0vxc") (yanked #t)))

(define-public crate-tftpd-0.1 (crate (name "tftpd") (vers "0.1.1") (hash "167k90847m0ayq1dvvjzpvayzwjfkwqzry0zqxkpvz2lmkw19507")))

(define-public crate-tftpd-0.1 (crate (name "tftpd") (vers "0.1.2") (hash "1zd8nvnn3f86lx2wz897qy85nm39dy5qa5w8sy77qsn739rj57sg") (yanked #t)))

(define-public crate-tftpd-0.1 (crate (name "tftpd") (vers "0.1.3") (hash "0wlw71f2bmvy1hibrlm6jhhlg569gmp4qrz7ziyc1mxp6g1d2bxk")))

(define-public crate-tftpd-0.1 (crate (name "tftpd") (vers "0.1.4") (hash "0lvgi7kx11z5d3grx09mc1hlks6g0xpiwpla4xzhsld7kpww364f")))

(define-public crate-tftpd-0.1 (crate (name "tftpd") (vers "0.1.5") (hash "09xvdvd6a59a89f2x1cd543jpqf5hkcah12sdsjjkjgd44k4gyia")))

(define-public crate-tftpd-0.2 (crate (name "tftpd") (vers "0.2.0") (hash "05hf42mj6zrdm17lr0v11gn7kvcpr280r7g2aqb7a2pmbwzm2qdc")))

(define-public crate-tftpd-0.2 (crate (name "tftpd") (vers "0.2.1") (hash "0b0m9js1lfwimyrhm00s0d4845mhw03jh9hpf1rfc5657q79wnfm")))

(define-public crate-tftpd-0.2 (crate (name "tftpd") (vers "0.2.2") (hash "0vpbjjqr74kqsi7z3rqgxk8mhn5kn91yjsigg4b9wv8gf5cqvxbz")))

(define-public crate-tftpd-0.2 (crate (name "tftpd") (vers "0.2.4") (hash "0g3sg4a73rah4zhab7wk5r3vnbagkvf91k4ib9dipc8gbc30mzsb") (yanked #t)))

(define-public crate-tftpd-0.2 (crate (name "tftpd") (vers "0.2.5") (hash "0wz495ip3d4h2qg2bnw997icv7c794zwyq72i0rf5kkjmjlvpqvr")))

(define-public crate-tftpd-0.2 (crate (name "tftpd") (vers "0.2.6") (hash "1p16v2b2nr6y3kyssp88b5h3pji41vm5ydjd8za2rmqwnhi0bj5m")))

(define-public crate-tftpd-0.2 (crate (name "tftpd") (vers "0.2.7") (hash "0s08lqvwplhncfjrh8d4scvllr0wwlnlmfpz2qsncbni2rczns4a")))

(define-public crate-tftpd-0.2 (crate (name "tftpd") (vers "0.2.8") (hash "1hsjryy7vs4aacvvg09vsc05nw9fcwarwrhxn6xbzgw3jnpqvnyn") (yanked #t)))

(define-public crate-tftpd-0.2 (crate (name "tftpd") (vers "0.2.9") (hash "04mjxdamkklzrc0xlxshbqvd929123ayf4apc1qb8c1g29527ajz")))

(define-public crate-tftpd-0.2 (crate (name "tftpd") (vers "0.2.10") (hash "0x6s7nb2gik25dm93nh8lxgfr4x6r27a5i37mjvbfqnggbgq1pi2") (features (quote (("integration"))))))

(define-public crate-tftpd-0.2 (crate (name "tftpd") (vers "0.2.11") (hash "0kp5a5inysrybfbyvpgx2vwhzk9sr7x2rn32kbax9ng9cbl62b4w") (features (quote (("integration"))))))

(define-public crate-tftpd-0.2 (crate (name "tftpd") (vers "0.2.12") (hash "1vlc83bc4l48hq3h3rswsb89ia6nq7rhbw690b8jprg9yjgc6znz") (features (quote (("integration"))))))

