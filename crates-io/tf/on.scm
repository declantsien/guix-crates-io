(define-module (crates-io tf on) #:use-module (crates-io))

(define-public crate-tfon-0.1 (crate (name "tfon") (vers "0.1.0") (deps (list (crate-dep (name "thiserror") (req "^1") (default-features #t) (kind 0)))) (hash "1zazr7z8w0vx0pn0k5c38jr2pfkhhynvwha7zzq2gk9qgd1k0sfl")))

(define-public crate-tfon-0.1 (crate (name "tfon") (vers "0.1.1") (deps (list (crate-dep (name "thiserror") (req "^1") (default-features #t) (kind 0)))) (hash "0962fr3gp5b44c6zi7z2il72ylprs90ghjp63aqmbh802i60clk1")))

