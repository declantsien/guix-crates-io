(define-module (crates-io tf -k) #:use-module (crates-io))

(define-public crate-tf-kubernetes-0.1 (crate (name "tf-kubernetes") (vers "0.1.0") (deps (list (crate-dep (name "tf-bindgen") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "tf-bindgen") (req "^0.1.0") (default-features #t) (kind 1)))) (hash "0s6v702m617mnpl05pc7zl0yk8i0sslazpdy6jak4mjal2lhx2n8")))

(define-public crate-tf-kubernetes-0.1 (crate (name "tf-kubernetes") (vers "0.1.1") (deps (list (crate-dep (name "tf-bindgen") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "tf-bindgen") (req "^0.1.0") (default-features #t) (kind 1)))) (hash "0afmzfnwhk0ki023bc5gbd6h22xqsb32xmxfjbwkgfnicz4hq3ki")))

