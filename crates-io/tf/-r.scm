(define-module (crates-io tf -r) #:use-module (crates-io))

(define-public crate-tf-rs-0.0.1 (crate (name "tf-rs") (vers "0.0.1") (deps (list (crate-dep (name "error-chain") (req "^0.11.0") (default-features #t) (kind 0)) (crate-dep (name "num-complex") (req "^0.1.40") (kind 0)) (crate-dep (name "tensorflow") (req "^0.6.0") (default-features #t) (kind 0)) (crate-dep (name "uuid") (req "^0.4") (features (quote ("v4"))) (default-features #t) (kind 0)))) (hash "0kvcfv4p71vfgzqamgl9safg19i79833ihngh34y1q2izz0ak5m0")))

(define-public crate-tf-rs-0.0.2 (crate (name "tf-rs") (vers "0.0.2") (deps (list (crate-dep (name "error-chain") (req "^0.11.0") (default-features #t) (kind 0)) (crate-dep (name "num-complex") (req "^0.1.40") (kind 0)) (crate-dep (name "tensorflow") (req "^0.7.0") (default-features #t) (kind 0)) (crate-dep (name "uuid") (req "^0.4") (features (quote ("v4"))) (default-features #t) (kind 0)))) (hash "1iinpq1pia9v3qgss3rqvljazy5ccyfcr09nkqqpy14s2m4bxwac")))

(define-public crate-tf-rs-0.0.4 (crate (name "tf-rs") (vers "0.0.4") (deps (list (crate-dep (name "error-chain") (req "^0.12") (default-features #t) (kind 0)) (crate-dep (name "num-complex") (req "^0.2.4") (kind 0)) (crate-dep (name "tensorflow") (req "^0.14.0") (default-features #t) (kind 0)) (crate-dep (name "uuid") (req "^0.8") (features (quote ("v4"))) (default-features #t) (kind 0)))) (hash "0d9zndp8yrfz0h2l9g842wk4n24v435504df3mnpgangc9bc4j0l")))

(define-public crate-tf-rs-0.0.5 (crate (name "tf-rs") (vers "0.0.5") (deps (list (crate-dep (name "error-chain") (req "^0.12") (default-features #t) (kind 0)) (crate-dep (name "num-complex") (req "^0.2.4") (kind 0)) (crate-dep (name "tensorflow") (req "^0.14.0") (default-features #t) (kind 0)) (crate-dep (name "uuid") (req "^0.8") (features (quote ("v4"))) (default-features #t) (kind 0)))) (hash "0820d2d4zsrylca27f76n4f5yq2b1xhdks8py7yd6zjw4nrzpgag")))

