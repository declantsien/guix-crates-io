(define-module (crates-io fo co) #:use-module (crates-io))

(define-public crate-foco-0.1 (crate (name "foco") (vers "0.1.1") (deps (list (crate-dep (name "array-macro") (req "^1.0.5") (default-features #t) (kind 0)) (crate-dep (name "generic-array") (req "^0.14.2") (default-features #t) (kind 0)) (crate-dep (name "nb") (req "^0.1.2") (default-features #t) (kind 0)) (crate-dep (name "spms_ring") (req "^0.1.2") (default-features #t) (kind 0)))) (hash "1m24l1ak9prywf32drf1scslv41k7g3hg3arclrcbxxjckzgw066")))

(define-public crate-foco-0.1 (crate (name "foco") (vers "0.1.2") (deps (list (crate-dep (name "array-macro") (req "^1.0.5") (default-features #t) (kind 0)) (crate-dep (name "generic-array") (req "^0.14.2") (default-features #t) (kind 0)) (crate-dep (name "nb") (req "^0.1.2") (default-features #t) (kind 0)) (crate-dep (name "spms_ring") (req "^0.1.3") (default-features #t) (kind 0)))) (hash "0n4inlndfw816x4lvh65rcz1d6ldr4rdmaqsd37w6b5k79h2slvp")))

(define-public crate-foco-0.1 (crate (name "foco") (vers "0.1.4") (deps (list (crate-dep (name "array-macro") (req "^1.0.5") (default-features #t) (kind 0)) (crate-dep (name "generic-array") (req "^0.14.2") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 2)) (crate-dep (name "nb") (req "^0.1.2") (default-features #t) (kind 0)) (crate-dep (name "spms_ring") (req "^0.1.4") (default-features #t) (kind 0)))) (hash "1pd7j7w2nl1l16zg1r1yyjc8pby08ss7jg05vdr4y0ayglj6b9a4")))

