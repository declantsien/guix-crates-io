(define-module (crates-io fo rr) #:use-module (crates-io))

(define-public crate-forr-0.1 (crate (name "forr") (vers "0.1.0") (deps (list (crate-dep (name "manyhow") (req "^0.4.2") (kind 0)) (crate-dep (name "proc-macro-utils") (req "^0.8.0") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1") (default-features #t) (kind 0)))) (hash "0l33x4dczlgsncx6hiwha28b8zi8rm1madnvpr4dvww6r6vszfnm")))

(define-public crate-forr-0.1 (crate (name "forr") (vers "0.1.1") (deps (list (crate-dep (name "manyhow") (req "^0.4.2") (kind 0)) (crate-dep (name "proc-macro-utils") (req "^0.8.0") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1.0.30") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1") (default-features #t) (kind 0)))) (hash "1v4a8rnaz8ryc53ihyxb9a2z4y8mk8zrp7cdlpq0f7hnbngkf3xg")))

(define-public crate-forr-0.2 (crate (name "forr") (vers "0.2.0") (deps (list (crate-dep (name "manyhow") (req "^0.6") (kind 0)) (crate-dep (name "proc-macro-utils") (req "^0.8.0") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1.0.30") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1") (default-features #t) (kind 0)))) (hash "1dn7ic01yyfqbn5p2lql68xqh7jxly1ic35wzzv5ip5a0qr2l49j")))

(define-public crate-forr-0.2 (crate (name "forr") (vers "0.2.1") (deps (list (crate-dep (name "manyhow") (req "^0.6") (kind 0)) (crate-dep (name "proc-macro-utils") (req "^0.8.0") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1.0.30") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1") (default-features #t) (kind 0)))) (hash "0dis0n4skyhyd6lbfi9nb3zxjgn5jxi5zcvc26i6hl7cd4srnr4q")))

(define-public crate-forr-0.2 (crate (name "forr") (vers "0.2.2") (deps (list (crate-dep (name "manyhow") (req "^0.6") (kind 0)) (crate-dep (name "proc-macro-utils") (req "^0.8.0") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1.0.30") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1") (default-features #t) (kind 0)))) (hash "0rfmbnz8q83cm65iwxzhwhx9p9hdrrfgjad1zzkz24r7agsmac3v")))

(define-public crate-forr-0.2 (crate (name "forr") (vers "0.2.3") (deps (list (crate-dep (name "manyhow") (req "^0.6") (kind 0)) (crate-dep (name "proc-macro-utils") (req "^0.9.1") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1.0.30") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1") (default-features #t) (kind 0)))) (hash "0whk251yi21lc74k6n2y80j2q0kicq365wxaap9ixsgp0iha320y")))

(define-public crate-forrest-0.1 (crate (name "forrest") (vers "0.1.0") (hash "01h3229ynlagm5jfk0l5x7h5skl7viq61sx7rbmss52b7lh64170")))

(define-public crate-forro-0.1 (crate (name "forro") (vers "0.1.0") (deps (list (crate-dep (name "byteorder") (req "^1") (kind 0)) (crate-dep (name "cfg-if") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "hex") (req "^0.4") (default-features #t) (kind 2)) (crate-dep (name "poly1305") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "zeroize") (req "^1.6") (features (quote ("derive"))) (optional #t) (kind 0)))) (hash "1ppk5cmxybghv626qindxfgmqvi31xjbjhk75w388yh10xivwv22") (features (quote (("std") ("error_in_core") ("default" "zeroize")))) (v 2) (features2 (quote (("zeroize" "dep:zeroize"))))))

(define-public crate-forrust-0.1 (crate (name "forrust") (vers "0.1.0") (deps (list (crate-dep (name "plotlib") (req "^0.5.1") (default-features #t) (kind 0)))) (hash "1j9hjmsjhy7ikn0chc65w331zd5wvmlgwzj9hvqrqm7isy3pk12d")))

(define-public crate-forrust-0.1 (crate (name "forrust") (vers "0.1.1") (deps (list (crate-dep (name "plotlib") (req "^0.5.1") (default-features #t) (kind 0)))) (hash "1szay1pmdkw7l3xqj53g78ckf2b537xs87fw02a9j4cmqkcgpjch")))

(define-public crate-forrust-0.1 (crate (name "forrust") (vers "0.1.2") (deps (list (crate-dep (name "plotlib") (req "^0.5.1") (default-features #t) (kind 0)))) (hash "10v5zms55h0gjcld6rshqs0i4arh6iwmnj11sb6zi01cnj44j3ik")))

(define-public crate-forrust-0.1 (crate (name "forrust") (vers "0.1.3") (deps (list (crate-dep (name "plotlib") (req "^0.5.1") (default-features #t) (kind 0)))) (hash "0jxjdizpz4lnqvf69vv8jjawj8ig70lp4q4i4lbxya4qlvn38647")))

(define-public crate-forrust-0.1 (crate (name "forrust") (vers "0.1.4") (deps (list (crate-dep (name "plotlib") (req "^0.5.1") (default-features #t) (kind 0)))) (hash "1wdnfm11k2slxr4xcn80y9f838sahw7lz1dwr9bnhap5ffx68mnf")))

(define-public crate-forrust-0.1 (crate (name "forrust") (vers "0.1.5") (deps (list (crate-dep (name "plotlib") (req "^0.5.1") (default-features #t) (kind 0)))) (hash "0702bsfl55nqbfdkdq4hbpb284f7vyz6hxardmyacgm1n0prqbqz")))

(define-public crate-forrust-0.1 (crate (name "forrust") (vers "0.1.6") (deps (list (crate-dep (name "plotlib") (req "^0.5.1") (default-features #t) (kind 0)))) (hash "14xkjmjsh1fqw213356sbxn8k1g7x1l5y8mpfs0y68pxb2lkkk99")))

(define-public crate-forrust-0.1 (crate (name "forrust") (vers "0.1.7") (deps (list (crate-dep (name "plotlib") (req "^0.5.1") (default-features #t) (kind 0)))) (hash "18c9c2v0dh5pym1888ivx5947bfhl6m3sssyx3nagb322cnjj6j5")))

(define-public crate-forrust-0.2 (crate (name "forrust") (vers "0.2.0") (deps (list (crate-dep (name "plotlib") (req "^0.5.1") (default-features #t) (kind 0)))) (hash "0q9v9sx8q5nkb72a9d5g49017fmq2dsdb6bx53az6d2wbnz6hlmd")))

(define-public crate-forrust-0.2 (crate (name "forrust") (vers "0.2.1") (deps (list (crate-dep (name "plotlib") (req "^0.5.1") (default-features #t) (kind 0)))) (hash "1lj0lyb50xa9dy2fjl651hl6xr458gwxrka5ybc4bcvz4qz15r54")))

(define-public crate-forrust-0.2 (crate (name "forrust") (vers "0.2.2") (deps (list (crate-dep (name "plotlib") (req "^0.5.1") (default-features #t) (kind 0)))) (hash "0j5nz7vvvag6262pp4wn0xjgybc2wk8s1an93lvrw9z328zsnann")))

(define-public crate-forrust-0.2 (crate (name "forrust") (vers "0.2.3") (deps (list (crate-dep (name "plotlib") (req "^0.5.1") (default-features #t) (kind 0)))) (hash "1vf93b5gbmkm4rr3dlrm9pjqy8r6z5ny9cd4h0yrj0qhhmfhjac7")))

(define-public crate-forrust-0.2 (crate (name "forrust") (vers "0.2.4") (deps (list (crate-dep (name "plotlib") (req "^0.5.1") (default-features #t) (kind 0)))) (hash "0aqij9f5llp5v6fkzggk5d3spw7z5vj9hywa5cv1ad93kmakx6ha")))

(define-public crate-forrustts-0.1 (crate (name "forrustts") (vers "0.1.0") (deps (list (crate-dep (name "GSL") (req "^2.0.1") (default-features #t) (kind 0)) (crate-dep (name "bitflags") (req "^1.2.1") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "tskit_rust") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "1xf4n1l38wvsp4q93cwa30s3r9cyrl54jmllc85l5z7djv6x74pl")))

(define-public crate-forrustts-0.1 (crate (name "forrustts") (vers "0.1.1") (deps (list (crate-dep (name "GSL") (req "^2.0.1") (default-features #t) (kind 0)) (crate-dep (name "bitflags") (req "^1.2.1") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "tskit_rust") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "1gkzaq0rp6gxyf4s9v9h7nhx5w03knb2x25nyw9cw24kp8n08g5z")))

(define-public crate-forrustts-0.1 (crate (name "forrustts") (vers "0.1.2") (deps (list (crate-dep (name "GSL") (req "^2.0.1") (default-features #t) (kind 0)) (crate-dep (name "bitflags") (req "^1.2.1") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "tskit") (req "^0.1.1") (default-features #t) (kind 0)))) (hash "0inr487f3jhx86699711jkzzrgd8zia42nxxxbbckpl70fngdxd8")))

(define-public crate-forrustts-0.1 (crate (name "forrustts") (vers "0.1.3") (deps (list (crate-dep (name "GSL") (req "^4.0.0") (default-features #t) (kind 0)) (crate-dep (name "bitflags") (req "^1.2.1") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "tskit") (req "^0.1.1") (default-features #t) (kind 0)))) (hash "0hak5w2h3fskx7vj4j7rmgg8gaxl2zd7xj3m3r2c6i0qn48jkdbv")))

(define-public crate-forrustts-0.2 (crate (name "forrustts") (vers "0.2.0") (deps (list (crate-dep (name "bitflags") (req "^1.2.1") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "~2.27.0") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.8.3") (default-features #t) (kind 2)) (crate-dep (name "rand_distr") (req "^0.4.0") (default-features #t) (kind 2)) (crate-dep (name "thiserror") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "tskit") (req "=0.2.0") (default-features #t) (kind 0)))) (hash "1x7wd8z6w9y8fl3xqpa3fx4v6nwagij5k0myxwza7jcmxa4752rp")))

(define-public crate-forrustts-definitions-0.1 (crate (name "forrustts-definitions") (vers "0.1.0") (hash "0r8wxdgkcdjfvxws4ww431553n67pjchqab2f4haymmqrrcm5y78") (yanked #t)))

