(define-module (crates-io fo rv) #:use-module (crates-io))

(define-public crate-forvo-cli-0.1 (crate (name "forvo-cli") (vers "0.1.0") (deps (list (crate-dep (name "argparse") (req "^0.2.2") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.4.3") (default-features #t) (kind 0)) (crate-dep (name "requests") (req "^0.0.30") (default-features #t) (kind 0)))) (hash "0djbsqncl3c3d1dbbwc7gl2v0nzr1n68b4hs9qcn9afvfcxqsmhb")))

(define-public crate-forvo-cli-0.1 (crate (name "forvo-cli") (vers "0.1.1") (deps (list (crate-dep (name "argparse") (req "^0.2.2") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.4.3") (default-features #t) (kind 0)) (crate-dep (name "requests") (req "^0.0.30") (default-features #t) (kind 0)))) (hash "0rp39wcjgcd92mzvqa7dx9v67pf6cdwh85sjg0hw8cb5zvd54vxs")))

(define-public crate-forvo-cli-0.1 (crate (name "forvo-cli") (vers "0.1.2") (deps (list (crate-dep (name "argparse") (req "^0.2.2") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.4.3") (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.9.24") (default-features #t) (kind 0)))) (hash "0124lqgh9pg6p3rfq2vc3xj2ljc8dxa5kczjaikg4nmag8kr9pfn")))

(define-public crate-forvo-cli-0.1 (crate (name "forvo-cli") (vers "0.1.3") (deps (list (crate-dep (name "argparse") (req "^0.2.2") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.4.3") (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.9.24") (default-features #t) (kind 0)))) (hash "15bx17s4hcd86fggq3392wxg6rl8dyjwn1lcrgi0jgj92q4z20ib")))

