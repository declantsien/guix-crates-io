(define-module (crates-io fo rz) #:use-module (crates-io))

(define-public crate-forza_dataout_parse-0.1 (crate (name "forza_dataout_parse") (vers "0.1.0") (hash "19l8qdki70dfnlr2w6k7wx5gnjx8ga172jj4nyxl776p00zks9ry")))

(define-public crate-forza_dataout_parse-0.2 (crate (name "forza_dataout_parse") (vers "0.2.0") (deps (list (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)))) (hash "1byp4z7n70dwqr419arbk4b0jhkrnznddbmipin1lp04azciqrnm")))

