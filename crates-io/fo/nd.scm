(define-module (crates-io fo nd) #:use-module (crates-io))

(define-public crate-fondant-0.1 (crate (name "fondant") (vers "0.1.0") (deps (list (crate-dep (name "fondant_deps") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "fondant_derive") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "0rdbqgfyljd17s35y91hnn7wr4lcl2sqi65k1526fxmm78ggqnmx")))

(define-public crate-fondant-0.1 (crate (name "fondant") (vers "0.1.1") (deps (list (crate-dep (name "fondant_deps") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "fondant_derive") (req "^0.1.1") (default-features #t) (kind 0)))) (hash "1413nlqvqxnz792fh8mng9brdkh1hfvkj05llv93spn70gqzx2vk")))

(define-public crate-fondant-0.1 (crate (name "fondant") (vers "0.1.2") (deps (list (crate-dep (name "fondant_deps") (req "^0.1.1") (default-features #t) (kind 0)) (crate-dep (name "fondant_derive") (req "^0.1.1") (default-features #t) (kind 0)))) (hash "0xs1izgqbfvssqmk91fsqlrpp5cb19p71196gm5jq246xjin7kc4")))

(define-public crate-fondant_deps-0.1 (crate (name "fondant_deps") (vers "0.1.0") (deps (list (crate-dep (name "directories") (req "^2.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.103") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.48") (default-features #t) (kind 0)) (crate-dep (name "serde_yaml") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (features (quote ("full"))) (default-features #t) (kind 0)) (crate-dep (name "toml") (req "^0.5") (default-features #t) (kind 0)))) (hash "1xax3lg6p3vrg6gv2r9mzjhdpwjdyyrzbygh1prwsi5w59qj1d97")))

(define-public crate-fondant_deps-0.1 (crate (name "fondant_deps") (vers "0.1.1") (deps (list (crate-dep (name "directories") (req "^2.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.103") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.48") (default-features #t) (kind 0)) (crate-dep (name "serde_yaml") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (features (quote ("full"))) (default-features #t) (kind 0)) (crate-dep (name "toml") (req "^0.5") (default-features #t) (kind 0)))) (hash "1c3xnw90bcdry56p3939hr9nkf8d24anz871h72x67d206q908v3")))

(define-public crate-fondant_derive-0.1 (crate (name "fondant_derive") (vers "0.1.0") (deps (list (crate-dep (name "proc-macro2") (req "^1.0.9") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "1vgdrww3v0j9c4fwlgsmx7138ljwnh4b9sq9vj1dg5hj02p8rls2")))

(define-public crate-fondant_derive-0.1 (crate (name "fondant_derive") (vers "0.1.1") (deps (list (crate-dep (name "proc-macro2") (req "^1.0.9") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "1z5zm9rl3qh6cv23pbfh3ya3nnawgaxjlnyskk4aaa7dlm9112p2")))

(define-public crate-fondue-0.1 (crate (name "fondue") (vers "0.1.0") (hash "1sv61j8ragid01vjsy8zgx98zd837l67bnjvgdvw0191q2bganj9")))

