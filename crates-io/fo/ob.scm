(define-module (crates-io fo ob) #:use-module (crates-io))

(define-public crate-foobar-0.0.0 (crate (name "foobar") (vers "0.0.0") (hash "0qzdksxw0pw95x7zkmfv63kmqwc68wmc1wxr24r6gafjq59d3h6r")))

(define-public crate-foobarbaz-0.1 (crate (name "foobarbaz") (vers "0.1.0") (hash "12rxk34scyjjvqnwj71hdavv1ydqgqj8vm2gga71w857jzjycylb") (yanked #t)))

(define-public crate-foobarbaz-0.1 (crate (name "foobarbaz") (vers "0.1.1") (hash "0p2myha4jz5nc8pn6w1nxygf7mzpjbcmry2md6ml21hg4ls1v5mf") (yanked #t)))

(define-public crate-foobarbaz-0.1 (crate (name "foobarbaz") (vers "0.1.2") (hash "0xyl2qln4gzrfs3wqfxwph6sf8d2sxgxhzj8najpfmkg1jxrh7by") (yanked #t)))

(define-public crate-foobarzz-0.1 (crate (name "foobarzz") (vers "0.1.0") (hash "0mdb4dsfj6c8kixl22gmmllyk6qp5mxdwslxwiarlacy0bc0q9vh")))

