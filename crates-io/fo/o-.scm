(define-module (crates-io fo o-) #:use-module (crates-io))

(define-public crate-foo-bar-0.1 (crate (name "foo-bar") (vers "0.1.0") (hash "121836wkcpp90dqny7iwgxh1fg87aczmw20ar13c29dfhp4xzs4q")))

(define-public crate-foo-cargo-filename-test-0.1 (crate (name "foo-cargo-filename-test") (vers "0.1.0") (hash "0a6chrl0svfl1frfhbzzd73lkzyz0nplq5p83ra598xjdp5bwn8x")))

(define-public crate-foo-cargo-filename-test-0.2 (crate (name "foo-cargo-filename-test") (vers "0.2.0") (deps (list (crate-dep (name "flate2") (req "^1.0.14") (default-features #t) (kind 1)) (crate-dep (name "tar") (req "^0.4") (default-features #t) (kind 1)))) (hash "19z432bbqk2ivpffcr85j7dr0iwpbvgvn6zcjax9inzysyj57d52")))

(define-public crate-foo-cargo-filename-test-0.3 (crate (name "foo-cargo-filename-test") (vers "0.3.0") (deps (list (crate-dep (name "flate2") (req "^1.0.14") (default-features #t) (kind 1)) (crate-dep (name "tar") (req "^0.4") (default-features #t) (kind 1)))) (hash "1g3m1hzlxlvvb5xz4m1zzahfyg60y5iymxkwcsmn1w7k97mpyx74")))

