(define-module (crates-io fo ba) #:use-module (crates-io))

(define-public crate-foba-0.0.1 (crate (name "foba") (vers "0.0.1") (hash "0nxch65b259lh732708fck3d4vsqbv3ajmjjygpl2davahgbfq53")))

(define-public crate-foba-0.0.2 (crate (name "foba") (vers "0.0.2") (hash "0shjfddk49i9dvhjmngbc2qvv2lv2z9nh9hrfinrn96zrbhlpxqf")))

(define-public crate-fobartest1-0.0.1 (crate (name "fobartest1") (vers "0.0.1") (hash "13yc3kf3p7vfwxx1fg1vmy3cq4rd5djmapv7zxk9gpjyl202xkwa")))

