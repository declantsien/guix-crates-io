(define-module (crates-io fo o_) #:use-module (crates-io))

(define-public crate-foo_bar_baz_cate_module-0.6 (crate (name "foo_bar_baz_cate_module") (vers "0.6.0") (hash "1rnlmbhcp29n47rys1lw1s8j0v9p86byvk9nrd0l77mckmqqvr4z")))

(define-public crate-foo_bar_baz_cate_module-0.7 (crate (name "foo_bar_baz_cate_module") (vers "0.7.0") (hash "08yq17nv14z0v6kpz8pq0kj9pq4y17zxq79f2mqbn84hh2x9kwj6")))

(define-public crate-foo_bar_baz_cate_module-0.8 (crate (name "foo_bar_baz_cate_module") (vers "0.8.0") (hash "0pjimw701jlsd3hjl5cyix17bvs93111h9jyjzys29w3zaw6g1a6")))

(define-public crate-foo_bar_baz_cate_module-0.9 (crate (name "foo_bar_baz_cate_module") (vers "0.9.0") (hash "06ml0bzq4f4i4m12znif1nl5jm63ppzqd25kqx3c90a0sp1qxpl0")))

(define-public crate-foo_bar_baz_cate_module-0.10 (crate (name "foo_bar_baz_cate_module") (vers "0.10.0") (hash "0x0hbjahhhgwylrld3w86zfa1ixjlnfmrddc5j3w7zdm0nkr5mxx")))

(define-public crate-foo_bar_baz_cate_module-0.11 (crate (name "foo_bar_baz_cate_module") (vers "0.11.0") (hash "1pyvjy7y9pl0wqk7idkv09g14g1dgd84lkfkf2bkjj5n1n0j7b4a")))

(define-public crate-foo_bar_baz_cate_module-0.12 (crate (name "foo_bar_baz_cate_module") (vers "0.12.0") (hash "0w8az7bv7zhzvr3yjqvbd428yw8jpr08982gsm6fars5iaqj44z5")))

