(define-module (crates-io fo am) #:use-module (crates-io))

(define-public crate-foam-highlighter-0.1 (crate (name "foam-highlighter") (vers "0.1.0") (deps (list (crate-dep (name "tree-sitter-foam") (req "^0.1.2") (default-features #t) (kind 0)) (crate-dep (name "tree-sitter-highlight") (req "^0.20.1") (default-features #t) (kind 0)))) (hash "17sv1c4alwg01qam5pdlxwlc2mml2wj61m4sl80803wm32qsiyhi")))

(define-public crate-foam-highlighter-0.1 (crate (name "foam-highlighter") (vers "0.1.1") (deps (list (crate-dep (name "tree-sitter-foam") (req "^0.1.3") (default-features #t) (kind 0)) (crate-dep (name "tree-sitter-highlight") (req "^0.20.1") (default-features #t) (kind 0)))) (hash "15nmb2zhsw0a28kjvxrb1i81gwnisp0wl6g05167hzgm8dasg3gw")))

(define-public crate-foam-highlighter-0.1 (crate (name "foam-highlighter") (vers "0.1.2") (deps (list (crate-dep (name "tree-sitter-foam") (req "^0.1.3") (default-features #t) (kind 0)) (crate-dep (name "tree-sitter-highlight") (req "^0.20.1") (default-features #t) (kind 0)))) (hash "15mm2mi72zqn7hpfzqidk2wdk004w88h1q8ln3k8hazz787icghk")))

(define-public crate-foam-highlighter-0.1 (crate (name "foam-highlighter") (vers "0.1.3") (deps (list (crate-dep (name "regex") (req "^1.6.0") (default-features #t) (kind 0)) (crate-dep (name "tree-sitter-foam") (req "^0.1.3") (default-features #t) (kind 0)) (crate-dep (name "tree-sitter-highlight") (req "^0.20.1") (default-features #t) (kind 0)))) (hash "0akb9qwx1n0r9azfz2lr6nijsq8y9920h7csd9ys4wnfmwb9mji9")))

(define-public crate-foam-highlighter-0.2 (crate (name "foam-highlighter") (vers "0.2.0") (deps (list (crate-dep (name "regex") (req "^1.6.0") (default-features #t) (kind 0)) (crate-dep (name "tree-sitter-foam") (req "^0.1.3") (default-features #t) (kind 0)) (crate-dep (name "tree-sitter-highlight") (req "^0.20.1") (default-features #t) (kind 0)))) (hash "193fkfy37ak0bj7ylb2jxjdrafgcjnlq4mxmn4z9xk6j16kyfx5v")))

(define-public crate-foam-up-1 (crate (name "foam-up") (vers "1.0.0") (deps (list (crate-dep (name "clap") (req "^2.33.3") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)))) (hash "1l8kg8xpagl4knip0rcl35vgdcdza17ypy10a24wk3q2lg399kv1")))

(define-public crate-foam-up-2 (crate (name "foam-up") (vers "2.0.0") (deps (list (crate-dep (name "clap") (req "^2.33.3") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)))) (hash "1ksyjg2dvbr6qjf56hjp9lnd19a25pvzfa3im5xs1xk1ymsamhc3")))

