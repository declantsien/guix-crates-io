(define-module (crates-io fo le) #:use-module (crates-io))

(define-public crate-foley-cargoexample-guessing_game-0.1 (crate (name "foley-cargoexample-guessing_game") (vers "0.1.0") (deps (list (crate-dep (name "rand") (req "^0.5.5") (default-features #t) (kind 0)))) (hash "028f5ji37d3wnzyxjymgyalkvkjvij80qgza02p9bfhc1q4fg87n")))

(define-public crate-foley-cargoexample-guessing_game-0.1 (crate (name "foley-cargoexample-guessing_game") (vers "0.1.1") (deps (list (crate-dep (name "rand") (req "^0.5.5") (default-features #t) (kind 0)))) (hash "09gwy3hbbika5z01wqw0c64h2rq22d0f5n2h6x47j02pk1g4vnf6")))

