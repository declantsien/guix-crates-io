(define-module (crates-io fo ri) #:use-module (crates-io))

(define-public crate-forismatic-0.1 (crate (name "forismatic") (vers "0.1.0") (deps (list (crate-dep (name "reqwest") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1") (default-features #t) (kind 0)))) (hash "0wz74w88h16md9mjx820hsj1l3myv8jvi0174xzl9y6ljgld651a")))

(define-public crate-forismatic-0.2 (crate (name "forismatic") (vers "0.2.0") (deps (list (crate-dep (name "reqwest") (req "^0.9") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)))) (hash "0vmvmhgi2d2lasw4zyhf9ggw624n2bz4dqx7xcsx19a4h92p1y01")))

(define-public crate-forismatic-0.3 (crate (name "forismatic") (vers "0.3.0") (deps (list (crate-dep (name "reqwest") (req "^0.10.4") (features (quote ("blocking"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.106") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.51") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^0.2.19") (default-features #t) (kind 0)))) (hash "09cvybdmvn25dfh48nwgy936pj12vb78kvx308rcgsbfrcjx6y1h") (yanked #t)))

(define-public crate-forismatic-0.3 (crate (name "forismatic") (vers "0.3.1") (deps (list (crate-dep (name "reqwest") (req "^0.10.4") (features (quote ("blocking"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.106") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.51") (default-features #t) (kind 0)))) (hash "1c3p8jswnsw2sla6bw9daiff6bm0xnv981ivkqsg95lkpppryam1")))

