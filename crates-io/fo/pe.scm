(define-module (crates-io fo pe) #:use-module (crates-io))

(define-public crate-fopencookie-0.1 (crate (name "fopencookie") (vers "0.1.0") (deps (list (crate-dep (name "cstream") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "fopencookie-sys") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2.154") (default-features #t) (kind 0)))) (hash "028glcx51gi8n0037zrylkarynx2fcg9ii1axks4j9mlnl1m6wbd")))

(define-public crate-fopencookie-0.1 (crate (name "fopencookie") (vers "0.1.1") (deps (list (crate-dep (name "cstream") (req "^0.1.1") (default-features #t) (kind 0)) (crate-dep (name "fopencookie-sys") (req "^0.1.1") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2.154") (default-features #t) (kind 0)))) (hash "0a49kg9xn85b0xbklqzi3npq1g4yj5ayh14b1xdkcj7gcqnywck3")))

(define-public crate-fopencookie-sys-0.1 (crate (name "fopencookie-sys") (vers "0.1.0") (deps (list (crate-dep (name "bindgen") (req "^0.69.4") (default-features #t) (kind 1)) (crate-dep (name "cc") (req "^1.0.97") (default-features #t) (kind 1)))) (hash "1w6b1sbq9ky7428mp53sbm1j6qzxljyrr6mn9pqhmqwnmfz37l22")))

(define-public crate-fopencookie-sys-0.1 (crate (name "fopencookie-sys") (vers "0.1.1") (deps (list (crate-dep (name "bindgen") (req "^0.69.4") (default-features #t) (kind 1)) (crate-dep (name "cc") (req "^1.0.97") (default-features #t) (kind 1)))) (hash "15f1yz0v018gwznxxf15cjlsah9s8mrdbnbgriqrkpah12g7d1aq")))

