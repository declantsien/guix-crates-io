(define-module (crates-io fo r_) #:use-module (crates-io))

(define-public crate-for_any-0.0.1 (crate (name "for_any") (vers "0.0.1-beta.0") (deps (list (crate-dep (name "darling") (req "^0.13.0") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1.0.27") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.9") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.73") (features (quote ("full"))) (default-features #t) (kind 0)) (crate-dep (name "trybuild") (req "^1.0.42") (default-features #t) (kind 2)))) (hash "18wmayh1h2vmf0mj08r69khg78l8nh3b57f9pm8zpc3z07adpc14")))

(define-public crate-for_ch-0.1 (crate (name "for_ch") (vers "0.1.0") (deps (list (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (features (quote ("full" "parsing"))) (default-features #t) (kind 0)))) (hash "18ldnsfhqhkhflpchmrh0b94y8r36i36r05whzb1d0fw3q87qq0c")))

(define-public crate-for_ch-0.1 (crate (name "for_ch") (vers "0.1.1") (deps (list (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (features (quote ("full" "parsing"))) (default-features #t) (kind 0)))) (hash "0q6d98spqsx7ss46ks2kmanz5jf8zamwln47vfcwk96fcjlkys5c") (yanked #t)))

(define-public crate-for_ch-0.1 (crate (name "for_ch") (vers "0.1.2") (deps (list (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (features (quote ("full" "parsing"))) (default-features #t) (kind 0)))) (hash "0k6grcqkv6zpfkk1avmmggp949ycbyk2i3211s3nx4sfsbspvlyq")))

(define-public crate-for_ch-0.1 (crate (name "for_ch") (vers "0.1.3") (deps (list (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (features (quote ("full" "parsing"))) (default-features #t) (kind 0)))) (hash "14hn79diis3p25zg5nh253vrkxf9k3acfj3wb9w4mw6dd33c16bn")))

(define-public crate-for_each-0.1 (crate (name "for_each") (vers "0.1.0") (deps (list (crate-dep (name "test_tools") (req "~0.1") (default-features #t) (kind 2)))) (hash "0x8ihmi3hwp658k5aqd4gj9hmcad0kyzxrzsif8hmsc42g63ywl9")))

(define-public crate-for_each-0.1 (crate (name "for_each") (vers "0.1.1") (deps (list (crate-dep (name "test_tools") (req "~0.1") (default-features #t) (kind 2)))) (hash "1yb7mdp70nz6qmc4hpl9khlf694rzyz7jqlspzc3g6qhza33xr7f")))

(define-public crate-for_each-0.1 (crate (name "for_each") (vers "0.1.2") (deps (list (crate-dep (name "test_tools") (req "~0.1") (default-features #t) (kind 2)))) (hash "0h3hivfy3ha0gbpgqg23zzc1rhhkzk53klhzdn6kz0nf2xf9n347")))

(define-public crate-for_each-0.1 (crate (name "for_each") (vers "0.1.3") (deps (list (crate-dep (name "test_tools") (req "~0.1") (default-features #t) (kind 2)))) (hash "0vx7c1wi9q5z12xcw7pkmxd3ym11v7cb5bzhk8ylc1sqry2qjh76")))

(define-public crate-for_each-0.2 (crate (name "for_each") (vers "0.2.0") (hash "0ix79nbi592hjcxl6irsix1hf9g43x1ccxkqj3l4y086a89vivzz") (features (quote (("use_alloc") ("no_std") ("full") ("default"))))))

(define-public crate-for_each-0.3 (crate (name "for_each") (vers "0.3.0") (deps (list (crate-dep (name "test_tools") (req "^0.1.5") (default-features #t) (kind 2)))) (hash "08s0maiqc1h6a87a6pqzm4zfidlvx8x9sqs2wgyb8w56si6lvv42") (features (quote (("use_alloc") ("no_std") ("full" "enabled") ("enabled") ("default" "enabled"))))))

(define-public crate-for_each-0.4 (crate (name "for_each") (vers "0.4.0") (deps (list (crate-dep (name "test_tools") (req "~0.5.0") (default-features #t) (kind 2)))) (hash "1crm9lc4zdn21amcsx9b3jg9vzir8szzai7r8l4wqh7w76wcjbzn") (features (quote (("use_alloc") ("no_std") ("full" "enabled") ("enabled") ("default" "enabled"))))))

(define-public crate-for_each-0.5 (crate (name "for_each") (vers "0.5.0") (deps (list (crate-dep (name "test_tools") (req "~0.6.0") (default-features #t) (kind 2)))) (hash "12f5vhq9c7p07i7fk3v2h8a3icivz9nlfvkb9sfsldjm6qdiqsz8") (features (quote (("use_alloc") ("no_std") ("full" "enabled") ("enabled") ("default" "enabled"))))))

(define-public crate-for_each-0.6 (crate (name "for_each") (vers "0.6.0") (deps (list (crate-dep (name "test_tools") (req "~0.7.0") (default-features #t) (kind 2)))) (hash "1rwnmdqn9ss7hzf8nhgzqzsbvj4z0vnf40gwd0n2g0kw1vacfnv9") (features (quote (("use_alloc") ("no_std") ("full" "enabled") ("enabled") ("default" "enabled"))))))

(define-public crate-for_each-0.7 (crate (name "for_each") (vers "0.7.0") (deps (list (crate-dep (name "test_tools") (req "~0.7.0") (default-features #t) (kind 2)))) (hash "1j36wgpnyq1cxlb0w0kr026367l3whjwby0dzx6dc380sarjcrcn") (features (quote (("use_alloc") ("no_std") ("full" "enabled") ("enabled") ("default" "enabled"))))))

(define-public crate-for_each-0.8 (crate (name "for_each") (vers "0.8.0") (deps (list (crate-dep (name "test_tools") (req "~0.8.0") (default-features #t) (kind 2)))) (hash "0m2grx5w8b0fd71cify389lbzxwd4xbl2cl60jw3f7iqsvkcw7hc") (features (quote (("use_alloc" "no_std") ("no_std") ("full" "enabled") ("enabled") ("default" "enabled"))))))

(define-public crate-for_each_repeat-0.1 (crate (name "for_each_repeat") (vers "0.1.0") (hash "0gl54zz4kabgahnyjqliilq7w2pgd0cdjgdvjzbvryplmibd68z3") (yanked #t)))

(define-public crate-for_each_repeat-0.1 (crate (name "for_each_repeat") (vers "0.1.1") (hash "0rl72k87mkydvc5h27q8by65cawlbwaxiick6j12zdjq5pw9ifzx") (yanked #t)))

(define-public crate-for_each_repeat-0.1 (crate (name "for_each_repeat") (vers "0.1.2") (hash "1zdg1h0y5zsyqs1basxds2yvvl2wqhwjhv6cafx6sg2r773br3dp") (yanked #t)))

(define-public crate-for_each_repeat-0.1 (crate (name "for_each_repeat") (vers "0.1.3") (hash "085ib4xg4lzgxjqbsgf9v764j1fpv6xkvklg00lavand98v75fhw")))

(define-public crate-for_event_bus-0.1 (crate (name "for_event_bus") (vers "0.1.0") (deps (list (crate-dep (name "anyhow") (req "^1.0.69") (default-features #t) (kind 0)) (crate-dep (name "async-trait") (req "^0.1.66") (default-features #t) (kind 0)) (crate-dep (name "custom-utils") (req "^0.10.12") (default-features #t) (kind 2)) (crate-dep (name "log") (req "^0.4.17") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.26.0") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "189lqa54ipkzz798qcdngybqg3ngwfzn2n4f6q3qkh54yx2wa6fl")))

(define-public crate-for_event_bus-0.1 (crate (name "for_event_bus") (vers "0.1.1") (deps (list (crate-dep (name "anyhow") (req "^1.0.69") (default-features #t) (kind 0)) (crate-dep (name "async-trait") (req "^0.1.66") (default-features #t) (kind 0)) (crate-dep (name "custom-utils") (req "^0.10.12") (default-features #t) (kind 2)) (crate-dep (name "log") (req "^0.4.17") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.26.0") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "1bhcl27wc3h5wnv0wqmyq6gchwpyzazbc7zdykmc22rq4hfm0yay")))

(define-public crate-for_event_bus-0.1 (crate (name "for_event_bus") (vers "0.1.2") (deps (list (crate-dep (name "custom-utils") (req "^0.10.12") (default-features #t) (kind 2)) (crate-dep (name "log") (req "^0.4.17") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.26.0") (features (quote ("sync" "rt"))) (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.26.0") (features (quote ("full"))) (default-features #t) (kind 2)))) (hash "0ipgygq923va2lqx6wr8hrxx5bvv0p58x4ay4gs0csc0akyxalci")))

(define-public crate-for_event_bus-0.1 (crate (name "for_event_bus") (vers "0.1.3") (deps (list (crate-dep (name "custom-utils") (req "^0.10.14") (default-features #t) (kind 2)) (crate-dep (name "for-event-bus-derive") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.17") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.26.0") (features (quote ("sync" "rt"))) (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.26.0") (features (quote ("full"))) (default-features #t) (kind 2)))) (hash "027xxjhxvjrjw56rkw5aa5jhc0d3fjv8x4i0fx7s9kjl91kb28l0")))

(define-public crate-for_event_bus-0.1 (crate (name "for_event_bus") (vers "0.1.4") (deps (list (crate-dep (name "custom-utils") (req "^0.10.14") (default-features #t) (kind 2)) (crate-dep (name "for-event-bus-derive") (req "^0.1.1") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.17") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.26.0") (features (quote ("sync" "rt"))) (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.26.0") (features (quote ("full"))) (default-features #t) (kind 2)))) (hash "0igfs16gk48y75rf0r1x2cv64b41657yzdxx6mr75xpy5d7v26k2")))

(define-public crate-for_event_bus-0.1 (crate (name "for_event_bus") (vers "0.1.5") (deps (list (crate-dep (name "custom-utils") (req "^0.10.14") (default-features #t) (kind 2)) (crate-dep (name "for-event-bus-derive") (req "^0.1.1") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.17") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.26.0") (features (quote ("sync" "rt"))) (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.26.0") (features (quote ("full"))) (default-features #t) (kind 2)))) (hash "0ky2vg9y1bmaf4fd8wzc0lxh846gaazcdklk9nzg9s8cffz8d0ry")))

(define-public crate-for_event_bus-0.1 (crate (name "for_event_bus") (vers "0.1.6") (deps (list (crate-dep (name "custom-utils") (req "^0.10.14") (default-features #t) (kind 2)) (crate-dep (name "for-event-bus-derive") (req "^0.1.3") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.17") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.26.0") (features (quote ("sync" "rt"))) (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.26.0") (features (quote ("full"))) (default-features #t) (kind 2)))) (hash "17x3a387gicy586nglhbmk05l2lryb5frkikj3986wlj9caqwyh1")))

(define-public crate-for_let-0.1 (crate (name "for_let") (vers "0.1.0") (deps (list (crate-dep (name "criterion") (req "^0.4.0") (default-features #t) (kind 2)) (crate-dep (name "proc-macro2") (req "~1") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "~1") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "~1") (features (quote ("parsing" "full" "proc-macro" "printing"))) (kind 0)))) (hash "0v8d8cm2g18za52384nbsn4yma3p6gfixr8x12py3dxd86k1awix")))

