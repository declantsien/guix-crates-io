(define-module (crates-io fo od) #:use-module (crates-io))

(define-public crate-food-0.1 (crate (name "food") (vers "0.1.0") (hash "0kfp3b74bfjzjljnzpdy48w7mgr26w4vjm97x5nddm2kdkm9lm0y")))

(define-public crate-food_Library-0.1 (crate (name "food_Library") (vers "0.1.0") (hash "1mkva7f09b94p4k3k6gdg1i6wgjh9phprp7zlj9n8cpyx7kjmadb")))

