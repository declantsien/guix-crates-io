(define-module (crates-io fo sc) #:use-module (crates-io))

(define-public crate-fosc-rs-0.1 (crate (name "fosc-rs") (vers "0.1.0") (deps (list (crate-dep (name "ta-common") (req "^0.1.2") (default-features #t) (kind 0)) (crate-dep (name "tsf-rs") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "08v184fi9bhmhpnfqjaybbmmcj225ps1qpcnahx3gq3cs6wvy5aq")))

