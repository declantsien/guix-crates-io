(define-module (crates-io fo ol) #:use-module (crates-io))

(define-public crate-fool-0.0.1 (crate (name "fool") (vers "0.0.1") (hash "09f819dia01jv1p3nyhk4cp5b968nffmlp1sglgmnxlkznml7s0m") (features (quote (("std") ("default" "std"))))))

(define-public crate-fool-0.0.2 (crate (name "fool") (vers "0.0.2") (hash "0ybbafk78q6m20s24g70m3d94myd6pdwx7a6l60b9b7bpndqj33l") (features (quote (("std") ("default"))))))

(define-public crate-fool-0.0.3 (crate (name "fool") (vers "0.0.3") (hash "111pgnryl01z0n9cm9w8974w0xmflm8qzimcgnmjwsjrf7ry46dj")))

(define-public crate-fool-0.0.4 (crate (name "fool") (vers "0.0.4") (deps (list (crate-dep (name "itertools") (req "^0.9.0") (optional #t) (kind 0)))) (hash "12wrapq5wl3q76z13i26y2nbnd87garh168gssk3hysqs4wigw3y") (features (quote (("zip" "itertools") ("default") ("bare"))))))

(define-public crate-foolang-0.0.0 (crate (name "foolang") (vers "0.0.0") (deps (list (crate-dep (name "assert_cmd") (req "0.12.*") (default-features #t) (kind 2)) (crate-dep (name "clap") (req "2.*.*") (default-features #t) (kind 0)) (crate-dep (name "getrandom") (req "0.1.*") (default-features #t) (kind 0)) (crate-dep (name "kiss3d") (req "0.22.*") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "1.*.*") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "0.2.*") (default-features #t) (target "cfg(unix)") (kind 0)) (crate-dep (name "nalgebra") (req "0.19.*") (default-features #t) (kind 0)) (crate-dep (name "predicates") (req "1.*.*") (default-features #t) (kind 2)) (crate-dep (name "pretty_assertions") (req "0.6.*") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "0.7.*") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "1.*.*") (default-features #t) (kind 0)) (crate-dep (name "rouille") (req "3.*.*") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "1.*.*") (default-features #t) (kind 2)) (crate-dep (name "webbrowser") (req "0.5.*") (default-features #t) (kind 0)) (crate-dep (name "winapi") (req "0.3.*") (default-features #t) (target "cfg(windows)") (kind 0)))) (hash "10dlmr6izbm945zgzz47dw7y2cc70k16vmk4qmqshi0b86j933p9")))

