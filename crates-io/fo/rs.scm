(define-module (crates-io fo rs) #:use-module (crates-io))

(define-public crate-fors-0.1 (crate (name "fors") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^2.18.0") (default-features #t) (kind 0)) (crate-dep (name "nix") (req "^0.7.0") (default-features #t) (kind 0)) (crate-dep (name "nom") (req "^1.2.4") (default-features #t) (kind 0)) (crate-dep (name "term") (req "^0.4.4") (default-features #t) (kind 0)))) (hash "0abnka7fv1q2pbsfcij3dj0vigk18vwfklv9in7yp3c12v3pcyas")))

(define-public crate-forsyth-0.1 (crate (name "forsyth") (vers "0.1.0") (deps (list (crate-dep (name "criterion") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "lz-fear") (req "^0.1") (default-features #t) (kind 2)) (crate-dep (name "tobj") (req "^2.0") (default-features #t) (kind 2)))) (hash "1k989s1ngd147189m4j0005g9hdgagg5lb3ldshq7lvxfacizq95")))

(define-public crate-forsyth-0.2 (crate (name "forsyth") (vers "0.2.0") (deps (list (crate-dep (name "criterion") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "lz-fear") (req "^0.1") (default-features #t) (kind 2)) (crate-dep (name "tobj") (req "^3.0") (default-features #t) (kind 2)))) (hash "0qfbd92w7h3qqsh13vd85y3kwn3s3gih2rlw2kjf9nxiz4zlc4hn")))

(define-public crate-forsyth-0.3 (crate (name "forsyth") (vers "0.3.0") (deps (list (crate-dep (name "criterion") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "lz-fear") (req "^0.1") (default-features #t) (kind 2)) (crate-dep (name "tobj") (req "^3.1") (default-features #t) (kind 2)))) (hash "07k9q6j7pc7w84bd9zn2ls9bqpqw9fxm9kjbfnn83i0v6kcb5hn1")))

(define-public crate-forsyth-1 (crate (name "forsyth") (vers "1.0.0") (deps (list (crate-dep (name "criterion") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "lz-fear") (req "^0.1") (default-features #t) (kind 2)) (crate-dep (name "tobj") (req "^3.1") (default-features #t) (kind 2)))) (hash "1yak3nl58g45xgs7qpk5blq4rdzzn05q9kwf8vl5mbrd8m427j9i")))

(define-public crate-forsyth-1 (crate (name "forsyth") (vers "1.0.1") (deps (list (crate-dep (name "criterion") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "lz-fear") (req "^0.1") (default-features #t) (kind 2)) (crate-dep (name "proptest") (req "^1.0") (default-features #t) (kind 2)) (crate-dep (name "tobj") (req "^3.1") (default-features #t) (kind 2)))) (hash "0vdkb2qj0djchsg510047j0mw48qhg0pmx5jyivmknic2bnzkr50")))

