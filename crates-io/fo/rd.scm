(define-module (crates-io fo rd) #:use-module (crates-io))

(define-public crate-ford-johnson-0.1 (crate (name "ford-johnson") (vers "0.1.0") (deps (list (crate-dep (name "rand") (req "^0.7") (default-features #t) (kind 2)) (crate-dep (name "rand_pcg") (req "^0.2") (default-features #t) (kind 2)))) (hash "1bg2ifldsm6d05dy7ri2na374vgbv2f0ymhkgrh4w02z6r0w97lc")))

(define-public crate-ford-johnson-0.2 (crate (name "ford-johnson") (vers "0.2.0") (deps (list (crate-dep (name "rand") (req "^0.7") (default-features #t) (kind 2)) (crate-dep (name "rand_pcg") (req "^0.2") (default-features #t) (kind 2)))) (hash "0g6k6hhlz4lvz8piiyxzqvqanlyshc5dgxyk52jikl1hw5ydcjv0")))

(define-public crate-ford-johnson-0.2 (crate (name "ford-johnson") (vers "0.2.1") (deps (list (crate-dep (name "criterion") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "quickcheck") (req "^0.9") (default-features #t) (kind 2)) (crate-dep (name "quickcheck_macros") (req "^0.9") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.7") (default-features #t) (kind 2)) (crate-dep (name "rand_pcg") (req "^0.2") (default-features #t) (kind 2)))) (hash "1f453sncqv6hfg2kdjrqclwmpgqnpwb659difxn4khl9f26ymxzi")))

