(define-module (crates-io fo ma) #:use-module (crates-io))

(define-public crate-fomat-macros-0.1 (crate (name "fomat-macros") (vers "0.1.0") (hash "1djbfwfr4acb51ssmhwicsi9a10wf36bv4lspdspbzywajgw4a3i")))

(define-public crate-fomat-macros-0.1 (crate (name "fomat-macros") (vers "0.1.1") (hash "16dw4q7clcsrma29l7d9wqhb398ypvrsf5ngkqn56dg0nf6gjllq")))

(define-public crate-fomat-macros-0.2 (crate (name "fomat-macros") (vers "0.2.0") (deps (list (crate-dep (name "tar") (req "^0.4.9") (default-features #t) (kind 2)) (crate-dep (name "tempdir") (req "^0.3.5") (default-features #t) (kind 2)))) (hash "0vx5h8vywkrzmssw0a553cwfkdffw7g243b57nd9bkfjbfa7abwq")))

(define-public crate-fomat-macros-0.2 (crate (name "fomat-macros") (vers "0.2.1") (deps (list (crate-dep (name "tar") (req "^0.4.9") (default-features #t) (kind 2)) (crate-dep (name "tempdir") (req "^0.3.5") (default-features #t) (kind 2)))) (hash "1iyhqlrb5blv8cmwjmb1wcbfpmhl2d04sapwcsxgnk9scifv6hdm")))

(define-public crate-fomat-macros-0.3 (crate (name "fomat-macros") (vers "0.3.0") (deps (list (crate-dep (name "tar") (req "^0.4.9") (default-features #t) (kind 2)) (crate-dep (name "tempdir") (req "^0.3.5") (default-features #t) (kind 2)))) (hash "00cpagvk1n18k8wn65mq5sp8x1qvs42si2hv4i2h83pd3igybf5x")))

(define-public crate-fomat-macros-0.3 (crate (name "fomat-macros") (vers "0.3.1") (deps (list (crate-dep (name "tar") (req "^0.4.9") (default-features #t) (kind 2)) (crate-dep (name "tempdir") (req "^0.3.5") (default-features #t) (kind 2)))) (hash "0cx8qrr5cskbyf2lbfyxs9gp3cx8q45kjszba1hmb7wziim5ampy")))

(define-public crate-fomat-macros-0.3 (crate (name "fomat-macros") (vers "0.3.2") (deps (list (crate-dep (name "tar") (req "^0.4.9") (default-features #t) (kind 2)) (crate-dep (name "tempfile") (req "^3.3.0") (default-features #t) (kind 2)))) (hash "15m090189g6vzwparj6y6q6q7splysclc05nxfh39399fnl2lwiz")))

(define-public crate-fomat-macros-test-0.3 (crate (name "fomat-macros-test") (vers "0.3.1") (deps (list (crate-dep (name "tar") (req "^0.4.9") (default-features #t) (kind 2)) (crate-dep (name "tempfile") (req "^3.3.0") (default-features #t) (kind 2)))) (hash "022svg3sf56iba4vjl700flhl8pz75fic7casnnfy3n39b6av1f8") (yanked #t)))

