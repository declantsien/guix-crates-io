(define-module (crates-io fo wl) #:use-module (crates-io))

(define-public crate-fowlhouse_bay-0.1 (crate (name "fowlhouse_bay") (vers "0.1.0") (hash "0skjiaf3y4p6c9znhf38dk6y7hmc3wkdyfh1cldn8hzlmrysmvv4")))

(define-public crate-fowlhouse_bay-0.2 (crate (name "fowlhouse_bay") (vers "0.2.0") (deps (list (crate-dep (name "crossterm") (req "^0.17.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.3.6") (default-features #t) (kind 0)))) (hash "1fdyn886adbigjvi7v3d7jkh6iffqjkqjxgmzz5gvd50a0akj462")))

(define-public crate-fowlhouse_bay-0.2 (crate (name "fowlhouse_bay") (vers "0.2.1") (deps (list (crate-dep (name "crossterm") (req "^0.17.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.3.6") (default-features #t) (kind 0)))) (hash "13vrgvw228hp4jn7y6rfryjxbila2c8dgj92glmd3s0ypm4ja4xh")))

(define-public crate-fowlhouse_bay-0.2 (crate (name "fowlhouse_bay") (vers "0.2.2") (deps (list (crate-dep (name "crossterm") (req "^0.17.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.3.6") (default-features #t) (kind 0)))) (hash "1jbr3912ify4kw69ns9wrzwizp2nn1d4s7zd5dsydmgphnxrbbxg")))

(define-public crate-fowlhouse_bay-0.2 (crate (name "fowlhouse_bay") (vers "0.2.3") (deps (list (crate-dep (name "crossterm") (req "^0.17.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.3.6") (default-features #t) (kind 0)))) (hash "04blyw7j0k3y292zcwg49mnyx4viarpp4m359l2r8rj8ck33b3j1")))

(define-public crate-fowlhouse_bay-0.3 (crate (name "fowlhouse_bay") (vers "0.3.0") (deps (list (crate-dep (name "crossterm") (req "^0.17.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.3.6") (default-features #t) (kind 0)))) (hash "1rhijl2940qjpbikzrknh84gblscijywprd34h7ralbrzm63kbyf")))

