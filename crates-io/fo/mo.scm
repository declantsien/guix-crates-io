(define-module (crates-io fo mo) #:use-module (crates-io))

(define-public crate-fomod-0.1 (crate (name "fomod") (vers "0.1.0") (deps (list (crate-dep (name "quick-xml") (req "^0.30.0") (features (quote ("serialize" "serde-types"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.188") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "0bz0h5p0py6jvc1a3vfrr5x9an6mzrvlczmg0zc99j0znq79gji5")))

(define-public crate-fomod-0.2 (crate (name "fomod") (vers "0.2.0") (deps (list (crate-dep (name "quick-xml") (req "^0.30.0") (features (quote ("serialize" "serde-types"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.188") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "1v9y3vah6vqpk656nrrr5bl7d3107pn8h1yhsrxidmwbahj5vci1")))

(define-public crate-fomod-0.2 (crate (name "fomod") (vers "0.2.1") (deps (list (crate-dep (name "quick-xml") (req "^0.30.0") (features (quote ("serialize" "serde-types"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.188") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "0sbs7nbv78cbbah1g6wqlh42clj09vy5fxjmhkxfb1g37y2rbk12")))

(define-public crate-fomod-0.2 (crate (name "fomod") (vers "0.2.2") (deps (list (crate-dep (name "quick-xml") (req "^0.30.0") (features (quote ("serialize" "serde-types"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.188") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "0g9gpagn23rzrlzbvdmjsaljvvlb6xbabg62ydkwzqmgaall5mb3")))

(define-public crate-fomod-0.2 (crate (name "fomod") (vers "0.2.3") (deps (list (crate-dep (name "quick-xml") (req "^0.30.0") (features (quote ("serialize" "serde-types"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.188") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "1lcrh7y3ip1ydhs2gbm2fvsbj5b5wc61w73kxzn8d1hvjrsm0vi8")))

(define-public crate-fomoscript-0.1 (crate (name "fomoscript") (vers "0.1.0") (deps (list (crate-dep (name "criterion") (req "^0.5.1") (features (quote ("html_reports"))) (default-features #t) (kind 2)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)))) (hash "1yg90y8n5h37c63fxnxgrzibrjdzrgkagn0cff8gb57f3wv3dmfr")))

(define-public crate-fomoscript-0.1 (crate (name "fomoscript") (vers "0.1.1") (deps (list (crate-dep (name "criterion") (req "^0.5.1") (features (quote ("html_reports"))) (default-features #t) (kind 2)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)))) (hash "1b4g8awf20lshbfadbfvi9wwrmis97n28h7bla0fxnq3v5vgfz0s")))

(define-public crate-fomoscript-0.1 (crate (name "fomoscript") (vers "0.1.2") (deps (list (crate-dep (name "criterion") (req "^0.5.1") (features (quote ("html_reports"))) (default-features #t) (kind 2)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)))) (hash "180fxj1qb6nd61yy3nk4bac0yn8x1l3d0d4il1b7nymrhapdpd19")))

(define-public crate-fomoscript-0.2 (crate (name "fomoscript") (vers "0.2.0") (deps (list (crate-dep (name "criterion") (req "^0.5.1") (features (quote ("html_reports"))) (default-features #t) (kind 2)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)))) (hash "121gfvs6r5cp81rcj7l43ljci85cm1jjahrb31m1rqszb8hjssld")))

(define-public crate-fomoscript-0.2 (crate (name "fomoscript") (vers "0.2.1") (deps (list (crate-dep (name "criterion") (req "^0.5.1") (features (quote ("html_reports"))) (default-features #t) (kind 2)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)))) (hash "0l3lnxyb7sj7b9dbdpw4bn6g3z9kdafyrn9sn839amvr000lg9rf")))

(define-public crate-fomoscript-0.2 (crate (name "fomoscript") (vers "0.2.4") (deps (list (crate-dep (name "criterion") (req "^0.5.1") (features (quote ("html_reports"))) (default-features #t) (kind 2)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)))) (hash "1g9rmdf5rswj4mya5j8dsi9rq6snqy7n71y7mfikk8pyhzs13cbf")))

(define-public crate-fomoscript-0.2 (crate (name "fomoscript") (vers "0.2.5") (deps (list (crate-dep (name "criterion") (req "^0.5.1") (features (quote ("html_reports"))) (default-features #t) (kind 2)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)))) (hash "1mgmy0dvcw72h8vivsqajydgqq97wlx7qijgzqa5s413s0bi3ya5")))

