(define-module (crates-io fv oi) #:use-module (crates-io))

(define-public crate-fvoid-0.1 (crate (name "fvoid") (vers "0.1.0") (hash "03bg3bp85zypclrshaaj8fr3v9avfg8fp7agj590gk20lby0yrx6")))

(define-public crate-fvoid-0.2 (crate (name "fvoid") (vers "0.2.4") (hash "1wvbv2vbsgrqx4jpxwwvrd3r1y7gyv3a4f488l4as1s9bwy4ns29")))

