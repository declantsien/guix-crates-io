(define-module (crates-io fv ad) #:use-module (crates-io))

(define-public crate-fvad-0.1 (crate (name "fvad") (vers "0.1.0") (deps (list (crate-dep (name "libfvad-sys") (req "^1.0.0") (default-features #t) (kind 0)))) (hash "0sgnssy9h4m2z1wmg4zvihkz423hcy2xyihpak53421fzw1x6paa") (yanked #t)))

(define-public crate-fvad-0.1 (crate (name "fvad") (vers "0.1.1") (deps (list (crate-dep (name "libfvad-sys") (req "^1.0.0") (default-features #t) (kind 0)))) (hash "04jq2j7r1v5z6phqjrjh2im829pmmpy5d2rrd7zv9acmmkp2xmn3")))

(define-public crate-fvad-0.1 (crate (name "fvad") (vers "0.1.2") (deps (list (crate-dep (name "libfvad-sys") (req "^1.0.0") (default-features #t) (kind 0)))) (hash "16j8isj6cshpcxbk0m60x1pgafcksc6121sb4yy8srnxan965fh6")))

(define-public crate-fvad-0.1 (crate (name "fvad") (vers "0.1.3") (deps (list (crate-dep (name "libfvad-sys") (req "^1.0.0") (default-features #t) (kind 0)))) (hash "0fklqi67vs50i599l5j6xq6pdf2cwh55lmsnjj6rdniihz7h93lb")))

