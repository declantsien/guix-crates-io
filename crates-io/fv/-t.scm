(define-module (crates-io fv -t) #:use-module (crates-io))

(define-public crate-fv-template-0.0.0 (crate (name "fv-template") (vers "0.0.0") (hash "1hkngl0qh29zsvffxv97ncvvwl8xpqvk67h6826nz9ysc4j7k18a")))

(define-public crate-fv-template-0.1 (crate (name "fv-template") (vers "0.1.0") (deps (list (crate-dep (name "proc-macro2") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1") (features (quote ("full" "visit-mut"))) (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1") (default-features #t) (kind 0)))) (hash "1szs97p05w6gvlc2zsqsmpvr422x6ki1pk753cdirrfrjdkpx7m0")))

(define-public crate-fv-template-0.2 (crate (name "fv-template") (vers "0.2.0") (deps (list (crate-dep (name "proc-macro2") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2") (features (quote ("full" "visit-mut"))) (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1") (default-features #t) (kind 0)))) (hash "0iszrgdfdi0qrcs1h4z7sphp156nkxhjd2n0rj640hr84p22yk81")))

(define-public crate-fv-template-0.2 (crate (name "fv-template") (vers "0.2.1") (deps (list (crate-dep (name "proc-macro2") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2") (features (quote ("full" "visit-mut"))) (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1") (default-features #t) (kind 0)))) (hash "0bqkjasd2iqgdan9990mp6cngi5965qy4yfibbwnbh750hgkv4gf")))

(define-public crate-fv-template-0.2 (crate (name "fv-template") (vers "0.2.2") (deps (list (crate-dep (name "proc-macro2") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2") (features (quote ("full" "visit-mut"))) (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1") (default-features #t) (kind 0)))) (hash "0sx7k846i0xyp3jfj2rj9g9b1vxi78zrlbmz07xf3ajpccxb4h97")))

(define-public crate-fv-template-0.3 (crate (name "fv-template") (vers "0.3.0") (deps (list (crate-dep (name "proc-macro2") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2") (features (quote ("full" "visit-mut"))) (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1") (default-features #t) (kind 0)))) (hash "1v9fj6icn62s1lg7f4a2j2v4hrqsllhghra716gqgqmlkapi3a3q")))

(define-public crate-fv-template-0.4 (crate (name "fv-template") (vers "0.4.0") (deps (list (crate-dep (name "proc-macro2") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2") (features (quote ("full" "visit-mut"))) (default-features #t) (kind 0)))) (hash "02k7l2d83a839k5v61kyn536aqb2k26j1l4vq7drxrzjyvliyxva")))

