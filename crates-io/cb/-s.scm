(define-module (crates-io cb -s) #:use-module (crates-io))

(define-public crate-cb-stm-temp-0.1 (crate (name "cb-stm-temp") (vers "0.1.0") (deps (list (crate-dep (name "crossbeam-epoch") (req "^0.4") (default-features #t) (kind 0)))) (hash "10qgdwg055yyajlag2a6ysr6sxjsxqkcbcxgn19isqjhbkhsxpyl") (yanked #t)))

(define-public crate-cb-stm-temp-0.1 (crate (name "cb-stm-temp") (vers "0.1.1") (deps (list (crate-dep (name "crossbeam-epoch") (req "^0.4") (default-features #t) (kind 0)))) (hash "19iqc11zryawlhf1vki2dzc9kwlvik33hhxd86aiv9wp1xx1dkqn") (yanked #t)))

(define-public crate-cb-stm-temp-0.2 (crate (name "cb-stm-temp") (vers "0.2.0") (deps (list (crate-dep (name "crossbeam-epoch") (req "^0.4") (default-features #t) (kind 0)))) (hash "1clsl8q4mnmbf33nm1v31kfwj9srh1djg4yz0r00wn20c8g1lsll") (yanked #t)))

(define-public crate-cb-stm-temp-0.3 (crate (name "cb-stm-temp") (vers "0.3.0") (deps (list (crate-dep (name "crossbeam-epoch") (req "^0.4") (default-features #t) (kind 0)))) (hash "1pax4w7wj4km1wf8lfzjzbqrfspx3d16vc0gfaicx5qn23cdbrc3") (yanked #t)))

