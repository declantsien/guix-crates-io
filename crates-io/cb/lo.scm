(define-module (crates-io cb lo) #:use-module (crates-io))

(define-public crate-cbloom-0.1 (crate (name "cbloom") (vers "0.1.0") (hash "103bgzr89nbg1hb9in14dlfscmmn4jy5c2r8j5nammqwgk3fynz0")))

(define-public crate-cbloom-0.1 (crate (name "cbloom") (vers "0.1.1") (hash "1vlgs4q9y3alignlfp7s0l4ga9h2ds5zykf5x8wfxlqvav6v5315")))

(define-public crate-cbloom-0.1 (crate (name "cbloom") (vers "0.1.2") (hash "0nm1ayzj23w60zrs1r6f9rc1vf0h9yf5m8v84clzqq6pwwdkla49")))

(define-public crate-cbloom-0.1 (crate (name "cbloom") (vers "0.1.3") (hash "1zyaa6c0p82mnk23q3589n065lqvsdali1660ay3h1bb9g4r7bhd")))

