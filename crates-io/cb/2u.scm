(define-module (crates-io cb #{2u}#) #:use-module (crates-io))

(define-public crate-cb2util-0.0.1 (crate (name "cb2util") (vers "0.0.1") (deps (list (crate-dep (name "clap") (req "^2") (default-features #t) (kind 0)) (crate-dep (name "codebreaker") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "flate2") (req "^0.2") (default-features #t) (kind 0)))) (hash "0gkjc3i1akiv0gkxaymsc0im7dwwif3s4jyvxasyr83a8h29ihg4")))

