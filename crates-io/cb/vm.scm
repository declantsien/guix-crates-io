(define-module (crates-io cb vm) #:use-module (crates-io))

(define-public crate-cbvm-0.1 (crate (name "cbvm") (vers "0.1.0") (hash "0jnrsnbch92f1ldj0d8w9dqmv99v3kxl87wc5dg93ym4bdik5rgw")))

(define-public crate-cbvm-0.1 (crate (name "cbvm") (vers "0.1.1") (hash "108j9rpgix33c8gn8lzqnrxvpcg659igfmbyxxcjk7iyz53sg8gy")))

(define-public crate-cbvm-0.1 (crate (name "cbvm") (vers "0.1.2") (hash "0m6fnzjb8xla9ajgcj64f5czc6qxmjb8r6m1k7qfsdgmgfj5r71p")))

(define-public crate-cbvm-0.1 (crate (name "cbvm") (vers "0.1.3") (hash "1h6iwwv2ryh517l2pk138gvqwr7wm8xyhl6lvlfhm9kmmyppd9n2")))

(define-public crate-cbvm-0.5 (crate (name "cbvm") (vers "0.5.4") (hash "1c0lc5r30953lynnjipbgcgwzxbydfd3l5knswg518jm0hfy0s4l")))

(define-public crate-cbvm-0.5 (crate (name "cbvm") (vers "0.5.5") (hash "0cvdsbn1mk1yzf5fgp03k9dy1yv9m1vxjkrwri2nyp4qas712ld4")))

(define-public crate-cbvm-0.5 (crate (name "cbvm") (vers "0.5.6") (hash "0840hdgy2r9a7jisbkbs4b5v8slx3vn15w0cxd8iv8vqi5p66ssq")))

(define-public crate-cbvm-0.5 (crate (name "cbvm") (vers "0.5.7") (hash "1934mgxdxgjmlapvpfdhw6711nsiw7zhks6yghdflh77ch8d7akk")))

(define-public crate-cbvm-0.5 (crate (name "cbvm") (vers "0.5.8") (hash "17gak24yqpy8gscbya19w7d4hha8cdsa0mfc9lr4rdqhpmkvc0nk")))

(define-public crate-cbvm-0.6 (crate (name "cbvm") (vers "0.6.1") (deps (list (crate-dep (name "cbasm") (req "^0.1.4") (default-features #t) (kind 0)))) (hash "0xqjsrjp80adqzcy1kxl7fv5kl3irbj1hq9l500bhdvxl0322vpa")))

(define-public crate-cbvm-0.6 (crate (name "cbvm") (vers "0.6.2") (hash "1sh7cq1sczl3xda3fd1sma2fr5y334dp6xlfjvg2slvk346dvbzw")))

(define-public crate-cbvm-0.6 (crate (name "cbvm") (vers "0.6.3") (deps (list (crate-dep (name "cbasm") (req "^0.1.6") (default-features #t) (kind 0)))) (hash "18hfmk2jln9pq0x2s33a9yiajdx4gfil5by4w2a2vnfnm5dgp9rw")))

(define-public crate-cbvm-0.6 (crate (name "cbvm") (vers "0.6.4") (deps (list (crate-dep (name "cbasm") (req "^0.1.6") (default-features #t) (kind 0)))) (hash "1lvflq3rkxbn0pdxl0hk9s93wafcskq8s00cidsmqqqpx2pr8sww")))

(define-public crate-cbvm-0.6 (crate (name "cbvm") (vers "0.6.5") (hash "00lq3k2cbkqlha5awdn2il4qxxnkhcphvwbw5v5plf0kn4cnra80")))

(define-public crate-cbvm-0.6 (crate (name "cbvm") (vers "0.6.6") (hash "0d44cgdd9swrl0r236ifailf6vjqp13aiigxm5v4j2bfk9l9b77q")))

(define-public crate-cbvm-0.6 (crate (name "cbvm") (vers "0.6.7") (hash "1nsmahpvmzvh0ik01vn8kw7m1dvckzsv4s88cilsnbanc1awwbfi")))

(define-public crate-cbvm-0.6 (crate (name "cbvm") (vers "0.6.8") (hash "07jbffw7b34afd9xzd6vk5lx3a3gny9nyylk88b6cp9qg4q68b8y")))

(define-public crate-cbvm-0.6 (crate (name "cbvm") (vers "0.6.9") (hash "141as554iapfgjhwd0iv60gb21n1fc02zad14vr9lz7816ncjdn5")))

(define-public crate-cbvm-0.7 (crate (name "cbvm") (vers "0.7.0") (hash "0a5ixjbncki8fvidadsrygivasaka5gfl2ay2hd7h7qwf3iz4arr")))

(define-public crate-cbvm-0.7 (crate (name "cbvm") (vers "0.7.1") (hash "0pnbd6ffn0ryxscv3pk2byvnxpj6bjrv6y6q9fdvpxn3v301fqvp")))

(define-public crate-cbvm-0.7 (crate (name "cbvm") (vers "0.7.2") (hash "00zdi08y0arwcbwds45x12nxqm034a9f0w2i5isyxsfbp0wgxyja")))

(define-public crate-cbvm-0.7 (crate (name "cbvm") (vers "0.7.3") (hash "087bnhf17y4707p7rgj0cr4rm3nvz66jrc3s9l0v0iwydch5fqhs")))

(define-public crate-cbvm-0.7 (crate (name "cbvm") (vers "0.7.4") (hash "1gj7bi1z4mjgx13jpbb2pdbqbynq456pb7nbzxwxv9gwcg08ldyr")))

(define-public crate-cbvm2-0.7 (crate (name "cbvm2") (vers "0.7.1") (hash "17dfwbnpmsvihpfbhrjzwxln8diq2q4n5yqiarrhph7kfp6d304x")))

