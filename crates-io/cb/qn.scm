(define-module (crates-io cb qn) #:use-module (crates-io))

(define-public crate-cbqn-0.0.8 (crate (name "cbqn") (vers "0.0.8") (deps (list (crate-dep (name "cbqn-sys") (req "^0.0.8") (default-features #t) (kind 0)) (crate-dep (name "once_cell") (req "^1.17.0") (default-features #t) (kind 0)) (crate-dep (name "parking_lot") (req "^0.12.1") (default-features #t) (kind 0)))) (hash "0qr7c538n334iygzj4756sx8d83bp3fymp4d2mhjb1x4s5ynilf0")))

(define-public crate-cbqn-0.1 (crate (name "cbqn") (vers "0.1.0") (deps (list (crate-dep (name "cbqn-sys") (req "^0.2.0") (kind 0)) (crate-dep (name "once_cell") (req "^1.17.0") (default-features #t) (kind 0)) (crate-dep (name "parking_lot") (req "^0.12.1") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.40") (default-features #t) (kind 0)) (crate-dep (name "wasmer") (req "^3.2.1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "wasmer-wasix") (req "^0.3.1") (optional #t) (default-features #t) (kind 0)))) (hash "1pka04psrkk187dk681xxjb2msdvx2c1j64kcxljdkwk82p8xjhb") (features (quote (("native-backend" "cbqn-sys/shared-object") ("default" "native-backend")))) (v 2) (features2 (quote (("wasi-backend" "dep:wasmer" "dep:wasmer-wasix"))))))

(define-public crate-cbqn-0.1 (crate (name "cbqn") (vers "0.1.1") (deps (list (crate-dep (name "cbqn-sys") (req "^0.2.0") (kind 0)) (crate-dep (name "once_cell") (req "^1.17.0") (default-features #t) (kind 0)) (crate-dep (name "parking_lot") (req "^0.12.1") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.40") (default-features #t) (kind 0)) (crate-dep (name "wasmer") (req "^4.0.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "wasmer-wasix") (req "^0.9.0") (optional #t) (default-features #t) (kind 0)))) (hash "10ph52q6iagl78m0ii62cvcm0ibs4b9kx08bvbpkimbgnxqq8apy") (features (quote (("native-backend" "cbqn-sys/shared-object") ("default" "native-backend")))) (v 2) (features2 (quote (("wasi-backend" "dep:wasmer" "dep:wasmer-wasix"))))))

(define-public crate-cbqn-sys-0.0.1 (crate (name "cbqn-sys") (vers "0.0.1") (deps (list (crate-dep (name "bindgen") (req "^0.59.2") (default-features #t) (kind 1)) (crate-dep (name "fs_extra") (req "^1.2.0") (default-features #t) (kind 1)))) (hash "04x1mbygvsw1xfngxm8nz4wxvlkkv436cp9s6b8fml0p07jb1r84") (links "cbqn")))

(define-public crate-cbqn-sys-0.0.2 (crate (name "cbqn-sys") (vers "0.0.2") (deps (list (crate-dep (name "bindgen") (req "^0.59.2") (default-features #t) (kind 1)) (crate-dep (name "fs_extra") (req "^1.2.0") (default-features #t) (kind 1)))) (hash "0pwydm0achglvs7jqkzjjzn1hdwh14p83c26qq80bg1m2clz2shg") (links "cbqn")))

(define-public crate-cbqn-sys-0.0.3 (crate (name "cbqn-sys") (vers "0.0.3") (deps (list (crate-dep (name "bindgen") (req "^0.59.2") (default-features #t) (kind 1)) (crate-dep (name "fs_extra") (req "^1.2.0") (default-features #t) (kind 1)))) (hash "1g1asfjvqkzs1chay5cda47jcij4ii4w89d2anzhhwb6i7d2i7gm") (yanked #t) (links "cbqn")))

(define-public crate-cbqn-sys-0.0.4 (crate (name "cbqn-sys") (vers "0.0.4") (deps (list (crate-dep (name "bindgen") (req "^0.59.2") (default-features #t) (kind 1)) (crate-dep (name "fs_extra") (req "^1.2.0") (default-features #t) (kind 1)))) (hash "1d1xqpi5wkwzragkw7c8k40854mvd189br6nfgdg2j5rlna5ar4k") (yanked #t) (links "cbqn")))

(define-public crate-cbqn-sys-0.0.5 (crate (name "cbqn-sys") (vers "0.0.5") (deps (list (crate-dep (name "bindgen") (req "^0.59.2") (default-features #t) (kind 1)) (crate-dep (name "fs_extra") (req "^1.2.0") (default-features #t) (kind 1)))) (hash "1k9qkkdycm9wzpf0dq3j12vx0wdsvcwybcz4g8xhl3ar1893syql") (links "cbqn")))

(define-public crate-cbqn-sys-0.0.6 (crate (name "cbqn-sys") (vers "0.0.6") (deps (list (crate-dep (name "bindgen") (req "^0.59.2") (default-features #t) (kind 1)) (crate-dep (name "fs_extra") (req "^1.2.0") (default-features #t) (kind 1)))) (hash "1bw7jg3vdhibza7z3jnvgx0r449gw5bm1fa6mlsizjzc5qlzhfqp") (links "cbqn")))

(define-public crate-cbqn-sys-0.0.7 (crate (name "cbqn-sys") (vers "0.0.7") (deps (list (crate-dep (name "bindgen") (req "^0.59.2") (default-features #t) (kind 1)) (crate-dep (name "fs_extra") (req "^1.2.0") (default-features #t) (kind 1)))) (hash "0rpvn27v5dnnlnzk5vrpr0z1kwmvhjgk57n3fb88ybk33d4zll5d") (links "cbqn")))

(define-public crate-cbqn-sys-0.0.8 (crate (name "cbqn-sys") (vers "0.0.8") (deps (list (crate-dep (name "bindgen") (req "^0.59.2") (default-features #t) (kind 1)) (crate-dep (name "fs_extra") (req "^1.2.0") (default-features #t) (kind 1)))) (hash "0jvdnsz78z1yv163g3g2ijk2hsbxw37i3622zqlq0pi2j462yk27") (links "cbqn")))

(define-public crate-cbqn-sys-0.1 (crate (name "cbqn-sys") (vers "0.1.0") (deps (list (crate-dep (name "bindgen") (req "^0.65.1") (optional #t) (default-features #t) (kind 1)) (crate-dep (name "fs_extra") (req "^1.3.0") (optional #t) (default-features #t) (kind 1)))) (hash "08x8y8swz8kgb9752qyrscqzpxiacs2maz1s51xwy7pwk5nmqqkd") (features (quote (("default" "shared-object")))) (links "cbqn") (v 2) (features2 (quote (("shared-object" "dep:fs_extra") ("bindgen" "dep:bindgen"))))))

(define-public crate-cbqn-sys-0.2 (crate (name "cbqn-sys") (vers "0.2.0") (deps (list (crate-dep (name "bindgen") (req "^0.65.1") (optional #t) (default-features #t) (kind 1)))) (hash "1qlbcwml3qj096b8nx8z05absa84cqsjxp69v5qny62y76nf18nf") (features (quote (("shared-object") ("default" "shared-object")))) (links "cbqn") (v 2) (features2 (quote (("bindgen" "dep:bindgen"))))))

