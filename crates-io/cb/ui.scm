(define-module (crates-io cb ui) #:use-module (crates-io))

(define-public crate-cbuild-0.1 (crate (name "cbuild") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^4.4.12") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.193") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_yaml") (req "^0.9.29") (default-features #t) (kind 0)))) (hash "1b3kh9i3dba0w6hy8g3yry36r22qlf38s8wx5xhq2z1xyc8gwqv7")))

(define-public crate-cbuild-0.1 (crate (name "cbuild") (vers "0.1.1") (deps (list (crate-dep (name "clap") (req "^4.4.12") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.193") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_yaml") (req "^0.9.29") (default-features #t) (kind 0)))) (hash "1gnp5x9nfkiy7i12x86m2lzxqg5m0xyqw7z3qdx4760wz2g7ljck")))

(define-public crate-cbuild-0.1 (crate (name "cbuild") (vers "0.1.3") (deps (list (crate-dep (name "clap") (req "^4.4.12") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.193") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_yaml") (req "^0.9.29") (default-features #t) (kind 0)))) (hash "1pik6d9ynci9k6ghq9pikzp5vy2wvml99ifhkk43shgi800ajvvn")))

(define-public crate-cbuild-0.1 (crate (name "cbuild") (vers "0.1.4") (deps (list (crate-dep (name "clap") (req "^4.4.12") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.193") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_yaml") (req "^0.9.29") (default-features #t) (kind 0)))) (hash "0r0zfpw7vb7hxviyp1l0ra0l3lyk1hjia0yz81ycj3xhajzj2wz0")))

