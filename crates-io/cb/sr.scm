(define-module (crates-io cb sr) #:use-module (crates-io))

(define-public crate-cbsrs-0.1 (crate (name "cbsrs") (vers "0.1.0") (deps (list (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)))) (hash "1zrph7fr2m9gglkny8vcyf6vm9b4wfdn85d3vfissw7snfx7nkk0")))

