(define-module (crates-io cb fr) #:use-module (crates-io))

(define-public crate-cbfr-0.1 (crate (name "cbfr") (vers "0.1.0") (hash "08lnrdyv6dglbqw5761450qnz5qd29ka4gkq3hhycncclm2zahv8")))

(define-public crate-cbfr-0.1 (crate (name "cbfr") (vers "0.1.1") (hash "04lwk436749aa2sip03kx9sfj0pr1m3aifgvpvf4ba15423jiibi")))

(define-public crate-cbfr-0.1 (crate (name "cbfr") (vers "0.1.2") (hash "1nmlxlb4xb31pi2hz3nbw8l7n81v4icjq9sf6340wmj9ab4clwyv")))

(define-public crate-cbfr-0.1 (crate (name "cbfr") (vers "0.1.3") (hash "0q96kp8hkpy5psh16p0wgwfw5c0xfdv417lxfjl9gpas4vfg67n7")))

(define-public crate-cbfr-0.1 (crate (name "cbfr") (vers "0.1.4") (hash "0cihah3a8ad98wbj3hv2vqqacgvgd1a70yaqw4yfg820mbl9aa2l")))

(define-public crate-cbfr-0.1 (crate (name "cbfr") (vers "0.1.5") (hash "15lskk6bx1ajg5czhanhk4fqm0p5c6401mvaqpnm0i6hzil64i4c")))

(define-public crate-cbfr-0.1 (crate (name "cbfr") (vers "0.1.6") (hash "0hmrszrhcjrpq4c6gw1x0fql3q4qfjyn8gld8z3dfzw21g84qh17")))

