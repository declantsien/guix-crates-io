(define-module (crates-io cb uf) #:use-module (crates-io))

(define-public crate-cbuf-0.1 (crate (name "cbuf") (vers "0.1.0") (hash "1f5xlppxfm7dsx35ihxa36icp191rq9p24da0y4nn1kxjamyklnw") (features (quote (("no_std") ("default"))))))

(define-public crate-cbuf-0.1 (crate (name "cbuf") (vers "0.1.1") (hash "1zqzns0mj0fikb7pgzmmvhgnzbl01xhfk60hlapmmmqs9c3sqw4q") (features (quote (("no_std") ("default"))))))

(define-public crate-cbuffer-0.1 (crate (name "cbuffer") (vers "0.1.0") (hash "07p9g30dq78vfricci02idm1gb8n8xrhd16zkc0idlbw68klp8vj")))

(define-public crate-cbuffer-0.1 (crate (name "cbuffer") (vers "0.1.1") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "uninit") (req "^0.4") (default-features #t) (kind 0)))) (hash "09zp6f0fi3yqpz9sf55wpaqn5q413xjhs10xql9c151r931znybi")))

(define-public crate-cbuffer-0.1 (crate (name "cbuffer") (vers "0.1.2") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "uninit") (req "^0.4") (default-features #t) (kind 0)))) (hash "0hvfmwdn1mr5276llfnb71xa7r0n05axq4f7d873i05pnd6hjjsz")))

(define-public crate-cbuffer-0.1 (crate (name "cbuffer") (vers "0.1.3") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "uninit") (req "^0.4") (default-features #t) (kind 0)))) (hash "1s4l6f9jq5sdfp0d002nqmrpw38hzq4vhyh2xcd1sxxj5cgr4a7w")))

(define-public crate-cbuffer-0.2 (crate (name "cbuffer") (vers "0.2.0-pre1") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "uninit") (req "^0.4") (default-features #t) (kind 0)))) (hash "17afqvrhgml8ryxw295065wxvbylxbyfnzi888mnnlayip852rgl")))

(define-public crate-cbuffer-0.2 (crate (name "cbuffer") (vers "0.2.0") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "uninit") (req "^0.4") (default-features #t) (kind 0)))) (hash "014kmkh7m99famg606q1kjry0zcw8i16085nsrbkfpya1zwhp5j2")))

(define-public crate-cbuffer-0.3 (crate (name "cbuffer") (vers "0.3.0") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "uninit") (req "^0.4") (default-features #t) (kind 0)))) (hash "1k01h6gdr78hfkffxkvwk9q9yv20blhqsk708y027jzjvyjbkb7p")))

(define-public crate-cbuffer-0.3 (crate (name "cbuffer") (vers "0.3.1") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "uninit") (req "^0.4") (default-features #t) (kind 0)))) (hash "1x8jys1xqyvxa9fw8i0s9acfwj82x9kjr9xccbklqh2nh3h4pyww")))

