(define-module (crates-io cb _f) #:use-module (crates-io))

(define-public crate-cb_fut-0.1 (crate (name "cb_fut") (vers "0.1.0") (deps (list (crate-dep (name "futures") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "futures") (req "^0.3") (features (quote ("thread-pool"))) (default-features #t) (kind 2)))) (hash "0l0lzhzdnmcawk5pwmd8vy89f64mny3x5bx60a73n9z6ysks505g")))

(define-public crate-cb_fut-0.2 (crate (name "cb_fut") (vers "0.2.0") (deps (list (crate-dep (name "crossbeam-channel") (req "^0.4") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "futures") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "futures") (req "^0.3") (features (quote ("thread-pool"))) (default-features #t) (kind 2)))) (hash "1278lyfcb78f28sabhq0vzn5ykifirfppzq1grzs037cdigqmh66") (features (quote (("crossbeam" "crossbeam-channel"))))))

(define-public crate-cb_fut-0.2 (crate (name "cb_fut") (vers "0.2.1") (deps (list (crate-dep (name "crossbeam-channel") (req "^0.4") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "futures") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "futures") (req "^0.3") (features (quote ("thread-pool"))) (default-features #t) (kind 2)))) (hash "1f3j3xgg2j02kfflrs6fnwry077b7nqvy2z29vpccyp0gn4y0w8l") (features (quote (("crossbeam" "crossbeam-channel"))))))

