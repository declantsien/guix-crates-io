(define-module (crates-io cb ox) #:use-module (crates-io))

(define-public crate-cbox-0.1 (crate (name "cbox") (vers "0.1.0") (deps (list (crate-dep (name "libc") (req "*") (default-features #t) (kind 0)))) (hash "1cd1nklwxjdxv36zixn36v1hxd88ka4vr75g58vd8jcp86vfjzk0")))

(define-public crate-cbox-0.1 (crate (name "cbox") (vers "0.1.1") (deps (list (crate-dep (name "libc") (req "*") (default-features #t) (kind 0)))) (hash "0m5s951d1v22s2dm2bnliyd4bipwmdkb8pf8r6hsz59rpc833d0h")))

(define-public crate-cbox-0.1 (crate (name "cbox") (vers "0.1.2") (deps (list (crate-dep (name "libc") (req "*") (default-features #t) (kind 0)))) (hash "1i638vpsxv2aj6iizcny3kqha27p42gi36n03pb9111spyiyr5z5")))

(define-public crate-cbox-0.1 (crate (name "cbox") (vers "0.1.3") (deps (list (crate-dep (name "libc") (req "*") (default-features #t) (kind 0)))) (hash "19nrbmidwyzyffi7b650ddlqw7id8dvqcgwzkc551qp73h2zqjaz")))

(define-public crate-cbox-0.2 (crate (name "cbox") (vers "0.2.0") (deps (list (crate-dep (name "libc") (req "*") (default-features #t) (kind 0)))) (hash "069g6f335078wxq3afmbk7q1m531yvqy5xsfl4p7g0g3jb3f591f")))

(define-public crate-cbox-0.2 (crate (name "cbox") (vers "0.2.1") (deps (list (crate-dep (name "libc") (req "*") (default-features #t) (kind 0)))) (hash "0c5jhdbv29lbzzd8kqdcxrp183iyjivzg5vvky98r6jn5qf8qbxb")))

(define-public crate-cbox-0.2 (crate (name "cbox") (vers "0.2.2") (deps (list (crate-dep (name "libc") (req "*") (default-features #t) (kind 0)))) (hash "0lfk34vi2bgdc5jjqqyb4qcnvwk1bzlpsw1fjk2ksky6jzkmarhv")))

(define-public crate-cbox-0.3 (crate (name "cbox") (vers "0.3.0") (deps (list (crate-dep (name "libc") (req "0.*") (default-features #t) (kind 0)))) (hash "07rj949r2d6vi0nqg0vdnc4mz170b2svl2myirsy2c3dp51q26g4")))

