(define-module (crates-io cb as) #:use-module (crates-io))

(define-public crate-cbasm-0.1 (crate (name "cbasm") (vers "0.1.1") (deps (list (crate-dep (name "cbvm") (req "^0.5.8") (default-features #t) (kind 0)))) (hash "17i4zl4ypfzriqrw0ql8agcfy9sgckpbrfkasxfa1r5546317jsm")))

(define-public crate-cbasm-0.1 (crate (name "cbasm") (vers "0.1.2") (deps (list (crate-dep (name "cbvm") (req "^0.5.8") (default-features #t) (kind 0)))) (hash "1jr7kq584v7n9cwra6y167zad3jzkwbgspi58kvllz4qp06yvnfl")))

(define-public crate-cbasm-0.1 (crate (name "cbasm") (vers "0.1.3") (deps (list (crate-dep (name "cbvm") (req "^0.5.8") (default-features #t) (kind 0)))) (hash "0vd8p5vlh7av9i97hpc1bv46lrm9g5myi26gp68pn28cj11wlsnr")))

(define-public crate-cbasm-0.1 (crate (name "cbasm") (vers "0.1.4") (deps (list (crate-dep (name "cbvm") (req "^0.5.8") (default-features #t) (kind 0)))) (hash "1az3hb50b5kkqkz8d9lqcj4vddjbwd0hp3zzjqisi2vhcn3r4ysw")))

(define-public crate-cbasm-0.1 (crate (name "cbasm") (vers "0.1.5") (deps (list (crate-dep (name "cbvm") (req "^0.6.1") (default-features #t) (kind 0)))) (hash "0yggw3y7icz88ypn64rfd8qyvj6zh2pxy2qddc754wcl7ckcc4ka")))

(define-public crate-cbasm-0.1 (crate (name "cbasm") (vers "0.1.6") (deps (list (crate-dep (name "cbvm") (req "^0.6.2") (default-features #t) (kind 0)))) (hash "19m927fkbk2m60k3b73s80s4ilsfznqh93vmaf67hxdjldx7i1v6")))

(define-public crate-cbasm-0.1 (crate (name "cbasm") (vers "0.1.8") (deps (list (crate-dep (name "cbvm") (req "^0.6.5") (default-features #t) (kind 0)))) (hash "0r0cg4q1wyfd636g45m29m9xhg8hi4zxwivjc2f3ya8gr7cppm7g")))

(define-public crate-cbasm-0.1 (crate (name "cbasm") (vers "0.1.9") (deps (list (crate-dep (name "cbvm") (req "^0.6.6") (default-features #t) (kind 0)))) (hash "1jg1vilcjnxv25y5r79rhbqscys9wzvi36025r40ivbslkpbmbj4")))

(define-public crate-cbasm-0.2 (crate (name "cbasm") (vers "0.2.0") (deps (list (crate-dep (name "cbvm") (req "^0.6.8") (default-features #t) (kind 0)))) (hash "0yzzsq4xps00sz7qv5g8br9igvrg9qcswz24ibdq5ynl2pz9yjp9")))

(define-public crate-cbasm-0.2 (crate (name "cbasm") (vers "0.2.1") (deps (list (crate-dep (name "cbvm") (req "^0.7.0") (default-features #t) (kind 0)))) (hash "18n42qrkr5q9a5nazm7058z6ib7rfxan9a8wb9a0iz8iyb2zm08q")))

(define-public crate-cbasm-0.2 (crate (name "cbasm") (vers "0.2.2") (deps (list (crate-dep (name "cbvm") (req "^0.7.0") (default-features #t) (kind 0)))) (hash "1xsa87ckb441kf6jc04yjjndds1v01hfy6k98c8aalvqm6b3m5iz")))

(define-public crate-cbasm-0.2 (crate (name "cbasm") (vers "0.2.3") (deps (list (crate-dep (name "cbvm") (req "^0.7.2") (default-features #t) (kind 0)))) (hash "1lwpmpksygc6wpgk0pc092k82iv6fmiibvh1gb8j46wyhyni8vpg")))

(define-public crate-cbasm-0.2 (crate (name "cbasm") (vers "0.2.4") (deps (list (crate-dep (name "cbvm") (req "^0.7.3") (default-features #t) (kind 0)))) (hash "0gkfww5yv596z25v9p35fcc0vm0hgdbjh4q1lwvwc1a6z3kdqvah")))

(define-public crate-cbasm-0.2 (crate (name "cbasm") (vers "0.2.5") (deps (list (crate-dep (name "cbvm") (req "^0.7.4") (default-features #t) (kind 0)))) (hash "0nyiwzs692yy2zl9xffymyfx7wffwpxg970ymsdhp0j89szky6gf")))

