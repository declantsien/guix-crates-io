(define-module (crates-io cb la) #:use-module (crates-io))

(define-public crate-cblas-0.0.1 (crate (name "cblas") (vers "0.0.1") (hash "1q9mxbgz0sn24d4drrwb9fqpagmcvfk9im9q400i5fqcabfkc1b9")))

(define-public crate-cblas-0.1 (crate (name "cblas") (vers "0.1.0") (deps (list (crate-dep (name "cblas-sys") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "num-complex") (req "^0.1") (kind 0)))) (hash "0gdsdxk90422f35w6wk32sf9z8d87piplwn0g93ivcw6ja2708f1")))

(define-public crate-cblas-0.1 (crate (name "cblas") (vers "0.1.1") (deps (list (crate-dep (name "cblas-sys") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "num-complex") (req "^0.1") (kind 0)))) (hash "1r2wlazlkgz3brpwddsf2ni2r6krjsi1s04ds7va4rbswqwhxm0w")))

(define-public crate-cblas-0.1 (crate (name "cblas") (vers "0.1.2") (deps (list (crate-dep (name "cblas-sys") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "num-complex") (req "^0.1") (kind 0)))) (hash "1yafk26cjz6p21gsvik9rwwnk48kgd8ap0gg5ihhcn958130lg9h")))

(define-public crate-cblas-0.1 (crate (name "cblas") (vers "0.1.3") (deps (list (crate-dep (name "cblas-sys") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "num-complex") (req "^0.1") (kind 0)))) (hash "1c9233zby96lmykqq2r5p7r7r2bs3wmyysz97yg4x82wnl5x3hg2")))

(define-public crate-cblas-0.1 (crate (name "cblas") (vers "0.1.4") (deps (list (crate-dep (name "cblas-sys") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "num-complex") (req "^0.1") (kind 0)))) (hash "1kvv0mabqfbg38jw2f3p55ib2if1gyh6sblbz31ywrghx4bz8q50")))

(define-public crate-cblas-0.1 (crate (name "cblas") (vers "0.1.5") (deps (list (crate-dep (name "cblas-sys") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "num-complex") (req "^0.1") (kind 0)))) (hash "0fm1y2kmmjxv4xvbzprrdxs4sjdrwc1x5x05nmvn1k316cic4iff")))

(define-public crate-cblas-0.1 (crate (name "cblas") (vers "0.1.6") (deps (list (crate-dep (name "cblas-sys") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "num-complex") (req "^0.2") (kind 0)))) (hash "1541jn06llhmibr6947chvxlh6mak16alimva0nnb4sb38nzhhzc") (yanked #t)))

(define-public crate-cblas-0.2 (crate (name "cblas") (vers "0.2.0") (deps (list (crate-dep (name "cblas-sys") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "num-complex") (req "^0.2") (kind 0)))) (hash "1nz5xi54xs9i10csyv81fxjzb5lv0iwdha6b856fpv1kvld36byq")))

(define-public crate-cblas-0.3 (crate (name "cblas") (vers "0.3.0") (deps (list (crate-dep (name "cblas-sys") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "num-complex") (req "^0.3") (kind 0)))) (hash "12y7171xcj5pv06chycpq0p14v21j7cnn1jwyiwh4spdd01aalq4")))

(define-public crate-cblas-0.4 (crate (name "cblas") (vers "0.4.0") (deps (list (crate-dep (name "cblas-sys") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "num-complex") (req "^0.4") (kind 0)))) (hash "0vrj2bza6q9wxl567ij1228sglj8gw8sxyj6pj8yimwffkznvr1x")))

(define-public crate-cblas-src-0.1 (crate (name "cblas-src") (vers "0.1.0") (deps (list (crate-dep (name "anyhow") (req "^1.0") (default-features #t) (kind 1)) (crate-dep (name "cc") (req "^1.0") (features (quote ("parallel"))) (default-features #t) (kind 1)) (crate-dep (name "curl") (req "^0.4") (default-features #t) (kind 1)) (crate-dep (name "flate2") (req "^1.0") (default-features #t) (kind 1)) (crate-dep (name "tar") (req "^0.4") (default-features #t) (kind 1)))) (hash "15hs7blfkzixv9721abqqibk9rl6yfmiywwav3028zmg21s5ck9n")))

(define-public crate-cblas-src-0.1 (crate (name "cblas-src") (vers "0.1.1") (deps (list (crate-dep (name "anyhow") (req "^1.0") (default-features #t) (kind 1)) (crate-dep (name "cblas") (req "^0.2.0") (default-features #t) (kind 2)) (crate-dep (name "cc") (req "^1.0") (features (quote ("parallel"))) (default-features #t) (kind 1)) (crate-dep (name "curl") (req "^0.4") (default-features #t) (kind 1)) (crate-dep (name "flate2") (req "^1.0") (default-features #t) (kind 1)) (crate-dep (name "openblas-src") (req "^0.9.0") (features (quote ("system"))) (default-features #t) (kind 2)) (crate-dep (name "tar") (req "^0.4") (default-features #t) (kind 1)))) (hash "1nc33d1xxnm01j1mzrw6fcnil5abwwc5xk28jsgbx928jfyzsng4")))

(define-public crate-cblas-src-0.1 (crate (name "cblas-src") (vers "0.1.2") (deps (list (crate-dep (name "anyhow") (req "^1.0") (default-features #t) (kind 1)) (crate-dep (name "cblas") (req "^0.2.0") (default-features #t) (kind 2)) (crate-dep (name "cc") (req "^1.0") (features (quote ("parallel"))) (default-features #t) (kind 1)) (crate-dep (name "curl") (req "^0.4") (default-features #t) (kind 1)) (crate-dep (name "flate2") (req "^1.0") (default-features #t) (kind 1)) (crate-dep (name "openblas-src") (req "^0.9.0") (features (quote ("system"))) (default-features #t) (kind 2)) (crate-dep (name "tar") (req "^0.4") (default-features #t) (kind 1)))) (hash "0fpmpggcghr4ja1qxmdf0mkswn06fabjpz79gy4xmbrvf5ba3ngy")))

(define-public crate-cblas-src-0.1 (crate (name "cblas-src") (vers "0.1.3") (deps (list (crate-dep (name "anyhow") (req "^1.0") (default-features #t) (kind 1)) (crate-dep (name "cblas") (req "^0.2.0") (default-features #t) (kind 2)) (crate-dep (name "cc") (req "^1.0") (features (quote ("parallel"))) (default-features #t) (kind 1)) (crate-dep (name "curl") (req "^0.4") (default-features #t) (kind 1)) (crate-dep (name "flate2") (req "^1.0") (default-features #t) (kind 1)) (crate-dep (name "openblas-src") (req "^0.9.0") (features (quote ("system"))) (default-features #t) (kind 2)) (crate-dep (name "tar") (req "^0.4") (default-features #t) (kind 1)))) (hash "1ppmdj00hnvdkgjhy6sx28lc8mvqh0n706kl2q3m7d4wmdcdni9a")))

(define-public crate-cblas-sys-0.0.1 (crate (name "cblas-sys") (vers "0.0.1") (hash "0fajb3q8rhbs62gz02n2c9shi52c1zyv4xi84c698vkmjwirpcj5")))

(define-public crate-cblas-sys-0.1 (crate (name "cblas-sys") (vers "0.1.0") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "03hs9ld2hcrypjq0zan5b0hnjfwzlkyczdgzdwi5gdwcwddsha8h")))

(define-public crate-cblas-sys-0.1 (crate (name "cblas-sys") (vers "0.1.1") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "0xbk3zji67bc8kj6dc8z9jfsciqcs6y1qkwss2415kl5zp0mnqlg")))

(define-public crate-cblas-sys-0.1 (crate (name "cblas-sys") (vers "0.1.2") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "08yysf12xymvnz9nx0qj0r5yxzki8rsaawriw9l87d81w4pgkrfi")))

(define-public crate-cblas-sys-0.1 (crate (name "cblas-sys") (vers "0.1.3") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "1y3w4ky3nsgf5kd28p0bxlr5ksjpnyhq0d6j5f9r1lqmjzyhh3pw")))

(define-public crate-cblas-sys-0.1 (crate (name "cblas-sys") (vers "0.1.4") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "0rgsn3klhhh09d8qf3b87zl4rwk93l2g0qzh9hhb0lff5kcfrzmn")))

(define-public crate-cblas_ffi-0.1 (crate (name "cblas_ffi") (vers "0.1.0") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "1s7yrnbxws2vrghprq5rnmx9a591yy8h6xpw8nf9sihfn0lfvj6k")))

