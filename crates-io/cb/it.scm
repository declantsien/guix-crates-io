(define-module (crates-io cb it) #:use-module (crates-io))

(define-public crate-cbit-0.1 (crate (name "cbit") (vers "0.1.0") (deps (list (crate-dep (name "proc-macro2") (req "^1.0.70") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.33") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2.0.39") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "0ir3c2759y66vvp2swwc0qfz29kjk8nprx0ik13gqnwklqa08g7b")))

(define-public crate-cbitmap-0.1 (crate (name "cbitmap") (vers "0.1.0") (hash "0jhl5yz9jscdn4vkqis9yxpm1qpf242f5rcn57jljakibimjzrax")))

(define-public crate-cbitmap-0.1 (crate (name "cbitmap") (vers "0.1.1") (hash "188kgna0n2wg78dgq0izxbf1s5h8pqb951i8jspd1m8priiqjn1h")))

(define-public crate-cbitmap-0.2 (crate (name "cbitmap") (vers "0.2.0") (hash "123wk75a4k3kvqmh6ra9wa1cr6x3p1x7hfa8frmqzrvrj53qm0xj")))

(define-public crate-cbitmap-0.3 (crate (name "cbitmap") (vers "0.3.0") (hash "02xwdl66wvnxvw1w5m9i8czhcmlxyjq2j0zmz0j7vzwrpgsxnz3r")))

(define-public crate-cbitmap-0.3 (crate (name "cbitmap") (vers "0.3.1") (deps (list (crate-dep (name "criterion") (req "^0.4") (features (quote ("html_reports"))) (default-features #t) (kind 2)))) (hash "0rq9m1wwxdy7n6818ksrchizamnvsbvk3fpqp1ky5ydgz3yws6j3")))

(define-public crate-cbitmap-0.3 (crate (name "cbitmap") (vers "0.3.2") (deps (list (crate-dep (name "criterion") (req "^0.4") (features (quote ("html_reports"))) (default-features #t) (kind 2)))) (hash "1i7nfvxdhn9pgnyf34vk43pfm34wl7204b8gl9vyrhdaxa0j2gr2")))

(define-public crate-cbitset-0.1 (crate (name "cbitset") (vers "0.1.0") (deps (list (crate-dep (name "num-traits") (req "^0.2.6") (kind 0)))) (hash "1b3yw3qwivnkvlyhxq9sa9fg77ffcfw6f77hwzbjv5335ykszag3")))

(define-public crate-cbitset-0.2 (crate (name "cbitset") (vers "0.2.0") (deps (list (crate-dep (name "num-traits") (req "^0.2.8") (kind 0)))) (hash "0ss6hjrbpqlir2ad4z2db0sb4yg15w5rfax11pxmjq99mqjsvdi9")))

