(define-module (crates-io kb uc) #:use-module (crates-io))

(define-public crate-kbucket-0.1 (crate (name "kbucket") (vers "0.1.0") (deps (list (crate-dep (name "rand") (req "^0.8.4") (default-features #t) (kind 0)))) (hash "1jf95l4pdwc5x3vb335z6bdg70jv35ha6q07vavlb4nlgfd4q94z")))

