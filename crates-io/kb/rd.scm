(define-module (crates-io kb rd) #:use-module (crates-io))

(define-public crate-kbrd-0.1 (crate (name "kbrd") (vers "0.1.0") (deps (list (crate-dep (name "pasts") (req "^0.12") (default-features #t) (kind 0)) (crate-dep (name "whisk") (req "^0.5") (features (quote ("pasts"))) (default-features #t) (kind 0)))) (hash "1qqmqappygkq0i83pp4khhrbdjasz5vzzrb8h2nrcm7qkcz306f8") (rust-version "1.60")))

