(define-module (crates-io kb ge) #:use-module (crates-io))

(define-public crate-kbgen-0.1 (crate (name "kbgen") (vers "0.1.1") (deps (list (crate-dep (name "anyhow") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^3.0.0-beta.2") (default-features #t) (kind 0)) (crate-dep (name "handlebars") (req "^3.5.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.117") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0") (default-features #t) (kind 0)))) (hash "05j3bbxsxg239xfdyvw0jl6pvqpyy2xw1ajr5wkipdxq62h0qnis")))

