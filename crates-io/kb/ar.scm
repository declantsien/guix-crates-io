(define-module (crates-io kb ar) #:use-module (crates-io))

(define-public crate-kbar-0.0.1 (crate (name "kbar") (vers "0.0.1") (deps (list (crate-dep (name "better_term") (req "^1.0.4") (default-features #t) (kind 0)) (crate-dep (name "crossterm") (req "^0.22.1") (optional #t) (default-features #t) (kind 0)))) (hash "0rgiv3sl097l9rkngr9mx78da9kx6c1g9k9q4sx56vx8xcg73ncx") (features (quote (("default" "crossterm"))))))

(define-public crate-kbar-0.0.2 (crate (name "kbar") (vers "0.0.2") (deps (list (crate-dep (name "better_term") (req "^1.0.4") (default-features #t) (kind 0)) (crate-dep (name "crossterm") (req "^0.22.1") (optional #t) (default-features #t) (kind 0)))) (hash "07pw9j9wci9r0440m9w8kg11hd1aj5wl8595ckzx3225rmlhskr9") (features (quote (("default" "crossterm"))))))

(define-public crate-kbar-0.0.3 (crate (name "kbar") (vers "0.0.3") (deps (list (crate-dep (name "better_term") (req "^1.0.4") (default-features #t) (kind 0)) (crate-dep (name "crossterm") (req "^0.22.1") (optional #t) (default-features #t) (kind 0)))) (hash "0dwz3rfqkms1p2mfg6wq24ndcjvvipzbd78sgskfrxn091l09kc7") (features (quote (("default" "crossterm"))))))

(define-public crate-kbar-0.0.4 (crate (name "kbar") (vers "0.0.4") (deps (list (crate-dep (name "better_term") (req "^1.0.4") (default-features #t) (kind 0)) (crate-dep (name "crossterm") (req "^0.22.1") (optional #t) (default-features #t) (kind 0)))) (hash "107liy4adz5x9ks4vzg0zgsxxwhx7fq3psq095c7fbd44j4ada1m") (features (quote (("default" "crossterm"))))))

(define-public crate-kbar-0.1 (crate (name "kbar") (vers "0.1.0") (deps (list (crate-dep (name "better_term") (req "^1.0.4") (default-features #t) (kind 0)) (crate-dep (name "crossterm") (req "^0.22.1") (optional #t) (default-features #t) (kind 0)))) (hash "04ycmz2pfgl3srv35swxa0v6z4fvdnsc8ddyqkxfi1qzx9qa4sya") (features (quote (("default" "crossterm"))))))

(define-public crate-kbar-0.1 (crate (name "kbar") (vers "0.1.1") (deps (list (crate-dep (name "better_term") (req "^1.0.4") (default-features #t) (kind 0)) (crate-dep (name "crossterm") (req "^0.22.1") (optional #t) (default-features #t) (kind 0)))) (hash "07ajw9bjkl8b2akcbwalf94dxzm8dcnfax938ki13gl6j5b7sbsg") (features (quote (("default" "crossterm"))))))

(define-public crate-kbar-0.1 (crate (name "kbar") (vers "0.1.2") (deps (list (crate-dep (name "terminal_size") (req "^0.1") (default-features #t) (kind 0)))) (hash "0q10h62fzmbrbshh2w82xl8pibb617l6hjbvl6nq4f112jk65gx5")))

(define-public crate-kbar-0.1 (crate (name "kbar") (vers "0.1.3") (deps (list (crate-dep (name "terminal_size") (req "^0.1") (default-features #t) (kind 0)))) (hash "0jzx1x2lda3sg4y1z5lkayvdq3m51f252phvns639q07xql646gg")))

(define-public crate-kbar-0.1 (crate (name "kbar") (vers "0.1.5") (deps (list (crate-dep (name "terminal_size") (req "^0.1") (default-features #t) (kind 0)))) (hash "0ypnl1xamb3748vrx5q4c0bc752ccnzp9zwxdw3zys5r3hdwivv6")))

