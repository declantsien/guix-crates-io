(define-module (crates-io kb pa) #:use-module (crates-io))

(define-public crate-kbparse-lib-0.2 (crate (name "kbparse-lib") (vers "0.2.0") (hash "1c8glq5cnbkrq9pbg3p2nqagiw3nxgxjihi3haq3shdfziiaxssd") (yanked #t)))

(define-public crate-kbparse-lib-0.3 (crate (name "kbparse-lib") (vers "0.3.0") (hash "0x99f46f8slda4nayz18617zdqfiq15y5ddy5s9jszaarr49yz5g") (yanked #t)))

(define-public crate-kbparse-lib-0.3 (crate (name "kbparse-lib") (vers "0.3.1") (hash "03ml8d5sqj7n5rf8ff58ql9bmmn1mimyn3v33a8vhpf1bs7cgl37")))

