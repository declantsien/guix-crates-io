(define-module (crates-io wg di) #:use-module (crates-io))

(define-public crate-wgdiff-0.1 (crate (name "wgdiff") (vers "0.1.0") (hash "0ai1j8f5x83k994qbc08i8r8g4nx97v3qdi6chmn9dwc5qz9wdrz")))

(define-public crate-wgdiff-0.2 (crate (name "wgdiff") (vers "0.2.0") (hash "0r5713y3m9nd6s487x7vv7jjl6qkm25zphpawkycsjz4z28xdrh0")))

(define-public crate-wgdiff-0.3 (crate (name "wgdiff") (vers "0.3.0") (deps (list (crate-dep (name "rand") (req "^0.8") (features (quote ("small_rng"))) (default-features #t) (kind 2)))) (hash "0q0ldqgmdw0jd91h7ywxizhln4phk364gdm9dplznyffpmyfnmz8")))

(define-public crate-wgdiff-0.4 (crate (name "wgdiff") (vers "0.4.0") (deps (list (crate-dep (name "rand") (req "^0.8") (features (quote ("small_rng"))) (default-features #t) (kind 2)))) (hash "08xy0cmlq5bbc2llpwn41yw3gqwcla1hsiwk4ddh8dhjmlqm55mc") (yanked #t)))

(define-public crate-wgdiff-0.4 (crate (name "wgdiff") (vers "0.4.1") (deps (list (crate-dep (name "rand") (req "^0.8") (features (quote ("small_rng"))) (default-features #t) (kind 2)))) (hash "0ybmipgb8nrphpynglxjg9z1h7m74dg7jnqjck6pz8sx4gqbdfhz") (yanked #t)))

(define-public crate-wgdiff-0.4 (crate (name "wgdiff") (vers "0.4.2") (deps (list (crate-dep (name "rand") (req "^0.8") (features (quote ("small_rng"))) (default-features #t) (kind 2)))) (hash "1j7ajsfddyz635zyl5l8q46ngp1rq3bmll6kgzzvdyvxm4iyd4h8") (yanked #t)))

(define-public crate-wgdiff-0.4 (crate (name "wgdiff") (vers "0.4.3") (deps (list (crate-dep (name "rand") (req "^0.8") (features (quote ("small_rng"))) (default-features #t) (kind 2)))) (hash "1i578rvsmbdawf85dnr5xi8galc53qqm93zrlyw0b1w4pwlsyj62")))

