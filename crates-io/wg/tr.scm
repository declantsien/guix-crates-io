(define-module (crates-io wg tr) #:use-module (crates-io))

(define-public crate-wgtr-ecs-0.1 (crate (name "wgtr-ecs") (vers "0.1.0") (hash "03byyvz7zxwf3s7i5abqdg5lxn99zp0glrzgw4kafslgxf81mvz3")))

(define-public crate-wgtr-ecs-0.2 (crate (name "wgtr-ecs") (vers "0.2.0") (hash "0fjcwycrlsr8xzscbbbr6zb6x8h20a1s0j0a4qgcx3dckggxg714")))

(define-public crate-wgtr-scene-manager-0.1 (crate (name "wgtr-scene-manager") (vers "0.1.0") (hash "0kl4ghll3alr17f9f25zbclqki01ck6gxfpx5ilpgqw5qqmg220g")))

(define-public crate-wgtr-scene-manager-0.2 (crate (name "wgtr-scene-manager") (vers "0.2.0") (hash "1jpwq25fa7d0lxssilx3nh7dgp6w08fm6fr9sswbhccqqy2kfbr0")))

