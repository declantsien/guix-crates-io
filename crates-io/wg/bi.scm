(define-module (crates-io wg bi) #:use-module (crates-io))

(define-public crate-wgbind-0.1 (crate (name "wgbind") (vers "0.1.0") (deps (list (crate-dep (name "bindgen") (req "^0.65.1") (default-features #t) (kind 1)) (crate-dep (name "cc") (req "^1.0") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2.150") (default-features #t) (kind 0)))) (hash "0bp8y85diz0434p2qjn3c6yx9mcx7df971xnjdjdkswddh4w6zs4")))

(define-public crate-wgbind-0.1 (crate (name "wgbind") (vers "0.1.1") (deps (list (crate-dep (name "bindgen") (req "^0.65.1") (default-features #t) (kind 1)) (crate-dep (name "cc") (req "^1.0") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2.150") (default-features #t) (kind 0)))) (hash "1gn36xd8ys7cqldpi2acrvxmyi6a1x2qm1qgjgfcd0fbj2c6f4pg")))

(define-public crate-wgbind-0.1 (crate (name "wgbind") (vers "0.1.2") (deps (list (crate-dep (name "bindgen") (req "^0.65.1") (default-features #t) (kind 1)) (crate-dep (name "cc") (req "^1.0") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2.150") (default-features #t) (kind 0)))) (hash "0v8nk7gsnfqiskm09pw423gr3gqn9i39rj31qbxz90mp4rs0pvrr")))

(define-public crate-wgbind-0.1 (crate (name "wgbind") (vers "0.1.3") (deps (list (crate-dep (name "bindgen") (req "^0.65.1") (default-features #t) (kind 1)) (crate-dep (name "cc") (req "^1.0") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2.150") (default-features #t) (kind 0)))) (hash "0ip2xjy6520k65r16vrv70gkfdv4yl5ka71sny6rvj3pxkkv8zpl")))

(define-public crate-wgbind-0.1 (crate (name "wgbind") (vers "0.1.4") (deps (list (crate-dep (name "libc") (req "^0.2.150") (default-features #t) (kind 0)) (crate-dep (name "wgbindraw-sys") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "18vg327zwcgzz17dc4g434898w3apbxb8sl3vs662nrw4h6w81yc")))

(define-public crate-wgbind-0.2 (crate (name "wgbind") (vers "0.2.0") (deps (list (crate-dep (name "libc") (req "^0.2.150") (default-features #t) (kind 0)) (crate-dep (name "wgbindraw-sys") (req "^0.2.0") (default-features #t) (kind 0)))) (hash "1irr7j4dgd73irlnwabmss32n5di3f7gf3nyigxpi4r641gkfs0p")))

(define-public crate-wgbind-0.2 (crate (name "wgbind") (vers "0.2.1") (deps (list (crate-dep (name "libc") (req "^0.2.150") (default-features #t) (kind 0)) (crate-dep (name "wgbindraw-sys") (req "^0.2.1") (default-features #t) (kind 0)))) (hash "1jkmh6dcrw4c0pzxpzxn49rs1w68ln0lnrvag00n46370lcg9r8n")))

(define-public crate-wgbind-0.2 (crate (name "wgbind") (vers "0.2.2") (deps (list (crate-dep (name "libc") (req "^0.2.150") (default-features #t) (kind 0)) (crate-dep (name "wgbindraw-sys") (req "^0.2.1") (default-features #t) (kind 0)))) (hash "06p06m8xizsrzlvrwdd6wgl57sl7ivkb3c9k43p6yn4al5by2yca")))

(define-public crate-wgbindraw-sys-0.1 (crate (name "wgbindraw-sys") (vers "0.1.0") (deps (list (crate-dep (name "bindgen") (req "^0.69.1") (default-features #t) (kind 1)) (crate-dep (name "cc") (req "^1.0") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2.150") (default-features #t) (kind 0)))) (hash "0kg41ja70hh9bf54sgwmajm9j2kbzs2fzpb0z47h7yna8pvnpa5r")))

(define-public crate-wgbindraw-sys-0.2 (crate (name "wgbindraw-sys") (vers "0.2.0") (deps (list (crate-dep (name "bindgen") (req "^0.69.1") (default-features #t) (kind 1)) (crate-dep (name "cc") (req "^1.0") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2.150") (default-features #t) (kind 0)))) (hash "07fxz7vz3x8q3c8k1pry39v00lp9rr3dydqjfxfn82wzaa0rgm8j")))

(define-public crate-wgbindraw-sys-0.2 (crate (name "wgbindraw-sys") (vers "0.2.1") (deps (list (crate-dep (name "bindgen") (req "^0.69.1") (default-features #t) (kind 1)) (crate-dep (name "cc") (req "^1.0") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2.150") (default-features #t) (kind 0)))) (hash "1x74ijrw2n5qr5rml6fbzqi2i21n2938h1mdqmn6mnx54m8yd1mb")))

