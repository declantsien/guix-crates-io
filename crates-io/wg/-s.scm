(define-module (crates-io wg -s) #:use-module (crates-io))

(define-public crate-wg-sys-0.0.1 (crate (name "wg-sys") (vers "0.0.1") (hash "1jqsmmnf1m50zhm0iq1953hmd6ddj1x84v31xx3x0kxf7xz4fl93")))

(define-public crate-wg-sys-0.1 (crate (name "wg-sys") (vers "0.1.0") (deps (list (crate-dep (name "bindgen") (req "^0.59.2") (default-features #t) (kind 1)) (crate-dep (name "cc") (req "^1.0.73") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2.125") (default-features #t) (kind 0)))) (hash "0p5jyf4wvlnrj97i8q064jq0x8vxlp0hf2spyz9lllgwa2hc8m4i")))

