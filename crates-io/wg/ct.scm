(define-module (crates-io wg ct) #:use-module (crates-io))

(define-public crate-wgctl-0.0.0 (crate (name "wgctl") (vers "0.0.0") (hash "10aglvc0qgid6x1s57wmbz0as59q95wyrq4svbf99vg9n8x1z31m")))

(define-public crate-wgctl-sys-0.0.0 (crate (name "wgctl-sys") (vers "0.0.0") (hash "0cvw7m8swx559xdic2p94qib7nqxql8dwlz2h073k5ynzbrl7cdx")))

(define-public crate-wgctrl-0.0.1 (crate (name "wgctrl") (vers "0.0.1") (hash "001cagj44ca69lrags8r6mvrb8lf9gydmlnnh2gwcak7pcwwxz0r")))

(define-public crate-wgctrl-0.0.2 (crate (name "wgctrl") (vers "0.0.2") (deps (list (crate-dep (name "anyhow") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "base64") (req "^0.22.0") (default-features #t) (kind 0)) (crate-dep (name "ipnet") (req "^2.9.0") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "x25519-dalek") (req "^2.0.1") (features (quote ("static_secrets"))) (default-features #t) (kind 0)))) (hash "16vnmydf7y5fdizld4wmrkirkrzpnq8saal33kbqnhqrrvmx9jgn")))

(define-public crate-wgctrl-0.0.3 (crate (name "wgctrl") (vers "0.0.3") (deps (list (crate-dep (name "anyhow") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "base64") (req "^0.22.0") (default-features #t) (kind 0)) (crate-dep (name "ipnet") (req "^2.9.0") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "x25519-dalek") (req "^2.0.1") (features (quote ("static_secrets"))) (default-features #t) (kind 0)))) (hash "0sy24jplyvapygwx0wrqkd8b831isqkaqzhnf102p8a78k5nappw")))

(define-public crate-wgctrl-rs-0.1 (crate (name "wgctrl-rs") (vers "0.1.0") (deps (list (crate-dep (name "chrono") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "wgctrl-sys") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "0q03861m6cy5vx1afqngb5vvf93fbh844lgkdrgiqdbzpkw1byv8")))

(define-public crate-wgctrl-sys-0.1 (crate (name "wgctrl-sys") (vers "0.1.0") (deps (list (crate-dep (name "bindgen") (req "^0.39") (default-features #t) (kind 1)) (crate-dep (name "cc") (req "^1.0") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "0xg9332h9skvnac95m4mnm0rfq58qkgfzcgxn1j94ql2r2s6paln")))

