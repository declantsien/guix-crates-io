(define-module (crates-io wg im) #:use-module (crates-io))

(define-public crate-wgimage-0.1 (crate (name "wgimage") (vers "0.1.0") (deps (list (crate-dep (name "bytemuck") (req "^1.13.1") (default-features #t) (kind 0)) (crate-dep (name "futures") (req "^0.3.28") (default-features #t) (kind 0)) (crate-dep (name "image") (req "^0.24.6") (default-features #t) (kind 0)) (crate-dep (name "wgpu") (req "^0.16.1") (default-features #t) (kind 0)))) (hash "0hsygd6bnkcbcd8c2dsqygcpz76kayr5d967h29ix6z1bfc58ms3")))

(define-public crate-wgimage-0.2 (crate (name "wgimage") (vers "0.2.0") (deps (list (crate-dep (name "bytemuck") (req "^1.13.1") (default-features #t) (kind 0)) (crate-dep (name "futures") (req "^0.3.28") (default-features #t) (kind 0)) (crate-dep (name "image") (req "^0.24.6") (default-features #t) (kind 0)) (crate-dep (name "wgpu") (req "^0.16.1") (default-features #t) (kind 0)))) (hash "0mi97nng80hc44zwf8iq7i7c13dkv0qm94ipwxfra1pwn9i90ch5")))

