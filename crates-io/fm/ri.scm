(define-module (crates-io fm ri) #:use-module (crates-io))

(define-public crate-fmri-0.1 (crate (name "fmri") (vers "0.1.0") (deps (list (crate-dep (name "serde") (req "^1") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "1zzy9nxygvlmm02f3qb1g5ppxg811mc1frf61h8w2wcjisvcrwnb")))

(define-public crate-fmri-0.1 (crate (name "fmri") (vers "0.1.1") (deps (list (crate-dep (name "serde") (req "^1") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "0282g6zqh4j1qv6bzn7pv12sak37bbbkh6dx6g6nyak8qxgy06c5")))

(define-public crate-fmri-0.2 (crate (name "fmri") (vers "0.2.0") (deps (list (crate-dep (name "serde") (req "^1") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "06y8yba604icpaddjy34m53h6ms9c8v6h5v3y10cpx0n8gqwqhkw")))

(define-public crate-fmri-0.2 (crate (name "fmri") (vers "0.2.1") (deps (list (crate-dep (name "serde") (req "^1") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "1xfns49hm283gqg8lx551shdkk44jx70zn22k7w0jw4lv3hn64p9")))

