(define-module (crates-io fm nl) #:use-module (crates-io))

(define-public crate-fmnl-0.1 (crate (name "fmnl") (vers "0.1.0") (deps (list (crate-dep (name "atty") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.3") (default-features #t) (kind 0)))) (hash "1vfcybl0v6iawp110i03zj0f2kf38yf5x6cah5bk2shzp25rgg8r")))

