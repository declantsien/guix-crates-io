(define-module (crates-io fm at) #:use-module (crates-io))

(define-public crate-fmath-0.1 (crate (name "fmath") (vers "0.1.0") (hash "0c3hncr2m7j76j36qqdw8qyarggylj9lakmbqf8x04g60aw5qd0k") (yanked #t)))

(define-public crate-fmath-0.2 (crate (name "fmath") (vers "0.2.0") (hash "0pabhgl984djw0iqcihla1wc2rd97xwapwpl42576311n4l2bgb3")))

(define-public crate-fmath-0.2 (crate (name "fmath") (vers "0.2.1") (hash "1g42bzpziqlzrcjh61rhvj6cpq3r70g64w0cdlh6bi05k1wnhvyk")))

(define-public crate-fmath-0.2 (crate (name "fmath") (vers "0.2.2") (hash "0hydjbx3gb5n8n6i5mrckdcnsahj6k7cj9wkg6ghq9sys1jb5id9")))

(define-public crate-fmath-0.2 (crate (name "fmath") (vers "0.2.3") (hash "16c8jwlbgzvxpdvy8jf2v1hbq9i2c1qfmxm256sa3vs2rrr932jp")))

(define-public crate-fmath-0.2 (crate (name "fmath") (vers "0.2.4") (hash "191yhwxb4361yl6pfg3hhip0wfrf3h44fx4pj9xgjnlfp0w5ri2n")))

