(define-module (crates-io fm ap) #:use-module (crates-io))

(define-public crate-fmap-0.0.1 (crate (name "fmap") (vers "0.0.1") (hash "0qapa9b7jynyrmppczgq6sq0rfj3ilg9h67x7sl60y6kkznryji5")))

(define-public crate-fmap-0.0.2 (crate (name "fmap") (vers "0.0.2") (hash "0yr4xhjmgsw5c86936ny1cgksvyzgz18yb55pmi8vakvw14qlr4z")))

(define-public crate-fmap-0.0.3 (crate (name "fmap") (vers "0.0.3") (hash "18v362x6jnx719mg6xxfagih22dr6hkh0r3q3dckkavwgqjhhh44")))

(define-public crate-fmap-0.0.4 (crate (name "fmap") (vers "0.0.4") (hash "0633ryzjphlb0yky40d3s49cs4jw1gizhcv4dmibkhl99hql43y6")))

(define-public crate-fmap-0.0.5 (crate (name "fmap") (vers "0.0.5") (hash "0jqfh0jylxwl8zvhc5k5vgbncnlzy2si6inxkn3xsn8880d27rkv")))

(define-public crate-fmap-0.0.6 (crate (name "fmap") (vers "0.0.6") (hash "0sdqivrll821496kzlqyzal219lks6fl5n95dqkqlwf36ylfqg5r")))

(define-public crate-fmap-0.1 (crate (name "fmap") (vers "0.1.0") (hash "0a1h3dnp0wqj0qzwm3iydic2qjfc2pr2zgzymf3qaa1hlp3y20js")))

(define-public crate-fmap-0.2 (crate (name "fmap") (vers "0.2.0") (hash "1s1868sfkxz1qrr4naaqdxw8i866kl40n0480hwjpc9qjl5di2zp")))

(define-public crate-fmap-0.2 (crate (name "fmap") (vers "0.2.1") (hash "07nznciqbrbspzgnch4jdx169v7bsmsxaz3vkr32c5jwc5l9k07b")))

(define-public crate-fmap-0.2 (crate (name "fmap") (vers "0.2.2") (hash "07vq1akwx7gaf9xcbjm0ni0m7df8v4ax5ajd23x4y9mysvmbgjdw") (yanked #t)))

(define-public crate-fmap-0.3 (crate (name "fmap") (vers "0.3.0") (hash "087akbyazwjv1qy0np1vdis5qhxvfxg7rr9d7hm0bpak3g2fdhm6")))

(define-public crate-fmap-0.3 (crate (name "fmap") (vers "0.3.1") (hash "1h9hfli5fxn6bsq2cb2l1lkkjrw402mn2dji2hf1wwv1p1wfhab2")))

(define-public crate-fmap-0.3 (crate (name "fmap") (vers "0.3.2") (hash "1c7sdc3c8n670nlwydlc0j38k1zm9v7l8z8hvfc8n998syk6ma8b")))

(define-public crate-fmap-0.4 (crate (name "fmap") (vers "0.4.0") (hash "05yw3scd771a36ds0jrddaykkkrsjqmd378xrfngkz8qz50z4ijl")))

(define-public crate-fmap-0.4 (crate (name "fmap") (vers "0.4.1") (hash "1pskvd9fpsn11w3j433y6w03yhgbpipxbn6x9k58d4vw1lzgjv7x")))

(define-public crate-fmap-0.5 (crate (name "fmap") (vers "0.5.0") (hash "1wx7lpbq1rpflkhpmwnqhldp4vxijcnjykdmjk448v1xvfi02aij")))

(define-public crate-fmap-0.5 (crate (name "fmap") (vers "0.5.1") (hash "056k1ylrih8s9bd7z1b4d0z4zfhfnxdlib5r77rbksv8vnh3y8py")))

(define-public crate-fmap-0.6 (crate (name "fmap") (vers "0.6.0") (deps (list (crate-dep (name "futures") (req "^0.3.28") (default-features #t) (kind 2)))) (hash "1iazsfawwxnpbqanqgyiszapq1rns34qrsin68lnnasj3pvscnc3")))

(define-public crate-fmap-0.7 (crate (name "fmap") (vers "0.7.0") (deps (list (crate-dep (name "futures") (req "^0.3.28") (default-features #t) (kind 2)))) (hash "0xjymsnvv0sb7n53q08rvg577z9ami9ijl9xrjqlcz132mmainb4")))

(define-public crate-fmap-0.8 (crate (name "fmap") (vers "0.8.0") (deps (list (crate-dep (name "futures") (req "^0.3.28") (default-features #t) (kind 2)))) (hash "1zbvb80202fvhxd4hl6y1hdf5jq8xlynlc9l186w42chl5n0di1j")))

(define-public crate-fmap-0.8 (crate (name "fmap") (vers "0.8.1") (deps (list (crate-dep (name "futures") (req "^0.3.28") (default-features #t) (kind 2)))) (hash "17sms8jdv3mraxlmsn5yp1640cyf0gcqbp5vqm2ip55r98xyz45j")))

(define-public crate-fmap-0.8 (crate (name "fmap") (vers "0.8.2") (deps (list (crate-dep (name "futures") (req "^0.3.28") (default-features #t) (kind 2)))) (hash "0v21dnv5xy5wjbi7ikk2j4rpwlv84sp06ps5ixar3818ygwmjj3c")))

(define-public crate-fmap-0.8 (crate (name "fmap") (vers "0.8.3") (deps (list (crate-dep (name "futures") (req "^0.3.28") (default-features #t) (kind 2)))) (hash "1v64d6qygp60crnkfs05bwvk35jpma5jzc1q3sp2pcgp9c26rbld")))

