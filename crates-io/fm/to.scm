(define-module (crates-io fm to) #:use-module (crates-io))

(define-public crate-fmtools-0.1 (crate (name "fmtools") (vers "0.1.0") (deps (list (crate-dep (name "obfstr") (req "^0.3") (optional #t) (default-features #t) (kind 0)))) (hash "1y7lsbbcjbfx52b2ld1d27h7yj63cqwk1hliipccyhf2gnvgjazp") (features (quote (("std") ("default" "std"))))))

(define-public crate-fmtools-0.1 (crate (name "fmtools") (vers "0.1.1") (deps (list (crate-dep (name "obfstr") (req "^0.3") (optional #t) (default-features #t) (kind 0)))) (hash "1impsx1zd54cj02vw1qv5iskx0vjgb9wxb6wi8vwlmnfxgxlbr06") (features (quote (("std") ("default" "std"))))))

(define-public crate-fmtools-0.1 (crate (name "fmtools") (vers "0.1.2") (deps (list (crate-dep (name "obfstr") (req "^0.4") (optional #t) (default-features #t) (kind 0)))) (hash "06hx6lx24gpw63hlw6crjqx7mqrwzz70i6fnf2wzcizc9rnyv0zr") (features (quote (("std") ("default" "std"))))))

(define-public crate-fmtor-0.1 (crate (name "fmtor") (vers "0.1.0") (hash "1jagdnq6k97kmd5krqrzkafpydpcr23l6i9w0pd3970jiz0zhxdh")))

(define-public crate-fmtor-0.1 (crate (name "fmtor") (vers "0.1.1") (hash "1fdlh4ai5gnhqz8rbx0xd5gb7z96r858d0grixkkq4nckahjpwxv")))

(define-public crate-fmtor-0.1 (crate (name "fmtor") (vers "0.1.2") (hash "1lnh1a7wx7c6p6q2gdhhswn5dssnjxb161k47w7hp2f4s1ilzsj7")))

