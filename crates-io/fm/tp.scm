(define-module (crates-io fm tp) #:use-module (crates-io))

(define-public crate-fmtparse-0.1 (crate (name "fmtparse") (vers "0.1.0") (deps (list (crate-dep (name "ariadne") (req "^0.1") (default-features #t) (kind 2)) (crate-dep (name "chumsky") (req "^0.9") (default-features #t) (kind 0)))) (hash "1jk8ypaw9ivjkbic644ad6010711ky1zs19gzv1yb12ialqajigm")))

(define-public crate-fmtparse-0.2 (crate (name "fmtparse") (vers "0.2.0") (deps (list (crate-dep (name "ariadne") (req "^0.1") (default-features #t) (kind 2)) (crate-dep (name "chumsky") (req "^0.9") (default-features #t) (kind 0)))) (hash "191byvw66wi48piz5j171rx983apidzd2g90p54g1zz1nvsj6y9v")))

