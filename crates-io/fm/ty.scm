(define-module (crates-io fm ty) #:use-module (crates-io))

(define-public crate-fmty-0.1 (crate (name "fmty") (vers "0.1.0") (deps (list (crate-dep (name "proptest") (req "^1") (default-features #t) (kind 2)) (crate-dep (name "test-strategy") (req "^0.3") (default-features #t) (kind 2)))) (hash "1g7w03c8jhmrr6bjfn70pqgahxs093b2wzkfmhk5wr85pv3h48dh") (rust-version "1.56.0")))

(define-public crate-fmty-0.1 (crate (name "fmty") (vers "0.1.1") (deps (list (crate-dep (name "proptest") (req "^1") (default-features #t) (kind 2)) (crate-dep (name "test-strategy") (req "^0.3") (default-features #t) (kind 2)))) (hash "0s3h0bjvf2xsp5qxz44w7zak5a2qpq8xga6jz1iz4vjp8dvfaiqp") (rust-version "1.56.0")))

