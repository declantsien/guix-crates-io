(define-module (crates-io fm ta) #:use-module (crates-io))

(define-public crate-fmtastic-0.1 (crate (name "fmtastic") (vers "0.1.0") (hash "1p58ygw8gpk1agnfbr5rkc32lp7misn58xm3n27y4pyhsn7piq01")))

(define-public crate-fmtastic-0.2 (crate (name "fmtastic") (vers "0.2.0") (hash "018vhry0qnlpsc37chb6y0wz3q5cz7qfi8n2wxwkw8cvp1p7r6ny") (rust-version "1.75.0")))

