(define-module (crates-io fm od) #:use-module (crates-io))

(define-public crate-fmod-0.9 (crate (name "fmod") (vers "0.9.5") (hash "0jy6yrazwvwksijqzfkr0gs7zhkqvkwzh3m1pbh1417s17hk5zgi")))

(define-public crate-fmod-0.9 (crate (name "fmod") (vers "0.9.6") (hash "0d6sgkr2b20ds88lc69v4lxzpf1k8gmdlhvprkz314q48v2kqrj2")))

(define-public crate-fmod-0.9 (crate (name "fmod") (vers "0.9.7") (hash "0ihr1nl2wnrwywgv2vkcm5wvhp99qqimj5gavzcxad0xnfa4sja8")))

(define-public crate-fmod-0.9 (crate (name "fmod") (vers "0.9.8") (hash "12mx5wnz2kk80fwlp6bdcj9n4agk2bj9v6dm49knmdw8ajadfk7q")))

(define-public crate-fmod-0.9 (crate (name "fmod") (vers "0.9.9") (hash "0hfvh4vc7q2ybknllmzwn7gsr3qx9lycd9j1r6aggmbic9jnp52r")))

(define-public crate-fmod-0.9 (crate (name "fmod") (vers "0.9.10") (deps (list (crate-dep (name "c_str") (req "^1.0.0") (default-features #t) (kind 0)) (crate-dep (name "c_vec") (req "^1.0.0") (default-features #t) (kind 0)))) (hash "10dp7rn0hmjgpfjqsy3r66ki4fm2n3wdvfqwf3awzj5a4dg9zdv5")))

(define-public crate-fmod-0.9 (crate (name "fmod") (vers "0.9.11") (deps (list (crate-dep (name "c_str") (req "^1.0.0") (default-features #t) (kind 0)) (crate-dep (name "c_vec") (req "^1.0.0") (default-features #t) (kind 0)))) (hash "0x4aqljhw61siy63jyh99qb7sykjgx0kflzyf7a87c422rh6lzdj")))

(define-public crate-fmod-0.9 (crate (name "fmod") (vers "0.9.12") (deps (list (crate-dep (name "c_str") (req "^1.0.0") (default-features #t) (kind 0)) (crate-dep (name "c_vec") (req "^1.0.0") (default-features #t) (kind 0)))) (hash "12y8disr98qy51ckv31y7nfn3ksghamc2gbdnzhc4lxlgf76wasm")))

(define-public crate-fmod-0.9 (crate (name "fmod") (vers "0.9.13") (deps (list (crate-dep (name "c_str") (req "^1.0.0") (default-features #t) (kind 0)) (crate-dep (name "c_vec") (req "^1.0.0") (default-features #t) (kind 0)))) (hash "02apszxy1bdy39ciddk06a0qpiwkj9zbczk9anj8099n52s764rv")))

(define-public crate-fmod-0.9 (crate (name "fmod") (vers "0.9.14") (deps (list (crate-dep (name "c_str") (req "^1.0.0") (default-features #t) (kind 0)) (crate-dep (name "c_vec") (req "^1.0.0") (default-features #t) (kind 0)))) (hash "03p7wq1y97zq83kw0nx9n5nn1szmvnrmrryv5kj5zf4rdkyyv098")))

(define-public crate-fmod-0.9 (crate (name "fmod") (vers "0.9.15") (deps (list (crate-dep (name "c_str") (req "^1.0.0") (default-features #t) (kind 0)) (crate-dep (name "c_vec") (req "^1.0.0") (default-features #t) (kind 0)))) (hash "10vcsdxn6g5rm3i8b5a26r57lxg6vhkbkp1d0w7rb3i914lklfi2")))

(define-public crate-fmod-0.9 (crate (name "fmod") (vers "0.9.16") (deps (list (crate-dep (name "byteorder") (req "*") (default-features #t) (kind 0)) (crate-dep (name "c_str") (req "^1.0.0") (default-features #t) (kind 0)) (crate-dep (name "c_vec") (req "^1.0.0") (default-features #t) (kind 0)))) (hash "1nwpqpqi5id6prlz7nrgjv4ny9cqv860pv21rrzvbbi4awp9rnkd")))

(define-public crate-fmod-0.9 (crate (name "fmod") (vers "0.9.17") (deps (list (crate-dep (name "byteorder") (req "*") (default-features #t) (kind 0)) (crate-dep (name "c_str") (req "^1.0.0") (default-features #t) (kind 0)) (crate-dep (name "c_vec") (req "^1.0.0") (default-features #t) (kind 0)))) (hash "1nqj20d2zx77p8xzpdqjzxrdwwxig6d3v2c7sbfcldb7qhwlakpm")))

(define-public crate-fmod-0.9 (crate (name "fmod") (vers "0.9.18") (deps (list (crate-dep (name "byteorder") (req "*") (default-features #t) (kind 0)) (crate-dep (name "c_vec") (req "^1.0.0") (default-features #t) (kind 0)))) (hash "0dvny7rjf84yh75ipggp3v42j8s1730abrqjfylk9kjfl3flj777")))

(define-public crate-fmod-0.9 (crate (name "fmod") (vers "0.9.19") (deps (list (crate-dep (name "byteorder") (req "*") (default-features #t) (kind 0)) (crate-dep (name "c_vec") (req "~1.0") (default-features #t) (kind 0)))) (hash "1njc7m2cwwbvjnf05dsxh166bqjzg307pfpn2bhbqcvcn6064kj3")))

(define-public crate-fmod-0.9 (crate (name "fmod") (vers "0.9.20") (deps (list (crate-dep (name "byteorder") (req "*") (default-features #t) (kind 0)) (crate-dep (name "c_vec") (req "~1.0") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "*") (default-features #t) (kind 0)))) (hash "0vnb8jx6bb8ki47c60c6kch2rd8kq4jcwd4kcw4makpczm98b7yp")))

(define-public crate-fmod-0.9 (crate (name "fmod") (vers "0.9.21") (deps (list (crate-dep (name "byteorder") (req "*") (default-features #t) (kind 0)) (crate-dep (name "c_vec") (req "~1.0") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "*") (default-features #t) (kind 0)))) (hash "0p9cmrdsl79ail2i919flycbd9879cfk8ga5ii2rpm509mb5hl3s")))

(define-public crate-fmod-0.9 (crate (name "fmod") (vers "0.9.22") (deps (list (crate-dep (name "byteorder") (req "^0.4.2") (default-features #t) (kind 0)) (crate-dep (name "c_vec") (req "~1.0") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2.6") (default-features #t) (kind 0)))) (hash "1bp0rv2sgaa2109bsdp033krxs7a19hw15msfzpibcn7pzfynhd0")))

(define-public crate-fmod-0.9 (crate (name "fmod") (vers "0.9.24") (deps (list (crate-dep (name "byteorder") (req "^1.2") (default-features #t) (kind 0)) (crate-dep (name "c_vec") (req "~1.0") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2.6") (default-features #t) (kind 0)))) (hash "0dvyv0r3flz12izkpyjzniap0jfv4gmn8z776rah2xs7cbbfylig")))

(define-public crate-fmod-0.10 (crate (name "fmod") (vers "0.10.0") (deps (list (crate-dep (name "byteorder") (req "^1.2") (default-features #t) (kind 0)) (crate-dep (name "c_vec") (req "~1.0") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2.6") (default-features #t) (kind 0)))) (hash "018cm2y8pww9dswqrkkmn6bw61xp2g5xhqrqcmkf66mcrac80n02")))

(define-public crate-fmod-0.10 (crate (name "fmod") (vers "0.10.1") (deps (list (crate-dep (name "byteorder") (req "^1.2") (default-features #t) (kind 0)) (crate-dep (name "c_vec") (req "~1.0") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2.6") (default-features #t) (kind 0)))) (hash "08jchj889ydxgi0fiamq0pxfbk9w2d6pf69cfcrdfkyvl36arizv")))

(define-public crate-fmod-0.10 (crate (name "fmod") (vers "0.10.2") (deps (list (crate-dep (name "byteorder") (req "^1.2") (default-features #t) (kind 0)) (crate-dep (name "c_vec") (req "~1.0") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2.6") (default-features #t) (kind 0)))) (hash "07gz8cvad45bdcxsmsx4rs0jgx2zvb3qxhmlk3ig46wp6xjnyy6g")))

(define-public crate-fmod-core-sys-0.0.0 (crate (name "fmod-core-sys") (vers "0.0.0") (hash "1q1jas8lal7ldbhqm6hblz89vgkhdg75nxqwijfvbyjfrn6b5n1r")))

(define-public crate-fmod-fsbank-sys-0.0.0 (crate (name "fmod-fsbank-sys") (vers "0.0.0") (hash "1a6i1awil6gwb3p8xjc7cffwl6kvj4zn6jkdpckc7h37l4r9ha5y")))

(define-public crate-fmod-rs-0.0.0 (crate (name "fmod-rs") (vers "0.0.0") (hash "0v1hwyb9dmkh8bl3kar7iwavqs3b84kpaccv1d43qic8blpf7gxz")))

(define-public crate-fmod-studio-sys-0.0.0 (crate (name "fmod-studio-sys") (vers "0.0.0") (hash "0qb0z4c9balj7pgvxrsv61s1fnphnlhpig96v0scw1sw2f1l17qg")))

(define-public crate-fmod-sys-0.1 (crate (name "fmod-sys") (vers "0.1.0") (deps (list (crate-dep (name "bindgen") (req "^0.53") (default-features #t) (kind 1)))) (hash "0xm6lqyvm1qqi52rfymqcxh4vs6lrrfxr565l3jga227faaswpq1") (features (quote (("studio") ("default")))) (yanked #t)))

(define-public crate-fmod-sys-0.1 (crate (name "fmod-sys") (vers "0.1.1") (deps (list (crate-dep (name "bindgen") (req "^0.53") (default-features #t) (kind 1)))) (hash "0a2hb8n836pfnyxi784nm8r5mxwsxzzikbaacy7g3h7ibzpr97kv") (features (quote (("studio") ("default")))) (yanked #t)))

(define-public crate-fmodel-0.1 (crate (name "fmodel") (vers "0.1.0") (hash "1q1n9gjcj46fpzb8waniapla062fb6vyz86vdk416dn0il816zdc")))

(define-public crate-fmodel-rust-0.1 (crate (name "fmodel-rust") (vers "0.1.0") (deps (list (crate-dep (name "async-trait") (req "^0.1.49") (default-features #t) (kind 0)) (crate-dep (name "derive_more") (req "^0.99.17") (default-features #t) (kind 2)) (crate-dep (name "tokio") (req "^1.0.0") (features (quote ("rt" "rt-multi-thread" "macros"))) (default-features #t) (kind 2)))) (hash "1g041zxwv2p128kkc22vik8vpk5504ixacdj8vg67rll7457jq5l")))

(define-public crate-fmodel-rust-0.2 (crate (name "fmodel-rust") (vers "0.2.0") (deps (list (crate-dep (name "async-trait") (req "^0.1.73") (default-features #t) (kind 0)) (crate-dep (name "derive_more") (req "^0.99.17") (default-features #t) (kind 2)) (crate-dep (name "tokio") (req "^1.32.0") (features (quote ("rt" "rt-multi-thread" "macros"))) (default-features #t) (kind 2)))) (hash "16v08z43dq46bwalh79n0xcdi2vbkqrirlgwdzxdnjqmkzp599qd")))

(define-public crate-fmodel-rust-0.3 (crate (name "fmodel-rust") (vers "0.3.0") (deps (list (crate-dep (name "async-trait") (req "^0.1.74") (default-features #t) (kind 0)) (crate-dep (name "derive_more") (req "^0.99.17") (default-features #t) (kind 2)) (crate-dep (name "tokio") (req "^1.33.0") (features (quote ("rt" "rt-multi-thread" "macros"))) (default-features #t) (kind 2)))) (hash "1vnsi604wficf1wx2cvqf0i2ws735c2v78p4p8ckpbvl3hm5gs9m")))

(define-public crate-fmodel-rust-0.4 (crate (name "fmodel-rust") (vers "0.4.0") (deps (list (crate-dep (name "async-trait") (req "^0.1.74") (default-features #t) (kind 0)) (crate-dep (name "derive_more") (req "^0.99.17") (default-features #t) (kind 2)) (crate-dep (name "serde") (req "^1.0.193") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.34.0") (features (quote ("rt" "rt-multi-thread" "macros"))) (default-features #t) (kind 2)))) (hash "00m8k7l8m67856kgkv4c03zf3mnrfh92sdd46kd15ns7ga0nnrlh")))

(define-public crate-fmodel-rust-0.5 (crate (name "fmodel-rust") (vers "0.5.0") (deps (list (crate-dep (name "async-trait") (req "^0.1.74") (default-features #t) (kind 0)) (crate-dep (name "derive_more") (req "^0.99.17") (default-features #t) (kind 2)) (crate-dep (name "serde") (req "^1.0.193") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.35.0") (features (quote ("rt" "rt-multi-thread" "macros"))) (default-features #t) (kind 2)))) (hash "01988l6yqmvvc7gk8d54970s0j6y6l0s6pwd7f5vjmysc9775wbf")))

(define-public crate-fmodel-rust-0.6 (crate (name "fmodel-rust") (vers "0.6.0") (deps (list (crate-dep (name "async-trait") (req "^0.1.75") (default-features #t) (kind 0)) (crate-dep (name "derive_more") (req "^0.99.17") (default-features #t) (kind 2)) (crate-dep (name "serde") (req "^1.0.193") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.35.1") (features (quote ("rt" "rt-multi-thread" "macros"))) (default-features #t) (kind 2)))) (hash "1lxw7q9rg8al1hx1g84kazfab8576ngkmvp0k18fpfdpi26hyr08")))

(define-public crate-fmodel-rust-0.7 (crate (name "fmodel-rust") (vers "0.7.0") (deps (list (crate-dep (name "derive_more") (req "^0.99.17") (default-features #t) (kind 2)) (crate-dep (name "serde") (req "^1.0.193") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.35.1") (features (quote ("rt" "rt-multi-thread" "macros"))) (default-features #t) (kind 2)))) (hash "0zxvrbm4qaymfyvfwjjy46114vqh19dr3gijwijsjc4885ixd0yj")))

(define-public crate-fmodsilo_interface_stdio-0.1 (crate (name "fmodsilo_interface_stdio") (vers "0.1.0") (deps (list (crate-dep (name "fmodsilo_server") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "1ka1f33z66mp2z6msy0ni7hd5lcp9ywxz2qxzdkgg1qg4jxxfhx7")))

(define-public crate-fmodsilo_interface_stdio-0.1 (crate (name "fmodsilo_interface_stdio") (vers "0.1.1") (deps (list (crate-dep (name "fmodsilo_server") (req "^0.1.1") (default-features #t) (kind 0)))) (hash "1sqsxzbasd1walvaa6hdqpx5b61fmj6z2qkx7xpr9ya2j9jyj18b")))

(define-public crate-fmodsilo_server-0.1 (crate (name "fmodsilo_server") (vers "0.1.0") (deps (list (crate-dep (name "lsp_json") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.100") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.82") (default-features #t) (kind 0)) (crate-dep (name "uuid") (req "^1.1.2") (features (quote ("v4" "fast-rng"))) (default-features #t) (kind 0)))) (hash "09iyzky266nzf4fh7gg6am2yrd8jakxnrhkbnj7msh9bhskdcqxk")))

(define-public crate-fmodsilo_server-0.1 (crate (name "fmodsilo_server") (vers "0.1.1") (deps (list (crate-dep (name "lsp_json") (req "^0.1.1") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.100") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.82") (default-features #t) (kind 0)) (crate-dep (name "uuid") (req "^1.1.2") (features (quote ("v4" "fast-rng"))) (default-features #t) (kind 0)))) (hash "079hh3cv54kflhfw51i5q6a4bdyz0r4xzmx86fkgr5yc39adgzmc")))

