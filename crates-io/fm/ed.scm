(define-module (crates-io fm ed) #:use-module (crates-io))

(define-public crate-fmedia-0.1 (crate (name "fmedia") (vers "0.1.0") (deps (list (crate-dep (name "lazy_static") (req "^1.1.1") (default-features #t) (kind 0)))) (hash "1xzv22lac7m6pk7lpcq5dfccma0hm9r2r8sjlizqzcdqg1rll8xk")))

(define-public crate-fmedia-0.2 (crate (name "fmedia") (vers "0.2.0") (deps (list (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)))) (hash "12v1v6bnidqrkvqys8kjbbiy9yyd4q42k21vkw44lr18z2v064f1")))

