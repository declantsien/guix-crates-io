(define-module (crates-io fm tb) #:use-module (crates-io))

(define-public crate-fmtbuf-0.1 (crate (name "fmtbuf") (vers "0.1.0") (hash "106j1jwgih5vnasvv5hbm28ch2mzm2ihk8ld83w7n1pizrnwf6fq") (features (quote (("std") ("default" "std"))))))

(define-public crate-fmtbuf-0.1 (crate (name "fmtbuf") (vers "0.1.1") (hash "018dwrr6ill42v0gfjm0nr55c1fnhbwkfai46vijqg0sxjxyf2kk") (features (quote (("std") ("default" "std"))))))

(define-public crate-fmtbuf-0.1 (crate (name "fmtbuf") (vers "0.1.2") (deps (list (crate-dep (name "clap") (req "^4.0.0") (features (quote ("derive"))) (default-features #t) (kind 2)))) (hash "0xx9q8m0rib8gzrb0d797w11d4vj0y2mp6k4rcpk18yn55sq7p99") (features (quote (("std") ("default" "std")))) (rust-version "1.31")))

