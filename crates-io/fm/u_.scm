(define-module (crates-io fm u_) #:use-module (crates-io))

(define-public crate-fmu_from_struct-0.1 (crate (name "fmu_from_struct") (vers "0.1.0") (deps (list (crate-dep (name "fmu_from_struct_derive") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "16aywxgfa18waxakv8i6dk64vlcpgq8fp4rgk2c7rhfy77v0kwvw")))

(define-public crate-fmu_from_struct-0.1 (crate (name "fmu_from_struct") (vers "0.1.1") (deps (list (crate-dep (name "fmu_from_struct_derive") (req "^0.1.1") (default-features #t) (kind 0)))) (hash "07j74v2cws3j8i9s1mkz151z5j716b5k461xk1ydb0mqq5dwyl64")))

(define-public crate-fmu_from_struct_derive-0.1 (crate (name "fmu_from_struct_derive") (vers "0.1.0") (deps (list (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.33") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2.0.40") (features (quote ("extra-traits"))) (default-features #t) (kind 0)) (crate-dep (name "uuid") (req "^1.6.1") (features (quote ("v4"))) (default-features #t) (kind 0)))) (hash "1vspbr61mv6m77x4xxi61xipzxans1yxc5snzn8w62b3qxlfgh8q")))

(define-public crate-fmu_from_struct_derive-0.1 (crate (name "fmu_from_struct_derive") (vers "0.1.1") (deps (list (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.33") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2.0.40") (features (quote ("extra-traits"))) (default-features #t) (kind 0)) (crate-dep (name "uuid") (req "^1.6.1") (features (quote ("v4"))) (default-features #t) (kind 0)))) (hash "1jjax86r5ws7bp853fig4654i8hfr9knjfzrlx01vmbbjhsjxd5q")))

