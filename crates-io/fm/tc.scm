(define-module (crates-io fm tc) #:use-module (crates-io))

(define-public crate-fmtcalc-0.1 (crate (name "fmtcalc") (vers "0.1.0") (deps (list (crate-dep (name "dynfmt") (req "^0.1") (features (quote ("curly"))) (default-features #t) (kind 0)))) (hash "0jv1ja072g7jrp24h0pz2a0p012xqknm7npib3cpj5czhcn9dcgd")))

