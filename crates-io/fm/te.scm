(define-module (crates-io fm te) #:use-module (crates-io))

(define-public crate-fmterr-0.1 (crate (name "fmterr") (vers "0.1.0") (hash "1qr44lp1rfmrgrn3hcsnw937iiqn3vcdkxf6b4an0k83ggqwiyla") (yanked #t)))

(define-public crate-fmterr-0.1 (crate (name "fmterr") (vers "0.1.1") (hash "08j2y86pjmi8nhasg9f9ybk9lid6b32m99jmx7p2ar1jys6ly8ka")))

