(define-module (crates-io fm ut) #:use-module (crates-io))

(define-public crate-fmutex-0.1 (crate (name "fmutex") (vers "0.1.0") (deps (list (crate-dep (name "libc") (req "^0.2.117") (default-features #t) (kind 0)) (crate-dep (name "temp-dir") (req "^0.1.11") (default-features #t) (kind 2)))) (hash "0v08yf26la204yn38ydn2m6wm34frh77vl0cgdm140q60wblrs01")))

