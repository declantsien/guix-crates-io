(define-module (crates-io fm _t) #:use-module (crates-io))

(define-public crate-fm_tree-0.1 (crate (name "fm_tree") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^2.31") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (features (quote ("preserve_order"))) (default-features #t) (kind 0)) (crate-dep (name "serde_yaml") (req "^0.7") (default-features #t) (kind 0)))) (hash "1yia8azai2lbv595wsz49cf4pjdvsin556i4aiwzvh9yszasz092")))

