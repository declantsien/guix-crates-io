(define-module (crates-io fm t_) #:use-module (crates-io))

(define-public crate-fmt_adapter-0.1 (crate (name "fmt_adapter") (vers "0.1.0") (hash "1wjbckqnw0rygxqkwcinxnn70vm2x1z46yg6cam3hd6mxapf289m") (yanked #t)))

(define-public crate-fmt_adapter-0.2 (crate (name "fmt_adapter") (vers "0.2.0") (hash "02rhzw7s8xx9nyb9r1cbdjqfh1vzfhpxnrv59m01rp3mhga9dyld") (yanked #t)))

(define-public crate-fmt_adapter-0.2 (crate (name "fmt_adapter") (vers "0.2.1") (hash "0hqixin703fy9d98bvri95k1mf0przi30641gk7wgg7giisxmw4v")))

(define-public crate-fmt_comma-0.1 (crate (name "fmt_comma") (vers "0.1.0") (hash "1g4amg0v3ww0i8ghw8g1n3r3dy21crfhq54794bcr152c8q0li9x")))

(define-public crate-fmt_comma-0.1 (crate (name "fmt_comma") (vers "0.1.1") (hash "13hf6h82ki4qy1jfwcw08p4c4971vazzdg7wc7avgb6psqi7why3")))

(define-public crate-fmt_comma-0.1 (crate (name "fmt_comma") (vers "0.1.3") (hash "10inhwgnqxvaa2lxhvybla402dgal8n1i1v1yghz0w1d8l07f8ap")))

(define-public crate-fmt_comma-0.1 (crate (name "fmt_comma") (vers "0.1.4") (hash "054idggy0a5b2rwb4brdfwq5z9m7ijx6qm0lq2bh0rc0kyrcnwcw")))

(define-public crate-fmt_compare_nostd-0.1 (crate (name "fmt_compare_nostd") (vers "0.1.0") (hash "0qbxqbsm26x3szf68w38gfnb1ar7vwf4d0i2kvm8pa7l61lrb9z2")))

(define-public crate-fmt_compare_nostd-0.1 (crate (name "fmt_compare_nostd") (vers "0.1.1") (hash "1jyxj4sp8rpp5yb3m7651vpwalwx8f868685596xannc6jh42dvx")))

(define-public crate-fmt_ext-0.1 (crate (name "fmt_ext") (vers "0.1.0") (hash "1vf0ac0mg814alsan477i685b0c3ixpps5326khpklmbzrsmm5af")))

(define-public crate-fmt_ext-0.1 (crate (name "fmt_ext") (vers "0.1.1") (hash "1jmvz7dipc9jqid71ivikx9b9s321qcf1bs67l3pnw6hr7dx9yqf")))

(define-public crate-fmt_ext-0.1 (crate (name "fmt_ext") (vers "0.1.2") (hash "1adsisz51s1ff7g5q4l9adidvjrp6sgmz01qlx7skkzg3g86npnm")))

