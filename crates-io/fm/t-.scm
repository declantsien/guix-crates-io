(define-module (crates-io fm t-) #:use-module (crates-io))

(define-public crate-fmt-cmp-0.1 (crate (name "fmt-cmp") (vers "0.1.0") (deps (list (crate-dep (name "itoa") (req "^1") (default-features #t) (kind 2)))) (hash "0zbi35bzzdsz7cha67jrqkc29l1n3i78qb482fv3wjln6ixkblw6") (features (quote (("std" "alloc") ("default" "alloc") ("alloc")))) (rust-version "1.41")))

(define-public crate-fmt-cmp-0.1 (crate (name "fmt-cmp") (vers "0.1.1") (deps (list (crate-dep (name "itoa") (req "^1") (default-features #t) (kind 2)))) (hash "05szgmy8p0gfsankyvm6n338pva85j57sm1ja8wv7kh7y7hvbf46") (features (quote (("std" "alloc") ("default" "alloc") ("alloc")))) (rust-version "1.41")))

(define-public crate-fmt-derive-0.0.1 (crate (name "fmt-derive") (vers "0.0.1") (deps (list (crate-dep (name "fmt-derive-proc") (req "=0.0.1") (default-features #t) (kind 0)) (crate-dep (name "pretty_assertions") (req "^1.3.0") (default-features #t) (kind 2)))) (hash "1fladla3nm2c1lpaq4q0bpaiyqxz1wjj6j5g02ml5jd60ar330bn") (features (quote (("std") ("default" "std"))))))

(define-public crate-fmt-derive-0.0.2 (crate (name "fmt-derive") (vers "0.0.2") (deps (list (crate-dep (name "fmt-derive-proc") (req "=0.0.2") (default-features #t) (kind 0)) (crate-dep (name "pretty_assertions") (req "^1.3.0") (default-features #t) (kind 2)))) (hash "1gwj7a1hr94pg46wzhvg99g3y03xajkhhlr10whvfpgn85dbgl30") (rust-version "1.56.0")))

(define-public crate-fmt-derive-0.0.3 (crate (name "fmt-derive") (vers "0.0.3") (deps (list (crate-dep (name "fmt-derive-proc") (req "=0.0.3") (default-features #t) (kind 0)) (crate-dep (name "pretty_assertions") (req "^1.3.0") (default-features #t) (kind 2)))) (hash "0r76sspdy50mmghgxk0njiqkv05b19arw9h7dpsxgdn1irm2p72s") (rust-version "1.56.0")))

(define-public crate-fmt-derive-0.0.4 (crate (name "fmt-derive") (vers "0.0.4") (deps (list (crate-dep (name "fmt-derive-proc") (req "=0.0.4") (default-features #t) (kind 0)) (crate-dep (name "pretty_assertions") (req "^1.3.0") (default-features #t) (kind 2)))) (hash "1rn93wh48n6h4kvbhhy8q018s33fbg7jdfyaxaq0n216vk00k389") (rust-version "1.56.0")))

(define-public crate-fmt-derive-0.0.5 (crate (name "fmt-derive") (vers "0.0.5") (deps (list (crate-dep (name "fmt-derive-proc") (req "=0.0.5") (default-features #t) (kind 0)) (crate-dep (name "pretty_assertions") (req "^1.3.0") (default-features #t) (kind 2)))) (hash "1yn8x3zggrs4i4jzdwz58dahjrj8licfp7l6idhsj7f4nb7cddy5") (rust-version "1.56.0")))

(define-public crate-fmt-derive-0.0.6 (crate (name "fmt-derive") (vers "0.0.6") (deps (list (crate-dep (name "fmt-derive-proc") (req "=0.0.6") (default-features #t) (kind 0)) (crate-dep (name "pretty_assertions") (req "^1.3.0") (default-features #t) (kind 2)))) (hash "1hajlvmhmgf1hwl15zg9l972kb5y600ny1nli9xw38nbrsh24prz") (rust-version "1.60.0")))

(define-public crate-fmt-derive-0.1 (crate (name "fmt-derive") (vers "0.1.0") (deps (list (crate-dep (name "fmt-derive-proc") (req "=0.1.0") (default-features #t) (kind 0)) (crate-dep (name "pretty_assertions") (req "^1.3.0") (default-features #t) (kind 2)))) (hash "01r9yd6sz6alhb0cky4ca7isd0bz4d60nnzvp2wybsm2yiyrdnsh") (rust-version "1.64.0")))

(define-public crate-fmt-derive-0.1 (crate (name "fmt-derive") (vers "0.1.1") (deps (list (crate-dep (name "fmt-derive-proc") (req "=0.1.1") (default-features #t) (kind 0)) (crate-dep (name "pretty_assertions") (req "^1.3.0") (default-features #t) (kind 2)))) (hash "11fr1434qbxnq2y2grrcrsnly1k98s3rvwvvyk422ldvxxs7jmfi") (rust-version "1.66.0")))

(define-public crate-fmt-derive-0.1 (crate (name "fmt-derive") (vers "0.1.2") (deps (list (crate-dep (name "fmt-derive-proc") (req "=0.1.2") (default-features #t) (kind 0)) (crate-dep (name "pretty_assertions") (req "^1.3.0") (default-features #t) (kind 2)))) (hash "0s81869syvx3p767ncqlr46gclcfjw9dnqmlz22mhi96bjsa9d0w") (rust-version "1.69.0")))

(define-public crate-fmt-derive-proc-0.0.1 (crate (name "fmt-derive-proc") (vers "0.0.1") (deps (list (crate-dep (name "pretty_assertions") (req "^1") (default-features #t) (kind 2)) (crate-dep (name "proc-macro-crate") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "proc-macro-error") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1") (default-features #t) (kind 0)))) (hash "1w4y2dqq7vq52j5vb3zdk5qfnpyii6hf7v3g7ffyhs3bygfs5ccy")))

(define-public crate-fmt-derive-proc-0.0.2 (crate (name "fmt-derive-proc") (vers "0.0.2") (deps (list (crate-dep (name "pretty_assertions") (req "^1") (default-features #t) (kind 2)) (crate-dep (name "proc-macro-crate") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "proc-macro-error") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1") (default-features #t) (kind 0)))) (hash "1nv7jbj65w20pjg1vqpgzlp6hivhpjz577gf795770967gw67rck") (rust-version "1.56.0")))

(define-public crate-fmt-derive-proc-0.0.3 (crate (name "fmt-derive-proc") (vers "0.0.3") (deps (list (crate-dep (name "pretty_assertions") (req "^1") (default-features #t) (kind 2)) (crate-dep (name "proc-macro-crate") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "proc-macro-error") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1") (default-features #t) (kind 0)))) (hash "042v3fvdpisgiq78ngsnlzxa39riscn9hh4k6l7cwpc122l27hg7") (rust-version "1.56.0")))

(define-public crate-fmt-derive-proc-0.0.4 (crate (name "fmt-derive-proc") (vers "0.0.4") (deps (list (crate-dep (name "pretty_assertions") (req "^1") (default-features #t) (kind 2)) (crate-dep (name "proc-macro-crate") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "proc-macro-error") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1") (default-features #t) (kind 0)))) (hash "144s5xz08nhgps42y9frg4x765wqkax4f06brbp1pb3d44pqc4la") (rust-version "1.56.0")))

(define-public crate-fmt-derive-proc-0.0.5 (crate (name "fmt-derive-proc") (vers "0.0.5") (deps (list (crate-dep (name "pretty_assertions") (req "^1") (default-features #t) (kind 2)) (crate-dep (name "proc-macro-crate") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "proc-macro-error") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1") (default-features #t) (kind 0)))) (hash "0c1h9rgkiq2l6mhgfl8gs7am7irbpf54bhsa7f7xqdmjp699zls8") (rust-version "1.56.0")))

(define-public crate-fmt-derive-proc-0.0.6 (crate (name "fmt-derive-proc") (vers "0.0.6") (deps (list (crate-dep (name "pretty_assertions") (req "^1") (default-features #t) (kind 2)) (crate-dep (name "proc-macro-crate") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "proc-macro-error") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2") (default-features #t) (kind 0)))) (hash "0vl925wvp6bgs16xvgic2gbbhbq806r12gdvisj7p7j71cl3iim5") (rust-version "1.60.0")))

(define-public crate-fmt-derive-proc-0.1 (crate (name "fmt-derive-proc") (vers "0.1.0") (deps (list (crate-dep (name "pretty_assertions") (req "^1") (default-features #t) (kind 2)) (crate-dep (name "proc-macro-crate") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "proc-macro-error") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2") (default-features #t) (kind 0)))) (hash "1fa93f7yq63sbpm1s6ifqd09jg75l8kbx2a5d3wjdzyyqdy5m82s") (rust-version "1.64.0")))

(define-public crate-fmt-derive-proc-0.1 (crate (name "fmt-derive-proc") (vers "0.1.1") (deps (list (crate-dep (name "pretty_assertions") (req "^1") (default-features #t) (kind 2)) (crate-dep (name "proc-macro-crate") (req "^2") (default-features #t) (kind 0)) (crate-dep (name "proc-macro-error") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2") (default-features #t) (kind 0)))) (hash "0s44x8qr392knad0fp7szdq9n2xwqy578wns7yd53a7s4x1arjlx") (rust-version "1.66.0")))

(define-public crate-fmt-derive-proc-0.1 (crate (name "fmt-derive-proc") (vers "0.1.2") (deps (list (crate-dep (name "pretty_assertions") (req "^1") (default-features #t) (kind 2)) (crate-dep (name "proc-macro-crate") (req "^3") (default-features #t) (kind 0)) (crate-dep (name "proc-macro-error") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2") (default-features #t) (kind 0)))) (hash "08qd72rl5l7vcflw7wm8qmfyhsvwx4hmpwakhz748hvkc03xl13p") (rust-version "1.69.0")))

(define-public crate-fmt-extra-0.0.1 (crate (name "fmt-extra") (vers "0.0.1") (hash "0iq0rhi14zp1i9icrkdzdy6wbijcrkgnhl61vmllrxb6nxqjggf7")))

(define-public crate-fmt-extra-0.0.2 (crate (name "fmt-extra") (vers "0.0.2") (hash "08gvk73ayl6hv64invis2ys8gzxz0b83h93j0z5cvq60w14smv0k")))

(define-public crate-fmt-extra-0.0.3 (crate (name "fmt-extra") (vers "0.0.3") (hash "0jrvxpc9n5k94b56jwa13q0v72rw8zv4wdybxzwwfdwfvs7imw9n")))

(define-public crate-fmt-extra-0.1 (crate (name "fmt-extra") (vers "0.1.0") (hash "0pd7j37zax6x8h657by9gkfrr9h9iqjp1bdvf3za3bsl4pw52xqs")))

(define-public crate-fmt-extra-0.1 (crate (name "fmt-extra") (vers "0.1.1") (hash "1hm7hfm3627jcihn4niv4ym1kyk2sv26hgn7027ikcl5gmb6jvfl")))

(define-public crate-fmt-extra-0.1 (crate (name "fmt-extra") (vers "0.1.2") (hash "1xmcrb9pf9d8kfg7rd3s5zc795bnq89khaav0dzrk21jqdkpbw5w")))

(define-public crate-fmt-extra-0.2 (crate (name "fmt-extra") (vers "0.2.0") (hash "13f82r913z3viwvphwcz0qr7ragcyn5a9q0dvgasmdwm6fpf301p")))

(define-public crate-fmt-extra-0.2 (crate (name "fmt-extra") (vers "0.2.1") (hash "1fgj6zp3wc2p23z9kp3ilhhkn730j2fqkcgv8w087gprn5qizw87")))

(define-public crate-fmt-iter-0.0.0 (crate (name "fmt-iter") (vers "0.0.0") (deps (list (crate-dep (name "derive_more") (req "^0.99.13") (default-features #t) (kind 0)) (crate-dep (name "pipe-trait") (req "^0.3.2") (default-features #t) (kind 0)))) (hash "07dfpiv9vkp2fmwkcr4ndmdji1k1ai3ycmrpi01y8x7ds0z77jxn")))

(define-public crate-fmt-iter-0.1 (crate (name "fmt-iter") (vers "0.1.0") (deps (list (crate-dep (name "derive_more") (req "^0.99.13") (default-features #t) (kind 0)) (crate-dep (name "pipe-trait") (req "^0.3.2") (default-features #t) (kind 0)))) (hash "0ddxkm8c7759v9n7cqqby661idw9vvlbrrckh3i1nkfxwgg9saj5")))

(define-public crate-fmt-iter-0.2 (crate (name "fmt-iter") (vers "0.2.0") (deps (list (crate-dep (name "derive_more") (req "^0.99.13") (default-features #t) (kind 0)) (crate-dep (name "pipe-trait") (req "^0.3.2") (default-features #t) (kind 0)))) (hash "08vg0857nlmlmfzqcr3k7q8nzg0qjiwli39vblff445nf4z0y09g")))

(define-public crate-fmt-iter-0.2 (crate (name "fmt-iter") (vers "0.2.1") (deps (list (crate-dep (name "derive_more") (req "^0.99.13") (default-features #t) (kind 0)) (crate-dep (name "itertools") (req "^0.10.0") (default-features #t) (kind 0)) (crate-dep (name "pipe-trait") (req "^0.3.2") (default-features #t) (kind 2)))) (hash "0456zjq0p10jw1vg5dgqd3bvmvyx5451s3w3v2270739fsfjiffh")))

(define-public crate-fmt-log-0.1 (crate (name "fmt-log") (vers "0.1.0") (hash "0h4i0fw4m69z2qqw9b9adjijr52l1xisqn31fplsp5p2l2wvmwyr")))

(define-public crate-fmt-log-0.1 (crate (name "fmt-log") (vers "0.1.1") (hash "189z8kvciz1gsypwfqx535ccbwy2v1vbwvzc9771iyazj11xg77d")))

(define-public crate-fmt-tools-0.1 (crate (name "fmt-tools") (vers "0.1.0") (hash "0q2zb3fa4j03jzywd6x8wandc7kdg16qaxmshbykcp4mklfg0chz")))

(define-public crate-fmt-utils-0.1 (crate (name "fmt-utils") (vers "0.1.0") (deps (list (crate-dep (name "fast_fmt") (req "^0.1.3") (optional #t) (default-features #t) (kind 0)))) (hash "0a555pxi5n1rz662s75ny56yn7bihapwjriilziwh4big7xip6p0")))

