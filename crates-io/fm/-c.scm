(define-module (crates-io fm -c) #:use-module (crates-io))

(define-public crate-fm-cli-0.1 (crate (name "fm-cli") (vers "0.1.0") (deps (list (crate-dep (name "chrono") (req "^0.4.24") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.96") (default-features #t) (kind 0)))) (hash "0wq34d23y69v5rlmw8gn7p3cij6kwsa4xjh3i2al28p0cygp6vrb")))

(define-public crate-fm-cli-0.1 (crate (name "fm-cli") (vers "0.1.1") (deps (list (crate-dep (name "chrono") (req "^0.4.24") (default-features #t) (kind 0)) (crate-dep (name "crossterm") (req "^0.27.0") (default-features #t) (kind 0)) (crate-dep (name "ratatui") (req "^0.22.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.96") (default-features #t) (kind 0)))) (hash "1sc3izhc2vwxqbyssgzdd4a3wlv8wb5r2a4xhrriir6i54pcf09p")))

