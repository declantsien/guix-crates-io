(define-module (crates-io dn or) #:use-module (crates-io))

(define-public crate-dnorm-0.1 (crate (name "dnorm") (vers "0.1.0") (deps (list (crate-dep (name "libm") (req "^0.2.8") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)))) (hash "0kzcxiviv5ixaw00akrnzxr8c7207vkfmng2hcjs7c9r4biryvvi")))

