(define-module (crates-io dn um) #:use-module (crates-io))

(define-public crate-dnum-0.1 (crate (name "dnum") (vers "0.1.0") (hash "19ksp4c18m92vpknhzvkhb1lkzmhrj7wjkk3xrbw65224z7zvi91")))

(define-public crate-dnum-0.3 (crate (name "dnum") (vers "0.3.0") (hash "0wh6qzhxjk3yh927gyhsi2x8mfzmh9x3bb0l1wd1p6as1bppvs6s") (yanked #t)))

