(define-module (crates-io dn ld) #:use-module (crates-io))

(define-public crate-dnld-0.1 (crate (name "dnld") (vers "0.1.0") (deps (list (crate-dep (name "anyhow") (req "^1.0.79") (default-features #t) (kind 0)) (crate-dep (name "bytes") (req "^1.5.0") (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.11.23") (features (quote ("blocking"))) (default-features #t) (kind 0)))) (hash "083jn0xr2n0mi5mfkf9qqakmxlphr86xdhkb552m8qsvlfdn8324")))

(define-public crate-dnld-0.1 (crate (name "dnld") (vers "0.1.1") (deps (list (crate-dep (name "anyhow") (req "^1.0.79") (default-features #t) (kind 0)) (crate-dep (name "bytes") (req "^1.5.0") (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.11.23") (features (quote ("blocking"))) (default-features #t) (kind 0)))) (hash "029g5iaifrh6cfb7ir7bhb2ssw9bqlw2k069f9kjpwcy11srlxam")))

(define-public crate-dnld-0.1 (crate (name "dnld") (vers "0.1.2") (deps (list (crate-dep (name "anyhow") (req "^1.0.79") (default-features #t) (kind 0)) (crate-dep (name "bytes") (req "^1.5.0") (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.11.23") (features (quote ("blocking"))) (default-features #t) (kind 0)))) (hash "0khlmg16a5mnx2bvx9ynrx5f0cwklxvlvrg4mqb256pnrafsqdf8")))

