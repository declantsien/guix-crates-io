(define-module (crates-io dn dt) #:use-module (crates-io))

(define-public crate-dndtools-0.1 (crate (name "dndtools") (vers "0.1.0") (deps (list (crate-dep (name "getopts") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "num_cpus") (req "^1.13") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.7") (default-features #t) (kind 0)) (crate-dep (name "rayon") (req "^1.5") (default-features #t) (kind 0)))) (hash "0316ri7bvx1i0i19b4zzlahbgwf911dnk4y75j452d03x8q50awb")))

(define-public crate-dndtrigger-0.1 (crate (name "dndtrigger") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^4.5.4") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "plist") (req "^1.6.1") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.197") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "service-manager") (req "^0.6.0") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.37.0") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "01z4wrkdw4aj40nalif7dpl4757292dppwi53flz1pwpndwd319v")))

