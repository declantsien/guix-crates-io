(define-module (crates-io dn f5) #:use-module (crates-io))

(define-public crate-dnf5-0.0.0 (crate (name "dnf5") (vers "0.0.0") (hash "1gr07s2qr8q8g9gfqihr4al90vrpkka0v64pyq8ckm7dvk4fihl4")))

(define-public crate-dnf5-sys-0.0.0 (crate (name "dnf5-sys") (vers "0.0.0") (hash "15pd1rz94gfh7yi6b1581ji0z6ypdfi67z9pxxi1q2xiwazns0j6")))

