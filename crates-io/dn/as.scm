(define-module (crates-io dn as) #:use-module (crates-io))

(define-public crate-dnas-0.1 (crate (name "dnas") (vers "0.1.0") (deps (list (crate-dep (name "atty") (req "^0.2.14") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^4.1.1") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "colored") (req "^2.0.0") (default-features #t) (kind 0)) (crate-dep (name "nom") (req "^7.1.2") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)))) (hash "19q30ki91avn4hp3adr405slzw9hk5g9k1ga0gsywjb5qj2pj4fz")))

(define-public crate-dnaslx-0.0.1 (crate (name "dnaslx") (vers "0.0.1") (hash "0hb4phcyr9z6h0g1pjajhyagp1xgiaqshybznj08ax9dshm52pxi")))

