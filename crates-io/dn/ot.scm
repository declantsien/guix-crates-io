(define-module (crates-io dn ot) #:use-module (crates-io))

(define-public crate-dnote-tui-0.1 (crate (name "dnote-tui") (vers "0.1.0") (deps (list (crate-dep (name "crossterm") (req "^0.27.0") (default-features #t) (kind 0)) (crate-dep (name "tui") (req "^0.22.0") (default-features #t) (kind 0) (package "ratatui")))) (hash "0fs8dc69yaw0swxdpxlc8ka8hiz6j3d6805bljn1zz6xqncmr4gv")))

(define-public crate-dnote-tui-0.1 (crate (name "dnote-tui") (vers "0.1.1") (deps (list (crate-dep (name "crossterm") (req "^0.27.0") (default-features #t) (kind 0)) (crate-dep (name "tui") (req "^0.23.0") (default-features #t) (kind 0) (package "ratatui")))) (hash "0krsv4pyh5vi7wk3zn9ckdihxydyzgyri6db2pn96z1wvla148gm")))

(define-public crate-dnote-tui-0.2 (crate (name "dnote-tui") (vers "0.2.0") (deps (list (crate-dep (name "crossterm") (req "^0.27.0") (default-features #t) (kind 0)) (crate-dep (name "tui") (req "^0.23.0") (default-features #t) (kind 0) (package "ratatui")))) (hash "10lrwf5fjal45120gpg4bp2p5nnjc5aqk7h4xg0ra8qlcmp3k2q0")))

(define-public crate-dnote-tui-0.2 (crate (name "dnote-tui") (vers "0.2.1") (deps (list (crate-dep (name "crossterm") (req "^0.27.0") (default-features #t) (kind 0)) (crate-dep (name "tui") (req "^0.23.0") (default-features #t) (kind 0) (package "ratatui")))) (hash "1za8kakp9i21djzc8ym38cb9a9xhqcbnb1wg2kjpi1qbhjjj9ikb")))

(define-public crate-dnoted-0.1 (crate (name "dnoted") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^4.5.1") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "colored") (req "^2.1.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)))) (hash "0y7vma88s21sgq6h6nsl59n48l7nsb7y14wzyz26jjciskcrk3h4")))

(define-public crate-dnoted-edc-0.1 (crate (name "dnoted-edc") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^4.5.1") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "colored") (req "^2.1.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)))) (hash "048s4vknd1c2rlj9cgfhkpnfzz18x7fkfiw0pyn1bj47pp7xz7kd")))

