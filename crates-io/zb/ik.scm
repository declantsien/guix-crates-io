(define-module (crates-io zb ik) #:use-module (crates-io))

(define-public crate-zbiko_art-0.1 (crate (name "zbiko_art") (vers "0.1.0") (hash "1zlh2fi0ygmwl3qi4i4vl7nx7pa6jy7vz4a68psinv28iwmig9s5") (yanked #t)))

(define-public crate-zbiko_art-0.2 (crate (name "zbiko_art") (vers "0.2.0") (hash "0i7z74h57m0paqgv8gf86jsvil864f1f67d717ym6l6gwyw6xmzd") (yanked #t)))

