(define-module (crates-io zb uf) #:use-module (crates-io))

(define-public crate-zbuf-0.1 (crate (name "zbuf") (vers "0.1.0") (hash "0lm5z3ccijiji8i166zymq9grsniijalzx22dmxhxg821v9plqfm")))

(define-public crate-zbuf-0.1 (crate (name "zbuf") (vers "0.1.1") (deps (list (crate-dep (name "utf-8") (req "^0.7.1") (default-features #t) (kind 0)))) (hash "0sigcrbb22i6l7z6ga5jrsmp1gmhn9nz8qz0ihgxx0s0x44axbg7")))

(define-public crate-zbuf-0.1 (crate (name "zbuf") (vers "0.1.2") (deps (list (crate-dep (name "utf-8") (req "^0.7.1") (default-features #t) (kind 0)))) (hash "0lz3haqg65wj1j44k8vgvz0sxvf9m1i4xm3c9acs7nd42mc76jh1")))

