(define-module (crates-io zb as) #:use-module (crates-io))

(define-public crate-zbase32-0.0.1 (crate (name "zbase32") (vers "0.0.1") (deps (list (crate-dep (name "clippy") (req "^0.0") (optional #t) (default-features #t) (kind 0)))) (hash "1qf242x9ccsllb74l42kdmpf9h8r9k2sfi84hmmwlinxh3h1x7m0")))

(define-public crate-zbase32-0.0.2 (crate (name "zbase32") (vers "0.0.2") (deps (list (crate-dep (name "clippy") (req "^0.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "quickcheck") (req "^0.4") (default-features #t) (kind 2)))) (hash "14n3j8sbnwzh40d752dk8dh1v7lghqq8qf0mc1hfiwn4qs005z1g")))

(define-public crate-zbase32-0.0.3 (crate (name "zbase32") (vers "0.0.3") (deps (list (crate-dep (name "clippy") (req "^0.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "quickcheck") (req "^0.4") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.3") (default-features #t) (kind 2)))) (hash "16f4hvvz78krbxmrkllbbcjwj7dw1yb95fvl0mkx0jmd059px02z")))

(define-public crate-zbase32-0.0.4 (crate (name "zbase32") (vers "0.0.4") (deps (list (crate-dep (name "clippy") (req "^0.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "quickcheck") (req "^0.4") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.3") (default-features #t) (kind 2)))) (hash "1yvzpb3ydg7m1rjglia6xhwbkfrkvydpj7bi4dz36xvjib7ak3h7")))

(define-public crate-zbase32-0.0.5 (crate (name "zbase32") (vers "0.0.5") (deps (list (crate-dep (name "clippy") (req "^0.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "quickcheck") (req "^0.4") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.3") (default-features #t) (kind 2)))) (hash "1d316xjwz73wr2mf3yiimdw3jsnvwpwfavmcbv6h06b5w6jbq6d8") (features (quote (("unstable"))))))

(define-public crate-zbase32-0.1 (crate (name "zbase32") (vers "0.1.0") (deps (list (crate-dep (name "clippy") (req "^0.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "cpython") (req "^0.1") (default-features #t) (kind 2)) (crate-dep (name "quickcheck") (req "^0.4") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.3") (default-features #t) (kind 2)))) (hash "0vh8ndda5iisgw6w5mcgkaqxb82ayiab73pzwpxhnjb3lk6wgc93") (features (quote (("unstable") ("python_tests"))))))

(define-public crate-zbase32-0.1 (crate (name "zbase32") (vers "0.1.1") (deps (list (crate-dep (name "clippy") (req "^0.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "cpython") (req "^0.2") (default-features #t) (kind 2)) (crate-dep (name "quickcheck") (req "^0.6") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.4") (default-features #t) (kind 2)))) (hash "086mmlxci8k308q7gmh8hps6j3ma8g6akyvvas4wwfmlfkrl82w5") (features (quote (("unstable") ("python_tests"))))))

(define-public crate-zbase32-0.1 (crate (name "zbase32") (vers "0.1.2") (deps (list (crate-dep (name "cpython") (req "^0.2") (default-features #t) (kind 2)) (crate-dep (name "quickcheck") (req "^0.7") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.6") (default-features #t) (kind 2)))) (hash "0gz3nmiaidscb5c85rh3qxi8i584gz5xm3amlxqminl8jq27k40g") (features (quote (("unstable") ("python_tests"))))))

