(define-module (crates-io i8 #{08}#) #:use-module (crates-io))

(define-public crate-i8080-0.1 (crate (name "i8080") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^2.26") (default-features #t) (kind 0)) (crate-dep (name "ears") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "image") (req "^0.15") (kind 0)) (crate-dep (name "piston_window") (req "^0.70") (default-features #t) (kind 0)))) (hash "0mvqq5yhkrk3i49yrz41bkjhj5i7binhcf8g6qk7mq3hmxxqn1z3") (features (quote (("cpudiag"))))))

(define-public crate-i8080emulator-0.1 (crate (name "i8080emulator") (vers "0.1.0") (hash "1vflk91l84l382pvngb87xa1v0wjk4v49qvdqpw4fr94vz3522c6") (features (quote (("bdos_mock"))))))

(define-public crate-i8086-0.0.0 (crate (name "i8086") (vers "0.0.0") (hash "083zh2463fz5sc2lw5vaj9ygxv1y1vjlyrj8xhhp3hlqn1lf3spa")))

