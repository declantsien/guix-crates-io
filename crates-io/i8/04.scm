(define-module (crates-io i8 #{04}#) #:use-module (crates-io))

(define-public crate-i8042-0.1 (crate (name "i8042") (vers "0.1.0") (deps (list (crate-dep (name "pc-keyboard") (req "^0.7") (default-features #t) (kind 0)))) (hash "1djvlfkdxpv16ivzwxwdrl4h44k8m44m0p421k85c1l0y3yqsrgl")))

