(define-module (crates-io im g-) #:use-module (crates-io))

(define-public crate-img-archive-parser-0.1 (crate (name "img-archive-parser") (vers "0.1.0") (deps (list (crate-dep (name "byteorder") (req "^1.5.0") (default-features #t) (kind 0)))) (hash "13g3zqhbga9wz30wjr6j1q72kv5pflfya18jx7bylzcdvphhadyl")))

(define-public crate-img-archive-parser-0.1 (crate (name "img-archive-parser") (vers "0.1.1") (deps (list (crate-dep (name "byteorder") (req "^1.5.0") (default-features #t) (kind 0)))) (hash "011vgchjxknl48hr0a6m9n6al8nyknmx18wis5ks7grp6mii13nx")))

(define-public crate-img-parts-0.1 (crate (name "img-parts") (vers "0.1.0") (deps (list (crate-dep (name "bytecount") (req "^0.6.0") (default-features #t) (kind 0)) (crate-dep (name "byteorder") (req "^1.1.0") (default-features #t) (kind 0)))) (hash "1nk0c54wnbkna69ldb6i8gnvpbh5awhdf9mxll9zmfvb60a9907m")))

(define-public crate-img-parts-0.1 (crate (name "img-parts") (vers "0.1.1") (deps (list (crate-dep (name "byteorder") (req "^1.1.0") (default-features #t) (kind 0)))) (hash "1wv30w2gr2v2r4kqxjazg3b1hfk8jkrza38z3wzhsd3jrhc9gg7r")))

(define-public crate-img-parts-0.2 (crate (name "img-parts") (vers "0.2.0") (deps (list (crate-dep (name "bytes") (req "^0.5.3") (kind 0)) (crate-dep (name "crc32fast") (req "^1.1.1") (kind 0)) (crate-dep (name "miniz_oxide") (req "^0.4") (default-features #t) (kind 0)))) (hash "15lrq745knkcjzznvvhc6bmi9qx5lqhd862nf8ijpljblyhs8gkh") (features (quote (("std" "bytes/std" "crc32fast/std") ("default" "std"))))))

(define-public crate-img-parts-0.2 (crate (name "img-parts") (vers "0.2.1") (deps (list (crate-dep (name "bytes") (req "^0.5.3") (kind 0)) (crate-dep (name "crc32fast") (req "^1.1.1") (kind 0)) (crate-dep (name "miniz_oxide") (req "^0.4") (default-features #t) (kind 0)))) (hash "0z656jnv2fdnlq7rm4i8b5kybhz12lf9sz8ib0wpzjnc6a9vqihm") (features (quote (("std" "bytes/std" "crc32fast/std") ("default" "std"))))))

(define-public crate-img-parts-0.2 (crate (name "img-parts") (vers "0.2.2") (deps (list (crate-dep (name "bytes") (req "^0.5.3") (kind 0)) (crate-dep (name "crc32fast") (req "^1.1.1") (kind 0)) (crate-dep (name "miniz_oxide") (req "^0.4") (default-features #t) (kind 0)))) (hash "0nblzdxmzdmx3ck9n9i9slqkq1lpqagg7xdlbpn8c1gd4rg0plj3") (features (quote (("std" "bytes/std" "crc32fast/std") ("default" "std"))))))

(define-public crate-img-parts-0.2 (crate (name "img-parts") (vers "0.2.3") (deps (list (crate-dep (name "bytes") (req "^0.5.3") (kind 0)) (crate-dep (name "crc32fast") (req "^1.1.1") (kind 0)) (crate-dep (name "miniz_oxide") (req "^0.4") (default-features #t) (kind 0)))) (hash "1rg4jhkza04lhb6gndzgvr7vb72l8wvp97zwdzaddaqp40fqf472") (features (quote (("std" "bytes/std" "crc32fast/std") ("default" "std"))))))

(define-public crate-img-parts-0.3 (crate (name "img-parts") (vers "0.3.0") (deps (list (crate-dep (name "bytes") (req "^1") (kind 0)) (crate-dep (name "crc32fast") (req "^1.1.1") (kind 0)) (crate-dep (name "miniz_oxide") (req "^0.5") (default-features #t) (kind 0)))) (hash "1w01cxdpps5iz5f1cdiydiisx4lg66jjgvbg8qsgr9criljmi4xi") (features (quote (("std" "bytes/std" "crc32fast/std") ("default" "std"))))))

(define-public crate-img-qoi-0.1 (crate (name "img-qoi") (vers "0.1.0") (deps (list (crate-dep (name "anyhow") (req "^1.0.51") (default-features #t) (kind 2)) (crate-dep (name "arrayvec") (req "^0.7.2") (default-features #t) (kind 0)) (crate-dep (name "byteorder") (req "^1.4.3") (default-features #t) (kind 0)) (crate-dep (name "image") (req "^0.23.14") (default-features #t) (kind 2)) (crate-dep (name "structopt") (req "^0.3.25") (default-features #t) (kind 2)) (crate-dep (name "thiserror") (req "^1.0.30") (default-features #t) (kind 0)))) (hash "02a9pqp1a5p1biwxvabh4wx737f9arqr5y1igabq3zx8lvjh78r3")))

(define-public crate-img-renamer-1 (crate (name "img-renamer") (vers "1.0.0") (deps (list (crate-dep (name "clap") (req "^2.33.3") (default-features #t) (kind 0)) (crate-dep (name "dialoguer") (req "^0.6.2") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.3.9") (default-features #t) (kind 0)) (crate-dep (name "vfs") (req "^0.4.0") (default-features #t) (kind 2)))) (hash "0fs27crii8klkbhjc0l7abvy8lgnsknarc2y8x2ma9hfs7w154fr")))

(define-public crate-img-renamer-1 (crate (name "img-renamer") (vers "1.0.1") (deps (list (crate-dep (name "clap") (req "^2.33.3") (default-features #t) (kind 0)) (crate-dep (name "dialoguer") (req "^0.6.2") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.3.9") (default-features #t) (kind 0)) (crate-dep (name "vfs") (req "^0.4.0") (default-features #t) (kind 2)))) (hash "15vp3cjj7n8jdsfhc65x7nx537m3v34kszgn3k3mgqb6ba6g8c53")))

(define-public crate-img-renamer-1 (crate (name "img-renamer") (vers "1.0.2") (deps (list (crate-dep (name "clap") (req "^2.33.3") (default-features #t) (kind 0)) (crate-dep (name "dialoguer") (req "^0.6.2") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.3.9") (default-features #t) (kind 0)) (crate-dep (name "vfs") (req "^0.4.0") (default-features #t) (kind 2)))) (hash "1axc2zgyggfp91jn6s83xzfw2s8d9bi9q75s5kzn8vqcx7ki0fwy")))

(define-public crate-img-renamer-2 (crate (name "img-renamer") (vers "2.0.0") (deps (list (crate-dep (name "clap") (req "^3.0.0-beta.2") (default-features #t) (kind 0)) (crate-dep (name "dialoguer") (req "^0.6.2") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.3.9") (default-features #t) (kind 0)) (crate-dep (name "vfs") (req "^0.4.0") (default-features #t) (kind 2)))) (hash "06wqcksfdsiin5npfrgmy3s9bfgw83jsin4b335zvrf46xly6h2i")))

(define-public crate-img-renamer-2 (crate (name "img-renamer") (vers "2.0.1") (deps (list (crate-dep (name "clap") (req "^3.0.0-beta.2") (default-features #t) (kind 0)) (crate-dep (name "dialoguer") (req "^0.6.2") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.3.9") (default-features #t) (kind 0)) (crate-dep (name "vfs") (req "^0.4.0") (default-features #t) (kind 2)))) (hash "05lh2ip8y5ybzr0aw5f5igg35j6wwgfm11mgp0afv6207r4z2s7z")))

(define-public crate-img-rs-1 (crate (name "img-rs") (vers "1.0.8") (hash "12cdhvb4n1gsxwj8qidl5df137k85rzf5k8lf44kfvccs826gmrc")))

(define-public crate-img-rs-2 (crate (name "img-rs") (vers "2.27.0") (deps (list (crate-dep (name "bindgen") (req "^0.60.1") (optional #t) (default-features #t) (kind 1)) (crate-dep (name "cc") (req "^1.0.73") (default-features #t) (kind 1)) (crate-dep (name "cty") (req "^0.2.2") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.31") (default-features #t) (kind 0)))) (hash "0vf52bvr98cyblafp4y5si6b3iyphnb26mpan0655pk7nqdc44fb") (features (quote (("internal-bindgen-on-build" "bindgen"))))))

(define-public crate-img-to-ascii-0.1 (crate (name "img-to-ascii") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^3.0.7") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "image") (req "^0.23.14") (default-features #t) (kind 0)))) (hash "0gr0df8zfn90cg0mchn1v7fkra7gxlqlcza1hzd0imdw5ja1wmyg")))

(define-public crate-img-to-ascii-0.1 (crate (name "img-to-ascii") (vers "0.1.1") (deps (list (crate-dep (name "clap") (req "^3.0.7") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "image") (req "^0.23.14") (default-features #t) (kind 0)))) (hash "0fhg02my3pi0735dfj527r37r11bz0zhsnzr94nzrcsar4f56hfh")))

(define-public crate-img-to-ascii-0.1 (crate (name "img-to-ascii") (vers "0.1.2") (deps (list (crate-dep (name "clap") (req "^3.0.7") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "image") (req "^0.23.14") (default-features #t) (kind 0)))) (hash "1i7a5yhamp9nf70qav2f1wd2ri05pqld108b2i9jpc4ngbs6kh1n")))

(define-public crate-img-to-ascii-0.1 (crate (name "img-to-ascii") (vers "0.1.3") (deps (list (crate-dep (name "clap") (req "^3.0.7") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "image") (req "^0.23.14") (default-features #t) (kind 0)))) (hash "1xc8gjcnmvrr6yq5z4fjgzr8q6bsm8jk407gqasbwa3630sb5c6y")))

(define-public crate-img-to-ascii-1 (crate (name "img-to-ascii") (vers "1.0.0") (deps (list (crate-dep (name "clap") (req "^3.0.7") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "image") (req "^0.23.14") (default-features #t) (kind 0)))) (hash "0z0kzb3c22b39rkb90r0aplxr5pgi652l4a5lv4bz0mvpd966mj7")))

(define-public crate-img-tool-0.1 (crate (name "img-tool") (vers "0.1.0") (deps (list (crate-dep (name "anyhow") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^4") (features (quote ("wrap_help" "derive"))) (default-features #t) (kind 0)) (crate-dep (name "clap_complete") (req "^4") (default-features #t) (kind 0)) (crate-dep (name "color-eyre") (req "^0.6.2") (default-features #t) (kind 0)) (crate-dep (name "enum_dispatch") (req "^0.3.12") (default-features #t) (kind 0)) (crate-dep (name "ico") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "resvg") (req "^0.36.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_yaml") (req "^0.9") (default-features #t) (kind 0)) (crate-dep (name "tiny-skia") (req "^0.11.2") (default-features #t) (kind 0)))) (hash "1sj57bh1m4zkvyn29w4gcdwqf0f9ibf4jpbhch2k0mm8d7minhsl")))

