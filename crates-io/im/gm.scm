(define-module (crates-io im gm) #:use-module (crates-io))

(define-public crate-imgmod-1 (crate (name "imgmod") (vers "1.0.0") (deps (list (crate-dep (name "image") (req "^0.24.1") (default-features #t) (kind 0)) (crate-dep (name "indicatif") (req "^0.16.2") (default-features #t) (kind 0)))) (hash "04rwh1xs6rv052ww9lkj3hynl09f41cmmfbw990ddf1rmq3jv0q5")))

(define-public crate-imgmod-1 (crate (name "imgmod") (vers "1.1.0") (deps (list (crate-dep (name "image") (req "^0.24.1") (default-features #t) (kind 0)) (crate-dep (name "indicatif") (req "^0.16.2") (default-features #t) (kind 0)))) (hash "1bbry0nsfbkimqdbh7jm3k2qibfwr9b2nkx3fbx2bdwhr8pcafpx")))

(define-public crate-imgmod-1 (crate (name "imgmod") (vers "1.2.0") (deps (list (crate-dep (name "image") (req "^0.24.1") (default-features #t) (kind 0)) (crate-dep (name "indicatif") (req "^0.16.2") (default-features #t) (kind 0)))) (hash "1wx0lhh7k9h38gcgkk2izj2130vfh1pvhrs2kjp80bda0namm7pq")))

(define-public crate-imgmod-1 (crate (name "imgmod") (vers "1.2.1") (deps (list (crate-dep (name "image") (req "^0.24.1") (default-features #t) (kind 0)) (crate-dep (name "indicatif") (req "^0.16.2") (default-features #t) (kind 0)))) (hash "1xpak5m22xaywz5zvb4hhskfih73r4r52gg4glp2mq7jybqxc4di")))

