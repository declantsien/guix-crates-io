(define-module (crates-io im p_) #:use-module (crates-io))

(define-public crate-imp_ast-0.2 (crate (name "imp_ast") (vers "0.2.0") (deps (list (crate-dep (name "int") (req "^0.2.0") (default-features #t) (kind 0) (package "imp_int")) (crate-dep (name "lexer") (req "^0.2.1") (default-features #t) (kind 0) (package "imp_lexer")) (crate-dep (name "nom") (req "^7.1.3") (default-features #t) (kind 0)) (crate-dep (name "nom-supreme") (req "^0.8.0") (default-features #t) (kind 0)) (crate-dep (name "parser") (req "^0.2.2") (default-features #t) (kind 0) (package "imp_parser")) (crate-dep (name "thiserror") (req "^1.0.58") (default-features #t) (kind 0)) (crate-dep (name "tree") (req "^0.2.0") (default-features #t) (kind 0) (package "imp_tree")))) (hash "197g0ism1nmb1wj59vfqx6zg5xn4s4gfcw802ki7qr6rcc01zcqm") (yanked #t)))

(define-public crate-imp_int-0.2 (crate (name "imp_int") (vers "0.2.0") (deps (list (crate-dep (name "num-bigint") (req "^0.4.4") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2.18") (default-features #t) (kind 0)))) (hash "0mdigiy4f9f2ms56xnpb8ql20gmp4aizi3c9gqs0fmrigj097mdi") (yanked #t)))

(define-public crate-imp_int-0.3 (crate (name "imp_int") (vers "0.3.0") (deps (list (crate-dep (name "num-bigint") (req "^0.4.4") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2.18") (default-features #t) (kind 0)))) (hash "131252ydj1bn9v4gxk5za28z7kkrpczb4p73vbmj64kvjsm1kqyj") (features (quote (("bigint")))) (yanked #t)))

(define-public crate-imp_int-0.3 (crate (name "imp_int") (vers "0.3.1") (deps (list (crate-dep (name "num-bigint") (req "^0.4.4") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2.18") (default-features #t) (kind 0)))) (hash "0r3wshcnannflxn7xysfm4r2hg3hhal0fjsa41qhmgwcg4jpj7nq") (yanked #t) (v 2) (features2 (quote (("bigint" "dep:num-bigint"))))))

(define-public crate-imp_lexer-0.2 (crate (name "imp_lexer") (vers "0.2.0") (deps (list (crate-dep (name "int") (req "^0.2.0") (default-features #t) (kind 2)) (crate-dep (name "nom") (req "^7.1.3") (default-features #t) (kind 0)))) (hash "0s0c6lwl8wfb3shj3g4jxpa48sm267yczn5a5h0j702r9mwijbn9") (yanked #t)))

(define-public crate-imp_lexer-0.2 (crate (name "imp_lexer") (vers "0.2.1") (deps (list (crate-dep (name "int") (req "^0.2.0") (default-features #t) (kind 2) (package "imp_int")) (crate-dep (name "nom") (req "^7.1.3") (default-features #t) (kind 0)))) (hash "00bfs36f04ivb0kfi1zjzig5inm76agxxqjh3vhyq5m9wci446mb") (yanked #t)))

(define-public crate-imp_parser-0.2 (crate (name "imp_parser") (vers "0.2.1") (deps (list (crate-dep (name "int") (req "^0.2.0") (default-features #t) (kind 0) (package "imp_int")) (crate-dep (name "lexer") (req "^0.2.0") (default-features #t) (kind 0) (package "imp_lexer")) (crate-dep (name "nom") (req "^7.1.3") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2.18") (default-features #t) (kind 0)) (crate-dep (name "tree") (req "^0.2.0") (default-features #t) (kind 0) (package "imp_tree")))) (hash "0646anrfck2fln1ib879rb81k4098ms4g7lknyv8kglkws3gr6if") (yanked #t)))

(define-public crate-imp_parser-0.2 (crate (name "imp_parser") (vers "0.2.2") (deps (list (crate-dep (name "int") (req "^0.2.0") (default-features #t) (kind 0) (package "imp_int")) (crate-dep (name "lexer") (req "^0.2.1") (default-features #t) (kind 0) (package "imp_lexer")) (crate-dep (name "nom") (req "^7.1.3") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2.18") (default-features #t) (kind 0)) (crate-dep (name "tree") (req "^0.2.0") (default-features #t) (kind 0) (package "imp_tree")))) (hash "1wfa2gvfawaxy1v6nj2hqc31a2pz717n22m48fy3fcybr0245mxb") (yanked #t)))

(define-public crate-imp_tree-0.2 (crate (name "imp_tree") (vers "0.2.0") (hash "0lq989mskzmz257z19g800ri6qrk54g325r5k5giddniilnhcgp1") (yanked #t)))

