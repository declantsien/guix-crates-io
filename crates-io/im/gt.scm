(define-module (crates-io im gt) #:use-module (crates-io))

(define-public crate-imgthin-0.1 (crate (name "imgthin") (vers "0.1.0") (hash "11jqwq6cvg2lsikz0i7l9qbq8k2hphayggaajf24hdzdws9n5hlq") (features (quote (("improved_ysc_whh") ("default"))))))

(define-public crate-imgthin-0.1 (crate (name "imgthin") (vers "0.1.1") (hash "1nq01ynm4i5xb8b31vnp2rcc2qbzqljf9gd15rnwg3771wpnmwyx") (features (quote (("improved_ysc_whh") ("default"))))))

(define-public crate-imgtiger-1 (crate (name "imgtiger") (vers "1.0.0") (deps (list (crate-dep (name "base64") (req "^0.10.1") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^2.33.0") (default-features #t) (kind 0)))) (hash "1dqmwl6c1j0kbwc670r7pr8af5xsjh5k15913vx52jfaah53mwx2")))

(define-public crate-imgtiger-1 (crate (name "imgtiger") (vers "1.0.2") (deps (list (crate-dep (name "base64") (req "^0.10.1") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^2.33.0") (default-features #t) (kind 0)) (crate-dep (name "failure") (req "^0.1.5") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.3.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.2.1") (default-features #t) (kind 0)))) (hash "1rwx9a7id4wgvi5nwvn6f7370j9bh1mpl4mrgrllmrnr7swgwppl")))

(define-public crate-imgtiger-1 (crate (name "imgtiger") (vers "1.0.3") (deps (list (crate-dep (name "base64") (req "^0.10.1") (default-features #t) (kind 0)) (crate-dep (name "failure") (req "^0.1.5") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.3.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.2.1") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.2.18") (default-features #t) (kind 0)))) (hash "1jg63bcbvrv28167gsky2l53iqb93spw09j93fjg0cjl5kghihfr")))

