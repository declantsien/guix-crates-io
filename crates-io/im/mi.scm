(define-module (crates-io im mi) #:use-module (crates-io))

(define-public crate-immi-0.1 (crate (name "immi") (vers "0.1.0") (deps (list (crate-dep (name "time") (req "^0.1") (default-features #t) (kind 0)))) (hash "0gf0fvq0z0a73b3ns0da1683wam161a63bfah2yqvj01y1m2wv3p")))

(define-public crate-immi-0.1 (crate (name "immi") (vers "0.1.1") (deps (list (crate-dep (name "time") (req "^0.1") (default-features #t) (kind 0)))) (hash "0l7xw9xdc3xkpl95zvfz8sa83drgs0600905m067cj18my9081r6")))

(define-public crate-immi-0.1 (crate (name "immi") (vers "0.1.2") (deps (list (crate-dep (name "time") (req "^0.1") (default-features #t) (kind 0)))) (hash "12m5l59qn35f0mxs7f8pfjsp05ia4qb2r5hp3m7q7w523mlln0mf")))

(define-public crate-immi-0.1 (crate (name "immi") (vers "0.1.3") (deps (list (crate-dep (name "time") (req "^0.1") (default-features #t) (kind 0)))) (hash "0hb5kh74pmafmn9zcg87rqm59b5fswb2xl4bihhnqvh4k5xhh35f")))

(define-public crate-immi-0.2 (crate (name "immi") (vers "0.2.0") (hash "1mhq07vmxj8q5gwbqg05jk93k1zvxf03gs16m3iifpwz1pwn07g9")))

(define-public crate-immi-0.3 (crate (name "immi") (vers "0.3.0") (hash "0kxdr727x5rwjr0imf3wp1hf8yc5jaba02b4wzm576f1fbi9jacd")))

(define-public crate-immi-1 (crate (name "immi") (vers "1.0.0") (hash "09ykbkqlf4awinvnn51d6a4bsrbkw0dakb8diqbm9v4szfmg18wm")))

(define-public crate-immi-1 (crate (name "immi") (vers "1.0.1") (hash "0a3xqvazxcnihl6cb1lx0j127p3mlqwpww31bd7zb48brbwpl94n")))

(define-public crate-immi-1 (crate (name "immi") (vers "1.0.2") (hash "1kk2i6mh8rpiiv3ym6j9znzagm5f2f4qcaki1ljgw30wlpxqk1fp")))

(define-public crate-immi-1 (crate (name "immi") (vers "1.0.3") (hash "15qc6895cpify98s07adcmsa2ywblv9xpw0hbl9fc15dxw4d5ll7")))

(define-public crate-immi-1 (crate (name "immi") (vers "1.0.4") (hash "19lgm33ydm8ibpgry6pxvivzq4ipgqh75nq1q817kpxvfwkd0w3f")))

(define-public crate-immintrin-0.1 (crate (name "immintrin") (vers "0.1.0") (deps (list (crate-dep (name "simd") (req "^0.1") (default-features #t) (kind 0)))) (hash "02vm7w8ipj53mm4qj2ffkbg9fbpjd0g9mwpjx40z55qrsj955gml")))

(define-public crate-immintrin-0.2 (crate (name "immintrin") (vers "0.2.0") (deps (list (crate-dep (name "simd") (req "^0.1") (default-features #t) (kind 0)))) (hash "01klv5cb0dfqnl0ap424ws26pd9saddikdvqnvrlaymlc5h7j9f4")))

