(define-module (crates-io im lo) #:use-module (crates-io))

(define-public crate-imlogger-0.1 (crate (name "imlogger") (vers "0.1.0") (deps (list (crate-dep (name "chrono") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)))) (hash "0dlx3k9231mrv6smahkp3cdsy9fh9imx2k4x9j9330mncl9n8zcs")))

(define-public crate-imlogger-0.1 (crate (name "imlogger") (vers "0.1.1") (deps (list (crate-dep (name "chrono") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)))) (hash "1ql2y344bmhjkv61qfiz1wqz5lx85hhv6r8j5cdgbj9xa6yachv2")))

(define-public crate-imlogger-0.1 (crate (name "imlogger") (vers "0.1.2") (deps (list (crate-dep (name "chrono") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)))) (hash "0ai7qyp39mc9l5lq3dg9v2is5kp2zja1f74539w1glmmgp4qzppd")))

(define-public crate-imlogger-0.1 (crate (name "imlogger") (vers "0.1.3") (deps (list (crate-dep (name "chrono") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)))) (hash "17sp3xi8bigq30x15ib8caw5p7lmz1gfrywgs44zxcdkrpy1aabg")))

(define-public crate-imlogger-0.1 (crate (name "imlogger") (vers "0.1.4") (deps (list (crate-dep (name "chrono") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)))) (hash "020wgfyjfq6flh6h1pd8yjxxycyj2r8x2idzsyjrvq73yb5r79fj")))

