(define-module (crates-io im m_) #:use-module (crates-io))

(define-public crate-imm_gc-0.2 (crate (name "imm_gc") (vers "0.2.0") (deps (list (crate-dep (name "imm_gc_derive") (req "^0.2") (optional #t) (default-features #t) (kind 0)))) (hash "05cq5jx9hgcvra7ja05sxyqcx6ymi8qa2m1kinrjjw5xypcpyj49") (features (quote (("trace") ("derive" "imm_gc_derive"))))))

(define-public crate-imm_gc-0.2 (crate (name "imm_gc") (vers "0.2.1") (deps (list (crate-dep (name "imm_gc_derive") (req "^0.2") (optional #t) (default-features #t) (kind 0)))) (hash "1l103ddr2z9l3z37h7zljchnpdcvjj1zyf3y3j56lwqjis9d08lq") (features (quote (("trace") ("derive" "imm_gc_derive"))))))

(define-public crate-imm_gc_derive-0.2 (crate (name "imm_gc_derive") (vers "0.2.0") (deps (list (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "synstructure") (req "^0.12") (default-features #t) (kind 0)))) (hash "0bnkawa4z20hhv2i2w3za0688ry49v623qnw76hwdiam0a3626vb")))

