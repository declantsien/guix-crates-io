(define-module (crates-io im l-) #:use-module (crates-io))

(define-public crate-iml-agent-0.1 (crate (name "iml-agent") (vers "0.1.0-alpha.1") (deps (list (crate-dep (name "env_logger") (req "^0.6.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1") (default-features #t) (kind 0)))) (hash "0ywssbns9wjzak3ygv808z1h21jgkjg674i4ls1chfc72n93x8ap")))

(define-public crate-iml-api-utils-0.2 (crate (name "iml-api-utils") (vers "0.2.0") (deps (list (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.3") (features (quote ("std" "unicode-perl"))) (kind 0)))) (hash "04vbq3aa852kf8dam9k4hxrsxjacn0wbxd55byklcm0j5xhnkiaq")))

(define-public crate-iml-wire-types-0.1 (crate (name "iml-wire-types") (vers "0.1.0") (deps (list (crate-dep (name "serde") (req "^1") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.39") (default-features #t) (kind 0)))) (hash "0q2anxw88m29619fm7ifhnxp2daiyp5ynahkiicw87221l063cr7")))

(define-public crate-iml-wire-types-0.1 (crate (name "iml-wire-types") (vers "0.1.1") (deps (list (crate-dep (name "serde") (req "^1") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.39") (default-features #t) (kind 0)))) (hash "0i8zjh4xrfh6f168yzkp92pm5v0lzayfxmjamn6b09mvl51arasc")))

(define-public crate-iml-wire-types-0.1 (crate (name "iml-wire-types") (vers "0.1.2") (deps (list (crate-dep (name "serde") (req "^1") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.39") (default-features #t) (kind 0)))) (hash "1bm6gjs389s6n2jq06wyh1lnmmvkiybn7rghwnjvy8xxsndaiyvh")))

(define-public crate-iml-wire-types-0.1 (crate (name "iml-wire-types") (vers "0.1.3") (deps (list (crate-dep (name "serde") (req "^1") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)))) (hash "09iq8s0y88p0x7k0nwy0xs7gcxpf67gbhj9rqm17px3pg77xwrpj")))

(define-public crate-iml-wire-types-0.1 (crate (name "iml-wire-types") (vers "0.1.4") (deps (list (crate-dep (name "serde") (req "^1") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)))) (hash "0jj45ly68xv49w3mmr4rmb0m76ih0aqfd0m7w5w53h5mhz1jkh4g")))

(define-public crate-iml-wire-types-0.1 (crate (name "iml-wire-types") (vers "0.1.5") (deps (list (crate-dep (name "serde") (req "^1") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)))) (hash "10x4vrdcgra4krg3bfsgy45b4n60h4ay8i349ms07l6sdn95icd1")))

(define-public crate-iml-wire-types-0.1 (crate (name "iml-wire-types") (vers "0.1.6") (deps (list (crate-dep (name "serde") (req "^1") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)))) (hash "0m64rw3qac5hl5cd3hlpss60hfnjgxhz2s2lgyw7y76rf74ca03s")))

(define-public crate-iml-wire-types-0.1 (crate (name "iml-wire-types") (vers "0.1.7") (deps (list (crate-dep (name "serde") (req "^1") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)))) (hash "006lw8394xdxlfasdrj4d0nbkpkl26qkblcb6vaf79vzwzdylnig")))

(define-public crate-iml-wire-types-0.1 (crate (name "iml-wire-types") (vers "0.1.8") (deps (list (crate-dep (name "serde") (req "^1") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)))) (hash "0zac58n44h7dadv93d2myf3948p1l1mw8781zhsdrqis8m4b8j0k")))

(define-public crate-iml-wire-types-0.1 (crate (name "iml-wire-types") (vers "0.1.9") (deps (list (crate-dep (name "serde") (req "^1") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)))) (hash "1sz4d2dpg2xv6d7mnj56r1rqq7vs1vzbxbnfdhiy03zbl8c329b7")))

(define-public crate-iml-wire-types-0.1 (crate (name "iml-wire-types") (vers "0.1.10") (deps (list (crate-dep (name "serde") (req "^1") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)))) (hash "1wd46gvp8lpz9w5awhq6dlnrcdhkhzq1zxr7c42fw0zb4ayhklqq")))

(define-public crate-iml-wire-types-0.1 (crate (name "iml-wire-types") (vers "0.1.11") (deps (list (crate-dep (name "serde") (req "^1") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "tokio-postgres") (req "^0.4.0-rc.3") (optional #t) (default-features #t) (kind 0)))) (hash "112v0pg982shvznr32rh1iwqhd2mibyn7y300amqximn2l12a9ii") (features (quote (("postgres-interop" "tokio-postgres"))))))

(define-public crate-iml-wire-types-0.2 (crate (name "iml-wire-types") (vers "0.2.0") (deps (list (crate-dep (name "serde") (req "^1") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "tokio-postgres") (req "^0.4.0-rc.3") (optional #t) (default-features #t) (kind 0)))) (hash "19b1yvpszq9aph15a4zjlzijyyqv9idag34p6yh85fpqn4f6w8hj") (features (quote (("postgres-interop" "tokio-postgres"))))))

(define-public crate-iml-wire-types-0.2 (crate (name "iml-wire-types") (vers "0.2.1") (deps (list (crate-dep (name "bytes") (req "^0.5") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "im") (req "^14.3") (features (quote ("serde"))) (default-features #t) (kind 0)) (crate-dep (name "iml-api-utils") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "postgres-types") (req "^0.1.1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_repr") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "tokio-postgres") (req "^0.5") (optional #t) (default-features #t) (kind 0)))) (hash "1bbgvxgbm04zy90wpi5nfajsfnrkp37i2z59ing62irnjpd6mk2d") (features (quote (("postgres-interop" "tokio-postgres" "postgres-types" "bytes"))))))

