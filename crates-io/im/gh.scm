(define-module (crates-io im gh) #:use-module (crates-io))

(define-public crate-imghash-1 (crate (name "imghash") (vers "1.0.0") (deps (list (crate-dep (name "image") (req "^0.24.8") (default-features #t) (kind 0)))) (hash "0cy1k3v61c4yzn279mnhv10sbbmqqdgw8ql35hmak4ap6xr7y8sr")))

(define-public crate-imghash-1 (crate (name "imghash") (vers "1.1.0") (deps (list (crate-dep (name "image") (req "^0.24.8") (default-features #t) (kind 0)))) (hash "0w82xlqfj95swf8qnriswv41qqxfpqzh5lzdjk4ll5bbyj22p595")))

(define-public crate-imghash-1 (crate (name "imghash") (vers "1.1.1") (deps (list (crate-dep (name "image") (req "^0.24.8") (default-features #t) (kind 0)))) (hash "0yxmzg5bld0s0bqh0k12s2im4caknmyn21z47ds9mpi65qngkxhq")))

(define-public crate-imghash-1 (crate (name "imghash") (vers "1.2.0") (deps (list (crate-dep (name "image") (req "^0.24.8") (features (quote ("rayon"))) (default-features #t) (kind 0)) (crate-dep (name "rayon") (req "^1.10.0") (default-features #t) (kind 0)))) (hash "0prdiqddi40iidn8vrl29cmpwnkimqv68rwj4yifjffl5rx7dakc")))

(define-public crate-imghdr-0.1 (crate (name "imghdr") (vers "0.1.0") (deps (list (crate-dep (name "clippy") (req "^0.0.79") (optional #t) (default-features #t) (kind 0)))) (hash "12gmykzsqhxrj717w39zj862qj0yrjqqzl7xga6p8y5w572l88qh")))

(define-public crate-imghdr-0.1 (crate (name "imghdr") (vers "0.1.1") (deps (list (crate-dep (name "clippy") (req "^0.0.79") (optional #t) (default-features #t) (kind 0)))) (hash "03psf7f3g38lv9laxsi680ibw3v5pq2cfmvhdbg6b1x06vs2h48d")))

(define-public crate-imghdr-0.1 (crate (name "imghdr") (vers "0.1.2") (deps (list (crate-dep (name "clippy") (req "^0.0.79") (optional #t) (default-features #t) (kind 0)))) (hash "1m55q0kfi8h24a31yfspm6dlkbg1958gr458wym52n5pf25kdhi2")))

(define-public crate-imghdr-0.1 (crate (name "imghdr") (vers "0.1.3") (deps (list (crate-dep (name "clippy") (req "^0.0.79") (optional #t) (default-features #t) (kind 0)))) (hash "1f3652kjjwjczpychhlbz4g9licmi6raybj4shrpnxg89i4v80p0")))

(define-public crate-imghdr-0.1 (crate (name "imghdr") (vers "0.1.4") (deps (list (crate-dep (name "clippy") (req "^0.0.79") (optional #t) (default-features #t) (kind 0)))) (hash "04qv18vqclnsssj3dgnq4rgl77ymhqrmkafylci34lmmb8gx5cgk")))

(define-public crate-imghdr-0.2 (crate (name "imghdr") (vers "0.2.0") (deps (list (crate-dep (name "clippy") (req "^0.0.79") (optional #t) (default-features #t) (kind 0)))) (hash "0w9f8pm7imvj31gibck85yngkx0hwfw34cfhvp9yx82dm543kk0q")))

(define-public crate-imghdr-0.3 (crate (name "imghdr") (vers "0.3.0") (deps (list (crate-dep (name "clippy") (req "^0.0.79") (optional #t) (default-features #t) (kind 0)))) (hash "1f0sir90h82j7gs765ikhv3zqxv8cskbinnwydiwvnz93vq0rghq")))

(define-public crate-imghdr-0.4 (crate (name "imghdr") (vers "0.4.0") (deps (list (crate-dep (name "clippy") (req "^0.0.79") (optional #t) (default-features #t) (kind 0)))) (hash "0m1q7868yb2xdgiz9j1is0z9wxrvnl55s6mvk5c8jvn9543h53ia")))

(define-public crate-imghdr-0.5 (crate (name "imghdr") (vers "0.5.0") (hash "0hcb7bpwfjdw76ip2dy8rzgfnl0rf0qywdsr42zcmf9bx62dji4q")))

(define-public crate-imghdr-0.6 (crate (name "imghdr") (vers "0.6.0") (hash "06mm80q1mzykkzsny6nclb4l5q33f2wd5hb3vvsbq68y2my6xszj")))

(define-public crate-imghdr-0.7 (crate (name "imghdr") (vers "0.7.0") (deps (list (crate-dep (name "criterion") (req "^0.2") (default-features #t) (kind 2)))) (hash "0a14y3bs1c2vdqmlv8wa6q5bfl04gbjdyx9kc20sqxjmv4x5zcy8") (features (quote (("std") ("default" "std"))))))

