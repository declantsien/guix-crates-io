(define-module (crates-io im no) #:use-module (crates-io))

(define-public crate-imnodes-0.1 (crate (name "imnodes") (vers "0.1.0") (deps (list (crate-dep (name "imgui") (req "^0.11.0") (default-features #t) (kind 0)) (crate-dep (name "imnodes-sys") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "1lndv3rhxnx66fxlnx1xl2s5ph7djix0xnh7kng8xdyrlz5hxa7l") (features (quote (("include_low_level_bindings"))))))

(define-public crate-imnodes-0.2 (crate (name "imnodes") (vers "0.2.0") (deps (list (crate-dep (name "imgui") (req "^0.11.0") (default-features #t) (kind 0)) (crate-dep (name "imnodes-sys") (req "^0.2.0") (default-features #t) (kind 0)))) (hash "0k1xfpclrsbmqvvyrf8clp5y9gps0a4h9ialz561lljb40qi0hfd") (features (quote (("include_low_level_bindings"))))))

(define-public crate-imnodes-0.2 (crate (name "imnodes") (vers "0.2.1") (deps (list (crate-dep (name "imgui") (req "^0.11.0") (default-features #t) (kind 0)) (crate-dep (name "imnodes-sys") (req "^0.2.0") (default-features #t) (kind 0)))) (hash "0lw5mrmvfblka5w13kdjk4icz9my4wz676s60832yxfv33p4n1vz") (features (quote (("include_low_level_bindings"))))))

(define-public crate-imnodes-0.2 (crate (name "imnodes") (vers "0.2.2") (deps (list (crate-dep (name "imgui") (req "^0.11.0") (default-features #t) (kind 0)) (crate-dep (name "imnodes-sys") (req "^0.2.2") (default-features #t) (kind 0)))) (hash "0f9x7b1mll28i359jfd1fh2v4bl2hb2afvn871hc6f2p1cncw180") (features (quote (("include_low_level_bindings"))))))

(define-public crate-imnodes-sys-0.1 (crate (name "imnodes-sys") (vers "0.1.0") (deps (list (crate-dep (name "cc") (req "^1.0.79") (default-features #t) (kind 1)) (crate-dep (name "imgui-sys") (req "^0.11.0") (default-features #t) (kind 0)))) (hash "17p4ndi4adw1rbdxp9b96661nxf1ri3wsr8ip6r6svk2ppzk65pd") (links "imnodes")))

(define-public crate-imnodes-sys-0.2 (crate (name "imnodes-sys") (vers "0.2.0") (deps (list (crate-dep (name "cc") (req "^1.0.83") (default-features #t) (kind 1)) (crate-dep (name "imgui-sys") (req "^0.11.0") (default-features #t) (kind 0)))) (hash "1x34b6g9fhd2zjjjpas92y14fs5icc8zdrwg39pcjng0c7mir9l0") (links "imnodes")))

(define-public crate-imnodes-sys-0.2 (crate (name "imnodes-sys") (vers "0.2.1") (deps (list (crate-dep (name "cc") (req "^1.0.83") (default-features #t) (kind 1)) (crate-dep (name "imgui-sys") (req "^0.11.0") (default-features #t) (kind 0)))) (hash "1zqj9wzggasamyfjya4d58avdh4j1nvgnmp0ahf1p0ql90r3i22d") (links "imnodes")))

(define-public crate-imnodes-sys-0.2 (crate (name "imnodes-sys") (vers "0.2.2") (deps (list (crate-dep (name "cc") (req "^1.0.83") (default-features #t) (kind 1)) (crate-dep (name "imgui-sys") (req "^0.11.0") (default-features #t) (kind 0)))) (hash "12iyxl5fvxiinhhv4ws4h10gwahlw1r2qw11jxv4bcypfg4727jx") (links "imnodes")))

