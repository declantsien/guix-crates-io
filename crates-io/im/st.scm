(define-module (crates-io im st) #:use-module (crates-io))

(define-public crate-imstr-0.1 (crate (name "imstr") (vers "0.1.0-alpha.1") (deps (list (crate-dep (name "serde") (req "^1.0.159") (optional #t) (default-features #t) (kind 0)))) (hash "1066b1frish5fbrw6bs07g9dfp9ag9a0l2kkidwq7k076x3rk33c") (features (quote (("debug-internal")))) (v 2) (features2 (quote (("serde" "dep:serde"))))))

(define-public crate-imstr-0.1 (crate (name "imstr") (vers "0.1.0") (deps (list (crate-dep (name "serde") (req "^1.0.159") (optional #t) (default-features #t) (kind 0)))) (hash "0yjfg86nz0g5fa6alzb8f2h7lls1h8q9s8520cfcnc2dx60g2789") (v 2) (features2 (quote (("serde" "dep:serde"))))))

(define-public crate-imstr-0.1 (crate (name "imstr") (vers "0.1.1") (deps (list (crate-dep (name "peg-runtime") (req "^0.8.1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.159") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "criterion") (req "^0.4.0") (default-features #t) (kind 2)) (crate-dep (name "peg") (req "^0.8.1") (default-features #t) (kind 2)))) (hash "0kap7l5s4vaqia0ymly3a22p630bij72wshphl0d8b3p06hghjgq") (v 2) (features2 (quote (("serde" "dep:serde") ("peg" "dep:peg-runtime"))))))

(define-public crate-imstr-0.2 (crate (name "imstr") (vers "0.2.0") (deps (list (crate-dep (name "nom") (req "^7.1.3") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "peg-runtime") (req "^0.8.1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.159") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "criterion") (req "^0.4.0") (default-features #t) (kind 2)) (crate-dep (name "peg") (req "^0.8.1") (default-features #t) (kind 2)))) (hash "1zkqxi5p8hyg837qvnwclvb8qhl5smabw4zkfascdaafn3g42d3x") (v 2) (features2 (quote (("serde" "dep:serde") ("peg" "dep:peg-runtime") ("nom" "dep:nom"))))))

