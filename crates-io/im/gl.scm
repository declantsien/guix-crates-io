(define-module (crates-io im gl) #:use-module (crates-io))

(define-public crate-imgland-0.0.0 (crate (name "imgland") (vers "0.0.0") (hash "1fmpv0ljjdxm280wjjf5h2b752hanjnkc8kgd1a2xjpq4pqgk2cl")))

(define-public crate-imglife-1 (crate (name "imglife") (vers "1.0.0") (deps (list (crate-dep (name "clap") (req "^2.31.2") (default-features #t) (kind 0)) (crate-dep (name "css-color-parser") (req "^0.1.2") (default-features #t) (kind 0)) (crate-dep (name "image") (req "^0.19.0") (default-features #t) (kind 0)))) (hash "08zqffyq3y5fid02ixcfk31x5iayg447mhxgiywapsh2q0mh2jh6")))

(define-public crate-imglife-1 (crate (name "imglife") (vers "1.0.1") (deps (list (crate-dep (name "clap") (req "^2.31.2") (default-features #t) (kind 0)) (crate-dep (name "css-color-parser") (req "^0.1.2") (default-features #t) (kind 0)) (crate-dep (name "image") (req "^0.19.0") (default-features #t) (kind 0)))) (hash "1s4rc22q91b7dxnm9p290ihnbfawyxywwgxm6gs913rihi3h5afn")))

(define-public crate-imglife-1 (crate (name "imglife") (vers "1.0.2") (deps (list (crate-dep (name "clap") (req "^2.31.2") (default-features #t) (kind 0)) (crate-dep (name "css-color-parser") (req "^0.1.2") (default-features #t) (kind 0)) (crate-dep (name "image") (req "^0.19.0") (default-features #t) (kind 0)))) (hash "0lsivc09jlnb6ilvpbji054fsj7dmy0adc7jjkwfzdz124zsj301")))

(define-public crate-imglife-1 (crate (name "imglife") (vers "1.0.3") (deps (list (crate-dep (name "clap") (req "^2.31.2") (default-features #t) (kind 0)) (crate-dep (name "css-color-parser") (req "^0.1.2") (default-features #t) (kind 0)) (crate-dep (name "image") (req "^0.19.0") (default-features #t) (kind 0)))) (hash "16ahlccwfda5j9di04cm7r2qf7mpnxv52n8f3sm7ckv2gswxkwl4")))

(define-public crate-imglife-1 (crate (name "imglife") (vers "1.0.4") (deps (list (crate-dep (name "clap") (req "^2.31.2") (default-features #t) (kind 0)) (crate-dep (name "css-color-parser") (req "^0.1.2") (default-features #t) (kind 0)) (crate-dep (name "image") (req "^0.19.0") (default-features #t) (kind 0)))) (hash "1gvzxdall1gn9wiiygzqg5dfgg59wwyi4kzajjbn7pxfv19w6sm6")))

(define-public crate-imglife-1 (crate (name "imglife") (vers "1.0.5") (deps (list (crate-dep (name "clap") (req "^2.31.2") (default-features #t) (kind 0)) (crate-dep (name "css-color-parser") (req "^0.1.2") (default-features #t) (kind 0)) (crate-dep (name "image") (req "^0.19.0") (default-features #t) (kind 0)))) (hash "12ljnvl96vhwwrjgp05h1lg87q8fwm7d9ibv0kp649vd42qlix9j")))

(define-public crate-imglife-1 (crate (name "imglife") (vers "1.0.6") (deps (list (crate-dep (name "clap") (req "^2.31.2") (default-features #t) (kind 0)) (crate-dep (name "css-color-parser") (req "^0.1.2") (default-features #t) (kind 0)) (crate-dep (name "image") (req "^0.19.0") (default-features #t) (kind 0)))) (hash "1vcy0qi0mkk11fphjm57h65px1iz6cq84hnp8x2rs5ichmgdk35p")))

(define-public crate-imglife-1 (crate (name "imglife") (vers "1.0.7") (deps (list (crate-dep (name "clap") (req "^2.31.2") (default-features #t) (kind 0)) (crate-dep (name "css-color-parser") (req "^0.1.2") (default-features #t) (kind 0)) (crate-dep (name "image") (req "^0.19.0") (default-features #t) (kind 0)))) (hash "1zydwwi2ii14j5ym8i8ajvqywl4rx2ya0q6az7c34f9p60gdika5")))

