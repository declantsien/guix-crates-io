(define-module (crates-io im gr) #:use-module (crates-io))

(define-public crate-imgr-0.1 (crate (name "imgr") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^4.4.7") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "image") (req "^0.24.7") (default-features #t) (kind 0)))) (hash "1vclsvvv63cf5n933hgy769d3srwwkbfwn81cvcxic6zr31z3vaz") (yanked #t)))

(define-public crate-imgr-0.1 (crate (name "imgr") (vers "0.1.1") (deps (list (crate-dep (name "clap") (req "^4.4.7") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "image") (req "^0.24.7") (default-features #t) (kind 0)))) (hash "0880y9ngsyz1p3h70y38z5gqka0cj91nvlqn8p7m2jmbkb9h6ccl") (yanked #t)))

(define-public crate-imgr-0.1 (crate (name "imgr") (vers "0.1.2") (deps (list (crate-dep (name "clap") (req "^4.4.7") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "image") (req "^0.24.7") (default-features #t) (kind 0)))) (hash "0hzf9wli7jfimcqgl87nczqj249b6hpmi4zxbnkaq75jj93lzr7i")))

(define-public crate-imgr-0.1 (crate (name "imgr") (vers "0.1.3") (deps (list (crate-dep (name "clap") (req "^4.4.7") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "image") (req "^0.24.7") (default-features #t) (kind 0)))) (hash "005spjxgmjvbnk25sw27g8gdyxvcifc82iwg6gg8aqiqqv3ijsfy")))

(define-public crate-imgr-0.1 (crate (name "imgr") (vers "0.1.4") (deps (list (crate-dep (name "clap") (req "^4.4.7") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "image") (req "^0.24.7") (default-features #t) (kind 0)))) (hash "1c86ijbfif2b3ir9ka4cij96fq20dxl30wgyl8y5aczi5dklvx7h")))

(define-public crate-imgr-1 (crate (name "imgr") (vers "1.0.0") (deps (list (crate-dep (name "clap") (req "^4.4.7") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "image") (req "^0.24.7") (default-features #t) (kind 0)))) (hash "11z60awqdgskic8znjq66djhn09r997ihcc3j22f8rzdk8bhaycl") (yanked #t)))

(define-public crate-imgr-1 (crate (name "imgr") (vers "1.0.1") (deps (list (crate-dep (name "clap") (req "^4.4.7") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "image") (req "^0.24.7") (default-features #t) (kind 0)))) (hash "0zds599dmhixjxn0rnnqv7x9xja39dz7z0dhpv9805acbsfc31jm")))

(define-public crate-imgref-1 (crate (name "imgref") (vers "1.0.0") (hash "0zxxjad0vlvrzbpv13xpcfa65v5qh9yzay7j8f4xzhpqmj3dxpbm") (yanked #t)))

(define-public crate-imgref-1 (crate (name "imgref") (vers "1.1.0") (hash "0s0d0l9jrxmdb00cfph72l4almfrf6lgl7c0d5kkvz701lmdfh96") (yanked #t)))

(define-public crate-imgref-1 (crate (name "imgref") (vers "1.1.1") (hash "1ndij2ryfk8ljkxglhk4cf9niilzsd9gzc53pxmzgv20zbrvk5vp") (yanked #t)))

(define-public crate-imgref-1 (crate (name "imgref") (vers "1.2.0") (hash "14gi3y0293wkj8xc69sj6gdxj7hkcib0r8qjkkbxhn6ia3q3xzi9") (yanked #t)))

(define-public crate-imgref-1 (crate (name "imgref") (vers "1.2.1") (hash "0xcr1ci1jmlsz5wb5kx4qcn6qd5qppbrwrc3ykdp2ci5zfasz6y2") (yanked #t)))

(define-public crate-imgref-1 (crate (name "imgref") (vers "1.2.2") (hash "07b3rylaznsal4fjgi9j0azblk51h89fnyd1l08m65n3zxvkgzcz") (yanked #t)))

(define-public crate-imgref-1 (crate (name "imgref") (vers "1.3.0") (hash "1f9gvx32b9skbq61n2v6hxvhc3m6x19armd8w94by0qp2c4d6lcg") (yanked #t)))

(define-public crate-imgref-1 (crate (name "imgref") (vers "1.3.1") (hash "0mfjz44vivymacf92z1yi820w1j1sb26lqval0wkbi2zypchr0lk") (yanked #t)))

(define-public crate-imgref-1 (crate (name "imgref") (vers "1.3.2") (hash "1fqk5cbsg24prpc2ka8k2za614ycy8zifyn9ljnzr59wa5w6c5h6") (yanked #t)))

(define-public crate-imgref-1 (crate (name "imgref") (vers "1.3.3") (hash "1gi5kmag5ahbj2x3js50av81gs8zrssw0p43nk5qqihzdqjlpmxr") (yanked #t)))

(define-public crate-imgref-1 (crate (name "imgref") (vers "1.3.4") (hash "0xpsk4v1h8n1pskr723w8bklsqhs0vhqan43x38wi77b90xzl794") (yanked #t)))

(define-public crate-imgref-1 (crate (name "imgref") (vers "1.3.5") (hash "1n6jrs60phiyd4gza4bjhgd4dg547j6swhmygrcnxcy0hlxjxn64") (yanked #t)))

(define-public crate-imgref-1 (crate (name "imgref") (vers "1.4.0") (hash "04m207csvhg7vgslmnrd2r5h4ym89ry7flsxwz429ya2vgcmspx4") (yanked #t)))

(define-public crate-imgref-1 (crate (name "imgref") (vers "1.4.1") (hash "0ax8bh8xms2gdp9vl9g1mlqjk36b5dgdbsyx17a83smvf2q8x8as") (yanked #t)))

(define-public crate-imgref-1 (crate (name "imgref") (vers "1.4.2") (hash "1ccbgzs5xm5pnam4xbad1702xnfdg0nnbhx3d7nz1i475jiqpnrq") (yanked #t)))

(define-public crate-imgref-1 (crate (name "imgref") (vers "1.4.3") (hash "14jzy3bjad6prz0mcrz48pwvi14jab0ps8ajnfrhsxwqjby8yds8") (yanked #t)))

(define-public crate-imgref-1 (crate (name "imgref") (vers "1.5.0") (hash "0pwbr8nm8ydzmjhbg3n6zjk3mlfrgjva07brcp9cl4qjq08vbb8p") (yanked #t)))

(define-public crate-imgref-1 (crate (name "imgref") (vers "1.6.0") (hash "0m5si8n4ls3hidnqqrq0v5q6hbnqgwlmxcnk83zinffzpccya53a") (yanked #t)))

(define-public crate-imgref-1 (crate (name "imgref") (vers "1.6.1") (hash "1f2m9lq769248bwbyzda714a8xcgk5bdpdsi20xcnavikiw449g8")))

(define-public crate-imgref-1 (crate (name "imgref") (vers "1.7.0") (hash "1bpwn4f3076fqjmh037y5i7bk0i80kf2rdfrzyrva2brpr1phxnq")))

(define-public crate-imgref-1 (crate (name "imgref") (vers "1.7.1") (hash "19dd5xss3nd40avv8az2kzicpxx71c2akiqznr616hki30w9vj07")))

(define-public crate-imgref-1 (crate (name "imgref") (vers "1.8.0") (hash "0jh584fb1khp451lvf2i6jmir8jl09yr7i5wc1xc69mjrhzzzn0k")))

(define-public crate-imgref-1 (crate (name "imgref") (vers "1.9.0") (hash "0i81gjb69y10qij8ydvkd4v17x99xv0sfkcayg93595sjhkh4vpr")))

(define-public crate-imgref-1 (crate (name "imgref") (vers "1.9.1") (hash "0ra3wl5s22ffcx18inmlryqykh7qwzr0k2gdw1i84bwkdkdw1l21")))

(define-public crate-imgref-1 (crate (name "imgref") (vers "1.9.2") (hash "1mf7h1xvb7zpshlm05xgaad2iszlqazwzslbkfch9ihdipa8qskx")))

(define-public crate-imgref-1 (crate (name "imgref") (vers "1.9.3") (hash "19jkx0bnd8w7vzb5gkxnah0m28b4pjy7k4wbp498bhjri0k7631j")))

(define-public crate-imgref-1 (crate (name "imgref") (vers "1.9.4") (hash "0b3czpz206z4nvpq7yq0v58bwjmqjwjmkr302hbzpp4523glkkxj")))

(define-public crate-imgref-1 (crate (name "imgref") (vers "1.10.0") (hash "0p03zlszq904z4pc21932ph6p08z2w5482v4r54l83zh6kil9nch") (features (quote (("deprecated") ("default" "deprecated"))))))

(define-public crate-imgref-1 (crate (name "imgref") (vers "1.10.1") (hash "09l18s80crfn7g8ank3v44g43xns4pg7f6hpaz3sfna1bwsxmzj4") (features (quote (("deprecated") ("default" "deprecated"))))))

(define-public crate-imgref-iter-0.1 (crate (name "imgref-iter") (vers "0.1.0") (deps (list (crate-dep (name "imgref") (req "^1.9.2") (default-features #t) (kind 0)))) (hash "0p7a6g383kr8vm0d9k3qhccy7q1gm53si6fav654xd6klby5mpmv") (rust-version "1.56")))

(define-public crate-imgref-iter-0.1 (crate (name "imgref-iter") (vers "0.1.1") (deps (list (crate-dep (name "imgref") (req "^1.9.2") (default-features #t) (kind 0)))) (hash "0qh22p2r8gk7vfvskjbp5qdjjvsxpfn7cv2n1ak3m4ffhs7y3qg8") (rust-version "1.56")))

(define-public crate-imgref-iter-0.1 (crate (name "imgref-iter") (vers "0.1.2") (deps (list (crate-dep (name "imgref") (req "^1.9.2") (default-features #t) (kind 0)))) (hash "0zij5kwi02v1nm5c9almnx0vd5i5anx0mwrc5l0wrkw1yzm3jymk") (yanked #t) (rust-version "1.56")))

(define-public crate-imgref-iter-0.2 (crate (name "imgref-iter") (vers "0.2.0") (deps (list (crate-dep (name "imgref") (req "^1.9.2") (default-features #t) (kind 0)))) (hash "1gxnmxc9f8dd2459gmfskph0ybypggwafpwcx5acfc4yj1rsxv02") (rust-version "1.56")))

(define-public crate-imgref-iter-0.3 (crate (name "imgref-iter") (vers "0.3.0") (deps (list (crate-dep (name "imgref") (req "^1.9.2") (default-features #t) (kind 0)))) (hash "05rzqmcplwrn9a5dzji4sgakbmdfim0ja9mn5hvdg373jyd7npkx") (features (quote (("simd")))) (rust-version "1.56")))

(define-public crate-imgref-iter-0.3 (crate (name "imgref-iter") (vers "0.3.1") (deps (list (crate-dep (name "imgref") (req "^1.9.2") (default-features #t) (kind 0)))) (hash "1270j24xzfj9pk2zbyrvk1k8n0gc6vfjjmchhg0720xl2w2z3a0f") (features (quote (("simd")))) (rust-version "1.56")))

(define-public crate-imgref-iter-0.3 (crate (name "imgref-iter") (vers "0.3.2") (deps (list (crate-dep (name "imgref") (req "^1.9.2") (default-features #t) (kind 0)))) (hash "0ihxrwkdx0c0fndz5yc6zrs7x7lsd4g9yhs4wwf771s90dla4kqp") (features (quote (("simd")))) (rust-version "1.56")))

(define-public crate-imgref-iter-0.3 (crate (name "imgref-iter") (vers "0.3.3") (deps (list (crate-dep (name "imgref") (req "^1.9.2") (default-features #t) (kind 0)))) (hash "0bp61002jicyjxd4mhsrlfvfxf15vcd7d8nlqkijrn4sdi1x6a4y") (features (quote (("simd")))) (rust-version "1.56")))

(define-public crate-imgref-iter-0.4 (crate (name "imgref-iter") (vers "0.4.0") (deps (list (crate-dep (name "imgref") (req "^1.9.2") (default-features #t) (kind 0)))) (hash "1akn14lw5m3x5jfmgphrhwk97cnh8l29x97bvik8n5zfq0rjf4aq") (features (quote (("simd")))) (rust-version "1.63")))

