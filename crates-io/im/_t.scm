(define-module (crates-io im _t) #:use-module (crates-io))

(define-public crate-im_ternary_tree-0.0.1 (crate (name "im_ternary_tree") (vers "0.0.1-a1") (hash "16m8bd32680cad4fi602hfw86cfr26qlmw4qdhzfnnb1yhhz1k3p")))

(define-public crate-im_ternary_tree-0.0.1 (crate (name "im_ternary_tree") (vers "0.0.1-a2") (hash "1gxq67gpvc5alfgzwrfdcjnvasmk8x2h33mhsyb53m36636ycz04")))

(define-public crate-im_ternary_tree-0.0.1 (crate (name "im_ternary_tree") (vers "0.0.1-a3") (hash "0w0q1rg20f2vdavamcd4b97hv5y9lvw54w2mnad0bcm7wfnsnvds")))

(define-public crate-im_ternary_tree-0.0.1 (crate (name "im_ternary_tree") (vers "0.0.1-a4") (deps (list (crate-dep (name "criterion") (req "^0.3") (default-features #t) (kind 2)))) (hash "0lwgc1zi5h7hm782gabdav00mnam4fl30kh7a0km5n2cz0iqiyn4")))

(define-public crate-im_ternary_tree-0.0.1 (crate (name "im_ternary_tree") (vers "0.0.1-a5") (deps (list (crate-dep (name "criterion") (req "^0.3") (default-features #t) (kind 2)))) (hash "0w7n4v63c8lgxk1nhwls46qdghk83gr0gwdqmrnn5fpn2wm6p07x")))

(define-public crate-im_ternary_tree-0.0.1 (crate (name "im_ternary_tree") (vers "0.0.1-a6") (deps (list (crate-dep (name "criterion") (req "^0.3") (default-features #t) (kind 2)))) (hash "0cn8bazczrg7f9y90s4gwklwbfc8fdk33a7jjzyq8j9f4jxa3i4j")))

(define-public crate-im_ternary_tree-0.0.1 (crate (name "im_ternary_tree") (vers "0.0.1-a7") (deps (list (crate-dep (name "criterion") (req "^0.3") (default-features #t) (kind 2)))) (hash "1a99dnld4zb57fvrvhjfd3s0rajb21rg2z6awswaa5k7vksigvhd")))

(define-public crate-im_ternary_tree-0.0.1 (crate (name "im_ternary_tree") (vers "0.0.1-a8") (deps (list (crate-dep (name "criterion") (req "^0.3") (default-features #t) (kind 2)))) (hash "0jxw5qfnvvx1987683rphpgk5zaz3q2imnf9pbh23qkzbv1j5058")))

(define-public crate-im_ternary_tree-0.0.2 (crate (name "im_ternary_tree") (vers "0.0.2-a1") (deps (list (crate-dep (name "criterion") (req "^0.3") (default-features #t) (kind 2)))) (hash "1m9n469lgsjj8i5bqzxb49z8k8044wpaia8qzyydhi5k2zlswav5")))

(define-public crate-im_ternary_tree-0.0.2 (crate (name "im_ternary_tree") (vers "0.0.2-a2") (deps (list (crate-dep (name "criterion") (req "^0.3") (default-features #t) (kind 2)))) (hash "0na5m72cw6pddgn974wd0rbkm1lr65jmjvyhm37kja05g2gjpqdn")))

(define-public crate-im_ternary_tree-0.0.2 (crate (name "im_ternary_tree") (vers "0.0.2") (deps (list (crate-dep (name "criterion") (req "^0.3") (default-features #t) (kind 2)))) (hash "00pbiamscn6hmp3lkw23fn43bljcnayn40y3w3bdxwl1205lrcff")))

(define-public crate-im_ternary_tree-0.0.3 (crate (name "im_ternary_tree") (vers "0.0.3") (deps (list (crate-dep (name "criterion") (req "^0.3") (default-features #t) (kind 2)))) (hash "0nyr2dmqlwyk0p0nzb0xilsycpgdypl2wx143r1n75c8vw9i78vh")))

(define-public crate-im_ternary_tree-0.0.4 (crate (name "im_ternary_tree") (vers "0.0.4") (deps (list (crate-dep (name "criterion") (req "^0.3") (default-features #t) (kind 2)))) (hash "0jnq15yvswz8i8bi80gsaqgfbr2msqbd6y4f0d7bhz6xb6dvwi0y")))

(define-public crate-im_ternary_tree-0.0.5 (crate (name "im_ternary_tree") (vers "0.0.5-a1") (deps (list (crate-dep (name "criterion") (req "^0.3") (default-features #t) (kind 2)))) (hash "0h1ivry3xxaykffy46l0dbk0gf7bwpk61qmkg7p8jjcflxdc2af7")))

(define-public crate-im_ternary_tree-0.0.5 (crate (name "im_ternary_tree") (vers "0.0.5") (deps (list (crate-dep (name "criterion") (req "^0.3") (default-features #t) (kind 2)))) (hash "0s0yjzm15k8k2gvxiy1h7bx4yiiq7llickj81iscgsjiin4zzzxd")))

(define-public crate-im_ternary_tree-0.0.6 (crate (name "im_ternary_tree") (vers "0.0.6") (deps (list (crate-dep (name "criterion") (req "^0.3") (default-features #t) (kind 2)))) (hash "1ix7mxlh989spm9r013hsp1pbb0jqampc4r6x97bsm9465vb7klp")))

(define-public crate-im_ternary_tree-0.0.7 (crate (name "im_ternary_tree") (vers "0.0.7") (deps (list (crate-dep (name "criterion") (req "^0.3") (default-features #t) (kind 2)))) (hash "045gfjqfqcjyai3avw8b8792mx3xjnkyl4zga774z1cbaifb76cp")))

(define-public crate-im_ternary_tree-0.0.8 (crate (name "im_ternary_tree") (vers "0.0.8") (deps (list (crate-dep (name "criterion") (req "^0.3") (default-features #t) (kind 2)))) (hash "1ycy692q4wbwsayq988h16ljzl2b9d1w96zs3gjiwxarw4fbc9h5")))

(define-public crate-im_ternary_tree-0.0.9 (crate (name "im_ternary_tree") (vers "0.0.9") (deps (list (crate-dep (name "criterion") (req "^0.3") (default-features #t) (kind 2)))) (hash "01xxbr8bksn3y5vwwxzi09hx25a6ln7y2fc0wz104fddszdwc1rz")))

(define-public crate-im_ternary_tree-0.0.10 (crate (name "im_ternary_tree") (vers "0.0.10") (deps (list (crate-dep (name "criterion") (req "^0.3") (default-features #t) (kind 2)))) (hash "08pxya9grvgd1ak1cf7y8w796d0m3fkqla03cznc6n88wi6zy1lb")))

(define-public crate-im_ternary_tree-0.0.11 (crate (name "im_ternary_tree") (vers "0.0.11") (deps (list (crate-dep (name "criterion") (req "^0.3") (default-features #t) (kind 2)))) (hash "1hxsckqaq1skhiamxhzkzjyggj8r3s3a8ypcz756r514xp7y0s2j")))

(define-public crate-im_ternary_tree-0.0.12 (crate (name "im_ternary_tree") (vers "0.0.12") (deps (list (crate-dep (name "criterion") (req "^0.3") (default-features #t) (kind 2)))) (hash "0r6p12mb84wa3qq9j8f03pinr6fd4ikmkvw4j6z2w9zhhff3v49w")))

(define-public crate-im_ternary_tree-0.0.13 (crate (name "im_ternary_tree") (vers "0.0.13") (deps (list (crate-dep (name "criterion") (req "^0.3") (default-features #t) (kind 2)))) (hash "1bl1lzsf8cgj9m96ff00frd56yawapwn4n5rsjnxmm1vl0wrprah")))

(define-public crate-im_ternary_tree-0.0.14 (crate (name "im_ternary_tree") (vers "0.0.14") (deps (list (crate-dep (name "criterion") (req "^0.3") (default-features #t) (kind 2)))) (hash "1igyxkcr0hr079g2xayzadp56jqn36pkhzrw3hcgggqcivqg0811")))

(define-public crate-im_ternary_tree-0.0.15 (crate (name "im_ternary_tree") (vers "0.0.15") (deps (list (crate-dep (name "criterion") (req "^0.3") (default-features #t) (kind 2)))) (hash "1xfmbr2z2694l6d4pvp26arwd9drwiy38nia91xbjhh6s6ijpi4a")))

(define-public crate-im_ternary_tree-0.0.16 (crate (name "im_ternary_tree") (vers "0.0.16") (deps (list (crate-dep (name "criterion") (req "^0.3") (default-features #t) (kind 2)))) (hash "1802z4665c3av1mdswjmip1nmin2swkd2d7bdjr40ps1bqxsbxpy")))

(define-public crate-im_ternary_tree-0.0.17 (crate (name "im_ternary_tree") (vers "0.0.17") (deps (list (crate-dep (name "criterion") (req "^0.3") (default-features #t) (kind 2)))) (hash "1xrpgg9rd4w43cd5k4hnvcrvdxrnahh4zd4lgphzsildbadlx30l")))

(define-public crate-im_ternary_tree-0.0.18 (crate (name "im_ternary_tree") (vers "0.0.18") (deps (list (crate-dep (name "criterion") (req "^0.3") (default-features #t) (kind 2)))) (hash "1dh2vxa1lnx77a489bxwvqs8x1kzbrd3rc6h35nkc3njnapp0m55")))

