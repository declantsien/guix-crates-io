(define-module (crates-io im vi) #:use-module (crates-io))

(define-public crate-imview-0.1 (crate (name "imview") (vers "0.1.0") (deps (list (crate-dep (name "image") (req "^0.23.12") (default-features #t) (kind 0)) (crate-dep (name "pallete") (req "^1.0.0") (default-features #t) (kind 0)))) (hash "0s1xxscgkk4rlhbydgf52g7ffxq9s4l9yyvv0pmmb1kiq4yhlddi")))

