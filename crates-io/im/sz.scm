(define-module (crates-io im sz) #:use-module (crates-io))

(define-public crate-imsz-0.2 (crate (name "imsz") (vers "0.2.0") (deps (list (crate-dep (name "clap") (req "^3.1.8") (features (quote ("derive"))) (default-features #t) (kind 2)))) (hash "0xbk40v1m7i9bh93ys2ik981hpx176b5pbwyrfy991vhigx4n94w")))

(define-public crate-imsz-0.2 (crate (name "imsz") (vers "0.2.1") (deps (list (crate-dep (name "clap") (req "^3.1.8") (features (quote ("derive"))) (default-features #t) (kind 2)))) (hash "088m10anz2c0dbx3yzwlgiwx2pliykmlrai42pwd5345nala86qs") (rust-version "1.59")))

(define-public crate-imsz-0.2 (crate (name "imsz") (vers "0.2.2") (deps (list (crate-dep (name "clap") (req "^3.1.8") (features (quote ("derive"))) (default-features #t) (kind 2)))) (hash "0zrz6nnfn2ps3zqyhrawncgbhldvgvjf3w8x4jxbql47pjp9x93n") (rust-version "1.59")))

(define-public crate-imsz-0.3 (crate (name "imsz") (vers "0.3.1") (deps (list (crate-dep (name "clap") (req "^3.1.8") (features (quote ("derive"))) (default-features #t) (kind 2)))) (hash "0amyhszlzskzvsw8zdqvy5hhimn5i4fab75ax6m3nks6cc7h4nsx") (rust-version "1.59")))

