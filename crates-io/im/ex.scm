(define-module (crates-io im ex) #:use-module (crates-io))

(define-public crate-imex-0.1 (crate (name "imex") (vers "0.1.0") (hash "1ggnljn5syw3kw1i9dxz5ldpljacv6hk94d72nrir6a4kydxc9w6")))

(define-public crate-imex-0.2 (crate (name "imex") (vers "0.2.0") (deps (list (crate-dep (name "nom") (req "^5.1.2") (default-features #t) (kind 0)))) (hash "0bpdrhj8y39gg0rx8pv7f3rhyhs1lvchyf4mwv9k1aykf0yh3h8n")))

(define-public crate-imex-0.2 (crate (name "imex") (vers "0.2.1") (deps (list (crate-dep (name "clap") (req "^2.33.3") (default-features #t) (kind 0)) (crate-dep (name "nom") (req "^5.1.2") (default-features #t) (kind 0)))) (hash "1c7pi4zlx1rpwj412fs39f8lg27szv5w2l4ng5zymymk94ywsk30")))

