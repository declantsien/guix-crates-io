(define-module (crates-io im gi) #:use-module (crates-io))

(define-public crate-imgine-0.1 (crate (name "imgine") (vers "0.1.0") (hash "0dfkaay6hmj43xd8ykrg8j84wiqz92l602y3afi5134bs9g49xcl")))

(define-public crate-imgix-0.1 (crate (name "imgix") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^2.33") (default-features #t) (kind 0)))) (hash "069z8gbik2d8iw7q43ahrk2cairfrcg24xy16a59igla1kfpws72")))

(define-public crate-imgix-0.1 (crate (name "imgix") (vers "0.1.1") (deps (list (crate-dep (name "clap") (req "^2.33") (default-features #t) (kind 0)))) (hash "1kn3lgvw9b6ji9nk9w2n1i2iy4s9c4yb5pphfj4cy45v8mshcgp7")))

(define-public crate-imgix-0.1 (crate (name "imgix") (vers "0.1.2") (deps (list (crate-dep (name "clap") (req "^2.33") (default-features #t) (kind 0)))) (hash "0jml63i08km5lq3g8h9majd9p5h2lr2byywbplfh5pdsd4lkqlyd")))

(define-public crate-imgix-0.1 (crate (name "imgix") (vers "0.1.3") (deps (list (crate-dep (name "clap") (req "^2.33") (default-features #t) (kind 0)) (crate-dep (name "criterion") (req "^0.3") (default-features #t) (kind 2)))) (hash "1d6sf9yzp2hrvdccxp9xwv1v66cxvz8lpsf3c87dpqwprabz9fib")))

