(define-module (crates-io im gp) #:use-module (crates-io))

(define-public crate-imgproc-0.1 (crate (name "imgproc") (vers "0.1.0") (deps (list (crate-dep (name "yuv-sys") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "14xyn66yfsnjfm1gg4q5vcc8flhwz27bj8vv6iy7354wm9dyj00k")))

(define-public crate-imgproc-0.1 (crate (name "imgproc") (vers "0.1.1") (deps (list (crate-dep (name "yuv-sys") (req "^0.2.1") (default-features #t) (kind 0)))) (hash "1r0jmk6p62vh492ib4yyw5xw2vz7xpg99cqqbwik9krvlxir909y")))

(define-public crate-imgproc-0.3 (crate (name "imgproc") (vers "0.3.0") (deps (list (crate-dep (name "yuv-sys") (req "^0.3.0") (default-features #t) (kind 0)))) (hash "02i3rqfgz4r7dwrhymb7f1paw8sdkndx0ix38x1svkw4fx6r5v12")))

(define-public crate-imgproc-0.3 (crate (name "imgproc") (vers "0.3.1") (deps (list (crate-dep (name "yuv-sys") (req "^0.3.0") (default-features #t) (kind 0)))) (hash "1dajgyiq8p5hp90lkjd50s7y5mxdfr1yq0azxnmwjgk652pljl86")))

(define-public crate-imgproc-0.3 (crate (name "imgproc") (vers "0.3.2") (deps (list (crate-dep (name "yuv-sys") (req "^0.3.0") (default-features #t) (kind 0)))) (hash "137njxawgzps0f3mw81zmba02f5sm83zn83q7ka5c9ig9hlrvm3z")))

(define-public crate-imgproc-0.3 (crate (name "imgproc") (vers "0.3.3") (deps (list (crate-dep (name "yuv-sys") (req "^0.3.0") (default-features #t) (kind 0)))) (hash "09v7y4ja96dq0bwfx8zqifjyyykxdmrdgxlz3khmawz6x62rc4qj")))

(define-public crate-imgproc-0.3 (crate (name "imgproc") (vers "0.3.4") (deps (list (crate-dep (name "yuv-sys") (req "^0.3.0") (default-features #t) (kind 0)))) (hash "1j20ynrikzmiax0pjvzzjv79pi5zhvfpss22y2l1gvsrixpcx6qf")))

(define-public crate-imgproc-0.3 (crate (name "imgproc") (vers "0.3.5") (deps (list (crate-dep (name "yuv-sys") (req "^0.3.0") (default-features #t) (kind 0)))) (hash "0yywxzvx6km9xadnwccvk0fac55z69sy7jkdij12nlhbwgsrqflx")))

(define-public crate-imgproc-0.3 (crate (name "imgproc") (vers "0.3.6") (deps (list (crate-dep (name "yuv-sys") (req "^0.3.1") (default-features #t) (kind 0)))) (hash "0hd3pk879hgzgvzcbc4r9iigg9sqbzpdakd37spyc5qxzq37156f")))

(define-public crate-imgproc-0.3 (crate (name "imgproc") (vers "0.3.7") (deps (list (crate-dep (name "yuv-sys") (req "^0.3.2") (default-features #t) (kind 0)))) (hash "1b3a6831mq4vavhz1q7wcblwp6gwmyv441d8rdjqkwvglsl5q3nw")))

(define-public crate-imgproc-0.3 (crate (name "imgproc") (vers "0.3.8") (deps (list (crate-dep (name "yuv-sys") (req "^0.3.3") (default-features #t) (kind 0)))) (hash "0p4sg2h7x4hx9yqq0xfmx8rij35xb3kpp808bjfyailk1w4nhvw3")))

(define-public crate-imgproc-0.3 (crate (name "imgproc") (vers "0.3.9") (deps (list (crate-dep (name "yuv-sys") (req "^0.3.4") (default-features #t) (kind 0)))) (hash "14ah425qvy4jwkqfayyqqjk2q2wk23v2jlbrcvigj8ndzhvyimmb")))

(define-public crate-imgproc-0.3 (crate (name "imgproc") (vers "0.3.10") (deps (list (crate-dep (name "yuv-sys") (req "^0.3.5") (default-features #t) (kind 0)))) (hash "16d02lm6q4wj78dh9qbr9f4prbymh9nqgllhwakcjb6cwqry1y5i")))

(define-public crate-imgproc-0.3 (crate (name "imgproc") (vers "0.3.11") (deps (list (crate-dep (name "yuv-sys") (req "^0.3.6") (default-features #t) (kind 0)))) (hash "14chpjnjmvvqhihlc86jk0px976x7bay2mhyr61bb248094j3qnc")))

(define-public crate-imgproc-rs-0.1 (crate (name "imgproc-rs") (vers "0.1.0") (deps (list (crate-dep (name "image") (req "^0.23.12") (default-features #t) (kind 0)) (crate-dep (name "rulinalg") (req "^0.4.2") (default-features #t) (kind 0)))) (hash "1j3401i40fl0d6m6fc1xdhyavcab3gg4mwkc5rqcbsfdys36s9wk")))

(define-public crate-imgproc-rs-0.1 (crate (name "imgproc-rs") (vers "0.1.1") (deps (list (crate-dep (name "image") (req "^0.23.12") (default-features #t) (kind 0)) (crate-dep (name "rulinalg") (req "^0.4.2") (default-features #t) (kind 0)))) (hash "16azcgnrykj9xcxrkvf6f741hm8ars162j2jw15m01qvi95w6n5x")))

(define-public crate-imgproc-rs-0.2 (crate (name "imgproc-rs") (vers "0.2.0") (deps (list (crate-dep (name "image") (req "^0.23.12") (default-features #t) (kind 0)) (crate-dep (name "rayon") (req "^1.5.0") (default-features #t) (kind 0)) (crate-dep (name "rulinalg") (req "^0.4.2") (default-features #t) (kind 0)))) (hash "17wvzbl0hxfqlxqay69mm7xa11dqqxg9dqd6c39hsv50jlzp047g")))

(define-public crate-imgproc-rs-0.2 (crate (name "imgproc-rs") (vers "0.2.1") (deps (list (crate-dep (name "image") (req "^0.23.12") (default-features #t) (kind 0)) (crate-dep (name "rayon") (req "^1.5.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "rulinalg") (req "^0.4.2") (default-features #t) (kind 0)))) (hash "1xvs1s342aa2jn4xjzj43xly33qswnaassymvmlkriydgx41ii93") (features (quote (("parallel" "rayon"))))))

(define-public crate-imgproc-rs-0.2 (crate (name "imgproc-rs") (vers "0.2.2") (deps (list (crate-dep (name "image") (req "^0.23.12") (default-features #t) (kind 0)) (crate-dep (name "rayon") (req "^1.5.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "rulinalg") (req "^0.4.2") (default-features #t) (kind 0)))) (hash "18fm4k01lqyp3gh4xfib321jba3n8iwkfw991y985ykfff9xmrvz") (features (quote (("parallel" "rayon"))))))

(define-public crate-imgproc-rs-0.2 (crate (name "imgproc-rs") (vers "0.2.3") (deps (list (crate-dep (name "image") (req "^0.23.12") (default-features #t) (kind 0)) (crate-dep (name "rayon") (req "^1.5.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "rulinalg") (req "^0.4.2") (default-features #t) (kind 0)))) (hash "0ikccj65fk2z0l46mznapk2vpy1gqvd80c3xw871dkmpvl95kdy9") (features (quote (("parallel" "rayon"))))))

(define-public crate-imgproc-rs-0.3 (crate (name "imgproc-rs") (vers "0.3.0") (deps (list (crate-dep (name "criterion") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "image") (req "^0.23.12") (default-features #t) (kind 0)) (crate-dep (name "rayon") (req "^1.5.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "rulinalg") (req "^0.4.2") (default-features #t) (kind 0)))) (hash "0n9ilk17ihdzq4n7m137zg4spcz5xbgw5bgqy1abr32b9hdw17jy") (features (quote (("simd") ("parallel" "rayon") ("default" "simd"))))))

