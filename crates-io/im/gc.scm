(define-module (crates-io im gc) #:use-module (crates-io))

(define-public crate-imgcat-0.1 (crate (name "imgcat") (vers "0.1.0") (deps (list (crate-dep (name "base64") (req "^0.11.0") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.3.5") (default-features #t) (kind 0)))) (hash "0z1bppcq34vy0nivjxipxz7dsgbzddnj7i6s6yndl7vwbw95s7ch")))

(define-public crate-imgcatr-0.1 (crate (name "imgcatr") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^4.4.11") (features (quote ("cargo" "string"))) (default-features #t) (kind 0)) (crate-dep (name "image") (req "^0.24") (default-features #t) (kind 0)) (crate-dep (name "term_size") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "winapi") (req "^0.3") (features (quote ("winbase" "wincon"))) (default-features #t) (target "cfg(target_os = \"windows\")") (kind 0)))) (hash "16l4yafqds128vfql7v96qshs73pabcvsb1143y302ji55r9f9jl")))

(define-public crate-imgcatr-0.1 (crate (name "imgcatr") (vers "0.1.1") (deps (list (crate-dep (name "clap") (req "^4.4.11") (features (quote ("cargo" "string"))) (default-features #t) (kind 0)) (crate-dep (name "image") (req "^0.24") (default-features #t) (kind 0)) (crate-dep (name "term_size") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "winapi") (req "^0.3") (features (quote ("winbase" "wincon"))) (default-features #t) (target "cfg(target_os = \"windows\")") (kind 0)))) (hash "0wlhh93v1kyd3bhapj0pb1b5vcis9abqxhw9d9vqbclkfh58b8gk")))

(define-public crate-imgcatr-0.1 (crate (name "imgcatr") (vers "0.1.2") (deps (list (crate-dep (name "clap") (req "^4.4.11") (features (quote ("cargo" "string"))) (default-features #t) (kind 0)) (crate-dep (name "image") (req "^0.24") (default-features #t) (kind 0)) (crate-dep (name "term_size") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "winapi") (req "^0.3") (features (quote ("winbase" "wincon"))) (default-features #t) (target "cfg(target_os = \"windows\")") (kind 0)))) (hash "1pici3l2kcrmqjxd02x6nnf38yhw3641jzckab38iww99iv71wqk")))

(define-public crate-imgcatr-0.1 (crate (name "imgcatr") (vers "0.1.4") (deps (list (crate-dep (name "clap") (req "^4.4.11") (features (quote ("cargo" "string"))) (default-features #t) (kind 0)) (crate-dep (name "image") (req "^0.24") (default-features #t) (kind 0)) (crate-dep (name "term_size") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "winapi") (req "^0.3") (features (quote ("winbase" "wincon"))) (default-features #t) (target "cfg(target_os = \"windows\")") (kind 0)))) (hash "0czlhylb4qllmrn55mh1f2cfgc6li3fv58vxcyjsn5qzhvw9i7cl")))

(define-public crate-imgcnvrt-0.1 (crate (name "imgcnvrt") (vers "0.1.0") (deps (list (crate-dep (name "assert_cmd") (req "^2.0.1") (default-features #t) (kind 2)) (crate-dep (name "image") (req "^0.23.14") (default-features #t) (kind 0)) (crate-dep (name "predicates") (req "^2.0.2") (default-features #t) (kind 2)))) (hash "05fbhwvw02kwb4xbgdm7i9jmbnxyzr0gd8zb8m290kzrpzrksmqr")))

(define-public crate-imgcnvrt-0.2 (crate (name "imgcnvrt") (vers "0.2.0") (deps (list (crate-dep (name "assert_cmd") (req "^2.0.1") (default-features #t) (kind 2)) (crate-dep (name "image") (req "^0.23.14") (default-features #t) (kind 0)) (crate-dep (name "predicates") (req "^2.0.2") (default-features #t) (kind 2)))) (hash "1rjjxf1bhwf6ap0ilr1kvmf608kdn66qq04ma3rzmq7v87jdyapf")))

(define-public crate-imgcompare-0.1 (crate (name "imgcompare") (vers "0.1.0") (deps (list (crate-dep (name "image") (req "^0.14") (default-features #t) (kind 0)))) (hash "0aickhjbh709bks7wv3385dykyvjkm985yx3w83d1d4h70n2dyp7")))

(define-public crate-imgcompare-0.1 (crate (name "imgcompare") (vers "0.1.1") (deps (list (crate-dep (name "image") (req "^0.14") (default-features #t) (kind 0)))) (hash "1mq5m6g1vps98shbbvsv0nhm6mh3rif7jkvk05zwygh7lpvlywsr")))

