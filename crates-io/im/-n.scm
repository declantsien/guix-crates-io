(define-module (crates-io im -n) #:use-module (crates-io))

(define-public crate-im-native-dialog-0.1 (crate (name "im-native-dialog") (vers "0.1.0") (deps (list (crate-dep (name "crossbeam-channel") (req "^0.5") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "native-dialog") (req "^0.5") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1") (default-features #t) (kind 0)))) (hash "0irsda6jjrhyfrnix56bam33r0filxx9jf8h48wjifm20fziss49")))

(define-public crate-im-native-dialog-0.1 (crate (name "im-native-dialog") (vers "0.1.1") (deps (list (crate-dep (name "crossbeam-channel") (req "^0.5") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "native-dialog") (req "^0.5") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1") (default-features #t) (kind 0)))) (hash "051656xky70qk6pp9fdzcr9c7xgz0hmwpdc9nim2zwblm3rdww18")))

(define-public crate-im-native-dialog-0.2 (crate (name "im-native-dialog") (vers "0.2.0") (deps (list (crate-dep (name "crossbeam-channel") (req "^0.5") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "native-dialog") (req "^0.5") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1") (default-features #t) (kind 0)))) (hash "027qgh0cmzc8xw6v5nipz606qxqdxhwi7s0ny7rm5kikgrz71cxf")))

(define-public crate-im-native-dialog-0.3 (crate (name "im-native-dialog") (vers "0.3.0") (deps (list (crate-dep (name "crossbeam-channel") (req "^0.5") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "native-dialog") (req "^0.6") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1") (default-features #t) (kind 0)))) (hash "12r26v5jpv1ssqizzidgslbpcjnchxrgp8drik7p79pds1j3iah3")))

