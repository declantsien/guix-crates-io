(define-module (crates-io im gk) #:use-module (crates-io))

(define-public crate-imgk-app-0.0.1 (crate (name "imgk-app") (vers "0.0.1") (hash "01r4n63j8mhlzl5cssmg2mssjncs8rg0m68i24kip9mlh2c3izin")))

(define-public crate-imgk-app-0.0.2 (crate (name "imgk-app") (vers "0.0.2") (hash "0b06c87jjzwh29095jqznjm7pr5h2k96b82x450dlf42981k6wf6")))

