(define-module (crates-io im ei) #:use-module (crates-io))

(define-public crate-imei-1 (crate (name "imei") (vers "1.0.0") (deps (list (crate-dep (name "chrono") (req "^0.4") (default-features #t) (kind 2)))) (hash "09n7zg2zb4dv1lmacji7l9fqhh5c8g5di100qyimjh5ja30ikkg0")))

(define-public crate-imei-1 (crate (name "imei") (vers "1.0.1") (deps (list (crate-dep (name "chrono") (req "^0.4") (default-features #t) (kind 2)))) (hash "1w1xfz7h8rfp5msffdpaff0iz2jaz7v465ybfrxwn5lq5hsll681")))

(define-public crate-imei-1 (crate (name "imei") (vers "1.0.2") (deps (list (crate-dep (name "chrono") (req "^0.4") (default-features #t) (kind 2)))) (hash "02ps8g049k315q8dijdgvj62crnhk1l7z3ax5zr6kl61r6a5bbdz")))

(define-public crate-imei-1 (crate (name "imei") (vers "1.1.0") (deps (list (crate-dep (name "chrono") (req "^0.4") (default-features #t) (kind 2)))) (hash "1mj1csj5b0hw2wqcnwvzm64986sl2jja4y5385qjhyh3pyzalmdi") (features (quote (("std") ("default" "std"))))))

(define-public crate-imei-1 (crate (name "imei") (vers "1.1.1") (deps (list (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "chrono") (req "^0.4") (default-features #t) (kind 2)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 2)))) (hash "0faqr6d1nzrz6b2z5llv77sxjcmdg3hyrpaci0hd99mximbnxf19") (features (quote (("std") ("default" "std" "serde")))) (v 2) (features2 (quote (("serde" "dep:serde"))))))

