(define-module (crates-io im li) #:use-module (crates-io))

(define-public crate-imlib2-0.1 (crate (name "imlib2") (vers "0.1.0") (deps (list (crate-dep (name "failure") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "imlib2-sys") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "leanshot_xlib") (req "^0.1") (default-features #t) (kind 0)))) (hash "0av6yqljbzf49wqizyk36i7mj9fnmb2r74c898i5bjy83braazjw")))

(define-public crate-imlib2-0.1 (crate (name "imlib2") (vers "0.1.1") (deps (list (crate-dep (name "failure") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "imlib2-sys") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "leanshot_xlib") (req "^0.1") (default-features #t) (kind 0)))) (hash "14y4zvx6q6n10qg1ihkfqff82jncphxlp2spf5cp1pr86b6hmysa")))

(define-public crate-imlib2-sys-0.1 (crate (name "imlib2-sys") (vers "0.1.0") (deps (list (crate-dep (name "bindgen") (req "^0.40") (default-features #t) (kind 1)))) (hash "1q6fgrpa6dh6dn8czs3mlan4bgcmc5y3syg8av2favb2jy5hskhw")))

