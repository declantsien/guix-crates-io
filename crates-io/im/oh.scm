(define-module (crates-io im oh) #:use-module (crates-io))

(define-public crate-imohash-0.1 (crate (name "imohash") (vers "0.1.0") (deps (list (crate-dep (name "hex") (req "^0.4.3") (default-features #t) (kind 2)) (crate-dep (name "md-5") (req "^0.10.5") (default-features #t) (kind 2)) (crate-dep (name "murmur3") (req "^0.5.2") (default-features #t) (kind 0)))) (hash "0qbhnhhnkylz6ci0h6r3cd5fxyqzszlmir55g44pprrsbb2d1vaf")))

