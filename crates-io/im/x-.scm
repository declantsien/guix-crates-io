(define-module (crates-io im x-) #:use-module (crates-io))

(define-public crate-imx-vpuwrap-rs-0.1 (crate (name "imx-vpuwrap-rs") (vers "0.1.0") (deps (list (crate-dep (name "imx-vpuwrap-safe") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "15ppsgi6g3n4qpipp7rhw2hl6ji6xdykiz6i4h90lj5ccizib9rd")))

(define-public crate-imx-vpuwrap-rs-0.1 (crate (name "imx-vpuwrap-rs") (vers "0.1.1") (deps (list (crate-dep (name "imx-vpuwrap-safe") (req "^0.1.1") (default-features #t) (kind 0)))) (hash "0405g8lc7n7by94d86vl5i1rjwl4dggv4qpgkcb8sba89m6sphjl")))

(define-public crate-imx-vpuwrap-safe-0.1 (crate (name "imx-vpuwrap-safe") (vers "0.1.0") (deps (list (crate-dep (name "imx-vpuwrap-sys") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "0zs8jgf717pp4gxyv3r93737zs8xgvca8rf5zp6d33r8kjjbhcj8")))

(define-public crate-imx-vpuwrap-safe-0.1 (crate (name "imx-vpuwrap-safe") (vers "0.1.1") (deps (list (crate-dep (name "imx-vpuwrap-sys") (req "^0.1.1") (default-features #t) (kind 0)))) (hash "0fkz5xlpvawlv9w6xcbk18grd7h6fc5i6qzdhy39i0gnk2dy36ls")))

(define-public crate-imx-vpuwrap-sys-0.1 (crate (name "imx-vpuwrap-sys") (vers "0.1.0") (hash "0k3jamq0wy7jbjiwvlbf5s2g4x1ik6254sclkgmign84qnq35vxm")))

(define-public crate-imx-vpuwrap-sys-0.1 (crate (name "imx-vpuwrap-sys") (vers "0.1.1") (hash "0q9jfyaw2q1mcip7r20ygp81cc6v31hcwwix2sb7z0xhr712knhi")))

