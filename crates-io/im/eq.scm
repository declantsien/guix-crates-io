(define-module (crates-io im eq) #:use-module (crates-io))

(define-public crate-imeq-0.0.1 (crate (name "imeq") (vers "0.0.1") (deps (list (crate-dep (name "clap") (req "^2.34.0") (features (quote ("yaml"))) (default-features #t) (kind 0)) (crate-dep (name "image") (req "^0.23.14") (default-features #t) (kind 0)) (crate-dep (name "rayon") (req "^1.5.1") (default-features #t) (kind 0)) (crate-dep (name "ring") (req "^0.16.20") (default-features #t) (kind 0)))) (hash "05cxfxl885s2h4l4l0pvlkk8py7ckfp2zx5jsfzi4l1rmpwn750m")))

(define-public crate-imeq-0.0.2 (crate (name "imeq") (vers "0.0.2") (deps (list (crate-dep (name "clap") (req "^2.34.0") (features (quote ("yaml"))) (default-features #t) (kind 0)) (crate-dep (name "image") (req "^0.23.14") (default-features #t) (kind 0)) (crate-dep (name "rayon") (req "^1.5.1") (default-features #t) (kind 0)) (crate-dep (name "ring") (req "^0.16.20") (default-features #t) (kind 0)))) (hash "0anwvn6i2vxpp346m2jg5kgzr78in9wq7w2snnkg84390rrpgzp3")))

(define-public crate-imeq-0.1 (crate (name "imeq") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^2.34.0") (features (quote ("yaml"))) (default-features #t) (kind 0)) (crate-dep (name "image") (req "^0.23.14") (default-features #t) (kind 0)) (crate-dep (name "rayon") (req "^1.5.1") (default-features #t) (kind 0)) (crate-dep (name "ring") (req "^0.16.20") (default-features #t) (kind 0)))) (hash "0zcfys7y1w05bzgc4d14zpmici0yprnfnvbgv1nqv84l7mw6g7ij")))

