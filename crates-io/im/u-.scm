(define-module (crates-io im u-) #:use-module (crates-io))

(define-public crate-imu-fusion-0.2 (crate (name "imu-fusion") (vers "0.2.1") (deps (list (crate-dep (name "csv") (req "^1.3.0") (default-features #t) (kind 2)) (crate-dep (name "libm") (req "^0.2.8") (default-features #t) (kind 0)) (crate-dep (name "nalgebra") (req "^0.32.3") (features (quote ("libm-force"))) (kind 0)))) (hash "1v4133jzb171gw53jm3qab7ld4cmqzlxwglyb6p7z8bcx2lv8ps0") (features (quote (("fusion-use-normal-sqrt"))))))

(define-public crate-imu-fusion-0.2 (crate (name "imu-fusion") (vers "0.2.4") (deps (list (crate-dep (name "csv") (req "^1.3.0") (default-features #t) (kind 2)) (crate-dep (name "libm") (req "^0.2.8") (default-features #t) (kind 0)) (crate-dep (name "nalgebra") (req "^0.32.3") (features (quote ("libm-force"))) (kind 0)))) (hash "1941xqaxcj880b3kv9szf500qdx91445ksjvlb6lyydxck19n2lb") (features (quote (("fusion-use-normal-sqrt"))))))

