(define-module (crates-io im -l) #:use-module (crates-io))

(define-public crate-im-lists-0.1 (crate (name "im-lists") (vers "0.1.0") (deps (list (crate-dep (name "criterion") (req "^0.3.5") (default-features #t) (kind 2)) (crate-dep (name "im-rc") (req "^15.0.0") (default-features #t) (kind 2)) (crate-dep (name "itertools") (req "^0.10.1") (default-features #t) (kind 0)) (crate-dep (name "proptest") (req "^1.0.0") (default-features #t) (kind 2)))) (hash "1j805zanlcgp3mi525f0xnxbbxawqpi18axkznvp7fzcmw30dbmh")))

(define-public crate-im-lists-0.2 (crate (name "im-lists") (vers "0.2.0") (deps (list (crate-dep (name "criterion") (req "^0.3.5") (default-features #t) (kind 2)) (crate-dep (name "im-rc") (req "^15.0.0") (default-features #t) (kind 2)) (crate-dep (name "itertools") (req "^0.10.1") (default-features #t) (kind 0)) (crate-dep (name "proptest") (req "^1.0.0") (default-features #t) (kind 2)))) (hash "0si9wr2im6s4mkpy6pgr9iggnrczmw9k9rcjn782g72wqcp80njc")))

(define-public crate-im-lists-0.3 (crate (name "im-lists") (vers "0.3.0") (deps (list (crate-dep (name "criterion") (req "^0.3.5") (default-features #t) (kind 2)) (crate-dep (name "im-rc") (req "^15.0.0") (default-features #t) (kind 2)) (crate-dep (name "itertools") (req "^0.10.1") (default-features #t) (kind 0)) (crate-dep (name "proptest") (req "^1.0.0") (default-features #t) (kind 2)))) (hash "0n3smxw89629d7482pb2vf4l21c8wxafaqzqj3ywwp41km8pc8k7")))

(define-public crate-im-lists-0.4 (crate (name "im-lists") (vers "0.4.0") (deps (list (crate-dep (name "criterion") (req "^0.3.5") (default-features #t) (kind 2)) (crate-dep (name "im-rc") (req "^15.0.0") (default-features #t) (kind 2)) (crate-dep (name "proptest") (req "^1.0.0") (default-features #t) (kind 2)))) (hash "02j91qqbjd6f1vznnqn14m0wszdzjsyjwp0bvjg40rajknz3xy88")))

(define-public crate-im-lists-0.5 (crate (name "im-lists") (vers "0.5.0") (deps (list (crate-dep (name "criterion") (req "^0.3.5") (default-features #t) (kind 2)) (crate-dep (name "im-rc") (req "^15.0.0") (default-features #t) (kind 2)) (crate-dep (name "proptest") (req "^1.0.0") (default-features #t) (kind 2)))) (hash "0j9rly9yfas1113x7gdbwzw1hx2w1bwvz22xdwz5clgpk5iymqfv")))

(define-public crate-im-lists-0.6 (crate (name "im-lists") (vers "0.6.0") (deps (list (crate-dep (name "criterion") (req "^0.3.5") (default-features #t) (kind 2)) (crate-dep (name "im-rc") (req "^15.0.0") (default-features #t) (kind 2)) (crate-dep (name "proptest") (req "^1.0.0") (default-features #t) (kind 2)))) (hash "08gx6xqqy6fnmdp41saxf7inb0m93ibb6mykfl95lmilwjjgn318")))

(define-public crate-im-lists-0.7 (crate (name "im-lists") (vers "0.7.0") (deps (list (crate-dep (name "criterion") (req "^0.3.5") (default-features #t) (kind 2)) (crate-dep (name "im-rc") (req "^15.0.0") (default-features #t) (kind 2)) (crate-dep (name "proptest") (req "^1.0.0") (default-features #t) (kind 2)))) (hash "1rpp8s509sr1fl9bwza85ipa333yz0j2bmfcwh8y9aslcrvnrkm8")))

(define-public crate-im-lists-0.7 (crate (name "im-lists") (vers "0.7.1") (deps (list (crate-dep (name "criterion") (req "^0.3.5") (default-features #t) (kind 2)) (crate-dep (name "im-rc") (req "^15.0.0") (default-features #t) (kind 2)) (crate-dep (name "proptest") (req "^1.0.0") (default-features #t) (kind 2)) (crate-dep (name "smallvec") (req "^1.10.0") (default-features #t) (kind 0)))) (hash "1627mzvr2q71q787b3v6g97iir89fmiahph8h8rr4i8nqkrfy1qc")))

(define-public crate-im-lists-0.8 (crate (name "im-lists") (vers "0.8.0") (deps (list (crate-dep (name "criterion") (req "^0.3.5") (default-features #t) (kind 2)) (crate-dep (name "im-rc") (req "^15.0.0") (default-features #t) (kind 2)) (crate-dep (name "proptest") (req "^1.0.0") (default-features #t) (kind 2)) (crate-dep (name "smallvec") (req "^1.10.0") (default-features #t) (kind 0)))) (hash "0qwb16ayx18ihyyanqlcc9j32i50fhd33z9kc0fhpz0zigxk6wiy")))

(define-public crate-im-lists-0.8 (crate (name "im-lists") (vers "0.8.1") (deps (list (crate-dep (name "criterion") (req "^0.3.5") (default-features #t) (kind 2)) (crate-dep (name "im-rc") (req "^15.0.0") (default-features #t) (kind 2)) (crate-dep (name "proptest") (req "^1.0.0") (default-features #t) (kind 2)) (crate-dep (name "smallvec") (req "^1.10.0") (default-features #t) (kind 0)))) (hash "12myxlk86wn5gwzb6b6asvf1mz4n382dggasb3qim3hj3yhs53f3")))

