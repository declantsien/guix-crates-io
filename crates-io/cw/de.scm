(define-module (crates-io cw de) #:use-module (crates-io))

(define-public crate-cwdemangle-0.1 (crate (name "cwdemangle") (vers "0.1.1") (deps (list (crate-dep (name "argh") (req "^0.1.8") (default-features #t) (kind 0)))) (hash "0s5ixbv6ga1wss13hfia2y5phgr8llafavrvscp5y654ccxx5l6g")))

(define-public crate-cwdemangle-0.1 (crate (name "cwdemangle") (vers "0.1.2") (deps (list (crate-dep (name "argh") (req "^0.1.8") (default-features #t) (kind 0)))) (hash "0gvr4nxgjxx1yfpqxvvj36jc778x5wnw99vv10kpxq5dywjnp26r")))

(define-public crate-cwdemangle-0.1 (crate (name "cwdemangle") (vers "0.1.3") (deps (list (crate-dep (name "argh") (req "^0.1.8") (default-features #t) (kind 0)))) (hash "0s0yp0fqr5h0bcjwrkj8cx0vv3k1gzm4bj9pnsysabq7xnjfy4g4")))

(define-public crate-cwdemangle-0.1 (crate (name "cwdemangle") (vers "0.1.4") (deps (list (crate-dep (name "argh") (req "^0.1.8") (default-features #t) (kind 0)))) (hash "13j9sd60fl9hl2xs4hnjpwcjl24admwi3rwd4iflxcakcsgn7ny6")))

(define-public crate-cwdemangle-0.1 (crate (name "cwdemangle") (vers "0.1.5") (deps (list (crate-dep (name "argh") (req "^0.1.8") (default-features #t) (kind 0)))) (hash "0wy37vfmm0c9i9m4f21kpncjw3waxskfsfnhpx70mziwl2ik93dm") (rust-version "1.58")))

(define-public crate-cwdemangle-0.1 (crate (name "cwdemangle") (vers "0.1.6") (deps (list (crate-dep (name "argh") (req "^0.1.8") (default-features #t) (kind 0)))) (hash "0qj15x62lg6mqannbks4amxmmsy2rxakk6vvbk43syrpadavqlf2") (rust-version "1.58")))

(define-public crate-cwdemangle-1 (crate (name "cwdemangle") (vers "1.0.0") (hash "11d27rpivcrqkg697bqqnb5k6cgnkyacprdiis4kqjk3rsdnzq62") (rust-version "1.58")))

(define-public crate-cwdemangle-bin-1 (crate (name "cwdemangle-bin") (vers "1.0.0") (deps (list (crate-dep (name "argh") (req "^0.1.12") (default-features #t) (kind 0)) (crate-dep (name "cwdemangle") (req "^1.0.0") (default-features #t) (kind 0)))) (hash "1599hxbszrapq5rd0zb84l0l33yc0i1rjvjifcffl3jr8q7bmi7m") (rust-version "1.58")))

