(define-module (crates-io cw tc) #:use-module (crates-io))

(define-public crate-cwtch-imp-0.2 (crate (name "cwtch-imp") (vers "0.2.0") (deps (list (crate-dep (name "chrono") (req "^0.4.19") (default-features #t) (kind 0)) (crate-dep (name "libcwtch") (req "^0.4.0") (default-features #t) (kind 0)))) (hash "0wnd3sgfwf4q1l7ygrf5w337pa3vd85zxj7wd4qxaxycvnaqxws3")))

(define-public crate-cwtch-imp-0.2 (crate (name "cwtch-imp") (vers "0.2.1") (deps (list (crate-dep (name "chrono") (req "^0.4.19") (default-features #t) (kind 0)) (crate-dep (name "libcwtch") (req "^0.6.1") (default-features #t) (kind 0)))) (hash "0sm403x1w5p968winzywbklccw5vq9kvrm77cyvh1ickap8v2kk8")))

(define-public crate-cwtch-imp-0.2 (crate (name "cwtch-imp") (vers "0.2.2") (deps (list (crate-dep (name "chrono") (req "^0.4.19") (default-features #t) (kind 0)) (crate-dep (name "libcwtch") (req "^0.6.1") (default-features #t) (kind 0)))) (hash "1pl24ny9z7hk72zrlfirxw9vy575v6mn6vda00v4bhmakxkjggkd")))

(define-public crate-cwtch-imp-0.2 (crate (name "cwtch-imp") (vers "0.2.3") (deps (list (crate-dep (name "chrono") (req "^0.4.19") (default-features #t) (kind 0)) (crate-dep (name "libcwtch") (req "^0.7.0") (default-features #t) (kind 0)))) (hash "1fhq75bzbgpkx3c2kamlpnhmmndf3h7jyhr4yhs6inhzhk3r3mhx")))

(define-public crate-cwtch-imp-0.3 (crate (name "cwtch-imp") (vers "0.3.0") (deps (list (crate-dep (name "chrono") (req "^0.4.19") (default-features #t) (kind 0)) (crate-dep (name "libcwtch") (req "^0.8.0") (default-features #t) (kind 0)))) (hash "1vianvvn52n7dlvbphvy3d9yax964bl93y4cwhmbwxxlj9b65wkj")))

