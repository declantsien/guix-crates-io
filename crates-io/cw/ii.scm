(define-module (crates-io cw ii) #:use-module (crates-io))

(define-public crate-cwiid-0.1 (crate (name "cwiid") (vers "0.1.0") (deps (list (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "libcwiid-sys") (req "^0.1.4") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2.15") (default-features #t) (kind 0)))) (hash "12i2nxbzcz1jhhsapxlkyhwcydh4cflcck5b13jhmdlasvm8dh2v")))

(define-public crate-cwiid-0.1 (crate (name "cwiid") (vers "0.1.1") (deps (list (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "libcwiid-sys") (req "^0.1.5") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2.15") (default-features #t) (kind 0)))) (hash "190fgrirdbsv0q2gw9n6my98mgafl4j3fg0rhj754h11b025i37m")))

(define-public crate-cwiid-0.1 (crate (name "cwiid") (vers "0.1.2") (deps (list (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "libcwiid-sys") (req "^0.1.5") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2.15") (default-features #t) (kind 0)))) (hash "1wvmas8xnr5lxzz9z8akpzfskl4nqd37q81pq28vs7nywdan6i2r")))

(define-public crate-cwiid-0.1 (crate (name "cwiid") (vers "0.1.3") (deps (list (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "libcwiid-sys") (req "^0.1.6") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2.15") (default-features #t) (kind 0)))) (hash "1dfi8rfpvqn82ni1zzj6j40cm4yb0qqf41i7zj8j46gjvnh38xh8")))

(define-public crate-cwiid-0.1 (crate (name "cwiid") (vers "0.1.4") (deps (list (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "libcwiid-sys") (req "^0.1.7") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2.15") (default-features #t) (kind 0)))) (hash "1wr1xy7gjqcs3vj8i84kw518navszidmkynmhcwk7k60m3wfd3bg")))

(define-public crate-cwiid-0.1 (crate (name "cwiid") (vers "0.1.5") (deps (list (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "libcwiid-sys") (req "^0.1.8") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2.15") (default-features #t) (kind 0)))) (hash "1x42vdyjqhwh23gsqj6slr402d2f8cr8zmr91k1x1kq3jdw8p428")))

(define-public crate-cwiid-0.1 (crate (name "cwiid") (vers "0.1.6") (deps (list (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "libcwiid-sys") (req "^0.1.9") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2.15") (default-features #t) (kind 0)))) (hash "1ppk0rl2k80l5chp2n5d0fwh34phc58gvyra814wk1kqjx3ykfa4")))

(define-public crate-cwiid-0.1 (crate (name "cwiid") (vers "0.1.7") (deps (list (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "libcwiid-sys") (req "^0.1.10") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2.15") (default-features #t) (kind 0)))) (hash "04jvy6vybclsifdp9099bb0an6bz7y7qbaksxl4hl4xah33wr3ml")))

(define-public crate-cwiid-0.1 (crate (name "cwiid") (vers "0.1.8") (deps (list (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "libcwiid-sys") (req "^0.1.11") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2.15") (default-features #t) (kind 0)))) (hash "19l6nsdli4ikhpppmpnfrj1in5k87i7gci67jds84lpk28ghwlxv")))

(define-public crate-cwiid-0.1 (crate (name "cwiid") (vers "0.1.9") (deps (list (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "libcwiid-sys") (req "^0.1.12") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2.15") (default-features #t) (kind 0)))) (hash "0qdnf7lllk2cyjv7xafyd6yzgwf12i8l0sv8vld8c1g2vm00x559")))

(define-public crate-cwiid-0.1 (crate (name "cwiid") (vers "0.1.10") (deps (list (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "libcwiid-sys") (req "^0.1.13") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2.15") (default-features #t) (kind 0)))) (hash "1cic71g7qvxycaxadf1j7wchbnipakp2jsyjc1y2i8lbvv2i7lix")))

(define-public crate-cwiid-0.1 (crate (name "cwiid") (vers "0.1.11") (deps (list (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "libcwiid-sys") (req "^0.1.14") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2.15") (default-features #t) (kind 0)))) (hash "0rcpwhgv4r15746s7jh05hxm41l8v3khxmyp2ak68yyh538ggpy8")))

(define-public crate-cwiid-0.1 (crate (name "cwiid") (vers "0.1.12") (deps (list (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "libcwiid-sys") (req "^0.1.15") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2.15") (default-features #t) (kind 0)))) (hash "17lyisd7989li3ya1c58jqc7hcbqp7kk4vscbp767q26d2rjqrk4")))

(define-public crate-cwiid-0.1 (crate (name "cwiid") (vers "0.1.13") (deps (list (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "libcwiid-sys") (req "^0.1.16") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2.15") (default-features #t) (kind 0)))) (hash "0z8l93d1kzymvqnk0zpxq39gcr44xhpkjwdw31frlsgfg4phq2d3")))

(define-public crate-cwiid-0.1 (crate (name "cwiid") (vers "0.1.14") (deps (list (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "libcwiid-sys") (req "^0.1.17") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2.15") (default-features #t) (kind 0)))) (hash "0vykavmyia2f6hp9r7lvsvg9fv53m6j59iqkwn38gd3k4al58flm")))

(define-public crate-cwiid-0.1 (crate (name "cwiid") (vers "0.1.15") (deps (list (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "libcwiid-sys") (req "^0.1.18") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2.15") (default-features #t) (kind 0)))) (hash "1akr3xm3y24ca9xkkh9p7bbmrkhmf0plywxaz73xnq6x2i9lqgba")))

