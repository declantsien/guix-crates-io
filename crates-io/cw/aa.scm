(define-module (crates-io cw aa) #:use-module (crates-io))

(define-public crate-cwaa-rs-0.1 (crate (name "cwaa-rs") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "~2.33.3") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.4") (default-features #t) (kind 0)))) (hash "05ypi42rhgj9qbq03zhkryb8j22ng9dkijdwd2zf2imijbar55z8")))

