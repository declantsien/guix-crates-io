(define-module (crates-io cw #{32}#) #:use-module (crates-io))

(define-public crate-cw32f030-hal-0.1 (crate (name "cw32f030-hal") (vers "0.1.0") (deps (list (crate-dep (name "cortex-m") (req "^0.7.7") (default-features #t) (kind 0)) (crate-dep (name "critical-section") (req "^1.1.2") (default-features #t) (kind 0)) (crate-dep (name "svd2rust") (req "^0.30.2") (default-features #t) (kind 2)) (crate-dep (name "vcell") (req "^0.1.3") (default-features #t) (kind 0)))) (hash "07n7zqx9gnci1rdyi0y0w12fc3dcrx9q11r25f8pw2zryp26c063")))

(define-public crate-cw32f030-hal-0.1 (crate (name "cw32f030-hal") (vers "0.1.1") (deps (list (crate-dep (name "cortex-m") (req "^0.7.7") (default-features #t) (kind 0)) (crate-dep (name "critical-section") (req "^1.1.2") (default-features #t) (kind 0)) (crate-dep (name "svd2rust") (req "^0.30.2") (default-features #t) (kind 2)) (crate-dep (name "vcell") (req "^0.1.3") (default-features #t) (kind 0)))) (hash "0pmhxq0j4kmsr6gf6ba9b5ns14si3rfpd5wl1xb85smm4jvh93qn")))

