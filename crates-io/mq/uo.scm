(define-module (crates-io mq uo) #:use-module (crates-io))

(define-public crate-mquote-0.1 (crate (name "mquote") (vers "0.1.0") (deps (list (crate-dep (name "mquote-impl") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "proc-macro-hack") (req "^0.5") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^0.4") (default-features #t) (kind 2)) (crate-dep (name "quote") (req "^0.6") (default-features #t) (kind 0)) (crate-dep (name "trybuild") (req "^1.0") (default-features #t) (kind 2)))) (hash "16l20v7731h9qsv4jgkh4ay45lxbgmr1fyph7mivlvwkaiyf8ckh")))

(define-public crate-mquote-impl-0.1 (crate (name "mquote-impl") (vers "0.1.0") (deps (list (crate-dep (name "proc-macro-hack") (req "^0.5") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^0.6") (default-features #t) (kind 0)))) (hash "1lb4yqf7ng4sz6sij6zdh5apmqrpi8say2y556knz11vw9cbwzmw")))

