(define-module (crates-io mq cl) #:use-module (crates-io))

(define-public crate-mqcloud-0.1 (crate (name "mqcloud") (vers "0.1.0") (hash "0ang9ngqqfbhqgizs1ybxlqwxq331bdpffw1wqg8nc1lgwmh4yj8")))

(define-public crate-mqcloud-client-0.1 (crate (name "mqcloud-client") (vers "0.1.0") (hash "0ld0wl4byzm5zl9lr9wa2f875zp1zd06j0mzk0c1w2zkmlap2jsr")))

