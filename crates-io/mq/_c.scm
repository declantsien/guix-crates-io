(define-module (crates-io mq _c) #:use-module (crates-io))

(define-public crate-mq_cli-3773 (crate (name "mq_cli") (vers "3773.0.0") (deps (list (crate-dep (name "clap") (req "^2.34") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "nix") (req "^0.23") (default-features #t) (kind 0)) (crate-dep (name "posix_mq") (req "^3771.0.0") (default-features #t) (kind 0)))) (hash "0z5bwgrw10pl4rv4yy4xnniblxfbvclzck3mbhccsssvrxsj0i9q")))

