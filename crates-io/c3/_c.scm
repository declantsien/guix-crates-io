(define-module (crates-io c3 _c) #:use-module (crates-io))

(define-public crate-c3_clang_extensions-0.3 (crate (name "c3_clang_extensions") (vers "0.3.1") (deps (list (crate-dep (name "clang-sys") (req "^0.18") (features (quote ("static" "clang_4_0"))) (default-features #t) (kind 0)) (crate-dep (name "gcc") (req "= 0.3.51") (default-features #t) (kind 1)))) (hash "0hmv5y6azgsazz3bf3whp12x0pssiin11mvxlgq1afskbl225k95") (yanked #t)))

(define-public crate-c3_clang_extensions-0.3 (crate (name "c3_clang_extensions") (vers "0.3.2") (deps (list (crate-dep (name "clang-sys") (req "^0.18") (features (quote ("static" "clang_4_0"))) (default-features #t) (kind 0)) (crate-dep (name "gcc") (req "= 0.3.51") (default-features #t) (kind 1)))) (hash "141fv9sabsbwwpjlbnfsln3kamdrc6i748g8x36cy51fqv8wgbwf") (yanked #t)))

(define-public crate-c3_clang_extensions-0.3 (crate (name "c3_clang_extensions") (vers "0.3.3") (deps (list (crate-dep (name "clang-sys") (req "^0.19") (features (quote ("static" "clang_4_0"))) (default-features #t) (kind 0)) (crate-dep (name "gcc") (req "= 0.3.51") (default-features #t) (kind 1)))) (hash "1yzy2k92zl0zvkc7gl9llk39y6fm1q638qdv5g5q47wjpnk6xfy8") (yanked #t)))

(define-public crate-c3_clang_extensions-0.3 (crate (name "c3_clang_extensions") (vers "0.3.4") (deps (list (crate-dep (name "clang-sys") (req "^0.19") (features (quote ("static" "clang_4_0"))) (default-features #t) (kind 0)) (crate-dep (name "gcc") (req "= 0.3.51") (default-features #t) (kind 1)))) (hash "0ikcr2sq370p9rz35d2mjwp6g3v0v5hhif4y8msrshp4l7fci7m9") (yanked #t)))

(define-public crate-c3_clang_extensions-0.3 (crate (name "c3_clang_extensions") (vers "0.3.5") (deps (list (crate-dep (name "clang-sys") (req "^0.19") (features (quote ("static" "clang_4_0"))) (default-features #t) (kind 0)) (crate-dep (name "gcc") (req "= 0.3.51") (default-features #t) (kind 1)))) (hash "0m2qd7ng8xw3c6d4pwi617h1wi0ggb2smm71w3szpvvqshp27fa0") (yanked #t)))

(define-public crate-c3_clang_extensions-0.3 (crate (name "c3_clang_extensions") (vers "0.3.6") (deps (list (crate-dep (name "clang-sys") (req "^0.20") (features (quote ("static" "clang_4_0"))) (default-features #t) (kind 0)) (crate-dep (name "gcc") (req "= 0.3.51") (default-features #t) (kind 1)))) (hash "15llb9r4mmrm297zivhnv0i1xqmb3zkw32d32wwmvqpsbisj8hn8") (yanked #t)))

(define-public crate-c3_clang_extensions-0.3 (crate (name "c3_clang_extensions") (vers "0.3.7") (deps (list (crate-dep (name "cc") (req "^1.0.3") (default-features #t) (kind 1)) (crate-dep (name "clang-sys") (req "^0.20") (features (quote ("static" "clang_4_0"))) (default-features #t) (kind 0)) (crate-dep (name "dunce") (req "^0.1.0") (default-features #t) (kind 1)))) (hash "0nn5vl7a0hs6inqq0q6yi1pm6wx16190rzqq2fq9h9kl8nqycs6m")))

(define-public crate-c3_clang_extensions-0.3 (crate (name "c3_clang_extensions") (vers "0.3.9") (deps (list (crate-dep (name "cc") (req "^1.0.3") (default-features #t) (kind 1)) (crate-dep (name "clang-sys") (req "^0.29") (features (quote ("static" "clang_4_0"))) (default-features #t) (kind 0)) (crate-dep (name "dunce") (req "^0.1.0") (default-features #t) (kind 1)))) (hash "1p6r07kwdi8jmnw5rcrg234pcd4p76hb3ycg1h94rlxqicjw5gnw") (links "c3_clang_extensions")))

