(define-module (crates-io c3 #{14}#) #:use-module (crates-io))

(define-public crate-c314-utils-0.1 (crate (name "c314-utils") (vers "0.1.0") (hash "1fr94j6dgaqx3fcp4n3m46i18spa0ki5x7q0djnc84ni9z702flm") (yanked #t)))

(define-public crate-c314-utils-0.1 (crate (name "c314-utils") (vers "0.1.1") (hash "02smlwpzg05rbs56z2lbbmzvdw1z4wn0j0gd3qqlbir3j8cdkhks") (yanked #t)))

(define-public crate-c314-utils-0.1 (crate (name "c314-utils") (vers "0.1.2") (hash "02lv3k17svdm5bj6xagkdalqb2lk9j0sn5jz6ssrcxpnblyxd7p2") (yanked #t)))

(define-public crate-c314-utils-0.1 (crate (name "c314-utils") (vers "0.1.3") (hash "1z9fb2w3s2flgcf467yvdah7kkwa1m4187rpj27mvmd9q87fshgj") (yanked #t)))

(define-public crate-c314-utils-0.1 (crate (name "c314-utils") (vers "0.1.4") (hash "17hai7h5hns6f32ad2xyxh6yafpmcn6qrwg2597fb1vxkq3a4cwq") (yanked #t)))

(define-public crate-c314-utils-0.2 (crate (name "c314-utils") (vers "0.2.0") (hash "1j7g3d0r1j561iqqbm733dzqrx87hj7apqc13qw0cl3zkls04wp5") (yanked #t)))

