(define-module (crates-io c3 di) #:use-module (crates-io))

(define-public crate-c3dio-0.1 (crate (name "c3dio") (vers "0.1.0") (deps (list (crate-dep (name "nalgebra") (req "^0.32") (default-features #t) (kind 0)))) (hash "0farfr7blmljhr5jhcjp03q09p3xvrg3v8mbp1852i90kn5xkrpp") (yanked #t)))

(define-public crate-c3dio-0.2 (crate (name "c3dio") (vers "0.2.0") (deps (list (crate-dep (name "nalgebra") (req "^0.32") (default-features #t) (kind 0)))) (hash "0ya08mndlwig8ly9g8bsj3za4rdsfnlg2c3wc6g9hs4701ll5hgl")))

(define-public crate-c3dio-0.3 (crate (name "c3dio") (vers "0.3.0") (deps (list (crate-dep (name "nalgebra") (req "^0.32") (default-features #t) (kind 0)))) (hash "1in5hj1v6nz39lcwpr7jqjdkvwzn8wnhhbczzsblbp320jr580zw")))

(define-public crate-c3dio-0.4 (crate (name "c3dio") (vers "0.4.0") (deps (list (crate-dep (name "grid") (req "^0.10") (default-features #t) (kind 0)))) (hash "1q0rp4fb03m7h8c19n91qx65fdvcjivmv03yyrpj9hdv4n28pxbz")))

(define-public crate-c3dio-0.5 (crate (name "c3dio") (vers "0.5.0") (deps (list (crate-dep (name "grid") (req "^0.10") (default-features #t) (kind 0)))) (hash "04xs3q54gdvc61j016s9742hcagkllvhf97jxjrw0r8jqmp4vrnq")))

(define-public crate-c3dio-0.6 (crate (name "c3dio") (vers "0.6.0") (deps (list (crate-dep (name "grid") (req "^0.10") (default-features #t) (kind 0)) (crate-dep (name "test-files") (req "^0.1.2") (default-features #t) (kind 2)))) (hash "0bh4kl7n94k4fcjfd1r2kx3gwg0385iy0ci2f697wpvsvfavc81f")))

(define-public crate-c3dio-0.6 (crate (name "c3dio") (vers "0.6.1") (deps (list (crate-dep (name "grid") (req "^0.10") (default-features #t) (kind 0)) (crate-dep (name "test-files") (req "^0.1.2") (default-features #t) (kind 2)))) (hash "1ikyl86g92a3rvr1kvpc19jhh9805g5fli0xi2v9chm8c8bvy4q0")))

(define-public crate-c3dio-0.7 (crate (name "c3dio") (vers "0.7.0") (deps (list (crate-dep (name "grid") (req "^0.10") (default-features #t) (kind 0)) (crate-dep (name "test-files") (req "^0.1.2") (default-features #t) (kind 2)))) (hash "09chadryni8vsmvs84y7dj96j194r8in6cg2g2bbkrbzp32mhaqy")))

(define-public crate-c3dio-0.8 (crate (name "c3dio") (vers "0.8.0") (deps (list (crate-dep (name "grid") (req "^0.10") (default-features #t) (kind 0)) (crate-dep (name "test-files") (req "^0.1.2") (default-features #t) (kind 2)))) (hash "1alch2qmmpzpci30jvsqkwx93nvb8jvwvwbs60ilnh9rd7vyvnw3")))

