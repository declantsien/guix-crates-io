(define-module (crates-io c3 d-) #:use-module (crates-io))

(define-public crate-c3d-rs-0.1 (crate (name "c3d-rs") (vers "0.1.0") (deps (list (crate-dep (name "anyhow") (req "^1.0.31") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.19") (default-features #t) (kind 0)))) (hash "0p9jfvba2hp37ki9wav16vzhdwgms5j89raa91873q7wzshy5ysn")))

(define-public crate-c3d-rs-0.1 (crate (name "c3d-rs") (vers "0.1.1") (deps (list (crate-dep (name "anyhow") (req "^1.0.31") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.19") (default-features #t) (kind 0)))) (hash "0fjk7klqrnnlzcbpvg7fvpq6pvcwjw3hcn69kkp23h7dplxddx4d")))

(define-public crate-c3d-rs-0.1 (crate (name "c3d-rs") (vers "0.1.2") (deps (list (crate-dep (name "anyhow") (req "^1.0.31") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.19") (default-features #t) (kind 0)))) (hash "04xhdw5m5w4vqlbfpg0f2vczpkkc2qwccv4hbfi78x3h5mi9rgzi")))

(define-public crate-c3d-rs-0.1 (crate (name "c3d-rs") (vers "0.1.3") (deps (list (crate-dep (name "anyhow") (req "^1.0.31") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.19") (default-features #t) (kind 0)))) (hash "13d3kh2hhyf6s6z80jdfpcm57xsppwq8g1ybfsrh5y2f6167bmsa")))

(define-public crate-c3d-rs-0.1 (crate (name "c3d-rs") (vers "0.1.4") (deps (list (crate-dep (name "anyhow") (req "^1.0.31") (default-features #t) (kind 0)) (crate-dep (name "femme") (req "^2.1.0") (default-features #t) (kind 2)) (crate-dep (name "log") (req "^0.4.8") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.19") (default-features #t) (kind 0)))) (hash "0j3yml703mh4rh8kq21xyvs1lqvqrcnswd222xh9xxwyxbi82jm5")))

(define-public crate-c3d-rs-0.1 (crate (name "c3d-rs") (vers "0.1.5") (deps (list (crate-dep (name "anyhow") (req "^1.0.31") (default-features #t) (kind 0)) (crate-dep (name "femme") (req "^2.1.0") (default-features #t) (kind 2)) (crate-dep (name "log") (req "^0.4.8") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.19") (default-features #t) (kind 0)))) (hash "1r0kfy1rh6c56yf029ngwvwcl7x8fxcc6f0wzz1v8rdx0zaqn33l")))

(define-public crate-c3d-rs-0.1 (crate (name "c3d-rs") (vers "0.1.6") (deps (list (crate-dep (name "anyhow") (req "^1.0.31") (default-features #t) (kind 0)) (crate-dep (name "femme") (req "^2.1.0") (default-features #t) (kind 2)) (crate-dep (name "log") (req "^0.4.8") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.19") (default-features #t) (kind 0)))) (hash "0y8bp0sf22n2nn76yp7xqvmq35kq3lkf934hzfncakxss006zbl2")))

(define-public crate-c3d-rs-0.1 (crate (name "c3d-rs") (vers "0.1.7") (deps (list (crate-dep (name "anyhow") (req "^1.0.31") (default-features #t) (kind 0)) (crate-dep (name "femme") (req "^2.1.0") (default-features #t) (kind 2)) (crate-dep (name "log") (req "^0.4.8") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.19") (default-features #t) (kind 0)))) (hash "1a3z6s8y0ym973jys449jcywmkjrh5wpci3jn6pjjcz6zdd6mliy")))

