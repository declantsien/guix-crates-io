(define-module (crates-io c3 -l) #:use-module (crates-io))

(define-public crate-c3-lang-linearization-0.0.1 (crate (name "c3-lang-linearization") (vers "0.0.1") (deps (list (crate-dep (name "quote") (req "^1") (default-features #t) (kind 0)))) (hash "04fg2hjrqj91gdqn5iq415iy3iwkwfsr558asq8y1rb5r4cn2kkq")))

(define-public crate-c3-lang-linearization-0.0.2 (crate (name "c3-lang-linearization") (vers "0.0.2") (deps (list (crate-dep (name "quote") (req "^1") (default-features #t) (kind 0)))) (hash "1a9vfddjnwjr3npgzbi45krik7zdmc3p1zby3ycnnw2j5xdfiy5a")))

(define-public crate-c3-lang-linearization-0.0.3 (crate (name "c3-lang-linearization") (vers "0.0.3") (deps (list (crate-dep (name "quote") (req "^1") (default-features #t) (kind 0)))) (hash "0k5dd909z1km54qvm7i3hfq3wbly53jlga3ksdvnw5lrxpxz605w")))

(define-public crate-c3-lang-linearization-0.0.4 (crate (name "c3-lang-linearization") (vers "0.0.4") (deps (list (crate-dep (name "quote") (req "^1") (default-features #t) (kind 0)))) (hash "0lvhgbjyr72aa1ngd7i8sfdzpivk5djs5587jcw720ra2fvxvn7d")))

(define-public crate-c3-lang-macro-0.0.1 (crate (name "c3-lang-macro") (vers "0.0.1") (deps (list (crate-dep (name "c3-lang-parser") (req "^0.0.1") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "1hr4srxkvvcyxyh7lfwyqsxn1kd7mdwikmvv211b7sz220k8h4l9")))

(define-public crate-c3-lang-macro-0.0.2 (crate (name "c3-lang-macro") (vers "0.0.2") (deps (list (crate-dep (name "c3-lang-parser") (req "^0.0.2") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "1bwdkdf1xwy2k599x652646w7bbxnapifgzqr3qixrpxpqxhky3w")))

(define-public crate-c3-lang-macro-0.0.3 (crate (name "c3-lang-macro") (vers "0.0.3") (deps (list (crate-dep (name "c3-lang-parser") (req "^0.0.3") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "1mhn8qgd1lb6plmxpyyilwwzvc315c1rp2ni8gy6v391z26aiv5i")))

(define-public crate-c3-lang-macro-0.0.4 (crate (name "c3-lang-macro") (vers "0.0.4") (deps (list (crate-dep (name "c3-lang-parser") (req "^0.0.4") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "0fg5b31np704rdm1bmhg158zn1xrdrq99w0f8lld6dxg49i8g0b7")))

(define-public crate-c3-lang-parser-0.0.1 (crate (name "c3-lang-parser") (vers "0.0.1") (deps (list (crate-dep (name "c3-lang-linearization") (req "^0.0.1") (default-features #t) (kind 0)) (crate-dep (name "pretty_assertions") (req "^1.2") (default-features #t) (kind 2)) (crate-dep (name "proc-macro2") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1") (features (quote ("full" "extra-traits"))) (default-features #t) (kind 0)))) (hash "18y1wmn8dy6mz6qc5q0lzr3dak4fc2abmv0wq4ffj0f32nv0wz2m")))

(define-public crate-c3-lang-parser-0.0.2 (crate (name "c3-lang-parser") (vers "0.0.2") (deps (list (crate-dep (name "c3-lang-linearization") (req "^0.0.2") (default-features #t) (kind 0)) (crate-dep (name "pretty_assertions") (req "^1.2") (default-features #t) (kind 2)) (crate-dep (name "proc-macro2") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1") (features (quote ("full" "extra-traits"))) (default-features #t) (kind 0)))) (hash "081kbaczp4cazbw4qa0chz4w5c957ykzviazgq00x46bx0z02ls4")))

(define-public crate-c3-lang-parser-0.0.3 (crate (name "c3-lang-parser") (vers "0.0.3") (deps (list (crate-dep (name "c3-lang-linearization") (req "^0.0.3") (default-features #t) (kind 0)) (crate-dep (name "pretty_assertions") (req "^1.2") (default-features #t) (kind 2)) (crate-dep (name "proc-macro2") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1") (features (quote ("full" "extra-traits"))) (default-features #t) (kind 0)))) (hash "0whbvhz24q9qcc1nhlvcm9cj4xwvympmrgx5klclqwi900bvhlgx")))

(define-public crate-c3-lang-parser-0.0.4 (crate (name "c3-lang-parser") (vers "0.0.4") (deps (list (crate-dep (name "c3-lang-linearization") (req "^0.0.4") (default-features #t) (kind 0)) (crate-dep (name "pretty_assertions") (req "^1.2") (default-features #t) (kind 2)) (crate-dep (name "proc-macro2") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1") (features (quote ("full" "extra-traits"))) (default-features #t) (kind 0)))) (hash "1s094s1r5jvqyksx94mwl4mzrsgn8dp4ivjysfx3marbnj3a20ki")))

(define-public crate-c3-linearization-0.1 (crate (name "c3-linearization") (vers "0.1.0") (hash "1289zx6jwq98mm2v4kaqq1f0qawkkh0jlfqd4rz84p50nfvl18an") (features (quote (("default"))))))

