(define-module (crates-io xy zi) #:use-module (crates-io))

(define-public crate-xyzio-0.1 (crate (name "xyzio") (vers "0.1.0") (hash "19hw4829qrlhcwd6fz97z7zbgm8vjjdykhcb4595psi4yc7bgka4")))

(define-public crate-xyzio-0.2 (crate (name "xyzio") (vers "0.2.0") (hash "0j6ddffx7bcgn3bnw1mw5zydg7q0b9c05cwm6q40j9xh19gqqx5q")))

(define-public crate-xyzio-0.3 (crate (name "xyzio") (vers "0.3.0") (hash "02rdi7ff9i81xmzvfikri05prdd8hyxw6nc405v37kr9ibm2qjqy")))

(define-public crate-xyzio-0.3 (crate (name "xyzio") (vers "0.3.1") (hash "1z2aiggxc2sq7mlkjfz3sjgw5kk53m6918h0hbcg49k4a24wq4db") (features (quote (("double_precision"))))))

