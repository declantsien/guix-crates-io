(define-module (crates-io xy lo) #:use-module (crates-io))

(define-public crate-xylo-0.1 (crate (name "xylo") (vers "0.1.0") (hash "0jhvskn053m4a4krjyqpikkx08zzp0ymhfafnc4ryvb6444xlhg0")))

(define-public crate-xylo-0.1 (crate (name "xylo") (vers "0.1.1") (deps (list (crate-dep (name "anyhow") (req "^1.0.75") (default-features #t) (kind 0)) (crate-dep (name "cpal") (req "^0.15.0") (default-features #t) (kind 0)) (crate-dep (name "crossbeam") (req "^0.8.2") (features (quote ("crossbeam-channel"))) (default-features #t) (kind 0)))) (hash "0d585n5ggsd75x4zwsk65lc2zh838nwpjg2gkqfxirk0kpygjjhx")))

(define-public crate-xylosip-0.1 (crate (name "xylosip") (vers "0.1.0") (deps (list (crate-dep (name "atoi") (req "^0.3.2") (kind 0)) (crate-dep (name "nom") (req "^5") (features (quote ("std"))) (kind 0)))) (hash "1fyxrbm304qxmaqk9xd4b30cdzvwzchxyryxc023rpcvldfjpd6c")))

