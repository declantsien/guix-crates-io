(define-module (crates-io xy io) #:use-module (crates-io))

(define-public crate-xyio-0.1 (crate (name "xyio") (vers "0.1.0") (deps (list (crate-dep (name "bitflags") (req "*") (default-features #t) (kind 0)) (crate-dep (name "chrono") (req "*") (default-features #t) (kind 0)) (crate-dep (name "getopts") (req "*") (default-features #t) (kind 0)) (crate-dep (name "http-muncher") (req "*") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "*") (default-features #t) (kind 0)) (crate-dep (name "log") (req "*") (default-features #t) (kind 0)) (crate-dep (name "nix") (req "*") (default-features #t) (kind 0)) (crate-dep (name "time") (req "*") (default-features #t) (kind 0)))) (hash "1nbrxpzx91mq1v5msh794hkvss031395k8npmy5hilwfd9n8wy91")))

