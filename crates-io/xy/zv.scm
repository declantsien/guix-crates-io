(define-module (crates-io xy zv) #:use-module (crates-io))

(define-public crate-xyzvec-0.1 (crate (name "xyzvec") (vers "0.1.0") (deps (list (crate-dep (name "fmt") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "0s4w9j4l2as7180438mi6vdm5irw6nhpb1m29pvhzsay3dm5apx8")))

(define-public crate-xyzvec-0.1 (crate (name "xyzvec") (vers "0.1.1") (deps (list (crate-dep (name "fmt") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "0jw1cmzljgpf4kb4miq3dggw3vhk6h3rvbfrvs2597k7s8k52h75")))

(define-public crate-xyzvec-0.1 (crate (name "xyzvec") (vers "0.1.3") (deps (list (crate-dep (name "approx") (req "^0.5.1") (default-features #t) (kind 2)) (crate-dep (name "fixed") (req "^1.25.1") (default-features #t) (kind 2)) (crate-dep (name "fmt") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "0ybcn5ajrf624vlb92sy6z9rcw2k6z45zjsjdiq9d0n4iy9p6zak")))

(define-public crate-xyzvec-0.1 (crate (name "xyzvec") (vers "0.1.4") (deps (list (crate-dep (name "approx") (req "^0.5.1") (default-features #t) (kind 2)) (crate-dep (name "fixed") (req "^1.25.1") (default-features #t) (kind 2)) (crate-dep (name "fmt") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "1syf545zvx6qfbnl3q6b7hnfcfab2ry95mb3k11bgbnq9nqsmdz7")))

(define-public crate-xyzvec-0.1 (crate (name "xyzvec") (vers "0.1.5") (deps (list (crate-dep (name "approx") (req "^0.5.1") (default-features #t) (kind 2)) (crate-dep (name "fixed") (req "^1.25.1") (default-features #t) (kind 2)) (crate-dep (name "fmt") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "0fyapwhdlicpbwywrdnsm4kxny43056zslnx9gb6k79wdf6gvz55")))

(define-public crate-xyzvec-0.1 (crate (name "xyzvec") (vers "0.1.6") (deps (list (crate-dep (name "approx") (req "^0.5.1") (default-features #t) (kind 2)) (crate-dep (name "cordic") (req "^0.1.5") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "fixed") (req "^1.25.1") (default-features #t) (kind 0)) (crate-dep (name "fmt") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "10cpl1p7glpglkkwi932n21sh68nrjr6iiinkh6qq6gfg8xrhr73") (v 2) (features2 (quote (("cordic" "dep:cordic"))))))

(define-public crate-xyzvec-0.1 (crate (name "xyzvec") (vers "0.1.7") (deps (list (crate-dep (name "approx") (req "^0.5.1") (default-features #t) (kind 2)) (crate-dep (name "cordic") (req "^0.1.5") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "fixed") (req "^1.25.1") (default-features #t) (kind 0)) (crate-dep (name "fmt") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "0i7i81x5dj3righygq9374nwb0w5qqicfdmh0r8zmid0a321idbb") (v 2) (features2 (quote (("cordic" "dep:cordic"))))))

(define-public crate-xyzvec-0.1 (crate (name "xyzvec") (vers "0.1.8") (deps (list (crate-dep (name "approx") (req "^0.5.1") (default-features #t) (kind 2)) (crate-dep (name "cordic") (req "^0.1.5") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "fixed") (req "^1.25.1") (default-features #t) (kind 0)) (crate-dep (name "fmt") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "0028zx1z0kfnp1cjx844p39v26knb5fblfyxwhbi7jmfsbcwkiqy") (v 2) (features2 (quote (("cordic" "dep:cordic"))))))

(define-public crate-xyzvec-0.1 (crate (name "xyzvec") (vers "0.1.9") (deps (list (crate-dep (name "approx") (req "^0.5.1") (default-features #t) (kind 2)) (crate-dep (name "cordic") (req "^0.1.5") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "fixed") (req "^1.25.1") (default-features #t) (kind 0)) (crate-dep (name "fmt") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "0if39i8qmpqxc33636kzcf1ypcgkaj2qv4whpjcfhmf3l4b3wywm") (v 2) (features2 (quote (("cordic" "dep:cordic"))))))

(define-public crate-xyzvec-0.1 (crate (name "xyzvec") (vers "0.1.10") (deps (list (crate-dep (name "approx") (req "^0.5.1") (default-features #t) (kind 2)) (crate-dep (name "cordic") (req "^0.1.5") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "fixed") (req "^1.25.1") (default-features #t) (kind 0)) (crate-dep (name "fmt") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "19zzjs2yygq2cfn425xrgw77pwhyjx8z99db3rv24a3v6adlygpz") (v 2) (features2 (quote (("cordic" "dep:cordic"))))))

(define-public crate-xyzvec-0.1 (crate (name "xyzvec") (vers "0.1.11") (deps (list (crate-dep (name "approx") (req "^0.5.1") (default-features #t) (kind 2)) (crate-dep (name "cordic") (req "^0.1.5") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "fixed") (req "^1.25.1") (default-features #t) (kind 0)) (crate-dep (name "fmt") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "0h90fyhkfh41cr0fifxh21gzbbfwfaqmr8yrvy1f36wfacvmsqg5") (v 2) (features2 (quote (("cordic" "dep:cordic"))))))

