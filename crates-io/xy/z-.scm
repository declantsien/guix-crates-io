(define-module (crates-io xy z-) #:use-module (crates-io))

(define-public crate-xyz-art-0.0.0 (crate (name "xyz-art") (vers "0.0.0") (hash "17ckrn9in7r8gdxy435liwv62c35m6hidym3ljaw6cx4gbgsbyg3") (yanked #t)))

(define-public crate-xyz-parse-0.1 (crate (name "xyz-parse") (vers "0.1.2") (deps (list (crate-dep (name "pyo3") (req "^0.21") (features (quote ("rust_decimal" "extension-module" "abi3-py37"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "rust_decimal") (req "^1.35") (kind 0)))) (hash "0k1w43zc8vpngkrlv75zw1jrv2ilyb3p8qi14m2f0j7agh3z9rk1") (features (quote (("python" "pyo3") ("default"))))))

(define-public crate-xyz-parse-0.1 (crate (name "xyz-parse") (vers "0.1.3") (deps (list (crate-dep (name "pyo3") (req "^0.21") (features (quote ("rust_decimal"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "rust_decimal") (req "^1.35") (kind 0)))) (hash "0fxih7g9jdqpsg4ls1d6jhys4d2c2izhqc5cwmaka6vplnb0sbhw") (features (quote (("pyo3-lib" "pyo3" "pyo3/extension-module" "pyo3/abi3-py37") ("default"))))))

