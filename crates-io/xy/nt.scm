(define-module (crates-io xy nt) #:use-module (crates-io))

(define-public crate-xynth-0.1 (crate (name "xynth") (vers "0.1.0") (hash "0qvnxbjhv8kqndyky9rgdigbm745p3df2kawayk0flpa2nr86agn")))

(define-public crate-xynth-0.1 (crate (name "xynth") (vers "0.1.1") (hash "1c1qqw4svdzjmca8ll89ixhjycq7l49lb98m1iwv63z483c6z31h")))

(define-public crate-xynth-0.1 (crate (name "xynth") (vers "0.1.2") (deps (list (crate-dep (name "hex") (req "^0.4.3") (default-features #t) (kind 0)))) (hash "1319k67j3qrksrkjbj3xkv3hcy6y6qss0hcs56zm8rwk70y5s9dw")))

(define-public crate-xynth-0.1 (crate (name "xynth") (vers "0.1.3") (deps (list (crate-dep (name "hex") (req "^0.4.3") (default-features #t) (kind 0)))) (hash "1dlrkhkq3p2sqrs6g9v81pcvamjyid95dyhw819yk7xn17di74f6")))

(define-public crate-xynth-0.1 (crate (name "xynth") (vers "0.1.4") (deps (list (crate-dep (name "hex") (req "^0.4.3") (default-features #t) (kind 0)))) (hash "0ic898dcrgf8w2dzl22d6lgkq2z8s93mqrnsaavsy5l8mpm7wg1k")))

(define-public crate-xynth-0.1 (crate (name "xynth") (vers "0.1.5") (deps (list (crate-dep (name "hex") (req "^0.4.3") (default-features #t) (kind 0)))) (hash "0x62sfwbd395xs2yvi9rd405cwyv373lqzi65aswfhp38hlh7pws")))

(define-public crate-xynth-0.1 (crate (name "xynth") (vers "0.1.6") (deps (list (crate-dep (name "hex") (req "^0.4.3") (default-features #t) (kind 0)))) (hash "1fnf2fry8mzf6ly0cssnwrpdqzdqk9m0wvlfvhqp1mjxj1ylfx4r")))

(define-public crate-xynth-0.1 (crate (name "xynth") (vers "0.1.7") (hash "1a2xk4dmq7hyx5lfpsj1zxcycykh0vbmjxdq087daj2ay7h3hk9q")))

(define-public crate-xynthe-ambassadors-0.0.0 (crate (name "xynthe-ambassadors") (vers "0.0.0") (hash "0hr217j7ixl29qm6qg3h6gmyb2fk1y39cm0ccfwbnacf4sgh7yci")))

(define-public crate-xynthe-data-0.0.0 (crate (name "xynthe-data") (vers "0.0.0") (hash "0r2645z2s9ifxs1rcqf4x2fqdd35299s83xjlsa1km4222y84m3i")))

(define-public crate-xynthe-subgraph-0.0.0 (crate (name "xynthe-subgraph") (vers "0.0.0") (hash "0h67yc95bkmmy4ayh8g8kqd91k5r6bmnnz1f0ad9mm53mvn706gz")))

(define-public crate-xynthe-website-0.0.0 (crate (name "xynthe-website") (vers "0.0.0") (hash "0fqdz3yqw4f8c17qns8bw5zn9xwjby4gx8czaghmcp5hmygv7ad3")))

