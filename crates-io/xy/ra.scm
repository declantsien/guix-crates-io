(define-module (crates-io xy ra) #:use-module (crates-io))

(define-public crate-xyraith-0.1 (crate (name "xyraith") (vers "0.1.0") (deps (list (crate-dep (name "ariadne") (req "^0.2.0") (features (quote ("concolor"))) (default-features #t) (kind 0)) (crate-dep (name "chumsky") (req "^1.0.0-alpha.4") (features (quote ("label"))) (default-features #t) (kind 0)))) (hash "0sbgdcya9yhxrc70ygsb6ixbkln3lylaafr531crc2c4dngxliwz")))

