(define-module (crates-io xy zp) #:use-module (crates-io))

(define-public crate-xyzpub-0.1 (crate (name "xyzpub") (vers "0.1.0") (deps (list (crate-dep (name "bitcoin") (req "^0.27.0") (default-features #t) (kind 0)))) (hash "09vx704ag4j4pk56100cgkgpd4v4bqqr1g0fll85lr2gga017rhr")))

(define-public crate-xyzpub-0.2 (crate (name "xyzpub") (vers "0.2.0") (deps (list (crate-dep (name "bitcoin") (req "^0.27.0") (default-features #t) (kind 0)))) (hash "1dh9gf9nk9iyyd04fll019lsdnmli4zrd9a2m0kb3npr6nv2dw55")))

(define-public crate-xyzpub-0.2 (crate (name "xyzpub") (vers "0.2.1") (deps (list (crate-dep (name "bitcoin") (req "^0.27.0") (default-features #t) (kind 0)))) (hash "1ny41pamph76fy3500mdb70sas15vzy7h20r000r0g740k4ak7vm")))

(define-public crate-xyzpub-0.2 (crate (name "xyzpub") (vers "0.2.2") (deps (list (crate-dep (name "bitcoin") (req "^0.29.0") (default-features #t) (kind 0)))) (hash "0h1nr6kf8iwsy58hfgl2vnbniya22vwqnw06c6p12fgvldq9lfv4") (yanked #t)))

(define-public crate-xyzpub-0.3 (crate (name "xyzpub") (vers "0.3.0") (deps (list (crate-dep (name "bitcoin") (req "^0.29.0") (default-features #t) (kind 0)))) (hash "1i380m95w3wba17bbzj5k8wy7gf62zd8kcbki44sspyi8s74m142")))

