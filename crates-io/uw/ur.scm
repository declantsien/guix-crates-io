(define-module (crates-io uw ur) #:use-module (crates-io))

(define-public crate-uwurandom-0.1 (crate (name "uwurandom") (vers "0.1.0") (deps (list (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)) (crate-dep (name "rand_xoshiro") (req "^0.6.0") (default-features #t) (kind 0)) (crate-dep (name "uwurandom-rs") (req "^1.0.0") (default-features #t) (kind 0)))) (hash "0yr8kqxv7al3qwfg88hz1pz0wwbz21iyy4yx1ihmpwmp75sqw1kn")))

(define-public crate-uwurandom-proc-macros-1 (crate (name "uwurandom-proc-macros") (vers "1.0.0") (deps (list (crate-dep (name "convert_case") (req "^0.6.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (features (quote ("parsing"))) (default-features #t) (kind 0)))) (hash "0pipnnksqj8jc99aqdk3kx1x76b1v6vzj64vcpw0r8zdrx3sdzq4")))

(define-public crate-uwurandom-rs-1 (crate (name "uwurandom-rs") (vers "1.0.0") (deps (list (crate-dep (name "rand_core") (req "^0.6.4") (default-features #t) (kind 0)) (crate-dep (name "uwurandom-proc-macros") (req "^1.0.0") (default-features #t) (kind 0)) (crate-dep (name "rand_pcg") (req "^0.3.1") (default-features #t) (kind 2)))) (hash "010a30npm117qqapnvbmrwx1yzwmik7xp73la5r99fx8wc6af7qh")))

(define-public crate-uwurandom-rs-1 (crate (name "uwurandom-rs") (vers "1.1.0") (deps (list (crate-dep (name "rand_core") (req "^0.6.4") (default-features #t) (kind 0)) (crate-dep (name "uwurandom-proc-macros") (req "^1.0.0") (default-features #t) (kind 0)) (crate-dep (name "rand_pcg") (req "^0.3.1") (default-features #t) (kind 2)))) (hash "0jsb210pnc3vckypsbi785r01hkdgqnm45rmx3lh8qjaai6qmn6i")))

