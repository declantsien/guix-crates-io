(define-module (crates-io uw eb) #:use-module (crates-io))

(define-public crate-uwebsockets_rs-0.0.1 (crate (name "uwebsockets_rs") (vers "0.0.1") (deps (list (crate-dep (name "libuwebsockets-sys") (req "^0.0.2") (features (quote ("uws_vendored"))) (default-features #t) (kind 0)))) (hash "04jrrg2ba1c06ii3h97nd5b8p0a4hc2q87i3hxyxw83jxb21s65z") (yanked #t)))

(define-public crate-uwebsockets_rs-0.0.2 (crate (name "uwebsockets_rs") (vers "0.0.2") (deps (list (crate-dep (name "libuwebsockets-sys") (req "^0.0.7") (features (quote ("uws_vendored"))) (default-features #t) (kind 0)))) (hash "1df50mf1g690l4j18rxvfzf7z9wd80zzxi8ynx8nxrw9nd5bj3sk") (yanked #t)))

(define-public crate-uwebsockets_rs-0.0.3 (crate (name "uwebsockets_rs") (vers "0.0.3") (deps (list (crate-dep (name "libuwebsockets-sys") (req "^0.0.7") (features (quote ("uws_vendored"))) (default-features #t) (kind 0)))) (hash "0my1jhqmqhxhs09a943h5mkjdxw53dcwgnz4k4lkvdwy8764i5dd") (features (quote (("native-access"))))))

(define-public crate-uwebsockets_rs-0.0.4 (crate (name "uwebsockets_rs") (vers "0.0.4") (deps (list (crate-dep (name "libuwebsockets-sys") (req "^0.0.7") (features (quote ("uws_vendored"))) (default-features #t) (kind 0)))) (hash "0gsrfnjwnyfyxkshjrbvzavwjklyv6z391sv4kkgazjp62jr1jnh") (features (quote (("native-access"))))))

(define-public crate-uwebsockets_rs-0.0.5 (crate (name "uwebsockets_rs") (vers "0.0.5") (deps (list (crate-dep (name "libuwebsockets-sys") (req "^0.0.7") (features (quote ("uws_vendored"))) (default-features #t) (kind 0)))) (hash "1zfiam6jka2gxvnyc5dwa8j6iwy1xd22wj0jn8wpygbq3v9326sd") (features (quote (("native-access"))))))

(define-public crate-uwebsockets_rs-0.0.6 (crate (name "uwebsockets_rs") (vers "0.0.6") (deps (list (crate-dep (name "libuwebsockets-sys") (req "^0.0.7") (features (quote ("uws_vendored"))) (default-features #t) (kind 0)))) (hash "0bk7brcpysd166lpij6rq4m62p466md1zayr6v2zzm9bdszcxd2j") (features (quote (("native-access"))))))

(define-public crate-uwebsockets_rs-0.0.7 (crate (name "uwebsockets_rs") (vers "0.0.7") (deps (list (crate-dep (name "libuwebsockets-sys") (req "^0.0.7") (features (quote ("uws_vendored"))) (default-features #t) (kind 0)))) (hash "1r9v0mxfxyyw6y6fc8nmpgdkffg0izm8nqjal1a1as7dgg4jxva9") (features (quote (("native-access"))))))

(define-public crate-uwebsockets_rs-0.0.8 (crate (name "uwebsockets_rs") (vers "0.0.8") (deps (list (crate-dep (name "libuwebsockets-sys") (req "^0.0.7") (features (quote ("uws_vendored"))) (default-features #t) (kind 0)))) (hash "0fj4b008fg13fwz34h5hcdh9j6hvd25frrx71ilb8kh0fzq838wb") (features (quote (("native-access"))))))

(define-public crate-uwebsockets_rs-0.0.9 (crate (name "uwebsockets_rs") (vers "0.0.9") (deps (list (crate-dep (name "libuwebsockets-sys") (req "^0.0.9") (features (quote ("uws_vendored"))) (default-features #t) (kind 0)))) (hash "1schi7xpq8nhrvmb8x4hqbkdw7gpxksb6a5yhp0ppd4mqnfdszka") (features (quote (("native-access"))))))

(define-public crate-uwebsockets_rs-0.0.10 (crate (name "uwebsockets_rs") (vers "0.0.10") (deps (list (crate-dep (name "libuwebsockets-sys") (req "^0.0.9") (features (quote ("uws_vendored"))) (default-features #t) (kind 0)))) (hash "1n07am2vfcg3f2yjg14mqd74j32xiclhjhcympilrl6ld315ric6") (features (quote (("native-access"))))))

(define-public crate-uwebsockets_rs-0.0.11 (crate (name "uwebsockets_rs") (vers "0.0.11") (deps (list (crate-dep (name "libuwebsockets-sys") (req "^0.0.9") (features (quote ("uws_vendored"))) (default-features #t) (kind 0)))) (hash "0rk76vs8z8dx630dj1gccwjsxwdwj1cinaskwma0v07rnva46ixl") (features (quote (("native-access"))))))

