(define-module (crates-io uw b_) #:use-module (crates-io))

(define-public crate-uwb_serial-1 (crate (name "uwb_serial") (vers "1.0.0") (deps (list (crate-dep (name "clap") (req "^3.2.16") (default-features #t) (kind 0)) (crate-dep (name "pyo3") (req "^0.16.5") (features (quote ("extension-module"))) (default-features #t) (kind 0)) (crate-dep (name "serialport") (req "^4.2") (default-features #t) (kind 0)))) (hash "012r2kk1vqg6p0zmvm79bkr663l3kc8x1bllasbdbzr86x6pqgii")))

