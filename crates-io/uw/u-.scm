(define-module (crates-io uw u-) #:use-module (crates-io))

(define-public crate-uwu-rs-0.1 (crate (name "uwu-rs") (vers "0.1.0") (deps (list (crate-dep (name "aho-corasick") (req "^1.1") (default-features #t) (kind 0)) (crate-dep (name "fastrand") (req "^2.0") (kind 0)) (crate-dep (name "thiserror") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "tracing") (req "^0.1") (default-features #t) (kind 0)))) (hash "0i6x9r06r011dc6mgcjndq8hxs3clraksfpwdcddhkrj0c7ah4jn")))

(define-public crate-uwu-rs-1 (crate (name "uwu-rs") (vers "1.0.0") (deps (list (crate-dep (name "aho-corasick") (req "^1.1") (default-features #t) (kind 0)) (crate-dep (name "fastrand") (req "^2.0") (kind 0)) (crate-dep (name "thiserror") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "tracing") (req "^0.1") (default-features #t) (kind 0)))) (hash "0908wxik7wf6p5vmrf37w7aq4gn9ywrxsb3z5r5iwq0fajgyr420")))

(define-public crate-uwu-types-1 (crate (name "uwu-types") (vers "1.0.0") (hash "06xpsxhkdc7gnlfzsj6qfw1r0qf45h8f3kwqy8kfljfja03ynnxc")))

