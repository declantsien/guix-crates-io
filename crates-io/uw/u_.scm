(define-module (crates-io uw u_) #:use-module (crates-io))

(define-public crate-uwu_cli-0.1 (crate (name "uwu_cli") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^4.5") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "tracing") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "tracing-subscriber") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "uwu-rs") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "0shds98dq60fiwchh63zs51kf6miy3nq3m0pbf1i9vbp0x9vdc47")))

(define-public crate-uwu_cli-1 (crate (name "uwu_cli") (vers "1.0.0") (deps (list (crate-dep (name "clap") (req "^4.5") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "tracing") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "tracing-subscriber") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "uwu-rs") (req "^1.0.0") (default-features #t) (kind 0)))) (hash "0gfxx5y6x985kizqdcpdg1y78m3xp7zf94jnvnapdf744klkhnd5")))

(define-public crate-uwu_stub-0.0.0 (crate (name "uwu_stub") (vers "0.0.0") (hash "1a4s8xsg8cz1w4cb7pvicg5nynraa8dnxx6a18a2hkhdqa8pig23")))

(define-public crate-uwu_wasm-0.2 (crate (name "uwu_wasm") (vers "0.2.2") (deps (list (crate-dep (name "uwu-rs") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "wasm-bindgen") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "wasm-bindgen-test") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "wee_alloc") (req "^0.4") (default-features #t) (kind 0)))) (hash "1ip57kz4phlnbq81yfc6p4p2q79vx8y9w3z31kvxfmqir8ljksqb")))

(define-public crate-uwu_wasm-1 (crate (name "uwu_wasm") (vers "1.0.0") (deps (list (crate-dep (name "uwu-rs") (req "^1.0.0") (default-features #t) (kind 0)) (crate-dep (name "wasm-bindgen") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "wasm-bindgen-test") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "wee_alloc") (req "^0.4") (default-features #t) (kind 0)))) (hash "1kc8mgd78693xjhfd3kvg27qsjhqhzyjw5hk3gzl3cj41gbw5gfw")))

