(define-module (crates-io uw ik) #:use-module (crates-io))

(define-public crate-uwiki-types-0.1 (crate (name "uwiki-types") (vers "0.1.0") (deps (list (crate-dep (name "serde") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0") (default-features #t) (kind 0)))) (hash "0d68mzgd9b8w6qqb4fxgx0avylb29i49lan75x81sq2bpnj2djyq")))

(define-public crate-uwiki-types-0.2 (crate (name "uwiki-types") (vers "0.2.0") (deps (list (crate-dep (name "serde") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0") (default-features #t) (kind 0)))) (hash "1hv34fq2ynic1073wzxhcfgwhqcg983m853g170crsh1cbd7h8yl")))

(define-public crate-uwiki-types-0.3 (crate (name "uwiki-types") (vers "0.3.0") (deps (list (crate-dep (name "serde") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0") (default-features #t) (kind 0)))) (hash "0fzil1v34xjxw8pnapxardf1d41v61w8mbkn5rnp6acpcr47mg89")))

