(define-module (crates-io uw uc) #:use-module (crates-io))

(define-public crate-uwucodec-0.1 (crate (name "uwucodec") (vers "0.1.0") (deps (list (crate-dep (name "structopt") (req "^0.3.22") (default-features #t) (kind 0)))) (hash "174hvig48rx7yfj63xv8kinmsysaidg9dalcx3ybmx0di12kn9q0")))

(define-public crate-uwucodec-0.1 (crate (name "uwucodec") (vers "0.1.1") (deps (list (crate-dep (name "structopt") (req "^0.3.22") (default-features #t) (kind 0)))) (hash "1h6l3k3hgm4nj1v3qxdv6vn4pq2n5hfyb3vg89rw8qxslp5q7j27")))

