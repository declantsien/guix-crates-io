(define-module (crates-io il da) #:use-module (crates-io))

(define-public crate-ilda-0.0.1 (crate (name "ilda") (vers "0.0.1") (deps (list (crate-dep (name "log") (req "0.3.*") (default-features #t) (kind 0)))) (hash "0z0zv4nr6zh60xwyvskfggwg10k0zmlz9wan16xpyapxm63gj44z")))

(define-public crate-ilda-0.0.2 (crate (name "ilda") (vers "0.0.2") (deps (list (crate-dep (name "image") (req "0.10.*") (default-features #t) (kind 2)) (crate-dep (name "log") (req "0.3.*") (default-features #t) (kind 0)))) (hash "1vdwmbr7babcqa0rh7hqxqdayh6x8mrxlz7fcyc4v6fl7wlpn7is")))

(define-public crate-ilda-0.0.3 (crate (name "ilda") (vers "0.0.3") (deps (list (crate-dep (name "image") (req "0.10.*") (default-features #t) (kind 2)) (crate-dep (name "log") (req "0.3.*") (default-features #t) (kind 0)))) (hash "13bla9i85npx8qmbrm9raic3c82nvxmpfxsjb4fxp9qkdi7xbh97")))

(define-public crate-ilda-0.0.4 (crate (name "ilda") (vers "0.0.4") (deps (list (crate-dep (name "image") (req "0.10.*") (default-features #t) (kind 2)) (crate-dep (name "log") (req "0.3.*") (default-features #t) (kind 0)))) (hash "10fj2abnhlf5w7sbqi9kvl0f8s47gd32nrr1lj52sfcbcamxrclj")))

(define-public crate-ilda-0.0.5 (crate (name "ilda") (vers "0.0.5") (deps (list (crate-dep (name "image") (req "0.10.*") (default-features #t) (kind 2)) (crate-dep (name "log") (req "0.3.*") (default-features #t) (kind 0)))) (hash "1cyc0mnwaj0k7nk3vksqimfya4aclrh6f3am7gnb7094lcj4hkqq")))

(define-public crate-ilda-0.0.6 (crate (name "ilda") (vers "0.0.6") (deps (list (crate-dep (name "image") (req "0.10.*") (default-features #t) (kind 2)) (crate-dep (name "log") (req "0.3.*") (default-features #t) (kind 0)))) (hash "1lvfs12ky6hi98v2d7wbx3dy75s3g8k6dibx51451hs44pyhni1p")))

(define-public crate-ilda-0.2 (crate (name "ilda") (vers "0.2.0") (deps (list (crate-dep (name "image") (req "0.10.*") (default-features #t) (kind 2)) (crate-dep (name "point") (req "^0.3") (default-features #t) (kind 0)))) (hash "1ghwg751zjjz33h7vsj11a0q0gxi88dwdrgnh7xca6hh5q9j9c6l")))

(define-public crate-ilda-idtf-0.1 (crate (name "ilda-idtf") (vers "0.1.0") (deps (list (crate-dep (name "bitflags") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "byteorder") (req "^1") (kind 0)) (crate-dep (name "walkdir") (req "^2") (default-features #t) (kind 2)) (crate-dep (name "zerocopy") (req "^0.3") (default-features #t) (kind 0)))) (hash "1fr4vgzfqx1kjcqrh9921p7lydzcyvdhmv63n1yf0k3azbb2vsrl")))

(define-public crate-ilda-player-0.0.1 (crate (name "ilda-player") (vers "0.0.1") (deps (list (crate-dep (name "argparse") (req "0.2.*") (default-features #t) (kind 0)) (crate-dep (name "ilda") (req "^0.0.6") (default-features #t) (kind 0)) (crate-dep (name "lase") (req "^0.0.1") (default-features #t) (kind 0)))) (hash "0xb00r1q0f0xs6kcygy7l7azdnqyiqhrlz1i9ccsqc1wi22wzzzd")))

(define-public crate-ilda-player-0.0.2 (crate (name "ilda-player") (vers "0.0.2") (deps (list (crate-dep (name "argparse") (req "0.2.*") (default-features #t) (kind 0)) (crate-dep (name "ilda") (req "^0.0.6") (default-features #t) (kind 0)) (crate-dep (name "lase") (req "^0.0.1") (default-features #t) (kind 0)))) (hash "161c5xpqdzl7cnp6il4fwn2ylvl663agi2a88v5kf9kcv2k2l80j")))

