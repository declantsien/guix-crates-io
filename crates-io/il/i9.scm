(define-module (crates-io il i9) #:use-module (crates-io))

(define-public crate-ili9163_driver-0.1 (crate (name "ili9163_driver") (vers "0.1.0") (deps (list (crate-dep (name "embedded-hal") (req "^0.2.7") (default-features #t) (kind 0)))) (hash "0cxjy8x5rg4mlxyiigw1lkw0fyyn9fr3nk1ff4jcpqk48kmzgzlv")))

(define-public crate-ili9163_driver-0.2 (crate (name "ili9163_driver") (vers "0.2.0") (deps (list (crate-dep (name "embedded-hal") (req "^0.2.7") (default-features #t) (kind 0)))) (hash "0bgz4imnck34wazbggpjqq08y1y55kyg6hb1ydwj84g3qqgryyzg")))

(define-public crate-ili9163_driver-0.2 (crate (name "ili9163_driver") (vers "0.2.1") (deps (list (crate-dep (name "embedded-hal") (req "^0.2.7") (default-features #t) (kind 0)))) (hash "1680pgm6ngfmmjl26zycq8k06wbkc3z7l0kv2dczhqbdkqdba88p")))

(define-public crate-ili9163_driver-0.2 (crate (name "ili9163_driver") (vers "0.2.2") (deps (list (crate-dep (name "embedded-hal") (req "^0.2.7") (default-features #t) (kind 0)))) (hash "176kv6whq9vf4i0kxz7lgxqihcsc426cjh458s2i8b664chhdivf")))

(define-public crate-ili9163_driver-1 (crate (name "ili9163_driver") (vers "1.0.0") (deps (list (crate-dep (name "embedded-hal") (req "^0.2.7") (default-features #t) (kind 0)))) (hash "1hp5593jfmlvz1xgn8lsayw2qzcy7dvvihb7nz35r97ypamhqml2")))

(define-public crate-ili9341-0.1 (crate (name "ili9341") (vers "0.1.0") (deps (list (crate-dep (name "embedded-hal") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "14ih0irsw84qaivr1vdy2fd93417a6xrz0sv4mwrmj94fn1fmhfp")))

(define-public crate-ili9341-0.1 (crate (name "ili9341") (vers "0.1.1") (deps (list (crate-dep (name "embedded-hal") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "0i2d8k2hvrskx49gv4rznp6nzs7i6r4ar4f362acgimq8cggczlv")))

(define-public crate-ili9341-0.1 (crate (name "ili9341") (vers "0.1.2") (deps (list (crate-dep (name "embedded-hal") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "02pval4jxrv040v9xy31qs0a23y4s087971pqprnhyd1kcr5qjxj")))

(define-public crate-ili9341-0.1 (crate (name "ili9341") (vers "0.1.3") (deps (list (crate-dep (name "embedded-hal") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "0s41qsxnqc7ryn863rd3rk7wdkgflsf3pw0v3585f2x92j08hvf3")))

(define-public crate-ili9341-0.1 (crate (name "ili9341") (vers "0.1.4") (deps (list (crate-dep (name "embedded-hal") (req "^0.2.1") (default-features #t) (kind 0)))) (hash "0b89s11jhdizmgfjf7r4m5yisvc7nk9qimcph7y07i1mbinhx6hz")))

(define-public crate-ili9341-0.1 (crate (name "ili9341") (vers "0.1.5") (deps (list (crate-dep (name "embedded-graphics") (req "^0.1.1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "embedded-hal") (req "^0.2.1") (default-features #t) (kind 0)))) (hash "1hsnfw94gzd1yc6w0lx192gbhy3aszlpl2yskhp8hmhslbgdywjh") (features (quote (("graphics" "embedded-graphics") ("default" "graphics")))) (yanked #t)))

(define-public crate-ili9341-0.1 (crate (name "ili9341") (vers "0.1.6") (deps (list (crate-dep (name "embedded-graphics") (req "^0.1.1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "embedded-hal") (req "^0.2.1") (default-features #t) (kind 0)))) (hash "1p68yv30lkqb9n7qrc5vfap1g60yc7kidwpxxygx08ckkaqkpkha") (features (quote (("graphics" "embedded-graphics") ("default" "graphics")))) (yanked #t)))

(define-public crate-ili9341-0.1 (crate (name "ili9341") (vers "0.1.7") (deps (list (crate-dep (name "embedded-graphics") (req "^0.1.1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "embedded-hal") (req "^0.2.1") (default-features #t) (kind 0)))) (hash "0cnj56gxyyd2fwl1s4mmr0n6j4rigp8cdagd8sh9kpjj15qbzqnd") (features (quote (("graphics" "embedded-graphics") ("default" "graphics"))))))

(define-public crate-ili9341-0.2 (crate (name "ili9341") (vers "0.2.0") (deps (list (crate-dep (name "embedded-graphics") (req "^0.4.7") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "embedded-hal") (req "^0.2.2") (default-features #t) (kind 0)))) (hash "0p2yxz8rbp44agbbp1xzhywf2p7dl7kh94ny2c5fkazcf85vbcdl") (features (quote (("graphics" "embedded-graphics") ("default" "graphics"))))))

(define-public crate-ili9341-0.3 (crate (name "ili9341") (vers "0.3.0-beta.1") (deps (list (crate-dep (name "embedded-graphics") (req "= 0.6.0-beta.1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "embedded-hal") (req "^0.2.3") (default-features #t) (kind 0)))) (hash "0f3xc7fslqwgxm0a3r1q89ijjzxas66yi8a99m6vnyi80jfkhqh0") (features (quote (("graphics" "embedded-graphics") ("default" "graphics"))))))

(define-public crate-ili9341-0.3 (crate (name "ili9341") (vers "0.3.0") (deps (list (crate-dep (name "embedded-graphics") (req "^0.6.1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "embedded-hal") (req "^0.2.3") (default-features #t) (kind 0)))) (hash "1v39rbfxrky6lrhdffm01i9rmwhvxsw7mnckpbyxbkqnd3rvw4in") (features (quote (("graphics" "embedded-graphics") ("default" "graphics"))))))

(define-public crate-ili9341-0.4 (crate (name "ili9341") (vers "0.4.0") (deps (list (crate-dep (name "display-interface") (req "^0.4.0") (default-features #t) (kind 0)) (crate-dep (name "embedded-graphics") (req "^0.6.2") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "embedded-hal") (req "^0.2.4") (default-features #t) (kind 0)))) (hash "0wpsrq5mfrk28dk0iwmi8aqi4hdvggaaz97h56j1z0czh0hfp3jh") (features (quote (("graphics" "embedded-graphics") ("default" "graphics"))))))

(define-public crate-ili9341-0.4 (crate (name "ili9341") (vers "0.4.1") (deps (list (crate-dep (name "display-interface") (req "^0.4.0") (default-features #t) (kind 0)) (crate-dep (name "embedded-graphics") (req "^0.6.2") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "embedded-hal") (req "^0.2.4") (default-features #t) (kind 0)))) (hash "093cy4blyws090zq4a7ckkc7d8igl438fagdyk3236azz0xz5qfx") (features (quote (("graphics" "embedded-graphics") ("default" "graphics"))))))

(define-public crate-ili9341-0.5 (crate (name "ili9341") (vers "0.5.0") (deps (list (crate-dep (name "display-interface") (req "^0.4.1") (default-features #t) (kind 0)) (crate-dep (name "embedded-graphics-core") (req "^0.3") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "embedded-hal") (req "^0.2.5") (default-features #t) (kind 0)))) (hash "1af06zqy6mm8r5nk8ndak04kp3f04v9v7w50gnp52l767s97m2aj") (features (quote (("graphics" "embedded-graphics-core") ("default" "graphics"))))))

(define-public crate-ili9341-0.6 (crate (name "ili9341") (vers "0.6.0") (deps (list (crate-dep (name "cortex-m") (req "^0.7.3") (default-features #t) (kind 2)) (crate-dep (name "cortex-m-rt") (req "^0.7.0") (default-features #t) (kind 2)) (crate-dep (name "cortex-m-rtic") (req "^1.0.0") (default-features #t) (kind 2)) (crate-dep (name "defmt-rtt") (req "^0.3.0") (default-features #t) (kind 2)) (crate-dep (name "display-interface") (req "^0.5") (default-features #t) (kind 0)) (crate-dep (name "embedded-graphics-core") (req "^0.4") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "embedded-hal") (req "^1.0.0") (default-features #t) (kind 0)) (crate-dep (name "panic-semihosting") (req "^0.6") (default-features #t) (kind 2)) (crate-dep (name "stm32f4xx-hal") (req "^0.12.0") (features (quote ("stm32f411"))) (default-features #t) (kind 2)))) (hash "02mkknw75pcxbrcz2ncqc7r6l8gzpj0bl8hsb87n6xm0vc2mc46c") (features (quote (("graphics" "embedded-graphics-core") ("default" "graphics"))))))

