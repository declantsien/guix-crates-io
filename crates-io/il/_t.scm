(define-module (crates-io il _t) #:use-module (crates-io))

(define-public crate-il_tz-0.1 (crate (name "il_tz") (vers "0.1.0") (deps (list (crate-dep (name "num") (req "^0.4.0") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.38") (default-features #t) (kind 0)))) (hash "0n9y9l5bc3mrwpqk2kd7n39gdrppx0d3s1rj2w6vhb4sc31svlrc")))

(define-public crate-il_tz-0.1 (crate (name "il_tz") (vers "0.1.1") (deps (list (crate-dep (name "num") (req "^0.4.0") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.38") (default-features #t) (kind 0)))) (hash "1dm46w7aq50hdjd5s7zhmn2dmf5cnirfkpyypnf24hw65qsd99vh")))

(define-public crate-il_tz-0.1 (crate (name "il_tz") (vers "0.1.2") (deps (list (crate-dep (name "clap") (req "^4.1.1") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "num") (req "^0.4.0") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.38") (default-features #t) (kind 0)))) (hash "086mn5wsxgmwjf8ywpvkkx2vw9h5xd3bb87944mbfw8raj5ksyvz") (features (quote (("cli" "clap"))))))

(define-public crate-il_tz-0.1 (crate (name "il_tz") (vers "0.1.3") (deps (list (crate-dep (name "clap") (req "^4.1.1") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "num") (req "^0.4.0") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.38") (default-features #t) (kind 0)))) (hash "1g0q13n7kp5c1b3qk4jahf7yfd7463cs65zqhnlak90b3lp13sin") (features (quote (("cli" "clap"))))))

