(define-module (crates-io il lm) #:use-module (crates-io))

(define-public crate-illmatic-scheduling-0.1 (crate (name "illmatic-scheduling") (vers "0.1.0") (deps (list (crate-dep (name "futures") (req "^0.3.15") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.5.4") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.5.0") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "12jyayjnwz8fh25fwn0qwpvzfhf01jlkmssbqvnc6crc50bwd8pb")))

(define-public crate-illmatic-scheduling-0.1 (crate (name "illmatic-scheduling") (vers "0.1.1") (deps (list (crate-dep (name "futures") (req "^0.3.15") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.5.4") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.5.0") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "188aa766gidwh3knx44gbr0s8zhkxmflggjzgxdyqf6slfbym00j")))

(define-public crate-illmatic-scheduling-0.1 (crate (name "illmatic-scheduling") (vers "0.1.2") (deps (list (crate-dep (name "futures") (req "^0.3.15") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.5.4") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.5.0") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "06rg50lssqm59b1pcgg2iczp7p8x6wszkfbca1hmckb9q1pb3zp0")))

