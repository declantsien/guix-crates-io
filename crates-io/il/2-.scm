(define-module (crates-io il #{2-}#) #:use-module (crates-io))

(define-public crate-il2-ilint-1 (crate (name "il2-ilint") (vers "1.1.0") (hash "02rzjy5h7jlc3zjwk4fbvv76vzw29kk9rbnp36x686akrvzbqsgk")))

(define-public crate-il2-ilint-1 (crate (name "il2-ilint") (vers "1.1.1") (hash "0nxm792w1pvj1y6jign4rlssmsjf1sp3yh8z3pk12m7p8wlnyjs9")))

(define-public crate-il2-iltags-1 (crate (name "il2-iltags") (vers "1.0.0") (hash "0r2zbb966wnjhhy1bhz7j98m626c43h5avj980jbsxwix4106x1f") (yanked #t)))

(define-public crate-il2-iltags-1 (crate (name "il2-iltags") (vers "1.0.1") (hash "1037vm09w8ddh1db3x18wjhik8qzpz3z4syar9d17y6k6kwvi73d")))

(define-public crate-il2-iltags-1 (crate (name "il2-iltags") (vers "1.1.0") (hash "087zfn6hvb7jyw09gagcc57cphxx9gjbr3w54sf46848mzmz46m8")))

(define-public crate-il2-iltags-1 (crate (name "il2-iltags") (vers "1.1.1") (hash "0zm6l3kwg6kv3rz9hfrglzlqa6a5bjk2mhi2mpsqji7llncj7h6f")))

(define-public crate-il2-iltags-1 (crate (name "il2-iltags") (vers "1.2.0") (hash "16b5i99f30x80p0b82q9jywsz9748lmkrs3q8dlx365szcl44y87")))

(define-public crate-il2-iltags-1 (crate (name "il2-iltags") (vers "1.3.0") (hash "0zifzwlph9bmg66cnh49kbvrhf02agqq7kxxkhwjdgyxvgjg7h52")))

(define-public crate-il2-iltags-1 (crate (name "il2-iltags") (vers "1.4.0") (hash "14pndhnpa66ycszy2km30spskx0kdpr0dm8r31z4p3y1bzv7qgwx")))

(define-public crate-il2-test-utils-0.1 (crate (name "il2-test-utils") (vers "0.1.0") (hash "0aj16k49blyb4sz8vnbb4igyqlmc2jlcd0vnfwybpc3hpzdz15l0") (yanked #t)))

(define-public crate-il2-test-utils-0.1 (crate (name "il2-test-utils") (vers "0.1.1") (hash "0ra7snv2l2q63s0skc2adjw4dzblih77ra103dy3hvfpsw4h5i3s")))

(define-public crate-il2-utils-0.1 (crate (name "il2-utils") (vers "0.1.0") (deps (list (crate-dep (name "chrono") (req "^0.4.19") (default-features #t) (kind 0)) (crate-dep (name "fd-lock") (req "^3.0.0") (default-features #t) (kind 0)) (crate-dep (name "il2-test-utils") (req "^0.1.1") (default-features #t) (kind 2)) (crate-dep (name "libc") (req "^0.2.98") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.7.3") (features (quote ("std" "getrandom"))) (default-features #t) (kind 0)) (crate-dep (name "windows") (req "^0.18.0") (default-features #t) (target "cfg(windows)") (kind 0)) (crate-dep (name "windows") (req "^0.18.0") (default-features #t) (target "cfg(windows)") (kind 1)) (crate-dep (name "zeroize") (req "^1.3.0") (default-features #t) (kind 0)))) (hash "0mwsnngjfqan3aj99vm5kmmd4z96ij80nsibh6w3bw9q89z187wr")))

(define-public crate-il2-utils-0.1 (crate (name "il2-utils") (vers "0.1.1") (deps (list (crate-dep (name "chrono") (req "^0.4.19") (default-features #t) (kind 0)) (crate-dep (name "fd-lock") (req "^3.0.0") (default-features #t) (kind 0)) (crate-dep (name "il2-test-utils") (req "^0.1.1") (default-features #t) (kind 2)) (crate-dep (name "libc") (req "^0.2.98") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.7.3") (features (quote ("std" "getrandom"))) (default-features #t) (kind 0)) (crate-dep (name "windows") (req "^0.18.0") (default-features #t) (target "cfg(windows)") (kind 0)) (crate-dep (name "windows") (req "^0.18.0") (default-features #t) (target "cfg(windows)") (kind 1)) (crate-dep (name "zeroize") (req "^1.3.0") (default-features #t) (kind 0)))) (hash "1pacr041avw5lwxssiki2f7d6zpscdwbqmxbhzvf7d48pvy3dns3")))

(define-public crate-il2-utils-0.1 (crate (name "il2-utils") (vers "0.1.2") (deps (list (crate-dep (name "chrono") (req "^0.4.19") (default-features #t) (kind 0)) (crate-dep (name "fd-lock") (req "^3.0.0") (default-features #t) (kind 0)) (crate-dep (name "il2-test-utils") (req "^0.1.1") (default-features #t) (kind 2)) (crate-dep (name "libc") (req "^0.2.98") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.7.3") (features (quote ("std" "getrandom"))) (default-features #t) (kind 0)) (crate-dep (name "windows") (req "^0.32.0") (features (quote ("Win32_Foundation" "Win32_Security_Cryptography" "Win32_System_Memory"))) (default-features #t) (target "cfg(windows)") (kind 0)) (crate-dep (name "windows") (req "^0.32.0") (default-features #t) (target "cfg(windows)") (kind 1)) (crate-dep (name "zeroize") (req "^1.3.0") (default-features #t) (kind 0)))) (hash "03mczcxrs7m92rcl0985kbxh2lryxrkjph8125ifz8y5bhyhfp63")))

