(define-module (crates-io il vm) #:use-module (crates-io))

(define-public crate-ilvm-0.2 (crate (name "ilvm") (vers "0.2.0") (deps (list (crate-dep (name "clap") (req "^2.32.0") (default-features #t) (kind 0)) (crate-dep (name "combine") (req "^3.6.2") (default-features #t) (kind 0)))) (hash "133fyn0ljw4pb5rgfrk309zz4ax2p63dn7cb9kfd4xzpbczycqz3")))

(define-public crate-ilvm-0.2 (crate (name "ilvm") (vers "0.2.1") (deps (list (crate-dep (name "clap") (req "^2.32.0") (default-features #t) (kind 0)) (crate-dep (name "combine") (req "^3.6.2") (default-features #t) (kind 0)))) (hash "03jkwa3dvwz28j9y13pkjdz55h2xhdawdpmkns5dcfm2iyw62swr")))

