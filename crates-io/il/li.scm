(define-module (crates-io il li) #:use-module (crates-io))

(define-public crate-illicit-0.9 (crate (name "illicit") (vers "0.9.0") (deps (list (crate-dep (name "illicit-macro") (req "^0.9.0") (default-features #t) (kind 0)) (crate-dep (name "owning_ref") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "scopeguard") (req "^1") (default-features #t) (kind 0)))) (hash "0l9a8icscql5mnail4hw17jm1aawlamp5znfszc6si8pj4w7d2n6")))

(define-public crate-illicit-0.9 (crate (name "illicit") (vers "0.9.1") (deps (list (crate-dep (name "illicit-macro") (req "^0.9.0") (default-features #t) (kind 0)) (crate-dep (name "owning_ref") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "scopeguard") (req "^1") (default-features #t) (kind 0)))) (hash "1a8m82sfcrznvbwr5cqmmnplkjxw6jfdcxv0lfkzi8imn7j2hgqd")))

(define-public crate-illicit-0.9 (crate (name "illicit") (vers "0.9.2") (deps (list (crate-dep (name "criterion") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "illicit-macro") (req "^0.9.0") (default-features #t) (kind 0)) (crate-dep (name "owning_ref") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "scopeguard") (req "^1") (default-features #t) (kind 0)))) (hash "0c0ixacjpf8w4n8myxkqnqbkaxp8djqj1h90a2y42aqmngb1jwmz")))

(define-public crate-illicit-0.10 (crate (name "illicit") (vers "0.10.0") (deps (list (crate-dep (name "criterion") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "illicit-macro") (req "^0.10.0") (default-features #t) (kind 0)) (crate-dep (name "owning_ref") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "scopeguard") (req "^1") (default-features #t) (kind 0)))) (hash "1kmya6pfa9c41rsiklndpd4836bxs6shx3wxr4sdv4l7rxc3wrdz")))

(define-public crate-illicit-1 (crate (name "illicit") (vers "1.0.0") (deps (list (crate-dep (name "criterion") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "illicit-macro") (req "^1.0.0") (default-features #t) (kind 0)) (crate-dep (name "owning_ref") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "scopeguard") (req "^1") (default-features #t) (kind 0)))) (hash "1drk9m40ih0ydavk9ac3zl6gj6j9fb5p52ldp10rgf1a7dixqfnm")))

(define-public crate-illicit-1 (crate (name "illicit") (vers "1.1.0") (deps (list (crate-dep (name "criterion") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "illicit-macro") (req "^1.0.0") (default-features #t) (kind 0)) (crate-dep (name "insta") (req "^0.16.1") (default-features #t) (kind 2)) (crate-dep (name "owning_ref") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "scopeguard") (req "^1") (default-features #t) (kind 0)))) (hash "18r32mnawbiscxjv9x9kyrwls0w8z893s605qlvfisiasmqkdmjd")))

(define-public crate-illicit-1 (crate (name "illicit") (vers "1.1.1") (deps (list (crate-dep (name "criterion") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "illicit-macro") (req "^1.0.0") (default-features #t) (kind 0)) (crate-dep (name "insta") (req "^0.16.1") (default-features #t) (kind 2)) (crate-dep (name "owning_ref") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "scopeguard") (req "^1") (default-features #t) (kind 0)))) (hash "19l4vfjcx04nnavwawn37qz4h8b1x72i4qs5rk9v7c4ijr49wpc2")))

(define-public crate-illicit-1 (crate (name "illicit") (vers "1.1.2") (deps (list (crate-dep (name "criterion") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "illicit-macro") (req "^1.0.0") (default-features #t) (kind 0)) (crate-dep (name "insta") (req "^1.0.0") (default-features #t) (kind 2)) (crate-dep (name "owning_ref") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "scopeguard") (req "^1") (default-features #t) (kind 0)))) (hash "1lqh2yiwlrwf5nfjm1n34ld64gyfjkw0vycngsjnqp2m4g9kdcx8")))

(define-public crate-illicit-macro-0.9 (crate (name "illicit-macro") (vers "0.9.0") (deps (list (crate-dep (name "proc-macro-error") (req "^0.4.4") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "12xmjwhfpabr708k1lbckq6d94sf67psni8wjclb6jwfl0bls9bn")))

(define-public crate-illicit-macro-0.10 (crate (name "illicit-macro") (vers "0.10.0") (deps (list (crate-dep (name "proc-macro-error") (req "^1.0.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "1idzsznyy69z2x225af06c3ssr58km7lif75i9kwjlkjjsvd89g9")))

(define-public crate-illicit-macro-1 (crate (name "illicit-macro") (vers "1.0.0") (deps (list (crate-dep (name "proc-macro-error") (req "^1.0.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "15kxgla6h83gydxsl29vkwixprgikdr5lbzvszk4rhs2zb2gldg7")))

(define-public crate-illist-0.0.1 (crate (name "illist") (vers "0.0.1") (deps (list (crate-dep (name "rand") (req "^0.4.2") (default-features #t) (kind 2)))) (hash "0p3r365j50fvwd62jwqmiik3rzyz4xzi2jgj7xs31h6giqzc5fc2")))

(define-public crate-illist-0.1 (crate (name "illist") (vers "0.1.0") (deps (list (crate-dep (name "rand") (req "^0.4.2") (default-features #t) (kind 2)))) (hash "0xm58968h58j29smwsn61jx4kflkgb5frp7ackcmgif9v0ril6zw")))

