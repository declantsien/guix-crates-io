(define-module (crates-io il tc) #:use-module (crates-io))

(define-public crate-iltcme-0.1 (crate (name "iltcme") (vers "0.1.0") (deps (list (crate-dep (name "approx") (req "^0.5.1") (default-features #t) (kind 2)) (crate-dep (name "nalgebra") (req "^0.32.3") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.188") (features (quote ("derive"))) (default-features #t) (kind 1)) (crate-dep (name "serde_json") (req "^1.0.105") (default-features #t) (kind 1)))) (hash "1yp2wc08bm0gybvmhxvvpfkjid84ksq023kl70glxwq9sdaa3w5a")))

(define-public crate-iltcme-0.2 (crate (name "iltcme") (vers "0.2.0") (deps (list (crate-dep (name "approx") (req "^0.5.1") (default-features #t) (kind 2)) (crate-dep (name "nalgebra") (req "^0.32.3") (default-features #t) (kind 0)) (crate-dep (name "pyo3") (req "^0.19.2") (features (quote ("auto-initialize"))) (default-features #t) (kind 2)))) (hash "10wli7cx3l79r9q9wdp5h001lkv8c226n08pbma9hkqqqbxqga5w")))

(define-public crate-iltcme-0.2 (crate (name "iltcme") (vers "0.2.2") (deps (list (crate-dep (name "approx") (req "^0.5.1") (default-features #t) (kind 2)) (crate-dep (name "nalgebra") (req "^0.32.3") (default-features #t) (kind 0)) (crate-dep (name "pyo3") (req "^0.20.2") (features (quote ("auto-initialize"))) (default-features #t) (kind 2)))) (hash "119916nmdcv19pv6w9sfkpb1q1lzih1q85byrjbdvwjxxca7xkjk")))

