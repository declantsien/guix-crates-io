(define-module (crates-io il ya) #:use-module (crates-io))

(define-public crate-ilyaraz_test_crate_1-0.1 (crate (name "ilyaraz_test_crate_1") (vers "0.1.0") (hash "0z7a7glzzl604w3pjyz799h5npcz5z481cwi4mjfkyxcwhjspijp")))

(define-public crate-ilyaraz_test_crate_1-0.1 (crate (name "ilyaraz_test_crate_1") (vers "0.1.1") (hash "0bcx0v74s4iqp299nqy33xzzg9id9ffkw3f7pbm4yqqr8dayzy2k")))

(define-public crate-ilyaraz_test_crate_1-0.1 (crate (name "ilyaraz_test_crate_1") (vers "0.1.2") (deps (list (crate-dep (name "serde_json") (req "^1.0.81") (default-features #t) (kind 0)))) (hash "03g76sm4rhadlnf75ld5fw6001pg418zc6vv3i5wkps1l74kqwij")))

(define-public crate-ilyaraz_test_crate_1-0.1 (crate (name "ilyaraz_test_crate_1") (vers "0.1.3") (deps (list (crate-dep (name "serde_json") (req "^1.0.81") (default-features #t) (kind 0)))) (hash "0mp8gynvh7pjh7hrg5zmg9874z5rnacphxai2608g7ykjxhq5m8a")))

(define-public crate-ilyavenner-0.1 (crate (name "ilyavenner") (vers "0.1.0") (hash "035m9zzhlsz0vbw2wp40s2hfpgp9vj8fbbapgnp7wq8xnri78i4p") (yanked #t)))

(define-public crate-ilyavenner-0.1 (crate (name "ilyavenner") (vers "0.1.1") (hash "1wi9mh6gyxlncwdzq4iddxfkhrlhg1px3673z9d5ldqvgdbzv8ng") (yanked #t)))

