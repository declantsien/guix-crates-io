(define-module (crates-io il og) #:use-module (crates-io))

(define-public crate-ilog-0.1 (crate (name "ilog") (vers "0.1.0") (deps (list (crate-dep (name "num-traits") (req "^0") (default-features #t) (kind 0)))) (hash "1xz35a4k77ivkk0mw8c143rfqwls0yj4q3wwgisz70nxqhir8jd3")))

(define-public crate-ilog-0.1 (crate (name "ilog") (vers "0.1.1") (deps (list (crate-dep (name "num-traits") (req "^0") (default-features #t) (kind 0)))) (hash "1ry4vqchx5f0z624w8pii438nilzcfhl1psj99vbjz4w1z351paa")))

(define-public crate-ilog-0.1 (crate (name "ilog") (vers "0.1.2") (hash "0c9y5d0jg33yyy1m1yhp0ll9hq8xy9pmks708pva7k9c2h8lgv0a")))

(define-public crate-ilog-0.1 (crate (name "ilog") (vers "0.1.3") (hash "04zlfldyy66dmiiqdz4nwlv4m6y8ynp5pdnlr54d1q2j7wvybrxc")))

(define-public crate-ilog-0.1 (crate (name "ilog") (vers "0.1.4") (hash "0lz66r05yqjc9k86nwlyj95snlhvnys1xvb528ljjbdyxysqaxvr")))

(define-public crate-ilog-1 (crate (name "ilog") (vers "1.0.0") (hash "15vx3hgiq1wsm4lj22qli43d0i1wg98lgz3q0pjx918sv8q1bx5i")))

(define-public crate-ilog-1 (crate (name "ilog") (vers "1.0.1") (hash "0qm3p4v2bqj623ailb2ddcsas7vfbb69q5fsh0xmlq2xb9nqkmm9")))

(define-public crate-ilog2-0.0.1 (crate (name "ilog2") (vers "0.0.1") (hash "0s0w6kr7cm9bj329s6hys1fy80z399fvk7iilyxsdv2v756ls9hc")))

(define-public crate-ilog2-0.0.2 (crate (name "ilog2") (vers "0.0.2") (hash "0680gr5d99cccgrw04fnrjyal2y7805qf4siihmdxxldp768j6vb")))

(define-public crate-ilog2-0.1 (crate (name "ilog2") (vers "0.1.0") (hash "0s4mdcfzyc9za5i9kpxa25ba050hs25cc3ppk67c23lz9llpas8w")))

(define-public crate-ilog2-0.1 (crate (name "ilog2") (vers "0.1.1") (hash "0nnmw4r33d8yq3n7wc5mb7k1c8zv4mayicv856li102z2ybrarsn")))

(define-public crate-ilog2-0.2 (crate (name "ilog2") (vers "0.2.0") (deps (list (crate-dep (name "num") (req "*") (default-features #t) (kind 0)))) (hash "1c4srq5idakbkkarskf33xbgynlcvsmcghz7j8vrycnckwxfghqc")))

(define-public crate-ilog2-0.2 (crate (name "ilog2") (vers "0.2.1") (deps (list (crate-dep (name "num") (req "*") (default-features #t) (kind 0)))) (hash "0m16l14kxlwr1kgyfrn23jj1dbf7h86ig5igbikpj9d003c3las2")))

(define-public crate-ilog2-0.2 (crate (name "ilog2") (vers "0.2.2") (deps (list (crate-dep (name "num") (req "*") (default-features #t) (kind 0)))) (hash "0crd0lwl0qsqkcnmw62zwwla0vm1wgxafp26lisw1846inlgldfa")))

