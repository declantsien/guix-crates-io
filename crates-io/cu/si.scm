(define-module (crates-io cu si) #:use-module (crates-io))

(define-public crate-cusip-0.1 (crate (name "cusip") (vers "0.1.0") (deps (list (crate-dep (name "bstr") (req "^0.2") (features (quote ("std"))) (default-features #t) (kind 0)) (crate-dep (name "criterion") (req "^0.3") (features (quote ("html_reports"))) (default-features #t) (kind 2)) (crate-dep (name "proptest") (req "^1.0") (default-features #t) (kind 2)))) (hash "17c6viy3x9xfidmx48y38sk2jyndjngh7q9kkid74pbpy4fjcwdy") (yanked #t)))

(define-public crate-cusip-0.1 (crate (name "cusip") (vers "0.1.1") (deps (list (crate-dep (name "bstr") (req "^0.2") (features (quote ("std"))) (default-features #t) (kind 0)) (crate-dep (name "criterion") (req "^0.3") (features (quote ("html_reports"))) (default-features #t) (kind 2)) (crate-dep (name "proptest") (req "^1.0") (default-features #t) (kind 2)))) (hash "01ghfsbix2wbfgmr9m7lws9v47if7v70b6jhhghh8s20v9y1fj5p") (yanked #t)))

(define-public crate-cusip-0.2 (crate (name "cusip") (vers "0.2.0") (deps (list (crate-dep (name "bstr") (req "^0.2") (features (quote ("std"))) (default-features #t) (kind 0)) (crate-dep (name "criterion") (req "^0.3") (features (quote ("html_reports"))) (default-features #t) (kind 2)) (crate-dep (name "proptest") (req "^1.0") (default-features #t) (kind 2)))) (hash "17fh0x1p2wv3x1bl768djplg4394mwv45glczqkqlim1r5axqs38")))

(define-public crate-cusip-0.2 (crate (name "cusip") (vers "0.2.1") (deps (list (crate-dep (name "bstr") (req "^0.2") (features (quote ("std"))) (default-features #t) (kind 0)) (crate-dep (name "criterion") (req "^0.3") (features (quote ("html_reports"))) (default-features #t) (kind 2)) (crate-dep (name "proptest") (req "^1.0") (default-features #t) (kind 2)))) (hash "08qr3359gnlxaxc52r8x0cd52hwrqnkvwri4q8ld473r35xm8b1w")))

(define-public crate-cusip-0.2 (crate (name "cusip") (vers "0.2.2") (deps (list (crate-dep (name "bstr") (req "^0.2") (features (quote ("std"))) (default-features #t) (kind 0)) (crate-dep (name "criterion") (req "^0.3") (features (quote ("html_reports"))) (default-features #t) (kind 2)) (crate-dep (name "proptest") (req "^1.0") (default-features #t) (kind 2)))) (hash "04nv16c6rkzkgjky3ila48i81yz2gd5wmq6aqlvqxi6k665ygpz8")))

(define-public crate-cusip-0.2 (crate (name "cusip") (vers "0.2.3") (deps (list (crate-dep (name "bstr") (req "^0.2") (features (quote ("std"))) (default-features #t) (kind 0)) (crate-dep (name "criterion") (req "^0.3") (features (quote ("html_reports"))) (default-features #t) (kind 2)) (crate-dep (name "proptest") (req "^1.0") (default-features #t) (kind 2)))) (hash "0xyq1qy4fyl9ync6b422gp883w53vxn90q7dl0jbbycayiv5gp92")))

(define-public crate-cusip-0.2 (crate (name "cusip") (vers "0.2.4") (deps (list (crate-dep (name "bstr") (req "^1.2") (features (quote ("std"))) (default-features #t) (kind 0)) (crate-dep (name "criterion") (req "^0.3") (features (quote ("html_reports"))) (default-features #t) (kind 2)) (crate-dep (name "proptest") (req "^1.0") (default-features #t) (kind 2)))) (hash "1d45nhc93b3c9hczvbsv6mi2rf3riyhpar9ricmrhzbvlzsm6k6i")))

(define-public crate-cusip-0.2 (crate (name "cusip") (vers "0.2.5") (deps (list (crate-dep (name "criterion") (req "^0.5.1") (features (quote ("html_reports"))) (default-features #t) (kind 2)) (crate-dep (name "proptest") (req "^1.2.0") (default-features #t) (kind 2)))) (hash "01rkakgydhhl3arznm13ixcrwsr6d6wc1xbabylv41jm8sr6f0am")))

(define-public crate-cusip-0.3 (crate (name "cusip") (vers "0.3.0") (deps (list (crate-dep (name "criterion") (req "^0.5.1") (features (quote ("html_reports"))) (default-features #t) (kind 2)) (crate-dep (name "proptest") (req "^1.4.0") (default-features #t) (kind 2)))) (hash "0xik3nnliak892b52x1nn814vxqb42l07q90c9sx8gz6xdfm58hj")))

