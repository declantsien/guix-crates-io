(define-module (crates-io cu bl) #:use-module (crates-io))

(define-public crate-cublas-0.1 (crate (name "cublas") (vers "0.1.0") (deps (list (crate-dep (name "clippy") (req "^0.0.35") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "collenchyma") (req "^0.0.7") (default-features #t) (kind 2)) (crate-dep (name "cublas-sys") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "1hccqsyjbyd35mhhy2v11i5ilqwdhlygxhfc2jkr0vfsaz2h9cmq") (features (quote (("travis") ("lint" "clippy") ("dev"))))))

(define-public crate-cublas-0.2 (crate (name "cublas") (vers "0.2.0") (deps (list (crate-dep (name "clippy") (req "^0.0.35") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "collenchyma") (req "^0.0.7") (default-features #t) (kind 2)) (crate-dep (name "cublas-sys") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "15qhky0cswd58rrzd6xax7c7fl6iwb1civxrvyv9m5c571mf9amc") (features (quote (("travis") ("lint" "clippy") ("dev"))))))

(define-public crate-cublas-sys-0.1 (crate (name "cublas-sys") (vers "0.1.0") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "pkg-config") (req "^0.3") (default-features #t) (kind 1)))) (hash "0cs6f5j07c37b5fimlj6sdjvhb6g2y0vkdmk9pr8wj67ifgarhn4")))

