(define-module (crates-io cu ma) #:use-module (crates-io))

(define-public crate-cumaea-0.1 (crate (name "cumaea") (vers "0.1.0") (deps (list (crate-dep (name "colored") (req "^2.1.0") (default-features #t) (kind 0)))) (hash "0sd0gi184flx0r3iijl3y56671wxr97w1q75whc43d574dajz9dp")))

(define-public crate-cumaea-0.1 (crate (name "cumaea") (vers "0.1.1") (deps (list (crate-dep (name "colored") (req "^2.1.0") (default-features #t) (kind 0)))) (hash "0fl2d35n7xkmg88p3b1p4my4ih3k6ja72linh79rq4991vdb3w7h")))

(define-public crate-cumath-0.1 (crate (name "cumath") (vers "0.1.0") (hash "1fjm5n3aqfmim08h5w40m0f64rb99m6cnwp7rh0aw3xb9762js01") (yanked #t)))

(define-public crate-cumath-0.1 (crate (name "cumath") (vers "0.1.1") (deps (list (crate-dep (name "cc") (req "^1.0") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2.39") (default-features #t) (kind 0)))) (hash "0ak5nqg3yap86pirpmd2wfcy47zj0wa7hh2kgfya6l7zi2xxgl00") (yanked #t)))

(define-public crate-cumath-0.1 (crate (name "cumath") (vers "0.1.11") (deps (list (crate-dep (name "cc") (req "^1.0") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2.39") (default-features #t) (kind 0)))) (hash "1x47ihl39z90aj3d6a4zrhdp55mmavckvfcjs1mpb834lzynvcjk") (yanked #t)))

(define-public crate-cumath-0.0.1 (crate (name "cumath") (vers "0.0.1") (deps (list (crate-dep (name "cc") (req "^1.0") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2.39") (default-features #t) (kind 0)))) (hash "1bq0jhpqrkdgpg1rn2wlbjqvdc9jsrm14xwq535z2qzr4i5730r0") (yanked #t)))

(define-public crate-cumath-0.0.2 (crate (name "cumath") (vers "0.0.2") (deps (list (crate-dep (name "cc") (req "^1.0") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2.39") (default-features #t) (kind 0)))) (hash "1xk58rapsfi2vm5spbx4892smkb5jg6cj2xhq26afxv11lby1zrx") (yanked #t)))

(define-public crate-cumath-0.2 (crate (name "cumath") (vers "0.2.0") (deps (list (crate-dep (name "cc") (req "^1.0") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2.39") (default-features #t) (kind 0)))) (hash "0lbfijg7vrr37dxk0g3frd8c92hgh5593r8l4lksqrznp0z16jd9")))

(define-public crate-cumath-0.2 (crate (name "cumath") (vers "0.2.1") (deps (list (crate-dep (name "cc") (req "^1.0") (default-features #t) (kind 1)))) (hash "13nrpqd4ii7rb0z85kwj6fc82x6w5xiscd1hznm9z2p5lx58zq37") (features (quote (("disable_checks") ("default"))))))

(define-public crate-cumath-0.2 (crate (name "cumath") (vers "0.2.2") (deps (list (crate-dep (name "cc") (req "^1.0") (default-features #t) (kind 1)))) (hash "04qvc7pim635b4x4rngcyzzmygq4br1ar3vjjjnp41wagci5652z") (features (quote (("disable_checks") ("default"))))))

(define-public crate-cumath-0.2 (crate (name "cumath") (vers "0.2.3") (deps (list (crate-dep (name "cc") (req "^1.0") (default-features #t) (kind 1)))) (hash "1ihynnfnkrba6m3mrpfc86nqqvymfpdaxx39nya6890jmjkp9aav") (features (quote (("disable_checks") ("default"))))))

(define-public crate-cumath-0.2 (crate (name "cumath") (vers "0.2.4") (deps (list (crate-dep (name "cc") (req "^1.0") (default-features #t) (kind 1)))) (hash "1djgf2jbb8a6zzzh6b4b38bfn5rn5n0q6wcilzs5hm88lbb4r4gw") (features (quote (("disable_checks") ("default"))))))

(define-public crate-cumath-0.2 (crate (name "cumath") (vers "0.2.5") (deps (list (crate-dep (name "cc") (req "^1.0.15") (default-features #t) (kind 1)))) (hash "0wrkr91h2gafs64xp41pbjjizhz6c1245qv66zvw04ddmh9ilkxb") (features (quote (("disable_checks") ("default"))))))

(define-public crate-cumath-0.2 (crate (name "cumath") (vers "0.2.6") (deps (list (crate-dep (name "cc") (req "^1.0.15") (default-features #t) (kind 1)))) (hash "024iigs8njg1r53xvdc68qf7ad4rzqhwwrbmrhm21j1gjnbx7400") (features (quote (("disable_checks") ("default"))))))

(define-public crate-cumath-0.2 (crate (name "cumath") (vers "0.2.7") (deps (list (crate-dep (name "cc") (req "^1.0.15") (default-features #t) (kind 1)))) (hash "1qni0nxpawrvdnsff4nd8l02pls0vl4vm12f9zq2irlsbv55n9vh") (features (quote (("disable_checks") ("default"))))))

