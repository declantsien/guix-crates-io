(define-module (crates-io cu tl) #:use-module (crates-io))

(define-public crate-cutlass-0.1 (crate (name "cutlass") (vers "0.1.0") (deps (list (crate-dep (name "proc-macro2") (req "^1.0.9") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "1a32032x7vz5qic9ky401blbv42jy0nbjqdn6ffrp99x6w07w7xs")))

