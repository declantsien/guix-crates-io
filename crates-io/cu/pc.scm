(define-module (crates-io cu pc) #:use-module (crates-io))

(define-public crate-Cupcake-0.1 (crate (name "Cupcake") (vers "0.1.0") (deps (list (crate-dep (name "bencher") (req "^0.1.5") (default-features #t) (kind 2)) (crate-dep (name "modinverse") (req "^0.1.1") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.5.5") (default-features #t) (kind 0)))) (hash "1x3sl1ih58gmadqqp60psrxiwih6gbih199n1i80i1kflq0xa7k6") (yanked #t)))

(define-public crate-Cupcake-0.1 (crate (name "Cupcake") (vers "0.1.1") (deps (list (crate-dep (name "bencher") (req "^0.1.5") (default-features #t) (kind 2)) (crate-dep (name "modinverse") (req "^0.1.1") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.5.5") (default-features #t) (kind 0)))) (hash "0d8wfrlbj81f2inpi0qmqwl5a1bk6szx0rsn46clr6db3jq4f2sl")))

(define-public crate-Cupcake-0.2 (crate (name "Cupcake") (vers "0.2.0") (deps (list (crate-dep (name "bencher") (req "^0.1.5") (default-features #t) (kind 2)) (crate-dep (name "modinverse") (req "^0.1.1") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.5.5") (default-features #t) (kind 0)))) (hash "0jyjpmnfr9fgz2i4v9sxxd2g2c35c05dzqfzvdxdzzfvld3zxnkk")))

(define-public crate-Cupcake-0.2 (crate (name "Cupcake") (vers "0.2.1") (deps (list (crate-dep (name "bencher") (req "^0.1.5") (default-features #t) (kind 2)) (crate-dep (name "modinverse") (req "^0.1.1") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.5.5") (default-features #t) (kind 0)))) (hash "0s6xypghxmwr5nkc46kc37dp40yijy6r431j9njxy135i3dhszry") (features (quote (("bench"))))))

(define-public crate-cupchan-0.1 (crate (name "cupchan") (vers "0.1.0") (deps (list (crate-dep (name "loom") (req "^0.5") (features (quote ("checkpoint"))) (default-features #t) (target "cfg(loom)") (kind 0)))) (hash "1k4jli1379x3554nlw6j0i8knz3iiykqxm7rxa8wyj056vz8icf0")))

(define-public crate-cupchan-0.1 (crate (name "cupchan") (vers "0.1.1") (deps (list (crate-dep (name "crossbeam-channel") (req "^0.5.4") (default-features #t) (kind 2)) (crate-dep (name "flume") (req "^0.10.12") (default-features #t) (kind 2)) (crate-dep (name "loom") (req "^0.5.4") (features (quote ("checkpoint"))) (default-features #t) (target "cfg(loom)") (kind 0)))) (hash "0d6jf8nqjw5ylmjpx8cxyxwxmxphbzjfbhzw9kip4kxlyhx4in5h")))

(define-public crate-cupchan-0.1 (crate (name "cupchan") (vers "0.1.2") (deps (list (crate-dep (name "crossbeam-channel") (req "^0.5.4") (default-features #t) (kind 2)) (crate-dep (name "flume") (req "^0.10.12") (default-features #t) (kind 2)) (crate-dep (name "loom") (req "^0.5.4") (features (quote ("checkpoint"))) (default-features #t) (target "cfg(loom)") (kind 0)))) (hash "1d2f8dm4hq23h66dpnwjil9fbwgixyfzmhkzgfhnlmyx9hczvzh1")))

