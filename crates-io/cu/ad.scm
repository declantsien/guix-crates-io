(define-module (crates-io cu ad) #:use-module (crates-io))

(define-public crate-cuadra-0.0.1 (crate (name "cuadra") (vers "0.0.1") (hash "098gd3drvys9qxkv2b8lnchv1f1irq46xic8di4bx8drni9fd580") (rust-version "1.61.0")))

(define-public crate-cuadra-0.0.2 (crate (name "cuadra") (vers "0.0.2") (hash "1inbcs7k2byfb8jxijka52sg82ijqzbn47zcwagf0rrsfby91cvq") (rust-version "1.61.0")))

(define-public crate-cuadra-0.1 (crate (name "cuadra") (vers "0.1.0") (hash "1wadhw3msrcss7dj9sfhsp0s1mz4hzf5kzdw0b2jnazy42xqjgb2") (features (quote (("std") ("default" "std")))) (rust-version "1.64.0")))

(define-public crate-cuadra-0.2 (crate (name "cuadra") (vers "0.2.0") (deps (list (crate-dep (name "paste") (req "^1.0.11") (default-features #t) (kind 0)))) (hash "1ihy5hzs8idcrwapl3hzkqx7nl6439fmbc690w6916p7zg91277i") (features (quote (("std") ("default" "std")))) (rust-version "1.64.0")))

(define-public crate-cuadra-0.3 (crate (name "cuadra") (vers "0.3.0") (deps (list (crate-dep (name "paste") (req "^1.0.12") (default-features #t) (kind 0)))) (hash "1ly8bmhar5bg469rxlz5pqz5q6p4n3blvzkkj6p66y2flmza2777") (features (quote (("std") ("nightly") ("default" "std")))) (rust-version "1.64.0")))

(define-public crate-cuadra-0.3 (crate (name "cuadra") (vers "0.3.1") (deps (list (crate-dep (name "paste") (req "^1.0.12") (default-features #t) (kind 0)))) (hash "00mnc5db88nydpdnx5xz8md9d7m3v984y8mhjr9agvxqak4n2q0l") (features (quote (("std") ("nightly") ("default" "std")))) (rust-version "1.64.0")))

