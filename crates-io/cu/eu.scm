(define-module (crates-io cu eu) #:use-module (crates-io))

(define-public crate-cueue-0.1 (crate (name "cueue") (vers "0.1.0") (deps (list (crate-dep (name "libc") (req "^0.2.132") (default-features #t) (kind 0)))) (hash "05yssr51zvwy504kiq9cn42ap7bmf2jdlnzl2162ck0rz7g7v5px")))

(define-public crate-cueue-0.1 (crate (name "cueue") (vers "0.1.1") (deps (list (crate-dep (name "libc") (req "^0.2.132") (default-features #t) (kind 0)))) (hash "1lzxv2fk651dbqvjnxh5f94swlxszi3lbrzz7c6gcsyw6byq2n3n")))

(define-public crate-cueue-0.2 (crate (name "cueue") (vers "0.2.0") (deps (list (crate-dep (name "libc") (req "^0.2.132") (default-features #t) (kind 0)))) (hash "15v3ingi2rij8hbp3289j6x6csj5acps9ijgc1acih2fm6g5q3i6")))

(define-public crate-cueue-0.3 (crate (name "cueue") (vers "0.3.0") (deps (list (crate-dep (name "libc") (req "^0.2.132") (default-features #t) (kind 0)))) (hash "159ab7i67sals4m3dm5va8b2l6kkcx5pqygp2d49w98jhcp82xp1")))

(define-public crate-cueue-0.3 (crate (name "cueue") (vers "0.3.1") (deps (list (crate-dep (name "libc") (req "^0.2.132") (default-features #t) (kind 0)))) (hash "1mhjgiq9x74cmkfw2laxn78zc29xbk8kzkq1ps2cc8bsdk7mg3cm")))

