(define-module (crates-io cu -s) #:use-module (crates-io))

(define-public crate-cu-sys-0.1 (crate (name "cu-sys") (vers "0.1.0") (deps (list (crate-dep (name "gcc") (req "^0.3") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "1qcs73a6r087vp94g9b252r922ad3fpb5yv0qfsdd7h77hqqydr1")))

