(define-module (crates-io cu co) #:use-module (crates-io))

(define-public crate-cuco-0.1 (crate (name "cuco") (vers "0.1.0") (hash "0n9kfm869pkzf7zvkzg0ngq194hrykac6a6bhwy7ra3i0bmpvmpj")))

(define-public crate-cuco-0.1 (crate (name "cuco") (vers "0.1.1") (hash "06p2617qwadws0p1l6ch3ski96y7ci943m0gl2dan3lgphffmbkr")))

(define-public crate-cuco-0.1 (crate (name "cuco") (vers "0.1.2") (hash "1lc921m17cwsz53197lgfn6wl22r3ids5i2w5bsw35y3sy8szdfy")))

(define-public crate-cuco-0.1 (crate (name "cuco") (vers "0.1.3") (hash "052mriy7bk7xbpaxfw8gfc25hr73d7jy7akvi3500rshdzjxlr40")))

(define-public crate-cucoqu-0.1 (crate (name "cucoqu") (vers "0.1.0") (deps (list (crate-dep (name "derive_more") (req "^0.99") (default-features #t) (kind 0)) (crate-dep (name "glifparser") (req "^1.2") (features (quote ("more-iof"))) (kind 0)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "nalgebra") (req "^0.31") (default-features #t) (kind 0)) (crate-dep (name "num-complex") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2") (default-features #t) (kind 0)))) (hash "14sgznhk4lzwk39mkdgfz22v891d0wrg5s3ag8jiplj42ma5viy1")))

