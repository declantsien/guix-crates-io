(define-module (crates-io cu ri) #:use-module (crates-io))

(define-public crate-curie-0.0.1 (crate (name "curie") (vers "0.0.1") (hash "0phm3ygz7p0yi8qhmx6va76fv2d05cdfsgsfwxx88y0bs1rl910b")))

(define-public crate-curie-0.0.2 (crate (name "curie") (vers "0.0.2") (hash "03vh9dm8ikfh76ndf2y2pcvgd0rd5f3sfq66wwmrzraizqi41bc6")))

(define-public crate-curie-0.0.3 (crate (name "curie") (vers "0.0.3") (hash "1jkpb3yi9xxj96f3mr4r1v13y4l31012ajapvk0nfjrghj2v7176")))

(define-public crate-curie-0.0.4 (crate (name "curie") (vers "0.0.4") (hash "1g8f2m41wgbmd55arhp809q54j722lmvd4yqg5dh14494zhlm7xr")))

(define-public crate-curie-0.0.5 (crate (name "curie") (vers "0.0.5") (hash "00nij5sw7md31fvmvpl5chd54ww363v4gwdcs140vf2h9s64c1bq")))

(define-public crate-curie-0.0.6 (crate (name "curie") (vers "0.0.6") (hash "1bh9d6spdx1mli80wm34fgplg59mnkw6yi4hi293xgb2kslmim51")))

(define-public crate-curie-0.0.7 (crate (name "curie") (vers "0.0.7") (hash "18zblw4i90rqmfb6hszgy10p134yc2q5zg36pcv4vkxncdfayzim")))

(define-public crate-curie-0.0.8 (crate (name "curie") (vers "0.0.8") (hash "03nvisjiv3vgkffih1n70nfq6863j5kklfnw5srd7gigd0rm9ga7")))

(define-public crate-curie-0.1 (crate (name "curie") (vers "0.1.0") (deps (list (crate-dep (name "indexmap") (req "^1") (default-features #t) (kind 0)))) (hash "03g6xvyapv3bns8qk1ki3qm21z9hydfvdrpmpdvfm2bf9yf0c41j")))

(define-public crate-curie-0.1 (crate (name "curie") (vers "0.1.1") (deps (list (crate-dep (name "indexmap") (req "^1") (default-features #t) (kind 0)))) (hash "0g107a34rh8bigx3ijyi3g3n3as4sksgv8q5xikrc33s3c9cw5a2")))

(define-public crate-curie-0.1 (crate (name "curie") (vers "0.1.2") (deps (list (crate-dep (name "indexmap") (req "^2") (default-features #t) (kind 0)))) (hash "1hw22ai1s3rz12rdnpv55xlcyansgqj7lfj6s2q48zzpqbs50v0d")))

(define-public crate-curies-0.1 (crate (name "curies") (vers "0.1.1") (deps (list (crate-dep (name "async-trait") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "ptrie") (req "^0.6") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.10") (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.11") (features (quote ("blocking" "json"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.34") (features (quote ("rt-multi-thread" "macros"))) (default-features #t) (kind 2)))) (hash "0072q54xbfb86n937n87x10md50g21wpgnzzas163m59602hvc4s")))

(define-public crate-curies-0.1 (crate (name "curies") (vers "0.1.2") (deps (list (crate-dep (name "async-trait") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "ptrie") (req "^0.7") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.10") (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.12") (kind 0)) (crate-dep (name "reqwest") (req "^0.12") (features (quote ("blocking" "json" "native-tls"))) (target "cfg(all(windows, target_arch = \"aarch64\"))") (kind 0)) (crate-dep (name "reqwest") (req "^0.12") (features (quote ("blocking" "json" "rustls-tls"))) (target "cfg(not(all(windows, target_arch = \"aarch64\")))") (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "sophia") (req "^0.8.0") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.34") (features (quote ("rt-multi-thread" "macros"))) (default-features #t) (kind 2)))) (hash "1gls45aqsmfwg3q2sdpcn6zirqiai0ci2qx80yc56ikphb9088w7")))

(define-public crate-curio-0.0.1 (crate (name "curio") (vers "0.0.1") (deps (list (crate-dep (name "rustls") (req "^0.18.1") (default-features #t) (kind 0)) (crate-dep (name "webpki") (req "^0.21.3") (default-features #t) (kind 0)) (crate-dep (name "webpki-roots") (req "^0.20.0") (default-features #t) (kind 0)))) (hash "196aipr3bzdv96l6llybrhirwrb8znbkg8n2n1dzqsmb4g22gxz8")))

(define-public crate-curio-0.0.2 (crate (name "curio") (vers "0.0.2") (deps (list (crate-dep (name "chunked_transfer") (req "^1.2.0") (default-features #t) (kind 0)) (crate-dep (name "rustls") (req "^0.18.1") (default-features #t) (kind 0)) (crate-dep (name "webpki") (req "^0.21.3") (default-features #t) (kind 0)) (crate-dep (name "webpki-roots") (req "^0.20.0") (default-features #t) (kind 0)))) (hash "16alf61nk8p1lyknlik0zpgr6pfw4rz927vda0xxa338fsfwfhci")))

(define-public crate-curio-0.0.3 (crate (name "curio") (vers "0.0.3") (deps (list (crate-dep (name "chunked_transfer") (req "^1.2.0") (default-features #t) (kind 0)) (crate-dep (name "rustls") (req "^0.18.1") (default-features #t) (kind 0)) (crate-dep (name "webpki") (req "^0.21.3") (default-features #t) (kind 0)) (crate-dep (name "webpki-roots") (req "^0.20.0") (default-features #t) (kind 0)))) (hash "0r9rwwxix6p1gc3rqajrjk2y7ww0lyn46xx0q40w004a6y4r4wgd")))

(define-public crate-curiosity-0.0.1 (crate (name "curiosity") (vers "0.0.1") (deps (list (crate-dep (name "cosmos") (req "^0.0.12") (default-features #t) (kind 0)) (crate-dep (name "docker") (req "^0.0.20") (default-features #t) (kind 0)) (crate-dep (name "rustc-serialize") (req "^0.3.12") (default-features #t) (kind 0)) (crate-dep (name "time") (req "^0.1.25") (default-features #t) (kind 0)))) (hash "1qjsypswjx8shqfph5k6khjnivl58lf98pbd138yjdvqwcpxhjly")))

