(define-module (crates-io cu -c) #:use-module (crates-io))

(define-public crate-cu-core-0.1 (crate (name "cu-core") (vers "0.1.0") (deps (list (crate-dep (name "bitflags") (req "^1.1.0") (default-features #t) (kind 0)) (crate-dep (name "cu-sys") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "error-chain") (req "^0.12.1") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "185wvivvhpxdydynkn65fkfz6f2d7mnha2c120im10jfrbk36ynh")))

