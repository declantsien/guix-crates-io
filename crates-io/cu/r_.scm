(define-module (crates-io cu r_) #:use-module (crates-io))

(define-public crate-cur_macro-0.1 (crate (name "cur_macro") (vers "0.1.0") (deps (list (crate-dep (name "proc-macro2") (req "^1.0.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.0") (features (quote ("full" "extra-traits"))) (default-features #t) (kind 0)))) (hash "1xv0k53bic9pnld15ldka9xj7xsk44zijswv1z6bbi1p2agwssxk")))

(define-public crate-cur_macro-0.2 (crate (name "cur_macro") (vers "0.2.0") (deps (list (crate-dep (name "proc-macro2") (req "^1.0.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.0") (features (quote ("full" "extra-traits"))) (default-features #t) (kind 0)))) (hash "0fix9xvwjcxl6mqcl2j1mrvfzzb12svnjnb94dki3ldpg5v5nd5f")))

(define-public crate-cur_macro-0.3 (crate (name "cur_macro") (vers "0.3.0") (deps (list (crate-dep (name "proc-macro2") (req "^1.0.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.0") (features (quote ("full" "extra-traits"))) (default-features #t) (kind 0)))) (hash "04awwgz8znsdw0z61a8k4hw6pilp9zgaascbvxyj7qvv5gmc7bzy")))

(define-public crate-cur_macro-0.4 (crate (name "cur_macro") (vers "0.4.0") (deps (list (crate-dep (name "proc-macro2") (req "^1.0.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.0") (features (quote ("full" "extra-traits"))) (default-features #t) (kind 0)))) (hash "1v9mcw71zihrp6rl1g4xjf4yl0hhjv5dhn931sls433sfv2vm90j")))

(define-public crate-cur_macro-0.5 (crate (name "cur_macro") (vers "0.5.0") (deps (list (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (default-features #t) (kind 0)))) (hash "1pzwdrd1hpav2p963zv8lybvw09mdzh1smkzrzfhmpgsjyp1q7v5")))

