(define-module (crates-io cu ru) #:use-module (crates-io))

(define-public crate-curuam-0.2 (crate (name "curuam") (vers "0.2.0") (hash "0y4cixw9wb80i95zr6a6hvxl089q5ijfvchbd1srrvqf1iq115xc")))

(define-public crate-curuam-0.2 (crate (name "curuam") (vers "0.2.2") (hash "0s27day7c2g5cy0c3c640wxy5dz8vnm2lzr7mliqp9p214q78rbj")))

(define-public crate-curuam-0.2 (crate (name "curuam") (vers "0.2.3") (hash "170ga88mqbyf1sv6iidharmm2204avapkxrb24x7y2299bq6awg3")))

