(define-module (crates-io cu ne) #:use-module (crates-io))

(define-public crate-cuneiform-0.0.0 (crate (name "cuneiform") (vers "0.0.0") (hash "0l76vy1bv8qqzn5nrlhlbf6bbvssvfiymjvg4q5sc9lkpdw5hdjs")))

(define-public crate-cuneiform-0.1 (crate (name "cuneiform") (vers "0.1.0") (deps (list (crate-dep (name "hashbrown") (req "^0.6") (default-features #t) (kind 0)) (crate-dep (name "heapless") (req "^0.5") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.4") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "trybuild") (req "^1.0") (default-features #t) (kind 2)) (crate-dep (name "walkdir") (req "^2.2") (default-features #t) (kind 0)))) (hash "136rnjqcnzp7zj4ypf2f8wpnc8pbnvjiyi88bbbsvmbp7ws47vdg")))

(define-public crate-cuneiform-0.1 (crate (name "cuneiform") (vers "0.1.1") (deps (list (crate-dep (name "lazy_static") (req "^1.4") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "trybuild") (req "^1.0") (default-features #t) (kind 2)) (crate-dep (name "walkdir") (req "^2.2") (default-features #t) (kind 0)))) (hash "1bp3viprrd39k9p4g0055431jvrspvprpxjqh3ckyfmrhhc8fldw")))

(define-public crate-cuneiform-fields-0.0.0 (crate (name "cuneiform-fields") (vers "0.0.0") (hash "1g05iwqsk3lmywrz3r77pbzf9vbgfizcdj9qdzvwcylzh6qnvqs6")))

(define-public crate-cuneiform-fields-0.1 (crate (name "cuneiform-fields") (vers "0.1.0") (deps (list (crate-dep (name "cuneiform") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "trybuild") (req "^1.0") (default-features #t) (kind 2)))) (hash "0c6fw4rm28y5k8vmcbpbd2f0hlx7m4s5r1s279zghcz5lnp2k9yh")))

(define-public crate-cuneiform-fields-0.1 (crate (name "cuneiform-fields") (vers "0.1.1") (deps (list (crate-dep (name "cuneiform") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "trybuild") (req "^1.0") (default-features #t) (kind 2)))) (hash "14mxzgx7ir23215fsj6vn10wjdf28njj340sjwglkp8m4mkmy2n6")))

