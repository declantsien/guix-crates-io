(define-module (crates-io cu ml) #:use-module (crates-io))

(define-public crate-cuml_map-0.1 (crate (name "cuml_map") (vers "0.1.0") (deps (list (crate-dep (name "num-traits") (req "^0.2") (default-features #t) (kind 0)))) (hash "0l2qgwsmyp287pmp8z45416ggp7wzh5gmi8jvibk2ii0c8hb6x1d")))

