(define-module (crates-io cu bo) #:use-module (crates-io))

(define-public crate-cubo-0.0.1 (crate (name "cubo") (vers "0.0.1") (deps (list (crate-dep (name "ferris-says") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "wasm-bindgen") (req "^0.2") (default-features #t) (kind 0)))) (hash "0d12n7s2pdbkrj9i0is6283sj8633c1rblxlhyx6w2myc58sbm4c")))

(define-public crate-cubob-0.1 (crate (name "cubob") (vers "0.1.0") (hash "1pifvyysjap8d6b43ihcypl2mm28vj5v3fs7d7hkqbn3cxcr9yw3")))

(define-public crate-cubob-0.2 (crate (name "cubob") (vers "0.2.0") (deps (list (crate-dep (name "maplit") (req "^1.0.2") (default-features #t) (kind 2)))) (hash "0v81ir4whsvfsfk92xx3s5yvm8c9169l91km4bw9lwsksginy6vd")))

(define-public crate-cubob-0.3 (crate (name "cubob") (vers "0.3.0") (deps (list (crate-dep (name "maplit") (req "^1.0.2") (default-features #t) (kind 2)))) (hash "0i7mbb9galxxffbs4xmhv2pvfn3c4cgg2w20ks2s7fk1nvzzkbja")))

(define-public crate-cubob-1 (crate (name "cubob") (vers "1.0.0") (deps (list (crate-dep (name "maplit") (req "^1.0.2") (default-features #t) (kind 2)))) (hash "1r4967r53dlmqalcd9ipy0sd7a8gr36pz4fww2h3ik15dz17mwcn")))

(define-public crate-cubob-1 (crate (name "cubob") (vers "1.1.0") (deps (list (crate-dep (name "maplit") (req "^1.0.2") (default-features #t) (kind 2)))) (hash "14jyq1jgzylrjkdh5dqgfhc84p336cazyalbmgijp478vxxgmlq6")))

(define-public crate-cubob-1 (crate (name "cubob") (vers "1.2.0") (deps (list (crate-dep (name "maplit") (req "^1.0.2") (default-features #t) (kind 2)))) (hash "0ma9284pq64h8h1h7ha6ak0g42kggcgnzjfaj9y1f78iimvnn0l2") (features (quote (("struct") ("list") ("instant") ("field") ("embed") ("default" "field" "list" "struct"))))))

(define-public crate-cubob-1 (crate (name "cubob") (vers "1.3.0") (deps (list (crate-dep (name "maplit") (req "^1.0.2") (default-features #t) (kind 2)))) (hash "0da1d22avzy1hp9cpy2y4wqd54rsssr6w420qd4g3h67y03f9w5v") (features (quote (("struct") ("list") ("instant") ("field") ("embed") ("default" "field" "list" "struct"))))))

(define-public crate-cubob-1 (crate (name "cubob") (vers "1.3.1") (deps (list (crate-dep (name "maplit") (req "^1.0.2") (default-features #t) (kind 2)))) (hash "0xwriqd3q88a909xhb0cicfqpizfjzpj2bkqqqh77div6x87d1lw") (features (quote (("struct") ("list") ("instant") ("field") ("embed") ("default" "field" "list" "struct"))))))

(define-public crate-cubob-1 (crate (name "cubob") (vers "1.3.2") (deps (list (crate-dep (name "maplit") (req "^1.0.2") (default-features #t) (kind 2)))) (hash "07kmn3wr3s8a4aj48h0r4ij2y0vbja6v7h7vby3rqi7bpwjjw2h8") (features (quote (("struct") ("list") ("instant") ("field") ("embed") ("default" "field" "list" "struct"))))))

(define-public crate-cuboid-0.1 (crate (name "cuboid") (vers "0.1.0") (deps (list (crate-dep (name "gl") (req "^0.14.0") (default-features #t) (kind 0)) (crate-dep (name "glfw") (req "^0.43.0") (default-features #t) (kind 0)))) (hash "1xgcj7r26r9jzs990hqhy35b1c3z4dqkzr155yzz8xg7fgkg1v3q")))

(define-public crate-cubox-0.2 (crate (name "cubox") (vers "0.2.0") (deps (list (crate-dep (name "chrono") (req "^0.4") (features (quote ("serde"))) (default-features #t) (kind 0)) (crate-dep (name "colored") (req "^2.0.0") (default-features #t) (kind 0)) (crate-dep (name "dirs") (req "^4.0.0") (default-features #t) (kind 0)) (crate-dep (name "getopts") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.11") (features (quote ("blocking" "json"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)))) (hash "0zk55r56jbrbsis99wvsz9xf11az0x7d51vr1h5967nk7jczs5jm")))

