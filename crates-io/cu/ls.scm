(define-module (crates-io cu ls) #:use-module (crates-io))

(define-public crate-culsynth-0.1 (crate (name "culsynth") (vers "0.1.0") (deps (list (crate-dep (name "fixed") (req "^1.24") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2") (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (features (quote ("small_rng"))) (default-features #t) (kind 0)) (crate-dep (name "tinyvec") (req "^1.6.0") (features (quote ("rustc_1_40"))) (default-features #t) (kind 0)))) (hash "0xnwm9255yfkymc7ya9qk9jp077s1pl1jc8s6p85p6p6rq6apsms") (features (quote (("libm" "num-traits/libm"))))))

(define-public crate-culsynth-0.2 (crate (name "culsynth") (vers "0.2.0") (deps (list (crate-dep (name "arrayvec") (req "^0.7.4") (kind 0)) (crate-dep (name "fixed") (req "^1.24") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2") (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (features (quote ("small_rng"))) (kind 0)))) (hash "1k5apdbrc8hpjy9lwn3ikjnwr3wmcbb10ypi23mzmqwdfndwgvcg") (features (quote (("rand_defaults" "rand/default") ("libm" "num-traits/libm"))))))

