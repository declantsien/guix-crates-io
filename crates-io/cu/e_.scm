(define-module (crates-io cu e_) #:use-module (crates-io))

(define-public crate-cue_sheet-0.1 (crate (name "cue_sheet") (vers "0.1.0") (deps (list (crate-dep (name "error-chain") (req "^0.10.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^0.2.2") (default-features #t) (kind 0)))) (hash "15241n19cjrlmjn1g7xkxac4cz1r5wz7z3v8s53gy1rdd9bln2hf")))

(define-public crate-cue_sheet-0.2 (crate (name "cue_sheet") (vers "0.2.0") (deps (list (crate-dep (name "error-chain") (req "^0.10.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^0.2.2") (default-features #t) (kind 0)))) (hash "02dggq8yc9gv72mgrbf83a4v076v6yh1mggs35hwda935d984vfh")))

(define-public crate-cue_sheet-0.3 (crate (name "cue_sheet") (vers "0.3.0") (deps (list (crate-dep (name "error-chain") (req "^0.12.0") (default-features #t) (kind 0)))) (hash "096vd0hdlkgq4mvizgbj8zmncx1isvknp01qm1405mcxk3gnr5ha")))

