(define-module (crates-io cu re) #:use-module (crates-io))

(define-public crate-cure-0.1 (crate (name "cure") (vers "0.1.0") (hash "0xfcrg00y8n4w84035ywcfafx90wpfr85099iby4ybwxya9cf73x")))

(define-public crate-curerr-0.1 (crate (name "curerr") (vers "0.1.0") (hash "03bb1kfjh81mwafsmil9dfbv0v22ddx2kwib1hkj03rn4kjq1xzc")))

(define-public crate-curerr-0.1 (crate (name "curerr") (vers "0.1.1") (hash "1y1c60v2vghhz1kwn5di02ryhvixsk0i9gw4h5ksfm103pgf3v3y") (yanked #t)))

(define-public crate-curerr-1 (crate (name "curerr") (vers "1.0.0") (hash "1pim3piw4h8261vyna97j86xb3n3jsxlvw8v7lq7sm8cn4gyjlk1") (yanked #t)))

(define-public crate-curerr-1 (crate (name "curerr") (vers "1.0.1") (hash "01774aba593p292krw74sh5n9k4jsmp76r1b2ihq3pfzr33d3i9n")))

(define-public crate-curerr-1 (crate (name "curerr") (vers "1.0.3") (hash "0dybfc5nkgpqngqjiqc3rh911n9sviq9shyn4iki9p6i8qih77r3")))

(define-public crate-curerr-1 (crate (name "curerr") (vers "1.0.6") (hash "1l87dcfggjg3d73b7dqmpppf5jl0jgkyk02icd8kccfqr2g7zg4m") (yanked #t)))

(define-public crate-curerr-1 (crate (name "curerr") (vers "1.0.7") (hash "1ssyvbasbrlr0f09rqvpmxpf7lp8ppwiva320w33b1rasja2wgmm")))

(define-public crate-curerr-1 (crate (name "curerr") (vers "1.1.0") (hash "17q87xh46wb5gx0jz9k0fh1k9pfnyygzrp18cjmarb49gzvdgbym")))

