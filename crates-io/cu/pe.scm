(define-module (crates-io cu pe) #:use-module (crates-io))

(define-public crate-cupertino-0.0.0 (crate (name "cupertino") (vers "0.0.0") (hash "0hhla1vw3jfmdj2ds9g4yvi1w6j30vwfcn3afam4ydp0d8c1z6gi")))

(define-public crate-cupertino-derive-0.0.0 (crate (name "cupertino-derive") (vers "0.0.0") (hash "0shqc2frparjxbyx35sw7i95bww2fxrr33fxmav35rrk7fwj2y6q")))

(define-public crate-cupertino-rt-0.1 (crate (name "cupertino-rt") (vers "0.1.0") (hash "0c5zwh3ng6fg4zb58r92q35nypk9c9qp9ykzs9bd118vr6a7677k") (yanked #t)))

(define-public crate-cupertino-sys-0.1 (crate (name "cupertino-sys") (vers "0.1.0") (hash "0yf59m22bpbiy5z9fs5jl5hcgdz8ybzk870hgsflc4wc330fm01y") (yanked #t)))

(define-public crate-cupertino-sys-0.0.0 (crate (name "cupertino-sys") (vers "0.0.0") (hash "1dj8i30rxixhd9aqfaqi6i0g2wwd4g0x5aqcwfd3ilqys3y551r5")))

