(define-module (crates-io cu di) #:use-module (crates-io))

(define-public crate-cudi-0.1 (crate (name "cudi") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^4.5.4") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "colored") (req "^2.1.0") (default-features #t) (kind 0)) (crate-dep (name "cust") (req "^0.3.2") (default-features #t) (kind 0)))) (hash "0nc55is9csb2r0i55c3irlfyzbhzky5b6hpjp5bb9ysd54c4j4zj")))

