(define-module (crates-io cu ra) #:use-module (crates-io))

(define-public crate-cura-0.1 (crate (name "cura") (vers "0.1.0") (hash "0l9d1avgfs39xippxg27gvb544n5adm6wag6pgjdb9j2pgkldbjh")))

(define-public crate-cura-0.2 (crate (name "cura") (vers "0.2.0") (hash "18g7w08sl4bbvczswx142k96pfmr8bn6h67p3wbxks3khknfy8cq")))

(define-public crate-cura-0.3 (crate (name "cura") (vers "0.3.0") (hash "1dqc9lf0dmgkf0qzvf4l54bbv5zl0scdinc4ifnzpzrn04yjrna4")))

(define-public crate-cura-0.4 (crate (name "cura") (vers "0.4.0") (hash "1mlcbx229ij7p3b6dg5gp7ss6xmj1ilhvpwal408izqf07zjw2cl")))

(define-public crate-cura-0.4 (crate (name "cura") (vers "0.4.1") (hash "0w0il1hvqwyhw8318qv61s4hv6i8kc9zzcjfzxkx9lgk03x0cxhy")))

(define-public crate-cura-0.4 (crate (name "cura") (vers "0.4.2") (hash "0sn9dhm0pvfc7106s85716gw59ygzifcbm7wxyl8z4xclfmbhv6i")))

(define-public crate-cura-0.5 (crate (name "cura") (vers "0.5.0") (hash "0i9fd0gd0mi87wszcnm9qj4w9srq6r2ygh37b47canay98payxjs")))

(define-public crate-cura-0.6 (crate (name "cura") (vers "0.6.0") (hash "0q4bkzs2hzdfa30zj78vhvyxji4v3jzpyd6h9b0bayiyya3bvpdv")))

(define-public crate-cura-0.6 (crate (name "cura") (vers "0.6.1") (hash "0pxsv6x6k7i3a7wc944sip3l88qlmy6kik6dlysbjf2zp68ysr9w")))

(define-public crate-cura-0.7 (crate (name "cura") (vers "0.7.0") (hash "0pzp1863qnar8sy7f2cjc07w5xa5rwlvnwbw8aqd2b0ww0bbhn53")))

(define-public crate-cura-0.7 (crate (name "cura") (vers "0.7.1") (hash "091x4w30kq0ya3n60kgijm6smsfknsyx8kw4qlnwl83n63qgmssv")))

(define-public crate-cura-0.7 (crate (name "cura") (vers "0.7.2") (hash "1h6jmjnbiszhf6zmv6aisp5qmavn53fmn2lzh25x1i8gwv2svm49")))

(define-public crate-cura-0.8 (crate (name "cura") (vers "0.8.0") (hash "1ls7d3jisn8pb8vhlzxvvsagk699kjs2kyi3yv743wahvnfcakc4")))

(define-public crate-cural-0.1 (crate (name "cural") (vers "0.1.0") (deps (list (crate-dep (name "windows") (req "^0.48") (features (quote ("Win32_System_Diagnostics_ToolHelp" "Win32_System_Diagnostics_Debug" "Win32_System_Threading" "Win32_Foundation"))) (default-features #t) (kind 0)))) (hash "0hyhmf48ilfyc9sclpdhmj3x4rinv2bsj4nbcz62vk6lylcbmmh7")))

(define-public crate-cural-0.1 (crate (name "cural") (vers "0.1.1") (deps (list (crate-dep (name "windows") (req "^0.48") (features (quote ("Win32_System_Diagnostics_ToolHelp" "Win32_System_Diagnostics_Debug" "Win32_System_Threading" "Win32_Foundation"))) (default-features #t) (kind 0)))) (hash "18zycsv4bb1igj0zabcz3mafawwix3yckajq1xriw8aj7kjwxdlc")))

(define-public crate-cural-0.1 (crate (name "cural") (vers "0.1.3") (deps (list (crate-dep (name "winapi") (req "^0.3.9") (features (quote ("tlhelp32" "handleapi" "memoryapi" "wow64apiset" "processthreadsapi"))) (default-features #t) (kind 0)))) (hash "0k2vxcf5l543rda7c01mbymw9jmnj6w0vh190xycrilm3zcw93mg")))

(define-public crate-curator-0.1 (crate (name "curator") (vers "0.1.0") (hash "0rra0s298pl0sy1fa7lc4nfyrqryym2bniiz4nk6ncdfsjbqs10v")))

