(define-module (crates-io cu rb) #:use-module (crates-io))

(define-public crate-curb-0.1 (crate (name "curb") (vers "0.1.0") (deps (list (crate-dep (name "hwloc") (req "^0.5") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.2") (default-features #t) (kind 0)))) (hash "0nablg8j7km2h18lk3cs2s1n1z9c9wyp9ji3kgnq4yxvc0vgx54w")))

(define-public crate-curb-0.1 (crate (name "curb") (vers "0.1.1") (deps (list (crate-dep (name "hwloc") (req "^0.5") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.2") (default-features #t) (kind 0)))) (hash "0i6i67fvqlkdb7nwd0z9kps74adb05nagfb7g2r4xn8628dy1b4d")))

(define-public crate-curb-0.1 (crate (name "curb") (vers "0.1.2") (deps (list (crate-dep (name "hwloc") (req "^0.5") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.6") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.2") (default-features #t) (kind 0)))) (hash "0pizvd64crz0s0pfsfbza21fc7lil5bxcwik2f2fbfhbzr463gxq")))

(define-public crate-curb-0.1 (crate (name "curb") (vers "0.1.3") (deps (list (crate-dep (name "hwloc") (req "^0.5") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.6") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.2") (default-features #t) (kind 0)))) (hash "1cspryi58k9nq4hpaijmd32pay1a22wyr25is8ms8xbqvp6bqk1p")))

(define-public crate-curb-0.1 (crate (name "curb") (vers "0.1.4") (deps (list (crate-dep (name "hwloc") (req "^0.5") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.6") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.2") (default-features #t) (kind 0)))) (hash "034rf8h53rimilzzbhdqsll9981s9w88kpic46py33b85yayk8wf")))

(define-public crate-curb-0.1 (crate (name "curb") (vers "0.1.5") (deps (list (crate-dep (name "hwloc") (req "^0.5") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.6") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.2") (default-features #t) (kind 0)))) (hash "01m7m2kjnd1v79wgbl3qr7mcqrr7dj6300dh6y8dqw0j7hc18vb4")))

(define-public crate-curb-0.1 (crate (name "curb") (vers "0.1.6") (deps (list (crate-dep (name "hwloc") (req "^0.5") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.7") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.3") (default-features #t) (kind 0)))) (hash "19p1jzk99c3rzsnip4b9370q9ih4i1ss65schmh3qw7f7hv7wh0v")))

