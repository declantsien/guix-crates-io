(define-module (crates-io by st) #:use-module (crates-io))

(define-public crate-bystr-0.1 (crate (name "bystr") (vers "0.1.0") (deps (list (crate-dep (name "bystr-impl") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "proc-macro-hack") (req "^0.5.11") (default-features #t) (kind 0)))) (hash "12121drzrd5mwgap0yqbg3gr2h81rlmgdxpk1xq0dnm7k9hdqrj4")))

(define-public crate-bystr-0.1 (crate (name "bystr") (vers "0.1.1") (deps (list (crate-dep (name "bystr-impl") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "proc-macro-hack") (req "^0.5.11") (default-features #t) (kind 0)))) (hash "035n7vfi9grdf696vvig4fqvs10x847z7gdn0sp8pd273bycqzhn")))

(define-public crate-bystr-0.1 (crate (name "bystr") (vers "0.1.2") (deps (list (crate-dep (name "bystr-impl") (req "^0.1.2") (default-features #t) (kind 0)) (crate-dep (name "proc-macro-hack") (req "^0.5.11") (default-features #t) (kind 0)))) (hash "0sr2h86jq5gj4dvi1bs22rpi3jxy402zacfl4vn9nhs21j1ri3r2")))

(define-public crate-bystr-0.1 (crate (name "bystr") (vers "0.1.3") (deps (list (crate-dep (name "bystr-impl") (req "^0.1.3") (default-features #t) (kind 0)) (crate-dep (name "proc-macro-hack") (req "^0.5.11") (default-features #t) (kind 0)))) (hash "13sv7yql9fp1dqxrazajqpyvab0lp2gi0qb8lnhd1kg4zam7zdny")))

(define-public crate-bystr-impl-0.1 (crate (name "bystr-impl") (vers "0.1.0") (deps (list (crate-dep (name "proc-macro-hack") (req "^0.5.11") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.2") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.8") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "042hyfij829kvg9a2cz57g4nqqf3yk3w364dckl01pgjip3lrqrg")))

(define-public crate-bystr-impl-0.1 (crate (name "bystr-impl") (vers "0.1.2") (deps (list (crate-dep (name "proc-macro-hack") (req "^0.5.11") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.2") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.8") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "0rs52d95xn9z7a88fljq41yfnvb4brxc5035472q9cjrsii8zbwv")))

(define-public crate-bystr-impl-0.1 (crate (name "bystr-impl") (vers "0.1.3") (deps (list (crate-dep (name "proc-macro-hack") (req "^0.5.11") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.2") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.8") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "0bfsl5kjmxwf3pvnbv94q3938srxlygh6zlj9ijc8zwx2nxxpvi3")))

