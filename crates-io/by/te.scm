(define-module (crates-io by te) #:use-module (crates-io))

(define-public crate-byte-0.2 (crate (name "byte") (vers "0.2.0") (deps (list (crate-dep (name "byteorder") (req "^1.0.0") (default-features #t) (kind 2)) (crate-dep (name "quickcheck") (req "^0.3") (default-features #t) (kind 2)))) (hash "0l5ddazgz3mjwmkc4zh5nksn4p1x9cj1n1f41hlx8ajakj6z3yfd")))

(define-public crate-byte-0.2 (crate (name "byte") (vers "0.2.1") (deps (list (crate-dep (name "byteorder") (req "^1.0.0") (default-features #t) (kind 2)) (crate-dep (name "quickcheck") (req "^0.3") (default-features #t) (kind 2)))) (hash "132f0y2p56fm52yy82ddcc5znlpsv99gqjsi6vi89xdbq5qnfpld")))

(define-public crate-byte-0.2 (crate (name "byte") (vers "0.2.2") (deps (list (crate-dep (name "byteorder") (req "^1.0.0") (default-features #t) (kind 2)) (crate-dep (name "quickcheck") (req "^0.3") (default-features #t) (kind 2)))) (hash "1jyrr87qlhbnl0dmxjskhm028psxryhd6ds4aa8rila6zv1yh3az")))

(define-public crate-byte-0.2 (crate (name "byte") (vers "0.2.3") (deps (list (crate-dep (name "byteorder") (req "^1.0.0") (default-features #t) (kind 2)) (crate-dep (name "quickcheck") (req "^0.3") (default-features #t) (kind 2)))) (hash "0jia8fs79kgnky1scqysjjfmjxrnslazjdbyd1975gc85ixl6pk6")))

(define-public crate-byte-0.2 (crate (name "byte") (vers "0.2.4") (deps (list (crate-dep (name "byteorder") (req "^1.0.0") (default-features #t) (kind 2)) (crate-dep (name "quickcheck") (req "^0.3") (default-features #t) (kind 2)))) (hash "112kx47awcd5lzc4mx1lay7dsmwx81b99j46dnccz1lgk658vzsf")))

(define-public crate-byte-0.2 (crate (name "byte") (vers "0.2.5") (deps (list (crate-dep (name "byteorder") (req "^1.0.0") (default-features #t) (kind 2)) (crate-dep (name "quickcheck") (req "^0.3") (default-features #t) (kind 2)))) (hash "0iiq0hfkzp5zd82p12g4vp961jx0nmmkvij3jvwk24jbzq2s15w9")))

(define-public crate-byte-0.2 (crate (name "byte") (vers "0.2.6") (deps (list (crate-dep (name "byteorder") (req "^1.0.0") (default-features #t) (kind 2)) (crate-dep (name "quickcheck") (req "^0.3") (default-features #t) (kind 2)))) (hash "00dxx93n0l8x2cg2v7mcq7aksf92ga43x05ryk5bzdichrdlmzbw")))

(define-public crate-byte-0.2 (crate (name "byte") (vers "0.2.7") (deps (list (crate-dep (name "byteorder") (req "^1.0.0") (default-features #t) (kind 2)) (crate-dep (name "quickcheck") (req "^0.3") (default-features #t) (kind 2)))) (hash "1k57i2h2ahqrcp8vhd1m6z604p52vw2dkp6wz33m627898zapir1")))

(define-public crate-byte-aes-0.1 (crate (name "byte-aes") (vers "0.1.0") (deps (list (crate-dep (name "aes") (req "^0.8.3") (default-features #t) (kind 0)))) (hash "02in2gazfr2iw7a74plbggqh8r70bfbnfgyd8x8x5nac692d7zyk")))

(define-public crate-byte-aes-0.1 (crate (name "byte-aes") (vers "0.1.1") (deps (list (crate-dep (name "aes") (req "^0.8.3") (default-features #t) (kind 0)))) (hash "1zcv5bjhxpfglkdizc9g2v8qj6afpsisq9awirjxwawhibpaxfcq")))

(define-public crate-byte-aes-0.1 (crate (name "byte-aes") (vers "0.1.2") (deps (list (crate-dep (name "aes") (req "^0.8.3") (default-features #t) (kind 0)))) (hash "0islqrwppjlbglbg3s6hvcr8cmrxda5m6ng8yp5iq0nxsgmzn8cg")))

(define-public crate-byte-aes-0.2 (crate (name "byte-aes") (vers "0.2.0") (deps (list (crate-dep (name "aes") (req "^0.8.3") (default-features #t) (kind 0)))) (hash "1nhh9r4pmng8p7n1ywhbjbiiljxsq2z0dw50si6sc2238wiq7h78")))

(define-public crate-byte-aes-0.2 (crate (name "byte-aes") (vers "0.2.1") (deps (list (crate-dep (name "aes") (req "^0.8.3") (default-features #t) (kind 0)))) (hash "1i9hv1xh0kx9m41kq72khvy269wq36lkf0x96kib07pqgfwgd4h3") (yanked #t)))

(define-public crate-byte-aes-0.2 (crate (name "byte-aes") (vers "0.2.2") (deps (list (crate-dep (name "aes") (req "^0.8.3") (default-features #t) (kind 0)))) (hash "0y2abqszbc5nrkvsndn93ab9rr1ah2rafxcqmxc317bf81gkd0a6")))

(define-public crate-byte-arena-0.1 (crate (name "byte-arena") (vers "0.1.0") (hash "0ax4gqqz7a2430zhycpknryh3m94jf63bd35jl6bzpd0dps71mga") (features (quote (("zero_headers") ("default" "zero_headers"))))))

(define-public crate-byte-arena-0.1 (crate (name "byte-arena") (vers "0.1.1") (hash "1wqzl6f3xagq8zk4pfqw8ya7k4axj7zz65pdyrxqbjmby6kfca4x") (features (quote (("zero_headers") ("default" "zero_headers"))))))

(define-public crate-byte-arena-0.1 (crate (name "byte-arena") (vers "0.1.2") (hash "1riv5pnq0xgn3gp630p97n82dv7yvpvyf82wiad5h62mzjkqsxfk") (features (quote (("zero_headers") ("default" "zero_headers"))))))

(define-public crate-byte-arena-0.2 (crate (name "byte-arena") (vers "0.2.0") (hash "1ll4hmaj3zdsbnagwvazpwvaqgvhmvznhmxzcl4wnz3nqvbl1bff") (features (quote (("zero_headers") ("default"))))))

(define-public crate-byte-array-literals-0.0.0 (crate (name "byte-array-literals") (vers "0.0.0") (hash "1rx0aj4irxzw72sgrqaan6kighj6f70jcmy7az98q7xk0q6kkx9b")))

(define-public crate-byte-array-struct-0.1 (crate (name "byte-array-struct") (vers "0.1.0") (deps (list (crate-dep (name "hex") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (optional #t) (default-features #t) (kind 0)))) (hash "1k7a4z18kgplkv66passpykxa9sm83pwn6pn1cvxyd7wpdsppdxc") (features (quote (("serialize" "serde" "serde_json") ("default" "serialize"))))))

(define-public crate-byte-array-struct-0.2 (crate (name "byte-array-struct") (vers "0.2.0") (deps (list (crate-dep (name "hex") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (optional #t) (default-features #t) (kind 0)))) (hash "1q63nsg0ai9sh1h2xwabiy8gsmrgjql4389sz6bwn2rq4snll568") (features (quote (("with-serde" "serde" "serde_json") ("default"))))))

(define-public crate-byte-enum-0.1 (crate (name "byte-enum") (vers "0.1.1") (deps (list (crate-dep (name "byte-enum-derive") (req "^0.1.1") (default-features #t) (kind 0)))) (hash "00y7ii8ad99jxpsfa675rhh3af0knrv7jba5w2flkpy9m5yg67fl") (features (quote (("external_doc")))) (yanked #t)))

(define-public crate-byte-enum-0.1 (crate (name "byte-enum") (vers "0.1.2") (deps (list (crate-dep (name "byte-enum-derive") (req "^0.1.2") (default-features #t) (kind 0)))) (hash "1cmlqw7dy1g5k5f049758zzgd405ic0a8xs2h5wlw3k69fbrmksj") (yanked #t)))

(define-public crate-byte-enum-derive-0.1 (crate (name "byte-enum-derive") (vers "0.1.0") (deps (list (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (default-features #t) (kind 0)))) (hash "1ykn99jjw58cd2q9512nxxkhbpk4dq2cy7l2jva96kinhlfhxaw7") (features (quote (("external_doc")))) (yanked #t)))

(define-public crate-byte-enum-derive-0.1 (crate (name "byte-enum-derive") (vers "0.1.1") (deps (list (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (default-features #t) (kind 0)))) (hash "0j7rf3qgq4bh0rf7pbw8p2apxbyl34r571jnchv690q27f9amd0j") (features (quote (("external_doc")))) (yanked #t)))

(define-public crate-byte-enum-derive-0.1 (crate (name "byte-enum-derive") (vers "0.1.2") (deps (list (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (default-features #t) (kind 0)))) (hash "150c73mnp05kcwlyrxp0hnb83d4yln73ma6rxd0r86wvac712ki5") (yanked #t)))

(define-public crate-byte-escape-0.0.1 (crate (name "byte-escape") (vers "0.0.1") (hash "1mbxdzv7zx8kl0wfkyzbkdchfhsazyc4xaksl5q91i3rv1hnp724")))

(define-public crate-byte-fmt-0.1 (crate (name "byte-fmt") (vers "0.1.0") (deps (list (crate-dep (name "abe") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "base64") (req "^0.13.0") (default-features #t) (kind 0)) (crate-dep (name "ruint") (req "^1.7.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.139") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_bytes") (req "^0.11.6") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.82") (default-features #t) (kind 2)) (crate-dep (name "tracing") (req "^0.1.35") (default-features #t) (kind 0)))) (hash "0dwyg2lv1wxv9m7qn4lrnqk1hacbvfjqnzkabl47rbkv2c1jikd9")))

(define-public crate-byte-fmt-0.3 (crate (name "byte-fmt") (vers "0.3.0-rc1") (deps (list (crate-dep (name "abe") (req "^0.3.0-rc1") (default-features #t) (kind 0)) (crate-dep (name "base64") (req "=0.21") (default-features #t) (kind 0)) (crate-dep (name "bytemuck") (req "^1.13.1") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "ruint") (req "^1.7.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "=1.0.160") (default-features #t) (kind 0)) (crate-dep (name "serde_bytes") (req "^0.11.6") (default-features #t) (kind 0)) (crate-dep (name "tracing") (req "=0.1.37") (features (quote ("release_max_level_info"))) (default-features #t) (kind 0)))) (hash "0bgsnnfsqdkkpvn4xizwnwbp5w46b3pq5ffw97q5f5lsx8hxmyhr")))

(define-public crate-byte-io-0.1 (crate (name "byte-io") (vers "0.1.0") (hash "10gqh5a63hbh997zxnvixwg0qlnm6m27hmpm1i0qgjslsl7bh6f4")))

(define-public crate-byte-io-0.1 (crate (name "byte-io") (vers "0.1.1") (hash "19cn67rzk4ana4jc5my4w5k6cv5rd814r9l03ycr2x9vwwczk3x6")))

(define-public crate-byte-mutator-0.1 (crate (name "byte-mutator") (vers "0.1.0") (deps (list (crate-dep (name "arrayvec") (req "^0.5.1") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.102") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0.102") (default-features #t) (kind 0)) (crate-dep (name "toml") (req "^0.5.5") (default-features #t) (kind 0)))) (hash "0555w62i9dmlayrgrkg8rwl5n3jy6bbjmfkvywwzl7bllfcf0507")))

(define-public crate-byte-mutator-0.1 (crate (name "byte-mutator") (vers "0.1.1") (deps (list (crate-dep (name "arrayvec") (req "^0.5.1") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.102") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0.102") (default-features #t) (kind 0)) (crate-dep (name "toml") (req "^0.5.5") (default-features #t) (kind 0)))) (hash "0nnh3x0ai6zpwnfkvqbq274mx7rdiz098yqhh7j9iyksf489ikva")))

(define-public crate-byte-mutator-0.1 (crate (name "byte-mutator") (vers "0.1.2") (deps (list (crate-dep (name "arrayvec") (req "^0.5.1") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.102") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0.102") (default-features #t) (kind 0)) (crate-dep (name "toml") (req "^0.5.5") (default-features #t) (kind 0)))) (hash "1dbkb1wgr2d1gj3cal6qlnd5pichz75ill42m226nh36n7g65mrz")))

(define-public crate-byte-mutator-0.2 (crate (name "byte-mutator") (vers "0.2.0") (deps (list (crate-dep (name "arrayvec") (req "^0.5.1") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.102") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0.102") (default-features #t) (kind 0)) (crate-dep (name "toml") (req "^0.5.5") (default-features #t) (kind 0)))) (hash "19pi3zhzhq4g4xzpl01y7rb4455g74nbvk9pgcb4s6yz8sa5phlr")))

(define-public crate-byte-num-0.1 (crate (name "byte-num") (vers "0.1.0") (hash "05b3yi2sdc3gsng16368zbjgbjn4wvzjnssvrpzs7kxl4wzk45dg") (features (quote (("nightly"))))))

(define-public crate-byte-num-0.1 (crate (name "byte-num") (vers "0.1.1") (hash "0rlwsmc355f5akmly9kx6vf659m6dsbvgwvh8bq39140fnxbvh2a") (features (quote (("nightly"))))))

(define-public crate-byte-num-0.1 (crate (name "byte-num") (vers "0.1.2") (hash "07hm5g43p984n1cmwkpnavbg239b3vdkrq64lrm01aqf2q5abbcm") (features (quote (("nightly"))))))

(define-public crate-byte-num-0.1 (crate (name "byte-num") (vers "0.1.3") (hash "1jzgc5nrxhr9wkhpbjrialr1pv99cd953fzq86n11d2bzdwa3rgc") (features (quote (("nightly"))))))

(define-public crate-byte-order-0.1 (crate (name "byte-order") (vers "0.1.0") (deps (list (crate-dep (name "docgen") (req "^0.1.2") (default-features #t) (kind 0)))) (hash "02vpcv4arxsgj8iylvggr00d0ndhh4qhi8m799gkx0nhm54lhmkb")))

(define-public crate-byte-order-0.2 (crate (name "byte-order") (vers "0.2.0") (hash "1cqcrkdk4spza0b7l898p97avv05in99049f9c58xa92znmr8ypg")))

(define-public crate-byte-order-0.3 (crate (name "byte-order") (vers "0.3.0") (hash "1x6hkfpsk2mzijfknjcp9hn2zs3s6c0rlq54mmwmcjpk9cza28dh")))

(define-public crate-byte-parser-0.2 (crate (name "byte-parser") (vers "0.2.0") (hash "0kyqdnhp4ql1nvb0wj26k8l5njdm84s6k470dl9f1shdn98q270s")))

(define-public crate-byte-parser-0.2 (crate (name "byte-parser") (vers "0.2.1") (hash "01bbk8pnc9lbs8ny19qvjzigddwwmxqxfmirgl0ymdsia8gwviiq") (features (quote (("unstable-parse-iter"))))))

(define-public crate-byte-parser-0.2 (crate (name "byte-parser") (vers "0.2.2") (hash "1r20h92shfgg52w18s6zkz4yyfsxm87439md9kjq2fc8rr0irynq") (features (quote (("unstable-parse-iter"))))))

(define-public crate-byte-parser-0.2 (crate (name "byte-parser") (vers "0.2.3") (hash "03vmlvlrqa1xqlyva6jgv7cmv303v7x49zimj2c9c8mjgla8ncmm") (features (quote (("unstable-parse-iter")))) (rust-version "1.56")))

(define-public crate-byte-pool-0.1 (crate (name "byte-pool") (vers "0.1.0") (deps (list (crate-dep (name "stable_deref_trait") (req "^1.1.1") (optional #t) (default-features #t) (kind 0)))) (hash "0vvsz3zkx1x5rc9cw1sci289p6kpzniyvf1hx5c48kzxylrxp6p4") (features (quote (("stable_deref" "stable_deref_trait") ("default" "stable_deref"))))))

(define-public crate-byte-pool-0.2 (crate (name "byte-pool") (vers "0.2.0") (deps (list (crate-dep (name "crossbeam-queue") (req "^0.2.0") (default-features #t) (kind 0)))) (hash "1xr3rdc9zx8h1xjn8clzqhc1b4kw132pf8za0s5mfhkpj04nzm2a") (features (quote (("default"))))))

(define-public crate-byte-pool-0.2 (crate (name "byte-pool") (vers "0.2.1") (deps (list (crate-dep (name "crossbeam-queue") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "stable_deref_trait") (req "^1.1.1") (default-features #t) (kind 0)))) (hash "08g2pbhcm4d79i6kqrjvh32hq2y4f82px6nzpsgqgcf8x81f2hlk") (features (quote (("default"))))))

(define-public crate-byte-pool-0.2 (crate (name "byte-pool") (vers "0.2.2") (deps (list (crate-dep (name "crossbeam-queue") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "stable_deref_trait") (req "^1.1.1") (default-features #t) (kind 0)))) (hash "0yh96ral0pni02bzm3fhvicp1ixz1hz3c5m03hsyq66mk61fjf0y") (features (quote (("default"))))))

(define-public crate-byte-pool-0.2 (crate (name "byte-pool") (vers "0.2.3") (deps (list (crate-dep (name "crossbeam-queue") (req "^0.3.1") (default-features #t) (kind 0)) (crate-dep (name "stable_deref_trait") (req "^1.1.1") (default-features #t) (kind 0)))) (hash "1jphxkfbqyg20swfwy1i6cv4vxgkk4d84za79l4v29xlvc6j7izq") (features (quote (("default"))))))

(define-public crate-byte-pool-0.2 (crate (name "byte-pool") (vers "0.2.4") (deps (list (crate-dep (name "crossbeam-queue") (req "^0.3.1") (default-features #t) (kind 0)) (crate-dep (name "stable_deref_trait") (req "^1.1.1") (default-features #t) (kind 0)))) (hash "0n3iibkqk5lyhwmxz0imwz1gsk4xbvs7q8m6xwjmc2zmi48v5wf2") (features (quote (("default"))))))

(define-public crate-byte-sequence-0.1 (crate (name "byte-sequence") (vers "0.1.0") (deps (list (crate-dep (name "error-chain") (req "^0.11") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (default-features #t) (kind 0)))) (hash "1i3yrgjyvgg6pz7gsgsahr7ci5n6zhm0wvaylki56d4q42ndwzvl")))

(define-public crate-byte-sequence-0.2 (crate (name "byte-sequence") (vers "0.2.0") (deps (list (crate-dep (name "failure") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.5") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (default-features #t) (kind 0)))) (hash "0vxj17c3z2lkyr4924fhh4d19avxxbvcaiqrs8xq66klvzjja8rs")))

(define-public crate-byte-sequence-0.2 (crate (name "byte-sequence") (vers "0.2.1") (deps (list (crate-dep (name "failure") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.5") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (default-features #t) (kind 0)))) (hash "1c1s4hdl698vqkkzm4w6gbmrqn3awxgx1lv3hvrabif55qgyk8p3")))

(define-public crate-byte-sequence-0.2 (crate (name "byte-sequence") (vers "0.2.2") (deps (list (crate-dep (name "failure") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.5") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (default-features #t) (kind 0)))) (hash "0w32h5kh4lksfwwyqfga88kz641j32w165ms2lgs1mi0i8c5fk3k")))

(define-public crate-byte-size-0.1 (crate (name "byte-size") (vers "0.1.0") (deps (list (crate-dep (name "bincode") (req "^1.3.3") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.152") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.38") (default-features #t) (kind 0)) (crate-dep (name "shoco-rs") (req "^0.1.1") (default-features #t) (kind 2)) (crate-dep (name "smaz") (req "^0.1.0") (default-features #t) (kind 2)))) (hash "0r7mlgwfzncbcrfnk1mrlgk8nhiwdlqv9whsmainjch6xhp6ys7g")))

(define-public crate-byte-size-0.1 (crate (name "byte-size") (vers "0.1.1") (deps (list (crate-dep (name "bincode") (req "^1.3.3") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.152") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.38") (default-features #t) (kind 0)) (crate-dep (name "shoco-rs") (req "^0.1.1") (default-features #t) (kind 2)) (crate-dep (name "smaz") (req "^0.1.0") (default-features #t) (kind 2)))) (hash "1ghggmb1r336a5f7n18m1llprgsrid3z9ydbm3gxdxffxqcsn3yh")))

(define-public crate-byte-size-0.2 (crate (name "byte-size") (vers "0.2.0") (deps (list (crate-dep (name "bincode") (req "^1.3.3") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.152") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.38") (default-features #t) (kind 0)) (crate-dep (name "shoco-rs") (req "^0.1.1") (default-features #t) (kind 2)) (crate-dep (name "smaz") (req "^0.1.0") (default-features #t) (kind 2)))) (hash "07i14hd74r7lrsj2z5nmv4qxwpapkzf546a0n1jfrpk77mk80dsc")))

(define-public crate-byte-size-0.2 (crate (name "byte-size") (vers "0.2.2") (deps (list (crate-dep (name "bincode") (req "^1.3.3") (default-features #t) (kind 0)) (crate-dep (name "phf") (req "^0.11.1") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.152") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.38") (default-features #t) (kind 0)) (crate-dep (name "shoco-rs") (req "^0.1.1") (default-features #t) (kind 2)) (crate-dep (name "smaz") (req "^0.1.0") (default-features #t) (kind 2)) (crate-dep (name "phf_codegen") (req "^0.11.1") (default-features #t) (kind 1)))) (hash "0hhipqphl31w5ayjwsgvn17awxc3zdn3pd55ibj332lk1mj30n03")))

(define-public crate-byte-size-0.2 (crate (name "byte-size") (vers "0.2.3") (deps (list (crate-dep (name "bincode") (req "^1.3.3") (default-features #t) (kind 0)) (crate-dep (name "phf") (req "^0.11.1") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.152") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.38") (default-features #t) (kind 0)) (crate-dep (name "shoco-rs") (req "^0.1.1") (default-features #t) (kind 2)) (crate-dep (name "smaz") (req "^0.1.0") (default-features #t) (kind 2)) (crate-dep (name "percent-encoding") (req "^2.2.0") (default-features #t) (kind 1)) (crate-dep (name "phf_codegen") (req "^0.11.1") (default-features #t) (kind 1)))) (hash "1nld4vs76xxs3zppz9jnyik3maasq2zb4ydljikrr7a000ldi6az")))

(define-public crate-byte-size-0.2 (crate (name "byte-size") (vers "0.2.4") (deps (list (crate-dep (name "bincode") (req "^1.3.3") (default-features #t) (kind 0)) (crate-dep (name "phf") (req "^0.11.1") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.152") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.38") (default-features #t) (kind 0)) (crate-dep (name "shoco-rs") (req "^0.1.1") (default-features #t) (kind 2)) (crate-dep (name "smaz") (req "^0.1.0") (default-features #t) (kind 2)) (crate-dep (name "percent-encoding") (req "^2.2.0") (default-features #t) (kind 1)) (crate-dep (name "phf_codegen") (req "^0.11.1") (default-features #t) (kind 1)))) (hash "0q557mzg5l4fnyirdyw10qabiisraya7jzyp3b0yg2h2qw39id7f")))

(define-public crate-byte-size-0.2 (crate (name "byte-size") (vers "0.2.5") (deps (list (crate-dep (name "bincode") (req "^1.3.3") (default-features #t) (kind 0)) (crate-dep (name "phf") (req "^0.11.1") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.152") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.38") (default-features #t) (kind 0)) (crate-dep (name "shoco-rs") (req "^0.1.1") (default-features #t) (kind 2)) (crate-dep (name "smaz") (req "^0.1.0") (default-features #t) (kind 2)) (crate-dep (name "percent-encoding") (req "^2.2.0") (default-features #t) (kind 1)) (crate-dep (name "phf_codegen") (req "^0.11.1") (default-features #t) (kind 1)))) (hash "1d9q5rdmx0iymgj77769fx2cwfdc63x7w361vqd1ci8r7c9cjfdk")))

(define-public crate-byte-size-0.2 (crate (name "byte-size") (vers "0.2.5-a") (deps (list (crate-dep (name "bincode") (req "^1.3.3") (default-features #t) (kind 0)) (crate-dep (name "phf") (req "^0.11.1") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.152") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.38") (default-features #t) (kind 0)) (crate-dep (name "shoco-rs") (req "^0.1.1") (default-features #t) (kind 2)) (crate-dep (name "smaz") (req "^0.1.0") (default-features #t) (kind 2)) (crate-dep (name "percent-encoding") (req "^2.2.0") (default-features #t) (kind 1)) (crate-dep (name "phf_codegen") (req "^0.11.1") (default-features #t) (kind 1)))) (hash "1p2f4dv6sjk243063z36plh8gdcv9cw16k8gwczdl8s4qf6pjr8r")))

(define-public crate-byte-size-0.2 (crate (name "byte-size") (vers "0.2.5-b") (deps (list (crate-dep (name "bincode") (req "^1.3.3") (default-features #t) (kind 0)) (crate-dep (name "phf") (req "^0.11.1") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.152") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.38") (default-features #t) (kind 0)) (crate-dep (name "shoco-rs") (req "^0.1.1") (default-features #t) (kind 2)) (crate-dep (name "smaz") (req "^0.1.0") (default-features #t) (kind 2)) (crate-dep (name "percent-encoding") (req "^2.2.0") (default-features #t) (kind 1)) (crate-dep (name "phf_codegen") (req "^0.11.1") (default-features #t) (kind 1)))) (hash "1qwkr4yjzzs13ds53ddzmlhdp59bvr1934ch8biw55dj8lm8g10f")))

(define-public crate-byte-size-0.2 (crate (name "byte-size") (vers "0.2.6") (deps (list (crate-dep (name "bincode") (req "^1.3.3") (default-features #t) (kind 0)) (crate-dep (name "phf") (req "^0.11.1") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.152") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.38") (default-features #t) (kind 0)) (crate-dep (name "shoco-rs") (req "^0.1.1") (default-features #t) (kind 2)) (crate-dep (name "smaz") (req "^0.1.0") (default-features #t) (kind 2)) (crate-dep (name "percent-encoding") (req "^2.2.0") (default-features #t) (kind 1)) (crate-dep (name "phf_codegen") (req "^0.11.1") (default-features #t) (kind 1)))) (hash "01gwzqzfcz0j686mj4yi0k7d3dyw567rxxssvl0s7805dcr82f62")))

(define-public crate-byte-size-0.2 (crate (name "byte-size") (vers "0.2.7") (deps (list (crate-dep (name "bimap") (req "^0.6.2") (default-features #t) (kind 0)) (crate-dep (name "bincode") (req "^1.3.3") (default-features #t) (kind 0)) (crate-dep (name "phf") (req "^0.11.1") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.152") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.38") (default-features #t) (kind 0)) (crate-dep (name "shoco-rs") (req "^0.1.1") (default-features #t) (kind 2)) (crate-dep (name "smaz") (req "^0.1.0") (default-features #t) (kind 2)) (crate-dep (name "percent-encoding") (req "^2.2.0") (default-features #t) (kind 1)) (crate-dep (name "phf_codegen") (req "^0.11.1") (default-features #t) (kind 1)))) (hash "1273616l3w0kqp1w93bjbk61065xjia5087ma9d9lhs5h4py37r9")))

(define-public crate-byte-slab-0.1 (crate (name "byte-slab") (vers "0.1.0") (deps (list (crate-dep (name "heapless") (req "^0.7.5") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.128") (kind 0)))) (hash "043nhra8wzmx93jkj4m84v0gx36kvwz8fhza9kgdllawgdr8ldrs") (features (quote (("usize_queue" "heapless/mpmc_large") ("default" "usize_queue"))))))

(define-public crate-byte-slab-0.1 (crate (name "byte-slab") (vers "0.1.1") (deps (list (crate-dep (name "heapless") (req "^0.7.5") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.128") (kind 0)))) (hash "0wdg4k5wc7rq68fd4abpzh72hv7xv6xfg2mhz5rhwbc5ngmlr4cv") (features (quote (("usize_queue" "heapless/mpmc_large") ("default" "usize_queue"))))))

(define-public crate-byte-slab-0.1 (crate (name "byte-slab") (vers "0.1.2") (deps (list (crate-dep (name "heapless") (req "^0.7.5") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.128") (kind 0)))) (hash "0sl2c1bbmi2sdz6l6d941mqnckmbcyxalj5ra84a2fv19g1i2wcl") (features (quote (("usize_queue" "heapless/mpmc_large") ("default" "usize_queue"))))))

(define-public crate-byte-slab-0.1 (crate (name "byte-slab") (vers "0.1.3") (deps (list (crate-dep (name "heapless") (req "^0.7.5") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.128") (kind 0)))) (hash "1a5ic8phmvkr2wm67slfb7z4hig46mdjigy6pjfmjijx5g54hrdq") (features (quote (("usize_queue" "heapless/mpmc_large") ("default" "usize_queue"))))))

(define-public crate-byte-slab-0.1 (crate (name "byte-slab") (vers "0.1.4") (deps (list (crate-dep (name "defmt") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "heapless") (req "^0.7.5") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.128") (kind 0)))) (hash "0vqamgx8q0a35sswjdgfsnya53ylrnphbaqgx6mamnawwmkni9h9") (features (quote (("usize_queue" "heapless/mpmc_large") ("default" "usize_queue"))))))

(define-public crate-byte-slab-0.1 (crate (name "byte-slab") (vers "0.1.5") (deps (list (crate-dep (name "defmt") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "heapless") (req "^0.7.5") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.128") (kind 0)))) (hash "0ckz6194223fkxipf6j4mmbw1y0ldz9kji06d2nfn2axr8q69vdc") (features (quote (("usize_queue" "heapless/mpmc_large") ("default" "usize_queue"))))))

(define-public crate-byte-slab-0.2 (crate (name "byte-slab") (vers "0.2.0") (deps (list (crate-dep (name "defmt") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "heapless") (req "^0.7.5") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.128") (kind 0)))) (hash "0crhrfh68a2mx3gfsbg5cdgsc3xkzj77kfwv5y9wiy8flx6k20rr") (features (quote (("usize_queue" "heapless/mpmc_large") ("default" "usize_queue"))))))

(define-public crate-byte-slab-0.2 (crate (name "byte-slab") (vers "0.2.1") (deps (list (crate-dep (name "defmt") (req "^0.3.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "heapless") (req "^0.7.5") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.128") (kind 0)))) (hash "126l9hywamcfvc1sp7bydi8hnqh03jwrcfcbnmd7hdrzafs96xp7") (features (quote (("usize_queue" "heapless/mpmc_large") ("default" "usize_queue" "defmt"))))))

(define-public crate-byte-slab-0.2 (crate (name "byte-slab") (vers "0.2.2") (deps (list (crate-dep (name "defmt") (req "^0.3.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "heapless") (req "^0.7.5") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.128") (kind 0)))) (hash "043kf1zy1x16x1aka9yr2bmik69962dlaqgnl4ibzzqb3nyk6krm") (features (quote (("usize_queue" "heapless/mpmc_large") ("default" "usize_queue" "defmt"))))))

(define-public crate-byte-slab-derive-0.2 (crate (name "byte-slab-derive") (vers "0.2.0") (deps (list (crate-dep (name "byte-slab") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1.0.34") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.10") (default-features #t) (kind 0)) (crate-dep (name "synstructure") (req "^0.12.6") (default-features #t) (kind 0)))) (hash "1p3hjz2ql51nx5cnc2jpn63bmzpchindqbqbz1v648ak4bdb1lp5")))

(define-public crate-byte-slab-derive-0.2 (crate (name "byte-slab-derive") (vers "0.2.1") (deps (list (crate-dep (name "byte-slab") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1.0.34") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.10") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.82") (default-features #t) (kind 0)) (crate-dep (name "synstructure") (req "^0.12.6") (default-features #t) (kind 0)))) (hash "0jwscnml38bj4nq54nxcsb5a6jqhd3nz6fqr57kd2inlxhmnbm29")))

(define-public crate-byte-slice-0.1 (crate (name "byte-slice") (vers "0.1.0") (hash "0hw2zqw296dr8sk4ypxsfdz7c4gg1kgv9avwkg74dxbbk74gfpbd")))

(define-public crate-byte-slice-0.1 (crate (name "byte-slice") (vers "0.1.1") (hash "1sz4k8m6cv06wz30j6wn4z7jc4hm7z8f7nnj0k34rxv2vn7j2j42")))

(define-public crate-byte-slice-0.1 (crate (name "byte-slice") (vers "0.1.2") (hash "1isncg840myxswrhs3yli13h4c0zhj4np9rvzwn4kybj1qz8c2jq")))

(define-public crate-byte-slice-0.1 (crate (name "byte-slice") (vers "0.1.3") (hash "01ysxyy6cnsvrcg50zx3hchkvy41mqg1x12h6zvqdvyk2q35rawd")))

(define-public crate-byte-slice-0.1 (crate (name "byte-slice") (vers "0.1.4") (hash "02xg02n0xl1l9r3k1jfhj842cgb4j2hxibznc1dngvah2rrhj97n")))

(define-public crate-byte-slice-0.1 (crate (name "byte-slice") (vers "0.1.5") (hash "0cvrwjvhkjyyn5hbpd1g30c27c4npfri0k9b8a89dylfmvm4j643")))

(define-public crate-byte-slice-0.1 (crate (name "byte-slice") (vers "0.1.6") (hash "0cczl762dg9lkicr5qb7w9i1ycddznnzq1vcksi17fwsnvhnfj9p")))

(define-public crate-byte-slice-0.1 (crate (name "byte-slice") (vers "0.1.7") (hash "150c5x2y3xah4v36bxjl6pdy2g54bfipn06c84yj95cwckc4562h")))

(define-public crate-byte-slice-0.1 (crate (name "byte-slice") (vers "0.1.8") (hash "07nhgf813dy7hwc8rc8g94n2i0jvyblyp8qfylwnliadv8c53czf")))

(define-public crate-byte-slice-0.1 (crate (name "byte-slice") (vers "0.1.9") (hash "036zjdm20l664hk322siyzgm4qi6a19asvdkq2z3flmhxwwiw0av")))

(define-public crate-byte-slice-0.1 (crate (name "byte-slice") (vers "0.1.10") (hash "0jgsp3m0w1s5ga7wjxqlb66dy53a9lhbdg0cacfzz1s6l4zvmhmx")))

(define-public crate-byte-slice-0.1 (crate (name "byte-slice") (vers "0.1.11") (hash "1wgms9n76mbjmdqxm981qy7qbaw45qnhz0z3riklbxylaadwc31p")))

(define-public crate-byte-slice-0.1 (crate (name "byte-slice") (vers "0.1.12") (hash "0kyc66m7g8m91wv13g2sq0cqnjg0g22ifff0xh15c67pn8qc3kzy")))

(define-public crate-byte-slice-cast-0.1 (crate (name "byte-slice-cast") (vers "0.1.0") (hash "1mw54i53rzzsffg67m0ybzvgkr36vka6g9ybdhhpjfvcz9xmx1js")))

(define-public crate-byte-slice-cast-0.2 (crate (name "byte-slice-cast") (vers "0.2.0") (hash "0r5ycb6ap8w0vj0x0hszdl60vlna9rpbs8y1zf2hf9shgc8nqd18")))

(define-public crate-byte-slice-cast-0.3 (crate (name "byte-slice-cast") (vers "0.3.0") (hash "1wgaigq4xzrpaaik1ja828zi4v2qn4md64c01yglzyz07z3fxmil")))

(define-public crate-byte-slice-cast-0.3 (crate (name "byte-slice-cast") (vers "0.3.1") (hash "0xfmpyx3i9r1fgqayhz8dy8syvra0wzhz2wimzgjd05d4jw7kjxn")))

(define-public crate-byte-slice-cast-0.3 (crate (name "byte-slice-cast") (vers "0.3.2") (hash "0xrykhvimjy7nz4s843xbfvzbzsrxihl818d9a6iviwf28cbzg3w") (features (quote (("std") ("default" "std"))))))

(define-public crate-byte-slice-cast-0.3 (crate (name "byte-slice-cast") (vers "0.3.3") (hash "0vvx2jl37kndrv8chywlgf8arkp3cnw6arcd363ynakwg5kwi3nr") (features (quote (("std") ("default" "std"))))))

(define-public crate-byte-slice-cast-0.3 (crate (name "byte-slice-cast") (vers "0.3.4") (hash "1aajgab19dmy7gspfr441axx60v9mmg6s09f01qa3phy5hxry87n") (features (quote (("std" "alloc") ("default" "std") ("alloc"))))))

(define-public crate-byte-slice-cast-0.3 (crate (name "byte-slice-cast") (vers "0.3.5") (hash "1lwkgp1ahziy0kc6hrczvgxkgbb4qsprak8x7kik7wfbdf8f79dh") (features (quote (("std" "alloc") ("default" "std") ("alloc"))))))

(define-public crate-byte-slice-cast-1 (crate (name "byte-slice-cast") (vers "1.0.0") (hash "10dw7dliqn9bbqwp31f3nma34gvp7mj66m8ji7sm93580i5bzhb5") (features (quote (("std") ("default" "std"))))))

(define-public crate-byte-slice-cast-1 (crate (name "byte-slice-cast") (vers "1.1.0") (hash "1rkji3p0cp66yy0r2g8hv4pryx9j60h1dnnxl2s52dlqdbbrc1ya") (features (quote (("std") ("default" "std"))))))

(define-public crate-byte-slice-cast-1 (crate (name "byte-slice-cast") (vers "1.2.0") (hash "162618ai9pnsim49lkjpq2yi2b3wssclvqxwwycw8xrbb58wfc0x") (features (quote (("std") ("default" "std"))))))

(define-public crate-byte-slice-cast-1 (crate (name "byte-slice-cast") (vers "1.2.1") (hash "0zh65170havnnqpd912fgbbplm6fm0dc0v7wcgaf35b02v8gvic7") (features (quote (("std") ("default" "std"))))))

(define-public crate-byte-slice-cast-1 (crate (name "byte-slice-cast") (vers "1.2.2") (hash "033vv1qddzsj9yfsam4abj55rp60digngcr9a8wgv9pccf5rzb63") (features (quote (("std") ("default" "std"))))))

(define-public crate-byte-strings-0.1 (crate (name "byte-strings") (vers "0.1.0") (deps (list (crate-dep (name "byte-strings-proc-macro") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "cfg-if") (req "^0.1.6") (default-features #t) (kind 0)))) (hash "0286fcv23bsl5vc59zqvc9gwplc1gmvvz7ym1bk0yvzfqpxp1fjk") (features (quote (("proc-macro-hygiene" "byte-strings-proc-macro/proc-macro-hygiene") ("nightly") ("default"))))))

(define-public crate-byte-strings-0.1 (crate (name "byte-strings") (vers "0.1.1") (deps (list (crate-dep (name "byte-strings-proc-macro") (req "^0.1.1") (default-features #t) (kind 0)) (crate-dep (name "cfg-if") (req "^0.1.6") (default-features #t) (kind 0)))) (hash "10mwc01rzb5jpd2iyfpn00rmhpjm0844r36jrmzjy6r1bp4vcxlw") (features (quote (("proc-macro-hygiene" "byte-strings-proc-macro/proc-macro-hygiene") ("default"))))))

(define-public crate-byte-strings-0.1 (crate (name "byte-strings") (vers "0.1.2") (deps (list (crate-dep (name "byte-strings-proc-macro") (req "^0.1.1") (default-features #t) (kind 0)) (crate-dep (name "cfg-if") (req "^0.1.6") (default-features #t) (kind 0)))) (hash "0r8z558m2a6bxl86gmqi8s2hhxn83awcz760lmk35b8b2wphz4ja") (features (quote (("proc-macro-hygiene" "byte-strings-proc-macro/proc-macro-hygiene") ("nightly") ("default"))))))

(define-public crate-byte-strings-0.1 (crate (name "byte-strings") (vers "0.1.3") (deps (list (crate-dep (name "byte-strings-proc-macro") (req "^0.1.1") (default-features #t) (kind 0)) (crate-dep (name "cfg-if") (req "^0.1.6") (default-features #t) (kind 0)))) (hash "1r7z07ynl4bbys77kam01h6ks7inrv919xiwibq2bckdpiqc0bi5") (features (quote (("proc-macro-hygiene" "byte-strings-proc-macro/proc-macro-hygiene") ("nightly") ("default"))))))

(define-public crate-byte-strings-0.2 (crate (name "byte-strings") (vers "0.2.0-rc1") (deps (list (crate-dep (name "byte-strings-proc_macros") (req "^0.2.0-rc1") (default-features #t) (kind 0)))) (hash "0w76wn6fqm6528f741nyw659s990nb25qxhff4f40a37x2b6nw4y") (features (quote (("ui-tests" "better-docs" "const-friendly") ("const-friendly") ("better-docs")))) (yanked #t)))

(define-public crate-byte-strings-0.2 (crate (name "byte-strings") (vers "0.2.0") (deps (list (crate-dep (name "byte-strings-proc_macros") (req "^0.2.0") (default-features #t) (kind 0)))) (hash "14qn4ihfd6zmc1lyrr8d0cissssc9sgbjy4vwvjsrzn2jdv4dzyq") (features (quote (("ui-tests" "better-docs" "const-friendly") ("const-friendly") ("better-docs")))) (yanked #t)))

(define-public crate-byte-strings-0.2 (crate (name "byte-strings") (vers "0.2.1") (deps (list (crate-dep (name "byte-strings-proc_macros") (req "^0.2.1") (default-features #t) (kind 0)))) (hash "1s5rjhfvzahp4550y14qdn6gyyy324pvmwk994szg16j6mmzjp6d") (features (quote (("ui-tests" "better-docs" "const-friendly") ("const-friendly") ("better-docs"))))))

(define-public crate-byte-strings-0.2 (crate (name "byte-strings") (vers "0.2.2") (deps (list (crate-dep (name "byte-strings-proc_macros") (req "^0.2.2") (default-features #t) (kind 0)))) (hash "1qvsx0awhky1rh2bpd8r46i16mjg70xswbwyvm61y7h4w3bfwg4n") (features (quote (("ui-tests" "better-docs" "const-friendly") ("const-friendly") ("better-docs"))))))

(define-public crate-byte-strings-0.3 (crate (name "byte-strings") (vers "0.3.0") (deps (list (crate-dep (name "byte-strings-proc_macros") (req "=0.3.0") (default-features #t) (kind 0)))) (hash "00v1q6sxkbadpv4dncks167p4fjivigvy8v1jzlb17b9azr356nj") (features (quote (("ui-tests" "better-docs") ("const-friendly") ("better-docs")))) (rust-version "1.65.0")))

(define-public crate-byte-strings-0.3 (crate (name "byte-strings") (vers "0.3.1") (deps (list (crate-dep (name "byte-strings-proc_macros") (req "=0.3.1") (default-features #t) (kind 0)))) (hash "16n2xd2i0xbj2q3qdcb0dmr9phza1razmzk2k3jm117b3x9yabh0") (features (quote (("ui-tests" "better-docs") ("const-friendly") ("better-docs")))) (rust-version "1.65.0")))

(define-public crate-byte-strings-proc-macro-0.1 (crate (name "byte-strings-proc-macro") (vers "0.1.0") (deps (list (crate-dep (name "cfg-if") (req "^0.1.6") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^0.6.11") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^0.15") (default-features #t) (kind 0)))) (hash "1mmkrp3mq4iwyb4i2xz8nlllhfjr2hvxpp0hy2x413azgnx9yy1v") (features (quote (("proc-macro-hygiene") ("default"))))))

(define-public crate-byte-strings-proc-macro-0.1 (crate (name "byte-strings-proc-macro") (vers "0.1.1") (deps (list (crate-dep (name "cfg-if") (req "^0.1.6") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^0.6.11") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^0.15") (default-features #t) (kind 0)))) (hash "0mcd5j0s3a9wb5qlvb77cn9ybai5l4d30jchhfcp12cb6dhmy27p") (features (quote (("proc-macro-hygiene") ("default"))))))

(define-public crate-byte-strings-proc_macros-0.2 (crate (name "byte-strings-proc_macros") (vers "0.2.0-rc1") (deps (list (crate-dep (name "proc-macro2") (req "^1.0.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.7") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "11nv5a0w73y0wgxw3w3qy0ls3dj14s8ygyhbz107f09p3r2n9i00")))

(define-public crate-byte-strings-proc_macros-0.2 (crate (name "byte-strings-proc_macros") (vers "0.2.0") (deps (list (crate-dep (name "proc-macro2") (req "^1.0.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.7") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "0mxlii3gcsqaicpfpbkac9ik2wpfs1invjqnc4fmb2j2949j1kbx")))

(define-public crate-byte-strings-proc_macros-0.2 (crate (name "byte-strings-proc_macros") (vers "0.2.1") (deps (list (crate-dep (name "proc-macro2") (req "^1.0.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.7") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "0ddwimzbc4xfy26fsldpwvcyp4ynlhm54wf09vp4ak3x6bjfj4q9")))

(define-public crate-byte-strings-proc_macros-0.2 (crate (name "byte-strings-proc_macros") (vers "0.2.2") (deps (list (crate-dep (name "proc-macro2") (req "^1.0.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.7") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "1r88qljf3bj6ma0hbxr61xmd1ylj5jq78r330xx4q8wp7mkyhy2f")))

(define-public crate-byte-strings-proc_macros-0.3 (crate (name "byte-strings-proc_macros") (vers "0.3.0") (deps (list (crate-dep (name "proc-macro2") (req "^1.0.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2.0.0") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "056y4ixfixjch3mrjqwcm32i9d77dmcvgp61yd0kbvw4iw5cjh3f")))

(define-public crate-byte-strings-proc_macros-0.3 (crate (name "byte-strings-proc_macros") (vers "0.3.1") (deps (list (crate-dep (name "proc-macro2") (req "^1.0.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2.0.0") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "00xfn3anw5gs8hnss7s013c0m1qggnjagc22zqdwgmlq3zky1xv2")))

(define-public crate-byte-style-encoder-0.1 (crate (name "byte-style-encoder") (vers "0.1.1") (deps (list (crate-dep (name "bitvec") (req "^0.19") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^2.33") (default-features #t) (kind 0)) (crate-dep (name "nom") (req "^6.2.1") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1") (default-features #t) (kind 0)))) (hash "1p5z995r2jffa53rpfbcri4gh8n78cs0c2dvi4sldbdl8yvvfvhz")))

(define-public crate-byte-tools-0.1 (crate (name "byte-tools") (vers "0.1.0") (hash "0jpsf4ynx3jc3km2y7ngkjmsaqv4m8qf2acxvmbb84d7a9yhdrfb")))

(define-public crate-byte-tools-0.1 (crate (name "byte-tools") (vers "0.1.1") (hash "1chqjn1lm8wx871zxw0nry9l5qp1apxfbxkaajqpdj0r1kj088vl")))

(define-public crate-byte-tools-0.1 (crate (name "byte-tools") (vers "0.1.2") (hash "1v2vg0mipvp0mcpwafzd2c4bdxk3zyqc4as18z63wx06gkgr4iwp")))

(define-public crate-byte-tools-0.1 (crate (name "byte-tools") (vers "0.1.3") (hash "0y8jk7vzxjw5kjvanwhwp1d91qqdgrmi2y42fzlgziq0m2dih689")))

(define-public crate-byte-tools-0.2 (crate (name "byte-tools") (vers "0.2.0") (hash "0h2zxygfnn46akmgh8cdp4x6xy4kb0b45rzmj76rxa0j99bk432n")))

(define-public crate-byte-tools-0.3 (crate (name "byte-tools") (vers "0.3.0") (hash "10i1pmgcn3p1kym5x72pww195sdlcq6mhisdnpglccp2zpk7j14q")))

(define-public crate-byte-tools-0.3 (crate (name "byte-tools") (vers "0.3.1") (hash "1mqi29wsm8njpl51pfwr31wmpzs5ahlcb40wsjyd92l90ixcmdg3")))

(define-public crate-byte-unit-1 (crate (name "byte-unit") (vers "1.0.0") (deps (list (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)))) (hash "0isrdizbb9vyf9jnl5q0w9b7wm795lqzshskflqn37870w7yn1sy")))

(define-public crate-byte-unit-1 (crate (name "byte-unit") (vers "1.0.1") (deps (list (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)))) (hash "1xg2n85n2agnr8djyj6r8fmc3m6z0x51f40li56hjacdk61j7331")))

(define-public crate-byte-unit-1 (crate (name "byte-unit") (vers "1.0.2") (deps (list (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)))) (hash "0an7crj3v2xn6cf9xlcahf8afa4n1aqlqqlvhksp3qs1jvhqva1w")))

(define-public crate-byte-unit-1 (crate (name "byte-unit") (vers "1.0.3") (deps (list (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)))) (hash "1fd7f45dkz4i9f3iv20bqlm98pzvnvd5fyd29c07j2pj4d1yvpx5")))

(define-public crate-byte-unit-1 (crate (name "byte-unit") (vers "1.0.4") (deps (list (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)))) (hash "04rd72irszmsjbqw010lwki7l6ha6spss4kd9r5ibgbsb03mzkw0")))

(define-public crate-byte-unit-1 (crate (name "byte-unit") (vers "1.0.5") (deps (list (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)))) (hash "0kn9p24wxybydqwg8jhmizv1qn2l9mp1pdblsxanb40fi7v2dagc")))

(define-public crate-byte-unit-1 (crate (name "byte-unit") (vers "1.0.6") (deps (list (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)))) (hash "1cx6ap20qs0vindw4lq97i4rrsw0s5zr2fd7gh85vg9a19xr716l")))

(define-public crate-byte-unit-1 (crate (name "byte-unit") (vers "1.0.7") (deps (list (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)))) (hash "1n7id1zvfgs7m2mwl9z3h8flr062mqx6zp0p2b8m5yv27y1dvlxb")))

(define-public crate-byte-unit-1 (crate (name "byte-unit") (vers "1.0.8") (deps (list (crate-dep (name "lazy_static") (req "^1.1.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)))) (hash "0c13v01k6b1jca8w8566r8bfmzmf08bn039c7rq1bifnpyj33qfk")))

(define-public crate-byte-unit-1 (crate (name "byte-unit") (vers "1.0.9") (deps (list (crate-dep (name "lazy_static") (req "^1.1.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)))) (hash "0byqd08x67h10n5hp5c47lg73s7wlm4hw80gdhmqhrkni3cbwswz")))

(define-public crate-byte-unit-1 (crate (name "byte-unit") (vers "1.1.0") (deps (list (crate-dep (name "lazy_static") (req "^1.1.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)))) (hash "0hflans9qdcsvf9k7mnwyzdl1mpbn2wahn4yimyd5dvk9f47ni2y")))

(define-public crate-byte-unit-2 (crate (name "byte-unit") (vers "2.0.0") (deps (list (crate-dep (name "lazy_static") (req "^1.1.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)))) (hash "16xa10s4xi9zq30rrj8f4mp5mm3sfx9ybxvf98bqayp6432d5p56")))

(define-public crate-byte-unit-2 (crate (name "byte-unit") (vers "2.1.0") (deps (list (crate-dep (name "lazy_static") (req "^1.1.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)))) (hash "0hdqmz9kmpkdf8zl9fg3rng1mhqw5y2cdw41agnpn5ma0d3vnm37")))

(define-public crate-byte-unit-3 (crate (name "byte-unit") (vers "3.0.0") (hash "0ybis2l3bzbpfxryvxa3ncb1x5xk1sf1nfkbh2g8ipb78nzak9n1")))

(define-public crate-byte-unit-3 (crate (name "byte-unit") (vers "3.0.1") (hash "11qyipx8g951wcyvz0d82plwpnncb3ji5wj45n1w8xlpxia9j4wh")))

(define-public crate-byte-unit-3 (crate (name "byte-unit") (vers "3.0.2") (hash "0glj3q3x980mabpjc1s3bcrr6k0zrxlddrzd5g96859himgakb42")))

(define-public crate-byte-unit-3 (crate (name "byte-unit") (vers "3.0.3") (hash "0mlh8xngs5izs8nl10ryr1q0x0zql1nql4wsy7cr0x40a2asg538")))

(define-public crate-byte-unit-3 (crate (name "byte-unit") (vers "3.1.0") (hash "0yx6gci6k7vm6i2giiba5am0ycni4yin4kwn4vghmzrwnyr578s8") (features (quote (("u128") ("std") ("default" "u128"))))))

(define-public crate-byte-unit-3 (crate (name "byte-unit") (vers "3.1.1") (hash "166ish3qfqkialf3fbhb65r6h24gbgp2gp8hvk666c6f120mm70a") (features (quote (("u128") ("std") ("default" "u128"))))))

(define-public crate-byte-unit-3 (crate (name "byte-unit") (vers "3.1.2") (hash "1bk0s69v4q0xzsmzbllb3ihv1ds380h1b22j9bb153cfj3ycmjm3") (features (quote (("u128") ("std") ("default" "u128"))))))

(define-public crate-byte-unit-3 (crate (name "byte-unit") (vers "3.1.3") (hash "0h90pzly6cnjmw6aci2ywdrs67d2037dl9p9yf1hdrqwyaxhsfam") (features (quote (("u128") ("std") ("default" "u128"))))))

(define-public crate-byte-unit-3 (crate (name "byte-unit") (vers "3.1.4") (hash "1f244x8l68x4s2zcj2lylkjkgp3smjvhwg0rj95ms00ivv4h2ls1") (features (quote (("u128") ("std") ("default" "u128"))))))

(define-public crate-byte-unit-4 (crate (name "byte-unit") (vers "4.0.0") (deps (list (crate-dep (name "serde") (req "^1") (optional #t) (default-features #t) (kind 0)))) (hash "0ggkhz41cnyp4r3bp3sg3nl57nlidyqvrrbxbjbzzaaz37djdlai") (features (quote (("u128") ("std") ("default" "std" "u128"))))))

(define-public crate-byte-unit-4 (crate (name "byte-unit") (vers "4.0.1") (deps (list (crate-dep (name "serde") (req "^1") (optional #t) (default-features #t) (kind 0)))) (hash "0n4a8y67nak76lvhds7n451lqz7rcmfvyxzrlck4mdphzf1b7v6i") (features (quote (("u128") ("std") ("default" "std" "u128"))))))

(define-public crate-byte-unit-4 (crate (name "byte-unit") (vers "4.0.2") (deps (list (crate-dep (name "serde") (req "^1") (optional #t) (default-features #t) (kind 0)))) (hash "0lmhyyqwnsbyrasncza36l59kz801rbc3nb30kwf0jiyf8pmfr8z") (features (quote (("u128") ("std") ("default" "std" "u128"))))))

(define-public crate-byte-unit-4 (crate (name "byte-unit") (vers "4.0.3") (deps (list (crate-dep (name "serde") (req "^1") (optional #t) (default-features #t) (kind 0)))) (hash "1x5g7n7xqwbx43aixc559wfhk4qkqzxf9a9h61b131ql8d2r429h") (features (quote (("u128") ("std") ("default" "std" "u128"))))))

(define-public crate-byte-unit-4 (crate (name "byte-unit") (vers "4.0.4") (deps (list (crate-dep (name "serde") (req "^1") (optional #t) (default-features #t) (kind 0)))) (hash "1dh3blkld26xydywk6ir6pr79nmndq0q2kdcf80q26d92w0jagm8") (features (quote (("u128") ("std") ("default" "std" "u128"))))))

(define-public crate-byte-unit-4 (crate (name "byte-unit") (vers "4.0.5") (deps (list (crate-dep (name "serde") (req "^1") (optional #t) (default-features #t) (kind 0)))) (hash "1zycj4s9ya764p177357ddhlacllqy55lrlbbqf0g1l5ng7y56xg") (features (quote (("u128") ("std") ("default" "std" "u128"))))))

(define-public crate-byte-unit-4 (crate (name "byte-unit") (vers "4.0.6") (deps (list (crate-dep (name "serde") (req "^1") (optional #t) (default-features #t) (kind 0)))) (hash "1l1iqcxgasdgw6waharx2gr2s3ysr1a1cp7sac087f6kxc8pwj7r") (features (quote (("u128") ("std") ("default" "std" "u128"))))))

(define-public crate-byte-unit-4 (crate (name "byte-unit") (vers "4.0.7") (deps (list (crate-dep (name "serde") (req "^1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "utf8-width") (req "^0.1") (default-features #t) (kind 0)))) (hash "1ggwkmhrsqkr184k2qx0gmxyw05cz0mvh90ffcs4xq1yqxx0rhg6") (features (quote (("u128") ("std") ("default" "std" "u128"))))))

(define-public crate-byte-unit-4 (crate (name "byte-unit") (vers "4.0.8") (deps (list (crate-dep (name "serde") (req "^1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "utf8-width") (req "^0.1") (default-features #t) (kind 0)))) (hash "0k1zcp911qn5sin93brgpqjc53w1l53n7nx9vj6sivrgv9iqb98v") (features (quote (("u128") ("std") ("default" "std" "u128"))))))

(define-public crate-byte-unit-4 (crate (name "byte-unit") (vers "4.0.9") (deps (list (crate-dep (name "serde") (req "^1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "utf8-width") (req "^0.1") (default-features #t) (kind 0)))) (hash "0lxi11qf6h1rqr0yhsh7i6755l325qrkv9r4bgismyik531mi1qw") (features (quote (("u128") ("std") ("default" "std" "u128"))))))

(define-public crate-byte-unit-4 (crate (name "byte-unit") (vers "4.0.10") (deps (list (crate-dep (name "serde") (req "^1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "utf8-width") (req "^0.1") (default-features #t) (kind 0)))) (hash "09zfw6dv0x3lkg2r3kvn1ar43zzypjvcgm0gzsy9nfhw8w00jlmr") (features (quote (("u128") ("std") ("default" "std" "u128"))))))

(define-public crate-byte-unit-4 (crate (name "byte-unit") (vers "4.0.11") (deps (list (crate-dep (name "serde") (req "^1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "utf8-width") (req "^0.1") (default-features #t) (kind 0)))) (hash "0hz7zzr6sc9cwwr8z9cygqcsdbrjk23knbzg7drj20wwn1kqxn96") (features (quote (("u128") ("std") ("default" "std" "u128"))))))

(define-public crate-byte-unit-4 (crate (name "byte-unit") (vers "4.0.12") (deps (list (crate-dep (name "serde") (req "^1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "utf8-width") (req "^0.1") (default-features #t) (kind 0)))) (hash "1n5w327g55ly5r4yjh71rhv2ifw615xdxv8d2rj5nxsbxgk9fc86") (features (quote (("u128") ("std") ("default" "std" "u128"))))))

(define-public crate-byte-unit-4 (crate (name "byte-unit") (vers "4.0.13") (deps (list (crate-dep (name "serde") (req "^1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "utf8-width") (req "^0.1") (default-features #t) (kind 0)))) (hash "0zxfqic18j1byrxhn7msqbywzx2sp9izs89zksaadmy71rdzqvwm") (features (quote (("u128") ("std") ("default" "std" "u128"))))))

(define-public crate-byte-unit-4 (crate (name "byte-unit") (vers "4.0.14") (deps (list (crate-dep (name "serde") (req "^1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "utf8-width") (req "^0.1") (default-features #t) (kind 0)))) (hash "0jq1w1xr6403v62ckzblzkbn1varldr5b89fykq9zwb5v86z3swm") (features (quote (("u128") ("std") ("default" "std" "u128"))))))

(define-public crate-byte-unit-4 (crate (name "byte-unit") (vers "4.0.15") (deps (list (crate-dep (name "serde") (req "^1") (optional #t) (kind 0)) (crate-dep (name "utf8-width") (req "^0.1") (default-features #t) (kind 0)))) (hash "1q4m3icp2qrx0rkb653k3iyv3wxpg5qnwc4ijvc6hyqbd812dv08") (features (quote (("u128") ("std" "serde/std" "alloc") ("serde" "alloc") ("default" "std" "u128") ("alloc")))) (yanked #t)))

(define-public crate-byte-unit-4 (crate (name "byte-unit") (vers "4.0.16") (deps (list (crate-dep (name "serde") (req "^1") (optional #t) (kind 0)) (crate-dep (name "utf8-width") (req "^0.1") (default-features #t) (kind 0)))) (hash "1j5g46y9mx3g4w9v0k51dickrb6givriqyb0vbbfd9hgy76q6kqk") (features (quote (("u128") ("std" "serde/std" "alloc") ("serde" "alloc") ("default" "std" "u128") ("alloc")))) (yanked #t)))

(define-public crate-byte-unit-4 (crate (name "byte-unit") (vers "4.0.17") (deps (list (crate-dep (name "serde_dep") (req "^1") (features (quote ("alloc"))) (optional #t) (kind 0) (package "serde")) (crate-dep (name "utf8-width") (req "^0.1") (default-features #t) (kind 0)))) (hash "1707hr76fv1bwk5m9kwk1g5cm6l3iwa14afb1jdc1c17ssrx86jq") (features (quote (("u128") ("std" "serde_dep/std" "alloc") ("serde" "alloc" "serde_dep") ("default" "std" "u128") ("alloc"))))))

(define-public crate-byte-unit-4 (crate (name "byte-unit") (vers "4.0.18") (deps (list (crate-dep (name "serde_dep") (req "^1") (features (quote ("alloc"))) (optional #t) (kind 0) (package "serde")) (crate-dep (name "utf8-width") (req "^0.1") (default-features #t) (kind 0)))) (hash "1124a4iggq5x85xp4w35mrf5syqyhsd8xapzgxj4hj7008v6fj1k") (features (quote (("u128") ("std" "serde_dep/std" "alloc") ("serde" "alloc" "serde_dep") ("default" "std" "u128") ("alloc"))))))

(define-public crate-byte-unit-4 (crate (name "byte-unit") (vers "4.0.19") (deps (list (crate-dep (name "serde") (req "^1") (features (quote ("alloc"))) (optional #t) (kind 0)) (crate-dep (name "utf8-width") (req "^0.1") (default-features #t) (kind 0)))) (hash "0v4v4z8qilnbg0lv16icbbbdq5kjpbp8yw044lszrzdqawhb6y6s") (features (quote (("u128") ("std" "serde/std" "alloc") ("default" "std" "u128") ("alloc")))) (v 2) (features2 (quote (("serde" "alloc" "dep:serde"))))))

(define-public crate-byte-unit-5 (crate (name "byte-unit") (vers "5.0.0-beta.1") (deps (list (crate-dep (name "rust_decimal") (req "^1") (optional #t) (kind 0)) (crate-dep (name "serde") (req "^1") (features (quote ("alloc"))) (optional #t) (kind 0)) (crate-dep (name "serde_json") (req "^1") (default-features #t) (kind 2)) (crate-dep (name "utf8-width") (req "^0.1") (default-features #t) (kind 0)))) (hash "1jan2jc22i1bpnv291xy8fh6mab84sq5g1lkk19blsb4al87q3p0") (features (quote (("u128") ("std" "serde/std" "alloc") ("default" "std" "byte") ("byte" "rust_decimal") ("bit" "rust_decimal") ("alloc")))) (v 2) (features2 (quote (("serde" "alloc" "dep:serde") ("rust_decimal" "dep:rust_decimal")))) (rust-version "1.60")))

(define-public crate-byte-unit-5 (crate (name "byte-unit") (vers "5.0.0") (deps (list (crate-dep (name "rust_decimal") (req "^1") (optional #t) (kind 0)) (crate-dep (name "serde") (req "^1") (features (quote ("alloc"))) (optional #t) (kind 0)) (crate-dep (name "serde_json") (req "^1") (default-features #t) (kind 2)) (crate-dep (name "utf8-width") (req "^0.1") (default-features #t) (kind 0)))) (hash "1kwh2sxws9jwnlclj9n62r4lmh46c1m28d0lbb97rvqhmwsbkyjl") (features (quote (("u128") ("std" "serde/std" "alloc") ("default" "std" "byte") ("byte" "rust_decimal") ("bit" "rust_decimal") ("alloc")))) (v 2) (features2 (quote (("serde" "alloc" "dep:serde") ("rust_decimal" "dep:rust_decimal")))) (rust-version "1.60")))

(define-public crate-byte-unit-5 (crate (name "byte-unit") (vers "5.0.1") (deps (list (crate-dep (name "rust_decimal") (req "^1") (optional #t) (kind 0)) (crate-dep (name "serde") (req "^1") (features (quote ("alloc"))) (optional #t) (kind 0)) (crate-dep (name "serde_json") (req "^1") (default-features #t) (kind 2)) (crate-dep (name "utf8-width") (req "^0.1") (default-features #t) (kind 0)))) (hash "1m1wb6i5gjb70p140i9mns4vkgkbzqlcnxgnl08zfp1pxz7rbs5y") (features (quote (("u128") ("std" "serde/std" "alloc") ("default" "std" "byte") ("byte" "rust_decimal") ("bit" "rust_decimal") ("alloc")))) (v 2) (features2 (quote (("serde" "alloc" "dep:serde") ("rust_decimal" "dep:rust_decimal")))) (rust-version "1.60")))

(define-public crate-byte-unit-5 (crate (name "byte-unit") (vers "5.0.2") (deps (list (crate-dep (name "rust_decimal") (req "^1") (optional #t) (kind 0)) (crate-dep (name "serde") (req "^1") (features (quote ("alloc"))) (optional #t) (kind 0)) (crate-dep (name "serde_json") (req "^1") (default-features #t) (kind 2)) (crate-dep (name "utf8-width") (req "^0.1") (default-features #t) (kind 0)))) (hash "0lkmy2lrfp3lpcjaabj9ljwqy5w5rzq40yz24s6ch5h65avaqrwi") (features (quote (("u128") ("std" "serde/std" "alloc") ("default" "std" "byte") ("byte" "rust_decimal") ("bit" "rust_decimal") ("alloc")))) (v 2) (features2 (quote (("serde" "alloc" "dep:serde") ("rust_decimal" "dep:rust_decimal")))) (rust-version "1.60")))

(define-public crate-byte-unit-5 (crate (name "byte-unit") (vers "5.0.3") (deps (list (crate-dep (name "rust_decimal") (req "^1") (optional #t) (kind 0)) (crate-dep (name "serde") (req "^1") (features (quote ("alloc"))) (optional #t) (kind 0)) (crate-dep (name "serde_json") (req "^1") (default-features #t) (kind 2)) (crate-dep (name "utf8-width") (req "^0.1") (default-features #t) (kind 0)))) (hash "1qvvipsmdj916ca39m82914gqy6fbhqq3g0ymfvn9ygpw29ayh5w") (features (quote (("u128") ("std" "serde/std" "alloc") ("default" "std" "byte") ("byte" "rust_decimal") ("bit" "rust_decimal") ("alloc")))) (v 2) (features2 (quote (("serde" "alloc" "dep:serde") ("rust_decimal" "dep:rust_decimal")))) (rust-version "1.60")))

(define-public crate-byte-unit-5 (crate (name "byte-unit") (vers "5.0.4") (deps (list (crate-dep (name "rust_decimal") (req "^1") (optional #t) (kind 0)) (crate-dep (name "serde") (req "^1") (features (quote ("alloc"))) (optional #t) (kind 0)) (crate-dep (name "serde_json") (req "^1") (default-features #t) (kind 2)) (crate-dep (name "utf8-width") (req "^0.1") (default-features #t) (kind 0)))) (hash "1n09c21wlrpg9qllzhdf78dsr8xmgr07kp5mkxry96gyfq03wp9v") (features (quote (("u128") ("default" "std" "byte") ("byte" "rust_decimal") ("bit" "rust_decimal")))) (v 2) (features2 (quote (("std" "serde?/std") ("serde" "dep:serde") ("rust_decimal" "dep:rust_decimal")))) (rust-version "1.60")))

(define-public crate-byte-unit-5 (crate (name "byte-unit") (vers "5.1.0") (deps (list (crate-dep (name "rocket") (req "^0.5") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "rust_decimal") (req "^1") (optional #t) (kind 0)) (crate-dep (name "serde") (req "^1") (features (quote ("alloc"))) (optional #t) (kind 0)) (crate-dep (name "serde_json") (req "^1") (default-features #t) (kind 2)) (crate-dep (name "utf8-width") (req "^0.1") (default-features #t) (kind 0)))) (hash "0z41rspmbn3pf7pmnscjwnxsvmadqmlz6ppdqjxfs278ahkf1lxj") (features (quote (("u128") ("default" "std" "byte") ("byte" "rust_decimal") ("bit" "rust_decimal")))) (v 2) (features2 (quote (("std" "serde?/std" "rust_decimal?/std") ("serde" "dep:serde") ("rust_decimal" "dep:rust_decimal") ("rocket" "dep:rocket" "std")))) (rust-version "1.69")))

(define-public crate-byte-unit-5 (crate (name "byte-unit") (vers "5.1.1") (deps (list (crate-dep (name "rocket") (req "^0.5") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "rust_decimal") (req "^1") (optional #t) (kind 0)) (crate-dep (name "serde") (req "^1") (features (quote ("alloc"))) (optional #t) (kind 0)) (crate-dep (name "serde_json") (req "^1") (default-features #t) (kind 2)) (crate-dep (name "utf8-width") (req "^0.1") (default-features #t) (kind 0)))) (hash "0wa6yqb3dxfrmbf7wqidnhd8cqi4n3i5pkqgybkhqj3mdxvs1sma") (features (quote (("u128") ("default" "std" "byte") ("byte" "rust_decimal") ("bit" "rust_decimal")))) (v 2) (features2 (quote (("std" "serde?/std" "rust_decimal?/std") ("serde" "dep:serde") ("rust_decimal" "dep:rust_decimal") ("rocket" "dep:rocket" "std")))) (rust-version "1.69")))

(define-public crate-byte-unit-5 (crate (name "byte-unit") (vers "5.1.2") (deps (list (crate-dep (name "rocket") (req "^0.5") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "rust_decimal") (req "^1") (optional #t) (kind 0)) (crate-dep (name "serde") (req "^1") (features (quote ("alloc"))) (optional #t) (kind 0)) (crate-dep (name "serde_json") (req "^1") (default-features #t) (kind 2)) (crate-dep (name "utf8-width") (req "^0.1") (default-features #t) (kind 0)))) (hash "1b1hchjm7zk73pzjmq4vlglxmd21kwsf6ljsvphv8qd140ab81fl") (features (quote (("u128") ("default" "std" "byte") ("byte" "rust_decimal") ("bit" "rust_decimal")))) (v 2) (features2 (quote (("std" "serde?/std" "rust_decimal?/std") ("serde" "dep:serde") ("rust_decimal" "dep:rust_decimal") ("rocket" "dep:rocket" "std")))) (rust-version "1.69")))

(define-public crate-byte-unit-5 (crate (name "byte-unit") (vers "5.1.3") (deps (list (crate-dep (name "rocket") (req "^0.5") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "rust_decimal") (req "^1") (optional #t) (kind 0)) (crate-dep (name "serde") (req "^1") (features (quote ("alloc"))) (optional #t) (kind 0)) (crate-dep (name "serde_json") (req "^1") (default-features #t) (kind 2)) (crate-dep (name "utf8-width") (req "^0.1") (default-features #t) (kind 0)))) (hash "0ihblvhqlb68gsyyvaja946ypig0pk5cw1m5d9a0h3g7dchjgnnb") (features (quote (("u128") ("default" "std" "byte") ("byte" "rust_decimal") ("bit" "rust_decimal")))) (v 2) (features2 (quote (("std" "serde?/std" "rust_decimal?/std") ("serde" "dep:serde") ("rust_decimal" "dep:rust_decimal") ("rocket" "dep:rocket" "std")))) (rust-version "1.69")))

(define-public crate-byte-unit-5 (crate (name "byte-unit") (vers "5.1.4") (deps (list (crate-dep (name "rocket") (req "^0.5") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "rust_decimal") (req "^1") (optional #t) (kind 0)) (crate-dep (name "serde") (req "^1") (features (quote ("alloc"))) (optional #t) (kind 0)) (crate-dep (name "serde_json") (req "^1") (default-features #t) (kind 2)) (crate-dep (name "utf8-width") (req "^0.1") (default-features #t) (kind 0)))) (hash "0gnnl92szl7d6bxz028n03ifflg96z4xp0lxqc3m8rmjy2yikb1k") (features (quote (("u128") ("default" "std" "byte") ("byte" "rust_decimal") ("bit" "rust_decimal")))) (v 2) (features2 (quote (("std" "serde?/std" "rust_decimal?/std") ("serde" "dep:serde") ("rust_decimal" "dep:rust_decimal") ("rocket" "dep:rocket" "std")))) (rust-version "1.69")))

(define-public crate-byte-unit-serde-0.1 (crate (name "byte-unit-serde") (vers "0.1.0") (deps (list (crate-dep (name "byte-unit") (req "^5") (features (quote ("serde"))) (default-features #t) (kind 0)) (crate-dep (name "include-utils") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "pretty_assertions") (req "^1.4.0") (default-features #t) (kind 2)) (crate-dep (name "serde") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1") (features (quote ("derive"))) (default-features #t) (kind 2)) (crate-dep (name "serde_json") (req "^1") (default-features #t) (kind 2)))) (hash "0cccgk3ifx9bcd60pfzl86mv3kk1l6prdnlkzhhakpsq865xjmzx") (features (quote (("default"))))))

(define-public crate-byte_arithmetic-0.2 (crate (name "byte_arithmetic") (vers "0.2.0") (deps (list (crate-dep (name "itertools") (req "^0.10") (default-features #t) (kind 0)))) (hash "1zd550jqpxi83bq5xw7nacv2zncl71x5rvqj3k4rc7ypkq0v9n1x")))

(define-public crate-byte_arithmetic-0.2 (crate (name "byte_arithmetic") (vers "0.2.1") (deps (list (crate-dep (name "itertools") (req "^0.10") (default-features #t) (kind 0)))) (hash "1x2czx3ag9q61s5phc7yqrllmw6kcxc1n7rp8zx968pbc94cd1ga")))

(define-public crate-byte_arithmetic-0.2 (crate (name "byte_arithmetic") (vers "0.2.2") (deps (list (crate-dep (name "itertools") (req "^0.10") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.145") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "0cp4mg1q813vaxfxw3x0w19qiz365ibhz1hm6wf4f058gyipbyzr")))

(define-public crate-byte_arithmetic-0.3 (crate (name "byte_arithmetic") (vers "0.3.0") (deps (list (crate-dep (name "itertools") (req "^0.10") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.145") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "000shsgivlddpfvmclxd17l3zzvk5csdqv87k2h50mqp65900zyl")))

(define-public crate-byte_arithmetic-0.3 (crate (name "byte_arithmetic") (vers "0.3.1") (deps (list (crate-dep (name "itertools") (req "^0.10") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.145") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "08vzimwxqgz9p92ln9qcl87wgp8wric1nm652prqx8d4zjilr62n")))

(define-public crate-byte_array-0.1 (crate (name "byte_array") (vers "0.1.0") (deps (list (crate-dep (name "byteorder") (req "^1") (default-features #t) (kind 0)))) (hash "1zv3kf20skacxfy7yia9bpqnid8si7gyn5748w91d3sdbsgm8sax")))

(define-public crate-byte_array-0.1 (crate (name "byte_array") (vers "0.1.1") (deps (list (crate-dep (name "byteorder") (req "^1") (default-features #t) (kind 0)))) (hash "0j0qj2vlimrwc77vaxjhv7jn9i6ihgiwq18yzmd44j5c3mgy7yr3")))

(define-public crate-byte_array-0.1 (crate (name "byte_array") (vers "0.1.2") (deps (list (crate-dep (name "byteorder") (req "^1") (default-features #t) (kind 0)))) (hash "0bkbfpgamirz3apapd2a0q2siircdx83bn3hsj9fzk1w7hcra0ks")))

(define-public crate-byte_array-0.1 (crate (name "byte_array") (vers "0.1.3") (deps (list (crate-dep (name "byteorder") (req "^1") (default-features #t) (kind 0)))) (hash "18sv2bai22n1y3m4n2xbnbndm6m0iv2rmlfj7g41rrpl5g0slg1h")))

(define-public crate-byte_bite-0.1 (crate (name "byte_bite") (vers "0.1.0") (deps (list (crate-dep (name "chrono") (req "^0.4") (features (quote ("serde"))) (default-features #t) (kind 0)) (crate-dep (name "crossterm") (req "^0.19") (features (quote ("serde"))) (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "log4rs") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.11") (features (quote ("json" "blocking"))) (default-features #t) (kind 0)) (crate-dep (name "rss") (req "^2.0.2") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.12.0") (features (quote ("full"))) (default-features #t) (kind 0)) (crate-dep (name "tui") (req "^0.14") (features (quote ("crossterm" "serde"))) (kind 0)) (crate-dep (name "unicode-width") (req "^0.1.10") (default-features #t) (kind 0)))) (hash "0r3hx7yqi4cz37882xdhn1kmxif4y46qcgbc09gnmc36fpvxsf7n")))

(define-public crate-byte_bite-0.1 (crate (name "byte_bite") (vers "0.1.1") (deps (list (crate-dep (name "chrono") (req "^0.4") (features (quote ("serde"))) (default-features #t) (kind 0)) (crate-dep (name "crossterm") (req "^0.19") (features (quote ("serde"))) (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "log4rs") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.11") (features (quote ("json" "blocking"))) (default-features #t) (kind 0)) (crate-dep (name "rss") (req "^2.0.2") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.12.0") (features (quote ("full"))) (default-features #t) (kind 0)) (crate-dep (name "tui") (req "^0.14") (features (quote ("crossterm" "serde"))) (kind 0)) (crate-dep (name "unicode-width") (req "^0.1.10") (default-features #t) (kind 0)))) (hash "1yc21lh2xb28186cbdkp3gxk6sw3m427d5h5aplwml452frasz39")))

(define-public crate-byte_bite-0.1 (crate (name "byte_bite") (vers "0.1.2") (deps (list (crate-dep (name "chrono") (req "^0.4") (features (quote ("serde"))) (default-features #t) (kind 0)) (crate-dep (name "crossterm") (req "^0.19") (features (quote ("serde"))) (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "log4rs") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.11") (features (quote ("json" "blocking"))) (default-features #t) (kind 0)) (crate-dep (name "rss") (req "^2.0.2") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.12.0") (features (quote ("full"))) (default-features #t) (kind 0)) (crate-dep (name "tui") (req "^0.14") (features (quote ("crossterm" "serde"))) (kind 0)) (crate-dep (name "unicode-width") (req "^0.1.10") (default-features #t) (kind 0)))) (hash "1mvcxwdzwa6hbvqnhnn8kgivk00dsra13c5vpiqhljmvzvskfbbr") (yanked #t)))

(define-public crate-byte_bite-0.1 (crate (name "byte_bite") (vers "0.1.3") (deps (list (crate-dep (name "chrono") (req "^0.4") (features (quote ("serde"))) (default-features #t) (kind 0)) (crate-dep (name "crossterm") (req "^0.19") (features (quote ("serde"))) (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "log4rs") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.11") (features (quote ("json" "blocking"))) (default-features #t) (kind 0)) (crate-dep (name "rss") (req "^2.0.2") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.12.0") (features (quote ("full"))) (default-features #t) (kind 0)) (crate-dep (name "tui") (req "^0.14") (features (quote ("crossterm" "serde"))) (kind 0)) (crate-dep (name "unicode-width") (req "^0.1.10") (default-features #t) (kind 0)))) (hash "1v3y0ch24q0s3w7cvri0pdv0298pjpgwwmhbyink4flc9av5ymyc")))

(define-public crate-byte_bite-0.1 (crate (name "byte_bite") (vers "0.1.4") (deps (list (crate-dep (name "chrono") (req "^0.4") (features (quote ("serde"))) (default-features #t) (kind 0)) (crate-dep (name "crossterm") (req "^0.19") (features (quote ("serde"))) (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "log4rs") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.11") (features (quote ("json" "blocking"))) (default-features #t) (kind 0)) (crate-dep (name "rss") (req "^2.0.2") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.12.0") (features (quote ("full"))) (default-features #t) (kind 0)) (crate-dep (name "tui") (req "^0.14") (features (quote ("crossterm" "serde"))) (kind 0)) (crate-dep (name "unicode-width") (req "^0.1.10") (default-features #t) (kind 0)))) (hash "1yncvwvddvik179nn306fy0g726pnqk71qh3x3z4ih43r5kw1dza")))

(define-public crate-byte_bite-0.1 (crate (name "byte_bite") (vers "0.1.5") (deps (list (crate-dep (name "chrono") (req "^0.4") (features (quote ("serde"))) (default-features #t) (kind 0)) (crate-dep (name "crossterm") (req "^0.19") (features (quote ("serde"))) (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "log4rs") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.11") (features (quote ("json" "blocking"))) (default-features #t) (kind 0)) (crate-dep (name "rss") (req "^2.0.2") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.12.0") (features (quote ("full"))) (default-features #t) (kind 0)) (crate-dep (name "tui") (req "^0.14") (features (quote ("crossterm" "serde"))) (kind 0)) (crate-dep (name "unicode-width") (req "^0.1.10") (default-features #t) (kind 0)))) (hash "0yvnfah634j9zimlcp8mbs7s5i17d9q9wmfs2kryn8dx9pip5alq")))

(define-public crate-byte_bite-0.1 (crate (name "byte_bite") (vers "0.1.6") (deps (list (crate-dep (name "chrono") (req "^0.4") (features (quote ("serde"))) (default-features #t) (kind 0)) (crate-dep (name "crossterm") (req "^0.19") (features (quote ("serde"))) (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "log4rs") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.11") (features (quote ("json" "blocking"))) (default-features #t) (kind 0)) (crate-dep (name "rss") (req "^2.0.2") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.12.0") (features (quote ("full"))) (default-features #t) (kind 0)) (crate-dep (name "tui") (req "^0.14") (features (quote ("crossterm" "serde"))) (kind 0)) (crate-dep (name "unicode-width") (req "^0.1.10") (default-features #t) (kind 0)))) (hash "18fnnk1pzmyci03llh3a64qwbj5h702z5rm19kmwmka7148fkmbp")))

(define-public crate-byte_buffer-0.1 (crate (name "byte_buffer") (vers "0.1.0") (deps (list (crate-dep (name "crossbeam-channel") (req "^0.3.0") (default-features #t) (kind 0)))) (hash "1j80ipaq6xkxlvr37fi1mn1n5839qwglwrjiypyj8dmclhaq68rv")))

(define-public crate-byte_buffer-0.1 (crate (name "byte_buffer") (vers "0.1.1") (deps (list (crate-dep (name "crossbeam-channel") (req "^0.3.0") (default-features #t) (kind 0)))) (hash "1ybsfhzzpf2692axiksklp9bz39qnk2gxp1nivwyyb6js8pgqi2l")))

(define-public crate-byte_buffer-0.1 (crate (name "byte_buffer") (vers "0.1.2") (deps (list (crate-dep (name "crossbeam-channel") (req "^0.3.0") (default-features #t) (kind 0)))) (hash "00y7afqnh2vsn1iyq3wpzpyv7flshyfq8gymzsai9pzlkrdmpl92") (yanked #t)))

(define-public crate-byte_buffer-0.1 (crate (name "byte_buffer") (vers "0.1.3") (deps (list (crate-dep (name "crossbeam-channel") (req "^0.3.0") (default-features #t) (kind 0)))) (hash "0n2qk6xhx45ir8y7kzrk4wgk3i5iblblpx3qfwxh7rsqlximkj6y")))

(define-public crate-byte_channel-0.0.1 (crate (name "byte_channel") (vers "0.0.1") (deps (list (crate-dep (name "bytes") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "futures") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "test_futures") (req "^0.0.1") (default-features #t) (kind 2)))) (hash "1bm1ys6zi3s9amn1r3q9i6gz41vkx3pdzmxz7vli6gcqnpg4mqq2")))

(define-public crate-byte_chunk-0.1 (crate (name "byte_chunk") (vers "0.1.0") (deps (list (crate-dep (name "bytes") (req "^1.0.1") (default-features #t) (kind 0)))) (hash "0z3q3fl739hsigkrcbm011gkw956nfkawxx65jaxs7yvk9v0dn0k")))

(define-public crate-byte_chunk-0.1 (crate (name "byte_chunk") (vers "0.1.1") (deps (list (crate-dep (name "bytes") (req "^1.0.1") (default-features #t) (kind 0)))) (hash "0w8qank5iia3y7mbm2dvkk651sa2ks8gmaxbcfqaal9mjkp0z77s")))

(define-public crate-byte_consumer-0.1 (crate (name "byte_consumer") (vers "0.1.0") (hash "1cjw68gxcvq7j7hcqfff8by1fg33w6sj40426xp3dk9ahw2r0fl9")))

(define-public crate-byte_consumer-0.2 (crate (name "byte_consumer") (vers "0.2.0") (hash "1d6zsxd1qqn4j6198v67a7n18y1w7z0dvkcw2sjzfgs00d0mqc4q")))

(define-public crate-byte_conv-0.1 (crate (name "byte_conv") (vers "0.1.0") (hash "00q9d9dmg56a2kwwa13mqlii9zcmk8l2lxs4j9hl71rpisw9c7ir")))

(define-public crate-byte_conv-0.1 (crate (name "byte_conv") (vers "0.1.1") (hash "102p9cdcj1a2n3g7pdd5jvbpl9gfjp5g6azw4rx16ca9blqp56b4")))

(define-public crate-byte_counter-0.1 (crate (name "byte_counter") (vers "0.1.0") (deps (list (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 2)) (crate-dep (name "serde") (req "^1.0.171") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0.171") (default-features #t) (kind 0)))) (hash "1bb2bir46izyzg0cfh46x7jp42klhis55w6qadpfs4bkwn55gkdi")))

(define-public crate-byte_counter-0.2 (crate (name "byte_counter") (vers "0.2.0") (deps (list (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 2)) (crate-dep (name "serde") (req "^1.0.171") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0.171") (default-features #t) (kind 0)))) (hash "0bj9izhcpflvpag24dd19i9vqgar9qm008i3xmg889pqyjcr4ck7")))

(define-public crate-byte_counter-0.3 (crate (name "byte_counter") (vers "0.3.0") (deps (list (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 2)) (crate-dep (name "serde") (req "^1.0.171") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0.171") (default-features #t) (kind 0)))) (hash "11hvc98rlzw97all6azrcvb9q03ya3xzw4q5zzmqq9kn7bf8p783")))

(define-public crate-byte_eater-0.1 (crate (name "byte_eater") (vers "0.1.0") (deps (list (crate-dep (name "darling") (req "^0.13") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (default-features #t) (kind 0)))) (hash "19q73wjnpfgbr6lv3h5qpgw2y2q8ni99ni4q9rcz9iha38xq06yg") (yanked #t)))

(define-public crate-byte_eater-0.1 (crate (name "byte_eater") (vers "0.1.1") (deps (list (crate-dep (name "darling") (req "^0.13") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (default-features #t) (kind 0)))) (hash "0clh5w79ppcywxb0mb77bc7qx5nr3n1slnbavdpag84bqylngn22")))

(define-public crate-byte_lamination-0.1 (crate (name "byte_lamination") (vers "0.1.0") (deps (list (crate-dep (name "serde") (req "^1.0.162") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde_bare") (req "^0.5.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde_cbor") (req "^0.11.2") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "zstd") (req "^0.12.3") (optional #t) (default-features #t) (kind 0)))) (hash "05hb9dy53bha38a7hbcmp5y8h624c0vjykm31vah8jrm83d6amz4") (features (quote (("cbor" "serde_cbor" "serde") ("bare" "serde_bare" "serde") ("all" "zstd" "cbor" "bare"))))))

(define-public crate-byte_lamination-0.1 (crate (name "byte_lamination") (vers "0.1.1") (deps (list (crate-dep (name "serde") (req "^1.0.162") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde_bare") (req "^0.5.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde_cbor") (req "^0.11.2") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "zstd") (req "^0.12.3") (optional #t) (default-features #t) (kind 0)))) (hash "1zphl1j6yp5gr2ma88c8zmgdlm7vih7289c4qnm4jsf1nx9c5xiy") (features (quote (("cbor" "serde_cbor" "serde") ("bare" "serde_bare" "serde") ("all" "zstd" "cbor" "bare"))))))

(define-public crate-byte_lamination-0.1 (crate (name "byte_lamination") (vers "0.1.2") (deps (list (crate-dep (name "serde") (req "^1.0.162") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde_bare") (req "^0.5.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde_cbor") (req "^0.11.2") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "zstd") (req "^0.12.3") (optional #t) (default-features #t) (kind 0)))) (hash "0rg49ax0akma8bp2gpca8layy27477kdv5i08ag01gb2nrnm3vlx") (features (quote (("cbor" "serde_cbor" "serde") ("bare" "serde_bare" "serde") ("all" "zstd" "cbor" "bare"))))))

(define-public crate-byte_lines-0.1 (crate (name "byte_lines") (vers "0.1.0") (deps (list (crate-dep (name "byte_string") (req "^1.0.0") (default-features #t) (kind 0)))) (hash "12cld3nf6jqkwlryf2hxdl0300zhj3fca22l202b517jhpb9438g")))

(define-public crate-byte_lines-0.1 (crate (name "byte_lines") (vers "0.1.1") (deps (list (crate-dep (name "byte_string") (req "^1.0.0") (default-features #t) (kind 0)))) (hash "138xwm12nn9b03n7n0aayx0x7hm7s1crj6xwjmv2jnq6zq5sf1m1")))

(define-public crate-byte_marks-0.1 (crate (name "byte_marks") (vers "0.1.0") (hash "0mn6k0adzf7ryd0vvc3g9wp9sjjajq4hs99d32cinibfk7baywk7") (features (quote (("std") ("default" "std")))) (yanked #t)))

(define-public crate-byte_marks-0.1 (crate (name "byte_marks") (vers "0.1.1") (deps (list (crate-dep (name "async-std") (req "^1.10.0") (features (quote ("attributes"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "futures") (req "^0.3.18") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)))) (hash "0vzy5pcxc2xbqk32ibrgyp1n4g6yjszsg92ivxyqri9svv65xvn7") (features (quote (("std") ("default_async" "async-std" "futures") ("default" "std"))))))

(define-public crate-byte_marks-0.1 (crate (name "byte_marks") (vers "0.1.2") (deps (list (crate-dep (name "async-std") (req "^1.10.0") (features (quote ("attributes"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "futures") (req "^0.3.18") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.4") (default-features #t) (kind 0)))) (hash "0dh50yqdjw1sdq78dbb28q22cs34ai9bxwmfrvgcn2ryk50nxpqd") (features (quote (("std") ("default_async" "async-std" "futures") ("default" "std"))))))

(define-public crate-byte_marks-0.1 (crate (name "byte_marks") (vers "0.1.3") (deps (list (crate-dep (name "async-std") (req "^1.10.0") (features (quote ("attributes"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "futures") (req "^0.3.18") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.4") (default-features #t) (kind 0)))) (hash "1jhwfddxgs8az2kcwzwxzbr89j815m27339i0fjzkf68rrzrzykx") (features (quote (("std") ("default_async" "async-std" "futures") ("default" "std"))))))

(define-public crate-byte_marks-0.1 (crate (name "byte_marks") (vers "0.1.4") (deps (list (crate-dep (name "async-std") (req "^1.10.0") (features (quote ("attributes"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "futures") (req "^0.3.17") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.4") (default-features #t) (kind 2)))) (hash "17kvaxvzl0baq36amizsj3svbihvqky2yj9cxrv037g3rkn862ja") (features (quote (("std") ("default_async" "async-std") ("default" "std"))))))

(define-public crate-byte_marks-0.1 (crate (name "byte_marks") (vers "0.1.5") (deps (list (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)))) (hash "0g25fqhk7v3v6mqyp30si819k2436kgja4x4nqpwdh6lgrqhyg3j")))

(define-public crate-byte_marks-0.1 (crate (name "byte_marks") (vers "0.1.6") (deps (list (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.4") (default-features #t) (kind 2)))) (hash "0fl20vy88ma92pwr4q5xdxndvlblyk5pvg4h6bwj5awwg20fpvix")))

(define-public crate-byte_marks-0.1 (crate (name "byte_marks") (vers "0.1.7") (deps (list (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.4") (default-features #t) (kind 2)))) (hash "0qbrk9i114y2js6ld5f8hrndkwh7i17g3bddxkrwinlchlzim3fn")))

(define-public crate-byte_marks-0.1 (crate (name "byte_marks") (vers "0.1.8") (deps (list (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.4") (default-features #t) (kind 2)))) (hash "1sq7jk370zagj6ism7g6scps2c2ygnlsllcsg569svz3nhx9mi5h")))

(define-public crate-byte_marks-0.1 (crate (name "byte_marks") (vers "0.1.9") (deps (list (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.4") (default-features #t) (kind 2)))) (hash "0kbk5c3nys5w5s0w9pfi66jv5pf9c3831hxdwzgfcz5y02ddavfp")))

(define-public crate-byte_marks-0.1 (crate (name "byte_marks") (vers "0.1.10") (deps (list (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.4") (default-features #t) (kind 2)))) (hash "13cgf2irfapzn4hm2xcxvwj2f2ds9lg1kq79qdz4mg2ylnnkmh49")))

(define-public crate-byte_marks-0.1 (crate (name "byte_marks") (vers "0.1.11") (deps (list (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.4") (default-features #t) (kind 2)))) (hash "0mbwgl6a1xgwnm8aw19hj82bi0hyn2070zzsm9179xps4icd58cn")))

(define-public crate-byte_muck-1 (crate (name "byte_muck") (vers "1.0.0") (hash "1vrzx7shxna02l0l7lzji0vzxhbg630j6dxi8hdg4jdpajblwa4h")))

(define-public crate-byte_operations-0.1 (crate (name "byte_operations") (vers "0.1.0") (hash "1mhbgag5zzv73h9flxffqnvn0fz68my3hfyfgyf56m4ywrcnywib") (yanked #t)))

(define-public crate-byte_prefix-1 (crate (name "byte_prefix") (vers "1.0.0") (hash "0kc9fr2h35f8jjx0gyzqafd93jaj6rf17gqzysbjifrrj5bdv6qk")))

(define-public crate-byte_rb-1 (crate (name "byte_rb") (vers "1.0.0") (hash "1vj0glsngchzxjmmrjj4q253c41zylcdgjfw5i0gz1q1s5hcd9zr") (yanked #t)))

(define-public crate-byte_rb-1 (crate (name "byte_rb") (vers "1.0.1") (hash "08xagk7vz7yqjvychbknpgsyv1xypdh5yyc6lpygfq1vxhrm77rz") (yanked #t)))

(define-public crate-byte_rb-1 (crate (name "byte_rb") (vers "1.0.2") (hash "120jw2f11i7mi482psfsr8836f9j3d3cwr0j3svkh62s824zm56y") (yanked #t)))

(define-public crate-byte_rb-1 (crate (name "byte_rb") (vers "1.0.3") (hash "1c2nm57p82vzj29ii3w20q4wah105p0ska11p8vphvw6dc9sdycf") (yanked #t)))

(define-public crate-byte_rb-1 (crate (name "byte_rb") (vers "1.0.4") (hash "01x7n2fg6z7mw04gcjx2v71aajml9d3wjz928cfrp7nw5apcg4gb")))

(define-public crate-byte_reader-0.1 (crate (name "byte_reader") (vers "0.1.0") (hash "0z55vyijxi7alnr1vcrwq1zxnhzkicbm7fkmm77qkxsakh9fkdf2")))

(define-public crate-byte_reader-0.1 (crate (name "byte_reader") (vers "0.1.1") (hash "0ny16kr3ig9cdgrq9k8h5v69pmf2cc795xpv147l9vldzpvrx2yv") (features (quote (("location"))))))

(define-public crate-byte_reader-0.1 (crate (name "byte_reader") (vers "0.1.2") (hash "0cwy4gbwrq0snnpihp5dzl5nncph1x7r5sarzsd01jns54ar7ayc") (features (quote (("location"))))))

(define-public crate-byte_reader-0.1 (crate (name "byte_reader") (vers "0.1.3") (hash "01w86v5qj9jfn74xygy3shlbg7vp49qn7b5icy3n1wnrcp5lxw23") (features (quote (("location"))))))

(define-public crate-byte_reader-0.2 (crate (name "byte_reader") (vers "0.2.0") (hash "0kwnm4kmkkl5aq9c4vw603x5dhmmrf2icbw689mancjh0dk8dss3") (features (quote (("location"))))))

(define-public crate-byte_reader-0.3 (crate (name "byte_reader") (vers "0.3.0") (hash "1741mddgzcjh79h3kpvj6a4gd9hi0b2bwvsb4ahx844k2yj8l7pl") (features (quote (("location"))))))

(define-public crate-byte_reader-0.3 (crate (name "byte_reader") (vers "0.3.1") (hash "00y5c6m5s4aj613gjfsyj47076a7p7dx3qdsn7bn98511sb1nj88") (features (quote (("location"))))))

(define-public crate-byte_reader-0.3 (crate (name "byte_reader") (vers "0.3.2") (hash "1m7msmqjab0v0xnvyx9hb1fh9jm18ys5c27gh68bk384fpzypp8z") (features (quote (("location"))))))

(define-public crate-byte_reader-0.3 (crate (name "byte_reader") (vers "0.3.3") (hash "1hr1l5xqd0ccdqmm0k375fvqgxzap3iy0lybj2qi8i1n85jaf7nm") (features (quote (("location"))))))

(define-public crate-byte_reader-0.3 (crate (name "byte_reader") (vers "0.3.4") (hash "1cb02qp3n37ccvz73vywrdppjll2bsxhz3rsc92hhsqk37j1y16f") (features (quote (("location"))))))

(define-public crate-byte_reader-0.3 (crate (name "byte_reader") (vers "0.3.5") (hash "1k1faqpbclcyl4188m8kj7p940cb6qd3cy1i8hcwjnmrgikg8blq") (features (quote (("location"))))))

(define-public crate-byte_reader-0.3 (crate (name "byte_reader") (vers "0.3.6") (hash "0fp5y0zg8vkdyjp9dn6nr8iiwi4pxslf8rjd15inziq2xkanmch9") (features (quote (("location")))) (yanked #t)))

(define-public crate-byte_reader-0.4 (crate (name "byte_reader") (vers "0.4.0") (hash "169c565dqfx07j7krcvrkbbdgd7ics1wgnrr362n8kknpgjhixrn") (features (quote (("location"))))))

(define-public crate-byte_reader-0.5 (crate (name "byte_reader") (vers "0.5.0") (hash "11rvs85qqpwq7cfvm617y91imbq7i9nfjmlcw8paiwcfhkqmh0fh") (features (quote (("location"))))))

(define-public crate-byte_reader-0.5 (crate (name "byte_reader") (vers "0.5.1") (hash "0mr89c1g59glw2z56ymms9lza8lcjh1f33ylyfz0li5g77nm4m1w") (features (quote (("location"))))))

(define-public crate-byte_reader-0.5 (crate (name "byte_reader") (vers "0.5.2") (hash "1yl1zppygzvl3mpngkwf0s70as1i24js3viqsq18vzidd76kcnpa") (features (quote (("location"))))))

(define-public crate-byte_reader-1 (crate (name "byte_reader") (vers "1.0.0") (hash "0vbyjfyyahkrc02af255jz1yykncyj4hl976wf57x1b012n0bkhy") (features (quote (("location"))))))

(define-public crate-byte_reader-1 (crate (name "byte_reader") (vers "1.1.0") (hash "1d5gzb4nzssixxha0af86grxlkg00vpvbszzmbk5imynw24frqj7") (features (quote (("location"))))))

(define-public crate-byte_reader-1 (crate (name "byte_reader") (vers "1.1.1") (hash "0rz36dnlggv27svgyd8avqm8q3gmzkirps1f6p34vlzg4dk9vvkp") (features (quote (("location"))))))

(define-public crate-byte_reader-1 (crate (name "byte_reader") (vers "1.1.2") (hash "1na1vbjzdsq8wi1lmpds4aql2byw78jx7jvv2yvjm2fk1d0psjsh") (features (quote (("location"))))))

(define-public crate-byte_reader-1 (crate (name "byte_reader") (vers "1.2.0") (hash "119jdjd6jyx80kl12qlgp6jnhixnq6zlns0ndk863jdfqcz1cssj") (features (quote (("text") ("location"))))))

(define-public crate-byte_reader-1 (crate (name "byte_reader") (vers "1.2.1") (hash "05nlbsxjskgbw6flqd83wrvhbvyk5hdq3k6ilvnk6im4vn3jgg5d") (features (quote (("text") ("location"))))))

(define-public crate-byte_reader-1 (crate (name "byte_reader") (vers "1.2.2") (hash "0wiv16g1zp555w4k44r7krxskd3gwdcraijlk3cgj95pn9iw6j2w") (features (quote (("text") ("location"))))))

(define-public crate-byte_reader-2 (crate (name "byte_reader") (vers "2.0.0") (hash "01njrarf5xxzzbp7jhr4l7nz6wz34by9lzgwq3cfd6if0vy1hyh8") (features (quote (("text") ("location"))))))

(define-public crate-byte_reader-2 (crate (name "byte_reader") (vers "2.1.0") (hash "02n4k7qh7wy456w58ymff61ckcqyi7d5yvc5vi9d9qq2272m2bdv") (features (quote (("text") ("location") ("detached"))))))

(define-public crate-byte_reader-3 (crate (name "byte_reader") (vers "3.0.0") (hash "1pvnnp4qm211csxgmp41c9cbgnwq5kba5nbf4hqlhsay8psngb0g") (features (quote (("text") ("location"))))))

(define-public crate-byte_set-0.1 (crate (name "byte_set") (vers "0.1.0") (deps (list (crate-dep (name "criterion") (req "^0.3.2") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.7") (optional #t) (kind 0)) (crate-dep (name "rand") (req "^0.7") (default-features #t) (kind 2)) (crate-dep (name "rand_core") (req "^0.5") (optional #t) (kind 0)))) (hash "1gig252fshx14lpf33r0pnrrd6bsqy29jnn39kl9ykl80gnay1l9")))

(define-public crate-byte_set-0.1 (crate (name "byte_set") (vers "0.1.1") (deps (list (crate-dep (name "criterion") (req "^0.3.2") (default-features #t) (kind 2)) (crate-dep (name "hashbrown") (req "^0.7.2") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.7") (optional #t) (kind 0)) (crate-dep (name "rand") (req "^0.7") (default-features #t) (kind 2)) (crate-dep (name "rand_core") (req "^0.5") (optional #t) (kind 0)))) (hash "1qzgld7cghihkmb495yxgsy7wp5da9p060jb3w0y55b144a43yzz")))

(define-public crate-byte_set-0.1 (crate (name "byte_set") (vers "0.1.2") (deps (list (crate-dep (name "criterion") (req "^0.3.2") (default-features #t) (kind 2)) (crate-dep (name "hashbrown") (req "^0.7.2") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.7") (optional #t) (kind 0)) (crate-dep (name "rand") (req "^0.7") (default-features #t) (kind 2)) (crate-dep (name "rand_core") (req "^0.5") (optional #t) (kind 0)))) (hash "08802c67dkn4yzham1lrbxjbd9gfzx2jk0lbdsc2jmh33wd60gq8")))

(define-public crate-byte_set-0.1 (crate (name "byte_set") (vers "0.1.3") (deps (list (crate-dep (name "criterion") (req "^0.3.2") (default-features #t) (kind 2)) (crate-dep (name "fixedbitset") (req "^0.3.0") (default-features #t) (kind 2)) (crate-dep (name "hashbrown") (req "^0.7.2") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.7") (optional #t) (kind 0)) (crate-dep (name "rand") (req "^0.7") (default-features #t) (kind 2)) (crate-dep (name "rand_core") (req "^0.5") (optional #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (optional #t) (kind 0)))) (hash "1g73g0w0sk9skf0kff915zhccv4mbqkhwi4mcx96rqdya9y891yh")))

(define-public crate-byte_sha-1 (crate (name "byte_sha") (vers "1.0.0") (deps (list (crate-dep (name "hex_d_hex") (req "^1.0.0") (default-features #t) (kind 0)))) (hash "02vq190y9al1lizdn6bfjsvdq3xyw1zw1ib84ng4k75z4yiqamh8")))

(define-public crate-byte_sha-1 (crate (name "byte_sha") (vers "1.0.1") (deps (list (crate-dep (name "hex_d_hex") (req "^1.0.0") (default-features #t) (kind 0)))) (hash "1wh4pwqa9cwn56rc4zh3fp1jg7vfgigmc4jbwfh69yg4bq8i7php")))

(define-public crate-byte_str-0.1 (crate (name "byte_str") (vers "0.1.0") (hash "1gr5i2c5g2qj7nhs27bz0mfa3cdp78hr007avgikdz7bkkyhm421") (features (quote (("no_std")))) (yanked #t)))

(define-public crate-byte_str-0.1 (crate (name "byte_str") (vers "0.1.1") (hash "0p6fmm3l060dd37blcjfw8vdjdw1gg5mnsv8c4sk6ayxvr54vjid") (features (quote (("no_std")))) (yanked #t)))

(define-public crate-byte_str-0.1 (crate (name "byte_str") (vers "0.1.2") (hash "16dw5k9pgfn7f9h3mi7cpw3ncwqxbx6yddpnghh5hgpykldknw5z") (features (quote (("no_std")))) (yanked #t)))

(define-public crate-byte_stream_splitter-0.1 (crate (name "byte_stream_splitter") (vers "0.1.0") (hash "1g4hq6czms0blnyg7lj5ykg1dcgapip80wwlx8sdp8n17laknbhz")))

(define-public crate-byte_stream_splitter-0.1 (crate (name "byte_stream_splitter") (vers "0.1.1") (hash "1v69pxgp2yya0zjli791l85wlhwpqyg95c1h8k0widmqs64kw9dr")))

(define-public crate-byte_stream_splitter-0.1 (crate (name "byte_stream_splitter") (vers "0.1.2") (hash "1bsn7w5wmly9i91yna26dymbbrj7cp2hs2cmffdl6k453g34b9ll")))

(define-public crate-byte_stream_splitter-0.1 (crate (name "byte_stream_splitter") (vers "0.1.3") (hash "0b648hkcdyqmxzwq2sj9b322flgxi85h0239q8va9hg5aqq0zr8j")))

(define-public crate-byte_stream_splitter-0.1 (crate (name "byte_stream_splitter") (vers "0.1.4") (deps (list (crate-dep (name "time") (req "0.*") (default-features #t) (kind 0)))) (hash "1n6363a7s5bjgd6xllxd2p7dlfx4v7c4fhnm539bcfgyhdgs45h2")))

(define-public crate-byte_stream_splitter-0.2 (crate (name "byte_stream_splitter") (vers "0.2.0") (hash "0wbbvdk38czw8scjq3dsv2b95b9fv2x1wzmrzwgy2cd7r23wr4bn")))

(define-public crate-byte_string-1 (crate (name "byte_string") (vers "1.0.0") (hash "1vdll2i5gdx98d3wqbrq8c2l61jqqhzlri7d3hskm35a0mxdxahi")))

(define-public crate-byte_struct-0.1 (crate (name "byte_struct") (vers "0.1.0") (deps (list (crate-dep (name "byte_struct_derive") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "117rnhiscp7vhg6xzy4733ramzz5pp1j6pz1617admih93hw4wbd")))

(define-public crate-byte_struct-0.2 (crate (name "byte_struct") (vers "0.2.0") (deps (list (crate-dep (name "byte_struct_derive") (req "^0.2.0") (default-features #t) (kind 0)))) (hash "039kdfdssn1dxwnpczgg7d7sazplhncxnwrzcphpiljax7kfss3k")))

(define-public crate-byte_struct-0.3 (crate (name "byte_struct") (vers "0.3.0") (deps (list (crate-dep (name "byte_struct_derive") (req "^0.2.0") (default-features #t) (kind 0)))) (hash "0hdgkp6q9qpja53qy4cncbw2nbjf4bz7r9kh7r5n88vdl7pw28bz")))

(define-public crate-byte_struct-0.4 (crate (name "byte_struct") (vers "0.4.0") (deps (list (crate-dep (name "byte_struct_derive") (req "^0.4.0") (default-features #t) (kind 0)))) (hash "0m1kjlckvpnxhw83d9x9ks2d1ndgvqa6wy2fal2jhhiyrxjv1b22")))

(define-public crate-byte_struct-0.4 (crate (name "byte_struct") (vers "0.4.1") (deps (list (crate-dep (name "byte_struct_derive") (req "^0.4.0") (default-features #t) (kind 0)))) (hash "1fd38h9rp8yb486dkhfn9kcnmxm7sxjbpgczx3xi1vfcrz1h0dci")))

(define-public crate-byte_struct-0.4 (crate (name "byte_struct") (vers "0.4.2") (deps (list (crate-dep (name "byte_struct_derive") (req "^0.4.2") (default-features #t) (kind 0)))) (hash "03d7aabvmj7fj3rprrs3mza2n5vm5p3ny9ihcv8vbna91gk8a751")))

(define-public crate-byte_struct-0.5 (crate (name "byte_struct") (vers "0.5.0") (deps (list (crate-dep (name "byte_struct_derive") (req "^0.4.2") (default-features #t) (kind 0)) (crate-dep (name "generic-array") (req "^0.13") (default-features #t) (kind 0)))) (hash "07lry1kjvkif1pq0kc7i9mfakpmzb5vw5aasdy84mzdkqik6nx93")))

(define-public crate-byte_struct-0.6 (crate (name "byte_struct") (vers "0.6.0") (deps (list (crate-dep (name "byte_struct_derive") (req "^0.4.2") (default-features #t) (kind 0)) (crate-dep (name "generic-array") (req ">= 0.11") (default-features #t) (kind 0)))) (hash "0kjn2wm2647wsc119nwaw319ilyz37jrvwshp5130vad88bjxpiv")))

(define-public crate-byte_struct-0.6 (crate (name "byte_struct") (vers "0.6.1") (deps (list (crate-dep (name "byte_struct_derive") (req "^0.4.2") (default-features #t) (kind 0)) (crate-dep (name "generic-array") (req ">=0.11") (default-features #t) (kind 0)))) (hash "0wp76z3skhikkav4ib27yvr116n7skgqcc7xswx75bjqkyazzp1k")))

(define-public crate-byte_struct-0.7 (crate (name "byte_struct") (vers "0.7.0") (deps (list (crate-dep (name "array-init") (req "^2") (default-features #t) (kind 0)) (crate-dep (name "byte_struct_derive") (req "^0.4.2") (default-features #t) (kind 0)))) (hash "1vqiiggiyaz86my7mn36qsrxc7m3hbr2amrzhpy422qfmla9kmh8")))

(define-public crate-byte_struct-0.7 (crate (name "byte_struct") (vers "0.7.1") (deps (list (crate-dep (name "array-init") (req "^2") (default-features #t) (kind 0)) (crate-dep (name "byte_struct_derive") (req "^0.4.2") (default-features #t) (kind 0)))) (hash "0vx7g6mn3z08ffwn1ypz5wikqmks0c1ancdbf8mvyzmbgjdxxac7")))

(define-public crate-byte_struct-0.8 (crate (name "byte_struct") (vers "0.8.0") (deps (list (crate-dep (name "array-init") (req "^2") (default-features #t) (kind 0)) (crate-dep (name "byte_struct_derive") (req "^0.4.2") (default-features #t) (kind 0)))) (hash "16wgnq8x97vc0cqry636c9glvyfid3k5qrb8q3cl60s915i1dgay")))

(define-public crate-byte_struct-0.9 (crate (name "byte_struct") (vers "0.9.0") (deps (list (crate-dep (name "byte_struct_derive") (req "^0.9.0") (default-features #t) (kind 0)))) (hash "1qk18dyvzb55981z4kr5y0irhjqw9rqv2cdpccf8h3fdz55n0rhs")))

(define-public crate-byte_struct_derive-0.1 (crate (name "byte_struct_derive") (vers "0.1.0") (deps (list (crate-dep (name "proc-macro2") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^0.6.3") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^0.14.4") (default-features #t) (kind 0)))) (hash "1yj11dhcsligjc1g9dw8naml9kfg0bdpckc1zfi8772rif1b5rix")))

(define-public crate-byte_struct_derive-0.2 (crate (name "byte_struct_derive") (vers "0.2.0") (deps (list (crate-dep (name "proc-macro2") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^0.6.3") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^0.14.4") (default-features #t) (kind 0)))) (hash "14nvbhbyhxvz7h1yyv861v05nsqfl5i1lif3c9lw5iyvr95xi5w3")))

(define-public crate-byte_struct_derive-0.4 (crate (name "byte_struct_derive") (vers "0.4.0") (deps (list (crate-dep (name "proc-macro2") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^0.6.3") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^0.14.4") (default-features #t) (kind 0)))) (hash "1h03xxdf91nng117pz5wrv5h01v0wz1h3lh24s1awplwfnac25nh")))

(define-public crate-byte_struct_derive-0.4 (crate (name "byte_struct_derive") (vers "0.4.2") (deps (list (crate-dep (name "proc-macro2") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^0.6.3") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^0.15.0") (default-features #t) (kind 0)))) (hash "0bhbnhdgvz1rzgzyx42k4m0712qnfs3in7vxam2c1zhawp6yrdkz")))

(define-public crate-byte_struct_derive-0.9 (crate (name "byte_struct_derive") (vers "0.9.0") (deps (list (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2.0") (default-features #t) (kind 0)))) (hash "04arvl1sq2bwl5iahaczywjc5c1545xp491s8xm5f9n8j3hiv307")))

(define-public crate-byte_trie-0.1 (crate (name "byte_trie") (vers "0.1.0") (deps (list (crate-dep (name "serde") (req "^1.0") (optional #t) (default-features #t) (kind 0)))) (hash "0m7xlpi4pgsalyc3pvjhbg2a5qxsqcscdfy7vxhgghiw6f0j2fiq")))

(define-public crate-byte_trie-0.1 (crate (name "byte_trie") (vers "0.1.1") (deps (list (crate-dep (name "serde") (req "^1.0") (optional #t) (default-features #t) (kind 0)))) (hash "0py4571gyhqpzq8jsn0vp241aahfakirf33v98xss81gm5iakfry")))

(define-public crate-byte_trie-0.2 (crate (name "byte_trie") (vers "0.2.0") (deps (list (crate-dep (name "criterion") (req "^0.2") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.6") (default-features #t) (kind 2)) (crate-dep (name "serde") (req "^1.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 2)))) (hash "1mf58k802f96imz5ahz8g4xaz4yrvgkvzhl5q3fqf5pialvy341a")))

(define-public crate-byte_trie-0.3 (crate (name "byte_trie") (vers "0.3.0") (deps (list (crate-dep (name "criterion") (req "^0.2") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.6") (default-features #t) (kind 2)) (crate-dep (name "serde") (req "^1.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 2)))) (hash "1d7v1hps8a1bx9wnj86n9lnx2cr1gl3ppmalx83ackk9ikk1iqn6")))

(define-public crate-byte_units-0.1 (crate (name "byte_units") (vers "0.1.0") (deps (list (crate-dep (name "error-chain") (req "^0.11.0") (default-features #t) (kind 0)))) (hash "01cza6d363qbvz3h8gfrn2jzbypdv1661yak5nbxwxaj6p13yzdd")))

(define-public crate-byte_units-0.1 (crate (name "byte_units") (vers "0.1.1") (deps (list (crate-dep (name "error-chain") (req "^0.11.0") (default-features #t) (kind 0)))) (hash "105hr5mrxbx58k8685jnbjl08iv43msc79g0s30gbg180lqb12lb")))

(define-public crate-byte_units-0.1 (crate (name "byte_units") (vers "0.1.2") (deps (list (crate-dep (name "error-chain") (req "^0.11.0") (default-features #t) (kind 0)))) (hash "0zi5591b23iib30shgz0rbnma89jk9r609s0n8m0nqhpn9dic4j4")))

(define-public crate-byte_units-0.1 (crate (name "byte_units") (vers "0.1.3") (deps (list (crate-dep (name "error-chain") (req "^0.11.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.21") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0.21") (default-features #t) (kind 0)))) (hash "07kmaa712whg7whnivasl4xhhxsqd4c4n8x9skr2n3hiksxfs6cz")))

(define-public crate-byte_utils-0.1 (crate (name "byte_utils") (vers "0.1.0") (hash "1xm5v2c6sdc0chs8qvy8czm5h3mnn9djjir7g20d46fkagrb5hv0") (yanked #t)))

(define-public crate-byte_utils-0.0.1 (crate (name "byte_utils") (vers "0.0.1") (hash "0vyqigkjw1xbw8092zqjj00sb2di79gpk46q89ll424awd3j6gp5") (yanked #t)))

(define-public crate-byte_utils-0.1 (crate (name "byte_utils") (vers "0.1.1") (hash "1x7q93ax5byrbd06x6yi6wzc0lccqrslk83rkzv6saaxjiliwq8k") (yanked #t)))

(define-public crate-bytebeam-esp-rs-0.1 (crate (name "bytebeam-esp-rs") (vers "0.1.0") (deps (list (crate-dep (name "anyhow") (req "^1.0.68") (default-features #t) (kind 0)) (crate-dep (name "embedded-svc") (req "^0.24.0") (default-features #t) (kind 0)) (crate-dep (name "esp-idf-hal") (req "^0.40.1") (default-features #t) (kind 0)) (crate-dep (name "esp-idf-svc") (req "^0.45.0") (default-features #t) (kind 0)) (crate-dep (name "esp-idf-sys") (req "^0.32.1") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.17") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (features (quote ("alloc"))) (kind 0)) (crate-dep (name "static_cell") (req "^1.0.0") (default-features #t) (kind 0)) (crate-dep (name "esp-idf-hal") (req "^0.40.1") (default-features #t) (kind 2)) (crate-dep (name "esp-idf-sys") (req "^0.32.1") (features (quote ("binstart"))) (default-features #t) (kind 2)) (crate-dep (name "toml-cfg") (req "^0.1.3") (default-features #t) (kind 2)) (crate-dep (name "embuild") (req "^0.31") (default-features #t) (kind 1)))) (hash "0v1sif76vq31svwviq4ffgasqkzzwx0wmrjv1zgf365csjgqmqvi")))

(define-public crate-bytebeat-0.1 (crate (name "bytebeat") (vers "0.1.0") (deps (list (crate-dep (name "anyhow") (req "^1.0.70") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^4.1.8") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "cpal") (req "^0.15.2") (default-features #t) (kind 0)))) (hash "1hmildb5ifi4z5593ggdlpp7wdnvscklr5dc4p950qk7x4pjqdal") (features (quote (("default"))))))

(define-public crate-bytebeat-0.2 (crate (name "bytebeat") (vers "0.2.0") (deps (list (crate-dep (name "tempfile") (req "^3.5.0") (default-features #t) (kind 0)))) (hash "1ly5ig9nan1akcahvjcnbar67sldlfwj4mbkhkdv5mr305dnm3w7") (features (quote (("default"))))))

(define-public crate-bytebeat-0.2 (crate (name "bytebeat") (vers "0.2.1") (deps (list (crate-dep (name "tempfile") (req "^3.5.0") (default-features #t) (kind 0)))) (hash "0pl8mpfrprx3xrg9z71ckhxvz3ki7aw6ix246il6wrzzpdxqxwzg") (features (quote (("default"))))))

(define-public crate-bytebeat-0.3 (crate (name "bytebeat") (vers "0.3.0") (deps (list (crate-dep (name "anyhow") (req "^1.0.70") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^4.1.8") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "cpal") (req "^0.15.2") (default-features #t) (kind 0)) (crate-dep (name "tempfile") (req "^3.5.0") (default-features #t) (kind 0)))) (hash "0747pmdq2nbnzpj69ld22fs5fr50galg759m8gn3d88pypbamm5f") (features (quote (("jack" "cpal/jack") ("default"))))))

(define-public crate-bytebeat-0.4 (crate (name "bytebeat") (vers "0.4.0") (deps (list (crate-dep (name "anyhow") (req "^1.0.70") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^4.1.8") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "cpal") (req "^0.15.2") (default-features #t) (kind 0)) (crate-dep (name "crossterm") (req "^0.27.0") (default-features #t) (kind 0)) (crate-dep (name "ratatui") (req "^0.25.0") (default-features #t) (kind 0)))) (hash "0mcdx7lq63k2nxv8j40fqzpahs0l3mqh64ja2azpfhc0sm2yssd7") (features (quote (("jack" "cpal/jack") ("default"))))))

(define-public crate-byteblitz-0.1 (crate (name "byteblitz") (vers "0.1.0") (deps (list (crate-dep (name "anyhow") (req "^1.0.79") (default-features #t) (kind 0)) (crate-dep (name "crossterm") (req "^0.27.0") (default-features #t) (kind 0)) (crate-dep (name "ratatui") (req "^0.25.0") (default-features #t) (kind 0)) (crate-dep (name "strum") (req "^0.25") (default-features #t) (kind 0)) (crate-dep (name "strum_macros") (req "^0.25.3") (default-features #t) (kind 0)))) (hash "1r10bcdh290cj0i911fk749vj7p5pwp5cgi997a2kya2lvia3m36")))

(define-public crate-byteblitz-1 (crate (name "byteblitz") (vers "1.0.0") (deps (list (crate-dep (name "anyhow") (req "^1.0.79") (default-features #t) (kind 0)) (crate-dep (name "crossterm") (req "^0.27.0") (default-features #t) (kind 0)) (crate-dep (name "ratatui") (req "^0.25.0") (default-features #t) (kind 0)) (crate-dep (name "strum") (req "^0.25") (default-features #t) (kind 0)) (crate-dep (name "strum_macros") (req "^0.25.3") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0") (default-features #t) (kind 0)))) (hash "03xcbc8c2qjdb19j48dld2f76zbpmaz0n8p76qdmyzqc8681lf1g")))

(define-public crate-bytebox-0.1 (crate (name "bytebox") (vers "0.1.0") (deps (list (crate-dep (name "dirs") (req "^5.0.1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "rmp-serde") (req "^1.1.2") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "1gb7y0nn4ask7lrj521pmz9vk9pi0iig3mlrfa91zs0azsn8h0wn") (features (quote (("default" "path")))) (v 2) (features2 (quote (("path" "dep:dirs"))))))

(define-public crate-bytebox-0.1 (crate (name "bytebox") (vers "0.1.1") (deps (list (crate-dep (name "bevy") (req "^0.12.1") (optional #t) (kind 0)) (crate-dep (name "bevy") (req "^0.12.1") (kind 2)) (crate-dep (name "dirs") (req "^5.0.1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "rmp-serde") (req "^1.1.2") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "0cvp19l5smkvwrx013bfbn50p43izqjyv353xghssf0m1g333abh") (features (quote (("default" "path")))) (v 2) (features2 (quote (("path" "dep:dirs") ("bevy" "dep:bevy"))))))

(define-public crate-bytebraise-0.1 (crate (name "bytebraise") (vers "0.1.2") (deps (list (crate-dep (name "anyhow") (req "^1.0.56") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^3.1.8") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "conch-parser") (req "^0.1.1") (default-features #t) (kind 0)) (crate-dep (name "cow-utils") (req "^0.1.2") (default-features #t) (kind 0)) (crate-dep (name "derive_builder") (req "^0.11.1") (default-features #t) (kind 0)) (crate-dep (name "derive_more") (req "^0.99.17") (default-features #t) (kind 0)) (crate-dep (name "im-rc") (req "^15.0.0") (default-features #t) (kind 0)) (crate-dep (name "indexmap") (req "^1.8.1") (default-features #t) (kind 0)) (crate-dep (name "itertools") (req "^0.10.3") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "maplit") (req "^1.0.2") (default-features #t) (kind 0)) (crate-dep (name "muncher") (req "^0.7.0") (default-features #t) (kind 0)) (crate-dep (name "nested_intervals") (req "^0.2.2") (default-features #t) (kind 0)) (crate-dep (name "once_cell") (req "^1.10.0") (default-features #t) (kind 0)) (crate-dep (name "paste") (req "^1.0.7") (default-features #t) (kind 0)) (crate-dep (name "phf") (req "^0.10.1") (features (quote ("macros"))) (default-features #t) (kind 0)) (crate-dep (name "pretty_assertions") (req "^1.2.0") (default-features #t) (kind 0)) (crate-dep (name "pyo3") (req "^0.16.2") (features (quote ("auto-initialize" "anyhow"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "python-parser") (req "^0.2.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.5.5") (features (quote ("pattern"))) (default-features #t) (kind 0)) (crate-dep (name "rowan") (req "^0.15.4") (default-features #t) (kind 0)) (crate-dep (name "scopeguard") (req "^1.1.0") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.30") (default-features #t) (kind 0)) (crate-dep (name "unicode-segmentation") (req "^1.9.0") (default-features #t) (kind 0)))) (hash "13bni699n8wrnvwzppzxskqpzdbpm45r5m53dq98wfiql0zgzk0q") (features (quote (("python" "python-parser" "pyo3") ("default"))))))

(define-public crate-bytebraise-0.1 (crate (name "bytebraise") (vers "0.1.3") (deps (list (crate-dep (name "anyhow") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^4") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "conch-parser") (req "^0.1.1") (default-features #t) (kind 0)) (crate-dep (name "cow-utils") (req "^0.1.2") (default-features #t) (kind 0)) (crate-dep (name "derive_builder") (req "^0.12.0") (default-features #t) (kind 0)) (crate-dep (name "derive_more") (req "^0.99.17") (default-features #t) (kind 0)) (crate-dep (name "im-rc") (req "^15") (default-features #t) (kind 0)) (crate-dep (name "indexmap") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "itertools") (req "^0.10.5") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "maplit") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "muncher") (req "^0.7.0") (default-features #t) (kind 0)) (crate-dep (name "nested_intervals") (req "^0.2.2") (default-features #t) (kind 0)) (crate-dep (name "once_cell") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "paste") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "phf") (req "^0.11.1") (features (quote ("macros"))) (default-features #t) (kind 0)) (crate-dep (name "pretty_assertions") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "pyo3") (req "^0.18.2") (features (quote ("auto-initialize" "anyhow"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "python-parser") (req "^0.2.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1") (features (quote ("pattern"))) (default-features #t) (kind 0)) (crate-dep (name "rowan") (req "^0.15.11") (default-features #t) (kind 0)) (crate-dep (name "scopeguard") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "unicode-segmentation") (req "^1") (default-features #t) (kind 0)))) (hash "08zrji6msbpx1yn0875hkwsg2rz7d685rsvw144916bxgn4pyf27") (features (quote (("python" "python-parser" "pyo3") ("default"))))))

(define-public crate-bytebuf-0.0.0 (crate (name "bytebuf") (vers "0.0.0") (hash "0y40p13pd66gxa9q63231mfvmd9hby2q9zmjx95cld9jnrz2vqd4")))

(define-public crate-bytebuff-0.1 (crate (name "bytebuff") (vers "0.1.0") (deps (list (crate-dep (name "marshall_derive") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "11nkhg9rhww6q8wsv0m3526ynvkxgk6zbp3rifw9py0jrcrwx5vi")))

(define-public crate-bytebuff-0.1 (crate (name "bytebuff") (vers "0.1.1") (deps (list (crate-dep (name "marshall_derive") (req "^0.1.1") (default-features #t) (kind 0)))) (hash "1grar9g60f7mws1kdvimw4sbwzxlwsx820qxi9hbsbard13hb0wg")))

(define-public crate-bytebuffer-0.1 (crate (name "bytebuffer") (vers "0.1.0") (deps (list (crate-dep (name "byteorder") (req "^0.3") (default-features #t) (kind 0)))) (hash "0f7994vv8vmhnabidrry4qk22fi3088p66nnpvfglmrwj5g6rx7s")))

(define-public crate-bytebuffer-0.1 (crate (name "bytebuffer") (vers "0.1.1") (deps (list (crate-dep (name "byteorder") (req "^0.3") (default-features #t) (kind 0)))) (hash "1yq9j3320090rl3lrcxwbh7ql49chjn66lgclhy6x4fb2r9d2wc6")))

(define-public crate-bytebuffer-0.2 (crate (name "bytebuffer") (vers "0.2.0") (deps (list (crate-dep (name "byteorder") (req "^0.3") (default-features #t) (kind 0)))) (hash "11947bwxgh7w7ycsb1zqqg0fh3s7nw8ip7p906f27wwdfxjwz0rv")))

(define-public crate-bytebuffer-0.2 (crate (name "bytebuffer") (vers "0.2.1") (deps (list (crate-dep (name "byteorder") (req "^0.3") (default-features #t) (kind 0)))) (hash "1wvcazv8q79maf7i6akf0w4nn6a962s8pbmfaa0q526ki49s48p8")))

(define-public crate-bytebuffer-2 (crate (name "bytebuffer") (vers "2.0.1") (deps (list (crate-dep (name "byteorder") (req "^1") (default-features #t) (kind 0)))) (hash "1s9ay9vv7m2mrnlxwjp5c6kbzc1wgrlqjgjap4z65sfhdivaz4aq")))

(define-public crate-bytebuffer-2 (crate (name "bytebuffer") (vers "2.1.0") (deps (list (crate-dep (name "byteorder") (req "^1") (default-features #t) (kind 0)))) (hash "1dpm676khsb2kcggb9wfbvw7v1g6ar8mny9f6ndbhqsqhzl0nqra")))

(define-public crate-bytebuffer-2 (crate (name "bytebuffer") (vers "2.1.1") (deps (list (crate-dep (name "byteorder") (req "^1") (default-features #t) (kind 0)))) (hash "0yrgqx0l54j2p5b8i35g1hzw3k706rlc3dkpgyp6k40ajspnh96f")))

(define-public crate-bytebuffer-2 (crate (name "bytebuffer") (vers "2.2.0") (deps (list (crate-dep (name "byteorder") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "half") (req "^2.3.1") (optional #t) (default-features #t) (kind 0)))) (hash "0z2pkds7lrfm3v1banifd69br763gb1j3df6rmscvjh8rpvzlyzs") (features (quote (("default")))) (v 2) (features2 (quote (("half" "dep:half"))))))

(define-public crate-bytebuffer-new-0.2 (crate (name "bytebuffer-new") (vers "0.2.2") (deps (list (crate-dep (name "byteorder") (req "^1.2.3") (default-features #t) (kind 0)))) (hash "0navf01y24998phckgcvd85zk0aczvq6ddnw2azr601irsgz4702")))

(define-public crate-bytebuffer-rs-0.3 (crate (name "bytebuffer-rs") (vers "0.3.0") (deps (list (crate-dep (name "byteorder") (req "^0.3") (default-features #t) (kind 0)))) (hash "0xzfd458zhd63cx0h04pvp59ya6pfsn3p7dhy8q2pizwy1692ksb") (yanked #t)))

(define-public crate-bytebuffer-rs-0.3 (crate (name "bytebuffer-rs") (vers "0.3.1") (deps (list (crate-dep (name "byteorder") (req "^1") (default-features #t) (kind 0)))) (hash "03y9a1407j05fjrxassqasgq67caxmv723xbp0dxbhz83mxsqwgx") (yanked #t)))

(define-public crate-bytebuffer-rs-0.4 (crate (name "bytebuffer-rs") (vers "0.4.0") (deps (list (crate-dep (name "byteorder") (req "^1") (default-features #t) (kind 0)))) (hash "0kja4s3qavjg09wnqk096k4zgw33ks1b37kaspzy7ymz61zhnv5n") (yanked #t)))

(define-public crate-bytebuffer-rs-1 (crate (name "bytebuffer-rs") (vers "1.0.0") (deps (list (crate-dep (name "byteorder") (req "^1") (default-features #t) (kind 0)))) (hash "0pc9wpqs9p0wgn2mbfihgdbswj1xd4qqj4lb9245p3ya7qlkjj3v") (yanked #t)))

(define-public crate-bytebuffer-rs-2 (crate (name "bytebuffer-rs") (vers "2.0.0") (deps (list (crate-dep (name "byteorder") (req "^1") (default-features #t) (kind 0)))) (hash "0wm3yk485d182a2m3ihnyw231kbnbhg2n894mphmsipbkx02hk42") (yanked #t)))

(define-public crate-bytebuffer-rs-2 (crate (name "bytebuffer-rs") (vers "2.0.1") (deps (list (crate-dep (name "byteorder") (req "^1") (default-features #t) (kind 0)))) (hash "15iypm8ygn573zbs9jfr5aw4a756rp5cwn5dim5z2ksqcwxnx23z")))

(define-public crate-bytebufrs-0.1 (crate (name "bytebufrs") (vers "0.1.0") (hash "08mvj74n4gpw59i9135grlvhk333p3jy7fn0xi2yi27yvg6ngscw")))

(define-public crate-bytecheck-0.1 (crate (name "bytecheck") (vers "0.1.0") (deps (list (crate-dep (name "bytecheck_derive") (req "=0.1.0") (default-features #t) (kind 0)) (crate-dep (name "bytecheck_derive") (req "^0.1.0") (default-features #t) (kind 2)) (crate-dep (name "memoffset") (req "^0.6") (default-features #t) (kind 0)))) (hash "16smas0j39kdybp5jiqlj0avb0xa4l7zz66r971414cb6c0gv2ly") (features (quote (("std") ("silent") ("default" "std") ("const_generics"))))))

(define-public crate-bytecheck-0.2 (crate (name "bytecheck") (vers "0.2.0") (deps (list (crate-dep (name "bytecheck_derive") (req "=0.2.0") (default-features #t) (kind 0)) (crate-dep (name "bytecheck_derive") (req "^0.2.0") (default-features #t) (kind 2)) (crate-dep (name "memoffset") (req "^0.6") (default-features #t) (kind 0)))) (hash "0m76gy6aw8xayv8wj2gbpkxj64v0c6lflz08fk9pf3cwnnrcwcmp") (features (quote (("default") ("const_generics"))))))

(define-public crate-bytecheck-0.2 (crate (name "bytecheck") (vers "0.2.1") (deps (list (crate-dep (name "bytecheck_derive") (req "=0.2.1") (default-features #t) (kind 0)) (crate-dep (name "bytecheck_derive") (req "^0.2.1") (default-features #t) (kind 2)) (crate-dep (name "memoffset") (req "^0.6") (default-features #t) (kind 0)))) (hash "1q21rxwmjlbmrbsybpvya4lw4dh43lmh0z0gvf6d83qv27n05f8w") (features (quote (("default") ("const_generics"))))))

(define-public crate-bytecheck-0.3 (crate (name "bytecheck") (vers "0.3.0") (deps (list (crate-dep (name "bytecheck_derive") (req "=0.3.0") (default-features #t) (kind 0)) (crate-dep (name "bytecheck_derive") (req "^0.3.0") (default-features #t) (kind 2)) (crate-dep (name "memoffset") (req "^0.6") (default-features #t) (kind 0)))) (hash "17x6fc05lihz661bg52ka9rgcslzjwg5hg77x8rs6v5h84s87y2p") (features (quote (("default") ("const_generics"))))))

(define-public crate-bytecheck-0.4 (crate (name "bytecheck") (vers "0.4.0") (deps (list (crate-dep (name "bytecheck_derive") (req "=0.4.0") (default-features #t) (kind 0)) (crate-dep (name "bytecheck_derive") (req "^0.4.0") (default-features #t) (kind 2)) (crate-dep (name "memoffset") (req "^0.6") (default-features #t) (kind 0)) (crate-dep (name "ptr_meta") (req "^0.1.1") (default-features #t) (kind 0)))) (hash "128gbdanw778159k1mirhb0dx060dvmz1bj9f9pj38hxv5k88kpz") (features (quote (("default") ("const_generics"))))))

(define-public crate-bytecheck-0.4 (crate (name "bytecheck") (vers "0.4.1") (deps (list (crate-dep (name "bytecheck_derive") (req "=0.4.1") (default-features #t) (kind 0)) (crate-dep (name "memoffset") (req "^0.6") (default-features #t) (kind 0)) (crate-dep (name "ptr_meta") (req "^0.1") (default-features #t) (kind 0)))) (hash "04srf65gza7jbrf8xs2fd0l7smb2xhnkmzbsvlkksypzmq6d00fw") (features (quote (("default") ("const_generics"))))))

(define-public crate-bytecheck-0.5 (crate (name "bytecheck") (vers "0.5.0") (deps (list (crate-dep (name "bytecheck_derive") (req "=0.5.0") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "memoffset") (req "^0.6") (default-features #t) (kind 0)) (crate-dep (name "ptr_meta") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "simdutf8") (req "^0.1") (kind 0)))) (hash "0dq9hw9wgcfqzdbqi6j4yyw9aspxdhs9q1vhba5vcz27nqiwwfci") (features (quote (("std" "simdutf8/std") ("full_errors") ("default" "const_generics" "std") ("const_generics"))))))

(define-public crate-bytecheck-0.5 (crate (name "bytecheck") (vers "0.5.1") (deps (list (crate-dep (name "bytecheck_derive") (req "=0.5.0") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "memoffset") (req "^0.6") (default-features #t) (kind 0)) (crate-dep (name "ptr_meta") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "simdutf8") (req "^0.1") (kind 0)))) (hash "191wbdfq5n81fid4rwxq8bmrz1ybzwzshg0c69qdpvawbmmw05sk") (features (quote (("std" "simdutf8/std") ("full_errors") ("default" "const_generics" "std") ("const_generics"))))))

(define-public crate-bytecheck-0.5 (crate (name "bytecheck") (vers "0.5.2") (deps (list (crate-dep (name "bytecheck_derive") (req "=0.5.2") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "ptr_meta") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "simdutf8") (req "^0.1") (kind 0)))) (hash "0q11ap6nqj0rsc8ypwjh918916zlrcrzdgqm175gnpb2yn9axyh1") (features (quote (("std" "simdutf8/std") ("full_errors") ("default" "const_generics" "std") ("const_generics"))))))

(define-public crate-bytecheck-0.6 (crate (name "bytecheck") (vers "0.6.0") (deps (list (crate-dep (name "bytecheck_derive") (req "=0.6.0") (kind 0)) (crate-dep (name "ptr_meta") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "simdutf8") (req "^0.1") (kind 0)) (crate-dep (name "uuid") (req "^0.8") (optional #t) (default-features #t) (kind 0)))) (hash "07zrrjs13l638n177i9hfrxpyi2sigp5aknkx0b7qap9jn5qnanw") (features (quote (("verbose") ("std" "bytecheck_derive/std" "simdutf8/std") ("default" "std"))))))

(define-public crate-bytecheck-0.6 (crate (name "bytecheck") (vers "0.6.1") (deps (list (crate-dep (name "bytecheck_derive") (req "=0.6.1") (kind 0)) (crate-dep (name "ptr_meta") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "simdutf8") (req "^0.1") (kind 0)) (crate-dep (name "uuid") (req "^0.8") (optional #t) (default-features #t) (kind 0)))) (hash "00kdxjs0dgzkxm7f62jpzlywmf2h6s3755jcsw1xis2bx4i65a5l") (features (quote (("verbose") ("std" "bytecheck_derive/std" "simdutf8/std") ("default" "std"))))))

(define-public crate-bytecheck-0.6 (crate (name "bytecheck") (vers "0.6.2") (deps (list (crate-dep (name "bytecheck_derive") (req "=0.6.2") (kind 0)) (crate-dep (name "ptr_meta") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "simdutf8") (req "^0.1") (kind 0)) (crate-dep (name "uuid") (req "^0.8") (optional #t) (default-features #t) (kind 0)))) (hash "0v1yl6knwg5sxpxw26p6rr0kwf4qj7sc6h64dh4s7q6dfjxvcs8n") (features (quote (("verbose") ("std" "bytecheck_derive/std" "simdutf8/std") ("default" "std"))))))

(define-public crate-bytecheck-0.6 (crate (name "bytecheck") (vers "0.6.3") (deps (list (crate-dep (name "bytecheck_derive") (req "=0.6.3") (kind 0)) (crate-dep (name "ptr_meta") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "simdutf8") (req "^0.1") (optional #t) (kind 0)) (crate-dep (name "uuid") (req "^0.8") (optional #t) (default-features #t) (kind 0)))) (hash "198fd8nmid7hpaz5qy7z3d3cq1076cn19caksifqxjisqas42vpz") (features (quote (("verbose") ("std" "bytecheck_derive/std") ("simdutf8_std" "simdutf8/std") ("default" "std"))))))

(define-public crate-bytecheck-0.6 (crate (name "bytecheck") (vers "0.6.4") (deps (list (crate-dep (name "bytecheck_derive") (req "=0.6.4") (kind 0)) (crate-dep (name "ptr_meta") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "simdutf8") (req "^0.1") (optional #t) (kind 0)) (crate-dep (name "uuid") (req "^0.8") (optional #t) (default-features #t) (kind 0)))) (hash "027biqsg1kmx1nidjkqpbcrvm2wfpqmknibkq32h73jy83dvnyvi") (features (quote (("verbose") ("std" "bytecheck_derive/std") ("simdutf8_std" "simdutf8/std") ("default" "std"))))))

(define-public crate-bytecheck-0.6 (crate (name "bytecheck") (vers "0.6.5") (deps (list (crate-dep (name "bytecheck_derive") (req "=0.6.5") (kind 0)) (crate-dep (name "ptr_meta") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "simdutf8") (req "^0.1") (optional #t) (kind 0)) (crate-dep (name "uuid") (req "^0.8") (optional #t) (default-features #t) (kind 0)))) (hash "0ank5w0nxz9j903f2im5ixl0jwmx86b0f5msbg6wv7lqclg8lwzv") (features (quote (("verbose") ("std" "bytecheck_derive/std") ("simdutf8_std" "simdutf8/std") ("default" "std"))))))

(define-public crate-bytecheck-0.6 (crate (name "bytecheck") (vers "0.6.6") (deps (list (crate-dep (name "bytecheck_derive") (req "=0.6.5") (kind 0)) (crate-dep (name "ptr_meta") (req "^0.1") (kind 0)) (crate-dep (name "simdutf8") (req "^0.1") (optional #t) (kind 0)) (crate-dep (name "uuid") (req "^0.8") (optional #t) (default-features #t) (kind 0)))) (hash "1y7hxs592cqc8agq4785ddg41iqhzgx4w4gjvsnaqnpmkni07x3p") (features (quote (("verbose") ("std" "ptr_meta/std" "bytecheck_derive/std") ("simdutf8_std" "simdutf8/std") ("default" "std"))))))

(define-public crate-bytecheck-0.6 (crate (name "bytecheck") (vers "0.6.7") (deps (list (crate-dep (name "bytecheck_derive") (req "=0.6.7") (kind 0)) (crate-dep (name "ptr_meta") (req "^0.1") (kind 0)) (crate-dep (name "simdutf8") (req "^0.1") (optional #t) (kind 0)) (crate-dep (name "uuid") (req "^0.8") (optional #t) (default-features #t) (kind 0)))) (hash "1gjasf0viyrn1p01fb77n3n8ga8mjif6wvixgk5n98nd67m8jj1i") (features (quote (("verbose") ("std" "ptr_meta/std" "bytecheck_derive/std") ("simdutf8_std" "simdutf8/std") ("default" "std"))))))

(define-public crate-bytecheck-0.6 (crate (name "bytecheck") (vers "0.6.8") (deps (list (crate-dep (name "bytecheck_derive") (req "=0.6.8") (kind 0)) (crate-dep (name "ptr_meta") (req "^0.1") (kind 0)) (crate-dep (name "simdutf8") (req "^0.1") (optional #t) (kind 0)) (crate-dep (name "uuid") (req "^1.0") (optional #t) (default-features #t) (kind 0)))) (hash "0kzmzwii5y0qvxk0jsixkdsmlrbf7qadywlb57j175fvq8izjc9s") (features (quote (("verbose") ("std" "ptr_meta/std" "bytecheck_derive/std") ("simdutf8_std" "simdutf8/std") ("default" "std"))))))

(define-public crate-bytecheck-0.6 (crate (name "bytecheck") (vers "0.6.9") (deps (list (crate-dep (name "bytecheck_derive") (req "=0.6.9") (kind 0)) (crate-dep (name "ptr_meta") (req "^0.1") (kind 0)) (crate-dep (name "simdutf8") (req "^0.1") (optional #t) (kind 0)) (crate-dep (name "uuid") (req "^1.0") (optional #t) (default-features #t) (kind 0)))) (hash "0vs0a8p3bpaz3vc15zknqkd5ajgzgswf2bmd1mbwdbdm28naq76i") (features (quote (("verbose") ("std" "ptr_meta/std" "bytecheck_derive/std") ("simdutf8_std" "simdutf8/std") ("default" "std"))))))

(define-public crate-bytecheck-0.7 (crate (name "bytecheck") (vers "0.7.0") (deps (list (crate-dep (name "bytecheck_derive") (req "^0.7.0") (default-features #t) (kind 0)) (crate-dep (name "ptr_meta") (req "^0.1") (kind 0)) (crate-dep (name "simdutf8") (req "^0.1") (optional #t) (kind 0)) (crate-dep (name "uuid") (req "^1.3") (optional #t) (default-features #t) (kind 0)))) (hash "0pfk0vdvjhxyziwpf5180cmywkbq2zh8k0rgpd6fak1hzqq2cl21") (features (quote (("verbose") ("std" "ptr_meta/std" "bytecheck_derive/std" "simdutf8/std") ("default" "simdutf8" "std"))))))

(define-public crate-bytecheck-0.6 (crate (name "bytecheck") (vers "0.6.10") (deps (list (crate-dep (name "bytecheck_derive") (req "^0.6.10") (default-features #t) (kind 0)) (crate-dep (name "ptr_meta") (req "^0.1") (kind 0)) (crate-dep (name "simdutf8") (req "^0.1") (optional #t) (kind 0)) (crate-dep (name "uuid") (req "^1.3") (optional #t) (default-features #t) (kind 0)))) (hash "07vxs7y441f7d6mjzmli80ykmfajwk9jqci549b29sr319j13zhk") (features (quote (("verbose") ("std" "ptr_meta/std" "bytecheck_derive/std" "simdutf8/std") ("default" "simdutf8" "std"))))))

(define-public crate-bytecheck-0.6 (crate (name "bytecheck") (vers "0.6.11") (deps (list (crate-dep (name "bytecheck_derive") (req "^0.6.11") (default-features #t) (kind 0)) (crate-dep (name "ptr_meta") (req "^0.1") (kind 0)) (crate-dep (name "simdutf8") (req "^0.1") (optional #t) (kind 0)) (crate-dep (name "uuid") (req "^1.3") (optional #t) (default-features #t) (kind 0)))) (hash "09xnpjfhw36a973dpdd2mcmb93rrix539j49vkkgcqf878174qwb") (features (quote (("verbose") ("std" "ptr_meta/std" "bytecheck_derive/std" "simdutf8/std") ("default" "simdutf8" "std"))))))

(define-public crate-bytecheck-0.8 (crate (name "bytecheck") (vers "0.8.0-pre1") (deps (list (crate-dep (name "bytecheck_derive") (req "^0.8.0-pre1") (kind 0)) (crate-dep (name "ptr_meta") (req "^0.3.0-pre1") (kind 0)) (crate-dep (name "rancor") (req "^0.1.0-pre1") (kind 0)) (crate-dep (name "simdutf8") (req "^0.1") (optional #t) (kind 0)) (crate-dep (name "uuid") (req "^1.4") (optional #t) (kind 0)))) (hash "1qd78m8gb53vw4jwrkgl0ygl5c61s7b938cgn4kdkn9fslqgqkvy") (features (quote (("default" "simdutf8" "std")))) (v 2) (features2 (quote (("std" "ptr_meta/std" "rancor/std" "simdutf8?/std"))))))

(define-public crate-bytecheck-0.8 (crate (name "bytecheck") (vers "0.8.0-pre2") (deps (list (crate-dep (name "bytecheck_derive") (req "^0.8.0-pre2") (kind 0)) (crate-dep (name "ptr_meta") (req "^0.3.0-pre1") (kind 0)) (crate-dep (name "rancor") (req "^0.1.0-pre5") (kind 0)) (crate-dep (name "simdutf8") (req "^0.1") (optional #t) (kind 0)) (crate-dep (name "uuid") (req "^1.4") (optional #t) (kind 0)))) (hash "0jlkd36kink09fsfcs95p2d6b1bpdszr28y9v6mzb3lgmbcmz614") (features (quote (("default" "simdutf8" "std")))) (v 2) (features2 (quote (("std" "ptr_meta/std" "rancor/std" "simdutf8?/std"))))))

(define-public crate-bytecheck-0.8 (crate (name "bytecheck") (vers "0.8.0-pre3") (deps (list (crate-dep (name "bytecheck_derive") (req "^0.8.0-pre3") (kind 0)) (crate-dep (name "ptr_meta") (req "^0.3.0-pre1") (kind 0)) (crate-dep (name "rancor") (req "^0.1.0-pre6") (kind 0)) (crate-dep (name "simdutf8") (req "^0.1") (optional #t) (kind 0)) (crate-dep (name "uuid") (req "^1.4") (optional #t) (kind 0)))) (hash "0b75yq70c3apj5j58cnx75qv5jxa6d5x94k5hnf9hqp9pkzihiis") (features (quote (("default" "simdutf8" "std")))) (v 2) (features2 (quote (("std" "ptr_meta/std" "rancor/std" "simdutf8?/std"))))))

(define-public crate-bytecheck-0.8 (crate (name "bytecheck") (vers "0.8.0-pre4") (deps (list (crate-dep (name "bytecheck_derive") (req "^0.8.0-pre4") (kind 0)) (crate-dep (name "ptr_meta") (req "^0.3.0-pre1") (kind 0)) (crate-dep (name "rancor") (req "^0.1.0-pre6") (kind 0)) (crate-dep (name "simdutf8") (req "^0.1") (optional #t) (kind 0)) (crate-dep (name "uuid") (req "^1.4") (optional #t) (kind 0)))) (hash "0rxfkbw8hgad43b73xlpx5v6v179krli3xh4s0agnyqrjlk57rp6") (features (quote (("default" "simdutf8" "std")))) (v 2) (features2 (quote (("std" "ptr_meta/std" "rancor/std" "simdutf8?/std"))))))

(define-public crate-bytecheck-0.8 (crate (name "bytecheck") (vers "0.8.0-pre5") (deps (list (crate-dep (name "bytecheck_derive") (req "^0.8.0-pre5") (kind 0)) (crate-dep (name "ptr_meta") (req "^0.3.0-pre1") (kind 0)) (crate-dep (name "rancor") (req "^0.1.0-pre7") (kind 0)) (crate-dep (name "simdutf8") (req "^0.1") (optional #t) (kind 0)) (crate-dep (name "uuid") (req "^1.4") (optional #t) (kind 0)))) (hash "05pzs3rw9yyddsfqpclbp9hqy784v21jnysqx842jl5d3zbv01ap") (features (quote (("default" "simdutf8" "std")))) (v 2) (features2 (quote (("std" "ptr_meta/std" "rancor/std" "simdutf8?/std"))))))

(define-public crate-bytecheck-0.8 (crate (name "bytecheck") (vers "0.8.0-pre6") (deps (list (crate-dep (name "bytecheck_derive") (req "^0.8.0-pre6") (kind 0)) (crate-dep (name "ptr_meta") (req "^0.3.0-pre1") (kind 0)) (crate-dep (name "rancor") (req "^0.1.0-pre8") (kind 0)) (crate-dep (name "simdutf8") (req "^0.1") (optional #t) (kind 0)) (crate-dep (name "uuid") (req "^1.4") (optional #t) (kind 0)))) (hash "0mbpgs00n76jfidpawwvyzg3hw8hf939y2jd1asw21a14k27q5jq") (features (quote (("default" "simdutf8" "std")))) (v 2) (features2 (quote (("std" "ptr_meta/std" "rancor/std" "simdutf8?/std"))))))

(define-public crate-bytecheck-0.6 (crate (name "bytecheck") (vers "0.6.12") (deps (list (crate-dep (name "bytecheck_derive") (req "^0.6.12") (kind 0)) (crate-dep (name "ptr_meta") (req "^0.1") (kind 0)) (crate-dep (name "simdutf8") (req "^0.1") (optional #t) (kind 0)) (crate-dep (name "uuid") (req "^1.3") (optional #t) (default-features #t) (kind 0)))) (hash "1hmipv4yyxgbamcbw5r65wagv9khs033v9483s9kri9sw9ycbk93") (features (quote (("verbose") ("std" "ptr_meta/std" "bytecheck_derive/std" "simdutf8/std") ("default" "simdutf8" "std"))))))

(define-public crate-bytecheck-0.8 (crate (name "bytecheck") (vers "0.8.0-alpha.9") (deps (list (crate-dep (name "bytecheck_derive") (req "^0.8.0-alpha.9") (kind 0)) (crate-dep (name "ptr_meta") (req "^0.3.0-alpha.2") (kind 0)) (crate-dep (name "rancor") (req "^0.1.0-alpha.9") (kind 0)) (crate-dep (name "simdutf8") (req "^0.1") (optional #t) (kind 0)) (crate-dep (name "uuid") (req "^1.4") (optional #t) (kind 0)))) (hash "1wik42cac8piag16c26pin9s0jk2bp84vj33z2gpyqqk6g5h7bcb") (features (quote (("default" "simdutf8" "std")))) (v 2) (features2 (quote (("std" "ptr_meta/std" "rancor/std" "simdutf8?/std"))))))

(define-public crate-bytecheck_derive-0.1 (crate (name "bytecheck_derive") (vers "0.1.0") (deps (list (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (default-features #t) (kind 0)))) (hash "1dfv8vr3mspfd1jbsinpigx6f11f481zpj185a7awbmd3gd6z5zd")))

(define-public crate-bytecheck_derive-0.2 (crate (name "bytecheck_derive") (vers "0.2.0") (deps (list (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (default-features #t) (kind 0)))) (hash "15m0v1qjhchlzim96773xh90a4nbzbqa5ys06wmnh1bhbfayhk5d")))

(define-public crate-bytecheck_derive-0.2 (crate (name "bytecheck_derive") (vers "0.2.1") (deps (list (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (default-features #t) (kind 0)))) (hash "0hazyzl5mkzixagjaq5lyx2nfqh3gmpksjnk9vrfhd0p3hjvlsda")))

(define-public crate-bytecheck_derive-0.3 (crate (name "bytecheck_derive") (vers "0.3.0") (deps (list (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (default-features #t) (kind 0)))) (hash "1wai31k8qa116z7f9bh084cg9s6hy58hpfv4gs5zran8pbwfks86")))

(define-public crate-bytecheck_derive-0.4 (crate (name "bytecheck_derive") (vers "0.4.0") (deps (list (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (default-features #t) (kind 0)))) (hash "1dsbici7v0vsmzy665812id4rrx4v7fp0n6qymbk3jd10mxakyjx")))

(define-public crate-bytecheck_derive-0.4 (crate (name "bytecheck_derive") (vers "0.4.1") (deps (list (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (default-features #t) (kind 0)))) (hash "0jl4935rrv2yr12j5l4wk5r5qc7hlqza7qpdc6fr0wc039iwp71a")))

(define-public crate-bytecheck_derive-0.5 (crate (name "bytecheck_derive") (vers "0.5.0") (deps (list (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (default-features #t) (kind 0)))) (hash "0snagypszv074i7kpg44f43sfy5fvkymqmc40yxibbgbyhhyc5hs")))

(define-public crate-bytecheck_derive-0.5 (crate (name "bytecheck_derive") (vers "0.5.2") (deps (list (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (default-features #t) (kind 0)))) (hash "0grbkwwv5j91n7zrimci6fh4k79flxga3mkjg50jysnyraizi088")))

(define-public crate-bytecheck_derive-0.6 (crate (name "bytecheck_derive") (vers "0.6.0") (deps (list (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (default-features #t) (kind 0)))) (hash "1z5jc7z1b2fmf84lwzmbjwfigxds55p03p3hmzqmhwa6240ig8qp") (features (quote (("std") ("default" "std"))))))

(define-public crate-bytecheck_derive-0.6 (crate (name "bytecheck_derive") (vers "0.6.1") (deps (list (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (default-features #t) (kind 0)))) (hash "1vxcx5cml4mgfkim0xrbgvp1clkhrn1z89qiph1yk6d3qyhjmjlf") (features (quote (("std") ("default" "std"))))))

(define-public crate-bytecheck_derive-0.6 (crate (name "bytecheck_derive") (vers "0.6.2") (deps (list (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (default-features #t) (kind 0)))) (hash "0w125mih2lz1gdkd3prvf298a0vqgjn539rr6z72wlhpbz5wb50k") (features (quote (("std") ("default" "std"))))))

(define-public crate-bytecheck_derive-0.6 (crate (name "bytecheck_derive") (vers "0.6.3") (deps (list (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (default-features #t) (kind 0)))) (hash "1lv33k7bw70dx9pxsamscjl621nacx08li1fl8w8c4yzlha5hy7v") (features (quote (("std") ("default" "std"))))))

(define-public crate-bytecheck_derive-0.6 (crate (name "bytecheck_derive") (vers "0.6.4") (deps (list (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (default-features #t) (kind 0)))) (hash "1zl7v5s23xx5bsv02n13lnjinr3k76q4kvznlc6fffv6wdfnvjg7") (features (quote (("std") ("default" "std"))))))

(define-public crate-bytecheck_derive-0.6 (crate (name "bytecheck_derive") (vers "0.6.5") (deps (list (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (default-features #t) (kind 0)))) (hash "02p1443m4i3wp20rkw51b8nsijzj2vn9yp27mdyq97ywdzrdzd63") (features (quote (("std") ("default" "std"))))))

(define-public crate-bytecheck_derive-0.6 (crate (name "bytecheck_derive") (vers "0.6.7") (deps (list (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (default-features #t) (kind 0)))) (hash "0006ycn72g788hzfy5zd7gl3hxhqi5dj0q7plrjydnimq693nasa") (features (quote (("std") ("default" "std"))))))

(define-public crate-bytecheck_derive-0.6 (crate (name "bytecheck_derive") (vers "0.6.8") (deps (list (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (default-features #t) (kind 0)))) (hash "1w3ln5n9gw55l18dm2lbvibgyvrj0bh7m8nsfhhr2pch5a37rcgd") (features (quote (("std") ("default" "std"))))))

(define-public crate-bytecheck_derive-0.6 (crate (name "bytecheck_derive") (vers "0.6.9") (deps (list (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (default-features #t) (kind 0)))) (hash "1gxr63mi91rrjzfzcb8pfwsnarp9i2w1n168nc05aq4fx7mpdr8k") (features (quote (("std") ("default" "std"))))))

(define-public crate-bytecheck_derive-0.7 (crate (name "bytecheck_derive") (vers "0.7.0") (deps (list (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (default-features #t) (kind 0)))) (hash "1nhymi1ln6fpwi2vfm3pgzhabfnr3f3xzf8s43f0gg45f1c8ra7d") (features (quote (("std") ("default" "std"))))))

(define-public crate-bytecheck_derive-0.6 (crate (name "bytecheck_derive") (vers "0.6.10") (deps (list (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (default-features #t) (kind 0)))) (hash "1r9dlkx5w1p8d5gif2yvn6bz1856yij2fxi4wakq2vxl7ia2a4p3") (features (quote (("std") ("default" "std"))))))

(define-public crate-bytecheck_derive-0.6 (crate (name "bytecheck_derive") (vers "0.6.11") (deps (list (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (default-features #t) (kind 0)))) (hash "0qdgfqx23gbjp5scbc8fhqc5kd014bpxn8hc9i9ssd8r4rplrv57") (features (quote (("std") ("default" "std"))))))

(define-public crate-bytecheck_derive-0.8 (crate (name "bytecheck_derive") (vers "0.8.0-pre1") (deps (list (crate-dep (name "proc-macro2") (req "^1.0") (kind 0)) (crate-dep (name "quote") (req "^1.0") (kind 0)) (crate-dep (name "syn") (req "^2.0") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "1vyw2ghbzsfnbbf6ahy43zm5qq1c3swpk7glps5xrm17nx1dwp22")))

(define-public crate-bytecheck_derive-0.8 (crate (name "bytecheck_derive") (vers "0.8.0-pre2") (deps (list (crate-dep (name "proc-macro2") (req "^1.0") (kind 0)) (crate-dep (name "quote") (req "^1.0") (kind 0)) (crate-dep (name "syn") (req "^2.0") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "118aa1g0li1rmvcw5bag1lx6a05wlsp6gsdvnk1hzazbhwmc5rrd")))

(define-public crate-bytecheck_derive-0.8 (crate (name "bytecheck_derive") (vers "0.8.0-pre3") (deps (list (crate-dep (name "proc-macro2") (req "^1.0") (kind 0)) (crate-dep (name "quote") (req "^1.0") (kind 0)) (crate-dep (name "syn") (req "^2.0") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "1v0aj2k08n8wycjr89xmii0qwf3vm6if5ylhb6g38kgbfdqrdsyd")))

(define-public crate-bytecheck_derive-0.8 (crate (name "bytecheck_derive") (vers "0.8.0-pre4") (deps (list (crate-dep (name "proc-macro2") (req "^1.0") (kind 0)) (crate-dep (name "quote") (req "^1.0") (kind 0)) (crate-dep (name "syn") (req "^2.0") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "0w95ad14a1v08jbrbjsbdh9mvaarsbchhn0z64raxqk28kcmclk9")))

(define-public crate-bytecheck_derive-0.8 (crate (name "bytecheck_derive") (vers "0.8.0-pre5") (deps (list (crate-dep (name "proc-macro2") (req "^1.0") (kind 0)) (crate-dep (name "quote") (req "^1.0") (kind 0)) (crate-dep (name "syn") (req "^2.0") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "040rjxnxg68f7h1v4jkf14cmscaarwyi1ism3z7p72pn9vb254dm")))

(define-public crate-bytecheck_derive-0.8 (crate (name "bytecheck_derive") (vers "0.8.0-pre6") (deps (list (crate-dep (name "proc-macro2") (req "^1.0") (kind 0)) (crate-dep (name "quote") (req "^1.0") (kind 0)) (crate-dep (name "syn") (req "^2.0") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "1lbwq12anp33111a4hlv30sfq4fpb1cr6k24ismcgqj7ypljnha1")))

(define-public crate-bytecheck_derive-0.6 (crate (name "bytecheck_derive") (vers "0.6.12") (deps (list (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (default-features #t) (kind 0)))) (hash "0ng6230brd0hvqpbgcx83inn74mdv3abwn95x515bndwkz90dd1x") (features (quote (("std") ("default" "std"))))))

(define-public crate-bytecheck_derive-0.8 (crate (name "bytecheck_derive") (vers "0.8.0-alpha.7") (deps (list (crate-dep (name "proc-macro2") (req "^1.0") (kind 0)) (crate-dep (name "quote") (req "^1.0") (kind 0)) (crate-dep (name "syn") (req "^2.0") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "01mn2p89102w8d5bamwdxnkxlasg8kk0dni4rb7xmc21q47a4q90")))

(define-public crate-bytecheck_derive-0.8 (crate (name "bytecheck_derive") (vers "0.8.0-alpha.9") (deps (list (crate-dep (name "proc-macro2") (req "^1.0") (kind 0)) (crate-dep (name "quote") (req "^1.0") (kind 0)) (crate-dep (name "syn") (req "^2.0") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "0008a62qgpab3c0ci904qpiv60vijh2z4yjd5j4fhim83na2w3kl")))

(define-public crate-bytecheck_test-0.6 (crate (name "bytecheck_test") (vers "0.6.5") (deps (list (crate-dep (name "bytecheck") (req "^0.6") (kind 0)))) (hash "0sq0s7d2akmk4lsjm9l38raclkx5xwnh8vxbqk16xw5l6jhd2i00") (features (quote (("verbose" "bytecheck/verbose") ("std" "bytecheck/std") ("default" "std"))))))

(define-public crate-bytecmp-0.4 (crate (name "bytecmp") (vers "0.4.2") (deps (list (crate-dep (name "bytepack") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 2)))) (hash "0lm17cw30h7kqvss0wkfc2l7469rcrb3nmpg5nxf67w1gflpbm6x")))

(define-public crate-bytecmp-0.5 (crate (name "bytecmp") (vers "0.5.0") (deps (list (crate-dep (name "bytepack") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 2)))) (hash "1razs4ri7rj8b56i387b16apv44ij56i7d83sx4bc2i9xdxxxb70")))

(define-public crate-bytecmp-0.5 (crate (name "bytecmp") (vers "0.5.1") (deps (list (crate-dep (name "bytepack") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 2)))) (hash "02qkrv0i66px27dpda035rp4d6lqw0ngd8n4ncfqyckls9k8zpcv")))

(define-public crate-bytecode-0.1 (crate (name "bytecode") (vers "0.1.0") (deps (list (crate-dep (name "tiny-ansi") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "1kycywvv4d6nidgc1bc5ifmh6ybqgml0zx04q32k8sazspgqlggm")))

(define-public crate-bytecode-0.1 (crate (name "bytecode") (vers "0.1.1") (deps (list (crate-dep (name "tiny-ansi") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "17m4v1kqcg5zdm9gxiphnf47qihcliqgnmz5maa0r9fwcyg1whyv")))

(define-public crate-bytecode-0.2 (crate (name "bytecode") (vers "0.2.0") (deps (list (crate-dep (name "tiny-ansi") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "1p6ccyvadxi3ch89zs7nzck1vzpc69zg6yx9w7ipzr718z14j924")))

(define-public crate-bytecode-0.3 (crate (name "bytecode") (vers "0.3.0") (deps (list (crate-dep (name "tiny-ansi") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "0cx3qyilfx56wlfhc85br6mrlpwxvvbf4rgxw985sc2xg7xwns02")))

(define-public crate-bytecode-interpreter-crypto-0.1 (crate (name "bytecode-interpreter-crypto") (vers "0.1.2") (deps (list (crate-dep (name "anyhow") (req "^1.0.52") (default-features #t) (kind 0)) (crate-dep (name "curve25519-dalek") (req "^0.1.0") (features (quote ("std"))) (kind 0) (package "curve25519-dalek-fiat")) (crate-dep (name "ed25519-dalek") (req "^0.1.0") (features (quote ("std" "serde"))) (kind 0) (package "ed25519-dalek-fiat")) (crate-dep (name "sha2") (req "^0.9.3") (default-features #t) (kind 0)) (crate-dep (name "sha3") (req "^0.9.1") (default-features #t) (kind 0)))) (hash "00hpcchigi96np84nmv5myxghkrk7fipk6bwx8dc7xnlni6hm2iy") (features (quote (("u64" "curve25519-dalek/u64_backend" "ed25519-dalek/u64_backend") ("u32" "curve25519-dalek/u32_backend" "ed25519-dalek/u32_backend") ("fiat" "curve25519-dalek/fiat_u64_backend" "ed25519-dalek/fiat_u64_backend") ("default" "fiat")))) (yanked #t)))

(define-public crate-bytecode-interpreter-crypto-0.1 (crate (name "bytecode-interpreter-crypto") (vers "0.1.4") (deps (list (crate-dep (name "anyhow") (req "^1.0.52") (default-features #t) (kind 0)) (crate-dep (name "curve25519-dalek") (req "^0.1.0") (features (quote ("std"))) (kind 0) (package "curve25519-dalek-fiat")) (crate-dep (name "ed25519-dalek") (req "^0.1.0") (features (quote ("std" "serde"))) (kind 0) (package "ed25519-dalek-fiat")) (crate-dep (name "sha2") (req "^0.9.3") (default-features #t) (kind 0)) (crate-dep (name "sha3") (req "^0.9.1") (default-features #t) (kind 0)))) (hash "1jkrhxn7hl0ijkfvs7i5a20n9znwwaj3r1la17qz3wvzaksi9pxx") (features (quote (("u64" "curve25519-dalek/u64_backend" "ed25519-dalek/u64_backend") ("u32" "curve25519-dalek/u32_backend" "ed25519-dalek/u32_backend") ("fiat" "curve25519-dalek/fiat_u64_backend" "ed25519-dalek/fiat_u64_backend") ("default" "fiat")))) (yanked #t)))

(define-public crate-bytecode-interpreter-crypto-0.1 (crate (name "bytecode-interpreter-crypto") (vers "0.1.5") (deps (list (crate-dep (name "anyhow") (req "^1.0.52") (default-features #t) (kind 0)) (crate-dep (name "curve25519-dalek") (req "^0.1.0") (features (quote ("std"))) (kind 0) (package "curve25519-dalek-fiat")) (crate-dep (name "ed25519-dalek") (req "^0.1.0") (features (quote ("std" "serde"))) (kind 0) (package "ed25519-dalek-fiat")) (crate-dep (name "sha2") (req "^0.9.3") (default-features #t) (kind 0)) (crate-dep (name "sha3") (req "^0.9.1") (default-features #t) (kind 0)))) (hash "1r1mbmagqqcc4kl3dx0ixi4nxhyqrz4r0c3va28aszi4shd93001") (features (quote (("u64" "curve25519-dalek/u64_backend" "ed25519-dalek/u64_backend") ("u32" "curve25519-dalek/u32_backend" "ed25519-dalek/u32_backend") ("fiat" "curve25519-dalek/fiat_u64_backend" "ed25519-dalek/fiat_u64_backend") ("default" "fiat")))) (yanked #t)))

(define-public crate-bytecode-interpreter-crypto-0.1 (crate (name "bytecode-interpreter-crypto") (vers "0.1.6") (deps (list (crate-dep (name "anyhow") (req "^1.0.52") (default-features #t) (kind 0)) (crate-dep (name "curve25519-dalek") (req "^0.1.0") (features (quote ("std"))) (kind 0) (package "curve25519-dalek-fiat")) (crate-dep (name "ed25519-dalek") (req "^0.1.0") (features (quote ("std" "serde"))) (kind 0) (package "ed25519-dalek-fiat")) (crate-dep (name "sha2") (req "^0.9.3") (default-features #t) (kind 0)) (crate-dep (name "sha3") (req "^0.9.1") (default-features #t) (kind 0)))) (hash "0klb21w61hfkch17a78bld2g4xspywshdlk60mbbsqx8v2h5qa7f") (features (quote (("u64" "curve25519-dalek/u64_backend" "ed25519-dalek/u64_backend") ("u32" "curve25519-dalek/u32_backend" "ed25519-dalek/u32_backend") ("fiat" "curve25519-dalek/fiat_u64_backend" "ed25519-dalek/fiat_u64_backend") ("default" "fiat")))) (yanked #t)))

(define-public crate-bytecode-interpreter-crypto-0.2 (crate (name "bytecode-interpreter-crypto") (vers "0.2.0") (deps (list (crate-dep (name "anyhow") (req "^1.0.52") (default-features #t) (kind 0)) (crate-dep (name "curve25519-dalek") (req "^0.1.0") (features (quote ("std"))) (kind 0) (package "curve25519-dalek-fiat")) (crate-dep (name "ed25519-dalek") (req "^0.1.0") (features (quote ("std" "serde"))) (kind 0) (package "ed25519-dalek-fiat")) (crate-dep (name "sha2") (req "^0.9.3") (default-features #t) (kind 0)) (crate-dep (name "sha3") (req "^0.9.1") (default-features #t) (kind 0)))) (hash "0wchy4qy1r59g56j769bvy604ql6pgs738q9jpz306x4x1ij5rl1") (features (quote (("u64" "curve25519-dalek/u64_backend" "ed25519-dalek/u64_backend") ("u32" "curve25519-dalek/u32_backend" "ed25519-dalek/u32_backend") ("fiat" "curve25519-dalek/fiat_u64_backend" "ed25519-dalek/fiat_u64_backend") ("default" "fiat")))) (yanked #t)))

(define-public crate-bytecode-interpreter-crypto-0.2 (crate (name "bytecode-interpreter-crypto") (vers "0.2.1") (deps (list (crate-dep (name "anyhow") (req "^1.0.52") (default-features #t) (kind 0)) (crate-dep (name "curve25519-dalek") (req "^0.1.0") (features (quote ("std"))) (kind 0) (package "curve25519-dalek-fiat")) (crate-dep (name "ed25519-dalek") (req "^0.1.0") (features (quote ("std" "serde"))) (kind 0) (package "ed25519-dalek-fiat")) (crate-dep (name "sha2") (req "^0.9.3") (default-features #t) (kind 0)) (crate-dep (name "sha3") (req "^0.9.1") (default-features #t) (kind 0)))) (hash "16zwrjqk1xdqa272kyqdmrbaym4znc8dys3zy7jpvnvbrd173ihr") (features (quote (("u64" "curve25519-dalek/u64_backend" "ed25519-dalek/u64_backend") ("u32" "curve25519-dalek/u32_backend" "ed25519-dalek/u32_backend") ("fiat" "curve25519-dalek/fiat_u64_backend" "ed25519-dalek/fiat_u64_backend") ("default" "fiat")))) (yanked #t)))

(define-public crate-bytecode-interpreter-crypto-0.3 (crate (name "bytecode-interpreter-crypto") (vers "0.3.0") (deps (list (crate-dep (name "anyhow") (req "^1.0.52") (default-features #t) (kind 0)) (crate-dep (name "curve25519-dalek") (req "^0.1.0") (features (quote ("std"))) (kind 0) (package "curve25519-dalek-fiat")) (crate-dep (name "ed25519-dalek") (req "^0.1.0") (features (quote ("std" "serde"))) (kind 0) (package "ed25519-dalek-fiat")) (crate-dep (name "sha2") (req "^0.9.3") (default-features #t) (kind 0)) (crate-dep (name "sha3") (req "^0.9.1") (default-features #t) (kind 0)))) (hash "1bcydsq3xiqzncihcwp6krfg0vh3isg0rywsxyf8m8x13xv93l7p") (features (quote (("u64" "curve25519-dalek/u64_backend" "ed25519-dalek/u64_backend") ("u32" "curve25519-dalek/u32_backend" "ed25519-dalek/u32_backend") ("fiat" "curve25519-dalek/fiat_u64_backend" "ed25519-dalek/fiat_u64_backend") ("default" "fiat")))) (yanked #t)))

(define-public crate-bytecode-interpreter-crypto-0.3 (crate (name "bytecode-interpreter-crypto") (vers "0.3.1") (deps (list (crate-dep (name "anyhow") (req "^1.0.52") (default-features #t) (kind 0)) (crate-dep (name "curve25519-dalek") (req "^0.1.0") (features (quote ("std"))) (kind 0) (package "curve25519-dalek-fiat")) (crate-dep (name "ed25519-dalek") (req "^0.1.0") (features (quote ("std" "serde"))) (kind 0) (package "ed25519-dalek-fiat")) (crate-dep (name "sha2") (req "^0.9.3") (default-features #t) (kind 0)) (crate-dep (name "sha3") (req "^0.9.1") (default-features #t) (kind 0)))) (hash "12y6ky6szvv1pahs0nrx9hdz0410k72349cba7pjkj05xbv9z7wm") (features (quote (("u64" "curve25519-dalek/u64_backend" "ed25519-dalek/u64_backend") ("u32" "curve25519-dalek/u32_backend" "ed25519-dalek/u32_backend") ("fiat" "curve25519-dalek/fiat_u64_backend" "ed25519-dalek/fiat_u64_backend") ("default" "fiat")))) (yanked #t)))

(define-public crate-bytecode-interpreter-crypto-0.3 (crate (name "bytecode-interpreter-crypto") (vers "0.3.2") (deps (list (crate-dep (name "anyhow") (req "^1.0.52") (default-features #t) (kind 0)) (crate-dep (name "curve25519-dalek") (req "^0.1.0") (features (quote ("std"))) (kind 0) (package "curve25519-dalek-fiat")) (crate-dep (name "ed25519-dalek") (req "^0.1.0") (features (quote ("std" "serde"))) (kind 0) (package "ed25519-dalek-fiat")) (crate-dep (name "sha2") (req "^0.9.3") (default-features #t) (kind 0)) (crate-dep (name "sha3") (req "^0.9.1") (default-features #t) (kind 0)))) (hash "04dbg0m5vdvsy0ja8nv9gz8dam765vyylclnr66gwf9y959bj5g4") (features (quote (("u64" "curve25519-dalek/u64_backend" "ed25519-dalek/u64_backend") ("u32" "curve25519-dalek/u32_backend" "ed25519-dalek/u32_backend") ("fiat" "curve25519-dalek/fiat_u64_backend" "ed25519-dalek/fiat_u64_backend") ("default" "fiat")))) (yanked #t)))

(define-public crate-bytecode-interpreter-testsuite-0.1 (crate (name "bytecode-interpreter-testsuite") (vers "0.1.4") (deps (list (crate-dep (name "anyhow") (req "^1.0.52") (default-features #t) (kind 2)) (crate-dep (name "datatest-stable") (req "^0.1.1") (default-features #t) (kind 2)) (crate-dep (name "move-command-line-common") (req "^0.1.0") (default-features #t) (kind 2)) (crate-dep (name "move-prover-test-utils") (req "^0.1.0") (default-features #t) (kind 2)) (crate-dep (name "move-stackless-bytecode-interpreter") (req "^0.1.0") (default-features #t) (kind 2)) (crate-dep (name "mv-stdlib") (req "^0.1.0") (default-features #t) (kind 2)) (crate-dep (name "mv-unit-test") (req "^0.1.0") (default-features #t) (kind 2)))) (hash "05rp552jsrcv68ddm44rhnc315plfig75h4ffi8fac5x7y8dhljk") (yanked #t)))

(define-public crate-bytecode-interpreter-testsuite-0.1 (crate (name "bytecode-interpreter-testsuite") (vers "0.1.5") (deps (list (crate-dep (name "anyhow") (req "^1.0.52") (default-features #t) (kind 2)) (crate-dep (name "datatest-stable") (req "^0.1.1") (default-features #t) (kind 2)) (crate-dep (name "move-command-line-common") (req "^0.1.0") (default-features #t) (kind 2)) (crate-dep (name "move-prover-test-utils") (req "^0.1.0") (default-features #t) (kind 2)) (crate-dep (name "move-stackless-bytecode-interpreter") (req "^0.1.0") (default-features #t) (kind 2)) (crate-dep (name "mv-stdlib") (req "^0.1.0") (default-features #t) (kind 2)) (crate-dep (name "mv-unit-test") (req "^0.1.0") (default-features #t) (kind 2)))) (hash "0k2ais8768y09b8s026ckb8qjkr4pciwvjrc95lvklly4632f9jf") (yanked #t)))

(define-public crate-bytecode-interpreter-testsuite-0.1 (crate (name "bytecode-interpreter-testsuite") (vers "0.1.6") (deps (list (crate-dep (name "anyhow") (req "^1.0.52") (default-features #t) (kind 2)) (crate-dep (name "datatest-stable") (req "^0.1.1") (default-features #t) (kind 2)) (crate-dep (name "move-command-line-common") (req "^0.1.0") (default-features #t) (kind 2)) (crate-dep (name "move-prover-test-utils") (req "^0.1.0") (default-features #t) (kind 2)) (crate-dep (name "move-stackless-bytecode-interpreter") (req "^0.1.0") (default-features #t) (kind 2)) (crate-dep (name "mv-stdlib") (req "^0.1.0") (default-features #t) (kind 2)) (crate-dep (name "mv-unit-test") (req "^0.1.0") (default-features #t) (kind 2)))) (hash "12y9ds8ffbl82gqzl28bbw5d7fy4wf6ipyfs7hn95bqzbkdbz6jd") (yanked #t)))

(define-public crate-bytecode-interpreter-testsuite-0.2 (crate (name "bytecode-interpreter-testsuite") (vers "0.2.0") (deps (list (crate-dep (name "anyhow") (req "^1.0.52") (default-features #t) (kind 2)) (crate-dep (name "datatest-stable") (req "^0.1.1") (default-features #t) (kind 2)) (crate-dep (name "move-command-line-common") (req "^0.2.0") (default-features #t) (kind 2)) (crate-dep (name "move-prover-test-utils") (req "^0.2.0") (default-features #t) (kind 2)) (crate-dep (name "move-stackless-bytecode-interpreter") (req "^0.2.0") (default-features #t) (kind 2)) (crate-dep (name "mv-stdlib") (req "^0.2.0") (default-features #t) (kind 2)) (crate-dep (name "mv-unit-test") (req "^0.2.0") (default-features #t) (kind 2)))) (hash "1milphvm7c4wcrdp2n07hi70z5vxrimdpzfz95wbysifxpxv4w2p") (yanked #t)))

(define-public crate-bytecode-interpreter-testsuite-0.2 (crate (name "bytecode-interpreter-testsuite") (vers "0.2.1") (deps (list (crate-dep (name "anyhow") (req "^1.0.52") (default-features #t) (kind 2)) (crate-dep (name "datatest-stable") (req "^0.1.1") (default-features #t) (kind 2)) (crate-dep (name "move-command-line-common") (req "^0.2.0") (default-features #t) (kind 2)) (crate-dep (name "move-prover-test-utils") (req "^0.2.0") (default-features #t) (kind 2)) (crate-dep (name "move-stackless-bytecode-interpreter") (req "^0.2.0") (default-features #t) (kind 2)) (crate-dep (name "mv-stdlib") (req "^0.2.0") (default-features #t) (kind 2)) (crate-dep (name "mv-unit-test") (req "^0.2.0") (default-features #t) (kind 2)))) (hash "0xmbbk0jkvap14qf83zibbw5qqmq08hvx5myrlmgsjqca33jhq66") (yanked #t)))

(define-public crate-bytecode-interpreter-testsuite-0.3 (crate (name "bytecode-interpreter-testsuite") (vers "0.3.0") (deps (list (crate-dep (name "anyhow") (req "^1.0.52") (default-features #t) (kind 2)) (crate-dep (name "datatest-stable") (req "^0.1.1") (default-features #t) (kind 2)) (crate-dep (name "move-command-line-common") (req "^0.3.0") (default-features #t) (kind 2)) (crate-dep (name "move-prover-test-utils") (req "^0.3.0") (default-features #t) (kind 2)) (crate-dep (name "move-stackless-bytecode-interpreter") (req "^0.3.0") (default-features #t) (kind 2)) (crate-dep (name "mv-stdlib") (req "^0.3.0") (default-features #t) (kind 2)) (crate-dep (name "mv-unit-test") (req "^0.3.0") (default-features #t) (kind 2)))) (hash "0qx18y3yyzfcf1g7mzipi4asmh5qhighw318r6b13dxiyfm2brk8") (yanked #t)))

(define-public crate-bytecode-interpreter-testsuite-0.3 (crate (name "bytecode-interpreter-testsuite") (vers "0.3.1") (deps (list (crate-dep (name "anyhow") (req "^1.0.52") (default-features #t) (kind 2)) (crate-dep (name "datatest-stable") (req "^0.1.1") (default-features #t) (kind 2)) (crate-dep (name "move-command-line-common") (req "^0.3.0") (default-features #t) (kind 2)) (crate-dep (name "move-prover-test-utils") (req "^0.3.0") (default-features #t) (kind 2)) (crate-dep (name "move-stackless-bytecode-interpreter") (req "^0.3.0") (default-features #t) (kind 2)) (crate-dep (name "mv-stdlib") (req "^0.3.0") (default-features #t) (kind 2)) (crate-dep (name "mv-unit-test") (req "^0.3.0") (default-features #t) (kind 2)))) (hash "04kybd3i2mb8j24rifplzaca03p83jn2yx6j9rq4n86y61qlr3cs") (yanked #t)))

(define-public crate-bytecode-interpreter-testsuite-0.3 (crate (name "bytecode-interpreter-testsuite") (vers "0.3.2") (deps (list (crate-dep (name "anyhow") (req "^1.0.52") (default-features #t) (kind 2)) (crate-dep (name "datatest-stable") (req "^0.1.1") (default-features #t) (kind 2)) (crate-dep (name "move-command-line-common") (req "^0.3.0") (default-features #t) (kind 2)) (crate-dep (name "move-prover-test-utils") (req "^0.3.0") (default-features #t) (kind 2)) (crate-dep (name "move-stackless-bytecode-interpreter") (req "^0.3.0") (default-features #t) (kind 2)) (crate-dep (name "mv-stdlib") (req "^0.3.0") (default-features #t) (kind 2)) (crate-dep (name "mv-unit-test") (req "^0.3.0") (default-features #t) (kind 2)))) (hash "0v4fs2qsyy3xz7ng0v6ah348jfh5xpscfah8q3pc5n06c9v5whwq") (yanked #t)))

(define-public crate-bytecode-verifier-tests-0.1 (crate (name "bytecode-verifier-tests") (vers "0.1.4") (deps (list (crate-dep (name "invalid-mutations") (req "^0.1.0") (default-features #t) (kind 2)) (crate-dep (name "move-bytecode-verifier") (req "^0.1.0") (default-features #t) (kind 2)) (crate-dep (name "mv-binary-format") (req "^0.1.0") (features (quote ("fuzzing"))) (default-features #t) (kind 2)) (crate-dep (name "mv-core-types") (req "^0.1.0") (default-features #t) (kind 2)) (crate-dep (name "petgraph") (req "^0.5.1") (default-features #t) (kind 2)) (crate-dep (name "proptest") (req "^1.0.0") (default-features #t) (kind 2)))) (hash "1y6wmiir7d6gb3jpm67z9ll0r97wi5k9y6kp1sn5rp5j8ivnvgq0") (features (quote (("fuzzing" "mv-binary-format/fuzzing")))) (yanked #t)))

(define-public crate-bytecode-verifier-tests-0.1 (crate (name "bytecode-verifier-tests") (vers "0.1.6") (deps (list (crate-dep (name "invalid-mutations") (req "^0.1.0") (default-features #t) (kind 2)) (crate-dep (name "move-bytecode-verifier") (req "^0.1.0") (default-features #t) (kind 2)) (crate-dep (name "mv-binary-format") (req "^0.1.0") (features (quote ("fuzzing"))) (default-features #t) (kind 2)) (crate-dep (name "mv-core-types") (req "^0.1.0") (default-features #t) (kind 2)) (crate-dep (name "petgraph") (req "^0.5.1") (default-features #t) (kind 2)) (crate-dep (name "proptest") (req "^1.0.0") (default-features #t) (kind 2)))) (hash "1cgrcwx3wf7brvs672xdxb4hcffmvim5xpa87r76xcl3plvsg35d") (features (quote (("fuzzing" "mv-binary-format/fuzzing")))) (yanked #t)))

(define-public crate-bytecode-verifier-tests-0.2 (crate (name "bytecode-verifier-tests") (vers "0.2.0") (deps (list (crate-dep (name "invalid-mutations") (req "^0.2.0") (default-features #t) (kind 2)) (crate-dep (name "move-bytecode-verifier") (req "^0.2.0") (default-features #t) (kind 2)) (crate-dep (name "mv-binary-format") (req "^0.2.0") (features (quote ("fuzzing"))) (default-features #t) (kind 2)) (crate-dep (name "mv-core-types") (req "^0.2.0") (default-features #t) (kind 2)) (crate-dep (name "petgraph") (req "^0.5.1") (default-features #t) (kind 2)) (crate-dep (name "proptest") (req "^1.0.0") (default-features #t) (kind 2)))) (hash "0ppkh3gm0m57m6dn2qijw2ci114wyl3p0d2g8nxa4bd35k8j68jf") (features (quote (("fuzzing" "mv-binary-format/fuzzing")))) (yanked #t)))

(define-public crate-bytecode-verifier-tests-0.2 (crate (name "bytecode-verifier-tests") (vers "0.2.1") (deps (list (crate-dep (name "invalid-mutations") (req "^0.2.0") (default-features #t) (kind 2)) (crate-dep (name "move-bytecode-verifier") (req "^0.2.0") (default-features #t) (kind 2)) (crate-dep (name "mv-binary-format") (req "^0.2.0") (features (quote ("fuzzing"))) (default-features #t) (kind 2)) (crate-dep (name "mv-core-types") (req "^0.2.0") (default-features #t) (kind 2)) (crate-dep (name "petgraph") (req "^0.5.1") (default-features #t) (kind 2)) (crate-dep (name "proptest") (req "^1.0.0") (default-features #t) (kind 2)))) (hash "00pdmljp4sl8lgcdndp8brj1m82frg1cih9kmc698pmlgpwk67mh") (features (quote (("fuzzing" "mv-binary-format/fuzzing")))) (yanked #t)))

(define-public crate-bytecode-verifier-tests-0.3 (crate (name "bytecode-verifier-tests") (vers "0.3.0") (deps (list (crate-dep (name "invalid-mutations") (req "^0.3.0") (default-features #t) (kind 2)) (crate-dep (name "move-bytecode-verifier") (req "^0.3.0") (default-features #t) (kind 2)) (crate-dep (name "mv-binary-format") (req "^0.3.0") (features (quote ("fuzzing"))) (default-features #t) (kind 2)) (crate-dep (name "mv-core-types") (req "^0.3.0") (default-features #t) (kind 2)) (crate-dep (name "petgraph") (req "^0.5.1") (default-features #t) (kind 2)) (crate-dep (name "proptest") (req "^1.0.0") (default-features #t) (kind 2)))) (hash "14yvqmlmj6nkj6bqisa687hc1i9vf30x349zd4zk92jw122riniz") (features (quote (("fuzzing" "mv-binary-format/fuzzing")))) (yanked #t)))

(define-public crate-bytecode-verifier-tests-0.3 (crate (name "bytecode-verifier-tests") (vers "0.3.1") (deps (list (crate-dep (name "invalid-mutations") (req "^0.3.0") (default-features #t) (kind 2)) (crate-dep (name "move-bytecode-verifier") (req "^0.3.0") (default-features #t) (kind 2)) (crate-dep (name "mv-binary-format") (req "^0.3.0") (features (quote ("fuzzing"))) (default-features #t) (kind 2)) (crate-dep (name "mv-core-types") (req "^0.3.0") (default-features #t) (kind 2)) (crate-dep (name "petgraph") (req "^0.5.1") (default-features #t) (kind 2)) (crate-dep (name "proptest") (req "^1.0.0") (default-features #t) (kind 2)))) (hash "1xxhlprpiaicxnba3hy0xdl5qfgbw7mwzchd397ig68zqf3acqa2") (features (quote (("fuzzing" "mv-binary-format/fuzzing")))) (yanked #t)))

(define-public crate-bytecode-verifier-tests-0.3 (crate (name "bytecode-verifier-tests") (vers "0.3.2") (deps (list (crate-dep (name "invalid-mutations") (req "^0.3.0") (default-features #t) (kind 2)) (crate-dep (name "move-bytecode-verifier") (req "^0.3.0") (default-features #t) (kind 2)) (crate-dep (name "mv-binary-format") (req "^0.3.0") (features (quote ("fuzzing"))) (default-features #t) (kind 2)) (crate-dep (name "mv-core-types") (req "^0.3.0") (default-features #t) (kind 2)) (crate-dep (name "petgraph") (req "^0.5.1") (default-features #t) (kind 2)) (crate-dep (name "proptest") (req "^1.0.0") (default-features #t) (kind 2)))) (hash "144flz3nd3f49s0xcj5raakmn3xq4z6v0d8jmvm71r8c4wc82azw") (features (quote (("fuzzing" "mv-binary-format/fuzzing")))) (yanked #t)))

(define-public crate-bytecode-verifier-transactional-tests-0.1 (crate (name "bytecode-verifier-transactional-tests") (vers "0.1.6") (deps (list (crate-dep (name "datatest-stable") (req "^0.1.1") (default-features #t) (kind 2)) (crate-dep (name "move-transactional-test-runner") (req "^0.1.0") (default-features #t) (kind 2)))) (hash "1dab2z5wfc7k4dyi71mi96gr6zpcdw3yqbf12w9qsgzfkd0p0y8x") (yanked #t)))

(define-public crate-bytecode-verifier-transactional-tests-0.2 (crate (name "bytecode-verifier-transactional-tests") (vers "0.2.0") (deps (list (crate-dep (name "datatest-stable") (req "^0.1.1") (default-features #t) (kind 2)) (crate-dep (name "move-transactional-test-runner") (req "^0.2.0") (default-features #t) (kind 2)))) (hash "1c57m1dpxbdsj6wvl2sr47xxq0p4fbkxz3xrfs9vkdpd4kmc0r3v") (yanked #t)))

(define-public crate-bytecode-verifier-transactional-tests-0.2 (crate (name "bytecode-verifier-transactional-tests") (vers "0.2.1") (deps (list (crate-dep (name "datatest-stable") (req "^0.1.1") (default-features #t) (kind 2)) (crate-dep (name "move-transactional-test-runner") (req "^0.2.0") (default-features #t) (kind 2)))) (hash "1dy98cddcb9132089ak12vin946bwrdjm5viv22xmii62nlqic2i") (yanked #t)))

(define-public crate-bytecode-verifier-transactional-tests-0.3 (crate (name "bytecode-verifier-transactional-tests") (vers "0.3.0") (deps (list (crate-dep (name "datatest-stable") (req "^0.1.1") (default-features #t) (kind 2)) (crate-dep (name "move-transactional-test-runner") (req "^0.3.0") (default-features #t) (kind 2)))) (hash "1j7kmljqv17z2gyv808h6bsm4v5rjn34mbkddql51mcnv0lzyr3x") (yanked #t)))

(define-public crate-bytecode-verifier-transactional-tests-0.3 (crate (name "bytecode-verifier-transactional-tests") (vers "0.3.1") (deps (list (crate-dep (name "datatest-stable") (req "^0.1.1") (default-features #t) (kind 2)) (crate-dep (name "move-transactional-test-runner") (req "^0.3.0") (default-features #t) (kind 2)))) (hash "1qfbwddvnzgfbc3flvggh72mxzn6nyb36i1q6laj65hgyypw5xcx") (yanked #t)))

(define-public crate-bytecode-verifier-transactional-tests-0.3 (crate (name "bytecode-verifier-transactional-tests") (vers "0.3.2") (deps (list (crate-dep (name "datatest-stable") (req "^0.1.1") (default-features #t) (kind 2)) (crate-dep (name "move-transactional-test-runner") (req "^0.3.0") (default-features #t) (kind 2)))) (hash "1akxzr7b6fl29njbf6arqsc6nblx0z9169i15x35hdrf0pxy1xf6") (yanked #t)))

(define-public crate-bytecodec-0.1 (crate (name "bytecodec") (vers "0.1.0") (deps (list (crate-dep (name "bincode") (req "^1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "byteorder") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "trackable") (req "^0.2") (default-features #t) (kind 0)))) (hash "0rskbfy1bybd8vi9air9s1crl6xwcaxrjfb1kkwz9rbp5lcj2771") (features (quote (("json_codec" "serde" "serde_json") ("bincode_codec" "serde" "bincode"))))))

(define-public crate-bytecodec-0.1 (crate (name "bytecodec") (vers "0.1.1") (deps (list (crate-dep (name "bincode") (req "^1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "byteorder") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "trackable") (req "^0.2") (default-features #t) (kind 0)))) (hash "0bqknh67rc6fgw6jpy5cp77g6zhjs3llmr22ki20vi3zkd6iy184") (features (quote (("json_codec" "serde" "serde_json") ("bincode_codec" "serde" "bincode"))))))

(define-public crate-bytecodec-0.1 (crate (name "bytecodec") (vers "0.1.2") (deps (list (crate-dep (name "bincode") (req "^1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "byteorder") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "trackable") (req "^0.2") (default-features #t) (kind 0)))) (hash "0fgbgxrkzr6xganjbn8l08gbwxac6igj61agg8bfg3m7hcbywi6w") (features (quote (("json_codec" "serde" "serde_json") ("bincode_codec" "serde" "bincode"))))))

(define-public crate-bytecodec-0.2 (crate (name "bytecodec") (vers "0.2.0") (deps (list (crate-dep (name "bincode") (req "^1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "byteorder") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "trackable") (req "^0.2") (default-features #t) (kind 0)))) (hash "1h24skwlqba5fj1p3shvwr9l8sd3bb2aafp53lj6pqmmrp9qy7jl") (features (quote (("json_codec" "serde" "serde_json") ("bincode_codec" "serde" "bincode"))))))

(define-public crate-bytecodec-0.2 (crate (name "bytecodec") (vers "0.2.1") (deps (list (crate-dep (name "bincode") (req "^1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "byteorder") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "trackable") (req "^0.2") (default-features #t) (kind 0)))) (hash "12l5maplaq2rmyg7dr1wjdw6v6bl405wx3l8lz2m253p4200sa80") (features (quote (("json_codec" "serde" "serde_json") ("bincode_codec" "serde" "bincode"))))))

(define-public crate-bytecodec-0.2 (crate (name "bytecodec") (vers "0.2.2") (deps (list (crate-dep (name "bincode") (req "^1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "byteorder") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "trackable") (req "^0.2") (default-features #t) (kind 0)))) (hash "07myzzaa9bmmc6ljh0psxxhdg01pcwk2mkf9n389h2lrq60220fm") (features (quote (("json_codec" "serde" "serde_json") ("bincode_codec" "serde" "bincode"))))))

(define-public crate-bytecodec-0.2 (crate (name "bytecodec") (vers "0.2.3") (deps (list (crate-dep (name "bincode") (req "^1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "byteorder") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "trackable") (req "^0.2") (default-features #t) (kind 0)))) (hash "1vsi08zhp2lg7sdkl37hk39hhsa8z4q84cdzkw10mwb8ndr69iak") (features (quote (("json_codec" "serde" "serde_json") ("bincode_codec" "serde" "bincode"))))))

(define-public crate-bytecodec-0.2 (crate (name "bytecodec") (vers "0.2.4") (deps (list (crate-dep (name "bincode") (req "^1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "byteorder") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "trackable") (req "^0.2") (default-features #t) (kind 0)))) (hash "0sy7lqsxbgq5hf0bgchyw4ncihzhz9c45m04bikqh89921bkfarp") (features (quote (("json_codec" "serde" "serde_json") ("bincode_codec" "serde" "bincode"))))))

(define-public crate-bytecodec-0.2 (crate (name "bytecodec") (vers "0.2.5") (deps (list (crate-dep (name "bincode") (req "^1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "byteorder") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "trackable") (req "^0.2") (default-features #t) (kind 0)))) (hash "1rqw7sm9sx79675ly8567p9m0nb4nw5ygs401wi3nrb99m6cmy0b") (features (quote (("json_codec" "serde" "serde_json") ("bincode_codec" "serde" "bincode"))))))

(define-public crate-bytecodec-0.2 (crate (name "bytecodec") (vers "0.2.6") (deps (list (crate-dep (name "bincode") (req "^1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "byteorder") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "trackable") (req "^0.2") (default-features #t) (kind 0)))) (hash "1fjw778lzw7p26w3xmala79sshhnb4kk6mrn8jzjpayj3yzbw33x") (features (quote (("json_codec" "serde" "serde_json") ("bincode_codec" "serde" "bincode"))))))

(define-public crate-bytecodec-0.2 (crate (name "bytecodec") (vers "0.2.7") (deps (list (crate-dep (name "bincode") (req "^1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "byteorder") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "trackable") (req "^0.2") (default-features #t) (kind 0)))) (hash "1bx3v1mpy80h82y8m9cp4di0fncjmqdhwsil6f7yk203cdybh10z") (features (quote (("json_codec" "serde" "serde_json") ("bincode_codec" "serde" "bincode"))))))

(define-public crate-bytecodec-0.2 (crate (name "bytecodec") (vers "0.2.8") (deps (list (crate-dep (name "bincode") (req "^1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "byteorder") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "trackable") (req "^0.2") (default-features #t) (kind 0)))) (hash "14v6l3kaszmw9srr6rcaivwp3ibnl94rw3iv33ll6xm5gzp0dj8y") (features (quote (("json_codec" "serde" "serde_json") ("bincode_codec" "serde" "bincode"))))))

(define-public crate-bytecodec-0.2 (crate (name "bytecodec") (vers "0.2.9") (deps (list (crate-dep (name "bincode") (req "^1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "byteorder") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "trackable") (req "^0.2") (default-features #t) (kind 0)))) (hash "01dvh2plni1d2ani5sixqb4r61wx002mamz211ng7kxh5vvg8x49") (features (quote (("json_codec" "serde" "serde_json") ("bincode_codec" "serde" "bincode"))))))

(define-public crate-bytecodec-0.2 (crate (name "bytecodec") (vers "0.2.10") (deps (list (crate-dep (name "bincode") (req "^1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "byteorder") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "trackable") (req "^0.2") (default-features #t) (kind 0)))) (hash "06n5mmzmmpi284s7s11h8f0fxwcldhrx224znby4z1g5bv7qybhm") (features (quote (("json_codec" "serde" "serde_json") ("bincode_codec" "serde" "bincode"))))))

(define-public crate-bytecodec-0.2 (crate (name "bytecodec") (vers "0.2.11") (deps (list (crate-dep (name "bincode") (req "^1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "byteorder") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "trackable") (req "^0.2") (default-features #t) (kind 0)))) (hash "0wfb90kg30zynwpqf5cmgjzrn9iz9dz2wcq8yshlgwn56c2w3c81") (features (quote (("json_codec" "serde" "serde_json") ("bincode_codec" "serde" "bincode"))))))

(define-public crate-bytecodec-0.2 (crate (name "bytecodec") (vers "0.2.12") (deps (list (crate-dep (name "bincode") (req "^1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "byteorder") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "trackable") (req "^0.2") (default-features #t) (kind 0)))) (hash "1w120d5w3gxnwcs7imsw3bavxm9hq9w7xis04kpnrcswn1nfl2av") (features (quote (("json_codec" "serde" "serde_json") ("bincode_codec" "serde" "bincode"))))))

(define-public crate-bytecodec-0.2 (crate (name "bytecodec") (vers "0.2.13") (deps (list (crate-dep (name "bincode") (req "^1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "byteorder") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "trackable") (req "^0.2") (default-features #t) (kind 0)))) (hash "0gn2rngc21r45dqrpy29vwdhyaj6djij8dci0svrknb0wr2v3nsk") (features (quote (("json_codec" "serde" "serde_json") ("bincode_codec" "serde" "bincode"))))))

(define-public crate-bytecodec-0.2 (crate (name "bytecodec") (vers "0.2.14") (deps (list (crate-dep (name "bincode") (req "^1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "byteorder") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "trackable") (req "^0.2") (default-features #t) (kind 0)))) (hash "0mnp9wv71z546axb1vm0grcp41gbsgkrz1zspgydmp2rkrn2vksq") (features (quote (("json_codec" "serde" "serde_json") ("bincode_codec" "serde" "bincode"))))))

(define-public crate-bytecodec-0.2 (crate (name "bytecodec") (vers "0.2.15") (deps (list (crate-dep (name "bincode") (req "^1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "byteorder") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "trackable") (req "^0.2") (default-features #t) (kind 0)))) (hash "122jpgpib6phnk7jj0l66hknkwlmiscmnahv8nbxyx44s095b3gl") (features (quote (("json_codec" "serde" "serde_json") ("bincode_codec" "serde" "bincode"))))))

(define-public crate-bytecodec-0.2 (crate (name "bytecodec") (vers "0.2.16") (deps (list (crate-dep (name "bincode") (req "^1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "byteorder") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "trackable") (req "^0.2") (default-features #t) (kind 0)))) (hash "1a0ic2k4k3dl7ix2z5sy4mbnbnf7f2cfjl57r46bhp1phzpzfl8n") (features (quote (("json_codec" "serde" "serde_json") ("bincode_codec" "serde" "bincode"))))))

(define-public crate-bytecodec-0.2 (crate (name "bytecodec") (vers "0.2.17") (deps (list (crate-dep (name "bincode") (req "^1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "byteorder") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "trackable") (req "^0.2") (default-features #t) (kind 0)))) (hash "0hq3avzp2idz4nlvb9wrb9xjpzdw6kzwhfs9vs3dwzskyk9cmf38") (features (quote (("json_codec" "serde" "serde_json") ("bincode_codec" "serde" "bincode"))))))

(define-public crate-bytecodec-0.2 (crate (name "bytecodec") (vers "0.2.18") (deps (list (crate-dep (name "bincode") (req "^1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "byteorder") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "trackable") (req "^0.2") (default-features #t) (kind 0)))) (hash "1868pdnkknlhzw72h0awmxs7qdd1bfcrhmpsyafx9m9hgx96hb44") (features (quote (("json_codec" "serde" "serde_json") ("bincode_codec" "serde" "bincode"))))))

(define-public crate-bytecodec-0.3 (crate (name "bytecodec") (vers "0.3.0") (deps (list (crate-dep (name "bincode") (req "^1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "byteorder") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "trackable") (req "^0.2") (default-features #t) (kind 0)))) (hash "0hl4fywl115f15c885vkzma468wqknmk087hq7h7ryix3q1v88if") (features (quote (("json_codec" "serde" "serde_json") ("bincode_codec" "serde" "bincode"))))))

(define-public crate-bytecodec-0.3 (crate (name "bytecodec") (vers "0.3.1") (deps (list (crate-dep (name "bincode") (req "^1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "byteorder") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "trackable") (req "^0.2") (default-features #t) (kind 0)))) (hash "1hfnf80kpk19y6k3dc1bjxgaj8rf98hczy2pd2k1kq5c6l5d0akb") (features (quote (("json_codec" "serde" "serde_json") ("bincode_codec" "serde" "bincode"))))))

(define-public crate-bytecodec-0.3 (crate (name "bytecodec") (vers "0.3.2") (deps (list (crate-dep (name "bincode") (req "^1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "byteorder") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "trackable") (req "^0.2") (default-features #t) (kind 0)))) (hash "1kydif7vpm437ccxny08afnp5ind8d1p6jwy57405qzmaf22q788") (features (quote (("json_codec" "serde" "serde_json") ("bincode_codec" "serde" "bincode"))))))

(define-public crate-bytecodec-0.3 (crate (name "bytecodec") (vers "0.3.3") (deps (list (crate-dep (name "bincode") (req "^1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "byteorder") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "trackable") (req "^0.2") (default-features #t) (kind 0)))) (hash "01s8qg9x50s4c5r01960v522swkyh59h9phi7xibk4mf1lxxq3fg") (features (quote (("json_codec" "serde" "serde_json") ("bincode_codec" "serde" "bincode"))))))

(define-public crate-bytecodec-0.3 (crate (name "bytecodec") (vers "0.3.4") (deps (list (crate-dep (name "bincode") (req "^1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "byteorder") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "trackable") (req "^0.2") (default-features #t) (kind 0)))) (hash "0f792zwrlr0j8zylamiy03yj87v4kisa2xxx7lyvxn1zh29l01p7") (features (quote (("json_codec" "serde" "serde_json") ("bincode_codec" "serde" "bincode"))))))

(define-public crate-bytecodec-0.3 (crate (name "bytecodec") (vers "0.3.5") (deps (list (crate-dep (name "bincode") (req "^1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "byteorder") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "trackable") (req "^0.2") (default-features #t) (kind 0)))) (hash "10brlqk25bsdfarxhq6gz669vv9bxnv9w431vr665visz5b55ya9") (features (quote (("json_codec" "serde" "serde_json") ("bincode_codec" "serde" "bincode"))))))

(define-public crate-bytecodec-0.3 (crate (name "bytecodec") (vers "0.3.6") (deps (list (crate-dep (name "bincode") (req "^1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "byteorder") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "trackable") (req "^0.2") (default-features #t) (kind 0)))) (hash "0phlj78j7x99d5gk78cx1rxnh7wk73acq9azpdbxrsy9a4ik9j25") (features (quote (("json_codec" "serde" "serde_json") ("bincode_codec" "serde" "bincode"))))))

(define-public crate-bytecodec-0.4 (crate (name "bytecodec") (vers "0.4.0") (deps (list (crate-dep (name "bincode") (req "^1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "byteorder") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "trackable") (req "^0.2") (default-features #t) (kind 0)))) (hash "1jn6r1szq0my6byijy6rabk9lpql39h2rispdiqfx9fapl1cpgcr") (features (quote (("json_codec" "serde" "serde_json") ("bincode_codec" "serde" "bincode"))))))

(define-public crate-bytecodec-0.4 (crate (name "bytecodec") (vers "0.4.1") (deps (list (crate-dep (name "bincode") (req "^1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "byteorder") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "trackable") (req "^0.2") (default-features #t) (kind 0)))) (hash "1wpklrrx8aj1r6j0wxpbbjhxzas00a997hvm6g84s95ni56xp52f") (features (quote (("json_codec" "serde" "serde_json") ("bincode_codec" "serde" "bincode"))))))

(define-public crate-bytecodec-0.4 (crate (name "bytecodec") (vers "0.4.2") (deps (list (crate-dep (name "bincode") (req "^1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "byteorder") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "trackable") (req "^0.2") (default-features #t) (kind 0)))) (hash "0vy3z0kw5cirhw5m5kzns5mq0f2yp9y09pi3wkgzbq32kf3la8b4") (features (quote (("json_codec" "serde" "serde_json") ("bincode_codec" "serde" "bincode"))))))

(define-public crate-bytecodec-0.4 (crate (name "bytecodec") (vers "0.4.3") (deps (list (crate-dep (name "bincode") (req "^1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "byteorder") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "trackable") (req "^0.2") (default-features #t) (kind 0)))) (hash "0mr04d6ja833l2lnpyjid0b6sf1c3v8slf2wxf818z891803dxhj") (features (quote (("json_codec" "serde" "serde_json") ("bincode_codec" "serde" "bincode"))))))

(define-public crate-bytecodec-0.4 (crate (name "bytecodec") (vers "0.4.4") (deps (list (crate-dep (name "bincode") (req "^1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "byteorder") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "trackable") (req "^0.2") (default-features #t) (kind 0)))) (hash "0j6gkid9r2gwk736a95wikcxwjqzky3gl26ais06k78qw8nzaljj") (features (quote (("json_codec" "serde" "serde_json") ("bincode_codec" "serde" "bincode"))))))

(define-public crate-bytecodec-0.4 (crate (name "bytecodec") (vers "0.4.5") (deps (list (crate-dep (name "bincode") (req "^1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "byteorder") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "trackable") (req "^0.2") (default-features #t) (kind 0)))) (hash "0sd56cbs6qx6ar0wds7zrakfnipq1smdvk80fwfljbnz46hwyzc3") (features (quote (("json_codec" "serde" "serde_json") ("bincode_codec" "serde" "bincode"))))))

(define-public crate-bytecodec-0.4 (crate (name "bytecodec") (vers "0.4.6") (deps (list (crate-dep (name "bincode") (req "^1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "byteorder") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "trackable") (req "^0.2") (default-features #t) (kind 0)))) (hash "1b2wpc8jd99mcn4dnmf8rwx2sv52n5mc7wlzn37vwx7k0v9vkq5r") (features (quote (("json_codec" "serde" "serde_json") ("bincode_codec" "serde" "bincode"))))))

(define-public crate-bytecodec-0.4 (crate (name "bytecodec") (vers "0.4.7") (deps (list (crate-dep (name "bincode") (req "^1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "byteorder") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "trackable") (req "^0.2") (default-features #t) (kind 0)))) (hash "0mywfnqyy829y2c5pndvzzpwm551823m7hrqlkv0fpp9sq62vkfh") (features (quote (("json_codec" "serde" "serde_json") ("bincode_codec" "serde" "bincode"))))))

(define-public crate-bytecodec-0.4 (crate (name "bytecodec") (vers "0.4.8") (deps (list (crate-dep (name "bincode") (req "^1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "byteorder") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "trackable") (req "^0.2") (default-features #t) (kind 0)))) (hash "1bjcgh2dxygjny89plmm1nigg60wv6711xbaacnqpyhh5l2hd440") (features (quote (("json_codec" "serde" "serde_json") ("bincode_codec" "serde" "bincode"))))))

(define-public crate-bytecodec-0.4 (crate (name "bytecodec") (vers "0.4.9") (deps (list (crate-dep (name "bincode") (req "^1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "byteorder") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "trackable") (req "^0.2") (default-features #t) (kind 0)))) (hash "1l9gv65msy3nfkblggh3gq1a94np1mp3y8y0wjnb1iph2bdx69vq") (features (quote (("json_codec" "serde" "serde_json") ("bincode_codec" "serde" "bincode"))))))

(define-public crate-bytecodec-0.4 (crate (name "bytecodec") (vers "0.4.10") (deps (list (crate-dep (name "bincode") (req "^1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "byteorder") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "trackable") (req "^0.2") (default-features #t) (kind 0)))) (hash "1smrfa4r4lmjn8vpzvzpyj4yggnp29wa85xxjv4cpmj30axslgvq") (features (quote (("json_codec" "serde" "serde_json") ("bincode_codec" "serde" "bincode"))))))

(define-public crate-bytecodec-0.4 (crate (name "bytecodec") (vers "0.4.11") (deps (list (crate-dep (name "bincode") (req "^1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "byteorder") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "trackable") (req "^0.2") (default-features #t) (kind 0)))) (hash "1hn3r608da2sar914zq9a1rls1yzilv33kxazay68mb428a5d72r") (features (quote (("json_codec" "serde" "serde_json") ("bincode_codec" "serde" "bincode"))))))

(define-public crate-bytecodec-0.4 (crate (name "bytecodec") (vers "0.4.12") (deps (list (crate-dep (name "bincode") (req "^1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "byteorder") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "trackable") (req "^0.2") (default-features #t) (kind 0)))) (hash "1n0aqfx9mdd6lnwn06kfkgkdfxgx7b5va1xks2sndlsz2hc5ggqv") (features (quote (("json_codec" "serde" "serde_json") ("bincode_codec" "serde" "bincode"))))))

(define-public crate-bytecodec-0.4 (crate (name "bytecodec") (vers "0.4.13") (deps (list (crate-dep (name "bincode") (req "^1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "byteorder") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "trackable") (req "^0.2") (default-features #t) (kind 0)))) (hash "1rq2m3sy3q3nqbg1w8qx7n2w1j7lc8kd5zlrz7lad6mxry1ahvmi") (features (quote (("json_codec" "serde" "serde_json") ("bincode_codec" "serde" "bincode"))))))

(define-public crate-bytecodec-0.4 (crate (name "bytecodec") (vers "0.4.14") (deps (list (crate-dep (name "bincode") (req "^1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "byteorder") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "pin-project") (req "^1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.0") (features (quote ("io-util"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "trackable") (req "^0.2") (default-features #t) (kind 0)))) (hash "0zb1cb5pf4wxi4bg8m1qx3mdi98iddhnnr6yfnfcivbnwp1w60ly") (features (quote (("tokio-async" "tokio" "pin-project") ("json_codec" "serde" "serde_json") ("bincode_codec" "serde" "bincode"))))))

(define-public crate-bytecodec-0.4 (crate (name "bytecodec") (vers "0.4.15") (deps (list (crate-dep (name "bincode") (req "^1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "byteorder") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "pin-project") (req "^1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.0") (features (quote ("io-util"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "trackable") (req "^0.2") (default-features #t) (kind 0)))) (hash "09d3553m06sn0dnjp07jy3d8xviqh42i5y60sxcflbpkpg8ckx5d") (features (quote (("tokio-async" "tokio" "pin-project") ("json_codec" "serde" "serde_json") ("bincode_codec" "serde" "bincode"))))))

(define-public crate-bytecoding-0.0.1 (crate (name "bytecoding") (vers "0.0.1") (deps (list (crate-dep (name "bytecoding_derive") (req "^0.0.1") (default-features #t) (kind 0)) (crate-dep (name "bytes") (req "^1.1.0") (default-features #t) (kind 0)) (crate-dep (name "paste") (req "^1.0.6") (default-features #t) (kind 0)))) (hash "1c1ml3c4bxpc917qbdi9n697s1m79l4b87xhgj2f2ahmhw57kg8d")))

(define-public crate-bytecoding-0.1 (crate (name "bytecoding") (vers "0.1.0") (deps (list (crate-dep (name "bytecoding_derive") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "bytes") (req "^1.1.0") (default-features #t) (kind 0)) (crate-dep (name "paste") (req "^1.0.6") (default-features #t) (kind 0)))) (hash "0ji60gqrd9dy0qc7nbi8ssici31v4vxbd4dbsf68ppplcdxx0qxz")))

(define-public crate-bytecoding_derive-0.0.1 (crate (name "bytecoding_derive") (vers "0.0.1") (deps (list (crate-dep (name "itertools") (req "^0.10.3") (default-features #t) (kind 0)) (crate-dep (name "proc-macro-error") (req "^1.0.4") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1.0.36") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.15") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.86") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "0j9csg631jzyji7iv6z673zd08y6dgz1hszrpk0k5f61sfkldy24")))

(define-public crate-bytecoding_derive-0.1 (crate (name "bytecoding_derive") (vers "0.1.0") (deps (list (crate-dep (name "itertools") (req "^0.10.3") (default-features #t) (kind 0)) (crate-dep (name "num-iter") (req "^0.1.42") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2.14") (default-features #t) (kind 0)) (crate-dep (name "proc-macro-error") (req "^1.0.4") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1.0.36") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.15") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.86") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "1x0gids555dnmh5655dhg24dynl0swhys6d4jjqk6bd9c8nnay75")))

(define-public crate-bytecount-0.1 (crate (name "bytecount") (vers "0.1.1") (deps (list (crate-dep (name "quickcheck") (req "^0.3.1") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.3.14") (default-features #t) (kind 2)))) (hash "1c50zc4qv7f7vyam9c1iwvc97vabr03r06x1p89wnfydqf86s8q8")))

(define-public crate-bytecount-0.1 (crate (name "bytecount") (vers "0.1.2") (deps (list (crate-dep (name "quickcheck") (req "^0.3.1") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.3.14") (default-features #t) (kind 2)))) (hash "1yn8kh3rbaym9pvqxsnhfdbz92ngbgdzip9w37fpk7nwssjv7z9h")))

(define-public crate-bytecount-0.1 (crate (name "bytecount") (vers "0.1.4") (deps (list (crate-dep (name "quickcheck") (req "^0.3.1") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.3.14") (default-features #t) (kind 2)) (crate-dep (name "simd") (req "^0.1.1") (optional #t) (default-features #t) (kind 0)))) (hash "04ylwllxqhfpqkyqdzqx8b3f11y8i951fg4dn00273jp2lcw5qs9") (features (quote (("simd-accel" "simd") ("avx-accel" "simd-accel"))))))

(define-public crate-bytecount-0.1 (crate (name "bytecount") (vers "0.1.5") (deps (list (crate-dep (name "bencher") (req "^0.1") (default-features #t) (kind 2)) (crate-dep (name "quickcheck") (req "^0.3.1") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.3.14") (default-features #t) (kind 2)) (crate-dep (name "simd") (req "^0.1.1") (optional #t) (default-features #t) (kind 0)))) (hash "1v8ab05010jgki5hfks68jwsjsp7zzkqp2k2s0yjg3xr9zdpp7i8") (features (quote (("simd-accel" "simd") ("avx-accel" "simd-accel"))))))

(define-public crate-bytecount-0.1 (crate (name "bytecount") (vers "0.1.6") (deps (list (crate-dep (name "bencher") (req "^0.1") (default-features #t) (kind 2)) (crate-dep (name "quickcheck") (req "^0.3.1") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.3.14") (default-features #t) (kind 2)) (crate-dep (name "simd") (req "^0.1.1") (optional #t) (default-features #t) (kind 0)))) (hash "0jljlpwqzhcaqw1sr4qvpgb6h429ai7vvkvdc55nlwn6r3xhk3qy") (features (quote (("simd-accel" "simd") ("avx-accel" "simd-accel"))))))

(define-public crate-bytecount-0.1 (crate (name "bytecount") (vers "0.1.7") (deps (list (crate-dep (name "bencher") (req "^0.1.2") (default-features #t) (kind 2)) (crate-dep (name "quickcheck") (req "^0.4.1") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.3.15") (default-features #t) (kind 2)) (crate-dep (name "simd") (req "^0.2.0") (optional #t) (default-features #t) (kind 0)))) (hash "0l2h357m6fi05mdqzsb611h7jizafvwbvd3qc2gy5z210g1vggjb") (features (quote (("simd-accel" "simd") ("avx-accel" "simd-accel"))))))

(define-public crate-bytecount-0.2 (crate (name "bytecount") (vers "0.2.0") (deps (list (crate-dep (name "bencher") (req "^0.1.4") (default-features #t) (kind 2)) (crate-dep (name "quickcheck") (req "^0.4.1") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.3.16") (default-features #t) (kind 2)) (crate-dep (name "simd") (req "^0.2.0") (optional #t) (default-features #t) (kind 0)))) (hash "1pj9p47gxyw9qx4rrzm57gfdrlv4zzzrs8x7l53fm7b7cchl49xg") (features (quote (("simd-accel" "simd") ("avx-accel" "simd-accel"))))))

(define-public crate-bytecount-0.3 (crate (name "bytecount") (vers "0.3.0") (deps (list (crate-dep (name "bencher") (req "^0.1.4") (default-features #t) (kind 2)) (crate-dep (name "quickcheck") (req "^0.6") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.4") (default-features #t) (kind 2)) (crate-dep (name "simd") (req "^0.2.0") (optional #t) (default-features #t) (kind 0)))) (hash "04zd0xc8qmciwl50gakym4hym5l1rmxrw5n2pn0cfz0r0g2vi7c6") (features (quote (("simd-accel" "simd") ("avx-accel" "simd-accel"))))))

(define-public crate-bytecount-0.3 (crate (name "bytecount") (vers "0.3.1") (deps (list (crate-dep (name "bencher") (req "^0.1.4") (default-features #t) (kind 2)) (crate-dep (name "quickcheck") (req "^0.6") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.4") (default-features #t) (kind 2)) (crate-dep (name "simd") (req "^0.2.0") (optional #t) (default-features #t) (kind 0)))) (hash "0i7pzhd9qigy18glwqb2pyrjs84i33haad6zf8j90kn8gv6qa9c8") (features (quote (("simd-accel" "simd") ("avx-accel" "simd-accel"))))))

(define-public crate-bytecount-0.3 (crate (name "bytecount") (vers "0.3.2") (deps (list (crate-dep (name "criterion") (req "^0.2.4") (default-features #t) (kind 2)) (crate-dep (name "quickcheck") (req "^0.6") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.4") (default-features #t) (kind 2)) (crate-dep (name "simd") (req "^0.2.0") (optional #t) (default-features #t) (kind 0)))) (hash "1n6bmj66dixkvrm80yxmmln77baj9sw1qahcdv5xnmlz6p7djqgq") (features (quote (("simd-accel" "simd") ("html_report") ("avx-accel" "simd-accel"))))))

(define-public crate-bytecount-0.4 (crate (name "bytecount") (vers "0.4.0") (deps (list (crate-dep (name "criterion") (req "^0.2.4") (kind 2)) (crate-dep (name "packed_simd") (req "^0.3.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "quickcheck") (req "^0.6") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.4") (default-features #t) (kind 2)))) (hash "13qpy38z5wx0rzcdvr2h0ixbfgi1dbrif068il3hwn3k2mah88mr") (features (quote (("runtime-dispatch-simd") ("html_report") ("generic-simd" "packed_simd"))))))

(define-public crate-bytecount-0.5 (crate (name "bytecount") (vers "0.5.0") (deps (list (crate-dep (name "criterion") (req "^0.2.7") (kind 2)) (crate-dep (name "packed_simd") (req "^0.3.1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "quickcheck") (req "^0.8") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.6") (default-features #t) (kind 2)))) (hash "039vrn09fpw1iajsgk52sw5ivzsayvlsxrbwb1wvva11bhrw06f2") (features (quote (("runtime-dispatch-simd") ("html_report") ("generic-simd" "packed_simd"))))))

(define-public crate-bytecount-0.5 (crate (name "bytecount") (vers "0.5.1") (deps (list (crate-dep (name "criterion") (req "^0.2.7") (kind 2)) (crate-dep (name "packed_simd") (req "^0.3.1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "quickcheck") (req "^0.8") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.6") (default-features #t) (kind 2)))) (hash "0z6a280kiy4kg5v3qw97pbyvwycr17fsm41804i8zpq7nmads3xy") (features (quote (("runtime-dispatch-simd") ("html_report") ("generic-simd" "packed_simd"))))))

(define-public crate-bytecount-0.6 (crate (name "bytecount") (vers "0.6.0") (deps (list (crate-dep (name "criterion") (req "^0.3") (kind 2)) (crate-dep (name "packed_simd") (req "^0.3.1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "quickcheck") (req "^0.9") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.7") (default-features #t) (kind 2)))) (hash "0vplsx73zncb7mz8x0fs3k0p0rz5bmavj09vjk5nqn4z6fa7h0dh") (features (quote (("runtime-dispatch-simd") ("html_report") ("generic-simd" "packed_simd"))))))

(define-public crate-bytecount-0.6 (crate (name "bytecount") (vers "0.6.1") (deps (list (crate-dep (name "criterion") (req "^0.3") (kind 2)) (crate-dep (name "packed_simd") (req "^0.3.4") (optional #t) (default-features #t) (kind 0) (package "packed_simd_2")) (crate-dep (name "quickcheck") (req "^0.9") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.7") (default-features #t) (kind 2)))) (hash "0k3nfbqys7zzg9ax0zf2gsj2xfbypyyz30ykv0k23caxlwxpg6n3") (features (quote (("runtime-dispatch-simd") ("html_report") ("generic-simd" "packed_simd")))) (yanked #t)))

(define-public crate-bytecount-0.6 (crate (name "bytecount") (vers "0.6.2") (deps (list (crate-dep (name "criterion") (req "^0.3") (kind 2)) (crate-dep (name "packed_simd") (req "^0.3.4") (optional #t) (default-features #t) (kind 0) (package "packed_simd_2")) (crate-dep (name "quickcheck") (req "^0.9") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.7") (default-features #t) (kind 2)))) (hash "0bklbbl5ml9ic18s9kn5iix1grrqc6sypz6hvfn8sjc6zhgv7zkj") (features (quote (("runtime-dispatch-simd") ("html_report") ("generic-simd" "packed_simd"))))))

(define-public crate-bytecount-0.6 (crate (name "bytecount") (vers "0.6.3") (deps (list (crate-dep (name "criterion") (req "^0.3") (kind 2)) (crate-dep (name "packed_simd") (req "^0.3.8") (optional #t) (default-features #t) (kind 0) (package "packed_simd_2")) (crate-dep (name "quickcheck") (req "^1.0") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.8") (default-features #t) (kind 2)))) (hash "173wsvyagflb7ic3hpvp1db6q3dsigr452inslnzmsb3ix3nlrrc") (features (quote (("runtime-dispatch-simd") ("html_report") ("generic-simd" "packed_simd"))))))

(define-public crate-bytecount-0.6 (crate (name "bytecount") (vers "0.6.4") (deps (list (crate-dep (name "criterion") (req "^0.4") (kind 2)) (crate-dep (name "packed_simd") (req "^0.3.8") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "quickcheck") (req "^1.0") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.8") (default-features #t) (kind 2)))) (hash "1rrx6jbc3g50r0ac5rrr7rwqzch27wxbzvagp45wh4y8l81js5dd") (features (quote (("runtime-dispatch-simd") ("html_report") ("generic-simd" "packed_simd"))))))

(define-public crate-bytecount-0.6 (crate (name "bytecount") (vers "0.6.5") (deps (list (crate-dep (name "criterion") (req "^0.4") (kind 2)) (crate-dep (name "packed_simd") (req "^0.3.8") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "quickcheck") (req "^1.0") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.8") (default-features #t) (kind 2)))) (hash "13651lr31cjslq0dp9lgsbns16sz2rw159c03b0h2yi3nxvj98fi") (features (quote (("runtime-dispatch-simd") ("html_report") ("generic-simd" "packed_simd")))) (yanked #t)))

(define-public crate-bytecount-0.6 (crate (name "bytecount") (vers "0.6.6") (deps (list (crate-dep (name "criterion") (req "^0.4") (kind 2)) (crate-dep (name "packed_simd") (req "^0.3.8") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "quickcheck") (req "^1.0") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.8") (default-features #t) (kind 2)))) (hash "17iym1fi0x58i08i97hn8gr48078b9v8071mgvxjbmsxiya9jvym") (features (quote (("runtime-dispatch-simd") ("html_report") ("generic-simd" "packed_simd")))) (yanked #t)))

(define-public crate-bytecount-0.6 (crate (name "bytecount") (vers "0.6.7") (deps (list (crate-dep (name "criterion") (req "^0.4") (kind 2)) (crate-dep (name "packed_simd") (req "^0.3.8") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "quickcheck") (req "^1.0") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.8") (default-features #t) (kind 2)))) (hash "019j3basq13gzmasbqqlhf4076231aw1v63lbyp27ikgs4sz1rg1") (features (quote (("runtime-dispatch-simd") ("html_report") ("generic-simd" "packed_simd"))))))

(define-public crate-bytecount-0.6 (crate (name "bytecount") (vers "0.6.8") (deps (list (crate-dep (name "criterion") (req "^0.4") (kind 2)) (crate-dep (name "quickcheck") (req "^1.0") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.8") (default-features #t) (kind 2)))) (hash "1klqfjwn41fwmcqw4z03v6i4imgrf7lmf3b5s9v74hxir8hrps2w") (features (quote (("runtime-dispatch-simd") ("html_report") ("generic-simd"))))))

(define-public crate-bytecursor-0.1 (crate (name "bytecursor") (vers "0.1.0") (deps (list (crate-dep (name "sp-std") (req "^3") (kind 0)))) (hash "0g2g12y4b8164dh9d104dh974k0wj5ghn7fa5yyxn4dmgpkmhhyg")))

(define-public crate-bytecursor-0.1 (crate (name "bytecursor") (vers "0.1.1") (deps (list (crate-dep (name "sp-std") (req "^3") (kind 0)))) (hash "17af74zi85j2qd94cch05gqfnwmy0n1pjp9x9njgl5a1pgibybs4")))

(define-public crate-bytecursor-0.1 (crate (name "bytecursor") (vers "0.1.2") (deps (list (crate-dep (name "sp-std") (req "^3") (kind 0)))) (hash "041jdpipkp10xgyigfwsdj4dkd1px9n38z0y3xi86lf8db1pwj9m")))

(define-public crate-bytecursor-0.2 (crate (name "bytecursor") (vers "0.2.0") (hash "1ci1y5fgd963bwd7kjx5pml58vfbj2yi65mivai5n19pzmwqrlsi")))

(define-public crate-bytefield-0.1 (crate (name "bytefield") (vers "0.1.0") (deps (list (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (optional #t) (kind 0)))) (hash "1jy2jsghaqbwwj0r37nvlmzkv3wcn307p035088ncc21h8f98i0l") (v 2) (features2 (quote (("serde" "dep:serde"))))))

(define-public crate-byteflow-0.1 (crate (name "byteflow") (vers "0.1.0") (hash "0wqd5l7h2gdgn0z57nazgkki4a0126jb97kmaqvhrwhsdcdbqpa4") (yanked #t)))

(define-public crate-byteflow-0.2 (crate (name "byteflow") (vers "0.2.0") (deps (list (crate-dep (name "criterion") (req "^0.4") (features (quote ("html_reports"))) (default-features #t) (kind 2)) (crate-dep (name "num-traits") (req "^0.2.17") (default-features #t) (kind 0)))) (hash "0zd443nlk6bzxakk1x3x7pwcxgb785kfpwzl7bn06np6yc2zv0ng")))

(define-public crate-byteflow-0.2 (crate (name "byteflow") (vers "0.2.1") (deps (list (crate-dep (name "criterion") (req "^0.4") (features (quote ("html_reports"))) (default-features #t) (kind 2)) (crate-dep (name "num-traits") (req "^0.2.17") (default-features #t) (kind 0)))) (hash "0r4z7lc57pkb13214ph3i1bc3znw1p6fswm60kk6yy4qijmq959i")))

(define-public crate-bytefly-0.1 (crate (name "bytefly") (vers "0.1.0") (deps (list (crate-dep (name "byteorder") (req "^1.4.3") (default-features #t) (kind 0)))) (hash "01yafm54p4wka9j0nxy7qqy1aqs8s9xhq696m6l3pz068568m2vw")))

(define-public crate-bytefmt-0.1 (crate (name "bytefmt") (vers "0.1.1") (deps (list (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)))) (hash "0b4h7jqjz68pbmkdsvc5fsx4lfisb164q7hnvz0s06ks5jn78cch")))

(define-public crate-bytefmt-0.1 (crate (name "bytefmt") (vers "0.1.2") (deps (list (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)))) (hash "1xhb6ilcs5jf4n8ycfb9bbws9z2wrhj32lqhyzan3d8y2jhdh8cy")))

(define-public crate-bytefmt-0.1 (crate (name "bytefmt") (vers "0.1.3") (deps (list (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)))) (hash "1rnfd1g1900dad4951xns7ap12ay31m8b3016diqj5ds0y0hpx6b")))

(define-public crate-bytefmt-0.1 (crate (name "bytefmt") (vers "0.1.4") (deps (list (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)))) (hash "0ycnsffg74mz3l3skjs3npspfjmlcbkx09xnhlbc3p7cb15cx888")))

(define-public crate-bytefmt-0.1 (crate (name "bytefmt") (vers "0.1.5") (deps (list (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)))) (hash "1sxivqk9saxm6pjmvjkvzmf38c5n0g0q4nisqhw1akzczyhmqkp4")))

(define-public crate-bytefmt-0.1 (crate (name "bytefmt") (vers "0.1.6") (deps (list (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)))) (hash "03r7h7nmnqz10lvflixfrgbhwf4lgb7y8i8qs3rm0sadyjrds37f")))

(define-public crate-bytefmt-0.1 (crate (name "bytefmt") (vers "0.1.7") (deps (list (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)))) (hash "0x1xxlxfbmqhbqmcyjjvifsr46cbw02izlbwvba4f752b7q1l2sr")))

(define-public crate-byteformat-0.1 (crate (name "byteformat") (vers "0.1.0") (hash "1pwig52f995r42ikl8b43dwr7cl4845irgzd520zzlhbbmvvfc4j") (yanked #t)))

(define-public crate-bytehash-1 (crate (name "bytehash") (vers "1.0.0") (deps (list (crate-dep (name "blake2-rfc") (req "^0.2.17") (default-features #t) (kind 0)) (crate-dep (name "byteorder") (req "^1.2.4") (default-features #t) (kind 0)))) (hash "0pqnab5zwb34kzxqvwf9h0ynb774ljjmjy5d6qhkwa6vizqp92kb") (yanked #t)))

(define-public crate-bytehash-2 (crate (name "bytehash") (vers "2.0.0") (deps (list (crate-dep (name "blake2-rfc") (req "^0.2.17") (default-features #t) (kind 0)) (crate-dep (name "byteorder") (req "^1.2.4") (default-features #t) (kind 0)))) (hash "0gn6jxy08vna5f04rkyrs1vnwxdkhmzvmhq80axahjxl04wl7xl4") (yanked #t)))

(define-public crate-bytehash-3 (crate (name "bytehash") (vers "3.0.0") (deps (list (crate-dep (name "blake2-rfc") (req "^0.2.17") (default-features #t) (kind 0)) (crate-dep (name "byteorder") (req "^1.2.4") (default-features #t) (kind 0)))) (hash "17m7lb9jknf3bhgdaw2fcgn3f17ncvjhh36k2xayx6arhqg0p86q") (yanked #t)))

(define-public crate-bytehash-0.1 (crate (name "bytehash") (vers "0.1.0") (deps (list (crate-dep (name "blake2-rfc") (req "^0.2.17") (default-features #t) (kind 0)) (crate-dep (name "byteorder") (req "^1.2.4") (default-features #t) (kind 0)))) (hash "1baral4r93bphlrdyna48fszic1b8b8pr9223qfgx1wssycy7m40")))

(define-public crate-bytehash-0.2 (crate (name "bytehash") (vers "0.2.0") (deps (list (crate-dep (name "blake2-rfc") (req "^0.2.17") (default-features #t) (kind 0)) (crate-dep (name "byteorder") (req "^1.2.4") (default-features #t) (kind 0)))) (hash "0v40zldcmb46np1069jilc1wl75qh5dh4xkl63q3bbmsfysxb5r7")))

(define-public crate-bytehash-0.2 (crate (name "bytehash") (vers "0.2.1") (deps (list (crate-dep (name "blake2-rfc") (req "^0.2.17") (default-features #t) (kind 0)) (crate-dep (name "byteorder") (req "^1.2.4") (default-features #t) (kind 0)))) (hash "1h27vmkq1gfiqcn9pn0yc1zpfnhi6riwrkghcyni5ghish4kvza1")))

(define-public crate-bytehash-0.3 (crate (name "bytehash") (vers "0.3.0") (deps (list (crate-dep (name "blake2-rfc") (req "^0.2.17") (default-features #t) (kind 0)) (crate-dep (name "byteorder") (req "^1.2.4") (default-features #t) (kind 0)))) (hash "0qdv8immnhg8kvd5agx4p8n5aa08v6l88vjds3y571ghdnrfb918")))

(define-public crate-byteio-0.0.0 (crate (name "byteio") (vers "0.0.0") (hash "1lx3cw9amb04sql1kygjd9ili9vcsrbmy0piyz0yxmi3zsirdj0w")))

(define-public crate-byteio-0.1 (crate (name "byteio") (vers "0.1.0") (deps (list (crate-dep (name "byteorder") (req "^1") (kind 0)) (crate-dep (name "itertools") (req "^0.8") (default-features #t) (kind 2)) (crate-dep (name "quickcheck") (req "^0.8") (kind 2)) (crate-dep (name "quickcheck_macros") (req "^0.8") (default-features #t) (kind 2)))) (hash "1yi84kqbcllfrr5ymkhnp73v8nhcwxrb0lg88b187pqxmlac6pfl") (features (quote (("std") ("default" "std") ("alloc"))))))

(define-public crate-byteio-0.2 (crate (name "byteio") (vers "0.2.0") (deps (list (crate-dep (name "itertools") (req "^0.8") (default-features #t) (kind 2)) (crate-dep (name "quickcheck") (req "^0.8") (kind 2)) (crate-dep (name "quickcheck_macros") (req "^0.8") (default-features #t) (kind 2)))) (hash "0xpr321gw1mb1dbvpcby3fy31mxw554gy9chvw4x4j2v3r4jdjh2") (features (quote (("std") ("default" "std") ("alloc"))))))

(define-public crate-byteio-0.2 (crate (name "byteio") (vers "0.2.1") (deps (list (crate-dep (name "itertools") (req "^0.8") (default-features #t) (kind 2)) (crate-dep (name "quickcheck") (req "^0.8") (kind 2)) (crate-dep (name "quickcheck_macros") (req "^0.8") (default-features #t) (kind 2)))) (hash "048rsn7gjsvnjcif54bf09jcp6zdlxxkw727pi3l1bch9zj3hlmw") (features (quote (("std") ("default" "std") ("alloc"))))))

(define-public crate-byteio-0.2 (crate (name "byteio") (vers "0.2.2") (deps (list (crate-dep (name "itertools") (req "^0.8") (default-features #t) (kind 2)) (crate-dep (name "quickcheck") (req "^0.8") (kind 2)) (crate-dep (name "quickcheck_macros") (req "^0.8") (default-features #t) (kind 2)))) (hash "1pbbdigya31dhnjvfgfs480vqmig3ipinkq2mww73k8p366fq442") (features (quote (("std") ("default" "std") ("alloc"))))))

(define-public crate-byteio-0.2 (crate (name "byteio") (vers "0.2.3") (deps (list (crate-dep (name "itertools") (req "^0.8") (default-features #t) (kind 2)) (crate-dep (name "quickcheck") (req "^0.8") (kind 2)) (crate-dep (name "quickcheck_macros") (req "^0.8") (default-features #t) (kind 2)))) (hash "1bw7c872j8yilgsq3ny1fxjk5rpzxc03d1w07m2h6f5j4nmsla0n") (features (quote (("std") ("default" "std") ("alloc"))))))

(define-public crate-bytekey-0.2 (crate (name "bytekey") (vers "0.2.0") (deps (list (crate-dep (name "quickcheck") (req "*") (default-features #t) (kind 2)) (crate-dep (name "quickcheck_macros") (req "*") (default-features #t) (kind 2)))) (hash "12rfddzp4w0bhl1dw2wmmjs0gvgghn7n4pplj0qksdh211dknqiw")))

(define-public crate-bytekey-0.2 (crate (name "bytekey") (vers "0.2.1") (deps (list (crate-dep (name "quickcheck") (req "*") (default-features #t) (kind 2)) (crate-dep (name "quickcheck_macros") (req "*") (default-features #t) (kind 2)))) (hash "0y0917ng6kjfzmr0mcnmyr7vww3ga22a7sxrm0k1cbiwjh31rqgx")))

(define-public crate-bytekey-0.3 (crate (name "bytekey") (vers "0.3.0") (deps (list (crate-dep (name "quickcheck") (req "*") (default-features #t) (kind 2)) (crate-dep (name "quickcheck_macros") (req "*") (default-features #t) (kind 2)) (crate-dep (name "rustc-serialize") (req "*") (default-features #t) (kind 0)))) (hash "0pl7d099sm47lg6q10p93ang3rwiqj3vbpdw9w2bfm6qaaia9h76")))

(define-public crate-bytekey-0.4 (crate (name "bytekey") (vers "0.4.0") (deps (list (crate-dep (name "byteorder") (req "*") (default-features #t) (kind 0)) (crate-dep (name "quickcheck") (req "*") (default-features #t) (kind 2)) (crate-dep (name "quickcheck_macros") (req "*") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "*") (default-features #t) (kind 2)) (crate-dep (name "rustc-serialize") (req "*") (default-features #t) (kind 0)))) (hash "0kr0iprcpbkcf0rm4685ymq6yw1jhvw9jrx0c7ickd3phnfz5qc9")))

(define-public crate-bytekey-0.4 (crate (name "bytekey") (vers "0.4.1") (deps (list (crate-dep (name "byteorder") (req "*") (default-features #t) (kind 0)) (crate-dep (name "quickcheck") (req "*") (default-features #t) (kind 2)) (crate-dep (name "quickcheck_macros") (req "*") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "*") (default-features #t) (kind 2)) (crate-dep (name "rustc-serialize") (req "*") (default-features #t) (kind 0)))) (hash "01fcz6fgr1rf1smgsv470jlkh6dbdf993kfa87b745l93pl8bz8s")))

(define-public crate-bytekey-0.4 (crate (name "bytekey") (vers "0.4.2") (deps (list (crate-dep (name "byteorder") (req "*") (default-features #t) (kind 0)) (crate-dep (name "quickcheck") (req "*") (default-features #t) (kind 2)) (crate-dep (name "quickcheck_macros") (req "*") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "*") (default-features #t) (kind 2)) (crate-dep (name "rustc-serialize") (req "*") (default-features #t) (kind 0)))) (hash "0dwr3hczc4nrmrdsypxcfsvi01irw2rhp4cqj1dr2k110fj7frgc")))

(define-public crate-bytekey-fix-0.5 (crate (name "bytekey-fix") (vers "0.5.0") (deps (list (crate-dep (name "byteorder") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.5") (default-features #t) (kind 2)) (crate-dep (name "serde") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1") (default-features #t) (kind 2)) (crate-dep (name "utf-8") (req "^0.7.5") (default-features #t) (kind 0)))) (hash "0i712y26k5f08ihc6nbpmajkl8qzb96vi5v6zzipg5ww6m76wk6h")))

(define-public crate-bytekey-fix-0.5 (crate (name "bytekey-fix") (vers "0.5.1") (deps (list (crate-dep (name "byteorder") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.5") (default-features #t) (kind 2)) (crate-dep (name "serde") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1") (default-features #t) (kind 2)) (crate-dep (name "utf-8") (req "^0.7.5") (default-features #t) (kind 0)))) (hash "06d1hdy70j0gvcd1cp74sk0j7z1sjq6fsqj32xqvq4l57b5hypzv")))

(define-public crate-bytekey2-0.4 (crate (name "bytekey2") (vers "0.4.3") (deps (list (crate-dep (name "byteorder") (req "^1.3.4") (default-features #t) (kind 0)) (crate-dep (name "jemallocator") (req "^0.3.2") (default-features #t) (target "cfg(not(target_env = \"msvc\"))") (kind 0)) (crate-dep (name "rand") (req "^0.8.0") (default-features #t) (kind 2)) (crate-dep (name "serde") (req "^1.0.118") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0.118") (default-features #t) (kind 2)) (crate-dep (name "utf-8") (req "^0.7.5") (default-features #t) (kind 0)))) (hash "1n0kx9jqwvy8kpxs2ic01qandh575mb0sxcf8m1wqrnmlh3367if")))

(define-public crate-bytekey2-0.4 (crate (name "bytekey2") (vers "0.4.4") (deps (list (crate-dep (name "byteorder") (req "^1.3.4") (default-features #t) (kind 0)) (crate-dep (name "jemallocator") (req "^0.3.2") (default-features #t) (target "cfg(not(target_env = \"msvc\"))") (kind 0)) (crate-dep (name "rand") (req "^0.8.0") (default-features #t) (kind 2)) (crate-dep (name "serde") (req "^1.0.118") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0.118") (default-features #t) (kind 2)) (crate-dep (name "utf-8") (req "^0.7.5") (default-features #t) (kind 0)))) (hash "18jkxvxzz5xagz3aifissw5ba1h3fzrv8m4dai9syfs5zssg0mvd")))

(define-public crate-bytekey2-0.4 (crate (name "bytekey2") (vers "0.4.5") (deps (list (crate-dep (name "byteorder") (req "^1.3.4") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.0") (default-features #t) (kind 2)) (crate-dep (name "serde") (req "^1.0.118") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0.118") (default-features #t) (kind 2)) (crate-dep (name "utf-8") (req "^0.7.5") (default-features #t) (kind 0)))) (hash "1rdx9w6z6zhlldyb4y1360q1lp2r2zgl6qfls417x18w1rn7dcsi")))

(define-public crate-bytekind-0.1 (crate (name "bytekind") (vers "0.1.0") (deps (list (crate-dep (name "hex") (req "^0.4") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1") (features (quote ("derive"))) (default-features #t) (kind 2)) (crate-dep (name "serde_json") (req "^1") (default-features #t) (kind 2)) (crate-dep (name "unarray") (req "^0.1.4") (default-features #t) (kind 0)))) (hash "0z2nzx28illcz01ri51nd1l1y7l1ng83rkjxys663kyn5mwc3h53") (features (quote (("std") ("default" "std" "hex"))))))

(define-public crate-bytekind-0.1 (crate (name "bytekind") (vers "0.1.1") (deps (list (crate-dep (name "hex") (req "^0.4") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1") (features (quote ("derive"))) (default-features #t) (kind 2)) (crate-dep (name "serde_json") (req "^1") (default-features #t) (kind 2)) (crate-dep (name "static_assertions") (req "^1") (default-features #t) (kind 2)) (crate-dep (name "unarray") (req "^0.1.4") (default-features #t) (kind 0)))) (hash "15myxf4dqiagnlm8xs4kgpfic3wgmlhnr0ac99kgnw5171dxaa45") (features (quote (("std") ("default" "std" "hex"))))))

(define-public crate-bytekind-0.1 (crate (name "bytekind") (vers "0.1.2") (deps (list (crate-dep (name "hex") (req "^0.4") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1") (features (quote ("derive"))) (default-features #t) (kind 2)) (crate-dep (name "serde_json") (req "^1") (default-features #t) (kind 2)) (crate-dep (name "static_assertions") (req "^1") (default-features #t) (kind 2)) (crate-dep (name "unarray") (req "^0.1.4") (default-features #t) (kind 0)))) (hash "1qzbghbdvk707mn0s5s24sin8l4b8l1vn95bmfirnzxyfryix2h7") (features (quote (("std") ("default" "std" "hex"))))))

(define-public crate-bytekind-0.1 (crate (name "bytekind") (vers "0.1.3") (deps (list (crate-dep (name "hex") (req "^0.4") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1") (features (quote ("derive"))) (default-features #t) (kind 2)) (crate-dep (name "serde_json") (req "^1") (default-features #t) (kind 2)) (crate-dep (name "static_assertions") (req "^1") (default-features #t) (kind 2)) (crate-dep (name "unarray") (req "^0.1.4") (default-features #t) (kind 0)))) (hash "1qm8xfypklpjvabflp1ab7hniixll2rf0gm1x9a2izjrba12qssv") (features (quote (("std") ("default" "std" "hex"))))))

(define-public crate-bytekind-0.1 (crate (name "bytekind") (vers "0.1.4") (deps (list (crate-dep (name "hex") (req "^0.4") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1") (features (quote ("derive"))) (default-features #t) (kind 2)) (crate-dep (name "serde_json") (req "^1") (default-features #t) (kind 2)) (crate-dep (name "static_assertions") (req "^1") (default-features #t) (kind 2)) (crate-dep (name "unarray") (req "^0.1.4") (default-features #t) (kind 0)))) (hash "0akljdhy35808kfaaksnh4qz7z10r128z950iimil4xs3rsmgv6y") (features (quote (("std") ("default" "std" "hex"))))))

(define-public crate-bytekind-0.1 (crate (name "bytekind") (vers "0.1.5") (deps (list (crate-dep (name "hex") (req "^0.4") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "proptest") (req "^1.1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1") (features (quote ("derive"))) (default-features #t) (kind 2)) (crate-dep (name "serde_json") (req "^1") (default-features #t) (kind 2)) (crate-dep (name "static_assertions") (req "^1") (default-features #t) (kind 2)) (crate-dep (name "unarray") (req "^0.1.4") (default-features #t) (kind 0)))) (hash "0za71bqxvh2kjsmwfnr4a9ig6m9d86cv5563kh4mk1bkh9s96c7p") (features (quote (("std") ("default" "std" "hex"))))))

(define-public crate-bytekind-0.1 (crate (name "bytekind") (vers "0.1.6") (deps (list (crate-dep (name "hex") (req "^0.4") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "proptest") (req "^1.1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1") (features (quote ("derive"))) (default-features #t) (kind 2)) (crate-dep (name "serde_json") (req "^1") (default-features #t) (kind 2)) (crate-dep (name "static_assertions") (req "^1") (default-features #t) (kind 2)) (crate-dep (name "unarray") (req "^0.1.4") (default-features #t) (kind 0)))) (hash "1c5fiqs57z1bp2sbk2sffag8i1xhwf7h8zzlabccdrppnqpcz9hc") (features (quote (("std") ("default" "std" "hex"))))))

(define-public crate-bytekind-0.1 (crate (name "bytekind") (vers "0.1.7") (deps (list (crate-dep (name "hex") (req "^0.4") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "proptest") (req "^1.1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1") (features (quote ("derive"))) (default-features #t) (kind 2)) (crate-dep (name "serde_json") (req "^1") (default-features #t) (kind 2)) (crate-dep (name "static_assertions") (req "^1") (default-features #t) (kind 2)) (crate-dep (name "unarray") (req "^0.1.4") (default-features #t) (kind 0)))) (hash "1awzs1gca35rw0p550nf32wa9w2hqc1ijaqdnwcls6c7kybq94as") (features (quote (("std") ("default" "std" "hex"))))))

(define-public crate-bytekind-0.2 (crate (name "bytekind") (vers "0.2.0") (deps (list (crate-dep (name "hex") (req "^0.4") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "proptest") (req "^1.1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "schemars") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1") (features (quote ("derive"))) (default-features #t) (kind 2)) (crate-dep (name "serde_json") (req "^1") (default-features #t) (kind 2)) (crate-dep (name "static_assertions") (req "^1") (default-features #t) (kind 2)) (crate-dep (name "unarray") (req "^0.1.4") (default-features #t) (kind 0)))) (hash "06n4fhj8nqc0hm3x6mmzqpjnh777xzx28pr3k2cz2nq3601wfwjc") (features (quote (("std") ("default" "std" "hex"))))))

(define-public crate-bytelinebuf-0.1 (crate (name "bytelinebuf") (vers "0.1.0") (deps (list (crate-dep (name "bytes") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "futures") (req "^0.3.28") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "pin-project") (req "^1.0.12") (optional #t) (default-features #t) (kind 0)))) (hash "1ch5zqi682xdsnd1cwvgk1sl87krmys6zh9rvl14qpms9y0w3nj8") (features (quote (("stream" "futures" "pin-project") ("default"))))))

(define-public crate-bytelines-1 (crate (name "bytelines") (vers "1.0.0") (hash "0x9yvd003az0i7y1zbgwh2walzqlk54jg8k41a45cgsabm164p5j")))

(define-public crate-bytelines-1 (crate (name "bytelines") (vers "1.0.1") (hash "06x2zm0gg8cwxw068qf17l40532xydj81cla8b4pc54zgz74nck2")))

(define-public crate-bytelines-2 (crate (name "bytelines") (vers "2.0.0") (hash "1l5ppl722q2xvnlv4420hlcglil6rx4bjwb75s4vcsp5m93p26ni")))

(define-public crate-bytelines-2 (crate (name "bytelines") (vers "2.1.0") (hash "1y999b9qhx5x5bfnwgj4pzcslkdphv2jgf205p6fimzc6j4pi2r1")))

(define-public crate-bytelines-2 (crate (name "bytelines") (vers "2.2.0") (hash "0pdf39xrl1qdg2hf7zgiri3jjpvr3ak0xmz4rcs3q1hpfphg702b")))

(define-public crate-bytelines-2 (crate (name "bytelines") (vers "2.2.1") (hash "1nscv011nx8v34fws4cvgh6yjyqfqlla1nv6vlkvjzlz11k4lxjm")))

(define-public crate-bytelines-2 (crate (name "bytelines") (vers "2.2.2") (hash "0pv1dwbbqws84v31f64g6b3nxi0jbhi59cipwpg6651ys504w7kr")))

(define-public crate-bytelines-2 (crate (name "bytelines") (vers "2.3.0") (deps (list (crate-dep (name "tokio") (req "^1.14") (features (quote ("fs" "io-util"))) (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.14") (features (quote ("full"))) (default-features #t) (kind 2)))) (hash "1i43h3j3yv3az70jywgz69kwziid77x64aghpdbw1yyqa14hi73m")))

(define-public crate-bytelines-2 (crate (name "bytelines") (vers "2.4.0") (deps (list (crate-dep (name "futures") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.14") (features (quote ("fs" "io-util"))) (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.14") (features (quote ("full"))) (default-features #t) (kind 2)))) (hash "17x55pg0k30wjqfk8mqbcjh3x98afbx34rj5l7czqdf547isqkvq")))

(define-public crate-bytelines-2 (crate (name "bytelines") (vers "2.5.0") (deps (list (crate-dep (name "futures-util") (req "^0.3") (optional #t) (kind 0)) (crate-dep (name "tokio") (req "^1.14") (features (quote ("fs" "io-util"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.14") (features (quote ("full"))) (default-features #t) (kind 2)))) (hash "1jxacxpb7v0qgh325s5b7mfk90fr63jpr90dar8m47r27imnb5qj") (features (quote (("default" "tokio")))) (v 2) (features2 (quote (("tokio" "dep:tokio" "futures-util"))))))

(define-public crate-byteloaf-0.2 (crate (name "byteloaf") (vers "0.2.0") (hash "12b8807arx8s8qcirqqqwkmhrlr4hcd3f0h0fnkf9asjm84k10lz")))

(define-public crate-byteme-0.0.1 (crate (name "byteme") (vers "0.0.1-dev") (deps (list (crate-dep (name "num-derive") (req "^0.3.3") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2.14") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1.0.30") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.10") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.80") (features (quote ("full" "extra-traits"))) (default-features #t) (kind 0)) (crate-dep (name "trybuild") (req "^1.0.49") (features (quote ("diff"))) (default-features #t) (kind 2)))) (hash "0ddlhimf4hvbsbb928ljhkj08vm6kn7djk9qx3hjw7c5729pnmh9")))

(define-public crate-byteme-0.0.2 (crate (name "byteme") (vers "0.0.2-dev") (deps (list (crate-dep (name "num-derive") (req "^0.3.3") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2.14") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1.0.30") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.10") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.80") (features (quote ("full" "extra-traits"))) (default-features #t) (kind 0)) (crate-dep (name "trybuild") (req "^1.0.49") (features (quote ("diff"))) (default-features #t) (kind 2)))) (hash "1p1346081k1xfp7ambwg6m229dwj1gfxxsf406l82154b06z7spi")))

(define-public crate-byteme-0.0.3 (crate (name "byteme") (vers "0.0.3-dev") (deps (list (crate-dep (name "num-derive") (req "^0.3.3") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2.14") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1.0.30") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.10") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.80") (features (quote ("full" "extra-traits"))) (default-features #t) (kind 0)) (crate-dep (name "trybuild") (req "^1.0.49") (features (quote ("diff"))) (default-features #t) (kind 2)))) (hash "0r9ag7vxfgypdmm4xv3qj809i6s6bhk0ysjv8g1jbw7pi3vhk4gl")))

(define-public crate-byteme-0.0.4 (crate (name "byteme") (vers "0.0.4-dev") (deps (list (crate-dep (name "num-derive") (req "^0.3.3") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2.14") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1.0.30") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.10") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.80") (features (quote ("full" "extra-traits"))) (default-features #t) (kind 0)) (crate-dep (name "trybuild") (req "^1.0.49") (features (quote ("diff"))) (default-features #t) (kind 2)))) (hash "0fj7qnmrm5wn3z3kr2d7nnrkq2b8iqqs7zy8kivmwag0l1sm3wkc")))

(define-public crate-byteme-0.0.4 (crate (name "byteme") (vers "0.0.4") (deps (list (crate-dep (name "num-derive") (req "^0.3.3") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2.14") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1.0.30") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.10") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.80") (features (quote ("full" "extra-traits"))) (default-features #t) (kind 0)) (crate-dep (name "trybuild") (req "^1.0.49") (features (quote ("diff"))) (default-features #t) (kind 2)))) (hash "0mzhigc46qp0n7w394fmy3h9p4pbp0j8k1j8s45nzvaxsy32anb9")))

(define-public crate-byteme-0.0.5 (crate (name "byteme") (vers "0.0.5") (deps (list (crate-dep (name "num-derive") (req "^0.3.3") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2.15") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1.0.39") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.18") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.96") (features (quote ("full" "extra-traits"))) (default-features #t) (kind 0)) (crate-dep (name "trybuild") (req "^1.0.63") (features (quote ("diff"))) (default-features #t) (kind 2)))) (hash "0frdr3m8kz53ig3hqc3zkw7d6r03zybjkyas4bbm3s59pr8801s5")))

(define-public crate-bytemuck-0.1 (crate (name "bytemuck") (vers "0.1.0") (hash "1vsr6k04baqlblx7s5vg9xmnxyiz7qkk7v5016s6ykzyxdns5kzv") (features (quote (("extern_crate_alloc")))) (yanked #t)))

(define-public crate-bytemuck-0.1 (crate (name "bytemuck") (vers "0.1.1") (hash "07kj3s9w86hlhapzch28hsqs37f9c1dhv207xd818j0xrkxhi0py") (features (quote (("extern_crate_alloc")))) (yanked #t)))

(define-public crate-bytemuck-0.1 (crate (name "bytemuck") (vers "0.1.2") (hash "0cqb8v1gcarqdl55lrgp88rlalrxz2qxv3x2fjqj3jzjgvjqjlrv") (features (quote (("extern_crate_alloc")))) (yanked #t)))

(define-public crate-bytemuck-1 (crate (name "bytemuck") (vers "1.0.0") (hash "0z4vmibm8ylsn57cmc1prb67by4z995jkfl3zzx87l8b06h5haip") (features (quote (("extern_crate_alloc"))))))

(define-public crate-bytemuck-1 (crate (name "bytemuck") (vers "1.0.1") (hash "16rzsni2ppfcwkbnh049gjda0wna4hyykl3k540c69dgqls99y94") (features (quote (("extern_crate_alloc"))))))

(define-public crate-bytemuck-1 (crate (name "bytemuck") (vers "1.1.0") (hash "1xwidlcxvjz2ickvafpc88w4rybdx6l3xxrg5ssi7f40ymn513gy") (features (quote (("extern_crate_alloc"))))))

(define-public crate-bytemuck-1 (crate (name "bytemuck") (vers "1.2.0-alpha.1") (hash "08c499i1s0nwxfs85jp398qdvzhbv96fvkzpxa4rpfkr3sq02rsm") (features (quote (("extern_crate_alloc"))))))

(define-public crate-bytemuck-1 (crate (name "bytemuck") (vers "1.2.0") (hash "0cclc9v10hc1abqrxgirg3qbwa3ra3s0dai3xiwv9v4j4bgi7yip") (features (quote (("extern_crate_alloc"))))))

(define-public crate-bytemuck-1 (crate (name "bytemuck") (vers "1.3.0-alpha.0") (hash "1db114m8s7hjz41rww5zh7pas7k67s3i2wf40psf6j9w0y9mvvwy") (features (quote (("extern_crate_std" "extern_crate_alloc") ("extern_crate_alloc")))) (yanked #t)))

(define-public crate-bytemuck-1 (crate (name "bytemuck") (vers "1.3.0") (hash "0rf3s6b9izgclz1bqz7vclh7l7vprcy5m22y33jmx930d823c1nl") (features (quote (("extern_crate_std" "extern_crate_alloc") ("extern_crate_alloc")))) (yanked #t)))

(define-public crate-bytemuck-1 (crate (name "bytemuck") (vers "1.3.1") (hash "1scaac5xbfynzbpvz9yjbmg9ag2jalxfijapwlqh7xldf4li0ynv") (features (quote (("extern_crate_std" "extern_crate_alloc") ("extern_crate_alloc"))))))

(define-public crate-bytemuck-1 (crate (name "bytemuck") (vers "1.4.0") (deps (list (crate-dep (name "bytemuck_derive") (req "^1") (optional #t) (default-features #t) (kind 0)))) (hash "071043n73hwi55z9c55ga4v52v8a7ri56gqja8r98clkdyxns14j") (features (quote (("zeroable_maybe_uninit") ("extern_crate_std" "extern_crate_alloc") ("extern_crate_alloc") ("derive" "bytemuck_derive"))))))

(define-public crate-bytemuck-1 (crate (name "bytemuck") (vers "1.4.1") (deps (list (crate-dep (name "bytemuck_derive") (req "^1") (optional #t) (default-features #t) (kind 0)))) (hash "1b3pc5j3sj73d981470vbxflkx14dpqar49wyx6cbdd3bk4jxaj1") (features (quote (("zeroable_maybe_uninit") ("extern_crate_std" "extern_crate_alloc") ("extern_crate_alloc") ("derive" "bytemuck_derive"))))))

(define-public crate-bytemuck-1 (crate (name "bytemuck") (vers "1.5.0") (deps (list (crate-dep (name "bytemuck_derive") (req "^1") (optional #t) (default-features #t) (kind 0)))) (hash "18355qn3r9yp7ibg00r688sjx58g2qsjylwyq15w5b41b46asjss") (features (quote (("zeroable_maybe_uninit") ("extern_crate_std" "extern_crate_alloc") ("extern_crate_alloc") ("derive" "bytemuck_derive"))))))

(define-public crate-bytemuck-1 (crate (name "bytemuck") (vers "1.5.1") (deps (list (crate-dep (name "bytemuck_derive") (req "^1") (optional #t) (default-features #t) (kind 0)))) (hash "0n6zqqjkk80j0vyck95gr627anjkrql6cdl3iyx86fsnj0h7xmdy") (features (quote (("zeroable_maybe_uninit") ("extern_crate_std" "extern_crate_alloc") ("extern_crate_alloc") ("derive" "bytemuck_derive"))))))

(define-public crate-bytemuck-1 (crate (name "bytemuck") (vers "1.6.0") (deps (list (crate-dep (name "bytemuck_derive") (req "^1") (optional #t) (default-features #t) (kind 0)))) (hash "1gq26pxzsakibs9bfa7w9x97sb98h6k809r2ch3n6m60sbxlplhv") (features (quote (("zeroable_maybe_uninit") ("min_const_generics") ("extern_crate_std" "extern_crate_alloc") ("extern_crate_alloc") ("derive" "bytemuck_derive")))) (yanked #t)))

(define-public crate-bytemuck-1 (crate (name "bytemuck") (vers "1.6.1") (deps (list (crate-dep (name "bytemuck_derive") (req "^1") (optional #t) (default-features #t) (kind 0)))) (hash "0fp49s1n4dk8hda6yq8r6zx5d8853v0clmkdfjw5afyzdpab2i8d") (features (quote (("zeroable_maybe_uninit") ("min_const_generics") ("extern_crate_std" "extern_crate_alloc") ("extern_crate_alloc") ("derive" "bytemuck_derive")))) (yanked #t)))

(define-public crate-bytemuck-1 (crate (name "bytemuck") (vers "1.6.2") (deps (list (crate-dep (name "bytemuck_derive") (req "^1") (optional #t) (default-features #t) (kind 0)))) (hash "19yhagv02dxhxjjj1npfs6gyscv5z3ib5npxbjigs6vrh35rqdid") (features (quote (("zeroable_maybe_uninit") ("min_const_generics") ("extern_crate_std" "extern_crate_alloc") ("extern_crate_alloc") ("derive" "bytemuck_derive")))) (yanked #t)))

(define-public crate-bytemuck-1 (crate (name "bytemuck") (vers "1.6.3") (deps (list (crate-dep (name "bytemuck_derive") (req "^1") (optional #t) (default-features #t) (kind 0)))) (hash "120kghhz9w8x93gclyac5zmqvvka06hkjiw4vmfdkhgrr4aq35af") (features (quote (("zeroable_maybe_uninit") ("min_const_generics") ("extern_crate_std" "extern_crate_alloc") ("extern_crate_alloc") ("derive" "bytemuck_derive")))) (yanked #t)))

(define-public crate-bytemuck-1 (crate (name "bytemuck") (vers "1.7.0") (deps (list (crate-dep (name "bytemuck_derive") (req "^1") (optional #t) (default-features #t) (kind 0)))) (hash "0dc4i6s9l86h2wb2h4r4njp80lm3a5i3k860p9fph3sdf6mx4rlr") (features (quote (("zeroable_maybe_uninit") ("unsound_ptr_pod_impl") ("min_const_generics") ("extern_crate_std" "extern_crate_alloc") ("extern_crate_alloc") ("derive" "bytemuck_derive")))) (yanked #t)))

(define-public crate-bytemuck-1 (crate (name "bytemuck") (vers "1.7.1") (deps (list (crate-dep (name "bytemuck_derive") (req "^1") (optional #t) (default-features #t) (kind 0)))) (hash "0jxhavc7nr0v7j0fkmshf3mpvgji1b45ssvhpibabfbblrlr8imn") (features (quote (("zeroable_maybe_uninit") ("unsound_ptr_pod_impl") ("min_const_generics") ("extern_crate_std" "extern_crate_alloc") ("extern_crate_alloc") ("derive" "bytemuck_derive")))) (yanked #t)))

(define-public crate-bytemuck-1 (crate (name "bytemuck") (vers "1.7.2") (deps (list (crate-dep (name "bytemuck_derive") (req "^1") (optional #t) (default-features #t) (kind 0)))) (hash "0nrcrwj7giny1ds3g67g4y1fpb9h70a8cm4az272pf0xqi3755bj") (features (quote (("zeroable_maybe_uninit") ("unsound_ptr_pod_impl") ("min_const_generics") ("extern_crate_std" "extern_crate_alloc") ("extern_crate_alloc") ("derive" "bytemuck_derive"))))))

(define-public crate-bytemuck-1 (crate (name "bytemuck") (vers "1.7.3") (deps (list (crate-dep (name "bytemuck_derive") (req "^1") (optional #t) (default-features #t) (kind 0)))) (hash "0bvi884940pgp8a3jzx9yll8316894gfz153f1jip3f3p3k8k6a3") (features (quote (("zeroable_maybe_uninit") ("wasm_simd") ("unsound_ptr_pod_impl") ("nightly_portable_simd") ("min_const_generics") ("extern_crate_std" "extern_crate_alloc") ("extern_crate_alloc") ("derive" "bytemuck_derive"))))))

(define-public crate-bytemuck-1 (crate (name "bytemuck") (vers "1.8.0") (deps (list (crate-dep (name "bytemuck_derive") (req "^1") (optional #t) (default-features #t) (kind 0)))) (hash "1b8y2j3mcs3m5p9i12ljnr2nadypjx3qlq01d0ryfwa8qakir18f") (features (quote (("zeroable_maybe_uninit") ("wasm_simd") ("unsound_ptr_pod_impl") ("nightly_portable_simd") ("min_const_generics") ("extern_crate_std" "extern_crate_alloc") ("extern_crate_alloc") ("derive" "bytemuck_derive") ("aarch64_simd"))))))

(define-public crate-bytemuck-1 (crate (name "bytemuck") (vers "1.9.0") (deps (list (crate-dep (name "bytemuck_derive") (req "^1") (optional #t) (default-features #t) (kind 0)))) (hash "00gq5r11rpzvzq6mi1gg0n6p2k4wm3gskc0hpxbbik7s4lhhw7pf") (features (quote (("zeroable_maybe_uninit") ("wasm_simd") ("unsound_ptr_pod_impl") ("nightly_portable_simd") ("min_const_generics") ("extern_crate_std" "extern_crate_alloc") ("extern_crate_alloc") ("derive" "bytemuck_derive") ("aarch64_simd"))))))

(define-public crate-bytemuck-1 (crate (name "bytemuck") (vers "1.9.1") (deps (list (crate-dep (name "bytemuck_derive") (req "^1.1") (optional #t) (default-features #t) (kind 0)))) (hash "1k02rxlr412d2wfgv7j567g27zj11q66gsxfzx51k761vrdxisnd") (features (quote (("zeroable_maybe_uninit") ("wasm_simd") ("unsound_ptr_pod_impl") ("nightly_portable_simd") ("min_const_generics") ("extern_crate_std" "extern_crate_alloc") ("extern_crate_alloc") ("derive" "bytemuck_derive") ("aarch64_simd"))))))

(define-public crate-bytemuck-1 (crate (name "bytemuck") (vers "1.10.0") (deps (list (crate-dep (name "bytemuck_derive") (req "^1.1") (optional #t) (default-features #t) (kind 0)))) (hash "12j0vfv746rhjzbr555sxzrfw90s727njwn5sf7dyx62gs8zlgf5") (features (quote (("zeroable_maybe_uninit") ("wasm_simd") ("unsound_ptr_pod_impl") ("nightly_portable_simd") ("min_const_generics") ("extern_crate_std" "extern_crate_alloc") ("extern_crate_alloc") ("derive" "bytemuck_derive") ("aarch64_simd"))))))

(define-public crate-bytemuck-1 (crate (name "bytemuck") (vers "1.11.0") (deps (list (crate-dep (name "bytemuck_derive") (req "^1.1") (optional #t) (default-features #t) (kind 0)))) (hash "0ddqr3zi5fqcaj49pdganrnjhlwzcr02v77j446icjp7cn47qdx5") (features (quote (("zeroable_maybe_uninit") ("wasm_simd") ("unsound_ptr_pod_impl") ("nightly_portable_simd") ("min_const_generics") ("extern_crate_std" "extern_crate_alloc") ("extern_crate_alloc") ("derive" "bytemuck_derive") ("aarch64_simd"))))))

(define-public crate-bytemuck-1 (crate (name "bytemuck") (vers "1.12.0") (deps (list (crate-dep (name "bytemuck_derive") (req "^1.2") (optional #t) (default-features #t) (kind 0)))) (hash "1zbib4nd8y3ypprkanr9fccm90z674vmhxwy3s754yhlnijcpy24") (features (quote (("zeroable_maybe_uninit") ("wasm_simd") ("unsound_ptr_pod_impl") ("nightly_portable_simd") ("min_const_generics") ("extern_crate_std" "extern_crate_alloc") ("extern_crate_alloc") ("derive" "bytemuck_derive") ("aarch64_simd")))) (yanked #t)))

(define-public crate-bytemuck-1 (crate (name "bytemuck") (vers "1.12.1") (deps (list (crate-dep (name "bytemuck_derive") (req "^1.2.1") (optional #t) (default-features #t) (kind 0)))) (hash "1nliallfh3f008ybs9d6ivac2pbvhh3adxdyqa7mk8dmj7j1amrg") (features (quote (("zeroable_maybe_uninit") ("wasm_simd") ("unsound_ptr_pod_impl") ("nightly_portable_simd") ("min_const_generics") ("extern_crate_std" "extern_crate_alloc") ("extern_crate_alloc") ("derive" "bytemuck_derive") ("aarch64_simd"))))))

(define-public crate-bytemuck-1 (crate (name "bytemuck") (vers "1.12.2") (deps (list (crate-dep (name "bytemuck_derive") (req "^1.2.1") (optional #t) (default-features #t) (kind 0)))) (hash "1n5zj5s8d4qi0arv9glz07lmxn8hawpgg08crlkzkqz6sksi9v2s") (features (quote (("zeroable_maybe_uninit") ("wasm_simd") ("unsound_ptr_pod_impl") ("nightly_portable_simd") ("min_const_generics") ("extern_crate_std" "extern_crate_alloc") ("extern_crate_alloc") ("derive" "bytemuck_derive") ("aarch64_simd"))))))

(define-public crate-bytemuck-1 (crate (name "bytemuck") (vers "1.12.3") (deps (list (crate-dep (name "bytemuck_derive") (req "^1.2.1") (optional #t) (default-features #t) (kind 0)))) (hash "0zwlaqkrp7r7bnl2n40x9ncpspb93d8xcckar61f54nal7csi8xa") (features (quote (("zeroable_maybe_uninit") ("wasm_simd") ("unsound_ptr_pod_impl") ("nightly_portable_simd") ("min_const_generics") ("extern_crate_std" "extern_crate_alloc") ("extern_crate_alloc") ("derive" "bytemuck_derive") ("aarch64_simd"))))))

(define-public crate-bytemuck-1 (crate (name "bytemuck") (vers "1.12.4") (deps (list (crate-dep (name "bytemuck_derive") (req "^1.2.1") (optional #t) (default-features #t) (kind 0)))) (hash "10168cwiksjgqwh8n30jsrapnbb0p934dglnia636dmdi7gcqjd5") (features (quote (("zeroable_maybe_uninit") ("zeroable_atomics") ("wasm_simd") ("unsound_ptr_pod_impl") ("nightly_stdsimd") ("nightly_portable_simd") ("min_const_generics") ("extern_crate_std" "extern_crate_alloc") ("extern_crate_alloc") ("derive" "bytemuck_derive") ("aarch64_simd")))) (yanked #t)))

(define-public crate-bytemuck-1 (crate (name "bytemuck") (vers "1.13.0") (deps (list (crate-dep (name "bytemuck_derive") (req "^1.4") (optional #t) (default-features #t) (kind 0)))) (hash "14v3ps53drpljf48sfrw2sd8a61zv93n89bbp2q0r228n3md6hf0") (features (quote (("zeroable_maybe_uninit") ("zeroable_atomics") ("wasm_simd") ("unsound_ptr_pod_impl") ("nightly_stdsimd") ("nightly_portable_simd") ("min_const_generics") ("extern_crate_std" "extern_crate_alloc") ("extern_crate_alloc") ("derive" "bytemuck_derive") ("aarch64_simd"))))))

(define-public crate-bytemuck-1 (crate (name "bytemuck") (vers "1.13.1") (deps (list (crate-dep (name "bytemuck_derive") (req "^1.4") (optional #t) (default-features #t) (kind 0)))) (hash "1sifp93886b552fwbywmp5f4gysar7z62mhh4y8dh5gxhkkbrzhp") (features (quote (("zeroable_maybe_uninit") ("zeroable_atomics") ("wasm_simd") ("unsound_ptr_pod_impl") ("nightly_stdsimd") ("nightly_portable_simd") ("min_const_generics") ("extern_crate_std" "extern_crate_alloc") ("extern_crate_alloc") ("derive" "bytemuck_derive") ("aarch64_simd"))))))

(define-public crate-bytemuck-1 (crate (name "bytemuck") (vers "1.14.0") (deps (list (crate-dep (name "bytemuck_derive") (req "^1.4") (optional #t) (default-features #t) (kind 0)))) (hash "1ik1ma5n3bg700skkzhx50zjk7kj7mbsphi773if17l04pn2hk9p") (features (quote (("zeroable_maybe_uninit") ("zeroable_atomics") ("wasm_simd") ("unsound_ptr_pod_impl") ("nightly_stdsimd") ("nightly_portable_simd") ("nightly_docs") ("must_cast") ("min_const_generics") ("extern_crate_std" "extern_crate_alloc") ("extern_crate_alloc") ("derive" "bytemuck_derive") ("align_offset") ("aarch64_simd"))))))

(define-public crate-bytemuck-1 (crate (name "bytemuck") (vers "1.14.1") (deps (list (crate-dep (name "bytemuck_derive") (req "^1.4") (optional #t) (default-features #t) (kind 0)))) (hash "1n9bjrxhngiv0lq05f7kl0jw5wyms4z1vqv7q6a2nks01xh9097d") (features (quote (("zeroable_maybe_uninit") ("zeroable_atomics") ("wasm_simd") ("unsound_ptr_pod_impl") ("nightly_stdsimd") ("nightly_portable_simd") ("nightly_docs") ("must_cast") ("min_const_generics") ("extern_crate_std" "extern_crate_alloc") ("extern_crate_alloc") ("derive" "bytemuck_derive") ("align_offset") ("aarch64_simd"))))))

(define-public crate-bytemuck-1 (crate (name "bytemuck") (vers "1.14.2") (deps (list (crate-dep (name "bytemuck_derive") (req "^1.4") (optional #t) (default-features #t) (kind 0)))) (hash "0aylwb0l3zx2c212k2nwik4zmbhw5826y7icav0w2ja9vadxccga") (features (quote (("zeroable_maybe_uninit") ("zeroable_atomics") ("wasm_simd") ("unsound_ptr_pod_impl") ("nightly_stdsimd") ("nightly_portable_simd") ("nightly_docs") ("must_cast") ("min_const_generics") ("extern_crate_std" "extern_crate_alloc") ("extern_crate_alloc") ("derive" "bytemuck_derive") ("align_offset") ("aarch64_simd"))))))

(define-public crate-bytemuck-1 (crate (name "bytemuck") (vers "1.14.3") (deps (list (crate-dep (name "bytemuck_derive") (req "^1.4") (optional #t) (default-features #t) (kind 0)))) (hash "17xpdkrnw7vcc72cfnmbs6x1pndrh5naj86rkdb4h6k90m7h7vx2") (features (quote (("zeroable_maybe_uninit") ("zeroable_atomics") ("wasm_simd") ("unsound_ptr_pod_impl") ("nightly_stdsimd") ("nightly_portable_simd") ("nightly_docs") ("must_cast") ("min_const_generics") ("extern_crate_std" "extern_crate_alloc") ("extern_crate_alloc") ("derive" "bytemuck_derive") ("align_offset") ("aarch64_simd"))))))

(define-public crate-bytemuck-1 (crate (name "bytemuck") (vers "1.15.0") (deps (list (crate-dep (name "bytemuck_derive") (req "^1.4") (optional #t) (default-features #t) (kind 0)))) (hash "05gxh5i8vhjhr8b7abzla1k74m3khsifr439320s18rmfb2nhvax") (features (quote (("zeroable_maybe_uninit") ("zeroable_atomics") ("wasm_simd") ("unsound_ptr_pod_impl") ("nightly_stdsimd") ("nightly_portable_simd") ("nightly_docs") ("must_cast") ("min_const_generics") ("extern_crate_std" "extern_crate_alloc") ("extern_crate_alloc") ("derive" "bytemuck_derive") ("align_offset") ("aarch64_simd"))))))

(define-public crate-bytemuck-1 (crate (name "bytemuck") (vers "1.16.0") (deps (list (crate-dep (name "bytemuck_derive") (req "^1.4") (optional #t) (default-features #t) (kind 0)))) (hash "19dwdvjri09mhgrngy0737965pchm25ix2yma8sgwpjxrcalr0vq") (features (quote (("zeroable_maybe_uninit") ("zeroable_atomics") ("wasm_simd") ("unsound_ptr_pod_impl") ("nightly_stdsimd") ("nightly_portable_simd") ("nightly_docs") ("must_cast") ("min_const_generics") ("extern_crate_std" "extern_crate_alloc") ("extern_crate_alloc") ("derive" "bytemuck_derive") ("const_zeroed") ("align_offset") ("aarch64_simd"))))))

(define-public crate-bytemuck_derive-1 (crate (name "bytemuck_derive") (vers "1.0.0-alpha.2") (deps (list (crate-dep (name "bytemuck") (req "^1.2") (default-features #t) (kind 2)) (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (default-features #t) (kind 0)))) (hash "1w6kvv0b51y3bb80xpk4scnsmkv6adv0q6ld3zl01asr91q513qn")))

(define-public crate-bytemuck_derive-1 (crate (name "bytemuck_derive") (vers "1.0.0-alpha.3") (deps (list (crate-dep (name "bytemuck") (req "^1.2") (default-features #t) (kind 2)) (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (default-features #t) (kind 0)))) (hash "16fdfpc1wa4v09glv21d3qdpzxnpdmgm4f0hsp89y5vim072rddi")))

(define-public crate-bytemuck_derive-1 (crate (name "bytemuck_derive") (vers "1.0.0") (deps (list (crate-dep (name "bytemuck") (req "^1.2") (default-features #t) (kind 2)) (crate-dep (name "proc-macro2") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1") (default-features #t) (kind 0)))) (hash "1k59b6g2d87nf32qwhp73vng3al0zklxg64iiwf0pkxy74xf5ni8")))

(define-public crate-bytemuck_derive-1 (crate (name "bytemuck_derive") (vers "1.0.1") (deps (list (crate-dep (name "bytemuck") (req ">=1.2.0, <2.0.0") (default-features #t) (kind 2)) (crate-dep (name "proc-macro2") (req ">=1.0.0, <2.0.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req ">=1.0.0, <2.0.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req ">=1.0.0, <2.0.0") (default-features #t) (kind 0)))) (hash "0m6cwx3bxl326xr81z75dz5vzm87zmznhpikr19wnycz5y65y8cf")))

(define-public crate-bytemuck_derive-1 (crate (name "bytemuck_derive") (vers "1.1.0") (deps (list (crate-dep (name "proc-macro2") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1") (default-features #t) (kind 0)))) (hash "03nd7lc6ycqmlhb9bna70l5x1aqr5bkcani724fwcnwph4j3hbjn")))

(define-public crate-bytemuck_derive-1 (crate (name "bytemuck_derive") (vers "1.1.1") (deps (list (crate-dep (name "proc-macro2") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.15") (default-features #t) (kind 0)))) (hash "101whslj53fj6rvca7756mxbh8p5rihr17nc6mixl8ap1hcg9lng")))

(define-public crate-bytemuck_derive-1 (crate (name "bytemuck_derive") (vers "1.2.0") (deps (list (crate-dep (name "proc-macro2") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.99") (default-features #t) (kind 0)))) (hash "0r00ipqhq53fl3gnwwy15qi22n4p52yg8r5yp1nckla7n0gdz71k") (yanked #t)))

(define-public crate-bytemuck_derive-1 (crate (name "bytemuck_derive") (vers "1.2.1") (deps (list (crate-dep (name "proc-macro2") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.99") (default-features #t) (kind 0)))) (hash "1a94pi65wll5ydg6ya0h4c3kng4yk3nyjym20xj4jscglxgiz7hv")))

(define-public crate-bytemuck_derive-1 (crate (name "bytemuck_derive") (vers "1.3.0") (deps (list (crate-dep (name "proc-macro2") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.99") (default-features #t) (kind 0)))) (hash "1d1j74dgq9b0wx73hvirsyzr3hmi7ip16bfvwc3q0bzic2wk7qjz")))

(define-public crate-bytemuck_derive-1 (crate (name "bytemuck_derive") (vers "1.4.0") (deps (list (crate-dep (name "proc-macro2") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.99") (default-features #t) (kind 0)))) (hash "08k31ga1h8cs8p50k489vzc2ig7ldyx5q30z1h5d90sdjy543jhs")))

(define-public crate-bytemuck_derive-1 (crate (name "bytemuck_derive") (vers "1.4.1") (deps (list (crate-dep (name "proc-macro2") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2.0.1") (default-features #t) (kind 0)))) (hash "14m18wy2rd6k7bjs56695x00wm9k1a900mmkw5n71gcysaf5rppx")))

(define-public crate-bytemuck_derive-1 (crate (name "bytemuck_derive") (vers "1.5.0") (deps (list (crate-dep (name "proc-macro2") (req "^1.0.60") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2.0.1") (default-features #t) (kind 0)))) (hash "1cgj75df2v32l4fmvnp25xxkkz4lp6hz76f7hfhd55wgbzmvfnln")))

(define-public crate-bytemuck_derive-1 (crate (name "bytemuck_derive") (vers "1.6.0") (deps (list (crate-dep (name "proc-macro2") (req "^1.0.60") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2.0.1") (default-features #t) (kind 0)))) (hash "0q1gkhwdg8xaslanwa45cq8c6rw229l2k1iwz80p8cgd7wps7aad")))

(define-public crate-bytemuck_derive-1 (crate (name "bytemuck_derive") (vers "1.6.1") (deps (list (crate-dep (name "proc-macro2") (req "^1.0.60") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2.0.1") (default-features #t) (kind 0)))) (hash "0289v33y8ls2xc6ilmg13ljhixf9jghb4wr043wdimdylprgm71n")))

(define-public crate-bytemuck_derive-1 (crate (name "bytemuck_derive") (vers "1.7.0") (deps (list (crate-dep (name "proc-macro2") (req "^1.0.60") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2.0.1") (default-features #t) (kind 0)))) (hash "06vn0ds3xdnyaz8dh3sswjr1g5l49gi8h8a1ig9rp9bl8aq93s0y")))

(define-public crate-bytemuck_parsing-0.1 (crate (name "bytemuck_parsing") (vers "0.1.0") (deps (list (crate-dep (name "bytemuck") (req "^1.13") (default-features #t) (kind 0)))) (hash "0ac5f9gz0q8ldw0cqhsffhva7zm5546mlmgb7hm2hn3vnwg02iwp")))

(define-public crate-bytemuck_parsing-0.2 (crate (name "bytemuck_parsing") (vers "0.2.0") (deps (list (crate-dep (name "bytemuck") (req "^1.13") (default-features #t) (kind 0)) (crate-dep (name "derive_more") (req "^0.99.17") (features (quote ("from" "display"))) (kind 0)))) (hash "0awh3qza9ddwryqjglhz2lfkjaqm5pbpjr38zxha7ws5vv3ic7fc")))

(define-public crate-bytenum-0.1 (crate (name "bytenum") (vers "0.1.0") (deps (list (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (default-features #t) (kind 0)))) (hash "0jk71n81l5cpbl46wh4hpkgg4470nhrf83qbhmkbqry2gbdvh54l")))

(define-public crate-bytenum-0.1 (crate (name "bytenum") (vers "0.1.1") (deps (list (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (default-features #t) (kind 0)))) (hash "1a64dzvjiv1k5gj17bqqrfvj9n8r2hwa98k9vlml2gq5fq479fs4")))

(define-public crate-bytenum-0.1 (crate (name "bytenum") (vers "0.1.2") (deps (list (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (default-features #t) (kind 0)))) (hash "1jh3fl8dpn5aqhp703k5r7rn744b0c8vabz7j23m6ajk7a7ijakp")))

(define-public crate-bytenum-0.1 (crate (name "bytenum") (vers "0.1.3") (deps (list (crate-dep (name "darling") (req "^0.14.2") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (default-features #t) (kind 0)))) (hash "1s19n3jkqn5lkmm1dpsl9ylrjb0r2554grmjy5z24jhavnbyskzr")))

(define-public crate-bytenum-0.1 (crate (name "bytenum") (vers "0.1.4") (deps (list (crate-dep (name "darling") (req "^0.14.2") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (default-features #t) (kind 0)))) (hash "13w0fa6ig0rj4psrdmn4dkf86cas3x4ny8lnxgpv4p44fm3ymb8a")))

(define-public crate-bytenum-0.1 (crate (name "bytenum") (vers "0.1.5") (deps (list (crate-dep (name "darling") (req "^0.14.2") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (default-features #t) (kind 0)))) (hash "1hw362h807pkdwm58m2mmq10bcs6vb33vfqhs33xj7ri136n58zf")))

(define-public crate-bytenum-0.1 (crate (name "bytenum") (vers "0.1.6") (deps (list (crate-dep (name "darling") (req "^0.14.2") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (default-features #t) (kind 0)))) (hash "1i1sh5n5yhlk56v81hpfjnckjf65gn5dmp1pnyjxi4cbkwpp71wb")))

(define-public crate-bytenum-0.1 (crate (name "bytenum") (vers "0.1.7") (deps (list (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (default-features #t) (kind 0)))) (hash "0fpa391ki3xgky5hkifdn9cv0vhmaxqwh1bm6wc2qk1ng52fnd0v")))

(define-public crate-bytenum-0.1 (crate (name "bytenum") (vers "0.1.8") (deps (list (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (default-features #t) (kind 0)))) (hash "0k4ykfnw9n26lk64xws23q4y7ad9p5fr4mcsllksnyxfm508v8cq")))

(define-public crate-bytenum-0.1 (crate (name "bytenum") (vers "0.1.9") (deps (list (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (default-features #t) (kind 0)))) (hash "1ii1chid4f4mz15ggh4xaw1k2d2apqmxdk6jxac3psy99nk6lbx2")))

(define-public crate-byteorder-0.1 (crate (name "byteorder") (vers "0.1.0") (hash "0k2sanwcry5n0zd835vr953xjl3amylsw69bycj3m5yyli4xj7vh")))

(define-public crate-byteorder-0.1 (crate (name "byteorder") (vers "0.1.1") (deps (list (crate-dep (name "quickcheck") (req "*") (default-features #t) (kind 2)))) (hash "0v3051cq4ng551032q2krdwnnvqvkhpdh838j4xf0lk113z7qywv")))

(define-public crate-byteorder-0.2 (crate (name "byteorder") (vers "0.2.0") (deps (list (crate-dep (name "quickcheck") (req "*") (default-features #t) (kind 2)))) (hash "0v29xk01206wdyixss8ki0j5mav2vc93bv97j4q7vjb2snj0kb22")))

(define-public crate-byteorder-0.2 (crate (name "byteorder") (vers "0.2.1") (deps (list (crate-dep (name "quickcheck") (req "*") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "*") (default-features #t) (kind 2)))) (hash "1x902gcb4ik5by10h1zd6i11qslssmd5xk3j319dmknbgqmak02n")))

(define-public crate-byteorder-0.2 (crate (name "byteorder") (vers "0.2.2") (deps (list (crate-dep (name "quickcheck") (req "*") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "*") (default-features #t) (kind 2)))) (hash "1dhn0g6g6l002lywsrdbcc31in0zcbflgs5nwnqdbf9zjgwavsbs")))

(define-public crate-byteorder-0.2 (crate (name "byteorder") (vers "0.2.3") (deps (list (crate-dep (name "quickcheck") (req "*") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "*") (default-features #t) (kind 2)))) (hash "1zq2n5by9vp75jvcpb7m9ag826jar178j0d4b927pj4r05jxw0zy")))

(define-public crate-byteorder-0.2 (crate (name "byteorder") (vers "0.2.4") (deps (list (crate-dep (name "quickcheck") (req "*") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "*") (default-features #t) (kind 2)))) (hash "0byyqdirgxm974nxlgm9xyyjkchzc1ra0vzf4gcd4fad8zn2325w")))

(define-public crate-byteorder-0.2 (crate (name "byteorder") (vers "0.2.5") (deps (list (crate-dep (name "quickcheck") (req "*") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "*") (default-features #t) (kind 2)))) (hash "14rc5n1bky6vfnw5asw2nz78bp9lx8k3l6khvz8vvd0gddb95f23")))

(define-public crate-byteorder-0.2 (crate (name "byteorder") (vers "0.2.6") (deps (list (crate-dep (name "quickcheck") (req "*") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "*") (default-features #t) (kind 2)))) (hash "1ccsbcvbangiiaz0hpgziwqm0z665vpsr8pyqg9ywv8ias5416ly")))

(define-public crate-byteorder-0.2 (crate (name "byteorder") (vers "0.2.7") (deps (list (crate-dep (name "quickcheck") (req "*") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "*") (default-features #t) (kind 2)))) (hash "0zv58hcsnqswwp9aj4bnv4kjap0wjq2b3r9a5rb7lcvfl4szjwdi")))

(define-public crate-byteorder-0.2 (crate (name "byteorder") (vers "0.2.8") (deps (list (crate-dep (name "quickcheck") (req "*") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "*") (default-features #t) (kind 2)))) (hash "10yk7514dip2848kjf1z735m51799w4pk92z8gi9pdj56dlk88s7")))

(define-public crate-byteorder-0.2 (crate (name "byteorder") (vers "0.2.9") (deps (list (crate-dep (name "quickcheck") (req "*") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "*") (default-features #t) (kind 2)))) (hash "1aksgvjfss6q6vxnjb90ljhr04hnbp7rjxay7qq5l92lhz0q0ygb")))

(define-public crate-byteorder-0.2 (crate (name "byteorder") (vers "0.2.10") (deps (list (crate-dep (name "quickcheck") (req "*") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "*") (default-features #t) (kind 2)))) (hash "1bbvchy4fcsc83pjjqvghhqilz1i12amgg6y4arl4kb7d4maj9k4")))

(define-public crate-byteorder-0.2 (crate (name "byteorder") (vers "0.2.11") (deps (list (crate-dep (name "quickcheck") (req "*") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "*") (default-features #t) (kind 2)))) (hash "0jvzfki27vz7z7jab8xlvbm5aync4gnfl8mddaq7h92vkvvymabf")))

(define-public crate-byteorder-0.2 (crate (name "byteorder") (vers "0.2.12") (deps (list (crate-dep (name "quickcheck") (req "*") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "*") (default-features #t) (kind 2)))) (hash "0v7yxnpz59gsc961qvcpjjc0qjdbr049m2n4hd1ng1awfps64fcs")))

(define-public crate-byteorder-0.2 (crate (name "byteorder") (vers "0.2.13") (deps (list (crate-dep (name "bswap") (req "*") (default-features #t) (kind 0)) (crate-dep (name "quickcheck") (req "*") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "*") (default-features #t) (kind 2)))) (hash "010cqb1k2bi2d85zxnrf602kbmdvnxvsybznrlmiwh24saxsiqyh")))

(define-public crate-byteorder-0.2 (crate (name "byteorder") (vers "0.2.14") (deps (list (crate-dep (name "bswap") (req "*") (default-features #t) (kind 0)) (crate-dep (name "quickcheck") (req "*") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "*") (default-features #t) (kind 2)))) (hash "0bdqk22jphygy8li5f5yv8w82v4bknrqaygiw2d485i5jvc53wm2")))

(define-public crate-byteorder-0.3 (crate (name "byteorder") (vers "0.3.0") (deps (list (crate-dep (name "bswap") (req "*") (default-features #t) (kind 0)) (crate-dep (name "quickcheck") (req "*") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "*") (default-features #t) (kind 2)))) (hash "03msg61k11ma8jp2nv38xzn4ixw0inxwc0pbf9zxqlh1i5rjbik3")))

(define-public crate-byteorder-0.3 (crate (name "byteorder") (vers "0.3.1") (deps (list (crate-dep (name "bswap") (req "*") (default-features #t) (kind 0)) (crate-dep (name "quickcheck") (req "*") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "*") (default-features #t) (kind 2)))) (hash "058zfxl7anidi743bqx2lay1va7crhjvcqiklg5k1p4h2fygjq96")))

(define-public crate-byteorder-0.3 (crate (name "byteorder") (vers "0.3.2") (deps (list (crate-dep (name "bswap") (req "*") (default-features #t) (kind 0)) (crate-dep (name "quickcheck") (req "*") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "*") (default-features #t) (kind 2)))) (hash "082z2pb45nj6z88y7yq0zfzyzsfrak4yham3ypawfjwfizg0zvxf")))

(define-public crate-byteorder-0.3 (crate (name "byteorder") (vers "0.3.3") (deps (list (crate-dep (name "quickcheck") (req "*") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "*") (default-features #t) (kind 2)))) (hash "08znk64ycqj8iz5y18g14nj3ynh61sxxr29hzx7q6pfxw98q0yxg")))

(define-public crate-byteorder-0.3 (crate (name "byteorder") (vers "0.3.4") (deps (list (crate-dep (name "quickcheck") (req "*") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "*") (default-features #t) (kind 2)))) (hash "01wz47lv2p3slmw4i0dd7m6866f03rad7jhbb3f3nlpbsaqmx1rw")))

(define-public crate-byteorder-0.3 (crate (name "byteorder") (vers "0.3.5") (deps (list (crate-dep (name "quickcheck") (req "*") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "*") (default-features #t) (kind 2)))) (hash "1idsvlp1h2iyqrnhi1zb9x5mm0ql6lq2a6z6nipp5j03jf4mrhh2")))

(define-public crate-byteorder-0.3 (crate (name "byteorder") (vers "0.3.6") (deps (list (crate-dep (name "quickcheck") (req "*") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "*") (default-features #t) (kind 2)))) (hash "1s9zvr5bpkvwjv39ai0ax8vs0lmnkzhfsb38yg8aqn696npv6hfs")))

(define-public crate-byteorder-0.3 (crate (name "byteorder") (vers "0.3.7") (deps (list (crate-dep (name "quickcheck") (req "*") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "*") (default-features #t) (kind 2)))) (hash "09p8qs1npw5gcz1skwr88dgybs9g4l7pm35pn19sjg56cr41nxdw")))

(define-public crate-byteorder-0.3 (crate (name "byteorder") (vers "0.3.8") (deps (list (crate-dep (name "quickcheck") (req "*") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "*") (default-features #t) (kind 2)))) (hash "1v2m74m5xvgqw4ms3fjllj7p8nw7mdn5wilah7gayqx1gijw21gz")))

(define-public crate-byteorder-0.3 (crate (name "byteorder") (vers "0.3.9") (deps (list (crate-dep (name "quickcheck") (req "*") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "*") (default-features #t) (kind 2)))) (hash "0by6lb5d8z033f07v0nrnhyd876gdvihjrfkdk8g5p3cfwi8j2kf")))

(define-public crate-byteorder-0.3 (crate (name "byteorder") (vers "0.3.10") (deps (list (crate-dep (name "quickcheck") (req "*") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "*") (default-features #t) (kind 2)))) (hash "1jg3gpnycbav11fyshppczpf4arcw426g9zgara2ic4l3zdn7s86")))

(define-public crate-byteorder-0.3 (crate (name "byteorder") (vers "0.3.11") (deps (list (crate-dep (name "quickcheck") (req "*") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "*") (default-features #t) (kind 2)))) (hash "1m9b5nv172w0xdi10kv26whjfr5x6mv8jhqkbqva6xidvxbis1jr")))

(define-public crate-byteorder-0.3 (crate (name "byteorder") (vers "0.3.12") (deps (list (crate-dep (name "quickcheck") (req "^0.2") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.3") (default-features #t) (kind 2)))) (hash "07fizq4syb0v3qdxcxy9a1lwjf7h9k5hn6c4rah05fsxb2l3xn9w")))

(define-public crate-byteorder-0.3 (crate (name "byteorder") (vers "0.3.13") (deps (list (crate-dep (name "quickcheck") (req "^0.2") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.3") (default-features #t) (kind 2)))) (hash "0xd1vzp1yzw9f9qpm7w3mp9kqxdxwrwzqs4d620n6m4g194smci9")))

(define-public crate-byteorder-0.4 (crate (name "byteorder") (vers "0.4.0") (deps (list (crate-dep (name "quickcheck") (req "^0.2") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.3") (default-features #t) (kind 2)))) (hash "0gn5rdk6igl872c5ack9s5asl4ly1lx20wky1sfz3m01ck8mg636") (features (quote (("no-std"))))))

(define-public crate-byteorder-0.4 (crate (name "byteorder") (vers "0.4.1") (deps (list (crate-dep (name "quickcheck") (req "^0.2") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.3") (default-features #t) (kind 2)))) (hash "1rkiqb6c6psk8mqs6wqhzczvvwp5vq0kf8wzhdafqk2ycrsyvs48") (features (quote (("no-std"))))))

(define-public crate-byteorder-0.4 (crate (name "byteorder") (vers "0.4.2") (deps (list (crate-dep (name "quickcheck") (req "^0.2") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.3") (default-features #t) (kind 2)))) (hash "010k8mb3fi4jgbj4nxzggyi7zg2jvm7aqirdyf5c1348h4cb9j4n") (features (quote (("no-std"))))))

(define-public crate-byteorder-0.5 (crate (name "byteorder") (vers "0.5.0") (deps (list (crate-dep (name "quickcheck") (req "^0.2") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.3") (default-features #t) (kind 2)))) (hash "0k926lglv8bwwb7zc8c0xx0mkvfdzdy7q19g39n7irkjwi0ipcdv") (features (quote (("no-std"))))))

(define-public crate-byteorder-0.5 (crate (name "byteorder") (vers "0.5.1") (deps (list (crate-dep (name "quickcheck") (req "^0.2") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.3") (default-features #t) (kind 2)))) (hash "1qnznh7smxwfp217gwfrfk0bamxrac02ahzyqc05nzljalbkmjp4") (features (quote (("no-std"))))))

(define-public crate-byteorder-0.5 (crate (name "byteorder") (vers "0.5.2") (deps (list (crate-dep (name "quickcheck") (req "^0.2") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.3") (default-features #t) (kind 2)))) (hash "0vn2m7kcx8lq3ga37rqgb49qgzmm7w3b1mak7acq799lnarx0s1y") (features (quote (("std") ("default" "std"))))))

(define-public crate-byteorder-0.5 (crate (name "byteorder") (vers "0.5.3") (deps (list (crate-dep (name "quickcheck") (req "^0.2") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.3") (default-features #t) (kind 2)))) (hash "0ma8pkyz1jbglr29m1yzlc9ghmv6672nvsrn7zd0yn5jqs60xh8g") (features (quote (("std") ("default" "std"))))))

(define-public crate-byteorder-1 (crate (name "byteorder") (vers "1.0.0") (deps (list (crate-dep (name "quickcheck") (req "^0.4") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.3") (default-features #t) (kind 2)))) (hash "1j0qvrvpmk01v5qkmp5l7gmjvlpxxygivm1w074qb63bxsq7f2f4") (features (quote (("std") ("default" "std"))))))

(define-public crate-byteorder-1 (crate (name "byteorder") (vers "1.1.0") (deps (list (crate-dep (name "quickcheck") (req "^0.4") (kind 2)) (crate-dep (name "rand") (req "^0.3") (default-features #t) (kind 2)))) (hash "0g9d0cs8czlyf8cdanffilp2342vnrzzxbyf6ab0jpbgfa5p70gz") (features (quote (("std") ("i128") ("default" "std"))))))

(define-public crate-byteorder-1 (crate (name "byteorder") (vers "1.2.0") (deps (list (crate-dep (name "quickcheck") (req "^0.4") (kind 2)) (crate-dep (name "rand") (req "^0.3") (default-features #t) (kind 2)))) (hash "0pxqqzn4wi67d2ydwbkjqd2i3l3fspc3ldvc3fy9w8a0i78bc161") (features (quote (("std") ("i128") ("default" "std"))))))

(define-public crate-byteorder-1 (crate (name "byteorder") (vers "1.2.1") (deps (list (crate-dep (name "quickcheck") (req "^0.4") (kind 2)) (crate-dep (name "rand") (req "^0.3") (default-features #t) (kind 2)))) (hash "08qdzm6y639swc9crvkav59cp46lmfj84rlsbvcakb9zwyvhaa35") (features (quote (("std") ("i128") ("default" "std"))))))

(define-public crate-byteorder-1 (crate (name "byteorder") (vers "1.2.2") (deps (list (crate-dep (name "quickcheck") (req "^0.6") (kind 2)) (crate-dep (name "rand") (req "^0.4") (default-features #t) (kind 2)))) (hash "11rwmf4rifak70hb0rb3344rxg4xga05h7c0r6chpbg3gvzbvdbk") (features (quote (("std") ("i128") ("default" "std"))))))

(define-public crate-byteorder-1 (crate (name "byteorder") (vers "1.2.3") (deps (list (crate-dep (name "quickcheck") (req "^0.6") (kind 2)) (crate-dep (name "rand") (req "^0.4") (default-features #t) (kind 2)))) (hash "1sfj7w3gzdjlbj26lxmqdpf8ri5jlhzxn33n9wp0lss4x43bkh3l") (features (quote (("std") ("i128") ("default" "std"))))))

(define-public crate-byteorder-1 (crate (name "byteorder") (vers "1.2.4") (deps (list (crate-dep (name "quickcheck") (req "^0.6") (kind 2)) (crate-dep (name "rand") (req "^0.4") (default-features #t) (kind 2)))) (hash "1yd4ix7rkr6djqs285an6lvpf0dgmjh05iaqra7gxfb2xh4wb2c3") (features (quote (("std") ("i128") ("default" "std"))))))

(define-public crate-byteorder-1 (crate (name "byteorder") (vers "1.2.5") (deps (list (crate-dep (name "quickcheck") (req "^0.7") (kind 2)) (crate-dep (name "rand") (req "^0.5") (default-features #t) (kind 2)))) (hash "1i8zsp0kpiy3fvr3f75l1kjph1bjv94cxc8k7rd1syaibzqnzmmn") (features (quote (("std") ("i128") ("default" "std"))))))

(define-public crate-byteorder-1 (crate (name "byteorder") (vers "1.2.6") (deps (list (crate-dep (name "quickcheck") (req "^0.7") (kind 2)) (crate-dep (name "rand") (req "^0.5") (default-features #t) (kind 2)))) (hash "109pz3i8fpbfg1h8zn8izh02hfi71pwqkyqwd5w2wzfxb1c2qjch") (features (quote (("std") ("i128") ("default" "std"))))))

(define-public crate-byteorder-1 (crate (name "byteorder") (vers "1.2.7") (deps (list (crate-dep (name "quickcheck") (req "^0.7") (kind 2)) (crate-dep (name "rand") (req "^0.5") (default-features #t) (kind 2)))) (hash "0pgpxnfcsg1s7chdkpbigzl304yc2rbhzar2r6npnh957br8vy4l") (features (quote (("std") ("i128") ("default" "std"))))))

(define-public crate-byteorder-1 (crate (name "byteorder") (vers "1.3.0") (deps (list (crate-dep (name "quickcheck") (req "^0.8") (kind 2)) (crate-dep (name "rand") (req "^0.6") (default-features #t) (kind 2)))) (hash "1llv9ywnbciizlk814qfqs2w3nmmlssi5z9889rx50m3q3ab1w30") (features (quote (("std") ("i128") ("default" "std"))))))

(define-public crate-byteorder-1 (crate (name "byteorder") (vers "1.3.1") (deps (list (crate-dep (name "quickcheck") (req "^0.8") (kind 2)) (crate-dep (name "rand") (req "^0.6") (default-features #t) (kind 2)))) (hash "1syvclxqjwf6qfq98y3fiy82msjp7q8wh7qkvf9b5pkw585b26d0") (features (quote (("std") ("i128") ("default" "std"))))))

(define-public crate-byteorder-1 (crate (name "byteorder") (vers "1.3.2") (deps (list (crate-dep (name "doc-comment") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "quickcheck") (req "^0.8") (kind 2)) (crate-dep (name "rand") (req "^0.6") (default-features #t) (kind 2)))) (hash "1xbwjlmq2ziqjmjvkqxdx1yh136xxhilxd40bky1w4d7hn4xvhx7") (features (quote (("std") ("i128") ("default" "std"))))))

(define-public crate-byteorder-1 (crate (name "byteorder") (vers "1.3.3") (deps (list (crate-dep (name "doc-comment") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "quickcheck") (req "^0.8") (kind 2)) (crate-dep (name "rand") (req "^0.6") (default-features #t) (kind 2)))) (hash "0m0dl2z8m63rg803n33qv94ga8n29lxqll155b3n1s93hf7v3237") (features (quote (("std") ("i128") ("default" "std"))))))

(define-public crate-byteorder-1 (crate (name "byteorder") (vers "1.3.4") (deps (list (crate-dep (name "doc-comment") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "quickcheck") (req "^0.8") (kind 2)) (crate-dep (name "rand") (req "^0.6") (default-features #t) (kind 2)))) (hash "1pkjfhgjnq898g1d38ygcfi0msg3m6756cwv0sgysj1d26p8mi08") (features (quote (("std") ("i128") ("default" "std"))))))

(define-public crate-byteorder-1 (crate (name "byteorder") (vers "1.4.0") (deps (list (crate-dep (name "quickcheck") (req "^0.9.2") (kind 2)) (crate-dep (name "rand") (req "^0.7") (default-features #t) (kind 2)))) (hash "1nrh7y7g8wbbnhgsc16lyx48ssdgahfgfd0dbvjbw760f8i43spz") (features (quote (("std") ("i128") ("default" "std"))))))

(define-public crate-byteorder-1 (crate (name "byteorder") (vers "1.4.1") (deps (list (crate-dep (name "quickcheck") (req "^0.9.2") (kind 2)) (crate-dep (name "rand") (req "^0.7") (default-features #t) (kind 2)))) (hash "08g8pjv0ywypcsmi4s2lv287qyi0cn798frdqiy8q7zdbpfjp2gl") (features (quote (("std") ("i128") ("default" "std"))))))

(define-public crate-byteorder-1 (crate (name "byteorder") (vers "1.4.2") (deps (list (crate-dep (name "quickcheck") (req "^0.9.2") (kind 2)) (crate-dep (name "rand") (req "^0.7") (default-features #t) (kind 2)))) (hash "0srh0h0594jmsnbvm7n0g8xabhla8lwb3gn8s0fzd7d1snix2i5f") (features (quote (("std") ("i128") ("default" "std"))))))

(define-public crate-byteorder-1 (crate (name "byteorder") (vers "1.4.3") (deps (list (crate-dep (name "quickcheck") (req "^0.9.2") (kind 2)) (crate-dep (name "rand") (req "^0.7") (default-features #t) (kind 2)))) (hash "0456lv9xi1a5bcm32arknf33ikv76p3fr9yzki4lb2897p2qkh8l") (features (quote (("std") ("i128") ("default" "std"))))))

(define-public crate-byteorder-1 (crate (name "byteorder") (vers "1.5.0") (deps (list (crate-dep (name "quickcheck") (req "^0.9.2") (kind 2)) (crate-dep (name "rand") (req "^0.7") (default-features #t) (kind 2)))) (hash "0jzncxyf404mwqdbspihyzpkndfgda450l0893pz5xj685cg5l0z") (features (quote (("std") ("i128") ("default" "std")))) (rust-version "1.60")))

(define-public crate-byteorder-lite-0.1 (crate (name "byteorder-lite") (vers "0.1.0") (deps (list (crate-dep (name "quickcheck") (req "^0.9.2") (kind 2)) (crate-dep (name "rand") (req "^0.7") (default-features #t) (kind 2)))) (hash "15alafmz4b9az56z6x7glcbcb6a8bfgyd109qc3bvx07zx4fj7wg") (features (quote (("std") ("default" "std")))) (rust-version "1.60")))

(define-public crate-byteorder-pack-0.1 (crate (name "byteorder-pack") (vers "0.1.0") (deps (list (crate-dep (name "byteorder") (req "^1.4") (default-features #t) (kind 0)))) (hash "0p52r777xr1hfcdm8xdkvfdns0cyprsg2d0ira6h29wzfn74xyzm")))

(define-public crate-byteorder-pod-0.0.1 (crate (name "byteorder-pod") (vers "0.0.1") (deps (list (crate-dep (name "byteorder") (req "^0.5.3") (default-features #t) (kind 0)) (crate-dep (name "pod") (req "^0.4.0") (default-features #t) (kind 0)) (crate-dep (name "uninitialized") (req "^0.0.2") (optional #t) (default-features #t) (kind 0)))) (hash "1pz4cgglj3f60n7gb1s4n6y8sicdycmk5n85yv31f7mkynsk6a83") (features (quote (("default" "uninitialized"))))))

(define-public crate-byteorder-pod-0.1 (crate (name "byteorder-pod") (vers "0.1.0") (deps (list (crate-dep (name "byteorder") (req "^0.5.3") (default-features #t) (kind 0)) (crate-dep (name "pod") (req "^0.5.0") (default-features #t) (kind 0)) (crate-dep (name "uninitialized") (req "^0.0.2") (optional #t) (default-features #t) (kind 0)))) (hash "03zwir0axiv1vsphlvjlhpm8lj6g0wfiaqbbrqi5jfg7607nayfi") (features (quote (("default" "uninitialized"))))))

(define-public crate-byteorder-sgx-0.0.1 (crate (name "byteorder-sgx") (vers "0.0.1") (deps (list (crate-dep (name "doc-comment") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "quickcheck") (req "^0.8") (kind 2)) (crate-dep (name "rand") (req "^0.6") (default-features #t) (kind 2)) (crate-dep (name "sgx_tstd") (req "^1.0.9") (default-features #t) (target "cfg(not(target_env = \"sgx\"))") (kind 0)))) (hash "1nzsvy57r81wcpldnf22ldbflydfb8hwjqfwp7djwjma60zms99y") (features (quote (("std") ("i128") ("default" "std"))))))

(define-public crate-byteorder_async-1 (crate (name "byteorder_async") (vers "1.0.0") (deps (list (crate-dep (name "futures") (req "^0.3.4") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "quickcheck") (req "^0.8") (kind 2)) (crate-dep (name "rand") (req "^0.6") (default-features #t) (kind 2)) (crate-dep (name "tokio") (req "^0.2.18") (features (quote ("io-util"))) (optional #t) (default-features #t) (kind 0)))) (hash "1y6h98nbcq09qyxjlkx1yrxgga6g4zb0q4viwkp0y72gys78ziwc") (features (quote (("tokio_async" "tokio") ("std") ("i128") ("futures_async" "futures") ("default" "std"))))))

(define-public crate-byteorder_async-1 (crate (name "byteorder_async") (vers "1.0.1") (deps (list (crate-dep (name "futures") (req "^0.3.4") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "quickcheck") (req "^0.8") (kind 2)) (crate-dep (name "rand") (req "^0.6") (default-features #t) (kind 2)) (crate-dep (name "tokio") (req "^0.2.18") (features (quote ("io-util"))) (optional #t) (default-features #t) (kind 0)))) (hash "0yzjkjshw9cwm7lh92xs5a84wd8mb0lbbgvpnrb7fan7k7xmqhz4") (features (quote (("tokio_async" "tokio") ("std") ("i128") ("futures_async" "futures") ("default" "std"))))))

(define-public crate-byteorder_async-1 (crate (name "byteorder_async") (vers "1.1.0") (deps (list (crate-dep (name "futures") (req "^0.3.4") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "quickcheck") (req "^0.8") (kind 2)) (crate-dep (name "rand") (req "^0.6") (default-features #t) (kind 2)) (crate-dep (name "tokio") (req "^0.2.18") (features (quote ("io-util"))) (optional #t) (default-features #t) (kind 0)))) (hash "15l43s3rvsdq6mr84kmg4rl7ks8cckwp72haxy426d0sacrb6aaj") (features (quote (("tokio_async" "tokio") ("std") ("i128") ("futures_async" "futures") ("default" "std"))))))

(define-public crate-byteorder_async-1 (crate (name "byteorder_async") (vers "1.2.0") (deps (list (crate-dep (name "futures") (req "^0.3.4") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "quickcheck") (req "^0.8") (kind 2)) (crate-dep (name "rand") (req "^0.6") (default-features #t) (kind 2)) (crate-dep (name "tokio") (req "^0.2.18") (features (quote ("io-util"))) (optional #t) (default-features #t) (kind 0)))) (hash "1nnw1idv122canpnmq4dx977z5n82lzzj69nncwk9n5lj5v5b1a0") (features (quote (("tokio_async" "tokio") ("std") ("i128") ("futures_async" "futures") ("default" "std"))))))

(define-public crate-byteorder_core_io-0.5 (crate (name "byteorder_core_io") (vers "0.5.3") (deps (list (crate-dep (name "core_io") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "quickcheck") (req "^0.2") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.3") (default-features #t) (kind 2)))) (hash "09bji66bymp0vm79pcai1gq4akvcv1ywcwyddij3wv9bqzx4nxk3") (features (quote (("std") ("default" "std"))))))

(define-public crate-byteorder_slice-0.1 (crate (name "byteorder_slice") (vers "0.1.0") (hash "1h9fbw04j50s9pbwhlam84kg3bfi5rkgrz17g6f5w73sdva9xkz2")))

(define-public crate-byteorder_slice-0.2 (crate (name "byteorder_slice") (vers "0.2.0") (deps (list (crate-dep (name "byteorder") (req "1.*") (optional #t) (default-features #t) (kind 0)))) (hash "1m217m6n63cyhx9crr6w7zr1zrcxpqchskvvcq659gn3ncg9ykb2") (features (quote (("default") ("byteorder_compat" "byteorder"))))))

(define-public crate-byteorder_slice-0.3 (crate (name "byteorder_slice") (vers "0.3.0") (deps (list (crate-dep (name "byteorder") (req "^1.4") (default-features #t) (kind 0)) (crate-dep (name "hex-literal") (req "^0.3.1") (default-features #t) (kind 2)))) (hash "1k2bb0zw2fgyrjf6xxvqssvrj1sq958saigppczvl2kwgrmkbh0n")))

(define-public crate-byteorder_slice-1 (crate (name "byteorder_slice") (vers "1.0.0") (deps (list (crate-dep (name "byteorder") (req "^1.4") (default-features #t) (kind 0)) (crate-dep (name "hex-literal") (req "^0.3.4") (default-features #t) (kind 2)))) (hash "00snh7di9zfhckyj95gxb7qdbwxhzxnmawd6gjlqgplyzdlmy8lp")))

(define-public crate-byteorder_slice-2 (crate (name "byteorder_slice") (vers "2.0.0") (deps (list (crate-dep (name "byteorder") (req "^1.4") (default-features #t) (kind 0)) (crate-dep (name "hex-literal") (req "^0.3.4") (default-features #t) (kind 2)))) (hash "0qx11r3mxnkvycdnbbphs6f2kd3vjhv62ndkmgmhy3gavay04gxw")))

(define-public crate-byteorder_slice-3 (crate (name "byteorder_slice") (vers "3.0.0") (deps (list (crate-dep (name "byteorder") (req "^1.4") (default-features #t) (kind 0)) (crate-dep (name "hex-literal") (req "^0.3.4") (default-features #t) (kind 2)))) (hash "141i06ka1kj04jn8q7pqbxhylc5r64hj9x7qif79ay3k70q4wa8b")))

(define-public crate-byteordered-0.1 (crate (name "byteordered") (vers "0.1.0") (deps (list (crate-dep (name "byteorder") (req "^1.0.0") (default-features #t) (kind 0)))) (hash "1b1a0aj1gpiy6x8zijmixbymxh4ahjm5nm1vrfdb0vck9rcnpngl") (features (quote (("i128" "byteorder/i128"))))))

(define-public crate-byteordered-0.2 (crate (name "byteordered") (vers "0.2.0") (deps (list (crate-dep (name "byteorder") (req "^1.0.0") (default-features #t) (kind 0)))) (hash "0rcrk405iwz72nw666cydl3f2dbhfz5995d8wl0mz9wx04d90nsr") (features (quote (("i128" "byteorder/i128"))))))

(define-public crate-byteordered-0.2 (crate (name "byteordered") (vers "0.2.1") (deps (list (crate-dep (name "byteorder") (req "^1.0.0") (default-features #t) (kind 0)))) (hash "1w4dpdns6w32qm4w0257mi0k60dcdy0ngjk300hzv8qn801y09wy") (features (quote (("i128" "byteorder/i128"))))))

(define-public crate-byteordered-0.3 (crate (name "byteordered") (vers "0.3.0") (deps (list (crate-dep (name "byteorder") (req "^1.0.0") (default-features #t) (kind 0)))) (hash "0sqjw84nl97c4dp7n7frzc5xhq5sqw6pkv68dndphygq8iz76vzg") (features (quote (("i128" "byteorder/i128"))))))

(define-public crate-byteordered-0.3 (crate (name "byteordered") (vers "0.3.1") (deps (list (crate-dep (name "byteorder") (req "^1.0.0") (default-features #t) (kind 0)))) (hash "0xxzwqhcmx72ybw9z6b7agcnmhwizx1923lpmy2f9xn99gg5297j") (features (quote (("i128" "byteorder/i128"))))))

(define-public crate-byteordered-0.4 (crate (name "byteordered") (vers "0.4.0") (deps (list (crate-dep (name "byteorder") (req "^1.0.0") (default-features #t) (kind 0)))) (hash "09sypx8mzdjplqh8anfl43jcvlc9hs40a8cm1h2gfcmsncignkam") (features (quote (("i128" "byteorder/i128"))))))

(define-public crate-byteordered-0.4 (crate (name "byteordered") (vers "0.4.1") (deps (list (crate-dep (name "byteorder") (req "^1.0.0") (default-features #t) (kind 0)))) (hash "1ag91lvv01sl61zignxn8p0igkgkmaqfsrpnxg1zb1x0vhxza3x9") (features (quote (("i128" "byteorder/i128"))))))

(define-public crate-byteordered-0.5 (crate (name "byteordered") (vers "0.5.0") (deps (list (crate-dep (name "byteorder") (req "^1.0.0") (features (quote ("i128"))) (default-features #t) (kind 0)))) (hash "0dwvv1vld8j58r4m6g685lhcwl0n2pgbxpq7xzijd1a9mgl7ws1j")))

(define-public crate-byteordered-0.6 (crate (name "byteordered") (vers "0.6.0") (deps (list (crate-dep (name "byteorder") (req "^1.0.0") (features (quote ("i128"))) (default-features #t) (kind 0)))) (hash "1rl0cba0yv67h6chm1wvd25ywj64rcswhn8rp9541zzm4jacvwmv")))

(define-public crate-bytepack-0.1 (crate (name "bytepack") (vers "0.1.0") (hash "1iayka1jm5bhq0dxs5x1r0cp9pbrmwzlh2fpkapklbn06v4r5k5d")))

(define-public crate-bytepack-0.1 (crate (name "bytepack") (vers "0.1.1") (hash "1dsw5fcdlyccbb322a48iz17l1svjfvkgqk4f17x3g8xkxdfil6q")))

(define-public crate-bytepack-0.2 (crate (name "bytepack") (vers "0.2.0") (deps (list (crate-dep (name "bytepack_derive") (req "^0.1.1") (default-features #t) (kind 0)))) (hash "0581a1nm66gyfs84h2ikbm7ym7p2h1805x6lyc2m31gy2qdl5y5q")))

(define-public crate-bytepack-0.3 (crate (name "bytepack") (vers "0.3.0") (deps (list (crate-dep (name "bytepack_derive") (req "^0.1.1") (default-features #t) (kind 0)))) (hash "0qd1ic9p9mvgf1h4pcvlbrs6b6hbjpkf63i079gxdkfnpq05lg6l")))

(define-public crate-bytepack-0.3 (crate (name "bytepack") (vers "0.3.1") (deps (list (crate-dep (name "bytepack_derive") (req "^0.2") (default-features #t) (kind 0)))) (hash "0fzj2jvk4b8z09v1mrbqa636pmdby3fmmwgs3i2fv6jmwqnch8h9")))

(define-public crate-bytepack-0.4 (crate (name "bytepack") (vers "0.4.0") (deps (list (crate-dep (name "bytepack_derive") (req "^0.2") (default-features #t) (kind 0)))) (hash "1r4v4zpgqy15hbxvyv2bhplnz62znvjjzxhk7y13a0cimb5323j0")))

(define-public crate-bytepack-0.4 (crate (name "bytepack") (vers "0.4.1") (deps (list (crate-dep (name "bytepack_derive") (req "^0.2") (default-features #t) (kind 0)))) (hash "1kfxzf7mkp5dcpiiarxdinv9qgm8mlppnimcv623z6qfslyw3lzx")))

(define-public crate-bytepack_derive-0.1 (crate (name "bytepack_derive") (vers "0.1.0") (deps (list (crate-dep (name "quote") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^0.11") (default-features #t) (kind 0)))) (hash "0aqsfl60rwqx21ag86a232w9wl621lmyl4lac9ljask7m3wl1zc5")))

(define-public crate-bytepack_derive-0.1 (crate (name "bytepack_derive") (vers "0.1.1") (deps (list (crate-dep (name "quote") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^0.11") (default-features #t) (kind 0)))) (hash "1dfmv5l1iv1kkpmx4wjl8m6xwinaanyf71l0ww1grkm50d7baykb")))

(define-public crate-bytepack_derive-0.2 (crate (name "bytepack_derive") (vers "0.2.0") (deps (list (crate-dep (name "quote") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^0.11") (default-features #t) (kind 0)))) (hash "16lsmmzk8xi9r0l6xc0p4pd1b9gzpc15byg61rxby0zmwx6ml80c")))

(define-public crate-bytepeep-0.1 (crate (name "bytepeep") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^4.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "colored") (req "^2.0.0") (default-features #t) (kind 0)) (crate-dep (name "strum") (req "^0.24") (default-features #t) (kind 0)) (crate-dep (name "strum_macros") (req "^0.24") (default-features #t) (kind 0)))) (hash "04kll0qgjcl080v922hplcwyzlzcg3w6qz8wybk8p6i665wm2jxr")))

(define-public crate-bytepiece-0.1 (crate (name "bytepiece") (vers "0.1.0") (deps (list (crate-dep (name "aho-corasick") (req "^1.1") (default-features #t) (kind 0)) (crate-dep (name "base64-simd") (req "^0.8.0") (default-features #t) (kind 0)) (crate-dep (name "bytepiece_rs") (req "^0.1.0") (default-features #t) (kind 2)) (crate-dep (name "criterion") (req "^0.5.1") (default-features #t) (kind 2)) (crate-dep (name "once_cell") (req "^1.18.0") (default-features #t) (kind 0)) (crate-dep (name "ouroboros") (req "^0.18.0") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.9") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.107") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.48") (default-features #t) (kind 0)))) (hash "19syd6fr405lag3743fp66fhkk5rskzcrhr17b0ifaxia5knzw5l") (features (quote (("default"))))))

(define-public crate-bytepiece-0.2 (crate (name "bytepiece") (vers "0.2.0") (deps (list (crate-dep (name "aho-corasick") (req "^1.1") (default-features #t) (kind 0)) (crate-dep (name "base64-simd") (req "^0.8.0") (default-features #t) (kind 0)) (crate-dep (name "bytepiece_rs") (req "^0.1.0") (default-features #t) (kind 2)) (crate-dep (name "criterion") (req "^0.5.1") (default-features #t) (kind 2)) (crate-dep (name "once_cell") (req "^1.18.0") (default-features #t) (kind 0)) (crate-dep (name "ouroboros") (req "^0.18.0") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.9") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0") (default-features #t) (kind 0)))) (hash "16bs1smz8zvn541avr9g2m7bwwndfy1dvycyh6xw520p3yjxfhx1") (features (quote (("default"))))))

(define-public crate-bytepiece_rs-0.0.1 (crate (name "bytepiece_rs") (vers "0.0.1") (deps (list (crate-dep (name "aho-corasick") (req "^1.0.5") (default-features #t) (kind 0)) (crate-dep (name "base64") (req "^0.21.4") (default-features #t) (kind 0)) (crate-dep (name "bytes") (req "^1.5.0") (default-features #t) (kind 0)) (crate-dep (name "criterion") (req "^0.5") (default-features #t) (kind 2)) (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.9.5") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.107") (default-features #t) (kind 0)) (crate-dep (name "unic-normal") (req "^0.9.0") (default-features #t) (kind 0)))) (hash "0wfqc7hy6ag49gcif3vvn9nwd8gwd58w3kzcnikcg0x9mhb8w8sj")))

(define-public crate-bytepiece_rs-0.0.2 (crate (name "bytepiece_rs") (vers "0.0.2") (deps (list (crate-dep (name "aho-corasick") (req "^1.0.5") (default-features #t) (kind 0)) (crate-dep (name "base64") (req "^0.21.4") (default-features #t) (kind 0)) (crate-dep (name "bytes") (req "^1.5.0") (default-features #t) (kind 0)) (crate-dep (name "criterion") (req "^0.5") (default-features #t) (kind 2)) (crate-dep (name "gnuplot") (req "^0.0.39") (default-features #t) (kind 2)) (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.9.5") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.107") (default-features #t) (kind 0)) (crate-dep (name "unic-normal") (req "^0.9.0") (default-features #t) (kind 0)))) (hash "0lp9rnf2sds6wjpsaq4day42hyys6wv289l2wh7h8s68as1y8f8q")))

(define-public crate-bytepiece_rs-0.0.3 (crate (name "bytepiece_rs") (vers "0.0.3") (deps (list (crate-dep (name "aho-corasick") (req "^1.0.5") (default-features #t) (kind 0)) (crate-dep (name "base64") (req "^0.21.4") (default-features #t) (kind 0)) (crate-dep (name "criterion") (req "^0.5") (default-features #t) (kind 2)) (crate-dep (name "gnuplot") (req "^0.0.39") (default-features #t) (kind 2)) (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.9.5") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.107") (default-features #t) (kind 0)) (crate-dep (name "unic-normal") (req "^0.9.0") (default-features #t) (kind 0)))) (hash "09nmxya8zbhhwm8k0dfcckr62jp06rzichlxbqqb6syj7088s3zk")))

(define-public crate-bytepiece_rs-0.1 (crate (name "bytepiece_rs") (vers "0.1.0") (deps (list (crate-dep (name "aho-corasick") (req "^1.0.5") (default-features #t) (kind 0)) (crate-dep (name "base64") (req "^0.21.4") (default-features #t) (kind 0)) (crate-dep (name "criterion") (req "^0.5") (default-features #t) (kind 2)) (crate-dep (name "gnuplot") (req "^0.0.39") (default-features #t) (kind 2)) (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.9.5") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.107") (default-features #t) (kind 0)) (crate-dep (name "unic-normal") (req "^0.9.0") (default-features #t) (kind 0)))) (hash "06sqzizmhs3zabq64x0sh26b3jgsl4a0ch6rhkj3pwy6x9fk715l")))

(define-public crate-bytepiece_rs-0.2 (crate (name "bytepiece_rs") (vers "0.2.0") (deps (list (crate-dep (name "aho-corasick") (req "^1.1.1") (default-features #t) (kind 0)) (crate-dep (name "base64") (req "^0.21.4") (default-features #t) (kind 0)) (crate-dep (name "criterion") (req "^0.5") (default-features #t) (kind 2)) (crate-dep (name "gnuplot") (req "^0.0.39") (default-features #t) (kind 2)) (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)) (crate-dep (name "rayon") (req "^1.5") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.9.5") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.107") (default-features #t) (kind 0)) (crate-dep (name "unic-normal") (req "^0.9.0") (default-features #t) (kind 0)))) (hash "1xpij0524jilr4snys4zgqa25wnp5h8ik5fgdv2kkhw4xrsrq16y")))

(define-public crate-bytepiece_rs-0.2 (crate (name "bytepiece_rs") (vers "0.2.1") (deps (list (crate-dep (name "aho-corasick") (req "^1.1.1") (default-features #t) (kind 0)) (crate-dep (name "base64") (req "^0.21.4") (default-features #t) (kind 0)) (crate-dep (name "criterion") (req "^0.5") (default-features #t) (kind 2)) (crate-dep (name "gnuplot") (req "^0.0.39") (default-features #t) (kind 2)) (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)) (crate-dep (name "rayon") (req "^1.5") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.9.5") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.107") (default-features #t) (kind 0)) (crate-dep (name "unic-normal") (req "^0.9.0") (default-features #t) (kind 0)))) (hash "1nq39fn1xhkpnidilkqlvpdk9fzz362bh3iv5ldqvlpi1g92hgqf")))

(define-public crate-bytepiece_rs-0.2 (crate (name "bytepiece_rs") (vers "0.2.2") (deps (list (crate-dep (name "aho-corasick") (req "^1.1.1") (default-features #t) (kind 0)) (crate-dep (name "base64") (req "^0.21.4") (default-features #t) (kind 0)) (crate-dep (name "criterion") (req "^0.5") (default-features #t) (kind 2)) (crate-dep (name "gnuplot") (req "^0.0.39") (default-features #t) (kind 2)) (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)) (crate-dep (name "rayon") (req "^1.5") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.9.5") (default-features #t) (kind 0)) (crate-dep (name "seeded-random") (req "^0.6.0") (features (quote ("std"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.107") (default-features #t) (kind 0)) (crate-dep (name "unic-normal") (req "^0.9.0") (default-features #t) (kind 0)))) (hash "0mqbmav1ilscqx6v7wms2ld8mf7nq81ig52v3mjxqqn8r3zdlvas")))

(define-public crate-byteplug-0.0.1 (crate (name "byteplug") (vers "0.0.1") (deps (list (crate-dep (name "gl_generator") (req "^0.5.0") (default-features #t) (kind 1)) (crate-dep (name "glutin") (req "^0.24") (default-features #t) (kind 0)) (crate-dep (name "winit") (req "^0.22.2") (default-features #t) (kind 0)))) (hash "0y103ijnq7qyy1w35vwp782j52ls6jgfqrsfqsnq3s6w9r6xikdq")))

(define-public crate-byter-0.0.0 (crate (name "byter") (vers "0.0.0") (hash "0r09r0jmvfpid77vpcamidswy377gv5c0qvw83xkn1p99x621phj")))

(define-public crate-byteread-0.0.0 (crate (name "byteread") (vers "0.0.0") (hash "0zm9qcizaw5bpjdcw7xn1vjqzdh29zblcr01slrm0yd38q2n0npf")))

(define-public crate-bytereader-0.1 (crate (name "bytereader") (vers "0.1.0") (hash "12m4jn2kd847g5vs0i369gccl5nlhz4hl44hf96ncj1pxw3ia6pg")))

(define-public crate-byterepr-0.1 (crate (name "byterepr") (vers "0.1.0") (deps (list (crate-dep (name "bytemuck") (req "^1.2") (default-features #t) (kind 0)))) (hash "1x7xcajlggjblxb4c1racqazibi6jq8x539xnc6qarpcq82imsz7")))

(define-public crate-byteripper-0.1 (crate (name "byteripper") (vers "0.1.1") (deps (list (crate-dep (name "clap") (req "^2.33.3") (default-features #t) (kind 0)) (crate-dep (name "failure") (req "^0.1.8") (default-features #t) (kind 0)) (crate-dep (name "goblin") (req "^0.3.4") (default-features #t) (kind 0)) (crate-dep (name "libloading") (req "^0.7.0") (default-features #t) (kind 0)) (crate-dep (name "xfailure") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "07l66ywrh40dvjk61km418kvzxfahpkgvf34csnksd0fy9h54wvq")))

(define-public crate-byteripper-0.1 (crate (name "byteripper") (vers "0.1.2") (deps (list (crate-dep (name "anyhow") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^2.33.3") (default-features #t) (kind 0)) (crate-dep (name "goblin") (req "^0.3.4") (default-features #t) (kind 0)) (crate-dep (name "libloading") (req "^0.7.0") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1") (default-features #t) (kind 0)))) (hash "1lv8kqlaljv083cf9dysadaxvy8krzsp9qv3awcnqiicd7d69791")))

(define-public crate-bytes-0.0.1 (crate (name "bytes") (vers "0.0.1") (hash "15b8ivajwpqc1v8x7ygmh310af3jmnz3b8cyr4hcva5s2j5a9af9")))

(define-public crate-bytes-0.1 (crate (name "bytes") (vers "0.1.0") (deps (list (crate-dep (name "iobuf") (req "*") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.1.2") (default-features #t) (kind 2)))) (hash "0f5vnhwcp2srfnv592j65ijm8q7xil9rsxxvf5hjnwylifmmqlpd")))

(define-public crate-bytes-0.1 (crate (name "bytes") (vers "0.1.1") (deps (list (crate-dep (name "iobuf") (req "*") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.1.2") (default-features #t) (kind 2)))) (hash "162ab5rwigicpl323zlqz2brqq26c42a87fsn3gsz1671k6al0rm")))

(define-public crate-bytes-0.1 (crate (name "bytes") (vers "0.1.2") (deps (list (crate-dep (name "rand") (req "^0.1.2") (default-features #t) (kind 2)))) (hash "0bzc2d032qb5a7cpjwx8mh7a0ww26534481m5xz4d1s720r83dxl")))

(define-public crate-bytes-0.2 (crate (name "bytes") (vers "0.2.0") (deps (list (crate-dep (name "rand") (req "^0.2.1") (default-features #t) (kind 2)))) (hash "19kflzkrxb1rygfy2qgic7h34nywwn09zqprxwxz8bbiczr63ds3")))

(define-public crate-bytes-0.2 (crate (name "bytes") (vers "0.2.1") (deps (list (crate-dep (name "rand") (req "^0.2.1") (default-features #t) (kind 2)))) (hash "1j7ndfqwl531d3alfls63wdh9iqq16aq2ybxhvz9w3vxxg70v97g")))

(define-public crate-bytes-0.2 (crate (name "bytes") (vers "0.2.2") (deps (list (crate-dep (name "rand") (req "^0.2.1") (default-features #t) (kind 2)))) (hash "0cav59sy6681qgf70s12mgchzkmp86kfq9f3ib8rc2hyd5qdqw61")))

(define-public crate-bytes-0.2 (crate (name "bytes") (vers "0.2.3") (deps (list (crate-dep (name "rand") (req "^0.2.1") (default-features #t) (kind 2)))) (hash "1wqzpc3ayl1nll52w5vrb70mzv9dnyvy7bz027qv9vg04h4lv5sh")))

(define-public crate-bytes-0.2 (crate (name "bytes") (vers "0.2.4") (deps (list (crate-dep (name "rand") (req "^0.3.5") (default-features #t) (kind 2)))) (hash "13gdyr5whgdw74nh2iz0qj3pnsihlp5y6b4p2sblk1mxignl8lch")))

(define-public crate-bytes-0.2 (crate (name "bytes") (vers "0.2.5") (deps (list (crate-dep (name "rand") (req "^0.3.5") (default-features #t) (kind 2)))) (hash "1y389fjppmnrbgh18i71drjcmz6z3pacq5cp0yn1hhjqvaic7642")))

(define-public crate-bytes-0.2 (crate (name "bytes") (vers "0.2.6") (deps (list (crate-dep (name "rand") (req "^0.3.5") (default-features #t) (kind 2)))) (hash "17qcgn3nxpz8a2s4gbq9xk5ymh1lh4a549fvd59jrch6czwpq79g")))

(define-public crate-bytes-0.2 (crate (name "bytes") (vers "0.2.7") (deps (list (crate-dep (name "rand") (req "^0.3.5") (default-features #t) (kind 2)))) (hash "0hbkzjcv4p5v8mqjpmcjyjf5vb42ggqig7ls0j6qzy21abf3vaik")))

(define-public crate-bytes-0.2 (crate (name "bytes") (vers "0.2.8") (deps (list (crate-dep (name "rand") (req "^0.3.5") (default-features #t) (kind 2)))) (hash "1f7m5jdi24vgv5as039przlyvvhmicf6hz1dvdmcfsginj589n97")))

(define-public crate-bytes-0.2 (crate (name "bytes") (vers "0.2.9") (deps (list (crate-dep (name "rand") (req "^0.3.5") (default-features #t) (kind 2)))) (hash "1b1kqc296knmixcvr97qidwglgy5bd4kv2w63vvwa2952z1by3ka")))

(define-public crate-bytes-0.2 (crate (name "bytes") (vers "0.2.10") (deps (list (crate-dep (name "rand") (req "^0.3.5") (default-features #t) (kind 2)))) (hash "1vffm3yljl2bhkx2b9wn7z4dslji8wxjch6hy2kii06m7yr8yx7q")))

(define-public crate-bytes-0.2 (crate (name "bytes") (vers "0.2.11") (deps (list (crate-dep (name "rand") (req "^0.3.5") (default-features #t) (kind 2)))) (hash "17lbv6isma560fvdk499w992zhmkiy8p3yqsjv8qb6lacgjsvj4k")))

(define-public crate-bytes-0.3 (crate (name "bytes") (vers "0.3.0") (deps (list (crate-dep (name "rand") (req "^0.3.5") (default-features #t) (kind 2)))) (hash "09vcp9kh12pva2xn2ir79k90v1a0id8f4sdv1abn5ifw2bqsyaf1")))

(define-public crate-bytes-0.4 (crate (name "bytes") (vers "0.4.0") (deps (list (crate-dep (name "byteorder") (req "^1.0.0") (default-features #t) (kind 0)) (crate-dep (name "tokio-core") (req "^0.1.0") (default-features #t) (kind 2)))) (hash "12m10gdkvlwrn7d5p1j102sf3fy3gq5rs3izwvvbsz4kpf2knl46")))

(define-public crate-bytes-0.4 (crate (name "bytes") (vers "0.4.1") (deps (list (crate-dep (name "byteorder") (req "^1.0.0") (default-features #t) (kind 0)) (crate-dep (name "iovec") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "tokio-core") (req "^0.1.0") (default-features #t) (kind 2)))) (hash "15jblvs4524ll4kzli5p2l9bk0p0af544id4z6iy65dfc002l4a6")))

(define-public crate-bytes-0.4 (crate (name "bytes") (vers "0.4.2") (deps (list (crate-dep (name "byteorder") (req "^1.0.0") (default-features #t) (kind 0)) (crate-dep (name "iovec") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "tokio-core") (req "^0.1.0") (default-features #t) (kind 2)))) (hash "16j3lncfb7kdfai18xf5nsnibkb76pbw5pf24ys1g1qxm0yr6h9r")))

(define-public crate-bytes-0.4 (crate (name "bytes") (vers "0.4.3") (deps (list (crate-dep (name "byteorder") (req "^1.0.0") (default-features #t) (kind 0)) (crate-dep (name "iovec") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "tokio-core") (req "^0.1.0") (default-features #t) (kind 2)))) (hash "0lxm4b7ccmw3a4ap1f3mg39r32kn05lk5yc0hvqx2rsx258vivgr")))

(define-public crate-bytes-0.4 (crate (name "bytes") (vers "0.4.4") (deps (list (crate-dep (name "byteorder") (req "^1.0.0") (default-features #t) (kind 0)) (crate-dep (name "iovec") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde_test") (req "^1.0") (default-features #t) (kind 2)))) (hash "052wkiz74zpim0p4zbgy23wp3wgpf9mx9vm564il4igljdjz294b")))

(define-public crate-bytes-0.4 (crate (name "bytes") (vers "0.4.5") (deps (list (crate-dep (name "byteorder") (req "^1.0.0") (default-features #t) (kind 0)) (crate-dep (name "iovec") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde_test") (req "^1.0") (default-features #t) (kind 2)))) (hash "1mis6gwz0f585xivh48fxsj2ssrd2gr0q7a21kjf6pfcb1xzja6q")))

(define-public crate-bytes-0.4 (crate (name "bytes") (vers "0.4.6") (deps (list (crate-dep (name "byteorder") (req "^1.0.0") (default-features #t) (kind 0)) (crate-dep (name "iovec") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde_test") (req "^1.0") (default-features #t) (kind 2)))) (hash "1scwpm3k85kr47fkzdwrp332mh23qdpzvqxjr5npz5qqswvv8z8v")))

(define-public crate-bytes-0.4 (crate (name "bytes") (vers "0.4.7") (deps (list (crate-dep (name "byteorder") (req "^1.0.0") (default-features #t) (kind 0)) (crate-dep (name "iovec") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde_test") (req "^1.0") (default-features #t) (kind 2)))) (hash "0n8yqvi3hdipsvayw6g8783rsd9zxqm8pkc9ybslaxgvfv45079g")))

(define-public crate-bytes-0.4 (crate (name "bytes") (vers "0.4.8") (deps (list (crate-dep (name "byteorder") (req "^1.0.0") (default-features #t) (kind 0)) (crate-dep (name "iovec") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde_test") (req "^1.0") (default-features #t) (kind 2)))) (hash "1zwrpz9mfprqsby47z4z9qzp66i8shaqyn56rgqd6mv9ls4jklvx")))

(define-public crate-bytes-0.4 (crate (name "bytes") (vers "0.4.9") (deps (list (crate-dep (name "byteorder") (req "^1.1.0") (default-features #t) (kind 0)) (crate-dep (name "iovec") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde_test") (req "^1.0") (default-features #t) (kind 2)))) (hash "1f2gl8vdgz4qhga480gqz5klwrdjashx986mhfq49s1rwbhbhy71") (features (quote (("i128" "byteorder/i128"))))))

(define-public crate-bytes-0.4 (crate (name "bytes") (vers "0.4.10") (deps (list (crate-dep (name "byteorder") (req "^1.1.0") (default-features #t) (kind 0)) (crate-dep (name "either") (req "^1.5") (optional #t) (kind 0)) (crate-dep (name "iovec") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde_test") (req "^1.0") (default-features #t) (kind 2)))) (hash "0qma2kkfx9pyml60k2wkznkrflig4jg4xbscqdpj95dhak9mpr8c") (features (quote (("i128" "byteorder/i128"))))))

(define-public crate-bytes-0.4 (crate (name "bytes") (vers "0.4.11") (deps (list (crate-dep (name "byteorder") (req "^1.1.0") (default-features #t) (kind 0)) (crate-dep (name "either") (req "^1.5") (optional #t) (kind 0)) (crate-dep (name "iovec") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde_test") (req "^1.0") (default-features #t) (kind 2)))) (hash "1yksbhrhis04k5c4ibm40rzfqv8s8vn2m4dhbqscphh3fv9f7ba0") (features (quote (("i128" "byteorder/i128"))))))

(define-public crate-bytes-0.4 (crate (name "bytes") (vers "0.4.12") (deps (list (crate-dep (name "byteorder") (req "^1.1.0") (default-features #t) (kind 0)) (crate-dep (name "either") (req "^1.5") (optional #t) (kind 0)) (crate-dep (name "iovec") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde_test") (req "^1.0") (default-features #t) (kind 2)))) (hash "0768a55q2fsqdjsvcv98ndg9dq7w2g44dvq1avhwpxrdzbydyvr0") (features (quote (("i128" "byteorder/i128"))))))

(define-public crate-bytes-0.5 (crate (name "bytes") (vers "0.5.0") (deps (list (crate-dep (name "loom") (req "^0.2.10") (default-features #t) (kind 2)) (crate-dep (name "serde") (req "^1.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde_test") (req "^1.0") (default-features #t) (kind 2)))) (hash "04kcq8bl2vh27jvv2vbb0h008nxjqs2fvc7bv6zlhygspvwqr72l") (features (quote (("std") ("default" "std")))) (yanked #t)))

(define-public crate-bytes-0.5 (crate (name "bytes") (vers "0.5.1") (deps (list (crate-dep (name "loom") (req "^0.2.10") (default-features #t) (kind 2)) (crate-dep (name "serde") (req "^1.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde_test") (req "^1.0") (default-features #t) (kind 2)))) (hash "13qwifqmxicki2jdgv7xl3liqibzmr6scznqdmaa738s3mnmirs2") (features (quote (("std") ("default" "std")))) (yanked #t)))

(define-public crate-bytes-0.5 (crate (name "bytes") (vers "0.5.2") (deps (list (crate-dep (name "loom") (req "^0.2.10") (default-features #t) (kind 2)) (crate-dep (name "serde") (req "^1.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde_test") (req "^1.0") (default-features #t) (kind 2)))) (hash "15fq2gn9zk111dkzm1lj3704c6mpdbi6i3k70g3jckky2ngk318w") (features (quote (("std") ("default" "std")))) (yanked #t)))

(define-public crate-bytes-0.5 (crate (name "bytes") (vers "0.5.3") (deps (list (crate-dep (name "loom") (req "^0.2.10") (default-features #t) (kind 2)) (crate-dep (name "serde") (req "^1.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde_test") (req "^1.0") (default-features #t) (kind 2)))) (hash "0f2z23hm1fjz605bfsiggjdcyqnkms88282ag9ghacmkvqalq00h") (features (quote (("std") ("default" "std"))))))

(define-public crate-bytes-0.5 (crate (name "bytes") (vers "0.5.4") (deps (list (crate-dep (name "loom") (req "^0.2.13") (default-features #t) (target "cfg(not(windows))") (kind 2)) (crate-dep (name "serde") (req "^1.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde_test") (req "^1.0") (default-features #t) (kind 2)))) (hash "1q9r7si1l8vndg4n2ny2nv833ghp5vyqzk5indb9rmhd5ibaq2hk") (features (quote (("std") ("default" "std"))))))

(define-public crate-bytes-0.5 (crate (name "bytes") (vers "0.5.5") (deps (list (crate-dep (name "loom") (req "^0.3") (default-features #t) (target "cfg(loom)") (kind 0)) (crate-dep (name "serde") (req "^1.0.60") (features (quote ("alloc"))) (optional #t) (kind 0)) (crate-dep (name "serde_test") (req "^1.0") (default-features #t) (kind 2)))) (hash "0fr1kksxb6xyvm8daqygizv61z4c3rx2sjy3wcb0hzdrzcvg130i") (features (quote (("std") ("default" "std"))))))

(define-public crate-bytes-0.5 (crate (name "bytes") (vers "0.5.6") (deps (list (crate-dep (name "loom") (req "^0.3") (default-features #t) (target "cfg(loom)") (kind 2)) (crate-dep (name "serde") (req "^1.0.60") (features (quote ("alloc"))) (optional #t) (kind 0)) (crate-dep (name "serde_test") (req "^1.0") (default-features #t) (kind 2)))) (hash "0f5s7xq6qzmdh22ygsy8v0sp02m51y0radvq4i4y8cizy1lfqk0f") (features (quote (("std") ("default" "std"))))))

(define-public crate-bytes-0.6 (crate (name "bytes") (vers "0.6.0") (deps (list (crate-dep (name "loom") (req "^0.3") (default-features #t) (target "cfg(loom)") (kind 2)) (crate-dep (name "serde") (req "^1.0.60") (features (quote ("alloc"))) (optional #t) (kind 0)) (crate-dep (name "serde_test") (req "^1.0") (default-features #t) (kind 2)))) (hash "05ivrcbgl4f7z2zzm9hbsi8cy66spi70xlm6fp16zsq4ylsvrp70") (features (quote (("std") ("default" "std"))))))

(define-public crate-bytes-1 (crate (name "bytes") (vers "1.0.0") (deps (list (crate-dep (name "loom") (req "^0.4") (default-features #t) (target "cfg(loom)") (kind 2)) (crate-dep (name "serde") (req "^1.0.60") (features (quote ("alloc"))) (optional #t) (kind 0)) (crate-dep (name "serde_test") (req "^1.0") (default-features #t) (kind 2)))) (hash "0wpsy2jwmbrsn7x6vcd00hw9vvz071lv8nrb25wrspvmkna8w7xd") (features (quote (("std") ("default" "std"))))))

(define-public crate-bytes-1 (crate (name "bytes") (vers "1.0.1") (deps (list (crate-dep (name "loom") (req "^0.4") (default-features #t) (target "cfg(loom)") (kind 2)) (crate-dep (name "serde") (req "^1.0.60") (features (quote ("alloc"))) (optional #t) (kind 0)) (crate-dep (name "serde_test") (req "^1.0") (default-features #t) (kind 2)))) (hash "0h6h1c8g3yj2b4k8g25gr3246mq985y0kl3z685cs784fr1ww05p") (features (quote (("std") ("default" "std"))))))

(define-public crate-bytes-1 (crate (name "bytes") (vers "1.1.0") (deps (list (crate-dep (name "loom") (req "^0.5") (default-features #t) (target "cfg(loom)") (kind 2)) (crate-dep (name "serde") (req "^1.0.60") (features (quote ("alloc"))) (optional #t) (kind 0)) (crate-dep (name "serde_test") (req "^1.0") (default-features #t) (kind 2)))) (hash "1y70b249m02lfp0j6565b29kviapj4xsl9whamcqwddnp9kjv1y4") (features (quote (("std") ("default" "std"))))))

(define-public crate-bytes-1 (crate (name "bytes") (vers "1.2.0") (deps (list (crate-dep (name "loom") (req "^0.5") (default-features #t) (target "cfg(loom)") (kind 2)) (crate-dep (name "serde") (req "^1.0.60") (features (quote ("alloc"))) (optional #t) (kind 0)) (crate-dep (name "serde_test") (req "^1.0") (default-features #t) (kind 2)))) (hash "03hk3qsjkafww539rl01j0pxq7nrmcimfwd0crhf2rsy1i5dxczh") (features (quote (("std") ("default" "std"))))))

(define-public crate-bytes-1 (crate (name "bytes") (vers "1.2.1") (deps (list (crate-dep (name "loom") (req "^0.5") (default-features #t) (target "cfg(loom)") (kind 2)) (crate-dep (name "serde") (req "^1.0.60") (features (quote ("alloc"))) (optional #t) (kind 0)) (crate-dep (name "serde_test") (req "^1.0") (default-features #t) (kind 2)))) (hash "1nsni0jbx1048inbrarn3hz6zxd000pp0rac2mr07s7xf1m7p2pc") (features (quote (("std") ("default" "std"))))))

(define-public crate-bytes-1 (crate (name "bytes") (vers "1.3.0") (deps (list (crate-dep (name "loom") (req "^0.5") (default-features #t) (target "cfg(loom)") (kind 2)) (crate-dep (name "serde") (req "^1.0.60") (features (quote ("alloc"))) (optional #t) (kind 0)) (crate-dep (name "serde_test") (req "^1.0") (default-features #t) (kind 2)))) (hash "0g1s7b19wa2v5vqjif5d9al9gwxqnv310gv63cmaz88mdf34xcnz") (features (quote (("std") ("default" "std"))))))

(define-public crate-bytes-1 (crate (name "bytes") (vers "1.4.0") (deps (list (crate-dep (name "loom") (req "^0.5") (default-features #t) (target "cfg(loom)") (kind 2)) (crate-dep (name "serde") (req "^1.0.60") (features (quote ("alloc"))) (optional #t) (kind 0)) (crate-dep (name "serde_test") (req "^1.0") (default-features #t) (kind 2)))) (hash "1gkh3fk4fm9xv5znlib723h5md5sxsvbd5113sbxff6g1lmgvcl9") (features (quote (("std") ("default" "std"))))))

(define-public crate-bytes-1 (crate (name "bytes") (vers "1.5.0") (deps (list (crate-dep (name "loom") (req "^0.5") (default-features #t) (target "cfg(loom)") (kind 2)) (crate-dep (name "serde") (req "^1.0.60") (features (quote ("alloc"))) (optional #t) (kind 0)) (crate-dep (name "serde_test") (req "^1.0") (default-features #t) (kind 2)))) (hash "08w2i8ac912l8vlvkv3q51cd4gr09pwlg3sjsjffcizlrb0i5gd2") (features (quote (("std") ("default" "std"))))))

(define-public crate-bytes-1 (crate (name "bytes") (vers "1.6.0") (deps (list (crate-dep (name "loom") (req "^0.7") (default-features #t) (target "cfg(loom)") (kind 2)) (crate-dep (name "serde") (req "^1.0.60") (features (quote ("alloc"))) (optional #t) (kind 0)) (crate-dep (name "serde_test") (req "^1.0") (default-features #t) (kind 2)))) (hash "1jf2awc1fywpk15m6pxay3wqcg65ararg9xi4b08vnszwiyy2kai") (features (quote (("std") ("default" "std")))) (rust-version "1.39")))

(define-public crate-bytes-cast-0.1 (crate (name "bytes-cast") (vers "0.1.0") (deps (list (crate-dep (name "bytes-cast-derive") (req "^0.1") (default-features #t) (kind 0)))) (hash "0lna2qz1c5qkc13193wai4x61sdkdi4pis1i8cm2ijbv1hqbm5ii")))

(define-public crate-bytes-cast-0.2 (crate (name "bytes-cast") (vers "0.2.0") (deps (list (crate-dep (name "bytes-cast-derive") (req "^0.1") (default-features #t) (kind 0)))) (hash "0ln4yah8npnbcmd5chipnv4m5sl2dx5jg9zxrkkqgsfb9sd4yhqd")))

(define-public crate-bytes-cast-0.3 (crate (name "bytes-cast") (vers "0.3.0") (deps (list (crate-dep (name "bytes-cast-derive") (req "^0.2") (default-features #t) (kind 0)))) (hash "1s2jnfmnn0mbw1k42pvi9pzwbkha647964lywfh3qw6pj4xyj3d2")))

(define-public crate-bytes-cast-derive-0.1 (crate (name "bytes-cast-derive") (vers "0.1.0") (deps (list (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (default-features #t) (kind 0)))) (hash "1nqzv2q301yhjc11aphwrvj85m1hzyd553mmsrj6cirqvvwnm4yb")))

(define-public crate-bytes-cast-derive-0.1 (crate (name "bytes-cast-derive") (vers "0.1.1") (deps (list (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (default-features #t) (kind 0)))) (hash "1z08czckwcd3pwhrq9smfv04xflr42l2x2y9ini1n0lizj7hwgmi")))

(define-public crate-bytes-cast-derive-0.1 (crate (name "bytes-cast-derive") (vers "0.1.2") (deps (list (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (default-features #t) (kind 0)))) (hash "1mbdjp5j1clk5yh61227ymh3z4fr4k3d89sigriy6nn4qvilwl6c")))

(define-public crate-bytes-cast-derive-0.2 (crate (name "bytes-cast-derive") (vers "0.2.0") (deps (list (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (default-features #t) (kind 0)))) (hash "0wagrd5qi6sw4pcyqsvy4isqp5q723vqnxff5kbf7kaqrvyacw3l")))

(define-public crate-bytes-core-0.0.0 (crate (name "bytes-core") (vers "0.0.0") (hash "1rh7p7s2iy7h1x2p8byv1wygybap3m3rwlk8py5fdp0hl2x3d1ln")))

(define-public crate-bytes-expand-0.4 (crate (name "bytes-expand") (vers "0.4.12") (deps (list (crate-dep (name "byteorder") (req "^1.1.0") (default-features #t) (kind 0)) (crate-dep (name "either") (req "^1.5") (optional #t) (kind 0)) (crate-dep (name "iovec") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde_test") (req "^1.0") (default-features #t) (kind 2)))) (hash "17n951f63777z6pr5jk85aa3z82fg5db96g4msa643rxn1vnk6fv") (features (quote (("i128" "byteorder/i128"))))))

(define-public crate-bytes-kman-0.1 (crate (name "bytes-kman") (vers "0.1.0") (deps (list (crate-dep (name "derive") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "0s86zyfxgm0d1128wp9a2g6qlc2rjw556dxgh26q8v0nckjl31af") (features (quote (("std") ("default" "core" "std") ("core"))))))

(define-public crate-bytes-kman-0.1 (crate (name "bytes-kman") (vers "0.1.1") (deps (list (crate-dep (name "derive") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "19qfjymmljdnfc40c74h7q04v3assq37czy63gpp424l9ys85sqc") (features (quote (("std") ("default" "core" "std") ("core"))))))

(define-public crate-bytes-kman-0.1 (crate (name "bytes-kman") (vers "0.1.2") (deps (list (crate-dep (name "bytes-kman-derive") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "07p0vks6pn8dhjvri0q12q0d48kal2yd0ac0p4q32qandl3d7c66") (features (quote (("std") ("default" "core" "std") ("core"))))))

(define-public crate-bytes-kman-0.1 (crate (name "bytes-kman") (vers "0.1.3") (deps (list (crate-dep (name "bytes-kman-derive") (req "^0.1.1") (default-features #t) (kind 0)))) (hash "0irgbli0blfah42bizvsdfr8gwaywx1b3njzr35ca5cccm2dvqjq") (features (quote (("std") ("default" "core" "std") ("core"))))))

(define-public crate-bytes-kman-0.1 (crate (name "bytes-kman") (vers "0.1.4") (deps (list (crate-dep (name "bytes-kman-derive") (req "^0.1.1") (default-features #t) (kind 0)))) (hash "1n2hgqknqkp9wfn5s3ysm9iixwl80b20mdnhpxlcclrs7wdr7rn1") (features (quote (("std") ("default" "core" "std") ("core"))))))

(define-public crate-bytes-kman-0.1 (crate (name "bytes-kman") (vers "0.1.5") (deps (list (crate-dep (name "bytes-kman-derive") (req "^0.1.2") (default-features #t) (kind 0)))) (hash "17lzxmrddmdifisic3qkiyx23dxr949l8v4p9glr63isxc6fc5c4") (features (quote (("std") ("default" "core" "std") ("core"))))))

(define-public crate-bytes-kman-0.1 (crate (name "bytes-kman") (vers "0.1.6") (deps (list (crate-dep (name "bytes-kman-derive") (req "^0.1.2") (default-features #t) (kind 0)))) (hash "01sm7q97s51n14mfdqhzizv798aq7lj4skhizh71l61l359h4qsf") (features (quote (("std") ("default" "core" "std") ("core"))))))

(define-public crate-bytes-kman-0.1 (crate (name "bytes-kman") (vers "0.1.7") (deps (list (crate-dep (name "bytes-kman-derive") (req "^0.1.3") (default-features #t) (kind 0)))) (hash "05xkwb5v5w0vb1m9v7k6gz4aqxhmjg2hxwxi3a1qqx887qr17yll") (features (quote (("std") ("default" "core" "std") ("core"))))))

(define-public crate-bytes-kman-0.1 (crate (name "bytes-kman") (vers "0.1.8") (deps (list (crate-dep (name "bytes-kman-derive") (req "^0.1.3") (default-features #t) (kind 0)))) (hash "01mgxjz63kxamdd5znlh3vnqhx6ax4cj3yzvmkxs57rjhvqnca8x") (features (quote (("std") ("default" "core" "std") ("core"))))))

(define-public crate-bytes-kman-0.1 (crate (name "bytes-kman") (vers "0.1.9") (deps (list (crate-dep (name "bytes-kman-derive") (req "^0.1.3") (default-features #t) (kind 0)))) (hash "0cswsxzqmvjr8a62kqs2b8mvf605pl5dzij72sf3636pk0g1731b") (features (quote (("std") ("default" "core" "std") ("core"))))))

(define-public crate-bytes-kman-0.1 (crate (name "bytes-kman") (vers "0.1.10") (deps (list (crate-dep (name "bytes-kman-derive") (req "^0.1.3") (default-features #t) (kind 0)))) (hash "0ihwqpw1y44i2xh8cv6wli1wjd13zckls82vfbxazcixyk0lvl0i") (features (quote (("std") ("default" "core" "std") ("core"))))))

(define-public crate-bytes-kman-0.1 (crate (name "bytes-kman") (vers "0.1.11") (deps (list (crate-dep (name "bytes-kman-derive") (req "^0.1.4") (default-features #t) (kind 0)))) (hash "105kszj4zrsphvdpzcmk46maw2rx7k4cq4vc60ci3qp5n9m7qxr7") (features (quote (("std") ("default" "core" "std") ("core"))))))

(define-public crate-bytes-kman-0.1 (crate (name "bytes-kman") (vers "0.1.12") (deps (list (crate-dep (name "bytes-kman-derive") (req "^0.1.5") (default-features #t) (kind 0)))) (hash "1zhy93yly7ikrc5iwxqp4dg4mgkhfwdjlqa61fs7zrma5g3yc0ia") (features (quote (("std") ("default" "core" "std") ("core"))))))

(define-public crate-bytes-kman-0.1 (crate (name "bytes-kman") (vers "0.1.13") (deps (list (crate-dep (name "bytes-kman-derive") (req "^0.1.5") (default-features #t) (kind 0)))) (hash "110j74y46m752j6g1dz59svvxpfya7pm512vimb1h2ggb11pk8bf") (features (quote (("std") ("default" "core" "std") ("core"))))))

(define-public crate-bytes-kman-0.1 (crate (name "bytes-kman") (vers "0.1.14") (deps (list (crate-dep (name "bytes-kman-derive") (req "^0.1.5") (default-features #t) (kind 0)))) (hash "0wrx6mz433hywjnqf76diwz5ymd0v2w390248rbqaz8qbprqsjhk") (features (quote (("std") ("default" "core" "std") ("core"))))))

(define-public crate-bytes-kman-0.1 (crate (name "bytes-kman") (vers "0.1.15") (deps (list (crate-dep (name "bytes-kman-derive") (req "^0.1.5") (default-features #t) (kind 0)))) (hash "0i6s2m566fwrs96lyb0paa0piv97cwhrqmx56qxj1x3rzixpw824") (features (quote (("std") ("default" "core" "std") ("core"))))))

(define-public crate-bytes-kman-0.1 (crate (name "bytes-kman") (vers "0.1.16") (deps (list (crate-dep (name "bytes-kman-derive") (req "^0.1.5") (default-features #t) (kind 0)))) (hash "0l6qm9i50x6hx2diqvxjw0v7ydlmiqnkka926jsnscg29zy3c2wd") (features (quote (("std") ("default" "core" "std") ("core"))))))

(define-public crate-bytes-kman-0.1 (crate (name "bytes-kman") (vers "0.1.17") (deps (list (crate-dep (name "bytes-kman-derive") (req "^0.1.5") (default-features #t) (kind 0)))) (hash "1sg8qk2sm83i2bn91zdpf2cfyiks82xvjkqznbawpi2p7zmczv2k") (features (quote (("std") ("default" "core" "std") ("core"))))))

(define-public crate-bytes-kman-0.1 (crate (name "bytes-kman") (vers "0.1.18") (deps (list (crate-dep (name "bytes-kman-derive") (req "^0.1.5") (default-features #t) (kind 0)))) (hash "182hf9hc1y2b05a3qpf830i4f80b03lz88hcb62s89zvahqnmpzn") (features (quote (("std") ("default" "core" "std") ("core"))))))

(define-public crate-bytes-kman-0.1 (crate (name "bytes-kman") (vers "0.1.19") (deps (list (crate-dep (name "bytes-kman-derive") (req "^0.1.5") (default-features #t) (kind 0)))) (hash "1lfla3k9p48kfndz4s69f2c68jhg4hhxykaqzd0kkpwd2l3y37lc") (features (quote (("std") ("default" "core" "std") ("core"))))))

(define-public crate-bytes-kman-0.1 (crate (name "bytes-kman") (vers "0.1.20") (deps (list (crate-dep (name "bytes-kman-derive") (req "^0.1.5") (default-features #t) (kind 0)))) (hash "02a2458vwymh39ckq0hipiac9j158hi37n0b7x29rjbmb7lxqb3c") (features (quote (("std") ("default" "core" "std") ("core"))))))

(define-public crate-bytes-kman-0.2 (crate (name "bytes-kman") (vers "0.2.0") (deps (list (crate-dep (name "bytes-kman-derive") (req "^0.2.0") (default-features #t) (kind 0)))) (hash "15qnlhq218mhqhifvfp9sg8dvgq4xhq5dzzbvln4l24r79l8h4f9") (features (quote (("std") ("default" "core" "std") ("core"))))))

(define-public crate-bytes-kman-0.3 (crate (name "bytes-kman") (vers "0.3.0") (deps (list (crate-dep (name "bytes-kman-derive") (req "^0.3.0") (default-features #t) (kind 0)))) (hash "08sg3ir0lsyl1xx7lqy64mwi0gl8nnnpvvxkvaxjhl3hcfaji2kk") (features (quote (("std") ("default" "core" "std") ("core"))))))

(define-public crate-bytes-kman-derive-0.1 (crate (name "bytes-kman-derive") (vers "0.1.0") (deps (list (crate-dep (name "proc-macro2") (req "^1.0.47") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.21") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.105") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "05iv30b4z1qpmwbyc14bk96p4zwf9lzxrnfi9kb0r1df80hn6gia")))

(define-public crate-bytes-kman-derive-0.1 (crate (name "bytes-kman-derive") (vers "0.1.1") (deps (list (crate-dep (name "proc-macro2") (req "^1.0.47") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.21") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.105") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "1pg0vc3mvcyxsq22z21rypi6xh45vi8izqhfv4k0qzfi4wpkvfpg")))

(define-public crate-bytes-kman-derive-0.1 (crate (name "bytes-kman-derive") (vers "0.1.2") (deps (list (crate-dep (name "proc-macro2") (req "^1.0.47") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.21") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.105") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "1lslj8xrpcsrj1nq66mciyp557hy1f97c6883z75gisaxjv5rs5z")))

(define-public crate-bytes-kman-derive-0.1 (crate (name "bytes-kman-derive") (vers "0.1.3") (deps (list (crate-dep (name "proc-macro2") (req "^1.0.47") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.21") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.105") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "0455bcl1blzdc4y3s7kg1ridixrk8xnknhj6b9y58dll6sf62w6v")))

(define-public crate-bytes-kman-derive-0.1 (crate (name "bytes-kman-derive") (vers "0.1.4") (deps (list (crate-dep (name "proc-macro2") (req "^1.0.47") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.21") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.105") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "1m772bi9wzzbcc7cl2a16gl6s2y7a2gwx9chlrqygb0mwihgki24")))

(define-public crate-bytes-kman-derive-0.1 (crate (name "bytes-kman-derive") (vers "0.1.5") (deps (list (crate-dep (name "proc-macro2") (req "^1.0.47") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.21") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.105") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "17pmnhmyzv1q7dxb1bkqkrn97lf3izjjxn9gmq92v2fjx0swyg03")))

(define-public crate-bytes-kman-derive-0.2 (crate (name "bytes-kman-derive") (vers "0.2.0") (deps (list (crate-dep (name "proc-macro2") (req "^1.0.47") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.21") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.105") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "1vn1gdxi02skq4680qp38r851rjp72wd9nxj8jdqa7kkrbigd3m8")))

(define-public crate-bytes-kman-derive-0.3 (crate (name "bytes-kman-derive") (vers "0.3.0") (deps (list (crate-dep (name "proc-macro2") (req "^1.0.47") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.21") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.105") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "1ql8lrywp2is99hgvqd59appzn2kil992bm3daa6d6wc8l2pwd1q")))

(define-public crate-bytes-lit-0.0.1 (crate (name "bytes-lit") (vers "0.0.1") (deps (list (crate-dep (name "num-bigint") (req "^0.4.3") (default-features #t) (kind 0)) (crate-dep (name "pretty_assertions") (req "^1.2.1") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (features (quote ("full" "extra-traits"))) (default-features #t) (kind 0)))) (hash "0fn2qvlkayvpsfiaczri5xx0wxvvyz09p6sv379h2ap5ajgly3rb")))

(define-public crate-bytes-lit-0.0.2 (crate (name "bytes-lit") (vers "0.0.2") (deps (list (crate-dep (name "num-bigint") (req "^0.4.3") (default-features #t) (kind 0)) (crate-dep (name "pretty_assertions") (req "^1.2.1") (default-features #t) (kind 2)) (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (features (quote ("full" "extra-traits"))) (default-features #t) (kind 0)))) (hash "1jy3myhx86anxsqawg403mhhz5xi03g4sw57gs889ylq3ql1wrbm")))

(define-public crate-bytes-lit-0.0.3 (crate (name "bytes-lit") (vers "0.0.3") (deps (list (crate-dep (name "num-bigint") (req "^0.4.3") (default-features #t) (kind 0)) (crate-dep (name "pretty_assertions") (req "^1.2.1") (default-features #t) (kind 2)) (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (features (quote ("full" "extra-traits"))) (default-features #t) (kind 0)))) (hash "1xijpvjwp994hhda7jgb7kajdd61va088k0zm4llrn6imfw4kagk") (rust-version "1.63")))

(define-public crate-bytes-lit-0.0.4 (crate (name "bytes-lit") (vers "0.0.4") (deps (list (crate-dep (name "num-bigint") (req "^0.4.3") (default-features #t) (kind 0)) (crate-dep (name "pretty_assertions") (req "^1.2.1") (default-features #t) (kind 2)) (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (features (quote ("full" "extra-traits"))) (default-features #t) (kind 0)))) (hash "0l229dbms1d187c91vcf14w726z19yjzjxgsj25k8j8s2v51n60w") (rust-version "1.63")))

(define-public crate-bytes-lit-0.0.5 (crate (name "bytes-lit") (vers "0.0.5") (deps (list (crate-dep (name "num-bigint") (req "^0.4.3") (default-features #t) (kind 0)) (crate-dep (name "pretty_assertions") (req "^1.2.1") (default-features #t) (kind 2)) (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2.0") (features (quote ("full" "extra-traits"))) (default-features #t) (kind 0)))) (hash "0p82m9q2g56vqwjg71fz7zmra32kn75spzimcgj7clhs44vvznha") (rust-version "1.66")))

(define-public crate-bytes-literal-0.1 (crate (name "bytes-literal") (vers "0.1.0") (deps (list (crate-dep (name "bytes") (req "^1") (kind 0)) (crate-dep (name "hex-literal") (req "^0.3") (kind 0)))) (hash "0rw1x2cqr6chpd16w8kzbjy4vv4fvjdi966xkccjgwgry77jgvpp")))

(define-public crate-bytes-quilt-0.1 (crate (name "bytes-quilt") (vers "0.1.0") (deps (list (crate-dep (name "bytes") (req "^1.3.0") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.38") (default-features #t) (kind 0)))) (hash "06zxcryvvlv7mqgwzx99xjwmrr1a4iaf890j5bphikg9iq6jx7mf")))

(define-public crate-bytes-ringbuffer-0.0.0 (crate (name "bytes-ringbuffer") (vers "0.0.0") (hash "1nmaa3pl424hby4khv4l03mxc9h7nnw4sljqz630wihadc6xp2x5")))

(define-public crate-bytes-ringbuffer-0.1 (crate (name "bytes-ringbuffer") (vers "0.1.0") (deps (list (crate-dep (name "bytes") (req "^0.5") (default-features #t) (kind 0)))) (hash "12ddspbv886lx8k8v78i14jmkv31397cxj2b09q9rpxlg7gcspkd")))

(define-public crate-bytes-stream-0.0.1 (crate (name "bytes-stream") (vers "0.0.1") (deps (list (crate-dep (name "bytes") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "futures") (req "^0.3.28") (default-features #t) (kind 2)) (crate-dep (name "futures-core") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "pin-project-lite") (req "^0.2") (default-features #t) (kind 0)))) (hash "05ibb6vd2fbdnl3062sw2lwmsm58d7h08wkhk84kjxipah6wsl9j")))

(define-public crate-bytes-stream-0.0.2 (crate (name "bytes-stream") (vers "0.0.2") (deps (list (crate-dep (name "bytes") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "futures") (req "^0.3.28") (default-features #t) (kind 2)) (crate-dep (name "futures-core") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "pin-project-lite") (req "^0.2") (default-features #t) (kind 0)))) (hash "17x1nd7s5b8i3wwi04hlr1iad3cx3g8swi6wzhcd4ifgqmns80j9")))

(define-public crate-bytes-stream-0.0.3 (crate (name "bytes-stream") (vers "0.0.3") (deps (list (crate-dep (name "bytes") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "futures") (req "^0.3.28") (default-features #t) (kind 2)) (crate-dep (name "futures-core") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "futures-test") (req "^0.3.28") (default-features #t) (kind 2)) (crate-dep (name "pin-project-lite") (req "^0.2") (default-features #t) (kind 0)))) (hash "0fhzrx8zjsdj1naacjrbq2slna6m4rcjm1d4prln2hhbaamd4x3c")))

(define-public crate-bytes-text-0.1 (crate (name "bytes-text") (vers "0.1.0") (deps (list (crate-dep (name "bytes") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "soft_assert") (req "^0.1") (default-features #t) (kind 0)))) (hash "02sblqjs2k03vybdfv2rbd8r5dm922p42icdhap1p2vjhgms2n4f")))

(define-public crate-bytes-utils-0.1 (crate (name "bytes-utils") (vers "0.1.0") (deps (list (crate-dep (name "bytes") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "either") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "itertools") (req "^0.10") (default-features #t) (kind 2)) (crate-dep (name "proptest") (req "^0.10") (default-features #t) (kind 2)))) (hash "1qhds8awnbknwsbzgw4h5r0yzc4gifyaaj22giczbbz0l6faibsh")))

(define-public crate-bytes-utils-0.1 (crate (name "bytes-utils") (vers "0.1.1") (deps (list (crate-dep (name "bytes") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "either") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "itertools") (req "^0.10") (default-features #t) (kind 2)) (crate-dep (name "proptest") (req "^0.10") (default-features #t) (kind 2)))) (hash "0m3fvz4pcxwrnf7pzppalmkwdb999534d910b4z14hqwjl94fcaf")))

(define-public crate-bytes-utils-0.1 (crate (name "bytes-utils") (vers "0.1.2") (deps (list (crate-dep (name "bytes") (req "^1") (kind 0)) (crate-dep (name "either") (req "^1") (kind 0)) (crate-dep (name "itertools") (req "^0.10") (default-features #t) (kind 2)) (crate-dep (name "proptest") (req "^0.10") (default-features #t) (kind 2)))) (hash "1wcf1lsblzkxdi22xm7i56iingkifwg7i4kajvjgv3mckkps6d0r") (features (quote (("std" "bytes/default" "either/default") ("default" "std"))))))

(define-public crate-bytes-utils-0.1 (crate (name "bytes-utils") (vers "0.1.3") (deps (list (crate-dep (name "bytes") (req "^1") (kind 0)) (crate-dep (name "either") (req "^1") (kind 0)) (crate-dep (name "itertools") (req "^0.10") (default-features #t) (kind 2)) (crate-dep (name "proptest") (req "^0.10") (default-features #t) (kind 2)) (crate-dep (name "serde") (req "^1") (optional #t) (kind 0)) (crate-dep (name "serde_test") (req "^1.0.144") (default-features #t) (kind 2)))) (hash "1ffllwr7kglgmn8qhbxpbd5vmczdj9cm603lsang70z2fs03lzg4") (features (quote (("std" "bytes/default" "either/default") ("default" "std")))) (v 2) (features2 (quote (("serde" "dep:serde" "bytes/serde"))))))

(define-public crate-bytes-utils-0.1 (crate (name "bytes-utils") (vers "0.1.4") (deps (list (crate-dep (name "bytes") (req "^1") (kind 0)) (crate-dep (name "either") (req "^1") (kind 0)) (crate-dep (name "itertools") (req "^0.12") (default-features #t) (kind 2)) (crate-dep (name "proptest") (req "^1.0") (default-features #t) (kind 2)) (crate-dep (name "serde") (req "^1") (optional #t) (kind 0)) (crate-dep (name "serde_test") (req "^1.0.144") (default-features #t) (kind 2)))) (hash "0dcd0lxfpj367j9nwm7izj4mkib3slg61rg4wqmpw0kvfnlf7bvx") (features (quote (("std" "bytes/default") ("default" "std")))) (v 2) (features2 (quote (("serde" "dep:serde" "bytes/serde"))))))

(define-public crate-bytes-varint-1 (crate (name "bytes-varint") (vers "1.0.0") (deps (list (crate-dep (name "bytes") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "rstest") (req "^0") (default-features #t) (kind 2)))) (hash "1cs13649s40ga6wyvns9vk51y2k9hvg9k7rxzap85b9zzl8ffsz5")))

(define-public crate-bytes-varint-1 (crate (name "bytes-varint") (vers "1.0.1") (deps (list (crate-dep (name "bytes") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "rstest") (req "^0") (default-features #t) (kind 2)))) (hash "15kg6750svzpp1dnim0dqa9apgqh9gz3hnk5x6791s2ckbxkrrr5")))

(define-public crate-bytes-varint-1 (crate (name "bytes-varint") (vers "1.0.2") (deps (list (crate-dep (name "bytes") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "rstest") (req "^0") (default-features #t) (kind 2)))) (hash "1qzkprvp96mfhmljq74mdq9r8a7v7qdqqlgh7w1jnxlgkip9a924")))

(define-public crate-bytes-varint-1 (crate (name "bytes-varint") (vers "1.0.3") (deps (list (crate-dep (name "bytes") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "rstest") (req "^0") (default-features #t) (kind 2)))) (hash "0brkr2n9cpb99x7fbbcsjrcml42l8ihf2hviqhk9ssrngh685hal")))

(define-public crate-bytes32-0.1 (crate (name "bytes32") (vers "0.1.0") (hash "17p32wwf73f4r1brkpsxgi23k7r4sqzyzpwimzqgf4jhixk9laln")))

(define-public crate-bytes32-0.1 (crate (name "bytes32") (vers "0.1.1") (hash "1d4frkvg558lxyjhz4ahrmcncwykbzjil3ha35nvxy7gqzvp3z90")))

(define-public crate-bytes32-0.1 (crate (name "bytes32") (vers "0.1.2") (hash "1ha927q9hfn5x2a560mrr54snvczihycv9v2x536nwz851mwhzm2")))

(define-public crate-bytes32-0.1 (crate (name "bytes32") (vers "0.1.3") (hash "1knq6scinp9zkppx3vlzij3483gqzk8d917vmkj6qi58wfx9vic8")))

(define-public crate-bytes32-0.1 (crate (name "bytes32") (vers "0.1.4-0") (hash "12g3n8ifk1v6vhb5h502px2w01yhhxcxq8836y3bxd0hx73lnycq")))

(define-public crate-bytes_chain-1 (crate (name "bytes_chain") (vers "1.1.7") (deps (list (crate-dep (name "bytes") (req "^0.4") (default-features #t) (kind 0)))) (hash "1sj461vsi5s6pgfl7zdkvbygmklrp69z3m4f077wf8af9k14da7m")))

(define-public crate-bytes_chain-1 (crate (name "bytes_chain") (vers "1.1.8") (deps (list (crate-dep (name "bytes") (req "^0.4") (default-features #t) (kind 0)))) (hash "0k4dnqsn8sihb45s3hxlw8cad151lryss1qbi1s0a0bddrmnm2vx")))

(define-public crate-bytes_convert-1 (crate (name "bytes_convert") (vers "1.0.0") (deps (list (crate-dep (name "bytes") (req "^1.4") (default-features #t) (kind 0)))) (hash "07sd3898kkfsxclc2f8njq506sjs3s6kk40yblyck31x0103zcbh")))

(define-public crate-bytes_iter-0.1 (crate (name "bytes_iter") (vers "0.1.0") (hash "0glqyiqic02rblss1641wg8hkf5pzxf17xp204gay7v77kfhb1v9")))

(define-public crate-bytes_iter-0.1 (crate (name "bytes_iter") (vers "0.1.1") (hash "0y5pnrqjaaibbzhnzdbq7qxxryh7s7ri3qkc64jm13lhy0kfhznn")))

(define-public crate-bytes_parser-0.1 (crate (name "bytes_parser") (vers "0.1.0") (deps (list (crate-dep (name "thiserror") (req "^1.0.38") (default-features #t) (kind 0)))) (hash "0i8idh5gzi7d82ih46fbgk5w9752sclsbh3pz9b9is3fhfvd6z0r")))

(define-public crate-bytes_parser-0.1 (crate (name "bytes_parser") (vers "0.1.1") (deps (list (crate-dep (name "thiserror") (req "^1.0.38") (default-features #t) (kind 0)))) (hash "19s19mn2fhv7cfd65v2pxa4wwcsmqr9dinqzqc14n96lhbdkqivq")))

(define-public crate-bytes_parser-0.1 (crate (name "bytes_parser") (vers "0.1.2") (deps (list (crate-dep (name "thiserror") (req "^1.0.38") (default-features #t) (kind 0)))) (hash "1wff443q7gvdl9dif09lcxkqxfxl7ff88w5rqb04mf6sf6a9lwq2")))

(define-public crate-bytes_parser-0.1 (crate (name "bytes_parser") (vers "0.1.3") (deps (list (crate-dep (name "thiserror") (req "^1.0.38") (default-features #t) (kind 0)))) (hash "0hrj9wfm6y2f4sk3i020gc8c5an04v8j71973nsjld2x91x2crak")))

(define-public crate-bytes_parser-0.1 (crate (name "bytes_parser") (vers "0.1.4") (deps (list (crate-dep (name "thiserror") (req "^1.0.39") (default-features #t) (kind 0)))) (hash "0m8lg850yn68f8plm6vs62ccq2nqwd0x9iz65picbv8d14m7amnj")))

(define-public crate-bytes_parser-0.1 (crate (name "bytes_parser") (vers "0.1.5") (deps (list (crate-dep (name "thiserror") (req "^1.0.61") (default-features #t) (kind 0)))) (hash "0qcxzca5v17a01xbkwrgqrjhrwwp02x0vyfva3k40vayw7rlcyp3")))

(define-public crate-bytes_size-0.1 (crate (name "bytes_size") (vers "0.1.0") (hash "1zyivhj24hlwna8kibwfad1mknx38av2r6hkvaphw6p4ra39nr1v")))

(define-public crate-bytes_size-0.1 (crate (name "bytes_size") (vers "0.1.1") (hash "09rh0s15nvbsrzjjzkrngmsarvg65qna3hx18yf8fggdyajn38j3")))

(define-public crate-bytes_size-0.1 (crate (name "bytes_size") (vers "0.1.1-fix") (hash "07vlmw0bjss8h40ry5ga7vbg7rfv8bmv01iawk5nxj9dzyyip35w") (yanked #t)))

(define-public crate-bytes_size-0.1 (crate (name "bytes_size") (vers "0.1.2") (hash "15jv0l3cf4filyb4jrrcm6igynkwkjbim2bg8c12ia83sbcjbdqq")))

(define-public crate-bytes_to_type-0.1 (crate (name "bytes_to_type") (vers "0.1.0") (deps (list (crate-dep (name "anyhow") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "paste") (req "^1.0") (default-features #t) (kind 0)))) (hash "0g954hp5p3nq89m5f8m2zh0z5zi721ks5kbxj2834rlibsmbydfa")))

(define-public crate-bytes_to_type-0.1 (crate (name "bytes_to_type") (vers "0.1.1") (deps (list (crate-dep (name "anyhow") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "paste") (req "^1.0") (default-features #t) (kind 0)))) (hash "02ggjdfqac32kryyy89rns0bf7ifr3rgxl5ry4s6l8wmzqxq76pd")))

(define-public crate-bytesagent-0.1 (crate (name "bytesagent") (vers "0.1.0") (deps (list (crate-dep (name "glam") (req "^0.20.2") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "half") (req "^1.8.2") (optional #t) (default-features #t) (kind 0)))) (hash "1i3pzk88vhm501dbmnmhqbq9nf9x1ipgsf3nd90ap0mwz33lxjhj")))

(define-public crate-bytesagent-0.2 (crate (name "bytesagent") (vers "0.2.0") (deps (list (crate-dep (name "glam") (req "^0.20.2") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "half") (req "^1.8.2") (optional #t) (default-features #t) (kind 0)))) (hash "0brwrsqm04vs9ryl643v2sfwk351mb8l6jja6fzgy9bw1a0mrcry")))

(define-public crate-bytesagent-0.3 (crate (name "bytesagent") (vers "0.3.0") (deps (list (crate-dep (name "glam") (req "^0.20.2") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "half") (req "^1.8.2") (optional #t) (default-features #t) (kind 0)))) (hash "1d76y1jw5carc5gfv9wj6793claadvkmpvzwf9ixca7jdvqzc9bp")))

(define-public crate-bytesagent-0.4 (crate (name "bytesagent") (vers "0.4.0") (deps (list (crate-dep (name "glam") (req "^0.21.3") (optional #t) (kind 0)) (crate-dep (name "half") (req "^2.1.0") (optional #t) (kind 0)))) (hash "0cw853q9699r1nkyqds7j5pgmivqxz6k5865x3yrvfg1r0a924pq") (features (quote (("default" "std")))) (v 2) (features2 (quote (("std" "glam?/std" "half?/std"))))))

(define-public crate-byteseeker-0.0.0 (crate (name "byteseeker") (vers "0.0.0") (hash "0mb3yb9w7p25zzcfj3nqrxm3ci6wx304mqgl6vmsfmrqkm2jhlsf") (yanked #t)))

(define-public crate-byteseeker-0.1 (crate (name "byteseeker") (vers "0.1.0") (hash "10hap77ksxwskn8lsgwb718c2g441dk2iqa0qfhv68wgzqvgvg70")))

(define-public crate-byteseeker-0.1 (crate (name "byteseeker") (vers "0.1.1") (hash "1lvm4ynskwni8cblmnalzisxb0rc5zapw3bng9l3p8m43h9wq9nm")))

(define-public crate-byteseeker-0.1 (crate (name "byteseeker") (vers "0.1.2") (hash "090w7z876agg340nvbr3r0ncan70a51rhcv8crznfya1ysfl72k6")))

(define-public crate-byteseeker-0.2 (crate (name "byteseeker") (vers "0.2.0") (hash "0gfg7b94ykvisi9v37dr4q3hfq05w4hv4jhyph6avsvxcblilifc")))

(define-public crate-byteseeker-0.2 (crate (name "byteseeker") (vers "0.2.1") (hash "1xii6gaj4dibgxzmn0yvhcp7rbf4gzmlp9nhk5mcrrakknqzq4ag")))

(define-public crate-byteserde-0.1 (crate (name "byteserde") (vers "0.1.1") (deps (list (crate-dep (name "env_logger") (req "^0.10") (default-features #t) (kind 2)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 2)) (crate-dep (name "text-diff") (req "^0.4") (default-features #t) (kind 2)))) (hash "1lkfmydsp0q8s8fpjgd8lfmkcj9ms5iq21lrglv0hibqyghg9if5") (yanked #t) (rust-version "1.69")))

(define-public crate-byteserde-0.2 (crate (name "byteserde") (vers "0.2.0") (deps (list (crate-dep (name "env_logger") (req "^0.10") (default-features #t) (kind 2)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 2)) (crate-dep (name "text-diff") (req "^0.4") (default-features #t) (kind 2)))) (hash "0a48cbb9qz8n95jqksbs3lsy88wnihdcwzd91hgxps77spmlwqni") (yanked #t) (rust-version "1.69")))

(define-public crate-byteserde-0.3 (crate (name "byteserde") (vers "0.3.0") (deps (list (crate-dep (name "bytes") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "env_logger") (req "^0.10") (default-features #t) (kind 2)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 2)) (crate-dep (name "text-diff") (req "^0.4") (default-features #t) (kind 2)))) (hash "1hs96zhxdvxah0lahydl26i7k31g3wn78wvnfly6r2pbhs4y5nwi") (yanked #t) (rust-version "1.69")))

(define-public crate-byteserde-0.4 (crate (name "byteserde") (vers "0.4.0") (deps (list (crate-dep (name "bytes") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "env_logger") (req "^0.10") (default-features #t) (kind 2)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 2)) (crate-dep (name "text-diff") (req "^0.4") (default-features #t) (kind 2)))) (hash "0r9rac89326dqp5ffl494lhfyaaj4sfm6bkdcx77m0d985mjjsfp") (yanked #t) (rust-version "1.69")))

(define-public crate-byteserde-0.5 (crate (name "byteserde") (vers "0.5.0") (deps (list (crate-dep (name "bytes") (req "^1.4") (default-features #t) (kind 0)) (crate-dep (name "env_logger") (req "^0.10") (default-features #t) (kind 2)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 2)) (crate-dep (name "text-diff") (req "^0.4") (default-features #t) (kind 2)))) (hash "0kjln0pwk6r6qnnspb0jrhxiynpn9vng21pjg13h5ynspdidid9n") (yanked #t) (rust-version "1.69")))

(define-public crate-byteserde-0.6 (crate (name "byteserde") (vers "0.6.0") (deps (list (crate-dep (name "bytes") (req "^1.4") (default-features #t) (kind 0)) (crate-dep (name "env_logger") (req "^0.10") (default-features #t) (kind 2)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 2)) (crate-dep (name "text-diff") (req "^0.4") (default-features #t) (kind 2)))) (hash "03adv3rdf658pa3yw763khwpw5bqn8grfqjsgdmhyhyvqk0ignhd") (yanked #t) (rust-version "1.69")))

(define-public crate-byteserde-0.6 (crate (name "byteserde") (vers "0.6.1") (deps (list (crate-dep (name "bytes") (req "^1.4") (default-features #t) (kind 0)) (crate-dep (name "env_logger") (req "^0.10") (default-features #t) (kind 2)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 2)) (crate-dep (name "text-diff") (req "^0.4") (default-features #t) (kind 2)))) (hash "0h8c8sl5n8y08w19fn15zk3cs94g4w6vrgyh5y1f07par2s4zhqb") (yanked #t) (rust-version "1.69")))

(define-public crate-byteserde-0.6 (crate (name "byteserde") (vers "0.6.2") (deps (list (crate-dep (name "bytes") (req "^1.4") (default-features #t) (kind 0)) (crate-dep (name "env_logger") (req "^0.10") (default-features #t) (kind 2)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 2)) (crate-dep (name "text-diff") (req "^0.4") (default-features #t) (kind 2)))) (hash "0666fkic7z1434arnlhkgg4qvg7x3n4kykvqb63b2dd8fnsd6kda") (rust-version "1.69")))

(define-public crate-byteserde_derive-0.1 (crate (name "byteserde_derive") (vers "0.1.0") (deps (list (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2.0") (features (quote ("full"))) (default-features #t) (kind 0)) (crate-dep (name "trybuild") (req "^1.0") (features (quote ("diff"))) (default-features #t) (kind 2)))) (hash "1fbj28bpm8r91rvhh71lldc2159ca8vcjazldl972mxmq3jakq4v") (yanked #t) (rust-version "1.69")))

(define-public crate-byteserde_derive-0.1 (crate (name "byteserde_derive") (vers "0.1.1") (deps (list (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2.0") (features (quote ("full"))) (default-features #t) (kind 0)) (crate-dep (name "trybuild") (req "^1.0") (features (quote ("diff"))) (default-features #t) (kind 2)))) (hash "06y9hnyr85sj3rma1fa5yjf74bs88ii9a5yag1b5sirc3g45m12c") (yanked #t) (rust-version "1.69")))

(define-public crate-byteserde_derive-0.2 (crate (name "byteserde_derive") (vers "0.2.0") (deps (list (crate-dep (name "env_logger") (req "^0.10") (default-features #t) (kind 2)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 2)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2.0") (features (quote ("full" "extra-traits"))) (default-features #t) (kind 0)) (crate-dep (name "trybuild") (req "^1.0") (features (quote ("diff"))) (default-features #t) (kind 2)))) (hash "1lg2zin8dzbyjz6x7vpjph3zakim5a0fg5s86qmlmp6kw5rhdlzc") (yanked #t) (rust-version "1.69")))

(define-public crate-byteserde_derive-0.3 (crate (name "byteserde_derive") (vers "0.3.0") (deps (list (crate-dep (name "env_logger") (req "^0.10") (default-features #t) (kind 2)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 2)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2.0") (features (quote ("full" "extra-traits"))) (default-features #t) (kind 0)) (crate-dep (name "trybuild") (req "^1.0") (features (quote ("diff"))) (default-features #t) (kind 2)))) (hash "1hkkvs5hzvxxg73jn7s87hrxs8dwy6c1fxwpxd363j9m1zbsd6df") (yanked #t) (rust-version "1.69")))

(define-public crate-byteserde_derive-0.4 (crate (name "byteserde_derive") (vers "0.4.0") (deps (list (crate-dep (name "env_logger") (req "^0.10") (default-features #t) (kind 2)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 2)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2.0") (features (quote ("full" "extra-traits"))) (default-features #t) (kind 0)) (crate-dep (name "trybuild") (req "^1.0") (features (quote ("diff"))) (default-features #t) (kind 2)))) (hash "08zd14529iqh2b0gjzsbxxh10a8cbbifaskcqh165p3k1y90nnl8") (yanked #t) (rust-version "1.69")))

(define-public crate-byteserde_derive-0.5 (crate (name "byteserde_derive") (vers "0.5.0") (deps (list (crate-dep (name "env_logger") (req "^0.10") (default-features #t) (kind 2)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 2)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2.0") (features (quote ("full" "extra-traits"))) (default-features #t) (kind 0)) (crate-dep (name "trybuild") (req "^1.0") (features (quote ("diff"))) (default-features #t) (kind 2)))) (hash "1569bmib5idkqwdhlsf6qdnml9vbb4whjpw89ahrprb4sk3f05fz") (yanked #t) (rust-version "1.69")))

(define-public crate-byteserde_derive-0.6 (crate (name "byteserde_derive") (vers "0.6.0") (deps (list (crate-dep (name "byteserde") (req "^0.6.0") (default-features #t) (kind 2)) (crate-dep (name "env_logger") (req "^0.10") (default-features #t) (kind 2)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 2)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2.0") (features (quote ("full" "extra-traits"))) (default-features #t) (kind 0)) (crate-dep (name "trybuild") (req "^1.0") (features (quote ("diff"))) (default-features #t) (kind 2)))) (hash "02qqmr96is26ax9v3gds15a1q911l9z8cz553svn2mw18yhpfpfk") (yanked #t) (rust-version "1.69")))

(define-public crate-byteserde_derive-0.6 (crate (name "byteserde_derive") (vers "0.6.1") (deps (list (crate-dep (name "byteserde") (req "^0.6.1") (default-features #t) (kind 2)) (crate-dep (name "env_logger") (req "^0.10") (default-features #t) (kind 2)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 2)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2.0") (features (quote ("full" "extra-traits"))) (default-features #t) (kind 0)) (crate-dep (name "trybuild") (req "^1.0") (features (quote ("diff"))) (default-features #t) (kind 2)))) (hash "08n9vq1kmd25a7zq6y4hjgi1sb14q9dr3d4hmlgkv13qcaf4xvci") (yanked #t) (rust-version "1.69")))

(define-public crate-byteserde_derive-0.6 (crate (name "byteserde_derive") (vers "0.6.2") (deps (list (crate-dep (name "byteserde") (req "^0.6.2") (default-features #t) (kind 2)) (crate-dep (name "env_logger") (req "^0.10") (default-features #t) (kind 2)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 2)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2.0") (features (quote ("full" "extra-traits"))) (default-features #t) (kind 0)) (crate-dep (name "trybuild") (req "^1.0") (features (quote ("diff"))) (default-features #t) (kind 2)))) (hash "13c63168srbx66j64pfb3r1xwirshnc15419dc77if4393ssl5l9") (rust-version "1.69")))

(define-public crate-byteserde_types-0.1 (crate (name "byteserde_types") (vers "0.1.1") (deps (list (crate-dep (name "byteserde") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "byteserde_derive") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "env_logger") (req "^0.10") (default-features #t) (kind 2)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 2)))) (hash "0l55m370s34qkcbmm8gyfxqzfyyp6b1y0ibf0zw86ds6nj0lm85z") (yanked #t) (rust-version "1.69")))

(define-public crate-byteserde_types-0.2 (crate (name "byteserde_types") (vers "0.2.0") (deps (list (crate-dep (name "byteserde") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "byteserde_derive") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "env_logger") (req "^0.10") (default-features #t) (kind 2)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 2)))) (hash "0byy49dijz1a4anf36cp6snz324br1438p17zy85m7msqv154ipd") (yanked #t) (rust-version "1.69")))

(define-public crate-byteserde_types-0.3 (crate (name "byteserde_types") (vers "0.3.0") (deps (list (crate-dep (name "byteserde") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "byteserde_derive") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "env_logger") (req "^0.10") (default-features #t) (kind 2)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 2)))) (hash "0yysmsbfj15r5k2khszalp7ddam36l3hazkj221z5fjsixgnrp0s") (yanked #t) (rust-version "1.69")))

(define-public crate-byteserde_types-0.4 (crate (name "byteserde_types") (vers "0.4.0") (deps (list (crate-dep (name "byteserde") (req "^0.4.0") (default-features #t) (kind 0)) (crate-dep (name "byteserde_derive") (req "^0.4.0") (default-features #t) (kind 0)) (crate-dep (name "env_logger") (req "^0.10") (default-features #t) (kind 2)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 2)))) (hash "1n2anjqfpnxzbpy3ppmg99bf69q0npb65wjqqz5igclij3v5gnrm") (yanked #t) (rust-version "1.69")))

(define-public crate-byteserde_types-0.5 (crate (name "byteserde_types") (vers "0.5.0") (deps (list (crate-dep (name "byteserde") (req "^0.5.0") (default-features #t) (kind 0)) (crate-dep (name "byteserde_derive") (req "^0.5.0") (default-features #t) (kind 0)) (crate-dep (name "env_logger") (req "^0.10") (default-features #t) (kind 2)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 2)) (crate-dep (name "serde") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 2)))) (hash "1nwxx86b2vj55czx83a6nag31km6ma9gylgryx0hwlmq5djgh98p") (yanked #t) (rust-version "1.69")))

(define-public crate-byteserde_types-0.6 (crate (name "byteserde_types") (vers "0.6.0") (deps (list (crate-dep (name "byteserde") (req "^0.6.0") (default-features #t) (kind 0)) (crate-dep (name "byteserde_derive") (req "^0.6.0") (default-features #t) (kind 0)) (crate-dep (name "env_logger") (req "^0.10") (default-features #t) (kind 2)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 2)) (crate-dep (name "serde") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 2)))) (hash "00yk1f1m44js03jbrd1zh9jh0cdpz7k9f0ylanvcwaia5lhvrh7d") (yanked #t) (rust-version "1.69")))

(define-public crate-byteserde_types-0.6 (crate (name "byteserde_types") (vers "0.6.1") (deps (list (crate-dep (name "byteserde") (req "^0.6.1") (default-features #t) (kind 0)) (crate-dep (name "byteserde_derive") (req "^0.6.1") (default-features #t) (kind 0)) (crate-dep (name "env_logger") (req "^0.10") (default-features #t) (kind 2)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 2)) (crate-dep (name "serde") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 2)))) (hash "1r5c0kgv8c5xnfhhw1gar3ivc0p89j5i79jxy35sqp4sp4np3f2d") (yanked #t) (rust-version "1.69")))

(define-public crate-byteserde_types-0.6 (crate (name "byteserde_types") (vers "0.6.2") (deps (list (crate-dep (name "byteserde") (req "^0.6.2") (default-features #t) (kind 0)) (crate-dep (name "byteserde_derive") (req "^0.6.2") (default-features #t) (kind 0)) (crate-dep (name "env_logger") (req "^0.10") (default-features #t) (kind 2)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 2)) (crate-dep (name "serde") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 2)))) (hash "0v4pvgdaayxpg16r597rhqhr6cr2awxk8j7f5biq11878gdjlya0") (rust-version "1.69")))

(define-public crate-bytesio-0.1 (crate (name "bytesio") (vers "0.1.26") (deps (list (crate-dep (name "byteorder") (req "^1.4.2") (default-features #t) (kind 0)) (crate-dep (name "bytes") (req "^1.0.0") (default-features #t) (kind 0)) (crate-dep (name "failure") (req "^0.1.1") (default-features #t) (kind 0)) (crate-dep (name "futures") (req "^0.3.5") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.4.0") (features (quote ("full"))) (kind 0)) (crate-dep (name "tokio-stream") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "tokio-util") (req "^0.6.5") (features (quote ("codec"))) (default-features #t) (kind 0)))) (hash "1aich48jfnx7jyk0sc55zp2hbz85x43rzvkl6hv9ad4qx686crq6")))

(define-public crate-bytesio-0.1 (crate (name "bytesio") (vers "0.1.27") (deps (list (crate-dep (name "byteorder") (req "^1.4.2") (default-features #t) (kind 0)) (crate-dep (name "bytes") (req "^1.0.0") (default-features #t) (kind 0)) (crate-dep (name "failure") (req "^0.1.1") (default-features #t) (kind 0)) (crate-dep (name "futures") (req "^0.3.5") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.4.0") (features (quote ("full"))) (kind 0)) (crate-dep (name "tokio-stream") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "tokio-util") (req "^0.6.5") (features (quote ("codec"))) (default-features #t) (kind 0)))) (hash "1b6qcf5cwpg0y38x4ilmzlca7nb97aix4mkj4kzf5dxlqrjhn73y")))

(define-public crate-bytesio-0.2 (crate (name "bytesio") (vers "0.2.0") (deps (list (crate-dep (name "byteorder") (req "^1.4.2") (default-features #t) (kind 0)) (crate-dep (name "bytes") (req "^1.0.0") (default-features #t) (kind 0)) (crate-dep (name "failure") (req "^0.1.1") (default-features #t) (kind 0)) (crate-dep (name "futures") (req "^0.3.5") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.4.0") (features (quote ("full"))) (kind 0)) (crate-dep (name "tokio-stream") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "tokio-util") (req "^0.6.5") (features (quote ("codec"))) (default-features #t) (kind 0)))) (hash "127aqnmjiqa6pkp0vfyinp76yzzdsz6sfqw8bi6fmv9jmksardnk")))

(define-public crate-bytesio-0.3 (crate (name "bytesio") (vers "0.3.0") (deps (list (crate-dep (name "async-trait") (req "^0.1.70") (default-features #t) (kind 0)) (crate-dep (name "byteorder") (req "^1.4.2") (default-features #t) (kind 0)) (crate-dep (name "bytes") (req "^1.0.0") (default-features #t) (kind 0)) (crate-dep (name "failure") (req "^0.1.1") (default-features #t) (kind 0)) (crate-dep (name "futures") (req "^0.3.5") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.4.0") (features (quote ("full"))) (kind 0)) (crate-dep (name "tokio-stream") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "tokio-util") (req "^0.6.5") (features (quote ("codec"))) (default-features #t) (kind 0)))) (hash "1mba796v9llv070q7f5dy790vdzhlhk6n8bhm8aljsv5hykc4d30")))

(define-public crate-bytesio-0.3 (crate (name "bytesio") (vers "0.3.1") (deps (list (crate-dep (name "async-trait") (req "^0.1.70") (default-features #t) (kind 0)) (crate-dep (name "byteorder") (req "^1.4.2") (default-features #t) (kind 0)) (crate-dep (name "bytes") (req "^1.0.0") (default-features #t) (kind 0)) (crate-dep (name "failure") (req "^0.1.1") (default-features #t) (kind 0)) (crate-dep (name "futures") (req "^0.3.5") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.4.0") (features (quote ("full"))) (kind 0)) (crate-dep (name "tokio-stream") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "tokio-util") (req "^0.6.5") (features (quote ("codec"))) (default-features #t) (kind 0)))) (hash "0aa1lp8cln721kzjkkm3vfikkg74nw9v397xvggc44hsni6j66gz")))

(define-public crate-bytesio-0.3 (crate (name "bytesio") (vers "0.3.2") (deps (list (crate-dep (name "async-trait") (req "^0.1.70") (default-features #t) (kind 0)) (crate-dep (name "byteorder") (req "^1.4.2") (default-features #t) (kind 0)) (crate-dep (name "bytes") (req "^1.0.0") (default-features #t) (kind 0)) (crate-dep (name "failure") (req "^0.1.8") (default-features #t) (kind 0)) (crate-dep (name "futures") (req "^0.3.5") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.4.0") (features (quote ("full"))) (kind 0)) (crate-dep (name "tokio-stream") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "tokio-util") (req "^0.6.5") (features (quote ("codec"))) (default-features #t) (kind 0)))) (hash "0fj6nyiayxfk961vf2vg5kgsn3l6y4nzpby954ky2jj6mvkr38vp")))

(define-public crate-bytesio-0.3 (crate (name "bytesio") (vers "0.3.3") (deps (list (crate-dep (name "async-trait") (req "^0.1.70") (default-features #t) (kind 0)) (crate-dep (name "byteorder") (req "^1.4.2") (default-features #t) (kind 0)) (crate-dep (name "bytes") (req "^1.0.0") (default-features #t) (kind 0)) (crate-dep (name "failure") (req "^0.1.8") (default-features #t) (kind 0)) (crate-dep (name "futures") (req "^0.3.5") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.4.0") (features (quote ("full"))) (kind 0)) (crate-dep (name "tokio-stream") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "tokio-util") (req "^0.6.5") (features (quote ("codec"))) (default-features #t) (kind 0)))) (hash "1z9lpyamhcaxcm52ri74z2sz73m6pmf2qgb43v1zka0676b2zsd1")))

(define-public crate-bytesize-0.0.1 (crate (name "bytesize") (vers "0.0.1") (hash "06c28jv20mxplbdkxwrhrk7bnaxww19ghjj2vfpk5m84jr2if49z")))

(define-public crate-bytesize-0.1 (crate (name "bytesize") (vers "0.1.0") (deps (list (crate-dep (name "num") (req "^0.1.27") (default-features #t) (kind 0)))) (hash "0s213padbln67arb39bbzisik0ilw002hx9qm8ln2c228c9rw6xz")))

(define-public crate-bytesize-0.1 (crate (name "bytesize") (vers "0.1.1") (deps (list (crate-dep (name "num") (req "^0.1.27") (default-features #t) (kind 0)))) (hash "1qk5x5icwsjgdwi27g75k9cyd8sm11qhq4z94jlp2f4n0aiq0rnn")))

(define-public crate-bytesize-0.1 (crate (name "bytesize") (vers "0.1.2") (deps (list (crate-dep (name "num") (req "^0.1.36") (default-features #t) (kind 0)))) (hash "0g9lz9vmv2hpqg8cvzgpmc72ls2pb6sbbkz8x3bjnshr04i88yzy")))

(define-public crate-bytesize-0.1 (crate (name "bytesize") (vers "0.1.3") (hash "1f30siqkprv6f5imxb6ry9lnq5s883g79qlaps7zlk2rzv2r9mqn")))

(define-public crate-bytesize-0.2 (crate (name "bytesize") (vers "0.2.0") (hash "1ca10jbwgb0g451pnqb2ffsd5jqmbjpha8x605dhkpp4p30pv3a3")))

(define-public crate-bytesize-0.2 (crate (name "bytesize") (vers "0.2.1") (hash "0wyszjdqp8a3w8kqavs3nrmqwfpa8yyp3kcjfpzij7j52ni03zdj")))

(define-public crate-bytesize-0.2 (crate (name "bytesize") (vers "0.2.2") (hash "1s6d0r9gwn230kgxnfkd8vagrf82km16427455j8k6mmnqb5m2ax")))

(define-public crate-bytesize-0.2 (crate (name "bytesize") (vers "0.2.3") (hash "0zh1h9zb5inaaq1p2wdyf9dazgdchqc2jv79bfw0jsj41j8wx6dd")))

(define-public crate-bytesize-0.2 (crate (name "bytesize") (vers "0.2.4") (hash "11l92fafp8vaj1r5wlanjncm9yka2dwcjimma70mhmk967y9z5s4")))

(define-public crate-bytesize-1 (crate (name "bytesize") (vers "1.0.0") (deps (list (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)))) (hash "040hwk5hwvx9ikcs4dmb5wcx7c7nqvqwpd81a7r411lpiyhn0sbi")))

(define-public crate-bytesize-1 (crate (name "bytesize") (vers "1.0.1") (deps (list (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)))) (hash "1nl2j2bi9nk3q564jhyb77947wdv5ch54r14gjv4c59s563qd8c1")))

(define-public crate-bytesize-1 (crate (name "bytesize") (vers "1.0.2") (deps (list (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)))) (hash "1ckpa57i821j0nk131jbj77mmkcacxf8nw2cr8g549sdd2qqvc5p") (yanked #t)))

(define-public crate-bytesize-1 (crate (name "bytesize") (vers "1.1.0") (deps (list (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)))) (hash "0w7wmmbcirxp5isza0i1lxq5d7r7f0z1pxbxl5f6s1n5m8vfqn3c")))

(define-public crate-bytesize-1 (crate (name "bytesize") (vers "1.2.0") (deps (list (crate-dep (name "serde") (req "^1.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 2)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 2)) (crate-dep (name "toml") (req "^0.5") (default-features #t) (kind 2)))) (hash "1rdjrxbdbqr3rg6l02k97d5x99s2vbis3y8w9swa8d7zksbw5z1q") (features (quote (("default")))) (v 2) (features2 (quote (("serde" "dep:serde"))))))

(define-public crate-bytesize-1 (crate (name "bytesize") (vers "1.3.0") (deps (list (crate-dep (name "serde") (req "^1.0.185") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.185") (features (quote ("derive"))) (default-features #t) (kind 2)) (crate-dep (name "serde_json") (req "^1.0.105") (default-features #t) (kind 2)) (crate-dep (name "toml") (req "^0.7.6") (default-features #t) (kind 2)))) (hash "1k3aak70iwz4s2gsjbxf0ws4xnixqbdz6p2ha96s06748fpniqx3") (features (quote (("default")))) (v 2) (features2 (quote (("serde" "dep:serde"))))))

(define-public crate-bytesize-serde-0.1 (crate (name "bytesize-serde") (vers "0.1.0") (deps (list (crate-dep (name "bytesize") (req "^1.1.0") (default-features #t) (kind 0)) (crate-dep (name "quickcheck") (req "^1.0.3") (default-features #t) (kind 2)) (crate-dep (name "quickcheck_macros") (req "^1.0.0") (default-features #t) (kind 2)) (crate-dep (name "serde") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.136") (features (quote ("derive"))) (default-features #t) (kind 2)) (crate-dep (name "serde_json") (req "^1.0.79") (default-features #t) (kind 2)))) (hash "1rvnynzaim71a77n0ycas0a5h2ig6ffs07pb3xgsgm52b7plw983")))

(define-public crate-bytesize-serde-0.2 (crate (name "bytesize-serde") (vers "0.2.0") (deps (list (crate-dep (name "bincode") (req "^1.3.3") (default-features #t) (kind 2)) (crate-dep (name "bytesize") (req "^1.1.0") (default-features #t) (kind 0)) (crate-dep (name "quickcheck") (req "^1.0.3") (default-features #t) (kind 2)) (crate-dep (name "quickcheck_macros") (req "^1.0.0") (default-features #t) (kind 2)) (crate-dep (name "serde") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.136") (features (quote ("derive"))) (default-features #t) (kind 2)) (crate-dep (name "serde_json") (req "^1.0.79") (default-features #t) (kind 2)))) (hash "0r1djf34n6wyh0mvldqybrwcvj47b7gacjnl4jl6bfn8s9z346ns")))

(define-public crate-bytesize-serde-0.2 (crate (name "bytesize-serde") (vers "0.2.1") (deps (list (crate-dep (name "bincode") (req "^1.3.3") (default-features #t) (kind 2)) (crate-dep (name "bytesize") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "quickcheck") (req "^1.0.3") (default-features #t) (kind 2)) (crate-dep (name "quickcheck_macros") (req "^1.0.0") (default-features #t) (kind 2)) (crate-dep (name "serde") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.136") (features (quote ("derive"))) (default-features #t) (kind 2)) (crate-dep (name "serde_json") (req "^1.0.79") (default-features #t) (kind 2)))) (hash "0y018j9m8kkshaarzn9mvbg9zsmpcq6716avg3lmk236s8pyplc6")))

(define-public crate-bytesstr-1 (crate (name "bytesstr") (vers "1.0.0") (deps (list (crate-dep (name "bytes") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (optional #t) (default-features #t) (kind 0)))) (hash "1bkahs3si4rn22h05f8wsbkwr5sywnbgr38d0rdmprr8szs6sr97") (features (quote (("unsafe") ("default"))))))

(define-public crate-bytesstr-1 (crate (name "bytesstr") (vers "1.0.1") (deps (list (crate-dep (name "bytes") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (optional #t) (default-features #t) (kind 0)))) (hash "0r63aaf1f77vcfa16zgcs7v2vpm4gsb2gxlrb9rl5l6viwa6i70g") (features (quote (("unsafe") ("default"))))))

(define-public crate-bytesstr-1 (crate (name "bytesstr") (vers "1.0.2") (deps (list (crate-dep (name "bytes") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (optional #t) (default-features #t) (kind 0)))) (hash "1n09ir6kmawqbygg04h7diixxf4337xa9sxj3dw15jail55jhmfj") (features (quote (("unsafe") ("default"))))))

(define-public crate-bytestat-0.0.1 (crate (name "bytestat") (vers "0.0.1") (deps (list (crate-dep (name "libbytestat") (req "^0.0.2") (default-features #t) (kind 0)))) (hash "0y3zimb1pvji5x53mzpwgnmhc0walfzwz2arl76wz68x5w38yiqj")))

(define-public crate-bytestool-0.1 (crate (name "bytestool") (vers "0.1.0") (hash "0l0zyyjpvx3k3hydwmc2ji180gxlh5c9l7ryl8nhgaxdahbjy54s") (yanked #t)))

(define-public crate-bytestool-0.2 (crate (name "bytestool") (vers "0.2.0") (hash "0r6nayskyixsy8zjkxb9a1qrh7l0p7ikd6177i0hq59129yfg2qh") (yanked #t)))

(define-public crate-bytestool-0.3 (crate (name "bytestool") (vers "0.3.0") (hash "0751akv1lqbxxk8xldk1bgis23zzkiw9m5ms34gy7dg90byqj2q4") (yanked #t)))

(define-public crate-bytestr-0.1 (crate (name "bytestr") (vers "0.1.0") (deps (list (crate-dep (name "bytes") (req "^1.2.1") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.183") (optional #t) (default-features #t) (kind 0)))) (hash "1vlirjqabz6n66vis3f202900bpxyy5jsfcpbab4v7ncv0h9xarj") (v 2) (features2 (quote (("serde" "dep:serde"))))))

(define-public crate-bytestream-0.1 (crate (name "bytestream") (vers "0.1.0") (deps (list (crate-dep (name "byteorder") (req "^1.3.4") (optional #t) (default-features #t) (kind 0)))) (hash "0bckzmb2raiskckj9fjv93nng0lw9pd0c9lzxhy7ms363a30k51v") (features (quote (("std-types" "byteorder") ("default" "std-types"))))))

(define-public crate-bytestream-0.2 (crate (name "bytestream") (vers "0.2.0") (deps (list (crate-dep (name "byteorder") (req "^1.3.4") (optional #t) (default-features #t) (kind 0)))) (hash "0pzij37cf7q5qwkgqfglb5y0ccrlan3dg8grh5gvc70gyrmldqms") (features (quote (("default" "batteries-included") ("batteries-included" "byteorder"))))))

(define-public crate-bytestream-0.3 (crate (name "bytestream") (vers "0.3.0") (deps (list (crate-dep (name "byteorder") (req "^1.3.4") (optional #t) (default-features #t) (kind 0)))) (hash "0n4rfgarwjsds8byzkaqlslhxffbc38ykbzpsqzssqk53hh4sk8g") (features (quote (("default" "batteries-included") ("batteries-included" "byteorder"))))))

(define-public crate-bytestream-0.4 (crate (name "bytestream") (vers "0.4.0") (deps (list (crate-dep (name "byteorder") (req "^1.4.2") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "test-case") (req "^1.1.0") (default-features #t) (kind 2)))) (hash "0g864gm7zb7nc3qipqvdjvb54918s3g71qi3ikx1mv24mzs0lbsy") (features (quote (("default" "byteorder"))))))

(define-public crate-bytestream-0.4 (crate (name "bytestream") (vers "0.4.1") (deps (list (crate-dep (name "byteorder") (req "^1.4.2") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "test-case") (req "^1.1.0") (default-features #t) (kind 2)))) (hash "0m34m19yzvp8n2qkxk0jbwbdq7kpkg32vvlzyvx6lzbi5a221xq4") (features (quote (("default" "byteorder"))))))

(define-public crate-bytestream-rs-0.1 (crate (name "bytestream-rs") (vers "0.1.0") (deps (list (crate-dep (name "byteorder") (req "^1.4.3") (default-features #t) (kind 0)) (crate-dep (name "flate2") (req "^1.0.24") (default-features #t) (kind 0)))) (hash "1f4pv3vjhg8s3lvfvpia4kvyvn00kd4i7gvzw32lm89xgi5j3v4j")))

(define-public crate-bytestream-rs-0.2 (crate (name "bytestream-rs") (vers "0.2.0") (deps (list (crate-dep (name "byteorder") (req "^1.4.3") (default-features #t) (kind 0)) (crate-dep (name "flate2") (req "^1.0.24") (default-features #t) (kind 0)))) (hash "0p7k2d760qw9qx8jmzg8k0q68c8liv4l9vk413ia2gk0nb65nznq")))

(define-public crate-bytestream-rs-0.2 (crate (name "bytestream-rs") (vers "0.2.1") (deps (list (crate-dep (name "byteorder") (req "^1.4.3") (default-features #t) (kind 0)) (crate-dep (name "flate2") (req "^1.0.24") (default-features #t) (kind 0)))) (hash "1rq69a91d0nfbm1yfnl0hp12h80rbnlr83v9pcb1ba5h0762wz9v")))

(define-public crate-bytestream-rs-0.2 (crate (name "bytestream-rs") (vers "0.2.2") (deps (list (crate-dep (name "byteorder") (req "^1.4.3") (default-features #t) (kind 0)) (crate-dep (name "flate2") (req "^1.0.24") (default-features #t) (kind 0)))) (hash "17fi2010bi4ihq7nsjcqhg3n62jhq5pw42587zjkaajj41r2nmma")))

(define-public crate-bytestream-rs-0.2 (crate (name "bytestream-rs") (vers "0.2.3") (deps (list (crate-dep (name "byteorder") (req "^1.4.3") (default-features #t) (kind 0)) (crate-dep (name "flate2") (req "^1.0.24") (default-features #t) (kind 0)))) (hash "0jz7kag3zkibga8pkd692fgmsy7q4nxa6xx6jp48a498y9jf1svc")))

(define-public crate-bytestream-rs-0.2 (crate (name "bytestream-rs") (vers "0.2.4") (deps (list (crate-dep (name "byteorder") (req "^1.4.3") (default-features #t) (kind 0)) (crate-dep (name "flate2") (req "^1.0.24") (default-features #t) (kind 0)))) (hash "1wzwa23ira9b19l6xcjgvhjvfi6yhyagf4v2p4ymf0yrsjil4g10")))

(define-public crate-bytestream-rs-0.2 (crate (name "bytestream-rs") (vers "0.2.5") (deps (list (crate-dep (name "byteorder") (req "^1.4.3") (default-features #t) (kind 0)) (crate-dep (name "flate2") (req "^1.0.24") (default-features #t) (kind 0)))) (hash "035qb0x95n3bz7wnr3kwrqsck8f7js26n97hsd8xcbix85505cw5")))

(define-public crate-bytestream-rs-0.2 (crate (name "bytestream-rs") (vers "0.2.6") (deps (list (crate-dep (name "byteorder") (req "^1.4.3") (default-features #t) (kind 0)) (crate-dep (name "flate2") (req "^1.0.24") (default-features #t) (kind 0)))) (hash "11kfx6nd0v5lylgl5slr2m34fh9jlkq6cfz2447y0i26y5xx46ks")))

(define-public crate-bytestream-rs-0.3 (crate (name "bytestream-rs") (vers "0.3.0") (deps (list (crate-dep (name "byteorder") (req "^1.4.3") (default-features #t) (kind 0)) (crate-dep (name "bytestream_derive") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "flate2") (req "^1.0.24") (default-features #t) (kind 0)))) (hash "1df1lcp1yj8glnjb6mrw8saj1n9b5802r3xim5283jfnr82sqgxw")))

(define-public crate-bytestream-rs-0.3 (crate (name "bytestream-rs") (vers "0.3.1") (deps (list (crate-dep (name "byteorder") (req "^1.4.3") (default-features #t) (kind 0)) (crate-dep (name "bytestream_derive") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "flate2") (req "^1.0.24") (default-features #t) (kind 0)))) (hash "0ipkvvs4hizdla9vyjn2yr1wmf7rvp33slhcsbmxjvq4yaqnc79w")))

(define-public crate-bytestream-rs-0.3 (crate (name "bytestream-rs") (vers "0.3.2") (deps (list (crate-dep (name "byteorder") (req "^1.4.3") (default-features #t) (kind 0)) (crate-dep (name "bytestream_derive") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "flate2") (req "^1.0.24") (default-features #t) (kind 0)))) (hash "152rhjpxngxnay3jvlk784s4fljrz21hrrg5zy817jh7nwhp3hcj")))

(define-public crate-bytestream-rs-0.3 (crate (name "bytestream-rs") (vers "0.3.3") (deps (list (crate-dep (name "byteorder") (req "^1.4.3") (default-features #t) (kind 0)) (crate-dep (name "bytestream_derive") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "flate2") (req "^1.0.24") (default-features #t) (kind 0)))) (hash "1dpw23gjnpnnk35xn3jfx4dp3h707vh2igm4f0674hbskv36is0y")))

(define-public crate-bytestream-rs-0.3 (crate (name "bytestream-rs") (vers "0.3.4") (deps (list (crate-dep (name "byteorder") (req "^1.4.3") (default-features #t) (kind 0)) (crate-dep (name "bytestream_derive") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "flate2") (req "^1.0.24") (default-features #t) (kind 0)))) (hash "0nirb4di83awcsjf5qgdlf51jb2mr1g2v3dgq1lzgdhjc0frpygd")))

(define-public crate-bytestream-rs-0.3 (crate (name "bytestream-rs") (vers "0.3.5") (deps (list (crate-dep (name "byteorder") (req "^1.4.3") (default-features #t) (kind 0)) (crate-dep (name "bytestream_derive") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "flate2") (req "^1.0.24") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)))) (hash "0z0gwxn1f53szjx9nvv54lxpz0qgv57d489qyg4jc2iansga2g4d")))

(define-public crate-bytestream-rs-0.3 (crate (name "bytestream-rs") (vers "0.3.6") (deps (list (crate-dep (name "byteorder") (req "^1.4.3") (default-features #t) (kind 0)) (crate-dep (name "bytestream_derive") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "flate2") (req "^1.0.24") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)))) (hash "0j9yks1jxdsild9q5y4z3v80bg13cl58ihjnilh3jk74qrjxdlw5")))

(define-public crate-bytestream-rs-0.3 (crate (name "bytestream-rs") (vers "0.3.8") (deps (list (crate-dep (name "byteorder") (req "^1.4.3") (default-features #t) (kind 0)) (crate-dep (name "bytestream_derive") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "flate2") (req "^1.0.24") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)))) (hash "0wdn8kjsx965xhr9iafdkaih0fskgnylhsdf938pr4ncv7rjk9kj")))

(define-public crate-bytestream-rs-0.4 (crate (name "bytestream-rs") (vers "0.4.0") (deps (list (crate-dep (name "byteorder") (req "^1.4.3") (default-features #t) (kind 0)) (crate-dep (name "bytestream_derive") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "flate2") (req "^1.0.24") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)))) (hash "13vl07kli83nxv8law5km80ki2nlw78hlzamf2iczbnnhhb6ls8s")))

(define-public crate-bytestream-rs-0.4 (crate (name "bytestream-rs") (vers "0.4.1") (deps (list (crate-dep (name "byteorder") (req "^1.4.3") (default-features #t) (kind 0)) (crate-dep (name "bytestream_derive") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "flate2") (req "^1.0.24") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)))) (hash "1ym9ryk7gi7pwdb8qdd8zinib3zf6iyrijl9kx71ma0w6cdzp51g")))

(define-public crate-bytestream_derive-0.1 (crate (name "bytestream_derive") (vers "0.1.0") (deps (list (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (default-features #t) (kind 0)))) (hash "1m80zl5aqim4jw8zi5zvlk5h8qrgydl3y3jipy25z8b07n3029fr")))

(define-public crate-bytestream_derive-0.2 (crate (name "bytestream_derive") (vers "0.2.0") (deps (list (crate-dep (name "heck") (req "^0.4.0") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (default-features #t) (kind 0)))) (hash "1ihrka1dadvr71hmcpvna9h3m7hxpw10vxhbs5j0hd440nzqq4cf")))

(define-public crate-bytestream_derive-0.2 (crate (name "bytestream_derive") (vers "0.2.1") (deps (list (crate-dep (name "heck") (req "^0.4.0") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1.0.47") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.21") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.103") (default-features #t) (kind 0)))) (hash "1zm9qygv0642fpqazplfzjbi91axqi4f3sk7dw6s1zd2hpicqcnl")))

(define-public crate-bytestream_derive-0.2 (crate (name "bytestream_derive") (vers "0.2.2") (deps (list (crate-dep (name "heck") (req "^0.4.0") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1.0.47") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.21") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.105") (default-features #t) (kind 0)))) (hash "16jwdw8f3vw8zr2lbwc8mr14fbd48ykfbg2drsqxy9dxvkclppa9")))

(define-public crate-bytestream_io-0.1 (crate (name "bytestream_io") (vers "0.1.0") (deps (list (crate-dep (name "byteorder") (req "^1.3") (default-features #t) (kind 0)) (crate-dep (name "coc-rs") (req "^0.7.1") (default-features #t) (kind 0)) (crate-dep (name "derive_more") (req "^0.99.1") (default-features #t) (kind 0)) (crate-dep (name "flate2") (req "^1.0.24") (default-features #t) (kind 0)))) (hash "068ibf8j290rzrszv4fxlm54pkqjmnxyy5ar6lpkhp0gv3b8fy4m")))

(define-public crate-bytestream_io-0.1 (crate (name "bytestream_io") (vers "0.1.1") (deps (list (crate-dep (name "byteorder") (req "^1.3") (default-features #t) (kind 0)) (crate-dep (name "coc-rs") (req "^0.7.1") (default-features #t) (kind 0)) (crate-dep (name "derive_more") (req "^0.99.1") (default-features #t) (kind 0)) (crate-dep (name "flate2") (req "^1.0.24") (default-features #t) (kind 0)))) (hash "1s28cn9w1fjyvbfb9zm1qbi6pi0iq4v949i67swri78x193y15cb")))

(define-public crate-bytestream_io-0.1 (crate (name "bytestream_io") (vers "0.1.2") (deps (list (crate-dep (name "byteorder") (req "^1.3") (default-features #t) (kind 0)) (crate-dep (name "coc-rs") (req "^0.7.1") (default-features #t) (kind 0)) (crate-dep (name "derive_more") (req "^0.99.1") (default-features #t) (kind 0)) (crate-dep (name "flate2") (req "^1.0.24") (default-features #t) (kind 0)))) (hash "0slp93s428jnf50w3v7hnsp0dn3f87yq273fah6vs16i5zgvvn0w")))

(define-public crate-bytestream_io-0.1 (crate (name "bytestream_io") (vers "0.1.4") (deps (list (crate-dep (name "byteorder") (req "^1.3") (default-features #t) (kind 0)) (crate-dep (name "coc-rs") (req "^0.7.1") (default-features #t) (kind 0)) (crate-dep (name "derive_more") (req "^0.99.1") (default-features #t) (kind 0)) (crate-dep (name "flate2") (req "^1.0.24") (default-features #t) (kind 0)))) (hash "1brzpczvl7gb05af1lsbjk85pxzqyz35mgyq3mjld6vcvvy2xzgp")))

(define-public crate-bytestream_io-0.1 (crate (name "bytestream_io") (vers "0.1.5") (deps (list (crate-dep (name "byteorder") (req "^1.3") (default-features #t) (kind 0)) (crate-dep (name "coc-rs") (req "^0.7.1") (default-features #t) (kind 0)) (crate-dep (name "derive_more") (req "^0.99.1") (default-features #t) (kind 0)) (crate-dep (name "flate2") (req "^1.0.24") (default-features #t) (kind 0)))) (hash "1ak7pfc903cmysm29jivn6ard1vjysbcg4gr7d19jnm0lxvbcvsy")))

(define-public crate-bytestream_io-0.1 (crate (name "bytestream_io") (vers "0.1.6") (deps (list (crate-dep (name "byteorder") (req "^1.3") (default-features #t) (kind 0)) (crate-dep (name "coc-rs") (req "^0.7.1") (default-features #t) (kind 0)) (crate-dep (name "derive_more") (req "^0.99.1") (default-features #t) (kind 0)) (crate-dep (name "flate2") (req "^1.0.24") (default-features #t) (kind 0)))) (hash "1xs9fl3xxkwakrbhjyh1i52gk37qmhp9nnnk58c1x5zy3z8q0ssz")))

(define-public crate-bytestream_io-0.1 (crate (name "bytestream_io") (vers "0.1.7") (deps (list (crate-dep (name "byteorder") (req "^1.4.3") (default-features #t) (kind 0)) (crate-dep (name "derive_more") (req "^0.99.17") (default-features #t) (kind 0)) (crate-dep (name "flate2") (req "^1.0.24") (default-features #t) (kind 0)) (crate-dep (name "logic-long") (req "^0.2.0") (default-features #t) (kind 0)))) (hash "12cyp3k3rnnh8p8hvcdwnq6mrl65a45xyvhn270y31khdll99x4q")))

(define-public crate-bytestring-0.1 (crate (name "bytestring") (vers "0.1.0") (deps (list (crate-dep (name "bytes") (req "^0.5.2") (default-features #t) (kind 0)))) (hash "1jj31j9riyjq8is1ip5hbi45nm07xrrfs7gm9yaqdq1zzirrgf8c")))

(define-public crate-bytestring-0.1 (crate (name "bytestring") (vers "0.1.1") (deps (list (crate-dep (name "bytes") (req "^0.5.2") (default-features #t) (kind 0)))) (hash "0dck240lhcpb1gcaza5czanbkx1czjqb2dpwxs7rasfp6sx2m1zy")))

(define-public crate-bytestring-0.1 (crate (name "bytestring") (vers "0.1.2") (deps (list (crate-dep (name "bytes") (req "^0.5.3") (default-features #t) (kind 0)))) (hash "178378s64sq7licfl1hl48v9489vfrg3z3d5rb90ir1j8ix10k5j")))

(define-public crate-bytestring-0.1 (crate (name "bytestring") (vers "0.1.3") (deps (list (crate-dep (name "bytes") (req "^0.5.3") (default-features #t) (kind 0)))) (hash "0szfb8nv75015xlljdzvzbrhm5gx040yfd4l79ifv5z23v624vcc")))

(define-public crate-bytestring-0.1 (crate (name "bytestring") (vers "0.1.4") (deps (list (crate-dep (name "bytes") (req "^0.5.3") (default-features #t) (kind 0)))) (hash "1m7advs49l66s7nj72ic6scx1c1y8ah64k06fj4crxlfymkp89pw")))

(define-public crate-bytestring-0.1 (crate (name "bytestring") (vers "0.1.5") (deps (list (crate-dep (name "bytes") (req "^0.5.3") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 2)))) (hash "0qzkncgky5p5vsdb9msmfg6d92dcs9idcjcr5nk7inkja7x0az7w")))

(define-public crate-bytestring-1 (crate (name "bytestring") (vers "1.0.0") (deps (list (crate-dep (name "bytes") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (optional #t) (default-features #t) (kind 0)))) (hash "0ka9gkn2rrxms0d5s32ckpchh06qmgidbh4xw630gfcpkshnnw4h")))

(define-public crate-bytestring-1 (crate (name "bytestring") (vers "1.1.0") (deps (list (crate-dep (name "bytes") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (optional #t) (default-features #t) (kind 0)))) (hash "12ljqf7v6x094n89vy6s1ab130dy2abxfp5g0vphi204sdgsgdl6")))

(define-public crate-bytestring-1 (crate (name "bytestring") (vers "1.2.0") (deps (list (crate-dep (name "bytes") (req "^1.2") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (optional #t) (default-features #t) (kind 0)))) (hash "18b9gbd19h4pgh6mffdrh0c28qs4d7i04is0apilhh8mv5bkxy7p")))

(define-public crate-bytestring-1 (crate (name "bytestring") (vers "1.3.0") (deps (list (crate-dep (name "bytes") (req "^1.2") (kind 0)) (crate-dep (name "serde") (req "^1.0") (optional #t) (default-features #t) (kind 0)))) (hash "1blscywg9gaw6zdc5hqsf9zwyqiym57q631nk7wc960dfs34i3i3")))

(define-public crate-bytestring-1 (crate (name "bytestring") (vers "1.3.1") (deps (list (crate-dep (name "bytes") (req "^1.2") (kind 0)) (crate-dep (name "serde") (req "^1.0") (optional #t) (default-features #t) (kind 0)))) (hash "0wpf0c5c72x3ycdb85vznkmcy8fy6ckzd512064dyabbx81h5n3l") (rust-version "1.65")))

(define-public crate-bytestruct-0.1 (crate (name "bytestruct") (vers "0.1.0") (hash "0mpby1jlxdasyh8axz2vjj0wf44gmpsn0412f4sygq00y8ni5p4h")))

(define-public crate-bytestruct-derive-0.1 (crate (name "bytestruct-derive") (vers "0.1.0") (deps (list (crate-dep (name "quote") (req "^1.0.34") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2.0.43") (default-features #t) (kind 0)))) (hash "1lpy2avrkbwdvlq24nw0pgfvfpfxpp4yyk11lqh9wk0hs6frvym5")))

(define-public crate-bytesutil-0.1 (crate (name "bytesutil") (vers "0.1.0") (hash "1f196icds6h8024lj7hb2yynih53b87ddlfv9dv0d87irbx5k3c1") (features (quote (("std") ("default" "std"))))))

(define-public crate-bytesutil-0.2 (crate (name "bytesutil") (vers "0.2.0") (hash "05zayqv2pb4iiw4xkblb4h7rnb77wk8083x9lfmg4fyxl7blxhza") (features (quote (("std") ("default" "std"))))))

(define-public crate-bytesutil-0.3 (crate (name "bytesutil") (vers "0.3.0") (hash "151kfla1030kvg7f2hmz0pbzhmnq4ka2dal8d58nigijqgpb32sr") (features (quote (("std") ("default" "std"))))))

(define-public crate-bytesutil-0.4 (crate (name "bytesutil") (vers "0.4.0") (hash "06db75qg2bff86lck3sbfnsys83q4mhwwg939siax1vs7n3lhi16") (features (quote (("std") ("default" "std"))))))

(define-public crate-bytesutil-0.5 (crate (name "bytesutil") (vers "0.5.0") (hash "1gmf12g0bk0ldn6ljv5w96mnp3m6yb7sfxh6a91kxn1b4cvh9m36") (features (quote (("std") ("default" "std"))))))

(define-public crate-bytesutil-0.6 (crate (name "bytesutil") (vers "0.6.0") (hash "0fb84rpdl9vjf11jw43b7d55dzxs9npxyr5z3fqbqkqrqqxn0a7h") (features (quote (("std") ("default" "std"))))))

(define-public crate-bytesutil-0.7 (crate (name "bytesutil") (vers "0.7.0") (hash "1fwx4sqk369vd3m2yzanx4ld9ixwva505b69x7v3wfx6nivslagl") (features (quote (("std") ("default" "std"))))))

(define-public crate-byteunit-0.1 (crate (name "byteunit") (vers "0.1.0") (hash "1dj4k00gni2qv45n1g83sn5nacagfcz10vc9d5a3vr5490g663q5") (yanked #t)))

(define-public crate-bytevec-0.1 (crate (name "bytevec") (vers "0.1.0") (hash "0fz7f7ixma09qjn2ncr70wx9c65kdza0dsvlafd5d3ycnxicinas")))

(define-public crate-bytevec-0.1 (crate (name "bytevec") (vers "0.1.1") (hash "0gdyav3w8jxlzlyimbm6hycm3fv8xpprf9zymkksj2hk6xq02f37")))

(define-public crate-bytevec-0.2 (crate (name "bytevec") (vers "0.2.0") (hash "0hrb7ilnsa2wia8y881zii8hj55iqs7x2yq7kiv4wl405nnyc7hd")))

(define-public crate-bytewise-0.1 (crate (name "bytewise") (vers "0.1.0") (hash "1kjczqnbiwhz82c0dddym2pk5ij0bbdkcqhdjyd3wi0b68dnrvx4") (yanked #t)))

(define-public crate-bytewise-0.2 (crate (name "bytewise") (vers "0.2.0") (hash "1dhwrrwjvqd9j85p0wn56nmn7r586i0j41631y1nv103ib9x9lic") (yanked #t)))

(define-public crate-bytewise-0.3 (crate (name "bytewise") (vers "0.3.0") (deps (list (crate-dep (name "paste") (req "^1.0") (default-features #t) (kind 0)))) (hash "1akp7a5pdpj07b4ygjp4i4rg6ji9pibyl6gc716dyw7rkva6hcwr") (yanked #t)))

(define-public crate-bytex-0.1 (crate (name "bytex") (vers "0.1.0") (hash "1dzcz9iprq94f5ybdqpr78mn851fqlrq5g39j50fhkwr0j97zlhi") (yanked #t)))

(define-public crate-bytex-0.1 (crate (name "bytex") (vers "0.1.1") (hash "19d65gakr7wr0sg3bwknr0rld43gprfg14sh3xdqp9hqnlb85545")))

(define-public crate-bytey-0.0.0 (crate (name "bytey") (vers "0.0.0") (hash "06cr8dzmfhm53la6nvzmkaqr1psbvzq6ygwy02avj5jhkd8r30pn")))

(define-public crate-bytey-0.1 (crate (name "bytey") (vers "0.1.0") (deps (list (crate-dep (name "bytey_byte_buffer") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "17cgrvaykm6vm0kzrkn8m7lb5yjhxdf5659j6cssik34y7dh9bjh")))

(define-public crate-bytey-0.1 (crate (name "bytey") (vers "0.1.1") (deps (list (crate-dep (name "bytey_byte_buffer") (req "^0.1.1") (default-features #t) (kind 0)))) (hash "0gsbvp2nq0016g2pcib67bqzzkaiy7szd60c7q4jgm0bsnpgn9wi")))

(define-public crate-bytey-0.2 (crate (name "bytey") (vers "0.2.0") (deps (list (crate-dep (name "bytey_bincode") (req "^0.1.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "bytey_byte_buffer") (req "^0.2.0") (default-features #t) (kind 0)))) (hash "021q473dvl1r1v0n7xqx17zwanrgvicmgkckk13l20grw26w8n5p") (features (quote (("bincode_serialize" "bytey_bincode"))))))

(define-public crate-bytey-0.3 (crate (name "bytey") (vers "0.3.0") (deps (list (crate-dep (name "bytey_bincode") (req "^0.1.1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "bytey_byte_buffer") (req "^0.2.1") (default-features #t) (kind 0)) (crate-dep (name "bytey_derive") (req "^0.2.0") (default-features #t) (kind 0)))) (hash "0nbq3qpa54d40psv91y4dy82ca8i6s4ypm2jv2zi8yp9wjzlh77w") (features (quote (("bincode_serialize" "bytey_bincode"))))))

(define-public crate-bytey_bincode-0.1 (crate (name "bytey_bincode") (vers "0.1.0") (deps (list (crate-dep (name "bincode") (req "^2.0.0-beta.2") (default-features #t) (kind 0)) (crate-dep (name "bytey_byte_buffer") (req "^0.2.0") (default-features #t) (kind 0)))) (hash "03qqqafqvbr2zf32i2bsw7fxsxjrv3n1lffz1f3xl46l6akf7naa")))

(define-public crate-bytey_bincode-0.1 (crate (name "bytey_bincode") (vers "0.1.1") (deps (list (crate-dep (name "bincode") (req "^2.0.0-beta.2") (default-features #t) (kind 0)) (crate-dep (name "bytey_byte_buffer") (req "^0.2.1") (default-features #t) (kind 0)))) (hash "14s3dj58qf260pxa1hb8cdn1xnk3c9f6i1rbm5sp8l6yrmhxankj")))

(define-public crate-bytey_byte_buffer-0.1 (crate (name "bytey_byte_buffer") (vers "0.1.0") (deps (list (crate-dep (name "cfg-if") (req "^1.0.0") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.30") (default-features #t) (kind 0)))) (hash "18dj1sygrbq9f9ah729xp9ysa18csvq8smi259yadwi3g7q5mr38")))

(define-public crate-bytey_byte_buffer-0.1 (crate (name "bytey_byte_buffer") (vers "0.1.1") (deps (list (crate-dep (name "cfg-if") (req "^1.0.0") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.30") (default-features #t) (kind 0)))) (hash "09kp1bjkmxbq75g374zwn2dag45d50qqmmjq48l83k8sp37h586f")))

(define-public crate-bytey_byte_buffer-0.2 (crate (name "bytey_byte_buffer") (vers "0.2.0") (deps (list (crate-dep (name "cfg-if") (req "^1.0.0") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.30") (default-features #t) (kind 0)))) (hash "1hy1g9yzzhzksqrw8hawmmxn90jqv0i67b7y0lmbpq9lm39ypmjs")))

(define-public crate-bytey_byte_buffer-0.2 (crate (name "bytey_byte_buffer") (vers "0.2.1") (deps (list (crate-dep (name "cfg-if") (req "^1.0.0") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.30") (default-features #t) (kind 0)))) (hash "0b57bsgkvqn575rv1hqvagxvw6xnlr9z33lh1jmp7bmxjxknklp2")))

(define-public crate-bytey_derive-0.1 (crate (name "bytey_derive") (vers "0.1.0") (deps (list (crate-dep (name "bytey_byte_buffer") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1.0.36") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.15") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.86") (default-features #t) (kind 0)))) (hash "1w14q75i0mjxi4hypp0zlwjdx5z2972c1r791807drbzdgc0s5a9")))

(define-public crate-bytey_derive-0.2 (crate (name "bytey_derive") (vers "0.2.0") (deps (list (crate-dep (name "proc-macro2") (req "^1.0.36") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.15") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.86") (default-features #t) (kind 0)))) (hash "1qdqrjlx1r1v1sixabx6sdirf02mprfyw1j4dq0xrv6hhxvy4yfq")))

(define-public crate-byteyarn-0.1 (crate (name "byteyarn") (vers "0.1.0") (hash "1p24xvsxmk706hk4gk6jngpd59d8pbd930f0q1451wsprhvczknm")))

(define-public crate-byteyarn-0.1 (crate (name "byteyarn") (vers "0.1.1") (hash "1kn80x2q0dy5x5zz3y41wpln87x5k7nbqivb1jyvp258xk5srf1h")))

(define-public crate-byteyarn-0.2 (crate (name "byteyarn") (vers "0.2.0") (hash "13g8f5r06b1i3l2i05c78p7km4s8fwxd111k3pbpjg2x5qminnap")))

(define-public crate-byteyarn-0.2 (crate (name "byteyarn") (vers "0.2.1") (hash "061mqnq2gwbisyjybqvinsaqv4s6hia88xzr9pdk1mlgn2k02xqr")))

(define-public crate-byteyarn-0.2 (crate (name "byteyarn") (vers "0.2.2") (hash "1qcrdaz8bjps306fr17jiknrkqk1bb9vcyfd6csgi29c86vgwxrr") (yanked #t)))

(define-public crate-byteyarn-0.2 (crate (name "byteyarn") (vers "0.2.3") (hash "1zs2r5qiqwhd6fj7bqg8ziha63sbggy5xmq6vfsan5zaq00l6lx7")))

(define-public crate-byteyarn-0.3 (crate (name "byteyarn") (vers "0.3.0") (hash "1imgslv51s11gymm40vhdsvsq8g6z7fg6p32kzngqawyndkjym50")))

(define-public crate-byteyarn-0.4 (crate (name "byteyarn") (vers "0.4.0") (deps (list (crate-dep (name "buf-trait") (req "^0.4") (default-features #t) (kind 0)))) (hash "0ys4ir2h6j8wg1948y9qpiz345z7361w49xl5bwfq096gk0vdy5p")))

(define-public crate-byteyarn-0.4 (crate (name "byteyarn") (vers "0.4.1") (deps (list (crate-dep (name "buf-trait") (req "^0.4") (default-features #t) (kind 0)))) (hash "0x2psrnab44m31xbnfx9l4qkjn366qhq1fphjpp5waxga05xhzsp")))

(define-public crate-byteyarn-0.5 (crate (name "byteyarn") (vers "0.5.0") (deps (list (crate-dep (name "buf-trait") (req "^0.4") (default-features #t) (kind 0)))) (hash "0pg1yv8dxbmnm070iwby0nsjm1rqgmj0hb25kshli76a0j9ahz7z")))

(define-public crate-byteyarn-0.5 (crate (name "byteyarn") (vers "0.5.1") (deps (list (crate-dep (name "buf-trait") (req "^0.4") (default-features #t) (kind 0)))) (hash "0yygyhncfij3skkb3824wr1xn1f47p0y09c5kyjmx8b8ck952gmr")))

(define-public crate-bytez-0.1 (crate (name "bytez") (vers "0.1.0") (deps (list (crate-dep (name "goblin") (req "^0.8.0") (default-features #t) (kind 0)))) (hash "07krzrllapz5niqhgsbkza7r3q5xkal1ybl3k4ymr6pbjy1b8p3n")))

(define-public crate-bytez-0.1 (crate (name "bytez") (vers "0.1.1") (deps (list (crate-dep (name "goblin") (req "^0.8.0") (default-features #t) (kind 0)))) (hash "0zmip8zn9xran3c1k7k2dqshpk80jvd3gl5r2kihc0jsqcprac1l")))

(define-public crate-bytezy-0.0.1 (crate (name "bytezy") (vers "0.0.1") (deps (list (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2.0") (features (quote ("extra-traits"))) (default-features #t) (kind 0)) (crate-dep (name "trybuild") (req "^1.0.49") (features (quote ("diff"))) (default-features #t) (kind 2)))) (hash "0d4mb3w5f5yrhfa8cd3l4pi0sd3fv15b2v22n1lanb980l5yn0jf")))

