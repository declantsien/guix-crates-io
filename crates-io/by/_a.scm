(define-module (crates-io by _a) #:use-module (crates-io))

(define-public crate-by_address-1 (crate (name "by_address") (vers "1.0.0") (hash "1xfv8aamy4khcv5hdhsz5s1yffqz64b283mn7v5ff9zriamq31h7")))

(define-public crate-by_address-1 (crate (name "by_address") (vers "1.0.1") (hash "06l5cmmdw6izgfafsixhvnwiplc2w67wfpwah0mm41rw294dn223")))

(define-public crate-by_address-1 (crate (name "by_address") (vers "1.0.2") (hash "0amk4d62xsxy3lsv2ry5id0vl48bkk5gqfzxxkb3763d5hf0pd85")))

(define-public crate-by_address-1 (crate (name "by_address") (vers "1.0.3") (hash "0zc7hxnsb9w408w8na8c7acrvl1lrlswzgsqxl08jv17k01cbafi")))

(define-public crate-by_address-1 (crate (name "by_address") (vers "1.0.4") (hash "0kwlq82yj7nhghnzw59pdw5babjdnm7g2p6n21cb8kpbc17p0ig2")))

(define-public crate-by_address-2 (crate (name "by_address") (vers "2.0.0") (hash "0zzqbjngh863zrh58kb9hjzdb1c7fmvq1ikhl0qyvm370ja0d3hg") (yanked #t)))

(define-public crate-by_address-2 (crate (name "by_address") (vers "2.1.0") (hash "0ql9b1krx9117psl6fl2x03l385gkkfvm3lsqzkr2yyqibzzh6ya") (yanked #t)))

(define-public crate-by_address-1 (crate (name "by_address") (vers "1.1.0") (hash "1l0km1y8ybwjkdn3nca4bcbk3csymv4hyngjl5lxfkhid0lbm3dz")))

(define-public crate-by_address-1 (crate (name "by_address") (vers "1.1.1") (hash "1p0gc05j2j53qb1aby1hgz88gz6mgr7pr987bxpzhkvjh25whii0")))

(define-public crate-by_address-1 (crate (name "by_address") (vers "1.1.2") (hash "16158zv5ncinbr7i8f2qdwk2zrab35l03zby7hbjl1axn330hkwx")))

(define-public crate-by_address-1 (crate (name "by_address") (vers "1.2.0") (hash "020fbsjsr4v4y2v3jhpzym24fz45lidmwrmci03mq1nljq1k7sg7")))

(define-public crate-by_address-1 (crate (name "by_address") (vers "1.2.1") (hash "01idmag3lcwnnqrnnyik2gmbrr34drsi97q15ihvcbbidf2kryk4")))

