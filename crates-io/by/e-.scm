(define-module (crates-io by e-) #:use-module (crates-io))

(define-public crate-bye-branch-0.1 (crate (name "bye-branch") (vers "0.1.0") (deps (list (crate-dep (name "requestty") (req "^0.5.0") (default-features #t) (kind 0)))) (hash "1c3x7d6b55dwc4gbqiwrfd3scwblqdq6psv4iavkh3815ja9cp1f") (yanked #t)))

(define-public crate-bye-branch-0.1 (crate (name "bye-branch") (vers "0.1.1") (deps (list (crate-dep (name "requestty") (req "^0.5.0") (default-features #t) (kind 0)))) (hash "19gsiqwb7cpcfrkv78h7h488zhjm1rydbfai1zwbx947fzrz9nxs")))

(define-public crate-bye-branch-0.1 (crate (name "bye-branch") (vers "0.1.11") (deps (list (crate-dep (name "requestty") (req "^0.5.0") (default-features #t) (kind 0)))) (hash "1pnsq2rn6gi2c5vvxs8jl2g9hljqxpsjhl5rxchp259a4nd1glqn")))

(define-public crate-bye-branch-0.1 (crate (name "bye-branch") (vers "0.1.12") (deps (list (crate-dep (name "requestty") (req "^0.5.0") (default-features #t) (kind 0)))) (hash "1wf2h3svnwm55b0p58jjkz78nhpfhipa1h07f04kp7nvq36yl5v8")))

