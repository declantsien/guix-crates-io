(define-module (crates-io g1 -m) #:use-module (crates-io))

(define-public crate-g1-macros-0.1 (crate (name "g1-macros") (vers "0.1.0-alpha.3") (deps (list (crate-dep (name "g1-common") (req "^0.1.0-alpha.3") (default-features #t) (kind 0)) (crate-dep (name "proc-macro-hack") (req "^0.5.11") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1.0.6") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.2") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.11") (default-features #t) (kind 0)))) (hash "0im67i43ywnaan18sdyxi7q1mv9mywsl85376bf1wm8mxhfl7r93")))

