(define-module (crates-io g1 #{3-}#) #:use-module (crates-io))

(define-public crate-g13-rs-0.1 (crate (name "g13-rs") (vers "0.1.0") (deps (list (crate-dep (name "libusb") (req "^0.3.0") (default-features #t) (kind 0)))) (hash "134w9ad0bxl6j9ny15gh6lkcni0g7s826ih5hlmmxdjvrhv29bqp")))

