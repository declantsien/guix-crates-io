(define-module (crates-io zm em) #:use-module (crates-io))

(define-public crate-zmem-0.2 (crate (name "zmem") (vers "0.2.1") (deps (list (crate-dep (name "clap") (req "^4.4.11") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "colored") (req "^2.0.4") (default-features #t) (kind 0)) (crate-dep (name "futures") (req "^0.3.29") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.34.0") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "1jbl97shfll3s70c7hqgx2lxkd9pp98w3qhgnbp9gvh927drvsk0")))

