(define-module (crates-io zm tp) #:use-module (crates-io))

(define-public crate-zmtp-0.6 (crate (name "zmtp") (vers "0.6.0") (deps (list (crate-dep (name "byteorder") (req "^0.5.3") (default-features #t) (kind 0)))) (hash "08rihd1qcb4gbm77wpvl2i2i17lalvh6hnx276lhcvy8wy86x1z1")))

