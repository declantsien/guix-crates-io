(define-module (crates-io bc hl) #:use-module (crates-io))

(define-public crate-bchlib-0.2 (crate (name "bchlib") (vers "0.2.0") (deps (list (crate-dep (name "bchlib-sys") (req "^0.2.0") (default-features #t) (kind 0)))) (hash "0qx8mg7a9blgj5r5z21ihqv5l6i4gsx060df65bh40w18h9am132")))

(define-public crate-bchlib-0.2 (crate (name "bchlib") (vers "0.2.1") (deps (list (crate-dep (name "bchlib-sys") (req "^0.2.1") (default-features #t) (kind 0)))) (hash "13bmsn5f0lrrbb1p09gm283n4nv5w59s6rkg2v70rlh1glh1hi9w")))

(define-public crate-bchlib-sys-0.1 (crate (name "bchlib-sys") (vers "0.1.0") (deps (list (crate-dep (name "bindgen") (req "^0.42.2") (default-features #t) (kind 1)) (crate-dep (name "cc") (req "^1.0") (default-features #t) (kind 1)))) (hash "0wvdfr1zkgzi49x8lr1mj5iim74qa63pgy9d46biwskksgjhpl29")))

(define-public crate-bchlib-sys-0.2 (crate (name "bchlib-sys") (vers "0.2.0") (deps (list (crate-dep (name "bindgen") (req "^0.42.2") (default-features #t) (kind 1)) (crate-dep (name "cc") (req "^1.0") (default-features #t) (kind 1)))) (hash "1wh5jd31368wizdsqmaswa3fxi6law6kpwbrjw501kz8djj91jnz")))

(define-public crate-bchlib-sys-0.2 (crate (name "bchlib-sys") (vers "0.2.1") (deps (list (crate-dep (name "bindgen") (req "^0.42.2") (default-features #t) (kind 1)) (crate-dep (name "cc") (req "^1.0") (default-features #t) (kind 1)))) (hash "14wcb4fnhdz43bdsl52p36p8w4g2652x49hb0f4xvmxjw9bp000c")))

