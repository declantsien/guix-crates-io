(define-module (crates-io bc ar) #:use-module (crates-io))

(define-public crate-bcar-0.1 (crate (name "bcar") (vers "0.1.0") (hash "14w0rwhzygk4c0asp7rgzhjdax7j3yayabma89j6xhf23h8f0q89")))

(define-public crate-bcar-0.2 (crate (name "bcar") (vers "0.2.0") (hash "05dw9va9fk9z7cvrxh5b5ys8zfwjkj3fkjr2yvq9bnranvfiambz")))

(define-public crate-bcar-0.2 (crate (name "bcar") (vers "0.2.1") (hash "13zlbhd0b3j5bn3789vkyqhky4vxdvlqwnrid37258kpcdvw9jcs")))

