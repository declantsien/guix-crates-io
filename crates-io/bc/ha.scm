(define-module (crates-io bc ha) #:use-module (crates-io))

(define-public crate-bchannel-0.0.1 (crate (name "bchannel") (vers "0.0.1") (hash "10nqfvac056s1lpvnmv5bii2a3xl2cnsf0hvh3qylr6rz5xjgfss")))

(define-public crate-bchannel-0.0.2 (crate (name "bchannel") (vers "0.0.2") (hash "1qrn4arhrvgj268mlbrph657qv2j0gcbdr119c6gmyam7r442qcq")))

(define-public crate-bchannel-0.0.3 (crate (name "bchannel") (vers "0.0.3") (hash "031y15wci41i0bpz2d5hgh0fpc8r07np9n252sm18v9r521havlx")))

(define-public crate-bchannel-0.0.4 (crate (name "bchannel") (vers "0.0.4") (hash "0snmmrswnyl5sjbx6g9rcq9mwhsnbmkb6j5z1qxf3p4gar841rlw")))

(define-public crate-bchannel-0.0.5 (crate (name "bchannel") (vers "0.0.5") (hash "15sl9r3bbmcr5n3g4axmw1m4di63cp093blq13i5klnghfgj5b3n")))

(define-public crate-bchannel-0.0.6 (crate (name "bchannel") (vers "0.0.6") (hash "1wz2ddzb71jnjb5a3nydl59zlb4iq00c8xjh2rmvc10h6micp24s")))

(define-public crate-bchannel-0.0.7 (crate (name "bchannel") (vers "0.0.7") (hash "188mxpa74axl9l3f2qrppw2lyq1c4021p6gxq05cnc9nzd3n46yw")))

(define-public crate-bchannel-0.0.8 (crate (name "bchannel") (vers "0.0.8") (hash "0gvgagzkipwb18q8pq4abs7asmvlvhkasgq0ibzq7d8yva42v41h")))

(define-public crate-bchannel-0.0.9 (crate (name "bchannel") (vers "0.0.9") (hash "18fn0k4v8y0iywpvmhf45hv0qxxmzpyyayjdacdkhxxzjpg92gwy")))

(define-public crate-bchannel-0.0.10 (crate (name "bchannel") (vers "0.0.10") (hash "1xn118m4kzdj22njyadqvv9s53h5il4n9hs9bs7l9yzpmrqk917z")))

