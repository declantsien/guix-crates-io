(define-module (crates-io bc nd) #:use-module (crates-io))

(define-public crate-bcndecode-0.1 (crate (name "bcndecode") (vers "0.1.0") (deps (list (crate-dep (name "gcc") (req "^0.3") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2.26") (default-features #t) (kind 0)))) (hash "07l3l8pzrjx02sqynpviqi5hdkw28yjx36jgaxjzxji07r7l19cq")))

(define-public crate-bcndecode-0.2 (crate (name "bcndecode") (vers "0.2.0") (deps (list (crate-dep (name "gcc") (req "^0.3") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2.26") (default-features #t) (kind 2)))) (hash "1k9qvahz10xy3pp82pzzi31q623fh639g80m4pm86dwrnvirjnyn") (features (quote (("test"))))))

(define-public crate-bcndecode-sys-0.1 (crate (name "bcndecode-sys") (vers "0.1.0") (deps (list (crate-dep (name "bindgen") (req "^0.53.1") (default-features #t) (kind 1)) (crate-dep (name "cc") (req "^1.0") (default-features #t) (kind 1)))) (hash "1chain323pg1p27kqwr2r0yhlpcs4fwwmjn61kpryknx9yz9rhkv")))

(define-public crate-bcndecode-sys-0.1 (crate (name "bcndecode-sys") (vers "0.1.1") (deps (list (crate-dep (name "cc") (req "^1.0") (default-features #t) (kind 1)))) (hash "0s518il4cxx1fipii2kzycq66kv48x6im49517zxvgv0klpgwr4i")))

