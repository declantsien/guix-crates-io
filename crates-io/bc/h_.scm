(define-module (crates-io bc h_) #:use-module (crates-io))

(define-public crate-bch_addr-0.1 (crate (name "bch_addr") (vers "0.1.0") (deps (list (crate-dep (name "bs58") (req "^0.2.2") (features (quote ("check"))) (default-features #t) (kind 0)) (crate-dep (name "cash_addr") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "failure") (req "^0.1.5") (default-features #t) (kind 0)))) (hash "0mgsj5vdb2vy96c11nm0l3dafih275fxa55j0rday57pnjvnnpks")))

