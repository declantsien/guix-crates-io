(define-module (crates-io bc sv) #:use-module (crates-io))

(define-public crate-bcsv-0.1 (crate (name "bcsv") (vers "0.1.0") (deps (list (crate-dep (name "base_core_socialist_values") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^2.33.0") (default-features #t) (kind 0)))) (hash "12dkn29xamicb5dw14pslvdq0klnr9905fw4s8lrj34x1fm5mghc")))

(define-public crate-bcsv-0.1 (crate (name "bcsv") (vers "0.1.1") (deps (list (crate-dep (name "base_core_socialist_values") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^2.33.0") (default-features #t) (kind 0)))) (hash "1hlxdy3k2y804d3r2pdp3fflan6jy9bqnpfwmz7pci19pxlsns7k")))

(define-public crate-bcsv-0.1 (crate (name "bcsv") (vers "0.1.2") (deps (list (crate-dep (name "base_core_socialist_values") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^2.33.0") (default-features #t) (kind 0)))) (hash "1n0lgbdy5llnb53lcyhnim0yxph7k4rja5hmijpzmdrz85c7ksqy")))

(define-public crate-bcsv-0.1 (crate (name "bcsv") (vers "0.1.3") (deps (list (crate-dep (name "base_core_socialist_values") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^2.33.0") (default-features #t) (kind 0)))) (hash "0g99lg4qgzgwrv3pbjwfpcwfm4wqcvn89k3fs1vd6gjh1ivh1v9w")))

(define-public crate-bcsv-0.1 (crate (name "bcsv") (vers "0.1.4") (deps (list (crate-dep (name "base_core_socialist_values") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^2.33.0") (default-features #t) (kind 0)))) (hash "0808x7pbdyhw54df7fb1nqyfsxm85rzdqib1ll1nlra11i1klncv")))

(define-public crate-bcsv-0.1 (crate (name "bcsv") (vers "0.1.5") (deps (list (crate-dep (name "base_core_socialist_values") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^2.33.0") (default-features #t) (kind 0)))) (hash "1g8j6j8yb5i0p1g2wl6hwxl419jzfm44gv9p9pgnhmkqd2jhqghk")))

