(define-module (crates-io bc mp) #:use-module (crates-io))

(define-public crate-bcmp-0.1 (crate (name "bcmp") (vers "0.1.0") (deps (list (crate-dep (name "bytepack") (req "^0.3.1") (default-features #t) (kind 0)))) (hash "0p62qjpgykclq95wk7bky61fw3i9nln4yx67cv20crlh1qhh7b06")))

(define-public crate-bcmp-0.2 (crate (name "bcmp") (vers "0.2.0") (deps (list (crate-dep (name "bytepack") (req "^0.3.1") (default-features #t) (kind 0)))) (hash "0cqn33s7z8ysdgnmm2algx30w9chi5f7y9jcxwa0dk1rl60qmlmy")))

(define-public crate-bcmp-0.3 (crate (name "bcmp") (vers "0.3.0") (deps (list (crate-dep (name "bytepack") (req "^0.3.1") (default-features #t) (kind 0)))) (hash "1v12q8s1bha92xi0w6figqawl60wy44f16ynphgrdllfxz2n9nq5")))

(define-public crate-bcmp-0.3 (crate (name "bcmp") (vers "0.3.1") (deps (list (crate-dep (name "bytepack") (req "^0.3.1") (default-features #t) (kind 0)))) (hash "0lnyc4ams9ziskd83bwwplnivajbwi94f50vjj7izzhccrnvi1lm")))

(define-public crate-bcmp-0.3 (crate (name "bcmp") (vers "0.3.2") (deps (list (crate-dep (name "bytepack") (req "^0.3.1") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.3") (default-features #t) (kind 2)))) (hash "15jv2x4xz824k8jpc973khh418a4w6iwqvvjqz231h6kmynccb7g")))

(define-public crate-bcmp-0.4 (crate (name "bcmp") (vers "0.4.0") (deps (list (crate-dep (name "bytepack") (req "^0.3.1") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.3") (default-features #t) (kind 2)))) (hash "0wzfw6rbwyv8g5mhrsa76r8inixh673vyx8cdjs3130ma9374lv4")))

(define-public crate-bcmp-0.4 (crate (name "bcmp") (vers "0.4.1") (deps (list (crate-dep (name "bytepack") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.3") (default-features #t) (kind 2)))) (hash "1cncnn2svwshivfqllcvd74x52knfv2g4xdp5v7hg1hajqmcswrd")))

