(define-module (crates-io bc e-) #:use-module (crates-io))

(define-public crate-bce-ocr-2022 (crate (name "bce-ocr") (vers "2022.6.18-0") (deps (list (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.81") (default-features #t) (kind 0)) (crate-dep (name "serde_repr") (req "^0.1.8") (default-features #t) (kind 0)) (crate-dep (name "serde_url_params") (req "^0.2.1") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.31") (default-features #t) (kind 0)) (crate-dep (name "ureq") (req "^2.4.0") (default-features #t) (kind 0)))) (hash "0zx4z12bf18933s7m282glz2j240ahgqasasjllz3r7vnzij3wpc")))

