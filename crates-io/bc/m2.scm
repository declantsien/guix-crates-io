(define-module (crates-io bc m2) #:use-module (crates-io))

(define-public crate-bcm2709-spi-0.1 (crate (name "bcm2709-spi") (vers "0.1.0") (deps (list (crate-dep (name "libc") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "vcell") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "11mv7621gbzmz81yxzwdc7isamxlg7bfxz17bj2sdz7rvsimyf6a")))

(define-public crate-bcm2709-spi-0.1 (crate (name "bcm2709-spi") (vers "0.1.1") (deps (list (crate-dep (name "libc") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "vcell") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "1vcdg3j7090g81gpdfv0r1kb4mycbxvzd35q7vk2s8fqhj3dcx7i")))

(define-public crate-bcm2711-lpa-0.1 (crate (name "bcm2711-lpa") (vers "0.1.0") (deps (list (crate-dep (name "critical-section") (req "^1.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "portable-atomic") (req "^0.3.16") (kind 0)) (crate-dep (name "vcell") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "17z6cd745ismdb38v2x2b18s54364xnca6c3niqc352qd3jh8gda") (features (quote (("rt")))) (rust-version "1.61.0")))

(define-public crate-bcm2711-lpa-0.2 (crate (name "bcm2711-lpa") (vers "0.2.0") (deps (list (crate-dep (name "critical-section") (req "^1.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "portable-atomic") (req "^0.3.16") (kind 0)) (crate-dep (name "vcell") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "0np2zicpnq3s0clrisgd5m9d3rs6yc1hji81h90jhsh19gbi3d1i") (features (quote (("rt")))) (rust-version "1.65.0")))

(define-public crate-bcm2711-lpa-0.2 (crate (name "bcm2711-lpa") (vers "0.2.1") (deps (list (crate-dep (name "critical-section") (req "^1.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "portable-atomic") (req "^0.3.16") (kind 0)) (crate-dep (name "vcell") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "11pfd77j9gwhss64jc5pbd11b6m4hgkc7l42zy3yfjspcgfavm00") (features (quote (("rt")))) (rust-version "1.65.0")))

(define-public crate-bcm2711-lpa-0.2 (crate (name "bcm2711-lpa") (vers "0.2.2") (deps (list (crate-dep (name "critical-section") (req "^1.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "portable-atomic") (req "^0.3.16") (kind 0)) (crate-dep (name "vcell") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "1cpw3h6jjs54kjpkia4z1gnihnfsdibid6bcdjd74zm4pl19bwki") (features (quote (("rt")))) (rust-version "1.65.0")))

(define-public crate-bcm2711-lpa-0.3 (crate (name "bcm2711-lpa") (vers "0.3.0") (deps (list (crate-dep (name "critical-section") (req "^1.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "portable-atomic") (req "^0.3.16") (kind 0)) (crate-dep (name "vcell") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "1398hqa54wq89va8bdyn0xdz35zd57dmzn8a58gw44w7ipcpv039") (features (quote (("rt")))) (rust-version "1.65.0")))

(define-public crate-bcm2711-lpa-0.4 (crate (name "bcm2711-lpa") (vers "0.4.0") (deps (list (crate-dep (name "critical-section") (req "^1.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "portable-atomic") (req "^0.3.16") (kind 0)) (crate-dep (name "vcell") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "0sgsjd29dsa7c36d7x353zii9ymasd4dfdqw8kmk2f2ry90f3g3n") (features (quote (("rt")))) (rust-version "1.65.0")))

(define-public crate-bcm2835-lpa-0.1 (crate (name "bcm2835-lpa") (vers "0.1.0") (deps (list (crate-dep (name "critical-section") (req "^1.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "portable-atomic") (req "^0.3.16") (kind 0)) (crate-dep (name "vcell") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "14md03xqm0w5h2dq5pszxk818pw2kisadpsinyi0h17rcdqjb0mc") (features (quote (("rt")))) (rust-version "1.61.0")))

(define-public crate-bcm2835-lpa-0.2 (crate (name "bcm2835-lpa") (vers "0.2.0") (deps (list (crate-dep (name "critical-section") (req "^1.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "portable-atomic") (req "^0.3.16") (kind 0)) (crate-dep (name "vcell") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "1hcqhff3i83q1n42xv88vwvp3c62b78mvddb25rm3kls1l3sr8fq") (features (quote (("rt")))) (rust-version "1.65.0")))

(define-public crate-bcm2835-lpa-0.2 (crate (name "bcm2835-lpa") (vers "0.2.1") (deps (list (crate-dep (name "critical-section") (req "^1.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "portable-atomic") (req "^0.3.16") (kind 0)) (crate-dep (name "vcell") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "1c1c0qrklgpyc63l5idazxn96zjk3lz3f6sq4fzx35zbg7lajmi5") (features (quote (("rt")))) (rust-version "1.65.0")))

(define-public crate-bcm2835-lpa-0.2 (crate (name "bcm2835-lpa") (vers "0.2.2") (deps (list (crate-dep (name "critical-section") (req "^1.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "portable-atomic") (req "^0.3.16") (kind 0)) (crate-dep (name "vcell") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "1jy4qrxvf8f661h9lhprh9dkfccaqp3dynm14fr3m7nfqc1az0q8") (features (quote (("rt")))) (rust-version "1.65.0")))

(define-public crate-bcm2835-lpa-0.3 (crate (name "bcm2835-lpa") (vers "0.3.0") (deps (list (crate-dep (name "critical-section") (req "^1.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "portable-atomic") (req "^0.3.16") (kind 0)) (crate-dep (name "vcell") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "08r4sr2ryva7nl9qsscdr8jcjyh6qq7c9i750m9xm4hs81w01cvz") (features (quote (("rt")))) (rust-version "1.65.0")))

(define-public crate-bcm2835-lpa-0.4 (crate (name "bcm2835-lpa") (vers "0.4.0") (deps (list (crate-dep (name "critical-section") (req "^1.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "portable-atomic") (req "^0.3.16") (kind 0)) (crate-dep (name "vcell") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "15rsxanjpf7jgmnjj4pjvv7x60r4bjgxw7jgav53c1z97s60r4cp") (features (quote (("rt")))) (rust-version "1.65.0")))

(define-public crate-bcm2837-0.1 (crate (name "bcm2837") (vers "0.1.0") (hash "0s7ykkiq97qzgczs7a6c0aq9lgx7h8gcrij0hfbkksg6ba8rvwvz")))

(define-public crate-bcm2837-lpa-0.1 (crate (name "bcm2837-lpa") (vers "0.1.0") (deps (list (crate-dep (name "critical-section") (req "^1.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "portable-atomic") (req "^0.3.16") (kind 0)) (crate-dep (name "vcell") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "1rg13d552knc1imsiv9sqg4sxhq8w8jryf5w0x3q5ah3lngmaj2f") (features (quote (("rt")))) (rust-version "1.61.0")))

(define-public crate-bcm2837-lpa-0.2 (crate (name "bcm2837-lpa") (vers "0.2.0") (deps (list (crate-dep (name "critical-section") (req "^1.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "portable-atomic") (req "^0.3.16") (kind 0)) (crate-dep (name "vcell") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "0smld5cfa0j438a6py2zvs16cfpnpq35kj147l7mk6wvl1b17k4s") (features (quote (("rt")))) (rust-version "1.65.0")))

(define-public crate-bcm2837-lpa-0.2 (crate (name "bcm2837-lpa") (vers "0.2.1") (deps (list (crate-dep (name "critical-section") (req "^1.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "portable-atomic") (req "^0.3.16") (kind 0)) (crate-dep (name "vcell") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "1s6361g5xl46fy0brbhqymn1c262hv53dbcq5h415pbgq1gfa5fj") (features (quote (("rt")))) (rust-version "1.65.0")))

(define-public crate-bcm2837-lpa-0.2 (crate (name "bcm2837-lpa") (vers "0.2.2") (deps (list (crate-dep (name "critical-section") (req "^1.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "portable-atomic") (req "^0.3.16") (kind 0)) (crate-dep (name "vcell") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "116845bjspb2d2xxb2qnk8aw4z26hk5qhzg2rs7d9qym4rhnc7sy") (features (quote (("rt")))) (rust-version "1.65.0")))

(define-public crate-bcm2837-lpa-0.3 (crate (name "bcm2837-lpa") (vers "0.3.0") (deps (list (crate-dep (name "critical-section") (req "^1.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "portable-atomic") (req "^0.3.16") (kind 0)) (crate-dep (name "vcell") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "0jjihw4j6qhng1kx2k2s6nbcqblda9d0bz8631bfc32dd8fq27p8") (features (quote (("rt")))) (rust-version "1.65.0")))

(define-public crate-bcm2837-lpa-0.4 (crate (name "bcm2837-lpa") (vers "0.4.0") (deps (list (crate-dep (name "critical-section") (req "^1.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "portable-atomic") (req "^0.3.16") (kind 0)) (crate-dep (name "vcell") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "1bh78r440njlqml7fa8n3q288aawq5gkhn2968ahdln3vxv708lk") (features (quote (("rt")))) (rust-version "1.65.0")))

(define-public crate-bcm283x-linux-gpio-0.2 (crate (name "bcm283x-linux-gpio") (vers "0.2.0") (deps (list (crate-dep (name "nix") (req "^0.13") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "yansi") (req "^0.5") (default-features #t) (kind 0)))) (hash "1l66rnb8gca6ps1knavrcwgh5cqvys2dkfph7x9gd77llh6gq51h")))

(define-public crate-bcm283x-linux-gpio-mod-0.3 (crate (name "bcm283x-linux-gpio-mod") (vers "0.3.0") (deps (list (crate-dep (name "nix") (req "^0.24.1") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.3.26") (default-features #t) (kind 0)) (crate-dep (name "yansi") (req "^0.5.1") (default-features #t) (kind 0)))) (hash "0fgl7wnzj3sk514c9b8jxmxv6d9s353sar53qnr8l83vabc45381")))

