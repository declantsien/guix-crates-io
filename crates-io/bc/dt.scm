(define-module (crates-io bc dt) #:use-module (crates-io))

(define-public crate-bcdt-0.1 (crate (name "bcdt") (vers "0.1.0") (deps (list (crate-dep (name "time") (req "^0.3.12") (default-features #t) (kind 0)))) (hash "0w3n80kvlni9q6cr37sb58rknvmahbapr3v9hnn55mwwvfq64ay7")))

(define-public crate-bcdt-0.1 (crate (name "bcdt") (vers "0.1.1") (deps (list (crate-dep (name "time") (req "^0.3.12") (default-features #t) (kind 0)))) (hash "180chq4bhixghiwi8xf501n5dalfsm61dp1mdcaink74ryiazvhr")))

