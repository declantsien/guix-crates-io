(define-module (crates-io bc by) #:use-module (crates-io))

(define-public crate-bcbypass-1 (crate (name "bcbypass") (vers "1.0.0") (hash "0bagfdk97g8335pjxyzlaad636i5ln2iyl1mq61sgwy6j5mhl1rb")))

(define-public crate-bcbypass-2 (crate (name "bcbypass") (vers "2.0.0") (hash "086djj7fr19xbaqrnsl61wxdjx0xzl1jw5hc9krkj85gd50mbq4v")))

(define-public crate-bcbypass-2 (crate (name "bcbypass") (vers "2.0.1") (hash "1gnvdi9ymwfsfd3mp5qs8y1zfv3hp89059d6w6x7jlpfs12c696v")))

(define-public crate-bcbypass-2 (crate (name "bcbypass") (vers "2.0.2") (hash "1a8hyp28m159y8pfa6gs4xn6fxw76fidiv6svngnk6xkmqh1gwrh")))

(define-public crate-bcbypass-2 (crate (name "bcbypass") (vers "2.1.0") (hash "0z104q78f87nlc365ij7qvnj2fm20kw1rpvvr6kgb9blbq3nyr4p")))

