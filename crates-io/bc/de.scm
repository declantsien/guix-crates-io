(define-module (crates-io bc de) #:use-module (crates-io))

(define-public crate-bcdec_rs-0.1 (crate (name "bcdec_rs") (vers "0.1.0") (hash "0705c5i4dbrhlbc561rzy3999ccaskym55wzq850n59csfpvr0g4")))

(define-public crate-bcdec_rs-0.1 (crate (name "bcdec_rs") (vers "0.1.1") (hash "0l4163jj0617n8vd4xymhb3v6w1v4c7p5gqf54c658x5r90h2f3q")))

(define-public crate-bcder-0.1 (crate (name "bcder") (vers "0.1.0") (deps (list (crate-dep (name "bytes") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "failure") (req "^0.1") (default-features #t) (kind 0)))) (hash "0msd20pn5l2hkza56aqjxilki31wy9ybxcza355rdk6yfak9wg2z") (features (quote (("extra-debug"))))))

(define-public crate-bcder-0.2 (crate (name "bcder") (vers "0.2.0") (deps (list (crate-dep (name "bytes") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "derive_more") (req "^0.14") (default-features #t) (kind 0)))) (hash "0y883mn9b1ajsymvdfwh56irihdfyq1h8bb8bvqppa4fmdfsff1k") (features (quote (("extra-debug"))))))

(define-public crate-bcder-0.2 (crate (name "bcder") (vers "0.2.1") (deps (list (crate-dep (name "bytes") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "derive_more") (req "^0.14") (default-features #t) (kind 0)))) (hash "1233mnkb33daxslhjj0h22iab6bh9m9syrsl0wcwvavrmbmcj1nm") (features (quote (("extra-debug"))))))

(define-public crate-bcder-0.3 (crate (name "bcder") (vers "0.3.0") (deps (list (crate-dep (name "backtrace") (req "^0.3.15") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "bytes") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "derive_more") (req "^0.14") (default-features #t) (kind 0)))) (hash "03h6islblrm6g4lrdnw837l83zfafmwfk47kll13jmmrgq1pmy9y") (features (quote (("extra-debug" "backtrace"))))))

(define-public crate-bcder-0.3 (crate (name "bcder") (vers "0.3.1") (deps (list (crate-dep (name "backtrace") (req "^0.3.15") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "bytes") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "derive_more") (req "^0.15") (default-features #t) (kind 0)))) (hash "15dlnxnhav0prnnlyb4rj2vpac26vw3a3963gr83grjs59zrbgdw") (features (quote (("extra-debug" "backtrace"))))))

(define-public crate-bcder-0.2 (crate (name "bcder") (vers "0.2.2") (deps (list (crate-dep (name "bytes") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "derive_more") (req "^0.14") (default-features #t) (kind 0)))) (hash "00snaxl7lzfn19680c9wk4h4fa8li11a11n16bnnjm4q74n93k6y") (features (quote (("extra-debug"))))))

(define-public crate-bcder-0.4 (crate (name "bcder") (vers "0.4.0") (deps (list (crate-dep (name "backtrace") (req "^0.3.15") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "bytes") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "smallvec") (req "^0.6.10") (default-features #t) (kind 0)) (crate-dep (name "unwrap") (req "^1.2.1") (default-features #t) (kind 0)))) (hash "04xbxc54bs857pmylgi9q0j3r6yf9hvnbhixib0i4swxrlbnxaz1") (features (quote (("extra-debug" "backtrace"))))))

(define-public crate-bcder-0.4 (crate (name "bcder") (vers "0.4.1") (deps (list (crate-dep (name "backtrace") (req "^0.3.15") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "bytes") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "smallvec") (req "^0.6.10") (default-features #t) (kind 0)) (crate-dep (name "unwrap") (req "^1.2.1") (default-features #t) (kind 0)))) (hash "03smg3w80zjggz0zxblypqxwwhhjg0xz0vcsdx8m066ys0q3xmmm") (features (quote (("extra-debug" "backtrace"))))))

(define-public crate-bcder-0.4 (crate (name "bcder") (vers "0.4.2") (deps (list (crate-dep (name "backtrace") (req "^0.3.15") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "bytes") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "smallvec") (req "^0.6.10") (default-features #t) (kind 0)) (crate-dep (name "unwrap") (req "^1.2.1") (default-features #t) (kind 0)))) (hash "0xhdyjrxd2lv3ah6qc6kycf0qdb420m8mgw80yhy5n5gmkwkn7hh") (features (quote (("extra-debug" "backtrace"))))))

(define-public crate-bcder-0.5 (crate (name "bcder") (vers "0.5.0") (deps (list (crate-dep (name "backtrace") (req "^0.3.15") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "bytes") (req "^0.5") (default-features #t) (kind 0)) (crate-dep (name "smallvec") (req "^1.1") (default-features #t) (kind 0)) (crate-dep (name "unwrap") (req "^1.2.1") (default-features #t) (kind 0)))) (hash "1srx0fnfgcp68p0l8qvpjbx3mkrjzfralm23nhm7yzcrv7zk88jx") (features (quote (("extra-debug" "backtrace"))))))

(define-public crate-bcder-0.5 (crate (name "bcder") (vers "0.5.1") (deps (list (crate-dep (name "backtrace") (req "^0.3.15") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "bytes") (req "^0.5") (default-features #t) (kind 0)) (crate-dep (name "smallvec") (req "^1.1") (default-features #t) (kind 0)) (crate-dep (name "unwrap") (req "^1.2.1") (default-features #t) (kind 0)))) (hash "0wir1xcxr4907rgnwz2l9lkjzpv73y2znkv2zfrq3nkspydfappi") (features (quote (("extra-debug" "backtrace"))))))

(define-public crate-bcder-0.6 (crate (name "bcder") (vers "0.6.0") (deps (list (crate-dep (name "backtrace") (req "^0.3.15") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "bytes") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "smallvec") (req "^1.1") (default-features #t) (kind 0)))) (hash "0nwwbnc23sh89f6vwghmrh3zlrssfxrymcr9h4cggvjxkamvhvn1") (features (quote (("extra-debug" "backtrace"))))))

(define-public crate-bcder-0.6 (crate (name "bcder") (vers "0.6.1") (deps (list (crate-dep (name "backtrace") (req "^0.3.15") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "bytes") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "smallvec") (req "^1.1") (default-features #t) (kind 0)))) (hash "0f8fkhrbafc7jsvvlhbsv4mxshbxggvz32h23ddyq0bvwnxc7rsj") (features (quote (("extra-debug" "backtrace"))))))

(define-public crate-bcder-0.7 (crate (name "bcder") (vers "0.7.0") (deps (list (crate-dep (name "bytes") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "smallvec") (req "^1.1") (default-features #t) (kind 0)))) (hash "0lgahjmpki63b7gwa3qj6m3m1lx64p068w8wj4cx5xwfzfndh1zh")))

(define-public crate-bcder-0.7 (crate (name "bcder") (vers "0.7.1") (deps (list (crate-dep (name "bytes") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "smallvec") (req "^1.1") (default-features #t) (kind 0)))) (hash "16nsx3d7bpr68j7gyhdmjbvasav6alxmahrwfa63zvja1pfbgpv9")))

(define-public crate-bcder-0.7 (crate (name "bcder") (vers "0.7.2") (deps (list (crate-dep (name "bytes") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "smallvec") (req "^1.1") (default-features #t) (kind 0)))) (hash "1nd23nfxsgwd6ah2227ysbgnnmp7mbp9m1yaya361wssg4cz09mb")))

(define-public crate-bcder-0.7 (crate (name "bcder") (vers "0.7.3") (deps (list (crate-dep (name "bytes") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "smallvec") (req "^1.1") (default-features #t) (kind 0)))) (hash "0ixwrgwg7ggzdk709i35yl8wybs5xw29j6b1nv52bspqj34vw5mz")))

(define-public crate-bcder-0.7 (crate (name "bcder") (vers "0.7.4") (deps (list (crate-dep (name "bytes") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "smallvec") (req "^1.6.1") (default-features #t) (kind 0)))) (hash "1w3fj0y3fbjb3bd42nl7b22kg0cl6240v69mxf5v7aklcxx789y6")))

