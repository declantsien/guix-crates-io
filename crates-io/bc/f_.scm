(define-module (crates-io bc f_) #:use-module (crates-io))

(define-public crate-bcf_reader-0.1 (crate (name "bcf_reader") (vers "0.1.0") (deps (list (crate-dep (name "byteorder") (req "^1.4.3") (default-features #t) (kind 0)) (crate-dep (name "flate2") (req "^1.0.28") (default-features #t) (kind 0)))) (hash "1v8rpa51qsxhbcji2p15ywhxwfvbs1fqaj9x2jbpid3j7ipc9946") (yanked #t)))

(define-public crate-bcf_reader-0.1 (crate (name "bcf_reader") (vers "0.1.1") (deps (list (crate-dep (name "byteorder") (req "^1.4.3") (default-features #t) (kind 0)) (crate-dep (name "flate2") (req "^1.0.28") (default-features #t) (kind 0)))) (hash "011jjh54adj7r62v93f1mz5y7xivaklr275hlkg26dggkym77r77") (yanked #t)))

(define-public crate-bcf_reader-0.1 (crate (name "bcf_reader") (vers "0.1.2") (deps (list (crate-dep (name "byteorder") (req "^1.4.3") (default-features #t) (kind 0)) (crate-dep (name "flate2") (req "^1.0.28") (default-features #t) (kind 0)))) (hash "0grxikylzn3zb0a3hgyh1ky50g5xq87vjgw4x132sq4ia641ndfa") (yanked #t)))

(define-public crate-bcf_reader-0.1 (crate (name "bcf_reader") (vers "0.1.3") (deps (list (crate-dep (name "byteorder") (req "^1.4.3") (default-features #t) (kind 0)) (crate-dep (name "flate2") (req "^1.0.28") (default-features #t) (kind 0)))) (hash "1isnpzjc9baxkmq5y446fhm8ysa7li00c0gwkbjj46dfmrg2mywd")))

(define-public crate-bcf_reader-0.1 (crate (name "bcf_reader") (vers "0.1.4") (deps (list (crate-dep (name "byteorder") (req "^1.4.3") (default-features #t) (kind 0)) (crate-dep (name "flate2") (req "^1.0.28") (default-features #t) (kind 0)) (crate-dep (name "rayon") (req "^1.10.0") (default-features #t) (kind 0)))) (hash "0nfnjvw0j6ydk06hq5iw75m11dz2gjzc1kwqk71ism7f0xwk8l2g")))

(define-public crate-bcf_reader-0.2 (crate (name "bcf_reader") (vers "0.2.1") (deps (list (crate-dep (name "byteorder") (req "^1.4.3") (default-features #t) (kind 0)) (crate-dep (name "flate2") (req "^1.0.28") (default-features #t) (kind 0)) (crate-dep (name "rayon") (req "^1.10.0") (default-features #t) (kind 0)))) (hash "10j5w3rb32xwfd19ci2fc8xwd2f6yflfrvgbacxrpcw9cm6jby3p") (yanked #t)))

(define-public crate-bcf_reader-0.2 (crate (name "bcf_reader") (vers "0.2.2") (deps (list (crate-dep (name "byteorder") (req "^1.4.3") (default-features #t) (kind 0)) (crate-dep (name "flate2") (req "^1.0.28") (default-features #t) (kind 0)) (crate-dep (name "rayon") (req "^1.10.0") (default-features #t) (kind 0)))) (hash "0rn56mbhixbnc30lgxm48m6n4mp9j0i6m1b8lcq3vq39m06rn8yi") (features (quote (("zlib-ng-compat" "flate2/zlib-ng-compat") ("zlib" "flate2/zlib")))) (yanked #t)))

(define-public crate-bcf_reader-0.2 (crate (name "bcf_reader") (vers "0.2.3") (deps (list (crate-dep (name "byteorder") (req "^1.4.3") (default-features #t) (kind 0)) (crate-dep (name "flate2") (req "^1.0.28") (default-features #t) (kind 0)) (crate-dep (name "rayon") (req "^1.10.0") (default-features #t) (kind 0)))) (hash "0g5qnr6ljbnpilimnhyprnpp45g9qgycsf8zyhfh5k5p7ccixzpb") (features (quote (("zlib-ng-compat" "flate2/zlib-ng-compat") ("zlib" "flate2/zlib"))))))

