(define-module (crates-io oj o_) #:use-module (crates-io))

(define-public crate-ojo_diff-0.1 (crate (name "ojo_diff") (vers "0.1.0") (deps (list (crate-dep (name "proptest") (req "^0.8") (default-features #t) (kind 2)))) (hash "1jvfkrz4lhx8kdgd9mlgiv8ai298vyjnyc5k75qymi1fhwbk79am")))

(define-public crate-ojo_graph-0.1 (crate (name "ojo_graph") (vers "0.1.0") (deps (list (crate-dep (name "itertools") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "proptest") (req "^0.8") (default-features #t) (kind 2)))) (hash "0bf0v94ybll2sidgjzs3w3dp2zabmrg91yzxw0cx441ym2wa2mzq")))

(define-public crate-ojo_multimap-0.1 (crate (name "ojo_multimap") (vers "0.1.0") (deps (list (crate-dep (name "serde") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_yaml") (req "^0.7") (default-features #t) (kind 2)))) (hash "1k56mmz9d3vy33hnlc2m0xyb5mn8399n7yrqp1nxj01pwyw5cs4g")))

(define-public crate-ojo_partition-0.1 (crate (name "ojo_partition") (vers "0.1.0") (deps (list (crate-dep (name "ojo_multimap") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0") (default-features #t) (kind 0)))) (hash "0yc2y8h9mj6dzy8k6wi1x5dwl8698d7ca21a4n0wq89yfvw3ykx5")))

(define-public crate-ojo_wasm-0.1 (crate (name "ojo_wasm") (vers "0.1.0") (deps (list (crate-dep (name "console_log") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "libojo") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "ojo_graph") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "wasm-bindgen") (req "^0.2") (features (quote ("serde-serialize"))) (default-features #t) (kind 0)))) (hash "01k0wsiabckm3i1gkqndgzrdm5v0mnwhimkqc7mgk1dwckz0mw63")))

