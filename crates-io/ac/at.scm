(define-module (crates-io ac at) #:use-module (crates-io))

(define-public crate-acat-0.1 (crate (name "acat") (vers "0.1.1") (deps (list (crate-dep (name "rand") (req "^0.7.2") (default-features #t) (kind 0)))) (hash "0i31a1jcd0wd8281m2n6ngwn4rpshrpqxpg9b3cbnjv0ynpw24ph")))

(define-public crate-acat-0.1 (crate (name "acat") (vers "0.1.2") (deps (list (crate-dep (name "clap") (req "^2.33.1") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.7.2") (default-features #t) (kind 0)) (crate-dep (name "random_color") (req "^0.4.4") (default-features #t) (kind 0)))) (hash "12na2ymccz8d20dk8a6pwjf5waq6jchxam0g3pz941ij592fb63i")))

(define-public crate-Acatsama0871-0.1 (crate (name "Acatsama0871") (vers "0.1.0") (hash "0vpzp5qcwd4nc9yra4ffkk8zh3yp2zczvrm2cgdcc81cvd10rfc3")))

