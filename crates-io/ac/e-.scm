(define-module (crates-io ac e-) #:use-module (crates-io))

(define-public crate-ace-test-lib-0.1 (crate (name "ace-test-lib") (vers "0.1.0") (deps (list (crate-dep (name "rust-crypto") (req "^0.2.36") (default-features #t) (kind 0)))) (hash "00dkq8xyqsiihzlvg5qkl5ps69n6v5pparmk2spq2lq3yf0490an")))

