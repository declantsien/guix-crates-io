(define-module (crates-io ac ke) #:use-module (crates-io))

(define-public crate-ackerman-0.1 (crate (name "ackerman") (vers "0.1.1") (deps (list (crate-dep (name "lsp-types") (req "^0.93.1") (features (quote ("proposed"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "num") (req "^0.4.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "peginator") (req "^0.3.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "ropey") (req "^1.5.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "ucd-trie") (req "^0.1.5") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "url") (req "^2.3.1") (default-features #t) (kind 0)))) (hash "06615q9q8gnq81c3kh28v676f2psphjpnhddm2hxydfaysrxihk7") (features (quote (("default"))))))

