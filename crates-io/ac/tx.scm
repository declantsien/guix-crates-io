(define-module (crates-io ac tx) #:use-module (crates-io))

(define-public crate-actx-0.0.1 (crate (name "actx") (vers "0.0.1") (hash "16rw5aiy1j5c7k28aj9i7cb8hq54pjbc8qy521cdhlq2rrqrjlm1")))

(define-public crate-actx-web-0.0.1 (crate (name "actx-web") (vers "0.0.1") (hash "1dkgdlmh2qsx1iq4qqhmcqiqsh3yd4hxy0mzz0fpsflpkj21f4ls")))

