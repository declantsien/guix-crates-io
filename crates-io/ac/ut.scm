(define-module (crates-io ac ut) #:use-module (crates-io))

(define-public crate-acute-0.0.1 (crate (name "acute") (vers "0.0.1") (deps (list (crate-dep (name "acute_app") (req "^0.1.1") (default-features #t) (kind 0)) (crate-dep (name "acute_assets") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "acute_core") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "acute_ecs") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "acute_input") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "acute_render") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "acute_render_backend") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "acute_scenes") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "acute_window") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "1qximh49mzx5d3xdka5w1j866rx6vq3c3fc46ddgc1bhsxv8x7gs")))

(define-public crate-acute-0.0.2 (crate (name "acute") (vers "0.0.2") (deps (list (crate-dep (name "acute_app") (req "^0.1.2") (default-features #t) (kind 0)) (crate-dep (name "acute_assets") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "acute_core") (req "^0.1.1") (default-features #t) (kind 0)) (crate-dep (name "acute_ecs") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "acute_input") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "acute_render") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "acute_render_backend") (req "^0.1.1") (default-features #t) (kind 0)) (crate-dep (name "acute_scenes") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "acute_window") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "0y82yx7brhfppd2imgp9hxbq4q35c8qg8krp5mq6h198mxcijmfa")))

(define-public crate-acute_ai-0.1 (crate (name "acute_ai") (vers "0.1.0") (hash "18sxg5gkl2y9q5vqz3n64qcyp3xy7sq92l5jxj365vm5yv7np0yj")))

(define-public crate-acute_animation-0.1 (crate (name "acute_animation") (vers "0.1.0") (hash "18vsip7gpjgphnr32njc1q3ncn1qpr79yhbbpc4n1hlv96y4b9zv")))

(define-public crate-acute_app-0.1 (crate (name "acute_app") (vers "0.1.0") (hash "1vwrqn85k8phcm57v8209alc76mdw5vqkppivckm7sdg94f9xiry")))

(define-public crate-acute_app-0.1 (crate (name "acute_app") (vers "0.1.1") (deps (list (crate-dep (name "acute_core") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "acute_ecs") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "acute_scenes") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "acute_window") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "futures") (req "^0.3.5") (default-features #t) (kind 0)))) (hash "18wgp2a26p41z8s0s6w58wfziqj0k4vmdn5wj3havnrqivacx868")))

(define-public crate-acute_app-0.1 (crate (name "acute_app") (vers "0.1.2") (deps (list (crate-dep (name "acute_core") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "acute_ecs") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "acute_render_backend") (req "^0.1.1") (default-features #t) (kind 0)) (crate-dep (name "acute_scenes") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "acute_window") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "futures") (req "^0.3.5") (default-features #t) (kind 0)))) (hash "12934zc8pq5ybkx3x0vjz8w0cfw9qlsgk1n99z6l0h4i0d14v3wx")))

(define-public crate-acute_app-0.1 (crate (name "acute_app") (vers "0.1.3") (deps (list (crate-dep (name "acute_core") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "acute_ecs") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "acute_input") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "acute_render_backend") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "acute_scenes") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "acute_window") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "futures") (req "^0.3.5") (default-features #t) (kind 0)))) (hash "0bs6ga48skjczx752rrbxlw62c53v0nwwdvq5sxv7w6k1hpsiv25")))

(define-public crate-acute_assets-0.1 (crate (name "acute_assets") (vers "0.1.0") (hash "0r6sddx7m2baifvhds7igi5yz7pl71p9pm0psh6spz104fz7k2qy")))

(define-public crate-acute_async-0.1 (crate (name "acute_async") (vers "0.1.0") (hash "1ilx43mgg25ypmzbpszp0nmxp13q1xrd4p0700hk0dlv1g8n5rd8")))

(define-public crate-acute_audio-0.1 (crate (name "acute_audio") (vers "0.1.0") (hash "1hpgp4qi2xcjcxi74c97afjkxyjphj4pxrh4mkwz8w0y44rk582q")))

(define-public crate-acute_core-0.1 (crate (name "acute_core") (vers "0.1.0") (deps (list (crate-dep (name "acute_ecs") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "rusty_timer") (req "^1.0.3") (default-features #t) (kind 0)))) (hash "0n57lp6gdnaabrx1lyd099n4ykpjd98jmsfc1m3k4rbqnhkb1x3z")))

(define-public crate-acute_core-0.1 (crate (name "acute_core") (vers "0.1.1") (deps (list (crate-dep (name "acute_ecs") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "rusty_timer") (req "^1.0.3") (default-features #t) (kind 0)))) (hash "127bk3s70hnm04cqsr0lqjr1lscbc3cpl2lpwxkvbyswdks66saf")))

(define-public crate-acute_ecs-0.1 (crate (name "acute_ecs") (vers "0.1.0") (deps (list (crate-dep (name "legion") (req "^0.2.4") (default-features #t) (kind 0)))) (hash "1bd1wd3a8kicwwdqwl0pknymb9zvjxacadnhrphnxi8lp1788mn4")))

(define-public crate-acute_imgui-0.1 (crate (name "acute_imgui") (vers "0.1.0") (hash "03r69wc4fij0w6zw1da8035m1nv60vhhb0ssq3q871mywa9l6saj")))

(define-public crate-acute_input-0.1 (crate (name "acute_input") (vers "0.1.0") (hash "06rk3skrdcjk4z5xx6vska8nq0q681jhnjjq7sd7zz3npd3mal1y")))

(define-public crate-acute_input-0.1 (crate (name "acute_input") (vers "0.1.2") (deps (list (crate-dep (name "acute_ecs") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "acute_window") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "dashmap") (req "^4.0.0-rc6") (default-features #t) (kind 0)))) (hash "0zni4rcwr4wxmk1a8gvswy3kg0sxs7xm3bmj18nwi51d436k1z85")))

(define-public crate-acute_math-0.1 (crate (name "acute_math") (vers "0.1.0") (hash "1jf0dg1w5c8j0m4gqg45ms25vixqh2b12r45zbx9ylna47h6wsqz")))

(define-public crate-acute_mesh-0.1 (crate (name "acute_mesh") (vers "0.1.0") (hash "0h6b3if9g39g2xrf6h4rwv2sm46r6hz8br2rcyjm7v3a3ib5416m")))

(define-public crate-acute_network-0.1 (crate (name "acute_network") (vers "0.1.0") (hash "127wpwn9j53lrc23iphn2isflny55s4kcdvhgphf8rzxhpp6jngc")))

(define-public crate-acute_nphysics-0.1 (crate (name "acute_nphysics") (vers "0.1.0") (hash "1brm09yy1r5i29q8bdlhr585al5f1x6251367jnkvi7n45pflsbr")))

(define-public crate-acute_particles-0.1 (crate (name "acute_particles") (vers "0.1.0") (hash "1nha4kd6bc558z37m51kdns3sd0hi37293k5dm8ghkyadafd9jw6")))

(define-public crate-acute_physics-0.1 (crate (name "acute_physics") (vers "0.1.0") (hash "1r2dhrjr5x9g3gg2qfwj7m154vpri5d4awgh9wzizkwfibb3s2hy")))

(define-public crate-acute_physics2d-0.1 (crate (name "acute_physics2d") (vers "0.1.0") (hash "1im1nvkwzrls65ikc2x8lg7jhdhcwpbkaijd1li48fbzfx9pzy9q")))

(define-public crate-acute_physics3d-0.1 (crate (name "acute_physics3d") (vers "0.1.0") (hash "1s0qh8jsarxgv0fg44j2h47z44v04d2jpsrcs9f3lrn4c54d6b36")))

(define-public crate-acute_render-0.1 (crate (name "acute_render") (vers "0.1.0") (hash "144cgnqmz6ys10x0yza666f4fclfg8lhm555wpjla3a48i8faljr")))

(define-public crate-acute_render_backend-0.1 (crate (name "acute_render_backend") (vers "0.1.0") (deps (list (crate-dep (name "wgpu") (req "^0.5.2") (default-features #t) (kind 0)))) (hash "0k22mvhnk713q34iqxiqziy2949sanbgiwx29igr3p905rgv83ml")))

(define-public crate-acute_render_backend-0.1 (crate (name "acute_render_backend") (vers "0.1.1") (deps (list (crate-dep (name "acute_ecs") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "acute_window") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "wgpu") (req "^0.5.2") (default-features #t) (kind 0)))) (hash "11clsvxr467jnb8q12biwd3q41xw088lnqdci38ijii2s7rrq8r8")))

(define-public crate-acute_render_backend-0.1 (crate (name "acute_render_backend") (vers "0.1.2") (deps (list (crate-dep (name "acute_ecs") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "acute_window") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "wgpu") (req "^0.5.2") (default-features #t) (kind 0)))) (hash "0mwwqa3rxka27gy90kkr5ki7as36a3ikr4n6sbq5ak922hipmk09")))

(define-public crate-acute_ron-0.1 (crate (name "acute_ron") (vers "0.1.0") (hash "0nrm1ps8ijrf01j8170xq1yvk9ll09k0sfyzhwlpb88sr4znlwrm")))

(define-public crate-acute_scenes-0.1 (crate (name "acute_scenes") (vers "0.1.0") (deps (list (crate-dep (name "acute_ecs") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "1a5psdy5rq5ikfvr42h3l212akz6875ngj4jpl8fbgrbxcp02y43")))

(define-public crate-acute_script-0.1 (crate (name "acute_script") (vers "0.1.0") (hash "19ya3y1f7hpfizw0vri7c8h8s1w9gvsnaryfv1np0nf18xl36597")))

(define-public crate-acute_transform-0.1 (crate (name "acute_transform") (vers "0.1.0") (hash "0vwlb2lvm5wzrrvd9k56d8pzdfh5lgxian4xlg6digyvkddznckc")))

(define-public crate-acute_ui-0.1 (crate (name "acute_ui") (vers "0.1.0") (hash "1asgypbj0xgb05a1csxlg79phzwxdh38mnfib410k7f09a43njz0")))

(define-public crate-acute_wgpu-0.1 (crate (name "acute_wgpu") (vers "0.1.0") (hash "1b8scswhbwlizc64fpw25nzy442xp3x7v30vyaavjay3qwqbxlz4")))

(define-public crate-acute_window-0.1 (crate (name "acute_window") (vers "0.1.0") (deps (list (crate-dep (name "acute_ecs") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "winit") (req "^0.22.2") (default-features #t) (kind 0)))) (hash "03894cz73ii6zywh05v2hp1lqq4wcalyp8bimhnch4mh1iqaw48s")))

(define-public crate-acute_window-0.1 (crate (name "acute_window") (vers "0.1.1") (deps (list (crate-dep (name "acute_ecs") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "winit") (req "^0.22.2") (default-features #t) (kind 0)))) (hash "028yvcsjwqpkmhc4ymwvd4vdj9j1knibx9x8ny0vj4yaggbbwx2r")))

(define-public crate-acute_winit-0.1 (crate (name "acute_winit") (vers "0.1.0") (hash "1xp0c1x4z5lyncs3lvhbkx7p9537p4vh5cy1sby1z79rgchdcjcv")))

