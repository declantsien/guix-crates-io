(define-module (crates-io ac e1) #:use-module (crates-io))

(define-public crate-ace128_driver-0.1 (crate (name "ace128_driver") (vers "0.1.0") (deps (list (crate-dep (name "embedded-hal") (req "^0.2.4") (features (quote ("unproven"))) (default-features #t) (kind 0)) (crate-dep (name "embedded-hal-mock") (req "^0.7.2") (default-features #t) (kind 2)))) (hash "0w955qdhdxfrim58vj3z477wwvkx7968kdn928g2vhxy0hkwf5wv")))

(define-public crate-ace128_driver-0.1 (crate (name "ace128_driver") (vers "0.1.1") (deps (list (crate-dep (name "embedded-hal") (req "^0.2.4") (features (quote ("unproven"))) (default-features #t) (kind 0)) (crate-dep (name "embedded-hal-mock") (req "^0.7.2") (default-features #t) (kind 2)))) (hash "07d04g34973apyx97vcw5k962w30v558ja71igq22q4yrkdw9jpn")))

(define-public crate-ace128_driver-0.1 (crate (name "ace128_driver") (vers "0.1.2") (deps (list (crate-dep (name "embedded-hal") (req "^0.2.4") (features (quote ("unproven"))) (default-features #t) (kind 0)) (crate-dep (name "embedded-hal-mock") (req "^0.7.2") (default-features #t) (kind 2)))) (hash "00f25c71ycaafa0fckfj3bj283q4xd2hg1p3nhqwmnsjh8y7125j")))

