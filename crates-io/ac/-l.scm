(define-module (crates-io ac -l) #:use-module (crates-io))

(define-public crate-ac-library-rs-0.1 (crate (name "ac-library-rs") (vers "0.1.0") (deps (list (crate-dep (name "proconio") (req "=0.3.6") (default-features #t) (kind 2)) (crate-dep (name "proconio-derive") (req "^0.2.1") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.7.3") (default-features #t) (kind 2)))) (hash "1v79qjs291lq476m49fdvzsrhfhplz7lig3brf892haccd937rjz") (rust-version "1.42")))

(define-public crate-ac-library-rs-0.1 (crate (name "ac-library-rs") (vers "0.1.1") (deps (list (crate-dep (name "proconio") (req "=0.3.6") (default-features #t) (kind 2)) (crate-dep (name "proconio-derive") (req "^0.2.1") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.7.3") (default-features #t) (kind 2)))) (hash "0wzgfw0nd93sljmzan0807icj03daz04cs96jy7nji1gnzbymq89") (rust-version "1.42")))

(define-public crate-ac-library-rs-expander-0.1 (crate (name "ac-library-rs-expander") (vers "0.1.0") (deps (list (crate-dep (name "cargo_metadata") (req "^0.12.0") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.3") (default-features #t) (kind 0)))) (hash "1bipyvis2difbigzpkz8l2nlp36hff8bd3h1yy05q7cyl96n1mww")))

