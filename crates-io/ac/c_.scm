(define-module (crates-io ac c_) #:use-module (crates-io))

(define-public crate-acc_reader-1 (crate (name "acc_reader") (vers "1.0.0") (hash "18mfmn11a3jn918dqx4vrswjgml8fnkpmf0335nmpycrx5njayc1")))

(define-public crate-acc_reader-2 (crate (name "acc_reader") (vers "2.0.0") (hash "1697ivh4rbkp0b1603nrcr36vnjayrihbgijms8dmfx2zspj7l0g")))

