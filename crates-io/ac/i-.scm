(define-module (crates-io ac i-) #:use-module (crates-io))

(define-public crate-aci-registry-1 (crate (name "aci-registry") (vers "1.0.0") (deps (list (crate-dep (name "csv") (req "^1.3.0") (default-features #t) (kind 1)) (crate-dep (name "serde") (req "^1.0.196") (default-features #t) (kind 1)) (crate-dep (name "serde_derive") (req "^1.0.196") (default-features #t) (kind 1)))) (hash "1j3w9ravvsk8w89gjwnyhzwcqiz1szsd349m71sfzjq53dv3wjrj") (features (quote (("fixed-repr") ("extended-info"))))))

(define-public crate-aci-registry-1 (crate (name "aci-registry") (vers "1.0.1") (deps (list (crate-dep (name "csv") (req "^1.3.0") (default-features #t) (kind 1)) (crate-dep (name "serde") (req "^1.0.196") (default-features #t) (kind 1)) (crate-dep (name "serde_derive") (req "^1.0.196") (default-features #t) (kind 1)))) (hash "002shlkw0xj9y7mxf01lk4l7pf1hww4cmz38159g8a1378pzkxdi") (features (quote (("fixed-repr") ("extended-info"))))))

(define-public crate-aci-registry-1 (crate (name "aci-registry") (vers "1.1.0") (deps (list (crate-dep (name "csv") (req "^1.3.0") (default-features #t) (kind 1)) (crate-dep (name "serde") (req "^1.0.196") (default-features #t) (kind 1)) (crate-dep (name "serde_derive") (req "^1.0.196") (default-features #t) (kind 1)))) (hash "1ln4lrwdbngk0xf8a3cjxks2pv2nvx2hlg7wvp97llv7b6ya3gxq") (features (quote (("non-authorative") ("fixed-repr") ("extended-info"))))))

(define-public crate-aci-registry-1 (crate (name "aci-registry") (vers "1.1.1") (deps (list (crate-dep (name "csv") (req "^1.3.0") (default-features #t) (kind 1)) (crate-dep (name "serde") (req "^1.0.196") (default-features #t) (kind 1)) (crate-dep (name "serde_derive") (req "^1.0.196") (default-features #t) (kind 1)))) (hash "0vw43mdd768hfziqv9q1ll1ydb3vh332pgja0n4fkmycnhckg172") (features (quote (("non-authorative") ("fixed-repr") ("extended-info"))))))

