(define-module (crates-io ac ur) #:use-module (crates-io))

(define-public crate-acursor-0.0.1 (crate (name "acursor") (vers "0.0.1") (hash "1rlpzpkgrzck4dr8pqrkiv538g5kj4s0ygc0cll54kjrkzq65kf0")))

(define-public crate-acursor-0.0.2 (crate (name "acursor") (vers "0.0.2") (deps (list (crate-dep (name "concat-idents") (req "^1.1.4") (default-features #t) (kind 0)))) (hash "13qf6xnz1a6xim8np061fq4kvfw9qm7v71hcmi1g1kvxm0qyhv9l")))

