(define-module (crates-io ac e_) #:use-module (crates-io))

(define-public crate-ace_it-0.1 (crate (name "ace_it") (vers "0.1.0") (deps (list (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "1d8a0fp4b19r29pm4gnivs4q2221y83yx23da44xn1njspsi2h3j")))

(define-public crate-ace_it-0.1 (crate (name "ace_it") (vers "0.1.1") (deps (list (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "0xkv7g9ifzzv0dk58fcmv81vpr8153rlagpxgqjvvg0f3f7y1rzz")))

(define-public crate-ace_rust_demo-0.1 (crate (name "ace_rust_demo") (vers "0.1.0") (deps (list (crate-dep (name "rust-crypto") (req "^0.2.36") (default-features #t) (kind 0)))) (hash "18idymh4ki3ky7al2m3f6r0wfl2c9n23hmbyazrw6khx2kn3xp0w") (yanked #t)))

