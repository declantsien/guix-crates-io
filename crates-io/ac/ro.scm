(define-module (crates-io ac ro) #:use-module (crates-io))

(define-public crate-acro-0.1 (crate (name "acro") (vers "0.1.0") (deps (list (crate-dep (name "assert_cmd") (req "^2") (default-features #t) (kind 2)) (crate-dep (name "clap") (req "^4.0.26") (default-features #t) (kind 0)) (crate-dep (name "colored") (req "^2.0.0") (default-features #t) (kind 0)) (crate-dep (name "csv") (req "^1.1") (default-features #t) (kind 0)) (crate-dep (name "predicates") (req "^2") (default-features #t) (kind 2)))) (hash "0ff61s7csxhkmf264yydzy199x4bnrm9v9gcg08r6g44gz3bv0c1") (yanked #t)))

(define-public crate-acro-0.2 (crate (name "acro") (vers "0.2.0") (deps (list (crate-dep (name "assert_cmd") (req "^2") (default-features #t) (kind 2)) (crate-dep (name "clap") (req "^4.0.26") (default-features #t) (kind 0)) (crate-dep (name "colored") (req "^2.0.0") (default-features #t) (kind 0)) (crate-dep (name "csv") (req "^1.1") (default-features #t) (kind 0)) (crate-dep (name "predicates") (req "^2") (default-features #t) (kind 2)))) (hash "1xyx0hr86s09y6q7np85g744sy5rmrksfcn4liiagc0pd1y45ggd")))

(define-public crate-across-0.1 (crate (name "across") (vers "0.1.1") (hash "0xyi7d0nc9vyz4bdiv15mgci0vkhzjl2ix7d0v2ydv0h5rpx4wq7")))

