(define-module (crates-io ac vp) #:use-module (crates-io))

(define-public crate-acvp-parser-0.1 (crate (name "acvp-parser") (vers "0.1.0") (deps (list (crate-dep (name "hex") (req "^0.4.3") (default-features #t) (kind 0)) (crate-dep (name "json") (req "^0.12.4") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.4") (default-features #t) (kind 0)))) (hash "0yyk0iazdzqqcfswpwpbi67qi1g4c24kjcvjixpcjbwi23z42y68")))

(define-public crate-acvp-parser-0.1 (crate (name "acvp-parser") (vers "0.1.1") (deps (list (crate-dep (name "hex") (req "^0.4.3") (default-features #t) (kind 0)) (crate-dep (name "json") (req "^0.12.4") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.4") (default-features #t) (kind 0)))) (hash "08qmlqg94m6hgm5wq6xk00cg9ibyhp6ja6z1kkzdjxzxwhjxdnib")))

