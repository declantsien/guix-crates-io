(define-module (crates-io ac ha) #:use-module (crates-io))

(define-public crate-achain-0.1 (crate (name "achain") (vers "0.1.0") (hash "0q7dkbz9s9bryxrd0qgmw5py24hby13bdnnh61h7yq2vaw3m3pnn")))

(define-public crate-achan-0.1 (crate (name "achan") (vers "0.1.0") (deps (list (crate-dep (name "serde") (req "^1") (features (quote ("alloc" "derive"))) (optional #t) (kind 0)) (crate-dep (name "wit-bindgen") (req "^0.17.0") (features (quote ("realloc"))) (optional #t) (kind 0)))) (hash "1irpjy1kh4vv9bwag6h37y31f4qhjxxb37gm6fcgb50ma1hsmgv6") (v 2) (features2 (quote (("wasm-component" "dep:wit-bindgen") ("serde" "dep:serde"))))))

(define-public crate-achan-0.2 (crate (name "achan") (vers "0.2.0") (deps (list (crate-dep (name "serde") (req "^1") (features (quote ("alloc" "derive"))) (optional #t) (kind 0)) (crate-dep (name "wit-bindgen") (req "^0.17.0") (features (quote ("realloc"))) (optional #t) (kind 0)))) (hash "10cqa49ywp7zh6dmj4941amwcc6v882x2xz8yg3dnz73hivw8nzq") (v 2) (features2 (quote (("wasm-component" "dep:wit-bindgen") ("serde" "dep:serde"))))))

