(define-module (crates-io ac m_) #:use-module (crates-io))

(define-public crate-acm_dependent-0.1 (crate (name "acm_dependent") (vers "0.1.0") (deps (list (crate-dep (name "arithmetic_congruence_monoid") (req "^1.0.3") (default-features #t) (kind 0)))) (hash "0lg0hcvxfdkim3lmsrcm266ndb87fxgmy22rnrc70ph95dnk7br5")))

