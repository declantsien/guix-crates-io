(define-module (crates-io ac i_) #:use-module (crates-io))

(define-public crate-aci_png-0.1 (crate (name "aci_png") (vers "0.1.0") (deps (list (crate-dep (name "png") (req "^0.9") (default-features #t) (kind 0)))) (hash "1ms55rxdsw09xx6k77fdjypdkz04n6nv2572rb9g4nhjk9l1dld8")))

(define-public crate-aci_png-0.2 (crate (name "aci_png") (vers "0.2.0") (deps (list (crate-dep (name "png") (req "^0.9") (default-features #t) (kind 0)))) (hash "0wpl77fcjn6547s0dd6nd38f3ini7zrn2825xynps2i3czvp4v3g")))

(define-public crate-aci_png-0.3 (crate (name "aci_png") (vers "0.3.0") (deps (list (crate-dep (name "afi") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "ami") (req "^0.5") (default-features #t) (kind 0)) (crate-dep (name "png") (req "^0.10") (default-features #t) (kind 0)))) (hash "01j0zqjgbhdz78zfd5hy72f28wrimcxlfzjn7qcgpi5aqcv5x20h")))

(define-public crate-aci_png-0.4 (crate (name "aci_png") (vers "0.4.0") (deps (list (crate-dep (name "afi") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "ami") (req "^0.5") (default-features #t) (kind 0)) (crate-dep (name "png") (req "^0.11") (default-features #t) (kind 0)))) (hash "1xa0s0b54h8312a5fcg89pa8687nni8kh0bxl997b52wh81c2p1j")))

(define-public crate-aci_png-0.5 (crate (name "aci_png") (vers "0.5.0") (deps (list (crate-dep (name "afi") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "ami") (req "^0.5") (default-features #t) (kind 0)) (crate-dep (name "png") (req "^0.11") (default-features #t) (kind 0)))) (hash "1pl60snvic0qnar358bqc8pcnrl843nawlb3g3ka0bqai4d07i9p")))

(define-public crate-aci_png-0.5 (crate (name "aci_png") (vers "0.5.1") (deps (list (crate-dep (name "afi") (req "^0.3.1") (default-features #t) (kind 0)) (crate-dep (name "png") (req "^0.11") (default-features #t) (kind 0)))) (hash "1n92v3cnqd5qydzc7bkqlk2cdz96fz1746hjjwx04s8hrr94lwyq")))

(define-public crate-aci_png-0.5 (crate (name "aci_png") (vers "0.5.2") (deps (list (crate-dep (name "afi") (req "^0.3.2") (default-features #t) (kind 0)) (crate-dep (name "png") (req "^0.11") (default-features #t) (kind 0)))) (hash "0ghkk9qzx82y2nsh2cy5kqmh30k18a1wifhqv6na63q1bg831rqr")))

(define-public crate-aci_png-0.6 (crate (name "aci_png") (vers "0.6.0") (deps (list (crate-dep (name "afi") (req "^0.5") (default-features #t) (kind 0)) (crate-dep (name "flate2") (req "^0.2") (default-features #t) (kind 0)))) (hash "0xv8ivc3dns9hzjgyd4smy7wh1cigd9qcwr2xmsi7xphnfp0iry9")))

(define-public crate-aci_png-0.6 (crate (name "aci_png") (vers "0.6.1") (deps (list (crate-dep (name "afi") (req "^0.5") (default-features #t) (kind 0)) (crate-dep (name "byteorder") (req "^1.2.2") (default-features #t) (kind 0)) (crate-dep (name "flate2") (req "^0.2") (default-features #t) (kind 0)))) (hash "15says7vhan2v6ypdpyj707jcip48bwinn9j6fscir4i6863iiy8")))

(define-public crate-aci_png-0.6 (crate (name "aci_png") (vers "0.6.2") (deps (list (crate-dep (name "afi") (req "^0.5") (default-features #t) (kind 0)) (crate-dep (name "byteorder") (req "^1.2") (default-features #t) (kind 0)) (crate-dep (name "deflate") (req "^0.7") (default-features #t) (kind 0)) (crate-dep (name "inflate") (req "^0.4") (default-features #t) (kind 0)))) (hash "1msn2d93v4kfh93x7b7z4bbw05csgj7hh9wnyffwaq62dqdh7w8d")))

(define-public crate-aci_png-0.7 (crate (name "aci_png") (vers "0.7.0") (deps (list (crate-dep (name "afi") (req "^0.7") (default-features #t) (kind 0)) (crate-dep (name "byteorder") (req "^1.2") (default-features #t) (kind 0)) (crate-dep (name "deflate") (req "^0.7") (default-features #t) (kind 0)) (crate-dep (name "inflate") (req "^0.4") (default-features #t) (kind 0)))) (hash "1gmz5bq9a2nw6qm1az4llyshk6m753q24hb3bhnkl9mhxw96jl08")))

(define-public crate-aci_png-0.8 (crate (name "aci_png") (vers "0.8.0-pre0") (deps (list (crate-dep (name "afi") (req "^0.8.0-pre0") (default-features #t) (kind 0)) (crate-dep (name "byteorder") (req "^1.2") (default-features #t) (kind 0)) (crate-dep (name "deflate") (req "^0.7") (default-features #t) (kind 0)) (crate-dep (name "inflate") (req "^0.4") (default-features #t) (kind 0)))) (hash "1r7h76m3f6mcr700j3gpxwil42l3l2jr0rs6hmqfghas7vay6fr0")))

(define-public crate-aci_ppm-0.1 (crate (name "aci_ppm") (vers "0.1.0") (hash "0qi58msad98nlg737flxxqnj21j6mg3pxzl68g0dss2n2dk4by92")))

(define-public crate-aci_ppm-0.2 (crate (name "aci_ppm") (vers "0.2.0") (deps (list (crate-dep (name "afi") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "ami") (req "^0.5") (default-features #t) (kind 0)))) (hash "0x5g273vfr6pnlk39hy7lnlvmynp02y8fsqgfqr6cxkrm5p2ma23")))

(define-public crate-aci_ppm-0.3 (crate (name "aci_ppm") (vers "0.3.0") (deps (list (crate-dep (name "afi") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "ami") (req "^0.5") (default-features #t) (kind 0)))) (hash "0mgq1rdravz704v2s8l3vcjcndapv6ynj6bxh7m7sf4qrps6752h")))

(define-public crate-aci_ppm-0.4 (crate (name "aci_ppm") (vers "0.4.0") (deps (list (crate-dep (name "afi") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "ami") (req "^0.5") (default-features #t) (kind 0)))) (hash "05f3r919l6a79c9rgn9wb92ns3pbz5lwdg993f31rkph299qzkgb")))

(define-public crate-aci_ppm-0.4 (crate (name "aci_ppm") (vers "0.4.1") (deps (list (crate-dep (name "afi") (req "^0.3.1") (default-features #t) (kind 0)))) (hash "18ajawf8z380x5yyadar0xnk2xgs3xip2xasb6mlh1dm4h3hnmwz")))

(define-public crate-aci_ppm-0.4 (crate (name "aci_ppm") (vers "0.4.2") (deps (list (crate-dep (name "afi") (req "^0.3.2") (default-features #t) (kind 0)))) (hash "147zn5apnzm35lh4j4n3czccy5b53146vj2k9ps1559dp7kcw9z0")))

(define-public crate-aci_ppm-0.5 (crate (name "aci_ppm") (vers "0.5.0") (deps (list (crate-dep (name "afi") (req "^0.5") (default-features #t) (kind 0)) (crate-dep (name "byteorder") (req "^1.2.2") (default-features #t) (kind 0)))) (hash "0q09yxmp9d399x5xw6s0401mimihwgz0vv025a7bpw8fi013ns2c")))

(define-public crate-aci_ppm-0.7 (crate (name "aci_ppm") (vers "0.7.0") (deps (list (crate-dep (name "aci_png") (req "^0.7") (default-features #t) (kind 2)) (crate-dep (name "afi") (req "^0.7") (default-features #t) (kind 0)) (crate-dep (name "byteorder") (req "^1.2") (default-features #t) (kind 0)))) (hash "09kaqgavizi1pms266gvqkzyg9cgqasdfs2mx8y8c8dhhg00gzdc")))

