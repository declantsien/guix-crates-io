(define-module (crates-io ac nh) #:use-module (crates-io))

(define-public crate-acnh-0.1 (crate (name "acnh") (vers "0.1.0") (deps (list (crate-dep (name "reqwest") (req "^0.10.8") (features (quote ("json"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.114") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^0.2.22") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "04961aqjn5n60np85qhz0gc11nxpqss70l9jy1bn7gmgl8r66j38")))

(define-public crate-acnh-0.2 (crate (name "acnh") (vers "0.2.0") (deps (list (crate-dep (name "reqwest") (req "^0.10.8") (features (quote ("json"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.114") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^0.2.22") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "01hcw381ni0ydk7icazcd69pzlbb8ql3lvbgv1z8kbva6i59ng6s")))

(define-public crate-acnh-0.3 (crate (name "acnh") (vers "0.3.0") (deps (list (crate-dep (name "reqwest") (req "^0.10.8") (features (quote ("json"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.114") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^0.2.22") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "0xk44d0ghs0l1cp9q4riz403c1c7yyrdnkzsh10gwfhw2h2gq63n")))

