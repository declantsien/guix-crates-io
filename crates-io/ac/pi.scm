(define-module (crates-io ac pi) #:use-module (crates-io))

(define-public crate-acpi-0.1 (crate (name "acpi") (vers "0.1.0") (hash "1am481xpc6jawz99dvpjvnbvi263cbpli4rgl106xd7rz2pnn9cs")))

(define-public crate-acpi-0.2 (crate (name "acpi") (vers "0.2.0") (deps (list (crate-dep (name "bit_field") (req "^0.9") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)))) (hash "1axqllxjbjy1xyyyxgmd6p3qq6ws9azipja9fm9rbfvs3h8959d2")))

(define-public crate-acpi-0.2 (crate (name "acpi") (vers "0.2.1") (deps (list (crate-dep (name "bit_field") (req "^0.9") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)))) (hash "0r61cc68jckxs8b2iv4db5mkmn6xfylsns4w08fif47xihpa7rkq")))

(define-public crate-acpi-0.3 (crate (name "acpi") (vers "0.3.0") (deps (list (crate-dep (name "bit_field") (req "^0.9") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "typenum") (req "^1") (default-features #t) (kind 0)))) (hash "0jzykz9m546jnvbawhaidgxvhxjcl48660yy5p33amxbiw5zwm3z")))

(define-public crate-acpi-0.4 (crate (name "acpi") (vers "0.4.0") (deps (list (crate-dep (name "bit_field") (req "^0.9") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "typenum") (req "^1") (default-features #t) (kind 0)))) (hash "1smcd6gjziqraj6nfaj5siw3vpyqka3sac3rix7xs8n3pl3df61c")))

(define-public crate-acpi-0.5 (crate (name "acpi") (vers "0.5.0") (deps (list (crate-dep (name "bit_field") (req "^0.10") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "typenum") (req "^1") (default-features #t) (kind 0)))) (hash "0rxk7m3qhx8072qxrnvk9vgqfl76ifh23zynk97f6fada76fn525")))

(define-public crate-acpi-0.6 (crate (name "acpi") (vers "0.6.0") (deps (list (crate-dep (name "bit_field") (req "^0.10") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)))) (hash "0ppzc36l4zg11xnlyvzprms42aaf4341hzqjy6c2vfnhswjw1n4j")))

(define-public crate-acpi-0.7 (crate (name "acpi") (vers "0.7.0") (deps (list (crate-dep (name "bit_field") (req "^0.10") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)))) (hash "0fa2jh6xfzv3lkb255rbw3zvrsajpcndg7x2gqhljk9208jk1sna")))

(define-public crate-acpi-0.8 (crate (name "acpi") (vers "0.8.0") (deps (list (crate-dep (name "bit_field") (req "^0.10") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)))) (hash "15c9kgk83g67bil5qj99m38ri52d98pr9ibpfayxssq9gs55lhiz")))

(define-public crate-acpi-1 (crate (name "acpi") (vers "1.0.0") (deps (list (crate-dep (name "bit_field") (req "^0.10") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)))) (hash "013zada7fs8wjni4a74994jcss4l9p9mw95f6cvxkn2n7361qnrz")))

(define-public crate-acpi-1 (crate (name "acpi") (vers "1.1.0") (deps (list (crate-dep (name "bit_field") (req "^0.10") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)))) (hash "0fk9qbvni9hbhgpghlj1z83j15xzknks2h7c4qbh7d0cl82fxhwz")))

(define-public crate-acpi-2 (crate (name "acpi") (vers "2.0.0-pre1") (deps (list (crate-dep (name "bit_field") (req "^0.10") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "rsdp") (req "^1") (default-features #t) (kind 0)))) (hash "1z6zd0gmf222m7cfc4v8wyhm9sah6abba8pf2iczcvvnnicwdqpi")))

(define-public crate-acpi-2 (crate (name "acpi") (vers "2.0.0") (deps (list (crate-dep (name "bit_field") (req "^0.10") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "rsdp") (req "^1") (default-features #t) (kind 0)))) (hash "1gqf01cqmqy48a2bnfb6dmharqqwmlg6cl19r621wjwxnlwkiayk")))

(define-public crate-acpi-2 (crate (name "acpi") (vers "2.1.0") (deps (list (crate-dep (name "bit_field") (req "^0.10") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "rsdp") (req "^1") (default-features #t) (kind 0)))) (hash "0g7k8sv6fwncxsy4k14lkbndz7ar44xynzb8fb5rp9va9fd4r4a6")))

(define-public crate-acpi-2 (crate (name "acpi") (vers "2.2.0-pre0") (deps (list (crate-dep (name "bit_field") (req "^0.10") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "rsdp") (req "=1.1.0-pre0") (default-features #t) (kind 0)))) (hash "1awpdcrbjq42v13gmd6j83lx9iw6hdsml51h7g1f6xbydxqqvy8z")))

(define-public crate-acpi-2 (crate (name "acpi") (vers "2.2.0") (deps (list (crate-dep (name "bit_field") (req "^0.10") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "rsdp") (req "^1") (default-features #t) (kind 0)))) (hash "1447k53dj7c8jjh9yibv5ijrv9xdrvipjhm0nchbvvylav4dx3jq")))

(define-public crate-acpi-2 (crate (name "acpi") (vers "2.3.0") (deps (list (crate-dep (name "bit_field") (req "^0.10") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "rsdp") (req "^1") (default-features #t) (kind 0)))) (hash "1cbs1y8h87lyidii9d0g5d086snsrv2pkdnb0gj3b2dwi2kzc0bl")))

(define-public crate-acpi-2 (crate (name "acpi") (vers "2.3.1") (deps (list (crate-dep (name "bit_field") (req "^0.10") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "rsdp") (req "^1") (default-features #t) (kind 0)))) (hash "17y044dan4lim9c7nj2w6bg88k83yg2zjmyfrswg06jchwm8pk0k")))

(define-public crate-acpi-3 (crate (name "acpi") (vers "3.0.0") (deps (list (crate-dep (name "bit_field") (req "^0.10") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "rsdp") (req "^2") (default-features #t) (kind 0)))) (hash "1ixh8b3klpq0bndj5vi19gcl5ysajwn1hz9sjv1k7fhqb3xsppih")))

(define-public crate-acpi-3 (crate (name "acpi") (vers "3.1.0") (deps (list (crate-dep (name "bit_field") (req "^0.10") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "rsdp") (req "^2") (default-features #t) (kind 0)))) (hash "11nm1gapajvwbya6q6p8bccwa886xix5kjk1w44al8gk35bg295q")))

(define-public crate-acpi-4 (crate (name "acpi") (vers "4.0.0") (deps (list (crate-dep (name "bit_field") (req "^0.10") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "rsdp") (req "^2") (default-features #t) (kind 0)))) (hash "1bnrywq1qdpnjiydrlrpimqi8wfyvivwlycvvfj8zv3m9wr7m0gf")))

(define-public crate-acpi-4 (crate (name "acpi") (vers "4.1.0") (deps (list (crate-dep (name "bit_field") (req "^0.10") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "rsdp") (req "^2") (default-features #t) (kind 0)))) (hash "016hbfarl15nbscl8phc4nhmx8z6ymd14igdwmkyliz7qr4pc8sm")))

(define-public crate-acpi-4 (crate (name "acpi") (vers "4.1.1") (deps (list (crate-dep (name "bit_field") (req "^0.10") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "rsdp") (req "^2") (default-features #t) (kind 0)))) (hash "05bdq9872pcpf5b7w2ppcb5ra24r7c3na5xy6njjwqvq66mlhkv5")))

(define-public crate-acpi-5 (crate (name "acpi") (vers "5.0.0") (deps (list (crate-dep (name "bit_field") (req "^0.10.2") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.20") (default-features #t) (kind 0)))) (hash "1hz4mc7dd69k9ga6k5hiysms6i9ayql2dfir3gv22h1hjn8l0j72") (features (quote (("default" "allocator_api" "alloc") ("allocator_api") ("alloc" "allocator_api"))))))

(define-public crate-acpi_client-0.1 (crate (name "acpi_client") (vers "0.1.0") (hash "1lw5qwk981a5pdksadimadlgqdlf2h1vjm2b321ipjm87xlxgids")))

(define-public crate-acpi_client-0.2 (crate (name "acpi_client") (vers "0.2.0") (hash "1hg3c3087bkg8961ja3kw96n9vdhh0hqydd8mnxwp1x94f2x6icj")))

(define-public crate-acpi_client-1 (crate (name "acpi_client") (vers "1.0.0") (hash "1r36krzh0hb5diay256iikxppbrrq8jfrxpzvnz1jxhqdvz0i8sn")))

(define-public crate-acpi_client-1 (crate (name "acpi_client") (vers "1.1.0") (deps (list (crate-dep (name "tempfile") (req "^3.1.0") (default-features #t) (kind 2)))) (hash "0ac9sjgvc1jb226qhynmbi7pr137rykx9f8b6jcdjify89vibw5i")))

(define-public crate-acpi_client-2 (crate (name "acpi_client") (vers "2.0.0") (deps (list (crate-dep (name "tempfile") (req "^3.1.0") (default-features #t) (kind 2)))) (hash "1659lqc986i6asaw3zfalb0dvgj34lcdx6449j4g9z3g6mxas4wl")))

(define-public crate-acpica-bindings-0.1 (crate (name "acpica-bindings") (vers "0.1.0") (deps (list (crate-dep (name "bitfield-struct") (req "^0.5") (kind 0)) (crate-dep (name "bitvec") (req "^1") (optional #t) (kind 0)) (crate-dep (name "cc") (req "^1.0") (default-features #t) (kind 1)) (crate-dep (name "flate2") (req "^1") (default-features #t) (kind 1)) (crate-dep (name "log") (req "^0.4") (kind 0)) (crate-dep (name "spin") (req "^0.9.8") (features (quote ("mutex" "spin_mutex"))) (kind 0)) (crate-dep (name "tar") (req "^0.4") (default-features #t) (kind 1)) (crate-dep (name "x86_64") (req "^0.14") (optional #t) (kind 0)))) (hash "0h8wmbalg0bzj0px3rlr4qpjh46gnvyk7ilcb5pjmlk5s287h9lp") (features (quote (("default" "builtin_cache" "builtin_lock" "builtin_semaphore") ("builtin_semaphore") ("builtin_lock") ("builtin_cache" "bitvec/alloc"))))))

(define-public crate-acpica-bindings-0.1 (crate (name "acpica-bindings") (vers "0.1.1") (deps (list (crate-dep (name "bitfield-struct") (req "^0.5") (kind 0)) (crate-dep (name "bitvec") (req "^1") (optional #t) (kind 0)) (crate-dep (name "cc") (req "^1.0") (default-features #t) (kind 1)) (crate-dep (name "flate2") (req "^1") (default-features #t) (kind 1)) (crate-dep (name "log") (req "^0.4") (kind 0)) (crate-dep (name "spin") (req "^0.9.8") (features (quote ("mutex" "spin_mutex"))) (kind 0)) (crate-dep (name "tar") (req "^0.4") (default-features #t) (kind 1)) (crate-dep (name "x86_64") (req "^0.14") (optional #t) (kind 0)))) (hash "1lwsa2lys15rjqrdp0rc1rcsxjhmid3m0lzawrphpm8g88rc18bf") (features (quote (("default" "builtin_cache" "builtin_lock" "builtin_semaphore") ("builtin_semaphore") ("builtin_lock") ("builtin_cache" "bitvec/alloc"))))))

(define-public crate-acpica-bindings-0.1 (crate (name "acpica-bindings") (vers "0.1.2") (deps (list (crate-dep (name "bitfield-struct") (req "^0.5") (kind 0)) (crate-dep (name "bitvec") (req "^1") (optional #t) (kind 0)) (crate-dep (name "cc") (req "^1.0") (default-features #t) (kind 1)) (crate-dep (name "flate2") (req "^1") (default-features #t) (kind 1)) (crate-dep (name "log") (req "^0.4") (kind 0)) (crate-dep (name "spin") (req "^0.9.8") (features (quote ("mutex" "spin_mutex"))) (kind 0)) (crate-dep (name "tar") (req "^0.4") (default-features #t) (kind 1)) (crate-dep (name "x86_64") (req "^0.14") (optional #t) (kind 0)))) (hash "076j8bwi76nhv0yl1j70l8v7ix220b1lxm4dd38kfhb7irrjc07c") (features (quote (("default" "builtin_cache" "builtin_lock" "builtin_semaphore") ("builtin_semaphore") ("builtin_lock") ("builtin_cache" "bitvec/alloc"))))))

(define-public crate-acpica-sys-0.0.1 (crate (name "acpica-sys") (vers "0.0.1") (hash "1kvgxp4y6l2m9ykpqhaj0s9lzflzsqj3769pzs2x90zqvh0cc91x")))

(define-public crate-acpica-sys-0.0.2 (crate (name "acpica-sys") (vers "0.0.2") (hash "000szjb3k2hymzflvfp2sg8f6kl1m6akphzmzj0y1l9472ibb71v")))

(define-public crate-acpica-sys-0.0.3 (crate (name "acpica-sys") (vers "0.0.3") (hash "01w8n5skwbp2c231h2np183nmf8lgdw26w7psw9kmj14n27cfmx2")))

(define-public crate-acpica-sys-0.0.4 (crate (name "acpica-sys") (vers "0.0.4") (hash "1kdq475nhigz6glfnqgd9dsfw6i21hazbdk9igbqylly3p371mrz")))

(define-public crate-acpid_plug-0.1 (crate (name "acpid_plug") (vers "0.1.0") (deps (list (crate-dep (name "futures-util") (req "^0.3.30") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.37.0") (features (quote ("fs" "io-util" "net"))) (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.37.0") (features (quote ("macros" "rt"))) (default-features #t) (kind 2)))) (hash "1x8aav58zvvy8yvkn413w9icr059llx1s16r7808cr43bcd4m72n") (yanked #t)))

(define-public crate-acpid_plug-0.1 (crate (name "acpid_plug") (vers "0.1.1") (deps (list (crate-dep (name "futures-util") (req "^0.3.30") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.37.0") (features (quote ("fs" "io-util" "net"))) (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.37.0") (features (quote ("macros" "rt"))) (default-features #t) (kind 2)))) (hash "0xi2cyk5p69klsh9kv6g2zxjlb595jmsfprj1c244qhwiizfff8z")))

(define-public crate-acpid_plug-0.1 (crate (name "acpid_plug") (vers "0.1.2") (deps (list (crate-dep (name "futures-util") (req "^0.3.30") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.37.0") (features (quote ("fs" "io-util" "net"))) (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.37.0") (features (quote ("macros" "rt"))) (default-features #t) (kind 2)))) (hash "1c5l2xmxd7a793m4zy8n8lpi4czy1izbg99n6avqgr661g6b8ja7")))

(define-public crate-acpitool-1 (crate (name "acpitool") (vers "1.0.0") (deps (list (crate-dep (name "acpi_client") (req "^1.0.0") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^2.33.0") (default-features #t) (kind 0)))) (hash "0g662qcl96yai9lciliqm25xsvh6ccg595zsh8s7nkm1x9dk5k35")))

