(define-module (crates-io ac al) #:use-module (crates-io))

(define-public crate-acall-cli-0.1 (crate (name "acall-cli") (vers "0.1.0") (deps (list (crate-dep (name "anyhow") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "assert_cmd") (req "^2.0") (default-features #t) (kind 2)) (crate-dep (name "assert_fs") (req "^1.0") (default-features #t) (kind 2)) (crate-dep (name "predicates") (req "^2.0") (default-features #t) (kind 2)) (crate-dep (name "structopt") (req "^0.3.22") (default-features #t) (kind 0)) (crate-dep (name "tempfile") (req "^3") (default-features #t) (kind 2)))) (hash "1zr6x8yl776dxf9nc0qkx2cgpxz5sv57vak29yr0a7l779bnjs3q")))

