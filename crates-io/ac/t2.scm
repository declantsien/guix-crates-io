(define-module (crates-io ac t2) #:use-module (crates-io))

(define-public crate-act2-0.1 (crate (name "act2") (vers "0.1.2") (deps (list (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)))) (hash "16307q31kfr7x8p41v2hvq5cmm86vry1agwsdbn5v98r7m4hwg9y")))

