(define-module (crates-io ac yc) #:use-module (crates-io))

(define-public crate-acyclic-graph-0.1 (crate (name "acyclic-graph") (vers "0.1.0") (deps (list (crate-dep (name "fnv") (req "^1") (default-features #t) (kind 0)))) (hash "1d9jvkjlsl3b2abh24wwm5j84v3hi35gw1cbj1h9rk149i8ng22m")))

(define-public crate-acyclic-network-0.0.1 (crate (name "acyclic-network") (vers "0.0.1") (deps (list (crate-dep (name "fixedbitset") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.3") (default-features #t) (kind 0)))) (hash "0zhsc2qb76w0ir5w2zr3dwww4qar39mh6npfm5n4yahzdffanlxc")))

(define-public crate-acyclic-network-0.1 (crate (name "acyclic-network") (vers "0.1.0") (deps (list (crate-dep (name "fixedbitset") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.3") (default-features #t) (kind 0)))) (hash "02hhmajly1fy0pracqr9zm2ggss17006p0p59vp4m6552cscj2q0")))

(define-public crate-acyclic-network-0.2 (crate (name "acyclic-network") (vers "0.2.0") (deps (list (crate-dep (name "fixedbitset") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.4") (default-features #t) (kind 0)))) (hash "1w2wlsxp2xc0w33qkhmc4cx7cap17xim5n3nz0mzpslyv9qfh57w")))

