(define-module (crates-io ac tf) #:use-module (crates-io))

(define-public crate-actfv-0.1 (crate (name "actfv") (vers "0.1.0") (deps (list (crate-dep (name "assert_cmd") (req "^2") (default-features #t) (kind 2)) (crate-dep (name "file_diff") (req "^1.0.0") (default-features #t) (kind 2)))) (hash "1pibcvsr58j33v6nblpd94sbvlpddwk07vhigcj8lnibbj5fz3fq")))

(define-public crate-actfv-0.1 (crate (name "actfv") (vers "0.1.1") (deps (list (crate-dep (name "assert_cmd") (req "^2") (default-features #t) (kind 2)) (crate-dep (name "file_diff") (req "^1.0.0") (default-features #t) (kind 2)))) (hash "11i1rdn1h14djsr879ppzmsn0rlw2hjvqpqpl0wyaqpgsb91w5wa")))

(define-public crate-actfv-0.1 (crate (name "actfv") (vers "0.1.2") (deps (list (crate-dep (name "assert_cmd") (req "^2") (default-features #t) (kind 2)) (crate-dep (name "file_diff") (req "^1.0.0") (default-features #t) (kind 2)))) (hash "01k8whmhp7avryqnp2avb7pfnpsqxgh3jrmyhryqfd3axl47dcmb")))

(define-public crate-actfv-0.2 (crate (name "actfv") (vers "0.2.0") (deps (list (crate-dep (name "assert_cmd") (req "^2") (default-features #t) (kind 2)) (crate-dep (name "clap") (req "^4.3.3") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "file_diff") (req "^1.0.0") (default-features #t) (kind 2)) (crate-dep (name "notify") (req "^6.0.0") (default-features #t) (kind 0)) (crate-dep (name "notify-debouncer-full") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "1hpjds5yahmf2rxihdny33fi2sncilvsnk2dz6m1zggick01b0cn")))

(define-public crate-actfv-0.3 (crate (name "actfv") (vers "0.3.0") (deps (list (crate-dep (name "assert_cmd") (req "^2") (default-features #t) (kind 2)) (crate-dep (name "clap") (req "^4.3.3") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "file_diff") (req "^1.0.0") (default-features #t) (kind 2)) (crate-dep (name "notify") (req "^6.0.0") (default-features #t) (kind 0)) (crate-dep (name "notify-debouncer-full") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "06bnl210mxw8iybnyg4kjgz05zf619zapalanan03gp446y1bp94")))

