(define-module (crates-io ac op) #:use-module (crates-io))

(define-public crate-acopen-0.1 (crate (name "acopen") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^3.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "webbrowser") (req "^0.6.0") (default-features #t) (kind 0)))) (hash "18j04iyp2jg577j5ldwwniyndgkgs7ykig91y3k6v7lg89c9n1p4")))

(define-public crate-acopen-0.1 (crate (name "acopen") (vers "0.1.1") (deps (list (crate-dep (name "clap") (req "^3.1.6") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "webbrowser") (req "^0.6.0") (default-features #t) (kind 0)))) (hash "1bg28dmr9i3iv1r9z1lc1fxxcfp4z31dkj7a75rl2d0vij6qm3kh")))

