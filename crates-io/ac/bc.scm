(define-module (crates-io ac bc) #:use-module (crates-io))

(define-public crate-acbc-0.1 (crate (name "acbc") (vers "0.1.0") (deps (list (crate-dep (name "criterion") (req "^0.3.4") (default-features #t) (kind 2)) (crate-dep (name "nom") (req "^6") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "tinyvec") (req "^1") (default-features #t) (kind 0)))) (hash "1w2fr6hkdfhdnivqxlnpyjirhv16617lpjs08xxbjb3zd8wk7iam")))

