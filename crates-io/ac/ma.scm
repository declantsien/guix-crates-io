(define-module (crates-io ac ma) #:use-module (crates-io))

(define-public crate-acmake-0.1 (crate (name "acmake") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^4.1.8") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "indoc") (req "^2.0.1") (default-features #t) (kind 0)))) (hash "01s80i39sg5rpb3svhnfxm2kinl2a10zx2z22nhwblrnyd3q92mq")))

(define-public crate-acmake-0.1 (crate (name "acmake") (vers "0.1.1") (deps (list (crate-dep (name "clap") (req "^4.1.8") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "indoc") (req "^2.0.1") (default-features #t) (kind 0)))) (hash "05wvqg98sp3mgncmr8719d219a2v16n51h6p4mb7zmrjp1djkshw")))

