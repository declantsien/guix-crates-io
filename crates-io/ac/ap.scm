(define-module (crates-io ac ap) #:use-module (crates-io))

(define-public crate-acap-0.1 (crate (name "acap") (vers "0.1.0") (deps (list (crate-dep (name "criterion") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "num-traits") (req "^0.2.11") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.7.3") (default-features #t) (kind 0)))) (hash "1abx2ws6r63jhsgki8k1p5mq5fcqzs87jg36wx4sj1x2h53zh7w8")))

(define-public crate-acap-0.1 (crate (name "acap") (vers "0.1.1") (deps (list (crate-dep (name "criterion") (req "^0.3.3") (default-features #t) (kind 2)) (crate-dep (name "num-traits") (req "^0.2.12") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.7.3") (default-features #t) (kind 2)))) (hash "15pgdlnn07hbcqwki7jw0kdh43v8p02x6czwgsyajgbr30vw9hgl")))

(define-public crate-acap-0.2 (crate (name "acap") (vers "0.2.0") (deps (list (crate-dep (name "criterion") (req "^0.3.3") (default-features #t) (kind 2)) (crate-dep (name "num-traits") (req "^0.2.12") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.7.3") (default-features #t) (kind 2)))) (hash "0v8pz6gcz8jy3c9fhlkx0pk3w79pplrvvr5i5gr8ckda4az9qsnv")))

(define-public crate-acap-0.3 (crate (name "acap") (vers "0.3.0") (deps (list (crate-dep (name "criterion") (req "^0.3.5") (default-features #t) (kind 2)) (crate-dep (name "num-traits") (req "^0.2.14") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.4") (default-features #t) (kind 2)))) (hash "0parmn20n6qjc7dfzc2604j757gd4dlw7qv1gq5rlwnmnars0lb8")))

