(define-module (crates-io ac l-) #:use-module (crates-io))

(define-public crate-acl-rs-0.1 (crate (name "acl-rs") (vers "0.1.0") (deps (list (crate-dep (name "acl-sys") (req "^1.0.1") (default-features #t) (kind 0)) (crate-dep (name "nix") (req "^0.16.1") (default-features #t) (kind 0)))) (hash "1qbk835zg80bdqxbdsbz9anv3jjmpwpb80sy841r4fcqlh72ckky")))

(define-public crate-acl-rs-0.1 (crate (name "acl-rs") (vers "0.1.1") (deps (list (crate-dep (name "acl-sys") (req "^1.0.1") (default-features #t) (kind 0)) (crate-dep (name "nix") (req "^0.26") (default-features #t) (kind 0)))) (hash "1557d5dqzalf6by1slq8aq4sxk565cyfl9d4m7bxd9nx65yaip45")))

(define-public crate-acl-sys-1 (crate (name "acl-sys") (vers "1.0.0") (deps (list (crate-dep (name "libc") (req "*") (default-features #t) (kind 0)))) (hash "00awisbcyn71kj5qlqq4yash775iya1isppw1r8qqfnqiyizg8cc")))

(define-public crate-acl-sys-1 (crate (name "acl-sys") (vers "1.0.1") (deps (list (crate-dep (name "libc") (req "^0.1") (default-features #t) (kind 0)))) (hash "07380z53gw3019y5l2kw6v4fg4rgcqjnmnk6n3xmdadppac4mkiq")))

(define-public crate-acl-sys-1 (crate (name "acl-sys") (vers "1.2.0") (deps (list (crate-dep (name "libc") (req "^0.2") (kind 0)))) (hash "1r7ivaiwl5kdq599bz2pnfj1gqy1gwl0wnm435rsadhs09n4721q")))

(define-public crate-acl-sys-1 (crate (name "acl-sys") (vers "1.2.1") (deps (list (crate-dep (name "libc") (req "^0.2") (kind 0)))) (hash "0d591bswrhgivax00p1js2x9vpa0k1lblm9hii4abrygzfbh7147")))

(define-public crate-acl-sys-1 (crate (name "acl-sys") (vers "1.2.2") (deps (list (crate-dep (name "libc") (req "^0.2") (kind 0)))) (hash "1ciygbhavz4ms3zxqlg139sj97gpw3vnfg7jip8ly4nkppwpkh5v")))

