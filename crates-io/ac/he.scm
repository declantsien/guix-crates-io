(define-module (crates-io ac he) #:use-module (crates-io))

(define-public crate-acheron-0.1 (crate (name "acheron") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^2.32.0") (default-features #t) (kind 0)) (crate-dep (name "im") (req "^12.3.3") (default-features #t) (kind 0)))) (hash "015p2dndg8qpqbb48d9n9hcchnw5y2kyxap349mm1y81ymyz6z1j")))

