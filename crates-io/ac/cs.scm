(define-module (crates-io ac cs) #:use-module (crates-io))

(define-public crate-accs-0.1 (crate (name "accs") (vers "0.1.0") (hash "18qzdxv2xvz4ngs1iiy0y59w8lizfbiw9kbk4mxllmrhjcx2vrlq")))

(define-public crate-accs-core-0.1 (crate (name "accs-core") (vers "0.1.0") (hash "1i6n568qsm7h7647mzadhmg6128d4i46nl3giwyrrah4ywxk2zq7")))

