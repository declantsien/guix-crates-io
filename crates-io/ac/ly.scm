(define-module (crates-io ac ly) #:use-module (crates-io))

(define-public crate-aclysma_spirv_cross-0.0.1 (crate (name "aclysma_spirv_cross") (vers "0.0.1") (deps (list (crate-dep (name "cc") (req "^1") (features (quote ("parallel"))) (default-features #t) (target "cfg(not(target_arch = \"wasm32\"))") (kind 1)) (crate-dep (name "js-sys") (req "^0.3.10") (default-features #t) (target "wasm32-unknown-unknown") (kind 0)) (crate-dep (name "wasm-bindgen") (req "^0.2.33") (default-features #t) (target "wasm32-unknown-unknown") (kind 0)))) (hash "0brc258psj31hwmi3xg66ffk35g1pbw9kqz4snzvp7qzlrv1iib4") (features (quote (("msl") ("hlsl") ("glsl") ("default"))))))

