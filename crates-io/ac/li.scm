(define-module (crates-io ac li) #:use-module (crates-io))

(define-public crate-aclint-0.0.0 (crate (name "aclint") (vers "0.0.0") (hash "0jmbskg4q7hls1h31qg0z1g9wvdsbx3divgw397nrjhy890bl0ca")))

(define-public crate-aclivo-0.1 (crate (name "aclivo") (vers "0.1.0") (hash "1n6s5h68qkl1hcdsj48wljhy4jaimhspl203pidzm2bw9ivxlvwy")))

(define-public crate-aclivo-0.1 (crate (name "aclivo") (vers "0.1.1") (hash "1nn9vb2q50qff5rz5vr51rxvgs8b64vgfs10gbyq99s9fx8pc8q2")))

