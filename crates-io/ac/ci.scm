(define-module (crates-io ac ci) #:use-module (crates-io))

(define-public crate-accio-0.1 (crate (name "accio") (vers "0.1.0") (deps (list (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (features (quote ("full" "fold"))) (default-features #t) (kind 0)))) (hash "15b07bypflswj22v49qwy9s571f32ldymwz6h4sknvzjpz88r26z")))

