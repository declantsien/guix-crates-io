(define-module (crates-io ac ho) #:use-module (crates-io))

(define-public crate-achoo-1 (crate (name "achoo") (vers "1.0.0") (deps (list (crate-dep (name "anyhow") (req "^1.0.70") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^4.2.1") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "0vf0fvy5x81r06a5xg25qiymqgrxkbpy4qw3hmvn9njxq2ps7xz4")))

(define-public crate-achoo-1 (crate (name "achoo") (vers "1.0.1") (deps (list (crate-dep (name "anyhow") (req "^1.0.70") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^4.2.1") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "18ky879v0xmvfy6rwa0m9lh5vrvsxn4nrz8n8lgw6i7ir8h0v3gq")))

