(define-module (crates-io ac tu) #:use-module (crates-io))

(define-public crate-actual_lix-0.1 (crate (name "actual_lix") (vers "0.1.0") (hash "16nlg1n141h40wwz2kglvksxn2zmyx10zjky7gnzya5gg4idwzx8") (yanked #t)))

(define-public crate-actual_lix-0.1 (crate (name "actual_lix") (vers "0.1.1") (hash "15xj2pwpkvgkcs7yzji6gwhiy774gkj71nljbynbsw91q391gqhn")))

(define-public crate-actually-0.1 (crate (name "actually") (vers "0.1.1") (hash "08sjczf7xg7zqp35p37vx1dg54jnnr41nhsr0hjnl6av494lv72a")))

(define-public crate-actuary-0.1 (crate (name "actuary") (vers "0.1.0") (hash "1jkygbda9lx5kpppif7ilfqqhlccm451pkbdhbkfp95x6f7sc7w5") (yanked #t)))

(define-public crate-actuate-0.1 (crate (name "actuate") (vers "0.1.0") (hash "095c05r8x7azk0ps6wsjay09dh5i91k5jdmqr7mp7gmfhprsbcz3")))

(define-public crate-actuate-0.2 (crate (name "actuate") (vers "0.2.0-alpha.1") (deps (list (crate-dep (name "hashbrown") (req "^0.14.3") (default-features #t) (kind 0)))) (hash "186assa7ja1gfkymdr89abwv18ylp78ychm57d5bm9a4rlcr57nd")))

(define-public crate-actuate-0.2 (crate (name "actuate") (vers "0.2.0") (deps (list (crate-dep (name "hashbrown") (req "^0.14.3") (default-features #t) (kind 0)))) (hash "0v4g911h5c6nm8id4yvd2r7p859ygiqidpgc7rl0vpm29ihrybjg")))

(define-public crate-actuate-0.3 (crate (name "actuate") (vers "0.3.0-alpha.1") (deps (list (crate-dep (name "slotmap") (req "^1.0.7") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.37.0") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "12w3yslx553nc4v75lfcw04smrimgay75ar5a3hnga115qb3mgyh")))

(define-public crate-actuate-0.3 (crate (name "actuate") (vers "0.3.0-alpha.2") (deps (list (crate-dep (name "slotmap") (req "^1.0.7") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.37.0") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "18x0kai1l6d3ivp9jdnzh9zs22k3fvbnsi7x9j2q3ayb4zbv13j1")))

(define-public crate-actuate-0.3 (crate (name "actuate") (vers "0.3.0-alpha.3") (deps (list (crate-dep (name "slotmap") (req "^1.0.7") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.37.0") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "13iylifgbqr3072091lv9q44psqnpx0168my8fqn12c816a1dxbm")))

(define-public crate-actuate-0.3 (crate (name "actuate") (vers "0.3.0-alpha.4") (deps (list (crate-dep (name "slotmap") (req "^1.0.7") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.37.0") (features (quote ("full"))) (default-features #t) (kind 2)))) (hash "0fy6cdjd4ymgl47wsv9h0daqi5iq2q3cpcdl6z79nwh9fn2nnf3n")))

