(define-module (crates-io ih ex) #:use-module (crates-io))

(define-public crate-ihex-0.1 (crate (name "ihex") (vers "0.1.0") (hash "0gmwg9169j4z0p1pzciw4grbcnx1knd5iraxk1n2s815w1wsm763")))

(define-public crate-ihex-0.1 (crate (name "ihex") (vers "0.1.1") (hash "09a4by68x2c0l0gxmd7r97f78a9kpvlz7bxlpdfrvw0gy285zlvf")))

(define-public crate-ihex-0.1 (crate (name "ihex") (vers "0.1.2") (hash "1w7z8d9zisqvz34j6jxwclx8khnyarlv9zys29a6ip2g0zwpwf93")))

(define-public crate-ihex-0.1 (crate (name "ihex") (vers "0.1.3") (hash "0wbw3qgp9x5bzj8rkd21qc7fsykr3ik10h74mvly5fgz3sq50lfd")))

(define-public crate-ihex-1 (crate (name "ihex") (vers "1.0.0") (hash "17s4jc55g5wm9zh1pw78dsp0mlsxny2jlnby4hyjikxrmzph02fd")))

(define-public crate-ihex-1 (crate (name "ihex") (vers "1.0.1") (hash "0xgxxh8a0ki6d8xp75p38ixwzk3ivd1g7vhvhi9b15gmk0n5n2b7")))

(define-public crate-ihex-1 (crate (name "ihex") (vers "1.0.2") (hash "0vpph6awp0p6hj8ffpvxlh2i1iwcr6zip9v3qc3w49vcpasph0qd")))

(define-public crate-ihex-1 (crate (name "ihex") (vers "1.1.0") (hash "0adnvks74dsih0y8ai2i3dxs64yanqq0swsrzvnk980dympar3ld")))

(define-public crate-ihex-1 (crate (name "ihex") (vers "1.1.1") (hash "11q5cwwysb871hyvz194b14avx01iy9hcx8s7zh6rf1aw5gps1p5")))

(define-public crate-ihex-1 (crate (name "ihex") (vers "1.1.2") (hash "1mc9r39ycxlldnpw125sv3izcffwzxzpl9msayrxzyrsphgyr54k")))

(define-public crate-ihex-2 (crate (name "ihex") (vers "2.0.0") (hash "1fn32hryvg8lw0kjpq4zd55dzdmr35q7jcswx1xhlwv6632wr6sk") (yanked #t)))

(define-public crate-ihex-3 (crate (name "ihex") (vers "3.0.0") (hash "1wlzfyy5fsqgpki5vdapw0jjczqdm6813fgd3661wf5vfi3phnin")))

(define-public crate-ihex-merge-0.1 (crate (name "ihex-merge") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^4.4.11") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "ihex") (req "^3.0.0") (default-features #t) (kind 0)))) (hash "17ig586awpxsvlyyhflmwsmciq1zrinnzrydi76195x2crpcr1b1")))

(define-public crate-ihex_ext-1 (crate (name "ihex_ext") (vers "1.0.0") (deps (list (crate-dep (name "ihex") (req "^3.0.0") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.0") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.2") (default-features #t) (kind 0)))) (hash "0zbixk97w0v17l12xda0pzskibwc7qj1bs2xpfqbkmbv7qzismwn")))

