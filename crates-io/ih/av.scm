(define-module (crates-io ih av) #:use-module (crates-io))

(define-public crate-ihavecoke-ufo-0.1 (crate (name "ihavecoke-ufo") (vers "0.1.0") (deps (list (crate-dep (name "url") (req "^2.4.1") (default-features #t) (kind 0)))) (hash "156c0qkdpcjpq0mp2g6iy1cpmr6lsfgd3p3hykfz24z8vnwx1c1i") (yanked #t)))

