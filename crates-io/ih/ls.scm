(define-module (crates-io ih ls) #:use-module (crates-io))

(define-public crate-ihlsh-0.0.1 (crate (name "ihlsh") (vers "0.0.1") (deps (list (crate-dep (name "exec") (req "^0.3.1") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2.132") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "users") (req "^0.11.0") (default-features #t) (kind 0)))) (hash "0711d8mri6a3mjsvp02zy783jgj0q3hv7nqvz5vpvrq7qgmk9zmp") (yanked #t)))

(define-public crate-ihlsh-0.0.2 (crate (name "ihlsh") (vers "0.0.2") (deps (list (crate-dep (name "exec") (req "^0.3.1") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2.132") (default-features #t) (kind 0)) (crate-dep (name "rustyline") (req "^10.0.0") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "users") (req "^0.11.0") (default-features #t) (kind 0)))) (hash "0mxl0vim7jnxh6wzg2ksx71r47xsf137g8njdb50nslg46yf1081") (yanked #t)))

