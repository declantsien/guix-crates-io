(define-module (crates-io ix dt) #:use-module (crates-io))

(define-public crate-ixdtf-0.0.0 (crate (name "ixdtf") (vers "0.0.0") (deps (list (crate-dep (name "criterion") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "serde-json-core") (req "^0.4") (features (quote ("std"))) (default-features #t) (kind 2)))) (hash "0x6jkihqacziqwvnl7gi3yn710gyxp1kx6fxbrdlgvg79jlss9sz")))

(define-public crate-ixdtf-0.1 (crate (name "ixdtf") (vers "0.1.0") (deps (list (crate-dep (name "criterion") (req "^0.4") (default-features #t) (kind 2)) (crate-dep (name "serde-json-core") (req "^0.4") (features (quote ("std"))) (default-features #t) (kind 2)))) (hash "0rvk1dzy83xlxnyqjz6vwjrzvizkrvg7w0wld5nd0h7djwkhyzsr")))

(define-public crate-ixdtf-0.2 (crate (name "ixdtf") (vers "0.2.0") (deps (list (crate-dep (name "criterion") (req "^0.5.0") (default-features #t) (target "cfg(not(target_arch = \"wasm32\"))") (kind 2)) (crate-dep (name "displaydoc") (req "^0.2.3") (kind 0)) (crate-dep (name "serde-json-core") (req "^0.4.0") (features (quote ("std"))) (kind 2)))) (hash "1wzvd8s50g3bxi2ynl2wr4kh3nc00a036p605x8ajlklj9gk08r1") (features (quote (("duration") ("default" "duration")))) (rust-version "1.67")))

