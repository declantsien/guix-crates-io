(define-module (crates-io ix -r) #:use-module (crates-io))

(define-public crate-ix-rs-0.1 (crate (name "ix-rs") (vers "0.1.0") (hash "0p969fwwkssvivyvidmlmcypm4mih94ik2x2jjrf040l6kmd4hfq") (yanked #t)))

(define-public crate-ix-rs-0.2 (crate (name "ix-rs") (vers "0.2.0") (hash "190z14hasfv9rayvbi05r0ja4jzngg7p743193cgd5sy02gid1ni") (yanked #t)))

(define-public crate-ix-rs-0.3 (crate (name "ix-rs") (vers "0.3.0") (hash "103i9vfgh8hm7jmrg2g2zrzf3s5625f008mgkmmjw3g6vlpqs376") (yanked #t)))

(define-public crate-ix-rs-0.3 (crate (name "ix-rs") (vers "0.3.1") (hash "1ixpjdp3bld2gwzk948cm778zx5kdh9qzmm68pcqzmhw78r5mnhi") (yanked #t)))

(define-public crate-ix-rs-0.4 (crate (name "ix-rs") (vers "0.4.0") (hash "1pxzgr0yf1vhp05h4mn7lk80ni54fkzg6xgz6m5fpjwvi1w3qi7y") (yanked #t)))

(define-public crate-ix-rs-0.5 (crate (name "ix-rs") (vers "0.5.0") (deps (list (crate-dep (name "paste") (req "^1.0.12") (default-features #t) (kind 2)) (crate-dep (name "proptest") (req "^1.1.0") (default-features #t) (kind 2)))) (hash "1d4zinm5nl4z21bcb4wz0wzvdrfis4rqimbgsskc2mf8qwhc9wpi")))

(define-public crate-ix-rs-0.6 (crate (name "ix-rs") (vers "0.6.0") (deps (list (crate-dep (name "paste") (req "^1.0.12") (default-features #t) (kind 2)) (crate-dep (name "proptest") (req "^1.1.0") (default-features #t) (kind 2)))) (hash "0h5qifj4xbqs18yd7hxvkphqasncnm03b2492apsshxwficim5fr")))

(define-public crate-ix-rs-0.6 (crate (name "ix-rs") (vers "0.6.1") (deps (list (crate-dep (name "paste") (req "^1.0.12") (default-features #t) (kind 2)) (crate-dep (name "proptest") (req "^1.1.0") (default-features #t) (kind 2)))) (hash "06qw0bx0p06z2a0b7biwz80iv4ld63jyb5yw83sws5ij0g0j8451")))

(define-public crate-ix-rs-0.6 (crate (name "ix-rs") (vers "0.6.2") (deps (list (crate-dep (name "paste") (req "^1.0.12") (default-features #t) (kind 2)) (crate-dep (name "proptest") (req "^1.1.0") (default-features #t) (kind 2)))) (hash "1a1p6c137i9jijvchazaravw5va9xw35npqzlb3832j3mv603mh1")))

(define-public crate-ix-rs-0.6 (crate (name "ix-rs") (vers "0.6.3") (deps (list (crate-dep (name "paste") (req "^1.0.12") (default-features #t) (kind 2)) (crate-dep (name "proptest") (req "^1.1.0") (default-features #t) (kind 2)))) (hash "034bavj8yv669hcs7k1vkplx9khahm3lqr29g5mypspkxnlmdj1w")))

(define-public crate-ix-rs-0.7 (crate (name "ix-rs") (vers "0.7.0") (deps (list (crate-dep (name "paste") (req "^1.0.12") (default-features #t) (kind 2)) (crate-dep (name "proptest") (req "^1.1.0") (default-features #t) (kind 2)))) (hash "1ndl4rrah7my1ylcyc55dn5mkw60ff11aa47zkzwf67sisicpd1s")))

(define-public crate-ix-rs-0.7 (crate (name "ix-rs") (vers "0.7.1") (deps (list (crate-dep (name "paste") (req "^1.0.12") (default-features #t) (kind 2)) (crate-dep (name "proptest") (req "^1.1.0") (default-features #t) (kind 2)))) (hash "0habz1qdn97fhj85mkj9cdqv6069vzm9i8bghrp65j8702innm19")))

(define-public crate-ix-rs-0.7 (crate (name "ix-rs") (vers "0.7.2") (deps (list (crate-dep (name "paste") (req "^1.0.12") (default-features #t) (kind 2)) (crate-dep (name "proptest") (req "^1.1.0") (default-features #t) (kind 2)))) (hash "0mkmmw001bnzm7ww13qspfb7k67yd4j7s6mnszr0vkqg0kn4lpsk")))

(define-public crate-ix-rs-0.7 (crate (name "ix-rs") (vers "0.7.3") (deps (list (crate-dep (name "paste") (req "^1.0.12") (default-features #t) (kind 2)) (crate-dep (name "proptest") (req "^1.1.0") (default-features #t) (kind 2)))) (hash "16camac7nl9489i05nd7abasw370rj71m2c7bifnz311pxp895z9")))

(define-public crate-ix-rs-0.7 (crate (name "ix-rs") (vers "0.7.4") (deps (list (crate-dep (name "paste") (req "^1.0.12") (default-features #t) (kind 2)) (crate-dep (name "proptest") (req "^1.1.0") (default-features #t) (kind 2)))) (hash "04jxi458m6c092z9c2a4a5q9jkd7x0sd31bk20jh36qxq3ada8yy")))

(define-public crate-ix-rs-0.8 (crate (name "ix-rs") (vers "0.8.0") (deps (list (crate-dep (name "paste") (req "^1.0.12") (default-features #t) (kind 2)) (crate-dep (name "proptest") (req "^1.1.0") (default-features #t) (kind 2)))) (hash "1rgkivfnjnnl564vd1ada1921hpkm5p0gr4i7znraxxsnx05zmg3")))

