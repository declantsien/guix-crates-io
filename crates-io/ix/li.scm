(define-module (crates-io ix li) #:use-module (crates-io))

(define-public crate-ixlist-0.1 (crate (name "ixlist") (vers "0.1.0") (deps (list (crate-dep (name "bencher") (req "^0.1") (default-features #t) (kind 2)) (crate-dep (name "itertools") (req "^0.6.2") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.3") (default-features #t) (kind 2)))) (hash "0y7hfidgr4azwi97q3gb75aaki7rppnk0pglg6yk57y524c5jmc2")))

