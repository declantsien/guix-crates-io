(define-module (crates-io l8 as) #:use-module (crates-io))

(define-public crate-l8ash-0.1 (crate (name "l8ash") (vers "0.1.0") (deps (list (crate-dep (name "ring") (req "^0.16.20") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.185") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0.185") (default-features #t) (kind 0)) (crate-dep (name "toml") (req "^0.7.6") (default-features #t) (kind 0)) (crate-dep (name "ttyui") (req "^0.1.4") (default-features #t) (kind 0)))) (hash "1lgdkpfc891dfp7r9czakhp2i5a37rh9lcfh4d6kfikjg0nnjf8l")))

(define-public crate-l8ash-0.1 (crate (name "l8ash") (vers "0.1.1") (deps (list (crate-dep (name "ring") (req "^0.16.20") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.185") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0.185") (default-features #t) (kind 0)) (crate-dep (name "toml") (req "^0.7.6") (default-features #t) (kind 0)) (crate-dep (name "ttyui") (req "^0.1.4") (default-features #t) (kind 0)))) (hash "0grbjjjn12wslwsiyw9ypp39pl7rndavl9qdrb0c26ld5xbz13vj")))

