(define-module (crates-io hy gg) #:use-module (crates-io))

(define-public crate-hygge-0.1 (crate (name "hygge") (vers "0.1.0") (hash "1mqqz55b2np0mqyk9qqlzmh1bvj2flcrqa8fc3i5vd01kzgizmkq")))

(define-public crate-hygge-0.3 (crate (name "hygge") (vers "0.3.0") (deps (list (crate-dep (name "kdl") (req "^3.0.0") (default-features #t) (kind 0)))) (hash "191qfx1i98400cv3a7yjjx83jahxqhh6lyn1kbvcn8l1w18l3ika")))

(define-public crate-hygge-0.3 (crate (name "hygge") (vers "0.3.1") (deps (list (crate-dep (name "kdl") (req "^3.0.0") (default-features #t) (kind 0)))) (hash "1di0wma8szm44l2smhbjsf9j0jph5hzn60ls1ww50qc1nznbd5pc")))

(define-public crate-hygge-0.4 (crate (name "hygge") (vers "0.4.0") (deps (list (crate-dep (name "kdl") (req "^3.0.0") (default-features #t) (kind 0)))) (hash "0m3mvzifgkb5vy5ij049ic53sacv0hnxrq4cfzjs5fia1gi4p6ni")))

