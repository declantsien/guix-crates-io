(define-module (crates-io hy st) #:use-module (crates-io))

(define-public crate-hysteresis-0.1 (crate (name "hysteresis") (vers "0.1.0") (deps (list (crate-dep (name "rstest") (req "^0.15.0") (default-features #t) (kind 2)))) (hash "0wkvdl9pz4hidmc3mr6b1836kdgmrwzbhj7cskbmi95w7010b3hd")))

(define-public crate-hysteresis-0.1 (crate (name "hysteresis") (vers "0.1.1") (deps (list (crate-dep (name "rstest") (req "^0.15.0") (default-features #t) (kind 2)))) (hash "0i3cvbv02m1c7xlyr3avpxly36d3409qqp5mif2fygh1qdng2i24")))

(define-public crate-hysteresis-0.2 (crate (name "hysteresis") (vers "0.2.0") (deps (list (crate-dep (name "num-traits") (req "^0.2.15") (features (quote ("libm"))) (kind 0)) (crate-dep (name "rstest") (req "^0.15.0") (default-features #t) (kind 2)))) (hash "14qav7jwl8k111lgvrm246kp7vvniafj6m60h0lf51qh5a6xkkmy")))

(define-public crate-hysteresis-0.3 (crate (name "hysteresis") (vers "0.3.0") (deps (list (crate-dep (name "num-traits") (req "^0.2.15") (features (quote ("libm"))) (kind 0)) (crate-dep (name "rstest") (req "^0.15.0") (default-features #t) (kind 2)))) (hash "11jcras35fzcia9jsiwlmjv9c1hlrc34lwxy8hj2wqyw192izm9i")))

(define-public crate-hysteresis-0.3 (crate (name "hysteresis") (vers "0.3.1") (deps (list (crate-dep (name "num-traits") (req "^0.2.15") (features (quote ("libm"))) (kind 0)) (crate-dep (name "rstest") (req "^0.15.0") (default-features #t) (kind 2)))) (hash "1yddbfdr3rqkpcyzmyc2wrd7plb1sk2nfrb55svn7wzdikq4bizs")))

(define-public crate-hysteresis-0.4 (crate (name "hysteresis") (vers "0.4.0") (deps (list (crate-dep (name "num-traits") (req "^0.2.15") (features (quote ("libm"))) (kind 0)) (crate-dep (name "rstest") (req "^0.15.0") (default-features #t) (kind 2)))) (hash "12ajra29lhnjx9vhh8d9rjprlhy6l7w7d7q7170w4z0ck7w4lnj4")))

(define-public crate-hysteresis-0.5 (crate (name "hysteresis") (vers "0.5.0") (deps (list (crate-dep (name "num-traits") (req "^0.2.15") (features (quote ("libm"))) (kind 0)) (crate-dep (name "rstest") (req "^0.15.0") (default-features #t) (kind 2)))) (hash "048pyks1mxsx2d6qjwac4xn3nj6sq45s47cfl56lijacb8sqwfr2")))

