(define-module (crates-io hy ta) #:use-module (crates-io))

(define-public crate-hytale-0.1 (crate (name "hytale") (vers "0.1.0") (hash "1hj0xh0ldzly2z9znb4y1wklwbzwf20bfiiydj6lpcw7icxlgx6s") (yanked #t)))

(define-public crate-hytale-protocol-0.1 (crate (name "hytale-protocol") (vers "0.1.0") (hash "1sz7nlz54r5kdqyh7ih9f3p67wcficpalx709244ay4vp9q8l6mb") (yanked #t)))

