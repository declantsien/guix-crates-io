(define-module (crates-io hy de) #:use-module (crates-io))

(define-public crate-hyde-0.1 (crate (name "hyde") (vers "0.1.0") (deps (list (crate-dep (name "handlebars") (req "^3.5.2") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.3.21") (default-features #t) (kind 0)))) (hash "1hqczci8jc9x3881hjsjirhr7ahvm3zfinmdxg5siiapn2p53ww6") (yanked #t)))

