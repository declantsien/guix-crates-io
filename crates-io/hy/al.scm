(define-module (crates-io hy al) #:use-module (crates-io))

(define-public crate-hyaline-smr-0.1 (crate (name "hyaline-smr") (vers "0.1.0") (deps (list (crate-dep (name "atomicdouble") (req "^0.1.4") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "loom") (req "^0.5") (features (quote ("checkpoint"))) (default-features #t) (target "cfg(loom)") (kind 0)) (crate-dep (name "rand") (req "^0.8.4") (default-features #t) (kind 2)))) (hash "0xlkby5qmff6ndsl20bmqnm7rag8wkqcwrvwhqdzbmnj2fmrznvp")))

(define-public crate-hyaline-smr-0.1 (crate (name "hyaline-smr") (vers "0.1.1") (deps (list (crate-dep (name "atomicdouble") (req "^0.1.4") (default-features #t) (kind 0)) (crate-dep (name "loom") (req "^0.5") (features (quote ("checkpoint"))) (default-features #t) (target "cfg(loom)") (kind 0)) (crate-dep (name "rand") (req "^0.8.4") (default-features #t) (kind 2)))) (hash "0fd2lvfcvj2zkd2n18lbzk6kgsywpz1rp0x4vwngmr7ixcv5jq9c")))

