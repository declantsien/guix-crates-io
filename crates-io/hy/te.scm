(define-module (crates-io hy te) #:use-module (crates-io))

(define-public crate-hyte-0.1 (crate (name "hyte") (vers "0.1.0") (deps (list (crate-dep (name "statrs") (req "^0.16.0") (default-features #t) (kind 0)))) (hash "0g66ff0b0a5iw1k1189760j031djzmlsdl77yrb8sbmbr34kfa57")))

(define-public crate-hyte-0.1 (crate (name "hyte") (vers "0.1.1") (deps (list (crate-dep (name "statrs") (req "^0.16.0") (default-features #t) (kind 0)))) (hash "13vjn1ny1zbq2pnphigsqycqpqf1xq1dhikpycjndpncyvdmnhc3")))

