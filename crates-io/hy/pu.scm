(define-module (crates-io hy pu) #:use-module (crates-io))

(define-public crate-hypua-0.1 (crate (name "hypua") (vers "0.1.0") (deps (list (crate-dep (name "phf") (req "^0.8.0") (kind 0)) (crate-dep (name "phf_codegen") (req "^0.8.0") (default-features #t) (kind 1)))) (hash "019q0xj53hpg553lcrhihnkng82i7iqz5f180c432haw6cpswr4f")))

(define-public crate-hypua-0.2 (crate (name "hypua") (vers "0.2.0") (deps (list (crate-dep (name "phf") (req "^0.8.0") (kind 0)) (crate-dep (name "phf_codegen") (req "^0.8.0") (default-features #t) (kind 1)))) (hash "04c60yflvrimhk3gsm32yk75rmkpnhan7000wld5y536fff938mb")))

