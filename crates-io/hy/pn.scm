(define-module (crates-io hy pn) #:use-module (crates-io))

(define-public crate-hypn-gameset-0.1 (crate (name "hypn-gameset") (vers "0.1.6") (deps (list (crate-dep (name "anchor-lang") (req "^0.18.2") (default-features #t) (kind 0)) (crate-dep (name "solrandhypn") (req "^0.1.7") (features (quote ("cpi"))) (default-features #t) (kind 0)))) (hash "0vf5b2yy98v8y06ms1f7dyf8l9zd302hryi77wjfx3dh3ys2ylmn") (features (quote (("no-idl") ("no-entrypoint") ("default") ("cpi" "no-entrypoint"))))))

(define-public crate-hypnos-0.1 (crate (name "hypnos") (vers "0.1.0") (hash "0vsby1ck7zcfg2wrp3y7k68vhwdjcysqiygmf3hpfg116s11h1kp")))

