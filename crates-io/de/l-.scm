(define-module (crates-io de l-) #:use-module (crates-io))

(define-public crate-del-cad-0.1 (crate (name "del-cad") (vers "0.1.0") (deps (list (crate-dep (name "del-dtri") (req "^0.1.1") (default-features #t) (kind 0)) (crate-dep (name "nalgebra") (req "^0.32.3") (default-features #t) (kind 0)))) (hash "1x89k73ggj6g8bzkjxd5x9fsh56ka6fc3rrglqkx21r1h2wdhfaf")))

(define-public crate-del-cad-0.1 (crate (name "del-cad") (vers "0.1.1") (deps (list (crate-dep (name "del-dtri") (req "^0.1.2") (default-features #t) (kind 0)) (crate-dep (name "nalgebra") (req "^0.32.3") (default-features #t) (kind 0)))) (hash "0si3qw8lingvl6r7kr4gaw4dcsnqsm5nlcj3mrwmhjlb6dqnlnpd")))

(define-public crate-del-cad-0.1 (crate (name "del-cad") (vers "0.1.2") (deps (list (crate-dep (name "del-dtri") (req "^0.1.2") (default-features #t) (kind 0)) (crate-dep (name "del-geo") (req "^0.1.5") (default-features #t) (kind 0)) (crate-dep (name "nalgebra") (req "^0.32.3") (default-features #t) (kind 0)) (crate-dep (name "ordered-float") (req "^3.9.2") (default-features #t) (kind 0)))) (hash "0aysywabad2bf09nc8m4hripc8azwhk5yvk2ix5ilcb20sab7p3a")))

(define-public crate-del-canvas-0.1 (crate (name "del-canvas") (vers "0.1.0") (deps (list (crate-dep (name "gif") (req "^0.12.0") (default-features #t) (kind 0)) (crate-dep (name "nalgebra") (req "^0.32.3") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2.17") (default-features #t) (kind 0)) (crate-dep (name "png") (req "^0.17.10") (default-features #t) (kind 0)))) (hash "0h2plfk64qzawv29wjfymnwhpa5vf056ljr1macdz3hda7w9plxw")))

(define-public crate-del-canvas-0.1 (crate (name "del-canvas") (vers "0.1.1") (deps (list (crate-dep (name "del-geo") (req "^0.1.22") (default-features #t) (kind 0)) (crate-dep (name "del-msh") (req "^0.1.28") (default-features #t) (kind 0)) (crate-dep (name "gif") (req "^0.13.1") (default-features #t) (kind 0)) (crate-dep (name "nalgebra") (req "^0.32.5") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2.18") (default-features #t) (kind 0)) (crate-dep (name "png") (req "^0.17.13") (default-features #t) (kind 0)))) (hash "0iwingpdf59pb43i1a02qva0pydwl9d2bjapr00gdzjxymgi8f8x")))

(define-public crate-del-dtri-0.1 (crate (name "del-dtri") (vers "0.1.0") (deps (list (crate-dep (name "nalgebra") (req "^0.31.2") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2.15") (default-features #t) (kind 0)))) (hash "15501sp0cwx8g99k96syiwj2cj2kcqi1zj29n92v0n2wd930jccs") (yanked #t)))

(define-public crate-del-dtri-0.1 (crate (name "del-dtri") (vers "0.1.1") (deps (list (crate-dep (name "nalgebra") (req "^0.32.3") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2.16") (default-features #t) (kind 0)))) (hash "15rwdrw9272mymy10i1sihrls18j53gcgcjnymx16mmn39whsas5") (yanked #t)))

(define-public crate-del-dtri-0.1 (crate (name "del-dtri") (vers "0.1.2") (deps (list (crate-dep (name "nalgebra") (req "^0.32.3") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2.16") (default-features #t) (kind 0)))) (hash "1mg05n6bl69zmdkqdrdkshxh7zgci6bfmj0n72wl3rffccm42931") (yanked #t)))

(define-public crate-del-dtri-0.1 (crate (name "del-dtri") (vers "0.1.3") (deps (list (crate-dep (name "nalgebra") (req "^0.32.3") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2.17") (default-features #t) (kind 0)))) (hash "0cd2gg4f21iflyg4xjapz395s7k32d58gp0bipik593dv47xf1r2") (yanked #t)))

(define-public crate-del-dtri-0.1 (crate (name "del-dtri") (vers "0.1.4") (deps (list (crate-dep (name "del-geo") (req "^0.1.20") (default-features #t) (kind 0)) (crate-dep (name "del-msh") (req "^0.1.25") (default-features #t) (kind 2)) (crate-dep (name "nalgebra") (req "^0.32.4") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2.18") (default-features #t) (kind 0)))) (hash "0r62y38x2cfpjylqwd6wk7a48gqyakrpvqnj8dgdan3x1fkrzll7") (yanked #t)))

(define-public crate-del-fem-0.1 (crate (name "del-fem") (vers "0.1.0") (deps (list (crate-dep (name "del-geo") (req "^0.1.18") (default-features #t) (kind 0)) (crate-dep (name "nalgebra") (req "^0.32.3") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2.18") (default-features #t) (kind 0)))) (hash "0klchjgfcwr7nvs5g7jgqlrilja5jwvn42q85cfxjnm562r781ka")))

(define-public crate-del-fem-0.1 (crate (name "del-fem") (vers "0.1.1") (deps (list (crate-dep (name "del-geo") (req "^0.1.18") (default-features #t) (kind 0)) (crate-dep (name "del-msh") (req "^0.1.25") (default-features #t) (kind 2)) (crate-dep (name "nalgebra") (req "^0.32.3") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2.18") (default-features #t) (kind 0)))) (hash "1d7bzlq1v8sd0lrwmcz6g767z4a8453zvl2iw76csg5pc93ci22v")))

(define-public crate-del-fem-0.1 (crate (name "del-fem") (vers "0.1.2") (deps (list (crate-dep (name "del-geo") (req "^0.1.19") (default-features #t) (kind 0)) (crate-dep (name "del-msh") (req "^0.1.25") (default-features #t) (kind 2)) (crate-dep (name "itertools") (req "^0.12.1") (default-features #t) (kind 0)) (crate-dep (name "nalgebra") (req "^0.32.4") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2.18") (default-features #t) (kind 0)))) (hash "1jjljz2nwhq8b8pj2jcc0z731j14l0acc48hy49qlarfj1bsp3x5")))

(define-public crate-del-fem-0.1 (crate (name "del-fem") (vers "0.1.3") (deps (list (crate-dep (name "del-geo") (req "^0.1.20") (default-features #t) (kind 0)) (crate-dep (name "del-msh") (req "^0.1.25") (default-features #t) (kind 2)) (crate-dep (name "itertools") (req "^0.12.1") (default-features #t) (kind 0)) (crate-dep (name "nalgebra") (req "^0.32.4") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2.18") (default-features #t) (kind 0)))) (hash "08rxh183mkx0vyyvhizzb4sb2bckkyrh526rm7cm5rpir05krw2c")))

(define-public crate-del-geo-0.1 (crate (name "del-geo") (vers "0.1.0") (deps (list (crate-dep (name "num-traits") (req "^0.2.15") (default-features #t) (kind 0)))) (hash "0xmwh6z1dkbl6z2qx7viq5b9lkgki3yg0xbx90s2b14g18s60x81")))

(define-public crate-del-geo-0.1 (crate (name "del-geo") (vers "0.1.1") (deps (list (crate-dep (name "num-traits") (req "^0.2.15") (default-features #t) (kind 0)))) (hash "0yv76x8cqwkzw5vkr2m1snnmshxfhnfidfxxink264fd9vm7zqhc")))

(define-public crate-del-geo-0.1 (crate (name "del-geo") (vers "0.1.2") (deps (list (crate-dep (name "nalgebra") (req "^0.32.3") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2.16") (default-features #t) (kind 0)))) (hash "1vw48p53362ysb75ddp9i3jhdnzmgxn4al6262fzwms77hbvpg9n")))

(define-public crate-del-geo-0.1 (crate (name "del-geo") (vers "0.1.3") (deps (list (crate-dep (name "nalgebra") (req "^0.32.3") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2.16") (default-features #t) (kind 0)))) (hash "1w79r7aa6yki11ys7a8g51raslz2vlbbfk15ajq1iy6i2rgcdbjh")))

(define-public crate-del-geo-0.1 (crate (name "del-geo") (vers "0.1.4") (deps (list (crate-dep (name "nalgebra") (req "^0.32.3") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2.16") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)))) (hash "1g4whl3hv525w1wqal5vl8k8v6i5j7vgqabj1m9gj7kc9xsc83zk")))

(define-public crate-del-geo-0.1 (crate (name "del-geo") (vers "0.1.5") (deps (list (crate-dep (name "nalgebra") (req "^0.32.3") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2.16") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)))) (hash "0l3wxj5k3qzcg24xfd0maz7k26600p9aj3kaad0pbmywfgwvh47r")))

(define-public crate-del-geo-0.1 (crate (name "del-geo") (vers "0.1.6") (deps (list (crate-dep (name "nalgebra") (req "^0.32.3") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2.16") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)))) (hash "0m4k2n95x0jbilf9894xjbch5pa217c21js1xlkzs3gnqcwq82mm")))

(define-public crate-del-geo-0.1 (crate (name "del-geo") (vers "0.1.7") (deps (list (crate-dep (name "nalgebra") (req "^0.32.3") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2.17") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)))) (hash "0gxfz5hzri3rxmc62h84q2mphidnxs4kh0aswk6lkv07sdd0j7bj")))

(define-public crate-del-geo-0.1 (crate (name "del-geo") (vers "0.1.8") (deps (list (crate-dep (name "nalgebra") (req "^0.32.3") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2.17") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)))) (hash "0zcm3gkyfcmy8irzjmqlnk6b9hfkjrjqwxl42yqw3ricm45la7bw")))

(define-public crate-del-geo-0.1 (crate (name "del-geo") (vers "0.1.9") (deps (list (crate-dep (name "nalgebra") (req "^0.32.3") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2.17") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)))) (hash "1vfjwhvn2n7fryd4yqv7daz6iscpr7bjfnq2k61r0j61493rdylx")))

(define-public crate-del-geo-0.1 (crate (name "del-geo") (vers "0.1.10") (deps (list (crate-dep (name "nalgebra") (req "^0.32.3") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2.17") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)))) (hash "0vvygn1n5790qjjm9c7ivb6bpdlr2zcvq408vxsrcvawcizm55g6")))

(define-public crate-del-geo-0.1 (crate (name "del-geo") (vers "0.1.11") (deps (list (crate-dep (name "nalgebra") (req "^0.32.3") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2.17") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)))) (hash "0nkrz0xvc3vnbaymf0w82g6pk9z42pz29lg5knz7fjggb093almp")))

(define-public crate-del-geo-0.1 (crate (name "del-geo") (vers "0.1.12") (deps (list (crate-dep (name "nalgebra") (req "^0.32.3") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2.17") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)))) (hash "1q62rwd3v0z0sc5lvmd3ihhl8aakcxm392swr58caadjzlyv36wd")))

(define-public crate-del-geo-0.1 (crate (name "del-geo") (vers "0.1.13") (deps (list (crate-dep (name "nalgebra") (req "^0.32.3") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2.17") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)))) (hash "1m7z03z4xg8gscsi8alslza07qqsd92m20l5h8kalnqfasvhy1gg")))

(define-public crate-del-geo-0.1 (crate (name "del-geo") (vers "0.1.14") (deps (list (crate-dep (name "nalgebra") (req "^0.32.3") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2.17") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)))) (hash "1ga71pxizxqik4rv12frdf621v7v409ps85yf166w9ng6x183y8v")))

(define-public crate-del-geo-0.1 (crate (name "del-geo") (vers "0.1.15") (deps (list (crate-dep (name "nalgebra") (req "^0.32.3") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2.17") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)))) (hash "0xsr24lwsfr3rf3knq5c2nncb1iskawyrgkjcv198wmc9nwc0zhy")))

(define-public crate-del-geo-0.1 (crate (name "del-geo") (vers "0.1.16") (deps (list (crate-dep (name "nalgebra") (req "^0.32.3") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2.17") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)))) (hash "0mcj44wx8anyyr1008zkzphhasazsmbzxla8cg7yq7djzi3q5kcn")))

(define-public crate-del-geo-0.1 (crate (name "del-geo") (vers "0.1.17") (deps (list (crate-dep (name "nalgebra") (req "^0.32.3") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2.18") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)))) (hash "05v6spvrnhn672ag57c5v21hrigyh3d41b13xc2z7y392vdfsbvg")))

(define-public crate-del-geo-0.1 (crate (name "del-geo") (vers "0.1.18") (deps (list (crate-dep (name "nalgebra") (req "^0.32.3") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2.18") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)))) (hash "102np8i85ay6p8b5krghwph6x2ldggx4avj0nj8zir86vrj0zqvw")))

(define-public crate-del-geo-0.1 (crate (name "del-geo") (vers "0.1.19") (deps (list (crate-dep (name "nalgebra") (req "^0.32.4") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2.18") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)))) (hash "1yribm69n88qybmamflc6ai7whiq45p4g7w5v9bic0sw8x4f8apx")))

(define-public crate-del-geo-0.1 (crate (name "del-geo") (vers "0.1.20") (deps (list (crate-dep (name "nalgebra") (req "^0.32.4") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2.18") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)))) (hash "11p7d95cixfbv7c6wfxw6l0d72wxnvr4cgfsm1v9rlxldq7qpis7")))

(define-public crate-del-geo-0.1 (crate (name "del-geo") (vers "0.1.21") (deps (list (crate-dep (name "nalgebra") (req "^0.32.4") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2.18") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)))) (hash "1br8nafx5pvawgdwkk9rakkjzz355l6yp15fn5hvp29f018wdrr8")))

(define-public crate-del-geo-0.1 (crate (name "del-geo") (vers "0.1.22") (deps (list (crate-dep (name "nalgebra") (req "^0.32.5") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2.18") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)))) (hash "16zw3fqdx1rsc980267i4g073zqhkhwvh063sppp30p5dgkikydg")))

(define-public crate-del-geo-0.1 (crate (name "del-geo") (vers "0.1.23") (deps (list (crate-dep (name "nalgebra") (req "^0.32.5") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2.18") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)))) (hash "11080b8ghajqi8s7q5kp5zp7sdsz1h2fvf5baxmdb074xs688acn")))

(define-public crate-del-geo-0.1 (crate (name "del-geo") (vers "0.1.24") (deps (list (crate-dep (name "nalgebra") (req "^0.32.5") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2.19") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)))) (hash "10n2lmrxcwbwg5m1gy3fc4f0m9dqpsgihgy94sadfx8ycdmx05yh")))

(define-public crate-del-gl-0.1 (crate (name "del-gl") (vers "0.1.0") (deps (list (crate-dep (name "del-misc") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "gl_generator") (req "^0.14.0") (default-features #t) (kind 1)) (crate-dep (name "glutin") (req "^0.29.1") (default-features #t) (kind 0)))) (hash "0bd8aj6amb4b19q5gk455d163c19x5dpazngwswydxv7ik5cn1zr") (yanked #t)))

(define-public crate-del-gl-0.1 (crate (name "del-gl") (vers "0.1.1") (deps (list (crate-dep (name "del-misc") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "gl_generator") (req "^0.14.0") (default-features #t) (kind 1)) (crate-dep (name "glutin") (req "^0.29.1") (default-features #t) (kind 0)))) (hash "0y5ggg3hrd2h4kfq1llxq45y53r2wgwmw55xhd7a9pzw5xr2a4l9")))

(define-public crate-del-gl-0.1 (crate (name "del-gl") (vers "0.1.2") (deps (list (crate-dep (name "del-misc") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "gl_generator") (req "^0.14.0") (default-features #t) (kind 1)) (crate-dep (name "glutin") (req "^0.29.1") (default-features #t) (kind 0)))) (hash "18fcjqdmnyapj7mqsw4sfgkrhm5lb2lj6ppjj4n9hf546mpr9gjs")))

(define-public crate-del-ls-0.1 (crate (name "del-ls") (vers "0.1.0") (deps (list (crate-dep (name "nalgebra") (req "^0.31.2") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2.15") (default-features #t) (kind 0)))) (hash "1jb22mbwsyrj0hjh807dy4qwbzy6qpaccs71dp73rx14xpanh8il")))

(define-public crate-del-ls-0.1 (crate (name "del-ls") (vers "0.1.1") (deps (list (crate-dep (name "nalgebra") (req "^0.31.2") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2.15") (default-features #t) (kind 0)))) (hash "08h2x46r49lr42j17hra6r3alyszas18wfv7ymqpmhqqiiikvjk8")))

(define-public crate-del-ls-0.1 (crate (name "del-ls") (vers "0.1.2") (deps (list (crate-dep (name "nalgebra") (req "^0.31.2") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2.15") (default-features #t) (kind 0)))) (hash "1llvivg0gzfq0vjbhkwd7w7gx924bc0vp7mi5q1n27ph55d2y41a")))

(define-public crate-del-ls-0.1 (crate (name "del-ls") (vers "0.1.3") (deps (list (crate-dep (name "nalgebra") (req "^0.32.3") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2.17") (default-features #t) (kind 0)))) (hash "0z0pncf23lkzwrphrzm3zpbhl04czycfjy0bzmdb3pzhv6pxskq9")))

(define-public crate-del-ls-0.1 (crate (name "del-ls") (vers "0.1.4") (deps (list (crate-dep (name "nalgebra") (req "^0.32.3") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2.17") (default-features #t) (kind 0)))) (hash "0ncf2s9z4ipzjrcn0h0pzhhb62z78wgvapmbdpirxx9dd9xpxzpd")))

(define-public crate-del-misc-0.1 (crate (name "del-misc") (vers "0.1.0") (deps (list (crate-dep (name "nalgebra") (req "^0.31.2") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2.15") (default-features #t) (kind 0)))) (hash "1f2xnaq66rhsvr14bn9q9h6lv0hx7qbg88isl2wswf8vigkfvrcl")))

(define-public crate-del-misc-0.1 (crate (name "del-misc") (vers "0.1.1") (deps (list (crate-dep (name "array-init") (req "^2.0.1") (default-features #t) (kind 0)) (crate-dep (name "del-geo") (req "^0.1.1") (default-features #t) (kind 0)) (crate-dep (name "del-ls") (req "^0.1.2") (default-features #t) (kind 0)) (crate-dep (name "del-msh") (req "^0.1.5") (default-features #t) (kind 0)) (crate-dep (name "nalgebra") (req "^0.31.2") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2.15") (default-features #t) (kind 0)))) (hash "198dp39w60qppy7n08c87f2x0vk8q7zmzv6cmj80za2na7nw1c3x")))

(define-public crate-del-misc-0.1 (crate (name "del-misc") (vers "0.1.2") (deps (list (crate-dep (name "array-init") (req "^2.0.1") (default-features #t) (kind 0)) (crate-dep (name "del-geo") (req "^0.1.1") (default-features #t) (kind 0)) (crate-dep (name "del-ls") (req "^0.1.2") (default-features #t) (kind 0)) (crate-dep (name "del-msh") (req "^0.1.9") (default-features #t) (kind 0)) (crate-dep (name "nalgebra") (req "^0.32.2") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2.15") (default-features #t) (kind 0)))) (hash "0ixqdjiip866afljjhcm2j7ss468pbkvh6cgvl352jakvpkm27sw")))

(define-public crate-del-misc-0.1 (crate (name "del-misc") (vers "0.1.3") (deps (list (crate-dep (name "array-init") (req "^2.0.1") (default-features #t) (kind 0)) (crate-dep (name "del-geo") (req "^0.1.1") (default-features #t) (kind 0)) (crate-dep (name "del-ls") (req "^0.1.2") (default-features #t) (kind 0)) (crate-dep (name "del-msh") (req "^0.1.10") (default-features #t) (kind 0)) (crate-dep (name "nalgebra") (req "^0.32.2") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2.15") (default-features #t) (kind 0)))) (hash "13wxw0hqxcwfnlys3c0v38v5h9xgkrm2gzwdkkpv7li980nwrp4p")))

(define-public crate-del-misc-0.1 (crate (name "del-misc") (vers "0.1.4") (deps (list (crate-dep (name "array-init") (req "^2.0.1") (default-features #t) (kind 0)) (crate-dep (name "del-geo") (req "^0.1.4") (default-features #t) (kind 0)) (crate-dep (name "del-ls") (req "^0.1.2") (default-features #t) (kind 0)) (crate-dep (name "del-msh") (req "^0.1.11") (default-features #t) (kind 0)) (crate-dep (name "nalgebra") (req "^0.32.3") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2.15") (default-features #t) (kind 0)))) (hash "1sifi566k11fljqf0pq6pks1927sqzkx5ygfcqcc3h77i0ynr2p2")))

(define-public crate-del-msh-0.1 (crate (name "del-msh") (vers "0.1.0") (deps (list (crate-dep (name "num-traits") (req "^0.2.15") (default-features #t) (kind 0)))) (hash "0bk67a05yx9farxn74hka2a9sadhsd8n24ngshdlqsfjywh9kjrx")))

(define-public crate-del-msh-0.1 (crate (name "del-msh") (vers "0.1.1") (deps (list (crate-dep (name "num-traits") (req "^0.2.15") (default-features #t) (kind 0)))) (hash "0xszzwnaddkap8ycyr07k28anqgpxi80m29cm73d6qjri673ih1c")))

(define-public crate-del-msh-0.1 (crate (name "del-msh") (vers "0.1.2") (deps (list (crate-dep (name "num-traits") (req "^0.2.15") (default-features #t) (kind 0)))) (hash "1x9lzlgy342bcdsxs1x1c2rv1chxsg66m5dwh8bbc4j8dklapn6d")))

(define-public crate-del-msh-0.1 (crate (name "del-msh") (vers "0.1.3") (deps (list (crate-dep (name "num-traits") (req "^0.2.15") (default-features #t) (kind 0)))) (hash "1wjqiz3b2mf45av1qlxwxcpi1vhfrr3hpy89rz0bi9qibc8b6ybw")))

(define-public crate-del-msh-0.1 (crate (name "del-msh") (vers "0.1.4") (deps (list (crate-dep (name "num-traits") (req "^0.2.15") (default-features #t) (kind 0)))) (hash "03cf7cn37czcd3mi57h4w0q9cb093wsz9gkg1azi85n2xbk2lzb4")))

(define-public crate-del-msh-0.1 (crate (name "del-msh") (vers "0.1.5") (deps (list (crate-dep (name "num-traits") (req "^0.2.15") (default-features #t) (kind 0)))) (hash "1q1wgk1jsmby4dl1m9y2vbvbmrfrn8hba3bgc20z3igvgg087xf9")))

(define-public crate-del-msh-0.1 (crate (name "del-msh") (vers "0.1.6") (deps (list (crate-dep (name "num-traits") (req "^0.2.15") (default-features #t) (kind 0)))) (hash "1vxc2sl0jswkkfxbih4afq5hgw5y0rfb8gx4lb5dawgv7hqyr2g9")))

(define-public crate-del-msh-0.1 (crate (name "del-msh") (vers "0.1.7") (deps (list (crate-dep (name "num-traits") (req "^0.2.15") (default-features #t) (kind 0)))) (hash "0vz9avakkdxy14mkbhq1kppp267m2ifvbhhl82q3q00c1xbn2p5y")))

(define-public crate-del-msh-0.1 (crate (name "del-msh") (vers "0.1.8") (deps (list (crate-dep (name "num-traits") (req "^0.2.15") (default-features #t) (kind 0)))) (hash "15flxxmg3g1c5glmac3gp45c1ds0pc20ik1djydkh9dcw2q1n742")))

(define-public crate-del-msh-0.1 (crate (name "del-msh") (vers "0.1.9") (deps (list (crate-dep (name "num-traits") (req "^0.2.15") (default-features #t) (kind 0)))) (hash "0ahj9zcbm1j141b7f5nsr0yiskkzv9q539a5qcadjsxjmgdbq06n")))

(define-public crate-del-msh-0.1 (crate (name "del-msh") (vers "0.1.10") (deps (list (crate-dep (name "num-traits") (req "^0.2.15") (default-features #t) (kind 0)))) (hash "0fm8nv1pskn88r2l3q64nrwdk00ajcph4h9615g84gd00bhdcrhv")))

(define-public crate-del-msh-0.1 (crate (name "del-msh") (vers "0.1.11") (deps (list (crate-dep (name "num-traits") (req "^0.2.16") (default-features #t) (kind 0)))) (hash "0cc2hpz9q7fsb384jaf5m2z33w6l129qz16skspfspwlksz35wk5")))

(define-public crate-del-msh-0.1 (crate (name "del-msh") (vers "0.1.12") (deps (list (crate-dep (name "del-geo") (req "^0.1.5") (default-features #t) (kind 0)) (crate-dep (name "nalgebra") (req "^0.32.3") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2.16") (default-features #t) (kind 0)))) (hash "01x6s7in7acgvmpv4qyccigqhn9h3c99jyqipy7w8mhb2jcvyi66")))

(define-public crate-del-msh-0.1 (crate (name "del-msh") (vers "0.1.13") (deps (list (crate-dep (name "del-geo") (req "^0.1.6") (default-features #t) (kind 0)) (crate-dep (name "nalgebra") (req "^0.32.3") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2.16") (default-features #t) (kind 0)))) (hash "1s1b24zmwb6vfvkpgb3qx6xk5paizc9qcq48mpsbqcsp29zhjyw3")))

(define-public crate-del-msh-0.1 (crate (name "del-msh") (vers "0.1.14") (deps (list (crate-dep (name "del-geo") (req "^0.1.7") (default-features #t) (kind 0)) (crate-dep (name "nalgebra") (req "^0.32.3") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2.17") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)))) (hash "0dpg73gvisq6j32dz3filj8jikb8lmfvi7jf12w9hpfzmd3bn1fy")))

(define-public crate-del-msh-0.1 (crate (name "del-msh") (vers "0.1.15") (deps (list (crate-dep (name "del-geo") (req "^0.1.7") (default-features #t) (kind 0)) (crate-dep (name "nalgebra") (req "^0.32.3") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2.17") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)))) (hash "1nsn8rbrgdildj0snkafnkrxfw8z2cw683g09qm7xnd5ksx856n2")))

(define-public crate-del-msh-0.1 (crate (name "del-msh") (vers "0.1.16") (deps (list (crate-dep (name "del-geo") (req "^0.1.8") (default-features #t) (kind 0)) (crate-dep (name "nalgebra") (req "^0.32.3") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2.17") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)))) (hash "18c89g6m0fn05a4v3j3xaym3qaywh3crkzmpnzx03gqr0rg7zyvl")))

(define-public crate-del-msh-0.1 (crate (name "del-msh") (vers "0.1.17") (deps (list (crate-dep (name "del-geo") (req "^0.1.9") (default-features #t) (kind 0)) (crate-dep (name "nalgebra") (req "^0.32.3") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2.17") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)))) (hash "1laswxb169ksf587z7xwp68nflx530593lxys345482akklf6z7s")))

(define-public crate-del-msh-0.1 (crate (name "del-msh") (vers "0.1.18") (deps (list (crate-dep (name "del-geo") (req "^0.1.10") (default-features #t) (kind 0)) (crate-dep (name "nalgebra") (req "^0.32.3") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2.17") (default-features #t) (kind 0)) (crate-dep (name "ordered-float") (req "^4.1.1") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)))) (hash "14gck59ppq4ripkddaaz2r8ih44vbvkn03i7yr143c0kfyha6csb")))

(define-public crate-del-msh-0.1 (crate (name "del-msh") (vers "0.1.19") (deps (list (crate-dep (name "del-geo") (req "^0.1.11") (default-features #t) (kind 0)) (crate-dep (name "nalgebra") (req "^0.32.3") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2.17") (default-features #t) (kind 0)) (crate-dep (name "ordered-float") (req "^4.1.1") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)))) (hash "1y4n0dbnmpra4ydj2z5i4q6x6lhj28n2gxw7rv6jj0439b4s87d5")))

(define-public crate-del-msh-0.1 (crate (name "del-msh") (vers "0.1.20") (deps (list (crate-dep (name "del-geo") (req "^0.1.12") (default-features #t) (kind 0)) (crate-dep (name "nalgebra") (req "^0.32.3") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2.17") (default-features #t) (kind 0)) (crate-dep (name "ordered-float") (req "^4.2.0") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)))) (hash "1b31rr553mj98slhw96w04g8f57i2aah7a3lrn699giv19rc28x9")))

(define-public crate-del-msh-0.1 (crate (name "del-msh") (vers "0.1.21") (deps (list (crate-dep (name "del-geo") (req "^0.1.13") (default-features #t) (kind 0)) (crate-dep (name "nalgebra") (req "^0.32.3") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2.17") (default-features #t) (kind 0)) (crate-dep (name "ordered-float") (req "^4.2.0") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)))) (hash "1x8sbg8xm7xrxlkcwpqfifphkiqsw769ir99l09f4yc2k5plggb3")))

(define-public crate-del-msh-0.1 (crate (name "del-msh") (vers "0.1.22") (deps (list (crate-dep (name "del-geo") (req "^0.1.14") (default-features #t) (kind 0)) (crate-dep (name "nalgebra") (req "^0.32.3") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2.17") (default-features #t) (kind 0)) (crate-dep (name "ordered-float") (req "^4.2.0") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)))) (hash "0bpda9fsyhhhidc3yibsp295rnb5188agm89497arbvx6f80sf52")))

(define-public crate-del-msh-0.1 (crate (name "del-msh") (vers "0.1.23") (deps (list (crate-dep (name "del-geo") (req "^0.1.15") (default-features #t) (kind 0)) (crate-dep (name "nalgebra") (req "^0.32.3") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2.17") (default-features #t) (kind 0)) (crate-dep (name "ordered-float") (req "^4.2.0") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)))) (hash "0y3b182n2yfi7bas5ahjncyxh6vf6r8mpkv44q6z3xir1bi7zsip")))

(define-public crate-del-msh-0.1 (crate (name "del-msh") (vers "0.1.24") (deps (list (crate-dep (name "del-geo") (req "^0.1.16") (default-features #t) (kind 0)) (crate-dep (name "nalgebra") (req "^0.32.3") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2.17") (default-features #t) (kind 0)) (crate-dep (name "ordered-float") (req "^4.2.0") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)))) (hash "1nkb6ymf5n4c5ip09glm1xs59j8rgpgabz72c45wg7wcizn79p64")))

(define-public crate-del-msh-0.1 (crate (name "del-msh") (vers "0.1.25") (deps (list (crate-dep (name "del-geo") (req "^0.1.18") (default-features #t) (kind 0)) (crate-dep (name "nalgebra") (req "^0.32.3") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2.18") (default-features #t) (kind 0)) (crate-dep (name "ordered-float") (req "^4.2.0") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)))) (hash "1sqcxq0nnpayf8zjxag014a3lk0xlcqscw0fjfisa8vh223ircwl")))

(define-public crate-del-msh-0.1 (crate (name "del-msh") (vers "0.1.26") (deps (list (crate-dep (name "del-geo") (req "^0.1.20") (default-features #t) (kind 0)) (crate-dep (name "nalgebra") (req "^0.32.4") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2.18") (default-features #t) (kind 0)) (crate-dep (name "ordered-float") (req "^4.2.0") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)))) (hash "1y97fifq0im9zbwicw07g7n2n7yhgi5qg1255kn7p5mh42a4nj1c")))

(define-public crate-del-msh-0.1 (crate (name "del-msh") (vers "0.1.27") (deps (list (crate-dep (name "del-geo") (req "^0.1.21") (default-features #t) (kind 0)) (crate-dep (name "nalgebra") (req "^0.32.4") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2.18") (default-features #t) (kind 0)) (crate-dep (name "ordered-float") (req "^4.2.0") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)))) (hash "02lxfh8i51p514vy8dcd5h4ckv1qnh4amkxp0f5ac59plfcfgh8v")))

(define-public crate-del-msh-0.1 (crate (name "del-msh") (vers "0.1.28") (deps (list (crate-dep (name "del-geo") (req "^0.1.22") (default-features #t) (kind 0)) (crate-dep (name "nalgebra") (req "^0.32.5") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2.18") (default-features #t) (kind 0)) (crate-dep (name "ordered-float") (req "^4.2.0") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)))) (hash "18z84hvi5296l9khjw5b0qfhkd49zq7a07pv44c9j93yvbm699sx")))

(define-public crate-del-msh-0.1 (crate (name "del-msh") (vers "0.1.29") (deps (list (crate-dep (name "del-geo") (req "^0.1.23") (default-features #t) (kind 0)) (crate-dep (name "nalgebra") (req "^0.32.5") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2.18") (default-features #t) (kind 0)) (crate-dep (name "ordered-float") (req "^4.2.0") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)))) (hash "1r1ky62pvc6hqjdg46wn9syxkmnvzsidpggjblm3yimwkg0787s6")))

(define-public crate-del-msh-0.1 (crate (name "del-msh") (vers "0.1.30") (deps (list (crate-dep (name "del-geo") (req "^0.1.24") (default-features #t) (kind 0)) (crate-dep (name "nalgebra") (req "^0.32.5") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2.18") (default-features #t) (kind 0)) (crate-dep (name "ordered-float") (req "^4.2.0") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)))) (hash "15hn8s3dlgsa9dmv9jsfj1b5c4z3smh570ig6jf8j3nv062z8mbk")))

(define-public crate-del-rs-0.1 (crate (name "del-rs") (vers "0.1.0") (deps (list (crate-dep (name "reqwest") (req "^0.11") (features (quote ("json"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "0zwc0m4j2z4aqw1r25b610mdipnws1q2dx1gclsc9lq71q632gjc") (yanked #t)))

(define-public crate-del-rs-0.2 (crate (name "del-rs") (vers "0.2.0") (deps (list (crate-dep (name "reqwest") (req "^0.11") (features (quote ("json"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.2") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "1v674qb5v8gwlbm5fqh8nhfhr9qfawzh9ajfa6r1mqvar7w739l0")))

