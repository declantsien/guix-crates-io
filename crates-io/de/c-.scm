(define-module (crates-io de c-) #:use-module (crates-io))

(define-public crate-dec-number-0.0.1 (crate (name "dec-number") (vers "0.0.1") (hash "0ymq4yr0hck1zvmmhfmhjdvikcs577hqja9n5f73zv19k4nv6g70")))

(define-public crate-dec-number-0.0.2 (crate (name "dec-number") (vers "0.0.2") (hash "0f2j5293xpn96kgzpi7fz1ipliacmvpvxg8jh6ybj8laiipl3rw0")))

(define-public crate-dec-number-0.0.4 (crate (name "dec-number") (vers "0.0.4") (hash "1cjabzg4xn0y3nghj7krdzd17flfnc9kscfxsbz7smlg1bknz8ws")))

(define-public crate-dec-number-sys-0.0.1 (crate (name "dec-number-sys") (vers "0.0.1") (hash "1dmrkjx4gid7k8ckg4x9cd55yin76hsnvsrg08aq264ivzi9bg7i")))

(define-public crate-dec-number-sys-0.0.2 (crate (name "dec-number-sys") (vers "0.0.2") (deps (list (crate-dep (name "libc") (req "^0.2.126") (default-features #t) (kind 0)))) (hash "0vyv99mw294v27id8k1c022y4j04p9qprji6hip88lnw9dh3n5m7") (links "decNumber64")))

(define-public crate-dec-number-sys-0.0.3 (crate (name "dec-number-sys") (vers "0.0.3") (deps (list (crate-dep (name "libc") (req "^0.2.126") (default-features #t) (kind 0)))) (hash "1fvkjcpayyzi3df0qylpbazr8gh15zk1nb9ppkx0srdsq05cm3ml") (links "decNumber64")))

(define-public crate-dec-number-sys-0.0.4 (crate (name "dec-number-sys") (vers "0.0.4") (deps (list (crate-dep (name "libc") (req "^0.2.126") (default-features #t) (kind 0)))) (hash "1krnm5jrg17fsklqix08qc8j6rqqblxrq7x0h08pf8dbx4qi4gv1") (links "decNumber64")))

(define-public crate-dec-number-sys-0.0.5 (crate (name "dec-number-sys") (vers "0.0.5") (deps (list (crate-dep (name "libc") (req "^0.2.126") (default-features #t) (kind 0)))) (hash "10cpxkgcchp9vlnwar46scgni70jv49j1gpymh4s24qlkfx2kr1a") (links "decNumber64")))

(define-public crate-dec-number-sys-0.0.6 (crate (name "dec-number-sys") (vers "0.0.6") (deps (list (crate-dep (name "libc") (req "^0.2.126") (default-features #t) (kind 0)))) (hash "1g0xsigb0asvai6aag33k3nm848x1pc42wykfp0b1ml3adn7kfq3") (links "decNumber64")))

(define-public crate-dec-number-sys-0.0.7 (crate (name "dec-number-sys") (vers "0.0.7") (deps (list (crate-dep (name "libc") (req "^0.2.126") (default-features #t) (kind 0)))) (hash "17ajy3qmr6ip7fjcnl66cg2rsdrv03lp9gf1rnhd2akasx4r3ms9") (links "decNumber64")))

(define-public crate-dec-number-sys-0.0.8 (crate (name "dec-number-sys") (vers "0.0.8") (deps (list (crate-dep (name "libc") (req "^0.2.126") (default-features #t) (kind 0)))) (hash "1p90qf1cpqawlr2sn52ay0q09kfz56pxnmjm8b8kawnhmnsndsxn") (links "decNumber64")))

(define-public crate-dec-number-sys-0.0.9 (crate (name "dec-number-sys") (vers "0.0.9") (deps (list (crate-dep (name "libc") (req "^0.2.126") (default-features #t) (kind 0)))) (hash "0v90hp5v58qwb2m4ca3727xgzg280jrvxsx9hjf449p2v0bpg6c8") (links "decNumber64")))

(define-public crate-dec-number-sys-0.0.10 (crate (name "dec-number-sys") (vers "0.0.10") (deps (list (crate-dep (name "cc") (req "^1.0.73") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2.126") (default-features #t) (kind 0)))) (hash "09z5q2nlwr9w02hs217ybnh799rsiqn1xdyd3kf4alqgq4isifda")))

(define-public crate-dec-number-sys-0.0.11 (crate (name "dec-number-sys") (vers "0.0.11") (deps (list (crate-dep (name "cc") (req "^1.0.73") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2.126") (default-features #t) (kind 0)))) (hash "12lsq5gnaqij85gwl5f1bscw5p69bab5p5jwd42qjys25r8rqqm9")))

(define-public crate-dec-number-sys-0.0.12 (crate (name "dec-number-sys") (vers "0.0.12") (deps (list (crate-dep (name "cc") (req "^1.0.73") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2.126") (default-features #t) (kind 0)))) (hash "05f1jci395dyp0vii58za8d9aanlgxrkw42fc4j6bb45q609rqp9")))

(define-public crate-dec-number-sys-0.0.13 (crate (name "dec-number-sys") (vers "0.0.13") (deps (list (crate-dep (name "cc") (req "^1.0.73") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2.132") (default-features #t) (kind 0)))) (hash "0kyda72flkkzh8s6kn6cmh25cac5qknlaphc8r09h1j3gnqbch35")))

(define-public crate-dec-number-sys-0.0.14 (crate (name "dec-number-sys") (vers "0.0.14") (deps (list (crate-dep (name "cc") (req "^1.0.73") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2.132") (default-features #t) (kind 0)))) (hash "0v2bm4b5b7yp4i00h4w0h90lbyms5hsl1905w4cqsxvhznmp2z1h")))

(define-public crate-dec-number-sys-0.0.15 (crate (name "dec-number-sys") (vers "0.0.15") (deps (list (crate-dep (name "cc") (req "^1.0.73") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2.132") (default-features #t) (kind 0)))) (hash "0hgs7hdrc1v48vsq0ldx79kp7r0nvgxl12zjr0ic53m8j23mqz47")))

(define-public crate-dec-number-sys-0.0.16 (crate (name "dec-number-sys") (vers "0.0.16") (deps (list (crate-dep (name "cc") (req "^1.0.73") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2.132") (default-features #t) (kind 0)))) (hash "1fjkwqk3j6nfdramji7d4cfdii085jk2yvcz6qqzr0maayx3dzvq")))

(define-public crate-dec-number-sys-0.0.17 (crate (name "dec-number-sys") (vers "0.0.17") (deps (list (crate-dep (name "cc") (req "^1.0.73") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2.132") (default-features #t) (kind 0)))) (hash "0759dlb4xflwk4lq2s6j2h7mzqn4p4byixwcjlmsvdsyh8najcc5")))

(define-public crate-dec-number-sys-0.0.18 (crate (name "dec-number-sys") (vers "0.0.18") (deps (list (crate-dep (name "cc") (req "^1.0.73") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2.132") (default-features #t) (kind 0)))) (hash "03k3xfkw0hhhygg7bvpigc18aj3grdmxjwq78xi7p66da99xhbmy")))

(define-public crate-dec-number-sys-0.0.19 (crate (name "dec-number-sys") (vers "0.0.19") (deps (list (crate-dep (name "cc") (req "^1.0.73") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2.132") (default-features #t) (kind 0)))) (hash "096s4hfliyn8gy4p5h1hl1a1qvm74nvizp1zvfaygbybm6yq7r43")))

(define-public crate-dec-number-sys-0.0.20 (crate (name "dec-number-sys") (vers "0.0.20") (deps (list (crate-dep (name "cc") (req "^1.0.73") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2.132") (default-features #t) (kind 0)))) (hash "0jgsklnpm86ggk8qh0gmqpqn5639l22xy10kjcc3ypb7iyf6lz28")))

(define-public crate-dec-number-sys-0.0.21 (crate (name "dec-number-sys") (vers "0.0.21") (deps (list (crate-dep (name "cc") (req "^1.0.73") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2.132") (default-features #t) (kind 0)))) (hash "0ky1y8i8288g8lk538l7pjj6ncdkqxh5q2z24w01bb1svs8jmqb6")))

(define-public crate-dec-number-sys-0.0.22 (crate (name "dec-number-sys") (vers "0.0.22") (deps (list (crate-dep (name "cc") (req "^1.0.73") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2.132") (default-features #t) (kind 0)))) (hash "0afqw8f2nqgll4qrvs5zcckh9lw2ssipcmh581sn5wc5hh5qkc40")))

(define-public crate-dec-number-sys-0.0.23 (crate (name "dec-number-sys") (vers "0.0.23") (deps (list (crate-dep (name "cc") (req "^1.0.73") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2.132") (default-features #t) (kind 0)))) (hash "1m15ssvxvbk3rqq7q3cvbxsli8n7pp9lfrmcx2d7syl4q95i7i2v")))

(define-public crate-dec-number-sys-0.0.24 (crate (name "dec-number-sys") (vers "0.0.24") (deps (list (crate-dep (name "cc") (req "^1.0.77") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2.137") (default-features #t) (kind 0)))) (hash "1bvv4bdvqag1n3yqwdk9dgl5yjzrc8iqklwsb76hpihfv6myz7zw")))

(define-public crate-dec-number-sys-0.0.25 (crate (name "dec-number-sys") (vers "0.0.25") (deps (list (crate-dep (name "cc") (req "^1.0.77") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2.137") (default-features #t) (kind 0)))) (hash "14m38kb3jkbcwyrp9ir4x8p8b4fwdzzdp831ybpir2zh5dsk674i")))

(define-public crate-dec-number-sys-0.0.26 (crate (name "dec-number-sys") (vers "0.0.26") (deps (list (crate-dep (name "cc") (req "^1.0.83") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2.149") (default-features #t) (kind 0)))) (hash "1kmjgn7snfqhr906v6y3chvpbm5k36ibv0g3kwpfxw78valnnm88")))

(define-public crate-dec-number-sys-0.0.27 (crate (name "dec-number-sys") (vers "0.0.27") (deps (list (crate-dep (name "cc") (req "^1.0.83") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2.149") (default-features #t) (kind 0)))) (hash "1m5143bkrdn44j4li5vrxcpyckh0jq4lsw2niyr47mjh78586w8i")))

(define-public crate-dec-number-sys-0.0.28 (crate (name "dec-number-sys") (vers "0.0.28") (deps (list (crate-dep (name "cc") (req "^1.0.83") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2.149") (default-features #t) (kind 0)))) (hash "1s6xbdqvhi8dgji06045kxg1i85cyjqv7slvj2b30wqmgciz3j4x")))

