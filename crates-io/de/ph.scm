(define-module (crates-io de ph) #:use-module (crates-io))

(define-public crate-dephy_proto-0.1 (crate (name "dephy_proto") (vers "0.1.0") (deps (list (crate-dep (name "prost") (req "^0.12.1") (default-features #t) (kind 0)) (crate-dep (name "prost-build") (req "^0.12.1") (default-features #t) (kind 1)))) (hash "02pc1v40kc8hjfrnjsywkv0jp6qmsxxcqy5cypd43kqvb7wj5af0") (yanked #t)))

(define-public crate-dephy_proto-0.1 (crate (name "dephy_proto") (vers "0.1.1") (deps (list (crate-dep (name "prost") (req "^0.12.1") (default-features #t) (kind 0)) (crate-dep (name "prost-build") (req "^0.12.1") (default-features #t) (kind 1)))) (hash "1yq0vm98h0pamb44m9d2k9c384w5l8l2jaznhqxrda26svasqsf4")))

(define-public crate-dephy_proto-0.1 (crate (name "dephy_proto") (vers "0.1.2") (deps (list (crate-dep (name "prost") (req "^0.12.1") (default-features #t) (kind 0)) (crate-dep (name "prost-build") (req "^0.12.1") (default-features #t) (kind 1)))) (hash "0y66hrnxd0iymqxid9rfxmhx8d6gxy609701qv0ya1i26iyhic6j")))

(define-public crate-dephy_proto-0.1 (crate (name "dephy_proto") (vers "0.1.3") (deps (list (crate-dep (name "prost") (req "^0.12.1") (default-features #t) (kind 0)) (crate-dep (name "prost-build") (req "^0.12.1") (default-features #t) (kind 1)))) (hash "0yba8ralkqngs95d69krhjhs37lvk5b7bvrjqvnpjdwk0bnh4k96")))

