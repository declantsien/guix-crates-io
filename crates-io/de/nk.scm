(define-module (crates-io de nk) #:use-module (crates-io))

(define-public crate-denkproto-rs-1 (crate (name "denkproto-rs") (vers "1.0.13") (deps (list (crate-dep (name "protobuf") (req "^3.3.0") (default-features #t) (kind 0)))) (hash "1g1y65vzvr6lik5ylyyvhd69xrzlkgfwhz7v31clv5ld67p2db5g")))

