(define-module (crates-io de ba) #:use-module (crates-io))

(define-public crate-debarchive-0.1 (crate (name "debarchive") (vers "0.1.0") (deps (list (crate-dep (name "ar") (req "^0.6.0") (default-features #t) (kind 0)) (crate-dep (name "libflate") (req "^0.1.18") (default-features #t) (kind 0)) (crate-dep (name "tar") (req "^0.4.17") (default-features #t) (kind 0)) (crate-dep (name "xz2") (req "^0.1.5") (default-features #t) (kind 0)))) (hash "1lrsz9xxq8dpalwifrqq376w6hzsjg19k18g3fzw9w8vsbgli3wv")))

(define-public crate-debarchive-0.2 (crate (name "debarchive") (vers "0.2.0") (deps (list (crate-dep (name "ar") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "libflate") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "tar") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "xz2") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "zstd") (req "^0.9") (default-features #t) (kind 0)))) (hash "0nd7pjwqzg9pgp6n8ra694qd6k6w7rv7xhk64j0xcr34nrv83p86") (yanked #t)))

(define-public crate-debarchive-0.2 (crate (name "debarchive") (vers "0.2.1") (deps (list (crate-dep (name "ar") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "libflate") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "tar") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "xz2") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "zstd") (req "^0.9") (default-features #t) (kind 0)))) (hash "1zh6744rbhy071n99l2s2x46mba7gl0fahhqgbzaiw401grs3bjw")))

