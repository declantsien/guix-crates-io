(define-module (crates-io de p3) #:use-module (crates-io))

(define-public crate-dep3-0.1 (crate (name "dep3") (vers "0.1.0") (deps (list (crate-dep (name "chrono") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "deb822-lossless") (req "^0.1.6") (default-features #t) (kind 0)))) (hash "184qnhay1h0vakdg4cfnnib5y91di5fc4arba5rcyx54la3mkp5z")))

(define-public crate-dep3-0.1 (crate (name "dep3") (vers "0.1.1") (deps (list (crate-dep (name "chrono") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "deb822-lossless") (req "^0.1.6") (default-features #t) (kind 0)))) (hash "0dd362wrcfcnj3iz9w9a9vv4a591fbbzrf1mvd32n9rv8ml4fc7q")))

(define-public crate-dep3-0.1 (crate (name "dep3") (vers "0.1.2") (deps (list (crate-dep (name "chrono") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "deb822-lossless") (req "^0.1.6") (default-features #t) (kind 0)))) (hash "1is3whmyayfca7l8vdwlmdvnx5ag0n7nnig1jcdhg5kzq4xlpin0")))

