(define-module (crates-io de ko) #:use-module (crates-io))

(define-public crate-dekoder-0.1 (crate (name "dekoder") (vers "0.1.0") (deps (list (crate-dep (name "anyhow") (req "^1.0.71") (default-features #t) (kind 0)) (crate-dep (name "base64") (req "^0.21.0") (default-features #t) (kind 0)) (crate-dep (name "lz4_flex") (req "^0.10.0") (default-features #t) (kind 0)) (crate-dep (name "ndarray") (req "^0.15.6") (default-features #t) (kind 0)) (crate-dep (name "ndarray-npy") (req "^0.8.1") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.162") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_yaml") (req "^0.9.21") (default-features #t) (kind 0)) (crate-dep (name "tar") (req "^0.4.38") (default-features #t) (kind 0)))) (hash "1zcl4pg21q8121qr0ri03c0nwqn4bcvf97n49l92isp0msgbbw0y") (rust-version "1.69.0")))

(define-public crate-dekopon-0.1 (crate (name "dekopon") (vers "0.1.0") (hash "0kndx42mbrszp0a95alx14kk17kgny39fai88p582iy6rgxnzyrx")))

(define-public crate-dekor-0.1 (crate (name "dekor") (vers "0.1.0") (hash "1xw2w5261vbvgvbimy3jjmpf0xajkzbin2a6kmj43yqq3p82iqq2")))

(define-public crate-dekor-0.2 (crate (name "dekor") (vers "0.2.0") (hash "0gy0pnkrll6zm54rn8w0zny7f24hs1ycfnkvs0mik7scz8by14m6")))

(define-public crate-dekor-0.2 (crate (name "dekor") (vers "0.2.1") (hash "0xz076ar1nzpd9qhfdw4viz3h22jbc9m4mxmvaqhpbqvif0jikzc")))

(define-public crate-dekor-0.2 (crate (name "dekor") (vers "0.2.2") (hash "0bynz16r032dlgmgfccpyqdxhhmllhbqf8zgxnhffib66s247vxk")))

