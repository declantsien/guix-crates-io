(define-module (crates-io de rr) #:use-module (crates-io))

(define-public crate-derrive-ops-0.1 (crate (name "derrive-ops") (vers "0.1.0") (deps (list (crate-dep (name "proc-macro2") (req "^1.0.80") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.36") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2.0.58") (default-features #t) (kind 0)))) (hash "1gimrlsjdpgd5kcwd3qhrh02ghfsc85bkv71g0w0gyyddm9872mc") (yanked #t)))

