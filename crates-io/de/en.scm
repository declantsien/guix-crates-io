(define-module (crates-io de en) #:use-module (crates-io))

(define-public crate-deen-0.1 (crate (name "deen") (vers "0.1.0") (deps (list (crate-dep (name "byteorder") (req "^1") (default-features #t) (kind 0)))) (hash "0pwp2q46yxcmmv0qwj2i2hs76379x8fkbpblagprnv2farw4mwvk")))

(define-public crate-deen-proc-0.1 (crate (name "deen-proc") (vers "0.1.0") (deps (list (crate-dep (name "deen") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^0.6") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^0.15") (default-features #t) (kind 0)))) (hash "0kbrd1i73z3bmbjjaqxv77vfb11aklridr5rmx6ck1bpsrfv6fa4")))

(define-public crate-deentry-0.0.1 (crate (name "deentry") (vers "0.0.1") (deps (list (crate-dep (name "regex") (req "^1.7.3") (default-features #t) (kind 2)))) (hash "145npm0vjmqrv3swpw1whphhy89zjf9005p5gydf9xzva7ap0qc6")))

