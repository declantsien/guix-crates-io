(define-module (crates-io de vx) #:use-module (crates-io))

(define-public crate-devx-cmd-0.1 (crate (name "devx-cmd") (vers "0.1.0") (hash "1rwbpxw8b68rzvvymn0aiylb4d3vnj4dfqdajifkav3bpmx9ig4s")))

(define-public crate-devx-cmd-0.2 (crate (name "devx-cmd") (vers "0.2.0") (hash "0jaq4nw1saw9x4ys35v0mj64w79qjij6x9hr722pkm2bkf5kxfcd")))

(define-public crate-devx-cmd-0.3 (crate (name "devx-cmd") (vers "0.3.0") (deps (list (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "simple_logger") (req "^1.6") (kind 2)))) (hash "0g5xlmb4qkc3z3q4g5fi085hzx9816raj49fdlxzpdvrm4ybp8lb")))

(define-public crate-devx-cmd-0.3 (crate (name "devx-cmd") (vers "0.3.1") (deps (list (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "simple_logger") (req "^1.9") (kind 2)))) (hash "1qm208m0x8gpykk44mkyqml7hllysfr9ypipjpjhsx3z7zh8c7bp")))

(define-public crate-devx-cmd-0.4 (crate (name "devx-cmd") (vers "0.4.0") (deps (list (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "simple_logger") (req "^1.9") (kind 2)))) (hash "0fvv4kpg3axfg0klbfm84l498r0mzqk9lnznvwa7q6cb5v6c440m")))

(define-public crate-devx-cmd-0.5 (crate (name "devx-cmd") (vers "0.5.0") (deps (list (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "simple_logger") (req "^1.9") (kind 2)))) (hash "134c8spmz5ds6fsd53v5nwgprzzy3vb47q842p50pdc2syb8vw95")))

(define-public crate-devx-pre-commit-0.1 (crate (name "devx-pre-commit") (vers "0.1.0") (deps (list (crate-dep (name "anyhow") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "devx-cmd") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "0ng240v5x5yjafm5nb3khcjnjkivys7x461v7pllxh21bdpbkrng")))

(define-public crate-devx-pre-commit-0.2 (crate (name "devx-pre-commit") (vers "0.2.0") (deps (list (crate-dep (name "anyhow") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "devx-cmd") (req "^0.2.0") (default-features #t) (kind 0)))) (hash "0q047l6i3jxj1dkisfl8w52vs4xl9a0kidfyf2y8adg1nv4fwmy1")))

(define-public crate-devx-pre-commit-0.3 (crate (name "devx-pre-commit") (vers "0.3.0") (deps (list (crate-dep (name "anyhow") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "devx-cmd") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "fs-err") (req "^2.3") (default-features #t) (kind 0)))) (hash "0i42x64ygiyq91lvmb93cyx6y2iynb2x3adjrw8yjnrj6dv08a61")))

(define-public crate-devx-pre-commit-0.3 (crate (name "devx-pre-commit") (vers "0.3.1") (deps (list (crate-dep (name "anyhow") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "devx-cmd") (req "^0.3.1") (default-features #t) (kind 0)) (crate-dep (name "fs-err") (req "^2.4") (default-features #t) (kind 0)))) (hash "1ndqwsnxl66jx4bl6mm6fvw921s31jk30jl5zkfs8r349v8f8c7j")))

(define-public crate-devx-pre-commit-0.4 (crate (name "devx-pre-commit") (vers "0.4.0") (deps (list (crate-dep (name "anyhow") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "devx-cmd") (req "^0.4.0") (default-features #t) (kind 0)) (crate-dep (name "fs-err") (req "^2.4") (default-features #t) (kind 0)))) (hash "0cp31k2a4kidz8l3qfw2ygzpjfvz0g2s4dqxcqdcx1d7wi15pvjc")))

(define-public crate-devx-pre-commit-0.5 (crate (name "devx-pre-commit") (vers "0.5.0") (deps (list (crate-dep (name "anyhow") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "devx-cmd") (req "^0.5.0") (default-features #t) (kind 0)) (crate-dep (name "fs-err") (req "^2.4") (default-features #t) (kind 0)))) (hash "0b6qbbzizq7llgpz5vbbiyf52s09rr1qvh79dlb3jw2x0h5kncw2")))

(define-public crate-devx1-0.1 (crate (name "devx1") (vers "0.1.0") (deps (list (crate-dep (name "cosmwasm") (req "^0.7.0") (default-features #t) (kind 0)) (crate-dep (name "cosmwasm-vm") (req "^0.7.0") (kind 2)) (crate-dep (name "cw-storage") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "schemars") (req "= 0.5") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "= 1.0.103") (features (quote ("derive"))) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 2)) (crate-dep (name "snafu") (req "= 0.5.0") (features (quote ("rust_1_30"))) (kind 0)))) (hash "1h3ws7vzc3hmn1c6k2prcpfzjdr3xbrvqhhjhg0g0ypnnrhm9din") (features (quote (("singlepass" "cosmwasm-vm/default-singlepass") ("default" "cranelift") ("cranelift" "cosmwasm-vm/default-cranelift") ("backtraces" "cosmwasm/backtraces" "cosmwasm-vm/backtraces"))))))

