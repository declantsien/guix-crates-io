(define-module (crates-io de ka) #:use-module (crates-io))

(define-public crate-deka-0.1 (crate (name "deka") (vers "0.1.0") (deps (list (crate-dep (name "crossterm") (req "^0.25.0") (default-features #t) (kind 0)))) (hash "156l1aadrkpxkdjs9dvvjrcnbsggscvfiqylq2mxwkl8397ndpys")))

(define-public crate-deka-0.2 (crate (name "deka") (vers "0.2.0") (deps (list (crate-dep (name "crossterm") (req "^0.25.0") (default-features #t) (kind 0)))) (hash "0rmp158vjl1qjrzr5kgwfmwrlc0cifdkzpppqn4jl7bqdx82fvqx")))

(define-public crate-deka-0.2 (crate (name "deka") (vers "0.2.1") (deps (list (crate-dep (name "crossterm") (req "^0.25.0") (default-features #t) (kind 0)))) (hash "1mipzsh1n6aqw3a3pvq7qjxp8x8pp71l0iv52ddi8lh0xy98wvw1")))

(define-public crate-deka-0.3 (crate (name "deka") (vers "0.3.2") (deps (list (crate-dep (name "crossterm") (req "^0.25.0") (default-features #t) (kind 0)) (crate-dep (name "terminal-clipboard") (req "^0.3.1") (default-features #t) (kind 0)))) (hash "0gx9f5hggd3dzj06n6x7yzr8maavnzk17xn6flwkp8y647igjyy1")))

(define-public crate-deka-0.3 (crate (name "deka") (vers "0.3.3") (deps (list (crate-dep (name "crossterm") (req "^0.25.0") (default-features #t) (kind 0)) (crate-dep (name "terminal-clipboard") (req "^0.3.1") (default-features #t) (kind 0)))) (hash "11lk3cn3glwm9djl1cm396bfzbq0jbwfd461z815880p0dgc0sli")))

