(define-module (crates-io de p_) #:use-module (crates-io))

(define-public crate-dep_c-1 (crate (name "dep_c") (vers "1.0.0") (deps (list (crate-dep (name "dep_d") (req "^2.0.0") (default-features #t) (kind 0)))) (hash "0gbvpizcvy7vanzq9xxm9jhdm8a63n5bm51fmq8q1yqa1jppbiza") (yanked #t)))

(define-public crate-dep_c-2 (crate (name "dep_c") (vers "2.0.0") (deps (list (crate-dep (name "dep_d") (req "^3.0.0") (default-features #t) (kind 0)))) (hash "1yg3qw6inp686cl058a8qs88prbavkzcva3bzz02bcr1f34nih59") (yanked #t)))

(define-public crate-dep_crusher-0.1 (crate (name "dep_crusher") (vers "0.1.0") (deps (list (crate-dep (name "regex") (req "^1.9.1") (default-features #t) (kind 2)) (crate-dep (name "same-file") (req "^1.0.6") (default-features #t) (kind 2)))) (hash "0xfp4l4f3mi41kkqc7sqvalyj4xzn4pflq2iy9qwa97zzm74pkdi")))

(define-public crate-dep_crusher-0.1 (crate (name "dep_crusher") (vers "0.1.1") (deps (list (crate-dep (name "regex") (req "^1.9.1") (default-features #t) (kind 2)) (crate-dep (name "same-file") (req "^1.0.6") (default-features #t) (kind 2)))) (hash "0v4q1bj7qaw0dqa66s2yzq4yznvlx9fxfcfqd1jp74laknq1hlrz")))

(define-public crate-dep_crusher-0.1 (crate (name "dep_crusher") (vers "0.1.2") (deps (list (crate-dep (name "regex") (req "^1.9.1") (default-features #t) (kind 2)) (crate-dep (name "same-file") (req "^1.0.6") (default-features #t) (kind 2)))) (hash "1bg1wrk6kyw8fs6qajwzngd623a8pq6li1lrh7sw1j32ab51vdyv")))

(define-public crate-dep_d-2 (crate (name "dep_d") (vers "2.0.0") (hash "1zg3g0r87aj0zgjlxc81c99js9hp5dssyx4fd0x8q2a644jixq5l") (yanked #t)))

(define-public crate-dep_d-3 (crate (name "dep_d") (vers "3.0.0") (hash "15cc4af08qjiffg2ks93y18a1vb472z09ld93xpnd7nhzq2dpx4p") (yanked #t)))

(define-public crate-dep_d-3 (crate (name "dep_d") (vers "3.1.0") (hash "1194djkhjkqfhcii90r93y0qb2v23clbikjcyji36w24hjkw5v6v") (yanked #t)))

(define-public crate-dep_doc-0.1 (crate (name "dep_doc") (vers "0.1.0") (hash "01iiyrzw4havar4q2p678nyj302v6mp46fqrsaw6i4nxjvl1z4fr") (rust-version "1.54")))

(define-public crate-dep_doc-0.1 (crate (name "dep_doc") (vers "0.1.1") (hash "1sajgbadp16ljaig57cdl19rr36naaxhr1v16glnm91ix3a1frhr") (rust-version "1.54")))

