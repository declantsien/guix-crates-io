(define-module (crates-io de un) #:use-module (crates-io))

(define-public crate-deunicode-0.4 (crate (name "deunicode") (vers "0.4.0") (hash "1ih3lksc7pw95nq7mcs8lwxh2qmifdwvcw436x3vn4n0j2v73aa8") (yanked #t)))

(define-public crate-deunicode-0.4 (crate (name "deunicode") (vers "0.4.1") (hash "1pjwpih0dam844sh0yyjcfj7h1c5z7h03vjpv34azlh7g3c362wm") (yanked #t)))

(define-public crate-deunicode-0.4 (crate (name "deunicode") (vers "0.4.2") (hash "0kxq7jmiwz4fs9z2jf65dyrc4n0ba82ican3v8q82brx0crb8a2w") (yanked #t)))

(define-public crate-deunicode-0.4 (crate (name "deunicode") (vers "0.4.3") (hash "146nc3wlwz2j9awwf7zf76qvjcahnls0mlv9jm6clcvr9dlph245") (yanked #t)))

(define-public crate-deunicode-1 (crate (name "deunicode") (vers "1.0.0") (hash "0q3mhnz4mzhi088h60n5n7i6ibw2wacbj687bmh61ppdpmdhz2na") (yanked #t)))

(define-public crate-deunicode-1 (crate (name "deunicode") (vers "1.1.0") (hash "1z534mxyndah3xphdy30c7p6r9c4f8vlfaq4qidlcfbra4ddwz9h") (yanked #t)))

(define-public crate-deunicode-1 (crate (name "deunicode") (vers "1.1.1") (hash "1ccd1d7bjr9z47njpsd4khnya9mywhwhli62h7hr2i70zlnml4c0") (yanked #t)))

(define-public crate-deunicode-1 (crate (name "deunicode") (vers "1.2.0") (hash "1diyq2bhk9z21abygvmf28p2h6rincs3nsd33032aadpdrnpbdy0")))

(define-public crate-deunicode-1 (crate (name "deunicode") (vers "1.2.1") (hash "0p4lwpb2371qw45k2hpqggqxmvyb6lr92717d3gyx0mnp0rxsi10")))

(define-public crate-deunicode-1 (crate (name "deunicode") (vers "1.3.0") (hash "1wpqgdfhssaqd25hvm898vgmc3x7wv5y1vcy70balr4zcdfpfdvz")))

(define-public crate-deunicode-1 (crate (name "deunicode") (vers "1.3.1") (hash "1pmfcndh6vx8bfpm5826h9rmqq8iclmfkvpnlww1dpz72mp77jgj")))

(define-public crate-deunicode-1 (crate (name "deunicode") (vers "1.3.2") (hash "1mhw4rv748fz42mfibw39p99y9r36zfbbs64kgjja9nlh126mzq8") (features (quote (("default" "alloc") ("alloc"))))))

(define-public crate-deunicode-1 (crate (name "deunicode") (vers "1.3.3") (hash "0spkk1f8ncz0z6pmdfiamqcr237igg550gv55g8m6jks497vl6wc") (features (quote (("default" "alloc") ("alloc"))))))

(define-public crate-deunicode-0.4 (crate (name "deunicode") (vers "0.4.4") (hash "0hrfp55qxvx5n8niw4d60pr7zkm0g7wc01r5a1d221h9lnk06lnr") (features (quote (("default" "alloc") ("alloc"))))))

(define-public crate-deunicode-1 (crate (name "deunicode") (vers "1.4.0") (hash "058bckh3q26d8k3xh374vw6y4srlm4lnvvr7fzxf8wf33fg7f3c9") (features (quote (("default" "alloc") ("alloc"))))))

(define-public crate-deunicode-0.4 (crate (name "deunicode") (vers "0.4.5") (deps (list (crate-dep (name "deunicode1") (req "^1") (optional #t) (kind 0) (package "deunicode")))) (hash "133m63d7x3y3kpf8hrdkjpqma9l74bv59rpmpb8rqgn2i6zz3nvi") (features (quote (("default" "alloc")))) (v 2) (features2 (quote (("force-upgrade" "dep:deunicode1") ("alloc" "deunicode1?/alloc"))))))

(define-public crate-deunicode-1 (crate (name "deunicode") (vers "1.4.1") (hash "1rmxjv7460j1bwbfgv84wgy53cbcc1ajpzb4kzjmnib1v3sbl6ka") (features (quote (("default" "alloc") ("alloc")))) (rust-version "1.64")))

(define-public crate-deunicode-1 (crate (name "deunicode") (vers "1.4.2") (hash "0nj8pycs1idacgraqpgc2lwihawb945pirlsnx047iy5fd9s7qis") (features (quote (("default" "alloc") ("alloc")))) (rust-version "1.64")))

(define-public crate-deunicode-1 (crate (name "deunicode") (vers "1.4.3") (hash "156zkxmlfm1qg84x8jaqbj3mpc86lzwqiv0xr2w9di2ncw959s5n") (features (quote (("default" "alloc") ("alloc")))) (rust-version "1.64")))

(define-public crate-deunicode-1 (crate (name "deunicode") (vers "1.4.4") (hash "0khx3356c984ycplqailbdm2fybza6aj5szhwql3drj48w4z0bij") (features (quote (("default" "alloc") ("alloc")))) (rust-version "1.64")))

(define-public crate-deunicode-1 (crate (name "deunicode") (vers "1.6.0") (hash "006gnml4jy3m03yqma8qvx7kl9i2bw667za9f7yc6k9ckv64959k") (features (quote (("default" "alloc") ("alloc")))) (rust-version "1.64")))

