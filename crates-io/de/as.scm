(define-module (crates-io de as) #:use-module (crates-io))

(define-public crate-deasync-0.1 (crate (name "deasync") (vers "0.1.0") (deps (list (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (features (quote ("visit-mut" "full"))) (default-features #t) (kind 0)))) (hash "02m2bncykdi85rhwzk7yhk9csgsqykcq50kd8xf8sm2c3r4sf7jn") (features (quote (("default") ("bypass"))))))

