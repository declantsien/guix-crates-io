(define-module (crates-io de bt) #:use-module (crates-io))

(define-public crate-debt64-0.0.2 (crate (name "debt64") (vers "0.0.2") (hash "0ricvwkwb1ssimpv31kn5rzd0pzwjhxi492010yidryq04zbkq4j")))

(define-public crate-debt64-0.1 (crate (name "debt64") (vers "0.1.0") (hash "1iv085fisr60f0rnnp90rvl4rpqkpyn9kdnybx92b7az0y97qpsi")))

(define-public crate-debt64-1 (crate (name "debt64") (vers "1.0.0") (hash "025kwqklyr3bz3nig9cdk2cwvsxg8bx66cdyszz6jc8y5ibpgw14")))

(define-public crate-debt64-1 (crate (name "debt64") (vers "1.1.0") (hash "1cywlmkpwc0rxknjxbwxxwjzq4sr35zv65sm20z4mmfv182p653n")))

(define-public crate-debt64-1 (crate (name "debt64") (vers "1.1.1") (hash "1iim4d2sa7qmakm444sqdrp2p77ja1ingh4zhsdx4qdz4sqm3n0v")))

(define-public crate-debt64-1 (crate (name "debt64") (vers "1.2.0") (hash "0msk41f1x03kj066wk3ixfnk7gf89an1mjsf06r6b4k4mpaki8r4")))

(define-public crate-debt64-2 (crate (name "debt64") (vers "2.0.0") (hash "1b3829b1k2j8d83p49sgm36c7jlvvq7g0icgc2lgwxcfhllpgfxw")))

(define-public crate-debt64-3 (crate (name "debt64") (vers "3.0.1") (hash "0jfq82q54vfhwl7k83ng2a8ghzrygdjcla9ywwv7cz713xv0j61c") (features (quote (("std"))))))

(define-public crate-debt64-3 (crate (name "debt64") (vers "3.1.0") (hash "0h5nqy39ywpvq8g2a2sv95xl6ni5x888j606xwlz7f1xcr94hj2w") (features (quote (("std"))))))

(define-public crate-debt64-3 (crate (name "debt64") (vers "3.1.1") (hash "09fvm2g7p1jsxl7vmf0zgi4s7pqs7xfhhz9b6b054gvv3zrbq3hh") (features (quote (("std"))))))

(define-public crate-debt64-3 (crate (name "debt64") (vers "3.1.2") (hash "0mk5sw28hl2qj55h5xwcjwz9aqvxwckp7p73sz0zdff8p5a0yvdw") (features (quote (("std"))))))

(define-public crate-debt64-4 (crate (name "debt64") (vers "4.0.0") (hash "1wsijd39pr4xvh7lc55b7x054qq66yx2jxm22q0235a45s3461vh") (features (quote (("std"))))))

(define-public crate-debt64-5 (crate (name "debt64") (vers "5.0.0") (hash "01qjvgpbqsqsxgybm80fzr94xyyvj23134x7hgkp59bxsak4lgbn") (features (quote (("std"))))))

(define-public crate-debt64-6 (crate (name "debt64") (vers "6.0.0") (hash "18p10fy06rgsa3b1q3scdv0xx19zhx011ycwsp87aw3qcnr2fh7m") (features (quote (("std"))))))

(define-public crate-debt64-7 (crate (name "debt64") (vers "7.0.0") (hash "0gh2sckfrpxzbwdanjgw6m6n90i16rsgsjc95nk2py30qv34b0rn") (features (quote (("std"))))))

(define-public crate-debt64-8 (crate (name "debt64") (vers "8.0.1") (hash "1x8yghjvc7l3k8vrlhmwxwis8ynsik1smrg69a6ziiinvx1ks5sj") (features (quote (("std"))))))

(define-public crate-debt64-8 (crate (name "debt64") (vers "8.0.2") (hash "0cwhrbgiikn0x0z72nfsrzrr5car4m11krkmqmxjnjp2bfzawfdm") (features (quote (("std"))))))

(define-public crate-debt64-9 (crate (name "debt64") (vers "9.0.0") (hash "0fpqhmdj5mnd5351ngafayqq4mmqbd377bmiqal86bx5yzsyxdfh") (features (quote (("std"))))))

(define-public crate-debtsolver-0.1 (crate (name "debtsolver") (vers "0.1.0") (deps (list (crate-dep (name "itertools") (req "^0.8.1") (default-features #t) (kind 0)))) (hash "153wvpfzbn13m8n5ln9hz2wpkd5zspzds3bchm3kfhkx0vw0l495")))

(define-public crate-debtsolver-0.2 (crate (name "debtsolver") (vers "0.2.0") (deps (list (crate-dep (name "itertools") (req "^0.8.1") (default-features #t) (kind 0)) (crate-dep (name "rusty-money") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "0wb898c6vl9rc7gqab3d22azsla1v15822nkbzi8fzy3gsl06la4")))

