(define-module (crates-io de fi) #:use-module (crates-io))

(define-public crate-defi_greeks-0.0.1 (crate (name "defi_greeks") (vers "0.0.1") (hash "1j5h4vm1y1bnzhss5x8anlrpq41jwznk77dq08w060wpy234x22s") (features (quote (("unstable"))))))

(define-public crate-defile-0.1 (crate (name "defile") (vers "0.1.0") (deps (list (crate-dep (name "proc_macros") (req "^0.1.0") (default-features #t) (kind 0) (package "defile-proc_macros")))) (hash "1qy2rljqyfj7adgw2baibk85s29b4555yg16dh3sxxcf4lad8w1d") (features (quote (("nightly"))))))

(define-public crate-defile-0.1 (crate (name "defile") (vers "0.1.1") (deps (list (crate-dep (name "proc_macros") (req "^0.1.1") (default-features #t) (kind 0) (package "defile-proc_macros")))) (hash "0m8a7ssg8f2k4zn27hd1lcjj3znclz10laa2kprlkyv6nmlq9s7r") (features (quote (("nightly"))))))

(define-public crate-defile-0.1 (crate (name "defile") (vers "0.1.2") (deps (list (crate-dep (name "proc_macros") (req "^0.1.2") (default-features #t) (kind 0) (package "defile-proc_macros")))) (hash "04r5b4r9dw0h3q74c4rk81kcpq2x3wyfkrfd0nj7i8f9yygp8qj3") (features (quote (("nightly"))))))

(define-public crate-defile-0.2 (crate (name "defile") (vers "0.2.0-rc1") (deps (list (crate-dep (name "defile-proc_macros") (req "=0.2.0-rc1") (default-features #t) (kind 0)))) (hash "0w307qy4wzwnbzm4pljm7rmq09b57z5qz09211rp9abihd5qf6mk") (features (quote (("better-docs"))))))

(define-public crate-defile-0.2 (crate (name "defile") (vers "0.2.0") (deps (list (crate-dep (name "defile-proc_macros") (req "=0.2.0") (default-features #t) (kind 0)))) (hash "07z2anrc9ppa3z8xpw41lp821w514nhjj2v1iha91hiw1bvff4g1") (features (quote (("better-docs"))))))

(define-public crate-defile-0.2 (crate (name "defile") (vers "0.2.1") (deps (list (crate-dep (name "defile-proc_macros") (req "=0.2.1") (default-features #t) (kind 0)))) (hash "0bclrn061p74fmvl36aqfrbr8jm46lw6vhf10g48c0l5anj28czs") (features (quote (("better-docs"))))))

(define-public crate-defile-proc_macros-0.1 (crate (name "defile-proc_macros") (vers "0.1.0") (hash "03fv3n5gvdbdmmb5iha9i4wcmi6ymimcxq3qf2hvdpyi7p2r0h2y")))

(define-public crate-defile-proc_macros-0.1 (crate (name "defile-proc_macros") (vers "0.1.1") (hash "10ayrrbkgs5n0n3jwr7f1pga61brsz7n68ykcr3vycf36s9a2l0h")))

(define-public crate-defile-proc_macros-0.1 (crate (name "defile-proc_macros") (vers "0.1.2") (hash "0ilv9bczvssvgjd3da4ama0j2daa4fkl0jiih9xcrz2fmj1l8vzq")))

(define-public crate-defile-proc_macros-0.2 (crate (name "defile-proc_macros") (vers "0.2.0-rc1") (hash "1gcv7lhhhmljqf369vqh2gp4icvc19hlf82i75yqn95xm263k0yk")))

(define-public crate-defile-proc_macros-0.2 (crate (name "defile-proc_macros") (vers "0.2.0") (hash "0h4inx0cv80a0wh8bjfj16h7kkvgjx4ff28qw4qxspr0afmpz8jm")))

(define-public crate-defile-proc_macros-0.2 (crate (name "defile-proc_macros") (vers "0.2.1") (hash "0pi4h1whsslfmcxr31fwkh2hypifafpbdslzi6g723zv1d3c3ic7")))

(define-public crate-define-0.0.0 (crate (name "define") (vers "0.0.0") (hash "1qr2izp8d633yvk36mv1x6yc92h6vln35689daasriz40cpq7cbr")))

(define-public crate-define-errors-0.1 (crate (name "define-errors") (vers "0.1.0") (hash "0njmxhdaqlg4jd3y6qplkrwwg9y1v3zhj4plpxjmalv4q2pj94w7") (yanked #t)))

(define-public crate-define-errors-0.1 (crate (name "define-errors") (vers "0.1.1") (hash "0nydjbzgfy0ds28jahfi49lfizykl84k5x0q96ihv6h8nrgsim8r") (yanked #t)))

(define-public crate-define-errors-0.1 (crate (name "define-errors") (vers "0.1.2") (hash "16xxivph4qi7qx1hnrs9zk6a5m0rsyzjrdxpr7cyrjmqbfxiawbr") (yanked #t)))

(define-public crate-define-errors-0.1 (crate (name "define-errors") (vers "0.1.3") (hash "0xwcwdnbkwlp2znm1x3vs58zrjvyd3cs5s3z951m9m3pc39j30gl") (yanked #t)))

(define-public crate-define-errors-0.1 (crate (name "define-errors") (vers "0.1.4") (hash "06p5wig3abjjl3b8vkf00cy8qfnk0hpigljxypxqnmfh076s9sf8") (yanked #t)))

(define-public crate-define-errors-0.1 (crate (name "define-errors") (vers "0.1.5") (hash "10i293qfhmj919cxs75fq2m4yaszyw75f0aij4apgvkhiphdgsrp") (yanked #t)))

(define-public crate-define-errors-0.1 (crate (name "define-errors") (vers "0.1.6") (hash "14qkch3iq72ixcifv03d6ssb05cl0wd6jl99x0wf634jnzp1dm9j") (yanked #t)))

(define-public crate-define-errors-0.1 (crate (name "define-errors") (vers "0.1.7") (hash "11412d59pl0l956n2d6s5anys8dlcfx3j2akc07w7vv7k8n23kd2") (yanked #t)))

(define-public crate-define-errors-0.1 (crate (name "define-errors") (vers "0.1.8") (hash "17j5xczrk1m7yklkgvpr109n7l3ahizdq9nx38d06b9ry28iqvy0")))

(define-public crate-define_error-1 (crate (name "define_error") (vers "1.0.0") (hash "1cypcx4yn9ps78kgh5nlqj9d95n1lmay5wxn67p3qxrajyg1ifms")))

(define-public crate-define_error-1 (crate (name "define_error") (vers "1.1.0") (hash "147dkql20f0934nsl99c6b6ircw966nzd6zcvwbf9lx032r26vd9")))

(define-public crate-define_error-1 (crate (name "define_error") (vers "1.2.0") (hash "0wb3218aih52b47ny7r1wyzxdwj8mfsi8sxk58n0dy0jibnjil2j")))

(define-public crate-define_error-1 (crate (name "define_error") (vers "1.3.0") (hash "1pl05fp358iyq2h32xzv9mz1cvncl813ayb0a0djabyvdxs4wgp9")))

(define-public crate-define_error-2 (crate (name "define_error") (vers "2.3.0") (hash "0lm17wpbyadzlhplq3m9p3wxzf8nwpccdn13pk9mfr113l81qs61")))

(define-public crate-define_error-3 (crate (name "define_error") (vers "3.0.0") (hash "0yca988nzax8dkg1bgjn1d1a7zhgazh81j6j3gy1a4ag7rr7mndr")))

(define-public crate-define_error-4 (crate (name "define_error") (vers "4.0.0") (hash "1m92b2y5yqzdf57gx7js91fhrg2r0nqjp2wy6d7jciyfx2v04hqa")))

(define-public crate-define_error-4 (crate (name "define_error") (vers "4.0.1") (hash "128jf6ilkn4h67r51qh6zvdb4mkv5zqkx7386ny6irdbznagffb9")))

(define-public crate-define_into_enum-0.1 (crate (name "define_into_enum") (vers "0.1.0") (hash "13lhl3icms1ff34vscfzrcm06z716bvhh17fzw6b33vj9y95lpq7") (yanked #t)))

(define-public crate-define_into_enum-0.1 (crate (name "define_into_enum") (vers "0.1.1") (hash "1xdw1cjz41j76bmm5y6chwlf3ndc5mlb938r1s06vcm8sz28jxap")))

(define-public crate-defined-0.2 (crate (name "defined") (vers "0.2.0") (deps (list (crate-dep (name "sea-query") (req "^0.30") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1") (optional #t) (default-features #t) (kind 0)))) (hash "1zw8f7v94qqf3a3r1fpkv6222m7l4nlbpb9jsvmvirhp67a3kjac") (features (quote (("default" "serde" "sea-query")))) (v 2) (features2 (quote (("serde" "dep:serde") ("sea-query" "dep:sea-query"))))))

(define-public crate-defined-0.2 (crate (name "defined") (vers "0.2.1") (deps (list (crate-dep (name "schemars") (req "^0.8") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "sea-query") (req "^0.30") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1") (optional #t) (default-features #t) (kind 0)))) (hash "003blzi0y0vz2a8rfym84d9iv2g90z5r8mn74a61c7mgwwy8gsyk") (features (quote (("default" "schemars" "serde" "sea-query")))) (v 2) (features2 (quote (("serde" "dep:serde") ("sea-query" "dep:sea-query") ("schemars" "dep:schemars" "dep:serde_json"))))))

(define-public crate-defined-0.2 (crate (name "defined") (vers "0.2.2") (deps (list (crate-dep (name "oasgen") (req "^0.18") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "schemars") (req "^0.8") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "sea-query") (req "^0.30") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1") (optional #t) (default-features #t) (kind 0)))) (hash "0vzff93biwyrlwbqvd4vz2z3ipvwrdah1avqf59znfbxkn1dc6qh") (features (quote (("default" "schemars" "serde" "sea-query" "oasgen")))) (v 2) (features2 (quote (("serde" "dep:serde") ("sea-query" "dep:sea-query") ("schemars" "dep:schemars" "dep:serde_json") ("oasgen" "dep:oasgen"))))))

(define-public crate-defined-0.2 (crate (name "defined") (vers "0.2.3") (deps (list (crate-dep (name "oasgen") (req "^0.20") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "schemars") (req "^0.8") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "sea-query") (req "^0.30") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1") (optional #t) (default-features #t) (kind 0)))) (hash "1izjmn1rc32as7ahj8lx28svp2vqjmf8qgp4lh3mcbb8hg50k5ph") (features (quote (("default" "schemars" "serde" "sea-query" "oasgen")))) (v 2) (features2 (quote (("serde" "dep:serde") ("sea-query" "dep:sea-query") ("schemars" "dep:schemars" "dep:serde_json") ("oasgen" "dep:oasgen"))))))

(define-public crate-definitely-not-piet-direct2d-please-ignore-0.6 (crate (name "definitely-not-piet-direct2d-please-ignore") (vers "0.6.0") (deps (list (crate-dep (name "associative-cache") (req "^1.0.1") (default-features #t) (kind 0)) (crate-dep (name "dwrote") (req "^0.11.0") (kind 0)) (crate-dep (name "piet") (req "=0.6.0") (default-features #t) (kind 0)) (crate-dep (name "utf16_lit") (req "^2.0.2") (default-features #t) (kind 0)) (crate-dep (name "winapi") (req "^0.3.9") (features (quote ("d2d1" "d2d1_1" "d2d1effects" "d2dbasetypes" "dcommon" "d3d11" "dxgi" "winnls"))) (default-features #t) (kind 0)) (crate-dep (name "wio") (req "^0.2.2") (default-features #t) (kind 0)) (crate-dep (name "piet") (req "=0.6.0") (features (quote ("samples"))) (default-features #t) (kind 2)) (crate-dep (name "piet-common") (req "=0.6.0") (features (quote ("png"))) (default-features #t) (kind 2)))) (hash "1bcrk3rxphgi534m9rzhqjizx5hdhpm1gs08pdw4bd8gh8nln6mf") (yanked #t)))

(define-public crate-definitive-0.0.0 (crate (name "definitive") (vers "0.0.0-alpha.0") (hash "0nc9jx355ppjp6ny47shsrw21mn1jv0ny7h08l6d54420cyljfxw") (features (quote (("simd") ("default" "simd"))))))

(define-public crate-definitive-0.0.0 (crate (name "definitive") (vers "0.0.0-alpha.1") (hash "0azz19lqziby4n90j02snmnbhglqyx9h733f5ydml4vw4k6ilq7a") (features (quote (("simd") ("default" "simd"))))))

(define-public crate-definitive-0.0.0 (crate (name "definitive") (vers "0.0.0-alpha.2") (hash "1z78v114gsg16nfnlf9mfz9w8g4r0fsv8h366rbrlapnq88v4ixy") (features (quote (("simd") ("default" "simd"))))))

(define-public crate-definitive-0.0.0 (crate (name "definitive") (vers "0.0.0-alpha.3") (hash "1b0a134j2ij9gcdwzhii24q7sfr3hc2sgk5j0bwg0cwgl6czk2j3") (features (quote (("std") ("simd") ("default" "std" "simd"))))))

(define-public crate-definitive-0.1 (crate (name "definitive") (vers "0.1.0") (hash "1ggwblp9vqgxbl5gql0sw8yxq98sbasm0m2ard1mgn3rhg5ldqxj") (features (quote (("std") ("simd") ("default" "std" "simd"))))))

(define-public crate-defios-0.1 (crate (name "defios") (vers "0.1.0") (deps (list (crate-dep (name "anchor-lang") (req "^0.29.0") (features (quote ("init-if-needed"))) (default-features #t) (kind 0)) (crate-dep (name "anchor-spl") (req "^0.29.0") (features (quote ("metadata"))) (default-features #t) (kind 0)) (crate-dep (name "solana-program") (req "~1.16.21") (default-features #t) (kind 0)))) (hash "17pzn4fnxshkgpsxnrr8gsa5clawiphd4fdr1bqsvs2k59xbx9h2") (features (quote (("no-log-ix-name") ("no-idl") ("no-entrypoint") ("default") ("cpi" "no-entrypoint"))))))

