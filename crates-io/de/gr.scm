(define-module (crates-io de gr) #:use-module (crates-io))

(define-public crate-degree_trigonometric-0.1 (crate (name "degree_trigonometric") (vers "0.1.0") (hash "0vb947scccbn1r5n7p8h5czvlxx1yx9ksca6ik9dnbn50a99kqw5")))

(define-public crate-degreen-0.1 (crate (name "degreen") (vers "0.1.1") (deps (list (crate-dep (name "clap") (req "^2.32") (features (quote ("color" "suggestions" "wrap_help"))) (kind 0)) (crate-dep (name "pkg") (req "^1.1") (default-features #t) (kind 0)) (crate-dep (name "pkg") (req "^1.1") (features (quote ("build"))) (default-features #t) (kind 1)))) (hash "01h5zdlb47dk6qprmgqf41avzalswgcayw0k1sqq6sqjm0rmzwlk")))

(define-public crate-degrees-0.1 (crate (name "degrees") (vers "0.1.0") (deps (list (crate-dep (name "serde") (req "^1.0.188") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "1pa3d41byqisb1h2bcdv54f3l3g1qgbkzxl35cwljxzmnlc619ik")))

(define-public crate-degrees-0.2 (crate (name "degrees") (vers "0.2.0") (deps (list (crate-dep (name "serde") (req "^1.0.188") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "1sp3513iqgvi8sngnh5maq0zdbysd381z12cn55chyx3363jw37k")))

(define-public crate-degrees-0.3 (crate (name "degrees") (vers "0.3.0") (deps (list (crate-dep (name "serde") (req "^1.0.99") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.99") (features (quote ("derive"))) (default-features #t) (kind 2)) (crate-dep (name "serde_json") (req "^1.0.99") (default-features #t) (kind 2)))) (hash "1skzhvdmappcw8mjx4faxnv0f5fm3pzrpyqn9d0yw60knpjanrvi") (v 2) (features2 (quote (("serde" "dep:serde" "serde/derive"))))))

(define-public crate-degrees-0.4 (crate (name "degrees") (vers "0.4.0") (deps (list (crate-dep (name "serde") (req "^1.0.99") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.99") (features (quote ("derive"))) (default-features #t) (kind 2)) (crate-dep (name "serde_json") (req "^1.0.107") (default-features #t) (kind 2)))) (hash "0zkknl86d53d4rlik635qd29cadrb1mv1k92ffk2gy9yrvcbix1c") (v 2) (features2 (quote (("serde" "dep:serde" "serde/derive"))))))

(define-public crate-degrees-0.5 (crate (name "degrees") (vers "0.5.0") (deps (list (crate-dep (name "serde") (req "^1.0.188") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.188") (features (quote ("derive"))) (default-features #t) (kind 2)) (crate-dep (name "serde_json") (req "^1.0.107") (default-features #t) (kind 2)))) (hash "1rxfy1qyy3137040rz8gq26wbclazgfdkn3cy21ym7f798vsdl9m") (v 2) (features2 (quote (("serde" "dep:serde" "serde/derive"))))))

(define-public crate-degrees-0.6 (crate (name "degrees") (vers "0.6.0") (deps (list (crate-dep (name "serde") (req "^1.0.188") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.188") (features (quote ("derive"))) (default-features #t) (kind 2)) (crate-dep (name "serde_json") (req "^1.0.107") (default-features #t) (kind 2)))) (hash "19v0192cg2skmig6k65l03jpgjc9xqf7fww8zkl1zkfva9v7360n") (v 2) (features2 (quote (("serde" "dep:serde" "serde/derive"))))))

