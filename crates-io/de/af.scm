(define-module (crates-io de af) #:use-module (crates-io))

(define-public crate-deaf-0.1 (crate (name "deaf") (vers "0.1.2") (deps (list (crate-dep (name "enumflags2") (req "^0.7.5") (default-features #t) (kind 0)) (crate-dep (name "num_enum") (req "^0.5.9") (default-features #t) (kind 0)) (crate-dep (name "once_cell") (req "^1.18.0") (default-features #t) (kind 0)) (crate-dep (name "paste") (req "^1.0.11") (default-features #t) (kind 0)) (crate-dep (name "serial_test") (req "^2.0.0") (default-features #t) (kind 2)) (crate-dep (name "thiserror") (req "^1.0.38") (default-features #t) (kind 0)))) (hash "0qb9idac4b0j5jsnn6r1zppjw3q87527l8b6j1fb4ihmvav36b5l")))

