(define-module (crates-io de on) #:use-module (crates-io))

(define-public crate-deonym-0.0.0 (crate (name "deonym") (vers "0.0.0") (hash "0ynn59435kz62v7pvd2grlhv7vm9pzq0wwc8wdzh0mxay1dbanas") (yanked #t)))

(define-public crate-deonym-0.1 (crate (name "deonym") (vers "0.1.0") (deps (list (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)) (crate-dep (name "rsa") (req "^0.5.0") (default-features #t) (kind 0)) (crate-dep (name "sha2") (req "^0.10.2") (default-features #t) (kind 0)) (crate-dep (name "tungstenite") (req "^0.16.0") (default-features #t) (kind 0)))) (hash "1wjhppx61rzj0sqyn6g0iivpkx0scj5xfhjgjj0wjzj3yky5q9c1") (yanked #t)))

(define-public crate-deonym-0.1 (crate (name "deonym") (vers "0.1.1") (deps (list (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)) (crate-dep (name "rsa") (req "^0.5.0") (default-features #t) (kind 0)) (crate-dep (name "sha2") (req "^0.10.2") (default-features #t) (kind 0)) (crate-dep (name "tungstenite") (req "^0.16.0") (default-features #t) (kind 0)))) (hash "1g2aljddxp2dgqnv0lb210dlxynb2fb22iq8hn1zf1ynp8jm4hbz") (yanked #t)))

(define-public crate-deonym-0.1 (crate (name "deonym") (vers "0.1.2") (deps (list (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)) (crate-dep (name "rsa") (req "^0.5.0") (default-features #t) (kind 0)) (crate-dep (name "sha2") (req "^0.10.2") (default-features #t) (kind 0)) (crate-dep (name "tungstenite") (req "^0.16.0") (default-features #t) (kind 0)))) (hash "0rbckv3xyy54i6fzy92x5bgcl8jq0kzssnbkmn1bb3x9zjsb5sch") (yanked #t)))

