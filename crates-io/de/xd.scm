(define-module (crates-io de xd) #:use-module (crates-io))

(define-public crate-dexd-0.1 (crate (name "dexd") (vers "0.1.0") (deps (list (crate-dep (name "byteorder") (req "^1.2.4") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^2.32.0") (default-features #t) (kind 0)))) (hash "0bjh680kxg26pza2rqcn24fyfjlmvh444grk19w6x5b8bks18cwh")))

