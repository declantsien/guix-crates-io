(define-module (crates-io de x_) #:use-module (crates-io))

(define-public crate-dex_checksum_tools-0.1 (crate (name "dex_checksum_tools") (vers "0.1.0") (deps (list (crate-dep (name "adler32") (req "^1.2.0") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.3") (default-features #t) (kind 0)))) (hash "0bsvhl344iv6zqi94hbniwnv3cdz0idk2zn1vaykwyp3p90plhvl")))

