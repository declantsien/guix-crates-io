(define-module (crates-io de fe) #:use-module (crates-io))

(define-public crate-defer-0.1 (crate (name "defer") (vers "0.1.0") (hash "1pmcz4av2wvw8zrccmac86dsyy34qlwacdhajp1qjpjx6jk0axk4")))

(define-public crate-defer-0.2 (crate (name "defer") (vers "0.2.0") (hash "14k1q2c4djdw0bayzknzhf8f8mn350nrq0dfxblf2w16rbhf07b9")))

(define-public crate-defer-0.2 (crate (name "defer") (vers "0.2.1") (hash "1s7qig25n27rajvsn013sb8k6bgdv67936yz5dwb37yzr1qp234k")))

(define-public crate-defer-drop-1 (crate (name "defer-drop") (vers "1.0.0") (deps (list (crate-dep (name "crossbeam") (req "^0.7.3") (default-features #t) (kind 0)))) (hash "0y7rdw3jfkcnr95i5mvb4xk9akv9kmq3gsqqr9qb9x4d74anpdg8")))

(define-public crate-defer-drop-1 (crate (name "defer-drop") (vers "1.0.1") (deps (list (crate-dep (name "crossbeam-channel") (req "^0.4.2") (default-features #t) (kind 0)) (crate-dep (name "once_cell") (req "^1.4.0") (default-features #t) (kind 0)))) (hash "1d3pmmn5k2ir3yv8z8fnv4jprs5aijkz5pbdyl8x8kp18m90bbhq")))

(define-public crate-defer-drop-1 (crate (name "defer-drop") (vers "1.1.0") (deps (list (crate-dep (name "crossbeam-channel") (req "^0.4.2") (default-features #t) (kind 0)) (crate-dep (name "once_cell") (req "^1.4.0") (default-features #t) (kind 0)))) (hash "0wlyyd40mhibrlpdsicfnmgad9042cfydbcclmf8bmwzi4d89r57")))

(define-public crate-defer-drop-1 (crate (name "defer-drop") (vers "1.2.0") (deps (list (crate-dep (name "crossbeam-channel") (req "^0.4.2") (default-features #t) (kind 0)) (crate-dep (name "once_cell") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.130") (optional #t) (kind 0)))) (hash "1w505xzkzlrkby5cwja84l38nnr5qv5hk4ii18rb0ha3bq7cm2l2")))

(define-public crate-defer-drop-1 (crate (name "defer-drop") (vers "1.3.0") (deps (list (crate-dep (name "crossbeam-channel") (req "^0.5.6") (default-features #t) (kind 0)) (crate-dep (name "once_cell") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (optional #t) (kind 0)))) (hash "0mswjjksrr6fvsgxvp64mzwkjkzjmpwjfaw4n76jhsvalsgyq4zn")))

(define-public crate-defer-lite-1 (crate (name "defer-lite") (vers "1.0.0") (hash "1kkq70zrffvl75ybay3hmfia1saqmpy2jy0fs2dxpz60a03yz8fn")))

(define-public crate-defer-rs-0.1 (crate (name "defer-rs") (vers "0.1.0") (deps (list (crate-dep (name "defer-rs-impl") (req "=0.1.0") (default-features #t) (kind 0)))) (hash "0wkjpnh3c2fs15h723azc8xqv2ns1nkqvmym8xrjxjjrxa238isf")))

(define-public crate-defer-rs-impl-0.1 (crate (name "defer-rs-impl") (vers "0.1.0") (deps (list (crate-dep (name "quote") (req "^1.0.35") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2.0.53") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "1k138mlzs30w52la1ipb5h8zjjf5j1iryki9nam1l6f6wadxpl1g")))

(define-public crate-deferred-1 (crate (name "deferred") (vers "1.0.0") (hash "0nzrihxf7c007r2hpnckkk9wfgh30zvybrlmf642dsv0v2pw97g9")))

(define-public crate-deferred-1 (crate (name "deferred") (vers "1.1.0") (hash "03l4c6lh7srw3c5pzg5yjwm95acn7hfvbfqzadsz78fyz97m1ds9")))

(define-public crate-deferred-future-0.1 (crate (name "deferred-future") (vers "0.1.2") (deps (list (crate-dep (name "futures") (req "^0.3.28") (default-features #t) (kind 0)) (crate-dep (name "futures") (req "^0.3.28") (features (quote ("executor" "thread-pool"))) (default-features #t) (kind 2)) (crate-dep (name "futures-time") (req "^3.0.0") (default-features #t) (kind 2)))) (hash "177zvhwn4dbmlzjl4crv6ylk8hk69ns04v59c2ck8fcl2h18p0lq") (features (quote (("thread") ("local") ("default" "local" "thread"))))))

(define-public crate-deferred-future-0.1 (crate (name "deferred-future") (vers "0.1.3") (deps (list (crate-dep (name "futures") (req "^0.3.28") (default-features #t) (kind 0)) (crate-dep (name "futures") (req "^0.3.28") (features (quote ("executor" "thread-pool"))) (default-features #t) (kind 2)) (crate-dep (name "futures-time") (req "^3.0.0") (default-features #t) (kind 2)))) (hash "050j13vr066yzdczlzcyqz9rajnmwi04i2mydid0il63qlzxpgq9") (features (quote (("thread") ("local") ("default" "local" "thread"))))))

(define-public crate-deferred-future-0.1 (crate (name "deferred-future") (vers "0.1.4") (deps (list (crate-dep (name "futures") (req "^0.3.28") (default-features #t) (kind 0)) (crate-dep (name "futures") (req "^0.3.28") (features (quote ("executor" "thread-pool"))) (default-features #t) (target "cfg(not(target_arch = \"wasm32\"))") (kind 2)) (crate-dep (name "futures") (req "^0.3.28") (default-features #t) (target "cfg(target_arch = \"wasm32\")") (kind 2)) (crate-dep (name "futures-time") (req "^3.0.0") (default-features #t) (target "cfg(not(target_arch = \"wasm32\"))") (kind 2)) (crate-dep (name "wasm-bindgen-test") (req "^0.3.28") (default-features #t) (target "cfg(target_arch = \"wasm32\")") (kind 2)) (crate-dep (name "wasm-gloo-dom-events") (req "^0.1.2") (default-features #t) (target "cfg(target_arch = \"wasm32\")") (kind 2)))) (hash "0pfz8mrrh4qc2w3zz6y9x18nrb4ak15ad4s4dmp6nsxr37dh0f55") (features (quote (("thread") ("local") ("default" "local" "thread"))))))

(define-public crate-deferred-future-0.1 (crate (name "deferred-future") (vers "0.1.5") (deps (list (crate-dep (name "futures") (req "^0.3.28") (default-features #t) (kind 0)) (crate-dep (name "futures") (req "^0.3.28") (features (quote ("executor" "thread-pool"))) (default-features #t) (target "cfg(not(target_arch = \"wasm32\"))") (kind 2)) (crate-dep (name "futures") (req "^0.3.28") (default-features #t) (target "cfg(target_arch = \"wasm32\")") (kind 2)) (crate-dep (name "futures-time") (req "^3.0.0") (default-features #t) (target "cfg(not(target_arch = \"wasm32\"))") (kind 2)) (crate-dep (name "wasm-bindgen-test") (req "^0.3.37") (default-features #t) (target "cfg(target_arch = \"wasm32\")") (kind 2)) (crate-dep (name "wasm-gloo-dom-events") (req "^0.2.0") (default-features #t) (target "cfg(target_arch = \"wasm32\")") (kind 2)))) (hash "0fzk6hpa8i1lja69f55c0bd39p77ss8p94pc0c13bjxk0aris4xm") (features (quote (("thread") ("nodejs") ("local") ("default" "local" "thread"))))))

(define-public crate-deferred-reference-0.1 (crate (name "deferred-reference") (vers "0.1.0") (hash "0n3apbsfd9grf5sl7jjd84grbjgvar5p3ngv3d1y6xdmkgmahaim") (features (quote (("unstable" "slice_ptr_len" "coerce_unsized") ("slice_ptr_len") ("default" "unstable") ("coerce_unsized")))) (yanked #t)))

(define-public crate-deferred-reference-0.1 (crate (name "deferred-reference") (vers "0.1.1") (hash "1fznh6lw2fv1w2wxfr6ix8gs9s1asi3gb44yw429zw946sw20p33") (features (quote (("unstable" "slice_ptr_len" "coerce_unsized") ("slice_ptr_len") ("default" "unstable") ("coerce_unsized"))))))

(define-public crate-deferred-reference-0.1 (crate (name "deferred-reference") (vers "0.1.2") (hash "0d4hmdvnnlga6gzrbiby5s7jl90ik5xdgr90gwmpaaavqmxv6n90") (features (quote (("unstable" "slice_ptr_len" "coerce_unsized") ("slice_ptr_len") ("default" "unstable") ("coerce_unsized"))))))

(define-public crate-deferred_vector-0.1 (crate (name "deferred_vector") (vers "0.1.0") (hash "1fqiw71alnyp9yj6nmq2mpj7blhf9pbp5yjcya5yqmaghgwpjzs2")))

(define-public crate-deferred_vector-0.1 (crate (name "deferred_vector") (vers "0.1.1") (hash "1ivp6abric897jfb9p960w92irlgxdf67nv0zpgg6vbk19dlj0cq")))

(define-public crate-deferred_vector-0.1 (crate (name "deferred_vector") (vers "0.1.2") (hash "15bc8zlhckg82p9q8k423zhn5q3q8wf3xh445i433qlbb4mpk275")))

(define-public crate-deferrer-0.1 (crate (name "deferrer") (vers "0.1.1") (hash "1hyqy8yznx15j31x7slxnir8dag3fk1z2vdc7nvnvvpmsmzlabnw")))

(define-public crate-deferrer-0.1 (crate (name "deferrer") (vers "0.1.2") (hash "01rd3llgq8n6pwc5kspmih4wirxxl46f5vyh9ixvmrvpxj4nmfxb")))

(define-public crate-deferrer-0.1 (crate (name "deferrer") (vers "0.1.3") (hash "190k74paa9sbnccii0qk51pg9yhvi2kwj37w5709gjniip906sgy")))

(define-public crate-deferrer-0.1 (crate (name "deferrer") (vers "0.1.4") (hash "0llwlkc6cdzr1928g086vgp7xnaczv6phhg1h2s95j1jb68pc0zx")))

