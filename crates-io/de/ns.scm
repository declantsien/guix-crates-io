(define-module (crates-io de ns) #:use-module (crates-io))

(define-public crate-dens-0.1 (crate (name "dens") (vers "0.1.0") (deps (list (crate-dep (name "criterion") (req "^0.5") (default-features #t) (kind 2)) (crate-dep (name "density-sys") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8") (default-features #t) (kind 2)))) (hash "19ycif64vz2x6kyq0hnkn5n04i41iiq3iya8a7pqa48j63bxjpad")))

(define-public crate-dense-1 (crate (name "dense") (vers "1.0.0") (deps (list (crate-dep (name "mnist_read") (req ">=1.0.0, <2.0.0") (default-features #t) (kind 0)) (crate-dep (name "ndarray") (req ">=0.13.1, <0.14.0") (default-features #t) (kind 0)))) (hash "0i2j4lvl247jb5npgn5vrwp67gzc62fxy7px64p344pkadhxsvi3")))

(define-public crate-dense-1 (crate (name "dense") (vers "1.0.1") (deps (list (crate-dep (name "mnist_read") (req ">=1.0.1, <2.0.0") (default-features #t) (kind 0)) (crate-dep (name "ndarray") (req ">=0.13.1, <0.14.0") (default-features #t) (kind 0)))) (hash "0lfmi6l9p13l70dp6883m06nsfy1ifbzm3r23lxiphql2l9zjzlr")))

(define-public crate-dense-1 (crate (name "dense") (vers "1.0.2") (deps (list (crate-dep (name "mnist_read") (req ">=1.0.1, <2.0.0") (default-features #t) (kind 0)) (crate-dep (name "ndarray") (req ">=0.13.1, <0.14.0") (default-features #t) (kind 0)))) (hash "1c05idh25f9xjladm6yqp05s37mjzqf8y201wpgdiamq2lvvck1q")))

(define-public crate-dense-1 (crate (name "dense") (vers "1.1.0") (deps (list (crate-dep (name "mnist_read") (req "^1.0.1") (default-features #t) (kind 0)) (crate-dep (name "ndarray") (req "^0.14.0") (default-features #t) (kind 0)))) (hash "1s4f1l079cdxz2iqpz3vfba9c9krwwhc7ansiv50mks4cnqzxpd7")))

(define-public crate-dense-2 (crate (name "dense") (vers "2.0.0") (deps (list (crate-dep (name "mnist_read") (req "^1.0.1") (default-features #t) (kind 2)) (crate-dep (name "ndarray") (req "^0.15.3") (default-features #t) (kind 0)) (crate-dep (name "num") (req "^0.4.0") (default-features #t) (kind 0)) (crate-dep (name "num-bytes") (req "^0.3.0") (default-features #t) (kind 0)))) (hash "19qkscw6a8mh4marwm6iapigmay09vrrzw2wryjd8ziccn1jrd1k")))

(define-public crate-dense-heap-0.1 (crate (name "dense-heap") (vers "0.1.0") (hash "0262qa8hyharr9q6ga9ai1779amz9lhrypinigfdn095gsh27m6g") (yanked #t)))

(define-public crate-dense-heap-0.1 (crate (name "dense-heap") (vers "0.1.1") (hash "0rgdps8ngzk1f48qm08bira5vlkrp9rp7sm0vlw89kgc5x308mkn") (yanked #t)))

(define-public crate-dense-heap-0.1 (crate (name "dense-heap") (vers "0.1.2") (hash "17zr77gbayqq3v9c6kz4551m8r2kdk1c62lmn3rf97lsgy9cxvi2")))

(define-public crate-dense_bitset-0.1 (crate (name "dense_bitset") (vers "0.1.0") (hash "1ln3al0l5z87jq84rrpqnm3m7li5kqacwfhikq0klgy58lzh9is0")))

(define-public crate-dense_bitset-0.1 (crate (name "dense_bitset") (vers "0.1.1") (hash "163400rsg1rz1sx143g4gxqny6vncsyz9chs9fkl6f5m598g258x")))

(define-public crate-dense_mats-0.1 (crate (name "dense_mats") (vers "0.1.0") (hash "06nbcan6yy2n3f6cskvhy8lsgbwqyfr5nc95p6v9129p4xq76a30")))

(define-public crate-dense_mats-0.1 (crate (name "dense_mats") (vers "0.1.1") (hash "1vj4kiiwrxgdhs5y60g198y238sgxyp125bqihihakbp27arkjhl")))

(define-public crate-dense_mats-0.2 (crate (name "dense_mats") (vers "0.2.0") (deps (list (crate-dep (name "num") (req "*") (default-features #t) (kind 0)))) (hash "18dhvn8l5ax9hmv7r8r0gr2gk2389rxz1vjv7183cj43zz2hhz6v")))

(define-public crate-dense_mats-0.3 (crate (name "dense_mats") (vers "0.3.0") (deps (list (crate-dep (name "num") (req "*") (default-features #t) (kind 0)))) (hash "0nmpr1bv107d5726qj4ww40q0qf11sdpq1djp1lhr1j5fxlc0m99")))

(define-public crate-dense_mats-0.3 (crate (name "dense_mats") (vers "0.3.1") (deps (list (crate-dep (name "num") (req "*") (default-features #t) (kind 0)))) (hash "11ki3kgyg9k0lwn3y9f1rw74cb2l2zsbfh75v3j5iy1dwv2lsb2q")))

(define-public crate-dense_mats-0.3 (crate (name "dense_mats") (vers "0.3.2") (deps (list (crate-dep (name "num") (req "*") (default-features #t) (kind 0)))) (hash "0fw8akdwcc5qp53ahsak6rdnkjik25sr2l363cwlg5l2ln8mgxjr")))

(define-public crate-dense_mats-0.3 (crate (name "dense_mats") (vers "0.3.3") (deps (list (crate-dep (name "num") (req "*") (default-features #t) (kind 0)))) (hash "0ba7sb5xk0xvnkr5d1q08s7jriknwlv362b2yp64hi1amgy2syyy")))

(define-public crate-dense_mats-0.3 (crate (name "dense_mats") (vers "0.3.4") (deps (list (crate-dep (name "num") (req "*") (default-features #t) (kind 0)))) (hash "16jnd0v1qv6lfnlxggdvlmirnxmafgdg6mrip46axsh6h5mkcra5")))

(define-public crate-dense_mats-0.3 (crate (name "dense_mats") (vers "0.3.5") (deps (list (crate-dep (name "num") (req "^0.1.27") (default-features #t) (kind 0)))) (hash "1ph0gkysa2xpnmnhr8nfgi8k04a10ykljnbxj1l8sc8bsliangj9")))

(define-public crate-dense_mats-0.4 (crate (name "dense_mats") (vers "0.4.0") (deps (list (crate-dep (name "num") (req "^0.1.27") (default-features #t) (kind 0)))) (hash "1k8069bsirk29fblvyx5sk15c70f0xcsqzp5x7nnlqpjb7pzlr7q")))

(define-public crate-densearray-0.1 (crate (name "densearray") (vers "0.1.0") (deps (list (crate-dep (name "byteorder") (req "^0.5") (default-features #t) (kind 0)) (crate-dep (name "densearray_kernels") (req "^0.0.1") (default-features #t) (kind 0)) (crate-dep (name "openblas_ffi") (req "^0.1") (default-features #t) (kind 0)))) (hash "00sn4024j7pahqrlmmybwkvlgbgsx0li542qh4wsc7j1zi28r3gz")))

(define-public crate-densearray-0.1 (crate (name "densearray") (vers "0.1.1") (deps (list (crate-dep (name "byteorder") (req "^0.5") (default-features #t) (kind 0)) (crate-dep (name "cblas_ffi") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "densearray_kernels") (req "^0.0.1") (default-features #t) (kind 0)) (crate-dep (name "mkl_link") (req "^0.1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "openblas_ffi") (req "^0.1") (default-features #t) (kind 0)))) (hash "14yz0xmx3lb0s6kj0mncpqjdnrfi3lrna54s4mmv4djcb37mgs2m") (features (quote (("mkl_parallel" "mkl_link/openmp") ("default"))))))

(define-public crate-densearray_kernels-0.0.1 (crate (name "densearray_kernels") (vers "0.0.1") (deps (list (crate-dep (name "gcc") (req "^0.3") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "0yc0kj7addx2iwqlsw6rygk9i6in0ldbq0bi3w7s9xzzlqx07s0a")))

(define-public crate-densemap-0.1 (crate (name "densemap") (vers "0.1.1") (deps (list (crate-dep (name "criterion") (req "^0.5") (features (quote ("html_reports"))) (default-features #t) (kind 2)) (crate-dep (name "slab") (req "^0.4") (default-features #t) (kind 2)) (crate-dep (name "slotmap") (req "^1") (default-features #t) (kind 2)))) (hash "1gg6lv1nbjgivvkclsbbxcw8va5cyb7x29hfz5s3fjywj1cpz7qc")))

(define-public crate-densemap-0.1 (crate (name "densemap") (vers "0.1.2") (deps (list (crate-dep (name "criterion") (req "^0.5") (features (quote ("html_reports"))) (default-features #t) (kind 2)) (crate-dep (name "serde") (req "^1") (optional #t) (kind 0)) (crate-dep (name "slab") (req "^0.4") (default-features #t) (kind 2)) (crate-dep (name "slotmap") (req "^1") (default-features #t) (kind 2)))) (hash "1kn14x21qzbqkw4l1xy22ygiym1bqi4p9wkjcaqs69lfrvdc7a10") (features (quote (("std") ("default" "std"))))))

(define-public crate-densemap-0.1 (crate (name "densemap") (vers "0.1.3") (deps (list (crate-dep (name "criterion") (req "^0.5") (features (quote ("html_reports"))) (default-features #t) (kind 2)) (crate-dep (name "serde") (req "^1") (optional #t) (kind 0)) (crate-dep (name "slab") (req "^0.4") (default-features #t) (kind 2)) (crate-dep (name "slotmap") (req "^1") (default-features #t) (kind 2)))) (hash "143p7pscivsd99r1js9bm11jnzhklyjr0663x1qgcbxfxrcbyscp") (features (quote (("std") ("default" "std"))))))

(define-public crate-densevec-0.1 (crate (name "densevec") (vers "0.1.0") (hash "1640ncs32xfcfj6i9schff3wvlm9jl2lxzkws3w8p301zf1fmxcv")))

(define-public crate-densevec-0.2 (crate (name "densevec") (vers "0.2.0") (deps (list (crate-dep (name "rayon") (req "^1.0") (optional #t) (default-features #t) (kind 0)))) (hash "1zwbjmp2fqcjflvdyixwc29v73zlx08k6jyr4v3zamnfp3qwih4s") (features (quote (("unstable") ("parallel" "rayon"))))))

(define-public crate-densevec-0.3 (crate (name "densevec") (vers "0.3.0") (deps (list (crate-dep (name "rayon") (req "^1.0") (optional #t) (default-features #t) (kind 0)))) (hash "1814lqi655kinysjvmy98f54ngwnbajiaj917w86lsbfz6yl09aj") (features (quote (("unstable") ("parallel" "rayon")))) (yanked #t)))

(define-public crate-densevec-0.3 (crate (name "densevec") (vers "0.3.1") (deps (list (crate-dep (name "rayon") (req "^1.0") (optional #t) (default-features #t) (kind 0)))) (hash "1n7r3ahnm6a416nd072msl1kyyc4wyjkfqkw1g14zmxljas2zjks") (features (quote (("unstable") ("parallel" "rayon"))))))

(define-public crate-densevec-0.3 (crate (name "densevec") (vers "0.3.2") (deps (list (crate-dep (name "rayon") (req "^1.0") (optional #t) (default-features #t) (kind 0)))) (hash "1qb1qmjgwk86qmpk6da29an4i57djs8qvxvlhgvdjx5g3qb6d6a8") (features (quote (("unstable") ("parallel" "rayon"))))))

(define-public crate-densevec-0.3 (crate (name "densevec") (vers "0.3.3") (deps (list (crate-dep (name "rayon") (req "^1.0") (optional #t) (default-features #t) (kind 0)))) (hash "00c0x7w448xc6rnylwiz9pq4xylcxqdcvby00jai6pii67sadmcn") (features (quote (("unstable") ("parallel" "rayon"))))))

(define-public crate-densevec-0.3 (crate (name "densevec") (vers "0.3.4") (deps (list (crate-dep (name "rayon") (req "^1.0") (optional #t) (default-features #t) (kind 0)))) (hash "0lgd3xlxccw7p6rlxvasapm78c6kmby51vafr7qpgbs1v43gm6i5") (features (quote (("unstable") ("parallel" "rayon"))))))

(define-public crate-densevec-0.3 (crate (name "densevec") (vers "0.3.5") (deps (list (crate-dep (name "rayon") (req "^1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1") (optional #t) (default-features #t) (kind 0)))) (hash "1jzshb9f7hsdmcmk8y8208vv8xm8bcih8a1kglr46hvz2arzkr54") (features (quote (("unstable") ("serialize" "serde" "serde_derive") ("parallel" "rayon"))))))

(define-public crate-densevec-0.3 (crate (name "densevec") (vers "0.3.6") (deps (list (crate-dep (name "rayon") (req "^1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1") (optional #t) (default-features #t) (kind 0)))) (hash "17q864b9c7hmyiz1kpvpvgphi7ydjyvpjv5vf9xwswin058i6cxh") (features (quote (("unstable") ("serialize" "serde" "serde_derive") ("parallel" "rayon"))))))

(define-public crate-densevec-0.4 (crate (name "densevec") (vers "0.4.0") (deps (list (crate-dep (name "rayon") (req "^1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1") (optional #t) (default-features #t) (kind 0)))) (hash "0jvyhrxzh2790wybahj1f78bnl3zw5y0ayr90k2gf20scl6909ib") (features (quote (("unstable") ("serialize" "serde" "serde_derive"))))))

(define-public crate-densevec-0.5 (crate (name "densevec") (vers "0.5.0") (deps (list (crate-dep (name "rayon") (req "^1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1") (optional #t) (default-features #t) (kind 0)))) (hash "1r5r7iinkc4sab0yc2cg4wc4np2whwlmw31a6s00dkiy6b37kpb9") (features (quote (("unstable") ("serialize" "serde" "serde_derive"))))))

(define-public crate-densevec-0.5 (crate (name "densevec") (vers "0.5.1") (deps (list (crate-dep (name "rayon") (req "^1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1") (optional #t) (default-features #t) (kind 0)))) (hash "1lydnb58bpycaapykl7fi9d0h7sfvyr841i18z7vqxhxl7fnm8jb") (features (quote (("unstable") ("serialize" "serde" "serde_derive"))))))

(define-public crate-density-0.1 (crate (name "density") (vers "0.1.0") (hash "0iayqp6nsdmmhhl9qm30ff3s2g5nqg5m7a2rmwbcwgyfw9ragakr")))

(define-public crate-density-0.2 (crate (name "density") (vers "0.2.0") (hash "0gp4bmyymhvw66kicyb89idwdb2sq0jw4mn0hyr9nvaz2mbhhwyv")))

(define-public crate-density-0.3 (crate (name "density") (vers "0.3.0") (hash "0s0wgb8lzfg4q75l1zpyygc6xkpkyhx2kbdn87pmcgxbx7vcfalq")))

(define-public crate-density-0.3 (crate (name "density") (vers "0.3.1") (hash "0dkiql9d41sav7n96ca9cgbjifcv67k3z65kv3fgq0fyxgligjv2")))

(define-public crate-density-0.4 (crate (name "density") (vers "0.4.0") (deps (list (crate-dep (name "getopts") (req "^0.2") (default-features #t) (kind 0)))) (hash "1lzyig7dfnyaffirj6rc2vpzxzljdnlz81swvmpvm2h76pn36ymb")))

(define-public crate-density-0.5 (crate (name "density") (vers "0.5.0") (deps (list (crate-dep (name "getopts") (req "^0.2") (default-features #t) (kind 0)))) (hash "1c8kj17hjx7qad8pcsznzwc0p4bnjhpwpih6mbbaprw3i6fcgvrb")))

(define-public crate-density-0.5 (crate (name "density") (vers "0.5.1") (deps (list (crate-dep (name "getopts") (req "^0.2") (default-features #t) (kind 0)))) (hash "1rl6pf4qhqcd0j72qyj53p45cc2mkhi9vx9jzgp96gv7f42sk0d9")))

(define-public crate-density-mesh-cli-1 (crate (name "density-mesh-cli") (vers "1.0.0") (deps (list (crate-dep (name "clap") (req "^2.23") (default-features #t) (kind 0)) (crate-dep (name "density-mesh-core") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "density-mesh-image") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "image") (req "^0.23") (features (quote ("png"))) (kind 0)) (crate-dep (name "obj-exporter") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "serde_yaml") (req "^0.8") (default-features #t) (kind 0)))) (hash "11rp303x9cqsqylqma29i56wbgsrsyzjj52jnqs98x1x7fadmbna")))

(define-public crate-density-mesh-cli-1 (crate (name "density-mesh-cli") (vers "1.1.0") (deps (list (crate-dep (name "clap") (req "^2.23") (default-features #t) (kind 0)) (crate-dep (name "density-mesh-core") (req "^1.0") (features (quote ("parallel"))) (default-features #t) (kind 0)) (crate-dep (name "density-mesh-image") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "image") (req "^0.23") (features (quote ("png"))) (kind 0)) (crate-dep (name "obj-exporter") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "serde_yaml") (req "^0.8") (default-features #t) (kind 0)))) (hash "0nx20dy9in2b50shp6j5a2m7cnha5fwj8d1smrjdxvgx6xcvi38y")))

(define-public crate-density-mesh-cli-1 (crate (name "density-mesh-cli") (vers "1.2.0") (deps (list (crate-dep (name "clap") (req "^2.23") (default-features #t) (kind 0)) (crate-dep (name "density-mesh-core") (req "^1.0") (features (quote ("parallel"))) (default-features #t) (kind 0)) (crate-dep (name "density-mesh-image") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "image") (req "^0.23") (features (quote ("png"))) (kind 0)) (crate-dep (name "obj-exporter") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "serde_yaml") (req "^0.8") (default-features #t) (kind 0)))) (hash "0v23nr8jv17vksdbswya4ir37bmp6j6acjhrrgy933srddwlzka3")))

(define-public crate-density-mesh-cli-1 (crate (name "density-mesh-cli") (vers "1.3.0") (deps (list (crate-dep (name "clap") (req "^2.23") (default-features #t) (kind 0)) (crate-dep (name "density-mesh-core") (req "^1.3") (features (quote ("parallel"))) (default-features #t) (kind 0)) (crate-dep (name "density-mesh-image") (req "^1.3") (default-features #t) (kind 0)) (crate-dep (name "image") (req "^0.23") (features (quote ("png"))) (kind 0)) (crate-dep (name "obj-exporter") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "serde_yaml") (req "^0.8") (default-features #t) (kind 0)))) (hash "1if7ryp23sfpb93lbpkqljz7g8x8dja3gb82l9i0frnba979cz2s")))

(define-public crate-density-mesh-cli-1 (crate (name "density-mesh-cli") (vers "1.4.0") (deps (list (crate-dep (name "clap") (req "^2.23") (default-features #t) (kind 0)) (crate-dep (name "density-mesh-core") (req "^1.3") (features (quote ("parallel"))) (default-features #t) (kind 0)) (crate-dep (name "density-mesh-image") (req "^1.3") (default-features #t) (kind 0)) (crate-dep (name "image") (req "^0.23") (features (quote ("png"))) (kind 0)) (crate-dep (name "obj-exporter") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "serde_yaml") (req "^0.8") (default-features #t) (kind 0)))) (hash "15wi2lwvl94pajydk2x55cv2rxbcrbbnbkpgsl2k5pzb75963cdl")))

(define-public crate-density-mesh-cli-1 (crate (name "density-mesh-cli") (vers "1.5.0") (deps (list (crate-dep (name "clap") (req "^2.23") (default-features #t) (kind 0)) (crate-dep (name "density-mesh-core") (req "^1.3") (features (quote ("parallel"))) (default-features #t) (kind 0)) (crate-dep (name "density-mesh-image") (req "^1.3") (default-features #t) (kind 0)) (crate-dep (name "image") (req "^0.23") (features (quote ("png"))) (kind 0)) (crate-dep (name "obj-exporter") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "serde_yaml") (req "^0.8") (default-features #t) (kind 0)))) (hash "1dckkk67xlcbippjczn2w4sckxph6ra3i2i553rdxz1n77v6h1gb")))

(define-public crate-density-mesh-core-1 (crate (name "density-mesh-core") (vers "1.0.0") (deps (list (crate-dep (name "serde") (req "^1") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "triangulation") (req "^0.1") (default-features #t) (kind 0)))) (hash "0v7fz10war9fpvjnj16b9zawb1rlgx4m7shg9l6m8j6v2p3s9ml1")))

(define-public crate-density-mesh-core-1 (crate (name "density-mesh-core") (vers "1.1.0") (deps (list (crate-dep (name "rayon") (req "^1.4") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "triangulation") (req "^0.1") (default-features #t) (kind 0)))) (hash "0cpiiss9vpixqa3dbjqrgxj1arpyv1izxcz5s4ymfb252kdvk8dl") (features (quote (("parallel" "rayon"))))))

(define-public crate-density-mesh-core-1 (crate (name "density-mesh-core") (vers "1.2.0") (deps (list (crate-dep (name "rayon") (req "^1.4") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "triangulation") (req "^0.1") (default-features #t) (kind 0)))) (hash "0aljr0in5bh73m1awr9im4vvi8v81phsxvkr086mph5qkww9763k") (features (quote (("parallel" "rayon"))))))

(define-public crate-density-mesh-core-1 (crate (name "density-mesh-core") (vers "1.3.0") (deps (list (crate-dep (name "rayon") (req "^1.4") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "triangulation") (req "^0.1") (default-features #t) (kind 0)))) (hash "09m3vg8k61vjfrx4rghq8lil1ynwd2n2x61n310ix0wl9x1jzv7w") (features (quote (("parallel" "rayon"))))))

(define-public crate-density-mesh-core-1 (crate (name "density-mesh-core") (vers "1.4.0") (deps (list (crate-dep (name "rayon") (req "^1.4") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "triangulation") (req "^0.1") (default-features #t) (kind 0)))) (hash "0vynbzhbm49viq7xv76iwy26nhamicsbnxnf5pska873xa7abpbr") (features (quote (("parallel" "rayon"))))))

(define-public crate-density-mesh-core-1 (crate (name "density-mesh-core") (vers "1.5.0") (deps (list (crate-dep (name "rayon") (req "^1.4") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "triangulation") (req "^0.1") (default-features #t) (kind 0)))) (hash "0fc8yi1ryy0ajkcxl1646gmmjcw2l3cnnvxr6zdxgmc0hw86ngjw") (features (quote (("parallel" "rayon"))))))

(define-public crate-density-mesh-image-1 (crate (name "density-mesh-image") (vers "1.0.0") (deps (list (crate-dep (name "density-mesh-core") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "image") (req "^0.23") (features (quote ("png"))) (kind 0)) (crate-dep (name "serde") (req "^1") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "004nng34hgfm50ndviz8h51b0zph9nky0i7xk2w0b7ivlv81ypah")))

(define-public crate-density-mesh-image-1 (crate (name "density-mesh-image") (vers "1.1.0") (deps (list (crate-dep (name "density-mesh-core") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "image") (req "^0.23") (features (quote ("png"))) (kind 0)) (crate-dep (name "serde") (req "^1") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "094acfnhpaki7ffbalvi0xg7wkd3p53k2s98v5bxhgv6bl11bpk2") (features (quote (("parallel" "density-mesh-core/parallel"))))))

(define-public crate-density-mesh-image-1 (crate (name "density-mesh-image") (vers "1.2.0") (deps (list (crate-dep (name "density-mesh-core") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "image") (req "^0.23") (features (quote ("png"))) (kind 0)) (crate-dep (name "serde") (req "^1") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "0c7c2r9vii87h0c5bwhbahvi8cqia1yfnawnf00903ah021m37rj") (features (quote (("parallel" "density-mesh-core/parallel"))))))

(define-public crate-density-mesh-image-1 (crate (name "density-mesh-image") (vers "1.3.0") (deps (list (crate-dep (name "density-mesh-core") (req "^1.3") (default-features #t) (kind 0)) (crate-dep (name "image") (req "^0.23") (features (quote ("png"))) (kind 0)) (crate-dep (name "serde") (req "^1") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "16v4ansbrq778va4av7wzd506ywnil3m859hfqh4r2zgdb0jixg8") (features (quote (("parallel" "density-mesh-core/parallel"))))))

(define-public crate-density-mesh-image-1 (crate (name "density-mesh-image") (vers "1.4.0") (deps (list (crate-dep (name "density-mesh-core") (req "^1.3") (default-features #t) (kind 0)) (crate-dep (name "image") (req "^0.23") (features (quote ("png"))) (kind 0)) (crate-dep (name "serde") (req "^1") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "1hzsrvr6fc0p1i3g64h96syqn9090gyzbqnzhxph3027pjyvbdg2") (features (quote (("parallel" "density-mesh-core/parallel"))))))

(define-public crate-density-mesh-image-1 (crate (name "density-mesh-image") (vers "1.5.0") (deps (list (crate-dep (name "density-mesh-core") (req "^1.3") (default-features #t) (kind 0)) (crate-dep (name "image") (req "^0.23") (features (quote ("png"))) (kind 0)) (crate-dep (name "serde") (req "^1") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "1gz9zk7rkji7iwd1s6jlypnghchypqgzvqlkww5pk258map0lp3h") (features (quote (("parallel" "density-mesh-core/parallel"))))))

(define-public crate-density-sys-0.1 (crate (name "density-sys") (vers "0.1.0") (deps (list (crate-dep (name "bindgen") (req "^0.68") (default-features #t) (kind 1)) (crate-dep (name "cc") (req "^1") (default-features #t) (kind 1)))) (hash "1p1n5gw680jqb5i3lc1dlk5m489vysh222gwhkv0bj22w8ax6jwc")))

(define-public crate-densky-0.1 (crate (name "densky") (vers "0.1.0") (deps (list (crate-dep (name "ahash") (req "^0.8.3") (default-features #t) (kind 0)) (crate-dep (name "anstyle") (req "^1.0.0") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^4.3.3") (features (quote ("cargo" "unstable-styles"))) (default-features #t) (kind 0)) (crate-dep (name "densky-core") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "indicatif") (req "^0.17.5") (default-features #t) (kind 0)) (crate-dep (name "recv-dir") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "signal-hook") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "ureq") (req "^2.6.2") (default-features #t) (kind 0)))) (hash "1zxxjdf3vbfk3h1qrn5f6nvw5p1fy1yvm2q64q4wjb8c9w978mpb")))

(define-public crate-densky-core-0.1 (crate (name "densky-core") (vers "0.1.0") (deps (list (crate-dep (name "colored") (req "^2") (default-features #t) (kind 0)) (crate-dep (name "dynamic-html") (req "^1.0.0") (kind 0)) (crate-dep (name "pathdiff") (req "^0.2.1") (default-features #t) (kind 0)) (crate-dep (name "prettify-js") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "recur-fn") (req "^2.2.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.7.1") (default-features #t) (kind 0)) (crate-dep (name "walkdir") (req "^2.3.3") (default-features #t) (kind 0)))) (hash "054wc92rq8vn0gfkv0smiydx75lic1nbw96wdv0gzkhgmq8qw31r")))

