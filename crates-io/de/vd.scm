(define-module (crates-io de vd) #:use-module (crates-io))

(define-public crate-devd-rs-0.1 (crate (name "devd-rs") (vers "0.1.0") (deps (list (crate-dep (name "nix") (req "^0.9") (default-features #t) (kind 0)) (crate-dep (name "nom") (req "^3.2") (default-features #t) (kind 0)))) (hash "0x0xsd4dx3dvdxi7iqkff7c50r1rg4dnrnyb464xpl0badsxbyvk")))

(define-public crate-devd-rs-0.2 (crate (name "devd-rs") (vers "0.2.0") (deps (list (crate-dep (name "libc") (req "^0") (default-features #t) (kind 0)) (crate-dep (name "nom") (req "^3.2") (default-features #t) (kind 0)))) (hash "156j5w7phkdijm6dcagr0w7a22hwsmzv6slyg3jkxi7wml9d5wx5")))

(define-public crate-devd-rs-0.2 (crate (name "devd-rs") (vers "0.2.1") (deps (list (crate-dep (name "libc") (req "^0") (default-features #t) (kind 0)) (crate-dep (name "nom") (req "^3.2") (default-features #t) (kind 0)))) (hash "0k23q8a0vm6r96i7jqqv3m4smpshd2hf8cmpsc0g9fiq3i4arjg7")))

(define-public crate-devd-rs-0.3 (crate (name "devd-rs") (vers "0.3.0") (deps (list (crate-dep (name "libc") (req "^0") (default-features #t) (kind 0)) (crate-dep (name "nom") (req "^4") (default-features #t) (kind 0)))) (hash "19sjzr7kdsqxs0r3s5asz2vilhd6ncq1cxfw16crz7hddhb9y00d")))

(define-public crate-devd-rs-0.3 (crate (name "devd-rs") (vers "0.3.1") (deps (list (crate-dep (name "libc") (req "^0") (default-features #t) (kind 0)) (crate-dep (name "nom") (req "^5") (features (quote ("std"))) (kind 0)))) (hash "1qfc6bcdnsjlbwr9hma2ny10a7pv83bylrkp6jwxzazdravwqi8r")))

(define-public crate-devd-rs-0.3 (crate (name "devd-rs") (vers "0.3.2") (deps (list (crate-dep (name "libc") (req "^0") (default-features #t) (kind 0)) (crate-dep (name "nom") (req "^6") (features (quote ("std"))) (kind 0)))) (hash "0v8ys4453xy78m84rmiz8dmxx341w2y7mzsr6sm2s2375izky727")))

(define-public crate-devd-rs-0.3 (crate (name "devd-rs") (vers "0.3.3") (deps (list (crate-dep (name "libc") (req "^0") (default-features #t) (kind 0)) (crate-dep (name "nom") (req "^7") (features (quote ("std"))) (kind 0)) (crate-dep (name "nom-supreme") (req "^0.6.0") (default-features #t) (kind 0)))) (hash "1rq5yca930kh98xab2rrvxpz3wp4rdg1liy7ypx4j3pnyjcmmrad")))

(define-public crate-devd-rs-0.3 (crate (name "devd-rs") (vers "0.3.4") (deps (list (crate-dep (name "libc") (req "^0") (default-features #t) (kind 0)) (crate-dep (name "nom") (req "^7") (features (quote ("std"))) (kind 0)))) (hash "116zqhmdsh3lb1m329wdbkblwxfmcb2wvbdvgcpx6j2ibw8s8qcq")))

(define-public crate-devd-rs-0.3 (crate (name "devd-rs") (vers "0.3.5") (deps (list (crate-dep (name "libc") (req "^0") (default-features #t) (kind 0)) (crate-dep (name "nom") (req "^7") (features (quote ("std"))) (kind 0)))) (hash "031n8amnir06iybk7473d8vr3x5xmf5c4c5hj4qflspjws7mnc8c")))

(define-public crate-devd-rs-0.3 (crate (name "devd-rs") (vers "0.3.6") (deps (list (crate-dep (name "libc") (req "^0") (default-features #t) (kind 0)) (crate-dep (name "nom") (req "^7") (features (quote ("std"))) (kind 0)))) (hash "15fkr028silhhk2d6m0lhw9mql3nzhja7h01zi30nlchnl2g24wk")))

