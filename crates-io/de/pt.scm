(define-module (crates-io de pt) #:use-module (crates-io))

(define-public crate-depth-0.0.0 (crate (name "depth") (vers "0.0.0") (hash "1n2kdsznfcz58n6qxfxczkhpi9gsn9495qq4xw568phkhlwl9nnw")))

(define-public crate-depth-0.0.1 (crate (name "depth") (vers "0.0.1") (deps (list (crate-dep (name "clap") (req "^4.4.8") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "crates_io_api") (req "^0.8.2") (default-features #t) (kind 0)) (crate-dep (name "petgraph") (req "^0.6.4") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.192") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.108") (default-features #t) (kind 0)) (crate-dep (name "toml") (req "^0.8.8") (default-features #t) (kind 0)))) (hash "0prphq2pkmnz965j3ag6z2yhci0kq6ziakslkcxvp5v2qixg1y9c")))

(define-public crate-depth-0.0.2 (crate (name "depth") (vers "0.0.2") (deps (list (crate-dep (name "clap") (req "^4.4.8") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "crates_io_api") (req "^0.8.2") (default-features #t) (kind 0)) (crate-dep (name "petgraph") (req "^0.6.4") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.192") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.108") (default-features #t) (kind 0)) (crate-dep (name "toml") (req "^0.8.8") (default-features #t) (kind 0)))) (hash "01fzqy5gcrib1rkn5p056sv2w4d101j0kcyvx6cvirdnhf5s092k")))

(define-public crate-depth-0.0.3 (crate (name "depth") (vers "0.0.3") (deps (list (crate-dep (name "clap") (req "^4.4.8") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "crates_io_api") (req "^0.8.2") (default-features #t) (kind 0)) (crate-dep (name "petgraph") (req "^0.6.4") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.192") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.108") (default-features #t) (kind 0)) (crate-dep (name "toml") (req "^0.8.8") (default-features #t) (kind 0)))) (hash "0f2cc4vdwgcr62jskmhz10imbjs5bfgydi8cvqf89dk0d07wfr98")))

(define-public crate-depth-0.0.4 (crate (name "depth") (vers "0.0.4") (deps (list (crate-dep (name "clap") (req "^4.4.8") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "crates_io_api") (req "^0.8.2") (default-features #t) (kind 0)) (crate-dep (name "petgraph") (req "^0.6.4") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.192") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.108") (default-features #t) (kind 0)) (crate-dep (name "toml") (req "^0.8.8") (default-features #t) (kind 0)))) (hash "0cbxsfj9gifkv3cbw80dqwrd2md1gp8vairdvifsnr6ak8b947ly")))

(define-public crate-depth_analyzer-0.1 (crate (name "depth_analyzer") (vers "0.1.0") (deps (list (crate-dep (name "image") (req "^0.24.8") (default-features #t) (kind 0)))) (hash "1i577rdwqnyrraryqd0p58nv037pa6lzd085vwszqkc1rwybd0ma")))

(define-public crate-depth_analyzer-0.1 (crate (name "depth_analyzer") (vers "0.1.1") (deps (list (crate-dep (name "image") (req "^0.24.8") (default-features #t) (kind 0)))) (hash "0fpi57bl4k5k17l5qbkwlw4drhi0n9r35mhbfjw6mw2zs33yj5qc")))

(define-public crate-depth_analyzer-0.1 (crate (name "depth_analyzer") (vers "0.1.2") (deps (list (crate-dep (name "image") (req "^0.24.8") (default-features #t) (kind 0)))) (hash "185rfsrw0sb1vqa7hdwpma178a6kipa6hxjn2y3la3srhlhga60r")))

(define-public crate-depth_analyzer-0.1 (crate (name "depth_analyzer") (vers "0.1.3") (deps (list (crate-dep (name "image") (req "^0.24.8") (default-features #t) (kind 0)))) (hash "0xaf59rr6alvaflgkw11jix2jrrpfa0iapsssjmikkj8fyjf7nxi")))

(define-public crate-depth_analyzer-0.1 (crate (name "depth_analyzer") (vers "0.1.4") (deps (list (crate-dep (name "image") (req "^0.24.8") (default-features #t) (kind 0)))) (hash "0h8chd2s22msv9a4c5qb3cg1b2caipv33ma54fikny0gs2mszmz4")))

(define-public crate-depth_analyzer-0.1 (crate (name "depth_analyzer") (vers "0.1.5") (deps (list (crate-dep (name "image") (req "^0.24.8") (default-features #t) (kind 0)))) (hash "1nzafb5kwa82rqmscdmwxl2l6ldg786jfanphfb1v6zkziak6lcz")))

(define-public crate-depth_analyzer-0.1 (crate (name "depth_analyzer") (vers "0.1.6") (deps (list (crate-dep (name "image") (req "^0.24.8") (default-features #t) (kind 0)))) (hash "0cfz45ywhacmzwi0953wy96ip321azx1kq9idc67pf94d0w91z20")))

(define-public crate-depth_analyzer-0.1 (crate (name "depth_analyzer") (vers "0.1.7") (deps (list (crate-dep (name "image") (req "^0.24.8") (default-features #t) (kind 0)))) (hash "1wk767vh4hyl23q6ib50qn3z59x7n0xp256jgwgn45y9cxfd96ki")))

(define-public crate-depth_analyzer-0.2 (crate (name "depth_analyzer") (vers "0.2.0") (deps (list (crate-dep (name "image") (req "^0.24.8") (default-features #t) (kind 0)))) (hash "0691587yg7x5srxbzhps9pdvm7ki5zv3fkalzw5m0sfismkaz1bv")))

(define-public crate-deptypes-0.1 (crate (name "deptypes") (vers "0.1.0") (deps (list (crate-dep (name "generativity") (req "^1.0.0") (default-features #t) (kind 0)) (crate-dep (name "generics2") (req "^0.3.3") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2.14") (default-features #t) (kind 0)) (crate-dep (name "paste") (req "^1.0.6") (default-features #t) (kind 0)))) (hash "1w1n96ybvkfq9y8fd07whl9wh1adcv7fy4ga14v1qar1wj1zhnba") (features (quote (("std") ("nightly" "gat" "never") ("never") ("gat") ("default" "std")))) (yanked #t)))

(define-public crate-deptypes-0.1 (crate (name "deptypes") (vers "0.1.1") (deps (list (crate-dep (name "generativity") (req "^1.0.0") (default-features #t) (kind 0)) (crate-dep (name "generics2") (req "^0.3.3") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2.14") (default-features #t) (kind 0)) (crate-dep (name "paste") (req "^1.0.6") (default-features #t) (kind 0)))) (hash "18vysl7d5gsxacacyby53c3bwb7mi334xvw09ws7i7xjc44wjnfl") (features (quote (("std") ("nightly" "gat" "never") ("never") ("gat") ("default" "std"))))))

(define-public crate-deptypes-0.2 (crate (name "deptypes") (vers "0.2.0") (deps (list (crate-dep (name "derive-where") (req "^1.2.7") (default-features #t) (kind 0)) (crate-dep (name "generativity") (req "^1.0.0") (default-features #t) (kind 0)) (crate-dep (name "generics2") (req "^0.3.3") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2.14") (default-features #t) (kind 0)) (crate-dep (name "paste") (req "^1.0.6") (default-features #t) (kind 0)))) (hash "0r7v4cjngyw6kl31f74al7n0z0rlsbws3m8dbbnanwvbhxfl65pl") (features (quote (("trusted_len") ("std") ("nightly" "never" "trusted_len") ("never") ("default" "std")))) (rust-version "1.66")))

(define-public crate-deptypes-0.2 (crate (name "deptypes") (vers "0.2.1") (deps (list (crate-dep (name "derive-where") (req "^1.2.7") (default-features #t) (kind 0)) (crate-dep (name "generativity") (req "^1.0.0") (default-features #t) (kind 0)) (crate-dep (name "generics2") (req "^0.3.3") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2.14") (default-features #t) (kind 0)) (crate-dep (name "paste") (req "^1.0.6") (default-features #t) (kind 0)))) (hash "0ax0nq4kgwdm523znmp5c3kqpjiw2v1g2zlmjfx3vvpza1fi78bg") (features (quote (("trusted_len") ("std") ("nightly" "never" "trusted_len") ("never") ("default" "std")))) (rust-version "1.66")))

