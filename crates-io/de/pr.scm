(define-module (crates-io de pr) #:use-module (crates-io))

(define-public crate-deprecate-until-0.1 (crate (name "deprecate-until") (vers "0.1.0") (deps (list (crate-dep (name "proc-macro2") (req "^1.0.64") (kind 0)) (crate-dep (name "quote") (req "^1.0.29") (kind 0)) (crate-dep (name "semver") (req "^1.0.17") (kind 0)) (crate-dep (name "syn") (req "^2.0.26") (features (quote ("parsing" "proc-macro"))) (kind 0)) (crate-dep (name "trybuild") (req "^1.0.81") (default-features #t) (kind 2)))) (hash "12zmmwb9bb7lcki1a7vnlbs0pwhhy3n0j0z57c1gv6n7s2if5hky") (rust-version "1.60.0")))

(define-public crate-deprecate-until-0.1 (crate (name "deprecate-until") (vers "0.1.1") (deps (list (crate-dep (name "proc-macro2") (req "^1.0.70") (kind 0)) (crate-dep (name "quote") (req "^1.0.33") (kind 0)) (crate-dep (name "semver") (req "^1.0.20") (kind 0)) (crate-dep (name "syn") (req "^2.0.39") (features (quote ("parsing" "proc-macro"))) (kind 0)) (crate-dep (name "trybuild") (req "^1.0.81") (default-features #t) (kind 2)))) (hash "0177x1ifh5296phrgrib40s1gc23ifsj0f89msjybfzg4vw6fdvs") (rust-version "1.60.0")))

(define-public crate-deprive-0.1 (crate (name "deprive") (vers "0.1.0") (deps (list (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "1ab1vyqwhi3q92jcrkxffdkjq3xplbmaf79my3hyx0f3l4jnalhn")))

(define-public crate-deprive-0.2 (crate (name "deprive") (vers "0.2.0") (deps (list (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "1a3z8lybssjyihppcn5c653jzvp7gpssi83c8p15a7pp825kkshp")))

(define-public crate-deprive-0.2 (crate (name "deprive") (vers "0.2.1") (deps (list (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "05k6syda2af51wq139cks5zjnm88ids7bwz26xjf18n5nkpd36kp")))

