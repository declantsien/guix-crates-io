(define-module (crates-io de re) #:use-module (crates-io))

(define-public crate-deref-derive-0.1 (crate (name "deref-derive") (vers "0.1.0") (deps (list (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (default-features #t) (kind 0)))) (hash "0wrjw8gldn6fqag0c38bv6dv36fnigj55shapn402n9qsmyvnma2")))

(define-public crate-deref-map-0.1 (crate (name "deref-map") (vers "0.1.0") (hash "1ninzksccdp09sifv2dr66qrbjw4yfpj9j832xk8vqz1dyfz13xz")))

(define-public crate-deref_owned-0.1 (crate (name "deref_owned") (vers "0.1.0") (hash "1pl1ml2lyr4hv0a6974ndxpcqavjbx357v0xdb5lah1h0fvycbib")))

(define-public crate-deref_owned-0.2 (crate (name "deref_owned") (vers "0.2.0") (hash "01s8066g5p5av1ppw1l819ycn15ccrsifza88vq5qx31vxxqfbcy")))

(define-public crate-deref_owned-0.3 (crate (name "deref_owned") (vers "0.3.0") (hash "0617g2d3xwk3d4iwazd49v9mjs43fxwyyhmaxbrcaby99qk6zkqw")))

(define-public crate-deref_owned-0.4 (crate (name "deref_owned") (vers "0.4.0") (hash "07102y3z05yx1kdmhww7d1fipwpb7dcmphhvkk6nw553lk7bzq26")))

(define-public crate-deref_owned-0.5 (crate (name "deref_owned") (vers "0.5.0") (hash "1mkplbp2v92zqdn1mayhzzc7gxlmxsxnncg76jkmchq78cmh3ylx") (yanked #t)))

(define-public crate-deref_owned-0.6 (crate (name "deref_owned") (vers "0.6.0") (hash "1kqqhslpqg0cwvyw7321s79prka2w0k2y0kqnmc2n0jma80vk1rd") (yanked #t)))

(define-public crate-deref_owned-0.6 (crate (name "deref_owned") (vers "0.6.1") (hash "1rp4y4dfw2a174whi683akzmlykymmkcmxyzv8vbh1b7dig9iii4")))

(define-public crate-deref_owned-0.7 (crate (name "deref_owned") (vers "0.7.0") (hash "1hcv5i71c7smym6m0zd8896p5hgfhcfzgslg4s91p54qc8jbyhf1")))

(define-public crate-deref_owned-0.8 (crate (name "deref_owned") (vers "0.8.0") (hash "181j6pqzlrmmcn5gzi3dzy7saawz22vxyxjw87r9j9ckic2n99ar")))

(define-public crate-deref_owned-0.8 (crate (name "deref_owned") (vers "0.8.1") (hash "059c92w00a9i478hxvfmw4qbllna9myyy1sq1y2bv5xz6gbv6dkp")))

(define-public crate-deref_owned-0.9 (crate (name "deref_owned") (vers "0.9.0") (hash "0n04gm3gzwng7b46nwj5hb4my6vykzxfmnfq38gjwz1h8gdr2xx6") (yanked #t)))

(define-public crate-deref_owned-0.8 (crate (name "deref_owned") (vers "0.8.2") (hash "02ylncmalpkljcslg01r6q4ydmmrq7xi6qpnhf6839fivl7jnl59")))

(define-public crate-deref_owned-0.10 (crate (name "deref_owned") (vers "0.10.0") (hash "1gf6gyha2vjj3gaja4v6f6bnr2lji99kz18cjjfw3b8iaggqqajz")))

(define-public crate-deref_owned-0.10 (crate (name "deref_owned") (vers "0.10.1") (hash "12b0w1686w9j1m1dk8a68x6sqknd8bpzgid51gkg7lkc7in94485")))

(define-public crate-deref_owned-0.10 (crate (name "deref_owned") (vers "0.10.2") (hash "1n731384iirfnzj78xkh0ns61k59h51my02xh394a31m4rvlhpga")))

(define-public crate-derefable-0.1 (crate (name "derefable") (vers "0.1.0") (deps (list (crate-dep (name "proc-macro2") (req "^0.4.26") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^0.6") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^0.15") (default-features #t) (kind 0)))) (hash "1ryb8k7dj4v41rx62qj9qnghg2wli679b0f9f4q7cxch53qsn6g5")))

(define-public crate-deregex-0.1 (crate (name "deregex") (vers "0.1.0") (hash "0c11q50g4qih1pc0z1hlm72m5x747kv27wg4z217yn90sb4wgvbz")))

(define-public crate-dereversa-0.0.1 (crate (name "dereversa") (vers "0.0.1") (hash "0n2cvdng10w531acmh2kqf3py58dkcb5zh04985a95dc0ggz5j17")))

(define-public crate-derezzed-0.0.1 (crate (name "derezzed") (vers "0.0.1-alpha.0") (deps (list (crate-dep (name "ndarray") (req "^0.15.6") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)))) (hash "1hk21f5mvx3dzmgc0sn9y9r84fkrz9i8fr68aqnnmjjai4lld0lb")))

