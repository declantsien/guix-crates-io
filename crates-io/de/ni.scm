(define-module (crates-io de ni) #:use-module (crates-io))

(define-public crate-denim-0.1 (crate (name "denim") (vers "0.1.0") (deps (list (crate-dep (name "cotton") (req "^0.0.5") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.3.3") (default-features #t) (kind 0)))) (hash "1vrwjprhvsc48nqaaic40jwwb0nmzabfh82v0y33mbk713mi1jak")))

(define-public crate-denim-0.1 (crate (name "denim") (vers "0.1.1") (deps (list (crate-dep (name "cotton") (req "^0.0.5") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.41") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.3.3") (default-features #t) (kind 0)))) (hash "06qf3pxjsaz8b647idyd15zwq6a80g11zi7ck6rxdgpbg6pl0xrx")))

(define-public crate-denim-0.1 (crate (name "denim") (vers "0.1.3") (deps (list (crate-dep (name "cotton") (req "^0.0.7") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.41") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.3.3") (default-features #t) (kind 0)))) (hash "0lz0z011s8c69kbmvflahfdd3vjgmgq1gjix2bm4323xbdd2n6jd")))

(define-public crate-denim-0.1 (crate (name "denim") (vers "0.1.4") (deps (list (crate-dep (name "cotton") (req "^0.0.13") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.41") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.3.3") (default-features #t) (kind 0)))) (hash "0h8jf827b3bxqabyr2ihkkiv9frjdbliih4xspy8330ccki0rj1k")))

(define-public crate-denim-0.1 (crate (name "denim") (vers "0.1.5") (deps (list (crate-dep (name "cotton") (req "^0.0.13") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.41") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.3.3") (default-features #t) (kind 0)))) (hash "0g40d120wich78y4kizdq2pmhjgaif4wa6in2c6jckvwjwz5r6kh")))

(define-public crate-denim-0.2 (crate (name "denim") (vers "0.2.0") (deps (list (crate-dep (name "cotton") (req "^0.1.0") (features (quote ("errors" "args" "logging" "app" "hashing" "process"))) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.89") (default-features #t) (kind 0)))) (hash "1vq83nm8bzg8qrij20nqrz0i7wc3yjn65m00g1jk1ljhwxrmz4lf")))

(define-public crate-denis-0.1 (crate (name "denis") (vers "0.1.0") (hash "08jivyzdjzbgmqd245r9snfrbakm1lgx9g6a69a26dg7cnhmxbbk")))

(define-public crate-denislessard-0.1 (crate (name "denislessard") (vers "0.1.0") (hash "03a0nji2l4lal7vj8nikdkwnbgsrkqrhhiv5mw9vfd2bb85m7biv")))

