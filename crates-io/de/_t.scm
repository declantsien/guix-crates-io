(define-module (crates-io de _t) #:use-module (crates-io))

(define-public crate-de_terrain-0.0.1 (crate (name "de_terrain") (vers "0.0.1") (hash "0cn16jr2i47jr8hbz54qap4pjq327v30lydjkcvqnv495vgm4x07")))

(define-public crate-de_tools-0.0.1 (crate (name "de_tools") (vers "0.0.1") (hash "0s6l110mi5zj00whaymh58vsjb8lp0jsn7i0g0aghmfxz68khqqp")))

