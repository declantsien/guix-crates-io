(define-module (crates-io de ed) #:use-module (crates-io))

(define-public crate-deedoo-0.1 (crate (name "deedoo") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^2.32.0") (default-features #t) (kind 0)) (crate-dep (name "crc") (req "^1.8") (default-features #t) (kind 0)) (crate-dep (name "walkdir") (req "^2.2") (default-features #t) (kind 0)))) (hash "0pjr87dgkw1wnhg6i7dw1sdfp3dg0983pfv1gl7rg197fpzznzmb")))

(define-public crate-deedoo-0.1 (crate (name "deedoo") (vers "0.1.2") (deps (list (crate-dep (name "clap") (req "^2.32.0") (default-features #t) (kind 0)) (crate-dep (name "crc") (req "^1.8") (default-features #t) (kind 0)) (crate-dep (name "walkdir") (req "^2.2") (default-features #t) (kind 0)))) (hash "1679p9qgnyg68dy7wdcnxpd62f36vz7mjy3prkhmhqiag6zfgywf")))

(define-public crate-deedoo-0.1 (crate (name "deedoo") (vers "0.1.3") (deps (list (crate-dep (name "clap") (req "^2.32.0") (default-features #t) (kind 0)) (crate-dep (name "crc") (req "^1.8") (default-features #t) (kind 0)) (crate-dep (name "walkdir") (req "^2.2") (default-features #t) (kind 0)))) (hash "0p80q4mv8d8pbsqrwhk1r3cy4wk4qdk17cl1ya2rgjysl631fy3h")))

