(define-module (crates-io de lf) #:use-module (crates-io))

(define-public crate-delfast-0.1 (crate (name "delfast") (vers "0.1.5") (hash "1b8aqvi1c69nw4ky42kg04c4jg9lrd20jha7c5ba3l4arkffk0d3")))

(define-public crate-delfast-0.1 (crate (name "delfast") (vers "0.1.6") (hash "0npvq59jzjd6wfbl6vcp4zl9dfxpajvdwhc77bd1s4p426ppf5k2")))

(define-public crate-delfast-0.1 (crate (name "delfast") (vers "0.1.7") (hash "0fywwiqi3cry6b80xi2dddg972qdvba6y1khdixd8bab4z4bi3qq")))

(define-public crate-delfast-0.1 (crate (name "delfast") (vers "0.1.8") (deps (list (crate-dep (name "clap") (req "^4.1.4") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "19hb68bvyzp71kc162s980mgacdyri6fz2d95wpy4rhdfxnyaz99")))

(define-public crate-delfast-0.1 (crate (name "delfast") (vers "0.1.9") (deps (list (crate-dep (name "clap") (req "^4.1.4") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "13fyp6gf9kviql3jpbwnn5xzv64a1qsip7m5frd0fisrgchybaq2")))

(define-public crate-delfi-0.0.1 (crate (name "delfi") (vers "0.0.1") (deps (list (crate-dep (name "csv") (req "^1.1") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.17") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "17wdqsci7ph8afm1np0k7lmp5bzdmd6p7bq2azhzmi5iqhfx7hl1")))

(define-public crate-delfi-0.0.2 (crate (name "delfi") (vers "0.0.2") (deps (list (crate-dep (name "csv") (req "^1.1") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "09wibnf46p64jvikgpn430gdgiwcp5h0k493jgk1jfrn3xa5v22b")))

(define-public crate-delfi-0.0.3 (crate (name "delfi") (vers "0.0.3") (deps (list (crate-dep (name "csv") (req "^1.1") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "0i0l5fq1a8zpv9c4nb3d0fxzn19l6wiis9a21gqi4m6s1f3yxvif")))

(define-public crate-delfi-0.0.4 (crate (name "delfi") (vers "0.0.4") (deps (list (crate-dep (name "csv") (req "^1.1") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "00da26zwb1dglh6sm6xl9f8nnp3yxy3vlmqyb268b5ldbbrcyjc8")))

(define-public crate-delfi-0.0.5 (crate (name "delfi") (vers "0.0.5") (deps (list (crate-dep (name "csv") (req "^1.1") (default-features #t) (kind 0)) (crate-dep (name "ndarray") (req "^0.15") (default-features #t) (kind 2)))) (hash "1c706bqy3xlcbz4f7chgwg65li2vk0dhms2q6m3igd0ibmlrfkzp")))

(define-public crate-delfi-0.1 (crate (name "delfi") (vers "0.1.0") (deps (list (crate-dep (name "csv") (req "^1.1") (default-features #t) (kind 0)) (crate-dep (name "delfi-macros") (req "^0.1.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "ndarray") (req "^0.15") (default-features #t) (kind 2)))) (hash "1xxzj397bi5r0pix1vznv2cr1l6wdyzyn7q33czys8qi3jr804vz") (features (quote (("macros" "delfi-macros"))))))

(define-public crate-delfi-macros-0.1 (crate (name "delfi-macros") (vers "0.1.0") (deps (list (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (features (quote ("extra-traits"))) (default-features #t) (kind 0)))) (hash "0jfsw33xz3xx19361yfpv0gwqvyrancs2dvhddk6z9xz178r1vg9")))

