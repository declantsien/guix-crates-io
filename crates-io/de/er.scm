(define-module (crates-io de er) #:use-module (crates-io))

(define-public crate-deer-0.0.0 (crate (name "deer") (vers "0.0.0-reserved") (hash "1g1p7b1lli1n23kjywbxk39m07d3jmhycszbjxl6ac0p9vpiknz6") (rust-version "1.65")))

(define-public crate-deer-json-0.0.0 (crate (name "deer-json") (vers "0.0.0-reserved") (hash "1p4s5vhrhywby4l7v0jxmn68xv5bzz95riinj9h1mnbgcmx6l2cl") (rust-version "1.65")))

(define-public crate-deer-macros-0.0.0 (crate (name "deer-macros") (vers "0.0.0-reserved") (hash "18kxqz1y0fsi9vnk5z63k04wd6f3swxnk56d8d7dgyxz2ifwjhy6") (rust-version "1.65")))

