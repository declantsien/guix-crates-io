(define-module (crates-io de sd) #:use-module (crates-io))

(define-public crate-desdemona-0.1 (crate (name "desdemona") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^2") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0") (default-features #t) (kind 0)) (crate-dep (name "rayon") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "text_io") (req "^0") (default-features #t) (kind 0)))) (hash "1n591n11cr4na9nqfmd7lpfy064kcxnm10gghbi5zfch0vhlljy1")))

(define-public crate-desdemona-0.1 (crate (name "desdemona") (vers "0.1.1") (deps (list (crate-dep (name "clap") (req "^2") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0") (default-features #t) (kind 0)) (crate-dep (name "rayon") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "text_io") (req "^0") (default-features #t) (kind 0)))) (hash "1g30zc6rmfj9f23pjwh3kbazjm2xlzs6fc7kj0rr9jmrvmz90ixp")))

(define-public crate-desdemona-0.2 (crate (name "desdemona") (vers "0.2.0") (deps (list (crate-dep (name "clap") (req "^2") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0") (default-features #t) (kind 0)) (crate-dep (name "rayon") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "text_io") (req "^0") (default-features #t) (kind 0)))) (hash "1ksvn6fk9wdxl44fdjyzi2lqkngz0vh5kdlvgzsmms44rj9qli2p")))

(define-public crate-desdemona-0.2 (crate (name "desdemona") (vers "0.2.1") (deps (list (crate-dep (name "clap") (req "^2") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0") (default-features #t) (kind 0)) (crate-dep (name "rayon") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "text_io") (req "^0") (default-features #t) (kind 0)))) (hash "0frlz0l5fddzrmn0m7d658i2vwgyq61f587qykz8zqkgaf447znj")))

(define-public crate-desdemona-0.3 (crate (name "desdemona") (vers "0.3.0") (deps (list (crate-dep (name "clap") (req "^2") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0") (default-features #t) (kind 0)) (crate-dep (name "rayon") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "text_io") (req "^0") (default-features #t) (kind 0)))) (hash "0fn2awjr57ql29q7knxzz9wvqmhzp96385fgvvk48g8nlxy1ghk2")))

