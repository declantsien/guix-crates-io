(define-module (crates-io de nn) #:use-module (crates-io))

(define-public crate-dennybiasiolli_guessing_game-0.1 (crate (name "dennybiasiolli_guessing_game") (vers "0.1.0") (deps (list (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)))) (hash "03yijg802yzgad00la93adrvapkhk203js04qg9xvfhcmmydwr4p")))

(define-public crate-dennybiasiolli_guessing_game-0.1 (crate (name "dennybiasiolli_guessing_game") (vers "0.1.1") (deps (list (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)))) (hash "1rm134gyh9mbkn4kg43x37p59w71xayv3ig7y1zpxmqpbpq00xbb")))

