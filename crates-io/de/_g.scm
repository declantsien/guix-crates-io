(define-module (crates-io de _g) #:use-module (crates-io))

(define-public crate-de_game-0.0.1 (crate (name "de_game") (vers "0.0.1") (hash "1iiirg69bwf0cr6qg2vfxi0ds37bb806ywcxw3dhlr38bpb94lrj")))

(define-public crate-de_generics-0.1 (crate (name "de_generics") (vers "0.1.0") (deps (list (crate-dep (name "proc-macro2") (req "^1.0.70") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.33") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2.0.39") (default-features #t) (kind 0)))) (hash "02nvf7lxa9dpis2ac03qg0x0mk37rvqmrmyhwdgd4zi83m2vn53q")))

