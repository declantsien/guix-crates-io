(define-module (crates-io de fr) #:use-module (crates-io))

(define-public crate-defr-0.1 (crate (name "defr") (vers "0.1.0") (deps (list (crate-dep (name "proc-macro2") (req "^1.0.47") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.21") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.103") (features (quote ("extra-traits" "full"))) (default-features #t) (kind 0)))) (hash "10dmbidl59r2f2a2a9a3afjfakq0yxsphzhx9884dwf04mkgvd9q")))

(define-public crate-defrag-0.1 (crate (name "defrag") (vers "0.1.0") (deps (list (crate-dep (name "rand") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "stopwatch") (req "^0.0.7") (default-features #t) (kind 2)))) (hash "1nhp27x044lfjhvckmfxcd53kg9pcqyv5mq7q6xffc6a2s8hy19d")))

(define-public crate-defrag-0.1 (crate (name "defrag") (vers "0.1.1") (deps (list (crate-dep (name "rand") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "stopwatch") (req "^0.0.7") (default-features #t) (kind 2)))) (hash "0h7siw5didqczjg0m1fyiz6b4v3j2ppkbffi98laqijq85s71fpq")))

(define-public crate-defrag-0.1 (crate (name "defrag") (vers "0.1.2") (deps (list (crate-dep (name "cbuf") (req "0.1.*") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "stopwatch") (req "^0.0.7") (default-features #t) (kind 2)))) (hash "0l17lvzbsis3w9jql4j663ir8k1nlipqcbx160sr0wscv28mc4iw") (features (quote (("no_std") ("default"))))))

(define-public crate-defrag-0.1 (crate (name "defrag") (vers "0.1.3") (deps (list (crate-dep (name "cbuf") (req "0.1.*") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "stopwatch") (req "^0.0.7") (default-features #t) (kind 2)))) (hash "1839kghr54vjqcafyy45qakh41prnr1mhd7ilfxaa5674ixb5gia") (features (quote (("no_std") ("default"))))))

(define-public crate-defrag-0.1 (crate (name "defrag") (vers "0.1.4") (deps (list (crate-dep (name "cbuf") (req "0.1.*") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "stopwatch") (req "^0.0.7") (default-features #t) (kind 2)))) (hash "15ypxndigqg05h0ld9r7sqml7gsrh0208aifw5z7bp4671a89rcc") (features (quote (("no_std") ("default"))))))

(define-public crate-defrag-dirs-0.1 (crate (name "defrag-dirs") (vers "0.1.0") (deps (list (crate-dep (name "btrfs") (req "^1.2.1") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^2.20") (default-features #t) (kind 0)) (crate-dep (name "derive-error") (req "^0.0.3") (default-features #t) (kind 0)) (crate-dep (name "nix") (req "^0.8.1") (default-features #t) (kind 0)) (crate-dep (name "time") (req "^0.1.37") (default-features #t) (kind 0)) (crate-dep (name "walkdir") (req "^1.0.7") (default-features #t) (kind 0)))) (hash "0qfmwwsiys3bhgcdswqgiq8ki4iz4pk9f91y873hmiknb7yxprqs")))

(define-public crate-defrag-dirs-0.1 (crate (name "defrag-dirs") (vers "0.1.1") (deps (list (crate-dep (name "btrfs2") (req "^1.2.2") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^2.20") (default-features #t) (kind 0)) (crate-dep (name "derive-error") (req "^0.0.3") (default-features #t) (kind 0)) (crate-dep (name "nix") (req "^0.8.1") (default-features #t) (kind 0)) (crate-dep (name "time") (req "^0.1.37") (default-features #t) (kind 0)) (crate-dep (name "walkdir") (req "^1.0.7") (default-features #t) (kind 0)))) (hash "133vzhfhg150c6xrdvsrzkhdx6vh2w897lxx88dbj1pms3x9da0l")))

(define-public crate-deframe-0.0.1 (crate (name "deframe") (vers "0.0.1") (hash "0ijskry6vbp3vrbi6mk31hv9wyyzvq0w6l3z7byffqpn5r5zwgi1") (features (quote (("default"))))))

