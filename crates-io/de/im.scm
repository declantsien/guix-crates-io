(define-module (crates-io de im) #:use-module (crates-io))

(define-public crate-deimos-0.1 (crate (name "deimos") (vers "0.1.0") (hash "1plas89pg54qrfp724fp2g02ryf8ipag67mj1azpa9l82dlld6lj") (yanked #t)))

(define-public crate-deimos-0.1 (crate (name "deimos") (vers "0.1.1") (hash "10x59c4iq0rv4pl6bc7m88rqhz6mrin4xhybsfcy6zx121x8xab0") (yanked #t)))

(define-public crate-deimos-0.2 (crate (name "deimos") (vers "0.2.0") (hash "1p73m48m3sbkqj5iz757n40y9yn6b2dczc0xypibi8p1rw9fcvib")))

