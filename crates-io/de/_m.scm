(define-module (crates-io de _m) #:use-module (crates-io))

(define-public crate-de_map-0.0.1 (crate (name "de_map") (vers "0.0.1") (hash "005ijjdxv5s1xayiz2qqnrx0s18m3jsk0ixnl3mjgw9g2hrsrdgv")))

(define-public crate-de_movement-0.0.1 (crate (name "de_movement") (vers "0.0.1") (hash "13dfpvigd71rc18b8fssk6z7f6sj5cwvvmn1xwqr5kyaizanq342")))

