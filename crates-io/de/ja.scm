(define-module (crates-io de ja) #:use-module (crates-io))

(define-public crate-dejavu-2 (crate (name "dejavu") (vers "2.37.0") (hash "1hgv2cc9gnk8q8j2zl4hk2vf2jhf9mfcqsm31m4nv065zfspayq1")))

(define-public crate-dejavu-engine-0.1 (crate (name "dejavu-engine") (vers "0.1.0") (deps (list (crate-dep (name "anyhow") (req "^1.0.66") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (optional #t) (default-features #t) (kind 0)))) (hash "02nbs0r6si1n96bcamgi7mqn6a7x8m897sq5bb2i2drqq0zdinfb") (features (quote (("json" "serde_json" "serde_json/preserve_order") ("default"))))))

(define-public crate-dejavu-ir-0.0.0 (crate (name "dejavu-ir") (vers "0.0.0") (hash "1j0x3x7cb97fxrywz2rmps7yac034ic42ddikahwvsa2vy2zyvm8") (features (quote (("default"))))))

(define-public crate-dejavu-parser-0.0.0 (crate (name "dejavu-parser") (vers "0.0.0") (deps (list (crate-dep (name "peginator") (req "^0.4.0") (default-features #t) (kind 0)) (crate-dep (name "peginator") (req "^0.4.0") (default-features #t) (kind 1)) (crate-dep (name "ucd-trie") (req "^0.1.5") (default-features #t) (kind 0)) (crate-dep (name "unicode-ident") (req "^1.0.5") (default-features #t) (kind 0)))) (hash "0nqycd8vjvbjm09bm83qwv19z3jyqgjnv30fjd2s3159yw8fy5d6") (features (quote (("default"))))))

(define-public crate-dejavu-parser-0.1 (crate (name "dejavu-parser") (vers "0.1.0") (deps (list (crate-dep (name "peginator") (req "^0.6.0") (default-features #t) (kind 0)) (crate-dep (name "peginator_codegen") (req "^0.6.0") (default-features #t) (kind 1)) (crate-dep (name "ucd-trie") (req "^0.1.5") (default-features #t) (kind 0)) (crate-dep (name "unicode-ident") (req "^1.0.5") (default-features #t) (kind 0)))) (hash "1z27gkcg0bann082s504k4a61hdc0rynx5cvz8rsi5nzhasc5vb9") (features (quote (("default"))))))

(define-public crate-dejavu-parser-0.1 (crate (name "dejavu-parser") (vers "0.1.1") (deps (list (crate-dep (name "yggdrasil-rt") (req "^0.0.11") (default-features #t) (kind 0)) (crate-dep (name "yggdrasil-shared") (req "0.2.*") (default-features #t) (kind 2)))) (hash "0y3062jf9ls6glm1izqb7h2ghkd23wklm4a41jzmbnfi76hgdp93") (features (quote (("default"))))))

(define-public crate-dejavu-runtime-0.1 (crate (name "dejavu-runtime") (vers "0.1.0") (deps (list (crate-dep (name "anyhow") (req "^1.0.66") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.89") (optional #t) (default-features #t) (kind 0)))) (hash "1xjwi78zb9vcrj5c8yiw3pyz7v81ygkd4qv9lzamyrid4glrr675") (features (quote (("json" "serde_json" "serde_json/preserve_order") ("default"))))))

(define-public crate-dejavu-runtime-0.1 (crate (name "dejavu-runtime") (vers "0.1.1") (hash "00nv4b1rqa6lpsmh7r9qxkm6sdwfl6dhs73xffgq9612vw6h5qww") (features (quote (("default"))))))

