(define-module (crates-io de we) #:use-module (crates-io))

(define-public crate-dewey-0.1 (crate (name "dewey") (vers "0.1.0") (deps (list (crate-dep (name "static_map") (req "^0.1.1") (default-features #t) (kind 0)) (crate-dep (name "static_map_macros") (req "^0.1.1") (default-features #t) (kind 0)))) (hash "0jcj0mnl28ha8iyd7qf3g45gv9a6frm7z45dwnh49a1mlvxl7qj8")))

(define-public crate-dewey-0.1 (crate (name "dewey") (vers "0.1.1") (deps (list (crate-dep (name "static_map") (req "^0.1.1") (default-features #t) (kind 0)) (crate-dep (name "static_map_macros") (req "^0.1.1") (default-features #t) (kind 0)))) (hash "14vwn636g3vm0b7xgnw78l7wavqbqqj411lbxd382w0iawkl3s05")))

(define-public crate-dewey-0.1 (crate (name "dewey") (vers "0.1.2") (deps (list (crate-dep (name "static_map") (req "^0.1.1") (default-features #t) (kind 0)) (crate-dep (name "static_map_macros") (req "^0.1.1") (default-features #t) (kind 0)))) (hash "1ss924186chc5ibyg7f3zhdmahz0cnri03zpn8xlccx2pr0lrvbp")))

(define-public crate-dewey-0.2 (crate (name "dewey") (vers "0.2.0") (hash "14b5fzybgp0sx2gbc9zn68w0h2sq5xgf179gpw7aav3c8gmcl88l")))

(define-public crate-dewey-0.3 (crate (name "dewey") (vers "0.3.0") (hash "1vh9w4h7ai7xib5655ilajr0sk6rllrwwx5m8168hfm0r6djh906")))

