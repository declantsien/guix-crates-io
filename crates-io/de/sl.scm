(define-module (crates-io de sl) #:use-module (crates-io))

(define-public crate-deslite-0.1 (crate (name "deslite") (vers "0.1.0") (deps (list (crate-dep (name "libsqlite3-sys") (req "^0.9.3") (default-features #t) (kind 0)))) (hash "1yn3pys2dvsrag54z16q5lq650jd227xn247v9acl3ci05l58vn5")))

(define-public crate-deslite-0.1 (crate (name "deslite") (vers "0.1.1") (deps (list (crate-dep (name "libsqlite3-sys") (req "^0.13.0") (default-features #t) (kind 0)))) (hash "1662snfdv59srl9186c0ip5ybiw7x9vb1pir9hxh91bh9m4s38nd")))

