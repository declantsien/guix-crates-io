(define-module (crates-io de -o) #:use-module (crates-io))

(define-public crate-de-obfuscator-0.1 (crate (name "de-obfuscator") (vers "0.1.0") (deps (list (crate-dep (name "base85") (req "^1.1.1") (default-features #t) (kind 0)))) (hash "0cfhpgcyabd43kkdxnjz9dj4prp23cvqbchyd9686ijv1z087s6n")))

