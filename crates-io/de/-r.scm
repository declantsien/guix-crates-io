(define-module (crates-io de -r) #:use-module (crates-io))

(define-public crate-de-ref-1 (crate (name "de-ref") (vers "1.0.0") (deps (list (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (default-features #t) (kind 0)))) (hash "09pnalgvq4nb61fqbwckzk1v82ai1589qfcalhwym9g6rdwcgy4g")))

(define-public crate-de-regex-0.1 (crate (name "de-regex") (vers "0.1.0") (deps (list (crate-dep (name "regex") (req "^1.4") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "1fsf054m4vdg28m7dc2c88vwcwb854n3a8clm89i2yh4n9vmr5v7")))

