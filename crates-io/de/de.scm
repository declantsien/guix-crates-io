(define-module (crates-io de de) #:use-module (crates-io))

(define-public crate-dede-0.1 (crate (name "dede") (vers "0.1.0") (deps (list (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "1fasfp62bw9vkpch7wkq3rza591l9wm618j2n9airlfllnnf42gb")))

(define-public crate-dede-0.1 (crate (name "dede") (vers "0.1.1") (deps (list (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "18dd7nsqjqy9hw1j5crlasdf5vf07lkp8ld0bwwakhazc89an5i6")))

(define-public crate-dedenne-0.1 (crate (name "dedenne") (vers "0.1.0") (deps (list (crate-dep (name "derive-debug") (req "^0.1.2") (default-features #t) (kind 0)))) (hash "17qg1k4p86wixjd5vl2mwfzpbs75lss5h51mjzia0cb0b4hzfngp")))

(define-public crate-dedep-0.0.1 (crate (name "dedep") (vers "0.0.1") (hash "0yw5p4ci6h05mffrfcarvkyrps9khwx39fwxhmlws8lvqjiib925")))

