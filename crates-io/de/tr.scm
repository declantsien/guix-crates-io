(define-module (crates-io de tr) #:use-module (crates-io))

(define-public crate-detrim-0.0.0 (crate (name "detrim") (vers "0.0.0") (hash "01cpk7j0fndnpff05j5vdmrrrhdij0jnvyw7fnhiad8j70cv23bq") (yanked #t)))

(define-public crate-detrim-0.1 (crate (name "detrim") (vers "0.1.0") (deps (list (crate-dep (name "serde") (req "^1") (features (quote ("alloc"))) (kind 0)) (crate-dep (name "serde") (req "^1") (features (quote ("std" "derive"))) (default-features #t) (kind 2)) (crate-dep (name "serde_json") (req "^1") (default-features #t) (kind 2)))) (hash "070lxs4sfcqy9dja21s855m1cq5k2zlk446c74cp2rrni60ndhp0") (yanked #t) (rust-version "1.56.1")))

(define-public crate-detrim-0.1 (crate (name "detrim") (vers "0.1.1") (deps (list (crate-dep (name "serde") (req "^1") (features (quote ("alloc"))) (kind 0)) (crate-dep (name "serde") (req "^1") (features (quote ("std" "derive"))) (default-features #t) (kind 2)) (crate-dep (name "serde_json") (req "^1") (default-features #t) (kind 2)))) (hash "0p4wg124i03nfr3rvi2q6v5wifcywncd2vd9hdg6ijrz2bgi293d") (yanked #t) (rust-version "1.56.1")))

(define-public crate-detrim-0.1 (crate (name "detrim") (vers "0.1.2") (deps (list (crate-dep (name "serde") (req "^1") (features (quote ("alloc"))) (kind 0)) (crate-dep (name "serde") (req "^1") (features (quote ("std" "derive"))) (default-features #t) (kind 2)) (crate-dep (name "serde_json") (req "^1") (default-features #t) (kind 2)))) (hash "1nhq8kn9jlm9ad03jikh6cj9lknisss0l3a4763ag2jwpcxq9653") (features (quote (("std") ("default" "std")))) (rust-version "1.56.1")))

(define-public crate-detrim-0.1 (crate (name "detrim") (vers "0.1.3") (deps (list (crate-dep (name "serde") (req "^1") (features (quote ("alloc"))) (kind 0)) (crate-dep (name "serde") (req "^1") (features (quote ("std" "derive"))) (default-features #t) (kind 2)) (crate-dep (name "serde_json") (req "^1") (default-features #t) (kind 2)))) (hash "1lj31rk0pidgsp641d7r9gxva1h150s5sw8lvdyjfwr94v27vfzd") (features (quote (("std") ("default" "std")))) (rust-version "1.56.1")))

(define-public crate-detrojt-0.1 (crate (name "detrojt") (vers "0.1.0") (deps (list (crate-dep (name "serde") (req "^1.0.8") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.2") (default-features #t) (kind 0)))) (hash "0jnkqiwiny8az70pl3cmzgzfxllasl8mmxmrffj91rx919zx3dbh")))

(define-public crate-detrojt-0.1 (crate (name "detrojt") (vers "0.1.1") (deps (list (crate-dep (name "serde") (req "^1.0.8") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.2") (default-features #t) (kind 0)))) (hash "1m9h6zv0nwy6x0psyxhq6k3s6frfr8yzfa1mdr1asj3gi9ywd00c")))

