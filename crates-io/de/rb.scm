(define-module (crates-io de rb) #:use-module (crates-io))

(define-public crate-derbyjson-0.0.1 (crate (name "derbyjson") (vers "0.0.1") (deps (list (crate-dep (name "serde") (req "^0.9") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^0.9") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^0.9.3") (default-features #t) (kind 0)))) (hash "153hmq7iknvvx1sm0b4lzcgs15cqxqdji7cwhn1sqch9v307hyrh")))

(define-public crate-derbyjson-0.0.2 (crate (name "derbyjson") (vers "0.0.2") (deps (list (crate-dep (name "serde") (req "^0.9") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^0.9") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^0.9.3") (default-features #t) (kind 0)))) (hash "06qd468n95n0vchqgrp2nkj9bkn2806sjqqzv9kbdih8vz40wlk0")))

(define-public crate-derbyjson-0.0.3 (crate (name "derbyjson") (vers "0.0.3") (deps (list (crate-dep (name "serde") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)))) (hash "1hq48jzl7ghl4nqrvpnk3bsp2wnrad5hw7jpib61a9jvavr1m37r")))

