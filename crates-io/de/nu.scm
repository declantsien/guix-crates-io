(define-module (crates-io de nu) #:use-module (crates-io))

(define-public crate-denumic-0.1 (crate (name "denumic") (vers "0.1.0") (deps (list (crate-dep (name "proc-macro2") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "02wl4qrliql2zdcrz1pfr8yavpggfv3m89vsk7d4kvv8n99cscwl")))

(define-public crate-denuo-0.1 (crate (name "denuo") (vers "0.1.0") (hash "0si7vg9did0wl4ijryxrx792nxl3wkb4fkcqrbwcravp3zq2my7s")))

