(define-module (crates-io de pg) #:use-module (crates-io))

(define-public crate-depgraph-0.1 (crate (name "depgraph") (vers "0.1.0") (deps (list (crate-dep (name "petgraph") (req "^0.4") (default-features #t) (kind 0)))) (hash "1cwry7avwk031nzb40fwr42gz8wlrjkzhniqldl2q6p2d28ywghw") (features (quote (("petgraph_visible") ("default"))))))

(define-public crate-depgraph-0.2 (crate (name "depgraph") (vers "0.2.0") (deps (list (crate-dep (name "petgraph") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "tempdir") (req "^0.3.5") (default-features #t) (kind 2)))) (hash "1xkvy478z1j5bfh6rl13crwdsbq818ngw7k1i3ngmdjcdm8smxsy") (features (quote (("petgraph_visible") ("default"))))))

(define-public crate-depgraph-0.2 (crate (name "depgraph") (vers "0.2.1") (deps (list (crate-dep (name "petgraph") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "tempdir") (req "^0.3.5") (default-features #t) (kind 2)))) (hash "0q9v2nrdxd0m4mrzicygvdsm9inljrc1531xk5pflm8ycp7mplyr") (features (quote (("petgraph_visible") ("default"))))))

(define-public crate-depgraph-0.2 (crate (name "depgraph") (vers "0.2.2") (deps (list (crate-dep (name "petgraph") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "tempdir") (req "^0.3.5") (default-features #t) (kind 2)))) (hash "06f4nyj6rh57a878aib5a75866vkwdfhffxfa1g3g6ng929zsc7r") (features (quote (("petgraph_visible") ("default"))))))

(define-public crate-depgraph-0.3 (crate (name "depgraph") (vers "0.3.0") (deps (list (crate-dep (name "petgraph") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "tempdir") (req "^0.3.5") (default-features #t) (kind 2)))) (hash "0vnaw0vi23bgq32si0n28ayz1f7bwpf2x2ysi79mhli5f8xl4rkd") (features (quote (("petgraph_visible") ("default"))))))

