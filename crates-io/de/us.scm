(define-module (crates-io de us) #:use-module (crates-io))

(define-public crate-deus-0.1 (crate (name "deus") (vers "0.1.0") (deps (list (crate-dep (name "ndarray") (req "^0.15.4") (default-features #t) (kind 2)) (crate-dep (name "pretty_assertions") (req "^0.3") (default-features #t) (kind 2)))) (hash "130qqkzkfqzbbg80cdvm13n7f5cfrjb2bxij1fx0fsyjm4m4f1iz")))

(define-public crate-deus-maze-0.0.0 (crate (name "deus-maze") (vers "0.0.0") (deps (list (crate-dep (name "maze-core") (req "0.0.*") (default-features #t) (kind 0)) (crate-dep (name "maze-image") (req "0.0.*") (default-features #t) (kind 0)))) (hash "024gi89yhz9rdsaqgpcc20lrdxd84gd7pmlrp0rka2y00yw36dk5") (features (quote (("mota" "maze-image/mota") ("default"))))))

(define-public crate-deus-nqueens-0.1 (crate (name "deus-nqueens") (vers "0.1.0") (deps (list (crate-dep (name "ndarray") (req "^0.15.4") (default-features #t) (kind 0)) (crate-dep (name "num") (req "^0.4.0") (default-features #t) (kind 0)))) (hash "0w22y15jlpw1wxshq7pqkvhfi8a9l4gmpjrr2qaz3d1xb3nyzlhj")))

(define-public crate-deus-nqueens-0.2 (crate (name "deus-nqueens") (vers "0.2.0") (deps (list (crate-dep (name "ndarray") (req "^0.15.4") (default-features #t) (kind 0)) (crate-dep (name "num") (req "^0.4.0") (default-features #t) (kind 0)))) (hash "0qisic8bssi4ai6hrbzyv1s46bfn7jw2z6gxvkbin2644sz6s3ij")))

(define-public crate-deus-nqueens-0.3 (crate (name "deus-nqueens") (vers "0.3.0") (deps (list (crate-dep (name "ndarray") (req "^0.15.4") (default-features #t) (kind 0)) (crate-dep (name "num") (req "^0.4.0") (default-features #t) (kind 0)))) (hash "0mlj78s523ykk6mbapnh2x816n8mkvhr7fb40xvf3z1vldryb7s3")))

(define-public crate-deus-nqueens-0.4 (crate (name "deus-nqueens") (vers "0.4.0") (deps (list (crate-dep (name "ndarray") (req "^0.15.4") (default-features #t) (kind 0)) (crate-dep (name "num") (req "^0.4.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.136") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0.136") (default-features #t) (kind 0)) (crate-dep (name "wolfram-expr") (req "^0.1.1") (optional #t) (default-features #t) (kind 0)))) (hash "1kj2h23pfha4nsdykjpa2qk03ckfsazq6sq14wy211sfxlc98mcz") (features (quote (("default"))))))

(define-public crate-deus-nqueens-0.4 (crate (name "deus-nqueens") (vers "0.4.1") (deps (list (crate-dep (name "ndarray") (req "^0.15.6") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)) (crate-dep (name "svg") (req "^0.13.1") (default-features #t) (kind 0)) (crate-dep (name "pretty_assertions") (req "^1.3.0") (default-features #t) (kind 2)))) (hash "1xijcpm428v2478bjbl6y9jxcy90rn785f84n21fm17krlanzn5i")))

(define-public crate-deus-nqueens-0.4 (crate (name "deus-nqueens") (vers "0.4.2") (deps (list (crate-dep (name "itertools") (req "^0.10.5") (default-features #t) (kind 0)) (crate-dep (name "ndarray") (req "^0.15.6") (default-features #t) (kind 0)) (crate-dep (name "num") (req "^0.4.0") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)) (crate-dep (name "svg") (req "^0.13.1") (default-features #t) (kind 0)) (crate-dep (name "pretty_assertions") (req "^1.3.0") (default-features #t) (kind 2)))) (hash "13l4bb81p4fn1jagcmnghk2y4pjb5hy7jp2ijpzw9fbkizyhfglv")))

