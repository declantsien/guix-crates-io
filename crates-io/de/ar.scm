(define-module (crates-io de ar) #:use-module (crates-io))

(define-public crate-dear-0.1 (crate (name "dear") (vers "0.1.0") (hash "1hasnhc0xz0v17dgdpd6wb0afibhwz78zys9mg171x8x0i1a2xdi") (yanked #t)))

(define-public crate-dear-0.1 (crate (name "dear") (vers "0.1.1") (deps (list (crate-dep (name "clap") (req "^3.1.9") (features (quote ("derive" "env"))) (default-features #t) (kind 0)) (crate-dep (name "owo-colors") (req "^3.4.0") (default-features #t) (kind 0)) (crate-dep (name "textwrap") (req "^0.15.0") (default-features #t) (kind 0)))) (hash "17j8f6p3k9v156jmmw0sqpp6bnqhdn9vgczhglsfd5z6lfx6v7f4")))

(define-public crate-dearbitrary-1 (crate (name "dearbitrary") (vers "1.0.0") (deps (list (crate-dep (name "arbitrary") (req "^1.3.0") (features (quote ("derive"))) (default-features #t) (kind 2)) (crate-dep (name "dearbitrary_derive") (req "^1.0.0") (optional #t) (default-features #t) (kind 0)))) (hash "0w8cgaibh75zh751v82cxa62hw2gdri1sf1nvjcg3745ygfz7lw7") (features (quote (("derive" "dearbitrary_derive")))) (rust-version "1.70.0")))

(define-public crate-dearbitrary-1 (crate (name "dearbitrary") (vers "1.0.1") (deps (list (crate-dep (name "arbitrary") (req "^1.3.0") (features (quote ("derive"))) (default-features #t) (kind 2)) (crate-dep (name "dearbitrary_derive") (req "^1.0.0") (optional #t) (default-features #t) (kind 0)))) (hash "14kpmbwfflyc4ffhr704w2yvjsqh69fj1x6a57lrnrx7cpcjljd6") (features (quote (("derive" "dearbitrary_derive")))) (rust-version "1.70.0")))

(define-public crate-dearbitrary-1 (crate (name "dearbitrary") (vers "1.0.2") (deps (list (crate-dep (name "arbitrary") (req "^1.3.0") (features (quote ("derive"))) (default-features #t) (kind 2)) (crate-dep (name "dearbitrary_derive") (req "^1.0.0") (optional #t) (default-features #t) (kind 0)))) (hash "1946g6vyz85w2k6alhqlicpamp8bivz2lv3askj5c864fygjdiw3") (features (quote (("derive" "dearbitrary_derive")))) (rust-version "1.70.0")))

(define-public crate-dearbitrary-1 (crate (name "dearbitrary") (vers "1.0.4") (deps (list (crate-dep (name "arbitrary") (req "^1.3.1") (features (quote ("derive"))) (default-features #t) (kind 2)) (crate-dep (name "derive_dearbitrary") (req "^1.0.4") (optional #t) (default-features #t) (kind 0)))) (hash "1m0lnc89m41iwlqgndyrbz2qri6w0drp0ylzqs2ynrb93l4yd3kh") (features (quote (("derive" "derive_dearbitrary")))) (rust-version "1.70.0")))

(define-public crate-dearbitrary_derive-1 (crate (name "dearbitrary_derive") (vers "1.0.0") (deps (list (crate-dep (name "proc-macro2") (req "^1.0.60") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.28") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2.0.17") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "0y14c3py43p6r9wz14v673c7k4p30gzw547gwlxgnl6afjfgyd0z") (rust-version "1.70.0")))

(define-public crate-dearbitrary_derive-1 (crate (name "dearbitrary_derive") (vers "1.0.1") (deps (list (crate-dep (name "proc-macro2") (req "^1.0.38") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.28") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2.0.17") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "1ib8v1dirxzqdkj10zwyavdg00jhxlvi9sxw30gvrgibqsk1bzm1") (rust-version "1.70.0")))

(define-public crate-deary-0.0.1 (crate (name "deary") (vers "0.0.1") (deps (list (crate-dep (name "chrono") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^3.0.0-beta.1") (default-features #t) (kind 0)) (crate-dep (name "git2") (req "^0.13") (default-features #t) (kind 0)) (crate-dep (name "tempfile") (req "^3") (default-features #t) (kind 0)))) (hash "0506jq297cpii3qddkh7ydh6sb8vij3hy1yspwahkz0z8qxwp629")))

