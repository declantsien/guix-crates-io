(define-module (crates-io de ft) #:use-module (crates-io))

(define-public crate-deftsilo-0.1 (crate (name "deftsilo") (vers "0.1.0") (deps (list (crate-dep (name "arrrg") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "arrrg_derive") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "getopts") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "sha2") (req "^0.9") (default-features #t) (kind 0)))) (hash "0ch1kq1lzl192nmm93bp9y30qpp1l46s9a8a0znrsjg79k725l29")))

