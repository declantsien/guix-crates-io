(define-module (crates-io de pa) #:use-module (crates-io))

(define-public crate-depacked-0.1 (crate (name "depacked") (vers "0.1.0") (deps (list (crate-dep (name "skiplist") (req "^0.4.0") (default-features #t) (kind 0)))) (hash "04ggqmxldyhp7cc4pw75lh0l8x5x3zdwylpq7axzs248sq3d33i3")))

(define-public crate-depacked-0.2 (crate (name "depacked") (vers "0.2.0") (deps (list (crate-dep (name "skiplist") (req "^0.4.0") (default-features #t) (kind 0)))) (hash "00xmggsj7hcp33kiddpshxg8dxlaqk5zgagcr32r44l8s65a8w5y")))

(define-public crate-depacked-0.2 (crate (name "depacked") (vers "0.2.1") (deps (list (crate-dep (name "skiplist") (req "^0.4.0") (default-features #t) (kind 0)))) (hash "1lh6qarl6wcbs1m9algn0yb59pjzd82fggxkz6svlsn3b9hgl991")))

(define-public crate-depacked-0.2 (crate (name "depacked") (vers "0.2.2") (deps (list (crate-dep (name "skiplist") (req "^0.4.0") (default-features #t) (kind 0)))) (hash "1sz1ln8ybz9bphvvmjs8zcw62bbkscg1ycirm24nm2vbjaxlmlc4")))

(define-public crate-depacked-0.2 (crate (name "depacked") (vers "0.2.3") (deps (list (crate-dep (name "skiplist") (req "^0.4.0") (default-features #t) (kind 0)))) (hash "01ay9rc98l07sx2r2cfl5nj2n6cwfbj01wfsjx6ns31zjpfhlkzw")))

(define-public crate-departed-0.1 (crate (name "departed") (vers "0.1.0") (deps (list (crate-dep (name "text_io") (req "^0.1.8") (default-features #t) (kind 2)))) (hash "0xz12c95pxv0nlcnhk32izy9bwl5vmhcxlykp8vrbg40f72iagbq")))

(define-public crate-department-0.1 (crate (name "department") (vers "0.1.0") (deps (list (crate-dep (name "spin") (req "^0.9") (default-features #t) (kind 0)))) (hash "0ac3wn6hkvhcybcslrgczrw6azqjzrmavjzsrqnk9jgy3yisdg9k") (features (quote (("std" "alloc") ("default" "std") ("alloc"))))))

(define-public crate-department-0.1 (crate (name "department") (vers "0.1.1") (deps (list (crate-dep (name "spin") (req "^0.9") (default-features #t) (kind 0)))) (hash "0wzkj2z3cyaww245hx8zi8gnx0vywr7jii7gkzhw74q7sg1lgxp3") (features (quote (("std" "alloc") ("default" "std") ("alloc"))))))

(define-public crate-department-0.1 (crate (name "department") (vers "0.1.2") (deps (list (crate-dep (name "spin") (req "^0.9") (features (quote ("spin_mutex" "mutex"))) (kind 0)))) (hash "13shbghzc0bqr1rwvrbm4k0dsp1phygqzhnhz8r6i77p3v3pd2s2") (features (quote (("std" "alloc") ("default" "std") ("alloc"))))))

(define-public crate-department-0.2 (crate (name "department") (vers "0.2.0") (deps (list (crate-dep (name "spin") (req "^0.9") (features (quote ("spin_mutex" "mutex"))) (kind 0)))) (hash "1x4hbcb5q91zbvdnki9564fls9wgwrflams31wnng34d4asqnxr0") (features (quote (("vec") ("string" "vec") ("std") ("static") ("inline") ("fallback") ("default" "std" "coerce-unsized" "all_storages" "all_collections") ("coerce-unsized") ("box") ("alloc") ("all_storages" "inline" "static" "alloc" "fallback") ("all_collections" "box" "vec" "string"))))))

(define-public crate-department-0.2 (crate (name "department") (vers "0.2.1") (deps (list (crate-dep (name "spin") (req "^0.9") (features (quote ("spin_mutex" "mutex"))) (kind 0)))) (hash "1p2lpy33132k6s4yhb3m09fqp456f80zhl74vbkqwvqkg8k44csf") (features (quote (("vec") ("string" "vec") ("std") ("static") ("inline") ("fallback") ("default" "std" "coerce-unsized" "all_storages" "all_collections") ("coerce-unsized") ("box") ("alloc") ("all_storages" "inline" "static" "alloc" "fallback") ("all_collections" "box" "vec" "string"))))))

(define-public crate-department-0.2 (crate (name "department") (vers "0.2.2") (deps (list (crate-dep (name "spin") (req "^0.9") (features (quote ("spin_mutex" "mutex"))) (kind 0)))) (hash "1rxyaibzkzsafmcfk32klakxig85jhyqj7sxjxs5chfjma27xif1") (features (quote (("vec") ("string" "vec") ("std") ("static") ("inline") ("fallback") ("default" "std" "coerce-unsized" "all_storages" "all_collections") ("coerce-unsized") ("box") ("alloc") ("all_storages" "inline" "static" "alloc" "fallback") ("all_collections" "box" "vec" "string"))))))

(define-public crate-department-0.2 (crate (name "department") (vers "0.2.3") (deps (list (crate-dep (name "spin") (req "^0.9") (features (quote ("spin_mutex" "mutex"))) (kind 0)))) (hash "0r0xkysjyzg0czbn0n8azr5fp0qm68biby2wymw58p7v3vvsbrcg") (features (quote (("vec") ("string" "vec") ("std") ("static") ("inline") ("fallback") ("default" "std" "coerce-unsized" "all_storages" "all_collections") ("debug") ("coerce-unsized") ("box") ("alloc") ("all_storages" "inline" "static" "alloc" "fallback" "debug") ("all_collections" "box" "vec" "string"))))))

(define-public crate-department-0.2 (crate (name "department") (vers "0.2.4") (deps (list (crate-dep (name "spin") (req "^0.9") (features (quote ("spin_mutex" "mutex"))) (kind 0)))) (hash "0kp809c2bhc10h15r99m40vaial5npz7f18ln303hqnkp4ywwl1k") (features (quote (("vec") ("string" "vec") ("std") ("static") ("inline") ("fallback") ("default" "std" "coerce-unsized" "all_storages" "all_collections") ("debug") ("coerce-unsized") ("box") ("alloc") ("all_storages" "inline" "static" "alloc" "fallback" "debug") ("all_collections" "box" "vec" "string"))))))

(define-public crate-department-0.2 (crate (name "department") (vers "0.2.5") (deps (list (crate-dep (name "spin") (req "^0.9") (features (quote ("spin_mutex" "mutex"))) (kind 0)))) (hash "12qmxsk232zgq04iaaxf32zpwkif9x37hgavynqcbbi00dwi3hqm") (features (quote (("vec") ("string" "vec") ("std") ("static") ("inline") ("fallback") ("default" "std" "coerce-unsized" "all_storages" "all_collections") ("debug") ("coerce-unsized") ("box") ("alloc") ("all_storages" "inline" "static" "alloc" "fallback" "debug") ("all_collections" "box" "vec" "string"))))))

