(define-module (crates-io de an) #:use-module (crates-io))

(define-public crate-dean_minigrep-0.1 (crate (name "dean_minigrep") (vers "0.1.0") (hash "0b3z85c8mgkpr2l8q052rzcpba6icc9s63ji6hmlk77d9fyrp2ap")))

(define-public crate-dean_minigrep-0.1 (crate (name "dean_minigrep") (vers "0.1.1") (hash "12xw324s667gs67197kba9lj2bhmyisld7h685i01lr9nh4ky7m4")))

(define-public crate-deanonro-0.1 (crate (name "deanonro") (vers "0.1.0") (deps (list (crate-dep (name "ark-bn254") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "ark-ec") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "ark-serialize") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "bech32") (req "^0.11") (default-features #t) (kind 0)) (crate-dep (name "cbor4ii") (req "^0.3") (features (quote ("use_alloc"))) (default-features #t) (kind 0)) (crate-dep (name "chacha20") (req "^0.9") (default-features #t) (kind 0)) (crate-dep (name "hex") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.12.4") (default-features #t) (kind 0)) (crate-dep (name "tiny-keccak") (req "^2") (features (quote ("keccak"))) (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "1vj5p8hypysvh4pc2bc05ymfpvaa9gifs4ix1krwqz0h7v49viv1")))

