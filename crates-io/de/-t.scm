(define-module (crates-io de -t) #:use-module (crates-io))

(define-public crate-de-todo-0.1 (crate (name "de-todo") (vers "0.1.0") (deps (list (crate-dep (name "directories") (req "^5.0.1") (default-features #t) (kind 0)) (crate-dep (name "pancurses") (req "^0.17.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.162") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.96") (default-features #t) (kind 0)))) (hash "144cc88n718gbnp7zazxfmhxaynipnlhl77kdxi2821xscblxgsd")))

(define-public crate-de-todo-0.1 (crate (name "de-todo") (vers "0.1.1") (deps (list (crate-dep (name "directories") (req "^5.0.1") (default-features #t) (kind 0)) (crate-dep (name "pancurses") (req "^0.17.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.162") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.96") (default-features #t) (kind 0)))) (hash "0zs5md6skncbk6z20klrxjii68y9y8dlqgpprjvi3ppq36crmbdi") (yanked #t)))

(define-public crate-de-todo-0.1 (crate (name "de-todo") (vers "0.1.2") (deps (list (crate-dep (name "directories") (req "^5.0.1") (default-features #t) (kind 0)) (crate-dep (name "pancurses") (req "^0.17.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.162") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.96") (default-features #t) (kind 0)))) (hash "0zb13k9sc9wmyqv0xg366hi5sryvm09ym5w4d3gifnvlfdy6xafr")))

