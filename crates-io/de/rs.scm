(define-module (crates-io de rs) #:use-module (crates-io))

(define-public crate-derse-0.1 (crate (name "derse") (vers "0.1.0") (deps (list (crate-dep (name "derse-derive") (req "^0") (default-features #t) (kind 0)))) (hash "1hqn8zqy7m226v0sl1pdqqwdv9vs5ifbsrlrvn3rrh568bnqimak")))

(define-public crate-derse-0.1 (crate (name "derse") (vers "0.1.1") (deps (list (crate-dep (name "derse-derive") (req "^0") (default-features #t) (kind 0)))) (hash "03gbxg47xq5aajgigra1c6rsvvh047s8kq21vh3y1q07bbl8yf6b")))

(define-public crate-derse-0.1 (crate (name "derse") (vers "0.1.2") (deps (list (crate-dep (name "derse-derive") (req "^0") (default-features #t) (kind 0)))) (hash "0l6msf773l9agmmq6ckk2l2zq5y88gs2fjz3lkdcg0vxar1xdcdv")))

(define-public crate-derse-0.1 (crate (name "derse") (vers "0.1.3") (deps (list (crate-dep (name "derse-derive") (req "^0") (default-features #t) (kind 0)))) (hash "1frc38kah7fc5d2nnw4mxp40fzlxi8awxaz2dxz69naagb0sg8aj")))

(define-public crate-derse-0.1 (crate (name "derse") (vers "0.1.4") (deps (list (crate-dep (name "derse-derive") (req "^0") (default-features #t) (kind 0)))) (hash "1xyaas7mjp1fnamnfkw87ipkwkj3c1spz4mwi2rri3yplxg0di5g")))

(define-public crate-derse-0.1 (crate (name "derse") (vers "0.1.5") (deps (list (crate-dep (name "derse-derive") (req "^0") (default-features #t) (kind 0)))) (hash "0nyvrh1qwcdhama8px8crp0bjp6fd3wjhp1xmv18v0bblql5b8sp")))

(define-public crate-derse-0.1 (crate (name "derse") (vers "0.1.6") (deps (list (crate-dep (name "derse-derive") (req "^0") (default-features #t) (kind 0)))) (hash "1vrxhvphxym1razq48hp4r8hrjsnixqg0wjm0wkhjad1n0ng3cx5")))

(define-public crate-derse-0.1 (crate (name "derse") (vers "0.1.7") (deps (list (crate-dep (name "derse-derive") (req ">=0.1.5") (default-features #t) (kind 0)))) (hash "15224mw3snnq872r3wwg85fvw8cdh93g2p6l1jvgz3c037j71vpr")))

(define-public crate-derse-derive-0.1 (crate (name "derse-derive") (vers "0.1.0") (deps (list (crate-dep (name "proc-macro2") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2") (default-features #t) (kind 0)))) (hash "0ld9wxp417wyb0w8lz3ngpxj01p885ggrbq1ks47s6xcj1j8cnl4")))

(define-public crate-derse-derive-0.1 (crate (name "derse-derive") (vers "0.1.1") (deps (list (crate-dep (name "proc-macro2") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2") (default-features #t) (kind 0)))) (hash "1jm6ds21fykpg5inlyh7p82bas08drsqq4hbqq7id4hd60is9d5l")))

(define-public crate-derse-derive-0.1 (crate (name "derse-derive") (vers "0.1.2") (deps (list (crate-dep (name "proc-macro2") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2") (default-features #t) (kind 0)))) (hash "1yy1ybmvgh9hf3p9pishv307xyi49npax6h97w86vfs1w13gs0r4")))

(define-public crate-derse-derive-0.1 (crate (name "derse-derive") (vers "0.1.3") (deps (list (crate-dep (name "proc-macro2") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2") (default-features #t) (kind 0)))) (hash "1lxw6077is6n9a6brwld3pjx768w00g7yxiqzq5pbvl60bgb81r8")))

(define-public crate-derse-derive-0.1 (crate (name "derse-derive") (vers "0.1.4") (deps (list (crate-dep (name "proc-macro2") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2") (default-features #t) (kind 0)))) (hash "1g0rylg12hv9ws1hsifvxnb5rs35cb8saly6dq6apj8fsipwhg7r")))

(define-public crate-derse-derive-0.1 (crate (name "derse-derive") (vers "0.1.5") (deps (list (crate-dep (name "proc-macro2") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2") (default-features #t) (kind 0)))) (hash "1s50rm9gz9qinzmh4c0y55iqw9dxdgvcvx9ndfikn8wkpqr3icv0")))

