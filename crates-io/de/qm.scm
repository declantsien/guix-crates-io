(define-module (crates-io de qm) #:use-module (crates-io))

(define-public crate-deqmap-0.0.1 (crate (name "deqmap") (vers "0.0.1") (hash "0cd1hply6i8ygsj6xn8q3pnbfjplz49dpg9mkivlyaairsp2md05") (rust-version "1.64.0")))

(define-public crate-deqmap-0.0.2 (crate (name "deqmap") (vers "0.0.2") (hash "1b1hkq42habwvgaf8755bm6jxmfq3qp0z771hfj964qwy7pcl591") (rust-version "1.64.0")))

(define-public crate-deqmap-0.0.3 (crate (name "deqmap") (vers "0.0.3") (hash "1wm0fz52z0i4i9ygz1zmdndi561dk93sncsj8718gp0dbln5dfyx") (rust-version "1.64.0")))

(define-public crate-deqmap-0.0.4 (crate (name "deqmap") (vers "0.0.4") (hash "046x4mjnfxiwf64xlx0g7y2ma8daa1drvhxgf8wkv16j51dm4kp6") (rust-version "1.64.0")))

