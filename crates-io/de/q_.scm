(define-module (crates-io de q_) #:use-module (crates-io))

(define-public crate-deq_core-0.1 (crate (name "deq_core") (vers "0.1.0") (hash "0iq87hqznc1ml3a7pilhykz6mv8bi18qvg1dsmqmnx29iiwnl5iv")))

(define-public crate-deq_core-0.1 (crate (name "deq_core") (vers "0.1.1") (hash "1xqvjjhbn5fyigwj8gp1cnyrpfby5w0k9mfjcc12prm2dlmnl43l")))

(define-public crate-deq_core-0.1 (crate (name "deq_core") (vers "0.1.2") (hash "1bgy9s2il25m524790ij3nwkgpkz327krv7hk6cm5fmzmb0hq417")))

(define-public crate-deq_core-0.1 (crate (name "deq_core") (vers "0.1.5") (hash "0wnaglpgmq2s5azkm71xg7rlk1zf3kx18jkkhdbf4x7prcm30yb1")))

(define-public crate-deq_macros-0.1 (crate (name "deq_macros") (vers "0.1.0") (deps (list (crate-dep (name "deq_core") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (default-features #t) (kind 0)))) (hash "0qgh0h4lidl9xp8z371kh56syd9iv8d91732kcx79nwah23yy815")))

(define-public crate-deq_macros-0.1 (crate (name "deq_macros") (vers "0.1.1") (deps (list (crate-dep (name "deq_core") (req "^0.1.1") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (default-features #t) (kind 0)))) (hash "1hrymxcd4y23vh7k4dvqvym1mr03yqzjmhkcadbr2q22d0lv7n14")))

(define-public crate-deq_macros-0.1 (crate (name "deq_macros") (vers "0.1.2") (deps (list (crate-dep (name "deq_core") (req "^0.1.1") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (default-features #t) (kind 0)))) (hash "0jqqivznkrd4sqji55s7l545f4gyyssc6r8ngjnb7kvn0hr8pdbm")))

(define-public crate-deq_macros-0.1 (crate (name "deq_macros") (vers "0.1.5") (deps (list (crate-dep (name "deq_core") (req "^0.1.5") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (default-features #t) (kind 0)))) (hash "1gfi3gk22gpf341sblqvinj12nv04y6ib3bvfsg1g7ja7zg9a4b0")))

