(define-module (crates-io de ob) #:use-module (crates-io))

(define-public crate-deobfuscator-0.1 (crate (name "deobfuscator") (vers "0.1.0") (deps (list (crate-dep (name "base85") (req "^1.1.1") (default-features #t) (kind 0)))) (hash "1m2iqqli54zbfj9k2rgxr6gfbiqiqi6chypa83sr07dss9d2p9bj")))

(define-public crate-deobfuscator-0.1 (crate (name "deobfuscator") (vers "0.1.1") (deps (list (crate-dep (name "base85") (req "^1.1.1") (default-features #t) (kind 0)))) (hash "0jppb464gc6hn9rp1ilmsm55bh4s6cgr44rm5dv5shyav74c4cvc")))

