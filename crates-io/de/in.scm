(define-module (crates-io de in) #:use-module (crates-io))

(define-public crate-deinclude-0.1 (crate (name "deinclude") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^2.33.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.1.9") (default-features #t) (kind 0)))) (hash "0xvvy048wm69b9ppcnqn1lyx567xjxh7jr1s8iiadg2nhr0bb5v2")))

(define-public crate-deinclude-0.1 (crate (name "deinclude") (vers "0.1.1") (deps (list (crate-dep (name "clap") (req "^2.33.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.1.9") (default-features #t) (kind 0)))) (hash "0qfakii00nr3l7vp2xaxaa2pj2jw9ggkqnlc5hva463h5y76phrg")))

(define-public crate-deindentor-1 (crate (name "deindentor") (vers "1.0.0") (deps (list (crate-dep (name "clap") (req "^3.0") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "1z02h5nqh7r8xdihqh6c7zjnz316h2pjm2xmpwn9ycws592qbdzr")))

(define-public crate-deindentor-1 (crate (name "deindentor") (vers "1.0.1") (deps (list (crate-dep (name "clap") (req "^3.0") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "1mgylrfnzjdd7a9swjf5gvrcpcmdqj7nrjphffk9cw77y8d03gjn")))

(define-public crate-deindentor-1 (crate (name "deindentor") (vers "1.0.2") (deps (list (crate-dep (name "clap") (req "^3.0") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "1y8zvx4d2x60h05bhcdfwdzjhf4q4rsvpyfna50zixkz8mddsr5p")))

(define-public crate-deindentor-1 (crate (name "deindentor") (vers "1.1.0") (deps (list (crate-dep (name "clap") (req "^3.0") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "04rwc4xdz09hv1ba6pqxmrn16c60f0b7q3gish9y8m5p7cgcm5y2")))

(define-public crate-deindentor-1 (crate (name "deindentor") (vers "1.1.1") (deps (list (crate-dep (name "clap") (req "^3.0") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "0zzw4qr0p5v3b8v3qspiwmkwsmyh7z75rssx5w6894c5r051q9ij")))

(define-public crate-deinflect-0.1 (crate (name "deinflect") (vers "0.1.0") (deps (list (crate-dep (name "bitflags") (req "^2.4.1") (default-features #t) (kind 0)) (crate-dep (name "once_cell") (req "^1.18.0") (default-features #t) (kind 0)))) (hash "07w02nflq3kfpzwaxh5q8kj2rmq9lqvjvw4brd71vqdj4qnhgjj1")))

(define-public crate-deinflect-0.1 (crate (name "deinflect") (vers "0.1.1") (deps (list (crate-dep (name "bitflags") (req "^2.4.1") (default-features #t) (kind 0)) (crate-dep (name "once_cell") (req "^1.18.0") (default-features #t) (kind 0)))) (hash "0pdkc706qia4w8rblsbn8qr9x867brx3r0swbld12w1fvrh7gplq")))

(define-public crate-deinflect-0.1 (crate (name "deinflect") (vers "0.1.2") (deps (list (crate-dep (name "bitflags") (req "^2.4.1") (default-features #t) (kind 0)) (crate-dep (name "once_cell") (req "^1.18.0") (default-features #t) (kind 0)))) (hash "0cbf94a7y5bf4wdcsadq0p6byjlf4jang9r4ji9b3pd4nlgbx1v9")))

(define-public crate-deinflect-0.1 (crate (name "deinflect") (vers "0.1.3") (deps (list (crate-dep (name "bitflags") (req "^2.4.1") (default-features #t) (kind 0)) (crate-dep (name "once_cell") (req "^1.18.0") (default-features #t) (kind 0)))) (hash "0s4hcw6q32gdyfig242pwn5q5idbm5q9s4p5j0mfbnx656byrh5k")))

(define-public crate-deinflect-0.1 (crate (name "deinflect") (vers "0.1.4") (deps (list (crate-dep (name "bitflags") (req "^2") (default-features #t) (kind 0)) (crate-dep (name "once_cell") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)))) (hash "01rbhi42bfl9pzndl62k301agnpvji1kpqywkzl8kq2l4njn5gh5") (v 2) (features2 (quote (("serde" "dep:serde" "bitflags/serde"))))))

