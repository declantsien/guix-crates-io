(define-module (crates-io de fa) #:use-module (crates-io))

(define-public crate-deface-0.1 (crate (name "deface") (vers "0.1.0") (hash "0zlim6qjh3wn4rbw587b68wx4ln82ng66m9x6arg8kdxgkicj5rn")))

(define-public crate-deface-0.1 (crate (name "deface") (vers "0.1.1") (hash "1q2ckw6jhy5yckywcyvlyin9g67qpa28ix0n3hbjsm76rbycg06n")))

(define-public crate-deface-0.1 (crate (name "deface") (vers "0.1.2") (deps (list (crate-dep (name "myriad") (req "^0.1.1") (default-features #t) (kind 0)))) (hash "0a0wj7yj9lhrc46zh7ks9jrvqdydxxsav1mwb9vcyzvmpwh73n5x")))

(define-public crate-default-0.1 (crate (name "default") (vers "0.1.0") (hash "1gi6ysz3yjzdlarsngc10d56zbxm56yy4lnvaj0dlk4fy6qzsdg8")))

(define-public crate-default-0.1 (crate (name "default") (vers "0.1.1") (hash "0fa648w6n5i9naqpy21nimcdbxggxzg0kr09bwwglmxybh5qkbq4")))

(define-public crate-default-0.1 (crate (name "default") (vers "0.1.2") (hash "19xs7nxy23knkjpfyypcbagjbknxfjz42mi14pw2gnwl7kg56f7k")))

(define-public crate-default-args-1 (crate (name "default-args") (vers "1.0.0-beta") (deps (list (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "15gp2dnp5n3lm6pamv3hnn0x9pzi8hdh6jph15jjhppqjcjjnsq8")))

(define-public crate-default-args-1 (crate (name "default-args") (vers "1.0.0") (deps (list (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (features (quote ("full"))) (default-features #t) (kind 0)) (crate-dep (name "tokio-test") (req "^0.4") (default-features #t) (kind 2)))) (hash "1sf55164sa87s9f0jkj4g7qka8gv2s4l1hy4rlkw8iw8m3spichk")))

(define-public crate-default-boxed-0.1 (crate (name "default-boxed") (vers "0.1.0") (deps (list (crate-dep (name "default-boxed-derive") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "01qjnzncfs603a77iy2qfdwjgkx551z5v3d28a43fpsnvd9c1nly")))

(define-public crate-default-boxed-0.1 (crate (name "default-boxed") (vers "0.1.1") (deps (list (crate-dep (name "default-boxed-derive") (req "^0.1.1") (default-features #t) (kind 0)))) (hash "0h0lfwvwxhwdcsbilns9cpiig60v7cilhk5hmh8npz9a3n5wcfiy")))

(define-public crate-default-boxed-0.1 (crate (name "default-boxed") (vers "0.1.2") (deps (list (crate-dep (name "default-boxed-derive") (req "^0.1.2") (default-features #t) (kind 0)))) (hash "0wsf32z17lmfzims3m80xs0hfjybwxz78drp19zs7fzxxdj6wgk3")))

(define-public crate-default-boxed-0.1 (crate (name "default-boxed") (vers "0.1.3") (deps (list (crate-dep (name "default-boxed-derive") (req "^0.1.3") (default-features #t) (kind 0)))) (hash "10czzb8hgp1f7j8mm534gp7fmf6c05jp963yf0idr3fxb77hafds")))

(define-public crate-default-boxed-0.1 (crate (name "default-boxed") (vers "0.1.4") (deps (list (crate-dep (name "default-boxed-derive") (req "^0.1.3") (default-features #t) (kind 0)))) (hash "0fj1miscp54smnlw4a76sf6phyb1kpwh06c0k7lw07j9g7r0ifxa")))

(define-public crate-default-boxed-0.1 (crate (name "default-boxed") (vers "0.1.5") (deps (list (crate-dep (name "default-boxed-derive") (req "^0.1.4") (default-features #t) (kind 0)))) (hash "00zcf9hb1nzf6vqyrvqlr0awni6sznq24h0a2kksx4fyz9r3rgpn")))

(define-public crate-default-boxed-0.1 (crate (name "default-boxed") (vers "0.1.6") (deps (list (crate-dep (name "default-boxed-derive") (req "^0.1.4") (default-features #t) (kind 0)))) (hash "1iq0ckin49zqrnlqmj2r9ww93q284vxc86hl827vqqwzfgdvigzp")))

(define-public crate-default-boxed-0.1 (crate (name "default-boxed") (vers "0.1.7") (deps (list (crate-dep (name "default-boxed-derive") (req "^0.1.4") (default-features #t) (kind 0)))) (hash "1kl5h2ya4yz479fbkdwyqy63gsns1fkqfbi34zzw3j5rr4vbvdmb")))

(define-public crate-default-boxed-0.1 (crate (name "default-boxed") (vers "0.1.8") (deps (list (crate-dep (name "default-boxed-derive") (req "^0.1.5") (default-features #t) (kind 0)))) (hash "0r1rv464rxvlxc07054qrk5rqdyyv3j413cy6bv4xahzf3qckwgq")))

(define-public crate-default-boxed-0.1 (crate (name "default-boxed") (vers "0.1.9") (deps (list (crate-dep (name "default-boxed-derive") (req "^0.1.5") (default-features #t) (kind 0)))) (hash "07nwqp703ii17awxldg86q3c6kkw11d1f44b5lqmq58i1y9aj9c0")))

(define-public crate-default-boxed-0.1 (crate (name "default-boxed") (vers "0.1.10") (deps (list (crate-dep (name "default-boxed-derive") (req "^0.1.5") (default-features #t) (kind 0)) (crate-dep (name "trybuild") (req "^1.0.28") (default-features #t) (kind 2)))) (hash "1qqlfwvdfi8crlqdyvhiawp54zvmpvgysrrmk30rj4b6x6fzy8md")))

(define-public crate-default-boxed-0.1 (crate (name "default-boxed") (vers "0.1.11") (deps (list (crate-dep (name "default-boxed-derive") (req "^0.1.5") (default-features #t) (kind 0)) (crate-dep (name "trybuild") (req "^1.0.28") (default-features #t) (target "cfg(not(miri))") (kind 2)))) (hash "1w2npp4wqn4mc8g8yx1vw783klidl08qhrx1w4pqm4jpziaiyilc")))

(define-public crate-default-boxed-0.2 (crate (name "default-boxed") (vers "0.2.0") (deps (list (crate-dep (name "default-boxed-derive") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "trybuild") (req "^1.0.28") (default-features #t) (target "cfg(not(miri))") (kind 2)))) (hash "0c8wgbjs3m6iwf3fnzd6axg7qnabixgy89pbwp78mk9v1g5wg35l")))

(define-public crate-default-boxed-derive-0.1 (crate (name "default-boxed-derive") (vers "0.1.0") (deps (list (crate-dep (name "proc-macro2") (req "^1.0.2") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.2") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.5") (default-features #t) (kind 0)))) (hash "1rqj0f0xzdny4vpkf6ai0i6iwf7sdv2h9a1j5ybii6kyinwdil19")))

(define-public crate-default-boxed-derive-0.1 (crate (name "default-boxed-derive") (vers "0.1.1") (deps (list (crate-dep (name "proc-macro2") (req "^1.0.2") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.2") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.5") (default-features #t) (kind 0)))) (hash "0hfi1by8fmxcsr7lgv97287q3pr2z1dpillywi9gbc5k2g24h72w")))

(define-public crate-default-boxed-derive-0.1 (crate (name "default-boxed-derive") (vers "0.1.2") (deps (list (crate-dep (name "proc-macro2") (req "^1.0.2") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.2") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.5") (default-features #t) (kind 0)))) (hash "1bsawlnv1j48n53aq72k839mlz0svb63kczzr0jqj8k0xnygswsy")))

(define-public crate-default-boxed-derive-0.1 (crate (name "default-boxed-derive") (vers "0.1.3") (deps (list (crate-dep (name "proc-macro2") (req "^1.0.2") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.2") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.5") (default-features #t) (kind 0)))) (hash "0az6rbpgd0pcqy0k61whr5ik43mhwzxba76h1binrl659rh79b8r")))

(define-public crate-default-boxed-derive-0.1 (crate (name "default-boxed-derive") (vers "0.1.4") (deps (list (crate-dep (name "proc-macro2") (req "^1.0.2") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.2") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.5") (default-features #t) (kind 0)))) (hash "0x55gmn8vfix0hv69cx6i4s2kh5z1gz5qncrjv7snsm46ifpq0f0")))

(define-public crate-default-boxed-derive-0.1 (crate (name "default-boxed-derive") (vers "0.1.5") (deps (list (crate-dep (name "proc-macro2") (req "^1.0.2") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.2") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.5") (default-features #t) (kind 0)))) (hash "0nrhqs4c38h2d62x8kjfcf3w0fafljac3swd3ni35j4if28mnldx")))

(define-public crate-default-boxed-derive-0.1 (crate (name "default-boxed-derive") (vers "0.1.6") (deps (list (crate-dep (name "proc-macro2") (req "^1.0.2") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.2") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.5") (features (quote ("derive" "parsing" "printing"))) (default-features #t) (kind 0)))) (hash "0axpynj94z9vzcn0d6sfyyy0ix0lkgbzwwwc7nsypbvhxwh6pcii")))

(define-public crate-default-boxed-derive-0.1 (crate (name "default-boxed-derive") (vers "0.1.7") (deps (list (crate-dep (name "proc-macro2") (req "^1.0.2") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.2") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.5") (features (quote ("derive" "parsing" "printing"))) (default-features #t) (kind 0)))) (hash "1myilxjblz0av9gkhpmrkv06hr10i98qn4jndk7k8ydw362spar3")))

(define-public crate-default-boxed-derive-0.2 (crate (name "default-boxed-derive") (vers "0.2.0") (deps (list (crate-dep (name "proc-macro2") (req "^1.0.2") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.2") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.5") (features (quote ("derive" "parsing" "printing"))) (default-features #t) (kind 0)))) (hash "0phg7aiqzrfi5jpj4bxf3s0fwzapnmz3flz0y0x7wvwrg2hddfwl")))

(define-public crate-default-constructor-0.1 (crate (name "default-constructor") (vers "0.1.0") (hash "0r83g1mkzn7gbazqpjprghfr953lkyjzixcwkmil4xs5sdy7j6mh")))

(define-public crate-default-constructor-0.1 (crate (name "default-constructor") (vers "0.1.1") (hash "1f7cdxd5ffj2kc7n1px4i9sq8w4kcixapyn903fp9vhac8rkwm42")))

(define-public crate-default-constructor-0.1 (crate (name "default-constructor") (vers "0.1.2") (hash "1y578gql5z06347asj89m97gmzjbval3ym3zaap6xwcfkrsl4plq")))

(define-public crate-default-constructor-0.1 (crate (name "default-constructor") (vers "0.1.3") (hash "1mszz00dnhvh5wh77dyacpjiyn8cfrg4x2ccqi9fdfwfav0f4qjl")))

(define-public crate-default-constructor-0.2 (crate (name "default-constructor") (vers "0.2.0") (hash "01vlc5dnwvgll6c2y21nn0ngwhilkq7i16y3wyizwrdnmhcm5w7m") (features (quote (("std") ("default" "std"))))))

(define-public crate-default-constructor-0.2 (crate (name "default-constructor") (vers "0.2.1") (hash "1x8x5xwdw1dghwjmdfvgblnd6r7048i5hnxpq924z0p9wz8qizis") (features (quote (("std") ("default" "std"))))))

(define-public crate-default-conversion-0.1 (crate (name "default-conversion") (vers "0.1.0") (deps (list (crate-dep (name "darling") (req "^0.10") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.7") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.44") (features (quote ("full" "extra-traits" "visit-mut" "parsing"))) (default-features #t) (kind 0)))) (hash "0nlppjnnsf4p58iv7gq6z7mnlf4xli9xn2bi851gczkn3xnkdws9")))

(define-public crate-default-conversion-0.2 (crate (name "default-conversion") (vers "0.2.0") (deps (list (crate-dep (name "darling") (req "^0.10") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.7") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.44") (features (quote ("full" "extra-traits" "visit-mut" "parsing"))) (default-features #t) (kind 0)))) (hash "0kc3m1dz8b8z63c155pkckrbsy6sga7mdhjkqfkc70drcrnykr2f")))

(define-public crate-default-editor-0.1 (crate (name "default-editor") (vers "0.1.0") (hash "00jpk0issyb0j6sw8rb7rkxlsa837pps9jbaslbg1x7kxvh7ya0z")))

(define-public crate-default-env-0.1 (crate (name "default-env") (vers "0.1.0") (deps (list (crate-dep (name "proc-macro2") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^0.6") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^0.15") (default-features #t) (kind 0)))) (hash "1gg4362lgp16hjizliv99yf67fra31xgbz88zikyi8vqbl91byy5")))

(define-public crate-default-env-0.1 (crate (name "default-env") (vers "0.1.1") (deps (list (crate-dep (name "proc-macro2") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^0.6") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^0.15") (default-features #t) (kind 0)))) (hash "1lxzfyd26h2779bsh18fwlg8bm6grap88pk2zjgffxwjsa1fnlzp")))

(define-public crate-default-ext-0.1 (crate (name "default-ext") (vers "0.1.0") (hash "164amyw232v9xaiyhnkayv1svzabgmas1kcyx1bff6xndfcwfy10")))

(define-public crate-default-from-serde-0.1 (crate (name "default-from-serde") (vers "0.1.0") (hash "05b85qcsg2p1rs8icahrmsaqnp94cnnbnzx68s6328cwfyyrphi1")))

(define-public crate-default-from-serde-0.1 (crate (name "default-from-serde") (vers "0.1.1") (deps (list (crate-dep (name "derive-default-from-serde") (req "^0.1.1") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1") (kind 0)) (crate-dep (name "serde_derive") (req "^1.0.193") (default-features #t) (kind 2)))) (hash "0fx4ir7qy81m62c7850ri5hmald4k28jmg1s42np21daj0mg08hh") (features (quote (("std" "serde/std") ("default" "std"))))))

(define-public crate-default-from-serde-0.1 (crate (name "default-from-serde") (vers "0.1.2") (deps (list (crate-dep (name "derive-default-from-serde") (req "^0.1.1") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1") (kind 0)) (crate-dep (name "serde_derive") (req "^1.0.193") (default-features #t) (kind 2)))) (hash "1mjndz2fhnxsdm3r14ai3ijliil9pg68gvc1x40gqx3gdhpxy951") (features (quote (("std" "serde/std") ("default" "std"))))))

(define-public crate-default-impl-0.1 (crate (name "default-impl") (vers "0.1.0") (hash "04g4hvgipxp76zx1slhwmmpwngfvafy17agdbh7zcvw08zmygyr7")))

(define-public crate-default-net-0.1 (crate (name "default-net") (vers "0.1.0") (deps (list (crate-dep (name "pnet") (req "^0.26") (default-features #t) (kind 0)))) (hash "0dbcd2bknvbc2vhwb8k2a2cs80a17cfap53gj55sk51wnaprm5n5") (yanked #t)))

(define-public crate-default-net-0.1 (crate (name "default-net") (vers "0.1.1") (deps (list (crate-dep (name "pnet") (req "^0.26") (default-features #t) (kind 0)))) (hash "08rgpz60pv4xkdfqjdpbcrcdy5wrjkkxyvz2jgqr0iclf4d2w71d") (yanked #t)))

(define-public crate-default-net-0.2 (crate (name "default-net") (vers "0.2.0") (deps (list (crate-dep (name "pnet") (req "^0.26") (default-features #t) (target "cfg(target_os = \"windows\")") (kind 0)) (crate-dep (name "pnet") (req "^0.27") (default-features #t) (target "cfg(not(windows))") (kind 0)))) (hash "1y3596fj4g0f1infs919pvzjiwhm7di9wf71qvd40vmg593v3hpv") (yanked #t)))

(define-public crate-default-net-0.2 (crate (name "default-net") (vers "0.2.1") (deps (list (crate-dep (name "pnet") (req "^0.26") (default-features #t) (kind 0)))) (hash "015bcj6maf84hnc9a7ang6yqzl87ck37kfn77b5fk59zgpk0c0mz") (yanked #t)))

(define-public crate-default-net-0.3 (crate (name "default-net") (vers "0.3.0") (deps (list (crate-dep (name "pnet") (req "^0.28") (default-features #t) (kind 0)) (crate-dep (name "winapi") (req "^0.3.8") (features (quote ("std"))) (default-features #t) (target "cfg(windows)") (kind 0)))) (hash "0rw4m6v1f2fnq62x72560qxpzyybfndlmg71x4k3k9k2wwaibakc") (yanked #t)))

(define-public crate-default-net-0.4 (crate (name "default-net") (vers "0.4.0") (deps (list (crate-dep (name "pnet") (req "^0.28") (default-features #t) (kind 0)) (crate-dep (name "winapi") (req "^0.3.9") (features (quote ("std"))) (default-features #t) (target "cfg(windows)") (kind 0)))) (hash "103m0b1h7rb99vxnn77rwwii595xs46nq9yg89d8g0crqx8rprhv") (yanked #t)))

(define-public crate-default-net-0.5 (crate (name "default-net") (vers "0.5.0") (deps (list (crate-dep (name "pnet_datalink") (req "^0.28") (default-features #t) (kind 0)) (crate-dep (name "pnet_packet") (req "^0.28") (default-features #t) (kind 0)) (crate-dep (name "winapi") (req "^0.3.9") (features (quote ("std"))) (default-features #t) (target "cfg(windows)") (kind 0)))) (hash "0hfl6kkzdhpalmn5jmmni15q18k8n4fbdsfc0920w0fpx1dzr8k1")))

(define-public crate-default-net-0.6 (crate (name "default-net") (vers "0.6.0") (deps (list (crate-dep (name "pnet_datalink") (req "^0.28") (default-features #t) (target "cfg(not(windows))") (kind 0)) (crate-dep (name "pnet_packet") (req "^0.28") (default-features #t) (target "cfg(not(windows))") (kind 0)) (crate-dep (name "windows") (req "^0.29.0") (features (quote ("Win32_Foundation" "Win32_NetworkManagement_IpHelper"))) (default-features #t) (target "cfg(windows)") (kind 0)))) (hash "1lr0l7fq3x6w81j1jb77m48yh2p0ni5z33xp8ynwb6s5n42xhxjy")))

(define-public crate-default-net-0.7 (crate (name "default-net") (vers "0.7.0") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (target "cfg(not(windows))") (kind 0)) (crate-dep (name "windows") (req "^0.29.0") (features (quote ("Win32_Foundation" "Win32_NetworkManagement_IpHelper"))) (default-features #t) (target "cfg(windows)") (kind 0)))) (hash "1n1xa33biy3dy6c0c2blg8am3df67gl0f86hqsngdmg9qrj7srla")))

(define-public crate-default-net-0.8 (crate (name "default-net") (vers "0.8.0") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (target "cfg(not(windows))") (kind 0)) (crate-dep (name "windows") (req "^0.29.0") (features (quote ("Win32_Foundation" "Win32_NetworkManagement_IpHelper"))) (default-features #t) (target "cfg(windows)") (kind 0)))) (hash "01sw8i11ydg2ckdnc4crfyfxwwb5mdfd720sgvvi520f1fk2yqxi")))

(define-public crate-default-net-0.8 (crate (name "default-net") (vers "0.8.1") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (target "cfg(not(windows))") (kind 0)) (crate-dep (name "windows") (req "^0.29.0") (features (quote ("Win32_Foundation" "Win32_NetworkManagement_IpHelper"))) (default-features #t) (target "cfg(windows)") (kind 0)))) (hash "1pn09ad3ls4agaq6y07aj2mrqf4qrmxrx3g8bcmddvyn3kwzkvlq")))

(define-public crate-default-net-0.8 (crate (name "default-net") (vers "0.8.2") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (target "cfg(not(windows))") (kind 0)) (crate-dep (name "windows") (req "^0.29.0") (features (quote ("Win32_Foundation" "Win32_NetworkManagement_IpHelper"))) (default-features #t) (target "cfg(windows)") (kind 0)))) (hash "15rzxpglgv9c3841h07vbqdkvs2kj3d43w91mqyvpps7s9j2bh5l")))

(define-public crate-default-net-0.9 (crate (name "default-net") (vers "0.9.0") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (target "cfg(not(windows))") (kind 0)) (crate-dep (name "system-configuration") (req "^0.5.0") (default-features #t) (target "cfg(any(target_os = \"macos\", target_os = \"ios\"))") (kind 0)) (crate-dep (name "windows") (req "^0.29.0") (features (quote ("Win32_Foundation" "Win32_NetworkManagement_IpHelper"))) (default-features #t) (target "cfg(windows)") (kind 0)))) (hash "0byn6qkm4ylnk02v7liyn94rzpp1g9fcqwdbd4k5xh7n8p3gn0cv")))

(define-public crate-default-net-0.10 (crate (name "default-net") (vers "0.10.0") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "memalloc") (req "^0.1.0") (default-features #t) (target "cfg(windows)") (kind 0)) (crate-dep (name "system-configuration") (req "^0.5.0") (default-features #t) (target "cfg(any(target_os = \"macos\", target_os = \"ios\"))") (kind 0)) (crate-dep (name "windows") (req "^0.30.0") (features (quote ("Win32_Foundation" "Win32_NetworkManagement_IpHelper" "Win32_Networking_WinSock"))) (default-features #t) (target "cfg(windows)") (kind 0)))) (hash "13y0ymwh9mnlgmyldkyr8qsg1cd6psg0fpafpl79rmmw89q0a5cd")))

(define-public crate-default-net-0.11 (crate (name "default-net") (vers "0.11.0") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "memalloc") (req "^0.1.0") (default-features #t) (target "cfg(windows)") (kind 0)) (crate-dep (name "system-configuration") (req "^0.5.0") (default-features #t) (target "cfg(any(target_os = \"macos\", target_os = \"ios\"))") (kind 0)) (crate-dep (name "windows") (req "^0.30.0") (features (quote ("Win32_Foundation" "Win32_NetworkManagement_IpHelper" "Win32_Networking_WinSock"))) (default-features #t) (target "cfg(windows)") (kind 0)))) (hash "0imn9lqra4hcx2nl6ji2n7yhv25n0jxv6lanr0ifg90b3d3hvrq5")))

(define-public crate-default-net-0.12 (crate (name "default-net") (vers "0.12.0") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "memalloc") (req "^0.1.0") (default-features #t) (target "cfg(windows)") (kind 0)) (crate-dep (name "system-configuration") (req "^0.5.0") (default-features #t) (target "cfg(any(target_os = \"macos\", target_os = \"ios\"))") (kind 0)) (crate-dep (name "windows") (req "^0.30.0") (features (quote ("Win32_Foundation" "Win32_NetworkManagement_IpHelper" "Win32_Networking_WinSock"))) (default-features #t) (target "cfg(windows)") (kind 0)))) (hash "0qrs0vy6x0dys9fhf0493srp3xjw6xvaddfqgm539yq63vnlkqql")))

(define-public crate-default-net-0.13 (crate (name "default-net") (vers "0.13.0") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "memalloc") (req "^0.1.0") (default-features #t) (target "cfg(windows)") (kind 0)) (crate-dep (name "system-configuration") (req "^0.5.0") (default-features #t) (target "cfg(any(target_os = \"macos\", target_os = \"ios\"))") (kind 0)) (crate-dep (name "windows") (req "^0.32.0") (features (quote ("Win32_Foundation" "Win32_NetworkManagement_IpHelper" "Win32_Networking_WinSock"))) (default-features #t) (target "cfg(windows)") (kind 0)))) (hash "0hz0x6yz48n1a0qnr62qw0p1msdln2fciq0p8bxxxhi4m9x4y1cv")))

(define-public crate-default-net-0.13 (crate (name "default-net") (vers "0.13.1") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "memalloc") (req "^0.1.0") (default-features #t) (target "cfg(windows)") (kind 0)) (crate-dep (name "system-configuration") (req "^0.5.0") (default-features #t) (target "cfg(any(target_os = \"macos\", target_os = \"ios\"))") (kind 0)) (crate-dep (name "windows") (req "^0.32.0") (features (quote ("Win32_Foundation" "Win32_NetworkManagement_IpHelper" "Win32_Networking_WinSock"))) (default-features #t) (target "cfg(windows)") (kind 0)))) (hash "0lddbvpafd4dr5576brh2wj21n441a8k6ag4vxg9dk12p8pypk8j")))

(define-public crate-default-net-0.14 (crate (name "default-net") (vers "0.14.0") (deps (list (crate-dep (name "dlopen2") (req "^0.4") (default-features #t) (target "cfg(target_os = \"android\")") (kind 0)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "memalloc") (req "^0.1.0") (default-features #t) (target "cfg(windows)") (kind 0)) (crate-dep (name "netlink-packet-core") (req "^0.5") (default-features #t) (target "cfg(target_os = \"android\")") (kind 0)) (crate-dep (name "netlink-packet-route") (req "^0.15") (default-features #t) (target "cfg(target_os = \"android\")") (kind 0)) (crate-dep (name "netlink-sys") (req "^0.8") (default-features #t) (target "cfg(target_os = \"android\")") (kind 0)) (crate-dep (name "once_cell") (req "^1") (default-features #t) (target "cfg(target_os = \"android\")") (kind 0)) (crate-dep (name "system-configuration") (req "^0.5.0") (default-features #t) (target "cfg(any(target_os = \"macos\", target_os = \"ios\"))") (kind 0)) (crate-dep (name "windows") (req "^0.32.0") (features (quote ("Win32_Foundation" "Win32_NetworkManagement_IpHelper" "Win32_Networking_WinSock"))) (default-features #t) (target "cfg(windows)") (kind 0)))) (hash "1jf4n53qibqs2kw0279pk49s3673gadp89c9m4lm50dvdg68vvqj")))

(define-public crate-default-net-0.14 (crate (name "default-net") (vers "0.14.1") (deps (list (crate-dep (name "dlopen2") (req "^0.4") (default-features #t) (target "cfg(target_os = \"android\")") (kind 0)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "memalloc") (req "^0.1.0") (default-features #t) (target "cfg(windows)") (kind 0)) (crate-dep (name "netlink-packet-core") (req "^0.5") (default-features #t) (target "cfg(target_os = \"android\")") (kind 0)) (crate-dep (name "netlink-packet-route") (req "^0.15") (default-features #t) (target "cfg(target_os = \"android\")") (kind 0)) (crate-dep (name "netlink-sys") (req "^0.8") (default-features #t) (target "cfg(target_os = \"android\")") (kind 0)) (crate-dep (name "once_cell") (req "^1") (default-features #t) (target "cfg(target_os = \"android\")") (kind 0)) (crate-dep (name "system-configuration") (req "^0.5.0") (default-features #t) (target "cfg(any(target_os = \"macos\", target_os = \"ios\"))") (kind 0)) (crate-dep (name "windows") (req "^0.32.0") (features (quote ("Win32_Foundation" "Win32_NetworkManagement_IpHelper" "Win32_Networking_WinSock"))) (default-features #t) (target "cfg(windows)") (kind 0)))) (hash "00z0hjh8rqxablp4kgvrh3v1v9bn7rdx355jsvx9jr6mmr1qp2d4")))

(define-public crate-default-net-0.15 (crate (name "default-net") (vers "0.15.0") (deps (list (crate-dep (name "dlopen2") (req "^0.4") (default-features #t) (target "cfg(target_os = \"android\")") (kind 0)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "memalloc") (req "^0.1.0") (default-features #t) (target "cfg(windows)") (kind 0)) (crate-dep (name "netlink-packet-core") (req "^0.5") (default-features #t) (target "cfg(target_os = \"android\")") (kind 0)) (crate-dep (name "netlink-packet-route") (req "^0.15") (default-features #t) (target "cfg(target_os = \"android\")") (kind 0)) (crate-dep (name "netlink-sys") (req "^0.8") (default-features #t) (target "cfg(target_os = \"android\")") (kind 0)) (crate-dep (name "once_cell") (req "^1") (default-features #t) (target "cfg(target_os = \"android\")") (kind 0)) (crate-dep (name "system-configuration") (req "^0.5.0") (default-features #t) (target "cfg(any(target_os = \"macos\", target_os = \"ios\"))") (kind 0)) (crate-dep (name "windows") (req "^0.48.0") (features (quote ("Win32_Foundation" "Win32_NetworkManagement_IpHelper" "Win32_Networking_WinSock" "Win32_NetworkManagement_Ndis"))) (default-features #t) (target "cfg(windows)") (kind 0)))) (hash "1ffnvs24slw2p0l2k6v2pw3g1gixmyqyljc8wzisk0lvgb658qmh")))

(define-public crate-default-net-0.16 (crate (name "default-net") (vers "0.16.0") (deps (list (crate-dep (name "dlopen2") (req "^0.4") (target "cfg(target_os = \"android\")") (kind 0)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "memalloc") (req "^0.1.0") (default-features #t) (target "cfg(windows)") (kind 0)) (crate-dep (name "netlink-packet-core") (req "^0.5") (default-features #t) (target "cfg(target_os = \"android\")") (kind 0)) (crate-dep (name "netlink-packet-route") (req "^0.15") (default-features #t) (target "cfg(target_os = \"android\")") (kind 0)) (crate-dep (name "netlink-sys") (req "^0.8") (default-features #t) (target "cfg(target_os = \"android\")") (kind 0)) (crate-dep (name "once_cell") (req "^1") (default-features #t) (target "cfg(target_os = \"android\")") (kind 0)) (crate-dep (name "system-configuration") (req "^0.5.1") (default-features #t) (target "cfg(any(target_os = \"macos\", target_os = \"ios\"))") (kind 0)) (crate-dep (name "windows") (req "^0.48.0") (features (quote ("Win32_Foundation" "Win32_NetworkManagement_IpHelper" "Win32_Networking_WinSock" "Win32_NetworkManagement_Ndis"))) (default-features #t) (target "cfg(windows)") (kind 0)))) (hash "0agpxdlvg7sn2s8jaim1x60fjp5pdw8bw56acnaiplz1xy333f2k") (yanked #t)))

(define-public crate-default-net-0.16 (crate (name "default-net") (vers "0.16.1") (deps (list (crate-dep (name "dlopen2") (req "^0.5") (target "cfg(target_os = \"android\")") (kind 0)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "memalloc") (req "^0.1.0") (default-features #t) (target "cfg(windows)") (kind 0)) (crate-dep (name "netlink-packet-core") (req "^0.7") (default-features #t) (target "cfg(target_os = \"android\")") (kind 0)) (crate-dep (name "netlink-packet-route") (req "^0.17") (default-features #t) (target "cfg(target_os = \"android\")") (kind 0)) (crate-dep (name "netlink-sys") (req "^0.8") (default-features #t) (target "cfg(target_os = \"android\")") (kind 0)) (crate-dep (name "once_cell") (req "^1") (default-features #t) (target "cfg(target_os = \"android\")") (kind 0)) (crate-dep (name "system-configuration") (req "^0.5.1") (default-features #t) (target "cfg(any(target_os = \"macos\", target_os = \"ios\"))") (kind 0)) (crate-dep (name "windows") (req "^0.48.0") (features (quote ("Win32_Foundation" "Win32_NetworkManagement_IpHelper" "Win32_Networking_WinSock" "Win32_NetworkManagement_Ndis"))) (default-features #t) (target "cfg(windows)") (kind 0)))) (hash "11qn6j5iknrc6hk060661s2rr8a0y9gxw80fz2xvl63lw8g40ixf") (yanked #t)))

(define-public crate-default-net-0.16 (crate (name "default-net") (vers "0.16.2") (deps (list (crate-dep (name "dlopen2") (req "^0.5") (target "cfg(target_os = \"android\")") (kind 0)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "memalloc") (req "^0.1.0") (default-features #t) (target "cfg(windows)") (kind 0)) (crate-dep (name "netlink-packet-core") (req "^0.7") (default-features #t) (target "cfg(target_os = \"android\")") (kind 0)) (crate-dep (name "netlink-packet-route") (req "^0.17") (default-features #t) (target "cfg(target_os = \"android\")") (kind 0)) (crate-dep (name "netlink-sys") (req "^0.8") (default-features #t) (target "cfg(target_os = \"android\")") (kind 0)) (crate-dep (name "once_cell") (req "^1") (default-features #t) (target "cfg(target_os = \"android\")") (kind 0)) (crate-dep (name "system-configuration") (req "^0.5.1") (default-features #t) (target "cfg(any(target_os = \"macos\", target_os = \"ios\"))") (kind 0)) (crate-dep (name "windows") (req "^0.48.0") (features (quote ("Win32_Foundation" "Win32_NetworkManagement_IpHelper" "Win32_Networking_WinSock" "Win32_NetworkManagement_Ndis"))) (default-features #t) (target "cfg(windows)") (kind 0)))) (hash "037k0p8gxx0yg2s19b91jcl1f0fgj6dwrxr42cxgc4c59c0m7rfg")))

(define-public crate-default-net-0.17 (crate (name "default-net") (vers "0.17.0") (deps (list (crate-dep (name "dlopen2") (req "^0.5") (target "cfg(target_os = \"android\")") (kind 0)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "memalloc") (req "^0.1.0") (default-features #t) (target "cfg(windows)") (kind 0)) (crate-dep (name "netlink-packet-core") (req "^0.7") (default-features #t) (target "cfg(target_os = \"android\")") (kind 0)) (crate-dep (name "netlink-packet-route") (req "^0.17") (default-features #t) (target "cfg(target_os = \"android\")") (kind 0)) (crate-dep (name "netlink-sys") (req "^0.8") (default-features #t) (target "cfg(target_os = \"android\")") (kind 0)) (crate-dep (name "once_cell") (req "^1") (default-features #t) (target "cfg(target_os = \"android\")") (kind 0)) (crate-dep (name "system-configuration") (req "^0.5.1") (default-features #t) (target "cfg(any(target_os = \"macos\", target_os = \"ios\"))") (kind 0)) (crate-dep (name "windows") (req "^0.48.0") (features (quote ("Win32_Foundation" "Win32_NetworkManagement_IpHelper" "Win32_Networking_WinSock" "Win32_NetworkManagement_Ndis"))) (default-features #t) (target "cfg(windows)") (kind 0)))) (hash "09jm32s4pj2f7bfxpr1j0pdmws1bk39sw8k5hlk5ri00fwzvklfm")))

(define-public crate-default-net-0.18 (crate (name "default-net") (vers "0.18.0") (deps (list (crate-dep (name "dlopen2") (req "^0.5") (target "cfg(target_os = \"android\")") (kind 0)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "memalloc") (req "^0.1.0") (default-features #t) (target "cfg(windows)") (kind 0)) (crate-dep (name "netlink-packet-core") (req "^0.7") (default-features #t) (target "cfg(target_os = \"android\")") (kind 0)) (crate-dep (name "netlink-packet-route") (req "^0.17") (default-features #t) (target "cfg(target_os = \"android\")") (kind 0)) (crate-dep (name "netlink-sys") (req "^0.8") (default-features #t) (target "cfg(target_os = \"android\")") (kind 0)) (crate-dep (name "once_cell") (req "^1") (default-features #t) (target "cfg(target_os = \"android\")") (kind 0)) (crate-dep (name "system-configuration") (req "^0.5.1") (default-features #t) (target "cfg(any(target_os = \"macos\", target_os = \"ios\"))") (kind 0)) (crate-dep (name "windows") (req "^0.48.0") (features (quote ("Win32_Foundation" "Win32_NetworkManagement_IpHelper" "Win32_Networking_WinSock" "Win32_NetworkManagement_Ndis"))) (default-features #t) (target "cfg(windows)") (kind 0)))) (hash "0f2hjmsginwf7d1anr23nj73g9c56jj65zhbayy61nazyi2fczi6")))

(define-public crate-default-net-0.19 (crate (name "default-net") (vers "0.19.0") (deps (list (crate-dep (name "dlopen2") (req "^0.5") (target "cfg(target_os = \"android\")") (kind 0)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "memalloc") (req "^0.1.0") (default-features #t) (target "cfg(windows)") (kind 0)) (crate-dep (name "netlink-packet-core") (req "^0.7") (default-features #t) (target "cfg(target_os = \"android\")") (kind 0)) (crate-dep (name "netlink-packet-route") (req "^0.17") (default-features #t) (target "cfg(target_os = \"android\")") (kind 0)) (crate-dep (name "netlink-sys") (req "^0.8") (default-features #t) (target "cfg(target_os = \"android\")") (kind 0)) (crate-dep (name "once_cell") (req "^1") (default-features #t) (target "cfg(target_os = \"android\")") (kind 0)) (crate-dep (name "serde") (req "^1") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 2)) (crate-dep (name "system-configuration") (req "^0.5.1") (default-features #t) (target "cfg(any(target_os = \"macos\", target_os = \"ios\"))") (kind 0)) (crate-dep (name "windows") (req "^0.48.0") (features (quote ("Win32_Foundation" "Win32_NetworkManagement_IpHelper" "Win32_Networking_WinSock" "Win32_NetworkManagement_Ndis"))) (default-features #t) (target "cfg(windows)") (kind 0)))) (hash "1qmqvzgkw9qzwysy3la55smanhqgvp17733qwx4cwd798hkllza0") (v 2) (features2 (quote (("serde" "dep:serde"))))))

(define-public crate-default-net-0.20 (crate (name "default-net") (vers "0.20.0") (deps (list (crate-dep (name "dlopen2") (req "^0.5") (target "cfg(target_os = \"android\")") (kind 0)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "memalloc") (req "^0.1.0") (default-features #t) (target "cfg(windows)") (kind 0)) (crate-dep (name "netlink-packet-core") (req "^0.7") (default-features #t) (target "cfg(target_os = \"android\")") (kind 0)) (crate-dep (name "netlink-packet-route") (req "^0.17") (default-features #t) (target "cfg(target_os = \"android\")") (kind 0)) (crate-dep (name "netlink-sys") (req "^0.8") (default-features #t) (target "cfg(target_os = \"android\")") (kind 0)) (crate-dep (name "once_cell") (req "^1") (default-features #t) (target "cfg(target_os = \"android\")") (kind 0)) (crate-dep (name "serde") (req "^1") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 2)) (crate-dep (name "system-configuration") (req "^0.5.1") (default-features #t) (target "cfg(any(target_os = \"macos\", target_os = \"ios\"))") (kind 0)) (crate-dep (name "windows") (req "^0.48.0") (features (quote ("Win32_Foundation" "Win32_NetworkManagement_IpHelper" "Win32_Networking_WinSock" "Win32_NetworkManagement_Ndis"))) (default-features #t) (target "cfg(windows)") (kind 0)))) (hash "1gq53vraf4igaagvv5a4znxkhvfz3kmjjbpxcr68byi79bc2k93v") (v 2) (features2 (quote (("serde" "dep:serde"))))))

(define-public crate-default-net-0.21 (crate (name "default-net") (vers "0.21.0") (deps (list (crate-dep (name "dlopen2") (req "^0.5") (target "cfg(target_os = \"android\")") (kind 0)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "memalloc") (req "^0.1.0") (default-features #t) (target "cfg(windows)") (kind 0)) (crate-dep (name "netlink-packet-core") (req "^0.7") (default-features #t) (target "cfg(target_os = \"android\")") (kind 0)) (crate-dep (name "netlink-packet-route") (req "^0.17") (default-features #t) (target "cfg(target_os = \"android\")") (kind 0)) (crate-dep (name "netlink-sys") (req "^0.8") (default-features #t) (target "cfg(target_os = \"android\")") (kind 0)) (crate-dep (name "once_cell") (req "^1") (default-features #t) (target "cfg(target_os = \"android\")") (kind 0)) (crate-dep (name "serde") (req "^1") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 2)) (crate-dep (name "system-configuration") (req "^0.5.1") (default-features #t) (target "cfg(any(target_os = \"macos\", target_os = \"ios\"))") (kind 0)) (crate-dep (name "windows") (req "^0.48.0") (features (quote ("Win32_Foundation" "Win32_NetworkManagement_IpHelper" "Win32_Networking_WinSock" "Win32_NetworkManagement_Ndis"))) (default-features #t) (target "cfg(windows)") (kind 0)))) (hash "1sskiryld57f2l7qanvrlh5rfv0gdp969p34mn33qv9lv1v7bp45") (v 2) (features2 (quote (("serde" "dep:serde"))))))

(define-public crate-default-net-0.22 (crate (name "default-net") (vers "0.22.0") (deps (list (crate-dep (name "dlopen2") (req "^0.5") (target "cfg(target_os = \"android\")") (kind 0)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "memalloc") (req "^0.1.0") (default-features #t) (target "cfg(windows)") (kind 0)) (crate-dep (name "netlink-packet-core") (req "^0.7") (default-features #t) (target "cfg(target_os = \"android\")") (kind 0)) (crate-dep (name "netlink-packet-route") (req "^0.17") (default-features #t) (target "cfg(target_os = \"android\")") (kind 0)) (crate-dep (name "netlink-sys") (req "^0.8") (default-features #t) (target "cfg(target_os = \"android\")") (kind 0)) (crate-dep (name "once_cell") (req "^1") (default-features #t) (target "cfg(target_os = \"android\")") (kind 0)) (crate-dep (name "serde") (req "^1") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 2)) (crate-dep (name "system-configuration") (req "^0.5.1") (default-features #t) (target "cfg(any(target_os = \"macos\", target_os = \"ios\"))") (kind 0)) (crate-dep (name "windows") (req "^0.48.0") (features (quote ("Win32_Foundation" "Win32_NetworkManagement_IpHelper" "Win32_Networking_WinSock" "Win32_NetworkManagement_Ndis"))) (default-features #t) (target "cfg(windows)") (kind 0)))) (hash "0rnafc5z04wbr4qzsqkmv761xjlssqbcbcqdn54lsd88m5lnanhc") (v 2) (features2 (quote (("serde" "dep:serde"))))))

(define-public crate-default-option-arr-0.1 (crate (name "default-option-arr") (vers "0.1.0") (hash "1dv0c9slcykq0liq0s136qvhbiz13a1azgqqw38s3jcpmaxdpgah")))

(define-public crate-default-option-arr-0.1 (crate (name "default-option-arr") (vers "0.1.1") (hash "1pqf2swq10dqfvvli7ii1jccxccvjcnm3jd7wqi6ldq6ggrszkld")))

(define-public crate-default-port-1 (crate (name "default-port") (vers "1.0.0") (hash "07s1xnyimrx0b7di70ji8s946xyiv7l16934vw0jkjgqy3j553av")))

(define-public crate-default-port-2 (crate (name "default-port") (vers "2.0.0") (hash "0673aqcp2j4jfk3j9lhk6i0ikllf47j8ycd4pihn8wrjbwb5ikv1")))

(define-public crate-default-port-2 (crate (name "default-port") (vers "2.1.0") (hash "08ap8hgismhm03vcp25wqxkai3in8zicwn4fbjd6cqd6dbzi840h")))

(define-public crate-default-struct-builder-0.1 (crate (name "default-struct-builder") (vers "0.1.0") (deps (list (crate-dep (name "darling") (req "^0.20") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2.0") (default-features #t) (kind 0)))) (hash "03s6dw502p9x22bhgzvz3p1ic4khl6b4cd8bm115wjz0m6pzksjn")))

(define-public crate-default-struct-builder-0.2 (crate (name "default-struct-builder") (vers "0.2.0") (deps (list (crate-dep (name "darling") (req "^0.20") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2.0") (default-features #t) (kind 0)))) (hash "1vahh6c514b304dr63mzzbm5sjikd9mjx72byvbgbnqpllfbmhm2")))

(define-public crate-default-struct-builder-0.2 (crate (name "default-struct-builder") (vers "0.2.1") (deps (list (crate-dep (name "darling") (req "^0.20") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2.0") (default-features #t) (kind 0)))) (hash "1p09si6skqqnvbr1h2h2s23lwn5naggyy627ama4svlrfjqb88aw")))

(define-public crate-default-struct-builder-0.3 (crate (name "default-struct-builder") (vers "0.3.0") (deps (list (crate-dep (name "darling") (req "^0.20") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2.0") (default-features #t) (kind 0)))) (hash "11j9w98camx0s86ypjrf5lr6x2zjlgvcwf6k7isg11nvjmz06945")))

(define-public crate-default-struct-builder-0.4 (crate (name "default-struct-builder") (vers "0.4.0") (deps (list (crate-dep (name "darling") (req "^0.20") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2.0") (default-features #t) (kind 0)))) (hash "1ikz7gmccxhl6ib21jgzp9z027asmaihkvd1agh38jpm3yx0yi45")))

(define-public crate-default-struct-builder-0.4 (crate (name "default-struct-builder") (vers "0.4.1") (deps (list (crate-dep (name "darling") (req "^0.20") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2.0") (default-features #t) (kind 0)))) (hash "1i9jfia5s75c812iw7bmmgy62q2a1s4m8byhycgy3dkspsddq47f")))

(define-public crate-default-struct-builder-0.4 (crate (name "default-struct-builder") (vers "0.4.2") (deps (list (crate-dep (name "darling") (req "^0.20") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2.0") (default-features #t) (kind 0)))) (hash "0jlvdsbqdww031mwncjr1szammqlwqywazhjhwjcyqdwkgj5v4sd")))

(define-public crate-default-struct-builder-0.5 (crate (name "default-struct-builder") (vers "0.5.0") (deps (list (crate-dep (name "darling") (req "^0.20") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2.0") (default-features #t) (kind 0)))) (hash "055ccqvf9811r0ry635anzij2fgp66kzglalawglkzdqjvd91ypq")))

(define-public crate-default-target-0.1 (crate (name "default-target") (vers "0.1.0") (hash "1vq4n9fzsslshwpppv80g6iyynl4ndapgnriy607p7z6p44ll9ix")))

(define-public crate-default-test-0.1 (crate (name "default-test") (vers "0.1.0") (hash "08zwqh5j81x9s8h5z2smcnqh7pkvj89hgy5lc0dyz88hj6lqw21y")))

(define-public crate-default-test-0.1 (crate (name "default-test") (vers "0.1.1") (hash "1w6mknv9ykfsr1sg9g30y01rqhskg822az35wcmri9rlx8fmlzs6")))

(define-public crate-default-vec-0.1 (crate (name "default-vec") (vers "0.1.0") (hash "1fs23n3mpfqmx5hlvp3m84814qr90qp060qrnhszwjzkyvq1zzvl")))

(define-public crate-default-vec-0.1 (crate (name "default-vec") (vers "0.1.1") (hash "1v7wyaikp2h52j75wzi89zdj75j7sbhr5h1h585w575qk2pgin3h")))

(define-public crate-default-vec-0.2 (crate (name "default-vec") (vers "0.2.0") (hash "0dsdhaqbmcc0hzd7d5mzzmvkfq68rmhxyzws0nwi7d59778wgwsd")))

(define-public crate-default-with-0.1 (crate (name "default-with") (vers "0.1.0") (hash "1kvlrxdygsjklvx5d218p5z44kqv2qi2w4qz3awvj0s16x6qas8a")))

(define-public crate-default-with-0.1 (crate (name "default-with") (vers "0.1.1") (hash "1vnrbjln7ahnd6frhjpamhgn95ydcpq0gwm10g8wbfa4wbnqn865")))

(define-public crate-default2-0.1 (crate (name "default2") (vers "0.1.0") (deps (list (crate-dep (name "proc-macro2") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1") (default-features #t) (kind 0)))) (hash "0bq5f6pyscyaz0pznmjj47aqyxq28n1lmi3gd6rhcwkw4ch30nr5")))

(define-public crate-default2-0.1 (crate (name "default2") (vers "0.1.1") (deps (list (crate-dep (name "proc-macro2") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1") (default-features #t) (kind 0)))) (hash "05i8sb5bbc5d27hjd9d6l2nw868mwfxl0z152x72waxwvb9ghsv9")))

(define-public crate-default2-0.2 (crate (name "default2") (vers "0.2.0") (deps (list (crate-dep (name "proc-macro2") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "0qay6d79lhkayw2q9yaj5d77ds071z4a21mvn9jyflsqbzxm6zz9")))

(define-public crate-default2-0.2 (crate (name "default2") (vers "0.2.1") (deps (list (crate-dep (name "proc-macro2") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "1lqcgmavv4dzdl69iwjdqabv3vn32dsx2bginnpa3n681zam1w43")))

(define-public crate-default2-0.3 (crate (name "default2") (vers "0.3.0") (deps (list (crate-dep (name "proc-macro2") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "10ww7961jmrnmms96r7b234519l1m3hl579713pf95f3z2p64n6l")))

(define-public crate-default2-0.3 (crate (name "default2") (vers "0.3.1") (deps (list (crate-dep (name "proc-macro2") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "1nabdwilmsa8klz2s4br8nxm7vsbszjha5xv7xdl0vzq1dl547z9")))

(define-public crate-default_allocator-0.1 (crate (name "default_allocator") (vers "0.1.0") (deps (list (crate-dep (name "loca") (req "^0.4") (default-features #t) (kind 0)))) (hash "12zxxz953kbmiagq0bw71r1jh1xl0x3m51plbhbp4zzl8y2y2366")))

(define-public crate-default_allocator-0.1 (crate (name "default_allocator") (vers "0.1.1") (deps (list (crate-dep (name "loca") (req ">= 0.4, < 0.6") (default-features #t) (kind 0)))) (hash "1rlxcqj8vw7gah6c4lqcv2w8fja4x4ibdljzsviz0ffnbs8vania")))

(define-public crate-default_allocator-0.1 (crate (name "default_allocator") (vers "0.1.2") (deps (list (crate-dep (name "loca") (req "^0.5.4") (default-features #t) (kind 0)))) (hash "003mpzh8ni3lg8vpkmfpsbbcqcgmsh1dq5g905bx7c2i3amdhmrd") (features (quote (("stable-rust" "loca/stable-rust"))))))

(define-public crate-default_allocator-0.2 (crate (name "default_allocator") (vers "0.2.0") (deps (list (crate-dep (name "loca") (req "^0.6") (default-features #t) (kind 0)))) (hash "10sr22ffs5hsn21iqi52p5sb61zq5v97ac8p4vvxnr83hgf4al8a")))

(define-public crate-default_allocator-0.2 (crate (name "default_allocator") (vers "0.2.1") (deps (list (crate-dep (name "loca") (req "^0.6") (default-features #t) (kind 0)))) (hash "1n2iy42hdi9vi3v9svmfj60plsirxrdszkcxzs6981051ydd6j75")))

(define-public crate-default_allocator-0.2 (crate (name "default_allocator") (vers "0.2.2") (deps (list (crate-dep (name "loca") (req "^0.6") (default-features #t) (kind 0)))) (hash "1v3r3mdj9zc5y4sik6ppkh34553g6s8613p070n0prm6x20p3wwa")))

(define-public crate-default_allocator-0.2 (crate (name "default_allocator") (vers "0.2.3") (deps (list (crate-dep (name "loca") (req "^0.6") (default-features #t) (kind 0)))) (hash "1xwv6idw6yhhr0w2v9469k60w5sf09gbg7ici0800253xbh8wqnx")))

(define-public crate-default_allocator-0.3 (crate (name "default_allocator") (vers "0.3.0") (deps (list (crate-dep (name "loca") (req "^0.7") (default-features #t) (kind 0)))) (hash "08pkq47aq9h2y1b005cl5zl9mss8yb3c7r1siarvw9f5nm6ra20h")))

(define-public crate-default_aware-0.1 (crate (name "default_aware") (vers "0.1.0") (deps (list (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 2)))) (hash "1l1vhvsnmzvaf999ziljkf1j0w3zzbsljss4y1kybild7v64n978") (features (quote (("default" "serde")))) (v 2) (features2 (quote (("serde" "dep:serde"))))))

(define-public crate-default_aware-0.2 (crate (name "default_aware") (vers "0.2.0") (deps (list (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 2)))) (hash "0vxz3a284bysr2l67ia5m81c1yg4z34498jkc26v7b47q809g906") (features (quote (("default" "serde")))) (v 2) (features2 (quote (("serde" "dep:serde"))))))

(define-public crate-default_kwargs-0.1 (crate (name "default_kwargs") (vers "0.1.0") (deps (list (crate-dep (name "proc-macro2") (req "^1.0.36") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.17") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.90") (features (quote ("full" "extra-traits" "parsing"))) (default-features #t) (kind 0)) (crate-dep (name "trybuild") (req "^1.0.58") (default-features #t) (kind 0)) (crate-dep (name "trybuild") (req "^1.0.58") (features (quote ("diff"))) (default-features #t) (kind 2)))) (hash "06ri89n98z6g3g8qxbjmbm889ym1i9cq725bgyarbk6fjkzcy175")))

(define-public crate-default_macro-0.1 (crate (name "default_macro") (vers "0.1.0") (hash "0val1g74s1ixiryry4a5bcc4jlixsdk700f4n5f8z5c301f2js70")))

(define-public crate-default_macro-0.2 (crate (name "default_macro") (vers "0.2.0") (hash "0f3i2v6bi0g67xwfzrnkgbln1kg8x4273xzrjmchwm8ixjmb6q96")))

(define-public crate-default_macro-0.2 (crate (name "default_macro") (vers "0.2.1") (hash "0pj2zy70pdn1yafs3vs6jsggnnxg0khn23i6jxj5zi591mv2mxvm")))

(define-public crate-default_variant-0.1 (crate (name "default_variant") (vers "0.1.0") (deps (list (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.9") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.77") (features (quote ("extra-traits" "full"))) (default-features #t) (kind 0)))) (hash "1v5884x5zygjsphl6xv1mqh5dimgxky58z5kh2sf81cki4v0ks22")))

(define-public crate-defaultdict-0.1 (crate (name "defaultdict") (vers "0.1.0") (hash "0vwzdydy2rjp05cyjivv8d749zqyxlwbhlwba9x7m3wqygs9cp15")))

(define-public crate-defaultdict-0.2 (crate (name "defaultdict") (vers "0.2.0") (hash "1z7vazapzaxkf9pvq17bj8nh8hc6qsjk8d92sxm05kvspidk2mys")))

(define-public crate-defaultdict-0.4 (crate (name "defaultdict") (vers "0.4.0") (hash "1s5h579agy66g8pl9znklraqmyw8m33snzzlzpkhm80g33bxjazr")))

(define-public crate-defaultdict-0.5 (crate (name "defaultdict") (vers "0.5.0") (hash "0ydwv5cx3miari4w2bmf2zvncgs5wgzblq9q817gm9xcpi8z9wsm")))

(define-public crate-defaultdict-0.6 (crate (name "defaultdict") (vers "0.6.0") (hash "0wn1m6ydhnwbh1k3glnqzp8r6p33j7y7kclwfnsi8g9kv0zvx57p")))

(define-public crate-defaultdict-0.7 (crate (name "defaultdict") (vers "0.7.0") (hash "1amb5sqpdix3cxxkh1npwr98720bf2i3fbjbi06ji8dirknnrv22")))

(define-public crate-defaultdict-0.7 (crate (name "defaultdict") (vers "0.7.1") (hash "1mi9saajdll5bril67dc474wkd2l2yx62l37iyf3ixyfq61jm27x")))

(define-public crate-defaultdict-0.8 (crate (name "defaultdict") (vers "0.8.0") (hash "0600h1sps6ir43akf0z82dljn4f4knzwgz8xvc7zi1l1gd459m8i")))

(define-public crate-defaultdict-0.9 (crate (name "defaultdict") (vers "0.9.0") (hash "1lxrh8rc0yv9wqzm78lm1cb9py34p6d0i1zzxsikhzl8g42h0n52")))

(define-public crate-defaultdict-0.10 (crate (name "defaultdict") (vers "0.10.0") (hash "03cqpzcv87z6abkv8mr3jwnrxgvzaipn64q92q23m3l9z5b4q2g5")))

(define-public crate-defaultdict-0.11 (crate (name "defaultdict") (vers "0.11.0") (hash "04dl4h12mdbrz36h17fcs5c4vwfxhj1sbq79h6hslxkf8ac9zmnl")))

(define-public crate-defaultdict-0.12 (crate (name "defaultdict") (vers "0.12.0") (hash "1w2js06rqg2ir6smk6vnx622dchwny8ahz29nvnaymk63ypachvd")))

(define-public crate-defaultdict-0.12 (crate (name "defaultdict") (vers "0.12.1") (hash "10cfs3awv7kc9l9h2sglgxvb52lnz7h5ss5lxli8hxw5ckiir3vg")))

(define-public crate-defaultdict-0.13 (crate (name "defaultdict") (vers "0.13.0") (hash "15kgvvqy9lpgr82x3gaq8abb1zddc9sk5q50csgkz4irim9wvi1k")))

(define-public crate-defaultdict-0.14 (crate (name "defaultdict") (vers "0.14.0") (hash "0x20kdzxgav8x1rnd5l2zv1g1vj7b1bjzkz7g7dakdfc7axlnbl8")))

(define-public crate-defaultdict-0.15 (crate (name "defaultdict") (vers "0.15.0") (hash "00wig7hzq2xkh45dc8g4ivwx6h1yjfsix27hqr3s7ic4nl2x9sx2")))

(define-public crate-defaultdict-0.16 (crate (name "defaultdict") (vers "0.16.0") (hash "1chc2r5mn937lnwprhgkqrdiz5m25pv2njhr9gv9ak1i4ln53kij")))

(define-public crate-defaultdict-0.16 (crate (name "defaultdict") (vers "0.16.1") (hash "05qpfnyl86vczsg2yv3hs6jjqvrh0gd8nbjpjbdx7yrm8x0qr50s")))

(define-public crate-defaultdict-0.17 (crate (name "defaultdict") (vers "0.17.0") (hash "1pz053fhr10y03yph3wq50fpgb8xmgqrzfi52nq7mdmaj0kpzr6x")))

(define-public crate-defaultdict-0.18 (crate (name "defaultdict") (vers "0.18.0") (hash "1z8nqirpfc78qpcfgpvvznmnxd370v11hbwdws3rrc96ajsxxhz2")))

(define-public crate-defaultmap-0.1 (crate (name "defaultmap") (vers "0.1.0") (deps (list (crate-dep (name "delegatemethod") (req "^0.2.2") (default-features #t) (kind 0)))) (hash "11zfjxiip3vigrydagfbvs0w4ras0cg21g9qb5wgi3jwq5ldnyxc")))

(define-public crate-defaultmap-0.1 (crate (name "defaultmap") (vers "0.1.1") (hash "1b0wpq59i6zbgbhkijfjhr5vsdjnmpx2ajnvianzngmk56201bb5")))

(define-public crate-defaultmap-0.1 (crate (name "defaultmap") (vers "0.1.2") (hash "1djrlqpz5msk1adwsw3ygdpbn04l9169a0wymw0kn3bmdiby7mf6")))

(define-public crate-defaultmap-0.1 (crate (name "defaultmap") (vers "0.1.3") (hash "0f8x5x3rvk0q1jpipa5adck2my94qwxqwhyx200qn1pg3lrp7w4j")))

(define-public crate-defaultmap-0.2 (crate (name "defaultmap") (vers "0.2.0") (hash "0d8d6v4rkq7isdkga4kyzxknhspdzy9r782fyxi34nxh9qifarmw")))

(define-public crate-defaultmap-0.2 (crate (name "defaultmap") (vers "0.2.1") (hash "0c1f95d70z5vfbrmjzmv3hr4sg16sz5c5gikypxaz21wmafycylk")))

(define-public crate-defaultmap-0.2 (crate (name "defaultmap") (vers "0.2.2") (hash "0nzrm6kalka3fbh17xlmipayc08sdhwcjh9yx1xydzalhawvacpd") (yanked #t)))

(define-public crate-defaultmap-0.3 (crate (name "defaultmap") (vers "0.3.0") (hash "0mssibgy02qacz6cixzrchk9g1y1x7qlwxjy5saqzd3938b3126k")))

(define-public crate-defaultmap-0.3 (crate (name "defaultmap") (vers "0.3.1") (hash "1cja4x6bklhl2axj51yvpkh72ffq0d8ksbvdgwrb5704lfjlrjj2")))

(define-public crate-defaultmap-0.4 (crate (name "defaultmap") (vers "0.4.0") (hash "016qig0paa5swmfrkqz7anjc95pniay2czwafp32a8h89fhs25wg")))

(define-public crate-defaultmap-0.5 (crate (name "defaultmap") (vers "0.5.0") (deps (list (crate-dep (name "serde") (req "^1.0.0") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 2)))) (hash "0d78s14v64w0ly20j6zb2nkharc24jq9gcl6zync6qb5svxx9i2i") (features (quote (("with-serde" "serde") ("default"))))))

(define-public crate-defaultmap-0.6 (crate (name "defaultmap") (vers "0.6.0") (deps (list (crate-dep (name "derive_more") (req "=1.0.0-beta.3") (features (quote ("debug"))) (default-features #t) (kind 0)) (crate-dep (name "dyn-clone") (req "^1.0.13") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.183") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.105") (default-features #t) (kind 2)))) (hash "1hzp5cdn170gnmaxvq3rfx25jnqy5hcf72w9alpq99kg7rjggy8m") (features (quote (("default")))) (v 2) (features2 (quote (("with-serde" "dep:serde")))) (rust-version "1.71.0")))

(define-public crate-defaultmgr-0.5 (crate (name "defaultmgr") (vers "0.5.0") (deps (list (crate-dep (name "clap") (req "^4.0.15") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "dirs") (req "^4.0.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.152") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.91") (default-features #t) (kind 0)))) (hash "13msh2qwlw5bl3z05037jjh019whiv07xl1bj02jbrq488jzvn70") (yanked #t)))

(define-public crate-defaults-0.1 (crate (name "defaults") (vers "0.1.0") (deps (list (crate-dep (name "proc-macro2") (req "^1.0.6") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.2") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.11") (features (quote ("full"))) (default-features #t) (kind 0)) (crate-dep (name "trybuild") (req "^1.0") (default-features #t) (kind 2)))) (hash "11sd26bpqvkpmzlqlp50zvw9574fw2qmy83hgvsbffqfz919rccr")))

(define-public crate-defaults-0.2 (crate (name "defaults") (vers "0.2.0") (deps (list (crate-dep (name "proc-macro2") (req "^1.0.6") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.2") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.11") (features (quote ("full"))) (default-features #t) (kind 0)) (crate-dep (name "trybuild") (req "^1.0") (default-features #t) (kind 2)))) (hash "0rx2kddbkaf0p0is36ggail0zklb20cqlm9sn8na46prchymbsa0")))

