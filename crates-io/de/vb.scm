(define-module (crates-io de vb) #:use-module (crates-io))

(define-public crate-devbit_99bugs_led_display_driver-0.2 (crate (name "devbit_99bugs_led_display_driver") (vers "0.2.1") (deps (list (crate-dep (name "spidev") (req "^0.5.0") (default-features #t) (kind 0)))) (hash "1pjhvgf0r2zpkccyhm3qvr077s2q2b61zljw24yvmvpld4qib1ay")))

(define-public crate-devboat-docker-0.1 (crate (name "devboat-docker") (vers "0.1.0") (deps (list (crate-dep (name "unix_socket") (req "^0.4.3") (default-features #t) (kind 0)))) (hash "1y9pq2r664jgqsb8r03h1r79f0j9hbiza8aqwjp3q6y5w3cxpb8p")))

(define-public crate-devbox-0.1 (crate (name "devbox") (vers "0.1.0") (deps (list (crate-dep (name "devbox-build") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "devbox-test-args") (req "^0.1.2") (default-features #t) (kind 0)))) (hash "186qf2zkb2xmdcghxq3mncg9f6vksxa2zq6bbvcrq6l7zhsws1i8")))

(define-public crate-devbox-0.1 (crate (name "devbox") (vers "0.1.1") (deps (list (crate-dep (name "devbox-build") (req "^0.1.1") (default-features #t) (kind 0)) (crate-dep (name "devbox-test-args") (req "^0.1.2") (default-features #t) (kind 0)))) (hash "15r74yg1yvr2305kdcanq2a68vr1i37x00q7k7khn74rj5vbv90c")))

(define-public crate-devbox-build-0.1 (crate (name "devbox-build") (vers "0.1.0") (deps (list (crate-dep (name "devbox-test-args") (req "^0.1.0") (default-features #t) (kind 2)) (crate-dep (name "filetime") (req "^0.2.7") (default-features #t) (kind 0)) (crate-dep (name "globset") (req "^0.4.4") (default-features #t) (kind 0)) (crate-dep (name "tempfile") (req "^3") (default-features #t) (kind 2)) (crate-dep (name "walkdir") (req "^2") (default-features #t) (kind 0)))) (hash "17h4riicp3p85hd00pnvhnzy94w6ar5zvccnv10afydz6zb2ssyz")))

(define-public crate-devbox-build-0.1 (crate (name "devbox-build") (vers "0.1.1") (deps (list (crate-dep (name "devbox-test-args") (req "^0.1.0") (default-features #t) (kind 2)) (crate-dep (name "filetime") (req "^0.2.7") (default-features #t) (kind 0)) (crate-dep (name "globset") (req "^0.4.4") (default-features #t) (kind 0)) (crate-dep (name "tempfile") (req "^3") (default-features #t) (kind 2)) (crate-dep (name "walkdir") (req "^2") (default-features #t) (kind 0)))) (hash "0fxfbv49qrz41d5scrx9dyl16621gw96zlz0izcy94i0gaa62dbg")))

(define-public crate-devbox-env-0.1 (crate (name "devbox-env") (vers "0.1.0") (hash "1j638yadpw155gjivnqq8frblj6h9bmr0r1l5vnnl5p2j8v2i701")))

(define-public crate-devbox-env-1 (crate (name "devbox-env") (vers "1.1.0") (hash "1k78a10wzhxhh3za5dpa0s3ssaky5ks9kbjg8p10jmh4pbsy5h4m")))

(define-public crate-devbox-env-1 (crate (name "devbox-env") (vers "1.1.1") (hash "0bbdxcxx95kzvkzsm9zjnczkh3kwff71in2ngnx6lviiqzgck5ql")))

(define-public crate-devbox-test-args-0.1 (crate (name "devbox-test-args") (vers "0.1.0") (deps (list (crate-dep (name "proc-macro-error") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "1iw1fdfmr91z47jp2gds7lpfa94q9sjl1j833y6kijsby46a8hhk")))

(define-public crate-devbox-test-args-0.1 (crate (name "devbox-test-args") (vers "0.1.1") (deps (list (crate-dep (name "proc-macro-error") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "17h0q5nvlypgi5x30b59qzr1q5khydv99lfncv4aibcl3kp4wigp")))

(define-public crate-devbox-test-args-0.1 (crate (name "devbox-test-args") (vers "0.1.2") (deps (list (crate-dep (name "proc-macro-error") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "0pf8ngfcqv3jcxgaqx8yahk133g88w4qrvmmbvdlkp5kxhfqd4nq")))

