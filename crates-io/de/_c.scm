(define-module (crates-io de _c) #:use-module (crates-io))

(define-public crate-de_camera-0.0.1 (crate (name "de_camera") (vers "0.0.1") (hash "0v41z2rqdqg17h3nw18v9v6l03rbpd5sfl6ibr2vh1prfmf7q2ql")))

(define-public crate-de_controller-0.0.1 (crate (name "de_controller") (vers "0.0.1") (hash "0ym6wg48x34s4xi7cvw072517fkfhx631zg8a77vf8zzfkn2zlrw")))

(define-public crate-de_core-0.0.1 (crate (name "de_core") (vers "0.0.1") (hash "0n6ldck8rx16225af56hzqsc1rmwj5rp0ny93cl5lwdwh5mld403")))

