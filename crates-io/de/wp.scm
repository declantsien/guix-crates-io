(define-module (crates-io de wp) #:use-module (crates-io))

(define-public crate-dewpoint-0.1 (crate (name "dewpoint") (vers "0.1.0") (deps (list (crate-dep (name "reqwest") (req "^0.8.7") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.71") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0.71") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.26") (default-features #t) (kind 0)))) (hash "1zlsbv0ymn8a029640s5rw121mfs0srzpr25iv2dkk0vkjk3hhjj") (yanked #t)))

