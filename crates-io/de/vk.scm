(define-module (crates-io de vk) #:use-module (crates-io))

(define-public crate-devker-0.1 (crate (name "devker") (vers "0.1.0") (hash "13zv5wd194515fd9dz13061p6r279iykl108d8qckarni26i4cwn")))

(define-public crate-devker-0.1 (crate (name "devker") (vers "0.1.1") (hash "1ijhqza7b2mipaqb136dn47qbmh8iahjmr693i448lnf6cimg45w")))

(define-public crate-devker-0.1 (crate (name "devker") (vers "0.1.2") (hash "07vy07adm68n1ayrgksn3hhhargmk5n5biv0h9m0lrkj35giyh4y")))

(define-public crate-devker-0.1 (crate (name "devker") (vers "0.1.3") (hash "0vjzwq6axbydirdqrvli8irlb3p9wjh5a3ijy8qg36nkpg6fsarj")))

(define-public crate-devker-0.1 (crate (name "devker") (vers "0.1.4") (hash "1mffwd5qjgzdpa6bbymzrjyilp9736n5hi3k5lql5psp1lj0sd6y")))

(define-public crate-devker-0.1 (crate (name "devker") (vers "0.1.5") (hash "025xd7ihnw29nvj4g8cky57935xi9z65gz2rpx9hyg6pycplcmzy")))

(define-public crate-devker-0.1 (crate (name "devker") (vers "0.1.6") (hash "16ryxlp639bc331d5rr993ffn3r1fvyj4b4n72fby03gs1b9pwnm")))

