(define-module (crates-io de l_) #:use-module (crates-io))

(define-public crate-del_lib-0.1 (crate (name "del_lib") (vers "0.1.0") (deps (list (crate-dep (name "filetime") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "ring") (req "^0.16") (default-features #t) (kind 0)))) (hash "0b6f47391w7x3p7plbshg7dl1dl8nq2xwvxa74c5ff40hfn82h8x")))

(define-public crate-del_line-0.1 (crate (name "del_line") (vers "0.1.0") (hash "0zy6n8ymspm9a28gdziry3lnv7z3ax8844856d3id3gsnc5aykvh")))

(define-public crate-del_line-0.1 (crate (name "del_line") (vers "0.1.1") (hash "1l6vzshjm7srszld406g19za252qjp40d3n072ak9v6f2m96k5ma")))

