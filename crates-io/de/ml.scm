(define-module (crates-io de ml) #:use-module (crates-io))

(define-public crate-deml-0.1 (crate (name "deml") (vers "0.1.0") (deps (list (crate-dep (name "anyhow") (req "^1.0.75") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^4.4.4") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "dagrs") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "pest") (req "^2.7.3") (default-features #t) (kind 0)) (crate-dep (name "pest_derive") (req "^2.7.3") (default-features #t) (kind 0)))) (hash "0zc5kzw88wrc25n69caqpzgk4gbczb8hjmhr68ispibybdpnsbvz")))

