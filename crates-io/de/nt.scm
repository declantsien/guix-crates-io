(define-module (crates-io de nt) #:use-module (crates-io))

(define-public crate-dent-0.1 (crate (name "dent") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^2.26.1") (default-features #t) (kind 0)))) (hash "1dwi7w9hjdk8xvk8gzdf4z5jwxkr11h4pmvbi8gjas98l10848gb")))

(define-public crate-dent-0.2 (crate (name "dent") (vers "0.2.0") (deps (list (crate-dep (name "clap") (req "^2.26.1") (default-features #t) (kind 0)) (crate-dep (name "stamp") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "term_size") (req "^0.3.0") (default-features #t) (kind 0)))) (hash "17l6fk7vgl2j7bs5f5izdncjldz376k0h4pv5sj323b0ps81c68n")))

(define-public crate-dent-0.3 (crate (name "dent") (vers "0.3.0") (deps (list (crate-dep (name "clap") (req "^2.26.1") (default-features #t) (kind 0)) (crate-dep (name "stamp") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "term_size") (req "^0.3.0") (default-features #t) (kind 0)))) (hash "08m1xzwwsjkirr497gm6vkswdfcyy8gi49i54izc0d54lbmw546p")))

(define-public crate-dent-0.4 (crate (name "dent") (vers "0.4.0") (deps (list (crate-dep (name "clap") (req "^2.26.1") (default-features #t) (kind 0)) (crate-dep (name "stamp") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "term") (req "^0.4.6") (default-features #t) (kind 0)) (crate-dep (name "term_size") (req "^0.3.0") (default-features #t) (kind 0)))) (hash "0msywap0xfidjac90cm0vq9fxbsjxa9lgqxqglsmjprv5zh8982x")))

(define-public crate-dent-0.4 (crate (name "dent") (vers "0.4.1") (deps (list (crate-dep (name "clap") (req "^2.26.1") (default-features #t) (kind 0)) (crate-dep (name "stamp") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "term") (req "^0.4.6") (default-features #t) (kind 0)) (crate-dep (name "term_size") (req "^0.3.0") (default-features #t) (kind 0)))) (hash "06nvqdxf5lbaxf0racjf18g3x1pbhhj3rnl701wf6jrnspqq4wkz")))

(define-public crate-dent-parse-0.1 (crate (name "dent-parse") (vers "0.1.0") (hash "0073aga0lvi7nbmcp8023xwy6a5dbpfy9csza1akgnrvfwvs08qb")))

(define-public crate-dental-notation-1 (crate (name "dental-notation") (vers "1.0.0") (deps (list (crate-dep (name "owo-colors") (req "^3") (default-features #t) (kind 0)))) (hash "1yw8ij1322dlij8j945pqkxycjs52p2g6l75pkb5hk0fqhnhph53")))

(define-public crate-dental-notation-cli-0.1 (crate (name "dental-notation-cli") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^3.2.5") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "dental-notation") (req "^1.0.0") (default-features #t) (kind 0)))) (hash "03615qchpbcf45p8gcqf9s4p9n99fkgwd2qjg4lvjx4xg2nklmn5")))

(define-public crate-dentist-0.1 (crate (name "dentist") (vers "0.1.0") (hash "016a3dgjpnlqwn2si737srb4id3l1qp2nrwbcy85cs2yllqkr2w8")))

