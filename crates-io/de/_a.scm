(define-module (crates-io de _a) #:use-module (crates-io))

(define-public crate-de_attacking-0.1 (crate (name "de_attacking") (vers "0.1.0-dev") (hash "1v4rpb4anlmrqq42qvx9llmxg0dkfvk36jza8vzlp7gw0safdp1c") (yanked #t)))

(define-public crate-de_attacking-0.0.1 (crate (name "de_attacking") (vers "0.0.1") (hash "0pxcyg0452v0kx97hkzyvliq7ihxv0hwayzkzga55sx7867a7yh6")))

