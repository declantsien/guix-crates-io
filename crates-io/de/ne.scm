(define-module (crates-io de ne) #:use-module (crates-io))

(define-public crate-deneb-0.0.0 (crate (name "deneb") (vers "0.0.0") (hash "05ywm107q2761s34qvk9p55wpglbmwbxr9n3y91f300bns2jjzgn")))

(define-public crate-deneb-core-0.0.0 (crate (name "deneb-core") (vers "0.0.0") (hash "1rmy378mwlprbwgv8xpp4mwhfi3axmclk04fm3kzkz2qzqw3631x")))

(define-public crate-deneb-fuse-0.0.0 (crate (name "deneb-fuse") (vers "0.0.0") (hash "1a96j2gg13zaw7b6mcn0lvcf1hbjd41k3ivy1pvjy7dg8nas9ndq")))

