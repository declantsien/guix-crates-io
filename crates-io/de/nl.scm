(define-module (crates-io de nl) #:use-module (crates-io))

(define-public crate-denlibs-0.1 (crate (name "denlibs") (vers "0.1.0") (hash "0dnr77ssyba1kdmj2jirzmhr5lkbzx07smxx66my3z9ns1qx83rf") (yanked #t)))

(define-public crate-denlibs-0.1 (crate (name "denlibs") (vers "0.1.1") (hash "1kn27jjz5zjs95a4bcpbi6n0fpaqgkqkhs1nbz15i9gpybbbxas8") (yanked #t)))

(define-public crate-denlibs-0.1 (crate (name "denlibs") (vers "0.1.2") (hash "0xsi3dx87mmrvyr2h61blbrg90k2zb889v1rv9jr18vnz87qzfck") (yanked #t)))

(define-public crate-denlibs-0.1 (crate (name "denlibs") (vers "0.1.3") (deps (list (crate-dep (name "chrono") (req "^0.4.25") (default-features #t) (kind 0)) (crate-dep (name "quad-rand") (req "^0.2.1") (default-features #t) (kind 0)))) (hash "1vbhk52jm5qb3zq2ihbwxs3gpn8azqw9rkf1pxyprhln0ymfv5p7") (yanked #t)))

(define-public crate-denlibs-0.1 (crate (name "denlibs") (vers "0.1.4") (deps (list (crate-dep (name "chrono") (req "^0.4.25") (default-features #t) (kind 0)) (crate-dep (name "quad-rand") (req "^0.2.1") (default-features #t) (kind 0)))) (hash "0sjy16ypcprrph7z4zjqsq5qq361kpv7891lycjrsmqdprpwazaz")))

(define-public crate-denlibs-0.1 (crate (name "denlibs") (vers "0.1.5") (deps (list (crate-dep (name "chrono") (req "^0.4.25") (default-features #t) (kind 0)) (crate-dep (name "quad-rand") (req "^0.2.1") (default-features #t) (kind 0)))) (hash "1v59gfn9a6qjhk0d53x3s4qkl4xwsy6r6nkdaiy7jz3dk5ab290x")))

(define-public crate-denlibs-0.1 (crate (name "denlibs") (vers "0.1.6") (deps (list (crate-dep (name "quad-rand") (req "^0.2.1") (default-features #t) (kind 0)))) (hash "0azgbyyzbv6x9ldg7gmiaxpn5230367w5jx52fx5c7jq0vrr10k0")))

(define-public crate-denlibs-0.1 (crate (name "denlibs") (vers "0.1.7") (deps (list (crate-dep (name "quad-rand") (req "^0.2.1") (default-features #t) (kind 0)))) (hash "0zvjza807bddccqhhbaa7rknp5in19m77raq1s76i4hxrdp471gk")))

(define-public crate-denlibs-0.1 (crate (name "denlibs") (vers "0.1.8") (deps (list (crate-dep (name "quad-rand") (req "^0.2.1") (default-features #t) (kind 0)))) (hash "0n93zp8yk5gizy6g451qypd02bmbiy71l0sp5n20mgxfhmvv6srs")))

(define-public crate-denlibs-0.1 (crate (name "denlibs") (vers "0.1.9") (deps (list (crate-dep (name "quad-rand") (req "^0.2.1") (default-features #t) (kind 0)))) (hash "0mryk0kkbr8xv6xffhfaqpfd4klbz1biq195a8p0sixl39gy84xy")))

