(define-module (crates-io de mi) #:use-module (crates-io))

(define-public crate-demi-0.1 (crate (name "demi") (vers "0.1.0") (hash "1ljylyn3jjv1a73lbj9kkwzhf9fn0682rdrnzdba8kd8kp68qnfm")))

(define-public crate-demi-0.1 (crate (name "demi") (vers "0.1.2") (hash "0as8ly74qy88i3plnrpfhsnanipabshrvw78js8nncpjfnd9b2zy")))

(define-public crate-demi-0.1 (crate (name "demi") (vers "0.1.3") (hash "03qi967386y1vfflp6nvafwd46g6blqrczpkqwbxyln5z287srnj") (yanked #t)))

(define-public crate-demigod-0.1 (crate (name "demigod") (vers "0.1.0") (deps (list (crate-dep (name "prost") (req "^0.11.8") (default-features #t) (kind 0)) (crate-dep (name "prost-build") (req "^0.11.8") (default-features #t) (kind 1)))) (hash "1bhlvnl5r87rfq09z3y5phink25sxma5iidmdysmkg3h6iy7wfjr")))

(define-public crate-demiurge-0.0.0 (crate (name "demiurge") (vers "0.0.0") (hash "1pc05v0r8y9dd53hmz3v35cil1k0abm427vid9vj9d1cfy4d7f6y")))

