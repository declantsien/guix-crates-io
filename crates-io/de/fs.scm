(define-module (crates-io de fs) #:use-module (crates-io))

(define-public crate-defs-0.1 (crate (name "defs") (vers "0.1.0") (deps (list (crate-dep (name "proc-macro-error") (req "^1.0.0") (default-features #t) (kind 0)) (crate-dep (name "proc-quote") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.7.3") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.32") (default-features #t) (kind 0)))) (hash "0wq7gwlbzg03x0ql34m00ln7vvc59993hjmmg4a5spfhls5knzi5")))

