(define-module (crates-io de tc) #:use-module (crates-io))

(define-public crate-detchar-0.1 (crate (name "detchar") (vers "0.1.0") (deps (list (crate-dep (name "anyhow") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "cargo-release") (req "^0.20") (default-features #t) (kind 2)) (crate-dep (name "chardetng") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^3.1") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "1jrp4fcx6p1klcbsn6n5qpv03j9rgz8vssdabvcmxqr7zx8890y0") (features (quote (("multithreading" "chardetng/multithreading"))))))

