(define-module (crates-io de va) #:use-module (crates-io))

(define-public crate-devarith-0.1 (crate (name "devarith") (vers "0.1.0") (hash "1sc1af756cn70ijkggbwzdvvfp82l4km7mm2klim0fpsha6xgj87") (yanked #t)))

(define-public crate-devarith-0.1 (crate (name "devarith") (vers "0.1.1") (hash "0ihn6chcaask81249vj8iyblxqzn4japf4sms21cq47qlkg5df1z") (yanked #t)))

(define-public crate-devarith-0.1 (crate (name "devarith") (vers "0.1.2") (hash "1b2c9rc5v9mpsr113x4qi45lsnacsljdyn09yk46jmfkv0mcpc7k") (yanked #t)))

(define-public crate-devault-0.1 (crate (name "devault") (vers "0.1.0") (deps (list (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (features (quote ("extra-traits"))) (default-features #t) (kind 0)))) (hash "16icjp9ljw8640a18avnxgfmf2nssa7f6bcp0zrkpmdw39mx7hgz")))

(define-public crate-devault-0.2 (crate (name "devault") (vers "0.2.0") (deps (list (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (features (quote ("extra-traits" "full"))) (default-features #t) (kind 0)))) (hash "0ja6k6lm91a8x94wkx8yap10gk1g36j0jsyxv5r4mbvwp54q3gdn")))

(define-public crate-devazuka-0.1 (crate (name "devazuka") (vers "0.1.0") (hash "1zxj52awfmbhg5vwid55adxaga22x7fqns9m8wgw6x9i5wzmmaz8")))

