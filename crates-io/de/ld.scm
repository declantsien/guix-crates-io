(define-module (crates-io de ld) #:use-module (crates-io))

(define-public crate-deldup-1 (crate (name "deldup") (vers "1.0.0") (deps (list (crate-dep (name "sha2") (req "^0.10.8") (default-features #t) (kind 0)))) (hash "1sl1j4dq3yhgn0ihzjafs64ydaj13wjgf6x0km4xf9z07ad19dng")))

(define-public crate-deldup-1 (crate (name "deldup") (vers "1.0.1") (deps (list (crate-dep (name "sha2") (req "^0.10.8") (default-features #t) (kind 0)))) (hash "1vf483mzxxcsl4p024jz69zz6kvval1nkf3nkmz1ibzmpy5cys9d")))

