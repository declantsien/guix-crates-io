(define-module (crates-io de f-) #:use-module (crates-io))

(define-public crate-def-mod-0.1 (crate (name "def-mod") (vers "0.1.0") (deps (list (crate-dep (name "proc-macro2") (req "^0.4") (features (quote ("nightly"))) (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^0.6") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^0.14") (features (quote ("full" "extra-traits" "parsing"))) (default-features #t) (kind 0)))) (hash "1j68jh1f6b09x128p60dv5040jsdjsbfw8xqr427rsnn329l9slx")))

(define-public crate-def-mod-0.2 (crate (name "def-mod") (vers "0.2.0") (deps (list (crate-dep (name "proc-macro2") (req "^0.4") (features (quote ("nightly"))) (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^0.6") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^0.14") (features (quote ("full" "extra-traits" "parsing"))) (default-features #t) (kind 0)))) (hash "1zl0ss0f04gc96l651i7xglsxpj6fpx1p417z562gkmn874pp4ig")))

(define-public crate-def-mod-0.3 (crate (name "def-mod") (vers "0.3.0") (deps (list (crate-dep (name "proc-macro2") (req "^0.4") (features (quote ("nightly"))) (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^0.6") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^0.14") (features (quote ("full" "extra-traits" "parsing"))) (default-features #t) (kind 0)))) (hash "1hvd4z1wgh4vjbs2p8wv02gcsa97xl4q2zswywips0ipm2i0a6hd")))

(define-public crate-def-mod-0.4 (crate (name "def-mod") (vers "0.4.0") (deps (list (crate-dep (name "proc-macro2") (req "^0.4") (features (quote ("nightly"))) (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^0.6") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^0.14") (features (quote ("full" "extra-traits" "parsing"))) (default-features #t) (kind 0)))) (hash "1wdg9kv86znksk56n58ybmhigf92yk4vh8s0vqx4q6nyyf12rshp")))

(define-public crate-def-mod-0.5 (crate (name "def-mod") (vers "0.5.0") (deps (list (crate-dep (name "proc-macro2") (req "^0.4") (features (quote ("nightly"))) (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^0.6") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^0.14") (features (quote ("full" "parsing"))) (default-features #t) (kind 0)))) (hash "1v44s6ljr7r35rp087fkv7dg56qydc1j3cphxgq998l6fd7jxi0y") (features (quote (("derive-debug" "syn/extra-traits") ("default"))))))

