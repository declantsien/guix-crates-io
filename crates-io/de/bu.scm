(define-module (crates-io de bu) #:use-module (crates-io))

(define-public crate-debug-adapter-protocol-0.1 (crate (name "debug-adapter-protocol") (vers "0.1.0") (deps (list (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (features (quote ("preserve_order"))) (default-features #t) (kind 0)) (crate-dep (name "typed-builder") (req "^0.12") (default-features #t) (kind 0)))) (hash "0ym2pxikvfadiccz3jdblw8c3k73fwfhzxqbsnb4v7k2x6g30pwn")))

(define-public crate-debug-builders-0.1 (crate (name "debug-builders") (vers "0.1.0") (hash "04ngq10zjy2hp3v2di8bgaksvikkj4l4fwcxlnlb5g6a2hyqwp8g")))

(define-public crate-debug-cell-0.1 (crate (name "debug-cell") (vers "0.1.0") (deps (list (crate-dep (name "backtrace") (req "^0.1") (default-features #t) (kind 0)))) (hash "0py1ly98yakzwk9jgzjgncnk189as7plll2843b401zawpwirq9k")))

(define-public crate-debug-cell-0.1 (crate (name "debug-cell") (vers "0.1.1") (deps (list (crate-dep (name "backtrace") (req "^0.3") (default-features #t) (kind 0)))) (hash "1vx4nggc7awp5xgkfjc7f4jk12nmwvfvqdbinl7l18155z04zvim")))

(define-public crate-debug-derive-0.0.0 (crate (name "debug-derive") (vers "0.0.0") (hash "1789fdans8nigadmvlc3ncvi0qp9v07iqlx6027r8q4xnw50pdnp") (yanked #t)))

(define-public crate-debug-derive-2 (crate (name "debug-derive") (vers "2.0.1") (deps (list (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.3") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.58") (default-features #t) (kind 0)))) (hash "1p7mi2w12bnw7vllq872b97wvkf9dq0bxyh2dc56cx7p4258znly") (features (quote (("std"))))))

(define-public crate-debug-derive-2 (crate (name "debug-derive") (vers "2.0.2") (deps (list (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.3") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.58") (default-features #t) (kind 0)))) (hash "11gqwphbi2sbl8qw4py4s9v562w03dxl07nfzdplyhcisr0dkrln") (features (quote (("std"))))))

(define-public crate-debug-derive-2 (crate (name "debug-derive") (vers "2.1.2") (deps (list (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.3") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.58") (default-features #t) (kind 0)))) (hash "0g45hjpdv8rym9bd5r9v86q772jwairl0l1sq7mahja8x0rk55mi") (features (quote (("std"))))))

(define-public crate-debug-everything-1 (crate (name "debug-everything") (vers "1.0.0") (hash "1gwi3bfnk8418rfdrscqim567hvrdg5rslirrp57bsgf5r9cb12k")))

(define-public crate-debug-helper-0.1 (crate (name "debug-helper") (vers "0.1.0") (hash "065dgbg5qb3ghzrzc456zz2jbgsinsil8z4ymn9v9czmnf74l2rb")))

(define-public crate-debug-helper-0.1 (crate (name "debug-helper") (vers "0.1.1") (hash "1335fvbfk574q72lpzr8ay2987wbqi6xmi3caq7lwxbrhic61swi")))

(define-public crate-debug-helper-0.2 (crate (name "debug-helper") (vers "0.2.0") (hash "1br516w6m2xnn1ym564w7549f5xsgji8is4907bi4vgd315y134f") (yanked #t)))

(define-public crate-debug-helper-0.1 (crate (name "debug-helper") (vers "0.1.2") (hash "1bc5nnd11pm5q9rvkahq8vjygdwsc0hx5mk9f5lv3mrcc5j2w5m5")))

(define-public crate-debug-helper-0.1 (crate (name "debug-helper") (vers "0.1.3") (hash "19h109jsjssq25hy5zl1dmsw2r29jj733sr89gmbrwsnfrgnpz6h")))

(define-public crate-debug-helper-0.1 (crate (name "debug-helper") (vers "0.1.4") (hash "0zabz1jmr7n2k7hyqrc0s8wncd3mx897vl6zmgl9yyl69jf7zcpy")))

(define-public crate-debug-helper-0.2 (crate (name "debug-helper") (vers "0.2.1") (hash "03hd2m2amsvwb74lvdqnp31fs20248dbz4cwrakmj0m19q5di5s1")))

(define-public crate-debug-helper-0.3 (crate (name "debug-helper") (vers "0.3.0") (hash "147mqwl2h88g9qjwhgiwv46pbcpj4g3ly28jaw1k5fp199h4lh8x")))

(define-public crate-debug-helper-0.3 (crate (name "debug-helper") (vers "0.3.1") (hash "05a7ma3vd0k1y7p0fq49n5d8nn6523riwrw9vdzsb2w6fsn855z5")))

(define-public crate-debug-helper-0.3 (crate (name "debug-helper") (vers "0.3.2") (hash "18yq21fms9j8j03zy6yh4j07mf7r7fqwfrhzpbmvpw1ldqfxzfn7")))

(define-public crate-debug-helper-0.3 (crate (name "debug-helper") (vers "0.3.3") (hash "1c7p95hf6qrbbmsjwwka0v2xivn7sr02yla4r0327lbqw65f6lwk")))

(define-public crate-debug-helper-0.3 (crate (name "debug-helper") (vers "0.3.4") (hash "0b4a7sr9ck3sb2srb7h7qxk8m9diz0j5qbqrlhp5jzj6ahxfmbbx")))

(define-public crate-debug-helper-0.3 (crate (name "debug-helper") (vers "0.3.5") (hash "05sf7ysym0bmfppzhadsxvfx7c508kcpk1jm79qji88qhga82pg1")))

(define-public crate-debug-helper-0.3 (crate (name "debug-helper") (vers "0.3.6") (hash "1lqi0a4lxsbr9mr8m5awhcbmayvzsbpg4b4p5h45lzk5i7gxd298")))

(define-public crate-debug-helper-0.3 (crate (name "debug-helper") (vers "0.3.7") (hash "1704g2qrvxcfw9rcf5kk0yryaykhs6r5vd5ss4l5cb2zcd0qbz4k")))

(define-public crate-debug-helper-0.3 (crate (name "debug-helper") (vers "0.3.8") (hash "00ndg40a1yllbngkbhn8cqj612646kziw2wychp6p92k6dqmy54w")))

(define-public crate-debug-helper-0.3 (crate (name "debug-helper") (vers "0.3.9") (hash "19a5n267i7p7q3hnv93mj4l8plkm6gkbpd5w9zsdcng8y2wnnvhp")))

(define-public crate-debug-helper-0.3 (crate (name "debug-helper") (vers "0.3.10") (hash "0pj34zars6plbp054igqq1kiiqw8ia92b6zi8z144kzjjjw5p2ls")))

(define-public crate-debug-helper-0.3 (crate (name "debug-helper") (vers "0.3.11") (hash "0k29p6slv6r580b0mqav28am77x2nq1p87x5vjyp6vw4cxl5jq24")))

(define-public crate-debug-helper-0.3 (crate (name "debug-helper") (vers "0.3.12") (hash "1a73xl73psmzyihd62jy30g1acfmgjmyi2k8khmh170mrq6x3yvn")))

(define-public crate-debug-helper-0.3 (crate (name "debug-helper") (vers "0.3.13") (hash "0bhnpzpgmg8dkdr27g2b49slf6ca79m4idcb01z2krs0qkifhy7m")))

(define-public crate-debug-here-0.1 (crate (name "debug-here") (vers "0.1.0") (deps (list (crate-dep (name "lazy_static") (req "^1.1.0") (default-features #t) (kind 0)) (crate-dep (name "nix") (req "^0.11.0") (default-features #t) (kind 0)) (crate-dep (name "which") (req "^2.0") (default-features #t) (kind 0)))) (hash "07z1fmy28p1b36y62q0qxaca1zfac0p9rf9i3k5sw6ksp4wnj73j")))

(define-public crate-debug-here-0.1 (crate (name "debug-here") (vers "0.1.1") (deps (list (crate-dep (name "lazy_static") (req "^1.1.0") (default-features #t) (kind 0)) (crate-dep (name "nix") (req "^0.11.0") (default-features #t) (kind 0)) (crate-dep (name "which") (req "^2.0") (default-features #t) (kind 0)))) (hash "1mnj0vrq9nv659wcg72radn8hcl44z2s8d9hrd9an0bqkcidqvgd")))

(define-public crate-debug-here-0.1 (crate (name "debug-here") (vers "0.1.2") (deps (list (crate-dep (name "lazy_static") (req "^1.1.0") (default-features #t) (kind 0)) (crate-dep (name "nix") (req "^0.11.0") (default-features #t) (kind 0)) (crate-dep (name "which") (req "^2.0") (default-features #t) (kind 0)))) (hash "0rj6y43mf253s2j3qslzzk3v71w5ky9fscnzkbks384x5j5z43xa")))

(define-public crate-debug-here-0.2 (crate (name "debug-here") (vers "0.2.0") (deps (list (crate-dep (name "lazy_static") (req "^1.1.0") (default-features #t) (kind 0)) (crate-dep (name "nix") (req "^0.11.0") (default-features #t) (kind 0)) (crate-dep (name "which") (req "^2.0") (default-features #t) (kind 0)))) (hash "01960n564x7dw7yjckfvvbbhkbq6rhb53sshiyijci6lsl74wmb4")))

(define-public crate-debug-here-0.2 (crate (name "debug-here") (vers "0.2.1") (deps (list (crate-dep (name "lazy_static") (req "^1.1.0") (default-features #t) (kind 0)) (crate-dep (name "which") (req "^2.0") (default-features #t) (kind 0)) (crate-dep (name "winapi") (req "^0.3") (features (quote ("debugapi"))) (default-features #t) (target "cfg(windows)") (kind 0)))) (hash "13qy9il1bkpagpp4v2f088mys2bmqs37wwx3hmxsx99rz5605nhl")))

(define-public crate-debug-here-0.2 (crate (name "debug-here") (vers "0.2.2") (deps (list (crate-dep (name "lazy_static") (req "^1.1.0") (default-features #t) (kind 0)) (crate-dep (name "which") (req "^2.0") (default-features #t) (kind 0)) (crate-dep (name "winapi") (req "^0.3") (features (quote ("debugapi"))) (default-features #t) (target "cfg(windows)") (kind 0)))) (hash "1cggcm85md5jsw0n0cngwahmwy3dkp11rmq6cjm31wsm7zw92xqx")))

(define-public crate-debug-here-gdb-wrapper-0.1 (crate (name "debug-here-gdb-wrapper") (vers "0.1.0") (hash "00r7a0vj7jlc3mp0id60k7xvqzc24vlh6yswp1x0l6af86qjj582")))

(define-public crate-debug-here-gdb-wrapper-0.2 (crate (name "debug-here-gdb-wrapper") (vers "0.2.0") (hash "0ffvfnjfp0ql17fw2gvnw8jw1r7d22yr57daa2pppv2nbmypl42m")))

(define-public crate-debug-ignore-1 (crate (name "debug-ignore") (vers "1.0.0") (deps (list (crate-dep (name "serde") (req "^1") (features (quote ("derive"))) (optional #t) (kind 0)) (crate-dep (name "serde_json") (req "^1") (default-features #t) (kind 2)))) (hash "15gfp4q9688gziwjink34ycq052ya603bz5bsaksis3cxr4l1c48")))

(define-public crate-debug-ignore-1 (crate (name "debug-ignore") (vers "1.0.1") (deps (list (crate-dep (name "serde") (req "^1") (features (quote ("derive"))) (optional #t) (kind 0)) (crate-dep (name "serde_json") (req "^1") (default-features #t) (kind 2)))) (hash "0i8prxp09456asxs4v7r1hb26lapz4rrkpfxl3q92i2fbb6qjc12")))

(define-public crate-debug-ignore-1 (crate (name "debug-ignore") (vers "1.0.2") (deps (list (crate-dep (name "serde") (req "^1") (features (quote ("derive"))) (optional #t) (kind 0)) (crate-dep (name "serde_json") (req "^1") (default-features #t) (kind 2)))) (hash "06vx6dryyyxrj03iav9qc08rygpzyhyrlhp66gwvm4cbbx66jlby")))

(define-public crate-debug-ignore-1 (crate (name "debug-ignore") (vers "1.0.3") (deps (list (crate-dep (name "serde") (req "^1") (features (quote ("derive"))) (optional #t) (kind 0)) (crate-dep (name "serde_json") (req "^1") (default-features #t) (kind 2)))) (hash "1kzld8z7bldjmnnn3vlpm6m2j9hgizkk7lfwkm4kqirgkssb0j2b")))

(define-public crate-debug-ignore-1 (crate (name "debug-ignore") (vers "1.0.4") (deps (list (crate-dep (name "serde") (req "^1") (features (quote ("derive"))) (optional #t) (kind 0)) (crate-dep (name "serde_json") (req "^1") (default-features #t) (kind 2)))) (hash "1l0dgsf4mbpvkqz62shg6fchqwdpjxxn0j2sd5xjh9s86cdqq31y")))

(define-public crate-debug-ignore-1 (crate (name "debug-ignore") (vers "1.0.5") (deps (list (crate-dep (name "serde") (req "^1") (features (quote ("derive"))) (optional #t) (kind 0)) (crate-dep (name "serde_json") (req "^1") (default-features #t) (kind 2)))) (hash "08gwdny6124ggy4hyli92hdyiqc5j2z9lqhbw81k0mgljcfyvrzz")))

(define-public crate-debug-impl-parser-0.1 (crate (name "debug-impl-parser") (vers "0.1.0") (deps (list (crate-dep (name "criterion") (req "^0.5") (default-features #t) (kind 2)) (crate-dep (name "nom") (req "^7.1.3") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.164") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.164") (features (quote ("derive"))) (default-features #t) (kind 2)) (crate-dep (name "serde-wasm-bindgen") (req "^0.5") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.96") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.96") (default-features #t) (kind 2)) (crate-dep (name "wasm-bindgen") (req "^0.2.86") (default-features #t) (kind 0)))) (hash "0a4sp9v9gdvwpzgiq8d9w1k5ga9w3q5szmahphr4x5nz3nsfgwx3") (yanked #t)))

(define-public crate-debug-interface-0.1 (crate (name "debug-interface") (vers "0.1.0") (deps (list (crate-dep (name "anyhow") (req "^1.0.57") (default-features #t) (kind 0)) (crate-dep (name "aptos-config") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "aptos-logger") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "aptos-metrics") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "aptos-workspace-hack") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "bytes") (req "^1.1.0") (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.11.10") (features (quote ("blocking" "json"))) (kind 0)) (crate-dep (name "serde") (req "^1.0.137") (features (quote ("derive"))) (kind 0)) (crate-dep (name "tokio") (req "^1.18.2") (features (quote ("full"))) (default-features #t) (kind 0)) (crate-dep (name "warp") (req "^0.3.2") (default-features #t) (kind 0)))) (hash "1krhyz510m3san9kzpdb9cmwinrafdwz3jqa6rdvay7m2q189d8s") (yanked #t)))

(define-public crate-debug-interface-0.1 (crate (name "debug-interface") (vers "0.1.1") (deps (list (crate-dep (name "anyhow") (req "^1.0.57") (default-features #t) (kind 0)) (crate-dep (name "aptos-config") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "aptos-logger") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "aptos-metrics") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "aptos-workspace-hack") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "bytes") (req "^1.1.0") (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.11.10") (features (quote ("blocking" "json"))) (kind 0)) (crate-dep (name "serde") (req "^1.0.137") (features (quote ("derive"))) (kind 0)) (crate-dep (name "tokio") (req "^1.18.2") (features (quote ("full"))) (default-features #t) (kind 0)) (crate-dep (name "warp") (req "^0.3.2") (default-features #t) (kind 0)))) (hash "08fxh7mn4c1zjlxl6wwsl6c2gf9fjnkhhbbflvnjz6s4bllj8jy6") (yanked #t)))

(define-public crate-debug-interface-0.1 (crate (name "debug-interface") (vers "0.1.3") (deps (list (crate-dep (name "anyhow") (req "^1.0.57") (default-features #t) (kind 0)) (crate-dep (name "aptos-config") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "aptos-logger") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "aptos-metrics") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "bytes") (req "^1.1.0") (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.11.10") (features (quote ("blocking" "json"))) (kind 0)) (crate-dep (name "serde") (req "^1.0.137") (features (quote ("derive"))) (kind 0)) (crate-dep (name "tokio") (req "^1.18.2") (features (quote ("full"))) (default-features #t) (kind 0)) (crate-dep (name "warp") (req "^0.3.2") (default-features #t) (kind 0)))) (hash "0zp6xvwa83cfm6dg50np5mqhkw5gkhxjn9mzs1qykcqi708hxwhp") (yanked #t)))

(define-public crate-debug-interface-0.1 (crate (name "debug-interface") (vers "0.1.4") (deps (list (crate-dep (name "anyhow") (req "^1.0.57") (default-features #t) (kind 0)) (crate-dep (name "aptos-config") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "aptos-logger") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "aptos-metrics") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "bytes") (req "^1.1.0") (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.11.10") (features (quote ("blocking" "json"))) (kind 0)) (crate-dep (name "serde") (req "^1.0.137") (features (quote ("derive"))) (kind 0)) (crate-dep (name "tokio") (req "^1.18.2") (features (quote ("full"))) (default-features #t) (kind 0)) (crate-dep (name "warp") (req "^0.3.2") (default-features #t) (kind 0)))) (hash "077wyf1prfvypdmnvmzfxxfp9n4vlxpkba25l1lb0dhp4iznpv8v") (yanked #t)))

(define-public crate-debug-log-0.1 (crate (name "debug-log") (vers "0.1.0") (hash "1lrxf7mpw8zvnwq4zcnwyy7d24ix1yb2yx32aaczqivcf33r1cby")))

(define-public crate-debug-log-0.1 (crate (name "debug-log") (vers "0.1.1") (hash "02y6xwzlz369ivd316pd559nrl49zpp63impc5318nx2cn7cwb6d")))

(define-public crate-debug-log-0.1 (crate (name "debug-log") (vers "0.1.2") (hash "0dxga8f0j2pc1vi3zqryhn9jkw3kgf8l2mlql2nplipf2b8z4wgs")))

(define-public crate-debug-log-0.1 (crate (name "debug-log") (vers "0.1.3") (hash "0f6w6nfd5xcvdrcvld3jpf39lqj77a0ga7xmwf220dj4d1k5r8r2")))

(define-public crate-debug-log-0.1 (crate (name "debug-log") (vers "0.1.4") (hash "0m32fr5m4h7hj2xarz8v8mwis7fwqz4zvkbd0ady7ziia1ygakrn")))

(define-public crate-debug-log-0.2 (crate (name "debug-log") (vers "0.2.0") (deps (list (crate-dep (name "once_cell") (req "^1.17.1") (default-features #t) (kind 0)) (crate-dep (name "wasm-bindgen") (req "^0.2.86") (optional #t) (default-features #t) (kind 0)))) (hash "1y82ih1qpjsfsf3360z7mxfkaamsw2ahnp6g3aqrbgvfrikariz4") (features (quote (("wasm" "wasm-bindgen"))))))

(define-public crate-debug-log-0.2 (crate (name "debug-log") (vers "0.2.1") (deps (list (crate-dep (name "once_cell") (req "^1.17.1") (default-features #t) (kind 0)) (crate-dep (name "wasm-bindgen") (req "^0.2.86") (optional #t) (default-features #t) (kind 0)))) (hash "0iy9fabw15brwxb9d3092l84zc55vyf2md526arycw1zs1ykxl93") (features (quote (("wasm" "wasm-bindgen"))))))

(define-public crate-debug-log-0.2 (crate (name "debug-log") (vers "0.2.2") (deps (list (crate-dep (name "once_cell") (req "^1.17.1") (default-features #t) (kind 0)) (crate-dep (name "wasm-bindgen") (req "^0.2") (optional #t) (default-features #t) (kind 0)))) (hash "0wvcn909a1im89485j4ij8xyyspcj8ikckl7baillk0l1jf9s3xr") (features (quote (("wasm" "wasm-bindgen"))))))

(define-public crate-debug-log-0.3 (crate (name "debug-log") (vers "0.3.0") (deps (list (crate-dep (name "once_cell") (req "^1.17.1") (default-features #t) (kind 0)) (crate-dep (name "wasm-bindgen") (req "^0.2") (optional #t) (default-features #t) (kind 0)))) (hash "0qwxc06dpjm1hqilm079mkkljqwsc0lq61h9ssnzjd9gf3jggqcy") (features (quote (("wasm" "wasm-bindgen"))))))

(define-public crate-debug-log-0.3 (crate (name "debug-log") (vers "0.3.1") (deps (list (crate-dep (name "once_cell") (req "^1.17.1") (default-features #t) (kind 0)) (crate-dep (name "wasm-bindgen") (req "^0.2") (optional #t) (default-features #t) (kind 0)))) (hash "0fjch669sr9hrrsvbrzlxj9d2m4ar5yb9j6c5ibgq8zc56v63y6a") (features (quote (("wasm" "wasm-bindgen"))))))

(define-public crate-debug-macro-0.1 (crate (name "debug-macro") (vers "0.1.0") (hash "11dj0c2gmhzgc3b0g4jwvc0ydpxmlxyyvlhkgpmfj5jj2al6fl0h")))

(define-public crate-debug-map-sorted-0.1 (crate (name "debug-map-sorted") (vers "0.1.0") (deps (list (crate-dep (name "itertools") (req "^0.10.5") (default-features #t) (kind 0)) (crate-dep (name "quickcheck") (req "^1.0.3") (default-features #t) (kind 2)) (crate-dep (name "quickcheck_macros") (req "^1.0.0") (default-features #t) (kind 2)))) (hash "1xvfkg647qvkbsssii8nnwhhlclwg4fdf4w99h9m32d5c4yms7lb")))

(define-public crate-debug-map-sorted-0.1 (crate (name "debug-map-sorted") (vers "0.1.1") (deps (list (crate-dep (name "itertools") (req "^0.10.5") (default-features #t) (kind 0)) (crate-dep (name "quickcheck") (req "^1.0.3") (default-features #t) (kind 2)) (crate-dep (name "quickcheck_macros") (req "^1.0.0") (default-features #t) (kind 2)))) (hash "08dp7zbh8b9kqahwb6xv4ya4wqvb2shxmah7zbwlnwqq6sldzivm")))

(define-public crate-debug-message-1 (crate (name "debug-message") (vers "1.0.0") (hash "1kmq4q235m6zyf1079v5ap8zv41wy0kvf8i0z5j9j15n1wcr5435")))

(define-public crate-debug-message-1 (crate (name "debug-message") (vers "1.1.0") (hash "0qk58pz4vwcyr4mhp3mj91z6qjzwx85ms9sg6inv819pjwxh0vzi")))

(define-public crate-debug-message-1 (crate (name "debug-message") (vers "1.2.0") (hash "1v3k7iy1dq4zxwd5gnjiwq00ndc0c6a1cscswyzj0xzdbvzpa2ah")))

(define-public crate-debug-probe-0.1 (crate (name "debug-probe") (vers "0.1.0") (deps (list (crate-dep (name "arm-memory") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "bitfield") (req "^0.13.2") (default-features #t) (kind 0)) (crate-dep (name "bitflags") (req "^1.2.0") (default-features #t) (kind 0)) (crate-dep (name "capstone") (req "^0.6.0") (default-features #t) (kind 0)) (crate-dep (name "coresight") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "crc") (req "^1.8.1") (default-features #t) (kind 0)) (crate-dep (name "derivative") (req "^1.0.3") (default-features #t) (kind 0)) (crate-dep (name "goblin") (req "^0.0.24") (default-features #t) (kind 0)) (crate-dep (name "hexdump") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "ihex") (req "^1.1.2") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.6") (default-features #t) (kind 0)) (crate-dep (name "object") (req "^0.14.0") (default-features #t) (kind 0)) (crate-dep (name "query_interface") (req "^0.3.5") (default-features #t) (kind 0)) (crate-dep (name "scroll") (req "^0.9.2") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.101") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0.101") (default-features #t) (kind 0)))) (hash "0rgf10msvb4blspbr9ayrwv2rl88d00kyrvp3750s5vf0jzvmha5") (yanked #t)))

(define-public crate-debug-rs-0.1 (crate (name "debug-rs") (vers "0.1.0") (deps (list (crate-dep (name "colored") (req "^1.5.2") (default-features #t) (kind 0)) (crate-dep (name "globset") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^0.2.8") (default-features #t) (kind 0)))) (hash "14jxj9y2vkv4hgzfl7pfrvmbhw8g6mwyicpx5l90rzfvfjh5kdbc") (features (quote (("disable") ("default"))))))

(define-public crate-debug-rs-0.1 (crate (name "debug-rs") (vers "0.1.1") (deps (list (crate-dep (name "colored") (req "^1.5.2") (default-features #t) (kind 0)) (crate-dep (name "globset") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^0.2.8") (default-features #t) (kind 0)))) (hash "0610hw9kln4kjn0yga2l92dh7n188l0vblx4kczvjnrn1b3v53g9") (features (quote (("disable") ("default"))))))

(define-public crate-debug-rs-0.1 (crate (name "debug-rs") (vers "0.1.2") (deps (list (crate-dep (name "colored") (req "^1.5.2") (default-features #t) (kind 0)) (crate-dep (name "globset") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^0.2.8") (default-features #t) (kind 0)))) (hash "1y2l1biqja0rb9ki1ydjzwsf1ic8q1jmy4j7czsw219hli13jq81") (features (quote (("disable") ("default"))))))

(define-public crate-debug-rs-0.1 (crate (name "debug-rs") (vers "0.1.3") (deps (list (crate-dep (name "colored") (req "^1.5.2") (default-features #t) (kind 0)) (crate-dep (name "globset") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^0.2.8") (default-features #t) (kind 0)))) (hash "1afqycfa42zpgxjmfnkc3xdbd7m7scdl2xwl31lbkjf9qy6ap2km") (features (quote (("disable") ("default" "debug_build_only") ("debug_build_only"))))))

(define-public crate-debug-span-0.1 (crate (name "debug-span") (vers "0.1.0") (deps (list (crate-dep (name "insta") (req "^1") (default-features #t) (kind 2)) (crate-dep (name "proc-macro2") (req "^1") (features (quote ("span-locations"))) (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2") (default-features #t) (kind 2)) (crate-dep (name "unindent") (req "^0.2") (default-features #t) (kind 2)))) (hash "1hr4n4zqqagscvrbykz7rfci856ynb1dzj8p5c7wvlgd2chzapl4")))

(define-public crate-debug-span-0.2 (crate (name "debug-span") (vers "0.2.0") (deps (list (crate-dep (name "insta") (req "^1") (default-features #t) (kind 2)) (crate-dep (name "proc-macro2") (req "^1") (features (quote ("span-locations"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2") (default-features #t) (kind 2)) (crate-dep (name "unindent") (req "^0.2") (default-features #t) (kind 2)))) (hash "1prx7sx0v3i5s91bg2qs69fdx7lmaflc08lw3kr8kix1203vdjn0") (features (quote (("default" "proc-macro2"))))))

(define-public crate-debug-tag-0.1 (crate (name "debug-tag") (vers "0.1.0") (hash "0hd0k1d47isgljb2jrdn9dj2ic5nizqwdnxbhj0gw5w26mc4jz81")))

(define-public crate-debug-try-0.1 (crate (name "debug-try") (vers "0.1.0") (deps (list (crate-dep (name "proc-macro2") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^0.6") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^0.15") (features (quote ("full" "visit-mut"))) (default-features #t) (kind 0)))) (hash "00967nxa1w3r5srmc1krfjq9fz7647mkx8h6vax2f1pkvhb13pix")))

(define-public crate-debug2-0.1 (crate (name "debug2") (vers "0.1.0") (deps (list (crate-dep (name "debug2-derive") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "insta") (req "^1.7.1") (default-features #t) (kind 2)))) (hash "0k642jy892jy1dhyfirx5590g4282x5ky9s6j891hqyzlcp0cw2y")))

(define-public crate-debug2-0.1 (crate (name "debug2") (vers "0.1.1") (deps (list (crate-dep (name "debug2-derive") (req "^0.1.1") (default-features #t) (kind 0)) (crate-dep (name "insta") (req "^1.7.1") (default-features #t) (kind 2)))) (hash "1c00fnd0r4925yhrfjdiig68sd23138ir5m1jws2s665f6gk7ams")))

(define-public crate-debug2-derive-0.1 (crate (name "debug2-derive") (vers "0.1.0") (deps (list (crate-dep (name "proc-macro2") (req "^1.0.27") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.9") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.73") (default-features #t) (kind 0)) (crate-dep (name "synstructure") (req "^0.12.5") (default-features #t) (kind 0)))) (hash "06l73rixxpi14s61lj22vhpi7ahgccxa6kjvry9jjqyx864pxcvp")))

(define-public crate-debug2-derive-0.1 (crate (name "debug2-derive") (vers "0.1.1") (deps (list (crate-dep (name "proc-macro2") (req "^1.0.27") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.9") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.73") (default-features #t) (kind 0)) (crate-dep (name "synstructure") (req "^0.12.5") (default-features #t) (kind 0)))) (hash "1nlydacs1v5y5cs7kb9wpa1ygdrsfw7nh0qy5z5fi1b442cd3c4g")))

(define-public crate-debug3-0.1 (crate (name "debug3") (vers "0.1.0") (deps (list (crate-dep (name "debug3-derive") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "rustversion") (req "^1.0.6") (default-features #t) (kind 2)) (crate-dep (name "trybuild") (req "^1.0.56") (default-features #t) (kind 2)))) (hash "19lj6q3rx55mzdrvv6b11pkjskvfy0xqfma75ykxpilx19xnsjh0")))

(define-public crate-debug3-0.2 (crate (name "debug3") (vers "0.2.0") (deps (list (crate-dep (name "aws-sdk-ec2") (req "^0.15.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "aws-smithy-types") (req "^0.45.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "bytes") (req "^1.1.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "dashmap") (req "^5.3.4") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "debug3-derive") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "expect-test") (req "^1.3.0") (default-features #t) (kind 2)) (crate-dep (name "fxhash") (req "^0.2.1") (default-features #t) (kind 2)) (crate-dep (name "hashbrown") (req "^0.12.1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "kdl") (req "^4.3.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "miette") (req "^4.7.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1.0.40") (features (quote ("span-locations"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "rustdoc-types") (req "^0.11.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "rustversion") (req "^1.0.7") (default-features #t) (kind 2)) (crate-dep (name "serde_json") (req "^1.0.82") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.82") (default-features #t) (kind 2)) (crate-dep (name "smallvec") (req "^1.9.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.98") (features (quote ("full"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "trybuild") (req "^1.0.63") (default-features #t) (kind 2)))) (hash "062zw4cc7z74r6gq5z6ys9q2ss9mc1z35vw19i33sidfbsiddcnr") (v 2) (features2 (quote (("syn" "dep:syn" "proc-macro2") ("kdl" "dep:kdl" "miette") ("aws-sdk-ec2" "dep:aws-sdk-ec2" "aws-smithy-types"))))))

(define-public crate-debug3-0.3 (crate (name "debug3") (vers "0.3.0") (deps (list (crate-dep (name "aws-sdk-ec2") (req "^0.15.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "aws-smithy-types") (req "^0.45.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "bytes") (req "^1.1.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "dashmap") (req "^5.3.4") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "debug3-derive") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "expect-test") (req "^1.3.0") (default-features #t) (kind 2)) (crate-dep (name "fxhash") (req "^0.2.1") (default-features #t) (kind 2)) (crate-dep (name "hashbrown") (req "^0.12.1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "kdl") (req "^4.4.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "miette") (req "^5.3.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1.0.40") (features (quote ("span-locations"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "rustdoc-types") (req "^0.11.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "rustversion") (req "^1.0.7") (default-features #t) (kind 2)) (crate-dep (name "serde_json") (req "^1.0.82") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.82") (default-features #t) (kind 2)) (crate-dep (name "smallvec") (req "^1.9.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.98") (features (quote ("full"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "trybuild") (req "^1.0.77") (default-features #t) (kind 2)))) (hash "041g6m9g1lbssjknp88z4q4vc67rr77s34v7dw5ayzxg830jka9b") (v 2) (features2 (quote (("syn" "dep:syn" "proc-macro2") ("kdl" "dep:kdl" "miette") ("aws-sdk-ec2" "dep:aws-sdk-ec2" "aws-smithy-types"))))))

(define-public crate-debug3-0.4 (crate (name "debug3") (vers "0.4.0") (deps (list (crate-dep (name "aws-sdk-ec2") (req "^0.15.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "aws-smithy-types") (req "^0.45.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "bytes") (req "^1.1.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "cid") (req "^0.10.1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "dashmap") (req "^5.3.4") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "debug3-derive") (req "^0.4.0") (default-features #t) (kind 0)) (crate-dep (name "expect-test") (req "^1.3.0") (default-features #t) (kind 2)) (crate-dep (name "fxhash") (req "^0.2.1") (default-features #t) (kind 2)) (crate-dep (name "hashbrown") (req "^0.12.1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "kdl") (req "^4.4.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "libipld-core") (req "^0.16.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "miette") (req "^5.3.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1.0.40") (features (quote ("span-locations"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "pulldown-cmark") (req "^0.9.2") (optional #t) (kind 0)) (crate-dep (name "rustdoc-types") (req "^0.11.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "rustversion") (req "^1.0.7") (default-features #t) (kind 2)) (crate-dep (name "serde_json") (req "^1.0.82") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.82") (default-features #t) (kind 2)) (crate-dep (name "smallvec") (req "^1.9.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.98") (features (quote ("full"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "trybuild") (req "^1.0.77") (default-features #t) (kind 2)) (crate-dep (name "ungrammar") (req "^1.16.1") (optional #t) (default-features #t) (kind 0)))) (hash "0mixp0zkiqm2ykvqnn070mhwws9csdvqack3pzwq25dc7xza5g5g") (v 2) (features2 (quote (("syn" "dep:syn" "proc-macro2") ("libipld-core" "dep:libipld-core" "cid") ("kdl" "dep:kdl" "miette") ("cid" "dep:cid") ("aws-sdk-ec2" "dep:aws-sdk-ec2" "aws-smithy-types"))))))

(define-public crate-debug3-0.4 (crate (name "debug3") (vers "0.4.1") (deps (list (crate-dep (name "aws-sdk-ec2") (req "^0.15.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "aws-smithy-types") (req "^0.45.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "bytes") (req "^1.1.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "cid") (req "^0.10.1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "dashmap") (req "^5.3.4") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "debug3-derive") (req "^0.4.0") (default-features #t) (kind 0)) (crate-dep (name "expect-test") (req "^1.3.0") (default-features #t) (kind 2)) (crate-dep (name "fxhash") (req "^0.2.1") (default-features #t) (kind 2)) (crate-dep (name "hashbrown") (req "^0.12.1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "kdl") (req "^4.4.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "libipld-core") (req "^0.16.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "miette") (req "^5.3.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1.0.40") (features (quote ("span-locations"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "pulldown-cmark") (req "^0.9.2") (optional #t) (kind 0)) (crate-dep (name "rustdoc-types") (req "^0.11.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "rustversion") (req "^1.0.7") (default-features #t) (kind 2)) (crate-dep (name "serde_json") (req "^1.0.82") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.82") (default-features #t) (kind 2)) (crate-dep (name "smallvec") (req "^1.9.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.98") (features (quote ("full"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "trybuild") (req "^1.0.77") (default-features #t) (kind 2)) (crate-dep (name "ungrammar") (req "^1.16.1") (optional #t) (default-features #t) (kind 0)))) (hash "1xr2sg0gvrbd9qa2brxrqlv21fi97zl215mlyg8zpgwz2gd07ilf") (v 2) (features2 (quote (("syn" "dep:syn" "proc-macro2") ("libipld-core" "dep:libipld-core" "cid") ("kdl" "dep:kdl" "miette") ("cid" "dep:cid") ("aws-sdk-ec2" "dep:aws-sdk-ec2" "aws-smithy-types"))))))

(define-public crate-debug3-derive-0.1 (crate (name "debug3-derive") (vers "0.1.0") (deps (list (crate-dep (name "proc-macro2") (req "^1.0.36") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.15") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.86") (default-features #t) (kind 0)) (crate-dep (name "synstructure") (req "^0.12.6") (default-features #t) (kind 0)))) (hash "0qjaiw205ad58bfk1k3vhn0xg2v58bbr91njypiclykpv41bnz15")))

(define-public crate-debug3-derive-0.2 (crate (name "debug3-derive") (vers "0.2.0") (deps (list (crate-dep (name "proc-macro2") (req "^1.0.36") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.15") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.86") (default-features #t) (kind 0)) (crate-dep (name "synstructure") (req "^0.12.6") (default-features #t) (kind 0)))) (hash "0cmvckfri9lg0qinq3aggs76s9pjij0f6ywip87hiyyh76346c0q")))

(define-public crate-debug3-derive-0.3 (crate (name "debug3-derive") (vers "0.3.0") (deps (list (crate-dep (name "proc-macro2") (req "^1.0.36") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.15") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.86") (default-features #t) (kind 0)) (crate-dep (name "synstructure") (req "^0.12.6") (default-features #t) (kind 0)))) (hash "06vdxwql9mck3207w7bbzmbhkwifxds1bg8hv1apa5xg9gknxzkv")))

(define-public crate-debug3-derive-0.4 (crate (name "debug3-derive") (vers "0.4.0") (deps (list (crate-dep (name "proc-macro2") (req "^1.0.36") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.15") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2.0.15") (default-features #t) (kind 0)) (crate-dep (name "synstructure") (req "^0.13.0") (default-features #t) (kind 0)))) (hash "0g04j1rbix30r2n7hm1asazlbzv9z5j6whz4axf3gwwlmjmr9qvn")))

(define-public crate-debug_canvas-0.1 (crate (name "debug_canvas") (vers "0.1.0") (hash "13mrm1bizwrni7y6jvb0977nipq057fda7gx9vp1hiqgfbq88x8n")))

(define-public crate-debug_canvas-0.1 (crate (name "debug_canvas") (vers "0.1.1") (hash "0i92livai7ryi13y5jj6w3p2054s87kdygcyzhjzmxgfwkpqpx7w")))

(define-public crate-debug_code-0.1 (crate (name "debug_code") (vers "0.1.0") (deps (list (crate-dep (name "proc-macro2") (req "^1.0.79") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.35") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2.0.52") (default-features #t) (kind 0)))) (hash "1csrsqwfs0h535lncvihryg4dsdf83072mq0c8cbxwyjmrzdx556")))

(define-public crate-debug_code-0.1 (crate (name "debug_code") (vers "0.1.1") (deps (list (crate-dep (name "proc-macro2") (req "^1.0.79") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.35") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2.0.52") (default-features #t) (kind 0)))) (hash "1llcspj5dixgj9xgnih12s3wnzwsq0s75i73a4lgn7kjbs7j5mj9")))

(define-public crate-debug_finder-0.1 (crate (name "debug_finder") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^2.33.0") (default-features #t) (kind 0)))) (hash "1sp3198lxyw4i3wlfagjp639q3mcif8zkdmi7d5800ljjgiiljyp")))

(define-public crate-debug_finder-0.1 (crate (name "debug_finder") (vers "0.1.1") (deps (list (crate-dep (name "clap") (req "^2.33.0") (default-features #t) (kind 0)))) (hash "15cd8a7nqs913sxkd1b94cgdh7gcm7lm6kn42yk0wkdsd4ki20qm")))

(define-public crate-debug_finder-0.1 (crate (name "debug_finder") (vers "0.1.2") (deps (list (crate-dep (name "clap") (req "^2.33.0") (default-features #t) (kind 0)))) (hash "0q8l77qcwqn86059mnf9d9cb2bk1slaxdx7a16mgq51hq3sxis6q")))

(define-public crate-debug_finder-0.2 (crate (name "debug_finder") (vers "0.2.0") (deps (list (crate-dep (name "clap") (req "^2.33.0") (default-features #t) (kind 0)))) (hash "1ilazhwdab45ik5i8k87czk7rjk0l256hwdyq3fl6bqar6hjkls3")))

(define-public crate-debug_finder-0.2 (crate (name "debug_finder") (vers "0.2.1") (deps (list (crate-dep (name "clap") (req "^2.33.0") (default-features #t) (kind 0)))) (hash "1gj626nfw4qa7j3k84nz0788wf1j6ajwr9cgw393y12vif9gycri")))

(define-public crate-debug_finder-0.2 (crate (name "debug_finder") (vers "0.2.2") (deps (list (crate-dep (name "clap") (req "^2.33.0") (default-features #t) (kind 0)))) (hash "0lax8424ywd9gxiqswry33492053n0wvy315690klkdms8lhggar")))

(define-public crate-debug_finder-0.2 (crate (name "debug_finder") (vers "0.2.3") (deps (list (crate-dep (name "clap") (req "^2.33.0") (default-features #t) (kind 0)))) (hash "0zmbzg0mszd5v3xkqjw9c5ym1canljbndvk06b8qdj0lgy0xpihq")))

(define-public crate-debug_finder-0.2 (crate (name "debug_finder") (vers "0.2.4") (deps (list (crate-dep (name "clap") (req "^2.33.0") (default-features #t) (kind 0)) (crate-dep (name "prettytable-rs") (req "^0.8.0") (default-features #t) (kind 0)))) (hash "1ma2xrpbhl9zhgidnx5005fb11d6hrgvyzsy99xr9k44p3avcry2")))

(define-public crate-debug_finder-0.3 (crate (name "debug_finder") (vers "0.3.0") (deps (list (crate-dep (name "clap") (req "^2.33.0") (default-features #t) (kind 0)) (crate-dep (name "prettytable-rs") (req "^0.8.0") (default-features #t) (kind 0)))) (hash "0qj59ajcqj4gdsss3ddbd3bxjaanmxwsa937v3gxgwvacxa5hf2n")))

(define-public crate-debug_finder-0.3 (crate (name "debug_finder") (vers "0.3.1") (deps (list (crate-dep (name "clap") (req "^2.33.0") (default-features #t) (kind 0)) (crate-dep (name "json") (req "^0.12.0") (default-features #t) (kind 0)) (crate-dep (name "prettytable-rs") (req "^0.8.0") (default-features #t) (kind 0)))) (hash "1vq47rr100xgdm70czqhc0hcjak0dqlwabwk75k4zhic66cjzz55")))

(define-public crate-debug_iterator-0.1 (crate (name "debug_iterator") (vers "0.1.0") (deps (list (crate-dep (name "log") (req "^0.4.7") (optional #t) (default-features #t) (kind 0)))) (hash "1hrcb1aws3x51kdym6g3w8582n6bmmp4gd58yqfpin2i8c4qjg8j") (features (quote (("logging" "log") ("default"))))))

(define-public crate-debug_macros-0.0.1 (crate (name "debug_macros") (vers "0.0.1") (hash "1z52i9wyi630x599y2v0krca7vyy57zw0yhw35c6sr5k2ig5dba6")))

(define-public crate-debug_overlay-0.1 (crate (name "debug_overlay") (vers "0.1.0") (deps (list (crate-dep (name "bytemuck") (req "^1.15.0") (default-features #t) (kind 0)) (crate-dep (name "wgpu") (req "^0.19.3") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "wgpu-core") (req "^0.19.3") (optional #t) (default-features #t) (kind 0)))) (hash "1p711aqw3z3rxkq52cyvg2k1vpp9z3xb9fvy4asb9kwkay26gxmw") (v 2) (features2 (quote (("wgpu-core" "dep:wgpu-core") ("wgpu" "dep:wgpu"))))))

(define-public crate-debug_overlay-0.2 (crate (name "debug_overlay") (vers "0.2.0") (deps (list (crate-dep (name "bytemuck") (req "^1.15.0") (default-features #t) (kind 0)) (crate-dep (name "wgpu") (req "^0.19.3") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "wgpu-core") (req "^0.19.3") (optional #t) (default-features #t) (kind 0)))) (hash "0nsk16nha02b3pl5n3llcx0xcz4d45rwrivjsghxy91qd6139yds") (v 2) (features2 (quote (("wgpu-core" "dep:wgpu-core") ("wgpu" "dep:wgpu"))))))

(define-public crate-debug_overlay-0.2 (crate (name "debug_overlay") (vers "0.2.1") (deps (list (crate-dep (name "bytemuck") (req "^1.15.0") (default-features #t) (kind 0)) (crate-dep (name "wgpu") (req "^0.19.3") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "wgpu-core") (req "^0.19.3") (optional #t) (default-features #t) (kind 0)))) (hash "0k3r0ghhfl4fqb54s0q5fyzqm2w5qzmm4vgw0pv5qz5if0h5nx66") (v 2) (features2 (quote (("wgpu-core" "dep:wgpu-core") ("wgpu" "dep:wgpu"))))))

(define-public crate-debug_overlay-0.2 (crate (name "debug_overlay") (vers "0.2.2") (deps (list (crate-dep (name "bytemuck") (req "^1.15.0") (default-features #t) (kind 0)) (crate-dep (name "wgpu") (req "^0.19.3") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "wgpu-core") (req "^0.19.3") (optional #t) (default-features #t) (kind 0)))) (hash "0lkszhs0w6dg8qs5fcjshw7id1a9mzv65gwrlcsdw5caczhi6lnb") (v 2) (features2 (quote (("wgpu-core" "dep:wgpu-core") ("wgpu" "dep:wgpu"))))))

(define-public crate-debug_overlay-0.3 (crate (name "debug_overlay") (vers "0.3.0") (deps (list (crate-dep (name "bytemuck") (req "^1.15.0") (default-features #t) (kind 0)) (crate-dep (name "wgpu") (req "^0.19.3") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "wgpu-core") (req "^0.19.3") (optional #t) (default-features #t) (kind 0)))) (hash "0isxg8jk51bsrq97y6833mrpzv31h46ida48wjzc9213cvnd18fj") (v 2) (features2 (quote (("wgpu-core" "dep:wgpu-core") ("wgpu" "dep:wgpu"))))))

(define-public crate-debug_overlay-0.4 (crate (name "debug_overlay") (vers "0.4.0") (deps (list (crate-dep (name "bytemuck") (req "^1.15.0") (default-features #t) (kind 0)) (crate-dep (name "wgpu") (req "^0.20.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "wgpu-core") (req "^0.20.0") (optional #t) (default-features #t) (kind 0)))) (hash "1zjya8gkz5kzjb31dk8zysq0iwzcf59ysjacwiq85iagc9bas2zv") (v 2) (features2 (quote (("wgpu-core" "dep:wgpu-core") ("wgpu" "dep:wgpu"))))))

(define-public crate-debug_panic-0.1 (crate (name "debug_panic") (vers "0.1.0") (hash "1iwzsr4picfh4isf0fgx6k0r657ynknz2kqpnavcfyb10a261ha9")))

(define-public crate-debug_panic-0.1 (crate (name "debug_panic") (vers "0.1.1") (hash "0kdqxbagb0849931l4xlq0vyz1c8dzjfmm5k9i583pa59h1y3cf8")))

(define-public crate-debug_panic-0.1 (crate (name "debug_panic") (vers "0.1.2") (hash "0w93p8541rw64h3i99g4l2h82k0ivhrcg8g2239n2nslx9hc83ca")))

(define-public crate-debug_panic-0.2 (crate (name "debug_panic") (vers "0.2.1") (hash "19m2g8a55cq2pzrgjhb7i45m25n1r0p7v3gb3m1ykqpc1h8ynxwk")))

(define-public crate-debug_parser-0.1 (crate (name "debug_parser") (vers "0.1.0") (deps (list (crate-dep (name "html-escape") (req "^0.2.9") (default-features #t) (kind 0)) (crate-dep (name "nom") (req "^7.1.0") (default-features #t) (kind 0)))) (hash "13p5p739cbm297f5r04z16m3nc6z7qi6pxiskxf4x634ijh0k8x6")))

(define-public crate-debug_parser-0.1 (crate (name "debug_parser") (vers "0.1.1") (deps (list (crate-dep (name "html-escape") (req "^0.2.9") (default-features #t) (kind 0)) (crate-dep (name "nom") (req "^7.1.0") (default-features #t) (kind 0)))) (hash "00fl08gjy1y4ki9dhshf9p1f3f2zrraspd7anni0z6qkhdpj65d3") (yanked #t)))

(define-public crate-debug_parser-0.1 (crate (name "debug_parser") (vers "0.1.2") (deps (list (crate-dep (name "html-escape") (req "^0.2.9") (default-features #t) (kind 0)) (crate-dep (name "nom") (req "^7.1.0") (default-features #t) (kind 0)))) (hash "0r9lj25qvp47j0y46hdxi31qna4asb2p3w7mbw38c6hg3v05sb10")))

(define-public crate-debug_parser-0.1 (crate (name "debug_parser") (vers "0.1.4") (deps (list (crate-dep (name "html-escape") (req "^0.2.9") (default-features #t) (kind 0)) (crate-dep (name "nom") (req "^7.1.0") (default-features #t) (kind 0)))) (hash "0gb3n3wqmr5ljvqb7pk1jycqf6p5p45pbmf9iaaqfmqvs90d1pvi")))

(define-public crate-debug_plotter-0.1 (crate (name "debug_plotter") (vers "0.1.0") (deps (list (crate-dep (name "num-traits") (req "^0.2") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "once_cell") (req "^1.8") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "plotters") (req "^0.3") (optional #t) (default-features #t) (kind 0)))) (hash "1nk36j5ly8rnzc16x9vwgxvnm1wn94yi8d17kaczxi5gghiggdx1") (features (quote (("default" "debug") ("debug" "plotters" "once_cell" "num-traits"))))))

(define-public crate-debug_plotter-0.1 (crate (name "debug_plotter") (vers "0.1.1") (deps (list (crate-dep (name "num-traits") (req "^0.2") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "once_cell") (req "^1.8") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "plotters") (req "^0.3") (optional #t) (default-features #t) (kind 0)))) (hash "1ax6l1ahdk4bf77lpps8wbmra57vbrzamrw8yljrk49x8fg8bndz") (features (quote (("default" "debug") ("debug" "plotters" "once_cell" "num-traits"))))))

(define-public crate-debug_plotter-0.2 (crate (name "debug_plotter") (vers "0.2.0") (deps (list (crate-dep (name "num-traits") (req "^0.2") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "plotters") (req "^0.3") (optional #t) (default-features #t) (kind 0)))) (hash "1bspgyvcxmxz8qhpskjhx40xn79acbybix3dph3ihg49bhlkmssy") (features (quote (("default" "debug") ("debug" "plotters" "num-traits"))))))

(define-public crate-debug_plotter-0.2 (crate (name "debug_plotter") (vers "0.2.1") (deps (list (crate-dep (name "num-traits") (req "^0.2") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "plotters") (req "^0.3") (optional #t) (default-features #t) (kind 0)))) (hash "1w88m3bv5sx2p27nh75z2c2ccv9cnxxpc5c6qvcvfvfx3p2hpja4") (features (quote (("plot-release") ("default" "debug") ("debug" "plotters" "num-traits"))))))

(define-public crate-debug_print-1 (crate (name "debug_print") (vers "1.0.0") (hash "147a3q1fd4mk9nz5vdhq9nrnin3pyqqm64an6avrzx14fadmy8cg")))

(define-public crate-debug_sat-0.1 (crate (name "debug_sat") (vers "0.1.0") (hash "1nj1334a9a2l0blgqws1a4khdzah8wv29xqln513lqlrkfv161mp")))

(define-public crate-debug_sat-0.2 (crate (name "debug_sat") (vers "0.2.0") (hash "00in3zs5p5xbg4bify7gff5hx0ln35v3f1bzc7vzmb8jj3b4r9if")))

(define-public crate-debug_sat-0.3 (crate (name "debug_sat") (vers "0.3.0") (hash "0388ik4yr56b2qp5cm4m2y9k5vz42pcw4fqf1d3vy3yrvlwcbd7v")))

(define-public crate-debug_sat-0.4 (crate (name "debug_sat") (vers "0.4.0") (hash "1a62m8bf50qz52h3k6ckiali27mbrvla16b9b0d9d4myajnjzrdb")))

(define-public crate-debug_stub_derive-0.1 (crate (name "debug_stub_derive") (vers "0.1.0") (deps (list (crate-dep (name "quote") (req "^0.3.10") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^0.11") (features (quote ("visit"))) (default-features #t) (kind 0)))) (hash "0w37fjngs7h32vz2yj5sg3rjjgd63y51268g9m0yha35xh7iakp1")))

(define-public crate-debug_stub_derive-0.2 (crate (name "debug_stub_derive") (vers "0.2.0") (deps (list (crate-dep (name "quote") (req "^0.3.10") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^0.11") (features (quote ("visit"))) (default-features #t) (kind 0)))) (hash "0qixdng1ps347sgjjlbnjsak30as691a3nc8fdaa98gsy2nxshxz")))

(define-public crate-debug_stub_derive-0.3 (crate (name "debug_stub_derive") (vers "0.3.0") (deps (list (crate-dep (name "quote") (req "^0.3.10") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^0.11") (features (quote ("visit"))) (default-features #t) (kind 0)))) (hash "0szzlg89wnc40s7xmk7xfhlw6hiyzzbl21iprb1i6cw55y57yss9")))

(define-public crate-debug_tree-0.2 (crate (name "debug_tree") (vers "0.2.1") (hash "0cprnzhihc46ijdq1hdwmlnkyn5jiygmapxi8dcqajkgx7j6pa5i")))

(define-public crate-debug_tree-0.2 (crate (name "debug_tree") (vers "0.2.2") (hash "0sjx764y62p9r7mz7g0s8bn4f29v2mxx6ravs0ivrckzfflwwzz9")))

(define-public crate-debug_tree-0.2 (crate (name "debug_tree") (vers "0.2.3") (hash "07j44gvbapf5y9cf86lnjadglxah7adv9qghx945m8rhy6cq4rpg")))

(define-public crate-debug_tree-0.2 (crate (name "debug_tree") (vers "0.2.4") (hash "08466dn7gh55da71avlr32w4ax1w3x9qjkna6mx1z6v43s0znyyk")))

(define-public crate-debug_tree-0.2 (crate (name "debug_tree") (vers "0.2.5") (hash "0ir0bfz80gj31xiphc7b6lks81n3h7nqnkrl114iz90qwi1gf007")))

(define-public crate-debug_tree-0.3 (crate (name "debug_tree") (vers "0.3.0") (deps (list (crate-dep (name "once_cell") (req "~1.3.1") (default-features #t) (kind 0)))) (hash "0ariw2izsdgh8b5hj9vmxkrjih6cxd69s008fcj95x2dw0pqjxmy")))

(define-public crate-debug_tree-0.3 (crate (name "debug_tree") (vers "0.3.1") (deps (list (crate-dep (name "futures") (req "^0.3.4") (default-features #t) (kind 2)) (crate-dep (name "once_cell") (req "~1.3.1") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^0.2.9") (features (quote ("macros" "fs"))) (default-features #t) (kind 2)))) (hash "1jjiz3ny2hcqy60afdcbybwrycakihdw09sdp0bq7fz595kq0b7q")))

(define-public crate-debug_tree-0.4 (crate (name "debug_tree") (vers "0.4.0") (deps (list (crate-dep (name "futures") (req "^0.3.4") (default-features #t) (kind 2)) (crate-dep (name "once_cell") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^0.2.9") (features (quote ("macros" "fs"))) (default-features #t) (kind 2)))) (hash "0mwfkaq8sld6y5f5dp5drl9qbr0sl4xjbr1l7hnr0i6qya1w67id")))

(define-public crate-debug_types-1 (crate (name "debug_types") (vers "1.0.0") (deps (list (crate-dep (name "serde") (req "^1.0.136") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0.136") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.79") (default-features #t) (kind 0)))) (hash "1vrcs9nph9c3xxb5684yz2k4z1vyv45qqnjgx07af9xxc0vz64r6")))

(define-public crate-debug_unreachable-0.0.1 (crate (name "debug_unreachable") (vers "0.0.1") (hash "17a1pjv0kyg9n5sma8vf3h7jr003sjsv77kh3ljgw5fl56dwxy7z")))

(define-public crate-debug_unreachable-0.0.2 (crate (name "debug_unreachable") (vers "0.0.2") (hash "0c0bj5xzni7izdzgx7vmir59rvzvvqxm7sjg3gm7chirc8pyj89a")))

(define-public crate-debug_unreachable-0.0.3 (crate (name "debug_unreachable") (vers "0.0.3") (hash "07f9i0mhha2c88mzmwsgk86zgkxpmrlppnlmswv0apy8h7z1fcax")))

(define-public crate-debug_unreachable-0.0.4 (crate (name "debug_unreachable") (vers "0.0.4") (hash "14dawd721krspzk2k9502b1msz9d7amlgcf3n3pq8vn7gbb8n96a")))

(define-public crate-debug_unreachable-0.0.5 (crate (name "debug_unreachable") (vers "0.0.5") (deps (list (crate-dep (name "unreachable") (req "^0") (default-features #t) (kind 0)))) (hash "1lldjhb98brkbiq7rcgcnz06f2kz95pkx198nhscrm309rqmrhsl")))

(define-public crate-debug_unreachable-0.0.6 (crate (name "debug_unreachable") (vers "0.0.6") (deps (list (crate-dep (name "unreachable") (req "^0") (default-features #t) (kind 0)))) (hash "0y5zis8pdzjicnvb9l09fd9738p45b2mm25n1srhpjbz6l8pgw46")))

(define-public crate-debug_unreachable-0.1 (crate (name "debug_unreachable") (vers "0.1.0") (deps (list (crate-dep (name "unreachable") (req "^0") (default-features #t) (kind 0)))) (hash "0nx61j5ayzrbdzha1nxdyp49nmbywhw0vmhfv1k1in8bwwbqxs4h")))

(define-public crate-debug_unreachable-0.1 (crate (name "debug_unreachable") (vers "0.1.1") (deps (list (crate-dep (name "unreachable") (req "^0.1") (default-features #t) (kind 0)))) (hash "1cx4vh1d0bxdpb8l9lyiv03az4527lykxy39s4a958swf2n2w0ws")))

(define-public crate-debug_unwrap-0.0.1 (crate (name "debug_unwrap") (vers "0.0.1") (hash "1ydqk9k1zfnxk0gq4mnac9y2r6719bg8rgcha5qw9qpsh1v36y3i") (yanked #t)))

(define-public crate-debug_unwrap-1 (crate (name "debug_unwrap") (vers "1.0.0") (hash "10na7xs7vqvb3ncmy6z82cqfmih3p6vmnm7jv16ap8q4jwydrj11") (features (quote (("peel") ("out") ("o") ("default" "out"))))))

(define-public crate-debug_unwrap-1 (crate (name "debug_unwrap") (vers "1.0.1") (hash "1a5l36k3zd1ji9y0imfj6g6ydfl35m9wz4fzn1ysmk81kr627k4n") (features (quote (("peel") ("out") ("o") ("default" "out"))))))

(define-public crate-debug_unwrap-1 (crate (name "debug_unwrap") (vers "1.0.2") (hash "1xnppv9kav5p7cgjy9ck6p6gkp1gd9pnidrsfrgrwanzw4rk6vbv") (features (quote (("peel") ("out") ("o") ("default" "out"))))))

(define-public crate-debug_unwrap-1 (crate (name "debug_unwrap") (vers "1.0.3") (hash "027hdifsx0mbv1r4h9j6yjhvx68sn2k8fmmij9wjd79if1yn98g7") (features (quote (("peel") ("out") ("o") ("deprecate") ("default" "out"))))))

(define-public crate-debug_unwraps-0.1 (crate (name "debug_unwraps") (vers "0.1.0") (hash "1ln1xh86sq9hrm9blyvizdmm0v5fv7frgsi4kws7d80ns3kv0da0")))

(define-public crate-debug_utils-0.1 (crate (name "debug_utils") (vers "0.1.0") (hash "0kdsfdmjybfl20p8nz5rlqmak8yrzzzlqivmr17m6fjzjc0c07rq")))

(define-public crate-debug_utils-0.1 (crate (name "debug_utils") (vers "0.1.1") (hash "1xbvrc4gk69c9vakxbz5i7918a55v60jdvawshlj24vkahhfx5k9")))

(define-public crate-debug_utils-0.1 (crate (name "debug_utils") (vers "0.1.2") (hash "0ja0j9rb8bv8zzdk6dlsanvbvdmvhnn99b4nsi59jrjhryf0z1rk")))

(define-public crate-debugger-0.0.1 (crate (name "debugger") (vers "0.0.1") (deps (list (crate-dep (name "capstone") (req "^0.11.0") (default-features #t) (kind 0)) (crate-dep (name "crossterm") (req "^0.25.0") (default-features #t) (kind 0)) (crate-dep (name "fireball") (req "^0.0.1") (default-features #t) (kind 0)) (crate-dep (name "tempdir") (req "^0.3.7") (default-features #t) (kind 0)))) (hash "1n2rk6xs8qhsyz317ww1y6swiwlp0ixv8m2n49f44z9qg3b74aby")))

(define-public crate-debugger_test-0.1 (crate (name "debugger_test") (vers "0.1.0") (deps (list (crate-dep (name "anyhow") (req "^1.0.40") (default-features #t) (kind 0)) (crate-dep (name "debugger_test_parser") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.17") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.20") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.6.0") (default-features #t) (kind 2)) (crate-dep (name "strum") (req "^0.24") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "1aacp5n2pppl302z1z90ly8v4w88a1d72qng02a9645ijywx32l5") (yanked #t)))

(define-public crate-debugger_test-0.1 (crate (name "debugger_test") (vers "0.1.1") (deps (list (crate-dep (name "anyhow") (req "^1.0.40") (default-features #t) (kind 0)) (crate-dep (name "debugger_test_parser") (req "^0.1.0") (default-features #t) (kind 2)) (crate-dep (name "log") (req "^0.4.17") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.20") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.6.0") (default-features #t) (kind 2)) (crate-dep (name "strum") (req "^0.24") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "0y7wx8ph5sgcj8yq9v790vichavxdb7varvalj0apaf8p6gnh3va") (yanked #t)))

(define-public crate-debugger_test-0.1 (crate (name "debugger_test") (vers "0.1.2") (deps (list (crate-dep (name "anyhow") (req "^1.0.40") (default-features #t) (kind 0)) (crate-dep (name "debugger_test_parser") (req "^0.1.0") (default-features #t) (kind 2)) (crate-dep (name "log") (req "^0.4.17") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.20") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.6.0") (default-features #t) (kind 2)) (crate-dep (name "strum") (req "^0.24") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "0c78hd8kdd7ffprjqrh6jnp0i0azaqx28bfpb788lf0srf89w5n5") (yanked #t)))

(define-public crate-debugger_test-0.1 (crate (name "debugger_test") (vers "0.1.3") (deps (list (crate-dep (name "anyhow") (req "^1.0.40") (default-features #t) (kind 0)) (crate-dep (name "debugger_test_parser") (req "^0.1.0") (default-features #t) (kind 2)) (crate-dep (name "log") (req "^0.4.17") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.20") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.6.0") (default-features #t) (kind 2)) (crate-dep (name "strum") (req "^0.24") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "1a3kc544ssyccy8x0z20j9z2slza5mf8pcypkgr4fga4yhcq8skn") (yanked #t)))

(define-public crate-debugger_test-0.1 (crate (name "debugger_test") (vers "0.1.4") (deps (list (crate-dep (name "anyhow") (req "^1.0.40") (default-features #t) (kind 0)) (crate-dep (name "debugger_test_parser") (req "^0.1.0") (default-features #t) (kind 2)) (crate-dep (name "log") (req "^0.4.17") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.20") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.6.0") (default-features #t) (kind 2)) (crate-dep (name "strum") (req "^0.24") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "13kaijcxd6fjbh7xinnczyairjrk19rfdgdkvjw451n4r5qs4qbz")))

(define-public crate-debugger_test-0.1 (crate (name "debugger_test") (vers "0.1.5") (deps (list (crate-dep (name "anyhow") (req "^1.0.40") (default-features #t) (kind 0)) (crate-dep (name "debugger_test_parser") (req "^0.1.0") (default-features #t) (kind 2)) (crate-dep (name "log") (req "^0.4.17") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.20") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.6.0") (default-features #t) (kind 2)) (crate-dep (name "syn") (req "^1.0") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "182j5sk71yhbn7f5qkx509bdcjz83n1nshpfgfa8dfrgb5gvanyr")))

(define-public crate-debugger_test_parser-0.1 (crate (name "debugger_test_parser") (vers "0.1.0") (deps (list (crate-dep (name "anyhow") (req "^1.0.58") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.17") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.6.0") (default-features #t) (kind 0)))) (hash "1z5hxyafq8hh3sc9vikzlb42p3ygnxr8hr8lm4c0vg4kiaryj723") (yanked #t)))

(define-public crate-debugger_test_parser-0.1 (crate (name "debugger_test_parser") (vers "0.1.1") (deps (list (crate-dep (name "anyhow") (req "^1.0.58") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.17") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.6.0") (default-features #t) (kind 0)))) (hash "1crdckrp7lgk1rg6vnf69ssvnxw96k0k2z39pqfdh6gq546qqmrv") (yanked #t)))

(define-public crate-debugger_test_parser-0.1 (crate (name "debugger_test_parser") (vers "0.1.2") (deps (list (crate-dep (name "anyhow") (req "^1.0.58") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.17") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.6.0") (default-features #t) (kind 0)))) (hash "1arab1fznh5lphnqyyz9kddwh861rqrx29gfwpqxsab65prh3bvr") (yanked #t)))

(define-public crate-debugger_test_parser-0.1 (crate (name "debugger_test_parser") (vers "0.1.3") (deps (list (crate-dep (name "anyhow") (req "^1.0.58") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.17") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.6.0") (default-features #t) (kind 0)))) (hash "0xcyxw0si7j4h701aksdd08j8jmrzc58833g66wm4xvp592kdrgb")))

(define-public crate-debugging_helpers-0.1 (crate (name "debugging_helpers") (vers "0.1.0") (deps (list (crate-dep (name "colored") (req "^2.0.4") (default-features #t) (kind 0)) (crate-dep (name "rstest") (req "^0.18.2") (default-features #t) (kind 2)))) (hash "1awdnv78dsykmxa9y8238519vmqk06xx8cspqqaibk1acyzqy55k")))

(define-public crate-debugid-0.1 (crate (name "debugid") (vers "0.1.0") (deps (list (crate-dep (name "lazy_static") (req "^1.0.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^0.2.10") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.36") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde_plain") (req "^0.3.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "uuid") (req "^0.6.2") (default-features #t) (kind 0)))) (hash "1j67lay3pfy6r3ffq2d8yd6m28n7bp33xk2zjp89c7pfc5ppwrkq") (features (quote (("with_serde" "serde" "serde_plain") ("default"))))))

(define-public crate-debugid-0.1 (crate (name "debugid") (vers "0.1.1") (deps (list (crate-dep (name "lazy_static") (req "^1.0.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^0.2.10") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.36") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.16") (default-features #t) (kind 2)) (crate-dep (name "uuid") (req "^0.6.2") (default-features #t) (kind 0)))) (hash "04bnwspjq7n7dy8hj4gyvb9b9mc4vbwfzih9ryshfhdyv32nz3ih") (features (quote (("with_serde" "serde") ("default"))))))

(define-public crate-debugid-0.2 (crate (name "debugid") (vers "0.2.0") (deps (list (crate-dep (name "lazy_static") (req "^1.0.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^0.2.10") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.36") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.16") (default-features #t) (kind 2)) (crate-dep (name "uuid") (req "^0.6.2") (default-features #t) (kind 0)))) (hash "1q2820bqylpl99iigrswzdjfi10jlvy91xjpy5y4xgzfwf39di2p") (features (quote (("with_serde" "serde") ("default"))))))

(define-public crate-debugid-0.3 (crate (name "debugid") (vers "0.3.0") (deps (list (crate-dep (name "lazy_static") (req "^1.1.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.0.5") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.78") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.27") (default-features #t) (kind 2)) (crate-dep (name "uuid") (req "^0.7.0") (default-features #t) (kind 0)))) (hash "0cycrajjq2ij07lja8665l9xfcz1xcv949icw0lzr0y77i2ilnfd") (features (quote (("with_serde" "serde") ("default"))))))

(define-public crate-debugid-0.3 (crate (name "debugid") (vers "0.3.1") (deps (list (crate-dep (name "lazy_static") (req "^1.1.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.0.5") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.79") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.32") (default-features #t) (kind 2)) (crate-dep (name "uuid") (req "^0.7.1") (default-features #t) (kind 0)))) (hash "1z4jz7w5j9snpdcj7w1hs7i5imni5z1gjiqmcj397xbqj6x8ic7f") (features (quote (("with_serde" "serde") ("default"))))))

(define-public crate-debugid-0.4 (crate (name "debugid") (vers "0.4.0") (deps (list (crate-dep (name "lazy_static") (req "^1.2.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.1.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.85") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.37") (default-features #t) (kind 2)) (crate-dep (name "uuid") (req "^0.7.1") (default-features #t) (kind 0)))) (hash "1sg31gi6xcb8xjh584k37l197676y5vp79yyz57lj7pcmlkrd308") (features (quote (("with_serde" "serde") ("default"))))))

(define-public crate-debugid-0.5 (crate (name "debugid") (vers "0.5.0") (deps (list (crate-dep (name "lazy_static") (req "^1.2.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.1.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.85") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.37") (default-features #t) (kind 2)) (crate-dep (name "uuid") (req "^0.7.1") (default-features #t) (kind 0)))) (hash "0apps5c9fjibzz3pyd26jfcp6jjmk4vs34pl6y6m6ganqvyijl8s")))

(define-public crate-debugid-0.5 (crate (name "debugid") (vers "0.5.1") (deps (list (crate-dep (name "lazy_static") (req "^1.2.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.1.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.85") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.37") (default-features #t) (kind 2)) (crate-dep (name "uuid") (req "^0.7.1") (default-features #t) (kind 0)))) (hash "0qa83xgx3cwhqh4sj7f7dkyxl7nanll6li2bwj9gcwpng8rigbrm")))

(define-public crate-debugid-0.6 (crate (name "debugid") (vers "0.6.0") (deps (list (crate-dep (name "lazy_static") (req "^1.2.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.1.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.85") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.37") (default-features #t) (kind 2)) (crate-dep (name "uuid") (req "^0.7.1") (default-features #t) (kind 0)))) (hash "1p931x6y9lch5xxx1fdn1l23py6wd7y0ghy1ih8n2ym7ggxdcg0l") (yanked #t)))

(define-public crate-debugid-0.5 (crate (name "debugid") (vers "0.5.2") (deps (list (crate-dep (name "lazy_static") (req "^1.2.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.1.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.85") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.37") (default-features #t) (kind 2)) (crate-dep (name "uuid") (req "^0.7.1") (default-features #t) (kind 0)))) (hash "1ipckq5a3zp4kbyq3xdr1djvhf3vp91180jx8prwmxzqrklmjbdm")))

(define-public crate-debugid-0.5 (crate (name "debugid") (vers "0.5.3") (deps (list (crate-dep (name "lazy_static") (req "^1.2.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.1.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.85") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.37") (default-features #t) (kind 2)) (crate-dep (name "uuid") (req "^0.7.1") (default-features #t) (kind 0)))) (hash "1x3xx40g5jppr5d0k6sw9082hvn4gaf14ci24rvslqxi8w9ss7bm")))

(define-public crate-debugid-0.7 (crate (name "debugid") (vers "0.7.0") (deps (list (crate-dep (name "lazy_static") (req "^1.2.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.1.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.85") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.37") (default-features #t) (kind 2)) (crate-dep (name "uuid") (req "^0.8.1") (default-features #t) (kind 0)))) (hash "0jzad3k1mmhy562rkb73a8crk191rbix3k26nb382xb0v7l2r7n4")))

(define-public crate-debugid-0.7 (crate (name "debugid") (vers "0.7.1") (deps (list (crate-dep (name "lazy_static") (req "^1.2.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.1.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.85") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.37") (default-features #t) (kind 2)) (crate-dep (name "uuid") (req "^0.8.1") (default-features #t) (kind 0)))) (hash "1z1am6i55gyskqv1pbnmcpk5mdpdva929wrjg3hlfxrxcqr4ha9n")))

(define-public crate-debugid-0.7 (crate (name "debugid") (vers "0.7.2") (deps (list (crate-dep (name "serde") (req "^1.0.85") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.37") (default-features #t) (kind 2)) (crate-dep (name "uuid") (req "^0.8.1") (default-features #t) (kind 0)))) (hash "1fh2nl4dzbcm3bn9knb30phprm1mhr826wb268m7w2gjqalga77r")))

(define-public crate-debugid-0.7 (crate (name "debugid") (vers "0.7.3") (deps (list (crate-dep (name "serde") (req "^1.0.85") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.37") (default-features #t) (kind 2)) (crate-dep (name "uuid") (req "^0.8.1") (default-features #t) (kind 0)))) (hash "0c370jsmnb0ahssda6f11l72ns1xpqrcmswa6y2zhknq66pqgvnn")))

(define-public crate-debugid-0.8 (crate (name "debugid") (vers "0.8.0") (deps (list (crate-dep (name "serde") (req "^1.0.85") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.37") (default-features #t) (kind 2)) (crate-dep (name "uuid") (req "^1.0.0") (default-features #t) (kind 0)))) (hash "13f15dfvn07fa7087pmacixqqv0lmj4hv93biw4ldr48ypk55xdy")))

(define-public crate-debugify-0.1 (crate (name "debugify") (vers "0.1.0") (deps (list (crate-dep (name "proc-macro2") (req "^1.0.27") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.33") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2.0.40") (features (quote ("full" "extra-traits"))) (default-features #t) (kind 0)))) (hash "1w0hnnd9kf4dkhlrjbwfhk2ikmzp2vlkl2dgr5y0s54hi442ls0p")))

(define-public crate-debugify-0.2 (crate (name "debugify") (vers "0.2.0") (deps (list (crate-dep (name "proc-macro2") (req "^1.0.27") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.33") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2.0.40") (features (quote ("full" "extra-traits"))) (default-features #t) (kind 0)))) (hash "12mv125bhfnh0lh2nv5zzm2xd6w5gmlv50ndixmqf33g048q37lf")))

(define-public crate-debuginfo-split-0.1 (crate (name "debuginfo-split") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^4.1.4") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "core-wasm-ast") (req "^0.1.17") (default-features #t) (kind 0)) (crate-dep (name "wasm-parser") (req "^0.1.17") (default-features #t) (kind 0)) (crate-dep (name "wasm-printer") (req "^0.1.17") (default-features #t) (kind 0)))) (hash "174w08bv2ickx3xz6sy8pys35h1z7dkzaj423f0dg4gkg5alnnl9")))

(define-public crate-debuginfo-split-0.1 (crate (name "debuginfo-split") (vers "0.1.18") (deps (list (crate-dep (name "clap") (req "^4.1.4") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "core-wasm-ast") (req "^0.1.18") (default-features #t) (kind 0)) (crate-dep (name "uuid") (req "^1.4.1") (features (quote ("v4"))) (default-features #t) (kind 0)) (crate-dep (name "wasm-parser") (req "^0.1.18") (default-features #t) (kind 0)) (crate-dep (name "wasm-printer") (req "^0.1.18") (default-features #t) (kind 0)))) (hash "1r4kbq6aycjg13k7yax3zxlnzd70514skwaszv6szzs655hmnr6q")))

(define-public crate-debuginfo-split-0.1 (crate (name "debuginfo-split") (vers "0.1.19") (deps (list (crate-dep (name "clap") (req "^4.1.4") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "core-wasm-ast") (req "^0.1.19") (default-features #t) (kind 0)) (crate-dep (name "uuid") (req "^1.4.1") (features (quote ("v4"))) (default-features #t) (kind 0)) (crate-dep (name "wasm-parser") (req "^0.1.19") (default-features #t) (kind 0)) (crate-dep (name "wasm-printer") (req "^0.1.19") (default-features #t) (kind 0)))) (hash "1n08cdprzdj8r2sb43ggmpd53nvv5gkd8hcjg5bfpsh6jvy4q2n4")))

(define-public crate-debuginfo-split-0.1 (crate (name "debuginfo-split") (vers "0.1.20") (deps (list (crate-dep (name "clap") (req "^4.1.4") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "core-wasm-ast") (req "^0.1.20") (default-features #t) (kind 0)) (crate-dep (name "uuid") (req "^1.4.1") (features (quote ("v4"))) (default-features #t) (kind 0)) (crate-dep (name "wasm-parser") (req "^0.1.20") (default-features #t) (kind 0)) (crate-dep (name "wasm-printer") (req "^0.1.20") (default-features #t) (kind 0)))) (hash "1na0nyhk4g6kzd1yp6jbchby8ir3rzz9b01pfpibrzr0aa9n7b6x")))

(define-public crate-debuginfo-split-0.1 (crate (name "debuginfo-split") (vers "0.1.21") (deps (list (crate-dep (name "clap") (req "^4.1.4") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "core-wasm-ast") (req "^0.1.21") (default-features #t) (kind 0)) (crate-dep (name "uuid") (req "^1.4.1") (features (quote ("v4"))) (default-features #t) (kind 0)) (crate-dep (name "wasm-parser") (req "^0.1.21") (default-features #t) (kind 0)) (crate-dep (name "wasm-printer") (req "^0.1.21") (default-features #t) (kind 0)))) (hash "092i9v4wjvj819bcs16id805yqqj16p0q5lswh453manprv8f175")))

(define-public crate-debuginfo-split-0.1 (crate (name "debuginfo-split") (vers "0.1.22") (deps (list (crate-dep (name "clap") (req "^4.1.4") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "core-wasm-ast") (req "^0.1.22") (default-features #t) (kind 0)) (crate-dep (name "uuid") (req "^1.4.1") (features (quote ("v4"))) (default-features #t) (kind 0)) (crate-dep (name "wasm-parser") (req "^0.1.22") (default-features #t) (kind 0)) (crate-dep (name "wasm-printer") (req "^0.1.22") (default-features #t) (kind 0)))) (hash "13iay52rbznp9wdyf7lh9av926jyn1xmi2yljc91rss80dj1pyx2")))

(define-public crate-debuginfod-0.0.0 (crate (name "debuginfod") (vers "0.0.0") (hash "0g068n01danz6gjyhbkl2faz7rriwkzcdpdhc8g6xxmylb03n1fp")))

(define-public crate-debuginfod-0.1 (crate (name "debuginfod") (vers "0.1.0") (deps (list (crate-dep (name "_log_unused") (req "^0.4.6") (default-features #t) (kind 2) (package "log")) (crate-dep (name "_openssl_unused") (req "^0.10.35") (default-features #t) (kind 2) (package "openssl")) (crate-dep (name "_rustc_version_unused") (req "^0.2.2") (default-features #t) (kind 2) (package "rustc_version")) (crate-dep (name "anyhow") (req "^1.0.68") (default-features #t) (kind 0)) (crate-dep (name "blazesym") (req "^0.2.0-alpha.11") (kind 2)) (crate-dep (name "reqwest") (req "^0.12.4") (features (quote ("blocking" "gzip"))) (default-features #t) (kind 0)) (crate-dep (name "tempfile") (req "^3.10.1") (optional #t) (kind 0)) (crate-dep (name "tracing") (req "^0.1.27") (optional #t) (kind 0)))) (hash "0wyyw4jwpb7i9sl6xk33jvfj97hfmla4yz345cx98b67b4cyinps") (features (quote (("default" "fs-cache")))) (v 2) (features2 (quote (("fs-cache" "dep:tempfile")))) (rust-version "1.64")))

(define-public crate-debuginfod-rs-0.1 (crate (name "debuginfod-rs") (vers "0.1.0") (deps (list (crate-dep (name "anyhow") (req "^1.0.75") (default-features #t) (kind 0)) (crate-dep (name "bytesize") (req "^1.3.0") (default-features #t) (kind 0)) (crate-dep (name "bzip2") (req "^0.4.4") (default-features #t) (kind 0)) (crate-dep (name "cpio") (req "^0.2.2") (default-features #t) (kind 0)) (crate-dep (name "elf") (req "^0.7.3") (default-features #t) (kind 0)) (crate-dep (name "env_logger") (req "^0.10.0") (default-features #t) (kind 0)) (crate-dep (name "flate2") (req "^1.0.28") (default-features #t) (kind 0)) (crate-dep (name "hex") (req "^0.4.3") (default-features #t) (kind 0)) (crate-dep (name "itertools") (req "^0.12.0") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2.150") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.20") (default-features #t) (kind 0)) (crate-dep (name "path-absolutize") (req "^3.1.1") (default-features #t) (kind 0)) (crate-dep (name "rayon") (req "^1.8.0") (default-features #t) (kind 0)) (crate-dep (name "rocket") (req "^0.5.0") (default-features #t) (kind 0)) (crate-dep (name "rpm") (req "^0.13.1") (default-features #t) (kind 0)) (crate-dep (name "walkdir") (req "^2.4.0") (default-features #t) (kind 0)) (crate-dep (name "xz2") (req "^0.1.7") (default-features #t) (kind 0)) (crate-dep (name "zstd") (req "^0.13.0") (default-features #t) (kind 0)))) (hash "1l4dry17mhdq5088nr4xnmnhi8734qzgpcxdigh6zzis7xds09v5")))

(define-public crate-debugit-0.1 (crate (name "debugit") (vers "0.1.0") (deps (list (crate-dep (name "rustc_version") (req "^0.1.7") (default-features #t) (kind 1)))) (hash "1p40n97nqyyhsx6qm2qnrrlzznkxazn29c6qxwb3s3qgh69ab1y7")))

(define-public crate-debugit-0.1 (crate (name "debugit") (vers "0.1.1") (deps (list (crate-dep (name "rustc_version") (req "^0.1.7") (default-features #t) (kind 1)))) (hash "0awa9x9qizb6f1s9l381dkx5rf6jxjnnjrlj2qk12zxzx5ql8i2p")))

(define-public crate-debugit-0.1 (crate (name "debugit") (vers "0.1.2") (deps (list (crate-dep (name "version_check") (req "^0.1") (default-features #t) (kind 1)))) (hash "1qr5d2j0zk2gpc2qyf5pwq0k7qyz38yf49q3fngv1wjd0gizghk3")))

(define-public crate-debugit-0.2 (crate (name "debugit") (vers "0.2.0") (hash "09pi91knrdkjcnrsvsssz4a3l223frpz2w3375a3bhrnsmdr0rr3")))

(define-public crate-debugless-unwrap-0.0.1 (crate (name "debugless-unwrap") (vers "0.0.1") (hash "1dv2q3zhny6f1bjybkqg9zz9aybs6j72jaxygvj8lqha9ciwr7x5")))

(define-public crate-debugless-unwrap-0.0.2 (crate (name "debugless-unwrap") (vers "0.0.2") (deps (list (crate-dep (name "assert-panic") (req "^1.0.1") (default-features #t) (kind 2)))) (hash "0b4cg8cj0rg5gbrwgnyl777al3si3jwbq576hnhx1275fyzlbpqw")))

(define-public crate-debugless-unwrap-0.0.3 (crate (name "debugless-unwrap") (vers "0.0.3") (deps (list (crate-dep (name "assert-panic") (req "^1.0.1") (default-features #t) (kind 2)) (crate-dep (name "doc-comment") (req "^0.3.3") (default-features #t) (kind 2)))) (hash "199vfr65ijwnlz8ic76q5yp73km55glp9ippgi1jqhak36llgvm3")))

(define-public crate-debugless-unwrap-0.0.4 (crate (name "debugless-unwrap") (vers "0.0.4") (deps (list (crate-dep (name "assert-panic") (req "^1.0.1") (default-features #t) (kind 2)) (crate-dep (name "cargo-husky") (req "^1.5.0") (default-features #t) (kind 2)) (crate-dep (name "doc-comment") (req "^0.3.3") (default-features #t) (kind 2)) (crate-dep (name "git_info") (req "^0.1.2") (default-features #t) (kind 2)) (crate-dep (name "version-sync") (req "^0.9.1") (default-features #t) (kind 2)))) (hash "00445wsxxb33i35a0sgb5rnlnq3gvas6q9gjjf29w1hc1isx007l")))

(define-public crate-debugln-0.0.1 (crate (name "debugln") (vers "0.0.1") (hash "1ag8r641vipnqgwy38cnnaw48f3017b1rgk37b18xzyhfbngqzxd")))

(define-public crate-debugoff-0.1 (crate (name "debugoff") (vers "0.1.0") (deps (list (crate-dep (name "const-random") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "crunchy") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "goldberg") (req "^0.1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "unroll") (req "^0.1") (default-features #t) (kind 0)))) (hash "0z8pzan6razjpq08w02m3l8vyk3y9sd20ry6qgfjgq6avyhn3l8w") (features (quote (("obfuscate" "goldberg"))))))

(define-public crate-debugoff-0.1 (crate (name "debugoff") (vers "0.1.1") (deps (list (crate-dep (name "const-random") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "crunchy") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "goldberg") (req "^0.1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "unroll") (req "^0.1") (default-features #t) (kind 0)))) (hash "0wyj4vps7ai9ad0vkrknykvrf48wafq953b919wbgnv7x5ryvf5b") (features (quote (("obfuscate" "goldberg"))))))

(define-public crate-debugoff-0.2 (crate (name "debugoff") (vers "0.2.0") (deps (list (crate-dep (name "const-random") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "crunchy") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "goldberg") (req "^0.1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "unroll") (req "^0.1") (default-features #t) (kind 0)))) (hash "14q3cn01hy4dqpxfyc2h56xnzm3zr754czwfx4w0hjcl4sxlrrk4") (features (quote (("obfuscate" "goldberg"))))))

(define-public crate-debugoff-0.2 (crate (name "debugoff") (vers "0.2.1") (deps (list (crate-dep (name "const-random") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "crunchy") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "goldberg") (req "^0.1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "unroll") (req "^0.1") (default-features #t) (kind 0)))) (hash "0rjb69rffdh8y02cdrj1pzz5lphxg3b8a18zpfgjs9mdkr4235w2") (features (quote (("syscallobf") ("obfuscate" "goldberg"))))))

(define-public crate-debugoff-0.2 (crate (name "debugoff") (vers "0.2.2") (deps (list (crate-dep (name "const-random") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "crunchy") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "goldberg") (req "^0.1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "unroll") (req "^0.1") (default-features #t) (kind 0)))) (hash "1ziddv0wmf36niknwpqs4ca1k4rgk8bpsxdpbavfnfnz5vagad51") (features (quote (("syscallobf") ("obfuscate" "goldberg"))))))

(define-public crate-debugprotector-0.1 (crate (name "debugprotector") (vers "0.1.0") (deps (list (crate-dep (name "winapi") (req "^0.3.9") (features (quote ("debugapi" "handleapi" "winuser" "processthreadsapi" "tlhelp32"))) (default-features #t) (kind 0)) (crate-dep (name "winsafe") (req "^0.0.13") (features (quote ("kernel"))) (default-features #t) (kind 0)))) (hash "1jxxx963ck1mf7vgkndppw3ws51sdnqjcvqb2lbyfprwyrdlyjvp") (yanked #t)))

(define-public crate-debugprotector-0.1 (crate (name "debugprotector") (vers "0.1.2") (deps (list (crate-dep (name "winapi") (req "^0.3.9") (features (quote ("debugapi" "handleapi" "winuser" "processthreadsapi" "tlhelp32"))) (default-features #t) (kind 0)) (crate-dep (name "winsafe") (req "^0.0.13") (features (quote ("kernel"))) (default-features #t) (kind 0)))) (hash "12sa800nfvl4dpwf5ca5z1mzyplcmlxfcmbq9r258fjhb7c1y7lz")))

(define-public crate-debugprotector-0.1 (crate (name "debugprotector") (vers "0.1.3") (deps (list (crate-dep (name "winapi") (req "^0.3.9") (features (quote ("debugapi" "handleapi" "winuser" "processthreadsapi" "tlhelp32"))) (default-features #t) (kind 0)) (crate-dep (name "winsafe") (req "^0.0.13") (features (quote ("kernel"))) (default-features #t) (kind 0)))) (hash "16pk87wixf919fnpr8s3wqas0bcwb49wvpdjhgm7j948qk852gvs")))

(define-public crate-debugprotector-0.1 (crate (name "debugprotector") (vers "0.1.4") (deps (list (crate-dep (name "obfstr") (req "^0.4.1") (default-features #t) (kind 0)) (crate-dep (name "winapi") (req "^0.3.9") (features (quote ("debugapi" "handleapi" "winuser" "processthreadsapi" "tlhelp32"))) (default-features #t) (kind 0)) (crate-dep (name "winsafe") (req "^0.0.13") (features (quote ("kernel"))) (default-features #t) (kind 0)))) (hash "1i7rf1kha4nnxmbqw2w8cm4gwpkp5gpka26z88wxysj4145azxny")))

(define-public crate-debugs_tools-0.1 (crate (name "debugs_tools") (vers "0.1.0") (hash "15qj2zj4cnwcx67n86gzdw1dhc6yjg88wwjp65r7y6cic527fc6p")))

(define-public crate-debugs_tools-0.2 (crate (name "debugs_tools") (vers "0.2.0") (hash "1a2f774hr4czxm0ys0jgskm8nqv5rc01q5b0ibw6k10rk4zwlbrf")))

(define-public crate-debugs_tools-0.2 (crate (name "debugs_tools") (vers "0.2.1") (hash "1n27yqhw61q44mvyy7fdr05q2g6f9wdfc8yiwmm4s756q464hvah")))

(define-public crate-debugserver-types-0.2 (crate (name "debugserver-types") (vers "0.2.0") (deps (list (crate-dep (name "schemafy") (req "^0.2.0") (default-features #t) (kind 1)) (crate-dep (name "serde") (req "^0.9") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^0.9") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^0.9") (default-features #t) (kind 0)))) (hash "1k0wy3hf0lkd76bxz13p9wdzdvw9xf1fpp7pja6w29fl6wh50c5p")))

(define-public crate-debugserver-types-0.1 (crate (name "debugserver-types") (vers "0.1.0") (deps (list (crate-dep (name "schemafy") (req "^0.1.1") (default-features #t) (kind 1)) (crate-dep (name "serde") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^0.8") (default-features #t) (kind 0)))) (hash "00kakwiznlyq5licacacl7kbwvfqwdkjydl96l00xgj03n7ivmyc")))

(define-public crate-debugserver-types-0.3 (crate (name "debugserver-types") (vers "0.3.0") (deps (list (crate-dep (name "schemafy") (req "^0.3.0") (default-features #t) (kind 1)) (crate-dep (name "serde") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)))) (hash "15y8mg4g0yfd6zlx45mgv2y80d4cakx6kfqz81xhdjfad9rqs57n")))

(define-public crate-debugserver-types-0.4 (crate (name "debugserver-types") (vers "0.4.0") (deps (list (crate-dep (name "schemafy") (req "^0.4.0") (default-features #t) (kind 1)) (crate-dep (name "serde") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)))) (hash "0xv0a3kryn3ph7gl2cl8kcs24jirsmdsdkdq0rbznsns9r3rr83m")))

(define-public crate-debugserver-types-0.5 (crate (name "debugserver-types") (vers "0.5.0") (deps (list (crate-dep (name "schemafy") (req "^0.5.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)))) (hash "0jmgv2f77y1k20gldkvf3w7ibshb34kxz0hqwkjfh57df1587xib")))

(define-public crate-debugtrace-0.1 (crate (name "debugtrace") (vers "0.1.0") (deps (list (crate-dep (name "backtrace") (req "^0.1.8") (default-features #t) (kind 0)))) (hash "1wskqmnv0fg3clg2wvjdqq3jkcxgsh7abn7byqbh7my5hfyk5r32")))

(define-public crate-debugui-0.1 (crate (name "debugui") (vers "0.1.0") (deps (list (crate-dep (name "egui") (req "^0.22") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "egui-wgpu") (req "^0.22") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "egui-winit") (req "^0.22") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "linkme") (req "^0.3") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "parking_lot") (req "^0.12") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "pollster") (req "^0.3") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "wgpu") (req "^0.16") (optional #t) (default-features #t) (kind 0)))) (hash "0z07w7fwps9rr9grh2q42fr1f0bi4rr63dng9523587zwnvd6j55") (features (quote (("enabled" "egui" "egui-winit" "egui-wgpu" "linkme" "parking_lot" "pollster" "wgpu"))))))

