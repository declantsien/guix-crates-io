(define-module (crates-io it yb) #:use-module (crates-io))

(define-public crate-itybity-0.1 (crate (name "itybity") (vers "0.1.0") (deps (list (crate-dep (name "rstest") (req "^0.17") (default-features #t) (kind 2)))) (hash "15rx97aa4kfidr6n8lydbv9q5xxkgw1crvpsn11lk0lszgrcqfi7") (features (quote (("std" "alloc") ("default" "std") ("alloc"))))))

(define-public crate-itybity-0.1 (crate (name "itybity") (vers "0.1.1") (deps (list (crate-dep (name "rstest") (req "^0.17") (default-features #t) (kind 2)))) (hash "1iw7dpr6ljnkwya7l4mf9cm63kvxp76737zn5plk7gi2f9c87fr3") (features (quote (("std" "alloc") ("default" "std") ("alloc"))))))

(define-public crate-itybity-0.1 (crate (name "itybity") (vers "0.1.2") (deps (list (crate-dep (name "rstest") (req "^0.17") (default-features #t) (kind 2)))) (hash "1ki908lxw3r24q8xzg0fq33c1asfsvxdwfg9r0wbpfshxhj7b8x4") (features (quote (("std" "alloc") ("default" "std") ("alloc"))))))

(define-public crate-itybity-0.1 (crate (name "itybity") (vers "0.1.3") (deps (list (crate-dep (name "rstest") (req "^0.17") (default-features #t) (kind 2)))) (hash "1nmm1vks22icxr23q760n9pzgb612wgg14ysp7pz9bv3j973sii2") (features (quote (("std" "alloc") ("default" "std") ("alloc"))))))

(define-public crate-itybity-0.1 (crate (name "itybity") (vers "0.1.4") (deps (list (crate-dep (name "rstest") (req "^0.17") (default-features #t) (kind 2)))) (hash "0phkavg3q10nsxxh6iyckrangkmj9vi5mw3s8qjfsrzcbv8k0n4m") (features (quote (("std" "alloc") ("default" "std") ("alloc"))))))

(define-public crate-itybity-0.1 (crate (name "itybity") (vers "0.1.5") (deps (list (crate-dep (name "rstest") (req "^0.17") (default-features #t) (kind 2)))) (hash "1qj8zzn0gl2k0gl6kx8namp8r8fvfa4c22pzh20jfq5nikw6376c") (features (quote (("std" "alloc") ("default" "std") ("alloc"))))))

(define-public crate-itybity-0.1 (crate (name "itybity") (vers "0.1.6") (deps (list (crate-dep (name "rstest") (req "^0.17") (default-features #t) (kind 2)))) (hash "1x112mfajljwylzxi7jf1snygbvlnvx7hnn0z1sq4vky0f6wap14") (features (quote (("std" "alloc") ("default" "std") ("alloc"))))))

(define-public crate-itybity-0.2 (crate (name "itybity") (vers "0.2.0") (deps (list (crate-dep (name "rayon") (req "^1.7") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "rstest") (req "^0.17") (default-features #t) (kind 2)))) (hash "02891sxladwhrk83yi3whnvrcqqmc9hnil9c4awmk63sgrlsn9gz") (features (quote (("std" "alloc") ("default" "std") ("alloc")))) (v 2) (features2 (quote (("rayon" "std" "dep:rayon"))))))

(define-public crate-itybity-0.2 (crate (name "itybity") (vers "0.2.1") (deps (list (crate-dep (name "rayon") (req "^1.7") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "rstest") (req "^0.18") (default-features #t) (kind 2)))) (hash "0yay2sw8q0nhxc8vkhdl6smmgs29mc1idbqypjcyrm563265ahpj") (features (quote (("std" "alloc") ("default" "std") ("alloc")))) (v 2) (features2 (quote (("rayon" "std" "dep:rayon"))))))

