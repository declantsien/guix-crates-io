(define-module (crates-io it tv) #:use-module (crates-io))

(define-public crate-ittv_sdk-0.1 (crate (name "ittv_sdk") (vers "0.1.0") (deps (list (crate-dep (name "reqwest") (req "^0.11.26") (features (quote ("json"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.197") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "0j1mgbm1qjj69xz7j3mw3az61fknaj1mnpmnvi013pkl213ga6n4")))

(define-public crate-ittv_sdk-0.1 (crate (name "ittv_sdk") (vers "0.1.1") (deps (list (crate-dep (name "reqwest") (req "^0.11.26") (features (quote ("json"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.197") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "1p7sqjdqpi9iaxldmy7fgjvcdc083hqdx3vwfx1avjkychwfpbbn")))

(define-public crate-ittv_sdk-0.1 (crate (name "ittv_sdk") (vers "0.1.2") (deps (list (crate-dep (name "reqwest") (req "^0.11.26") (features (quote ("json"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.197") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "1q1hqnpyq388bc57nh252pmaqzcqq9j5zr7d46hg6z5j93fyaj1b")))

(define-public crate-ittv_sdk-0.1 (crate (name "ittv_sdk") (vers "0.1.3") (deps (list (crate-dep (name "reqwest") (req "^0.11.26") (features (quote ("json"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.197") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "0fdq6yskm0zzyb6yiq6x5p3s459526pqaimszckrzn5lwszsq30g")))

(define-public crate-ittv_sdk-0.1 (crate (name "ittv_sdk") (vers "0.1.4") (deps (list (crate-dep (name "reqwest") (req "^0.11.26") (features (quote ("json"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.197") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.114") (default-features #t) (kind 0)))) (hash "17abyr4hlmv2wrn1k05hzsfddmh1yfbjs581jijjr94dd75rcbyn")))

