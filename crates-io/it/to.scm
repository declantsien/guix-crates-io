(define-module (crates-io it to) #:use-module (crates-io))

(define-public crate-ittokun_guessing_game-0.1 (crate (name "ittokun_guessing_game") (vers "0.1.0") (deps (list (crate-dep (name "rand") (req "^0.8.3") (default-features #t) (kind 0)))) (hash "1k0hnl8cpnjl1azc8c1y48rzhsyfxpy2ih74054abbhplmwfj9xy")))

