(define-module (crates-io it er) #:use-module (crates-io))

(define-public crate-iter-chunks-0.1 (crate (name "iter-chunks") (vers "0.1.0") (hash "0bbchbhmhc32grgkrcbb7cz9fwkdr80f6i2j800yks36imccb7b2")))

(define-public crate-iter-chunks-0.1 (crate (name "iter-chunks") (vers "0.1.1") (hash "1xa84wfn4b4yrc2wk7gz9x9iz15b7389jbb0by3kr288231vqvxq")))

(define-public crate-iter-chunks-0.1 (crate (name "iter-chunks") (vers "0.1.2") (hash "18vsn2qpqs5r0sia80g3866rrb4364zvs41xkgsdjmm6whrzb7qi")))

(define-public crate-iter-chunks-0.1 (crate (name "iter-chunks") (vers "0.1.3") (hash "14vmdksj4ndfwp229w12x5q18knvzr0v6h9fjbrkig0r9vydvaxp")))

(define-public crate-iter-chunks-0.2 (crate (name "iter-chunks") (vers "0.2.0") (hash "1b73jzliwax30h2ixg7b9nrz04hddh2ipr9khpzfi24wfq93xw6a")))

(define-public crate-iter-chunks-0.2 (crate (name "iter-chunks") (vers "0.2.1") (hash "11azjw34f5fahq6lyizc4yi64dy4g8q1b762fz27h7r36ic8yaf4")))

(define-public crate-iter-chunks-0.2 (crate (name "iter-chunks") (vers "0.2.2") (hash "01x49f0fkhyncfdzam1438s0v69l37cg1rnv016x2vxnbwwkfwld")))

(define-public crate-iter-comprehensions-0.1 (crate (name "iter-comprehensions") (vers "0.1.0") (hash "071hj6k9vp8zmr3f74y88s6hcg7hv6cy688skm3h3gb7f9vpc4bj")))

(define-public crate-iter-comprehensions-0.2 (crate (name "iter-comprehensions") (vers "0.2.0") (hash "1m5zvs3cszwp192f88h9xvdjqbsh8iwhr3m0cb4xn0r1j94hki5b")))

(define-public crate-iter-comprehensions-0.3 (crate (name "iter-comprehensions") (vers "0.3.0") (hash "142nf3n747yi2yfbzrx6vmnw3lw0gs4wawi8drbgk7scr4fniziq")))

(define-public crate-iter-comprehensions-0.4 (crate (name "iter-comprehensions") (vers "0.4.0") (hash "0v54srm41dq550pnvxhi3y4xzpbqms8l122z6x4n30zd5byzzj7c")))

(define-public crate-iter-comprehensions-0.4 (crate (name "iter-comprehensions") (vers "0.4.1") (hash "00102ylkfwwc788hc984v6nm1fj3vg1xdz1s6xy8l32l5qbcpmhj")))

(define-public crate-iter-comprehensions-0.4 (crate (name "iter-comprehensions") (vers "0.4.2") (hash "103g3f9wr2x9r21ifn47wy1g8yd5l7h74gg4xpbv954whqx8aizw")))

(define-public crate-iter-comprehensions-0.5 (crate (name "iter-comprehensions") (vers "0.5.0") (hash "1j9z0i8wixk56ymwhp2bnbblnfg24gjzbwyi7mp8gyv3shvpafra")))

(define-public crate-iter-comprehensions-0.5 (crate (name "iter-comprehensions") (vers "0.5.1") (hash "1zif1bf6p9wrl59zq72ljykzfl8gsrm60d5wk0w6zx5xjh78l12k")))

(define-public crate-iter-comprehensions-0.5 (crate (name "iter-comprehensions") (vers "0.5.2") (hash "1r7ibzl6adbjncj9206qgvar30p6izpk8gp0xzhd4r5iykxn10kd")))

(define-public crate-iter-comprehensions-1 (crate (name "iter-comprehensions") (vers "1.0.0") (hash "19pzsldzr9avrv6s3xbkfdnqnds20qqlb0fc8wpzaxb9xlzzmf4j")))

(define-public crate-iter-debug-1 (crate (name "iter-debug") (vers "1.0.0") (hash "1rnkiz39y1f21qcl9b5468bm2a0smxzz67zj7wga20pi2g50sp31")))

(define-public crate-iter-diff-0.1 (crate (name "iter-diff") (vers "0.1.0") (hash "0lvx2vq5da029anaqzkaw078hg9hmwk8klaash7hw0cacyndqwz0") (yanked #t)))

(define-public crate-iter-diff-0.1 (crate (name "iter-diff") (vers "0.1.1") (hash "0kvyav6s1c8dbqx9zy5gqr1kj4d4p45c17mps16n572gb54vbnyf")))

(define-public crate-iter-enum-0.2 (crate (name "iter-enum") (vers "0.2.3") (deps (list (crate-dep (name "derive_utils") (req "^0.9") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "rayon_crate") (req "^1.2") (default-features #t) (kind 2) (package "rayon")) (crate-dep (name "syn") (req "^1.0") (default-features #t) (kind 0)))) (hash "023h1sgspca5rg77xaxn2k13sqb736435q4qkyyafk4p7y1pn42q") (features (quote (("trusted_len") ("rayon"))))))

(define-public crate-iter-enum-0.2 (crate (name "iter-enum") (vers "0.2.4") (deps (list (crate-dep (name "derive_utils") (req "^0.10") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "rayon_crate") (req "^1.2") (default-features #t) (kind 2) (package "rayon")) (crate-dep (name "syn") (req "^1.0") (default-features #t) (kind 0)))) (hash "0fz4xqs0amigfz16p926hj2zfyj7rm7mlrkc7y4raggc3dvskpic") (features (quote (("trusted_len") ("rayon"))))))

(define-public crate-iter-enum-0.2 (crate (name "iter-enum") (vers "0.2.5") (deps (list (crate-dep (name "derive_utils") (req "^0.11") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "rayon_crate") (req "^1") (default-features #t) (kind 2) (package "rayon")) (crate-dep (name "syn") (req "^1") (default-features #t) (kind 0)))) (hash "1fajdl5dprcr4hakdn478mxym9cphvmjkvmc0nvq9gsk5b0lpac6") (features (quote (("trusted_len") ("rayon"))))))

(define-public crate-iter-enum-0.2 (crate (name "iter-enum") (vers "0.2.6") (deps (list (crate-dep (name "derive_utils") (req "^0.11") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "rayon_crate") (req "^1") (default-features #t) (kind 2) (package "rayon")) (crate-dep (name "syn") (req "^1") (default-features #t) (kind 0)))) (hash "0nha62q9cfb40c7v14iyajb1g4y9ljza80kxkir2l4lfsgalj0f3") (features (quote (("trusted_len") ("rayon"))))))

(define-public crate-iter-enum-0.2 (crate (name "iter-enum") (vers "0.2.7") (deps (list (crate-dep (name "derive_utils") (req "^0.11") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "rayon_crate") (req "^1") (default-features #t) (kind 2) (package "rayon")) (crate-dep (name "syn") (req "^1") (default-features #t) (kind 0)))) (hash "0wf8i6wcvs3gx3c07r8kq6f1sz8vrvsjvbrqzzyyz35lscj4zlya") (features (quote (("trusted_len") ("rayon"))))))

(define-public crate-iter-enum-1 (crate (name "iter-enum") (vers "1.0.0") (deps (list (crate-dep (name "derive_utils") (req "^0.11") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "rayon_crate") (req "^1") (default-features #t) (kind 2) (package "rayon")) (crate-dep (name "syn") (req "^1") (default-features #t) (kind 0)))) (hash "0zfv5m1ls00pq531pl6caidagyyqa3c3zl928axvqlvydkns77i0") (features (quote (("rayon"))))))

(define-public crate-iter-enum-1 (crate (name "iter-enum") (vers "1.0.1") (deps (list (crate-dep (name "derive_utils") (req "^0.11") (default-features #t) (kind 0)) (crate-dep (name "macrotest") (req "^1.0.8") (default-features #t) (kind 2)) (crate-dep (name "quote") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "rayon_crate") (req "^1") (default-features #t) (kind 2) (package "rayon")) (crate-dep (name "rustversion") (req "^1") (default-features #t) (kind 2)) (crate-dep (name "syn") (req "^1") (default-features #t) (kind 0)))) (hash "01dhvfjdv53rm2sxk3xxksxma53lhc50r58ayr6rrrppkl6pz52g") (features (quote (("rayon"))))))

(define-public crate-iter-enum-1 (crate (name "iter-enum") (vers "1.0.2") (deps (list (crate-dep (name "derive_utils") (req "^0.12") (default-features #t) (kind 0)) (crate-dep (name "macrotest") (req "^1.0.9") (default-features #t) (kind 2)) (crate-dep (name "quote") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "rayon_crate") (req "^1") (default-features #t) (kind 2) (package "rayon")) (crate-dep (name "rustversion") (req "^1") (default-features #t) (kind 2)) (crate-dep (name "syn") (req "^1") (default-features #t) (kind 0)))) (hash "18r1jg38b8ax7bpw617yw7kc4b9k3f5vgyhc2x41rsh1p659n7gh") (features (quote (("rayon")))) (rust-version "1.31")))

(define-public crate-iter-enum-1 (crate (name "iter-enum") (vers "1.1.0") (deps (list (crate-dep (name "derive_utils") (req "^0.13") (default-features #t) (kind 0)) (crate-dep (name "macrotest") (req "^1.0.9") (default-features #t) (kind 2)) (crate-dep (name "quote") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "rayon_crate") (req "^1") (default-features #t) (kind 2) (package "rayon")) (crate-dep (name "rustversion") (req "^1") (default-features #t) (kind 2)) (crate-dep (name "syn") (req "^2") (default-features #t) (kind 0)))) (hash "12z95r0573anzv0ac0871qay3a4llrxzzsbj2p6qpsd20dw9nw8y") (features (quote (("rayon")))) (rust-version "1.56")))

(define-public crate-iter-enum-1 (crate (name "iter-enum") (vers "1.1.1") (deps (list (crate-dep (name "derive_utils") (req "^0.13.1") (default-features #t) (kind 0)) (crate-dep (name "macrotest") (req "^1.0.9") (default-features #t) (kind 2)) (crate-dep (name "quote") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "rayon_crate") (req "^1") (default-features #t) (kind 2) (package "rayon")) (crate-dep (name "rustversion") (req "^1") (default-features #t) (kind 2)) (crate-dep (name "syn") (req "^2") (default-features #t) (kind 0)))) (hash "1kb50wiiyicrhlbdkxfc9wy7xd2ddbagdivjf44cpjb1ad7graw3") (features (quote (("rayon")))) (rust-version "1.56")))

(define-public crate-iter-enum-1 (crate (name "iter-enum") (vers "1.1.2") (deps (list (crate-dep (name "derive_utils") (req "^0.14") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.25") (default-features #t) (kind 0)) (crate-dep (name "rayon_crate") (req "^1") (default-features #t) (kind 2) (package "rayon")) (crate-dep (name "rustversion") (req "^1") (default-features #t) (kind 2)))) (hash "11isk3pv4n1c3mk86cgw4ydxq1dwhn5y79204df3g65g7way6jpf") (features (quote (("rayon")))) (rust-version "1.56")))

(define-public crate-iter-enum-1 (crate (name "iter-enum") (vers "1.1.3") (deps (list (crate-dep (name "derive_utils") (req "^0.14.1") (default-features #t) (kind 0)) (crate-dep (name "rayon_crate") (req "^1") (default-features #t) (kind 2) (package "rayon")) (crate-dep (name "rustversion") (req "^1") (default-features #t) (kind 2)))) (hash "0bq7vvhng8mf4l7lchckwxdz8pi3kaiy5jpyvwqif2l95jq8xzls") (features (quote (("rayon")))) (rust-version "1.56")))

(define-public crate-iter-flow-0.1 (crate (name "iter-flow") (vers "0.1.0") (deps (list (crate-dep (name "anyhow") (req "^1") (default-features #t) (kind 2)))) (hash "1nlhllvwf8kik43n7wdhi3iy1i1ki71q9cpr1bd4ws00q38gcbd8") (features (quote (("use_alloc") ("default" "use_alloc"))))))

(define-public crate-iter-group-0.1 (crate (name "iter-group") (vers "0.1.0") (hash "1x2xdw2i7qim3720dvqlvwhk9xq0v5w6r71dr9cm083gw69ssskc")))

(define-public crate-iter-group-0.2 (crate (name "iter-group") (vers "0.2.0") (hash "16w8nr3v3nkdsnd44djdmx89l208znq6x370hv14bjwgigjfjz1g")))

(define-public crate-iter-identify_first_last-0.0.1 (crate (name "iter-identify_first_last") (vers "0.0.1") (deps (list (crate-dep (name "arrayvec") (req "^0.7.2") (kind 2)))) (hash "0q0zz9rj6gi1qcm7lqpd6fglr8ba3l67rr00lzazrng2251dfbv1") (rust-version "1.59")))

(define-public crate-iter-identify_first_last-0.1 (crate (name "iter-identify_first_last") (vers "0.1.0") (deps (list (crate-dep (name "arrayvec") (req "^0.7.2") (kind 2)) (crate-dep (name "document-features") (req "^0.2.7") (default-features #t) (kind 0)))) (hash "0wgkg3il60cfh1wb4mis934sjymz5sqv3hcqvwsbihmx8fzlkzvv") (features (quote (("nightly") ("default" "nightly")))) (rust-version "1.59")))

(define-public crate-iter-identify_first_last-0.2 (crate (name "iter-identify_first_last") (vers "0.2.0") (deps (list (crate-dep (name "arrayvec") (req "^0.7.2") (kind 2)) (crate-dep (name "document-features") (req "^0.2.7") (default-features #t) (kind 0)))) (hash "1w8fal0j7vpsa8llk2kvfnq4zms427ihrwr68c3gb8jnj5kvwha8") (features (quote (("nightly") ("default" "nightly")))) (rust-version "1.70")))

(define-public crate-iter-identify_first_last-0.2 (crate (name "iter-identify_first_last") (vers "0.2.1") (deps (list (crate-dep (name "arrayvec") (req "^0.7.2") (kind 2)) (crate-dep (name "document-features") (req "^0.2.7") (default-features #t) (kind 0)))) (hash "0md7ndz3bhsxxb8s1h35fmicw36hlr4j13nds6ilcq4yk1893v8n") (features (quote (("nightly") ("default" "nightly")))) (rust-version "1.70")))

(define-public crate-iter-merge-sort-0.1 (crate (name "iter-merge-sort") (vers "0.1.0") (hash "15wmq0swd5rylj10y7sikm4kybg9dlafwpvxqa9phqmdffzq6ndq")))

(define-public crate-iter-merge-sort-0.1 (crate (name "iter-merge-sort") (vers "0.1.1") (hash "1kiqg4xd3dbrhia4g1517i3zsq4azbvs39k533j9658k98vxmn37")))

(define-public crate-iter-merge-sort-0.1 (crate (name "iter-merge-sort") (vers "0.1.2") (hash "1wi1y9c6hjj594c26c07qyq3mrajr36lamar3sd8nr5psn9dyhnj")))

(define-public crate-iter-n-0.1 (crate (name "iter-n") (vers "0.1.0") (hash "1rawhyyzbq22mq5d6fpyv0zi4qh9q6m1bzin5xsmlag4cziak1im")))

(define-public crate-iter-opt-filter-0.1 (crate (name "iter-opt-filter") (vers "0.1.0") (hash "0bv78rd1ck92w901c124i6c8ji804w6662npshm2kzm4agsnm9k4")))

(define-public crate-iter-progress-0.3 (crate (name "iter-progress") (vers "0.3.0") (deps (list (crate-dep (name "time") (req "^0.1") (default-features #t) (kind 0)))) (hash "0s6xnf2r83qd2zj55pn9n05pbbdwi8xysp0qj83py19rwvchba8l")))

(define-public crate-iter-progress-0.4 (crate (name "iter-progress") (vers "0.4.0") (hash "1c4333zj7sswhvfxicdivx99fm3jlhrw4zrrxz1xpbrhkcwbgz30")))

(define-public crate-iter-progress-0.5 (crate (name "iter-progress") (vers "0.5.0") (hash "08d8yyf2fz8f0r23zkix4rlb64vbdsrb2a414acldcdvbkr9zmi7")))

(define-public crate-iter-progress-0.6 (crate (name "iter-progress") (vers "0.6.0") (hash "01iw6hb4687qsfscgl9qg3qdijx0nbrwbnlhhz96w5j87jm9fr0j")))

(define-public crate-iter-progress-0.7 (crate (name "iter-progress") (vers "0.7.0") (hash "1579zci8y2ynqly1b4b7h9kzdq9fwzgddna9dxx712k5jb4qbzq5")))

(define-public crate-iter-progress-0.8 (crate (name "iter-progress") (vers "0.8.0") (hash "1m2w1h94zy6p7yvkjgwq16ks6p8x1kjhnv3gd4b8wfjfvmj9s1cp")))

(define-public crate-iter-progress-0.8 (crate (name "iter-progress") (vers "0.8.1-rc1") (hash "075g21yl686hgyvzr8k0s5s1f9fyj58m98wwllc9cn82kbbql5mb")))

(define-public crate-iter-python-0.9 (crate (name "iter-python") (vers "0.9.0") (deps (list (crate-dep (name "itertools") (req "^0.8.0") (default-features #t) (kind 2)))) (hash "0nmznrp7v2mziczmzv26yyb1qib6p15l6naiymiax0b370xz0vw9") (features (quote (("nightly") ("default"))))))

(define-public crate-iter-python-0.9 (crate (name "iter-python") (vers "0.9.1") (deps (list (crate-dep (name "itertools") (req "^0.8.0") (default-features #t) (kind 2)))) (hash "1gmdxvn3h0kn74alfjqpizbjvmmkffq747y09dhqdjmpqmwifc6f") (features (quote (("nightly") ("default"))))))

(define-public crate-iter-python-0.9 (crate (name "iter-python") (vers "0.9.2") (deps (list (crate-dep (name "candy") (req "^0.1.3") (default-features #t) (kind 0)) (crate-dep (name "join-lazy-fmt") (req "^0.9.2") (default-features #t) (kind 0)))) (hash "14n4fkfl4v5qxigcky8f6pjyv4fhqsgqhxw7b5z52hndw9sxf4gz") (features (quote (("nightly") ("default"))))))

(define-public crate-iter-python-0.10 (crate (name "iter-python") (vers "0.10.0-rc1") (deps (list (crate-dep (name "join-lazy-fmt") (req "^0.9.2") (default-features #t) (kind 0)))) (hash "0giny5y25pv6wjan2xf119kds6bnyygcv0585vyah7f8c87spinz") (features (quote (("std") ("default" "std") ("better-docs"))))))

(define-public crate-iter-python-0.10 (crate (name "iter-python") (vers "0.10.0") (deps (list (crate-dep (name "join-lazy-fmt") (req "^0.9.2") (default-features #t) (kind 0)))) (hash "0lpc86pb516k2cwq373b5kwvwzc95bkcz1l470hajpjl50b5nd0w") (features (quote (("std") ("default" "std") ("better-docs"))))))

(define-public crate-iter-python-0.10 (crate (name "iter-python") (vers "0.10.1") (deps (list (crate-dep (name "join-lazy-fmt") (req "^0.9.2") (default-features #t) (kind 0)))) (hash "0lfkd3ymwrylmpk4yw31q9c2m0irr0pxifllpklz7rm18g8ry6sq") (features (quote (("std") ("default" "std") ("better-docs"))))))

(define-public crate-iter-rationals-0.1 (crate (name "iter-rationals") (vers "0.1.0") (deps (list (crate-dep (name "num-integer") (req "^0.1.45") (default-features #t) (kind 0)) (crate-dep (name "num-rational") (req "^0.4.1") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2.15") (default-features #t) (kind 0)))) (hash "0c1i4f8hrxvcc76i43lwiyj6drrvffkizajg2lvg7x05z4nry8vs")))

(define-public crate-iter-rationals-0.2 (crate (name "iter-rationals") (vers "0.2.0") (deps (list (crate-dep (name "num-integer") (req "^0.1.45") (default-features #t) (kind 0)) (crate-dep (name "num-rational") (req "^0.4.1") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2.15") (default-features #t) (kind 0)))) (hash "105g1gdx3lra6wm49766d98lxkccsv4ih3cyar8fn1db2gmvblbw")))

(define-public crate-iter-rationals-0.2 (crate (name "iter-rationals") (vers "0.2.1") (deps (list (crate-dep (name "num-integer") (req "^0.1.45") (default-features #t) (kind 0)) (crate-dep (name "num-rational") (req "^0.4.1") (default-features #t) (kind 0)))) (hash "1w8m5gnibdi05y93hqwkvb0qqv8mqbx6hmn5bv3dgsfvwvjsnfl8")))

(define-public crate-iter-read-0.1 (crate (name "iter-read") (vers "0.1.0") (hash "01gnzmr9sgr534chd4rnbhxynbfi5cn4kxbmjfad3xbp5bs8kdi9") (features (quote (("unstable"))))))

(define-public crate-iter-read-0.2 (crate (name "iter-read") (vers "0.2.0") (hash "1gp2dw4jr00z1ffybkpkzirpz1aij78av68a6d1jynh5w4jzw8gx") (features (quote (("unstable"))))))

(define-public crate-iter-read-0.2 (crate (name "iter-read") (vers "0.2.1") (hash "00xk55m2yjd7cjc9yyhxhr73bmg5p96n2r21b46zga5bvrwn6gnh") (features (quote (("unstable"))))))

(define-public crate-iter-read-0.3 (crate (name "iter-read") (vers "0.3.0") (hash "0pczfmfpzpq5w6i5ik6i3lf8pky24h8ax284pd4j83r97cblr8sf") (features (quote (("unstable"))))))

(define-public crate-iter-read-0.3 (crate (name "iter-read") (vers "0.3.1") (hash "1i4kiz7y1aj3a7ynj76afsikc4kpnhlfl7s5xk20kmasl0zcm5y3") (features (quote (("unstable"))))))

(define-public crate-iter-read-1 (crate (name "iter-read") (vers "1.0.0") (hash "0kyhi9f45bf4lf0ndk1psfk76w8qx85l3259wk39pkjyg0k8r57s") (features (quote (("unstable"))))))

(define-public crate-iter-read-1 (crate (name "iter-read") (vers "1.0.1") (hash "12q45qrvyx88kmdzs7shnsdjmg5najr6hy2ivbmmcd4fmsmw3655") (features (quote (("unstable"))))))

(define-public crate-iter-scan-0.0.0 (crate (name "iter-scan") (vers "0.0.0") (hash "1syd3g5ccsixm0g27mxjk60vdcfds0nqa3fdd0vkg0fpy8bp899s")))

(define-public crate-iter-scan-0.1 (crate (name "iter-scan") (vers "0.1.0") (hash "0qr72pscfbr4h8nyddsdm3pg7lxiyak36z87dfnk5cnxsdaxdg5n")))

(define-public crate-iter-scan-0.2 (crate (name "iter-scan") (vers "0.2.0") (deps (list (crate-dep (name "replace_with") (req "^0.1.7") (kind 0)))) (hash "0g32s69dznwxnh5kcz91b71ni6ashhw1a8crj61k187ci9fxjgr6") (features (quote (("std" "replace_with/std") ("default" "std"))))))

(define-public crate-iter-set-0.1 (crate (name "iter-set") (vers "0.1.0") (hash "0lwjldnzvsdf70bl4sbnq8z9cm1zd1mklvfbmbiw5nfcsz0cv7is")))

(define-public crate-iter-set-0.1 (crate (name "iter-set") (vers "0.1.1") (hash "0pn6miffxszcmna7k16hxf2salx9a6q102l4qbnmzcrp088qswhz")))

(define-public crate-iter-set-0.2 (crate (name "iter-set") (vers "0.2.0") (hash "0gl2m03x9kyqida9745ssvgqpyjqsw0jz3rbzgwqdxqsz34b6sxl")))

(define-public crate-iter-set-1 (crate (name "iter-set") (vers "1.0.0") (hash "1j2mbjybp27j2q3av3hs8vlqlbryj2q1i0kp3qiqja1iw5pkqmv4")))

(define-public crate-iter-set-1 (crate (name "iter-set") (vers "1.0.1") (hash "080bfyhl6cjnf5d0zss6jxwgdncn2whzh2r2lawf91f6xmycn07f")))

(define-public crate-iter-set-1 (crate (name "iter-set") (vers "1.0.2") (hash "1ka660h99wbbdjzpb3ibmnmf0jc1vcwq8nms1w10wipyncywgcxw")))

(define-public crate-iter-set-1 (crate (name "iter-set") (vers "1.0.3") (hash "07sh9q0453wmrx9li3dx9hnww4xwdhxsk3rmjvfqw7j5wwrp6lqm")))

(define-public crate-iter-set-2 (crate (name "iter-set") (vers "2.0.0") (hash "03z129rlqg360zjddvsi1gnyhssvfv1y51hlzf6cc1d771n7rjxy")))

(define-public crate-iter-set-2 (crate (name "iter-set") (vers "2.0.1") (hash "04v8m78002cj3gml0fs7ba2l9jm52sh8m4grczxq5h8rp2nknnqg")))

(define-public crate-iter-set-2 (crate (name "iter-set") (vers "2.0.2") (hash "16xdfdfrfhygmbvfnlx7g1wkav6vf0h68ycfhlfk8ib9n1zdx0yk")))

(define-public crate-iter-set-ops-0.1 (crate (name "iter-set-ops") (vers "0.1.0") (deps (list (crate-dep (name "binary-heap-plus") (req "^0.5.0") (default-features #t) (kind 0)) (crate-dep (name "compare") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "1jqyga8jxqvqz5887h8abi1y7ck3p0wb7yd5767f13kh66wabjk4")))

(define-public crate-iter-set-ops-0.1 (crate (name "iter-set-ops") (vers "0.1.1") (deps (list (crate-dep (name "binary-heap-plus") (req "^0.5.0") (default-features #t) (kind 0)) (crate-dep (name "compare") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "0bxlmzp3sv8i8xrgnhmf9kyis4zdiiw3w8bvm6j8shda9s73ai0d")))

(define-public crate-iter-set-ops-0.2 (crate (name "iter-set-ops") (vers "0.2.0") (deps (list (crate-dep (name "binary-heap-plus") (req "^0.5") (default-features #t) (kind 0)) (crate-dep (name "compare") (req "^0.1") (default-features #t) (kind 0)))) (hash "1h4bwbka098a3qhv91403fv4v5jmarsqa3ffn4bijdyjnbj55q4r")))

(define-public crate-iter-set-ops-0.2 (crate (name "iter-set-ops") (vers "0.2.1") (deps (list (crate-dep (name "binary-heap-plus") (req "^0.5") (default-features #t) (kind 0)) (crate-dep (name "compare") (req "^0.1") (default-features #t) (kind 0)))) (hash "02ffsnd3g07lyhgv55mqvbzq22dvkcwy5768yihgj6x9r7xpnggn")))

(define-public crate-iter-set-ops-0.2 (crate (name "iter-set-ops") (vers "0.2.2") (deps (list (crate-dep (name "binary-heap-plus") (req "^0.5") (default-features #t) (kind 0)) (crate-dep (name "compare") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8") (default-features #t) (kind 2)))) (hash "1ab581ykylh68i84h2bsiwg2sagjfgacvynyyr0yqlrz4q9acg7i")))

(define-public crate-iter-set-ops-0.2 (crate (name "iter-set-ops") (vers "0.2.3") (deps (list (crate-dep (name "binary-heap-plus") (req "^0.5") (default-features #t) (kind 0)) (crate-dep (name "compare") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8") (default-features #t) (kind 2)))) (hash "0f2xa3d7pa8krl8ghsm2bkxszfa0lagc87px7ads019l5sxbbagq")))

(define-public crate-iter-skak-0.1 (crate (name "iter-skak") (vers "0.1.0") (hash "0snl4vx9i3hc43kbc40hj5w581qjwh3b5z64w0vzzqsl42kn7qnz")))

(define-public crate-iter-skak-0.2 (crate (name "iter-skak") (vers "0.2.0") (hash "141m9l4hc51ybqgfpq35zicgpgmaazm1c7sp2i6vcwchs2y3ysng")))

(define-public crate-iter-tee-0.1 (crate (name "iter-tee") (vers "0.1.0") (hash "01lhvrdfdiadr2m3mjp8xydbx0ywz1f6ca7ypmwlk7c9z68w1xpk")))

(define-public crate-iter-trait-0.1 (crate (name "iter-trait") (vers "0.1.0") (deps (list (crate-dep (name "bit-set") (req "^0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "bit-vec") (req "^0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "blist") (req "^0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "cfg-if") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "enum-set") (req "^0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "interval-heap") (req "^0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "linear-map") (req "^0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "linked-hash-map") (req "^0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "lru-cache") (req "^0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "vec_map") (req "^0") (optional #t) (default-features #t) (kind 0)))) (hash "1sldp74im38v9a4hqsma18s0zsa4qmv8zn60jh5vnxislnn9rdz0") (features (quote (("std" "collections") ("nightly") ("default" "std" "nightly") ("contain-rs" "std" "bit-set" "bit-vec" "blist" "enum-set" "interval-heap" "linear-map" "linked-hash-map" "lru-cache" "vec_map") ("collections"))))))

(define-public crate-iter-trait-0.2 (crate (name "iter-trait") (vers "0.2.0") (deps (list (crate-dep (name "cfg-if") (req "^0.1") (default-features #t) (kind 0)))) (hash "11p9sgm9gdjylij6mb4cy3d7qqhjlv2pyl4kma0pry277k3wkl6r") (features (quote (("std" "alloc") ("nightly") ("default" "std") ("alloc"))))))

(define-public crate-iter-trait-0.3 (crate (name "iter-trait") (vers "0.3.0") (deps (list (crate-dep (name "cfg-if") (req "^0.1") (default-features #t) (kind 0)))) (hash "102a2q10abpmzcmdd9v3dzvfmg4bfn8r7a3r0mdialmfwp0fvqa8") (features (quote (("std" "alloc") ("nightly") ("default" "std") ("alloc"))))))

(define-public crate-iter-trait-0.3 (crate (name "iter-trait") (vers "0.3.1") (deps (list (crate-dep (name "cfg-if") (req "^0.1.2") (default-features #t) (kind 0)))) (hash "1gqa2blpk6icdjf7wzp5jgy6a3kspxkil5i9qdjwgg6kyy91b4pw") (features (quote (("std" "alloc") ("nightly") ("default" "std") ("alloc"))))))

(define-public crate-iter-tree-0.1 (crate (name "iter-tree") (vers "0.1.0") (hash "1ismmsw2r020vcc2fwiwjs9n9xndp05ayckaq72dd1qf7zzmdbwl") (features (quote (("vec") ("deque") ("default" "vec"))))))

(define-public crate-iter-tree-0.1 (crate (name "iter-tree") (vers "0.1.1") (hash "1kw2ap2qdcl9mwmyn9xggc5k1i2hhy3vyg775xiky436v65wnfhb") (features (quote (("vec") ("deque") ("default" "vec"))))))

(define-public crate-iter-tree-0.1 (crate (name "iter-tree") (vers "0.1.2") (hash "0xi5pljd7hx72rq9r0c9gxnzvcjw4v2mld7pfrdz8gg23gw7xb8k") (features (quote (("vec") ("deque") ("default" "vec"))))))

(define-public crate-iter-tree-0.1 (crate (name "iter-tree") (vers "0.1.3") (hash "110k602rsxn876z55sb26a9q6kzvi7qp3mqv2c3lmx5akxzlld0b") (features (quote (("vec") ("deque") ("default" "vec"))))))

(define-public crate-iter-tree-0.1 (crate (name "iter-tree") (vers "0.1.4") (hash "1h8h36ldcy0zxr6abm3lf8a7mrazxf087i863vhcmh8f19a8n4lw") (features (quote (("vec") ("deque") ("default" "vec"))))))

(define-public crate-iter-tree-0.1 (crate (name "iter-tree") (vers "0.1.5") (hash "11cwvyi96hwr5q9ri51b666l85636kjzfqyx2hx1a176brz8lzn9") (features (quote (("vec") ("deque") ("default" "vec"))))))

(define-public crate-iter-tree-0.1 (crate (name "iter-tree") (vers "0.1.6") (hash "0f5ld65rczvq9mgic9sfg01h76mmw5fl88x51by7a472kfrihn7p") (features (quote (("vec") ("deque") ("default" "vec"))))))

(define-public crate-iter2-0.1 (crate (name "iter2") (vers "0.1.0") (hash "0qxy7i2b0b4d36rz45vv2wsb4m2s9bdbdrkc4mf3172363v47p67")))

(define-public crate-iter_accumulate-1 (crate (name "iter_accumulate") (vers "1.0.0") (hash "1f85kqnyxfbp2pbylh8lbil7khas3mw6angxry0ajw6s0p6ib991")))

(define-public crate-iter_all-0.1 (crate (name "iter_all") (vers "0.1.0") (deps (list (crate-dep (name "log") (req "^0.4.17") (default-features #t) (kind 0)) (crate-dep (name "serial_test") (req "^2.0.0") (default-features #t) (kind 2)))) (hash "1fr8jhwzv08a91gf1byqg8g6fy3g0pzzwsjag5yf60in7wlbksrj")))

(define-public crate-iter_all-0.1 (crate (name "iter_all") (vers "0.1.1") (hash "09zh980mq3z3cgknbi96c63qffgn1am0ndf1xf8nzm3n0asfb9dy")))

(define-public crate-iter_all-0.1 (crate (name "iter_all") (vers "0.1.2") (hash "0xfcpcy7ni38p7cwpp58c6ix1lzkqgqvj2lji1w0i3gvsnq9m8yk")))

(define-public crate-iter_all-0.1 (crate (name "iter_all") (vers "0.1.4") (hash "0dkdihjrxgg0qw7h34g27q9qa499acglsqp9jap8y4fmp1hprbsv")))

(define-public crate-iter_all-0.1 (crate (name "iter_all") (vers "0.1.5") (hash "03yrlwvvqbfrcz2vjsm1z2q8lnm9yxd9g8yx3g027p2n2fkvw61c")))

(define-public crate-iter_all-0.2 (crate (name "iter_all") (vers "0.2.0") (hash "1pgi3n5fq46y89dmwfw0h17ssp2vsymr7d00xywmhl8gin4dg3ys")))

(define-public crate-iter_all-0.2 (crate (name "iter_all") (vers "0.2.1") (hash "1ay4jb70rlfhampflhirskbzvx4m9bvsg64gn65czn2vbsh69hlp")))

(define-public crate-iter_all-0.2 (crate (name "iter_all") (vers "0.2.2") (hash "0a43vgfr362kkgn8qxxm0lx01m5xj7rhr11hbk2hjl1b6i204jaw")))

(define-public crate-iter_all_proc-0.1 (crate (name "iter_all_proc") (vers "0.1.0") (deps (list (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (features (quote ("full" "extra-traits"))) (default-features #t) (kind 0)))) (hash "0lbnq03703ajwb3j4zw21hsj2wbv9ksszi5r65ra7dd9443n619k")))

(define-public crate-iter_all_proc-0.1 (crate (name "iter_all_proc") (vers "0.1.1") (deps (list (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (features (quote ("full" "extra-traits"))) (default-features #t) (kind 0)))) (hash "13zrxy8rliggmh7syggrj8plisyzicxl435ldmy8syl9hgs61q7j")))

(define-public crate-iter_all_proc-0.1 (crate (name "iter_all_proc") (vers "0.1.2") (deps (list (crate-dep (name "proc-macro2") (req "^1.0.59") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2.0.18") (features (quote ("full" "extra-traits"))) (default-features #t) (kind 0)))) (hash "17chggc1h2s4ka85j90qihwrshav1y8m8yssqg74jgf9izk22fc5")))

(define-public crate-iter_all_proc-0.1 (crate (name "iter_all_proc") (vers "0.1.4") (deps (list (crate-dep (name "proc-macro2") (req "^1.0.59") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2.0.18") (features (quote ("full" "extra-traits"))) (default-features #t) (kind 0)))) (hash "03mjqs8rm026xranz4j8jl08mzb30jc6dgffbvz5nzvvsapbff43")))

(define-public crate-iter_all_proc-0.1 (crate (name "iter_all_proc") (vers "0.1.5") (deps (list (crate-dep (name "proc-macro2") (req "^1.0.59") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2.0.18") (features (quote ("full" "extra-traits"))) (default-features #t) (kind 0)))) (hash "05mqka30nxy8q9cjq34mcxp9ig38xwybs4dlh7lnj89zd7wn6zbc")))

(define-public crate-iter_all_proc-0.2 (crate (name "iter_all_proc") (vers "0.2.0") (deps (list (crate-dep (name "proc-macro2") (req "^1.0.67") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.33") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2.0.36") (features (quote ("full" "extra-traits"))) (default-features #t) (kind 0)))) (hash "04ph0x0f4967qnj3r9dkjbvq511q5v0vb8zk5k7pgb2gy1cqgglb")))

(define-public crate-iter_all_proc-0.2 (crate (name "iter_all_proc") (vers "0.2.1") (deps (list (crate-dep (name "proc-macro2") (req "^1.0.67") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.33") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2.0.36") (features (quote ("full" "extra-traits"))) (default-features #t) (kind 0)))) (hash "1r974m1xrspj67insqkx5v4ccq3wspm62pk89s86pydzxnkj9h1p")))

(define-public crate-iter_all_proc-0.2 (crate (name "iter_all_proc") (vers "0.2.2") (deps (list (crate-dep (name "proc-macro2") (req "^1.0.67") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.33") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2.0.36") (features (quote ("full" "extra-traits"))) (default-features #t) (kind 0)))) (hash "06p798fi6whbkhka7lpqqxflpfx1liqzxnqspzc0iprck5ksidv8")))

(define-public crate-iter_columns-0.1 (crate (name "iter_columns") (vers "0.1.0") (deps (list (crate-dep (name "iter_columns_derive") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "0s2ha64kj6irmppfiqn5v231yh4cqdha6y9ml8lvjj4szgd78rh4")))

(define-public crate-iter_columns-0.2 (crate (name "iter_columns") (vers "0.2.0") (deps (list (crate-dep (name "iter_columns_derive") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "1wfm4yl6ibi220yzz6ilw0d7rbgplidr8kbh0zb1jzxl9p19cjil") (features (quote (("no_array"))))))

(define-public crate-iter_columns-0.2 (crate (name "iter_columns") (vers "0.2.1") (deps (list (crate-dep (name "iter_columns_derive") (req "^0.1.1") (default-features #t) (kind 0)))) (hash "1w9kp5531piccfc97v2b78xm84bvnw0p54w2xs1p2r2hprrsn1cr") (features (quote (("no_array"))))))

(define-public crate-iter_columns-0.3 (crate (name "iter_columns") (vers "0.3.0") (hash "1amjzmjprb2myjdbsnijsbql02ll71a2hyhkqly7f7snli16kz6n") (features (quote (("array_into_iter"))))))

(define-public crate-iter_columns_derive-0.1 (crate (name "iter_columns_derive") (vers "0.1.0") (deps (list (crate-dep (name "quote") (req "^1.0.9") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.72") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "01x86z0ifasqyfvyb8x607h6swvkavsh9q9nl7kahjc8rwzyx3w6") (yanked #t)))

(define-public crate-iter_columns_derive-0.1 (crate (name "iter_columns_derive") (vers "0.1.1") (deps (list (crate-dep (name "quote") (req "^1.0.9") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.72") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "0jqw3w55xc1d2vcr83l4lxw5965vw9w4d4wiqbpnjdlk4kl2kkqi") (yanked #t)))

(define-public crate-iter_fallback-0.1 (crate (name "iter_fallback") (vers "0.1.0") (hash "09kd47dv54v30qqifs79waad77cihsg5wzr8sdrd8x81b86py3vz")))

(define-public crate-iter_fallback-0.1 (crate (name "iter_fallback") (vers "0.1.1") (hash "1z5s8qzrkik4q72mx05n0glva15m9il17c0fafi3mxb5bq3yyi33")))

(define-public crate-iter_fallback-0.1 (crate (name "iter_fallback") (vers "0.1.2") (hash "1n1js14fj7jfh4415x03s5gp9fhbxjkgi1073n0q869lsx620h9v")))

(define-public crate-iter_fixed-0.1 (crate (name "iter_fixed") (vers "0.1.0") (hash "0vbpd1pjsfa4848wzn04kmb1ca2yyf8xpjqr29207mb9wygmpsjx")))

(define-public crate-iter_fixed-0.1 (crate (name "iter_fixed") (vers "0.1.1") (hash "0sqv0yybwwdiki4ayz5zb8scjwdvdm67gi26lc8klv0qi1h756ha")))

(define-public crate-iter_fixed-0.1 (crate (name "iter_fixed") (vers "0.1.2") (hash "12bqr9v6iwd1snz75wfq7dyywh1fhh5b0d48annpqg3ajn82hcbk")))

(define-public crate-iter_fixed-0.1 (crate (name "iter_fixed") (vers "0.1.3") (hash "19309fpmgjw8f9bkhmrzp0nlwiw9cq2fi4psk800cnhvad8nhbr2") (features (quote (("nightly_features") ("default" "nightly_features"))))))

(define-public crate-iter_fixed-0.2 (crate (name "iter_fixed") (vers "0.2.0") (hash "1ddkqrix5ym3y3jw1p6vlz75qjkv1g7d8cghx8ib5zwi642f47b1") (features (quote (("nightly_features") ("default" "nightly_features"))))))

(define-public crate-iter_fixed-0.2 (crate (name "iter_fixed") (vers "0.2.1") (hash "171nb9dcijf9005rcljz9g183h1b59qv7qjvyrv57w5cvafykns1") (features (quote (("nightly_features") ("default" "nightly_features"))))))

(define-public crate-iter_fixed-0.2 (crate (name "iter_fixed") (vers "0.2.2") (hash "1msmfr8l4618wn8v0pk0s1l1i90mzlgp0nfglnixfsyspizn6vap") (features (quote (("nightly_features") ("default" "nightly_features"))))))

(define-public crate-iter_fixed-0.3 (crate (name "iter_fixed") (vers "0.3.0") (hash "0wl9a24r6sb52zzy9rz56y4avj898nllsa49qf08ifln00482q8f") (features (quote (("nightly_features") ("default"))))))

(define-public crate-iter_fixed-0.3 (crate (name "iter_fixed") (vers "0.3.1") (hash "0z3kc0q7a7h6myrfxpmnvfc9wv01w38lb13ajhnz4kzh1s0i67ad") (features (quote (("nightly_features") ("default"))))))

(define-public crate-iter_from_closure-1 (crate (name "iter_from_closure") (vers "1.0.0") (hash "1asbqy55mx06kl72ahvq41gpiggbhng8vd29xh20nvsam7cwjb8g")))

(define-public crate-iter_from_fn-0.1 (crate (name "iter_from_fn") (vers "0.1.0") (hash "1kz26ykqd98rbwkjw550l5z9q0qpss2rwx0q583vz51j1yzf3cyw")))

(define-public crate-iter_from_fn-1 (crate (name "iter_from_fn") (vers "1.0.0") (hash "09my1rwqv7r1k22qwjqsxmx341j2mj61sp3xj797vv0y5ipz47hg")))

(define-public crate-iter_num_tools-0.1 (crate (name "iter_num_tools") (vers "0.1.0") (deps (list (crate-dep (name "num-traits") (req "^0.2") (default-features #t) (kind 0)))) (hash "15m4cf5bmcnynpyy1s2a61s2hlq2hi1w4vjnhy3i8vd2479v878n")))

(define-public crate-iter_num_tools-0.1 (crate (name "iter_num_tools") (vers "0.1.1") (deps (list (crate-dep (name "itertools") (req "^0.9") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2") (default-features #t) (kind 0)))) (hash "1waqd3d1jhjxa5dkdz7wq5ymrcxg8k1mcapr8z0dl0000z8vqgax")))

(define-public crate-iter_num_tools-0.1 (crate (name "iter_num_tools") (vers "0.1.2") (deps (list (crate-dep (name "itertools") (req "^0.9") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2") (default-features #t) (kind 0)))) (hash "0amm14mjzh2p3s3gkwk7f67vqd9mp81zkd3lmmpbfnfwmxmj3znf")))

(define-public crate-iter_num_tools-0.1 (crate (name "iter_num_tools") (vers "0.1.3") (deps (list (crate-dep (name "itertools") (req "^0.9") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2") (default-features #t) (kind 0)))) (hash "0jd511wmr1aqv11xd4j5414f6hn8mbmm247bdrcnzri6dicvmxpg")))

(define-public crate-iter_num_tools-0.1 (crate (name "iter_num_tools") (vers "0.1.4") (deps (list (crate-dep (name "itertools") (req "^0.9") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2") (default-features #t) (kind 0)))) (hash "1hcq7r7hl9zffv9kci25iqnwfhvkwraricvggz7sdjzlmrmwlrqi")))

(define-public crate-iter_num_tools-0.1 (crate (name "iter_num_tools") (vers "0.1.5") (deps (list (crate-dep (name "itertools") (req "^0.9") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2") (default-features #t) (kind 0)))) (hash "0pzjg8b2riywd1vl5r7yjqb496mx483qmqapbjdq73pyg9g98xf7")))

(define-public crate-iter_num_tools-0.1 (crate (name "iter_num_tools") (vers "0.1.6") (deps (list (crate-dep (name "itertools") (req "^0.9") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2") (default-features #t) (kind 0)))) (hash "175bihllvc8s8571gq9snmv5dmzw90f05sb2zwa562hgfsdlcwns")))

(define-public crate-iter_num_tools-0.1 (crate (name "iter_num_tools") (vers "0.1.7") (deps (list (crate-dep (name "itertools") (req "^0.9") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2") (default-features #t) (kind 0)))) (hash "029xi14p6b1qwkyhqfzlmfb6j416339zcxzclqg863kcrir81ixl")))

(define-public crate-iter_num_tools-0.1 (crate (name "iter_num_tools") (vers "0.1.8") (deps (list (crate-dep (name "itertools") (req "^0.9") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2") (default-features #t) (kind 0)))) (hash "1sbrrx74221vva0x8qb1yf6yp6hng5v37y3sc5ggvci3dcwhykhd")))

(define-public crate-iter_num_tools-0.1 (crate (name "iter_num_tools") (vers "0.1.9") (deps (list (crate-dep (name "itertools") (req "^0.9") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2") (default-features #t) (kind 0)))) (hash "0h4s2rs6xcwldhwwypjrc87r5hsahr9f6jfrvd4zg3fsvmkpdk4n")))

(define-public crate-iter_num_tools-0.1 (crate (name "iter_num_tools") (vers "0.1.10") (deps (list (crate-dep (name "itertools") (req "^0.9") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2") (default-features #t) (kind 0)))) (hash "01qh1k6wishpxqzkwgqafic0ibfr9dnf1sliw1ry5k16lzp92n72")))

(define-public crate-iter_num_tools-0.1 (crate (name "iter_num_tools") (vers "0.1.11") (deps (list (crate-dep (name "itertools") (req "^0.9") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2") (default-features #t) (kind 0)))) (hash "0bwxq3r1s12xqv2f5c175gr4p93p3lxnp6m51izlca85sjxsr2mb")))

(define-public crate-iter_num_tools-0.1 (crate (name "iter_num_tools") (vers "0.1.12") (deps (list (crate-dep (name "itertools") (req "^0.9") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2") (default-features #t) (kind 0)))) (hash "16mwx7r8biwckh4hw910ydd90q8izmf3qy7c2vfq2sfl7hm9896h")))

(define-public crate-iter_num_tools-0.1 (crate (name "iter_num_tools") (vers "0.1.13") (deps (list (crate-dep (name "itertools") (req "^0.9") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2") (default-features #t) (kind 0)))) (hash "0158r0kpvcxcy2379mmb3x5x7hbz3wv6mxz9a1id61xw5h1djnnh")))

(define-public crate-iter_num_tools-0.1 (crate (name "iter_num_tools") (vers "0.1.14") (deps (list (crate-dep (name "itertools") (req "^0.9") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2") (default-features #t) (kind 0)))) (hash "1c4g638idfbvk09j2rjbbriyl8cpqxngkiv3z7wam51xsqzgzrmh")))

(define-public crate-iter_num_tools-0.1 (crate (name "iter_num_tools") (vers "0.1.15") (deps (list (crate-dep (name "itertools") (req "^0.9") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2") (default-features #t) (kind 0)))) (hash "0zl3v43wfr8djs5a2nsjl1gn7iy2j88bd7kdcpf0d71gipdbs50b")))

(define-public crate-iter_num_tools-0.1 (crate (name "iter_num_tools") (vers "0.1.16") (deps (list (crate-dep (name "itertools") (req "^0.9") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2") (default-features #t) (kind 0)))) (hash "0zh8hafd6sg987b5yychkikap188znf1h3qkkg6dliyiq477rqq7")))

(define-public crate-iter_num_tools-0.1 (crate (name "iter_num_tools") (vers "0.1.17") (deps (list (crate-dep (name "itertools") (req "^0.9") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2") (default-features #t) (kind 0)))) (hash "1cymx4ci2lrz0r6b6dbk1w3wp780vsh8z6s5q6l5595m1fl0gwk6")))

(define-public crate-iter_num_tools-0.2 (crate (name "iter_num_tools") (vers "0.2.0") (deps (list (crate-dep (name "itertools") (req "^0.10") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2") (default-features #t) (kind 0)))) (hash "075jscjmy5praiqzy2cvy1rq6jl7gk3z78dw9v8yd14z1p02ag0c")))

(define-public crate-iter_num_tools-0.4 (crate (name "iter_num_tools") (vers "0.4.0") (deps (list (crate-dep (name "approx") (req "^0.4") (default-features #t) (kind 2)) (crate-dep (name "criterion") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "itertools") (req "^0.10") (kind 0)) (crate-dep (name "num-traits") (req "^0.2") (features (quote ("libm"))) (kind 0)))) (hash "0xwish1bv6pya7fb860j2dgig0gyygqd3qpkzgdv2md4hpyli5y1") (yanked #t)))

(define-public crate-iter_num_tools-0.4 (crate (name "iter_num_tools") (vers "0.4.1") (deps (list (crate-dep (name "approx") (req "^0.4") (default-features #t) (kind 2)) (crate-dep (name "criterion") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "itertools") (req "^0.10") (kind 0)) (crate-dep (name "num-traits") (req "^0.2") (features (quote ("libm"))) (kind 0)))) (hash "1xqlymhh2178yjgsrmg4yrkxc024yx09ysmhqz3x3gihbb8vcgnc")))

(define-public crate-iter_num_tools-0.4 (crate (name "iter_num_tools") (vers "0.4.2") (deps (list (crate-dep (name "approx") (req "^0.4") (default-features #t) (kind 2)) (crate-dep (name "criterion") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "itertools") (req "^0.10") (default-features #t) (kind 2)) (crate-dep (name "num-traits") (req "^0.2") (features (quote ("libm"))) (kind 0)))) (hash "03gzyh4lh3lbncis83j6wy97f0zknrp6hh7pg7lw82vda5q6q0z2")))

(define-public crate-iter_num_tools-0.4 (crate (name "iter_num_tools") (vers "0.4.3") (deps (list (crate-dep (name "approx") (req "^0.5") (default-features #t) (kind 2)) (crate-dep (name "array-init") (req "^2.0.0") (default-features #t) (kind 0)) (crate-dep (name "criterion") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "itertools") (req "^0.10") (default-features #t) (kind 2)) (crate-dep (name "num-traits") (req "^0.2") (features (quote ("libm"))) (kind 0)))) (hash "1ryrvpl7dc06zcglifsdwcbkdpgd62695lhgsgd2visnwnzfh1dg")))

(define-public crate-iter_num_tools-0.4 (crate (name "iter_num_tools") (vers "0.4.4") (deps (list (crate-dep (name "array-init") (req "^2.0.0") (default-features #t) (kind 0)) (crate-dep (name "criterion") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "itertools") (req "^0.10") (default-features #t) (kind 2)) (crate-dep (name "num-traits") (req "^0.2") (features (quote ("libm"))) (kind 0)))) (hash "0dqx940hzs6jd837mlnyxjbc3lvfm1ddak9i0vhs23qbmapqg2gp")))

(define-public crate-iter_num_tools-0.4 (crate (name "iter_num_tools") (vers "0.4.5") (deps (list (crate-dep (name "array-init") (req "^2.0.0") (default-features #t) (kind 0)) (crate-dep (name "criterion") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "itertools") (req "^0.10") (default-features #t) (kind 2)) (crate-dep (name "itertools-num") (req "^0.1.3") (default-features #t) (kind 2)) (crate-dep (name "num-traits") (req "^0.2") (features (quote ("libm"))) (kind 0)))) (hash "0yrmblh720kvmd7k50zcfy4vx6s7y42w8kc1vwyn9i4xszyj4x19")))

(define-public crate-iter_num_tools-0.5 (crate (name "iter_num_tools") (vers "0.5.0") (deps (list (crate-dep (name "array-init") (req "^2.0.0") (default-features #t) (kind 0)) (crate-dep (name "criterion") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "itertools") (req "^0.10") (default-features #t) (kind 2)) (crate-dep (name "itertools-num") (req "^0.1.3") (default-features #t) (kind 2)) (crate-dep (name "num-traits") (req "^0.2") (features (quote ("libm"))) (kind 0)))) (hash "0y094q8b605in3fn9j5b7zvb009f4hz2dh8c9gmfwzflxgij79yf") (features (quote (("trusted_len"))))))

(define-public crate-iter_num_tools-0.6 (crate (name "iter_num_tools") (vers "0.6.0") (deps (list (crate-dep (name "array_iter_tools") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "criterion") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "itertools") (req "^0.10") (default-features #t) (kind 2)) (crate-dep (name "itertools-num") (req "^0.1.3") (default-features #t) (kind 2)) (crate-dep (name "num-traits") (req "^0.2") (features (quote ("libm"))) (kind 0)))) (hash "0gb8n8idyj82wp5fgwvwfzzd8x7m8mbc7dsbpifibpr964igyz34") (features (quote (("trusted_len") ("iter_advance_by"))))))

(define-public crate-iter_num_tools-0.6 (crate (name "iter_num_tools") (vers "0.6.1") (deps (list (crate-dep (name "array_iter_tools") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "criterion") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "itertools") (req "^0.10") (default-features #t) (kind 2)) (crate-dep (name "itertools-num") (req "^0.1.3") (default-features #t) (kind 2)) (crate-dep (name "num-traits") (req "^0.2") (features (quote ("libm"))) (kind 0)))) (hash "0sd7h97y35al0a759i9g7xbddxw7jq1vy7iy3md5nfiiyhbrxgxi") (features (quote (("trusted_len") ("iter_advance_by"))))))

(define-public crate-iter_num_tools-0.6 (crate (name "iter_num_tools") (vers "0.6.2") (deps (list (crate-dep (name "array_iter_tools") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "criterion") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "itertools") (req "^0.10") (default-features #t) (kind 2)) (crate-dep (name "itertools-num") (req "^0.1.3") (default-features #t) (kind 2)) (crate-dep (name "num-traits") (req "^0.2") (features (quote ("libm"))) (kind 0)))) (hash "0wriqf0lqm6df79wdhdsra6ym8n6xp4y0n1van3hccn6mgpwc4bp") (features (quote (("trusted_len") ("iter_advance_by"))))))

(define-public crate-iter_num_tools-0.7 (crate (name "iter_num_tools") (vers "0.7.0") (deps (list (crate-dep (name "array-bin-ops") (req "^0.1.6") (default-features #t) (kind 0)) (crate-dep (name "criterion") (req "^0.4.0") (default-features #t) (kind 2)) (crate-dep (name "itertools") (req "^0.10") (default-features #t) (kind 2)) (crate-dep (name "itertools-num") (req "^0.1.3") (default-features #t) (kind 2)) (crate-dep (name "num-traits") (req "^0.2") (features (quote ("libm"))) (kind 0)) (crate-dep (name "pretty_assertions") (req "^1") (default-features #t) (kind 2)))) (hash "1nhzcxmay0w7aixiy8nd1499k1lr6wi05ri8c8dladh5q49vid86") (features (quote (("trusted_len") ("iter_advance_by")))) (yanked #t)))

(define-public crate-iter_num_tools-0.7 (crate (name "iter_num_tools") (vers "0.7.1") (deps (list (crate-dep (name "array-bin-ops") (req "^0.1.6") (default-features #t) (kind 0)) (crate-dep (name "criterion") (req "^0.4.0") (default-features #t) (kind 2)) (crate-dep (name "itertools") (req "^0.10") (default-features #t) (kind 2)) (crate-dep (name "itertools-num") (req "^0.1.3") (default-features #t) (kind 2)) (crate-dep (name "num-traits") (req "^0.2") (features (quote ("libm"))) (kind 0)) (crate-dep (name "pretty_assertions") (req "^1") (default-features #t) (kind 2)))) (hash "197l67h55n9lzlw1xnslgkmas9c1d2kjgbddliib1d22ik0rc6a1") (features (quote (("trusted_len") ("iter_advance_by"))))))

(define-public crate-iter_spread-0.1 (crate (name "iter_spread") (vers "0.1.0") (deps (list (crate-dep (name "array-init") (req "^2.0.1") (default-features #t) (kind 0)))) (hash "10gx1ixcsij5a5nk1xnd1z27g5nxbyp6rdfwqyvsn5labks4s4ax")))

(define-public crate-iter_tools-0.1 (crate (name "iter_tools") (vers "0.1.0") (deps (list (crate-dep (name "itertools") (req "~0.10.3") (default-features #t) (kind 0)) (crate-dep (name "wtest_basic") (req "~0.1") (default-features #t) (kind 2)))) (hash "09f336fcfdnvybm2kdrph2gjcn6m3fbszk6m6qpzk1cy3r8n5jbg")))

(define-public crate-iter_tools-0.1 (crate (name "iter_tools") (vers "0.1.1") (deps (list (crate-dep (name "itertools") (req "~0.10.3") (default-features #t) (kind 0)) (crate-dep (name "test_tools") (req "~0.1") (default-features #t) (kind 2)))) (hash "1zc0kv2b478622x4iqp9gw2ghxhwy079qxma9mxqx5wpnpxsq11h")))

(define-public crate-iter_tools-0.1 (crate (name "iter_tools") (vers "0.1.2") (deps (list (crate-dep (name "itertools") (req "~0.10.3") (optional #t) (kind 0)) (crate-dep (name "test_tools") (req "~0.1") (default-features #t) (kind 2)))) (hash "0klqs6sgfjbfmkv2p1d679zcm63smr458rmka8m0qx14gpncxv1f") (features (quote (("use_std" "itertools/use_std") ("use_alloc" "itertools/use_alloc") ("default" "itertools" "use_std") ("all" "itertools" "use_std"))))))

(define-public crate-iter_tools-0.1 (crate (name "iter_tools") (vers "0.1.3") (deps (list (crate-dep (name "itertools") (req "~0.10.3") (optional #t) (kind 0)) (crate-dep (name "test_tools") (req "~0.1") (default-features #t) (kind 2)))) (hash "0lbiw93n3a97w8c8pl6km2s3fjx7758nzf3lkpwqipfszdzziqay") (features (quote (("use_std" "itertools/use_std") ("use_alloc" "itertools/use_alloc") ("default" "itertools" "use_std") ("all" "itertools" "use_std"))))))

(define-public crate-iter_tools-0.1 (crate (name "iter_tools") (vers "0.1.4") (deps (list (crate-dep (name "itertools") (req "~0.10.3") (default-features #t) (kind 0)) (crate-dep (name "test_tools") (req "~0.1") (default-features #t) (kind 2)))) (hash "15s8hn1p8bhv5nqh16f1xwar9can3pk20migncmjbcxkk7fay72k") (features (quote (("use_std" "itertools/use_std") ("use_alloc" "itertools/use_alloc") ("full" "use_std" "use_alloc") ("default" "use_std"))))))

(define-public crate-iter_tools-0.2 (crate (name "iter_tools") (vers "0.2.0") (deps (list (crate-dep (name "itertools") (req "~0.11.0") (features (quote ("use_std"))) (default-features #t) (kind 0)) (crate-dep (name "test_tools") (req "^0.1.5") (default-features #t) (kind 2)))) (hash "1qgl6bmbwx4yn0d83i21bhfzf3cmqyrs0qfwg7wfgl5wd1l0qhd1") (features (quote (("use_alloc" "itertools/use_alloc") ("no_std") ("full" "enabled") ("enabled") ("default" "enabled"))))))

(define-public crate-iter_tools-0.4 (crate (name "iter_tools") (vers "0.4.0") (deps (list (crate-dep (name "itertools") (req "~0.11.0") (features (quote ("use_std"))) (default-features #t) (kind 0)) (crate-dep (name "test_tools") (req "~0.4.0") (default-features #t) (kind 2)))) (hash "0yghvp4p9bvvy5nh87l5m96hdxkpnzf5arnl8kzsnaa2bhp1c8d6") (features (quote (("use_alloc" "itertools/use_alloc") ("no_std") ("full" "enabled") ("enabled") ("default" "enabled"))))))

(define-public crate-iter_tools-0.5 (crate (name "iter_tools") (vers "0.5.0") (deps (list (crate-dep (name "itertools") (req "~0.11.0") (features (quote ("use_std"))) (default-features #t) (kind 0)) (crate-dep (name "test_tools") (req "~0.5.0") (default-features #t) (kind 2)))) (hash "0c4mhk4yk1ga9hvfl5i4g13ca2vnw181xcgbzjjchxpvr0jbfan0") (features (quote (("use_alloc" "itertools/use_alloc") ("no_std") ("full" "enabled") ("enabled") ("default" "enabled"))))))

(define-public crate-iter_tools-0.6 (crate (name "iter_tools") (vers "0.6.0") (deps (list (crate-dep (name "itertools") (req "~0.11.0") (features (quote ("use_std"))) (default-features #t) (kind 0)) (crate-dep (name "test_tools") (req "~0.6.0") (default-features #t) (kind 2)))) (hash "0kq2ivicidq6bwvincaqsnfixlysgxaignrldxlvf9ggd9syflv6") (features (quote (("use_alloc" "itertools/use_alloc") ("no_std") ("full" "enabled") ("enabled") ("default" "enabled"))))))

(define-public crate-iter_tools-0.7 (crate (name "iter_tools") (vers "0.7.0") (deps (list (crate-dep (name "itertools") (req "~0.11.0") (features (quote ("use_std"))) (default-features #t) (kind 0)) (crate-dep (name "test_tools") (req "~0.6.0") (default-features #t) (kind 2)))) (hash "1q14cp750m7z1vvcwc0mply6dq3h3cabfwjjk9s84ycj222gh94j") (features (quote (("use_alloc" "itertools/use_alloc") ("no_std") ("full" "enabled") ("enabled") ("default" "enabled"))))))

(define-public crate-iter_tools-0.8 (crate (name "iter_tools") (vers "0.8.0") (deps (list (crate-dep (name "itertools") (req "~0.11.0") (features (quote ("use_std"))) (default-features #t) (kind 0)) (crate-dep (name "test_tools") (req "~0.6.0") (default-features #t) (kind 2)))) (hash "1kjq64xqddr07wxhmrwjzb76x6w88l0p21miwzmjhk7y79lx18hb") (features (quote (("use_alloc" "itertools/use_alloc") ("no_std") ("full" "enabled") ("enabled") ("default" "enabled"))))))

(define-public crate-iter_tools-0.9 (crate (name "iter_tools") (vers "0.9.0") (deps (list (crate-dep (name "itertools") (req "~0.11.0") (features (quote ("use_std"))) (default-features #t) (kind 0)) (crate-dep (name "test_tools") (req "~0.6.0") (default-features #t) (kind 2)))) (hash "0r6fqc7gnc6r5zj0q2vqmmz53ia6l2f91lb5z06bgrhqjrp63xjr") (features (quote (("use_alloc" "itertools/use_alloc") ("no_std") ("full" "enabled") ("enabled") ("default" "enabled"))))))

(define-public crate-iter_tools-0.10 (crate (name "iter_tools") (vers "0.10.0") (deps (list (crate-dep (name "itertools") (req "~0.11.0") (features (quote ("use_std"))) (default-features #t) (kind 0)) (crate-dep (name "test_tools") (req "~0.6.0") (default-features #t) (kind 2)))) (hash "0j7bh051f1h94sh34vpgalqh1zljn27f7r9jkv839mjkyxjq58h7") (features (quote (("use_alloc" "itertools/use_alloc") ("no_std") ("full" "enabled") ("enabled") ("default" "enabled"))))))

(define-public crate-iter_tools-0.11 (crate (name "iter_tools") (vers "0.11.0") (deps (list (crate-dep (name "itertools") (req "~0.11.0") (features (quote ("use_std"))) (default-features #t) (kind 0)) (crate-dep (name "test_tools") (req "~0.6.0") (default-features #t) (kind 2)))) (hash "19v5ya4hfn06c963f5w9rzh55yadplfdka7zbcn6xr0zizix7yp8") (features (quote (("use_alloc" "itertools/use_alloc") ("no_std") ("full" "enabled") ("enabled") ("default" "enabled"))))))

(define-public crate-iter_tools-0.12 (crate (name "iter_tools") (vers "0.12.0") (deps (list (crate-dep (name "itertools") (req "~0.11.0") (features (quote ("use_std"))) (default-features #t) (kind 0)) (crate-dep (name "test_tools") (req "~0.7.0") (default-features #t) (kind 2)))) (hash "0ca1gh2m6y2lwj4i60q4zrxyca0qrzn6b6xp2b7yjnywk53l7qwn") (features (quote (("use_alloc" "itertools/use_alloc") ("no_std") ("full" "enabled") ("enabled") ("default" "enabled"))))))

(define-public crate-iter_tools-0.13 (crate (name "iter_tools") (vers "0.13.0") (deps (list (crate-dep (name "itertools") (req "~0.11.0") (features (quote ("use_std"))) (default-features #t) (kind 0)) (crate-dep (name "test_tools") (req "~0.7.0") (default-features #t) (kind 2)))) (hash "1ai7jd22xa9a8z7dlcad5zmllz16p202dzvgflyxrpax7hmjc73x") (features (quote (("use_alloc" "itertools/use_alloc") ("no_std") ("full" "enabled") ("enabled") ("default" "enabled"))))))

(define-public crate-iter_tools-0.14 (crate (name "iter_tools") (vers "0.14.0") (deps (list (crate-dep (name "itertools") (req "~0.11.0") (features (quote ("use_std"))) (default-features #t) (kind 0)) (crate-dep (name "test_tools") (req "~0.8.0") (default-features #t) (kind 2)))) (hash "0cwmx8v2yqg00nv8hl9xhl53nvhjhsxr9qrl11kh66lpbwgpzv7r") (features (quote (("use_alloc" "itertools/use_alloc") ("no_std") ("full" "enabled") ("enabled") ("default" "enabled"))))))

(define-public crate-iter_tools-0.15 (crate (name "iter_tools") (vers "0.15.0") (deps (list (crate-dep (name "itertools") (req "~0.11.0") (features (quote ("use_std"))) (default-features #t) (kind 0)) (crate-dep (name "test_tools") (req "~0.8.0") (default-features #t) (kind 2)))) (hash "1lgzdxjlvz4nlh79q8qjanm9m3w5428ic64n7r7zjf0yg3ajz2hb") (features (quote (("use_alloc" "itertools/use_alloc") ("no_std") ("full" "enabled") ("enabled") ("default" "enabled"))))))

(define-public crate-iter_tools-0.16 (crate (name "iter_tools") (vers "0.16.0") (deps (list (crate-dep (name "itertools") (req "~0.11.0") (features (quote ("use_std"))) (default-features #t) (kind 0)) (crate-dep (name "test_tools") (req "~0.8.0") (default-features #t) (kind 2)))) (hash "18pw6q36lxncwkx5q3fnbx4b8gdd2c4q7jp7fscfza0l0zpyk8yx") (features (quote (("use_alloc" "itertools/use_alloc") ("no_std") ("full" "enabled") ("enabled") ("default" "enabled"))))))

(define-public crate-iter_tools-0.17 (crate (name "iter_tools") (vers "0.17.0") (deps (list (crate-dep (name "itertools") (req "~0.11.0") (features (quote ("use_std"))) (default-features #t) (kind 0)) (crate-dep (name "test_tools") (req "~0.9.0") (default-features #t) (kind 2)))) (hash "1w7kngvw0xq6i3fif8wq73hb6j37g280fhq1a9ykd8h86c5z9yam") (features (quote (("use_alloc" "itertools/use_alloc") ("no_std") ("full" "enabled") ("enabled") ("default" "enabled"))))))

(define-public crate-iter_traits-0.1 (crate (name "iter_traits") (vers "0.1.0") (hash "0qr6q15sc3qaxd7rcdp80mzkk8nsxmksjaz6kdi7c8rcv34jv536") (yanked #t)))

(define-public crate-iter_traits-0.1 (crate (name "iter_traits") (vers "0.1.1") (hash "13npkrbn32zp2mg5r681p0n18xvzn491ygqg5gl65qpxivl719x7") (yanked #t)))

(define-public crate-iter_utils-0.1 (crate (name "iter_utils") (vers "0.1.0") (hash "1zynzvb985gaafi7b5jz596rcv4ibd27ygskhs2dsxy1k3hl9r2s")))

(define-public crate-iter_vals-0.1 (crate (name "iter_vals") (vers "0.1.0") (hash "05i852nqbgmr9ji5j0y5m0mz6ky2vqfn91a0ym94cjhdrwkq5sgr")))

(define-public crate-iter_vec-0.1 (crate (name "iter_vec") (vers "0.1.0") (hash "0558m7pf65v1v1gh2856djgdcz3pxxnxl5q9icdgnfbcffqfr737")))

(define-public crate-iter_view-0.1 (crate (name "iter_view") (vers "0.1.0") (hash "0zixyzmdwwkhysf6n3dl6ygnvyn9p5wkgqb93fr202yqywbmmzn8")))

(define-public crate-iter_view-0.1 (crate (name "iter_view") (vers "0.1.1") (hash "04sdisk6cmd1bccxmc0zm6ji3qmriwihxkd5zr6500s4jd68895s")))

(define-public crate-iter_view-0.1 (crate (name "iter_view") (vers "0.1.2") (hash "0r12jgdl5b83jvfs7krf53cvaccnl5q2h28afrb5xmkzss5ck2ww")))

(define-public crate-iter_view-0.1 (crate (name "iter_view") (vers "0.1.3") (hash "0pvyglp61mxq4n315jsr6pqkbrk9wgqa8smnjdch7d71d5dmg9wy")))

(define-public crate-iter_view-0.1 (crate (name "iter_view") (vers "0.1.4") (hash "01xjlzvadai0d8cpddaz6dxwssiflbh0h21x41yn8xvqkiz66khh")))

(define-public crate-iteraaaaaaaaaaaaaaaaaaaaaaaaaaaaaa-0.1 (crate (name "iteraaaaaaaaaaaaaaaaaaaaaaaaaaaaaa") (vers "0.1.0") (hash "00jhpj3z6w3yj2wcrvkc5z8n9zs8s098y3phvl5b35kp24p2ji2h") (yanked #t)))

(define-public crate-iterable-0.1 (crate (name "iterable") (vers "0.1.0") (deps (list (crate-dep (name "maplit") (req "^1.0.2") (default-features #t) (kind 2)))) (hash "1yamw5bkm5b81fbflhn2rnkd3sl11vkqd4mn6kz8bwwlnf6fyj76")))

(define-public crate-iterable-0.2 (crate (name "iterable") (vers "0.2.0") (deps (list (crate-dep (name "itertools") (req "^0.9.0") (default-features #t) (kind 0)) (crate-dep (name "maplit") (req "^1.0.2") (default-features #t) (kind 2)))) (hash "1pj8j89pszwjl68p9l9ybm088x7xjnimynqhx56d2np26fylr3v3")))

(define-public crate-iterable-0.3 (crate (name "iterable") (vers "0.3.0") (deps (list (crate-dep (name "itertools") (req "^0.9.0") (default-features #t) (kind 0)) (crate-dep (name "maplit") (req "^1.0.2") (default-features #t) (kind 2)))) (hash "1kpfhwxpbycpz445b6h3g3p3rjyxbpnd10lvl8kg963z3bifwkrv")))

(define-public crate-iterable-0.4 (crate (name "iterable") (vers "0.4.0") (deps (list (crate-dep (name "itertools") (req "^0.9.0") (default-features #t) (kind 0)) (crate-dep (name "maplit") (req "^1.0.2") (default-features #t) (kind 2)))) (hash "14fwkkh19w3bsvmign2fpnr9344045b7v7a2dinbfv0mpmns6m0h")))

(define-public crate-iterable-0.4 (crate (name "iterable") (vers "0.4.1") (deps (list (crate-dep (name "itertools") (req "^0.9.0") (default-features #t) (kind 0)) (crate-dep (name "maplit") (req "^1.0.2") (default-features #t) (kind 2)))) (hash "1pdlxdx561c58mfbfv1y3jgiiswpxasmzy7al0x35szkl4avq5zn")))

(define-public crate-iterable-0.5 (crate (name "iterable") (vers "0.5.0") (deps (list (crate-dep (name "itertools") (req "^0.10.3") (default-features #t) (kind 0)) (crate-dep (name "maplit") (req "^1.0.2") (default-features #t) (kind 2)))) (hash "1nyz0g1k1rqdak6jzq13rjjbxz6xjd2w6x19v3c601mj7wk5kndp")))

(define-public crate-iterable-0.6 (crate (name "iterable") (vers "0.6.0") (deps (list (crate-dep (name "itertools") (req "^0.10.5") (default-features #t) (kind 0)) (crate-dep (name "maplit") (req "^1.0.2") (default-features #t) (kind 2)))) (hash "0px6axq6vlm68hj1wp78xilp9w46n8dh90kxasjmrzvxmgbdylf1")))

(define-public crate-iterate-1 (crate (name "iterate") (vers "1.0.0") (deps (list (crate-dep (name "iterate-proc-macro") (req "^1.0.0") (default-features #t) (kind 0)))) (hash "0q3mhqs99kvkkajs7g514ds4bz1s9d06sdv5p97s575fg1w765xk")))

(define-public crate-iterate-proc-macro-1 (crate (name "iterate-proc-macro") (vers "1.0.0") (deps (list (crate-dep (name "either") (req "^1.6.1") (default-features #t) (kind 0)) (crate-dep (name "itertools") (req "^0.10.0") (default-features #t) (kind 0)) (crate-dep (name "joinery") (req "^2.1.0") (features (quote ("token-stream"))) (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1.0.27") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.9") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.72") (features (quote ("full" "visit"))) (default-features #t) (kind 0)))) (hash "0n9xwx05jqv6fwc1jcaqyvfg11ppy6karngshn4h5q51krs9j3sp")))

(define-public crate-iterate-proc-macro-2 (crate (name "iterate-proc-macro") (vers "2.0.0") (deps (list (crate-dep (name "either") (req "^1.6.1") (default-features #t) (kind 0)) (crate-dep (name "itertools") (req "^0.10.0") (default-features #t) (kind 0)) (crate-dep (name "joinery") (req "^2.1.0") (features (quote ("token-stream"))) (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1.0.27") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.9") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.72") (features (quote ("full" "visit"))) (default-features #t) (kind 0)))) (hash "11sb0k6n4z0crzpmqsmdijxmpjlkr7ckha40gfcdpm0n328f23nl")))

(define-public crate-iterate-text-0.0.1 (crate (name "iterate-text") (vers "0.0.1") (deps (list (crate-dep (name "argparse") (req "^0.2.2") (default-features #t) (kind 2)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 2)))) (hash "0mbhm964gf8fmfr9rg2fvlpvxn5zwj2ijkfidgl18qvw44r88fl5")))

(define-public crate-iterative_methods-0.2 (crate (name "iterative_methods") (vers "0.2.0") (deps (list (crate-dep (name "eigenvalues") (req "^0.3.1") (default-features #t) (kind 0)) (crate-dep (name "nalgebra") (req "^0.19.0") (default-features #t) (kind 0)) (crate-dep (name "ndarray") (req "^0.13.0") (kind 0)) (crate-dep (name "quickcheck") (req "=1.0.1") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.8.0") (default-features #t) (kind 0)) (crate-dep (name "rand_distr") (req "^0.4.0") (default-features #t) (kind 0)) (crate-dep (name "rand_pcg") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "streaming-iterator") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "yaml-rust") (req "^0.4") (default-features #t) (kind 0)))) (hash "1gmkn58nvly8n5wq40p7l1lpagyaws1fsd3a2prh0w0gwr13dfnm")))

(define-public crate-iterative_methods-0.2 (crate (name "iterative_methods") (vers "0.2.1") (deps (list (crate-dep (name "eigenvalues") (req "^0.3.1") (default-features #t) (kind 0)) (crate-dep (name "nalgebra") (req "^0.19.0") (default-features #t) (kind 0)) (crate-dep (name "ndarray") (req "^0.13.0") (kind 0)) (crate-dep (name "quickcheck") (req "=1.0.1") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.8.0") (default-features #t) (kind 0)) (crate-dep (name "rand_distr") (req "^0.4.0") (default-features #t) (kind 0)) (crate-dep (name "rand_pcg") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "streaming-iterator") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "yaml-rust") (req "^0.4") (default-features #t) (kind 0)))) (hash "0qh0pgxamdczhby29wbkn71ccwwxh4q5qspqdf5zvk3ihklz294f")))

(define-public crate-iterator-0.1 (crate (name "iterator") (vers "0.1.0") (hash "1dw81c48w5abg8b1k8szbhdzkk2spz4x9wgyi4fqkfyy39q3rjxk")))

(define-public crate-iterator-2000-0.1 (crate (name "iterator-2000") (vers "0.1.0") (deps (list (crate-dep (name "rand") (req "^0.8.4") (default-features #t) (kind 0)))) (hash "0rw11075qjsxp50frl2mn62byyahm4kficjj2vsz4l073v66ciqm")))

(define-public crate-iterator-assertions-0.1 (crate (name "iterator-assertions") (vers "0.1.0") (deps (list (crate-dep (name "push-in-place") (req "^0.1.0") (default-features #t) (kind 2)))) (hash "03izjmbhw2jvpkw3j1f02l0r1pv3l5xf6cf2jlnb4khky4zrvwvb")))

(define-public crate-iterator-cache-0.1 (crate (name "iterator-cache") (vers "0.1.0") (deps (list (crate-dep (name "queues") (req "^1.1.0") (default-features #t) (kind 0)))) (hash "0iy2zlgwbqvz467g1ykjlsy4rc835glhm34c00gnp951dj26nnys")))

(define-public crate-iterator-endiate-0.1 (crate (name "iterator-endiate") (vers "0.1.0") (hash "1k243bk11s47jc18gnmy61xgcbcnk9dfgrlp4wqs0m7d54wyzzjw")))

(define-public crate-iterator-endiate-0.2 (crate (name "iterator-endiate") (vers "0.2.0") (hash "10vzanv71cyknzd7i6hpkkvgyps1yxi1fhrcxiwasrdq8mf2gs58")))

(define-public crate-iterator-endiate-0.2 (crate (name "iterator-endiate") (vers "0.2.1") (hash "04ki6nx2pjdwk1z5rpcpxpjkvzdc7fms1wifk1zv630a381lgffa")))

(define-public crate-iterator-enum-0.1 (crate (name "iterator-enum") (vers "0.1.0") (deps (list (crate-dep (name "derive_utils") (req "^0.6.2") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^0.6.8") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^0.15.19") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "1y6rx5p0w6vra7czydwbvzl4wf9slwpsngznhpxw0m9svim7miq2") (features (quote (("try_trait") ("trusted_len") ("std") ("exact_size_is_empty") ("default" "std"))))))

(define-public crate-iterator-enum-0.1 (crate (name "iterator-enum") (vers "0.1.1") (deps (list (crate-dep (name "derive_utils") (req "^0.6.2") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^0.6.8") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^0.15.19") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "0sgc6frrwrby0q63b85wlaa9v23rn7xmqxrjalfwazgiz62h7nk4") (features (quote (("try_trait") ("trusted_len") ("std") ("exact_size_is_empty") ("default" "std"))))))

(define-public crate-iterator-enum-0.1 (crate (name "iterator-enum") (vers "0.1.2") (deps (list (crate-dep (name "derive_utils") (req "^0.6.3") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^0.6.8") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^0.15.22") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "0qpdg33n83qsmlqy1r7jv0xfl5b9xrcn7in5b3v12ix7ws4bky96") (features (quote (("try_trait") ("trusted_len") ("std") ("exact_size_is_empty") ("default" "std"))))))

(define-public crate-iterator-enum-0.2 (crate (name "iterator-enum") (vers "0.2.0") (deps (list (crate-dep (name "derive_utils") (req "^0.7.2") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^0.4.13") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^0.6.8") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^0.15.29") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "00jhhb9lajiymk5ls4am0q8vpafhrzpfl09pfkcc40bjlq7qj7by") (features (quote (("try_trait") ("trusted_len") ("exact_size_is_empty") ("default"))))))

(define-public crate-iterator-enum-0.2 (crate (name "iterator-enum") (vers "0.2.1") (deps (list (crate-dep (name "derive_utils") (req "^0.9") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "rayon_crate") (req "^1.1") (default-features #t) (kind 2) (package "rayon")) (crate-dep (name "syn") (req "^1.0") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "0fna1qbc9q7jr1d815f9f1rird4jyxjiqcbb931x3db56jpff3yw") (features (quote (("try_trait") ("trusted_len") ("rayon") ("exact_size_is_empty"))))))

(define-public crate-iterator-enum-0.2 (crate (name "iterator-enum") (vers "0.2.2") (deps (list (crate-dep (name "derive_utils") (req "^0.9") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "rayon_crate") (req "^1.2") (default-features #t) (kind 2) (package "rayon")) (crate-dep (name "syn") (req "^1.0") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "1lss5la54f061plw9hcgslvjhqjq5im9rms14jllzq9zcfsy8dbd") (features (quote (("trusted_len") ("rayon"))))))

(define-public crate-iterator-enum-0.2 (crate (name "iterator-enum") (vers "0.2.3") (deps (list (crate-dep (name "iter-enum") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "rayon_crate") (req "^1.2") (default-features #t) (kind 2) (package "rayon")))) (hash "037zxpq6qxkkdyh068f29215iyxjqzv7s6l4bvri827smg6ryl7f") (features (quote (("trusted_len" "iter-enum/trusted_len") ("rayon" "iter-enum/rayon"))))))

(define-public crate-iterator-ext-0.1 (crate (name "iterator-ext") (vers "0.1.0") (hash "0nr2pv2ygh36ax7m9ng7gbhhfcq7yzkkaak6ys29izy7sic4glxf")))

(define-public crate-iterator-ext-0.2 (crate (name "iterator-ext") (vers "0.2.0") (hash "0dw0ji4kgcarcnaak7qzpi8agpa20r1s1d5il62js9gkxzzcfjhn")))

(define-public crate-iterator-ext-0.2 (crate (name "iterator-ext") (vers "0.2.1") (hash "1k2k6a4rsjpkki8qjhph04byiqxh77m5kdrkgpsl25w0zpjddj32")))

(define-public crate-iterator-sorted-0.1 (crate (name "iterator-sorted") (vers "0.1.0") (hash "1llbpv5p150ph1xqvs51p4wd238csyg9pwlbq559zyf85dfpf0fi")))

(define-public crate-iterator-sorted-0.2 (crate (name "iterator-sorted") (vers "0.2.0") (hash "1l9m9jw9h4r345i4bkh3x1b4374xlslw37cqkd1ndhhz35k1sg7d")))

(define-public crate-iterator_ilp-1 (crate (name "iterator_ilp") (vers "1.0.0") (deps (list (crate-dep (name "criterion") (req "^0.3") (kind 2)) (crate-dep (name "multiversion") (req "^0.7") (default-features #t) (kind 2)) (crate-dep (name "num-traits") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "quickcheck") (req "^1.0") (default-features #t) (kind 2)) (crate-dep (name "quickcheck_macros") (req "^1.0") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.8") (default-features #t) (kind 2)))) (hash "0pnl3p23mvjlmqa33z4v3plf40f75ffj7hfcq6hjvg5sg2bx87f3") (features (quote (("std") ("default" "std")))) (rust-version "1.67.0")))

(define-public crate-iterator_ilp-1 (crate (name "iterator_ilp") (vers "1.0.1") (deps (list (crate-dep (name "criterion") (req "^0.3") (kind 2)) (crate-dep (name "multiversion") (req "^0.7") (default-features #t) (kind 2)) (crate-dep (name "num-traits") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "quickcheck") (req "^1.0") (default-features #t) (kind 2)) (crate-dep (name "quickcheck_macros") (req "^1.0") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.8") (default-features #t) (kind 2)))) (hash "0v3mvrssfbky735np727cx37cssnff37mrdgascb848ai2hx2fx3") (features (quote (("std") ("default" "std")))) (rust-version "1.67.0")))

(define-public crate-iterator_ilp-1 (crate (name "iterator_ilp") (vers "1.0.2") (deps (list (crate-dep (name "criterion") (req "^0.3") (kind 2)) (crate-dep (name "multiversion") (req "^0.7") (default-features #t) (kind 2)) (crate-dep (name "num-traits") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "quickcheck") (req "^1.0") (default-features #t) (kind 2)) (crate-dep (name "quickcheck_macros") (req "^1.0") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.8") (default-features #t) (kind 2)))) (hash "18j9g5gisiqks5590h02pwzlyd2gbwv3h84idkgbnldb8d0dajnr") (features (quote (("std") ("default" "std")))) (rust-version "1.67.0")))

(define-public crate-iterator_ilp-1 (crate (name "iterator_ilp") (vers "1.0.3") (deps (list (crate-dep (name "criterion") (req "^0.3") (kind 2)) (crate-dep (name "multiversion") (req "^0.7") (default-features #t) (kind 2)) (crate-dep (name "num-traits") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "quickcheck") (req "^1.0") (default-features #t) (kind 2)) (crate-dep (name "quickcheck_macros") (req "^1.0") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.8") (default-features #t) (kind 2)))) (hash "0qbhbxygfp9lqwd5pv0zifn7rdasabl5llxfgizwivc597bxpkds") (features (quote (("std") ("default" "std")))) (rust-version "1.67.0")))

(define-public crate-iterator_ilp-1 (crate (name "iterator_ilp") (vers "1.0.4") (deps (list (crate-dep (name "criterion") (req "^0.3") (kind 2)) (crate-dep (name "multiversion") (req "^0.7") (default-features #t) (kind 2)) (crate-dep (name "num-traits") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "quickcheck") (req "^1.0") (default-features #t) (kind 2)) (crate-dep (name "quickcheck_macros") (req "^1.0") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.8") (default-features #t) (kind 2)))) (hash "0d857346xqf73sfdmlw17hn7rsaj56s4z8wkn7gabcyjb2y17983") (features (quote (("std") ("default" "std")))) (rust-version "1.67.0")))

(define-public crate-iterator_ilp-1 (crate (name "iterator_ilp") (vers "1.0.5") (deps (list (crate-dep (name "criterion") (req "^0.3") (kind 2)) (crate-dep (name "multiversion") (req "^0.7") (default-features #t) (kind 2)) (crate-dep (name "num-traits") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "quickcheck") (req "^1.0") (default-features #t) (kind 2)) (crate-dep (name "quickcheck_macros") (req "^1.0") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.8") (default-features #t) (kind 2)))) (hash "01hwp797nanc27rkjh6c1qchqfkfnlf7ny72h84wlcmap29cdmfg") (features (quote (("std") ("default" "std")))) (rust-version "1.67.0")))

(define-public crate-iterator_ilp-2 (crate (name "iterator_ilp") (vers "2.0.0") (deps (list (crate-dep (name "criterion") (req "^0.3") (kind 2)) (crate-dep (name "multiversion") (req "^0.7") (default-features #t) (kind 2)) (crate-dep (name "num-traits") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "quickcheck") (req "^1.0") (default-features #t) (kind 2)) (crate-dep (name "quickcheck_macros") (req "^1.0") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.8") (default-features #t) (kind 2)))) (hash "12v4b1bx8rz6by3g2q1i05bdazr164x8xnqxdaaxpn61yp6jidjq") (features (quote (("std") ("default" "std")))) (rust-version "1.67.0")))

(define-public crate-iterator_ilp-2 (crate (name "iterator_ilp") (vers "2.0.1") (deps (list (crate-dep (name "criterion") (req "^0.3") (kind 2)) (crate-dep (name "multiversion") (req "^0.7") (default-features #t) (kind 2)) (crate-dep (name "num-traits") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "quickcheck") (req "^1.0") (default-features #t) (kind 2)) (crate-dep (name "quickcheck_macros") (req "^1.0") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.8") (default-features #t) (kind 2)))) (hash "0l659lcljg9fdwsw6gfh662c1nkyk23jpz9jv6c742pgkd078vd7") (features (quote (("std") ("default" "std")))) (rust-version "1.67.0")))

(define-public crate-iterator_ilp-2 (crate (name "iterator_ilp") (vers "2.0.2") (deps (list (crate-dep (name "criterion") (req "^0.3") (kind 2)) (crate-dep (name "multiversion") (req "^0.7") (default-features #t) (kind 2)) (crate-dep (name "num-traits") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "quickcheck") (req "^1.0") (default-features #t) (kind 2)) (crate-dep (name "quickcheck_macros") (req "^1.0") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.8") (default-features #t) (kind 2)))) (hash "1l0h9g6vs7vv6wqryff0xyxa141by711nl52xq2a06i78ryyvwmi") (features (quote (("std") ("default" "std")))) (rust-version "1.67.0")))

(define-public crate-iterator_ilp-2 (crate (name "iterator_ilp") (vers "2.0.3") (deps (list (crate-dep (name "criterion") (req "^0.3") (kind 2)) (crate-dep (name "multiversion") (req "^0.7") (default-features #t) (kind 2)) (crate-dep (name "num-traits") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "quickcheck") (req "^1.0") (default-features #t) (kind 2)) (crate-dep (name "quickcheck_macros") (req "^1.0") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.8") (default-features #t) (kind 2)))) (hash "1z4fjp1z61a09dg6njjg9hqmv85gg3v3a4j0l01qgwk7lwv5l8ra") (features (quote (("std") ("default" "std")))) (rust-version "1.67.0")))

(define-public crate-iterator_ilp-2 (crate (name "iterator_ilp") (vers "2.0.4") (deps (list (crate-dep (name "criterion") (req "^0.3") (kind 2)) (crate-dep (name "multiversion") (req "^0.7") (default-features #t) (kind 2)) (crate-dep (name "num-traits") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "quickcheck") (req "^1.0") (default-features #t) (kind 2)) (crate-dep (name "quickcheck_macros") (req "^1.0") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.8") (default-features #t) (kind 2)))) (hash "1gmfh4jx10z14kb3pn6kcy9zyidsg7mrhqwq4s6cwzfh1p661xnh") (features (quote (("std") ("default" "std")))) (rust-version "1.67.0")))

(define-public crate-iterator_ilp-2 (crate (name "iterator_ilp") (vers "2.0.5") (deps (list (crate-dep (name "criterion") (req "^0.5") (kind 2)) (crate-dep (name "multiversion") (req "^0.7") (default-features #t) (kind 2)) (crate-dep (name "num-traits") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "proptest") (req "^1.3") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.8") (default-features #t) (kind 2)))) (hash "09dki0n0zicalvcd5jj6naszpmpjm7myaphq68fm6fjlxrwj47sd") (features (quote (("std") ("default" "std")))) (rust-version "1.70.0")))

(define-public crate-iterator_ilp-2 (crate (name "iterator_ilp") (vers "2.0.6") (deps (list (crate-dep (name "criterion") (req "^0.5") (kind 2)) (crate-dep (name "multiversion") (req "^0.7") (default-features #t) (kind 2)) (crate-dep (name "num-traits") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "proptest") (req "^1.3") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.8") (default-features #t) (kind 2)))) (hash "1p0clwyxn1r255nmbs9skhnb744p8zb3ynm1m78x3g9p1g52n092") (features (quote (("std") ("default" "std")))) (rust-version "1.70.0")))

(define-public crate-iterator_ilp-2 (crate (name "iterator_ilp") (vers "2.1.0") (deps (list (crate-dep (name "criterion") (req "^0.5") (kind 2)) (crate-dep (name "hwlocality") (req "^1.0.0-alpha.3") (default-features #t) (kind 2)) (crate-dep (name "multiversion") (req "^0.7") (default-features #t) (kind 2)) (crate-dep (name "num-traits") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "proptest") (req "^1.3") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.8") (default-features #t) (kind 2)) (crate-dep (name "static_assertions") (req "^1.1.0") (default-features #t) (kind 2)))) (hash "0y91mj3ba5wsjjda1kc12s4fmhzvr8wn3zjr4gr6dg10zzhp141z") (features (quote (("std") ("default" "std")))) (rust-version "1.71.0")))

(define-public crate-iterator_ilp-2 (crate (name "iterator_ilp") (vers "2.1.1") (deps (list (crate-dep (name "criterion") (req "^0.5") (kind 2)) (crate-dep (name "hwlocality") (req "^1.0.0-alpha.3") (default-features #t) (kind 2)) (crate-dep (name "multiversion") (req "^0.7") (default-features #t) (kind 2)) (crate-dep (name "num-traits") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "proptest") (req "^1.3") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.8") (default-features #t) (kind 2)) (crate-dep (name "static_assertions") (req "^1.1.0") (default-features #t) (kind 2)))) (hash "0rghbmfqswxb507zkaj2v2n9irg2aq1l1s0ws24v9ajp9sbxbx82") (features (quote (("std") ("default" "std")))) (rust-version "1.71.0")))

(define-public crate-iterator_ilp-2 (crate (name "iterator_ilp") (vers "2.1.2") (deps (list (crate-dep (name "criterion") (req "^0.5") (kind 2)) (crate-dep (name "hwlocality") (req "^1.0.0-alpha.3") (default-features #t) (kind 2)) (crate-dep (name "multiversion") (req "^0.7") (default-features #t) (kind 2)) (crate-dep (name "num-traits") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "proptest") (req "^1.3") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.8") (default-features #t) (kind 2)) (crate-dep (name "static_assertions") (req "^1.1.0") (default-features #t) (kind 2)))) (hash "0c98khj07wva722wkhmskdd7v5fckihncrvah1cg0bfvkd35rgc0") (features (quote (("std") ("default" "std")))) (rust-version "1.71.0")))

(define-public crate-iterator_item-0.1 (crate (name "iterator_item") (vers "0.1.0") (deps (list (crate-dep (name "futures") (req "^0.3.17") (default-features #t) (kind 0)) (crate-dep (name "iterator_item_macros") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.13") (features (quote ("macros" "rt"))) (default-features #t) (kind 2)))) (hash "05fwdfx76al35jqly3436hwx020jksbmgccily5hr880wy3dx085") (features (quote (("std_async_iter"))))))

(define-public crate-iterator_item-0.2 (crate (name "iterator_item") (vers "0.2.0") (deps (list (crate-dep (name "futures") (req "^0.3.17") (default-features #t) (kind 0)) (crate-dep (name "iterator_item_macros") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.13") (features (quote ("macros" "rt"))) (default-features #t) (kind 2)))) (hash "0rp679ri7ywxn691kx9c03slix8aw65mq1cvw1izkgkvwrp3xm1y") (features (quote (("std_async_iter"))))))

(define-public crate-iterator_item_macros-0.1 (crate (name "iterator_item_macros") (vers "0.1.0") (deps (list (crate-dep (name "proc-macro2") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1") (features (quote ("full" "visit-mut" "parsing" "fold"))) (default-features #t) (kind 0)))) (hash "17qldnzi19hfkp57rcslixal3gg9nl68yn20whfrqqqrwhhbxzfm")))

(define-public crate-iterator_to_hash_map-0.9 (crate (name "iterator_to_hash_map") (vers "0.9.0") (hash "1fv7l8qc7b2v7h6gf0h8m4pljzq9ychp21wg64s9q05zi070d7na")))

(define-public crate-iterator_whq-0.1 (crate (name "iterator_whq") (vers "0.1.0") (hash "0i5iydfppy6jwpz7h4dbwgrzh86yfnnlsnpk03ayb794zh4i29nm")))

(define-public crate-iterators-0.1 (crate (name "iterators") (vers "0.1.0") (hash "08is751sx3rhvj78y2qzpgdr99braxl9h3vsf0vmvk7zg8r0h2sw")))

(define-public crate-iterators-collection-0.3 (crate (name "iterators-collection") (vers "0.3.0") (hash "04ql3fsjl6cjz5gvcsdmnjmkgb9m2nh4l9jzw73zljaanhjqagak")))

(define-public crate-iterators-collection-0.3 (crate (name "iterators-collection") (vers "0.3.1") (hash "0a1jz6adl98c2p3af0xh1al07mc3w67ci79xdpq2vadnqp6mdrwh")))

(define-public crate-iterators-collection-0.3 (crate (name "iterators-collection") (vers "0.3.2") (hash "0yhagmdzpc50vpz4rn5q14jrbfaxxan2s55vm295i2rnf7n9bmkp")))

(define-public crate-iterators-collection-0.3 (crate (name "iterators-collection") (vers "0.3.3") (hash "0886gw47ygjzwmnf30pzmm0y64bqwg4dy2wj1xdxm1m1y586c8dy")))

(define-public crate-iterators-rfoxmich-0.1 (crate (name "iterators-rfoxmich") (vers "0.1.0") (hash "17khgd3igqbpn5ac3dw1s6zzyngpj046lp7dvgbi2vralqc43i7h")))

(define-public crate-iterators-rfoxmich-0.1 (crate (name "iterators-rfoxmich") (vers "0.1.1") (hash "0gvnlrhmnkp0abi4q6l78nzj9b98hcw4ldy0dila7y8ynp70rqkx")))

(define-public crate-iterators_closures-0.1 (crate (name "iterators_closures") (vers "0.1.0") (hash "0dbg19vzjavbhaqs4knv1q1dk0lpi27is0hv6cy3s5mnzl04bah3")))

(define-public crate-iterchunks-0.1 (crate (name "iterchunks") (vers "0.1.0") (hash "1i8zw8r0lp99kiyj10ff5xgzj2zcp4xmflxxwg79k9qs5nr9yis7")))

(define-public crate-iterchunks-0.1 (crate (name "iterchunks") (vers "0.1.1") (hash "0kxiwj8819h66wg69jns1wrlpf97n5zxrngrmpzjm4jyj185s7sb") (rust-version "1.56")))

(define-public crate-iterchunks-0.1 (crate (name "iterchunks") (vers "0.1.2") (deps (list (crate-dep (name "arrays") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "1ybac2n4gmaz4c6vqjm48gmk7i1r1gpd7vw77dmfimm2a6mskvz0") (rust-version "1.56")))

(define-public crate-iterchunks-0.2 (crate (name "iterchunks") (vers "0.2.0") (deps (list (crate-dep (name "arrays") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "0xaikgbaaa2h62r4a5vd2g5arkgv1s2l2qlwfwm8rj4s4wfhn9qg") (rust-version "1.56")))

(define-public crate-iterchunks-0.3 (crate (name "iterchunks") (vers "0.3.0") (deps (list (crate-dep (name "arrays") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "1037h1nbasgrp75mmg1322vfbmj8n5mvyh1m52z79j1v6ij19dnf") (rust-version "1.56")))

(define-public crate-iterchunks-0.4 (crate (name "iterchunks") (vers "0.4.0") (deps (list (crate-dep (name "itermore") (req "^0.5.0") (features (quote ("array_chunks"))) (kind 0)))) (hash "0wwap136md1gmcwj9qmnx5r2kw6icin4w62il4yjgl21a864xsj2") (rust-version "1.56")))

(define-public crate-iterchunks-0.5 (crate (name "iterchunks") (vers "0.5.0") (deps (list (crate-dep (name "itermore") (req "^0.6.0") (features (quote ("array_chunks"))) (kind 0)))) (hash "1vrwl5j5agm8bsxz8srs57hxmf8ki757nqydp7cwzxg6rfi8jxzm") (rust-version "1.56")))

(define-public crate-itercounter-0.1 (crate (name "itercounter") (vers "0.1.0") (hash "0kk9wfg71cwm79ifrxvkkx0p4pqbk5363bghjbbr4h4902hp4ds4")))

(define-public crate-iterdir-0.1 (crate (name "iterdir") (vers "0.1.0") (hash "1yk0mk320szfvmvm9zwwgzz4gcz1sig18392vsyiz7q8jdr63xwz")))

(define-public crate-iterext-0.1 (crate (name "iterext") (vers "0.1.0") (deps (list (crate-dep (name "itertools") (req "^0.10.0") (default-features #t) (kind 0)))) (hash "0jdb9dv2132dgvippywl14kxrpjayi9xg3q1cw2mjl5jfawl0sp7")))

(define-public crate-iterextd-0.1 (crate (name "iterextd") (vers "0.1.0") (deps (list (crate-dep (name "trybuild") (req "^1.0.83") (default-features #t) (kind 2)))) (hash "017hfh8dq5zwnggl64mj2ngv89812l79ww8dw6rllpxdx9smxill")))

(define-public crate-iterextd-0.2 (crate (name "iterextd") (vers "0.2.0") (deps (list (crate-dep (name "trybuild") (req "^1.0.83") (default-features #t) (kind 2)))) (hash "07i5bprq7265h0h0jlqhlcx9mz47gr4swk8amp6gh8wd3wkz69js") (features (quote (("itern"))))))

(define-public crate-iterextd-0.3 (crate (name "iterextd") (vers "0.3.0") (deps (list (crate-dep (name "trybuild") (req "^1.0.83") (default-features #t) (kind 2)))) (hash "0s67nkkmyrrmjh38jizjkr865rxlfvsfh85r9g454f618gnn00y2") (features (quote (("itern"))))))

(define-public crate-iterextd-0.4 (crate (name "iterextd") (vers "0.4.0") (deps (list (crate-dep (name "criterion") (req "^0.5.1") (default-features #t) (kind 2)) (crate-dep (name "fixedbitset") (req "^0.5.7") (default-features #t) (kind 0)) (crate-dep (name "itertools") (req "^0.12.1") (default-features #t) (kind 0)) (crate-dep (name "num_convert") (req "^0.7.1") (default-features #t) (kind 0)) (crate-dep (name "trybuild") (req "^1.0.83") (default-features #t) (kind 2)))) (hash "1vd1cxwl1i1cimiq5inc2ynyqvcq4g5f3am0zklrdcp6yl1zpkfb") (features (quote (("itern"))))))

(define-public crate-iterextd-0.4 (crate (name "iterextd") (vers "0.4.1") (deps (list (crate-dep (name "criterion") (req "^0.5.1") (default-features #t) (kind 2)) (crate-dep (name "fixedbitset") (req "^0.5.7") (default-features #t) (kind 0)) (crate-dep (name "itertools") (req "^0.12.1") (default-features #t) (kind 0)) (crate-dep (name "num_convert") (req "^0.7.1") (default-features #t) (kind 0)) (crate-dep (name "trybuild") (req "^1.0.83") (default-features #t) (kind 2)))) (hash "05v72qrj470jr220hpdyl7a5ihlpmm98igjxllcf5hknxzpfqrdk") (features (quote (("itern")))) (rust-version "1.75.0")))

(define-public crate-iterify-0.1 (crate (name "iterify") (vers "0.1.0") (hash "1pmq5hclf1xhzr799nwrzzr6a8cb43bz8pp6vdgpp8cgrgrbzizv")))

(define-public crate-iterify-0.1 (crate (name "iterify") (vers "0.1.1") (hash "0a2gq4vw4bq09b5zg2j58fnhhag4pg0ywdlylcq2h33drf836cci")))

(define-public crate-iterify-0.1 (crate (name "iterify") (vers "0.1.2") (hash "1bmixp3ifxcypqrbmf0vmcq9606f86p4jj1x5wkgw2wsrl1j4fl9")))

(define-public crate-iteritor-0.1 (crate (name "iteritor") (vers "0.1.0") (hash "1ifd35az9x9ypvg7az6yxh34fnbfaj9akyfr5is666zgrm4a1j7k") (features (quote (("std") ("nightly") ("default" "std")))) (rust-version "1.56")))

(define-public crate-iterlower-1 (crate (name "iterlower") (vers "1.0.0") (deps (list (crate-dep (name "unic-ucd") (req "^0.9.0") (default-features #t) (kind 0)))) (hash "0fx7w5lyfbmd2s6xnjax8sgkvmx89dzfl0nibvdzga7m5appplad")))

(define-public crate-iterlower-1 (crate (name "iterlower") (vers "1.0.1") (deps (list (crate-dep (name "unic-ucd") (req "^0.9.0") (default-features #t) (kind 0)))) (hash "1mvmc5w99cb6ppfiqxdisy06zxwmwmhgwrvbwhrqclbwr3l1430l")))

(define-public crate-iterm-0.1 (crate (name "iterm") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^4.4.7") (default-features #t) (kind 0)) (crate-dep (name "duct") (req "^0.13.6") (default-features #t) (kind 0)))) (hash "1bhpvgvwwlrw57s7435jzjh0yksv4dpj9b4w2nna8sp3zfc8x358")))

(define-public crate-iterm-0.2 (crate (name "iterm") (vers "0.2.0") (deps (list (crate-dep (name "clap") (req "^4.4.7") (default-features #t) (kind 0)) (crate-dep (name "duct") (req "^0.13.6") (default-features #t) (kind 0)))) (hash "1s7hwmvrhmx7a2pals25zb700yrl63wjim6iqsv3hmn4xhpzkx6a")))

(define-public crate-iterm-0.3 (crate (name "iterm") (vers "0.3.0") (deps (list (crate-dep (name "clap") (req "^4.4.7") (default-features #t) (kind 0)) (crate-dep (name "duct") (req "^0.13.6") (default-features #t) (kind 0)))) (hash "1rln4slps82g6xnc7z66zd868j2gw3564w3cbsa8ma95ab8dp7bd")))

(define-public crate-iterm-0.4 (crate (name "iterm") (vers "0.4.0") (deps (list (crate-dep (name "clap") (req "^4.4.7") (default-features #t) (kind 0)) (crate-dep (name "duct") (req "^0.13.6") (default-features #t) (kind 0)))) (hash "1zjkhzax1a7bbn0hcfjfx0jak77hgvsvhb0i65d2d9w0qaf72jjc")))

(define-public crate-iterm-0.5 (crate (name "iterm") (vers "0.5.0") (deps (list (crate-dep (name "clap") (req "^4.4.7") (default-features #t) (kind 0)) (crate-dep (name "duct") (req "^0.13.6") (default-features #t) (kind 0)))) (hash "1gba4011bg4vpdaykcqrmj3ww9kpyjx7vjzz68xvnr9dmba1q2zw")))

(define-public crate-iterm-0.6 (crate (name "iterm") (vers "0.6.0") (deps (list (crate-dep (name "clap") (req "^4.4.7") (default-features #t) (kind 0)))) (hash "0s0riqxny9yml8dhwbj3w81cc6lnrirk8vh2pwrj84mq1j4mrsr9")))

(define-public crate-iterm2-0.1 (crate (name "iterm2") (vers "0.1.0") (deps (list (crate-dep (name "base64") (req "^0.9.0") (default-features #t) (kind 0)))) (hash "1njr60h0drgw18qjm7m3km150b2y2jw442kiwrikvi3chl145zw8")))

(define-public crate-iterm2-0.2 (crate (name "iterm2") (vers "0.2.0") (deps (list (crate-dep (name "base64") (req "^0.9.0") (default-features #t) (kind 0)))) (hash "0qhirggdw2h9rybfigh16m8s0z1wnnyyb53m28pvn87rq2hw23nj")))

(define-public crate-iterm2-0.2 (crate (name "iterm2") (vers "0.2.1") (deps (list (crate-dep (name "base64") (req "^0.10.1") (default-features #t) (kind 0)))) (hash "1ric9ypwj2ylalg9zjmacmzj5fs3r9xkai753h2gysk7rkfggxzv")))

(define-public crate-iterm2img-0.1 (crate (name "iterm2img") (vers "0.1.0") (deps (list (crate-dep (name "base64") (req "^0.21.0") (default-features #t) (kind 0)))) (hash "1iy1dc1df4bx0s9harb9h15s9yhzxkzag76iwk5kyi05m22rzfar")))

(define-public crate-iterm2mintty-0.1 (crate (name "iterm2mintty") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^2.29.2") (default-features #t) (kind 0)) (crate-dep (name "plist") (req "^0.2.4") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_ini") (req "^0.1.3") (default-features #t) (kind 0)))) (hash "0b3zal09aqzy7b7i5pf8wz1dafpy6fy6mj7k3k9rgy7xqhysqzli")))

(define-public crate-iterm2mintty-0.1 (crate (name "iterm2mintty") (vers "0.1.1") (deps (list (crate-dep (name "clap") (req "^2.29.2") (default-features #t) (kind 0)) (crate-dep (name "plist") (req "^0.2.4") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_ini") (req "^0.1.3") (default-features #t) (kind 0)))) (hash "06h0x67vsb5hbhs2d9xh7a7v1bn0gznvhazli523zbz8hw8bj2fi")))

(define-public crate-iterm2mintty-0.1 (crate (name "iterm2mintty") (vers "0.1.2") (deps (list (crate-dep (name "clap") (req "^2.29.2") (default-features #t) (kind 0)) (crate-dep (name "plist") (req "^0.2.4") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_ini") (req "^0.1.3") (default-features #t) (kind 0)) (crate-dep (name "serde_yaml") (req "^0.8.8") (default-features #t) (kind 0)))) (hash "19byn9s470jag03r6q1vwlhgf7j1k6k6n96n7n8ycyh1k2dbyp73")))

(define-public crate-iterm2mintty-0.2 (crate (name "iterm2mintty") (vers "0.2.0") (deps (list (crate-dep (name "clap") (req "^2.29.2") (default-features #t) (kind 0)) (crate-dep (name "plist") (req "^0.2.4") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_ini") (req "^0.1.3") (default-features #t) (kind 0)) (crate-dep (name "serde_yaml") (req "^0.8.8") (default-features #t) (kind 0)))) (hash "1qlfr6xngi64gr1132nkv4mpzc2wpdydpq2m0cdhr5sal8zxxxc1")))

(define-public crate-itermacros-0.1 (crate (name "itermacros") (vers "0.1.0") (hash "1mc9yh4fxgcl1c4wz63jm7ay1wabr69qan4a3v672m9v47dhgqw8")))

(define-public crate-itermap-0.1 (crate (name "itermap") (vers "0.1.0") (deps (list (crate-dep (name "maplit") (req "^1") (default-features #t) (kind 2)))) (hash "0acvrghz7zj1954lqlnls43jp6y06nv2s5lbdwccw66jwysc6p3r")))

(define-public crate-itermap-0.2 (crate (name "itermap") (vers "0.2.0") (deps (list (crate-dep (name "maplit") (req "^1") (default-features #t) (kind 2)))) (hash "05ak951440rzg09x3cha2a5dis87p5znzw12vpd7m6wamv48rbw0")))

(define-public crate-itermap-0.2 (crate (name "itermap") (vers "0.2.1") (deps (list (crate-dep (name "maplit") (req "^1") (default-features #t) (kind 2)))) (hash "0zfa9q7flw8i8xdj2j2ghjdrpf6xa9dks3ralbyph4dhq40b9wwk")))

(define-public crate-itermap-0.2 (crate (name "itermap") (vers "0.2.2") (deps (list (crate-dep (name "maplit") (req "^1") (default-features #t) (kind 2)))) (hash "16p9p8cvvbc28p2r0mmhiafz3aij2yv3py2q4v9hy2swrpg3ykqn")))

(define-public crate-itermap-0.3 (crate (name "itermap") (vers "0.3.0") (deps (list (crate-dep (name "pretty_assertions") (req "^1") (default-features #t) (kind 2)))) (hash "0zvl8pj827hp0lgyidni6xv1i9d2984i6bh6x81rmmn8lw7k0l8r") (yanked #t)))

(define-public crate-itermap-0.4 (crate (name "itermap") (vers "0.4.0") (deps (list (crate-dep (name "pretty_assertions") (req "^1") (default-features #t) (kind 2)))) (hash "0g2ir3v1ch7m15d5ahn5lf20xxc4bs0q4s4h8h09923b19r1kmwk")))

(define-public crate-itermore-0.1 (crate (name "itermore") (vers "0.1.0") (hash "1md8jkxkqmqky2v9y0kpz25cha939z4j41n5kvb06wdldbcl8gp2")))

(define-public crate-itermore-0.2 (crate (name "itermore") (vers "0.2.0") (hash "08yi0hz234zlqkx97dyaby7z5bhgir8azs4w5rag1k8wpgidn30b")))

(define-public crate-itermore-0.3 (crate (name "itermore") (vers "0.3.0") (deps (list (crate-dep (name "iterchunks") (req "^0.1.1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "iterwindows") (req "^0.1.1") (optional #t) (default-features #t) (kind 0)))) (hash "07lgx98q9gclycjizfvf4pynpxgj03l674gvq2s307q6wh72s7wh") (features (quote (("std") ("default" "std" "chunks" "windows")))) (v 2) (features2 (quote (("windows" "dep:iterwindows") ("chunks" "dep:iterchunks")))) (rust-version "1.56")))

(define-public crate-itermore-0.3 (crate (name "itermore") (vers "0.3.1") (deps (list (crate-dep (name "iterchunks") (req "^0.1.2") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "iterwindows") (req "^0.1.2") (optional #t) (default-features #t) (kind 0)))) (hash "13w2mfc767zb0wkaz9lqd994ww0bvz16gpni58va3afgmhw5061n") (features (quote (("std" "alloc") ("sorted") ("default" "std" "chunks" "sorted" "windows") ("alloc")))) (v 2) (features2 (quote (("windows" "dep:iterwindows") ("chunks" "dep:iterchunks")))) (rust-version "1.56")))

(define-public crate-itermore-0.4 (crate (name "itermore") (vers "0.4.0") (deps (list (crate-dep (name "iterchunks") (req "^0.2.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "iterwindows") (req "^0.2.0") (optional #t) (default-features #t) (kind 0)))) (hash "1qr6akcawz3nm55xmr31isc287a0accghjf110qfzpakjjqrsrhq") (features (quote (("std" "alloc") ("sorted") ("default" "std" "chunks" "sorted" "windows") ("alloc")))) (v 2) (features2 (quote (("windows" "dep:iterwindows") ("chunks" "dep:iterchunks")))) (rust-version "1.56")))

(define-public crate-itermore-0.5 (crate (name "itermore") (vers "0.5.0") (deps (list (crate-dep (name "arrays") (req "^0.1.1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "iterchunks") (req "^0.3.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "iterwindows") (req "^0.3.0") (optional #t) (default-features #t) (kind 0)))) (hash "1nml9g9fazqrvi1yjdzd06vgdcx1fgrd9cykz766yicyc0qf4c83") (features (quote (("std" "alloc") ("sorted" "alloc") ("default" "std" "array_chunks" "array_combinations" "array_windows" "sorted") ("alloc")))) (v 2) (features2 (quote (("array_windows" "dep:iterwindows") ("array_combinations" "alloc" "dep:arrays") ("array_chunks" "dep:iterchunks")))) (rust-version "1.56")))

(define-public crate-itermore-0.6 (crate (name "itermore") (vers "0.6.0") (deps (list (crate-dep (name "arrays") (req "^0.1.1") (optional #t) (default-features #t) (kind 0)))) (hash "02am6328520x6r8x5kq735w7d5a5msk9p22vv5fwpyph04ndanby") (features (quote (("std" "alloc") ("sorted" "alloc") ("min_max") ("default" "std" "array_chunks" "array_combinations" "array_windows" "cartesian_product" "circular_array_windows" "combinations" "min_max" "sorted") ("combinations" "alloc") ("circular_array_windows" "array_windows") ("cartesian_product") ("alloc")))) (v 2) (features2 (quote (("array_windows" "dep:arrays") ("array_combinations" "alloc" "dep:arrays") ("array_chunks" "dep:arrays")))) (rust-version "1.56")))

(define-public crate-itermore-0.7 (crate (name "itermore") (vers "0.7.0") (deps (list (crate-dep (name "arrays") (req "^0.2.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8") (default-features #t) (kind 2)))) (hash "142s3mn4xdyhfl7cbr1w3xkmypq2cl0xjckfz7ivi60ah09xi4w9") (features (quote (("std" "alloc") ("sorted" "alloc") ("min_max") ("full" "std" "array_chunks" "array_combinations" "array_combinations_with_reps" "array_windows" "cartesian_product" "circular_array_windows" "combinations" "combinations_with_reps" "min_max" "next_chunk" "sorted") ("default") ("combinations_with_reps" "alloc") ("combinations" "alloc") ("circular_array_windows" "array_windows") ("cartesian_product") ("alloc")))) (v 2) (features2 (quote (("next_chunk" "dep:arrays") ("array_windows" "dep:arrays") ("array_combinations_with_reps" "alloc" "dep:arrays") ("array_combinations" "alloc" "dep:arrays") ("array_chunks" "dep:arrays")))) (rust-version "1.56")))

(define-public crate-itermore-0.7 (crate (name "itermore") (vers "0.7.1") (deps (list (crate-dep (name "arrays") (req "^0.2.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 2)))) (hash "19avp2r4wk0lbsfp019zxb8423xcv21ds7zyjcv8v0d30gwdml7y") (features (quote (("std" "alloc") ("sorted" "alloc") ("min_max") ("full" "std" "array_chunks" "array_combinations_with_reps" "array_combinations" "array_windows" "cartesian_product" "circular_array_windows" "collect_array" "combinations_with_reps" "combinations" "min_max" "next_chunk" "sorted") ("default") ("combinations_with_reps" "alloc") ("combinations" "alloc") ("circular_array_windows" "array_windows") ("cartesian_product") ("alloc")))) (v 2) (features2 (quote (("next_chunk" "dep:arrays") ("collect_array" "dep:arrays") ("array_windows" "dep:arrays") ("array_combinations_with_reps" "alloc" "dep:arrays") ("array_combinations" "alloc" "dep:arrays") ("array_chunks" "dep:arrays")))) (rust-version "1.60")))

(define-public crate-itern-0.1 (crate (name "itern") (vers "0.1.0") (hash "0221mfbh98zv5gghjr9v9nij30c96phfmlq962fm29iandisbkv3")))

(define-public crate-iterpipes-0.1 (crate (name "iterpipes") (vers "0.1.0") (deps (list (crate-dep (name "time") (req "^0.2.9") (default-features #t) (kind 2)))) (hash "1j19nrdbmirm987412w47c56qd3bs0xppi3mc7dy5v44lhscwvrz")))

(define-public crate-iterpipes-0.1 (crate (name "iterpipes") (vers "0.1.1") (deps (list (crate-dep (name "time") (req "^0.2.9") (default-features #t) (kind 2)))) (hash "0sghi61kmllbmmxf5kbgl7rk6sq4bjl9wjp4w5wa6d3aq58n8ylv")))

(define-public crate-iterpipes-0.1 (crate (name "iterpipes") (vers "0.1.2") (deps (list (crate-dep (name "time") (req "^0.2.9") (default-features #t) (kind 2)))) (hash "0sa4r8lbwslhpp5j436i5dp30sglmy5ma2nif1wac2y7i385mklv")))

(define-public crate-iterpipes-0.1 (crate (name "iterpipes") (vers "0.1.3") (deps (list (crate-dep (name "time") (req "^0.2.9") (default-features #t) (kind 2)))) (hash "12h4f6dwj1gqgj0nl40g8hk5wdmg8pq4wljb75gglah9mfp1smbm")))

(define-public crate-iterpipes-0.1 (crate (name "iterpipes") (vers "0.1.4") (deps (list (crate-dep (name "time") (req "^0.2.9") (default-features #t) (kind 2)))) (hash "0s9qg93mqggs2k1r3q8pyqjfqryq0gya37rgjzvhj6qb0z82yy1b")))

(define-public crate-iterpipes-0.2 (crate (name "iterpipes") (vers "0.2.0") (deps (list (crate-dep (name "time") (req "^0.2.9") (default-features #t) (kind 2)))) (hash "02fjyr4jbmg3vgw2slrjwsl1mg61kfd1rqg0i9nmls3685ywb0jh")))

(define-public crate-iterr-0.2 (crate (name "iterr") (vers "0.2.0") (hash "1j34z6npx3nk4jpq53if4mhypdxzkm9jfyjwck16hyd2xg6l9gzi")))

(define-public crate-iterslide-0.0.1 (crate (name "iterslide") (vers "0.0.1") (hash "02xd99vh98k4p6bdn1dkgh2b70cfslxvwk2ysq5c4ixyj9vji5aj")))

(define-public crate-iterslide-0.0.2 (crate (name "iterslide") (vers "0.0.2") (hash "03jn7r5n1246v8lwqd6qpih6dcjacw6prkl3062rsw3r9w579183")))

(define-public crate-iterslide-0.0.3 (crate (name "iterslide") (vers "0.0.3") (hash "09lvwh3c5dnpljw236fwih37yfwks2vr6cibp8s927v66y18d2i4")))

(define-public crate-iterslide-0.0.4 (crate (name "iterslide") (vers "0.0.4") (hash "1pak8fw8d1f5b2zdfdyyfsw96xqw9gyhnivw2hhvqdq3sggmyzq2")))

(define-public crate-iterslide-0.0.5 (crate (name "iterslide") (vers "0.0.5") (hash "1i467kzn6qg9wfy086vpzqp8mc418dbpfmzmklx0ndd5302m42gz")))

(define-public crate-iterslide-1 (crate (name "iterslide") (vers "1.0.0") (hash "0q07aa94aiiblvbyzjcknhji8chvvzzaj6ddw7fl5sl0yi9gl816")))

(define-public crate-iterslide-1 (crate (name "iterslide") (vers "1.0.1") (hash "1qb2frl4yikd3nivqnfwcwid1812jw133qnpw2ryr0i5mxyip0dv")))

(define-public crate-iterstream-0.1 (crate (name "iterstream") (vers "0.1.0") (deps (list (crate-dep (name "futures") (req "^0.3.6") (features (quote ("thread-pool"))) (default-features #t) (kind 0)))) (hash "1mvk8vzaxi5ii16216fzl1iwwdqn6s8j0pcszgws6ys36f5fx14d")))

(define-public crate-iterstream-0.1 (crate (name "iterstream") (vers "0.1.1") (deps (list (crate-dep (name "futures") (req "^0.3.6") (features (quote ("thread-pool"))) (default-features #t) (kind 0)))) (hash "1jqwkb37a71bl5j64lanniymgl3pyzijnk33kc142h6cxd14jjif")))

(define-public crate-iterstream-0.1 (crate (name "iterstream") (vers "0.1.2") (deps (list (crate-dep (name "futures") (req "^0") (features (quote ("thread-pool"))) (default-features #t) (kind 0)))) (hash "0d0pj629xwyhvalmpkylqa4j6bj3qaijics3sv7zn1y9s12di9fl")))

(define-public crate-itertools-0.0.2 (crate (name "itertools") (vers "0.0.2") (hash "0mfpyc1kq7bzjhnb4540cdkpfs5vs5wh9p6xmc7m7nz9ha26paca")))

(define-public crate-itertools-0.0.3 (crate (name "itertools") (vers "0.0.3") (hash "0mqjp9lc2ncrf7wljjwqvfsk6qrpyb2zykcl5kk6bjir2fr99h8f")))

(define-public crate-itertools-0.0.4 (crate (name "itertools") (vers "0.0.4") (hash "1977y6ciy5b7b5d06v0zx16s2376qhir1qwwykgzfdzndkpgsp9s")))

(define-public crate-itertools-0.0.5 (crate (name "itertools") (vers "0.0.5") (hash "1x07w2j95vxgrwdgq9m11cdhlj6p4rnkn4xlq4hf8nx3zsdjbfkb")))

(define-public crate-itertools-0.0.6 (crate (name "itertools") (vers "0.0.6") (hash "0wk6mzmm05ijg5a95qc8y99fdpp1336yc19rhibix81xkr1x0mxv")))

(define-public crate-itertools-0.0.7 (crate (name "itertools") (vers "0.0.7") (hash "118mijgi7871km2a5049h5m9lninl3lmg7224n076y2vlqyfwjv1")))

(define-public crate-itertools-0.0.8 (crate (name "itertools") (vers "0.0.8") (hash "0n3bfywhx1bvz280bwa1l85fjjxia1mzqwd6q4nsj917qbkbngy8")))

(define-public crate-itertools-0.0.9 (crate (name "itertools") (vers "0.0.9") (hash "0nfsqb4lkjlnzpidwri6689lglbmp71wbfcaw9k5im4bpq9zqzl2")))

(define-public crate-itertools-0.0.10 (crate (name "itertools") (vers "0.0.10") (hash "0n6397fwpwgaf45kin5ssgj85p345y8005b7dlkvdc9ph4cgb7mh")))

(define-public crate-itertools-0.0.11 (crate (name "itertools") (vers "0.0.11") (hash "1zj1ibgzgs41klhypqlmiqlqja11mqpmiw1lvj35rdqmxdxljrds")))

(define-public crate-itertools-0.0.12 (crate (name "itertools") (vers "0.0.12") (hash "1xzdysixinnv8lg2yc01cczlsid3lm4gd2iprhwhrdqm8kpj0pdk")))

(define-public crate-itertools-0.0.13 (crate (name "itertools") (vers "0.0.13") (hash "0vzw5i9b71rymk4qlarxpxg11w4b8bbj89awh4k9aiay6cl93897")))

(define-public crate-itertools-0.0.14 (crate (name "itertools") (vers "0.0.14") (hash "0briwilr30gpp0d1iam8w99p7rx6hm7pxbch5a6n633cnm985029")))

(define-public crate-itertools-0.0.16 (crate (name "itertools") (vers "0.0.16") (hash "1fw9b8plpscdfax4i34vc3q1k89d88g2da0gqk4ckx74rr5rr79b")))

(define-public crate-itertools-0.0.17 (crate (name "itertools") (vers "0.0.17") (hash "0pv2a82yzspqvjnc429b0kyyy33m1ddjb56r0xirhl765lmrmyg8")))

(define-public crate-itertools-0.0.18 (crate (name "itertools") (vers "0.0.18") (hash "1pjsabkcvfwaisixb7bg03hwfhszn0jrrp670g2q0135a675cijx")))

(define-public crate-itertools-0.0.19 (crate (name "itertools") (vers "0.0.19") (hash "1g9gpz3klps6iajjapspjpr8dy798acm69m9y8i0x75j7xh4njxb")))

(define-public crate-itertools-0.0.20 (crate (name "itertools") (vers "0.0.20") (hash "0l2swq0vspns0igyakfmgw7jlpb8l34fyhxa9hwnl5k0fxrmz2dz")))

(define-public crate-itertools-0.1 (crate (name "itertools") (vers "0.1.0") (hash "044xz3xvr71njzv0rxp3i89vjphwg8bjj72wczkqpcqn4dj4jr1x")))

(define-public crate-itertools-0.1 (crate (name "itertools") (vers "0.1.1") (hash "15qnc6zqxd32gfmvlzlikzcwgwblrh0n2x6k3lw4vy5v107rgnf7")))

(define-public crate-itertools-0.1 (crate (name "itertools") (vers "0.1.2") (hash "1a8fxgl9csh6w83isx430j80variphah6ydv4m2m2z0jk25i1kgp")))

(define-public crate-itertools-0.1 (crate (name "itertools") (vers "0.1.3") (hash "1yj9j1gcm97v5wad52qwfk7nwh8wypv3rjlazrj9jd0cfn0vvy42")))

(define-public crate-itertools-0.1 (crate (name "itertools") (vers "0.1.4") (hash "0zbnx0jcazlapp9h16lrs8d3kcwz6jwq8nv28kr3ncqcbwg95yd7")))

(define-public crate-itertools-0.1 (crate (name "itertools") (vers "0.1.5") (hash "08bz5mv4618kprjdn6bkwqylxjqpqdblvy46pjg2fkswwjwdvvrp")))

(define-public crate-itertools-0.1 (crate (name "itertools") (vers "0.1.6") (hash "1n5xfla6370d9bdk7cx3qpmfy439d23f1bsd532cs0rpgj29nn6x")))

(define-public crate-itertools-0.1 (crate (name "itertools") (vers "0.1.7") (hash "0rwy946gd4lpkjy187kxvx8ywdrdlldqhjijk6yhlh3vrj0g66gr")))

(define-public crate-itertools-0.1 (crate (name "itertools") (vers "0.1.8") (hash "03gmmy5zj9g6g64f0g7ss6m7cb404m0z82w3j31qdsx29makvwsw")))

(define-public crate-itertools-0.1 (crate (name "itertools") (vers "0.1.9") (hash "1zgjm8sinwb818gna3v7d656ndzpwqimx1yz6y0cb4ddcz5l88wv")))

(define-public crate-itertools-0.1 (crate (name "itertools") (vers "0.1.10") (hash "0xxkz3lgn1dv8zxxxhxssxf3pwvin0zrgbkkfhfic2pa3px6vpiv")))

(define-public crate-itertools-0.2 (crate (name "itertools") (vers "0.2.0") (hash "1sbq7h2bs17fgmssb1295zmklj33kkr3g2pj58margwwv5nascji") (features (quote (("unstable"))))))

(define-public crate-itertools-0.2 (crate (name "itertools") (vers "0.2.1") (hash "0z1i5krqc1bqbmfxhg94hkfg1phdni8fa9c5g992w7yndi19z9fw") (features (quote (("unstable"))))))

(define-public crate-itertools-0.2 (crate (name "itertools") (vers "0.2.2") (hash "1568gg4s5sr2s5ny85h5gn5zb9r4i10bpqsrjawai4pnvvlh7ckg") (features (quote (("unstable"))))))

(define-public crate-itertools-0.2 (crate (name "itertools") (vers "0.2.3") (hash "0c7kzlvnvb13995sgvbx7wsaplf85nv8ccqz96kvh20hfnj2nx8n") (features (quote (("unstable"))))))

(define-public crate-itertools-0.2 (crate (name "itertools") (vers "0.2.4") (hash "18blzml1fls69c3jagwk27vzmbnlmcx3gasyny8fmflin0gx3yxf") (features (quote (("unstable"))))))

(define-public crate-itertools-0.2 (crate (name "itertools") (vers "0.2.5") (hash "19xgh12q54rgkzzdi0glbd6kancwf2la7qznallj1g6idy7snbn3") (features (quote (("unstable"))))))

(define-public crate-itertools-0.2 (crate (name "itertools") (vers "0.2.6") (hash "1k0fx9hk1wbzksdwyvz0xxmy936592f5yrb30c76qc0yy41a1w59") (features (quote (("unstable"))))))

(define-public crate-itertools-0.2 (crate (name "itertools") (vers "0.2.7") (hash "0ry75zg4a7ys3nz6aqdy8hcl7nhzrp1hnxmp225ah9bzvvsjd707") (features (quote (("unstable"))))))

(define-public crate-itertools-0.2 (crate (name "itertools") (vers "0.2.8") (hash "1hcjqyc838lj9pbrz5dwsxy4whz2sf5ln0xsx231vf9ixk56myjy") (features (quote (("unstable"))))))

(define-public crate-itertools-0.2 (crate (name "itertools") (vers "0.2.9") (hash "030b5s37hkylw7yc1ri7hgn09095yz6zdk4pxddy6p9dmjk5z7xa") (features (quote (("unstable"))))))

(define-public crate-itertools-0.3 (crate (name "itertools") (vers "0.3.0") (hash "1hrvz6scf5npv9wxapds2zg195dh76agnlbsadbhycmj7zg2sfss") (features (quote (("unstable"))))))

(define-public crate-itertools-0.3 (crate (name "itertools") (vers "0.3.1") (deps (list (crate-dep (name "quickcheck") (req "*") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "quickcheck_macros") (req "*") (optional #t) (default-features #t) (kind 0)))) (hash "1mk92h6mx8v5m92yb9lbb3dy22rvlx7bfsl14ixs2c313m9b2prb") (features (quote (("unstable") ("qc" "quickcheck" "quickcheck_macros"))))))

(define-public crate-itertools-0.3 (crate (name "itertools") (vers "0.3.2") (deps (list (crate-dep (name "quickcheck") (req "*") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "quickcheck_macros") (req "*") (optional #t) (default-features #t) (kind 0)))) (hash "0ybxgr6yndip67j9ds9b0648laz4h4z11s196calp0mdn3kjg4kw") (features (quote (("unstable") ("qc" "quickcheck" "quickcheck_macros"))))))

(define-public crate-itertools-0.3 (crate (name "itertools") (vers "0.3.3") (deps (list (crate-dep (name "quickcheck") (req "*") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "quickcheck_macros") (req "*") (optional #t) (default-features #t) (kind 0)))) (hash "0d37kpl89yiacl3yn11vz4p3npqyvi2d344ri380pb8wbh9x5748") (features (quote (("unstable") ("qc" "quickcheck" "quickcheck_macros"))))))

(define-public crate-itertools-0.3 (crate (name "itertools") (vers "0.3.4") (deps (list (crate-dep (name "quickcheck") (req "*") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "quickcheck_macros") (req "*") (optional #t) (default-features #t) (kind 0)))) (hash "1a6g9s10p9ynbrvwp24fzb6nhayxna5c1ymyn7l6vnazklb7kgh8") (features (quote (("unstable") ("qc" "quickcheck" "quickcheck_macros"))))))

(define-public crate-itertools-0.3 (crate (name "itertools") (vers "0.3.5") (deps (list (crate-dep (name "quickcheck") (req "*") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "quickcheck_macros") (req "*") (optional #t) (default-features #t) (kind 0)))) (hash "0v362xm0hxpy75qfalxwhlc9h86ya3lg0yrm9pj36g0jyppr4b8l") (features (quote (("unstable") ("qc" "quickcheck" "quickcheck_macros"))))))

(define-public crate-itertools-0.3 (crate (name "itertools") (vers "0.3.6") (deps (list (crate-dep (name "quickcheck") (req "*") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "quickcheck_macros") (req "*") (optional #t) (default-features #t) (kind 0)))) (hash "09kngnb559mh5jz7x7w0k7s9sy5ffvpyi95hfy27f8rgin5wg8j4") (features (quote (("unstable") ("qc" "quickcheck" "quickcheck_macros"))))))

(define-public crate-itertools-0.3 (crate (name "itertools") (vers "0.3.7") (deps (list (crate-dep (name "quickcheck") (req "*") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "quickcheck_macros") (req "*") (optional #t) (default-features #t) (kind 0)))) (hash "0pkawjyzrsa1311xkpdn89hv2cx9r7vg76qjyd5m6g8n7xd3p2z0") (features (quote (("unstable") ("qc" "quickcheck" "quickcheck_macros"))))))

(define-public crate-itertools-0.3 (crate (name "itertools") (vers "0.3.8") (deps (list (crate-dep (name "quickcheck") (req "*") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "quickcheck_macros") (req "*") (optional #t) (default-features #t) (kind 0)))) (hash "0jhxjc90v9rmp8k963qn2y9j7ljm5qjn8msi9h1skz2y360rzpkz") (features (quote (("unstable") ("qc" "quickcheck" "quickcheck_macros"))))))

(define-public crate-itertools-0.3 (crate (name "itertools") (vers "0.3.9") (deps (list (crate-dep (name "quickcheck") (req "*") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "quickcheck_macros") (req "*") (optional #t) (default-features #t) (kind 0)))) (hash "1k5pr9iyx3ihi36473vfvm2nivv5pzmiz22ymccf9yh30kgsw9av") (features (quote (("unstable") ("qc" "quickcheck" "quickcheck_macros"))))))

(define-public crate-itertools-0.3 (crate (name "itertools") (vers "0.3.10") (deps (list (crate-dep (name "quickcheck") (req "*") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "quickcheck_macros") (req "*") (optional #t) (default-features #t) (kind 0)))) (hash "1gas6z9wmfsvp0x9fk2dfrya55l996v2qnq09ni4rm18xz9j5bai") (features (quote (("unstable") ("qc" "quickcheck" "quickcheck_macros"))))))

(define-public crate-itertools-0.3 (crate (name "itertools") (vers "0.3.11") (deps (list (crate-dep (name "quickcheck") (req "*") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "quickcheck_macros") (req "*") (optional #t) (default-features #t) (kind 0)))) (hash "0rxq1x3xzd8lb02wp0qybl5zwh21d61z2lim7q6gpm08m3vv6yrl") (features (quote (("unstable") ("qc" "quickcheck" "quickcheck_macros"))))))

(define-public crate-itertools-0.3 (crate (name "itertools") (vers "0.3.12") (deps (list (crate-dep (name "quickcheck") (req "*") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "quickcheck_macros") (req "*") (optional #t) (default-features #t) (kind 0)))) (hash "0xmmzslz6s92j5x363ycriy1ahv4z203rcxv9n8dc6xr1srx7v2y") (features (quote (("unstable") ("qc" "quickcheck" "quickcheck_macros"))))))

(define-public crate-itertools-0.3 (crate (name "itertools") (vers "0.3.13") (deps (list (crate-dep (name "quickcheck") (req "*") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "quickcheck_macros") (req "*") (optional #t) (default-features #t) (kind 0)))) (hash "1lvzjn6dzz0h4rw7a53c5zzdck469wkppgz95wzwj060aim3vlq5") (features (quote (("unstable") ("qc" "quickcheck" "quickcheck_macros"))))))

(define-public crate-itertools-0.3 (crate (name "itertools") (vers "0.3.14") (deps (list (crate-dep (name "quickcheck") (req "*") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "quickcheck_macros") (req "*") (optional #t) (default-features #t) (kind 0)))) (hash "1md5bm5akxyzfxzsj6pchfadianc296inzfgc0spsdgddzf20li6") (features (quote (("unstable") ("qc" "quickcheck" "quickcheck_macros"))))))

(define-public crate-itertools-0.3 (crate (name "itertools") (vers "0.3.15") (deps (list (crate-dep (name "quickcheck") (req "*") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "quickcheck_macros") (req "*") (optional #t) (default-features #t) (kind 0)))) (hash "0prhflwr3xc252ssjsay7lac3dsvp9xr2ix5kdn90x568cpbir3s") (features (quote (("unstable") ("qc" "quickcheck" "quickcheck_macros"))))))

(define-public crate-itertools-0.3 (crate (name "itertools") (vers "0.3.16") (deps (list (crate-dep (name "quickcheck") (req "*") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "quickcheck_macros") (req "*") (optional #t) (default-features #t) (kind 0)))) (hash "08x9324cbppybb4nyg77xgmx2nz96z4d7qzhz506gzylaymap1gj") (features (quote (("unstable") ("qc" "quickcheck" "quickcheck_macros"))))))

(define-public crate-itertools-0.3 (crate (name "itertools") (vers "0.3.17") (deps (list (crate-dep (name "quickcheck") (req "*") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "quickcheck_macros") (req "*") (optional #t) (default-features #t) (kind 0)))) (hash "0rxb3wc9xh4p53nbbh5j0wz8m2w8nqp1k9dhn05rq1p92kvmmrra") (features (quote (("unstable") ("qc" "quickcheck" "quickcheck_macros"))))))

(define-public crate-itertools-0.3 (crate (name "itertools") (vers "0.3.18") (deps (list (crate-dep (name "quickcheck") (req "*") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "quickcheck_macros") (req "*") (optional #t) (default-features #t) (kind 0)))) (hash "19c36n5z0hcmw5kz6aj0yizz8y2g4msz4b6sj49zc7xbmgxyl14i") (features (quote (("unstable") ("qc" "quickcheck" "quickcheck_macros"))))))

(define-public crate-itertools-0.3 (crate (name "itertools") (vers "0.3.19") (deps (list (crate-dep (name "quickcheck") (req "*") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "quickcheck_macros") (req "*") (optional #t) (default-features #t) (kind 0)))) (hash "0p7vxli0kmrj7az5rdqhrj8nx4jaw1pqbx78z6cn0l5mn05dx52s") (features (quote (("unstable") ("qc" "quickcheck" "quickcheck_macros"))))))

(define-public crate-itertools-0.3 (crate (name "itertools") (vers "0.3.20") (deps (list (crate-dep (name "permutohedron") (req "^0.1") (default-features #t) (kind 2)) (crate-dep (name "quickcheck") (req "*") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "quickcheck_macros") (req "*") (optional #t) (default-features #t) (kind 0)))) (hash "1rvsr89vk2x8j1693ynijrpr58w2d66azzn3hixnh94jz7gpfmd1") (features (quote (("unstable") ("qc" "quickcheck" "quickcheck_macros"))))))

(define-public crate-itertools-0.3 (crate (name "itertools") (vers "0.3.21") (deps (list (crate-dep (name "permutohedron") (req "^0.1") (default-features #t) (kind 2)) (crate-dep (name "quickcheck") (req "*") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "quickcheck_macros") (req "*") (optional #t) (default-features #t) (kind 0)))) (hash "10cn6076mfapbd4y0wylczjkii78lfdz3h3si5wrm0n0d23hwi2f") (features (quote (("unstable") ("qc" "quickcheck" "quickcheck_macros"))))))

(define-public crate-itertools-0.3 (crate (name "itertools") (vers "0.3.22") (deps (list (crate-dep (name "permutohedron") (req "^0.1") (default-features #t) (kind 2)) (crate-dep (name "quickcheck") (req "*") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "quickcheck_macros") (req "*") (optional #t) (default-features #t) (kind 0)))) (hash "0qvgvp2yc88xdaw828s7cblx6gh0h2nv3fx5684djhqsxqsjsib2") (features (quote (("unstable") ("qc" "quickcheck" "quickcheck_macros"))))))

(define-public crate-itertools-0.3 (crate (name "itertools") (vers "0.3.23") (deps (list (crate-dep (name "permutohedron") (req "^0.1") (default-features #t) (kind 2)) (crate-dep (name "quickcheck") (req "*") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "quickcheck_macros") (req "*") (optional #t) (default-features #t) (kind 0)))) (hash "1gl7blnqqrbji6jfcglqm4j7w81lp231qs0p21arpawbwlikv7pk") (features (quote (("unstable") ("qc" "quickcheck" "quickcheck_macros"))))))

(define-public crate-itertools-0.3 (crate (name "itertools") (vers "0.3.24") (deps (list (crate-dep (name "permutohedron") (req "^0.1") (default-features #t) (kind 2)) (crate-dep (name "quickcheck") (req "^0.2.21") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "quickcheck_macros") (req "^0.2.21") (optional #t) (default-features #t) (kind 0)))) (hash "14r3ndm9jl6v08y64m5xg2ri9wli3d8jhqxagf83ga8bzdqkqfsx") (features (quote (("unstable") ("qc" "quickcheck" "quickcheck_macros"))))))

(define-public crate-itertools-0.3 (crate (name "itertools") (vers "0.3.25") (deps (list (crate-dep (name "permutohedron") (req "^0.1") (default-features #t) (kind 2)) (crate-dep (name "quickcheck") (req "^0.2.21") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "quickcheck_macros") (req "^0.2.21") (optional #t) (default-features #t) (kind 0)))) (hash "036ss6ckcb2wsiihh35k8jcx130qws3yv62nszw8zzawd0f3zdqn") (features (quote (("unstable") ("qc" "quickcheck" "quickcheck_macros"))))))

(define-public crate-itertools-0.4 (crate (name "itertools") (vers "0.4.0") (deps (list (crate-dep (name "permutohedron") (req "^0.1") (default-features #t) (kind 2)) (crate-dep (name "quickcheck") (req "^0.2.21") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "quickcheck_macros") (req "^0.2.21") (optional #t) (default-features #t) (kind 0)))) (hash "16810zyjy57qsgrhy9x3w91q8zz7k59k9ghiwayqyrncy7jdyaqa") (features (quote (("unstable") ("qc" "quickcheck" "quickcheck_macros"))))))

(define-public crate-itertools-0.4 (crate (name "itertools") (vers "0.4.1") (deps (list (crate-dep (name "permutohedron") (req "^0.1") (default-features #t) (kind 2)) (crate-dep (name "quickcheck") (req "^0.2.21") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "quickcheck_macros") (req "^0.2.21") (optional #t) (default-features #t) (kind 0)))) (hash "0phlv5w6nfr6azqfql1cnvyypvhyicqxw6wp07872lxhl46smwrd") (features (quote (("unstable") ("qc" "quickcheck" "quickcheck_macros"))))))

(define-public crate-itertools-0.4 (crate (name "itertools") (vers "0.4.2") (deps (list (crate-dep (name "permutohedron") (req "^0.1") (default-features #t) (kind 2)) (crate-dep (name "quickcheck") (req "^0.2.21") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "quickcheck_macros") (req "^0.2.21") (optional #t) (default-features #t) (kind 0)))) (hash "0swbvh34cxnid21q311861zchba5pc58bagh3rnwmsw7fb26anya") (features (quote (("unstable") ("qc" "quickcheck" "quickcheck_macros"))))))

(define-public crate-itertools-0.4 (crate (name "itertools") (vers "0.4.3") (deps (list (crate-dep (name "permutohedron") (req "^0.1") (default-features #t) (kind 2)) (crate-dep (name "quickcheck") (req "^0.2.21") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "quickcheck_macros") (req "^0.2.21") (optional #t) (default-features #t) (kind 0)))) (hash "1wqs0iyc4i2a7snb0fahbim6dz8m1g1mlp5xpqhpxrwscr0pww10") (features (quote (("unstable") ("qc" "quickcheck" "quickcheck_macros"))))))

(define-public crate-itertools-0.4 (crate (name "itertools") (vers "0.4.4") (deps (list (crate-dep (name "permutohedron") (req "^0.1") (default-features #t) (kind 2)) (crate-dep (name "quickcheck") (req "^0.2.21") (optional #t) (default-features #t) (kind 0)))) (hash "13nw859k0v49n4nxlb386idxhbjs81vkldbxbh4nwyc0acfs4f67") (features (quote (("unstable") ("qc" "quickcheck"))))))

(define-public crate-itertools-0.4 (crate (name "itertools") (vers "0.4.5") (deps (list (crate-dep (name "permutohedron") (req "^0.1") (default-features #t) (kind 2)) (crate-dep (name "quickcheck") (req "^0.2.21") (optional #t) (default-features #t) (kind 0)))) (hash "0rlnvwdw4zn1l5bjsqb3ab76k0nd66yfhy5ara2gqpngwkl57x70") (features (quote (("unstable") ("qc" "quickcheck"))))))

(define-public crate-itertools-0.4 (crate (name "itertools") (vers "0.4.6") (deps (list (crate-dep (name "permutohedron") (req "^0.1") (default-features #t) (kind 2)) (crate-dep (name "quickcheck") (req "^0.2.21") (optional #t) (default-features #t) (kind 0)))) (hash "0znrwqgic5rp6f2b6w206l1ryl0czbi5h77v22453cka1k2a1jky") (features (quote (("unstable") ("qc" "quickcheck"))))))

(define-public crate-itertools-0.4 (crate (name "itertools") (vers "0.4.7") (deps (list (crate-dep (name "permutohedron") (req "^0.1") (default-features #t) (kind 2)) (crate-dep (name "quickcheck") (req "^0.2.21") (optional #t) (default-features #t) (kind 0)))) (hash "0id9wx9rjw592jwpb0z7qj00h8161sb3f1dym6qfbca3q3w88wd9") (features (quote (("unstable") ("qc" "quickcheck"))))))

(define-public crate-itertools-0.4 (crate (name "itertools") (vers "0.4.8") (deps (list (crate-dep (name "permutohedron") (req "^0.1") (default-features #t) (kind 2)) (crate-dep (name "quickcheck") (req "^0.2.21") (optional #t) (default-features #t) (kind 0)))) (hash "09cg6gx00j6ykcfcxvps828xn9m6zi0jwb71lwjhdc7qwm3asi8d") (features (quote (("unstable") ("qc" "quickcheck"))))))

(define-public crate-itertools-0.4 (crate (name "itertools") (vers "0.4.9") (deps (list (crate-dep (name "permutohedron") (req "^0.1") (default-features #t) (kind 2)) (crate-dep (name "quickcheck") (req "^0.2.21") (optional #t) (default-features #t) (kind 0)))) (hash "1xdnx2dy72pn6wwk93m4s81a363fnvi1pnm9d65hgxp4b0470fq5") (features (quote (("unstable"))))))

(define-public crate-itertools-0.4 (crate (name "itertools") (vers "0.4.10") (deps (list (crate-dep (name "permutohedron") (req "^0.1") (default-features #t) (kind 2)) (crate-dep (name "quickcheck") (req "^0.2.21") (optional #t) (default-features #t) (kind 0)))) (hash "1q49zg66pizcm14j9qmzz2pywwnx049sd02pfz3srq5vs4xw7v06") (features (quote (("unstable"))))))

(define-public crate-itertools-0.4 (crate (name "itertools") (vers "0.4.11") (deps (list (crate-dep (name "permutohedron") (req "^0.1") (default-features #t) (kind 2)) (crate-dep (name "quickcheck") (req "^0.2.21") (optional #t) (default-features #t) (kind 0)))) (hash "1r8cs5gj0c6ixnanw66im6zhxyxfwcfcimj688j5jnmpw438nlph") (features (quote (("unstable"))))))

(define-public crate-itertools-0.4 (crate (name "itertools") (vers "0.4.12") (deps (list (crate-dep (name "permutohedron") (req "^0.1") (default-features #t) (kind 2)) (crate-dep (name "quickcheck") (req "^0.2.21") (optional #t) (default-features #t) (kind 0)))) (hash "06521kzfsj59sllvr95szql46idh33c53c8gp0qfb4d7w4rdppfz") (features (quote (("unstable"))))))

(define-public crate-itertools-0.4 (crate (name "itertools") (vers "0.4.13") (deps (list (crate-dep (name "permutohedron") (req "^0.1") (default-features #t) (kind 2)) (crate-dep (name "quickcheck") (req "^0.2.21") (optional #t) (default-features #t) (kind 0)))) (hash "0xjd33fpqjkr5brpxj64spw9jm8iwg3j0fpgzlf0p128zsjiyvh8") (features (quote (("unstable"))))))

(define-public crate-itertools-0.4 (crate (name "itertools") (vers "0.4.14") (deps (list (crate-dep (name "permutohedron") (req "^0.1") (default-features #t) (kind 2)) (crate-dep (name "quickcheck") (req "^0.2.21") (optional #t) (default-features #t) (kind 0)))) (hash "0xm6d5lmv4acdwrjdpbv3gcbzpwl4yvibn8jb5ng8nv33200bhzj") (features (quote (("unstable"))))))

(define-public crate-itertools-0.4 (crate (name "itertools") (vers "0.4.15") (deps (list (crate-dep (name "permutohedron") (req "^0.1") (default-features #t) (kind 2)) (crate-dep (name "quickcheck") (req "^0.2.21") (optional #t) (default-features #t) (kind 0)))) (hash "0kq6smsnyrpw89593lrkhph96ws145yd6v0h0kdz95mxbvnizwl8") (features (quote (("unstable"))))))

(define-public crate-itertools-0.4 (crate (name "itertools") (vers "0.4.16") (deps (list (crate-dep (name "permutohedron") (req "^0.1") (default-features #t) (kind 2)) (crate-dep (name "quickcheck") (req "^0.2.21") (optional #t) (default-features #t) (kind 0)))) (hash "0gq37lavj22imj3nnh2zzvclaj3r078lnqag9k7yy46przkmcvmc") (features (quote (("unstable"))))))

(define-public crate-itertools-0.4 (crate (name "itertools") (vers "0.4.17") (deps (list (crate-dep (name "permutohedron") (req "^0.1") (default-features #t) (kind 2)) (crate-dep (name "quickcheck") (req "^0.2.21") (optional #t) (default-features #t) (kind 0)))) (hash "03v1dhlfqrrq1bc7qpq74bx4x2wlfy887rm4zc5863bkx7h87nvx") (features (quote (("unstable"))))))

(define-public crate-itertools-0.4 (crate (name "itertools") (vers "0.4.18") (deps (list (crate-dep (name "permutohedron") (req "^0.1") (default-features #t) (kind 2)) (crate-dep (name "quickcheck") (req "^0.2.21") (optional #t) (default-features #t) (kind 0)))) (hash "1hybgjw80fadyjhba48xz5r284rznf0vj7cmlvvnxm7pwyadc5bg") (features (quote (("unstable"))))))

(define-public crate-itertools-0.4 (crate (name "itertools") (vers "0.4.19") (deps (list (crate-dep (name "permutohedron") (req "^0.1") (default-features #t) (kind 2)) (crate-dep (name "quickcheck") (req "^0.2.21") (optional #t) (default-features #t) (kind 0)))) (hash "0gxwcmxyq7fmccdqclfzyg4wnb2b445g8n3fqqyz8n30nmpbbaf4") (features (quote (("unstable"))))))

(define-public crate-itertools-0.5 (crate (name "itertools") (vers "0.5.0-alpha.0") (deps (list (crate-dep (name "either") (req "^1.0") (kind 0)) (crate-dep (name "permutohedron") (req "^0.2") (default-features #t) (kind 2)) (crate-dep (name "quickcheck") (req "^0.2.21") (optional #t) (default-features #t) (kind 0)))) (hash "0q72lfh8pm0kb94crlawa895l4wpwranfy2ky8rqfd1rqn4xp7w2")))

(define-public crate-itertools-0.5 (crate (name "itertools") (vers "0.5.0-alpha.1") (deps (list (crate-dep (name "either") (req "^1.0") (kind 0)) (crate-dep (name "permutohedron") (req "^0.2") (default-features #t) (kind 2)) (crate-dep (name "quickcheck") (req "^0.2.21") (optional #t) (default-features #t) (kind 0)))) (hash "00w6jwmr0ary4002a3zi57wyqa5qj2ysd9pik1g1yxvqr3lsp3xn")))

(define-public crate-itertools-0.5 (crate (name "itertools") (vers "0.5.0") (deps (list (crate-dep (name "either") (req "^1.0") (kind 0)) (crate-dep (name "permutohedron") (req "^0.2") (default-features #t) (kind 2)) (crate-dep (name "quickcheck") (req "^0.2.21") (optional #t) (default-features #t) (kind 0)))) (hash "14vhj45azp8lfa4ymfrmcnl0a6van70lhnbjzy126x4cjgsyilzn")))

(define-public crate-itertools-0.5 (crate (name "itertools") (vers "0.5.1") (deps (list (crate-dep (name "either") (req "^1.0") (kind 0)) (crate-dep (name "permutohedron") (req "^0.2") (default-features #t) (kind 2)) (crate-dep (name "quickcheck") (req "^0.2.21") (optional #t) (default-features #t) (kind 0)))) (hash "1jl7iksdgk92ryksiknmj32kx8lq4z2vh3hqds6r8v7lkb14mph0")))

(define-public crate-itertools-0.5 (crate (name "itertools") (vers "0.5.2") (deps (list (crate-dep (name "either") (req "^1.0") (kind 0)) (crate-dep (name "permutohedron") (req "^0.2") (default-features #t) (kind 2)) (crate-dep (name "quickcheck") (req "^0.2.21") (optional #t) (default-features #t) (kind 0)))) (hash "1xdhshggv144z3mvdh7znlnr53wyvm0dkpvsbsprgd2m9jvsg1qn")))

(define-public crate-itertools-0.5 (crate (name "itertools") (vers "0.5.3") (deps (list (crate-dep (name "either") (req "^1.0") (kind 0)) (crate-dep (name "permutohedron") (req "^0.2") (default-features #t) (kind 2)) (crate-dep (name "quickcheck") (req "^0.2.21") (optional #t) (default-features #t) (kind 0)))) (hash "07x97rvcwapkadf9bsa8sbsc6zi2q6b2n38n7r0l8w0bji3snncz")))

(define-public crate-itertools-0.5 (crate (name "itertools") (vers "0.5.4") (deps (list (crate-dep (name "either") (req "^1.0") (kind 0)) (crate-dep (name "permutohedron") (req "^0.2") (default-features #t) (kind 2)) (crate-dep (name "quickcheck") (req "^0.2.21") (optional #t) (default-features #t) (kind 0)))) (hash "1lz62zwnxg630g1g7nxk36qhhxan7145sqapiiqgnfb8ngjbb8nx")))

(define-public crate-itertools-0.5 (crate (name "itertools") (vers "0.5.5") (deps (list (crate-dep (name "either") (req "^1.0") (kind 0)) (crate-dep (name "permutohedron") (req "^0.2") (default-features #t) (kind 2)) (crate-dep (name "quickcheck") (req "^0.2.21") (optional #t) (default-features #t) (kind 0)))) (hash "092lkxnxvwa7fp69f4dc6j5yi7njfxrairpbsg7hh64ybahv10gg")))

(define-public crate-itertools-0.5 (crate (name "itertools") (vers "0.5.6") (deps (list (crate-dep (name "either") (req "^1.0") (kind 0)) (crate-dep (name "permutohedron") (req "^0.2") (default-features #t) (kind 2)) (crate-dep (name "quickcheck") (req "^0.4") (kind 2)))) (hash "1541342408ga2p2dxalhcid5xx1cm5pvcm002vxlni6lc684g02f")))

(define-public crate-itertools-0.5 (crate (name "itertools") (vers "0.5.7") (deps (list (crate-dep (name "either") (req "^1.0") (kind 0)) (crate-dep (name "permutohedron") (req "^0.2") (default-features #t) (kind 2)) (crate-dep (name "quickcheck") (req "^0.4") (kind 2)))) (hash "0yh8592l85rwwix14bsapk727n06qwbjfhbm62nxd7xf01z6z3cf")))

(define-public crate-itertools-0.5 (crate (name "itertools") (vers "0.5.8") (deps (list (crate-dep (name "either") (req "^1.0") (kind 0)) (crate-dep (name "permutohedron") (req "^0.2") (default-features #t) (kind 2)) (crate-dep (name "quickcheck") (req "^0.4") (kind 2)))) (hash "083hib5kajnpn61fb4jrk0kfxi3wb270w1a9q2cbvg6vfaj6v566")))

(define-public crate-itertools-0.5 (crate (name "itertools") (vers "0.5.9") (deps (list (crate-dep (name "either") (req "^1.0") (kind 0)) (crate-dep (name "permutohedron") (req "^0.2") (default-features #t) (kind 2)) (crate-dep (name "quickcheck") (req "^0.4") (kind 2)))) (hash "02kkmxqa6mzm02jh2sbaj9dbgwakdklklfrc1xxkfwbbpbkmfmfr")))

(define-public crate-itertools-0.5 (crate (name "itertools") (vers "0.5.10") (deps (list (crate-dep (name "either") (req "^1.0") (kind 0)) (crate-dep (name "permutohedron") (req "^0.2") (default-features #t) (kind 2)) (crate-dep (name "quickcheck") (req "^0.4") (kind 2)))) (hash "1z4lyrakgynvhylya72qb3vizmxmd62whjmg4r8k01d4inbxccs8")))

(define-public crate-itertools-0.6 (crate (name "itertools") (vers "0.6.0") (deps (list (crate-dep (name "either") (req "^1.0") (kind 0)) (crate-dep (name "permutohedron") (req "^0.2") (default-features #t) (kind 2)) (crate-dep (name "quickcheck") (req "^0.4") (kind 2)))) (hash "16bvnjf24jr2qhjgsdlbmcki67w7ynfp5952b5yicikjm4l0jakp")))

(define-public crate-itertools-0.6 (crate (name "itertools") (vers "0.6.1") (deps (list (crate-dep (name "either") (req "^1.0") (kind 0)) (crate-dep (name "permutohedron") (req "^0.2") (default-features #t) (kind 2)) (crate-dep (name "quickcheck") (req "^0.4") (kind 2)))) (hash "1fq5hdisxj64n3yj3g1maz74jq3j1vng34lii1cpydr08x0mk0z5")))

(define-public crate-itertools-0.6 (crate (name "itertools") (vers "0.6.2") (deps (list (crate-dep (name "either") (req "^1.0") (kind 0)) (crate-dep (name "permutohedron") (req "^0.2") (default-features #t) (kind 2)) (crate-dep (name "quickcheck") (req "^0.4") (kind 2)))) (hash "180mwh16i9qd0vnzwrkrnmqgvg6yab58jhc98hr43krr07b8bhi2")))

(define-public crate-itertools-0.6 (crate (name "itertools") (vers "0.6.3") (deps (list (crate-dep (name "either") (req "^1.0") (kind 0)) (crate-dep (name "permutohedron") (req "^0.2") (default-features #t) (kind 2)) (crate-dep (name "quickcheck") (req "^0.4") (kind 2)))) (hash "1w62gv1a7js20yqqvbvpyabccfxji2aj66bhj5n2gvri7hknlkdb")))

(define-public crate-itertools-0.6 (crate (name "itertools") (vers "0.6.4") (deps (list (crate-dep (name "either") (req "^1.0") (kind 0)) (crate-dep (name "permutohedron") (req "^0.2") (default-features #t) (kind 2)) (crate-dep (name "quickcheck") (req "^0.4") (kind 2)))) (hash "0qmv9jf7p0wkyxxqgb2vnvgq376vrq18k4j8afd6r5zb06gfgkr6")))

(define-public crate-itertools-0.6 (crate (name "itertools") (vers "0.6.5") (deps (list (crate-dep (name "either") (req "^1.0") (kind 0)) (crate-dep (name "permutohedron") (req "^0.2") (default-features #t) (kind 2)) (crate-dep (name "quickcheck") (req "^0.4") (kind 2)))) (hash "08fx60wxmjakssqn3w9045dasqvaf1gmgzg5kag062k9l56vxwnk")))

(define-public crate-itertools-0.7 (crate (name "itertools") (vers "0.7.0") (deps (list (crate-dep (name "either") (req "^1.0") (kind 0)) (crate-dep (name "permutohedron") (req "^0.2") (default-features #t) (kind 2)) (crate-dep (name "quickcheck") (req "^0.4") (kind 2)))) (hash "1558zmj02z1h07y2s7qs0sqgbsxzjh3kaa8y7ai2wpzm1jw4gziy") (features (quote (("use_std") ("default" "use_std"))))))

(define-public crate-itertools-0.7 (crate (name "itertools") (vers "0.7.1") (deps (list (crate-dep (name "either") (req "^1.0") (kind 0)) (crate-dep (name "permutohedron") (req "^0.2") (default-features #t) (kind 2)) (crate-dep (name "quickcheck") (req "^0.4") (kind 2)))) (hash "0v3iwbz30h2ifz6qdcng6dkpg1yhwj9rkv44w32rq9prxhp37h3w") (features (quote (("use_std") ("default" "use_std"))))))

(define-public crate-itertools-0.7 (crate (name "itertools") (vers "0.7.2") (deps (list (crate-dep (name "either") (req "^1.0") (kind 0)) (crate-dep (name "permutohedron") (req "^0.2") (default-features #t) (kind 2)) (crate-dep (name "quickcheck") (req "^0.4") (kind 2)))) (hash "06kmcb7r490qs32avs45hn016cn4bqp0zs8fd9whbdfk7wfhalic") (features (quote (("use_std") ("default" "use_std"))))))

(define-public crate-itertools-0.7 (crate (name "itertools") (vers "0.7.3") (deps (list (crate-dep (name "either") (req "^1.0") (kind 0)) (crate-dep (name "permutohedron") (req "^0.2") (default-features #t) (kind 2)) (crate-dep (name "quickcheck") (req "^0.4") (kind 2)))) (hash "1pqg3h34yw0bq25kvzja4v3wcmabd3lfy486vgpag5gigifzgwwr") (features (quote (("use_std") ("default" "use_std"))))))

(define-public crate-itertools-0.7 (crate (name "itertools") (vers "0.7.4") (deps (list (crate-dep (name "either") (req "^1.0") (kind 0)) (crate-dep (name "permutohedron") (req "^0.2") (default-features #t) (kind 2)) (crate-dep (name "quickcheck") (req "^0.4") (kind 2)))) (hash "1igqdvcc9x32l3zqhspa4yyb706nlanmsv4klgpr4xs1pfws0yll") (features (quote (("use_std") ("default" "use_std"))))))

(define-public crate-itertools-0.7 (crate (name "itertools") (vers "0.7.5") (deps (list (crate-dep (name "either") (req "^1.0") (kind 0)) (crate-dep (name "permutohedron") (req "^0.2") (default-features #t) (kind 2)) (crate-dep (name "quickcheck") (req "^0.5") (kind 2)))) (hash "1hjr3n2lz5b0jacr5gbllng25syaxg1r8ximfdyp938bqv4rbwd3") (features (quote (("use_std") ("default" "use_std"))))))

(define-public crate-itertools-0.7 (crate (name "itertools") (vers "0.7.6") (deps (list (crate-dep (name "either") (req "^1.0") (kind 0)) (crate-dep (name "permutohedron") (req "^0.2") (default-features #t) (kind 2)) (crate-dep (name "quickcheck") (req "^0.5") (kind 2)))) (hash "15035jjc8hwc8091z3jn6lajjrda013qqzmnrqdhbdak74i34wxh") (features (quote (("use_std") ("default" "use_std"))))))

(define-public crate-itertools-0.7 (crate (name "itertools") (vers "0.7.7") (deps (list (crate-dep (name "either") (req "^1.0") (kind 0)) (crate-dep (name "permutohedron") (req "^0.2") (default-features #t) (kind 2)) (crate-dep (name "quickcheck") (req "^0.5") (kind 2)))) (hash "1lccfc9jxp7bg0ak5kcy0jp40yxgrarcbj5rqd280cwlfd63pm93") (features (quote (("use_std") ("default" "use_std"))))))

(define-public crate-itertools-0.7 (crate (name "itertools") (vers "0.7.8") (deps (list (crate-dep (name "either") (req "^1.0") (kind 0)) (crate-dep (name "permutohedron") (req "^0.2") (default-features #t) (kind 2)) (crate-dep (name "quickcheck") (req "^0.5") (kind 2)))) (hash "0l7lfiadcg04qw0rnz8fyhcmgcigl0bpc4rkapcysvvpdfbmd27m") (features (quote (("use_std") ("default" "use_std"))))))

(define-public crate-itertools-0.7 (crate (name "itertools") (vers "0.7.9") (deps (list (crate-dep (name "either") (req "^1.0") (kind 0)) (crate-dep (name "permutohedron") (req "^0.2") (default-features #t) (kind 2)) (crate-dep (name "quickcheck") (req "^0.5") (kind 2)))) (hash "07dga7w8w4w0sgklzrl01bf2kmv99k2f79vpxjwxnnk7axwm5vvw") (features (quote (("use_std") ("default" "use_std"))))))

(define-public crate-itertools-0.7 (crate (name "itertools") (vers "0.7.10") (deps (list (crate-dep (name "either") (req "^1.0") (kind 0)) (crate-dep (name "permutohedron") (req "^0.2") (default-features #t) (kind 2)) (crate-dep (name "quickcheck") (req "^0.5") (kind 2)))) (hash "18vwsdn38sh0zdg3462c4c3agaqk355rrk2hda26lkh3fpz59m2v") (features (quote (("use_std") ("default" "use_std"))))))

(define-public crate-itertools-0.7 (crate (name "itertools") (vers "0.7.11") (deps (list (crate-dep (name "either") (req "^1.0") (kind 0)) (crate-dep (name "permutohedron") (req "^0.2") (default-features #t) (kind 2)) (crate-dep (name "quickcheck") (req "^0.5") (kind 2)))) (hash "03cpsj26xmyamcalclqzr1i700vwx8hnbgxbpjvs354f8mnr8iqd") (features (quote (("use_std") ("default" "use_std"))))))

(define-public crate-itertools-0.8 (crate (name "itertools") (vers "0.8.0") (deps (list (crate-dep (name "either") (req "^1.0") (kind 0)) (crate-dep (name "permutohedron") (req "^0.2") (default-features #t) (kind 2)) (crate-dep (name "quickcheck") (req "^0.7") (kind 2)) (crate-dep (name "rand") (req "^0.6") (default-features #t) (kind 2)))) (hash "0n2k13b6w4x2x6np2lykh9bj3b3z4hwh2r4cn3z2dgnfq7cng12v") (features (quote (("use_std") ("default" "use_std"))))))

(define-public crate-itertools-0.8 (crate (name "itertools") (vers "0.8.1") (deps (list (crate-dep (name "either") (req "^1.0") (kind 0)) (crate-dep (name "permutohedron") (req "^0.2") (default-features #t) (kind 2)) (crate-dep (name "quickcheck") (req "^0.7") (kind 2)) (crate-dep (name "rand") (req "^0.6") (default-features #t) (kind 2)))) (hash "0vhdvb2ysn24h3wpdp2wkccyljzxhfxsnjcc2gippc57vv4pbyl7") (features (quote (("use_std") ("default" "use_std"))))))

(define-public crate-itertools-0.8 (crate (name "itertools") (vers "0.8.2") (deps (list (crate-dep (name "either") (req "^1.0") (kind 0)) (crate-dep (name "permutohedron") (req "^0.2") (default-features #t) (kind 2)) (crate-dep (name "quickcheck") (req "^0.7") (kind 2)) (crate-dep (name "rand") (req "^0.6") (default-features #t) (kind 2)))) (hash "1154j48aw913v5jnyhpxialxhdn2sfpl4d7bwididyb1r05jsspm") (features (quote (("use_std") ("default" "use_std"))))))

(define-public crate-itertools-0.9 (crate (name "itertools") (vers "0.9.0") (deps (list (crate-dep (name "criterion") (req "= 0.3.0") (default-features #t) (kind 2)) (crate-dep (name "either") (req "^1.0") (kind 0)) (crate-dep (name "permutohedron") (req "^0.2") (default-features #t) (kind 2)) (crate-dep (name "quickcheck") (req "^0.9") (kind 2)) (crate-dep (name "rand") (req "^0.7") (default-features #t) (kind 2)))) (hash "0jyml7ygr7kijkcjdl3fk5f34y5h5jsavclim7l13zjiavw1hkr8") (features (quote (("use_std") ("default" "use_std"))))))

(define-public crate-itertools-0.10 (crate (name "itertools") (vers "0.10.0") (deps (list (crate-dep (name "criterion") (req "=0") (default-features #t) (kind 2)) (crate-dep (name "either") (req "^1.0") (kind 0)) (crate-dep (name "paste") (req "^1.0.0") (default-features #t) (kind 2)) (crate-dep (name "permutohedron") (req "^0.2") (default-features #t) (kind 2)) (crate-dep (name "quickcheck") (req "^0.9") (kind 2)) (crate-dep (name "rand") (req "^0.7") (default-features #t) (kind 2)))) (hash "06dkghwi1a6ah2103gibxnr2ys762m5x4rp75x0q43imis8p5m9p") (features (quote (("use_std" "use_alloc") ("use_alloc") ("default" "use_std"))))))

(define-public crate-itertools-0.10 (crate (name "itertools") (vers "0.10.1") (deps (list (crate-dep (name "criterion") (req "=0") (default-features #t) (kind 2)) (crate-dep (name "either") (req "^1.0") (kind 0)) (crate-dep (name "paste") (req "^1.0.0") (default-features #t) (kind 2)) (crate-dep (name "permutohedron") (req "^0.2") (default-features #t) (kind 2)) (crate-dep (name "quickcheck") (req "^0.9") (kind 2)) (crate-dep (name "rand") (req "^0.7") (default-features #t) (kind 2)))) (hash "1bsyxnm20x05rwc5qskrqy4cfswrcadzlwc26dkqml6hz64vipb9") (features (quote (("use_std" "use_alloc") ("use_alloc") ("default" "use_std"))))))

(define-public crate-itertools-0.10 (crate (name "itertools") (vers "0.10.3") (deps (list (crate-dep (name "criterion") (req "=0") (default-features #t) (kind 2)) (crate-dep (name "either") (req "^1.0") (kind 0)) (crate-dep (name "paste") (req "^1.0.0") (default-features #t) (kind 2)) (crate-dep (name "permutohedron") (req "^0.2") (default-features #t) (kind 2)) (crate-dep (name "quickcheck") (req "^0.9") (kind 2)) (crate-dep (name "rand") (req "^0.7") (default-features #t) (kind 2)))) (hash "1qy55fqbaisr9qgbn7cvdvqlfqbh1f4ddf99zwan56z7l6gx3ad9") (features (quote (("use_std" "use_alloc") ("use_alloc") ("default" "use_std"))))))

(define-public crate-itertools-0.10 (crate (name "itertools") (vers "0.10.4") (deps (list (crate-dep (name "criterion") (req "=0") (default-features #t) (kind 2)) (crate-dep (name "either") (req "^1.0") (kind 0)) (crate-dep (name "paste") (req "^1.0.0") (default-features #t) (kind 2)) (crate-dep (name "permutohedron") (req "^0.2") (default-features #t) (kind 2)) (crate-dep (name "quickcheck") (req "^0.9") (kind 2)) (crate-dep (name "rand") (req "^0.7") (default-features #t) (kind 2)))) (hash "1l3qmfysx7salxbci99i2crd3ky73bkla2vrlj190yp6g5vj9gyq") (features (quote (("use_std" "use_alloc" "either/use_std") ("use_alloc") ("default" "use_std"))))))

(define-public crate-itertools-0.10 (crate (name "itertools") (vers "0.10.5") (deps (list (crate-dep (name "criterion") (req "= 0") (default-features #t) (kind 2)) (crate-dep (name "either") (req "^1.0") (kind 0)) (crate-dep (name "paste") (req "^1.0.0") (default-features #t) (kind 2)) (crate-dep (name "permutohedron") (req "^0.2") (default-features #t) (kind 2)) (crate-dep (name "quickcheck") (req "^0.9") (kind 2)) (crate-dep (name "rand") (req "^0.7") (default-features #t) (kind 2)))) (hash "0ww45h7nxx5kj6z2y6chlskxd1igvs4j507anr6dzg99x1h25zdh") (features (quote (("use_std" "use_alloc" "either/use_std") ("use_alloc") ("default" "use_std"))))))

(define-public crate-itertools-0.11 (crate (name "itertools") (vers "0.11.0") (deps (list (crate-dep (name "criterion") (req "^0.4.0") (default-features #t) (kind 2)) (crate-dep (name "either") (req "^1.0") (kind 0)) (crate-dep (name "paste") (req "^1.0.0") (default-features #t) (kind 2)) (crate-dep (name "permutohedron") (req "^0.2") (default-features #t) (kind 2)) (crate-dep (name "quickcheck") (req "^0.9") (kind 2)) (crate-dep (name "rand") (req "^0.7") (default-features #t) (kind 2)))) (hash "0mzyqcc59azx9g5cg6fs8k529gvh4463smmka6jvzs3cd2jp7hdi") (features (quote (("use_std" "use_alloc" "either/use_std") ("use_alloc") ("default" "use_std")))) (rust-version "1.36.0")))

(define-public crate-itertools-0.12 (crate (name "itertools") (vers "0.12.0") (deps (list (crate-dep (name "criterion") (req "^0.4.0") (default-features #t) (kind 2)) (crate-dep (name "either") (req "^1.0") (kind 0)) (crate-dep (name "paste") (req "^1.0.0") (default-features #t) (kind 2)) (crate-dep (name "permutohedron") (req "^0.2") (default-features #t) (kind 2)) (crate-dep (name "quickcheck") (req "^0.9") (kind 2)) (crate-dep (name "rand") (req "^0.7") (default-features #t) (kind 2)))) (hash "1c07gzdlc6a1c8p8jrvvw3gs52bss3y58cs2s21d9i978l36pnr5") (features (quote (("use_std" "use_alloc" "either/use_std") ("use_alloc") ("default" "use_std")))) (rust-version "1.43.1")))

(define-public crate-itertools-0.12 (crate (name "itertools") (vers "0.12.1") (deps (list (crate-dep (name "criterion") (req "^0.4.0") (default-features #t) (kind 2)) (crate-dep (name "either") (req "^1.0") (kind 0)) (crate-dep (name "paste") (req "^1.0.0") (default-features #t) (kind 2)) (crate-dep (name "permutohedron") (req "^0.2") (default-features #t) (kind 2)) (crate-dep (name "quickcheck") (req "^0.9") (kind 2)) (crate-dep (name "rand") (req "^0.7") (default-features #t) (kind 2)))) (hash "0s95jbb3ndj1lvfxyq5wanc0fm0r6hg6q4ngb92qlfdxvci10ads") (features (quote (("use_std" "use_alloc" "either/use_std") ("use_alloc") ("default" "use_std")))) (rust-version "1.43.1")))

(define-public crate-itertools-0.13 (crate (name "itertools") (vers "0.13.0") (deps (list (crate-dep (name "criterion") (req "^0.4.0") (default-features #t) (kind 2)) (crate-dep (name "either") (req "^1.0") (kind 0)) (crate-dep (name "paste") (req "^1.0.0") (default-features #t) (kind 2)) (crate-dep (name "permutohedron") (req "^0.2") (default-features #t) (kind 2)) (crate-dep (name "quickcheck") (req "^0.9") (kind 2)) (crate-dep (name "rand") (req "^0.7") (default-features #t) (kind 2)))) (hash "11hiy3qzl643zcigknclh446qb9zlg4dpdzfkjaa9q9fqpgyfgj1") (features (quote (("use_std" "use_alloc" "either/use_std") ("use_alloc") ("default" "use_std")))) (rust-version "1.43.1")))

(define-public crate-itertools-num-0.1 (crate (name "itertools-num") (vers "0.1.0") (deps (list (crate-dep (name "itertools") (req "^0.4") (default-features #t) (kind 2)) (crate-dep (name "num-traits") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "quickcheck") (req "^0.3") (default-features #t) (kind 2)))) (hash "0yjr572kfkgq7gq50ap80i369w8yc9hm2wxcpcvw380ji50ryi97")))

(define-public crate-itertools-num-0.1 (crate (name "itertools-num") (vers "0.1.1") (deps (list (crate-dep (name "itertools") (req "^0.5") (default-features #t) (kind 2)) (crate-dep (name "num-traits") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "quickcheck") (req "^0.4") (kind 2)))) (hash "04vqf6k4zc8i8wxvjgi0sxw6hp8xk6n65y9np84fdrl3hdhgly2d")))

(define-public crate-itertools-num-0.1 (crate (name "itertools-num") (vers "0.1.2") (deps (list (crate-dep (name "itertools") (req "^0.7.7") (default-features #t) (kind 2)) (crate-dep (name "num-traits") (req "^0.2.2") (default-features #t) (kind 0)) (crate-dep (name "quickcheck") (req "^0.4") (kind 2)))) (hash "1z42m1ywrk9ivp5dwjj6hbxk9zhwxnhngwy2qr5y7wiqp1q7pjl3")))

(define-public crate-itertools-num-0.1 (crate (name "itertools-num") (vers "0.1.3") (deps (list (crate-dep (name "itertools") (req "^0.7.7") (default-features #t) (kind 2)) (crate-dep (name "num-traits") (req "^0.2.2") (default-features #t) (kind 0)) (crate-dep (name "quickcheck") (req "^0.7") (kind 2)))) (hash "1rr7ig9nkpampcas23s91x7yac6qdnwssq3nap522xbgkqps4wm8")))

(define-public crate-itertools-wild-0.1 (crate (name "itertools-wild") (vers "0.1.0") (deps (list (crate-dep (name "either") (req "^1.0") (kind 0)) (crate-dep (name "itertools") (req "^0.6.2") (default-features #t) (kind 0)) (crate-dep (name "quickcheck") (req "^0.4") (kind 2)))) (hash "0pnm8ysww74rh3wm42s3xa2yz2yn3szm7irl52b69w7499pp0y0m")))

(define-public crate-itertools-wild-0.1 (crate (name "itertools-wild") (vers "0.1.1") (deps (list (crate-dep (name "either") (req "^1.0") (kind 0)) (crate-dep (name "itertools") (req "^0.6.2") (default-features #t) (kind 0)) (crate-dep (name "quickcheck") (req "^0.4") (kind 2)))) (hash "1m72ib0c2nmvzpqbqdlnrk6020fbaq64n5912z8gkrsjxv4j9kxb")))

(define-public crate-itertree-0.0.3 (crate (name "itertree") (vers "0.0.3") (deps (list (crate-dep (name "rstest") (req "^0.9.0") (default-features #t) (kind 2)))) (hash "0zf1lszjwk2q293f6rz235v92zr7vgmy2aa56ardxld8gffhyi6j")))

(define-public crate-iterutils-0.1 (crate (name "iterutils") (vers "0.1.0") (hash "17cawvgkjg64cvw6x6j12c59i9733280s0lir3z4293pngwjv143")))

(define-public crate-iterware-0.1 (crate (name "iterware") (vers "0.1.0") (hash "0bpgm1nrh0gjcjzqzs7yr2jk89p23jk05ifgza01pqsnzpxld45r")))

(define-public crate-iterwindows-0.1 (crate (name "iterwindows") (vers "0.1.0") (hash "0hb4ydf9bx0q0n8mi6b9a0prdq7pw3bd3yx05g0wafgmxrh4xch8")))

(define-public crate-iterwindows-0.1 (crate (name "iterwindows") (vers "0.1.1") (hash "0ncrl2f7c0ck94p60jzxaz150i3ppajgbsqaq8g22dwcgp83ln93") (rust-version "1.56")))

(define-public crate-iterwindows-0.1 (crate (name "iterwindows") (vers "0.1.2") (deps (list (crate-dep (name "arrays") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "0jyhyaq5idbix15w61yvvny22d9xdzvlsv7a169jds46gfabq0yj") (rust-version "1.56")))

(define-public crate-iterwindows-0.2 (crate (name "iterwindows") (vers "0.2.0") (deps (list (crate-dep (name "arrays") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "1kcwxpdf8bv0l620jf7kbg4vr2pximz550dhk2zqa2d11xi9r0j0") (rust-version "1.56")))

(define-public crate-iterwindows-0.3 (crate (name "iterwindows") (vers "0.3.0") (deps (list (crate-dep (name "arrays") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "1c1kcfz7cxgjc0z8jgf5cpbwizwpyxyg8xm9prynda8rq243mgrw") (rust-version "1.56")))

(define-public crate-iterwindows-0.4 (crate (name "iterwindows") (vers "0.4.0") (deps (list (crate-dep (name "itermore") (req "^0.5.0") (features (quote ("array_windows"))) (kind 0)))) (hash "0fj8p8qgiww5p1p80w51n2kr439h61k0dj9sa1izay6igfypqhw8") (rust-version "1.56")))

(define-public crate-iterwindows-0.5 (crate (name "iterwindows") (vers "0.5.0") (deps (list (crate-dep (name "itermore") (req "^0.6.0") (features (quote ("array_windows"))) (kind 0)))) (hash "1rn4adhkwi5d7i112sxiy7rxp7x4inrky4j2gyz997y878pxkads") (rust-version "1.56")))

(define-public crate-iterx-0.0.1 (crate (name "iterx") (vers "0.0.1") (deps (list (crate-dep (name "itertools") (req "^0.10.5") (default-features #t) (kind 0)))) (hash "0vvfcq3mir1yf1ssf53k667lrxy7bxwi5c06ymh2a021dy9vc35s") (yanked #t)))

(define-public crate-iterx-0.0.2 (crate (name "iterx") (vers "0.0.2") (deps (list (crate-dep (name "itertools") (req "^0.10.5") (default-features #t) (kind 0)))) (hash "18zp4wlb8paqj5chlynmqb06z469ifld817d89mg4ng97rw8lp1h") (yanked #t)))

(define-public crate-iterx-0.0.3 (crate (name "iterx") (vers "0.0.3") (deps (list (crate-dep (name "itertools") (req "^0.10.5") (default-features #t) (kind 0)))) (hash "0r92aad44z7566k7y1yaxk313ny6jqs0p850c88cj3zjb2wksmd3") (yanked #t)))

(define-public crate-iterx-0.0.4 (crate (name "iterx") (vers "0.0.4") (deps (list (crate-dep (name "itertools") (req "^0.10.5") (default-features #t) (kind 0)))) (hash "1w8v5i051rsncl5hmy7m6hq654bkz7kjrl4cmqzkixk3msgwibdn") (yanked #t)))

(define-public crate-iterx-0.0.5 (crate (name "iterx") (vers "0.0.5") (deps (list (crate-dep (name "itertools") (req "^0.10.5") (default-features #t) (kind 0)))) (hash "0va8grmb67gri2v9hbdpiqd2j0ql3x2r9pbd473z50w61p8m7p33") (yanked #t)))

(define-public crate-iterx-0.0.6 (crate (name "iterx") (vers "0.0.6") (deps (list (crate-dep (name "itertools") (req "^0.10.5") (default-features #t) (kind 0)))) (hash "0vya65k81fbh1g3l2jwr8nxy3q9vn79d8hs88akw310h1jx0rb9d") (yanked #t)))

(define-public crate-iterx-0.0.7 (crate (name "iterx") (vers "0.0.7") (deps (list (crate-dep (name "itertools") (req "^0.10.5") (default-features #t) (kind 0)))) (hash "0d4p8a7si51l8zz214g3cg8ig4l769l684j33i5hy9k55njc3xyh") (yanked #t)))

(define-public crate-iterx-0.0.8 (crate (name "iterx") (vers "0.0.8") (deps (list (crate-dep (name "itertools") (req "^0.10.5") (default-features #t) (kind 0)))) (hash "0xfbnr95lia4c70nir1d5xmsr1nz40f0nfiwpkcr419bgbcb393m") (yanked #t)))

(define-public crate-iterx-0.0.9 (crate (name "iterx") (vers "0.0.9") (deps (list (crate-dep (name "itertools") (req "^0.10.5") (default-features #t) (kind 0)))) (hash "1z28gkalzr61l75fcr5j9wfvgmnqxz3yipr412ih6383if78jad7") (yanked #t)))

(define-public crate-iterx-0.0.10 (crate (name "iterx") (vers "0.0.10") (deps (list (crate-dep (name "itertools") (req "^0.10.5") (default-features #t) (kind 0)))) (hash "0mixb5p069f469gbmawxacv2ypm4pv1ydzzwhmllqv1yph3nkihj")))

