(define-module (crates-io it -m) #:use-module (crates-io))

(define-public crate-it-memory-traits-0.1 (crate (name "it-memory-traits") (vers "0.1.0") (deps (list (crate-dep (name "thiserror") (req "^1.0.24") (default-features #t) (kind 0)))) (hash "1bq3ni6hr4slrd26xsdz12gz877jhsa13q1lxp831nqqpnj9l7wv")))

(define-public crate-it-memory-traits-0.2 (crate (name "it-memory-traits") (vers "0.2.0") (deps (list (crate-dep (name "thiserror") (req "^1.0.24") (default-features #t) (kind 0)))) (hash "0hr59s8vpra98larva7dmhxpv6500cwn2gl6fiqhi22pw0ja31w5")))

(define-public crate-it-memory-traits-0.3 (crate (name "it-memory-traits") (vers "0.3.0") (deps (list (crate-dep (name "thiserror") (req "^1.0.24") (default-features #t) (kind 0)))) (hash "0h2wkzjwp6c6ms3n1s0bizjyjk84i52v2jm1h0p1j6p5i7nrj5ya")))

(define-public crate-it-memory-traits-0.3 (crate (name "it-memory-traits") (vers "0.3.1") (deps (list (crate-dep (name "thiserror") (req "^1.0.34") (default-features #t) (kind 0)))) (hash "18jn9nd1k61jl6s5vfrir93ckqy4pbkjp6y3x73r972gsrnz5s20")))

(define-public crate-it-memory-traits-0.4 (crate (name "it-memory-traits") (vers "0.4.0") (deps (list (crate-dep (name "thiserror") (req "^1.0.38") (default-features #t) (kind 0)))) (hash "0vchrjs3zksp22gy6zclx32yqzxidawxylbr3d3wy5zfpakg4qym")))

(define-public crate-it-memory-traits-0.5 (crate (name "it-memory-traits") (vers "0.5.0") (deps (list (crate-dep (name "thiserror") (req "^1.0.38") (default-features #t) (kind 0)))) (hash "04j6biliahd7qhky9bw59kyg46s75c9qsz6z2cmkxmb926c8jc78")))

