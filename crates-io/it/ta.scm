(define-module (crates-io it ta) #:use-module (crates-io))

(define-public crate-ittapi-0.3 (crate (name "ittapi") (vers "0.3.0") (deps (list (crate-dep (name "anyhow") (req "^1.0.56") (default-features #t) (kind 0)) (crate-dep (name "ittapi-sys") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.16") (default-features #t) (kind 0)) (crate-dep (name "scoped-env") (req "^2.1") (default-features #t) (kind 2)))) (hash "1n5i2kddfcd7ra1g87a091g5anpqv8mcr8q34zhrj2vxvalsdfx4")))

(define-public crate-ittapi-0.3 (crate (name "ittapi") (vers "0.3.1") (deps (list (crate-dep (name "anyhow") (req "^1.0.56") (default-features #t) (kind 0)) (crate-dep (name "ittapi-sys") (req "^0.3.1") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.16") (default-features #t) (kind 0)) (crate-dep (name "scoped-env") (req "^2.1") (default-features #t) (kind 2)))) (hash "18dilrgc0kdcv508220axmlwiq9yrnln864qkvsiy1vh01ay0gv6")))

(define-public crate-ittapi-0.3 (crate (name "ittapi") (vers "0.3.2") (deps (list (crate-dep (name "anyhow") (req "^1.0.56") (default-features #t) (kind 0)) (crate-dep (name "ittapi-sys") (req "^0.3.2") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.16") (default-features #t) (kind 0)) (crate-dep (name "scoped-env") (req "^2.1") (default-features #t) (kind 2)))) (hash "1x227sj52zmq0vmilwi9m4b3fzhwa2qm0ldcil2fg70n0vzzdi78")))

(define-public crate-ittapi-0.3 (crate (name "ittapi") (vers "0.3.3") (deps (list (crate-dep (name "anyhow") (req "^1.0.56") (default-features #t) (kind 0)) (crate-dep (name "ittapi-sys") (req "^0.3.3") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.16") (default-features #t) (kind 0)) (crate-dep (name "scoped-env") (req "^2.1") (default-features #t) (kind 2)))) (hash "19v29bx28ddl2slmb93gh9a201smhw31m8awqcx7vkkjf51qqr1f")))

(define-public crate-ittapi-0.3 (crate (name "ittapi") (vers "0.3.4") (deps (list (crate-dep (name "anyhow") (req "^1.0.56") (default-features #t) (kind 0)) (crate-dep (name "ittapi-sys") (req "^0.3.4") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.16") (default-features #t) (kind 0)) (crate-dep (name "scoped-env") (req "^2.1") (default-features #t) (kind 2)))) (hash "0b2qh33qgdvbn6wgblcq9j5nnahi00sdw3mqx2kr4gdmnfvx1q21")))

(define-public crate-ittapi-0.3 (crate (name "ittapi") (vers "0.3.5") (deps (list (crate-dep (name "anyhow") (req "^1.0.56") (default-features #t) (kind 0)) (crate-dep (name "ittapi-sys") (req "^0.3.5") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.16") (default-features #t) (kind 0)) (crate-dep (name "scoped-env") (req "^2.1") (default-features #t) (kind 2)))) (hash "0wbjdpq28dvm5iigi4jmscvz7nf5cmjhgsi2c9wss730jfww1995")))

(define-public crate-ittapi-0.4 (crate (name "ittapi") (vers "0.4.0") (deps (list (crate-dep (name "anyhow") (req "^1.0.56") (default-features #t) (kind 0)) (crate-dep (name "ittapi-sys") (req "^0.4.0") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.16") (default-features #t) (kind 0)) (crate-dep (name "scoped-env") (req "^2.1") (default-features #t) (kind 2)))) (hash "1cb41dapbximlma0vnar144m2j2km44g8g6zmv6ra4y42kk6z6bb")))

(define-public crate-ittapi-rs-0.1 (crate (name "ittapi-rs") (vers "0.1.0") (deps (list (crate-dep (name "bindgen") (req "^0.50") (default-features #t) (kind 1)) (crate-dep (name "cmake") (req "^0.1.42") (default-features #t) (kind 1)))) (hash "0vgnqmql4fhjz3l22mz3a871bsnz1yfrs3jm0584hr7kbsadrv4k") (features (quote (("force_32")))) (yanked #t)))

(define-public crate-ittapi-rs-0.1 (crate (name "ittapi-rs") (vers "0.1.1") (deps (list (crate-dep (name "bindgen") (req "^0.50") (default-features #t) (kind 1)) (crate-dep (name "cmake") (req "^0.1.42") (default-features #t) (kind 1)))) (hash "0mvjy4yz32x61xyp7immg3wbzhf432ar327vyh9zi5001g748ls6") (features (quote (("force_32"))))))

(define-public crate-ittapi-rs-0.1 (crate (name "ittapi-rs") (vers "0.1.2") (deps (list (crate-dep (name "bindgen") (req "^0.50") (default-features #t) (kind 1)) (crate-dep (name "cmake") (req "^0.1.42") (default-features #t) (kind 1)))) (hash "1w86x09akim87kxgx4qvcxqn3xhsv7fs0awz2d27kpj6qpy2pfaw") (features (quote (("force_32"))))))

(define-public crate-ittapi-rs-0.1 (crate (name "ittapi-rs") (vers "0.1.3") (deps (list (crate-dep (name "bindgen") (req "^0.52.0") (default-features #t) (kind 2)) (crate-dep (name "cmake") (req "^0.1.42") (default-features #t) (kind 1)) (crate-dep (name "diff") (req "^0.1") (default-features #t) (kind 2)))) (hash "1175028j2xway7qc8ksgvp6fjw32l57aa02zkbhapb218ysq0aik") (features (quote (("force_32"))))))

(define-public crate-ittapi-rs-0.1 (crate (name "ittapi-rs") (vers "0.1.4") (deps (list (crate-dep (name "bindgen") (req "^0.52.0") (default-features #t) (kind 2)) (crate-dep (name "cmake") (req "^0.1.42") (default-features #t) (kind 1)) (crate-dep (name "diff") (req "^0.1") (default-features #t) (kind 2)))) (hash "177jvh1ifbq83kw67dmb11mbn8g6xs19ql025vjhmahhhd0dq47a") (features (quote (("force_32"))))))

(define-public crate-ittapi-rs-0.1 (crate (name "ittapi-rs") (vers "0.1.5") (deps (list (crate-dep (name "bindgen") (req "^0.52.0") (default-features #t) (kind 2)) (crate-dep (name "cmake") (req "^0.1.42") (default-features #t) (kind 1)) (crate-dep (name "diff") (req "^0.1") (default-features #t) (kind 2)))) (hash "1c7qy7i897kdxcjvq9q1my3ckn34xcxf6cv78p2fa6b323vxl5ps") (features (quote (("force_32"))))))

(define-public crate-ittapi-rs-0.1 (crate (name "ittapi-rs") (vers "0.1.6") (deps (list (crate-dep (name "bindgen") (req "^0.59") (default-features #t) (kind 2)) (crate-dep (name "cc") (req "^1.0.72") (default-features #t) (kind 1)) (crate-dep (name "diff") (req "^0.1") (default-features #t) (kind 2)))) (hash "0n1nfacd1bmf0lgcxngmc6mzf6mgnxmgd8cppw19ngmmsmfd4pi6") (features (quote (("force_32"))))))

(define-public crate-ittapi-rs-0.2 (crate (name "ittapi-rs") (vers "0.2.0") (deps (list (crate-dep (name "bindgen") (req "^0.59") (default-features #t) (kind 2)) (crate-dep (name "cc") (req "^1.0.72") (default-features #t) (kind 1)) (crate-dep (name "diff") (req "^0.1") (default-features #t) (kind 2)))) (hash "1c3j67fpha5sxymwnc6gz0ig144d3qrjqxy2mnvvybyp3a5684pp")))

(define-public crate-ittapi-rs-0.3 (crate (name "ittapi-rs") (vers "0.3.0") (hash "12czvcrz25jsgf65vd22l1ylw8g4yypca938nlrbx7009p4vqd6w")))

(define-public crate-ittapi-sys-0.3 (crate (name "ittapi-sys") (vers "0.3.0") (deps (list (crate-dep (name "bindgen") (req "^0.59") (default-features #t) (kind 2)) (crate-dep (name "cc") (req "^1.0.73") (default-features #t) (kind 1)) (crate-dep (name "diff") (req "^0.1") (default-features #t) (kind 2)))) (hash "18ghrw1vjbg81sww8appyj9cw3z6fvxqckbcyxjbwfj3ip1qa3wd")))

(define-public crate-ittapi-sys-0.3 (crate (name "ittapi-sys") (vers "0.3.1") (deps (list (crate-dep (name "bindgen") (req "^0.59") (default-features #t) (kind 2)) (crate-dep (name "cc") (req "^1.0.73") (default-features #t) (kind 1)) (crate-dep (name "diff") (req "^0.1") (default-features #t) (kind 2)))) (hash "00q787ciwlg5wbn3fw02a7djhkiigx98lixbflfwff1z32vi26g2")))

(define-public crate-ittapi-sys-0.3 (crate (name "ittapi-sys") (vers "0.3.2") (deps (list (crate-dep (name "bindgen") (req "^0.59") (default-features #t) (kind 2)) (crate-dep (name "cc") (req "^1.0.73") (default-features #t) (kind 1)) (crate-dep (name "diff") (req "^0.1") (default-features #t) (kind 2)))) (hash "1njfwvlbqzx1gly8a0jm0vrfvah4sr6v7gp3p8cg918lw367iq47")))

(define-public crate-ittapi-sys-0.3 (crate (name "ittapi-sys") (vers "0.3.3") (deps (list (crate-dep (name "bindgen") (req "^0.63") (default-features #t) (kind 2)) (crate-dep (name "cc") (req "^1.0.73") (default-features #t) (kind 1)) (crate-dep (name "diff") (req "^0.1") (default-features #t) (kind 2)))) (hash "19f11lg8dgd51w6mqmr5irchhskb5k0z6m6yilblh9gp4d6jmcx9")))

(define-public crate-ittapi-sys-0.3 (crate (name "ittapi-sys") (vers "0.3.4") (deps (list (crate-dep (name "bindgen") (req "^0.59") (default-features #t) (kind 2)) (crate-dep (name "cc") (req "^1.0.73") (default-features #t) (kind 1)) (crate-dep (name "diff") (req "^0.1") (default-features #t) (kind 2)))) (hash "0z572hb4qgqsja2n3bmf78qy6csyic6rkhnc1mm6skp5jqy7dy7j")))

(define-public crate-ittapi-sys-0.3 (crate (name "ittapi-sys") (vers "0.3.5") (deps (list (crate-dep (name "bindgen") (req "^0.68") (default-features #t) (kind 2)) (crate-dep (name "cc") (req "^1.0.73") (default-features #t) (kind 1)) (crate-dep (name "diff") (req "^0.1") (default-features #t) (kind 2)))) (hash "1n972w6wcna94rir1r3k1j0imksqywkx3vk0lqv0a1k56x3mwyyb")))

(define-public crate-ittapi-sys-0.4 (crate (name "ittapi-sys") (vers "0.4.0") (deps (list (crate-dep (name "bindgen") (req "^0.68") (default-features #t) (kind 2)) (crate-dep (name "cc") (req "^1.0.73") (default-features #t) (kind 1)) (crate-dep (name "diff") (req "^0.1") (default-features #t) (kind 2)))) (hash "1z7lgc7gwlhcvkdk6bg9sf1ww4w0b41blp90hv4a4kq6ji9kixaj")))

