(define-module (crates-io it dl) #:use-module (crates-io))

(define-public crate-itdl-0.4 (crate (name "itdl") (vers "0.4.0") (deps (list (crate-dep (name "crossterm") (req "^0.27.0") (default-features #t) (kind 0)))) (hash "13r8pa0xh9vwnv2ah5zjcd7xdi5d8hl4xnf6r5530yli4va7qm1c")))

