(define-module (crates-io it ho) #:use-module (crates-io))

(define-public crate-ithos-0.0.0 (crate (name "ithos") (vers "0.0.0") (deps (list (crate-dep (name "lmdb-rs") (req ">= 0.7.1") (default-features #t) (kind 0)))) (hash "19ja7fkj32wjhimskksly7vz6zdjkhn04y56cq0pb8mbx69ikgsd")))

(define-public crate-ithosc-0.0.0 (crate (name "ithosc") (vers "0.0.0") (deps (list (crate-dep (name "abscissa") (req "^0") (default-features #t) (kind 0)))) (hash "1glfy8w9g71idqrwbjxswkp3g3nda9rj8fkj7ksyb9mbb41i5y45") (yanked #t)))

(define-public crate-ithosd-0.0.0 (crate (name "ithosd") (vers "0.0.0") (deps (list (crate-dep (name "abscissa") (req "^0") (default-features #t) (kind 0)))) (hash "187dgaqiykgnbqf8kjpgzqvn4y0knn7bvlqkcx52ih0f46h1ri79") (yanked #t)))

