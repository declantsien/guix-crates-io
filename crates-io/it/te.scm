(define-module (crates-io it te) #:use-module (crates-io))

(define-public crate-ittech-0.1 (crate (name "ittech") (vers "0.1.1") (deps (list (crate-dep (name "anyhow") (req "^1.0") (default-features #t) (kind 2)) (crate-dep (name "bitflags") (req "^1.2") (default-features #t) (kind 0)) (crate-dep (name "nom") (req "^6.1") (kind 0)) (crate-dep (name "pretty_assertions") (req "^0.6") (default-features #t) (kind 2)))) (hash "155xbxhzzxi4261gp23pwjinb4whzhwn7jkqf9srq02wr5304siw")))

(define-public crate-ittech-0.2 (crate (name "ittech") (vers "0.2.0") (deps (list (crate-dep (name "anyhow") (req "^1.0") (default-features #t) (kind 2)) (crate-dep (name "bitflags") (req "^1.2") (default-features #t) (kind 0)) (crate-dep (name "nom") (req "^6.1") (kind 0)) (crate-dep (name "pretty_assertions") (req "^0.6") (default-features #t) (kind 2)))) (hash "1s0377d5hlc988g56dyxlry8m00izwbz7q91i4nvwqfh0iygz2ns")))

(define-public crate-ittech-0.3 (crate (name "ittech") (vers "0.3.0") (deps (list (crate-dep (name "anyhow") (req "^1.0") (default-features #t) (kind 2)) (crate-dep (name "bitflags") (req "^1.2") (default-features #t) (kind 0)) (crate-dep (name "nom") (req "^7.0") (default-features #t) (kind 0)) (crate-dep (name "pretty_assertions") (req "^0.6") (default-features #t) (kind 2)) (crate-dep (name "tracing") (req "^0.1") (features (quote ("std"))) (optional #t) (kind 0)))) (hash "065w205wd8lcjww7d5mxzyxs4mni1jklg9zaq6a1bp4q86p474lz") (features (quote (("log" "tracing/log"))))))

