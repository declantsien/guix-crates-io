(define-module (crates-io it -f) #:use-module (crates-io))

(define-public crate-it-fits-0.1 (crate (name "it-fits") (vers "0.1.0") (hash "04wfc3m0jls75ar2f7sc40ixjyzjr45qvnr1xmgm4jb8v1pr330x") (yanked #t)))

(define-public crate-it-fits-0.1 (crate (name "it-fits") (vers "0.1.1") (hash "1la6cyh5pk671lsn1m8qwywn4lkgcflz59qq6c44byqdqh0b1mm6")))

