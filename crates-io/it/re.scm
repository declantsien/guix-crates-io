(define-module (crates-io it re) #:use-module (crates-io))

(define-public crate-itree-0.2 (crate (name "itree") (vers "0.2.2") (deps (list (crate-dep (name "clap") (req "^2.31") (default-features #t) (kind 0)) (crate-dep (name "ignore") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "indextree") (req "^1.1") (default-features #t) (kind 0)) (crate-dep (name "termion") (req "^1.5") (default-features #t) (kind 0)))) (hash "0s8vy6k4yihnvy87pms0r4jp01lydhnnj4cqmd1gsawas8sj02rc")))

(define-public crate-itree-0.3 (crate (name "itree") (vers "0.3.0") (deps (list (crate-dep (name "clap") (req "^2.31") (default-features #t) (kind 0)) (crate-dep (name "ignore") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "indextree") (req "^1.1") (default-features #t) (kind 0)) (crate-dep (name "termion") (req "^1.5") (default-features #t) (kind 0)))) (hash "181pcih7g96hc6ha6iwf01rlz222q95dclxd89bs8gb2ad7h0vd4")))

(define-public crate-itree-0.3 (crate (name "itree") (vers "0.3.1") (deps (list (crate-dep (name "clap") (req "^2.31") (default-features #t) (kind 0)) (crate-dep (name "ignore") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "indextree") (req "^1.1") (default-features #t) (kind 0)) (crate-dep (name "termion") (req "^1.5") (default-features #t) (kind 0)))) (hash "16wj2i3q8lgnvi3fyrs6vrqlvd6xgrqzzr6d2yfd67g9jfl3a0jg")))

