(define-module (crates-io it em) #:use-module (crates-io))

(define-public crate-item-0.1 (crate (name "item") (vers "0.1.0") (deps (list (crate-dep (name "nom") (req "^1.2.4") (default-features #t) (kind 0)))) (hash "1rwy7f0byrxphqh44xc5rkcjwwahbw5gqiwqfxhfpc3lss6bz7ch") (yanked #t)))

(define-public crate-item-0.2 (crate (name "item") (vers "0.2.0") (deps (list (crate-dep (name "nom") (req "^1.2.4") (default-features #t) (kind 0)))) (hash "19ds09gpy8506a0185165k607w5hz0kv3l0l2pq0kj9djfmr61hd") (yanked #t)))

(define-public crate-item-0.3 (crate (name "item") (vers "0.3.0") (deps (list (crate-dep (name "nom") (req "^1.2.4") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^0.1.1") (optional #t) (default-features #t) (kind 0)))) (hash "1qjzcrzvn57qgwnl1zdk2gfvwy7ysmyf1kzvgwr55zi731v18x6n") (features (quote (("printing" "quote") ("parsing" "nom") ("default" "parsing" "printing")))) (yanked #t)))

(define-public crate-item-0.3 (crate (name "item") (vers "0.3.1") (deps (list (crate-dep (name "nom") (req "^1.2.4") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^0.1.1") (optional #t) (default-features #t) (kind 0)))) (hash "0rlljxnbin8b4mmhacc9sf2caa4b7xigqpbvhzd7ji1gdp76ik8j") (features (quote (("printing" "quote") ("parsing" "nom") ("default" "parsing" "printing")))) (yanked #t)))

(define-public crate-item-0.3 (crate (name "item") (vers "0.3.2") (deps (list (crate-dep (name "nom") (req "^1.2.4") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^0.1.1") (optional #t) (default-features #t) (kind 0)))) (hash "1rl5ral0a7ny60jlssivfbly4q4qhkyczh4p7s23nxrksj8lzkyn") (features (quote (("printing" "quote") ("parsing" "nom") ("default" "parsing" "printing")))) (yanked #t)))

(define-public crate-itemgen-0.0.0 (crate (name "itemgen") (vers "0.0.0") (hash "1j9hvr8angfdmad02vvm3fz8xvi8rj8v4gc8zbffhd6r1bnq4rfv")))

(define-public crate-itemops-0.1 (crate (name "itemops") (vers "0.1.0") (hash "1hxj87xx629k5jkyvdsk890xnypgq9ngh4r6vlj74sqnlh894621")))

(define-public crate-itemops-0.1 (crate (name "itemops") (vers "0.1.1") (hash "1a23zdm8srxx8jc8pnlhycwjyqwix7xpdwxp66x96fqyz1q9mzff")))

