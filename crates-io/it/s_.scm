(define-module (crates-io it s_) #:use-module (crates-io))

(define-public crate-its_ok-0.1 (crate (name "its_ok") (vers "0.1.0") (deps (list (crate-dep (name "proc-macro2") (req "^1.0.39") (default-features #t) (kind 2)) (crate-dep (name "quote") (req "^1.0.18") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.96") (features (quote ("fold" "full" "parsing"))) (default-features #t) (kind 0)))) (hash "1k6x4r5n5sza097439j2qyb7g0hlf72dq9rwc55j9z316jrdiw8m")))

(define-public crate-its_ok_to_be_block_on-0.1 (crate (name "its_ok_to_be_block_on") (vers "0.1.0") (deps (list (crate-dep (name "futures-task") (req "^0.3.30") (default-features #t) (kind 0)) (crate-dep (name "pin-utils") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "185vqj7ifkqj3z61ixsn11hiw8acn6xndhxh9k67i05pfwgs7g55")))

