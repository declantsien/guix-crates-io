(define-module (crates-io it su) #:use-module (crates-io))

(define-public crate-itsuki-0.1 (crate (name "itsuki") (vers "0.1.0") (deps (list (crate-dep (name "proc-macro2") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "0x52x4jplwlqjra36sdim77dz8wdci9mn7z6170vsbb1pdyb0jy2")))

(define-public crate-itsuki-0.1 (crate (name "itsuki") (vers "0.1.1") (deps (list (crate-dep (name "proc-macro2") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "18by7bh41jfc2y8ri0h4casc63kw607qmafphjsab12na3fh9qb2")))

(define-public crate-itsuki-0.1 (crate (name "itsuki") (vers "0.1.2") (deps (list (crate-dep (name "proc-macro2") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "19chslvds5li2fxhclbpgl9sa2vnhfgssmx1cjk1ygsj288n6i93")))

(define-public crate-itsuki-0.2 (crate (name "itsuki") (vers "0.2.0") (deps (list (crate-dep (name "proc-macro2") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "147bwr92skzbvga7qnlkpby3bb4hjj3i02vd5y2cvnqq6v12akk9")))

