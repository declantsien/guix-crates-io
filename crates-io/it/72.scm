(define-module (crates-io it #{72}#) #:use-module (crates-io))

(define-public crate-it7259-0.0.1 (crate (name "it7259") (vers "0.0.1") (deps (list (crate-dep (name "defmt") (req "^0.3.4") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "embedded-hal-02") (req "^0.2") (features (quote ("unproven"))) (default-features #t) (kind 0) (package "embedded-hal")) (crate-dep (name "embedded-hal-1") (req "^1.0.0-alpha.10") (default-features #t) (kind 0) (package "embedded-hal")))) (hash "0wzvr1za3702jfypkgx3bhiabyf4hgqzpwiiyzd047czcb6igwsr")))

