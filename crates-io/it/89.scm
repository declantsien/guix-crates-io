(define-module (crates-io it #{89}#) #:use-module (crates-io))

(define-public crate-it8951-0.1 (crate (name "it8951") (vers "0.1.0") (deps (list (crate-dep (name "embedded-graphics") (req "^0.8.0") (default-features #t) (kind 0)) (crate-dep (name "embedded-hal") (req "^0.2.7") (features (quote ("unproven"))) (default-features #t) (kind 0)) (crate-dep (name "linux-embedded-hal") (req "^0.3.2") (default-features #t) (kind 2)))) (hash "1liagdrwsxhii337zd527052mwknzzqwi4rgza387f0hlyv2ia70")))

(define-public crate-it8951-0.2 (crate (name "it8951") (vers "0.2.0") (deps (list (crate-dep (name "embedded-graphics") (req "^0.8.1") (default-features #t) (kind 2)) (crate-dep (name "embedded-graphics-core") (req "^0.4.0") (default-features #t) (kind 0)) (crate-dep (name "embedded-hal") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "linux-embedded-hal") (req "^0.4") (default-features #t) (kind 2)))) (hash "1px01dl6xglpxnx1ayh7byma0b4xwadp0wqyvm2xmkca5xqfmv41")))

