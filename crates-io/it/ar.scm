(define-module (crates-io it ar) #:use-module (crates-io))

(define-public crate-itars-0.1 (crate (name "itars") (vers "0.1.0") (deps (list (crate-dep (name "itars-core") (req "^0.1") (default-features #t) (kind 0)))) (hash "162wknfi8i9pw2l46zmk2wi505fymz8a0vzy4ajqzl2xqk4f9hp3")))

(define-public crate-itars-core-0.1 (crate (name "itars-core") (vers "0.1.0") (deps (list (crate-dep (name "serde") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "toml") (req "^0.5") (default-features #t) (kind 0)))) (hash "03ilc2gfzpbv45j69gycblvs2c511sychn2rm2bdii8iih2cb7bc")))

