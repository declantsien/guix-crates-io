(define-module (crates-io it el) #:use-module (crates-io))

(define-public crate-itelem-0.1 (crate (name "itelem") (vers "0.1.0") (deps (list (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_yaml") (req "^0.9") (default-features #t) (kind 0)))) (hash "1x5niyr18pwz168gpbq4yjq6mdpgf4bj9w61g24491kl17qqjwli")))

(define-public crate-itelem-0.1 (crate (name "itelem") (vers "0.1.1") (deps (list (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_yaml") (req "^0.9") (default-features #t) (kind 0)))) (hash "1vly2wgyllrv8iwk3avf5xcrs18lpl16x8p7hjcbpx0czl2bhdx1")))

(define-public crate-itelem-0.1 (crate (name "itelem") (vers "0.1.2") (deps (list (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (kind 0)) (crate-dep (name "serde_yaml") (req "^0.9") (default-features #t) (kind 0)))) (hash "1n1bzqhaxva302ds4b0jf97gzpplmjb880z8s18zww0vsrvhpfc7")))

(define-public crate-itelem-0.1 (crate (name "itelem") (vers "0.1.3") (deps (list (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (kind 0)) (crate-dep (name "serde_yaml") (req "^0.9") (default-features #t) (kind 0)))) (hash "0ma3n4vbdx049yvkdkgh9vmvv78r7qy79vgabdxyss9na53wxcj5")))

(define-public crate-itelem-0.1 (crate (name "itelem") (vers "0.1.4") (deps (list (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (kind 0)) (crate-dep (name "serde_yaml") (req "^0.9") (default-features #t) (kind 0)))) (hash "1i37qpxbbbrdac5i2r7rmln192wr7vpl44pi60j7yhhdjhrhmsn6")))

(define-public crate-itelem-0.1 (crate (name "itelem") (vers "0.1.5") (deps (list (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (kind 0)) (crate-dep (name "serde_yaml") (req "^0.9") (default-features #t) (kind 0)) (crate-dep (name "yore") (req "^1.0.2") (default-features #t) (kind 0)))) (hash "03hk0i1gk0sjp98bfld1pwapnwvlafw8877nbd4gmdwgpbbpqqr9")))

