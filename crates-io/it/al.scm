(define-module (crates-io it al) #:use-module (crates-io))

(define-public crate-italian_energy_prices-0.1 (crate (name "italian_energy_prices") (vers "0.1.0") (deps (list (crate-dep (name "calamine") (req "^0.18") (default-features #t) (kind 0)))) (hash "03bm90y3ib6sqf6l8bysmyibw7gl9m37qdfsipfxfkdr045vhjkv")))

(define-public crate-italian_energy_prices-0.1 (crate (name "italian_energy_prices") (vers "0.1.1") (deps (list (crate-dep (name "calamine") (req "^0.18") (default-features #t) (kind 0)))) (hash "0f84xwfr1qs4c4yyij6j0i7ndwpn6b4bb3sivcisgvq2v9h8hphq")))

(define-public crate-italian_energy_prices-0.1 (crate (name "italian_energy_prices") (vers "0.1.2") (deps (list (crate-dep (name "calamine") (req "^0.19") (default-features #t) (kind 0)))) (hash "15fyj5ng2c0mrzf63b15mm2d63n5jz1pxp1pb8nqhxf65i5y6bmc")))

(define-public crate-italian_energy_prices-0.1 (crate (name "italian_energy_prices") (vers "0.1.3") (deps (list (crate-dep (name "calamine") (req "^0.20") (default-features #t) (kind 0)))) (hash "0vdrxi1mvshj20fad99yxqskzc5p7skxjrsnqsg3w4d41klbhp8c")))

(define-public crate-italian_energy_prices-0.1 (crate (name "italian_energy_prices") (vers "0.1.4") (deps (list (crate-dep (name "calamine") (req "^0.22") (default-features #t) (kind 0)))) (hash "1m65b3lvjk0m87kmzw6n64m24ipn1gjyp5q58wp74zcavgvnycxs")))

(define-public crate-italian_energy_prices-0.1 (crate (name "italian_energy_prices") (vers "0.1.5") (deps (list (crate-dep (name "calamine") (req "^0.24") (default-features #t) (kind 0)))) (hash "1cq5bm9d8154mh1xzgy6qdxaj3k6bwnmij5m4cnrg5vxicfrsx52")))

(define-public crate-italian_numbers-0.1 (crate (name "italian_numbers") (vers "0.1.0") (deps (list (crate-dep (name "regex") (req "^1.10.4") (default-features #t) (kind 0)))) (hash "11qv5dbyn0z86jzdfmnm7dyyf9j6xwcwbbvvwdknijiqsb0wg8rm") (rust-version "1.78.0")))

(define-public crate-italo-api-0.0.1 (crate (name "italo-api") (vers "0.0.1") (deps (list (crate-dep (name "anyhow") (req "^1.0.75") (default-features #t) (kind 0)) (crate-dep (name "derive-new") (req "^0.5.9") (default-features #t) (kind 0)) (crate-dep (name "getset") (req "^0.1.2") (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.11.20") (features (quote ("json"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.188") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.107") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.32.0") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "1g8cb3cnppb2ix6rkrw455z1w0n9626s61nh68rd5j0vv7ddyh45")))

(define-public crate-italo-api-0.0.2 (crate (name "italo-api") (vers "0.0.2") (deps (list (crate-dep (name "anyhow") (req "^1.0.75") (default-features #t) (kind 0)) (crate-dep (name "chrono") (req "^0.4.31") (default-features #t) (kind 0)) (crate-dep (name "derive-new") (req "^0.5.9") (default-features #t) (kind 0)) (crate-dep (name "getset") (req "^0.1.2") (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.11.20") (features (quote ("json"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.188") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.107") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.32.0") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "0drja5jsrv9z8f9qzmlkv4cbrcqf4vjabxnf54chg4fw2f5radlb")))

