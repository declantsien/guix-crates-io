(define-module (crates-io it oa) #:use-module (crates-io))

(define-public crate-itoa-0.1 (crate (name "itoa") (vers "0.1.0") (hash "0avhjk310kpl5rf0ff2yvg5qcg26hf666ynhb8bipbfkd8l91qwg")))

(define-public crate-itoa-0.1 (crate (name "itoa") (vers "0.1.1") (hash "18g7p2hrb3dk84z3frfgmszfc9hjb4ps9vp99qlb1kmf9gm8hc5f")))

(define-public crate-itoa-0.2 (crate (name "itoa") (vers "0.2.0") (hash "0bq2b0x919b080yxfp8ff65gwqmdncg1ix0nh3pz4xf7adv9x3zz")))

(define-public crate-itoa-0.2 (crate (name "itoa") (vers "0.2.1") (hash "0dl6fw90jdckx3faik2ziq76ksyskfc1xl6smlk1b9gaxp6sqdsm")))

(define-public crate-itoa-0.3 (crate (name "itoa") (vers "0.3.0") (hash "1mdqbfhwfx54l0rap357jgcc661qbqssvi7y2zl7s1l7qp19vzci")))

(define-public crate-itoa-0.3 (crate (name "itoa") (vers "0.3.1") (hash "0715dhfb2pscd2467as4isnv5v770n2j96792fn9mzb6pi7l0bzb")))

(define-public crate-itoa-0.3 (crate (name "itoa") (vers "0.3.2") (hash "147vllb74mqrnar5psdc5jjaab6czjvqlydrlab89hnv3g5gck7p")))

(define-public crate-itoa-0.3 (crate (name "itoa") (vers "0.3.3") (hash "1xp188lcf1dcq04axwp8s3vwgc2q7kw5qmgxr7dpgvf289s2a5xc")))

(define-public crate-itoa-0.3 (crate (name "itoa") (vers "0.3.4") (hash "136vwi6l2k1vrlvfx49lhficj813pk88xrcx1q3axqh1mwms6943") (features (quote (("i128"))))))

(define-public crate-itoa-0.4 (crate (name "itoa") (vers "0.4.0") (hash "11wmaj7rfg47lnc9s1cbm8dw8w941a6swy37ywvrqy4gfxhdzacj") (features (quote (("std") ("i128") ("default" "std"))))))

(define-public crate-itoa-0.4 (crate (name "itoa") (vers "0.4.1") (hash "10n6y9nrmzya7c0h1dqn7mf79zvk8zz5vrb6c5cmmjp1c7nbnsf0") (features (quote (("std") ("i128") ("default" "std"))))))

(define-public crate-itoa-0.4 (crate (name "itoa") (vers "0.4.2") (hash "01i6wrldqr0gnq287ddp4ip4h8zf4qr5zl8bbxmph7fdimaminss") (features (quote (("std") ("i128") ("default" "std"))))))

(define-public crate-itoa-0.4 (crate (name "itoa") (vers "0.4.3") (hash "02z6pfmppv4n5zcnz2aydqijvmgvg4fd6wr3s4q0xwsi953g61hk") (features (quote (("std") ("i128") ("default" "std"))))))

(define-public crate-itoa-0.4 (crate (name "itoa") (vers "0.4.4") (hash "0zvg2d9qv3avhf3d8ggglh6fdyw8kkwqg3r4622ly5yhxnvnc4jh") (features (quote (("std") ("i128") ("default" "std"))))))

(define-public crate-itoa-0.4 (crate (name "itoa") (vers "0.4.5") (hash "13nxqrfnh83a7x5rw4wq2ilp8nxvwy74dxzysdg59dbxqk0agdxq") (features (quote (("std") ("i128") ("default" "std"))))))

(define-public crate-itoa-0.4 (crate (name "itoa") (vers "0.4.6") (hash "1rnpb7rr8df76gnlk07b9086cn7fc0dxxy1ghh00q6nip7bklvyw") (features (quote (("std") ("i128") ("default" "std"))))))

(define-public crate-itoa-0.4 (crate (name "itoa") (vers "0.4.7") (hash "0di7fggbknwfjcw8cgzm1dnm3ik32l2m1f7nmyh8ipmh45h069fx") (features (quote (("std") ("i128") ("default" "std"))))))

(define-public crate-itoa-0.4 (crate (name "itoa") (vers "0.4.8") (hash "1m1dairwyx8kfxi7ab3b5jc71z1vigh9w4shnhiajji9avzr26dp") (features (quote (("std") ("i128") ("default" "std"))))))

(define-public crate-itoa-1 (crate (name "itoa") (vers "1.0.0") (hash "0y53m7zlkyk8m5niwjbdwqsz05fg6vyg1pg6r4q7ns47yqffdwyj") (rust-version "1.36")))

(define-public crate-itoa-1 (crate (name "itoa") (vers "1.0.1") (hash "0d8wr2qf5b25a04xf10rz9r0pdbjdgb0zaw3xvf8k2sqcz1qzaqs") (rust-version "1.36")))

(define-public crate-itoa-1 (crate (name "itoa") (vers "1.0.2") (hash "13ap85z7slvma9c36bzi7h5j66dm5sxm4a2g7wiwxbsh826nfb0i") (rust-version "1.36")))

(define-public crate-itoa-1 (crate (name "itoa") (vers "1.0.3") (hash "0m7773c6lb61c20gp81a0pac8sh8w473m4rck0x247zyfi3gi2kc") (rust-version "1.36")))

(define-public crate-itoa-1 (crate (name "itoa") (vers "1.0.4") (deps (list (crate-dep (name "no-panic") (req "^0.1") (optional #t) (default-features #t) (kind 0)))) (hash "1k2y06z87wjxda4v751ybf7n9q4kwl9ly9jffa78vpxs3qsas5s2") (rust-version "1.36")))

(define-public crate-itoa-1 (crate (name "itoa") (vers "1.0.5") (deps (list (crate-dep (name "no-panic") (req "^0.1") (optional #t) (default-features #t) (kind 0)))) (hash "0h343sak699ci49anaa7l3p94b9kcc4ypaqwcam6qsz8p7s85mgs") (rust-version "1.36")))

(define-public crate-itoa-1 (crate (name "itoa") (vers "1.0.6") (deps (list (crate-dep (name "no-panic") (req "^0.1") (optional #t) (default-features #t) (kind 0)))) (hash "19jc2sa3wvdc29zhgbwf3bayikq4rq18n20dbyg9ahd4hbsxjfj5") (rust-version "1.36")))

(define-public crate-itoa-1 (crate (name "itoa") (vers "1.0.7") (deps (list (crate-dep (name "no-panic") (req "^0.1") (optional #t) (default-features #t) (kind 0)))) (hash "1n4z37s9h8fd3n46gcvk1l2y3x48hjp84h59r94qlgc9nbx4ian0") (rust-version "1.36")))

(define-public crate-itoa-1 (crate (name "itoa") (vers "1.0.8") (deps (list (crate-dep (name "no-panic") (req "^0.1") (optional #t) (default-features #t) (kind 0)))) (hash "0jig0fmn7bdqpb1jz3ibnlw6gdm31wyn510x0k9mninch59jmc32") (rust-version "1.36")))

(define-public crate-itoa-1 (crate (name "itoa") (vers "1.0.9") (deps (list (crate-dep (name "no-panic") (req "^0.1") (optional #t) (default-features #t) (kind 0)))) (hash "0f6cpb4yqzhkrhhg6kqsw3wnmmhdnnffi6r2xzy248gzi2v0l5dg") (rust-version "1.36")))

(define-public crate-itoa-1 (crate (name "itoa") (vers "1.0.10") (deps (list (crate-dep (name "no-panic") (req "^0.1") (optional #t) (default-features #t) (kind 0)))) (hash "0k7xjfki7mnv6yzjrbnbnjllg86acmbnk4izz2jmm1hx2wd6v95i") (rust-version "1.36")))

(define-public crate-itoa-1 (crate (name "itoa") (vers "1.0.11") (deps (list (crate-dep (name "no-panic") (req "^0.1") (optional #t) (default-features #t) (kind 0)))) (hash "0nv9cqjwzr3q58qz84dcz63ggc54yhf1yqar1m858m1kfd4g3wa9") (rust-version "1.36")))

(define-public crate-itoa-const-1 (crate (name "itoa-const") (vers "1.0.0") (hash "03h1ly4ka22mdpn0aznf9b5wyr3vbma9g6y49mifhxc6sza43rq5") (yanked #t) (rust-version "1.36")))

(define-public crate-itoa-const-1 (crate (name "itoa-const") (vers "1.0.1") (hash "1v7f4f3r3qa6wvpv9869xaqhi2rphfgps9yfrmdpjzngnj96vryr") (yanked #t) (rust-version "1.36")))

(define-public crate-itoa-const-1 (crate (name "itoa-const") (vers "1.0.5") (deps (list (crate-dep (name "no-panic") (req "^0.1") (optional #t) (default-features #t) (kind 0)))) (hash "0n46nlkaxsgwbnsxyv3ifa912fkkq5i3mhl3l3nvzklwz9cpzxn6") (yanked #t) (rust-version "1.36")))

(define-public crate-itoap-0.1 (crate (name "itoap") (vers "0.1.0") (deps (list (crate-dep (name "bencher") (req "^0.1.5") (default-features #t) (kind 2)) (crate-dep (name "itoa") (req "^0.4.6") (features (quote ("i128"))) (default-features #t) (kind 2)))) (hash "1sdh7n4h6y7ga5vygxanf1va77dywfkii2crfx1z8xdlb5blm04f") (features (quote (("std" "alloc") ("default" "std") ("alloc"))))))

(define-public crate-itoap-0.1 (crate (name "itoap") (vers "0.1.1") (deps (list (crate-dep (name "bencher") (req "^0.1.5") (default-features #t) (kind 2)) (crate-dep (name "itoa") (req "^0.4.6") (features (quote ("i128"))) (default-features #t) (kind 2)))) (hash "1i5byqiir7mar2fajah3wzkpwnxragc4z8bds52i5l4csy8bzgql") (features (quote (("std" "alloc") ("perf-inline") ("default" "std") ("alloc")))) (yanked #t)))

(define-public crate-itoap-0.1 (crate (name "itoap") (vers "0.1.2") (deps (list (crate-dep (name "bencher") (req "^0.1.5") (default-features #t) (kind 2)) (crate-dep (name "itoa") (req "^0.4.6") (features (quote ("i128"))) (default-features #t) (kind 2)))) (hash "1rvay6lg6baj4h3grf2h6qsxdag31l3abwfy416l1x0sy7iwminx") (features (quote (("std" "alloc") ("simd") ("default" "simd" "std") ("alloc"))))))

(define-public crate-itoap-0.1 (crate (name "itoap") (vers "0.1.3") (deps (list (crate-dep (name "bencher") (req "^0.1.5") (default-features #t) (kind 2)) (crate-dep (name "itoa") (req "^0.4.6") (features (quote ("i128"))) (default-features #t) (kind 2)))) (hash "00lshczfzrcnpyamlqjqzcr07jka0hwwvbc7jsq44g51ianb7icb") (features (quote (("std" "alloc") ("simd") ("default" "simd" "std") ("alloc"))))))

(define-public crate-itoap-0.1 (crate (name "itoap") (vers "0.1.4") (deps (list (crate-dep (name "rand") (req "^0.8.2") (features (quote ("small_rng"))) (kind 2)))) (hash "05xy9vn7q2znn214yyr0m82s44y7qz08kp1kzm1vffqypadyb71s") (features (quote (("std" "alloc") ("simd") ("default" "simd" "std") ("alloc"))))))

(define-public crate-itoap-0.1 (crate (name "itoap") (vers "0.1.5") (deps (list (crate-dep (name "rand") (req "^0.8.2") (features (quote ("small_rng"))) (kind 2)))) (hash "0xspww8y6dwcgszl3lcn1f7n7ghxsa9zmxyv7m2c3g46wjx1dzr2") (features (quote (("std" "alloc") ("simd") ("default" "simd" "std") ("alloc"))))))

(define-public crate-itoap-1 (crate (name "itoap") (vers "1.0.0") (deps (list (crate-dep (name "itoa") (req "^0.4.6") (features (quote ("i128"))) (kind 2)) (crate-dep (name "rand") (req "^0.8.2") (features (quote ("small_rng"))) (kind 2)))) (hash "1hxbqj3fqgqffw0db3qhka4hk736jiyn9y2hw7sc40clv7ssmiip") (features (quote (("std" "alloc" "itoa/std") ("simd") ("default" "simd" "std") ("alloc")))) (yanked #t)))

(define-public crate-itoap-1 (crate (name "itoap") (vers "1.0.1") (deps (list (crate-dep (name "itoa") (req "^0.4.6") (features (quote ("i128"))) (kind 2)) (crate-dep (name "rand") (req "^0.8.2") (features (quote ("small_rng"))) (kind 2)))) (hash "1f48gsd18kbvskwbnwszhqjpk1l4rdmahh7kaz86b432cj9g8a4h") (features (quote (("std" "alloc") ("simd") ("default" "simd" "std") ("alloc"))))))

