(define-module (crates-io it m_) #:use-module (crates-io))

(define-public crate-itm_logger-0.1 (crate (name "itm_logger") (vers "0.1.0") (deps (list (crate-dep (name "cortex-m") (req "^0.6.2") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.6") (optional #t) (kind 0)))) (hash "11xvsbbbv8glldqsnr3pc53dhg126ihvm0fab7zavslz4vr69xi8") (features (quote (("logging" "log" "cortex-m") ("default" "logging"))))))

(define-public crate-itm_logger-0.1 (crate (name "itm_logger") (vers "0.1.1") (deps (list (crate-dep (name "cortex-m") (req "^0.6.2") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.6") (optional #t) (kind 0)))) (hash "0amwx76jafpqyyjk52s1j4ywmafmcwmqxzwsggp0cw2mfcbajl4r") (features (quote (("perform-enabled-checks") ("logging" "log" "cortex-m") ("default" "logging"))))))

(define-public crate-itm_logger-0.1 (crate (name "itm_logger") (vers "0.1.2") (deps (list (crate-dep (name "cortex-m") (req "^0.6.2") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.6") (optional #t) (kind 0)))) (hash "003kmmc7qpmadya39pivkxr936gnwl7kqsw08qzyq7iwk3xlmj69") (features (quote (("perform-enabled-checks") ("logging" "log" "cortex-m") ("default" "logging"))))))

