(define-module (crates-io it o-) #:use-module (crates-io))

(define-public crate-ito-canvas-0.1 (crate (name "ito-canvas") (vers "0.1.0") (deps (list (crate-dep (name "dot-canvas") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "unicode-canvas") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "1qlhmxcxiphq201gx08d1arjrvyv2y7rhhmwi451fpimv93qpzi3")))

(define-public crate-ito-canvas-0.1 (crate (name "ito-canvas") (vers "0.1.1") (deps (list (crate-dep (name "dot-canvas") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "unicode-canvas") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "0dv0mvzcrb29ph24gjk820x5264h5kw698n0yrw8dpkxvqsb6hwn")))

