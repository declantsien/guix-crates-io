(define-module (crates-io it ty) #:use-module (crates-io))

(define-public crate-itty-0.0.1 (crate (name "itty") (vers "0.0.1") (hash "12ax4f6fww33ywb47c321h7d9ba7qd7r62nc08ziqqkxwgslpxkd") (yanked #t)))

(define-public crate-ittyr-0.1 (crate (name "ittyr") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^2.33.3") (default-features #t) (kind 0)) (crate-dep (name "colored") (req "^2.0.0") (default-features #t) (kind 0)) (crate-dep (name "nix") (req "^0.21.0") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.3") (default-features #t) (kind 0)) (crate-dep (name "url") (req "^2.2.2") (default-features #t) (kind 0)))) (hash "0jbxp6awkkzbvbf16f3ybijdyhhdkfpjjii7sd7if1mr7217xz39")))

(define-public crate-ittyr-0.1 (crate (name "ittyr") (vers "0.1.1") (deps (list (crate-dep (name "clap") (req "^2.33.3") (default-features #t) (kind 0)) (crate-dep (name "colored") (req "^2.0.0") (default-features #t) (kind 0)) (crate-dep (name "nix") (req "^0.21.0") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.3") (default-features #t) (kind 0)) (crate-dep (name "url") (req "^2.2.2") (default-features #t) (kind 0)))) (hash "0yrzppfjs16yrzisxc3rca90snizfsm241bq1d1pchadb0j0i5qf")))

