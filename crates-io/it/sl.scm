(define-module (crates-io it sl) #:use-module (crates-io))

(define-public crate-itslit-0.1 (crate (name "itslit") (vers "0.1.0") (deps (list (crate-dep (name "opener") (req "^0.4.1") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.7.3") (default-features #t) (kind 0)))) (hash "06h2028rjcwdcqj1qa9h5ld7vgplwd85wykj3may38x63s9p1afi")))

