(define-module (crates-io nu ml) #:use-module (crates-io))

(define-public crate-numlib-0.1 (crate (name "numlib") (vers "0.1.0") (deps (list (crate-dep (name "assert_approx_eq") (req "^1.1.0") (default-features #t) (kind 0)))) (hash "0ym33bgjgkdm9w5h2vblq7qn2k126krk5nqnmq1hb03kl1symkmg")))

