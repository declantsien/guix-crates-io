(define-module (crates-io nu mw) #:use-module (crates-io))

(define-public crate-numwit-0.1 (crate (name "numwit") (vers "0.1.0") (deps (list (crate-dep (name "derive_more") (req "^0.99.17") (default-features #t) (kind 0)) (crate-dep (name "num") (req "^0.4.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.152") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.38") (default-features #t) (kind 0)))) (hash "1pxy5xq78idf6s8icgskkhracmnaq5shhsjpmjlzi864my2vi4kg")))

