(define-module (crates-io nu ba) #:use-module (crates-io))

(define-public crate-nuban-1 (crate (name "nuban") (vers "1.0.0") (hash "0bkin8wd87ph2y17b1ad7pqj49v4vh4f2jgj9d421way0q337na3")))

(define-public crate-nuban-1 (crate (name "nuban") (vers "1.1.0") (hash "0y8vihvwz48vyk0dhg7zk9ccamilh1bvz8qawhgs65hvyksjggxd")))

(define-public crate-nuban_rust-0.1 (crate (name "nuban_rust") (vers "0.1.0") (hash "1xqcrv4c1x585lkzs66wii25l59v4lrhrns5br5qh68d7za4z0wa")))

(define-public crate-nuban_rust-0.1 (crate (name "nuban_rust") (vers "0.1.1") (hash "1aiwmx8xdka7dgma046kavri8bqmqq3a5bsgwsws58837z9ks05q")))

