(define-module (crates-io nu tr) #:use-module (crates-io))

(define-public crate-nutrimatic-0.1 (crate (name "nutrimatic") (vers "0.1.0") (deps (list (crate-dep (name "any_ascii") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "byteorder") (req "^1") (default-features #t) (kind 0)))) (hash "184hf7bh1d1zjvl287rfvnj3vk971npc7zlm65r7i4vhnq2vdy3y")))

(define-public crate-nutrimatic-0.1 (crate (name "nutrimatic") (vers "0.1.1") (deps (list (crate-dep (name "byteorder") (req "^1") (default-features #t) (kind 0)))) (hash "11lhr1qzip6hp240vmk57rky94ypgr8kgbpjypg05y1jb4nqwhp5")))

