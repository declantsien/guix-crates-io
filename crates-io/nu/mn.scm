(define-module (crates-io nu mn) #:use-module (crates-io))

(define-public crate-numnums-0.1 (crate (name "numnums") (vers "0.1.0") (deps (list (crate-dep (name "anyhow") (req "^1.0.52") (default-features #t) (kind 2)) (crate-dep (name "nom") (req "^7.1.0") (features (quote ("alloc"))) (default-features #t) (kind 0)))) (hash "0r00vmb9qcsfh1vb49qdakknhslk4fkli5m051ihnqirjymsj7yd")))

(define-public crate-numnums-0.1 (crate (name "numnums") (vers "0.1.1") (deps (list (crate-dep (name "anyhow") (req "^1.0.52") (default-features #t) (kind 2)) (crate-dep (name "nom") (req "^7.1.0") (features (quote ("alloc"))) (default-features #t) (kind 0)))) (hash "05qsxiajw2i3flhrjqx5cavdkm18vgkv1nlrx35kk7kaq5y9j037")))

