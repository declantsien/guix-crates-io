(define-module (crates-io nu mq) #:use-module (crates-io))

(define-public crate-numquant-0.1 (crate (name "numquant") (vers "0.1.0") (deps (list (crate-dep (name "approx") (req "^0.5.1") (default-features #t) (kind 2)) (crate-dep (name "num-traits") (req "^0.2.14") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)))) (hash "00brwz6m6rdqyp9bp00226q1z5jwlsdvpgn9sf4iqdsqfy0qakv2")))

(define-public crate-numquant-0.2 (crate (name "numquant") (vers "0.2.0") (deps (list (crate-dep (name "approx") (req "^0.5.1") (default-features #t) (kind 2)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)))) (hash "16advb1id8yrdkphrfajw7r9dp9gysnwc0qd5i1k59cssx1rx02l")))

