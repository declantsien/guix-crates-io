(define-module (crates-io nu l-) #:use-module (crates-io))

(define-public crate-nul-terminated-0.1 (crate (name "nul-terminated") (vers "0.1.0") (deps (list (crate-dep (name "fallible") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "idem") (req "^0.3") (default-features #t) (kind 0)))) (hash "03a3sllngn8b74fn77v91xp97pa57vphwpj8vshrfys00iiiqny8")))

