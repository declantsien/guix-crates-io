(define-module (crates-io nu mc) #:use-module (crates-io))

(define-public crate-numcmp-0.1 (crate (name "numcmp") (vers "0.1.0") (hash "1d9hkqv0b0dpd6j9m8lkx0l2p3fckp9j6daplzcpmcn476bm0h7y")))

(define-public crate-numconverter-1 (crate (name "numconverter") (vers "1.1.0") (deps (list (crate-dep (name "clipboard") (req "^0.5") (default-features #t) (target "cfg(not(target_os = \"linux\"))") (kind 0)) (crate-dep (name "nix") (req "^0.10") (default-features #t) (target "cfg(target_os = \"linux\")") (kind 0)) (crate-dep (name "structopt") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "x11-clipboard") (req "^0.3") (default-features #t) (target "cfg(target_os = \"linux\")") (kind 0)))) (hash "1b6kabch5h90d1k3lgbr8mpdr92cybx2130zc6yis1ay86qgv9r5") (features (quote (("fail-on-warnings"))))))

(define-public crate-numcore-1 (crate (name "numcore") (vers "1.0.0") (deps (list (crate-dep (name "itertools") (req "^0.10.3") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "num") (req "^0.4.0") (features (quote ("serde"))) (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "tuple-conv") (req "^1.0.1") (default-features #t) (kind 0)))) (hash "0hqlxddj8fhhvl62ihi8kdlh7pqx6p5lv43v3hl5hw2yk9wvhgmc") (features (quote (("serde_support" "serde")))) (yanked #t)))

(define-public crate-numcount-0.1 (crate (name "numcount") (vers "0.1.0") (hash "1v8wx287dxvw9zw0xhyxj8wna6adp78w6p9dw4mw92ykhv2v9bj0")))

