(define-module (crates-io nu td) #:use-module (crates-io))

(define-public crate-nutdb-0.0.0 (crate (name "nutdb") (vers "0.0.0") (hash "1wv0w05avv1ri24hr4pkpd2zjbdjihvnn7g546wcab6lhvynwkwj") (yanked #t)))

(define-public crate-nutdb-api-0.0.0 (crate (name "nutdb-api") (vers "0.0.0") (hash "1pkxlhszlhqh0csmrdpawdk5bvrqsw1ak13yx7i0ppbph13ah596") (yanked #t)))

(define-public crate-nutdb-api-sql-0.0.0 (crate (name "nutdb-api-sql") (vers "0.0.0") (hash "0k7fz739f3wp2xi5xghaagcz6q0pzp4r2mfzkpy4m6mrrimivx65") (yanked #t)))

(define-public crate-nutdb-core-0.0.1 (crate (name "nutdb-core") (vers "0.0.1") (hash "1h5p1nxvzy4b5cais2rv9v5f54q2pdjk31hbx309pdw9glj46bjj") (yanked #t)))

