(define-module (crates-io nu mt) #:use-module (crates-io))

(define-public crate-numtest-0.1 (crate (name "numtest") (vers "0.1.0") (deps (list (crate-dep (name "nalgebra") (req "^0.32.5") (default-features #t) (kind 2)) (crate-dep (name "ndarray") (req "^0.15.6") (default-features #t) (kind 2)) (crate-dep (name "num-traits") (req "^0.2.18") (default-features #t) (kind 0)))) (hash "0x00qbw9x7ims4hx90qpgm3sfgzw71a8qywf3wix731ahnhl3k82")))

(define-public crate-numtest-0.1 (crate (name "numtest") (vers "0.1.1") (deps (list (crate-dep (name "nalgebra") (req "^0.32.5") (default-features #t) (kind 2)) (crate-dep (name "ndarray") (req "^0.15.6") (default-features #t) (kind 2)) (crate-dep (name "num-traits") (req "^0.2.18") (default-features #t) (kind 0)))) (hash "0la0mr53sxd5h167mmcfldzhb2hnfn605ia7d3nxlabwfimndf4a")))

(define-public crate-numtest-0.1 (crate (name "numtest") (vers "0.1.2") (deps (list (crate-dep (name "nalgebra") (req "^0.32.5") (default-features #t) (kind 2)) (crate-dep (name "ndarray") (req "^0.15.6") (default-features #t) (kind 2)) (crate-dep (name "num-traits") (req "^0.2.18") (default-features #t) (kind 0)))) (hash "0yyyj4y3hmf1d1yv8lxxk4sih60sf9wvcrafp6pky6hkshcdkh4b")))

(define-public crate-numtest-0.1 (crate (name "numtest") (vers "0.1.3") (deps (list (crate-dep (name "nalgebra") (req "^0.32.5") (default-features #t) (kind 2)) (crate-dep (name "ndarray") (req "^0.15.6") (default-features #t) (kind 2)) (crate-dep (name "num-traits") (req "^0.2.18") (default-features #t) (kind 0)))) (hash "1vrlc27lfy8warkka4q7svsgvzkwhvgdkhbwf005vpl0m9r34wgg")))

(define-public crate-numtoa-0.0.1 (crate (name "numtoa") (vers "0.0.1") (hash "1si45jb0zf0p5rsy9pccvhyycb06czqpric0cc7vnng9jgrygljl") (yanked #t)))

(define-public crate-numtoa-0.0.2 (crate (name "numtoa") (vers "0.0.2") (hash "0rsv5rnq6jhz9wvrzsbhy8mgcnl3jfvhkr5wygmbnxcqb2pbwqjw") (yanked #t)))

(define-public crate-numtoa-0.0.3 (crate (name "numtoa") (vers "0.0.3") (hash "1yrgddn23dgh4ff05kw9cfmf9fcndz84ai1bnfbqd1kvsm5x9i25") (yanked #t)))

(define-public crate-numtoa-0.0.4 (crate (name "numtoa") (vers "0.0.4") (hash "19pw6wqcg3lbyrw4p18zvf1q3acg6yn7dsymm6f5rwlkx5magdg1") (yanked #t)))

(define-public crate-numtoa-0.0.5 (crate (name "numtoa") (vers "0.0.5") (hash "1f6xccg0dhl0gi4pzb2asdcqxsb1gc1rffvkwxj74sq4q4jb5dy9")))

(define-public crate-numtoa-0.0.6 (crate (name "numtoa") (vers "0.0.6") (hash "1slcg67pm5ka61r3q5h4z4mlbrh2f7wpbaxkzzrl70wlxmnwdx24")))

(define-public crate-numtoa-0.0.7 (crate (name "numtoa") (vers "0.0.7") (hash "13wg769b3crrq0x2vk6s3zww8fpkqfqbsl7wih92vfhyvw4wp3sw")))

(define-public crate-numtoa-0.1 (crate (name "numtoa") (vers "0.1.0") (hash "1vs9rhggqbql1p26x8nkha1j06wawwgb2jp5fs88b5gi7prvvy5q") (features (quote (("std"))))))

(define-public crate-numtoa-0.2 (crate (name "numtoa") (vers "0.2.0") (hash "0sndjpkgcxk7414bc2g8wdpcvhic6y0j5hy4378gkg5fk41xbfzj") (features (quote (("std")))) (yanked #t)))

(define-public crate-numtoa-0.2 (crate (name "numtoa") (vers "0.2.1") (hash "1iprnwins1z3pb1nrlj7mj15ffxf5ab6bl0rs62kqi6hfgbrf2d2") (yanked #t)))

(define-public crate-numtoa-0.2 (crate (name "numtoa") (vers "0.2.2") (hash "15nf5sjky1kadjymjzpavwpmrfry35yj3za9cf9mzzxb5grl63w5") (yanked #t)))

(define-public crate-numtoa-0.2 (crate (name "numtoa") (vers "0.2.3") (hash "1c10ndd0wk48blhljqhlwrpni23246jzkg9abpxc3cm0xynvc8g5")))

(define-public crate-numtoa-0.2 (crate (name "numtoa") (vers "0.2.4") (hash "03yhkhjb3d1zx22m3pgcbpk8baj0zzvaxqc25c584sdq77jw98ka")))

(define-public crate-numtostr-0.1 (crate (name "numtostr") (vers "0.1.0") (hash "1xsggcw38yg2vkkvbwnwwvnqsnk6xqi3paqlyv4gff1xqjg1dkhq")))

(define-public crate-numtraits-0.0.1 (crate (name "numtraits") (vers "0.0.1") (hash "1029x51j2bq2h9b2pk0ivrz4b2v30hm70wbv3j1vpzwzsrbrizj9")))

