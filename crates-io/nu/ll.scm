(define-module (crates-io nu ll) #:use-module (crates-io))

(define-public crate-NULL-1 (crate (name "NULL") (vers "1.0.0") (deps (list (crate-dep (name "num") (req "^0.1.36") (features (quote ("bigint"))) (kind 0)) (crate-dep (name "primal-sieve") (req "^0.2.6") (default-features #t) (kind 0)))) (hash "06d9r7akkd11f2cvysfwpf2zw4a95vjzbs0jij27yjrrabnkn5m1")))

(define-public crate-NULL-1 (crate (name "NULL") (vers "1.0.1") (deps (list (crate-dep (name "num-bigint") (req "^0.1.35") (kind 0)) (crate-dep (name "num-integer") (req "^0.1.32") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.1.36") (default-features #t) (kind 0)) (crate-dep (name "primal-sieve") (req "^0.2.6") (default-features #t) (kind 0)))) (hash "17kmfvlywkznf45bzvas6b1f400ai50fwgwxgrnp62nj9kr4lsg8")))

(define-public crate-NULL-1 (crate (name "NULL") (vers "1.1.0") (deps (list (crate-dep (name "num-bigint") (req "^0.3.0") (features (quote ("std"))) (kind 0)) (crate-dep (name "num-integer") (req "^0.1.43") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2.12") (default-features #t) (kind 0)) (crate-dep (name "primal-sieve") (req "^0.3.1") (default-features #t) (kind 0)))) (hash "1bhgjn4q7nxxp5b35v843p3i5z1bvr35fdh60cymy0f09qap6xiv")))

(define-public crate-NULL-1 (crate (name "NULL") (vers "1.2.0") (deps (list (crate-dep (name "num-bigint") (req "^0.4.3") (features (quote ("std"))) (kind 0)) (crate-dep (name "num-integer") (req "^0.1.43") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2.12") (default-features #t) (kind 0)) (crate-dep (name "primal-sieve") (req "^0.3.1") (default-features #t) (kind 0)))) (hash "1l1rlm6iz3s8xjj36qkl716pdpsrc92bcqib4b087y0l2j7r1kpc")))

(define-public crate-null-engine-0.0.0 (crate (name "null-engine") (vers "0.0.0") (hash "0givdzwkaq46lwi6jm2wl31i64c5wpgxyb44768vpjnznxj39lzn")))

(define-public crate-null-engine-0.1 (crate (name "null-engine") (vers "0.1.0") (deps (list (crate-dep (name "block-reward") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "common-types") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "enjen") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "mashina") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "vapjson") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "vapory-types") (req "^0.8.0") (default-features #t) (kind 0)))) (hash "1mq0nyf9rzd891ib88syaks6v823k3xys12jvljxlrlrrrljkx2i")))

(define-public crate-null-kane-0.1 (crate (name "null-kane") (vers "0.1.0") (deps (list (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)))) (hash "0jgmdxcv8xjfmagbdjj6zph9j3z0x5wb0ls3ry6jbjr5vzxl6php") (v 2) (features2 (quote (("serde" "dep:serde"))))))

(define-public crate-null-kane-0.1 (crate (name "null-kane") (vers "0.1.1") (deps (list (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)))) (hash "063lqmmqvacszy04wm4csqkfr8d9n6pd6jn4mgpnqjsixyi3qzym") (v 2) (features2 (quote (("serde" "dep:serde"))))))

(define-public crate-null-kane-0.1 (crate (name "null-kane") (vers "0.1.2") (deps (list (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)))) (hash "00jg2kidyjhyy365gd5aydw8p093y692gzn4d63d35b13v9d07rm") (v 2) (features2 (quote (("serde" "dep:serde"))))))

(define-public crate-null-kane-0.1 (crate (name "null-kane") (vers "0.1.3") (deps (list (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)))) (hash "1rlwr1qx1axpgwag8c2s3b1c2zfnghcngz2ym9j6lnl4z683qnl3") (v 2) (features2 (quote (("serde" "dep:serde"))))))

(define-public crate-null-kane-0.1 (crate (name "null-kane") (vers "0.1.4") (deps (list (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)))) (hash "0jz3q3djh8iaclnhrvvfqy7vfwkg2b7imfyf2i2j2g9vg8q77f57") (v 2) (features2 (quote (("serde" "dep:serde"))))))

(define-public crate-null-kane-0.2 (crate (name "null-kane") (vers "0.2.0") (deps (list (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)))) (hash "1hxd13dia1spkih1hg8nvxqv230f4jk7ni3jnwcszb8c3wq79plw") (v 2) (features2 (quote (("serde" "dep:serde"))))))

(define-public crate-null-kane-0.2 (crate (name "null-kane") (vers "0.2.1") (deps (list (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)))) (hash "08w3ljhxrhzh95ashna1hbgzap25nlnb21xp3s4hncdidrprb2ia") (v 2) (features2 (quote (("serde" "dep:serde"))))))

(define-public crate-null-kane-0.3 (crate (name "null-kane") (vers "0.3.0") (deps (list (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)))) (hash "0zjlf8ygqn6qiirmbf1bwnibzg190b8my448gc6r0aqw8arpbwx0") (v 2) (features2 (quote (("serde" "dep:serde"))))))

(define-public crate-null-kane-1 (crate (name "null-kane") (vers "1.0.0") (deps (list (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)))) (hash "0rfbcvmyhw3v8ka2likx9gp0pvpypgnw0g9aklxsjhacvz6l1svy") (v 2) (features2 (quote (("serde" "dep:serde"))))))

(define-public crate-null-kane-1 (crate (name "null-kane") (vers "1.0.1") (deps (list (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)))) (hash "0zl3l64izmmfpg1gl8k59f5iwmlh4dj2f9linniqr46am2v8mf4m") (v 2) (features2 (quote (("serde" "dep:serde"))))))

(define-public crate-null-kane-1 (crate (name "null-kane") (vers "1.0.2") (deps (list (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)))) (hash "1fimg0pi5vyijphan8gbvpag9q17m31q20fr61hsxkrbb96fxp44") (v 2) (features2 (quote (("serde" "dep:serde"))))))

(define-public crate-null-kane-2 (crate (name "null-kane") (vers "2.0.0") (deps (list (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)))) (hash "0c0yjwsxm3jx8hjgvj5if24nxka9ifnzvzjkbg5mga96l831v0fh") (v 2) (features2 (quote (("serde" "dep:serde"))))))

(define-public crate-null-kane-2 (crate (name "null-kane") (vers "2.0.1") (deps (list (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)))) (hash "0xwj518ckl66m5fljxkh7n3vv4yzpcj6r97v9jcwck4gjyw166i1") (v 2) (features2 (quote (("serde" "dep:serde"))))))

(define-public crate-null-kane-2 (crate (name "null-kane") (vers "2.0.2") (deps (list (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)))) (hash "05qj7v4zi8gzssfa6dfg9m3yr73lnc4cgry7z7ksxyifmx5rbpr0") (v 2) (features2 (quote (("serde" "dep:serde"))))))

(define-public crate-null-kane-2 (crate (name "null-kane") (vers "2.0.3") (deps (list (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)))) (hash "1n4anh7rcvpxaw1y5fzdiv7r3qahvw07sk57x7kibvyphx6gdyq5") (v 2) (features2 (quote (("serde" "dep:serde"))))))

(define-public crate-null-kane-2 (crate (name "null-kane") (vers "2.0.4") (deps (list (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)))) (hash "0is5swmh1yaij5hh9kiswmdknbg8wjmbc2zwrpk0sivr1c96lyi7") (v 2) (features2 (quote (("serde" "dep:serde"))))))

(define-public crate-null-pointer-0.0.1 (crate (name "null-pointer") (vers "0.0.1") (hash "0ikv392701svc0kaxpcw4ia9ijmmnz7qiz8qp1pyk1yg0y9k3swc") (yanked #t)))

(define-public crate-null-pointer-0.1 (crate (name "null-pointer") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^4.4.16") (features (quote ("std" "error-context" "help" "derive"))) (kind 0)) (crate-dep (name "minreq") (req "^2.11.0") (features (quote ("https"))) (default-features #t) (kind 0)))) (hash "0wx3l8z6qfbx6id711fwnrm6jdvhhi02lgkxamcz55za5d2n3f68")))

(define-public crate-null-pointer-0.1 (crate (name "null-pointer") (vers "0.1.1") (deps (list (crate-dep (name "clap") (req "^4.4.16") (features (quote ("std" "error-context" "help" "derive"))) (kind 0)) (crate-dep (name "minreq") (req "^2.11.0") (features (quote ("https"))) (default-features #t) (kind 0)))) (hash "15nlgrvyz2wdc5glgzh2qhwa7ysfzipwval5q1m3bnkrrn5xj03z")))

(define-public crate-null-pointer-0.1 (crate (name "null-pointer") (vers "0.1.2") (deps (list (crate-dep (name "clap") (req "^4.4.16") (features (quote ("std" "error-context" "help" "derive"))) (kind 0)) (crate-dep (name "minreq") (req "^2.11.0") (features (quote ("https"))) (default-features #t) (kind 0)))) (hash "0ia5p0g0jpqyljslcjqh0nn72nflnvyfc0y0ikgnva63g6ic0wmg") (yanked #t)))

(define-public crate-null-pointer-0.2 (crate (name "null-pointer") (vers "0.2.0") (deps (list (crate-dep (name "clap") (req "^4.4.16") (features (quote ("std" "error-context" "help" "derive"))) (kind 0)) (crate-dep (name "minreq") (req "^2.11.0") (features (quote ("https"))) (default-features #t) (kind 0)))) (hash "18rbfr0hmsn1v0vh5pz3cricqxj8ix44knkyn9ng0bk04gwq3dyg")))

(define-public crate-null-pointer-0.2 (crate (name "null-pointer") (vers "0.2.1") (deps (list (crate-dep (name "clap") (req "^4.4.17") (features (quote ("std" "error-context" "help" "derive"))) (kind 0)) (crate-dep (name "minreq") (req "^2.11.0") (features (quote ("https"))) (default-features #t) (kind 0)))) (hash "10ak0zn11sm0flwg72igyhnjjb7mfmc5554anr7ncsbxg7q9kbk0")))

(define-public crate-null-pointer-0.2 (crate (name "null-pointer") (vers "0.2.2") (deps (list (crate-dep (name "clap") (req "^4.4.18") (features (quote ("std" "error-context" "help" "usage" "derive"))) (kind 0)) (crate-dep (name "minreq") (req "^2.11.0") (features (quote ("https"))) (default-features #t) (kind 0)))) (hash "193ajdjr95bpxr2mi32ymc7k4sbgm38a2ps6abdzihrzrp8ncrh9")))

(define-public crate-null-pointer-0.2 (crate (name "null-pointer") (vers "0.2.3") (deps (list (crate-dep (name "clap") (req "^4.4.18") (features (quote ("std" "error-context" "help" "usage" "derive"))) (kind 0)) (crate-dep (name "minreq") (req "^2.11.0") (features (quote ("https"))) (default-features #t) (kind 0)))) (hash "1qm3s7ashlj76265dizbkmzxmq6jqiklvx8g13m4li9h0y64rpza")))

(define-public crate-null-terminated-0.1 (crate (name "null-terminated") (vers "0.1.0") (deps (list (crate-dep (name "fallible") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "idem") (req "^0.3") (default-features #t) (kind 0)))) (hash "1kwp1wb9p87mfql26sldd3i1h0zgqnl05fp29v059bsfy6xhg97r")))

(define-public crate-null-terminated-0.2 (crate (name "null-terminated") (vers "0.2.0") (deps (list (crate-dep (name "fallible") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "idem") (req "^0.3") (default-features #t) (kind 0)))) (hash "1ny3lzcrivc9rb6qi9sqhladhalncjddvvsll9yixcs85mkn7d82")))

(define-public crate-null-terminated-0.2 (crate (name "null-terminated") (vers "0.2.1") (deps (list (crate-dep (name "fallible") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "idem") (req "^0.3.1") (default-features #t) (kind 0)))) (hash "1im9gwz3axlsgirj3sz6yb415dkrfi3vhbpdf4s2fzg4cg1fnz0n") (yanked #t)))

(define-public crate-null-terminated-0.2 (crate (name "null-terminated") (vers "0.2.2") (deps (list (crate-dep (name "fallible") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "idem") (req "^0.3.1") (default-features #t) (kind 0)))) (hash "0c801agxkq4i46pm9zg5nkm2gz8rk2zr4k8dqjvdm12dx0czhg8y") (yanked #t)))

(define-public crate-null-terminated-0.2 (crate (name "null-terminated") (vers "0.2.3") (deps (list (crate-dep (name "fallible") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "idem") (req "^0.3.1") (default-features #t) (kind 0)))) (hash "1aln9hhqd1hxjxlzlz4hvcvqc0gkqx3l0vknapyqr5bxkzsvdi01") (yanked #t)))

(define-public crate-null-terminated-0.2 (crate (name "null-terminated") (vers "0.2.4") (deps (list (crate-dep (name "fallible") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "idem") (req "^0.4.0") (default-features #t) (kind 0)))) (hash "0f322rqc395fng234bdi5whvr2nnz8qg3av7w1h3m99v8p8laj0p") (yanked #t)))

(define-public crate-null-terminated-0.2 (crate (name "null-terminated") (vers "0.2.5") (deps (list (crate-dep (name "fallible") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "idem") (req "^0.4.0") (default-features #t) (kind 0)) (crate-dep (name "quickcheck") (req "^0.4") (default-features #t) (kind 2)) (crate-dep (name "quickcheck_macros") (req "^0.4.2") (default-features #t) (kind 2)))) (hash "0c9h5mls1a08srj1lg191rszdlwv1b23kk2inkrq47sc91sp8j0a") (yanked #t)))

(define-public crate-null-terminated-0.2 (crate (name "null-terminated") (vers "0.2.6") (deps (list (crate-dep (name "fallible") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "idem") (req "^0.4.0") (default-features #t) (kind 0)) (crate-dep (name "quickcheck") (req "^0.4") (default-features #t) (kind 2)) (crate-dep (name "quickcheck_macros") (req "^0.4.2") (default-features #t) (kind 2)))) (hash "11mdkn2m0fbm5m6yha9g6fdrlbq3i7rgskn4ivsdxp5iw26y6262")))

(define-public crate-null-terminated-0.2 (crate (name "null-terminated") (vers "0.2.7") (deps (list (crate-dep (name "fallible") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "quickcheck") (req "^0.4") (default-features #t) (kind 2)) (crate-dep (name "quickcheck_macros") (req "^0.4.2") (default-features #t) (kind 2)))) (hash "03vzrjwc9d0i319p33i4f6ll682g0k4adhdc4h399c7arch9g2aw")))

(define-public crate-null-terminated-0.2 (crate (name "null-terminated") (vers "0.2.8") (deps (list (crate-dep (name "fallible") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "quickcheck") (req "^0.6") (default-features #t) (kind 2)) (crate-dep (name "quickcheck_macros") (req "^0.6") (default-features #t) (kind 2)))) (hash "19301hmj8w27s1s9g8bg6h2zahkzc8lfb8qdwjn7zl94iyxba98k")))

(define-public crate-null-terminated-0.2 (crate (name "null-terminated") (vers "0.2.9") (deps (list (crate-dep (name "fallible") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "quickcheck") (req "^0.6") (default-features #t) (kind 2)) (crate-dep (name "quickcheck_macros") (req "^0.6") (default-features #t) (kind 2)))) (hash "1m3aaynbgll88lxjy1ynavj4f9cv4x6z570qkf5m8l932ylca7db")))

(define-public crate-null-terminated-0.2 (crate (name "null-terminated") (vers "0.2.10") (deps (list (crate-dep (name "fallible") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "quickcheck") (req "^0.6") (default-features #t) (kind 2)) (crate-dep (name "quickcheck_macros") (req "^0.6") (default-features #t) (kind 2)))) (hash "0d64gsshk2ihp5l8y34av93pqpapwx56vqb4z5hds7x71iwda2rm")))

(define-public crate-null-terminated-0.3 (crate (name "null-terminated") (vers "0.3.0") (deps (list (crate-dep (name "fallible") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "quickcheck") (req "^0.6") (default-features #t) (kind 2)) (crate-dep (name "quickcheck_macros") (req "^0.6") (default-features #t) (kind 2)))) (hash "1njv0yycnx09wk84i24nkzdrxpi9bxpxazc1py0lxccdaf3v6syd")))

(define-public crate-null-terminated-0.2 (crate (name "null-terminated") (vers "0.2.11") (deps (list (crate-dep (name "null-terminated") (req "^0.3") (default-features #t) (kind 0)))) (hash "182myqc57w82drnfxgqzan97gkgpv6iqmnzwk084p98g14x91dfy")))

(define-public crate-null-terminated-0.3 (crate (name "null-terminated") (vers "0.3.1") (deps (list (crate-dep (name "fallible") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "quickcheck") (req "^0.6") (default-features #t) (kind 2)) (crate-dep (name "quickcheck_macros") (req "^0.6") (default-features #t) (kind 2)))) (hash "0q2jxnqlqzh60yw2cwxfba3hiqiy40bq3sda6b9g67lvq895gv2y")))

(define-public crate-null-terminated-0.3 (crate (name "null-terminated") (vers "0.3.2") (deps (list (crate-dep (name "fallible") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "quickcheck") (req "^0.6") (default-features #t) (kind 2)) (crate-dep (name "quickcheck_macros") (req "^0.6") (default-features #t) (kind 2)))) (hash "05zalacgm4pwf2frbmbc100ck1lq6y4wcm01w789s3kax9r1zbbn")))

(define-public crate-null-terminated-0.3 (crate (name "null-terminated") (vers "0.3.3") (deps (list (crate-dep (name "fallible") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "quickcheck") (req "^0.6") (default-features #t) (kind 2)) (crate-dep (name "quickcheck_macros") (req "^0.6") (default-features #t) (kind 2)) (crate-dep (name "unreachable") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "utf") (req "^0.1") (optional #t) (default-features #t) (kind 0)))) (hash "1q3kmkr25bkinq19bc3fgywrl00yv3kdvpzs5zpdaxyg7fqhp43s") (features (quote (("default" "utf"))))))

(define-public crate-null-terminated-0.3 (crate (name "null-terminated") (vers "0.3.4") (deps (list (crate-dep (name "fallible") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "quickcheck") (req "^0.6") (default-features #t) (kind 2)) (crate-dep (name "quickcheck_macros") (req "^0.6") (default-features #t) (kind 2)) (crate-dep (name "unreachable") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "utf") (req "^0.1") (optional #t) (default-features #t) (kind 0)))) (hash "1ln1aqy5lmcmsw1qss06md7sr3726wlk7qdv42gnp2qr6bds53n2") (features (quote (("default" "utf"))))))

(define-public crate-null-terminated-0.3 (crate (name "null-terminated") (vers "0.3.5") (deps (list (crate-dep (name "fallible") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "quickcheck") (req "^0.7") (default-features #t) (kind 2)) (crate-dep (name "quickcheck_macros") (req "^0.7") (default-features #t) (kind 2)) (crate-dep (name "unreachable") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "utf") (req "^0.1") (optional #t) (default-features #t) (kind 0)))) (hash "06y2vq4v9fs7wnl5g7k9q94asxrsn574wj4n5islk7pmbb9vx73j") (features (quote (("default" "utf"))))))

(define-public crate-null-terminated-0.3 (crate (name "null-terminated") (vers "0.3.6") (deps (list (crate-dep (name "fallible") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "quickcheck") (req "^0.7") (default-features #t) (kind 2)) (crate-dep (name "quickcheck_macros") (req "^0.7") (default-features #t) (kind 2)) (crate-dep (name "unreachable") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "utf") (req "^0.1") (optional #t) (default-features #t) (kind 0)))) (hash "05hxyhb2sl3spl9cc33sm4pr5a63s79fhqn5rd12dlsqrkgyngc9") (features (quote (("default" "utf"))))))

(define-public crate-null-terminated-0.3 (crate (name "null-terminated") (vers "0.3.7") (deps (list (crate-dep (name "fallible") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "quickcheck") (req "^0.7") (default-features #t) (kind 2)) (crate-dep (name "quickcheck_macros") (req "^0.7") (default-features #t) (kind 2)) (crate-dep (name "system-call") (req "^0.1") (default-features #t) (kind 2)) (crate-dep (name "unreachable") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "utf") (req "^0.1") (optional #t) (default-features #t) (kind 0)))) (hash "1kacksbplz4fp04wya7y15qdkmq9llp23q14wdam0v45b08n34wg") (features (quote (("default" "utf"))))))

(define-public crate-null-terminated-0.3 (crate (name "null-terminated") (vers "0.3.8") (deps (list (crate-dep (name "fallible") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "quickcheck") (req "^0.7") (default-features #t) (kind 2)) (crate-dep (name "quickcheck_macros") (req "^0.7") (default-features #t) (kind 2)) (crate-dep (name "system-call") (req "^0.1") (default-features #t) (kind 2)) (crate-dep (name "unreachable") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "utf") (req "^0.1") (optional #t) (default-features #t) (kind 0)))) (hash "0h0pm9r7d4803xdkdfz3yxppdcv9yjzgxds4r0fr9mb77nw66ikg") (features (quote (("default" "utf"))))))

(define-public crate-null-terminated-0.3 (crate (name "null-terminated") (vers "0.3.9") (deps (list (crate-dep (name "fallible") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "quickcheck") (req "^0.7") (default-features #t) (kind 2)) (crate-dep (name "quickcheck_macros") (req "^0.7") (default-features #t) (kind 2)) (crate-dep (name "system-call") (req "^0.1") (default-features #t) (kind 2)) (crate-dep (name "unreachable") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "utf") (req "^0.1") (optional #t) (default-features #t) (kind 0)))) (hash "04qw00731lfc2jn37364kah44anp1xrpyr0rbjnbzqw5m5hy1ff3") (features (quote (("default" "utf"))))))

(define-public crate-null-terminated-0.3 (crate (name "null-terminated") (vers "0.3.10") (deps (list (crate-dep (name "fallible") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "quickcheck") (req "^0.7") (default-features #t) (kind 2)) (crate-dep (name "quickcheck_macros") (req "^0.8") (default-features #t) (kind 2)) (crate-dep (name "system-call") (req "^0.1") (default-features #t) (kind 2)) (crate-dep (name "unreachable") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "utf") (req "^0.1") (optional #t) (default-features #t) (kind 0)))) (hash "1k5x4cclcpr0qwxw4h4cvg5r4dflww7vwc3b3q8kl2ips8vck1m0") (features (quote (("default" "utf")))) (yanked #t)))

(define-public crate-null-terminated-0.3 (crate (name "null-terminated") (vers "0.3.11") (deps (list (crate-dep (name "fallible") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "quickcheck") (req "^0.8") (default-features #t) (kind 2)) (crate-dep (name "quickcheck_macros") (req "^0.8") (default-features #t) (kind 2)) (crate-dep (name "system-call") (req "^0.1") (default-features #t) (kind 2)) (crate-dep (name "unreachable") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "utf") (req "^0.1") (optional #t) (default-features #t) (kind 0)))) (hash "1xr5wskb6rrngw3y8hlyg44mnpk9zdbl571b74c2mfvmaxpxj30s") (features (quote (("default" "utf")))) (yanked #t)))

(define-public crate-null-terminated-0.3 (crate (name "null-terminated") (vers "0.3.12") (deps (list (crate-dep (name "fallible") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "quickcheck") (req "^0.8") (default-features #t) (kind 2)) (crate-dep (name "quickcheck_macros") (req "^0.8") (default-features #t) (kind 2)) (crate-dep (name "system-call") (req "^0.1") (default-features #t) (kind 2)) (crate-dep (name "unreachable") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "utf") (req "^0.1") (optional #t) (default-features #t) (kind 0)))) (hash "0hfa6f1wzfzm8ivmgr6ssw8vl2fnjm7x07p11994wf36mq9h732i") (features (quote (("default" "utf"))))))

(define-public crate-null-terminated-0.3 (crate (name "null-terminated") (vers "0.3.13") (deps (list (crate-dep (name "fallible") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "quickcheck") (req "^0.8") (default-features #t) (kind 2)) (crate-dep (name "quickcheck_macros") (req "^0.8") (default-features #t) (kind 2)) (crate-dep (name "system-call") (req "^0.1") (default-features #t) (kind 2)) (crate-dep (name "unreachable") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "utf") (req "^0.1") (optional #t) (default-features #t) (kind 0)))) (hash "0b6wbspkykhk1dnz9cr674rfd2jwhpcxffxxqd258vrhnvcrdlpi") (features (quote (("default" "utf"))))))

(define-public crate-null-terminated-0.3 (crate (name "null-terminated") (vers "0.3.14") (deps (list (crate-dep (name "quickcheck") (req "^0.8") (default-features #t) (kind 2)) (crate-dep (name "quickcheck_macros") (req "^0.8") (default-features #t) (kind 2)) (crate-dep (name "system-call") (req "^0.1") (default-features #t) (kind 2)) (crate-dep (name "unreachable") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "utf") (req "^0.1") (optional #t) (default-features #t) (kind 0)))) (hash "0bdnw3z3sv22a0mhgr6vxrngwrkryz95brbc8lzwqdnwvbfsgisc") (features (quote (("default" "utf"))))))

(define-public crate-null-terminated-0.3 (crate (name "null-terminated") (vers "0.3.15") (deps (list (crate-dep (name "quickcheck") (req "^0.8") (default-features #t) (kind 2)) (crate-dep (name "quickcheck_macros") (req "^0.8") (default-features #t) (kind 2)) (crate-dep (name "system-call") (req "^0.1") (default-features #t) (kind 2)) (crate-dep (name "unreachable") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "utf") (req "^0.1") (optional #t) (default-features #t) (kind 0)))) (hash "0gmgj9bcf7grmq6jws6scjzyk8259fg0x0j5jxffb3kigbna83z8") (features (quote (("default" "utf"))))))

(define-public crate-null-terminated-0.3 (crate (name "null-terminated") (vers "0.3.16") (deps (list (crate-dep (name "byte-strings-proc_macros") (req "^0.2.1") (default-features #t) (kind 0)) (crate-dep (name "quickcheck") (req "^0.8") (default-features #t) (kind 2)) (crate-dep (name "quickcheck_macros") (req "^0.8") (default-features #t) (kind 2)) (crate-dep (name "system-call") (req "^0.1") (default-features #t) (kind 2)) (crate-dep (name "unreachable") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "utf") (req "^0.1") (optional #t) (default-features #t) (kind 0)))) (hash "1xg67g16a9glk9mfzpjz102gis52adqm0pncsrabxxlbkdswzfdg") (features (quote (("default" "utf"))))))

(define-public crate-null-terminated-0.3 (crate (name "null-terminated") (vers "0.3.17") (deps (list (crate-dep (name "byte-strings-proc_macros") (req "^0.2.1") (default-features #t) (kind 0)) (crate-dep (name "quickcheck") (req "^0.8") (default-features #t) (kind 2)) (crate-dep (name "quickcheck_macros") (req "^0.8") (default-features #t) (kind 2)) (crate-dep (name "system-call") (req "^0.1") (default-features #t) (kind 2)) (crate-dep (name "unreachable") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "utf") (req "^0.1") (optional #t) (default-features #t) (kind 0)))) (hash "1grh9vhqs6kwnk065z4yr1p4bd0q8cy9yndkf4ax4sxa4p3b4fnv") (features (quote (("default" "utf"))))))

(define-public crate-null-terminated-str-0.1 (crate (name "null-terminated-str") (vers "0.1.0") (hash "0c9prw94wsc2aa740snp90phvps4jjwykd1l8z47x39dhqfv4svl") (rust-version "1.63")))

(define-public crate-null-terminated-str-0.1 (crate (name "null-terminated-str") (vers "0.1.1") (hash "0zsddq2rm3plq70xjbfggwfzm7hqr1y29j7dq7qbmhajqdbqj41f") (rust-version "1.63")))

(define-public crate-null-terminated-str-0.1 (crate (name "null-terminated-str") (vers "0.1.2") (hash "0n4pxq57zawfqir6w49i4x5ql0m8zwdbl9r0kchylcxp37fh2jyi") (rust-version "1.63")))

(define-public crate-null-terminated-str-0.1 (crate (name "null-terminated-str") (vers "0.1.3") (deps (list (crate-dep (name "serde") (req "^1.0.144") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde_test") (req "^1.0.144") (default-features #t) (kind 2)))) (hash "11iccixc9vlp0vz47q74lm9d18wg9pw7qx6dcc8p51iam4ap2928") (rust-version "1.63")))

(define-public crate-null-terminated-str-0.1 (crate (name "null-terminated-str") (vers "0.1.4") (deps (list (crate-dep (name "serde") (req "^1.0.144") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde_test") (req "^1.0.144") (default-features #t) (kind 2)))) (hash "1gahyaf26z2mb3gy3700nkn52n19v4g2m7gz4k7rpvw9nncc0kjr") (rust-version "1.63")))

(define-public crate-null-vec-0.1 (crate (name "null-vec") (vers "0.1.0") (hash "150pyy1lcgzkhz6gj2ihz3d1n636rr9anfzyzl5pvb6f48ixc9g2")))

(define-public crate-null_fn-0.1 (crate (name "null_fn") (vers "0.1.0") (deps (list (crate-dep (name "proc-macro2") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "171v4h3q4r8m6ph1fz449qsjwb9xfmnapwpddb21a14gp1iyqd29")))

(define-public crate-null_fn-0.1 (crate (name "null_fn") (vers "0.1.1") (deps (list (crate-dep (name "proc-macro2") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "1hbnk61744gg4zdxppwa7ag6vabc0ibfl6n2xykqs39dnp7ijn9l")))

(define-public crate-nullable-0.0.1 (crate (name "nullable") (vers "0.0.1") (hash "1jvkq3d5cnkkya2zgmrw13g0ii692qbhx8aqqp68a2v3zlk8j5zq")))

(define-public crate-nullable-result-0.1 (crate (name "nullable-result") (vers "0.1.0") (hash "1ybhin59c4h24mwjrlyr5ng5knfd0k56x4s02ypqrbp7kf22rjiv") (features (quote (("std") ("default" "std"))))))

(define-public crate-nullable-result-0.2 (crate (name "nullable-result") (vers "0.2.0") (hash "0gpp2sigvl4a3pxqj0kmjsrmff3i0102rm5x7axwn67q0f28f2ar") (features (quote (("std") ("default" "std"))))))

(define-public crate-nullable-result-0.3 (crate (name "nullable-result") (vers "0.3.0") (hash "1vn1gx07pdvd0fg87hdlinyrca8kl844qz9wxw2zhm4l437rvf6n") (features (quote (("std") ("default" "std"))))))

(define-public crate-nullable-result-0.4 (crate (name "nullable-result") (vers "0.4.0") (hash "1llx0n7agy74rhrnwm5w850fssjlqxgppi9li0j253x98nhhjl2c") (features (quote (("std") ("default" "std"))))))

(define-public crate-nullable-result-0.5 (crate (name "nullable-result") (vers "0.5.0") (hash "0zs7bik1hpkwj8v4wlpqpbi84babgmn5dnl2qh8fbrc5314b4bdj") (features (quote (("std") ("default" "std"))))))

(define-public crate-nullable-result-0.6 (crate (name "nullable-result") (vers "0.6.0") (hash "1b2flx39maiq6m77fn2xlfx4vxzap76cbjdh3c16b71pf7p03zb9") (features (quote (("std") ("default" "std"))))))

(define-public crate-nullable-result-0.7 (crate (name "nullable-result") (vers "0.7.0") (hash "16dr8vlya6xsc86m7dlsr6j7jd1v68bxds7g1d9dm7by98srwp35") (features (quote (("try_trait" "__unstable") ("std") ("default" "std") ("__unstable"))))))

(define-public crate-nullable_struct-0.1 (crate (name "nullable_struct") (vers "0.1.0") (deps (list (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (default-features #t) (kind 0)))) (hash "0zjdzzd83x5hiq7xgr8bd1rd6nw866pc78smx14011ng5yzb3lix")))

(define-public crate-nulled_auth-0.1 (crate (name "nulled_auth") (vers "0.1.0") (deps (list (crate-dep (name "getopts") (req "^0.2.21") (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.11.10") (features (quote ("json"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.137") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.79") (default-features #t) (kind 0)) (crate-dep (name "sha2") (req "^0.10.1") (default-features #t) (kind 0)) (crate-dep (name "sysinfo") (req "^0.23.9") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.17.0") (features (quote ("full"))) (default-features #t) (kind 0)) (crate-dep (name "whoami") (req "^1.2.1") (default-features #t) (kind 0)) (crate-dep (name "winreg") (req "^0.10.1") (default-features #t) (kind 0)))) (hash "0d4aiw27hnyx3m0n89kgiknzhajh4d3m0v9ldar2r61ifkz73nnm")))

(define-public crate-nulled_auth-0.1 (crate (name "nulled_auth") (vers "0.1.1") (deps (list (crate-dep (name "reqwest") (req "^0.11.10") (features (quote ("json"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.137") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.79") (default-features #t) (kind 0)) (crate-dep (name "sha2") (req "^0.10.1") (default-features #t) (kind 0)) (crate-dep (name "sysinfo") (req "^0.23.9") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.17.0") (features (quote ("full"))) (default-features #t) (kind 0)) (crate-dep (name "whoami") (req "^1.2.1") (default-features #t) (kind 0)) (crate-dep (name "winreg") (req "^0.10.1") (default-features #t) (kind 0)))) (hash "06pw7v21avmjgjjl6wbyq93q3drla222nl2b2bn8rc1gdnmwwgg9")))

(define-public crate-nulled_auth-0.1 (crate (name "nulled_auth") (vers "0.1.2") (deps (list (crate-dep (name "reqwest") (req "^0.11.10") (features (quote ("json"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.137") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.79") (default-features #t) (kind 0)) (crate-dep (name "sha2") (req "^0.10.1") (default-features #t) (kind 0)) (crate-dep (name "sysinfo") (req "^0.23.9") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.17.0") (features (quote ("full"))) (default-features #t) (kind 0)) (crate-dep (name "whoami") (req "^1.2.1") (default-features #t) (kind 0)) (crate-dep (name "winreg") (req "^0.10.1") (default-features #t) (kind 0)))) (hash "0k6qc3rgd181v1d367l15ldkqi28l519v3l0z9ds2dvr79kjd8n5")))

(define-public crate-nullnet-firewall-0.1 (crate (name "nullnet-firewall") (vers "0.1.0") (deps (list (crate-dep (name "etherparse") (req "^0.13.0") (default-features #t) (kind 0)))) (hash "0j1iyd8dvr3w11n7jbjibyky4yc6znz530wi2yrj6mnjv5wy3kaq")))

(define-public crate-nullnet-firewall-0.2 (crate (name "nullnet-firewall") (vers "0.2.0") (deps (list (crate-dep (name "chrono") (req "^0.4.31") (features (quote ("clock"))) (kind 0)) (crate-dep (name "etherparse") (req "^0.13.0") (default-features #t) (kind 0)) (crate-dep (name "rusqlite") (req "^0.30.0") (default-features #t) (kind 0)) (crate-dep (name "serial_test") (req "^2.0.0") (kind 2)))) (hash "12bfkx5vyf618ladrvgvlkn0nwsgziqsmvljivkx01fhh07dfwsv")))

(define-public crate-nullnet-firewall-0.2 (crate (name "nullnet-firewall") (vers "0.2.1") (deps (list (crate-dep (name "chrono") (req "^0.4.31") (features (quote ("clock"))) (kind 0)) (crate-dep (name "etherparse") (req "^0.13.0") (default-features #t) (kind 0)) (crate-dep (name "rusqlite") (req "^0.30.0") (default-features #t) (kind 0)) (crate-dep (name "serial_test") (req "^3.0.0") (kind 2)))) (hash "19qv6rhf36ga65i1mzqsxzc1g1q00lb794bbzw5h73vajqi6wqki")))

(define-public crate-nullnet-firewall-0.2 (crate (name "nullnet-firewall") (vers "0.2.2") (deps (list (crate-dep (name "chrono") (req "^0.4.31") (features (quote ("clock"))) (kind 0)) (crate-dep (name "etherparse") (req "^0.13.0") (default-features #t) (kind 0)) (crate-dep (name "rusqlite") (req "^0.30.0") (default-features #t) (kind 0)) (crate-dep (name "serial_test") (req "^3.0.0") (kind 2)))) (hash "1r436ld22gnf1h0y4pq68a67z72dxpa2adr35f1qm2m7fj3h9j5w")))

(define-public crate-nullterm-0.1 (crate (name "nullterm") (vers "0.1.0") (deps (list (crate-dep (name "clearscreen") (req "^1.0.9") (default-features #t) (kind 0)))) (hash "0l8qml5kj8pzvbp8391cg7kkfhxckamm1ndqlfsihj6zpmqimiq8")))

(define-public crate-nulltermarrayiter-0.1 (crate (name "nulltermarrayiter") (vers "0.1.0") (hash "0rhyy5f36z69ghgi4b1j39kizd5q4dm4snqlb44cz9552k0ysnk2")))

(define-public crate-nulltermarrayiter-0.1 (crate (name "nulltermarrayiter") (vers "0.1.1") (hash "1jdy7xwlr0qdd6wq5z1j1xi8cxw0mwf842sgg9b1gyv58snw7285")))

(define-public crate-nullvec-0.1 (crate (name "nullvec") (vers "0.1.0") (deps (list (crate-dep (name "num") (req "^0.1.36") (default-features #t) (kind 0)) (crate-dep (name "rustc-serialize") (req "^0.3.21") (default-features #t) (kind 0)))) (hash "1j4w5sckipv4ss8axjpxal81wp0hzsym9w13fy9f1sb2h1wz54n7")))

(define-public crate-nullvec-0.1 (crate (name "nullvec") (vers "0.1.1") (deps (list (crate-dep (name "num") (req "^0.1.36") (default-features #t) (kind 0)) (crate-dep (name "rustc-serialize") (req "^0.3.21") (default-features #t) (kind 0)))) (hash "01dz607msawzws817a92a45f4jif8jdxhs6577fnqyhzjslifa8f")))

(define-public crate-nullvec-0.1 (crate (name "nullvec") (vers "0.1.2") (deps (list (crate-dep (name "num") (req "^0.1.36") (default-features #t) (kind 0)) (crate-dep (name "rustc-serialize") (req "^0.3.21") (default-features #t) (kind 0)))) (hash "0y2bl9zxvpwfnad0s25nyckgiacwgchvqqmpl9akg98p03xq360c")))

(define-public crate-nullvec-0.1 (crate (name "nullvec") (vers "0.1.3") (deps (list (crate-dep (name "num") (req "^0.1.36") (default-features #t) (kind 0)) (crate-dep (name "rustc-serialize") (req "^0.3.21") (default-features #t) (kind 0)))) (hash "18pslx0m2qrdf9y302c192xh0z28xvx8gcn5s0s1ymkz7mcwqqlv")))

(define-public crate-nullvec-0.1 (crate (name "nullvec") (vers "0.1.4") (deps (list (crate-dep (name "num") (req "^0.1.36") (default-features #t) (kind 0)) (crate-dep (name "rustc-serialize") (req "^0.3.21") (default-features #t) (kind 0)))) (hash "04fszm093vydys76pssf2cqxz5qbwa0wd6j64zdqmwx2klwfcg2w")))

(define-public crate-nullvec-0.1 (crate (name "nullvec") (vers "0.1.5") (deps (list (crate-dep (name "num") (req "^0.1.36") (default-features #t) (kind 0)) (crate-dep (name "rustc-serialize") (req "^0.3.21") (default-features #t) (kind 0)))) (hash "0k19p08cc5rh2ii1m5krcpz63zx9wvcj20hr50pdd78hyv36qrmf")))

(define-public crate-nullvec-0.2 (crate (name "nullvec") (vers "0.2.0") (deps (list (crate-dep (name "num") (req "^0.1.40") (default-features #t) (kind 0)))) (hash "1g3ymmwv441pzcyrrgkkr9is60qn7sq6bd8yr26dprfl95w4fr93")))

