(define-module (crates-io nu -m) #:use-module (crates-io))

(define-public crate-nu-macros-0.7 (crate (name "nu-macros") (vers "0.7.0") (deps (list (crate-dep (name "nu-protocol") (req "^0.7.0") (default-features #t) (kind 0)))) (hash "1sjv4ayv0760vklvjzmkvqg0fdcpikz253b3d93w53yz3ipmqbvv")))

(define-public crate-nu-macros-0.8 (crate (name "nu-macros") (vers "0.8.0") (deps (list (crate-dep (name "nu-protocol") (req "^0.8.0") (default-features #t) (kind 0)))) (hash "13jrlbbx4f5bviz3awacmq8nqld2rwpv0n2daarmcaqh6k447jjd")))

(define-public crate-nu-macros-0.9 (crate (name "nu-macros") (vers "0.9.0") (deps (list (crate-dep (name "nu-protocol") (req "^0.9.0") (default-features #t) (kind 0)))) (hash "1i3pvglqbc34vl7yn0kv4az2092izjd7c025rxi3ff48lh9dc1h4")))

(define-public crate-nu-macros-0.10 (crate (name "nu-macros") (vers "0.10.0") (deps (list (crate-dep (name "nu-protocol") (req "^0.10.0") (default-features #t) (kind 0)))) (hash "0mrwzcwlqml9yjcf9sk04i5cwkqrpl3xd3rmqcqf9dp83wbr9mz6")))

(define-public crate-nu-macros-0.11 (crate (name "nu-macros") (vers "0.11.0") (deps (list (crate-dep (name "nu-protocol") (req "^0.11.0") (default-features #t) (kind 0)))) (hash "0k2bbshrhxg3w43s86kwyqf10xiibx3hlcwxdz3g06xfq0kxah0n")))

(define-public crate-nu-macros-0.12 (crate (name "nu-macros") (vers "0.12.0") (deps (list (crate-dep (name "nu-protocol") (req "^0.12.0") (default-features #t) (kind 0)))) (hash "1y060sf8y05ic25d46aginkkw5wqbjjqx1hm9gn4iblsif53sa02")))

