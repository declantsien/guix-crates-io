(define-module (crates-io nu it) #:use-module (crates-io))

(define-public crate-nuit-0.0.3 (crate (name "nuit") (vers "0.0.3") (deps (list (crate-dep (name "nuit-bridge-swiftui") (req "^0.0.3") (default-features #t) (target "cfg(target_os = \"macos\")") (kind 0)) (crate-dep (name "nuit-core") (req "^0.0.3") (default-features #t) (kind 0)) (crate-dep (name "nuit-derive") (req "^0.0.3") (default-features #t) (kind 0)))) (hash "0yd4jqvmgbianvnl7zj2ayh85fmdzhn1dr8ags0bbvb55r9npb9k")))

(define-public crate-nuit-0.0.4 (crate (name "nuit") (vers "0.0.4") (deps (list (crate-dep (name "nuit-bridge-swiftui") (req "^0.0.4") (default-features #t) (target "cfg(target_os = \"macos\")") (kind 0)) (crate-dep (name "nuit-core") (req "^0.0.4") (default-features #t) (kind 0)) (crate-dep (name "nuit-derive") (req "^0.0.4") (default-features #t) (kind 0)))) (hash "0wk3gxacl7701la8b3k3jn53rq0lw3kcr8zbg9qcgqrr0n4hc98g")))

(define-public crate-nuit-bridge-swiftui-0.0.1 (crate (name "nuit-bridge-swiftui") (vers "0.0.1") (deps (list (crate-dep (name "nuit-core") (req "^0.0.1") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 1)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 1)))) (hash "0m8z3jl70m4m3qal1hzkkbjhghfdkb34gyxyz3050wv61q5l2awj")))

(define-public crate-nuit-bridge-swiftui-0.0.2 (crate (name "nuit-bridge-swiftui") (vers "0.0.2") (deps (list (crate-dep (name "nuit-core") (req "^0.0.2") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 1)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 1)))) (hash "0fzh1wdk1j9mgya2i5v2pk04dkza8akgmh46rv0lp6bk66f27riw")))

(define-public crate-nuit-bridge-swiftui-0.0.3 (crate (name "nuit-bridge-swiftui") (vers "0.0.3") (deps (list (crate-dep (name "nuit-core") (req "^0.0.3") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 1)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 1)))) (hash "16qna8r4lfh3hxgg37cijdmplps76iz9nr0qbvbdhmg5c4i4pbkd")))

(define-public crate-nuit-bridge-swiftui-0.0.4 (crate (name "nuit-bridge-swiftui") (vers "0.0.4") (deps (list (crate-dep (name "nuit-core") (req "^0.0.4") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 1)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 1)))) (hash "1y0ah67zqrp77mqywq1l3z1lh6ijx98bqhcs3f7paaaqfs3p91k5")))

(define-public crate-nuit-core-0.0.1 (crate (name "nuit-core") (vers "0.0.1") (deps (list (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)))) (hash "0s1j0dqmkjk71bysanxgk43v60qw6l90pqgaxcdxknyk9gzsvhpr")))

(define-public crate-nuit-core-0.0.2 (crate (name "nuit-core") (vers "0.0.2") (deps (list (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)))) (hash "1wds0bwixw3lwi33c9xfw31j3pd0l7w6klgkzqszn37f8hygbxlv")))

(define-public crate-nuit-core-0.0.3 (crate (name "nuit-core") (vers "0.0.3") (deps (list (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)))) (hash "0n37fcn8rry87gnz8hkwcadhh8fgj46bzr2pwg42wyya4lfxa6b7")))

(define-public crate-nuit-core-0.0.4 (crate (name "nuit-core") (vers "0.0.4") (deps (list (crate-dep (name "ref-cast") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)))) (hash "0pyrmh2wd98r1hrwapc7r6g7i7sm9m5i7azyl61mrj1gqqq9yzjl")))

(define-public crate-nuit-derive-0.0.3 (crate (name "nuit-derive") (vers "0.0.3") (deps (list (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2.0") (default-features #t) (kind 0)))) (hash "0dfndznjl6gr5jyvprilipxnj8j4b49kzg0x8jl6n3w6fx0574rs")))

(define-public crate-nuit-derive-0.0.4 (crate (name "nuit-derive") (vers "0.0.4") (deps (list (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2.0") (default-features #t) (kind 0)))) (hash "1qa3r7f9fg3l387m99r2hag62a3wlbm9nzxg5m8njy8qrr7vh3nk")))

