(define-module (crates-io nu gi) #:use-module (crates-io))

(define-public crate-nugine-rust-utils-0.1 (crate (name "nugine-rust-utils") (vers "0.1.0") (hash "1lqp3ispkq4b3i1qlwg0jr4n186czb94hjl2fxsrzs2zjkqds47s") (features (quote (("std" "alloc") ("default") ("alloc")))) (yanked #t)))

(define-public crate-nugine-rust-utils-0.1 (crate (name "nugine-rust-utils") (vers "0.1.1") (hash "1c4503j5x35zx0zgq16wbsv1kf6sashavczykmw7v9g3hzwcr7nm") (features (quote (("std" "alloc") ("default") ("alloc")))) (yanked #t)))

(define-public crate-nugine-rust-utils-0.1 (crate (name "nugine-rust-utils") (vers "0.1.2") (deps (list (crate-dep (name "simdutf8") (req "^0.1.4") (optional #t) (kind 0)))) (hash "15k4ayphsccnj99x2p7saj2q6q3z1d24r00ycqfr5pgdz9y6g8cr") (features (quote (("default" "std") ("ascii") ("alloc")))) (yanked #t) (v 2) (features2 (quote (("utf8" "dep:simdutf8") ("std" "alloc" "simdutf8?/std"))))))

(define-public crate-nugine-rust-utils-0.1 (crate (name "nugine-rust-utils") (vers "0.1.3") (deps (list (crate-dep (name "simdutf8") (req "^0.1.4") (optional #t) (kind 0)))) (hash "0vgp6czj9a2c0xcbyx0lwddgmcirmrbfiidhkz7zwygshf3az22k") (features (quote (("default" "std") ("ascii") ("alloc")))) (yanked #t) (v 2) (features2 (quote (("utf8" "dep:simdutf8") ("std" "alloc" "simdutf8?/std"))))))

(define-public crate-nugine-rust-utils-0.1 (crate (name "nugine-rust-utils") (vers "0.1.4") (deps (list (crate-dep (name "simdutf8") (req "^0.1.4") (optional #t) (kind 0)))) (hash "05zlg8pnm15c7dmwxxkjaqa7i96n1s316kgbz8azqjnp2ih3x91x") (features (quote (("default" "std") ("ascii") ("alloc")))) (yanked #t) (v 2) (features2 (quote (("utf8" "dep:simdutf8") ("std" "alloc" "simdutf8?/std"))))))

(define-public crate-nugine-rust-utils-0.2 (crate (name "nugine-rust-utils") (vers "0.2.0") (deps (list (crate-dep (name "simdutf8") (req "^0.1.4") (optional #t) (kind 0)))) (hash "0m254ayxbr8fw9y9zj1lzs5h4h98fasy6y4md5js371wvffdkl5f") (features (quote (("default" "std") ("ascii") ("alloc")))) (yanked #t) (v 2) (features2 (quote (("utf8" "dep:simdutf8") ("std" "alloc" "simdutf8?/std"))))))

(define-public crate-nugine-rust-utils-0.2 (crate (name "nugine-rust-utils") (vers "0.2.1") (deps (list (crate-dep (name "simdutf8") (req "^0.1.4") (optional #t) (kind 0)))) (hash "1ha99dzfxz01ikqzyg80h9gpmjzpmifq5wqlnwkbwrf7451574hb") (features (quote (("default" "std") ("ascii") ("alloc")))) (v 2) (features2 (quote (("utf8" "dep:simdutf8") ("std" "alloc" "simdutf8?/std"))))))

(define-public crate-nugine-rust-utils-0.3 (crate (name "nugine-rust-utils") (vers "0.3.0") (deps (list (crate-dep (name "simdutf8") (req "^0.1.4") (kind 0)))) (hash "01161jsn9pjphc0jxaq7wvazlz7l426xkird973n120x3451507w") (features (quote (("std" "alloc" "simdutf8/std") ("default" "std") ("alloc"))))))

(define-public crate-nugine-rust-utils-0.3 (crate (name "nugine-rust-utils") (vers "0.3.1") (deps (list (crate-dep (name "simdutf8") (req "^0.1.4") (kind 0)))) (hash "18mf0a47q6qqka6f02psw5zdgsf46c3y0w86rb3sjii2m77xkp04") (features (quote (("std" "alloc" "simdutf8/std") ("default" "std") ("alloc"))))))

