(define-module (crates-io nu ms) #:use-module (crates-io))

(define-public crate-nums-0.1 (crate (name "nums") (vers "0.1.0") (deps (list (crate-dep (name "num-bigint") (req "^0.4.4") (features (quote ("rand"))) (default-features #t) (kind 0)) (crate-dep (name "num-integer") (req "^0.1.46") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2.19") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)))) (hash "00avpznwb12fb62rhhwy1yvq61lawky2lbq2m14zr37v4pwp8g6g")))

(define-public crate-nums-1 (crate (name "nums") (vers "1.0.0") (deps (list (crate-dep (name "num-bigint") (req "^0.4.5") (features (quote ("rand"))) (default-features #t) (kind 0)) (crate-dep (name "num-integer") (req "^0.1.46") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2.19") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)) (crate-dep (name "tracing") (req "^0.1.40") (default-features #t) (kind 0)) (crate-dep (name "tracing-forest") (req "^0.1.6") (features (quote ("ansi" "smallvec"))) (default-features #t) (kind 0)) (crate-dep (name "tracing-subscriber") (req "^0.3.17") (features (quote ("std" "env-filter"))) (default-features #t) (kind 0)))) (hash "02yan34dc2sd6m2b9myxd54drxf21dl2mzpqqi0ww444m104nanp")))

(define-public crate-numscale-0.1 (crate (name "numscale") (vers "0.1.0") (hash "0z13himsrnrpbd3qmci8jywnxsbpj07s117is2bp1i289fin722x")))

(define-public crate-numscan-0.1 (crate (name "numscan") (vers "0.1.0") (deps (list (crate-dep (name "round_mult") (req "^0.1.2") (default-features #t) (kind 0)))) (hash "1xd1i80csqizr8ikvm0w1dif9v0lcrwhdddd6d11f1qzcm0jxi3k")))

(define-public crate-numsep-0.1 (crate (name "numsep") (vers "0.1.0") (deps (list (crate-dep (name "slicestring") (req "^0.1.3") (default-features #t) (kind 0)))) (hash "124vv6ii8civxv6zh9r1f41rxkrl562b6x5710hvjqav4xykvcc6")))

(define-public crate-numsep-0.1 (crate (name "numsep") (vers "0.1.1") (deps (list (crate-dep (name "slicestring") (req "^0.1.3") (default-features #t) (kind 0)))) (hash "1d2337cbjpiqjsj811ks7ihyywva33ji309al4h75vx5dyajd552")))

(define-public crate-numsep-0.1 (crate (name "numsep") (vers "0.1.12") (deps (list (crate-dep (name "slicestring") (req "^0.1.3") (default-features #t) (kind 0)))) (hash "08nsjxg3qcdyn9m969pfb76psr8v0cva3js33zxlwc9cw71ljp5d")))

