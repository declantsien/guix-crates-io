(define-module (crates-io nu -h) #:use-module (crates-io))

(define-public crate-nu-hist-0.1 (crate (name "nu-hist") (vers "0.1.0") (deps (list (crate-dep (name "chrono") (req "^0.4.31") (default-features #t) (kind 0)) (crate-dep (name "comfy-table") (req "^7.1.0") (default-features #t) (kind 0)) (crate-dep (name "inline_colorization") (req "^0.1.6") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)) (crate-dep (name "rusqlite") (req "^0.30.0") (default-features #t) (kind 0)))) (hash "1z43dg76bzsrxyvkp78sxx7kdla6a3ycpdxhfxbbp66g8c76v2v3")))

