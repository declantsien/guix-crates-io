(define-module (crates-io nu rs) #:use-module (crates-io))

(define-public crate-nursery-0.0.1 (crate (name "nursery") (vers "0.0.1") (hash "19bgif2phsavw296qsfp0mpxbsn345crmigvd8jvpinp1i72vgd4")))

(define-public crate-nursery_macro-0.1 (crate (name "nursery_macro") (vers "0.1.0") (deps (list (crate-dep (name "smol") (req "^1.3.0") (default-features #t) (kind 0)))) (hash "0lywj09xi3i94f41ivr6q62p2mg7bdwwv6icssg1z0ss672124br")))

