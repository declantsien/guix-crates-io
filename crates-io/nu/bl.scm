(define-module (crates-io nu bl) #:use-module (crates-io))

(define-public crate-nubls-0.1 (crate (name "nubls") (vers "0.1.0") (deps (list (crate-dep (name "bls12_381") (req "^0.3.1") (features (quote ("nightly" "endo"))) (default-features #t) (kind 0)) (crate-dep (name "getrandom") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "sha2") (req "^0.9.2") (default-features #t) (kind 0)))) (hash "0b5gj81wk34xn9586h2vdpyy0fbsda90bav6vbmyf7wdpdi8faba")))

