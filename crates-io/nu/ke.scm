(define-module (crates-io nu ke) #:use-module (crates-io))

(define-public crate-nuke-0.0.0 (crate (name "nuke") (vers "0.0.0") (hash "1knfizpdxxzsmqpv51lyl4lyc0jjpzhnirn6mipi7rx9ncj3yg2d")))

(define-public crate-nuke-dir-0.1 (crate (name "nuke-dir") (vers "0.1.0") (hash "1yk4inlppvw0hawxlhf0733q5bg9qgynr2hmcjdd324v17c44s3a")))

