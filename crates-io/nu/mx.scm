(define-module (crates-io nu mx) #:use-module (crates-io))

(define-public crate-numX-0.2 (crate (name "numX") (vers "0.2.0") (deps (list (crate-dep (name "cfg-if") (req "~0.1") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "~0.2") (optional #t) (kind 0)) (crate-dep (name "serde") (req "~1.0") (optional #t) (default-features #t) (kind 0)))) (hash "18lbpakim0as3wd1xawin1vsdaz6n8mszvzzrdrbn64p2g56xr7x") (features (quote (("use_serde" "serde") ("std") ("num" "num-traits") ("default" "std")))) (yanked #t)))

