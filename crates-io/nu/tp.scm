(define-module (crates-io nu tp) #:use-module (crates-io))

(define-public crate-nutp-0.1 (crate (name "nutp") (vers "0.1.0") (deps (list (crate-dep (name "hashbrown") (req "^0.14.2") (default-features #t) (kind 0)))) (hash "0dha054vrwxfkjvyw9dracmpwf81fbp1xjrzwdshdx7n7i06ac0d")))

(define-public crate-nutp-0.1 (crate (name "nutp") (vers "0.1.1") (deps (list (crate-dep (name "hashbrown") (req "^0.14.2") (default-features #t) (kind 0)))) (hash "06csdzc8s5yjlxbzjj511dq5dblbaqkm8i4x51iq06ndxjkgdjpf")))

(define-public crate-nutp-0.1 (crate (name "nutp") (vers "0.1.2") (deps (list (crate-dep (name "hashbrown") (req "^0.14.2") (default-features #t) (kind 0)))) (hash "1hzs2y5p5rvp0w96rf4iw1fqdlywk6hdbivm44qhvajg651s1klf")))

(define-public crate-nutp-0.1 (crate (name "nutp") (vers "0.1.3") (deps (list (crate-dep (name "hashbrown") (req "^0.14.2") (default-features #t) (kind 0)))) (hash "1gk2xdf9f4r1abqafczip5f47i9c0ir1yvprilzkmgjia200qijh")))

(define-public crate-nutp-0.2 (crate (name "nutp") (vers "0.2.0") (deps (list (crate-dep (name "hashbrown") (req "^0.14.2") (default-features #t) (kind 0)) (crate-dep (name "heapless") (req "^0.7.16") (features (quote ("ufmt-write"))) (default-features #t) (kind 0)))) (hash "1x3zrfak5nc9fwax58hsnnmdmk6cknnrfhb2pf59p95i7fi28s1x")))

(define-public crate-nutp-0.3 (crate (name "nutp") (vers "0.3.0") (deps (list (crate-dep (name "heapless") (req "^0.7.16") (features (quote ("ufmt-write"))) (default-features #t) (kind 0)))) (hash "1ldkfr8czzqrkr9c19lgblyyfncr7l9a9sv2mrk2nag13m7c8y17")))

(define-public crate-nutp-0.3 (crate (name "nutp") (vers "0.3.1") (deps (list (crate-dep (name "heapless") (req "^0.8") (default-features #t) (kind 0)))) (hash "1513s7z7wy5frfkgklb5jd5n4x373dwsgp93id5ckpsska5pcqbw")))

(define-public crate-nutp-0.3 (crate (name "nutp") (vers "0.3.2") (deps (list (crate-dep (name "heapless") (req "^0.8") (default-features #t) (kind 0)))) (hash "1a0hr6xm1sgkb85krdjr4fibrwr8zf0xmp6byb6hjwmi9f9idv1w")))

