(define-module (crates-io nu co) #:use-module (crates-io))

(define-public crate-nucoff-0.0.0 (crate (name "nucoff") (vers "0.0.0") (hash "1nk22qxkj7349xzj3l7s6gb1aqghiyab5m73zj9lvzz4j7sz3ggs")))

(define-public crate-nucomcore-0.1 (crate (name "nucomcore") (vers "0.1.0") (deps (list (crate-dep (name "nuidl") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "nuidl") (req "^0.1.0") (default-features #t) (kind 1)) (crate-dep (name "paste") (req "^1.0.14") (default-features #t) (kind 0)) (crate-dep (name "uuid") (req "^1.3.1") (default-features #t) (kind 0)))) (hash "1jn9naiap2f5igrmxm6c63rry83cwayf20lnzalvmbzzqfvm6qc4")))

(define-public crate-nucomcore-0.1 (crate (name "nucomcore") (vers "0.1.1") (deps (list (crate-dep (name "nuidl") (req "^0.1.1") (default-features #t) (kind 0)) (crate-dep (name "nuidl") (req "^0.1.1") (default-features #t) (kind 1)) (crate-dep (name "paste") (req "^1.0.14") (default-features #t) (kind 0)) (crate-dep (name "uuid") (req "^1.3.1") (default-features #t) (kind 0)))) (hash "183bcrg6n79dvpz7hzy1nrf5cqif6q2zzwyqni67qhh5s34kvczf")))

