(define-module (crates-io nu ef) #:use-module (crates-io))

(define-public crate-nuefi-0.0.0 (crate (name "nuefi") (vers "0.0.0") (hash "1xnjhni2y34pj070l04rmbk1i66219c3myhvaqhw8m0nkvqrnhqb")))

(define-public crate-nuefi_core-0.0.0 (crate (name "nuefi_core") (vers "0.0.0") (hash "0nm7z199v2flyrzm272f6n7a34my1m2k4s4slwv1zvyy0gicry7q")))

(define-public crate-nuefi_raw-0.0.0 (crate (name "nuefi_raw") (vers "0.0.0") (hash "0q8limirl15v98x16ph098c3gkvml3nafgl6zmg3qw9db515jaxg")))

(define-public crate-nuefi_sdk-0.0.0 (crate (name "nuefi_sdk") (vers "0.0.0") (hash "0zyw3r864yjbrk28nk90jqll1k65r1mgd4hi9fkhhjhls7cj6fgb")))

(define-public crate-nuefi_types-0.0.0 (crate (name "nuefi_types") (vers "0.0.0") (hash "1av7z2zciylyfqw69vm32m1yhqd79z9jic148dxwbf5q1bp3m1qd")))

