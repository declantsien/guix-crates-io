(define-module (crates-io nu ho) #:use-module (crates-io))

(define-public crate-nuhound-0.1 (crate (name "nuhound") (vers "0.1.0") (deps (list (crate-dep (name "regex") (req "^1") (default-features #t) (kind 2)))) (hash "02d6lzq061xfjh45giq4vfz57dk7a6r377gjwl9xps4i8h0pps38") (features (quote (("disclose"))))))

(define-public crate-nuhound-0.1 (crate (name "nuhound") (vers "0.1.1") (deps (list (crate-dep (name "regex") (req "^1") (default-features #t) (kind 2)))) (hash "1nd8z2r1i1rj1n45zqlrswqr8980xw48izjvy16hdnd1hbfps4q0") (features (quote (("disclose"))))))

(define-public crate-nuhound-0.1 (crate (name "nuhound") (vers "0.1.2") (deps (list (crate-dep (name "regex") (req "^1") (default-features #t) (kind 2)))) (hash "19sklvbwqar1piqy2m5kfg4ja5k0jr76npl02y60pmvzvypqb9m2") (features (quote (("disclose"))))))

