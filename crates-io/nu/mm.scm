(define-module (crates-io nu mm) #:use-module (crates-io))

(define-public crate-nummap-0.1 (crate (name "nummap") (vers "0.1.0") (hash "0p53cb4fr9m00m45z713s5f8srrsvag1d5qr9gnx7p1wlc2054d9") (features (quote (("map_get_key_value"))))))

(define-public crate-nummap-0.1 (crate (name "nummap") (vers "0.1.1") (hash "0p576n6xl3lsnhva6jl3ffx7g17vfvvr3jvvqmf02b4r7jilrjjh") (features (quote (("map_get_key_value"))))))

(define-public crate-nummap-0.2 (crate (name "nummap") (vers "0.2.0") (hash "09549z7zpnilgx113nam5x3jdwjpn36sz46vvi64wvi104hwb057") (features (quote (("map_get_key_value"))))))

(define-public crate-nummap-0.2 (crate (name "nummap") (vers "0.2.1") (hash "1zsbnclh2kmhf608i6r19z3pz85q3mahilg1yz1xvh9jxf3pb8rr") (features (quote (("map_get_key_value"))))))

(define-public crate-nummap-0.2 (crate (name "nummap") (vers "0.2.2") (hash "1v0sccw804sqh5ggzdy8mdvmizap99b19p0gris4gpfpf2p2sx9a") (features (quote (("map_get_key_value"))))))

(define-public crate-nummap-0.2 (crate (name "nummap") (vers "0.2.3") (hash "0n7039pgic7f3k8aci70cygjw9l91vw8wd93cyq574j5d0dyxw1b") (features (quote (("map_get_key_value"))))))

(define-public crate-nummap-0.2 (crate (name "nummap") (vers "0.2.4") (hash "1v2w3nplwdzz2nkjspk1000jkkqw3ql6fvy0vmwi3cvbpskyk85h") (features (quote (("map_get_key_value"))))))

(define-public crate-nummap-0.3 (crate (name "nummap") (vers "0.3.0") (hash "1yrwccfxlr3g9f73lfmj5b73dz2mbh7sba935b9gw4cjimf8138f") (features (quote (("map_get_key_value"))))))

(define-public crate-nummap-0.3 (crate (name "nummap") (vers "0.3.1") (hash "09x938sr7mcvk7faypi1iljz34rgp7js7jh25h6vnihm595w1888") (features (quote (("map_get_key_value"))))))

(define-public crate-nummap-0.3 (crate (name "nummap") (vers "0.3.2") (hash "19bq78q41d9g7c73xbymhk55z2shjf7ai9z93vjvz1arggxqair6") (features (quote (("map_get_key_value"))))))

(define-public crate-nummap-0.3 (crate (name "nummap") (vers "0.3.3") (hash "19xrs2j29prn4xpg4gmk2n489firhgl7ry443f3gl2zrymhnf97r") (features (quote (("map_get_key_value"))))))

(define-public crate-nummap-0.3 (crate (name "nummap") (vers "0.3.4") (hash "1wpzkyz8zdlc4ssi86f0nhx4cvn2pqrg1ss4xs3gg3zv2hcdykm5") (features (quote (("map_get_key_value"))))))

(define-public crate-nummap-0.4 (crate (name "nummap") (vers "0.4.0") (deps (list (crate-dep (name "hashbrown") (req "^0.4") (optional #t) (default-features #t) (kind 0)))) (hash "1bvdxzivs7ws76sb65f7limj0namv4hlpfng9hckpgxk6lsdbklr") (features (quote (("map_get_key_value") ("default" "hashbrown"))))))

(define-public crate-nummap-0.4 (crate (name "nummap") (vers "0.4.1") (deps (list (crate-dep (name "hashbrown") (req "^0.4") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde_cbor") (req "^0.9") (default-features #t) (kind 2)))) (hash "1kdm0gj5m48jn9vkf1gv7qx7j5yl6ba5vfnd2ma14c0pk7nb53gv") (features (quote (("map_get_key_value") ("default" "hashbrown"))))))

(define-public crate-nummap-0.4 (crate (name "nummap") (vers "0.4.2") (deps (list (crate-dep (name "hashbrown") (req "^0.4") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde_cbor") (req "^0.9") (default-features #t) (kind 2)))) (hash "0xpfcpsb2jdi4ckk7a9awiy01n9d3d6p171hyg1sa8bmqipscrmp") (features (quote (("map_get_key_value") ("default" "hashbrown"))))))

(define-public crate-nummap-0.5 (crate (name "nummap") (vers "0.5.0") (deps (list (crate-dep (name "hashbrown") (req "^0.6") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde_cbor") (req "^0.9") (default-features #t) (kind 2)))) (hash "0ld1r22xbngv7nlv15h214z1pn7yyvskkydqn2wm6dp20lp34nwk") (features (quote (("no_std" "hashbrown") ("map_get_key_value") ("default" "no_std"))))))

(define-public crate-nummap-0.5 (crate (name "nummap") (vers "0.5.1") (deps (list (crate-dep (name "hashbrown") (req "^0.6") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde_cbor") (req "^0.9") (default-features #t) (kind 2)))) (hash "0lphs4i73d7814k929qdyi0sbkkd3vc0mvgycnck5vvpkcgld8sl") (features (quote (("no_std" "hashbrown") ("map_get_key_value") ("default" "no_std"))))))

(define-public crate-nummer-0.1 (crate (name "nummer") (vers "0.1.0") (hash "0r63y1zv0cv0csrm05173pxzmh43b10k1x1p489bx1n5g6n2dh65")))

(define-public crate-nummer-0.2 (crate (name "nummer") (vers "0.2.0") (hash "1acgphpryg4a6ccqg72w49hf47ahlki6qi46shlbnqz0jg3qa4gm")))

(define-public crate-nummer-0.2 (crate (name "nummer") (vers "0.2.1") (hash "0r8zhmz6w9b6nlyllg4c1k2359fw45iqix5m30ymir19vr0mvfj7")))

(define-public crate-nummy-0.1 (crate (name "nummy") (vers "0.1.0") (deps (list (crate-dep (name "decimal") (req "^2.1.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "fructose") (req "^0.3.3") (default-features #t) (kind 0)))) (hash "1lms4i0hw7vxbj5ff7qnrw3m4w8d4mlqg68fjriyjp0pj8j79p0v") (features (quote (("default"))))))

