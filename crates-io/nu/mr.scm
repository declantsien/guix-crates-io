(define-module (crates-io nu mr) #:use-module (crates-io))

(define-public crate-numred-0.1 (crate (name "numred") (vers "0.1.0") (hash "0yvaaacfy73mpg31cwik1jd89dprn401mfnxby52br93v5i1cpz3")))

(define-public crate-numrepr-0.1 (crate (name "numrepr") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^2") (default-features #t) (kind 0)) (crate-dep (name "env_logger") (req "^0.5") (default-features #t) (kind 0)) (crate-dep (name "failure") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "failure_derive") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)))) (hash "1xqkw27sr4cgnylw8bndk2xxh8nbiypw57yqzhhadhn7dajp5wh0")))

(define-public crate-numrs-0.1 (crate (name "numrs") (vers "0.1.0") (hash "01chk4bql8kkapxxv47f0mq139jfhc8lnwsjll69xp5l17nqn2f1")))

(define-public crate-numrs-0.2 (crate (name "numrs") (vers "0.2.0") (deps (list (crate-dep (name "num") (req "^0.1.32") (default-features #t) (kind 0)))) (hash "1cp4b6cgi6286g3jwm3y90dra39i76bnfp5dawb3ksdvxj75gxkv")))

(define-public crate-NumRust-0.0.0 (crate (name "NumRust") (vers "0.0.0") (hash "1awx27k2wykx6d7md4wd5h0a04fnwqz6pvdmkpjxp08qw274yjs3")))

