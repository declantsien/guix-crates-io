(define-module (crates-io nu nc) #:use-module (crates-io))

(define-public crate-nunc-0.1 (crate (name "nunc") (vers "0.1.0") (deps (list (crate-dep (name "dirs") (req "^4.0") (default-features #t) (kind 0)) (crate-dep (name "rusqlite") (req "^0.28") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "toml") (req "^0.5") (default-features #t) (kind 0)))) (hash "03vg0ishys6r68z7pkz20nsvx55n8rj8737kn16zaa437zqihcnl")))

