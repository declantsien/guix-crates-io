(define-module (crates-io nu mi) #:use-module (crates-io))

(define-public crate-numid-0.1 (crate (name "numid") (vers "0.1.0") (hash "188gk4y2538w0vq3yb2nnf2izbjh7q0bf9xbr23xdiwys1vampx5")))

(define-public crate-numid-0.1 (crate (name "numid") (vers "0.1.1") (hash "1b2fpzzafiqglxinbjd21kjm16azbc7d6w5j8fwlj198cvnzpm0l") (features (quote (("example") ("default"))))))

(define-public crate-numid-0.1 (crate (name "numid") (vers "0.1.2") (hash "0rpf341v2f73fv32hah02x3lcwgkm2lxzgh4sjvmyfligriffn5d") (features (quote (("std") ("example") ("default" "std")))) (yanked #t)))

(define-public crate-numid-0.2 (crate (name "numid") (vers "0.2.0") (hash "1vkbjqqfzqhziva81clwpz33jh16m0rg8yr65fzlbvkg6xsqkvjm") (features (quote (("example") ("default"))))))

(define-public crate-numid-0.2 (crate (name "numid") (vers "0.2.1") (hash "18v45g1hjyg7r2wmhg4m00p9zw69xhczbrwg9jm1p08m3hg53d16") (features (quote (("example") ("default"))))))

(define-public crate-numid-0.2 (crate (name "numid") (vers "0.2.2") (hash "0cr88v6lkbm1q0g0ng35cf952y5j3vqpg623bvxsrca2j7vbgr8m") (features (quote (("example") ("display") ("default" "display"))))))

(define-public crate-numid-0.2 (crate (name "numid") (vers "0.2.3") (hash "1xww97h15cp1s6zgn1wwffkv0530020h4vjpx24hbr1ydc1vyl8b") (features (quote (("example") ("display") ("default" "display"))))))

(define-public crate-numid-0.2 (crate (name "numid") (vers "0.2.4") (hash "1lbhs9f8p85cqslxb28ixfb7synadhavfrv068859bqnlmfvfq2s") (features (quote (("example") ("display") ("default" "display")))) (yanked #t)))

(define-public crate-numid-0.2 (crate (name "numid") (vers "0.2.5") (hash "03xmg5c55cmfs4fhgz02bqddp4sbjjw7qn2q6aipx3l2mdfx7wxc") (features (quote (("example") ("display") ("default" "display"))))))

(define-public crate-numid-0.2 (crate (name "numid") (vers "0.2.6") (hash "1ssgp9nyjc9bz3dp124fk4v9yi7szhdfyr17yxs03cqnfc8sqn2m") (features (quote (("example") ("display") ("default" "display"))))))

(define-public crate-numid-0.2 (crate (name "numid") (vers "0.2.7") (hash "03ay49v59s8xz9i3b05a3617fmspa00nipp9ij3j7vn75y9g3sxn") (features (quote (("example") ("display") ("default" "display"))))))

(define-public crate-numid-0.2 (crate (name "numid") (vers "0.2.8") (deps (list (crate-dep (name "const_fn_assert") (req "^0.1") (default-features #t) (kind 0)))) (hash "0v68sz2swblxgq1f7liigcmis2gdxxy2i670ks7wnbz8h0g157ym") (features (quote (("example") ("display") ("default" "display"))))))

(define-public crate-numid-0.2 (crate (name "numid") (vers "0.2.9") (deps (list (crate-dep (name "const_fn_assert") (req "^0.1") (default-features #t) (kind 0)))) (hash "1vgamqcc7wzla5qq28qpf9j8g0dnvvmbj2j4vgxf6ig428gwqyz0") (features (quote (("example") ("display") ("default" "display"))))))

(define-public crate-numina-pkg-0.1 (crate (name "numina-pkg") (vers "0.1.0") (deps (list (crate-dep (name "cosmwasm-schema") (req "^1.3.0") (default-features #t) (kind 0)) (crate-dep (name "cosmwasm-std") (req "^1.3.0") (features (quote ("cosmwasm_1_2"))) (default-features #t) (kind 0)) (crate-dep (name "cw-asset") (req "^3.0.0") (default-features #t) (kind 0)) (crate-dep (name "cw20") (req "^1.1.0") (default-features #t) (kind 0)) (crate-dep (name "osmosis-std") (req "^0.19.1") (default-features #t) (kind 0)) (crate-dep (name "pyth-sdk-cw") (req "^1.2.0") (default-features #t) (kind 0)) (crate-dep (name "rhaki-cw-plus") (req "^0.6.5") (default-features #t) (kind 0)) (crate-dep (name "serde-json-wasm") (req "^1.0.0") (default-features #t) (kind 0)))) (hash "1sqvkaykw08nv85chxgxvss7np74d8szwcdixdsf6lsxzkv834l3")))

(define-public crate-numina-pkg-0.1 (crate (name "numina-pkg") (vers "0.1.1") (deps (list (crate-dep (name "cosmwasm-schema") (req "^1.3.0") (default-features #t) (kind 0)) (crate-dep (name "cosmwasm-std") (req "^1.3.0") (features (quote ("cosmwasm_1_2"))) (default-features #t) (kind 0)) (crate-dep (name "cw-asset") (req "^3.0.0") (default-features #t) (kind 0)) (crate-dep (name "cw20") (req "^1.1.0") (default-features #t) (kind 0)) (crate-dep (name "osmosis-std") (req "^0.19.1") (default-features #t) (kind 0)) (crate-dep (name "pyth-sdk-cw") (req "^1.2.0") (default-features #t) (kind 0)) (crate-dep (name "rhaki-cw-plus") (req "^0.6.10") (default-features #t) (kind 0)) (crate-dep (name "serde-json-wasm") (req "^1.0.0") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.108") (default-features #t) (kind 0)))) (hash "098k58wm1h38qfa57f9jmj8vdwwfpwai40s12iy8q47mjsjin5cc")))

