(define-module (crates-io nu dg) #:use-module (crates-io))

(define-public crate-nudge-0.1 (crate (name "nudge") (vers "0.1.0") (deps (list (crate-dep (name "cfg-if") (req "^0.1.9") (default-features #t) (kind 0)))) (hash "10858qjcdgk0cd2xr682d47y9rf2y5y9l6xv6x7lyx3vd7cqm18m") (features (quote (("std") ("nightly") ("default" "std"))))))

(define-public crate-nudge-0.2 (crate (name "nudge") (vers "0.2.0") (deps (list (crate-dep (name "cfg-if") (req "^0.1.9") (default-features #t) (kind 0)))) (hash "12mbl2axmz8yrx3nm5fccs3zvhgif4r2n4dxf833sb9kchbv6scs") (features (quote (("std") ("nightly") ("default" "std"))))))

(define-public crate-nudge-0.2 (crate (name "nudge") (vers "0.2.1") (deps (list (crate-dep (name "cfg-if") (req "^0.1.9") (default-features #t) (kind 0)))) (hash "0ygphx57dscnxsr417k9wz3ga8fxj485znawzf7n6a5bmwz9qdsz") (features (quote (("std") ("nightly") ("default" "std"))))))

(define-public crate-nudge-0.2 (crate (name "nudge") (vers "0.2.2") (hash "0qgq4rbbi8894s5vqidqd85f60sll0licc9iggydxpprppn6h73q") (features (quote (("std") ("nightly") ("default" "std"))))))

