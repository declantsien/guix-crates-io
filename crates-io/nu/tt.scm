(define-module (crates-io nu tt) #:use-module (crates-io))

(define-public crate-nuttx-embedded-hal-1 (crate (name "nuttx-embedded-hal") (vers "1.0.0") (deps (list (crate-dep (name "embedded-hal") (req "^0.2.7") (features (quote ("unproven"))) (default-features #t) (kind 0)) (crate-dep (name "heapless") (req "^0.7.5") (default-features #t) (kind 0)))) (hash "07db0k9lviqsjsclyrpw4m82xbvqvfikmxn59250wnv3qwi6q9bi") (features (quote (("default"))))))

(define-public crate-nuttx-embedded-hal-1 (crate (name "nuttx-embedded-hal") (vers "1.0.1") (deps (list (crate-dep (name "embedded-hal") (req "^0.2.7") (features (quote ("unproven"))) (default-features #t) (kind 0)) (crate-dep (name "heapless") (req "^0.7.5") (default-features #t) (kind 0)))) (hash "0mivw3r6gpvl8bvs9rc9qyrsa0klx2kw8gpjzilvw7rnv9gis7pl") (features (quote (("default"))))))

(define-public crate-nuttx-embedded-hal-1 (crate (name "nuttx-embedded-hal") (vers "1.0.2") (deps (list (crate-dep (name "embedded-hal") (req "^0.2.7") (features (quote ("unproven"))) (default-features #t) (kind 0)) (crate-dep (name "heapless") (req "^0.7.5") (default-features #t) (kind 0)))) (hash "04adrik9mbaw7rxiawp2dc12pq389axfd6vbfdqc3h4q99mpri4x") (features (quote (("default"))))))

(define-public crate-nuttx-embedded-hal-1 (crate (name "nuttx-embedded-hal") (vers "1.0.3") (deps (list (crate-dep (name "embedded-hal") (req "^0.2.7") (features (quote ("unproven"))) (default-features #t) (kind 0)) (crate-dep (name "heapless") (req "^0.7.5") (default-features #t) (kind 0)))) (hash "0z3sph3m2wzmd9xpq32b7qszjzcwswb9sp80w37sich85gkshqkv") (features (quote (("default"))))))

(define-public crate-nuttx-embedded-hal-1 (crate (name "nuttx-embedded-hal") (vers "1.0.4") (deps (list (crate-dep (name "embedded-hal") (req "^0.2.7") (features (quote ("unproven"))) (default-features #t) (kind 0)) (crate-dep (name "heapless") (req "^0.7.10") (default-features #t) (kind 0)))) (hash "0ad0wlzscmhxaapc6sb2sgk0cb2m259qxl6r9k783jpnf4z34mzl") (features (quote (("default"))))))

(define-public crate-nuttx-embedded-hal-1 (crate (name "nuttx-embedded-hal") (vers "1.0.5") (deps (list (crate-dep (name "embedded-hal") (req "^0.2.7") (features (quote ("unproven"))) (default-features #t) (kind 0)) (crate-dep (name "heapless") (req "^0.7.10") (default-features #t) (kind 0)))) (hash "0ih5c6adwrwaa8dzdw89n675x9jmsl4c3gvkhamy9wy67sk61cga") (features (quote (("default"))))))

(define-public crate-nuttx-embedded-hal-1 (crate (name "nuttx-embedded-hal") (vers "1.0.6") (deps (list (crate-dep (name "embedded-hal") (req "^0.2.7") (features (quote ("unproven"))) (default-features #t) (kind 0)) (crate-dep (name "heapless") (req "^0.7.10") (default-features #t) (kind 0)))) (hash "0y09y0sw99bias2196fmmr4ndd9vnill5xxxd1l560cvgj7pa5mr") (features (quote (("default"))))))

(define-public crate-nuttx-embedded-hal-1 (crate (name "nuttx-embedded-hal") (vers "1.0.7") (deps (list (crate-dep (name "embedded-hal") (req "^0.2.7") (features (quote ("unproven"))) (default-features #t) (kind 0)) (crate-dep (name "heapless") (req "^0.7.10") (default-features #t) (kind 0)))) (hash "07px9qkdd84fc5wb39g10dfzqkd2xjjrhp1iq1nqv6jx5qlgjcmb") (features (quote (("default"))))))

(define-public crate-nuttx-embedded-hal-1 (crate (name "nuttx-embedded-hal") (vers "1.0.8") (deps (list (crate-dep (name "embedded-hal") (req "^0.2.7") (features (quote ("unproven"))) (default-features #t) (kind 0)) (crate-dep (name "heapless") (req "^0.7.10") (default-features #t) (kind 0)))) (hash "0xl5905lz4dcw5m6l5jqzypnymba7qc56gbkl27f3j0b6a0yg8nz") (features (quote (("default"))))))

(define-public crate-nuttx-embedded-hal-1 (crate (name "nuttx-embedded-hal") (vers "1.0.9") (deps (list (crate-dep (name "embedded-hal") (req "^0.2.7") (features (quote ("unproven"))) (default-features #t) (kind 0)) (crate-dep (name "heapless") (req "^0.7.10") (default-features #t) (kind 0)))) (hash "0zs1m5xm6lgwgfsaxbywq7rdiv3gcwfsw28j986bcfjskvlij85f") (features (quote (("default"))))))

(define-public crate-nuttx-embedded-hal-1 (crate (name "nuttx-embedded-hal") (vers "1.0.10") (deps (list (crate-dep (name "embedded-hal") (req "^0.2.7") (features (quote ("unproven"))) (default-features #t) (kind 0)) (crate-dep (name "heapless") (req "^0.7.10") (default-features #t) (kind 0)))) (hash "1s5nci0dj3bg0adj6bc49waacwxgi3nynsd92c213ypj69j237y6") (features (quote (("default"))))))

(define-public crate-nutty-0.0.0 (crate (name "nutty") (vers "0.0.0") (hash "1lslz3bg799dx8qnhnpx7c485f0b0079ndlna2hzlx4a1n64l1w5")))

