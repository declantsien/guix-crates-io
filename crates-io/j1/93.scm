(define-module (crates-io j1 #{93}#) #:use-module (crates-io))

(define-public crate-j1939-0.1 (crate (name "j1939") (vers "0.1.13") (hash "0ficx9qx3dmw90941503ilvvvhg50sdm7ppy7zp3xnl2whnimlww") (features (quote (("can"))))))

(define-public crate-j1939-0.1 (crate (name "j1939") (vers "0.1.14") (hash "0cfx0zqpq3xj5059hzggxzqn24v6s28ssr179dsjqmdy5s3gwwc6") (features (quote (("can"))))))

(define-public crate-j1939-0.1 (crate (name "j1939") (vers "0.1.15") (hash "0s872hqm0wl8is71rsk11whsl91d2j5npvwn1fzn5s37xsbdxwhv") (features (quote (("can"))))))

(define-public crate-j1939-0.1 (crate (name "j1939") (vers "0.1.16") (hash "1381q65gkmn5ss1169hp6w2mvnl9q7ihwrd6mrr0b8sfkvbl55k2") (features (quote (("can"))))))

(define-public crate-j1939-0.1 (crate (name "j1939") (vers "0.1.17") (hash "0rqhbnybxdrri04gswb080drjjy5avsqhwjx18lkfaacbbgvhfs8") (features (quote (("can"))))))

(define-public crate-j1939-0.1 (crate (name "j1939") (vers "0.1.18") (hash "1qvlp3l3yg85m6vwr73xrhm0lhjiz65f446sr5xwlmiwi51qbydd") (features (quote (("can"))))))

(define-public crate-j1939-0.1 (crate (name "j1939") (vers "0.1.19") (hash "0qsm2jqrr5z78k36wd28sjfbbjb3jrg7d7426yp8ywb7qh9kqz3f") (features (quote (("can"))))))

(define-public crate-j1939-0.1 (crate (name "j1939") (vers "0.1.20") (hash "0l0mwip8d12j8znzrf37x660h58sa4d77s1jgkad5x4gmnl4fdh2") (features (quote (("can"))))))

(define-public crate-j1939-0.1 (crate (name "j1939") (vers "0.1.21") (hash "1g1r26rq00gisg0cra5scvgy5h0lp2kka513zyi1s6m554yxmsp1") (features (quote (("can"))))))

(define-public crate-j1939-0.1 (crate (name "j1939") (vers "0.1.22") (hash "0d7zazhps8v6sw8s9vnhm296s7lwjlv1rkqjm4silrdpg4gmipqn") (features (quote (("can"))))))

(define-public crate-j1939-0.1 (crate (name "j1939") (vers "0.1.23") (hash "12vfx9m37xwk5g51klnc24mlg9bryi8cqa3yly9ham605f5hykmd") (features (quote (("can"))))))

(define-public crate-j1939-0.1 (crate (name "j1939") (vers "0.1.24") (hash "0yhvym9wrcv7wyk5sk6mgzhz832xcjz84d3alsrk35y84ckb8c98") (features (quote (("can"))))))

(define-public crate-j1939-0.1 (crate (name "j1939") (vers "0.1.25") (hash "0xxjbl4pr7xzp1npgl0mjmwxqx77sbhli0fz65fpgybdpz9r4ssz") (features (quote (("can"))))))

(define-public crate-j1939-0.1 (crate (name "j1939") (vers "0.1.26") (hash "0c8jk9rg6r6lbhqm469jsjcx9jdkzz7r1rzxflz3c2ldf6i25ndv") (features (quote (("can"))))))

(define-public crate-j1939-0.1 (crate (name "j1939") (vers "0.1.27") (hash "0ihhg33bqp3jzd6kddkq7p4ksqmnp5jz3y579lx648adcv8y0glf") (features (quote (("can"))))))

(define-public crate-j1939-0.1 (crate (name "j1939") (vers "0.1.28") (hash "1q0ahscaa9ph5s4h9sgr54piyws26dvi8cmy3snxfrwj4i5cxw8q") (features (quote (("can"))))))

(define-public crate-j1939-0.1 (crate (name "j1939") (vers "0.1.29") (hash "0q2phjrs932s3j9ljz6ab7a79614y78m8ddxgv7zy3j3zhf0fbh1")))

(define-public crate-j1939-0.1 (crate (name "j1939") (vers "0.1.30") (hash "1964xrp2c7na37014law3n5c6r3i38phpvwrrqyc5p882dqrizin")))

(define-public crate-j1939-0.1 (crate (name "j1939") (vers "0.1.31") (hash "1ab9zgbs72j807ry4ix1qkwf9k7r1cvw3cimg14g7p279jcp9bnb")))

