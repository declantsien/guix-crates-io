(define-module (crates-io xf in) #:use-module (crates-io))

(define-public crate-xfinal-macro-0.1 (crate (name "xfinal-macro") (vers "0.1.2") (hash "1ijc465j34zlszv44r3xcvg29b0ps3y12lxad4brp7z125cwpp0y")))

(define-public crate-xfind-0.1 (crate (name "xfind") (vers "0.1.0") (deps (list (crate-dep (name "memchr") (req "^2.4") (default-features #t) (kind 0)))) (hash "1d72f0vspw9sad6ddv9xbh4pyx2v2x1nzh0r7n7li5mcnkvf5lxp") (yanked #t)))

(define-public crate-xfind-0.1 (crate (name "xfind") (vers "0.1.1") (deps (list (crate-dep (name "memchr") (req "^2.4") (default-features #t) (kind 0)))) (hash "0xhx7bw51lbgdgdnfsfwlcl1jvgk6plk2agdr29iswgiyy9p9yx9") (yanked #t)))

(define-public crate-xfind-0.1 (crate (name "xfind") (vers "0.1.2") (deps (list (crate-dep (name "memchr") (req "^2.4") (default-features #t) (kind 0)))) (hash "18r4zgh4ws7r1qib3wfdyrghm5kpr714h3605v65bkrilbrxqha0") (yanked #t)))

(define-public crate-xfind-0.2 (crate (name "xfind") (vers "0.2.0") (deps (list (crate-dep (name "aho-corasick") (req "^0.7") (default-features #t) (kind 2)) (crate-dep (name "memchr") (req "^2.4") (default-features #t) (kind 0)))) (hash "1j60d453x6hl9ykaxmpzaybg033kzpr0jm0fciq5byhlkazfki0g")))

(define-public crate-xfind-0.2 (crate (name "xfind") (vers "0.2.1") (deps (list (crate-dep (name "aho-corasick") (req "^0.7") (default-features #t) (kind 2)) (crate-dep (name "memchr") (req "^2.4") (default-features #t) (kind 0)))) (hash "1kg4x1dipf1alhlsllls07s763glynv1kbdsf82nzy9d6v92kk11")))

(define-public crate-xfind-0.2 (crate (name "xfind") (vers "0.2.2") (deps (list (crate-dep (name "aho-corasick") (req "^0.7") (default-features #t) (kind 2)) (crate-dep (name "memchr") (req "^2.4") (default-features #t) (kind 0)))) (hash "09kygald4vsjqhrsf0hxhnflazqrwsh97s8zqbkzcxs8xw47rrzv")))

(define-public crate-xfind-0.2 (crate (name "xfind") (vers "0.2.3") (deps (list (crate-dep (name "aho-corasick") (req "^0.7") (default-features #t) (kind 2)) (crate-dep (name "memchr") (req "^2.4") (default-features #t) (kind 0)))) (hash "01mzbldrdlrq783f8270d1ckfz0430vmyg8hrlw0hy5g449rjg1n")))

(define-public crate-xfind-0.2 (crate (name "xfind") (vers "0.2.4") (deps (list (crate-dep (name "aho-corasick") (req "^0.7") (default-features #t) (kind 2)) (crate-dep (name "memchr") (req "^2.4") (default-features #t) (kind 0)))) (hash "1rpzkz8zmxl2id0kln7qgbf8jlxgrmydrv6dyygrbz1k4475mvc3")))

(define-public crate-xfind-0.2 (crate (name "xfind") (vers "0.2.5") (deps (list (crate-dep (name "aho-corasick") (req "^0.7") (default-features #t) (kind 2)) (crate-dep (name "memchr") (req "^2.4") (default-features #t) (kind 0)))) (hash "0ky9962ss7dj7zvm5av0dyvzrbm4mbmwpxhqsdmrch4h30s4p2np")))

(define-public crate-xfind-0.2 (crate (name "xfind") (vers "0.2.6") (deps (list (crate-dep (name "aho-corasick") (req "^0.7") (default-features #t) (kind 2)) (crate-dep (name "memchr") (req "^2.4") (default-features #t) (kind 0)))) (hash "156vc2ri90v0hz48yz2441kjmq6fph3gcacnkrir22fjfsim4h0m")))

(define-public crate-xfind-0.2 (crate (name "xfind") (vers "0.2.7") (deps (list (crate-dep (name "aho-corasick") (req "^0.7") (default-features #t) (kind 2)) (crate-dep (name "memchr") (req "^2.4") (default-features #t) (kind 0)))) (hash "1f3rwl87nkcisxb3f0plydsnh1x1n1plhfqkkhd4nb6fnch4alsr")))

