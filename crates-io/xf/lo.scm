(define-module (crates-io xf lo) #:use-module (crates-io))

(define-public crate-xflow-0.1 (crate (name "xflow") (vers "0.1.1") (deps (list (crate-dep (name "libc") (req "^0.1.12") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^0.9") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^0.9") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^0.9") (default-features #t) (kind 0)))) (hash "0318yb0v50b98kw887q5p9i6zpl81f8q7jqm2lpzag64m6zji7qg") (features (quote (("embedded"))))))

