(define-module (crates-io xf ce) #:use-module (crates-io))

(define-public crate-xfce4_natural_scroll-0.1 (crate (name "xfce4_natural_scroll") (vers "0.1.0") (deps (list (crate-dep (name "regex") (req "^1.1") (default-features #t) (kind 0)))) (hash "0s24s0z5m9wrzn2abbjyafgwz0720vg2q4hnydh80f8bfyli9001")))

