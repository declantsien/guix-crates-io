(define-module (crates-io xf bs) #:use-module (crates-io))

(define-public crate-xfbs_euler-0.1 (crate (name "xfbs_euler") (vers "0.1.0") (deps (list (crate-dep (name "bcrypt") (req "^0.7") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^2.33.0") (default-features #t) (kind 0)))) (hash "07bgx0kcp6nspw99cgbvjfshg5fl0a0h2vyc3y1b409q1z1gpc6y") (yanked #t)))

