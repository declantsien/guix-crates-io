(define-module (crates-io xf ft) #:use-module (crates-io))

(define-public crate-xfft-0.1 (crate (name "xfft") (vers "0.1.0") (hash "0css1ffhqy5p5bn16g8fh3rwkdidaj8mfaszxy9riibxnywbbyp6")))

(define-public crate-xfft-0.2 (crate (name "xfft") (vers "0.2.0") (hash "17rhl5s11l5d3npvr9nz88rkazl2cl6qim7l1wf878l7f8nig0cm")))

(define-public crate-xfft-0.2 (crate (name "xfft") (vers "0.2.1") (hash "0a4ylvmcpgi5b6iyrlbvrfj2mndsks2iisxqxp8q2h8s4ixijpgp")))

