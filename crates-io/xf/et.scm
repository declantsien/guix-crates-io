(define-module (crates-io xf et) #:use-module (crates-io))

(define-public crate-xfetch-0.1 (crate (name "xfetch") (vers "0.1.0") (deps (list (crate-dep (name "criterion") (req "^0.2") (default-features #t) (kind 2)) (crate-dep (name "lru") (req "^0.1") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.6") (default-features #t) (kind 0)))) (hash "0qnaz530vb451v0nv048qdgddyqvjar4cqflm17wbr89x4dfyizc")))

(define-public crate-xfetch-1 (crate (name "xfetch") (vers "1.0.0") (deps (list (crate-dep (name "criterion") (req "^0.3.0") (default-features #t) (kind 2)) (crate-dep (name "lru") (req "^0.4.0") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.7.2") (default-features #t) (kind 0)))) (hash "1z5zi3j0ad81dfqpcaijw0alwz71hbr8f0q5l0cnjxqxyb1fgjvs")))

(define-public crate-xfetch-1 (crate (name "xfetch") (vers "1.0.1") (deps (list (crate-dep (name "criterion") (req "^0.3.0") (default-features #t) (kind 2)) (crate-dep (name "lru") (req "^0.5.3") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.7.3") (default-features #t) (kind 0)))) (hash "0wm054pqwwjvxwimj6rn1z3n4c12ys4423sn2scxf2439rfsh59y")))

