(define-module (crates-io xf cc) #:use-module (crates-io))

(define-public crate-xfcc-parser-0.1 (crate (name "xfcc-parser") (vers "0.1.1") (deps (list (crate-dep (name "nom") (req "^7.1.3") (default-features #t) (kind 0)) (crate-dep (name "strum") (req "^0.24.1") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "1zw3704lp1wk4xxfh76rxv2yvxb0jlk6giad877z84w6lzbj1brm")))

(define-public crate-xfcc-parser-0.1 (crate (name "xfcc-parser") (vers "0.1.2") (deps (list (crate-dep (name "nom") (req "^7.1.3") (default-features #t) (kind 0)) (crate-dep (name "strum") (req "^0.25.0") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "1bxg1y1vyk931jx4m4kx80c670jwsam6k26wka13p18skzvy6z29")))

