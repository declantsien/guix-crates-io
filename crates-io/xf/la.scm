(define-module (crates-io xf la) #:use-module (crates-io))

(define-public crate-xflag-0.1 (crate (name "xflag") (vers "0.1.0") (hash "1fp2blkk4sp778f7d01nb1ys18h1h0sllph59x4ir15vdhpimz87")))

(define-public crate-xflags-0.1 (crate (name "xflags") (vers "0.1.0") (deps (list (crate-dep (name "xflags-macros") (req "=0.1.0") (default-features #t) (kind 0)))) (hash "19pbgq3wmixjrnnk7s7xcqbnfx0pcv77dwpb12x4v7n8mird89mx")))

(define-public crate-xflags-0.1 (crate (name "xflags") (vers "0.1.1") (deps (list (crate-dep (name "xflags-macros") (req "=0.1.1") (default-features #t) (kind 0)))) (hash "1ybj7khaiqpa77f5sdan54fs5yc07pbmziy1qywhh6546jjbkglb")))

(define-public crate-xflags-0.1 (crate (name "xflags") (vers "0.1.2") (deps (list (crate-dep (name "xflags-macros") (req "=0.1.2") (default-features #t) (kind 0)))) (hash "1yzs6khbnhgy859xjb7yq289afnx2h48l6x44p5hdz4faawr4qhs")))

(define-public crate-xflags-0.1 (crate (name "xflags") (vers "0.1.3") (deps (list (crate-dep (name "xflags-macros") (req "=0.1.3") (default-features #t) (kind 0)))) (hash "1nx5jq0cb35z9lqp3vzrlqkxpkbgyng1icp1npigh4xq1myb1d6x")))

(define-public crate-xflags-0.1 (crate (name "xflags") (vers "0.1.4") (deps (list (crate-dep (name "xflags-macros") (req "=0.1.4") (default-features #t) (kind 0)))) (hash "0af7mfzg6c96r51dcqlh1v2m4fjbl486s4f5b8qdgiff8d5r2bi2")))

(define-public crate-xflags-0.2 (crate (name "xflags") (vers "0.2.0") (deps (list (crate-dep (name "xflags-macros") (req "=0.2.0") (default-features #t) (kind 0)))) (hash "1fyl8jad233hia9c6a0d0mzdy429wpbzbyl402anrmxx1jsaax4v")))

(define-public crate-xflags-0.2 (crate (name "xflags") (vers "0.2.1") (deps (list (crate-dep (name "xflags-macros") (req "=0.2.1") (default-features #t) (kind 0)))) (hash "12i0m43fmvwhlqid5xbr017c12j7jv3vlkjv04q428mpl3k6rbar")))

(define-public crate-xflags-0.2 (crate (name "xflags") (vers "0.2.2") (deps (list (crate-dep (name "xflags-macros") (req "=0.2.2") (default-features #t) (kind 0)))) (hash "1jb3jch40l6ycj501zixg4yyqm3wi5rwxz8c5czh0bfg1z58anx2")))

(define-public crate-xflags-0.2 (crate (name "xflags") (vers "0.2.3") (deps (list (crate-dep (name "xflags-macros") (req "=0.2.3") (default-features #t) (kind 0)))) (hash "0x9pww5fljpnaj2z3pf53vrhsqvk418p670d8a4777b8gvq58a1c")))

(define-public crate-xflags-0.2 (crate (name "xflags") (vers "0.2.4") (deps (list (crate-dep (name "xflags-macros") (req "=0.2.4") (default-features #t) (kind 0)))) (hash "007fplsi2xqa6smzllrrbpjrwmca99n5hrgmydg2nnhsshggw51z")))

(define-public crate-xflags-0.3 (crate (name "xflags") (vers "0.3.0-pre.1") (deps (list (crate-dep (name "xflags-macros") (req "=0.3.0-pre.1") (default-features #t) (kind 0)))) (hash "1mpkl4sl2fjwj9ndbkhnpcmp69b3naxhzbmrapidbv4qx9zx6ry3")))

(define-public crate-xflags-0.3 (crate (name "xflags") (vers "0.3.0-pre.2") (deps (list (crate-dep (name "xflags-macros") (req "=0.3.0-pre.2") (default-features #t) (kind 0)))) (hash "02w1jzniqxsyl6pmzm2z5654dp51xi0a6wimgs2hiw0w6ncd1za0")))

(define-public crate-xflags-0.3 (crate (name "xflags") (vers "0.3.0") (deps (list (crate-dep (name "xflags-macros") (req "=0.3.0") (default-features #t) (kind 0)))) (hash "0s6jxyqksqfk35vaikj6k43q7211c60nzqgddzli5a516589zwfb")))

(define-public crate-xflags-0.3 (crate (name "xflags") (vers "0.3.1") (deps (list (crate-dep (name "xflags-macros") (req "=0.3.1") (default-features #t) (kind 0)))) (hash "1sn2y82vsjvmjd4j9rxg3pb28ig3dj7nphb9hciwml120mc4nmf4")))

(define-public crate-xflags-0.3 (crate (name "xflags") (vers "0.3.2") (deps (list (crate-dep (name "xflags-macros") (req "=0.3.2") (default-features #t) (kind 0)))) (hash "0i490jzw7z9riqfnyginkc07j4k7isr19qq6055lamfyngxib7kx")))

(define-public crate-xflags-0.4 (crate (name "xflags") (vers "0.4.0-pre.1") (deps (list (crate-dep (name "xflags-macros") (req "=0.4.0-pre.1") (default-features #t) (kind 0)))) (hash "0gx6855vz986m54aji2xgkyglff7j9637vcpz5y2gdygabdw15s6")))

(define-public crate-xflags-macros-0.1 (crate (name "xflags-macros") (vers "0.1.0") (deps (list (crate-dep (name "expect-test") (req "^1") (default-features #t) (kind 2)) (crate-dep (name "proc-macro2") (req "^1") (default-features #t) (kind 0)))) (hash "0sdngvzip1hc2abq4b38k9fnsjimzlarx7s3m6x833bf1i4zyb7f")))

(define-public crate-xflags-macros-0.1 (crate (name "xflags-macros") (vers "0.1.1") (deps (list (crate-dep (name "expect-test") (req "^1") (default-features #t) (kind 2)) (crate-dep (name "proc-macro2") (req "^1") (default-features #t) (kind 0)))) (hash "00jrp37a0kqrb42wmllhcry8dp0d5md0di5d2xbmww3gf3wnc1nf")))

(define-public crate-xflags-macros-0.1 (crate (name "xflags-macros") (vers "0.1.2") (deps (list (crate-dep (name "expect-test") (req "^1") (default-features #t) (kind 2)) (crate-dep (name "proc-macro2") (req "^1") (default-features #t) (kind 0)))) (hash "17nqddf555mxb9x1kk62bjmhqzj4a6gsbvg45qznb8291va0h8ds")))

(define-public crate-xflags-macros-0.1 (crate (name "xflags-macros") (vers "0.1.3") (deps (list (crate-dep (name "expect-test") (req "^1") (default-features #t) (kind 2)) (crate-dep (name "proc-macro2") (req "^1") (default-features #t) (kind 0)))) (hash "1qfhvahn5f417zw8zpaj7627g4r7xnyz24q91pfxbsbcknlniqgq")))

(define-public crate-xflags-macros-0.1 (crate (name "xflags-macros") (vers "0.1.4") (deps (list (crate-dep (name "expect-test") (req "^1") (default-features #t) (kind 2)) (crate-dep (name "proc-macro2") (req "^1") (default-features #t) (kind 0)))) (hash "1paxwap6vl8dgzwpap2d1f93hfac2vv794jvklh5xyd799dqzwaj")))

(define-public crate-xflags-macros-0.2 (crate (name "xflags-macros") (vers "0.2.0") (deps (list (crate-dep (name "expect-test") (req "^1") (default-features #t) (kind 2)) (crate-dep (name "proc-macro2") (req "^1") (default-features #t) (kind 0)))) (hash "1xng9yg0a4q340r421yb02blfzcvwv5sygj17sc5283x5vgzfx0h")))

(define-public crate-xflags-macros-0.2 (crate (name "xflags-macros") (vers "0.2.1") (deps (list (crate-dep (name "expect-test") (req "^1") (default-features #t) (kind 2)) (crate-dep (name "proc-macro2") (req "^1") (default-features #t) (kind 0)))) (hash "1jb2bq76kzzmq5rdyi3hzkq6x41l11fr1yn00f5ib5j9l4y7s0y8")))

(define-public crate-xflags-macros-0.2 (crate (name "xflags-macros") (vers "0.2.2") (deps (list (crate-dep (name "expect-test") (req "^1") (default-features #t) (kind 2)) (crate-dep (name "proc-macro2") (req "^1") (default-features #t) (kind 0)))) (hash "1w2vlv6x6fcphywv74dgyddy4960pd1cpzhjj1l137w3v0454180")))

(define-public crate-xflags-macros-0.2 (crate (name "xflags-macros") (vers "0.2.3") (deps (list (crate-dep (name "expect-test") (req "^1") (default-features #t) (kind 2)) (crate-dep (name "proc-macro2") (req "^1") (default-features #t) (kind 2)))) (hash "1k4zqrg18y1922jkbqk18nh8x917hbdk1sd06d3rmlba8w0h69ns")))

(define-public crate-xflags-macros-0.2 (crate (name "xflags-macros") (vers "0.2.4") (deps (list (crate-dep (name "expect-test") (req "^1") (default-features #t) (kind 2)) (crate-dep (name "proc-macro2") (req "^1") (default-features #t) (kind 2)))) (hash "08hhj8bmh17bln3a6zyx8qkc9crk0nl0q5wbxpnqfwm9q9givla5")))

(define-public crate-xflags-macros-0.3 (crate (name "xflags-macros") (vers "0.3.0-pre.1") (deps (list (crate-dep (name "expect-test") (req "^1") (default-features #t) (kind 2)) (crate-dep (name "proc-macro2") (req "^1") (default-features #t) (kind 2)))) (hash "1zrv27w6qw4p2svwjxy3j6kvbc2s2kr59hzzn2k83yj009wp5dl4")))

(define-public crate-xflags-macros-0.3 (crate (name "xflags-macros") (vers "0.3.0-pre.2") (deps (list (crate-dep (name "expect-test") (req "^1") (default-features #t) (kind 2)) (crate-dep (name "proc-macro2") (req "^1") (default-features #t) (kind 2)))) (hash "1cxh421nd8638h57ndag8bslb19apgs36k8ywfr42fcmhsbl5a6k")))

(define-public crate-xflags-macros-0.3 (crate (name "xflags-macros") (vers "0.3.0") (deps (list (crate-dep (name "expect-test") (req "^1") (default-features #t) (kind 2)) (crate-dep (name "proc-macro2") (req "^1") (default-features #t) (kind 2)))) (hash "028q4pa28mb0ga5pd64324k4w3cwykgwbw25vp9cmdlv0grdgyra")))

(define-public crate-xflags-macros-0.3 (crate (name "xflags-macros") (vers "0.3.1") (deps (list (crate-dep (name "expect-test") (req "^1") (default-features #t) (kind 2)) (crate-dep (name "proc-macro2") (req "^1") (default-features #t) (kind 2)))) (hash "1a6123c3qy1sqjsvlc357b2w8vr161vnlyxqwsm96w4pm0y7p3pm")))

(define-public crate-xflags-macros-0.3 (crate (name "xflags-macros") (vers "0.3.2") (deps (list (crate-dep (name "expect-test") (req "^1") (default-features #t) (kind 2)) (crate-dep (name "proc-macro2") (req "^1") (default-features #t) (kind 2)))) (hash "0md1sh3h0yadrhjz2mh50xc65p0kx8qh19jvqbva5zx7zva26937")))

(define-public crate-xflags-macros-0.4 (crate (name "xflags-macros") (vers "0.4.0-pre.1") (deps (list (crate-dep (name "expect-test") (req "^1") (default-features #t) (kind 2)) (crate-dep (name "proc-macro2") (req "^1") (default-features #t) (kind 2)))) (hash "0p239idnvp3c9vrn31znc5ibij98m3w5cwsqxmq1fc9nl70qmlcl")))

