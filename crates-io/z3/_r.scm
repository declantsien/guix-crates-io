(define-module (crates-io z3 _r) #:use-module (crates-io))

(define-public crate-z3_ref-0.1 (crate (name "z3_ref") (vers "0.1.0") (deps (list (crate-dep (name "bindgen") (req "^0.40.0") (default-features #t) (kind 1)))) (hash "0pgdywn2x5ar48q51wlf250wbf1lxjviwm0alx4g3vfpdq268ca8")))

(define-public crate-z3_ref-0.1 (crate (name "z3_ref") (vers "0.1.1") (deps (list (crate-dep (name "bindgen") (req "^0.40.0") (default-features #t) (kind 1)))) (hash "0fyy3ayqsin1d2ik91p1wv0s8fiq841xiqcm7hibfi26lqwcir0d")))

(define-public crate-z3_ref-0.1 (crate (name "z3_ref") (vers "0.1.2") (deps (list (crate-dep (name "bindgen") (req "^0.40.0") (default-features #t) (kind 1)))) (hash "0xm651aiq0d7cizmkf3cgv44f4vm86a4k9g0gqj5gn9lxs1p4qvb")))

(define-public crate-z3_ref-0.1 (crate (name "z3_ref") (vers "0.1.3") (deps (list (crate-dep (name "bindgen") (req "^0.40.0") (default-features #t) (kind 1)))) (hash "0hgr7rbqxwbby9ja9wrqwxr53635ndcffbr0rayxmm5cfgfr345q")))

(define-public crate-z3_ref-0.1 (crate (name "z3_ref") (vers "0.1.4") (deps (list (crate-dep (name "bindgen") (req "^0.40.0") (default-features #t) (kind 1)))) (hash "175pbk827hhl8svjngh56ygnxgd2y9fjn0n90rf2dpl235p78dzl")))

