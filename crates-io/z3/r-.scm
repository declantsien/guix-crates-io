(define-module (crates-io z3 r-) #:use-module (crates-io))

(define-public crate-z3r-sramr-0.1 (crate (name "z3r-sramr") (vers "0.1.1") (deps (list (crate-dep (name "anyhow") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "byteorder") (req "^1") (default-features #t) (kind 0)))) (hash "1lvyjjhxnrqf7av9hax9q0gify8vpyp21akwk0x506hs7l712cbz")))

(define-public crate-z3r-sramr-0.2 (crate (name "z3r-sramr") (vers "0.2.0") (deps (list (crate-dep (name "anyhow") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "byteorder") (req "^1") (default-features #t) (kind 0)))) (hash "17lqc53bw22a8ny84wq5vc81xqvwbchv4vba21dm2gqbjypi0pvb")))

(define-public crate-z3r-sramr-0.2 (crate (name "z3r-sramr") (vers "0.2.3") (deps (list (crate-dep (name "anyhow") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "byteorder") (req "^1") (default-features #t) (kind 0)))) (hash "0w390nx7nfn3iw92xwn0820212vyczkay36asvq12jfza9l2swrp")))

