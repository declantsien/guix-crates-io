(define-module (crates-io z3 -s) #:use-module (crates-io))

(define-public crate-z3-sys-0.1 (crate (name "z3-sys") (vers "0.1.0") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "va_list") (req "^0.0.1") (default-features #t) (kind 0)))) (hash "164v430777qfhz9qvi9vb9mqki70q5jyf306hmajpyzb7ff3db89")))

(define-public crate-z3-sys-0.2 (crate (name "z3-sys") (vers "0.2.0") (hash "1dfqyx9g2jjkal5kmh9b2hl89p062c5q4xy5vsg9mr2yhnx0md76")))

(define-public crate-z3-sys-0.3 (crate (name "z3-sys") (vers "0.3.0") (hash "0gcngw03wjqmjv807618b152l96hqqzlifipnw2cvns243987dmr")))

(define-public crate-z3-sys-0.4 (crate (name "z3-sys") (vers "0.4.0") (hash "00255l78bkzxz0l6y6z0l9qg366d39j91mnb1al6z71l28vr1zqx")))

(define-public crate-z3-sys-0.5 (crate (name "z3-sys") (vers "0.5.0") (hash "0cfy6y0zy3g6576fph6c48js3n1cmkgf98rfpw7n5hmb3v1v8mxf")))

(define-public crate-z3-sys-0.6 (crate (name "z3-sys") (vers "0.6.0") (deps (list (crate-dep (name "cmake") (req "^0.1") (optional #t) (default-features #t) (kind 1)))) (hash "0cr8dhs7f42vi9810989amasw2b15lv8bpr5i674wjfib52rg1r4") (features (quote (("static-link-z3" "cmake")))) (links "z3")))

(define-public crate-z3-sys-0.6 (crate (name "z3-sys") (vers "0.6.1") (deps (list (crate-dep (name "cmake") (req "^0.1") (optional #t) (default-features #t) (kind 1)))) (hash "1bhimqgb7amcyg16p4rhnf562l2l3ql52qdqpwnx0m6bzahxh0j0") (features (quote (("static-link-z3" "cmake")))) (links "z3")))

(define-public crate-z3-sys-0.6 (crate (name "z3-sys") (vers "0.6.2") (deps (list (crate-dep (name "cmake") (req "^0.1") (optional #t) (default-features #t) (kind 1)))) (hash "08as34ky9b9n9wyyjik3hgl991983d11i4lwsvbnvf4fmspkr1p1") (features (quote (("static-link-z3" "cmake")))) (links "z3")))

(define-public crate-z3-sys-0.6 (crate (name "z3-sys") (vers "0.6.3") (deps (list (crate-dep (name "cmake") (req "^0.1") (optional #t) (default-features #t) (kind 1)))) (hash "1q0xh5iqar5nvwyww31inz6yj7zrj7yw6h5lzx0kx4ylzfjqp8dg") (features (quote (("static-link-z3" "cmake")))) (links "z3")))

(define-public crate-z3-sys-0.7 (crate (name "z3-sys") (vers "0.7.0") (deps (list (crate-dep (name "bindgen") (req "^0.58") (default-features #t) (kind 1)) (crate-dep (name "cmake") (req "^0.1") (optional #t) (default-features #t) (kind 1)))) (hash "0pz7kkmb9qmri1dglak5043wa2rskswpxkmjq4lbc6ihp64jysx0") (features (quote (("static-link-z3" "cmake")))) (links "z3")))

(define-public crate-z3-sys-0.7 (crate (name "z3-sys") (vers "0.7.1") (deps (list (crate-dep (name "bindgen") (req "^0.58") (default-features #t) (kind 1)) (crate-dep (name "cmake") (req "^0.1") (optional #t) (default-features #t) (kind 1)))) (hash "19l63517glil9wj5gp0gxmmd7hyn5g5nc02j9jcz2jxyb2ydr0hw") (features (quote (("static-link-z3" "cmake")))) (links "z3")))

(define-public crate-z3-sys-0.8 (crate (name "z3-sys") (vers "0.8.0") (deps (list (crate-dep (name "bindgen") (req "^0.66.0") (features (quote ("runtime"))) (kind 1)) (crate-dep (name "cmake") (req "^0.1.49") (optional #t) (default-features #t) (kind 1)))) (hash "106d86abhw8yc8g8r8ahswgzd5p94i1cz33gwdv4csx2npwb0cg8") (features (quote (("static-link-z3" "cmake")))) (links "z3")))

(define-public crate-z3-sys-0.8 (crate (name "z3-sys") (vers "0.8.1") (deps (list (crate-dep (name "bindgen") (req "^0.66.0") (features (quote ("runtime"))) (kind 1)) (crate-dep (name "cmake") (req "^0.1.49") (optional #t) (default-features #t) (kind 1)))) (hash "1c7l61a2zvsy2l5gyagaahi5d0kad3ab0jag80mz9qqdpkyp1kyp") (features (quote (("static-link-z3" "cmake")))) (links "z3")))

