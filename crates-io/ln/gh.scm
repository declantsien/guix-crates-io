(define-module (crates-io ln gh) #:use-module (crates-io))

(define-public crate-lngh_strings-0.1 (crate (name "lngh_strings") (vers "0.1.0") (hash "05r3b4mwhlvmqh27yp0j4b4r9xj4gabr8jmyx3dy1cpjpwrm242l")))

(define-public crate-lngh_strings-0.2 (crate (name "lngh_strings") (vers "0.2.0") (hash "0yw8ni65km6rxabyphj4565l1jvl7fgyd8fib1jk1dbzv00bc82x")))

