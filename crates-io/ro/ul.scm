(define-module (crates-io ro ul) #:use-module (crates-io))

(define-public crate-rouler-0.1 (crate (name "rouler") (vers "0.1.0") (deps (list (crate-dep (name "pest") (req "^0.4.0") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.3.0") (default-features #t) (kind 0)))) (hash "0w4ndjslnrwvab45bsnaa0sz4h3miwnl2qaqskq6bdqplnm6qrph")))

(define-public crate-rouler-0.1 (crate (name "rouler") (vers "0.1.1") (deps (list (crate-dep (name "pest") (req "^0.4.0") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.3.0") (default-features #t) (kind 0)))) (hash "1yq07hjl1hz02kjvn1c3klig4vxjmzsncncdvyz52vyhfgq73s3j")))

(define-public crate-rouler-0.1 (crate (name "rouler") (vers "0.1.2") (deps (list (crate-dep (name "pest") (req "^0.4.0") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.3.0") (default-features #t) (kind 0)))) (hash "0b6z2c2k6j3cbx3pkd14srsxdjjcq6vmbrw0yxhgxsxcrmzg41ii")))

(define-public crate-rouler-0.1 (crate (name "rouler") (vers "0.1.3") (deps (list (crate-dep (name "pest") (req "^0.4.0") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.3.0") (default-features #t) (kind 0)))) (hash "1j6pkynhg26ggw818rqzd431aal7i6jmf3ipcj8g4xkkmmxplhni")))

(define-public crate-rouler-0.2 (crate (name "rouler") (vers "0.2.0") (deps (list (crate-dep (name "lazy_static") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "pest") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "pest_derive") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.5") (default-features #t) (kind 0)))) (hash "030ipp38wpbb0hykn0pv3l8n3dd3ffkn8cskg2zzgkl6lqbf0r8v")))

(define-public crate-rouler-0.2 (crate (name "rouler") (vers "0.2.1") (deps (list (crate-dep (name "lazy_static") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "pest") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "pest_derive") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.5") (default-features #t) (kind 0)))) (hash "1lsd98p8ssrny9ihgzklw32fnxg6134bw9wq0ijbdn6k8d2zgi65")))

(define-public crate-rouler-0.2 (crate (name "rouler") (vers "0.2.2") (deps (list (crate-dep (name "lazy_static") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "pest") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "pest_derive") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.5") (default-features #t) (kind 0)))) (hash "00rrbi4c1p60x3hbwj89x3vakbckm6318n99w1ynczbjcp9m3y9h")))

(define-public crate-roulette-0.1 (crate (name "roulette") (vers "0.1.0") (deps (list (crate-dep (name "rand") (req "*") (default-features #t) (kind 0)))) (hash "1w49f2qyc3na3p8mzg5v50iap796d55wlp9jbw8nbwdkv78cddsx")))

(define-public crate-roulette-0.2 (crate (name "roulette") (vers "0.2.0") (deps (list (crate-dep (name "rand") (req "^0.5") (default-features #t) (kind 0)))) (hash "000f0apzyq59gjmrp8jq35f5nskmlga1rajzgl7hm8m3iadgrd26")))

(define-public crate-roulette-0.3 (crate (name "roulette") (vers "0.3.0") (deps (list (crate-dep (name "rand") (req "^0.7.0") (default-features #t) (kind 0)))) (hash "0s381hl0s7rhp3yhd1lx7apjcrb2h5pj2mfp8sa07k930q4swpxg")))

(define-public crate-roulette-wheel-0.1 (crate (name "roulette-wheel") (vers "0.1.0") (deps (list (crate-dep (name "rand") (req "^0.3") (default-features #t) (kind 0)))) (hash "0gndw4cdcfyjaawr3fmdaggm1zkwh90i2ml31wpfx8knxc9kkr6j")))

(define-public crate-roulette-wheel-0.2 (crate (name "roulette-wheel") (vers "0.2.0") (deps (list (crate-dep (name "rand") (req "^0.3") (default-features #t) (kind 0)))) (hash "0nrjcp85xlrai4p8c4247rz4y4ihy3xa3ld7fjn8kxsqi37jm6s4")))

(define-public crate-roulette-wheel-0.2 (crate (name "roulette-wheel") (vers "0.2.1") (deps (list (crate-dep (name "rand") (req "^0.3") (default-features #t) (kind 0)))) (hash "036v0hwm06zika68wl3zr3434fszwgjnbpbmknfsbg4k34cbjxn1")))

(define-public crate-roulette-wheel-0.2 (crate (name "roulette-wheel") (vers "0.2.2") (deps (list (crate-dep (name "rand") (req "^0.3") (default-features #t) (kind 0)))) (hash "0m9y45x0ssx9afwc1x02ckch06d6iqg45skxld59yqg1v1q1hhb7")))

