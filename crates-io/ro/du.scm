(define-module (crates-io ro du) #:use-module (crates-io))

(define-public crate-rodu-0.1 (crate (name "rodu") (vers "0.1.1") (deps (list (crate-dep (name "git2") (req "^0.18.1") (default-features #t) (kind 0)))) (hash "10d20qi285lnfp6fc8p09y0csadsf0wyzn39p8v4hwiwl5mid4l0")))

(define-public crate-rodu-0.1 (crate (name "rodu") (vers "0.1.3") (deps (list (crate-dep (name "git2") (req "^0.18.1") (default-features #t) (kind 0)))) (hash "1p4a3cqwl6wdvz7qiybisswbvwg2r54ig6jqpcaxwznfd6wi21g0")))

