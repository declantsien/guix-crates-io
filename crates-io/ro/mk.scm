(define-module (crates-io ro mk) #:use-module (crates-io))

(define-public crate-romkan-0.2 (crate (name "romkan") (vers "0.2.1") (deps (list (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "phf") (req "^0.9") (features (quote ("macros"))) (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.5.4") (default-features #t) (kind 0)))) (hash "1crc8p5p8q8i4dw2p75pzxb1njkqd10qdfc6w14r4w0rwjrffrjj")))

(define-public crate-romkan-0.2 (crate (name "romkan") (vers "0.2.2") (deps (list (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "phf") (req "^0.9") (features (quote ("macros"))) (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.5.4") (default-features #t) (kind 0)))) (hash "0bdn24c5siac03k5l13nhcg6hq37v7xls7fgbjanc1nxcvl6x51m")))

