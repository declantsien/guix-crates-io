(define-module (crates-io ro id) #:use-module (crates-io))

(define-public crate-roid-0.1 (crate (name "roid") (vers "0.1.1") (deps (list (crate-dep (name "clap") (req "^2.33.3") (default-features #t) (kind 0)) (crate-dep (name "dirs") (req "^4.0.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.126") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "toml") (req "^0.5") (default-features #t) (kind 0)))) (hash "1bkga4kkzf4hnypq47hzcbs3s7jpm5xlhclmb9c5lw85mn67v3r6")))

(define-public crate-roid-0.1 (crate (name "roid") (vers "0.1.2") (deps (list (crate-dep (name "clap") (req "^2.33.3") (default-features #t) (kind 0)) (crate-dep (name "dirs") (req "^4.0.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.126") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "toml") (req "^0.5") (default-features #t) (kind 0)))) (hash "1znqq6zc1jq7hvjsqadd8zi74gs3k585gbph6c9g2ds2m6yx1v6h")))

(define-public crate-roids-0.0.0 (crate (name "roids") (vers "0.0.0") (hash "0vl8n1ckcq5lmn6szsv18a9ybvh9qin1a5vmyi08f4l6g24f8c3j")))

