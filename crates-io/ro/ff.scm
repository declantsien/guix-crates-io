(define-module (crates-io ro ff) #:use-module (crates-io))

(define-public crate-roff-0.1 (crate (name "roff") (vers "0.1.0") (deps (list (crate-dep (name "duct") (req "^0.10.0") (default-features #t) (kind 2)) (crate-dep (name "pretty_assertions") (req "^0.5.1") (default-features #t) (kind 2)))) (hash "0pk76fw9hqnvr8qbd5r8yq08zpgymk14wgkn5h2qhs54gfrlygp3")))

(define-public crate-roff-0.2 (crate (name "roff") (vers "0.2.0") (deps (list (crate-dep (name "duct") (req "^0.13") (default-features #t) (kind 2)) (crate-dep (name "pretty_assertions") (req "^1.0.0") (default-features #t) (kind 2)))) (hash "0l3dpqy846awxpwq6cm9y39khwkflbm70r5zd85rymbny35jck2g")))

(define-public crate-roff-0.2 (crate (name "roff") (vers "0.2.1") (deps (list (crate-dep (name "duct") (req "^0.13") (default-features #t) (kind 2)) (crate-dep (name "pretty_assertions") (req "^1.0.0") (default-features #t) (kind 2)))) (hash "05j324x84xkgp848smhnknnlpl70833xb9lalqg4n2ga6k8dhcxq")))

(define-public crate-roffman-0.1 (crate (name "roffman") (vers "0.1.0") (hash "1fp2dhll9556bkgzmrqaabl696fjiyh04smq7bj0abngbsf1zjx1")))

(define-public crate-roffman-0.2 (crate (name "roffman") (vers "0.2.0") (hash "07bnrwjv9brsgqgyi1imwpd1jk33a1ckyqda87is37p37ijhzxvz")))

(define-public crate-roffman-0.3 (crate (name "roffman") (vers "0.3.0") (hash "08kdjh0yhabprf5daa0a1k7sndywybin1hi7j8c5mx8365p02b7n")))

(define-public crate-roffman-0.4 (crate (name "roffman") (vers "0.4.0") (hash "04m1krlm47950073jm38pjyaixf7r7nyivsr5d177895is49xwhs")))

