(define-module (crates-io ro br) #:use-module (crates-io))

(define-public crate-robrix-0.0.1 (crate (name "robrix") (vers "0.0.1-pre-alpha") (deps (list (crate-dep (name "matrix-sdk") (req "^0.6.2") (default-features #t) (kind 0)))) (hash "1f5a1dffsbpxczisal6wplfc1g0ii33azwb5brfb88470dwa65a6")))

