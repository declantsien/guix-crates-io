(define-module (crates-io ro rd) #:use-module (crates-io))

(define-public crate-rordle-0.1 (crate (name "rordle") (vers "0.1.0") (deps (list (crate-dep (name "rand") (req "^0.8.4") (default-features #t) (kind 0)) (crate-dep (name "termion") (req "^1.5.6") (default-features #t) (kind 0)))) (hash "0d1yps2bbjsdlwa4byp82kb2y965nvaydmbfl1h8fw262hgcyn04")))

(define-public crate-rordle-0.1 (crate (name "rordle") (vers "0.1.1") (deps (list (crate-dep (name "rand") (req "^0.8.4") (default-features #t) (kind 0)) (crate-dep (name "termion") (req "^1.5.6") (default-features #t) (kind 0)))) (hash "08wa14zsjcf3l8l51hkdncr9ag1ws2dnv43q4vjjqkv0d2gwf4q4")))

(define-public crate-rordle-0.1 (crate (name "rordle") (vers "0.1.2") (deps (list (crate-dep (name "rand") (req "^0.8.4") (default-features #t) (kind 0)) (crate-dep (name "termion") (req "^1.5.6") (default-features #t) (kind 0)))) (hash "13sg4wkjqj9c5a0b9qkcyyribypza66j7g4682dgyby2z0ri3zw4")))

(define-public crate-rordle-0.2 (crate (name "rordle") (vers "0.2.0") (deps (list (crate-dep (name "clap") (req "^3.0.7") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.4") (default-features #t) (kind 0)) (crate-dep (name "termion") (req "^1.5.6") (default-features #t) (kind 0)))) (hash "1arrn7ph5bg3wkvcr2x3s566c0rrar8sib55hihz5wg1bz035z7g")))

