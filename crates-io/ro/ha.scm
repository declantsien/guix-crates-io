(define-module (crates-io ro ha) #:use-module (crates-io))

(define-public crate-rohanasan-0.1 (crate (name "rohanasan") (vers "0.1.2") (deps (list (crate-dep (name "libc") (req "^0.2.153") (default-features #t) (kind 0)))) (hash "0yc69x5hgjfqkap5f60szhjsqx3674w23gfz76z50rkb6dvyfmmk")))

(define-public crate-rohanasan-0.1 (crate (name "rohanasan") (vers "0.1.1") (deps (list (crate-dep (name "libc") (req "^0.2.153") (default-features #t) (kind 0)))) (hash "1ll3yif9srf6d02hrqh2l14phqxji9fki9pjdfc0g335vkd6r7bs")))

(define-public crate-rohanasan-0.1 (crate (name "rohanasan") (vers "0.1.4") (deps (list (crate-dep (name "libc") (req "^0.2.153") (default-features #t) (kind 0)))) (hash "0nmwv77fvcvns6fwscrb4i6gzwil0smfny16zn683lg93yclas6c")))

(define-public crate-rohanasan-0.1 (crate (name "rohanasan") (vers "0.1.5") (deps (list (crate-dep (name "libc") (req "^0.2.153") (default-features #t) (kind 0)))) (hash "1n73vjrhjpd7sq2gb720aw984rlxb9zd9id5p7g9zaj20kcfkd0f")))

(define-public crate-rohanasan-0.2 (crate (name "rohanasan") (vers "0.2.0") (deps (list (crate-dep (name "libc") (req "^0.2.153") (default-features #t) (kind 0)))) (hash "0jfn119n6y5nsvq1bhk0hkkgd0dv7s0d8rycc49fzdqrj7sxhpyy")))

(define-public crate-rohanasan-0.2 (crate (name "rohanasan") (vers "0.2.1") (deps (list (crate-dep (name "libc") (req "^0.2.153") (default-features #t) (kind 0)))) (hash "1rg59n29p32pjagxgzrfxbgp8d8iqlfs65zrwk0amshfc45rnv6i")))

(define-public crate-rohanasan-0.2 (crate (name "rohanasan") (vers "0.2.5") (deps (list (crate-dep (name "libc") (req "^0.2.153") (default-features #t) (kind 0)))) (hash "1a4bap26j8jrppjrsps62lc9fmhldfsfd43c179nab2kwi40jffr")))

(define-public crate-rohanasan-0.2 (crate (name "rohanasan") (vers "0.2.9") (deps (list (crate-dep (name "async-std") (req "^1.12.0") (features (quote ("attributes"))) (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2.153") (default-features #t) (kind 0)) (crate-dep (name "urldecode") (req "^0.1.1") (default-features #t) (kind 0)))) (hash "192q3yjd1rr3iyz2qpl7ndb477hsivib7vrd0llz05cssf1pj510") (yanked #t)))

(define-public crate-rohanasan-0.4 (crate (name "rohanasan") (vers "0.4.0") (deps (list (crate-dep (name "async-std") (req "^1.12.0") (features (quote ("attributes"))) (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2.153") (default-features #t) (kind 0)) (crate-dep (name "urldecode") (req "^0.1.1") (default-features #t) (kind 0)))) (hash "0kj73khhklbba07w5k31r5yi910767k8v4xyvim1krgn0mcqzgfj") (yanked #t)))

(define-public crate-rohanasan-0.4 (crate (name "rohanasan") (vers "0.4.1") (deps (list (crate-dep (name "async-std") (req "^1.12.0") (features (quote ("attributes"))) (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2.153") (default-features #t) (kind 0)) (crate-dep (name "urldecode") (req "^0.1.1") (default-features #t) (kind 0)))) (hash "07mi7vzgzbasyc93mbzhry2hkna0jpjvrw415yg2r9i7ikd295c8") (yanked #t)))

(define-public crate-rohanasan-0.4 (crate (name "rohanasan") (vers "0.4.2") (deps (list (crate-dep (name "async-std") (req "^1.12.0") (features (quote ("attributes"))) (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2.153") (default-features #t) (kind 0)) (crate-dep (name "urldecode") (req "^0.1.1") (default-features #t) (kind 0)))) (hash "1xh439anxfrixws06jrrdc9my3rdw4nl4xm8pcy0figfqydp2a61") (yanked #t)))

(define-public crate-rohanasan-0.4 (crate (name "rohanasan") (vers "0.4.3") (deps (list (crate-dep (name "async-std") (req "^1.12.0") (features (quote ("attributes"))) (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2.153") (default-features #t) (kind 0)) (crate-dep (name "urldecode") (req "^0.1.1") (default-features #t) (kind 0)))) (hash "04kharp7mxgfg5my6kjylg55yzwp1n5nwsplm2r6x4l8zqh387sr") (yanked #t)))

(define-public crate-rohanasan-0.4 (crate (name "rohanasan") (vers "0.4.4") (deps (list (crate-dep (name "async-std") (req "^1.12.0") (features (quote ("attributes"))) (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2.153") (default-features #t) (kind 0)) (crate-dep (name "urldecode") (req "^0.1.1") (default-features #t) (kind 0)))) (hash "0ba3aw70w2ja16wvpxhdiw15mxh3n807kc0b38rh2jljrwl15z1m")))

(define-public crate-rohanasan-0.4 (crate (name "rohanasan") (vers "0.4.49") (deps (list (crate-dep (name "async-std") (req "^1.12.0") (features (quote ("attributes"))) (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2.153") (default-features #t) (kind 0)) (crate-dep (name "urldecode") (req "^0.1.1") (default-features #t) (kind 0)))) (hash "0q3f9l59iq3x3l33bm5spy0kk5xh93rxa26zn79z7kay57alv47j")))

(define-public crate-rohanasan-0.4 (crate (name "rohanasan") (vers "0.4.54") (deps (list (crate-dep (name "async-std") (req "^1.12.0") (features (quote ("attributes"))) (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2.153") (default-features #t) (kind 0)) (crate-dep (name "urldecode") (req "^0.1.1") (default-features #t) (kind 0)))) (hash "1s518d52whv1xf23s80k3wprh7x1j5h9kcsy7wvniyndvr8a4fh9")))

(define-public crate-rohanasan-0.4 (crate (name "rohanasan") (vers "0.4.55") (deps (list (crate-dep (name "async-std") (req "^1.12.0") (features (quote ("attributes"))) (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2.153") (default-features #t) (kind 0)) (crate-dep (name "urldecode") (req "^0.1.1") (default-features #t) (kind 0)))) (hash "1inwj0a8v2ipppl96qv6lv1caqxl8waxs7k18hpgysn06f9j47g6")))

(define-public crate-rohanasan-0.4 (crate (name "rohanasan") (vers "0.4.56") (deps (list (crate-dep (name "async-std") (req "^1.12.0") (features (quote ("attributes"))) (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2.153") (default-features #t) (kind 0)) (crate-dep (name "urldecode") (req "^0.1.1") (default-features #t) (kind 0)))) (hash "0sxw0dmnkqrm7dv5hyzl09ydwvggkx5y5bddl35rw4pcrldlyjzp")))

(define-public crate-rohanasan-0.4 (crate (name "rohanasan") (vers "0.4.57") (deps (list (crate-dep (name "async-std") (req "^1.12.0") (features (quote ("attributes"))) (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2.153") (default-features #t) (kind 0)) (crate-dep (name "urldecode") (req "^0.1.1") (default-features #t) (kind 0)))) (hash "187mnfw9bfhyv9d0jszhhbbhi767j00485p3wh4d4g24w3gr3rpv")))

(define-public crate-rohanasan-0.4 (crate (name "rohanasan") (vers "0.4.60") (deps (list (crate-dep (name "async-std") (req "^1.12.0") (features (quote ("attributes"))) (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2.153") (default-features #t) (kind 0)) (crate-dep (name "urldecode") (req "^0.1.1") (default-features #t) (kind 0)))) (hash "1iznpixygn50p945aa9adplzif1m0m2ab8px51vs6czfxdb4jwdh")))

(define-public crate-rohanasan-0.4 (crate (name "rohanasan") (vers "0.4.90") (deps (list (crate-dep (name "async-std") (req "^1.12.0") (features (quote ("attributes"))) (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2.153") (default-features #t) (kind 0)) (crate-dep (name "urldecode") (req "^0.1.1") (default-features #t) (kind 0)))) (hash "1fal9qiinavdq01mbggpj0xjc8nlqrq8ynmnw3yspsp732fam7zp")))

(define-public crate-rohanasan-0.4 (crate (name "rohanasan") (vers "0.4.91") (deps (list (crate-dep (name "async-std") (req "^1.12.0") (features (quote ("attributes"))) (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2.153") (default-features #t) (kind 0)) (crate-dep (name "urldecode") (req "^0.1.1") (default-features #t) (kind 0)))) (hash "0v6gzp05bins2r4xdwkplwdzi7l7f2i5c0jk335v130qhiw4lv9f")))

(define-public crate-rohanasan-0.5 (crate (name "rohanasan") (vers "0.5.0") (deps (list (crate-dep (name "async-std") (req "^1.12.0") (default-features #t) (kind 0)))) (hash "04pn4cwdfah074kmj01jwzghwgvdw49a72bdkbjwx9nivhkiwiwh")))

(define-public crate-rohanasan-0.5 (crate (name "rohanasan") (vers "0.5.1") (deps (list (crate-dep (name "async-std") (req "^1.12.0") (default-features #t) (kind 0)))) (hash "1rdwdblnza5yq41mfk4xc049h7f6s2pxfvnyxka7862lyc2pyr0m")))

(define-public crate-rohanasan-0.5 (crate (name "rohanasan") (vers "0.5.2") (deps (list (crate-dep (name "async-std") (req "^1.12.0") (default-features #t) (kind 0)) (crate-dep (name "urldecode") (req "^0.1.1") (default-features #t) (kind 0)))) (hash "173piy3cc4y08sb46xdaxg8wnvd5zczvpy3zm1bjgqzx05kalvnj")))

(define-public crate-rohanasan-0.5 (crate (name "rohanasan") (vers "0.5.21") (deps (list (crate-dep (name "async-std") (req "^1.12.0") (default-features #t) (kind 0)) (crate-dep (name "urldecode") (req "^0.1.1") (default-features #t) (kind 0)))) (hash "0czs0kacmgsvvq2p4yjny31gb66gddl9sivhdr65xjxazwc3q7ia")))

(define-public crate-rohanasan-0.5 (crate (name "rohanasan") (vers "0.5.22") (deps (list (crate-dep (name "tokio") (req "^1.36.0") (features (quote ("full"))) (default-features #t) (kind 0)) (crate-dep (name "urldecode") (req "^0.1.1") (default-features #t) (kind 0)))) (hash "04dql9bpm204dpmpvg7d5rf2xny6jp1d8p115bfj0296zz2d9w6d")))

(define-public crate-rohanasan-0.5 (crate (name "rohanasan") (vers "0.5.24") (deps (list (crate-dep (name "tokio") (req "^1.36.0") (features (quote ("full"))) (default-features #t) (kind 0)) (crate-dep (name "urldecode") (req "^0.1.1") (default-features #t) (kind 0)))) (hash "0jin8nsi0zdiqrim5bif8c1iaamsnvsd0bm2mlhn53mzbrvl3vc8")))

(define-public crate-rohanasan-0.5 (crate (name "rohanasan") (vers "0.5.25") (deps (list (crate-dep (name "tokio") (req "^1.36.0") (features (quote ("full"))) (default-features #t) (kind 0)) (crate-dep (name "urldecode") (req "^0.1.1") (default-features #t) (kind 0)))) (hash "17x7dv9wbyck3i8rddgs4q2gzxfb6xrfydh0cp2a61vda42cj0w5")))

(define-public crate-rohanasan-0.5 (crate (name "rohanasan") (vers "0.5.26") (deps (list (crate-dep (name "tokio") (req "^1.36.0") (features (quote ("full"))) (default-features #t) (kind 0)) (crate-dep (name "urldecode") (req "^0.1.1") (default-features #t) (kind 0)))) (hash "1llhgg52g4x7584srpgx6fqjszjk3ijvcrf34cqjkkhf0axgqcql")))

(define-public crate-rohanasan-0.5 (crate (name "rohanasan") (vers "0.5.27") (deps (list (crate-dep (name "tokio") (req "^1.36.0") (features (quote ("full"))) (default-features #t) (kind 0)) (crate-dep (name "urldecode") (req "^0.1.1") (default-features #t) (kind 0)))) (hash "0pxndlmv7cwgjs6q5zn4lva283irx1p8b9k86xky4c4qa79iaaxv")))

(define-public crate-rohanasan-0.5 (crate (name "rohanasan") (vers "0.5.28") (deps (list (crate-dep (name "tokio") (req "^1.36.0") (features (quote ("full"))) (default-features #t) (kind 0)) (crate-dep (name "urldecode") (req "^0.1.1") (default-features #t) (kind 0)))) (hash "11r06f8avdghqk99v65077igz3qs0iq528pkpyzcy151vi7ishfs")))

(define-public crate-rohanasan-0.5 (crate (name "rohanasan") (vers "0.5.29") (deps (list (crate-dep (name "quinn") (req "^0.10.2") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.36.0") (features (quote ("full"))) (default-features #t) (kind 0)) (crate-dep (name "urldecode") (req "^0.1.1") (default-features #t) (kind 0)))) (hash "1r585lcggc8gln11i0p3b7khnq019xai69jnlb84084xv57xhygz")))

(define-public crate-rohanasan-0.5 (crate (name "rohanasan") (vers "0.5.40") (deps (list (crate-dep (name "quinn") (req "^0.10.2") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.36.0") (features (quote ("full"))) (default-features #t) (kind 0)) (crate-dep (name "urldecode") (req "^0.1.1") (default-features #t) (kind 0)))) (hash "1w0w6v0v325kb93g3m4k9aildz5zr4vgw48fpkrvqccgmsnzrsjs")))

(define-public crate-rohanasan-0.5 (crate (name "rohanasan") (vers "0.5.41") (deps (list (crate-dep (name "tokio") (req "^1.36.0") (features (quote ("full"))) (default-features #t) (kind 0)) (crate-dep (name "urldecode") (req "^0.1.1") (default-features #t) (kind 0)))) (hash "06d5xkyq8f7g5s5y9r3gg9mb8w571sgv2c7w0gbv3v0adfc8mbj3")))

(define-public crate-rohanasan-0.5 (crate (name "rohanasan") (vers "0.5.42") (deps (list (crate-dep (name "tokio") (req "^1.36.0") (features (quote ("full"))) (default-features #t) (kind 0)) (crate-dep (name "urldecode") (req "^0.1.1") (default-features #t) (kind 0)))) (hash "1lwq5m19ny5b3zjb7lzvi8w0cl4hhggab4rbgpmpl8p8p3qgh8n7")))

(define-public crate-rohanasan-0.5 (crate (name "rohanasan") (vers "0.5.44") (deps (list (crate-dep (name "tokio") (req "^1.36.0") (features (quote ("full"))) (default-features #t) (kind 0)) (crate-dep (name "urldecode") (req "^0.1.1") (default-features #t) (kind 0)))) (hash "1jb3rjwiiz73wg5awqjzmrn1mkqhz3pd8ny8hmdjdgyq1bjcwv6k")))

(define-public crate-rohanasan-0.5 (crate (name "rohanasan") (vers "0.5.46") (deps (list (crate-dep (name "tokio") (req "^1.36.0") (features (quote ("full"))) (default-features #t) (kind 0)) (crate-dep (name "urldecode") (req "^0.1.1") (default-features #t) (kind 0)))) (hash "1yjg9i0hna6z0h8ryv7v11l1jwv9j1qx3ajzdcbip13pgz53hsad")))

(define-public crate-rohanasan-0.5 (crate (name "rohanasan") (vers "0.5.47") (deps (list (crate-dep (name "tokio") (req "^1.36.0") (features (quote ("full"))) (default-features #t) (kind 0)) (crate-dep (name "urldecode") (req "^0.1.1") (default-features #t) (kind 0)))) (hash "0z2adzgf4c6jp23iiqm982j3v3q3xqjzdl19mymhr40fcaxm87v5")))

(define-public crate-rohanasan-rs-0.1 (crate (name "rohanasan-rs") (vers "0.1.0") (deps (list (crate-dep (name "libc") (req "^0.2.153") (default-features #t) (kind 0)))) (hash "05ym5r430nznk4v8l37675y97f6py2wv4fb60i267yg66fjbfyvn") (yanked #t)))

(define-public crate-rohanasan-rs-0.1 (crate (name "rohanasan-rs") (vers "0.1.1") (deps (list (crate-dep (name "libc") (req "^0.2.153") (default-features #t) (kind 0)))) (hash "01hfpcd58cvn38lzsk92mx7d8v5665lnlqhv0n11nm1n37qll90s") (yanked #t)))

(define-public crate-rohanasan-rs-0.1 (crate (name "rohanasan-rs") (vers "0.1.2") (deps (list (crate-dep (name "libc") (req "^0.2.153") (default-features #t) (kind 0)))) (hash "0c1jxdj4vsi00dg3n5364m85x2gbag26rj9sd2vfnlbvazxi2v5h") (yanked #t)))

(define-public crate-rohanasan-rs-0.2 (crate (name "rohanasan-rs") (vers "0.2.0") (hash "0m3qimjbaq7yrwzz89kppylb28kq6nglmafr55lx1qxk0rdld64h") (yanked #t)))

(define-public crate-rohanasanpm-0.0.0 (crate (name "rohanasanpm") (vers "0.0.0") (deps (list (crate-dep (name "colored") (req "^2.1.0") (default-features #t) (kind 0)))) (hash "14whsbnlbd76vn3zygqppfr6p2knzfhaprfis8wpyyha3d5kwgq7")))

(define-public crate-rohanasanpm-0.1 (crate (name "rohanasanpm") (vers "0.1.1") (deps (list (crate-dep (name "colored") (req "^2.1.0") (default-features #t) (kind 0)))) (hash "1fq75fqmag5a305fdscq5bil60zwddm3i0g7jyhd0zh11by04w5g")))

(define-public crate-rohanasantml-0.0.0 (crate (name "rohanasantml") (vers "0.0.0") (hash "0ady5ak9k7q6y0pi8qnjwgic4d1n9xh3803bm500ws55a3iyhwyw")))

(define-public crate-rohanasantml-0.0.1 (crate (name "rohanasantml") (vers "0.0.1") (hash "01wbz3db1f74jiskfqvk6snlm7vj1navilfcii1aqm9cp1riaj8p")))

(define-public crate-rohanasantml-0.0.2 (crate (name "rohanasantml") (vers "0.0.2") (hash "0wbmyz8q1a1iknr1vl0w9l8g1wx5wfgvbs0270wah4cq4lybl823")))

