(define-module (crates-io ro t1) #:use-module (crates-io))

(define-public crate-rot13-0.1 (crate (name "rot13") (vers "0.1.0") (hash "17lm8b9ywl8n42hclrk9v71bqr4k74f370dhsgg3hra34pclj59a")))

(define-public crate-rot13-0.1 (crate (name "rot13") (vers "0.1.1") (hash "1nwikrs00qrqb4bzf9iv8bc64jn1fidlrrcmg6lhc63hf84j8kwl")))

(define-public crate-rot13-rs-0.1 (crate (name "rot13-rs") (vers "0.1.1") (hash "1fzpm46zfl3hwa7qnsva2b00hxrq3mmc43g32qayhs1razh2sssa")))

