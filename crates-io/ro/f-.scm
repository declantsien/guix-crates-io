(define-module (crates-io ro f-) #:use-module (crates-io))

(define-public crate-rof-rs-0.1 (crate (name "rof-rs") (vers "0.1.1") (deps (list (crate-dep (name "rof_rs_core") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "rof_rs_macros") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "0hijsakpgkw8w5wlgbx2g0y1yxqfnlgwn721f5s8b9myyydy6gln") (yanked #t)))

(define-public crate-rof-rs-0.1 (crate (name "rof-rs") (vers "0.1.3") (deps (list (crate-dep (name "rof_rs_core") (req "^0.1.1") (default-features #t) (kind 0)) (crate-dep (name "rof_rs_macros") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "1lwiw8z7xikz3aj3a49w8b95f51nji03a26519n1rppf59lrnxqy") (yanked #t)))

(define-public crate-rof-rs-0.1 (crate (name "rof-rs") (vers "0.1.4") (deps (list (crate-dep (name "rof_rs_core") (req "^0.1.1") (default-features #t) (kind 0)) (crate-dep (name "rof_rs_macros") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "1xi5ii1rdb41mwlg6f5qyipms8jdljz6pfs65y1j4gb41cbsq2rh") (yanked #t)))

(define-public crate-rof-rs-0.1 (crate (name "rof-rs") (vers "0.1.5") (deps (list (crate-dep (name "rof_rs_core") (req "^0.1.1") (default-features #t) (kind 0)) (crate-dep (name "rof_rs_macros") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "1cs0k1g2nic5irxfr4y60zz7adkkv589lh2ac2zihp556cd38gig") (yanked #t)))

(define-public crate-rof-rs-0.1 (crate (name "rof-rs") (vers "0.1.6") (deps (list (crate-dep (name "rof_rs_macros") (req "^0.1.2") (default-features #t) (kind 0)))) (hash "1mnyszs13nm11q06z2w27543yy3nw7bkx8zpxir1m3nifh8vc72r") (yanked #t)))

(define-public crate-rof-rs-0.1 (crate (name "rof-rs") (vers "0.1.7") (deps (list (crate-dep (name "rof_rs_macros") (req "^0.1.3") (default-features #t) (kind 0)))) (hash "1hwx23yd06gbcfh6s3xq03nczl5yyid4cq4vssn1mzd9p0g4g196")))

