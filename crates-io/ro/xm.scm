(define-module (crates-io ro xm) #:use-module (crates-io))

(define-public crate-roxmltree-0.1 (crate (name "roxmltree") (vers "0.1.0") (deps (list (crate-dep (name "bencher") (req "^0.1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "elementtree") (req "^0.5.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "pretty_assertions") (req "^0.5.1") (default-features #t) (kind 2)) (crate-dep (name "rustc-test") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "sxd-document") (req "^0.2.6") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "treexml") (req "^0.7.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "xml-rs") (req "^0.8.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "xmlparser") (req "^0.6.0") (default-features #t) (kind 0)) (crate-dep (name "xmltree") (req "^0.8") (optional #t) (default-features #t) (kind 0)))) (hash "1xzbb8hl7yk8bwg1vrax234fsym4jy19isdfz62bq1gxcxhrzdyy") (features (quote (("benchmark" "bencher" "xmltree" "sxd-document" "elementtree" "treexml" "xml-rs"))))))

(define-public crate-roxmltree-0.2 (crate (name "roxmltree") (vers "0.2.0") (deps (list (crate-dep (name "pretty_assertions") (req "^0.5.1") (default-features #t) (kind 2)) (crate-dep (name "rustc-test") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "xmlparser") (req "^0.6.1") (default-features #t) (kind 0)))) (hash "0q2arqzs9i15h5pxm7ls3xwqmgr2c6x2513zsr1i2xxdqxd3drwd")))

(define-public crate-roxmltree-0.3 (crate (name "roxmltree") (vers "0.3.0") (deps (list (crate-dep (name "pretty_assertions") (req "^0.5.1") (default-features #t) (kind 2)) (crate-dep (name "rustc-test") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "xmlparser") (req "^0.7") (default-features #t) (kind 0)))) (hash "0sbblz35kfpwxyc71i8a3h8rlixjqigr75p24i2zg29zcs1hq1xd")))

(define-public crate-roxmltree-0.4 (crate (name "roxmltree") (vers "0.4.0") (deps (list (crate-dep (name "pretty_assertions") (req "^0.5.1") (default-features #t) (kind 2)) (crate-dep (name "rustc-test") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "xmlparser") (req "^0.8") (default-features #t) (kind 0)))) (hash "0jv5wa9cjp4nf4fyg432hqcsyf63vfz5gr4as9n8q8bjcd2pg6l6")))

(define-public crate-roxmltree-0.4 (crate (name "roxmltree") (vers "0.4.1") (deps (list (crate-dep (name "pretty_assertions") (req "^0.5.1") (default-features #t) (kind 2)) (crate-dep (name "rustc-test") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "xmlparser") (req "^0.8") (default-features #t) (kind 0)))) (hash "1wxcag6r9cl5y51k4hjrzywcapp1wsp02984fri1pnn2s1kh8rh2")))

(define-public crate-roxmltree-0.5 (crate (name "roxmltree") (vers "0.5.0") (deps (list (crate-dep (name "pretty_assertions") (req "^0.5") (default-features #t) (kind 2)) (crate-dep (name "rustc-test") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "xmlparser") (req "^0.9") (default-features #t) (kind 0)))) (hash "0vim9fywnj8zcf67yc43hxk4zhx26vsm9kjsqk9r9r8y0flifvzd")))

(define-public crate-roxmltree-0.6 (crate (name "roxmltree") (vers "0.6.0") (deps (list (crate-dep (name "pretty_assertions") (req "^0.5") (default-features #t) (kind 2)) (crate-dep (name "rustc-test") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "xmlparser") (req "^0.9") (default-features #t) (kind 0)))) (hash "0ydm25hc0cyisfzqyanbazf1syk9gmqr4r87sv7zdcx8pw621c2k")))

(define-public crate-roxmltree-0.6 (crate (name "roxmltree") (vers "0.6.1") (deps (list (crate-dep (name "pretty_assertions") (req "^0.5") (default-features #t) (kind 2)) (crate-dep (name "rustc-test") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "xmlparser") (req "^0.9") (default-features #t) (kind 0)))) (hash "0747azn8njasc5lsvw87d6wj8zhfjx2y73lh12v3rg3lla08y39k")))

(define-public crate-roxmltree-0.7 (crate (name "roxmltree") (vers "0.7.0") (deps (list (crate-dep (name "pretty_assertions") (req "^0.5") (default-features #t) (kind 2)) (crate-dep (name "rustc-test") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "xmlparser") (req "^0.9") (default-features #t) (kind 0)))) (hash "0756w1df02ylhw1cygzg0f5zi81v12yr5vrpwspzg3pvx5y3cg0m")))

(define-public crate-roxmltree-0.7 (crate (name "roxmltree") (vers "0.7.1") (deps (list (crate-dep (name "pretty_assertions") (req "^0.5") (default-features #t) (kind 2)) (crate-dep (name "xmlparser") (req "^0.10") (default-features #t) (kind 0)))) (hash "0x71f30ssznlg4vxvbj7m50r5xy7hpq7ml3zh4pjcvlcaqz1k8xi")))

(define-public crate-roxmltree-0.7 (crate (name "roxmltree") (vers "0.7.2") (deps (list (crate-dep (name "pretty_assertions") (req "^0.5") (default-features #t) (kind 2)) (crate-dep (name "xmlparser") (req "^0.10") (default-features #t) (kind 0)))) (hash "0l8ciwi5h0f79yskwpnq5jind9sbyi2scj5zbvjwgl9zanjwpbz9")))

(define-public crate-roxmltree-0.7 (crate (name "roxmltree") (vers "0.7.3") (deps (list (crate-dep (name "pretty_assertions") (req "^0.5") (default-features #t) (kind 2)) (crate-dep (name "xmlparser") (req "^0.10") (default-features #t) (kind 0)))) (hash "0ay04flfbwz6a205qwpsl922g73nwzzv77bbqsh9ddn1axr40lh8")))

(define-public crate-roxmltree-0.8 (crate (name "roxmltree") (vers "0.8.0") (deps (list (crate-dep (name "pretty_assertions") (req "^0.5") (default-features #t) (kind 2)) (crate-dep (name "xmlparser") (req "^0.12") (default-features #t) (kind 0)))) (hash "0hr6f0h749rpgvb3z2cr67m3skqj0l6vvmsbffag8ri6rkd4r0i7")))

(define-public crate-roxmltree-0.9 (crate (name "roxmltree") (vers "0.9.0") (deps (list (crate-dep (name "pretty_assertions") (req "^0.5") (default-features #t) (kind 2)) (crate-dep (name "xmlparser") (req "^0.13") (default-features #t) (kind 0)))) (hash "1wz23l88gx0s2cxrv4x75xdrnd31v6vkpc2sr9mchw2vbqsfi3kh")))

(define-public crate-roxmltree-0.9 (crate (name "roxmltree") (vers "0.9.1") (deps (list (crate-dep (name "pretty_assertions") (req "^0.5") (default-features #t) (kind 2)) (crate-dep (name "xmlparser") (req "^0.13") (default-features #t) (kind 0)))) (hash "0mq99l2grdlwbchac4diwscpq003mgq5d17x10pf1lwj1fr9dmlr")))

(define-public crate-roxmltree-0.10 (crate (name "roxmltree") (vers "0.10.0") (deps (list (crate-dep (name "pretty_assertions") (req "^0.5") (default-features #t) (kind 2)) (crate-dep (name "xmlparser") (req "^0.13.1") (default-features #t) (kind 0)))) (hash "10m0f13zz115jz66489kr0x1cs6mbjxg0an2bi9biwwfvlpka55z")))

(define-public crate-roxmltree-0.10 (crate (name "roxmltree") (vers "0.10.1") (deps (list (crate-dep (name "pretty_assertions") (req "^0.5") (default-features #t) (kind 2)) (crate-dep (name "xmlparser") (req "^0.13.1") (default-features #t) (kind 0)))) (hash "0mz1mzsgqrmi5z5ixqhii3g54rsjps72rl4p5dh79xh5ylx78aa1")))

(define-public crate-roxmltree-0.11 (crate (name "roxmltree") (vers "0.11.0") (deps (list (crate-dep (name "pretty_assertions") (req "^0.5") (default-features #t) (kind 2)) (crate-dep (name "xmlparser") (req "^0.13.1") (default-features #t) (kind 0)))) (hash "17bq0paqvbx1liy1qgx4yx2j9pwhnr9992vwyy3rs1kp809iy06m")))

(define-public crate-roxmltree-0.11 (crate (name "roxmltree") (vers "0.11.1") (deps (list (crate-dep (name "pretty_assertions") (req "^0.5") (default-features #t) (kind 2)) (crate-dep (name "xmlparser") (req "^0.13.2") (default-features #t) (kind 0)))) (hash "035zz0s1y09qql3gi01gbjmlcfss9nqr2i91zmnsklkjiighfy02") (yanked #t)))

(define-public crate-roxmltree-0.12 (crate (name "roxmltree") (vers "0.12.0") (deps (list (crate-dep (name "pretty_assertions") (req "^0.5") (default-features #t) (kind 2)) (crate-dep (name "xmlparser") (req "^0.13.2") (default-features #t) (kind 0)))) (hash "1f1vhq4fvibsjx2sjc77phwjb7zvdxbwlsv72rcyii4cqb2i2g78")))

(define-public crate-roxmltree-0.13 (crate (name "roxmltree") (vers "0.13.0") (deps (list (crate-dep (name "pretty_assertions") (req "^0.5") (default-features #t) (kind 2)) (crate-dep (name "xmlparser") (req "^0.13.2") (default-features #t) (kind 0)))) (hash "1wji8hszj8jxjsdpi00ka26n0yax0l9ashn45ryzqsw4kz1wdpqp")))

(define-public crate-roxmltree-0.13 (crate (name "roxmltree") (vers "0.13.1") (deps (list (crate-dep (name "pretty_assertions") (req "^0.5") (default-features #t) (kind 2)) (crate-dep (name "xmlparser") (req "^0.13.3") (default-features #t) (kind 0)))) (hash "0wgnlvwf6j180j233gyx63z7xnm6cf01alw11q6khvb4xaqxgxyv")))

(define-public crate-roxmltree-0.14 (crate (name "roxmltree") (vers "0.14.0") (deps (list (crate-dep (name "pretty_assertions") (req "^0.5") (default-features #t) (kind 2)) (crate-dep (name "xmlparser") (req "^0.13.3") (default-features #t) (kind 0)))) (hash "010mahzyarkzb7kaawgga1a9y46nsp209yh2i4g4pq98bg8afn5z") (features (quote (("std"))))))

(define-public crate-roxmltree-0.14 (crate (name "roxmltree") (vers "0.14.1") (deps (list (crate-dep (name "pretty_assertions") (req "^0.5") (default-features #t) (kind 2)) (crate-dep (name "xmlparser") (req "^0.13.3") (default-features #t) (kind 0)))) (hash "0sv44pfl218p6r9anq5si6fhv0vz26vq20y42pi3f3j15sk086cj") (features (quote (("std") ("default" "std"))))))

(define-public crate-roxmltree-0.15 (crate (name "roxmltree") (vers "0.15.0") (deps (list (crate-dep (name "xmlparser") (req "^0.13.3") (default-features #t) (kind 0)))) (hash "03jcdal74zrznrvnjcmaa6ln3w7mr8p46j1r9das24a7mk1hha8i") (features (quote (("std") ("default" "std"))))))

(define-public crate-roxmltree-0.15 (crate (name "roxmltree") (vers "0.15.1") (deps (list (crate-dep (name "xmlparser") (req "^0.13.5") (default-features #t) (kind 0)))) (hash "12p4vyg6c906pclhpgq8h21x1acza3dl5wk1gqp156qj3a1yk7bb") (features (quote (("std") ("default" "std"))))))

(define-public crate-roxmltree-0.16 (crate (name "roxmltree") (vers "0.16.0") (deps (list (crate-dep (name "xmlparser") (req "^0.13.5") (default-features #t) (kind 0)))) (hash "13pxvdvbn17jhjjzgdbsii6w2bl30a2wcw7jqy4axc3hjyslfvgv") (features (quote (("std") ("positions") ("default" "std" "positions"))))))

(define-public crate-roxmltree-0.17 (crate (name "roxmltree") (vers "0.17.0") (deps (list (crate-dep (name "xmlparser") (req "^0.13.5") (default-features #t) (kind 0)))) (hash "10vk3i8yv2ph9z5rc4p003mzvldm1086vwpf79winj6i9ja9n726") (features (quote (("std") ("positions") ("default" "std" "positions"))))))

(define-public crate-roxmltree-0.18 (crate (name "roxmltree") (vers "0.18.0") (deps (list (crate-dep (name "xmlparser") (req "^0.13.5") (default-features #t) (kind 0)))) (hash "1n5ikvn00ciqkkr4hnch5ws1k3gfj8z50j3alv6wdf5nayj9bxfq") (features (quote (("std") ("positions") ("default" "std" "positions"))))))

(define-public crate-roxmltree-0.18 (crate (name "roxmltree") (vers "0.18.1") (deps (list (crate-dep (name "xmlparser") (req "^0.13.6") (kind 0)))) (hash "00mkd2xyrxm8ap39sxpkhzdzfn2m98q3zicf6wd2f6yfa7il08w6") (features (quote (("std" "xmlparser/std") ("positions") ("default" "std" "positions"))))))

(define-public crate-roxmltree-0.19 (crate (name "roxmltree") (vers "0.19.0") (hash "0zs0q8hg5nnh91s1ib6r0fky7xm8ay63ayfa5i1afxxpwgalzl9w") (features (quote (("std") ("positions") ("default" "std" "positions")))) (rust-version "1.60")))

(define-public crate-roxmltree-0.20 (crate (name "roxmltree") (vers "0.20.0") (hash "15vw91ps91wkmmgy62khf9zb63bdinvm80957dascbsw7dwvc83c") (features (quote (("std") ("positions") ("default" "std" "positions")))) (rust-version "1.60")))

