(define-module (crates-io ro rs) #:use-module (crates-io))

(define-public crate-rorschach-0.1 (crate (name "rorschach") (vers "0.1.0") (deps (list (crate-dep (name "ansi_term") (req "^0.7") (default-features #t) (kind 0)) (crate-dep (name "byteorder") (req "^0.5") (default-features #t) (kind 0)))) (hash "0w1d8010pw9yafxxwz7ig48bz0kaz3z7165h925c45d37i8mr1fl")))

(define-public crate-rorschach-0.1 (crate (name "rorschach") (vers "0.1.1") (deps (list (crate-dep (name "ansi_term") (req "^0.7") (default-features #t) (kind 0)) (crate-dep (name "byteorder") (req "^0.5") (default-features #t) (kind 0)))) (hash "0077bm4f0ffck6azb9ai4narblqc4is3316fllcx5g5n0ps7ykyj")))

(define-public crate-rorschach-0.1 (crate (name "rorschach") (vers "0.1.2") (deps (list (crate-dep (name "ansi_term") (req "^0.7") (default-features #t) (kind 0)) (crate-dep (name "byteorder") (req "^0.5") (default-features #t) (kind 0)))) (hash "10zr1iqh578vcb4k16bq5r53fzcqzxq8kycng0pw5ibz7684vwcm")))

(define-public crate-rorschach-0.1 (crate (name "rorschach") (vers "0.1.3") (deps (list (crate-dep (name "ansi_term") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "byteorder") (req "^0.5") (default-features #t) (kind 0)))) (hash "154j8vy3hmisdx3wfrp2fvs8r3mnvgjcr7byd12g02vk20hj9wg9")))

(define-public crate-rorshach-0.2 (crate (name "rorshach") (vers "0.2.0") (deps (list (crate-dep (name "clap") (req "~2.27.0") (features (quote ("yaml"))) (default-features #t) (kind 0)) (crate-dep (name "csv") (req "^1.1.3") (default-features #t) (kind 0)) (crate-dep (name "futures") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "hotwatch") (req "^0.4.3") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.11") (default-features #t) (kind 0)) (crate-dep (name "pub-sub") (req "^2.0.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "shellexpand") (req "^1.0.0") (default-features #t) (kind 0)) (crate-dep (name "simple_logger") (req "^1.6.0") (default-features #t) (kind 0)) (crate-dep (name "strum") (req "^0.18.0") (default-features #t) (kind 0)) (crate-dep (name "strum_macros") (req "^0.18.0") (default-features #t) (kind 0)))) (hash "0xs4cmzrs8aw8brf8p9hvyfdljx7i1b84z0cfdw1qr89n0abhb31")))

