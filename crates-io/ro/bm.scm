(define-module (crates-io ro bm) #:use-module (crates-io))

(define-public crate-robma_builder-0.0.1 (crate (name "robma_builder") (vers "0.0.1") (deps (list (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.7") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.31") (features (quote ("extra-traits"))) (default-features #t) (kind 0)) (crate-dep (name "trybuild") (req "^1.0") (features (quote ("diff"))) (default-features #t) (kind 2)))) (hash "1m84ns8lrk132yfmh4a999lp0iricpk5r46kq93dfk65mbr443vd")))

