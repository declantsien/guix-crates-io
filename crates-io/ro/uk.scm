(define-module (crates-io ro uk) #:use-module (crates-io))

(define-public crate-roukens-0.1 (crate (name "roukens") (vers "0.1.0") (deps (list (crate-dep (name "chrono") (req "^0.4.38") (default-features #t) (kind 0)) (crate-dep (name "hashbrown") (req "^0.14.3") (default-features #t) (kind 0)) (crate-dep (name "random-string") (req "^1.1.0") (default-features #t) (kind 0)))) (hash "181ciyfw4aixvxww6445x8kdly1f3y8fv7b7ngkq176rryhdkgj4")))

(define-public crate-roukens-0.1 (crate (name "roukens") (vers "0.1.1") (deps (list (crate-dep (name "chrono") (req "^0.4.38") (default-features #t) (kind 0)) (crate-dep (name "hashbrown") (req "^0.14.3") (default-features #t) (kind 0)) (crate-dep (name "random-string") (req "^1.1.0") (default-features #t) (kind 0)))) (hash "0k25my9p23c1v62k27gzj16xaa51bbrd2kb1aalpvrzb06ibygg4")))

(define-public crate-roukens-0.1 (crate (name "roukens") (vers "0.1.2") (deps (list (crate-dep (name "chrono") (req "^0.4.38") (default-features #t) (kind 0)) (crate-dep (name "hashbrown") (req "^0.14.3") (default-features #t) (kind 0)) (crate-dep (name "random-string") (req "^1.1.0") (default-features #t) (kind 0)))) (hash "1qag20glpzhwxc5m86p951755mx2gbzqg4djxabjrlz3k44n8hwj")))

(define-public crate-roukens-0.1 (crate (name "roukens") (vers "0.1.3") (deps (list (crate-dep (name "chrono") (req "^0.4.38") (default-features #t) (kind 0)) (crate-dep (name "hashbrown") (req "^0.14.3") (default-features #t) (kind 0)) (crate-dep (name "random-string") (req "^1.1.0") (default-features #t) (kind 0)))) (hash "0838wi23wp5zmxq39lpx70y0v9v3bci7xd9a8b5vj7xksapa91gh")))

