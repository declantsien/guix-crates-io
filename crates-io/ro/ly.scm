(define-module (crates-io ro ly) #:use-module (crates-io))

(define-public crate-rolyng_basic_make_parser-0.1 (crate (name "rolyng_basic_make_parser") (vers "0.1.2") (deps (list (crate-dep (name "anyhow") (req "^1.0.75") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^4.4.7") (features (quote ("derive" "cargo"))) (default-features #t) (kind 0)) (crate-dep (name "pest") (req "^2.7.5") (default-features #t) (kind 0)) (crate-dep (name "pest_derive") (req "^2.7.5") (default-features #t) (kind 0)))) (hash "15kdym7qg0wjcmbjvcajls1yhjj7qkkh18f9zn3pvf9k1445bxyn")))

(define-public crate-rolyng_parser_c-0.1 (crate (name "rolyng_parser_c") (vers "0.1.0") (deps (list (crate-dep (name "peg") (req "^0.8.2") (default-features #t) (kind 0)))) (hash "0hk243m64chg11sqv17zpndzn0hvqsng391pcclln0gyb0cg8jw9") (yanked #t)))

(define-public crate-rolyng_parser_c-0.1 (crate (name "rolyng_parser_c") (vers "0.1.1") (deps (list (crate-dep (name "peg") (req "^0.8.2") (default-features #t) (kind 0)))) (hash "1xn73jbvg1j4dzg3nnmq971rfaahhjcqdbjsidlrznmrp1jbabih") (yanked #t)))

