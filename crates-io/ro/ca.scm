(define-module (crates-io ro ca) #:use-module (crates-io))

(define-public crate-rocat-0.1 (crate (name "rocat") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^4.4.4") (features (quote ("derive" "cargo"))) (default-features #t) (kind 0)))) (hash "0wgnqrksvx0b5x1mxarlzvr02ga1dv50jkzrkxr1bv0g716z26jv")))

