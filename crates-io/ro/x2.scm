(define-module (crates-io ro x2) #:use-module (crates-io))

(define-public crate-rox2d-0.1 (crate (name "rox2d") (vers "0.1.0") (deps (list (crate-dep (name "glam") (req "^0.22.0") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)) (crate-dep (name "bevy") (req "^0.9.1") (default-features #t) (kind 2)))) (hash "06v1aqjmv5s5qq7p62cknmaaghdyl7sa7as4i96n4d81wph61vm8")))

