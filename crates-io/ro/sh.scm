(define-module (crates-io ro sh) #:use-module (crates-io))

(define-public crate-rosh-0.1 (crate (name "rosh") (vers "0.1.0") (hash "0pdqh58fphdj3bi02ippsh657jz7cys1iq9z0n6hg2nkvfvnrz7z")))

(define-public crate-roshennair-grrs-0.1 (crate (name "roshennair-grrs") (vers "0.1.0") (deps (list (crate-dep (name "structopt") (req "^0.3.13") (default-features #t) (kind 0)))) (hash "1864lvlvnwycszbpk1fqdkw9rslkxqixya8j7ziz4c9q8v3a5x6n")))

