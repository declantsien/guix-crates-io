(define-module (crates-io ro tk) #:use-module (crates-io))

(define-public crate-rotkeappchen-0.1 (crate (name "rotkeappchen") (vers "0.1.0") (deps (list (crate-dep (name "blake3") (req "^1.5.0") (default-features #t) (kind 0)) (crate-dep (name "criterion") (req "^0.4") (features (quote ("html_reports"))) (default-features #t) (kind 2)))) (hash "1ijblhdwqw91djijz2hn6mzmpbccw642wa2bxxcm8ddw630zyi8a")))

