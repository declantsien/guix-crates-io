(define-module (crates-io ro q-) #:use-module (crates-io))

(define-public crate-roq-dec-0.1 (crate (name "roq-dec") (vers "0.1.0") (deps (list (crate-dep (name "byteorder") (req "^1.4.3") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)))) (hash "0lbc6cgkf11cnqr7z8hi1gwblf1fnf7ffisd4d0xnzwjj1iq190k")))

