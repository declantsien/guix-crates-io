(define-module (crates-io ro te) #:use-module (crates-io))

(define-public crate-rotenv-0.15 (crate (name "rotenv") (vers "0.15.0") (deps (list (crate-dep (name "clap") (req "^3") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "tempfile") (req "^3.0.0") (default-features #t) (kind 2)))) (hash "01dsim2llcaa25hf7y8kg35x4w0j65ql9fl6wygg48w1cf0g8m33") (features (quote (("cli" "clap"))))))

(define-public crate-rotenv_codegen-0.15 (crate (name "rotenv_codegen") (vers "0.15.0") (deps (list (crate-dep (name "litrs") (req "^0.2.3") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "rotenv") (req "^0.15") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "0y0v8dyq3kfm71px5gzn81j592nd1h55wrqbpkc9h66n2fkc8k9y")))

