(define-module (crates-io ro ta) #:use-module (crates-io))

(define-public crate-rotala-0.1 (crate (name "rotala") (vers "0.1.0") (deps (list (crate-dep (name "actix-web") (req "^4") (default-features #t) (kind 0)) (crate-dep (name "criterion") (req "^0.5.1") (features (quote ("async_tokio"))) (default-features #t) (kind 2)) (crate-dep (name "csv") (req "^1.1.6") (default-features #t) (kind 0)) (crate-dep (name "env_logger") (req "^0.10.0") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.4") (default-features #t) (kind 0)) (crate-dep (name "rand_distr") (req "^0.4.1") (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.11.11") (features (quote ("blocking" "json"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.193") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.108") (default-features #t) (kind 0)) (crate-dep (name "time") (req "^0.3.17") (features (quote ("macros" "parsing"))) (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.35.1") (features (quote ("full"))) (default-features #t) (kind 0)) (crate-dep (name "zip") (req "^0.6.2") (default-features #t) (kind 0)))) (hash "0k177j21afjwk67d7bqy8sj7rc1286fh70rlvrqyydddyryjv8cp")))

(define-public crate-rotala-0.1 (crate (name "rotala") (vers "0.1.1") (deps (list (crate-dep (name "actix-web") (req "^4") (default-features #t) (kind 0)) (crate-dep (name "criterion") (req "^0.5.1") (features (quote ("async_tokio"))) (default-features #t) (kind 2)) (crate-dep (name "csv") (req "^1.1.6") (default-features #t) (kind 0)) (crate-dep (name "env_logger") (req "^0.10.0") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.4") (default-features #t) (kind 0)) (crate-dep (name "rand_distr") (req "^0.4.1") (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.11.11") (features (quote ("blocking" "json"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.193") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.108") (default-features #t) (kind 0)) (crate-dep (name "time") (req "^0.3.17") (features (quote ("macros" "parsing"))) (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.35.1") (features (quote ("full"))) (default-features #t) (kind 0)) (crate-dep (name "zip") (req "^0.6.2") (default-features #t) (kind 0)))) (hash "0ygczwi93wp7jwmi7axvcpc3jrmjpmw0wb93ghblya24gvchl4v3")))

(define-public crate-rotary-0.1 (crate (name "rotary") (vers "0.1.0") (deps (list (crate-dep (name "rand") (req "^0.8.3") (default-features #t) (kind 2)))) (hash "1g0ik0yacdqziw2a5k17q5hjsip21yzy29vjy60qq714xzm5kc0j")))

(define-public crate-rotary-0.2 (crate (name "rotary") (vers "0.2.0") (deps (list (crate-dep (name "rand") (req "^0.8.3") (default-features #t) (kind 2)))) (hash "0cnymvrddg9a77q3b13pzh89v883bjf7654v4mp8jr89rss7z217")))

(define-public crate-rotary-0.3 (crate (name "rotary") (vers "0.3.0") (deps (list (crate-dep (name "rand") (req "^0.8.3") (default-features #t) (kind 2)))) (hash "0ylykprrwms8jv5cb3yp115j543slsn8n1bpjxnr8y16hw8y2mf9")))

(define-public crate-rotary-0.4 (crate (name "rotary") (vers "0.4.0") (deps (list (crate-dep (name "rand") (req "^0.8.3") (default-features #t) (kind 2)))) (hash "1dv9zldx8h1xbbgpl3dg3zah8n2kygg92yncv73fpwr3bgy1jbzm")))

(define-public crate-rotary-0.4 (crate (name "rotary") (vers "0.4.1") (deps (list (crate-dep (name "rand") (req "^0.8.3") (default-features #t) (kind 2)))) (hash "09l6zyrw99wvaxvjx1hfl795xz15w4rld6hgh24gfixclyc69can")))

(define-public crate-rotary-0.5 (crate (name "rotary") (vers "0.5.0") (deps (list (crate-dep (name "rand") (req "^0.8.3") (default-features #t) (kind 2)))) (hash "0npbj061sm3n1b4cl2qh76jxw0d7l09p96l9rkx0a5yr6cyxbq49")))

(define-public crate-rotary-0.7 (crate (name "rotary") (vers "0.7.0") (deps (list (crate-dep (name "rand") (req "^0.8.3") (default-features #t) (kind 2)))) (hash "0cq707y7gqfgjgf455dljpyj0xhvzw8mb7m1sa914vwrh8nvzdhc")))

(define-public crate-rotary-0.8 (crate (name "rotary") (vers "0.8.0") (deps (list (crate-dep (name "rand") (req "^0.8.3") (default-features #t) (kind 2)))) (hash "0gk37db05an5nndpasni32kkw5g77df8ygl22d833kgqris34h46")))

(define-public crate-rotary-0.9 (crate (name "rotary") (vers "0.9.0") (deps (list (crate-dep (name "rand") (req "^0.8.3") (default-features #t) (kind 2)))) (hash "1351vkl5kmf7kra9j5yrjlighlw146ssa88r2n7xppyd1bgq42cq")))

(define-public crate-rotary-0.9 (crate (name "rotary") (vers "0.9.1") (deps (list (crate-dep (name "rand") (req "^0.8.3") (default-features #t) (kind 2)))) (hash "0ppxsxjyhd592h9sx656frzbapyhndm984sn9ylspqd445x57d23")))

(define-public crate-rotary-0.10 (crate (name "rotary") (vers "0.10.0") (deps (list (crate-dep (name "rand") (req "^0.8.3") (default-features #t) (kind 2)))) (hash "1p6pq2p2s3kp9gx6zwqw0702nzyw8dmhbvizj0mkdvfq47xnq6kw")))

(define-public crate-rotary-0.11 (crate (name "rotary") (vers "0.11.0") (deps (list (crate-dep (name "rand") (req "^0.8.3") (default-features #t) (kind 2)))) (hash "1vqvgyy6fv9f9p4hxpjz160w9mqk4fa73m6nd2v7f5gfa32qv7mk")))

(define-public crate-rotary-0.12 (crate (name "rotary") (vers "0.12.0") (deps (list (crate-dep (name "rand") (req "^0.8.3") (default-features #t) (kind 2)))) (hash "0cmk1jqb43wsilkqzlf5cl0br950l4yg9a3apak714kqccvmvi4v")))

(define-public crate-rotary-0.13 (crate (name "rotary") (vers "0.13.0") (deps (list (crate-dep (name "rand") (req "^0.8.3") (default-features #t) (kind 2)))) (hash "1ik0yr7613vm53jppna41a7qdhisblpzswwx6nly6v5yzp2rx31p")))

(define-public crate-rotary-0.15 (crate (name "rotary") (vers "0.15.0") (deps (list (crate-dep (name "rand") (req "^0.8.3") (default-features #t) (kind 2)))) (hash "09i0fz04pp5n6siw8n4cbffhl4zfnnjm5r3dcwsb8cfbqk1cssgl")))

(define-public crate-rotary-0.16 (crate (name "rotary") (vers "0.16.0") (deps (list (crate-dep (name "rand") (req "^0.8.3") (default-features #t) (kind 2)))) (hash "1sl4hxr1s28cqf0wk1a65ra0v10m47n1zins5lff2dv53rnxic30")))

(define-public crate-rotary-0.17 (crate (name "rotary") (vers "0.17.0") (deps (list (crate-dep (name "rand") (req "^0.8.3") (default-features #t) (kind 2)))) (hash "1naafn9cqhc2b8hvg53d6770nkng3hf382g7vwazf74c215jxh7c")))

(define-public crate-rotary-0.18 (crate (name "rotary") (vers "0.18.0") (deps (list (crate-dep (name "rand") (req "^0.8.3") (default-features #t) (kind 2)))) (hash "1bv61yzkwdqcmv5195rc0000zj78jr4ls8qfzzs4yn3sf4ws5sjy")))

(define-public crate-rotary-0.19 (crate (name "rotary") (vers "0.19.0") (deps (list (crate-dep (name "rand") (req "^0.8.3") (default-features #t) (kind 2)))) (hash "18017zp1jmdz112jzjiaj9fddi2w8zrb2yg6rqv0zzwclqzgribx")))

(define-public crate-rotary-0.20 (crate (name "rotary") (vers "0.20.0") (deps (list (crate-dep (name "rand") (req "^0.8.3") (default-features #t) (kind 2)))) (hash "0yf0sys3zywwbv7chagb1mp8cph7ns6ywl8km6zrz25id375s8ma")))

(define-public crate-rotary-0.21 (crate (name "rotary") (vers "0.21.0") (deps (list (crate-dep (name "rand") (req "^0.8.3") (default-features #t) (kind 2)))) (hash "0kd1qcy8vhhb1c9clpnrqrkv1ry0mk5n8057wkgxpi0ssf93li44")))

(define-public crate-rotary-0.22 (crate (name "rotary") (vers "0.22.0") (deps (list (crate-dep (name "rand") (req "^0.8.3") (default-features #t) (kind 2)))) (hash "15ir2h7415mf7g417z71cjr8jp33l9hlyh5cxsbmljqzahc2xq1f")))

(define-public crate-rotary-0.23 (crate (name "rotary") (vers "0.23.0") (deps (list (crate-dep (name "rand") (req "^0.8.3") (default-features #t) (kind 2)))) (hash "027436icbzdlb96r6amwi00bqbas4x42x5d2qhf0f11iwqzp27zk")))

(define-public crate-rotary-0.24 (crate (name "rotary") (vers "0.24.0") (deps (list (crate-dep (name "rand") (req "^0.8.3") (default-features #t) (kind 2)))) (hash "0w7b6rhlwl3na74mkg98n3i65s50vs4qq6mwn88mam8zxkb4nki6")))

(define-public crate-rotary-0.25 (crate (name "rotary") (vers "0.25.0") (deps (list (crate-dep (name "rand") (req "^0.8.3") (default-features #t) (kind 2)))) (hash "13mcpph21grcixzp3bglkjbi7ffzwrqd6lnp6vg470r1byk4yipa")))

(define-public crate-rotary-0.26 (crate (name "rotary") (vers "0.26.0") (deps (list (crate-dep (name "rand") (req "^0.8.3") (default-features #t) (kind 2)))) (hash "1sg7nn3fc270dsv10y23d27ixnvjsipargb1sk85ap2snvfkps5y")))

(define-public crate-rotary-0.27 (crate (name "rotary") (vers "0.27.0") (deps (list (crate-dep (name "bittle") (req "^0.1.0") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.8.3") (default-features #t) (kind 2)) (crate-dep (name "rotary-core") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "0hvj392lbkhljk5wmifjkzijc4gbsndp5lqmr5j5jarw0cbnd77b") (yanked #t)))

(define-public crate-rotary-0.27 (crate (name "rotary") (vers "0.27.1") (deps (list (crate-dep (name "bittle") (req "^0.1.0") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.8.3") (default-features #t) (kind 2)) (crate-dep (name "rotary-core") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "0i76h8hymzqn0c5jpx6gnzsanjadq0cvvpfr2b919wxydqzcjnrr") (yanked #t)))

(define-public crate-rotary-0.27 (crate (name "rotary") (vers "0.27.2") (deps (list (crate-dep (name "bittle") (req "^0.1.0") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.8.3") (default-features #t) (kind 2)) (crate-dep (name "rotary-core") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "1qrp7ky6lhyl3cgzjw9llg6pnjxyvqqxs06xwsgl7p9d4psiidmr") (yanked #t)))

(define-public crate-rotary-0.28 (crate (name "rotary") (vers "0.28.0") (deps (list (crate-dep (name "bittle") (req "^0.1.0") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.8.3") (default-features #t) (kind 2)) (crate-dep (name "rotary-core") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "0k5v65s7mc75ry5v1z02snq143144x6s4z8bz4z4w20052045arh")))

(define-public crate-rotary-0.28 (crate (name "rotary") (vers "0.28.1") (deps (list (crate-dep (name "bittle") (req "^0.1.0") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.8.3") (default-features #t) (kind 2)) (crate-dep (name "rotary-core") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "1d3203v6gfrn6v9581xfbdch9lx9sg9cn0m9ji4yrzs9109i0lcm")))

(define-public crate-rotary-0.29 (crate (name "rotary") (vers "0.29.0-alpha.1") (deps (list (crate-dep (name "bittle") (req "^0.1.0") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.8.3") (default-features #t) (kind 2)) (crate-dep (name "rotary-core") (req "^0.2.0-alpha.1") (default-features #t) (kind 0)))) (hash "13102vxrfd3hyqj05337jywrdi0qn7vsphzwmma0andidgc7i27l")))

(define-public crate-rotary-0.29 (crate (name "rotary") (vers "0.29.0-alpha.2") (deps (list (crate-dep (name "bittle") (req "^0.1.0") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.8.3") (default-features #t) (kind 2)) (crate-dep (name "rotary-core") (req "^0.2.0-alpha.2") (default-features #t) (kind 0)))) (hash "1i4kwgcicrxnbhbdkd80m13zx88yagrlris6y8lb19g7mny5skn0")))

(define-public crate-rotary-add-0.1 (crate (name "rotary-add") (vers "0.1.0") (hash "0hw3rap0i24b4153i804sl2bvxys46rp5b1pwzg39k77720q1d2h") (yanked #t)))

(define-public crate-rotary-add-0.1 (crate (name "rotary-add") (vers "0.1.1") (hash "066i4gd37zf33k4wdv81rv1j7kxm3x94k1h5shz1czb8npcqny9a") (yanked #t)))

(define-public crate-rotary-add-0.1 (crate (name "rotary-add") (vers "0.1.2") (hash "0p7ns8m01sqqy9n0r3r8h4f111n5yskgmx1mv87hzls61f4lwfpy") (yanked #t)))

(define-public crate-rotary-add-0.1 (crate (name "rotary-add") (vers "0.1.3") (hash "0jqdv7938a9pix7jrs7ma2n5kwdyb6jx1449587y6954g8kbfh31") (yanked #t)))

(define-public crate-rotary-add-0.1 (crate (name "rotary-add") (vers "0.1.4") (hash "0b50xirb6gxh4hlalrma2snd6mlfgsxd3l30ps5dxl6mp3jk9cn9")))

(define-public crate-rotary-core-0.1 (crate (name "rotary-core") (vers "0.1.0") (deps (list (crate-dep (name "rotary") (req "^0.26.0") (default-features #t) (kind 2)))) (hash "1bbqpw10x5xhyl7yy16149byny9i7sm62bvdmqs9g9dsxhq64fjd") (yanked #t)))

(define-public crate-rotary-core-0.1 (crate (name "rotary-core") (vers "0.1.1") (deps (list (crate-dep (name "rotary") (req "^0.27.0") (default-features #t) (kind 2)))) (hash "0ghsrgisvg501mv54lk2d6fi1gjw7zc33wb5g6bhlzp1psk21rw5") (yanked #t)))

(define-public crate-rotary-core-0.1 (crate (name "rotary-core") (vers "0.1.2") (deps (list (crate-dep (name "rotary") (req "^0.27.0") (default-features #t) (kind 2)))) (hash "1s3snp38laaqnz2i5knsmjghlf9i6s2yrijq8d43gpc1b7qnig5h") (yanked #t)))

(define-public crate-rotary-core-0.1 (crate (name "rotary-core") (vers "0.1.3") (deps (list (crate-dep (name "rotary") (req "^0.28.0") (default-features #t) (kind 2)))) (hash "0hps8wz1cnkjs3m0y1zhlx84dkszsdc0hvagi6n2hlqr7fpxrgy1") (yanked #t)))

(define-public crate-rotary-core-0.1 (crate (name "rotary-core") (vers "0.1.4") (deps (list (crate-dep (name "rotary") (req "^0.28.0") (default-features #t) (kind 2)))) (hash "1y6vsqq2yg23mmd5b4rhp75lvzflv2x0zz156dcyb1zcpyymnd58")))

(define-public crate-rotary-core-0.2 (crate (name "rotary-core") (vers "0.2.0-alpha.1") (deps (list (crate-dep (name "rotary") (req "^0.29.0-alpha.1") (default-features #t) (kind 2)))) (hash "11xaxqywn6kng2v0jprk3igr5yqff43c1pf9y3wi0qf2rwzy36k4")))

(define-public crate-rotary-core-0.2 (crate (name "rotary-core") (vers "0.2.0-alpha.2") (deps (list (crate-dep (name "rotary") (req "^0.29.0-alpha.2") (default-features #t) (kind 2)))) (hash "0vhnf91v9z9jgzqkn47c2755dvrqmqzqxwgp3jfj5ki8dnv8y63y")))

(define-public crate-rotary-encoder-0.1 (crate (name "rotary-encoder") (vers "0.1.0") (deps (list (crate-dep (name "futures") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "mio") (req "^0.6") (default-features #t) (kind 2)) (crate-dep (name "sysfs_gpio") (req "^0.5") (features (quote ("tokio"))) (default-features #t) (kind 2)) (crate-dep (name "tokio-core") (req "^0.1") (default-features #t) (kind 2)))) (hash "08lfnyzpfysjgzpi7wjh0fvz724nc45grj3b0958jj1c66sy9jq4")))

(define-public crate-rotary-encoder-embedded-0.0.1 (crate (name "rotary-encoder-embedded") (vers "0.0.1") (deps (list (crate-dep (name "chrono") (req "^0.4.19") (kind 0)) (crate-dep (name "embedded-hal") (req "^0.2.4") (features (quote ("unproven"))) (default-features #t) (kind 0)))) (hash "1q9240xglwh8q70rg31ycnwds3sv94r5yjx7b3k7p1kx2qpd7rjr")))

(define-public crate-rotary-encoder-embedded-0.0.2 (crate (name "rotary-encoder-embedded") (vers "0.0.2") (deps (list (crate-dep (name "chrono") (req "^0.4.19") (kind 0)) (crate-dep (name "embedded-hal") (req "^0.2.4") (features (quote ("unproven"))) (default-features #t) (kind 0)))) (hash "1g7xhfvkkc2drhriixfbvbxm0552cipqvaj31ifb0ssq17qpak8j")))

(define-public crate-rotary-encoder-embedded-0.0.3 (crate (name "rotary-encoder-embedded") (vers "0.0.3") (deps (list (crate-dep (name "chrono") (req "^0.4.19") (kind 0)) (crate-dep (name "embedded-hal") (req "^0.2.4") (features (quote ("unproven"))) (default-features #t) (kind 0)))) (hash "0ydhck1pgbbhix1ql0gxr64g94ij9iq4gf4kk6zcm0865sf126y1")))

(define-public crate-rotary-encoder-embedded-0.0.4 (crate (name "rotary-encoder-embedded") (vers "0.0.4") (deps (list (crate-dep (name "chrono") (req "^0.4.19") (kind 0)) (crate-dep (name "embedded-hal") (req "^0.2.4") (features (quote ("unproven"))) (default-features #t) (kind 0)))) (hash "15d3nfvhaiayclma10890c36751x6ijs420zv7j5w6mkc7a6h4bj")))

(define-public crate-rotary-encoder-embedded-0.1 (crate (name "rotary-encoder-embedded") (vers "0.1.0") (deps (list (crate-dep (name "chrono") (req "^0.4.19") (kind 0)) (crate-dep (name "embedded-hal") (req "^0.2.7") (features (quote ("unproven"))) (default-features #t) (kind 0)))) (hash "1ydrzl7xl9lzhbqpw5k44ndbk0r2sm1dbq8m2xwlpg17iy6g0z8i") (features (quote (("angular-velocity"))))))

(define-public crate-rotary-encoder-embedded-0.1 (crate (name "rotary-encoder-embedded") (vers "0.1.1") (deps (list (crate-dep (name "embedded-hal") (req "^0.2.7") (features (quote ("unproven"))) (default-features #t) (kind 0)))) (hash "0qp9fw1arz94k7vz2qch4xxz5f8i605qaan73q5wvn007kswcb6x") (features (quote (("angular-velocity"))))))

(define-public crate-rotary-encoder-embedded-0.2 (crate (name "rotary-encoder-embedded") (vers "0.2.0") (deps (list (crate-dep (name "embedded-hal") (req "^0.2.7") (features (quote ("unproven"))) (default-features #t) (kind 0)))) (hash "0pfnlyyi681ynn332zy24b65wxiiax48pxa6hcyj0g46gwzrzj01") (features (quote (("standard") ("default" "standard") ("angular-velocity"))))))

(define-public crate-rotary-encoder-embedded-0.3 (crate (name "rotary-encoder-embedded") (vers "0.3.0") (deps (list (crate-dep (name "embedded-hal") (req "^0.2.7") (features (quote ("unproven"))) (default-features #t) (kind 0)) (crate-dep (name "embedded-hal-mock") (req "^0.10.0") (features (quote ("eh0"))) (default-features #t) (kind 2)))) (hash "1brqfv6bnshzpg8j9f4nb5gx6ax2n5hg375dricbsx2x79jhqc6s")))

(define-public crate-rotary-encoder-hal-0.1 (crate (name "rotary-encoder-hal") (vers "0.1.0") (deps (list (crate-dep (name "either") (req "^1.5.3") (kind 0)) (crate-dep (name "embedded-hal") (req "^0.2.3") (features (quote ("unproven"))) (default-features #t) (kind 0)) (crate-dep (name "version-sync") (req "^0.8") (default-features #t) (kind 2)))) (hash "15ldxnk6m8qhb1amxs0xk22jvw84sy8x9d2hvkpqvp9hkd43msbl") (yanked #t)))

(define-public crate-rotary-encoder-hal-0.2 (crate (name "rotary-encoder-hal") (vers "0.2.0") (deps (list (crate-dep (name "either") (req "^1.5.3") (kind 0)) (crate-dep (name "embedded-hal") (req "^0.2.3") (features (quote ("unproven"))) (default-features #t) (kind 0)) (crate-dep (name "version-sync") (req "^0.8") (default-features #t) (kind 2)))) (hash "1875idkm17bpcaad9pgm0ygr09pnphwnf6w0hss8g4xwqsnhx1ix") (yanked #t)))

(define-public crate-rotary-encoder-hal-0.2 (crate (name "rotary-encoder-hal") (vers "0.2.1") (deps (list (crate-dep (name "cortex-m") (req "^0.6.1") (default-features #t) (kind 2)) (crate-dep (name "cortex-m-rt") (req "^0.6.10") (default-features #t) (kind 2)) (crate-dep (name "either") (req "^1.5.3") (kind 0)) (crate-dep (name "embedded-hal") (req "^0.2.3") (features (quote ("unproven"))) (default-features #t) (kind 0)) (crate-dep (name "panic-semihosting") (req "^0.5.3") (default-features #t) (kind 2)) (crate-dep (name "stm32f3xx-hal") (req "^0.3.0") (features (quote ("rt" "stm32f303"))) (default-features #t) (kind 2)) (crate-dep (name "version-sync") (req "^0.8") (default-features #t) (kind 2)))) (hash "0xzgsr3xklbvfqhrrawrhs7abgjq2pb67vancff4bwzy5b52gyj8")))

(define-public crate-rotary-encoder-hal-0.3 (crate (name "rotary-encoder-hal") (vers "0.3.0") (deps (list (crate-dep (name "cortex-m") (req "^0.6") (default-features #t) (kind 2)) (crate-dep (name "cortex-m-rt") (req "^0.6") (default-features #t) (kind 2)) (crate-dep (name "cortex-m-semihosting") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "defmt") (req "^0.1.2") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "either") (req "^1.6") (kind 0)) (crate-dep (name "embedded-hal") (req "^0.2") (features (quote ("unproven"))) (default-features #t) (kind 0)) (crate-dep (name "embedded-hal-alpha") (req "=1.0.0-alpha.4") (optional #t) (default-features #t) (kind 0) (package "embedded-hal")) (crate-dep (name "panic-semihosting") (req "^0.5") (default-features #t) (kind 2)) (crate-dep (name "stm32f3xx-hal") (req "^0.5.0") (features (quote ("rt" "stm32f303"))) (default-features #t) (kind 2)) (crate-dep (name "version-sync") (req "^0.9") (default-features #t) (kind 2)))) (hash "0qyk6zd4k8454brqn294fgaynxj6qzh3jryk47bc1j08bjsh1sih") (features (quote (("table-decoder") ("default"))))))

(define-public crate-rotary-encoder-hal-0.5 (crate (name "rotary-encoder-hal") (vers "0.5.0") (deps (list (crate-dep (name "cortex-m") (req "^0.6") (default-features #t) (kind 2)) (crate-dep (name "cortex-m-rt") (req "^0.6") (default-features #t) (kind 2)) (crate-dep (name "cortex-m-semihosting") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "defmt") (req "^0.1.2") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "either") (req "^1.6") (kind 0)) (crate-dep (name "embedded-hal") (req "^0.2") (features (quote ("unproven"))) (default-features #t) (kind 0)) (crate-dep (name "embedded-hal-alpha") (req "=1.0.0-alpha.5") (optional #t) (default-features #t) (kind 0) (package "embedded-hal")) (crate-dep (name "panic-semihosting") (req "^0.5") (default-features #t) (kind 2)) (crate-dep (name "stm32f3xx-hal") (req "^0.5.0") (features (quote ("rt" "stm32f303"))) (default-features #t) (kind 2)) (crate-dep (name "version-sync") (req "^0.9") (default-features #t) (kind 2)))) (hash "11slnwlxalc2jjn6rnjk1vhwpka3lcaki4gmv54d8bl4svy10pya") (features (quote (("table-decoder") ("default"))))))

(define-public crate-rotate-0.3 (crate (name "rotate") (vers "0.3.0") (deps (list (crate-dep (name "approx") (req "^0.5.1") (default-features #t) (kind 0)) (crate-dep (name "nalgebra") (req "^0.32.3") (default-features #t) (kind 0)) (crate-dep (name "qc_file_parsers") (req "^0.2.2") (default-features #t) (kind 0)))) (hash "149s72gxyl7c5917kfhvnpbqadds5yrh6adk41fg9scas5y19h0y")))

(define-public crate-rotate-0.4 (crate (name "rotate") (vers "0.4.0") (deps (list (crate-dep (name "approx") (req "^0.5.1") (default-features #t) (kind 0)) (crate-dep (name "nalgebra") (req "^0.32.3") (default-features #t) (kind 0)) (crate-dep (name "qc_file_parsers") (req "^0.2.2") (default-features #t) (kind 0)))) (hash "0sl5qkyp6k9d5slbcacv8zxm9z091r7ddbg01fb81fmnkcy4hri1")))

(define-public crate-rotate-enum-0.1 (crate (name "rotate-enum") (vers "0.1.0") (deps (list (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "trybuild") (req "^1.0") (features (quote ("diff"))) (default-features #t) (kind 2)))) (hash "165iwd90v8gqssdlvb631bi3y38djdbqn9wpfli5mvk2dl8gb0pb")))

(define-public crate-rotate-enum-0.1 (crate (name "rotate-enum") (vers "0.1.1") (deps (list (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (default-features #t) (kind 0)))) (hash "1nzwa4f8lrmxcghj4sjn0ilajsqjd9ny29sphg8f6b3w6iibx916")))

(define-public crate-rotate-enum-0.1 (crate (name "rotate-enum") (vers "0.1.2") (deps (list (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (default-features #t) (kind 0)))) (hash "120rl0dmq7lan6p4hgqkwp1jvxhwcd2gfx4qg7b4wphg93br92q0")))

(define-public crate-rotate-puts-0.2 (crate (name "rotate-puts") (vers "0.2.0") (deps (list (crate-dep (name "clap") (req "^2.33") (default-features #t) (kind 0)) (crate-dep (name "rust_util") (req "^0.6.41") (default-features #t) (kind 0)))) (hash "09dgbsas34qz3xgd7v8ywasj1k5jjdw7b3mss7fjziq22ifc7cph")))

(define-public crate-rotate-puts-0.3 (crate (name "rotate-puts") (vers "0.3.0") (deps (list (crate-dep (name "clap") (req "^2.33") (default-features #t) (kind 0)) (crate-dep (name "daemonize") (req "^0.5.0") (default-features #t) (kind 0)) (crate-dep (name "rust_util") (req "^0.6.41") (default-features #t) (kind 0)))) (hash "1n4zd4v2h0dsbw2xgwv2jyb7ab5pkqk9idgb3hcpzcna6d6pn6aq")))

(define-public crate-rotated-array-set-0.1 (crate (name "rotated-array-set") (vers "0.1.0") (deps (list (crate-dep (name "criterion") (req "^0.3.0") (default-features #t) (kind 2)) (crate-dep (name "is_sorted") (req "^0.1.1") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.4") (default-features #t) (kind 2)) (crate-dep (name "proptest") (req "^0.9") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.6.5") (default-features #t) (kind 2)))) (hash "039d0xjr5jc5ykma9sxfhih32991yapd7sxrhrl99k3sk27mnr71")))

(define-public crate-rotated-array-set-0.1 (crate (name "rotated-array-set") (vers "0.1.1") (deps (list (crate-dep (name "criterion") (req "^0.3.0") (default-features #t) (kind 2)) (crate-dep (name "is_sorted") (req "^0.1.1") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.4") (default-features #t) (kind 2)) (crate-dep (name "proptest") (req "^0.9") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.6.5") (default-features #t) (kind 2)))) (hash "0xdss41ybyx9bnvp40vsxjc2dkas31gh73r990sms7pq3dy2kz44")))

(define-public crate-rotated-grid-0.0.1 (crate (name "rotated-grid") (vers "0.0.1") (deps (list (crate-dep (name "opencv") (req "^0.82.1") (features (quote ("imgproc" "imgcodecs" "highgui"))) (kind 2)))) (hash "1sivahhzxgk25maylran4q6197vi3iydm53phv2pc7lkr30csq5s") (yanked #t) (rust-version "1.56")))

(define-public crate-rotated-grid-0.1 (crate (name "rotated-grid") (vers "0.1.0") (hash "0wzadmzr18sc5ds2dn722w6jbkwsy2kcz7la2dd7cbh18awk9nd8") (rust-version "1.59")))

(define-public crate-rotated-grid-0.1 (crate (name "rotated-grid") (vers "0.1.1") (hash "0xdij5qax9z01d904n4g7gs1pk41xzp2hl8jw243xcmj5r7s615k") (rust-version "1.59")))

(define-public crate-rotated-vec-0.1 (crate (name "rotated-vec") (vers "0.1.0") (deps (list (crate-dep (name "is_sorted") (req "^0.1.1") (default-features #t) (kind 2)) (crate-dep (name "itertools") (req "^0.8.0") (default-features #t) (kind 0)) (crate-dep (name "proptest") (req "^0.9") (default-features #t) (kind 2)))) (hash "0i8h64jflic5lajgrj48gnlvm4m5zxbhxwmra27gxfbi9m0biim5")))

(define-public crate-rotated-vec-0.1 (crate (name "rotated-vec") (vers "0.1.1") (deps (list (crate-dep (name "is_sorted") (req "^0.1.1") (default-features #t) (kind 2)) (crate-dep (name "itertools") (req "^0.8.0") (default-features #t) (kind 0)) (crate-dep (name "proptest") (req "^0.9") (default-features #t) (kind 2)))) (hash "1i4xkgsqyvrb81bgiplxf5gyr226w2069cplvkl8xlgg9cgr0jcr")))

(define-public crate-rotating-buffer-0.2 (crate (name "rotating-buffer") (vers "0.2.0") (hash "1n3zh83llnls8ij6pxyc4d009ci4z797ibs6jb98px9w9s2z5dvh")))

(define-public crate-rotating-file-0.1 (crate (name "rotating-file") (vers "0.1.0") (deps (list (crate-dep (name "chrono") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "flate2") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "zip") (req "^0.5.9") (default-features #t) (kind 0)))) (hash "10bjk73wbivvvxqv8vrl6hlidz6m6dbj7x54xz78w5agxszsggsg")))

(define-public crate-rotating-file-0.2 (crate (name "rotating-file") (vers "0.2.0") (deps (list (crate-dep (name "chrono") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "flate2") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1") (default-features #t) (kind 2)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "zip") (req "^0.5.9") (default-features #t) (kind 0)))) (hash "1c45bn0l0fajh4hsw8cfdnv5lvkspmzgjgw95m325paahjpm2qrc")))

(define-public crate-rotating-file-0.3 (crate (name "rotating-file") (vers "0.3.0") (deps (list (crate-dep (name "chrono") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "flate2") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1") (default-features #t) (kind 2)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "zip") (req "^0.5.9") (default-features #t) (kind 0)))) (hash "0vmp6qcqj2fqdvykxpl0sjdwjghknmw0q0jjbwf9pqld03rfb0xq")))

(define-public crate-rotating-file-0.3 (crate (name "rotating-file") (vers "0.3.1") (deps (list (crate-dep (name "chrono") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "flate2") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1") (default-features #t) (kind 2)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "zip") (req "^0.5.9") (default-features #t) (kind 0)))) (hash "0j31pblpcpdck25lk455670hkg3flidsq5714binyppsnq0b11pd")))

(define-public crate-rotating-file-0.3 (crate (name "rotating-file") (vers "0.3.2") (deps (list (crate-dep (name "chrono") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "flate2") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1") (default-features #t) (kind 2)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "zip") (req "^0.5.9") (default-features #t) (kind 0)))) (hash "0c5jcvqrbzv4bvksigjir0bhirml4irv276mn182k96mdg1d1g6d")))

(define-public crate-rotating-file-0.3 (crate (name "rotating-file") (vers "0.3.3") (deps (list (crate-dep (name "chrono") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "flate2") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1") (default-features #t) (kind 2)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "zip") (req "^0.5.9") (default-features #t) (kind 0)))) (hash "1sgk3h3rnlab5qsc9054610l2sn55ijxqr3l460qi5nhzfzgacyd")))

(define-public crate-rotating-file-0.3 (crate (name "rotating-file") (vers "0.3.4") (deps (list (crate-dep (name "chrono") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "flate2") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1") (default-features #t) (kind 2)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "zip") (req "^0.5.9") (default-features #t) (kind 0)))) (hash "1s8pjszwdvqcdikb2hmj8k578hb2f0gzf6v22hj17zz5grrssdkl")))

(define-public crate-rotating-file-0.3 (crate (name "rotating-file") (vers "0.3.5") (deps (list (crate-dep (name "chrono") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "flate2") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1") (default-features #t) (kind 2)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "zip") (req "^0.5.9") (default-features #t) (kind 0)))) (hash "15i2s3p1cgk6i7iz3daklvafh35cjcxdb9g6qag4n03xd2railh2")))

(define-public crate-rotating-file-0.3 (crate (name "rotating-file") (vers "0.3.6") (deps (list (crate-dep (name "chrono") (req "^0.4.19") (default-features #t) (kind 0)) (crate-dep (name "flate2") (req "^1.0.23") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.16") (default-features #t) (kind 0)) (crate-dep (name "once_cell") (req "^1.10.0") (default-features #t) (kind 2)) (crate-dep (name "zip") (req "^0.6.2") (default-features #t) (kind 0)))) (hash "1x0chdjxvzljlqf3a06b9b41z4jnfz5jfskck60aigal91v077dg")))

(define-public crate-rotation-0.1 (crate (name "rotation") (vers "0.1.1") (deps (list (crate-dep (name "gcd-bitwise") (req "^0.1.6") (default-features #t) (kind 0)))) (hash "1q8lqhbvb7xbxbcdw50kzmancblwcyr6s09gfa6gjkr8shyzwkik")))

(define-public crate-rotato-0.1 (crate (name "rotato") (vers "0.1.2") (hash "16d353b65h0rnqarmqq839krpzjm17svbz0l1wf8shg4lf4zjyhy")))

