(define-module (crates-io ro q_) #:use-module (crates-io))

(define-public crate-roq_derive-0.1 (crate (name "roq_derive") (vers "0.1.0") (deps (list (crate-dep (name "quote") (req "^1.0.35") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2.0.58") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "1d7hnrkg516350mdwd9z9675j8j9fr7zh6z7a4lwj9j8v1fkapki")))

