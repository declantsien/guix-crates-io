(define-module (crates-io ro lt) #:use-module (crates-io))

(define-public crate-rolt-0.1 (crate (name "rolt") (vers "0.1.0+Jolt-5.0.0") (deps (list (crate-dep (name "joltc-sys") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "1ygvh73p4ry3j543n7a0svwyh0drfp46bf7j29z6bf062cd6g4fm") (features (quote (("object-layer-u32" "joltc-sys/object-layer-u32") ("double-precision" "joltc-sys/double-precision"))))))

(define-public crate-rolt-0.1 (crate (name "rolt") (vers "0.1.1+Jolt-5.0.0") (deps (list (crate-dep (name "joltc-sys") (req "^0.1.1") (default-features #t) (kind 0)))) (hash "041dbdzhrcrrdpv7r18dpprbdkcjvphvshjhj0jzvgh5kx4apgv7") (features (quote (("object-layer-u32" "joltc-sys/object-layer-u32") ("double-precision" "joltc-sys/double-precision"))))))

(define-public crate-rolt-0.2 (crate (name "rolt") (vers "0.2.0+Jolt-5.0.0") (deps (list (crate-dep (name "joltc-sys") (req "^0.2.0") (default-features #t) (kind 0)))) (hash "017xj3fy8k2bsf505nya8amarhx9302dn3ndhs7yyjlwcczxlrlh") (features (quote (("object-layer-u32" "joltc-sys/object-layer-u32") ("double-precision" "joltc-sys/double-precision"))))))

(define-public crate-rolt-0.3 (crate (name "rolt") (vers "0.3.0+Jolt-5.0.0") (deps (list (crate-dep (name "glam") (req "^0.27.0") (default-features #t) (kind 0)) (crate-dep (name "joltc-sys") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "paste") (req "^1.0.15") (default-features #t) (kind 0)))) (hash "05215bvlkxx69p0gipdwlnsndgc3sd6xyzgb4xymf9dh6qidr79j") (features (quote (("object-layer-u32" "joltc-sys/object-layer-u32") ("double-precision" "joltc-sys/double-precision"))))))

(define-public crate-rolt-0.3 (crate (name "rolt") (vers "0.3.1+Jolt-5.0.0") (deps (list (crate-dep (name "glam") (req "^0.27.0") (default-features #t) (kind 0)) (crate-dep (name "joltc-sys") (req "^0.3.1") (default-features #t) (kind 0)) (crate-dep (name "paste") (req "^1.0.15") (default-features #t) (kind 0)))) (hash "0q9rbpdmhsnxsxh90sxlxgj1x7x3zr7wvzi6qzsnkxqnbhvpa1nx") (features (quote (("object-layer-u32" "joltc-sys/object-layer-u32") ("double-precision" "joltc-sys/double-precision"))))))

