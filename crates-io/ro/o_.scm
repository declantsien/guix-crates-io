(define-module (crates-io ro o_) #:use-module (crates-io))

(define-public crate-roo_engine-0.1 (crate (name "roo_engine") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^3.2") (features (quote ("cargo"))) (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "rocket") (req "^0.5.0-rc.2") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "toml") (req "^0.4.2") (default-features #t) (kind 0)))) (hash "02p872xs862rqd0mfp5wpkrmdmwlr349flk0zfrpgax107ifn0h6")))

(define-public crate-roo_parser-0.1 (crate (name "roo_parser") (vers "0.1.0") (deps (list (crate-dep (name "byteorder") (req "^1.5.0") (default-features #t) (kind 0)))) (hash "0v1b6xsh66lpqbaaphk14zjg2apyckg8bli5nd216jl3dg501c23")))

