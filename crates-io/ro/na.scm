(define-module (crates-io ro na) #:use-module (crates-io))

(define-public crate-ronat-0.1 (crate (name "ronat") (vers "0.1.0") (deps (list (crate-dep (name "compiletest_rs") (req "^0.2.5") (default-features #t) (kind 2)) (crate-dep (name "ispell") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "markdown") (req "^0.2.0") (default-features #t) (kind 0)))) (hash "0s6f98cs2immr7hzb1rs6rim0ndjkhb0556yma6ans959ddbvwi1")))

