(define-module (crates-io ro tc) #:use-module (crates-io))

(define-public crate-rotcipher-0.1 (crate (name "rotcipher") (vers "0.1.0") (hash "0kajz6qvbx71r36zhwa1zcw1f5jliz0lya88czs37c2h9l4ag1g0") (yanked #t)))

(define-public crate-rotcipher-0.1 (crate (name "rotcipher") (vers "0.1.1") (hash "1v0gq3waxhnj948wn1ws6x4gnzidy2xhdh08zd0a99y13lrz7z7r") (yanked #t)))

(define-public crate-rotcipher-0.1 (crate (name "rotcipher") (vers "0.1.2") (hash "1pqxn6x723p9wksfk67jmkg1nwwrh9zdw11wwdj0359d9rn0dnpz") (yanked #t)))

(define-public crate-rotcipher-0.1 (crate (name "rotcipher") (vers "0.1.3") (hash "17si16vf88p0ypq6ryklsa489179dyvx1w5i1z9xi2lg205k0cny") (yanked #t)))

