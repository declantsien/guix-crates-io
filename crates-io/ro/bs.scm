(define-module (crates-io ro bs) #:use-module (crates-io))

(define-public crate-robs-0.1 (crate (name "robs") (vers "0.1.0") (deps (list (crate-dep (name "stringr") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "079mcnaiwdsmp1f1dk3j7dk3rzwi1i8h3qp4zrxkwrx5k6zj9yh8")))

(define-public crate-robs-0.1 (crate (name "robs") (vers "0.1.1") (deps (list (crate-dep (name "stringr") (req "^0.1.1") (default-features #t) (kind 0)))) (hash "1fdh3m1n20mxyr3jll6d1mzhi5r0pi3w68ig48bmi5g4gxiq9znn")))

(define-public crate-robs-0.1 (crate (name "robs") (vers "0.1.2") (deps (list (crate-dep (name "stringr") (req "^0.1.2") (default-features #t) (kind 0)))) (hash "0fkanrq5ijn13cdii56iys6fwg3l9ns1iplb6545gmj0a9aam4dy")))

(define-public crate-robs-0.2 (crate (name "robs") (vers "0.2.0") (deps (list (crate-dep (name "itertools") (req "^0.10.3") (default-features #t) (kind 0)) (crate-dep (name "stringr") (req "^0.1.2") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.31") (default-features #t) (kind 0)))) (hash "0nigiky1am4p4xdmivfpbn59dmilj4nis47ahd1mxm3y71g77j39")))

(define-public crate-robs-0.2 (crate (name "robs") (vers "0.2.1") (deps (list (crate-dep (name "itertools") (req "^0.10.3") (default-features #t) (kind 0)) (crate-dep (name "stringr") (req "^0.1.2") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.31") (default-features #t) (kind 0)))) (hash "14d65ddqh0czl61dmw2i4xipkzshg5syv6cigr07aqfbpcdlgr3p")))

(define-public crate-robs-0.2 (crate (name "robs") (vers "0.2.2") (deps (list (crate-dep (name "itertools") (req "^0.11.0") (default-features #t) (kind 0)) (crate-dep (name "rayon") (req "^1.8.0") (default-features #t) (kind 0)) (crate-dep (name "stringr") (req "^0.1.3") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.49") (default-features #t) (kind 0)))) (hash "07bdhpn0mmbx8hprk2hd6mk70z7lllwm22wssmzxx89krisksw78")))

(define-public crate-robson_compiler-0.1 (crate (name "robson_compiler") (vers "0.1.4") (deps (list (crate-dep (name "crossterm") (req "^0.23.2") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)))) (hash "06d33hzmw793ycjqhnw34yj2qgxgqrfmp43aw4x2v1gl3ybsl5ib")))

(define-public crate-robson_compiler-0.1 (crate (name "robson_compiler") (vers "0.1.5") (deps (list (crate-dep (name "crossterm") (req "^0.23.2") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)))) (hash "12vlpc0ivx6af0kmljxbdmbmiqsg7gr2lxn3jg1c13akjbhhhss4")))

(define-public crate-robson_compiler-0.1 (crate (name "robson_compiler") (vers "0.1.6") (deps (list (crate-dep (name "getrandom") (req "^0.2.8") (features (quote ("js"))) (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)))) (hash "0wlmajsb6ncpszw3xaj3fpr2gpg515f5vjss3fqz2xc532c73ia8")))

