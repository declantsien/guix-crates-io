(define-module (crates-io ro sv) #:use-module (crates-io))

(define-public crate-rosv-1 (crate (name "rosv") (vers "1.0.1") (hash "0ga1hrhrnd87x7l5ci1baxzbd1rrin0a5ym59dgj7j5bzrk5jqk9")))

(define-public crate-rosv-1 (crate (name "rosv") (vers "1.0.2") (hash "0xvbd297dqamg7wyxjvbwbjqpp0x1hi3yjys5scm4lxyn24gki37")))

(define-public crate-rosv-1 (crate (name "rosv") (vers "1.0.3") (hash "0lzwlijn0l2qpzfnpb2hrky1ymrg8xjhsycggx6w6bh5b93qs0ir")))

(define-public crate-rosv-1 (crate (name "rosv") (vers "1.1.0") (deps (list (crate-dep (name "clap") (req "^4.4.14") (features (quote ("derive"))) (default-features #t) (kind 2)) (crate-dep (name "error-stack") (req "^0.4.1") (optional #t) (default-features #t) (kind 0)))) (hash "02c57ll7prw94b2vvlvvwjg5d4q7dw7q6b9qjzgfljiijxylz41c") (features (quote (("default" "fs")))) (v 2) (features2 (quote (("fs" "dep:error-stack"))))))

(define-public crate-rosvgtree-0.1 (crate (name "rosvgtree") (vers "0.1.0") (deps (list (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "roxmltree") (req "^0.18") (default-features #t) (kind 0)) (crate-dep (name "simplecss") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "siphasher") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "svgtypes") (req "^0.9") (default-features #t) (kind 0)) (crate-dep (name "xmlwriter") (req "^0.1") (default-features #t) (kind 2)))) (hash "0l6i81zfcm1kh1a4jc40ivp622yqiiqg05kx7havimh3rqd3vhmx")))

(define-public crate-rosvgtree-0.2 (crate (name "rosvgtree") (vers "0.2.0") (deps (list (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "roxmltree") (req "^0.18") (default-features #t) (kind 0)) (crate-dep (name "simplecss") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "siphasher") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "svgtypes") (req "^0.11") (default-features #t) (kind 0)) (crate-dep (name "xmlwriter") (req "^0.1") (default-features #t) (kind 2)))) (hash "1wrrhxgf2853ggx9rjd9hsnn9ni2njv4lnsqqalchp1w02b7knwc")))

(define-public crate-rosvgtree-0.3 (crate (name "rosvgtree") (vers "0.3.0") (deps (list (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "roxmltree") (req "^0.18") (default-features #t) (kind 0)) (crate-dep (name "simplecss") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "siphasher") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "svgtypes") (req "^0.11") (default-features #t) (kind 0)) (crate-dep (name "xmlwriter") (req "^0.1") (default-features #t) (kind 2)))) (hash "15yl8cbws8nbq9kpla6mqv0497xdgcxl7fhm7grpn3llhirpwx5d")))

