(define-module (crates-io ro t2) #:use-module (crates-io))

(define-public crate-rot26-0.1 (crate (name "rot26") (vers "0.1.0") (hash "0jdlrqf3s5abg523l68whp05z1aa6kb2pcdm409r0wqs7bbblqi9")))

(define-public crate-rot26-0.1 (crate (name "rot26") (vers "0.1.1") (deps (list (crate-dep (name "rayon") (req "^0.9.0") (optional #t) (default-features #t) (kind 0)))) (hash "0d4ml5bbai7n7nwrmxi9vgnpal7byphdwfypyy6l861v04lr2xgy")))

(define-public crate-rot26-0.1 (crate (name "rot26") (vers "0.1.2") (deps (list (crate-dep (name "rayon") (req "^0.9.0") (optional #t) (default-features #t) (kind 0)))) (hash "06936zyckzy5xr8mffqdy16ny4lmla6fpwvj8rx6wak32wqyk6k8")))

(define-public crate-rot26-rs-0.1 (crate (name "rot26-rs") (vers "0.1.0") (hash "02p4cwphibaa3m9b2xvcamqd0xb0wxbdb1zjnckghidcn9yhhwb9")))

(define-public crate-rot26-rs-0.1 (crate (name "rot26-rs") (vers "0.1.1") (hash "1l1zisrz3rzcw2i7y3yl2xh8ldni63ijbm45pjj6shn7hnwxfy68")))

(define-public crate-rot26-rs-0.1 (crate (name "rot26-rs") (vers "0.1.2") (hash "1vzxpp3kq82vffgj86knrvj3vizw3b9f95jzc8dzlbgr32pvb65z")))

