(define-module (crates-io ro cd) #:use-module (crates-io))

(define-public crate-rocdoc-0.1 (crate (name "rocdoc") (vers "0.1.1") (deps (list (crate-dep (name "clap") (req "^3.0.0-beta.1") (default-features #t) (kind 0)) (crate-dep (name "select") (req "^0.5.0") (default-features #t) (kind 0)) (crate-dep (name "test-case") (req "^1.0") (default-features #t) (kind 2)))) (hash "1vxmzwp7cncyyd5757iy94wlis4ilvgvqkw51yvr62qcjssj3rxm")))

(define-public crate-rocdoc-0.1 (crate (name "rocdoc") (vers "0.1.2") (deps (list (crate-dep (name "clap") (req "^3.0.0-beta.1") (default-features #t) (kind 0)) (crate-dep (name "colored") (req "^1.9.3") (default-features #t) (kind 0)) (crate-dep (name "select") (req "^0.5.0") (default-features #t) (kind 0)) (crate-dep (name "term_size") (req "^0.3.2") (default-features #t) (kind 0)) (crate-dep (name "test-case") (req "^1.0") (default-features #t) (kind 2)))) (hash "0shyjarvpdq56gn2d7bads5i4laa790vp0qyi0djphvf2j7lj51q")))

