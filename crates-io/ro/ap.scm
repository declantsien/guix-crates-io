(define-module (crates-io ro ap) #:use-module (crates-io))

(define-public crate-roapi-0.1 (crate (name "roapi") (vers "0.1.0") (hash "1rfk4d0vi7alp9wvg1dry3r6jfwihs34808lm3rdaa2qcjx33xh9")))

(define-public crate-roapi-http-0.1 (crate (name "roapi-http") (vers "0.1.0") (hash "187d1cpw09q6wzba159zs1bbc6dbvfcxf929qvq1azyk0bxnrcmw")))

