(define-module (crates-io ro de) #:use-module (crates-io))

(define-public crate-rodent-0.1 (crate (name "rodent") (vers "0.1.0") (hash "1031cw53wn7vlmmvhnqdcbhf9kqfxkw5p9pap3kb68qabdq5d4dv")))

(define-public crate-rodeo-0.1 (crate (name "rodeo") (vers "0.1.0") (deps (list (crate-dep (name "bumpalo") (req "^3.11.1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "proptest") (req "^1.0.0") (default-features #t) (kind 2)))) (hash "1wyc8d7za0070rxjy9hd2zwkg38vv9hn91c4bkzpwsm534hmfw1x") (features (quote (("default" "bumpalo"))))))

(define-public crate-rodeo-0.1 (crate (name "rodeo") (vers "0.1.1") (deps (list (crate-dep (name "bumpalo") (req "^3.11.1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "proptest") (req "^1.0.0") (default-features #t) (kind 2)))) (hash "1y0qmz5h3kir20iibzh59k03lpdp0r96pjn9m2mql6087plmivny") (features (quote (("std") ("default" "bumpalo" "std")))) (yanked #t)))

(define-public crate-rodeo-0.2 (crate (name "rodeo") (vers "0.2.0") (deps (list (crate-dep (name "bumpalo") (req "^3.11.1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "criterion") (req "^0.4.0") (default-features #t) (kind 2)) (crate-dep (name "typed-arena") (req "^2.0.1") (default-features #t) (kind 2)))) (hash "1igycx9vd88xh0fxxq9ywzy9xbgdn08hkkasxsdszyh8f1hab9qv") (features (quote (("std") ("default" "bumpalo" "std")))) (yanked #t) (rust-version "1.56.1")))

(define-public crate-rodeo-0.2 (crate (name "rodeo") (vers "0.2.1") (deps (list (crate-dep (name "bumpalo") (req "^3.15.4") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "criterion") (req "^0.5.1") (default-features #t) (kind 2)) (crate-dep (name "typed-arena") (req "^2.0.2") (default-features #t) (kind 2)))) (hash "0mjq88fb40qd14w67iqnrl9hs216384hfq07rvm3hx34pipckvb9") (features (quote (("std") ("default" "bumpalo" "std")))) (rust-version "1.73.0")))

