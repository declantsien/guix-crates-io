(define-module (crates-io ro nf) #:use-module (crates-io))

(define-public crate-ronfig-0.1 (crate (name "ronfig") (vers "0.1.0") (deps (list (crate-dep (name "encoding_rs_io") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "ron") (req "^0.6") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (default-features #t) (kind 0)))) (hash "15fbn46n1a2l9b78j14s94r233l69wmylfb6jirqk0ils7d48xiz")))

(define-public crate-ronfig-0.1 (crate (name "ronfig") (vers "0.1.1") (deps (list (crate-dep (name "encoding_rs_io") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "ron") (req "^0.6") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (default-features #t) (kind 0)))) (hash "0mrf26rjv5ji4f17frczgvahkmmmikf4qwd0b535h5dps77sa0j3")))

(define-public crate-ronfmt-0.1 (crate (name "ronfmt") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^2.32.0") (default-features #t) (kind 0)) (crate-dep (name "itertools") (req "^0.7.8") (default-features #t) (kind 0)) (crate-dep (name "pest") (req "^2.0") (default-features #t) (kind 0)) (crate-dep (name "pest_derive") (req "^2.0") (default-features #t) (kind 0)))) (hash "0m4fdjcgpjkpz6mp78xgfmnj7g52anrv4mmnyadpxf4v6fj3jrv4")))

