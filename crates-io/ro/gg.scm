(define-module (crates-io ro gg) #:use-module (crates-io))

(define-public crate-rogg-0.1 (crate (name "rogg") (vers "0.1.0") (deps (list (crate-dep (name "byteorder") (req "^1.2") (default-features #t) (kind 0)) (crate-dep (name "nom") (req "^3.2") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.4") (default-features #t) (kind 2)))) (hash "0ipxyh6az2p1kd75fqs7xccq8cjp7kpn0an4h3g7q90giviz76pr")))

(define-public crate-rogga-0.0.0 (crate (name "rogga") (vers "0.0.0") (hash "10r9nwpg2hvywkcbzi931rr0v42ipw4fvn885g56fscp94hnldci")))

(define-public crate-rogger-0.1 (crate (name "rogger") (vers "0.1.0") (hash "17ki4qak2xscgwchcbqkh5gr4hk4a82vr1mw7jf435vg5klhc42f")))

(define-public crate-rogger-0.1 (crate (name "rogger") (vers "0.1.1") (hash "024xbiw1079ygdr49g1yqw6ngl29wsi7v6rmfasym4jjg88mp61d") (features (quote (("utc_jst") ("jst"))))))

(define-public crate-roggle-0.1 (crate (name "roggle") (vers "0.1.0") (hash "1r2jm5d69pq2j367pcayagrqzryscv4ckiqszmnl1b9qbvafjp47")))

(define-public crate-roggle-0.2 (crate (name "roggle") (vers "0.2.0") (hash "0gwi3n5dqn6vn834bkciv22b2pdv8fn5ramrq58pz672xdys75rf")))

(define-public crate-roggle-0.3 (crate (name "roggle") (vers "0.3.0") (hash "1bf09ahdbhhz3bk99dg9mny2iwls9ad93xpswa136dgvzxdpzf0a")))

(define-public crate-roggle-0.4 (crate (name "roggle") (vers "0.4.0") (hash "0y8nskcs1kcil24f3a1pvyrm7rymhbs8ngx6a6zfpmmb5vgrcbva")))

(define-public crate-roggle-0.5 (crate (name "roggle") (vers "0.5.0") (hash "1f5r2n1sa98j32sapidvd0gyd5w0fhslilpg3kzm1k0cd7aah5gh")))

(define-public crate-roggle-0.5 (crate (name "roggle") (vers "0.5.1") (hash "0nd8nj6s10vgn3dj6w7ilphi0vcf7yvixr52iy1hyn3x66fzgbm7")))

(define-public crate-roggle-0.5 (crate (name "roggle") (vers "0.5.2") (hash "0nl6xraf8v1nlw2mygy1b5pfjz2101bai93lvhsawg4s02lrxpji")))

(define-public crate-roggle-0.6 (crate (name "roggle") (vers "0.6.0") (hash "1ljf1l968z20wh9if4bx72qinwbk8ps7iz1lnmsbrvr2rdh37ca6")))

(define-public crate-roggle-0.7 (crate (name "roggle") (vers "0.7.0") (hash "1qzkmwaj2122b9av781r9yih08wxslk5zrf78g6zhz3izrfzyi6w")))

(define-public crate-roggle-0.7 (crate (name "roggle") (vers "0.7.1") (hash "0vcwi0q25skc0fd0d9gq5f4c0n0pyfv1n38gbrs0zbry4x0ibh06")))

(define-public crate-roggle-0.7 (crate (name "roggle") (vers "0.7.2") (hash "1p4qybf0nkrriny5v13886bzdkwmsxiyc4x4b37l8wrz6ms3j07g")))

