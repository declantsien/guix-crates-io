(define-module (crates-io ro nn) #:use-module (crates-io))

(define-public crate-ronnie-crypto-0.1 (crate (name "ronnie-crypto") (vers "0.1.0") (deps (list (crate-dep (name "hex") (req "^0.4.3") (default-features #t) (kind 2)) (crate-dep (name "openssl") (req "^0.10.57") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.49") (default-features #t) (kind 0)))) (hash "0ma8mfa9kg2pcd5l41cxynj8a3rqqbybssm41mv0cfwr02m3bv4p")))

