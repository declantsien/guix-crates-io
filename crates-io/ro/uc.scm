(define-module (crates-io ro uc) #:use-module (crates-io))

(define-public crate-rouch-0.1 (crate (name "rouch") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^3.0") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "1y44cdkn625fxmpmfzd1y3km5fiwpshm8hl4z3qirisfgwz3ssq1")))

(define-public crate-roucoule-0.1 (crate (name "roucoule") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^4.1.8") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "env_logger") (req "^0.10.0") (default-features #t) (kind 0)) (crate-dep (name "glob") (req "^0.3.1") (default-features #t) (kind 0)) (crate-dep (name "image") (req "^0.24.5") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.17") (features (quote ("max_level_debug" "release_max_level_warn"))) (default-features #t) (kind 0)))) (hash "156hzm4x134gwfkwjsgwdnyk3wspdm61mm2msakl755flxycykx8")))

