(define-module (crates-io ro sd) #:use-module (crates-io))

(define-public crate-rosd-0.1 (crate (name "rosd") (vers "0.1.0") (hash "1244c0wqivk689gqhb7arh2wv4lkiin5hpgsgw0493v3ci7450yx")))

(define-public crate-rosd_cli-0.1 (crate (name "rosd_cli") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^2.33.3") (default-features #t) (kind 0)) (crate-dep (name "rosd") (req "~0.1.0") (kind 0)))) (hash "08k3ld4vwmsqi1wxhjgvbbrr95dn6f2ablvk977m5npf7vklkwj4")))

