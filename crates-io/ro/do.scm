(define-module (crates-io ro do) #:use-module (crates-io))

(define-public crate-rodo_lib-0.1 (crate (name "rodo_lib") (vers "0.1.0") (deps (list (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_with") (req "^2.3.1") (default-features #t) (kind 0)) (crate-dep (name "uuid") (req "^1.3.0") (features (quote ("v4" "fast-rng" "macro-diagnostics" "serde"))) (default-features #t) (kind 0)))) (hash "1ch5dmynn82h51cqimms7mb0jimfv9nmw108rhkz5r9hd5vjn8yy")))

(define-public crate-rodo_lib-0.1 (crate (name "rodo_lib") (vers "0.1.1") (deps (list (crate-dep (name "paste") (req "^1.0.12") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_with") (req "^2.3.1") (default-features #t) (kind 0)) (crate-dep (name "uuid") (req "^1.3.0") (features (quote ("v4" "fast-rng" "macro-diagnostics" "serde"))) (default-features #t) (kind 0)))) (hash "07jj5mz3xbaf4nxn4qid6zqxs50rp2l606hbbbn94xz29v9hpmsl")))

(define-public crate-rodo_lib-0.1 (crate (name "rodo_lib") (vers "0.1.2") (deps (list (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_with") (req "^2.3.2") (default-features #t) (kind 0)) (crate-dep (name "uuid") (req "^1.3.0") (features (quote ("v4" "fast-rng" "macro-diagnostics" "serde"))) (default-features #t) (kind 0)))) (hash "1qjbvi61bm516z7spkjibqj1w0flqa2zj0yjfapjcw03aqs4vlqf")))

(define-public crate-rodo_tui-0.1 (crate (name "rodo_tui") (vers "0.1.0") (deps (list (crate-dep (name "rodo_lib") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "0m8y0769wj8z9w2hr8as3wqmmqfbldhljf3v3s8vnky3pqkwnyzq")))

(define-public crate-rodo_tui-0.1 (crate (name "rodo_tui") (vers "0.1.1") (deps (list (crate-dep (name "rodo_lib") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "0c6vrb5cqj97kf30ldw66hxyp4j92vwvdc9mvllzn39m24mhw9ml")))

