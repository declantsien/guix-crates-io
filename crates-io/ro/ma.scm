(define-module (crates-io ro ma) #:use-module (crates-io))

(define-public crate-roma-0.1 (crate (name "roma") (vers "0.1.0") (deps (list (crate-dep (name "bstr") (req "^0.2") (default-features #t) (kind 0)))) (hash "0j3k8hkcsmv0m1c45hxw68c1rb36m4h1fwlipscchjbxknlraglm")))

(define-public crate-romad-0.1 (crate (name "romad") (vers "0.1.0") (hash "0vkgcnvms095v256045wkgagb30lilhsiv77v4iai277hy2p9vmp")))

(define-public crate-romad-0.1 (crate (name "romad") (vers "0.1.0-0") (hash "0209fai5rk1adkcb6rjvhx5hcdyw3wxqirjicrfwx53ydd980f3g")))

(define-public crate-romad-0.1 (crate (name "romad") (vers "0.1.0-1") (hash "1i696lvk9dkpbbilnjzspf9345wyb7dvk88kjmy4ls2s33lvrasy")))

(define-public crate-romad-0.2 (crate (name "romad") (vers "0.2.0") (deps (list (crate-dep (name "env_logger") (req "^0.7.1") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.10.8") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^0.2.22") (default-features #t) (kind 2)))) (hash "0z6wb3n465j6nmkkbyla5ajq21jikq2n2iprbf72nsf93qqinbs3")))

(define-public crate-romaji-0.1 (crate (name "romaji") (vers "0.1.0") (hash "16bjb0zq2dsbm3biccf7rzqbmsh7r7qzjlgvcbywqw3vyvka89ps")))

(define-public crate-romaji-0.1 (crate (name "romaji") (vers "0.1.1") (hash "1y96npzpksd1404xgirvqib20360l1nl1znfjckgxx8mhclh9ag8")))

(define-public crate-roman-0.0.1 (crate (name "roman") (vers "0.0.1") (hash "1fpnc01lzzhy07jvmifwsdivhj6g5qc0nf7w6l940ql304p4xf24")))

(define-public crate-roman-0.0.2 (crate (name "roman") (vers "0.0.2") (hash "1fdpc7chh1pxnq6q7d008yvll83jfz8ws7q67wd7q9rcs7vj8jdg")))

(define-public crate-roman-0.0.3 (crate (name "roman") (vers "0.0.3") (hash "0z81illbal616lhmp1b1fr0blq4jdr3gmfk4fps6i3cxg61fjd8k")))

(define-public crate-roman-0.0.4 (crate (name "roman") (vers "0.0.4") (hash "0dsps0mqqd1c589f99k65n5gahvwp0lv5q6klmias627fgdxkkbq")))

(define-public crate-roman-0.1 (crate (name "roman") (vers "0.1.0") (hash "0z4aqx9whkd9mb5hh7y9fqcdk569s0iiqvngh6ikjzbdbdj923bp")))

(define-public crate-roman-0.1 (crate (name "roman") (vers "0.1.1") (hash "1cpkf2a88s5ifyzl0a153ny6is82mb34v7rii7ixlvj6dkywj6g5")))

(define-public crate-roman-0.1 (crate (name "roman") (vers "0.1.2") (hash "02gx1awhpd1rd4gvqysydvj0yskd9ijy9l80zmycfii1r9lmmr30")))

(define-public crate-roman-0.1 (crate (name "roman") (vers "0.1.3") (hash "0i38bd4799vy5x8nm2p411q6jzlk8bzgvlar7p68jrgf0zmqkfxh")))

(define-public crate-roman-0.1 (crate (name "roman") (vers "0.1.4") (hash "1d6d4avfi2b6ihfw44z869bvyfl27sgl8mbarispv3zsqvgh7m86")))

(define-public crate-roman-0.1 (crate (name "roman") (vers "0.1.5") (hash "0k75359ydgqv44zygpjigdxrbp0s28k2w8xc9pv2kc6a3g0i66q7")))

(define-public crate-roman-0.1 (crate (name "roman") (vers "0.1.6") (hash "16wdsmkw9qzsd16lrfw56yz0lbff9gyi160mjggj9bi7r3q47ifj")))

(define-public crate-roman-literals-0.1 (crate (name "roman-literals") (vers "0.1.0") (deps (list (crate-dep (name "paste") (req "^1.0.8") (default-features #t) (kind 0)))) (hash "173jp8nsjqv0qc5fn3wyvkfpzqz8d2f1g6mhjah448hj0ficpxbq")))

(define-public crate-roman-literals-0.2 (crate (name "roman-literals") (vers "0.2.0") (deps (list (crate-dep (name "paste") (req "^1.0.8") (default-features #t) (kind 0)))) (hash "0nsara4jcynmsi8299cjzpwib34z9liwbk3ncbrvksm0sn71viwy")))

(define-public crate-roman-literals-0.2 (crate (name "roman-literals") (vers "0.2.1") (deps (list (crate-dep (name "paste") (req "^1.0.8") (default-features #t) (kind 0)))) (hash "1hacrgffifvzigly4p3zih4k8d1s1d4vz2rci5pdcn6ybn7zdyi2")))

(define-public crate-roman-numerals-0.1 (crate (name "roman-numerals") (vers "0.1.0") (hash "15yfznpk7fppf4rr2ix8lb7p286q1w3hji7f63kla0l66w1r864j")))

(define-public crate-roman-numerals-0.2 (crate (name "roman-numerals") (vers "0.2.0") (hash "102y2kxwks0r5a1d7ggm8m2501w0qlw3gspmj6dmfx9jb7pf9gap")))

(define-public crate-roman-numerals-0.3 (crate (name "roman-numerals") (vers "0.3.0") (hash "07z85p5qdsc3h5j8mkz0pamgq0bbpszbpsacmr3dlr0jrz6xm435")))

(define-public crate-roman-numerals-0.4 (crate (name "roman-numerals") (vers "0.4.0") (hash "1g9g2lvl7968hl8d5w0z3p1awv559d3xrjb9scc4pldm3lpxk6p0")))

(define-public crate-roman_encoder-1 (crate (name "roman_encoder") (vers "1.0.2") (hash "070hw9c7c0wgkspqa04cwffwdvvapc6xvv2mrjam85glhfqxnn7v")))

(define-public crate-roman_encoder-1 (crate (name "roman_encoder") (vers "1.1.0") (hash "0m0bgz56vbl0707sqn9nlb18m20l1q95lw525d77hybz59376rlv")))

(define-public crate-roman_int-0.1 (crate (name "roman_int") (vers "0.1.0") (deps (list (crate-dep (name "num-traits") (req "^0.2.15") (default-features #t) (kind 0)))) (hash "1xmjhfk7iscm76n8glpxskz3pdfacih2dd36qq6w8p0wxq1jdwl9")))

(define-public crate-roman_numeral-0.1 (crate (name "roman_numeral") (vers "0.1.0") (hash "1b0fi9qjnc63g5qh6yl5czjnl7xcq217iaxqyq8clghy5bm4vfk6")))

(define-public crate-roman_numerals_converter-0.1 (crate (name "roman_numerals_converter") (vers "0.1.0") (hash "0sag5nsmarz0nibh96zzq8dqnnakja9rl77dikswr4d3wdmfy0ag")))

(define-public crate-romantic-0.1 (crate (name "romantic") (vers "0.1.0") (deps (list (crate-dep (name "num") (req "^0.4.0") (default-features #t) (kind 0)) (crate-dep (name "test-case") (req "^2.0.2") (default-features #t) (kind 2)) (crate-dep (name "thiserror") (req "^1.0.30") (default-features #t) (kind 0)))) (hash "0mx5fpxjx6rgm4w1phbazj8gy866nad7sj8ky78k21cz6q85sp9p")))

(define-public crate-romantic-0.1 (crate (name "romantic") (vers "0.1.1") (deps (list (crate-dep (name "num") (req "^0.4.0") (default-features #t) (kind 0)) (crate-dep (name "test-case") (req "^2.2.1") (default-features #t) (kind 2)) (crate-dep (name "thiserror") (req "^1.0.36") (default-features #t) (kind 0)))) (hash "1r7w627kd2cawi5mnnzbswfjhzyj92q2wsplvr0q1wmis24072ya")))

(define-public crate-romantic-0.1 (crate (name "romantic") (vers "0.1.2") (deps (list (crate-dep (name "num") (req "^0.4.1") (default-features #t) (kind 0)) (crate-dep (name "test-case") (req "^3.3.1") (default-features #t) (kind 2)) (crate-dep (name "thiserror") (req "^1.0.56") (default-features #t) (kind 0)))) (hash "19rhczpjmrgcbcsvdcmj6k06dva9gara45jipfwqc93xv4yhv92g")))

