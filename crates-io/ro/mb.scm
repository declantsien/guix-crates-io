(define-module (crates-io ro mb) #:use-module (crates-io))

(define-public crate-rombok-0.1 (crate (name "rombok") (vers "0.1.0") (deps (list (crate-dep (name "rombok_macro") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "1w9mb0s0v0arn970hdakhnbxw6f5wwxxix8yq4mjh1hrf78qggc1")))

(define-public crate-rombok-0.1 (crate (name "rombok") (vers "0.1.1") (deps (list (crate-dep (name "rombok_macro") (req "^0.1.1") (default-features #t) (kind 0)))) (hash "02hsw8khqrjzbxldcn7zacyw4rgfis3779viz83bsr24kx3sia28")))

(define-public crate-rombok-0.1 (crate (name "rombok") (vers "0.1.2") (deps (list (crate-dep (name "rombok_macro") (req "^0.1.1") (default-features #t) (kind 0)))) (hash "05xd3vqi5mwizs6ih19rzkclm1zs0jr3hq13f6nwk7phmdfwngh3")))

(define-public crate-rombok-0.2 (crate (name "rombok") (vers "0.2.0") (deps (list (crate-dep (name "rombok_macro") (req "^0.2.0") (default-features #t) (kind 0)))) (hash "1sn5z7y9jx762qmi9b3r8vnp4kcpllzimrl7aw8s2b2xma6lcggy")))

(define-public crate-rombok_macro-0.1 (crate (name "rombok_macro") (vers "0.1.0") (hash "0x9jvqzyqagkh7p18qnrfj80bgj2zvmny6b5pdbrzgkcz28mc938")))

(define-public crate-rombok_macro-0.1 (crate (name "rombok_macro") (vers "0.1.1") (hash "1433906vqbgmlxagrh4b356hvbsflzx3ggh9kzwzhr7hn7ynjvx1")))

(define-public crate-rombok_macro-0.2 (crate (name "rombok_macro") (vers "0.2.0") (hash "0kl2n6lzlhksdyyxliw072w1b3rrici3m65xcx6w6ms5m4kklxvj")))

