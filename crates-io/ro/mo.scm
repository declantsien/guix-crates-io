(define-module (crates-io ro mo) #:use-module (crates-io))

(define-public crate-romodoro-0.1 (crate (name "romodoro") (vers "0.1.0") (deps (list (crate-dep (name "chrono") (req "^0.4.6") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^2.32.0") (default-features #t) (kind 0)) (crate-dep (name "ctrlc") (req "^3.1.1") (default-features #t) (kind 0)) (crate-dep (name "termion") (req "^1.5.1") (default-features #t) (kind 0)))) (hash "146fiqqy858anzaps7m5j3n6dcbs6j3mga1nichs2ds7r6wszfjc")))

(define-public crate-romodoro-0.2 (crate (name "romodoro") (vers "0.2.0") (deps (list (crate-dep (name "chrono") (req "^0.4.6") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^2.32.0") (default-features #t) (kind 0)) (crate-dep (name "ctrlc") (req "^3.1.1") (default-features #t) (kind 0)) (crate-dep (name "termion") (req "^1.5.1") (default-features #t) (kind 0)))) (hash "11f2hhrn856lk1d3lx6hx2r0s3l636l6v9fh1w5kmr8l90lybkb7")))

(define-public crate-romodoro-0.3 (crate (name "romodoro") (vers "0.3.0") (deps (list (crate-dep (name "chrono") (req "^0.4.6") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^2.32.0") (default-features #t) (kind 0)) (crate-dep (name "ctrlc") (req "^3.1.1") (default-features #t) (kind 0)) (crate-dep (name "termion") (req "^1.5.1") (default-features #t) (kind 0)))) (hash "0hapsppjm3j605hj4skkl1qsmhmlyccgjxivzd4ggxa1f1bp0hc7")))

(define-public crate-romodoro-0.4 (crate (name "romodoro") (vers "0.4.0") (deps (list (crate-dep (name "chrono") (req "^0.4.6") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^2.32.0") (default-features #t) (kind 0)) (crate-dep (name "ctrlc") (req "^3.1.1") (default-features #t) (kind 0)) (crate-dep (name "termion") (req "^1.5.1") (default-features #t) (kind 0)))) (hash "15kqwiyh5wprh47nl3yvyl4mj20cb4m91z5vb491gd5hq026ld1r")))

(define-public crate-romodoro-0.5 (crate (name "romodoro") (vers "0.5.0") (deps (list (crate-dep (name "chrono") (req "^0.4.6") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^2.33.0") (default-features #t) (kind 0)) (crate-dep (name "ctrlc") (req "^3.1.1") (default-features #t) (kind 0)) (crate-dep (name "humantime") (req "^1.2.0") (default-features #t) (kind 0)) (crate-dep (name "termion") (req "^1.5.1") (default-features #t) (kind 0)))) (hash "0w8nxf3w87p9lk0v7lah7manfzpr1ns1nl22cjf0h4bx6janax1n")))

