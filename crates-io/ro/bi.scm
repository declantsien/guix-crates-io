(define-module (crates-io ro bi) #:use-module (crates-io))

(define-public crate-robin-0.1 (crate (name "robin") (vers "0.1.0") (deps (list (crate-dep (name "redis") (req "^0.8.0") (default-features #t) (kind 0)) (crate-dep (name "robin-derives") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.11") (default-features #t) (kind 0)) (crate-dep (name "typesafe-derive-builder") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "uuid") (req "^0.6") (features (quote ("v4"))) (default-features #t) (kind 0)) (crate-dep (name "version-sync") (req "^0.5") (default-features #t) (kind 2)))) (hash "0vsq2bf4bq4r85aygvnnkk0hbcwnw1lk1apmixdmjkm3cynk08c3")))

(define-public crate-robin-0.2 (crate (name "robin") (vers "0.2.0") (deps (list (crate-dep (name "redis") (req "^0.8.0") (default-features #t) (kind 0)) (crate-dep (name "robin-derives") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.11") (default-features #t) (kind 0)) (crate-dep (name "typesafe-derive-builder") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "uuid") (req "^0.6") (features (quote ("v4"))) (default-features #t) (kind 0)) (crate-dep (name "version-sync") (req "^0.5") (default-features #t) (kind 2)))) (hash "1rg14309rq794lrsycxjqj96ixvxic9ci25jji1nvbsickv07kg2")))

(define-public crate-robin-0.3 (crate (name "robin") (vers "0.3.0") (deps (list (crate-dep (name "num_cpus") (req "^1.8.0") (default-features #t) (kind 0)) (crate-dep (name "redis") (req "^0.8.0") (default-features #t) (kind 0)) (crate-dep (name "robin-derives") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.11") (default-features #t) (kind 0)) (crate-dep (name "typesafe-derive-builder") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "uuid") (req "^0.6") (features (quote ("v4"))) (default-features #t) (kind 0)) (crate-dep (name "version-sync") (req "^0.5") (default-features #t) (kind 2)))) (hash "00nnw03kl3wh2zfgswf8dhd519nmyfq26h4sdwqllfqsswjfl07g")))

(define-public crate-robin-derives-0.1 (crate (name "robin-derives") (vers "0.1.0") (deps (list (crate-dep (name "lazy_static") (req "^1.0.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^0.4.2") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^0.12.14") (default-features #t) (kind 0)))) (hash "0ba4d32hq1xpy6s0bz3vs2zcxk3cnzc4x79y95kaklp02df1kbz0")))

(define-public crate-robin-derives-0.2 (crate (name "robin-derives") (vers "0.2.0") (deps (list (crate-dep (name "lazy_static") (req "^1.0.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^0.4.2") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^0.12.14") (default-features #t) (kind 0)))) (hash "0awanign50gj90kfj1xi47a4rhypkx82ldxrx9bbr0j3fk9c82sn")))

(define-public crate-robin-derives-0.3 (crate (name "robin-derives") (vers "0.3.0") (deps (list (crate-dep (name "lazy_static") (req "^1.0.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^0.4.2") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^0.12.14") (default-features #t) (kind 0)))) (hash "13w741gd11mv0lldp4xbpkj65ki7ark23dq23n7vg79i979njani")))

(define-public crate-robin_cli-0.1 (crate (name "robin_cli") (vers "0.1.0") (deps (list (crate-dep (name "anyhow") (req "^1.0.82") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^4.5.4") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "robin_cli_core") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.37.0") (features (quote ("full"))) (default-features #t) (kind 0)) (crate-dep (name "walkdir") (req "^2.5.0") (default-features #t) (kind 0)) (crate-dep (name "zip") (req "^0.6") (default-features #t) (kind 0)))) (hash "129a6gi4ff4f842yw4x00088rspfyvsa5706ywb9m59h9byav4q2")))

(define-public crate-robin_cli-0.1 (crate (name "robin_cli") (vers "0.1.1") (deps (list (crate-dep (name "anyhow") (req "^1.0.82") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^4.5.4") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "robin_cli_core") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.37.0") (features (quote ("full"))) (default-features #t) (kind 0)) (crate-dep (name "walkdir") (req "^2.5.0") (default-features #t) (kind 0)) (crate-dep (name "zip") (req "^0.6") (default-features #t) (kind 0)))) (hash "0imy3xrw9pjnc13gsnv2bvyqwwqr1iv92hqil1icb6c61s7fp8v9")))

(define-public crate-robin_cli_core-0.1 (crate (name "robin_cli_core") (vers "0.1.0") (deps (list (crate-dep (name "anyhow") (req "^1.0.81") (default-features #t) (kind 0)) (crate-dep (name "async-trait") (req "^0.1.79") (default-features #t) (kind 0)) (crate-dep (name "futures") (req "^0.3.30") (default-features #t) (kind 0)) (crate-dep (name "pbr") (req "^1.1.1") (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.12.3") (features (quote ("cookies" "gzip" "brotli" "deflate" "json"))) (default-features #t) (kind 0)) (crate-dep (name "scraper") (req "^0.19.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "tempfile") (req "^3.10.1") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.37.0") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "1llv8ysj00mms8g74ma7gvvf81nmn9k0ngybb003ylgi1ba1zvks")))

(define-public crate-robin_core-0.1 (crate (name "robin_core") (vers "0.1.0") (deps (list (crate-dep (name "nom") (req "^3.2") (default-features #t) (kind 0)))) (hash "0l0cc7q792rkpxxnrq2rvwisr3i0mvbhqn1z1ji8wfzvdbpql6c4")))

(define-public crate-robin_core-0.2 (crate (name "robin_core") (vers "0.2.0") (deps (list (crate-dep (name "nom") (req "^3.2") (default-features #t) (kind 0)))) (hash "0k8jm7g1gvbqp51kph4sbjgylsf6dbja16sg5ayzx177wy1y63ij")))

(define-public crate-robin_core-0.2 (crate (name "robin_core") (vers "0.2.1") (deps (list (crate-dep (name "nom") (req "^3.2") (default-features #t) (kind 0)))) (hash "0xywsvkk6h42nh17ql0wqv722cz0jfmn3iixsr6izxbd29qy8yl2")))

(define-public crate-robin_core-0.2 (crate (name "robin_core") (vers "0.2.3") (deps (list (crate-dep (name "nom") (req "^3.2") (default-features #t) (kind 0)))) (hash "0c1yqzna73wpgqfr4nvw0ppmcjmynllnw73yzzhm1mbkgd7g9k4a")))

(define-public crate-robin_core-0.2 (crate (name "robin_core") (vers "0.2.4") (deps (list (crate-dep (name "nom") (req "^3.2") (default-features #t) (kind 0)))) (hash "18b6dwrf82hqr7hqwdpfzl33m7dfz8isbyqgwpsjb2rfmwnr46wh")))

(define-public crate-robin_core-0.2 (crate (name "robin_core") (vers "0.2.5") (deps (list (crate-dep (name "nom") (req "^3.2") (default-features #t) (kind 0)))) (hash "0wx1f8v0dxxsffknlbnvd68bnc95w675lmhaafh02fqbri9jvq5s")))

(define-public crate-robin_core-0.2 (crate (name "robin_core") (vers "0.2.6") (deps (list (crate-dep (name "nom") (req "^3.2") (default-features #t) (kind 0)))) (hash "1cj2myad212fnw9hry4v2fhjwqjvfsgj6x9dpf5h8q8gdgcc777r")))

(define-public crate-robin_core-0.2 (crate (name "robin_core") (vers "0.2.7") (deps (list (crate-dep (name "nom") (req "^3.2") (default-features #t) (kind 0)))) (hash "0wciml2g93df763mbgr3vj9qgkh37fhrmcdvnmafhxq7pv0xxr70")))

(define-public crate-robin_core-0.2 (crate (name "robin_core") (vers "0.2.8") (deps (list (crate-dep (name "nom") (req "^3.2") (default-features #t) (kind 0)))) (hash "16wizc0d0i8gx1g4zmf9sx2r3fdy591s23jm9nbqwrxczfxp1w8d")))

(define-public crate-robin_core-0.2 (crate (name "robin_core") (vers "0.2.9") (deps (list (crate-dep (name "nom") (req "^3.2") (default-features #t) (kind 0)))) (hash "0nhc0vybjcsr8gn53wgxaq9lkahjgmyvd961hqdcahgpy4vzyxv9")))

(define-public crate-robin_core-0.2 (crate (name "robin_core") (vers "0.2.10") (deps (list (crate-dep (name "nom") (req "^3.2") (default-features #t) (kind 0)))) (hash "14dxqmcpimg49b0n0gaar4d7pl3ijzk4a97cb783mdkfz9b4k43h")))

(define-public crate-robin_core-0.2 (crate (name "robin_core") (vers "0.2.11") (deps (list (crate-dep (name "nom") (req "^3.2") (default-features #t) (kind 0)))) (hash "01fy8c07dr9564hi7bl8iw5q953lmf9m89wii2gzisq6s44jzass")))

(define-public crate-robin_core-0.2 (crate (name "robin_core") (vers "0.2.12") (deps (list (crate-dep (name "nom") (req "^3.2") (default-features #t) (kind 0)))) (hash "19rvd33lrwf1bqvnwl2f7c40ddhhi0ddbhr4li1wrv17kmrhfybb")))

(define-public crate-robin_core-0.2 (crate (name "robin_core") (vers "0.2.13") (deps (list (crate-dep (name "ansi_term") (req "^0.9") (default-features #t) (kind 0)) (crate-dep (name "nom") (req "^3.2") (default-features #t) (kind 0)))) (hash "0mgkxxblaxjw9bjlrkf3wfj8pjy1cz91yk32vvjrsk7yjniv1f9v")))

(define-public crate-robin_core-0.2 (crate (name "robin_core") (vers "0.2.14") (deps (list (crate-dep (name "ansi_term") (req "^0.9") (default-features #t) (kind 0)) (crate-dep (name "nom") (req "^3.2") (default-features #t) (kind 0)))) (hash "017svcwgzaasnnn3jdcwccqaa6rx6g4prki8f9za2pzy9bsn7zck")))

(define-public crate-robin_core-0.2 (crate (name "robin_core") (vers "0.2.15") (deps (list (crate-dep (name "ansi_term") (req "^0.9") (default-features #t) (kind 0)) (crate-dep (name "nom") (req "^3.2") (default-features #t) (kind 0)))) (hash "1c22hdbw3m70gdxxjw10dqw1pq81qcgz86dpm4hnixd9808qpf4c")))

(define-public crate-robin_core-0.2 (crate (name "robin_core") (vers "0.2.16") (deps (list (crate-dep (name "ansi_term") (req "^0.9") (default-features #t) (kind 0)) (crate-dep (name "nom") (req "^3.2") (default-features #t) (kind 0)))) (hash "0mvdmxdm22zwknczn557r8qc8khax35q71w71jyvgb1wsqjmznrz")))

(define-public crate-robin_core-0.2 (crate (name "robin_core") (vers "0.2.17") (deps (list (crate-dep (name "ansi_term") (req "^0.9") (default-features #t) (kind 0)) (crate-dep (name "nom") (req "^3.2") (default-features #t) (kind 0)))) (hash "1xg0sgj6xqmv7p8zaalxds9mxg957diwwva2qngm97jj6m1wcmjk")))

(define-public crate-robin_core-0.2 (crate (name "robin_core") (vers "0.2.18") (deps (list (crate-dep (name "ansi_term") (req "^0.9") (default-features #t) (kind 0)) (crate-dep (name "nom") (req "^3.2") (default-features #t) (kind 0)))) (hash "1g8yph9v537kgj63nbrk03k1ppm6i96iaxlqaw6psgcxs2s96d0l")))

(define-public crate-robin_core-0.3 (crate (name "robin_core") (vers "0.3.0") (deps (list (crate-dep (name "ansi_term") (req "^0.9") (default-features #t) (kind 0)) (crate-dep (name "pest") (req "^1.0.0-beta") (default-features #t) (kind 0)) (crate-dep (name "pest_derive") (req "^1.0.0-beta") (default-features #t) (kind 0)))) (hash "15lpgjn8b21rrzvwwj7kngv8c5wk8mrvmxspgicay8qscrafln15")))

(define-public crate-robin_core-0.3 (crate (name "robin_core") (vers "0.3.1") (deps (list (crate-dep (name "ansi_term") (req "^0.9") (default-features #t) (kind 0)) (crate-dep (name "pest") (req "^1.0.0-beta") (default-features #t) (kind 0)) (crate-dep (name "pest_derive") (req "^1.0.0-beta") (default-features #t) (kind 0)))) (hash "1ad2fkd4ws9aap3qxwc8vjcd69hls23k7dmzjslbzasvjlikqlk1")))

(define-public crate-robin_core-0.3 (crate (name "robin_core") (vers "0.3.2") (deps (list (crate-dep (name "ansi_term") (req "^0.9") (default-features #t) (kind 0)) (crate-dep (name "pest") (req "^1.0.0-beta") (default-features #t) (kind 0)) (crate-dep (name "pest_derive") (req "^1.0.0-beta") (default-features #t) (kind 0)))) (hash "1hgw6sb4mqbwq72ikc0g7gim81dcavkx19lmgdqff0r0vbbk6g61")))

(define-public crate-robin_core-0.3 (crate (name "robin_core") (vers "0.3.3") (deps (list (crate-dep (name "ansi_term") (req "^0.9") (default-features #t) (kind 0)) (crate-dep (name "pest") (req "^1.0.0-beta") (default-features #t) (kind 0)) (crate-dep (name "pest_derive") (req "^1.0.0-beta") (default-features #t) (kind 0)))) (hash "1s965r9r5m6h9f88sfhrpbc99vv8v4h02skr37j98wpxn4d62kqv")))

(define-public crate-robin_core-0.3 (crate (name "robin_core") (vers "0.3.4") (deps (list (crate-dep (name "ansi_term") (req "^0.9") (default-features #t) (kind 0)) (crate-dep (name "pest") (req "^1.0.0-beta") (default-features #t) (kind 0)) (crate-dep (name "pest_derive") (req "^1.0.0-beta") (default-features #t) (kind 0)))) (hash "1scxg42m335074w9i2ibyhlxnvjihkns879gqmp2sib3d9sz3s57")))

(define-public crate-robin_core-0.3 (crate (name "robin_core") (vers "0.3.5") (deps (list (crate-dep (name "ansi_term") (req "^0.9") (default-features #t) (kind 0)) (crate-dep (name "pest") (req "^1.0.0-beta") (default-features #t) (kind 0)) (crate-dep (name "pest_derive") (req "^1.0.0-beta") (default-features #t) (kind 0)))) (hash "0zai5dp8f19h6lsginapw9l5nqg4cfsd3m86z8mvb334j8scr5pk")))

(define-public crate-robin_core-0.3 (crate (name "robin_core") (vers "0.3.6") (deps (list (crate-dep (name "ansi_term") (req "^0.9") (default-features #t) (kind 0)) (crate-dep (name "pest") (req "^1.0.0-beta") (default-features #t) (kind 0)) (crate-dep (name "pest_derive") (req "^1.0.0-beta") (default-features #t) (kind 0)))) (hash "1z8n839sdas3gg3100jjh6sigi7bz7zqmvcx6hgs4zbh0wsh8ax0")))

(define-public crate-robin_core-0.3 (crate (name "robin_core") (vers "0.3.7") (deps (list (crate-dep (name "ansi_term") (req "^0.9") (default-features #t) (kind 0)) (crate-dep (name "pest") (req "^1.0.0-beta") (default-features #t) (kind 0)) (crate-dep (name "pest_derive") (req "^1.0.0-beta") (default-features #t) (kind 0)))) (hash "0k3f9c5f4s1vy893sy62m41cdnhz2775vhmqzxj5f4d977xi1pys")))

(define-public crate-robin_core-0.3 (crate (name "robin_core") (vers "0.3.8") (deps (list (crate-dep (name "ansi_term") (req "^0.9") (default-features #t) (kind 0)) (crate-dep (name "pest") (req "^1.0.0-beta") (default-features #t) (kind 0)) (crate-dep (name "pest_derive") (req "^1.0.0-beta") (default-features #t) (kind 0)))) (hash "1m3mhbg14wqqhv92qg1632iqb0wnyq10h8hrxgqa196m83fyq3j6")))

(define-public crate-robin_core-0.3 (crate (name "robin_core") (vers "0.3.9") (deps (list (crate-dep (name "ansi_term") (req "^0.9") (default-features #t) (kind 0)) (crate-dep (name "pest") (req "^1.0.0-beta") (default-features #t) (kind 0)) (crate-dep (name "pest_derive") (req "^1.0.0-beta") (default-features #t) (kind 0)))) (hash "0rqvbgdjdxs2k7vl2yjl0c0q0mzyk6sl5x4bml1pidxmzksw49qj")))

(define-public crate-robin_core-0.4 (crate (name "robin_core") (vers "0.4.0") (deps (list (crate-dep (name "ansi_term") (req "^0.9") (default-features #t) (kind 0)) (crate-dep (name "pest") (req "^1.0.0-beta") (default-features #t) (kind 0)) (crate-dep (name "pest_derive") (req "^1.0.0-beta") (default-features #t) (kind 0)))) (hash "0gaspq2zvl033vwgc91iaz1cwasyfhivd122wcln62627wq17yy5")))

(define-public crate-robin_core-0.4 (crate (name "robin_core") (vers "0.4.1") (deps (list (crate-dep (name "ansi_term") (req "^0.9") (default-features #t) (kind 0)) (crate-dep (name "pest") (req "^1.0.0-beta") (default-features #t) (kind 0)) (crate-dep (name "pest_derive") (req "^1.0.0-beta") (default-features #t) (kind 0)))) (hash "0f5mgmv5g11089yc0a1kq6jwqlz5k5wlcks8n97g7v08nss4ais5")))

(define-public crate-robin_core-0.4 (crate (name "robin_core") (vers "0.4.2") (deps (list (crate-dep (name "ansi_term") (req "^0.9") (default-features #t) (kind 0)) (crate-dep (name "pest") (req "^1.0.0-beta") (default-features #t) (kind 0)) (crate-dep (name "pest_derive") (req "^1.0.0-beta") (default-features #t) (kind 0)))) (hash "1zj6m0mnd5g35m00k6acffx0cb2fpjllr5d9bv6av0ck2mfsnq6a")))

(define-public crate-robin_core-0.4 (crate (name "robin_core") (vers "0.4.3") (deps (list (crate-dep (name "ansi_term") (req "^0.9") (default-features #t) (kind 0)) (crate-dep (name "pest") (req "^1.0.0-beta") (default-features #t) (kind 0)) (crate-dep (name "pest_derive") (req "^1.0.0-beta") (default-features #t) (kind 0)))) (hash "1a4gcxvv8zib1gz4k463mfav50ync59gqgb1nhiv43224kl6cyh9")))

(define-public crate-robin_core-0.4 (crate (name "robin_core") (vers "0.4.4") (deps (list (crate-dep (name "ansi_term") (req "^0.9") (default-features #t) (kind 0)) (crate-dep (name "pest") (req "^1.0.0-beta") (default-features #t) (kind 0)) (crate-dep (name "pest_derive") (req "^1.0.0-beta") (default-features #t) (kind 0)))) (hash "0pfk6xkjymn534dnjkmsxpmdpxzc59xlr9xsi3hc4c2f9jrjjnmy")))

(define-public crate-robin_core-0.4 (crate (name "robin_core") (vers "0.4.5") (deps (list (crate-dep (name "ansi_term") (req "^0.9") (default-features #t) (kind 0)) (crate-dep (name "pest") (req "^1.0.0-beta") (default-features #t) (kind 0)) (crate-dep (name "pest_derive") (req "^1.0.0-beta") (default-features #t) (kind 0)))) (hash "1h2wbc36rx27sjra95lwb292a3mryh9j5w8d01g8m592f2rpv360")))

(define-public crate-robin_core-0.4 (crate (name "robin_core") (vers "0.4.6") (deps (list (crate-dep (name "ansi_term") (req "^0.9") (default-features #t) (kind 0)) (crate-dep (name "pest") (req "^1.0.0-beta") (default-features #t) (kind 0)) (crate-dep (name "pest_derive") (req "^1.0.0-beta") (default-features #t) (kind 0)))) (hash "0gnzzvpkwk66w3k062pyv6alxrv10iyv8viivpqxmcwwkn7zdsd7")))

(define-public crate-robin_core-0.4 (crate (name "robin_core") (vers "0.4.7") (deps (list (crate-dep (name "ansi_term") (req "^0.9") (default-features #t) (kind 0)) (crate-dep (name "pest") (req "^1.0.0-beta") (default-features #t) (kind 0)) (crate-dep (name "pest_derive") (req "^1.0.0-beta") (default-features #t) (kind 0)))) (hash "0ii1zwdjc0wb4h1r988w24bjif8ymfy0hzy1gqj4g5dp6cmw6xn8")))

(define-public crate-robin_core-0.4 (crate (name "robin_core") (vers "0.4.8") (deps (list (crate-dep (name "ansi_term") (req "^0.9") (default-features #t) (kind 0)) (crate-dep (name "pest") (req "^1.0.0-beta") (default-features #t) (kind 0)) (crate-dep (name "pest_derive") (req "^1.0.0-beta") (default-features #t) (kind 0)))) (hash "0wy6c788mfa2xp0x336s9n28ylmif22ynd5alycvwb7xwnp6kh1v")))

(define-public crate-robin_core-0.4 (crate (name "robin_core") (vers "0.4.9") (deps (list (crate-dep (name "ansi_term") (req "^0.9") (default-features #t) (kind 0)) (crate-dep (name "pest") (req "^1.0.0-beta") (default-features #t) (kind 0)) (crate-dep (name "pest_derive") (req "^1.0.0-beta") (default-features #t) (kind 0)))) (hash "00wl4581bf1r74457ajcz8diibn7j5rdpzgmv4q6afcf8fxc1c9n")))

(define-public crate-robin_core-0.4 (crate (name "robin_core") (vers "0.4.10") (deps (list (crate-dep (name "ansi_term") (req "^0.9") (default-features #t) (kind 0)) (crate-dep (name "pest") (req "^1.0.0-beta") (default-features #t) (kind 0)) (crate-dep (name "pest_derive") (req "^1.0.0-beta") (default-features #t) (kind 0)))) (hash "1rdp2xa7vl1yw0wpf8w113gna3v3z98nkpwrm6qjjk8gn3sjavvb")))

(define-public crate-robin_core-0.4 (crate (name "robin_core") (vers "0.4.11") (deps (list (crate-dep (name "ansi_term") (req "^0.9") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "pest") (req "^1.0.0-beta") (default-features #t) (kind 0)) (crate-dep (name "pest_derive") (req "^1.0.0-beta") (default-features #t) (kind 0)))) (hash "0kndr0kmhp1rk204yjxa4khd3fcldkqadlh10pkqzn5fian2g6j5")))

(define-public crate-robin_core-0.4 (crate (name "robin_core") (vers "0.4.12") (deps (list (crate-dep (name "ansi_term") (req "^0.9") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "pest") (req "^1.0.0-beta") (default-features #t) (kind 0)) (crate-dep (name "pest_derive") (req "^1.0.0-beta") (default-features #t) (kind 0)))) (hash "048yhyh9sygkmqih28dh1rvcfk5m67j5vk7r0lhdks049vw7j443")))

(define-public crate-robin_core-0.4 (crate (name "robin_core") (vers "0.4.13") (deps (list (crate-dep (name "ansi_term") (req "^0.9") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "pest") (req "^1.0.0-beta") (default-features #t) (kind 0)) (crate-dep (name "pest_derive") (req "^1.0.0-beta") (default-features #t) (kind 0)))) (hash "11c5v2ylq2s67h0zh416856iy0k7h6ryabpdbrn96riw12bbflvq")))

(define-public crate-robin_merge-0.1 (crate (name "robin_merge") (vers "0.1.0") (hash "05ryvs15mifqh20p3sznvxh8i5dacbsrm34vm3j1znwnwzd4023j")))

(define-public crate-robin_merge-0.1 (crate (name "robin_merge") (vers "0.1.1") (hash "0qb40qidsvbnql1j5y8k6p9yp4h479z4mgikjj08brbfnsm80c6b")))

(define-public crate-robinhood-0.1 (crate (name "robinhood") (vers "0.1.0") (deps (list (crate-dep (name "error-chain") (req "^0.10") (default-features #t) (kind 0)) (crate-dep (name "openssl") (req "~0.9") (default-features #t) (target "cfg(not(any(target_os = \"windows\", target_os = \"macos\")))") (kind 0)) (crate-dep (name "reqwest") (req "^0.8.1") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "time") (req "^0.1") (default-features #t) (kind 0)))) (hash "1l79bar3088hpwkdb6gw40v06iy4j3ikrn6p0y4jai1kghdiwh5q")))

(define-public crate-robinhood-0.1 (crate (name "robinhood") (vers "0.1.1-alpha.1") (deps (list (crate-dep (name "chrono") (req "^0.4") (features (quote ("serde"))) (default-features #t) (kind 0)) (crate-dep (name "error-chain") (req "^0.10") (default-features #t) (kind 0)) (crate-dep (name "openssl") (req "~0.9") (default-features #t) (target "cfg(not(any(target_os = \"windows\", target_os = \"macos\")))") (kind 0)) (crate-dep (name "reqwest") (req "^0.8.1") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1") (default-features #t) (kind 0)))) (hash "0lr701hcraqg8jahgfzwmpyg2yky61idk3q09npxgyllxm88n9hq")))

(define-public crate-robinson_mmodules-0.1 (crate (name "robinson_mmodules") (vers "0.1.0") (deps (list (crate-dep (name "avl") (req "^0.7.1") (default-features #t) (kind 0)))) (hash "0705s6y6qgr53kjhl14s2ykjsgx4xgvy9rycmv1l9gwiz0h1kr87") (yanked #t)))

(define-public crate-robinson_mmodules-0.1 (crate (name "robinson_mmodules") (vers "0.1.1") (deps (list (crate-dep (name "avl") (req "^0.7.1") (default-features #t) (kind 0)))) (hash "0ng00d4k4qq7f2zqzksfi9pyqbfx2y69cchnjj05k5khxa4s976r") (yanked #t)))

(define-public crate-robinson_mmodules-0.1 (crate (name "robinson_mmodules") (vers "0.1.2") (deps (list (crate-dep (name "avl") (req "^0.7.1") (default-features #t) (kind 0)))) (hash "0j9ci2frrk374l7q988mr4ww9mjzdkxch7ds77n8knmvprsbv9gy") (yanked #t)))

(define-public crate-robinson_mmodules-0.1 (crate (name "robinson_mmodules") (vers "0.1.3") (deps (list (crate-dep (name "avl") (req "^0.7.1") (default-features #t) (kind 0)))) (hash "1vjaxr05zyw6idmhsfd2lb1il7agagnkh4j4s9kx9ipgplnpnr1x")))

(define-public crate-robinson_modules_test-0.1 (crate (name "robinson_modules_test") (vers "0.1.0") (deps (list (crate-dep (name "avl") (req "^0.7.1") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)))) (hash "0v3mf3kc4dj6csynkp5v54lk83y5y44sgprg1pnvwal9mnjcr3z3") (yanked #t)))

(define-public crate-robinson_modules_test-0.2 (crate (name "robinson_modules_test") (vers "0.2.0") (deps (list (crate-dep (name "avl") (req "^0.7.1") (default-features #t) (kind 0)))) (hash "1yqd6yychp8dv8n4n5j1ydh1cppppi8zx1x4q9dwgcq51p8yzgz5") (yanked #t)))

(define-public crate-robinson_modules_test-0.3 (crate (name "robinson_modules_test") (vers "0.3.0") (deps (list (crate-dep (name "avl") (req "^0.7.1") (default-features #t) (kind 0)))) (hash "1j9p96alzqqspkr2d5wmcxcdiq814p1xgsmaw37kpvnw79x3h7hc") (yanked #t)))

(define-public crate-robinson_modules_test-0.4 (crate (name "robinson_modules_test") (vers "0.4.0") (deps (list (crate-dep (name "avl") (req "^0.7.1") (default-features #t) (kind 0)))) (hash "15wysd8nqwfhcgjdci6fsdqqq4lnrhl1mzqs1bzssa15y77vmqn9") (yanked #t)))

(define-public crate-robinson_modules_test-0.5 (crate (name "robinson_modules_test") (vers "0.5.0") (deps (list (crate-dep (name "avl") (req "^0.7.1") (default-features #t) (kind 0)))) (hash "16xl4ciam1q778z3w2dlzq7naxrn9rp9p3zf8b4gb6k6f7pm0y60") (yanked #t)))

(define-public crate-robius-android-env-0.1 (crate (name "robius-android-env") (vers "0.1.0") (deps (list (crate-dep (name "jni") (req "^0.21.1") (default-features #t) (kind 0)) (crate-dep (name "makepad-android-state") (req "^0.1.0") (optional #t) (default-features #t) (kind 0)))) (hash "0kvcalpgw4ms26p719pdhmbwlf3camp8qjf8l2b6r68d1k28899i") (v 2) (features2 (quote (("makepad" "dep:makepad-android-state"))))))

(define-public crate-robius-open-0.1 (crate (name "robius-open") (vers "0.1.0") (deps (list (crate-dep (name "cfg-if") (req "^1.0.0") (default-features #t) (kind 0)) (crate-dep (name "icrate") (req "^0.1.0") (features (quote ("Foundation" "Foundation_NSString" "Foundation_NSURL"))) (default-features #t) (target "cfg(target_os = \"ios\")") (kind 0)) (crate-dep (name "icrate") (req "^0.1.0") (features (quote ("AppKit" "AppKit_NSWorkspace" "Foundation" "Foundation_NSString" "Foundation_NSURL"))) (default-features #t) (target "cfg(target_os = \"macos\")") (kind 0)) (crate-dep (name "jni") (req "^0.21.1") (target "cfg(target_os = \"android\")") (kind 0)) (crate-dep (name "log") (req "^0.4") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "objc2") (req "^0.5.0") (default-features #t) (target "cfg(target_os = \"ios\")") (kind 0)) (crate-dep (name "robius-android-env") (req "^0.1.0") (default-features #t) (target "cfg(target_os = \"android\")") (kind 0)))) (hash "1nmz4nk2l4v3f46b6gaqnngg7yans9gxbvvswgi0pggyx0s6ck0v") (features (quote (("default" "android-result") ("android-result")))) (v 2) (features2 (quote (("log" "dep:log"))))))

(define-public crate-robius-use-makepad-0.1 (crate (name "robius-use-makepad") (vers "0.1.0") (deps (list (crate-dep (name "robius-android-env") (req "^0.1.0") (optional #t) (default-features #t) (target "cfg(target_os = \"android\")") (kind 0)))) (hash "1i49bxkpszifhnikjnf98993xs9zs4qji6s0iagwv20w27209i0x") (features (quote (("default" "robius-android-env/makepad"))))))

