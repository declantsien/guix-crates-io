(define-module (crates-io ro us) #:use-module (crates-io))

(define-public crate-rousan_main_rust-0.1 (crate (name "rousan_main_rust") (vers "0.1.0") (hash "16rvwbdqpjxnlk0dx3vbdb22s26n5rvn90mqcmkd457dpg0w8dgr")))

(define-public crate-rousan_main_rust-0.1 (crate (name "rousan_main_rust") (vers "0.1.1") (hash "0wyz5ya4cd586kggll74y5vn8wy3x13nsb48g2b6khffkphdxn4z")))

(define-public crate-rousan_main_rust-0.2 (crate (name "rousan_main_rust") (vers "0.2.1") (hash "0a6cpskjfnswqjk8vg69nlbs293d09dp9kkwk66gsf674b30xqi3")))

(define-public crate-rousan_main_rust-0.3 (crate (name "rousan_main_rust") (vers "0.3.0") (hash "1667wqw6x2rpjg5phlnpzpgiphh4a1q4a8j3jgx01mn832zycq2y")))

(define-public crate-rousan_main_rust-0.4 (crate (name "rousan_main_rust") (vers "0.4.0") (hash "0h9p9fvkzv7pvjspz0drpvn0ynapd97z7q85b75n94ifvjpqckhz")))

(define-public crate-rousan_main_rust-0.5 (crate (name "rousan_main_rust") (vers "0.5.0") (hash "0hkj2fwz7xxplmr936i8badz6p9b9x5vakfwcdqpl48lkyhb8x3m")))

(define-public crate-rousan_rust_poc-0.1 (crate (name "rousan_rust_poc") (vers "0.1.0") (deps (list (crate-dep (name "rand") (req "^0.7.3") (default-features #t) (kind 0)))) (hash "12955v3h3cizg707r0splr79nxw95q5gppxmpb4fdsb42q0xcryk")))

(define-public crate-rouse-0.1 (crate (name "rouse") (vers "0.1.0") (deps (list (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "toml") (req "^0.5") (default-features #t) (kind 0)))) (hash "1ywip5x82ffvl3c3xg9l4062559lky95vd0fmnyxqz04810fp239")))

(define-public crate-rouse-0.1 (crate (name "rouse") (vers "0.1.1") (deps (list (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "toml") (req "^0.5") (default-features #t) (kind 0)))) (hash "1x7ys7v8pabbzsajyq0png93rmpl67f2xc3fh2yxxvavy3k2xxcq")))

(define-public crate-rouse-0.1 (crate (name "rouse") (vers "0.1.2") (deps (list (crate-dep (name "fastrand") (req "^1.8.0") (default-features #t) (kind 0)) (crate-dep (name "owo-colors") (req "^3.5.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "toml") (req "^0.5") (default-features #t) (kind 0)))) (hash "0dcjpnbz7c1g1r00dlry0y2yrrdrg3zcxqsw3zp81bfqj086fgbq")))

(define-public crate-rouste-0.1 (crate (name "rouste") (vers "0.1.0") (hash "0xpsxrx8qxrlck6600laa16cw8g1jqicni07cvizpa8759vhxhd7") (yanked #t)))

(define-public crate-rouste-0.1 (crate (name "rouste") (vers "0.1.1") (hash "01lcqkysw1blh8f3wn36n83pzjg7asq3cp9cazy21zg9v35f16bg") (yanked #t)))

(define-public crate-rouste-0.1 (crate (name "rouste") (vers "0.1.2") (hash "05n1ijiwl6fjsikisfrykm3i6pm86cmydm3qq4vv6mfd87qaqrc3") (yanked #t)))

(define-public crate-rouste-0.1 (crate (name "rouste") (vers "0.1.3") (hash "1v65jdrqhjf4cpk247h592l4yw0qf0mfb1w73ni8pngdhhw81g21") (yanked #t)))

(define-public crate-rouste-0.1 (crate (name "rouste") (vers "0.1.4") (hash "0x4vzk69zkggh0xg44nz7p3cp72p3hhkgg8g2zga2nrv3x58lyc1") (yanked #t)))

(define-public crate-rouste-0.1 (crate (name "rouste") (vers "0.1.5") (hash "1aqsv6zx2fcpav4f5qgvb40lfgx1r50ymrkcfkpyhzn5wvnybxjw") (yanked #t)))

(define-public crate-rouste-0.1 (crate (name "rouste") (vers "0.1.6") (hash "0pnf75dzmywmrb9ql3h4pwm3lpw1bhpb49g7n6nycsarl0aiks34") (yanked #t)))

(define-public crate-rouste-0.1 (crate (name "rouste") (vers "0.1.7") (hash "0vbyq83mcdszliw2xk25a66bfhaj0v2wb1n9g3ngpd6z2xws7g9v") (yanked #t)))

(define-public crate-rouste-0.2 (crate (name "rouste") (vers "0.2.0") (hash "0vyxclhv3rv4dcf6wr2mrk3ya0ff2hicnk7i1xy98fwx2vhyli9l")))

(define-public crate-rouste-0.2 (crate (name "rouste") (vers "0.2.1") (hash "0y93j3ydgkild4jhs404lipha4qick4v4llkih944idambc1a9n2")))

