(define-module (crates-io ro m-) #:use-module (crates-io))

(define-public crate-rom-thumbnails-0.1 (crate (name "rom-thumbnails") (vers "0.1.1") (deps (list (crate-dep (name "camino") (req "^1.1.6") (default-features #t) (kind 0)) (crate-dep (name "cxx") (req "^1.0.110") (features (quote ("c++20"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.50") (default-features #t) (kind 0)))) (hash "0fjpl4pjzdwijvbml7m3m2gka5fc66jly440r12n2p1vkc5kn3jz") (features (quote (("default")))) (v 2) (features2 (quote (("cxx" "dep:cxx"))))))

(define-public crate-rom-thumbnails-0.1 (crate (name "rom-thumbnails") (vers "0.1.2") (deps (list (crate-dep (name "camino") (req "^1.1.6") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.50") (default-features #t) (kind 0)))) (hash "1fmxi5p0pqp98v6jfn5lk4f08qvczwam2kkrd79dhmkiw02zmr65") (features (quote (("default"))))))

(define-public crate-rom-thumbnails-0.1 (crate (name "rom-thumbnails") (vers "0.1.3") (deps (list (crate-dep (name "camino") (req "^1.1.6") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.50") (default-features #t) (kind 0)))) (hash "0hc02dnbpp2h8b1vs4dksahrwrgv6ajli161ljd8asxh2rrz72n0") (features (quote (("default"))))))

(define-public crate-rom-thumbnails-0.1 (crate (name "rom-thumbnails") (vers "0.1.4") (deps (list (crate-dep (name "camino") (req "^1.1.6") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.50") (default-features #t) (kind 0)))) (hash "0cbax89rbyxc6x1ph2hr4rbsjvs5xl2mj222a6wxfwr716yk2gwp") (features (quote (("default"))))))

(define-public crate-rom-thumbnails-0.1 (crate (name "rom-thumbnails") (vers "0.1.5") (deps (list (crate-dep (name "camino") (req "^1.1.6") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.50") (default-features #t) (kind 0)))) (hash "0zawhma3c3dwv3qmhmizqii16lm9qljdhmyxfjj8wcqdvm5qv2lv") (features (quote (("default"))))))

(define-public crate-rom-thumbnails-0.1 (crate (name "rom-thumbnails") (vers "0.1.6") (deps (list (crate-dep (name "camino") (req "^1.1.6") (default-features #t) (kind 0)) (crate-dep (name "image") (req "^0.24.7") (features (quote ("png"))) (kind 2)) (crate-dep (name "thiserror") (req "^1.0.50") (default-features #t) (kind 0)))) (hash "0azpwl3xacc5ih44d6qwim28548hg76wbywswg6m1a4niyb3nr1y") (features (quote (("default"))))))

