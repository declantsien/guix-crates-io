(define-module (crates-io ro t-) #:use-module (crates-io))

(define-public crate-rot-13-0.1 (crate (name "rot-13") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^2.33") (default-features #t) (kind 0)))) (hash "0q5a9zqpy7cqsbmsxnlzdgsd1y5j122fby8h99h53l7ahw8b6xcn")))

(define-public crate-rot-13-0.1 (crate (name "rot-13") (vers "0.1.1") (deps (list (crate-dep (name "clap") (req "^2.33") (default-features #t) (kind 0)))) (hash "1ijinbjjfngvwpi3wz3gq1alsjrvjq0kqvwykmrab6z58asr8zjs")))

