(define-module (crates-io ro sc) #:use-module (crates-io))

(define-public crate-rosc-0.1 (crate (name "rosc") (vers "0.1.1") (deps (list (crate-dep (name "byteorder") (req "^0.3.13") (default-features #t) (kind 0)))) (hash "1c2zdy2sx3d7r6hmdisigfkaacfman4fki33ssigs036gqm1yfjq")))

(define-public crate-rosc-0.1 (crate (name "rosc") (vers "0.1.1-2") (deps (list (crate-dep (name "byteorder") (req "^0.3.13") (default-features #t) (kind 0)))) (hash "006y5jb4c50sjp95kc6hkav7xj5rgwvn9r5d5ps46c3vfr3fc9bn")))

(define-public crate-rosc-0.1 (crate (name "rosc") (vers "0.1.2") (deps (list (crate-dep (name "byteorder") (req "^0.3.13") (default-features #t) (kind 0)))) (hash "15cr6xhsh741p4l6xi6rk3pxihzwqr7a4myx6hfgg3zpmc3a1q69")))

(define-public crate-rosc-0.1 (crate (name "rosc") (vers "0.1.3") (deps (list (crate-dep (name "byteorder") (req "^0.3.13") (default-features #t) (kind 0)))) (hash "0j8y8wsyxhcny05sn087ai4l92d6402434x7dmi0q8wlgl4ihfxr")))

(define-public crate-rosc-0.1 (crate (name "rosc") (vers "0.1.4") (deps (list (crate-dep (name "byteorder") (req "^0.3") (default-features #t) (kind 0)))) (hash "1slsmzj16iq3dn23szkyh171znsgc7aaxri779v056nz3wvk0jby")))

(define-public crate-rosc-0.1 (crate (name "rosc") (vers "0.1.5") (deps (list (crate-dep (name "byteorder") (req "^0.5") (default-features #t) (kind 0)) (crate-dep (name "clippy") (req "^0") (optional #t) (default-features #t) (kind 0)))) (hash "056cq2rqq2nn8xw4zz3nw12vrgmjv5p0cbdrx0jf16xzby75lzkd") (features (quote (("lints" "clippy"))))))

(define-public crate-rosc-0.1 (crate (name "rosc") (vers "0.1.6") (deps (list (crate-dep (name "byteorder") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "clippy") (req "^0") (optional #t) (default-features #t) (kind 0)))) (hash "1ndqy5zyhzap82gihw30k7697nbxpczjlscgjmylj1089sj2ncrr") (features (quote (("lints" "clippy"))))))

(define-public crate-rosc-0.2 (crate (name "rosc") (vers "0.2.0") (deps (list (crate-dep (name "byteorder") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "clippy") (req "^0") (optional #t) (default-features #t) (kind 0)))) (hash "104v0xyqdq5y6ldfiw7jypiwsdn5ilylzly8p2c09lgm1a0pi9gj") (features (quote (("lints" "clippy"))))))

(define-public crate-rosc-0.3 (crate (name "rosc") (vers "0.3.0") (deps (list (crate-dep (name "byteorder") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "clippy") (req "^0") (optional #t) (default-features #t) (kind 0)))) (hash "0q9in4galy2p1hm1lf989w793zfs27g3bbwq20cvil7a0qf2d9ph") (features (quote (("lints" "clippy"))))))

(define-public crate-rosc-0.4 (crate (name "rosc") (vers "0.4.0") (deps (list (crate-dep (name "byteorder") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "clippy") (req "^0") (optional #t) (default-features #t) (kind 0)))) (hash "1g00yfwna5ibn7ql4jzwhm6yrvw6bsnbb1p0ssr7984dsnyadnlc") (features (quote (("lints" "clippy"))))))

(define-public crate-rosc-0.4 (crate (name "rosc") (vers "0.4.1") (deps (list (crate-dep (name "byteorder") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "clippy") (req "^0") (optional #t) (default-features #t) (kind 0)))) (hash "08hg99gq68q32qc0n8zyyw853ikj2bh5j95mz94l9h7qigarwr9m") (features (quote (("lints" "clippy"))))))

(define-public crate-rosc-0.4 (crate (name "rosc") (vers "0.4.2") (deps (list (crate-dep (name "byteorder") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "clippy") (req "^0") (optional #t) (default-features #t) (kind 0)))) (hash "1w1bhlappypa9lxp3nnfi1r8pr9dsfyq496s1x68vixl505qj55b") (features (quote (("lints" "clippy"))))))

(define-public crate-rosc-0.4 (crate (name "rosc") (vers "0.4.3") (deps (list (crate-dep (name "byteorder") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "clippy") (req "^0") (optional #t) (default-features #t) (kind 0)))) (hash "0kxxbh70fqjxznwk813h8ji19ld82lizhv3vl3sjbp5inbsf21a7") (features (quote (("lints" "clippy"))))))

(define-public crate-rosc-0.5 (crate (name "rosc") (vers "0.5.0") (deps (list (crate-dep (name "byteorder") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "clippy") (req "^0") (optional #t) (default-features #t) (kind 0)))) (hash "13p59jmc54wx07h7frpq5m7n5lmv1iz26xnfr0d6wwx7slpvzzql") (features (quote (("lints" "clippy"))))))

(define-public crate-rosc-0.5 (crate (name "rosc") (vers "0.5.1") (deps (list (crate-dep (name "byteorder") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "clippy") (req "^0") (optional #t) (default-features #t) (kind 0)))) (hash "1qgagn57zrb1n6bzj2x6lc2awnv1w5z3007zhi737hmmx34h7ws4") (features (quote (("lints" "clippy"))))))

(define-public crate-rosc-0.5 (crate (name "rosc") (vers "0.5.2") (deps (list (crate-dep (name "byteorder") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "clippy") (req "^0") (optional #t) (default-features #t) (kind 0)))) (hash "1p7z1skwf749hl03cyza6g3sfipwjzc6v9fq38cyza0ccjdkbjn2") (features (quote (("lints" "clippy"))))))

(define-public crate-rosc-0.6 (crate (name "rosc") (vers "0.6.0") (deps (list (crate-dep (name "byteorder") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "clippy") (req "^0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "nom") (req "^7") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.5") (default-features #t) (kind 0)))) (hash "181ikf5zzb8k56y7pvgqvpjrsi8p92k4mpv44jazzss0rrq6rwn2") (features (quote (("lints" "clippy"))))))

(define-public crate-rosc-0.7 (crate (name "rosc") (vers "0.7.0") (deps (list (crate-dep (name "byteorder") (req "^1") (kind 0)) (crate-dep (name "clippy") (req "^0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "nom") (req "^7") (features (quote ("alloc"))) (kind 0)))) (hash "1k6bb2zz3frvbfrvmgznnfk0gfhiyza0vwvd1cvmh4cz9c9pg48x") (features (quote (("std") ("lints" "clippy") ("default" "std"))))))

(define-public crate-rosc-0.8 (crate (name "rosc") (vers "0.8.0") (deps (list (crate-dep (name "byteorder") (req "^1") (kind 0)) (crate-dep (name "clippy") (req "^0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "nom") (req "^7") (features (quote ("alloc"))) (kind 0)))) (hash "05306kw80syg5j38kkv54f5h0ac4maz4kqn8fzsmf68136m0dizh") (features (quote (("std") ("lints" "clippy") ("default" "std"))))))

(define-public crate-rosc-0.8 (crate (name "rosc") (vers "0.8.1") (deps (list (crate-dep (name "byteorder") (req "^1") (kind 0)) (crate-dep (name "clippy") (req "^0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "nom") (req "^7") (features (quote ("alloc"))) (kind 0)))) (hash "06f13kqhclz1s7z577nbcymb865vq2n2q3007lcb5rfz6yza4lcq") (features (quote (("std") ("lints" "clippy") ("default" "std"))))))

(define-public crate-rosc-0.9 (crate (name "rosc") (vers "0.9.0") (deps (list (crate-dep (name "byteorder") (req "^1") (kind 0)) (crate-dep (name "clippy") (req "^0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "nom") (req "^7") (features (quote ("alloc"))) (kind 0)))) (hash "04d2q56ij6d406gc0675c9dwv5r2d6a91m1gpvxwkbh6341sbzxk") (features (quote (("std") ("lints" "clippy") ("default" "std"))))))

(define-public crate-rosc-0.9 (crate (name "rosc") (vers "0.9.1") (deps (list (crate-dep (name "byteorder") (req "^1") (kind 0)) (crate-dep (name "clippy") (req "^0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "nom") (req "^7") (features (quote ("alloc"))) (kind 0)))) (hash "1pbman9gy1qpn9dd3pcjlpss0knx0jmd526z6y66w2vldg153dqh") (features (quote (("std") ("lints" "clippy") ("default" "std"))))))

(define-public crate-rosc-0.10 (crate (name "rosc") (vers "0.10.0") (deps (list (crate-dep (name "byteorder") (req "^1") (kind 0)) (crate-dep (name "clippy") (req "^0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "nom") (req "^7") (features (quote ("alloc"))) (kind 0)))) (hash "0rb3h5f1lsx200xwhfffbg9gk5pzcdsbik2m2nq8r96s986vi745") (features (quote (("std") ("lints" "clippy") ("default" "std")))) (rust-version "1.52")))

(define-public crate-rosc-0.9 (crate (name "rosc") (vers "0.9.2") (deps (list (crate-dep (name "byteorder") (req "^1") (kind 0)) (crate-dep (name "clippy") (req "^0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "nom") (req "^7") (features (quote ("alloc"))) (kind 0)))) (hash "1zzlpy92c5gs24vzccnabz2klfchfr8j90j77pq6rbzzs7ka7b5j") (features (quote (("std") ("lints" "clippy") ("default" "std"))))))

(define-public crate-rosc-0.10 (crate (name "rosc") (vers "0.10.1") (deps (list (crate-dep (name "byteorder") (req "^1") (kind 0)) (crate-dep (name "clippy") (req "^0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "nom") (req "^7") (features (quote ("alloc"))) (kind 0)))) (hash "10d9li3zz8jfaiiljqg1sg976facw2qmkwaw93hhn28ddfg3vrmj") (features (quote (("std") ("lints" "clippy") ("default" "std")))) (rust-version "1.52")))

(define-public crate-rosc_supercollider-0.2 (crate (name "rosc_supercollider") (vers "0.2.0") (deps (list (crate-dep (name "byteorder") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "clippy") (req "^0") (optional #t) (default-features #t) (kind 0)))) (hash "05mw6w84wxs3bjc9a46c38rnddwyh7xg08djmz9i5vk0aw3c8dki") (features (quote (("lints" "clippy"))))))

(define-public crate-rosc_supercollider-0.2 (crate (name "rosc_supercollider") (vers "0.2.1-pre.0") (deps (list (crate-dep (name "byteorder") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "clippy") (req "^0") (optional #t) (default-features #t) (kind 0)))) (hash "15cmv0k1kc4bd0k8k8xa68ljr0kbq4km1z86ihlgys7nhjqz20pg") (features (quote (("lints" "clippy"))))))

(define-public crate-roscal_lib-0.1 (crate (name "roscal_lib") (vers "0.1.0") (deps (list (crate-dep (name "chrono") (req "^0.4.34") (features (quote ("serde"))) (default-features #t) (kind 0)) (crate-dep (name "derivative") (req "^2.2.0") (default-features #t) (kind 0)) (crate-dep (name "derive_builder") (req "^0.20.0") (default-features #t) (kind 0)) (crate-dep (name "json-schema-diff") (req "^0.1.7") (default-features #t) (kind 1)) (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "prettyplease") (req "^0.2.16") (default-features #t) (kind 1)) (crate-dep (name "proc-macro2") (req "^1.0.78") (default-features #t) (kind 1)) (crate-dep (name "pulldown-cmark") (req "^0.10.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.35") (default-features #t) (kind 1)) (crate-dep (name "regex") (req "^1.10.2") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.197") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.114") (default-features #t) (kind 1)) (crate-dep (name "serde_json") (req "^1.0.114") (default-features #t) (kind 2)) (crate-dep (name "serde_yaml") (req "^0.9.32") (default-features #t) (kind 2)) (crate-dep (name "strum") (req "^0.26.1") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2.0.50") (features (quote ("full"))) (default-features #t) (kind 1)) (crate-dep (name "uriparse") (req "^0.6.4") (default-features #t) (kind 0)) (crate-dep (name "uuid") (req "^1.6.1") (features (quote ("v4" "v5" "fast-rng" "serde"))) (default-features #t) (kind 0)))) (hash "0nz9jmyrm8fq7p2xdbyyz9wqsa50vm45d88gryh5xzxgghdsy74w")))

