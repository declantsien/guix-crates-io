(define-module (crates-io ro xi) #:use-module (crates-io))

(define-public crate-roxido-0.4 (crate (name "roxido") (vers "0.4.1") (deps (list (crate-dep (name "roxido_macro") (req "^0.4.1") (default-features #t) (kind 0)))) (hash "07zq4imb23y3m95094frgf9hcxvlg70a2y02yfgab77dqhn935qj") (yanked #t)))

(define-public crate-roxido-0.4 (crate (name "roxido") (vers "0.4.2") (deps (list (crate-dep (name "roxido_macro") (req "^0.4.2") (default-features #t) (kind 0)))) (hash "07zxm6h2ikxjnpsl88ljbp8zz3p2pilgj2y5wcw81f29rcwryj9p") (yanked #t)))

(define-public crate-roxido-0.5 (crate (name "roxido") (vers "0.5.0") (deps (list (crate-dep (name "roxido_macro") (req "^0.5.0") (default-features #t) (kind 0)))) (hash "1wvszq3wdjp4vnj2sf0b5xcvkqq72hibw0w6n3hywxnd4vm6dsr1") (yanked #t)))

(define-public crate-roxido-0.6 (crate (name "roxido") (vers "0.6.0") (deps (list (crate-dep (name "cargo_author") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "flate2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.12") (features (quote ("blocking"))) (default-features #t) (kind 0)) (crate-dep (name "tar") (req "^0.4") (default-features #t) (kind 0)))) (hash "09whq528schfh2qr82ib6nibj33v2qyvbyjjdym9ngm7wy8jq9kz")))

(define-public crate-roxido_macro-0.4 (crate (name "roxido_macro") (vers "0.4.1") (deps (list (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.74") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "0c058gscwddgwksr55g67xaa46rk0pyfn8q2w100kim82f40qgas") (yanked #t)))

(define-public crate-roxido_macro-0.4 (crate (name "roxido_macro") (vers "0.4.2") (deps (list (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.75") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "1ngjl9q07l0wn2iggbazr1w2g4hq4wckai6v0dwa6j159ikipl7h") (yanked #t)))

(define-public crate-roxido_macro-0.5 (crate (name "roxido_macro") (vers "0.5.0") (deps (list (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "0qvlj0j1s5hknfvkf2y1mvzibbxhxlmzyn8l03ppjigchxw9z3zy") (yanked #t)))

