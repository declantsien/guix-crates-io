(define-module (crates-io ro wr) #:use-module (crates-io))

(define-public crate-rowrap-0.0.0 (crate (name "rowrap") (vers "0.0.0") (deps (list (crate-dep (name "anyhow") (req "^1.0.68") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.7.1") (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.11.14") (features (quote ("json"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.152") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.91") (default-features #t) (kind 0)) (crate-dep (name "strum") (req "^0.24.1") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "1rw8jvylbss2hvh4irm15bkxwsbhjxjxwsyx16cr8qp9r0kvjrjm")))

(define-public crate-rowrap-rs-0.1 (crate (name "rowrap-rs") (vers "0.1.0") (deps (list (crate-dep (name "anyhow") (req "^1.0.68") (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.11") (features (quote ("json"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "00s156xqphbwq4f5z9aijpqsb52hixw0vkfshiz49px7dyc64mxh") (yanked #t)))

(define-public crate-rowrap-rs-0.0.1 (crate (name "rowrap-rs") (vers "0.0.1") (hash "1qm2pixyv24f1j2s6yqkk57q795sjxjxdsdqlh472d8izdc98s9s") (yanked #t)))

(define-public crate-rowrap-rs-0.0.0 (crate (name "rowrap-rs") (vers "0.0.0") (hash "1b9p7xjx1s8wgs61b8jqihqvl1m5h5gf0as62cw9hi0aqzxxlaq7") (yanked #t)))

(define-public crate-rowrap-rs-0.0.0 (crate (name "rowrap-rs") (vers "0.0.0-moved") (hash "014a53vhlf8gbvq9f2ajjr7dshdbjxw0brb2wrgjmsglh59ik94r") (yanked #t)))

