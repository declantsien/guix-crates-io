(define-module (crates-io ro n_) #:use-module (crates-io))

(define-public crate-ron_to_table-0.1 (crate (name "ron_to_table") (vers "0.1.0") (deps (list (crate-dep (name "ron") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "tabled") (req "^0.11") (features (quote ("std"))) (kind 0)))) (hash "16gwxzqg7w0wwvljrjfpizswpzmi5fb2qi013ay949msjxclm5cd") (features (quote (("color" "tabled/color"))))))

(define-public crate-ron_to_table-0.2 (crate (name "ron_to_table") (vers "0.2.0") (deps (list (crate-dep (name "ron") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "tabled") (req "^0.12") (features (quote ("std"))) (kind 0)))) (hash "1hrw1h5i28kibnkdmqpy3had84l1bqm3ca5x3jfwxcw97j01bf48") (features (quote (("color" "tabled/color"))))))

(define-public crate-ron_to_table-0.3 (crate (name "ron_to_table") (vers "0.3.0") (deps (list (crate-dep (name "ron") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "tabled") (req "^0.15") (features (quote ("std"))) (kind 0)))) (hash "0dkmjs8jg926akvry4qzbprqjq2icqscx73w5pfzvl7cl7xxgizk") (features (quote (("macros" "tabled/macros") ("derive" "tabled/derive") ("ansi" "tabled/ansi"))))))

