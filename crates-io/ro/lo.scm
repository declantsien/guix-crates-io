(define-module (crates-io ro lo) #:use-module (crates-io))

(define-public crate-rolock-0.1 (crate (name "rolock") (vers "0.1.0") (hash "0gkzfkwcv4nhf7hh8mvs73q30cqwry5lcwwn0adcl0i353gw99pr")))

(define-public crate-rolock-0.1 (crate (name "rolock") (vers "0.1.1") (hash "0bl9vjaykzf2xxy4mdjw188m9inkx67rm0vy3dfldbw6qjkpaqyv")))

(define-public crate-rolock-0.1 (crate (name "rolock") (vers "0.1.2") (hash "0gvw49f5ki264dfsw17n3a8fmhr2xwwm73wgr0zc6lwy91kdn5cp")))

(define-public crate-rolodex-0.1 (crate (name "rolodex") (vers "0.1.0") (deps (list (crate-dep (name "chrono") (req "^0.4.19") (default-features #t) (kind 0)) (crate-dep (name "nom") (req "^6.1.2") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.64") (default-features #t) (kind 2)) (crate-dep (name "typed-builder") (req "^0.9.0") (optional #t) (default-features #t) (kind 0)))) (hash "1ypzajmlryjh2vnynjxv509hxqcb32dhkwbx488gr09vp3q763wg") (features (quote (("serialize" "serde" "chrono/serde") ("default" "serialize" "typed-builder"))))))

(define-public crate-rolodex-0.1 (crate (name "rolodex") (vers "0.1.1") (deps (list (crate-dep (name "chrono") (req "^0.4.19") (default-features #t) (kind 0)) (crate-dep (name "nom") (req "^6.1.2") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.64") (default-features #t) (kind 2)) (crate-dep (name "typed-builder") (req "^0.9.0") (optional #t) (default-features #t) (kind 0)))) (hash "0gbp93zrpf876c7d4hz4icfmdhvaw96v5r0m95capjq5rd7g5xd1") (features (quote (("serialize" "serde" "chrono/serde") ("default" "serialize" "typed-builder"))))))

(define-public crate-rolodex-0.1 (crate (name "rolodex") (vers "0.1.2") (deps (list (crate-dep (name "chrono") (req "^0.4.19") (default-features #t) (kind 0)) (crate-dep (name "nom") (req "^6.1.2") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.64") (default-features #t) (kind 2)) (crate-dep (name "typed-builder") (req "^0.9.0") (optional #t) (default-features #t) (kind 0)))) (hash "1raia5vi3zsmw1fy6nxkz07i25njy8sxghjmd79z7jgiyxi525k3") (features (quote (("serialize" "serde" "chrono/serde") ("default" "serialize" "typed-builder"))))))

