(define-module (crates-io ro tb) #:use-module (crates-io))

(define-public crate-rotbuf-0.0.1 (crate (name "rotbuf") (vers "0.0.1") (deps (list (crate-dep (name "bytes") (req "^1.6.0") (default-features #t) (kind 0)) (crate-dep (name "env_logger") (req "^0.11.3") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.21") (optional #t) (default-features #t) (kind 0)))) (hash "15ympas1vqafqfi22cx3d8366xl3hrc4v8cnd7ycgsy620yn0hgs") (features (quote (("default" "DEBUG_TRACING")))) (v 2) (features2 (quote (("DEBUG_TRACING" "dep:env_logger" "dep:log"))))))

(define-public crate-rotbuf-0.0.2 (crate (name "rotbuf") (vers "0.0.2") (deps (list (crate-dep (name "bytes") (req "^1.6.0") (default-features #t) (kind 0)))) (hash "1z4mnlip8cp6qqd1a4wx4hlkknrgsg9m9j128bzm44i15r4d3x9h") (features (quote (("default" "DEBUG_TRACING") ("DEBUG_TRACING"))))))

(define-public crate-rotbuf-0.0.3 (crate (name "rotbuf") (vers "0.0.3") (deps (list (crate-dep (name "bytes") (req "^1.6.0") (default-features #t) (kind 0)))) (hash "0m5lb4az2n5mc0ixn70wrm51qz4m7hrk1w52r65lyvdr2wijgnbm") (features (quote (("default" "DEBUG_TRACING") ("DEBUG_TRACING"))))))

