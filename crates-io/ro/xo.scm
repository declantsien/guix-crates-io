(define-module (crates-io ro xo) #:use-module (crates-io))

(define-public crate-roxor-0.1 (crate (name "roxor") (vers "0.1.0") (hash "1mzzh55z8j026z26767swjqigwd4bbrikvy5avkgdin6kdf0lsyr")))

(define-public crate-roxor-1 (crate (name "roxor") (vers "1.0.0") (hash "11zxz7gvbxsd4mnqkr0fr38x6n60rr7m8gmw5bmf2za2c1dx7lmd")))

