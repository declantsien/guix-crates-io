(define-module (crates-io ro ze) #:use-module (crates-io))

(define-public crate-roze-0.1 (crate (name "roze") (vers "0.1.0") (hash "0xj79z59y7fr4v7q0qsv3paklb77xdc9n2ylimiqbfmkj92vwg3v") (yanked #t)))

(define-public crate-roze-0.1 (crate (name "roze") (vers "0.1.1") (hash "0s3vkcc8lj7ra5375nygsxasd9ijlhsdjy2ykhygbxy9czv85l0n")))

(define-public crate-rozen-0.1 (crate (name "rozen") (vers "0.1.0") (hash "0233a1hyqrs9gxvfia2lgxcanram38cc5f3ghdn7psa188n7mc9z")))

