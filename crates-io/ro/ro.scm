(define-module (crates-io ro ro) #:use-module (crates-io))

(define-public crate-roro-0.0.0 (crate (name "roro") (vers "0.0.0") (deps (list (crate-dep (name "clap") (req "^2.33.0") (default-features #t) (kind 0)) (crate-dep (name "roro-lib") (req "^0.0") (default-features #t) (kind 0)))) (hash "1cik0jzf06yiy8dbk5prvigsi069mrlrhf6mz1a78jcw7c87p1x1")))

(define-public crate-roro-lib-0.0.0 (crate (name "roro-lib") (vers "0.0.0") (hash "0gcpgzq680dvj3f7nrcqynlp28jz73zkc16n48zdzsvryx87dq7b")))

