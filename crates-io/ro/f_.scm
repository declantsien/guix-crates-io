(define-module (crates-io ro f_) #:use-module (crates-io))

(define-public crate-rof_rs_core-0.1 (crate (name "rof_rs_core") (vers "0.1.0") (hash "0pkgzi6gmsariag27icdawvaqmjcdpzvjs7gpg3myvf2z1qs8q2a") (yanked #t)))

(define-public crate-rof_rs_core-0.1 (crate (name "rof_rs_core") (vers "0.1.1") (hash "1vdkj13fs19sx17py0bany43yrpdi966sfdnns26wahdm08hsyx7") (yanked #t)))

(define-public crate-rof_rs_core-0.1 (crate (name "rof_rs_core") (vers "0.1.2") (hash "1wsd5ns407snz972pi5aiqw863c4cn2y0xd4rndsrdj46df18l6m") (yanked #t)))

(define-public crate-rof_rs_macros-0.1 (crate (name "rof_rs_macros") (vers "0.1.0") (deps (list (crate-dep (name "proc-macro2") (req "^1.0.55") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.26") (default-features #t) (kind 0)) (crate-dep (name "rof_rs_core") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2.0.13") (features (quote ("extra-traits"))) (default-features #t) (kind 0)))) (hash "1vzqcfiwwgva5jacjcaaanzsd8v9wryd2ldx7f264hm7m5jpzxpf")))

(define-public crate-rof_rs_macros-0.1 (crate (name "rof_rs_macros") (vers "0.1.1") (deps (list (crate-dep (name "proc-macro2") (req "^1.0.55") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.26") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2.0.13") (features (quote ("extra-traits"))) (default-features #t) (kind 0)))) (hash "0zx3zwldcfzwa05m7rpshr1ygzvvlsvh0si03h3gwdayamarccwj")))

(define-public crate-rof_rs_macros-0.1 (crate (name "rof_rs_macros") (vers "0.1.2") (deps (list (crate-dep (name "proc-macro2") (req "^1.0.55") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.26") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2.0.13") (features (quote ("extra-traits"))) (default-features #t) (kind 0)))) (hash "0van6fqdfbxiyi2il80s8z24xrhqxrbvlfmpx069ilk9fzx8ij17")))

(define-public crate-rof_rs_macros-0.1 (crate (name "rof_rs_macros") (vers "0.1.3") (deps (list (crate-dep (name "proc-macro2") (req "^1.0.55") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.26") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2.0.13") (features (quote ("extra-traits"))) (default-features #t) (kind 0)))) (hash "0kd87bmzc8xsnk1k20ix7ls9bkidxmw6r0ws4c9hg3p5nma5mphn")))

