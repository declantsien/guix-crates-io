(define-module (crates-io ro sa) #:use-module (crates-io))

(define-public crate-rosa-0.0.1 (crate (name "rosa") (vers "0.0.1") (hash "057syw3zym7cs8hnyz5d84hw8z96dsnzh0n9qczdyjqzfhjwwhv2") (yanked #t)))

(define-public crate-rosa-0.0.5 (crate (name "rosa") (vers "0.0.5") (hash "1xw7y3jkbqlkxmdzw2cvwy90pa8gij8c2lj6jq7c66mm0nkngpg2") (yanked #t)))

(define-public crate-rosa-0.0.7 (crate (name "rosa") (vers "0.0.7") (hash "01n24zy36aymvayh0x10pr7ijxqkl0g3my8scx9ig27dkihd458l") (yanked #t)))

(define-public crate-rosa-0.1 (crate (name "rosa") (vers "0.1.1") (hash "13hn3fcvsvhmq02aw27pn08m9ycrvpqb5b5ykvrrnikjsrscw45a") (yanked #t)))

(define-public crate-rosa-0.1 (crate (name "rosa") (vers "0.1.3") (hash "0w6x2aqqgayd65l4vlixng2969c8cav5as5fn82mrlyja9lh9pag") (yanked #t)))

(define-public crate-rosa-0.1 (crate (name "rosa") (vers "0.1.4") (hash "1angw5ivv64fmhfcir5hpc7h4yli6aj8z7g72hav51sw4bpz10wv") (yanked #t)))

(define-public crate-rosa-0.1 (crate (name "rosa") (vers "0.1.77") (hash "0l6a6fax98s92jys0fy7adcl5z8mp34z0xrc131ybq073g45ks32") (yanked #t)))

(define-public crate-rosa-0.77 (crate (name "rosa") (vers "0.77.0") (hash "16m2jkjc0aq3fzyqb364l96gvp5dlb52kck1na8i9z0f7i01cbk0") (yanked #t)))

(define-public crate-rosa-7 (crate (name "rosa") (vers "7.7.18") (hash "0w8k5z4h9vsfk9gfnnd0zcjgpd4hdmkfd2hddkaxa5x3xpfpsy2z") (yanked #t)))

(define-public crate-rosa-2018 (crate (name "rosa") (vers "2018.7.7") (hash "0w965aw5nyi89si0603lycj21mqyriwsj6yyxmppfgnhl67izp8s") (yanked #t)))

(define-public crate-rosa-2019 (crate (name "rosa") (vers "2019.12.13") (hash "1mwz6ajcdj6rhnclpbpf6xbaffvpaxh9v9kvyq79psf14f35pfz0") (yanked #t)))

(define-public crate-rosa-9999 (crate (name "rosa") (vers "9999.999.99") (hash "0aj4086d2hq5cdj962zn4hllwh69ll9ab19zfnmqd96bsb65phc1") (yanked #t)))

(define-public crate-rosa-9 (crate (name "rosa") (vers "9.9.9") (hash "1gz0wsyhd199kmr0xx6x23km47gkrycw2nbl2hqi1xgp37mzq2yx") (yanked #t)))

(define-public crate-rosa-99999 (crate (name "rosa") (vers "99999.99999.99999") (hash "08sp73m4yrws5iq8pzyx42y23fv8jpqp3zvb1nhydy64qiy39nj6") (yanked #t)))

(define-public crate-rosa-9999999 (crate (name "rosa") (vers "9999999.9999999.9999999") (hash "04dpbhbfr7vfhz1vzf3197jmyiwgrz1q3hlmr5sdbi5wiqwlm464") (yanked #t)))

(define-public crate-rosa-999999999 (crate (name "rosa") (vers "999999999.999999999.999999999") (hash "0gk2fcl626k4vkrahx0gjkivb4az0l0kmgv3val9bdppzk4n3848")))

(define-public crate-rosa_parse-0.1 (crate (name "rosa_parse") (vers "0.1.0") (deps (list (crate-dep (name "anyhow") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "serde_yaml") (req "^0") (default-features #t) (kind 0)) (crate-dep (name "toml") (req "^0") (default-features #t) (kind 0)))) (hash "1cbw51ynfyngc5yr5mjn86c749bp9ghf8i5xhfmvh6hfm5mnmydv")))

(define-public crate-rosa_parse-0.1 (crate (name "rosa_parse") (vers "0.1.1") (deps (list (crate-dep (name "anyhow") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "serde_yaml") (req "^0") (default-features #t) (kind 0)) (crate-dep (name "toml") (req "^0") (default-features #t) (kind 0)))) (hash "1j4ff0kmyp4jvlxcdz8inihwlqm6cnqifvp6bdq0lha9pa3816x3")))

(define-public crate-rosa_parse-0.2 (crate (name "rosa_parse") (vers "0.2.0") (deps (list (crate-dep (name "anyhow") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "serde_yaml") (req "^0") (default-features #t) (kind 0)) (crate-dep (name "toml") (req "^0") (default-features #t) (kind 0)))) (hash "1f9dcq230n6idjd6frziszp86rvp2m64gcsan6k8msgfknw43b6p")))

(define-public crate-rosal-0.1 (crate (name "rosal") (vers "0.1.0") (hash "1lxiqv9kcjv5fanqx0hzdc4gnyj849ixnf38qp94b1gyjg184n8z")))

(define-public crate-rosalind-0.0.1 (crate (name "rosalind") (vers "0.0.1") (hash "0s5piqrn4v30wziczrb7y8nxwlr71q32bv0milsm47pbykgzym6q")))

(define-public crate-rosalind-0.0.2 (crate (name "rosalind") (vers "0.0.2") (hash "115qc56i285zam9qpg06qrg22iidkrlry2hbwa9w2bqzmj69qhxv")))

(define-public crate-rosalind-0.1 (crate (name "rosalind") (vers "0.1.0") (hash "0swmw488xlcppimn66b6fwkv8wcbbr9ii6mbmi6wa4ffx673l255")))

(define-public crate-rosalind-0.2 (crate (name "rosalind") (vers "0.2.0") (hash "0a2w90xpk9wl7s1ns716wr3gs9yqkdacnkj4v50cxg292sjshjx4")))

(define-public crate-rosalind-0.2 (crate (name "rosalind") (vers "0.2.1") (hash "1v53ll2c4cmja5sd9lf6ikfzbml0f7xy6yis425fmmpdcxgb4mz6")))

(define-public crate-rosalind-0.3 (crate (name "rosalind") (vers "0.3.0") (hash "08a9iyhlaq9dmcab7a6v6s752dcjzr3g5dr5rs8yx5y0l0pl5yhs")))

(define-public crate-rosalind-0.3 (crate (name "rosalind") (vers "0.3.1") (hash "0y1jpwm1ihqvq6sy5psm2ifyhv2sycgvlhdxvdwzlvphz850l9ha")))

(define-public crate-rosalind-0.4 (crate (name "rosalind") (vers "0.4.0") (hash "0xj24zcjgz4izdp2hknr96190mrjjp0yfffjx5br61dclshb1rb9")))

(define-public crate-rosalind-0.4 (crate (name "rosalind") (vers "0.4.1") (hash "0l3wpykl021jgd09yxk7bhplhdbgbzvvzyzrgprc3r4yiqlxl263")))

(define-public crate-rosalind-0.5 (crate (name "rosalind") (vers "0.5.0") (hash "0k0kswyiqkbkhaknclzswjm6lpriqwgz0rxcpri8f9hm1riy1dj6")))

(define-public crate-rosalind-0.5 (crate (name "rosalind") (vers "0.5.1") (hash "1v93f3kz6ff8v1j6grdha7jjylalkc72zrj9lfcww11cdr4bw1nb")))

(define-public crate-rosalind-0.6 (crate (name "rosalind") (vers "0.6.0") (hash "1cx7idk2mjsg32525x8j9d5drk9qfhss8c469mzakrk16xmjv80q")))

(define-public crate-rosalind-0.6 (crate (name "rosalind") (vers "0.6.1") (hash "0xgikbckdrban3wign1z4yqv5ygikjb6icv8vxazfjs7avpmdc4c")))

(define-public crate-rosalind-0.7 (crate (name "rosalind") (vers "0.7.0") (deps (list (crate-dep (name "num") (req "^0.1.31") (default-features #t) (kind 0)))) (hash "0nrxk010h5i20y7byx968hcl4q3mjb8y8i6hqk050cf59rwxc6y0")))

(define-public crate-rosalind-0.8 (crate (name "rosalind") (vers "0.8.0") (deps (list (crate-dep (name "num") (req "^0.1.31") (default-features #t) (kind 0)))) (hash "0z4v14l71gzc18wfi3syaznwg4wxp5g4px0isps0ym79nfsscka4")))

(define-public crate-rosalind-0.8 (crate (name "rosalind") (vers "0.8.1") (deps (list (crate-dep (name "num") (req "^0.1.31") (default-features #t) (kind 0)))) (hash "109kzfkk5l7wij5zikaaaglfcx1a4dfba4h6llq7dj21v423v9vc")))

(define-public crate-rosalind-0.9 (crate (name "rosalind") (vers "0.9.0") (deps (list (crate-dep (name "num") (req "^0.1.31") (default-features #t) (kind 0)))) (hash "02v2ijml6jlxcbbs13l44cn086fi5n54xwdg98zkyc5jn1xp01dh")))

(define-public crate-rosalind-0.10 (crate (name "rosalind") (vers "0.10.0") (deps (list (crate-dep (name "num") (req "^0.1.31") (default-features #t) (kind 0)))) (hash "127qrd6hck172bc8k2dmsf1d0vvrqa1vv9cid85z20lkpjkbfb12")))

(define-public crate-rosalind-cli-0.0.1 (crate (name "rosalind-cli") (vers "0.0.1") (deps (list (crate-dep (name "getopts") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "rosalind") (req "^0.0.1") (default-features #t) (kind 0)))) (hash "1i3jv2wkpnlzdfs23xhc49spz3xqb3vinq57xnn9lc7r690w32cf")))

(define-public crate-rosalind-cli-0.0.2 (crate (name "rosalind-cli") (vers "0.0.2") (deps (list (crate-dep (name "getopts") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "rosalind") (req ">= 0.0.1") (default-features #t) (kind 0)))) (hash "0bjcpm0v22vh4snrb2ddzra0c6s915v95bjwfavy44yrhkczv4j4")))

(define-public crate-rosalind-cli-0.0.3 (crate (name "rosalind-cli") (vers "0.0.3") (deps (list (crate-dep (name "getopts") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "rosalind") (req ">= 0.0.1") (default-features #t) (kind 0)))) (hash "01ls20c747kgxa31hgq2bpyighyf6cs6p7l6s61rran5d4rkcwv6")))

(define-public crate-rosalind-cli-0.0.4 (crate (name "rosalind-cli") (vers "0.0.4") (deps (list (crate-dep (name "getopts") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "rosalind") (req ">= 0.0.1") (default-features #t) (kind 0)))) (hash "032frcjxaap3lmblid2c3pjq0m1rchf7x11fqppwiamz0lx7kwqp")))

(define-public crate-rosalind-cli-0.0.5 (crate (name "rosalind-cli") (vers "0.0.5") (deps (list (crate-dep (name "getopts") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "rosalind") (req ">= 0.0.1") (default-features #t) (kind 0)))) (hash "1mz8zmvg5f2g4y646bc9fi6dzcv9ry6icxh51ff0jil5hgh0aj5y")))

(define-public crate-rosalind-cli-0.0.6 (crate (name "rosalind-cli") (vers "0.0.6") (deps (list (crate-dep (name "getopts") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "rosalind") (req ">= 0.0.1") (default-features #t) (kind 0)))) (hash "0lxnmfj3hsdpp4zvmfyvmy104gknbwdw85r1hycdfbrzl890vqwg")))

(define-public crate-rosalind-cli-0.0.7 (crate (name "rosalind-cli") (vers "0.0.7") (deps (list (crate-dep (name "getopts") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "rosalind") (req ">= 0.0.1") (default-features #t) (kind 0)))) (hash "1szzvh7c8nvckxziqgjmk2s36sq1k92mvr49q528kmnk82r3xnhk")))

(define-public crate-rosalind-cli-0.0.8 (crate (name "rosalind-cli") (vers "0.0.8") (deps (list (crate-dep (name "getopts") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "rosalind") (req ">= 0.0.1") (default-features #t) (kind 0)))) (hash "0jbq6j4l05wpa4zdn6xmi9m3cpil6lpkbpycj2n2p6c4sdsbxhmg")))

(define-public crate-rosalind-cli-0.0.9 (crate (name "rosalind-cli") (vers "0.0.9") (deps (list (crate-dep (name "getopts") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "rosalind") (req ">= 0.0.1") (default-features #t) (kind 0)))) (hash "011zcmaqa5av0bqinkxbws5g2c1wjcfjhypl6b6lnn5r9fjxnrwf")))

(define-public crate-rosalind-cli-0.0.10 (crate (name "rosalind-cli") (vers "0.0.10") (deps (list (crate-dep (name "getopts") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "rosalind") (req ">= 0.0.1") (default-features #t) (kind 0)))) (hash "1lirxyyw5gafsfgkmnyg7qy7mdmn3nd77ss0787hyw5x34dj9rxa")))

(define-public crate-rosalind-cli-0.0.11 (crate (name "rosalind-cli") (vers "0.0.11") (deps (list (crate-dep (name "getopts") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "rosalind") (req ">= 0.0.1") (default-features #t) (kind 0)))) (hash "14f3sgwgaml746jca8y2zfmhs7p569n3vrxlcwl1j4c6fpvzw3df")))

(define-public crate-rosalind-cli-0.0.12 (crate (name "rosalind-cli") (vers "0.0.12") (deps (list (crate-dep (name "getopts") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "rosalind") (req ">= 0.0.1") (default-features #t) (kind 0)))) (hash "1jqmr5wp2zc0inkvjyqv4cnj3i5kr1jqczab1l7126475l6zpiqw")))

(define-public crate-rosalind-cli-0.0.13 (crate (name "rosalind-cli") (vers "0.0.13") (deps (list (crate-dep (name "getopts") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "rosalind") (req ">= 0.0.1") (default-features #t) (kind 0)))) (hash "1n5ahzjhjfki9cpvvb4vp3vdrly6nafhc7lzzr8cw280s5imdk1q")))

(define-public crate-rosalind-cli-0.0.14 (crate (name "rosalind-cli") (vers "0.0.14") (deps (list (crate-dep (name "getopts") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "rosalind") (req ">= 0.0.1") (default-features #t) (kind 0)))) (hash "0h2zz7sr72gnbqlmvf6z4fsg75cbis07r9viamnbmfrj2m9bwqvf")))

(define-public crate-rosalind-cli-0.0.15 (crate (name "rosalind-cli") (vers "0.0.15") (deps (list (crate-dep (name "getopts") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "rosalind") (req ">= 0.0.1") (default-features #t) (kind 0)))) (hash "0sdkh7bf5n1342n50qsw8yw6v6wbr5wpxkxxd5prmq9zgqv9xd2v")))

(define-public crate-rosalind_test_reader-0.1 (crate (name "rosalind_test_reader") (vers "0.1.0") (hash "16j053ilmp8x9pn3ppnfkms3zaagxb6jrdggy9jqf50n9cdnbsrl")))

(define-public crate-rosary-0.1 (crate (name "rosary") (vers "0.1.0") (hash "05i40k1j2zlaslbq6w8czn3sbzs22vil2kay9h0l38i45zls67h9")))

(define-public crate-rosary-0.1 (crate (name "rosary") (vers "0.1.1") (deps (list (crate-dep (name "boolinator") (req ">=2.4") (default-features #t) (kind 0)))) (hash "01f1bmz5qg00wv4xb6d2zx32bd7gfp0gpwywflwhlwmkyyjr4a1p")))

(define-public crate-rosary-0.1 (crate (name "rosary") (vers "0.1.2") (deps (list (crate-dep (name "boolinator") (req ">=2.4") (default-features #t) (kind 0)))) (hash "10wx01q1q87p3mvk4nbpq1dgyzaw3cag38yfdcpw1dyjbahg9840")))

(define-public crate-rosary-0.1 (crate (name "rosary") (vers "0.1.3") (deps (list (crate-dep (name "boolinator") (req ">=2.4") (default-features #t) (kind 0)))) (hash "0h1ah9hsgdbbaxbba322f27hr00qp0wr3q3mifd9gy8dxy92svya")))

(define-public crate-rosary-0.1 (crate (name "rosary") (vers "0.1.4") (deps (list (crate-dep (name "boolinator") (req ">=2.4") (default-features #t) (kind 0)))) (hash "13lll3lgpi8ban0iarhbqm6yamrw3iv66h95prr3x1xr32x013g2")))

(define-public crate-rosary-0.1 (crate (name "rosary") (vers "0.1.5") (deps (list (crate-dep (name "boolinator") (req ">=2.4") (default-features #t) (kind 0)))) (hash "10441h5rrvvyrin7qr4hg6a8kq649xg9vzw89avkgff7qpkqn2kh")))

(define-public crate-rosary-0.1 (crate (name "rosary") (vers "0.1.6") (deps (list (crate-dep (name "boolinator") (req ">=2.4") (default-features #t) (kind 0)))) (hash "012is7cqc2xz52rr8s5j3r7xf92009dyyscx4mlbyy25k1md7qls")))

(define-public crate-rosascript-0.1 (crate (name "rosascript") (vers "0.1.0") (deps (list (crate-dep (name "bindgen") (req "^0.69.1") (default-features #t) (kind 0)) (crate-dep (name "bytesize") (req "^1.3.0") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "once_cell") (req "^1.19.0") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.3.26") (default-features #t) (kind 0)))) (hash "0qa75s8i7mg8qd42v5f6ydsnj3ms57b5n7qiyx9443h04cfn0q0v")))

(define-public crate-rosascript-0.1 (crate (name "rosascript") (vers "0.1.1") (deps (list (crate-dep (name "bindgen") (req "^0.69.1") (default-features #t) (kind 0)) (crate-dep (name "bytesize") (req "^1.3.0") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "once_cell") (req "^1.19.0") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.3.26") (default-features #t) (kind 0)))) (hash "0jpd9i85cj8v0z0w7qasjhwg5fgspwyf2s742q7xgfxbr15f8rzp")))

(define-public crate-rosascript-0.1 (crate (name "rosascript") (vers "0.1.2") (deps (list (crate-dep (name "bindgen") (req "^0.69.1") (default-features #t) (kind 0)) (crate-dep (name "bytesize") (req "^1.3.0") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "once_cell") (req "^1.19.0") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.3.26") (default-features #t) (kind 0)))) (hash "0iijvm282567dmxjrz1s19ln79ysrclhywih9s58zqz6gnlm1ksa")))

