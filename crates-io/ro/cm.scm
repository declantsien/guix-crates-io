(define-module (crates-io ro cm) #:use-module (crates-io))

(define-public crate-rocm_lib_sys-0.1 (crate (name "rocm_lib_sys") (vers "0.1.0") (deps (list (crate-dep (name "cmake") (req "^0.1.31") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2.147") (default-features #t) (kind 0)))) (hash "1riklwq9r344x8a7m5kcxm7axjnmmj5pzd38wxaxjblyd7m2j2pi") (features (quote (("vendored")))) (yanked #t)))

(define-public crate-rocm_smi_installer-0.1 (crate (name "rocm_smi_installer") (vers "0.1.0") (deps (list (crate-dep (name "sudo") (req "^0.6.0") (default-features #t) (kind 0)) (crate-dep (name "sys-info") (req "^0.9.1") (default-features #t) (kind 0)))) (hash "0ba22sfbspsmmjd4jcc7dlzn3i7hgyf01hwdkdxda91p39bxxdas")))

(define-public crate-rocm_smi_installer-0.1 (crate (name "rocm_smi_installer") (vers "0.1.1") (deps (list (crate-dep (name "sudo") (req "^0.6.0") (default-features #t) (kind 0)) (crate-dep (name "sys-info") (req "^0.9.1") (default-features #t) (kind 0)))) (hash "1xspz00697bsgqqxcpyd8m9zs1zwhwqggv7l7lx7w4j6ihk1026w")))

(define-public crate-rocm_smi_installer-0.1 (crate (name "rocm_smi_installer") (vers "0.1.2") (deps (list (crate-dep (name "sudo") (req "^0.6.0") (default-features #t) (kind 0)) (crate-dep (name "sys-info") (req "^0.9.1") (default-features #t) (kind 0)))) (hash "0w8lp7v0mzr92xfdwg79w29sf23i5ffyhsf41a8dbnzmd14hzv8d")))

(define-public crate-rocm_smi_lib-0.1 (crate (name "rocm_smi_lib") (vers "0.1.0-beta") (deps (list (crate-dep (name "cmake") (req "^0.1.31") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2.147") (default-features #t) (kind 0)))) (hash "0c11jdwyibdl7wi2rdmcmzilmkychhzddl1bpj3h3vbyf32rjirc") (yanked #t)))

(define-public crate-rocm_smi_lib-0.1 (crate (name "rocm_smi_lib") (vers "0.1.1-beta") (deps (list (crate-dep (name "cmake") (req "^0.1.31") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2.147") (default-features #t) (kind 0)))) (hash "1crggyr3d66n2viq2bjxswg7rx843hqd59rjqqdkvfhdl34jvfhm") (yanked #t)))

(define-public crate-rocm_smi_lib-0.1 (crate (name "rocm_smi_lib") (vers "0.1.2-beta") (deps (list (crate-dep (name "cmake") (req "^0.1.31") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2.147") (default-features #t) (kind 0)))) (hash "1560akmxwp3w1lf2fbsblwbdnalkgs0azbg7js0qd3fab71cx8l3") (yanked #t)))

(define-public crate-rocm_smi_lib-0.1 (crate (name "rocm_smi_lib") (vers "0.1.3-beta") (deps (list (crate-dep (name "cmake") (req "^0.1.31") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2.147") (default-features #t) (kind 0)))) (hash "0r3npcwwxqkwz08m83l217067ls2jsf3w063r1l37k32xwkfd0m5") (yanked #t)))

(define-public crate-rocm_smi_lib-0.1 (crate (name "rocm_smi_lib") (vers "0.1.4-beta") (deps (list (crate-dep (name "cmake") (req "^0.1.31") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2.147") (default-features #t) (kind 0)))) (hash "18cbhh876213a8044jy8ilrscml2b52w4x32jr6db33c4208k45i") (yanked #t)))

(define-public crate-rocm_smi_lib-0.1 (crate (name "rocm_smi_lib") (vers "0.1.5-beta") (deps (list (crate-dep (name "cmake") (req "^0.1.31") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2.147") (default-features #t) (kind 0)))) (hash "0r4q97s9l1n8k4rlfgm4xyf5ilpsxys1s33xsy873irywjgc8qb4") (yanked #t)))

(define-public crate-rocm_smi_lib-0.1 (crate (name "rocm_smi_lib") (vers "0.1.6-beta") (deps (list (crate-dep (name "cmake") (req "^0.1.31") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2.147") (default-features #t) (kind 0)))) (hash "0i8r037is0xaiz08gd9pg0hsywkpr72z1a216i66x3w3zbxwr7q6") (yanked #t)))

(define-public crate-rocm_smi_lib-0.1 (crate (name "rocm_smi_lib") (vers "0.1.7-beta") (deps (list (crate-dep (name "cmake") (req "^0.1.31") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2.147") (default-features #t) (kind 0)))) (hash "0gr7n6djs7g7x8zi9fb436bp2y3vrk6zbqj49m5sq2nvh88vjch2") (yanked #t)))

(define-public crate-rocm_smi_lib-0.1 (crate (name "rocm_smi_lib") (vers "0.1.8-beta") (deps (list (crate-dep (name "cmake") (req "^0.1.31") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2.147") (default-features #t) (kind 0)))) (hash "12552njs8w1v449divgf9z35w772rcl2f3jpiryfpfi42sbpdzpj") (yanked #t)))

(define-public crate-rocm_smi_lib-0.1 (crate (name "rocm_smi_lib") (vers "0.1.8-beta.2") (deps (list (crate-dep (name "cmake") (req "^0.1.31") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2.147") (default-features #t) (kind 0)))) (hash "0h820zgcq5xln6dccw53r0m8n6v5smmzf1m8nyjshg4x9lbhq0fa") (yanked #t)))

(define-public crate-rocm_smi_lib-0.1 (crate (name "rocm_smi_lib") (vers "0.1.8-beta.3") (deps (list (crate-dep (name "cmake") (req "^0.1.31") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2.147") (default-features #t) (kind 0)))) (hash "0ylwjdd7y4dykf0h1kmd9n78c1ph0bp1wxn021lpfg33cpspv2nq") (yanked #t)))

(define-public crate-rocm_smi_lib-0.1 (crate (name "rocm_smi_lib") (vers "0.1.9") (deps (list (crate-dep (name "cmake") (req "^0.1.31") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2.147") (default-features #t) (kind 0)))) (hash "1jzd07pakfbb11v38ksjajdwwd1wi8v5hysbqs9rcyvbg424pvbq") (yanked #t)))

(define-public crate-rocm_smi_lib-0.1 (crate (name "rocm_smi_lib") (vers "0.1.10") (deps (list (crate-dep (name "cmake") (req "^0.1.31") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2.147") (default-features #t) (kind 0)))) (hash "1yxbj8rjm8k9p24di1ln84zmmbhb8w1ds0k7mcm36hkscwjw88ka") (features (quote (("vendored")))) (yanked #t)))

(define-public crate-rocm_smi_lib-0.1 (crate (name "rocm_smi_lib") (vers "0.1.11") (deps (list (crate-dep (name "cmake") (req "^0.1.31") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2.147") (default-features #t) (kind 0)))) (hash "0kpc4zkxfbrvkwwi1rnqbahgll1974n0bihlj7l8vhr3kqillk3c") (features (quote (("vendored")))) (yanked #t)))

(define-public crate-rocm_smi_lib-0.1 (crate (name "rocm_smi_lib") (vers "0.1.12") (deps (list (crate-dep (name "cmake") (req "^0.1.31") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2.147") (default-features #t) (kind 0)))) (hash "17g60bixyl63lrx3z7a0k6l2m13k4c6rixjsx4i08jqy8006g37l") (features (quote (("vendored")))) (yanked #t)))

(define-public crate-rocm_smi_lib-0.1 (crate (name "rocm_smi_lib") (vers "0.1.13") (deps (list (crate-dep (name "libc") (req "^0.2.147") (default-features #t) (kind 0)) (crate-dep (name "rocm_smi_lib_sys") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "00l306drw4ih8sqq9midzaxrkvzjjrsrvpjnp6l54y8k23rmk0g0") (features (quote (("vendored" "rocm_smi_lib_sys/vendored")))) (yanked #t)))

(define-public crate-rocm_smi_lib-0.1 (crate (name "rocm_smi_lib") (vers "0.1.14-rc.1") (deps (list (crate-dep (name "libc") (req "^0.2.147") (default-features #t) (kind 0)) (crate-dep (name "rocm_smi_lib_sys") (req "^0.1.2") (default-features #t) (kind 0)))) (hash "1xf06512p5vb1kncxb69jg66ffh6djvq5040j658ki4qrgbabzb3") (features (quote (("vendored" "rocm_smi_lib_sys/vendored")))) (yanked #t)))

(define-public crate-rocm_smi_lib-0.1 (crate (name "rocm_smi_lib") (vers "0.1.14") (deps (list (crate-dep (name "libc") (req "^0.2.147") (default-features #t) (kind 0)) (crate-dep (name "rocm_smi_lib_sys") (req "^0.1.3") (default-features #t) (kind 0)))) (hash "1fv5jhfdx1hg693hjm0y9ymkbsq4kz3j9jg5zjgdfcj3gc9aicxa") (features (quote (("vendored" "rocm_smi_lib_sys/vendored")))) (yanked #t)))

(define-public crate-rocm_smi_lib-0.1 (crate (name "rocm_smi_lib") (vers "0.1.15") (deps (list (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2.147") (default-features #t) (kind 0)) (crate-dep (name "rocm_smi_lib_sys") (req "^0.1.4") (default-features #t) (kind 0)))) (hash "15szvclwx4ypwsm4nc43w5lcg2hxp0dffykj322r08lv0kk8awzj") (features (quote (("vendored" "rocm_smi_lib_sys/vendored")))) (yanked #t)))

(define-public crate-rocm_smi_lib-0.1 (crate (name "rocm_smi_lib") (vers "0.1.16") (deps (list (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2.147") (default-features #t) (kind 0)) (crate-dep (name "rocm_smi_lib_sys") (req "^0.1.4") (default-features #t) (kind 0)))) (hash "0nys57qdr9dxwrnjcwh79f8rcksjn4h9jrcnxrj3j5kk1vmcva9a") (features (quote (("vendored" "rocm_smi_lib_sys/vendored") ("fn_query") ("device")))) (yanked #t)))

(define-public crate-rocm_smi_lib-0.1 (crate (name "rocm_smi_lib") (vers "0.1.17") (deps (list (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2.147") (default-features #t) (kind 0)) (crate-dep (name "rocm_smi_lib_sys") (req "^0.1.4") (default-features #t) (kind 0)))) (hash "0p8apgc7wvhbpcxihag98l1si0grv93zz2x7rwncs3f9p6mwl8b4") (features (quote (("vendored" "rocm_smi_lib_sys/vendored") ("fn_query") ("device"))))))

(define-public crate-rocm_smi_lib-0.2 (crate (name "rocm_smi_lib") (vers "0.2.0-rc1") (deps (list (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2.147") (default-features #t) (kind 0)) (crate-dep (name "rocm_smi_lib_sys") (req "^0.2.1") (default-features #t) (kind 0)))) (hash "1d8jn50sky9s7d44ys2pm8nlh3sxxz1cic5vchz5ya4g9fr4rvn0") (features (quote (("fn_query") ("device")))) (yanked #t)))

(define-public crate-rocm_smi_lib-0.2 (crate (name "rocm_smi_lib") (vers "0.2.0") (deps (list (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2.147") (default-features #t) (kind 0)) (crate-dep (name "rocm_smi_lib_sys") (req "^0.2.1") (default-features #t) (kind 0)))) (hash "1107sqzl68349xhphw12ymn83z7ii0dq2n8ywcq5hla5f3qmrv7l") (features (quote (("fn_query") ("device"))))))

(define-public crate-rocm_smi_lib-0.2 (crate (name "rocm_smi_lib") (vers "0.2.2") (deps (list (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2.147") (default-features #t) (kind 0)) (crate-dep (name "rocm_smi_lib_sys") (req "^0.2.2") (default-features #t) (kind 0)))) (hash "0z999mb5l4yvdrdk6bnvhj8wqk5l0ld90ag5cphjmj2hfahjgm7n") (features (quote (("fn_query") ("device"))))))

(define-public crate-rocm_smi_lib_sys-0.1 (crate (name "rocm_smi_lib_sys") (vers "0.1.0") (deps (list (crate-dep (name "cmake") (req "^0.1.31") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2.147") (default-features #t) (kind 0)))) (hash "1q4wwvf4lj5m18bd3c2b3427avbgm3ikhkcwrckjhsphkqsisj12") (features (quote (("vendored")))) (yanked #t)))

(define-public crate-rocm_smi_lib_sys-0.1 (crate (name "rocm_smi_lib_sys") (vers "0.1.1") (deps (list (crate-dep (name "cmake") (req "^0.1.31") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2.147") (default-features #t) (kind 0)))) (hash "1220s2685jb56y38ckcd6gc0pjnvmbcnj8r7v6n1yylsgc2n93bd") (features (quote (("vendored")))) (yanked #t)))

(define-public crate-rocm_smi_lib_sys-0.1 (crate (name "rocm_smi_lib_sys") (vers "0.1.2") (deps (list (crate-dep (name "cmake") (req "^0.1.31") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2.147") (default-features #t) (kind 0)))) (hash "1ba786yzs73iymhfxsvc368k5wsxfi5sq23y2c0jwqm4lc94x5cd") (features (quote (("vendored")))) (yanked #t)))

(define-public crate-rocm_smi_lib_sys-0.1 (crate (name "rocm_smi_lib_sys") (vers "0.1.3") (deps (list (crate-dep (name "cmake") (req "^0.1.31") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2.147") (default-features #t) (kind 0)))) (hash "0yb4blmhph0ibac3arwlgyx0amjywg6ybaic6h1x6mjfyl9j7hwg") (features (quote (("vendored")))) (yanked #t)))

(define-public crate-rocm_smi_lib_sys-0.1 (crate (name "rocm_smi_lib_sys") (vers "0.1.4") (deps (list (crate-dep (name "cmake") (req "^0.1.31") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2.147") (default-features #t) (kind 0)))) (hash "0jnwc2dq38fpkdq5xvg7ahdyja82lgj0q4pdiczbrfwg8ziawvr7") (features (quote (("vendored")))) (yanked #t)))

(define-public crate-rocm_smi_lib_sys-0.1 (crate (name "rocm_smi_lib_sys") (vers "0.1.5") (deps (list (crate-dep (name "cmake") (req "^0.1.31") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2.147") (default-features #t) (kind 0)))) (hash "1fqnlziij844s31f1lynz3mxr4pxb9h5mkw5f8z65nf0ghf0fmlj") (features (quote (("vendored"))))))

(define-public crate-rocm_smi_lib_sys-0.2 (crate (name "rocm_smi_lib_sys") (vers "0.2.0-rc1") (deps (list (crate-dep (name "cmake") (req "^0.1.31") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2.147") (default-features #t) (kind 0)) (crate-dep (name "libloading") (req "^0.8.1") (default-features #t) (kind 0)))) (hash "14wnjc8gcv561rfppg1vc036950pqnbx53r95ix8kcjxgy6mzr9f") (yanked #t)))

(define-public crate-rocm_smi_lib_sys-0.2 (crate (name "rocm_smi_lib_sys") (vers "0.2.0-rc2") (deps (list (crate-dep (name "cmake") (req "^0.1.31") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2.147") (default-features #t) (kind 0)) (crate-dep (name "libloading") (req "^0.8.1") (default-features #t) (kind 0)))) (hash "0h9593w0phijsqj8gqy60ymrn1fsqqp2zlam2s2mjhbcvs1dvi3p") (yanked #t)))

(define-public crate-rocm_smi_lib_sys-0.2 (crate (name "rocm_smi_lib_sys") (vers "0.2.0") (deps (list (crate-dep (name "cmake") (req "^0.1.31") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2.147") (default-features #t) (kind 0)) (crate-dep (name "libloading") (req "^0.8.1") (default-features #t) (kind 0)))) (hash "1ba7p5811c19csw028302s5dpm5z82iy608r31y5x7wd54cdibjh") (yanked #t)))

(define-public crate-rocm_smi_lib_sys-0.2 (crate (name "rocm_smi_lib_sys") (vers "0.2.1") (deps (list (crate-dep (name "cmake") (req "^0.1.31") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2.147") (default-features #t) (kind 0)) (crate-dep (name "libloading") (req "^0.8.1") (default-features #t) (kind 0)))) (hash "1cnpib9sfsnq5w42nr53bbnrq2mcnv4c9b5ipq15lh145xr3vmfz")))

(define-public crate-rocm_smi_lib_sys-0.2 (crate (name "rocm_smi_lib_sys") (vers "0.2.2-rc1") (deps (list (crate-dep (name "cmake") (req "^0.1.31") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2.147") (default-features #t) (kind 0)) (crate-dep (name "libloading") (req "^0.8.1") (default-features #t) (kind 0)))) (hash "1cvhsx1sq5zqmhrq64fdpq7c7c773ywfqqh536pz2ccn2fq98cl3")))

(define-public crate-rocm_smi_lib_sys-0.2 (crate (name "rocm_smi_lib_sys") (vers "0.2.2-rc2") (deps (list (crate-dep (name "cmake") (req "^0.1.31") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2.147") (default-features #t) (kind 0)) (crate-dep (name "libloading") (req "^0.8.1") (default-features #t) (kind 0)))) (hash "1w40clqikkba7bzb1z57x42xb0qbb1a8gmvkxiy8m4rkzgiikw8w")))

(define-public crate-rocm_smi_lib_sys-0.2 (crate (name "rocm_smi_lib_sys") (vers "0.2.2") (deps (list (crate-dep (name "cmake") (req "^0.1.31") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2.147") (default-features #t) (kind 0)) (crate-dep (name "libloading") (req "^0.8.1") (default-features #t) (kind 0)))) (hash "1vl66yxxd5dcpc5a43qsrj6dlh96ggvvnm6ni6p15hln6i13sv6h")))

(define-public crate-rocm_smi_lib_sys-0.2 (crate (name "rocm_smi_lib_sys") (vers "0.2.2-patch1") (deps (list (crate-dep (name "cmake") (req "^0.1.31") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2.147") (default-features #t) (kind 0)) (crate-dep (name "libloading") (req "^0.8.1") (default-features #t) (kind 0)))) (hash "0lxyvj46fd2jlmyik3kl98zj6zkczrl7q3s4rpk07vshb547bhyn")))

