(define-module (crates-io r_ fe) #:use-module (crates-io))

(define-public crate-r_fetcher-0.1 (crate (name "r_fetcher") (vers "0.1.0") (deps (list (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)))) (hash "0x81rfdwfhgxx05xzjnf1qqfx3b7jwgcrzvsgz8gq0nwa1ygadw2")))

