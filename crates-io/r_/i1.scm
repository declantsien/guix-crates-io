(define-module (crates-io r_ i1) #:use-module (crates-io))

(define-public crate-r_i18n-0.1 (crate (name "r_i18n") (vers "0.1.0") (deps (list (crate-dep (name "json") (req "^0.11.13") (default-features #t) (kind 0)))) (hash "0nc637hdgfip33ijcs4sifl0g899m40w0cqrypdb01hcn9pbyyqx")))

(define-public crate-r_i18n-1 (crate (name "r_i18n") (vers "1.0.0") (deps (list (crate-dep (name "json") (req "^0.11.13") (default-features #t) (kind 0)))) (hash "1a1sibxadp0hizs3lrjlx11q3dk2rq798zzx5vh0bndgws7bky2f")))

(define-public crate-r_i18n-1 (crate (name "r_i18n") (vers "1.0.1") (deps (list (crate-dep (name "json") (req "^0.11.13") (default-features #t) (kind 0)))) (hash "1ijvpbipnckxyj71932d4ncyfrqszsz6jgb5ar2nf3c9lgs4da44")))

