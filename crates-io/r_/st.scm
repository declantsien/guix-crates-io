(define-module (crates-io r_ st) #:use-module (crates-io))

(define-public crate-r_stats-0.1 (crate (name "r_stats") (vers "0.1.0") (deps (list (crate-dep (name "bindgen") (req "^0.54.1") (default-features #t) (kind 1)) (crate-dep (name "cc") (req "^1.0.54") (default-features #t) (kind 1)))) (hash "1lbpp9c5imy68c0sp108qlkgaz1nyr0k0jc769i81a6mx8nv9jgs")))

