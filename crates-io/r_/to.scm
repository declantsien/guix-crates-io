(define-module (crates-io r_ to) #:use-module (crates-io))

(define-public crate-r_tools-0.1 (crate (name "r_tools") (vers "0.1.0") (hash "1bxwp2p1j51wsa8fbfh4fjvdc2a97ak0k8yyh2ni2gksjv4d6wh3") (yanked #t)))

(define-public crate-r_tools-0.1 (crate (name "r_tools") (vers "0.1.1") (hash "08ba8z2gf2qn83g6hkjca4v1b3q94gm0aci8cmq4b4hg8p56ia34") (yanked #t)))

(define-public crate-r_tools-0.1 (crate (name "r_tools") (vers "0.1.2") (hash "0rxmcsncsbrw3swq893zv3hmm7bgkz81fjna10qyvj49g25iw5ah")))

