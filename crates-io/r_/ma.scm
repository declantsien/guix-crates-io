(define-module (crates-io r_ ma) #:use-module (crates-io))

(define-public crate-r_mathlib-0.1 (crate (name "r_mathlib") (vers "0.1.0") (deps (list (crate-dep (name "bindgen") (req "^0.55.1") (default-features #t) (kind 1)) (crate-dep (name "cc") (req "^1.0.54") (default-features #t) (kind 1)))) (hash "1i1h04pd5h02zw6p67qx12k51ll2bbymdhb5r4ihxccvk84cgmjc")))

(define-public crate-r_mathlib-0.1 (crate (name "r_mathlib") (vers "0.1.1") (deps (list (crate-dep (name "bindgen") (req "^0.55.1") (default-features #t) (kind 1)) (crate-dep (name "cc") (req "^1.0.54") (default-features #t) (kind 1)))) (hash "0qdxzwzk0kcibfvnx7fypr73d1c3h4q9awhfckh60ipbsrn1zmb0")))

(define-public crate-r_mathlib-0.2 (crate (name "r_mathlib") (vers "0.2.0") (deps (list (crate-dep (name "bindgen") (req "^0.66.1") (default-features #t) (kind 1)) (crate-dep (name "cc") (req "^1.0.79") (default-features #t) (kind 1)))) (hash "0mfw6zjfawdhkpzm26kgiv7bgfgiqk8bmqrihznckwrrg3lc5s52")))

