(define-module (crates-io r_ la) #:use-module (crates-io))

(define-public crate-r_lamp-0.2 (crate (name "r_lamp") (vers "0.2.1") (deps (list (crate-dep (name "clap") (req "^3.2.12") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^3.2.12") (default-features #t) (kind 1)) (crate-dep (name "clap_complete") (req "^3.2.3") (default-features #t) (kind 1)) (crate-dep (name "exitcode") (req "^1.1.2") (default-features #t) (kind 0)))) (hash "0h3hvamq7qirc5vn2cygjzwk8my3yzkix7lsadyd9lg1z97zv4qc") (yanked #t)))

