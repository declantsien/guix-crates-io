(define-module (crates-io r_ co) #:use-module (crates-io))

(define-public crate-r_core-0.1 (crate (name "r_core") (vers "0.1.0") (deps (list (crate-dep (name "bindgen") (req "^0.53.1") (default-features #t) (kind 1)) (crate-dep (name "pkg-config") (req "^0.3.16") (default-features #t) (kind 1)))) (hash "1y06xvdz3pcfylsfrd7cqyrlvsqzr8ys97iy9lbh3d8nlr968csw") (links "r_core")))

