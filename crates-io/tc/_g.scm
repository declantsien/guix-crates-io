(define-module (crates-io tc _g) #:use-module (crates-io))

(define-public crate-tc_generic-0.1 (crate (name "tc_generic") (vers "0.1.0") (deps (list (crate-dep (name "tc_core") (req "^0.2") (default-features #t) (kind 0)))) (hash "0fpfyjz05rhvdg1ghzv6bd1x1vqc7gp0l2hqavkmk2flffx4pdn3")))

(define-public crate-tc_generic-0.2 (crate (name "tc_generic") (vers "0.2.0") (deps (list (crate-dep (name "tc_core") (req "^0.3") (default-features #t) (kind 0)))) (hash "039pnhimvniwryz27isazgc7s4jf4hr9g7jss95q1nfyy7j8rh3j")))

(define-public crate-tc_generic-0.2 (crate (name "tc_generic") (vers "0.2.1") (deps (list (crate-dep (name "tc_core") (req "^0.3") (default-features #t) (kind 0)))) (hash "0vcz9kk7ymc4wwbaa7fj832f1204asns47k7bcc7m0f93svww6p1")))

