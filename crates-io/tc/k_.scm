(define-module (crates-io tc k_) #:use-module (crates-io))

(define-public crate-tck_no-1 (crate (name "tck_no") (vers "1.0.0") (deps (list (crate-dep (name "rand") (req "^0.8.4") (default-features #t) (kind 0)))) (hash "0xbl4s50ifpfcx0mijg4hqcfnf3006jn7nchq3m9fnjl2fngbz09")))

(define-public crate-tck_no-1 (crate (name "tck_no") (vers "1.0.1") (deps (list (crate-dep (name "rand") (req "^0.8.4") (default-features #t) (kind 0)))) (hash "1k71nywik8w067zjlgrg56v1qibws0yd8pwxf8m0flknk9p3pjwm")))

(define-public crate-tck_no-1 (crate (name "tck_no") (vers "1.0.2") (deps (list (crate-dep (name "rand") (req "^0.8.4") (default-features #t) (kind 0)))) (hash "14dz2b627vik9fw49p4dk1q9ls9j19pqvjkqqdj55567n15s1i2a")))

