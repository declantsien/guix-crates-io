(define-module (crates-io tc fs) #:use-module (crates-io))

(define-public crate-TCFSMFTTTCS-0.1 (crate (name "TCFSMFTTTCS") (vers "0.1.0") (deps (list (crate-dep (name "num-traits") (req "^0.2.11") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1.0.10") (default-features #t) (kind 0)))) (hash "1n6gfy9zqqxf8fwwha5rc3gq6v1b3695nvcgzlm3gql8flxff1g9")))

(define-public crate-TCFSMFTTTCS-0.1 (crate (name "TCFSMFTTTCS") (vers "0.1.1") (deps (list (crate-dep (name "num-traits") (req "^0.2.11") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1.0.10") (default-features #t) (kind 0)))) (hash "0bvhnsyf2bskl6qd1pcv2bjmcsc6kh0cgwy9lrcg57s10vd2646k")))

(define-public crate-TCFSMFTTTCS-0.1 (crate (name "TCFSMFTTTCS") (vers "0.1.2") (deps (list (crate-dep (name "num-traits") (req "^0.2.11") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1.0.10") (default-features #t) (kind 0)))) (hash "1agyp7yijv30a2868m49967m70f6cj932p1gjn5qhddcnq9ad252")))

(define-public crate-TCFSMFTTTCS-0.1 (crate (name "TCFSMFTTTCS") (vers "0.1.3") (deps (list (crate-dep (name "num-traits") (req "^0.2.11") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1.0.10") (default-features #t) (kind 0)))) (hash "1zwdpbgsgkc5i7b90kg0vgk1i5ngyi14m5wx6lpz4qxi7dg2x98d")))

