(define-module (crates-io tc _d) #:use-module (crates-io))

(define-public crate-tc_dynamodb_local-0.1 (crate (name "tc_dynamodb_local") (vers "0.1.0") (deps (list (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "tc_core") (req "^0.2") (default-features #t) (kind 0)))) (hash "1hb5pk5ryqbvq8ij57qqzdf0mi984nsdnx2zwnd71aj30cfsy5f8")))

(define-public crate-tc_dynamodb_local-0.2 (crate (name "tc_dynamodb_local") (vers "0.2.0") (deps (list (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "tc_core") (req "^0.3") (default-features #t) (kind 0)))) (hash "0vl6qn9pa58y0pnjgnbfb2njizsziakcwjzcb1fkgngrfq9x5yql")))

(define-public crate-tc_dynamodb_local-0.2 (crate (name "tc_dynamodb_local") (vers "0.2.1") (deps (list (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "tc_core") (req "^0.3") (default-features #t) (kind 0)))) (hash "1zx9f83c5q6zbnr24bsly9w7d766538c9g7l3007hjmwqykqqxvg")))

