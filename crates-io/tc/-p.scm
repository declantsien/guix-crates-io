(define-module (crates-io tc -p) #:use-module (crates-io))

(define-public crate-tc-peerset-0.0.0 (crate (name "tc-peerset") (vers "0.0.0") (hash "0kj3w0h9mj8l0xzy09x1inqh7fbdqnqpf9b1iaczl3q9ig5mi4ba") (yanked #t)))

(define-public crate-tc-peerset-2 (crate (name "tc-peerset") (vers "2.0.0") (deps (list (crate-dep (name "futures") (req "^0.3.9") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.8") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.7.2") (default-features #t) (kind 2)) (crate-dep (name "serde_json") (req "^1.0.41") (default-features #t) (kind 0)) (crate-dep (name "tetcore-utils") (req "^2.0.2") (default-features #t) (kind 0)) (crate-dep (name "tetsy-libp2p") (req "^0.34.2") (kind 0)) (crate-dep (name "wasm-timer") (req "^0.2") (default-features #t) (kind 0)))) (hash "0cmrrr5s4cvkjhjyw036mr0z1dmyvv9l96md7xcfrjkrsmc9pkwq")))

(define-public crate-tc-proposer-metrics-0.0.0 (crate (name "tc-proposer-metrics") (vers "0.0.0") (hash "0qcqvfblzzhxwbnm1sw6hm2z7b0a89713njzbmnykj0drjdcbbv8") (yanked #t)))

(define-public crate-tc-proposer-metrics-0.8 (crate (name "tc-proposer-metrics") (vers "0.8.0") (deps (list (crate-dep (name "log") (req "^0.4.8") (default-features #t) (kind 0)) (crate-dep (name "prometheus-endpoint") (req "^0.8.1") (default-features #t) (kind 0) (package "prometheus-endpoint")))) (hash "1n7b9k422sq97q71nlxhjw4kf8w6mc43s6rpkich134f1arqc0vz")))

