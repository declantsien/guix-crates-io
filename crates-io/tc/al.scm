(define-module (crates-io tc al) #:use-module (crates-io))

(define-public crate-tcalc-1 (crate (name "tcalc") (vers "1.0.1") (deps (list (crate-dep (name "getopts") (req "^0.2.14") (default-features #t) (kind 0)))) (hash "1fq3sqfda31inpwcd8l3rhpmdd3i0x7bd8w3bqqf4pmnjkd7ivjk")))

(define-public crate-tcalc-1 (crate (name "tcalc") (vers "1.0.2") (deps (list (crate-dep (name "getopts") (req "^0.2.14") (default-features #t) (kind 0)))) (hash "1hj3dw6rj84dx05v8iiqvw0hq2yjsxp89l7k3nh2365vxylhix0y")))

(define-public crate-tcalc-1 (crate (name "tcalc") (vers "1.0.3") (deps (list (crate-dep (name "getopts") (req "^0.2.14") (default-features #t) (kind 0)))) (hash "0n3ngii4wv6fwl7n3p415avrkr7j6n65xh6ysgn2cpyds8jdmyd3") (features (quote (("trace") ("default"))))))

(define-public crate-tcalc-1 (crate (name "tcalc") (vers "1.1.4") (deps (list (crate-dep (name "getopts") (req "^0.2.14") (default-features #t) (kind 0)))) (hash "1pkc380gxdmnyf4lf7cdwxnjm5cw92ll6vg7cigx46a0adnwg87d") (features (quote (("trace"))))))

(define-public crate-tcalc-1 (crate (name "tcalc") (vers "1.2.5") (deps (list (crate-dep (name "getopts") (req "^0.2.14") (default-features #t) (kind 0)))) (hash "1n3xgck44jpqgpg7vmn4yrz16a4bsdr4dz1c5rdqaladd2kq6p8n") (features (quote (("trace"))))))

(define-public crate-tcalc-1 (crate (name "tcalc") (vers "1.3.6") (deps (list (crate-dep (name "getopts") (req "^0.2.14") (default-features #t) (kind 0)))) (hash "0b0pdig3vd4l3z4p6qsy18hzvk672qy9ikkwf3dq90inhgqrj8yr") (features (quote (("trace"))))))

(define-public crate-tcalc-1 (crate (name "tcalc") (vers "1.4.7") (deps (list (crate-dep (name "getopts") (req "^0.2.14") (default-features #t) (kind 0)))) (hash "104jn6z9rfn1g66jnzcww3kl63kdk4ai5l1cpf66ji44rhdkb5gj") (features (quote (("trace"))))))

(define-public crate-tcalc-1 (crate (name "tcalc") (vers "1.4.8") (deps (list (crate-dep (name "getopts") (req "^0.2.14") (default-features #t) (kind 0)))) (hash "11dbakasdvm695qh98l7swi0faani5db1pmf7nh6v7smf4xzaj41") (features (quote (("trace"))))))

(define-public crate-tcalc-1 (crate (name "tcalc") (vers "1.4.9") (deps (list (crate-dep (name "getopts") (req "^0.2.14") (default-features #t) (kind 0)))) (hash "0i3a6ni06l6v3qqvzm5v3n5qfmi6k8ann3inpw3zdjy2z5jg1qw4") (features (quote (("trace"))))))

(define-public crate-tcalc-1 (crate (name "tcalc") (vers "1.4.10") (deps (list (crate-dep (name "getopts") (req "^0.2.14") (default-features #t) (kind 0)))) (hash "06z7qnniiy7i933jis8asfxyw6gr4lhfr4qsjyxnzf5xp175iar5") (features (quote (("trace"))))))

(define-public crate-tcalc-1 (crate (name "tcalc") (vers "1.5.11") (deps (list (crate-dep (name "getopts") (req "^0.2.14") (default-features #t) (kind 0)) (crate-dep (name "rustyline") (req "^1.0.0") (default-features #t) (kind 0)))) (hash "1674zcdj42x0l93x9ly2zshvgvg7m9y9pp85kdzdd1nr8mcgrv9r") (features (quote (("trace"))))))

(define-public crate-tcalc-1 (crate (name "tcalc") (vers "1.6.12") (deps (list (crate-dep (name "tcalc-rustyline") (req "^1.0.0") (default-features #t) (kind 0)))) (hash "18hx9yjz43xf2nkmjb2zhj203z2wc00z6xr1f48jnmljcy0qjqnp") (features (quote (("trace"))))))

(define-public crate-tcalc-1 (crate (name "tcalc") (vers "1.6.13") (deps (list (crate-dep (name "tcalc-rustyline") (req "^1.0.1") (default-features #t) (kind 0)))) (hash "085q0sn814yw2rymd7xvfszxdr6pz6ba5x57szksb14xh0qbf325") (features (quote (("trace"))))))

(define-public crate-tcalc-1 (crate (name "tcalc") (vers "1.6.14") (deps (list (crate-dep (name "dirs") (req "^2.0.1") (default-features #t) (kind 0)) (crate-dep (name "tcalc-rustyline") (req "^1.0.2") (default-features #t) (kind 0)))) (hash "1zizg1z1sgsp5jlw965kk128wkkhj2bzhcwqrr3p8fvs0nysq19b") (features (quote (("trace"))))))

(define-public crate-tcalc-1 (crate (name "tcalc") (vers "1.7.15") (deps (list (crate-dep (name "dirs") (req "^2.0.2") (default-features #t) (kind 0)) (crate-dep (name "tcalc-rustyline") (req "^1.0.2") (default-features #t) (kind 0)))) (hash "1vqxvarbzl55d43grfgx6jpn5jlwv504y8jrs4kxnnha5d9w12a3") (features (quote (("trace"))))))

(define-public crate-tcalc-1 (crate (name "tcalc") (vers "1.7.16") (deps (list (crate-dep (name "dirs") (req "^2.0.2") (default-features #t) (kind 0)) (crate-dep (name "tcalc-rustyline") (req "^1.0.2") (default-features #t) (kind 0)))) (hash "0wcwhqxax3m363qm85471j3f6myqbba0sjpd6zli9rsx1wxxkyb0") (features (quote (("trace"))))))

(define-public crate-tcalc-1 (crate (name "tcalc") (vers "1.8.0") (deps (list (crate-dep (name "dirs") (req "4.0.*") (default-features #t) (kind 0)) (crate-dep (name "tcalc-rustyline") (req "^1.0.2") (default-features #t) (kind 0)))) (hash "16y45rq877ryxw5anmc1f218dcj814php6qxpmmp53c5y0xmrlbb") (features (quote (("trace"))))))

(define-public crate-tcalc-1 (crate (name "tcalc") (vers "1.8.1") (deps (list (crate-dep (name "dirs") (req "4.0.*") (default-features #t) (kind 0)) (crate-dep (name "tcalc-rustyline") (req "^1.0.2") (default-features #t) (kind 0)))) (hash "0p92xw5bcmcck9d35l7pdqiaxlbzaky2czwd24gzmaz1yl1jibvv") (features (quote (("trace"))))))

(define-public crate-tcalc-rustyline-1 (crate (name "tcalc-rustyline") (vers "1.0.0") (deps (list (crate-dep (name "encode_unicode") (req "^0.1.3") (default-features #t) (kind 0)) (crate-dep (name "kernel32-sys") (req "^0.2") (default-features #t) (target "cfg(windows)") (kind 0)) (crate-dep (name "libc") (req "^0.2.7") (default-features #t) (kind 0)) (crate-dep (name "nix") (req "^0.7") (default-features #t) (target "cfg(unix)") (kind 0)) (crate-dep (name "tempdir") (req "^0.3.4") (default-features #t) (kind 2)) (crate-dep (name "unicode-width") (req "^0.1.3") (default-features #t) (kind 0)) (crate-dep (name "winapi") (req "^0.2") (default-features #t) (target "cfg(windows)") (kind 0)))) (hash "1la27m4zpdq183m0n8ib7aacljf98rs99ckzmx4zs45dx7h5qnpc")))

(define-public crate-tcalc-rustyline-1 (crate (name "tcalc-rustyline") (vers "1.0.1") (deps (list (crate-dep (name "encode_unicode") (req "^0.1.3") (default-features #t) (kind 0)) (crate-dep (name "kernel32-sys") (req "^0.2") (default-features #t) (target "cfg(windows)") (kind 0)) (crate-dep (name "libc") (req "^0.2.7") (default-features #t) (kind 0)) (crate-dep (name "nix") (req "^0.7") (default-features #t) (target "cfg(unix)") (kind 0)) (crate-dep (name "tempdir") (req "^0.3.4") (default-features #t) (kind 2)) (crate-dep (name "unicode-width") (req "^0.1.3") (default-features #t) (kind 0)) (crate-dep (name "winapi") (req "^0.2") (default-features #t) (target "cfg(windows)") (kind 0)))) (hash "193c2s1p4gpaa6d05xc2w9xrpbygk2psbmpi3p11w0lr1ja8p0nc")))

(define-public crate-tcalc-rustyline-1 (crate (name "tcalc-rustyline") (vers "1.0.2") (deps (list (crate-dep (name "dirs") (req "^2.0.1") (default-features #t) (kind 0)) (crate-dep (name "encode_unicode") (req "^0.1.3") (default-features #t) (kind 0)) (crate-dep (name "kernel32-sys") (req "^0.2") (default-features #t) (target "cfg(windows)") (kind 0)) (crate-dep (name "libc") (req "^0.2.7") (default-features #t) (kind 0)) (crate-dep (name "nix") (req "^0.7") (default-features #t) (target "cfg(unix)") (kind 0)) (crate-dep (name "tempdir") (req "^0.3.4") (default-features #t) (kind 2)) (crate-dep (name "unicode-width") (req "^0.1.3") (default-features #t) (kind 0)) (crate-dep (name "winapi") (req "^0.2") (default-features #t) (target "cfg(windows)") (kind 0)))) (hash "0gdgv0795mii538j76p7kfkyqmj3yszd9hz81nbg849kbki91ipk")))

