(define-module (crates-io tc pl) #:use-module (crates-io))

(define-public crate-tcplinecombine-0.1 (crate (name "tcplinecombine") (vers "0.1.0") (deps (list (crate-dep (name "anyhow") (req "^1.0.68") (default-features #t) (kind 0)) (crate-dep (name "async-compression") (req "^0.3.15") (features (quote ("tokio" "zstd"))) (default-features #t) (kind 0)) (crate-dep (name "bytes") (req "^1.3.0") (default-features #t) (kind 0)) (crate-dep (name "futures") (req "^0.3.25") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.23.0") (features (quote ("macros" "net" "rt" "fs" "io-util" "time" "sync"))) (default-features #t) (kind 0)) (crate-dep (name "tokio-util") (req "^0.7.4") (features (quote ("codec"))) (default-features #t) (kind 0)) (crate-dep (name "xflags") (req "^0.3.1") (default-features #t) (kind 0)))) (hash "1l674ixvyfkac6j8h4svxzgq8sbcvbn5k6zk90l1i1qifgx2a5wa")))

(define-public crate-tcplistener-accept-timeout-0.0.0 (crate (name "tcplistener-accept-timeout") (vers "0.0.0") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (target "cfg(not(target_os = \"windows\"))") (kind 0)) (crate-dep (name "windows-sys") (req "^0.52") (features (quote ("Win32_Networking_WinSock"))) (default-features #t) (target "cfg(target_os = \"windows\")") (kind 0)))) (hash "0zwlbvdhkr2sxfmhv3bywn2qsrnpbmg0a7y1az5f76q9mkw75p5b")))

(define-public crate-tcplscan-0.1 (crate (name "tcplscan") (vers "0.1.0") (deps (list (crate-dep (name "colored") (req "^2.0.4") (default-features #t) (kind 0)) (crate-dep (name "git2") (req "^0.17.2") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)))) (hash "1sdw4rpdp60mx17gng9x4361k2a08gs3y0rvsd70h4vcdk13q92j")))

(define-public crate-tcplscan-0.1 (crate (name "tcplscan") (vers "0.1.1") (deps (list (crate-dep (name "colored") (req "^2.0.4") (default-features #t) (kind 0)) (crate-dep (name "git2") (req "^0.17.2") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)))) (hash "08ny1wnyg2d7nw3v8mkdij2mqk51l820ij62jf5b5n4fcrndfw4n")))

(define-public crate-tcplscan-0.1 (crate (name "tcplscan") (vers "0.1.2") (deps (list (crate-dep (name "colored") (req "^2.0.4") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)))) (hash "09d3s7yg7lnybmv4mph1visqr64ay1p8n9q177sfihd4dr52bsh6")))

(define-public crate-tcplscan-0.1 (crate (name "tcplscan") (vers "0.1.3") (deps (list (crate-dep (name "colored") (req "^2.0.4") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)))) (hash "1zv9rd1yv0pn57awb1bj9cyk0c4awsmvyi7fyb3mcrz47vjxm565")))

(define-public crate-tcplscan-0.1 (crate (name "tcplscan") (vers "0.1.4") (deps (list (crate-dep (name "colored") (req "^2.0.4") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)))) (hash "1nsb5j9hh5bbqsj53bpn911rj39riqj7j4528x0gy0fmmcgr0p02")))

(define-public crate-tcplscan-0.1 (crate (name "tcplscan") (vers "0.1.5") (deps (list (crate-dep (name "colored") (req "^2.0.4") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)))) (hash "0r442ija1mnagmrh8fb16jg6a8yw583af3imlfsmsc4n7a4llj03")))

