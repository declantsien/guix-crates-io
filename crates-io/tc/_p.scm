(define-module (crates-io tc _p) #:use-module (crates-io))

(define-public crate-tc_parity_parity-0.1 (crate (name "tc_parity_parity") (vers "0.1.0") (deps (list (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "testcontainers") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "web3") (req "^0.4") (default-features #t) (kind 2)))) (hash "1ld7i9b710mwhj90mh04wdfrla6v4wrb37x4rj6bw5w9y5rj6zkd")))

(define-public crate-tc_parity_parity-0.2 (crate (name "tc_parity_parity") (vers "0.2.0") (deps (list (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "tc_core") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "web3") (req "^0.4") (default-features #t) (kind 2)))) (hash "1y931s10s9gd6cgfbxzldir7fic09fh9wly1739pijl0ffws9dw2")))

(define-public crate-tc_parity_parity-0.2 (crate (name "tc_parity_parity") (vers "0.2.1") (deps (list (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "tc_core") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "web3") (req "^0.4") (default-features #t) (kind 2)))) (hash "0gxb96a43rkq962f3wfzrwpd5gss1fawnbvpnn0zrhl8cvyrfyfn")))

(define-public crate-tc_parity_parity-0.3 (crate (name "tc_parity_parity") (vers "0.3.0") (deps (list (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "tc_core") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "web3") (req "^0.4") (default-features #t) (kind 2)))) (hash "1qqkfk13j07hd0x9fgzzwlazvzpm64xbgs85g2dqm16m8p87q2r2")))

(define-public crate-tc_parity_parity-0.4 (crate (name "tc_parity_parity") (vers "0.4.0") (deps (list (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "tc_core") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "web3") (req "^0.5") (default-features #t) (kind 2)))) (hash "1j0zlxlp2g3qi07kry2r0ywpb6avjq9fnq360wj4fpjijrjj4dcq")))

(define-public crate-tc_parity_parity-0.5 (crate (name "tc_parity_parity") (vers "0.5.0") (deps (list (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "tc_core") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "web3") (req "^0.6") (default-features #t) (kind 2)))) (hash "0kp0ga19z453p55xcavfffxpf62dxpgf6bz4is33rc3whyallmw5")))

(define-public crate-tc_parity_parity-0.5 (crate (name "tc_parity_parity") (vers "0.5.1") (deps (list (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "tc_core") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "web3") (req "^0.8") (default-features #t) (kind 2)))) (hash "09w4dqhlg5fgj6c2q5m31ma0lnybr1cg2gvis75z17malg62d68n")))

(define-public crate-tc_postgres-0.2 (crate (name "tc_postgres") (vers "0.2.0") (deps (list (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "tc_core") (req "^0.3") (default-features #t) (kind 0)))) (hash "1cy3ak3laaydxnvgyc7dp3n37nrxf69bxqwjzdfh2kz7ri8jl6nv")))

