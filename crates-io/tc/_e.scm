(define-module (crates-io tc _e) #:use-module (crates-io))

(define-public crate-tc_elasticmq-0.1 (crate (name "tc_elasticmq") (vers "0.1.0") (deps (list (crate-dep (name "tc_core") (req "^0.2") (default-features #t) (kind 0)))) (hash "0n3makw72cn6rqdq6g0jx1m4cqasfgd18vkbqaj8kfq1vpfqyhc9")))

(define-public crate-tc_elasticmq-0.2 (crate (name "tc_elasticmq") (vers "0.2.0") (deps (list (crate-dep (name "tc_core") (req "^0.3") (default-features #t) (kind 0)))) (hash "1c98j28a35pixgzd6m2xwss8gbay7mc6jqhq9052h4q9qxr7qyhd")))

(define-public crate-tc_elasticmq-0.2 (crate (name "tc_elasticmq") (vers "0.2.1") (deps (list (crate-dep (name "tc_core") (req "^0.3") (default-features #t) (kind 0)))) (hash "1l0s6gvxq0a0xwkpgyry28r2cmrz8624wqynscq1g04wj9izgh86")))

