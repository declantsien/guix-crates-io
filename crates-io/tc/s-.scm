(define-module (crates-io tc s-) #:use-module (crates-io))

(define-public crate-tcs-dhbw-0.1 (crate (name "tcs-dhbw") (vers "0.1.0") (hash "04rj2zh14140hh2f0dawpyz3fx257yaaghrbr8sa21hr0c0wvsb0")))

(define-public crate-tcs-dhbw-0.1 (crate (name "tcs-dhbw") (vers "0.1.1") (hash "0ibxw8am3i6qh2zhvjx4bl9l346kf0i81chhmzhih25m5nk65s7d")))

(define-public crate-tcs-dhbw-0.1 (crate (name "tcs-dhbw") (vers "0.1.2") (hash "11af0fz508fnkjyd02ckpi6aidn5cx2w78bvdilf8k0y7wwcx7qm")))

(define-public crate-tcs-dhbw-0.1 (crate (name "tcs-dhbw") (vers "0.1.3") (hash "1pvpsh9qv84187i75isvb7s6w4wdmqmyndzggs4zxqw9hz93hzd3")))

(define-public crate-tcs-dhbw-0.1 (crate (name "tcs-dhbw") (vers "0.1.4") (hash "09yx0nyihljjd5hilkw66bziwnxkxrpqj417n94gyq282mwjs2yn")))

(define-public crate-tcs-dhbw-0.1 (crate (name "tcs-dhbw") (vers "0.1.5") (hash "1iw5kpxkjy1bi6zsjdlkqamysb0gxfrhsrkcqvzc1nf61p4wmb7m")))

(define-public crate-tcs-dhbw-1 (crate (name "tcs-dhbw") (vers "1.0.0") (hash "18p1vmzax4qk74rk53dy36j0cdzngzmh854360656mjy618jk3ff")))

