(define-module (crates-io tc _r) #:use-module (crates-io))

(define-public crate-tc_redis-0.1 (crate (name "tc_redis") (vers "0.1.0") (deps (list (crate-dep (name "tc_core") (req "^0.2") (default-features #t) (kind 0)))) (hash "1iiipz4hvi47y525w1mw5n1hxp62zvx72yk1zx852ysknsdrwxl3")))

(define-public crate-tc_redis-0.2 (crate (name "tc_redis") (vers "0.2.0") (deps (list (crate-dep (name "tc_core") (req "^0.3") (default-features #t) (kind 0)))) (hash "148ccxln94z6nb0y2l79fnjk903l0xdwgqcsj3gdpp6dai4vbblh")))

(define-public crate-tc_redis-0.2 (crate (name "tc_redis") (vers "0.2.1") (deps (list (crate-dep (name "tc_core") (req "^0.3") (default-features #t) (kind 0)))) (hash "1lgndnsi8iql9valrsf3382v82y06ksr761r68hbblw9536qndil")))

