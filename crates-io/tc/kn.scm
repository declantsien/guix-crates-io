(define-module (crates-io tc kn) #:use-module (crates-io))

(define-public crate-tckn_utils-0.1 (crate (name "tckn_utils") (vers "0.1.0") (deps (list (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)) (crate-dep (name "rstest") (req "^0.18.2") (default-features #t) (kind 2)))) (hash "079j6csiadg0lv55rw75gil4126r4bsbvybkf3p3sk0nmqg6skdx")))

