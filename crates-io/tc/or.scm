(define-module (crates-io tc or) #:use-module (crates-io))

(define-public crate-tcorp_math_mods-0.1 (crate (name "tcorp_math_mods") (vers "0.1.0") (deps (list (crate-dep (name "num") (req "^0.1.27") (default-features #t) (kind 0)))) (hash "1z3fb1fiwli9x91dxgbmzgwxw8wb7zgakrqvs9na1hsq3km4cpjh")))

