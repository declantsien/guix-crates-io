(define-module (crates-io tc pi) #:use-module (crates-io))

(define-public crate-tcping-0.0.1 (crate (name "tcping") (vers "0.0.1") (deps (list (crate-dep (name "clap") (req "^4.4.16") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "ctrlc") (req "^3.4.2") (default-features #t) (kind 0)))) (hash "0mnxwf9xymks79k3k82bx52gmyvjcd15xn2nv54jdwgxxhki945j")))

(define-public crate-tcping-1 (crate (name "tcping") (vers "1.0.0") (deps (list (crate-dep (name "clap") (req "^4.4.16") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "ctrlc") (req "^3.4.2") (default-features #t) (kind 0)))) (hash "1hq36l6l2akyzbwr2jybp8rwpj2p1y1zpclaan6r2qyxhgvl5mrl")))

(define-public crate-tcping-1 (crate (name "tcping") (vers "1.0.1") (deps (list (crate-dep (name "clap") (req "^4.4.18") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "ctrlc") (req "^3.4.2") (default-features #t) (kind 0)))) (hash "1wxmkrccacarw8gn5x9llvm1ziryvlr3jbrspyyf116bnr98caq1")))

