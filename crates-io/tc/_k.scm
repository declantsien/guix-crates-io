(define-module (crates-io tc _k) #:use-module (crates-io))

(define-public crate-tc_kimlik-0.1 (crate (name "tc_kimlik") (vers "0.1.0") (hash "0m4mcf0mwxz108f7ppdbw2rxhm9w5jqpfkqmmf25wqxqbsb3j9jk")))

(define-public crate-tc_kimlik-0.2 (crate (name "tc_kimlik") (vers "0.2.0") (deps (list (crate-dep (name "rand") (req "^0.7") (default-features #t) (kind 0)))) (hash "1v2rdc93b3jfayh90mvgxirxm19cw23yrdgrikc9w9mqri3hp4lm")))

