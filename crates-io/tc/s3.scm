(define-module (crates-io tc s3) #:use-module (crates-io))

(define-public crate-tcs3400-0.1 (crate (name "tcs3400") (vers "0.1.0") (deps (list (crate-dep (name "embedded-hal") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "embedded-hal-mock") (req "^0.7") (default-features #t) (kind 2)) (crate-dep (name "linux-embedded-hal") (req "^0.3") (default-features #t) (kind 2)))) (hash "1dlw6n3nx5cmri99y4ynwqc5dms4dcdd26cxvf1f1l8yg7jdv8xv")))

(define-public crate-tcs3472-0.1 (crate (name "tcs3472") (vers "0.1.0") (deps (list (crate-dep (name "embedded-hal") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "embedded-hal-mock") (req "^0.2") (default-features #t) (kind 2)) (crate-dep (name "linux-embedded-hal") (req "^0.2") (default-features #t) (kind 2)))) (hash "0w4i24461xw1qjzkblpf17cgsn74dnc73yba6h79qrjdmpzdvdgi")))

(define-public crate-tcs3472-0.1 (crate (name "tcs3472") (vers "0.1.1") (deps (list (crate-dep (name "embedded-hal") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "embedded-hal-mock") (req "^0.2") (default-features #t) (kind 2)) (crate-dep (name "linux-embedded-hal") (req "^0.2") (default-features #t) (kind 2)))) (hash "15wh3wn47r22yqayl7m3xg7kvnrijsjkr95s2dvyyh003dkhbizl")))

(define-public crate-tcs3472-0.2 (crate (name "tcs3472") (vers "0.2.0") (deps (list (crate-dep (name "embedded-hal") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "embedded-hal-mock") (req "^0.7") (default-features #t) (kind 2)) (crate-dep (name "linux-embedded-hal") (req "^0.3") (default-features #t) (kind 2)))) (hash "0kgc9n1l9qb1f2xm1i7z9bdv857c0pk4wx5vfp9mmjkz8sq69dbz")))

