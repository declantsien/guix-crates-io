(define-module (crates-io tc pf) #:use-module (crates-io))

(define-public crate-tcpforward-0.1 (crate (name "tcpforward") (vers "0.1.0") (deps (list (crate-dep (name "structopt") (req "^0.3.21") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.5.0") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "14jyc6jgr4pq9l1pd0dhyvk1l84pkrm7jc565dr0qa7h1f07fgnl")))

(define-public crate-tcpforward-0.1 (crate (name "tcpforward") (vers "0.1.1") (deps (list (crate-dep (name "structopt") (req "^0.3.21") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.5.0") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "0plfvm46ny2gqjyps691gnkrwrbd1m1zg702nl74kaq0isdb5qvg")))

(define-public crate-tcpforward-0.1 (crate (name "tcpforward") (vers "0.1.2") (deps (list (crate-dep (name "structopt") (req "^0.3.21") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.5.0") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "1nl3g5vg46vnjbfp4ivn41jx1zfllgpjnvzwbqzjkz7rb55q7xjm")))

(define-public crate-tcpforward-0.1 (crate (name "tcpforward") (vers "0.1.3") (deps (list (crate-dep (name "structopt") (req "^0.3.21") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.5.0") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "12vk6n32kn3gcanxpji0lpwjj229vwzj554dbfvhcivrc5r796gx")))

