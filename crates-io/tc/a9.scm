(define-module (crates-io tc a9) #:use-module (crates-io))

(define-public crate-tca9535-0.1 (crate (name "tca9535") (vers "0.1.0") (deps (list (crate-dep (name "bitflags") (req "^1.2") (default-features #t) (kind 0)) (crate-dep (name "embedded-hal") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "embedded-hal-mock") (req "^0.7") (default-features #t) (kind 2)))) (hash "15g28yk76c9vgygprisshwh9mrb5gn8slkg8m6n04xnjk3yfdv1j")))

(define-public crate-tca9539-0.1 (crate (name "tca9539") (vers "0.1.0") (deps (list (crate-dep (name "bit_field") (req "^0.10.1") (default-features #t) (kind 0)) (crate-dep (name "embedded-hal") (req "^0.2.3") (default-features #t) (kind 0)) (crate-dep (name "num_enum") (req "^0.5.7") (kind 0)))) (hash "0xiva3i32m5hyis8shkn8gjnxadyj7qja67biplpg07vb3hvl7cc")))

(define-public crate-tca9539-0.2 (crate (name "tca9539") (vers "0.2.0") (deps (list (crate-dep (name "bit_field") (req "^0.10.1") (default-features #t) (kind 0)) (crate-dep (name "embedded-hal") (req "^0.2.3") (default-features #t) (kind 0)) (crate-dep (name "num_enum") (req "^0.5.7") (kind 0)))) (hash "0wd9r5px54jhb2766k5mmsqixmgnh98jm3yyq1j5ng57lc08ni85")))

(define-public crate-tca9539-0.2 (crate (name "tca9539") (vers "0.2.1") (deps (list (crate-dep (name "bit_field") (req "^0.10.1") (default-features #t) (kind 0)) (crate-dep (name "embedded-hal") (req "^0.2.3") (default-features #t) (kind 0)) (crate-dep (name "num_enum") (req "^0.7.2") (kind 0)))) (hash "0pdzvxp3097cv5lsqci8v7n4jrphwsla8h3a0mz7n0815m8ns7vv")))

(define-public crate-tca9548-0.1 (crate (name "tca9548") (vers "0.1.0") (deps (list (crate-dep (name "embedded-hal") (req "^0.2.4") (default-features #t) (kind 0)))) (hash "02s7adl5147wghc66m8x74kmjvidrigr814flyfj16zylgpndbvg")))

(define-public crate-tca9555-0.1 (crate (name "tca9555") (vers "0.1.0") (deps (list (crate-dep (name "defmt") (req "^0.3") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "embedded-hal") (req "^0.2") (default-features #t) (kind 0)))) (hash "1ajg9grklrfsl0970jq399kdg4adzqkkzzj5nz7smfn8kknv7sb1") (features (quote (("unproven" "embedded-hal/unproven") ("default" "unproven")))) (v 2) (features2 (quote (("use_defmt" "dep:defmt"))))))

