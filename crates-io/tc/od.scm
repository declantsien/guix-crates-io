(define-module (crates-io tc od) #:use-module (crates-io))

(define-public crate-tcod-0.3 (crate (name "tcod") (vers "0.3.0") (deps (list (crate-dep (name "tcod-sys") (req "^1.5.2") (default-features #t) (kind 0)))) (hash "09izvvkbbzq4byjs7sq16366jiai9g4pzxjq300sacin7h2r2w2s")))

(define-public crate-tcod-0.3 (crate (name "tcod") (vers "0.3.1") (deps (list (crate-dep (name "tcod-sys") (req "^1.5.2") (default-features #t) (kind 0)))) (hash "0001174qlyv6kwwfmwssq3435l80y0w7jisdh4r3s0v43s3gxa36")))

(define-public crate-tcod-0.3 (crate (name "tcod") (vers "0.3.2") (deps (list (crate-dep (name "tcod-sys") (req "^1.5.2") (default-features #t) (kind 0)))) (hash "0xyhpmglqwb44q3w7qwgzbic9l4jvqh07y04awsfdnmgkrz1rf3q")))

(define-public crate-tcod-0.3 (crate (name "tcod") (vers "0.3.3") (deps (list (crate-dep (name "tcod-sys") (req "^1.5.2") (default-features #t) (kind 0)))) (hash "1x3x8s4nams7rqm18q3xjf99avshld4pinc619x1hlrmc5gxac6k")))

(define-public crate-tcod-0.4 (crate (name "tcod") (vers "0.4.0") (deps (list (crate-dep (name "tcod-sys") (req "^1.5.2") (default-features #t) (kind 0)))) (hash "1v3x2nz5cf5kn7zd337cs9qymrrhggkrf1vn27jgmbs82qddcf5p")))

(define-public crate-tcod-0.4 (crate (name "tcod") (vers "0.4.1") (deps (list (crate-dep (name "tcod-sys") (req "^2.0.0") (default-features #t) (kind 0)))) (hash "1hwn2cbh896w8zhykmzhcnzj43xyryp82n6x904m8l4gxqvqq9rm")))

(define-public crate-tcod-0.4 (crate (name "tcod") (vers "0.4.2") (deps (list (crate-dep (name "tcod-sys") (req "^2.0.0") (default-features #t) (kind 0)))) (hash "1c9ffj10ha8wvw8kanlhhig3apy5hc7mxc7176fs4mjnzj6szzrv")))

(define-public crate-tcod-0.4 (crate (name "tcod") (vers "0.4.3") (deps (list (crate-dep (name "bitflags") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "tcod-sys") (req "^2.0.0") (default-features #t) (kind 0)))) (hash "050f3x0ns4i05vhldf2b2f2v410da3p51qnsf1yad9ky51vf6q84")))

(define-public crate-tcod-0.4 (crate (name "tcod") (vers "0.4.4") (deps (list (crate-dep (name "bitflags") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "tcod-sys") (req "^2.0.1") (default-features #t) (kind 0)))) (hash "1cafi80xri8hq7pacj1mxixy6h0ir8mhlh2bvvp803pr21d6xylz")))

(define-public crate-tcod-0.4 (crate (name "tcod") (vers "0.4.5") (deps (list (crate-dep (name "bitflags") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "tcod-sys") (req "^2.0.2") (default-features #t) (kind 0)))) (hash "144ggglj5d7mg5q17i3w1dk44ww484vc9q753hrx55kf65f5wjfl")))

(define-public crate-tcod-0.4 (crate (name "tcod") (vers "0.4.6") (deps (list (crate-dep (name "bitflags") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "tcod-sys") (req "^2.0.3") (default-features #t) (kind 0)))) (hash "1n8h5c19nq3f4g0wzbps5fkamijswwizgklfg8nmnxmmlckydj1s")))

(define-public crate-tcod-0.4 (crate (name "tcod") (vers "0.4.7") (deps (list (crate-dep (name "bitflags") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.1.2") (default-features #t) (kind 2)) (crate-dep (name "tcod-sys") (req "^2.0.4") (default-features #t) (kind 0)))) (hash "052wg2l4lijq0a1gn7m1z6qfdfpkam7imjnjqx0d82hp8wydbq7i")))

(define-public crate-tcod-0.5 (crate (name "tcod") (vers "0.5.0") (deps (list (crate-dep (name "bitflags") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.1.2") (default-features #t) (kind 2)) (crate-dep (name "tcod-sys") (req "^2.0.5") (default-features #t) (kind 0)))) (hash "1x1mhmx8wm2l32nl7bxxx6rs0kkn5igfzzfdw5m6mcyqh7jkm4ld")))

(define-public crate-tcod-0.5 (crate (name "tcod") (vers "0.5.1") (deps (list (crate-dep (name "bitflags") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.1.2") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.2.1") (default-features #t) (kind 2)) (crate-dep (name "tcod-sys") (req "^2.0.6") (default-features #t) (kind 0)))) (hash "0frv5j2p126jrjipmandmk4c0j9mpq7k6ql2q44majxmzd6gzvxl")))

(define-public crate-tcod-0.5 (crate (name "tcod") (vers "0.5.2") (deps (list (crate-dep (name "bitflags") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.1.2") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.2.1") (default-features #t) (kind 2)) (crate-dep (name "tcod-sys") (req "^2.0.7") (default-features #t) (kind 0)))) (hash "1id9v2rvvkljssgvbmw1gs5p4jg099m0dl1phpbx0mg4vnskvdyr")))

(define-public crate-tcod-0.5 (crate (name "tcod") (vers "0.5.4") (deps (list (crate-dep (name "bitflags") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.1.2") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.3.5") (default-features #t) (kind 2)) (crate-dep (name "tcod-sys") (req "^2.0.8") (default-features #t) (kind 0)))) (hash "1w0gf7al54p05nmh0yzxpbbrmg82ml651j6zydx1lmn90p9h8rc2")))

(define-public crate-tcod-0.5 (crate (name "tcod") (vers "0.5.5") (deps (list (crate-dep (name "bitflags") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.1.5") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.3.5") (default-features #t) (kind 2)) (crate-dep (name "tcod-sys") (req "^2.0.10") (default-features #t) (kind 0)) (crate-dep (name "time") (req "^0.1.24") (default-features #t) (kind 0)))) (hash "0jgwlsys5z9fr6sy45qjw3qiwggn61x24z744xnifp0vk11w2wy8")))

(define-public crate-tcod-0.6 (crate (name "tcod") (vers "0.6.0") (deps (list (crate-dep (name "bitflags") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.1.5") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.3.5") (default-features #t) (kind 2)) (crate-dep (name "tcod-sys") (req "^2.0.11") (default-features #t) (kind 0)) (crate-dep (name "time") (req "^0.1.24") (default-features #t) (kind 0)))) (hash "0jkw9m3g3j0w5fayk7alhs66sz6z9s89pvd3llzxb1pr97nlicay")))

(define-public crate-tcod-0.7 (crate (name "tcod") (vers "0.7.0") (deps (list (crate-dep (name "bitflags") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.1.5") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.3.8") (default-features #t) (kind 2)) (crate-dep (name "tcod-sys") (req "^2.0.12") (default-features #t) (kind 0)) (crate-dep (name "time") (req "^0.1.25") (default-features #t) (kind 0)))) (hash "0498xyv130czc4x19m94sil6qflsn5ayv63g452gpghv54vgkfdb")))

(define-public crate-tcod-0.8 (crate (name "tcod") (vers "0.8.0") (deps (list (crate-dep (name "bitflags") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "rustc-serialize") (req "^0.3") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^0.6") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^0.6") (default-features #t) (kind 2)) (crate-dep (name "tcod-sys") (req "^3.0") (default-features #t) (kind 0)))) (hash "0qza7b0w2mh5890wrx8s6rpziqxfb9l547mxr5r8xpgwy7lxddpb")))

(define-public crate-tcod-0.8 (crate (name "tcod") (vers "0.8.1") (deps (list (crate-dep (name "bitflags") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "rustc-serialize") (req "^0.3") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^0.6") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^0.6") (default-features #t) (kind 2)) (crate-dep (name "tcod-sys") (req "^3.0") (default-features #t) (kind 0)))) (hash "085m7fq7131nvgz28bh786ps2m1mg1k2wishk6jlzc6gi8b404zh")))

(define-public crate-tcod-0.9 (crate (name "tcod") (vers "0.9.0") (deps (list (crate-dep (name "bitflags") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "rustc-serialize") (req "^0.3") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^0.6") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^0.6") (default-features #t) (kind 2)) (crate-dep (name "tcod-sys") (req "^4.0") (default-features #t) (kind 0)))) (hash "0f2az36pyj82ffwj9zh70s3vb37ccsjdvfyijj723dr1phylb5fi")))

(define-public crate-tcod-0.10 (crate (name "tcod") (vers "0.10.0") (deps (list (crate-dep (name "bitflags") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "rustc-serialize") (req "^0.3") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^0.6") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^0.6") (default-features #t) (kind 2)) (crate-dep (name "tcod-sys") (req "^4.0.1") (default-features #t) (kind 0)))) (hash "0gg695mn0i0q1l6sj09mz3n6d38k8mfh6jir03h9zln3n4xhrfa0")))

(define-public crate-tcod-0.11 (crate (name "tcod") (vers "0.11.0") (deps (list (crate-dep (name "bitflags") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "rustc-serialize") (req "^0.3") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^0.9") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^0.9") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^0.9") (default-features #t) (kind 2)) (crate-dep (name "tcod-sys") (req "^4.0.1") (default-features #t) (kind 0)))) (hash "1y1wq6d3680ckdn5wn1gfkm9r6plcv916hkrz0hiwy8dc3sk9927") (features (quote (("serialization" "serde" "serde_derive"))))))

(define-public crate-tcod-0.12 (crate (name "tcod") (vers "0.12.0") (deps (list (crate-dep (name "bitflags") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "rustc-serialize") (req "^0.3") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 2)) (crate-dep (name "tcod-sys") (req "^4.1.0") (default-features #t) (kind 0)))) (hash "0ap7s2ik73h7468nhdsyyxaz20vcwxx8mskmx8nx6ahr69gl3w6l") (features (quote (("serialization" "serde" "serde_derive"))))))

(define-public crate-tcod-0.12 (crate (name "tcod") (vers "0.12.1") (deps (list (crate-dep (name "bitflags") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "rustc-serialize") (req "^0.3") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 2)) (crate-dep (name "tcod-sys") (req "^4.0.1") (default-features #t) (kind 0)))) (hash "16hyvfnyai6w16vadssy1bl7np2scxl7v316gwyv87ixdyxhxr19") (features (quote (("serialization" "serde" "serde_derive"))))))

(define-public crate-tcod-0.13 (crate (name "tcod") (vers "0.13.0") (deps (list (crate-dep (name "bitflags") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "rustc-serialize") (req "^0.3") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 2)) (crate-dep (name "tcod-sys") (req "^5.0.0") (default-features #t) (kind 0)))) (hash "0nca0h079afzyq9nra2d3vrfwxgmvnkqkpxv6k1l812mrmyq85a2") (features (quote (("serialization" "serde" "serde_derive"))))))

(define-public crate-tcod-0.14 (crate (name "tcod") (vers "0.14.0") (deps (list (crate-dep (name "bitflags") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "rustc-serialize") (req "^0.3") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 2)) (crate-dep (name "tcod-sys") (req "^5.0.0") (default-features #t) (kind 0)))) (hash "1x0fhdfxxpbbmhjmfabghqx7rmbl46vki85lqr0jay4csw2j85v4") (features (quote (("serialization" "serde" "serde_derive"))))))

(define-public crate-tcod-0.15 (crate (name "tcod") (vers "0.15.0") (deps (list (crate-dep (name "bitflags") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "rustc-serialize") (req "^0.3") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 2)) (crate-dep (name "tcod-sys") (req "^5.0.0") (default-features #t) (kind 0)))) (hash "0i71wli2vz7dq4192lxwi3fhb900gjfnsvdwilp72j8rcsq1ira4") (features (quote (("serialization" "serde" "serde_derive"))))))

(define-public crate-tcod-sys-1 (crate (name "tcod-sys") (vers "1.5.2") (hash "18913nbjbmn59y1pdppacz9jjnina7av6pphcrs0cs487jp5hl6x")))

(define-public crate-tcod-sys-2 (crate (name "tcod-sys") (vers "2.0.0") (hash "1q58nm8qi6s777pi3x544gx7p63mr6qbxinzv5y3sc08xw7vxppm")))

(define-public crate-tcod-sys-2 (crate (name "tcod-sys") (vers "2.0.1") (hash "1ffm1vcsaf7c735777jc55xj0irbm2rvn4zy3z8hvy8d4zr02q0w")))

(define-public crate-tcod-sys-2 (crate (name "tcod-sys") (vers "2.0.2") (hash "076dccrazp2b30qd7f2gfd4jfg3f1bnr8nxklzbrcw5wiil0y1vy")))

(define-public crate-tcod-sys-2 (crate (name "tcod-sys") (vers "2.0.3") (hash "0l9y0gdxf4pxxcyck0w5pw07a7p1pkazjas6h3v0a7453m572mbn")))

(define-public crate-tcod-sys-2 (crate (name "tcod-sys") (vers "2.0.4") (hash "0d91vq52naq11sjx3xfv5in7mnb0l894x1ajrmizyzpb1zqd3hk9")))

(define-public crate-tcod-sys-2 (crate (name "tcod-sys") (vers "2.0.5") (hash "008czbnrl8bs3ivghwnd7dkkghw4q3z1kaw72n7ldr35lpbs3xic")))

(define-public crate-tcod-sys-2 (crate (name "tcod-sys") (vers "2.0.6") (deps (list (crate-dep (name "libc") (req "^0.1.2") (default-features #t) (kind 0)))) (hash "0vmasdscpv39xnv2xwkaa415rhxmgpvz25daljm3j2cb4i03358i")))

(define-public crate-tcod-sys-2 (crate (name "tcod-sys") (vers "2.0.7") (deps (list (crate-dep (name "libc") (req "^0.1.2") (default-features #t) (kind 0)))) (hash "0cvw2bdlbfhxz5sqz3xlps7xjyrzh31zhf49l28api2br1ymlygd")))

(define-public crate-tcod-sys-2 (crate (name "tcod-sys") (vers "2.0.8") (deps (list (crate-dep (name "libc") (req "^0.1.2") (default-features #t) (kind 0)))) (hash "0kh85v6g8pqgyg79n6519i42awz1nyhcyfr3sl10clcp06d27s1w")))

(define-public crate-tcod-sys-2 (crate (name "tcod-sys") (vers "2.0.9") (deps (list (crate-dep (name "libc") (req "^0.1.2") (default-features #t) (kind 0)))) (hash "103pi8wa8vkk0ni4w36d6nhfrrivjxmqgx431k66axh1spxlkhj2")))

(define-public crate-tcod-sys-2 (crate (name "tcod-sys") (vers "2.0.10") (deps (list (crate-dep (name "libc") (req "^0.1.5") (default-features #t) (kind 0)))) (hash "055zprx06x74ky08m2p6ibg6379iww16pyh4s1i9zfc7dcq7nryz")))

(define-public crate-tcod-sys-2 (crate (name "tcod-sys") (vers "2.0.11") (deps (list (crate-dep (name "libc") (req "^0.1.5") (default-features #t) (kind 0)))) (hash "0hl5ayqm6w0k631f21s5agc68x2ykx1vp5p3jji64gz6n9y9fz5r")))

(define-public crate-tcod-sys-2 (crate (name "tcod-sys") (vers "2.0.12") (deps (list (crate-dep (name "libc") (req "^0.1.5") (default-features #t) (kind 0)))) (hash "0yjpzil6098sxvdv91yhmg89s7cwhanmr052s2i5wrxmal0axqjl")))

(define-public crate-tcod-sys-3 (crate (name "tcod-sys") (vers "3.0.0") (deps (list (crate-dep (name "libc") (req "^0.1") (default-features #t) (kind 0)))) (hash "01fb7a6kiy06sksisr49mzmb64r3bb965rsmc2lggpyvy7vkzcds")))

(define-public crate-tcod-sys-3 (crate (name "tcod-sys") (vers "3.0.1") (deps (list (crate-dep (name "libc") (req "^0.1") (default-features #t) (kind 0)))) (hash "1qaf62pn4bbs541b7dm581mc7nk0fl38nprpfycjm379b4591wxm")))

(define-public crate-tcod-sys-3 (crate (name "tcod-sys") (vers "3.0.2") (deps (list (crate-dep (name "libc") (req "^0.1") (default-features #t) (kind 0)))) (hash "1ajjlfs96zbikypsmrhx36v60snnglxggw3bxb82xpywy1kcv9rn")))

(define-public crate-tcod-sys-4 (crate (name "tcod-sys") (vers "4.0.0") (deps (list (crate-dep (name "gcc") (req "^0.3") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "pkg-config") (req "^0.3") (default-features #t) (kind 1)))) (hash "15y32ym7cs8d6x751mhpnyy69ilffh1ldrywy52d5s5frdxcm4jl")))

(define-public crate-tcod-sys-4 (crate (name "tcod-sys") (vers "4.0.1") (deps (list (crate-dep (name "gcc") (req "^0.3") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "pkg-config") (req "^0.3") (default-features #t) (kind 1)))) (hash "11iigzjsakmyfmdy8zzh56hsclsq28675ncmwym86df3nakr2jsp")))

(define-public crate-tcod-sys-4 (crate (name "tcod-sys") (vers "4.1.0") (deps (list (crate-dep (name "gcc") (req "^0.3.53") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "pkg-config") (req "^0.3") (default-features #t) (kind 1)))) (hash "0mi4vc4091dl959n0bj8ca8sfl5n41ksghng0k19s5426z3fdjcm")))

(define-public crate-tcod-sys-5 (crate (name "tcod-sys") (vers "5.0.0") (deps (list (crate-dep (name "cc") (req "^1.0") (default-features #t) (kind 1)) (crate-dep (name "pkg-config") (req "^0.3") (default-features #t) (kind 1)))) (hash "00a5wz5bsdy1kipfhpifsacziq1dgd4wsw77f92ppwk9fnmfqmjg") (features (quote (("dynlib")))) (links "tcod")))

(define-public crate-tcod-sys-5 (crate (name "tcod-sys") (vers "5.0.1") (deps (list (crate-dep (name "cc") (req "^1.0") (default-features #t) (kind 1)) (crate-dep (name "pkg-config") (req "^0.3") (default-features #t) (kind 1)))) (hash "1apllm7vnfhrjjswrviw2wv0hzr7f2pc6qbsmrhcixv149yv43vk") (features (quote (("dynlib")))) (links "tcod")))

(define-public crate-tcod_window-0.0.1 (crate (name "tcod_window") (vers "0.0.1") (deps (list (crate-dep (name "pistoncore-input") (req ">= 0.0.5") (default-features #t) (kind 0)) (crate-dep (name "pistoncore-window") (req ">= 0.0.5") (default-features #t) (kind 0)) (crate-dep (name "quack") (req ">= 0.0.8") (default-features #t) (kind 0)) (crate-dep (name "tcod") (req ">= 0.4.6") (default-features #t) (kind 0)))) (hash "13dk2b2qa6g6h20dby8r0yp4v8cpfzsf9l2m3bdda7llvc657ks1")))

(define-public crate-tcod_window-0.0.3 (crate (name "tcod_window") (vers "0.0.3") (deps (list (crate-dep (name "pistoncore-input") (req "^0.0.5") (default-features #t) (kind 0)) (crate-dep (name "pistoncore-window") (req "^0.0.12") (default-features #t) (kind 0)) (crate-dep (name "quack") (req "^0.0.13") (default-features #t) (kind 0)) (crate-dep (name "tcod") (req "^0.5.0") (default-features #t) (kind 0)))) (hash "0qgs50l0hck21ca19a35zh2k9csmy1lshzmi28fx6zwpd3jci9pr")))

(define-public crate-tcod_window-0.1 (crate (name "tcod_window") (vers "0.1.0") (deps (list (crate-dep (name "clippy") (req "^0.0.63") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "pistoncore-input") (req "^0.10.0") (default-features #t) (kind 0)) (crate-dep (name "pistoncore-window") (req "^0.17.0") (default-features #t) (kind 0)) (crate-dep (name "tcod") (req "^0.9.0") (default-features #t) (kind 0)))) (hash "1z3231an0ms8zn9dgwgpxm9d6100rf8gf7xlsnlm2q012xrwk1bc") (features (quote (("nightly-testing" "clippy"))))))

(define-public crate-tcod_window-0.2 (crate (name "tcod_window") (vers "0.2.0") (deps (list (crate-dep (name "clippy") (req "^0.0.63") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "piston") (req "^0.20.0") (default-features #t) (kind 2)) (crate-dep (name "pistoncore-input") (req "^0.10.0") (default-features #t) (kind 0)) (crate-dep (name "pistoncore-window") (req "^0.17.0") (default-features #t) (kind 0)) (crate-dep (name "tcod") (req "^0.9.0") (default-features #t) (kind 0)))) (hash "1mwrs1yf6b7kq1hszl786abbk1sk9bilf4k379isj5nscpwn3z59") (features (quote (("nightly-testing" "clippy"))))))

