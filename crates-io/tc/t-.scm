(define-module (crates-io tc t-) #:use-module (crates-io))

(define-public crate-tct-live-edit-0.1 (crate (name "tct-live-edit") (vers "0.1.0") (hash "00jh1wy1dzvvkmhnzj7c1cf2a9jfvgzkk65gqw30iqlv146fjzbj")))

(define-public crate-tct-visualize-0.1 (crate (name "tct-visualize") (vers "0.1.0") (hash "17ns7qs69aj4dcvmvz996mnhh9xp8j489nmvmkmrlbgwz5gjm26r")))

