(define-module (crates-io tc ma) #:use-module (crates-io))

(define-public crate-tcmalloc-0.1 (crate (name "tcmalloc") (vers "0.1.0") (hash "0iid4bay05zg5ijsjv9k9w7pk0bcfwp65fijx4n8lc6dbcn91hdh")))

(define-public crate-tcmalloc-0.1 (crate (name "tcmalloc") (vers "0.1.1") (hash "0hnl9djjkggfxzh78h065040ywqkwpsh0bm20p9dp94cs1p8cc8l")))

(define-public crate-tcmalloc-0.1 (crate (name "tcmalloc") (vers "0.1.2") (hash "1bfsfjg83qh6xmb4bim8rr4bz60csvhbizc3w5c063rx4wna3pqd")))

(define-public crate-tcmalloc-0.1 (crate (name "tcmalloc") (vers "0.1.3") (hash "097sy977a75dx9h79lrvx1x4dr4sz8ablqxmw6cb6cr681vw6znk")))

(define-public crate-tcmalloc-0.1 (crate (name "tcmalloc") (vers "0.1.4") (hash "0k7vqm6lcfxjmd0dy481ybxvdm9fdi1908b29db927jxqqgbv1c3")))

(define-public crate-tcmalloc-0.1 (crate (name "tcmalloc") (vers "0.1.5") (hash "1i66hj2rw7lq5z85px5chvy159y0p0v82x3wwjkph6lg7ypv8spz")))

(define-public crate-tcmalloc-0.2 (crate (name "tcmalloc") (vers "0.2.0") (hash "0ch2awcg725r22bb8nb5g5x1pxch1hc3nk7svzpnv0dw2ibyp487")))

(define-public crate-tcmalloc-0.3 (crate (name "tcmalloc") (vers "0.3.0") (deps (list (crate-dep (name "tcmalloc-sys") (req "^0.3.0") (optional #t) (default-features #t) (kind 0)))) (hash "16w1kpfzv093mljk9k2skldbwhcfw06amgk7xppcb8c47l8halip") (features (quote (("default") ("bundled" "tcmalloc-sys"))))))

(define-public crate-tcmalloc-sys-0.3 (crate (name "tcmalloc-sys") (vers "0.3.0") (hash "0h0zzya214rd5ljrasyc47j1v9jrkwk5jv8hqb135ljxcczdfyiv") (links "tcmalloc")))

(define-public crate-tcmalloc2-0.1 (crate (name "tcmalloc2") (vers "0.1.0+2.10") (deps (list (crate-dep (name "bindgen") (req "^0.66") (default-features #t) (kind 1)) (crate-dep (name "num_cpus") (req "^1.15") (default-features #t) (kind 1)))) (hash "19y7pyrxy1j0vxx5bmn7vsbnx0m6axsg3spi05lwa8hnzx4lm48g")))

(define-public crate-tcmalloc2-0.1 (crate (name "tcmalloc2") (vers "0.1.1+2.10") (deps (list (crate-dep (name "bindgen") (req "^0.66") (default-features #t) (kind 1)) (crate-dep (name "num_cpus") (req "^1.15") (default-features #t) (kind 1)))) (hash "0l1q1zwnn17vcjamcpay13ppf2vgcgbnbnnikf5jz5fasdkh7mwp")))

(define-public crate-tcmalloc2-0.1 (crate (name "tcmalloc2") (vers "0.1.2+2.13") (deps (list (crate-dep (name "bindgen") (req "^0.68") (default-features #t) (kind 1)) (crate-dep (name "num_cpus") (req "^1") (default-features #t) (kind 1)))) (hash "1984kzd70ydmy12h57jnnqdzqwfnj6qjka2vbvh3m74n6vza9yas")))

(define-public crate-tcmalloc2-0.2 (crate (name "tcmalloc2") (vers "0.2.0+2.15") (deps (list (crate-dep (name "bindgen") (req "^0.69") (default-features #t) (kind 1)) (crate-dep (name "num_cpus") (req "^1") (default-features #t) (kind 1)))) (hash "10qsf5nnd10gdvqh4n9nlmsafdcv0z85sbxy07kfzgz9bhldhlgy")))

