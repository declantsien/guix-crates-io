(define-module (crates-io tc _t) #:use-module (crates-io))

(define-public crate-tc_tea-0.1 (crate (name "tc_tea") (vers "0.1.0") (deps (list (crate-dep (name "rand") (req "^0.8.0") (features (quote ("rand_chacha"))) (default-features #t) (kind 0)) (crate-dep (name "rand_chacha") (req "^0.3.1") (default-features #t) (kind 0)))) (hash "0ylsvm5y20ysh06jvjg02x1bf309240xzbmnffjpw1gmhigp9gb8") (features (quote (("secure_random" "rand/getrandom") ("default" "secure_random"))))))

(define-public crate-tc_tea-0.1 (crate (name "tc_tea") (vers "0.1.1") (deps (list (crate-dep (name "rand") (req "^0.8.0") (features (quote ("rand_chacha"))) (default-features #t) (kind 0)) (crate-dep (name "rand_chacha") (req "^0.3.1") (default-features #t) (kind 0)))) (hash "1h4jfwaw9dscn1abqp3npkniqh1gz3qyi4q4lrhjw96i3n8ijyf3") (features (quote (("secure_random" "rand/getrandom") ("default" "secure_random"))))))

(define-public crate-tc_tea-0.1 (crate (name "tc_tea") (vers "0.1.3") (deps (list (crate-dep (name "rand") (req "^0.8.0") (features (quote ("rand_chacha"))) (default-features #t) (kind 0)) (crate-dep (name "rand_chacha") (req "^0.3.1") (default-features #t) (kind 0)))) (hash "0q8wg1b1j8dc36lj1kwjdwc8i0ry2d4r24461sk06fb5cd3s950r") (features (quote (("secure_random" "rand/getrandom") ("default" "secure_random"))))))

(define-public crate-tc_tea-0.1 (crate (name "tc_tea") (vers "0.1.4") (deps (list (crate-dep (name "rand") (req "^0.8.0") (features (quote ("rand_chacha"))) (default-features #t) (kind 0)) (crate-dep (name "rand_chacha") (req "^0.3.1") (default-features #t) (kind 0)))) (hash "1lk6gl9gv9mcki5x3nj39db5prnz4j0npiw2hpncpr8fwy2n5cna") (features (quote (("secure_random" "rand/getrandom") ("default" "secure_random"))))))

(define-public crate-tc_trufflesuite_ganachecli-0.1 (crate (name "tc_trufflesuite_ganachecli") (vers "0.1.0") (deps (list (crate-dep (name "testcontainers") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "web3") (req "^0.3.0") (default-features #t) (kind 2)))) (hash "1i7j1gvlbw4cmk6dad6vjibf7kpvgyz8xg3ina6j8m6k3as3ykw0")))

(define-public crate-tc_trufflesuite_ganachecli-0.1 (crate (name "tc_trufflesuite_ganachecli") (vers "0.1.1") (deps (list (crate-dep (name "testcontainers") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "web3") (req "^0.4") (default-features #t) (kind 2)))) (hash "10n856c2slyg913ra6l0lfl6di3c4m483x5wrk2qbkbzb898kzry") (yanked #t)))

(define-public crate-tc_trufflesuite_ganachecli-0.2 (crate (name "tc_trufflesuite_ganachecli") (vers "0.2.0") (deps (list (crate-dep (name "testcontainers") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "web3") (req "^0.4") (default-features #t) (kind 2)))) (hash "0bdbgvb0xaal0nq5k2m0gj3bqyvyg3if5hrgyk3b4l1k0rr685l3")))

(define-public crate-tc_trufflesuite_ganachecli-0.3 (crate (name "tc_trufflesuite_ganachecli") (vers "0.3.0") (deps (list (crate-dep (name "tc_core") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "web3") (req "^0.4") (default-features #t) (kind 2)))) (hash "1nspv36ns2wb06nqj52mx23hgj1jxiy1abg1fh33pcs09si6mrzz")))

(define-public crate-tc_trufflesuite_ganachecli-0.3 (crate (name "tc_trufflesuite_ganachecli") (vers "0.3.1") (deps (list (crate-dep (name "tc_core") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "web3") (req "^0.4") (default-features #t) (kind 2)))) (hash "1l0v30wgflc3kb7g15smvq3dzdli2v587qyi4iw37hnhhax6csar")))

(define-public crate-tc_trufflesuite_ganachecli-0.4 (crate (name "tc_trufflesuite_ganachecli") (vers "0.4.0") (deps (list (crate-dep (name "tc_core") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "web3") (req "^0.5") (default-features #t) (kind 2)))) (hash "1lijnprj052g5gqqn2zi715i7a5xhgyq4gwl6kb8avdibvd9kmkd")))

(define-public crate-tc_trufflesuite_ganachecli-0.4 (crate (name "tc_trufflesuite_ganachecli") (vers "0.4.1") (deps (list (crate-dep (name "tc_core") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "web3") (req "^0.8") (default-features #t) (kind 2)))) (hash "1q5x87l2nbppxi3g8vd50z00a6qf2pnjaym9gis16zc73cgp7dbd")))

