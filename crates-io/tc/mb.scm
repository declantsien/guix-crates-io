(define-module (crates-io tc mb) #:use-module (crates-io))

(define-public crate-tcmb_evds-0.1 (crate (name "tcmb_evds") (vers "0.1.0") (deps (list (crate-dep (name "curl") (req "^0.4.38") (default-features #t) (kind 0)))) (hash "1xs871xbwd1q1wwrc07a9mddkaa8g6dxzwagqrqs0qfy8i4c10av") (features (quote (("sync_mode") ("default" "async_mode") ("async_mode"))))))

