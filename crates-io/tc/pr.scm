(define-module (crates-io tc pr) #:use-module (crates-io))

(define-public crate-tcprint-0.0.0 (crate (name "tcprint") (vers "0.0.0-dev.0") (deps (list (crate-dep (name "termcolor") (req "^1.0") (default-features #t) (kind 0)))) (hash "0b2sg46pfc55r169b1d3ns7a5wmm4nijx8fmhn4cy696ycisprj4")))

(define-public crate-tcprint-0.0.0 (crate (name "tcprint") (vers "0.0.0-dev.1") (deps (list (crate-dep (name "termcolor") (req "^1.0") (default-features #t) (kind 0)))) (hash "05h54dgbg4qkhri4nqp0l99k3ch37c28cmd7kz2mv7g9pxpv03p3")))

(define-public crate-tcprint-0.0.0 (crate (name "tcprint") (vers "0.0.0-dev.2") (deps (list (crate-dep (name "termcolor") (req "^1.0") (default-features #t) (kind 0)))) (hash "1bfi2kz3v0lf0msajjmwl4hg0pkkb05jrfazvbplc51mza64xj6i")))

(define-public crate-tcprint-0.0.0 (crate (name "tcprint") (vers "0.0.0-dev.3") (deps (list (crate-dep (name "termcolor") (req "^1.0") (default-features #t) (kind 0)))) (hash "1gi62w6ypfxv5wssxwdzrh8zvqaas8na1qc6ypy73yjdxkf8mjy2")))

(define-public crate-tcprint-0.1 (crate (name "tcprint") (vers "0.1.5") (deps (list (crate-dep (name "termcolor") (req "^1.0") (default-features #t) (kind 0)))) (hash "0b2f33fcav6n1mcvph5bzm167zaihnblhhmac68fmkkfxk8zb47r")))

