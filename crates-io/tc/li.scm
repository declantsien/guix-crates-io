(define-module (crates-io tc li) #:use-module (crates-io))

(define-public crate-tcli-0.1 (crate (name "tcli") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^3.1.5") (default-features #t) (kind 0)) (crate-dep (name "termion") (req "^1.5.5") (default-features #t) (kind 0)))) (hash "194535933mwvk7v4wyw7mw2jccjxccy8zq4rr9splrjgwhj8jbms")))

(define-public crate-tcli-0.1 (crate (name "tcli") (vers "0.1.1") (deps (list (crate-dep (name "clap") (req "^3.1.5") (default-features #t) (kind 0)) (crate-dep (name "termion") (req "^1.5.5") (default-features #t) (kind 0)))) (hash "1jdn70r5b7mj544i52k86n4z726hwqajqfl26m2v26wabwb5djl7")))

(define-public crate-tcli-0.1 (crate (name "tcli") (vers "0.1.2") (deps (list (crate-dep (name "clap") (req "^3.1.5") (default-features #t) (kind 0)) (crate-dep (name "termion") (req "^1.5.5") (default-features #t) (kind 0)))) (hash "05f9jyph8a67j6v99ikvvw8nddvgzs79w419f8668dfa68lhkqqs")))

(define-public crate-tcli-0.1 (crate (name "tcli") (vers "0.1.3") (deps (list (crate-dep (name "argh") (req "^0.1.7") (default-features #t) (kind 0)) (crate-dep (name "termion") (req "^1.5.5") (default-features #t) (kind 0)))) (hash "18q92cqiqxidln9fnxmfixi220bk1z44q8n1pansj1pn8k1n7gam")))

(define-public crate-tcli-0.1 (crate (name "tcli") (vers "0.1.4") (deps (list (crate-dep (name "argh") (req "^0.1.7") (default-features #t) (kind 0)) (crate-dep (name "termion") (req "^1.5.5") (default-features #t) (kind 0)))) (hash "0g1bl0qfz4kwd6xqaczjmkn22plp9r22gbqzr6cv6id0vk72sgfw")))

