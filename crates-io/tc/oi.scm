(define-module (crates-io tc oi) #:use-module (crates-io))

(define-public crate-tcoi-0.1 (crate (name "tcoi") (vers "0.1.0") (deps (list (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)))) (hash "1il0s3zaxgzn61fw31jill1p9j964glrcqsg9qv6sr85pi31k3ms") (yanked #t)))

(define-public crate-tcoi-0.2 (crate (name "tcoi") (vers "0.2.0") (deps (list (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)))) (hash "0zggwpdxh2g2spzqv9i7rrk4q4hi5mv2pniq3n4zf5hb0an1dqag") (yanked #t)))

(define-public crate-tcoi-0.2 (crate (name "tcoi") (vers "0.2.1") (deps (list (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)))) (hash "1a1hy6nzkhzyxs7yhgsflkdwz91c7gbscggjjm45j9495ys602r5") (yanked #t)))

(define-public crate-tcoi-0.2 (crate (name "tcoi") (vers "0.2.2") (deps (list (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)))) (hash "0ndxvr29bv3kkyhg3zq00iyimaza59hdhcppv3sj8ci76k9k43f1") (yanked #t)))

(define-public crate-tcoi-0.2 (crate (name "tcoi") (vers "0.2.3") (deps (list (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)))) (hash "13r66fbag9fk0nhl4jnxjc5b40yv5hb7cahnibq2chg1hsqamisd") (yanked #t)))

(define-public crate-tcoi-0.2 (crate (name "tcoi") (vers "0.2.4") (hash "1a1miid77bhsqddsyyszvnlbjlx9wqsv0k3bqxp4vzr1x6g76dwp") (yanked #t)))

