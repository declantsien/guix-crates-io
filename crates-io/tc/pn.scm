(define-module (crates-io tc pn) #:use-module (crates-io))

(define-public crate-tcpnet-0.1 (crate (name "tcpnet") (vers "0.1.0") (deps (list (crate-dep (name "tokio") (req "^1.32.0") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "02p0df9prhkzi0pv2lw3dzjn7g3s2vswfd0qxlinspggbrrd2vhk") (yanked #t)))

(define-public crate-tcpnet-0.1 (crate (name "tcpnet") (vers "0.1.1") (deps (list (crate-dep (name "tokio") (req "^1.32.0") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "06aa990fjh9rhfh9q8zhlm94yimc8zzjai5jgb3w4qch74mdb49k")))

