(define-module (crates-io gr db) #:use-module (crates-io))

(define-public crate-grdb_orm_lib-0.1 (crate (name "grdb_orm_lib") (vers "0.1.0") (deps (list (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "13m08mxkg46zwf5762gqfzqmpc9s0jzs5k0s760cv8vhb7mfdzpw")))

(define-public crate-grdb_orm_lib-0.1 (crate (name "grdb_orm_lib") (vers "0.1.1") (deps (list (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "toml") (req "^0.5") (default-features #t) (kind 0)))) (hash "018b7d3h8rk54v3j3ngk0jsv80m6ylrhi8rq7vvscmpkvhyllsfi")))

(define-public crate-grdb_orm_lib-0.1 (crate (name "grdb_orm_lib") (vers "0.1.3") (deps (list (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "toml") (req "^0.5") (default-features #t) (kind 0)))) (hash "0gi9vcz49d2mak4pp7ni0i3wliy9ln33kxn105p0i5h7d7dcxm4l")))

(define-public crate-grdb_orm_lib-0.1 (crate (name "grdb_orm_lib") (vers "0.1.4") (deps (list (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "toml") (req "^0.5") (default-features #t) (kind 0)))) (hash "1sc3hdg0h7f18ymk1jhqhd8ingfmg1gkrl9prxay6ilc626mzcym")))

(define-public crate-grdb_orm_lib-0.1 (crate (name "grdb_orm_lib") (vers "0.1.5") (deps (list (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "toml") (req "^0.5") (default-features #t) (kind 0)))) (hash "1d8vrph08vbj983j7lnfdplkggb99s1s58qdq603vyirw096sdmp")))

(define-public crate-grdb_orm_lib-0.1 (crate (name "grdb_orm_lib") (vers "0.1.2") (deps (list (crate-dep (name "serde") (req "^1.0.164") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "toml") (req "^0.7.4") (default-features #t) (kind 0)))) (hash "1rwr8zi3dz7sq04v2xynsdi7h8n67461nwygl043pj8l2xzhq6c8")))

(define-public crate-grdb_orm_lib-0.1 (crate (name "grdb_orm_lib") (vers "0.1.6") (deps (list (crate-dep (name "serde") (req "^1.0.193") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "toml") (req "^0.8.8") (default-features #t) (kind 0)))) (hash "144dmkx0qxbfiz0y7mgk0x2ir7fhs8mn9b8gzpv3lrmfsxgizj93")))

