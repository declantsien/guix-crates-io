(define-module (crates-io gr nx) #:use-module (crates-io))

(define-public crate-grnx_rust_pkg-0.1 (crate (name "grnx_rust_pkg") (vers "0.1.0") (deps (list (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)))) (hash "0l3pphgnzm743mrz7ivrdydx3ysb49w11lsh4svn99ivq592b5gg") (yanked #t)))

