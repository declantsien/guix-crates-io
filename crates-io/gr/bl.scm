(define-module (crates-io gr bl) #:use-module (crates-io))

(define-public crate-grbli-0.1 (crate (name "grbli") (vers "0.1.0") (deps (list (crate-dep (name "log") (req "^0.4.16") (default-features #t) (kind 0)) (crate-dep (name "serialport") (req "^4.0.1") (default-features #t) (kind 0)))) (hash "0pvc7gh4mbzn60ivzx24pyp3h9k837vlxmk86r1xmhbqbnm0dq91")))

