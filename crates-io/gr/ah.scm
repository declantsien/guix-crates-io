(define-module (crates-io gr ah) #:use-module (crates-io))

(define-public crate-graham-0.0.1 (crate (name "graham") (vers "0.0.1") (hash "0ggwk1dck5d40ig87xgqbqd17wkx83b1drnqsb965a0mrc6kdkvq")))

(define-public crate-graham-number-0.0.0 (crate (name "graham-number") (vers "0.0.0") (deps (list (crate-dep (name "num") (req "^0.4.0") (default-features #t) (kind 0)) (crate-dep (name "power-mod") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "10ylhcl3n1623g8l8jjnz6l5wazdmia4n60qbilcvlwngiif7v2d") (features (quote (("default"))))))

(define-public crate-graham-number-1 (crate (name "graham-number") (vers "1.0.0") (deps (list (crate-dep (name "num") (req "^0.4.0") (default-features #t) (kind 0)))) (hash "14v4c8ssj9mipy0prksvc462khs24v2crimd2g901kb2r77qjdp0") (features (quote (("default"))))))

(define-public crate-graham-number-0.1 (crate (name "graham-number") (vers "0.1.0") (deps (list (crate-dep (name "num") (req "^0.4.0") (default-features #t) (kind 0)))) (hash "1qab0gbcyvrng85ipmlz6vi961fwp8spiyfn1y8mjl2kmndq3bc8") (features (quote (("default"))))))

