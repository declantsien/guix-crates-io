(define-module (crates-io gr un) #:use-module (crates-io))

(define-public crate-grun-0.1 (crate (name "grun") (vers "0.1.0") (hash "1yl1yvbj4rnlngssjbj0b8cha3zhqzg8d2lf63ks36rxabm22fg7")))

(define-public crate-grunt-0.0.1 (crate (name "grunt") (vers "0.0.1") (deps (list (crate-dep (name "serenity") (req "^0.6") (default-features #t) (kind 0)))) (hash "1qp4z7z1w6kmsz8sl7aba4ws96a0yp9cr37jggs9pnjj088n1dp8") (yanked #t)))

(define-public crate-grunt-0.0.5 (crate (name "grunt") (vers "0.0.5") (hash "1pia1i2ns3780nv8gfjr0vf5kkbl0rhxcp623agj91lhwh24w2rc") (yanked #t)))

(define-public crate-grunt-0.0.7 (crate (name "grunt") (vers "0.0.7") (hash "06815m05kb7phcy2fjm1pflryff44jql18bdyzyd4wvrgi423n15") (yanked #t)))

(define-public crate-grunt-0.1 (crate (name "grunt") (vers "0.1.1") (hash "1wqw8xkrx28h938p5syp7z5ipdf5lphyjw8m1277wi51q5n2d9b7") (yanked #t)))

(define-public crate-grunt-0.1 (crate (name "grunt") (vers "0.1.3") (hash "0hj1l2r241v0jhkzii00g78zid436dz11w6r8zbam93mavmxbql9") (yanked #t)))

(define-public crate-grunt-0.1 (crate (name "grunt") (vers "0.1.4") (hash "0a849a4bqa9bbpbwbzwhzqn1zxn61b5znskdirwgs47syacixrzs") (yanked #t)))

(define-public crate-grunt-0.1 (crate (name "grunt") (vers "0.1.77") (hash "1vwyyp0mfww9zv31hz78sj0b99hh1wmyqvggxg97rnlsvgnpyi03") (yanked #t)))

(define-public crate-grunt-0.77 (crate (name "grunt") (vers "0.77.0") (hash "0ar1wipi7bw0kps7bvyzh2x6syhz7ic9mg9svllvz153992h7mv4") (yanked #t)))

(define-public crate-grunt-7 (crate (name "grunt") (vers "7.7.18") (hash "1scqssyn5jaxscnv8xkivgzw0yp494a9gb50hx08z2nlkm3di6h1") (yanked #t)))

(define-public crate-grunt-2018 (crate (name "grunt") (vers "2018.7.7") (hash "0wg0z65kfq8kirhf06880xnar5dgc1a83qmf5s3v6bqbi2yv972a") (yanked #t)))

(define-public crate-grunt-2019 (crate (name "grunt") (vers "2019.12.13") (hash "0xmrxrdw6124mf6ayx5psd85g9m6mi665bm989khfafal393h32j") (yanked #t)))

(define-public crate-grunt-9999 (crate (name "grunt") (vers "9999.999.99") (hash "1vz2ii1c0pczyshn5g5mfmziil0r02gc8f6iwz5pjmxwbfl0d4np") (yanked #t)))

(define-public crate-grunt-9 (crate (name "grunt") (vers "9.9.9") (hash "0p2lh5qwc7ghmprz72cx14vzkmz8bz4k2l9rdpdnqnfshy36vp0l") (yanked #t)))

(define-public crate-grunt-99999 (crate (name "grunt") (vers "99999.99999.99999") (hash "1hxjs01yjhbmfwnjch5z8bjksw4nrivf7sf3rpn0vxvxi4869fg6") (yanked #t)))

(define-public crate-grunt-9999999 (crate (name "grunt") (vers "9999999.9999999.9999999") (hash "0zy07fslmjz2gscvj07ywcv4xhhr0945wywzafw03di1p8z6n4wa") (yanked #t)))

(define-public crate-grunt-999999999 (crate (name "grunt") (vers "999999999.999999999.999999999") (hash "028d919zf5rkz6kn5f3f79m9w1irm0zbl9zr5fdcxs8vh3chgiw1")))

