(define-module (crates-io gr um) #:use-module (crates-io))

(define-public crate-grumpkin-0.0.0 (crate (name "grumpkin") (vers "0.0.0") (hash "1cfwwmbs325nqqv89y7fgi1vr341ygx5afc4db0hm2jfxwaqhhxg")))

(define-public crate-grumpkin-msm-0.1 (crate (name "grumpkin-msm") (vers "0.1.0") (deps (list (crate-dep (name "blst") (req "~0.3.11") (default-features #t) (kind 0)) (crate-dep (name "cc") (req "^1.0.70") (default-features #t) (kind 1)) (crate-dep (name "criterion") (req "^0.3") (features (quote ("html_reports"))) (default-features #t) (kind 2)) (crate-dep (name "halo2curves") (req "^0.4.0") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0") (default-features #t) (kind 0)) (crate-dep (name "rand_chacha") (req "^0") (default-features #t) (kind 0)) (crate-dep (name "rayon") (req "^1.5") (default-features #t) (kind 0)) (crate-dep (name "sppark") (req "~0.1.2") (default-features #t) (kind 0)) (crate-dep (name "which") (req "^4.0") (default-features #t) (kind 1)))) (hash "1q9d1vj3cad16akx001x2csxc9jf5fa18dj027fg40nacml0gi0m") (features (quote (("portable" "blst/portable") ("force-adx" "blst/force-adx") ("default") ("cuda-mobile"))))))

