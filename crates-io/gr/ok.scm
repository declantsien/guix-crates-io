(define-module (crates-io gr ok) #:use-module (crates-io))

(define-public crate-grok-0.1 (crate (name "grok") (vers "0.1.0") (deps (list (crate-dep (name "regex") (req "^0.2") (default-features #t) (kind 0)))) (hash "1ml7qrdw0blljk0cw4zdhnlh8myqb8rqij18jvs63ypb0cr4p7i1")))

(define-public crate-grok-0.2 (crate (name "grok") (vers "0.2.0") (deps (list (crate-dep (name "regex") (req "^0.2") (default-features #t) (kind 0)))) (hash "1jcn7gzygd0k4hwfg2zg1cy0qz8fydy6j3hr84ddwkab2kl2bff3")))

(define-public crate-grok-0.3 (crate (name "grok") (vers "0.3.0") (deps (list (crate-dep (name "glob") (req "^0.2") (default-features #t) (kind 1)) (crate-dep (name "onig") (req "^1.6") (default-features #t) (kind 0)))) (hash "0bbrhivywb2vds0jr46pd2isay2jnjrpdrhggdd8s34c9yrrik9v")))

(define-public crate-grok-0.4 (crate (name "grok") (vers "0.4.0") (deps (list (crate-dep (name "glob") (req "^0.2") (default-features #t) (kind 1)) (crate-dep (name "onig") (req "^2.0") (default-features #t) (kind 0)))) (hash "0rjk2n218a4r9y4mk4l5p1js5svjxhwhf6azf28m3r5ypx0kxqy2")))

(define-public crate-grok-0.4 (crate (name "grok") (vers "0.4.1") (deps (list (crate-dep (name "glob") (req "^0.2") (default-features #t) (kind 1)) (crate-dep (name "onig") (req "^2.0") (default-features #t) (kind 0)))) (hash "0vqycgc6dgiinnvm0jwqf7clik83c11nbszz9rhg1dxbjgp295kk")))

(define-public crate-grok-0.5 (crate (name "grok") (vers "0.5.0") (deps (list (crate-dep (name "glob") (req "^0.2") (default-features #t) (kind 1)) (crate-dep (name "onig") (req "^3.1") (default-features #t) (kind 0)))) (hash "0nhb90ibwxg435jqhd11qx54psjqw1xjskgcgf7skg50ibwrmj16")))

(define-public crate-grok-1 (crate (name "grok") (vers "1.0.0") (deps (list (crate-dep (name "glob") (req "^0.3") (default-features #t) (kind 1)) (crate-dep (name "onig") (req "^4.3") (default-features #t) (kind 0)))) (hash "080jh7sasab1ivjz05l65kbyr320bm6ymspbgbm0pf8vrp6pw7vf")))

(define-public crate-grok-1 (crate (name "grok") (vers "1.1.0") (deps (list (crate-dep (name "glob") (req "^0.3") (default-features #t) (kind 1)) (crate-dep (name "onig") (req "^5.0") (default-features #t) (kind 0)))) (hash "03kc5lv704sliypk740ians4271q65sphns20yrqqjd4b2y49ywh")))

(define-public crate-grok-1 (crate (name "grok") (vers "1.0.1") (deps (list (crate-dep (name "glob") (req "^0.3") (default-features #t) (kind 1)) (crate-dep (name "onig") (req "^4.3") (default-features #t) (kind 0)))) (hash "1fbia69yz6fzzkkn3i9cqyfraraiky6saivmci7xjdhms6gmicrc")))

(define-public crate-grok-1 (crate (name "grok") (vers "1.2.0") (deps (list (crate-dep (name "glob") (req "^0.3") (default-features #t) (kind 1)) (crate-dep (name "onig") (req "^6.1") (default-features #t) (kind 0)))) (hash "0w16c0l3jygipw8y3w117smjx7g7jfgdpkdw7ywi2g7j7dfbn3w4")))

(define-public crate-grok-2 (crate (name "grok") (vers "2.0.0") (deps (list (crate-dep (name "glob") (req "^0.3") (default-features #t) (kind 1)) (crate-dep (name "onig") (req "^6.3") (kind 0)))) (hash "1gfzgnaa3cm1jrnfyglgwqgvja5aqhzn28hh0xrha9v0h6b9fdr7") (rust-version "1.56")))

(define-public crate-grokj2k-sys-0.1 (crate (name "grokj2k-sys") (vers "0.1.0") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "pkg-config") (req "^0.3.19") (default-features #t) (kind 1)))) (hash "12nf15xlbvx02z9if8lxlin75blbnk9jm9x911vyjvjbvw50kvhq") (links "grokj2k")))

(define-public crate-grokj2k-sys-0.1 (crate (name "grokj2k-sys") (vers "0.1.1") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "pkg-config") (req "^0.3.19") (default-features #t) (kind 1)))) (hash "10b6i9n5kr5a5pcp49px9b6mgy975qdc4v3f0i6ax4w47j1ia4dw") (links "grokj2k")))

