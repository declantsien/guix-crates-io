(define-module (crates-io gr ey) #:use-module (crates-io))

(define-public crate-grey-0.1 (crate (name "grey") (vers "0.1.0") (hash "09n1li001zg4n25iairp8zwkshh9zpv1g0d5cglzdwmpnblwi02j")))

(define-public crate-grey-0.1 (crate (name "grey") (vers "0.1.1") (hash "0hkcws5nbj0jx2xs429hlcgh3l8xl8v0aqjvrc3lkpgsvhrychk4")))

(define-public crate-greyhawk-vm-0.0.1 (crate (name "greyhawk-vm") (vers "0.0.1-alpha") (deps (list (crate-dep (name "getopts") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "0.1.*") (default-features #t) (kind 0)) (crate-dep (name "yaml-rust") (req "^0.2.2") (default-features #t) (kind 0)))) (hash "0bi6j26jawldv7xrgl217fhb0c79rdpfgxm7wcg55j9hhq898l7g") (yanked #t)))

(define-public crate-greyhound-0.0.0 (crate (name "greyhound") (vers "0.0.0") (deps (list (crate-dep (name "async-h1") (req "^2.3.3") (default-features #t) (kind 0)) (crate-dep (name "async-std") (req "^1.12") (features (quote ("attributes"))) (default-features #t) (kind 0)) (crate-dep (name "http-types") (req "^2.12") (default-features #t) (kind 0)))) (hash "090rin322si707m3b8gqzf4gk460cjy18q9c63f7x8sq6dhfxghm")))

(define-public crate-greyhound-0.0.1 (crate (name "greyhound") (vers "0.0.1") (deps (list (crate-dep (name "async-h1") (req "^2.3.3") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "async-std") (req "^1.12") (features (quote ("attributes"))) (default-features #t) (kind 0)) (crate-dep (name "async-trait") (req "^0.1.73") (default-features #t) (kind 0)) (crate-dep (name "futures-util") (req "^0.3.28") (default-features #t) (kind 0)) (crate-dep (name "http-types") (req "^2.12") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.20") (default-features #t) (kind 0)) (crate-dep (name "routefinder") (req "^0.5.3") (default-features #t) (kind 0)))) (hash "0vrqf018wqjhgf2s2a6s5xdkp3cdbfcj2v3i89qvq0lnz2nrxkbf") (features (quote (("h1-server" "async-h1") ("default" "h1-server"))))))

