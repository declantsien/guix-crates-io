(define-module (crates-io gr ev) #:use-module (crates-io))

(define-public crate-grev-0.1 (crate (name "grev") (vers "0.1.0") (deps (list (crate-dep (name "anyhow") (req "^1.0.68") (default-features #t) (kind 0)))) (hash "08wak54c63h9i4359kp8mjfpgkvp7fc7i478j31qklgvy9cz9x2b")))

(define-public crate-grev-0.1 (crate (name "grev") (vers "0.1.1") (deps (list (crate-dep (name "anyhow") (req "^1.0.68") (default-features #t) (kind 0)))) (hash "1hs2ykwz7qq77yvwiscvbsxv7np3qdcrpb81fbis8yb868ajkplf")))

(define-public crate-grev-0.1 (crate (name "grev") (vers "0.1.2") (deps (list (crate-dep (name "anyhow") (req "^1.0.68") (default-features #t) (kind 0)))) (hash "0p7axi5cqlagp589rmrvil61jld4ki0gdqxdl46d1i8r5grg9zci")))

(define-public crate-grev-0.1 (crate (name "grev") (vers "0.1.3") (deps (list (crate-dep (name "anyhow") (req "^1.0.68") (default-features #t) (kind 0)))) (hash "13glhhbvqp727isflfy1cdspxq8c4yzs0rbai80z72w2ldiz782f")))

