(define-module (crates-io gr aa) #:use-module (crates-io))

(define-public crate-graaf-0.1 (crate (name "graaf") (vers "0.1.0") (deps (list (crate-dep (name "divan") (req "^0.1.14") (default-features #t) (kind 2)))) (hash "0lhnk4axx6642kz89l1acfckzf762ph1id9pcgqzkbb6vmc3j491")))

(define-public crate-graaf-0.2 (crate (name "graaf") (vers "0.2.0") (deps (list (crate-dep (name "divan") (req "^0.1.14") (default-features #t) (kind 2)))) (hash "0wbc8ihrg333ixq4wdnz8dzrw5hk0hi51f4qvrsdpk6aqis6z8yh")))

(define-public crate-graaf-0.2 (crate (name "graaf") (vers "0.2.1") (deps (list (crate-dep (name "divan") (req "^0.1.14") (default-features #t) (kind 2)))) (hash "00jyl9aax3dyzq1lib6ac09y593x7wlrq8yywykyqg4xm5jc9q59")))

(define-public crate-graaf-0.2 (crate (name "graaf") (vers "0.2.2") (deps (list (crate-dep (name "divan") (req "^0.1.14") (default-features #t) (kind 2)))) (hash "1ix5xcjn20j97zgrqx8xd9z2rjznzwyyrhb5h27gaqxxgi8rinwg")))

(define-public crate-graaf-0.2 (crate (name "graaf") (vers "0.2.3") (deps (list (crate-dep (name "divan") (req "^0.1.14") (default-features #t) (kind 2)))) (hash "0wl1gc2svf1rniwkb9fbh0iqf75k6dyv1zjv8hcj6ic7km27kx6a")))

(define-public crate-graaf-0.3 (crate (name "graaf") (vers "0.3.0") (deps (list (crate-dep (name "divan") (req "^0.1.14") (default-features #t) (kind 2)))) (hash "0b9pxaagypdca5gg5x0kp1hycal0yjfzdfs96j8xalvn2b442nnr")))

(define-public crate-graaf-0.3 (crate (name "graaf") (vers "0.3.1") (deps (list (crate-dep (name "divan") (req "^0.1.14") (default-features #t) (kind 2)))) (hash "09l2hgi5qkn7fs1c9w87h5hqilrm4i2ijlxj2vgpkwfpz116frw3")))

(define-public crate-graaf-0.3 (crate (name "graaf") (vers "0.3.2") (deps (list (crate-dep (name "divan") (req "^0.1.14") (default-features #t) (kind 2)))) (hash "0n33a07zmbi60fsdnn1a46x167hrq2hsna542vij68rhndgicw8n")))

(define-public crate-graaf-0.3 (crate (name "graaf") (vers "0.3.3") (deps (list (crate-dep (name "divan") (req "^0.1.14") (default-features #t) (kind 2)))) (hash "0vqfshx31s3zr15iqks4mlqzmpagzm1kz3kjid97gx125b68x87v")))

(define-public crate-graaf-0.4 (crate (name "graaf") (vers "0.4.0") (deps (list (crate-dep (name "divan") (req "^0.1.14") (default-features #t) (kind 2)))) (hash "1cnq4d7dndjkf98xgjidm1vrrcjp1l6afcvlv7z1hp1slh99jhj8")))

(define-public crate-graaf-0.4 (crate (name "graaf") (vers "0.4.1") (deps (list (crate-dep (name "divan") (req "^0.1.14") (default-features #t) (kind 2)))) (hash "0g2yb0jsqfw400gjlsx6y9kh9d9gn88s0i5l7rr6ky8mnr1y2bc1")))

(define-public crate-graaf-0.4 (crate (name "graaf") (vers "0.4.2") (deps (list (crate-dep (name "divan") (req "^0.1.14") (default-features #t) (kind 2)))) (hash "1rycnls6h7d57h25x59x0br2cfxjxhybzjg08j2akrgqxfbl010l")))

(define-public crate-graaf-0.5 (crate (name "graaf") (vers "0.5.0") (deps (list (crate-dep (name "divan") (req "^0.1.14") (default-features #t) (kind 2)))) (hash "168y5gvfbgdismljyc723w8mq6wj5ij73k774x9fjfiwsx1dhv3d")))

(define-public crate-graaf-0.5 (crate (name "graaf") (vers "0.5.1") (deps (list (crate-dep (name "divan") (req "^0.1.14") (default-features #t) (kind 2)))) (hash "0b0dl7l7z7yl5x49s9mcs2b36656fwca1pw1qxj6cn4zwhw3vn0c")))

(define-public crate-graaf-0.5 (crate (name "graaf") (vers "0.5.2") (deps (list (crate-dep (name "divan") (req "^0.1.14") (default-features #t) (kind 2)))) (hash "09907jkm99x27i2jv6gh6ynxd630v63sag0byqxhqwgib7cllgxi")))

(define-public crate-graaf-0.5 (crate (name "graaf") (vers "0.5.3") (deps (list (crate-dep (name "divan") (req "^0.1.14") (default-features #t) (kind 2)))) (hash "0gm27x9f4m73n6g3ps8a9q212pmvx602ahj9z1q0grlyfpiwq8i0")))

(define-public crate-graaf-0.6 (crate (name "graaf") (vers "0.6.0") (deps (list (crate-dep (name "divan") (req "^0.1.14") (default-features #t) (kind 2)))) (hash "162y0jbpps7ibahdgbxrq33a7zlh3dk74fy1nv9296i221igxcwk")))

(define-public crate-graaf-0.6 (crate (name "graaf") (vers "0.6.1") (deps (list (crate-dep (name "divan") (req "^0.1.14") (default-features #t) (kind 2)))) (hash "1gf7p7i32bzb8b7f3ssqxcb7pq7nr2v6p6wn2lfma399mqy8pxzx")))

(define-public crate-graaf-0.6 (crate (name "graaf") (vers "0.6.2") (deps (list (crate-dep (name "divan") (req "^0.1.14") (default-features #t) (kind 2)))) (hash "1cizqrnmqgmjggqfg5n504vnkhnpf1q3q4k1ii2yi8qzy1209c5h")))

(define-public crate-graaf-0.6 (crate (name "graaf") (vers "0.6.3") (deps (list (crate-dep (name "divan") (req "^0.1.14") (default-features #t) (kind 2)))) (hash "02cqyf93j0gj91pw9afi1w0394yv6zya1n74011j6b8csyqic1p4")))

(define-public crate-graaf-0.7 (crate (name "graaf") (vers "0.7.0") (deps (list (crate-dep (name "divan") (req "^0.1.14") (default-features #t) (kind 2)))) (hash "0qrw7pifjl7jzp18j1s0x192074f9vi1zshvdfhxnwvxgbq7hkd0")))

(define-public crate-graaf-0.8 (crate (name "graaf") (vers "0.8.0") (deps (list (crate-dep (name "divan") (req "^0.1.14") (default-features #t) (kind 2)))) (hash "10l6gk6acz6jldbs4mf508f4wadnvz06x1fkakhprrgi0rl8rf01")))

(define-public crate-graaf-0.8 (crate (name "graaf") (vers "0.8.1") (deps (list (crate-dep (name "divan") (req "^0.1.14") (default-features #t) (kind 2)))) (hash "00mbfcf8al3swn5ff1bl0fap7jyrl5wz90vc9y71v822v8hp8pzk")))

(define-public crate-graaf-0.8 (crate (name "graaf") (vers "0.8.2") (deps (list (crate-dep (name "divan") (req "^0.1.14") (default-features #t) (kind 2)))) (hash "0h3zzys3pqzi19rpyfrzgbxad4hgk4hkhhg7hii8x7f081yhfr67")))

(define-public crate-graaf-0.8 (crate (name "graaf") (vers "0.8.3") (deps (list (crate-dep (name "divan") (req "^0.1.14") (default-features #t) (kind 2)))) (hash "0w5439n4l47bhiphmykxmhyr5z408ijck8qqhsw778r2m6p0ni0w")))

(define-public crate-graaf-0.8 (crate (name "graaf") (vers "0.8.4") (deps (list (crate-dep (name "divan") (req "^0.1.14") (default-features #t) (kind 2)))) (hash "0pkarpd6xi05bjv62586dqqdsmjdzzn0rsvjnn4nn4d3r8dm6p35")))

(define-public crate-graaf-0.9 (crate (name "graaf") (vers "0.9.0") (deps (list (crate-dep (name "divan") (req "^0.1.14") (default-features #t) (kind 2)))) (hash "1pm5569gs783v61cf3wcn050rws74sdl1gwcxh6lkkzffsl2x7b2")))

(define-public crate-graaf-0.10 (crate (name "graaf") (vers "0.10.0") (deps (list (crate-dep (name "divan") (req "^0.1.14") (default-features #t) (kind 2)))) (hash "1d4icaw0m05lfan0542vsprg2z7j55ph6a71g5gr9jgz7vak6pd6")))

(define-public crate-graaf-0.11 (crate (name "graaf") (vers "0.11.0") (deps (list (crate-dep (name "divan") (req "^0.1.14") (default-features #t) (kind 2)))) (hash "0l7rqhhh340ycl21lyb2lbff25jnd5ban6k6g0zbdmj569al5faf")))

(define-public crate-graaf-0.11 (crate (name "graaf") (vers "0.11.1") (deps (list (crate-dep (name "divan") (req "^0.1.14") (default-features #t) (kind 2)))) (hash "0ph8lcfnh0hl9rq13zjdjj0i8g2s51vs6mr4c3v77pm6fyaag65z")))

(define-public crate-graaf-0.12 (crate (name "graaf") (vers "0.12.0") (deps (list (crate-dep (name "divan") (req "^0.1.14") (default-features #t) (kind 2)))) (hash "0a4r94zmg5xjmwq0v64ng5wqfyyipada1fh51fl6w400xxq93zcc")))

(define-public crate-graaf-0.12 (crate (name "graaf") (vers "0.12.1") (deps (list (crate-dep (name "divan") (req "^0.1.14") (default-features #t) (kind 2)))) (hash "0q8ahyw01snn3m01rq1v1r0s1zlfpn5zsagfm4vzf5dgk69a0yrf")))

(define-public crate-graaf-0.13 (crate (name "graaf") (vers "0.13.0") (deps (list (crate-dep (name "divan") (req "^0.1.14") (default-features #t) (kind 2)))) (hash "1hwy0a5d626rpk02i7jfliapldn0zr6icb9h0ph7c8fl5fn34166")))

(define-public crate-graaf-0.13 (crate (name "graaf") (vers "0.13.1") (deps (list (crate-dep (name "divan") (req "^0.1.14") (default-features #t) (kind 2)))) (hash "0qdhvik40hhhwa5wndvq7q7di0v8fs9dg8j2jfl4sysr7ld1f58a")))

(define-public crate-graaf-0.13 (crate (name "graaf") (vers "0.13.2") (deps (list (crate-dep (name "divan") (req "^0.1.14") (default-features #t) (kind 2)))) (hash "1i6fl9rfbgnqw1pf8zvjkpz0pp4gsi3vxrqjshh43q8ybiljrvbf")))

(define-public crate-graaf-0.13 (crate (name "graaf") (vers "0.13.3") (deps (list (crate-dep (name "divan") (req "^0.1.14") (default-features #t) (kind 2)))) (hash "03bxpksc0nm3fhxkvphbbapiqri13jkay78hpc2hid3bdis9hpvv")))

(define-public crate-graaf-0.14 (crate (name "graaf") (vers "0.14.0") (deps (list (crate-dep (name "divan") (req "^0.1.14") (default-features #t) (kind 2)))) (hash "15dih712czx7kn2rvdicj3l8j13z94akgj7an32ngzj8r700p9ip")))

(define-public crate-graaf-0.14 (crate (name "graaf") (vers "0.14.1") (deps (list (crate-dep (name "divan") (req "^0.1.14") (default-features #t) (kind 2)))) (hash "0viai0ppvnyrph27fpyi4qchlp3dw7pm43a6ci0dbz8cwy34g6c4")))

(define-public crate-graaf-0.14 (crate (name "graaf") (vers "0.14.2") (deps (list (crate-dep (name "divan") (req "^0.1.14") (default-features #t) (kind 2)))) (hash "022z92f19gs7d8f7j904sx4zzmk841l634h9sbsniis9fryz6m64")))

(define-public crate-graaf-0.15 (crate (name "graaf") (vers "0.15.0") (deps (list (crate-dep (name "divan") (req "^0.1.14") (default-features #t) (kind 2)))) (hash "0jw2fxxx8zf9032fl3zmlfkd56j4772y47ls24n0mpx6zwckx4gs")))

(define-public crate-graaf-0.15 (crate (name "graaf") (vers "0.15.1") (deps (list (crate-dep (name "divan") (req "^0.1.14") (default-features #t) (kind 2)))) (hash "1jz8cavln428phw2fj8bb0abnrh86gdj1kqhr307z6vk5hr0brll")))

(define-public crate-graaf-0.16 (crate (name "graaf") (vers "0.16.0") (deps (list (crate-dep (name "divan") (req "^0.1.14") (default-features #t) (kind 2)))) (hash "10rcrzahh35l8bd30x1arhnbbciiiczwngpf7h90md9s344n59h1")))

(define-public crate-graaf-0.16 (crate (name "graaf") (vers "0.16.1") (deps (list (crate-dep (name "divan") (req "^0.1.14") (default-features #t) (kind 2)))) (hash "1mbmknnvs8hzssibpwg1z4daw1m7f3ayvcvrm54awd9d5j51jpks")))

(define-public crate-graaf-0.17 (crate (name "graaf") (vers "0.17.0") (deps (list (crate-dep (name "divan") (req "^0.1.14") (default-features #t) (kind 2)))) (hash "1kl63aixvb137na1m6gfysvl9aqxhr9fgd4j0z7bxw0kky61y84s")))

(define-public crate-graaf-0.17 (crate (name "graaf") (vers "0.17.1") (deps (list (crate-dep (name "divan") (req "^0.1.14") (default-features #t) (kind 2)))) (hash "1lc7x1xzrakf900kvnp1sjy77kbfa45cb4rrr2zm0px5b26fqp3c")))

(define-public crate-graaf-0.17 (crate (name "graaf") (vers "0.17.2") (deps (list (crate-dep (name "divan") (req "^0.1.14") (default-features #t) (kind 2)))) (hash "1hp8rnj07c4qijhbzfwkrgsdc7g7zykv87gdfla64c7f23q9ww33")))

(define-public crate-graaf-0.18 (crate (name "graaf") (vers "0.18.0") (deps (list (crate-dep (name "divan") (req "^0.1.14") (default-features #t) (kind 2)))) (hash "0811wz4pw5v79j6acdinmcrj0nv0ln9w7jgxn82pxg3wk7a7wwci") (features (quote (("default" "adjacency_matrix") ("adjacency_matrix"))))))

(define-public crate-graaf-0.19 (crate (name "graaf") (vers "0.19.0") (deps (list (crate-dep (name "divan") (req "^0.1.14") (default-features #t) (kind 2)))) (hash "16m0v7qcs35xbfcs7pj4glm44y0v69p53q9414zamg3qwgk7mc9z") (features (quote (("default" "adjacency_matrix") ("adjacency_matrix"))))))

(define-public crate-graaf-0.20 (crate (name "graaf") (vers "0.20.0") (deps (list (crate-dep (name "divan") (req "^0.1.14") (default-features #t) (kind 2)))) (hash "1pqyn6z6qy8srznkvwdp4i2bp6yw6ricfvnn60sxvmi0xxssw9mb") (features (quote (("default" "adjacency_matrix") ("adjacency_matrix"))))))

(define-public crate-graaf-0.20 (crate (name "graaf") (vers "0.20.1") (deps (list (crate-dep (name "divan") (req "^0.1.14") (default-features #t) (kind 2)))) (hash "0m40r80dr1yb85ipqjvj7mnn0shlarhq3gj34d9b6vlfv7j241a2") (features (quote (("default" "adjacency_matrix") ("adjacency_matrix"))))))

(define-public crate-graaf-0.20 (crate (name "graaf") (vers "0.20.2") (deps (list (crate-dep (name "divan") (req "^0.1.14") (default-features #t) (kind 2)))) (hash "0vcs997d5cp1qxl561jh9kpsj8jvdhij73vniy2nqinag99dv2n4") (features (quote (("default" "adjacency_matrix") ("adjacency_matrix"))))))

(define-public crate-graaf-0.21 (crate (name "graaf") (vers "0.21.0") (deps (list (crate-dep (name "divan") (req "^0.1.14") (default-features #t) (kind 2)))) (hash "0911lmwk42jf2n4rpymlxsd6dn1jr83vn8wk1l12gw26mkwzx8n0") (features (quote (("nightly") ("adjacency_matrix" "nightly"))))))

(define-public crate-graaf-0.22 (crate (name "graaf") (vers "0.22.0") (deps (list (crate-dep (name "divan") (req "^0.1.14") (default-features #t) (kind 2)))) (hash "0lw4vkch4vxnw8brp6y0xdmjbf05vlk07dqw10qwggybcvgsjjhl") (features (quote (("nightly") ("default" "adjacency_matrix") ("adjacency_matrix" "nightly"))))))

(define-public crate-graaf-0.22 (crate (name "graaf") (vers "0.22.1") (deps (list (crate-dep (name "divan") (req "^0.1.14") (default-features #t) (kind 2)))) (hash "0jczr8ag3jhm7hlgsbpb5z2spn1ik1kay27cxjrldx9zqf760lk8") (features (quote (("nightly") ("default" "adjacency_matrix") ("adjacency_matrix" "nightly"))))))

(define-public crate-graaf-0.23 (crate (name "graaf") (vers "0.23.0") (deps (list (crate-dep (name "divan") (req "^0.1.14") (default-features #t) (kind 2)))) (hash "0hn1jbrm78p7shraly6rk9nq0k3sml3ax641p4f9mdgbyrdn1zql") (features (quote (("nightly") ("default" "adjacency_matrix") ("adjacency_matrix" "nightly"))))))

(define-public crate-graaf-0.23 (crate (name "graaf") (vers "0.23.1") (deps (list (crate-dep (name "divan") (req "^0.1.14") (default-features #t) (kind 2)))) (hash "19kwcn0cs0s1fdiqahchna7064w0bnql54vfkllxkrv0n002d90g") (features (quote (("nightly") ("default" "adjacency_matrix") ("adjacency_matrix" "nightly"))))))

(define-public crate-graaf-0.24 (crate (name "graaf") (vers "0.24.0") (deps (list (crate-dep (name "divan") (req "^0.1.14") (default-features #t) (kind 2)))) (hash "1rvvzsmkzss1qlndpklkwkbhb7arazg399ap7jdx65qwlzq62hwz") (features (quote (("nightly") ("default" "adjacency_matrix") ("adjacency_matrix" "nightly"))))))

(define-public crate-graaf-0.24 (crate (name "graaf") (vers "0.24.1") (deps (list (crate-dep (name "divan") (req "^0.1.14") (default-features #t) (kind 2)))) (hash "1ip1gds9k7vn42wbj2idbz5g0gaifj7xgdf6qslxq9q1yq0dr6iw") (features (quote (("nightly") ("default" "adjacency_matrix") ("adjacency_matrix" "nightly"))))))

(define-public crate-graaf-0.25 (crate (name "graaf") (vers "0.25.0") (deps (list (crate-dep (name "divan") (req "^0.1.14") (default-features #t) (kind 2)))) (hash "1i2j7h1s76ih2i00v8wqb5l4wgnkr38hy6yzaxzqwjrh6q4xpqml") (features (quote (("nightly") ("default" "adjacency_matrix") ("adjacency_matrix" "nightly"))))))

(define-public crate-graaf-0.25 (crate (name "graaf") (vers "0.25.1") (deps (list (crate-dep (name "divan") (req "^0.1.14") (default-features #t) (kind 2)))) (hash "1cfwp7r9ynipym1l5gjrxsz2js44gv3ajrqdnhhddrb51ndpfxyy") (features (quote (("nightly") ("default" "adjacency_matrix") ("adjacency_matrix" "nightly"))))))

(define-public crate-graaf-0.25 (crate (name "graaf") (vers "0.25.2") (deps (list (crate-dep (name "divan") (req "^0.1.14") (default-features #t) (kind 2)))) (hash "0n3s9sgry08mpl769rbr6jcqml501jnwcrczps5103rlf272q1nw") (features (quote (("nightly") ("default" "adjacency_matrix") ("adjacency_matrix" "nightly"))))))

(define-public crate-graaf-0.26 (crate (name "graaf") (vers "0.26.0") (deps (list (crate-dep (name "divan") (req "^0.1.14") (default-features #t) (kind 2)))) (hash "03li7v7lvah08f6a70cg35gfnlwlh1r6a7a95jzb6vz3a0phia9r") (features (quote (("default" "adjacency_matrix") ("adjacency_matrix"))))))

(define-public crate-graaf-0.26 (crate (name "graaf") (vers "0.26.1") (deps (list (crate-dep (name "divan") (req "^0.1.14") (default-features #t) (kind 2)))) (hash "08kirpaf47haqgjn8wcf6bvxv5n7dh5mw1nnl0zq84hb2zdf1pdj") (features (quote (("default" "adjacency_matrix") ("adjacency_matrix"))))))

(define-public crate-graaf-0.27 (crate (name "graaf") (vers "0.27.0") (deps (list (crate-dep (name "divan") (req "^0.1.14") (default-features #t) (kind 2)))) (hash "0qlchakc6xhvgyg78dvaqdzgm64qwr457i1lsy31w30hq51qnpgv") (features (quote (("default" "adjacency_matrix") ("adjacency_matrix"))))))

(define-public crate-graaf-0.27 (crate (name "graaf") (vers "0.27.1") (deps (list (crate-dep (name "divan") (req "^0.1.14") (default-features #t) (kind 2)))) (hash "142p6nl96bcwwv7g0wmfbpbncf482lvxvnpjxkf286l2aiy1m8bx") (features (quote (("default" "adjacency_matrix") ("adjacency_matrix"))))))

(define-public crate-graaf-0.28 (crate (name "graaf") (vers "0.28.0") (deps (list (crate-dep (name "divan") (req "^0.1.14") (default-features #t) (kind 2)))) (hash "1rbz105psgh1liya4kk0rbzwhmgq64p8nyskcr3ajzb8w41vwp8a") (features (quote (("default" "adjacency_matrix") ("adjacency_matrix"))))))

(define-public crate-graaf-0.28 (crate (name "graaf") (vers "0.28.1") (deps (list (crate-dep (name "divan") (req "^0.1.14") (default-features #t) (kind 2)))) (hash "01n64vjkv04zj8ykqr0scg6qlwdyc0bv823r33ibxl8b0s33kqqr") (features (quote (("default" "adjacency_matrix") ("adjacency_matrix"))))))

(define-public crate-graaf-0.28 (crate (name "graaf") (vers "0.28.2") (deps (list (crate-dep (name "divan") (req "^0.1.14") (default-features #t) (kind 2)))) (hash "0ypfpbi5drw8hifc5ns5z95p4mink0w4xxs0ra3bx0m6ywsjrmin") (features (quote (("default" "adjacency_matrix") ("adjacency_matrix"))))))

(define-public crate-graaf-0.29 (crate (name "graaf") (vers "0.29.0") (deps (list (crate-dep (name "divan") (req "^0.1.14") (default-features #t) (kind 2)) (crate-dep (name "proptest") (req "^1.4.0") (default-features #t) (kind 2)))) (hash "0j4w2knl4qybk496ip145vp1rys7j3f28b5xchwpxcvj278cblx6") (features (quote (("default" "adjacency_matrix") ("adjacency_matrix"))))))

(define-public crate-graaf-0.29 (crate (name "graaf") (vers "0.29.1") (deps (list (crate-dep (name "divan") (req "^0.1.14") (default-features #t) (kind 2)) (crate-dep (name "proptest") (req "^1.4.0") (default-features #t) (kind 2)))) (hash "1vr8dfnx6z5spjsqfnjmcc383f3fhp8h9ycggdlbcz1jgrb3cpwz") (features (quote (("default" "adjacency_matrix") ("adjacency_matrix"))))))

(define-public crate-graaf-0.30 (crate (name "graaf") (vers "0.30.0") (deps (list (crate-dep (name "divan") (req "^0.1.14") (default-features #t) (kind 2)) (crate-dep (name "proptest") (req "^1.4.0") (default-features #t) (kind 2)))) (hash "19kw915xd0ay1za5qsb4bf8b9zh5pfk7rh4myi8zvfr952n1s21q") (features (quote (("default" "adjacency_matrix") ("adjacency_matrix"))))))

(define-public crate-graaf-0.30 (crate (name "graaf") (vers "0.30.1") (deps (list (crate-dep (name "divan") (req "^0.1.14") (default-features #t) (kind 2)) (crate-dep (name "proptest") (req "^1.4.0") (default-features #t) (kind 2)))) (hash "0dig4h3761gxlgvv544yj6afbbri4d7v50j4py10pv8j90y5ib27") (features (quote (("default" "adjacency_matrix") ("adjacency_matrix"))))))

(define-public crate-graaf-0.30 (crate (name "graaf") (vers "0.30.2") (deps (list (crate-dep (name "divan") (req "^0.1.14") (default-features #t) (kind 2)) (crate-dep (name "proptest") (req "^1.4.0") (default-features #t) (kind 2)))) (hash "06y942cdwnj3nkdz7wvh117ps0p5jjpmz8c7acmcdsqzwcn68n74") (features (quote (("default" "adjacency_matrix") ("adjacency_matrix"))))))

(define-public crate-graaf-0.30 (crate (name "graaf") (vers "0.30.3") (deps (list (crate-dep (name "divan") (req "^0.1.14") (default-features #t) (kind 2)) (crate-dep (name "proptest") (req "^1.4.0") (default-features #t) (kind 2)))) (hash "1yz577cbrj1kcahbaznxjmcbch1nnr1123mfzjammrlfil7yc9n2") (features (quote (("default" "adjacency_matrix") ("adjacency_matrix"))))))

(define-public crate-graaf-0.30 (crate (name "graaf") (vers "0.30.4") (deps (list (crate-dep (name "divan") (req "^0.1.14") (default-features #t) (kind 2)) (crate-dep (name "proptest") (req "^1.4.0") (default-features #t) (kind 2)))) (hash "1m5l70bbv71f888r1qb5bhx6wbiskw5zh5ls4fib6x7cmwxjbvdp") (features (quote (("default" "adjacency_matrix") ("adjacency_matrix"))))))

(define-public crate-graaf-0.30 (crate (name "graaf") (vers "0.30.5") (deps (list (crate-dep (name "divan") (req "^0.1.14") (default-features #t) (kind 2)) (crate-dep (name "proptest") (req "^1.4.0") (default-features #t) (kind 2)))) (hash "0qflv8p2vbkdx5q0ki67z228038nz3pq43ly513iwb0bj0sqvdfv") (features (quote (("default" "adjacency_matrix") ("adjacency_matrix"))))))

(define-public crate-graaf-0.30 (crate (name "graaf") (vers "0.30.6") (deps (list (crate-dep (name "divan") (req "^0.1.14") (default-features #t) (kind 2)) (crate-dep (name "proptest") (req "^1.4.0") (default-features #t) (kind 2)))) (hash "1b3sdrf06ddllnaqiq7zb6igbap0nd9psi9r2la69dj2fb2pg77a") (features (quote (("default" "adjacency_matrix") ("adjacency_matrix"))))))

(define-public crate-graaf-0.30 (crate (name "graaf") (vers "0.30.7") (deps (list (crate-dep (name "divan") (req "^0.1.14") (default-features #t) (kind 2)) (crate-dep (name "proptest") (req "^1.4.0") (default-features #t) (kind 2)))) (hash "17dafhsz655qwr06xn3yg2yfkjqaird8m5b8hpm30p5zy2016lzg") (features (quote (("default" "adjacency_matrix") ("adjacency_matrix"))))))

(define-public crate-graaf-0.30 (crate (name "graaf") (vers "0.30.8") (deps (list (crate-dep (name "divan") (req "^0.1.14") (default-features #t) (kind 2)) (crate-dep (name "proptest") (req "^1.4.0") (default-features #t) (kind 2)))) (hash "014wfd2fjk2ql8yiqrbk162ajfbimc3igkqg2syy0vdk77kpip2w") (features (quote (("default" "adjacency_matrix") ("adjacency_matrix"))))))

(define-public crate-graaf-0.30 (crate (name "graaf") (vers "0.30.9") (deps (list (crate-dep (name "divan") (req "^0.1.14") (default-features #t) (kind 2)) (crate-dep (name "proptest") (req "^1.4.0") (default-features #t) (kind 2)))) (hash "047i6mwc2mczzmzksk3h22k4ax7m8nakw2v8a2sfqkkp36jav626") (features (quote (("default" "adjacency_matrix") ("adjacency_matrix"))))))

(define-public crate-graaf-0.31 (crate (name "graaf") (vers "0.31.0") (deps (list (crate-dep (name "divan") (req "^0.1.14") (default-features #t) (kind 2)) (crate-dep (name "proptest") (req "^1.4.0") (default-features #t) (kind 2)))) (hash "1fzdrwsls0sy47z8nm0ma2kr5z9mb53yppbn36hy1zlsr9bjrjvh") (features (quote (("default" "adjacency_matrix") ("adjacency_matrix"))))))

(define-public crate-graaf-0.32 (crate (name "graaf") (vers "0.32.0") (deps (list (crate-dep (name "divan") (req "^0.1.14") (default-features #t) (kind 2)) (crate-dep (name "proptest") (req "^1.4.0") (default-features #t) (kind 2)))) (hash "1aixzwa9cxv4zvf2nnyyqf7nl8zqsq6yx3z3v86r45ldq41aag2s") (features (quote (("default" "adjacency_matrix") ("adjacency_matrix"))))))

(define-public crate-graaf-0.32 (crate (name "graaf") (vers "0.32.1") (deps (list (crate-dep (name "divan") (req "^0.1.14") (default-features #t) (kind 2)) (crate-dep (name "proptest") (req "^1.4.0") (default-features #t) (kind 2)))) (hash "0xx2jy6cm9cvc9a1iz9a88ciai45clik70a2vpic2cci5cb896q2") (features (quote (("default" "adjacency_matrix") ("adjacency_matrix"))))))

(define-public crate-graaf-0.32 (crate (name "graaf") (vers "0.32.2") (deps (list (crate-dep (name "divan") (req "^0.1.14") (default-features #t) (kind 2)) (crate-dep (name "proptest") (req "^1.4.0") (default-features #t) (kind 2)))) (hash "1r94l4x32i7g6bkv40yy1560y49172ab0v7ivgf0nwzlri45zqii") (features (quote (("default" "adjacency_matrix") ("adjacency_matrix"))))))

(define-public crate-graaf-0.32 (crate (name "graaf") (vers "0.32.3") (deps (list (crate-dep (name "divan") (req "^0.1.14") (default-features #t) (kind 2)) (crate-dep (name "proptest") (req "^1.4.0") (default-features #t) (kind 2)))) (hash "1mad4vmf761lnw710kncrbc9qbwhmc9b9k3ah6z9li91pi68ansj") (features (quote (("default" "adjacency_matrix") ("adjacency_matrix"))))))

(define-public crate-graaf-0.33 (crate (name "graaf") (vers "0.33.0") (deps (list (crate-dep (name "divan") (req "^0.1.14") (default-features #t) (kind 2)) (crate-dep (name "proptest") (req "^1.4.0") (default-features #t) (kind 2)))) (hash "0andxsfqwx3l4gn5vcchpkfw3n4phlgjjw244cpip7m4q7gk9s1c") (features (quote (("default" "adjacency_matrix") ("adjacency_matrix"))))))

(define-public crate-graaf-0.33 (crate (name "graaf") (vers "0.33.1") (deps (list (crate-dep (name "divan") (req "^0.1.14") (default-features #t) (kind 2)) (crate-dep (name "proptest") (req "^1.4.0") (default-features #t) (kind 2)))) (hash "0ya48gfmdfbgml3jwmlbzya4mxgm8ac3frzs1hksvq2iy124n8vv") (features (quote (("default" "adjacency_matrix") ("adjacency_matrix"))))))

(define-public crate-graaf-0.34 (crate (name "graaf") (vers "0.34.0") (deps (list (crate-dep (name "divan") (req "^0.1.14") (default-features #t) (kind 2)) (crate-dep (name "proptest") (req "^1.4.0") (default-features #t) (kind 2)))) (hash "1mdzygi35x5sdzd79xzfz1a7mryk08y9595j4mkw39fqgcv8amjs") (features (quote (("default" "adjacency_matrix") ("adjacency_matrix"))))))

(define-public crate-graaf-0.34 (crate (name "graaf") (vers "0.34.1") (deps (list (crate-dep (name "divan") (req "^0.1.14") (default-features #t) (kind 2)) (crate-dep (name "proptest") (req "^1.4.0") (default-features #t) (kind 2)))) (hash "1p4l5kyfrhmc4vcyxna74xs3pp9953f796d440gkawq73x4bs908") (features (quote (("default" "adjacency_matrix") ("adjacency_matrix"))))))

(define-public crate-graaf-0.35 (crate (name "graaf") (vers "0.35.0") (deps (list (crate-dep (name "divan") (req "^0.1.14") (default-features #t) (kind 2)) (crate-dep (name "proptest") (req "^1.4.0") (default-features #t) (kind 2)))) (hash "1h5xvxwhshyqqfd91p8agzk2pzi0gcj625cximciqxxrxxy02zk9") (features (quote (("default" "adjacency_matrix") ("adjacency_matrix"))))))

(define-public crate-graaf-0.35 (crate (name "graaf") (vers "0.35.1") (deps (list (crate-dep (name "divan") (req "^0.1.14") (default-features #t) (kind 2)) (crate-dep (name "proptest") (req "^1.4.0") (default-features #t) (kind 2)))) (hash "1dghvby5ficf5g6j66lw8s026j45876h0ajciik6py4qqwygfn5q") (features (quote (("default" "adjacency_matrix") ("adjacency_matrix"))))))

(define-public crate-graaf-0.35 (crate (name "graaf") (vers "0.35.2") (deps (list (crate-dep (name "divan") (req "^0.1.14") (default-features #t) (kind 2)) (crate-dep (name "proptest") (req "^1.4.0") (default-features #t) (kind 2)))) (hash "1pcf7mq9sfjr907ky46hllawh6rrx3ic4kk3fsp20gq4fdh8mbbp") (features (quote (("default" "adjacency_matrix") ("adjacency_matrix"))))))

(define-public crate-graaf-0.35 (crate (name "graaf") (vers "0.35.3") (deps (list (crate-dep (name "divan") (req "^0.1.14") (default-features #t) (kind 2)) (crate-dep (name "proptest") (req "^1.4.0") (default-features #t) (kind 2)))) (hash "1msvfymqab6mj7bhbmjkzlpa2q4ccyv43kwb1arxwrrqxjc4yr3p") (features (quote (("default" "adjacency_matrix") ("adjacency_matrix"))))))

(define-public crate-graaf-0.35 (crate (name "graaf") (vers "0.35.4") (deps (list (crate-dep (name "divan") (req "^0.1.14") (default-features #t) (kind 2)) (crate-dep (name "proptest") (req "^1.4.0") (default-features #t) (kind 2)))) (hash "1yilgvx360fkjxdpv3djm9dzvsbs3lsmsz32d2skz0dq0h31yi4w") (features (quote (("default" "adjacency_matrix") ("adjacency_matrix"))))))

(define-public crate-graaf-0.35 (crate (name "graaf") (vers "0.35.5") (deps (list (crate-dep (name "divan") (req "^0.1.14") (default-features #t) (kind 2)) (crate-dep (name "proptest") (req "^1.4.0") (default-features #t) (kind 2)))) (hash "14y6wj6vv45p704cx58qlhw8r6aik65xm2alipzfzlzzdksmhqrk") (features (quote (("default" "adjacency_matrix") ("adjacency_matrix"))))))

(define-public crate-graaf-0.35 (crate (name "graaf") (vers "0.35.6") (deps (list (crate-dep (name "divan") (req "^0.1.14") (default-features #t) (kind 2)) (crate-dep (name "proptest") (req "^1.4.0") (default-features #t) (kind 2)))) (hash "1v8i6qnzjzdgivg9jfrh3zicjw8rd8z9xkhhgh9sg6s9242qii33") (features (quote (("default" "adjacency_matrix") ("adjacency_matrix"))))))

(define-public crate-graaf-0.36 (crate (name "graaf") (vers "0.36.0") (deps (list (crate-dep (name "divan") (req "^0.1.14") (default-features #t) (kind 2)) (crate-dep (name "proptest") (req "^1.4.0") (default-features #t) (kind 2)))) (hash "0qpnkhqqll56dmjf7i6ya9m6ab8s9nybb238k9j4zxaspymldnkj") (features (quote (("default" "adjacency_matrix") ("adjacency_matrix"))))))

(define-public crate-graaf-0.37 (crate (name "graaf") (vers "0.37.0") (deps (list (crate-dep (name "divan") (req "^0.1.14") (default-features #t) (kind 2)) (crate-dep (name "proptest") (req "^1.4.0") (default-features #t) (kind 2)))) (hash "0gqr4wlpd6j3gm2na1lsh2xis1glf1v4gwamlnjay7hsiwdfdq6y") (features (quote (("default" "adjacency_matrix") ("adjacency_matrix"))))))

(define-public crate-graaf-0.38 (crate (name "graaf") (vers "0.38.0") (deps (list (crate-dep (name "divan") (req "^0.1.14") (default-features #t) (kind 2)) (crate-dep (name "proptest") (req "^1.4.0") (default-features #t) (kind 2)))) (hash "0lcy0dlff3izxy63ml4p0splrvpm0q57myqvzm6agz3qrby6cfqk") (features (quote (("default" "adjacency_matrix") ("adjacency_matrix"))))))

(define-public crate-graaf-0.38 (crate (name "graaf") (vers "0.38.1") (deps (list (crate-dep (name "divan") (req "^0.1.14") (default-features #t) (kind 2)) (crate-dep (name "proptest") (req "^1.4.0") (default-features #t) (kind 2)))) (hash "0l7q9rnn0fywd2i95716w6aki0r9g8qs36yxv0lsq9d4xick45vp") (features (quote (("default" "adjacency_matrix") ("adjacency_matrix"))))))

(define-public crate-graaf-0.38 (crate (name "graaf") (vers "0.38.2") (deps (list (crate-dep (name "divan") (req "^0.1.14") (default-features #t) (kind 2)) (crate-dep (name "proptest") (req "^1.4.0") (default-features #t) (kind 2)))) (hash "0z5d8j8m36vlffwwp94asm31ndjwmnk4ajrbfa3lh3q3jww4s0pi") (features (quote (("default" "adjacency_matrix") ("adjacency_matrix"))))))

(define-public crate-graaf-0.39 (crate (name "graaf") (vers "0.39.0") (deps (list (crate-dep (name "divan") (req "^0.1.14") (default-features #t) (kind 2)) (crate-dep (name "proptest") (req "^1.4.0") (default-features #t) (kind 2)))) (hash "19863kf6k3c2gc8khg4ny3kqrjsjlcqg8ggk7rb1isgch3d1wvjh") (features (quote (("default" "adjacency_matrix") ("adjacency_matrix"))))))

(define-public crate-graaf-0.40 (crate (name "graaf") (vers "0.40.0") (deps (list (crate-dep (name "divan") (req "^0.1.14") (default-features #t) (kind 2)) (crate-dep (name "proptest") (req "^1.4.0") (default-features #t) (kind 2)))) (hash "1kn70kdrq7kpi8lh5ppblpcd0fd9b068g4qzpwmg38i5ipc172jr") (features (quote (("default" "adjacency_matrix") ("adjacency_matrix"))))))

(define-public crate-graaf-0.41 (crate (name "graaf") (vers "0.41.0") (deps (list (crate-dep (name "divan") (req "^0.1.14") (default-features #t) (kind 2)) (crate-dep (name "proptest") (req "^1.4.0") (default-features #t) (kind 2)))) (hash "092w9cccpp41lq37x02l0c05bwwzfc2c53ha3y3pz7505lm87d95") (features (quote (("default" "adjacency_matrix") ("adjacency_matrix"))))))

(define-public crate-graaf-0.42 (crate (name "graaf") (vers "0.42.0") (deps (list (crate-dep (name "divan") (req "^0.1.14") (default-features #t) (kind 2)) (crate-dep (name "proptest") (req "^1.4.0") (default-features #t) (kind 2)))) (hash "18s3xck757rs80slzpzfs4ybbm9522sxxcb2pv2s5dk17hlh856q") (features (quote (("default" "adjacency_matrix") ("adjacency_matrix"))))))

(define-public crate-graaf-0.42 (crate (name "graaf") (vers "0.42.1") (deps (list (crate-dep (name "divan") (req "^0.1.14") (default-features #t) (kind 2)) (crate-dep (name "proptest") (req "^1.4.0") (default-features #t) (kind 2)))) (hash "0mc4jr9ifmnyvwdm1p8amrfq5fc18fzq1lpna59mh9w90qq296zg") (features (quote (("default" "adjacency_matrix") ("adjacency_matrix"))))))

(define-public crate-graaf-0.42 (crate (name "graaf") (vers "0.42.2") (deps (list (crate-dep (name "divan") (req "^0.1.14") (default-features #t) (kind 2)) (crate-dep (name "proptest") (req "^1.4.0") (default-features #t) (kind 2)))) (hash "12g8sgvzx6r2x5jwky356hjg6l4qwnb7bjqnyy1clz646h8m04ba") (features (quote (("default" "adjacency_matrix") ("adjacency_matrix"))))))

(define-public crate-graaf-0.42 (crate (name "graaf") (vers "0.42.3") (deps (list (crate-dep (name "divan") (req "^0.1.14") (default-features #t) (kind 2)) (crate-dep (name "proptest") (req "^1.4.0") (default-features #t) (kind 2)))) (hash "00drp7b4byhdpqlv5spydhp8f56m8zxfn07syvj8s04gjr0mbynn") (features (quote (("default" "adjacency_matrix") ("adjacency_matrix"))))))

(define-public crate-graaf-0.42 (crate (name "graaf") (vers "0.42.4") (deps (list (crate-dep (name "divan") (req "^0.1.14") (default-features #t) (kind 2)) (crate-dep (name "proptest") (req "^1.4.0") (default-features #t) (kind 2)))) (hash "15rmm9nw1pmxvp5wvgbjh0pl9dmd4n4pw0jrn7ax0dg61fyfrnq5") (features (quote (("default" "adjacency_matrix") ("adjacency_matrix"))))))

(define-public crate-graaf-0.43 (crate (name "graaf") (vers "0.43.0") (deps (list (crate-dep (name "divan") (req "^0.1.14") (default-features #t) (kind 2)) (crate-dep (name "proptest") (req "^1.4.0") (default-features #t) (kind 2)))) (hash "061zaxl7rqbgngssccbmbknqx4al2ssiyq6c0103ympplj5fgg9g") (features (quote (("default" "adjacency_matrix") ("adjacency_matrix"))))))

(define-public crate-graaf-0.44 (crate (name "graaf") (vers "0.44.0") (deps (list (crate-dep (name "divan") (req "^0.1.14") (default-features #t) (kind 2)) (crate-dep (name "proptest") (req "^1.4.0") (default-features #t) (kind 2)))) (hash "1arbs39kzrlbkqq5hfr4aiyfabz958j1kdzxssyi6x3ws7jnmwxr") (features (quote (("default" "adjacency_matrix") ("adjacency_matrix"))))))

(define-public crate-graaf-0.45 (crate (name "graaf") (vers "0.45.0") (deps (list (crate-dep (name "divan") (req "^0.1.14") (default-features #t) (kind 2)) (crate-dep (name "proptest") (req "^1.4.0") (default-features #t) (kind 2)))) (hash "1678f49k2gqbh1cqk6csnaiji6b2jnx0bzyp93w66fxayl1jz0rw") (features (quote (("default" "adjacency_matrix") ("adjacency_matrix"))))))

(define-public crate-graaf-0.45 (crate (name "graaf") (vers "0.45.1") (deps (list (crate-dep (name "divan") (req "^0.1.14") (default-features #t) (kind 2)) (crate-dep (name "proptest") (req "^1.4.0") (default-features #t) (kind 2)))) (hash "1pb71zb0dl9r43vwzy497h5wnpfhz45lg67j8rvkzvzjh5k8la9i") (features (quote (("default" "adjacency_matrix") ("adjacency_matrix"))))))

(define-public crate-graaf-0.46 (crate (name "graaf") (vers "0.46.0") (deps (list (crate-dep (name "divan") (req "^0.1.14") (default-features #t) (kind 2)) (crate-dep (name "proptest") (req "^1.4.0") (default-features #t) (kind 2)))) (hash "0bs0nlm90lnr912lbh240ql0pgacvhnqry2z4inh83dghnzricaa") (features (quote (("default" "adjacency_matrix") ("adjacency_matrix"))))))

(define-public crate-graaf-0.47 (crate (name "graaf") (vers "0.47.1") (deps (list (crate-dep (name "divan") (req "^0.1.14") (default-features #t) (kind 2)) (crate-dep (name "proptest") (req "^1.4.0") (default-features #t) (kind 2)))) (hash "0r4bqali2chr66gazqak1fv1rnr9pqllz76mfcqb5zkfkwi26z2k") (features (quote (("default" "adjacency_matrix") ("adjacency_matrix"))))))

(define-public crate-graaf-0.47 (crate (name "graaf") (vers "0.47.2") (deps (list (crate-dep (name "divan") (req "^0.1.14") (default-features #t) (kind 2)) (crate-dep (name "proptest") (req "^1.4.0") (default-features #t) (kind 2)))) (hash "0gapy7yi8g8q5x2nfxc6r3pcdrxn510v1nk6x9j1r68py9784bya") (features (quote (("default" "adjacency_matrix") ("adjacency_matrix"))))))

(define-public crate-graaf-0.47 (crate (name "graaf") (vers "0.47.3") (deps (list (crate-dep (name "divan") (req "^0.1.14") (default-features #t) (kind 2)) (crate-dep (name "proptest") (req "^1.4.0") (default-features #t) (kind 2)))) (hash "0dvn32crhfx10dx0a9q1vrwzn34lg942a004qh1isvyc36pvfki3") (features (quote (("default" "adjacency_matrix") ("adjacency_matrix"))))))

(define-public crate-graaf-0.48 (crate (name "graaf") (vers "0.48.0") (deps (list (crate-dep (name "divan") (req "^0.1.14") (default-features #t) (kind 2)) (crate-dep (name "proptest") (req "^1.4.0") (default-features #t) (kind 2)))) (hash "1zmw456iw4wlx3rac53lzwrw41dah3ksnayh1rkl81dyry9rblaz") (features (quote (("default" "adjacency_matrix") ("adjacency_matrix"))))))

(define-public crate-graaf-0.48 (crate (name "graaf") (vers "0.48.1") (deps (list (crate-dep (name "divan") (req "^0.1.14") (default-features #t) (kind 2)) (crate-dep (name "proptest") (req "^1.4.0") (default-features #t) (kind 2)))) (hash "0b2x57syal7z3yar8n2ly2npl5076hkng03664bpsnia8scxd5jq") (features (quote (("default" "adjacency_matrix") ("adjacency_matrix"))))))

(define-public crate-graaf-0.48 (crate (name "graaf") (vers "0.48.2") (deps (list (crate-dep (name "divan") (req "^0.1.14") (default-features #t) (kind 2)) (crate-dep (name "proptest") (req "^1.4.0") (default-features #t) (kind 2)))) (hash "1995kyhj82zz380h8vf5ck1qsicijkp9x167b7csa11i9s1sb87h") (features (quote (("default" "adjacency_matrix") ("adjacency_matrix"))))))

(define-public crate-graaf-0.49 (crate (name "graaf") (vers "0.49.0") (deps (list (crate-dep (name "divan") (req "^0.1.14") (default-features #t) (kind 2)) (crate-dep (name "proptest") (req "^1.4.0") (default-features #t) (kind 2)))) (hash "0lzisiqh1lankgndxxmvsdsyv143y51fqacmpznhhgz4dixbgyay") (features (quote (("default" "adjacency_matrix") ("adjacency_matrix"))))))

(define-public crate-graaf-0.49 (crate (name "graaf") (vers "0.49.1") (deps (list (crate-dep (name "divan") (req "^0.1.14") (default-features #t) (kind 2)) (crate-dep (name "proptest") (req "^1.4.0") (default-features #t) (kind 2)))) (hash "0ks1rhxmbxh60hl8zcv951rijxdfkhhplkghii4vaq1mw9cqbq79") (features (quote (("default" "adjacency_matrix") ("adjacency_matrix"))))))

(define-public crate-graaf-0.50 (crate (name "graaf") (vers "0.50.0") (deps (list (crate-dep (name "divan") (req "^0.1.14") (default-features #t) (kind 2)) (crate-dep (name "proptest") (req "^1.4.0") (default-features #t) (kind 2)))) (hash "03an03wf8d1shkshz5ikx040n1by93l4y2dyp1dpcp103ql2hw01") (features (quote (("default" "adjacency_matrix") ("adjacency_matrix"))))))

(define-public crate-graaf-0.50 (crate (name "graaf") (vers "0.50.1") (deps (list (crate-dep (name "divan") (req "^0.1.14") (default-features #t) (kind 2)) (crate-dep (name "proptest") (req "^1.4.0") (default-features #t) (kind 2)))) (hash "0mq0vr8c92ppyv2lhvybac6ivcwgbb4hms06ldrk21ykgvm7p8p3") (features (quote (("default" "adjacency_matrix") ("adjacency_matrix"))))))

(define-public crate-graaf-0.50 (crate (name "graaf") (vers "0.50.2") (deps (list (crate-dep (name "divan") (req "^0.1.14") (default-features #t) (kind 2)) (crate-dep (name "proptest") (req "^1.4.0") (default-features #t) (kind 2)))) (hash "023mmf8mbsi1qnayj5ah7kcmmdqnkvbrxmvw77yrfyakcdlj70r3") (features (quote (("default" "adjacency_matrix") ("adjacency_matrix"))))))

(define-public crate-graaf-0.51 (crate (name "graaf") (vers "0.51.0") (deps (list (crate-dep (name "divan") (req "^0.1.14") (default-features #t) (kind 2)) (crate-dep (name "proptest") (req "^1.4.0") (default-features #t) (kind 2)))) (hash "0jy7bbs31381zjpdrgqjr7a7jrw8644lmrc3w524vkj2wkz0ngkx") (features (quote (("default" "adjacency_matrix") ("adjacency_matrix"))))))

(define-public crate-graaf-0.51 (crate (name "graaf") (vers "0.51.1") (deps (list (crate-dep (name "divan") (req "^0.1.14") (default-features #t) (kind 2)) (crate-dep (name "proptest") (req "^1.4.0") (default-features #t) (kind 2)))) (hash "1vln3v59swyml4d8d4mxyzx37x5xdlw8s8rj1fyr1c8pqyhwsm9d") (features (quote (("default" "adjacency_matrix") ("adjacency_matrix"))))))

(define-public crate-graaf-0.52 (crate (name "graaf") (vers "0.52.0") (deps (list (crate-dep (name "divan") (req "^0.1.14") (default-features #t) (kind 2)) (crate-dep (name "proptest") (req "^1.4.0") (default-features #t) (kind 2)))) (hash "1m50vwl3az6zb06ahxljr14zcn7595dnqhl42wr2rgnwvdlfyisn") (features (quote (("default" "adjacency_matrix") ("adjacency_matrix"))))))

(define-public crate-graaf-0.52 (crate (name "graaf") (vers "0.52.1") (deps (list (crate-dep (name "divan") (req "^0.1.14") (default-features #t) (kind 2)) (crate-dep (name "proptest") (req "^1.4.0") (default-features #t) (kind 2)))) (hash "1jgk57k2825hnmqg55mzhhn3dyjlczxvzgf82fs0l1yhwb7zspba") (features (quote (("default" "adjacency_matrix") ("adjacency_matrix"))))))

(define-public crate-graaf-0.52 (crate (name "graaf") (vers "0.52.2") (deps (list (crate-dep (name "divan") (req "^0.1.14") (default-features #t) (kind 2)) (crate-dep (name "proptest") (req "^1.4.0") (default-features #t) (kind 2)))) (hash "1pk5qxwj4jcagpv4hgabr6b68d65nngvcjzybf321z6wbfnpb5r3") (features (quote (("default" "adjacency_matrix") ("adjacency_matrix"))))))

(define-public crate-graaf-0.52 (crate (name "graaf") (vers "0.52.3") (deps (list (crate-dep (name "divan") (req "^0.1.14") (default-features #t) (kind 2)) (crate-dep (name "proptest") (req "^1.4.0") (default-features #t) (kind 2)))) (hash "091qy3nw6gs8snsnkdqgwrim77bnljnz4hx92s5mgdl16r9ydcz0") (features (quote (("default" "adjacency_matrix") ("adjacency_matrix"))))))

(define-public crate-graaf-0.52 (crate (name "graaf") (vers "0.52.4") (deps (list (crate-dep (name "divan") (req "^0.1.14") (default-features #t) (kind 2)) (crate-dep (name "proptest") (req "^1.4.0") (default-features #t) (kind 2)))) (hash "0hk90afwsnz02rhqz8f4jr8pps09jqyzz5cn92fq9sv7wv7iaa23") (features (quote (("default" "adjacency_matrix") ("adjacency_matrix"))))))

(define-public crate-graaf-0.52 (crate (name "graaf") (vers "0.52.5") (deps (list (crate-dep (name "divan") (req "^0.1.14") (default-features #t) (kind 2)) (crate-dep (name "proptest") (req "^1.4.0") (default-features #t) (kind 2)))) (hash "0vdm47qs9j1k9l8kip5d29y7dyfhznhcf0ky52a7vcp21ck06i5r") (features (quote (("default" "adjacency_matrix") ("adjacency_matrix"))))))

(define-public crate-graal-bindgen-macros-0.1 (crate (name "graal-bindgen-macros") (vers "0.1.0") (deps (list (crate-dep (name "proc-macro2") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1") (features (quote ("full" "extra-traits"))) (default-features #t) (kind 0)))) (hash "0qasbrn83mzxkzxmk5vvdlkfsln10ffn0g81k0z0rr7hc1yi712g")))

