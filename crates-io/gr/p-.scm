(define-module (crates-io gr p-) #:use-module (crates-io))

(define-public crate-grp-cli-0.1 (crate (name "grp-cli") (vers "0.1.0") (deps (list (crate-dep (name "structopt") (req "^0.3.13") (default-features #t) (kind 0)))) (hash "14ziyndmcymlgz6b64smkjc7rd9v9rhiljv3xwnv3lb1yp9adarh")))

(define-public crate-grp-cli-0.2 (crate (name "grp-cli") (vers "0.2.0") (deps (list (crate-dep (name "structopt") (req "^0.3.13") (default-features #t) (kind 0)))) (hash "13syx3i1q4nc9cmp9i9mc773iksmcmj8psf552jzhz54pwn4a98n")))

