(define-module (crates-io gr yf) #:use-module (crates-io))

(define-public crate-gryf-0.0.0 (crate (name "gryf") (vers "0.0.0") (hash "1qqcgjl17a4pqkhhqp78aca74i1a763wvrclphbd5f7cxyys6zwn")))

(define-public crate-gryf-derive-0.0.0 (crate (name "gryf-derive") (vers "0.0.0") (hash "1wqjbl18lb81wkghapr5n8jqr1v5hpw5ip03dllf9rp1gsfgx4zp")))

