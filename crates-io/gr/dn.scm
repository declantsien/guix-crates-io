(define-module (crates-io gr dn) #:use-module (crates-io))

(define-public crate-grdn-0.0.1 (crate (name "grdn") (vers "0.0.1") (deps (list (crate-dep (name "serde") (req "^1") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "time") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "uuid") (req "^1") (features (quote ("v4" "fast-rng" "macro-diagnostics" "serde"))) (default-features #t) (kind 0)))) (hash "1mjcrrwrldfwrkmyshc2wmwjmfc1ihhfan46053ihyp10ishq7hv")))

