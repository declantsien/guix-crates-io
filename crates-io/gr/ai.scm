(define-module (crates-io gr ai) #:use-module (crates-io))

(define-public crate-grail-0.0.0 (crate (name "grail") (vers "0.0.0") (hash "0k615vspasylxdqqwd8k346hkpx1f30dpwad568yqmlpv25dc0dd")))

(define-public crate-grail-rs-0.0.0 (crate (name "grail-rs") (vers "0.0.0") (deps (list (crate-dep (name "hound") (req "^3.4") (default-features #t) (kind 2)) (crate-dep (name "rodio") (req "^0.14") (default-features #t) (kind 2)))) (hash "1qhkxm644a3v8pl37j9qdn18104n4lip1212l75r0pjqdx6kii5a")))

(define-public crate-grain128-0.1 (crate (name "grain128") (vers "0.1.0") (deps (list (crate-dep (name "hex-literal") (req "^0.3.4") (default-features #t) (kind 2)))) (hash "05vwbnj8y2qk8bilpnx720ry2l2k9vhs13gdq15nz87ckvm765cz")))

