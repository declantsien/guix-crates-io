(define-module (crates-io gr oq) #:use-module (crates-io))

(define-public crate-groq-0.0.0 (crate (name "groq") (vers "0.0.0") (hash "1cdwy8pqnij05l03nfraw425z166amhw40rvm6a4g243fkph46z7")))

(define-public crate-groq-rs-0.0.0 (crate (name "groq-rs") (vers "0.0.0") (hash "1jijhj3kigs19ji1zh9mxfbbyzl7ysf9yl6jnf2c57r4bb5r7cnb")))

(define-public crate-groq-rust-0.1 (crate (name "groq-rust") (vers "0.1.0") (deps (list (crate-dep (name "reqwest") (req "^0.12.4") (features (quote ("blocking"))) (default-features #t) (kind 0)))) (hash "06qh94yzgqwq5a37s0qldcl2hz1c2abc4drs55cha9ixlalkarwf")))

