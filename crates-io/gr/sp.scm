(define-module (crates-io gr sp) #:use-module (crates-io))

(define-public crate-grsp-0.1 (crate (name "grsp") (vers "0.1.0") (deps (list (crate-dep (name "rayon") (req "^1.8.1") (default-features #t) (kind 0)) (crate-dep (name "termcolor") (req "^1.4.1") (default-features #t) (kind 0)))) (hash "12m10lxm17p2m3i6hcv5s8v15ss1b7yzglz8ny5qb0w28rd0imgm")))

(define-public crate-grsp-0.1 (crate (name "grsp") (vers "0.1.1") (deps (list (crate-dep (name "rayon") (req "^1.8.1") (default-features #t) (kind 0)) (crate-dep (name "termcolor") (req "^1.4.1") (default-features #t) (kind 0)))) (hash "1hi2ngikl58zxvqazk0d1vhl4h9z18cj4y6mllf70fwq7xaays9d")))

(define-public crate-grsp-0.1 (crate (name "grsp") (vers "0.1.2") (deps (list (crate-dep (name "rayon") (req "^1.8.1") (default-features #t) (kind 0)) (crate-dep (name "termcolor") (req "^1.4.1") (default-features #t) (kind 0)))) (hash "04dz9n57nhdwq1dj1yyrdp3570apbf64rz0k44s2cnmc6b39l34r")))

