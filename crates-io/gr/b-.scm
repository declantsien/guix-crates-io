(define-module (crates-io gr b-) #:use-module (crates-io))

(define-public crate-grb-macro-0.1 (crate (name "grb-macro") (vers "0.1.0") (deps (list (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (features (quote ("full" "parsing" "printing" "derive" "proc-macro" "clone-impls"))) (kind 0)))) (hash "1l5habny21xpl8l8nccwny2qrap9xyh1kx0f53ra683bsf31w1vh")))

(define-public crate-grb-sys-0.1 (crate (name "grb-sys") (vers "0.1.0") (deps (list (crate-dep (name "bindgen") (req "^0.49.0") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.0") (default-features #t) (kind 1)))) (hash "17s169dyfym19hy4f0iz6w7m3i67lgv0pd6pfns4qqxb5dpa17n3")))

(define-public crate-grb-sys-0.1 (crate (name "grb-sys") (vers "0.1.1") (deps (list (crate-dep (name "bindgen") (req "^0.49.0") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.0") (default-features #t) (kind 1)))) (hash "1iwqkm7nmhxcdss9gyhhdar04ywmms1hmkdm43wfa1n2g68b6v07")))

(define-public crate-grb-sys-0.1 (crate (name "grb-sys") (vers "0.1.2") (deps (list (crate-dep (name "bindgen") (req "^0.52.0") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.0") (default-features #t) (kind 1)))) (hash "01kvmnvmzgr62kd9ds3mvy09qw7bp165g1fph7gwq66p0cgmnspq")))

(define-public crate-grb-sys-0.1 (crate (name "grb-sys") (vers "0.1.3") (deps (list (crate-dep (name "bindgen") (req "^0.52.0") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.0") (default-features #t) (kind 1)))) (hash "1245b1889qg6byjs86p6ld3xasc9q0krzw0jrs0zzg9dr05646kh")))

(define-public crate-grb-sys-0.1 (crate (name "grb-sys") (vers "0.1.4") (deps (list (crate-dep (name "bindgen") (req "^0.65") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.0") (default-features #t) (kind 1)))) (hash "14kffh0wsk6xzy8gxmdq8w3vnzh5s5pazpp0pdy58fln866k54ll")))

(define-public crate-grb-sys-0.1 (crate (name "grb-sys") (vers "0.1.5") (deps (list (crate-dep (name "bindgen") (req "^0.65") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.0") (default-features #t) (kind 1)))) (hash "151vskclf8hlr409z3ly0jcncrj0ghwmd3m5qblh50mrdja9hb0p")))

(define-public crate-grb-sys-0.1 (crate (name "grb-sys") (vers "0.1.6") (deps (list (crate-dep (name "bindgen") (req "^0.66") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.0") (default-features #t) (kind 1)))) (hash "0y3w7z8gjynjwkiq1y1vynax03gzmzyzn24i1vig62bd6n8bk5zw")))

(define-public crate-grb-sys-0.1 (crate (name "grb-sys") (vers "0.1.7") (deps (list (crate-dep (name "bindgen") (req "^0.68") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.0") (default-features #t) (kind 1)))) (hash "1ljbm79sh9lkvi0sn71k4n1k31jni3nyw85cfr7f5j3vsa6w5jin")))

(define-public crate-grb-sys2-0.1 (crate (name "grb-sys2") (vers "0.1.0") (hash "0apy38phsx92nv1hkrm5iy3h06il7y7v0pybkshw26wmnywbr5fr")))

(define-public crate-grb-sys2-0.1 (crate (name "grb-sys2") (vers "0.1.1") (hash "0i332w3qvb7mml0cgcw7cp23f8c8f5cnzmlzbhqh2q64d4x7m0g8")))

(define-public crate-grb-sys2-0.2 (crate (name "grb-sys2") (vers "0.2.0") (hash "06d9j723fbw24pny46hvw9has9ijf78glnz7dfilzh5anay2g1lr")))

(define-public crate-grb-sys2-9 (crate (name "grb-sys2") (vers "9.5.0") (hash "004l0m4b9dqy4v82196l4imq16lvs4k37yg571780rim901y8chk") (links "gurobi95")))

(define-public crate-grb-sys2-10 (crate (name "grb-sys2") (vers "10.0.0") (deps (list (crate-dep (name "anyhow") (req "^1.0.83") (default-features #t) (kind 1)))) (hash "1d5cypmk7axf9srg14h8zmb4pnsp47pgzyaqjgsdl06d9mkv840l") (links "gurobi")))

