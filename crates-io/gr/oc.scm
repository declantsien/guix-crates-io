(define-module (crates-io gr oc) #:use-module (crates-io))

(define-public crate-grocer-0.1 (crate (name "grocer") (vers "0.1.0") (deps (list (crate-dep (name "image") (req "^0.25.1") (default-features #t) (kind 0)))) (hash "0l610h24wcr0rvhqr7k57q8y4kzzgqsnm32kh1nl2r3xzgjfcivs")))

(define-public crate-grocer-0.1 (crate (name "grocer") (vers "0.1.1") (deps (list (crate-dep (name "image") (req "^0.25.1") (default-features #t) (kind 0)))) (hash "0i6mxb1whgn1jcw727gpsg9fagakcrgm0aagrr7rv95rkr5iipky")))

