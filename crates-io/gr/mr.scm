(define-module (crates-io gr mr) #:use-module (crates-io))

(define-public crate-grmr-0.1 (crate (name "grmr") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^2.32.0") (default-features #t) (kind 0)) (crate-dep (name "colored") (req "^1.7") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.1.0") (default-features #t) (kind 0)))) (hash "1nc9yn1pvnn770hb0ajj47a1dcbafizyym49znsbbsr827nr0078")))

(define-public crate-grmr-0.2 (crate (name "grmr") (vers "0.2.0") (deps (list (crate-dep (name "clap") (req "^2.32.0") (default-features #t) (kind 0)) (crate-dep (name "colored") (req "^1.7") (default-features #t) (kind 0)) (crate-dep (name "glob") (req "^0.2.11") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.1.0") (default-features #t) (kind 0)))) (hash "0yvjndnk6yk7jqqxjn3ny51gba0mhjxszr625cr95g2qkr4316rn")))

