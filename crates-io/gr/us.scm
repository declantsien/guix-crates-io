(define-module (crates-io gr us) #:use-module (crates-io))

(define-public crate-grus-0.1 (crate (name "grus") (vers "0.1.0") (deps (list (crate-dep (name "bincode") (req "^1.3.3") (default-features #t) (kind 0)) (crate-dep (name "chrono") (req "^0.4.23") (features (quote ("serde"))) (default-features #t) (kind 0)) (crate-dep (name "crossterm") (req "^0.25.0") (default-features #t) (kind 0)) (crate-dep (name "dirs") (req "^4.0.0") (default-features #t) (kind 0)) (crate-dep (name "fuzzydate") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "sanakirja") (req "^1.2.16") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.152") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.38") (default-features #t) (kind 0)))) (hash "0sb9qn4s1hlfdkf673csh6q9nmp6bkrpvw7kwhb6kp04g32fi9g6")))

(define-public crate-grus-lib-0.1 (crate (name "grus-lib") (vers "0.1.0") (deps (list (crate-dep (name "chrono") (req "^0.4.23") (default-features #t) (kind 0)) (crate-dep (name "interim") (req "^0.1.0") (features (quote ("chrono"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "sanakirja") (req "^1.2.16") (default-features #t) (kind 0)))) (hash "01gdig20d5cygdv5sfp4mgbmfhygifsx1pb1xcywpsl73zqz98z0")))

(define-public crate-grus-lib-0.1 (crate (name "grus-lib") (vers "0.1.1") (deps (list (crate-dep (name "chrono") (req "^0.4.34") (default-features #t) (kind 0)) (crate-dep (name "interim") (req "^0.1.1") (features (quote ("chrono"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "sanakirja") (req "^1.4.1") (default-features #t) (kind 0)))) (hash "043sg5f8vd4bzv3mvycri3pr882bq6wyc9dsa5k003vi0v4dabxs")))

(define-public crate-grust-0.0.2 (crate (name "grust") (vers "0.0.2") (deps (list (crate-dep (name "glib-2-0-sys") (req "^0.0.1") (default-features #t) (kind 0)) (crate-dep (name "gobject-2-0-sys") (req "^0.0.2") (default-features #t) (kind 0)))) (hash "0ij97lvmn66ips6g34vxn762y6ihs6gbwvcscbfd6hrvp5mw16xs")))

(define-public crate-grust-0.0.3 (crate (name "grust") (vers "0.0.3") (deps (list (crate-dep (name "glib-2-0-sys") (req "^0.0.2") (default-features #t) (kind 0)) (crate-dep (name "gobject-2-0-sys") (req "^0.0.3") (default-features #t) (kind 0)))) (hash "1srzaszzmcmzgm58lvn2rgdqlrllpmayi4vdbmwdwnwj01633kgi")))

(define-public crate-grust-0.0.4 (crate (name "grust") (vers "0.0.4") (deps (list (crate-dep (name "glib-2-0-sys") (req "^0.0.2") (default-features #t) (kind 0)) (crate-dep (name "gobject-2-0-sys") (req "^0.0.4") (default-features #t) (kind 0)))) (hash "0cz1lm0nl5ksi6b1pgbk2mgf5dkhpk70x0ybk8v9dk7265k00w2h")))

(define-public crate-grust-0.0.5 (crate (name "grust") (vers "0.0.5") (deps (list (crate-dep (name "glib-2-0-sys") (req "^0.0.3") (default-features #t) (kind 0)) (crate-dep (name "gobject-2-0-sys") (req "^0.0.5") (default-features #t) (kind 0)))) (hash "0ydj3sypgj5amm9d6pxyw4xjb8jnr9fvmq4f74qy4bd409lz2xnq")))

(define-public crate-grust-0.0.6 (crate (name "grust") (vers "0.0.6") (deps (list (crate-dep (name "glib-2-0-sys") (req "^0.0.4") (default-features #t) (kind 0)) (crate-dep (name "gobject-2-0-sys") (req "^0.0.6") (default-features #t) (kind 0)))) (hash "15nnja9r198ck6vdj7d8kib2l4l3c10a3chl6gbk9rg1wix6ar7i")))

(define-public crate-grust-0.0.7 (crate (name "grust") (vers "0.0.7") (deps (list (crate-dep (name "glib-2-0-sys") (req "^0.0.4") (default-features #t) (kind 0)) (crate-dep (name "gobject-2-0-sys") (req "^0.0.6") (default-features #t) (kind 0)))) (hash "1d52876mb0w6pijls8mzdzdb20xdfdbgdvxdlxj5j1kwksdrs0y8")))

(define-public crate-grust-0.0.8 (crate (name "grust") (vers "0.0.8") (deps (list (crate-dep (name "glib-2-0-sys") (req "^0.0.6") (default-features #t) (kind 0)) (crate-dep (name "gobject-2-0-sys") (req "^0.0.7") (default-features #t) (kind 0)))) (hash "0ncmr5rgqaf06pgwiikbib9xc02wqx3ra94flwkfqpjciy4c2fzr")))

(define-public crate-grust-0.0.9 (crate (name "grust") (vers "0.0.9") (deps (list (crate-dep (name "glib-2-0-sys") (req "^0.0.6") (default-features #t) (kind 0)) (crate-dep (name "gobject-2-0-sys") (req "^0.0.7") (default-features #t) (kind 0)))) (hash "0hp9cj8md1ck8c4awzbj8zbxcnzkq5dfxxznn12d1xr9x2z0xwfv")))

(define-public crate-grust-0.0.10 (crate (name "grust") (vers "0.0.10") (deps (list (crate-dep (name "glib-2-0-sys") (req "^0.0.8") (default-features #t) (kind 0)) (crate-dep (name "gobject-2-0-sys") (req "^0.0.9") (default-features #t) (kind 0)))) (hash "11d5zrmj457ipv6livsb0zrx9972c2axi21skrqddnnvd6y33hpk")))

(define-public crate-grust-0.1 (crate (name "grust") (vers "0.1.0") (deps (list (crate-dep (name "glib-2-0-sys") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "gobject-2-0-sys") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.1") (default-features #t) (kind 0)))) (hash "1s8qwlabrh4ap1m9pc45g89kwiyjmscja6l1w0n7zn9cd7m3g0r0")))

(define-public crate-grust-0.1 (crate (name "grust") (vers "0.1.1") (deps (list (crate-dep (name "glib-2-0-sys") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "gobject-2-0-sys") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.1") (default-features #t) (kind 0)))) (hash "1j09pk1kxz3vm1l2pvcqnkrycpa4jxz05hk5d6kbkx2dz3kd07hq")))

(define-public crate-grust-0.1 (crate (name "grust") (vers "0.1.2") (deps (list (crate-dep (name "glib-2-0-sys") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "gobject-2-0-sys") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.1") (default-features #t) (kind 0)))) (hash "0ja8isbm8z8n0dms796iijqx4gs3ry6hx4hb7kdkx60d31sn83vx")))

(define-public crate-grust-0.1 (crate (name "grust") (vers "0.1.3") (deps (list (crate-dep (name "glib-2-0-sys") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "gobject-2-0-sys") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.1") (default-features #t) (kind 0)))) (hash "0ws7q018920ny17q80y0i7vxa5l8d2i80wzbi8wnzcdp9y4gxxqv")))

(define-public crate-grust-0.2 (crate (name "grust") (vers "0.2.0") (deps (list (crate-dep (name "glib-2-0-sys") (req "^0.1.1") (default-features #t) (kind 0)) (crate-dep (name "gobject-2-0-sys") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.1") (default-features #t) (kind 0)))) (hash "126ig6h01rrlrf2ln084d5garwsamkgzac9l11wibz0ycp9m1y2f")))

(define-public crate-grust-0.3 (crate (name "grust") (vers "0.3.0") (deps (list (crate-dep (name "glib-2-0-sys") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "gobject-2-0-sys") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "gtypes") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.1") (default-features #t) (kind 0)))) (hash "03f1ziyqwfxaipkkdmz19cvbbjhkyzigi1x9mww4d999mvyzhmw4")))

(define-public crate-grust-0.3 (crate (name "grust") (vers "0.3.1") (deps (list (crate-dep (name "glib-2-0-sys") (req "^0.46") (default-features #t) (kind 0)) (crate-dep (name "gobject-2-0-sys") (req "^0.46") (default-features #t) (kind 0)) (crate-dep (name "gtypes") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.1") (default-features #t) (kind 0)))) (hash "0hi0wfljaqnr98hyfjnkpv5zzi0lx7dkfmfj4nhbmx0ld8p5fzqz")))

(define-public crate-grust-0.3 (crate (name "grust") (vers "0.3.2") (deps (list (crate-dep (name "glib-2-0-sys") (req "^0.46") (default-features #t) (kind 0)) (crate-dep (name "gobject-2-0-sys") (req "^0.46") (default-features #t) (kind 0)) (crate-dep (name "gtypes") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "00xd399yhzr2alrz16iw3lydxj5if3lrmar7w9g1lxy1nk766wn0")))

