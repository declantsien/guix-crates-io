(define-module (crates-io gr az) #:use-module (crates-io))

(define-public crate-graze-0.1 (crate (name "graze") (vers "0.1.0") (deps (list (crate-dep (name "serde") (req "^1.0.147") (features (quote ("derive"))) (default-features #t) (kind 2)) (crate-dep (name "thiserror") (req "^1.0.37") (default-features #t) (kind 0)) (crate-dep (name "toml") (req "^0.5.9") (default-features #t) (kind 2)))) (hash "0aam5alr19xpyy0w63ka66a79lcl80hrz04mh8mqb60w3gl58fx4")))

