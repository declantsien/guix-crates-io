(define-module (crates-io gr ip) #:use-module (crates-io))

(define-public crate-grip-0.0.0 (crate (name "grip") (vers "0.0.0") (hash "0y0wmckblg5pph4vjy9z9yripz3yzl0qqs7wwl21hzmnjzascf1y")))

(define-public crate-gripwoud-0.1 (crate (name "gripwoud") (vers "0.1.0") (deps (list (crate-dep (name "anyhow") (req "^1.0.80") (default-features #t) (kind 0)) (crate-dep (name "assert_cmd") (req "^2.0.14") (default-features #t) (kind 2)) (crate-dep (name "assert_fs") (req "^1.1.1") (default-features #t) (kind 2)) (crate-dep (name "clap") (req "^4.5.1") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "clap-verbosity-flag") (req "^2.2.0") (default-features #t) (kind 0)) (crate-dep (name "env_logger") (req "^0.11.2") (default-features #t) (kind 0)) (crate-dep (name "indicatif") (req "^0.17.8") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.20") (default-features #t) (kind 0)) (crate-dep (name "predicates") (req "^3.1.0") (default-features #t) (kind 2)))) (hash "1mg0s3k8kvg8xb8asxmiykw7n0hrd16hvxqfxipanixbrsp2qavf")))

