(define-module (crates-io gr eg) #:use-module (crates-io))

(define-public crate-greg-0.1 (crate (name "greg") (vers "0.1.0") (hash "1m9f785jf4iapmahwyqn5m2nr8rvb6s41v8vw8jpc0q9lybyfjzg")))

(define-public crate-greg-0.1 (crate (name "greg") (vers "0.1.1") (hash "0slv67fb1hiyakqybkga2ygf8j4v3cipd3bcb1zhrwypyxysla9f")))

(define-public crate-greg-0.1 (crate (name "greg") (vers "0.1.2") (deps (list (crate-dep (name "rusqlite") (req "^0.28") (optional #t) (kind 0)))) (hash "0zrc8nq4kjnq0wbhxr1r3mqbabdqq5k9zgbqi713j2am74xiffsl") (v 2) (features2 (quote (("rusqlite" "dep:rusqlite"))))))

(define-public crate-greg-0.2 (crate (name "greg") (vers "0.2.0") (deps (list (crate-dep (name "rusqlite") (req "^0.28") (optional #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (optional #t) (kind 0)))) (hash "0x38sw51g7sb48cpbh6mw1bssn10483c8bfwclrja2c11b6ki1si") (v 2) (features2 (quote (("serde" "dep:serde") ("rusqlite" "dep:rusqlite"))))))

(define-public crate-greg-0.2 (crate (name "greg") (vers "0.2.1") (deps (list (crate-dep (name "rusqlite") (req "^0.29") (optional #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (optional #t) (kind 0)))) (hash "0kkrngl14043g2dzb2y03dwazykicfyjh9v500v7c5f1mhskf25n") (v 2) (features2 (quote (("serde" "dep:serde") ("rusqlite" "dep:rusqlite"))))))

(define-public crate-greg-0.2 (crate (name "greg") (vers "0.2.2") (deps (list (crate-dep (name "rusqlite") (req "^0.29") (optional #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (optional #t) (kind 0)))) (hash "0bm2zzgwd5ap3a2r7anc9hj2mmgibkcwqwcmxa1mlqxk7qm0xirr") (v 2) (features2 (quote (("serde" "dep:serde") ("rusqlite" "dep:rusqlite"))))))

(define-public crate-greg-0.2 (crate (name "greg") (vers "0.2.3") (deps (list (crate-dep (name "rusqlite") (req "^0.29") (optional #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (optional #t) (kind 0)))) (hash "1xxf359avp6ssh6njkrbld7qbq2rd8vawvk938hm1ic3lqq1bx7l") (v 2) (features2 (quote (("serde" "dep:serde") ("rusqlite" "dep:rusqlite"))))))

(define-public crate-greg-tz-0.1 (crate (name "greg-tz") (vers "0.1.0") (deps (list (crate-dep (name "greg") (req "^0.1.1") (default-features #t) (kind 0)) (crate-dep (name "parse-zoneinfo") (req "^0.3.0") (default-features #t) (kind 1)) (crate-dep (name "phf") (req "^0.11.1") (features (quote ("macros"))) (default-features #t) (kind 0)))) (hash "1h4w89xxkz4qny44irwi8g083dvzlgzxca67j2vl9p85yqnh8yhd")))

(define-public crate-greg-tz-0.1 (crate (name "greg-tz") (vers "0.1.1") (deps (list (crate-dep (name "greg") (req "^0.1.2") (default-features #t) (kind 0)) (crate-dep (name "parse-zoneinfo") (req "^0.3.0") (default-features #t) (kind 1)) (crate-dep (name "phf") (req "^0.11.1") (features (quote ("macros"))) (default-features #t) (kind 0)))) (hash "1w3bh1q19wqbcm6nz048453zp6vh5md53sz0qacxfs7ybjc0kvgn")))

(define-public crate-greg-tz-0.1 (crate (name "greg-tz") (vers "0.1.2") (deps (list (crate-dep (name "greg") (req "^0.1.2") (default-features #t) (kind 0)) (crate-dep (name "parse-zoneinfo") (req "^0.3.0") (default-features #t) (kind 1)) (crate-dep (name "phf") (req "^0.11.1") (features (quote ("macros"))) (default-features #t) (kind 0)))) (hash "0imjd6lpd0c3ppsxqnnr8kc537s02wzvgr2lyambr1hp0b6fa9cq")))

(define-public crate-greg-tz-0.2 (crate (name "greg-tz") (vers "0.2.0") (deps (list (crate-dep (name "greg") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "parse-zoneinfo") (req "^0.3.0") (default-features #t) (kind 1)) (crate-dep (name "phf") (req "^0.11.1") (features (quote ("macros"))) (default-features #t) (kind 0)))) (hash "07kz5d4pk28lsjnkim52ipc0711zz6pw8glja2dly7wxadx7y118")))

(define-public crate-greg-tz-0.2 (crate (name "greg-tz") (vers "0.2.1") (deps (list (crate-dep (name "greg") (req "^0.2.1") (default-features #t) (kind 0)) (crate-dep (name "parse-zoneinfo") (req "^0.3.0") (default-features #t) (kind 1)) (crate-dep (name "phf") (req "^0.11") (features (quote ("macros"))) (default-features #t) (kind 0)))) (hash "1ghcrjzms7hv6kks7hp8l7yx0sjn9hvwrq4d8s05izildywgad63")))

(define-public crate-greg-tz-0.2 (crate (name "greg-tz") (vers "0.2.2") (deps (list (crate-dep (name "greg") (req "^0.2.3") (default-features #t) (kind 0)) (crate-dep (name "parse-zoneinfo") (req "^0.3.0") (default-features #t) (kind 1)) (crate-dep (name "phf") (req "^0.11") (features (quote ("macros"))) (default-features #t) (kind 0)))) (hash "1x3nwxz0ncfs6h0yrc2c1ndbvkl1xzf7jlc7zsvcsnhbf18m6ijs")))

(define-public crate-greg-tz-0.2 (crate (name "greg-tz") (vers "0.2.3") (deps (list (crate-dep (name "greg") (req "^0.2.3") (default-features #t) (kind 0)) (crate-dep (name "parse-zoneinfo") (req "^0.3.0") (default-features #t) (kind 1)) (crate-dep (name "phf") (req "^0.11") (features (quote ("macros"))) (default-features #t) (kind 0)))) (hash "177581dkrsv7jxb008l45bizrcg0nfwbf2vzk4j4nb06jbp4cm09")))

(define-public crate-gregarious-0.1 (crate (name "gregarious") (vers "0.1.0") (hash "0pqlzjv6399hzzchp8p13991y803mxdh959nvrc0g4wykjvf969v")))

(define-public crate-gregex-0.4 (crate (name "gregex") (vers "0.4.1") (hash "1slmhaw02scn8nszk7rj93lcba3jpv76bqfy0ghgl4392njbxs20")))

(define-public crate-gregex-0.4 (crate (name "gregex") (vers "0.4.3") (hash "18cjgjhfl6pc4k8kp8xz6mfdcf8128kys4y0sjphbmwxhxj535x1")))

(define-public crate-gregex-0.5 (crate (name "gregex") (vers "0.5.0") (hash "1qhj6q4p3ys36drvw6z3gqp51qv29scw2gdiksbcksdxjsj2fwan")))

(define-public crate-gregex-0.5 (crate (name "gregex") (vers "0.5.1") (hash "0izwvrd68gqdlhwgfyyhglpbr3nsxny21nkkf3armx8ga3yzyppw")))

(define-public crate-gregex-0.5 (crate (name "gregex") (vers "0.5.2") (hash "020vlzkigl23rl3vbxx3hdly2gwbq3nhyyyf7ar75z01x3xzkxqg") (yanked #t)))

(define-public crate-gregex-0.5 (crate (name "gregex") (vers "0.5.3") (hash "1rr5rcj41rkv22wr0qs3xf2nxsc86mslx5hqc7mh9v0dpdfbffd0")))

(define-public crate-gregor-0.1 (crate (name "gregor") (vers "0.1.0") (hash "1y2hjira714b519q7ggb8f93cz23mpyl9g9qxmmw8q0p914wqfar")))

(define-public crate-gregor-0.2 (crate (name "gregor") (vers "0.2.0") (hash "0ms1gy739lq4hzvrip7ssr44x6ic3mhd73q9j078khww8icrqs02") (features (quote (("system_time"))))))

(define-public crate-gregor-0.2 (crate (name "gregor") (vers "0.2.1") (hash "11f1z64kzv3dwjssw4843zzxiy58dx7lrh1c9pw4hngpr7s0vnbk") (features (quote (("system_time"))))))

(define-public crate-gregor-0.2 (crate (name "gregor") (vers "0.2.2") (hash "1amqxyyp2bfvxpzpfn54nn7as7mnlysrxdg2fp0b5kiqlz9f5rmz") (features (quote (("system_time"))))))

(define-public crate-gregor-0.3 (crate (name "gregor") (vers "0.3.0") (hash "0bylp9sii9c1grimbna9qpixlvci47zh9r5n9skq3gvpxl6dfq3g") (features (quote (("system_time"))))))

(define-public crate-gregor-0.3 (crate (name "gregor") (vers "0.3.1") (hash "0myn83lp5998z0iv8jy2i05hsc4qm9h221mqn82mdz3byspbh4yw") (features (quote (("system_time"))))))

(define-public crate-gregor-0.3 (crate (name "gregor") (vers "0.3.2") (hash "0yir48ypfxbvajpw3gi4962416y7bjll3kfbbvazfaljhyhf8kny") (features (quote (("system_time"))))))

(define-public crate-gregor-0.3 (crate (name "gregor") (vers "0.3.3") (hash "0gjdn9mngnfrpynjiaqdfhr26iqbvvs5n8pw9a6cndycl7mjaxfc") (features (quote (("system_time"))))))

(define-public crate-gregorian-0.1 (crate (name "gregorian") (vers "0.1.0") (deps (list (crate-dep (name "assert2") (req "^0.2.1") (default-features #t) (kind 2)))) (hash "1qdv2q3f7bcyn2iswiwx1w22k95qj6zspb748qlbrldj9j6xdxcp") (features (quote (("std") ("default" "std")))) (yanked #t)))

(define-public crate-gregorian-0.1 (crate (name "gregorian") (vers "0.1.1") (deps (list (crate-dep (name "assert2") (req "^0.2.1") (default-features #t) (kind 2)))) (hash "0175xfxxjx62wbzxz1y6y9c1b1pxg346hc008dw7569i1zdnvamp") (features (quote (("std") ("default" "std"))))))

(define-public crate-gregorian-0.2 (crate (name "gregorian") (vers "0.2.0") (deps (list (crate-dep (name "assert2") (req "^0.3.3") (default-features #t) (kind 2)))) (hash "14gs790dr6ykw8c6gbgw95aa9g4z6zsf4zxahfbcwky8bf4h9drq") (features (quote (("std") ("default" "std"))))))

(define-public crate-gregorian-0.2 (crate (name "gregorian") (vers "0.2.1") (deps (list (crate-dep (name "assert2") (req "^0.3.3") (default-features #t) (kind 2)) (crate-dep (name "libc") (req "^0.2.82") (optional #t) (default-features #t) (kind 0)))) (hash "0l4hybg8zmgyarkzwhh6b2h6590w9zhrzw44kgf3hp4r5hprflil") (features (quote (("std" "libc") ("default" "std"))))))

(define-public crate-gregorian-0.2 (crate (name "gregorian") (vers "0.2.2") (deps (list (crate-dep (name "assert2") (req "^0.3.3") (default-features #t) (kind 2)) (crate-dep (name "libc") (req "^0.2.82") (optional #t) (default-features #t) (kind 0)))) (hash "1b75q2mm0pxbybw2psvyba2qxs4kxr79s7d617yicxfqf8skhwy8") (features (quote (("std" "libc") ("default" "std")))) (yanked #t)))

(define-public crate-gregorian-0.2 (crate (name "gregorian") (vers "0.2.3") (deps (list (crate-dep (name "assert2") (req "^0.3.3") (default-features #t) (kind 2)) (crate-dep (name "libc") (req "^0.2.82") (optional #t) (default-features #t) (kind 0)))) (hash "19s5chpldr5gip9fb53g6x7969vb6cf11p4qxcbk24pds6q81vq9") (features (quote (("std" "libc") ("default" "std"))))))

(define-public crate-gregorian-0.2 (crate (name "gregorian") (vers "0.2.4") (deps (list (crate-dep (name "assert2") (req "^0.3.3") (default-features #t) (kind 2)) (crate-dep (name "libc") (req "^0.2.82") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde_yaml") (req "^0.9.14") (default-features #t) (kind 2)))) (hash "0m13n1zaq7rbcx346qyzn6crf6psbxgb0n8xk66l7a6d8bn8m20q") (features (quote (("std" "libc") ("default" "std")))) (v 2) (features2 (quote (("serde" "dep:serde"))))))

(define-public crate-gregtatum_symbol_table-1 (crate (name "gregtatum_symbol_table") (vers "1.0.0") (deps (list (crate-dep (name "elsa") (req "^1.8") (default-features #t) (kind 0)) (crate-dep (name "fxhash") (req "^0.2") (default-features #t) (kind 0)))) (hash "15f1afahqjxk0s5ry1b008m3y678bm1cf6mh3mhyxb5byz31gpfg")))

