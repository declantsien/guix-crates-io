(define-module (crates-io gr ab) #:use-module (crates-io))

(define-public crate-grab-0.3 (crate (name "grab") (vers "0.3.0") (deps (list (crate-dep (name "bitflags") (req "^1.2") (default-features #t) (kind 0)) (crate-dep (name "nom") (req "^6.0") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.3") (default-features #t) (kind 2)))) (hash "1bb7pwn2i21bsn42ydjv9qal34pvkl9v4x3zkgv1ncd62zvwaqqx")))

(define-public crate-grab-0.3 (crate (name "grab") (vers "0.3.1") (deps (list (crate-dep (name "bitflags") (req "^1.2") (default-features #t) (kind 0)) (crate-dep (name "nom") (req "^6.0") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.3") (default-features #t) (kind 2)))) (hash "0fxbasz47lbc2hlm5a30633sl837x5kvgk35jj0pdkfgdvidsp1z")))

(define-public crate-grab-meta-0.1 (crate (name "grab-meta") (vers "0.1.0") (deps (list (crate-dep (name "reqwest") (req "^0.10.8") (default-features #t) (kind 0)) (crate-dep (name "select") (req "^0.5.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.116") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0.116") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.58") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^0.2") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "1ir1dv4rvvhwfqv4vql4rybbsik6dm9y27kdh6bp76lvnq8614ap")))

(define-public crate-grab-meta-0.1 (crate (name "grab-meta") (vers "0.1.1") (deps (list (crate-dep (name "reqwest") (req "^0.10.8") (default-features #t) (kind 0)) (crate-dep (name "select") (req "^0.5.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.116") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0.116") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.58") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^0.2") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "0ilj50c2av7a85bzi3x1mn1zagif7z4kk79q8p5pfvmfpivk20x7")))

(define-public crate-grab-meta-0.1 (crate (name "grab-meta") (vers "0.1.2") (deps (list (crate-dep (name "reqwest") (req "^0.10.8") (default-features #t) (kind 0)) (crate-dep (name "select") (req "^0.5.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.116") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0.116") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.58") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^0.2") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "0z8m66dibn8q5r1h3vsb9ximakv9ms9m8csdhihg4d9g60wl9ws1")))

(define-public crate-grab-meta-0.1 (crate (name "grab-meta") (vers "0.1.3") (deps (list (crate-dep (name "reqwest") (req "^0.10.8") (default-features #t) (kind 0)) (crate-dep (name "select") (req "^0.5.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.116") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0.116") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.58") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^0.2") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "1fv51wc9ri31mxhprijv5azyn1pp6kbb3zvm61fnjcvi1xhzc8w6")))

(define-public crate-grab_github_permalink-0.1 (crate (name "grab_github_permalink") (vers "0.1.0") (deps (list (crate-dep (name "anyhow") (req "^1.0.65") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^3.2.21") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "html5ever") (req "^0.26.0") (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.11.11") (features (quote ("blocking"))) (default-features #t) (kind 0)) (crate-dep (name "scraper") (req "^0.13.0") (default-features #t) (kind 0)) (crate-dep (name "url") (req "^2.3.1") (default-features #t) (kind 0)))) (hash "0vqxkji194lyb5nr5pp5kn3g1mbcxvld2dhi0dinb9s6f6kyx7qz")))

(define-public crate-grab_github_permalink-0.1 (crate (name "grab_github_permalink") (vers "0.1.1") (deps (list (crate-dep (name "anyhow") (req "^1.0.65") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^3.2.21") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.11.11") (features (quote ("blocking"))) (default-features #t) (kind 0)) (crate-dep (name "scraper") (req "^0.13.0") (default-features #t) (kind 0)) (crate-dep (name "url") (req "^2.3.1") (default-features #t) (kind 0)))) (hash "0vawh38ml8s5dzva3r7fngxdafndsj6hv0kkk1qd4brwl7wig32j")))

(define-public crate-grab_github_permalink-0.1 (crate (name "grab_github_permalink") (vers "0.1.2") (deps (list (crate-dep (name "anyhow") (req "^1.0.65") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^3.2.21") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.6.0") (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.11.11") (features (quote ("blocking"))) (default-features #t) (kind 0)) (crate-dep (name "scraper") (req "^0.13.0") (default-features #t) (kind 0)) (crate-dep (name "url") (req "^2.3.1") (default-features #t) (kind 0)))) (hash "0nrql4x0g87rp3fkak3knngdwi3k7kz8p0zxr9k4vyjx6j25yp77")))

(define-public crate-grab_github_permalink-0.1 (crate (name "grab_github_permalink") (vers "0.1.3") (deps (list (crate-dep (name "anyhow") (req "^1.0.65") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^3.2.21") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.6.0") (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.11.11") (features (quote ("blocking"))) (default-features #t) (kind 0)) (crate-dep (name "scraper") (req "^0.13.0") (default-features #t) (kind 0)) (crate-dep (name "url") (req "^2.3.1") (default-features #t) (kind 0)))) (hash "06zn27hbs5ap90yh3pxqx0w880r9014n926dj0gpqgwfizqy3y69")))

(define-public crate-grab_github_permalink-0.1 (crate (name "grab_github_permalink") (vers "0.1.4") (deps (list (crate-dep (name "anyhow") (req "^1.0.65") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^3.2.21") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.6.0") (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.11.11") (features (quote ("blocking"))) (default-features #t) (kind 0)) (crate-dep (name "scraper") (req "^0.13.0") (default-features #t) (kind 0)) (crate-dep (name "url") (req "^2.3.1") (default-features #t) (kind 0)))) (hash "07fsvyfn6r2pdq1z58z5n190ilcy8ri9p8arkrkhzw6fc9m8l6mp")))

(define-public crate-grab_github_permalink-0.2 (crate (name "grab_github_permalink") (vers "0.2.0") (deps (list (crate-dep (name "anyhow") (req "^1.0.65") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^3.2.21") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.6.0") (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.11.11") (features (quote ("blocking"))) (default-features #t) (kind 0)) (crate-dep (name "scraper") (req "^0.13.0") (default-features #t) (kind 0)) (crate-dep (name "url") (req "^2.3.1") (default-features #t) (kind 0)))) (hash "13mq8j5aynyn2248g7baq2jd4zpq6kvaigiaply8j0d4627a87a2")))

(define-public crate-grabbag-0.0.1 (crate (name "grabbag") (vers "0.0.1") (deps (list (crate-dep (name "grabbag_macros") (req "^0.0.1") (default-features #t) (kind 0)))) (hash "1fpqjp2l4zn9fzhk0445l57p6r9jzgmwvwyb7zm8943wz52a39bs")))

(define-public crate-grabbag-0.0.2 (crate (name "grabbag") (vers "0.0.2") (deps (list (crate-dep (name "grabbag_macros") (req "^0.0.2") (default-features #t) (kind 0)))) (hash "1x6m5lq0h5hpgaphy7i4nk5bbc79c23i352lr3yaxk6jhgy44x7b")))

(define-public crate-grabbag-0.0.3 (crate (name "grabbag") (vers "0.0.3") (deps (list (crate-dep (name "grabbag_macros") (req "^0.0.3") (default-features #t) (kind 0)))) (hash "084xfd6ay90fx3d6p6qcymsy6inlsv2cf4gl3vh1fqmh6i85n65n")))

(define-public crate-grabbag-0.0.4 (crate (name "grabbag") (vers "0.0.4") (deps (list (crate-dep (name "grabbag_macros") (req "^0.0.4") (default-features #t) (kind 0)))) (hash "0xls7dgp85xpr9k0m6kwdyzx5nmir71x7pz3wnish0ihi2cqnw3j")))

(define-public crate-grabbag-0.1 (crate (name "grabbag") (vers "0.1.0") (deps (list (crate-dep (name "grabbag_macros") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "02vkpd8idg47h2d6slwkayy8k35jq4mi4iwddd2cz794mjqms3z6")))

(define-public crate-grabbag-0.1 (crate (name "grabbag") (vers "0.1.1") (deps (list (crate-dep (name "grabbag_macros") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "12bjc0jsn63wgkf8dc26p8zps93yp5c1nz3b0b8zviwdphzydl8q")))

(define-public crate-grabbag-0.1 (crate (name "grabbag") (vers "0.1.2") (deps (list (crate-dep (name "grabbag_macros") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "1cvsc67jq95w9fq4qcbmfy7nc3rppzrf830kwa5nwwwn37nkc8j8")))

(define-public crate-grabbag_macros-0.0.1 (crate (name "grabbag_macros") (vers "0.0.1") (hash "0kpj9s56cflgm130hd5hxzhs191w9laxzv6dggc0wd08ww6swpzp")))

(define-public crate-grabbag_macros-0.0.2 (crate (name "grabbag_macros") (vers "0.0.2") (hash "1wy7ip62n5nria43p97psj7sbba64x9cbk041nnzw14nvz5xfdcb")))

(define-public crate-grabbag_macros-0.0.3 (crate (name "grabbag_macros") (vers "0.0.3") (hash "1k2jar9jpgj4zzk3wijgj1dkl8zyjxkags8rr9kqp9r7lwnvb8qj")))

(define-public crate-grabbag_macros-0.0.4 (crate (name "grabbag_macros") (vers "0.0.4") (hash "1ibg0qp0nap5ibwc2pzcc5ycp20n76q29x04mmyi2s2y6j3m6fc3")))

(define-public crate-grabbag_macros-0.1 (crate (name "grabbag_macros") (vers "0.1.0") (hash "1cmzcz7fmmp84kisrfkdcz58pc605m4fpi8m6gpbsp34pwwzl2rj")))

(define-public crate-grabbag_macros-0.1 (crate (name "grabbag_macros") (vers "0.1.2") (deps (list (crate-dep (name "rustc_version") (req "^0.1.4") (default-features #t) (kind 1)))) (hash "0sxcvv3ycs6kzp81s3n4iwxdr5h2g4kczbkxgg68q0vx25h526bx")))

(define-public crate-grabinput-0.1 (crate (name "grabinput") (vers "0.1.0") (hash "0cpjimlcdvgkdjhv3il69ighrby9masnlil7ddzz1rm5gf69p2ij")))

(define-public crate-grabinput-0.1 (crate (name "grabinput") (vers "0.1.1") (hash "094xcn2581cf79m7n21zj2sffaw1k64rrx8vjfd2y5gibjpgain5")))

(define-public crate-grabinput-0.2 (crate (name "grabinput") (vers "0.2.0") (deps (list (crate-dep (name "clippy") (req "^0.0.90") (optional #t) (default-features #t) (kind 0)))) (hash "112839kqlv81ya6ykpbdgw2w2kb4wfd7hd2vp94r55qs0ld3kz98")))

(define-public crate-grabinput-0.2 (crate (name "grabinput") (vers "0.2.1") (deps (list (crate-dep (name "clippy") (req "^0.0") (optional #t) (default-features #t) (kind 0)))) (hash "08mmhsr2npln8rfnx9cq2hma2n78lfjrm0gy71vyx5vwr0lr4y92")))

(define-public crate-graburl-0.1 (crate (name "graburl") (vers "0.1.0") (deps (list (crate-dep (name "ahref") (req "^0.2.7") (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.11") (features (quote ("blocking" "json"))) (default-features #t) (kind 0)))) (hash "1mqcs2kzlsn99w07505vhc8zg9ppkgyy147v26pklixnd97ivq5l") (rust-version "1.72.0")))

(define-public crate-graburl-0.1 (crate (name "graburl") (vers "0.1.1") (deps (list (crate-dep (name "ahref") (req "^0.2.7") (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.11") (features (quote ("blocking" "json"))) (default-features #t) (kind 0)))) (hash "1838jf53rrnc0lk0qb42n6l9ccvyg5a2nx6sp94y46436xzmw565") (rust-version "1.72.0")))

(define-public crate-graburl-0.1 (crate (name "graburl") (vers "0.1.2") (deps (list (crate-dep (name "ahref") (req "^0.2.7") (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.11") (features (quote ("blocking" "json"))) (default-features #t) (kind 0)))) (hash "1kf3fqp3h019scncljvqsdc30sa09jxw5fl0d1d2s1y29zbrp5v3") (rust-version "1.72.0")))

(define-public crate-graburl-0.1 (crate (name "graburl") (vers "0.1.3") (deps (list (crate-dep (name "ahref") (req "^0.2.7") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.9.5") (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.11") (features (quote ("blocking" "json"))) (default-features #t) (kind 0)))) (hash "0zg6fsxwi5b0rv9pj610d8y1vr1wiskkyvv85hfa64fm0s28qndj") (rust-version "1.72.0")))

(define-public crate-graburl-0.1 (crate (name "graburl") (vers "0.1.4") (deps (list (crate-dep (name "ahref") (req "^0.2.8") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.9.5") (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.11") (features (quote ("blocking" "json"))) (default-features #t) (kind 0)))) (hash "0c4sabdkb08vblfqc0zf0k0mvqddrmc0napn7zvkqz7nc438sxd1") (rust-version "1.72.0")))

(define-public crate-graburl-0.1 (crate (name "graburl") (vers "0.1.5") (deps (list (crate-dep (name "ahref") (req "^0.2.9") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.9.5") (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.11") (features (quote ("blocking" "json"))) (default-features #t) (kind 0)))) (hash "1sjrhs18ddlq697g1qg0wvvv9zyi1vbwdrbdrwxrixclwk9wpgjy") (rust-version "1.72.0")))

(define-public crate-graburl-0.1 (crate (name "graburl") (vers "0.1.6") (deps (list (crate-dep (name "ahref") (req "^0.2.9") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.9.5") (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.11") (features (quote ("blocking" "json"))) (default-features #t) (kind 0)))) (hash "111d74fs00yhf85cdsrdq79cx6xbndrriixxxkq5kwqwqzn7ifiq") (rust-version "1.72.0")))

(define-public crate-graburl-0.1 (crate (name "graburl") (vers "0.1.7") (deps (list (crate-dep (name "ahref") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.9.5") (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.11") (features (quote ("blocking" "json"))) (default-features #t) (kind 0)))) (hash "1vhvqqwzc8r14cn8ixaygc78hdiqsg8709ky9npwg4s6sq36dsg2") (rust-version "1.72.0")))

(define-public crate-graburl-0.1 (crate (name "graburl") (vers "0.1.8") (deps (list (crate-dep (name "ahref") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.9.5") (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.11") (features (quote ("blocking" "json"))) (default-features #t) (kind 0)))) (hash "037avp0hsm2ad83n328d6f12fm7d2rw9y79hnx59zg57q34svr33") (rust-version "1.72.0")))

(define-public crate-graby-0.1 (crate (name "graby") (vers "0.1.0") (deps (list (crate-dep (name "anyhow") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^4.3.19") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "0fd46nvrq433mwysxfh0q83f0bdycqlds7g45x376ss2s2j944kr")))

(define-public crate-graby-0.1 (crate (name "graby") (vers "0.1.1") (deps (list (crate-dep (name "anyhow") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^4.3.19") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "0cvl3m1vd5bdq41drivrq1fng4mq57gpf1p60djfh0cgbkas1x8f")))

(define-public crate-graby-1 (crate (name "graby") (vers "1.0.0") (deps (list (crate-dep (name "anyhow") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^4.3.19") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.9.5") (default-features #t) (kind 0)))) (hash "1pgiik2xfidir2x2icfq7pz16y8lrak2xkifqszxy9z38bsh9q56")))

