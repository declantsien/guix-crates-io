(define-module (crates-io gr jo) #:use-module (crates-io))

(define-public crate-grjoni_gps-0.1 (crate (name "grjoni_gps") (vers "0.1.0") (deps (list (crate-dep (name "rppal") (req "^0.11.3") (default-features #t) (kind 0)))) (hash "0r0vxjr54xwv0vw7915a80bzy62gg1llf6yizz2c8a6adfdvvwq0")))

(define-public crate-grjoni_gps-0.1 (crate (name "grjoni_gps") (vers "0.1.1") (deps (list (crate-dep (name "rppal") (req "^0.11.3") (default-features #t) (kind 0)))) (hash "1z2d8r5d413fag8jjh50ixna7z1dp7br1nzqhg85lp9m06iknh79")))

