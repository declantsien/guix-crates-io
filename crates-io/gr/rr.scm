(define-module (crates-io gr rr) #:use-module (crates-io))

(define-public crate-grrrrrrs-0.1 (crate (name "grrrrrrs") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^3.0") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "151z7f1i5rwzg9fm4lg0rwindm1jdaf3q9m9ihq133g82rr84sai")))

(define-public crate-grrrrrs-0.1 (crate (name "grrrrrs") (vers "0.1.0") (deps (list (crate-dep (name "anyhow") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "assert_cmd") (req "^2.0") (default-features #t) (kind 2)) (crate-dep (name "assert_fs") (req "^1.0") (default-features #t) (kind 2)) (crate-dep (name "clap") (req "^4.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "predicates") (req "^2.1") (default-features #t) (kind 2)))) (hash "1ysz8b4whk726gp4v2lc435lwk6cbh630wafbyncr6a4xwh2c5ff")))

