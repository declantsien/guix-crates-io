(define-module (crates-io gr is) #:use-module (crates-io))

(define-public crate-gris-0.1 (crate (name "gris") (vers "0.1.0") (hash "046bsib64fjhiibxgdbpin8qzl6iqb8dygc1kbqlayqzlfyyq1n4")))

(define-public crate-grist-1 (crate (name "grist") (vers "1.2.0") (deps (list (crate-dep (name "clap") (req "^3.0.0-beta.2") (default-features #t) (kind 0)))) (hash "1sdghxwh6gdi1bh063bbzmk3153mkzqz2nw4gkygygv4wsic5lsy")))

