(define-module (crates-io gr og) #:use-module (crates-io))

(define-public crate-grog-0.0.0 (crate (name "grog") (vers "0.0.0") (hash "17j8h348l60l1ixbnqi4jxazbakgj2p044ym5safb3w499v8rhdr")))

(define-public crate-grogu-0.0.1 (crate (name "grogu") (vers "0.0.1") (hash "1di03blcmw24qa8ja3h4pspwr3w864a935jqq56llcx8dlnvmbnq")))

