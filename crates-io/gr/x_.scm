(define-module (crates-io gr x_) #:use-module (crates-io))

(define-public crate-grx_macros-0.1 (crate (name "grx_macros") (vers "0.1.0") (deps (list (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (features (quote ("full" "fold"))) (default-features #t) (kind 0)))) (hash "17qcayzr2zizpxvl0lmck4xgdhpsvx83mr6bxcm5idcl0cx0w7xb")))

(define-public crate-grx_macros-0.1 (crate (name "grx_macros") (vers "0.1.1") (deps (list (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (features (quote ("full" "fold"))) (default-features #t) (kind 0)))) (hash "1f2v4rs66rjm8w0rchjarjf60qv5ghscv885wdc7094bxan7fywp")))

