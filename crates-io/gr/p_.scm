(define-module (crates-io gr p_) #:use-module (crates-io))

(define-public crate-grp_api-0.1 (crate (name "grp_api") (vers "0.1.0") (deps (list (crate-dep (name "rand") (req "^0.7.3") (default-features #t) (kind 0)))) (hash "1hgiw8q4imkxvkasmgywxa068iwk80h3yry5f91bgjqrj3ylqw8k")))

(define-public crate-grp_api-0.1 (crate (name "grp_api") (vers "0.1.1") (deps (list (crate-dep (name "rand") (req "^0.7.3") (default-features #t) (kind 0)))) (hash "1wz354dqzxxbbghyrq0h3bq8c5fq2a7yl3d702v327l0jdc834a5")))

