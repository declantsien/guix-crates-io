(define-module (crates-io r1 #{8n}#) #:use-module (crates-io))

(define-public crate-r18n-0.0.1 (crate (name "r18n") (vers "0.0.1") (deps (list (crate-dep (name "toml") (req "*") (default-features #t) (kind 0)))) (hash "1qixvzxg54qqb9lxlg58g2pvdg7q48whdxa173gygzj7r1f09bbg")))

(define-public crate-r18n-0.0.2 (crate (name "r18n") (vers "0.0.2") (deps (list (crate-dep (name "toml") (req "*") (default-features #t) (kind 0)))) (hash "0y7adc530p524k58ql5jwngv6yj7fs8rf5d38mr9fkhcx6649vzl")))

