(define-module (crates-io te as) #:use-module (crates-io))

(define-public crate-tease-0.1 (crate (name "tease") (vers "0.1.0") (deps (list (crate-dep (name "smartcore") (req "^0.2.1") (default-features #t) (kind 2)) (crate-dep (name "wry") (req "^0.18.2") (default-features #t) (kind 0)))) (hash "05dk45q08f4k6jdan4phb6lzxi6v9qzvdpvf3ll0rwrxqhrrzai7")))

(define-public crate-tease-0.1 (crate (name "tease") (vers "0.1.1") (deps (list (crate-dep (name "smartcore") (req "^0.2.1") (default-features #t) (kind 2)) (crate-dep (name "wry") (req "^0.18.2") (default-features #t) (kind 0)))) (hash "1wvbhzmkdsn5gqsr0lwh9g5c4zv0hvjlxdh0lwnixii2jccwpxk5")))

