(define-module (crates-io te ci) #:use-module (crates-io))

(define-public crate-tecio-0.1 (crate (name "tecio") (vers "0.1.0") (deps (list (crate-dep (name "libc") (req "^0.2.51") (default-features #t) (kind 0)))) (hash "1m1dgy443iqw2h05qhi5qw1na907jlzfbv6mq9gjwqk60ygq0w8i") (yanked #t)))

(define-public crate-tecio-0.1 (crate (name "tecio") (vers "0.1.1") (deps (list (crate-dep (name "libc") (req "^0.2.51") (default-features #t) (kind 0)))) (hash "0ddi4xa8p471vzhc0zgzj0xm5lw5i7gwf35ml1w1b50rgp0hgyka") (yanked #t)))

(define-public crate-tecio-0.1 (crate (name "tecio") (vers "0.1.2") (deps (list (crate-dep (name "libc") (req "^0.2.51") (default-features #t) (kind 0)))) (hash "1i9ns2f989b8s06m8dwj9akylcdifylrxi9q7h867m7gnfslj1ar") (yanked #t)))

(define-public crate-tecio-0.2 (crate (name "tecio") (vers "0.2.0") (deps (list (crate-dep (name "libc") (req "^0.2.51") (default-features #t) (kind 0)))) (hash "16sgc8zfdbsq44c6vcr4wh198fqcnb9ckwgy2kwaz01dnsjg8ign") (yanked #t)))

(define-public crate-tecio-0.2 (crate (name "tecio") (vers "0.2.1") (deps (list (crate-dep (name "libc") (req "^0.2.51") (default-features #t) (kind 0)))) (hash "05g60lai1skpqw3251hr81plf3d3wk4nvn8pssvyn33zr9fvkgq1") (yanked #t)))

(define-public crate-tecio-0.2 (crate (name "tecio") (vers "0.2.2") (deps (list (crate-dep (name "libc") (req "^0.2.51") (default-features #t) (kind 0)))) (hash "1077a708mgxlylnm24axbz2gaxg6g1157zs06v4v0qqybgkxz5i4") (yanked #t)))

(define-public crate-tecio-0.3 (crate (name "tecio") (vers "0.3.0") (deps (list (crate-dep (name "libc") (req "^0.2.51") (default-features #t) (kind 0)))) (hash "1lwfkh364vfrm0yb5jkayjdf07fi95p3f4hkknl5vnp4ngiy7vds") (yanked #t)))

