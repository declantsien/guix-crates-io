(define-module (crates-io te rs) #:use-module (crates-io))

(define-public crate-terse-0.1 (crate (name "terse") (vers "0.1.0") (hash "11bxqz4052736xbbv0cg6cyw03fhjb0jmvssb02n6vm9hwkyxl9n")))

(define-public crate-terser-0.0.1 (crate (name "terser") (vers "0.0.1") (hash "09j4xv6nd08qj0byw8z8rn0sl2iphg195jnz8n4082fsp97yayrp")))

(define-public crate-terses-0.1 (crate (name "terses") (vers "0.1.0") (hash "055dwsh52za7nnil8kgm0ick5shkl5apyxxxjz1xms38763dqz9v")))

(define-public crate-tersh-0.1 (crate (name "tersh") (vers "0.1.0") (hash "171dkymxxdsp0ji0myygslbssvdmwjxhnd05b8h8bifcqbbysj56")))

