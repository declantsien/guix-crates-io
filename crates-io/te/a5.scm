(define-module (crates-io te a5) #:use-module (crates-io))

(define-public crate-tea5767-0.1 (crate (name "tea5767") (vers "0.1.0") (deps (list (crate-dep (name "bit_field") (req "^0.10.1") (default-features #t) (kind 0)) (crate-dep (name "embedded-hal") (req "^0.2.4") (default-features #t) (kind 0)) (crate-dep (name "micromath") (req "^1.1.0") (default-features #t) (kind 0)))) (hash "0yfrihlic698b0x66wfjyyxag2ip41zskxb3qsc6hj5z89z3jiah")))

