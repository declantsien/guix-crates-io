(define-module (crates-io te am) #:use-module (crates-io))

(define-public crate-team-0.1 (crate (name "team") (vers "0.1.0") (deps (list (crate-dep (name "team-runtime") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "03g2vx1flgwsf7xqam7am5i1l7wwz85n0ybxzxyc7qgrn667rsy1")))

(define-public crate-team-runtime-0.1 (crate (name "team-runtime") (vers "0.1.0") (hash "1ykkmsdfc3yk0pv18h35nmhyh9243qlhr6j3gb78b5g8viv8hyhw")))

(define-public crate-teamdate-0.1 (crate (name "teamdate") (vers "0.1.0") (deps (list (crate-dep (name "chrono") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "chrono-english") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "chrono-tz") (req "^0.6") (features (quote ("serde"))) (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^3.2") (default-features #t) (kind 0)) (crate-dep (name "dirs") (req "^4.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "toml") (req "^0.5") (default-features #t) (kind 0)) (crate-dep (name "wyz") (req "^0.5") (default-features #t) (kind 0)))) (hash "0b302in3xr4708c7dlhlrz7agz3bcgspv7y72wvl4hm1izhn5frf")))

