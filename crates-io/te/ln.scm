(define-module (crates-io te ln) #:use-module (crates-io))

(define-public crate-telnet-0.1 (crate (name "telnet") (vers "0.1.0") (hash "06xqwfanwj8dhxqxdpmf8fqiwy4308p31j1j7gr5ww5miwrnd4h5")))

(define-public crate-telnet-0.1 (crate (name "telnet") (vers "0.1.1") (hash "0gfb27va7w908ccy8hm0gcbwip46cvkm1x2bnrl7nsfsay4gyq9v")))

(define-public crate-telnet-0.1 (crate (name "telnet") (vers "0.1.2") (hash "1lz9maff2c7gx33n3hqf9hz6krvg5466jziy6pahz71zy9i6jl79")))

(define-public crate-telnet-0.1 (crate (name "telnet") (vers "0.1.3") (hash "1dw88gk133yzlphnnf3sa4pc75c7sxk1z34f4hdawi8jjmkvlbcm")))

(define-public crate-telnet-0.1 (crate (name "telnet") (vers "0.1.4") (deps (list (crate-dep (name "flate2") (req "^1.0.9") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "replace_with") (req "^0.1.3") (optional #t) (default-features #t) (kind 0)))) (hash "1mdr1j6751dcliz6vk6r63bxq4gp2zacyjvpjw0y6yr1r7x6dd1y") (features (quote (("zcstream" "flate2" "replace_with"))))))

(define-public crate-telnet-0.2 (crate (name "telnet") (vers "0.2.0") (deps (list (crate-dep (name "flate2") (req "^1.0.22") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "replace_with") (req "^0.1.7") (optional #t) (default-features #t) (kind 0)))) (hash "13ldg6rn68bi826gxysn36ijlaq9q0v9wdzvp2ph0xf4ifqjgaas") (features (quote (("zcstream" "flate2" "replace_with"))))))

(define-public crate-telnet-0.2 (crate (name "telnet") (vers "0.2.1") (deps (list (crate-dep (name "flate2") (req "^1.0.22") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "replace_with") (req "^0.1.7") (optional #t) (default-features #t) (kind 0)))) (hash "1wx4d0y0plbdmvnkj2983l88f6x402b2r0xlbaaijlpa1ckcrwlr") (features (quote (("zcstream" "flate2" "replace_with"))))))

(define-public crate-telnet-codec-0.1 (crate (name "telnet-codec") (vers "0.1.0") (deps (list (crate-dep (name "bytes") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "tokio-util") (req "^0.6") (features (quote ("codec"))) (default-features #t) (kind 0)))) (hash "05dqbff2jrfxn518bvidx466v12808vqnj2f34dsrnyrqfz2sb4r")))

(define-public crate-telnetify-0.1 (crate (name "telnetify") (vers "0.1.0") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "1i2fcswndi68mfsh86kdgvpxiy10zy4ff43bn8pcq0v68aqp68vl")))

(define-public crate-telnetify-0.1 (crate (name "telnetify") (vers "0.1.1") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "1akwyjzfqn8z44x059skrzhxav1z9dqc5608f65q9shspdc54cyx")))

