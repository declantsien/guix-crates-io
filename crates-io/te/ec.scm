(define-module (crates-io te ec) #:use-module (crates-io))

(define-public crate-teeconfig-0.1 (crate (name "teeconfig") (vers "0.1.0-alpha.0") (deps (list (crate-dep (name "bitflags") (req "^2.3.3") (features (quote ("std"))) (default-features #t) (kind 0)) (crate-dep (name "lalrpop") (req "^0.20.0") (default-features #t) (kind 1)) (crate-dep (name "lalrpop-util") (req "^0.20.0") (default-features #t) (kind 0)) (crate-dep (name "logos") (req "^0.13.0") (default-features #t) (kind 0)))) (hash "1icdc4cwi5zh82zflqzxdfnaphwyk0j1p0svhsln7kd57wc1paf8")))

(define-public crate-teeconfig-0.1 (crate (name "teeconfig") (vers "0.1.0-alpha.1") (deps (list (crate-dep (name "bitflags") (req "^2.3.3") (features (quote ("std"))) (default-features #t) (kind 0)) (crate-dep (name "lalrpop") (req "^0.20.0") (default-features #t) (kind 1)) (crate-dep (name "lalrpop-util") (req "^0.20.0") (default-features #t) (kind 0)) (crate-dep (name "logos") (req "^0.13.0") (default-features #t) (kind 0)))) (hash "114flld9rfxag7rabya1am4i99bg9g6jsmvjx7ajbsl0nwqkckaf")))

(define-public crate-teeconfig-0.1 (crate (name "teeconfig") (vers "0.1.0-alpha.2") (deps (list (crate-dep (name "bitflags") (req "^2.3.3") (features (quote ("std"))) (default-features #t) (kind 0)) (crate-dep (name "lalrpop") (req "^0.20.0") (default-features #t) (kind 1)) (crate-dep (name "lalrpop-util") (req "^0.20.0") (default-features #t) (kind 0)) (crate-dep (name "logos") (req "^0.13.0") (default-features #t) (kind 0)))) (hash "1fkgqgn25waklyz94l34wn8c30zwkvyhgppzkrvckyidxa4c1cwi")))

(define-public crate-teeconfig-0.1 (crate (name "teeconfig") (vers "0.1.0") (deps (list (crate-dep (name "bitflags") (req "^2.3.3") (features (quote ("std"))) (default-features #t) (kind 0)) (crate-dep (name "lalrpop") (req "^0.20.0") (default-features #t) (kind 1)) (crate-dep (name "lalrpop-util") (req "^0.20.0") (default-features #t) (kind 0)) (crate-dep (name "logos") (req "^0.13.0") (default-features #t) (kind 0)))) (hash "0iwsa8v490nj2sj8i2yn4ji0filjhd50sg0g2j1ma5zkayfvapzx")))

(define-public crate-teecosmos-0.0.0 (crate (name "teecosmos") (vers "0.0.0") (hash "1d7vrsfw1hsw8pqpc0y1hldv39nrhzbbh2dw5s7svlwj4n9yzi9v")))

