(define-module (crates-io te ia) #:use-module (crates-io))

(define-public crate-teia-0.1 (crate (name "teia") (vers "0.1.0") (deps (list (crate-dep (name "botao") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "failure") (req "^0.1.5") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.2.14") (default-features #t) (kind 0)))) (hash "0rsnbxymzmghdz85klafg84wcl623x4lf25dmb42ji1y08v635g9")))

