(define-module (crates-io te x2) #:use-module (crates-io))

(define-public crate-tex2csv-0.1 (crate (name "tex2csv") (vers "0.1.0") (hash "1jh7p13cjwxnkha3m48kkfjh1b6am83vx2p1ykkjvn97qkwskamq")))

(define-public crate-tex2csv-0.2 (crate (name "tex2csv") (vers "0.2.0") (hash "0hiqfdk493ch3lggs16002wxc8a877lf87w6i7wqc0grkx53max3")))

