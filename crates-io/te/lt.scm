(define-module (crates-io te lt) #:use-module (crates-io))

(define-public crate-teltonika-rs-0.1 (crate (name "teltonika-rs") (vers "0.1.0") (deps (list (crate-dep (name "crc") (req "^2.0.0") (default-features #t) (kind 0)) (crate-dep (name "hex") (req "^0.4.3") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.14") (default-features #t) (kind 0)) (crate-dep (name "nom") (req "^6.1.2") (default-features #t) (kind 0)))) (hash "0y7q186vlb9snrw8xs29aad33xkzspfk2qdz831pqfgn06zqf2v3")))

(define-public crate-teltonika-rs-0.1 (crate (name "teltonika-rs") (vers "0.1.1") (deps (list (crate-dep (name "crc") (req "^3.0.0") (default-features #t) (kind 0)) (crate-dep (name "hex") (req "^0.4.3") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.14") (default-features #t) (kind 0)) (crate-dep (name "nom") (req "^7.1.1") (default-features #t) (kind 0)))) (hash "0yli4cv559c8k0hwqag57gaqx739clnxf6hw5qkm3w9di83j44dk")))

