(define-module (crates-io te pi) #:use-module (crates-io))

(define-public crate-tepimg-0.1 (crate (name "tepimg") (vers "0.1.0") (deps (list (crate-dep (name "camino") (req "^1.1.1") (default-features #t) (kind 0)) (crate-dep (name "getopts") (req "^0.2.21") (default-features #t) (kind 0)) (crate-dep (name "png") (req "^0.17.6") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.37") (default-features #t) (kind 0)))) (hash "0n1mfqlsmrs5c09vs4j94wjdjj1zd4f8fi5vidhbbwy90513nma2")))

