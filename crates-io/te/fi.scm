(define-module (crates-io te fi) #:use-module (crates-io))

(define-public crate-tefi-oracle-0.2 (crate (name "tefi-oracle") (vers "0.2.0") (deps (list (crate-dep (name "cosmwasm-bignumber") (req "^2.2.0") (default-features #t) (kind 0)) (crate-dep (name "cosmwasm-std") (req "^0.16.2") (default-features #t) (kind 0)) (crate-dep (name "schemars") (req "^0.8.1") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.103") (features (quote ("derive"))) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.23") (default-features #t) (kind 0)))) (hash "1s0hcxx4gwz762hgp8d1rdzqkh74fwf2fh8zry7kj0br6hcs0drg") (features (quote (("internal"))))))

