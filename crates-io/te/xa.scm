(define-module (crates-io te xa) #:use-module (crates-io))

(define-public crate-Texas-0.1 (crate (name "Texas") (vers "0.1.0") (hash "057gr8zv7ass05i8lbsxkz02s6z7iq4jhbgq8ciqmdaa9rrpwx44") (yanked #t)))

(define-public crate-Texas-0.1 (crate (name "Texas") (vers "0.1.2") (hash "1acz390pdss859rj6r1s4ajqs1n1l4dsgnzl5js3y161r4shfx13") (yanked #t)))

(define-public crate-Texas-0.1 (crate (name "Texas") (vers "0.1.3") (hash "1y8cglynkzinmrii9s6m5izd0ap7yhsnl15f0n1hk1dkhx2cmnnz") (yanked #t)))

(define-public crate-Texas-0.1 (crate (name "Texas") (vers "0.1.4") (hash "01wvpqnfss2p3w51nsf83vy4rhnnykra08c7f8rk01n3pcjhcm34") (yanked #t)))

(define-public crate-Texas-0.1 (crate (name "Texas") (vers "0.1.5") (hash "18bvkpmrwxzc7dmp38szirls7z443kc2di0xypr4qjnsf7z28lzq") (yanked #t)))

(define-public crate-Texas-0.1 (crate (name "Texas") (vers "0.1.6") (hash "06wddw1xw6la1yg3nvr3i9b6l91hgdh1bh6kqizph6fhzxgxf7dl") (yanked #t)))

(define-public crate-Texas-0.1 (crate (name "Texas") (vers "0.1.7") (hash "1dd7mji769djml3vlvsnhc4yy2nc0gnsbdj9ivz77krw89kyb6q5") (yanked #t)))

(define-public crate-Texas-0.1 (crate (name "Texas") (vers "0.1.8") (hash "1a9lrybnb9chlh28dkz1yb8r6ad92903g7r9qi59afrx2jk8wdka") (yanked #t)))

(define-public crate-Texas-0.1 (crate (name "Texas") (vers "0.1.9") (hash "0nqj0lm5dyhp29w214y8bcncyhddvxpgwxb82qrf990g1648wzn8") (yanked #t)))

(define-public crate-Texas-0.2 (crate (name "Texas") (vers "0.2.0") (hash "08fi2q59xn0xmqbwagdigq3hqkzn1q9p67l3md07rxrfljib4z4l") (yanked #t)))

(define-public crate-Texas-0.2 (crate (name "Texas") (vers "0.2.1") (hash "0kv56hkq11r4nfifikm8wms6xbv50zdj70g71dh0bd3xpzj1q6g6") (yanked #t)))

(define-public crate-Texas-0.2 (crate (name "Texas") (vers "0.2.2") (hash "18can59v35b85s4gdixzjpqq7ynw50p7hi6h6fd119m816nfgj00") (yanked #t)))

(define-public crate-Texas-0.2 (crate (name "Texas") (vers "0.2.3") (hash "0ypwxxfvlfvniwccm5wyg107hy75vhvvibyy6kh9hasfkcwya463") (yanked #t)))

