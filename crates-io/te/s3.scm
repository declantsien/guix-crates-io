(define-module (crates-io te s3) #:use-module (crates-io))

(define-public crate-tes3-0.1 (crate (name "tes3") (vers "0.1.0") (hash "1fjnqckzi666wbf09j1ng2kix4m6z76rsvm5dx8zm21kxf0ixflb")))

(define-public crate-tes3mp-plugin-0.1 (crate (name "tes3mp-plugin") (vers "0.1.0") (hash "119x8kpazapj455y536d7xd130zy3q81hqfkdm9gvai4knkqf6dg") (yanked #t)))

(define-public crate-tes3mp-plugin-0.1 (crate (name "tes3mp-plugin") (vers "0.1.1") (hash "0vk7a4mvfzqksijl9bcv79fycd7znqdgf0x5h90x6gsdzlyinkbj") (yanked #t)))

(define-public crate-tes3mp-plugin-0.1 (crate (name "tes3mp-plugin") (vers "0.1.2") (hash "0zdj46djbq1028720sl28rlp58mwpq6qjz82pd2w48zr1xvxv9p1")))

