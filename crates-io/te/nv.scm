(define-module (crates-io te nv) #:use-module (crates-io))

(define-public crate-tenv-0.2 (crate (name "tenv") (vers "0.2.0") (deps (list (crate-dep (name "clap") (req "^3.2.13") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "ctrlc") (req "^3.2.2") (default-features #t) (kind 0)))) (hash "0v4fpp87w2npqycwjh2728vy08fh7rl2nxlrkzfrlpi4mw8y6d77")))

(define-public crate-tenv-0.3 (crate (name "tenv") (vers "0.3.0") (deps (list (crate-dep (name "argfile") (req "^0.1.4") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^3.2.13") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "ctrlc") (req "^3.2.2") (default-features #t) (kind 0)) (crate-dep (name "dirs") (req "^4.0.0") (default-features #t) (kind 0)) (crate-dep (name "dunce") (req "^1.0.2") (default-features #t) (kind 0)) (crate-dep (name "shellexpand") (req "^2.1.2") (default-features #t) (kind 0)))) (hash "1hjf3lz00dn1wpb701kycdfk06gfj8wcd616hlzm7727sfd8jlg4")))

