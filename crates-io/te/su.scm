(define-module (crates-io te su) #:use-module (crates-io))

(define-public crate-tesuto-0.1 (crate (name "tesuto") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^4.4.1") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.188") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_yaml") (req "^0.9.25") (default-features #t) (kind 0)) (crate-dep (name "tesuto_project") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "1a2apvaiqq6gvdxd2sdnp6bjd7vyzysaimr334nr08zyz2adahpz")))

(define-public crate-tesuto-0.1 (crate (name "tesuto") (vers "0.1.1") (deps (list (crate-dep (name "clap") (req "^4.4.1") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.188") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_yaml") (req "^0.9.25") (default-features #t) (kind 0)) (crate-dep (name "tesuto_project") (req "^0.1.1") (default-features #t) (kind 0)))) (hash "0pfab4yzc300sjy373wmhzvd51hc0j50wwcpzmpsd66cmafj24n2")))

(define-public crate-tesuto_project-0.1 (crate (name "tesuto_project") (vers "0.1.0") (deps (list (crate-dep (name "serde") (req "^1.0.188") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_yaml") (req "^0.9.25") (default-features #t) (kind 0)))) (hash "1fn0nz64851dxr09ldzra9b74j7la0863awbhb1p26ja0cjiizh2")))

(define-public crate-tesuto_project-0.1 (crate (name "tesuto_project") (vers "0.1.1") (deps (list (crate-dep (name "serde") (req "^1.0.188") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_yaml") (req "^0.9.25") (default-features #t) (kind 0)))) (hash "13f9r0v3myhq1gnjfvxsp4h6w5g1jv6ibr66sd1l2yrq7x5s15ya")))

