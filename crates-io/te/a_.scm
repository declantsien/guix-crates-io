(define-module (crates-io te a_) #:use-module (crates-io))

(define-public crate-tea_render-0.1 (crate (name "tea_render") (vers "0.1.0") (deps (list (crate-dep (name "gl") (req "^0.14.0") (default-features #t) (kind 0)))) (hash "1r15adrh2c2k8cniqkizdqnz40qdvzr4m2j9dam976cysh49h5zd")))

(define-public crate-tea_render-0.1 (crate (name "tea_render") (vers "0.1.1") (deps (list (crate-dep (name "gl") (req "^0.14.0") (default-features #t) (kind 0)))) (hash "1g0kqa6i51rfwxsdfzy965ca35wvkdanx0y9196c4cmmakx4h1p4")))

