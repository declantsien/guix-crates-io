(define-module (crates-io te v_) #:use-module (crates-io))

(define-public crate-tev_client-0.1 (crate (name "tev_client") (vers "0.1.0") (hash "1njkm5brf6w6q3d22ay07np7ji4x4j1ka3liqwdqqqf6cddihpm3")))

(define-public crate-tev_client-0.2 (crate (name "tev_client") (vers "0.2.0") (hash "1y1r6mrn15p2b8ky1jp9bshjy1867nm8rfg6pl1jlzr2qq4ny3l6")))

(define-public crate-tev_client-0.3 (crate (name "tev_client") (vers "0.3.0") (hash "0h50wxb5lplas3v0lc7gi2f44jzkkn8clh9f0x2j8vbx2ps599pm")))

(define-public crate-tev_client-0.4 (crate (name "tev_client") (vers "0.4.0") (hash "0i7c663hpa3kgswa37v6g0bmvnizsb1pkzgdc1f4ssdwg59dzdcm")))

(define-public crate-tev_client-0.5 (crate (name "tev_client") (vers "0.5.0") (hash "078b4d4xhc5a75qci842dfpmh1x8q8nr5n46d3vdl4k7nfkkfspv")))

(define-public crate-tev_client-0.5 (crate (name "tev_client") (vers "0.5.1") (hash "1dcnxcxpdr6acbg3jj4p0z71lwkfknyw5pnrdy94pxil69r6kjqn")))

(define-public crate-tev_client-0.5 (crate (name "tev_client") (vers "0.5.2") (hash "0dhh2k1hawil19zrz3grlpkkp4h17z6s57iclc4jswsgdpaw4if8")))

