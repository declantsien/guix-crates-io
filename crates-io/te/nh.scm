(define-module (crates-io te nh) #:use-module (crates-io))

(define-public crate-tenhou-shuffle-0.1 (crate (name "tenhou-shuffle") (vers "0.1.0") (deps (list (crate-dep (name "base64") (req "^0.13.0") (kind 0)) (crate-dep (name "sha2") (req "^0.10") (kind 0)))) (hash "0rpzjf6666llsqrm521s8yvlvkp8cpisk5xqvbilrf9838y6207v")))

(define-public crate-tenhou-shuffle-0.1 (crate (name "tenhou-shuffle") (vers "0.1.1") (deps (list (crate-dep (name "base64") (req "^0.13.0") (kind 0)) (crate-dep (name "sha2") (req "^0.10") (kind 0)))) (hash "1r2w64ablf7svkpigfdsmp66fr92p5fxacjgch45fv8fa33g5sy0")))

(define-public crate-tenhou-shuffle-0.1 (crate (name "tenhou-shuffle") (vers "0.1.2") (deps (list (crate-dep (name "base64") (req "^0.13.0") (kind 0)) (crate-dep (name "sha2") (req "^0.10") (kind 0)))) (hash "0wp286wlg2pkd1pwfdmm3rlzj45hjzwynfjpf9i6iyppfxlmaxmh")))

