(define-module (crates-io te me) #:use-module (crates-io))

(define-public crate-temex-0.10 (crate (name "temex") (vers "0.10.0") (deps (list (crate-dep (name "itertools") (req "^0.11") (default-features #t) (kind 0)) (crate-dep (name "polars") (req "^0.30.0") (features (quote ("rows"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0") (default-features #t) (kind 0)))) (hash "18iqnq87r2wxvmar6bf7xs7hfd58fykas5h3gim7qw9s7cs25qgv") (features (quote (("default" "polars")))) (v 2) (features2 (quote (("polars" "dep:polars"))))))

