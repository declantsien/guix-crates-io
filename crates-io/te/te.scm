(define-module (crates-io te te) #:use-module (crates-io))

(define-public crate-tetengo_trie-0.1 (crate (name "tetengo_trie") (vers "0.1.0") (hash "0g68fy322z581729gqi16m8v3naq2l73z42h63yj7japgxbffzap") (yanked #t)))

(define-public crate-tetengo_trie-0.1 (crate (name "tetengo_trie") (vers "0.1.1") (hash "0ks3np0m26y5ilgh5jvbfjvsrqbai0zsb8h7jgf5lknmkww05is7") (yanked #t)))

(define-public crate-tetengo_trie-0.1 (crate (name "tetengo_trie") (vers "0.1.2") (hash "0xc7l29big093v6qg4cbp0s5xfmljq7nvfzggpjnwq74rg120w3a") (yanked #t)))

(define-public crate-tetengo_trie-1 (crate (name "tetengo_trie") (vers "1.0.0") (deps (list (crate-dep (name "anyhow") (req "^1.0.75") (default-features #t) (kind 0)) (crate-dep (name "hashlink") (req "^0.8.4") (default-features #t) (kind 0)) (crate-dep (name "memmap2") (req "^0.7.1") (default-features #t) (kind 0)) (crate-dep (name "once_cell") (req "^1.18.0") (default-features #t) (kind 0)) (crate-dep (name "tempfile") (req "^3.8.0") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.49") (default-features #t) (kind 0)))) (hash "16axzcjr29alff5lwrbbhv4lddcr6bip26mmf2fybzijdxsrfrwi")))

