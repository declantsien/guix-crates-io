(define-module (crates-io te nk) #:use-module (crates-io))

(define-public crate-tenki-0.1 (crate (name "tenki") (vers "0.1.0") (hash "1k38myqfqhv7f84rsw8z77flhf0c84m57077bg918b4qdnh78r5s") (yanked #t)))

(define-public crate-tenki-0.0.1 (crate (name "tenki") (vers "0.0.1") (hash "1malqzigxshrsn55xdmcs4cc2ralm5ryzwrs8nw1k7ngvpsg2dam")))

