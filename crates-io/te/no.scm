(define-module (crates-io te no) #:use-module (crates-io))

(define-public crate-tenon-0.0.1 (crate (name "tenon") (vers "0.0.1") (hash "0srv9nj5sgqgg1i2yyl63x2qwhzl65mdr66hm6ldz8avraaq6bmf")))

(define-public crate-tenonport-0.1 (crate (name "tenonport") (vers "0.1.0") (hash "1bc68xpz6rd69qphlamjfznmvbm31c1927qigiyny5dvzhg89sm2")))

(define-public crate-tenorite-0.1 (crate (name "tenorite") (vers "0.1.0") (deps (list (crate-dep (name "async-trait") (req "^0.1.53") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.17.0") (features (quote ("sync" "rt"))) (default-features #t) (kind 0)))) (hash "05zq5a5pcihpvpvxfv403275yjn2j745lwj9s05l2gxkd1d07ya8")))

(define-public crate-tenorite-0.1 (crate (name "tenorite") (vers "0.1.1") (deps (list (crate-dep (name "async-trait") (req "^0.1.53") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.17.0") (features (quote ("sync" "rt"))) (default-features #t) (kind 0)))) (hash "1fxag7hiq5f34pmzjxlazpccdalyi35lyipypa6854g5bmd6vyd5")))

(define-public crate-tenorite-0.1 (crate (name "tenorite") (vers "0.1.2") (deps (list (crate-dep (name "async-trait") (req "^0.1.53") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.31") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.17.0") (features (quote ("sync" "rt"))) (default-features #t) (kind 0)))) (hash "08pn2r4bp6awrh6d4awsyy4q3hgz2n4kf5pqd80g88zmqq5pq2zh")))

