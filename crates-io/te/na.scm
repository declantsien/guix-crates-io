(define-module (crates-io te na) #:use-module (crates-io))

(define-public crate-tenable-0.1 (crate (name "tenable") (vers "0.1.0") (deps (list (crate-dep (name "async-std") (req "^1.6") (default-features #t) (kind 2)) (crate-dep (name "bytes") (req "^0.5") (default-features #t) (kind 0)) (crate-dep (name "http") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.10") (features (quote ("blocking" "rustls-tls"))) (kind 2)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^0.2") (default-features #t) (kind 2)))) (hash "1rm8gd2c3gzi682018nkjxm36pb8am5qwc5vzqmqfhzk82w5ap7i")))

(define-public crate-tenable-0.1 (crate (name "tenable") (vers "0.1.1") (deps (list (crate-dep (name "async-std") (req "^1.6") (default-features #t) (kind 2)) (crate-dep (name "bytes") (req "^0.5") (default-features #t) (kind 0)) (crate-dep (name "http") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.10") (features (quote ("blocking" "rustls-tls"))) (kind 2)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^0.2") (default-features #t) (kind 2)))) (hash "0fpcd9kn74ln5zlxm5sr3rar1gfx2lhy21g7acs7l7730aqrqmz7")))

(define-public crate-tenacious-0.0.1 (crate (name "tenacious") (vers "0.0.1") (deps (list (crate-dep (name "compiletest_rs") (req "*") (default-features #t) (kind 2)))) (hash "02cq3vamjax908ghh0a1h5kmnhc7rdcls583xidhjbqxwmn7hjq1")))

(define-public crate-tenacious-0.0.2 (crate (name "tenacious") (vers "0.0.2") (deps (list (crate-dep (name "compiletest_rs") (req "*") (default-features #t) (kind 2)))) (hash "0lf1dm3vzq2vh8mw0d0sy4jg5bnngccpbx4pp61dgjbw70npj0fg")))

(define-public crate-tenacious-0.0.3 (crate (name "tenacious") (vers "0.0.3") (deps (list (crate-dep (name "compiletest_rs") (req "*") (default-features #t) (kind 2)))) (hash "0r0dkmjngpp1vj1dzi3m35x602zc79gsvh6k75bja20mjrq9zkdp")))

(define-public crate-tenacious-0.0.4 (crate (name "tenacious") (vers "0.0.4") (deps (list (crate-dep (name "compiletest_rs") (req "*") (default-features #t) (kind 2)))) (hash "13xjim1v4wklrw633x034skzkdbhw2cjxi0lx8ihb1hlqlrp7yif")))

(define-public crate-tenacious-0.0.5 (crate (name "tenacious") (vers "0.0.5") (deps (list (crate-dep (name "compiletest_rs") (req "*") (default-features #t) (kind 2)))) (hash "1a2jdz9zrad4l1r0f0fnhjk520l0xqc5jzg5jqc594x32fmhngm8")))

(define-public crate-tenacious-0.0.6 (crate (name "tenacious") (vers "0.0.6") (deps (list (crate-dep (name "compiletest_rs") (req "*") (default-features #t) (kind 2)))) (hash "038rkldgl6xls3sk78gmid3lpcf370xsdwdaxk63yjk16q6fzs16")))

(define-public crate-tenacious-0.0.7 (crate (name "tenacious") (vers "0.0.7") (deps (list (crate-dep (name "compiletest_rs") (req "*") (default-features #t) (kind 2)))) (hash "0q2lxxxw15kn2m9m52r54bhs98hjfw8mkv21i0nyfb5940jb0d84")))

(define-public crate-tenacious-0.0.8 (crate (name "tenacious") (vers "0.0.8") (deps (list (crate-dep (name "compiletest_rs") (req "*") (default-features #t) (kind 2)))) (hash "1sdnv32wv3wpw4zb2if27xfzk8f04xff0fx8v56ra1m7xhmj0gpp") (features (quote (("rvalue_checks"))))))

(define-public crate-tenacious-0.0.9 (crate (name "tenacious") (vers "0.0.9") (deps (list (crate-dep (name "compiletest_rs") (req "*") (default-features #t) (kind 2)))) (hash "123jj7z0yscawnjqdpx9n8iqcml0w2zv2ivpzp5v1b2wwwk74lpk") (features (quote (("rvalue_checks"))))))

(define-public crate-tenacious-0.0.10 (crate (name "tenacious") (vers "0.0.10") (deps (list (crate-dep (name "compiletest_rs") (req "*") (default-features #t) (kind 2)))) (hash "1cjcgx34qhdnakkkxqvpgnil0kyx3pq83h0qrfha0nkpxqc8jkbw") (features (quote (("rvalue_checks"))))))

(define-public crate-tenacious-0.0.11 (crate (name "tenacious") (vers "0.0.11") (deps (list (crate-dep (name "compiletest_rs") (req "*") (default-features #t) (kind 2)))) (hash "1apfinhfady487n5hyhm1j54g4fsp8jpdjwwvz2cki2mvhzrcsx4") (features (quote (("rvalue_checks"))))))

(define-public crate-tenacious-0.0.12 (crate (name "tenacious") (vers "0.0.12") (deps (list (crate-dep (name "compiletest_rs") (req "*") (default-features #t) (kind 2)))) (hash "0k6l8mqcgg8wdvy0ri0d5k47xpkwszlbyj3lnk79sw8v58khfqi8") (features (quote (("rvalue_checks"))))))

(define-public crate-tenacious-0.0.13 (crate (name "tenacious") (vers "0.0.13") (deps (list (crate-dep (name "compiletest_rs") (req "*") (default-features #t) (kind 2)))) (hash "1qvrh7pwqn2sqp3dqw4kg856ac2r4m3kn7jjqgvnnw44aswam7yx") (features (quote (("rvalue_checks"))))))

(define-public crate-tenacious-0.0.14 (crate (name "tenacious") (vers "0.0.14") (deps (list (crate-dep (name "compiletest_rs") (req "*") (default-features #t) (kind 2)))) (hash "1h1j3xn0m17akgnb71x0m1cywshxgjn9yvavz7qimxxbxrnn52w0") (features (quote (("rvalue_checks"))))))

(define-public crate-tenacious-0.0.15 (crate (name "tenacious") (vers "0.0.15") (deps (list (crate-dep (name "compiletest_rs") (req "*") (default-features #t) (kind 2)))) (hash "05i48yfkkjx449hqpw7zf07xcnlp02i66f0vy9kwk7d6l7hvwc87") (features (quote (("rvalue_checks"))))))

(define-public crate-tenacious-0.1 (crate (name "tenacious") (vers "0.1.0") (deps (list (crate-dep (name "compiletest_rs") (req "^0.0.11") (default-features #t) (kind 2)))) (hash "10idlgpsmm37cxindrsclzcvxpsnyxw5v2v53msfj3kmrmqmfzda") (features (quote (("rvalue_checks"))))))

(define-public crate-tenacious-0.1 (crate (name "tenacious") (vers "0.1.1") (deps (list (crate-dep (name "compiletest_rs") (req "^0.1.0") (default-features #t) (kind 2)))) (hash "1ar4sicn7mnxs6szxwz6c11b9wcv9zhjmll4djs6x12c858am8hz") (features (quote (("rvalue_checks"))))))

(define-public crate-tenacious-0.1 (crate (name "tenacious") (vers "0.1.2") (deps (list (crate-dep (name "compiletest_rs") (req "^0.1.0") (default-features #t) (kind 2)))) (hash "0cbygk79xygvj4rn7ffx8axwg6bxsbdl3b4s9pkwdjn4rmw5rgzw") (features (quote (("rvalue_checks"))))))

(define-public crate-tenacious-0.2 (crate (name "tenacious") (vers "0.2.0") (deps (list (crate-dep (name "compiletest_rs") (req "^0.1.0") (default-features #t) (kind 2)))) (hash "120n7bswbnalgl7v4pyc1yyv5bj1h0f6fgr265p8q4n7djs07hlb") (features (quote (("rvalue_checks"))))))

(define-public crate-tenacious-0.2 (crate (name "tenacious") (vers "0.2.1") (deps (list (crate-dep (name "compiletest_rs") (req "^0.1.0") (default-features #t) (kind 2)))) (hash "0ny4vhfw2zd58bprqy77lq0655wk0ikckxqhwgsn98n5g4f7jidr") (features (quote (("rvalue_checks"))))))

(define-public crate-tenacious-0.2 (crate (name "tenacious") (vers "0.2.2") (deps (list (crate-dep (name "compiletest_rs") (req "^0.2.0") (default-features #t) (kind 2)))) (hash "02y24vlkkp5z69pl4vx0n7d8lk2j3r223cb239sq2sp759gy7832") (features (quote (("rvalue_checks"))))))

(define-public crate-tenacious-0.2 (crate (name "tenacious") (vers "0.2.3") (deps (list (crate-dep (name "compiletest_rs") (req "^0.2.0") (default-features #t) (kind 2)))) (hash "0mh4r0dx27qv507xrnqb990m645q771s8i5vk8i3h9vd1ljps6m6") (features (quote (("rvalue_checks"))))))

(define-public crate-tenant-0.0.0 (crate (name "tenant") (vers "0.0.0-alpha") (hash "12dgfi4iqw9k1jcw02va6xp81xrbs22763yyajq139sspm4z6jxi")))

