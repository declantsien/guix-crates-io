(define-module (crates-io te rb) #:use-module (crates-io))

(define-public crate-terbilang-0.1 (crate (name "terbilang") (vers "0.1.0") (hash "1qy268pfczmbd3dgv64dvvh32nfbagad9iiinfwyb176ckj4ivs8")))

(define-public crate-terbium-0.0.0 (crate (name "terbium") (vers "0.0.0") (hash "16bs74x0aqa3w89fcjxfc8zmj6iclh5q9qk6pxm26b394ssvh6k3") (yanked #t)))

(define-public crate-terbium-0.0.1 (crate (name "terbium") (vers "0.0.1") (deps (list (crate-dep (name "terbium_grammar") (req "^0") (default-features #t) (kind 0)))) (hash "0l3jpl4k30whd2xdbhwibncq369ywkfzpy72j0pyxv8v85wksa4n")))

(define-public crate-terbium_grammar-0.0.1 (crate (name "terbium_grammar") (vers "0.0.1") (deps (list (crate-dep (name "chumsky") (req "^0.8") (default-features #t) (kind 0)))) (hash "1qqa97c523m7rwlrpv5n3x70b0b6gmlxq268jishqw2qk8y3x8al")))

(define-public crate-terble-0.1 (crate (name "terble") (vers "0.1.0") (deps (list (crate-dep (name "termion") (req "^1.5.6") (default-features #t) (kind 0)))) (hash "0vg0bgjy8j7jznjhsckc9mxx5hrw1q33mrdi08c6h18y7mb3nvlb")))

