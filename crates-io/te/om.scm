(define-module (crates-io te om) #:use-module (crates-io))

(define-public crate-teom_guessing_game-0.1 (crate (name "teom_guessing_game") (vers "0.1.0") (deps (list (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)))) (hash "1vv6l060s5mljwjsd7l3ss5qqlrh97jrcfp8sf8a0rxpkr8b7dgc")))

(define-public crate-teom_guessing_game-0.1 (crate (name "teom_guessing_game") (vers "0.1.1") (deps (list (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)))) (hash "1wwlbzbmwsfxyc8x4h29yaf09fzqmxlz0071mdq4knn9xmfhmgi8")))

