(define-module (crates-io te ng) #:use-module (crates-io))

(define-public crate-tengfei-ff-0.1 (crate (name "tengfei-ff") (vers "0.1.0") (deps (list (crate-dep (name "structopt") (req "^0.3.26") (default-features #t) (kind 0)))) (hash "0hlg0hzha0wqqzv3igbmrsv3lqcwfhqs8zf7pfb2g341wya2gmvy")))

(define-public crate-tengfei-ff-0.1 (crate (name "tengfei-ff") (vers "0.1.1") (deps (list (crate-dep (name "structopt") (req "^0.3.26") (default-features #t) (kind 0)))) (hash "0kdwbpj483g1l9vgd3gadmccvk6p7x5d6hkxsgipdhplg3ij2pg8")))

(define-public crate-tengfei-ff-0.2 (crate (name "tengfei-ff") (vers "0.2.0") (deps (list (crate-dep (name "structopt") (req "^0.3.26") (default-features #t) (kind 0)))) (hash "0p0i1lzi927dnzhma45xqr6jbi1ygld3471n5817437bjrhvj0i9")))

(define-public crate-tengfei-ff-0.3 (crate (name "tengfei-ff") (vers "0.3.0") (deps (list (crate-dep (name "structopt") (req "^0.3.26") (default-features #t) (kind 0)))) (hash "034vp8qbq6qj2jqvaqv03jaskbx75hif09h5i2z0hrqi2vw4jdhb")))

(define-public crate-tengfei-ff-0.3 (crate (name "tengfei-ff") (vers "0.3.1") (deps (list (crate-dep (name "structopt") (req "^0.3.26") (default-features #t) (kind 0)))) (hash "07ckhh9ai3f3hj4kqfcq5n0mai9b9wfkbzpxlc9amcjz7lxli8d1")))

(define-public crate-tengwar-0.3 (crate (name "tengwar") (vers "0.3.0") (deps (list (crate-dep (name "argh") (req "^0.1.4") (default-features #t) (kind 0)) (crate-dep (name "itertools") (req "^0.10.0") (default-features #t) (kind 0)))) (hash "12za3r9yf272vajiykdbh84jy4lg0vj0r8xcdwp9cm7nhl75ndyl") (features (quote (("long-vowel-unique") ("long-vowel-double") ("circumflex"))))))

(define-public crate-tengwar-0.6 (crate (name "tengwar") (vers "0.6.0") (deps (list (crate-dep (name "argh") (req "^0.1.4") (default-features #t) (kind 0)) (crate-dep (name "itertools") (req "^0.10.0") (default-features #t) (kind 0)))) (hash "1z3lynbwrk290di9d6sc27r0xzrd60kp4xma06hw3wf3yfcid36y") (features (quote (("nuquernar") ("long-vowel-unique") ("long-vowel-double") ("default" "long-vowel-double" "nuquernar") ("circumflex") ("alt-rince"))))))

(define-public crate-tengwar-0.6 (crate (name "tengwar") (vers "0.6.1") (deps (list (crate-dep (name "argh") (req "^0.1.4") (default-features #t) (kind 0)) (crate-dep (name "itertools") (req "^0.10.0") (default-features #t) (kind 0)))) (hash "1jyhrgnrzi3hd05nrcz7fqfh7qljpdp127009m7in0xxxhmzccsn") (features (quote (("nuquernar") ("long-vowel-unique") ("long-vowel-double") ("default" "long-vowel-double" "nuquernar") ("circumflex") ("alt-rince"))))))

(define-public crate-tengwar-0.7 (crate (name "tengwar") (vers "0.7.0") (deps (list (crate-dep (name "argh") (req "^0.1.4") (default-features #t) (kind 0)) (crate-dep (name "itertools") (req "^0.10.0") (default-features #t) (kind 0)))) (hash "0w55mzwf91gmssg6kb2bfmki8yh32gjqs3nmxhb85vmkpjg6mlwn") (features (quote (("nuquernar") ("long-vowel-unique") ("long-vowel-double") ("default" "long-vowel-double" "nuquernar") ("circumflex") ("alt-rince"))))))

(define-public crate-tengwar-0.7 (crate (name "tengwar") (vers "0.7.1") (deps (list (crate-dep (name "argh") (req "^0.1.4") (default-features #t) (kind 0)) (crate-dep (name "itertools") (req "^0.10.0") (default-features #t) (kind 0)))) (hash "06vv6m6dh5df5jmgzh7g1gwkc7sv6yjxbaravj18l0ai6l5cb5s1") (features (quote (("nuquernar") ("long-vowel-unique") ("long-vowel-double") ("default" "long-vowel-double" "nuquernar") ("circumflex") ("alt-rince"))))))

(define-public crate-tengwar-0.8 (crate (name "tengwar") (vers "0.8.0") (deps (list (crate-dep (name "clap") (req "^4.0.18") (features (quote ("cargo" "derive" "wrap_help"))) (default-features #t) (kind 0)) (crate-dep (name "itertools") (req "^0.10.0") (default-features #t) (kind 0)))) (hash "0xn953k2qvgi7z6yigmpgwa6115lln7ajyl74ryyip1f1bb5djcv") (features (quote (("nuquernar") ("long-vowel-unique") ("long-vowel-double") ("default" "long-vowel-double" "nuquernar") ("circumflex") ("alt-rince"))))))

(define-public crate-tengwar-0.9 (crate (name "tengwar") (vers "0.9.0") (deps (list (crate-dep (name "cfg-if") (req "^1.0.0") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^4.0.18") (features (quote ("cargo" "derive" "wrap_help"))) (default-features #t) (kind 0)) (crate-dep (name "itertools") (req "^0.10.0") (default-features #t) (kind 0)))) (hash "0wg3xqr6f9jbsn20i35ah4rfnjmlbb94yrs9f5zz3bv6n227yaaq") (features (quote (("dots-standard") ("default"))))))

(define-public crate-tengwar-0.9 (crate (name "tengwar") (vers "0.9.1") (deps (list (crate-dep (name "cfg-if") (req "^1.0.0") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^4.0.18") (features (quote ("cargo" "derive" "wrap_help"))) (default-features #t) (kind 0)) (crate-dep (name "itertools") (req "^0.10.0") (default-features #t) (kind 0)) (crate-dep (name "unicode-normalization") (req "^0.1.22") (default-features #t) (kind 0)))) (hash "039vyiqd23mfkn6jvz5wz4f1jj6f1sqh9vwx4n7rjrds29f5rzyn") (features (quote (("dots-standard") ("default"))))))

(define-public crate-tengwar-1 (crate (name "tengwar") (vers "1.0.0") (deps (list (crate-dep (name "cfg-if") (req "^1.0.0") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^4.0.18") (features (quote ("cargo" "derive" "wrap_help"))) (default-features #t) (kind 0)) (crate-dep (name "itertools") (req "^0.10.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.147") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "unicode-normalization") (req "^0.1.22") (default-features #t) (kind 0)))) (hash "1v2ckp1pvbrri7pl65gajnavad33zfzr96i8cxz5xabb64m7wf1b") (features (quote (("dots-standard") ("default") ("csur"))))))

(define-public crate-tengwar-1 (crate (name "tengwar") (vers "1.1.0") (deps (list (crate-dep (name "cfg-if") (req "^1.0.0") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^4.0.18") (features (quote ("cargo" "derive" "wrap_help"))) (default-features #t) (kind 0)) (crate-dep (name "itertools") (req "^0.11.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.147") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "unicode-normalization") (req "^0.1.22") (default-features #t) (kind 0)))) (hash "1rc9wnpbxjz5zc2cmx4sbk25373cv6xzcrbcf6gwb997i5hmrdyz") (features (quote (("mode-custom" "serde") ("dots-standard") ("default") ("csur"))))))

