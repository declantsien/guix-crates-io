(define-module (crates-io te cs) #:use-module (crates-io))

(define-public crate-tecs-0.1 (crate (name "tecs") (vers "0.1.0") (hash "1dl76m1qzzpmw6zw8bwkafyhnprh4skikjdyvmmks9vhgq9z98nz")))

(define-public crate-tecs-0.1 (crate (name "tecs") (vers "0.1.1") (hash "18860qkqqpjjwiz74gcv17zjy0m4w2j77b2l20xjrmswww8pi6gp")))

(define-public crate-tecs-1 (crate (name "tecs") (vers "1.0.0") (deps (list (crate-dep (name "itertools") (req "^0.9.0") (default-features #t) (kind 0)))) (hash "1fvrl1mww11xiys5ijr32wd4v8li6x0a6vi28wh3n5j4hldr0hqm")))

(define-public crate-tecs-1 (crate (name "tecs") (vers "1.1.0") (deps (list (crate-dep (name "itertools") (req "^0.9.0") (default-features #t) (kind 0)))) (hash "0bv4hj5gq129m6kanl2da60jli5kfzsnlgsp4r4x4832prydgn2m")))

(define-public crate-tecs_proc-0.1 (crate (name "tecs_proc") (vers "0.1.0") (deps (list (crate-dep (name "syn") (req "^2.0.18") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "1zz7zhaqw46azbw5i5nr5bqbgm49viphl6bfcqjlqg839jjwr7bp") (features (quote (("system") ("default")))) (yanked #t)))

