(define-module (crates-io te vm) #:use-module (crates-io))

(define-public crate-tevm-0.1 (crate (name "tevm") (vers "0.1.0") (hash "0qaji2nyhv6is9nqkldd2ri2gscx75mq0w3qkbzsvj80ra6077kd") (yanked #t)))

(define-public crate-tevm-0.2 (crate (name "tevm") (vers "0.2.0") (deps (list (crate-dep (name "clap") (req "^3.1") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "clap_complete") (req "^3.1") (default-features #t) (kind 0)))) (hash "1l4fwgwk0pcdv34hqjw5912k4zd78gkhzd622pzldisvyr5rv503") (yanked #t)))

(define-public crate-tevm-0.0.0 (crate (name "tevm") (vers "0.0.0") (deps (list (crate-dep (name "clap") (req "^3.1") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "clap_complete") (req "^3.1") (default-features #t) (kind 0)))) (hash "0fl4gpsq7p69bzkn1rc7z7jxf2q1c3234x9n6wdjxafbdyqs1yir") (yanked #t)))

(define-public crate-tevm-0.0.1 (crate (name "tevm") (vers "0.0.1") (hash "0hhhdap149rgg4if7dcb59bhz2g3dw67q6zzij2bzablg8flcs6v") (yanked #t)))

