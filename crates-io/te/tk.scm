(define-module (crates-io te tk) #:use-module (crates-io))

(define-public crate-tetkey-0.0.0 (crate (name "tetkey") (vers "0.0.0") (hash "0jbhrj3r96qddpl4lv74x61xaqj75y2d5kcj1p686g99lxz8dka6") (yanked #t)))

(define-public crate-tetkey-2 (crate (name "tetkey") (vers "2.0.0") (deps (list (crate-dep (name "structopt") (req "^0.3.14") (default-features #t) (kind 0)) (crate-dep (name "tc-cli") (req "^0.8.0") (default-features #t) (kind 0)))) (hash "1scjy01rs5ni5hfqpyi7zaw0j67m7yj261nmh802zfdk43d9wkxr")))

