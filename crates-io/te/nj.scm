(define-module (crates-io te nj) #:use-module (crates-io))

(define-public crate-tenjin-0.1 (crate (name "tenjin") (vers "0.1.0") (deps (list (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)))) (hash "0pxcss6jw240b3fhvcznwvnhgxgy3kas8irmbf81w7hdxsjx19z1")))

(define-public crate-tenjin-0.2 (crate (name "tenjin") (vers "0.2.0") (deps (list (crate-dep (name "htmlescape") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)))) (hash "139p38wgph68lfh92s81i2giwj2nyglsdcz2pvqb3l114zxbkv5z")))

(define-public crate-tenjin-0.3 (crate (name "tenjin") (vers "0.3.0") (deps (list (crate-dep (name "htmlescape") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "toml") (req "^0.4") (optional #t) (default-features #t) (kind 0)))) (hash "1ddvsazfakjdgy7pxcijs3yvkcgmhl1hmljh25j2cjbqphlj4rsh") (features (quote (("default" "serde_json" "toml"))))))

(define-public crate-tenjin-0.4 (crate (name "tenjin") (vers "0.4.0") (deps (list (crate-dep (name "htmlescape") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "toml") (req "^0.4") (optional #t) (default-features #t) (kind 0)))) (hash "04n43wwfbjf15m9k2lwfj9ff05k08habz8a8gc3bclgh140163q3") (features (quote (("default" "serde_json" "toml"))))))

(define-public crate-tenjin-0.4 (crate (name "tenjin") (vers "0.4.1") (deps (list (crate-dep (name "htmlescape") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "toml") (req "^0.4") (optional #t) (default-features #t) (kind 0)))) (hash "0a86rh50828a6ssrisn24xral22s7z70ayvz1jb94pnqscybw857") (features (quote (("default" "serde_json" "toml"))))))

(define-public crate-tenjin-0.4 (crate (name "tenjin") (vers "0.4.2") (deps (list (crate-dep (name "htmlescape") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "toml") (req "^0.4") (optional #t) (default-features #t) (kind 0)))) (hash "0f8a9mkyfyq4xm3asxg3006gm78q3i86bmdxz98h8gjacpl1xn6v") (features (quote (("default" "serde_json" "toml"))))))

(define-public crate-tenjin-0.5 (crate (name "tenjin") (vers "0.5.0") (deps (list (crate-dep (name "htmlescape") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "toml") (req "^0.4") (optional #t) (default-features #t) (kind 0)))) (hash "1zijvhdp3qk0fnndrnwb90r3qsqzpbsdr3slf1677hdrmadilp5g") (features (quote (("default" "serde_json" "toml"))))))

(define-public crate-tenjin-0.5 (crate (name "tenjin") (vers "0.5.1") (deps (list (crate-dep (name "htmlescape") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "toml") (req "^0.4") (optional #t) (default-features #t) (kind 0)))) (hash "179mfxaxflyrq85lpr62iaiy5pwx7m22d6sxgcxzkcr5fb8rj52h") (features (quote (("default" "serde_json" "toml"))))))

