(define-module (crates-io te eu) #:use-module (crates-io))

(define-public crate-teeu-0.1 (crate (name "teeu") (vers "0.1.0") (hash "10jbkmmpz337wj439vx9w5bzs52pqs2l46j0f2j7i9m77p4xh524")))

(define-public crate-teeuniverse-0.0.0 (crate (name "teeuniverse") (vers "0.0.0") (hash "1lw3n358dmb4mj6dqiwsj4yximnm3z6zfxgs4qca4p5zayicjx6x")))

