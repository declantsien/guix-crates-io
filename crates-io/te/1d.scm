(define-module (crates-io te #{1d}#) #:use-module (crates-io))

(define-public crate-te1d-0.0.1 (crate (name "te1d") (vers "0.0.1") (deps (list (crate-dep (name "ndarray") (req "^0.15.4") (default-features #t) (kind 0)))) (hash "1v4xf3paf8h232ip4klslpdibyj5wn83ncq7isl3rl7ngwb8lkyn")))

