(define-module (crates-io te xo) #:use-module (crates-io))

(define-public crate-texoder-0.0.1 (crate (name "texoder") (vers "0.0.1") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "1jpva0jky4103lj9kj4v024h0y7y7a0xv93lj27sjkrrallsr530")))

(define-public crate-texoder-0.0.2 (crate (name "texoder") (vers "0.0.2") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "1v22d1h5irh1p7gicymdg11j1429idajxx8qn99nggaj3zcj5dd4")))

(define-public crate-texoder-0.0.3 (crate (name "texoder") (vers "0.0.3") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "1as4pcw728xqd3vaql3c9x9d0qgi5mjidb0mxb9y43dbjg31c0nb")))

(define-public crate-texoder-0.0.4 (crate (name "texoder") (vers "0.0.4") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "17dyrdhgrmcw0d9d504xdp8c0nyxgn2ljw7ggs9wrswqy9z86dpb")))

(define-public crate-texoder-0.0.5 (crate (name "texoder") (vers "0.0.5") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "14nqkbvsjv8byb3jkgnd5041z4lpd7sm2jszgdb1kyg90n61g3w2")))

