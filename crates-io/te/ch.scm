(define-module (crates-io te ch) #:use-module (crates-io))

(define-public crate-tech-ui-0.1 (crate (name "tech-ui") (vers "0.1.0") (deps (list (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)))) (hash "0lvylgjiphw9dvl196sfn5cpw5k5c3rixlq0285d1v3il5w6mmln")))

(define-public crate-tech_analysis-0.1 (crate (name "tech_analysis") (vers "0.1.0") (deps (list (crate-dep (name "polars") (req "^0.32.1") (features (quote ("lazy" "describe"))) (default-features #t) (kind 0)))) (hash "00vlf4xbb8fz4kbabqv858cpa2pp392grb82z8isrd98cpis3a29")))

(define-public crate-tech_analysis-0.1 (crate (name "tech_analysis") (vers "0.1.1") (deps (list (crate-dep (name "polars") (req "^0.32.1") (features (quote ("lazy" "describe"))) (default-features #t) (kind 0)))) (hash "175gpvs4wkgi6b1qv49qd8s9mwqdmhpgb74bpfsm8zl8k054x8hx")))

(define-public crate-techlead-0.1 (crate (name "techlead") (vers "0.1.0") (deps (list (crate-dep (name "chat-gpt-lib-rs") (req "^0.1.5") (default-features #t) (kind 0)) (crate-dep (name "console") (req "^0.15.5") (default-features #t) (kind 0)) (crate-dep (name "dotenvy") (req "^0.15.6") (default-features #t) (kind 0)) (crate-dep (name "env_logger") (req "^0.10.0") (default-features #t) (kind 0)) (crate-dep (name "ignore") (req "^0.4.20") (default-features #t) (kind 0)) (crate-dep (name "indicatif") (req "^0.17.3") (default-features #t) (kind 0)) (crate-dep (name "keyring") (req "^2.0.1") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.26.0") (default-features #t) (kind 0)))) (hash "1sfik608kjalrd2drvgg8ng3i52lh6617qqmf9bkgmd4f112p5ah")))

(define-public crate-techlead-0.1 (crate (name "techlead") (vers "0.1.1") (deps (list (crate-dep (name "anyhow") (req "^1.0.70") (default-features #t) (kind 0)) (crate-dep (name "chat-gpt-lib-rs") (req "^0.2.1") (default-features #t) (kind 0)) (crate-dep (name "console") (req "^0.15.5") (default-features #t) (kind 0)) (crate-dep (name "dotenvy") (req "^0.15.6") (default-features #t) (kind 0)) (crate-dep (name "env_logger") (req "^0.10.0") (default-features #t) (kind 0)) (crate-dep (name "ignore") (req "^0.4.20") (default-features #t) (kind 0)) (crate-dep (name "indicatif") (req "^0.17.3") (default-features #t) (kind 0)) (crate-dep (name "keyring") (req "^2.0.1") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.26.0") (default-features #t) (kind 0)))) (hash "0lasz0khlbji1yma4zk6xsjs97jszh3iyika71acncxch99kyjxj")))

(define-public crate-techlead-0.2 (crate (name "techlead") (vers "0.2.0") (deps (list (crate-dep (name "anyhow") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "chat-gpt-lib-rs") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "console") (req "^0.15") (default-features #t) (kind 0)) (crate-dep (name "dotenvy") (req "^0.15") (default-features #t) (kind 0)) (crate-dep (name "env_logger") (req "^0.10") (default-features #t) (kind 0)) (crate-dep (name "ignore") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "indicatif") (req "^0.17") (default-features #t) (kind 0)) (crate-dep (name "keyring") (req "^2.0") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.33") (default-features #t) (kind 0)))) (hash "1v56x79gfsll636wy5nznxs8rk1f397cifv2z28pp6l3g6s1yin5")))

(define-public crate-technetium-0.0.0 (crate (name "technetium") (vers "0.0.0") (hash "0pydl7n08kwgk6mx1b022jnzn79zdxf3aw85qi7by0ln6y43k1xv")))

(define-public crate-technical_indicators-0.1 (crate (name "technical_indicators") (vers "0.1.0") (deps (list (crate-dep (name "assert_approx_eq") (req "^1") (default-features #t) (kind 2)) (crate-dep (name "json") (req "^0.11.9") (default-features #t) (kind 2)) (crate-dep (name "lazy_static") (req "^0.2") (default-features #t) (kind 2)))) (hash "1ldbanssvr8yz9n35pgnbgj9g7jf3x8cm5dbnj310f4xv6my2k20")))

(define-public crate-technical_indicators-0.2 (crate (name "technical_indicators") (vers "0.2.0") (deps (list (crate-dep (name "assert_approx_eq") (req "^1") (default-features #t) (kind 2)) (crate-dep (name "json") (req "^0.11.9") (default-features #t) (kind 2)) (crate-dep (name "lazy_static") (req "^0.2") (default-features #t) (kind 2)))) (hash "0fvnll1f0030qgmsmw5kdihwc87k36y56g256fy2h13l05zsmyr6")))

(define-public crate-technical_indicators-0.3 (crate (name "technical_indicators") (vers "0.3.0") (deps (list (crate-dep (name "assert_approx_eq") (req "^1") (default-features #t) (kind 2)) (crate-dep (name "json") (req "^0.11.12") (default-features #t) (kind 2)) (crate-dep (name "lazy_static") (req "^1.0") (default-features #t) (kind 2)))) (hash "0s3mw8l7xgs7nmf9hjf3wj133a8hd1awardx64nd2ldk1a2sgxkc")))

(define-public crate-technical_indicators-0.3 (crate (name "technical_indicators") (vers "0.3.1") (deps (list (crate-dep (name "assert_approx_eq") (req "^1") (default-features #t) (kind 2)) (crate-dep (name "json") (req "^0.11.12") (default-features #t) (kind 2)) (crate-dep (name "lazy_static") (req "^1.0") (default-features #t) (kind 2)))) (hash "0qgwnyrimlmqfwsgpy30k6q9l8w65nji68v8zsgvv6g9xigqgwj7")))

(define-public crate-technical_indicators-0.4 (crate (name "technical_indicators") (vers "0.4.0") (deps (list (crate-dep (name "assert_approx_eq") (req "^1") (default-features #t) (kind 2)) (crate-dep (name "json") (req "^0.11.12") (default-features #t) (kind 2)) (crate-dep (name "lazy_static") (req "^1.0") (default-features #t) (kind 2)))) (hash "1ihqjkxavy6wkcwgaq3yfh0vxc8rqh1lzdl6bljrzvig80n74lzm")))

(define-public crate-technical_indicators-0.5 (crate (name "technical_indicators") (vers "0.5.0") (deps (list (crate-dep (name "assert_approx_eq") (req "^1") (default-features #t) (kind 2)) (crate-dep (name "json") (req "^0.11.12") (default-features #t) (kind 2)) (crate-dep (name "lazy_static") (req "^1.0") (default-features #t) (kind 2)))) (hash "0d4lbd0fwydxqaqy9cb1lc82wrbipp3igcwipr18b90j9zbqv8kc")))

(define-public crate-technomancy-0.0.0 (crate (name "technomancy") (vers "0.0.0") (hash "086vziyfj7jp2zrlk09nxwwfif5z37miwr3km2iny1azlnbj7n9m")))

(define-public crate-technomancy_cc-0.0.0 (crate (name "technomancy_cc") (vers "0.0.0") (hash "1bck1j2vgyxnbgpj721gx7gp96gyzfl21vzp77qb699zvcqdwwqw")))

(define-public crate-technomancy_core-0.0.0 (crate (name "technomancy_core") (vers "0.0.0") (hash "1bcascvargnvg6v15jmij1k7hj85gdza789qcfjya6wcd4i6n374")))

(define-public crate-technomancy_engine-0.0.0 (crate (name "technomancy_engine") (vers "0.0.0") (hash "0l2j6qv1qwihzr9gcg3z134vzv6azyh5qzabbm7ffq982xva6n65")))

(define-public crate-technomancy_lib-0.0.0 (crate (name "technomancy_lib") (vers "0.0.0") (hash "0wwc7a5a0598nhhvwbrhf467n6fhlkwx6s75yr4gmcd8jbv23q92")))

