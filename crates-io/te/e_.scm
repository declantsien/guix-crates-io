(define-module (crates-io te e_) #:use-module (crates-io))

(define-public crate-tee_libc-0.0.1 (crate (name "tee_libc") (vers "0.0.1") (hash "1y8bv83hsg59a468lfyky3h7d1bwqcx2myp5rkzinv9bkr0wz5jc")))

(define-public crate-tee_readwrite-0.1 (crate (name "tee_readwrite") (vers "0.1.0") (hash "1y3a8v5g6gijd0vfh6jscap3556l6yhyfl5c2jyyal3k525aqhh6")))

(define-public crate-tee_readwrite-0.2 (crate (name "tee_readwrite") (vers "0.2.0") (hash "0mfrr3n1fgvffwsiyh22b52hlbr7nqy2sym86i0h0zxhkdifblsp")))

