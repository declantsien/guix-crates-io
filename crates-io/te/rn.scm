(define-module (crates-io te rn) #:use-module (crates-io))

(define-public crate-ternary-0.1 (crate (name "ternary") (vers "0.1.0") (hash "0h0pnfs3m80j9cixxaglbzk760pis1xxjvgx9v3d0psp8iplbvpl")))

(define-public crate-ternary-0.1 (crate (name "ternary") (vers "0.1.2") (hash "1zv7cfaj8kvx71w56zz21ywvgy7xz75xp48aa9y7bngxh61s3kh2") (features (quote (("unstable"))))))

(define-public crate-ternary-0.1 (crate (name "ternary") (vers "0.1.3") (hash "04fvi3bkjlxwgcsk8f71mg6l08w3w0ja2r961n33pjfbqyvmcims") (features (quote (("unstable"))))))

(define-public crate-ternary-operator-macro-0.0.1 (crate (name "ternary-operator-macro") (vers "0.0.1") (hash "1w3h3qb87j37fdnxvlm46jkfvd6mc3z18n9wrv7301nlmpns614r")))

(define-public crate-ternary-rs-1 (crate (name "ternary-rs") (vers "1.0.0") (hash "0canl6jdj52q837n3g6xr8g4a7cpwb7g9j57ww1mmjg384jgf6z6")))

(define-public crate-ternary-tree-0.0.1 (crate (name "ternary-tree") (vers "0.0.1") (hash "1ysy3pbr7p39gnw03byvkz3zm98l8jbyfjiyyfcya3vh86nrzzbq")))

(define-public crate-ternary-tree-0.0.2 (crate (name "ternary-tree") (vers "0.0.2") (hash "19crx4gafsx2fjjr4fjcyi9jpr6c3ywwwi62fj8qav7bdp15cblf")))

(define-public crate-ternary-tree-0.0.3 (crate (name "ternary-tree") (vers "0.0.3") (hash "0ibgc8kdsnqz9d1w5k6k12zi8gi7f072sj3mq7xxs8x0igq2zm2v")))

(define-public crate-ternary-tree-0.0.4 (crate (name "ternary-tree") (vers "0.0.4") (hash "1rk6cm8177k51cjn4mqxf18r1jz25iwk48vympj4q8fgkl29jyal")))

(define-public crate-ternary-tree-0.0.5 (crate (name "ternary-tree") (vers "0.0.5") (hash "1q338py1ly4b3dj1dzaqwn02d1cxkaazypr2yc5mr06wqf7mr6c5")))

(define-public crate-ternary-tree-0.0.6 (crate (name "ternary-tree") (vers "0.0.6") (hash "1lmzfykfn3ki146xbic1bqv57dxl9sf84lgzskdffch2fbrvavb9")))

(define-public crate-ternary-tree-0.1 (crate (name "ternary-tree") (vers "0.1.0") (hash "0xi2al9w4ii9d22ywqkalwr56687j1sjadq6gd5zg02698x1rywi")))

(define-public crate-ternary-tree-0.1 (crate (name "ternary-tree") (vers "0.1.1") (hash "1fviyx610bs2cm7h8zdfrw9k50v578vxzv81hgz3azp91bga52k8")))

(define-public crate-ternary-tree-wasm-0.0.1 (crate (name "ternary-tree-wasm") (vers "0.0.1") (deps (list (crate-dep (name "js-sys") (req "^0.3.32") (default-features #t) (kind 0)) (crate-dep (name "ternary-tree") (req "^0.1.1") (default-features #t) (kind 0)) (crate-dep (name "wasm-bindgen") (req "^0.2.55") (default-features #t) (kind 0)))) (hash "09b1x52n4g4s3blm9fyrbg84lqi8sjlyrlxs774z4lv6960bsanx")))

(define-public crate-ternary-tree-wasm-0.0.2 (crate (name "ternary-tree-wasm") (vers "0.0.2") (deps (list (crate-dep (name "js-sys") (req "^0.3.32") (default-features #t) (kind 0)) (crate-dep (name "ternary-tree") (req "^0.1.1") (default-features #t) (kind 0)) (crate-dep (name "wasm-bindgen") (req "^0.2.55") (default-features #t) (kind 0)))) (hash "03v181wr8yk5lnwqmmg7nz66j29v675jm9m4lk789r6m9nk8i0g8")))

(define-public crate-ternary-tree-wasm-0.0.3 (crate (name "ternary-tree-wasm") (vers "0.0.3") (deps (list (crate-dep (name "js-sys") (req "^0.3.32") (default-features #t) (kind 0)) (crate-dep (name "ternary-tree") (req "^0.1.1") (default-features #t) (kind 0)) (crate-dep (name "wasm-bindgen") (req "^0.2.55") (default-features #t) (kind 0)))) (hash "0kxydxmx1f8ykx6fvi8c6qcba9mfi7dsg6ln0fq1i0qn9ljmbyi5")))

(define-public crate-ternary-tree-wasm-0.0.4 (crate (name "ternary-tree-wasm") (vers "0.0.4") (deps (list (crate-dep (name "js-sys") (req "^0.3.32") (default-features #t) (kind 0)) (crate-dep (name "ternary-tree") (req "^0.1.1") (default-features #t) (kind 0)) (crate-dep (name "wasm-bindgen") (req "^0.2.55") (default-features #t) (kind 0)))) (hash "04bkrg1k5bafi22p9kck75cbzak16mmc4rv777plp3717pnj4vhn")))

(define-public crate-ternary-tree-wasm-0.0.5 (crate (name "ternary-tree-wasm") (vers "0.0.5") (deps (list (crate-dep (name "js-sys") (req "^0.3.32") (default-features #t) (kind 0)) (crate-dep (name "ternary-tree") (req "^0.1.1") (default-features #t) (kind 0)) (crate-dep (name "wasm-bindgen") (req "^0.2.55") (default-features #t) (kind 0)))) (hash "085ngfbd5kazpyfq34wpakr1rm7xbjw3ph0njgl8fpvncvgkviin")))

(define-public crate-ternary-tree-wasm-0.0.6 (crate (name "ternary-tree-wasm") (vers "0.0.6") (deps (list (crate-dep (name "js-sys") (req "^0.3.32") (default-features #t) (kind 0)) (crate-dep (name "ternary-tree") (req "^0.1.1") (default-features #t) (kind 0)) (crate-dep (name "wasm-bindgen") (req "^0.2.55") (default-features #t) (kind 0)))) (hash "1dl8xs2a8g3jxa68kfmlsvfq35jsd0mqfnxbfnwrffkk3lz44hz7")))

(define-public crate-ternoa-cli-0.1 (crate (name "ternoa-cli") (vers "0.1.0") (deps (list (crate-dep (name "anyhow") (req "^1.0.39") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^4.0.32") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "codec") (req "^3.0.0") (features (quote ("derive" "full" "bitvec"))) (kind 0) (package "parity-scale-codec")) (crate-dep (name "futures") (req "^0.3.13") (default-features #t) (kind 0)) (crate-dep (name "graphql_client") (req "^0.9.0") (default-features #t) (kind 0)) (crate-dep (name "hex") (req "^0.4.3") (default-features #t) (kind 0)) (crate-dep (name "hex-literal") (req "^0.3.4") (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.11.0") (features (quote ("json"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.114") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0.114") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.56") (default-features #t) (kind 0)) (crate-dep (name "sp-keyring") (req "^7.0.0") (default-features #t) (kind 0)) (crate-dep (name "subxt") (req "^0.25.0") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.8") (features (quote ("rt-multi-thread" "macros" "time"))) (default-features #t) (kind 0)) (crate-dep (name "tracing-subscriber") (req "^0.3.11") (default-features #t) (kind 0)))) (hash "1gd3gxlm48rfg0bkaawmma7daw2ybn788gkngwqrcmxm956nn82w") (features (quote (("command"))))))

(define-public crate-ternoa-cli-0.1 (crate (name "ternoa-cli") (vers "0.1.1") (deps (list (crate-dep (name "anyhow") (req "^1.0.39") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^4.0.32") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "codec") (req "^3.0.0") (features (quote ("derive" "full" "bitvec"))) (kind 0) (package "parity-scale-codec")) (crate-dep (name "futures") (req "^0.3.13") (default-features #t) (kind 0)) (crate-dep (name "graphql_client") (req "^0.9.0") (default-features #t) (kind 0)) (crate-dep (name "hex") (req "^0.4.3") (default-features #t) (kind 0)) (crate-dep (name "hex-literal") (req "^0.3.4") (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.11.0") (features (quote ("json"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.114") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0.114") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.56") (default-features #t) (kind 0)) (crate-dep (name "sp-keyring") (req "^7.0.0") (default-features #t) (kind 0)) (crate-dep (name "subxt") (req "^0.25.0") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.8") (features (quote ("rt-multi-thread" "macros" "time"))) (default-features #t) (kind 0)) (crate-dep (name "tracing-subscriber") (req "^0.3.11") (default-features #t) (kind 0)))) (hash "1rpgb3kmk6phx79ws1xw0jqd70r1dd9spj77k5z3dad5lyhwz93p") (features (quote (("command"))))))

(define-public crate-ternop-1 (crate (name "ternop") (vers "1.0.0") (hash "0lv5fqd0wkx6afbp21k4b8y0dhf69xxvkps0hdx00mfzcfxxqzn3") (yanked #t)))

(define-public crate-ternop-1 (crate (name "ternop") (vers "1.0.1") (hash "04810gzk0lgpb980gvkhl3ki574iasq72hsk52fah1a618ny6jlx")))

(define-public crate-terny-0.1 (crate (name "terny") (vers "0.1.0") (deps (list (crate-dep (name "quote") (req "^1.0.26") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2.0.15") (default-features #t) (kind 0)))) (hash "0ks94d86yh40pgflppkvjla7ddg0dqhg82i7s8y6qpqbd75qrckz")))

(define-public crate-terny-0.2 (crate (name "terny") (vers "0.2.0") (hash "1phms73nbly000v88sxrg6bgwn7mlm57x0bk16mh4hclp1z8r8ff")))

