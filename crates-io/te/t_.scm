(define-module (crates-io te t_) #:use-module (crates-io))

(define-public crate-tet_rs-0.1 (crate (name "tet_rs") (vers "0.1.1") (deps (list (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)))) (hash "114mlxlkk1yjpdbdj47hx5mjqh9h3ywysarghwyc2wisg9wxnvi1") (features (quote (("serde1" "serde"))))))

(define-public crate-tet_rs-0.1 (crate (name "tet_rs") (vers "0.1.2") (deps (list (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)))) (hash "0p2s504x6ly1phixdwnwm4fx3wawdlxpcs12hqksqhfq1pbx3zmq") (features (quote (("serde1" "serde"))))))

(define-public crate-tet_rs-0.2 (crate (name "tet_rs") (vers "0.2.1") (deps (list (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)))) (hash "130naxjjx85y64rsibbpy1kx10jbva9jd58i36w17v5fp1nbcy4b") (features (quote (("serde1" "serde"))))))

(define-public crate-tet_rs-0.2 (crate (name "tet_rs") (vers "0.2.2") (deps (list (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)))) (hash "11lk9iv16q0w53pnzg1b1zxa71n72c4kh8p1qsv3jn4z159dgijd") (features (quote (("serde1" "serde"))))))

(define-public crate-tet_rs-0.3 (crate (name "tet_rs") (vers "0.3.1") (deps (list (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)))) (hash "1nkj2hylky212ky2pmgaksngpyjdljlj1dfvhc8zgi1742ll9lc1") (features (quote (("serde1" "serde"))))))

