(define-module (crates-io te ac) #:use-module (crates-io))

(define-public crate-teach-0.0.1 (crate (name "teach") (vers "0.0.1") (hash "118bgda5q4qwdhbyib3gnwx48sl4ncwbjlmp42lfwxzrm08yrcrg")))

(define-public crate-teacup-0.1 (crate (name "teacup") (vers "0.1.0") (deps (list (crate-dep (name "structopt") (req "^0.3") (default-features #t) (kind 0)))) (hash "0hc6111ifzb9hwdd5aigis09c22974w18cd3hkb82723ipbbfv4d")))

