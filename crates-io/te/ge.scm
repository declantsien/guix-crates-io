(define-module (crates-io te ge) #:use-module (crates-io))

(define-public crate-tegen-0.1 (crate (name "tegen") (vers "0.1.0") (deps (list (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)))) (hash "0ii6ypppc5bfn5jk1aqgljjbbd7a1bhcx2m4lfm4xys9n18z0hvf")))

(define-public crate-tegen-0.1 (crate (name "tegen") (vers "0.1.1") (deps (list (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)))) (hash "0dxhyb20diz1cwymddqn0inki8mf7x339y689s0bqqa5qr0zv8gm")))

(define-public crate-tegen-0.1 (crate (name "tegen") (vers "0.1.2") (deps (list (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)))) (hash "1n6rxk5pznmc909lddv24fc90irjgd2rx6phhhgg3d1xykgn5k1b")))

(define-public crate-tegen-0.1 (crate (name "tegen") (vers "0.1.3") (deps (list (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)))) (hash "136kvfrsji7blgnrzd4wl7mhjfrx26a5ys44d424x5ms66igqi0l")))

(define-public crate-tegen-0.1 (crate (name "tegen") (vers "0.1.4") (deps (list (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)))) (hash "1k9a03cnj5ywbf6vs6wbn71p6r0kfnl395qk22s5kj5payixb8hh")))

