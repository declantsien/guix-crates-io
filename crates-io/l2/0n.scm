(define-module (crates-io l2 #{0n}#) #:use-module (crates-io))

(define-public crate-l20n-0.0.1 (crate (name "l20n") (vers "0.0.1") (hash "1chvaa9062qvp1jqfbw20c1a1kjqgybjsh2j78pcdbl8m37vpixr") (yanked #t)))

(define-public crate-l20n-0.1 (crate (name "l20n") (vers "0.1.0") (deps (list (crate-dep (name "serde") (req "^0.6") (default-features #t) (kind 0)))) (hash "08x5fyw8c1r9lbjmh8xxqycv0jawyxdsgaqs0vcwv2ky82j4aslx") (yanked #t)))

(define-public crate-l20n-0.1 (crate (name "l20n") (vers "0.1.1") (deps (list (crate-dep (name "serde") (req "^0.6") (default-features #t) (kind 0)))) (hash "0ikh6vkycc068smq784a3cl054v28xclqn57gckks0l3gvvvjmfg") (yanked #t)))

(define-public crate-l20n-0.1 (crate (name "l20n") (vers "0.1.2") (deps (list (crate-dep (name "serde") (req "^0.6") (default-features #t) (kind 0)))) (hash "1fgl3a6b5rld17y07gv96f1mcpdw598hdaywqxzf4ifn0r1lw1yq")))

