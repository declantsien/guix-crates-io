(define-module (crates-io l2 #{98}#) #:use-module (crates-io))

(define-public crate-l298n-0.1 (crate (name "l298n") (vers "0.1.0") (deps (list (crate-dep (name "embedded-hal") (req "^0.2.3") (default-features #t) (kind 0)))) (hash "1hcffxfmymav1a0almysj4v10z8rypz53qkzfhx9s0nz1zzqrh15")))

(define-public crate-l298n-0.1 (crate (name "l298n") (vers "0.1.1") (deps (list (crate-dep (name "embedded-hal") (req "^0.2.3") (default-features #t) (kind 0)))) (hash "03rylcksicqgybf5pnpdj559dsa7mhzgsd2lqkz5y0lnhrpklh6f")))

(define-public crate-l298n-0.1 (crate (name "l298n") (vers "0.1.2") (deps (list (crate-dep (name "embedded-hal") (req "^0.2.3") (default-features #t) (kind 0)))) (hash "0idjcjgxkv27h00cw6s9b2aq0i3lfmdw99r2c2qr7l7hjciyj1cg")))

(define-public crate-l298n-0.1 (crate (name "l298n") (vers "0.1.3") (deps (list (crate-dep (name "embedded-hal") (req "^0.2.3") (default-features #t) (kind 0)))) (hash "0p6qyam2jwx5zzmxp02igw6d01p7s3cql2r1pg15rayzmdqkh8s1")))

(define-public crate-l298n-0.2 (crate (name "l298n") (vers "0.2.0") (deps (list (crate-dep (name "embedded-hal") (req "^0.2.3") (default-features #t) (kind 0)))) (hash "1l95vqk8iimyf6y3klsj7ldhxvx6dlja2awmyjk4afxgmqvknhqx")))

