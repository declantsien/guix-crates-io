(define-module (crates-io l2 r0) #:use-module (crates-io))

(define-public crate-l2r0-profiler-guest-0.21 (crate (name "l2r0-profiler-guest") (vers "0.21.0") (hash "0m38rs2b27syfsqjnw9f68rfbclwdl4khkxwn5zd9jz5hnlk5sm8") (features (quote (("print-trace")))) (yanked #t)))

(define-public crate-l2r0-profiler-guest-0.20 (crate (name "l2r0-profiler-guest") (vers "0.20.1") (hash "0h5yxzpssiymmc2kcgs14nlss6s4f2splx6v17w73m64mr06iqgl") (features (quote (("print-trace"))))))

(define-public crate-l2r0-profiler-host-0.21 (crate (name "l2r0-profiler-host") (vers "0.21.0") (deps (list (crate-dep (name "ahash") (req "=0.8.6") (default-features #t) (kind 0)) (crate-dep (name "colored") (req "=2.0.4") (default-features #t) (kind 0)) (crate-dep (name "raki") (req "^0.1.3") (default-features #t) (kind 0)) (crate-dep (name "risc0-zkvm") (req "^0.20.1") (features (quote ("client"))) (default-features #t) (kind 0)))) (hash "1y1242hc7yg37hg5j580q3cby8ysd5xv5jy6qmrmc6pyxvmgmaca") (yanked #t)))

(define-public crate-l2r0-profiler-host-0.20 (crate (name "l2r0-profiler-host") (vers "0.20.1") (deps (list (crate-dep (name "ahash") (req "=0.8.6") (default-features #t) (kind 0)) (crate-dep (name "colored") (req "=2.0.4") (default-features #t) (kind 0)) (crate-dep (name "raki") (req "^0.1.3") (default-features #t) (kind 0)) (crate-dep (name "risc0-zkvm") (req "^0.20.1") (features (quote ("client"))) (default-features #t) (kind 0)))) (hash "0md874irz9yrfngx3qhphbjj7lw3h8ryywx4qc8c3clb4hva6v7b")))

(define-public crate-l2r0-small-serde-0.20 (crate (name "l2r0-small-serde") (vers "0.20.1") (deps (list (crate-dep (name "bytemuck") (req "^1.14.3") (default-features #t) (kind 0)) (crate-dep (name "risc0-zkvm") (req "^0.20.1") (kind 0)) (crate-dep (name "serde") (req "^1.0.196") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "1fbfvhvmahc70xbi8dmgcndqyl6kml9lza10wbvc5g5vpwdv4wls")))

