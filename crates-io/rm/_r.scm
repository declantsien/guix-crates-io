(define-module (crates-io rm _r) #:use-module (crates-io))

(define-public crate-rm_rf-0.1 (crate (name "rm_rf") (vers "0.1.0") (hash "1va7nphxfk98hjl9a6d4ix2d82jj6r60jjyv282s8kgki84yakdm")))

(define-public crate-rm_rf-0.1 (crate (name "rm_rf") (vers "0.1.1") (hash "1k1pfjjlrhkpr5qwn4v2n5fhxzwqylf6d6s9rcqx79bkivg3bjh0")))

(define-public crate-rm_rf-0.2 (crate (name "rm_rf") (vers "0.2.0") (hash "0prz576ydnibw0rbaqs14cm6fcjzm7zmmwcc4hz2p0kxy04wmw0d")))

(define-public crate-rm_rf-0.2 (crate (name "rm_rf") (vers "0.2.1") (deps (list (crate-dep (name "stacker") (req "^0.1") (default-features #t) (kind 0)))) (hash "13gy03qbr2w1f41m0jcy45cxx9x0xs3kdqgkipl78l492m7pwrv1")))

(define-public crate-rm_rf-0.2 (crate (name "rm_rf") (vers "0.2.2") (deps (list (crate-dep (name "stacker") (req "^0.1") (default-features #t) (kind 0)))) (hash "1lry4ypya4q62dg7fji192f9xpvv55k29m194ili8amckdida723")))

(define-public crate-rm_rf-0.2 (crate (name "rm_rf") (vers "0.2.3") (deps (list (crate-dep (name "stacker") (req "^0.1.5") (default-features #t) (kind 0)))) (hash "01ny6qaa3gb50kkb9scmixqs95abl0rrjnm1vr6b93ff3h6w11zv")))

(define-public crate-rm_rf-0.2 (crate (name "rm_rf") (vers "0.2.4") (deps (list (crate-dep (name "stacker") (req "^0.1.5") (default-features #t) (kind 0)))) (hash "0jxhgwghgbfmkwha8b4aqdcbrfrcg0g1ychakcp3qk6xj3lzbxbb")))

(define-public crate-rm_rf-0.3 (crate (name "rm_rf") (vers "0.3.0") (deps (list (crate-dep (name "stacker") (req "^0.1.5") (default-features #t) (kind 0)))) (hash "01ywxmm2y46yivy8mk65g0jgkml1qrzx7dx9ib1bisvwi73fy266")))

(define-public crate-rm_rf-0.4 (crate (name "rm_rf") (vers "0.4.0") (deps (list (crate-dep (name "stacker") (req "^0.1.6") (default-features #t) (kind 0)))) (hash "10hihzyq0ivvxmi9kgx1sjy4aj97j1dp3fpxl0bj8r6iqhdf49kp")))

(define-public crate-rm_rf-0.4 (crate (name "rm_rf") (vers "0.4.1") (deps (list (crate-dep (name "stacker") (req "^0.1.6") (default-features #t) (kind 0)))) (hash "1bj4m80awcfgwd5g4bkm6vdx24hgm58pgdkjkilj41dxv54mjr7v")))

(define-public crate-rm_rf-0.5 (crate (name "rm_rf") (vers "0.5.0") (deps (list (crate-dep (name "stacker") (req "^0.1.6") (default-features #t) (kind 0)))) (hash "0nfj87alyprywxcn2xnjml4avwb6dc8v6d9jnq2fv7ajqsminxls")))

(define-public crate-rm_rf-0.5 (crate (name "rm_rf") (vers "0.5.1") (deps (list (crate-dep (name "stacker") (req "^0.1.6") (default-features #t) (kind 0)))) (hash "0ywr1d2ac8d9qcrzz5a8nfgagnkwszkjsz411sfwhf96565j15h3")))

(define-public crate-rm_rf-0.6 (crate (name "rm_rf") (vers "0.6.0") (deps (list (crate-dep (name "stacker") (req "^0.1.6") (default-features #t) (kind 0)))) (hash "08fpfb2b3k8vks1p7fxkc1icc7wl3varhf87dl70428xb8avcyh4")))

(define-public crate-rm_rf-0.6 (crate (name "rm_rf") (vers "0.6.1") (deps (list (crate-dep (name "stacker") (req "^0.1.12") (default-features #t) (kind 0)))) (hash "0n66d1zkc59bibw5qh31pzwa2dif3bsvli9szl5a9pln9v6a4xlv")))

(define-public crate-rm_rf-0.6 (crate (name "rm_rf") (vers "0.6.2") (deps (list (crate-dep (name "stacker") (req "^0.1.14") (default-features #t) (kind 0)))) (hash "07ppl6pa3pw1k7i87k05hf2b8lvazszfrh6zkvlx4bm1baivfhrl")))

