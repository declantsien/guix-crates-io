(define-module (crates-io rm -c) #:use-module (crates-io))

(define-public crate-rm-config-0.1 (crate (name "rm-config") (vers "0.1.0") (deps (list (crate-dep (name "anyhow") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "toml") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "url") (req "^2.5") (default-features #t) (kind 0)) (crate-dep (name "xdg") (req "^2.5") (default-features #t) (kind 0)))) (hash "06lpbw6l5ipcvfkji7yqblvsg11jnb05jyqjya6damdmgp8zkafj")))

(define-public crate-rm-config-0.2 (crate (name "rm-config") (vers "0.2.0") (deps (list (crate-dep (name "anyhow") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "toml") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "url") (req "^2.5") (default-features #t) (kind 0)) (crate-dep (name "xdg") (req "^2.5") (default-features #t) (kind 0)))) (hash "1xgmjry9y2hwdhs271352nxw7ngsziga9x9gn0ylh5gk9nyp2gvx")))

