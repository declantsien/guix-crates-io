(define-module (crates-io rm ai) #:use-module (crates-io))

(define-public crate-rmail-0.0.1 (crate (name "rmail") (vers "0.0.1") (deps (list (crate-dep (name "clap") (req "^2.33") (default-features #t) (kind 0)) (crate-dep (name "dsn") (req "^1") (default-features #t) (kind 0)))) (hash "1g1xnma4gzk5l3pw7rykzl7adz1rmy3dcvy4ypglwh47qbkc6q0s")))

