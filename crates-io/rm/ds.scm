(define-module (crates-io rm ds) #:use-module (crates-io))

(define-public crate-rmds-0.1 (crate (name "rmds") (vers "0.1.0") (deps (list (crate-dep (name "walkdir") (req "^2.5.0") (default-features #t) (kind 0)))) (hash "166pn74sj21n9al13qwmmyc4jgfhh9j6nkp54n87ba5yx34nfnbx")))

