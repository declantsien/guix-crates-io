(define-module (crates-io rm ta) #:use-module (crates-io))

(define-public crate-rmtag2-0.0.0 (crate (name "rmtag2") (vers "0.0.0") (hash "15ar01ak3yy77qhas8vc5wl05awifh94ndps8j3qv9yrp6xfci96") (yanked #t)))

(define-public crate-rmtarget-0.1 (crate (name "rmtarget") (vers "0.1.0") (deps (list (crate-dep (name "anyhow") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "chrono") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^3.2") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "fs_extra") (req "^1.2") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.4") (default-features #t) (kind 0)))) (hash "1h1x6bhy7c5hg1bd4p74md37l66jdv9llhk4r1pm8psir7wi5fmz")))

