(define-module (crates-io rm p3) #:use-module (crates-io))

(define-public crate-rmp3-0.1 (crate (name "rmp3") (vers "0.1.0") (deps (list (crate-dep (name "cc") (req "^1.0") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "1pz4ys9d0g796vak4zrv845ms4prglls76gy8xplm7hy2b1mxzki") (features (quote (("only-mp3") ("no-simd") ("float") ("default" "only-mp3"))))))

(define-public crate-rmp3-0.2 (crate (name "rmp3") (vers "0.2.0") (deps (list (crate-dep (name "cc") (req "^1.0") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "0kqf6d339azcrrl90p1vkzblk6gm3cjq3q092iasvqbnfn72g7bw") (features (quote (("only-mp3") ("no-simd") ("float") ("default" "only-mp3"))))))

(define-public crate-rmp3-0.2 (crate (name "rmp3") (vers "0.2.1") (deps (list (crate-dep (name "cc") (req "^1.0") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "03sj0zc4ik1h4i5w0fxkzmq05y6alpbr2crdn9dlqml8225b7l3w") (features (quote (("only-mp3") ("no-simd") ("float") ("default" "only-mp3"))))))

(define-public crate-rmp3-0.3 (crate (name "rmp3") (vers "0.3.0") (deps (list (crate-dep (name "cc") (req "^1.0") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "0vy6jnq4ls47gg3ibr607wlp3xsqsyjfgwmw1cai6wxs77mkr3xg") (features (quote (("std") ("simd") ("nightly-docs") ("mp1-mp2") ("float") ("default" "simd"))))))

(define-public crate-rmp3-0.3 (crate (name "rmp3") (vers "0.3.1") (deps (list (crate-dep (name "cc") (req "^1.0") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "1bdi3b11si0aadgxzqchwams3gxm9pyq4xq0axf686d8i4ngpnfk") (features (quote (("std") ("simd") ("nightly-docs") ("mp1-mp2") ("float") ("default" "simd"))))))

