(define-module (crates-io rm du) #:use-module (crates-io))

(define-public crate-rmdup-0.1 (crate (name "rmdup") (vers "0.1.0") (deps (list (crate-dep (name "blake3") (req "^1.4") (features (quote ("rayon"))) (default-features #t) (kind 0)) (crate-dep (name "kagero") (req "^0.4") (features (quote ("printer"))) (kind 0)))) (hash "1qxladmc3975ziwdffaiv66iby5klha10c4w7g8vxgb91vjrcmvz")))

