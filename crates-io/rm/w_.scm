(define-module (crates-io rm w_) #:use-module (crates-io))

(define-public crate-rmw_config-0.0.7 (crate (name "rmw_config") (vers "0.0.7") (deps (list (crate-dep (name "const-str") (req "^0.4.3") (default-features #t) (kind 0)) (crate-dep (name "env_dir") (req "^0.0.7") (default-features #t) (kind 0)) (crate-dep (name "err") (req "^0.0.5") (default-features #t) (kind 0) (package "rmw_err")) (crate-dep (name "log") (req "^0.4.17") (default-features #t) (kind 0)) (crate-dep (name "parking_lot") (req "^0.12.1") (default-features #t) (kind 0)) (crate-dep (name "rmw_str") (req "^0.0.7") (default-features #t) (kind 0)))) (hash "1qxl8hk7dg1v5qicmf1kz7pizprk13dpj8m271n938zxxnyhk8s6") (features (quote (("default"))))))

(define-public crate-rmw_err-0.0.4 (crate (name "rmw_err") (vers "0.0.4") (deps (list (crate-dep (name "anyhow") (req "^1.0.58") (default-features #t) (kind 2)) (crate-dep (name "log") (req "^0.4.17") (default-features #t) (kind 0)))) (hash "04g68zizdapdsw4d9l61m191bmaqabxiakqsxrdm9qs6vmpryzb9")))

(define-public crate-rmw_err-0.0.5 (crate (name "rmw_err") (vers "0.0.5") (deps (list (crate-dep (name "anyhow") (req "^1.0.58") (default-features #t) (kind 2)) (crate-dep (name "log") (req "^0.4.17") (default-features #t) (kind 0)))) (hash "1nxhp4n0ys9n8vsaz0pfrw5k1zvclbf9j3nnpgwrkmy5qflk63hl")))

(define-public crate-rmw_log-0.0.8 (crate (name "rmw_log") (vers "0.0.8") (deps (list (crate-dep (name "chrono") (req "^0.4.19") (default-features #t) (kind 0)) (crate-dep (name "colored") (req "^2.0.0") (default-features #t) (kind 0)) (crate-dep (name "fern") (req "^0.6.1") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.17") (default-features #t) (kind 0)))) (hash "01xsa2i67gcql65jrsmg2fn5hh9q4zz3rdwk3wx6vfh1vx65pixv") (features (quote (("default"))))))

(define-public crate-rmw_log-0.0.9 (crate (name "rmw_log") (vers "0.0.9") (deps (list (crate-dep (name "chrono") (req "^0.4.19") (default-features #t) (kind 0)) (crate-dep (name "colored") (req "^2.0.0") (default-features #t) (kind 0)) (crate-dep (name "fern") (req "^0.6.1") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.17") (default-features #t) (kind 0)))) (hash "10b0gyqdcz95mcp1pkxsflk8iaxnh2vgwdgqias4z6ypbxdyh1jb") (features (quote (("default"))))))

(define-public crate-rmw_str-0.0.1 (crate (name "rmw_str") (vers "0.0.1") (deps (list (crate-dep (name "anyhow") (req "^1.0.47") (default-features #t) (kind 0)))) (hash "1q2x4p98gjjsg89qqan211dl40z5vb9815rfpiqk6zvpc3z5d2xd")))

(define-public crate-rmw_str-0.0.2 (crate (name "rmw_str") (vers "0.0.2") (deps (list (crate-dep (name "anyhow") (req "^1.0.52") (default-features #t) (kind 0)))) (hash "1zs3p9jmdi8mx0nwc71451mvm5dqp2kzabvxk2bbs5fbzbnnddbb")))

(define-public crate-rmw_str-0.0.5 (crate (name "rmw_str") (vers "0.0.5") (deps (list (crate-dep (name "anyhow") (req "^1.0.52") (default-features #t) (kind 0)))) (hash "0nkg732wid928qgax657dkf04zbgjm2as8zp5hv0kgda4aac4160")))

(define-public crate-rmw_str-0.0.6 (crate (name "rmw_str") (vers "0.0.6") (deps (list (crate-dep (name "anyhow") (req "^1.0.52") (default-features #t) (kind 0)))) (hash "0x84fbxfnwcjcrjchm1w7m0fw24s2qsgh7ryg2j14lskidiz3683")))

(define-public crate-rmw_str-0.0.7 (crate (name "rmw_str") (vers "0.0.7") (deps (list (crate-dep (name "anyhow") (req "^1.0.55") (default-features #t) (kind 0)))) (hash "1kbmv8q60p2gncvlw5ryj7jswp0s23lv27mzm77362cbk2l207dy")))

(define-public crate-rmw_ttlmap-0.2 (crate (name "rmw_ttlmap") (vers "0.2.2") (deps (list (crate-dep (name "async-lock") (req "^2.4.0") (default-features #t) (kind 0)) (crate-dep (name "async-std") (req "^1.10.0") (features (quote ("attributes"))) (default-features #t) (kind 2)) (crate-dep (name "async-timer") (req "^0.7.4") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.14") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.4") (default-features #t) (kind 0)) (crate-dep (name "simple_logger") (req "^1.13.0") (default-features #t) (kind 2)) (crate-dep (name "smol") (req "^1.2.5") (default-features #t) (kind 2)) (crate-dep (name "tokio") (req "^1.14.0") (features (quote ("full"))) (default-features #t) (kind 2)))) (hash "0nlmwlb46d1wv5ns5zanp9w7ag5dgckpfn879fkvz9v8q3f6hhv4")))

