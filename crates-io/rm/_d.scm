(define-module (crates-io rm _d) #:use-module (crates-io))

(define-public crate-rm_ds_store-0.1 (crate (name "rm_ds_store") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^4.1.8") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "glob") (req "^0.3.1") (default-features #t) (kind 0)))) (hash "02nc0ixm18wb6p41flvml6as3va3hmw49kpvsahc4p0vqdcpnzgi")))

(define-public crate-rm_ds_store-0.1 (crate (name "rm_ds_store") (vers "0.1.1") (deps (list (crate-dep (name "clap") (req "^4.1.8") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "glob") (req "^0.3.1") (default-features #t) (kind 0)))) (hash "0r9nnza2hkn2mj9qvk7k2d7aal0vwc1rl6i23njms0pqs0ff20lb")))

(define-public crate-rm_ds_store-0.1 (crate (name "rm_ds_store") (vers "0.1.2") (deps (list (crate-dep (name "clap") (req "^4.1.8") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "glob") (req "^0.3.1") (default-features #t) (kind 0)))) (hash "0dngs8q87290fdgynxxsn1g006yp11bg9z55k3h7qp47s2b3a79r")))

