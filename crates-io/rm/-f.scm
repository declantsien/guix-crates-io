(define-module (crates-io rm -f) #:use-module (crates-io))

(define-public crate-rm-frust5-api-0.0.1 (crate (name "rm-frust5-api") (vers "0.0.1") (deps (list (crate-dep (name "hdf5") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "resolve-path") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "0a5162j98cki878v0jigmn6npphg6h7f0hry4ilq8lr4lgd1392i") (rust-version "1.58.0")))

(define-public crate-rm-frust5-api-0.0.2 (crate (name "rm-frust5-api") (vers "0.0.2") (deps (list (crate-dep (name "hdf5") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "resolve-path") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "0x2qlfmg8ryfcsa49ivhspsyxwmpjayzv7cabfnzh3h67h05dy0b") (rust-version "1.58.0")))

(define-public crate-rm-frust5-api-0.0.3 (crate (name "rm-frust5-api") (vers "0.0.3") (deps (list (crate-dep (name "hdf5") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "resolve-path") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "1kq15iqy28s2qqa8p1lwryjarvidfwkq542azq6ipjnyn8dx5hw0") (rust-version "1.58.0")))

