(define-module (crates-io rm #{1a}#) #:use-module (crates-io))

(define-public crate-rm1asm-1 (crate (name "rm1asm") (vers "1.0.1") (deps (list (crate-dep (name "ariadne") (req "^0.1.5") (default-features #t) (kind 0)) (crate-dep (name "chumsky") (req "^0.8.0") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^2.33.3") (default-features #t) (kind 0)) (crate-dep (name "logos") (req "^0.12.0") (default-features #t) (kind 0)))) (hash "1h7iwrnlp0ma6h5xxv18yarar738m2q8925ql2syv0cmgzg1x1xx")))

(define-public crate-rm1asm-1 (crate (name "rm1asm") (vers "1.0.0") (deps (list (crate-dep (name "ariadne") (req "^0.1.5") (default-features #t) (kind 0)) (crate-dep (name "chumsky") (req "^0.8.0") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^2.33.3") (default-features #t) (kind 0)) (crate-dep (name "logos") (req "^0.12.0") (default-features #t) (kind 0)))) (hash "07gh1czbcvl77h9xdvn2xzsp1a9mgx46sxln79klkd6qs72nwhgd")))

(define-public crate-rm1asm-1 (crate (name "rm1asm") (vers "1.0.2") (deps (list (crate-dep (name "ariadne") (req "^0.1.5") (default-features #t) (kind 0)) (crate-dep (name "chumsky") (req "^0.8.0") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^2.33.3") (default-features #t) (kind 0)) (crate-dep (name "logos") (req "^0.12.0") (default-features #t) (kind 0)))) (hash "0l3g4jk278c0np5sswg1rznn3v45pl9sl2svjwzj8mk96hvnax7i")))

(define-public crate-rm1asm-1 (crate (name "rm1asm") (vers "1.0.3") (deps (list (crate-dep (name "ariadne") (req "^0.1.5") (default-features #t) (kind 0)) (crate-dep (name "chumsky") (req "^0.8.0") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^2.33.3") (default-features #t) (kind 0)) (crate-dep (name "logos") (req "^0.12.0") (default-features #t) (kind 0)))) (hash "1cfrjq2vasyh78m167m0qf6zksqpbcc3kki2li5nldczr43jfmji")))

