(define-module (crates-io rm tg) #:use-module (crates-io))

(define-public crate-rmtg_core-0.1 (crate (name "rmtg_core") (vers "0.1.0") (hash "0zk02059w53k0imjp2xfyqrvbbc73hyrsxrdczak7g7ddaw9774k")))

(define-public crate-rmtg_core-0.1 (crate (name "rmtg_core") (vers "0.1.1") (hash "0n3fi85hdn2g3cwcqiycf9gvz3vjmhpf1k1v4sa16i2rprijmdb5")))

(define-public crate-rmtg_core-0.1 (crate (name "rmtg_core") (vers "0.1.2") (hash "0bg90xq98izqzvgxdig3b9aa5ckgndcba6rqd4iqhrcx7yxl27ss")))

(define-public crate-rmtg_server-0.1 (crate (name "rmtg_server") (vers "0.1.0") (deps (list (crate-dep (name "rmtg_core") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "1vrxaphzijwm95xlx1vcfmw9y5bwy2fbdwb2jw4bpjsszxm049a6")))

(define-public crate-rmtg_server-0.1 (crate (name "rmtg_server") (vers "0.1.1") (deps (list (crate-dep (name "rmtg_core") (req "^0.1.2") (default-features #t) (kind 0)))) (hash "0pqbcbszx57qwxkbwg8la8rknzbp14wivs6ylpb5n2x351zc9jw2")))

(define-public crate-rmtg_server-0.1 (crate (name "rmtg_server") (vers "0.1.2") (deps (list (crate-dep (name "rmtg_core") (req "^0.1.2") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "132mm0k2nyi7xff9vh6f530p3jkw66h5av8mr1y1fka98bz9nac5")))

