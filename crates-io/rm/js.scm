(define-module (crates-io rm js) #:use-module (crates-io))

(define-public crate-rmjs-0.1 (crate (name "rmjs") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^3.2.17") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "exitcode") (req "^1.1.2") (default-features #t) (kind 0)) (crate-dep (name "glob") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "rayon") (req "^1.5.3") (default-features #t) (kind 0)))) (hash "119w3nrflgigm5spj7k9g69nx4kqn5b8kfk2fjy4s7slgy742fgx")))

