(define-module (crates-io rm bg) #:use-module (crates-io))

(define-public crate-rmbg-0.1 (crate (name "rmbg") (vers "0.1.0") (deps (list (crate-dep (name "anyhow") (req "^1.0.81") (default-features #t) (kind 0)) (crate-dep (name "fast_image_resize") (req "^3.0.4") (default-features #t) (kind 0)) (crate-dep (name "image") (req "^0.25.0") (default-features #t) (kind 0)) (crate-dep (name "ndarray") (req "^0.15.6") (default-features #t) (kind 0)) (crate-dep (name "ort") (req "^2.0.0-rc.1") (features (quote ("ndarray"))) (kind 0)) (crate-dep (name "ort") (req "^2.0.0-rc.1") (features (quote ("download-binaries"))) (kind 2)))) (hash "01bwjp0nknnraxcg4783ynin9lj9gxk6xznvpxx26bdyswx66mfj")))

