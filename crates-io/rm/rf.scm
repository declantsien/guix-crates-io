(define-module (crates-io rm rf) #:use-module (crates-io))

(define-public crate-rmrf-0.1 (crate (name "rmrf") (vers "0.1.0") (deps (list (crate-dep (name "trash") (req "^3.0") (features (quote ("coinit_speed_over_memory"))) (default-features #t) (kind 0)))) (hash "1qvjabr0868wz1nfbi284l2vdrd2k1i89hhzsjzfkcky9xxa5jmb")))

(define-public crate-rmrf-0.1 (crate (name "rmrf") (vers "0.1.1") (deps (list (crate-dep (name "trash") (req "^3.0") (features (quote ("coinit_speed_over_memory"))) (default-features #t) (kind 0)))) (hash "0m2mzlq8n1l4xjc6c2a2678pzvkaczawg19p8k31h2x0cr1yb5vb")))

(define-public crate-rmrf-0.1 (crate (name "rmrf") (vers "0.1.2") (deps (list (crate-dep (name "trash") (req "^3.0") (features (quote ("coinit_speed_over_memory"))) (default-features #t) (kind 0)))) (hash "1bx68xqihy94qpzi59nd4p3rnfa4l7b8ds0kda48fpnl7zr1277v")))

(define-public crate-rmrf-0.1 (crate (name "rmrf") (vers "0.1.3") (deps (list (crate-dep (name "trash") (req "^3.0") (features (quote ("coinit_speed_over_memory"))) (default-features #t) (kind 0)))) (hash "04g1pzf3y0z19v05mfsws7s32i17xhfqrsax35j8cvqb37nvvbny")))

(define-public crate-rmrf-0.1 (crate (name "rmrf") (vers "0.1.4") (deps (list (crate-dep (name "argh") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "trash") (req "^3.0") (features (quote ("coinit_speed_over_memory"))) (default-features #t) (kind 0)))) (hash "0gc0zcph0apbifin1spas3as2nnvis7gzydwb40r8cgzfjpifjjz")))

(define-public crate-rmrf-0.1 (crate (name "rmrf") (vers "0.1.5") (deps (list (crate-dep (name "argh") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "trash") (req "^3.0") (features (quote ("coinit_speed_over_memory"))) (default-features #t) (kind 0)))) (hash "1am22l736ix5b09q6c3vzf6k90hy7d3bmxlbzdsbps5qxkfvna2h")))

(define-public crate-rmrf-0.1 (crate (name "rmrf") (vers "0.1.6") (deps (list (crate-dep (name "argh") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "trash") (req "^3.0") (features (quote ("coinit_speed_over_memory"))) (default-features #t) (kind 0)))) (hash "07p32z9yl1sslwk7n3f6x3rmx3virl5zlca56ranz5py3n076xmm")))

(define-public crate-rmrfd-0.0.0 (crate (name "rmrfd") (vers "0.0.0") (hash "11xj2zkgzbxq32kgl5w26av50z7i8skzyfyxlr50nksypq281152")))

