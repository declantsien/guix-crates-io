(define-module (crates-io rm id) #:use-module (crates-io))

(define-public crate-rmididings-0.1 (crate (name "rmididings") (vers "0.1.0") (deps (list (crate-dep (name "alsa") (req "^0.5.0") (default-features #t) (kind 0)))) (hash "0bh3awlix57ka0rq4qgp4fcnhw4rs563v9z5mal9mg3yiyh5m428")))

(define-public crate-rmididings-0.1 (crate (name "rmididings") (vers "0.1.1") (deps (list (crate-dep (name "alsa") (req "^0.5.0") (default-features #t) (kind 0)))) (hash "0x5nbdnn7x5mfpz3hlym5b4srics945kxcdyj78013xal8fcf6pb")))

(define-public crate-rmididings-0.1 (crate (name "rmididings") (vers "0.1.2") (deps (list (crate-dep (name "alsa") (req "^0.5.0") (default-features #t) (kind 0)))) (hash "0xr0j50vqfgxniv2ww8kb5x4mqr7fcwb4zsv040q2g3nvssaw35y")))

(define-public crate-rmididings-0.2 (crate (name "rmididings") (vers "0.2.0") (deps (list (crate-dep (name "alsa") (req "^0.5.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "dbus") (req "^0.9.3") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2.97") (default-features #t) (kind 0)) (crate-dep (name "rosc") (req "^0.5.2") (optional #t) (default-features #t) (kind 0)))) (hash "1dqg0b49akmc64lh481p1hkg35q4x1p6l2zjp73s8fjpyjxsj8pp") (features (quote (("osc" "rosc") ("default" "alsa" "osc"))))))

(define-public crate-rmididings-0.2 (crate (name "rmididings") (vers "0.2.1") (deps (list (crate-dep (name "alsa") (req "^0.5.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "dbus") (req "^0.9.3") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2.97") (default-features #t) (kind 0)) (crate-dep (name "rosc") (req "^0.5.2") (optional #t) (default-features #t) (kind 0)))) (hash "0g8y2djl8drawmjwj1axddaid40dk5bpmwqk7h6h9b7v2nlr90n6") (features (quote (("osc" "rosc") ("default" "alsa" "osc"))))))

