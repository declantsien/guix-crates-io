(define-module (crates-io rm im) #:use-module (crates-io))

(define-public crate-rmime-0.1 (crate (name "rmime") (vers "0.1.0") (hash "110k6y00nhqk3nbd8x2kfbnz09b1slr8kym5rk6gcql7gwpn6xkf")))

(define-public crate-rmime-0.1 (crate (name "rmime") (vers "0.1.1") (hash "1r80hr4ryslif5ydjyk6is4cai93mmamyd89gzsj7597n9fl9yk7")))

(define-public crate-rmime-0.1 (crate (name "rmime") (vers "0.1.2") (hash "1xdwbh9dwgln45c46gb2404bm7vg5w1vi1skgx8scq8s735d8day")))

(define-public crate-rmime-0.1 (crate (name "rmime") (vers "0.1.3") (hash "0halxab3rlsdq2byvnw9m7vzgsa2yx3sbzsh61j4cxzjb733n7rv")))

