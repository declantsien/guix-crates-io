(define-module (crates-io rm -l) #:use-module (crates-io))

(define-public crate-rm-lines-0.1 (crate (name "rm-lines") (vers "0.1.0") (deps (list (crate-dep (name "nom") (req "^7.1.1") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.31") (default-features #t) (kind 0)))) (hash "1wvpipbvd152xsm9wv9537b1x8n3yl4phmki6dj6cprz9s6gkn9f")))

