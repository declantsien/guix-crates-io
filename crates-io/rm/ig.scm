(define-module (crates-io rm ig) #:use-module (crates-io))

(define-public crate-rmigrate-0.1 (crate (name "rmigrate") (vers "0.1.0") (hash "171y0ynafp8356pbxd9af0iiq6mhw3pzv1yidl2z6wclh71cgw4z")))

(define-public crate-rmigrate-cli-0.1 (crate (name "rmigrate-cli") (vers "0.1.0") (hash "18w1a3nv2a7q343fvifvzv9j3pl6hbh122igx2xnhlnxkh0y2bzs")))

(define-public crate-rmigrate-core-0.1 (crate (name "rmigrate-core") (vers "0.1.0") (hash "0rl2jnvz1jjxm1yraay2axrd2mywy215r510mr7m23j76zsssjd6")))

(define-public crate-rmigrate-sdk-0.1 (crate (name "rmigrate-sdk") (vers "0.1.0") (hash "0bx7l76saf9ykbaj5n9a62hhgdgi2gb9ngxgmffisrl4grgqvkra")))

