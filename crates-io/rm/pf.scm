(define-module (crates-io rm pf) #:use-module (crates-io))

(define-public crate-rmpfit-0.1 (crate (name "rmpfit") (vers "0.1.0") (deps (list (crate-dep (name "assert_approx_eq") (req "^1.1.0") (default-features #t) (kind 2)))) (hash "0c48iv3xp3wccg9s304si7vq4j5wxk7d2p99a8ms4vpgjqyn6xii")))

(define-public crate-rmpfit-0.1 (crate (name "rmpfit") (vers "0.1.1") (deps (list (crate-dep (name "assert_approx_eq") (req "^1.1.0") (default-features #t) (kind 2)))) (hash "1vd4994239f2n0gyx23jpiqnax78ipkbcmlxkkf87crzy120yrxx")))

(define-public crate-rmpfit-0.2 (crate (name "rmpfit") (vers "0.2.0") (deps (list (crate-dep (name "assert_approx_eq") (req "^1.1") (default-features #t) (kind 2)))) (hash "0dsxyvh02mps08m57yn585xy5iv0vdjki4kpm12vib14z44s4b19")))

(define-public crate-rmpfit-0.3 (crate (name "rmpfit") (vers "0.3.0") (deps (list (crate-dep (name "assert_approx_eq") (req "^1.1") (default-features #t) (kind 2)))) (hash "1379liwq26qcvajrq8h0zwrx4q93s9dn6r6qmiq6djmiaswpl6im")))

