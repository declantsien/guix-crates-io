(define-module (crates-io rm -r) #:use-module (crates-io))

(define-public crate-rm-rs-0.2 (crate (name "rm-rs") (vers "0.2.0") (deps (list (crate-dep (name "assert_cmd") (req "^2.0.14") (default-features #t) (kind 2)) (crate-dep (name "clap") (req "^4.5.4") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "path-absolutize") (req "^3.1.1") (default-features #t) (kind 0)) (crate-dep (name "predicates") (req "^3.1.0") (default-features #t) (kind 2)))) (hash "09ncvywvib6nf1z1fdsrgl7hzv3s03zps6lbznnfngv9sk8wl2qx")))

(define-public crate-rm-rs-0.2 (crate (name "rm-rs") (vers "0.2.1") (deps (list (crate-dep (name "assert_cmd") (req "^2.0.14") (default-features #t) (kind 2)) (crate-dep (name "clap") (req "^4.5.4") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "path-absolutize") (req "^3.1.1") (default-features #t) (kind 0)) (crate-dep (name "predicates") (req "^3.1.0") (default-features #t) (kind 2)))) (hash "1npz3g9jn66fgj9c8sj3dmrmcqqgd1s45rrjik95ncaq2xaw51f5")))

(define-public crate-rm-rs-0.2 (crate (name "rm-rs") (vers "0.2.2") (deps (list (crate-dep (name "assert_cmd") (req "^2.0.14") (default-features #t) (kind 2)) (crate-dep (name "clap") (req "^4.5.4") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "path-absolutize") (req "^3.1.1") (default-features #t) (kind 0)) (crate-dep (name "predicates") (req "^3.1.0") (default-features #t) (kind 2)))) (hash "0ym1cjdzsgkdbypxcv4h62vbxmbxrqczrdzbn33m8qx798amxvkc")))

