(define-module (crates-io rm gl) #:use-module (crates-io))

(define-public crate-rmgl-1 (crate (name "rmgl") (vers "1.0.0") (deps (list (crate-dep (name "gl") (req "^0.14") (default-features #t) (kind 0)) (crate-dep (name "image") (req "^0.25.1") (default-features #t) (kind 0)))) (hash "1ww3yzw8wn8p5498f7s507nli80nxsjzrnfl509dy13cg2gr3hp1") (yanked #t)))

(define-public crate-rmgl-1 (crate (name "rmgl") (vers "1.0.1") (deps (list (crate-dep (name "gl") (req "^0.14") (default-features #t) (kind 0)) (crate-dep (name "image") (req "^0.25.1") (default-features #t) (kind 0)))) (hash "0xgl1sh64n64pkwyx4dwj38fkx3bk84qgy70wsgqvi829lwhgdkd") (yanked #t)))

(define-public crate-rmgl-1 (crate (name "rmgl") (vers "1.0.2") (deps (list (crate-dep (name "gl") (req "^0.14") (default-features #t) (kind 0)) (crate-dep (name "image") (req "^0.25.1") (default-features #t) (kind 0)))) (hash "1730xjjm6j2wp5fjwansyx2rsx4w9617s2wwn8jvnmssqvwk4b2h") (yanked #t)))

(define-public crate-rmgl-1 (crate (name "rmgl") (vers "1.0.3") (deps (list (crate-dep (name "gl") (req "^0.14") (default-features #t) (kind 0)) (crate-dep (name "image") (req "^0.25.1") (default-features #t) (kind 0)))) (hash "183s6qvgiz69df3gww2lal5fqgxh4xjzwkmig07yvabvfzfmiyq4") (yanked #t)))

(define-public crate-rmgl-1 (crate (name "rmgl") (vers "1.0.4") (deps (list (crate-dep (name "gl") (req "^0.14") (default-features #t) (kind 0)) (crate-dep (name "image") (req "^0.25.1") (default-features #t) (kind 0)))) (hash "0fqjidpja6gqriiwm74p2q7rvi62caj61i7223la0jv14dckbl9y")))

