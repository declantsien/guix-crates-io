(define-module (crates-io rm en) #:use-module (crates-io))

(define-public crate-rmenu-0.1 (crate (name "rmenu") (vers "0.1.0") (deps (list (crate-dep (name "conrod") (req "0.54.*") (features (quote ("glium" "winit"))) (default-features #t) (kind 0)))) (hash "1n6jyd5csawq5hv9jpgnx50b3xjd6zapf1v47hfiapmqmfv4nq6q")))

(define-public crate-rmenu-0.1 (crate (name "rmenu") (vers "0.1.1") (deps (list (crate-dep (name "conrod") (req "0.54.*") (features (quote ("glium" "winit"))) (default-features #t) (kind 0)))) (hash "0r51na3x1f5jgmxbqnwvibmamcd45cqyh9h74gq9d26hwlpbgayf")))

(define-public crate-rmenu-0.1 (crate (name "rmenu") (vers "0.1.2") (deps (list (crate-dep (name "conrod") (req "0.56.*") (features (quote ("glium" "winit"))) (default-features #t) (kind 0)))) (hash "12ylc2bwnrx7kg0b28vhaj9wybhx0vv280qyjghmdv69c5bqdswv")))

(define-public crate-rmenu-0.1 (crate (name "rmenu") (vers "0.1.3") (deps (list (crate-dep (name "conrod") (req "0.56.*") (features (quote ("glium" "winit"))) (default-features #t) (kind 0)))) (hash "1lds9q6dddn554bn1dyx5n7zscmfzpvk7r8pvc6pacajqsi054fq")))

(define-public crate-rmenu-0.1 (crate (name "rmenu") (vers "0.1.4") (deps (list (crate-dep (name "conrod") (req "0.58.*") (features (quote ("glium" "winit"))) (default-features #t) (kind 0)))) (hash "07l3wvzdnhm0kv41cp72044m4i54chgc7ji8rj60qp6c60ilqxmk")))

