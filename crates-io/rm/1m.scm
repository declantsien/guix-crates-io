(define-module (crates-io rm #{1m}#) #:use-module (crates-io))

(define-public crate-rm1masm-1 (crate (name "rm1masm") (vers "1.0.0") (deps (list (crate-dep (name "ariadne") (req "^0.1.5") (default-features #t) (kind 0)) (crate-dep (name "chumsky") (req "^0.8.0") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^2.33.3") (default-features #t) (kind 0)) (crate-dep (name "logos") (req "^0.12.0") (default-features #t) (kind 0)))) (hash "0a3nc6c4igzhx63j0aklmqmpmzy23caa3p9fnvvax34la3qk23yl")))

(define-public crate-rm1masm-1 (crate (name "rm1masm") (vers "1.0.1") (deps (list (crate-dep (name "ariadne") (req "^0.1.5") (default-features #t) (kind 0)) (crate-dep (name "chumsky") (req "^0.8.0") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^2.33.3") (default-features #t) (kind 0)) (crate-dep (name "logos") (req "^0.12.0") (default-features #t) (kind 0)))) (hash "0br5mnzikrzrr041x38hfh4wj1b4xbz47p8bw5vpb423klzi6n3x")))

(define-public crate-rm1masm-1 (crate (name "rm1masm") (vers "1.0.2") (deps (list (crate-dep (name "ariadne") (req "^0.1.5") (default-features #t) (kind 0)) (crate-dep (name "chumsky") (req "^0.8.0") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^2.33.3") (default-features #t) (kind 0)) (crate-dep (name "logos") (req "^0.12.0") (default-features #t) (kind 0)))) (hash "0fd9k0mwb8vadni9xx1y6q9f941327bw7idnc2mq5w7l3kbjvday")))

(define-public crate-rm1masm-1 (crate (name "rm1masm") (vers "1.0.3") (deps (list (crate-dep (name "ariadne") (req "^0.1.5") (default-features #t) (kind 0)) (crate-dep (name "chumsky") (req "^0.8.0") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^2.33.3") (default-features #t) (kind 0)) (crate-dep (name "logos") (req "^0.12.0") (default-features #t) (kind 0)))) (hash "0zixrhs9pybcajcz93hq180zrvfdmrisl967by44rxk1zzjm4a50")))

