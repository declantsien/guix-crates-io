(define-module (crates-io rm at) #:use-module (crates-io))

(define-public crate-rmath-0.1 (crate (name "rmath") (vers "0.1.0") (hash "161fk4bh36jancnd76fab3bwm149js865zgzsxi1rwwj2bfplk15")))

(define-public crate-rmath-0.1 (crate (name "rmath") (vers "0.1.1") (hash "07x6fsq9alxylvk7asc03jmc9qnbzk0g5wfvyr3fd45j7z1qfxa2")))

(define-public crate-rmath-0.1 (crate (name "rmath") (vers "0.1.2") (hash "18ikn8dhmicsc1cr0x7vawrsphij41caq4z4fklb1ydj5bmig86a")))

(define-public crate-rmath-0.1 (crate (name "rmath") (vers "0.1.3") (hash "0yqb45f7h7c358brwcw16bvmbaq4hgbglhcw96kk0rksmphbijqj")))

(define-public crate-rmath-0.1 (crate (name "rmath") (vers "0.1.4") (hash "14k1wjzvxvfkcy2k232yhyj2n288x3idq0v9mav83pmxda233905")))

(define-public crate-rmath-0.1 (crate (name "rmath") (vers "0.1.5") (hash "0a2986s7n1sm8zjvxjj79hp3rfbimg14dphvgagfs25hr34w7djb")))

(define-public crate-rmath-cli-0.1 (crate (name "rmath-cli") (vers "0.1.0") (hash "1qgy6hjyp3rkjhd1wxv0shs4a98dx1rxafz0jssjl61ivcprj7rn")))

(define-public crate-rmath-rs-0.1 (crate (name "rmath-rs") (vers "0.1.0") (deps (list (crate-dep (name "assert_approx_eq") (req "^1.1.0") (default-features #t) (kind 0)))) (hash "1q8b8bd13rm6la1knq8zc8s93y9xx1x143fv3q90lljsa9xa7h7g") (yanked #t)))

(define-public crate-rmathlib-1 (crate (name "rmathlib") (vers "1.0.0") (deps (list (crate-dep (name "libm") (req "^0.2") (default-features #t) (kind 0)))) (hash "01vqa9qk1ygfw8vsm669qcsysm8ni19yj6wi8n3hbx45wzal0bsa") (rust-version "1.56.1")))

(define-public crate-rmathlib-1 (crate (name "rmathlib") (vers "1.1.0") (deps (list (crate-dep (name "libm") (req "^0.2") (default-features #t) (kind 0)))) (hash "1p1cca0xjdckp5cbfzjh7y787q9symkr94pph0d49q7p9vxfd385") (rust-version "1.56.1")))

(define-public crate-rMaths-0.1 (crate (name "rMaths") (vers "0.1.0") (hash "1n3yi7ym207kjb9xi9pn5rgs8r55334jdjc3i1j24yddcwv6pyi1")))

(define-public crate-rmatrix-0.1 (crate (name "rmatrix") (vers "0.1.0") (deps (list (crate-dep (name "ncurses") (req "^5.99") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.6") (default-features #t) (kind 0)))) (hash "0lpvbh6q6ybda14ljhrvfcvyyr8yi6503f0cilqr07hviaaaih8f")))

(define-public crate-rmatrix-0.1 (crate (name "rmatrix") (vers "0.1.1") (deps (list (crate-dep (name "ncurses") (req "^5.99") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.6") (default-features #t) (kind 0)))) (hash "0whc4b4c5bh8rn01lzbdrw8b3yxmc4y6y1hbf2mslbhpyprznzmv")))

(define-public crate-rmatrix_ks-0.2 (crate (name "rmatrix_ks") (vers "0.2.0") (deps (list (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)))) (hash "06yy7iy9xb6skyraf2vc70vmlwzlbsvqik0gix4ryp0ykxshg44k")))

(define-public crate-rmatrix_ks-0.2 (crate (name "rmatrix_ks") (vers "0.2.1") (deps (list (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)))) (hash "0bpqa8pkfvfc3nbq63yc5p8j8lnhd4a7np561bdlhpdsagffd7bi")))

(define-public crate-rmatte-hello-rust-0.1 (crate (name "rmatte-hello-rust") (vers "0.1.0") (hash "1d2f5cbn35gdn3sjxbg53fqxnrxw865ypshg87sjy4fzj2g7gnck") (yanked #t)))

