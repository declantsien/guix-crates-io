(define-module (crates-io rm ju) #:use-module (crates-io))

(define-public crate-rmjunk-0.1 (crate (name "rmjunk") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^4.3.21") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "junk_file") (req "^0.1.1") (default-features #t) (kind 0)))) (hash "1k57rjddm03dk4xp2a3vypr3xmbmgf8jscnsr834aglsyr8mhx85")))

