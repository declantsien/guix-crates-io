(define-module (crates-io s_ cu) #:use-module (crates-io))

(define-public crate-s_curve-0.1 (crate (name "s_curve") (vers "0.1.0") (deps (list (crate-dep (name "gnuplot") (req "^0.0.37") (default-features #t) (kind 2)))) (hash "1q1ii6ipaid7y528lwplyl6viaqk7lcwg3mixr4fds6d8yvp2bm2")))

(define-public crate-s_curve-0.1 (crate (name "s_curve") (vers "0.1.1") (deps (list (crate-dep (name "gnuplot") (req "^0.0.37") (default-features #t) (kind 2)))) (hash "01h7gx5rkjfxw9jn49b6b96zhrmh0dvk0a86sjyh4idr3i6ra2mg")))

(define-public crate-s_curve-0.1 (crate (name "s_curve") (vers "0.1.2") (deps (list (crate-dep (name "gnuplot") (req "^0.0.37") (default-features #t) (kind 2)))) (hash "159123i0zif79m11am8byihqdal4n9pjf59fmklvskvig20j298b")))

(define-public crate-s_curve-0.1 (crate (name "s_curve") (vers "0.1.3") (deps (list (crate-dep (name "gnuplot") (req "^0.0.37") (default-features #t) (kind 2)))) (hash "1ml3bjsfvw76xdhlm53a4ykyhdjpjgrk6ifpl0277sggxzcw2cn0")))

(define-public crate-s_curve-0.1 (crate (name "s_curve") (vers "0.1.4") (deps (list (crate-dep (name "gnuplot") (req "^0.0.37") (default-features #t) (kind 2)))) (hash "1w6smadwjiiybfxnrka6f3nbdfspnpff9y6mnsnglfbgzxlmlkld")))

(define-public crate-s_curve-0.1 (crate (name "s_curve") (vers "0.1.5") (deps (list (crate-dep (name "gnuplot") (req "^0.0.37") (default-features #t) (kind 2)))) (hash "16j25wzzqgm2aqkz1wzydihdlikwp4181hg7khgmm8kwcifahdv1")))

(define-public crate-s_curve-0.1 (crate (name "s_curve") (vers "0.1.6") (deps (list (crate-dep (name "gnuplot") (req "^0.0.37") (default-features #t) (kind 2)))) (hash "146zd1pvf73klrk7vx1w798d1pw2c61b97mdvywarpnd4c87qaqz")))

(define-public crate-s_curve-0.1 (crate (name "s_curve") (vers "0.1.7") (deps (list (crate-dep (name "gnuplot") (req "^0.0.37") (default-features #t) (kind 2)))) (hash "06lx83a89xnpandsm3biq81nzpx9b3c1999vaasa8hrqw3f1wl3d")))

