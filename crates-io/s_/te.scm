(define-module (crates-io s_ te) #:use-module (crates-io))

(define-public crate-s_test_fixture-0.1 (crate (name "s_test_fixture") (vers "0.1.0") (deps (list (crate-dep (name "quote") (req "^1.0.8") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.57") (features (quote ("full" "visit" "extra-traits" "visit-mut"))) (default-features #t) (kind 0)))) (hash "1gbhllvbkip4qslkfndia4rsagy1acwyhpckqrabgqld6shn27w9")))

(define-public crate-s_test_fixture-0.1 (crate (name "s_test_fixture") (vers "0.1.1") (deps (list (crate-dep (name "quote") (req "^1.0.8") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.57") (features (quote ("full" "visit" "extra-traits" "visit-mut"))) (default-features #t) (kind 0)))) (hash "0qh0a8s2fqsj65xwkkz6y0m608riqcw6imxqk0bdky7jis6pih64")))

(define-public crate-s_test_fixture-0.1 (crate (name "s_test_fixture") (vers "0.1.2") (deps (list (crate-dep (name "quote") (req "^1.0.8") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.57") (features (quote ("full" "visit" "extra-traits" "visit-mut"))) (default-features #t) (kind 0)))) (hash "1590w7kw4rcxghd72ccpp17f9zagw17sz84jywjfqzsap3grg3sx")))

(define-public crate-s_test_fixture-0.1 (crate (name "s_test_fixture") (vers "0.1.3") (deps (list (crate-dep (name "quote") (req "^1.0.8") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.57") (features (quote ("full" "visit" "extra-traits" "visit-mut"))) (default-features #t) (kind 0)))) (hash "1nwywr7jgb7adwrqmzrhvxcg4slsgbj7khxz2ah9f3bd5bl4rzik")))

(define-public crate-s_test_fixture-0.1 (crate (name "s_test_fixture") (vers "0.1.4") (deps (list (crate-dep (name "quote") (req "^1.0.8") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.57") (features (quote ("full" "visit" "extra-traits" "visit-mut"))) (default-features #t) (kind 0)))) (hash "0bsw7aj5fg5y7ldyiay9wfhy4lgnyy0sqjw1jqqwcfxp6ivw90jb")))

(define-public crate-s_test_fixture-0.1 (crate (name "s_test_fixture") (vers "0.1.5") (deps (list (crate-dep (name "quote") (req "^1.0.8") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.57") (features (quote ("full" "visit" "extra-traits" "visit-mut"))) (default-features #t) (kind 0)))) (hash "1lmah2698hj4rr3kja7nvf5jqwa9hh02prh2zam5inkc3vhpq8y4")))

(define-public crate-s_test_fixture-0.1 (crate (name "s_test_fixture") (vers "0.1.6") (deps (list (crate-dep (name "quote") (req "^1.0.8") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.57") (features (quote ("full" "visit" "extra-traits" "visit-mut"))) (default-features #t) (kind 0)))) (hash "1xmdd9457rwg0b16hm9h8xirqg6bxycc57wmf390hfwrflm8ifb8")))

(define-public crate-s_test_fixture-0.1 (crate (name "s_test_fixture") (vers "0.1.7") (deps (list (crate-dep (name "quote") (req "^1.0.8") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.57") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "1v2l2lg1g2dgrfi28sp9ia6jlz48jp0fm4rb4rgjbirq8kx3c7jr")))

(define-public crate-s_test_fixture-0.1 (crate (name "s_test_fixture") (vers "0.1.8") (deps (list (crate-dep (name "quote") (req "^1.0.8") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.57") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "1nhwd7nd585mk7j4n9cpqb4911khhzk6fcz6xbgnk1i4xwimvs8g")))

