(define-module (crates-io s_ no) #:use-module (crates-io))

(define-public crate-s_nor-1 (crate (name "s_nor") (vers "1.0.0") (deps (list (crate-dep (name "clearscreen") (req "^3.0.0") (default-features #t) (kind 0)) (crate-dep (name "colored") (req "^2.1.0") (default-features #t) (kind 0)))) (hash "099c4wwxjassycb58fr8k4r2clmw07jya32gj9gbrf29c9ml1akh")))

(define-public crate-s_nor-1 (crate (name "s_nor") (vers "1.0.1") (deps (list (crate-dep (name "clearscreen") (req "^3.0.0") (default-features #t) (kind 0)) (crate-dep (name "colored") (req "^2.1.0") (default-features #t) (kind 0)))) (hash "1n4xbnnnc7v52mlvsp5ga5ssaa21y3jxdrqqljqk5lzmsyl50115")))

