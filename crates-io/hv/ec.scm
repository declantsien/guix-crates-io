(define-module (crates-io hv ec) #:use-module (crates-io))

(define-public crate-hvec-0.1 (crate (name "hvec") (vers "0.1.0") (deps (list (crate-dep (name "criterion") (req "~0.3.5") (default-features #t) (kind 2)))) (hash "0fr0c7vb75sy2hcj69fwv3lhdcgv23ckynqjzl067v63h9c4c3fj")))

(define-public crate-hvec-0.2 (crate (name "hvec") (vers "0.2.0") (deps (list (crate-dep (name "criterion") (req "~0.3.5") (default-features #t) (kind 2)))) (hash "0bm7ywkd5m6s1xkw7r71n34z7ilfk2rbb0msrfmckyl518xp84qk") (features (quote (("no_drop") ("default"))))))

(define-public crate-hvec-0.3 (crate (name "hvec") (vers "0.3.0") (deps (list (crate-dep (name "criterion") (req "~0.3.5") (default-features #t) (kind 2)))) (hash "0pgny7v73hrm4ak2vwq30lh10haclxylkd7x7yhm8ywgwmfy04zj") (features (quote (("no_drop") ("default"))))))

(define-public crate-hvec-0.4 (crate (name "hvec") (vers "0.4.0") (deps (list (crate-dep (name "criterion") (req "~0.3.5") (default-features #t) (kind 2)))) (hash "1729jqdrxwdwj4my7zh5y2qqi2liym2fdvmz24nwix8yikwz4avs") (features (quote (("type_assertions") ("no_drop") ("default" "type_assertions"))))))

