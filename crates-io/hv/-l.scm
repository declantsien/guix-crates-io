(define-module (crates-io hv -l) #:use-module (crates-io))

(define-public crate-hv-lease-tracker-0.1 (crate (name "hv-lease-tracker") (vers "0.1.0") (deps (list (crate-dep (name "slab") (req "^0.4.5") (default-features #t) (kind 0)) (crate-dep (name "spin") (req "^0.9.2") (default-features #t) (kind 0)))) (hash "1i2km21q0nwnb8qkx17dbpj08hzq6zjq1qz8i2c1brbzshgxaz9s")))

