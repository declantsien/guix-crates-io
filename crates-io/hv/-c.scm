(define-module (crates-io hv -c) #:use-module (crates-io))

(define-public crate-hv-cell-0.1 (crate (name "hv-cell") (vers "0.1.0") (deps (list (crate-dep (name "hv-guarded-borrow") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "hv-lease-tracker") (req "^0.1.0") (optional #t) (default-features #t) (kind 0)))) (hash "0b84nf7yal0kik17yf3wqsj4gx06lhyzk6sa0p22jp1p77pgq1y3") (features (quote (("track-leases" "hv-lease-tracker"))))))

