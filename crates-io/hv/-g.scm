(define-module (crates-io hv -g) #:use-module (crates-io))

(define-public crate-hv-guarded-borrow-0.1 (crate (name "hv-guarded-borrow") (vers "0.1.0") (deps (list (crate-dep (name "hv-ecs") (req "^0.1.0") (optional #t) (default-features #t) (kind 0)))) (hash "1c3z2smc823f6y8hv86gqnn6sgcz2z9yzg2airvlamydwnznc5rp") (features (quote (("std") ("default"))))))

(define-public crate-hv-guarded-borrow-0.1 (crate (name "hv-guarded-borrow") (vers "0.1.1") (deps (list (crate-dep (name "hv-ecs") (req "^0.1.0") (optional #t) (default-features #t) (kind 0)))) (hash "1vi1c6gpw669kwpi8h055bds7q6ms6hqjkhacyygh9jdjy8h9v36") (features (quote (("std") ("default"))))))

