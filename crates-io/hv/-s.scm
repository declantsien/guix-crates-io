(define-module (crates-io hv -s) #:use-module (crates-io))

(define-public crate-hv-stampede-0.1 (crate (name "hv-stampede") (vers "0.1.0") (deps (list (crate-dep (name "bumpalo") (req "^3.8.0") (features (quote ("allocator_api" "boxed"))) (default-features #t) (kind 0)) (crate-dep (name "spin") (req "^0.9.2") (default-features #t) (kind 0)))) (hash "1lsgfgc7wz2z2zij23dkahqlmpl2y6j9hn95647i6vv53n93cdak")))

(define-public crate-hv-stampede-0.2 (crate (name "hv-stampede") (vers "0.2.0") (deps (list (crate-dep (name "bumpalo") (req "^3.8.0") (features (quote ("allocator_api" "boxed" "collections"))) (default-features #t) (kind 0)) (crate-dep (name "spin") (req "^0.9.2") (default-features #t) (kind 0)))) (hash "0igs3mwffg4hc7cb2xibmcmwrnjh81jkqfypwxc9l5ywrmd0wlsd")))

(define-public crate-hv-stampede-0.2 (crate (name "hv-stampede") (vers "0.2.1") (deps (list (crate-dep (name "bumpalo") (req "^3.8.0") (features (quote ("allocator_api" "boxed" "collections"))) (default-features #t) (kind 0)) (crate-dep (name "spin") (req "^0.9.2") (default-features #t) (kind 0)))) (hash "06bibpxm8wl3mqxl05iam9y2ykrkw18wcmj4l9zlwg85k7z3h1px")))

(define-public crate-hv-sys-0.1 (crate (name "hv-sys") (vers "0.1.0") (deps (list (crate-dep (name "bindgen") (req "^0.58") (default-features #t) (kind 1)))) (hash "07danmcqwkz9fimalbmwxahxfd3r0zznd8kibhws5z2m982r32b5")))

(define-public crate-hv-sys-0.1 (crate (name "hv-sys") (vers "0.1.1") (deps (list (crate-dep (name "bindgen") (req "^0.58") (default-features #t) (kind 1)))) (hash "01lfj67zyjmlg4l7cclv258g5djggnmrzjv1ji3sxhbnj163lcym")))

