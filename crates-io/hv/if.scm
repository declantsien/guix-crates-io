(define-module (crates-io hv if) #:use-module (crates-io))

(define-public crate-hvif-0.1 (crate (name "hvif") (vers "0.1.0") (deps (list (crate-dep (name "bitflags") (req "^1.3") (default-features #t) (kind 0)) (crate-dep (name "cairo-rs") (req "^0.14") (optional #t) (kind 0)) (crate-dep (name "nom") (req "^7") (default-features #t) (kind 0)))) (hash "1m30agphc6lddvn7ilx2c4id36ck4y8fydm2sf0y989b0mv3496h")))

(define-public crate-hvif-0.1 (crate (name "hvif") (vers "0.1.1") (deps (list (crate-dep (name "bitflags") (req "^1.3") (default-features #t) (kind 0)) (crate-dep (name "cairo-rs") (req "^0.14") (optional #t) (kind 0)) (crate-dep (name "nom") (req "^7") (default-features #t) (kind 0)))) (hash "17ibl28lqbfhsrysnb0r4ivbicwwaxrlnnn6x8mxvpzck704hl69")))

