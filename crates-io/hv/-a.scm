(define-module (crates-io hv -a) #:use-module (crates-io))

(define-public crate-hv-alchemy-0.1 (crate (name "hv-alchemy") (vers "0.1.0") (deps (list (crate-dep (name "hashbrown") (req "^0.11.2") (default-features #t) (kind 0)) (crate-dep (name "hv-atom") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "hv-cell") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.4.0") (features (quote ("spin_no_std"))) (default-features #t) (kind 0)) (crate-dep (name "spin") (req "^0.9.2") (default-features #t) (kind 0)) (crate-dep (name "static_assertions") (req "^1.1.0") (default-features #t) (kind 0)))) (hash "1v9rmzq0i64wvfdiklw9jnmb85g3zrf5jxshs12310cznn8w530b")))

(define-public crate-hv-atom-0.1 (crate (name "hv-atom") (vers "0.1.0") (hash "1ay8hq6xd640dzci0i07raihrq8vxx6c1xck66pm30md9ka2fb05")))

