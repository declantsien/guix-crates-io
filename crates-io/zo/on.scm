(define-module (crates-io zo on) #:use-module (crates-io))

(define-public crate-zoon-0.1 (crate (name "zoon") (vers "0.1.0") (hash "03xx7fi7f6p6icngb8fskpm79933a6flcpqiglk4s0h0ymkfylvk")))

(define-public crate-zoon-0.1 (crate (name "zoon") (vers "0.1.1") (hash "0s5a5263c3b4ibfgn59b2dq47ch54fgckgifdsg3g6x82wc959ih")))

(define-public crate-zoon-0.1 (crate (name "zoon") (vers "0.1.2") (hash "0m9d61w16p6yqn4sbp7bkgkywlvigg5x315725gk4a6bbi9xa20s")))

(define-public crate-zoon-0.1 (crate (name "zoon") (vers "0.1.3") (hash "15lld3hglw1y6qmlzxrgyvjva2nivnql8yhs9lfc8p8zwhy48yxj")))

(define-public crate-zoon-0.1 (crate (name "zoon") (vers "0.1.5") (hash "0c23p9xi3sxx4lshwayxg5gcdb4ysx2afzx9zdgrxfcrf94y98sk")))

(define-public crate-zoon-0.1 (crate (name "zoon") (vers "0.1.6") (hash "1apcq0sx9hrbyhhzpmi6hjnpkb66b3m9w6gpva902xs66h6cl3ps")))

