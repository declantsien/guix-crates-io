(define-module (crates-io zo ml) #:use-module (crates-io))

(define-public crate-zoml-0.1 (crate (name "zoml") (vers "0.1.0") (hash "19vnvcrig5h4y8wqxbf62m4saxv53m0bihpj0dwaj6m02m21avg8") (yanked #t)))

(define-public crate-zoml-0.1 (crate (name "zoml") (vers "0.1.1") (deps (list (crate-dep (name "doe") (req "^0.1.71") (default-features #t) (kind 0)))) (hash "0mkfhdgq9f36vb09mh2jb2kd3x9zdvlikz96x4ndi3cbrd0crw0y") (yanked #t)))

(define-public crate-zoml-0.1 (crate (name "zoml") (vers "0.1.2") (deps (list (crate-dep (name "doe") (req "^0.1.71") (default-features #t) (kind 0)))) (hash "12ldskpja8k2bgfzd1aydc2inm8v70jw9khbgmidkxlhlq0i5prh") (yanked #t)))

