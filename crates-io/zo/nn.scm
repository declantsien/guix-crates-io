(define-module (crates-io zo nn) #:use-module (crates-io))

(define-public crate-zonne-0.1 (crate (name "zonne") (vers "0.1.0") (hash "1sbdwj9rm5x1v78mx023in0sfizy2j49ggmypn8lmjfhjd74zl1x")))

(define-public crate-zonne-0.2 (crate (name "zonne") (vers "0.2.0") (hash "1ia2wdhy1qm51802wnm0g4yxqglmp59gwczd9idc0ckjgz3v5k7a")))

(define-public crate-zonne-0.2 (crate (name "zonne") (vers "0.2.1") (hash "124frqd54gc977krri9ndpx6lb5zz00j3s60985ilr8889pfr8nd")))

(define-public crate-zonne-0.2 (crate (name "zonne") (vers "0.2.2") (hash "18h9zb67hnqnbjcaj78is1w4kz9g8pnvsz42vxx0apdmq8wic77n")))

(define-public crate-zonne-0.2 (crate (name "zonne") (vers "0.2.3") (hash "1w3f14nfavqpqqq4f11s3506yr9lx1izsxp4b3qgx0061pgjl2cn")))

