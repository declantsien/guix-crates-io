(define-module (crates-io zo lt) #:use-module (crates-io))

(define-public crate-zolt-0.5 (crate (name "zolt") (vers "0.5.0") (deps (list (crate-dep (name "colored") (req "^2") (default-features #t) (kind 0)))) (hash "0yyfw3qynyprpmffgxsbyv45z1wv0i5nislnmpqd1srr8hdkv75z")))

(define-public crate-zolt-0.6 (crate (name "zolt") (vers "0.6.0") (deps (list (crate-dep (name "colored") (req "^2") (default-features #t) (kind 0)))) (hash "02vs5z31ma2v2gr7cbv96hy107ryxq9cc092nqxpg2sqkqrninh9")))

(define-public crate-zolt-0.7 (crate (name "zolt") (vers "0.7.0") (deps (list (crate-dep (name "colored") (req "^2") (default-features #t) (kind 0)) (crate-dep (name "zolt-macros") (req "=0.7.0") (default-features #t) (kind 0)))) (hash "1j1w3bpzz3xb5sdsskndal54kx3f6c1470g6jh116qqs8qpr21i5")))

(define-public crate-zolt-0.7 (crate (name "zolt") (vers "0.7.1") (deps (list (crate-dep (name "colored") (req "^2") (default-features #t) (kind 0)) (crate-dep (name "zolt-macros") (req "=0.7.1") (default-features #t) (kind 0)))) (hash "1hmc87zsz7iaf6c2s6gnl2gpvyiqxwkmk4rj65x2sbvzp0hs9ps5")))

(define-public crate-zolt-0.7 (crate (name "zolt") (vers "0.7.2") (deps (list (crate-dep (name "colored") (req "^2") (default-features #t) (kind 0)) (crate-dep (name "zolt-macros") (req "=0.7.2") (default-features #t) (kind 0)))) (hash "0jlzij9shw5xrhqihmjr8q62593z2qpr6zkkvdyhhqrx4r8kdj7q")))

(define-public crate-zolt-macros-0.1 (crate (name "zolt-macros") (vers "0.1.0") (hash "0nq9kby7gpppxfalj5hmlbvng5kjiif1hs1f5lnyqg49czhyywk6")))

(define-public crate-zolt-macros-0.7 (crate (name "zolt-macros") (vers "0.7.0") (deps (list (crate-dep (name "colored") (req "^2") (default-features #t) (kind 0)) (crate-dep (name "zolt-proc") (req "=0.7.0") (default-features #t) (kind 0)))) (hash "106v3w2vcyg457n62ip9jrypjl2ly93bxd8biaj46abw98j28phf")))

(define-public crate-zolt-macros-0.7 (crate (name "zolt-macros") (vers "0.7.1") (deps (list (crate-dep (name "colored") (req "^2") (default-features #t) (kind 0)) (crate-dep (name "zolt-proc") (req "=0.7.1") (default-features #t) (kind 0)))) (hash "0swdynqc6c6g752443mazmdh53y252vvd2l9x59ika0l9ghvcj7f")))

(define-public crate-zolt-macros-0.7 (crate (name "zolt-macros") (vers "0.7.2") (deps (list (crate-dep (name "colored") (req "^2") (default-features #t) (kind 0)) (crate-dep (name "zolt-proc") (req "=0.7.2") (default-features #t) (kind 0)))) (hash "15sj1s92qc05pkmax89kimfxkk7kxnb28xkd888ki6fydazcczlb")))

(define-public crate-zolt-proc-0.1 (crate (name "zolt-proc") (vers "0.1.0") (hash "0i7mgs1ww6myd1q141xxvh6s7p0l3ipavy49syakrnzl5jyird83")))

(define-public crate-zolt-proc-0.7 (crate (name "zolt-proc") (vers "0.7.0") (hash "1m3a0yglgfabj41dgb9r236lvnh5mlv6vv9rhckw8zzwrnx2c29n")))

(define-public crate-zolt-proc-0.7 (crate (name "zolt-proc") (vers "0.7.1") (hash "05l12digwz00120f8sxfsly3i8d54z61dhncwllg20nq21sxdzzm")))

(define-public crate-zolt-proc-0.7 (crate (name "zolt-proc") (vers "0.7.2") (hash "1zxxmyn82a4x5g82f05ja4dlvpcc2zrhhhiqxdlrqvi7v9vg5140")))

