(define-module (crates-io zo rr) #:use-module (crates-io))

(define-public crate-zorro-0.0.1 (crate (name "zorro") (vers "0.0.1") (deps (list (crate-dep (name "base64") (req "^0.13.0") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "png") (req "^0.16.8") (default-features #t) (kind 0)))) (hash "1ijxcb765pya366jr9jw6ai6nfa10zmjc4x8zjsq29pqfgc5dbl2")))

