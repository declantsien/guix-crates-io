(define-module (crates-io zo nb) #:use-module (crates-io))

(define-public crate-zonbi-0.1 (crate (name "zonbi") (vers "0.1.0") (deps (list (crate-dep (name "zonbi_macros") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "1gs89cqibabbs7j5wb0scdjxxdzvki231c6775k1k355nr431bzn")))

(define-public crate-zonbi-0.2 (crate (name "zonbi") (vers "0.2.0") (deps (list (crate-dep (name "zonbi_macros") (req "^0.2.0") (default-features #t) (kind 0)))) (hash "0fc9jynvwxqkkwaw80yszssibhm0vnck2bs8jsd2w4cf5hkv5gi0")))

(define-public crate-zonbi-0.3 (crate (name "zonbi") (vers "0.3.0") (deps (list (crate-dep (name "zonbi_macros") (req "^0.3.0") (default-features #t) (kind 0)))) (hash "0r31n15q357yv2h0mhgysb5lw5x6f7qkqgnfskmb8xa7fksz79qi")))

(define-public crate-zonbi-0.3 (crate (name "zonbi") (vers "0.3.1") (deps (list (crate-dep (name "zonbi_macros") (req "^0.3.0") (default-features #t) (kind 0)))) (hash "0z8yycv3hfnr0bxs42pk7k43q386c839lbn5qlp55b9b4vmz68c6")))

(define-public crate-zonbi-0.3 (crate (name "zonbi") (vers "0.3.2") (deps (list (crate-dep (name "zonbi_macros") (req "^0.3.0") (default-features #t) (kind 0)))) (hash "12q4v9gxcmc6kvag6g39vj02wyi0z8c4wyggxj8xqakhwb0yb0nw")))

(define-public crate-zonbi_macros-0.1 (crate (name "zonbi_macros") (vers "0.1.0") (deps (list (crate-dep (name "proc-macro2") (req "^1.0.79") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2.0.38") (features (quote ("full" "parsing" "extra-traits"))) (default-features #t) (kind 0)))) (hash "15ba3vmsqa86yv2plx6hp7ndxjk5677vdjn7r4gqlw03jn0hils6")))

(define-public crate-zonbi_macros-0.2 (crate (name "zonbi_macros") (vers "0.2.0") (deps (list (crate-dep (name "proc-macro2") (req "^1.0.79") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2.0.38") (features (quote ("full" "parsing" "extra-traits"))) (default-features #t) (kind 0)))) (hash "0d4yrg27aw6nv5saklgwvdz4dxikfg6a2q9xcbn5i7vkz5w8ilsk")))

(define-public crate-zonbi_macros-0.3 (crate (name "zonbi_macros") (vers "0.3.0") (deps (list (crate-dep (name "proc-macro2") (req "^1.0.79") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2.0.38") (features (quote ("full" "parsing" "extra-traits"))) (default-features #t) (kind 0)))) (hash "1yrcmnrr54sf5jx5m0ww43hjf4bhbi02i0gmwjw9lsji1vmh7fs4")))

