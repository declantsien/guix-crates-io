(define-module (crates-io zo rq) #:use-module (crates-io))

(define-public crate-zorq-acl-0.1 (crate (name "zorq-acl") (vers "0.1.0") (deps (list (crate-dep (name "env_logger") (req "^0.7") (default-features #t) (kind 2)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "test-env-log") (req "^0.2") (default-features #t) (kind 2)))) (hash "1fwc6xv84j4jg53zmb776ri086lp0p103cxqw1vqh0lhcr0vr1m2")))

