(define-module (crates-io zo oi) #:use-module (crates-io))

(define-public crate-zooid-0.0.0 (crate (name "zooid") (vers "0.0.0") (hash "064j78vshg1sc1ar9k1bg2k4g0zj3rnsszvc373daqni8vy3ssb4")))

(define-public crate-zooid-0.0.1 (crate (name "zooid") (vers "0.0.1") (deps (list (crate-dep (name "tokio") (req "^1.15.0") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "0zc333yqbmf50c7crlq83l93mhf4wpp7pgwz17ayf0yin7mbnp65")))

