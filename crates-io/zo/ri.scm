(define-module (crates-io zo ri) #:use-module (crates-io))

(define-public crate-zorin-0.0.0 (crate (name "zorin") (vers "0.0.0") (hash "0llpg6j9z7igr57gqhj45cmcdcvpx0h82xzsjhghhgn0lcq26j10")))

(define-public crate-zorio-0.1 (crate (name "zorio") (vers "0.1.0") (deps (list (crate-dep (name "memchr") (req "^2") (kind 0)))) (hash "13a0wv4b9qvbg3g8r3vqv5h02fpkidl35ak4svxhdd0hg8z8h3wp")))

