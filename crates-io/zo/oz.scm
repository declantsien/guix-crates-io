(define-module (crates-io zo oz) #:use-module (crates-io))

(define-public crate-zoozle-0.1 (crate (name "zoozle") (vers "0.1.0") (hash "175a6scvlhfl8a016a1d647v5sc6nb9yxgvm6zqz5pw2hsvrvzzj")))

(define-public crate-zoozle-0.1 (crate (name "zoozle") (vers "0.1.1") (hash "11jf2lsmz2cgjwj4lqnywb2cdykawnl1lz7djrq7vxrd3gw5yhrg")))

(define-public crate-zoozle-0.1 (crate (name "zoozle") (vers "0.1.2") (hash "13p53v14sznn5gscimc327g43sz4ng5hpwbwbyp6wx0q52qwjjsw")))

(define-public crate-zoozle-0.1 (crate (name "zoozle") (vers "0.1.3") (hash "0g33mmldlmkl1cpb3w5jz58d2z9p8xf2hql7hlk5y1md86cmi6k2")))

(define-public crate-zoozle-0.1 (crate (name "zoozle") (vers "0.1.4") (hash "11jiljzlig8gx1hpb4pflzpx349gxdablwlkljz7ca8vwnfb8l6x")))

(define-public crate-zoozle-0.1 (crate (name "zoozle") (vers "0.1.5") (hash "0l3g03wz1b01h468ypvwhlkdxg1fckrcq9pkq47fs0fhrnd3wrw9")))

(define-public crate-zoozle-0.1 (crate (name "zoozle") (vers "0.1.6") (hash "0ib47cr4bi93n2is80jvr13ss1r7pra916q48h5462wvln5izqwd")))

(define-public crate-zoozle-0.1 (crate (name "zoozle") (vers "0.1.7") (hash "15zf9r3jg1kpb5dhnkbv3gzyj1ip8h1gk43jpqszjhcklx9fd548")))

(define-public crate-zoozle-0.1 (crate (name "zoozle") (vers "0.1.8") (hash "1f0pvkmz82xx9ls2i67d363wbha7w3f3r62x1q9whf5xj3v8w5bf")))

(define-public crate-zoozle-0.1 (crate (name "zoozle") (vers "0.1.9") (hash "171xg0pnwb7qx3yn5al0sl10jnnc6q5jp2wlalvvzd0v3scv6v2w")))

