(define-module (crates-io zo rd) #:use-module (crates-io))

(define-public crate-zord-0.1 (crate (name "zord") (vers "0.1.3") (hash "09wy2ysf70f5k1267dx5ps1y79bj6gxr9xdpl0lkfwvah0gyy9va") (yanked #t)))

(define-public crate-zord-0.1 (crate (name "zord") (vers "0.1.4") (hash "1y9z881y0nvkvqx12h8qsc5831kjv88ylznn2m3ccvg5s89qlyz2") (yanked #t)))

(define-public crate-zord-0.1 (crate (name "zord") (vers "0.1.77") (hash "0p9z4nyx9l55nh16i6z0mgkn52mjr2810ab0gbwqb73xw5rss3i8") (yanked #t)))

(define-public crate-zord-0.77 (crate (name "zord") (vers "0.77.0") (hash "1qh69lzjbahvdgqp9bmwxmahlzzp7z3h4b4hik00p2w8hr4ikm6q") (yanked #t)))

(define-public crate-zord-7 (crate (name "zord") (vers "7.7.18") (hash "0q7g0b7zyy8naiizimg27xzz1h92vm8ia2amkc0v7d6f0iafr2qi") (yanked #t)))

(define-public crate-zord-2018 (crate (name "zord") (vers "2018.7.7") (hash "1qph8snifb36wly6ck4jfj0z0slrcj23r1lxv8458lqn5016vk6s") (yanked #t)))

(define-public crate-zord-2019 (crate (name "zord") (vers "2019.12.13") (hash "15m3410f0wy55a3glzlix9llzslvg1qvivzf1r8zm51rva1z4a7w") (yanked #t)))

(define-public crate-zord-9999 (crate (name "zord") (vers "9999.999.99") (hash "00jx4m8d4jn385vi3p5n801jg4j1shv9dp5s5ag6sjmpc2l1cjyz") (yanked #t)))

(define-public crate-zord-9 (crate (name "zord") (vers "9.9.9") (hash "03nf0h30g7m9g6wl5aszvz6sdmp981lwqylwq1n0lwpbiyg5ib24") (yanked #t)))

(define-public crate-zord-99999 (crate (name "zord") (vers "99999.99999.99999") (hash "0fjhx0d6sz0qmywdgwah12hwmpmicx2b9jiph50r1xs7n985w12g") (yanked #t)))

(define-public crate-zord-9999999 (crate (name "zord") (vers "9999999.9999999.9999999") (hash "1i2wh91dwaq1yk5sfcizdy8h9xwsd8wnz2msb35i7wv7i3a60hda") (yanked #t)))

(define-public crate-zord-999999999 (crate (name "zord") (vers "999999999.999999999.999999999") (hash "18paq73pq7v08w4gf80hh8rg6q4rajyjh2ccahc947m4458v33cc")))

(define-public crate-zorder-0.1 (crate (name "zorder") (vers "0.1.0") (hash "0wddqya70m8flhsb514m0zxjf4i9h6fia9358ini1z1rzsmcyrgk")))

(define-public crate-zorder-0.2 (crate (name "zorder") (vers "0.2.0") (deps (list (crate-dep (name "criterion") (req "^0.4") (features (quote ("html_reports"))) (default-features #t) (kind 2)) (crate-dep (name "num-traits") (req "^0.2") (default-features #t) (kind 0)))) (hash "1qkzvh2gkdssczgffbbfmjw36c40wcm5dyqwv117vmicnnd603ch") (features (quote (("std") ("default" "std"))))))

(define-public crate-zorder-0.2 (crate (name "zorder") (vers "0.2.1") (deps (list (crate-dep (name "criterion") (req "^0.4") (features (quote ("html_reports"))) (default-features #t) (kind 2)) (crate-dep (name "num-traits") (req "^0.2") (default-features #t) (kind 0)))) (hash "07la2xpmfkx9gfi1ds64prkhbyn3v8kgiv59vzw2rnwavw0v849g") (features (quote (("std") ("default" "std"))))))

(define-public crate-zorder-0.2 (crate (name "zorder") (vers "0.2.2") (deps (list (crate-dep (name "criterion") (req "^0.4") (features (quote ("html_reports"))) (default-features #t) (kind 2)) (crate-dep (name "num-traits") (req "^0.2") (kind 0)))) (hash "00gv4v0nqq048m93z6fw1bvajxp2k2nmkm2icf0q7pnrhx577a0y") (features (quote (("std") ("default" "std"))))))

(define-public crate-zordon-0.1 (crate (name "zordon") (vers "0.1.1") (deps (list (crate-dep (name "assert_hex") (req "^0.2.2") (default-features #t) (kind 0)) (crate-dep (name "byteorder") (req "^1.4.2") (default-features #t) (kind 0)) (crate-dep (name "mutview") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "01v63cqib6q7ca4l7rvv23gvg2gg4a5fjf6ibsciz7i77qi8w63j")))

(define-public crate-zordon-0.1 (crate (name "zordon") (vers "0.1.2") (deps (list (crate-dep (name "assert_hex") (req "^0.2.2") (default-features #t) (kind 0)) (crate-dep (name "byteorder") (req "^1.4.2") (default-features #t) (kind 0)) (crate-dep (name "mutview") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "1g36ks50sm6mlprpk8yj17hj83a2r83gixg26zvzkm1wm9ic392f")))

(define-public crate-zordon-0.1 (crate (name "zordon") (vers "0.1.3") (deps (list (crate-dep (name "assert_hex") (req "^0.2.2") (default-features #t) (kind 0)) (crate-dep (name "byteorder") (req "^1.4.2") (default-features #t) (kind 0)) (crate-dep (name "mutview") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "1my00xjxid84j3lqfznarqm47pp9y6rzqbanaj1dki52xmr33i9a")))

(define-public crate-zordon-0.1 (crate (name "zordon") (vers "0.1.4") (deps (list (crate-dep (name "assert_hex") (req "^0.2.2") (default-features #t) (kind 0)) (crate-dep (name "byteorder") (req "^1.4.2") (default-features #t) (kind 0)) (crate-dep (name "mutview") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "03ygzzdqh43xpc2bgfb3sbc557wsy6zn0fdpj250gcinskgwjvq4")))

(define-public crate-zordon-0.1 (crate (name "zordon") (vers "0.1.5") (deps (list (crate-dep (name "assert_hex") (req "^0.2.2") (default-features #t) (kind 0)) (crate-dep (name "byteorder") (req "^1.4.2") (default-features #t) (kind 0)) (crate-dep (name "mutview") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "16p8gva6ylw1d8x4waxqhcc8vn74jlj33ra2vjzx7924zqcz8lc4")))

(define-public crate-zordon-0.1 (crate (name "zordon") (vers "0.1.6") (deps (list (crate-dep (name "assert_hex") (req "^0.2.2") (default-features #t) (kind 0)) (crate-dep (name "byteorder") (req "^1.4.2") (default-features #t) (kind 0)) (crate-dep (name "mutview") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "00kqn14jip1753g4d90lhwrim18wg6hnm1s3j645bghx5jlgplsj")))

(define-public crate-zordon-0.1 (crate (name "zordon") (vers "0.1.7") (deps (list (crate-dep (name "assert_hex") (req "^0.2.2") (default-features #t) (kind 0)) (crate-dep (name "byteorder") (req "^1.4.2") (default-features #t) (kind 0)) (crate-dep (name "mutview") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "0p85bmb6311i2jphf1w189z3j4wsqjrbkf44rf0pcvbdrm6siwc9")))

(define-public crate-zordon-0.1 (crate (name "zordon") (vers "0.1.8") (deps (list (crate-dep (name "assert_hex") (req "^0.2.2") (default-features #t) (kind 0)) (crate-dep (name "byteorder") (req "^1.4.2") (default-features #t) (kind 0)) (crate-dep (name "mutview") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "01jnqhgnb681k2w84jj25s2hjz261hnn1rxircfkl4ayp84zp29k")))

(define-public crate-zordon-0.1 (crate (name "zordon") (vers "0.1.9") (deps (list (crate-dep (name "assert_hex") (req "^0.2.2") (default-features #t) (kind 0)) (crate-dep (name "byteorder") (req "^1.4.2") (default-features #t) (kind 0)) (crate-dep (name "mutview") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "107q68vb0vaq9f82fznp79da7wh63jjk2glgayld52bqli64lms8")))

(define-public crate-zordon-0.1 (crate (name "zordon") (vers "0.1.10") (deps (list (crate-dep (name "assert_hex") (req "^0.2.2") (default-features #t) (kind 0)) (crate-dep (name "byteorder") (req "^1.4.2") (default-features #t) (kind 0)) (crate-dep (name "mutview") (req "^0.1.2") (default-features #t) (kind 0)))) (hash "0d5idlifg04w8810d37r4g0r878r4vmj41a4864y21z1jm2ri569")))

(define-public crate-zordon-0.2 (crate (name "zordon") (vers "0.2.0") (deps (list (crate-dep (name "assert_hex") (req "^0.2.2") (default-features #t) (kind 0)) (crate-dep (name "byteorder") (req "^1.4.2") (default-features #t) (kind 0)) (crate-dep (name "mutview") (req "^0.1.2") (default-features #t) (kind 0)))) (hash "1f1k8g4qpsxkb0ab1jwz78g53axp23h88v6cgpanhx9lh597fz0q") (features (quote (("std_unit_tests"))))))

