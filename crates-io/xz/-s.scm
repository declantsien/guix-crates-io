(define-module (crates-io xz -s) #:use-module (crates-io))

(define-public crate-xz-sys-0.0.1 (crate (name "xz-sys") (vers "0.0.1") (hash "1h1bhk44nzcphpmnschg47q8vn20b9xj7npwax6ai547zsc9cg0q")))

(define-public crate-xz-sys-0.1 (crate (name "xz-sys") (vers "0.1.0") (deps (list (crate-dep (name "lzma-sys") (req "^0.1") (default-features #t) (kind 0)))) (hash "0bcrrfx1kgzavwj6nyri2w9qq98zd1nmz8msj9rrvwjz2ybpax4f")))

