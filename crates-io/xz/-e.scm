(define-module (crates-io xz -e) #:use-module (crates-io))

(define-public crate-xz-embedded-sys-0.1 (crate (name "xz-embedded-sys") (vers "0.1.1") (deps (list (crate-dep (name "gcc") (req "^0.3") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.1") (default-features #t) (kind 0)))) (hash "0185piq3gibj7xhk12lwxdhqh2vry659m2bqjchjs3flfn5vggfp")))

