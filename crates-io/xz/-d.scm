(define-module (crates-io xz -d) #:use-module (crates-io))

(define-public crate-xz-decom-0.2 (crate (name "xz-decom") (vers "0.2.0") (deps (list (crate-dep (name "xz-embedded-sys") (req "^0.1.1") (default-features #t) (kind 0)))) (hash "0p4as1cp0hk06rfqhgnz22ws61v5sy5lfxsqjwiacx398jd2mri6")))

