(define-module (crates-io co do) #:use-module (crates-io))

(define-public crate-codo-0.0.1 (crate (name "codo") (vers "0.0.1") (deps (list (crate-dep (name "chrono") (req "^0.4.31") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^4.4.7") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "grep") (req "^0.2.12") (default-features #t) (kind 0)) (crate-dep (name "ignore") (req "^0.4.20") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.10.2") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.108") (default-features #t) (kind 0)))) (hash "062yjdkjf8qpvbniqkp90vgvb09j3yvs5lm668giviwkylli87nf")))

