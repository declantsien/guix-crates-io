(define-module (crates-io co wi) #:use-module (crates-io))

(define-public crate-cowirc-0.1 (crate (name "cowirc") (vers "0.1.0") (deps (list (crate-dep (name "hex") (req "^0.4.3") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.49") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.35.1") (features (quote ("full"))) (default-features #t) (kind 0)) (crate-dep (name "tokio-native-tls") (req "^0.3.1") (default-features #t) (kind 0)))) (hash "1w3hf9cjmnbv1i4vbhqq5rpk1x9fwxcljl30jwyq2n9pxnnarr27")))

(define-public crate-cowirc-0.2 (crate (name "cowirc") (vers "0.2.0") (deps (list (crate-dep (name "hex") (req "^0.4.3") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.49") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.35.1") (features (quote ("full"))) (default-features #t) (kind 0)) (crate-dep (name "tokio-native-tls") (req "^0.3.1") (default-features #t) (kind 0)))) (hash "16p0ndpr7ni2c3z8r5d2s1n2q97wmv2c2vkwqab0d9dknlwhf47x")))

