(define-module (crates-io co np) #:use-module (crates-io))

(define-public crate-conpty-0.1 (crate (name "conpty") (vers "0.1.0") (deps (list (crate-dep (name "windows") (req "^0.19.0") (default-features #t) (kind 0)) (crate-dep (name "windows") (req "^0.19.0") (default-features #t) (kind 1)))) (hash "0w198i8wgsz1czx01lyfwyagw6nrmznafac25fbs00v2cabsvd4d")))

(define-public crate-conpty-0.1 (crate (name "conpty") (vers "0.1.1") (deps (list (crate-dep (name "windows") (req "^0.19.0") (default-features #t) (kind 0)) (crate-dep (name "windows") (req "^0.19.0") (default-features #t) (kind 1)))) (hash "17sip26ms6hpbwfix4scrcsi84m25b75ajkmjcq1ss23ibl448fg")))

(define-public crate-conpty-0.2 (crate (name "conpty") (vers "0.2.0") (deps (list (crate-dep (name "conpty-bindings") (req "^0.0.0") (default-features #t) (kind 0)) (crate-dep (name "windows") (req "^0.19.0") (default-features #t) (kind 0)))) (hash "09bsr8rzkyb48xnaayi546d2dg54spa14m83fpfsqy1ygfisfc8v")))

(define-public crate-conpty-0.2 (crate (name "conpty") (vers "0.2.1") (deps (list (crate-dep (name "windows") (req "^0.19.0") (default-features #t) (kind 0)) (crate-dep (name "windows") (req "^0.19.0") (default-features #t) (kind 1)))) (hash "0c3vlf526f609l17bppj69wjql9iamadqjb9a54q0nagm3a6nc0v")))

(define-public crate-conpty-0.3 (crate (name "conpty") (vers "0.3.0") (deps (list (crate-dep (name "strip-ansi-escapes") (req "^0.1.1") (default-features #t) (kind 2)) (crate-dep (name "windows") (req "^0.29.0") (features (quote ("alloc" "Win32_Foundation" "Win32_Security" "Win32_System_Threading" "Win32_System_Console" "Win32_System_Pipes" "Win32_System_SystemServices" "Win32_System_WindowsProgramming" "Win32_System_IO" "Win32_Storage_FileSystem"))) (default-features #t) (kind 0)))) (hash "0m27fflp74g0vi1gg6c7r7dbjypd9bmsi839pgwxfwv20bjalywp")))

(define-public crate-conpty-0.4 (crate (name "conpty") (vers "0.4.0") (deps (list (crate-dep (name "strip-ansi-escapes") (req "^0.1.1") (default-features #t) (kind 2)) (crate-dep (name "windows") (req "^0.43.0") (features (quote ("Win32_Foundation" "Win32_Security" "Win32_System_Threading" "Win32_System_Console" "Win32_System_Pipes" "Win32_System_SystemServices" "Win32_System_WindowsProgramming" "Win32_System_IO" "Win32_Storage_FileSystem"))) (default-features #t) (kind 0)))) (hash "0684fv8zvgdpvn3f681qzvnr6n8xd0gwnz68hx0djwxihx6mv09b")))

(define-public crate-conpty-0.5 (crate (name "conpty") (vers "0.5.0") (deps (list (crate-dep (name "windows") (req "^0.44.0") (features (quote ("Win32_Foundation" "Win32_Security" "Win32_System_Threading" "Win32_System_Console" "Win32_System_Pipes" "Win32_System_SystemServices" "Win32_System_WindowsProgramming" "Win32_System_IO" "Win32_Storage_FileSystem"))) (default-features #t) (kind 0)) (crate-dep (name "strip-ansi-escapes") (req "^0.1.1") (default-features #t) (kind 2)))) (hash "07lm6cm1bm1p7j3yl6m70nxc1ra91yfycy9zsjav97yjh2x9s6vi")))

(define-public crate-conpty-0.5 (crate (name "conpty") (vers "0.5.1") (deps (list (crate-dep (name "windows") (req "^0.44.0") (features (quote ("Win32_Foundation" "Win32_Security" "Win32_System_Threading" "Win32_System_Console" "Win32_System_Pipes" "Win32_System_SystemServices" "Win32_System_WindowsProgramming" "Win32_System_IO" "Win32_Storage_FileSystem"))) (default-features #t) (kind 0)) (crate-dep (name "strip-ansi-escapes") (req "^0.1.1") (default-features #t) (kind 2)))) (hash "1nmkhiysnssvbi4kqaq8cybb0ffngbl64kfpk8s86ihdg940caxp")))

(define-public crate-conpty-bindings-0.0.0 (crate (name "conpty-bindings") (vers "0.0.0") (deps (list (crate-dep (name "windows") (req "^0.19.0") (default-features #t) (kind 0)) (crate-dep (name "windows") (req "^0.19.0") (default-features #t) (kind 1)))) (hash "0gr0pb0f9by16lzk225mjvpjrgcvpqc5q06i3fhkdgikg6v01y54")))

