(define-module (crates-io co ke) #:use-module (crates-io))

(define-public crate-coke-0.2 (crate (name "coke") (vers "0.2.0") (deps (list (crate-dep (name "anyhow") (req "^1.0.61") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^3.2.17") (features (quote ("derive" "cargo"))) (default-features #t) (kind 0)) (crate-dep (name "mlua") (req "^0.8.3") (features (quote ("lua54" "vendored" "send"))) (default-features #t) (kind 0)))) (hash "0z0nxx0kpvckwv7waaa1jrff0gdnhjc4mm3rwk10yxicvjl9wjd5") (yanked #t)))

(define-public crate-coke-0.1 (crate (name "coke") (vers "0.1.0") (deps (list (crate-dep (name "anyhow") (req "^1.0.61") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^3.2.17") (features (quote ("derive" "cargo"))) (default-features #t) (kind 0)) (crate-dep (name "mlua") (req "^0.8.3") (features (quote ("lua54" "vendored" "send"))) (default-features #t) (kind 0)))) (hash "1k1jrdhyjiy1wrnm11zdx0vkzafgc0rjx92vnkpm1p9rq0q2pnis") (yanked #t)))

(define-public crate-coke-0.2 (crate (name "coke") (vers "0.2.1") (deps (list (crate-dep (name "anyhow") (req "^1.0.61") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^3.2.17") (features (quote ("derive" "cargo"))) (default-features #t) (kind 0)) (crate-dep (name "mlua") (req "^0.8.3") (features (quote ("lua54" "vendored" "send"))) (default-features #t) (kind 0)) (crate-dep (name "tabled") (req "^0.8.0") (default-features #t) (kind 0)))) (hash "01bziqa2navblns33h8q40xmb94iir3x3gwfrklszkb69y45659h") (yanked #t)))

