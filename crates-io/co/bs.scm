(define-module (crates-io co bs) #:use-module (crates-io))

(define-public crate-cobs-0.1 (crate (name "cobs") (vers "0.1.1") (deps (list (crate-dep (name "quickcheck") (req "*") (default-features #t) (kind 2)))) (hash "1a7dvg1f18ik4fd74ns3zfm5s2fbbx8ivq65grz9hjc2by1snlic")))

(define-public crate-cobs-0.1 (crate (name "cobs") (vers "0.1.2") (deps (list (crate-dep (name "quickcheck") (req "*") (default-features #t) (kind 2)))) (hash "19n7bn0sgxlvl5q75vjz7nq20w2b9ac3grdim80pcmixg88lan44")))

(define-public crate-cobs-0.1 (crate (name "cobs") (vers "0.1.3") (deps (list (crate-dep (name "quickcheck") (req "^0.5.0") (default-features #t) (kind 2)))) (hash "1bblsbqvk6853zjsjqv8ywj5qi4a64q0z624r3297vy3bzyyvb4q") (features (quote (("use_std") ("default" "use_std"))))))

(define-public crate-cobs-0.1 (crate (name "cobs") (vers "0.1.4") (deps (list (crate-dep (name "quickcheck") (req "^0.5.0") (default-features #t) (kind 2)))) (hash "1xip86ifss0cqk8na5plpl6f7fygmxmmj79k1xxranbrzi2f8jy4") (features (quote (("use_std") ("default" "use_std"))))))

(define-public crate-cobs-0.2 (crate (name "cobs") (vers "0.2.0") (deps (list (crate-dep (name "quickcheck") (req "^0.5.0") (default-features #t) (kind 2)))) (hash "1sxrrxmvp6s4c8mvdw79dmwj83fzcjj6gxvkx2zqdk5q40vshqi7") (features (quote (("use_std") ("default" "use_std"))))))

(define-public crate-cobs-0.2 (crate (name "cobs") (vers "0.2.1") (deps (list (crate-dep (name "quickcheck") (req "^0.5.0") (default-features #t) (kind 2)))) (hash "1hnsdmz94bq15pnj3dqc5xcv78wh0bk7fjjg775367bw41gz7k0p") (features (quote (("use_std") ("default" "use_std"))))))

(define-public crate-cobs-0.2 (crate (name "cobs") (vers "0.2.2") (deps (list (crate-dep (name "quickcheck") (req "^0.5.0") (default-features #t) (kind 2)))) (hash "00g37zq8wi0p4xbk2bgx65sdf055gynbxj2y5n14010xy1jf6q3a") (features (quote (("use_std") ("default" "use_std"))))))

(define-public crate-cobs-0.2 (crate (name "cobs") (vers "0.2.3") (deps (list (crate-dep (name "quickcheck") (req "^0.5.0") (default-features #t) (kind 2)))) (hash "05gd16mws4yd63h8jr3p08in8y8w21rpjp5jb55hzl9bgalh5fk7") (features (quote (("use_std") ("default" "use_std"))))))

(define-public crate-cobs-codec-0.1 (crate (name "cobs-codec") (vers "0.1.0") (deps (list (crate-dep (name "bytes") (req "^1.5.0") (default-features #t) (kind 0)) (crate-dep (name "futures") (req "^0.3.28") (kind 2)) (crate-dep (name "paste") (req "^1.0.14") (default-features #t) (kind 2)) (crate-dep (name "thiserror") (req "^1.0.48") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.32.0") (features (quote ("rt" "macros"))) (kind 2)) (crate-dep (name "tokio-util") (req "^0.7.8") (features (quote ("codec"))) (default-features #t) (kind 0)))) (hash "0vvlm1s9z8q78fl53fwi1r5kamxxgd29rr3s549bsr9sbhlqs6yi")))

(define-public crate-cobs-rs-1 (crate (name "cobs-rs") (vers "1.0.0") (hash "0gp041msjfr6a06i97qbk6hwz3bcmxg2aqqg0b299bfln8v828jq")))

(define-public crate-cobs-rs-1 (crate (name "cobs-rs") (vers "1.0.1") (hash "0xknr6ifn6hy7vgd0sm114p5y0qcsw85vya3p7i6b9sj6snx2y35")))

(define-public crate-cobs-rs-1 (crate (name "cobs-rs") (vers "1.1.0") (hash "1qlvsd4vy2hyrgzxxj47qzqg41sfqaml9cyz5mfavd62sx72swsh")))

(define-public crate-cobs-rs-1 (crate (name "cobs-rs") (vers "1.1.1") (hash "0x0mhif5kn2frj1axv4v854mq17xn7w6djdr2kf5k7md8z75gb5h")))

(define-public crate-cobs-rs-1 (crate (name "cobs-rs") (vers "1.1.2") (hash "0czz6r3g9zl2fwj64kgpl7alcz44b40ks4g6vbp0grvrzqrmb1km")))

(define-public crate-cobs-simd-0.1 (crate (name "cobs-simd") (vers "0.1.0") (deps (list (crate-dep (name "concat-idents") (req "^1.1.5") (default-features #t) (kind 2)) (crate-dep (name "corncobs") (req "^0.1.3") (default-features #t) (kind 2)) (crate-dep (name "criterion") (req "^0.5.1") (default-features #t) (kind 2)) (crate-dep (name "quickcheck") (req "^1.0.3") (default-features #t) (kind 2)) (crate-dep (name "quickcheck_macros") (req "^1.0.0") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.8") (default-features #t) (kind 2)) (crate-dep (name "rand_pcg") (req "^0.3.1") (default-features #t) (kind 2)))) (hash "0gmd1rrgirffsvp4k08r1ncmifizs0lcc8pc8ll3pa4l94rmc758")))

(define-public crate-cobs-simd-0.2 (crate (name "cobs-simd") (vers "0.2.0") (deps (list (crate-dep (name "concat-idents") (req "^1.1.5") (default-features #t) (kind 2)) (crate-dep (name "corncobs") (req "^0.1.3") (default-features #t) (kind 2)) (crate-dep (name "criterion") (req "^0.5.1") (default-features #t) (kind 2)) (crate-dep (name "num") (req "^0.4.1") (default-features #t) (kind 0)) (crate-dep (name "quickcheck") (req "^1.0.3") (default-features #t) (kind 2)) (crate-dep (name "quickcheck_macros") (req "^1.0.0") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.8") (default-features #t) (kind 2)) (crate-dep (name "rand_pcg") (req "^0.3.1") (default-features #t) (kind 2)) (crate-dep (name "strum") (req "^0.25.0") (default-features #t) (kind 0)) (crate-dep (name "strum_macros") (req "^0.25.3") (default-features #t) (kind 0)))) (hash "1hvnkzn5gd33ipkh2m6p6ld0ir9kaqdj3jjn25l4v5qgaw85chyz")))

(define-public crate-cobs-simd-0.2 (crate (name "cobs-simd") (vers "0.2.1") (deps (list (crate-dep (name "concat-idents") (req "^1.1.5") (default-features #t) (kind 2)) (crate-dep (name "corncobs") (req "^0.1.3") (default-features #t) (kind 2)) (crate-dep (name "criterion") (req "^0.5.1") (default-features #t) (kind 2)) (crate-dep (name "num") (req "^0.4.1") (default-features #t) (kind 0)) (crate-dep (name "quickcheck") (req "^1.0.3") (default-features #t) (kind 2)) (crate-dep (name "quickcheck_macros") (req "^1.0.0") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.8") (default-features #t) (kind 2)) (crate-dep (name "rand_pcg") (req "^0.3.1") (default-features #t) (kind 2)) (crate-dep (name "strum") (req "^0.25.0") (default-features #t) (kind 0)) (crate-dep (name "strum_macros") (req "^0.25.3") (default-features #t) (kind 0)))) (hash "1v049vzybcp4ggh9fvl6clv604jbbddf0vd0vd1ianqznx9jyhl0")))

