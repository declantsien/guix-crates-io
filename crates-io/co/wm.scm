(define-module (crates-io co wm) #:use-module (crates-io))

(define-public crate-cowmug-0.0.1 (crate (name "cowmug") (vers "0.0.1") (deps (list (crate-dep (name "termion") (req "^1.5.5") (default-features #t) (kind 0)))) (hash "0g67x4qlc8aqwchkabfaz3zvy21iqi3sxymgd9nqwxif7iph817f")))

