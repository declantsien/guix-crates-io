(define-module (crates-io co it) #:use-module (crates-io))

(define-public crate-coitrees-0.2 (crate (name "coitrees") (vers "0.2.0") (deps (list (crate-dep (name "clap") (req "^3.0.0-beta.1") (default-features #t) (kind 2)) (crate-dep (name "fnv") (req "^1.0.3") (default-features #t) (kind 2)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.7") (default-features #t) (kind 2)))) (hash "1w1x91zks1p8y766vdd0z2c33wp9g2f326zgfihlaasglx6kr0pr")))

(define-public crate-coitrees-0.2 (crate (name "coitrees") (vers "0.2.1") (deps (list (crate-dep (name "clap") (req "^3.0.0-beta.1") (default-features #t) (kind 2)) (crate-dep (name "fnv") (req "^1.0.3") (default-features #t) (kind 2)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.7") (default-features #t) (kind 2)))) (hash "1l2ybr8n02vm08wq9mrix7r07bgwm85i6fyachlm8d626w9w9d3f")))

(define-public crate-coitrees-0.3 (crate (name "coitrees") (vers "0.3.0") (deps (list (crate-dep (name "clap") (req "^4.3.7") (features (quote ("derive"))) (default-features #t) (kind 2)) (crate-dep (name "fnv") (req "^1.0.7") (default-features #t) (kind 2)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.8") (default-features #t) (kind 2)))) (hash "08qvm0nqgck6hzrlndni6xfyf36qnszcjpjdsv1l4kny5gnvb95i") (features (quote (("nosimd"))))))

(define-public crate-coitrees-0.4 (crate (name "coitrees") (vers "0.4.0") (deps (list (crate-dep (name "clap") (req "^4.3.7") (features (quote ("derive"))) (default-features #t) (kind 2)) (crate-dep (name "fnv") (req "^1.0.7") (default-features #t) (kind 2)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.8") (default-features #t) (kind 2)))) (hash "1qwb4c5gx30gl1kyi85rbq6z23l2f9lm0q02ym160n0fvc89c3r4") (features (quote (("nosimd"))))))

