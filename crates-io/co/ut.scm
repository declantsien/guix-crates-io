(define-module (crates-io co ut) #:use-module (crates-io))

(define-public crate-cout2json-0.1 (crate (name "cout2json") (vers "0.1.0") (deps (list (crate-dep (name "anyhow") (req "^1.0") (default-features #t) (kind 2)) (crate-dep (name "clap") (req "^4") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)))) (hash "13215df03cpfgl7i8dfdc5gfypw9i7kyqb0nvz2xvl0yfj0mm3c3")))

(define-public crate-coutils-1 (crate (name "coutils") (vers "1.0.0") (deps (list (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)))) (hash "1rgw1abwqj4zhkmpqx4xx51a1s2kr313zj6gkvfxxr98haam3xsf")))

(define-public crate-coutils-1 (crate (name "coutils") (vers "1.1.0") (deps (list (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)))) (hash "1hlvjfjahpns3a8wja4ddyk81pjxwxiak22a24amngd61kqdwx1l")))

(define-public crate-coutils-1 (crate (name "coutils") (vers "1.2.0") (deps (list (crate-dep (name "file-serve") (req "^0.2.4") (default-features #t) (kind 0)) (crate-dep (name "fs_extra") (req "^1.2.0") (default-features #t) (kind 0)) (crate-dep (name "git2") (req "^0.17.2") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)))) (hash "0gx8n9ajfml5agnxmy6ki81w9r82p2izlnzs9w4w76nqcmw64x92")))

(define-public crate-coutils-1 (crate (name "coutils") (vers "1.3.0") (deps (list (crate-dep (name "file-serve") (req "^0.2.4") (default-features #t) (kind 0)) (crate-dep (name "fs_extra") (req "^1.2.0") (default-features #t) (kind 0)) (crate-dep (name "git2") (req "^0.17.2") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)))) (hash "127b7bn05nnl60svb1kb980lqqchnzqxvhfcgrifprns8di5j3ad")))

(define-public crate-coutils-1 (crate (name "coutils") (vers "1.4.0") (deps (list (crate-dep (name "chrono") (req "^0.4.26") (default-features #t) (kind 0)) (crate-dep (name "file-serve") (req "^0.2.4") (default-features #t) (kind 0)) (crate-dep (name "fs_extra") (req "^1.2.0") (default-features #t) (kind 0)) (crate-dep (name "git2") (req "^0.17.2") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)))) (hash "1nbwhixcx5jgsjfdjj0l5lmcbh7f3f2ic8snzy6bqi0y7a3rih8n")))

(define-public crate-coutils-1 (crate (name "coutils") (vers "1.5.0") (deps (list (crate-dep (name "chrono") (req "^0.4.26") (default-features #t) (kind 0)) (crate-dep (name "fs_extra") (req "^1.2.0") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)))) (hash "1zn38rq3vn3fgyy80rghiq5142df2v90jmwqbkfcg9g4j9wyhn3f")))

(define-public crate-coutils-1 (crate (name "coutils") (vers "1.6.0") (deps (list (crate-dep (name "chrono") (req "^0.4.37") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "fs_extra") (req "^1.3.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "git2") (req "^0.18.3") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)))) (hash "045p495d9d0zrg7kkjghz9i67av6g2p2z1yq1xg2lahl49y7xsa5") (features (quote (("time" "chrono") ("networking" "git2" "filesystem" "fs_extra") ("filesystem" "fs_extra")))) (yanked #t)))

(define-public crate-coutils-2 (crate (name "coutils") (vers "2.0.0") (deps (list (crate-dep (name "chrono") (req "^0.4.37") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "fs_extra") (req "^1.3.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "git2") (req "^0.18.3") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)))) (hash "0xxhj9qw8bqbs596cp3y8x4wk6ankalggjb3f63s9c2qv488749s") (features (quote (("time" "chrono") ("networking" "filesystem" "git2") ("filesystem" "fs_extra"))))))

