(define-module (crates-io co up) #:use-module (crates-io))

(define-public crate-coup-0.0.1 (crate (name "coup") (vers "0.0.1") (deps (list (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)))) (hash "188pzvc00b4zk65cb94b0bpl28s7x4i1fzc3cvrh9n48nkxqqdfy")))

(define-public crate-coup-1 (crate (name "coup") (vers "1.0.0") (deps (list (crate-dep (name "cfonts") (req "^1.1.4") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)))) (hash "1ps7gs4c64dd9lqxj50wflprkhc4ywc0xqlg1mfrly66zmkc38bv")))

(define-public crate-coup-1 (crate (name "coup") (vers "1.1.0") (deps (list (crate-dep (name "cfonts") (req "^1.1.4") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)))) (hash "0snwc2qc169xm285fgnwg4cg8qb1cqhih5n38p0y2d4fp95ggjgr")))

(define-public crate-coup-1 (crate (name "coup") (vers "1.1.1") (deps (list (crate-dep (name "cfonts") (req "^1.1.4") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)))) (hash "12w6wdd6rwx9ylwzz9dfvyiag1k3afi4ipdj7c49hxwg06kzr88v")))

(define-public crate-coupe-0.1 (crate (name "coupe") (vers "0.1.0") (deps (list (crate-dep (name "approx") (req "^0.5") (default-features #t) (kind 0)) (crate-dep (name "async-lock") (req "^2") (default-features #t) (kind 0)) (crate-dep (name "criterion") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "event-listener") (req "^2") (default-features #t) (kind 0)) (crate-dep (name "futures-lite") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "gnuplot") (req "^0.0.35") (default-features #t) (kind 2)) (crate-dep (name "itertools") (req "^0.10") (default-features #t) (kind 0)) (crate-dep (name "nalgebra") (req "^0.29") (features (quote ("rand"))) (default-features #t) (kind 0)) (crate-dep (name "ndarray") (req "^0.15") (default-features #t) (kind 0)) (crate-dep (name "num") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "proptest") (req "^1.0.0") (features (quote ("std" "timeout"))) (kind 2)) (crate-dep (name "rand") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "rayon") (req "^1.3") (default-features #t) (kind 0)) (crate-dep (name "sprs") (req "^0.11") (features (quote ("multi_thread"))) (kind 0)) (crate-dep (name "tracing") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "tracing-chrome") (req "^0.4") (default-features #t) (kind 2)) (crate-dep (name "tracing-subscriber") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "tracing-tree") (req "^0.2") (default-features #t) (kind 2)))) (hash "0kwr5ys2i8g3qlmk2lmn7rix01rwmfqx92s7j58av0qh0k6avaky")))

(define-public crate-coupler-0.0.0 (crate (name "coupler") (vers "0.0.0") (hash "0sxx2icjrpvggciw5r0w5vd5ixljk91v4shqhxmxhcrcs6ns21jq")))

(define-public crate-coupon-collect-0.0.0 (crate (name "coupon-collect") (vers "0.0.0") (deps (list (crate-dep (name "itertools") (req "^0.10.5") (default-features #t) (kind 0)) (crate-dep (name "num") (req "^0.4.0") (default-features #t) (kind 0)))) (hash "1rz1jwgks1wyi6q0alnhx0y0408bxk4w5b12vwz0zvmsw5grg42g") (features (quote (("default"))))))

(define-public crate-coupon_rs-0.1 (crate (name "coupon_rs") (vers "0.1.0") (deps (list (crate-dep (name "chrono") (req "^0.4.0") (default-features #t) (kind 0)) (crate-dep (name "chronoutil") (req "^0.2.0") (default-features #t) (kind 0)))) (hash "144qjzlghjyivwb5ppzyi4hd0na4kw6y2mkd3ljn9vyhh7nqrzpa")))

