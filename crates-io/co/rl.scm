(define-module (crates-io co rl) #:use-module (crates-io))

(define-public crate-corlib-0.1 (crate (name "corlib") (vers "0.1.0") (deps (list (crate-dep (name "delegate") (req "0.*") (default-features #t) (kind 0)) (crate-dep (name "paste") (req "1.0.*") (default-features #t) (kind 0)))) (hash "055vh4fv8vbdqraq0xpxr8pkx2a6pg7i2apvlpkxbsmbd0240lrb")))

(define-public crate-corlib-0.2 (crate (name "corlib") (vers "0.2.0") (deps (list (crate-dep (name "delegate") (req "0.*") (default-features #t) (kind 0)) (crate-dep (name "paste") (req "1.0.*") (default-features #t) (kind 0)))) (hash "137g6r1bmy5jyxq6zxa1cwkykp9vickqd4zjm107wb1pc7agkfvn")))

