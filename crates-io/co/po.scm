(define-module (crates-io co po) #:use-module (crates-io))

(define-public crate-copo-0.1 (crate (name "copo") (vers "0.1.0") (deps (list (crate-dep (name "console") (req "^0.11.3") (default-features #t) (kind 0)) (crate-dep (name "exitfailure") (req "^0.5.1") (default-features #t) (kind 0)) (crate-dep (name "failure") (req "^0.1.8") (default-features #t) (kind 0)) (crate-dep (name "indicatif") (req "^0.15.0") (default-features #t) (kind 0)) (crate-dep (name "notifica") (req "^1.0.2") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.3.15") (default-features #t) (kind 0)))) (hash "16zlff2gx5065a816cffpmplykz1iw2a317i9x4yhxg7amz83h98")))

(define-public crate-copo-0.1 (crate (name "copo") (vers "0.1.1") (deps (list (crate-dep (name "console") (req "^0.11.3") (default-features #t) (kind 0)) (crate-dep (name "exitfailure") (req "^0.5.1") (default-features #t) (kind 0)) (crate-dep (name "failure") (req "^0.1.8") (default-features #t) (kind 0)) (crate-dep (name "indicatif") (req "^0.15.0") (default-features #t) (kind 0)) (crate-dep (name "notifica") (req "^1.0.2") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.3.15") (default-features #t) (kind 0)))) (hash "11j109377igakp6vs4hq6q4p4z7plvxydxa3kyhif4k3k20lz9if")))

(define-public crate-copo-0.1 (crate (name "copo") (vers "0.1.2") (deps (list (crate-dep (name "console") (req "^0.11.3") (default-features #t) (kind 0)) (crate-dep (name "exitfailure") (req "^0.5.1") (default-features #t) (kind 0)) (crate-dep (name "failure") (req "^0.1.8") (default-features #t) (kind 0)) (crate-dep (name "indicatif") (req "^0.15.0") (default-features #t) (kind 0)) (crate-dep (name "notify-rust") (req "^4") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.3.15") (default-features #t) (kind 0)))) (hash "1siadzsssy4aikfgj8y5rlikwhvqihzkc1rwh09f2k0d5apr6l1b")))

(define-public crate-copoll-0.1 (crate (name "copoll") (vers "0.1.0") (deps (list (crate-dep (name "nix") (req "^0.23.1") (default-features #t) (kind 0)))) (hash "0cdjcc2khqqxdmbgrmskxh50ifpqnkjc66b564md83j3ii6shnd8")))

(define-public crate-copoll-1 (crate (name "copoll") (vers "1.1.0") (deps (list (crate-dep (name "nix") (req "^0.23.1") (default-features #t) (kind 0)))) (hash "0w1bqbg1ykmmf0q8ihwhlhk3gr0r6p1hl95xidc3yfjrbbya5fz0")))

(define-public crate-copoll-1 (crate (name "copoll") (vers "1.2.0") (deps (list (crate-dep (name "nix") (req "^0.23.1") (default-features #t) (kind 0)))) (hash "08j99zymzg2f2aq9bh6jxlkpssx7riyv20s36aywsqlbpfkj665n")))

(define-public crate-copoll-1 (crate (name "copoll") (vers "1.3.0") (deps (list (crate-dep (name "nix") (req "^0.23.1") (default-features #t) (kind 0)))) (hash "03nx16pgpxk6rzns4l875b2g0vaif46qjmmxky6vahlajasb27x4")))

