(define-module (crates-io co pc) #:use-module (crates-io))

(define-public crate-copc-rs-0.1 (crate (name "copc-rs") (vers "0.1.0") (deps (list (crate-dep (name "byteorder") (req "^1.4.3") (default-features #t) (kind 0)) (crate-dep (name "las") (req "^0.7.7") (default-features #t) (kind 0)) (crate-dep (name "laz") (req "^0.6.1") (default-features #t) (kind 0)) (crate-dep (name "num") (req "^0.4.0") (default-features #t) (kind 0)))) (hash "1ryfsiazrl2xbaq355vm1m4rzmgci0q40g0d129a6qn8kwcb805i")))

(define-public crate-copc-rs-0.2 (crate (name "copc-rs") (vers "0.2.0") (deps (list (crate-dep (name "byteorder") (req "^1.4.3") (default-features #t) (kind 0)) (crate-dep (name "las") (req "^0.7.7") (default-features #t) (kind 0)) (crate-dep (name "laz") (req "^0.6.1") (default-features #t) (kind 0)) (crate-dep (name "num") (req "^0.4.0") (default-features #t) (kind 0)))) (hash "066m408n2mnqgzp1dhz5akggb11k4r1d6svyw3zyq5q7qpwcsiqc")))

(define-public crate-copc-rs-0.3 (crate (name "copc-rs") (vers "0.3.0") (deps (list (crate-dep (name "byteorder") (req "^1.4.3") (default-features #t) (kind 0)) (crate-dep (name "env_logger") (req "^0.10.0") (default-features #t) (kind 2)) (crate-dep (name "http-range-client") (req "^0.7.0") (default-features #t) (kind 2)) (crate-dep (name "las") (req "^0.8.1") (default-features #t) (kind 0)) (crate-dep (name "laz") (req "^0.8.2") (default-features #t) (kind 0)))) (hash "1w3659irv7mjmxdd1a88rwvwnaynrkc536j35rcg9bh6phhwl0nh")))

