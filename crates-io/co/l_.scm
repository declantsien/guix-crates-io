(define-module (crates-io co l_) #:use-module (crates-io))

(define-public crate-col_macros-0.1 (crate (name "col_macros") (vers "0.1.0") (hash "0n8dr6nybb8q86k3aqd4y2j76yv1bmi1y48xkz31b5nkjbq5zcyl") (yanked #t)))

(define-public crate-col_macros-0.1 (crate (name "col_macros") (vers "0.1.1") (deps (list (crate-dep (name "high_mem_utils") (req "^0.1.1") (default-features #t) (kind 0)))) (hash "0n8v7ykf4xy2pi5576nmxv552mwazwa3lfflkpj0pwbl21gq236b") (yanked #t)))

(define-public crate-col_macros-0.1 (crate (name "col_macros") (vers "0.1.2") (deps (list (crate-dep (name "high_mem_utils") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "macro_helper") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "0qsh2myrbc79l65qbbn79z6xh7qq8v0c46q9hmg9r383cg1cv66x") (yanked #t)))

(define-public crate-col_macros-0.1 (crate (name "col_macros") (vers "0.1.3") (deps (list (crate-dep (name "high_mem_utils") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "macro_helper") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "0g0clbipqd554q8kdjjcjbkj9mg6fvpzyqr0bjvwb0b694br425f") (yanked #t)))

(define-public crate-col_macros-0.1 (crate (name "col_macros") (vers "0.1.4") (deps (list (crate-dep (name "compile_ops") (req "^0.1.1") (default-features #t) (kind 0)) (crate-dep (name "high_mem_utils") (req "^0.2.1") (default-features #t) (kind 0)) (crate-dep (name "macro_helper") (req "^0.1.1") (default-features #t) (kind 0)))) (hash "0x1pmh9qik1rd7v9v29fqh1c27iidq2w3n5cxqc9f8drzjpk4qm6") (yanked #t)))

(define-public crate-col_macros-0.1 (crate (name "col_macros") (vers "0.1.5") (deps (list (crate-dep (name "compile_ops") (req "^0.1.2") (default-features #t) (kind 0)) (crate-dep (name "high_mem_utils") (req "^0.2.1") (default-features #t) (kind 0)) (crate-dep (name "macro_helper") (req "^0.1.2") (default-features #t) (kind 0)))) (hash "180xq1fzhxyffz7cmpf48417c0pvndxk37hqqkg7w81h2a7hq45i") (yanked #t)))

(define-public crate-col_macros-0.1 (crate (name "col_macros") (vers "0.1.6") (deps (list (crate-dep (name "compile_ops") (req "^0.1.2") (default-features #t) (kind 0)) (crate-dep (name "high_mem_utils") (req "^0.2.1") (default-features #t) (kind 0)) (crate-dep (name "macro_helper") (req "^0.1.2") (default-features #t) (kind 0)))) (hash "0b7q6qcivfdz9wvpry4mfavbdhn49ai0sfi89db586lgs87bz6yc") (features (quote (("owned_slice")))) (yanked #t)))

(define-public crate-col_macros-0.1 (crate (name "col_macros") (vers "0.1.62") (deps (list (crate-dep (name "compile_ops") (req "^0.1.2") (default-features #t) (kind 0)) (crate-dep (name "high_mem_utils") (req "^0.2.1") (default-features #t) (kind 0)) (crate-dep (name "macro_helper") (req "^0.1.2") (default-features #t) (kind 0)))) (hash "0lf6wjssy58l7hrrd9p8xhw39jbq0qkbah2mjjidwxz7cib0j0wy") (features (quote (("owned_slice")))) (yanked #t)))

(define-public crate-col_macros-0.1 (crate (name "col_macros") (vers "0.1.64") (deps (list (crate-dep (name "compile_ops") (req "^0.1.2") (default-features #t) (kind 0)) (crate-dep (name "high_mem_utils") (req "^0.2.1") (default-features #t) (kind 0)) (crate-dep (name "macro_helper") (req "^0.1.2") (default-features #t) (kind 0)))) (hash "1zm8swq1avs7s2qfym6s5y05xiislzp4rr45ppgvwai2hk2nq47a") (features (quote (("owned_slice")))) (yanked #t)))

(define-public crate-col_macros-0.1 (crate (name "col_macros") (vers "0.1.68") (deps (list (crate-dep (name "compile_ops") (req "^0.1.2") (default-features #t) (kind 0)) (crate-dep (name "high_mem_utils") (req "^0.2.2") (default-features #t) (kind 0)) (crate-dep (name "macro_helper") (req "^0.1.2") (default-features #t) (kind 0)))) (hash "1r48mq465bhz52mlm737j8ab1wcdf47cblbi3i2qqifc4yqbki0r") (features (quote (("owned_slice")))) (yanked #t)))

(define-public crate-col_macros-0.1 (crate (name "col_macros") (vers "0.1.7") (deps (list (crate-dep (name "compile_ops") (req "^0.1.2") (default-features #t) (kind 0)) (crate-dep (name "high_mem_utils") (req "^0.2.2") (default-features #t) (kind 0)) (crate-dep (name "macro_helper") (req "^0.1.2") (default-features #t) (kind 0)))) (hash "1pg7qmabnwh5072d4z535lxgv6r290apkrc9bfqarbldjx8fhsmp") (features (quote (("owned_slice")))) (yanked #t)))

(define-public crate-col_macros-0.1 (crate (name "col_macros") (vers "0.1.8") (deps (list (crate-dep (name "compile_ops") (req "^0.1.2") (default-features #t) (kind 0)) (crate-dep (name "high_mem_utils") (req "^0.2.2") (default-features #t) (kind 0)) (crate-dep (name "macro_helper") (req "^0.1.2") (default-features #t) (kind 0)))) (hash "0fd1289r79aq24n73q2xn2fsiv66bsb4p57h4ydmdpfmc5l6gckj") (features (quote (("owned_slice")))) (yanked #t)))

(define-public crate-col_macros-0.1 (crate (name "col_macros") (vers "0.1.81") (deps (list (crate-dep (name "compile_ops") (req "^0.1.2") (default-features #t) (kind 0)) (crate-dep (name "high_mem_utils") (req "^0.2.2") (default-features #t) (kind 0)) (crate-dep (name "macro_helper") (req "^0.1.3") (default-features #t) (kind 0)))) (hash "0ck0d91vpzqnk0q2mwriz12lx1n8zxjwd738hvyk045w1j0grwbb") (features (quote (("owned_slice")))) (yanked #t)))

(define-public crate-col_macros-0.1 (crate (name "col_macros") (vers "0.1.9") (deps (list (crate-dep (name "compile_ops") (req "^0.1.2") (default-features #t) (kind 0)) (crate-dep (name "high_mem_utils") (req "^0.2.2") (default-features #t) (kind 0)) (crate-dep (name "macro_helper") (req "^0.1.4") (default-features #t) (kind 0)))) (hash "09v7xr0i5c62irbirkws1yrs9lcz41cjacdsn97y2dc0ywwfnmwc") (features (quote (("owned_slice")))) (yanked #t)))

(define-public crate-col_macros-0.1 (crate (name "col_macros") (vers "0.1.91") (deps (list (crate-dep (name "compile_ops") (req "^0.1.2") (default-features #t) (kind 0)) (crate-dep (name "high_mem_utils") (req "^0.2.2") (default-features #t) (kind 0)) (crate-dep (name "macro_helper") (req "^0.1.4") (default-features #t) (kind 0)))) (hash "04x71r4bh4cm0x15xcly5qqb3w6kyysbjbrbdz2wk2saxrfc7nvm") (features (quote (("owned_slice")))) (yanked #t)))

(define-public crate-col_macros-0.1 (crate (name "col_macros") (vers "0.1.93") (deps (list (crate-dep (name "compile_ops") (req "^0.1.2") (default-features #t) (kind 0)) (crate-dep (name "high_mem_utils") (req "^0.2.2") (default-features #t) (kind 0)) (crate-dep (name "macro_helper") (req "^0.1.4") (default-features #t) (kind 0)))) (hash "0mbisppywra04sw2xjfhl648z6arpvz9sbrv2hrzzbf1ggr1y6mm") (features (quote (("owned_slice")))) (yanked #t)))

(define-public crate-col_macros-0.2 (crate (name "col_macros") (vers "0.2.0") (deps (list (crate-dep (name "compile_ops") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "high_mem_utils") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "macro_helper") (req "^0.1") (default-features #t) (kind 0)))) (hash "1jpsqc96hw4lkwxfjynx8mfj5fv74mjyqmwfprxnlshzixkj2d0h") (features (quote (("owned_slice")))) (yanked #t)))

(define-public crate-col_macros-0.2 (crate (name "col_macros") (vers "0.2.2") (deps (list (crate-dep (name "compile_ops") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "high_mem_utils") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "macro_helper") (req "^0.1") (default-features #t) (kind 0)))) (hash "0szq36ygj1xca6xrrk8ba73186yk6piqwr6d8i4sym82ry04sljb")))

(define-public crate-col_proc_macros-0.1 (crate (name "col_proc_macros") (vers "0.1.0") (deps (list (crate-dep (name "col_proc_macros_impl") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "proc-macro-hack") (req "^0.5.11") (default-features #t) (kind 0)) (crate-dep (name "proc-macro-nested") (req "^0.1.4") (default-features #t) (kind 0)))) (hash "0i8lk3jk6yc210mypx3z9xra3xyfpjm1r5fk0gp1fw50x43rxclr")))

(define-public crate-col_proc_macros-0.1 (crate (name "col_proc_macros") (vers "0.1.1") (deps (list (crate-dep (name "col_proc_macros_impl") (req "^0") (default-features #t) (kind 0)))) (hash "070vdhcyl6p9jgcmdpd0xd2qn99q73gpygnqh6qqmayii9d3jw4j")))

(define-public crate-col_proc_macros_impl-0.1 (crate (name "col_proc_macros_impl") (vers "0.1.0") (deps (list (crate-dep (name "proc-macro-hack") (req "^0.5.11") (default-features #t) (kind 0)) (crate-dep (name "unicode-xid") (req "^0.2.0") (default-features #t) (kind 0)))) (hash "1g6rq78nivd28x8skhjgwmm936ccdvi7rl9jf2whh8hqdvpxj5xd")))

(define-public crate-col_proc_macros_impl-0.1 (crate (name "col_proc_macros_impl") (vers "0.1.2") (deps (list (crate-dep (name "unicode-xid") (req "^0.2.0") (default-features #t) (kind 0)))) (hash "1k3b9h5kx6jmdgia45537j2n9qfxdc0g0bbm25b7wwbqy76gp6xj")))

