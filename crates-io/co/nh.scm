(define-module (crates-io co nh) #:use-module (crates-io))

(define-public crate-conhash-0.1 (crate (name "conhash") (vers "0.1.0") (deps (list (crate-dep (name "env_logger") (req "*") (default-features #t) (kind 0)) (crate-dep (name "log") (req "*") (default-features #t) (kind 0)) (crate-dep (name "rust-crypto") (req "*") (default-features #t) (kind 0)))) (hash "05swwv6y4kc1nb5zmk6b33gnj3fcy5lhmnrcy81w3ip7iyl9hrbq")))

(define-public crate-conhash-0.2 (crate (name "conhash") (vers "0.2.0") (deps (list (crate-dep (name "env_logger") (req "*") (default-features #t) (kind 0)) (crate-dep (name "log") (req "*") (default-features #t) (kind 0)) (crate-dep (name "rust-crypto") (req "*") (default-features #t) (kind 0)))) (hash "06a7i86v8148v30bfvbdxgwilw6pa7g3gspn10nmpiyqnkf83v6y")))

(define-public crate-conhash-0.3 (crate (name "conhash") (vers "0.3.0") (deps (list (crate-dep (name "env_logger") (req "*") (default-features #t) (kind 0)) (crate-dep (name "log") (req "*") (default-features #t) (kind 0)) (crate-dep (name "rust-crypto") (req "*") (default-features #t) (kind 0)))) (hash "01y6bsa6ldg11v1fj5jin2134bwnxb5slvpcmhc1w485qg6m9fx8")))

(define-public crate-conhash-0.3 (crate (name "conhash") (vers "0.3.1") (deps (list (crate-dep (name "env_logger") (req "*") (default-features #t) (kind 2)) (crate-dep (name "log") (req "^0.3.4") (default-features #t) (kind 0)) (crate-dep (name "rust-crypto") (req "^0.2.34") (default-features #t) (kind 0)))) (hash "1hd4wkb5c5680j6g8vzvw5cpz0l24apifqfhrwra88yz6ljj5jkc")))

(define-public crate-conhash-0.3 (crate (name "conhash") (vers "0.3.2") (deps (list (crate-dep (name "env_logger") (req "^0.3.3") (default-features #t) (kind 2)) (crate-dep (name "log") (req "^0.3.4") (default-features #t) (kind 0)) (crate-dep (name "rust-crypto") (req "^0.2.34") (default-features #t) (kind 0)))) (hash "1f3ka6ka83zwm9rb1mjg48fmbax29il2rnb7sisbyvdqqk29cz3y")))

(define-public crate-conhash-0.3 (crate (name "conhash") (vers "0.3.3") (deps (list (crate-dep (name "env_logger") (req "^0.4") (default-features #t) (kind 2)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "rust-crypto") (req "^0.2") (default-features #t) (kind 0)))) (hash "1zn55qs0iwfl44ql0a44s2y4a4c0glf1wnyvg203bhkzal15npgl")))

(define-public crate-conhash-0.4 (crate (name "conhash") (vers "0.4.0") (deps (list (crate-dep (name "env_logger") (req "^0.4") (default-features #t) (kind 2)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "md5") (req "^0.3") (default-features #t) (kind 0)))) (hash "1jhzkf744si69mrvg4il1p8pqdysh9cgl530igcx0y47096kdmlr")))

(define-public crate-conhash-0.5 (crate (name "conhash") (vers "0.5.0") (deps (list (crate-dep (name "criterion") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "md5") (req "^0.7") (default-features #t) (kind 0)) (crate-dep (name "once_cell") (req "^1.9.0") (default-features #t) (kind 2)))) (hash "0xf7siza443c67sdcgsvp0rpdcvvqbr3hv28ia5l5159b6h689aw")))

(define-public crate-conhash-0.5 (crate (name "conhash") (vers "0.5.1") (deps (list (crate-dep (name "criterion") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "md5") (req "^0.7") (default-features #t) (kind 0)) (crate-dep (name "once_cell") (req "^1.9.0") (default-features #t) (kind 2)))) (hash "13dgzz9n0yrd2whyspmrq8qhwnjwwhxdcm83vdwr0l2kgq5rakv7")))

