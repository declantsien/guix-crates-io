(define-module (crates-io co bi) #:use-module (crates-io))

(define-public crate-cobin-0.1 (crate (name "cobin") (vers "0.1.0") (deps (list (crate-dep (name "bitflags") (req "^1.2.1") (default-features #t) (kind 0)) (crate-dep (name "block") (req "^0.1.6") (default-features #t) (kind 0)) (crate-dep (name "cty") (req "^0.2.1") (default-features #t) (kind 0)) (crate-dep (name "objc") (req "^0.2.7") (default-features #t) (kind 0)))) (hash "1fqqjp512dpjdynd3nwahaadwq43dgndk4z201sznsy8nwy439zn")))

(define-public crate-cobin-0.1 (crate (name "cobin") (vers "0.1.1") (deps (list (crate-dep (name "bitflags") (req "^1.2.1") (default-features #t) (kind 0)) (crate-dep (name "block") (req "^0.1.6") (default-features #t) (kind 0)) (crate-dep (name "cty") (req "^0.2.1") (default-features #t) (kind 0)) (crate-dep (name "objc") (req "^0.2.7") (default-features #t) (kind 0)))) (hash "0gr56ds9jf1p48sa1sycf7jy2c8w4n94qfcyxxc6savmchl15v4f")))

