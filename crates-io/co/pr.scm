(define-module (crates-io co pr) #:use-module (crates-io))

(define-public crate-copra-0.1 (crate (name "copra") (vers "0.1.0") (deps (list (crate-dep (name "bytes") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "futures") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "httparse") (req "^1.2") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "protobuf") (req "^1.4") (features (quote ("with-bytes"))) (default-features #t) (kind 0)) (crate-dep (name "protoc-rust") (req "^1.4") (default-features #t) (kind 1)) (crate-dep (name "rand") (req "^0.4") (default-features #t) (kind 2)) (crate-dep (name "smallvec") (req "^0.5") (default-features #t) (kind 0)) (crate-dep (name "tokio-core") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "tokio-io") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "tokio-proto") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "tokio-service") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "tokio-timer-plus") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "url") (req "^1.6") (default-features #t) (kind 0)))) (hash "04grj1l0y70z9q2zy25caphhz76j5a2lcwxp4l3xlclk7da2fa82") (yanked #t)))

(define-public crate-copra-0.1 (crate (name "copra") (vers "0.1.1") (deps (list (crate-dep (name "bytes") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "futures") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "httparse") (req "^1.2") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "protobuf") (req "^1.4") (features (quote ("with-bytes"))) (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.4") (default-features #t) (kind 2)) (crate-dep (name "smallvec") (req "^0.5") (default-features #t) (kind 0)) (crate-dep (name "tokio-core") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "tokio-io") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "tokio-proto") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "tokio-service") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "tokio-timer-plus") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "url") (req "^1.6") (default-features #t) (kind 0)))) (hash "0y015dg2jbf34wqi2q1rfmiwyhar5r7p52n0dj4l2syjq37wp344")))

(define-public crate-coprocessor-0.2 (crate (name "coprocessor") (vers "0.2.0") (hash "1hnv2958xq9g4y41hl87mj7kcz2aypkqcj4hq3micnw0rk9iqdyn")))

(define-public crate-coproduct-0.1 (crate (name "coproduct") (vers "0.1.0") (deps (list (crate-dep (name "frunk") (req "^0.4") (default-features #t) (kind 0)))) (hash "131qz0vik0gdmpgn14rgw4hkphp4fn0mzsj8ck5fajgd9s15pbx4")))

(define-public crate-coproduct-0.1 (crate (name "coproduct") (vers "0.1.1") (deps (list (crate-dep (name "frunk") (req "^0.4") (default-features #t) (kind 0)))) (hash "17ngms41ybf6hz35hq1qgam10zh2dbcyr5c967xldiimnkvyzb25")))

(define-public crate-coproduct-0.2 (crate (name "coproduct") (vers "0.2.0") (hash "12y7yp21r4sx4h9jzmw67ddjqai8qwg250mfnv0psrcai2mla6fy")))

(define-public crate-coproduct-0.3 (crate (name "coproduct") (vers "0.3.0") (deps (list (crate-dep (name "coproduct-idtype-macro") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "trybuild") (req "^1.0") (default-features #t) (kind 2)))) (hash "0clsviw6c3ay7lh5klnrjjc17r9hwymq6jjvwd1hcjk5gjgjp7zy")))

(define-public crate-coproduct-0.4 (crate (name "coproduct") (vers "0.4.0") (deps (list (crate-dep (name "trybuild") (req "^1.0") (default-features #t) (kind 2)))) (hash "0m3wdv3pcrfh3vbvr2lk6mprs03c8wh3xz17l1lp139szidxrxlv") (features (quote (("type_inequality_hack"))))))

(define-public crate-coproduct-0.4 (crate (name "coproduct") (vers "0.4.1") (deps (list (crate-dep (name "trybuild") (req "^1.0") (default-features #t) (kind 2)))) (hash "0dc7bpl533j3mmmgd0j9vq9icgr7pcjq59b895adrlqxrcvm6syw") (features (quote (("type_inequality_hack"))))))

(define-public crate-coproduct-idtype-macro-0.1 (crate (name "coproduct-idtype-macro") (vers "0.1.0") (deps (list (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (default-features #t) (kind 0)))) (hash "17fjhr5bak85cl1yqvrfaxp4w5p2dfpyjrqbk0fg9zhi0jz5s65d")))

(define-public crate-coprosize-0.0.0 (crate (name "coprosize") (vers "0.0.0") (hash "02fvs4azclv8blakzflngp4ywykg310nk1ci4anjjy8775fw8g0z") (yanked #t)))

(define-public crate-coprosize-1 (crate (name "coprosize") (vers "1.0.0-alpha.1") (hash "0s3a6k4syffj05i6drxvix6w34b6zia65zpg6nn8bwr87jkkw6pz") (yanked #t)))

(define-public crate-coprosize-1 (crate (name "coprosize") (vers "1.0.0-alpha.2") (hash "0r0cdgvgn0vwdai58x839wxwlz6xrf9gynwccwvvnsx700cd4z10") (yanked #t)))

(define-public crate-coprosize-1 (crate (name "coprosize") (vers "1.0.0-alpha.3") (hash "1g1r07wplcgl6v16lbfi8559w5m65wvv850g0mhs8q56fncmmnc6") (yanked #t)))

(define-public crate-coprosize-1 (crate (name "coprosize") (vers "1.0.0-alpha.4") (hash "0579yqc53jak0rwvgv6p696vqyrfjjb2vzv1vgqnrxd26vvwd8v5") (yanked #t)))

(define-public crate-coprosize-1 (crate (name "coprosize") (vers "1.0.0-alpha.5") (hash "0fzigf64qrllfsrxy4ciijvwb1aib0yjbny9nz49i19m04zm8ymj") (yanked #t)))

(define-public crate-coprosize-1 (crate (name "coprosize") (vers "1.0.0-alpha.6") (hash "0hjbj7f8ni4n9i7lpc3yh7gli8mjzfbln9nfi450c2075sda95ji") (yanked #t)))

(define-public crate-coprosize-1 (crate (name "coprosize") (vers "1.0.0-alpha.7") (hash "1b7lin3hb1bzmckyv8msalgyr2b5ajz79az8qvgv9zvb6vkk3gp0") (yanked #t)))

(define-public crate-coprosize-1 (crate (name "coprosize") (vers "1.0.0-alpha.8") (hash "00x6fa4iqfw3kvbk116c9p2liw7d4dahsnx33x03ar5rv9jf7jqy") (yanked #t)))

(define-public crate-coprosize-1 (crate (name "coprosize") (vers "1.0.0-alpha.9") (hash "05m0gq19vc91ln3al371jnyjbar451q5plangjhqqhb16llfn2j7") (yanked #t)))

(define-public crate-coprosize-1 (crate (name "coprosize") (vers "1.0.0-beta") (hash "1cqb58xjhazns0f6cjdgsswqycj27yd9i4wk8pnv5c6v9vc3kf6l") (yanked #t)))

(define-public crate-coprosize-1 (crate (name "coprosize") (vers "1.0.0") (hash "0ijfmajn36mb34f2znn7ipm716dfr46dd5wldy721nhq56fdbhk1")))

(define-public crate-coprosize-1 (crate (name "coprosize") (vers "1.0.1") (hash "055nkc2jpjsdkns8m1235lfa5w8765sr377ip491xl8lcfrndprl")))

(define-public crate-coprosize-1 (crate (name "coprosize") (vers "1.0.2") (hash "0fydmpd3s025r8a04rhfsrn8bcjb96hf8ab0fdl0rc581nynj710")))

(define-public crate-coprosize-1 (crate (name "coprosize") (vers "1.0.3") (hash "02frwmvh8dfwgidss4280y5h0c5bvhaxq2wbir8yx619swbbxh84")))

(define-public crate-coprosize-1 (crate (name "coprosize") (vers "1.0.4") (hash "06xl5794k9g2r02a60iwrpzrlxmv9naxaspr5wf3kxl6nv0ak0i7")))

