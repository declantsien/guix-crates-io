(define-module (crates-io co _m) #:use-module (crates-io))

(define-public crate-co_managed-0.1 (crate (name "co_managed") (vers "0.1.0") (deps (list (crate-dep (name "may") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "parking_lot") (req "^0.12") (default-features #t) (kind 0)))) (hash "11rd1ch5pwzykb91m7z16i9wc6ag317xhz611iylbqn4x4qmxn43")))

