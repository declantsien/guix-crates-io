(define-module (crates-io co t_) #:use-module (crates-io))

(define-public crate-cot_publisher-1 (crate (name "cot_publisher") (vers "1.0.0-rc.1") (deps (list (crate-dep (name "bytes") (req "^1.4") (default-features #t) (kind 0)) (crate-dep (name "chrono") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "openssl") (req "^0.10") (default-features #t) (kind 0)) (crate-dep (name "prost") (req "^0.11") (default-features #t) (kind 0)) (crate-dep (name "prost-build") (req "^0.11") (default-features #t) (kind 1)) (crate-dep (name "varint-rs") (req "^2.2") (default-features #t) (kind 0)))) (hash "0vdqlfkbh4lmk4d99y81hqqa8zwlxjzysbyjfci29a43wb0ws597")))

