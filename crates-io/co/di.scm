(define-module (crates-io co di) #:use-module (crates-io))

(define-public crate-codi-0.0.0 (crate (name "codi") (vers "0.0.0") (hash "1xhj80czcln5a7bwmqndvwbbm9z951kj7l3s7y5kq0n6h4zdj872")))

(define-public crate-codic-0.1 (crate (name "codic") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^2.33.3") (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.10") (features (quote ("json"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^0.2") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "1fipjmrdkhkyixx7aabx03r5a2fc042bqivigf1ymlyvh8hd3w01")))

(define-public crate-codic-0.2 (crate (name "codic") (vers "0.2.0") (deps (list (crate-dep (name "clap") (req "^2.33.3") (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.10") (features (quote ("json"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^0.2") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "0qj9njhsdi8vz3lvf1lmnfpd1c6wbks43bbnb7cg484cbs0rwrhv")))

(define-public crate-codice_fiscale-0.1 (crate (name "codice_fiscale") (vers "0.1.0") (deps (list (crate-dep (name "error-chain") (req "^0.11.0") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^0.2.2") (default-features #t) (kind 0)) (crate-dep (name "time") (req "^0.1.40") (default-features #t) (kind 0)))) (hash "1z76r7sd0fs9i724x1hmx5bag7gjz9xwlxrkl1ggmclnpg4ia31v") (yanked #t)))

(define-public crate-codice_fiscale-0.1 (crate (name "codice_fiscale") (vers "0.1.1") (deps (list (crate-dep (name "error-chain") (req "^0.11.0") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^0.2.2") (default-features #t) (kind 0)) (crate-dep (name "time") (req "^0.1.40") (default-features #t) (kind 0)))) (hash "16mcv2g2grsbg7j6n5zkhfwaq138pgw6ykjka01fq9lx86640czr") (yanked #t)))

(define-public crate-codice_fiscale-0.2 (crate (name "codice_fiscale") (vers "0.2.0") (deps (list (crate-dep (name "failure") (req "^0.1.1") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.0.1") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^0.2.11") (default-features #t) (kind 0)) (crate-dep (name "time") (req "^0.1.40") (default-features #t) (kind 0)))) (hash "16h27d4b7i0f0w0n4lpy6z1libgy6fngjhmmmcqi5ainjqrydnrk")))

(define-public crate-codice_fiscale-0.3 (crate (name "codice_fiscale") (vers "0.3.0") (deps (list (crate-dep (name "failure") (req "^0.1.8") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.3.9") (default-features #t) (kind 0)) (crate-dep (name "time") (req "^0.1.40") (default-features #t) (kind 0)))) (hash "1bh3199b4pklippj8s71vrcryzdfyd4in8k27l17lwjkgs4yb46h")))

(define-public crate-codice_fiscale-0.3 (crate (name "codice_fiscale") (vers "0.3.1") (deps (list (crate-dep (name "failure") (req "^0.1.8") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.3.9") (default-features #t) (kind 0)) (crate-dep (name "time") (req "^0.1.40") (default-features #t) (kind 0)))) (hash "1ak0gbq44r662h23gz1ys35xwwl03kw0y5h18v8ih6l67qsmgn7b")))

(define-public crate-codice_fiscale-0.4 (crate (name "codice_fiscale") (vers "0.4.0") (deps (list (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.3.9") (default-features #t) (kind 0)) (crate-dep (name "time") (req "^0.3.14") (features (quote ("parsing" "macros"))) (default-features #t) (kind 0)))) (hash "0z2ajsr51dkvq1bnfxsbsh6rhszwqs6siyw9sgq1vczy570wpq5w")))

(define-public crate-codice_fiscale-0.4 (crate (name "codice_fiscale") (vers "0.4.1") (deps (list (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.7") (default-features #t) (kind 0)) (crate-dep (name "time") (req "^0.3.14") (features (quote ("parsing" "macros"))) (default-features #t) (kind 0)))) (hash "0vghwcbfkg9m84jq2apsb5s5mms63g4mv46ir8hgmypxcl3c7mmm")))

(define-public crate-codice_fiscale_rs-0.1 (crate (name "codice_fiscale_rs") (vers "0.1.0") (deps (list (crate-dep (name "chrono") (req "^0.4.23") (default-features #t) (kind 0)) (crate-dep (name "criterion") (req "^0.4.0") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.38") (default-features #t) (kind 0)))) (hash "01g2p70rw5dh616s7wvf0wqzxvpq78qlhhkvxlr7b06d4z93968m")))

(define-public crate-codice_fiscale_rs-0.1 (crate (name "codice_fiscale_rs") (vers "0.1.1") (deps (list (crate-dep (name "chrono") (req "^0.4.23") (default-features #t) (kind 0)) (crate-dep (name "criterion") (req "^0.4.0") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.38") (default-features #t) (kind 0)))) (hash "1va08nmq8s3vhx78j4v3m35v719605ra4gqrckwplc61sajyxwj7")))

(define-public crate-codice_fiscale_rs-0.1 (crate (name "codice_fiscale_rs") (vers "0.1.2") (deps (list (crate-dep (name "chrono") (req "^0.4.23") (default-features #t) (kind 0)) (crate-dep (name "criterion") (req "^0.4.0") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.38") (default-features #t) (kind 0)))) (hash "0cnf6cqqjn3qkkpvfrl5575xayimh82kl3847239hpv7sp3x7zp5")))

(define-public crate-codice_fiscale_rs-0.1 (crate (name "codice_fiscale_rs") (vers "0.1.3") (deps (list (crate-dep (name "chrono") (req "^0.4.23") (default-features #t) (kind 0)) (crate-dep (name "criterion") (req "^0.4.0") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.38") (default-features #t) (kind 0)))) (hash "1r70kd43cmn4n2gck1f67jfzmzrdrw7ig55mynzkqf3kz55jmk4i")))

(define-public crate-codice_fiscale_rs-0.2 (crate (name "codice_fiscale_rs") (vers "0.2.0") (deps (list (crate-dep (name "chrono") (req "^0.4.23") (default-features #t) (kind 0)) (crate-dep (name "criterion") (req "^0.4.0") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.38") (default-features #t) (kind 0)))) (hash "1y5v1iiqv6jsp40gp8y59nrycvjfxyqsdaiyziw2fmc9898p40wg")))

(define-public crate-codice_fiscale_rs-0.2 (crate (name "codice_fiscale_rs") (vers "0.2.1") (deps (list (crate-dep (name "chrono") (req "^0.4.23") (default-features #t) (kind 0)) (crate-dep (name "criterion") (req "^0.4.0") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.38") (default-features #t) (kind 0)))) (hash "01bf8q071fbx4cs9ianch660ildxy1qadf3y03zvlc5ppkh1h6gi")))

(define-public crate-codicon-0.1 (crate (name "codicon") (vers "0.1.0") (hash "1d5vl7n3r9fy9z2zqawp2lzifsgv0qqzn1wm57wi3xhjv8jnd6a9")))

(define-public crate-codicon-1 (crate (name "codicon") (vers "1.0.0") (hash "17jxrhcdm5n24mx7vwa5jpfpcr93bqawwhdz2lafxqmj4ljhamq6")))

(define-public crate-codicon-2 (crate (name "codicon") (vers "2.0.0") (hash "0lx0fnzzfy9vjkbr21wc4v3sxsvq1nmcsapx9mrzisw8lcjnzgv2")))

(define-public crate-codicon-2 (crate (name "codicon") (vers "2.1.0") (hash "1y0l78lh6nv47waviad1nsxb9s03c1inmkbh0m9vaf0sbbgipj0w")))

(define-public crate-codicon-3 (crate (name "codicon") (vers "3.0.0") (hash "0qfyx6s7911b2z435p3793xd0m38hdpmk0czl44nygakyf0005qj")))

(define-public crate-codify_hoijui-0.6 (crate (name "codify_hoijui") (vers "0.6.0") (hash "1bvqn9r07bjcvq7z0vl09y5ipln6x4v1klmmvavm7q6rj5v0v35v")))

(define-public crate-codifyle-0.0.1 (crate (name "codifyle") (vers "0.0.1") (hash "15xh8i204kiwlhl5zrwya044kfjzmf1zj8b51gyr91s4d5848baw")))

(define-public crate-coding-0.1 (crate (name "coding") (vers "0.1.0") (hash "03c8mam4y0rapqfbn5lvvya4zmcxx4c1qgkk4k18m7iwgr83n0k9")))

(define-public crate-coding_benchmark-0.1 (crate (name "coding_benchmark") (vers "0.1.0") (deps (list (crate-dep (name "bit-vec") (req "^0.6") (default-features #t) (kind 0)) (crate-dep (name "bitm") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "butils") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^4") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "constriction") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "dyn_size_of") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "huffman-compress") (req "^0.6.1") (default-features #t) (kind 0)) (crate-dep (name "minimum_redundancy") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "rand_pcg") (req "^0.3") (default-features #t) (kind 0)))) (hash "0412sanm0i3gm8h76pv1xxddhb3q4hqlgj1l6f1bksggj9yv27cv")))

(define-public crate-coding_benchmark-0.1 (crate (name "coding_benchmark") (vers "0.1.1") (deps (list (crate-dep (name "bit-vec") (req "^0.6") (default-features #t) (kind 0)) (crate-dep (name "bitm") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "butils") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^4") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "constriction") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "dyn_size_of") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "huffman-compress") (req "^0.6.1") (default-features #t) (kind 0)) (crate-dep (name "minimum_redundancy") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "rand_pcg") (req "^0.3") (default-features #t) (kind 0)))) (hash "16n92xfm7zxbfgdaw943mqwx7z0lmc4bj1xpaag0h16b232r804s")))

(define-public crate-coding_benchmark-0.1 (crate (name "coding_benchmark") (vers "0.1.2") (deps (list (crate-dep (name "bit-vec") (req "^0.6") (default-features #t) (kind 0)) (crate-dep (name "bitm") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "butils") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^4") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "constriction") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "dyn_size_of") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "huffman-compress") (req "^0.6.1") (default-features #t) (kind 0)) (crate-dep (name "minimum_redundancy") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "rand_pcg") (req "^0.3") (default-features #t) (kind 0)))) (hash "1xqd3wv8556g3zf2g22lwsach4x3mw4f4prlv76dwn6hz5sr1pg7")))

(define-public crate-coding_benchmark-0.1 (crate (name "coding_benchmark") (vers "0.1.3") (deps (list (crate-dep (name "bit-vec") (req "^0.6") (default-features #t) (kind 0)) (crate-dep (name "bitm") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "butils") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^4") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "constriction") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "dyn_size_of") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "huffman-compress") (req "^0.6.1") (default-features #t) (kind 0)) (crate-dep (name "minimum_redundancy") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "rand_pcg") (req "^0.3") (default-features #t) (kind 0)))) (hash "1ramqwp8n45zhj38id481k6g8rvmpxiildm8fdxp07jgwm6sxr70")))

(define-public crate-coding_pk-0.0.0 (crate (name "coding_pk") (vers "0.0.0") (hash "1i91z1iwb3mz12x8irc1rh0ll1vgsdbb9aj04bb2bp30vgnxb7vm")))

(define-public crate-coding_pk-0.1 (crate (name "coding_pk") (vers "0.1.0") (hash "0qkdqbrmpy5ijx86xljp3m6cszidz46ghz2zlkckynr778ppxa6p")))

(define-public crate-coding_pk-0.2 (crate (name "coding_pk") (vers "0.2.0") (hash "16xhwzxx9k7gchj8s8msk19da8wv12cjib7n5cmgjdi2ahwlk6im")))

(define-public crate-coding_pk-0.3 (crate (name "coding_pk") (vers "0.3.0") (hash "0ckdnsyd9r05wjcx7pxl05974m9vgp2cy25bxwp0jpk4myd4c2zg")))

(define-public crate-coding_pk-0.4 (crate (name "coding_pk") (vers "0.4.0") (hash "1853sla3nynaprxmvll9pzbyvl3mcrdkwxd591ngcga2fcm0zgb0")))

(define-public crate-coding_pk-0.5 (crate (name "coding_pk") (vers "0.5.0") (hash "0x6i239wxqk2k9py0mz7gsa9l0jqkmj5x2pyy1hhlaba65dai5yk")))

(define-public crate-coding_pk-0.5 (crate (name "coding_pk") (vers "0.5.1") (hash "1pv75yxcb0qsv914m781dmsh5bi05dizxr3h7cvjibm07daxl87v")))

(define-public crate-coding_pk-0.6 (crate (name "coding_pk") (vers "0.6.0") (hash "0rh94lxn3pn9qsrv0k6xqp9xcfi5zjja0qz183s04l505i7i7g40")))

(define-public crate-coding_pk-0.7 (crate (name "coding_pk") (vers "0.7.0") (hash "1mg2zyigwj7mlmf26dgy13xcs7sh6h217244zr2i961z1rfq2zzy")))

(define-public crate-coding_pk-0.8 (crate (name "coding_pk") (vers "0.8.0") (hash "13mnrxcbp27cj72ag1zqyrj1s74biyfwhi5bx8nxjhpclwmv3ijq")))

(define-public crate-coding_pk-0.9 (crate (name "coding_pk") (vers "0.9.0") (hash "0pfwr84yzk30nlcbj68wjxqaf6svag4yi57r0h2zjphz06v5ml6v")))

(define-public crate-coding_pk-0.10 (crate (name "coding_pk") (vers "0.10.0") (hash "17fj86sw9yccr28sh9m0b8l6fbh0qs8gmxvf5syhkwcpfz9v7vzg")))

(define-public crate-coding_pk-0.11 (crate (name "coding_pk") (vers "0.11.0") (hash "10cs5x4qv7bh0l7gixiy0s0arypaa5mbn9ix6306rcmpnfkk5a93")))

(define-public crate-coding_pk-0.11 (crate (name "coding_pk") (vers "0.11.1") (hash "0xh67kib5ypwc79mgp1y4v6cfpnr5mpkrmmkyl72ypnkzrk54adf")))

(define-public crate-coding_pk-0.11 (crate (name "coding_pk") (vers "0.11.2") (hash "1avnwdrdvqfx0wi58dsn5681jyk8yr04sqkjmgvm0smrz51bk6zi")))

(define-public crate-coding_pk-0.12 (crate (name "coding_pk") (vers "0.12.0") (hash "1yspn1fkdbjsflr8yl8fr4w3xbd2zibbnh522r84fsjcly7db330")))

(define-public crate-coding_pk-0.13 (crate (name "coding_pk") (vers "0.13.0") (hash "0zyxyrrv2i0lc7izfhqdj8dnfssc7r44pnshyy8vipi1v4kwd9xw")))

(define-public crate-coding_quizzer_minigrep-0.1 (crate (name "coding_quizzer_minigrep") (vers "0.1.0") (hash "19zs5r14rxd3qja14d2msr83ijbl69l00vd5a06g7sfjs36npnsl")))

(define-public crate-codit-0.1 (crate (name "codit") (vers "0.1.0") (hash "11fl3wv4wx3xx0ypqd4zkxm7x6qrq056nr1g20y4255yhxwd2l5w")))

(define-public crate-codize-0.1 (crate (name "codize") (vers "0.1.0") (hash "07mx5avkxxwyay56fipyq481n05mz9n7nxva26ykzprdnsqv5yjg")))

(define-public crate-codize-0.1 (crate (name "codize") (vers "0.1.1") (hash "1528wi3s2ny786ahlqkij425d650xc1qb9apl7jmi8qjcn3860w5")))

(define-public crate-codize-0.1 (crate (name "codize") (vers "0.1.2") (hash "0k2mrbymlrgndsm36xjk419sbir2x2z71iz9fi1wppi4vqj9dxwb")))

(define-public crate-codize-0.2 (crate (name "codize") (vers "0.2.0") (hash "0h8p091662gl7jy66c36jj6p4ch6pqxj1qfy136frl2ak100vy8m")))

(define-public crate-codize-0.3 (crate (name "codize") (vers "0.3.0") (deps (list (crate-dep (name "derivative") (req "^2.2.0") (default-features #t) (kind 0)) (crate-dep (name "indoc") (req "^2.0.5") (default-features #t) (kind 2)))) (hash "15xxlv5w86wsmncy43a9xxlggn55skvmm01cxr1y69yzzk4nkgf0")))

(define-public crate-codize-0.3 (crate (name "codize") (vers "0.3.1") (deps (list (crate-dep (name "derivative") (req "^2.2.0") (default-features #t) (kind 0)) (crate-dep (name "indoc") (req "^2.0.5") (default-features #t) (kind 2)))) (hash "0y2i819v7b8m91xnk2ljq9gf4xrnl8p3b89sn8gr9b8dzm07jz8f")))

(define-public crate-codize-0.3 (crate (name "codize") (vers "0.3.2") (deps (list (crate-dep (name "derivative") (req "^2.2.0") (default-features #t) (kind 0)) (crate-dep (name "indoc") (req "^2.0.5") (default-features #t) (kind 2)))) (hash "0ihmhj1www2ws80iymjz9lvvmjg7kz0dxc2v7zzljmvalaxgrdjw")))

(define-public crate-codize-0.3 (crate (name "codize") (vers "0.3.3") (deps (list (crate-dep (name "derivative") (req "^2.2.0") (default-features #t) (kind 0)) (crate-dep (name "indoc") (req "^2.0.5") (default-features #t) (kind 2)))) (hash "1gcg41w0n8a3cr30lg13ck9ppa394w8pmxddms9w1a1vcalc5icz")))

