(define-module (crates-io co w_) #:use-module (crates-io))

(define-public crate-cow_arc-0.2 (crate (name "cow_arc") (vers "0.2.0") (hash "1y871rb333p3wvvgam8049jgslx2h90n0fg5fnrjbg6jlla794k4")))

(define-public crate-cow_arc-0.2 (crate (name "cow_arc") (vers "0.2.1") (hash "1aqakh5ma7kza9gr1775ag259cdqyrkbd6yc54i778drjm9080wa")))

(define-public crate-cow_arc-0.2 (crate (name "cow_arc") (vers "0.2.2") (hash "0wh8cjk047xjakdqszc9zg3f6cv8il0xijfczg72bab5qgvskqb8")))

(define-public crate-cow_struct-0.3 (crate (name "cow_struct") (vers "0.3.1") (deps (list (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (features (quote ("extra-traits"))) (default-features #t) (kind 0)))) (hash "16q2s9d26drif36qc6331pyjbsp339csbaqs0ls3j2ayyk1zj7d0") (yanked #t)))

(define-public crate-cow_struct-0.0.1 (crate (name "cow_struct") (vers "0.0.1") (deps (list (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (features (quote ("extra-traits"))) (default-features #t) (kind 0)))) (hash "1dd6nmab7w40kghalv883kwvs1z75jn7fd6y2rs73s1l4f2n60vq")))

(define-public crate-cow_vec_item-0.1 (crate (name "cow_vec_item") (vers "0.1.0") (hash "0gjvj1hclxilabpmrmv625y8q5dv4sd47ayqcyp7cg9hpwmrm5vh")))

(define-public crate-cow_vec_item-0.2 (crate (name "cow_vec_item") (vers "0.2.0") (hash "0kdl1143xhxmfdl2r50fzbpq97aaca6chrhlfb3crznk0zp7fzfm")))

(define-public crate-cow_vec_item-0.3 (crate (name "cow_vec_item") (vers "0.3.0") (hash "0z0rbj6m4a0q27a6a17nppbbkh8dj4lcndvkz9g1i224zijmibfs")))

(define-public crate-cow_vec_item-0.4 (crate (name "cow_vec_item") (vers "0.4.0") (hash "0x79sg2a17g8f4jlm12jpprcn45myvvb7a04j5xmqahp00i4wkzm")))

(define-public crate-cow_vec_item-0.5 (crate (name "cow_vec_item") (vers "0.5.0") (hash "0fac5pl4d35plkl5m4cyxkkp91gb2plx94ylmagl181ak9n0z5jn")))

(define-public crate-cow_vec_item-0.5 (crate (name "cow_vec_item") (vers "0.5.1") (hash "1j7b0d90barqcvsmq24nc5iba4rwqcafljbr4ncfdsbz9lzbqsix")))

