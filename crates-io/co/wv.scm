(define-module (crates-io co wv) #:use-module (crates-io))

(define-public crate-cowvec-0.1 (crate (name "cowvec") (vers "0.1.0") (hash "18khsx555159xnkk30lsci9jghy9id029plgfk6ar92ckhm62hxf")))

(define-public crate-cowvec-0.1 (crate (name "cowvec") (vers "0.1.1") (hash "1mpn6rf5id2fh986v577dm9sqq83v2vay9mijqpigfn772n1zng8")))

(define-public crate-cowvec-0.1 (crate (name "cowvec") (vers "0.1.2") (hash "1z0d4rfhldqwbjxqpf0dmmb6vp261j7hi02vhp4m9pvia8yvkhl1")))

