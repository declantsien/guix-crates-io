(define-module (crates-io co o1) #:use-module (crates-io))

(define-public crate-coo1-0.1 (crate (name "coo1") (vers "0.1.0") (deps (list (crate-dep (name "futures") (req "^0.3.5") (default-features #t) (kind 0)) (crate-dep (name "pretty_env_logger") (req "^0.4.0") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^0.2.22") (features (quote ("full"))) (default-features #t) (kind 0)) (crate-dep (name "warp") (req "^0.2.5") (default-features #t) (kind 0)))) (hash "18g3kx6jvgvxz2akarpd4klwfrmi4h9wvygzmf9iqmv55wxb3fwr")))

