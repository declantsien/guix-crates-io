(define-module (crates-io co du) #:use-module (crates-io))

(define-public crate-codump-0.1 (crate (name "codump") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^4.3.11") (features (quote ("cargo" "derive"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.9.1") (default-features #t) (kind 0)) (crate-dep (name "toml") (req "^0.7.6") (default-features #t) (kind 2)))) (hash "161gjb8f7i0qs5z4k6bdjg9zwg1mvc3z2i0h5kk7jbazvkp6i47h") (features (quote (("cli" "clap"))))))

(define-public crate-codump-0.1 (crate (name "codump") (vers "0.1.1") (deps (list (crate-dep (name "clap") (req "^4.3.11") (features (quote ("cargo" "derive"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.9.1") (default-features #t) (kind 0)) (crate-dep (name "toml") (req "^0.7.6") (default-features #t) (kind 2)))) (hash "13xjpylqdyj19gz67w6p2hyn9q5c6kfdj9pdg53flddn791qyfhl") (features (quote (("cli" "clap"))))))

