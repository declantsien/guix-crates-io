(define-module (crates-io co wo) #:use-module (crates-io))

(define-public crate-cowonverter-1 (crate (name "cowonverter") (vers "1.0.0") (deps (list (crate-dep (name "clap") (req "^2.33.3") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.3.13") (default-features #t) (kind 0)))) (hash "0d1bzlfn3nnvjm199s5nz4na037zx2fqhg2h2w5aa1jirbr62qnf") (yanked #t)))

(define-public crate-cowonverter-2 (crate (name "cowonverter") (vers "2.0.0") (hash "1wy5vqc60vahchh437byr4vfpl5v9m5jh913l9fhnfds9i81c0y9")))

(define-public crate-cowonverter-2 (crate (name "cowonverter") (vers "2.1.0") (hash "1kc68y300bsrmw81pnd8qgmr4cx7kx2j16d2jqiivzrccz85y7hb")))

(define-public crate-cowonverter-2 (crate (name "cowonverter") (vers "2.2.0") (hash "00415lvi982zqrsdijj7b3ch0ngsw88jsnk5fp3lc8klq3zi188z")))

(define-public crate-cowonverter-2 (crate (name "cowonverter") (vers "2.3.0") (hash "1ca94p5gncw6fv7z0h8mr3fqinnpcz6qdcx8vl3c81ml01fa5n59")))

(define-public crate-cowonverter-2 (crate (name "cowonverter") (vers "2.4.0") (hash "1gl99pwl4gcn6ma4m1572j47sd422y6p6sh5w048083sq9rfi1k9")))

(define-public crate-cowonverter-2 (crate (name "cowonverter") (vers "2.5.0") (hash "1v5y3r9pqznhjmw77rbgx17mvqw4fzcpiknm6drrbjm0j4lv0wsi")))

(define-public crate-cowor-0.1 (crate (name "cowor") (vers "0.1.0") (hash "1sq6pldc4nj4m7gd7bncr6m9snyfzncgaz879jc1myazifgsqsxa")))

