(define-module (crates-io co ac) #:use-module (crates-io))

(define-public crate-coachman-0.1 (crate (name "coachman") (vers "0.1.0") (deps (list (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "slab") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.13") (features (quote ("rt" "sync" "macros" "time"))) (default-features #t) (kind 0)))) (hash "0lsli5lcqpajcd4nfgkmr4xw6zcflrar7b97j3k2n4vpl3d792q4")))

(define-public crate-coachman-0.2 (crate (name "coachman") (vers "0.2.0") (deps (list (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "slab") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.13") (features (quote ("rt" "sync" "macros" "time"))) (default-features #t) (kind 0)))) (hash "0vl02gw93qzhdw518x1kamydca04cq4mvsfd0bdyr8vv5lz5dyyr")))

(define-public crate-coachman-0.3 (crate (name "coachman") (vers "0.3.0") (deps (list (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "slab") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.13") (features (quote ("rt" "sync" "macros" "time"))) (default-features #t) (kind 0)))) (hash "13s725n89av4dbgj8zcpjxbyl7r3qbd1z1cr4mffb81lgb63pj01")))

