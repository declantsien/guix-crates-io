(define-module (crates-io co le) #:use-module (crates-io))

(define-public crate-colerr-1 (crate (name "colerr") (vers "1.0.0") (deps (list (crate-dep (name "docopt") (req "^0.6.18") (default-features #t) (kind 0)) (crate-dep (name "env_logger") (req "^0.3.3") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2.13") (default-features #t) (kind 0)) (crate-dep (name "mioco") (req "^0.7.0") (default-features #t) (kind 0)) (crate-dep (name "nix") (req "^0.6.0") (default-features #t) (kind 0)) (crate-dep (name "rustc-serialize") (req "^0.3.19") (default-features #t) (kind 0)))) (hash "03zsjlcs500l4n40vzjgvzwjldm3j53k1nhns4pasd579yqf70q4")))

