(define-module (crates-io co bh) #:use-module (crates-io))

(define-public crate-cobhan-0.1 (crate (name "cobhan") (vers "0.1.0") (deps (list (crate-dep (name "libc") (req "^0.2.103") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.68") (default-features #t) (kind 0)) (crate-dep (name "tempfile") (req "^3.2.0") (default-features #t) (kind 0)))) (hash "0y3z79y1cwviqfxljb2m9gc33rgh5qylldma9202kr8ag8if2y7p") (features (quote (("cobhan_debug"))))))

(define-public crate-cobhan-0.1 (crate (name "cobhan") (vers "0.1.1") (deps (list (crate-dep (name "libc") (req "^0.2.103") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.68") (default-features #t) (kind 0)) (crate-dep (name "tempfile") (req "^3.2.0") (default-features #t) (kind 0)))) (hash "1bxnsgg041rh49alxmsh9l8bnd6g7d1vgvz0s0fmaqivdql9nm6v") (features (quote (("cobhan_debug"))))))

