(define-module (crates-io co se) #:use-module (crates-io))

(define-public crate-cose-0.1 (crate (name "cose") (vers "0.1.0") (hash "1azjqyr5kf1q33fdmcxs67xv3w1myp6sigbr15m905d6zp2rgi44")))

(define-public crate-cose-0.1 (crate (name "cose") (vers "0.1.1") (deps (list (crate-dep (name "scopeguard") (req "^0.3") (default-features #t) (kind 2)))) (hash "0wdp9084s4m1zafj638xi65l2nw37j6a8ic91zi8bm2gav9qn8s2") (features (quote (("default"))))))

(define-public crate-cose-0.1 (crate (name "cose") (vers "0.1.2") (deps (list (crate-dep (name "scopeguard") (req "^0.3") (default-features #t) (kind 2)))) (hash "0sv4wnkaydbj6r85ar6jr3s59fgsfhs9l6cfy1bsb3zk55k8247c") (features (quote (("default"))))))

(define-public crate-cose-0.1 (crate (name "cose") (vers "0.1.3") (deps (list (crate-dep (name "scopeguard") (req "^0.3") (default-features #t) (kind 2)))) (hash "0pvcf6fjpdnsdwf06ckxdfvbhlayr12fw9884qjs7kvg3qyzxw2m") (features (quote (("default"))))))

(define-public crate-cose-0.1 (crate (name "cose") (vers "0.1.4") (deps (list (crate-dep (name "moz_cbor") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "scopeguard") (req "^0.3") (default-features #t) (kind 2)))) (hash "03armrxfyfshgypgpypa1wi09kjpxl7pvmk31yvy8fhx2p5jdykj") (features (quote (("default"))))))

(define-public crate-cose-c-0.1 (crate (name "cose-c") (vers "0.1.0") (deps (list (crate-dep (name "cose") (req "^0.1") (default-features #t) (kind 0)))) (hash "05yzbivrwhzr7rcr89xr8z0k3pf2gaavz1ia2cj4k6cqlbms12dk")))

(define-public crate-cose-c-0.1 (crate (name "cose-c") (vers "0.1.1") (deps (list (crate-dep (name "cose") (req "^0.1.2") (default-features #t) (kind 0)))) (hash "0bzwn0kff85bgqqlv1glqwmp7dvg9ddbhsckxx0kbsf2bsw8pk07")))

(define-public crate-cose-c-0.1 (crate (name "cose-c") (vers "0.1.2") (deps (list (crate-dep (name "cose") (req "^0.1.2") (default-features #t) (kind 0)))) (hash "0awm0q6fifafb2788zcp1m1sva7dal1llf6rkcr3hcq4lg9kaqml")))

(define-public crate-cose-c-0.1 (crate (name "cose-c") (vers "0.1.3") (deps (list (crate-dep (name "cose") (req "^0.1.2") (default-features #t) (kind 0)))) (hash "1p5rn7bdlwgjia25gvw0anppbam2jw9hb47pr3wxzn139kh43aqx")))

(define-public crate-cose-c-0.1 (crate (name "cose-c") (vers "0.1.4") (deps (list (crate-dep (name "cose") (req "^0.1.3") (default-features #t) (kind 0)))) (hash "16s1vl7hzrcjbwgbijajw1bfflwv2pxks2i5d7bw2k8rdnd9ayj8")))

(define-public crate-cose-c-0.1 (crate (name "cose-c") (vers "0.1.5") (deps (list (crate-dep (name "cose") (req "^0.1.4") (default-features #t) (kind 0)))) (hash "154szzzjznjlym9zlxx7jnvicfjlg951x9nc9wa6b9qcmcan0wj9")))

(define-public crate-cose-rs-0.0.0 (crate (name "cose-rs") (vers "0.0.0") (hash "1x115k2kxbmawnk67nq1kagxk50ibiifd2nx0ipzyxq0ygw94666")))

(define-public crate-cose-rust-0.1 (crate (name "cose-rust") (vers "0.1.0") (deps (list (crate-dep (name "cbor-codec") (req "^0.7.1") (default-features #t) (kind 0)) (crate-dep (name "hex") (req "^0.4.3") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "openssl") (req "^0.10") (features (quote ("vendored"))) (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.3") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.64") (optional #t) (default-features #t) (kind 0)))) (hash "14l89frbz9cs190sf3cgzbkxy4glma6k18v49d0fq2h7lh173yvf") (features (quote (("json" "hex" "serde_json"))))))

(define-public crate-cose-rust-0.1 (crate (name "cose-rust") (vers "0.1.1") (deps (list (crate-dep (name "cbor-codec") (req "^0.7.1") (default-features #t) (kind 0)) (crate-dep (name "hex") (req "^0.4.3") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "openssl") (req "^0.10") (features (quote ("vendored"))) (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.3") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.64") (optional #t) (default-features #t) (kind 0)))) (hash "19hkbgl6bad0ym4wf2cr3vj8ivws735cw2n82hizzpx9hmrhz9b3") (features (quote (("json" "hex" "serde_json"))))))

(define-public crate-cose-rust-0.1 (crate (name "cose-rust") (vers "0.1.2") (deps (list (crate-dep (name "cbor-codec") (req "^0.7.1") (default-features #t) (kind 0)) (crate-dep (name "hex") (req "^0.4.3") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "openssl") (req "^0.10") (features (quote ("vendored"))) (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.3") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.64") (optional #t) (default-features #t) (kind 0)))) (hash "110880a3vrxprsmj43qg09mcpg2c9ji9dilwli1srjj2gqkypdh9") (features (quote (("json" "hex" "serde_json"))))))

(define-public crate-cose-rust-0.1 (crate (name "cose-rust") (vers "0.1.3") (deps (list (crate-dep (name "cbor-codec") (req "^0.7.1") (default-features #t) (kind 0)) (crate-dep (name "hex") (req "^0.4.3") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "openssl") (req "^0.10") (features (quote ("vendored"))) (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.3") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.64") (optional #t) (default-features #t) (kind 0)))) (hash "0wfprp6gbxikr9w1vxvymycs1px7jhj02hvvh8c8xmwkis2hk94k") (features (quote (("json" "hex" "serde_json"))))))

(define-public crate-cose-rust-0.1 (crate (name "cose-rust") (vers "0.1.4") (deps (list (crate-dep (name "cbor-codec") (req "^0.7.1") (default-features #t) (kind 0)) (crate-dep (name "hex") (req "^0.4.3") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "openssl") (req "^0.10") (features (quote ("vendored"))) (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.3") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.64") (optional #t) (default-features #t) (kind 0)))) (hash "1y7hskdwsmy0gg9pjmr150fmlv8qs24rrnfwjnc4z0j98swyzhsa") (features (quote (("json" "hex" "serde_json"))))))

(define-public crate-cose-rust-0.1 (crate (name "cose-rust") (vers "0.1.5") (deps (list (crate-dep (name "cbor-codec") (req "^0.7.1") (default-features #t) (kind 0)) (crate-dep (name "hex") (req "^0.4.3") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "openssl") (req "^0.10") (features (quote ("vendored"))) (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.3") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.64") (optional #t) (default-features #t) (kind 0)))) (hash "16wp5nkmj57i09zr4ham6bg7pc9ygbz4baqmfgndk3y2lnfa304m") (features (quote (("json" "hex" "serde_json"))))))

(define-public crate-cose-rust-0.1 (crate (name "cose-rust") (vers "0.1.6") (deps (list (crate-dep (name "cbor-codec") (req "^0.7.1") (default-features #t) (kind 0)) (crate-dep (name "hex") (req "^0.4.3") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "openssl") (req "^0.10") (features (quote ("vendored"))) (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.3") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.64") (optional #t) (default-features #t) (kind 0)))) (hash "1h9hjc676fb4ma6qg71mpx7bxryds7rh7zazzgadilf0b05j1apq") (features (quote (("json" "hex" "serde_json"))))))

(define-public crate-cose-rust-0.1 (crate (name "cose-rust") (vers "0.1.7") (deps (list (crate-dep (name "cbor-codec") (req "^0.7.1") (default-features #t) (kind 0)) (crate-dep (name "hex") (req "^0.4.3") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "openssl") (req "^0.10") (features (quote ("vendored"))) (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.3") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.64") (optional #t) (default-features #t) (kind 0)))) (hash "115bwfc1d0b24l1x764npl5k7k212xflk8bzbdsr7kkjkccb88gj") (features (quote (("json" "hex" "serde_json"))))))

(define-public crate-cose-rust-plus-0.2 (crate (name "cose-rust-plus") (vers "0.2.0") (deps (list (crate-dep (name "cbor-codec-plus") (req "^0.8.0") (default-features #t) (kind 0)) (crate-dep (name "hex") (req "^0.4") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "hex") (req "^0.4") (default-features #t) (kind 2)) (crate-dep (name "openssl") (req "^0.10") (features (quote ("vendored"))) (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (optional #t) (default-features #t) (kind 0)))) (hash "1k1rciw68wr9kxnn4bax72p9fgh1p5sp2q229nid1l6rylhjdwy7") (features (quote (("json" "hex" "serde_json"))))))

(define-public crate-coset-0.1 (crate (name "coset") (vers "0.1.0") (deps (list (crate-dep (name "hex") (req "^0.4.2") (default-features #t) (kind 2)) (crate-dep (name "maplit") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.123") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_cbor") (req "^0.11.1") (features (quote ("std" "tags"))) (default-features #t) (kind 0)))) (hash "0n4skknpkda6alnsfp9kh85v8879g1cxw0r1q2ln8vk1zgd0fjgr")))

(define-public crate-coset-0.1 (crate (name "coset") (vers "0.1.1") (deps (list (crate-dep (name "hex") (req "^0.4.2") (default-features #t) (kind 2)) (crate-dep (name "maplit") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.126") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_cbor") (req "^0.11.1") (features (quote ("std" "tags"))) (default-features #t) (kind 0)))) (hash "0m6ycljr5a6gdbn6263yig7picpd938mrmdhjsjjmbz04fjixczs")))

(define-public crate-coset-0.1 (crate (name "coset") (vers "0.1.2") (deps (list (crate-dep (name "hex") (req "^0.4.2") (default-features #t) (kind 2)) (crate-dep (name "maplit") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.127") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_cbor") (req "^0.11.1") (features (quote ("std" "tags"))) (default-features #t) (kind 0)))) (hash "0sh5wq710s5860c198phip1pnabpqjcahsvjf6yr9gw9gc7hrnzm")))

(define-public crate-coset-0.2 (crate (name "coset") (vers "0.2.0") (deps (list (crate-dep (name "hex") (req "^0.4.2") (default-features #t) (kind 2)) (crate-dep (name "sk-cbor") (req "^0.1.2") (default-features #t) (kind 0)))) (hash "0mrlsysdyygamjcr0hfca2n95fmfp2lq234kv2ls5zdl1jysybmq")))

(define-public crate-coset-0.3 (crate (name "coset") (vers "0.3.0") (deps (list (crate-dep (name "ciborium") (req "^0.2.0") (kind 0)) (crate-dep (name "ciborium-io") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "hex") (req "^0.4.2") (default-features #t) (kind 2)))) (hash "0z33mdxncr2jfv51ydqnhfgm5j51k45sm2xxnym3r7d7qs4gbhc7")))

(define-public crate-coset-0.3 (crate (name "coset") (vers "0.3.1") (deps (list (crate-dep (name "ciborium") (req "^0.2.0") (kind 0)) (crate-dep (name "ciborium-io") (req "^0.2.0") (features (quote ("alloc"))) (default-features #t) (kind 0)) (crate-dep (name "hex") (req "^0.4.2") (default-features #t) (kind 2)))) (hash "16g3k6swlb0fhnhp3sj8z5pizkxc25fxnq2v359rgvg6rd0pgc3k")))

(define-public crate-coset-0.3 (crate (name "coset") (vers "0.3.2") (deps (list (crate-dep (name "ciborium") (req "^0.2.0") (kind 0)) (crate-dep (name "ciborium-io") (req "^0.2.0") (features (quote ("alloc"))) (default-features #t) (kind 0)) (crate-dep (name "hex") (req "^0.4.2") (default-features #t) (kind 2)))) (hash "1jya5im9a93jmb76lx1mz1ckddjj3jf733njxx7i994vhkw3zg2m")))

(define-public crate-coset-0.3 (crate (name "coset") (vers "0.3.3") (deps (list (crate-dep (name "ciborium") (req "^0.2.0") (kind 0)) (crate-dep (name "ciborium-io") (req "^0.2.0") (features (quote ("alloc"))) (default-features #t) (kind 0)) (crate-dep (name "hex") (req "^0.4.2") (default-features #t) (kind 2)))) (hash "12wkrfkyg08f0mm6z7h9v6c0zby0sgv8ry6xp42ywkvggc7v6k30")))

(define-public crate-coset-0.3 (crate (name "coset") (vers "0.3.4") (deps (list (crate-dep (name "ciborium") (req "^0.2.0") (kind 0)) (crate-dep (name "ciborium-io") (req "^0.2.0") (features (quote ("alloc"))) (default-features #t) (kind 0)) (crate-dep (name "hex") (req "^0.4.2") (default-features #t) (kind 2)))) (hash "158glv25450qwwhh0jhky2myqzssk5hcv7wflilmrwnfabla8rf7") (features (quote (("std") ("default"))))))

(define-public crate-coset-0.3 (crate (name "coset") (vers "0.3.5") (deps (list (crate-dep (name "ciborium") (req "^0.2.1") (kind 0)) (crate-dep (name "ciborium-io") (req "^0.2.0") (features (quote ("alloc"))) (default-features #t) (kind 0)) (crate-dep (name "hex") (req "^0.4.2") (default-features #t) (kind 2)))) (hash "1m8i7hqvaaspyrd1pqzkxj0sisiz696sx76pas453d68qnxi9hlr") (features (quote (("std") ("default"))))))

(define-public crate-coset-0.3 (crate (name "coset") (vers "0.3.6") (deps (list (crate-dep (name "ciborium") (req "^0.2.1") (kind 0)) (crate-dep (name "ciborium-io") (req "^0.2.0") (features (quote ("alloc"))) (default-features #t) (kind 0)) (crate-dep (name "hex") (req "^0.4.2") (default-features #t) (kind 2)))) (hash "1ndbj4nd5f9iq6c9krsi4kwj5iic76zhb2l42j38nhaqxs4gdivp") (features (quote (("std") ("default"))))))

(define-public crate-coset-0.3 (crate (name "coset") (vers "0.3.7") (deps (list (crate-dep (name "ciborium") (req "^0.2.1") (kind 0)) (crate-dep (name "ciborium-io") (req "^0.2.0") (features (quote ("alloc"))) (default-features #t) (kind 0)) (crate-dep (name "hex") (req "^0.4.2") (default-features #t) (kind 2)))) (hash "0g2fdsa9pycb2y4jl95lkx6nr8mm3q2i6ac1gsjdm1hz1j2sv2pz") (features (quote (("std") ("default"))))))

(define-public crate-cosey-0.1 (crate (name "cosey") (vers "0.1.0-alpha.0") (deps (list (crate-dep (name "heapless-bytes") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (kind 0)) (crate-dep (name "serde_repr") (req "^0.1") (default-features #t) (kind 0)))) (hash "1aybcn100g00g075bqhig4i56gwf47x3d9hfx4ib62hqfby9z7pa")))

(define-public crate-cosey-0.1 (crate (name "cosey") (vers "0.1.0") (deps (list (crate-dep (name "heapless-bytes") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (kind 0)) (crate-dep (name "serde_repr") (req "^0.1") (default-features #t) (kind 0)))) (hash "0wzlji41i4lycz3vpzc70bkwkpakjbc5r4j576c6dp550i85fh9g")))

(define-public crate-cosey-0.2 (crate (name "cosey") (vers "0.2.0") (deps (list (crate-dep (name "heapless-bytes") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (kind 0)) (crate-dep (name "serde_repr") (req "^0.1") (default-features #t) (kind 0)))) (hash "1y3l9kmks23mx4yqg7fkxlqhccyr6pnnizlyf8ib1yf8c6z0xrys")))

(define-public crate-cosey-0.3 (crate (name "cosey") (vers "0.3.0") (deps (list (crate-dep (name "heapless-bytes") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (kind 0)) (crate-dep (name "serde_repr") (req "^0.1") (default-features #t) (kind 0)))) (hash "1apxmjmml1bf67j3m3knjf1zrz3m5xapzfpm0879g6xnb0n3qx5v")))

