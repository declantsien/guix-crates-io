(define-module (crates-io co z-) #:use-module (crates-io))

(define-public crate-coz-temporary-0.1 (crate (name "coz-temporary") (vers "0.1.3") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "once_cell") (req "^1.1") (default-features #t) (kind 0)))) (hash "0p4hfmsq622dkvf888cjwigvfy2pmwby887q95h4l11xcmra0s0j")))

