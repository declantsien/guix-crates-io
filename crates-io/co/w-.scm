(define-module (crates-io co w-) #:use-module (crates-io))

(define-public crate-cow-cards-rs-0.1 (crate (name "cow-cards-rs") (vers "0.1.0") (deps (list (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)))) (hash "1c7czb660fnpjp1imi5zjxyfah4m5frqiaq9406gcjynpxfajm7k")))

(define-public crate-cow-dice-rs-0.1 (crate (name "cow-dice-rs") (vers "0.1.0") (deps (list (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)))) (hash "0v0ra8110m1qwxhgav0097jg8y0xq30cwgk1hrl7iqimfi9afjlh")))

(define-public crate-cow-rc-str-0.1 (crate (name "cow-rc-str") (vers "0.1.0") (hash "05jw8n3hwsndq9dw7vhgzzw9vbvkrfl5s3a1axyi4iijwj2xyvvp")))

(define-public crate-cow-utils-0.1 (crate (name "cow-utils") (vers "0.1.0") (deps (list (crate-dep (name "assert_matches") (req "^1.3.0") (default-features #t) (kind 2)))) (hash "1br8xza6w97bijxvylanc07di10wahvjy4k8dadbk5fpz9d9bp3r") (features (quote (("nightly") ("default"))))))

(define-public crate-cow-utils-0.1 (crate (name "cow-utils") (vers "0.1.1") (deps (list (crate-dep (name "assert_matches") (req "^1.3.0") (default-features #t) (kind 2)))) (hash "0hkvqghfxk28k3gsqavgvg5glc9r8rjqpsfw7iw4ap9pdy0sb2ad") (features (quote (("nightly") ("default"))))))

(define-public crate-cow-utils-0.1 (crate (name "cow-utils") (vers "0.1.2") (deps (list (crate-dep (name "assert_matches") (req "^1.3.0") (default-features #t) (kind 2)) (crate-dep (name "criterion") (req "^0.3.1") (default-features #t) (kind 2)))) (hash "0wq1hcqj17ivzb45w3l0l87q81srapvpbqxf055x4xazmzgkmfvr") (features (quote (("nightly") ("default"))))))

(define-public crate-cow-utils-0.1 (crate (name "cow-utils") (vers "0.1.3") (deps (list (crate-dep (name "assert_matches") (req "^1.3.0") (default-features #t) (kind 2)) (crate-dep (name "criterion") (req "^0.3.1") (default-features #t) (kind 2)))) (hash "0y9cxf0hm2hy4bn050wl2785md5q4i5gy9asjq006ip1mwjfyys1") (features (quote (("nightly") ("default"))))))

