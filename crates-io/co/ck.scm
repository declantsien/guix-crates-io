(define-module (crates-io co ck) #:use-module (crates-io))

(define-public crate-cock-cli-1 (crate (name "cock-cli") (vers "1.0.0") (deps (list (crate-dep (name "cock-lib") (req "^1.0.0") (default-features #t) (kind 0)))) (hash "1vh24nqvy6zp98ainc8v787sw3p5z0biinpxngmak128rn17h1nv")))

(define-public crate-cock-cli-1 (crate (name "cock-cli") (vers "1.0.1") (deps (list (crate-dep (name "cock-lib") (req "^1.0.1") (default-features #t) (kind 0)))) (hash "12zxcs8nbm97vkzh75i03r1q32vx61pbfk40xpq96alv2x1xf09k")))

(define-public crate-cock-cli-2 (crate (name "cock-cli") (vers "2.0.0") (deps (list (crate-dep (name "cock-lib") (req "^2.1.0") (default-features #t) (kind 0)))) (hash "15kfy09584bvbjqqk0vfqj6ns589rv89x1wpjsczzkdl1r56n41d")))

(define-public crate-cock-lib-1 (crate (name "cock-lib") (vers "1.0.0") (deps (list (crate-dep (name "cursive") (req "^0.20.0") (default-features #t) (kind 0)))) (hash "1z7zdlw8b6x4b3h0f0y2fipdk4q99hwh8n0r29f09c6n2kdahh16")))

(define-public crate-cock-lib-1 (crate (name "cock-lib") (vers "1.0.1") (deps (list (crate-dep (name "cursive") (req "^0.20.0") (default-features #t) (kind 0)))) (hash "0d2gdpf0ph1w8sshsv7n3n1444wgawk2jpw5npnz9x6zxa6r6rbb")))

(define-public crate-cock-lib-1 (crate (name "cock-lib") (vers "1.1.0") (deps (list (crate-dep (name "cursive") (req "^0.20.0") (default-features #t) (kind 0)))) (hash "124nzh3lsqmr6p3dj6jynp70h3fvrxiv6gk333a5w0ahp69v8las")))

(define-public crate-cock-lib-2 (crate (name "cock-lib") (vers "2.0.0") (hash "0zs805n6x47mvbdz0d8klycads8rvppii74ycww9sbj998ccrrqj")))

(define-public crate-cock-lib-2 (crate (name "cock-lib") (vers "2.1.0") (hash "1sxhfwhv4irqvp2dqanxsl115i0xi1pwkh9wcm40ynd06nzjclil")))

(define-public crate-cock-lib-2 (crate (name "cock-lib") (vers "2.2.0") (deps (list (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "0v0bkz059x2dwxx8nb76jqj7h42ir8px71cw29r0ikcv9qd4smm2") (features (quote (("bin_use"))))))

(define-public crate-cock-tier-0.1 (crate (name "cock-tier") (vers "0.1.0") (hash "105b5rsafhydzaxank3bszrmhrwv8p6aq5f81g6s55qqxsh54zd1") (yanked #t)))

(define-public crate-cock-tier-0.1 (crate (name "cock-tier") (vers "0.1.1") (hash "1hd0nhqdnywh59c4fd5s8px7qj4scn1wvlz0kg6kpz1hm9m15k7j") (yanked #t)))

(define-public crate-cock-tier-0.1 (crate (name "cock-tier") (vers "0.1.2") (hash "0sxp43xc08s3vngpl4a4xl6y55zrr2ndcrz4lriy4ab6f3n9gyxq") (yanked #t)))

(define-public crate-cock-tier-0.1 (crate (name "cock-tier") (vers "0.1.3") (deps (list (crate-dep (name "cursive") (req "^0.20.0") (default-features #t) (kind 0)))) (hash "1hq232pc42sh6n5qg0wk3g4mgrg7kzr4fn49zpq9qn7c5r0rwss5") (yanked #t)))

(define-public crate-cock-tier-0.1 (crate (name "cock-tier") (vers "0.1.4") (deps (list (crate-dep (name "cursive") (req "^0.20.0") (default-features #t) (kind 0)))) (hash "1bjw0kbblmcny7rahlw4yr76q3qxx6ksp2cx706gfz8kg9q8zxnz") (yanked #t)))

(define-public crate-cock-tier-0.2 (crate (name "cock-tier") (vers "0.2.0") (deps (list (crate-dep (name "cursive") (req "^0.20.0") (default-features #t) (kind 0)))) (hash "009aby78wsp64p4zpwx2zr03awbfbjdwysg9vbry00md7y33rb7j") (yanked #t)))

(define-public crate-cock-tier-0.2 (crate (name "cock-tier") (vers "0.2.1") (deps (list (crate-dep (name "cursive") (req "^0.20.0") (default-features #t) (kind 0)))) (hash "1z869r2bvkzdx565l1lmpbb29mzs0qnalsyqpgdzb4srav05rxgc") (yanked #t)))

(define-public crate-cock-tier-0.2 (crate (name "cock-tier") (vers "0.2.2") (deps (list (crate-dep (name "cursive") (req "^0.20.0") (default-features #t) (kind 0)))) (hash "0ki3lgxk806ck36995mqyy09kamk6j6869l2s4vrrcblm8in4b8j") (yanked #t)))

(define-public crate-cock-tier-0.2 (crate (name "cock-tier") (vers "0.2.3") (deps (list (crate-dep (name "cursive") (req "^0.20.0") (default-features #t) (kind 0)))) (hash "11xq6ffy6phadhv8878aq6l1ww6knndxnd0n6zc2mry8di8wpn1s") (yanked #t)))

(define-public crate-cock-tier-1 (crate (name "cock-tier") (vers "1.0.0") (deps (list (crate-dep (name "cursive") (req "^0.20.0") (default-features #t) (kind 0)))) (hash "16v8wch44gwh0xf2ks5s7jl8xn3k85l2pn01s26jnjxl3izjvy1x") (yanked #t)))

(define-public crate-cock-tier-1 (crate (name "cock-tier") (vers "1.0.1") (deps (list (crate-dep (name "cursive") (req "^0.20.0") (default-features #t) (kind 0)))) (hash "10c7qhgkb77pjlyllli4kj5j4yk7kajfyg0829mkmdda9mi35s7f")))

(define-public crate-cock-tui-0.8 (crate (name "cock-tui") (vers "0.8.0") (deps (list (crate-dep (name "cock-lib") (req "^1.0.0") (default-features #t) (kind 0)) (crate-dep (name "cursive") (req "^0.20.0") (default-features #t) (kind 0)))) (hash "00h8162fb4d0yx8b7b6nialh9n66jfl8awkwsadjxjllvq8czkz4")))

(define-public crate-cock-tui-0.8 (crate (name "cock-tui") (vers "0.8.1") (deps (list (crate-dep (name "cock-lib") (req "^1.0.1") (default-features #t) (kind 0)) (crate-dep (name "cursive") (req "^0.20.0") (default-features #t) (kind 0)))) (hash "0c0ly366bpyvc0sxil3h8lcrgzphx8qbfa0v8ga3icahh28176fh")))

(define-public crate-cock-tui-1 (crate (name "cock-tui") (vers "1.0.0") (deps (list (crate-dep (name "cock-lib") (req "^2.1.0") (default-features #t) (kind 0)) (crate-dep (name "cursive") (req "^0.20.0") (default-features #t) (kind 0)))) (hash "0dj4gm4kmx935bni4cg1qhigndb5vv5cqp25gs83fy3s216lcyaz")))

(define-public crate-cock-web-0.1 (crate (name "cock-web") (vers "0.1.1") (deps (list (crate-dep (name "cock-lib") (req "^1.0.1") (default-features #t) (kind 0)))) (hash "1ajrg1xx8w3c7xgpya6dnghg2ri5dsir2n99v0mgs2qk4lxiv03y") (yanked #t)))

(define-public crate-cock-web-0.5 (crate (name "cock-web") (vers "0.5.0") (deps (list (crate-dep (name "hyper") (req "^0.14") (features (quote ("full"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.0") (features (quote ("full"))) (default-features #t) (kind 0)) (crate-dep (name "toml") (req "^0.7.4") (default-features #t) (kind 0)))) (hash "1z9rnf0lg0x10x2ggwrp54firmfixnf944bn4fjj1a451k2r5gy3") (yanked #t)))

(define-public crate-cock-web-0.7 (crate (name "cock-web") (vers "0.7.0") (deps (list (crate-dep (name "hyper") (req "^0.14") (features (quote ("full"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.0") (features (quote ("full"))) (default-features #t) (kind 0)) (crate-dep (name "toml") (req "^0.7.4") (default-features #t) (kind 0)))) (hash "1d4g42570zcvxwimfl1mr6l0k6fgc0lxrm3kf0sqy109pq4i5yp1") (yanked #t)))

(define-public crate-cocklock-0.1 (crate (name "cocklock") (vers "0.1.0") (deps (list (crate-dep (name "native-tls") (req "^0.2.10") (default-features #t) (kind 0)) (crate-dep (name "postgres") (req "^0.19") (features (quote ("with-uuid-1"))) (default-features #t) (kind 0)) (crate-dep (name "postgres-native-tls") (req "^0.5.0") (default-features #t) (kind 0)) (crate-dep (name "testcontainers") (req "^0.14.0") (default-features #t) (kind 2)) (crate-dep (name "tokio-postgres") (req "^0.7.6") (default-features #t) (kind 0)) (crate-dep (name "uuid") (req "^1.0.0") (features (quote ("v4" "fast-rng"))) (default-features #t) (kind 0)))) (hash "0i2s2fdbqn4s476pmmxdh22dackqgs88j15482pmqprdff0z836z")))

(define-public crate-cocks-0.0.0 (crate (name "cocks") (vers "0.0.0") (deps (list (crate-dep (name "num-bigint") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "num-primes") (req "^0.2") (default-features #t) (kind 0)))) (hash "12n0nmyimlkkl0rirvfc9rifmls9cvimh38q2jkz5frblxir96sh")))

(define-public crate-cocktail-1 (crate (name "cocktail") (vers "1.0.0") (deps (list (crate-dep (name "reqwest") (req "^0.11") (features (quote ("blocking"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.0") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0.0") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.0") (default-features #t) (kind 0)) (crate-dep (name "urlencoding") (req "^2.1.0") (default-features #t) (kind 0)))) (hash "018x0qjz0m1yk28szcbqwc6a9syqsz76lyv37vqzh0yh0vwgr3fm")))

