(define-module (crates-io co e-) #:use-module (crates-io))

(define-public crate-coe-rs-0.1 (crate (name "coe-rs") (vers "0.1.0") (hash "08akd3ah5ijpbhlisan32946bizly652kj4bgwaklcymwfahc4kj")))

(define-public crate-coe-rs-0.1 (crate (name "coe-rs") (vers "0.1.1") (hash "0ic0wq1b160id7jc8fgdjw07hzg5sgc4946670dwivmd388zqs30")))

(define-public crate-coe-rs-0.1 (crate (name "coe-rs") (vers "0.1.2") (hash "16660pc135s339lxarggb0fkrbk99g00s7lb48qpdh222mj1x3vy")))

