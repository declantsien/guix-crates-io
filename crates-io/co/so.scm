(define-module (crates-io co so) #:use-module (crates-io))

(define-public crate-cosort-0.1 (crate (name "cosort") (vers "0.1.0") (deps (list (crate-dep (name "thiserror") (req "^1") (optional #t) (default-features #t) (kind 0)))) (hash "1j7xb2g3ljxnzbh6sdl24zckkcl229k41ayify9n7fjcyd2b5n1h") (features (quote (("default" "std-error")))) (v 2) (features2 (quote (("std-error" "dep:thiserror"))))))

