(define-module (crates-io co m_) #:use-module (crates-io))

(define-public crate-com_gd_rastizer_art-0.1 (crate (name "com_gd_rastizer_art") (vers "0.1.0") (hash "14b2b8vyhvyi6wiw3zy9yglfgsyr87ph88fwy9g9h7nmnkm2skm0")))

(define-public crate-com_goldsrc_formats-0.1 (crate (name "com_goldsrc_formats") (vers "0.1.0") (deps (list (crate-dep (name "binrw") (req "^0.12.0") (default-features #t) (kind 0)))) (hash "1al61mjabbihfjlsyws3pzbmfm92x12q53fbyf8h192dyq12a2r6")))

(define-public crate-com_logger-0.1 (crate (name "com_logger") (vers "0.1.0") (deps (list (crate-dep (name "log") (req "^0.4") (kind 0)) (crate-dep (name "uart_16550") (req "^0.2") (default-features #t) (kind 0)))) (hash "078sy6xb0fl2rd22s67wircx4x1sqh5w77dpm11pdqd0hm9my5pf") (features (quote (("readme") ("default"))))))

(define-public crate-com_logger-0.1 (crate (name "com_logger") (vers "0.1.1") (deps (list (crate-dep (name "log") (req "^0.4") (kind 0)) (crate-dep (name "uart_16550") (req "^0.2") (default-features #t) (kind 0)))) (hash "1hh7zqz7k2hjbxbbq4ci61j16hx4csim2fxxkwycc3pp0zqm4p0d")))

(define-public crate-com_macros-0.1 (crate (name "com_macros") (vers "0.1.0") (deps (list (crate-dep (name "com_macros_support") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1.0.1") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.5") (features (quote ("full"))) (default-features #t) (kind 0)) (crate-dep (name "trybuild") (req "^1.0.13") (default-features #t) (kind 2)))) (hash "1k2ax1v1bgsvgfgyb1ni78zr7dh17ramd34y99jyxfw07vmlfgag")))

(define-public crate-com_macros-0.2 (crate (name "com_macros") (vers "0.2.0") (deps (list (crate-dep (name "com_macros_support") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1.0.1") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.5") (features (quote ("full"))) (default-features #t) (kind 0)) (crate-dep (name "trybuild") (req "^1.0.13") (default-features #t) (kind 2)))) (hash "1hghsfldbra8l7ivysldpahb5g0fhsw5704yr3f8v9py89cb01kn")))

(define-public crate-com_macros-0.3 (crate (name "com_macros") (vers "0.3.0") (deps (list (crate-dep (name "com_macros_support") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "055ba3w6czasxmsd6rv5shva2v6gkznwpw8bn9dmaax5za4qyf3n")))

(define-public crate-com_macros-0.4 (crate (name "com_macros") (vers "0.4.0") (deps (list (crate-dep (name "com_macros_support") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "0wxc3qn5jy1w8dafzihxsiqy334rhwacjd6vk3fdqwznmcf3z55q")))

(define-public crate-com_macros-0.5 (crate (name "com_macros") (vers "0.5.0") (deps (list (crate-dep (name "com_macros_support") (req "^0.5") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "09r3ia69pxpjb402hb8wsp4smszqyv3v926f2v28c3yl8z7zjaj4")))

(define-public crate-com_macros-0.6 (crate (name "com_macros") (vers "0.6.0") (deps (list (crate-dep (name "com_macros_support") (req "^0.6") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "198k9fqd9rnpv3x6pxav6g636gl6m30iyqx63r4cfs56h0sqhxfk")))

(define-public crate-com_macros_support-0.1 (crate (name "com_macros_support") (vers "0.1.0") (deps (list (crate-dep (name "proc-macro2") (req "^1.0.1") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.5") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "12i0vdxhrp9fvll2i81dqyr5hzd770yljbn8wy5c1ln5pgh03vfy")))

(define-public crate-com_macros_support-0.2 (crate (name "com_macros_support") (vers "0.2.0") (deps (list (crate-dep (name "proc-macro2") (req "^1.0.1") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.5") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "1nh7w4920k5vbz2qhz59ymm5rrhnjiz5sics60787j2a1z9adscp")))

(define-public crate-com_macros_support-0.3 (crate (name "com_macros_support") (vers "0.3.0") (deps (list (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (features (quote ("full" "extra-traits"))) (default-features #t) (kind 0)))) (hash "1cimajn746d4b70vzfgnyrpfb7ncbhsj27pi9yq1rmf548isk5as")))

(define-public crate-com_macros_support-0.4 (crate (name "com_macros_support") (vers "0.4.0") (deps (list (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (features (quote ("full" "extra-traits"))) (default-features #t) (kind 0)))) (hash "0cnr29jvfs0yy7grpnspgyc8w5l18jd2n4dycw2p118a6vs637iq")))

(define-public crate-com_macros_support-0.5 (crate (name "com_macros_support") (vers "0.5.0") (deps (list (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (features (quote ("full" "extra-traits"))) (default-features #t) (kind 0)))) (hash "0wn16cdyx9ijn6ylg0kwia8p1mnfxhfz0b59wg400p40ra3ia05p")))

(define-public crate-com_macros_support-0.6 (crate (name "com_macros_support") (vers "0.6.0") (deps (list (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (features (quote ("full" "extra-traits"))) (default-features #t) (kind 0)))) (hash "033ix2k6j0930b0gpm77r2zc2d4f5fvpqbbr8ib6sad9hw89m2dd")))

(define-public crate-com_ptr-0.1 (crate (name "com_ptr") (vers "0.1.0") (deps (list (crate-dep (name "winapi") (req "^0.3.7") (features (quote ("combaseapi" "unknwnbase"))) (default-features #t) (target "cfg(not(test))") (kind 0)) (crate-dep (name "winapi") (req "^0.3.7") (features (quote ("combaseapi" "unknwnbase" "objbase" "wincodec"))) (default-features #t) (target "cfg(test)") (kind 0)))) (hash "1m3by6r4v59rva3aa4v1y3l9678q6k7ghpiczs3cvwm8kyrcc1r3")))

(define-public crate-com_ptr-0.1 (crate (name "com_ptr") (vers "0.1.1") (deps (list (crate-dep (name "winapi") (req "^0.3.7") (features (quote ("combaseapi" "unknwnbase"))) (default-features #t) (target "cfg(not(test))") (kind 0)) (crate-dep (name "winapi") (req "^0.3.7") (features (quote ("combaseapi" "unknwnbase" "objbase" "wincodec"))) (default-features #t) (target "cfg(test)") (kind 0)))) (hash "031pr3p6yg3w85fqbpl52vhd87f4ws8fgs5b7bf4hm273pfznnp7")))

(define-public crate-com_ptr-0.1 (crate (name "com_ptr") (vers "0.1.2") (deps (list (crate-dep (name "winapi") (req "^0.3.7") (features (quote ("combaseapi" "unknwnbase"))) (default-features #t) (target "cfg(not(test))") (kind 0)) (crate-dep (name "winapi") (req "^0.3.7") (features (quote ("combaseapi" "unknwnbase" "objbase" "wincodec"))) (default-features #t) (target "cfg(test)") (kind 0)))) (hash "1qyw4y6gchpdav4bmy61qxq609h6cyhqwqrzb601hbhjcmzvhi7w")))

(define-public crate-com_ptr-0.1 (crate (name "com_ptr") (vers "0.1.3") (deps (list (crate-dep (name "winapi") (req "^0.3.8") (features (quote ("combaseapi" "unknwnbase"))) (default-features #t) (target "cfg(not(test))") (kind 0)) (crate-dep (name "winapi") (req "^0.3.8") (features (quote ("combaseapi" "unknwnbase" "objbase" "wincodec"))) (default-features #t) (target "cfg(test)") (kind 0)))) (hash "0bfpd629f9a2bbmq3xhbbcc4zy6gj5pyqa0xzjrrldk2gpni0yfd")))

(define-public crate-com_ptr-0.2 (crate (name "com_ptr") (vers "0.2.0") (deps (list (crate-dep (name "anyhow") (req "^1.0.38") (default-features #t) (kind 2)) (crate-dep (name "winapi") (req "^0.3.9") (features (quote ("combaseapi" "unknwnbase" "winbase"))) (default-features #t) (kind 0)) (crate-dep (name "winapi") (req "^0.3.9") (features (quote ("combaseapi" "unknwnbase" "objbase" "wincodec" "winbase"))) (default-features #t) (kind 2)))) (hash "096rcql4dzpblkpw14mn8950ijix00i1s3wsf269l6a850dqk336")))

(define-public crate-com_ptr-0.2 (crate (name "com_ptr") (vers "0.2.1") (deps (list (crate-dep (name "anyhow") (req "^1.0.38") (default-features #t) (kind 2)) (crate-dep (name "winapi") (req "^0.3.9") (features (quote ("combaseapi" "unknwnbase" "winbase"))) (default-features #t) (kind 0)) (crate-dep (name "winapi") (req "^0.3.9") (features (quote ("combaseapi" "unknwnbase" "objbase" "wincodec" "winbase"))) (default-features #t) (kind 2)))) (hash "0rpfk23lnn905z7gc6vdi12kbb0yzc9yk2q7187vgan3mqgw4h81")))

