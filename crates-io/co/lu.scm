(define-module (crates-io co lu) #:use-module (crates-io))

(define-public crate-column-0.0.1 (crate (name "column") (vers "0.0.1") (deps (list (crate-dep (name "column_derive") (req "^0.0.1") (default-features #t) (kind 0)))) (hash "14w8xx717mb1vgk9ds9gjf9qh7dmvcngbivhnc3d1f7mhw09rv2g") (features (quote (("verbose" "column_derive/verbose") ("default"))))))

(define-public crate-column_derive-0.0.1 (crate (name "column_derive") (vers "0.0.1") (deps (list (crate-dep (name "quote") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "rustfmt") (req "^0.9") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^0.11") (default-features #t) (kind 0)))) (hash "1z3097ds45w6kd65giwdanpsisjxfc1g4pg4yq1lpdsrxy3xv1nk") (features (quote (("verbose" "rustfmt") ("default"))))))

(define-public crate-column_store-0.1 (crate (name "column_store") (vers "0.1.0") (deps (list (crate-dep (name "column_store_proc_macros") (req "^0") (default-features #t) (kind 0)))) (hash "1ick274mgqzgf69510z5bms2g14d1smkykbhgsm6f7a2mvi3dj4b")))

(define-public crate-column_store_proc_macros-0.1 (crate (name "column_store_proc_macros") (vers "0.1.0") (hash "0fadg5290nksxwmrq1b2r1sc46bz3g1f9r2xkzc5hn9aajilrdcc")))

(define-public crate-columnar-0.0.2 (crate (name "columnar") (vers "0.0.2") (hash "1i1c2pmsdvppx46bg8xv7avyyp6rhjd8qbj5q49q2yqn1j35n91q")))

(define-public crate-columnar-0.0.3 (crate (name "columnar") (vers "0.0.3") (deps (list (crate-dep (name "time") (req "^0.1.3") (default-features #t) (kind 0)))) (hash "1js9jymhgl3f0i5xjwfzqlwjdqjsf5c8sdbz07y5n7anvl0s9r5m")))

(define-public crate-columnar-0.0.4 (crate (name "columnar") (vers "0.0.4") (deps (list (crate-dep (name "time") (req "^0.1.3") (default-features #t) (kind 0)))) (hash "02mgvx03pyph6vv6rxgbdjidc0rzv5q549q6y60q6ls3x1aa4jhb")))

(define-public crate-columnar-0.0.5 (crate (name "columnar") (vers "0.0.5") (hash "15qgyd31r922rkir7plawm6y0wf1xi62bzjw9didf2yp12g4lj24")))

(define-public crate-columnar-0.0.6 (crate (name "columnar") (vers "0.0.6") (hash "18pmj97p351nk0b8xk1cbmcrzhjkv4nsdsamz2s5sxb69hfc4p4w")))

(define-public crate-columnar-0.0.7 (crate (name "columnar") (vers "0.0.7") (hash "0yfbywyfqkaar7lh67lq8awiwsypn8qq9j08q1v2barccclr3xkr")))

(define-public crate-columnar-0.0.8 (crate (name "columnar") (vers "0.0.8") (hash "1m0ixgw3fagrbx7c38m9z6psgqg5qqc813f0vchiy0dh0lyr5mxr")))

(define-public crate-columnar-0.0.9 (crate (name "columnar") (vers "0.0.9") (hash "14cgmqbgfnnxkgaykyd3b6gxp01k0ccgxp6i73fyk97fzpv0yzml")))

(define-public crate-columnar-0.0.10 (crate (name "columnar") (vers "0.0.10") (hash "0z9025kc3f6b4sy1wflbgxrdqn9kb1gqwvyhdisa1gsq5pbdq2ir")))

(define-public crate-columnar-0.0.11 (crate (name "columnar") (vers "0.0.11") (hash "0c26xyfha26619qyw1yn23qklkdr1x450pglhga0xa9sb85v0l64")))

(define-public crate-columnar-0.0.12 (crate (name "columnar") (vers "0.0.12") (hash "0mpy7khsn2iif1w9z17q3gn12yhhf3c2a84c533kdqp2nzb3kys0")))

(define-public crate-columnar-0.0.13 (crate (name "columnar") (vers "0.0.13") (hash "1wi5d1ilphl2v5ix7v4iaf5fhy62b20953p8y585sgw1a8ack5va")))

(define-public crate-columnar-0.0.14 (crate (name "columnar") (vers "0.0.14") (deps (list (crate-dep (name "byteorder") (req "*") (default-features #t) (kind 0)))) (hash "1k5plm4jmpj8a1pn6nlg3dm5a3y55d5rchfp07db5lx123i2j4ll")))

(define-public crate-columnar-0.0.15 (crate (name "columnar") (vers "0.0.15") (deps (list (crate-dep (name "byteorder") (req "^0.3.5") (default-features #t) (kind 0)))) (hash "1wbzbfxmsha5789gqhgqkls2ilcnmz7nbjpiqvdaywyaxfj73mdn")))

(define-public crate-columnar-0.0.16 (crate (name "columnar") (vers "0.0.16") (deps (list (crate-dep (name "byteorder") (req "^0.3.5") (default-features #t) (kind 0)))) (hash "1ph4x5xpf9aaa5yp3451bmq6ww6bb7sgnq8fk2ajar4q4rsdjari")))

(define-public crate-columnar-0.0.17 (crate (name "columnar") (vers "0.0.17") (deps (list (crate-dep (name "byteorder") (req "^0.3.5") (default-features #t) (kind 0)))) (hash "1fkkzy5n40s864q0m9pyj3gnqhs6f0bw5c6ba06ycgmjvfbnf8l7")))

(define-public crate-columnar-0.0.18 (crate (name "columnar") (vers "0.0.18") (deps (list (crate-dep (name "byteorder") (req "*") (default-features #t) (kind 0)))) (hash "0jhrilrv0w8wcrd1mwysj7zh0iq0ngyiwr594xnl96fpca27apnl")))

(define-public crate-columnar-0.0.19 (crate (name "columnar") (vers "0.0.19") (deps (list (crate-dep (name "byteorder") (req "0.4.*") (default-features #t) (kind 0)))) (hash "1f5f77w2q07h4ilpla7bgab2kw7my5mlhrzpb6ksrc8yp3vjabi2")))

(define-public crate-columnar_derive-0.0.1 (crate (name "columnar_derive") (vers "0.0.1") (deps (list (crate-dep (name "quote") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "rustfmt") (req "^0.9") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^0.11") (default-features #t) (kind 0)))) (hash "1j4lj0lbgpc4lx15dd4n1zhsbrfanmlh1v8f62k8rqm2kvylcl6v") (features (quote (("verbose" "rustfmt") ("default")))) (yanked #t)))

(define-public crate-columnation-master-0.1 (crate (name "columnation-master") (vers "0.1.0-dev.1") (deps (list (crate-dep (name "paste") (req "^1.0.6") (default-features #t) (kind 0)))) (hash "1ah73rkwiji8jh2a3aksd0id59rk17f5pxscl4x6ag1rrjya0jgp")))

(define-public crate-columnq-0.1 (crate (name "columnq") (vers "0.1.0") (hash "14ac7vw141c6xl9asj877vnah464lp9nm920xg91pcibg4ldw6wl")))

(define-public crate-columns-0.1 (crate (name "columns") (vers "0.1.0") (hash "105bmrpyymr1vxyas6ym38d210r2zkzsrrh20n39676xhf7qy4a2")))

(define-public crate-columnstore-0.0.0 (crate (name "columnstore") (vers "0.0.0") (hash "149hqj8h718d4dijgx469d73gz3hr67a4ipsavfrxwcabqh2zi7q")))

