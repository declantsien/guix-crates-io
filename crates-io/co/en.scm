(define-module (crates-io co en) #:use-module (crates-io))

(define-public crate-coen-0.1 (crate (name "coen") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^4.1.4") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "indexmap") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)))) (hash "1p6wpia1l6l1da4akhmxh13g76q2dc3wbklscdp70aphhyaqis4z")))

(define-public crate-coen-0.1 (crate (name "coen") (vers "0.1.1") (deps (list (crate-dep (name "clap") (req "^4.1.4") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "indexmap") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)))) (hash "1n12jm5xvnxigd4da4bb0xyjyar7fb6wzwkrsz6x8v064nds288l")))

(define-public crate-coen-0.1 (crate (name "coen") (vers "0.1.2-alpha") (deps (list (crate-dep (name "clap") (req "^4.1.4") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "indexmap") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)))) (hash "1dzkhmg8i8prjyd9rsdgb4h9dv84dqb069zq5xi9r2ddfydvxf46")))

(define-public crate-coenobita-0.1 (crate (name "coenobita") (vers "0.1.0") (hash "03g4yhsn9dg8z6fpvg3pi7k6yz9cgim3na7ndgayy2pry8pw9lxk")))

