(define-module (crates-io co _s) #:use-module (crates-io))

(define-public crate-co_sort-0.1 (crate (name "co_sort") (vers "0.1.0") (deps (list (crate-dep (name "criterion") (req "^0.2.10") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.6.5") (default-features #t) (kind 2)))) (hash "14y26v3pcmyrnk6gvbij5bkn9q6gikrbrjv6j7m459cf7z934dc9")))

(define-public crate-co_sort-0.1 (crate (name "co_sort") (vers "0.1.1") (deps (list (crate-dep (name "criterion") (req "^0.2.10") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.6.5") (default-features #t) (kind 2)))) (hash "08phjln7c990xsih9rzk091kidrpl8p0fg4jqadrswflzschv336")))

(define-public crate-co_sort-0.1 (crate (name "co_sort") (vers "0.1.2") (deps (list (crate-dep (name "criterion") (req "^0.2.10") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.6.5") (default-features #t) (kind 2)))) (hash "1k51df6rh724ivisv0wc60lwhhryxinjw9gs1np5j9rf2wp23956")))

(define-public crate-co_sort-0.2 (crate (name "co_sort") (vers "0.2.0") (deps (list (crate-dep (name "criterion") (req "^0.2.10") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.6.5") (default-features #t) (kind 2)))) (hash "1x6wmf4ayyp3pkn6dw15gki68wsvs8qpc3423cgbm57dbl8qxhaw")))

