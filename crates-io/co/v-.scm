(define-module (crates-io co v-) #:use-module (crates-io))

(define-public crate-cov-mark-1 (crate (name "cov-mark") (vers "1.0.0") (hash "0ghfkxzmm2cwb1215m6f7815mnk2sd9ynjavlb7pikm7h410myg6")))

(define-public crate-cov-mark-1 (crate (name "cov-mark") (vers "1.1.0") (hash "1wv75ylrai556m388a40d50fxiyacmvm6qqz6va6qf1q04z3vylz") (features (quote (("thread-local"))))))

(define-public crate-cov-mark-2 (crate (name "cov-mark") (vers "2.0.0-pre.1") (hash "0jj4yz70k31ax1n3s7iyjv1k5yzrm4hkibrazqciycyrdgvxhj0d") (features (quote (("enable") ("default" "enable"))))))

