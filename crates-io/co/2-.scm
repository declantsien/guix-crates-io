(define-module (crates-io co #{2-}#) #:use-module (crates-io))

(define-public crate-co2-mini-monitor-0.1 (crate (name "co2-mini-monitor") (vers "0.1.1") (deps (list (crate-dep (name "anyhow") (req "^1.0.38") (default-features #t) (kind 0)) (crate-dep (name "chrono") (req "^0.4.19") (default-features #t) (kind 0)) (crate-dep (name "hidapi") (req "^1.2.5") (default-features #t) (kind 0)) (crate-dep (name "json") (req "^0.12.4") (default-features #t) (kind 0)))) (hash "1fmxkj04pw879k0va87s6cg09kf7v21lb8qnimdaaawk08qbdiri")))

(define-public crate-co2-mini-monitor-0.1 (crate (name "co2-mini-monitor") (vers "0.1.2") (deps (list (crate-dep (name "anyhow") (req "^1.0.38") (default-features #t) (kind 0)) (crate-dep (name "chrono") (req "^0.4.19") (default-features #t) (kind 0)) (crate-dep (name "hidapi") (req "^1.2.5") (default-features #t) (kind 0)) (crate-dep (name "json") (req "^0.12.4") (default-features #t) (kind 0)))) (hash "1y8lw2d5mksb61m4pxmx069z3l34l9jxkj29fanyfakxy0z9q7wc")))

(define-public crate-co2-mini-monitor-0.1 (crate (name "co2-mini-monitor") (vers "0.1.3") (deps (list (crate-dep (name "anyhow") (req "^1.0.38") (default-features #t) (kind 0)) (crate-dep (name "chrono") (req "^0.4.19") (default-features #t) (kind 0)) (crate-dep (name "hidapi") (req "^1.2.5") (default-features #t) (kind 0)) (crate-dep (name "json") (req "^0.12.4") (default-features #t) (kind 0)))) (hash "02lhywg4bk41lm473v3frcay1gcq9l53kcwsxd3rdqwrpabk9kss")))

