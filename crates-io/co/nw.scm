(define-module (crates-io co nw) #:use-module (crates-io))

(define-public crate-conway-0.1 (crate (name "conway") (vers "0.1.2") (deps (list (crate-dep (name "rand") (req "^0.3") (default-features #t) (kind 0)))) (hash "09w3srk6rpnlpa3ab43a8d85q53fsqlsmd4jqm8591hicjxl27d8")))

(define-public crate-conway-0.1 (crate (name "conway") (vers "0.1.3") (deps (list (crate-dep (name "rand") (req "^0.3") (default-features #t) (kind 0)))) (hash "05kgfbkhzm9jggvvzxxkyc50wn0wq4v05cvq7659qfh5990l9m6r")))

(define-public crate-conway-0.1 (crate (name "conway") (vers "0.1.4") (deps (list (crate-dep (name "env_logger") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.3") (default-features #t) (kind 0)))) (hash "07zy0ly7wjhbgaakr7wqmnz0ggcy04qn2073r97wj5v8f353ssry")))

(define-public crate-conway-0.1 (crate (name "conway") (vers "0.1.5") (deps (list (crate-dep (name "env_logger") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.3") (default-features #t) (kind 0)))) (hash "17611h3wjcpk59n887b87jww0cwnmaa1i1n9v29m2dx1qvyid6k0")))

(define-public crate-conway-0.2 (crate (name "conway") (vers "0.2.0") (deps (list (crate-dep (name "env_logger") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.3") (default-features #t) (kind 0)))) (hash "0k2j8xjy6jaavdmrqr7i8fymvajcm41gxy71z26x6136a2l5c2l7")))

(define-public crate-conway-0.3 (crate (name "conway") (vers "0.3.0") (deps (list (crate-dep (name "custom_error") (req "^1.4") (default-features #t) (kind 0)) (crate-dep (name "env_logger") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.3") (default-features #t) (kind 0)))) (hash "1rqcg9rcjja586q8fd1irxvsa72q5qarb1sy2s9n6vh4das97b1i")))

(define-public crate-conway-nes-0.1 (crate (name "conway-nes") (vers "0.1.0") (deps (list (crate-dep (name "ines") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "mos6502_assembler") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "mos6502_model") (req "^0.1") (default-features #t) (kind 0)))) (hash "1nflg60r3sy7qi3gq4vmkik699a8v894pkfj59n0jpzfs935rrbp")))

(define-public crate-conway-nes-0.1 (crate (name "conway-nes") (vers "0.1.1") (deps (list (crate-dep (name "env_logger") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "ines") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "mos6502_assembler") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "mos6502_model") (req "^0.2") (default-features #t) (kind 0)))) (hash "16mpwhzn9gv5a7kidmm7m3l6x86cip2zxr1p066iqbjgarx7wx9l")))

(define-public crate-conway-rs-0.1 (crate (name "conway-rs") (vers "0.1.0") (deps (list (crate-dep (name "rand") (req "^0.3") (default-features #t) (kind 0)))) (hash "0hszxx12jpa7xwgz30fq4rvdnnyxim27rb0kbjzh293z3xlfdksc")))

(define-public crate-conway-rs-0.1 (crate (name "conway-rs") (vers "0.1.1") (deps (list (crate-dep (name "rand") (req "^0.3") (default-features #t) (kind 0)))) (hash "0lr21j599yxmq7z0c5lhwr2vn211xpw8agy8j8i9c4a47crvq6xs")))

(define-public crate-conway-rs-0.1 (crate (name "conway-rs") (vers "0.1.2") (deps (list (crate-dep (name "rand") (req "^0.3") (default-features #t) (kind 0)))) (hash "13859qjgmbri98ky42g6qgambiwvq49423a6hsxkvhjvj6s9z9l8")))

(define-public crate-conways-game-of-languages-0.1 (crate (name "conways-game-of-languages") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^4.5.4") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "crossterm") (req "^0.25.0") (default-features #t) (kind 0)) (crate-dep (name "ron") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "0p4bkfj5vrwpvr6q4dy3zpkk5r2864axs2vs2vdk1z3x4wkz3hrq")))

(define-public crate-conways-game-of-languages-0.2 (crate (name "conways-game-of-languages") (vers "0.2.0") (deps (list (crate-dep (name "clap") (req "^4.5.4") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "crossterm") (req "^0.25.0") (default-features #t) (kind 0)) (crate-dep (name "ron") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "0inh1rhzpmkkida5pm7s6r5zlm1fq356x0v9n1ysy5zi16n4pd7c")))

(define-public crate-conways_game_of_life_lib_rust-0.1 (crate (name "conways_game_of_life_lib_rust") (vers "0.1.0") (deps (list (crate-dep (name "criterion") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.8.5") (features (quote ("small_rng"))) (default-features #t) (kind 2)) (crate-dep (name "rand_pcg") (req "^0.3.1") (default-features #t) (kind 2)))) (hash "1r9j7bgkqmh0c0r9xzjny4pyzx4f633vvcvbrsvg4026rq4nksy5")))

(define-public crate-conways_game_of_life_lib_rust-0.1 (crate (name "conways_game_of_life_lib_rust") (vers "0.1.1") (deps (list (crate-dep (name "criterion") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.8.5") (features (quote ("small_rng"))) (default-features #t) (kind 2)) (crate-dep (name "rand_pcg") (req "^0.3.1") (default-features #t) (kind 2)))) (hash "159hzf5zh5f9viq6zjbsqhljnl1q03bwwj65rm9gjbyvfazzl75j")))

(define-public crate-conways_game_of_life_lib_rust-0.1 (crate (name "conways_game_of_life_lib_rust") (vers "0.1.2") (deps (list (crate-dep (name "criterion") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.8.5") (features (quote ("small_rng"))) (default-features #t) (kind 2)) (crate-dep (name "rand_pcg") (req "^0.3.1") (default-features #t) (kind 2)))) (hash "1pxikcrrmk0fc1y8jbx80xhszfgwh6kxz6mml88sf22fzgly2a5h")))

(define-public crate-conways_game_of_life_lib_rust-0.2 (crate (name "conways_game_of_life_lib_rust") (vers "0.2.0") (deps (list (crate-dep (name "criterion") (req "^0.4") (features (quote ("html_reports"))) (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.8.5") (features (quote ("small_rng"))) (default-features #t) (kind 2)) (crate-dep (name "rand_pcg") (req "^0.3.1") (default-features #t) (kind 2)))) (hash "06xg9snikji1cw76r6bnf192rv6innj8racrvpkipwx9dm0ygzhd")))

(define-public crate-conways_game_of_life_lib_rust-0.2 (crate (name "conways_game_of_life_lib_rust") (vers "0.2.1") (deps (list (crate-dep (name "criterion") (req "^0.4") (features (quote ("html_reports"))) (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.8.5") (features (quote ("small_rng"))) (default-features #t) (kind 2)) (crate-dep (name "rand_pcg") (req "^0.3.1") (default-features #t) (kind 2)))) (hash "0fdkpypi54i1k8pg4fw3vl75haaslj582q3nyk869bm3spjlh2jn")))

(define-public crate-conways_game_of_life_lib_rust-0.2 (crate (name "conways_game_of_life_lib_rust") (vers "0.2.2") (deps (list (crate-dep (name "criterion") (req "^0.4") (features (quote ("html_reports"))) (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.8.5") (features (quote ("small_rng"))) (default-features #t) (kind 2)) (crate-dep (name "rand_pcg") (req "^0.3.1") (default-features #t) (kind 2)))) (hash "0akkgyfggls18qmjr1vvnp5wdzl9nz4lx580ficjvdmb5hs74qqc")))

