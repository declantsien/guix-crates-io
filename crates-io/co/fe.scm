(define-module (crates-io co fe) #:use-module (crates-io))

(define-public crate-cofe-0.0.1 (crate (name "cofe") (vers "0.0.1") (hash "18l5j8al5z5nacrh6xsa2bgghp8mzh8d5ysnk3cdidiydvg1q1s3")))

(define-public crate-cofe-0.1 (crate (name "cofe") (vers "0.1.0") (hash "0qj9w25lipqcnql956clirghb4bpi2j40rbiscv2zln3692k7kjh")))

(define-public crate-cofe-0.1 (crate (name "cofe") (vers "0.1.1") (hash "0ci94a3frdj6d11y1ckcxrpdxral7a4h2qab8anvw3yb8mcpi8vh")))

