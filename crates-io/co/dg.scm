(define-module (crates-io co dg) #:use-module (crates-io))

(define-public crate-codgenhelp-0.0.1 (crate (name "codgenhelp") (vers "0.0.1") (deps (list (crate-dep (name "syn") (req "^0.10.5") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "0wvnv27jnk475szfhjx1lflcv125yv8x98993c708chvi5b7hr4w")))

