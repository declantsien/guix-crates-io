(define-module (crates-io co wr) #:use-module (crates-io))

(define-public crate-cowrc-0.0.1 (crate (name "cowrc") (vers "0.0.1") (hash "1rmx9fnmbd8ffj3zl5l7dkcmgcafxjqw9hzzsr96vlgg19kicfsw")))

(define-public crate-cowrc-0.0.2 (crate (name "cowrc") (vers "0.0.2") (hash "170kp5a8rn4n871xz5xv72cqs1plb76f4i372slxgqvpwdam7nd2")))

(define-public crate-cowrc-0.0.3 (crate (name "cowrc") (vers "0.0.3") (hash "0p3xb6f4ici3xpid8ssnv8kc94zgrrv4szjbrvflm7n8rj6bpc6y")))

(define-public crate-cowrc-0.0.4 (crate (name "cowrc") (vers "0.0.4") (hash "0gyzw5xdhglyi008plmnczjivn15qlmn3f9p02f89nz21i65pqyf")))

(define-public crate-cowrie-0.1 (crate (name "cowrie") (vers "0.1.0") (deps (list (crate-dep (name "automato") (req "^0.3.0") (default-features #t) (kind 0)))) (hash "0pd8kz70q088fyd2vxwxgzwv87gm242vhf2skcm9gnpjn7wc0d9s")))

