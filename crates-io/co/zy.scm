(define-module (crates-io co zy) #:use-module (crates-io))

(define-public crate-cozy-chess-0.1 (crate (name "cozy-chess") (vers "0.1.0") (deps (list (crate-dep (name "cozy-chess-types") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "cozy-chess-types") (req "^0.1") (default-features #t) (kind 1)) (crate-dep (name "criterion") (req "^0.3") (default-features #t) (kind 2)))) (hash "0vr2gi4ldma9cr8bkc4y06is9zbviszqkgh02w4wrcvhqvavz922")))

(define-public crate-cozy-chess-0.1 (crate (name "cozy-chess") (vers "0.1.1") (deps (list (crate-dep (name "cozy-chess-types") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "cozy-chess-types") (req "^0.1") (default-features #t) (kind 1)) (crate-dep (name "criterion") (req "^0.3") (default-features #t) (kind 2)))) (hash "13pwng67yxa700jmlvri9nc8lzgajsy31xdr8rx09s7fadan7p1b")))

(define-public crate-cozy-chess-0.1 (crate (name "cozy-chess") (vers "0.1.2") (deps (list (crate-dep (name "cozy-chess-types") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "cozy-chess-types") (req "^0.1") (default-features #t) (kind 1)) (crate-dep (name "criterion") (req "^0.3") (default-features #t) (kind 2)))) (hash "00zpanzc4gis24xd5nzd54rkwjhgs2vg4f27z65vhj8ax3470i4q")))

(define-public crate-cozy-chess-0.1 (crate (name "cozy-chess") (vers "0.1.3") (deps (list (crate-dep (name "cozy-chess-types") (req "^0.1.2") (default-features #t) (kind 0)) (crate-dep (name "cozy-chess-types") (req "^0.1.2") (default-features #t) (kind 1)) (crate-dep (name "criterion") (req "^0.3") (default-features #t) (kind 2)))) (hash "09zmy1rzv2562n6gj517842zxiilzwgi2f6ypfwd3xxxzfg264i2")))

(define-public crate-cozy-chess-0.1 (crate (name "cozy-chess") (vers "0.1.4") (deps (list (crate-dep (name "cozy-chess-types") (req "^0.1.3") (default-features #t) (kind 0)) (crate-dep (name "cozy-chess-types") (req "^0.1.3") (default-features #t) (kind 1)) (crate-dep (name "criterion") (req "^0.3") (default-features #t) (kind 2)))) (hash "0yrmhzl4pqh3sjkvn5rq3681j9zhpy0j46kf6km075lyj1ys6hgw")))

(define-public crate-cozy-chess-0.2 (crate (name "cozy-chess") (vers "0.2.0") (deps (list (crate-dep (name "cozy-chess-types") (req "^0.1.4") (default-features #t) (kind 0)) (crate-dep (name "cozy-chess-types") (req "^0.1.4") (default-features #t) (kind 1)) (crate-dep (name "criterion") (req "^0.3") (default-features #t) (kind 2)))) (hash "0bw6hf6x150mqm95salnnp2ww9rv67n89nx168v4j6phra164cra")))

(define-public crate-cozy-chess-0.2 (crate (name "cozy-chess") (vers "0.2.1") (deps (list (crate-dep (name "cozy-chess-types") (req "^0.1.4") (default-features #t) (kind 0)) (crate-dep (name "cozy-chess-types") (req "^0.1.4") (default-features #t) (kind 1)) (crate-dep (name "criterion") (req "^0.3") (default-features #t) (kind 2)))) (hash "1mrb2qcsqgidrvqrv2ld4bdn6jmjbw4mbyz5fqzgdijp5w0z7mkj")))

(define-public crate-cozy-chess-0.2 (crate (name "cozy-chess") (vers "0.2.2") (deps (list (crate-dep (name "cozy-chess-types") (req "^0.1.4") (default-features #t) (kind 0)) (crate-dep (name "cozy-chess-types") (req "^0.1.4") (default-features #t) (kind 1)) (crate-dep (name "criterion") (req "^0.3") (default-features #t) (kind 2)))) (hash "0fbz5hvcqv2mmi6hgrr59n7pi6dx6p39q4j9bxv974f4wahgqk1d")))

(define-public crate-cozy-chess-0.3 (crate (name "cozy-chess") (vers "0.3.0") (deps (list (crate-dep (name "cozy-chess-types") (req "^0.2.1") (default-features #t) (kind 0)) (crate-dep (name "cozy-chess-types") (req "^0.2.1") (default-features #t) (kind 1)) (crate-dep (name "criterion") (req "^0.3.5") (default-features #t) (kind 2)))) (hash "0p5a2b6anvm6gy947cvhmagygf14nsj1q1mw6aandcx9rkp7fwmm") (features (quote (("std" "cozy-chess-types/std") ("pext" "cozy-chess-types/pext"))))))

(define-public crate-cozy-chess-0.3 (crate (name "cozy-chess") (vers "0.3.1") (deps (list (crate-dep (name "cozy-chess-types") (req "^0.2.1") (default-features #t) (kind 0)) (crate-dep (name "cozy-chess-types") (req "^0.2.1") (default-features #t) (kind 1)) (crate-dep (name "criterion") (req "^0.3.5") (default-features #t) (kind 2)))) (hash "1ffkw62jg29ryvgjzwc8bbbibp53ry9slqg381y57a5igvd777r5") (features (quote (("std" "cozy-chess-types/std") ("pext" "cozy-chess-types/pext"))))))

(define-public crate-cozy-chess-0.3 (crate (name "cozy-chess") (vers "0.3.2") (deps (list (crate-dep (name "cozy-chess-types") (req "^0.2.1") (default-features #t) (kind 0)) (crate-dep (name "cozy-chess-types") (req "^0.2.1") (default-features #t) (kind 1)) (crate-dep (name "criterion") (req "^0.3.5") (default-features #t) (kind 2)))) (hash "01m3pzw592aqybklfvz54hfjr8ac817yx08gmwch1cb6pv6jvaph") (features (quote (("std" "cozy-chess-types/std") ("pext" "cozy-chess-types/pext"))))))

(define-public crate-cozy-chess-0.3 (crate (name "cozy-chess") (vers "0.3.3") (deps (list (crate-dep (name "cozy-chess-types") (req "^0.2.1") (default-features #t) (kind 0)) (crate-dep (name "cozy-chess-types") (req "^0.2.1") (default-features #t) (kind 1)) (crate-dep (name "criterion") (req "^0.3.5") (default-features #t) (kind 2)))) (hash "1kq8aa34q8kixwrrsdc14mrxkd6mv76mf96w7rwflacr1fxxggfp") (features (quote (("std" "cozy-chess-types/std") ("pext" "cozy-chess-types/pext"))))))

(define-public crate-cozy-chess-0.3 (crate (name "cozy-chess") (vers "0.3.4") (deps (list (crate-dep (name "cozy-chess-types") (req "^0.2.2") (default-features #t) (kind 0)) (crate-dep (name "cozy-chess-types") (req "^0.2.2") (default-features #t) (kind 1)) (crate-dep (name "criterion") (req "^0.3.5") (default-features #t) (kind 2)))) (hash "0s7zmb45yk8xaj957sayrdhy16r17n5jrh17xs2lzz2n20v9l0i7") (features (quote (("std" "cozy-chess-types/std") ("pext" "cozy-chess-types/pext"))))))

(define-public crate-cozy-chess-types-0.1 (crate (name "cozy-chess-types") (vers "0.1.0") (hash "06iwy5fa18d2am52gwdxbwj009k5cycazk70q97pmf2rir5d24gx")))

(define-public crate-cozy-chess-types-0.1 (crate (name "cozy-chess-types") (vers "0.1.1") (hash "0gmcxi4b09h2427hjgwhqysn217wam16lpc5byzcwq1g3dy73f3m")))

(define-public crate-cozy-chess-types-0.1 (crate (name "cozy-chess-types") (vers "0.1.2") (hash "0aj87ghc5qvhylxh5y3zcswrv195gcabmfn8dmqjzn5hvyj131m2")))

(define-public crate-cozy-chess-types-0.1 (crate (name "cozy-chess-types") (vers "0.1.3") (hash "0w515dp2rjryi9f390j3n5w0y9fz7c2k7zs1hsxbqibzz21vgspq")))

(define-public crate-cozy-chess-types-0.1 (crate (name "cozy-chess-types") (vers "0.1.4") (hash "00xr3a1izarbcs8j23cmmbsxwdj8n7d3qjc2s5l5kv51ww44cz44")))

(define-public crate-cozy-chess-types-0.2 (crate (name "cozy-chess-types") (vers "0.2.0") (hash "1pv3zma2zsvaywvfn2bahkhb389651qq5z8kwhkm6iixas9by87m") (features (quote (("std") ("pext"))))))

(define-public crate-cozy-chess-types-0.2 (crate (name "cozy-chess-types") (vers "0.2.1") (hash "04hyg95iv7g7hls3xnpz833mj5m6r5rz8chp61fp1f2rcry5ajkg") (features (quote (("std") ("pext"))))))

(define-public crate-cozy-chess-types-0.2 (crate (name "cozy-chess-types") (vers "0.2.2") (hash "1safwz7b39x6gmp6a51afkpajqixs9d6d01chlssyiskfw24hsr7") (features (quote (("std") ("pext"))))))

