(define-module (crates-io co ni) #:use-module (crates-io))

(define-public crate-conifer-0.0.0 (crate (name "conifer") (vers "0.0.0") (deps (list (crate-dep (name "framebuffer") (req "^0.1") (default-features #t) (kind 0)))) (hash "0hyx531hz19bk5ma0biri9r1ql5v7wd50ak66hpvqsngk82i2cch")))

(define-public crate-conifer-0.1 (crate (name "conifer") (vers "0.1.0") (deps (list (crate-dep (name "env_logger") (req "^0.7") (default-features #t) (kind 2)) (crate-dep (name "evdev") (req "^0.10") (default-features #t) (kind 0)) (crate-dep (name "flume") (req "^0.7") (default-features #t) (kind 0)) (crate-dep (name "framebuffer") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "image") (req "^0.23") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0") (default-features #t) (kind 0)))) (hash "1diillqjbrsh4cc0ga91bhkds5yv72qqyz7r4pc3l0hwvanphz9c")))

(define-public crate-coniferous-0.1 (crate (name "coniferous") (vers "0.1.0") (hash "07wp7d0wan0i8rpihp442wir66bnvq308nz26g9frzy7rdp7vp1v")))

(define-public crate-coniferous-0.1 (crate (name "coniferous") (vers "0.1.1") (deps (list (crate-dep (name "rayon") (req "^1.0") (default-features #t) (kind 0)))) (hash "1q0rr07cfc7xfdwg2g9nc0nkr0cdkcw2cma663gd38hyq78vr7ba")))

(define-public crate-coniferous-0.2 (crate (name "coniferous") (vers "0.2.0") (deps (list (crate-dep (name "rand") (req "^0.5") (default-features #t) (kind 0)) (crate-dep (name "rayon") (req "^1.0") (default-features #t) (kind 0)))) (hash "1fkyyzcq6rg2nw98fp2lzb1xqmmabnfj02q9sfva2364vsmzqf46")))

(define-public crate-coniferous-0.2 (crate (name "coniferous") (vers "0.2.1") (deps (list (crate-dep (name "parsnip") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.5") (default-features #t) (kind 0)) (crate-dep (name "rayon") (req "^1.0") (default-features #t) (kind 0)))) (hash "0az6m79iaiz2j1ls2wqa66di7xgahgz2szfmmwp10q0dw9z29x86")))

(define-public crate-conio-0.1 (crate (name "conio") (vers "0.1.0") (hash "0s4snzqy9x9wi53l0f20g26c0nqh7m8jbmpi8pxz2icmy1l7g4vi")))

(define-public crate-conio-0.1 (crate (name "conio") (vers "0.1.1") (hash "17h7i8ja7fqgcd8zvqr3cysd4f9sm23sl3p5i64j91d2kwxx0sxj")))

(define-public crate-conio-0.1 (crate (name "conio") (vers "0.1.2") (hash "0wrwxsc9vxs1y9q4p5831ydmycf9xc1pski6zaclrpi5hyw55x9s")))

(define-public crate-conio_rs-0.1 (crate (name "conio_rs") (vers "0.1.0") (hash "1a12sz7p0c94mg8kqqp43h7c6nazil7cvnjc0a5zi34l92ay5jv4")))

(define-public crate-conio_rs-0.1 (crate (name "conio_rs") (vers "0.1.1") (hash "1ar1mpmb9nzcjy3zbvp92rd553ks56b0jwrxc95lfzrkicz5rl2m")))

(define-public crate-conio_rs-0.1 (crate (name "conio_rs") (vers "0.1.2") (hash "0lwvbajydr8dy109j9wz9r80d8zy5s3ymrpqbgd51iyf5gwxn17z")))

