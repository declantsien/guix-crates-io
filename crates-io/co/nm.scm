(define-module (crates-io co nm) #:use-module (crates-io))

(define-public crate-conman-0.1 (crate (name "conman") (vers "0.1.1") (hash "0ipx02mg03xx9bq6jsgmgisww6zz6px11rk84ryiw8x8v3gxqw2z")))

(define-public crate-conman-0.1 (crate (name "conman") (vers "0.1.2") (hash "0jaz0sw5wqpraz65pha0syk62kbm4yv7qyz5papfykaa0g90byr4")))

(define-public crate-conman-0.1 (crate (name "conman") (vers "0.1.3") (hash "0bbb1vqf01ax4aj991d22y447dmddkbjyqqv7r1rqcgmwbihgngx")))

(define-public crate-conman-0.1 (crate (name "conman") (vers "0.1.4") (hash "13j1a815cd0bzjhg3abfgwjr157lkih5m5i4wbg015139y5jq1c1")))

(define-public crate-conman-0.1 (crate (name "conman") (vers "0.1.5") (hash "10lgp229xcxln76p8xagw82z71bsxwmyqjy6dw3dfhpm0qfmrsz6")))

(define-public crate-conman-0.1 (crate (name "conman") (vers "0.1.6") (hash "0h3mqi3vv9hpmp20ggyakgy0bci889fcggfqbwy11kiz59g4zdvw")))

