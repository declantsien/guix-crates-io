(define-module (crates-io co lm) #:use-module (crates-io))

(define-public crate-colm-0.1 (crate (name "colm") (vers "0.1.0") (deps (list (crate-dep (name "aesni") (req "^0.2") (default-features #t) (kind 2)) (crate-dep (name "arrayref") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "generic-array") (req "^0.9") (default-features #t) (kind 2)) (crate-dep (name "subtle") (req "^0.3") (kind 0)))) (hash "17mvgk1wjfsi822wm44rwndh1fg9gyyw8c2gyyxmiql707xxiw72") (features (quote (("x32") ("x16") ("default" "x16"))))))

(define-public crate-colmac-0.1 (crate (name "colmac") (vers "0.1.0") (hash "01qwkr4kzxr6xqw5ls4f4vhmnzi1c8d986a3vpz0jnv1i42r8nim")))

(define-public crate-colmac-0.1 (crate (name "colmac") (vers "0.1.1") (hash "19dwcyawndxdfkzbn9zg7767gcipn312l5c9pvhdinzb45yf790x")))

(define-public crate-colmac-0.1 (crate (name "colmac") (vers "0.1.2") (hash "0f77a1cb646wvxil2a0miy51v84l3mjbg3pvj29jp7dk3kv0p8h8")))

