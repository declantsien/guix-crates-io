(define-module (crates-io co or) #:use-module (crates-io))

(define-public crate-coord-0.1 (crate (name "coord") (vers "0.1.0") (hash "0gkw9v9b3rxf63ifzflyk002zvav60cqjb1rn58rnqca10drznab")))

(define-public crate-coord-0.2 (crate (name "coord") (vers "0.2.0") (hash "1r8mjjyy3xilynw0gpzh76vh5gf6vgamzxazazlkdnpm43a7s8ys")))

(define-public crate-coord-0.3 (crate (name "coord") (vers "0.3.0") (deps (list (crate-dep (name "num") (req "^0.1") (default-features #t) (kind 0)))) (hash "0q1m3b8n3d2hm5niy6b4z3rqx63vq8fr78jgjjfz4b6199vs5s6c")))

(define-public crate-coord-0.4 (crate (name "coord") (vers "0.4.0") (deps (list (crate-dep (name "num") (req "^0.1") (default-features #t) (kind 0)))) (hash "0y97c46fynk31nkjigp7rvl2s69dsrckprijsk6lk2fvaizihz07") (features (quote (("large_defaults"))))))

(define-public crate-coord-0.5 (crate (name "coord") (vers "0.5.0") (deps (list (crate-dep (name "num") (req "^0.1") (default-features #t) (kind 0)))) (hash "0b6pk7q2r5awd195isqfz0rwdhf43w3phk2r17mvp894f260389m") (features (quote (("large_defaults"))))))

(define-public crate-coord-0.6 (crate (name "coord") (vers "0.6.0") (deps (list (crate-dep (name "num") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0") (default-features #t) (kind 0)))) (hash "0p6ggg539ghc3iy5lsscjbalsx1j5h0gwf6qwd9fsq0ymhanm3pq") (features (quote (("large_defaults"))))))

(define-public crate-coord-0.6 (crate (name "coord") (vers "0.6.1") (deps (list (crate-dep (name "num") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0") (default-features #t) (kind 0)))) (hash "0i17zn05690njv48dssdysfpdy9mi8xmc58zy46dhv6ds5sh0d60") (features (quote (("large_defaults"))))))

(define-public crate-coord-0.7 (crate (name "coord") (vers "0.7.0") (deps (list (crate-dep (name "num") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0") (optional #t) (default-features #t) (kind 0)))) (hash "0f53qf7l4f6vs2hsij4h6wgnx1hmsblvz1a54b8hc55xx2h41nj0") (features (quote (("serialize" "serde" "serde_derive") ("large_defaults"))))))

(define-public crate-coord-0.8 (crate (name "coord") (vers "0.8.0") (deps (list (crate-dep (name "num") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0") (optional #t) (default-features #t) (kind 0)))) (hash "07845nvvydy30fx08795g4ca9h3f6d7a3xw48x8ka80f34ij24h1") (features (quote (("serialize" "serde" "serde_derive") ("large_defaults"))))))

(define-public crate-coord-0.8 (crate (name "coord") (vers "0.8.1") (deps (list (crate-dep (name "num") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0") (optional #t) (default-features #t) (kind 0)))) (hash "1nc144jhyn7rdk0m640d4xqdjs2r23g36jxc8jx2j64xzk78kc72") (features (quote (("serialize" "serde" "serde_derive") ("large_defaults"))))))

(define-public crate-coord-0.9 (crate (name "coord") (vers "0.9.0") (deps (list (crate-dep (name "num") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0") (optional #t) (default-features #t) (kind 0)))) (hash "08favnp4hmhpfg7hmi4hqwl5m9xppqiaza7rl3l0xzjg30brlm41") (features (quote (("serialize" "serde" "serde_derive") ("large_defaults"))))))

(define-public crate-coord-0.9 (crate (name "coord") (vers "0.9.1") (deps (list (crate-dep (name "num") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0") (optional #t) (default-features #t) (kind 0)))) (hash "00b8a4614b4g9wdl6majikyc2dakdnlr6a5n2hcbfvvj67pgn6cj") (features (quote (("serialize" "serde" "serde_derive") ("large_defaults"))))))

(define-public crate-coord-0.10 (crate (name "coord") (vers "0.10.0") (deps (list (crate-dep (name "num") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0") (optional #t) (default-features #t) (kind 0)))) (hash "01bpkdmdg1z297a2qd3hnfwyw0kiwnffxx0yjjv1q6v60z3fsvp9") (features (quote (("serialize" "serde" "serde_derive") ("large_defaults"))))))

(define-public crate-coord-0.10 (crate (name "coord") (vers "0.10.1") (deps (list (crate-dep (name "num") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0") (optional #t) (default-features #t) (kind 0)))) (hash "1s274cxy0vq1nx20gw8pz47wm5sv7jh9sdqnfjvkkifmhhcgc9jj") (features (quote (("serialize" "serde" "serde_derive") ("large_defaults"))))))

(define-public crate-coord-0.11 (crate (name "coord") (vers "0.11.0") (deps (list (crate-dep (name "num") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0") (optional #t) (default-features #t) (kind 0)))) (hash "04v4s9ihzc3b6xsqi6d9jrglgaq6k13ahfcc6nx0nbb0s98jpc3w") (features (quote (("serialize" "serde" "serde_derive") ("large_defaults"))))))

(define-public crate-coord-0.11 (crate (name "coord") (vers "0.11.1") (deps (list (crate-dep (name "num") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0") (optional #t) (default-features #t) (kind 0)))) (hash "0a3y00blqg1pil2y0vhw4if3g1fpl6xa1j0m606gzv8y75gc1jsq") (features (quote (("serialize" "serde" "serde_derive") ("large_defaults"))))))

(define-public crate-coord-transform-0.1 (crate (name "coord-transform") (vers "0.1.0") (hash "1phrsj40hxqa8w2v87fvfckf6jvnyp2rag77b6hg03sd5i2pjr7h")))

(define-public crate-coord2d-0.1 (crate (name "coord2d") (vers "0.1.0") (hash "1awsr69pnz3zpbaf56hcmll8gh3616z9am78mf6xl1pzn1jsxp7d") (yanked #t)))

(define-public crate-coord2d-0.1 (crate (name "coord2d") (vers "0.1.2") (hash "1mhk7zksks38dcd3rh397x24ky9zk9zfvlfyr6m8f2blw7nnvd08") (yanked #t)))

(define-public crate-coord2d-0.1 (crate (name "coord2d") (vers "0.1.4") (hash "1cr4184ni9x5ard0v780qfq2xfkjrsbfzr6mmcc7a298mdx94mad") (yanked #t)))

(define-public crate-coord2d-0.1 (crate (name "coord2d") (vers "0.1.8") (hash "1hf60cl5y2rn7pgbrgnxijjbjd67aazmynr7xqx63m2hwd2yi80i") (yanked #t)))

(define-public crate-coord2d-0.1 (crate (name "coord2d") (vers "0.1.7") (hash "1321is1bi4vlyb81fgdfl6vd69k7ywpqw8sfvf69k7k7jj6ygd26") (yanked #t)))

(define-public crate-coord2d-0.1 (crate (name "coord2d") (vers "0.1.9") (hash "1d7kxwica6g121jjbq8sfv7z9dnz8kj08x0dc36i2s99bvfaxxbg")))

(define-public crate-coord2d-0.1 (crate (name "coord2d") (vers "0.1.10") (hash "1pf1783idnpla4dds79azg67hykijf534canyl5hfhh43nd8dgav") (yanked #t)))

(define-public crate-coord2d-0.1 (crate (name "coord2d") (vers "0.1.11") (hash "0dr28ndcgd8alwk66n8mvq7s8ysqxly7j2sbzhdgnraxhmdps845") (yanked #t)))

(define-public crate-coord2d-0.1 (crate (name "coord2d") (vers "0.1.12") (hash "1jvf2c93vh0rb99qpm3h72q28cpc5da730acjq615g54ygm3dx7g") (yanked #t)))

(define-public crate-coord2d-0.1 (crate (name "coord2d") (vers "0.1.13") (hash "1xq2k6kqbx4jla5b9pk15symp7hjrvpmyzs11kg2912b21lp1d6k") (yanked #t)))

(define-public crate-coord2d-0.1 (crate (name "coord2d") (vers "0.1.14") (hash "0inms69vcaabamhr119h8a0xdkfffs0670h5kzgjljpfihgm9w5d") (yanked #t)))

(define-public crate-coord2d-0.1 (crate (name "coord2d") (vers "0.1.15") (hash "0a1lj8njvhpsv9qpf92xhngk3l6z6azmz8hgn9x0s918bwbyswyp") (yanked #t)))

(define-public crate-coord2d-0.1 (crate (name "coord2d") (vers "0.1.16") (hash "1xw41kazcv0p3g9ylnysvpqbrrs7f31kr8hai4npw1crqp2x77z3") (yanked #t)))

(define-public crate-coord2d-0.1 (crate (name "coord2d") (vers "0.1.17") (hash "04ar4q2x6zhlbg6z7kc9kacgpwhaajkrx6qhdqyfz88sxwwbf8za") (yanked #t)))

(define-public crate-coord2d-0.1 (crate (name "coord2d") (vers "0.1.18") (hash "0pixbw9nnbim25xb7lzzc7wfy8cyh24wr7bhf0iyajiq98z8yqby") (yanked #t)))

(define-public crate-coord2d-0.1 (crate (name "coord2d") (vers "0.1.19") (hash "05z5h2wnmd5l9rvkjsb8z8hlp39wvbhailk69d9mgx7rvb4fb1yh") (yanked #t)))

(define-public crate-coord2d-0.2 (crate (name "coord2d") (vers "0.2.0") (hash "0jjynbrgx14x5ca5y08mfamqw34d147mn1rh5vhw2nc93qcygvc9") (yanked #t)))

(define-public crate-coord2d-0.2 (crate (name "coord2d") (vers "0.2.1") (hash "172bs6ghfrgj6ndn0vp19fvf7c6bzanyh9q48jqsxb6h6r8b135i") (yanked #t)))

(define-public crate-coord2d-0.2 (crate (name "coord2d") (vers "0.2.2") (hash "00818li22ldnldr353k4xsdg4h38kr1bhqbi9s1s2pyjrqnqld99") (yanked #t)))

(define-public crate-coord2d-0.2 (crate (name "coord2d") (vers "0.2.3") (hash "0791pnfaidynyjdzdmq5a8kz8gf10h5rg9kd2lxyz4nixp411jq3") (yanked #t)))

(define-public crate-coord2d-0.2 (crate (name "coord2d") (vers "0.2.4") (hash "1zdcwsihjsnmsgyzl6liwrdianb3zjbbavdzlrnnm4qcc27i9qfl") (yanked #t)))

(define-public crate-coord2d-0.2 (crate (name "coord2d") (vers "0.2.5") (hash "0mx0fham029j6ihh2f03kxgj3s7x5p2swaan8hpkzyhvqmrva2z6") (yanked #t)))

(define-public crate-coord2d-0.2 (crate (name "coord2d") (vers "0.2.6") (hash "0rlyvvz3k5ja43jsl37w6jhxmix3by6amgyrmnq25wrp4m6vlqf4") (yanked #t)))

(define-public crate-coord2d-0.2 (crate (name "coord2d") (vers "0.2.7") (hash "0mwrc72vfrc1l9kzdhlcfxsxciihchzq8z7i4h2h927hsswgbkbd") (yanked #t)))

(define-public crate-coord2d-0.2 (crate (name "coord2d") (vers "0.2.8") (hash "1zs8lzr62iv91fc36xr4lvksrjg6ivwjk19nlzzqbyqkxb1arr75") (yanked #t)))

(define-public crate-coord_2d-0.1 (crate (name "coord_2d") (vers "0.1.0") (deps (list (crate-dep (name "serde") (req "^1.0") (features (quote ("serde_derive"))) (optional #t) (default-features #t) (kind 0)))) (hash "08l6x2vgdb5qvnrfy4x6ajiggah01swm464zxis8wzm9vzyhnkcz")))

(define-public crate-coord_2d-0.2 (crate (name "coord_2d") (vers "0.2.0") (deps (list (crate-dep (name "serde") (req "^1.0") (features (quote ("serde_derive"))) (optional #t) (default-features #t) (kind 0)))) (hash "0lz753vb4wf1lzi0844mwhcv30f3fhbchj35inririkh82y5cqhx")))

(define-public crate-coord_2d-0.2 (crate (name "coord_2d") (vers "0.2.1") (deps (list (crate-dep (name "serde") (req "^1.0") (features (quote ("serde_derive"))) (optional #t) (default-features #t) (kind 0)))) (hash "0r4xcwr19kf8k3zmzp8icr027half54mr355axz65wyr0z3p8z88") (features (quote (("serialize" "serde"))))))

(define-public crate-coord_2d-0.2 (crate (name "coord_2d") (vers "0.2.2") (deps (list (crate-dep (name "serde") (req "^1.0") (features (quote ("serde_derive"))) (optional #t) (default-features #t) (kind 0)))) (hash "1wp6bziia9pnnhwbpik8hpha8rjhr87rnigcgksfb2ifx6p66a3z") (features (quote (("serialize" "serde"))))))

(define-public crate-coord_2d-0.2 (crate (name "coord_2d") (vers "0.2.3") (deps (list (crate-dep (name "serde") (req "^1.0") (features (quote ("serde_derive"))) (optional #t) (default-features #t) (kind 0)))) (hash "0vqi6nl9hlfjbycppa6vx673ir5jyz4mgvgb6kvsr22hn9djagza") (features (quote (("serialize" "serde"))))))

(define-public crate-coord_2d-0.2 (crate (name "coord_2d") (vers "0.2.4") (deps (list (crate-dep (name "serde") (req "^1.0") (features (quote ("serde_derive"))) (optional #t) (default-features #t) (kind 0)))) (hash "0jv38mjkw5mbzrl6b2b5i49ffkhs76fpc52d0a056s3qlgzisv0h") (features (quote (("serialize" "serde"))))))

(define-public crate-coord_2d-0.2 (crate (name "coord_2d") (vers "0.2.5") (deps (list (crate-dep (name "serde") (req "^1.0") (features (quote ("serde_derive"))) (optional #t) (default-features #t) (kind 0)))) (hash "012q8x05i00fi1d1jqqwp5wi3axq5jh6aj8q2g8f44vxlxh1h3z0") (features (quote (("serialize" "serde"))))))

(define-public crate-coord_2d-0.2 (crate (name "coord_2d") (vers "0.2.6") (deps (list (crate-dep (name "serde") (req "^1.0") (features (quote ("serde_derive"))) (optional #t) (default-features #t) (kind 0)))) (hash "0hhsnsmjfwh054m8kz8afcchiim9qiajwbdx3d2x6nk5i8phkm4z") (features (quote (("serialize" "serde"))))))

(define-public crate-coord_2d-0.2 (crate (name "coord_2d") (vers "0.2.7") (deps (list (crate-dep (name "serde") (req "^1.0") (features (quote ("serde_derive"))) (optional #t) (default-features #t) (kind 0)))) (hash "0wr0y66ihdp6q7amwd0ynj4ic8a2ibh3yd2g63i6d68yd5c9k0v9") (features (quote (("serialize" "serde"))))))

(define-public crate-coord_2d-0.2 (crate (name "coord_2d") (vers "0.2.8") (deps (list (crate-dep (name "serde") (req "^1.0") (features (quote ("serde_derive"))) (optional #t) (default-features #t) (kind 0)))) (hash "1k1bcf5fcvd13nq10his9lrz1avg411xk9cikb6gjc7h0c1w2r76") (features (quote (("serialize" "serde"))))))

(define-public crate-coord_2d-0.2 (crate (name "coord_2d") (vers "0.2.9") (deps (list (crate-dep (name "serde") (req "^1.0") (features (quote ("serde_derive"))) (optional #t) (default-features #t) (kind 0)))) (hash "16zc091hvpb0w1fblvad1zpx7mncgmwnlchx4b1ygh5xm4y4k6y3") (features (quote (("serialize" "serde"))))))

(define-public crate-coord_2d-0.2 (crate (name "coord_2d") (vers "0.2.10") (deps (list (crate-dep (name "serde") (req "^1.0") (features (quote ("serde_derive"))) (optional #t) (default-features #t) (kind 0)))) (hash "0fljjxfnh42kwl0if3f9vkdx8xsxvq2irp8sljygwpx7p3hwbvcj") (features (quote (("serialize" "serde"))))))

(define-public crate-coord_2d-0.2 (crate (name "coord_2d") (vers "0.2.11") (deps (list (crate-dep (name "serde") (req "^1.0") (features (quote ("serde_derive"))) (optional #t) (default-features #t) (kind 0)))) (hash "1ghjjqkjmyjh93sz1dgljyrcfqipxqwgabff101q4d25d14774pp") (features (quote (("serialize" "serde"))))))

(define-public crate-coord_2d-0.2 (crate (name "coord_2d") (vers "0.2.12") (deps (list (crate-dep (name "serde") (req "^1.0") (features (quote ("serde_derive"))) (optional #t) (default-features #t) (kind 0)))) (hash "0whn4xj8swvnkxgzxl3g36492b4vzir4q3n1n1059bynzlvp36pb") (features (quote (("serialize" "serde"))))))

(define-public crate-coord_2d-0.2 (crate (name "coord_2d") (vers "0.2.13") (deps (list (crate-dep (name "serde") (req "^1.0") (features (quote ("serde_derive"))) (optional #t) (default-features #t) (kind 0)))) (hash "1596dbl56siwrf66m1dqz4qj0wj70a0z022yzw7gdcxwjwr49kxh") (features (quote (("serialize" "serde"))))))

(define-public crate-coord_2d-0.2 (crate (name "coord_2d") (vers "0.2.14") (deps (list (crate-dep (name "rand") (req "^0.7") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("serde_derive"))) (optional #t) (default-features #t) (kind 0)))) (hash "1d20s9r4ra9jbr9k2zsr50j32512migsfcdhkkz46nhzqj0nqry8") (features (quote (("serialize" "serde"))))))

(define-public crate-coord_2d-0.2 (crate (name "coord_2d") (vers "0.2.15") (deps (list (crate-dep (name "rand") (req "^0.7") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("serde_derive"))) (optional #t) (default-features #t) (kind 0)))) (hash "0qjkj3ip7dixkvy32by34jkxa55dnnb4rxz05yd0wj53inc17zbl") (features (quote (("serialize" "serde"))))))

(define-public crate-coord_2d-0.2 (crate (name "coord_2d") (vers "0.2.16") (deps (list (crate-dep (name "rand") (req "^0.7") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("serde_derive"))) (optional #t) (default-features #t) (kind 0)))) (hash "0ycimzjgkwcrg4rcs1jydzyb0njddn2m49lqawi0i6785ijk2hnf") (features (quote (("serialize" "serde"))))))

(define-public crate-coord_2d-0.2 (crate (name "coord_2d") (vers "0.2.17") (deps (list (crate-dep (name "rand") (req "^0.7") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("serde_derive"))) (optional #t) (default-features #t) (kind 0)))) (hash "1nm3nmvv1zqvpyj958jwaz9b6m12y59syf7jplrbyfy3hfxz2dlc") (features (quote (("serialize" "serde"))))))

(define-public crate-coord_2d-0.2 (crate (name "coord_2d") (vers "0.2.18") (deps (list (crate-dep (name "rand") (req "^0.7") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("serde_derive"))) (optional #t) (default-features #t) (kind 0)))) (hash "1zakidyza5ziry32rd0ivlaxkm5fnpf2cl3hi60v2cp4czgb2w9a") (features (quote (("serialize" "serde"))))))

(define-public crate-coord_2d-0.2 (crate (name "coord_2d") (vers "0.2.19") (deps (list (crate-dep (name "rand") (req "^0.7") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("serde_derive"))) (optional #t) (default-features #t) (kind 0)))) (hash "16s1zljbigjxapjwnxdkm2x4yqpawks9xybnvag0cjp2160136ap") (features (quote (("serialize" "serde"))))))

(define-public crate-coord_2d-0.2 (crate (name "coord_2d") (vers "0.2.20") (deps (list (crate-dep (name "rand") (req "^0.7") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("serde_derive"))) (optional #t) (default-features #t) (kind 0)))) (hash "147sqdm863gwpl1m9352vh2p04z43sa62qrh9gmsx0d5fibjpmlm") (features (quote (("serialize" "serde"))))))

(define-public crate-coord_2d-0.2 (crate (name "coord_2d") (vers "0.2.21") (deps (list (crate-dep (name "rand") (req "^0.7") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("serde_derive"))) (optional #t) (default-features #t) (kind 0)))) (hash "0g80z85q074bjjypvirjdkp2k5kxbk742j71dw3xh544z7gi0dak") (features (quote (("serialize" "serde"))))))

(define-public crate-coord_2d-0.2 (crate (name "coord_2d") (vers "0.2.22") (deps (list (crate-dep (name "rand") (req "^0.7") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("serde_derive"))) (optional #t) (default-features #t) (kind 0)))) (hash "1lz6z1jjp3ymjxmgv548fhxjzpq0las25gp7ljdvmkla72jl6nlf") (features (quote (("serialize" "serde"))))))

(define-public crate-coord_2d-0.2 (crate (name "coord_2d") (vers "0.2.23") (deps (list (crate-dep (name "rand") (req "^0.8") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("serde_derive"))) (optional #t) (default-features #t) (kind 0)))) (hash "1cp67qwlzslmnwkik1lwia9sckgbi4rkiihij0hrn3wxbbn4jpgn") (features (quote (("serialize" "serde"))))))

(define-public crate-coord_2d-0.2 (crate (name "coord_2d") (vers "0.2.24") (deps (list (crate-dep (name "rand") (req "^0.8") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("serde_derive"))) (optional #t) (default-features #t) (kind 0)))) (hash "1s3r2qgl93inrlwpy0i48wdcyp6k7clb0qn2rykgx8x2jx7n47sw") (features (quote (("serialize" "serde"))))))

(define-public crate-coord_2d-0.2 (crate (name "coord_2d") (vers "0.2.25") (deps (list (crate-dep (name "rand") (req "^0.7") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("serde_derive"))) (optional #t) (default-features #t) (kind 0)))) (hash "0vkrsg5nslm2xs4ywhgqnx8yaibjq02606d3ndrv34jnxp2dma67") (features (quote (("serialize" "serde"))))))

(define-public crate-coord_2d-0.3 (crate (name "coord_2d") (vers "0.3.0") (deps (list (crate-dep (name "rand") (req "^0.8") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("serde_derive"))) (optional #t) (default-features #t) (kind 0)))) (hash "1f16g42g1a047spa98sajqfmh819axchs57lwa4grbz8fmrrmnll") (features (quote (("serialize" "serde"))))))

(define-public crate-coord_2d-0.3 (crate (name "coord_2d") (vers "0.3.1") (deps (list (crate-dep (name "rand") (req "^0.8") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("serde_derive"))) (optional #t) (default-features #t) (kind 0)))) (hash "1jgyg45m20jl06cmkn7hnd5y66m1dq2m2g18y8csn2yvr7jrzy87") (features (quote (("serialize" "serde"))))))

(define-public crate-coord_2d-0.3 (crate (name "coord_2d") (vers "0.3.2") (deps (list (crate-dep (name "rand") (req "^0.8") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("serde_derive"))) (optional #t) (default-features #t) (kind 0)))) (hash "04gc9jyw8y7y04329csacmqc63sqijijl2n852jlifnjrn7gqkwl") (features (quote (("serialize" "serde"))))))

(define-public crate-coord_2d-0.3 (crate (name "coord_2d") (vers "0.3.3") (deps (list (crate-dep (name "rand") (req "^0.8") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("serde_derive"))) (optional #t) (default-features #t) (kind 0)))) (hash "1lisxh9ghywsr0hb93nblsyzrf6qy919r2hycgskrj18gh180p9g") (features (quote (("serialize" "serde"))))))

(define-public crate-coord_2d-0.3 (crate (name "coord_2d") (vers "0.3.4") (deps (list (crate-dep (name "rand") (req "^0.8") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("serde_derive"))) (optional #t) (default-features #t) (kind 0)))) (hash "0dg2sg8f5s089jv0h2sz0yddasn6xvpah4fq7nwzwj227gp931q0") (features (quote (("std") ("serialize" "serde"))))))

(define-public crate-coord_2d-0.3 (crate (name "coord_2d") (vers "0.3.5") (deps (list (crate-dep (name "rand") (req "^0.8") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("serde_derive"))) (optional #t) (default-features #t) (kind 0)))) (hash "0pbmbp3x58hyqrbsy8d6qfd3c5fm44sp15x6y6zsy1zspaq82dj6") (features (quote (("std") ("serialize" "serde"))))))

(define-public crate-coord_2d-0.3 (crate (name "coord_2d") (vers "0.3.6") (deps (list (crate-dep (name "rand") (req "^0.8") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("serde_derive"))) (optional #t) (default-features #t) (kind 0)))) (hash "0mzczqaspljp31gnm04d7lz4s6g4q1q7bnq11f2zqhxzv0p0l0pi") (features (quote (("std") ("serialize" "serde"))))))

(define-public crate-coord_transforms-1 (crate (name "coord_transforms") (vers "1.0.0") (deps (list (crate-dep (name "assert_approx_eq") (req "^1.0.0") (default-features #t) (kind 0)) (crate-dep (name "nalgebra") (req "^0.13") (default-features #t) (kind 0)))) (hash "0wwsbnwncv4viy6sl9m7j9xmyvzllpy3g2s1f5xwigxm1pfycv10")))

(define-public crate-coord_transforms-1 (crate (name "coord_transforms") (vers "1.1.0") (deps (list (crate-dep (name "chrono") (req "^0.4.0") (default-features #t) (kind 2)) (crate-dep (name "float-cmp") (req "^0.2.5") (default-features #t) (kind 0)) (crate-dep (name "nalgebra") (req "^0.13") (default-features #t) (kind 0)) (crate-dep (name "rayon") (req "^0.8.2") (default-features #t) (kind 2)))) (hash "0lmc638hl78f3krchwqc5ls8ga6aljizi4lqailriq3afcg9yzcx")))

(define-public crate-coord_transforms-1 (crate (name "coord_transforms") (vers "1.2.0") (deps (list (crate-dep (name "chrono") (req "^0.4.0") (default-features #t) (kind 2)) (crate-dep (name "float-cmp") (req "^0.2.5") (default-features #t) (kind 0)) (crate-dep (name "minifb") (req "^0.10.3") (default-features #t) (kind 2)) (crate-dep (name "nalgebra") (req "^0.13") (default-features #t) (kind 0)) (crate-dep (name "rayon") (req "^0.8.2") (default-features #t) (kind 2)))) (hash "1pv2vz1h19phx32rsainnj6i0xyvjfc3ddd1iw6jp69v1n58476i")))

(define-public crate-coord_transforms-1 (crate (name "coord_transforms") (vers "1.2.1") (deps (list (crate-dep (name "chrono") (req "^0.4.0") (default-features #t) (kind 2)) (crate-dep (name "float-cmp") (req "^0.2.5") (default-features #t) (kind 0)) (crate-dep (name "minifb") (req "^0.10.3") (default-features #t) (kind 2)) (crate-dep (name "nalgebra") (req "^0.13") (default-features #t) (kind 0)) (crate-dep (name "rayon") (req "^0.8.2") (default-features #t) (kind 2)))) (hash "1600v7rpysbyn32g8b3qha9a4gs2ci5n75cnzbv2zrxr3wa08qab")))

(define-public crate-coord_transforms-1 (crate (name "coord_transforms") (vers "1.2.2") (deps (list (crate-dep (name "chrono") (req "^0.4.0") (default-features #t) (kind 2)) (crate-dep (name "float-cmp") (req "^0.2.5") (default-features #t) (kind 0)) (crate-dep (name "minifb") (req "^0.10.3") (default-features #t) (kind 2)) (crate-dep (name "nalgebra") (req "^0.16.8") (default-features #t) (kind 0)) (crate-dep (name "rayon") (req "^0.8.2") (default-features #t) (kind 2)))) (hash "01l8w4g1iimj068742qmvz9204bp1kl19dmk93kpsmw7x6s7gkam")))

(define-public crate-coord_transforms-1 (crate (name "coord_transforms") (vers "1.3.0") (deps (list (crate-dep (name "chrono") (req "^0.4.9") (default-features #t) (kind 2)) (crate-dep (name "float-cmp") (req "^0.5.2") (default-features #t) (kind 0)) (crate-dep (name "minifb") (req "^0.13.0") (default-features #t) (kind 2)) (crate-dep (name "nalgebra") (req "^0.21.1") (default-features #t) (kind 0)) (crate-dep (name "rayon") (req "^1.2.0") (default-features #t) (kind 2)))) (hash "06kjjix9ym09fwrcz95qgzii8mjx72qcq0asc5ryffqrfr5gcz4d")))

(define-public crate-coord_transforms-1 (crate (name "coord_transforms") (vers "1.3.1") (deps (list (crate-dep (name "chrono") (req "^0.4.9") (default-features #t) (kind 2)) (crate-dep (name "float-cmp") (req "^0.5.2") (default-features #t) (kind 0)) (crate-dep (name "minifb") (req "^0.13.0") (default-features #t) (kind 2)) (crate-dep (name "nalgebra") (req "^0.21.1") (default-features #t) (kind 0)) (crate-dep (name "rayon") (req "^1.2.0") (default-features #t) (kind 2)))) (hash "083cqz1d7znlvg2i1rlrmzpipkqpkvljzf1d0s9fqx1c4hxdsmb7")))

(define-public crate-coord_transforms-1 (crate (name "coord_transforms") (vers "1.4.0") (deps (list (crate-dep (name "chrono") (req "^0.4.19") (default-features #t) (kind 2)) (crate-dep (name "float-cmp") (req "^0.9.0") (default-features #t) (kind 0)) (crate-dep (name "minifb") (req "^0.23.0") (default-features #t) (kind 2)) (crate-dep (name "nalgebra") (req "^0.31.0") (default-features #t) (kind 0)) (crate-dep (name "rayon") (req "^1.5.3") (default-features #t) (kind 2)))) (hash "08gwzq0zarkayp8ksacbdibnily5k8ylpwrqhm6flxhv2q29wd7l")))

(define-public crate-coordgen-0.1 (crate (name "coordgen") (vers "0.1.0") (deps (list (crate-dep (name "cc") (req "^1.0.70") (default-features #t) (kind 1)) (crate-dep (name "cmake") (req "^0.1.45") (default-features #t) (kind 1)) (crate-dep (name "reqwest") (req "^0.11.4") (features (quote ("rustls-tls" "blocking" "json"))) (kind 2)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 2)))) (hash "12l85qqqm6yfxslsfl9kpf4rgipfk17qs0g15ich2zsjp4qlk783")))

(define-public crate-coordgen-0.2 (crate (name "coordgen") (vers "0.2.0") (deps (list (crate-dep (name "cmake") (req "^0.1.45") (default-features #t) (kind 1)) (crate-dep (name "reqwest") (req "^0.11.4") (features (quote ("rustls-tls" "blocking" "json"))) (kind 2)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 2)))) (hash "0x5zfgwrs6kwc9cra0sd78f6jyxc42jyg2gw3zclvrxi39ijl7bz")))

(define-public crate-coordgen-0.2 (crate (name "coordgen") (vers "0.2.1") (deps (list (crate-dep (name "cmake") (req "^0.1.45") (default-features #t) (kind 1)) (crate-dep (name "reqwest") (req "^0.11.4") (features (quote ("rustls-tls" "blocking" "json"))) (kind 2)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 2)))) (hash "0mjgm2n9iix0km0mjrnf1hzirbzkmc95rqh6r8mk8dfax4kwshs7")))

(define-public crate-coordgen-0.2 (crate (name "coordgen") (vers "0.2.2") (deps (list (crate-dep (name "cmake") (req "^0.1.45") (default-features #t) (kind 1)) (crate-dep (name "reqwest") (req "^0.11.4") (features (quote ("rustls-tls" "blocking" "json"))) (kind 2)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 2)))) (hash "12v49m4lgjzyp26vh05sf6bkn4spfq9mfhm3fssgf4zqg0zmcndg")))

(define-public crate-coordinate-transformer-1 (crate (name "coordinate-transformer") (vers "1.0.0") (hash "1bkycpzb1m9yb7x82wskd01fh21xxm5390lvdkhfwp24a726a4g3")))

(define-public crate-coordinate-transformer-1 (crate (name "coordinate-transformer") (vers "1.1.0") (deps (list (crate-dep (name "assert-be-close") (req "^1.0.0") (default-features #t) (kind 2)))) (hash "15117i6436yamskzdwv047w8q5vzvxcvpqi33lf7lbb7h7mi55db")))

(define-public crate-coordinate-transformer-1 (crate (name "coordinate-transformer") (vers "1.2.0") (deps (list (crate-dep (name "assert-be-close") (req "^1.0.0") (default-features #t) (kind 2)))) (hash "15gvd1zjv3cjcv9skyg8lzmiq71pw0fdv7jpq68s2r4fifzpmbi0")))

(define-public crate-coordinate-transformer-1 (crate (name "coordinate-transformer") (vers "1.3.0") (deps (list (crate-dep (name "close-to") (req "^0.1.0") (default-features #t) (kind 2)))) (hash "1dl1p6lp77hhl7bvk5xwfi2sgjnq40zl5xpjq0i6268s7hbg1yb1") (yanked #t)))

(define-public crate-coordinate-transformer-1 (crate (name "coordinate-transformer") (vers "1.4.0") (deps (list (crate-dep (name "close-to") (req "^0.1.0") (default-features #t) (kind 2)))) (hash "0bp2i9gb2jjvwi6nwady5vzly1km3s22k68wcr0zffl9hxyg30si")))

(define-public crate-coordinate-transformer-1 (crate (name "coordinate-transformer") (vers "1.5.0") (deps (list (crate-dep (name "close-to") (req "^0.1.0") (default-features #t) (kind 2)) (crate-dep (name "vec-x") (req "^0.2.0") (optional #t) (default-features #t) (kind 0)))) (hash "1n3d71dycj7dlz1vsym4dcq8d5hniqs53qil2075r3yqyrp1hr2n")))

(define-public crate-coordinate-transformer-1 (crate (name "coordinate-transformer") (vers "1.6.0") (deps (list (crate-dep (name "close-to") (req "^0.1.0") (default-features #t) (kind 2)) (crate-dep (name "vec-x") (req "^0.2.0") (optional #t) (default-features #t) (kind 0)))) (hash "1i99h054fhs2bwfpj4lhgwh0k9bsafbrwxnc6j2f6185njr9my6c")))

(define-public crate-coordinate-transformer-1 (crate (name "coordinate-transformer") (vers "1.7.0") (deps (list (crate-dep (name "close-to") (req "^0.1.0") (default-features #t) (kind 2)) (crate-dep (name "num") (req "^0.4.3") (default-features #t) (kind 0)) (crate-dep (name "vec-x") (req "^0.2.0") (optional #t) (default-features #t) (kind 0)))) (hash "0fq6h025iw3cd9g35x4ysdahz1yamb6j20754c0anlnbghs85mba")))

(define-public crate-coordinates-0.1 (crate (name "coordinates") (vers "0.1.0") (deps (list (crate-dep (name "assert_float_eq") (req "^1.1.3") (default-features #t) (kind 2)) (crate-dep (name "criterion") (req "^0.3.5") (default-features #t) (kind 2)) (crate-dep (name "num-traits") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 2)) (crate-dep (name "roots") (req "^0.0.7") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (optional #t) (default-features #t) (kind 0)))) (hash "1n4p19n81xyffxk0s4c7vb3d21zcr6r8c2vrmv9118l8ns60zi6i") (v 2) (features2 (quote (("serde" "dep:serde"))))))

(define-public crate-coordinates-0.1 (crate (name "coordinates") (vers "0.1.1") (deps (list (crate-dep (name "assert_float_eq") (req "^1.1.3") (default-features #t) (kind 2)) (crate-dep (name "criterion") (req "^0.3.5") (default-features #t) (kind 2)) (crate-dep (name "num-traits") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 2)) (crate-dep (name "roots") (req "^0.0.7") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (optional #t) (default-features #t) (kind 0)))) (hash "013wg1gbf6w25250v9h7kp3ablfxspfddc599z6jmmm636mb4ns6") (v 2) (features2 (quote (("serde" "dep:serde"))))))

(define-public crate-coordinates-0.2 (crate (name "coordinates") (vers "0.2.0") (deps (list (crate-dep (name "assert_float_eq") (req "^1.1.3") (default-features #t) (kind 2)) (crate-dep (name "criterion") (req "^0.3.5") (default-features #t) (kind 2)) (crate-dep (name "num-traits") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 2)) (crate-dep (name "roots") (req "^0.0.7") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (optional #t) (default-features #t) (kind 0)))) (hash "1lhrff6jzjs0rn9ndp85i1gr5fhyb97yzn2cfkcr3iyp5zqnsir8") (v 2) (features2 (quote (("serde" "dep:serde"))))))

(define-public crate-coordinates-0.3 (crate (name "coordinates") (vers "0.3.1") (deps (list (crate-dep (name "assert_float_eq") (req "^1.1.3") (default-features #t) (kind 2)) (crate-dep (name "criterion") (req "^0.4.0") (default-features #t) (kind 2)) (crate-dep (name "num-traits") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 2)) (crate-dep (name "roots") (req "^0.0.7") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)))) (hash "1nwvbv7z6n4vdgqr7vs6fqvvxi4jyiajrfdqg54f7g3phzgk5f4c") (v 2) (features2 (quote (("serde" "dep:serde"))))))

(define-public crate-coordinates-0.3 (crate (name "coordinates") (vers "0.3.0") (deps (list (crate-dep (name "assert_float_eq") (req "^1.1.3") (default-features #t) (kind 2)) (crate-dep (name "criterion") (req "^0.3.5") (default-features #t) (kind 2)) (crate-dep (name "num-traits") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 2)) (crate-dep (name "roots") (req "^0.0.7") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (optional #t) (default-features #t) (kind 0)))) (hash "0nqgrsrpjvbkr6v97pnwiiywlbj0xkvrdk3q3g0rf12bxxbj36zw") (v 2) (features2 (quote (("serde" "dep:serde"))))))

(define-public crate-coordinates_outliers-0.1 (crate (name "coordinates_outliers") (vers "0.1.0") (deps (list (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)) (crate-dep (name "simple_accumulator") (req "^0.3.1") (default-features #t) (kind 0)))) (hash "090ir5p2pjmk4f9i5p1xnhw74i9vcmd1q6aa07w58x126kp0z0cn")))

(define-public crate-coordinates_outliers-0.2 (crate (name "coordinates_outliers") (vers "0.2.1") (deps (list (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)) (crate-dep (name "simple_accumulator") (req "^0.3.1") (default-features #t) (kind 0)))) (hash "1yyyasxkm6fw8hwn2b3araxmf3gl92h5kqpbclzspz5dvsvrw9pd")))

(define-public crate-coordtransform-rs-0.1 (crate (name "coordtransform-rs") (vers "0.1.0") (hash "0bjjghpsqq5md6a898s6jh7wphzh6b8wc1bc1szfzh7a2z2q5yjk")))

