(define-module (crates-io co pp) #:use-module (crates-io))

(define-public crate-coppeliasim_zmq_remote_api-4 (crate (name "coppeliasim_zmq_remote_api") (vers "4.6.0") (deps (list (crate-dep (name "ciborium") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "env_logger") (req "^0.9.0") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.17") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.144") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.85") (default-features #t) (kind 0)) (crate-dep (name "uuid") (req "^1.1.2") (features (quote ("v4" "fast-rng"))) (default-features #t) (kind 0)) (crate-dep (name "zmq") (req "^0.9.2") (features (quote ("vendored"))) (default-features #t) (kind 0)))) (hash "14pj028xqag61grsc8ja1n0jazd5w8r3kgji8p3d1ik72hb86wj3")))

(define-public crate-coppeliasim_zmq_remote_api-4 (crate (name "coppeliasim_zmq_remote_api") (vers "4.6.1") (deps (list (crate-dep (name "ciborium") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "env_logger") (req "^0.9.0") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.17") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.144") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.85") (default-features #t) (kind 0)) (crate-dep (name "uuid") (req "^1.1.2") (features (quote ("v4" "fast-rng"))) (default-features #t) (kind 0)) (crate-dep (name "zmq") (req "^0.9.2") (features (quote ("vendored"))) (default-features #t) (kind 0)))) (hash "0lr483agv9k1swiqlp02wwbvpbjfwr432l07fc6si23ld425jh9k")))

(define-public crate-copper-0.1 (crate (name "copper") (vers "0.1.0") (deps (list (crate-dep (name "dyn-clone") (req "^1.0") (default-features #t) (kind 0)))) (hash "1k2r5jmp2lk1hvi7m47mjlnad7vkvrqnnbls1znpl4y0ffjn5hc7") (rust-version "1.77")))

(define-public crate-copper-rs-1 (crate (name "copper-rs") (vers "1.0.0") (deps (list (crate-dep (name "wasm-bindgen") (req "^0.2") (default-features #t) (kind 0)))) (hash "1a91gfbn5rvdyix952dz5rh782yav6ij1bv0dnzxscm2g8xsz80m")))

(define-public crate-copperline-0.0.1 (crate (name "copperline") (vers "0.0.1") (deps (list (crate-dep (name "libc") (req "^0.1.8") (default-features #t) (kind 0)) (crate-dep (name "nix") (req "^0.3.9") (default-features #t) (kind 0)))) (hash "08z9glyzj97yg9iqwv4k7kirngx56pihwb2mbf6npm7704q8yk6j")))

(define-public crate-copperline-0.0.2 (crate (name "copperline") (vers "0.0.2") (deps (list (crate-dep (name "libc") (req "^0.1.8") (default-features #t) (kind 0)) (crate-dep (name "nix") (req "^0.3.9") (default-features #t) (kind 0)))) (hash "01w584kwz709ffgn0rp8dld58yk1s7kmjp3bvhiapxwbnp1q3fsy")))

(define-public crate-copperline-0.0.3 (crate (name "copperline") (vers "0.0.3") (deps (list (crate-dep (name "libc") (req "^0.1.8") (default-features #t) (kind 0)) (crate-dep (name "nix") (req "^0.3.9") (default-features #t) (kind 0)))) (hash "0kp1d4wd493qcla2r06bkyqxwh4fv6j9lcw1gx8k7gf8aan9i6q7")))

(define-public crate-copperline-0.1 (crate (name "copperline") (vers "0.1.0") (deps (list (crate-dep (name "libc") (req "^0.1.8") (default-features #t) (kind 0)) (crate-dep (name "nix") (req "^0.3.9") (default-features #t) (kind 0)))) (hash "08ya6mp2z94diwj9irdn5a2ba1bqb6h0nk880r8ghz436kpfi4wh")))

(define-public crate-copperline-0.1 (crate (name "copperline") (vers "0.1.1") (deps (list (crate-dep (name "libc") (req "^0.1.8") (default-features #t) (kind 0)) (crate-dep (name "nix") (req "^0.3.9") (default-features #t) (kind 0)))) (hash "0hmhxkbmldhbwrax9hjvyczisf8jjiv22rxwyxq6qnawr06pxxcc")))

(define-public crate-copperline-0.1 (crate (name "copperline") (vers "0.1.2") (deps (list (crate-dep (name "libc") (req "^0.1.8") (default-features #t) (kind 0)) (crate-dep (name "nix") (req "^0.3.9") (default-features #t) (kind 0)))) (hash "1pkgfk1sww9v3f47g1zj91d1l6j44x610axbv047niarhv4pvvq9")))

(define-public crate-copperline-0.1 (crate (name "copperline") (vers "0.1.3") (deps (list (crate-dep (name "libc") (req "^0.1.10") (default-features #t) (kind 0)) (crate-dep (name "nix") (req "^0.4.0") (default-features #t) (kind 0)))) (hash "0w1wskjsk0zi3n9a2j2h7bxd7yxjxqvw4yc964jfmh51ncrpfzq3")))

(define-public crate-copperline-0.2 (crate (name "copperline") (vers "0.2.0") (deps (list (crate-dep (name "libc") (req "^0.1.10") (default-features #t) (kind 0)) (crate-dep (name "nix") (req "^0.4.0") (default-features #t) (kind 0)))) (hash "13as168zkm3yccazvz4fnnfb4q29lm5c8dhwjimzrlwlb18gf216")))

(define-public crate-copperline-0.2 (crate (name "copperline") (vers "0.2.1") (deps (list (crate-dep (name "libc") (req "^0.1.10") (default-features #t) (kind 0)) (crate-dep (name "nix") (req "^0.4.0") (default-features #t) (kind 0)))) (hash "13yfxmnpwgbqhv3i8c9ga8qh3bf0dm9z33qykkljci8ay6vki9ms")))

(define-public crate-copperline-0.3 (crate (name "copperline") (vers "0.3.0") (deps (list (crate-dep (name "encoding") (req "^0.2.32") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.1.10") (default-features #t) (kind 0)) (crate-dep (name "nix") (req "^0.4.0") (default-features #t) (kind 0)) (crate-dep (name "strcursor") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "unicode-width") (req "^0.1.3") (default-features #t) (kind 0)))) (hash "1wr6svf23jinq0rs5fx2d57ihf1ixr0j86f2slxrsi3p3d3sd01y")))

(define-public crate-coppers-0.0.1 (crate (name "coppers") (vers "0.0.1") (deps (list (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)))) (hash "0yaj79c0lsfmpnxmlap0922sy7qwdbva24c621gprwqx4c4h50x9") (yanked #t)))

(define-public crate-coppers-0.1 (crate (name "coppers") (vers "0.1.0") (deps (list (crate-dep (name "git2") (req "^0.14.2") (default-features #t) (kind 0)) (crate-dep (name "hex") (req "^0.4.3") (default-features #t) (kind 0)) (crate-dep (name "pyo3") (req "^0.16") (features (quote ("auto-initialize"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)))) (hash "0haq5q2lx73wgysmwmnxfshyhcrf28b3x76q39kffalbjqqkc7gv") (features (quote (("visualization" "pyo3"))))))

(define-public crate-coppers-0.1 (crate (name "coppers") (vers "0.1.1") (deps (list (crate-dep (name "git2") (req "^0.14.2") (default-features #t) (kind 0)) (crate-dep (name "hex") (req "^0.4.3") (default-features #t) (kind 0)) (crate-dep (name "pyo3") (req "^0.16") (features (quote ("auto-initialize"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)))) (hash "064kbycfz5cl15c77y0y5jcpfyfpm5xrmyijaj5d7gzsanvlvs2x") (features (quote (("visualization" "pyo3"))))))

(define-public crate-coppy-0.1 (crate (name "coppy") (vers "0.1.0") (deps (list (crate-dep (name "copypasta") (req "^0.8.1") (default-features #t) (kind 0)))) (hash "0pv2wiszaccybm31wv87vx589pwprr7a5v1v8qsmivglprfhr3pk")))

(define-public crate-coppy-0.1 (crate (name "coppy") (vers "0.1.1") (deps (list (crate-dep (name "copypasta") (req "^0.8.1") (default-features #t) (kind 0)))) (hash "10fps16qvwa3c9mf47b6s4jch8qhdccvafrw0h9vklng6575y40v")))

(define-public crate-coppy-0.1 (crate (name "coppy") (vers "0.1.2") (deps (list (crate-dep (name "copypasta") (req "^0.8.1") (default-features #t) (kind 0)))) (hash "12438rkah05l1w40xkzx3xxrhjn442qh75s4fsh03v6wn24fpgqp")))

