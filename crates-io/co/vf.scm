(define-module (crates-io co vf) #:use-module (crates-io))

(define-public crate-covfefe-0.1 (crate (name "covfefe") (vers "0.1.0") (hash "1qnibfhhxva5bvq23c9zasmrbz3y1fldw1a8slw03ndz3111p8xn")))

(define-public crate-covfefify-0.1 (crate (name "covfefify") (vers "0.1.0") (hash "03hwqk1cfxzsl7fz8ziaag54vmvp435j2m9q6id8npv0nm8711ds")))

