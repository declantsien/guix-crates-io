(define-module (crates-io co nq) #:use-module (crates-io))

(define-public crate-conque-0.0.0 (crate (name "conque") (vers "0.0.0") (hash "1l8dcv4f7hmqrlrmq6ljy3qgrydf59b9kc2spnqvvp4ip37a1rwd") (yanked #t)))

(define-public crate-conque-0.0.0 (crate (name "conque") (vers "0.0.0-alpha") (hash "0bs653yzn3r84hxqsnvciyszlyqlcb3lvlgj8rh2s65rnx0cxnkl")))

(define-public crate-conquer-0.0.0 (crate (name "conquer") (vers "0.0.0") (hash "1rnynhamkf7kc7dzwdp0x2b4k3yw3bzkx8csha90zyvqj54ys6hf")))

(define-public crate-conquer-once-0.1 (crate (name "conquer-once") (vers "0.1.0") (deps (list (crate-dep (name "conquer-util") (req "^0.1.0") (features (quote ("back-off"))) (default-features #t) (kind 0)))) (hash "11rsapwn6yk1b22v5kmhflbjfkvscspv5dir762argz19sb5cgmf") (features (quote (("std") ("default" "std"))))))

(define-public crate-conquer-once-0.1 (crate (name "conquer-once") (vers "0.1.1") (deps (list (crate-dep (name "conquer-util") (req "^0.1.0") (features (quote ("back-off"))) (default-features #t) (kind 0)))) (hash "129g2h6kq1nvdpkf0hr7lr61q1ijizbjz9w4c030q3c2fs9a45ff") (features (quote (("std") ("default" "std"))))))

(define-public crate-conquer-once-0.1 (crate (name "conquer-once") (vers "0.1.2") (deps (list (crate-dep (name "conquer-util") (req "^0.2.0") (features (quote ("back-off"))) (default-features #t) (kind 0)))) (hash "1pp22dkirj8h3w5cpdswy1nsg8iqyvd0vcxwvb9vxlwl8aa4xyhn") (features (quote (("std") ("default" "std"))))))

(define-public crate-conquer-once-0.2 (crate (name "conquer-once") (vers "0.2.0") (deps (list (crate-dep (name "conquer-util") (req "^0.2.0") (features (quote ("back-off"))) (kind 0)))) (hash "13zrnhly3f836az11rjv0kfzg60pmy9874aa9vbwm3jl19h48xkg") (features (quote (("std") ("default" "std"))))))

(define-public crate-conquer-once-0.2 (crate (name "conquer-once") (vers "0.2.1") (deps (list (crate-dep (name "conquer-util") (req "^0.2.0") (features (quote ("back-off"))) (kind 0)))) (hash "18zgz5wzbkyjybpp1qbmi6p30253ws4x62chmvxicrs6d7xi5swn") (features (quote (("std") ("default" "std"))))))

(define-public crate-conquer-once-0.3 (crate (name "conquer-once") (vers "0.3.0") (deps (list (crate-dep (name "conquer-util") (req "^0.3.0") (features (quote ("back-off"))) (kind 0)))) (hash "1x1ahqj25c9jrvwmyxd0z70868x9bvvki14l788lk51y592slxnk") (features (quote (("std") ("default" "std"))))))

(define-public crate-conquer-once-0.3 (crate (name "conquer-once") (vers "0.3.1") (deps (list (crate-dep (name "conquer-util") (req ">=0.3.0, <0.4.0") (features (quote ("back-off"))) (kind 0)))) (hash "053ji063gxirhl1iwlsci48bbxa8q7k0r1a83gxfw8wk14i57d2w") (features (quote (("std") ("default" "std"))))))

(define-public crate-conquer-once-0.3 (crate (name "conquer-once") (vers "0.3.2") (deps (list (crate-dep (name "conquer-util") (req "^0.3.0") (features (quote ("back-off"))) (kind 0)))) (hash "12lixkr25rw437wjlkfssdbh5lvynscqk26cw8gnv7x6fnbklvbw") (features (quote (("std") ("default" "std"))))))

(define-public crate-conquer-once-0.4 (crate (name "conquer-once") (vers "0.4.0") (deps (list (crate-dep (name "conquer-util") (req "^0.3.0") (features (quote ("back-off"))) (kind 0)))) (hash "1d30445kpsvnp6c1vvpz1pka71ls0ql2aw8kr8v9y9hg3i28l02x") (features (quote (("std") ("default" "std"))))))

(define-public crate-conquer-struct-0.1 (crate (name "conquer-struct") (vers "0.1.0") (deps (list (crate-dep (name "proc-macro2") (req "^1.0.59") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.28") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2.0.18") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.28.2") (features (quote ("macros" "rt-multi-thread"))) (default-features #t) (kind 2)))) (hash "1xann39ymfbvnfkxwyklsbc2kl49rh0bry7a1y8gnmrvp24gwcw5")))

(define-public crate-conquer-util-0.1 (crate (name "conquer-util") (vers "0.1.0") (deps (list (crate-dep (name "rand") (req "^0.7.0") (features (quote ("small_rng"))) (optional #t) (kind 0)))) (hash "1bdcv22fzry3qf8n75nk0803hlv2acr4577ykmjr0ji1rwcgx4n8") (features (quote (("tls") ("std") ("default" "std") ("back-off" "rand") ("alloc") ("align"))))))

(define-public crate-conquer-util-0.2 (crate (name "conquer-util") (vers "0.2.0") (deps (list (crate-dep (name "rand") (req "^0.7.0") (features (quote ("small_rng"))) (optional #t) (kind 0)))) (hash "0ahqn6ari4v51xla1rqa6zppnihxm0gkl427ql8x6sf35i3v4kv5") (features (quote (("tls") ("std") ("random" "back-off" "rand") ("default" "std") ("back-off") ("alloc") ("align"))))))

(define-public crate-conquer-util-0.3 (crate (name "conquer-util") (vers "0.3.0") (deps (list (crate-dep (name "rand") (req "^0.7.3") (features (quote ("small_rng"))) (optional #t) (kind 0)))) (hash "10imvg3jaz5x6w11g5bfwnj0q2vp06jcvzkxyf0b64vbhkwfwqz7") (features (quote (("tls" "alloc") ("std") ("random" "back-off" "rand") ("default" "std") ("back-off") ("alloc") ("align"))))))

(define-public crate-conqueue-0.1 (crate (name "conqueue") (vers "0.1.0") (hash "0r6vhdlfgvjs4dcrpr07zjraxi0gz5czbf7jqih49ra8zz34lyvf")))

(define-public crate-conqueue-0.1 (crate (name "conqueue") (vers "0.1.1") (hash "17z4r1fjxqca95zh8f6j9aws6jf6727zz4hac7pw7r5qc726y6nk")))

(define-public crate-conqueue-0.2 (crate (name "conqueue") (vers "0.2.0") (hash "0b1rw0rc5bsc5cmgvqpa77l0fajrr3mfs5zsdqmj511a1kmn08av")))

(define-public crate-conqueue-0.2 (crate (name "conqueue") (vers "0.2.1") (hash "1fmpzl35rp5naq2l4rcafn0yjy9chn77zpz4bdd6z8f73dj4s7kw")))

(define-public crate-conqueue-0.3 (crate (name "conqueue") (vers "0.3.0") (hash "0qsyisjkcf1hmffvy5jn1vzj4wwa7gq6qy9rh93dv22j6y5j0saj")))

(define-public crate-conqueue-0.4 (crate (name "conqueue") (vers "0.4.0") (hash "0rkv88k094b49xmbhm2snh4fxnjpi80nbyll9fbd75bbg5n31i7a")))

