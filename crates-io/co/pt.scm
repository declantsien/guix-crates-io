(define-module (crates-io co pt) #:use-module (crates-io))

(define-public crate-copt-0.1 (crate (name "copt") (vers "0.1.0") (deps (list (crate-dep (name "bytes") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "num_enum") (req "^0.6.1") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.40") (default-features #t) (kind 0)) (crate-dep (name "tokio-util") (req "^0.7.8") (features (quote ("codec"))) (default-features #t) (kind 0)) (crate-dep (name "tpkt") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "01x4fff09sfzvr3il234agp0vfym4sryh8v9fvs41p8a8w88f7p7")))

(define-public crate-coptic-0.1 (crate (name "coptic") (vers "0.1.0") (hash "1wcxy4wfacvhzzsqlvq9g34amzcww7bfgpmcs1mm4as3zldm9z0q")))

(define-public crate-coptic-0.1 (crate (name "coptic") (vers "0.1.1") (hash "16l8j0ciziqld5ibfz83y9r4kpqaqcxdygfh5s3ixhwgnzwyyb2g")))

(define-public crate-coption-0.1 (crate (name "coption") (vers "0.1.0") (deps (list (crate-dep (name "self-rust-tokenize") (req "0.3.*") (default-features #t) (kind 0)))) (hash "0m1qi5wswcgkiy3hm6l2d56haxwblcw2n95kmvdiz0hrgpqj90w9") (features (quote (("self_rust_tokenize") ("default" "self_rust_tokenize"))))))

