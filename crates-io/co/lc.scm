(define-module (crates-io co lc) #:use-module (crates-io))

(define-public crate-colci-0.1 (crate (name "colci") (vers "0.1.0") (hash "0wvnak41cx6gyxz2ln8jkilzsk1376b18s35q1wmwxdahcqrk0pj")))

(define-public crate-colcon-0.1 (crate (name "colcon") (vers "0.1.0") (hash "1akhiz3p6gimkzh1bbxb9v6vlk33kl94y1jb4cj9gj3ya8i2aq9i") (features (quote (("D50"))))))

(define-public crate-colcon-0.1 (crate (name "colcon") (vers "0.1.1") (hash "0crfar14m3lhh8rvjg9x5hhyr8r025f6x72f2afa079l5hcsl7by") (features (quote (("D50"))))))

(define-public crate-colcon-0.2 (crate (name "colcon") (vers "0.2.0") (hash "1pxjz70z89hfyi8mcx20r2nh9dl9la6lnlaqixkdqnfn2vfiljmh") (features (quote (("D50"))))))

(define-public crate-colcon-0.2 (crate (name "colcon") (vers "0.2.1") (deps (list (crate-dep (name "criterion") (req "^0.5") (default-features #t) (kind 2)))) (hash "0rwf7h6f8qfvw72x7qbw9plfn1cg3pxri1jf800ddwvl8a0h4j0a") (features (quote (("D50"))))))

(define-public crate-colcon-0.2 (crate (name "colcon") (vers "0.2.2") (deps (list (crate-dep (name "criterion") (req "^0.5") (default-features #t) (kind 2)))) (hash "1b2hrimqbd3x96mdrhbihzn6mfgl608w755s26ypmvzkw5d92bdc") (features (quote (("D50"))))))

(define-public crate-colcon-0.3 (crate (name "colcon") (vers "0.3.0") (deps (list (crate-dep (name "criterion") (req "^0.5") (default-features #t) (kind 2)))) (hash "0bym5mi59rp1y0f7nw3712fmixajsnlmiw2izks44gkpnf81rrs6") (features (quote (("D50"))))))

(define-public crate-colcon-0.4 (crate (name "colcon") (vers "0.4.0") (deps (list (crate-dep (name "criterion") (req "^0.5") (default-features #t) (kind 2)))) (hash "0qaqjiif7y3fdwvq5gpjjcgf1p1gy7z43y4zlfak4p8ak0qblq8s")))

(define-public crate-colcon-0.4 (crate (name "colcon") (vers "0.4.1") (deps (list (crate-dep (name "criterion") (req "^0.5") (default-features #t) (kind 2)))) (hash "1d6w04gfyy620gp5z4c3fh4b3qjhbj9chjzv2dxgca6qnnh1lhpa")))

(define-public crate-colcon-0.5 (crate (name "colcon") (vers "0.5.0") (deps (list (crate-dep (name "criterion") (req "^0.5") (default-features #t) (kind 2)))) (hash "17n0779llkh2mpd3jizjwcywyrgq6hwklw7sa385qkln9wi83ayf")))

(define-public crate-colcon-0.5 (crate (name "colcon") (vers "0.5.1") (deps (list (crate-dep (name "criterion") (req "^0.5") (default-features #t) (kind 2)))) (hash "1a4wx0iizv4s8fmfsvifi17cg7gzhlrdah7xqxhwfm5j38rcmlys")))

(define-public crate-colcon-0.6 (crate (name "colcon") (vers "0.6.0") (deps (list (crate-dep (name "criterion") (req "^0.5") (default-features #t) (kind 2)))) (hash "06b7p05qjvlgscrdynr1iz3sc8mm4vr07vgng4qnlnrfhrh2yq9r")))

(define-public crate-colcon-0.7 (crate (name "colcon") (vers "0.7.0") (deps (list (crate-dep (name "criterion") (req "^0.5") (default-features #t) (kind 2)))) (hash "10sxaa77lycxy1jk8bwfwqpjlr533k0vw4hi37l1z5mg6rixgyx2")))

(define-public crate-colcon-0.7 (crate (name "colcon") (vers "0.7.1") (deps (list (crate-dep (name "criterion") (req "^0.5") (default-features #t) (kind 2)))) (hash "092n2ik3cfhc4zgzqg0ig94mn5yfb2yc3wwjn8bbjzpf654zbg1v")))

(define-public crate-colcon-0.8 (crate (name "colcon") (vers "0.8.0") (deps (list (crate-dep (name "criterion") (req "^0.5") (default-features #t) (kind 2)))) (hash "1h7vgvk6ihc1dwam36mz1yibag7i4czbm9byy2wnjqv6zk3n3m53")))

(define-public crate-colcon-0.8 (crate (name "colcon") (vers "0.8.1") (deps (list (crate-dep (name "criterion") (req "^0.5") (default-features #t) (kind 2)))) (hash "09i0bpzp6crh6wygf4s4y7ff7cxj15zh52cj7skb769vyg2r8hlx")))

