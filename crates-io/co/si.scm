(define-module (crates-io co si) #:use-module (crates-io))

(define-public crate-cosiest_noisiest-0.1 (crate (name "cosiest_noisiest") (vers "0.1.0") (deps (list (crate-dep (name "cfg-if") (req "^1.0.0") (default-features #t) (kind 0)) (crate-dep (name "divan") (req "^0.1.14") (default-features #t) (kind 2)) (crate-dep (name "num-integer") (req "^0.1") (kind 0)) (crate-dep (name "rand") (req "^0.8") (kind 0)) (crate-dep (name "rand_chacha") (req "^0.3") (kind 0)) (crate-dep (name "splines") (req "^4.3") (default-features #t) (kind 0)))) (hash "1s20yn2mzqk31aqmzs3gqxcp3p9xghidw5991sqlik9prfcm2pqh") (features (quote (("f32"))))))

(define-public crate-cosiest_noisiest-0.1 (crate (name "cosiest_noisiest") (vers "0.1.1") (deps (list (crate-dep (name "cfg-if") (req "^1.0.0") (default-features #t) (kind 0)) (crate-dep (name "divan") (req "^0.1.14") (default-features #t) (kind 2)) (crate-dep (name "num-integer") (req "^0.1") (kind 0)) (crate-dep (name "rand") (req "^0.8") (kind 0)) (crate-dep (name "rand_chacha") (req "^0.3") (kind 0)) (crate-dep (name "splines") (req "^4.3") (default-features #t) (kind 0)))) (hash "0g7ic2hl008k0ksflaqffiq9lplg06mqbn8bc19dq29xf5c4khy6") (features (quote (("f32"))))))

(define-public crate-cosine-lsh-0.1 (crate (name "cosine-lsh") (vers "0.1.0") (deps (list (crate-dep (name "rand") (req "^0.6.5") (default-features #t) (kind 0)))) (hash "1p8k2jgwbfnyi11pphazkd5fdpvz92rb5shcd7pdb0fmy0687pg3")))

(define-public crate-cosine-lsh-0.1 (crate (name "cosine-lsh") (vers "0.1.1") (deps (list (crate-dep (name "rand") (req "^0.6.5") (default-features #t) (kind 0)))) (hash "13rw9dlb9rfh9c645f22456c6hhji052nc0b5ypjh8qgs555clcv")))

