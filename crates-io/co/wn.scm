(define-module (crates-io co wn) #:use-module (crates-io))

(define-public crate-cown-0.1 (crate (name "cown") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^3.1") (features (quote ("cargo"))) (default-features #t) (kind 0)) (crate-dep (name "glob") (req "^0.3") (default-features #t) (kind 0)))) (hash "0447i1fq4wdcrglmibs82jk76wsv0g9xcskgsnq275aa1bwi08iq")))

(define-public crate-cown-0.1 (crate (name "cown") (vers "0.1.1") (deps (list (crate-dep (name "clap") (req "^3.1") (features (quote ("cargo"))) (default-features #t) (kind 0)) (crate-dep (name "glob") (req "^0.3") (default-features #t) (kind 0)))) (hash "1yfi449iydhql23fsxygdw275p8fj3mymcwpj95fl168yyaq2y25")))

(define-public crate-cown-0.1 (crate (name "cown") (vers "0.1.2") (deps (list (crate-dep (name "clap") (req "^3.1") (features (quote ("cargo"))) (default-features #t) (kind 0)) (crate-dep (name "glob") (req "^0.3") (default-features #t) (kind 0)))) (hash "04ga3nnjxd7fm60ggbnqcayrywdi1syp0n668d5glfhxq7lnaclz")))

