(define-module (crates-io co m-) #:use-module (crates-io))

(define-public crate-com-croftsoft-0.1 (crate (name "com-croftsoft") (vers "0.1.0") (hash "05yz1hy8ffw5nl25izj9sdm1w888y637946ibfiapm8xrn0yi9jp") (yanked #t)))

(define-public crate-com-croftsoft-0.1 (crate (name "com-croftsoft") (vers "0.1.1") (deps (list (crate-dep (name "com-croftsoft-core") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "1bpajbkf3acfmfvp7v0gnnjgkhpf4a8ndmzq9qihic28dmfqb3j3") (yanked #t)))

(define-public crate-com-croftsoft-0.1 (crate (name "com-croftsoft") (vers "0.1.2") (deps (list (crate-dep (name "com-croftsoft-core") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "1x9wb5s10pnxdiy6r7rfnya7xwfgz208cpq6jh5k5ivwq6x888c9")))

(define-public crate-com-croftsoft-0.2 (crate (name "com-croftsoft") (vers "0.2.0") (deps (list (crate-dep (name "com-croftsoft-core") (req "^0.2.0") (default-features #t) (kind 0)))) (hash "19df7r44ac37gcz3bwv1ybn7d5wrhmkz1b5lm3c6mcn5xkzh2air")))

(define-public crate-com-croftsoft-0.9 (crate (name "com-croftsoft") (vers "0.9.0") (deps (list (crate-dep (name "com-croftsoft-core") (req "^0.9.0") (default-features #t) (kind 0)))) (hash "0745faypyai4vbax1arwf0lxrrc6jcpwg9q713dg31wfknh4dhc0")))

(define-public crate-com-croftsoft-0.10 (crate (name "com-croftsoft") (vers "0.10.0") (deps (list (crate-dep (name "com-croftsoft-core") (req "^0.10.0") (default-features #t) (kind 0)))) (hash "1pd1ichk5a0ykpb0b45hcz1k15ihky6443lralcscwbg1b3bpd7d")))

(define-public crate-com-croftsoft-0.11 (crate (name "com-croftsoft") (vers "0.11.0") (deps (list (crate-dep (name "com-croftsoft-core") (req "^0.14.0") (default-features #t) (kind 0)) (crate-dep (name "com-croftsoft-lib") (req "^0.1.1") (default-features #t) (kind 0)))) (hash "09w9migv4brnfy1gflhgnknjq0kb5r87pbfkms86h5kqcl04af07") (rust-version "1.73")))

(define-public crate-com-croftsoft-core-0.1 (crate (name "com-croftsoft-core") (vers "0.1.0") (hash "00p4iik0hvglh0v0ay4a1lfd1l7r22l7vwl0j54cpq9qacpqd849")))

(define-public crate-com-croftsoft-core-0.2 (crate (name "com-croftsoft-core") (vers "0.2.0") (hash "0xa4ciwjmj5gg9v4bmsm93wlgm80apzmr9cpk3d34jv7cg9wblmx")))

(define-public crate-com-croftsoft-core-0.3 (crate (name "com-croftsoft-core") (vers "0.3.0") (hash "14djdslqdnafl2qvaqv70x2rskqm6g1pvq1z1ci2h1vwhy6wbf6v")))

(define-public crate-com-croftsoft-core-0.4 (crate (name "com-croftsoft-core") (vers "0.4.0") (hash "1h2a9a7icdjz8v97v1zylkh62g4qn09982wx16vhn57davh7szib")))

(define-public crate-com-croftsoft-core-0.5 (crate (name "com-croftsoft-core") (vers "0.5.0") (hash "015v8h1chc29p96745nnyh3470rdz4jfgbwpsw25jb7k6pbvh23s")))

(define-public crate-com-croftsoft-core-0.6 (crate (name "com-croftsoft-core") (vers "0.6.0") (hash "0fihzq3w9jzmq8wii88l6w4wq75gm7rx32y3g08yzl492kcndscj")))

(define-public crate-com-croftsoft-core-0.7 (crate (name "com-croftsoft-core") (vers "0.7.0") (hash "0dbpksm66kdczi5yp9kcz62rlmyhjp5nnfvi12n55qjmi3d2ag88")))

(define-public crate-com-croftsoft-core-0.8 (crate (name "com-croftsoft-core") (vers "0.8.0") (hash "1y7mg3jzl6sh2jka9lbz0wgdih0k42m95qm2zcaq6a2vy2lrrkxc")))

(define-public crate-com-croftsoft-core-0.9 (crate (name "com-croftsoft-core") (vers "0.9.0") (hash "01mxsxzga77rq9ifg98nriqv0xy35zksv256fazpkyhiyffrnbsr")))

(define-public crate-com-croftsoft-core-0.10 (crate (name "com-croftsoft-core") (vers "0.10.0") (hash "15aifp1hfqjilnf66f5bb92rzkfg75jinr0x7lbgf529vvlzb6v4")))

(define-public crate-com-croftsoft-core-0.11 (crate (name "com-croftsoft-core") (vers "0.11.0") (hash "1i5rfzql5cgnfv8908zc40gmv73yh2injkcmgzdslgb146aixgiq")))

(define-public crate-com-croftsoft-core-0.12 (crate (name "com-croftsoft-core") (vers "0.12.0") (hash "0m8y0j6r9c4q8gnj78bwf5r50hdix9s7sgqa42b14kq0347chxvk")))

(define-public crate-com-croftsoft-core-0.13 (crate (name "com-croftsoft-core") (vers "0.13.0") (hash "1h84wzqhsj4drd3zb4pxc4b91nyxv06znpk3m1d1gm4zg18apzhi")))

(define-public crate-com-croftsoft-core-0.14 (crate (name "com-croftsoft-core") (vers "0.14.0") (hash "1z1lk4kpw67i9ziqrgb9qwkwzm2vy4dcrnk9q3h32bvg40g0ij6i")))

(define-public crate-com-croftsoft-lib-0.1 (crate (name "com-croftsoft-lib") (vers "0.1.0") (deps (list (crate-dep (name "com-croftsoft-lib-animation") (req "^0.0.8") (default-features #t) (kind 0)) (crate-dep (name "com-croftsoft-lib-role") (req "^0.5.1") (default-features #t) (kind 0)) (crate-dep (name "com-croftsoft-lib-string") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "03vx69jkgw5dh635gnvkccgzcabpjywf9qwp507di4zqbdnz5fb1") (rust-version "1.73")))

(define-public crate-com-croftsoft-lib-0.1 (crate (name "com-croftsoft-lib") (vers "0.1.1") (deps (list (crate-dep (name "com-croftsoft-lib-animation") (req "^0.0.8") (default-features #t) (kind 0)) (crate-dep (name "com-croftsoft-lib-role") (req "^0.5.1") (default-features #t) (kind 0)) (crate-dep (name "com-croftsoft-lib-string") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "0lpkcw29sd3z4jz5691grc996c2wa0qrp3mn5mxr0wmmcbd0nawj") (rust-version "1.73")))

(define-public crate-com-croftsoft-lib-animation-0.0.0 (crate (name "com-croftsoft-lib-animation") (vers "0.0.0") (deps (list (crate-dep (name "com-croftsoft-lib-role") (req "^0.1.4") (default-features #t) (kind 0)))) (hash "05i10ygi4dhmgs5q68wi14r0d6rhhc4703k32s73xd6wipb8ady7")))

(define-public crate-com-croftsoft-lib-animation-0.0.1 (crate (name "com-croftsoft-lib-animation") (vers "0.0.1") (deps (list (crate-dep (name "com-croftsoft-lib-role") (req "^0.1.4") (default-features #t) (kind 0)))) (hash "1k98mafcc44bb95sw11424kihz9710cfkhsnrdcvzaqfg6wfgwam")))

(define-public crate-com-croftsoft-lib-animation-0.0.2 (crate (name "com-croftsoft-lib-animation") (vers "0.0.2") (deps (list (crate-dep (name "com-croftsoft-lib-role") (req "^0.1.4") (default-features #t) (kind 0)))) (hash "1z6csi5r3gc2sry2f3g9swdbl2yxr4fr5mp7aafh0z58cvdyb9ci")))

(define-public crate-com-croftsoft-lib-animation-0.0.3 (crate (name "com-croftsoft-lib-animation") (vers "0.0.3") (deps (list (crate-dep (name "com-croftsoft-lib-role") (req "^0.1.4") (default-features #t) (kind 0)))) (hash "1b0hcsxjfxyi99yam6sm5v4aynqkxx2cddwwdxhk46250hn6k0zb")))

(define-public crate-com-croftsoft-lib-animation-0.0.4 (crate (name "com-croftsoft-lib-animation") (vers "0.0.4") (deps (list (crate-dep (name "com-croftsoft-lib-role") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "wasm-bindgen") (req "^0.2.83") (default-features #t) (kind 0)) (crate-dep (name "web-sys") (req "^0.3.60") (features (quote ("CanvasRenderingContext2d"))) (default-features #t) (kind 0)))) (hash "1nhsqqr6gngmrv647jp1alvy6zln5nznmw84158v6qvp6wma5mkm")))

(define-public crate-com-croftsoft-lib-animation-0.0.5 (crate (name "com-croftsoft-lib-animation") (vers "0.0.5") (deps (list (crate-dep (name "anyhow") (req "^1.0.66") (default-features #t) (kind 0)) (crate-dep (name "com-croftsoft-lib-role") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "futures") (req "^0.3.26") (default-features #t) (kind 0)) (crate-dep (name "js-sys") (req "^0.3.61") (default-features #t) (kind 0)) (crate-dep (name "wasm-bindgen") (req "^0.2.84") (default-features #t) (kind 0)) (crate-dep (name "wasm-bindgen-futures") (req "^0.4.34") (default-features #t) (kind 0)) (crate-dep (name "web-sys") (req "^0.3.61") (features (quote ("CanvasRenderingContext2d" "Document" "DomRect" "Element" "Event" "HtmlCanvasElement" "MouseEvent" "Window" "console"))) (default-features #t) (kind 0)))) (hash "01zym94f9mw2x1marqn5jwz7r1bm6ahfcvsbcw9z0z5xlj68qh9c")))

(define-public crate-com-croftsoft-lib-animation-0.0.6 (crate (name "com-croftsoft-lib-animation") (vers "0.0.6") (deps (list (crate-dep (name "anyhow") (req "^1.0.66") (default-features #t) (kind 0)) (crate-dep (name "com-croftsoft-lib-role") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "futures") (req "^0.3.26") (default-features #t) (kind 0)) (crate-dep (name "js-sys") (req "^0.3.61") (default-features #t) (kind 0)) (crate-dep (name "wasm-bindgen") (req "^0.2.84") (default-features #t) (kind 0)) (crate-dep (name "wasm-bindgen-futures") (req "^0.4.34") (default-features #t) (kind 0)) (crate-dep (name "web-sys") (req "^0.3.61") (features (quote ("CanvasRenderingContext2d" "Document" "DomRect" "Element" "Event" "HtmlCanvasElement" "MouseEvent" "Window" "console"))) (default-features #t) (kind 0)))) (hash "1sx3j9kky5v9wwpk7qxpc5fcdrl925pp6bjmw951x6pgpq7cssy9")))

(define-public crate-com-croftsoft-lib-animation-0.0.7 (crate (name "com-croftsoft-lib-animation") (vers "0.0.7") (deps (list (crate-dep (name "anyhow") (req "^1.0.75") (default-features #t) (kind 0)) (crate-dep (name "com-croftsoft-lib-role") (req "^0.4.0") (default-features #t) (kind 0)) (crate-dep (name "futures") (req "^0.3.28") (default-features #t) (kind 0)) (crate-dep (name "js-sys") (req "^0.3.64") (default-features #t) (kind 0)) (crate-dep (name "wasm-bindgen") (req "^0.2.87") (default-features #t) (kind 0)) (crate-dep (name "wasm-bindgen-futures") (req "^0.4.37") (default-features #t) (kind 0)) (crate-dep (name "web-sys") (req "^0.3.64") (features (quote ("CanvasRenderingContext2d" "Document" "DomRect" "Element" "Event" "HtmlCanvasElement" "MouseEvent" "Window" "console"))) (default-features #t) (kind 0)))) (hash "1zc8kf5giyqvr4yc14nbiqkkvjv7n41z286x8li5ksapz16c6snn")))

(define-public crate-com-croftsoft-lib-animation-0.0.8 (crate (name "com-croftsoft-lib-animation") (vers "0.0.8") (deps (list (crate-dep (name "anyhow") (req "^1.0.80") (default-features #t) (kind 0)) (crate-dep (name "com-croftsoft-lib-role") (req "^0.5.1") (default-features #t) (kind 0)) (crate-dep (name "futures") (req "^0.3.30") (default-features #t) (kind 0)) (crate-dep (name "js-sys") (req "^0.3.68") (default-features #t) (kind 0)) (crate-dep (name "wasm-bindgen") (req "^0.2.91") (default-features #t) (kind 0)) (crate-dep (name "wasm-bindgen-futures") (req "^0.4.41") (default-features #t) (kind 0)) (crate-dep (name "web-sys") (req "^0.3.68") (features (quote ("CanvasRenderingContext2d" "Document" "DomRect" "Element" "Event" "HtmlCanvasElement" "MouseEvent" "Window" "console"))) (default-features #t) (kind 0)))) (hash "0qfs8x57xxhz80aa6jrh5xi92dlakqpa1mms3090cbz5vppvb0c5")))

(define-public crate-com-croftsoft-lib-role-0.1 (crate (name "com-croftsoft-lib-role") (vers "0.1.0") (hash "13grj9s91cazjybw2rppxb0bk4pf7la2llwdqv6xs4gdzp1gh30c") (yanked #t)))

(define-public crate-com-croftsoft-lib-role-0.1 (crate (name "com-croftsoft-lib-role") (vers "0.1.1") (hash "07x22gizjgg87npfjky0p6f4k92d48bzazd45kg0l0nxf5qz4cwh") (yanked #t)))

(define-public crate-com-croftsoft-lib-role-0.1 (crate (name "com-croftsoft-lib-role") (vers "0.1.2") (hash "1darnwn54ikw2sclc9m7c1arnw4d9apjr1wryhzyid775qbvad4z") (yanked #t)))

(define-public crate-com-croftsoft-lib-role-0.1 (crate (name "com-croftsoft-lib-role") (vers "0.1.3") (hash "099bx38k9kfg3xhsjhc417a7vz7yli06wlv4m691ir0gqxv8fl4b")))

(define-public crate-com-croftsoft-lib-role-0.1 (crate (name "com-croftsoft-lib-role") (vers "0.1.4") (hash "19fmadyzrn1qxz5hr64mhgkww46srv1h1xpd69z44w6343732hwq")))

(define-public crate-com-croftsoft-lib-role-0.2 (crate (name "com-croftsoft-lib-role") (vers "0.2.0") (hash "1q35kmr2qndyllhzkh5ld9j9f1krk9nhf8m0rnnxlczgwq20sc7p")))

(define-public crate-com-croftsoft-lib-role-0.3 (crate (name "com-croftsoft-lib-role") (vers "0.3.0") (hash "0pxjy22a9pw0acmh7zgwd88m5kirp179zqy2i16bbnk5qp1565cn")))

(define-public crate-com-croftsoft-lib-role-0.4 (crate (name "com-croftsoft-lib-role") (vers "0.4.0") (hash "09w78frv67c34zmhfw01drvadf58fffsrvmyapdm5ill69n0w6v4")))

(define-public crate-com-croftsoft-lib-role-0.4 (crate (name "com-croftsoft-lib-role") (vers "0.4.1") (hash "1hcy727d0m0zafahrz9061m7mfmd7v1pia8shp6825vxcg8ln3yw")))

(define-public crate-com-croftsoft-lib-role-0.5 (crate (name "com-croftsoft-lib-role") (vers "0.5.0") (hash "1nppmz0nii2rrffg21vxcis30hpw2wgjqm676ibrc9w7ymxnjr1b")))

(define-public crate-com-croftsoft-lib-role-0.5 (crate (name "com-croftsoft-lib-role") (vers "0.5.1") (hash "0q2hfx16wha628696avda0r777irmx4r93dv9n4yncldi5mf3kzh")))

(define-public crate-com-croftsoft-lib-string-0.1 (crate (name "com-croftsoft-lib-string") (vers "0.1.0") (hash "0h7br29f43z822lcvxnmkiy8hvlbmqy4rq0ydmzb9y66w52inl2b")))

(define-public crate-com-fun-0.0.1 (crate (name "com-fun") (vers "0.0.1") (deps (list (crate-dep (name "chardetng") (req "^0.1.17") (default-features #t) (kind 0)))) (hash "08f63vp7m85mxva4xzkibfww0738rsdqgq2gsj0pk928290f9mia") (yanked #t)))

(define-public crate-com-impl-0.1 (crate (name "com-impl") (vers "0.1.0-alpha1") (deps (list (crate-dep (name "winapi") (req "^0.3.6") (features (quote ("unknwnbase"))) (default-features #t) (target "cfg(windows)") (kind 0)) (crate-dep (name "wio") (req "^0.2.0") (default-features #t) (target "cfg(windows)") (kind 0)))) (hash "1v82cf7gy51pz91rl65y0qzlmp4x78vbr0zs6g072bliyfbc4igg")))

(define-public crate-com-impl-0.1 (crate (name "com-impl") (vers "0.1.0-alpha2") (deps (list (crate-dep (name "winapi") (req "^0.3.6") (features (quote ("unknwnbase"))) (default-features #t) (target "cfg(windows)") (kind 0)) (crate-dep (name "wio") (req "^0.2.0") (default-features #t) (target "cfg(windows)") (kind 0)))) (hash "1wvkbxz921qlrn0v7x2dw9w2b0lr27nn61zdwasxj5gljbinkxlc")))

(define-public crate-com-impl-0.1 (crate (name "com-impl") (vers "0.1.0") (deps (list (crate-dep (name "derive-com-impl") (req "^0.1.0") (default-features #t) (target "cfg(windows)") (kind 0)) (crate-dep (name "winapi") (req "^0.3.6") (features (quote ("unknwnbase"))) (default-features #t) (target "cfg(windows)") (kind 0)) (crate-dep (name "winapi") (req "^0.3.6") (features (quote ("dwrite" "winerror"))) (default-features #t) (kind 2)) (crate-dep (name "wio") (req "^0.2.0") (default-features #t) (target "cfg(windows)") (kind 0)))) (hash "19jrlav0jjjaz2ia5lg5qbvqgsn5kgq6ls9bcy1axbllpwgwym02")))

(define-public crate-com-impl-0.1 (crate (name "com-impl") (vers "0.1.1") (deps (list (crate-dep (name "derive-com-impl") (req "^0.1.0") (default-features #t) (target "cfg(windows)") (kind 0)) (crate-dep (name "winapi") (req "^0.3.6") (features (quote ("unknwnbase"))) (default-features #t) (target "cfg(windows)") (kind 0)) (crate-dep (name "winapi") (req "^0.3.6") (features (quote ("dwrite" "winerror"))) (default-features #t) (kind 2)) (crate-dep (name "wio") (req "^0.2.0") (default-features #t) (target "cfg(windows)") (kind 0)))) (hash "1gixqvnpm85frzii6j4xvrmy1v1y1bx7vgj1xq8h9vc0d73w5siq")))

(define-public crate-com-impl-0.2 (crate (name "com-impl") (vers "0.2.0") (deps (list (crate-dep (name "derive-com-impl") (req "^0.2.0") (default-features #t) (target "cfg(windows)") (kind 0)) (crate-dep (name "winapi") (req "^0.3.6") (features (quote ("unknwnbase"))) (default-features #t) (target "cfg(windows)") (kind 0)) (crate-dep (name "winapi") (req "^0.3.6") (features (quote ("dwrite" "winerror"))) (default-features #t) (kind 2)) (crate-dep (name "wio") (req "^0.2.0") (default-features #t) (target "cfg(windows)") (kind 0)))) (hash "1gw1ysv4f32b5nvyf57v4gm0fk7bsq5yhkl6nmpk9wsmfg87fw8h")))

(define-public crate-com-policy-config-0.1 (crate (name "com-policy-config") (vers "0.1.0") (deps (list (crate-dep (name "windows") (req "^0.44.0") (features (quote ("Win32_Foundation" "Win32_Media_Audio" "Win32_UI_Shell_PropertiesSystem" "Win32_System_Com_StructuredStorage" "Devices_Custom"))) (default-features #t) (kind 0)) (crate-dep (name "windows") (req "^0.44.0") (features (quote ("Win32_Devices_FunctionDiscovery"))) (default-features #t) (kind 2)))) (hash "0a2q25yjrh92kq8ps7z2x5sh9yd27kig1vxvrv8dy27w7b31mqfd")))

(define-public crate-com-policy-config-0.2 (crate (name "com-policy-config") (vers "0.2.0") (deps (list (crate-dep (name "windows") (req "^0.44.0") (features (quote ("Win32_Foundation" "Win32_Media_Audio" "Win32_UI_Shell_PropertiesSystem" "Win32_System_Com_StructuredStorage" "Devices_Custom"))) (default-features #t) (kind 0)) (crate-dep (name "windows") (req "^0.44.0") (features (quote ("Win32_Devices_FunctionDiscovery"))) (default-features #t) (kind 2)))) (hash "19lc0bzv0myi9q362s5gl958gsfvcha18k508jp7xg3migfmsrh4")))

(define-public crate-com-policy-config-0.3 (crate (name "com-policy-config") (vers "0.3.0") (deps (list (crate-dep (name "windows") (req "^0.48") (features (quote ("Win32_Foundation" "Win32_Media_Audio" "Win32_UI_Shell_PropertiesSystem" "Win32_System_Com_StructuredStorage" "Devices_Custom"))) (default-features #t) (kind 0)) (crate-dep (name "windows") (req "^0.48") (features (quote ("Win32_Devices_FunctionDiscovery"))) (default-features #t) (kind 2)))) (hash "0npfb88sbb619pifsj1m5089dm4fryv2yka3cnvw27chwzgmgkz7")))

(define-public crate-com-policy-config-0.4 (crate (name "com-policy-config") (vers "0.4.0") (deps (list (crate-dep (name "windows") (req "^0.51") (features (quote ("Win32_Foundation" "Win32_Media_Audio" "Win32_UI_Shell_PropertiesSystem" "Win32_System_Com_StructuredStorage" "Win32_System_Variant" "Devices_Custom"))) (default-features #t) (kind 0)) (crate-dep (name "windows") (req "^0.51") (features (quote ("Win32_Devices_FunctionDiscovery"))) (default-features #t) (kind 2)))) (hash "0579zv0q4b48csp2bchdcgiw6xqaqsilw3ybm0ahfxhvkn8835aj")))

(define-public crate-com-policy-config-0.5 (crate (name "com-policy-config") (vers "0.5.0") (deps (list (crate-dep (name "windows") (req "^0.52") (features (quote ("Win32_Foundation" "Win32_Media_Audio" "Win32_UI_Shell_PropertiesSystem" "Win32_System_Com_StructuredStorage" "Win32_System_Variant" "Devices_Custom"))) (default-features #t) (kind 0)) (crate-dep (name "windows") (req "^0.52") (features (quote ("Win32_Devices_FunctionDiscovery"))) (default-features #t) (kind 2)))) (hash "1hhhc38pyailf4v4ni7m5k1rp4ardvjmfb3sglcj38vyddpqxpkd")))

(define-public crate-com-rs-0.1 (crate (name "com-rs") (vers "0.1.0") (deps (list (crate-dep (name "libc") (req "^0.1.8") (default-features #t) (kind 0)) (crate-dep (name "winapi") (req "^0.1.20") (default-features #t) (kind 0)))) (hash "1v29aqiiq4kmqvzdwmk354105q7m5s8zgixpiw0xgdw4mhy9vjln")))

(define-public crate-com-rs-0.1 (crate (name "com-rs") (vers "0.1.1") (deps (list (crate-dep (name "libc") (req "^0.1.8") (default-features #t) (kind 0)) (crate-dep (name "winapi") (req "^0.1.20") (default-features #t) (kind 0)))) (hash "0jic20a9kwxfba0ln8a0dvpb47i15clmy9q6pp0p98kq0qc5a0vp")))

(define-public crate-com-rs-0.1 (crate (name "com-rs") (vers "0.1.2") (deps (list (crate-dep (name "libc") (req "^0.1.8") (default-features #t) (kind 0)) (crate-dep (name "winapi") (req "^0.1.20") (default-features #t) (kind 0)))) (hash "1m0i39gr3sfq8gxnjz509vvcljy14jaykgi7smvjlkakvb99hgsh")))

(define-public crate-com-rs-0.1 (crate (name "com-rs") (vers "0.1.3") (deps (list (crate-dep (name "winapi") (req "~0.2.0") (default-features #t) (kind 0)))) (hash "1pm5k93hikch9sslrl1sipiqkkhqn8fl86jw5vdqnw6b54w69pkz")))

(define-public crate-com-rs-0.1 (crate (name "com-rs") (vers "0.1.4") (hash "0yb69xppy1wvw1g2vqdgk9nshbqilpi71dg0ik1jj2db2qxb3md2")))

(define-public crate-com-rs-0.1 (crate (name "com-rs") (vers "0.1.5") (hash "0hbr1lbj8shil5j6916f98168nmkd0zxcd07l7hybq3g9s1p12mg")))

(define-public crate-com-rs-0.2 (crate (name "com-rs") (vers "0.2.0") (hash "0qrf5i1pvb1b4cc00j888908fai6npg671y435j38bnwz4qkj5lq")))

(define-public crate-com-rs-0.2 (crate (name "com-rs") (vers "0.2.1") (hash "0hk6051kwpabjs2dx32qkkpy0xrliahpqfh9df292aa0fv2yshxz")))

(define-public crate-com-scrape-0.1 (crate (name "com-scrape") (vers "0.1.0") (deps (list (crate-dep (name "clang-sys") (req "^1") (features (quote ("clang_6_0" "runtime"))) (default-features #t) (kind 0)))) (hash "1w4bbglm1v1rnakb9f25yiplg810ayw5aga134cghzv13h9i0f7v")))

(define-public crate-com-scrape-0.1 (crate (name "com-scrape") (vers "0.1.1") (deps (list (crate-dep (name "clang-sys") (req "^1") (features (quote ("clang_6_0" "runtime"))) (default-features #t) (kind 0)))) (hash "1paf1nw4hwl1g59if95vyrajk5pi8z3nbs133cdmb1ix27c20mw0")))

(define-public crate-com-scrape-0.1 (crate (name "com-scrape") (vers "0.1.2") (deps (list (crate-dep (name "clang-sys") (req "^1") (features (quote ("clang_6_0" "runtime"))) (default-features #t) (kind 0)))) (hash "1c6xm25j4qy1glzr24wzfwc7wss2v668vsh89q9bdgd5ismhi4dp")))

(define-public crate-com-scrape-0.1 (crate (name "com-scrape") (vers "0.1.3") (deps (list (crate-dep (name "clang-sys") (req "^1") (features (quote ("clang_6_0" "runtime"))) (default-features #t) (kind 0)))) (hash "0d8rfxv97g5ddxzzn8l90hvjv274r1abslzgmss7fvgvrjvqzslp")))

(define-public crate-com-scrape-types-0.1 (crate (name "com-scrape-types") (vers "0.1.0") (hash "00xlpxn75f2jzf3s8c3b3h4l944m4vy5d4cq6slfvi2fi9cc3ix5")))

(define-public crate-com-scrape-types-0.1 (crate (name "com-scrape-types") (vers "0.1.1") (hash "1l1slxb9mypf59wmqmyzczcgnmbjf414174i41m77z5qfnwq7qaw")))

(define-public crate-com-shim-0.1 (crate (name "com-shim") (vers "0.1.0") (deps (list (crate-dep (name "com-shim-macro") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "windows") (req "^0.48.0") (features (quote ("Win32_System_Com" "Win32_Foundation" "Win32_System_Ole"))) (default-features #t) (kind 0)))) (hash "1k7vwscrr8y4ph3sw4ysqkyrrznsadnzny6ydhfdnzdymqqrcgyy")))

(define-public crate-com-shim-0.1 (crate (name "com-shim") (vers "0.1.1") (deps (list (crate-dep (name "com-shim-macro") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "windows") (req "^0.48.0") (features (quote ("Win32_System_Com" "Win32_Foundation" "Win32_System_Ole"))) (default-features #t) (kind 0)))) (hash "1m2c9yjizbhz50j23axdr8pz078c4cbrr8xll6wd0n8f70wq0y43")))

(define-public crate-com-shim-0.1 (crate (name "com-shim") (vers "0.1.2") (deps (list (crate-dep (name "com-shim-macro") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "windows") (req "^0.48.0") (features (quote ("Win32_System_Com" "Win32_Foundation" "Win32_System_Ole"))) (default-features #t) (kind 0)))) (hash "1wf7am86kqvbmv7pvgqgfbxbb7v4gzml1cgas8vs6fcx58bsvja4")))

(define-public crate-com-shim-0.2 (crate (name "com-shim") (vers "0.2.1") (deps (list (crate-dep (name "com-shim-macro") (req "^0.2.1") (default-features #t) (kind 0)) (crate-dep (name "windows") (req "^0.48.0") (features (quote ("Win32_System_Com" "Win32_Foundation" "Win32_System_Ole"))) (default-features #t) (kind 0)))) (hash "0h4f2kbvqd5f0gybnpxr44fahs7snjlsl39mk1q4aj9aymq20w40")))

(define-public crate-com-shim-0.2 (crate (name "com-shim") (vers "0.2.2") (deps (list (crate-dep (name "com-shim-macro") (req "^0.2.2") (default-features #t) (kind 0)) (crate-dep (name "windows") (req "^0.48.0") (features (quote ("Win32_System_Com" "Win32_Foundation" "Win32_System_Ole"))) (default-features #t) (kind 0)))) (hash "0g8rc94k4735nw1mshbxylxl9cbif63r90ljfmv9syqp0kqawl7l")))

(define-public crate-com-shim-0.3 (crate (name "com-shim") (vers "0.3.0") (deps (list (crate-dep (name "com-shim-macro") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "windows") (req "^0.48.0") (features (quote ("Win32_System_Com" "Win32_Foundation" "Win32_System_Ole"))) (default-features #t) (kind 0)))) (hash "1drib5bafyyz5zc0ir55wshq0fk8z2l0lq1qk36m89v36s4dpl5s") (features (quote (("debug" "com-shim-macro/debug"))))))

(define-public crate-com-shim-0.3 (crate (name "com-shim") (vers "0.3.1") (deps (list (crate-dep (name "com-shim-macro") (req "^0.3.1") (default-features #t) (kind 0)) (crate-dep (name "windows") (req "^0.48.0") (features (quote ("Win32_System_Com" "Win32_Foundation" "Win32_System_Ole"))) (default-features #t) (kind 0)))) (hash "19ipyry9klyf4fvbcz2lrr03c32v4sr7bgn0961pfgxh7vn8xs2f") (features (quote (("debug" "com-shim-macro/debug"))))))

(define-public crate-com-shim-0.3 (crate (name "com-shim") (vers "0.3.2") (deps (list (crate-dep (name "com-shim-macro") (req "^0.3.2") (default-features #t) (kind 0)) (crate-dep (name "windows") (req "^0.48.0") (features (quote ("Win32_System_Com" "Win32_Foundation" "Win32_System_Ole"))) (default-features #t) (kind 0)))) (hash "1hviz5b2xcqpl6pqdqlj1cg555rh6sr2b3kg9cbkdmg8a72lmv0g") (features (quote (("debug" "com-shim-macro/debug"))))))

(define-public crate-com-shim-0.3 (crate (name "com-shim") (vers "0.3.3") (deps (list (crate-dep (name "com-shim-macro") (req "^0.3.3") (default-features #t) (kind 0)) (crate-dep (name "windows") (req "^0.48.0") (features (quote ("Win32_System_Com" "Win32_Foundation" "Win32_System_Ole"))) (default-features #t) (kind 0)))) (hash "15xvm54wmjdqv5wyhc53vddik2qnmfid971lbak7cxwqrj4ql6nl") (features (quote (("debug" "com-shim-macro/debug"))))))

(define-public crate-com-shim-0.3 (crate (name "com-shim") (vers "0.3.4") (deps (list (crate-dep (name "com-shim-macro") (req "^0.3.4") (default-features #t) (kind 0)) (crate-dep (name "windows") (req "^0.48.0") (features (quote ("Win32_System_Com" "Win32_Foundation" "Win32_System_Ole"))) (default-features #t) (kind 0)))) (hash "1x2zww38qwpqdfgdjk4pcmrqgn2jq1v1ar7bm3w8w93rcam8bal6") (features (quote (("debug" "com-shim-macro/debug"))))))

(define-public crate-com-shim-0.3 (crate (name "com-shim") (vers "0.3.5") (deps (list (crate-dep (name "com-shim-macro") (req "^0.3.5") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.20") (default-features #t) (kind 0)) (crate-dep (name "windows") (req "^0.51") (features (quote ("Win32_System_Variant" "Win32_System_Com" "Win32_Foundation" "Win32_System_Ole"))) (default-features #t) (kind 0)))) (hash "1avwlsq1i0zqh6c5xdwrkajcv7kx1m7kj6w87zsz4hcs9vpx4dwx") (features (quote (("debug" "com-shim-macro/debug"))))))

(define-public crate-com-shim-0.3 (crate (name "com-shim") (vers "0.3.6") (deps (list (crate-dep (name "com-shim-macro") (req "^0.3.5") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.20") (default-features #t) (kind 0)) (crate-dep (name "windows") (req "^0.51") (features (quote ("Win32_System_Variant" "Win32_System_Com" "Win32_Foundation" "Win32_System_Ole"))) (default-features #t) (kind 0)))) (hash "1b9ipjya9hffnjw92f329rik28p0lx7k1msr1hdicyr93554l0dx") (features (quote (("debug" "com-shim-macro/debug"))))))

(define-public crate-com-shim-macro-0.1 (crate (name "com-shim-macro") (vers "0.1.0") (deps (list (crate-dep (name "heck") (req "^0.4.1") (default-features #t) (kind 0)))) (hash "0iizkld6iq9mg8w59b5axka7h0m1p8y5jiszvjdws3kxmf1576gs")))

(define-public crate-com-shim-macro-0.1 (crate (name "com-shim-macro") (vers "0.1.1") (deps (list (crate-dep (name "heck") (req "^0.4.1") (default-features #t) (kind 0)))) (hash "1wc0af6mzd7hfg1fv7mhj2zdpisf6vay8djdr2gbfb5b8rsx7g9a")))

(define-public crate-com-shim-macro-0.1 (crate (name "com-shim-macro") (vers "0.1.2") (deps (list (crate-dep (name "heck") (req "^0.4.1") (default-features #t) (kind 0)))) (hash "15w1wmhbla829w3qzlfplcx9755w82afgw3asv2bdc8ryigwp8kk")))

(define-public crate-com-shim-macro-0.2 (crate (name "com-shim-macro") (vers "0.2.0") (deps (list (crate-dep (name "heck") (req "^0.4.1") (default-features #t) (kind 0)))) (hash "0z7wfnxpyzwpr526hd9pdhq8h13q7nbi7xfz7liajm8sjf8vmaai")))

(define-public crate-com-shim-macro-0.2 (crate (name "com-shim-macro") (vers "0.2.1") (deps (list (crate-dep (name "heck") (req "^0.4.1") (default-features #t) (kind 0)))) (hash "1wrsnh2fk83z6wxayrf25n4ks2inlipz2fx821765nnnqm5ln2rx")))

(define-public crate-com-shim-macro-0.2 (crate (name "com-shim-macro") (vers "0.2.2") (deps (list (crate-dep (name "heck") (req "^0.4.1") (default-features #t) (kind 0)))) (hash "1zc7plikwsww1ic5vk0a5sqip0yywjkg8a4kr7kz0asimw3c6aq1")))

(define-public crate-com-shim-macro-0.3 (crate (name "com-shim-macro") (vers "0.3.0") (deps (list (crate-dep (name "heck") (req "^0.4.1") (default-features #t) (kind 0)))) (hash "078jmiw4xrrk9h2qd727fwr5gchg0awl9k175vqqrrqjv2am9m7x") (features (quote (("debug"))))))

(define-public crate-com-shim-macro-0.3 (crate (name "com-shim-macro") (vers "0.3.1") (deps (list (crate-dep (name "heck") (req "^0.4.1") (default-features #t) (kind 0)))) (hash "1jd0db12fpg10yyjiqlzrnambgkzr84cwnjd8dvwps1c94193q8w") (features (quote (("debug"))))))

(define-public crate-com-shim-macro-0.3 (crate (name "com-shim-macro") (vers "0.3.2") (deps (list (crate-dep (name "heck") (req "^0.4.1") (default-features #t) (kind 0)))) (hash "1fpns2pgp9d7c6p7sz2106vkczrl8fng9srd3nk9vw3mpjh1ndyn") (features (quote (("debug"))))))

(define-public crate-com-shim-macro-0.3 (crate (name "com-shim-macro") (vers "0.3.3") (deps (list (crate-dep (name "heck") (req "^0.4.1") (default-features #t) (kind 0)))) (hash "0xa5053nlkszpwbj3mwmlil15z75siawfcf62iarvbxghmi73p86") (features (quote (("debug"))))))

(define-public crate-com-shim-macro-0.3 (crate (name "com-shim-macro") (vers "0.3.4") (deps (list (crate-dep (name "heck") (req "^0.4.1") (default-features #t) (kind 0)))) (hash "1s5naxqxj6w4krpxyib1lw0rddjalcgsla4p23wn2i6l79xj6a2c") (features (quote (("debug"))))))

(define-public crate-com-shim-macro-0.3 (crate (name "com-shim-macro") (vers "0.3.5") (deps (list (crate-dep (name "heck") (req "^0.4.1") (default-features #t) (kind 0)))) (hash "0d68fgv439fj2l1fs7yq0dxggpa7pwfxyg8v0frwr5g01xl7w52a") (features (quote (("debug"))))))

(define-public crate-com-shim-macro-0.3 (crate (name "com-shim-macro") (vers "0.3.6") (deps (list (crate-dep (name "heck") (req "^0.4.1") (default-features #t) (kind 0)))) (hash "1pa4phw71sp8sqr2gl2d08dc302fzibxkgpi33wna5h9d6p2gg8m") (features (quote (("debug"))))))

(define-public crate-com-wrapper-0.1 (crate (name "com-wrapper") (vers "0.1.0-alpha1") (deps (list (crate-dep (name "derive-com-wrapper") (req "^0.1.0-alpha1") (default-features #t) (kind 2)) (crate-dep (name "winapi") (req "^0.3.6") (default-features #t) (target "cfg(windows)") (kind 0)) (crate-dep (name "winapi") (req "^0.3.6") (features (quote ("unknwnbase"))) (default-features #t) (kind 2)) (crate-dep (name "wio") (req "^0.2.0") (default-features #t) (target "cfg(windows)") (kind 0)))) (hash "0a499vj7bvsg916xk9ayp9vjafcwh579hs2bcz8szh6sn523wan2")))

(define-public crate-com-wrapper-0.1 (crate (name "com-wrapper") (vers "0.1.0-alpha2") (deps (list (crate-dep (name "derive-com-wrapper") (req "^0.1.0-alpha4") (default-features #t) (kind 2)) (crate-dep (name "winapi") (req "^0.3.6") (default-features #t) (target "cfg(windows)") (kind 0)) (crate-dep (name "winapi") (req "^0.3.6") (features (quote ("unknwnbase"))) (default-features #t) (kind 2)) (crate-dep (name "wio") (req "^0.2.0") (default-features #t) (target "cfg(windows)") (kind 0)))) (hash "0012fqamkg7nl79bvr6jz6yiz64dk5viz1srwpj8bz6ld64wxc37")))

(define-public crate-com-wrapper-0.1 (crate (name "com-wrapper") (vers "0.1.0") (deps (list (crate-dep (name "derive-com-wrapper") (req "^0.1.0") (default-features #t) (target "cfg(windows)") (kind 0)) (crate-dep (name "winapi") (req "^0.3.6") (features (quote ("unknwnbase"))) (default-features #t) (target "cfg(windows)") (kind 0)) (crate-dep (name "wio") (req "^0.2.0") (default-features #t) (target "cfg(windows)") (kind 0)))) (hash "0w5vpdsjlkibf3xxwq382rsym9wbg0had3qky3hrvw90ac90jfvr")))

