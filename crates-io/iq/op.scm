(define-module (crates-io iq op) #:use-module (crates-io))

(define-public crate-iqoption-rs-0.0.2 (crate (name "iqoption-rs") (vers "0.0.2") (deps (list (crate-dep (name "rusty-hook") (req "^0.11.2") (default-features #t) (kind 2)) (crate-dep (name "tokio") (req "^1.29.1") (features (quote ("test-util"))) (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.29.1") (features (quote ("full"))) (default-features #t) (kind 2)))) (hash "06gplg8xs4mipcxpbns5gr4s59wmiyd7prvbwrn7j9sy2j628x7z") (rust-version "1.70.0")))

(define-public crate-iqoption-rs-0.1 (crate (name "iqoption-rs") (vers "0.1.0") (deps (list (crate-dep (name "rusty-hook") (req "^0.11.2") (default-features #t) (kind 2)) (crate-dep (name "tokio") (req "^1.29.1") (features (quote ("test-util"))) (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.29.1") (features (quote ("full"))) (default-features #t) (kind 2)))) (hash "1zah64xz80hcjdmmna33gbcjkss0v4z8q2kjfw07kaai0l9jd623") (yanked #t) (rust-version "1.70.0")))

