(define-module (crates-io iq km) #:use-module (crates-io))

(define-public crate-iqkms-0.0.0 (crate (name "iqkms") (vers "0.0.0") (hash "1i7cjqxy5hykdpfpva831grdg0y0rn57c9d2wsw762czh7amyk4g") (yanked #t)))

(define-public crate-iqkms-0.0.1 (crate (name "iqkms") (vers "0.0.1") (deps (list (crate-dep (name "proto") (req "^0.0.1") (default-features #t) (kind 0) (package "iqkms-proto")) (crate-dep (name "tokio") (req "^1") (features (quote ("full"))) (default-features #t) (kind 0)) (crate-dep (name "tonic") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "types") (req "^0.0.1") (optional #t) (default-features #t) (kind 0) (package "iqkms-types")))) (hash "1cg5fw1j46wia0hy5jg9f7h0k8nnhrwd0r4bzj7ffy3xsj090zhc") (features (quote (("ethereum" "types/ethereum")))) (rust-version "1.64")))

(define-public crate-iqkms-ethereum-0.0.0 (crate (name "iqkms-ethereum") (vers "0.0.0") (hash "0zj9rv6fylsy5mv9fy22jpcifiw16c83labvhb93ny4vqvipi8yn")))

(define-public crate-iqkms-ethereum-0.0.1 (crate (name "iqkms-ethereum") (vers "0.0.1") (deps (list (crate-dep (name "proto") (req "^0.0.1") (default-features #t) (kind 0) (package "iqkms-proto")) (crate-dep (name "signing") (req "^0.0.1") (features (quote ("ethereum"))) (default-features #t) (kind 0) (package "iqkms-signing")) (crate-dep (name "tonic") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "tracing") (req "^0.1.37") (default-features #t) (kind 0)) (crate-dep (name "types") (req "^0.0.1") (features (quote ("ethereum"))) (default-features #t) (kind 0) (package "iqkms-types")))) (hash "01xw51vaynpvq4vdppx1dqqym206bn4nmmgaj96b19lfqmp12cn1") (rust-version "1.64")))

(define-public crate-iqkms-keyring-0.0.0 (crate (name "iqkms-keyring") (vers "0.0.0") (hash "0qqvvlm5fn0gzyy4l8jzjqajsprwxzqpjhv9p2sa9rm1wvzg6xys")))

(define-public crate-iqkms-proto-0.0.0 (crate (name "iqkms-proto") (vers "0.0.0") (hash "1rscnlfpppkhi8j2hqxivhg6hgam3kihlkwvsrbrinsdr4n0n4ci")))

(define-public crate-iqkms-proto-0.0.1 (crate (name "iqkms-proto") (vers "0.0.1") (deps (list (crate-dep (name "prost") (req "^0.11") (default-features #t) (kind 0)) (crate-dep (name "tonic") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "tonic-build") (req "^0.8") (default-features #t) (kind 1)))) (hash "137krnhij713b537rdspak3x3gw0jvpyp283q3h8pjyp8h5ji2wh") (rust-version "1.64")))

(define-public crate-iqkms-signing-0.0.0 (crate (name "iqkms-signing") (vers "0.0.0") (hash "10wyrv8l70m90zb1qag5rnh5pxnnff66p61861w9673i5y151fn5")))

(define-public crate-iqkms-signing-0.0.1 (crate (name "iqkms-signing") (vers "0.0.1") (deps (list (crate-dep (name "crypto") (req "^0.0.1") (features (quote ("ecdsa" "getrandom" "sha2" "std"))) (default-features #t) (kind 0) (package "iq-crypto")) (crate-dep (name "types") (req "^0.0.1") (optional #t) (default-features #t) (kind 0) (package "iqkms-types")))) (hash "1jq43jil6jnr33gapm0fxv78x2l2bcy5bpsrq4zzriwi9g83lkyw") (features (quote (("secp256k1" "crypto/secp256k1") ("ethereum" "crypto/sha3" "secp256k1" "types/ethereum")))) (rust-version "1.64")))

(define-public crate-iqkms-types-0.0.0 (crate (name "iqkms-types") (vers "0.0.0") (hash "1v097a5j98bn4822ygd8zf0fx2syh6vj8qzd9wc6r04lm7l34hj3") (yanked #t)))

(define-public crate-iqkms-types-0.0.1 (crate (name "iqkms-types") (vers "0.0.1") (deps (list (crate-dep (name "crypto") (req "^0.0.1") (optional #t) (default-features #t) (kind 0) (package "iq-crypto")) (crate-dep (name "ethereum-types") (req "^0.13") (optional #t) (kind 0)) (crate-dep (name "hex") (req "^0.1") (features (quote ("alloc"))) (optional #t) (default-features #t) (kind 0) (package "base16ct")))) (hash "1vwvqmhcjaxshxqnmvagx42y8rv5lrq2wvgp678v32kxilih0g9j") (features (quote (("ethereum" "crypto/secp256k1" "crypto/sha3" "ethereum-types" "hex")))) (rust-version "1.64")))

(define-public crate-iqkmsd-0.0.0 (crate (name "iqkmsd") (vers "0.0.0") (hash "1hd5qjjq2byr7z51rivi23wvz9xd749xg4zbcsxqyl8ci4m2ia4c")))

(define-public crate-iqkmsd-0.0.1 (crate (name "iqkmsd") (vers "0.0.1") (deps (list (crate-dep (name "ethereum") (req "^0.0.1") (default-features #t) (kind 0) (package "iqkms-ethereum")) (crate-dep (name "proto") (req "^0.0.1") (default-features #t) (kind 0) (package "iqkms-proto")) (crate-dep (name "signing") (req "^0.0.1") (default-features #t) (kind 0) (package "iqkms-signing")) (crate-dep (name "tokio") (req "^1") (features (quote ("full"))) (default-features #t) (kind 0)) (crate-dep (name "tonic") (req "^0.8") (default-features #t) (kind 0)))) (hash "0qwbi70b84pwn95cwlbx182cyqbjwgf1why29awhssh5pzdzdcam") (rust-version "1.64")))

