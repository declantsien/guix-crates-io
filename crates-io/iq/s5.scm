(define-module (crates-io iq s5) #:use-module (crates-io))

(define-public crate-iqs5xx-0.1 (crate (name "iqs5xx") (vers "0.1.0") (deps (list (crate-dep (name "defmt") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "embedded-hal") (req "^0.2.5") (features (quote ("unproven"))) (default-features #t) (kind 0)) (crate-dep (name "paste") (req "^1.0.0") (default-features #t) (kind 0)))) (hash "1ll1p6rkac12yhr0vdw2c70iyyvnfmbl5gi0dhwgyplibshpi6wy")))

(define-public crate-iqs5xx-0.1 (crate (name "iqs5xx") (vers "0.1.1") (deps (list (crate-dep (name "defmt") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "embedded-hal") (req "^0.2.5") (features (quote ("unproven"))) (default-features #t) (kind 0)) (crate-dep (name "paste") (req "^1.0.0") (default-features #t) (kind 0)))) (hash "0iyvybsfsjl1zdga3hgv338g05pp9gmgw974v6r5llh6023dwav6")))

(define-public crate-iqs5xx-0.1 (crate (name "iqs5xx") (vers "0.1.2") (deps (list (crate-dep (name "defmt") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "embedded-hal") (req "^0.2.5") (features (quote ("unproven"))) (default-features #t) (kind 0)) (crate-dep (name "paste") (req "^1.0.0") (default-features #t) (kind 0)))) (hash "053w5kql8598l3cwr42raijjdrcknpx2s759cml4fml74ajs3c5w")))

