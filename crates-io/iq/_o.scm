(define-module (crates-io iq _o) #:use-module (crates-io))

(define-public crate-iq_osc-3 (crate (name "iq_osc") (vers "3.0.0") (deps (list (crate-dep (name "num-traits") (req "^0.1") (default-features #t) (kind 0)))) (hash "00q46wq0kjampg3nfkyc2icqjq7vm4qm6yssc6wi74k26nw5ki6f")))

