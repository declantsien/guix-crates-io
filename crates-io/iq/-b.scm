(define-module (crates-io iq -b) #:use-module (crates-io))

(define-public crate-iq-bech32-0.0.1 (crate (name "iq-bech32") (vers "0.0.1") (deps (list (crate-dep (name "clear_on_drop") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "failure") (req "^0.1") (default-features #t) (kind 0)))) (hash "0mqyj8053v22b0q75bgjvsg25pin6yzbl02m8v32v18ilx5qli27") (yanked #t)))

(define-public crate-iq-bech32-0.1 (crate (name "iq-bech32") (vers "0.1.0") (deps (list (crate-dep (name "clear_on_drop") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "failure") (req "^0.1") (default-features #t) (kind 0)))) (hash "0vshfp1blfbrvz848jdrvlsgfq2qj2bi9jf9z89s9a44nzvi4sy9") (yanked #t)))

(define-public crate-iq-bech32-0.1 (crate (name "iq-bech32") (vers "0.1.1") (deps (list (crate-dep (name "clear_on_drop") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "failure") (req "^0.1") (default-features #t) (kind 0)))) (hash "1vs8b4jkqvhjhdxpcikp7jizg64xa2qyaym2c52w28b6arklpyj8") (yanked #t)))

(define-public crate-iq-bech32-0.0.999 (crate (name "iq-bech32") (vers "0.0.999") (hash "13sw4zcaibr6vvdkhpn3cwswlg4380ahl15jjrnglpbqx7zw8wfv") (yanked #t)))

(define-public crate-iq-bech32-0.99 (crate (name "iq-bech32") (vers "0.99.99") (hash "1sm5wr2qhfz3011378smyfzzkcwrxbk89igw23c3qjipjgxn55d2") (yanked #t)))

