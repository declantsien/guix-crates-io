(define-module (crates-io u2 ff) #:use-module (crates-io))

(define-public crate-u2fframing-0.1 (crate (name "u2fframing") (vers "0.1.0") (deps (list (crate-dep (name "byteorder") (req "^1.3.2") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.8") (default-features #t) (kind 0)))) (hash "1ggijdmxi05ivjb9hxrv2kzla4901vsxjxc2hqvfwsnzafd6i0fx")))

