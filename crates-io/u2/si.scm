(define-module (crates-io u2 si) #:use-module (crates-io))

(define-public crate-u2size-0.1 (crate (name "u2size") (vers "0.1.0") (hash "0ypih8rvgk9ad0vapi08iwy5a5va2i97iar0crmnh0zgx50hhspp")))

(define-public crate-u2size-0.1 (crate (name "u2size") (vers "0.1.1") (hash "12s4pc9zxpjxz7yi954fw4hrx7rbwgpl3527r7xsf2kxv1jz9bqi")))

(define-public crate-u2size-0.1 (crate (name "u2size") (vers "0.1.2") (hash "0xr9jga8kwl21n8bh03ca5xnap7bpx24z5djinwq1fjzwym320na")))

(define-public crate-u2size-0.1 (crate (name "u2size") (vers "0.1.3") (hash "1d5sdj9s893syb5by8mbhhaglp27i5zp5pclqbbbdmn8gz6bdbg1")))

(define-public crate-u2size-0.1 (crate (name "u2size") (vers "0.1.4") (hash "1fkwlcij85fns99rv5qq0qj2l8jqj93ij14qka2g3nbddw47bl5i")))

