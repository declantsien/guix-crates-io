(define-module (crates-io u2 #{56}#) #:use-module (crates-io))

(define-public crate-u256-0.1 (crate (name "u256") (vers "0.1.0") (hash "025szkx2h33qc5j32yha4m6q1rpn4lvwkhqgmyj49hm3x91r6nsj")))

(define-public crate-u256-literal-0.1 (crate (name "u256-literal") (vers "0.1.0") (deps (list (crate-dep (name "primitive-types") (req "^0.9") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (default-features #t) (kind 0)))) (hash "04ipfj78bc1vl08zm4s8hqi5lafpdjg21l0ld1hv24qw1wc1br7p")))

(define-public crate-u256-literal-1 (crate (name "u256-literal") (vers "1.0.0") (deps (list (crate-dep (name "primitive-types") (req "^0.9") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (default-features #t) (kind 0)))) (hash "0xbaapz41wnr6k4mvi9kwx6q40y4jamzhmdwlfzlw3fp2cp917a5")))

(define-public crate-u256-literal-1 (crate (name "u256-literal") (vers "1.1.0") (deps (list (crate-dep (name "primitive-types") (req "^0.10") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (default-features #t) (kind 0)))) (hash "02z49mibjnx3j2k276ycbczi2rn2j6nljyx3iv9igjxsk9wpgnzq")))

(define-public crate-u256-literal-1 (crate (name "u256-literal") (vers "1.0.1") (deps (list (crate-dep (name "primitive-types") (req "^0.11") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (default-features #t) (kind 0)))) (hash "0bzfki0mrddxnhisvfm7vyp5hbwks88c0vhg5jbpbhp415layzr8")))

(define-public crate-u256-literal-1 (crate (name "u256-literal") (vers "1.0.2") (deps (list (crate-dep (name "primitive-types") (req "^0.11") (kind 0)) (crate-dep (name "syn") (req "^1.0") (default-features #t) (kind 0)))) (hash "17r31zagy296kdc2mi5vs0gvl3nhfxc28wrqmwzqjc3m7lgn3xvh")))

(define-public crate-u256-literal-1 (crate (name "u256-literal") (vers "1.1.2") (deps (list (crate-dep (name "primitive-types") (req "^0.11") (kind 0)) (crate-dep (name "syn") (req "^1.0") (default-features #t) (kind 0)))) (hash "1v5g9vh55cqvpz3jzqg7xs80vzl1q5n3bvanhbizw88p2646vn4w")))

(define-public crate-u256-literal-1 (crate (name "u256-literal") (vers "1.2.0") (deps (list (crate-dep (name "primitive-types") (req "^0.11") (kind 0)) (crate-dep (name "syn") (req "^1.0") (default-features #t) (kind 0)))) (hash "1x9bzpvyb3ya5w5dykl4asb3jc9calsy4wm4j6kxadm8hd20m9z1")))

(define-public crate-u256-literal-1 (crate (name "u256-literal") (vers "1.2.1") (deps (list (crate-dep (name "primitive-types") (req "^0.12") (kind 0)) (crate-dep (name "syn") (req "^1.0") (default-features #t) (kind 0)))) (hash "0y27xjnlmxibazjj2g3m2f5wzcqzp9nihfj1rzdpqp448bc1x6n3")))

