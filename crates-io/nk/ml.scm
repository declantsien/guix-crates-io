(define-module (crates-io nk ml) #:use-module (crates-io))

(define-public crate-nkml-0.0.1 (crate (name "nkml") (vers "0.0.1") (hash "1bx87j62v2sbzp8611wd31m0xp5svy3mfrby5k2kvgnciz5bppgh") (yanked #t)))

(define-public crate-nkml-0.0.2 (crate (name "nkml") (vers "0.0.2") (hash "0ikzs44lbfdpbzj71cwm1mf8nys7kc3vs4js7axb0014cnwgh409")))

