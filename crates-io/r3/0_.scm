(define-module (crates-io r3 #{0_}#) #:use-module (crates-io))

(define-public crate-r30_rs-0.1 (crate (name "r30_rs") (vers "0.1.0") (deps (list (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)))) (hash "095ryrnk9ppqmdxhqcigq18l46lrp8qy584y12hh5l53py0mvv0a")))

