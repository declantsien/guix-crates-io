(define-module (crates-io r3 ma) #:use-module (crates-io))

(define-public crate-r3ma_backup-0.1 (crate (name "r3ma_backup") (vers "0.1.0") (deps (list (crate-dep (name "data-encoding") (req "^2.2.1") (default-features #t) (kind 0)) (crate-dep (name "hex") (req "^0.4.2") (default-features #t) (kind 0)) (crate-dep (name "hmac") (req "^0.7.1") (default-features #t) (kind 0)) (crate-dep (name "pbkdf2") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.7.3") (default-features #t) (kind 0)) (crate-dep (name "salsa20") (req "^0.4.1") (default-features #t) (kind 0)) (crate-dep (name "sha2") (req "^0.8.2") (default-features #t) (kind 0)))) (hash "138k4bv4j91sah98d2hfh9fw5x14xd4xsp4rzmblv48r4188mcbq")))

