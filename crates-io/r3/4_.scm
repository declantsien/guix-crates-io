(define-module (crates-io r3 #{4_}#) #:use-module (crates-io))

(define-public crate-r34_api-0.2 (crate (name "r34_api") (vers "0.2.0") (deps (list (crate-dep (name "reqwest") (req "^0.12.0") (default-features #t) (kind 0)) (crate-dep (name "scraper") (req "^0.19.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.197") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.114") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.37.0") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "0pi42hzn2sl5ykfq6ja93vmi50382rbl42kscbp54vgh5icism4p") (yanked #t)))

(define-public crate-r34_api-0.2 (crate (name "r34_api") (vers "0.2.1") (deps (list (crate-dep (name "serde_json") (req "^1.0.114") (default-features #t) (kind 0)))) (hash "0lhk42mcar9j1vkv740spmw00i22jqxh7dfq6pbww20s8wdk5csh") (yanked #t)))

(define-public crate-r34_api-0.2 (crate (name "r34_api") (vers "0.2.4") (deps (list (crate-dep (name "serde_json") (req "^1.0.114") (default-features #t) (kind 0)))) (hash "0xib49fxj9c3d2l4gg38ppsp128qaacbhmf3sx2hnjpbzmdd24gg")))

(define-public crate-r34_api-0.2 (crate (name "r34_api") (vers "0.2.5") (deps (list (crate-dep (name "serde_json") (req "^1.0.114") (default-features #t) (kind 0)))) (hash "0nz6aajwns0nwkzl6b41lli6wy323lmsjiwzswp82s0qqllqp7cp")))

(define-public crate-r34_api-0.2 (crate (name "r34_api") (vers "0.2.6") (deps (list (crate-dep (name "serde_json") (req "^1.0.114") (default-features #t) (kind 0)))) (hash "1q4qfa2vl88lj4hscm2gipxq0h27kbk6755mwas2y1a1gyy2ks4q")))

(define-public crate-r34_api-0.2 (crate (name "r34_api") (vers "0.2.61") (deps (list (crate-dep (name "serde_json") (req "^1.0.114") (default-features #t) (kind 0)))) (hash "162xxrw96m0pmh7gmn489689ksbak8wrsh72y26bqbk9ydr3fbk8") (yanked #t)))

(define-public crate-r34_api-0.2 (crate (name "r34_api") (vers "0.2.62") (deps (list (crate-dep (name "serde_json") (req "^1.0.114") (default-features #t) (kind 0)))) (hash "13kjgh8hhwxf70ajigpyqwb22j5ryl597kk3a2s9ncibc75cdg2a")))

(define-public crate-r34_api-0.2 (crate (name "r34_api") (vers "0.2.7") (deps (list (crate-dep (name "serde_json") (req "^1.0.114") (default-features #t) (kind 0)))) (hash "0s777039lmlpgr5mp55fgrs6b1kd7n3rshrjd6qxk2dqjc0m2qg6") (yanked #t)))

(define-public crate-r34_api-0.3 (crate (name "r34_api") (vers "0.3.0") (deps (list (crate-dep (name "serde_json") (req "^1.0.114") (default-features #t) (kind 0)))) (hash "0hgy4qqvc9pbxnfrl2i1p753lc94cp18nz0cxrwgkakw7121i4jw")))

