(define-module (crates-io ls da) #:use-module (crates-io))

(define-public crate-lsdata-0.1 (crate (name "lsdata") (vers "0.1.0") (deps (list (crate-dep (name "implicit-clone") (req "^0.4.8") (default-features #t) (kind 0)) (crate-dep (name "indexmap") (req "^2.0.2") (default-features #t) (kind 0)) (crate-dep (name "utf8-chars") (req "^3.0.1") (default-features #t) (kind 0)))) (hash "03xq8i70jxx4s6sg84fr5wh4nm9al4a1p5h5xd3a887qidkkaqvx")))

