(define-module (crates-io ls b_) #:use-module (crates-io))

(define-public crate-lsb_png_steganography-0.1 (crate (name "lsb_png_steganography") (vers "0.1.0") (deps (list (crate-dep (name "image") (req "^0.20") (default-features #t) (kind 0)))) (hash "1h3208cblr921r2kqi6il23d79vf0kgw522zmka5vs7c0ip623bx")))

(define-public crate-lsb_release-0.1 (crate (name "lsb_release") (vers "0.1.0") (hash "04k3mxb355yww3nvyfr0gwpaxqkynrn93zi5fs9ljw0r6qyl924m")))

(define-public crate-lsb_text_png_steganography-0.1 (crate (name "lsb_text_png_steganography") (vers "0.1.0") (deps (list (crate-dep (name "image") (req "^0.20") (default-features #t) (kind 0)))) (hash "19wyjwsykwkqx76f8r5l1nzs3zd4bi65s5wk7xqsbm64r50ymkdy")))

(define-public crate-lsb_text_png_steganography-0.1 (crate (name "lsb_text_png_steganography") (vers "0.1.1") (deps (list (crate-dep (name "image") (req "^0.20") (default-features #t) (kind 0)))) (hash "1f3frgkdqv49sqrr35k0azfv0pyhzcqix58bbwlv2lpa8zh8v7ip")))

(define-public crate-lsb_text_png_steganography-0.1 (crate (name "lsb_text_png_steganography") (vers "0.1.2") (deps (list (crate-dep (name "image") (req "^0.20") (default-features #t) (kind 0)))) (hash "07vindi7h5hdfqdxsv7rxdhl8vggqsa4wdpzsi0z3ylqdm6wgbwf")))

