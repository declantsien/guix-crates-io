(define-module (crates-io ls ap) #:use-module (crates-io))

(define-public crate-lsap-1 (crate (name "lsap") (vers "1.0.0") (deps (list (crate-dep (name "approx") (req "^0.5.1") (default-features #t) (kind 2)) (crate-dep (name "ndarray") (req "^0.15.6") (default-features #t) (kind 2)))) (hash "0p7ygijlfi4bipxfk5br3x1vh3v8vhd0hzs893vypn76kidjxyyp")))

(define-public crate-lsap-1 (crate (name "lsap") (vers "1.0.1") (deps (list (crate-dep (name "approx") (req "^0.5.1") (default-features #t) (kind 2)) (crate-dep (name "ndarray") (req "^0.15.6") (default-features #t) (kind 2)))) (hash "060vb3s1vz8a0m20arscknans8laixvxzllg5l0f6b12f9165mj5")))

(define-public crate-lsap-1 (crate (name "lsap") (vers "1.0.2") (deps (list (crate-dep (name "approx") (req "^0.5.1") (default-features #t) (kind 2)) (crate-dep (name "derive_more") (req "^0.99.17") (default-features #t) (kind 0)) (crate-dep (name "ndarray") (req "^0.15.6") (default-features #t) (kind 2)))) (hash "0x57czjnvbf54q9g2442ab329fipdryi82x4cqmg4y32q9hf5x38")))

