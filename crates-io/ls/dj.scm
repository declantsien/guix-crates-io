(define-module (crates-io ls dj) #:use-module (crates-io))

(define-public crate-lsdj-0.1 (crate (name "lsdj") (vers "0.1.0") (deps (list (crate-dep (name "anyhow") (req "^1.0.57") (default-features #t) (kind 2)) (crate-dep (name "system-interface") (req "^0.20.0") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.30") (default-features #t) (kind 0)) (crate-dep (name "ux") (req "^0.1.4") (default-features #t) (kind 0)))) (hash "0lv70c9jmg2lzgxr71jf4q0rb7fzp3ljhhv3bp7mr5rrp2h14aa5")))

(define-public crate-lsdj-tools-0.1 (crate (name "lsdj-tools") (vers "0.1.0") (deps (list (crate-dep (name "anyhow") (req "^1.0.56") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^3.0.10") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "lsdj") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "walkdir") (req "^2.3.2") (default-features #t) (kind 0)) (crate-dep (name "wild") (req "^2.0.4") (default-features #t) (kind 0)))) (hash "0q0hn01adyfyxzbxxpbm2dc08z0m17lni77whfvsi5nfzdh1bscs")))

