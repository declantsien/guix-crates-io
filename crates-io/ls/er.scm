(define-module (crates-io ls er) #:use-module (crates-io))

(define-public crate-lser-0.1 (crate (name "lser") (vers "0.1.0") (deps (list (crate-dep (name "cli-table") (req "^0.4.6") (default-features #t) (kind 0)) (crate-dep (name "serial_enumerator") (req "^0.2.2") (default-features #t) (kind 0)))) (hash "0ywidm8x7s38gm7hlyyh05ag6gbxingg05k1jlw14pil409hq2sc")))

