(define-module (crates-io ls -t) #:use-module (crates-io))

(define-public crate-ls-tiny-0.1 (crate (name "ls-tiny") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^2.33.0") (default-features #t) (kind 0)) (crate-dep (name "colored") (req "^1.9.3") (default-features #t) (kind 0)))) (hash "009rcpq03c0iygv3jvhqkq6jagv9lfpzvxl926s7jb7zfiih2f2b")))

(define-public crate-ls-tiny-0.1 (crate (name "ls-tiny") (vers "0.1.1") (deps (list (crate-dep (name "clap") (req "^2.33.0") (default-features #t) (kind 0)) (crate-dep (name "colored") (req "^1.9.3") (default-features #t) (kind 0)))) (hash "03l9w0yd9vmnynhzhfirv1xyrysyn2js5kdv1zlmq3bdfd4r8pv5")))

(define-public crate-ls-tiny-0.1 (crate (name "ls-tiny") (vers "0.1.2") (deps (list (crate-dep (name "clap") (req "^2.33.0") (default-features #t) (kind 0)) (crate-dep (name "colored") (req "^1.9.3") (default-features #t) (kind 0)))) (hash "1rv23wmsfmaqz8s5yhkby5bhl46kgplbs8kzjdb361jnyvwfjwal")))

(define-public crate-ls-tiny-0.1 (crate (name "ls-tiny") (vers "0.1.3") (deps (list (crate-dep (name "clap") (req "^2.33.0") (default-features #t) (kind 0)) (crate-dep (name "colored") (req "^1.9.3") (default-features #t) (kind 0)))) (hash "1ybfxnasppvyhj8r959fk7mm18c8s7lyzp3amq7f9dnpq8hchv9p")))

(define-public crate-ls-tiny-0.1 (crate (name "ls-tiny") (vers "0.1.4") (deps (list (crate-dep (name "clap") (req "^2.33.0") (default-features #t) (kind 0)) (crate-dep (name "colored") (req "^1.9.3") (default-features #t) (kind 0)))) (hash "0f6kzxd55i3hr791vpn7rclfnl0nspy5n4r109zhx50haj6bsqsz")))

(define-public crate-ls-tiny-0.1 (crate (name "ls-tiny") (vers "0.1.5") (deps (list (crate-dep (name "clap") (req "^2.33.0") (default-features #t) (kind 0)) (crate-dep (name "colored") (req "^1.9.3") (default-features #t) (kind 0)))) (hash "1wlzaspsz0pgc09r8n1x8v3vym3r2fflzrrw883vdih89bdhx96b")))

(define-public crate-ls-tiny-0.1 (crate (name "ls-tiny") (vers "0.1.6") (deps (list (crate-dep (name "clap") (req "^2.33.0") (default-features #t) (kind 0)) (crate-dep (name "colored") (req "^1.9.3") (default-features #t) (kind 0)))) (hash "03ay0fpn2xfkjwj7750sfklnbvvcwg2qpb7khm7ym0hw6xbz9mlg")))

(define-public crate-ls-tiny-0.1 (crate (name "ls-tiny") (vers "0.1.7") (deps (list (crate-dep (name "clap") (req "^2.33.0") (default-features #t) (kind 0)) (crate-dep (name "colored") (req "^1.9.3") (default-features #t) (kind 0)))) (hash "0cx2jzsl1pdhr9dnz6s575lz7y0v8skp28ykbb40w7ypd9g8w6qi")))

