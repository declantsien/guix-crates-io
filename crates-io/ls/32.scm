(define-module (crates-io ls #{32}#) #:use-module (crates-io))

(define-public crate-ls32-hid-lib-0.1 (crate (name "ls32-hid-lib") (vers "0.1.0") (deps (list (crate-dep (name "bincode") (req "^1.2.1") (default-features #t) (kind 0)) (crate-dep (name "hidapi") (req "^1.1.0") (default-features #t) (kind 0)) (crate-dep (name "phf") (req "^0.8") (features (quote ("macros"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "18yai0xj4r2irfbk5mv85qqqfibw8a13s3inqxhy25rhn0ihrvg9")))

(define-public crate-ls32-hid-lib-0.2 (crate (name "ls32-hid-lib") (vers "0.2.0") (deps (list (crate-dep (name "bincode") (req "^1.2.1") (default-features #t) (kind 0)) (crate-dep (name "hidapi") (req "^1.1.0") (default-features #t) (kind 0)) (crate-dep (name "phf") (req "^0.8") (features (quote ("macros"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "13aa570xpcjx748j6g6641y3vwl910x1wrxsrxcqy4iqcv443v0n")))

(define-public crate-ls32-hid-lib-0.3 (crate (name "ls32-hid-lib") (vers "0.3.0") (deps (list (crate-dep (name "bincode") (req "^1.2.1") (default-features #t) (kind 0)) (crate-dep (name "hidapi") (req "^1.3.0") (default-features #t) (kind 0)) (crate-dep (name "phf") (req "^0.8") (features (quote ("macros"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "1qc1cqnw6pqvclxi5n3qaiz17vhh7lh4m2clnlzkgrhjc08sm6q4")))

