(define-module (crates-io ls _r) #:use-module (crates-io))

(define-public crate-ls_rules-0.1 (crate (name "ls_rules") (vers "0.1.0") (deps (list (crate-dep (name "serde") (req "^1") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1") (default-features #t) (kind 2)))) (hash "1vf569yd6b8n4a5y9vr6jg88kalx1x44mi4bgy20bgb8dkwjaidx") (yanked #t)))

(define-public crate-ls_rules-0.2 (crate (name "ls_rules") (vers "0.2.0") (deps (list (crate-dep (name "serde") (req "^1") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1") (default-features #t) (kind 2)))) (hash "0rl9ya3gxl1mvp48sgykj7glk2xjgsa42ni70l8yqdaw2qykxlr4") (yanked #t)))

(define-public crate-ls_rules-0.3 (crate (name "ls_rules") (vers "0.3.0") (deps (list (crate-dep (name "serde") (req "^1") (kind 0)) (crate-dep (name "serde_derive") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1") (default-features #t) (kind 2)))) (hash "0ak8i6b8lqaq5srlsjpmm6fw29cg6wgqk2jca35gnzdl04s23cg7") (features (quote (("std" "serde/std") ("default" "std") ("alloc" "serde/alloc")))) (yanked #t) (rust-version "1.56")))

(define-public crate-ls_rules-0.3 (crate (name "ls_rules") (vers "0.3.1") (deps (list (crate-dep (name "serde") (req "^1") (kind 0)) (crate-dep (name "serde_derive") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1") (default-features #t) (kind 2)))) (hash "0vyhina9iazsjlic7nfbx454aj1gxpi18i7n3cicmqk0l8ys7wrn") (features (quote (("std" "serde/std") ("default" "std") ("alloc" "serde/alloc")))) (yanked #t) (rust-version "1.56")))

(define-public crate-ls_rules-0.4 (crate (name "ls_rules") (vers "0.4.0") (deps (list (crate-dep (name "serde") (req "^1") (kind 0)) (crate-dep (name "serde_derive") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1") (default-features #t) (kind 2)))) (hash "1za9achx98misayp3acb0ccccwzqadnd191a4fw9zm3fyp453nkd") (features (quote (("std" "serde/std") ("default" "std") ("alloc" "serde/alloc")))) (rust-version "1.56.0")))

(define-public crate-ls_rules-0.4 (crate (name "ls_rules") (vers "0.4.1") (deps (list (crate-dep (name "serde") (req "^1.0.103") (features (quote ("derive"))) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.0") (default-features #t) (kind 2)))) (hash "008j2g0q9jxjhfxn5zj1lliy9m49ghl6w3b935mmz05g45k4hly5") (features (quote (("std" "serde/std") ("default" "std") ("alloc" "serde/alloc")))) (rust-version "1.56.0")))

