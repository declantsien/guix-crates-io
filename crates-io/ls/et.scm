(define-module (crates-io ls et) #:use-module (crates-io))

(define-public crate-lset-0.1 (crate (name "lset") (vers "0.1.0") (hash "16k6rv3lkrfbdqhdxcq7366x8zddfyy9pr2qq27nn0p8cahhxci3")))

(define-public crate-lset-0.2 (crate (name "lset") (vers "0.2.0") (hash "1c59qik34li9hji22qrl3qf07ifkrhqg9y0wwasp5c024wlfbspg")))

(define-public crate-lset-0.3 (crate (name "lset") (vers "0.3.0") (hash "1ddzd8skdspzrdglm0lkr3wg6b854k1z9qr5sw7my1cvbvnxh69r")))

