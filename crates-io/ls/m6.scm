(define-module (crates-io ls m6) #:use-module (crates-io))

(define-public crate-lsm6ds33-0.1 (crate (name "lsm6ds33") (vers "0.1.0") (deps (list (crate-dep (name "embedded-hal") (req "^0.2") (default-features #t) (kind 0)))) (hash "1059rmpr1g57ff35l98v21bf52cl548lrncvxfdzs2r2qd8mzsw0")))

(define-public crate-lsm6ds33-0.2 (crate (name "lsm6ds33") (vers "0.2.0") (deps (list (crate-dep (name "embedded-hal") (req "^0.2") (default-features #t) (kind 0)))) (hash "1k8pl265kmdsbkifdsn47ld2rg5lfmxfqa77lwj7fhj64s509y4n")))

(define-public crate-lsm6ds33-0.3 (crate (name "lsm6ds33") (vers "0.3.0") (deps (list (crate-dep (name "embedded-hal") (req "^0.2") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "embedded-hal-async") (req "^0.1.0-alpha.1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "maybe-async-cfg") (req "^0.2.1") (default-features #t) (kind 0)))) (hash "00f3wk4srvzh231y6qq5ppbbwvk7z5s5lyaf1n43vpfid1avbp90") (features (quote (("default" "blocking") ("blocking" "embedded-hal") ("async" "embedded-hal-async"))))))

(define-public crate-lsm6ds33-0.4 (crate (name "lsm6ds33") (vers "0.4.0") (deps (list (crate-dep (name "embedded-hal") (req "^0.2") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "embedded-hal-async") (req "^0.1.0-alpha.1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "maybe-async-cfg") (req "^0.2.1") (default-features #t) (kind 0)))) (hash "1kd3ly2y1a6m73090kx6462z4ibwrlnsxrpaqdpkjsg04rh0j2cz") (features (quote (("default" "blocking") ("blocking" "embedded-hal") ("async" "embedded-hal-async"))))))

(define-public crate-lsm6ds33-0.5 (crate (name "lsm6ds33") (vers "0.5.0") (deps (list (crate-dep (name "arrayref") (req "^0.3.6") (default-features #t) (kind 0)) (crate-dep (name "embedded-hal") (req "^0.2") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "embedded-hal-async") (req "^0.1.0-alpha.1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "maybe-async-cfg") (req "^0.2.1") (default-features #t) (kind 0)))) (hash "0zr2d81alfsswb6zsjhdvrg2zd6gl200swffivrwymfq5vi1p7l8") (features (quote (("default" "blocking") ("blocking" "embedded-hal") ("async" "embedded-hal-async"))))))

(define-public crate-lsm6ds33-rs-0.0.1 (crate (name "lsm6ds33-rs") (vers "0.0.1") (hash "0dd0s77kcfvqfdmiphv27ya78x0s69wfxki0wzh6dzcf26q026j9")))

(define-public crate-lsm6ds3tr-0.0.0 (crate (name "lsm6ds3tr") (vers "0.0.0") (hash "1aky5z0ik5baz6kbpgqilmrzvv98l0nq1bjxp2s2d8d978m6di2k")))

(define-public crate-lsm6ds3tr-0.1 (crate (name "lsm6ds3tr") (vers "0.1.0") (deps (list (crate-dep (name "defmt") (req "^0.3.2") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "embedded-hal") (req "^0.2.7") (default-features #t) (kind 0)))) (hash "0bvn9qjgcbwdk8z15aqnmnmgg7zm17ixjv9w9p9dkm9zmzap5fv2") (features (quote (("default")))) (v 2) (features2 (quote (("defmt" "dep:defmt"))))))

(define-public crate-lsm6ds3tr-0.1 (crate (name "lsm6ds3tr") (vers "0.1.1") (deps (list (crate-dep (name "defmt") (req "^0.3.5") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "embedded-hal") (req "^0.2.7") (default-features #t) (kind 0)))) (hash "11il4h6awvpdibxw6xz9pqkp6m5j981pda47s7yi52x1q5wv24ny") (features (quote (("default")))) (v 2) (features2 (quote (("defmt" "dep:defmt"))))))

(define-public crate-lsm6dso-0.1 (crate (name "lsm6dso") (vers "0.1.0") (deps (list (crate-dep (name "arrayref") (req "^0.3.6") (default-features #t) (kind 0)) (crate-dep (name "embedded-hal") (req "^0.2") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "embedded-hal-async") (req "^0.2.0-alpha.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "maybe-async-cfg") (req "^0.2.1") (default-features #t) (kind 0)))) (hash "0k0x5wgwn09k9axcjmdarlldz0hhjg8hkciv0pqdnf97f9hh8yk3") (features (quote (("default" "blocking") ("blocking" "embedded-hal") ("async" "embedded-hal-async"))))))

(define-public crate-lsm6dsox-1 (crate (name "lsm6dsox") (vers "1.0.1") (deps (list (crate-dep (name "accelerometer") (req "^0.12.0") (default-features #t) (kind 0)) (crate-dep (name "byteorder") (req "^1.4.3") (kind 0)) (crate-dep (name "embedded-hal") (req "^0.2.7") (default-features #t) (kind 0)) (crate-dep (name "embedded-hal-mock") (req "^0.8.0") (default-features #t) (kind 2)) (crate-dep (name "enumflags2") (req "^0.7.3") (default-features #t) (kind 0)) (crate-dep (name "measurements") (req "^0.11") (default-features #t) (kind 0)) (crate-dep (name "num_enum") (req "^0.5") (kind 0)))) (hash "1qwc4ki72xcd1gps34731aa1zzfdz7y0j1id8afs92dzlk0x45y4")))

(define-public crate-lsm6dsr-0.1 (crate (name "lsm6dsr") (vers "0.1.0") (deps (list (crate-dep (name "embedded-hal") (req "^1.0.0") (default-features #t) (kind 0)) (crate-dep (name "glam") (req "^0.27.0") (default-features #t) (kind 0)))) (hash "08f3nq9rklbv6wmf6b14c17p4vsk4rn3zichdlwpk7hbv1i4dkyv")))

