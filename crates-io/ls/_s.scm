(define-module (crates-io ls _s) #:use-module (crates-io))

(define-public crate-ls_solver-0.1 (crate (name "ls_solver") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^4.2.1") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "criterion") (req "^0.4.0") (features (quote ("html_reports"))) (default-features #t) (kind 2)) (crate-dep (name "nalgebra") (req "^0.32.2") (features (quote ("io" "sparse" "rand" "libm-force"))) (default-features #t) (kind 0)) (crate-dep (name "nalgebra-sparse") (req "^0.9.0") (features (quote ("io"))) (default-features #t) (kind 0)) (crate-dep (name "rayon") (req "^1.7.0") (default-features #t) (kind 0)))) (hash "1jv96qsgsqv7rxsn34cwqysgyvhks4sp5wkfmyw72g6yxh1kv8kc")))

