(define-module (crates-io ls -q) #:use-module (crates-io))

(define-public crate-ls-qpack-0.1 (crate (name "ls-qpack") (vers "0.1.0-alpha") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "ls-qpack-sys") (req "^0.1.0-alpha") (default-features #t) (kind 0)))) (hash "07f4a0a02yg4v2xyiiy837wifgkq8v1qf41qh5y0n3yqp6ac1i4x")))

(define-public crate-ls-qpack-0.1 (crate (name "ls-qpack") (vers "0.1.1-alpha") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "ls-qpack-sys") (req "^0.1.1-alpha") (default-features #t) (kind 0)))) (hash "16lbnaqy71yinbislrgb8xaijw23ifwqa18x40hg00xzd1njjcn3")))

(define-public crate-ls-qpack-0.1 (crate (name "ls-qpack") (vers "0.1.1") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "ls-qpack-sys") (req "^0.1.1") (default-features #t) (kind 0)))) (hash "15bz586adxqib9mjj551kxr2x1izyanmydkas8rxq5j76rlrllrb")))

(define-public crate-ls-qpack-0.1 (crate (name "ls-qpack") (vers "0.1.2") (deps (list (crate-dep (name "libc") (req "^0.2.146") (default-features #t) (kind 0)) (crate-dep (name "ls-qpack-sys") (req "^0.1.2") (default-features #t) (kind 0)))) (hash "1arb5l52rhdim3a0flsqj3lfn47i5ccqffm7psvxiw882ygc173w")))

(define-public crate-ls-qpack-0.1 (crate (name "ls-qpack") (vers "0.1.3") (deps (list (crate-dep (name "libc") (req "^0.2.146") (default-features #t) (kind 0)) (crate-dep (name "ls-qpack-sys") (req "^0.1.3") (default-features #t) (kind 0)))) (hash "01m6lyhnr0sylzsfh0qy1bqqqjks6bpij3f5ss8ki1wszr3yk7c6")))

(define-public crate-ls-qpack-0.1 (crate (name "ls-qpack") (vers "0.1.4") (deps (list (crate-dep (name "libc") (req "^0.2.146") (default-features #t) (kind 0)) (crate-dep (name "ls-qpack-sys") (req "^0.1.4") (default-features #t) (kind 0)))) (hash "1j2249ppx8qhzjbzry11kh42fv1x95r9nwzysivba0k1i77h2wad")))

(define-public crate-ls-qpack-sys-0.1 (crate (name "ls-qpack-sys") (vers "0.1.0-alpha") (deps (list (crate-dep (name "bindgen") (req "^0.59") (default-features #t) (kind 1)) (crate-dep (name "cmake") (req "^0.1") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2.134") (default-features #t) (kind 0)))) (hash "0p2x5w0fd05dqn2xcas0nn7gkfxzn0nhldxhyrks629vca9gf2h0")))

(define-public crate-ls-qpack-sys-0.1 (crate (name "ls-qpack-sys") (vers "0.1.1-alpha") (deps (list (crate-dep (name "bindgen") (req "^0.59") (default-features #t) (kind 1)) (crate-dep (name "cmake") (req "^0.1") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2.134") (default-features #t) (kind 0)))) (hash "1dygsg5rmpf0m465aws2my1sl5zcn755pk498v9xgk0ysz7n0hw7")))

(define-public crate-ls-qpack-sys-0.1 (crate (name "ls-qpack-sys") (vers "0.1.1") (deps (list (crate-dep (name "bindgen") (req "^0.59") (default-features #t) (kind 1)) (crate-dep (name "cmake") (req "^0.1") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2.134") (default-features #t) (kind 0)))) (hash "11j14bxahhw42k1zzn1z6g2qylamrmqc7xigzx7qzkpqbk4725n6")))

(define-public crate-ls-qpack-sys-0.1 (crate (name "ls-qpack-sys") (vers "0.1.2") (deps (list (crate-dep (name "bindgen") (req "^0.66.1") (default-features #t) (kind 1)) (crate-dep (name "cmake") (req "^0.1.50") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2.146") (default-features #t) (kind 0)))) (hash "0m9wbkxiqh7rzbji7m4dvw4nr22lf81v2jlmlznbr1n70dy36hdz")))

(define-public crate-ls-qpack-sys-0.1 (crate (name "ls-qpack-sys") (vers "0.1.3") (deps (list (crate-dep (name "bindgen") (req "^0.66.1") (default-features #t) (kind 1)) (crate-dep (name "cmake") (req "^0.1.50") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2.146") (default-features #t) (kind 0)))) (hash "0v7253vr71l5vy6w66g7psglnlpz2fnd60x9nz5m6mg4blv877qj")))

(define-public crate-ls-qpack-sys-0.1 (crate (name "ls-qpack-sys") (vers "0.1.4") (deps (list (crate-dep (name "bindgen") (req "^0.66.1") (default-features #t) (kind 1)) (crate-dep (name "cmake") (req "^0.1.50") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2.146") (default-features #t) (kind 0)))) (hash "1rnzxmv92v3i3h4v8vacv8pjk06mvdsdxx67vm5kfmvhp8nbz60k")))

