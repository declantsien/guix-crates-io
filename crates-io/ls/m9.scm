(define-module (crates-io ls m9) #:use-module (crates-io))

(define-public crate-lsm9ds1-0.1 (crate (name "lsm9ds1") (vers "0.1.0") (deps (list (crate-dep (name "embedded-hal") (req "^0.2.3") (default-features #t) (kind 0)))) (hash "1xx5d4k4z82pxfs0lwlbjkgz3zjk98w3pz493sk3g4gs2fbj400l")))

