(define-module (crates-io ls od) #:use-module (crates-io))

(define-public crate-lsode-0.1 (crate (name "lsode") (vers "0.1.0") (deps (list (crate-dep (name "cc") (req "^1.0") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "0rkw02f6y32vd9ng9bn3rccpx1y6agp8vy22izx908d9xvxhrsik")))

(define-public crate-lsode-0.1 (crate (name "lsode") (vers "0.1.1") (deps (list (crate-dep (name "cc") (req "^1.0") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "062j4qckjm0bm2ingjc69jjr42rhcsp74alxmcchcncmyqm19zk0")))

(define-public crate-lsode-0.1 (crate (name "lsode") (vers "0.1.2") (deps (list (crate-dep (name "cc") (req "^1.0") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "0ligjcfyjf0b5gqzcyqsnv31ia8kdd5ql4lbck9lki9x8w8rbh54")))

(define-public crate-lsode-0.1 (crate (name "lsode") (vers "0.1.3") (deps (list (crate-dep (name "cc") (req "^1.0") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "1zpbicfvklhsvingdh2qdzzs1fcm0c6306rdismapkd5vgy5pjzl")))

(define-public crate-lsode-0.2 (crate (name "lsode") (vers "0.2.0") (deps (list (crate-dep (name "cc") (req "^1.0") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "libffi") (req "^0.9") (default-features #t) (kind 0)))) (hash "1s3m1v2zxa08b8f3ynnf95ikhak14vanfjwqgcbb4v5irkakfjgf")))

(define-public crate-lsode-0.2 (crate (name "lsode") (vers "0.2.1") (deps (list (crate-dep (name "cc") (req "^1.0") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "libffi") (req "^0.9") (default-features #t) (kind 0)))) (hash "0ywfhpg0d8i7m0skfvp2wvsaf6j52hif34fshgsfpvyvnwc835n5")))

(define-public crate-lsode-0.2 (crate (name "lsode") (vers "0.2.2") (deps (list (crate-dep (name "cc") (req "^1.0") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "libffi") (req "^0.9") (default-features #t) (kind 0)))) (hash "08jz4yd6d7l8a5161fs4qya1xcbr5pwapiggynfmhvd192q35a88")))

(define-public crate-lsode-0.2 (crate (name "lsode") (vers "0.2.3") (deps (list (crate-dep (name "cc") (req "^1.0") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "libffi") (req "^0.9") (default-features #t) (kind 0)))) (hash "0cbj0qyn5adzpvadnrxrhn3gaf2l2xk0jwmf7lrcln8czqvc9bq1")))

(define-public crate-lsode-0.2 (crate (name "lsode") (vers "0.2.4") (deps (list (crate-dep (name "cc") (req "^1.0") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "libffi") (req "^0.9") (default-features #t) (kind 0)))) (hash "1zw9mlricgykz4yxhiki2h9kn7bh1adpclg9vcldgvsbcrqkqj97")))

(define-public crate-lsode-0.2 (crate (name "lsode") (vers "0.2.5") (deps (list (crate-dep (name "cc") (req "^1.0") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "libffi") (req "^0.9") (default-features #t) (kind 0)))) (hash "14can1kbrdrx9bdpwvcmjx2kkwnckr5hqm1mnbrhf5z8zp87l3xh")))

(define-public crate-lsode-0.2 (crate (name "lsode") (vers "0.2.6") (deps (list (crate-dep (name "cc") (req "^1.0") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "libffi") (req "^0.9") (default-features #t) (kind 0)))) (hash "0xc320syq01f0rsvlmcf57ax0qbphjkq57ya6ayyga9wf4gc2xsn")))

(define-public crate-lsode-0.2 (crate (name "lsode") (vers "0.2.7") (deps (list (crate-dep (name "cc") (req "^1.0") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "libffi") (req "^0.9") (default-features #t) (kind 0)))) (hash "0nw70xpgklq3wblmm5rhxjffkmrkrg4m8pzfs50w197znl135cy5")))

(define-public crate-lsode-0.2 (crate (name "lsode") (vers "0.2.8") (deps (list (crate-dep (name "cc") (req "^1.0") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "libffi") (req "^0.9") (default-features #t) (kind 0)))) (hash "14rm5dhhfyh3bj1sznjiggkkfjp3ldvsnq210q0z67y302xi3syf") (yanked #t)))

