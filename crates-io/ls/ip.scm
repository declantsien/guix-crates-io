(define-module (crates-io ls ip) #:use-module (crates-io))

(define-public crate-lsip-0.0.1 (crate (name "lsip") (vers "0.0.1") (deps (list (crate-dep (name "rayon") (req "^1.7.0") (default-features #t) (kind 0)))) (hash "16drqza2ysbmyl8wmjbafs8cf5y0z3bi1bzq5ly8ga5114xgx74x")))

(define-public crate-lsip-0.0.2 (crate (name "lsip") (vers "0.0.2") (deps (list (crate-dep (name "clap") (req "^4.1.8") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "mimalloc") (req "^0.1.34") (kind 0)) (crate-dep (name "rayon") (req "^1.7.0") (default-features #t) (kind 0)))) (hash "0r4n47hignah2salvgccwdx2rlzazxiqkmv0zgxzalzawlwn7grm")))

