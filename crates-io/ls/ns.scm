(define-module (crates-io ls ns) #:use-module (crates-io))

(define-public crate-lsns-0.0.1 (crate (name "lsns") (vers "0.0.1") (deps (list (crate-dep (name "argparse") (req "^0.2.1") (default-features #t) (kind 0)) (crate-dep (name "procinfo") (req "^0.3.1") (default-features #t) (kind 0)) (crate-dep (name "text_io") (req "^0.1.6") (default-features #t) (kind 0)) (crate-dep (name "unshare") (req "^0.1.14") (default-features #t) (kind 0)))) (hash "18c0ws91ixjp12c4qkkmmd946w7bda45vg41794829nx703cdqyk")))

(define-public crate-lsns-0.0.2 (crate (name "lsns") (vers "0.0.2") (deps (list (crate-dep (name "argparse") (req "^0.2.1") (default-features #t) (kind 0)) (crate-dep (name "nsutils") (req "^0.0.2") (default-features #t) (kind 0)) (crate-dep (name "procinfo") (req "^0.3.1") (default-features #t) (kind 0)) (crate-dep (name "text_io") (req "^0.1.6") (default-features #t) (kind 0)) (crate-dep (name "unshare") (req "^0.1.14") (default-features #t) (kind 0)))) (hash "1ldvhcmy5k8zcxdkjlyad7srxr4fhhsgqp63gq67f7nf04dnzbrl")))

(define-public crate-lsns-0.0.3 (crate (name "lsns") (vers "0.0.3") (deps (list (crate-dep (name "argparse") (req "^0.2.1") (default-features #t) (kind 0)) (crate-dep (name "nsutils") (req "0.*") (default-features #t) (kind 0)) (crate-dep (name "procinfo") (req "^0.3.1") (default-features #t) (kind 0)) (crate-dep (name "text_io") (req "^0.1.6") (default-features #t) (kind 0)) (crate-dep (name "unshare") (req "^0.1.14") (default-features #t) (kind 0)))) (hash "13ma8h7i5asx54d0sdj057dmckwdrx7nmqab85446i5bmz2xrlhz")))

