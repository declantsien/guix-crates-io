(define-module (crates-io ls eq) #:use-module (crates-io))

(define-public crate-lseq-0.1 (crate (name "lseq") (vers "0.1.0") (deps (list (crate-dep (name "num") (req "^0.1.42") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.4.2") (default-features #t) (kind 0)) (crate-dep (name "rug") (req "^1.0.1") (features (quote ("integer" "rand"))) (kind 0)) (crate-dep (name "skiplist") (req "^0.2.10") (default-features #t) (kind 0)) (crate-dep (name "uuid") (req "^0.6.2") (features (quote ("serde" "v4"))) (default-features #t) (kind 0)))) (hash "05pwp93fgphb1v90jrknc23gc8wwq81idl6f2hlz5wlb7m0aiyqw")))

