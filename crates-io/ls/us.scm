(define-module (crates-io ls us) #:use-module (crates-io))

(define-public crate-lsusb-0.1 (crate (name "lsusb") (vers "0.1.0") (deps (list (crate-dep (name "libusb") (req "^0.2.2") (default-features #t) (kind 0)))) (hash "144q4qq5zlr1hza3f5a1mf5pkm7ip8kjpgrfaviwy60q61vb3wba")))

