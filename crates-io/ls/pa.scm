(define-module (crates-io ls pa) #:use-module (crates-io))

(define-public crate-lspath-0.1 (crate (name "lspath") (vers "0.1.0") (hash "098vfsm208wsdwz8lnb9yx84dggdbxawwsj0xx1k4p18276hp791")))

(define-public crate-lspath-0.1 (crate (name "lspath") (vers "0.1.1") (hash "180r6b0gc4c9mh6pc8kqvblyych8xksk2kh3xfg2ykzkn770kf10")))

(define-public crate-lspath-0.1 (crate (name "lspath") (vers "0.1.3") (hash "1ac0wdq92vlhpaf7mcxzm8zk4ys3n74yv6rnpjn7nnj3vvxsxghz")))

(define-public crate-lspath-paged-0.1 (crate (name "lspath-paged") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^4.0.26") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "minus") (req "^5.1.0") (features (quote ("static_output" "search"))) (default-features #t) (kind 0)))) (hash "16f9p6y7x45483x1adjryfnncs1r4i1d2b8zrdnbydv7m56f3yaf") (rust-version "1.65")))

(define-public crate-lspath-paged-0.1 (crate (name "lspath-paged") (vers "0.1.1") (deps (list (crate-dep (name "clap") (req "^4.0.26") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "minus") (req "^5.1.0") (features (quote ("static_output" "search"))) (default-features #t) (kind 0)))) (hash "0kvzmgicym79vg0ngwdcsdc4jb53fsxs95isd5z07pxy7r49j97h") (rust-version "1.65")))

