(define-module (crates-io ls md) #:use-module (crates-io))

(define-public crate-lsmdb-0.2 (crate (name "lsmdb") (vers "0.2.0") (deps (list (crate-dep (name "rand") (req "^0.8") (default-features #t) (kind 2)))) (hash "0bvzjj4fafckpgqdszj6ds6k9mmnsb5bygsa6qyq2dqry3wj294v") (yanked #t)))

(define-public crate-lsmdb-0.1 (crate (name "lsmdb") (vers "0.1.0") (deps (list (crate-dep (name "rand") (req "^0.8") (default-features #t) (kind 2)))) (hash "1d9155c1jymn4wn6grhvnr3wis0i8hhhiv3ly7rjmasis30z4vvc") (yanked #t)))

(define-public crate-lsmdb-0.3 (crate (name "lsmdb") (vers "0.3.0") (deps (list (crate-dep (name "rand") (req "^0.8") (default-features #t) (kind 2)))) (hash "0mzwqcqcaajzhqs3jj57nsqa73fjlp9n4gjm6mii24b5mmprqcna")))

(define-public crate-lsmdb-0.4 (crate (name "lsmdb") (vers "0.4.0") (deps (list (crate-dep (name "bit-vec") (req "^0.6.3") (default-features #t) (kind 0)) (crate-dep (name "chrono") (req "^0.4.26") (default-features #t) (kind 0)))) (hash "148q1qssc5dkcl8zi9ns8vc7n1ih30yqm4ww7h161jinki9vx2zm")))

(define-public crate-lsmdb-0.4 (crate (name "lsmdb") (vers "0.4.1") (deps (list (crate-dep (name "bit-vec") (req "^0.6.3") (default-features #t) (kind 0)) (crate-dep (name "chrono") (req "^0.4.26") (default-features #t) (kind 0)))) (hash "0iy82vm1xsgkfm0328f2zf8jkgapdmai2llkzw9la36ggpnk53cj")))

(define-public crate-lsmdso-0.1 (crate (name "lsmdso") (vers "0.1.0") (deps (list (crate-dep (name "arrayref") (req "^0.3.6") (default-features #t) (kind 0)) (crate-dep (name "embedded-hal") (req "^0.2") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "embedded-hal-async") (req "^0.2.0-alpha.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "maybe-async-cfg") (req "^0.2.1") (default-features #t) (kind 0)))) (hash "19nydxmx7za79nmrskgqajvr5gi5m0ycdn9ajcajg7rvcgyhzl5s") (features (quote (("default" "blocking") ("blocking" "embedded-hal") ("async" "embedded-hal-async")))) (yanked #t)))

