(define-module (crates-io ls am) #:use-module (crates-io))

(define-public crate-lsamp-0.1 (crate (name "lsamp") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^3") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "duration-str") (req "^0.3.9") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1") (features (quote ("macros" "rt" "time" "io-std" "io-util"))) (default-features #t) (kind 0)))) (hash "1nnqqccimqc7lnlar585vk82kasnx91rfswh5yl1m7qbnqkl8xdv")))

(define-public crate-lsamp-0.1 (crate (name "lsamp") (vers "0.1.1") (deps (list (crate-dep (name "clap") (req "^3") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "duration-str") (req "^0.3.9") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1") (features (quote ("macros" "rt" "time" "io-std" "io-util"))) (default-features #t) (kind 0)))) (hash "00bnkmgcgbvz8pp7v8kxp50zvsfzv0pyjcw5cvqb8rqymfsbn698")))

(define-public crate-lsamp-0.1 (crate (name "lsamp") (vers "0.1.2") (hash "1ga3swwqywy8iwiyj5bxp52nv31laz7nvp8li2ksdmnaalbn7gyz")))

(define-public crate-lsamp-0.1 (crate (name "lsamp") (vers "0.1.3") (hash "1a44w610716g5vp4q1m5bcl9iyrrnr7ylpyh2fcs7b806igw2kp2")))

(define-public crate-lsamp-0.1 (crate (name "lsamp") (vers "0.1.4") (deps (list (crate-dep (name "rand") (req "^0.8") (default-features #t) (kind 2)))) (hash "035a2hhbr3w7kd47fhpllz3p6w8iha8g5vw97mdglsjqlbz9npsw")))

