(define-module (crates-io ls -o) #:use-module (crates-io))

(define-public crate-ls-option-0.1 (crate (name "ls-option") (vers "0.1.0") (hash "1b0w4idrsjd8sajqk6r2jfzh4r6v5gxm6qv4dh03bdimmn1rxp2x")))

(define-public crate-ls-option-0.1 (crate (name "ls-option") (vers "0.1.1") (hash "1cyiv1s6ayscv32b8a5xlifyzvrmj6f26ph7jr26cgvvvwqlpjyx")))

(define-public crate-ls-option-0.1 (crate (name "ls-option") (vers "0.1.2") (hash "0lnf86basb1zab8f6pybj16imwr5m3sarcv7ykhi854d0pwmx854")))

(define-public crate-ls-option-0.1 (crate (name "ls-option") (vers "0.1.3") (hash "0y1n962i895zmwc5dr0w5b09yqlm345cphayljcifnmyzd3s8nm6")))

(define-public crate-ls-option-0.1 (crate (name "ls-option") (vers "0.1.4") (hash "00difmig3lib0msar2zam7rdjvi3v11jlpgj87h29bxw5bvqagdh")))

(define-public crate-ls-option-0.1 (crate (name "ls-option") (vers "0.1.5") (hash "0nra6sn5rgxgqza6x6wkfz4j972672kcs83sa1g3py844fdnf463")))

