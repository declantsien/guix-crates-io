(define-module (crates-io ls ts) #:use-module (crates-io))

(define-public crate-lsts-0.0.1 (crate (name "lsts") (vers "0.0.1") (hash "0abc2qyglr96jxw27j8qxhicpq24xla1l3x19i7z5671g6pr7naw")))

(define-public crate-lsts-0.0.2 (crate (name "lsts") (vers "0.0.2") (hash "0iaa0ilgq0ml99jnhv0hl0jxcn5lrrg1wxq6j7fawid0k2gb0pbi")))

(define-public crate-lsts-0.0.3 (crate (name "lsts") (vers "0.0.3") (deps (list (crate-dep (name "syn") (req "^1.0") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "0ag9964qvygiaba2n5h9d1ml7ng0j7kj1z1ill3s6h74x0fmpr2z")))

(define-public crate-lsts-0.0.4 (crate (name "lsts") (vers "0.0.4") (deps (list (crate-dep (name "syn") (req "^1.0") (features (quote ("full"))) (optional #t) (default-features #t) (kind 0)))) (hash "1m0c5id2n2psh0fznkz1dj79ivmhqqps94335916imjxabzpgc8g") (features (quote (("rust" "syn"))))))

(define-public crate-lsts-0.0.5 (crate (name "lsts") (vers "0.0.5") (deps (list (crate-dep (name "syn") (req "^1.0") (features (quote ("full" "extra-traits"))) (optional #t) (default-features #t) (kind 0)))) (hash "02wna2i99hgf2kjx76qq1zlfn3ch9r7zhnsa74zx12mqivwnzmrc") (features (quote (("rust" "syn"))))))

(define-public crate-lsts-0.0.6 (crate (name "lsts") (vers "0.0.6") (deps (list (crate-dep (name "syn") (req "^1.0") (features (quote ("full" "extra-traits"))) (optional #t) (default-features #t) (kind 0)))) (hash "1dnyzgld92brnklwqg4snpak8k6amhh156s9kb8ylbmr006mbbxs") (features (quote (("rust" "syn"))))))

(define-public crate-lsts-0.0.7 (crate (name "lsts") (vers "0.0.7") (deps (list (crate-dep (name "pest") (req "^2.1.3") (default-features #t) (kind 0)) (crate-dep (name "pest_derive") (req "^2.1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (features (quote ("full" "extra-traits"))) (optional #t) (default-features #t) (kind 0)))) (hash "1jbslbya4a5bcb7n7bagv971nbf48w0ag2iskgjvk5jach9lzhdv") (features (quote (("rust" "syn"))))))

(define-public crate-lsts-0.0.8 (crate (name "lsts") (vers "0.0.8") (deps (list (crate-dep (name "pest") (req "^2.1.3") (default-features #t) (kind 0)) (crate-dep (name "pest_derive") (req "^2.1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (features (quote ("full" "extra-traits"))) (optional #t) (default-features #t) (kind 0)))) (hash "1l617q2bhx2a8888qdzhzafdx8vvw2bj7vfwd8w9a7prwwsrigjl") (features (quote (("rust" "syn"))))))

(define-public crate-lsts-0.0.9 (crate (name "lsts") (vers "0.0.9") (deps (list (crate-dep (name "pest") (req "^2.1.3") (default-features #t) (kind 0)) (crate-dep (name "pest_derive") (req "^2.1.0") (default-features #t) (kind 0)))) (hash "0rcgy6m4kwhjpi9s9c0k79p1mg2zyalxwmdkllgf0gg7qr45pzvj")))

(define-public crate-lsts-0.0.10 (crate (name "lsts") (vers "0.0.10") (deps (list (crate-dep (name "pest") (req "^2.1.3") (default-features #t) (kind 0)) (crate-dep (name "pest_derive") (req "^2.1.0") (default-features #t) (kind 0)))) (hash "166lj33892njc4za8f82z6vaqk8lb24lrskmbmn86zzhhdvndnsw")))

(define-public crate-lsts-0.0.11 (crate (name "lsts") (vers "0.0.11") (deps (list (crate-dep (name "pest") (req "^2.1.3") (default-features #t) (kind 0)) (crate-dep (name "pest_derive") (req "^2.1.0") (default-features #t) (kind 0)))) (hash "0zxlij20qrc5f8kyylaz67l6axvbfqy9l0vs8flmrn2jiva2wyn8")))

(define-public crate-lsts-0.0.12 (crate (name "lsts") (vers "0.0.12") (deps (list (crate-dep (name "pest") (req "^2.1.3") (default-features #t) (kind 0)) (crate-dep (name "pest_derive") (req "^2.1.0") (default-features #t) (kind 0)))) (hash "1iayv9v7qic39rfympcyy3jvskpylh90imracq4zgqa3cv0czawp")))

(define-public crate-lsts-0.0.13 (crate (name "lsts") (vers "0.0.13") (deps (list (crate-dep (name "pest") (req "^2.1.3") (default-features #t) (kind 0)) (crate-dep (name "pest_derive") (req "^2.1.0") (default-features #t) (kind 0)))) (hash "11bzihdz67ry7rp50rir3zpn370lszpfnylvl9yl9h08na206gpc")))

(define-public crate-lsts-0.0.14 (crate (name "lsts") (vers "0.0.14") (deps (list (crate-dep (name "pest") (req "^2.1.3") (default-features #t) (kind 0)) (crate-dep (name "pest_derive") (req "^2.1.0") (default-features #t) (kind 0)))) (hash "0ywkzh39s8wjx2x3avkmss9p4jzzc8hv0824ay3gvbkg8n3xxfw6")))

(define-public crate-lsts-0.0.15 (crate (name "lsts") (vers "0.0.15") (deps (list (crate-dep (name "pest") (req "^2.1.3") (default-features #t) (kind 0)) (crate-dep (name "pest_derive") (req "^2.1.0") (default-features #t) (kind 0)))) (hash "09kjyjmdgpzwqjl4lfbik49l5g3zv4asyf0hlgldw6sjzks1zc63")))

(define-public crate-lsts-0.0.16 (crate (name "lsts") (vers "0.0.16") (deps (list (crate-dep (name "pest") (req "^2.1.3") (default-features #t) (kind 0)) (crate-dep (name "pest_derive") (req "^2.1.0") (default-features #t) (kind 0)))) (hash "06d6g4zhbjjqvfiibg4bw6bv4w877rcnnpbcgxw7f8rmgs2pqkv1")))

(define-public crate-lsts-0.0.17 (crate (name "lsts") (vers "0.0.17") (deps (list (crate-dep (name "pest") (req "^2.1.3") (default-features #t) (kind 0)) (crate-dep (name "pest_derive") (req "^2.1.0") (default-features #t) (kind 0)))) (hash "1lzxv41pkqfmyxkascqwlv8wzadv7zlf5vjld6mllj9yrpc4851x")))

(define-public crate-lsts-0.0.18 (crate (name "lsts") (vers "0.0.18") (deps (list (crate-dep (name "pest") (req "^2.1.3") (default-features #t) (kind 0)) (crate-dep (name "pest_derive") (req "^2.1.0") (default-features #t) (kind 0)))) (hash "18549qwnfzxrnffldvd5llynb3l1zm0lyi3y8fc0b2kaal9yxhj8")))

(define-public crate-lsts-0.0.19 (crate (name "lsts") (vers "0.0.19") (deps (list (crate-dep (name "pest") (req "^2.1.3") (default-features #t) (kind 0)) (crate-dep (name "pest_derive") (req "^2.1.0") (default-features #t) (kind 0)))) (hash "1rkz3pzxlm1rys5j2ndii7km5r1hvhmf5ppm1386glyzl5r3ijzx")))

(define-public crate-lsts-0.0.20 (crate (name "lsts") (vers "0.0.20") (deps (list (crate-dep (name "pest") (req "^2.1.3") (default-features #t) (kind 0)) (crate-dep (name "pest_derive") (req "^2.1.0") (default-features #t) (kind 0)))) (hash "030a3yn36bcs38h4m9wp1b0gy8wicsvg99wjahaxr5115sgsw1q2")))

(define-public crate-lsts-0.0.21 (crate (name "lsts") (vers "0.0.21") (deps (list (crate-dep (name "pest") (req "^2.1.3") (default-features #t) (kind 0)) (crate-dep (name "pest_derive") (req "^2.1.0") (default-features #t) (kind 0)))) (hash "15abi5qbhvjiyv7slrl1pl7wj1xh75gbimchgjfxnyrxs0zrz2jn")))

(define-public crate-lsts-0.0.22 (crate (name "lsts") (vers "0.0.22") (deps (list (crate-dep (name "pest") (req "^2.1.3") (default-features #t) (kind 0)) (crate-dep (name "pest_derive") (req "^2.1.0") (default-features #t) (kind 0)))) (hash "1yi52c4yavi9gkjrg88crmnkin19sh8dhhsxvzl2jj9hq4xh594g")))

(define-public crate-lsts-0.0.23 (crate (name "lsts") (vers "0.0.23") (deps (list (crate-dep (name "pest") (req "^2.1.3") (default-features #t) (kind 0)) (crate-dep (name "pest_derive") (req "^2.1.0") (default-features #t) (kind 0)))) (hash "07ncf4yyzgjgqwgf8pz21c1qyd9a559qzv46fhzfy3qzbsljdlrm")))

(define-public crate-lsts-0.0.24 (crate (name "lsts") (vers "0.0.24") (deps (list (crate-dep (name "pest") (req "^2.1.3") (default-features #t) (kind 0)) (crate-dep (name "pest_derive") (req "^2.1.0") (default-features #t) (kind 0)))) (hash "1bp6l4gqx7h2alj2zbdc54pm60smj0h1gi350ynlfcmalxzadyn6")))

(define-public crate-lsts-0.0.25 (crate (name "lsts") (vers "0.0.25") (deps (list (crate-dep (name "pest") (req "^2.1.3") (default-features #t) (kind 0)) (crate-dep (name "pest_derive") (req "^2.1.0") (default-features #t) (kind 0)))) (hash "01w4pagw5rhl9an2n99z9251vlcrp45r4f0ryha45wl4kdqwj2nm")))

(define-public crate-lsts-0.0.26 (crate (name "lsts") (vers "0.0.26") (deps (list (crate-dep (name "pest") (req "^2.1.3") (default-features #t) (kind 0)) (crate-dep (name "pest_derive") (req "^2.1.0") (default-features #t) (kind 0)))) (hash "0lkfx3s1z3hyrqbk51ailp05bpzacxk5ga5nn4c2q357vnqcqwnn")))

(define-public crate-lsts-0.0.27 (crate (name "lsts") (vers "0.0.27") (deps (list (crate-dep (name "pest") (req "^2.1.3") (default-features #t) (kind 0)) (crate-dep (name "pest_derive") (req "^2.1.0") (default-features #t) (kind 0)))) (hash "0z4198pj1y9lryy90hajw24r6cdf9vb7v74nhg9fm0fcxihkzxj3")))

(define-public crate-lsts-0.0.28 (crate (name "lsts") (vers "0.0.28") (deps (list (crate-dep (name "pest") (req "^2.1.3") (default-features #t) (kind 0)) (crate-dep (name "pest_derive") (req "^2.1.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)))) (hash "03mnhia4nkh5dgkkdasmsmxby5y70jbg7bcq46hj8rwqsc6f5cca")))

(define-public crate-lsts-0.0.29 (crate (name "lsts") (vers "0.0.29") (deps (list (crate-dep (name "pest") (req "^2.1.3") (default-features #t) (kind 0)) (crate-dep (name "pest_derive") (req "^2.1.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)))) (hash "1andl3c5wz1v7cfjzsx1iiz03vsirbsxi9kwy162155ziwjyd9dl")))

(define-public crate-lsts-0.0.30 (crate (name "lsts") (vers "0.0.30") (deps (list (crate-dep (name "pest") (req "^2.1.3") (default-features #t) (kind 0)) (crate-dep (name "pest_derive") (req "^2.1.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)))) (hash "1pvxp5dx62z26d3x4a1c7rqgammjqhf50ygl4s9j8j0x1z5b2rnx")))

(define-public crate-lsts-0.0.31 (crate (name "lsts") (vers "0.0.31") (deps (list (crate-dep (name "pest") (req "^2.1.3") (default-features #t) (kind 0)) (crate-dep (name "pest_derive") (req "^2.1.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)))) (hash "042p0id2is1xh1lq64kfvwamnmd21pz6hyzqa843yfrqvbvx1wp0")))

(define-public crate-lsts-0.1 (crate (name "lsts") (vers "0.1.0") (deps (list (crate-dep (name "pest") (req "^2.1.3") (default-features #t) (kind 0)) (crate-dep (name "pest_derive") (req "^2.1.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)))) (hash "1jp55p4ijg3a9ncfb29frj4chq3psbmfxfbjplkp3jh5hmrqlrpy")))

(define-public crate-lsts-0.1 (crate (name "lsts") (vers "0.1.1") (deps (list (crate-dep (name "pest") (req "^2.1.3") (default-features #t) (kind 0)) (crate-dep (name "pest_derive") (req "^2.1.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)))) (hash "0b8slfl4949kxivnfp267fvn6422zikfvbd0nr7ldqh7zxf5qizz")))

(define-public crate-lsts-0.1 (crate (name "lsts") (vers "0.1.2") (deps (list (crate-dep (name "pest") (req "^2.1.3") (default-features #t) (kind 0)) (crate-dep (name "pest_derive") (req "^2.1.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)))) (hash "10dw6s28van6g2g6lllv937vr2840wam6a8fx3f4dgqxp6qf6446")))

(define-public crate-lsts-0.1 (crate (name "lsts") (vers "0.1.3") (deps (list (crate-dep (name "pest") (req "^2.1.3") (default-features #t) (kind 0)) (crate-dep (name "pest_derive") (req "^2.1.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)))) (hash "1wy82sm44ri7b6n6iih68pv6nlyxllbmyvrr4fzyjv9vgklav0l1")))

(define-public crate-lsts-0.1 (crate (name "lsts") (vers "0.1.4") (deps (list (crate-dep (name "pest") (req "^2.1.3") (default-features #t) (kind 0)) (crate-dep (name "pest_derive") (req "^2.1.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)))) (hash "0smw9gncsals2k1di1s4ngnghpp2fv7ngjsvzcwnlb0wmxg3wi7m")))

(define-public crate-lsts-0.1 (crate (name "lsts") (vers "0.1.5") (deps (list (crate-dep (name "pest") (req "^2.1.3") (default-features #t) (kind 0)) (crate-dep (name "pest_derive") (req "^2.1.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)))) (hash "120il4qbzygkkkd0m0c8shgqmyhkjm7sl5syai0blw11azdzcpi3")))

(define-public crate-lsts-0.1 (crate (name "lsts") (vers "0.1.6") (deps (list (crate-dep (name "pest") (req "^2.1.3") (default-features #t) (kind 0)) (crate-dep (name "pest_derive") (req "^2.1.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)))) (hash "1m42zl5688khh75816hgb2dn3xpc0lkhb6drzihw0d26zwdcclwi")))

(define-public crate-lsts-0.1 (crate (name "lsts") (vers "0.1.7") (deps (list (crate-dep (name "pest") (req "^2.1.3") (default-features #t) (kind 0)) (crate-dep (name "pest_derive") (req "^2.1.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)))) (hash "0vn1s0j9gwh2454wvzj5hzwpz0k5vr4cs1i30bx8pfx4bfvm9ady")))

(define-public crate-lsts-0.1 (crate (name "lsts") (vers "0.1.8") (deps (list (crate-dep (name "pest") (req "^2.1.3") (default-features #t) (kind 0)) (crate-dep (name "pest_derive") (req "^2.1.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)))) (hash "1rlj7iqsh7yjkh71wvyhxb3czn16xqban6mi91zy286d7l9vv0pc")))

(define-public crate-lsts-0.1 (crate (name "lsts") (vers "0.1.9") (deps (list (crate-dep (name "pest") (req "^2.1.3") (default-features #t) (kind 0)) (crate-dep (name "pest_derive") (req "^2.1.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)))) (hash "1af7cpl0v09hy77zfz63f1laka83jb5crlpjn39lz6y8c2civ54n")))

(define-public crate-lsts-0.1 (crate (name "lsts") (vers "0.1.10") (deps (list (crate-dep (name "pest") (req "^2.1.3") (default-features #t) (kind 0)) (crate-dep (name "pest_derive") (req "^2.1.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)))) (hash "18p2nxal76jk4nrm5a5skb5w6kmqhj6yw6kc052wsf47abcd7238")))

(define-public crate-lsts-0.1 (crate (name "lsts") (vers "0.1.11") (deps (list (crate-dep (name "pest") (req "^2.1.3") (default-features #t) (kind 0)) (crate-dep (name "pest_derive") (req "^2.1.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)))) (hash "0bvzqf066bda9rn4ygf0l5k4kdvxmyyfwap2cirwkb50i7cyh1nj")))

(define-public crate-lsts-0.2 (crate (name "lsts") (vers "0.2.0") (deps (list (crate-dep (name "pest") (req "^2.1.3") (default-features #t) (kind 0)) (crate-dep (name "pest_derive") (req "^2.1.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)))) (hash "0i5pw9wmlh7idqxpylm3igbh0a8wly9nlimqhpjm3gcj5b38a5kj")))

(define-public crate-lsts-0.2 (crate (name "lsts") (vers "0.2.1") (deps (list (crate-dep (name "pest") (req "^2.1.3") (default-features #t) (kind 0)) (crate-dep (name "pest_derive") (req "^2.1.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)))) (hash "1wnjw33givdj1a8bqjax323hfpshmpbzpb0g9yzk4f2wwqsz00vn")))

(define-public crate-lsts-0.2 (crate (name "lsts") (vers "0.2.2") (deps (list (crate-dep (name "pest") (req "^2.1.3") (default-features #t) (kind 0)) (crate-dep (name "pest_derive") (req "^2.1.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)))) (hash "0yaap9nljk8yrgdka9f96a673283pr4mlrynhk5wycm4yc8fnpza")))

(define-public crate-lsts-0.2 (crate (name "lsts") (vers "0.2.3") (deps (list (crate-dep (name "pest") (req "^2.1.3") (default-features #t) (kind 0)) (crate-dep (name "pest_derive") (req "^2.1.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)))) (hash "0a8qjw2k4a9pl0wkpw4djvqpc8wilibd160mflqsrf4ajijdi7as")))

(define-public crate-lsts-0.2 (crate (name "lsts") (vers "0.2.4") (deps (list (crate-dep (name "pest") (req "^2.1.3") (default-features #t) (kind 0)) (crate-dep (name "pest_derive") (req "^2.1.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)))) (hash "1sw2wqcrkni86n33n7ahjq4xg3qvryb4mq9jmxnkydp3nlz2r6k6")))

(define-public crate-lsts-0.3 (crate (name "lsts") (vers "0.3.0") (deps (list (crate-dep (name "pest") (req "^2.1.3") (default-features #t) (kind 0)) (crate-dep (name "pest_derive") (req "^2.1.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)))) (hash "0214f1d09zkbzkwsrfsssp12fwj3cfady4yxxxyxr23zagw1zj58")))

(define-public crate-lsts-0.3 (crate (name "lsts") (vers "0.3.1") (deps (list (crate-dep (name "pest") (req "^2.1.3") (default-features #t) (kind 0)) (crate-dep (name "pest_derive") (req "^2.1.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)))) (hash "1nsqrqfv20nvyi9hak0vr2zqalx3rpk11v4j0a5fnfpw2q2k1dsc")))

(define-public crate-lsts-0.3 (crate (name "lsts") (vers "0.3.2") (deps (list (crate-dep (name "pest") (req "^2.1.3") (default-features #t) (kind 0)) (crate-dep (name "pest_derive") (req "^2.1.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)))) (hash "0xdw91smsbpw6w0njl4lyqg1wnzf9wg0davnrcgmdlp3sq9npwd8")))

(define-public crate-lsts-0.3 (crate (name "lsts") (vers "0.3.3") (deps (list (crate-dep (name "pest") (req "^2.1.3") (default-features #t) (kind 0)) (crate-dep (name "pest_derive") (req "^2.1.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)))) (hash "0xw6b5q6rwsjjms45wpmn5q48axphbwgd64isbqcgb28d7jvw9r4")))

(define-public crate-lsts-0.3 (crate (name "lsts") (vers "0.3.4") (deps (list (crate-dep (name "pest") (req "^2.1.3") (default-features #t) (kind 0)) (crate-dep (name "pest_derive") (req "^2.1.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)))) (hash "0sz063irddgxzypi6vvpi4j3z9lfknxs5qb0xn87ffnf700avdbb")))

(define-public crate-lsts-0.4 (crate (name "lsts") (vers "0.4.0") (deps (list (crate-dep (name "pest") (req "^2.1.3") (default-features #t) (kind 0)) (crate-dep (name "pest_derive") (req "^2.1.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)))) (hash "03gsbhyjwhglcgygblgx052c565hpdbzzr477yi41m3cnvy896nh")))

(define-public crate-lsts-0.4 (crate (name "lsts") (vers "0.4.1") (deps (list (crate-dep (name "pest") (req "^2.1.3") (default-features #t) (kind 0)) (crate-dep (name "pest_derive") (req "^2.1.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)))) (hash "1yq2c039b2sl7x5h6il2d46hd2rsbmzghpx08xd7s1k64p9g5kin")))

(define-public crate-lsts-0.4 (crate (name "lsts") (vers "0.4.2") (deps (list (crate-dep (name "pest") (req "^2.1.3") (default-features #t) (kind 0)) (crate-dep (name "pest_derive") (req "^2.1.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)))) (hash "1x7xdfiw1995zmzda8rn636bbbvql48xx3vrm0awigfra9md524f")))

(define-public crate-lsts-0.4 (crate (name "lsts") (vers "0.4.3") (deps (list (crate-dep (name "pest") (req "^2.1.3") (default-features #t) (kind 0)) (crate-dep (name "pest_derive") (req "^2.1.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)))) (hash "1xfiaxayjn7v0i2syqiq3bg67ykh162d0i4gdzv8x8vya24l5k6z")))

(define-public crate-lsts-0.4 (crate (name "lsts") (vers "0.4.4") (deps (list (crate-dep (name "pest") (req "^2.1.3") (default-features #t) (kind 0)) (crate-dep (name "pest_derive") (req "^2.1.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)))) (hash "1pj01qbbwysi0d96xdzy34px8x0ghks1jy4lj5ff9as3hsgvnnhj")))

(define-public crate-lsts-0.4 (crate (name "lsts") (vers "0.4.5") (deps (list (crate-dep (name "pest") (req "^2.1.3") (default-features #t) (kind 0)) (crate-dep (name "pest_derive") (req "^2.1.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)))) (hash "10b6y19khh24cpl7nx15rbnvdrc3awxgzachyfa1myimk7mirshv")))

(define-public crate-lsts-0.4 (crate (name "lsts") (vers "0.4.6") (deps (list (crate-dep (name "pest") (req "^2.1.3") (default-features #t) (kind 0)) (crate-dep (name "pest_derive") (req "^2.1.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)))) (hash "1h4n6k5n10g24h69g6q5ymzpwlpgc7a6q2hzhq666jnf3sgzli5k")))

(define-public crate-lsts-0.5 (crate (name "lsts") (vers "0.5.0") (deps (list (crate-dep (name "pest") (req "^2.1.3") (default-features #t) (kind 0)) (crate-dep (name "pest_derive") (req "^2.1.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)))) (hash "1hb1cbgd4zz6qa6w23k0sqwpwc37vrwi1z2fyafazmcxirv1sh9w")))

(define-public crate-lsts-0.5 (crate (name "lsts") (vers "0.5.1") (deps (list (crate-dep (name "pest") (req "^2.1.3") (default-features #t) (kind 0)) (crate-dep (name "pest_derive") (req "^2.1.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)))) (hash "1jpyb7z30lsvp24lqb9cfa21bdmd462b0wmfxhj0fif8vj43g903")))

(define-public crate-lsts-0.5 (crate (name "lsts") (vers "0.5.2") (deps (list (crate-dep (name "pest") (req "^2.1.3") (default-features #t) (kind 0)) (crate-dep (name "pest_derive") (req "^2.1.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)))) (hash "1a220za8r8pb7cdl2wkr6nnk19q9mk1pv5h8rcnfqp2i3h7b0y6x")))

(define-public crate-lsts-0.5 (crate (name "lsts") (vers "0.5.3") (deps (list (crate-dep (name "pest") (req "^2.1.3") (default-features #t) (kind 0)) (crate-dep (name "pest_derive") (req "^2.1.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)))) (hash "1nlswiyahm6h053nvv8sw61mr5kzhr2vjrxgh234d69g5gls5zia")))

(define-public crate-lsts-0.5 (crate (name "lsts") (vers "0.5.4") (deps (list (crate-dep (name "pest") (req "^2.1.3") (default-features #t) (kind 0)) (crate-dep (name "pest_derive") (req "^2.1.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)))) (hash "0v413wh2j4nzinhjlyanin5x722rdcah7z53pdyknwp3bkxml9a0")))

(define-public crate-lsts-0.5 (crate (name "lsts") (vers "0.5.5") (deps (list (crate-dep (name "pest") (req "^2.1.3") (default-features #t) (kind 0)) (crate-dep (name "pest_derive") (req "^2.1.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)))) (hash "1hwvcs7gawjp9ygyxdh3zbg7qglwncw2xf4xgwfgwzf61fyvvd73")))

(define-public crate-lsts-0.5 (crate (name "lsts") (vers "0.5.6") (deps (list (crate-dep (name "pest") (req "^2.1.3") (default-features #t) (kind 0)) (crate-dep (name "pest_derive") (req "^2.1.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)))) (hash "0birkhjz1jirxg8vnff554xzmqvi349zli8k7lwni9kgkpy7r0sq")))

(define-public crate-lsts-0.5 (crate (name "lsts") (vers "0.5.7") (deps (list (crate-dep (name "pest") (req "^2.1.3") (default-features #t) (kind 0)) (crate-dep (name "pest_derive") (req "^2.1.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)))) (hash "1agdb7izcki2p9bcmxy2byzniimgz40dlh1wvyv0cyhjcr1lg46b")))

(define-public crate-lsts-0.5 (crate (name "lsts") (vers "0.5.8") (deps (list (crate-dep (name "pest") (req "^2.1.3") (default-features #t) (kind 0)) (crate-dep (name "pest_derive") (req "^2.1.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)))) (hash "1pnihdwycwbpglqjxl4r9wr60dbjv63xslq4hckbr5ykdak6mlc7")))

(define-public crate-lsts-0.5 (crate (name "lsts") (vers "0.5.9") (deps (list (crate-dep (name "pest") (req "^2.1.3") (default-features #t) (kind 0)) (crate-dep (name "pest_derive") (req "^2.1.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)))) (hash "1kjl8dgg0qvj2vxznzfd2k36p375qv2b6r5cnjhd03brksvvqgfw")))

(define-public crate-lsts-0.5 (crate (name "lsts") (vers "0.5.10") (deps (list (crate-dep (name "pest") (req "^2.1.3") (default-features #t) (kind 0)) (crate-dep (name "pest_derive") (req "^2.1.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)))) (hash "0d4sgx18z4hqddjs4hyxmrgbkh0m47jqm8v2wr97hsjjxwky559c")))

(define-public crate-lsts-0.5 (crate (name "lsts") (vers "0.5.11") (deps (list (crate-dep (name "pest") (req "^2.1.3") (default-features #t) (kind 0)) (crate-dep (name "pest_derive") (req "^2.1.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)))) (hash "094yjrc95jyj8abh4gjllgxwcfg2m7j9f8ll400cax4pjh56zv71")))

(define-public crate-lsts-0.5 (crate (name "lsts") (vers "0.5.12") (deps (list (crate-dep (name "pest") (req "^2.1.3") (default-features #t) (kind 0)) (crate-dep (name "pest_derive") (req "^2.1.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)))) (hash "11012xkzzipnrkqi2d2av7hzr8dc7j3zx422jd5ad3ig3k82z64l")))

(define-public crate-lsts-0.5 (crate (name "lsts") (vers "0.5.13") (deps (list (crate-dep (name "pest") (req "^2.1.3") (default-features #t) (kind 0)) (crate-dep (name "pest_derive") (req "^2.1.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)))) (hash "1bpw3mb4crav824q1bz773irxgspna5wnjdbsx45l8gvdmzdj0s9")))

(define-public crate-lsts-0.5 (crate (name "lsts") (vers "0.5.14") (deps (list (crate-dep (name "pest") (req "^2.1.3") (default-features #t) (kind 0)) (crate-dep (name "pest_derive") (req "^2.1.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)))) (hash "0scf847qsdiz1s4a9v8rkadlbcjhfqf2l3dmdpyycmnpq7lkg78y")))

(define-public crate-lsts-0.5 (crate (name "lsts") (vers "0.5.15") (deps (list (crate-dep (name "pest") (req "^2.1.3") (default-features #t) (kind 0)) (crate-dep (name "pest_derive") (req "^2.1.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)))) (hash "1jbs23jhvq5c8hnra128bnqb0x81qsdxmc0z67rr5pxx8pfhinlw")))

(define-public crate-lsts-0.5 (crate (name "lsts") (vers "0.5.16") (deps (list (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)))) (hash "18hvazd6livl92vg9sqy6f7r81rszl88vmwvczbczgrcqk71f5px")))

(define-public crate-lsts-0.5 (crate (name "lsts") (vers "0.5.17") (deps (list (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)))) (hash "0w50pd0g0bxijlyl55bxra60spw3iv6ljfcpikv2x4cgcr7hrhg0")))

(define-public crate-lsts-0.5 (crate (name "lsts") (vers "0.5.18") (deps (list (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)))) (hash "1xbw4w99qr45ic8jlv235mh0id5g4sh4h8kqdb2mzng14pz5fcrs")))

(define-public crate-lsts-0.5 (crate (name "lsts") (vers "0.5.19") (deps (list (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)))) (hash "12n6934c4lrdxyxzpja35j7vz37fkb1qifz8vy5pvkdf834lmla6")))

(define-public crate-lsts-0.5 (crate (name "lsts") (vers "0.5.20") (deps (list (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)))) (hash "1iznrrm2l87y4ak7x35m9fq4iivmig9rai2hkwxzxddb2dzwpmdh")))

(define-public crate-lsts-0.5 (crate (name "lsts") (vers "0.5.21") (deps (list (crate-dep (name "criterion") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)))) (hash "08h326mxnibq7hhyg1jdikpw1rw774kfvc01gy63rmpd3gh64cr5")))

(define-public crate-lsts-0.5 (crate (name "lsts") (vers "0.5.22") (deps (list (crate-dep (name "criterion") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)))) (hash "1hbznplmb0x0n1fyzkphpr3f69wkzzcj4j5nbkcngpfwy8zjc9gk")))

(define-public crate-lsts-0.5 (crate (name "lsts") (vers "0.5.23") (deps (list (crate-dep (name "criterion") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)))) (hash "01zd1clfq1lnbdx5vnpqx9rmgfplg0nrpkxhrsjajbwgd7q5dl1y")))

(define-public crate-lsts-0.5 (crate (name "lsts") (vers "0.5.24") (deps (list (crate-dep (name "criterion") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)))) (hash "160yqkcl8nqzs05syylvs10cs2p0b1ma65rmlkdx9f5v6y2inb4c")))

(define-public crate-lsts-0.5 (crate (name "lsts") (vers "0.5.25") (deps (list (crate-dep (name "criterion") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)))) (hash "0d371g818cp7i8vym1rpd5pbfb7m09g25j3rj7a203q5c5mxgnhm")))

(define-public crate-lsts-0.5 (crate (name "lsts") (vers "0.5.26") (deps (list (crate-dep (name "criterion") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)))) (hash "1vrly4pxylv6655fnc74fwx968j80dw5prnmzw66r6n9ki98cvxc")))

(define-public crate-lsts-0.5 (crate (name "lsts") (vers "0.5.27") (deps (list (crate-dep (name "criterion") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)))) (hash "06xl9hxh0xc2dn19jmiqhh76kp88r4fjv5snc1srxy50066mav0b")))

(define-public crate-lsts-0.5 (crate (name "lsts") (vers "0.5.28") (deps (list (crate-dep (name "criterion") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)))) (hash "0ha3lh5ar8mshrbqp5gnlv7abixs8p9wdp83mci11pg9dvi45iw7")))

(define-public crate-lsts-0.5 (crate (name "lsts") (vers "0.5.29") (deps (list (crate-dep (name "criterion") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)))) (hash "10c8b2025hdla75l0db4jkadd01qm8cljv4ly8nz0jrdxiwszv5n")))

(define-public crate-lsts-0.6 (crate (name "lsts") (vers "0.6.0") (deps (list (crate-dep (name "criterion") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)))) (hash "15f2avll4h904l4m2b6bijb17mkx8sfpq324csqiygh1kcavf05p")))

(define-public crate-lsts-0.6 (crate (name "lsts") (vers "0.6.1") (deps (list (crate-dep (name "criterion") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)))) (hash "1hxj70wq36ypayjscpcw9x2fsxhd11bzv4zhizfhf5h2h1ka2jzh")))

(define-public crate-lsts-0.6 (crate (name "lsts") (vers "0.6.2") (deps (list (crate-dep (name "criterion") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)))) (hash "0i84zab9acnpvr65vpcm5yf1f9mgznkfqds185rr83f7v2r1p1wp")))

(define-public crate-lsts-0.6 (crate (name "lsts") (vers "0.6.3") (deps (list (crate-dep (name "criterion") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)))) (hash "19h37x72y8lb1dc947j31xrbzlj98fs67fm7cykyn2yw1gkhw6vd")))

(define-public crate-lsts-0.6 (crate (name "lsts") (vers "0.6.4") (deps (list (crate-dep (name "criterion") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)))) (hash "09cp4xcs6mvsx5i22aj3aahr6lbxgbk2jv6x0vvivy1ls3y3bazz")))

(define-public crate-lsts-0.6 (crate (name "lsts") (vers "0.6.5") (deps (list (crate-dep (name "criterion") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)))) (hash "01k7i3v73vfcqslsp5q66xxns66a7zwqf2xsjyg3vcwacvyqpjz6")))

(define-public crate-lsts-0.6 (crate (name "lsts") (vers "0.6.6") (deps (list (crate-dep (name "criterion") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)))) (hash "052cni961cacwgqw0p6d3j3l33nd02hkj182l40kssrjv8lk0903")))

(define-public crate-lsts-0.6 (crate (name "lsts") (vers "0.6.7") (deps (list (crate-dep (name "criterion") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)))) (hash "18rjaz3afq7pvbm3mykgy0rnp8pclpfnnplnwxajsbnm6hw71dyh")))

(define-public crate-lsts-0.6 (crate (name "lsts") (vers "0.6.8") (deps (list (crate-dep (name "criterion") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)))) (hash "149cpbiidh29cdrmr91iasylzx6bwy07gzh9g0fk9srb3znc43r1")))

(define-public crate-lsts-0.6 (crate (name "lsts") (vers "0.6.9") (deps (list (crate-dep (name "criterion") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)))) (hash "11cskr9lcmriak87lbkhx75czn7w4lpwswvm6di5caibc6n08ym5")))

(define-public crate-lsts-0.6 (crate (name "lsts") (vers "0.6.10") (deps (list (crate-dep (name "criterion") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)))) (hash "1ys74251aw6f3bhjfhclkp2k7gsn5anxbjxfl9hk8aaz28kdb4hh")))

(define-public crate-lsts-0.6 (crate (name "lsts") (vers "0.6.11") (deps (list (crate-dep (name "criterion") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)))) (hash "1bs0ymy6pyy00nabzi6srpfcv54s20lpgx9aq5il89i8a8n50vx5")))

(define-public crate-lsts-0.6 (crate (name "lsts") (vers "0.6.12") (deps (list (crate-dep (name "criterion") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)))) (hash "0qkk2wrhi4kxni7c995hmfi2j77kl73gmng0pgkn9cvh5ldfsqgd")))

(define-public crate-lsts-0.6 (crate (name "lsts") (vers "0.6.13") (deps (list (crate-dep (name "criterion") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)))) (hash "12pn5yyv7ngmc2hpgnl65pybh9mp983p46xf4hkbnc1bivf8jhjy")))

(define-public crate-lsts-0.6 (crate (name "lsts") (vers "0.6.14") (deps (list (crate-dep (name "criterion") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)))) (hash "1z83srx1n5yhy4fgscrq7pa6r5l6hvwkzil084jrib92h76b251v")))

(define-public crate-lsts-0.6 (crate (name "lsts") (vers "0.6.15") (deps (list (crate-dep (name "criterion") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)))) (hash "16q66d26kfj3009qjmmznpqnzf1bacbppbk6gw95i4lizrz296rv")))

(define-public crate-lsts-0.6 (crate (name "lsts") (vers "0.6.16") (deps (list (crate-dep (name "criterion") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)))) (hash "13sj5v6y5fnhi3mfy9v73v3jx2gxbblz5zk0yb1w3wyjws29q5qf")))

(define-public crate-lsts-0.6 (crate (name "lsts") (vers "0.6.17") (deps (list (crate-dep (name "criterion") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)))) (hash "1aw0dnp9wv0y65nbzccr1dn47spvkk5rqykhz4d4qiz9qkykkgjp")))

(define-public crate-lsts-0.6 (crate (name "lsts") (vers "0.6.18") (deps (list (crate-dep (name "criterion") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)))) (hash "1kp9znfrfgs9y9zkisz4dg6xic8r5zd2iradx8ifwh34xgy5mlak")))

(define-public crate-lsts-0.6 (crate (name "lsts") (vers "0.6.19") (deps (list (crate-dep (name "criterion") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)))) (hash "0rlrihl7zv5rsjvy1b2r618iwi4w2ld06hrcmspczzvibzlg25qc")))

(define-public crate-lsts-0.6 (crate (name "lsts") (vers "0.6.20") (deps (list (crate-dep (name "criterion") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)))) (hash "0kxi6413xpzccqzmphzc5bclrfx4pv1kzxm0bgxfz2qdmhfbh807")))

(define-public crate-lsts-0.6 (crate (name "lsts") (vers "0.6.21") (deps (list (crate-dep (name "criterion") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "l1_ir") (req "^0.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)))) (hash "1yfxjgpc1j1ys60cz47adbncz6nlxzgrif9zlrqyfcgwvp3qbcj5")))

(define-public crate-lsts-0.6 (crate (name "lsts") (vers "0.6.22") (deps (list (crate-dep (name "criterion") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "l1_ir") (req "^0.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)))) (hash "1i6ap2pajjsil7w0zqdp855dry3gwfkwv06l22izkkdwzn3cdb07")))

(define-public crate-lsts-0.6 (crate (name "lsts") (vers "0.6.23") (deps (list (crate-dep (name "criterion") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "l1_ir") (req "^0.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)))) (hash "1658akxm0aljqackw2siw6yb4smap9zc3s3bqdfgsw3qz3cq03b1")))

(define-public crate-lsts-0.6 (crate (name "lsts") (vers "0.6.24") (deps (list (crate-dep (name "criterion") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "l1_ir") (req "^0.0.26") (features (quote ("cranelift"))) (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)))) (hash "1ny157x5rnisk0r8awnhasffnsfc1s76mnj2mvglamq0zgivvkkb")))

(define-public crate-lsts-0.6 (crate (name "lsts") (vers "0.6.25") (deps (list (crate-dep (name "criterion") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "l1_ir") (req "^0.0.27") (features (quote ("cranelift"))) (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)))) (hash "1pp1sp6s4w84iii3n4kp8y8pp8l5ads77h4fjwzy60s2k1bi99ci")))

(define-public crate-lsts-0.6 (crate (name "lsts") (vers "0.6.26") (deps (list (crate-dep (name "gag") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "l1_ir") (req "^0.0.41") (features (quote ("cranelift"))) (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)))) (hash "0h025nb8z7wfkkv2k5wvbyan3qkw4r25ibh0v0156437a244sciv")))

(define-public crate-lsts-0.6 (crate (name "lsts") (vers "0.6.27") (deps (list (crate-dep (name "gag") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "l1_ir") (req "^0.0.43") (features (quote ("cranelift"))) (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)))) (hash "10rargi17hrgp71r2mc8js8xj42iq829c5a0vihzh44raqyhvqar")))

(define-public crate-lsts-0.6 (crate (name "lsts") (vers "0.6.28") (deps (list (crate-dep (name "gag") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "l1_ir") (req "^0.0.47") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)))) (hash "14d5d7f1zy5w95s785x463byq7vzqgwhd0vyw83pdsmq3pvnjrrl")))

(define-public crate-lsts-0.6 (crate (name "lsts") (vers "0.6.29") (deps (list (crate-dep (name "gag") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "l1_ir") (req "^0.0.47") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)))) (hash "0p98mjxm64b00rnvlcb3hp1vcahszz3klzjl8iypimjj15a6dp7s")))

(define-public crate-lsts-0.6 (crate (name "lsts") (vers "0.6.30") (deps (list (crate-dep (name "gag") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "l1_ir") (req "^0.0.50") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)))) (hash "0ri3lffv1h27rns3r09kblzql4n2nf8s6wfzzsanh8bvrfasqq3m")))

(define-public crate-lsts-0.6 (crate (name "lsts") (vers "0.6.31") (deps (list (crate-dep (name "gag") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "l1_ir") (req "^0.0.51") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)))) (hash "1wvps2xz1iay908mpagjjkydb42r7vbpzv5r3pg4mn552j4r3l57")))

(define-public crate-lsts-0.6 (crate (name "lsts") (vers "0.6.32") (deps (list (crate-dep (name "gag") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "l1_ir") (req "^0.0.52") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)))) (hash "171dsd7gjdb57b5qgyx4kvjmdnzz1fmimrnlixbsmggpd4m882nx")))

(define-public crate-lsts-0.6 (crate (name "lsts") (vers "0.6.33") (deps (list (crate-dep (name "gag") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "l1_ir") (req "^0.0.52") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)))) (hash "0acinblm271yirlnxphm2ywf36v1h0j6wybrvswl8p3rdhgclj3w")))

(define-public crate-lsts-0.6 (crate (name "lsts") (vers "0.6.34") (deps (list (crate-dep (name "gag") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "lambda_mountain") (req "^0.0.16") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)))) (hash "1ynx759q7k0vbvg2imcqpnkvnxfwiqya5j3bl2jir0wy7qhcbdnj")))

(define-public crate-lstsq-0.1 (crate (name "lstsq") (vers "0.1.0") (deps (list (crate-dep (name "approx") (req "^0.5") (kind 2)) (crate-dep (name "nalgebra") (req "^0.28") (kind 0)))) (hash "0j001nfs6jn9ll56b2118b679bpcp5wyq6ci7g68zcfb3knsyivi") (features (quote (("std" "nalgebra/std") ("default" "std"))))))

(define-public crate-lstsq-0.2 (crate (name "lstsq") (vers "0.2.0") (deps (list (crate-dep (name "approx") (req "^0.5") (kind 2)) (crate-dep (name "nalgebra") (req "^0.29") (kind 0)))) (hash "19g3m9mkjbf5g9iif5ghgdxkdlg2n5as6jghfb3a7nrbarp9idnv") (features (quote (("std" "nalgebra/std") ("default" "std"))))))

(define-public crate-lstsq-0.3 (crate (name "lstsq") (vers "0.3.0") (deps (list (crate-dep (name "approx") (req "^0.5") (kind 2)) (crate-dep (name "nalgebra") (req "^0.30") (kind 0)))) (hash "07fg9wfvzgf1nmw11vdn7yqphc58wwacgkybyw50m563fx2ki26a") (features (quote (("std" "nalgebra/std") ("default" "std"))))))

(define-public crate-lstsq-0.4 (crate (name "lstsq") (vers "0.4.0") (deps (list (crate-dep (name "approx") (req "^0.5") (kind 2)) (crate-dep (name "nalgebra") (req "^0.31") (kind 0)))) (hash "1blapm4m1znx33gj0nci2gwikq211clyb3yaj23i29ga5jv84309") (features (quote (("std" "nalgebra/std") ("default" "std"))))))

(define-public crate-lstsq-0.5 (crate (name "lstsq") (vers "0.5.0") (deps (list (crate-dep (name "nalgebra") (req "^0.32") (kind 0)) (crate-dep (name "approx") (req "^0.5") (kind 2)))) (hash "1n3i8khygjnb8xw06b3bp8scas9b81xid5yxzf1l5dv9a32r29cb") (features (quote (("std" "nalgebra/std") ("default" "std"))))))

