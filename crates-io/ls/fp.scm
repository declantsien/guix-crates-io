(define-module (crates-io ls fp) #:use-module (crates-io))

(define-public crate-lsfp-0.4 (crate (name "lsfp") (vers "0.4.0") (hash "1rkj6vcj0q7crm6v149zw4bmckv3n6yyagg25bhb98fjn5m7fmms")))

(define-public crate-lsfp-0.4 (crate (name "lsfp") (vers "0.4.1") (hash "1f2q4pixclgmnykgc2rww8pp10dx4y8jffn9i89kwg38aiy9cv0q")))

(define-public crate-lsfp-0.4 (crate (name "lsfp") (vers "0.4.2") (hash "1hqd64if2dgvp0130ppadc2r7x5zan85qybi4ifwmfyfnpilc1fi") (features (quote (("git" "config") ("default" "git" "color") ("config") ("color" "config"))))))

(define-public crate-lsfp-0.4 (crate (name "lsfp") (vers "0.4.3") (deps (list (crate-dep (name "cc") (req "^1.0.66") (default-features #t) (kind 1)))) (hash "05afyxyfr2iswihkg3kd5ld76w8jrdds1p6d4ydi5w601vvxlgdb") (features (quote (("git" "config") ("default" "git" "color") ("config") ("color" "config"))))))

(define-public crate-lsfp-0.5 (crate (name "lsfp") (vers "0.5.0") (deps (list (crate-dep (name "cc") (req "^1.0.66") (default-features #t) (kind 1)))) (hash "11a4nq1z3x340sav6svnrhpkk9r9qxjpr8ggll7k876zjh1p19ni") (features (quote (("git" "config") ("default" "git" "color") ("config") ("color" "config"))))))

