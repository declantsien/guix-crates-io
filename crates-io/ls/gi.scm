(define-module (crates-io ls gi) #:use-module (crates-io))

(define-public crate-lsgit-0.1 (crate (name "lsgit") (vers "0.1.0") (hash "12m061gzz9fcb56ps9kzbm7ib9mmv1j2rx3f75ifg5h69cj4hjpm")))

(define-public crate-lsgit-0.1 (crate (name "lsgit") (vers "0.1.1") (hash "01m1rl178wgdkf8541mvccgy4bd0yilg1ffm9pndd49qjyhiwm58")))

(define-public crate-lsgit-0.1 (crate (name "lsgit") (vers "0.1.4") (hash "1rd7qsi0i01bgjbli0wl4i664ln934j37il1g54xpmpq55laa2f1")))

