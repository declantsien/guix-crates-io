(define-module (crates-io ls #{73}#) #:use-module (crates-io))

(define-public crate-ls7366-0.1 (crate (name "ls7366") (vers "0.1.0") (deps (list (crate-dep (name "bitfield") (req "^0.13.2") (default-features #t) (kind 0)) (crate-dep (name "embedded-hal") (req "^0.2.3") (default-features #t) (kind 0)) (crate-dep (name "embedded-hal-mock") (req "^0.7.1") (default-features #t) (kind 2)) (crate-dep (name "rppal") (req "^0.11.3") (features (quote ("hal"))) (default-features #t) (kind 2)))) (hash "1i297lzw0f1i2an04xj345l1c8gh5l1jcpvjcwcg59ddir8wz41j")))

(define-public crate-ls7366-0.2 (crate (name "ls7366") (vers "0.2.0") (deps (list (crate-dep (name "bitfield") (req "^0.13.2") (default-features #t) (kind 0)) (crate-dep (name "embedded-hal") (req "^0.2.3") (default-features #t) (kind 0)) (crate-dep (name "embedded-hal-mock") (req "^0.7.1") (default-features #t) (kind 2)) (crate-dep (name "rppal") (req "^0.11.3") (features (quote ("hal"))) (default-features #t) (kind 2)))) (hash "0wxbc19hn3mp1cib556gn8nflj03m1i110ssw0mfpb7cvyhirf8i")))

(define-public crate-ls7366-0.2 (crate (name "ls7366") (vers "0.2.1") (deps (list (crate-dep (name "bitfield") (req "^0.13.2") (default-features #t) (kind 0)) (crate-dep (name "embedded-hal") (req "^0.2.3") (default-features #t) (kind 0)) (crate-dep (name "embedded-hal-mock") (req "^0.7.1") (default-features #t) (kind 2)) (crate-dep (name "rppal") (req "^0.11.3") (features (quote ("hal"))) (default-features #t) (kind 2)))) (hash "00halyy3k353p0ijnhgf8x6agkvsdz5k7sdkami6vwlyvimfx0ng")))

