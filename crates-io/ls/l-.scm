(define-module (crates-io ls l-) #:use-module (crates-io))

(define-public crate-lsl-sys-0.1 (crate (name "lsl-sys") (vers "0.1.0") (deps (list (crate-dep (name "cmake") (req "^0.1.44") (default-features #t) (kind 1)))) (hash "16wwyw52g09hfjqjj8dkgklw8qk3ims6cbz16q6z8rwkz2ci2xpl") (links "lsl")))

(define-public crate-lsl-sys-0.1 (crate (name "lsl-sys") (vers "0.1.1") (deps (list (crate-dep (name "cmake") (req "^0.1.44") (default-features #t) (kind 1)))) (hash "19px9c8swhi96s1a1z7pvvvfk411ns8x6cw0lnxd364pdsz2fskl") (links "lsl")))

