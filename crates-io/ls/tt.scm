(define-module (crates-io ls tt) #:use-module (crates-io))

(define-public crate-lstty-0.1 (crate (name "lstty") (vers "0.1.0") (deps (list (crate-dep (name "log") (req "^0.4.20") (default-features #t) (kind 0)) (crate-dep (name "logging") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "pretty_env_logger") (req "^0.5.0") (default-features #t) (kind 0)) (crate-dep (name "serialport") (req "^4.2.2") (default-features #t) (kind 0)))) (hash "11c5rh254vf6ycxvd43jv3p7chb4h58c3k50876rdfck9qh4qml4")))

