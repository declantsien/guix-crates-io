(define-module (crates-io ls di) #:use-module (crates-io))

(define-public crate-lsdiff-rs-0.1 (crate (name "lsdiff-rs") (vers "0.1.1") (deps (list (crate-dep (name "clap") (req "^2.32.0") (default-features #t) (kind 0)) (crate-dep (name "failure") (req "^0.1.5") (default-features #t) (kind 0)))) (hash "1wjs5q0npm87ix6xwmsfvah4lzsyxssy8y305q6z1b3bv1ccpjw7")))

(define-public crate-lsdiff-rs-0.1 (crate (name "lsdiff-rs") (vers "0.1.2") (deps (list (crate-dep (name "clap") (req "^2.32.0") (default-features #t) (kind 0)) (crate-dep (name "failure") (req "^0.1.5") (default-features #t) (kind 0)))) (hash "0k5dnh3a3xis9j92h1zinm6y8xiamb39fz0mmmkc0a3wxk7dsg3f")))

