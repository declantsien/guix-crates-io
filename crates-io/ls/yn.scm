(define-module (crates-io ls yn) #:use-module (crates-io))

(define-public crate-lsynth-1 (crate (name "lsynth") (vers "1.0.0") (deps (list (crate-dep (name "rand") (req "^0.7.3") (default-features #t) (kind 0)) (crate-dep (name "rayon") (req "^1.5.1") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.144") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "0a71jhqc45ircbirz5d5synfn6am6z9cpda3xrwkflyxll433i15")))

(define-public crate-lsynth-1 (crate (name "lsynth") (vers "1.0.1") (deps (list (crate-dep (name "rand") (req "^0.7.3") (default-features #t) (kind 0)) (crate-dep (name "rayon") (req "^1.5.1") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.144") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "1s1bj1va6mprnghb14k3szjqjk95rlvx2vp1ag0bfhxzz1sn07d0")))

