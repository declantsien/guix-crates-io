(define-module (crates-io ls #{01}#) #:use-module (crates-io))

(define-public crate-ls010b7dh01-0.1 (crate (name "ls010b7dh01") (vers "0.1.0") (deps (list (crate-dep (name "embedded-hal") (req "^0.1.2") (default-features #t) (kind 0)) (crate-dep (name "nb") (req "^0.1.1") (default-features #t) (kind 0)))) (hash "104zxx1h78lr0m7a00m3b7rm22bcqzhx53hnv70v2aspfa8byx74")))

