(define-module (crates-io ls _c) #:use-module (crates-io))

(define-public crate-ls_clone-0.1 (crate (name "ls_clone") (vers "0.1.0") (hash "0zglfci5wijzjbz6720kfnvxmq63zzgd80n8rsd0s9mljbgajz0d")))

(define-public crate-ls_clone-0.1 (crate (name "ls_clone") (vers "0.1.1") (deps (list (crate-dep (name "colored") (req "^2.0.4") (default-features #t) (kind 0)))) (hash "0mjcp3wm67y55f965qzr9i0r1wp2skw73mfin2xjig5iz6b19566")))

