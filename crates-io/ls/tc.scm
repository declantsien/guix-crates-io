(define-module (crates-io ls tc) #:use-module (crates-io))

(define-public crate-lstc-calendar-0.1 (crate (name "lstc-calendar") (vers "0.1.0") (deps (list (crate-dep (name "chrono") (req "^0.4.34") (default-features #t) (kind 0)) (crate-dep (name "test-case") (req "^3.3.1") (default-features #t) (kind 2)))) (hash "1wx96f6gz7cvx79z6lcp295jjapa02qrcnx9fi2gbz49vrhim7w5")))

(define-public crate-lstc-calendar-0.1 (crate (name "lstc-calendar") (vers "0.1.1") (deps (list (crate-dep (name "chrono") (req "^0.4.34") (features (quote ("serde"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.196") (features (quote ("serde_derive"))) (default-features #t) (kind 0)) (crate-dep (name "test-case") (req "^3.3.1") (default-features #t) (kind 2)))) (hash "0897i35x4cbikfhpyn86y65c0b65v5wfyk6n3alqn4qspaa7mp8y")))

(define-public crate-lstc-calendar-0.2 (crate (name "lstc-calendar") (vers "0.2.0") (deps (list (crate-dep (name "chrono") (req "^0.4.34") (features (quote ("serde"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.196") (features (quote ("serde_derive"))) (default-features #t) (kind 0)) (crate-dep (name "test-case") (req "^3.3.1") (default-features #t) (kind 2)))) (hash "1klimiv9zbq8876r75vfwy2nw1g023pj7pifm4f1g3qnms3raksn")))

