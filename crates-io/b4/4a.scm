(define-module (crates-io b4 #{4a}#) #:use-module (crates-io))

(define-public crate-b44a9e20a2903987528588cf60116202-0.1 (crate (name "b44a9e20a2903987528588cf60116202") (vers "0.1.0") (deps (list (crate-dep (name "strum") (req "^0.24.0") (default-features #t) (kind 0)) (crate-dep (name "strum_macros") (req "^0.24.0") (default-features #t) (kind 0)))) (hash "02f2425fl6m02795qgd9kf8zg7xk9b2r03rqlfs523h5x4c5brx9") (yanked #t)))

