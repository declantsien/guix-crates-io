(define-module (crates-io rq tt) #:use-module (crates-io))

(define-public crate-rqtt-0.1 (crate (name "rqtt") (vers "0.1.0") (deps (list (crate-dep (name "async-std") (req "^1.2.0") (features (quote ("attributes"))) (default-features #t) (kind 0)) (crate-dep (name "bitreader_async") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "cargo-edit") (req "^0.3.3") (default-features #t) (kind 0)) (crate-dep (name "mqtt_5") (req "^0.1.1") (default-features #t) (kind 0)) (crate-dep (name "packattack") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "033ssja64612rng0ski25ywlpzmzhdnxyaqx7iv9ngl5p3biv4kp")))

