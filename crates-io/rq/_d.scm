(define-module (crates-io rq _d) #:use-module (crates-io))

(define-public crate-rq_derive-0.1 (crate (name "rq_derive") (vers "0.1.0") (deps (list (crate-dep (name "proc-macro2") (req "^0.4.27") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^0.6.12") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^0.15.30") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "1lpzkkazwsrlb2yj3nkb1mj2r36jpvaz1kj7iczaks7a80gj1zsm")))

(define-public crate-rq_derive-0.1 (crate (name "rq_derive") (vers "0.1.2") (deps (list (crate-dep (name "proc-macro2") (req "^0.4.27") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^0.6.12") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^0.15.30") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "0q49z4dz67dli9zb2kpfa8k9dmhbp3nyq4rjyfr0dipzga0gmyj0")))

