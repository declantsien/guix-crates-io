(define-module (crates-io rq ue) #:use-module (crates-io))

(define-public crate-rquery-0.1 (crate (name "rquery") (vers "0.1.0") (deps (list (crate-dep (name "xml-rs") (req "*") (default-features #t) (kind 0)))) (hash "1vb95n8avbp50n8grxkm7shqr5nw5gfgcn122ia9asxnkvk8m0is")))

(define-public crate-rquery-0.1 (crate (name "rquery") (vers "0.1.1") (deps (list (crate-dep (name "xml-rs") (req "*") (default-features #t) (kind 0)))) (hash "1716ri4dgxyzf02z77idgfpq8vn2gcwk0h4m3x29dxxpip7s8pxn")))

(define-public crate-rquery-0.1 (crate (name "rquery") (vers "0.1.2") (deps (list (crate-dep (name "xml-rs") (req "*") (default-features #t) (kind 0)))) (hash "1s6pw4a0gzfy8h4hi47j13c2fc0qncbfjvr7lxicp5vsf65npxqf")))

(define-public crate-rquery-0.2 (crate (name "rquery") (vers "0.2.0") (deps (list (crate-dep (name "xml-rs") (req "*") (default-features #t) (kind 0)))) (hash "0p1nd2sisall2xadvg5b8n3xnsv3a70wi4z8805kciy8wzi80m1l")))

(define-public crate-rquery-0.2 (crate (name "rquery") (vers "0.2.1") (deps (list (crate-dep (name "xml-rs") (req "*") (default-features #t) (kind 0)))) (hash "163nvvzbqivpjz6lhdcq0jjihxp210akix2nzhs89f70vi7y16fs")))

(define-public crate-rquery-0.3 (crate (name "rquery") (vers "0.3.0") (deps (list (crate-dep (name "xml-rs") (req "^0.3.4") (default-features #t) (kind 0)))) (hash "185j7lzvhd9ygzz5kcjraai0bgvfcaad90scqyip0dnlyl0zvpg0")))

(define-public crate-rquery-0.3 (crate (name "rquery") (vers "0.3.1") (deps (list (crate-dep (name "xml-rs") (req "^0.3.4") (default-features #t) (kind 0)))) (hash "1wk82lxshkgg8sj4lw97bzmlbrv3p4rhqw66x9fqjb8pgirfba7h")))

(define-public crate-rquery-0.3 (crate (name "rquery") (vers "0.3.2") (deps (list (crate-dep (name "xml-rs") (req "^0.3.4") (default-features #t) (kind 0)))) (hash "1bdfb1gi2idl4vx7fin9nlc882g86v3p1xkwlqrgm4czalqpgb3s")))

(define-public crate-rquery-0.4 (crate (name "rquery") (vers "0.4.0") (deps (list (crate-dep (name "xml-rs") (req "^0.4") (default-features #t) (kind 0)))) (hash "00npdr15x1ayvyrara6b7kyk59y3c10nzamfq92m4adqbb5cwks6")))

(define-public crate-rquery-0.4 (crate (name "rquery") (vers "0.4.1") (deps (list (crate-dep (name "xml-rs") (req "^0.7") (default-features #t) (kind 0)))) (hash "0lnd4gsmpw5c8wbm7lbnfjdzhbwqldi1y677plgxa0nynbg8m1ap")))

