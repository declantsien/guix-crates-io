(define-module (crates-io ka on) #:use-module (crates-io))

(define-public crate-kaon-0.1 (crate (name "kaon") (vers "0.1.4") (deps (list (crate-dep (name "ahash") (req "^0.7.6") (default-features #t) (kind 0)) (crate-dep (name "criterion") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "fnv") (req "^1.0.7") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 2)) (crate-dep (name "regex") (req "^1.5.4") (default-features #t) (kind 2)) (crate-dep (name "smallvec") (req "^1.8.0") (features (quote ("const_new"))) (default-features #t) (kind 0)) (crate-dep (name "termcolor") (req "^1.1.2") (default-features #t) (kind 0)))) (hash "0h3fz8gci9j3nq38ld4pddqgazlxn59d4j3s8qaxwkspz3zp4lbl")))

