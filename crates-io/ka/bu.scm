(define-module (crates-io ka bu) #:use-module (crates-io))

(define-public crate-kabuki-0.0.1 (crate (name "kabuki") (vers "0.0.1") (hash "15m0h51q13y6cq13zaqm3nlhw43jzadjx00ys8l44z6m0bcx2pyl")))

(define-public crate-kabuki-0.0.2 (crate (name "kabuki") (vers "0.0.2") (deps (list (crate-dep (name "futures") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "futures-mpsc") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "futures-spawn") (req "^0.1") (features (quote ("use_std" "tokio"))) (kind 0)) (crate-dep (name "futures-threadpool") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "tokio-core") (req "^0.1") (default-features #t) (kind 0)))) (hash "0cas7d77wz9ncxx38c6q3j80mvp5mxavbry4k23qsinfndrn6kpf")))

