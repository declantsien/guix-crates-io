(define-module (crates-io ka rg) #:use-module (crates-io))

(define-public crate-kargo-0.0.0 (crate (name "kargo") (vers "0.0.0") (hash "13hb6ja5f3f2gl5prdfbzi8llj4fsis1p0wyxx4hkpqjdhpybnzw")))

(define-public crate-kargs-0.1 (crate (name "kargs") (vers "0.1.0") (hash "03g4sgyckd7acg8r4bdcs9yyi9ylqcqrc853nmlkmdz4ynkl5d92")))

