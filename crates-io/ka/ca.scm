(define-module (crates-io ka ca) #:use-module (crates-io))

(define-public crate-kaca-0.1 (crate (name "kaca") (vers "0.1.0") (deps (list (crate-dep (name "kacha") (req "^0.1.0") (default-features #t) (kind 0) (package "kacha")))) (hash "0s4a7w1kq5xkij929xj0hjrw6myc5w8g2gqi4wq5bbg8ahj8q8ds")))

