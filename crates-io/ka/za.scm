(define-module (crates-io ka za) #:use-module (crates-io))

(define-public crate-kazari-0.0.1 (crate (name "kazari") (vers "0.0.1") (deps (list (crate-dep (name "embedded-graphics") (req "^0.7.0-alpha.3") (default-features #t) (kind 0)) (crate-dep (name "hashbrown") (req "^0.9") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0") (default-features #t) (kind 0)) (crate-dep (name "once_cell") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "pretty_assertions") (req "^0") (default-features #t) (kind 2)) (crate-dep (name "smallvec") (req "^1.6") (default-features #t) (kind 0)))) (hash "0fmlf0s50r01dylbj0bwyb1r8vlrs06ngr3859dz7s35rqalbyfq")))

