(define-module (crates-io ka pr) #:use-module (crates-io))

(define-public crate-kaprekar-0.1 (crate (name "kaprekar") (vers "0.1.0") (hash "0wq18sbm2arpknwhkw5cicw6i9ipzfiylzqsdldma7r1fsn2fq1b")))

(define-public crate-kaprekar-0.1 (crate (name "kaprekar") (vers "0.1.1") (hash "14zszqi7cya9y8ns83ryqkyfykvfki54276rnm9v9pdhn3b8nbia")))

(define-public crate-kaprekar-0.1 (crate (name "kaprekar") (vers "0.1.2") (hash "01ymkwzkam3ch5f7rv5j9zn3bfaka6rligg48qdwia0qr1i9v5vv")))

(define-public crate-kaprekar-0.1 (crate (name "kaprekar") (vers "0.1.3") (hash "1yc92dhmh6vq3jc02j237njvrsp00s146d9lql9wv32n2f3ahrb5")))

(define-public crate-kaprekar-0.1 (crate (name "kaprekar") (vers "0.1.4") (hash "042lgmrpd6wywv04zayc856kcs7611ha1m1sc9iv28q884q91h55")))

(define-public crate-kaprekar-0.1 (crate (name "kaprekar") (vers "0.1.5") (hash "1yn8bbk8cx9h8axik31hbg9h7a783c9yq9lnyq86a5rxc949672p")))

