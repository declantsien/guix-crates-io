(define-module (crates-io ka ki) #:use-module (crates-io))

(define-public crate-kaki-0.0.0 (crate (name "kaki") (vers "0.0.0") (hash "0imhi4lfqf12ra26d9ds50c9y87yg5hm0x25q62i6sxyh3v7zfcz") (yanked #t)))

(define-public crate-kaki-0.1 (crate (name "kaki") (vers "0.1.0") (hash "0irgik4p5zfm23jcgyqy54ncc38k607v206p121lfc7jii8mswg9")))

(define-public crate-kakistocracy-0.1 (crate (name "kakistocracy") (vers "0.1.0") (deps (list (crate-dep (name "winapi") (req "^0.3.9") (features (quote ("winuser"))) (default-features #t) (target "cfg(windows)") (kind 0)))) (hash "0bj1zc7w0jyrdrv53wxr3v4l2azfq9jxzp3dwycwl533vmhgjnwy")))

