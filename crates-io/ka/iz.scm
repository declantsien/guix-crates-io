(define-module (crates-io ka iz) #:use-module (crates-io))

(define-public crate-kaizen-0.0.0 (crate (name "kaizen") (vers "0.0.0") (hash "15lqv6md677mliil10s3f1sb3g142f5a1yahpi4n7vn5vzhwq9c6")))

(define-public crate-kaizen-macros-0.0.0 (crate (name "kaizen-macros") (vers "0.0.0") (hash "0xkx42q2fvqwrdrfjs29vijdxfxvcsmhr8l60aqjn6w4apkmf9w6")))

