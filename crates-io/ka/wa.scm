(define-module (crates-io ka wa) #:use-module (crates-io))

(define-public crate-kawa-0.6 (crate (name "kawa") (vers "0.6.1") (deps (list (crate-dep (name "nom") (req "^7.1.3") (default-features #t) (kind 0)))) (hash "12f6839laywbasrsam9j3640vq2idghn4w2mpfjh5qmqrs2ikgkf") (features (quote (("rc-alloc")))) (rust-version "1.66.1")))

(define-public crate-kawa-0.6 (crate (name "kawa") (vers "0.6.2") (deps (list (crate-dep (name "nom") (req "^7.1.3") (default-features #t) (kind 0)))) (hash "1xnarffzqn03114qk8g58ydrlqfvs0p06164bfz29pg4fcin24q7") (features (quote (("rc-alloc") ("custom-vecdeque")))) (rust-version "1.66.1")))

(define-public crate-kawa-0.6 (crate (name "kawa") (vers "0.6.3") (deps (list (crate-dep (name "nom") (req "^7.1.3") (default-features #t) (kind 0)))) (hash "109xasyrga7lw2g6vrjpr1nbrmwjf3i0dkpal1z23wqaww65yprg") (features (quote (("simd") ("rc-alloc") ("default" "simd") ("custom-vecdeque")))) (rust-version "1.66.1")))

(define-public crate-kawa-0.6 (crate (name "kawa") (vers "0.6.4") (deps (list (crate-dep (name "nom") (req "^7.1.3") (default-features #t) (kind 0)))) (hash "0l6bnbv37yg9n5f09z2v08f2n01j167cph5xirgkyrfribyc4ji8") (features (quote (("tolerant-parsing") ("simd") ("rc-alloc") ("default" "simd" "tolerant-parsing") ("custom-vecdeque")))) (rust-version "1.66.1")))

(define-public crate-kawa-0.6 (crate (name "kawa") (vers "0.6.5") (deps (list (crate-dep (name "nom") (req "^7.1.3") (default-features #t) (kind 0)))) (hash "1q5r97wv44fps1xzxkl0r4qizrpd7ikcnyrp4vqxi1difdf6k1fv") (features (quote (("tolerant-parsing") ("simd") ("rc-alloc") ("default" "simd" "tolerant-parsing") ("custom-vecdeque")))) (rust-version "1.66.1")))

(define-public crate-kawa-0.6 (crate (name "kawa") (vers "0.6.6") (deps (list (crate-dep (name "nom") (req "^7.1.3") (default-features #t) (kind 0)))) (hash "1fgmznj08s0gwly846zwyj47gdc35d3n80qhbp186i924piq95gq") (features (quote (("tolerant-parsing") ("simd") ("rc-alloc") ("default" "simd" "tolerant-parsing" "rc-alloc") ("custom-vecdeque")))) (rust-version "1.66.1")))

(define-public crate-kawaii-0.1 (crate (name "kawaii") (vers "0.1.0-alpha") (deps (list (crate-dep (name "ansi_term") (req "^0.10") (default-features #t) (kind 0)) (crate-dep (name "typemap") (req "^0.3") (default-features #t) (kind 0)))) (hash "1chs6rjp9zbxnkldrij5whasn6fmnfxhljddrji3jbx4761d0dgy") (features (quote (("unstable"))))))

