(define-module (crates-io ka it) #:use-module (crates-io))

(define-public crate-kaitai-0.1 (crate (name "kaitai") (vers "0.1.0") (hash "0xb3yyx847qcbw4rinn25gv5yvjwa3h0fz46ibnafqxs1h7jz2yd")))

(define-public crate-kaitai-0.1 (crate (name "kaitai") (vers "0.1.1") (deps (list (crate-dep (name "proc-macro2") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1") (features (quote ("extra-traits"))) (default-features #t) (kind 0)) (crate-dep (name "yaml-rust") (req "^0.4") (default-features #t) (kind 0)))) (hash "0byh5lmkh0n7gm0xs4vs872li528rf58qr8a1cx8nm3ls849m3wd")))

(define-public crate-kaitai-0.1 (crate (name "kaitai") (vers "0.1.2") (deps (list (crate-dep (name "byteorder") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "kaitai-macros") (req "^0") (default-features #t) (kind 0)) (crate-dep (name "paste") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1") (default-features #t) (kind 0)))) (hash "1dq29nb70zklb9ayb23sgwq0ygr489nmygq7mcz7b2k1iqiq1f8h")))

(define-public crate-kaitai-macros-0.1 (crate (name "kaitai-macros") (vers "0.1.0") (deps (list (crate-dep (name "proc-macro2") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1") (features (quote ("extra-traits"))) (default-features #t) (kind 0)) (crate-dep (name "yaml-rust") (req "^0.4") (default-features #t) (kind 0)))) (hash "023g5s4gl4h212bn3xan9l27db6a33glb7sl85jpi7kc39hbsz2a")))

(define-public crate-kaitai-macros-0.1 (crate (name "kaitai-macros") (vers "0.1.1") (deps (list (crate-dep (name "proc-macro2") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1") (features (quote ("extra-traits" "full"))) (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "yaml-rust") (req "^0.4") (default-features #t) (kind 0)))) (hash "1k1g18qiarp0rrqkxjyn1idckxxm0kasddk67h3c0piiqk4c63q1")))

(define-public crate-kaitai-macros-0.1 (crate (name "kaitai-macros") (vers "0.1.2") (deps (list (crate-dep (name "proc-macro2") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1") (features (quote ("extra-traits" "full"))) (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "yaml-rust") (req "^0.4") (default-features #t) (kind 0)))) (hash "126za2qrld3y5ak2ldjc67q296wkcpgmg1rjfali14wym5h3b7gf")))

(define-public crate-kaiten-sushi-0.1 (crate (name "kaiten-sushi") (vers "0.1.0") (hash "0xr8vwiflf55fm7lbhcagwrljw6d7wamsiqv4szgmvh669fqbq4s")))

(define-public crate-kaiten-sushi-0.2 (crate (name "kaiten-sushi") (vers "0.2.0") (deps (list (crate-dep (name "rand") (req "^0.6.0") (default-features #t) (kind 0)))) (hash "061v7l6g5pmsqb8ds92m0w2raa7p571dk5gml66cfx41lv3jphz8")))

(define-public crate-kaitlynsguessinggame-1 (crate (name "kaitlynsguessinggame") (vers "1.0.0") (deps (list (crate-dep (name "clearscreen") (req "^2.0.1") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)))) (hash "0mz64s1a7bhq1d3mmf6rb2na78g0cahb6lawxzvz2czp3vb9hs3b")))

