(define-module (crates-io ka bb) #:use-module (crates-io))

(define-public crate-kabbali_rust_modules-0.1 (crate (name "kabbali_rust_modules") (vers "0.1.0") (deps (list (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)))) (hash "1xmp3bshcm80pm56cn25g8z081jwpxhd19ql7wrc0vv855x7m5in")))

