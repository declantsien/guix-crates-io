(define-module (crates-io ka be) #:use-module (crates-io))

(define-public crate-kabegami-0.1 (crate (name "kabegami") (vers "0.1.0") (deps (list (crate-dep (name "anyhow") (req "^1.0.66") (default-features #t) (kind 0)) (crate-dep (name "infer") (req "^0.11.0") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.37") (default-features #t) (kind 0)) (crate-dep (name "wall") (req "^0.2.3") (default-features #t) (kind 0)))) (hash "046a884qm6px4nmp00wzs6nxbm6bk39zwzfvqnlbpnyr4gkhd8vp") (yanked #t)))

(define-public crate-kabegami-0.1 (crate (name "kabegami") (vers "0.1.1") (deps (list (crate-dep (name "anyhow") (req "^1.0.66") (default-features #t) (kind 0)) (crate-dep (name "infer") (req "^0.11.0") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.37") (default-features #t) (kind 0)) (crate-dep (name "wall") (req "^0.2.3") (default-features #t) (kind 0)))) (hash "1mvmphnxggzrqmcki6wdy05pfilid28g9x1wb03zrylrg59xbdv9") (yanked #t)))

(define-public crate-kabegami-0.2 (crate (name "kabegami") (vers "0.2.0") (deps (list (crate-dep (name "argh") (req "^0.1.10") (default-features #t) (kind 0)) (crate-dep (name "waraq") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "1yk21153w4yrk3w6iwanyjk4ixf62gr9yr0rnp899if8dcqjl180")))

(define-public crate-kabegami-0.2 (crate (name "kabegami") (vers "0.2.1") (deps (list (crate-dep (name "argh") (req "^0.1.10") (default-features #t) (kind 0)) (crate-dep (name "waraq") (req "^0.1.1") (default-features #t) (kind 0)))) (hash "0iskiq9dswi1wq7245icfnw2v6xcbwkafyq5hyr1hig0x3w41acc")))

(define-public crate-kabegami-0.2 (crate (name "kabegami") (vers "0.2.2") (deps (list (crate-dep (name "argh") (req "^0.1.10") (default-features #t) (kind 0)) (crate-dep (name "waraq") (req "^0.1.1") (default-features #t) (kind 0)))) (hash "1xbj8shl2nwl5sjcrnwx3lfvggjxhskilya57rh6h81d6xihx062")))

(define-public crate-kabegami-0.3 (crate (name "kabegami") (vers "0.3.0") (deps (list (crate-dep (name "anyhow") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "argh") (req "^0.1.10") (default-features #t) (kind 0)) (crate-dep (name "image") (req "^0.24.5") (features (quote ("jpeg" "png"))) (kind 0)) (crate-dep (name "infer") (req "^0.11.0") (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)) (crate-dep (name "resize") (req "^0.7.4") (default-features #t) (kind 0)) (crate-dep (name "rgb") (req "^0.8.35") (default-features #t) (kind 0)) (crate-dep (name "waraq") (req "^0.2.1") (default-features #t) (kind 0)))) (hash "0xwb94b49srp0mnmzxmrkg9yf9gjj1zsjl7gqw9vsmkv3z2anwi6")))

