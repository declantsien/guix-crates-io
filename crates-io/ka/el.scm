(define-module (crates-io ka el) #:use-module (crates-io))

(define-public crate-kaelix-0.1 (crate (name "kaelix") (vers "0.1.0") (deps (list (crate-dep (name "regex") (req "^1.7.3") (default-features #t) (kind 0)) (crate-dep (name "termion") (req "^2.0.1") (default-features #t) (kind 0)))) (hash "07rcxgczm0vlkxh10wi6jqm1zd1y6sichjcrm9yx491qgcn30jkk")))

(define-public crate-kaelix-0.1 (crate (name "kaelix") (vers "0.1.1") (deps (list (crate-dep (name "regex") (req "^1.7.3") (default-features #t) (kind 0)) (crate-dep (name "termion") (req "^2.0.1") (default-features #t) (kind 0)))) (hash "05ss41qzm72gip107qs2jdhvagklzayh7yv7g8rw5b52a2lbmkz9")))

