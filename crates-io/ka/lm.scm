(define-module (crates-io ka lm) #:use-module (crates-io))

(define-public crate-kalman-0.0.0 (crate (name "kalman") (vers "0.0.0") (hash "1bp7glg526qprdf0q86rifgadd40kp33g15bn1zr6bzidwiz763m")))

(define-public crate-kalman-fusion-0.1 (crate (name "kalman-fusion") (vers "0.1.0") (deps (list (crate-dep (name "chrono") (req "^0.4.31") (default-features #t) (kind 2)) (crate-dep (name "fixed") (req "^1.24.0") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2.17") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 2)) (crate-dep (name "rand_distr") (req "^0.4.3") (default-features #t) (kind 2)))) (hash "08mbbav4nv50hi7fjz98rf3c7q9ahjkwidxl5n250jh8zpb00gv2")))

(define-public crate-kalman-fusion-0.1 (crate (name "kalman-fusion") (vers "0.1.1") (deps (list (crate-dep (name "chrono") (req "^0.4.31") (default-features #t) (kind 2)) (crate-dep (name "fixed") (req "^1.24.0") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2.17") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 2)) (crate-dep (name "rand_distr") (req "^0.4.3") (default-features #t) (kind 2)))) (hash "1vfhnhgawg3j05lxfn9w3bbgz24zh1k44rd7z1545z0s90356by5")))

(define-public crate-kalman-rust-0.1 (crate (name "kalman-rust") (vers "0.1.0") (deps (list (crate-dep (name "chrono") (req "^0.4") (features (quote ("serde"))) (default-features #t) (kind 0)) (crate-dep (name "nalgebra") (req "^0.32.1") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "uuid") (req "^0.8") (features (quote ("serde" "v4"))) (default-features #t) (kind 0)))) (hash "0bd2r5c55q46ndfvqbrpiskd6f8nf3f24pwj3lazhyhp05ry2prp")))

(define-public crate-kalman-rust-0.2 (crate (name "kalman-rust") (vers "0.2.0") (deps (list (crate-dep (name "chrono") (req "^0.4") (features (quote ("serde"))) (default-features #t) (kind 0)) (crate-dep (name "nalgebra") (req "^0.32.1") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "uuid") (req "^0.8") (features (quote ("serde" "v4"))) (default-features #t) (kind 0)))) (hash "1fxi8jyfiqyzwi9q8yphzscayajszxwxhq5jlqg96ahskavz6spy")))

(define-public crate-kalman-rust-0.2 (crate (name "kalman-rust") (vers "0.2.1") (deps (list (crate-dep (name "chrono") (req "^0.4") (features (quote ("serde"))) (default-features #t) (kind 0)) (crate-dep (name "nalgebra") (req "^0.32.1") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "uuid") (req "^0.8") (features (quote ("serde" "v4"))) (default-features #t) (kind 0)))) (hash "1npkzmss80gpbk28vziqr7axz23cxl9vqlnjk1mc131x0gcam2lw")))

(define-public crate-kalman-rust-0.2 (crate (name "kalman-rust") (vers "0.2.2") (deps (list (crate-dep (name "chrono") (req "^0.4") (features (quote ("serde"))) (default-features #t) (kind 0)) (crate-dep (name "nalgebra") (req "^0.32.1") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "uuid") (req "^0.8") (features (quote ("serde" "v4"))) (default-features #t) (kind 0)))) (hash "0hgf25r71ag9lrzfay6mxdxvc686f3izxn43k2crapbvwk9m1yll")))

(define-public crate-kalman-rust-0.2 (crate (name "kalman-rust") (vers "0.2.3") (deps (list (crate-dep (name "chrono") (req "^0.4.31") (features (quote ("serde"))) (default-features #t) (kind 0)) (crate-dep (name "nalgebra") (req "^0.32.3") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)) (crate-dep (name "uuid") (req "^1.5.0") (features (quote ("serde" "v4"))) (default-features #t) (kind 0)))) (hash "1486z25136zax8am3bg93nxavrl146120mcgwx5lbdxnlp07cc2y")))

(define-public crate-kalman_filter-0.1 (crate (name "kalman_filter") (vers "0.1.0") (deps (list (crate-dep (name "nalgebra") (req "^0.14.1") (default-features #t) (kind 0)))) (hash "1hg5j8cmnap9lny45ba1iqh6sk0dxbk88zpkz3ysmi8qx3czlwnn")))

(define-public crate-kalman_filter-0.1 (crate (name "kalman_filter") (vers "0.1.1") (deps (list (crate-dep (name "nalgebra") (req "^0.14.1") (default-features #t) (kind 0)))) (hash "0ampxxy051bapby1dn05gph5847pa08spxvvsq7qfcybglfni9xx")))

(define-public crate-kalman_filter-0.1 (crate (name "kalman_filter") (vers "0.1.2") (deps (list (crate-dep (name "nalgebra") (req "^0.14.1") (default-features #t) (kind 0)))) (hash "07c4yn2k28lhm8vrb4qrpr6c1qmnlimbya7ak1g607casvf7wc4l")))

(define-public crate-kalman_rs-0.1 (crate (name "kalman_rs") (vers "0.1.0") (deps (list (crate-dep (name "nalgebra") (req "^0.17.2") (default-features #t) (kind 0)))) (hash "0kx01vgib0ia0lccnycazklkdbq34za33aw6ym4nh5zrdmj20j81")))

(define-public crate-kalman_rs-0.1 (crate (name "kalman_rs") (vers "0.1.1") (deps (list (crate-dep (name "nalgebra") (req "^0.17.2") (default-features #t) (kind 0)))) (hash "11bvk9xfznqf49iwazsd1whj7lbnfhlk38sl39p2p7chpmg6lbh1")))

(define-public crate-kalman_rs-0.1 (crate (name "kalman_rs") (vers "0.1.2") (deps (list (crate-dep (name "nalgebra") (req "^0.17.2") (default-features #t) (kind 0)))) (hash "1mabi7qnilyxbav2vsgl7x173rljnn0qr0ziby0fydvc4wpmg7m7")))

(define-public crate-kalman_rs-0.1 (crate (name "kalman_rs") (vers "0.1.3") (deps (list (crate-dep (name "nalgebra") (req "^0.17.2") (default-features #t) (kind 0)))) (hash "15zyslp9wpiv2fpihwsrdl4h38ba0pn0vmbjl03l6qb6pm9c053p")))

(define-public crate-kalmanfilt-0.2 (crate (name "kalmanfilt") (vers "0.2.2") (deps (list (crate-dep (name "assert_approx_eq") (req "^1.1.0") (default-features #t) (kind 2)) (crate-dep (name "nalgebra") (req "^0.32.3") (features (quote ("libm"))) (kind 0)) (crate-dep (name "num-traits") (req "^0.2.17") (features (quote ("libm"))) (kind 0)))) (hash "07chq1r3a5bf9kx8msbzrns2qpy2fqf6l2nl62zp4dspfmjnq42n") (features (quote (("default" "alloc") ("alloc"))))))

(define-public crate-kalmanfilt-0.2 (crate (name "kalmanfilt") (vers "0.2.3") (deps (list (crate-dep (name "assert_approx_eq") (req "^1.1.0") (default-features #t) (kind 2)) (crate-dep (name "nalgebra") (req "^0.32.3") (features (quote ("libm"))) (kind 0)) (crate-dep (name "num-traits") (req "^0.2.17") (features (quote ("libm"))) (kind 0)))) (hash "1hxrdq0dfk51yndnlc7i56ik6aw9mibx4r3l90drbvivx9s87nad") (features (quote (("default" "alloc") ("alloc"))))))

(define-public crate-kalmanfilt-0.2 (crate (name "kalmanfilt") (vers "0.2.4") (deps (list (crate-dep (name "assert_approx_eq") (req "^1.1.0") (default-features #t) (kind 2)) (crate-dep (name "nalgebra") (req "^0.32.3") (features (quote ("libm"))) (kind 0)) (crate-dep (name "num-traits") (req "^0.2.17") (features (quote ("libm"))) (kind 0)))) (hash "1dzinfp8ls6s7gq383ga41afypsr2z4v37w32lg0a1wjgnlzx0av") (features (quote (("default" "alloc") ("alloc"))))))

(define-public crate-kalmanrs-0.1 (crate (name "kalmanrs") (vers "0.1.0") (deps (list (crate-dep (name "nalgebra") (req "^0.18.0") (default-features #t) (kind 0)))) (hash "03cda5wq20lr58pip9srz81is2y1mnkb8spxkbvv05659xdyzsw0")))

