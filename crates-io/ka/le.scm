(define-module (crates-io ka le) #:use-module (crates-io))

(define-public crate-kale-0.0.0 (crate (name "kale") (vers "0.0.0") (hash "13vw4k4zv256x7cqly4jg6zj00zcpcxn9v2xbgckjbznbli0hbgz")))

(define-public crate-kale-sh-0.0.1 (crate (name "kale-sh") (vers "0.0.1") (deps (list (crate-dep (name "anyhow") (req "^1.0.58") (default-features #t) (kind 0)) (crate-dep (name "chrono") (req "^0.4.19") (default-features #t) (kind 0)) (crate-dep (name "cli-table") (req "^0.4.7") (kind 0)) (crate-dep (name "colored") (req "^2") (default-features #t) (kind 0)) (crate-dep (name "dirs") (req "^4.0.0") (default-features #t) (kind 0)) (crate-dep (name "human_bytes") (req "^0.3.1") (default-features #t) (kind 0)) (crate-dep (name "pico-args") (req "^0.5.0") (default-features #t) (kind 0)))) (hash "04vzck8fd4q9lns24j70l68irxc069jcyf7byg46vgvhplrdpaiw")))

(define-public crate-kaleidoscope-focus-0.1 (crate (name "kaleidoscope-focus") (vers "0.1.0") (deps (list (crate-dep (name "indicatif") (req "^0.17.1") (default-features #t) (kind 2)) (crate-dep (name "serialport") (req "^4.2") (default-features #t) (kind 0)))) (hash "13kmjg48v2znpx2yn2aqvgnwmlxna5ylfjp24gn6ipc31r6l58hk") (rust-version "1.59.0")))

(define-public crate-kaleidoscope-focus-cli-0.1 (crate (name "kaleidoscope-focus-cli") (vers "0.1.0") (deps (list (crate-dep (name "anyhow") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^4.0") (features (quote ("std" "derive" "env" "help" "usage"))) (kind 0)) (crate-dep (name "indicatif") (req "^0.17.1") (default-features #t) (kind 0)) (crate-dep (name "kaleidoscope-focus") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)))) (hash "0wn0n1swm0f50nciqhd4a3ixn0apmzcqmqz0lrnkk19riframqzc") (rust-version "1.59.0")))

