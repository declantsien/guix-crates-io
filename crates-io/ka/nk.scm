(define-module (crates-io ka nk) #:use-module (crates-io))

(define-public crate-kankyo-0.1 (crate (name "kankyo") (vers "0.1.0") (hash "1b49gxs08amxfa3n4xy05jf01pzk42l8nlg69p72gcy73797cc0a")))

(define-public crate-kankyo-0.1 (crate (name "kankyo") (vers "0.1.1") (hash "0r6qrw4mvmn006yg2gfh8j1xgbfvk6gk61z1flrpgq9y7nps388w")))

(define-public crate-kankyo-0.2 (crate (name "kankyo") (vers "0.2.0") (hash "1pw1jvnwqm552s7jgjmf1c2qxjsw6qs3nkkhm0f6s9b6dixfqz8l")))

(define-public crate-kankyo-0.3 (crate (name "kankyo") (vers "0.3.0") (hash "0h0cmrqdcnzk64w1fbynfql05dpggisxnm96aqdis3573wii2nij")))

