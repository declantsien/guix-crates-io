(define-module (crates-io ka rd) #:use-module (crates-io))

(define-public crate-kard-0.0.0 (crate (name "kard") (vers "0.0.0") (hash "0dn8msggb839b6r79q33gnaddy7l13cd2rhzk55ag0q0sg49yhw5")))

(define-public crate-kard-0.0.1 (crate (name "kard") (vers "0.0.1") (hash "04pw3f2gias9m3mzml8acm2ij3g8aj7wvs9ygb0ig7iika0ykjqz")))

