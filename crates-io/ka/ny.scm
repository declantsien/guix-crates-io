(define-module (crates-io ka ny) #:use-module (crates-io))

(define-public crate-kanye-0.1 (crate (name "kanye") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^2.33.3") (default-features #t) (kind 0)) (crate-dep (name "colored") (req "^2.0.0") (default-features #t) (kind 0)) (crate-dep (name "ureq") (req "^2.1.0") (default-features #t) (kind 0)))) (hash "141p6zhba1mrwyvk1nwq43mgiwlsphxf2hl9didyg58mabw94gb1")))

(define-public crate-kanye-0.1 (crate (name "kanye") (vers "0.1.1") (deps (list (crate-dep (name "clap") (req "^2.33.3") (default-features #t) (kind 0)) (crate-dep (name "colored") (req "^2.0.0") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.64") (default-features #t) (kind 0)) (crate-dep (name "ureq") (req "^2.1.0") (default-features #t) (kind 0)))) (hash "1wa7mjgi2q2cp3gg8g9wvkycxk80zf7k8zx3s466gk6xhw5khcxf")))

(define-public crate-kanyey-0.1 (crate (name "kanyey") (vers "0.1.0") (deps (list (crate-dep (name "colored") (req "^2.0.4") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.188") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.107") (default-features #t) (kind 0)))) (hash "03rchwz0vbhvmadvl6qz344sk45fgxrkxipq0ga9qr5plm8nr3wp")))

(define-public crate-kanyey-0.1 (crate (name "kanyey") (vers "0.1.1") (deps (list (crate-dep (name "colored") (req "^2.0.4") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.188") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.107") (default-features #t) (kind 0)))) (hash "165x8b0sv7hqq16v7jj00fp8mqp9x8a74vgp7xdfy03bzh2mcypf")))

(define-public crate-kanyey-0.1 (crate (name "kanyey") (vers "0.1.2") (deps (list (crate-dep (name "colored") (req "^2.0.4") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.188") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.107") (default-features #t) (kind 0)))) (hash "158kp3d0fg3z5mb255002lx51p06197525blhnpqd6f4j3nffc4m")))

(define-public crate-kanyey-0.1 (crate (name "kanyey") (vers "0.1.3") (deps (list (crate-dep (name "colored") (req "^2.0.4") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.188") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.107") (default-features #t) (kind 0)))) (hash "0ghi4vq73whfsf69gqx7jxmm2i72j639hcbi5wy3d127hdhg84fa")))

