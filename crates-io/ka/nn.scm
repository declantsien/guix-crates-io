(define-module (crates-io ka nn) #:use-module (crates-io))

(define-public crate-kannon-0.0.2 (crate (name "kannon") (vers "0.0.2") (deps (list (crate-dep (name "crossbeam-deque") (req "^0.8.0") (default-features #t) (kind 0)) (crate-dep (name "crossbeam-utils") (req "^0.8.3") (default-features #t) (kind 0)) (crate-dep (name "paste") (req "^1.0.5") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.3") (default-features #t) (kind 2)) (crate-dep (name "thiserror") (req "^1.0.24") (default-features #t) (kind 0)))) (hash "1512fbl7k2cyr1qqq7x9ylrcjwpila68qf1i1paxvqbbzcgv77ra")))

