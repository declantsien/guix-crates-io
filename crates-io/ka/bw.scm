(define-module (crates-io ka bw) #:use-module (crates-io))

(define-public crate-kabwoy_auth_service-0.1 (crate (name "kabwoy_auth_service") (vers "0.1.0") (deps (list (crate-dep (name "cargo-modules") (req "^0.15.0") (default-features #t) (kind 0)) (crate-dep (name "env_logger") (req "^0.11.3") (default-features #t) (kind 0)))) (hash "0mrzadsw2dkfc0y9frwy4iqkg126p2b9y321wd7aym5wjf5xxjwd")))

