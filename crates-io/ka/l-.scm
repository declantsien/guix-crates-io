(define-module (crates-io ka l-) #:use-module (crates-io))

(define-public crate-kal-derive-0.1 (crate (name "kal-derive") (vers "0.1.0") (deps (list (crate-dep (name "darling") (req "^0.14.1") (default-features #t) (kind 0)) (crate-dep (name "kal") (req "^0.1") (default-features #t) (kind 2)) (crate-dep (name "quote") (req "^1.0.21") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.99") (default-features #t) (kind 0)))) (hash "0dsppf7xnm4skd41ql4j6vppa28z2hj3wpwnd2xdkza2p9ykm37a")))

(define-public crate-kal-derive-0.1 (crate (name "kal-derive") (vers "0.1.1") (deps (list (crate-dep (name "darling") (req "^0.14.1") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.21") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.99") (default-features #t) (kind 0)))) (hash "1blvyaawfp5mg8xqcvgz5c2r08kvpjs1yqllwk1pzmy505sqhsv4")))

(define-public crate-kal-derive-0.2 (crate (name "kal-derive") (vers "0.2.0") (deps (list (crate-dep (name "darling") (req "^0.14.1") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.21") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.99") (default-features #t) (kind 0)))) (hash "16wd4wav4m06lzaclxkqig2zhs1jnqji4yy7gpawhg3b8938ssjm")))

(define-public crate-kal-derive-0.2 (crate (name "kal-derive") (vers "0.2.1") (deps (list (crate-dep (name "darling") (req "^0.14.1") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.21") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.99") (default-features #t) (kind 0)))) (hash "08qsyaqsrhiaynccsdw1hl5p27b4fcaczlblga6hpmpxc1is4k7n")))

(define-public crate-kal-derive-0.2 (crate (name "kal-derive") (vers "0.2.2") (deps (list (crate-dep (name "darling") (req "^0.14.1") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.21") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.99") (default-features #t) (kind 0)))) (hash "13bsycwcfsj6yn4grs04y7f3m4091xi7zykz7r0lii4xgkiqbdzc")))

(define-public crate-kal-derive-0.3 (crate (name "kal-derive") (vers "0.3.0") (deps (list (crate-dep (name "darling") (req "^0.14.1") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.21") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.99") (default-features #t) (kind 0)))) (hash "1nqanx90jfv6rf0d087b0maj9y6ws3wkicdm9r2vwbc0dbxg75z4") (features (quote (("lex"))))))

(define-public crate-kal-derive-0.3 (crate (name "kal-derive") (vers "0.3.1") (deps (list (crate-dep (name "darling") (req "^0.14") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1") (default-features #t) (kind 0)))) (hash "138c25ia5wiy0kd816hxzymgbfwfr34z8qywp08n39fnq5mqk72p") (features (quote (("lex"))))))

(define-public crate-kal-derive-0.4 (crate (name "kal-derive") (vers "0.4.0") (deps (list (crate-dep (name "darling") (req "^0.14") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1") (default-features #t) (kind 0)))) (hash "07wzv054x7z5q3fh4s333r7pj6llj2v0brf7c1kigvgrk7ra2jmp") (features (quote (("lex")))) (yanked #t)))

(define-public crate-kal-derive-0.4 (crate (name "kal-derive") (vers "0.4.1") (deps (list (crate-dep (name "darling") (req "^0.14") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1") (default-features #t) (kind 0)))) (hash "1ybd16cv6g9m8lnj7x0absj1nzww6v45r1dv8zc16izx6a6nkd1b") (features (quote (("lex"))))))

(define-public crate-kal-derive-0.5 (crate (name "kal-derive") (vers "0.5.0") (deps (list (crate-dep (name "darling") (req "^0.14") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1") (default-features #t) (kind 0)))) (hash "0wccrinbx749mmccpwsph187010yklgkwhz9ibn469666ls2s76n") (features (quote (("lex"))))))

