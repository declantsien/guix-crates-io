(define-module (crates-io ka ga) #:use-module (crates-io))

(define-public crate-kagamijxl-0.1 (crate (name "kagamijxl") (vers "0.1.0") (deps (list (crate-dep (name "libjxl-sys") (req "^0.2.1") (default-features #t) (kind 0)))) (hash "19ni02rnf1rq6g9prgvy016w5qn0phh3419zfcj3i0ff52yni941")))

(define-public crate-kagamijxl-0.1 (crate (name "kagamijxl") (vers "0.1.1") (deps (list (crate-dep (name "libjxl-sys") (req "^0.2.1") (default-features #t) (kind 0)))) (hash "0azb0qv30mw9jzb96vr2ws7k21sw0b23qrpmbxv2iwd72ffb4gki")))

(define-public crate-kagamijxl-0.2 (crate (name "kagamijxl") (vers "0.2.0") (deps (list (crate-dep (name "libjxl-sys") (req "^0.3.0") (default-features #t) (kind 0)))) (hash "08852p1rw7im2m4zxyqyvib7zfs4dmr1595xqqigrhs5vld3p9yb")))

(define-public crate-kagamijxl-0.2 (crate (name "kagamijxl") (vers "0.2.1") (deps (list (crate-dep (name "libjxl-sys") (req "^0.3.0") (default-features #t) (kind 0)))) (hash "0wx68g9z5ybfbnfkgyjxzxh3fbv3gwfm4vrgsr96ms64gkxf2yla")))

(define-public crate-kagamijxl-0.2 (crate (name "kagamijxl") (vers "0.2.2") (deps (list (crate-dep (name "libjxl-sys") (req "^0.3.0") (default-features #t) (kind 0)))) (hash "0sspdfa6cb3rqk15rx6gjg1afk9b79mxrbirg9n9r9i2386f1yq2")))

(define-public crate-kagamijxl-0.2 (crate (name "kagamijxl") (vers "0.2.3") (deps (list (crate-dep (name "libjxl-sys") (req "^0.3.3") (default-features #t) (kind 0)))) (hash "16j4y3yzmli0091xl1jm2vllrndbpxcrlq1y1ksqkx9vfl0qccyf")))

(define-public crate-kagamijxl-0.2 (crate (name "kagamijxl") (vers "0.2.4") (deps (list (crate-dep (name "libjxl-sys") (req "^0.3.4") (default-features #t) (kind 0)))) (hash "19f1a0sn78r8gp88mry1pjd0r9zzza6djckw4bzysqj4mnr6jnr8")))

(define-public crate-kagamijxl-0.2 (crate (name "kagamijxl") (vers "0.2.5") (deps (list (crate-dep (name "libjxl-sys") (req "^0.3.5") (default-features #t) (kind 0)))) (hash "1492n4vpxgmnkfa8qp737bds6awzbi5dcyxglbzdca2pbmg0s6f3")))

(define-public crate-kagamijxl-0.2 (crate (name "kagamijxl") (vers "0.2.6") (deps (list (crate-dep (name "libjxl-sys") (req "^0.3.5") (default-features #t) (kind 0)))) (hash "0nkc5q2j1fbszlb3mxy8zd96zjgy8vqfqjs6l5xywmfksdz2vg3k")))

(define-public crate-kagamijxl-0.2 (crate (name "kagamijxl") (vers "0.2.7") (deps (list (crate-dep (name "libjxl-sys") (req "^0.3.5") (default-features #t) (kind 0)))) (hash "1xif8m6c6w3z8jwy3671gq8rnv7iqaaa2239fgrba993rb19vh9x")))

(define-public crate-kagamijxl-0.2 (crate (name "kagamijxl") (vers "0.2.8") (deps (list (crate-dep (name "libjxl-sys") (req "^0.3.5") (default-features #t) (kind 0)))) (hash "101hiyk3g5g2kiqr8bzsh5h8axj7fg9pxd7wwzbi7f45jv7bd3vd")))

(define-public crate-kagamijxl-0.2 (crate (name "kagamijxl") (vers "0.2.9") (deps (list (crate-dep (name "libjxl-sys") (req "^0.7.0") (default-features #t) (kind 0)))) (hash "1dpsfrqqshgp8frmhwr1xa3bhncpprs88bpm4jpmvcb18sdnd3h1")))

(define-public crate-kagamijxl-0.3 (crate (name "kagamijxl") (vers "0.3.0") (deps (list (crate-dep (name "libjxl-sys") (req "^0.7.0") (default-features #t) (kind 0)))) (hash "0wkv06nmw7zhacx4bn95z407zvxzc9h1y8p1grw60q12cq5w2psb")))

(define-public crate-kagamijxl-0.3 (crate (name "kagamijxl") (vers "0.3.1") (deps (list (crate-dep (name "libjxl-sys") (req "^0.7.0") (default-features #t) (kind 0)))) (hash "1r7vm0950f6p9a8vcdmmgjv9pv73by0khffjixs2ra8d8lb12q8b")))

(define-public crate-kagamijxl-0.3 (crate (name "kagamijxl") (vers "0.3.2") (deps (list (crate-dep (name "libjxl-sys") (req "^0.7.0") (default-features #t) (kind 0)))) (hash "1x737vrpqy167a6ziypzbhjrrc402wpmb9jx3gpizl9n19a1rz13")))

(define-public crate-kagamijxl-0.3 (crate (name "kagamijxl") (vers "0.3.3") (deps (list (crate-dep (name "libjxl-sys") (req "^0.7.1") (default-features #t) (kind 0)))) (hash "1yn8y4ri5499s2zq17m20l60nyaaxys7mx4hl3m3xwcdahamymwl")))

