(define-module (crates-io ka rp) #:use-module (crates-io))

(define-public crate-karplus-0.1 (crate (name "karplus") (vers "0.1.0") (deps (list (crate-dep (name "rand") (req "^0.3.14") (default-features #t) (kind 0)))) (hash "0z64pxnqfi3z4masjnfpqz4p2mvarc1icn4p3x17rlgn7jhlhzn9")))

