(define-module (crates-io ka st) #:use-module (crates-io))

(define-public crate-kaste-0.1 (crate (name "kaste") (vers "0.1.0") (deps (list (crate-dep (name "dirs") (req "^5.0.1") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.163") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.96") (default-features #t) (kind 0)))) (hash "102aj621j2y7mfk6n933whk4hj797nf6ilinc85b2cwx6880li3w")))

