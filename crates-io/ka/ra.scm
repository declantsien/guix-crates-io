(define-module (crates-io ka ra) #:use-module (crates-io))

(define-public crate-kara-0.0.0 (crate (name "kara") (vers "0.0.0") (hash "0390jm6qgxdgmiai2dgbmd71zb1pvk200rmx2cfzlizkq2529v5i")))

(define-public crate-karabiner-0.1 (crate (name "karabiner") (vers "0.1.0") (deps (list (crate-dep (name "karabiner-onetime") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "karabiner-thunk") (req "^0.1") (default-features #t) (kind 0)))) (hash "1bsycy0zrw1szn0klsmj1p6mn3gjkn1xfngivdvi65ahp0b9rnwf")))

(define-public crate-karabiner-driverkit-0.1 (crate (name "karabiner-driverkit") (vers "0.1.0") (deps (list (crate-dep (name "cc") (req "^1.0") (features (quote ("parallel"))) (default-features #t) (kind 1)))) (hash "0s37gfzzzymf5cd68w9f17vginc2c07qwj15rxk49pi37cj3443g")))

(define-public crate-karabiner-driverkit-0.1 (crate (name "karabiner-driverkit") (vers "0.1.1") (deps (list (crate-dep (name "cc") (req "^1.0") (features (quote ("parallel"))) (default-features #t) (kind 1)))) (hash "1rfx8jg4qx7p4jzw0z2661m3hmc3q7knhybpbr44mc9bzj82ijcd")))

(define-public crate-karabiner-driverkit-0.1 (crate (name "karabiner-driverkit") (vers "0.1.2") (deps (list (crate-dep (name "cc") (req "^1.0") (features (quote ("parallel"))) (default-features #t) (kind 1)) (crate-dep (name "os_info") (req "^3.7.0") (default-features #t) (kind 1)))) (hash "16grh9bak0janpzngcq91f3npr93gvn0508h74vy78z1a3ag1h1p")))

(define-public crate-karabiner-driverkit-0.1 (crate (name "karabiner-driverkit") (vers "0.1.3") (deps (list (crate-dep (name "cc") (req "^1.0") (features (quote ("parallel"))) (default-features #t) (kind 1)) (crate-dep (name "os_info") (req "^3.7.0") (default-features #t) (kind 1)))) (hash "0ci9jzjaax6iq227jki5mlagnw08ga7ys589xv2iknc2k8n39cvw")))

(define-public crate-karabiner-onetime-0.1 (crate (name "karabiner-onetime") (vers "0.1.0") (deps (list (crate-dep (name "parking_lot") (req "^0.4") (default-features #t) (kind 0)))) (hash "0nvswjllfhcyg8nawvc442z3888mzgj65xzglgp9mnw3gawkqhcb")))

(define-public crate-karabiner-thunk-0.1 (crate (name "karabiner-thunk") (vers "0.1.0") (deps (list (crate-dep (name "karabiner-onetime") (req "^0.1") (default-features #t) (kind 0)))) (hash "1l7qzcxf9bwyzpmxjfl6srgb2fw4xpvi1w2r17hv6iq8s99hy3w8")))

(define-public crate-karabinux-0.1 (crate (name "karabinux") (vers "0.1.0") (hash "05aac5w1mjnscb1mg8wns1spyriczgqpiv2sxqczks2yajq9mrqq")))

(define-public crate-karabinux_cli-0.1 (crate (name "karabinux_cli") (vers "0.1.0") (hash "0zbalr976py7j6p4my5r9jpmmk3ji6nqx54f2jyd5a04nyyavh8f")))

(define-public crate-karaconv-0.2 (crate (name "karaconv") (vers "0.2.2") (deps (list (crate-dep (name "chrono") (req "^0.4.0") (default-features #t) (kind 0)) (crate-dep (name "failure") (req "^0.1.1") (default-features #t) (kind 0)) (crate-dep (name "failure_derive") (req "^0.1.1") (default-features #t) (kind 0)) (crate-dep (name "itertools") (req "^0.7.7") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^0.2.9") (default-features #t) (kind 0)) (crate-dep (name "result") (req "^1.0.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.30") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde-xml-rs") (req "^0.2.1") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.11") (features (quote ("preserve_order"))) (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.2.5") (default-features #t) (kind 0)) (crate-dep (name "tempdir") (req "^0.3.6") (default-features #t) (kind 2)))) (hash "05l38632mhxlqh2gcibldnz8bcdwcw4l8d6r40z40awfzk9vm0i7")))

(define-public crate-karaconv-0.3 (crate (name "karaconv") (vers "0.3.0") (deps (list (crate-dep (name "chrono") (req "^0.4.0") (default-features #t) (kind 0)) (crate-dep (name "failure") (req "^0.1.1") (default-features #t) (kind 0)) (crate-dep (name "failure_derive") (req "^0.1.1") (default-features #t) (kind 0)) (crate-dep (name "itertools") (req "^0.7.7") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^0.2.9") (default-features #t) (kind 0)) (crate-dep (name "result") (req "^1.0.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.30") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde-xml-rs") (req "^0.2.1") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.11") (features (quote ("preserve_order"))) (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.2.5") (default-features #t) (kind 0)) (crate-dep (name "tempdir") (req "^0.3.6") (default-features #t) (kind 2)))) (hash "1czl0wj271xs1l9gy5y6xx9rkr8w5n41x7a1j2pszb7jnad0r6rc")))

(define-public crate-karakuri-0.1 (crate (name "karakuri") (vers "0.1.0") (hash "0rip513c0p88dni1pz745a0if7vb4pp2fbgr7zmj8wxq3mlkm2bs") (yanked #t)))

(define-public crate-karaorust-0.0.1 (crate (name "karaorust") (vers "0.0.1") (deps (list (crate-dep (name "combine") (req "^1.0.1") (default-features #t) (kind 0)))) (hash "0kaas17hfdj38bhdlwa05pmprvhlyzr59s3yykimw2wnff7sn2j2")))

(define-public crate-karasu-0.1 (crate (name "karasu") (vers "0.1.0") (hash "0ilkin4q2yn3khih7fd33627k1kwds6jssj79fwc5i9hl8nv5lrl")))

(define-public crate-karatsuba-rs-0.1 (crate (name "karatsuba-rs") (vers "0.1.0") (deps (list (crate-dep (name "num-bigint") (req "^0.3") (default-features #t) (kind 0)))) (hash "0g828sv7as1x5jmw1d0xvm1cp8i5ywamgzwmfm8f4kbi5jvyn9ya")))

(define-public crate-karaty-blueprint-0.2 (crate (name "karaty-blueprint") (vers "0.2.0") (deps (list (crate-dep (name "dioxus") (req "^0.4.3") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "reqwasm") (req "^0.5.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "toml") (req "^0.8") (default-features #t) (kind 0)))) (hash "0jpy3489wjf3yyma5p4hvrdqmyzd0zzx6703g6ldgi1i7i672cij")))

(define-public crate-karaty-template-0.2 (crate (name "karaty-template") (vers "0.2.0") (deps (list (crate-dep (name "chrono") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "dioxus") (req "^0.4.3") (default-features #t) (kind 0)) (crate-dep (name "dioxus-retrouter") (req "^0.4.0") (features (quote ("web"))) (default-features #t) (kind 0)) (crate-dep (name "karaty-blueprint") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "markdown") (req "^1.0.0-alpha.16") (default-features #t) (kind 0)) (crate-dep (name "markdown-meta-parser") (req "^0.1.3") (default-features #t) (kind 0)) (crate-dep (name "reqwasm") (req "^0.5") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "toml") (req "^0.8") (default-features #t) (kind 0)))) (hash "1y0c4anbfgpiw854g3bj6mah3cr0rkp300x1kd3mi0mqjlb5cx8h")))

