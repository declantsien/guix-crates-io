(define-module (crates-io ka ma) #:use-module (crates-io))

(define-public crate-kamadak-exif-0.1 (crate (name "kamadak-exif") (vers "0.1.0") (hash "1jinfiwh816mbjajl2r4z6vqk2ki462rmli1jbbyvm9m8bdpvrzp")))

(define-public crate-kamadak-exif-0.1 (crate (name "kamadak-exif") (vers "0.1.1") (hash "1qmiyzdljsnz876zn188v0wsypl1r6iasn0j3r54gq7618jfhyha")))

(define-public crate-kamadak-exif-0.1 (crate (name "kamadak-exif") (vers "0.1.2") (hash "19rvnzmz0pczxjpm2c04npiiwgxmjnlg62kyym7p3x1d8ggvbgcp")))

(define-public crate-kamadak-exif-0.1 (crate (name "kamadak-exif") (vers "0.1.3") (hash "0ml45kbvhb8jj5mvnb7m3j2k2k2m9ffziaz4i3wvc82l9nrddmh7")))

(define-public crate-kamadak-exif-0.2 (crate (name "kamadak-exif") (vers "0.2.0") (hash "1n8s9z6fjg2h9k90bhpy1vdk1zphnkb2i09rxwrs9db8qfmixysn")))

(define-public crate-kamadak-exif-0.2 (crate (name "kamadak-exif") (vers "0.2.1") (hash "0va5l66c7b7rm12sj73d8j6dz5kxiskzhnmxr9q4znshqb0j8ya2")))

(define-public crate-kamadak-exif-0.2 (crate (name "kamadak-exif") (vers "0.2.2") (hash "1l9jikngpry4d91bwz5fmml95lagavmp6ws4nbwygk5mbkndgl5v")))

(define-public crate-kamadak-exif-0.2 (crate (name "kamadak-exif") (vers "0.2.3") (hash "02pnly0rs7yhwp7p6lg58h71pkzvgc1lsbr5hhl0jb58nmdrykc4")))

(define-public crate-kamadak-exif-0.3 (crate (name "kamadak-exif") (vers "0.3.0") (hash "1mh1inh7540a34wzjr3ic2kmragg5s44d08nmlmfdfvp5a0xyf22")))

(define-public crate-kamadak-exif-0.3 (crate (name "kamadak-exif") (vers "0.3.1") (hash "1bcp3xa02y8i1srgkmi6x66n3fzj7ycabf8xdnljzbddspa6scbc")))

(define-public crate-kamadak-exif-0.4 (crate (name "kamadak-exif") (vers "0.4.0") (deps (list (crate-dep (name "mutate_once") (req "^0.1.1") (default-features #t) (kind 0)))) (hash "0mqwm6i6b47fbliysw3k4yc6ym695j5v5k2wla5hwmv107gyicv8")))

(define-public crate-kamadak-exif-0.5 (crate (name "kamadak-exif") (vers "0.5.0") (deps (list (crate-dep (name "mutate_once") (req "^0.1.1") (default-features #t) (kind 0)))) (hash "1fkph8dcjlbs544g452ajv1l9fzm6g0h3cp3ybj7nf7hxh9siy0c")))

(define-public crate-kamadak-exif-0.5 (crate (name "kamadak-exif") (vers "0.5.1") (deps (list (crate-dep (name "mutate_once") (req "^0.1.1") (default-features #t) (kind 0)))) (hash "1rxcvcxaxj5lbdp7p62lzpj8k2d48plg1xqihq1j34s6npancpka")))

(define-public crate-kamadak-exif-0.5 (crate (name "kamadak-exif") (vers "0.5.2") (deps (list (crate-dep (name "mutate_once") (req "^0.1.1") (default-features #t) (kind 0)))) (hash "1a0ac77fp117vd3qdws1kp0947mgzd7dai5agi74yz9z6yk24ksj")))

(define-public crate-kamadak-exif-0.5 (crate (name "kamadak-exif") (vers "0.5.3") (deps (list (crate-dep (name "mutate_once") (req "^0.1.1") (default-features #t) (kind 0)))) (hash "1qsi1jv05bwi6k61p71z90d60dj0wz2h4ackg7ib6dm32s5icff2")))

(define-public crate-kamadak-exif-0.5 (crate (name "kamadak-exif") (vers "0.5.4") (deps (list (crate-dep (name "mutate_once") (req "^0.1.1") (default-features #t) (kind 0)))) (hash "1dng0brs7fa8sxbcrrpd01qyn9wn1kbwal9rxf8y9y1b95j4jjbh")))

(define-public crate-kamadak-exif-0.5 (crate (name "kamadak-exif") (vers "0.5.5") (deps (list (crate-dep (name "mutate_once") (req "^0.1.1") (default-features #t) (kind 0)))) (hash "0xw0lpmra8j1y98c0agwrmjajpkh91mnl89hzaxbdrdp186wfkzg")))

(define-public crate-kamajii-0.0.0 (crate (name "kamajii") (vers "0.0.0") (hash "0jbzc9g9p63b5cmm2dnkgwqchlvwkxln5aa1c74axdy3q9m59lby") (yanked #t)))

(define-public crate-kamali_test-0.1 (crate (name "kamali_test") (vers "0.1.0") (hash "0mn0m1j25wi5jk0wj2wp7bz5g23n7ci6k8rv8qr15liwwxji7fzg")))

(define-public crate-kamazik-0.1 (crate (name "kamazik") (vers "0.1.0") (deps (list (crate-dep (name "anyhow") (req "^1.0.79") (default-features #t) (kind 0)) (crate-dep (name "eframe") (req "^0.26.0") (features (quote ("default"))) (default-features #t) (kind 0)) (crate-dep (name "egui_extras") (req "^0.26.0") (features (quote ("default" "image"))) (default-features #t) (kind 0)) (crate-dep (name "env_logger") (req "^0.11.1") (features (quote ("auto-color" "humantime"))) (kind 0)) (crate-dep (name "futures-lite") (req "^2.2.0") (default-features #t) (kind 0)) (crate-dep (name "ruma") (req "^0.9.4") (features (quote ("client-api-c" "client-ext-client-api" "client-reqwest-rustls-native-roots"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.196") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "toml") (req "^0.8.10") (default-features #t) (kind 0)) (crate-dep (name "toml_edit") (req "^0.22.4") (default-features #t) (kind 0)) (crate-dep (name "url") (req "^2.5.0") (default-features #t) (kind 0)))) (hash "1yygf5pim2cwvdzb482x8chszn0hxgqwk2v2j0bpina4kjffqcqr")))

