(define-module (crates-io ka re) #:use-module (crates-io))

(define-public crate-karen-0.1 (crate (name "karen") (vers "0.1.0") (deps (list (crate-dep (name "libc") (req "~0.2") (default-features #t) (kind 0)) (crate-dep (name "log") (req "~0.4") (default-features #t) (kind 0)) (crate-dep (name "simple_logger") (req ">=4") (default-features #t) (kind 2)))) (hash "1skq3rx7rnbnvnqqppbx6rr6mdqrqhvda2zgin9mpvv10a4h12r1")))

