(define-module (crates-io ka or) #:use-module (crates-io))

(define-public crate-kaori-hsm-0.1 (crate (name "kaori-hsm") (vers "0.1.0") (deps (list (crate-dep (name "kaori-hsm-derive") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 2)))) (hash "1wk9kzj1g7a6psvvl9ai7m913mrz9ah7d7awgma77gqv2m52z2mq")))

(define-public crate-kaori-hsm-0.1 (crate (name "kaori-hsm") (vers "0.1.1") (deps (list (crate-dep (name "kaori-hsm-derive") (req "^0.1.1") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 2)))) (hash "1dypz6fx75vkjg5gc5j9pz90qi717ss1j9cq68bzafiq0nlg2261") (rust-version "1.70")))

(define-public crate-kaori-hsm-derive-0.1 (crate (name "kaori-hsm-derive") (vers "0.1.0") (deps (list (crate-dep (name "proc-macro2") (req "^1.0.75") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.35") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2.0") (features (quote ("full" "extra-traits"))) (default-features #t) (kind 0)))) (hash "0gcjv3avc1c14lgib6k2avy7mkv1kbjwih0h7s2q8x0wmqzbsr5c")))

(define-public crate-kaori-hsm-derive-0.1 (crate (name "kaori-hsm-derive") (vers "0.1.1") (deps (list (crate-dep (name "proc-macro2") (req "^1.0.75") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.35") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2.0") (features (quote ("full" "extra-traits"))) (default-features #t) (kind 0)))) (hash "073wid9mgb1d0q64qir9vqvzq6m44y9snvs12sccccg5af96i2jd") (rust-version "1.70")))

