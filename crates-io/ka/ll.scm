(define-module (crates-io ka ll) #:use-module (crates-io))

(define-public crate-kallisti-0.1 (crate (name "kallisti") (vers "0.1.0") (hash "1112vlp69p4pdh7hhpz52r2g41yc0kgx0whn8akzaa5pk44nzjyb")))

(define-public crate-kalloc-0.1 (crate (name "kalloc") (vers "0.1.0") (hash "18g955vikkpn7x73ivw72jjh2qc7p93wy9pcs8hj1q59n9d5skdn")))

