(define-module (crates-io ka tj) #:use-module (crates-io))

(define-public crate-katjing-0.1 (crate (name "katjing") (vers "0.1.0") (deps (list (crate-dep (name "doc-comment") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "iso_currency") (req "^0.4.3") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0") (default-features #t) (kind 0)))) (hash "139wwrbpckrhp1v75prr74kf3scs1r5jfh3vjj0z99gfga7ccnjr")))

