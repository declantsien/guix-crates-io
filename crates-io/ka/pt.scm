(define-module (crates-io ka pt) #:use-module (crates-io))

(define-public crate-kapta-0.0.0 (crate (name "kapta") (vers "0.0.0") (hash "0k2b564lw9vxs9spirv3cr4zmznm9hzh6ss1dhhjdwlrlz2rn3ii")))

(define-public crate-kapta-0.0.1 (crate (name "kapta") (vers "0.0.1") (deps (list (crate-dep (name "approx") (req "^0.5") (default-features #t) (kind 0)) (crate-dep (name "geo") (req "^0.26.0") (default-features #t) (kind 0)) (crate-dep (name "geo-types") (req "^0.7.11") (features (quote ("serde"))) (default-features #t) (kind 0)))) (hash "0jdqvqysv23cw6mannlfrjhb3f58wzq50lnihn4vj77bnrxyj3cq")))

(define-public crate-kapta-0.0.2 (crate (name "kapta") (vers "0.0.2") (deps (list (crate-dep (name "approx") (req "^0.5") (default-features #t) (kind 0)) (crate-dep (name "geo") (req "^0.26.0") (default-features #t) (kind 0)) (crate-dep (name "geo-types") (req "^0.7.11") (features (quote ("serde"))) (default-features #t) (kind 0)))) (hash "10wbyl0frv4fhm7pj53dvc3x0a6c54ckzf58d0nwj7dq7z6396hf")))

(define-public crate-kapta-0.0.3 (crate (name "kapta") (vers "0.0.3") (deps (list (crate-dep (name "approx") (req "^0.5") (default-features #t) (kind 0)) (crate-dep (name "geo") (req "^0.26.0") (default-features #t) (kind 0)) (crate-dep (name "geo-types") (req "^0.7.11") (features (quote ("serde"))) (default-features #t) (kind 0)))) (hash "1gl7b11xdla16w35c1gxzapcxah1pm2pldhg926afxh8391b2lbn")))

(define-public crate-kapta-0.0.4 (crate (name "kapta") (vers "0.0.4") (deps (list (crate-dep (name "approx") (req "^0.5") (default-features #t) (kind 0)) (crate-dep (name "geo") (req "^0.26.0") (default-features #t) (kind 0)) (crate-dep (name "geo-types") (req "^0.7.11") (features (quote ("serde"))) (default-features #t) (kind 0)) (crate-dep (name "geojson") (req "^0.24.1") (default-features #t) (kind 0)))) (hash "1xfxf8l424r3iw55z2az3cvndb28z0v8ndyj85girygw7l4djp3m")))

