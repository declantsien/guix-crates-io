(define-module (crates-io ka ns) #:use-module (crates-io))

(define-public crate-kansas-0.1 (crate (name "kansas") (vers "0.1.0") (deps (list (crate-dep (name "gfx") (req "^0.16") (default-features #t) (kind 0)) (crate-dep (name "gfx_device_gl") (req "^0.14") (default-features #t) (kind 0)) (crate-dep (name "gfx_window_glutin") (req "^0.17") (default-features #t) (kind 0)) (crate-dep (name "glutin") (req "^0.9") (default-features #t) (kind 0)))) (hash "0mf7s955y6l7ad749h72zqr4arciyzbk5zmdk10n2ywbclyv1bk6")))

(define-public crate-kanshi-0.1 (crate (name "kanshi") (vers "0.1.0") (deps (list (crate-dep (name "edid") (req "^0.1.1") (default-features #t) (kind 0)) (crate-dep (name "getopts") (req "^0.2.14") (default-features #t) (kind 0)) (crate-dep (name "xmltree") (req "^0.4.0") (default-features #t) (kind 0)))) (hash "1m3h52w7hflfy087df61y1gh0hl66lqgy6zs3hjjw0f4kvp84rs3") (yanked #t)))

(define-public crate-kanshi-0.1 (crate (name "kanshi") (vers "0.1.1") (deps (list (crate-dep (name "edid") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "getopts") (req "^0.2.14") (default-features #t) (kind 0)) (crate-dep (name "xmltree") (req "^0.4.0") (default-features #t) (kind 0)))) (hash "0jf5jh3574p9hzx1j3g155bfhiwzg3qssp8acxfjfrvanzd0ga72") (yanked #t)))

(define-public crate-kanshi-0.2 (crate (name "kanshi") (vers "0.2.0") (deps (list (crate-dep (name "edid") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "getopts") (req "^0.2.14") (default-features #t) (kind 0)) (crate-dep (name "nom") (req "^3.2.0") (features (quote ("verbose-errors"))) (default-features #t) (kind 0)) (crate-dep (name "xmltree") (req "^0.4.0") (default-features #t) (kind 0)))) (hash "0x5qb2wfb2rqdn5krlwg5ck2wasq55jwpzp8y74rbgaqs0cqs1bj") (yanked #t)))

(define-public crate-kanshi-0.2 (crate (name "kanshi") (vers "0.2.1") (deps (list (crate-dep (name "edid") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "getopts") (req "^0.2.14") (default-features #t) (kind 0)) (crate-dep (name "nom") (req "^3.2.0") (features (quote ("verbose-errors"))) (default-features #t) (kind 0)) (crate-dep (name "xmltree") (req "^0.4.0") (default-features #t) (kind 0)))) (hash "01c1x70qx6z6glkfy0m44k0vnky25vclv2g6pnsmv0kawkli7faq") (yanked #t)))

(define-public crate-kanshi-rs-0.1 (crate (name "kanshi-rs") (vers "0.1.0") (hash "1lmpjmalkiskizja3zjqypyybwx2pk0r40kjhv6plrdqdn0as62w")))

(define-public crate-kanso-0.0.1 (crate (name "kanso") (vers "0.0.1") (hash "1d8qy6v37lixwz8rxxs32088wnxkp1xylvq8ajqiqw7vqb8l9vcn")))

(define-public crate-kansuji-0.1 (crate (name "kansuji") (vers "0.1.0") (deps (list (crate-dep (name "thiserror") (req "^1.0.57") (default-features #t) (kind 0)))) (hash "1gpgkl8hr0jrbhljrm6vj6qwgjbh6glxg22rvparf3bh75vvaiqq") (rust-version "1.56.1")))

(define-public crate-kansuji-0.1 (crate (name "kansuji") (vers "0.1.1") (deps (list (crate-dep (name "thiserror") (req "^1.0.57") (default-features #t) (kind 0)))) (hash "120d61pa6459bpjfv7g2cklanvcr0gv5a4699zz4fn50zryr6lgd") (rust-version "1.56.1")))

