(define-module (crates-io ka se) #:use-module (crates-io))

(define-public crate-kase-0.1 (crate (name "kase") (vers "0.1.1") (deps (list (crate-dep (name "anyhow") (req "^1.0.65") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^4.0.17") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "strum") (req "^0.24.1") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "strum_macros") (req "^0.24") (default-features #t) (kind 0)))) (hash "0r6bkhf159zgxg0jvppv8l0d44kr5bbk18p0ics0pwq6p6xy8afa")))

(define-public crate-kase-0.1 (crate (name "kase") (vers "0.1.2") (deps (list (crate-dep (name "anyhow") (req "^1.0.65") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^4.0.17") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "strum") (req "^0.24.1") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "strum_macros") (req "^0.24") (default-features #t) (kind 0)))) (hash "14g8v07d91q38w3zpbw1kh4b55x8svsb3xlqayqwggacy14hbh05")))

(define-public crate-kase-0.1 (crate (name "kase") (vers "0.1.3") (deps (list (crate-dep (name "anyhow") (req "^1.0.65") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^4.0.17") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "strum") (req "^0.24.1") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "strum_macros") (req "^0.24") (default-features #t) (kind 0)))) (hash "0yiczp7ni2l1bd3cxl1017hagy0h80ypxk81hasj39jnf51ada22")))

(define-public crate-kasedenv-0.1 (crate (name "kasedenv") (vers "0.1.0") (deps (list (crate-dep (name "unicase") (req "^2.7.0") (optional #t) (default-features #t) (kind 0)))) (hash "1wwdmm3ki3c1qgbpclfyki451q3x606hg0b2yfcmfhlgiak1wkbn") (v 2) (features2 (quote (("unicode" "dep:unicase"))))))

