(define-module (crates-io ka zy) #:use-module (crates-io))

(define-public crate-kazyol_chat-0.1 (crate (name "kazyol_chat") (vers "0.1.0") (deps (list (crate-dep (name "derive_builder") (req "^0.10.0-alpha") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.119") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.61") (default-features #t) (kind 0)))) (hash "0pkwz17zmjdvgy1v9w0ysj391l110bmj6w7ksicn200j1i4348r0")))

(define-public crate-kazyol_chat-0.1 (crate (name "kazyol_chat") (vers "0.1.1") (deps (list (crate-dep (name "derive_builder") (req "^0.10.0-alpha") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.119") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.61") (default-features #t) (kind 0)))) (hash "12mbl1djk3067hh2jq5wqx1l78w42wz4n6lva3f8msml1872w4an")))

