(define-module (crates-io ka kt) #:use-module (crates-io))

(define-public crate-kaktovik-0.1 (crate (name "kaktovik") (vers "0.1.0") (hash "1nh660bs1dm1izywnh894kr9ivnz322n2lf5i7lhskzy8ain2070")))

(define-public crate-kaktovik-0.1 (crate (name "kaktovik") (vers "0.1.1") (hash "0ygs15q3v1cjd0z4h1sdgdq35ii1njykwhbrwr00pp2011il1adj")))

(define-public crate-kaktovik-0.1 (crate (name "kaktovik") (vers "0.1.2") (deps (list (crate-dep (name "criterion") (req "^0.4.0") (default-features #t) (kind 2)))) (hash "1zw9kxf7ql6qmjvn0w3k7qjmqwmr3839h32clvg0vrfn4060zp0p")))

(define-public crate-kaktovik-0.1 (crate (name "kaktovik") (vers "0.1.3") (deps (list (crate-dep (name "criterion") (req "^0.4.0") (default-features #t) (kind 2)))) (hash "1a7agr90h322ybbn54vfbn7zjff2aq7q77n4ivhni03kbpyxw3y9")))

(define-public crate-kaktovik-0.1 (crate (name "kaktovik") (vers "0.1.4") (deps (list (crate-dep (name "criterion") (req "^0.4.0") (default-features #t) (kind 2)))) (hash "0vk5gp28wvdzjmzdjp14xp72w27xcwb6c5pqxcsk79k12381ibcd")))

(define-public crate-kaktus-0.1 (crate (name "kaktus") (vers "0.1.0") (hash "1vcsxhcj6v1lwjbqcs9b7xab02qdqc5gkyvfsqppxm5w3wv4rak0")))

(define-public crate-kaktus-0.1 (crate (name "kaktus") (vers "0.1.1") (hash "1qyzv8p9qs8zgrvk20wvsf0ysvislpdxys33dm3i2aql3865zwi1")))

(define-public crate-kaktus-0.1 (crate (name "kaktus") (vers "0.1.2") (hash "0n9pn4fhnhw4gmwf1jihyvasw8f2r6zp3svgl7zjznh2y3m13q6y")))

(define-public crate-kaktus-0.1 (crate (name "kaktus") (vers "0.1.3") (hash "1574ryns057mfz0jhpna16hy6483rkwb4jjyvp7a45zkjcjj87ka")))

