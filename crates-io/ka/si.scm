(define-module (crates-io ka si) #:use-module (crates-io))

(define-public crate-kasi-kule-0.1 (crate (name "kasi-kule") (vers "0.1.0") (deps (list (crate-dep (name "criterion") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "fastrand") (req "^1.6.0") (default-features #t) (kind 2)) (crate-dep (name "lab") (req "^0.11") (default-features #t) (kind 2)) (crate-dep (name "lazy_static") (req "^1") (default-features #t) (kind 0)))) (hash "0h2npg78qgnqyx9vnws0y5d9j7vl1zqaif7mhmm1navzryamwxy4")))

(define-public crate-kasi-kule-0.2 (crate (name "kasi-kule") (vers "0.2.0") (deps (list (crate-dep (name "criterion") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "fastrand") (req "^1.6.0") (default-features #t) (kind 2)) (crate-dep (name "lab") (req "^0.11") (default-features #t) (kind 2)))) (hash "1xlzhd6ca14wb62sjr73vxmlx608maik42bfyqvdfharshpxq7p2")))

(define-public crate-kasi-kule-0.3 (crate (name "kasi-kule") (vers "0.3.0") (deps (list (crate-dep (name "criterion") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "fastrand") (req "^1.6.0") (default-features #t) (kind 2)) (crate-dep (name "lab") (req "^0.11") (default-features #t) (kind 2)))) (hash "08adah28d8paa042rdfjicjzskqplks1jw3w855qbrw3wmip8lij")))

(define-public crate-kasi-kule-0.3 (crate (name "kasi-kule") (vers "0.3.1") (deps (list (crate-dep (name "criterion") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "fastrand") (req "^1.6.0") (default-features #t) (kind 2)) (crate-dep (name "lab") (req "^0.11") (default-features #t) (kind 2)))) (hash "0jj5k897svki2lj8mmdsj62xd9y15j76nq4m4bqgid2nf1wi3v18")))

(define-public crate-kasi-kule-0.3 (crate (name "kasi-kule") (vers "0.3.2") (deps (list (crate-dep (name "criterion") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "fastrand") (req "^1.6.0") (default-features #t) (kind 2)) (crate-dep (name "lab") (req "^0.11") (default-features #t) (kind 2)))) (hash "05z5rybrj0ww8lfkg88m7vh93lsjwrpj35fdrz70zg0vg2ykdd52")))

(define-public crate-kasi-kule-0.3 (crate (name "kasi-kule") (vers "0.3.4") (deps (list (crate-dep (name "approx") (req "^0.5.0") (default-features #t) (kind 2)) (crate-dep (name "criterion") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "fastrand") (req "^1.6.0") (default-features #t) (kind 2)) (crate-dep (name "lab") (req "^0.11") (default-features #t) (kind 2)) (crate-dep (name "micromath") (req "^2.0.0") (optional #t) (default-features #t) (kind 0)))) (hash "1h0d8nmli75zc81zh5kg12648vms9zy08zkinpi8amzr4zqfi304") (features (quote (("sse") ("approximate_math" "micromath"))))))

