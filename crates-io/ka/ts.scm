(define-module (crates-io ka ts) #:use-module (crates-io))

(define-public crate-katsuyou-0.0.1 (crate (name "katsuyou") (vers "0.0.1") (deps (list (crate-dep (name "clap") (req "^2.32.0") (default-features #t) (kind 0)) (crate-dep (name "human-panic") (req "^1.0.1") (default-features #t) (kind 0)))) (hash "1jzixsf3z95w0dzz336vv8gaad2iavaklk4v268xhd2pjmw11qyv")))

