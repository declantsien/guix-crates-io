(define-module (crates-io ka hu) #:use-module (crates-io))

(define-public crate-kahuna-0.1 (crate (name "kahuna") (vers "0.1.0") (deps (list (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)))) (hash "06pllz8xcy7wxmnv278vmjiczq9cwf2ywiwd0vb04g2nv7hqhlaf")))

(define-public crate-kahuna-0.2 (crate (name "kahuna") (vers "0.2.0") (deps (list (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)))) (hash "1ky99bl9wmnnzj21fqmkbz9xignn3b8yidmzivsbj1fz4679i6hl")))

(define-public crate-kahuna-0.3 (crate (name "kahuna") (vers "0.3.0") (deps (list (crate-dep (name "image") (req "^0.24.2") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)))) (hash "01qng03vkh7a3yfrncral0swlcdmzihs3h7c6yfqyhk7pcdqlx8n")))

