(define-module (crates-io ka fi) #:use-module (crates-io))

(define-public crate-kafi-0.1 (crate (name "kafi") (vers "0.1.0") (deps (list (crate-dep (name "bincode") (req "^0.9.2") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0") (default-features #t) (kind 0)))) (hash "1x2iqwxq8axk3wfhpn97p81n9dsrijz6s0mpil2h4kqgbvif42sx")))

(define-public crate-kafi-0.1 (crate (name "kafi") (vers "0.1.1") (deps (list (crate-dep (name "bincode") (req "^0.9.2") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "serde") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0") (default-features #t) (kind 0)))) (hash "0n1frp85dzw8w4q84w1y6xj6cik6mlr5jvq98j1ab3n1mc8zfxb8")))

(define-public crate-kafi-0.1 (crate (name "kafi") (vers "0.1.2") (deps (list (crate-dep (name "bincode") (req "^0.9.2") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "serde") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0") (default-features #t) (kind 0)))) (hash "15s4maqqgbzvxaamf1p9nd68zwvxisk52lyqnjrwhdmma303kkin")))

