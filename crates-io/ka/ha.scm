(define-module (crates-io ka ha) #:use-module (crates-io))

(define-public crate-kahan-0.1 (crate (name "kahan") (vers "0.1.0") (deps (list (crate-dep (name "num-traits") (req "^0.1") (default-features #t) (kind 0)))) (hash "0cb5763iwlmwz12ic9sbizg2p62iwy0r6ywr6bhd3wm1qca9gaz5")))

(define-public crate-kahan-0.1 (crate (name "kahan") (vers "0.1.2") (deps (list (crate-dep (name "num-traits") (req "^0.1") (default-features #t) (kind 0)))) (hash "1fbr877ip5qa68b3x2qgqfsadbsn3rblrwgqq499gl4l47h3x9g7")))

(define-public crate-kahan-0.1 (crate (name "kahan") (vers "0.1.3") (deps (list (crate-dep (name "num-traits") (req "^0.1") (default-features #t) (kind 0)))) (hash "1vwpcn416gpn2xxhdkmdy3v7fqh46pqzdja0mss5ww4r90q5a4b5")))

(define-public crate-kahan-0.1 (crate (name "kahan") (vers "0.1.4") (deps (list (crate-dep (name "num-traits") (req "^0.2") (default-features #t) (kind 0)))) (hash "0y8zgnxmy1sipyyc7av7vafvsqrmw9b289z852z88qzymw5va77j")))

(define-public crate-kahan_pairs-1 (crate (name "kahan_pairs") (vers "1.0.0") (deps (list (crate-dep (name "libm") (req "^0.2") (optional #t) (default-features #t) (kind 0)))) (hash "01k4kpf143gazy90gyzfapwlzn654172nigdx1d1v1v8b89ffzi6") (features (quote (("std") ("default" "std"))))))

