(define-module (crates-io ka nd) #:use-module (crates-io))

(define-public crate-kandilli-0.1 (crate (name "kandilli") (vers "0.1.0") (deps (list (crate-dep (name "chrono") (req "^0.4.23") (default-features #t) (kind 0)) (crate-dep (name "dateparser") (req "^0.1.8") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "tl") (req "^0.7.7") (features (quote ("simd"))) (default-features #t) (kind 0)) (crate-dep (name "ureq") (req "^2.6.2") (default-features #t) (kind 0)))) (hash "1mxzyszfnfdl5sjvghql66hdl7mp0y3iw5x2gmm3xdv9hm8a6h03")))

