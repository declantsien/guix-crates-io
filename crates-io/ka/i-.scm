(define-module (crates-io ka i-) #:use-module (crates-io))

(define-public crate-kai-cli-0.1 (crate (name "kai-cli") (vers "0.1.0") (deps (list (crate-dep (name "chrono") (req "^0.4.19") (default-features #t) (kind 0)) (crate-dep (name "dirs") (req "^4.0.0") (default-features #t) (kind 0)) (crate-dep (name "getopts") (req "^0.2") (default-features #t) (kind 0)))) (hash "045qjhqi65nz11ppcmbs8621y00nqry2939niqcwnzl48i49q325")))

(define-public crate-kai-rs-0.1 (crate (name "kai-rs") (vers "0.1.0") (deps (list (crate-dep (name "anyhow") (req "^1.0.70") (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.11.16") (features (quote ("json"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.160") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.96") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.28.0") (features (quote ("rt-multi-thread" "macros"))) (default-features #t) (kind 0)))) (hash "182kj2hd5rd70m0hsrswkhar53cqmis43yr0yk75sw1fz97ggqxm")))

