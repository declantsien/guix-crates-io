(define-module (crates-io zd ex) #:use-module (crates-io))

(define-public crate-zdex-0.1 (crate (name "zdex") (vers "0.1.0") (deps (list (crate-dep (name "bit_collection") (req "^0.2.3") (default-features #t) (kind 0)) (crate-dep (name "vob") (req "^2.0.4") (default-features #t) (kind 0)))) (hash "1y8faqikxl9ni8a7hd68k3blfmbgp6nh3pwmisrpjjd8pshhm943")))

(define-public crate-zdex-0.2 (crate (name "zdex") (vers "0.2.0") (deps (list (crate-dep (name "bit_collection") (req "^0.2.3") (default-features #t) (kind 0)) (crate-dep (name "vob") (req "^2.0.4") (default-features #t) (kind 0)))) (hash "15q9myznxkhjsf4fxx7fywp9zdikwjm7snmpnh4fqjk3n1a3g39n")))

(define-public crate-zdex-0.3 (crate (name "zdex") (vers "0.3.0") (deps (list (crate-dep (name "bit_collection") (req "^0.2.3") (default-features #t) (kind 0)) (crate-dep (name "vob") (req "^2.0.4") (default-features #t) (kind 0)))) (hash "0rf4ww84c8whkz8rzbphj56i6nppc1yakkjikvrb9sr1s3xhs0vk")))

(define-public crate-zdex-0.3 (crate (name "zdex") (vers "0.3.1") (deps (list (crate-dep (name "bit_collection") (req "^0.2.3") (default-features #t) (kind 0)) (crate-dep (name "vob") (req "^2.0.4") (default-features #t) (kind 0)))) (hash "16gjsn3wqgp1h896kh28r9ayncfp7xhm8zbrn67r9wb5xmry6n9a")))

