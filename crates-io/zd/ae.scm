(define-module (crates-io zd ae) #:use-module (crates-io))

(define-public crate-zdaemon-0.0.2 (crate (name "zdaemon") (vers "0.0.2") (deps (list (crate-dep (name "czmq") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "rustc-serialize") (req "0.3.*") (default-features #t) (kind 0)) (crate-dep (name "tempdir") (req "0.3.*") (default-features #t) (kind 2)))) (hash "0fdakdarkzkh3sks7mzyvhrxfmclklji3bh4b2zpcax90661qkav") (yanked #t)))

