(define-module (crates-io zd um) #:use-module (crates-io))

(define-public crate-zdump-0.1 (crate (name "zdump") (vers "0.1.0") (deps (list (crate-dep (name "structopt") (req "^0.3.2") (kind 0)) (crate-dep (name "tzparse") (req "^0.3.4") (default-features #t) (kind 0)))) (hash "04alarx7ilbc29qz6jbizy5lqnh01f992ix1g5hss4hybav79yl9")))

(define-public crate-zdump-0.1 (crate (name "zdump") (vers "0.1.1") (deps (list (crate-dep (name "structopt") (req "^0.3.2") (kind 0)) (crate-dep (name "tzparse") (req "^0.3.4") (default-features #t) (kind 0)))) (hash "1dsykjfdq4jq3zzn3rh6nxwladwi65k20d0gyimnavf327x4n75r")))

(define-public crate-zdump-0.1 (crate (name "zdump") (vers "0.1.2") (deps (list (crate-dep (name "structopt") (req "^0.3.2") (kind 0)) (crate-dep (name "tzparse") (req "^0.3.5") (default-features #t) (kind 0)))) (hash "12s5slk1lwak34pvmxpx91k4p91skz7a0izls8941z2xycphq7df")))

(define-public crate-zdump-0.2 (crate (name "zdump") (vers "0.2.0") (deps (list (crate-dep (name "chrono") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.3.2") (kind 0)) (crate-dep (name "tzparse") (req "^0.4.0") (default-features #t) (kind 0)))) (hash "1alfpkvphg94ji8j9z1rmdipxg62cv7ki0fr9d8mdv9g1cja39fv")))

(define-public crate-zdump-0.2 (crate (name "zdump") (vers "0.2.2") (deps (list (crate-dep (name "chrono") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.3.2") (kind 0)) (crate-dep (name "tzparse") (req "^0.5.0") (default-features #t) (kind 0)))) (hash "0da0hgyi6zqkjrbl6c4aclrfw9qzbfakr43jgil0014gd21sczzf")))

(define-public crate-zdump-0.2 (crate (name "zdump") (vers "0.2.4") (deps (list (crate-dep (name "structopt") (req "^0.3.2") (kind 0)) (crate-dep (name "tzparse") (req "^0.5.1") (default-features #t) (kind 0)))) (hash "0pd9c2qnwww0zmmz1sr3qrbnp2i3s0fq620xw9b2s7m5ij3x9mgh")))

(define-public crate-zdump-0.2 (crate (name "zdump") (vers "0.2.5") (deps (list (crate-dep (name "structopt") (req "^0.3.2") (kind 0)) (crate-dep (name "tzparse") (req "^0.5.2") (default-features #t) (kind 0)))) (hash "1fqz6qdcyazg07mmkrbfv8vr3rlciygrkhpjh4zq9kljf6d272d8")))

(define-public crate-zdump-1 (crate (name "zdump") (vers "1.0.0") (deps (list (crate-dep (name "libtzfile") (req "^1.0.5") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.3.2") (kind 0)) (crate-dep (name "tzparse") (req "^1.0.1") (default-features #t) (kind 0)))) (hash "1iyf4x671c9hg95ng5z5ssjf4cbxvmrxpvkjp3zd5fzbvxaviirv")))

(define-public crate-zdump-1 (crate (name "zdump") (vers "1.0.1") (deps (list (crate-dep (name "libtzfile") (req "^1.0.6") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.3.2") (kind 0)) (crate-dep (name "tzparse") (req "^1.0.2") (default-features #t) (kind 0)))) (hash "0qsf0v78df3xq6si4l9mxr600z91f9av59md9ikn4vf0kw78schz")))

(define-public crate-zdump-1 (crate (name "zdump") (vers "1.0.2") (deps (list (crate-dep (name "structopt") (req "^0.3.2") (kind 0)) (crate-dep (name "tzparse") (req "^1.0.3") (default-features #t) (kind 0)))) (hash "1gyibq0mm2w7j30crs5608g4gzwzjl8zisnvhplrai90sm87fjj3")))

(define-public crate-zdump-1 (crate (name "zdump") (vers "1.1.0") (deps (list (crate-dep (name "tzparse") (req "^1.1") (default-features #t) (kind 0)))) (hash "14hgj1p9a1nb2z6s5nf5c19h71hrpi0jxkclzq07mx35m2w67kba")))

(define-public crate-zdump-1 (crate (name "zdump") (vers "1.2.0") (deps (list (crate-dep (name "tzparse") (req "^1.1") (default-features #t) (kind 0)))) (hash "02hhw3zdf6vfj76kb7sib87lczhbm0xiy2qlq64nfhw4m4324qjl")))

(define-public crate-zdump-1 (crate (name "zdump") (vers "1.2.1") (deps (list (crate-dep (name "tzparse") (req "^1.1") (default-features #t) (kind 0)))) (hash "0xgjzyz2ficgrmjcfk7w5x3spdag4i87ka2qrqncapi2wffvh73z")))

(define-public crate-zdump-1 (crate (name "zdump") (vers "1.2.2") (deps (list (crate-dep (name "tzparse") (req "^1.1") (default-features #t) (kind 0)))) (hash "1jfgih95qsi11p98220hbjfl7kfbkc6gc1wc0v1m4130maf59sv3")))

(define-public crate-zdump-1 (crate (name "zdump") (vers "1.2.3") (deps (list (crate-dep (name "libtzfile") (req "^2.0") (features (quote ("parse"))) (default-features #t) (kind 0)))) (hash "0plbm5glpl5660hygldgbi2zvaw9v7sv14gm46gi9ca32m86sl51")))

(define-public crate-zdump-1 (crate (name "zdump") (vers "1.3.0") (deps (list (crate-dep (name "libtzfile") (req "^2.0") (features (quote ("parse"))) (default-features #t) (kind 0)))) (hash "0ffg42d4yq4iyh5x7wc8pfz0wrsc9707bl30ib9dxn7by0z6n79n")))

(define-public crate-zdump-1 (crate (name "zdump") (vers "1.3.1") (deps (list (crate-dep (name "libtzfile") (req "^2.0") (features (quote ("parse"))) (default-features #t) (kind 0)))) (hash "02342w6alwinz9kpngwz8kvjy88dhgxsf88r4mhsl46m7by7mywz")))

(define-public crate-zdump-1 (crate (name "zdump") (vers "1.3.2") (deps (list (crate-dep (name "libtzfile") (req "^2.0") (features (quote ("parse"))) (default-features #t) (kind 0)))) (hash "1g5wjyhahf8nd0zymhjd736gyawlkxidczmq4cf6nyp3ygkkqv6s")))

(define-public crate-zdump-1 (crate (name "zdump") (vers "1.3.3") (deps (list (crate-dep (name "libtzfile") (req "^2.0") (features (quote ("parse"))) (default-features #t) (kind 0)))) (hash "1zvafrrmn1r51ahjxlr3ym63m196c2gy5hv5hswy83z6dgr640im")))

(define-public crate-zdump-1 (crate (name "zdump") (vers "1.3.4") (deps (list (crate-dep (name "libtzfile") (req "^2.0") (features (quote ("parse"))) (default-features #t) (kind 0)))) (hash "05jgbg28hv21hc60zkvbc7cyy3g7kv55ypjcp01wpkhzwz53aj0k")))

(define-public crate-zdump-1 (crate (name "zdump") (vers "1.3.5") (deps (list (crate-dep (name "libtzfile") (req "^3.0") (features (quote ("parse"))) (default-features #t) (kind 0)))) (hash "04pndwcx512ik7kdi10rny5myfx9dz1rvb1b3ngjvmymsms9ymjc")))

(define-public crate-zdump-1 (crate (name "zdump") (vers "1.3.6") (deps (list (crate-dep (name "libtzfile") (req "^3") (features (quote ("parse"))) (default-features #t) (kind 0)))) (hash "1icvs1s4r860xdqc9vlnmq7msskqklfgyjyia0hhy12y792jkb2s")))

