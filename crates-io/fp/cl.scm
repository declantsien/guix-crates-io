(define-module (crates-io fp cl) #:use-module (crates-io))

(define-public crate-fpcli-0.0.1 (crate (name "fpcli") (vers "0.0.1") (deps (list (crate-dep (name "clap") (req "^3") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "flatpak-rs") (req "^0.8") (default-features #t) (kind 0)))) (hash "18nad7hcmg8mz7q8bj4a38y7vnfgm2qp7fc036yj8cl7wkmlbi0w")))

(define-public crate-fpcli-0.0.2 (crate (name "fpcli") (vers "0.0.2") (deps (list (crate-dep (name "clap") (req "^3") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "flatpak-rs") (req "^0.8") (default-features #t) (kind 0)))) (hash "17anw8rr8qvsdlkg8wwlc5sii73ivzzqxx5hpyvpnjjr9ah0yz9h")))

(define-public crate-fpcli-0.0.3 (crate (name "fpcli") (vers "0.0.3") (deps (list (crate-dep (name "clap") (req "^3") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "flatpak-rs") (req "^0.8") (default-features #t) (kind 0)))) (hash "1paxzas60m84dj5vhx91sc0pkwhb2nnxfqjm4g8gqrh0yyih09z8")))

(define-public crate-fpcli-0.0.4 (crate (name "fpcli") (vers "0.0.4") (deps (list (crate-dep (name "clap") (req "^3") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "flatpak-rs") (req "^0.9") (default-features #t) (kind 0)))) (hash "1kr29by1xijwcncfzakd2zg061plfrz4fq0i9laqx6zrn8vg8l8a")))

(define-public crate-fpcli-0.0.5 (crate (name "fpcli") (vers "0.0.5") (deps (list (crate-dep (name "clap") (req "^3") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "flatpak-rs") (req "^0.9") (default-features #t) (kind 0)))) (hash "01wyy7xxb66bsr1rc5xr68drznjyzb8n3dvqh2k6bzc68ii5gx3v") (rust-version "1.56")))

(define-public crate-fpcli-0.0.6 (crate (name "fpcli") (vers "0.0.6") (deps (list (crate-dep (name "clap") (req "^3") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "flatpak-rs") (req "^0.10") (default-features #t) (kind 0)))) (hash "0ld5jx5arpf2m51qhhsnx8267qdxmipjgk8iha3hap8y679l84lm") (rust-version "1.56")))

(define-public crate-fpcli-0.0.7 (crate (name "fpcli") (vers "0.0.7") (deps (list (crate-dep (name "clap") (req "^3") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "flatpak-rs") (req "^0.12") (default-features #t) (kind 0)))) (hash "19sfr644n4yn5a5gf4cr2m4rh4dxhhbqybwjmjcq7a0514b3hxqr") (rust-version "1.56")))

(define-public crate-fpcli-0.0.8 (crate (name "fpcli") (vers "0.0.8") (deps (list (crate-dep (name "clap") (req "^3") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "flatpak-rs") (req "^0.12") (default-features #t) (kind 0)))) (hash "0aj5p51gaf21vlla7pipf52pclnkigqd4y463f0jdv1cp80x98v1") (rust-version "1.56")))

(define-public crate-fpcli-0.1 (crate (name "fpcli") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^3") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "flatpak-rs") (req "^0.13") (features (quote ("toml"))) (default-features #t) (kind 0)))) (hash "0lx16dj1496b85hxjizbzcp4ak906gns71c7ldkyrmdf18k4na6a") (rust-version "1.56")))

(define-public crate-fpcli-0.2 (crate (name "fpcli") (vers "0.2.0") (deps (list (crate-dep (name "clap") (req "^3") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "flatpak-rs") (req "^0.13") (features (quote ("toml"))) (default-features #t) (kind 0)))) (hash "16p3mzcfgr7wipfagh3rv1246pr5ir4nly0ikzf649n56gpnbamw") (rust-version "1.56")))

(define-public crate-fpcli-0.3 (crate (name "fpcli") (vers "0.3.0") (deps (list (crate-dep (name "clap") (req "^3") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "flatpak-rs") (req "^0.14") (features (quote ("toml"))) (default-features #t) (kind 0)))) (hash "1b0jin0ryzhxiqiv8cnmq1g5g3ny9wv2s8gzf2nfq7f6vnv5r67n") (rust-version "1.56")))

(define-public crate-fpcli-0.3 (crate (name "fpcli") (vers "0.3.1") (deps (list (crate-dep (name "clap") (req "^3") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "flatpak-rs") (req "^0.14") (features (quote ("toml"))) (default-features #t) (kind 0)))) (hash "1ih12mfscm6mxq6vmq6wv7v6i9f15i3lgaxbx2njg9likfajp1gx") (rust-version "1.56")))

(define-public crate-fpcli-0.4 (crate (name "fpcli") (vers "0.4.0") (deps (list (crate-dep (name "clap") (req "^3") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "flatpak-rs") (req "^0.18") (default-features #t) (kind 0)))) (hash "0y3dplrrlhwdmyiiwhmciy8352agwjh5j15q2mpi820wcqsi44qa") (rust-version "1.56")))

(define-public crate-fpcli-0.5 (crate (name "fpcli") (vers "0.5.0") (deps (list (crate-dep (name "clap") (req "^3") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "flatpak-rs") (req "^0.18") (default-features #t) (kind 0)))) (hash "0cbdk83q6iawibr1qq9pgjw3kvnwzbs0jya35yp95lv17r2rrpn7") (rust-version "1.56")))

