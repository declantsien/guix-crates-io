(define-module (crates-io fp -c) #:use-module (crates-io))

(define-public crate-fp-collections-0.0.0 (crate (name "fp-collections") (vers "0.0.0") (hash "1f3amiqhr4q6hcx9l6pww324a1pkz2w1v2krkqbjgdfs7q1ypvl1")))

(define-public crate-fp-collections-0.0.1 (crate (name "fp-collections") (vers "0.0.1") (hash "0q58ai8k4n6imjak0y4a9sfbmj5ysv1hgs7kv71hzs3qfyhd6mb5")))

(define-public crate-fp-collections-0.0.2 (crate (name "fp-collections") (vers "0.0.2") (hash "05llr6qk77v86nfiwsk4bjzwpncdyfdrwm4hm430kks9fzi2kilc")))

(define-public crate-fp-consensus-0.1 (crate (name "fp-consensus") (vers "0.1.0") (hash "1yqa5jibzr86xcsibr5q021divzi2wxlrmlpdacrc5fx6faln85l")))

(define-public crate-fp-consensus-1 (crate (name "fp-consensus") (vers "1.0.0") (deps (list (crate-dep (name "codec") (req "^2.0.0") (features (quote ("derive"))) (kind 0) (package "parity-scale-codec")) (crate-dep (name "ethereum") (req "^0.7.1") (features (quote ("with-codec"))) (kind 0)) (crate-dep (name "rlp") (req "^0.5") (kind 0)) (crate-dep (name "sha3") (req "^0.8") (kind 0)) (crate-dep (name "sp-core") (req "^3.0.0") (kind 0)) (crate-dep (name "sp-runtime") (req "^3.0.0") (kind 0)) (crate-dep (name "sp-std") (req "^3.0.0") (kind 0)))) (hash "12k7bipysiqdyfi1nxv2qb1ajgmvd8ajlbg4jv0pp7fhcf4sk68j") (features (quote (("std" "sp-std/std" "sp-runtime/std" "sp-core/std" "codec/std" "ethereum/std" "rlp/std" "sha3/std") ("default" "std"))))))

(define-public crate-fp-core-0.1 (crate (name "fp-core") (vers "0.1.0") (hash "1x2pqgasyrjg26gkcs2cipm15dalbphrbz09z4g1hn6npp8rqdk5")))

(define-public crate-fp-core-0.1 (crate (name "fp-core") (vers "0.1.1") (hash "11ibfp4hn3kcm9cj76jsx65f1rnvd62hs75i4fr2qic6n9fb77n7")))

(define-public crate-fp-core-0.1 (crate (name "fp-core") (vers "0.1.2") (deps (list (crate-dep (name "itertools") (req "^0.8.0") (default-features #t) (kind 0)))) (hash "0grynkhka3k0pakamwz5iy7xnc24vj9i3yl6cqhmnrj4mb4z5p8s")))

(define-public crate-fp-core-0.1 (crate (name "fp-core") (vers "0.1.3") (deps (list (crate-dep (name "itertools") (req "^0.8.0") (default-features #t) (kind 0)))) (hash "1qwj43nnwnwjal2jsyizyaijgc9b3rrdfrks6yp2ajanmnpxwnc4")))

(define-public crate-fp-core-0.1 (crate (name "fp-core") (vers "0.1.4") (deps (list (crate-dep (name "itertools") (req "^0.8.0") (default-features #t) (kind 0)))) (hash "0n4qj77xxmy9yvb1gqg8lmbrikq098v7ypkzj2184r12lfnks8wi")))

(define-public crate-fp-core-0.1 (crate (name "fp-core") (vers "0.1.5") (deps (list (crate-dep (name "itertools") (req "^0.8.0") (default-features #t) (kind 0)))) (hash "13xrmdmmmd0y6wxb97qgwsjcvdk27w5v00jbcvv86zvc277a6ycv")))

(define-public crate-fp-core-0.1 (crate (name "fp-core") (vers "0.1.6") (deps (list (crate-dep (name "itertools") (req "^0.8.0") (default-features #t) (kind 0)))) (hash "0x44njlkwsrwa7bg3v0mpz4gg4a2fx6kjr8qrpx8wj2bnia95bs7")))

(define-public crate-fp-core-0.1 (crate (name "fp-core") (vers "0.1.7") (deps (list (crate-dep (name "itertools") (req "^0.8.0") (default-features #t) (kind 0)))) (hash "1jdky6mkrh6r2q985442vkrxq7zqwdihgwsg3lvy5jyk0459637i")))

(define-public crate-fp-core-0.1 (crate (name "fp-core") (vers "0.1.8") (deps (list (crate-dep (name "itertools") (req "^0.8.0") (default-features #t) (kind 0)))) (hash "1g1vni876dzb1jh98s719c28n5hxv7r4vjcmxpcf4gaq0ligdm0l")))

(define-public crate-fp-core-0.1 (crate (name "fp-core") (vers "0.1.9") (deps (list (crate-dep (name "itertools") (req "^0.8.0") (default-features #t) (kind 0)))) (hash "02ydzgsw5rspsvhpy59ag5gbdawr9vd5i9rxz8yn0j3jdkmmz2ik")))

