(define-module (crates-io fp oo) #:use-module (crates-io))

(define-public crate-fpool-0.1 (crate (name "fpool") (vers "0.1.0") (deps (list (crate-dep (name "failure") (req "^0.1") (default-features #t) (kind 0)))) (hash "1z8pk10nv8pjq5m5dw4w157fn3axf6i34l3vbxaqphfzrwbks8cb")))

(define-public crate-fpool-0.2 (crate (name "fpool") (vers "0.2.0") (hash "11s23kzvfvp6p5wd4y8v9f4x6iq3i5fhiz4zavhzdva5alicjpnw")))

(define-public crate-fpool-0.3 (crate (name "fpool") (vers "0.3.0") (hash "0xzk1qq4214vlq8dv281ab3vhmsrvlb1ryg5k9l7rxy3657igcn5")))

(define-public crate-fpool-0.3 (crate (name "fpool") (vers "0.3.1") (hash "1r9n2ahaqj40nmclc2bki9gqxy70zagaqp2wgqa4fwf666jhjnrl")))

(define-public crate-fpool-0.4 (crate (name "fpool") (vers "0.4.0") (hash "12wgapdbmfkjd4r39ncw5z3qs4ifwg17vc6q8vl1yxxpl6yxma29")))

(define-public crate-fpool-0.5 (crate (name "fpool") (vers "0.5.0") (hash "1w37zf2jb5qs6assvl8rwb1d2xlqq9r1qrplvqd7f0zzvx41jnl9")))

