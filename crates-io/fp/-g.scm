(define-module (crates-io fp -g) #:use-module (crates-io))

(define-public crate-fp-growth-0.1 (crate (name "fp-growth") (vers "0.1.0") (hash "15zxmvi21vbnjkazkjjddvjk91p3qbhsbcsk0f7rvbi62mwakf6z")))

(define-public crate-fp-growth-0.1 (crate (name "fp-growth") (vers "0.1.1") (hash "1fipvs9b748vr8pqc1yvdf8zpj1kh1k1q7c147c0spvz0wlqqmyd")))

(define-public crate-fp-growth-0.1 (crate (name "fp-growth") (vers "0.1.2") (hash "1644s5bgbfmc7n4ypjjrhgv9zimhc79li91rdwlnj8g6lzwiydyn")))

(define-public crate-fp-growth-0.1 (crate (name "fp-growth") (vers "0.1.3") (hash "19g7dyi8rd64qjfwhvnqbda9ay0l4avh1c24wm9v6h6s1bi8h91a") (yanked #t)))

(define-public crate-fp-growth-0.1 (crate (name "fp-growth") (vers "0.1.4") (hash "1spmw19vqmhpfd8qvyckhfpin7glhn4kc2l36pdncmd7yic3jn6h")))

(define-public crate-fp-growth-0.1 (crate (name "fp-growth") (vers "0.1.6") (hash "1f4yp5c4idzqbkhyaz5wlp7sxvhsn8acxjjsrn6wf50rs7fva470")))

