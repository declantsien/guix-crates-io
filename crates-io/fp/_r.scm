(define-module (crates-io fp _r) #:use-module (crates-io))

(define-public crate-fp_rust-0.1 (crate (name "fp_rust") (vers "0.1.0") (hash "0ckh2apai9k3i53h2sl9p5071vbp6s16wgih1fa6j0yvixwv3nxq")))

(define-public crate-fp_rust-0.1 (crate (name "fp_rust") (vers "0.1.1") (hash "0pp5hwq2zmw20c2pfcsrj40jawlp5xrw6fjx4pvf25lzngqy1mpv")))

(define-public crate-fp_rust-0.1 (crate (name "fp_rust") (vers "0.1.2") (hash "1lslqlwcavbflp0q1xacrnzvv8bfxmr1xp2vwli3dajy40cpaq1b") (features (quote (("default"))))))

(define-public crate-fp_rust-0.1 (crate (name "fp_rust") (vers "0.1.3") (hash "1lpdcbjmdb6y0xc5rx14v4cm2r5fnpsxi1m1bsf53fn43fbna6q5") (features (quote (("default"))))))

(define-public crate-fp_rust-0.1 (crate (name "fp_rust") (vers "0.1.4") (hash "1h9x0yhs585234a343hryvyv5k1dj4zpj0494x8nbcp9v9ql55z8") (features (quote (("default"))))))

(define-public crate-fp_rust-0.1 (crate (name "fp_rust") (vers "0.1.5") (hash "1793lw985g9x1wacax9a6bci8njm54651is7fr3xqkx4iw282cl9") (features (quote (("default"))))))

(define-public crate-fp_rust-0.1 (crate (name "fp_rust") (vers "0.1.6") (hash "0y4asdzjmhdnv3whrxj7aj7ii8xydwk3ikwqpz8174bwvgmz3pxd") (features (quote (("default"))))))

(define-public crate-fp_rust-0.1 (crate (name "fp_rust") (vers "0.1.7") (hash "00wdg1vqy5phn3l28n7p22rk87d3mn4k4brcap07nssfg5kvml2g") (features (quote (("default"))))))

(define-public crate-fp_rust-0.1 (crate (name "fp_rust") (vers "0.1.8") (hash "0i5hlvikwhg6fffa3ix0x6k5x58wybnfiagpj9kg7z77rxzr1fsj") (features (quote (("default"))))))

(define-public crate-fp_rust-0.1 (crate (name "fp_rust") (vers "0.1.9") (hash "1w5f1lcwnvqqspy8w472f8vhnx4qjvjx0vwxvgsfwjpvqjzmdi8d") (features (quote (("default"))))))

(define-public crate-fp_rust-0.1 (crate (name "fp_rust") (vers "0.1.10") (hash "1jdmw2zghmkap3pbi0qvvqgzar4w2lsa2f986cmncsw796d4qfdn") (features (quote (("default"))))))

(define-public crate-fp_rust-0.1 (crate (name "fp_rust") (vers "0.1.11") (hash "1fpvhc9z84la86rp0jvyv47b57a4xbsgh9h19bc47fnh19pp6wx7") (features (quote (("default"))))))

(define-public crate-fp_rust-0.1 (crate (name "fp_rust") (vers "0.1.12") (hash "00qfzxa4zvwxirdfp89cy3ih7valpwh4pqg68vhwaj2wx3m38rrk") (features (quote (("default"))))))

(define-public crate-fp_rust-0.1 (crate (name "fp_rust") (vers "0.1.14") (hash "0km2rpqsbv03am6dx6cbrjvqqkn9a4sl3lqjxcn5r84yc4yali84") (features (quote (("default"))))))

(define-public crate-fp_rust-0.1 (crate (name "fp_rust") (vers "0.1.15") (hash "0s0f66dyl90dij7blwpzs5l2ga7p6mnkjwfc0z3bwv0v0ik5gf36") (features (quote (("default"))))))

(define-public crate-fp_rust-0.1 (crate (name "fp_rust") (vers "0.1.16") (hash "0yfw0vzzprhxasd2cziyli3drvkwy237baxl1v4qqlhjvk70599w") (features (quote (("default"))))))

(define-public crate-fp_rust-0.1 (crate (name "fp_rust") (vers "0.1.17") (hash "0ym29pw3zj4ndsm057y2n3y7nbq7wp0yanaxjqcvr8cbib2qcv2a") (features (quote (("default"))))))

(define-public crate-fp_rust-0.1 (crate (name "fp_rust") (vers "0.1.18") (hash "1ffb7rxmbwqdz8fbmj8k83y6p18nfy4rxxdnvsd4wkwr5aks1d8j") (features (quote (("default"))))))

(define-public crate-fp_rust-0.1 (crate (name "fp_rust") (vers "0.1.19") (hash "053mkasm31sgs0gwymm6hm66y8k1b509nh56r960gfw4fyzlwayw") (features (quote (("default"))))))

(define-public crate-fp_rust-0.1 (crate (name "fp_rust") (vers "0.1.20") (hash "1bkfjixnyrk80h60hsar3ycjmkcb1pa38k63nxcl228x60b81cbv") (features (quote (("default"))))))

(define-public crate-fp_rust-0.1 (crate (name "fp_rust") (vers "0.1.21") (hash "0j75dagrd2zbfrigpgs83vlzrsgsh9v95b94qw5vd70rd3d41l67") (features (quote (("default"))))))

(define-public crate-fp_rust-0.1 (crate (name "fp_rust") (vers "0.1.22") (hash "1zb9mrdv1qa6kzhbdmcmw7l8871y3856qc6h7i54xlqyshk05nra") (features (quote (("default"))))))

(define-public crate-fp_rust-0.1 (crate (name "fp_rust") (vers "0.1.23") (hash "1w5n4idyys2d8fr5cc7n3bgimcaq5y4i30j9sglwlsnzsf0p9js6") (features (quote (("default"))))))

(define-public crate-fp_rust-0.1 (crate (name "fp_rust") (vers "0.1.24") (hash "0j2h1cx0r1gvlnifcsn0qzgn15sr320b8b9zjmwny6dcpzcmdar1") (features (quote (("default"))))))

(define-public crate-fp_rust-0.1 (crate (name "fp_rust") (vers "0.1.25") (hash "08rj3cr480in40ayxwg5v78z7f2c5ssp4q5j1qiqdr0w60z7f2hj") (features (quote (("default"))))))

(define-public crate-fp_rust-0.1 (crate (name "fp_rust") (vers "0.1.26") (hash "119yrkl5pg5chhqrzv68abbk0ygzl0aq4idryz7y2x5q78xmjz0s") (features (quote (("default"))))))

(define-public crate-fp_rust-0.1 (crate (name "fp_rust") (vers "0.1.27") (hash "0ym2xhf1fkg1m8wd9py1csm6q4p0p6wdxarzl6gk0frdq6px1sz5") (features (quote (("default"))))))

(define-public crate-fp_rust-0.1 (crate (name "fp_rust") (vers "0.1.28") (hash "1avdkby734cz7g2hbad34w4j45hcs7lyspp15sf6dwf0fw65h8af") (features (quote (("default"))))))

(define-public crate-fp_rust-0.1 (crate (name "fp_rust") (vers "0.1.29") (hash "0gf0ffjh8yqsl0b98mrdahj55syg8zwch0yw5pybq4rn6f521h69") (features (quote (("default"))))))

(define-public crate-fp_rust-0.1 (crate (name "fp_rust") (vers "0.1.30") (hash "1gzqsxc76p9icdhzawwph115d3bbla14wzd11fnrmwanxj4zg0ng") (features (quote (("default"))))))

(define-public crate-fp_rust-0.1 (crate (name "fp_rust") (vers "0.1.31") (hash "0dm65xk079brl5s9yqh11jwri5dapsnh6wm83qrkq0ca6lq567z7") (features (quote (("default"))))))

(define-public crate-fp_rust-0.1 (crate (name "fp_rust") (vers "0.1.32") (hash "0b8bj0mysys2x2a61qfsszdicylfgj7s20skspvibrq4gjp81ayi") (features (quote (("default"))))))

(define-public crate-fp_rust-0.1 (crate (name "fp_rust") (vers "0.1.33") (hash "0vkc8v7bdb9kh9m9f2ngy0522j4ry1d1cg964x22794pmh4b179d") (features (quote (("default"))))))

(define-public crate-fp_rust-0.1 (crate (name "fp_rust") (vers "0.1.34") (hash "0gjy1k9gqz9i7m9243ihhcdy7m6283kbhb66srpnjh97xkhvdw3x") (features (quote (("default"))))))

(define-public crate-fp_rust-0.1 (crate (name "fp_rust") (vers "0.1.35") (hash "0b3g47s84qkxrcbpqydfkdx5bw9w2jy2chan6j6r5l891hqwqc0g") (features (quote (("default"))))))

(define-public crate-fp_rust-0.1 (crate (name "fp_rust") (vers "0.1.36") (hash "010gqi9lgqmfkn117sy48v9bnzy7xarkwmz6ysg632dsca8f8k42") (features (quote (("default"))))))

(define-public crate-fp_rust-0.1 (crate (name "fp_rust") (vers "0.1.37") (hash "0wrv69cp4bqnj7j6pkvijhahdjanmz4bx0hwl1hgry19ffa308dz") (features (quote (("default"))))))

(define-public crate-fp_rust-0.1 (crate (name "fp_rust") (vers "0.1.38") (hash "15xsvlib1kx89my24cl9zrir9ywkh2lwd82sppq9cpk2w8cc3lwc") (features (quote (("default"))))))

(define-public crate-fp_rust-0.1 (crate (name "fp_rust") (vers "0.1.39") (hash "16yaqfbvjbk0xnaz7j1hsfk86zzh201m873q939csh4lfci992bz") (features (quote (("default"))))))

(define-public crate-fp_rust-0.1 (crate (name "fp_rust") (vers "0.1.40") (hash "1wwsh84vbmnp38g1g4224k4mc1q29a2z21a2v8nc0pyjbfdp3j3x") (features (quote (("default"))))))

(define-public crate-fp_rust-0.2 (crate (name "fp_rust") (vers "0.2.0") (deps (list (crate-dep (name "futures") (req "^0.3") (features (quote ("thread-pool"))) (optional #t) (kind 0)) (crate-dep (name "futures-test") (req "^0.3") (optional #t) (default-features #t) (kind 0)))) (hash "194izhhawmnl2f22l6jdamdi6jgg8z01bp4x0j71dja4k8rcjgjj") (features (quote (("test_runtime" "for_futures") ("pure") ("for_futures" "futures" "futures-test") ("default"))))))

(define-public crate-fp_rust-0.2 (crate (name "fp_rust") (vers "0.2.1") (deps (list (crate-dep (name "futures") (req "^0.3") (features (quote ("thread-pool"))) (optional #t) (kind 0)) (crate-dep (name "futures-test") (req "^0.3") (optional #t) (default-features #t) (kind 0)))) (hash "1aasmgnyvh3ppaqbrxxmfr5p27l38pybknv0s7xi2b9iqq2cfg8r") (features (quote (("test_runtime" "for_futures") ("pure") ("for_futures" "futures" "futures-test") ("default"))))))

(define-public crate-fp_rust-0.2 (crate (name "fp_rust") (vers "0.2.3") (deps (list (crate-dep (name "futures") (req "^0.3") (features (quote ("thread-pool"))) (optional #t) (kind 0)) (crate-dep (name "futures-test") (req "^0.3") (optional #t) (default-features #t) (kind 0)))) (hash "0xg8agirmmmvignw2b6m4igl69k2ffgxpzh7fq2kj7c3wcdphl78") (features (quote (("test_runtime" "for_futures") ("pure") ("for_futures" "futures" "futures-test") ("default"))))))

(define-public crate-fp_rust-0.2 (crate (name "fp_rust") (vers "0.2.2") (deps (list (crate-dep (name "futures") (req "^0.3") (features (quote ("thread-pool"))) (optional #t) (kind 0)) (crate-dep (name "futures-test") (req "^0.3") (optional #t) (default-features #t) (kind 0)))) (hash "1ym5bmzl6xrgmmnhc438jx3j2fyy9qxmfl7hv8xig912l75b3xxs") (features (quote (("test_runtime" "for_futures") ("pure") ("for_futures" "futures" "futures-test") ("default"))))))

(define-public crate-fp_rust-0.2 (crate (name "fp_rust") (vers "0.2.4") (deps (list (crate-dep (name "futures") (req "^0.3") (features (quote ("thread-pool"))) (optional #t) (kind 0)) (crate-dep (name "futures-test") (req "^0.3") (optional #t) (default-features #t) (kind 0)))) (hash "1cklr13rxzqgw39lmqdfxmx0za4skljgiiljvbyq9rwzkzcp1h0z") (features (quote (("test_runtime" "for_futures") ("pure") ("for_futures" "futures" "futures-test") ("default"))))))

(define-public crate-fp_rust-0.2 (crate (name "fp_rust") (vers "0.2.5") (deps (list (crate-dep (name "futures") (req "^0.3") (features (quote ("thread-pool"))) (optional #t) (kind 0)) (crate-dep (name "futures-test") (req "^0.3") (optional #t) (default-features #t) (kind 0)))) (hash "1fsbpm6kyjm64m722xm9lz6kgmrrs1g4hpyrakm9yhg3b59jbzkv") (features (quote (("test_runtime" "for_futures") ("pure") ("for_futures" "futures" "futures-test") ("default"))))))

(define-public crate-fp_rust-0.2 (crate (name "fp_rust") (vers "0.2.6") (deps (list (crate-dep (name "futures") (req "^0.3") (features (quote ("thread-pool"))) (optional #t) (kind 0)) (crate-dep (name "futures-test") (req "^0.3") (optional #t) (default-features #t) (kind 0)))) (hash "06d6f1c94vz9ic0i7f5djz9mgb3dr9gxi53ss5iml0m0a630j8jy") (features (quote (("test_runtime" "pure" "for_futures") ("pure" "fp" "maybe" "monadio" "cor" "actor") ("monadio") ("maybe") ("fp") ("for_futures" "futures" "futures-test") ("default" "pure") ("cor") ("actor"))))))

(define-public crate-fp_rust-0.2 (crate (name "fp_rust") (vers "0.2.7") (deps (list (crate-dep (name "futures") (req "^0.3") (features (quote ("thread-pool"))) (optional #t) (kind 0)) (crate-dep (name "futures-test") (req "^0.3") (optional #t) (default-features #t) (kind 0)))) (hash "007a1075v2q85yis3b4q5gg67sli8qr8nfky1wqapcgdjdsxwd2w") (features (quote (("test_runtime" "pure" "for_futures") ("pure" "fp" "maybe" "monadio" "cor" "actor") ("monadio") ("maybe") ("fp") ("for_futures" "futures" "futures-test") ("default" "pure") ("cor") ("actor"))))))

(define-public crate-fp_rust-0.2 (crate (name "fp_rust") (vers "0.2.8") (deps (list (crate-dep (name "futures") (req "^0.3") (features (quote ("thread-pool"))) (optional #t) (kind 0)) (crate-dep (name "futures-test") (req "^0.3") (optional #t) (default-features #t) (kind 0)))) (hash "05k160k1byjarxgq4dx69llr0zw9rv9w2xswd7m22sxrns1gbxc6") (features (quote (("test_runtime" "pure" "for_futures") ("sync") ("pure" "fp" "maybe" "sync" "cor" "actor" "handler" "monadio" "publisher") ("publisher" "sync" "handler") ("monadio" "sync" "handler") ("maybe") ("handler" "sync") ("fp") ("for_futures" "futures" "futures-test") ("default" "pure") ("cor") ("actor" "sync"))))))

(define-public crate-fp_rust-0.3 (crate (name "fp_rust") (vers "0.3.0") (deps (list (crate-dep (name "futures") (req "^0.3") (features (quote ("thread-pool"))) (optional #t) (kind 0)) (crate-dep (name "futures-test") (req "^0.3") (optional #t) (default-features #t) (kind 0)))) (hash "11zydvkwh5zp78xwz2xzb7f8wv7ggb9kx933da18qw5vsp3dcsy5") (features (quote (("test_runtime" "pure" "for_futures") ("sync") ("pure" "fp" "maybe" "sync" "cor" "actor" "handler" "monadio" "publisher") ("publisher" "sync" "handler") ("monadio" "sync" "handler") ("maybe") ("handler" "sync") ("fp") ("for_futures" "futures" "futures-test") ("default" "pure") ("cor") ("actor" "sync"))))))

(define-public crate-fp_rust-0.3 (crate (name "fp_rust") (vers "0.3.1") (deps (list (crate-dep (name "futures") (req "^0.3") (features (quote ("thread-pool"))) (optional #t) (kind 0)) (crate-dep (name "futures-test") (req "^0.3") (optional #t) (default-features #t) (kind 0)))) (hash "1hdll5rr72xri5lpzbyf6v9zx51z50gc309lkgj1712pfca28y7j") (features (quote (("test_runtime" "pure" "for_futures") ("sync") ("pure" "fp" "maybe" "sync" "cor" "actor" "handler" "monadio" "publisher") ("publisher" "sync" "handler") ("monadio" "sync" "handler") ("maybe") ("handler" "sync") ("fp") ("for_futures" "futures" "futures-test") ("default" "pure") ("cor") ("actor" "sync"))))))

(define-public crate-fp_rust-0.3 (crate (name "fp_rust") (vers "0.3.2") (deps (list (crate-dep (name "futures") (req "^0.3") (features (quote ("thread-pool"))) (optional #t) (kind 0)) (crate-dep (name "futures-test") (req "^0.3") (optional #t) (default-features #t) (kind 0)))) (hash "0h1fhfigkvrx935a54qyx9pfsc4xd6xsrqi32fnyyllb8razw9k5") (features (quote (("test_runtime" "pure" "for_futures") ("sync") ("pure" "fp" "maybe" "sync" "cor" "actor" "handler" "monadio" "publisher") ("publisher" "sync" "handler") ("monadio" "sync" "handler") ("maybe") ("handler" "sync") ("fp") ("for_futures" "futures" "futures-test") ("default" "pure") ("cor") ("actor" "sync"))))))

(define-public crate-fp_rust-0.3 (crate (name "fp_rust") (vers "0.3.3") (deps (list (crate-dep (name "futures") (req "^0.3") (features (quote ("thread-pool"))) (optional #t) (kind 0)) (crate-dep (name "futures-test") (req "^0.3") (optional #t) (default-features #t) (kind 0)))) (hash "1a5dy2i7byrasn56926cknbb05kf46m2zf0aba8wkrrvcyyyqc3x") (features (quote (("test_runtime" "pure" "for_futures") ("sync") ("pure" "fp" "maybe" "sync" "cor" "actor" "handler" "monadio" "publisher") ("publisher" "sync" "handler") ("monadio" "sync" "handler") ("maybe") ("handler" "sync") ("fp") ("for_futures" "futures" "futures-test") ("default" "pure") ("cor") ("actor" "sync"))))))

(define-public crate-fp_rust-0.3 (crate (name "fp_rust") (vers "0.3.4") (deps (list (crate-dep (name "futures") (req "^0.3") (features (quote ("thread-pool"))) (optional #t) (kind 0)) (crate-dep (name "futures-test") (req "^0.3") (optional #t) (default-features #t) (kind 0)))) (hash "08fk055nzd5fgsbfdxp5rgzqaxl63rcnj3a62vh3alsylcxwxcif") (features (quote (("test_runtime" "pure" "for_futures") ("sync") ("pure" "fp" "maybe" "sync" "cor" "actor" "handler" "monadio" "publisher") ("publisher" "sync" "handler") ("monadio" "sync" "handler") ("maybe") ("handler" "sync") ("fp") ("for_futures" "futures" "futures-test") ("default" "pure") ("cor") ("actor" "sync"))))))

(define-public crate-fp_rust-0.3 (crate (name "fp_rust") (vers "0.3.5") (deps (list (crate-dep (name "futures") (req "^0.3") (features (quote ("thread-pool"))) (optional #t) (kind 0)) (crate-dep (name "futures-test") (req "^0.3") (optional #t) (default-features #t) (kind 0)))) (hash "0s1k06q5fxzn4zxb21nafrz598ylgyrpvn6j432abkqll5r92i7n") (features (quote (("test_runtime" "pure" "for_futures") ("sync") ("pure" "fp" "maybe" "sync" "cor" "actor" "handler" "monadio" "publisher") ("publisher" "sync" "handler") ("monadio" "sync" "handler") ("maybe") ("handler" "sync") ("fp") ("for_futures" "futures" "futures-test") ("default" "pure") ("cor") ("actor" "sync"))))))

