(define-module (crates-io fp _l) #:use-module (crates-io))

(define-public crate-fp_lib-0.1 (crate (name "fp_lib") (vers "0.1.0") (hash "0lm3f40q7lw6y8vc9c6g8ni23h0ac4nzlf67y90x8mr39yi0kfww")))

(define-public crate-fp_lib-0.2 (crate (name "fp_lib") (vers "0.2.0") (hash "0ki8g4bx8b8gkgkjf82sjk43qxqcna14iwq9iwsacwq5b6203w7v")))

(define-public crate-fp_lib-0.2 (crate (name "fp_lib") (vers "0.2.1") (hash "0zrz8i8bhxrzalraig3z275f0yrig9j7cdib181da3vg2i94jhrg")))

(define-public crate-fp_lib-0.3 (crate (name "fp_lib") (vers "0.3.0") (hash "051ir3wgwcx5z9h3varhy1wgyzvf2bddvvpnvmg0jqzw91jnvqs1")))

(define-public crate-fp_lib-0.4 (crate (name "fp_lib") (vers "0.4.0") (hash "1fsq9phz3ipnx73yrzag1qj7c56s6g90szkvj34cig167p2rpdrj")))

(define-public crate-fp_lib-0.5 (crate (name "fp_lib") (vers "0.5.0") (hash "0jgfypx0zsqn3aq379gnlls30xr31pismcxpawvvxa9lq6skzprh")))

(define-public crate-fp_lib-0.5 (crate (name "fp_lib") (vers "0.5.1") (hash "08ps9lsmf4lc8352w6p7hdqa0b18q272wz5y05dgmh65mb1341yi")))

(define-public crate-fp_lib-0.5 (crate (name "fp_lib") (vers "0.5.2") (hash "0xqszr3gmljhhdzk7cfzxsdcb9qb9pi7d0xwqydakk41jzqwpczn")))

(define-public crate-fp_lib-0.5 (crate (name "fp_lib") (vers "0.5.3") (hash "1m81agcc8ck16f7d0cnia5zca4q97mnxjgdg3v026qaz6brz8r9h")))

