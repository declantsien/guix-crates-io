(define-module (crates-io fp gc) #:use-module (crates-io))

(define-public crate-fpgcli-0.0.1 (crate (name "fpgcli") (vers "0.0.1") (deps (list (crate-dep (name "arboard") (req "^3.2.1") (default-features #t) (kind 0)) (crate-dep (name "arboard") (req "^3.2.1") (features (quote ("wayland-data-control" "wl-clipboard-rs"))) (default-features #t) (target "cfg(all(unix, not(any(target_os = \"macos\", target_os = \"android\", target_os = \"emscripten\"))))") (kind 0)) (crate-dep (name "flowerypassgen") (req "^0.0.1") (default-features #t) (kind 0)))) (hash "1xfn7a85759psdiias4nhirmz70jnhwgll77033yn4qad83yjqck")))

