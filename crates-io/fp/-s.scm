(define-module (crates-io fp -s) #:use-module (crates-io))

(define-public crate-fp-storage-1 (crate (name "fp-storage") (vers "1.0.0") (deps (list (crate-dep (name "codec") (req "^2.0.0") (kind 0) (package "parity-scale-codec")) (crate-dep (name "ethereum") (req "^0.7.1") (features (quote ("with-codec"))) (kind 0)) (crate-dep (name "ethereum-types") (req "^0.11") (kind 0)) (crate-dep (name "fp-evm") (req "^1.0.0") (kind 0)) (crate-dep (name "sp-api") (req "^3.0.0") (kind 0)) (crate-dep (name "sp-core") (req "^3.0.0") (kind 0)) (crate-dep (name "sp-runtime") (req "^3.0.0") (kind 0)) (crate-dep (name "sp-std") (req "^3.0.0") (kind 0)))) (hash "1kkyfnnhxdhrvnpjf77dpz1y20nlj90pp9zgzjy5ykgljpbf8pyn") (features (quote (("std" "sp-core/std" "sp-api/std" "fp-evm/std" "ethereum/std" "ethereum-types/std" "codec/std" "sp-runtime/std" "sp-std/std") ("default" "std"))))))

(define-public crate-fp-storage-1 (crate (name "fp-storage") (vers "1.0.1") (hash "1s9dibrnknwrch3l2qid2ggmkffdfabjas7mws9b0f3hz557sda1") (features (quote (("std") ("default" "std"))))))

(define-public crate-fp-storage-2 (crate (name "fp-storage") (vers "2.0.0") (hash "0374j7z6mnrdkm2kp8pm1llcrd4b2bkhz5nrp6clam5qpi3p76cv") (features (quote (("std") ("default" "std"))))))

