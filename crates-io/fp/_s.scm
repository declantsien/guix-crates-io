(define-module (crates-io fp _s) #:use-module (crates-io))

(define-public crate-fp_server-0.1 (crate (name "fp_server") (vers "0.1.0") (hash "1wvyf6bbm7j424819cgmqvq6fxd7gp0darwa0pw7nz2yiv00m1b2")))

(define-public crate-fp_server-0.1 (crate (name "fp_server") (vers "0.1.1") (hash "1f49b3g6j1rizkw23fc8dxcmwbkpb3f0qd443l91fx1c2zjzl411")))

(define-public crate-fp_server-0.1 (crate (name "fp_server") (vers "0.1.2") (hash "1kxs5qavdyqsj8sc0vy1lz3fa1c89d6pc797m2vqxgskp5fl9d6y")))

(define-public crate-fp_server-0.1 (crate (name "fp_server") (vers "0.1.3") (hash "1rzyyb49x3qhlw0plv47li3mx9ayn5w64836gyycyhnjpabm1k18")))

(define-public crate-fp_server-1 (crate (name "fp_server") (vers "1.0.0") (hash "0cvccf3mdmmb1j2pm4mnf6qzn2z7gzy45ni8kmqzs107iym5vwv1")))

(define-public crate-fp_server-1 (crate (name "fp_server") (vers "1.1.0") (hash "1708q7kk7hvg84gfwgsw4ss8lh8a8168kdz9qdrawxpiimhqmfyq")))

(define-public crate-fp_server-1 (crate (name "fp_server") (vers "1.1.1") (hash "0q5af5vn2vjp62zlvkbwjna5xirmri0jn3b0gccif63gdpv1xj59")))

(define-public crate-fp_server-1 (crate (name "fp_server") (vers "1.1.2") (hash "07nsqyvv4fvw5080vxqgqdp2x89czxk1hclbwihmpq090lm5a003")))

(define-public crate-fp_server-1 (crate (name "fp_server") (vers "1.2.0") (hash "0qpkwvdhxm1fwilan04w3bh2b352vgjmzkanfn5p9xwbc3vnasvz")))

(define-public crate-fp_server-2 (crate (name "fp_server") (vers "2.0.0") (hash "1jab0wbcadzz516ah2ipw5pa9gscjrlvna84gi3546zg0zfcdwh2")))

(define-public crate-fp_server-2 (crate (name "fp_server") (vers "2.0.1") (hash "1pdkslxck6184nj72g5jd0j8a1qym3x7n7xfz8csmw9w1naqn8b3")))

(define-public crate-fp_server-2 (crate (name "fp_server") (vers "2.0.2") (hash "0a236lnqbdf6dzrsxyylvfcj17psjc01g211nac5mw7y5ws7fyym")))

(define-public crate-fp_server-2 (crate (name "fp_server") (vers "2.0.3") (hash "0hf0qcxc4453q8j485n09l8dn07yjq7xjbs6db03wy0cdv9sas4j")))

(define-public crate-fp_server-2 (crate (name "fp_server") (vers "2.0.4") (hash "0bxdi0d0ykqfrwlficmp89k9qyik2djc2xwk36z09kccv15ki85l")))

(define-public crate-fp_server-2 (crate (name "fp_server") (vers "2.0.5") (hash "0x0g45pxw2kpb529n87mm9h7xmigc6jgmmk57hcq07ymlp4abclg")))

