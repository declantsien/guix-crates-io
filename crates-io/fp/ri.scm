(define-module (crates-io fp ri) #:use-module (crates-io))

(define-public crate-fprint-0.1 (crate (name "fprint") (vers "0.1.0") (hash "00n849kay29y88hpna5wzz507scahnbm10xpair4plk6rbc6b0sr")))

(define-public crate-fprint-0.1 (crate (name "fprint") (vers "0.1.1") (hash "111lhjhx4z6dpzirmlfrhfchgmc1w32lq81i0x1qpgkbk4z4dvkg")))

(define-public crate-fprint-rs-0.1 (crate (name "fprint-rs") (vers "0.1.0") (deps (list (crate-dep (name "failure") (req "^0.1.5") (default-features #t) (kind 0)) (crate-dep (name "fprint-sys") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2.48") (default-features #t) (kind 0)))) (hash "0xcbx179slhn3wjc3nb2accbvgf85381z2pwa07wa3kyq0w4yqrr")))

(define-public crate-fprint-sys-0.1 (crate (name "fprint-sys") (vers "0.1.0") (deps (list (crate-dep (name "bindgen") (req "^0.47.1") (default-features #t) (kind 1)) (crate-dep (name "pkg-config") (req "^0.3.14") (default-features #t) (kind 1)))) (hash "1gd3imfk6w5l39s7dm9sg6dz4hj74ivpswvl60mpr8v2306s8nc4") (links "fprint")))

(define-public crate-fprint-sys-0.1 (crate (name "fprint-sys") (vers "0.1.1") (deps (list (crate-dep (name "bindgen") (req "^0.47.1") (default-features #t) (kind 1)) (crate-dep (name "pkg-config") (req "^0.3.14") (default-features #t) (kind 1)))) (hash "1didx2ani6rsm6vnp9g6jspdzjsqir7g40nl01r1qvjm8dcw1h5g") (links "fprint")))

(define-public crate-fprint-sys-0.1 (crate (name "fprint-sys") (vers "0.1.2") (deps (list (crate-dep (name "bindgen") (req "^0.47") (default-features #t) (kind 1)) (crate-dep (name "pkg-config") (req "^0.3") (default-features #t) (kind 1)))) (hash "127wakcj13p5zzf9fcq1dg1y63iips04ag1gsprmw7gfh1nnhj0r") (links "fprint")))

(define-public crate-fprint-sys-0.1 (crate (name "fprint-sys") (vers "0.1.3") (deps (list (crate-dep (name "bindgen") (req "^0.50") (default-features #t) (kind 1)) (crate-dep (name "pkg-config") (req "^0.3") (default-features #t) (kind 1)))) (hash "0fcdllv84aqa3y7axk4fl7aasyhqsbpfyd1kcys6x3h7y8ijjp6a") (links "fprint")))

