(define-module (crates-io fp s-) #:use-module (crates-io))

(define-public crate-fps-camera-0.1 (crate (name "fps-camera") (vers "0.1.0") (deps (list (crate-dep (name "bitflags") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "piston3d-cam") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "vecmath") (req "^0.3") (default-features #t) (kind 0)))) (hash "0cv8zz71z9l9sd3lnhinnssw5si97ldpzbywqk2gvkw5i6z9pp64")))

(define-public crate-fps-camera-0.1 (crate (name "fps-camera") (vers "0.1.1") (deps (list (crate-dep (name "bitflags") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "piston3d-cam") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "vecmath") (req "^0.3") (default-features #t) (kind 0)))) (hash "1cgzrq4vjrngrxm25gr6lqh6j5fvk2gkbllm089b2wcm9rc19lv4")))

(define-public crate-fps-camera-0.1 (crate (name "fps-camera") (vers "0.1.2") (deps (list (crate-dep (name "bitflags") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "piston3d-cam") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "vecmath") (req "^0.3") (default-features #t) (kind 0)))) (hash "1cz9n5ir1sm10sp9hm6pp8kx7c86845fpyp161cn285042haviv0")))

(define-public crate-fps-controller-0.1 (crate (name "fps-controller") (vers "0.1.0") (deps (list (crate-dep (name "piston_window") (req "^0.86.0") (default-features #t) (kind 2)))) (hash "1rg854csll1lfc794l2dqzqa417aha6gf07wx11x33jgrpi4i19a")))

(define-public crate-fps-controller-0.1 (crate (name "fps-controller") (vers "0.1.1") (deps (list (crate-dep (name "piston_window") (req "^0.89.0") (default-features #t) (kind 2)))) (hash "1j7sr56k168hfizqh4bix83msnyv7095gb2d66ifa63wnkvsac8r")))

