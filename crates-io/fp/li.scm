(define-module (crates-io fp li) #:use-module (crates-io))

(define-public crate-fplist-0.1 (crate (name "fplist") (vers "0.1.0") (hash "16a0db7wx8xwm0j01xv6cvarhiakxb996fn6g2b570z8saskslmr") (features (quote (("multithreaded"))))))

(define-public crate-fplist-0.1 (crate (name "fplist") (vers "0.1.1") (hash "12byy7vqh5m49vnfgs9fzw91fszxbqzhz7dr39qf4hfjapqvq9cm") (features (quote (("multithreaded"))))))

(define-public crate-fplist-0.1 (crate (name "fplist") (vers "0.1.2") (deps (list (crate-dep (name "serde") (req "^1.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 2)))) (hash "0ma6fa4r2mrjsnacrgbarn79z623qll0qs9jd6bsjfr35jll64s6") (features (quote (("serde_impls" "serde") ("multithreaded"))))))

(define-public crate-fplist-0.1 (crate (name "fplist") (vers "0.1.3") (deps (list (crate-dep (name "serde") (req "^1.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 2)))) (hash "0k9dv0a0ikv38lgv783z68zq95bmfc877hzdavnpyhbfbgidif8j") (features (quote (("serde_impls" "serde") ("multithreaded"))))))

(define-public crate-fplist-0.1 (crate (name "fplist") (vers "0.1.4") (deps (list (crate-dep (name "serde") (req "^1.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 2)))) (hash "0i7i8dwkx774bxfnlf6zavrlhwc5sk48x1rdllpapph6la086p86") (features (quote (("serde_impls" "serde") ("multithreaded"))))))

(define-public crate-fplist-0.2 (crate (name "fplist") (vers "0.2.0") (deps (list (crate-dep (name "serde") (req "^1.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 2)))) (hash "13r8rzzaklclyfn2sjbgfylkn2hx68ficcajnps6ma5bi59cvwx4") (features (quote (("serde_impls" "serde") ("multithreaded"))))))

(define-public crate-fplist-0.2 (crate (name "fplist") (vers "0.2.1") (deps (list (crate-dep (name "serde") (req "^1.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 2)))) (hash "13xa6sc3ik87wslnnx65pfwsj6d8g4xbjxxrsmnp39smrqvb9b8w") (features (quote (("serde_impls" "serde") ("multithreaded"))))))

