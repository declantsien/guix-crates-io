(define-module (crates-io fp zi) #:use-module (crates-io))

(define-public crate-fpzip-sys-0.1 (crate (name "fpzip-sys") (vers "0.1.0") (deps (list (crate-dep (name "bindgen") (req "^0.54") (default-features #t) (kind 1)) (crate-dep (name "cmake") (req "^0.1") (default-features #t) (kind 1)))) (hash "12hzbpdq8k0rg0xy4bqpcasm3qh96a7md8agks2j7jw95sw61hdk")))

(define-public crate-fpzip-sys-0.1 (crate (name "fpzip-sys") (vers "0.1.1") (deps (list (crate-dep (name "bindgen") (req "^0.54") (default-features #t) (kind 1)) (crate-dep (name "cmake") (req "^0.1") (default-features #t) (kind 1)))) (hash "09is2cicr8r9jmd1s1zk5sidhqjlarqdb21j4x8ykr8z52jj687z")))

(define-public crate-fpzip-sys-0.1 (crate (name "fpzip-sys") (vers "0.1.2") (deps (list (crate-dep (name "bindgen") (req "^0.54") (default-features #t) (kind 1)) (crate-dep (name "cmake") (req "^0.1") (default-features #t) (kind 1)))) (hash "16rkzpz8bl53xi1fvzz3kz0b58zgcqwrv8p0nj2icr6ib4vy2wny")))

(define-public crate-fpzip-sys-0.1 (crate (name "fpzip-sys") (vers "0.1.3") (deps (list (crate-dep (name "bindgen") (req "^0.54") (default-features #t) (kind 1)) (crate-dep (name "cmake") (req "^0.1") (default-features #t) (kind 1)))) (hash "1014crq6g77vvf06bs9bq82aj1qqh30z3y4vb36b96r26nhz6xf5")))

(define-public crate-fpzip-sys-0.1 (crate (name "fpzip-sys") (vers "0.1.4") (deps (list (crate-dep (name "bindgen") (req "^0.55") (default-features #t) (kind 1)) (crate-dep (name "cmake") (req "^0.1") (default-features #t) (kind 1)))) (hash "1226p6k7ysihm3wi8lypy5q1115dq9xb4ip6bqch3npy8gxqbqzb")))

(define-public crate-fpzip-sys-0.1 (crate (name "fpzip-sys") (vers "0.1.5") (deps (list (crate-dep (name "bindgen") (req "^0.59") (default-features #t) (kind 1)) (crate-dep (name "cmake") (req "^0.1") (default-features #t) (kind 1)))) (hash "0p2d34zcy8zg5sggz30g7l5yvh49liagml3sq9pq8d3aappzdgi4")))

(define-public crate-fpzip-sys-0.1 (crate (name "fpzip-sys") (vers "0.1.6") (deps (list (crate-dep (name "bindgen") (req "^0.60") (default-features #t) (kind 1)) (crate-dep (name "cmake") (req "^0.1") (default-features #t) (kind 1)))) (hash "08vg0zidmbrxsjj6a11glzlpg05ljqvm1z8wgn08by146nr6719f")))

(define-public crate-fpzip-sys-0.1 (crate (name "fpzip-sys") (vers "0.1.7") (deps (list (crate-dep (name "bindgen") (req "^0.65.1") (default-features #t) (kind 1)) (crate-dep (name "cmake") (req "^0.1") (default-features #t) (kind 1)))) (hash "016xh3r7fy9wqv5h1yqszqjf7gcq32i637vm97jpvxznqlk41qn5")))

(define-public crate-fpzip-sys-0.1 (crate (name "fpzip-sys") (vers "0.1.8") (deps (list (crate-dep (name "bindgen") (req "^0.69.1") (default-features #t) (kind 1)) (crate-dep (name "cmake") (req "^0.1") (default-features #t) (kind 1)))) (hash "19f0xqbxxkpz07xz91siapzi65m8hif07vbzvc2dfkiswydhmnsp")))

