(define-module (crates-io fp s_) #:use-module (crates-io))

(define-public crate-fps_clock-1 (crate (name "fps_clock") (vers "1.0.0") (hash "1fn4p2x7vkmkm26xidgknzwrsz5p3wdy0z53h520yrn37s0ww9y9")))

(define-public crate-fps_clock-2 (crate (name "fps_clock") (vers "2.0.0") (hash "1w12byi14f8mhm1rfv8agrnns1za51i3cf47mgv1ziik8dj0fbyn")))

(define-public crate-fps_counter-0.0.0 (crate (name "fps_counter") (vers "0.0.0") (deps (list (crate-dep (name "time") (req "^0.1.1") (default-features #t) (kind 0)))) (hash "10sn00z6z547sjrvk72cwwm9bslxkzbn633ilwgrl9zhmndmbpzv")))

(define-public crate-fps_counter-0.0.1 (crate (name "fps_counter") (vers "0.0.1") (deps (list (crate-dep (name "time") (req "*") (default-features #t) (kind 0)))) (hash "027kz4ih9lj2ljcl60cbgrr9r938mcp5f22f52wkr8jqbm9qank7")))

(define-public crate-fps_counter-0.0.2 (crate (name "fps_counter") (vers "0.0.2") (deps (list (crate-dep (name "clock_ticks") (req "^0.0.5") (default-features #t) (kind 0)))) (hash "1bd4mi71y3w4bzry77psa6rzaih26p6za4g8j9hsfkc0bkg985q1")))

(define-public crate-fps_counter-0.1 (crate (name "fps_counter") (vers "0.1.0") (deps (list (crate-dep (name "clock_ticks") (req "^0.0.6") (default-features #t) (kind 0)))) (hash "1bxgcr6i1b9pk0v10jbrkm6bawr7cmi88nrg8rrklbd4f7xk0jaf")))

(define-public crate-fps_counter-0.1 (crate (name "fps_counter") (vers "0.1.1") (deps (list (crate-dep (name "clock_ticks") (req "^0.0.6") (default-features #t) (kind 0)))) (hash "01y3xzwghh6f0v1s68r35yiwcxa0qvn1qyi2jx0z4ilbhxhpjffz")))

(define-public crate-fps_counter-0.2 (crate (name "fps_counter") (vers "0.2.0") (deps (list (crate-dep (name "time") (req "^0.1.33") (default-features #t) (kind 0)))) (hash "1sijsg2f4bxyhga0gjdil0l3d230lk8dfb22bfyf60gvsh5zcxsn")))

(define-public crate-fps_counter-1 (crate (name "fps_counter") (vers "1.0.0") (hash "19ina9gx8nbla2jw5qdbz09gydj9ikbxa9mg8mw2kwy9n7f82wg8")))

(define-public crate-fps_counter-2 (crate (name "fps_counter") (vers "2.0.0") (hash "0sx3y6mjsxn3r4sb2cacwzc96kp9n607z4k2nl1disafa7zsgars")))

(define-public crate-fps_counter-3 (crate (name "fps_counter") (vers "3.0.0") (deps (list (crate-dep (name "instant") (req "^0.1.12") (default-features #t) (kind 0)))) (hash "1mkdvpd2r1gn6phpkcj4w670va0w7g514g7ff3rmkf541gcs88zz") (features (quote (("wasm-bindgen" "instant/wasm-bindgen") ("stdweb" "instant/stdweb"))))))

(define-public crate-fps_ticker-1 (crate (name "fps_ticker") (vers "1.0.0") (hash "06cj5c5rk5grm2ajh4sabcppxr1h57gxfqacvi5psxb9zw2lj5py")))

