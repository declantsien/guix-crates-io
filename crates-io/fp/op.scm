(define-module (crates-io fp op) #:use-module (crates-io))

(define-public crate-fpopt-0.0.1 (crate (name "fpopt") (vers "0.0.1") (deps (list (crate-dep (name "bindgen") (req "^0.69.4") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2.153") (default-features #t) (kind 0)))) (hash "0lvs7nwfazn09156k6y04q2mn3gvf8zm045mh9lsd9ihj3razcv3")))

(define-public crate-fpopt-0.0.2 (crate (name "fpopt") (vers "0.0.2") (deps (list (crate-dep (name "bindgen") (req "^0.69.4") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2.153") (default-features #t) (kind 0)))) (hash "0mvp9qyv14lbd1x4f5ha0hp7q2c5dfw9mnh8gdmbi3c3k43x82dc")))

(define-public crate-fpopt-0.0.3 (crate (name "fpopt") (vers "0.0.3") (deps (list (crate-dep (name "bindgen") (req "^0.69.4") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2.153") (default-features #t) (kind 0)))) (hash "11j50jayhfbk1k823jnwqh2lw9wsfdz6ihwairxikcng992xxygf")))

