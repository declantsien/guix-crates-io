(define-module (crates-io se q-) #:use-module (crates-io))

(define-public crate-seq-macro-0.1 (crate (name "seq-macro") (vers "0.1.0") (hash "19ca7icy4b3zbcf5nvkln5lyp49mrisx24amjqh0q13kkinnis9h")))

(define-public crate-seq-macro-0.1 (crate (name "seq-macro") (vers "0.1.1") (hash "09f0pqaashmn8q9djlvlfc7vj44xfgj9kyp8rmn01b2w5997nr5p") (features (quote (("docs-rs"))))))

(define-public crate-seq-macro-0.1 (crate (name "seq-macro") (vers "0.1.2") (hash "0xz5rlxl2dx5pq2fg7dq9rkrq1nhksm86166mmxn5if1cz04bijh") (features (quote (("docs-rs"))))))

(define-public crate-seq-macro-0.1 (crate (name "seq-macro") (vers "0.1.3") (hash "0acjla5kzmhrzi5ki49xmz4jfcd660aw556nj5j4kxmvslc6znmc")))

(define-public crate-seq-macro-0.1 (crate (name "seq-macro") (vers "0.1.4") (hash "0hz846c2zs6mh6dkxsqbafg3x3jwy8bmdr4iykx7w2bsp1r1b2zy")))

(define-public crate-seq-macro-0.1 (crate (name "seq-macro") (vers "0.1.5") (hash "036sxc6h92gh2a1mx86dqp03lpr9zlycqj8ggyk50i3q29af2qma")))

(define-public crate-seq-macro-0.2 (crate (name "seq-macro") (vers "0.2.0") (deps (list (crate-dep (name "rustversion") (req "^1.0") (default-features #t) (kind 2)) (crate-dep (name "trybuild") (req "^1.0") (default-features #t) (kind 2)))) (hash "1yzs4qdah0rhcmz8kd80sbymkjcg5hnxxaczvb3yi1cg6m8gb6wk")))

(define-public crate-seq-macro-0.2 (crate (name "seq-macro") (vers "0.2.1") (deps (list (crate-dep (name "rustversion") (req "^1.0") (default-features #t) (kind 2)) (crate-dep (name "trybuild") (req "^1.0") (default-features #t) (kind 2)))) (hash "0nnjm5gcdpqa6ipmqyv6hx0xgvd62jyvy8wi0zbp739jbxkbvcym")))

(define-public crate-seq-macro-0.2 (crate (name "seq-macro") (vers "0.2.2") (deps (list (crate-dep (name "rustversion") (req "^1.0") (default-features #t) (kind 2)) (crate-dep (name "trybuild") (req "^1.0") (default-features #t) (kind 2)))) (hash "168y8k344gssy1q0q6napy8cswgl4hyh2kcim9pk3b9wxbx4g7ss")))

(define-public crate-seq-macro-0.3 (crate (name "seq-macro") (vers "0.3.0") (deps (list (crate-dep (name "rustversion") (req "^1.0") (default-features #t) (kind 2)) (crate-dep (name "trybuild") (req "^1.0.49") (features (quote ("diff"))) (default-features #t) (kind 2)))) (hash "1li007dx6939alzf35ixhayyx4i2hv94cygdby7r5jfv8c989nbh") (rust-version "1.45")))

(define-public crate-seq-macro-0.3 (crate (name "seq-macro") (vers "0.3.1") (deps (list (crate-dep (name "rustversion") (req "^1.0") (default-features #t) (kind 2)) (crate-dep (name "trybuild") (req "^1.0.49") (features (quote ("diff"))) (default-features #t) (kind 2)))) (hash "1628inf65zd9js6waflszafhj718d5f593jghgv9238s1v1wawh7") (rust-version "1.45")))

(define-public crate-seq-macro-0.3 (crate (name "seq-macro") (vers "0.3.2") (deps (list (crate-dep (name "rustversion") (req "^1.0") (default-features #t) (kind 2)) (crate-dep (name "trybuild") (req "^1.0.49") (features (quote ("diff"))) (default-features #t) (kind 2)))) (hash "1kn5spxw9s2l1q29jdng4qpyz7aiv03vkgdx4f21jwmjv7gdx18n") (rust-version "1.45")))

(define-public crate-seq-macro-0.3 (crate (name "seq-macro") (vers "0.3.3") (deps (list (crate-dep (name "rustversion") (req "^1.0") (default-features #t) (kind 2)) (crate-dep (name "trybuild") (req "^1.0.49") (features (quote ("diff"))) (default-features #t) (kind 2)))) (hash "1g6h3aki8xdw17zfib6c0m38plc3v96rac6j6riyc51sr67lxd76") (rust-version "1.45")))

(define-public crate-seq-macro-0.3 (crate (name "seq-macro") (vers "0.3.4") (deps (list (crate-dep (name "rustversion") (req "^1.0") (default-features #t) (kind 2)) (crate-dep (name "trybuild") (req "^1.0.49") (features (quote ("diff"))) (default-features #t) (kind 2)))) (hash "00yg1j96gwp4nm1kshl9rb62abyl7w81n4rc9pz32cav2wwlj4v3") (rust-version "1.45")))

(define-public crate-seq-macro-0.3 (crate (name "seq-macro") (vers "0.3.5") (deps (list (crate-dep (name "rustversion") (req "^1.0") (default-features #t) (kind 2)) (crate-dep (name "trybuild") (req "^1.0.49") (features (quote ("diff"))) (default-features #t) (kind 2)))) (hash "1d50kbaslrrd0374ivx15jg57f03y5xzil1wd2ajlvajzlkbzw53") (rust-version "1.45")))

(define-public crate-seq-scanrs-0.1 (crate (name "seq-scanrs") (vers "0.1.0") (hash "0bai2ljzwjkvq5zp8w59if9833knwil2dd85yipv96qzj9m5hnld")))

(define-public crate-seq-timer-0.1 (crate (name "seq-timer") (vers "0.1.0") (deps (list (crate-dep (name "rusty-hook") (req "^0.11") (default-features #t) (kind 2)))) (hash "0p9m36598f0f0ix0nplcmirg9ha9akdvgp42vzikkbvwvpscnkij")))

