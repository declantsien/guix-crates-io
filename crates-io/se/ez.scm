(define-module (crates-io se ez) #:use-module (crates-io))

(define-public crate-seezee-0.1 (crate (name "seezee") (vers "0.1.0") (deps (list (crate-dep (name "proptest") (req "^1.4.0") (default-features #t) (kind 2)) (crate-dep (name "watto") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "zstd") (req "^0.13.0") (features (quote ("experimental"))) (default-features #t) (kind 0)))) (hash "1psmpfygfjg2wj9hcqr595kxd0mdxcvnnni18wfhcbyr8gjkpk69")))

