(define-module (crates-io se te) #:use-module (crates-io))

(define-public crate-setec-0.0.0 (crate (name "setec") (vers "0.0.0") (hash "1w056iawi1lxiqam33kf68cr7l89ra410x0k2w5ml2byvc42cjhh") (yanked #t)))

(define-public crate-setec-astronomy-0.0.0 (crate (name "setec-astronomy") (vers "0.0.0") (deps (list (crate-dep (name "abscissa") (req "^0") (default-features #t) (kind 0)))) (hash "1qbk255k38kpwyfvq27b9n3wh46d8k9zr1sjzhw7h34p20ykch64") (yanked #t)))

(define-public crate-setenv-0.1 (crate (name "setenv") (vers "0.1.0") (deps (list (crate-dep (name "tempdir") (req "^0.3") (default-features #t) (kind 0)))) (hash "0gm1pj4xd1adhafvmxvlf0d466g2djvqm8mz0h9n8xlnk88s14zx")))

(define-public crate-setenv-0.1 (crate (name "setenv") (vers "0.1.1") (deps (list (crate-dep (name "tempdir") (req "^0.3") (default-features #t) (kind 2)))) (hash "0b6r45xy17d9ghfsa9kpwsz8jfcflg4myxk9zl9a7fiqwcz1l01c")))

(define-public crate-setenv-0.1 (crate (name "setenv") (vers "0.1.2") (deps (list (crate-dep (name "tempdir") (req "^0.3") (default-features #t) (kind 2)))) (hash "17mj7ijq09ig4slj6xqsii31v9sr7yq71mbk5lgpn3i6fnwq3vym")))

