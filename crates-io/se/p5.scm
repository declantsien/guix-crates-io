(define-module (crates-io se p5) #:use-module (crates-io))

(define-public crate-sep5-0.0.1 (crate (name "sep5") (vers "0.0.1") (deps (list (crate-dep (name "slip10") (req "^0.4.3") (default-features #t) (kind 0)) (crate-dep (name "stellar-strkey") (req "^0.0.6") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.31") (default-features #t) (kind 0)) (crate-dep (name "tiny-bip39") (req "^1.0.0") (default-features #t) (kind 0)))) (hash "1xlh7lgkdm2rnwmvgyq8i9nc8q3vwd15q96y3jr3qjvbwldm8sgc") (rust-version "1.66")))

(define-public crate-sep5-0.0.2 (crate (name "sep5") (vers "0.0.2") (deps (list (crate-dep (name "slip10") (req "^0.4.3") (default-features #t) (kind 0)) (crate-dep (name "stellar-strkey") (req "^0.0.7") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.31") (default-features #t) (kind 0)) (crate-dep (name "tiny-bip39") (req "^1.0.0") (default-features #t) (kind 0)))) (hash "0sik9y412xixf4dzxgn3hkjg3lykgc1wpk675w5zmdhzpp639zls") (rust-version "1.67")))

(define-public crate-sep5-0.0.3 (crate (name "sep5") (vers "0.0.3") (deps (list (crate-dep (name "slip10") (req "^0.4.3") (default-features #t) (kind 0)) (crate-dep (name "stellar-strkey") (req "^0.0.8") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.31") (default-features #t) (kind 0)) (crate-dep (name "tiny-bip39") (req "^1.0.0") (default-features #t) (kind 0)))) (hash "14r5gg0q7z4ax0h9n358zcg2ahr754c09ax7zs6xnpqqb714v9hb") (rust-version "1.67")))

