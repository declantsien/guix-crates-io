(define-module (crates-io se nm) #:use-module (crates-io))

(define-public crate-senml-deser-0.2 (crate (name "senml-deser") (vers "0.2.1") (deps (list (crate-dep (name "heapless") (req "^0.7") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (features (quote ("small_rng"))) (default-features #t) (kind 2)) (crate-dep (name "rand_chacha") (req "^0.3.1") (default-features #t) (kind 2)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (kind 0)) (crate-dep (name "serde_cbor") (req "^0.11.2") (kind 0)))) (hash "1i2yv3pgablb37yhwqdrd1fjj05fr45ir78pfh6ak637223x5sph")))

