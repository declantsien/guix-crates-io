(define-module (crates-io se tw) #:use-module (crates-io))

(define-public crate-setwall-1 (crate (name "setwall") (vers "1.0.1") (deps (list (crate-dep (name "gumdrop") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.7") (default-features #t) (kind 0)))) (hash "0q9l5x028g6303yfqhh24dax9927ap2l2gwnd7n34ls8ncp81pvn")))

(define-public crate-setwall-1 (crate (name "setwall") (vers "1.0.2") (deps (list (crate-dep (name "anyhow") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "gumdrop") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.7") (default-features #t) (kind 0)))) (hash "1lrc40vp7gp4pyykk5pd2rsming2m5zg5qc6lggmaakywik5jq6m")))

(define-public crate-setwall-1 (crate (name "setwall") (vers "1.0.3") (deps (list (crate-dep (name "anyhow") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "gumdrop") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.7") (default-features #t) (kind 0)))) (hash "03zalcdjyj159hg783yj06z7frlap0w79xp2v932vdl3jgpvm601")))

(define-public crate-setwall-1 (crate (name "setwall") (vers "1.1.0") (deps (list (crate-dep (name "anyhow") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "gumdrop") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8") (default-features #t) (kind 0)))) (hash "0yp4z85xw4pbkl6m0h5bad09x6j50z5i1as33b71r3s9vm8lm8wl")))

(define-public crate-setwall-1 (crate (name "setwall") (vers "1.2.0") (deps (list (crate-dep (name "anyhow") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "gumdrop") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8") (default-features #t) (kind 0)))) (hash "0la28hygyd4a3aypprjsvawyqmg53s31n9q10k6jay0p8f6y55p6")))

