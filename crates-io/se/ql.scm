(define-module (crates-io se ql) #:use-module (crates-io))

(define-public crate-seqlock-0.1 (crate (name "seqlock") (vers "0.1.0") (deps (list (crate-dep (name "parking_lot") (req "^0.2") (default-features #t) (kind 0)))) (hash "1342s40r7q3mg0mhwl0r05c3arl9qrbc24x48rzxiziy43lln6br") (features (quote (("nightly" "parking_lot/nightly"))))))

(define-public crate-seqlock-0.1 (crate (name "seqlock") (vers "0.1.1") (deps (list (crate-dep (name "parking_lot") (req "^0.2") (default-features #t) (kind 0)))) (hash "1ghzy27fnv7s2nm9ppq1hk9191w91nabiyphx50nmhc31ml1hf3i") (features (quote (("nightly" "parking_lot/nightly"))))))

(define-public crate-seqlock-0.1 (crate (name "seqlock") (vers "0.1.2") (deps (list (crate-dep (name "parking_lot") (req "^0.11") (default-features #t) (kind 0)))) (hash "18mgi2sk7ql7ycrhwkn7jmp92ml48iic6nw7vfj9gfmdxidr6kk6") (features (quote (("nightly" "parking_lot/nightly"))))))

(define-public crate-seqlock-0.2 (crate (name "seqlock") (vers "0.2.0") (deps (list (crate-dep (name "parking_lot") (req "^0.12") (default-features #t) (kind 0)))) (hash "040r6wl53vqax3d3r2jsaiw2cdcja1mxfqzscrnbiigc2ipppimm")))

