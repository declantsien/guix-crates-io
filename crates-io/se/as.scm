(define-module (crates-io se as) #:use-module (crates-io))

(define-public crate-seaslug-0.0.0 (crate (name "seaslug") (vers "0.0.0") (deps (list (crate-dep (name "lalrpop") (req "^0.19.5") (default-features #t) (kind 1)) (crate-dep (name "lalrpop-util") (req "^0.19.5") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2.88") (default-features #t) (kind 0)) (crate-dep (name "num_cpus") (req "^1.13.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.4.4") (default-features #t) (kind 0)))) (hash "1pgijj2819j4nrz0vm08k12lz8niwd3g0cw9r5w5sh0bx854xq2i")))

(define-public crate-season-0.0.0 (crate (name "season") (vers "0.0.0") (hash "0i8z66nnmnzcy7l83cc1wmw50qxpdkvjrv9b5swfwmjpf06vqhzh")))

(define-public crate-seastar-0.1 (crate (name "seastar") (vers "0.1.0") (deps (list (crate-dep (name "criterion") (req "^0.4") (features (quote ("html_reports"))) (default-features #t) (kind 2)) (crate-dep (name "iridescent") (req "^0.2") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.8") (default-features #t) (kind 2)))) (hash "083fyjx33ff1nfw6n250jiwayhrnx8a69vzgn8if7aypgdnn3b52")))

(define-public crate-seastar-0.2 (crate (name "seastar") (vers "0.2.0") (deps (list (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "criterion") (req "^0.4") (features (quote ("html_reports"))) (default-features #t) (kind 2)) (crate-dep (name "iridescent") (req "^0.2") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.8") (default-features #t) (kind 2)))) (hash "0pzgcl9q1sfij36gvcjbbbgxjjdzxakkrmxadwbckkragmg7xdp7") (features (quote (("default")))) (v 2) (features2 (quote (("serde" "dep:serde"))))))

