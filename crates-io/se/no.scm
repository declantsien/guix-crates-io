(define-module (crates-io se no) #:use-module (crates-io))

(define-public crate-senor_borroso-0.1 (crate (name "senor_borroso") (vers "0.1.0") (deps (list (crate-dep (name "fxhash") (req "^0.2.1") (default-features #t) (kind 0)) (crate-dep (name "hnsw") (req "^0.8.0") (default-features #t) (kind 0)) (crate-dep (name "levenshtein") (req "^1.0.5") (default-features #t) (kind 0)) (crate-dep (name "rand_pcg") (req "^0.3.1") (default-features #t) (kind 0)) (crate-dep (name "space") (req "^0.12.0") (default-features #t) (kind 0)) (crate-dep (name "unidecode") (req "^0.3.0") (default-features #t) (kind 0)))) (hash "0wf4isvs1nlzzpczlv2qkw548xqxb9vqj5p38d86vi8gbgf90ycv")))

