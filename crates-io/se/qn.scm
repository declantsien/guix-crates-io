(define-module (crates-io se qn) #:use-module (crates-io))

(define-public crate-seqname-0.1 (crate (name "seqname") (vers "0.1.0") (deps (list (crate-dep (name "structopt") (req "^0.3") (default-features #t) (kind 0)))) (hash "1l49bvz293k7j3gkifjfv5l33r8v615h1znbnzcl3j6qqqwcxs97")))

(define-public crate-seqname-0.2 (crate (name "seqname") (vers "0.2.1") (deps (list (crate-dep (name "structopt") (req "^0.3") (default-features #t) (kind 0)))) (hash "197bscj3s4mcqm3ln7l8xamm7ncln3w5x6h25a6ns837binsnmdg")))

(define-public crate-seqnmf-linalg-0.1 (crate (name "seqnmf-linalg") (vers "0.1.0") (deps (list (crate-dep (name "ndarray") (req "^0.13.1") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2.12") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.7.3") (default-features #t) (kind 0)))) (hash "0c1h5i5rpxsn74ch70n1ngw5qzzmk40c7hamhxnj98y9whyg92d0")))

(define-public crate-seqnmf-linalg-0.1 (crate (name "seqnmf-linalg") (vers "0.1.1") (deps (list (crate-dep (name "ndarray") (req "^0.13.1") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2.12") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.7.3") (default-features #t) (kind 0)))) (hash "00gwq73x2qscdrwsxyd2v4irskzhdq5v6fpvrsysk4gz8d6zdh2z")))

(define-public crate-seqnmf-linalg-0.1 (crate (name "seqnmf-linalg") (vers "0.1.2") (deps (list (crate-dep (name "ndarray") (req "^0.13.1") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2.12") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.7.3") (default-features #t) (kind 0)))) (hash "1ql17dhmsg1flw9kqbk4q9a2xg2pbs7a85cxpwlwp11yi5l7b8ik")))

(define-public crate-seqnmf-linalg-0.1 (crate (name "seqnmf-linalg") (vers "0.1.3") (deps (list (crate-dep (name "ndarray") (req "^0.13.1") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2.12") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.7.3") (default-features #t) (kind 0)))) (hash "1wi8v101h71g1lj6sxpix783z37lydhbil2hacln33apvddkmr9x")))

(define-public crate-seqnmf-linalg-0.1 (crate (name "seqnmf-linalg") (vers "0.1.4") (deps (list (crate-dep (name "ndarray") (req "^0.13.1") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2.12") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.7.3") (default-features #t) (kind 0)))) (hash "0psw779y8rybnw3i3xmwg4z3qaj59kw4srxwds6mh9zq5naa7inq")))

