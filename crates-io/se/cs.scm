(define-module (crates-io se cs) #:use-module (crates-io))

(define-public crate-secs-0.0.1 (crate (name "secs") (vers "0.0.1") (hash "1vjnsp8ixv9izdfknpn04rlm75dr6vk0lqj9j26wal9jjmg94jbl")))

(define-public crate-secstr-0.1 (crate (name "secstr") (vers "0.1.0") (deps (list (crate-dep (name "libc") (req "*") (default-features #t) (kind 0)))) (hash "0xg60hvw6b6qv5734w12ffjz7nm9qikdlxljffs91l49mgml2l85")))

(define-public crate-secstr-0.2 (crate (name "secstr") (vers "0.2.0") (deps (list (crate-dep (name "cbor") (req "^0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "rustc-serialize") (req "^0") (optional #t) (default-features #t) (kind 0)))) (hash "0rc4dqy0gszabl9avnflp08gc6xccj2s2dldvidn88nlql6hn1ah") (features (quote (("cbor-serialize" "rustc-serialize" "cbor"))))))

(define-public crate-secstr-0.2 (crate (name "secstr") (vers "0.2.1") (deps (list (crate-dep (name "cbor") (req "^0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "libsodium-sys") (req "^0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "rustc-serialize") (req "^0") (optional #t) (default-features #t) (kind 0)))) (hash "1g2g4y79bmnla6958f2jp78n5np5a51cw1qavjws7aywqx2x6q59") (features (quote (("cbor-serialize" "rustc-serialize" "cbor"))))))

(define-public crate-secstr-0.3 (crate (name "secstr") (vers "0.3.0") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "libsodium-sys") (req "^0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde_cbor") (req "^0.6.0") (default-features #t) (kind 2)))) (hash "1521qspwj0k7ir8cvg4sx1rqwfi94whwk7n83203q9s826fgyknz")))

(define-public crate-secstr-0.3 (crate (name "secstr") (vers "0.3.1") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "libsodium-sys") (req "^0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde_cbor") (req "^0.6.0") (default-features #t) (kind 2)))) (hash "0n1izkvrq5bl4pa4ycymp9by67cbdw3sagjzqfsrvasj1q3p8ir1")))

(define-public crate-secstr-0.3 (crate (name "secstr") (vers "0.3.2") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "libsodium-sys") (req "^0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde_cbor") (req "^0.9") (default-features #t) (kind 2)))) (hash "12113228mjgn68y96519nmpgmispa0ah2sr22rssji7jrr9ks07v")))

(define-public crate-secstr-0.4 (crate (name "secstr") (vers "0.4.0") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "libsodium-sys") (req "^0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde_cbor") (req "^0.11") (default-features #t) (kind 2)))) (hash "1z6s8fhipn213l0gsjjijjp95hn2nchrmp8yixdhwchwfhkcgqnc")))

(define-public crate-secstr-0.5 (crate (name "secstr") (vers "0.5.0") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "libsodium-sys") (req "^0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "pre") (req "=0.2") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "pre") (req "=0.2") (default-features #t) (kind 2)) (crate-dep (name "serde") (req "^1.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde_cbor") (req "^0.11") (default-features #t) (kind 2)))) (hash "01416mhw1lcyrjhlvgw3myxn3zw8n3c0gxgvfqkmxp77i4fqryj9")))

(define-public crate-secstr-0.5 (crate (name "secstr") (vers "0.5.1") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "libsodium-sys") (req "^0.2") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "pre") (req "=0.2") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "pre") (req "=0.2") (default-features #t) (kind 2)) (crate-dep (name "serde") (req "^1.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde_cbor") (req "^0.11") (default-features #t) (kind 2)))) (hash "04iy25y5qb8lzymsx1iz6250q1dxx29mkppn737w81gn8ir6akz0")))

(define-public crate-secsy-0.0.1 (crate (name "secsy") (vers "0.0.1") (deps (list (crate-dep (name "secsy_ecs") (req "^0") (default-features #t) (kind 0)))) (hash "0cip2zk02vpm6zvppp96g8niwav03jvgqlc3b8q9igfh7dlzdgn8") (yanked #t)))

(define-public crate-secsy-0.1 (crate (name "secsy") (vers "0.1.0") (deps (list (crate-dep (name "secsy_world") (req "^0") (default-features #t) (kind 0)))) (hash "1pcxidz9g38gqvdkyjdns379isy36kvxh9nkj09f4zryngl94hyj") (yanked #t)))

(define-public crate-secsy-0.1 (crate (name "secsy") (vers "0.1.1") (deps (list (crate-dep (name "secsy_world") (req "^0") (default-features #t) (kind 0)))) (hash "1hga1fm22x97cpjn7pxnpygjlp6z0gsnzv6lshfjkbpx5p4rn2ba") (yanked #t)))

(define-public crate-secsy_ecs-0.0.1 (crate (name "secsy_ecs") (vers "0.0.1") (hash "1vkj9bxl5avafsi61ip9z4i16q6dr440lf154g5agq9nc9m79saj")))

(define-public crate-secsy_ecs-0.0.2 (crate (name "secsy_ecs") (vers "0.0.2") (hash "17bxwi1v9xg6ry43a93fa0slgf42yf3dgifvv653injbbdivkq3r")))

(define-public crate-secsy_world-0.0.1 (crate (name "secsy_world") (vers "0.0.1") (hash "0czl6bpzrl7v4wm6sqgnlnpkz3zwrzjagvnpkpdqs5m8xgid8pjs") (yanked #t)))

