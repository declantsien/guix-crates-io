(define-module (crates-io se _d) #:use-module (crates-io))

(define-public crate-se_dump-0.1 (crate (name "se_dump") (vers "0.1.0") (deps (list (crate-dep (name "chrono") (req "^0.4.23") (features (quote ("serde"))) (default-features #t) (kind 0)) (crate-dep (name "quick-xml") (req "^0.27.0") (features (quote ("serialize"))) (default-features #t) (kind 2)) (crate-dep (name "serde") (req "^1.0.152") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_repr") (req "^0.1.10") (default-features #t) (kind 0)))) (hash "01zl8nj7phwv5vi2pmfxl93hwsl1cs19cdjqhxz5w5r9r5kz1sil")))

