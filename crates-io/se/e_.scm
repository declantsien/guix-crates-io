(define-module (crates-io se e_) #:use-module (crates-io))

(define-public crate-see_derive-0.0.1 (crate (name "see_derive") (vers "0.0.1") (deps (list (crate-dep (name "proc-macro2") (req "^1.0.49") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.23") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.107") (default-features #t) (kind 0)))) (hash "03kn00lyq6nl6pzyy5rs9x5hmwmbbw4y482afmcsfszr930lqx51")))

(define-public crate-see_derive-0.0.2 (crate (name "see_derive") (vers "0.0.2") (deps (list (crate-dep (name "proc-macro2") (req "^1.0.49") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.23") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.107") (default-features #t) (kind 0)))) (hash "0hkgqba3vp3mxn0586yr4dbs21bzl9axa939c71n0ig50bi8airq")))

(define-public crate-see_derive-0.0.3 (crate (name "see_derive") (vers "0.0.3") (deps (list (crate-dep (name "proc-macro2") (req "^1.0.49") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.23") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.107") (default-features #t) (kind 0)))) (hash "0js2kf2aghpahs5pcpaj769rhpls2y6hw4ll5jvdr6ih535w77mr")))

(define-public crate-see_derive-0.0.4 (crate (name "see_derive") (vers "0.0.4") (deps (list (crate-dep (name "proc-macro2") (req "^1.0.49") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.23") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.107") (default-features #t) (kind 0)))) (hash "177cvqfnr12khlnivy2r0d99z42jlc8hgii74dn15pg9sjwlxz2p")))

(define-public crate-see_derive-0.0.5 (crate (name "see_derive") (vers "0.0.5") (deps (list (crate-dep (name "proc-macro2") (req "^1.0.49") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.23") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.107") (default-features #t) (kind 0)))) (hash "1d20qn85cd69jdvba8ja3z7jblr4ng3idx0syd75b99nmxm40wgb")))

(define-public crate-see_directory-0.1 (crate (name "see_directory") (vers "0.1.0") (hash "16yv19napbmvb8m0r9i7splq8zalrd9vr5lakwg7cl80dmjbdvqc")))

(define-public crate-see_directory-0.2 (crate (name "see_directory") (vers "0.2.1") (hash "1b3iz4hhsc6kkvlnmjlqjkm82hd2h9cd7kmq5vbid3dixv7ka886")))

(define-public crate-see_directory-0.3 (crate (name "see_directory") (vers "0.3.1") (hash "0dj505myrj8zncfp61v74cn8jqzf8x1n9lkxfwcr8z91c52ch7da")))

