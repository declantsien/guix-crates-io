(define-module (crates-io se we) #:use-module (crates-io))

(define-public crate-sewer-0.1 (crate (name "sewer") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^3.1.15") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "peg") (req "^0.8.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.5.5") (default-features #t) (kind 0)) (crate-dep (name "sewer-replacement") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "tempfile") (req "^3.3.0") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.31") (default-features #t) (kind 0)))) (hash "1hr8isn0wmmryisqbwm48i0daridalww4d9znmghalyr1r4z19qf")))

(define-public crate-sewer-replacement-0.1 (crate (name "sewer-replacement") (vers "0.1.0") (deps (list (crate-dep (name "peg") (req "^0.8.0") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.31") (default-features #t) (kind 0)))) (hash "0v1m2w4bf5lf7p0pk99jif203z9yfwqq4jarvl0w8xkxxhya62ha")))

