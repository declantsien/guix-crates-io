(define-module (crates-io se xp) #:use-module (crates-io))

(define-public crate-sexp-0.1 (crate (name "sexp") (vers "0.1.0") (hash "16vhv41f2cr2yv4j63czld5781zy81ffv4z6jn769y2xv0v7q3cc")))

(define-public crate-sexp-0.1 (crate (name "sexp") (vers "0.1.1") (hash "1ms9h4gb61jvhn1azk1hizk0aw7lsrjw3xbszpwsl2jqzj56w178")))

(define-public crate-sexp-1 (crate (name "sexp") (vers "1.0.0") (hash "1k05dpadqzqm0b4mpbn83lxqykawvsbh1lqwn09a20ynwydqf1kv")))

(define-public crate-sexp-1 (crate (name "sexp") (vers "1.0.1") (hash "10p581c5hjb4iqxdi7qr9wznnds28c335i3gpgh0dpvqm1p93r3l")))

(define-public crate-sexp-1 (crate (name "sexp") (vers "1.1.0") (hash "0l3l4fsn8ahav2lvfg52j3xzsan819isd417g662kvw5k5fnf911")))

(define-public crate-sexp-1 (crate (name "sexp") (vers "1.1.1") (hash "1c1k2i2yk9gbfvnvwhp8wxgbyiszrxqm0ns4f4jl6cfxfqkr13db")))

(define-public crate-sexp-1 (crate (name "sexp") (vers "1.1.2") (hash "16150m91wgsgagbwpyxclnxjnnn60gina3acxz27y16wawild3lq")))

(define-public crate-sexp-1 (crate (name "sexp") (vers "1.1.3") (hash "1wwj86gma5lzm2c35n3nnakkk307gvlw4wny1z0abhq6ag01wn4p")))

(define-public crate-sexp-1 (crate (name "sexp") (vers "1.1.4") (hash "07spb15kp06jg7qqqz9azz1blmih5p5rgx4c4fq00h7qknnag3ww")))

(define-public crate-sexpfmt-0.1 (crate (name "sexpfmt") (vers "0.1.0") (deps (list (crate-dep (name "nom") (req "^7.1.1") (default-features #t) (kind 0)) (crate-dep (name "nom_locate") (req "^4.1.0") (default-features #t) (kind 0)))) (hash "0qvmdjwi3mg2i3v39yan24l9g360kaj7wzls9cqvjqj5g0wcp4j7")))

(define-public crate-sexpr-0.1 (crate (name "sexpr") (vers "0.1.0") (deps (list (crate-dep (name "gc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "gc_derive") (req "^0.2") (default-features #t) (kind 0)))) (hash "1nafb8za6v86i82kivcylwbq7kswzv1l1z249y503rjni6frhcms")))

(define-public crate-sexpr_ir-0.1 (crate (name "sexpr_ir") (vers "0.1.0") (deps (list (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "pest") (req "^2.1.3") (default-features #t) (kind 0)) (crate-dep (name "pest_derive") (req "^2.1.0") (default-features #t) (kind 0)))) (hash "057jd46kcsq5icilinydkmjjrm68hbnldqfz171ih791inbsrd96")))

(define-public crate-sexpr_ir-0.2 (crate (name "sexpr_ir") (vers "0.2.0") (deps (list (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "pest") (req "^2.1.3") (default-features #t) (kind 0)) (crate-dep (name "pest_derive") (req "^2.1.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "1.0.*") (features (quote ("rc" "derive"))) (default-features #t) (kind 0)))) (hash "0s5xwjaf8bpkmwkyg8pg97rj4i3m1950s5nixais1q61125pn64v")))

(define-public crate-sexpr_ir-0.2 (crate (name "sexpr_ir") (vers "0.2.1") (deps (list (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "pest") (req "^2.1.3") (default-features #t) (kind 0)) (crate-dep (name "pest_derive") (req "^2.1.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "1.0.*") (features (quote ("rc" "derive"))) (default-features #t) (kind 0)))) (hash "04ds3kxpg3iaqqhy4bpxbzwn4bjk6iicniqxai1ckw1zj4zd6ip7")))

(define-public crate-sexpr_ir-0.3 (crate (name "sexpr_ir") (vers "0.3.1") (deps (list (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "pest") (req "^2.1.3") (default-features #t) (kind 0)) (crate-dep (name "pest_derive") (req "^2.1.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "1.0.*") (features (quote ("rc" "derive"))) (default-features #t) (kind 0)))) (hash "0asc9pib2gq46s971kyhi789l12ym6x0pq8j7rwkc3aqfr1izjq3")))

(define-public crate-sexpr_ir-0.3 (crate (name "sexpr_ir") (vers "0.3.2") (deps (list (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "pest") (req "^2.1.3") (default-features #t) (kind 0)) (crate-dep (name "pest_derive") (req "^2.1.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "1.0.*") (features (quote ("rc" "derive"))) (default-features #t) (kind 0)))) (hash "03nz2my44d54llf47glqnl1i58k5z8q44d65926f3ky4vrqvjjfm")))

(define-public crate-sexpr_ir-0.4 (crate (name "sexpr_ir") (vers "0.4.0") (deps (list (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "pest") (req "^2.1.3") (default-features #t) (kind 0)) (crate-dep (name "pest_derive") (req "^2.1.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "1.0.*") (features (quote ("rc" "derive"))) (default-features #t) (kind 0)))) (hash "19l4na1fravywh3va756m23hszc4bzlb7048lf0fls3lb3ybyd6l")))

(define-public crate-sexpr_ir-0.4 (crate (name "sexpr_ir") (vers "0.4.1") (deps (list (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "pest") (req "^2.1.3") (default-features #t) (kind 0)) (crate-dep (name "pest_derive") (req "^2.1.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "1.0.*") (features (quote ("rc" "derive"))) (default-features #t) (kind 0)))) (hash "14zqwxl124xl3cnvw8wvrkcw57921xx4qfsr4rbwc0rph6sh7gx2")))

(define-public crate-sexpr_ir-0.4 (crate (name "sexpr_ir") (vers "0.4.2") (deps (list (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "pest") (req "^2.1.3") (default-features #t) (kind 0)) (crate-dep (name "pest_derive") (req "^2.1.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "1.0.*") (features (quote ("rc" "derive"))) (default-features #t) (kind 0)))) (hash "1nqms2gv29bh9zvdii5k2lcq6wxafvl1pk2mp0adm3rxs9dyc5g7")))

(define-public crate-sexpr_ir-0.4 (crate (name "sexpr_ir") (vers "0.4.3") (deps (list (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "pest") (req "^2.1.3") (default-features #t) (kind 0)) (crate-dep (name "pest_derive") (req "^2.1.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "1.0.*") (features (quote ("rc" "derive"))) (default-features #t) (kind 0)))) (hash "18vxryfwn56z6hddrz95pq6p5gh6hix8cb626rpkima6cyzm5330")))

(define-public crate-sexpr_ir-0.4 (crate (name "sexpr_ir") (vers "0.4.4") (deps (list (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "pest") (req "^2.1.3") (default-features #t) (kind 0)) (crate-dep (name "pest_derive") (req "^2.1.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "1.0.*") (features (quote ("rc" "derive"))) (default-features #t) (kind 0)))) (hash "1dlvpwsdxwck9zidykpvvmmxgp5bq0acli3ijdihbclg87d1hx82")))

(define-public crate-sexpr_ir-0.5 (crate (name "sexpr_ir") (vers "0.5.0") (deps (list (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "pest") (req "^2.1.3") (default-features #t) (kind 0)) (crate-dep (name "pest_derive") (req "^2.1.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "1.0.*") (features (quote ("rc" "derive"))) (default-features #t) (kind 0)))) (hash "09w5bq3flrrny0g0k5n88igd2xhiz3yac63q8k522qx2c9xk3vpm")))

(define-public crate-sexpr_ir-0.6 (crate (name "sexpr_ir") (vers "0.6.0") (deps (list (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "pest") (req "^2.1.3") (default-features #t) (kind 0)) (crate-dep (name "pest_derive") (req "^2.1.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("rc" "derive"))) (default-features #t) (kind 0)))) (hash "18iasgf47fvfx6fm7xi1lpzbjjm8hdvl58054faa2mqky3hg13y6")))

(define-public crate-sexpr_ir-0.6 (crate (name "sexpr_ir") (vers "0.6.1") (deps (list (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "pest") (req "^2.1.3") (default-features #t) (kind 0)) (crate-dep (name "pest_derive") (req "^2.1.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("rc" "derive"))) (default-features #t) (kind 0)))) (hash "0sqshd66ryx99403lg3ap7m8p7lmx5mn5nsb46hqibw8qjz3cpf9")))

(define-public crate-sexpr_ir-0.7 (crate (name "sexpr_ir") (vers "0.7.0") (deps (list (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "pest") (req "^2.1.3") (default-features #t) (kind 0)) (crate-dep (name "pest_derive") (req "^2.1.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("rc" "derive"))) (default-features #t) (kind 0)))) (hash "0f0vhh0chbbq5rm6bq5q7427mc0dlqkas90va0lnwcwgzzsaj0lf")))

(define-public crate-sexpr_ir-0.7 (crate (name "sexpr_ir") (vers "0.7.1") (deps (list (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "pest") (req "^2.1.3") (default-features #t) (kind 0)) (crate-dep (name "pest_derive") (req "^2.1.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("rc" "derive"))) (default-features #t) (kind 0)))) (hash "13d5bv9fd3s4kq68h8gzisicj06f18k7i64nb50bfnxxc01r1m5v")))

(define-public crate-sexpr_ir-0.7 (crate (name "sexpr_ir") (vers "0.7.2") (deps (list (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "pest") (req "^2.1.3") (default-features #t) (kind 0)) (crate-dep (name "pest_derive") (req "^2.1.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("rc" "derive"))) (default-features #t) (kind 0)))) (hash "15lsih1z9cjf4frddzap6500z4mc4l806567v51k3xfm7dg1zmi1")))

(define-public crate-sexpr_matcher-0.1 (crate (name "sexpr_matcher") (vers "0.1.0") (hash "0ppb5hqfbh5cmlvw62h3xgxhwhzi4impkf15whhj2hx3wm94s38d")))

(define-public crate-sexpr_parser-0.1 (crate (name "sexpr_parser") (vers "0.1.0") (hash "0raslpwyjqsjjibp6pvhq1d7lbgnziyh850gnng0xih4c2pagnlv")))

(define-public crate-sexpr_parser-0.1 (crate (name "sexpr_parser") (vers "0.1.1") (hash "03433w77pyz5mdyqvldbm9k6lqjivhwdsfja9a0flfwrw4mim0a4")))

(define-public crate-sexpr_parser-0.1 (crate (name "sexpr_parser") (vers "0.1.2") (hash "0aj4913m2jlryihbwh0ma16gdmpkd1lvzdsh2pqcdh2kjz4scz8b")))

(define-public crate-sexpr_parser-0.2 (crate (name "sexpr_parser") (vers "0.2.0") (hash "1rrv2b3g3q0mcclbmzykr07vclr0ln7mljgl6k0gfdgyzd60a2ql")))

(define-public crate-sexpr_parser-0.2 (crate (name "sexpr_parser") (vers "0.2.1") (hash "0h7fcw1w15hz6qz936f4vz21bnlxbjqbl506k0sm4qhqi1ypap79")))

(define-public crate-sexpr_parser-0.3 (crate (name "sexpr_parser") (vers "0.3.0") (hash "12n6xcqfdhnfb5890jz0yfl18byxn5l0s17szjhc4r6fdk55x3yy")))

(define-public crate-sexpr_process-0.1 (crate (name "sexpr_process") (vers "0.1.0") (deps (list (crate-dep (name "sexpr_ir") (req "0.4.*") (default-features #t) (kind 0)))) (hash "1ial0f7c7syyylbdhf3bbzacxhsx3a2lgq74l4g2l3rsh68rlvi0")))

(define-public crate-sexpr_process-0.1 (crate (name "sexpr_process") (vers "0.1.1") (deps (list (crate-dep (name "sexpr_ir") (req "^0.5.0") (default-features #t) (kind 0)))) (hash "1wgvyp4yjrlq4ch5ynqmz7im2cca0nbiyxajqrhn9arf8qqdx6df")))

(define-public crate-sexpr_zero-0.1 (crate (name "sexpr_zero") (vers "0.1.0") (hash "1ri6smfywcf8xp6hj3wfag8gfzhp7v5lg0iq83wwi19kxrmrjca7")))

(define-public crate-sexprs-0.1 (crate (name "sexprs") (vers "0.1.0") (deps (list (crate-dep (name "serde") (req "^1") (default-features #t) (kind 0)))) (hash "1wz9d8gffxnpb9xhm1cdnyd21p90p7jp0jdzp1f93f3gymvym8r2")))

(define-public crate-sexpy-0.1 (crate (name "sexpy") (vers "0.1.0") (deps (list (crate-dep (name "nom") (req "~5.0") (default-features #t) (kind 0)) (crate-dep (name "sexpy_derive") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "1q8yackz8a25im49w83qyfwds650kr8h92vddxw3g1dyif17b4k0")))

(define-public crate-sexpy-0.1 (crate (name "sexpy") (vers "0.1.1") (deps (list (crate-dep (name "nom") (req "~5.0") (default-features #t) (kind 0)) (crate-dep (name "sexpy_derive") (req "~0.1") (default-features #t) (kind 0)))) (hash "0xb7hxg27lr6zpcz82dr4z39c35kd6yr1b6rlmvhf2kpzkf4c9qs")))

(define-public crate-sexpy-0.1 (crate (name "sexpy") (vers "0.1.2") (deps (list (crate-dep (name "nom") (req "~5.0") (default-features #t) (kind 0)) (crate-dep (name "sexpy_derive") (req "~0.1") (default-features #t) (kind 0)))) (hash "0gk3mv04v5qij75jagg0zb4mrywanxcxgbdgppz3gz1fi7g8lams")))

(define-public crate-sexpy-0.2 (crate (name "sexpy") (vers "0.2.0") (deps (list (crate-dep (name "nom") (req "~5.0") (default-features #t) (kind 0)) (crate-dep (name "sexpy_derive") (req "~0.2") (default-features #t) (kind 0)))) (hash "02f2r3q7hz70m1vgpyq6bdrlrs4zy42hxabyi8cdn6vbn2mnqggg")))

(define-public crate-sexpy-0.3 (crate (name "sexpy") (vers "0.3.0") (deps (list (crate-dep (name "nom") (req "~5.0") (default-features #t) (kind 0)) (crate-dep (name "sexpy_derive") (req "~0.3") (default-features #t) (kind 0)))) (hash "1hfizgl4aa67ck5ackcs40mask38v21qk7c8763w2cgjvqnsncly")))

(define-public crate-sexpy-0.4 (crate (name "sexpy") (vers "0.4.0") (deps (list (crate-dep (name "nom") (req "~5.0") (default-features #t) (kind 0)) (crate-dep (name "sexpy_derive") (req "~0.4") (default-features #t) (kind 0)))) (hash "0g3qbjkg6g6cacirgjgcfmriy24qaq4k9yh63rbh092ffz0pk9m5")))

(define-public crate-sexpy-0.5 (crate (name "sexpy") (vers "0.5.0") (deps (list (crate-dep (name "nom") (req "~5.0") (default-features #t) (kind 0)) (crate-dep (name "sexpy_derive") (req "~0.5") (default-features #t) (kind 0)))) (hash "1viswmj1dzjyz7cw1fxl40rkl84kc4vmcb02r1w27yb1940k5qs1")))

(define-public crate-sexpy-0.5 (crate (name "sexpy") (vers "0.5.1") (deps (list (crate-dep (name "nom") (req "~5.0") (default-features #t) (kind 0)) (crate-dep (name "sexpy_derive") (req "~0.5") (default-features #t) (kind 0)))) (hash "05iw0140lh0sz2y8napmmkffhix1dw6pw9kg08pw57y0pmqgl7z8")))

(define-public crate-sexpy-0.5 (crate (name "sexpy") (vers "0.5.2") (deps (list (crate-dep (name "nom") (req "~5.0") (default-features #t) (kind 0)) (crate-dep (name "sexpy_derive") (req "~0.5") (default-features #t) (kind 0)))) (hash "0z3x9qwc7lkk2w0wzbfln4z1x3fy7lmxil9b55w83p49y6lvy1zj")))

(define-public crate-sexpy-0.5 (crate (name "sexpy") (vers "0.5.3") (deps (list (crate-dep (name "nom") (req "~5.0") (default-features #t) (kind 0)) (crate-dep (name "sexpy_derive") (req "^0.5.2") (default-features #t) (kind 0)))) (hash "0hgrnkq3n6gmfhwd9y46nf48nzv6irnircjhn6hhx32jaskndcsc")))

(define-public crate-sexpy-0.5 (crate (name "sexpy") (vers "0.5.4") (deps (list (crate-dep (name "nom") (req "^5.1.1") (default-features #t) (kind 0)) (crate-dep (name "sexpy_derive") (req "^0.5.2") (default-features #t) (kind 0)))) (hash "0h0qwribi57jbgk9ia1dcbkmhf6wjl5lc6kbv8dx7b9af4g5mn8x")))

(define-public crate-sexpy-0.5 (crate (name "sexpy") (vers "0.5.5") (deps (list (crate-dep (name "nom") (req "^5.1.1") (default-features #t) (kind 0)) (crate-dep (name "sexpy_derive") (req "^0.5.3") (default-features #t) (kind 0)))) (hash "1slzggy856nnypl7qq01fgnqw86gh2jib3swyxafqsa2gqqhsj3j")))

(define-public crate-sexpy-0.5 (crate (name "sexpy") (vers "0.5.6") (deps (list (crate-dep (name "nom") (req "^5.1.1") (default-features #t) (kind 0)) (crate-dep (name "sexpy_derive") (req "^0.5.3") (default-features #t) (kind 0)))) (hash "0lichh0ccdfxqviw1g3ldw1z5ym9x3dc442nzygf4d2n1lvsaqks")))

(define-public crate-sexpy_derive-0.1 (crate (name "sexpy_derive") (vers "0.1.0") (deps (list (crate-dep (name "proc-macro-error") (req "^0.4.3") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (default-features #t) (kind 0)))) (hash "1cnp92xaqd36yqxcal49cxb2mjwxcmkm8jxdw9qv17dj0vijldmf")))

(define-public crate-sexpy_derive-0.1 (crate (name "sexpy_derive") (vers "0.1.1") (deps (list (crate-dep (name "proc-macro-error") (req "^0.4.3") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (default-features #t) (kind 0)))) (hash "0951lh3gl4phkgsgw2xk21ykakb0k9gvkplijx4xswfr7a4d7pw4")))

(define-public crate-sexpy_derive-0.2 (crate (name "sexpy_derive") (vers "0.2.0") (deps (list (crate-dep (name "proc-macro-error") (req "^0.4.3") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (default-features #t) (kind 0)))) (hash "150sb66dvndxndzmsz3sw9ikxdvqckhjvvlffjlqz9ak4bmvl9nr")))

(define-public crate-sexpy_derive-0.3 (crate (name "sexpy_derive") (vers "0.3.0") (deps (list (crate-dep (name "proc-macro-error") (req "^0.4.3") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (default-features #t) (kind 0)))) (hash "06wda77d4b0qjm9nkznnw03ddiy4qjc4sy1wc8y870rfzip6vavi")))

(define-public crate-sexpy_derive-0.4 (crate (name "sexpy_derive") (vers "0.4.0") (deps (list (crate-dep (name "proc-macro-error") (req "^0.4.3") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (default-features #t) (kind 0)))) (hash "115k2l6x619j9x8plf3x7nqyywsxgb7rnxaxw218v05cj2blfkq9")))

(define-public crate-sexpy_derive-0.5 (crate (name "sexpy_derive") (vers "0.5.0") (deps (list (crate-dep (name "proc-macro-error") (req "^0.4.3") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (default-features #t) (kind 0)))) (hash "0n6z9yaxqbhlk1lwy24c34gih922mb7529mccsjj75r6192s4ihn")))

(define-public crate-sexpy_derive-0.5 (crate (name "sexpy_derive") (vers "0.5.2") (deps (list (crate-dep (name "proc-macro-error") (req "^0.4.3") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (default-features #t) (kind 0)))) (hash "08wgcw7rym89p5sl49xj24zg6xx12ym3ga8y9j15vj0iq56v5zf0")))

(define-public crate-sexpy_derive-0.5 (crate (name "sexpy_derive") (vers "0.5.3") (deps (list (crate-dep (name "proc-macro-error") (req "^0.4.3") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (default-features #t) (kind 0)))) (hash "1mkxdqgknr428q77khia4fqv8n9wdk8w471jqdhfxid8hx1wpy6f")))

