(define-module (crates-io se mk) #:use-module (crates-io))

(define-public crate-semka-1 (crate (name "semka") (vers "1.0.0") (deps (list (crate-dep (name "error-code") (req "^2") (default-features #t) (target "cfg(unix)") (kind 0)) (crate-dep (name "libc") (req "^0.2") (target "cfg(unix)") (kind 0)))) (hash "0xvbbz1bm12dlzpwj6h91r2ynv0k2j6nk6qqc345khr8s7qhv01f") (yanked #t)))

(define-public crate-semka-1 (crate (name "semka") (vers "1.0.1") (deps (list (crate-dep (name "error-code") (req "^2") (default-features #t) (target "cfg(unix)") (kind 0)) (crate-dep (name "libc") (req "^0.2") (target "cfg(unix)") (kind 0)))) (hash "09p2zj08lshpx8i9184jaw9y8897ibv9bn9z28l2smlpq84y87k9") (yanked #t)))

(define-public crate-semka-2 (crate (name "semka") (vers "2.0.0") (deps (list (crate-dep (name "error-code") (req "^2") (default-features #t) (target "cfg(unix)") (kind 0)) (crate-dep (name "libc") (req "^0.2") (target "cfg(unix)") (kind 0)))) (hash "1p2z4lzw3d7vbhg6v0gajpv12ihwlrcwykzfx2h3083n6zxq473a") (yanked #t)))

(define-public crate-semka-2 (crate (name "semka") (vers "2.0.1") (deps (list (crate-dep (name "error-code") (req "^2") (default-features #t) (target "cfg(unix)") (kind 0)) (crate-dep (name "libc") (req "^0.2") (target "cfg(unix)") (kind 0)))) (hash "068dd2s1l9gp22pxvq77zb6ii793pg76bamjc51i9v71hr4srl4a") (yanked #t)))

(define-public crate-semka-2 (crate (name "semka") (vers "2.0.2") (deps (list (crate-dep (name "error-code") (req "^2") (default-features #t) (target "cfg(unix)") (kind 0)) (crate-dep (name "libc") (req "^0.2") (target "cfg(unix)") (kind 0)))) (hash "0p5s2yxqdmmbrssay54m3lqpxq49c6mzkcf0cvs0ax9h8vir1qj3") (yanked #t)))

(define-public crate-semka-2 (crate (name "semka") (vers "2.0.3") (deps (list (crate-dep (name "error-code") (req "^2") (default-features #t) (target "cfg(unix)") (kind 0)) (crate-dep (name "libc") (req "^0.2") (target "cfg(unix)") (kind 0)))) (hash "0l9rj3v6gljg2l341qnxg1v13qcrqp8ixnc715bbbp3bjmyck4kx")))

(define-public crate-semka-2 (crate (name "semka") (vers "2.0.4") (deps (list (crate-dep (name "error-code") (req "^2") (default-features #t) (target "cfg(unix)") (kind 0)) (crate-dep (name "libc") (req "^0.2") (target "cfg(unix)") (kind 0)))) (hash "1fa6b7cxjz252plbi55vfqhbc6c7r366gg12afl32xp1f5vp04gh")))

(define-public crate-semka-2 (crate (name "semka") (vers "2.0.5") (deps (list (crate-dep (name "error-code") (req "^2") (default-features #t) (target "cfg(unix)") (kind 0)) (crate-dep (name "libc") (req "^0.2") (target "cfg(unix)") (kind 0)))) (hash "1wssymy3ma0rv548qcxa4b7d2vbwa5xihs338xby0mj1dpzg1v1s") (yanked #t)))

(define-public crate-semka-2 (crate (name "semka") (vers "2.0.6") (deps (list (crate-dep (name "error-code") (req "^2") (default-features #t) (target "cfg(unix)") (kind 0)) (crate-dep (name "libc") (req "^0.2") (target "cfg(unix)") (kind 0)))) (hash "1bcav3rz65clzd07vqmdyydiy315yzispn8n6278s0apy9jpdpcb")))

(define-public crate-semka-2 (crate (name "semka") (vers "2.0.7") (deps (list (crate-dep (name "error-code") (req "^3") (default-features #t) (target "cfg(unix)") (kind 0)) (crate-dep (name "libc") (req "^0.2") (target "cfg(unix)") (kind 0)))) (hash "1swkgy7a7jaz83gk11qqaxy44bvlyny4v63ym23q89dikajknpn3")))

