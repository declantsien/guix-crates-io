(define-module (crates-io se qi) #:use-module (crates-io))

(define-public crate-seqid-0.1 (crate (name "seqid") (vers "0.1.0") (hash "1v6qa57547h33gz2sybkg5xy7vhbk5fndvfvz2ympg4gp435rilj")))

(define-public crate-seqid-0.1 (crate (name "seqid") (vers "0.1.1") (hash "0yvpwfkgpr9v7hd534c38ayw9431pjfbk212s608p6mv5960dx7a") (features (quote (("default" "alloc") ("alloc"))))))

(define-public crate-seqid-0.1 (crate (name "seqid") (vers "0.1.2") (hash "10vhp8q5cmmwyy032v1sz5gixpcav08lblplpgrsv5by1wabjn14") (features (quote (("default" "alloc") ("alloc"))))))

(define-public crate-seqid-0.1 (crate (name "seqid") (vers "0.1.3") (hash "117g8rn72r4ww00h7n2h85f04lg99npahjdwrc40ggah7bhb6yrn") (features (quote (("default" "alloc") ("alloc"))))))

(define-public crate-seqio-0.1 (crate (name "seqio") (vers "0.1.0") (deps (list (crate-dep (name "flate2") (req "^1.0.28") (default-features #t) (kind 0)))) (hash "02ap9pdf2j2si3inb0xzar34b97khivn0s9mf87vgz0sy701l0sd") (rust-version "1.65.0")))

(define-public crate-seqio-0.1 (crate (name "seqio") (vers "0.1.1") (deps (list (crate-dep (name "flate2") (req "^1.0.28") (default-features #t) (kind 0)))) (hash "03bnhcjx9d8xl1f68nvlmfmgjc6hcgs4wy6ik63r13csrnn2iql8") (rust-version "1.65.0")))

