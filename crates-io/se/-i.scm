(define-module (crates-io se #{-i}#) #:use-module (crates-io))

(define-public crate-se-io-0.1 (crate (name "se-io") (vers "0.1.0") (deps (list (crate-dep (name "byteorder") (req "^1.4.3") (default-features #t) (kind 0)))) (hash "0p3w5whb0dqps0jn4mqag1zc952piqnvd4llycknbpxnmrqkd8v5")))

