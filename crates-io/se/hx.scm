(define-module (crates-io se hx) #:use-module (crates-io))

(define-public crate-sehx-0.1 (crate (name "sehx") (vers "0.1.0") (hash "0sdza67vy90yk8ya2i8371vhvcyx8r9nzrfikyd7aib3g17k6inl")))

(define-public crate-sehx-0.1 (crate (name "sehx") (vers "0.1.1") (hash "1w44jnpmskggkg7cwaj4rrqjflirk62m54y0rpf4jz1qrp4d7i3h")))

(define-public crate-sehx-0.1 (crate (name "sehx") (vers "0.1.2") (hash "1l5lkvvb7y4cm5sqrmigc1ij5lg25x4r4i5gbnzvq3in2qknq06x")))

