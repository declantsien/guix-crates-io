(define-module (crates-io se ax) #:use-module (crates-io))

(define-public crate-seax-0.0.2 (crate (name "seax") (vers "0.0.2") (deps (list (crate-dep (name "docopt") (req "*") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "*") (default-features #t) (kind 0)) (crate-dep (name "rustc-serialize") (req "*") (default-features #t) (kind 0)) (crate-dep (name "seax_scheme") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "seax_svm") (req "^0.2.2") (default-features #t) (kind 0)))) (hash "1g9h9f3xg0qj40q2y5mazn0j1ypi61bmy68ffx6pc5ddxqliqhmf")))

(define-public crate-seax_scheme-0.0.2 (crate (name "seax_scheme") (vers "0.0.2") (deps (list (crate-dep (name "parser-combinators") (req "~0.2.2") (default-features #t) (kind 0)) (crate-dep (name "seax_svm") (req "^0.1.3") (default-features #t) (kind 0)))) (hash "0ssj5rdkj1y5jn1kc0vjmfigypqh5klysxs2gsy6cs60sqrq3klz")))

(define-public crate-seax_scheme-0.0.3 (crate (name "seax_scheme") (vers "0.0.3") (deps (list (crate-dep (name "parser-combinators") (req "~0.2.6") (default-features #t) (kind 0)) (crate-dep (name "seax_svm") (req "~0.2.0") (default-features #t) (kind 0)))) (hash "1fsp33ckff7r98i0100f83n65l7ngfrbzgh1p9ffapcl1r1czmjj")))

(define-public crate-seax_scheme-0.0.6 (crate (name "seax_scheme") (vers "0.0.6") (deps (list (crate-dep (name "parser-combinators") (req "~0.2.6") (default-features #t) (kind 0)) (crate-dep (name "seax_svm") (req "^0.2.2") (default-features #t) (kind 0)))) (hash "1vq0lz1j91xjhjil3xjdh8ilzjxzhd973vp9y81xyn10s0yywlrn")))

(define-public crate-seax_scheme-0.1 (crate (name "seax_scheme") (vers "0.1.0") (deps (list (crate-dep (name "parser-combinators") (req "~0.2.6") (default-features #t) (kind 0)) (crate-dep (name "seax_svm") (req "^0.2.2") (default-features #t) (kind 0)))) (hash "19mnv9rlick2darl3ca1cd0jvmpw0xvvk5smmwq1vfj4id661vb2")))

(define-public crate-seax_scheme-0.1 (crate (name "seax_scheme") (vers "0.1.1") (deps (list (crate-dep (name "parser-combinators") (req "~0.2.6") (default-features #t) (kind 0)) (crate-dep (name "seax_svm") (req "^0.2.2") (default-features #t) (kind 0)))) (hash "1qkxy199wc3jq0h6iw3xkhpr9n31avb11cbl44flqhhsjas7c61r")))

(define-public crate-seax_scheme-0.2 (crate (name "seax_scheme") (vers "0.2.1") (deps (list (crate-dep (name "log") (req "^0.3.1") (default-features #t) (kind 0)) (crate-dep (name "parser-combinators") (req "~0.2.6") (default-features #t) (kind 0)) (crate-dep (name "seax_svm") (req "^0.2.2") (default-features #t) (kind 0)))) (hash "01bga7z1pw732lfqrm5xpbva55g518npjw63na79xlaw9al5nkm6")))

(define-public crate-seax_scheme-0.3 (crate (name "seax_scheme") (vers "0.3.0") (deps (list (crate-dep (name "log") (req "^0.3.1") (default-features #t) (kind 0)) (crate-dep (name "parser-combinators") (req "~0.2.6") (default-features #t) (kind 0)) (crate-dep (name "seax_svm") (req "^0.2.6") (default-features #t) (kind 0)))) (hash "1iq88fzl7xhikfqlpfk3x7056r53ss5d4psnrda64940hj0f23js")))

(define-public crate-seax_svm-0.1 (crate (name "seax_svm") (vers "0.1.2") (hash "1krzkj9bixg7hdr3cwpbqv2lfj2dwjb00051g170zci777bxfcjh")))

(define-public crate-seax_svm-0.1 (crate (name "seax_svm") (vers "0.1.3") (hash "004j15lv2ghhzf129n9v07nswz8s92bwrcg0jvg2wbfzw6rm95w8")))

(define-public crate-seax_svm-0.2 (crate (name "seax_svm") (vers "0.2.0-rc.2") (hash "07hd628sy270hc4c1kh5fkkb6bv20bj33kd7lg8adsp7yilija6y")))

(define-public crate-seax_svm-0.2 (crate (name "seax_svm") (vers "0.2.0-rc.3") (hash "1q4pafc1w20mwv1s7p07k9irymmrq6axqh1fjbkn1k8akr3f414v")))

(define-public crate-seax_svm-0.2 (crate (name "seax_svm") (vers "0.2.3") (hash "1vnv9hjnxz0cy5p86ymjhd4vpw24bzw9s8z1rqc6dwmg2i54a9x0")))

(define-public crate-seax_svm-0.2 (crate (name "seax_svm") (vers "0.2.5") (deps (list (crate-dep (name "byteorder") (req "*") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.3.1") (default-features #t) (kind 0)) (crate-dep (name "quickcheck") (req "*") (default-features #t) (kind 2)))) (hash "034m6vi8dr66szsaxphkn3axcfihvz0vp5jw6icg8yn21xc2fmpa")))

(define-public crate-seax_svm-0.2 (crate (name "seax_svm") (vers "0.2.6") (deps (list (crate-dep (name "byteorder") (req "*") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.3.1") (default-features #t) (kind 0)) (crate-dep (name "quickcheck") (req "*") (default-features #t) (kind 2)))) (hash "1vbdc2bhy5cvhgzlaka3s4g923zb6n98c5mg52qslcl9kasmnli6")))

(define-public crate-seax_svm-0.2 (crate (name "seax_svm") (vers "0.2.7") (deps (list (crate-dep (name "byteorder") (req "*") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.3.1") (default-features #t) (kind 0)) (crate-dep (name "quickcheck") (req "*") (default-features #t) (kind 2)))) (hash "1693vj2mhy0f9fradgbppyapp489mndbq6n7q1k4a7hi1vrd4iwz")))

(define-public crate-seax_svm-0.2 (crate (name "seax_svm") (vers "0.2.8") (deps (list (crate-dep (name "byteorder") (req "*") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.3.1") (default-features #t) (kind 0)) (crate-dep (name "quickcheck") (req "*") (default-features #t) (kind 2)))) (hash "0dyd3dkzg2r9pgbaakmg6w47i4l3pqj2vkqf8flpql3r9n9d6ppw") (features (quote (("nightly"))))))

(define-public crate-seax_util-0.1 (crate (name "seax_util") (vers "0.1.0") (deps (list (crate-dep (name "byteorder") (req "*") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.3.1") (default-features #t) (kind 0)) (crate-dep (name "quickcheck") (req "*") (default-features #t) (kind 2)))) (hash "1xlmsrnbl3z7fn534kcf05zfm38f3wkvkr47i2ngj1dmxz8nk1ds") (features (quote (("unstable"))))))

(define-public crate-seax_util-0.1 (crate (name "seax_util") (vers "0.1.1") (deps (list (crate-dep (name "byteorder") (req "*") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.3.1") (default-features #t) (kind 0)) (crate-dep (name "quickcheck") (req "*") (default-features #t) (kind 2)))) (hash "0c0az45yv25wa9gxigp1xmrqvnk0ykrb6883chp86lyxqj5s3jg3") (features (quote (("unstable"))))))

(define-public crate-seax_util-0.1 (crate (name "seax_util") (vers "0.1.2") (deps (list (crate-dep (name "byteorder") (req "*") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.3.1") (default-features #t) (kind 0)) (crate-dep (name "quickcheck") (req "*") (default-features #t) (kind 2)))) (hash "0bvy8xymscfbcfs2633mx4nd2pfnr7hzcrx87vspb6icjf5nl9gn") (features (quote (("unstable"))))))

