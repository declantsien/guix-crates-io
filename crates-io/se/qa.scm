(define-module (crates-io se qa) #:use-module (crates-io))

(define-public crate-seqalign-0.1 (crate (name "seqalign") (vers "0.1.0") (deps (list (crate-dep (name "lazy_static") (req "^1.0") (default-features #t) (kind 2)))) (hash "1k1i7v2q15spvrg3kq7jyx5fjz8zw761kb8aclvwz07vgz7h90mm")))

(define-public crate-seqalign-0.1 (crate (name "seqalign") (vers "0.1.1") (deps (list (crate-dep (name "lazy_static") (req "^1.0") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.4") (default-features #t) (kind 2)))) (hash "0k6rwl1y852h4k6r1jwlhnw391y7bmhjz3h0ncm3m3634xnfdjm7")))

(define-public crate-seqalign-0.2 (crate (name "seqalign") (vers "0.2.1") (deps (list (crate-dep (name "lazy_static") (req "^1") (default-features #t) (kind 2)) (crate-dep (name "maplit") (req "^1") (default-features #t) (kind 2)) (crate-dep (name "pretty_assertions") (req "^0.5") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.4") (default-features #t) (kind 2)))) (hash "0qlswjxvaxkx3058w1061m404q833p752inqgrwkdd2m47jr98xw")))

(define-public crate-seqalign-0.2 (crate (name "seqalign") (vers "0.2.2") (deps (list (crate-dep (name "lazy_static") (req "^1") (default-features #t) (kind 2)) (crate-dep (name "maplit") (req "^1") (default-features #t) (kind 2)) (crate-dep (name "pretty_assertions") (req "^0.5") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.4") (default-features #t) (kind 2)))) (hash "0pdv8r9a6hf5kya2jgax8hc493kpmyg7q06207lzh1h03xscbmwr")))

(define-public crate-seqalign-0.2 (crate (name "seqalign") (vers "0.2.3") (deps (list (crate-dep (name "lazy_static") (req "^1") (default-features #t) (kind 2)) (crate-dep (name "maplit") (req "^1") (default-features #t) (kind 2)) (crate-dep (name "pretty_assertions") (req "^0.5") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.8") (default-features #t) (kind 2)))) (hash "1xv3kcyhp9x1bkvbdy28lh7k8fd3p4pm84cya0xqbacp47ycqja7")))

