(define-module (crates-io se ou) #:use-module (crates-io))

(define-public crate-seoul-0.1 (crate (name "seoul") (vers "0.1.0") (deps (list (crate-dep (name "seoul-derive") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "1p6smkdy7k0iif71rz52a9dhinz6f89lh3jmj60dclpqb8b2ks4l")))

(define-public crate-seoul-derive-0.1 (crate (name "seoul-derive") (vers "0.1.0") (deps (list (crate-dep (name "proc-macro2") (req "^1.0.76") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.33") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2.0.39") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "0zm8vz957h9iqz82y2azp6gjndyngz0nplgnyscrqsg1v5k62wgs")))

