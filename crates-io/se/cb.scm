(define-module (crates-io se cb) #:use-module (crates-io))

(define-public crate-secbox-0.1 (crate (name "secbox") (vers "0.1.0") (deps (list (crate-dep (name "libc") (req "^0.2.14") (default-features #t) (kind 0)))) (hash "0m81prjdgh5scbj2jyqjc4azhgzrj0k58vskghayzz365xx95m54")))

(define-public crate-secbox-0.1 (crate (name "secbox") (vers "0.1.1") (deps (list (crate-dep (name "libc") (req "^0.2.14") (default-features #t) (kind 0)))) (hash "00l9shk7iijsbim09cmnyzza1v828g5nhjaq3q2r7lraxsqa9j3m")))

