(define-module (crates-io se l4) #:use-module (crates-io))

(define-public crate-sel4-0.0.1 (crate (name "sel4") (vers "0.0.1") (deps (list (crate-dep (name "sel4-sys") (req "^0.0.15") (default-features #t) (kind 0)))) (hash "0grxs1wvv164fbk88dc0p06w8c1p3q86xr8xsvpqw8cj81bvxfqp") (features (quote (("unstable"))))))

(define-public crate-sel4-0.0.2 (crate (name "sel4") (vers "0.0.2") (deps (list (crate-dep (name "sel4-sys") (req "^0.0.20") (default-features #t) (kind 0)))) (hash "11r3lnbxsipwvl4v5lfkgc5423br0i0h7m64qbh74bdawv3zbl52") (features (quote (("unstable"))))))

(define-public crate-sel4-0.0.4 (crate (name "sel4") (vers "0.0.4") (deps (list (crate-dep (name "sel4-sys") (req "^0.0.22") (default-features #t) (kind 0)))) (hash "0gj9s5qd5dndv65baa8ayzjibx77iaxc2jgigc3b9fwh0a7qa90v") (features (quote (("unstable"))))))

(define-public crate-sel4-0.0.5 (crate (name "sel4") (vers "0.0.5") (deps (list (crate-dep (name "sel4-sys") (req "^0.0.22") (default-features #t) (kind 0)))) (hash "0kvi55bwyp6zjap7phj7rjjg3v30w4nm0im1wa33kxwbby76p5sk") (features (quote (("unstable"))))))

(define-public crate-sel4-0.0.6 (crate (name "sel4") (vers "0.0.6") (deps (list (crate-dep (name "sel4-sys") (req "^0.0.22") (default-features #t) (kind 0)))) (hash "0ic3gwwvqczalmwrzf6wdlc43n55ab4f7i1kwdr6z5vc3p1z5zaa") (features (quote (("unstable"))))))

(define-public crate-sel4-0.0.7 (crate (name "sel4") (vers "0.0.7") (deps (list (crate-dep (name "sel4-sys") (req "^0.0.23") (default-features #t) (kind 0)))) (hash "0ijkcbfl8yga1kvd2sqzl7pbwvwxrfizl37m66x8l2flprvbanl4") (features (quote (("unstable"))))))

(define-public crate-sel4-0.0.8 (crate (name "sel4") (vers "0.0.8") (deps (list (crate-dep (name "sel4-sys") (req "^0.0.25") (default-features #t) (kind 0)))) (hash "1g9yhp3yawadhc0p3mlw4jzggg34qmh4idcqmf0i6ifdc9k50zv6") (features (quote (("unstable"))))))

(define-public crate-sel4-0.0.9 (crate (name "sel4") (vers "0.0.9") (deps (list (crate-dep (name "sel4-sys") (req "^0.0.26") (default-features #t) (kind 0)))) (hash "0l30g5vsqfnirpb72ikcrz8ga7x123g66w6sav0jp6yry03ba9nc") (features (quote (("unstable"))))))

(define-public crate-sel4-0.0.10 (crate (name "sel4") (vers "0.0.10") (deps (list (crate-dep (name "sel4-sys") (req "^0.0.26") (default-features #t) (kind 0)))) (hash "0g7bsj1srgrpa2v8a6yix3c7khjp1hlar9xmwhmd4vsfz47ckbqr") (features (quote (("unstable"))))))

(define-public crate-sel4-0.0.11 (crate (name "sel4") (vers "0.0.11") (deps (list (crate-dep (name "sel4-sys") (req "^0.0.27") (default-features #t) (kind 0)))) (hash "0nb7qhrl3rz9mh4hp1jyvnyar6kr0478g3ybw1xixw8741m5nbkv") (features (quote (("unstable"))))))

(define-public crate-sel4-0.0.12 (crate (name "sel4") (vers "0.0.12") (deps (list (crate-dep (name "sel4-sys") (req "^0.0.28") (default-features #t) (kind 0)))) (hash "1yrglwzfxydzkg221c9wswrbxi3z8p6nbycfig2pc3vn4sps4idr") (features (quote (("unstable"))))))

(define-public crate-sel4-start-0.0.4 (crate (name "sel4-start") (vers "0.0.4") (deps (list (crate-dep (name "maplit") (req "^0.1.3") (default-features #t) (kind 1)) (crate-dep (name "sel4-sys") (req "^0.0.4") (default-features #t) (kind 0)))) (hash "1nmxc2wnxs5i68vj1gn12kmz1m709w9s9r1d6cgzzdh7cy3s7x5s") (features (quote (("unstable"))))))

(define-public crate-sel4-start-0.0.5 (crate (name "sel4-start") (vers "0.0.5") (deps (list (crate-dep (name "maplit") (req "^0.1.3") (default-features #t) (kind 1)) (crate-dep (name "sel4-sys") (req "^0.0.5") (default-features #t) (kind 0)))) (hash "1ninmxkjsziqpyh33z1yn2f4j37s9p9b7q29ncmib82cgx5j7xab") (features (quote (("unstable"))))))

(define-public crate-sel4-start-0.0.6 (crate (name "sel4-start") (vers "0.0.6") (deps (list (crate-dep (name "maplit") (req "^0.1.3") (default-features #t) (kind 1)) (crate-dep (name "sel4-sys") (req "^0.0.6") (default-features #t) (kind 0)))) (hash "06bc5l2pzysp9slv0xpdccdz8i62h3rfpr1i6356k84nls6b2syc") (features (quote (("unstable"))))))

(define-public crate-sel4-start-0.0.15 (crate (name "sel4-start") (vers "0.0.15") (deps (list (crate-dep (name "maplit") (req "^0.1.3") (default-features #t) (kind 1)) (crate-dep (name "sel4-sys") (req "^0.0.15") (default-features #t) (kind 0)))) (hash "1x55p7bf2z6v6vcmwfvq5dzshvqv77f08dxv3p7d0fnpw3yz1224") (features (quote (("unstable"))))))

(define-public crate-sel4-start-0.0.18 (crate (name "sel4-start") (vers "0.0.18") (deps (list (crate-dep (name "maplit") (req "^0.1.3") (default-features #t) (kind 1)) (crate-dep (name "sel4-sys") (req "^0.0.18") (default-features #t) (kind 0)))) (hash "17vxzll9md69v1iqsa83j4gqkpxcbmslfzkl99w7fi6xm2gdhb8p") (features (quote (("unstable"))))))

(define-public crate-sel4-start-0.0.19 (crate (name "sel4-start") (vers "0.0.19") (deps (list (crate-dep (name "maplit") (req "^0.1.3") (default-features #t) (kind 1)) (crate-dep (name "sel4-sys") (req "^0.0.19") (default-features #t) (kind 0)))) (hash "1wmp5sij8phqlxc705690iydmkg184psq4azj0rrl0dayy9p884i") (features (quote (("unstable"))))))

(define-public crate-sel4-start-0.0.20 (crate (name "sel4-start") (vers "0.0.20") (deps (list (crate-dep (name "maplit") (req "^0.1.3") (default-features #t) (kind 1)) (crate-dep (name "sel4-sys") (req "^0.0.20") (default-features #t) (kind 0)))) (hash "0kj3b241350sql4k6vq5lxrfv1lj16lnwxmmvlyswj860g2v62gk") (features (quote (("unstable"))))))

(define-public crate-sel4-start-0.0.21 (crate (name "sel4-start") (vers "0.0.21") (deps (list (crate-dep (name "maplit") (req "^0.1.3") (default-features #t) (kind 1)) (crate-dep (name "sel4") (req "^0.0.2") (default-features #t) (kind 0)) (crate-dep (name "sel4-sys") (req "^0.0.20") (default-features #t) (kind 0)))) (hash "0adh382s8c38s4l3a75gr61ps02xnyyfkq7ksj0hv6l9csw7wpgv") (features (quote (("unstable"))))))

(define-public crate-sel4-start-0.0.23 (crate (name "sel4-start") (vers "0.0.23") (deps (list (crate-dep (name "maplit") (req "^0.1.3") (default-features #t) (kind 1)) (crate-dep (name "sel4") (req "^0.0.7") (default-features #t) (kind 0)) (crate-dep (name "sel4-sys") (req "^0.0.23") (default-features #t) (kind 0)))) (hash "0hl8d87nvbynzhnkg1ag5qxkpacf5lhf1m5z26j863132pqs1j85") (features (quote (("unstable"))))))

(define-public crate-sel4-start-0.0.26 (crate (name "sel4-start") (vers "0.0.26") (deps (list (crate-dep (name "maplit") (req "^0.1.3") (default-features #t) (kind 1)) (crate-dep (name "sel4") (req "^0.0.9") (default-features #t) (kind 0)) (crate-dep (name "sel4-sys") (req "^0.0.26") (default-features #t) (kind 0)))) (hash "19fhlkkqd4lnhzp4b3pi0afgxw3p3fbh6ii6j8aq602j2z4hmgxp") (features (quote (("unstable"))))))

(define-public crate-sel4-start-0.0.27 (crate (name "sel4-start") (vers "0.0.27") (deps (list (crate-dep (name "maplit") (req "^0.1.3") (default-features #t) (kind 1)) (crate-dep (name "sel4") (req "^0.0.11") (default-features #t) (kind 0)) (crate-dep (name "sel4-sys") (req "^0.0.27") (default-features #t) (kind 0)))) (hash "08xhbmn4i4gj4r1r0zrsaibbc3yl7k6rhq8isznng4bvmz78jl2j") (features (quote (("unstable"))))))

(define-public crate-sel4-start-0.0.28 (crate (name "sel4-start") (vers "0.0.28") (deps (list (crate-dep (name "maplit") (req "^0.1.3") (default-features #t) (kind 1)) (crate-dep (name "sel4") (req "^0.0.12") (default-features #t) (kind 0)) (crate-dep (name "sel4-sys") (req "^0.0.28") (default-features #t) (kind 0)))) (hash "1as3cn5v9aymgy20wcvn8mi4vg5yij7sy8lk4vhalkcl1rhxhkq1") (features (quote (("unstable"))))))

(define-public crate-sel4-sys-0.0.1 (crate (name "sel4-sys") (vers "0.0.1") (hash "08f363csrdic6ixxbiydhl51al4i8hvdwglxk61dijj3fhhyqayl") (features (quote (("unstable"))))))

(define-public crate-sel4-sys-0.0.2 (crate (name "sel4-sys") (vers "0.0.2") (hash "1scjg9ppfmv9vbd60pr647k63m6r5y07pbx4m58qwqhag882hiyb") (features (quote (("unstable") ("SEL4_DEBUG") ("SEL4_DANGEROUS_CODE_INJECTION") ("SEL4_CONFIG_BENCHMARK"))))))

(define-public crate-sel4-sys-0.0.3 (crate (name "sel4-sys") (vers "0.0.3") (hash "13z202inr4zqwl898nz7hxa9h84s5lc52c8m2q6ix14fcfvc59f9") (features (quote (("unstable") ("SEL4_DEBUG") ("SEL4_DANGEROUS_CODE_INJECTION") ("SEL4_CONFIG_BENCHMARK"))))))

(define-public crate-sel4-sys-0.0.4 (crate (name "sel4-sys") (vers "0.0.4") (hash "03zdhwv8z5dkixa1halxq7r4isjnc86vhdzavq257jrwwdfsg09l") (features (quote (("unstable") ("SEL4_DEBUG") ("SEL4_DANGEROUS_CODE_INJECTION") ("SEL4_CONFIG_BENCHMARK"))))))

(define-public crate-sel4-sys-0.0.5 (crate (name "sel4-sys") (vers "0.0.5") (hash "0czcl09x6zqxg00bi3b5ik3cdsds48kw58n8c2nmw9p6afdidn6y") (features (quote (("unstable") ("SEL4_DEBUG") ("SEL4_DANGEROUS_CODE_INJECTION") ("SEL4_CONFIG_BENCHMARK"))))))

(define-public crate-sel4-sys-0.0.6 (crate (name "sel4-sys") (vers "0.0.6") (deps (list (crate-dep (name "rlibc") (req "^0.1.4") (default-features #t) (kind 0)))) (hash "1jr9bfi9g6yj5466gz4pxsiii0si6li7gd5prn6fkfmrjgp0r3nj") (features (quote (("unstable") ("SEL4_DEBUG") ("SEL4_DANGEROUS_CODE_INJECTION") ("SEL4_CONFIG_BENCHMARK"))))))

(define-public crate-sel4-sys-0.0.8 (crate (name "sel4-sys") (vers "0.0.8") (deps (list (crate-dep (name "bitflags-core") (req "^0.3.4") (default-features #t) (kind 0)) (crate-dep (name "rlibc") (req "^0.1.4") (default-features #t) (kind 0)))) (hash "15ynxdpch0fkn38g6nfj6sy9139iprbx6ik1vslr3yp3qrkflnm0") (features (quote (("unstable") ("SEL4_DEBUG") ("SEL4_DANGEROUS_CODE_INJECTION") ("SEL4_CONFIG_BENCHMARK"))))))

(define-public crate-sel4-sys-0.0.9 (crate (name "sel4-sys") (vers "0.0.9") (deps (list (crate-dep (name "bitflags-core") (req "^0.3.4") (default-features #t) (kind 0)) (crate-dep (name "rlibc") (req "^0.1.4") (default-features #t) (kind 0)))) (hash "070fh281pkb2bjdbplb8w5940zsp04maqsngsmwysz3zqzv3g129") (features (quote (("unstable") ("SEL4_DEBUG") ("SEL4_DANGEROUS_CODE_INJECTION") ("SEL4_CONFIG_BENCHMARK"))))))

(define-public crate-sel4-sys-0.0.10 (crate (name "sel4-sys") (vers "0.0.10") (deps (list (crate-dep (name "bitflags-core") (req "^0.3.4") (default-features #t) (kind 0)) (crate-dep (name "rlibc") (req "^0.1.4") (default-features #t) (kind 0)))) (hash "1wm823b12z8l85m8r19jdngmcpwrhfaq0hqysrligmf8cx2j505h") (features (quote (("unstable") ("SEL4_DEBUG") ("SEL4_DANGEROUS_CODE_INJECTION") ("SEL4_CONFIG_BENCHMARK"))))))

(define-public crate-sel4-sys-0.0.11 (crate (name "sel4-sys") (vers "0.0.11") (deps (list (crate-dep (name "bitflags-core") (req "^0.3.4") (default-features #t) (kind 0)) (crate-dep (name "rlibc") (req "^0.1.4") (default-features #t) (kind 0)))) (hash "0lvxim415zsqsk3d0r7i7f3sjc8029ln37g24qg60gmjfkbvn3wh") (features (quote (("unstable") ("SEL4_DEBUG") ("SEL4_DANGEROUS_CODE_INJECTION") ("SEL4_CONFIG_BENCHMARK"))))))

(define-public crate-sel4-sys-0.0.12 (crate (name "sel4-sys") (vers "0.0.12") (deps (list (crate-dep (name "bitflags-core") (req "^0.3.4") (default-features #t) (kind 0)) (crate-dep (name "rlibc") (req "^0.1.4") (default-features #t) (kind 0)))) (hash "1fcl4s0m6x4zv6b8qlsnd8aws0s1gyhnh5vmji35qk4n4b3ljrqv") (features (quote (("unstable") ("SEL4_DEBUG") ("SEL4_DANGEROUS_CODE_INJECTION") ("SEL4_CONFIG_BENCHMARK")))) (yanked #t)))

(define-public crate-sel4-sys-0.0.13 (crate (name "sel4-sys") (vers "0.0.13") (deps (list (crate-dep (name "bitflags-core") (req "^0.3.4") (default-features #t) (kind 0)) (crate-dep (name "rlibc") (req "^0.1.4") (default-features #t) (kind 0)))) (hash "0pf25h6hh4926a2rsmq3myqbc49h7j1wfdlcv9h0h7srplnixpw8") (features (quote (("unstable") ("SEL4_DEBUG") ("SEL4_DANGEROUS_CODE_INJECTION") ("SEL4_CONFIG_BENCHMARK"))))))

(define-public crate-sel4-sys-0.0.14 (crate (name "sel4-sys") (vers "0.0.14") (deps (list (crate-dep (name "bitflags-core") (req "^0.3.4") (default-features #t) (kind 0)) (crate-dep (name "rlibc") (req "^0.1.4") (default-features #t) (kind 0)))) (hash "1xvs39a4b81mz42a6n7mhxbm0iv3rzqy1wl0h52qks4zn6q1nnmr") (features (quote (("unstable") ("SEL4_DEBUG") ("SEL4_DANGEROUS_CODE_INJECTION") ("SEL4_CONFIG_BENCHMARK"))))))

(define-public crate-sel4-sys-0.0.15 (crate (name "sel4-sys") (vers "0.0.15") (deps (list (crate-dep (name "bitflags-core") (req "^0.3.4") (default-features #t) (kind 0)) (crate-dep (name "rlibc") (req "^0.1.4") (default-features #t) (kind 0)))) (hash "1xfsq10jlq916dcfvkyv8qlj11zlxn3v4yay2zp42nz153sp81ad") (features (quote (("unstable") ("SEL4_DEBUG") ("SEL4_DANGEROUS_CODE_INJECTION") ("SEL4_CONFIG_BENCHMARK"))))))

(define-public crate-sel4-sys-0.0.16 (crate (name "sel4-sys") (vers "0.0.16") (deps (list (crate-dep (name "bitflags-core") (req "^0.3.4") (default-features #t) (kind 0)) (crate-dep (name "rlibc") (req "^0.1.4") (default-features #t) (kind 0)))) (hash "0qqhfibdamhh1js2glcs7rflr5ilv76xqx753p7riv5chaj3xdmb") (features (quote (("unstable") ("SEL4_DEBUG") ("SEL4_DANGEROUS_CODE_INJECTION") ("SEL4_CONFIG_BENCHMARK"))))))

(define-public crate-sel4-sys-0.0.17 (crate (name "sel4-sys") (vers "0.0.17") (deps (list (crate-dep (name "bitflags-core") (req "^0.3.4") (default-features #t) (kind 0)) (crate-dep (name "rlibc") (req "^0.1.4") (default-features #t) (kind 0)))) (hash "0pmjpjpf3ra90j9is8r9wvh0da7bb848dr2l95yr2gv1jd778ka2") (features (quote (("unstable") ("SEL4_DANGEROUS_CODE_INJECTION") ("SEL4_CONFIG_BENCHMARK"))))))

(define-public crate-sel4-sys-0.0.18 (crate (name "sel4-sys") (vers "0.0.18") (deps (list (crate-dep (name "bitflags-core") (req "^0.3.4") (default-features #t) (kind 0)) (crate-dep (name "rlibc") (req "^0.1.4") (default-features #t) (kind 0)))) (hash "19cff5psijzya61mym00dwshpx5fni48xsrnks66p3c6mra58nhj") (features (quote (("unstable") ("SEL4_DANGEROUS_CODE_INJECTION") ("SEL4_CONFIG_BENCHMARK"))))))

(define-public crate-sel4-sys-0.0.19 (crate (name "sel4-sys") (vers "0.0.19") (deps (list (crate-dep (name "bitflags-core") (req "^0.3.4") (default-features #t) (kind 0)) (crate-dep (name "rlibc") (req "^0.1.4") (default-features #t) (kind 0)))) (hash "1qywgbsga580q34pql7s7shyx5ks7m6ppzsl1vx2i7aaxnx91xm5") (features (quote (("unstable") ("SEL4_DANGEROUS_CODE_INJECTION") ("SEL4_CONFIG_BENCHMARK"))))))

(define-public crate-sel4-sys-0.0.20 (crate (name "sel4-sys") (vers "0.0.20") (deps (list (crate-dep (name "bitflags-core") (req "^0.3.4") (default-features #t) (kind 0)) (crate-dep (name "rlibc") (req "^0.1.4") (default-features #t) (kind 0)))) (hash "023l8fs8jmja0hmhw8k0kjkgz43fbbwhs53kh59rn3jmpas1wr19") (features (quote (("unstable") ("SEL4_DANGEROUS_CODE_INJECTION") ("SEL4_CONFIG_BENCHMARK"))))))

(define-public crate-sel4-sys-0.0.21 (crate (name "sel4-sys") (vers "0.0.21") (deps (list (crate-dep (name "bitflags-core") (req "^0.3.4") (default-features #t) (kind 0)) (crate-dep (name "rlibc") (req "^0.1.4") (default-features #t) (kind 0)))) (hash "174aj6qvgkrj6kar2ynffdp8nvyd0rjnpiidazsfy1fv8hfnk2d5") (features (quote (("unstable") ("SEL4_DANGEROUS_CODE_INJECTION") ("SEL4_CONFIG_BENCHMARK")))) (yanked #t)))

(define-public crate-sel4-sys-0.0.22 (crate (name "sel4-sys") (vers "0.0.22") (deps (list (crate-dep (name "bitflags") (req "^0.7.0") (default-features #t) (kind 0)) (crate-dep (name "rlibc") (req "^1.0.0") (default-features #t) (kind 0)))) (hash "1g5i2w470xjf0ynn6hcqygbqbpw3vjdqsbg48sh9mma95y293mj6") (features (quote (("unstable") ("SEL4_DANGEROUS_CODE_INJECTION") ("SEL4_CONFIG_BENCHMARK"))))))

(define-public crate-sel4-sys-0.0.23 (crate (name "sel4-sys") (vers "0.0.23") (deps (list (crate-dep (name "bitflags") (req "^0.7.0") (default-features #t) (kind 0)) (crate-dep (name "rlibc") (req "^1.0.0") (default-features #t) (kind 0)))) (hash "1230cvskzyivwy2kdp7pr1hvc2ba4b6dwmhsdasxgsmcbxiq3i8x") (features (quote (("unstable") ("SEL4_DANGEROUS_CODE_INJECTION") ("SEL4_CONFIG_BENCHMARK") ("CONFIG_VTX") ("CONFIG_HARDWARE_DEBUG_API") ("CONFIG_ARM_SMMU") ("CONFIG_ARM_HYPERVISOR_SUPPORT"))))))

(define-public crate-sel4-sys-0.0.24 (crate (name "sel4-sys") (vers "0.0.24") (deps (list (crate-dep (name "bitflags") (req "^0.7.0") (default-features #t) (kind 0)) (crate-dep (name "rlibc") (req "^1.0.0") (default-features #t) (kind 0)))) (hash "0qmik8vq1clraczl0svhwg0ql53aiv9qkpmm9qd7ak7j2bl3bbfz") (features (quote (("unstable") ("SEL4_DANGEROUS_CODE_INJECTION") ("SEL4_CONFIG_BENCHMARK") ("CONFIG_VTX") ("CONFIG_HARDWARE_DEBUG_API") ("CONFIG_ARM_SMMU") ("CONFIG_ARM_HYPERVISOR_SUPPORT"))))))

(define-public crate-sel4-sys-0.0.25 (crate (name "sel4-sys") (vers "0.0.25") (deps (list (crate-dep (name "bitflags") (req "^0.7.0") (default-features #t) (kind 0)) (crate-dep (name "rlibc") (req "^1.0.0") (default-features #t) (kind 0)))) (hash "0cyks3f9pbzd23na5v5k5wz35b3g1kcknihkpibn5zsjn99i4nm8") (features (quote (("unstable") ("SEL4_DANGEROUS_CODE_INJECTION") ("SEL4_CONFIG_BENCHMARK") ("CONFIG_VTX") ("CONFIG_HARDWARE_DEBUG_API") ("CONFIG_ARM_SMMU") ("CONFIG_ARM_HYPERVISOR_SUPPORT"))))))

(define-public crate-sel4-sys-0.0.26 (crate (name "sel4-sys") (vers "0.0.26") (deps (list (crate-dep (name "bitflags") (req "^0.7.0") (default-features #t) (kind 0)) (crate-dep (name "rlibc") (req "^1.0.0") (default-features #t) (kind 0)))) (hash "129byf6x0zrxsy1apf67mg434w00h9wnp5ilsczgrxz6r9iyg4fj") (features (quote (("unstable") ("SEL4_DANGEROUS_CODE_INJECTION") ("SEL4_CONFIG_BENCHMARK") ("CONFIG_VTX") ("CONFIG_HARDWARE_DEBUG_API") ("CONFIG_ARM_SMMU") ("CONFIG_ARM_HYPERVISOR_SUPPORT"))))))

(define-public crate-sel4-sys-0.0.27 (crate (name "sel4-sys") (vers "0.0.27") (deps (list (crate-dep (name "bitflags") (req "^0.7.0") (default-features #t) (kind 0)) (crate-dep (name "rlibc") (req "^1.0.0") (default-features #t) (kind 0)))) (hash "06yl3gvkg3vwmj2fcrmygm9kqmqlz2wbhv61f8s7q2n3bv9x3qdg") (features (quote (("unstable") ("SEL4_DANGEROUS_CODE_INJECTION") ("SEL4_CONFIG_BENCHMARK") ("CONFIG_VTX") ("CONFIG_HARDWARE_DEBUG_API") ("CONFIG_ARM_SMMU") ("CONFIG_ARM_HYPERVISOR_SUPPORT"))))))

(define-public crate-sel4-sys-0.0.28 (crate (name "sel4-sys") (vers "0.0.28") (deps (list (crate-dep (name "bitflags") (req "^0.7.0") (default-features #t) (kind 0)) (crate-dep (name "rlibc") (req "^1.0.0") (default-features #t) (kind 0)))) (hash "08j80gv72ys526k947na9slw66jlhv6bxamyay74df2f9jpph9dl") (features (quote (("unstable") ("SEL4_DANGEROUS_CODE_INJECTION") ("SEL4_CONFIG_BENCHMARK") ("CONFIG_VTX") ("CONFIG_HARDWARE_DEBUG_API") ("CONFIG_ARM_SMMU") ("CONFIG_ARM_HYPERVISOR_SUPPORT"))))))

