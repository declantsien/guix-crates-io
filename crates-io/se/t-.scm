(define-module (crates-io se t-) #:use-module (crates-io))

(define-public crate-set-cargo-version-1 (crate (name "set-cargo-version") (vers "1.0.0") (deps (list (crate-dep (name "toml_edit") (req "^0.2") (default-features #t) (kind 0)))) (hash "15j05r7fn44lqmjv1r71kn92zfn31hlzvd6mpal6lsww3dm7wc8x")))

(define-public crate-set-cargo-version-1 (crate (name "set-cargo-version") (vers "1.2.0") (deps (list (crate-dep (name "toml_edit") (req "^0.13") (default-features #t) (kind 0)))) (hash "1i9l0jw4cyf0qfb16zrij1qxlw9y10v8vv78izzhl3ipx6j1880i")))

(define-public crate-set-cargo-version-1 (crate (name "set-cargo-version") (vers "1.2.1") (deps (list (crate-dep (name "toml_edit") (req "^0.14") (default-features #t) (kind 0)))) (hash "1bl9d2qxmdsxmwc4asmq6rs62nprjqfi42k6ygbcx28i0nc9c6i3")))

(define-public crate-set-config-toml-version-0.0.1 (crate (name "set-config-toml-version") (vers "0.0.1") (deps (list (crate-dep (name "toml_edit") (req "^0.1.5") (default-features #t) (kind 0)))) (hash "1rwddxz0kphx3ddsk8zj6gxj2sw3czh7066v3kvs1f4k2wa1d5yr")))

(define-public crate-set-encoding-0.1 (crate (name "set-encoding") (vers "0.1.0") (deps (list (crate-dep (name "bitrw") (req "^0.7.0") (default-features #t) (kind 0)) (crate-dep (name "int") (req "^0.2.11") (default-features #t) (kind 0)) (crate-dep (name "tbe") (req "^0.2.0") (default-features #t) (kind 0)))) (hash "05vdkdrczjj9iqhq3igp9gxg8hbjzrc275gi129gg1a4fl0vbjca")))

(define-public crate-set-encoding-0.2 (crate (name "set-encoding") (vers "0.2.0") (deps (list (crate-dep (name "bitrw") (req "^0.8.3") (default-features #t) (kind 0)) (crate-dep (name "int") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2.6") (default-features #t) (kind 0)) (crate-dep (name "tbe") (req "^0.3.3") (default-features #t) (kind 0)))) (hash "00f5kb22cx41c06xwb1mi6jsaa7sn6ligjngp24bds252swdplc1")))

(define-public crate-set-error-0.0.1 (crate (name "set-error") (vers "0.0.1") (hash "1xbv2l3frwg9000r5709d948jy0lhw6lm19908dk7654k26sfyrp")))

(define-public crate-set-error-0.0.2 (crate (name "set-error") (vers "0.0.2") (hash "1ik7yvr8xshw194qmwzj5z1039c1091p5gvd0l9b7s5c6jqxbcjv")))

(define-public crate-set-error-0.0.3 (crate (name "set-error") (vers "0.0.3") (hash "0dllhc3qgs83ax2fmz9kzsg1n7nqd129lilrmpyg0fdfnv4yy76b")))

(define-public crate-set-error-1 (crate (name "set-error") (vers "1.0.0") (hash "1k806kzq8609cl5n5l76wa7c0f1hqhdfxi0pix9wv9rmw8jyra5c")))

(define-public crate-set-error-1 (crate (name "set-error") (vers "1.0.1") (hash "0vj7hm8nz8hq30r8a7dccghbk8n6rz2389kl7whqax8l7zb6agsa")))

(define-public crate-set-partitions-1 (crate (name "set-partitions") (vers "1.0.0") (deps (list (crate-dep (name "arrayvec") (req "^0.7.2") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2.14") (default-features #t) (kind 0)))) (hash "1gvvqrs4hay587san6haq6clmf8av4s0fzil6p6y2q0iixm7xlsp") (yanked #t)))

(define-public crate-set-partitions-1 (crate (name "set-partitions") (vers "1.0.1") (deps (list (crate-dep (name "arrayvec") (req "^0.7.2") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2.14") (default-features #t) (kind 0)))) (hash "15lha3xmv1b0nakdpvhf5mcsbl8y88q19brx056qsf587hhllib1")))

(define-public crate-set-trie-0.1 (crate (name "set-trie") (vers "0.1.0") (hash "1mlpalr9b8ah480a9y97hn4m9am8bibmpw4ph220n7wr9l0ipb32")))

(define-public crate-set-trie-0.2 (crate (name "set-trie") (vers "0.2.0") (deps (list (crate-dep (name "proptest") (req "^0.10") (default-features #t) (kind 2)) (crate-dep (name "proptest-derive") (req "^0.1.0") (default-features #t) (kind 2)) (crate-dep (name "skeptic") (req "^0.13") (default-features #t) (kind 1)) (crate-dep (name "skeptic") (req "^0.13") (default-features #t) (kind 2)))) (hash "0mnlxf3k2hcczndd5a9acml0zmqacw5fafgkfwpdi61k2jfpcrx7")))

(define-public crate-set-trie-0.2 (crate (name "set-trie") (vers "0.2.1") (deps (list (crate-dep (name "proptest") (req "^0.10") (default-features #t) (kind 2)) (crate-dep (name "proptest-derive") (req "^0.1.0") (default-features #t) (kind 2)) (crate-dep (name "skeptic") (req "^0.13") (default-features #t) (kind 1)) (crate-dep (name "skeptic") (req "^0.13") (default-features #t) (kind 2)))) (hash "08ir8ddvgpycq58nxfa1pnwl2f1l4vdw8sifgybb6n3ipni6jdrb")))

(define-public crate-set-trie-0.2 (crate (name "set-trie") (vers "0.2.2") (deps (list (crate-dep (name "proptest") (req "^0.10") (default-features #t) (kind 2)) (crate-dep (name "proptest-derive") (req "^0.1.0") (default-features #t) (kind 2)) (crate-dep (name "skeptic") (req "^0.13") (default-features #t) (kind 1)) (crate-dep (name "skeptic") (req "^0.13") (default-features #t) (kind 2)))) (hash "1xsff8h2jrw7bv3zdyvd85svrv5iw16cxbsp0xg9q6qk4anbgbb3")))

(define-public crate-set-trie-0.2 (crate (name "set-trie") (vers "0.2.3") (deps (list (crate-dep (name "criterion") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "proptest") (req "^0.10") (default-features #t) (kind 2)) (crate-dep (name "proptest-derive") (req "^0.1.0") (default-features #t) (kind 2)) (crate-dep (name "skeptic") (req "^0.13") (default-features #t) (kind 1)) (crate-dep (name "skeptic") (req "^0.13") (default-features #t) (kind 2)))) (hash "01smzrq8bgbf23nys31jcq5rhc5gy111jrnhnlcciaijrayz6y8z")))

(define-public crate-set-user-tasks-0.1 (crate (name "set-user-tasks") (vers "0.1.0") (deps (list (crate-dep (name "windows") (req "^0.39.0") (features (quote ("Win32_Foundation" "Win32_System_SystemServices" "Win32_System_Com" "Win32_UI_Shell" "Win32_UI_Shell_Common" "Win32_UI_Shell_PropertiesSystem" "Win32_System_Com_StructuredStorage" "Win32_Storage_EnhancedStorage"))) (default-features #t) (kind 0)))) (hash "1v67d3x1aiiiqzbpml5am4a02pkawj0592bf1h689asvbjii29bk")))

(define-public crate-set-user-tasks-0.1 (crate (name "set-user-tasks") (vers "0.1.1") (deps (list (crate-dep (name "windows") (req ">=0.36.1") (features (quote ("Win32_Foundation" "Win32_System_SystemServices" "Win32_System_Com" "Win32_UI_Shell" "Win32_UI_Shell_Common" "Win32_UI_Shell_PropertiesSystem" "Win32_System_Com_StructuredStorage" "Win32_Storage_EnhancedStorage"))) (default-features #t) (kind 0)))) (hash "0lxhhrmsryzbwd8gni916y86s9wbjl5wncbn8wjpcnl790lwj3vi")))

(define-public crate-set-user-tasks-0.36 (crate (name "set-user-tasks") (vers "0.36.1") (deps (list (crate-dep (name "windows") (req "^0.36.1") (features (quote ("Win32_Foundation" "Win32_System_SystemServices" "Win32_System_Com" "Win32_UI_Shell" "Win32_UI_Shell_Common" "Win32_UI_Shell_PropertiesSystem" "Win32_System_Com_StructuredStorage" "Win32_Storage_EnhancedStorage"))) (default-features #t) (kind 0)))) (hash "0ld8xmwsnnangylmni9kml3krghs2ywzbv8arxr7r3r5gna6mjv3")))

(define-public crate-set-user-tasks-0.34 (crate (name "set-user-tasks") (vers "0.34.0") (deps (list (crate-dep (name "windows") (req "^0.34.0") (features (quote ("Win32_Foundation" "Win32_System_SystemServices" "Win32_System_Com" "Win32_UI_Shell" "Win32_UI_Shell_Common" "Win32_UI_Shell_PropertiesSystem" "Win32_System_Com_StructuredStorage" "Win32_Storage_EnhancedStorage"))) (default-features #t) (kind 0)))) (hash "0d9lv84spg862nkijy4bjhkqap40wv9bkhrk6s6mii646lmfkak3")))

(define-public crate-set-user-tasks-0.48 (crate (name "set-user-tasks") (vers "0.48.0") (deps (list (crate-dep (name "windows") (req "^0.48") (features (quote ("Win32_Foundation" "Win32_System_SystemServices" "Win32_System_Com" "Win32_UI_Shell" "Win32_UI_Shell_Common" "Win32_UI_Shell_PropertiesSystem" "Win32_System_Com_StructuredStorage" "Win32_Storage_EnhancedStorage"))) (default-features #t) (kind 0)))) (hash "1zajw7aais9vhh7zwzp3llb91qbx45y2vjnh6ssw9z1wiyckxzvb")))

(define-public crate-set-with-grab-1 (crate (name "set-with-grab") (vers "1.0.0") (deps (list (crate-dep (name "set-with-grab-macro") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "bevy") (req "^0.9") (default-features #t) (kind 2)) (crate-dep (name "rstest") (req "^0.16") (default-features #t) (kind 2)))) (hash "00jr13jpnkdxgsj20s05dsc2klxfi2aqy0z65dgqf8r20w49b1jy")))

(define-public crate-set-with-grab-macro-1 (crate (name "set-with-grab-macro") (vers "1.0.0") (deps (list (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "to-syn-error") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "assert-parse") (req "^1.0") (default-features #t) (kind 2)) (crate-dep (name "rstest") (req "^0.16") (default-features #t) (kind 2)))) (hash "1v96waxhgflr2jwhy2b9qfd3i9gj5z3v7w794axvfmql6li4rncc")))

