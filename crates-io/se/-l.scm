(define-module (crates-io se -l) #:use-module (crates-io))

(define-public crate-se-logger-0.1 (crate (name "se-logger") (vers "0.1.0") (deps (list (crate-dep (name "chrono") (req "^0.4.22") (default-features #t) (kind 0)))) (hash "004l6yah5pl9z85c3qf7n1lsyzrdqd39m72qb707r59w8kw345yd")))

(define-public crate-se-logger-0.1 (crate (name "se-logger") (vers "0.1.1") (deps (list (crate-dep (name "chrono") (req "^0.4.22") (default-features #t) (kind 0)))) (hash "1zqwmcgzyag0ch20dmw4isg1rqgk00spd8ypl21df2wwz7xx4xrd")))

(define-public crate-se-logger-0.1 (crate (name "se-logger") (vers "0.1.2") (deps (list (crate-dep (name "chrono") (req "^0.4.22") (default-features #t) (kind 0)))) (hash "1zh55dgxvjcqzm33sw1qh993kh37npqc3ngg2qgda7zzfx7pm5li")))

