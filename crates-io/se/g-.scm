(define-module (crates-io se g-) #:use-module (crates-io))

(define-public crate-seg-tree-0.1 (crate (name "seg-tree") (vers "0.1.0") (hash "1bhvf3cfnsmb8vwragz875zdaw8isbp9x59i3bibazsr1758ilqv")))

(define-public crate-seg-tree-0.1 (crate (name "seg-tree") (vers "0.1.1") (hash "0r7babgj82d3g7cmv0mw644m9jv0w8agazqaqp1mfkdzg2rb44hx")))

(define-public crate-seg-tree-0.1 (crate (name "seg-tree") (vers "0.1.2") (hash "0x822nal8z8zpsgsa0976z6hhpkp3x1imnqb683inrbkav90hj4b")))

(define-public crate-seg-tree-0.1 (crate (name "seg-tree") (vers "0.1.3") (hash "0d4kl8qpah9m9l51mpsv2c0xrgvfp7l9xwbpmx1cvp7xs3v42bhk")))

(define-public crate-seg-tree-0.2 (crate (name "seg-tree") (vers "0.2.0") (deps (list (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 2)))) (hash "0jkny00b62zz2nddxmvm343s58i86lns31ny3mmgb8hagz4484qv")))

(define-public crate-seg-tree-0.2 (crate (name "seg-tree") (vers "0.2.1") (deps (list (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 2)))) (hash "0c866fqv60iwysnp30swzzg2wsxx5v8m1m8pp5xgc7bdnbb0kp2q")))

(define-public crate-seg-tree-0.2 (crate (name "seg-tree") (vers "0.2.2") (deps (list (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 2)))) (hash "0mwk5m9mnbnjm23hm53rfn0nmbkl6j7n7smhcc7whfj7685xbyvj") (yanked #t)))

(define-public crate-seg-tree-0.3 (crate (name "seg-tree") (vers "0.3.0") (deps (list (crate-dep (name "criterion") (req "^0.3.6") (features (quote ("html_reports"))) (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 2)))) (hash "16n4ac7nqqsliy3rs6g2yp8zvr9fkfz473q2haw46x8bqjyrflxk")))

