(define-module (crates-io se jo) #:use-module (crates-io))

(define-public crate-sejong-0.1 (crate (name "sejong") (vers "0.1.0") (deps (list (crate-dep (name "lazy_static") (req "^1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "wasm-bindgen") (req "^0.2") (optional #t) (default-features #t) (kind 0)))) (hash "0lv7v1za9xa3nzafr118g2720dzfdyl05z8lls2bx11q9gj4rcc8") (features (quote (("wasm" "wasm-bindgen" "lazy_static"))))))

(define-public crate-sejong-0.1 (crate (name "sejong") (vers "0.1.1") (deps (list (crate-dep (name "lazy_static") (req "^1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "wasm-bindgen") (req "^0.2") (optional #t) (default-features #t) (kind 0)))) (hash "1c5mig1yq6rhd6vfz18s7d6rmaif0r68yirikk6vvkdzvqsfwrjr") (features (quote (("wasm" "wasm-bindgen" "lazy_static"))))))

(define-public crate-sejong-0.1 (crate (name "sejong") (vers "0.1.2") (deps (list (crate-dep (name "lazy_static") (req "^1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "wasm-bindgen") (req "^0.2") (optional #t) (default-features #t) (kind 0)))) (hash "0vaga104by14b4gcv6xrra68b1fxddlfy371jd7rcn9lpnffya58") (features (quote (("wasm" "wasm-bindgen" "lazy_static"))))))

(define-public crate-sejong-0.1 (crate (name "sejong") (vers "0.1.3") (deps (list (crate-dep (name "lazy_static") (req "^1.4") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "wasm-bindgen") (req "^0.2") (optional #t) (default-features #t) (kind 0)))) (hash "1sqnh3vbkgp2y8frbj4d908h1pz0gpmv84nxll06phgg1qjjklil") (features (quote (("wasm" "wasm-bindgen" "lazy_static"))))))

(define-public crate-sejong-0.1 (crate (name "sejong") (vers "0.1.4") (deps (list (crate-dep (name "lazy_static") (req "^1.4") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "wasm-bindgen") (req "^0.2") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "wee_alloc") (req "^0.4") (optional #t) (default-features #t) (kind 0)))) (hash "0f3l7lds81xmdzyq7123q53c9icplfrrk2apv63fva4mki7ghs80") (features (quote (("wasm" "wasm-bindgen" "lazy_static" "wee_alloc"))))))

(define-public crate-sejong-0.1 (crate (name "sejong") (vers "0.1.5") (deps (list (crate-dep (name "lazy_static") (req "^1.4") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "wasm-bindgen") (req "^0.2") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "wee_alloc") (req "^0.4") (optional #t) (default-features #t) (kind 0)))) (hash "1ddljvq91qi1m50sc9ba0ys5qypql3hxxnzpavd9xiwl6322aqrr") (features (quote (("wasm" "wasm-bindgen" "lazy_static" "wee_alloc"))))))

