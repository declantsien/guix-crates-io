(define-module (crates-io se ts) #:use-module (crates-io))

(define-public crate-sets-0.1 (crate (name "sets") (vers "0.1.0") (deps (list (crate-dep (name "indxvec") (req "^0.1") (default-features #t) (kind 0)))) (hash "1z053nd883pi59mn25n1c9bp8ld83wrajlcqs4blfs9yw9rv6mkb")))

(define-public crate-sets-0.1 (crate (name "sets") (vers "0.1.1") (deps (list (crate-dep (name "indxvec") (req "^0.1") (default-features #t) (kind 0)))) (hash "1n27phxfyv7qa2k3zfh5n55mw6s9m9m3975rmn04gg7cq0qb4ny6")))

(define-public crate-sets-0.1 (crate (name "sets") (vers "0.1.2") (deps (list (crate-dep (name "indxvec") (req "^0.2") (default-features #t) (kind 0)))) (hash "1khhgk9bjx7m2qkq2mp1kzai414zwz6rgb2896ka06pzicx5gcbk")))

(define-public crate-sets-0.1 (crate (name "sets") (vers "0.1.3") (deps (list (crate-dep (name "indxvec") (req "^0.2") (default-features #t) (kind 0)))) (hash "07gjsq1v9z1a7404srx6iyxd0wbilhv6hqpv9nmybfnxm0f8cg8l")))

(define-public crate-sets-0.1 (crate (name "sets") (vers "0.1.4") (deps (list (crate-dep (name "indxvec") (req "^0.2") (default-features #t) (kind 0)))) (hash "0hn3b36jp9b23kjvnb2czhybfnllxic6d1plasb7i2yfaq40hsws")))

(define-public crate-sets-0.1 (crate (name "sets") (vers "0.1.5") (deps (list (crate-dep (name "indxvec") (req "^0.2") (default-features #t) (kind 0)))) (hash "1linxi9zmpkd822mrm7rqchy2lygx6f7hg5wpg7lvjrf3w850rbc")))

(define-public crate-sets-0.1 (crate (name "sets") (vers "0.1.6") (deps (list (crate-dep (name "indxvec") (req "^0.2") (default-features #t) (kind 0)))) (hash "0kqhz3rxxgjal5lck3wpxip87xlq69qzwz36mklyixmrlbqrijyb")))

(define-public crate-sets-0.1 (crate (name "sets") (vers "0.1.7") (deps (list (crate-dep (name "indxvec") (req "^0.2") (default-features #t) (kind 0)))) (hash "064hm5cpc10h5pwhikl09ybhwgvd9ili50jk33hayqxayfsrn7ga")))

(define-public crate-sets-0.1 (crate (name "sets") (vers "0.1.8") (deps (list (crate-dep (name "indxvec") (req "^0.2") (default-features #t) (kind 0)))) (hash "1m7jcxnhrypzd575yzycvirmp43hzhlnlxr5b2ai3k2wx9mn6ali")))

(define-public crate-sets-1 (crate (name "sets") (vers "1.0.0") (deps (list (crate-dep (name "indxvec") (req "^1") (default-features #t) (kind 0)))) (hash "1jrmd75kcf3d7lgf2kqcv8h2psg4gw79dz5him1xybw0sgnbqncx")))

(define-public crate-sets-1 (crate (name "sets") (vers "1.0.1") (deps (list (crate-dep (name "indxvec") (req "^1") (default-features #t) (kind 0)))) (hash "10dgxpzgy00xqga0whv7ybb8pbj5raviwdrk0mrqii29s70ns350")))

(define-public crate-sets-1 (crate (name "sets") (vers "1.0.2") (deps (list (crate-dep (name "indxvec") (req "^1") (default-features #t) (kind 0)))) (hash "0916frxs95whp4nqza55l59dim9xalxzicplspmikimds92z175y")))

(define-public crate-sets-1 (crate (name "sets") (vers "1.0.3") (deps (list (crate-dep (name "indxvec") (req "^1") (default-features #t) (kind 0)))) (hash "0zr1hk7indg76zkyidkc1kc7yqdyib0r0q79ya9mbmpiq220d6fm")))

(define-public crate-sets-1 (crate (name "sets") (vers "1.0.4") (deps (list (crate-dep (name "indxvec") (req "^1") (default-features #t) (kind 0)))) (hash "1g1pqh7bynq5k23dqsabm3zii7zldjx436bqi9lsq6fld6bzyjpv")))

(define-public crate-sets-1 (crate (name "sets") (vers "1.0.5") (deps (list (crate-dep (name "indxvec") (req "^1") (default-features #t) (kind 0)))) (hash "0h80n4f60gh1pmngkyy7y25h4b8halyry6h55ps31z650s00dl06")))

(define-public crate-sets-1 (crate (name "sets") (vers "1.0.6") (deps (list (crate-dep (name "indxvec") (req "^1") (default-features #t) (kind 0)))) (hash "00kz2cnipl29j3dc79rhgd9x4kf09154kladwrby4hchq4s49ai5")))

(define-public crate-sets-1 (crate (name "sets") (vers "1.1.0") (deps (list (crate-dep (name "indxvec") (req "^1") (default-features #t) (kind 0)))) (hash "0xr5n49kny8z5mf09mw5nkpxmcfs2d3vbckqxnryjyw76md4fbz0")))

(define-public crate-sets-1 (crate (name "sets") (vers "1.1.1") (deps (list (crate-dep (name "indxvec") (req "^1") (default-features #t) (kind 0)))) (hash "1d5wiijmpkixy60n44dshpabhzy6whj8m61bp86w5gbsfmgw72sh")))

(define-public crate-sets-1 (crate (name "sets") (vers "1.1.2") (deps (list (crate-dep (name "indxvec") (req "^1") (default-features #t) (kind 0)))) (hash "07j40r60g3wc558nbw3sw9phsn81xg911nhxzdhlafq2qy3y0r9k")))

(define-public crate-sets-1 (crate (name "sets") (vers "1.1.3") (deps (list (crate-dep (name "indxvec") (req "^1.4.2") (default-features #t) (kind 0)))) (hash "02mi9h7kmf94a5q4395vh541fkz8wxqzzh9v9c7j89qdkqqwy117")))

(define-public crate-sets-1 (crate (name "sets") (vers "1.1.4") (deps (list (crate-dep (name "indxvec") (req "^1.4") (default-features #t) (kind 0)))) (hash "114475yjgizl2xxr1l82934vvwsn2m31ma550ld1li5lq0q28n7j")))

(define-public crate-sets-1 (crate (name "sets") (vers "1.2.0") (deps (list (crate-dep (name "indxvec") (req "^1.4") (default-features #t) (kind 0)))) (hash "1mab4g378cddyfhdzka9n1qvpa4g3lcblk25d569as3hm230ggjd")))

(define-public crate-sets-1 (crate (name "sets") (vers "1.2.1") (deps (list (crate-dep (name "indxvec") (req "^1.8") (default-features #t) (kind 0)))) (hash "0p4cdkc47hp09nln139pw396z5lhh0dkqi6mxz8q1igk495zjv7s")))

(define-public crate-sets_multisets-0.0.1 (crate (name "sets_multisets") (vers "0.0.1") (deps (list (crate-dep (name "bytevec") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "fasthash") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8") (default-features #t) (kind 0)))) (hash "0jbmainyxbnfb6zjxasj9ycva6bsqfgzwbyjhy4m6aakbn71mb8y")))

(define-public crate-sets_multisets-0.1 (crate (name "sets_multisets") (vers "0.1.0") (deps (list (crate-dep (name "bytevec") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "fasthash") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8") (default-features #t) (kind 0)))) (hash "0ks23fhqhdxdsp0sabqn0vpk34rhf5wy6bs0svgmib45lnivxgi5")))

(define-public crate-sets_multisets-0.2 (crate (name "sets_multisets") (vers "0.2.0") (deps (list (crate-dep (name "bytevec") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "fasthash") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8") (default-features #t) (kind 0)))) (hash "1m4fp25n6ipxbb6sv53y6wsi0md2827bc6xi3x0fjxfb1svh6aa6")))

(define-public crate-sets_multisets-0.2 (crate (name "sets_multisets") (vers "0.2.1") (deps (list (crate-dep (name "bytevec") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "fasthash") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8") (default-features #t) (kind 0)))) (hash "0qxqr2rv56vs79rpml1nj44g4f2yj4yw2m3hx2ywl77mvyz41anm")))

(define-public crate-sets_multisets-0.3 (crate (name "sets_multisets") (vers "0.3.1") (deps (list (crate-dep (name "bytevec") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "fasthash") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8") (default-features #t) (kind 0)))) (hash "05ddn8p1l3j7jh275qgrnybb4s0vvx96j5n6yz051gvrcd8ibbmq")))

(define-public crate-sets_multisets-0.3 (crate (name "sets_multisets") (vers "0.3.2") (deps (list (crate-dep (name "bytevec") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "fasthash") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8") (default-features #t) (kind 0)))) (hash "15sawwf05sk27425qly6dkdm7bf3w5w69nl92njys5zbrw7v458f")))

(define-public crate-sets_multisets-0.4 (crate (name "sets_multisets") (vers "0.4.0") (deps (list (crate-dep (name "bytevec") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "fasthash") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8") (default-features #t) (kind 0)))) (hash "0p44a5zx8kaapi20zwq247c33j8p5q4h1ym56hyz0cynlfl4rl7d")))

(define-public crate-sets_multisets-0.4 (crate (name "sets_multisets") (vers "0.4.1") (deps (list (crate-dep (name "bytevec") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "fasthash") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8") (default-features #t) (kind 0)))) (hash "0xd85ybyblab4p5ac45fzqf26jyqxq74xvps8fjmfcp86gdl7jns")))

(define-public crate-sets_multisets-0.4 (crate (name "sets_multisets") (vers "0.4.2") (deps (list (crate-dep (name "bytevec") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "fasthash") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8") (default-features #t) (kind 0)))) (hash "0zfqj3k69fq1rwia5xn22ihqny8i32gba7cmnwlzh6mr9wxa5f1a")))

(define-public crate-sets_multisets-0.5 (crate (name "sets_multisets") (vers "0.5.0") (deps (list (crate-dep (name "bytevec") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "xxh3") (req "^0.1") (default-features #t) (kind 0)))) (hash "16srag2xk9hbpcgfvk1i24lq7ry86gr3zjkqblgw7y01gg83g4wk") (yanked #t)))

(define-public crate-sets_multisets-0.6 (crate (name "sets_multisets") (vers "0.6.0") (deps (list (crate-dep (name "bytevec") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "xxh3") (req "^0.1") (default-features #t) (kind 0)))) (hash "0xhjqyffl1hmcqjwnv6g3hhdhl827ha4jqkx9w3jcjwvyw4d47dn")))

(define-public crate-sets_multisets-0.7 (crate (name "sets_multisets") (vers "0.7.0") (deps (list (crate-dep (name "bytevec") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "sha3") (req "^0.10") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "xxh3") (req "^0.1") (optional #t) (default-features #t) (kind 0)))) (hash "06krasq7aylnd3zn26pmqiqyh01r5nmf7afhvnyvzwmgs6kcknbw") (features (quote (("default" "xxh3")))) (v 2) (features2 (quote (("xxh3" "dep:xxh3") ("shake128" "dep:sha3"))))))

(define-public crate-sets_multisets-0.7 (crate (name "sets_multisets") (vers "0.7.1") (deps (list (crate-dep (name "blake3") (req "^1.3") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "bytevec") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "sha3") (req "^0.10") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "xxh3") (req "^0.1") (optional #t) (default-features #t) (kind 0)))) (hash "1ywbb31mjc5wg5yz035hj7cmwg7z20c6wl68z9pr2dccx969r2i4") (features (quote (("default" "xxh3")))) (v 2) (features2 (quote (("xxh3" "dep:xxh3") ("shake128" "dep:sha3") ("blake3" "dep:blake3"))))))

(define-public crate-sets_multisets-0.7 (crate (name "sets_multisets") (vers "0.7.2") (deps (list (crate-dep (name "blake3") (req "^1.3") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "bytevec") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "sha3") (req "^0.10") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "xxh3") (req "^0.1") (optional #t) (default-features #t) (kind 0)))) (hash "1f43z9g8rkginyxsd2gqfhwnph6hys9g88csix82siq174qjj4in") (features (quote (("default" "xxh3")))) (v 2) (features2 (quote (("xxh3" "dep:xxh3") ("shake128" "dep:sha3") ("blake3" "dep:blake3"))))))

(define-public crate-sets_multisets-0.8 (crate (name "sets_multisets") (vers "0.8.0") (deps (list (crate-dep (name "blake3") (req "^1.3") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "bytevec") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "sha3") (req "^0.10") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "xxh3") (req "^0.1") (optional #t) (default-features #t) (kind 0)))) (hash "1yhnhzkpmw7dcdrrp0xdzzcn64vy2a2szgnjq9zmm1nrlskvq3j9") (features (quote (("default" "xxh3")))) (v 2) (features2 (quote (("xxh3" "dep:xxh3") ("shake128" "dep:sha3") ("blake3" "dep:blake3"))))))

(define-public crate-setsum-0.1 (crate (name "setsum") (vers "0.1.0") (deps (list (crate-dep (name "sha2") (req "^0.9") (default-features #t) (kind 0)))) (hash "18nhjc41c7mp4v37wnlffxnnjzm3493lxzyszx8n8crf75f81b4r")))

(define-public crate-setsum-0.2 (crate (name "setsum") (vers "0.2.0") (deps (list (crate-dep (name "sha2") (req "^0.9") (default-features #t) (kind 0)))) (hash "1i39ynblz4jbq91yyii0dxdg1hj10w2hzbdnd3xn626531bpxwhz")))

(define-public crate-setsum-0.3 (crate (name "setsum") (vers "0.3.0") (deps (list (crate-dep (name "sha2") (req "^0.10") (default-features #t) (kind 0)))) (hash "17fdqylw8wm96zd1yc1kmwfxcyk9q96dvrmkjb7zx1jws64g33kv")))

(define-public crate-setsum-0.4 (crate (name "setsum") (vers "0.4.0") (deps (list (crate-dep (name "sha2") (req "^0.10") (default-features #t) (kind 0)))) (hash "0ml6qllsa917zbivzc1abxa3xmg0dgs3iffzk7bnvzcic9j3978c")))

(define-public crate-setsum-0.5 (crate (name "setsum") (vers "0.5.0") (deps (list (crate-dep (name "sha3") (req "^0.10") (default-features #t) (kind 0)))) (hash "0b8snrz761ar6i53prasfwplcwrmxab763cq6278ppfjy7lvsn14") (features (quote (("default" "binaries") ("binaries"))))))

