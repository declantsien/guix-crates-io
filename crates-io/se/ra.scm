(define-module (crates-io se ra) #:use-module (crates-io))

(define-public crate-sera-0.0.0 (crate (name "sera") (vers "0.0.0") (hash "0lp4j92y2hh7vbl0c5ahx52gipkdv9546qqvpcdl5fb9f2sb3nx7")))

(define-public crate-seraf-0.0.0 (crate (name "seraf") (vers "0.0.0") (hash "0h577mmvgk5bpxgnfd2gbq2bvs2hzhy3mqsrakbnwim5vbsp63k9")))

(define-public crate-serai-0.0.1 (crate (name "serai") (vers "0.0.1") (hash "02d47kb9kkgd56d7v70vf4wz31va6n7xncn61gx7dbb7imj6k927")))

(define-public crate-serai-abi-0.1 (crate (name "serai-abi") (vers "0.1.0") (hash "1amvyf433v14hwagn6k4kmd7xaibp2k1lxvgn3sllbq67jnmdnws") (rust-version "1.69")))

(define-public crate-serai-client-0.1 (crate (name "serai-client") (vers "0.1.0") (deps (list (crate-dep (name "scale") (req "^3") (default-features #t) (kind 0) (package "parity-scale-codec")) (crate-dep (name "scale-info") (req "^2") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "zeroize") (req "^1.5") (default-features #t) (kind 0)))) (hash "1q8s7fg0md0vim42iry417x87ks71avgxq2a30nr1jz9qs8lh8w4")))

(define-public crate-serai-coins-pallet-0.0.1 (crate (name "serai-coins-pallet") (vers "0.0.1") (hash "173py04661aczwrxsxa9vhkhn60j0mac0bycphgi82rsmzpxaa4r")))

(define-public crate-serai-coins-primitives-0.0.1 (crate (name "serai-coins-primitives") (vers "0.0.1") (hash "1gr1hrq82gny3i5xa98k6vpraywhl5znaj1s8xlx0lfdsn0ywqss")))

(define-public crate-serai-coordinator-0.1 (crate (name "serai-coordinator") (vers "0.1.0") (deps (list (crate-dep (name "async-trait") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "blake2") (req "^0.10") (default-features #t) (kind 0)) (crate-dep (name "futures") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "hex") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "rand_core") (req "^0.6") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1") (features (quote ("full"))) (default-features #t) (kind 0)) (crate-dep (name "zeroize") (req "^1.5") (default-features #t) (kind 0)))) (hash "1w25n4fn1qcw00z0vjq2sqipahsf3f2qksq9snwhzq1zf2fbn2a5")))

(define-public crate-serai-coordinator-tests-0.1 (crate (name "serai-coordinator-tests") (vers "0.1.0") (hash "0wlc9qpg9skhiq3nf7bna3nbgjbnjmzbx0yxrljx4za7ypsl6bqc")))

(define-public crate-serai-db-0.1 (crate (name "serai-db") (vers "0.1.0") (hash "0nnjdp21hhc0aq5cmd1afy5h1x2bswbhr5c3z2rbs69kwb94f8pk")))

(define-public crate-serai-dex-pallet-0.0.1 (crate (name "serai-dex-pallet") (vers "0.0.1") (hash "1i7d4dgsj9lqxahhpan4gng9pgj4cd7j61qv9hg74f3jnsxynrp3")))

(define-public crate-serai-dex-primitives-0.0.1 (crate (name "serai-dex-primitives") (vers "0.0.1") (hash "01vmqvdvrkbasl3g1z3d2hria6v34kw4skyng03j9w48szfb9crq")))

(define-public crate-serai-docker-tests-0.1 (crate (name "serai-docker-tests") (vers "0.1.0") (hash "0yvb19ig51v14ww0hpf6lf9i6kn171s0lw9pqhic4sdww1pya3h5")))

(define-public crate-serai-env-0.1 (crate (name "serai-env") (vers "0.1.0") (hash "0p89arjp0bdq0cxfya5agqdgh8qlbmkkpmb7q73ssy38kipn6di5")))

(define-public crate-serai-ethereum-relayer-0.1 (crate (name "serai-ethereum-relayer") (vers "0.1.0") (hash "0x34cf1wxn71jn9yvjycbilbbxsapwwasqb09ri58z3b45r62g2j")))

(define-public crate-serai-full-stack-tests-0.1 (crate (name "serai-full-stack-tests") (vers "0.1.0") (hash "087mxbwcnpfka9i10c2k4vzraf08vq0220sb1zmapk087r9vq73v")))

(define-public crate-serai-in-instructions-pallet-0.1 (crate (name "serai-in-instructions-pallet") (vers "0.1.0") (deps (list (crate-dep (name "scale") (req "^3") (features (quote ("derive" "max-encoded-len"))) (kind 0) (package "parity-scale-codec")) (crate-dep (name "scale-info") (req "^2") (features (quote ("derive"))) (kind 0)) (crate-dep (name "thiserror") (req "^1") (optional #t) (default-features #t) (kind 0)))) (hash "1dzz80wx5r69ns2f1w4bb5jb1sfcl56g3xj1190h0l8di7qw2445")))

(define-public crate-serai-in-instructions-primitives-0.1 (crate (name "serai-in-instructions-primitives") (vers "0.1.0") (deps (list (crate-dep (name "scale") (req "^3") (features (quote ("derive"))) (kind 0) (package "parity-scale-codec")) (crate-dep (name "scale-info") (req "^2") (features (quote ("derive"))) (kind 0)) (crate-dep (name "serde") (req "^1") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "zeroize") (req "^1.5") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)))) (hash "020pqrs9rhsnm91na6j3x6jk2vjyg57w9dvb6lklylpp6kyi18dk")))

(define-public crate-serai-message-queue-0.1 (crate (name "serai-message-queue") (vers "0.1.0") (deps (list (crate-dep (name "hex") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "jsonrpsee") (req "^0.16") (features (quote ("server"))) (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "01y9cj6bik169yj1jsz0p3482if0wl63lzqakh5cbiq30hhk017q")))

(define-public crate-serai-message-queue-tests-0.1 (crate (name "serai-message-queue-tests") (vers "0.1.0") (deps (list (crate-dep (name "dockertest") (req "^0.3") (default-features #t) (kind 0)))) (hash "098psgbqfra7gyhb0wdjhflk8ki29pa59fwxlm3mc2bajpaif0d8")))

(define-public crate-serai-no-std-tests-0.1 (crate (name "serai-no-std-tests") (vers "0.1.0") (hash "160n1mha4dlq640000cfvpl3xk3gr2yd840qjl0awzipbppjdlks")))

(define-public crate-serai-node-0.1 (crate (name "serai-node") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^4") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "futures") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "jsonrpsee") (req "^0.16") (features (quote ("server"))) (default-features #t) (kind 0)))) (hash "03if5rw0f52dhqrzfhdnrcwfyam74k3gkrpxa80g1gb9p5h3grlc")))

(define-public crate-serai-orchestrator-0.0.1 (crate (name "serai-orchestrator") (vers "0.0.1") (hash "1mjgs6mq3ziakb4jw280fvvqgkcxddljpy3fa673n9y23x9wqcik")))

(define-public crate-serai-polyfill-0.0.1 (crate (name "serai-polyfill") (vers "0.0.1") (hash "17pl14f4kgn8l3v70lgvc37ql94qnqrskbadlqx0sspm5d3nv1nj")))

(define-public crate-serai-polyfills-0.0.1 (crate (name "serai-polyfills") (vers "0.0.1") (hash "1rbphkmg2h1aanfxqxb8ghxvl28vy1bcvqg51v7vbmr2cjpmnpnw")))

(define-public crate-serai-primitives-0.1 (crate (name "serai-primitives") (vers "0.1.0") (deps (list (crate-dep (name "lazy_static") (req "^1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "scale") (req "^3") (features (quote ("derive"))) (kind 0) (package "parity-scale-codec")) (crate-dep (name "scale-info") (req "^2") (features (quote ("derive"))) (kind 0)) (crate-dep (name "serde") (req "^1") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "zeroize") (req "^1.5") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)))) (hash "0567bhbsbhsfzadckfxmlbh357pxklhf8kp7px4hmxc8nxlbpfcf")))

(define-public crate-serai-processor-0.1 (crate (name "serai-processor") (vers "0.1.0") (deps (list (crate-dep (name "async-trait") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "bincode") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "group") (req "^0.13") (default-features #t) (kind 0)) (crate-dep (name "hex") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "rand_chacha") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "rand_core") (req "^0.6") (default-features #t) (kind 0)) (crate-dep (name "scale") (req "^3") (default-features #t) (kind 0) (package "parity-scale-codec")) (crate-dep (name "serde") (req "^1") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "zeroize") (req "^1") (default-features #t) (kind 0)))) (hash "1zpnhwjgfnaw4lqmsp641688nqg3zma6mwqbri25vqaaxpib2f3l")))

(define-public crate-serai-processor-messages-0.1 (crate (name "serai-processor-messages") (vers "0.1.0") (deps (list (crate-dep (name "bincode") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "zeroize") (req "^1") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "06241r9iwmymnag3336z03lxfxmcc99xkgj8nncm4iba1s6xi1vi")))

(define-public crate-serai-processor-tests-0.1 (crate (name "serai-processor-tests") (vers "0.1.0") (hash "1igpg3x6vsj4jyrjqw21bxq49c1c3jg6xvwlaqjp8qcl9bn66miv")))

(define-public crate-serai-reproducible-runtime-0.1 (crate (name "serai-reproducible-runtime") (vers "0.1.0") (hash "0703kq51mc4p4m6x119r7gym741abvv10bsg1pi9bbdr3v97xm8k")))

(define-public crate-serai-reproducible-runtime-tests-0.1 (crate (name "serai-reproducible-runtime-tests") (vers "0.1.0") (deps (list (crate-dep (name "dockertest") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "hex") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "rand_core") (req "^0.6") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1") (features (quote ("time"))) (default-features #t) (kind 0)))) (hash "1frxvzg63n05zflajhcp0kjwwn1yynykc2g80i7w4gpva63im9d9")))

(define-public crate-serai-runtime-0.1 (crate (name "serai-runtime") (vers "0.1.0") (deps (list (crate-dep (name "codec") (req "^3") (features (quote ("derive"))) (kind 0) (package "parity-scale-codec")) (crate-dep (name "scale-info") (req "^2") (features (quote ("derive"))) (kind 0)))) (hash "0p089vk7wrbfblqrr6yz7l5jpih9wcq28w8b8z040hjqrva4hkai")))

(define-public crate-serai-signals-pallet-0.0.1 (crate (name "serai-signals-pallet") (vers "0.0.1") (hash "0v5dvq97bp9ffhf6c7njd3qkm5fd62dda6vmiwq83cpyspshlc79")))

(define-public crate-serai-signals-primitives-0.0.1 (crate (name "serai-signals-primitives") (vers "0.0.1") (hash "17n2qkyk541jgsm4m0x1s840kzz7zlp5bq69nrvl9lvvxng6q8wn")))

(define-public crate-serai-signals-primitives-0.1 (crate (name "serai-signals-primitives") (vers "0.1.0") (deps (list (crate-dep (name "borsh") (req "^1") (features (quote ("derive" "de_strict_order"))) (optional #t) (kind 0)) (crate-dep (name "scale") (req "^3") (features (quote ("derive"))) (kind 0) (package "parity-scale-codec")) (crate-dep (name "scale-info") (req "^2") (features (quote ("derive"))) (kind 0)) (crate-dep (name "serai-primitives") (req "^0.1") (kind 0)) (crate-dep (name "serde") (req "^1") (features (quote ("derive" "alloc"))) (optional #t) (kind 0)) (crate-dep (name "zeroize") (req "^1.5") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)))) (hash "1mcz1qvagg6l24x3a9pipyg0h8zgszr2yb8r9sh64dbxkrba66wd") (features (quote (("default" "std")))) (v 2) (features2 (quote (("std" "zeroize" "scale/std" "scale-info/std" "borsh?/std" "serde?/std" "serai-primitives/std") ("serde" "dep:serde") ("borsh" "dep:borsh")))) (rust-version "1.74")))

(define-public crate-serai-staking-pallet-0.1 (crate (name "serai-staking-pallet") (vers "0.1.0") (hash "1ncji7k75lfpj9j2lz93lpfyprcd2yc94fw143xl1vix4w2sd3sp")))

(define-public crate-serai-staking-primitives-0.1 (crate (name "serai-staking-primitives") (vers "0.1.0") (hash "1jvzgq2dph1frq73liaysw1xqv9jyyw10kmqk43sqr6llc3n638s")))

(define-public crate-serai-tokens-pallet-0.1 (crate (name "serai-tokens-pallet") (vers "0.1.0") (deps (list (crate-dep (name "scale") (req "^3") (features (quote ("derive" "max-encoded-len"))) (kind 0) (package "parity-scale-codec")) (crate-dep (name "scale-info") (req "^2") (features (quote ("derive"))) (kind 0)))) (hash "1s006qayfhdifkq5apy6sjzcn8gab792mr5r0j2yl8lp21vb8y0h")))

(define-public crate-serai-tokens-primitives-0.1 (crate (name "serai-tokens-primitives") (vers "0.1.0") (deps (list (crate-dep (name "scale") (req "^3") (features (quote ("derive"))) (kind 0) (package "parity-scale-codec")) (crate-dep (name "scale-info") (req "^2") (features (quote ("derive"))) (kind 0)) (crate-dep (name "serde") (req "^1") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "zeroize") (req "^1.5") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)))) (hash "0pl8lc6hmlgx9ligavzrgs54n6cjpda0h5vqrkgdplw0rf4vr1pk")))

(define-public crate-serai-validator-sets-pallet-0.1 (crate (name "serai-validator-sets-pallet") (vers "0.1.0") (deps (list (crate-dep (name "hashbrown") (req "^0.13") (kind 0)) (crate-dep (name "scale") (req "^3") (features (quote ("derive"))) (kind 0) (package "parity-scale-codec")) (crate-dep (name "scale-info") (req "^2") (features (quote ("derive"))) (kind 0)))) (hash "1xl141k7ydl641bdbrkpyifinkzb6wfk83l5nac49qpq84p165ai")))

(define-public crate-serai-validator-sets-primitives-0.1 (crate (name "serai-validator-sets-primitives") (vers "0.1.0") (deps (list (crate-dep (name "serde") (req "^1") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "zeroize") (req "^1.5") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)))) (hash "1dkg30sx0inaakm70rl7yfzm38dxgcx0ldpr4ndl9jcr6hxch4bz")))

(define-public crate-seraph-0.1 (crate (name "seraph") (vers "0.1.0") (hash "1axj8sqs4vi79drn8c6jfcagb3wy29aic64z5awb0clsdnw66d46")))

(define-public crate-seraphine-0.1 (crate (name "seraphine") (vers "0.1.0") (hash "06r9x1917awghl6a2gjn5jxp11m9fj3pgfjfg6m2i5nj40mlcxmp")))

(define-public crate-seraphine-cli-0.1 (crate (name "seraphine-cli") (vers "0.1.0") (deps (list (crate-dep (name "seraphine-core") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "termion") (req "^2.0.1") (default-features #t) (kind 0)))) (hash "05z75pvpgd3b1gm85d67h4nvv07c4gjpmbmwzkgsv627j44zcv8k")))

(define-public crate-seraphine-cli-0.1 (crate (name "seraphine-cli") (vers "0.1.1") (deps (list (crate-dep (name "seraphine-core") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "termion") (req "^2.0.1") (default-features #t) (kind 0)))) (hash "17x4hkmzb9r8hjam8a7rjrcijphy50x4crax6n6wwllw82l4j4fb")))

(define-public crate-seraphine-core-0.1 (crate (name "seraphine-core") (vers "0.1.0") (hash "1q8v33w5q64nqn95fw60yjch8jvra2pk362a350ap5g83if599wc")))

(define-public crate-seraphine-core-0.1 (crate (name "seraphine-core") (vers "0.1.1") (hash "0nfgk031pp71s6k3wx1m6l39rs27y35l52vk8i6bnj940nry43df")))

(define-public crate-seraphite-1 (crate (name "seraphite") (vers "1.0.0") (deps (list (crate-dep (name "anyhow") (req "^1.0.75") (default-features #t) (kind 0)) (crate-dep (name "chrono") (req "^0.4.26") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^4.3.23") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "clap_complete") (req "^4.4.0") (default-features #t) (kind 0)) (crate-dep (name "git2") (req "^0.18.0") (default-features #t) (kind 0)) (crate-dep (name "owo-colors") (req "^3.5.0") (default-features #t) (kind 0)) (crate-dep (name "which") (req "^3.1") (default-features #t) (kind 0)))) (hash "0m12rh1zss2m74jhn3sn1szd3gfqi5b1l1ziznkq26yqcwnikqxq") (yanked #t)))

(define-public crate-seraphite-1 (crate (name "seraphite") (vers "1.0.1") (deps (list (crate-dep (name "anyhow") (req "^1.0.75") (default-features #t) (kind 0)) (crate-dep (name "chrono") (req "^0.4.26") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^4.3.23") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "clap_complete") (req "^4.4.0") (default-features #t) (kind 0)) (crate-dep (name "git2") (req "^0.18.0") (default-features #t) (kind 0)) (crate-dep (name "owo-colors") (req "^3.5.0") (default-features #t) (kind 0)) (crate-dep (name "which") (req "^3.1") (default-features #t) (kind 0)))) (hash "1s90b7v59qn8a3w2dxbfbq3kkmxykwaw3fah5p5gw44qxl1cilcc")))

(define-public crate-seratodj-0.0.1 (crate (name "seratodj") (vers "0.0.1") (deps (list (crate-dep (name "base64") (req "^0.13.0") (default-features #t) (kind 0)) (crate-dep (name "id3") (req "^0.5.1") (default-features #t) (kind 2)) (crate-dep (name "nom") (req "^6.1.0") (default-features #t) (kind 0)) (crate-dep (name "textwrap") (req "^0.13.2") (default-features #t) (kind 2)) (crate-dep (name "thiserror") (req "^1.0") (default-features #t) (kind 0)))) (hash "1i5jwlds29kdimv41n6v049d4j6wwp46lwlw9r2xcnrkhfs3im50") (yanked #t)))

