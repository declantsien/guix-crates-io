(define-module (crates-io se ep) #:use-module (crates-io))

(define-public crate-seep-0.1 (crate (name "seep") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^4.5.1") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "0c0brzsavbz4sandda2bqwp5vfapcxb3sljhvf68zlk048vaqn7r")))

(define-public crate-seep-0.1 (crate (name "seep") (vers "0.1.1") (deps (list (crate-dep (name "clap") (req "^4.5.1") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "1w98qwhlld9s8v2ly6fc08ky9rfr5anx9z032wkvk8ph0a20cds0")))

(define-public crate-seeport-0.1 (crate (name "seeport") (vers "0.1.0") (deps (list (crate-dep (name "coingecko") (req "^1.0.0") (default-features #t) (kind 0)) (crate-dep (name "crossterm") (req "^0.24.0") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.20.0") (default-features #t) (kind 0)) (crate-dep (name "tui") (req "^0.18.0") (default-features #t) (kind 0)))) (hash "1c7qmkzl5cmrnwnwcnnaxh8dmx6cali9j5if7mcbfbhfzbbwd7p9")))

(define-public crate-seeport-0.1 (crate (name "seeport") (vers "0.1.1") (deps (list (crate-dep (name "coingecko") (req "^1.0.0") (default-features #t) (kind 0)) (crate-dep (name "crossterm") (req "^0.24.0") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.20.0") (default-features #t) (kind 0)) (crate-dep (name "tui") (req "^0.18.0") (default-features #t) (kind 0)))) (hash "10lnvr82ac6scz33pcbiwhnx9csdfrzbva1wychx4v9sbsycpf7y")))

(define-public crate-seeport-0.1 (crate (name "seeport") (vers "0.1.2") (deps (list (crate-dep (name "chrono") (req "^0.4.19") (default-features #t) (kind 0)) (crate-dep (name "coingecko") (req "^1.0.0") (default-features #t) (kind 0)) (crate-dep (name "confy") (req "^0.4.0") (default-features #t) (kind 0)) (crate-dep (name "crossterm") (req "^0.24.0") (default-features #t) (kind 0)) (crate-dep (name "directories") (req "^4.0.1") (default-features #t) (kind 0)) (crate-dep (name "futures") (req "^0.3.21") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.140") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.20.0") (default-features #t) (kind 0)) (crate-dep (name "toml") (req "^0.5.9") (default-features #t) (kind 0)) (crate-dep (name "tui") (req "^0.18.0") (default-features #t) (kind 0)))) (hash "108xj962fid3vsq23rn0lxf75lqg2zmdwjnr3pap2a4hll3cy8l4")))

