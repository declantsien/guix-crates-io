(define-module (crates-io se n5) #:use-module (crates-io))

(define-public crate-sen5x-0.0.1 (crate (name "sen5x") (vers "0.0.1") (deps (list (crate-dep (name "embedded-hal") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "embedded-hal-mock") (req "^0.9") (default-features #t) (kind 2)) (crate-dep (name "linux-embedded-hal") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "sensirion-i2c") (req "^0.2") (default-features #t) (kind 0)))) (hash "1pjarrm66l68j6smh99dw9y8rf6jllvzy3mvv18whhrszx4rxhb8")))

(define-public crate-sen5x-rs-0.0.2 (crate (name "sen5x-rs") (vers "0.0.2") (deps (list (crate-dep (name "embedded-hal") (req "=1.0.0") (default-features #t) (kind 0)) (crate-dep (name "embedded-hal-mock") (req "=0.10.0") (features (quote ("eh1"))) (default-features #t) (kind 2)))) (hash "19ks9ph25n9y46cspgvvkpmb99ibmp82wwa7r1bxh99lfxr7yl9g")))

(define-public crate-sen5x-rs-0.1 (crate (name "sen5x-rs") (vers "0.1.0") (deps (list (crate-dep (name "embedded-hal") (req "=1.0.0") (default-features #t) (kind 0)) (crate-dep (name "embedded-hal-mock") (req "=0.10.0") (features (quote ("eh1"))) (default-features #t) (kind 2)))) (hash "1j3qrzxqsl4246a4slnndc3awf910krrk3jn5xh5ikg2ij2lrrwf")))

(define-public crate-sen5x-rs-0.2 (crate (name "sen5x-rs") (vers "0.2.0") (deps (list (crate-dep (name "embedded-hal") (req "=1.0.0") (default-features #t) (kind 0)) (crate-dep (name "embedded-hal-mock") (req "=0.10.0") (features (quote ("eh1"))) (default-features #t) (kind 2)) (crate-dep (name "sensirion-i2c") (req "^0.3.0") (default-features #t) (kind 0)))) (hash "0ryp0l82yxzdnkr7y9kdnzir5r8na52g5jhqf70821v7s0g1z2rx")))

