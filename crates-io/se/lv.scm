(define-module (crates-io se lv) #:use-module (crates-io))

(define-public crate-selva-0.2 (crate (name "selva") (vers "0.2.0") (deps (list (crate-dep (name "glium") (req "^0.21.0") (default-features #t) (kind 0)) (crate-dep (name "glsl-include") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "glslwatch") (req "^0.1.1") (default-features #t) (kind 0)) (crate-dep (name "image") (req "^0.19") (default-features #t) (kind 0)) (crate-dep (name "itertools") (req "^0.7") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.2") (default-features #t) (kind 0)))) (hash "0cacb9pf2lvw7h6nd9ljy6wlg9cjmv0zvf1zbngpdlj5fmqank12")))

