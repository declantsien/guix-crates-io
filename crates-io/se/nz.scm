(define-module (crates-io se nz) #:use-module (crates-io))

(define-public crate-senzu-0.1 (crate (name "senzu") (vers "0.1.0") (deps (list (crate-dep (name "bitcoin") (req "^0.27.0") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^2.33.3") (default-features #t) (kind 0)) (crate-dep (name "indicatif") (req "^0.16.2") (default-features #t) (kind 0)) (crate-dep (name "rayon") (req "^1.5.1") (default-features #t) (kind 0)) (crate-dep (name "xyzpub") (req "^0.2.1") (default-features #t) (kind 0)))) (hash "0b6ka47bcs8mx4yxxp6ipi6z7jwx9d67qkah0szxh44f58jajzmy")))

