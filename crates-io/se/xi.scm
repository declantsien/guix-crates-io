(define-module (crates-io se xi) #:use-module (crates-io))

(define-public crate-seximal-0.1 (crate (name "seximal") (vers "0.1.0") (deps (list (crate-dep (name "num") (req "^0.1") (default-features #t) (kind 0)))) (hash "1f57rh77bnxl7acq5ilrg34n0rsdci6k3yxjmz8z5f2y0x5b8cv6")))

(define-public crate-seximal-0.1 (crate (name "seximal") (vers "0.1.1") (deps (list (crate-dep (name "num") (req "^0.1") (default-features #t) (kind 0)))) (hash "17z90qx3f9wd9krrdc52gd9n6adxlv92fz6vzsxc8bjzc844r6l4")))

(define-public crate-seximal-0.1 (crate (name "seximal") (vers "0.1.2") (deps (list (crate-dep (name "num") (req "^0.1") (default-features #t) (kind 0)))) (hash "14aws5qq1gai05gr20brak70l1xd8dfgkqn3zqdlw0iicdg5k5fn")))

(define-public crate-seximal-0.1 (crate (name "seximal") (vers "0.1.3") (deps (list (crate-dep (name "num") (req "^0.1") (default-features #t) (kind 0)))) (hash "1k3a3q3igx0ckhd0jiq7hibxm29m7lpwplxwn3rxy3swc0pp7snc")))

(define-public crate-seximal-0.1 (crate (name "seximal") (vers "0.1.4") (deps (list (crate-dep (name "num") (req "^0.1") (default-features #t) (kind 0)))) (hash "1460prng7wcdhvj4akl5ivhww2mrfzd90ma5b388ac80ly7cs8yl")))

(define-public crate-seximal-0.1 (crate (name "seximal") (vers "0.1.5") (deps (list (crate-dep (name "num") (req "^0.1") (default-features #t) (kind 0)))) (hash "0lj238lprgcy2724hldxlq4z5aqgig1biiqw9l53syfny1y6846f")))

