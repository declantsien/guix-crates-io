(define-module (crates-io se ce) #:use-module (crates-io))

(define-public crate-secel-0.0.1 (crate (name "secel") (vers "0.0.1") (hash "0vkln2084lqwadipsh8iq9yxyz6wfzh974qfcpvh18m2vdy46f8f")))

(define-public crate-secel-0.0.2 (crate (name "secel") (vers "0.0.2") (deps (list (crate-dep (name "ascii_tree") (req "^0.1.1") (default-features #t) (kind 0)) (crate-dep (name "difference") (req "^2.0.0") (default-features #t) (kind 0)) (crate-dep (name "rust_decimal") (req "^1.26.1") (default-features #t) (kind 0)))) (hash "089i20yll9a0pig8vnwckfgn600rxdsv2ivnx5cp35r1nb5d31fy")))

(define-public crate-secel-0.0.3 (crate (name "secel") (vers "0.0.3") (deps (list (crate-dep (name "ascii_tree") (req "^0.1.1") (default-features #t) (kind 0)) (crate-dep (name "difference") (req "^2.0.0") (default-features #t) (kind 0)) (crate-dep (name "rust_decimal") (req "^1.26.1") (default-features #t) (kind 0)))) (hash "0nnqbinl7sc2kyslsbis154nkkw46i1w7nwv6g2isb7216iz3csj")))

(define-public crate-secel-0.0.4 (crate (name "secel") (vers "0.0.4") (deps (list (crate-dep (name "ascii_tree") (req "^0.1.1") (default-features #t) (kind 0)) (crate-dep (name "difference") (req "^2.0.0") (default-features #t) (kind 0)) (crate-dep (name "rust_decimal") (req "^1.26.1") (default-features #t) (kind 0)))) (hash "0za7zb6rp6v16il1afhrvqj8n773bmr2f12s7rwcy17d7gigqvf5")))

(define-public crate-secel-0.0.5 (crate (name "secel") (vers "0.0.5") (deps (list (crate-dep (name "ascii_tree") (req "^0.1.1") (default-features #t) (kind 0)) (crate-dep (name "difference") (req "^2.0.0") (default-features #t) (kind 0)) (crate-dep (name "rust_decimal") (req "^1.26.1") (default-features #t) (kind 0)))) (hash "1pfwwplgpr3hj0zq5riz2b9rm8kpla9vxgbifj6fkg6as2yy8i5q")))

(define-public crate-secel-0.0.6 (crate (name "secel") (vers "0.0.6") (deps (list (crate-dep (name "ascii_tree") (req "^0.1.1") (default-features #t) (kind 0)) (crate-dep (name "difference") (req "^2.0.0") (default-features #t) (kind 0)) (crate-dep (name "rust_decimal") (req "^1.32.0") (default-features #t) (kind 0)))) (hash "0ydr2zhqc9fy5iabh73hw90xyrq8dk7s01qv7mjjalhwzl178flg")))

(define-public crate-secel-0.0.7 (crate (name "secel") (vers "0.0.7") (deps (list (crate-dep (name "ascii_tree") (req "^0.1.1") (default-features #t) (kind 0)) (crate-dep (name "difference") (req "^2.0.0") (default-features #t) (kind 0)) (crate-dep (name "rust_decimal") (req "^1.32.0") (default-features #t) (kind 0)))) (hash "0y2ki2ba302hinaxxrzapvz430sk3s4l0fqp2fmc2zp1z2mvjl3b")))

