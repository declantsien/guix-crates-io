(define-module (crates-io se nc) #:use-module (crates-io))

(define-public crate-sencha-1 (crate (name "sencha") (vers "1.0.0") (deps (list (crate-dep (name "clap") (req "^2.29") (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "rss") (req "^1.2") (default-features #t) (kind 0)))) (hash "19q5frpnk8xw5fdxnaqxbm683zqidb6wag6l3n7xshp8d0d9zpr9") (yanked #t)))

