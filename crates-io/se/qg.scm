(define-module (crates-io se qg) #:use-module (crates-io))

(define-public crate-seqgen-0.1 (crate (name "seqgen") (vers "0.1.0") (hash "0wljh10xga2cl9b82mj67f3dakx7nwkjq9pbhv5yjgrvhdxyh79s")))

(define-public crate-seqgen-0.1 (crate (name "seqgen") (vers "0.1.1") (hash "12lssgsvln3rz0bjw7mjd426r9bpdzg506dis9bqqpsaahkc9c62")))

(define-public crate-seqgen-0.1 (crate (name "seqgen") (vers "0.1.2") (hash "1dx771h7l7p71qgab9xcmiw5k3xl4srd1fqpflpqs8c47h19blp4")))

(define-public crate-seqgen-0.1 (crate (name "seqgen") (vers "0.1.3") (hash "1y9fqcsdnmjn5bgc5c48zpbbmczkcr4s0i6y5c7z7840wh0n6ass")))

(define-public crate-seqgen-0.1 (crate (name "seqgen") (vers "0.1.4") (hash "1i9462whhv49x629vsfjnry0qdlcygxrqhgmf4wgpnlvhc4z8rdn")))

(define-public crate-seqgen-0.1 (crate (name "seqgen") (vers "0.1.5") (hash "0kkmxiqbnfal1nbswv48g4nvp6nf2v2p7w45f6xf1mckw5cn1040")))

(define-public crate-seqgen-0.1 (crate (name "seqgen") (vers "0.1.6") (hash "14h05ysz1mfxbwjps39hz8lld25ipb1j9w2zd275xld75ywphmkj")))

(define-public crate-seqgen-0.2 (crate (name "seqgen") (vers "0.2.0") (hash "14hc7l397pig1mgnvh6q02lkzylbq5z2ifcjjash9vj09xli1gvb")))

(define-public crate-seqgen-0.2 (crate (name "seqgen") (vers "0.2.1") (hash "14s50aki77bghs76hkdmirxc7dp9h73y0x518l6x0wgmc3djdvdj")))

(define-public crate-seqgen-0.2 (crate (name "seqgen") (vers "0.2.2") (hash "0429k55a0kwlr2a3kvzb8wyw1f29388da0ia7gvld4kzvbh1gvp6")))

(define-public crate-seqgen-0.2 (crate (name "seqgen") (vers "0.2.3") (hash "0jg5cixdfhliil71s733fplnjn5q5amh2qx77dqr1wq6jhpk6rvc")))

(define-public crate-seqgen-0.2 (crate (name "seqgen") (vers "0.2.4") (hash "0kk7wpw8s9ynigciwyf6wi1y89dmknn435mqlda5j3f9sykpz9ny")))

(define-public crate-seqgen-0.2 (crate (name "seqgen") (vers "0.2.5") (hash "1l051najmygfhhs3fvm5grmzlg15prrjn7galadzpryvl7ykj4gp")))

(define-public crate-seqgen-0.2 (crate (name "seqgen") (vers "0.2.6") (hash "1zplczaiz7km4lcz01van4c9c9c8hk9m6j9f0iir2nd4pg2yyjp2")))

(define-public crate-seqgen-0.2 (crate (name "seqgen") (vers "0.2.7") (hash "1n7x5xc2s2fqwcy3x6iqv72h5x3w7nmj9ckj1by53xlksy4l1bnm")))

(define-public crate-seqgen-0.2 (crate (name "seqgen") (vers "0.2.8") (hash "1siy8ycjhpxfabjsialvv2hj6lkjhal8cizjim8dji8xr5dy082m")))

(define-public crate-seqgen-0.2 (crate (name "seqgen") (vers "0.2.9") (hash "1fxhzfvcmz34kk1j7k897j9y4ns8mn1hyv49apbcccm732q6lv6s")))

(define-public crate-seqgen-0.2 (crate (name "seqgen") (vers "0.2.10") (hash "046n0qmz4n57xd00avw10gsa4wbr1f36g5r86l135mqvqbb04hwx")))

(define-public crate-seqgen-0.2 (crate (name "seqgen") (vers "0.2.11") (hash "0y9ajs5fgn5xslf9qsrmlpwrhz2c3bhndyhmk4687ajjc45qpzsp")))

(define-public crate-seqgen-0.2 (crate (name "seqgen") (vers "0.2.12") (hash "160r3q2xnyrjkyrfa41wrp5xs73dazfqxc5qii6fg75ahjjrpk27")))

(define-public crate-seqgen-0.2 (crate (name "seqgen") (vers "0.2.13") (hash "01k74h5sp931fp6a1n77w6s1zkm3nwidj1pyqjiziqn7kyw5s89h")))

(define-public crate-seqgen-0.3 (crate (name "seqgen") (vers "0.3.0") (hash "08fny5hxanfmg3ll73hi50zb39hjydh35594cj81m032rw0jc2j9")))

(define-public crate-seqgen-0.3 (crate (name "seqgen") (vers "0.3.1") (hash "0pk6rqar4da0pyrsmlg8c8h6y7l8bhwf6dka8swzrkn26v8717xr")))

(define-public crate-seqgen-0.3 (crate (name "seqgen") (vers "0.3.2") (hash "0747szbvb71j4796cdbsrzg1c2i9dyby429n7cr7snjxi0km4jw1")))

(define-public crate-seqgen-0.3 (crate (name "seqgen") (vers "0.3.3") (hash "0hmvhg1mfklgl4ka3y2gr1q4vyll16pndnw9ak88piq2vnr6pp4w")))

