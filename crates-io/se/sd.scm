(define-module (crates-io se sd) #:use-module (crates-io))

(define-public crate-sesdiff-0.1 (crate (name "sesdiff") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^2.33.2") (default-features #t) (kind 0)) (crate-dep (name "dissimilar") (req "^1.0.2") (default-features #t) (kind 0)))) (hash "0h5n2v0aii3myk9lrm83y7hqhs64wfi61v2vqps0i6npgz70nvg8")))

(define-public crate-sesdiff-0.1 (crate (name "sesdiff") (vers "0.1.1") (deps (list (crate-dep (name "clap") (req "^2.33.2") (default-features #t) (kind 0)) (crate-dep (name "dissimilar") (req "^1.0.2") (default-features #t) (kind 0)))) (hash "096j0rdsj1qsc0m8k903r0vbf1nzghi45gqpqswq7v5nck179sq7")))

(define-public crate-sesdiff-0.1 (crate (name "sesdiff") (vers "0.1.2") (deps (list (crate-dep (name "clap") (req "^2.33.2") (default-features #t) (kind 0)) (crate-dep (name "dissimilar") (req "^1.0.2") (default-features #t) (kind 0)))) (hash "17qcvibx260hlng7qrsn27b18w3lln5i61d2xlr5ni4zzkykqf6c")))

(define-public crate-sesdiff-0.1 (crate (name "sesdiff") (vers "0.1.4") (deps (list (crate-dep (name "clap") (req "^2.33.2") (default-features #t) (kind 0)) (crate-dep (name "dissimilar") (req "^1.0.2") (default-features #t) (kind 0)))) (hash "0zq35zzdpz4lyifb53py4jsgzf8i1jd0l9cmdgby764m55dsdqb9")))

(define-public crate-sesdiff-0.1 (crate (name "sesdiff") (vers "0.1.5") (deps (list (crate-dep (name "clap") (req "^2.33.2") (default-features #t) (kind 0)) (crate-dep (name "dissimilar") (req "^1.0.2") (default-features #t) (kind 0)))) (hash "1qggmd25hh16sc91rkf6dn50hcsvpmgdbiml4bi37983i9q3cq47")))

(define-public crate-sesdiff-0.2 (crate (name "sesdiff") (vers "0.2.0") (deps (list (crate-dep (name "clap") (req "^2.33.2") (default-features #t) (kind 0)) (crate-dep (name "dissimilar") (req "^1.0.2") (default-features #t) (kind 0)))) (hash "0wvfdlvjf6sb09kf1r78sk8zpfbfl0yn07yn80x5p9raqfclkkqz")))

(define-public crate-sesdiff-0.3 (crate (name "sesdiff") (vers "0.3.0") (deps (list (crate-dep (name "clap") (req "^2.33.2") (default-features #t) (kind 0)) (crate-dep (name "dissimilar") (req "^1.0.2") (default-features #t) (kind 0)))) (hash "10dmfjcrsxfnrw7243syabchrjf7asaikb3xfvspqq6f53rjh9w7")))

