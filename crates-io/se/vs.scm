(define-module (crates-io se vs) #:use-module (crates-io))

(define-public crate-sevseg_3642bs-0.3 (crate (name "sevseg_3642bs") (vers "0.3.1") (deps (list (crate-dep (name "embedded-hal") (req "^0.2.7") (default-features #t) (kind 0)))) (hash "00lhq20wf70ibbq6i1y74v14v90wp847bqbhy2h5cq5bvw82sb0s")))

(define-public crate-sevseg_3642bs-0.3 (crate (name "sevseg_3642bs") (vers "0.3.2") (deps (list (crate-dep (name "embedded-hal") (req "^0.2.7") (default-features #t) (kind 0)))) (hash "1g6x7dmsjia7mjqc4waaqv4k0lw10lg79rgvi0bv37x4iq4jyc6d")))

(define-public crate-sevseg_3642bs-0.3 (crate (name "sevseg_3642bs") (vers "0.3.3") (deps (list (crate-dep (name "embedded-hal") (req "^0.2.7") (default-features #t) (kind 0)))) (hash "18kkmbikfqyk9flkf8yzhhpfiw3q10rahq48wvhb33sqdlbq89il")))

(define-public crate-sevseg_3642bs-0.3 (crate (name "sevseg_3642bs") (vers "0.3.4") (deps (list (crate-dep (name "embedded-hal") (req "^0.2.7") (default-features #t) (kind 0)))) (hash "19rvgcw1bls7gjrjlkvzbs7g4y3hizqjx4zjic4g7nhajds8i5sg")))

