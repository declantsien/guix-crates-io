(define-module (crates-io se e-) #:use-module (crates-io))

(define-public crate-see-through-0.0.1 (crate (name "see-through") (vers "0.0.1") (deps (list (crate-dep (name "see_derive") (req "^0.0.1") (optional #t) (default-features #t) (kind 0)))) (hash "0hq82p9ciwym3h1r1bipx7a9fp3mq9jw8afs62pafm0zy47mz06f") (features (quote (("default")))) (v 2) (features2 (quote (("macros" "dep:see_derive"))))))

(define-public crate-see-through-0.0.2 (crate (name "see-through") (vers "0.0.2") (deps (list (crate-dep (name "see_derive") (req "^0.0.2") (optional #t) (default-features #t) (kind 0)))) (hash "1hma0xfkl7mqbzsc91rl9n6k2ppkzhkn352z9vln7gcmwd10clmd") (features (quote (("default")))) (v 2) (features2 (quote (("macros" "dep:see_derive"))))))

(define-public crate-see-through-0.0.3 (crate (name "see-through") (vers "0.0.3") (deps (list (crate-dep (name "see_derive") (req "^0.0.3") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "trybuild") (req "^1.0.77") (default-features #t) (kind 2)))) (hash "1xiybf6zhmr6hnax2lnn3662ksh3pqdzk1yw13j4hsw7n8q3hzki") (features (quote (("default" "macros")))) (v 2) (features2 (quote (("macros" "dep:see_derive"))))))

(define-public crate-see-through-0.0.4 (crate (name "see-through") (vers "0.0.4") (deps (list (crate-dep (name "see_derive") (req "^0.0.4") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "trybuild") (req "^1.0.77") (default-features #t) (kind 2)))) (hash "0r9yzrdld4ibmgb26rjs1v8pavxk2y608k1sx0pvy8266r6rqqxg") (features (quote (("default" "macros")))) (v 2) (features2 (quote (("macros" "dep:see_derive"))))))

(define-public crate-see-through-0.0.5 (crate (name "see-through") (vers "0.0.5") (deps (list (crate-dep (name "see_derive") (req "^0.0.5") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "trybuild") (req "^1.0.77") (default-features #t) (kind 2)))) (hash "1h6hw0452m7imhm5n8jv5ywc35y1zjd3gk9kn053ahq2xcs883gb") (features (quote (("default" "macros")))) (v 2) (features2 (quote (("macros" "dep:see_derive"))))))

(define-public crate-see-you-later-0.1 (crate (name "see-you-later") (vers "0.1.0") (deps (list (crate-dep (name "futures") (req "^0.3.4") (default-features #t) (kind 0)) (crate-dep (name "futures-timer") (req "^3.0") (default-features #t) (kind 0)) (crate-dep (name "pin-project-lite") (req "^0.1.4") (default-features #t) (kind 0)) (crate-dep (name "smol") (req "^0.1") (default-features #t) (kind 2)) (crate-dep (name "smol-potat") (req "^0.2") (default-features #t) (kind 2)) (crate-dep (name "wait-for-me") (req "^0.1") (default-features #t) (kind 2)))) (hash "0vnyypfvwnrq5zq2hzl3fr54nsi5p01h9x3dcvkqhg2nfgrx13if")))

