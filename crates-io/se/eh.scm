(define-module (crates-io se eh) #:use-module (crates-io))

(define-public crate-seehecht-3 (crate (name "seehecht") (vers "3.0.0") (deps (list (crate-dep (name "chrono") (req "^0.4.19") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^3.1.2") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "10pdjf458rwk8v67p0n81i1m800d702npk1if0n559za60vkniqw")))

(define-public crate-seehecht-3 (crate (name "seehecht") (vers "3.0.2") (deps (list (crate-dep (name "chrono") (req "^0.4.19") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^3.1.2") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "14swjncii7vfajpb78xgcy304a1x3bz4ghh2mpx3fw4y6xxcpfdv")))

