(define-module (crates-io se tj) #:use-module (crates-io))

(define-public crate-setjmp-0.1 (crate (name "setjmp") (vers "0.1.0") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "1wzyzpzpv1h804n7snhc2v46nq013imwg9yqmqrhippmcpaa392p")))

(define-public crate-setjmp-0.1 (crate (name "setjmp") (vers "0.1.1") (deps (list (crate-dep (name "bindgen") (req "^0.53") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "1v1ydjid53jw3754fqd2dvgjc4hk5d5xficzg2gin0yndhnwjd3m")))

(define-public crate-setjmp-0.1 (crate (name "setjmp") (vers "0.1.2") (deps (list (crate-dep (name "bindgen") (req "^0.53") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "0hx3y9bycq0wvrh4kjvz7zf0ln0af0462gky5s3pjgqvrfdlg7gi")))

(define-public crate-setjmp-0.1 (crate (name "setjmp") (vers "0.1.3") (deps (list (crate-dep (name "bindgen") (req "^0.53") (default-features #t) (kind 1)) (crate-dep (name "clang-sys") (req "^0.29") (features (quote ("clang_6_0"))) (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 1)))) (hash "1jx02ymqgfy1fik23nnm4s9vncrz0w1nvgv55lg4642m5i9s668m")))

(define-public crate-setjmp-0.1 (crate (name "setjmp") (vers "0.1.4") (deps (list (crate-dep (name "bindgen") (req "^0.53") (default-features #t) (kind 1)) (crate-dep (name "clang-sys") (req "^0.29") (features (quote ("clang_6_0"))) (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 1)))) (hash "0wrw13md93mf6d3ljfslzw3n1vmpr6sswp6fyz64jhwv5q28xklv")))

