(define-module (crates-io se ah) #:use-module (crates-io))

(define-public crate-seahash-0.1 (crate (name "seahash") (vers "0.1.0") (hash "1vlk5v3g5jqry8vqr5kjqhgik2q393lc3wrf9x9gifjcj73lbq59")))

(define-public crate-seahash-0.2 (crate (name "seahash") (vers "0.2.0") (hash "02jvwxa80x25n7npmhc3i85s6qj0sscf4hn24k58ar3kjipcdq5p")))

(define-public crate-seahash-1 (crate (name "seahash") (vers "1.0.0") (hash "1s8cf27sks7pdydd0cx3b42x11i814mq3vcpc79kcrnyv25bxnlf")))

(define-public crate-seahash-2 (crate (name "seahash") (vers "2.0.0") (hash "1k199xapv9mlf27p07yp68arqldgk4d92sc7m5p1pky042a8smxi")))

(define-public crate-seahash-2 (crate (name "seahash") (vers "2.1.0") (hash "0ypf7wnw6kq9ykw3d0ghblymv0v7zx7jgjcrniccyg3arbxqka03")))

(define-public crate-seahash-2 (crate (name "seahash") (vers "2.1.1") (hash "18rcj68y7zw2nhmqwhs07jyxnw85l475i6pbdlxa53y0pwky499v")))

(define-public crate-seahash-2 (crate (name "seahash") (vers "2.2.0") (hash "1yqrasvf2vlyfwmxy7dsvwpyzh35sxhfk2y2m3iv1mcbm07m37pz")))

(define-public crate-seahash-2 (crate (name "seahash") (vers "2.3.0") (hash "17q4rmm92v5m1ghwwh2jgkbyr6bbwv2n5w4ci5ixvj8pzal4haqf")))

(define-public crate-seahash-2 (crate (name "seahash") (vers "2.4.0") (hash "1sp8k11z3mpqyy469y712kgpikn9n5pwccfa13bga4s4waf2hqb3")))

(define-public crate-seahash-3 (crate (name "seahash") (vers "3.0.0") (hash "1s5kly7wlp9mqbgnqk4y230s6335vh03692bnw6pgwvwcw27vz6w")))

(define-public crate-seahash-3 (crate (name "seahash") (vers "3.0.1") (hash "0k04v9jw7qjgj3w19k9pdkdl2rdvbghqkkwsair06qsnb9s4icd3")))

(define-public crate-seahash-3 (crate (name "seahash") (vers "3.0.2") (hash "1dbh1d62npz6dhhb6ky4shli61ds3knznb5m2vfzn9kxd0w7gizc")))

(define-public crate-seahash-3 (crate (name "seahash") (vers "3.0.3") (hash "0d15lyy4mvdbb89jsx95wh3134f8giz25kiz07avpcyx8jlmjcx4")))

(define-public crate-seahash-3 (crate (name "seahash") (vers "3.0.4") (hash "02h8wy8zc0r8vfskk41756lw4i1xs0hhnkkbs8iz88whb7pj0pc7")))

(define-public crate-seahash-3 (crate (name "seahash") (vers "3.0.5") (hash "17fhp7i4qnlicjdpajwabd5jvdwmcbnd2nkfqg6jz115xmmn6j70")))

(define-public crate-seahash-3 (crate (name "seahash") (vers "3.0.6") (hash "1pr8ijnxnp68ki4m4740yc5mr01zijf86yx07wbsqzwiyhghdmhq")))

(define-public crate-seahash-3 (crate (name "seahash") (vers "3.0.7") (hash "0iqg12lxkn0ivsfa1gkylcwj5wmi6zl87mbizlrkg918s6hprxaq")))

(define-public crate-seahash-4 (crate (name "seahash") (vers "4.0.0") (hash "0ph42nn27nb491adsyj0gppsmw9za8z8l54r5zgdkvzja14fc9xl")))

(define-public crate-seahash-4 (crate (name "seahash") (vers "4.0.1") (deps (list (crate-dep (name "quickcheck") (req "^0.9.2") (default-features #t) (kind 2)))) (hash "05n97n62msdxz3wl9i9x4r7rinh6akgs22fa514j4b97msf4bvir")))

(define-public crate-seahash-4 (crate (name "seahash") (vers "4.1.0") (deps (list (crate-dep (name "criterion") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "quickcheck") (req "^0.9.2") (default-features #t) (kind 2)))) (hash "0sxsb64np6bvnppjz5hg4rqpnkczhsl8w8kf2a5lr1c08xppn40w") (features (quote (("use_std") ("default"))))))

(define-public crate-seahorse-0.1 (crate (name "seahorse") (vers "0.1.0") (hash "1p2wc153wg7mjvhr0wnzxh6alibclb1j86lac09s0k8f2frf9nph")))

(define-public crate-seahorse-0.1 (crate (name "seahorse") (vers "0.1.1") (hash "0azwrbxx6diz3q9sf18l17ys4mx8apsvlrwcq1mqdmkn4xyzz7q4")))

(define-public crate-seahorse-0.2 (crate (name "seahorse") (vers "0.2.0") (hash "1jfl1yv6icrp2l8lblma3mi0q72srw197lgahf3hb8i5ba229ial")))

(define-public crate-seahorse-0.2 (crate (name "seahorse") (vers "0.2.1") (hash "06hbwji6km6hrn1wyj2f8dfq0g3wk3jhvbnxd9aw7vikbysa7hfa")))

(define-public crate-seahorse-0.2 (crate (name "seahorse") (vers "0.2.2") (hash "1mx8ijrb7ndqnpr9p0kqjb0rsiszq31qf436ycrgrcdjahdzk5fq")))

(define-public crate-seahorse-0.2 (crate (name "seahorse") (vers "0.2.3") (hash "0q3m0sdbmkmqhs01vjw88ymakir5cff2r8lkx37v4yagwr7jfz1s")))

(define-public crate-seahorse-0.2 (crate (name "seahorse") (vers "0.2.4") (hash "0cfn2hg7l7s8xbpg03056pb4cvc1wlyfzla9pf6cflr13vjgsv5f")))

(define-public crate-seahorse-0.2 (crate (name "seahorse") (vers "0.2.5") (hash "1p7n70xl8xkp54f2av8h31m1z8fw827jfhvyfli2wyg50gk4ky45")))

(define-public crate-seahorse-0.3 (crate (name "seahorse") (vers "0.3.0") (hash "1ac5f098dcx02ka6d68gcn77j3d75igywlqpvhj8c3b8qxqi8izc")))

(define-public crate-seahorse-0.3 (crate (name "seahorse") (vers "0.3.1") (hash "0vmafbgfac12cni9ygl34vsb32arxxcdiahgckq3nq1vkvac438r")))

(define-public crate-seahorse-0.4 (crate (name "seahorse") (vers "0.4.0") (hash "1xb8ibc18qkzkfiakp2iz2w6smbjgihpvgnzx08bzrq0z5bbxzrm")))

(define-public crate-seahorse-0.4 (crate (name "seahorse") (vers "0.4.1") (hash "1c0g1wj3mcwgxpawsrxic52msfgkgi9485y6lv7pikd61wibq8kj")))

(define-public crate-seahorse-0.4 (crate (name "seahorse") (vers "0.4.2") (hash "0ymdv69k8fyk19kp3wslbiirf9a5idbllxa2kz5vy864ygm62z5g")))

(define-public crate-seahorse-0.4 (crate (name "seahorse") (vers "0.4.3") (hash "16a41542j4d66y0m2kw95ycwd00nfb5c19mg6bfrcxrwribh8z0l")))

(define-public crate-seahorse-0.4 (crate (name "seahorse") (vers "0.4.4") (hash "0cr87aab2988r3cibn69k7a4kqxsn9m0pgx956rk1pz1x5lj2q3j")))

(define-public crate-seahorse-0.4 (crate (name "seahorse") (vers "0.4.5") (hash "0n4wa3z8wcinrd986gs2577v68piiis6923353pb1lswsnrpw4si")))

(define-public crate-seahorse-0.4 (crate (name "seahorse") (vers "0.4.6") (hash "1jr67xlzs3z1vahnfkkiklz1bkm237x9nrl16yvaly5jjz62hg2v")))

(define-public crate-seahorse-0.4 (crate (name "seahorse") (vers "0.4.7") (hash "0zpbak69lq4zm2606inn21ifxanf7ymkcn84yflpdkyyx2vh5fsk")))

(define-public crate-seahorse-0.5 (crate (name "seahorse") (vers "0.5.0") (hash "1bk5gm049z6ihfxvx7rzw54cpwkpzkbc32nia95asin2vnfsr9qw")))

(define-public crate-seahorse-0.5 (crate (name "seahorse") (vers "0.5.1") (hash "1bfns0808bhb9rxfm6kk55xndygmbnsvb9ng6id9p436v607byz2")))

(define-public crate-seahorse-0.5 (crate (name "seahorse") (vers "0.5.2") (hash "00plfb0mpy9zllr1hw1y8lppiyr8f0f82b2fv1cld5na98livsrl")))

(define-public crate-seahorse-0.5 (crate (name "seahorse") (vers "0.5.3") (hash "0qbjhcz402hasvfr12w3p293lwzjw8dzrviy3c62ij78fxgq2riw")))

(define-public crate-seahorse-0.6 (crate (name "seahorse") (vers "0.6.0") (hash "05cg21rlpf9qhcanqv9d1vcqm5974s0z31qd3p6cb5nvkqkx8an9")))

(define-public crate-seahorse-0.6 (crate (name "seahorse") (vers "0.6.1") (hash "0i9q5ryxcpyh2i02fvkx2i6irlspg2nv08gri1karh2b1j44wdr4")))

(define-public crate-seahorse-0.6 (crate (name "seahorse") (vers "0.6.2") (hash "0wqfma6cxa8ip8vbkxnr72zad8claw4w5c8hsiyylpk310mc6g0h")))

(define-public crate-seahorse-0.7 (crate (name "seahorse") (vers "0.7.0") (hash "17byc80ph1lq4kkxik02kmpzn195rll3gf2qqmwj0dgwx4v9757x")))

(define-public crate-seahorse-0.7 (crate (name "seahorse") (vers "0.7.1") (hash "1akf2wx8bcah9pzxji9n4x7h8a9cqknqi0096mma5f0clk3l1b94")))

(define-public crate-seahorse-1 (crate (name "seahorse") (vers "1.0.0") (hash "1iz9sfz19g438am74s1s4316gmn5qn8mdx338ianp4q98wgdrfj8")))

(define-public crate-seahorse-1 (crate (name "seahorse") (vers "1.1.0") (hash "118agkznwz2nrlv8pbdj9lmhlrahp5qd50dhj7r1rqw2bv7bx9sn")))

(define-public crate-seahorse-1 (crate (name "seahorse") (vers "1.1.1") (hash "0n3dd2jrpz516k7dmfij5mxd6dqvb4qcbg9knw6wwp46w9098zff")))

(define-public crate-seahorse-1 (crate (name "seahorse") (vers "1.1.2") (hash "1garn1xr6ng0ip2kplj2pblrdjmsvhx3vjq7d8iisvvqakj1zvlb")))

(define-public crate-seahorse-1 (crate (name "seahorse") (vers "1.2.0") (hash "1mj3mk2lwj66yr6fwa2lzf8vb6wbzkk7zq0qs1yfm5zpm0qj0814") (yanked #t)))

(define-public crate-seahorse-2 (crate (name "seahorse") (vers "2.0.0") (hash "1jjdcn3ndx6wzyxphnxn1v3cbhnmk6alx0391v8wkr1wvsxhiz4d")))

(define-public crate-seahorse-2 (crate (name "seahorse") (vers "2.1.0") (hash "174wmxvvqz06nq4x9caym6fpys7n3ch4dyqn3zsz4w9ix48p2dmk")))

(define-public crate-seahorse-2 (crate (name "seahorse") (vers "2.2.0") (hash "0g273i7i57gxj6kzw7p51wm3458sz5vfmg8sya2p93k75yk7jmnj")))

(define-public crate-seahorse-dev-0.1 (crate (name "seahorse-dev") (vers "0.1.0") (deps (list (crate-dep (name "base58") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^3.2.8") (features (quote ("derive" "cargo"))) (default-features #t) (kind 0)) (crate-dep (name "heck") (req "^0.4.0") (default-features #t) (kind 0)) (crate-dep (name "owo-colors") (req "^3.4.0") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1.0.40") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1.0.40") (default-features #t) (kind 1)) (crate-dep (name "quote") (req "^1.0.20") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.20") (default-features #t) (kind 1)) (crate-dep (name "regex") (req "^1.5.6") (default-features #t) (kind 0)) (crate-dep (name "rustfmt-wrapper") (req "^0.2.0") (default-features #t) (target "cfg(not(target_arch = \"wasm32\"))") (kind 0)) (crate-dep (name "rustpython-parser") (req "^0.1.2") (default-features #t) (kind 0)) (crate-dep (name "spinners") (req "^4.1.0") (default-features #t) (kind 0)) (crate-dep (name "toml_edit") (req "^0.14.4") (default-features #t) (kind 0)))) (hash "1qgakhl9r5iw4xzsccslab2k6p65mmx4d5ky7rifqvzrv5a1l97r")))

(define-public crate-seahorse-dev-0.1 (crate (name "seahorse-dev") (vers "0.1.1") (deps (list (crate-dep (name "base58") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^3.2.8") (features (quote ("derive" "cargo"))) (default-features #t) (kind 0)) (crate-dep (name "heck") (req "^0.4.0") (default-features #t) (kind 0)) (crate-dep (name "owo-colors") (req "^3.4.0") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1.0.40") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1.0.40") (default-features #t) (kind 1)) (crate-dep (name "quote") (req "^1.0.20") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.20") (default-features #t) (kind 1)) (crate-dep (name "regex") (req "^1.5.6") (default-features #t) (kind 0)) (crate-dep (name "rustfmt-wrapper") (req "^0.2.0") (default-features #t) (target "cfg(not(target_arch = \"wasm32\"))") (kind 0)) (crate-dep (name "rustpython-parser") (req "^0.1.2") (default-features #t) (kind 0)) (crate-dep (name "spinners") (req "^4.1.0") (default-features #t) (kind 0)) (crate-dep (name "toml_edit") (req "^0.14.4") (default-features #t) (kind 0)))) (hash "0l3dnxkfs6zgj94vw9n741d6mcfjya11q7kj0kap5ypvwfsrb9j9")))

(define-public crate-seahorse-dev-0.2 (crate (name "seahorse-dev") (vers "0.2.0") (deps (list (crate-dep (name "base58") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^3.2.8") (features (quote ("derive" "cargo"))) (default-features #t) (kind 0)) (crate-dep (name "heck") (req "^0.4.0") (default-features #t) (kind 0)) (crate-dep (name "owo-colors") (req "^3.4.0") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1.0.40") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1.0.40") (default-features #t) (kind 1)) (crate-dep (name "quote") (req "^1.0.20") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.20") (default-features #t) (kind 1)) (crate-dep (name "regex") (req "^1.5.6") (default-features #t) (kind 0)) (crate-dep (name "rustfmt-wrapper") (req "^0.2.0") (default-features #t) (target "cfg(not(target_arch = \"wasm32\"))") (kind 0)) (crate-dep (name "rustpython-parser") (req "^0.1.2") (default-features #t) (kind 0)) (crate-dep (name "spinners") (req "^4.1.0") (default-features #t) (kind 0)) (crate-dep (name "toml_edit") (req "^0.14.4") (default-features #t) (kind 0)))) (hash "190i8j812hqldiyg1zbhbcji26n6jvbm7fvsrjvf5d17k5nlibyk")))

(define-public crate-seahorse-lang-0.1 (crate (name "seahorse-lang") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^3.2.8") (features (quote ("derive" "cargo"))) (default-features #t) (kind 0)) (crate-dep (name "heck") (req "^0.4.0") (default-features #t) (kind 0)) (crate-dep (name "owo-colors") (req "^3.4.0") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1.0.40") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1.0.40") (default-features #t) (kind 1)) (crate-dep (name "quote") (req "^1.0.20") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.20") (default-features #t) (kind 1)) (crate-dep (name "regex") (req "^1.5.6") (default-features #t) (kind 0)) (crate-dep (name "rustfmt-wrapper") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "rustpython-parser") (req "^0.1.2") (default-features #t) (kind 0)) (crate-dep (name "spinners") (req "^4.1.0") (default-features #t) (kind 0)) (crate-dep (name "toml_edit") (req "^0.14.4") (default-features #t) (kind 0)))) (hash "1a538cghzc72ix23rr8ayw8qxk3jpb9nm4r3z1lk2hkkjyp1v91g")))

(define-public crate-seahorse-lang-0.1 (crate (name "seahorse-lang") (vers "0.1.1") (deps (list (crate-dep (name "clap") (req "^3.2.8") (features (quote ("derive" "cargo"))) (default-features #t) (kind 0)) (crate-dep (name "heck") (req "^0.4.0") (default-features #t) (kind 0)) (crate-dep (name "owo-colors") (req "^3.4.0") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1.0.40") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1.0.40") (default-features #t) (kind 1)) (crate-dep (name "quote") (req "^1.0.20") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.20") (default-features #t) (kind 1)) (crate-dep (name "regex") (req "^1.5.6") (default-features #t) (kind 0)) (crate-dep (name "rustfmt-wrapper") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "rustpython-parser") (req "^0.1.2") (default-features #t) (kind 0)) (crate-dep (name "spinners") (req "^4.1.0") (default-features #t) (kind 0)) (crate-dep (name "toml_edit") (req "^0.14.4") (default-features #t) (kind 0)))) (hash "0dmzf8960nllybn2hflypb1hcli98dac5wmnqdiirvpmypd0256d")))

(define-public crate-seahorse-lang-0.1 (crate (name "seahorse-lang") (vers "0.1.2") (deps (list (crate-dep (name "clap") (req "^3.2.8") (features (quote ("derive" "cargo"))) (default-features #t) (kind 0)) (crate-dep (name "heck") (req "^0.4.0") (default-features #t) (kind 0)) (crate-dep (name "owo-colors") (req "^3.4.0") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1.0.40") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1.0.40") (default-features #t) (kind 1)) (crate-dep (name "quote") (req "^1.0.20") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.20") (default-features #t) (kind 1)) (crate-dep (name "regex") (req "^1.5.6") (default-features #t) (kind 0)) (crate-dep (name "rustfmt-wrapper") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "rustpython-parser") (req "^0.1.2") (default-features #t) (kind 0)) (crate-dep (name "spinners") (req "^4.1.0") (default-features #t) (kind 0)) (crate-dep (name "toml_edit") (req "^0.14.4") (default-features #t) (kind 0)))) (hash "08ssmxkcn52c0gi1ihf37dh6bdjh3ar1vs5ry0cpdkg2pcrfkx4d")))

(define-public crate-seahorse-lang-0.1 (crate (name "seahorse-lang") (vers "0.1.3") (deps (list (crate-dep (name "clap") (req "^3.2.8") (features (quote ("derive" "cargo"))) (default-features #t) (kind 0)) (crate-dep (name "heck") (req "^0.4.0") (default-features #t) (kind 0)) (crate-dep (name "owo-colors") (req "^3.4.0") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1.0.40") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1.0.40") (default-features #t) (kind 1)) (crate-dep (name "quote") (req "^1.0.20") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.20") (default-features #t) (kind 1)) (crate-dep (name "regex") (req "^1.5.6") (default-features #t) (kind 0)) (crate-dep (name "rustfmt-wrapper") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "rustpython-parser") (req "^0.1.2") (default-features #t) (kind 0)) (crate-dep (name "spinners") (req "^4.1.0") (default-features #t) (kind 0)) (crate-dep (name "toml_edit") (req "^0.14.4") (default-features #t) (kind 0)))) (hash "1l8775iz22a9li0g63z986b99nx79f2bgihxgsywivn8hxh1ar3g")))

(define-public crate-seahorse-lang-0.1 (crate (name "seahorse-lang") (vers "0.1.4") (deps (list (crate-dep (name "clap") (req "^3.2.8") (features (quote ("derive" "cargo"))) (default-features #t) (kind 0)) (crate-dep (name "heck") (req "^0.4.0") (default-features #t) (kind 0)) (crate-dep (name "owo-colors") (req "^3.4.0") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1.0.40") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1.0.40") (default-features #t) (kind 1)) (crate-dep (name "quote") (req "^1.0.20") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.20") (default-features #t) (kind 1)) (crate-dep (name "regex") (req "^1.5.6") (default-features #t) (kind 0)) (crate-dep (name "rustfmt-wrapper") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "rustpython-parser") (req "^0.1.2") (default-features #t) (kind 0)) (crate-dep (name "spinners") (req "^4.1.0") (default-features #t) (kind 0)) (crate-dep (name "toml_edit") (req "^0.14.4") (default-features #t) (kind 0)))) (hash "156v49nrac5ka8w81bjla8sim4riasxx8pz14mvc76ivbg4pjzih")))

(define-public crate-seahorse-lang-0.1 (crate (name "seahorse-lang") (vers "0.1.5") (deps (list (crate-dep (name "clap") (req "^3.2.8") (features (quote ("derive" "cargo"))) (default-features #t) (kind 0)) (crate-dep (name "heck") (req "^0.4.0") (default-features #t) (kind 0)) (crate-dep (name "owo-colors") (req "^3.4.0") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1.0.40") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1.0.40") (default-features #t) (kind 1)) (crate-dep (name "quote") (req "^1.0.20") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.20") (default-features #t) (kind 1)) (crate-dep (name "regex") (req "^1.5.6") (default-features #t) (kind 0)) (crate-dep (name "rustfmt-wrapper") (req "^0.2.0") (default-features #t) (target "cfg(not(target_arch = \"wasm32\"))") (kind 0)) (crate-dep (name "rustpython-parser") (req "^0.1.2") (default-features #t) (kind 0)) (crate-dep (name "spinners") (req "^4.1.0") (default-features #t) (kind 0)) (crate-dep (name "toml_edit") (req "^0.14.4") (default-features #t) (kind 0)))) (hash "146m1pidnw1d12vxbffrz6jsk4afq6543sf6g0ifz7qwgzwbv9yn")))

(define-public crate-seahorse-lang-0.1 (crate (name "seahorse-lang") (vers "0.1.6") (deps (list (crate-dep (name "clap") (req "^3.2.8") (features (quote ("derive" "cargo"))) (default-features #t) (kind 0)) (crate-dep (name "heck") (req "^0.4.0") (default-features #t) (kind 0)) (crate-dep (name "owo-colors") (req "^3.4.0") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1.0.40") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1.0.40") (default-features #t) (kind 1)) (crate-dep (name "quote") (req "^1.0.20") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.20") (default-features #t) (kind 1)) (crate-dep (name "regex") (req "^1.5.6") (default-features #t) (kind 0)) (crate-dep (name "rustfmt-wrapper") (req "^0.2.0") (default-features #t) (target "cfg(not(target_arch = \"wasm32\"))") (kind 0)) (crate-dep (name "rustpython-parser") (req "^0.1.2") (default-features #t) (kind 0)) (crate-dep (name "spinners") (req "^4.1.0") (default-features #t) (kind 0)) (crate-dep (name "toml_edit") (req "^0.14.4") (default-features #t) (kind 0)))) (hash "1szan8a6754hi5mslkn25xdsl3gmcqvhwagzl22sg1f3asvdnj9k")))

(define-public crate-seahorse-lang-0.2 (crate (name "seahorse-lang") (vers "0.2.0") (deps (list (crate-dep (name "clap") (req "^3.2.8") (features (quote ("derive" "cargo"))) (default-features #t) (kind 0)) (crate-dep (name "heck") (req "^0.4.0") (default-features #t) (kind 0)) (crate-dep (name "owo-colors") (req "^3.4.0") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1.0.40") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1.0.40") (default-features #t) (kind 1)) (crate-dep (name "quote") (req "^1.0.20") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.20") (default-features #t) (kind 1)) (crate-dep (name "regex") (req "^1.5.6") (default-features #t) (kind 0)) (crate-dep (name "rustfmt-wrapper") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "rustpython-parser") (req "^0.1.2") (default-features #t) (kind 0)) (crate-dep (name "spinners") (req "^4.1.0") (default-features #t) (kind 0)) (crate-dep (name "toml_edit") (req "^0.14.4") (default-features #t) (kind 0)))) (hash "1p7v572kb7y6nkz7vbcx7rywv151si07bl0i4j9vmdi4mxysmx1v")))

(define-public crate-seahorse-lang-0.2 (crate (name "seahorse-lang") (vers "0.2.1") (deps (list (crate-dep (name "clap") (req "^3.2.8") (features (quote ("derive" "cargo"))) (default-features #t) (kind 0)) (crate-dep (name "heck") (req "^0.4.0") (default-features #t) (kind 0)) (crate-dep (name "owo-colors") (req "^3.4.0") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1.0.40") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1.0.40") (default-features #t) (kind 1)) (crate-dep (name "quote") (req "^1.0.20") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.20") (default-features #t) (kind 1)) (crate-dep (name "regex") (req "^1.5.6") (default-features #t) (kind 0)) (crate-dep (name "rustfmt-wrapper") (req "^0.2.0") (default-features #t) (target "cfg(not(target_arch = \"wasm32\"))") (kind 0)) (crate-dep (name "rustpython-parser") (req "^0.1.2") (default-features #t) (kind 0)) (crate-dep (name "spinners") (req "^4.1.0") (default-features #t) (kind 0)) (crate-dep (name "toml_edit") (req "^0.14.4") (default-features #t) (kind 0)))) (hash "17z4s42fgw0drsdz70yminj0fxskwz0h20kbm14ni07wcdpq418g")))

(define-public crate-seahorse-lang-0.2 (crate (name "seahorse-lang") (vers "0.2.2") (deps (list (crate-dep (name "clap") (req "^3.2.8") (features (quote ("derive" "cargo"))) (default-features #t) (kind 0)) (crate-dep (name "heck") (req "^0.4.0") (default-features #t) (kind 0)) (crate-dep (name "owo-colors") (req "^3.4.0") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1.0.40") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1.0.40") (default-features #t) (kind 1)) (crate-dep (name "quote") (req "^1.0.20") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.20") (default-features #t) (kind 1)) (crate-dep (name "regex") (req "^1.5.6") (default-features #t) (kind 0)) (crate-dep (name "rustfmt-wrapper") (req "^0.2.0") (default-features #t) (target "cfg(not(target_arch = \"wasm32\"))") (kind 0)) (crate-dep (name "rustpython-parser") (req "^0.1.2") (default-features #t) (kind 0)) (crate-dep (name "spinners") (req "^4.1.0") (default-features #t) (kind 0)) (crate-dep (name "toml_edit") (req "^0.14.4") (default-features #t) (kind 0)))) (hash "1ysb962gxcj1czzkd51fk9wfy93kl9216vqgjbvi2vywiafn7s5v")))

(define-public crate-seahorse-lang-0.2 (crate (name "seahorse-lang") (vers "0.2.3") (deps (list (crate-dep (name "clap") (req "^3.2.8") (features (quote ("derive" "cargo"))) (default-features #t) (kind 0)) (crate-dep (name "heck") (req "^0.4.0") (default-features #t) (kind 0)) (crate-dep (name "owo-colors") (req "^3.4.0") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1.0.40") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1.0.40") (default-features #t) (kind 1)) (crate-dep (name "quote") (req "^1.0.20") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.20") (default-features #t) (kind 1)) (crate-dep (name "regex") (req "^1.5.6") (default-features #t) (kind 0)) (crate-dep (name "rustfmt-wrapper") (req "^0.2.0") (default-features #t) (target "cfg(not(target_arch = \"wasm32\"))") (kind 0)) (crate-dep (name "rustpython-parser") (req "^0.1.2") (default-features #t) (kind 0)) (crate-dep (name "spinners") (req "^4.1.0") (default-features #t) (kind 0)) (crate-dep (name "toml_edit") (req "^0.14.4") (default-features #t) (kind 0)))) (hash "17550fks8spyh2cll7ayqbjdill7r54nfnz17bykir4jgi4vm09j")))

(define-public crate-seahorse-lang-0.2 (crate (name "seahorse-lang") (vers "0.2.4") (deps (list (crate-dep (name "base58") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^3.2.8") (features (quote ("derive" "cargo"))) (default-features #t) (kind 0)) (crate-dep (name "heck") (req "^0.4.0") (default-features #t) (kind 0)) (crate-dep (name "owo-colors") (req "^3.4.0") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1.0.40") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1.0.40") (default-features #t) (kind 1)) (crate-dep (name "quote") (req "^1.0.20") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.20") (default-features #t) (kind 1)) (crate-dep (name "regex") (req "^1.5.6") (default-features #t) (kind 0)) (crate-dep (name "rustfmt-wrapper") (req "^0.2.0") (default-features #t) (target "cfg(not(target_arch = \"wasm32\"))") (kind 0)) (crate-dep (name "rustpython-parser") (req "^0.1.2") (default-features #t) (kind 0)) (crate-dep (name "spinners") (req "^4.1.0") (default-features #t) (kind 0)) (crate-dep (name "toml_edit") (req "^0.14.4") (default-features #t) (kind 0)))) (hash "1sl932zs5bij9kylcw0fci08syw33i0xrk8xx34cn0ikfxx081ls")))

(define-public crate-seahorse-lang-0.2 (crate (name "seahorse-lang") (vers "0.2.5") (deps (list (crate-dep (name "base58") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^3.2.8") (features (quote ("derive" "cargo"))) (default-features #t) (kind 0)) (crate-dep (name "heck") (req "^0.4.0") (default-features #t) (kind 0)) (crate-dep (name "owo-colors") (req "^3.4.0") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1.0.40") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1.0.40") (default-features #t) (kind 1)) (crate-dep (name "quote") (req "^1.0.20") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.20") (default-features #t) (kind 1)) (crate-dep (name "regex") (req "^1.5.6") (default-features #t) (kind 0)) (crate-dep (name "rustfmt-wrapper") (req "^0.2.0") (default-features #t) (target "cfg(not(target_arch = \"wasm32\"))") (kind 0)) (crate-dep (name "rustpython-parser") (req "^0.1.2") (default-features #t) (kind 0)) (crate-dep (name "spinners") (req "^4.1.0") (default-features #t) (kind 0)) (crate-dep (name "toml_edit") (req "^0.14.4") (default-features #t) (kind 0)))) (hash "1mq4xv4wa27a1ddqrqqlvi23zla3a453bvfrjjvff9nd1p3jaf3k")))

(define-public crate-seahorse-lang-0.2 (crate (name "seahorse-lang") (vers "0.2.6") (deps (list (crate-dep (name "base58") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^3.2.8") (features (quote ("derive" "cargo"))) (default-features #t) (kind 0)) (crate-dep (name "heck") (req "^0.4.0") (default-features #t) (kind 0)) (crate-dep (name "owo-colors") (req "^3.4.0") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1.0.40") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1.0.40") (default-features #t) (kind 1)) (crate-dep (name "quote") (req "^1.0.20") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.20") (default-features #t) (kind 1)) (crate-dep (name "regex") (req "^1.5.6") (default-features #t) (kind 0)) (crate-dep (name "rustfmt-wrapper") (req "^0.2.0") (default-features #t) (target "cfg(not(target_arch = \"wasm32\"))") (kind 0)) (crate-dep (name "rustpython-parser") (req "^0.1.2") (default-features #t) (kind 0)) (crate-dep (name "spinners") (req "^4.1.0") (default-features #t) (kind 0)) (crate-dep (name "toml_edit") (req "^0.14.4") (default-features #t) (kind 0)))) (hash "1kfq5bb0pl6977mr5jnnnhbb49cv9yq9gpcn19zwzdvhabb0ka7a")))

(define-public crate-seahorse-lang-0.2 (crate (name "seahorse-lang") (vers "0.2.7") (deps (list (crate-dep (name "base58") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^3.2.8") (features (quote ("derive" "cargo"))) (default-features #t) (kind 0)) (crate-dep (name "heck") (req "^0.4.0") (default-features #t) (kind 0)) (crate-dep (name "owo-colors") (req "^3.4.0") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1.0.40") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.20") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.5.6") (default-features #t) (kind 0)) (crate-dep (name "rustpython-parser") (req "^0.1.2") (default-features #t) (kind 0)) (crate-dep (name "spinners") (req "^4.1.0") (default-features #t) (kind 0)) (crate-dep (name "toml_edit") (req "^0.14.4") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1.0.40") (default-features #t) (kind 1)) (crate-dep (name "quote") (req "^1.0.20") (default-features #t) (kind 1)) (crate-dep (name "rustfmt-wrapper") (req "^0.2.0") (default-features #t) (target "cfg(not(target_arch = \"wasm32\"))") (kind 0)))) (hash "01xlbfsr8bjyiry6zhdpb3gi9why5qyfy3lbdjxf4jhzbfg12br7")))

