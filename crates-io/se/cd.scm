(define-module (crates-io se cd) #:use-module (crates-io))

(define-public crate-secdb-0.1 (crate (name "secdb") (vers "0.1.0") (hash "1phg8ms7dilrj9l91qgbjaxznp26j12g4d0xfc7hbc4vlpcbz9dr")))

(define-public crate-secded-0.1 (crate (name "secded") (vers "0.1.0") (deps (list (crate-dep (name "byteorder") (req "^1.3.2") (features (quote ("i128"))) (kind 0)) (crate-dep (name "lazy_static") (req "^1.3.0") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.7.0") (default-features #t) (kind 2)))) (hash "0ib6k8q6sn7dih04lx8c41s46jrmzw2gvk87ls7918qnn3w88gnf") (features (quote (("no-panics") ("ffi") ("dyn") ("default") ("bench" "no-panics"))))))

(define-public crate-secded-1 (crate (name "secded") (vers "1.0.0") (deps (list (crate-dep (name "byteorder") (req "^1.3.2") (features (quote ("i128"))) (kind 0)) (crate-dep (name "lazy_static") (req "^1.3.0") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.7.0") (default-features #t) (kind 2)))) (hash "15lhr691q9hv9i60apb5fkkn91bdgjf973nh48b4ch91ggbfx8a0") (features (quote (("no-panics") ("ffi") ("dyn") ("default") ("bench" "no-panics"))))))

(define-public crate-secded-1 (crate (name "secded") (vers "1.0.1") (deps (list (crate-dep (name "byteorder") (req "^1.3.2") (features (quote ("i128"))) (kind 0)) (crate-dep (name "lazy_static") (req "^1.3.0") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.7.0") (default-features #t) (kind 2)))) (hash "0293ylskkxri11mxwq9bds1axj7nh79ppk16gkckfbh3dzmxnyhw") (features (quote (("no-panics") ("ffi") ("dyn") ("default") ("bench" "no-panics"))))))

(define-public crate-secded-1 (crate (name "secded") (vers "1.1.0") (deps (list (crate-dep (name "byteorder") (req "^1.3.2") (features (quote ("i128"))) (kind 0)) (crate-dep (name "lazy_static") (req "^1.3.0") (default-features #t) (kind 0)) (crate-dep (name "quickcheck") (req "^0.9.2") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.7.0") (default-features #t) (kind 2)))) (hash "097d97jkpfkmshf7xnlyf27zwys2rag3balp998331ckzgxknxjh") (features (quote (("no-panics") ("ffi") ("dyn") ("default") ("bench" "no-panics"))))))

