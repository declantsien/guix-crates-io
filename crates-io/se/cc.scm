(define-module (crates-io se cc) #:use-module (crates-io))

(define-public crate-secc-0.0.5 (crate (name "secc") (vers "0.0.5") (deps (list (crate-dep (name "env_logger") (req "^0.6.2") (default-features #t) (kind 2)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)))) (hash "1iyxdxzm7x6wv1c15mhwcgb3mjgznn108mm0p42g2n4m5dln2x3d")))

(define-public crate-secc-0.0.6 (crate (name "secc") (vers "0.0.6") (deps (list (crate-dep (name "env_logger") (req "^0.6.2") (default-features #t) (kind 2)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)))) (hash "10jn07s4g44f4mncglysw9b5gdlksh27wz29xyhwpkz28s47l8zl")))

(define-public crate-secc-0.0.7 (crate (name "secc") (vers "0.0.7") (deps (list (crate-dep (name "env_logger") (req "^0.6.2") (default-features #t) (kind 2)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)))) (hash "0jrz0zbgyq4j5y9icd2mq7wf4kpbv6mx4c7xnv08l6qq09sgj1yn")))

(define-public crate-secc-0.0.8 (crate (name "secc") (vers "0.0.8") (deps (list (crate-dep (name "env_logger") (req "^0.6.2") (default-features #t) (kind 2)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)))) (hash "0isr99hg0k3q0ds4g39677ji7m4zpxaax8bkqjxxbxpij151ykl3")))

(define-public crate-secc-0.0.9 (crate (name "secc") (vers "0.0.9") (deps (list (crate-dep (name "env_logger") (req "^0.6.2") (default-features #t) (kind 2)) (crate-dep (name "log") (req "^0.4.8") (default-features #t) (kind 0)))) (hash "1j542m2xvxskbdi2zs621q66i5434q6bdg3971yvcli5p0iyhyal")))

(define-public crate-secc-0.0.10 (crate (name "secc") (vers "0.0.10") (hash "0p5r5g5p9hp3lbnqxfnsr3viqq8n0wn7a1pqq637jgjmq03q9krz")))

(define-public crate-seccomp-0.1 (crate (name "seccomp") (vers "0.1.0") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "seccomp-sys") (req "^0.1.1") (default-features #t) (kind 0)))) (hash "0q66rb410ff6iwd91pa3i16k6n0b05820biy1875pnmld244zgvh")))

(define-public crate-seccomp-0.1 (crate (name "seccomp") (vers "0.1.1") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "seccomp-sys") (req "^0.1.1") (default-features #t) (kind 0)))) (hash "173bw9zzalxvqwf0bd8xbqly01250m2rl739nzx71a1z6zxqjpq7")))

(define-public crate-seccomp-0.1 (crate (name "seccomp") (vers "0.1.2") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "seccomp-sys") (req "^0.1.1") (default-features #t) (kind 0)))) (hash "0nrixpxgz0b5nji924q5w0dcsmccd3x1nh9wxbd6ypnifi2dv8vc")))

(define-public crate-seccomp-droundy-0.1 (crate (name "seccomp-droundy") (vers "0.1.0") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "seccomp-droundy-sys") (req "^0.1.2") (default-features #t) (kind 0)))) (hash "1y75z8hni0mjfz6h6bjnlvr8ic3k1dhfh88b7sd1mgy6f6mn5dyl")))

(define-public crate-seccomp-droundy-sys-0.1 (crate (name "seccomp-droundy-sys") (vers "0.1.2") (deps (list (crate-dep (name "gcc") (req "^0.3") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "pkg-config") (req "^0.3.9") (default-features #t) (kind 1)))) (hash "121l6xy5vh24dv8nvj1p6sm156m8dbz82dn2qswxhb7j9z67c290")))

(define-public crate-seccomp-filters-0.1 (crate (name "seccomp-filters") (vers "0.1.0") (hash "19k5kgx39q9kyicfs24p67xb35ywi4r7qhcl5jj78wnjzpv9jx6m")))

(define-public crate-seccomp-filters-0.1 (crate (name "seccomp-filters") (vers "0.1.1") (hash "1x0cd0b1hp021jg5cbdgb5ijmh6zm4l6x89gg8slr6fgakww3ldg")))

(define-public crate-seccomp-stream-0.1 (crate (name "seccomp-stream") (vers "0.1.0") (deps (list (crate-dep (name "libc") (req "^0.2.153") (default-features #t) (kind 2)) (crate-dep (name "libseccomp") (req "^0.3.0") (default-features #t) (kind 2)) (crate-dep (name "libseccomp-sys") (req "^0.2.1") (default-features #t) (kind 0)) (crate-dep (name "syscalls") (req "^0.6") (kind 0)) (crate-dep (name "tokio") (req "^1.37.0") (features (quote ("net"))) (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.37.0") (features (quote ("macros" "rt-multi-thread" "net"))) (default-features #t) (kind 2)) (crate-dep (name "tokio-stream") (req "^0.1.15") (default-features #t) (kind 0)))) (hash "0kfamlx1iqzrz2bhkmy604lqljk0fl437pbqllqxjkscsn9k7d5h")))

(define-public crate-seccomp-stream-0.2 (crate (name "seccomp-stream") (vers "0.2.0") (deps (list (crate-dep (name "libc") (req "^0.2.153") (default-features #t) (kind 2)) (crate-dep (name "libseccomp") (req "^0.3.0") (default-features #t) (kind 2)) (crate-dep (name "libseccomp-sys") (req "^0.2.1") (default-features #t) (kind 0)) (crate-dep (name "syscalls") (req "^0.6") (kind 0)) (crate-dep (name "tokio") (req "^1.37.0") (features (quote ("net"))) (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.37.0") (features (quote ("macros" "rt-multi-thread" "net"))) (default-features #t) (kind 2)) (crate-dep (name "tokio-stream") (req "^0.1.15") (default-features #t) (kind 0)))) (hash "1707hz8kwnspjc904jp58gq6cj640c1zpkxcwh8c378j5piaq0g5")))

(define-public crate-seccomp-stream-0.2 (crate (name "seccomp-stream") (vers "0.2.1") (deps (list (crate-dep (name "libc") (req "^0.2.153") (default-features #t) (kind 2)) (crate-dep (name "libseccomp") (req "^0.3.0") (default-features #t) (kind 2)) (crate-dep (name "libseccomp-sys") (req "^0.2.1") (default-features #t) (kind 0)) (crate-dep (name "syscalls") (req "^0.6") (kind 0)) (crate-dep (name "tokio") (req "^1.37.0") (features (quote ("net"))) (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.37.0") (features (quote ("macros" "rt-multi-thread" "net"))) (default-features #t) (kind 2)) (crate-dep (name "tokio-stream") (req "^0.1.15") (default-features #t) (kind 0)))) (hash "0pwl72hz6jnzbc8k9rqlrl4m1ciig8c9s0144k1c63hz5qhlpd75")))

(define-public crate-seccomp-sys-0.1 (crate (name "seccomp-sys") (vers "0.1.0") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "0byadsriksy25znbgsgnldhps6b56n8kmpximhm73q95a2b0a2lw")))

(define-public crate-seccomp-sys-0.1 (crate (name "seccomp-sys") (vers "0.1.1") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "0kbp3nkypgkm7938vdhrpw9g91li6b8dg7gh5r3w0m272y17iw7a")))

(define-public crate-seccomp-sys-0.1 (crate (name "seccomp-sys") (vers "0.1.2") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "1ylxk4ihmhm4n7gc5iiasygr4p56w5bdqy5zvn0z4p6j22qq4h0d")))

(define-public crate-seccomp-sys-0.1 (crate (name "seccomp-sys") (vers "0.1.3") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "1rkbiq1vd5d3cc4w5ql05pj1vbjqfi7zir02szwfm2khl15zgg75")))

(define-public crate-seccomp-tiny-0.1 (crate (name "seccomp-tiny") (vers "0.1.0") (deps (list (crate-dep (name "compiler_builtins") (req "^0.1.36") (features (quote ("compiler-builtins" "mem"))) (default-features #t) (kind 2)) (crate-dep (name "memoffset") (req "^0.5.6") (default-features #t) (kind 2)) (crate-dep (name "sc") (req "^0.2.3") (default-features #t) (kind 0)))) (hash "02rkn70z88qghhn59fzi1g228zidgk138zi2mkyyxzjbgk2lj5s7")))

(define-public crate-seccomp-tiny-0.1 (crate (name "seccomp-tiny") (vers "0.1.1") (deps (list (crate-dep (name "compiler_builtins") (req "^0.1.36") (features (quote ("compiler-builtins" "mem"))) (default-features #t) (kind 2)) (crate-dep (name "memoffset") (req "^0.5.6") (default-features #t) (kind 2)) (crate-dep (name "sc") (req "^0.2.3") (default-features #t) (kind 0)))) (hash "02kyllzi98sinss8vd47sd9hxw2bxq1lnqrc2xi2vm0nrc4macw7")))

(define-public crate-seccompiler-0.1 (crate (name "seccompiler") (vers "0.1.0") (hash "1dziqw3axiiqgskw5x0wksnlbjjqw1vjdsjq62gf7rjyhiqxyk93")))

(define-public crate-seccompiler-0.2 (crate (name "seccompiler") (vers "0.2.0") (deps (list (crate-dep (name "libc") (req "^0.2.39") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.27") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.9") (optional #t) (default-features #t) (kind 0)))) (hash "0s0fahfw7p38jb9rh6pcvrfqn8zi0v8hpws9xb6246qkl69147g0") (features (quote (("json" "serde" "serde_json"))))))

(define-public crate-seccompiler-0.3 (crate (name "seccompiler") (vers "0.3.0") (deps (list (crate-dep (name "libc") (req "^0.2.39") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.27") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.9") (optional #t) (default-features #t) (kind 0)))) (hash "0zh1i5m8r31p7l95xnl3ywlprbfym2v5af75ycpgx85kqbiparbg") (features (quote (("json" "serde" "serde_json"))))))

(define-public crate-seccompiler-0.4 (crate (name "seccompiler") (vers "0.4.0") (deps (list (crate-dep (name "libc") (req "^0.2.39") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.27") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.9") (optional #t) (default-features #t) (kind 0)))) (hash "1rg2nr757r4p6g0zd1k0y5dqy2kc7jw9filx11wa88gpvm6kwnil") (features (quote (("json" "serde" "serde_json"))))))

