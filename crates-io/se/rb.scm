(define-module (crates-io se rb) #:use-module (crates-io))

(define-public crate-serbia-0.1 (crate (name "serbia") (vers "0.1.0") (deps (list (crate-dep (name "quote") (req "^1.0.9") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.124") (features (quote ("derive"))) (default-features #t) (kind 2)) (crate-dep (name "serde_yaml") (req "^0.8.17") (default-features #t) (kind 2)) (crate-dep (name "syn") (req "^1.0.62") (features (quote ("full" "printing"))) (default-features #t) (kind 0)))) (hash "07iz72qsw53v57j8f7gb5v7066igvin1jnch6395wcsrhrhqhj7d")))

(define-public crate-serbia-0.1 (crate (name "serbia") (vers "0.1.1") (deps (list (crate-dep (name "proc-macro2") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.124") (features (quote ("derive"))) (default-features #t) (kind 2)) (crate-dep (name "serde_yaml") (req "^0.8.17") (default-features #t) (kind 2)) (crate-dep (name "syn") (req "^1") (features (quote ("full" "printing"))) (default-features #t) (kind 0)))) (hash "0xj1baxymcna1jdz04dnmdflarjrw8l6l0xsihbkxv9zhs29av4m")))

(define-public crate-serbia-0.2 (crate (name "serbia") (vers "0.2.0") (deps (list (crate-dep (name "proc-macro2") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.124") (features (quote ("derive"))) (default-features #t) (kind 2)) (crate-dep (name "serde_yaml") (req "^0.8.17") (default-features #t) (kind 2)) (crate-dep (name "syn") (req "^1") (features (quote ("full" "printing"))) (default-features #t) (kind 0)))) (hash "1chjnrahb65bb3rxks0cjf29vhf10mayyws8b2bca420gq3z6446") (yanked #t)))

(define-public crate-serbia-0.3 (crate (name "serbia") (vers "0.3.0") (deps (list (crate-dep (name "proc-macro2") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.124") (features (quote ("derive"))) (default-features #t) (kind 2)) (crate-dep (name "serde_yaml") (req "^0.8.17") (default-features #t) (kind 2)) (crate-dep (name "syn") (req "^1") (features (quote ("full" "printing"))) (default-features #t) (kind 0)))) (hash "19jf12041dv4gx7bkrb63ry2c8bsr08i54ll8ghbrhidbxhc0bqq") (yanked #t)))

(define-public crate-serbia-0.4 (crate (name "serbia") (vers "0.4.0") (deps (list (crate-dep (name "proc-macro2") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.124") (features (quote ("derive"))) (default-features #t) (kind 2)) (crate-dep (name "serde_yaml") (req "^0.8.17") (default-features #t) (kind 2)) (crate-dep (name "syn") (req "^1") (features (quote ("full" "printing"))) (default-features #t) (kind 0)))) (hash "13vmsa6b6qaj5xsncrp18vxmj39blfsaa13n57in7vwnhsdmr0hq") (yanked #t)))

(define-public crate-serbia-0.4 (crate (name "serbia") (vers "0.4.1") (deps (list (crate-dep (name "proc-macro2") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.124") (features (quote ("derive"))) (default-features #t) (kind 2)) (crate-dep (name "serde_json") (req "^1.0.64") (default-features #t) (kind 2)) (crate-dep (name "serde_yaml") (req "^0.8.17") (default-features #t) (kind 2)) (crate-dep (name "syn") (req "^1") (features (quote ("full" "printing"))) (default-features #t) (kind 0)))) (hash "1sxfxjxj64ini708imfn9hz9yj4idyyh8c38mijpnminmikfigs2")))

(define-public crate-serbia-0.4 (crate (name "serbia") (vers "0.4.2") (deps (list (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 2)) (crate-dep (name "proc-macro2") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.124") (features (quote ("derive"))) (default-features #t) (kind 2)) (crate-dep (name "serde_json") (req "^1.0.64") (default-features #t) (kind 2)) (crate-dep (name "serde_yaml") (req "^0.8.17") (default-features #t) (kind 2)) (crate-dep (name "syn") (req "^1") (features (quote ("full" "printing"))) (default-features #t) (kind 0)))) (hash "0fgwva5q44g34wn15557i1j86xdhs3nriqkinrysdflz24a60my1")))

(define-public crate-serbia-0.4 (crate (name "serbia") (vers "0.4.3") (deps (list (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 2)) (crate-dep (name "proc-macro2") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.124") (features (quote ("derive"))) (default-features #t) (kind 2)) (crate-dep (name "serde_json") (req "^1.0.64") (default-features #t) (kind 2)) (crate-dep (name "serde_yaml") (req "^0.8.17") (default-features #t) (kind 2)) (crate-dep (name "syn") (req "^1") (features (quote ("full" "printing"))) (default-features #t) (kind 0)))) (hash "05cd3zzzsqp06b574bsphp4kk09br31jdbk6n9cq1ssjrwv4hfcx")))

(define-public crate-serbo-mc-0.1 (crate (name "serbo-mc") (vers "0.1.0") (deps (list (crate-dep (name "copy_dir") (req "^0.1.2") (default-features #t) (kind 0)))) (hash "1fddlyjbgsanfgrcl3q4qn2jylscv6wd37kyjrh92p4jh8rn99mz")))

(define-public crate-serbuffer-0.1 (crate (name "serbuffer") (vers "0.1.0") (deps (list (crate-dep (name "bytes") (req "^0.5") (default-features #t) (kind 0)) (crate-dep (name "uuid") (req "^0.8") (features (quote ("serde" "v4"))) (default-features #t) (kind 2)))) (hash "0nbhk0ygrgq5fz2f8kspkmhn9rb53qz13s84fwwgl9xcgkwv88hh")))

(define-public crate-serbuffer-0.1 (crate (name "serbuffer") (vers "0.1.1") (deps (list (crate-dep (name "bytes") (req "^0.5") (default-features #t) (kind 0)) (crate-dep (name "uuid") (req "^0.8") (features (quote ("serde" "v4"))) (default-features #t) (kind 2)))) (hash "19jahq6bppgvj1fxy1kzd5m5gish1mx5s8hrzkwbf93c5wdfcd01")))

(define-public crate-serbuffer-1 (crate (name "serbuffer") (vers "1.0.0") (deps (list (crate-dep (name "bytes") (req "^0.5") (default-features #t) (kind 0)) (crate-dep (name "uuid") (req "^0.8") (features (quote ("serde" "v4"))) (default-features #t) (kind 2)))) (hash "0bdi4jcv8h03c1bhqyf83mxjb6n2nns0ind6r8mvrn0kd1jzdv3x")))

(define-public crate-serbuffer-1 (crate (name "serbuffer") (vers "1.1.0") (deps (list (crate-dep (name "bytes") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "uuid") (req "^0.8") (features (quote ("serde" "v4"))) (default-features #t) (kind 2)))) (hash "11i1j21yacbw62azcc3py559wakffajlxvwyvs26n2f4zh7mvxpq")))

(define-public crate-serbuffer-1 (crate (name "serbuffer") (vers "1.1.1") (deps (list (crate-dep (name "bytes") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "uuid") (req "^0.8") (features (quote ("serde" "v4"))) (default-features #t) (kind 2)))) (hash "1gfl18v66ph232yd5s33f8qib59y7zf35l8kyxlb4w3k1h7h7k7r")))

(define-public crate-serbuffer-1 (crate (name "serbuffer") (vers "1.2.0") (deps (list (crate-dep (name "bytes") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "uuid") (req "^0.8") (features (quote ("serde" "v4"))) (default-features #t) (kind 2)))) (hash "1b4svbmiqlkbvsxg9b2n47hhmlkxxhbi4cgl7nc20z02mzvhlzq1")))

(define-public crate-serbuffer-1 (crate (name "serbuffer") (vers "1.2.1") (deps (list (crate-dep (name "bytes") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "uuid") (req "^0.8") (features (quote ("serde" "v4"))) (default-features #t) (kind 2)))) (hash "0zh3gc90ibb6i5xh7147acviy4jadan0dq1br04bfpqh58l3n05q")))

(define-public crate-serbuffer-1 (crate (name "serbuffer") (vers "1.2.2") (deps (list (crate-dep (name "bytes") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "uuid") (req "^0.8") (features (quote ("serde" "v4"))) (default-features #t) (kind 2)))) (hash "074jp8dgxq632pbdl9dmr17b6w32b5p935kpqwgqhncnyldf3lq4")))

(define-public crate-serbuffer-1 (crate (name "serbuffer") (vers "1.2.3") (deps (list (crate-dep (name "bytes") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "uuid") (req "^0.8") (features (quote ("serde" "v4"))) (default-features #t) (kind 2)))) (hash "0ppvkcm8skapicqnm6ajhgxxbm500cx9500qi54wxayspkgyfln4")))

(define-public crate-serbuffer-1 (crate (name "serbuffer") (vers "1.2.4") (deps (list (crate-dep (name "bytes") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "uuid") (req "^0.8") (features (quote ("serde" "v4"))) (default-features #t) (kind 2)))) (hash "18zwpl5dcha8rpghd674vxprpp77bxx6vpm91yvjjx9lzq0dsb8b")))

(define-public crate-serbuffer-1 (crate (name "serbuffer") (vers "1.2.5") (deps (list (crate-dep (name "bytes") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "uuid") (req "^0.8") (features (quote ("serde" "v4"))) (default-features #t) (kind 2)))) (hash "0kz75sf4s54an6jxmjr3q4i2c7sznzfknyzr5jlcq5l0i44kfamx")))

(define-public crate-serbuffer-1 (crate (name "serbuffer") (vers "1.3.0") (deps (list (crate-dep (name "bytes") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "uuid") (req "^0.8") (features (quote ("serde" "v4"))) (default-features #t) (kind 2)))) (hash "0vjs8j6ywchhyjaq7dx1s28b16hsm49r96h3i5sgmnzydscizrf2")))

(define-public crate-serbuffer-1 (crate (name "serbuffer") (vers "1.3.1") (deps (list (crate-dep (name "bytes") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "uuid") (req "^0.8") (features (quote ("serde" "v4"))) (default-features #t) (kind 2)))) (hash "1z54kj6z3wgm9qn1ld3nmx373xh9arq1hjmrgbqiizd9yc3iapi7")))

(define-public crate-serbuffer-1 (crate (name "serbuffer") (vers "1.3.2") (deps (list (crate-dep (name "bytes") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "uuid") (req "^0.8") (features (quote ("serde" "v4"))) (default-features #t) (kind 2)))) (hash "1v0baa02c070ilbbjiwpkv9wrs4i6fg93zh5v0b3dzgms9psz1ia")))

(define-public crate-serbuffer-1 (crate (name "serbuffer") (vers "1.3.3") (deps (list (crate-dep (name "bytes") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "uuid") (req "^0.8") (features (quote ("serde" "v4"))) (default-features #t) (kind 2)))) (hash "1hksfpg7pgpjb29lcyp6sw3swmrznvyp6pycadr8s0nkbk48b6jj")))

(define-public crate-serbuffer-gen-0.1 (crate (name "serbuffer-gen") (vers "0.1.0") (hash "16qxnc69lz1gi66xrv948ygah7a3vkl74x2jqgzgpalsj68nmhvl")))

(define-public crate-serbuffer-gen-0.1 (crate (name "serbuffer-gen") (vers "0.1.1") (hash "01507dvcmjr99agraqiasvb29420s5zny2pcgapwcq8n7pypbjzs")))

(define-public crate-serbuffer-gen-0.1 (crate (name "serbuffer-gen") (vers "0.1.2") (hash "0df9knz5mwhrz6k6ffjp8z099wfbi4z5vc9529y7hza7dxnxs1c3")))

(define-public crate-serbuffer-gen-1 (crate (name "serbuffer-gen") (vers "1.0.0") (hash "0pm95vdgm3lk6i5bh40m7jz59cc1qqgyk7dv5vfcl8n09v3vb563")))

(define-public crate-serbuffer-gen-1 (crate (name "serbuffer-gen") (vers "1.0.1") (hash "0jlj90dmamzk4fd3pkm0w6nhvjnaxflb5wxplc2q4gvca7p01cx7")))

(define-public crate-serbuffer-gen-1 (crate (name "serbuffer-gen") (vers "1.2.0") (hash "09pikwipxd9q9dwimpn9m86r0w3ms6zq0zamyfmgzw1rw11ndmdl")))

(define-public crate-serbuffer-gen-1 (crate (name "serbuffer-gen") (vers "1.2.1") (hash "1959ra5i0m5pvkij6fvvy1yfd5xsh8mvaf665gm6278lvnj0zdn4")))

(define-public crate-serbuffer-gen-1 (crate (name "serbuffer-gen") (vers "1.2.2") (hash "0cfjk7xxiazl06f6r3b3ylvasm1bacvx01fbr5xwx810zilmslp4")))

(define-public crate-serbuffer-gen-1 (crate (name "serbuffer-gen") (vers "1.2.3") (hash "13hwviwfvfdirgj311jvvpiwf1rk88mmhs5na9zpa6f0jk2w25f8")))

(define-public crate-serbuffer-gen-1 (crate (name "serbuffer-gen") (vers "1.2.4") (hash "1fvhhb0wc921lrp77z13n5nfhgaasvnh3l8kdp39llhfkqsw5sr6")))

(define-public crate-serbuffer-gen-1 (crate (name "serbuffer-gen") (vers "1.2.5") (hash "1fc5rg627fjr78wlr9qssi8pd17301vx6sg5jgfmwvdhg80fhi1j")))

(define-public crate-serbuffer-gen-1 (crate (name "serbuffer-gen") (vers "1.2.6") (hash "11ffqyxfi4pv0h0c5hxq04s87kkn9lycwmj7ah3vnkqj9n9rh3ng")))

(define-public crate-serbuffer-gen-1 (crate (name "serbuffer-gen") (vers "1.3.0") (hash "1059c9hbjn0lh6wy9fpim75w9fj3qp2vh58n1wf14rqh5cdbnq39")))

(define-public crate-serbuffer-gen-1 (crate (name "serbuffer-gen") (vers "1.3.1") (hash "10asnfwh3vravdrnqc189wdx2qwfy36nyvqifqarc46m8fy1ajc9")))

(define-public crate-serbzip-0.1 (crate (name "serbzip") (vers "0.1.0") (deps (list (crate-dep (name "bincode") (req "^2.0.0-rc.1") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^3.2.16") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "home") (req "^0.5.3") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 2)) (crate-dep (name "reqwest") (req "^0.11") (features (quote ("blocking"))) (optional #t) (default-features #t) (kind 0)))) (hash "1n9gwb6j9qihla72ivi650908k86n0yrfjfq7icrv3fqlxwalvck") (features (quote (("build-binary" "clap" "home" "reqwest"))))))

(define-public crate-serbzip-0.2 (crate (name "serbzip") (vers "0.2.0") (deps (list (crate-dep (name "bincode") (req "^2.0.0-rc.1") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^3.2.16") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "home") (req "^0.5.3") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 2)) (crate-dep (name "reqwest") (req "^0.11") (features (quote ("blocking"))) (default-features #t) (kind 0)))) (hash "1ayg2gs7yi6lvy9y7vyc51ahpr3bfc46rz9n1564dmpyvscmgzig")))

(define-public crate-serbzip-0.3 (crate (name "serbzip") (vers "0.3.0") (deps (list (crate-dep (name "bincode") (req "^2.0.0-rc.1") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^3.2.16") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "home") (req "^0.5.3") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 2)) (crate-dep (name "reqwest") (req "^0.11") (features (quote ("blocking"))) (default-features #t) (kind 0)))) (hash "1nlwsmsl5gpy2xnyd31lqzb9n8x93v79xi8zyjij46dr8x6alklz")))

(define-public crate-serbzip-0.4 (crate (name "serbzip") (vers "0.4.0") (deps (list (crate-dep (name "bincode") (req "^2.0.0-rc.1") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^3.2.16") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "home") (req "^0.5.3") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 2)) (crate-dep (name "reqwest") (req "^0.11") (features (quote ("blocking"))) (default-features #t) (kind 0)))) (hash "1fa5fy8a5mvxkxajcrlmgcs0014gb5l685cg0295mswn3fch184s")))

(define-public crate-serbzip-0.5 (crate (name "serbzip") (vers "0.5.0") (deps (list (crate-dep (name "bincode") (req "^2.0.0-rc.1") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^3.2.16") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "home") (req "^0.5.3") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 2)) (crate-dep (name "reqwest") (req "^0.11") (features (quote ("blocking"))) (default-features #t) (kind 0)))) (hash "1rf4pax7bfi5d5qy916fba72i9ip7gmpxrn3r2m9dn0xnwl649kv")))

(define-public crate-serbzip-0.6 (crate (name "serbzip") (vers "0.6.0") (deps (list (crate-dep (name "clap") (req "^3.2.16") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "home") (req "^0.5.3") (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.11") (features (quote ("blocking"))) (default-features #t) (kind 0)) (crate-dep (name "serbzip-core") (req "^0.6.0") (default-features #t) (kind 0)))) (hash "098nm3wfri3xc7cddiyjjxdmja4dg3qg3shlz48k647ml7p3sgpc")))

(define-public crate-serbzip-0.7 (crate (name "serbzip") (vers "0.7.0") (deps (list (crate-dep (name "clap") (req "^3.2.16") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "home") (req "^0.5.3") (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.11") (features (quote ("blocking"))) (default-features #t) (kind 0)) (crate-dep (name "serbzip-core") (req "^0.7.0") (default-features #t) (kind 0)))) (hash "0940hkh8q3kfxy7zybgqkpwjdwh2z2h87hkx6in3waplkkklrw7g")))

(define-public crate-serbzip-0.8 (crate (name "serbzip") (vers "0.8.0") (deps (list (crate-dep (name "clap") (req "^3.2.16") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "home") (req "^0.5.3") (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.11") (features (quote ("blocking"))) (default-features #t) (kind 0)) (crate-dep (name "serbzip-core") (req "^0.8.0") (default-features #t) (kind 0)))) (hash "1wdgw7h090jnpyqxxm3rps4nx9fw7xp8ims9jkgkh8laqcnipcb6")))

(define-public crate-serbzip-0.9 (crate (name "serbzip") (vers "0.9.0") (deps (list (crate-dep (name "clap") (req "^3.2.16") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "home") (req "^0.5.3") (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.11") (features (quote ("blocking"))) (default-features #t) (kind 0)) (crate-dep (name "serbzip-core") (req "^0.9.0") (default-features #t) (kind 0)))) (hash "1kwryyfrcp0cslnyrwdjdrs9p7cf0jl0wv1gif8h42vn6zn0v6z1")))

(define-public crate-serbzip-0.10 (crate (name "serbzip") (vers "0.10.0") (deps (list (crate-dep (name "clap") (req "^3.2.16") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "home") (req "^0.5.3") (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.11") (features (quote ("blocking"))) (default-features #t) (kind 0)) (crate-dep (name "serbzip-core") (req "^0.10.0") (default-features #t) (kind 0)))) (hash "1c7awqi7krrq8whk48fvk135jcpgyb4rin38q796qjhi59qncw2g")))

(define-public crate-serbzip-core-0.5 (crate (name "serbzip-core") (vers "0.5.0") (deps (list (crate-dep (name "bincode") (req "^2.0.0-rc.1") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 2)))) (hash "0zgh8dchjbzxr2gbfsp0bq1n9zrnabbh8h0g9i1k0wvlwbjwkrnd")))

(define-public crate-serbzip-core-0.6 (crate (name "serbzip-core") (vers "0.6.0") (deps (list (crate-dep (name "bincode") (req "^2.0.0-rc.1") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 2)))) (hash "1i35drgafx5n3lgrvikzdjjf0wh4jh0y1q5zlj0c190wwdvjj0bk")))

(define-public crate-serbzip-core-0.7 (crate (name "serbzip-core") (vers "0.7.0") (deps (list (crate-dep (name "bincode") (req "^2.0.0-rc.1") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 2)))) (hash "12b27m4adjkjlikdh4lkk2qv289nqajwvd9w3myvfvhma99gypjc")))

(define-public crate-serbzip-core-0.8 (crate (name "serbzip-core") (vers "0.8.0") (deps (list (crate-dep (name "bincode") (req "^2.0.0-rc.1") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 2)))) (hash "0llx3z5wd94cwky0biiibmxznhfxnzxx5zmg7mwvg0b1dxrs6jkp")))

(define-public crate-serbzip-core-0.9 (crate (name "serbzip-core") (vers "0.9.0") (deps (list (crate-dep (name "bincode") (req "^2.0.0-rc.1") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 2)))) (hash "039flh0wv38141l691wb2gq3z53v74dkazlaipn8z5g32wb1qnjl")))

(define-public crate-serbzip-core-0.10 (crate (name "serbzip-core") (vers "0.10.0") (deps (list (crate-dep (name "bincode") (req "^2.0.0-rc.1") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 2)))) (hash "0fw1jqpffr452gznk450ly0vdgqmdywxmqyx8dkz102704636imj")))

