(define-module (crates-io se xt) #:use-module (crates-io))

(define-public crate-sext-0.1 (crate (name "sext") (vers "0.1.0") (deps (list (crate-dep (name "env_logger") (req "^0.9.1") (default-features #t) (kind 0)) (crate-dep (name "fontdue") (req "^0.7.2") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.17") (default-features #t) (kind 0)))) (hash "1mvcz12g1mdkknpbkkplf8h77ix7srhh4l16qyfrsk9cjx1fww38") (yanked #t)))

(define-public crate-sext-0.1 (crate (name "sext") (vers "0.1.1") (deps (list (crate-dep (name "env_logger") (req "^0.9.1") (default-features #t) (kind 0)) (crate-dep (name "fontdue") (req "^0.7.2") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.17") (default-features #t) (kind 0)))) (hash "136kmmhps8hklgh3xbb1z24qb2f9bwcsbr88qd41dx04ga3rx8n5")))

