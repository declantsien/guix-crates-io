(define-module (crates-io se tv) #:use-module (crates-io))

(define-public crate-setver-0.1 (crate (name "setver") (vers "0.1.0") (hash "1bkzvx8kk6hf9vf8llb6lr61ia1pv54dwpcxlylaifc72wbf5l95")))

(define-public crate-setver-0.2 (crate (name "setver") (vers "0.2.0") (hash "1ianjl81agrnhnb39dbqvnz75y2mpf2rrl60r2i01pyd76qs1c0x")))

(define-public crate-setver-0.3 (crate (name "setver") (vers "0.3.0") (hash "16r56p9dpwc36083r19rbzff20af0hmh7sz2m43ck3v4rkcbi1ph")))

