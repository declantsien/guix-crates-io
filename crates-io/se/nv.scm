(define-module (crates-io se nv) #:use-module (crates-io))

(define-public crate-senv-0.1 (crate (name "senv") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^4") (features (quote ("std" "help" "usage" "derive"))) (kind 0)) (crate-dep (name "either") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "pest") (req "^2") (default-features #t) (kind 0)) (crate-dep (name "pest_derive") (req "^2") (default-features #t) (kind 0)))) (hash "03aj3xmcg1s2l94r7kdhwsixff01r3cdppd2w7lzrwvvzqjczapz")))

(define-public crate-senv-0.2 (crate (name "senv") (vers "0.2.0") (deps (list (crate-dep (name "clap") (req "^4") (features (quote ("std" "help" "usage" "derive"))) (kind 0)) (crate-dep (name "either") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "pest") (req "^2") (default-features #t) (kind 0)) (crate-dep (name "pest_derive") (req "^2") (default-features #t) (kind 0)))) (hash "15zd61wzfsvynhzw7r1r4n4p9kmrhg6xvm1ny7afc9xv3z4ygn6d")))

