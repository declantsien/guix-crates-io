(define-module (crates-io se ym) #:use-module (crates-io))

(define-public crate-seymour-protocol-0.1 (crate (name "seymour-protocol") (vers "0.1.0") (deps (list (crate-dep (name "thiserror") (req "^1.0") (default-features #t) (kind 0)))) (hash "0imc56xxgn2gdl47gjswy49j1mwqijhl1w0f09bz3nj5gazrs82f")))

(define-public crate-seymour-protocol-0.1 (crate (name "seymour-protocol") (vers "0.1.1") (deps (list (crate-dep (name "thiserror") (req "^1.0") (default-features #t) (kind 0)))) (hash "10rkw7bglmnhh7mzgw2gs1ymkndkdhqla52p0j7dq6bgdcq4x8lp")))

(define-public crate-seymour-protocol-0.1 (crate (name "seymour-protocol") (vers "0.1.2") (deps (list (crate-dep (name "thiserror") (req "^1.0") (default-features #t) (kind 0)))) (hash "0czm9bya5ia3bdmnsl6bhv79c6d52khb4f3dlqbqrxsvfnzdb5ny")))

(define-public crate-seymour-protocol-0.1 (crate (name "seymour-protocol") (vers "0.1.3") (deps (list (crate-dep (name "thiserror") (req "^1.0") (default-features #t) (kind 0)))) (hash "1a1799nzljg3zqcb1r1yn277jdbplchvx1201fm22r0vcjavpagr")))

(define-public crate-seymour-protocol-0.1 (crate (name "seymour-protocol") (vers "0.1.4") (deps (list (crate-dep (name "thiserror") (req "^1.0") (default-features #t) (kind 0)))) (hash "14wh6gagriyf6cyaz0x2600grkryakwxqzclrl49qpa3avv8aaqz")))

