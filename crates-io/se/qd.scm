(define-module (crates-io se qd) #:use-module (crates-io))

(define-public crate-seqdiff-0.1 (crate (name "seqdiff") (vers "0.1.0") (deps (list (crate-dep (name "quickcheck") (req "^0.9.2") (default-features #t) (kind 2)) (crate-dep (name "quickcheck_macros") (req "^0.9.1") (default-features #t) (kind 2)))) (hash "03w9pbnw7c96q8djb40miplp0izb503mcpmbw6vb9f4ixa41sg3h")))

(define-public crate-seqdiff-0.1 (crate (name "seqdiff") (vers "0.1.2") (deps (list (crate-dep (name "quickcheck") (req "^0.9.2") (default-features #t) (kind 2)) (crate-dep (name "quickcheck_macros") (req "^0.9.1") (default-features #t) (kind 2)))) (hash "0yplvnvq3b57drziwykrfqyhyafc377818ydg8x3ryx1rnnnab56")))

(define-public crate-seqdiff-0.1 (crate (name "seqdiff") (vers "0.1.3") (deps (list (crate-dep (name "quickcheck") (req "^0.9.2") (default-features #t) (kind 2)) (crate-dep (name "quickcheck_macros") (req "^0.9.1") (default-features #t) (kind 2)))) (hash "0wn4s2zwix8h0ql1gjiinfshsgcsn3z9mg1g5wnqnx6dxyvqi17m")))

(define-public crate-seqdiff-0.1 (crate (name "seqdiff") (vers "0.1.4") (deps (list (crate-dep (name "quickcheck") (req "^0.9.2") (default-features #t) (kind 2)) (crate-dep (name "quickcheck_macros") (req "^0.9.1") (default-features #t) (kind 2)))) (hash "1rcqrawisi118pcsfhl68ssc07jq7yy2x7g5bl6kfk14llshi67f")))

(define-public crate-seqdiff-0.2 (crate (name "seqdiff") (vers "0.2.1") (deps (list (crate-dep (name "criterion") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "quickcheck") (req "^0.9.2") (default-features #t) (kind 2)) (crate-dep (name "quickcheck_macros") (req "^0.9.1") (default-features #t) (kind 2)) (crate-dep (name "rstest") (req "^0.6.4") (default-features #t) (kind 2)))) (hash "0p8ssnybqqz9k88igg89bvwbxxx43jnw9j5aiwb0wihf6bmwlsc7")))

(define-public crate-seqdiff-0.2 (crate (name "seqdiff") (vers "0.2.3") (deps (list (crate-dep (name "criterion") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "quickcheck") (req "^0.9.2") (default-features #t) (kind 2)) (crate-dep (name "quickcheck_macros") (req "^0.9.1") (default-features #t) (kind 2)) (crate-dep (name "rstest") (req "^0.6.4") (default-features #t) (kind 2)))) (hash "0msa7pahzm83z3nkbkdhrlli5iz2xlc3sab4g5xvlx75s70nrmcg")))

(define-public crate-seqdiff-0.2 (crate (name "seqdiff") (vers "0.2.4") (deps (list (crate-dep (name "criterion") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "quickcheck") (req "^0.9.2") (default-features #t) (kind 2)) (crate-dep (name "quickcheck_macros") (req "^0.9.1") (default-features #t) (kind 2)) (crate-dep (name "rstest") (req "^0.6.4") (default-features #t) (kind 2)))) (hash "1bq6k7hx0msf04vdd2jag96r6sl8vpyfp521lpvcyfxzf5alr4y5")))

(define-public crate-seqdiff-0.3 (crate (name "seqdiff") (vers "0.3.0") (deps (list (crate-dep (name "criterion") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "quickcheck") (req "^0.9.2") (default-features #t) (kind 2)) (crate-dep (name "quickcheck_macros") (req "^0.9.1") (default-features #t) (kind 2)) (crate-dep (name "rstest") (req "^0.6.4") (default-features #t) (kind 2)))) (hash "1pxvvgcz1d2rv3rx9v0aqd8d61wvi6crz057bbx6dlp6mrbfsn70")))

(define-public crate-seqdupes-0.1 (crate (name "seqdupes") (vers "0.1.0") (deps (list (crate-dep (name "bio") (req "^0.41.0") (default-features #t) (kind 0)) (crate-dep (name "chrono") (req "^0.4.19") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^3.0.12") (default-features #t) (kind 0)) (crate-dep (name "itertools") (req "^0.10.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.9.0") (default-features #t) (kind 0)) (crate-dep (name "rustc-hash") (req "^1.1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.62") (default-features #t) (kind 0)) (crate-dep (name "tera") (req "^1.6.1") (default-features #t) (kind 0)))) (hash "1a9q8zqkhl2w0ba57bn1k5hkdf49ypf1lskcfq9f6ifqj00cg6mk")))

