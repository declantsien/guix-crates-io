(define-module (crates-io se rc) #:use-module (crates-io))

(define-public crate-serch-0.1 (crate (name "serch") (vers "0.1.0") (deps (list (crate-dep (name "jwalk") (req "^0.8.1") (default-features #t) (kind 0)) (crate-dep (name "termcolor") (req "^1.4.1") (default-features #t) (kind 0)))) (hash "14i88q0vsk2ays5z38anrjdjv2ncpg0n36cxcfdf493fjgwx9wgx")))

(define-public crate-serch-0.1 (crate (name "serch") (vers "0.1.1") (deps (list (crate-dep (name "jwalk") (req "^0.8.1") (default-features #t) (kind 0)) (crate-dep (name "termcolor") (req "^1.4.1") (default-features #t) (kind 0)))) (hash "152nw36cc5vjcf7klkc89v2r2grpwmwhj19prnn1jvvgj5vvg5zb")))

