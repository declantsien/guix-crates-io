(define-module (crates-io se pa) #:use-module (crates-io))

(define-public crate-separable-0.1 (crate (name "separable") (vers "0.1.0") (deps (list (crate-dep (name "separable-derive") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "0m3jbl7qss82w4xbgblc9lwkak1bd8f6iw5iwq3myvw3j1w6paj5")))

(define-public crate-separable-0.2 (crate (name "separable") (vers "0.2.0") (deps (list (crate-dep (name "separable-derive") (req "^0.2.0") (default-features #t) (kind 0)))) (hash "1k4wy60lzwlnnzz3vi6m16r66g45ayqn89yxri030bcb6n1wm801")))

(define-public crate-separable-0.2 (crate (name "separable") (vers "0.2.1") (deps (list (crate-dep (name "separable-derive") (req "^0.2.1") (default-features #t) (kind 0)))) (hash "080z6bw329x71z5m1gbvn3nsb9h35chjx4hdi233hiryg55wjkg3")))

(define-public crate-separable-derive-0.1 (crate (name "separable-derive") (vers "0.1.0") (deps (list (crate-dep (name "proc-macro2") (req "^1.0.40") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.20") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.98") (default-features #t) (kind 0)))) (hash "19zjl0yb8mifwyicvzxz6227ab5kww45hyagcbxkldpxb9qf9rl3")))

(define-public crate-separable-derive-0.2 (crate (name "separable-derive") (vers "0.2.0") (deps (list (crate-dep (name "proc-macro2") (req "^1.0.40") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.20") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.98") (default-features #t) (kind 0)))) (hash "1jzsdwqivaazkwylyij7qi7k8g9iwwwkqwjgvfvzjav3p45b5apv")))

(define-public crate-separable-derive-0.2 (crate (name "separable-derive") (vers "0.2.1") (deps (list (crate-dep (name "proc-macro2") (req "^1.0.40") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.20") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.98") (default-features #t) (kind 0)))) (hash "1dcap56qj02ygp35dflayqfpq1za9y30p002i2yqq66w1b6i6nnv")))

(define-public crate-separator-0.1 (crate (name "separator") (vers "0.1.0") (hash "0d80la7qgcpi481jjmx9zvb8qqbbdk9ammgdc6dn7ghbjbn4f18k")))

(define-public crate-separator-0.2 (crate (name "separator") (vers "0.2.0") (hash "0qk7rpgg1ybmx49h129kiyj4yi7vzga7mf4x2dfvz1yawlz84bqb")))

(define-public crate-separator-0.3 (crate (name "separator") (vers "0.3.0") (hash "1dz34x3568v0jhq329mkps50wf1ggvv32v4yih4pq1m28qpasdz2")))

(define-public crate-separator-0.3 (crate (name "separator") (vers "0.3.1") (hash "0ay3ixpbal576ph34a5sqnih12wc16l5vpqig4pd0blmiy0lvk3s")))

(define-public crate-separator-0.4 (crate (name "separator") (vers "0.4.0") (hash "0nf29j2vxcd4k13wbdfhq2rdp8dvxjnlvshhdfgf4haiyb7y8ia8")))

(define-public crate-separator-0.4 (crate (name "separator") (vers "0.4.1") (hash "1r9cfwknv4p4v5ryd204wqk5j85742ibkcz7sby41w7f8ykl2y7r")))

(define-public crate-sepax2d-0.1 (crate (name "sepax2d") (vers "0.1.0") (hash "1c889y84fr7a98nr2fdpjcjqdcbnckzq2wwgqj6iyr5m968dfzp4")))

(define-public crate-sepax2d-0.2 (crate (name "sepax2d") (vers "0.2.0") (hash "07fkxws30xyj6gfd7fpmxqrxsn3p2pgf97m9gpwx8djga7nmkjn1")))

(define-public crate-sepax2d-0.3 (crate (name "sepax2d") (vers "0.3.0") (deps (list (crate-dep (name "ggez") (req "^0.7") (default-features #t) (kind 2)))) (hash "00862qav2ydckmglwilsfc850x183ai6pmdiwhmnlbqc8218391z")))

(define-public crate-sepax2d-0.3 (crate (name "sepax2d") (vers "0.3.1") (deps (list (crate-dep (name "ggez") (req "^0.7") (default-features #t) (kind 2)))) (hash "0gi5gljpzz6igmnhwkak2af4mmn6cg5igy6kaqdj929dfplaj5lv")))

(define-public crate-sepax2d-0.3 (crate (name "sepax2d") (vers "0.3.2") (deps (list (crate-dep (name "ggez") (req "^0.7") (default-features #t) (kind 2)))) (hash "15i67kzmmzwj1jixjymadr1apkb1lbq0ra30bpzv0as453jfg58d")))

(define-public crate-sepax2d-0.3 (crate (name "sepax2d") (vers "0.3.3") (deps (list (crate-dep (name "ggez") (req "^0.7") (default-features #t) (kind 2)))) (hash "0r38c3xpmdi7sypa0khyf14ff1ljx523zfrcqjkq3518s0ayprca")))

(define-public crate-sepax2d-0.3 (crate (name "sepax2d") (vers "0.3.4") (deps (list (crate-dep (name "ggez") (req "^0.7") (default-features #t) (kind 2)))) (hash "1yygppm85hnsjpia35bk539fv1c0bcy8lxayyln3awl22599pnfi")))

(define-public crate-sepax2d-0.3 (crate (name "sepax2d") (vers "0.3.5") (deps (list (crate-dep (name "ggez") (req "^0.7") (default-features #t) (kind 2)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)))) (hash "1mzmqnqqc9gxf3fp6b016nlxgnjiahj286lixwxp9zrg8ljaqjab") (v 2) (features2 (quote (("serde" "dep:serde"))))))

(define-public crate-sepax2d-0.3 (crate (name "sepax2d") (vers "0.3.6") (deps (list (crate-dep (name "ggez") (req "^0.7") (default-features #t) (kind 2)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)))) (hash "02yfygkdlz3zkn6n2gcgr966n9lccs2hk4h204nawlryglc49zag") (v 2) (features2 (quote (("serde" "dep:serde"))))))

(define-public crate-sepax2d-0.3 (crate (name "sepax2d") (vers "0.3.7") (deps (list (crate-dep (name "ggez") (req "^0.7") (default-features #t) (kind 2)) (crate-dep (name "ron") (req "^0.7") (default-features #t) (kind 2)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)))) (hash "0v2qapiaxa5n3ya4rvfik9fa2czrgh0rzlyq9b7jh7c17gkz586q") (v 2) (features2 (quote (("serde" "dep:serde"))))))

(define-public crate-sepax2d-0.3 (crate (name "sepax2d") (vers "0.3.8") (deps (list (crate-dep (name "ggez") (req "^0.7") (default-features #t) (kind 2)) (crate-dep (name "ron") (req "^0.7") (default-features #t) (kind 2)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)))) (hash "083pkqjlakrk9vfbmvbfgmc6q6wayz9pr21p6wbpvpmc9543499r") (v 2) (features2 (quote (("serde" "dep:serde"))))))

