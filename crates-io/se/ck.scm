(define-module (crates-io se ck) #:use-module (crates-io))

(define-public crate-seckey-0.1 (crate (name "seckey") (vers "0.1.0") (deps (list (crate-dep (name "memsec") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "nix") (req "^0.6") (default-features #t) (kind 2)))) (hash "1ryh51frnxwmp6flb6cv2hyb2kvqi408mvss343ihpfmmkhhprlh")))

(define-public crate-seckey-0.1 (crate (name "seckey") (vers "0.1.1") (deps (list (crate-dep (name "memsec") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "nix") (req "^0.6") (default-features #t) (kind 2)))) (hash "0q9jx8qk1mrqq8za7827npfkqjzfyizmssad1cxqn4r1n5y7haj1")))

(define-public crate-seckey-0.1 (crate (name "seckey") (vers "0.1.2") (deps (list (crate-dep (name "memsec") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "nix") (req "^0.6") (default-features #t) (kind 2)))) (hash "1mlvjjyz9lc2vx1fri3gicbggl6az4aqnmhd280px07kln2ibbim")))

(define-public crate-seckey-0.2 (crate (name "seckey") (vers "0.2.0") (deps (list (crate-dep (name "memsec") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "nix") (req "^0.6") (default-features #t) (kind 2)))) (hash "0q91cgchx63njb8jjz92lkykrg216800v6vh6n5y4l3i4ynkcmrz")))

(define-public crate-seckey-0.2 (crate (name "seckey") (vers "0.2.1") (deps (list (crate-dep (name "memsec") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "nix") (req "^0.6") (default-features #t) (kind 2)))) (hash "14sqs4sjvi51vgrs06cdy14a4cpka63sjcl04aj2p39fm3igpybx")))

(define-public crate-seckey-0.2 (crate (name "seckey") (vers "0.2.2") (deps (list (crate-dep (name "memsec") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "nix") (req "^0.6") (default-features #t) (kind 2)))) (hash "1731yxpkz62b2qqyv6gsyxbx5y7r5hvdak6yxdal3ridzyxc0sig")))

(define-public crate-seckey-0.2 (crate (name "seckey") (vers "0.2.3") (deps (list (crate-dep (name "memsec") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "nix") (req "^0.6") (default-features #t) (kind 2)))) (hash "1adnp3ckf04n65j0zl2a1aprw9kvw4pss7d83klhayv8g3vk2fv3")))

(define-public crate-seckey-0.2 (crate (name "seckey") (vers "0.2.4") (deps (list (crate-dep (name "memsec") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "nix") (req "^0.7") (default-features #t) (kind 2)))) (hash "1i0dc9iy8ampjxr0d5ds5mw4mmdyk1csgynxzrp40n3a5nz0q5zh")))

(define-public crate-seckey-0.2 (crate (name "seckey") (vers "0.2.5") (deps (list (crate-dep (name "memsec") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "nix") (req "^0.7") (default-features #t) (kind 2)))) (hash "1aj9x4x6nyxpq5kq7ibyprjvf00bl01cr4zlrdpszzwr77sgly5n")))

(define-public crate-seckey-0.2 (crate (name "seckey") (vers "0.2.6") (deps (list (crate-dep (name "memsec") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "nix") (req "^0.7") (default-features #t) (kind 2)))) (hash "1ln2vn7mc843zqwmsd5sjnhc7p3r0ms960qj1bbz1yjcd3a47lic")))

(define-public crate-seckey-0.3 (crate (name "seckey") (vers "0.3.0") (deps (list (crate-dep (name "memsec") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "nix") (req "^0.7") (default-features #t) (kind 2)))) (hash "1mdm1qd0nrmvxy9xnq58vyxvmhf4wfkfpzlmzcnf9mgpmyarkqgj")))

(define-public crate-seckey-0.4 (crate (name "seckey") (vers "0.4.0") (deps (list (crate-dep (name "memsec") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "nix") (req "^0.7") (default-features #t) (kind 2)))) (hash "0vr77iip5cq4cg16szpf1d7sgw6s2cly16xcqhhhd5x6pf0zz9ay") (features (quote (("place"))))))

(define-public crate-seckey-0.4 (crate (name "seckey") (vers "0.4.1") (deps (list (crate-dep (name "memsec") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "nix") (req "^0.7") (default-features #t) (kind 2)))) (hash "00ggs1vr82li736j3i50yg20b4zg12yin2pg6fwl8s68cvr1yy9y") (features (quote (("place"))))))

(define-public crate-seckey-0.4 (crate (name "seckey") (vers "0.4.2") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "memsec") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "nix") (req "^0.7") (default-features #t) (kind 2)))) (hash "11axsvvinzddq4cz0dyw6v3c5bmc4w0z2dgl17flv42rx6qq1gdf") (features (quote (("place")))) (yanked #t)))

(define-public crate-seckey-0.4 (crate (name "seckey") (vers "0.4.3") (deps (list (crate-dep (name "memsec") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "nix") (req "^0.7") (default-features #t) (kind 2)))) (hash "1h9wgfqflanc6ajgbck96xd3r8cf93vcs1mchdbvnmbmv52wm7bk") (features (quote (("place")))) (yanked #t)))

(define-public crate-seckey-0.5 (crate (name "seckey") (vers "0.5.0") (deps (list (crate-dep (name "memsec") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "nix") (req "^0.7") (default-features #t) (kind 2)))) (hash "189j75sqvag8m3z8hfg8g1cs1lza86y242714x85xjw7ir9g4dhj")))

(define-public crate-seckey-0.5 (crate (name "seckey") (vers "0.5.1") (deps (list (crate-dep (name "memsec") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "nix") (req "^0.7") (default-features #t) (kind 2)) (crate-dep (name "nodrop") (req "^0.1") (default-features #t) (kind 0)))) (hash "1v9yvczg5blf0cyhhzd46m1fvw488dp21wa66kczqmp2rs31f8f2")))

(define-public crate-seckey-0.5 (crate (name "seckey") (vers "0.5.2") (deps (list (crate-dep (name "memsec") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "nix") (req "^0.7") (default-features #t) (kind 2)) (crate-dep (name "nodrop") (req "^0.1") (default-features #t) (kind 0)))) (hash "1aim9qvvwzl6al1i64zy865a9iq7y3in95a8j6sbkjz00lmpazzf")))

(define-public crate-seckey-0.5 (crate (name "seckey") (vers "0.5.3") (deps (list (crate-dep (name "memsec") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "nix") (req "^0.7") (default-features #t) (kind 2)) (crate-dep (name "nodrop") (req "^0.1") (optional #t) (default-features #t) (kind 0)))) (hash "1j6sxgx22k5wq1q7n1q13lhfq5yknj2yxdvjpfzwcgvli05ddllj") (features (quote (("default" "nodrop"))))))

(define-public crate-seckey-0.6 (crate (name "seckey") (vers "0.6.0") (deps (list (crate-dep (name "memsec") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "nix") (req "^0.8") (default-features #t) (kind 2)) (crate-dep (name "nodrop") (req "^0.1") (optional #t) (default-features #t) (kind 0)))) (hash "0x0s3vvxqn7rf4vingk648r1ad9gschirlfqb5cxqynys4dxr7gn") (features (quote (("default" "nodrop"))))))

(define-public crate-seckey-0.6 (crate (name "seckey") (vers "0.6.1") (deps (list (crate-dep (name "memsec") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "nix") (req "^0.9") (default-features #t) (kind 2)))) (hash "0hs38vfp259dqkzln8s2lmwllzq4r1znc67ww81c5i6glx6dnkww")))

(define-public crate-seckey-0.7 (crate (name "seckey") (vers "0.7.0") (deps (list (crate-dep (name "memsec") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "nix") (req "^0.9") (default-features #t) (kind 2)))) (hash "1rqrhxqwk5k9hi3zc1f90djdffal63aifsqmpi1bfxv4kw8kib36")))

(define-public crate-seckey-0.7 (crate (name "seckey") (vers "0.7.1") (deps (list (crate-dep (name "memsec") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "nix") (req "^0.9") (default-features #t) (kind 2)))) (hash "1m1g8ahf9lv13ilkna7hvvg84s8yz5xgm1nq3gc97xv793jyh99c")))

(define-public crate-seckey-0.7 (crate (name "seckey") (vers "0.7.2") (deps (list (crate-dep (name "memsec") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "nix") (req "^0.9") (default-features #t) (kind 2)))) (hash "119q2pjnkyvkxg2f656p91cd6rfr39fwyb0k7swgg93m72w7bzkv")))

(define-public crate-seckey-0.7 (crate (name "seckey") (vers "0.7.3") (deps (list (crate-dep (name "memsec") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "nix") (req "^0.9") (default-features #t) (kind 2)))) (hash "0n5vddz45iajcgwvrxm5rz1fvnz8dikwlrzvg6wk416gz5n9nspn")))

(define-public crate-seckey-0.7 (crate (name "seckey") (vers "0.7.4") (deps (list (crate-dep (name "memsec") (req "^0.4") (kind 0)))) (hash "0k3n2m2zc7s9zc0107y8s2hxq0s9k0y7ms35k4hx37ssza11p5i5") (features (quote (("use_std" "memsec/alloc") ("default" "use_std"))))))

(define-public crate-seckey-0.7 (crate (name "seckey") (vers "0.7.5") (deps (list (crate-dep (name "memsec") (req "^0.4") (kind 0)))) (hash "0kywf4q1sdvc1fd7wsb7d6ldk5rhbrdprwb2pz88g2hvy8bsgjln") (features (quote (("use_std" "memsec/alloc") ("nightly" "memsec/nightly") ("default" "use_std"))))))

(define-public crate-seckey-0.7 (crate (name "seckey") (vers "0.7.6") (deps (list (crate-dep (name "memsec") (req "^0.4") (kind 0)))) (hash "0np9hv18c19bixynf106zq0xqcm1jg8ijwaz6wvcq6csb7z3a5f7") (features (quote (("use_std" "memsec/alloc") ("nightly" "memsec/nightly") ("default" "use_std"))))))

(define-public crate-seckey-0.7 (crate (name "seckey") (vers "0.7.7") (deps (list (crate-dep (name "memsec") (req "^0.4") (kind 0)))) (hash "03s0wywr0hy5ks4y9k7ghnx60vriblfknxzbzzgg40plwzprs2pw") (features (quote (("use_std" "memsec/alloc") ("nightly" "memsec/nightly") ("default" "use_std"))))))

(define-public crate-seckey-0.7 (crate (name "seckey") (vers "0.7.8") (deps (list (crate-dep (name "memsec") (req "^0.4") (kind 0)))) (hash "03b7wmafvz0yjinmbpngdik1lwcpc7l5lkx7ifjd09d3chiqbara") (features (quote (("use_std" "memsec/alloc") ("nightly" "memsec/nightly") ("default" "use_std"))))))

(define-public crate-seckey-0.7 (crate (name "seckey") (vers "0.7.9") (deps (list (crate-dep (name "memsec") (req "^0.4") (kind 0)))) (hash "1iadzjmrpdd08s1bvdgfr8j8kqzn4gnnmqn7dqm1qhpg3g93lykn") (features (quote (("use_std" "memsec/alloc") ("nightly" "memsec/nightly") ("default" "use_std"))))))

(define-public crate-seckey-0.8 (crate (name "seckey") (vers "0.8.0") (deps (list (crate-dep (name "memsec") (req "^0.4") (kind 0)))) (hash "08dl6i18rwnsc9c8mdzs5gqip25zlk50li7bl9c8h98drwqsp60g") (features (quote (("use_std" "memsec/alloc") ("nightly" "memsec/nightly") ("default" "use_std"))))))

(define-public crate-seckey-0.9 (crate (name "seckey") (vers "0.9.0") (deps (list (crate-dep (name "memsec") (req "^0.5") (kind 0)))) (hash "1fqwybbyc55n5bi4yids64saxa0s9mvdwd8idmccbbmlwfdq45lk") (features (quote (("use_std" "memsec/alloc" "memsec/use_os") ("nightly" "memsec/nightly") ("default" "use_std"))))))

(define-public crate-seckey-0.10 (crate (name "seckey") (vers "0.10.0") (deps (list (crate-dep (name "memsec") (req "^0.5") (kind 0)))) (hash "1rmhll9slhzh84d519570jkvma658vzcapla6277bmb6vg0xlfyh") (features (quote (("use_std" "memsec/alloc" "memsec/use_os") ("nightly" "memsec/nightly") ("default" "use_std")))) (yanked #t)))

(define-public crate-seckey-0.10 (crate (name "seckey") (vers "0.10.1") (deps (list (crate-dep (name "memsec") (req "^0.5") (kind 0)))) (hash "18af30sg8rpn5dprlks3817v0rz4q9z9d0cgvyyq8wdmyilj7zvp") (features (quote (("use_std" "memsec/alloc" "memsec/use_os") ("nightly" "memsec/nightly") ("default" "use_std")))) (yanked #t)))

(define-public crate-seckey-0.9 (crate (name "seckey") (vers "0.9.1") (deps (list (crate-dep (name "memsec") (req "^0.5") (kind 0)))) (hash "0drjw76asj659l1f6zgrh527abr4jbwm39jmxvj24xnvk6kd06f8") (features (quote (("use_std" "memsec/alloc" "memsec/use_os") ("nightly" "memsec/nightly") ("default" "use_std"))))))

(define-public crate-seckey-0.9 (crate (name "seckey") (vers "0.9.2") (deps (list (crate-dep (name "memsec") (req "^0.5.7") (kind 0)))) (hash "0i7rschf4lzb52qzr88cr1y9sdyxq5p7vba3vlqvghxwvlmch1w7") (features (quote (("use_std" "memsec/alloc" "memsec/use_os") ("nightly" "memsec/nightly") ("default" "use_std"))))))

(define-public crate-seckey-0.9 (crate (name "seckey") (vers "0.9.3") (deps (list (crate-dep (name "memsec") (req "^0.5.7") (kind 0)))) (hash "05qn59pgzn2yf5rdhxvhlnlvwji9da41y3mf5yiqgpnj90qsy9gx") (features (quote (("use_std" "memsec/alloc" "memsec/use_os") ("nightly" "memsec/nightly") ("default" "use_std"))))))

(define-public crate-seckey-0.11 (crate (name "seckey") (vers "0.11.0") (deps (list (crate-dep (name "getrandom") (req "^0.1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "memsec") (req "^0.6.0") (kind 0)))) (hash "148y03my2y1wm7vxy971ghyzagdsccs9gn66h32vr3d7w9pb36c9") (features (quote (("use_std") ("use_os" "getrandom" "use_std" "memsec/alloc" "memsec/use_os") ("nightly" "memsec/nightly") ("default" "use_os"))))))

(define-public crate-seckey-0.11 (crate (name "seckey") (vers "0.11.1") (deps (list (crate-dep (name "getrandom") (req "^0.1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "memsec") (req "^0.6.0") (kind 0)))) (hash "18maayszh25q03j6cg2pvj10vp7c1165xi4aaqk4bd1scwbb8y7k") (features (quote (("use_std") ("use_os" "getrandom" "use_std" "memsec/alloc" "memsec/use_os") ("nightly" "memsec/nightly") ("default" "use_os"))))))

(define-public crate-seckey-0.11 (crate (name "seckey") (vers "0.11.2") (deps (list (crate-dep (name "getrandom") (req "^0.1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "memsec") (req "^0.6.0") (kind 0)))) (hash "1fqgg5kirk9fhaj7ilrvcnzarxdnxk5sl7dgfwrd2dk6wjip3crk") (features (quote (("use_std") ("use_os" "getrandom" "use_std" "memsec/alloc" "memsec/use_os") ("nightly" "memsec/nightly") ("default" "use_os"))))))

(define-public crate-seckey-0.12 (crate (name "seckey") (vers "0.12.0") (deps (list (crate-dep (name "memsec") (req "^0.6.0") (kind 0)))) (hash "0qn3hk7z2bc5mgvwsb76ymgd1jdxirb078k460phi0pl24x2xj44") (features (quote (("use_std") ("use_os" "use_std" "memsec/alloc" "memsec/use_os") ("nightly" "memsec/nightly") ("default" "use_os")))) (yanked #t)))

(define-public crate-seckey-0.12 (crate (name "seckey") (vers "0.12.1") (deps (list (crate-dep (name "memsec") (req "^0.6.0") (kind 0)))) (hash "10r0glfy1hfn9v2np2pi1b072m8wxpnrmsgx4zmdjf30kdjbhr6d") (features (quote (("use_std") ("use_os" "use_std" "memsec/alloc" "memsec/use_os") ("nightly" "memsec/nightly") ("default" "use_os"))))))

(define-public crate-seckoo-0.0.0 (crate (name "seckoo") (vers "0.0.0-alpha0.1") (deps (list (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 2)))) (hash "12q68bc39dhr6yjaprlad73jyxkzq565ql5m7ss9f4czvxql9qdi")))

(define-public crate-seckoo-0.0.0 (crate (name "seckoo") (vers "0.0.0-alpha0.2") (deps (list (crate-dep (name "crossbeam-epoch") (req "^0.9.14") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 2)))) (hash "1rn3pvmgq3kcyz3krfd70m790r8bvjl84gh4qh6q78zif5dwlil3")))

(define-public crate-seckoo-0.0.0 (crate (name "seckoo") (vers "0.0.0-alpha1.0") (deps (list (crate-dep (name "crossbeam-epoch") (req "^0.9.14") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 2)))) (hash "1shmn5mqmps4gfxl2s86akb953q1nndpivbp2x8p0wgn4v2x2fxv")))

(define-public crate-seckoo-0.0.0 (crate (name "seckoo") (vers "0.0.0-alpha2.0") (deps (list (crate-dep (name "crossbeam-epoch") (req "^0.9.14") (default-features #t) (kind 0)) (crate-dep (name "criterion") (req "^0.4") (default-features #t) (kind 2)) (crate-dep (name "moka") (req "^0.10") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 2)))) (hash "10bwd2bdp4c2b10gfw1idc8w2qiczicppxj5abkzi4rr1ixvdaxg")))

(define-public crate-seckoo-0.0.0 (crate (name "seckoo") (vers "0.0.0-alpha2.1") (deps (list (crate-dep (name "crossbeam-epoch") (req "^0.9.14") (default-features #t) (kind 0)) (crate-dep (name "criterion") (req "^0.4") (default-features #t) (kind 2)) (crate-dep (name "moka") (req "^0.10") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 2)))) (hash "0cpjhwk352ssh9a4mfa799ibki5f20akv89vwkd63xr4jjs8y7dw")))

(define-public crate-secky-0.1 (crate (name "secky") (vers "0.1.0") (deps (list (crate-dep (name "atty") (req "^0.2.11") (default-features #t) (kind 0)) (crate-dep (name "failure") (req "^0.1.2") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.2.10") (default-features #t) (kind 0)))) (hash "0dqzam4ian1xhx6y2wqq3n6f7gd56fk8bcl8y8r04ispvbyvj1n9")))

