(define-module (crates-io se p-) #:use-module (crates-io))

(define-public crate-sep-40-oracle-0.1 (crate (name "sep-40-oracle") (vers "0.1.0") (deps (list (crate-dep (name "soroban-sdk") (req "^20.0.0-rc2") (default-features #t) (kind 0)) (crate-dep (name "soroban-sdk") (req "^20.0.0-rc2") (features (quote ("testutils"))) (default-features #t) (kind 2)))) (hash "0aylx6bxa66hqgnkskvf7p0ffpblvk7f84w5gyx4qsffgi3n2yvv") (features (quote (("testutils" "soroban-sdk/testutils"))))))

(define-public crate-sep-40-oracle-0.1 (crate (name "sep-40-oracle") (vers "0.1.1") (deps (list (crate-dep (name "soroban-sdk") (req "^20.0.0-rc2") (default-features #t) (kind 0)) (crate-dep (name "soroban-sdk") (req "^20.0.0-rc2") (features (quote ("testutils"))) (default-features #t) (kind 2)))) (hash "1lilbcikdysfl9aaamw9cb2nr92cnvm0h2grg9x7qf8fns6rwn7s") (features (quote (("testutils" "soroban-sdk/testutils"))))))

(define-public crate-sep-40-oracle-0.2 (crate (name "sep-40-oracle") (vers "0.2.0") (deps (list (crate-dep (name "soroban-sdk") (req "^20.0.0") (default-features #t) (kind 0)) (crate-dep (name "soroban-sdk") (req "^20.0.0") (features (quote ("testutils"))) (default-features #t) (kind 2)))) (hash "10d175ks28rv6jrggkf1kmvy73n7h5xplvmy68xag8p2cyc3wfga") (features (quote (("testutils" "soroban-sdk/testutils"))))))

(define-public crate-sep-40-oracle-1 (crate (name "sep-40-oracle") (vers "1.0.0") (deps (list (crate-dep (name "soroban-sdk") (req "^20.5.0") (default-features #t) (kind 0)) (crate-dep (name "soroban-sdk") (req "^20.5.0") (features (quote ("testutils"))) (default-features #t) (kind 2)))) (hash "14zvly8n9qacvzscdjp3fj3hmh0gw2wa9jy6cw2la7cp483nl6w4") (features (quote (("testutils" "soroban-sdk/testutils"))))))

(define-public crate-sep-41-token-0.2 (crate (name "sep-41-token") (vers "0.2.0") (deps (list (crate-dep (name "soroban-sdk") (req "^20.0.0-rc2") (default-features #t) (kind 0)) (crate-dep (name "soroban-sdk") (req "^20.0.0-rc2") (features (quote ("testutils"))) (default-features #t) (kind 2)))) (hash "0yc20jf6ld6ck0zigynwxif8fi7qn6w5sz4r997ldnk27f3y5xji") (features (quote (("testutils" "soroban-sdk/testutils"))))))

(define-public crate-sep-41-token-0.2 (crate (name "sep-41-token") (vers "0.2.1") (deps (list (crate-dep (name "soroban-sdk") (req "^20.0.0-rc2") (default-features #t) (kind 0)) (crate-dep (name "soroban-sdk") (req "^20.0.0-rc2") (features (quote ("testutils"))) (default-features #t) (kind 2)))) (hash "0y4nbgbiq5p48rnv93jib3l9mx54jxzx41gp9cv87xys2jhvdvg0") (features (quote (("testutils" "soroban-sdk/testutils"))))))

(define-public crate-sep-41-token-0.3 (crate (name "sep-41-token") (vers "0.3.0") (deps (list (crate-dep (name "soroban-sdk") (req "^20.0.0") (default-features #t) (kind 0)) (crate-dep (name "soroban-sdk") (req "^20.0.0") (features (quote ("testutils"))) (default-features #t) (kind 2)))) (hash "1zv74lpz8jfzazynj8qfrbvv8yk7hjyk915cz4qivbfjsrywacl8") (features (quote (("testutils" "soroban-sdk/testutils"))))))

(define-public crate-sep-41-token-1 (crate (name "sep-41-token") (vers "1.0.0") (deps (list (crate-dep (name "soroban-sdk") (req "^20.5.0") (default-features #t) (kind 0)) (crate-dep (name "soroban-sdk") (req "^20.5.0") (features (quote ("testutils"))) (default-features #t) (kind 2)))) (hash "130nqdph8l3kwf8qsmpc2mljg25zgc9wi5bf0ylsjd6v56c2yld0") (features (quote (("testutils" "soroban-sdk/testutils"))))))

(define-public crate-sep-sys-1 (crate (name "sep-sys") (vers "1.1.1+1.0.0") (deps (list (crate-dep (name "cc") (req "^1.0.67") (default-features #t) (kind 1)))) (hash "1fs92qwqzj1qyi89n0qd7azg66xwr0dq82d27km70xb6jwc7d64y") (yanked #t) (links "sep")))

(define-public crate-sep-sys-1 (crate (name "sep-sys") (vers "1.1.1+1.0.1") (deps (list (crate-dep (name "cc") (req "^1.0.67") (default-features #t) (kind 1)))) (hash "1qi5lkl2qv779jrbvvqdzl2gv1p60a8nz8gb5jqh2fh28lcdg9d7") (links "sep")))

(define-public crate-sep-sys-1 (crate (name "sep-sys") (vers "1.2.0") (deps (list (crate-dep (name "cc") (req "^1.0.67") (default-features #t) (kind 1)))) (hash "1hsx56a53h163sgr1wsa9q94lqha1zikp7ghy2g2zlfwp51kfkig") (links "sep")))

