(define-module (crates-io se ro) #:use-module (crates-io))

(define-public crate-sero-0.1 (crate (name "sero") (vers "0.1.0") (deps (list (crate-dep (name "dashmap") (req "^5.4") (default-features #t) (kind 0)))) (hash "0d1j4dmg9s0z90mg4klsmf0wjxfpiym899rlwcg2p2775ivaaqs8")))

(define-public crate-sero-0.1 (crate (name "sero") (vers "0.1.1") (deps (list (crate-dep (name "dashmap") (req "^5.0") (default-features #t) (kind 0)))) (hash "04x24ykzkgkn9bzcmil0amwhkjhj96v1704ihdcimvcddzi2rrmf")))

(define-public crate-sero-0.1 (crate (name "sero") (vers "0.1.2") (deps (list (crate-dep (name "dashmap") (req "^5") (default-features #t) (kind 0)))) (hash "07x9krvz0wlvv7bzz3zpg217zq04jp3qzg4xvfcybipk547dw5pv")))

(define-public crate-sero-0.1 (crate (name "sero") (vers "0.1.3") (deps (list (crate-dep (name "dashmap") (req "^5") (default-features #t) (kind 0)))) (hash "05im09rrqxdrrmjajc5jwvaxzzjjkxl9qp218mc82qywc12vf2l1")))

(define-public crate-sero-0.1 (crate (name "sero") (vers "0.1.4") (deps (list (crate-dep (name "dashmap") (req "^5") (default-features #t) (kind 0)))) (hash "1x5msx61xv6cpk97ha7mp90ckx4bnwbq9gh6s8p6x7wj78wmdrp8")))

(define-public crate-sero-0.1 (crate (name "sero") (vers "0.1.5") (deps (list (crate-dep (name "dashmap") (req "^5") (default-features #t) (kind 0)))) (hash "02y5nfwsdcgbhqjl4aming4hwzwmdj6snbqhqza8pp0ig878zngv")))

(define-public crate-sero-0.1 (crate (name "sero") (vers "0.1.6") (deps (list (crate-dep (name "dashmap") (req "^5") (default-features #t) (kind 0)))) (hash "101093cr9kkmm5m0ng8qk2wnnn4jycqv4vnp5zi3bpczdqp9xgld")))

(define-public crate-sero-0.1 (crate (name "sero") (vers "0.1.7") (deps (list (crate-dep (name "dashmap") (req "^5") (default-features #t) (kind 0)))) (hash "0ysdgbl2y138qipi5m2ih6p3fgf41cqmack6ckbv9n1js1ksia40")))

(define-public crate-sero-0.1 (crate (name "sero") (vers "0.1.8") (deps (list (crate-dep (name "dashmap") (req "^5") (default-features #t) (kind 0)))) (hash "03qmf2nz85p5wq7414fniffwnhcf157mhsc0isfwscp2p30r9ay6")))

(define-public crate-sero-0.1 (crate (name "sero") (vers "0.1.9") (deps (list (crate-dep (name "dashmap") (req "^5") (default-features #t) (kind 0)))) (hash "06zwzfhvfpvy50gzr7i5mqr66ma09qv76m0pnkgwjr11gallvdbi")))

(define-public crate-sero-0.1 (crate (name "sero") (vers "0.1.10") (deps (list (crate-dep (name "dashmap") (req "^5") (default-features #t) (kind 0)))) (hash "0v5p8bk73h8ar86blbfsbi0f9shp66k0nkh868zh7z4gbvdif663")))

(define-public crate-sero-0.1 (crate (name "sero") (vers "0.1.11") (deps (list (crate-dep (name "dashmap") (req "^5") (default-features #t) (kind 0)))) (hash "0fk5arsqwfjj3x8vxihrzplgwfhi31mnsnr481rrslr09k718k42")))

(define-public crate-sero-0.1 (crate (name "sero") (vers "0.1.12") (deps (list (crate-dep (name "dashmap") (req "^5") (default-features #t) (kind 0)))) (hash "0dj42ypjhhgialq20my79p3lxcm68n28gz1489zhdg2dmz8327jb")))

(define-public crate-sero-0.1 (crate (name "sero") (vers "0.1.13") (deps (list (crate-dep (name "dashmap") (req "^5") (default-features #t) (kind 0)))) (hash "1v3z51d39xsmjic432vanrb7n4zallxrjhggzf52yqi2h45nsqal")))

(define-public crate-sero-0.1 (crate (name "sero") (vers "0.1.14") (deps (list (crate-dep (name "dashmap") (req "^5") (default-features #t) (kind 0)))) (hash "0ck8jhqvxv5n63nf3ihc1dmprppxnb00apmfcjmkwig6ps5602pz")))

(define-public crate-sero-0.1 (crate (name "sero") (vers "0.1.15") (deps (list (crate-dep (name "dashmap") (req "^5") (default-features #t) (kind 0)))) (hash "17d2iqi6v8rw2ypxl11ff61h2g1yha705rcrk5fqc2xjjdl39cbs")))

(define-public crate-sero-0.1 (crate (name "sero") (vers "0.1.16") (deps (list (crate-dep (name "dashmap") (req "^5") (default-features #t) (kind 0)))) (hash "1pv5bmrzh8x1da8x2b68vjybsvlm44y2kvrl77rmcv2z340307cp")))

(define-public crate-sero-0.1 (crate (name "sero") (vers "0.1.17") (deps (list (crate-dep (name "dashmap") (req "^5") (default-features #t) (kind 0)))) (hash "11inzqwvwvdlqva2gb2r7xaqrlldw0ljby7r57xg1mrv5mn4mfdn")))

