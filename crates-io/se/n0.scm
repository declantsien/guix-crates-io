(define-module (crates-io se n0) #:use-module (crates-io))

(define-public crate-sen0177-0.1 (crate (name "sen0177") (vers "0.1.0") (deps (list (crate-dep (name "serial") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "serial-core") (req "^0.4") (default-features #t) (kind 0)))) (hash "1ra7jiq786njm7r5pj00kc87pssfh4ka70fyls879g6rg43k6s4i")))

(define-public crate-sen0177-0.1 (crate (name "sen0177") (vers "0.1.1") (deps (list (crate-dep (name "serial") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "serial-core") (req "^0.4") (default-features #t) (kind 0)))) (hash "17bfxcnciy4m1mwda9xz0jlascyvdlspl970a1jjbwmcz87phhix")))

(define-public crate-sen0177-0.2 (crate (name "sen0177") (vers "0.2.0") (deps (list (crate-dep (name "embedded-hal") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "linux-embedded-hal") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "nb") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "serial") (req "^0.4") (default-features #t) (kind 2)))) (hash "130y8gr568i4qilzgqrfaqs9h6icr8qyx8h50fwv2x4s03dj41fq") (features (quote (("std") ("default" "std"))))))

(define-public crate-sen0177-0.3 (crate (name "sen0177") (vers "0.3.0-alpha.1") (deps (list (crate-dep (name "embedded-hal") (req "=1.0.0-alpha.8") (default-features #t) (kind 0)) (crate-dep (name "linux-embedded-hal") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "serial") (req "^0.4") (default-features #t) (kind 2)))) (hash "0awlr0qjp07y6vl9pc65nszgxz7jv1alf1hqcgj12gxizph6kl9f") (features (quote (("std") ("default" "std"))))))

(define-public crate-sen0177-0.4 (crate (name "sen0177") (vers "0.4.0-alpha.1") (deps (list (crate-dep (name "anyhow") (req "^1") (default-features #t) (kind 2)) (crate-dep (name "embedded-hal") (req "=1.0.0-alpha.8") (default-features #t) (kind 0)) (crate-dep (name "linux-embedded-hal") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "serial") (req "^0.4") (default-features #t) (kind 2)))) (hash "1anzjawm6j4yx478pnigamjdc2djzwvvbba00mhcxc2azniz5ccm") (features (quote (("std") ("default"))))))

(define-public crate-sen0177-0.5 (crate (name "sen0177") (vers "0.5.0") (deps (list (crate-dep (name "anyhow") (req "^1") (default-features #t) (kind 2)) (crate-dep (name "embedded-hal") (req "=1.0.0-rc.1") (default-features #t) (kind 0)) (crate-dep (name "embedded-hal-nb") (req "=1.0.0-rc.1") (default-features #t) (kind 0)) (crate-dep (name "serial") (req "^0.4") (default-features #t) (kind 2)))) (hash "0sqsbmsa0p9dyar5hj81gv71lbc2srmg3yd999gjdxxfpz0q5yvm") (features (quote (("std") ("default"))))))

(define-public crate-sen0177-0.6 (crate (name "sen0177") (vers "0.6.0") (deps (list (crate-dep (name "anyhow") (req "^1") (default-features #t) (kind 2)) (crate-dep (name "embedded-hal") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "embedded-hal-nb") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "serial") (req "^0.4") (default-features #t) (kind 2)))) (hash "19g78995k6waffa72622af58pqqqrr66f5l1ngqvk9sj9pdkwdwc") (features (quote (("std") ("default"))))))

