(define-module (crates-io se rt) #:use-module (crates-io))

(define-public crate-sertools-0.1 (crate (name "sertools") (vers "0.1.0") (deps (list (crate-dep (name "serde") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1") (features (quote ("derive"))) (default-features #t) (kind 2)) (crate-dep (name "serde_json") (req "^1") (default-features #t) (kind 2)))) (hash "0lw6p5ab98i18nmr0h92z27clx84z31pxb7rqx3fq3v8ipnd8i72")))

(define-public crate-sertools-0.2 (crate (name "sertools") (vers "0.2.0") (deps (list (crate-dep (name "serde") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1") (features (quote ("derive"))) (default-features #t) (kind 2)) (crate-dep (name "serde_json") (req "^1") (default-features #t) (kind 2)))) (hash "00jzxz0lmsk0pi01bhbx16b5yyv2ayv620kda9z77bnc7ldibvi6")))

(define-public crate-serty-0.1 (crate (name "serty") (vers "0.1.0") (hash "1b9z2zgrigdqadmwr31jm8qxph5fjajr9ydz2nh90x7m8ml3xc93") (yanked #t)))

