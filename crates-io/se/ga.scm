(define-module (crates-io se ga) #:use-module (crates-io))

(define-public crate-sega-controller-0.1 (crate (name "sega-controller") (vers "0.1.0") (deps (list (crate-dep (name "embedded-hal") (req "^0.2.7") (features (quote ("unproven"))) (default-features #t) (kind 0)))) (hash "1mdjvdrzid6rablsck927j010fzvzg4sy8fcpr68qr25g0rr5x1p") (features (quote (("mega-drive") ("all" "mega-drive"))))))

(define-public crate-sega_cmp-0.1 (crate (name "sega_cmp") (vers "0.1.0") (deps (list (crate-dep (name "cc") (req "^1.0") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2.36") (default-features #t) (kind 0)))) (hash "0gkpcmcqyqnx09ghxzk623g71xskyxayik45yvqp2i8sjh8d3wnl")))

(define-public crate-sega_cmp-0.2 (crate (name "sega_cmp") (vers "0.2.0") (deps (list (crate-dep (name "cc") (req "^1.0") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2.36") (default-features #t) (kind 0)))) (hash "1v1c5rdrv3562zzq0mkklis5jmw1gc70pwifwgmmy41r49y5cdsg")))

(define-public crate-sega_film-0.1 (crate (name "sega_film") (vers "0.1.0") (hash "01l8gbq6qs5gjzw60ylmacbcwpkm1wss1gw8xhjac36ihdxmspsq")))

(define-public crate-segappend-0.1 (crate (name "segappend") (vers "0.1.0") (deps (list (crate-dep (name "cc") (req "^1.0.79") (default-features #t) (kind 1)))) (hash "1l1wfh4lhcli9602vdpywba8rxj24zlqvb0pxla6yzygwkr12k2i")))

(define-public crate-segappend-0.2 (crate (name "segappend") (vers "0.2.0") (deps (list (crate-dep (name "cc") (req "^1.0.79") (default-features #t) (kind 1)))) (hash "1ansk8ram1pr4bvc6v0628x1z5r64r9qb3mjd7mj3d3968dww7b1")))

