(define-module (crates-io se ca) #:use-module (crates-io))

(define-public crate-seca-0.1 (crate (name "seca") (vers "0.1.0") (deps (list (crate-dep (name "reqwest") (req "^0.11") (features (quote ("blocking" "json"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.85") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.31") (default-features #t) (kind 0)) (crate-dep (name "url") (req "^2.2.2") (default-features #t) (kind 0)))) (hash "1vzbifn9nplqn68zl3rrpyz6mn2fxq9i0cssc3xmlqq7nrjd3fv9") (yanked #t)))

(define-public crate-seca-1 (crate (name "seca") (vers "1.0.0") (hash "049v02h9am118dnwjfb1yv07jwsfksclsk0ddvcf28cdqx6sd4n9") (yanked #t)))

(define-public crate-seca-1 (crate (name "seca") (vers "1.1.0") (hash "0ws1w0gx2nmrlg6s7g7j5fi6nrj5dvbini068fzl09xzwmvrz2wn") (yanked #t)))

(define-public crate-secapi-0.1 (crate (name "secapi") (vers "0.1.0") (deps (list (crate-dep (name "bitflags") (req "^2.4.0") (default-features #t) (kind 0)) (crate-dep (name "chrono") (req "^0.4.30") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2.147") (default-features #t) (kind 0)) (crate-dep (name "secapi-sys") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "uuid") (req "^1.4.1") (default-features #t) (kind 0)))) (hash "1i7sln79d03c5dzfa1gab4rj4zr3wz63sqwja0bvcmf5k33yg6m7") (features (quote (("system-sa-client" "secapi-sys/system-sa-client"))))))

(define-public crate-secapi-sys-0.1 (crate (name "secapi-sys") (vers "0.1.0") (deps (list (crate-dep (name "copy_dir") (req "^0.1.3") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2.147") (default-features #t) (kind 0)))) (hash "102zk03g61hk4jsw0gl70hrm276r72zg7jxsll48grch1801vyg2") (features (quote (("system-sa-client") ("default"))))))

