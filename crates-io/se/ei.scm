(define-module (crates-io se ei) #:use-module (crates-io))

(define-public crate-seeip-1 (crate (name "seeip") (vers "1.0.0") (deps (list (crate-dep (name "reqwest") (req "^0.11") (features (quote ("blocking" "json"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "117yaanbglczkg9n3p4d9fm84kjvdnvy1bvxkafv6rs7j37028j6")))

(define-public crate-seeip-2 (crate (name "seeip") (vers "2.0.0") (deps (list (crate-dep (name "reqwest") (req "^0.11") (features (quote ("blocking" "json"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "0yrqbhq862q9d3wlrg21a6r3frd030vav0xnlgsphvw05c0jashw")))

(define-public crate-seeip-3 (crate (name "seeip") (vers "3.0.0") (deps (list (crate-dep (name "reqwest") (req "^0.11") (features (quote ("blocking" "json"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "0hd6wjdz2qk43s95d92z15h48y3v4cwbyg6kvwl0lfiq8ma53nx9")))

