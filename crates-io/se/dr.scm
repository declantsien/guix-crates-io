(define-module (crates-io se dr) #:use-module (crates-io))

(define-public crate-sedre-0.0.1 (crate (name "sedre") (vers "0.0.1") (hash "1xcj13mq847fcj6dhwbmaip7dmf7y2dfx5kzqc80a801qk48fgq7") (yanked #t)))

(define-public crate-sedregex-0.1 (crate (name "sedregex") (vers "0.1.0") (deps (list (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)))) (hash "1jjz9bxgrrkq703z2yalxaaf2z3dnarl98s4x5wh5cagzwf8p8im")))

(define-public crate-sedregex-0.1 (crate (name "sedregex") (vers "0.1.1") (deps (list (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)))) (hash "1mrpr9iy74a6yqgxy3fm0jj93m2k9pfvdpm20nd9q305wf6ijqzi")))

(define-public crate-sedregex-0.2 (crate (name "sedregex") (vers "0.2.0") (deps (list (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)))) (hash "1aww6fggzshlixv09zhv29q9bin32c647nd5yf1753n0cdw1svm8")))

(define-public crate-sedregex-0.2 (crate (name "sedregex") (vers "0.2.1") (deps (list (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)))) (hash "11p1drm8xrkdfibmrn97mrpg2w61svx95x7m8073nmwxw99gvlzx")))

(define-public crate-sedregex-0.2 (crate (name "sedregex") (vers "0.2.2") (deps (list (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)))) (hash "1igz6m2n2saqpx19rs17k5zb7ww3w0mkhd884lsnvkab1vd3856s")))

(define-public crate-sedregex-0.2 (crate (name "sedregex") (vers "0.2.3") (deps (list (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)))) (hash "02lrgzb9h6mds4vm2z0hqx8xyymfgbxbjmz3668mkj2gi0n9akrm")))

(define-public crate-sedregex-0.2 (crate (name "sedregex") (vers "0.2.4") (deps (list (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)))) (hash "0bwp2qbqgr93k4ggn9wcgb5jhinwiisnp686fsmz39ji6saahf79")))

(define-public crate-sedregex-0.2 (crate (name "sedregex") (vers "0.2.5") (deps (list (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)))) (hash "0gpy5sqck7rb7v8ryyy7zr5vnab37dh4bp0iplxz14v0b4iiwh8r")))

