(define-module (crates-io se ly) #:use-module (crates-io))

(define-public crate-selya-0.1 (crate (name "selya") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^4.3.8") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)))) (hash "12fqv41806z5m5p6q7c65giz22266nc4nsg375pplklw1jkwq82k") (features (quote (("executable" "clap") ("default"))))))

