(define-module (crates-io se _r) #:use-module (crates-io))

(define-public crate-se_rs_ial-0.0.2 (crate (name "se_rs_ial") (vers "0.0.2") (deps (list (crate-dep (name "bitflags") (req "*") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "*") (default-features #t) (kind 0)))) (hash "1x4kk1zy2hvm7209lag9v9rvr067xhjrkhhv9pmhvncjbni613lx")))

(define-public crate-se_rs_ial-0.0.3 (crate (name "se_rs_ial") (vers "0.0.3") (deps (list (crate-dep (name "bitflags") (req "*") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "*") (default-features #t) (kind 0)))) (hash "1a8vn3kb8zk7wys1pxb4ddirpf3fsyb8gj8qcwhk3jkflrsi9hnl") (yanked #t)))

