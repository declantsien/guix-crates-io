(define-module (crates-io se ex) #:use-module (crates-io))

(define-public crate-seexpr-0.0.0 (crate (name "seexpr") (vers "0.0.0") (hash "0l6y03afygfjjrxw9fxhri60nfr8mf3d4c16yas2mi4sbqmc6847")))

(define-public crate-seexpr-sys-0.0.0 (crate (name "seexpr-sys") (vers "0.0.0") (hash "0ndk0pykaqqwmqnh0h1vjkzavdcahclpzmsg3f7d0vqwd1mi5db8")))

