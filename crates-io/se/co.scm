(define-module (crates-io se co) #:use-module (crates-io))

(define-public crate-second-0.0.0 (crate (name "second") (vers "0.0.0") (hash "0wwz5lq5wzqj3sv1mnas4jlgwh13zl15bpr7b2i0b4irxpwl2375")))

(define-public crate-second-music-system-0.1 (crate (name "second-music-system") (vers "0.1.0") (deps (list (crate-dep (name "arcow") (req "^1.1.1") (default-features #t) (kind 0)) (crate-dep (name "compact_str") (req "^0.7.1") (default-features #t) (kind 0)) (crate-dep (name "crossbeam") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "derivative") (req "^2.2") (default-features #t) (kind 0)) (crate-dep (name "futures") (req "^0.3.29") (default-features #t) (kind 0)) (crate-dep (name "libsoxr") (req "^0.2.9") (default-features #t) (kind 0)) (crate-dep (name "num_cpus") (req "^1.16.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "once_cell") (req "^1.19.0") (default-features #t) (kind 0)) (crate-dep (name "shellish_parse") (req "^2.2.0") (default-features #t) (kind 0)) (crate-dep (name "switchyard") (req "^0.3.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.35.1") (features (quote ("rt"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "vecmap-rs") (req "^0.1.13") (default-features #t) (kind 0)))) (hash "151w5y8k0vby7n7g1wnynkcjjrfiz4k1wqqrpyq3m6cvy70ba7lr") (features (quote (("ffi-expose-issuer") ("default" "switchyard")))) (v 2) (features2 (quote (("tokio" "dep:tokio") ("switchyard" "dep:switchyard" "dep:num_cpus"))))))

(define-public crate-second-stack-0.2 (crate (name "second-stack") (vers "0.2.0") (deps (list (crate-dep (name "testdrop") (req "^0.1.2") (default-features #t) (kind 2)))) (hash "0d9gymrz7jb8jzwdzy9q9w3j6hl2awdgqinq98n85jav9q3gs2b6") (features (quote (("experimental"))))))

(define-public crate-second-stack-0.2 (crate (name "second-stack") (vers "0.2.1") (deps (list (crate-dep (name "criterion") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.7.1") (default-features #t) (kind 2)) (crate-dep (name "rand_pcg") (req "^0.2.0") (default-features #t) (kind 2)) (crate-dep (name "testdrop") (req "^0.1.2") (default-features #t) (kind 2)))) (hash "1nxd2i7marar43sg8hg94mcvyag9zrmysny40dy0mfwfbij7036q") (features (quote (("experimental"))))))

(define-public crate-second-stack-0.3 (crate (name "second-stack") (vers "0.3.0") (hash "0shqgvrk7izpfghmqahvlvpcplp3zijsi4bq8js4wwz45kxi6ghk")))

(define-public crate-second-stack-0.3 (crate (name "second-stack") (vers "0.3.1") (deps (list (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 2)))) (hash "13mif5frah0jzvam8cvab8xd8iq0vqvixa56djwahxh9d0qnlxjz")))

(define-public crate-second-stack-0.3 (crate (name "second-stack") (vers "0.3.2") (deps (list (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 2)) (crate-dep (name "testdrop") (req "^0.1.2") (default-features #t) (kind 2)))) (hash "1wb8n1l5iimvzxgib4qm0m37mmmdk9pnjnxd1bzish5ba6jddn1v")))

(define-public crate-second-stack-0.3 (crate (name "second-stack") (vers "0.3.3") (deps (list (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 2)) (crate-dep (name "testdrop") (req "^0.1.2") (default-features #t) (kind 2)))) (hash "0xiz3q7rk4bhjvsrd0b8ij5c8yi3qcsv4riidnha2z0z6z311hgl")))

(define-public crate-second-stack-0.3 (crate (name "second-stack") (vers "0.3.4") (deps (list (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 2)) (crate-dep (name "testdrop") (req "^0.1.2") (default-features #t) (kind 2)))) (hash "1caq8xj3qsnn498gg524v9a1hwqhyh5hlmwjlqlh362jrj3nd42i")))

(define-public crate-second-stack-0.3 (crate (name "second-stack") (vers "0.3.5") (deps (list (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 2)) (crate-dep (name "testdrop") (req "^0.1.2") (default-features #t) (kind 2)))) (hash "1fy036i108l52danyvhq0y1qwyaibbrqcnpsifqbkwaidqych129")))

(define-public crate-second_class-0.1 (crate (name "second_class") (vers "0.1.0") (hash "100b454rh6aravfrzm0iqn3ixyamk0qsb36wd0yz7simmqi1fn3z")))

(define-public crate-second_class_demo-0.1 (crate (name "second_class_demo") (vers "0.1.1") (hash "15n1ffd7fg5rl1ym3q60r93p8gd0gr94zv6mbymyk4bikcnvkmv5")))

(define-public crate-second_law-0.1 (crate (name "second_law") (vers "0.1.0") (hash "0594zi2ahhijzgid83d9hdpcgqki7l37w1lbw78dpz602kajqw6i")))

(define-public crate-second_law-0.2 (crate (name "second_law") (vers "0.2.1") (deps (list (crate-dep (name "tempdir") (req "^0.3") (default-features #t) (kind 0)))) (hash "0y290mcfzhlnc781s5a1wq4n37qrqbinyrq8sil70pb7542pw89l")))

(define-public crate-second_law-0.2 (crate (name "second_law") (vers "0.2.2") (deps (list (crate-dep (name "tempdir") (req "^0.3") (default-features #t) (kind 0)))) (hash "1hbyki0s7zx153ib69zymxbzbdpd3lf9mywx14bhgwb1dajy43yg")))

(define-public crate-second_law-0.3 (crate (name "second_law") (vers "0.3.1") (deps (list (crate-dep (name "tempdir") (req "^0.3") (default-features #t) (kind 0)))) (hash "0kiicq9p4s6xdhqlhp17y3rxj677di443qbpilyl3d5z0av3d1n5")))

(define-public crate-second_order_ode-0.1 (crate (name "second_order_ode") (vers "0.1.0") (deps (list (crate-dep (name "approx") (req "^0.2.0") (default-features #t) (kind 2)))) (hash "0bfcn4fw8zhrm2lj896yhv08sgw4dyfmh7qv9yq84b14d7akzlxj")))

(define-public crate-secondary-crate-0.1 (crate (name "secondary-crate") (vers "0.1.0") (hash "1l6prhdlbqgd2kvyq2cjw6b7jvigfsrnpi9jx0xfxqdj64csjmaz")))

(define-public crate-secondary_rewriter-0.1 (crate (name "secondary_rewriter") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^3.2") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "0m3d4wnpg4d2xcsbr8grxyadx9dddcgqxhj8912kjnwfxn135iz2")))

(define-public crate-secondary_rewriter-0.1 (crate (name "secondary_rewriter") (vers "0.1.1") (deps (list (crate-dep (name "clap") (req "^3.2") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "flate2") (req "^1") (default-features #t) (kind 0)))) (hash "1zgvr93yiclajm7vrrg4xsb160ya9ifk4s3qbz7vjgzxf525qdkm")))

(define-public crate-secondary_rewriter-0.1 (crate (name "secondary_rewriter") (vers "0.1.2") (deps (list (crate-dep (name "clap") (req "^3.2") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "flate2") (req "^1") (default-features #t) (kind 0)))) (hash "1w8fka4i47xffaswi6hqxwi2nbylng79055hpyljhmz0787adv6r")))

(define-public crate-secondary_rewriter-0.1 (crate (name "secondary_rewriter") (vers "0.1.3") (deps (list (crate-dep (name "clap") (req "^3.2") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "flate2") (req "^1") (default-features #t) (kind 0)))) (hash "1k50wgdawgh4jhdqg557papk1f9ny1hr1j537acrgg4vlkzmdv1d")))

(define-public crate-secondary_rewriter-0.1 (crate (name "secondary_rewriter") (vers "0.1.4") (deps (list (crate-dep (name "clap") (req "^3.2") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "flate2") (req "^1") (default-features #t) (kind 0)))) (hash "05r05liq6rqn4mllq3761dnb2vjginip4j51m74zjs79jyb2cswk")))

(define-public crate-secondary_rewriter-0.1 (crate (name "secondary_rewriter") (vers "0.1.5") (deps (list (crate-dep (name "clap") (req "^3.2") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "flate2") (req "^1") (default-features #t) (kind 0)))) (hash "0zp93q74xjmmmmzdmpq87j651dm1c0a1vrybywdl26qvx29ych96")))

(define-public crate-secondary_rewriter-0.1 (crate (name "secondary_rewriter") (vers "0.1.6") (deps (list (crate-dep (name "clap") (req "^3.2") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "flate2") (req "^1") (default-features #t) (kind 0)))) (hash "0x5s41qqw9g398x9zq5w5fpvb2xwswspqmlmsxs5aqk45hg4dd62")))

(define-public crate-secondary_rewriter-0.1 (crate (name "secondary_rewriter") (vers "0.1.7") (deps (list (crate-dep (name "clap") (req "^3.2") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "flate2") (req "^1") (default-features #t) (kind 0)))) (hash "1k9757d3j3vjmzddmcj7lyx1mh3b01kwa3g1iyvngiy508fx9xpv")))

(define-public crate-secondary_rewriter-0.1 (crate (name "secondary_rewriter") (vers "0.1.8") (deps (list (crate-dep (name "clap") (req "^3.2") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "flate2") (req "^1") (default-features #t) (kind 0)))) (hash "121nbimpxsznbgva2c34m577nn9v8ix0llalc5drd4xldv2ql131")))

(define-public crate-secondary_rewriter-0.1 (crate (name "secondary_rewriter") (vers "0.1.9") (deps (list (crate-dep (name "clap") (req "^3.2") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "flate2") (req "^1") (default-features #t) (kind 0)))) (hash "0hrg6y2d39269ryrmplc6skpfg5ssg7spifha9sk7hx8gjvkqw7k")))

(define-public crate-secondary_rewriter-0.2 (crate (name "secondary_rewriter") (vers "0.2.0") (deps (list (crate-dep (name "clap") (req "^3.2") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "flate2") (req "^1") (default-features #t) (kind 0)))) (hash "16rzrxkq7lnyjg88ar0npb709r4p09hhvawnw1y0c29651s71zmy")))

(define-public crate-secondfry-libft-0.1 (crate (name "secondfry-libft") (vers "0.1.0") (hash "1fh4fl94qdinvlagv885iv07fhc35b67dw6fpdfxxxss2p85aamz")))

(define-public crate-secondfry-libft-0.1 (crate (name "secondfry-libft") (vers "0.1.1") (hash "13wfs9llq3p4j7jzkhx8zdn4wvr4rwfgkcw8m6k4af8aabqgy2a2")))

(define-public crate-secondlaw-0.1 (crate (name "secondlaw") (vers "0.1.0") (hash "1xk63gc77a766wp37kz4rz2abxlri57wiaj2znavxs2icdxrdvm8")))

(define-public crate-secondsight-0.1 (crate (name "secondsight") (vers "0.1.0") (hash "05nf0dw1whsy8yy996b8nwixcxx8wyqzvniaricwk1zhkwkzv489")))

(define-public crate-secondsight-0.1 (crate (name "secondsight") (vers "0.1.1") (deps (list (crate-dep (name "fern") (req "^0.6.2") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.17") (default-features #t) (kind 0)) (crate-dep (name "simple_logger") (req "^4.1.0") (default-features #t) (kind 0)))) (hash "1c0pzmckxj4pk0lngr9fxavilxcxvd4xmbv5hf0x9y74brpqm449") (yanked #t)))

(define-public crate-secondsight-0.1 (crate (name "secondsight") (vers "0.1.2") (hash "1d4h90g87ns6l84fr7fpraz67zppxl2m10k4svlq7d3r0wf14yyk")))

(define-public crate-Secondwelcome-0.1 (crate (name "Secondwelcome") (vers "0.1.1") (hash "1515836jmjkgk5xrs52yj3fidmbpjxangvaqpla54ch8vng9vh54")))

(define-public crate-Secondwelcome-0.1 (crate (name "Secondwelcome") (vers "0.1.2") (hash "0d35j0mcgr79jgax361vg4vc7lriw645kk51jabwys2882cknnin")))

(define-public crate-secop-0.1 (crate (name "secop") (vers "0.1.3") (deps (list (crate-dep (name "clap") (req "^2.33.0") (default-features #t) (kind 0)) (crate-dep (name "daemonize") (req "^0.4.1") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.8") (default-features #t) (kind 0)) (crate-dep (name "mlzlog") (req "^0.7.0") (default-features #t) (kind 0)) (crate-dep (name "mlzutil") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "secop-core") (req "^0.1.3") (default-features #t) (kind 0)) (crate-dep (name "secop-modules") (req "^0.1.3") (default-features #t) (kind 0)) (crate-dep (name "signal-hook") (req "^0.3.8") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.3.2") (default-features #t) (kind 0)))) (hash "0ilhn83lnamji8mvjhndi0yxqp6s2v1g2jylwsnhb59crcdc9a8s")))

(define-public crate-secop-core-0.1 (crate (name "secop-core") (vers "0.1.3") (deps (list (crate-dep (name "base64") (req "^0.13.0") (default-features #t) (kind 0)) (crate-dep (name "crossbeam-channel") (req "^0.5.0") (default-features #t) (kind 0)) (crate-dep (name "derive-new") (req "^0.5.8") (default-features #t) (kind 0)) (crate-dep (name "hashbrown") (req "^0.11.2") (features (quote ("serde"))) (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.8") (default-features #t) (kind 0)) (crate-dep (name "memchr") (req "^2.2.1") (default-features #t) (kind 0)) (crate-dep (name "mlzlog") (req "^0.7.0") (default-features #t) (kind 0)) (crate-dep (name "mlzutil") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "parking_lot") (req "^0.11.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.3.1") (default-features #t) (kind 0)) (crate-dep (name "secop-derive") (req "^0.1.3") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.101") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0.101") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.41") (default-features #t) (kind 0)) (crate-dep (name "time") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "toml") (req "^0.5.3") (default-features #t) (kind 0)) (crate-dep (name "url") (req "^2.1.0") (default-features #t) (kind 0)))) (hash "02w6v6v2bcdcrazdggz3mn4ij8gh7p0hickljamm2qwv42dc0b9v")))

(define-public crate-secop-derive-0.1 (crate (name "secop-derive") (vers "0.1.3") (deps (list (crate-dep (name "darling") (req "^0.13.0") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1.0.5") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.2") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.5") (default-features #t) (kind 0)) (crate-dep (name "synstructure") (req "^0.12.1") (default-features #t) (kind 0)))) (hash "0x5cja4rfdm3mjnxprya0m70rp8hgixzy42m585bylk9a6wc1v4w")))

(define-public crate-secop-modules-0.1 (crate (name "secop-modules") (vers "0.1.3") (deps (list (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.8") (default-features #t) (kind 0)) (crate-dep (name "mlzlog") (req "^0.7.0") (default-features #t) (kind 0)) (crate-dep (name "mlzutil") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "parking_lot") (req "^0.11.0") (default-features #t) (kind 0)) (crate-dep (name "secop-core") (req "^0.1.3") (default-features #t) (kind 0)) (crate-dep (name "secop-derive") (req "^0.1.3") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.41") (default-features #t) (kind 0)) (crate-dep (name "serialport") (req "^4.0.0") (default-features #t) (kind 0)))) (hash "0iiw68h2wrxkfqfjfr714i22pqnlnfm5wn9krh7q8spfdn8s6d5b")))

