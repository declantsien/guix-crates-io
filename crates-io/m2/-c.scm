(define-module (crates-io m2 -c) #:use-module (crates-io))

(define-public crate-m2-ctrl-0.1 (crate (name "m2-ctrl") (vers "0.1.0") (deps (list (crate-dep (name "cc") (req "^1.0.73") (default-features #t) (kind 1)) (crate-dep (name "paste") (req "^1.0.6") (default-features #t) (kind 0)) (crate-dep (name "simulink-binder") (req "^0.1.1") (default-features #t) (kind 0)))) (hash "1k63cxwiddbdw9p43b0qd6n8kp9zjpmmdz6nxsy72p12ic0irw7z")))

(define-public crate-m2-ctrl-1 (crate (name "m2-ctrl") (vers "1.0.0") (deps (list (crate-dep (name "asm") (req "^0.1.0") (optional #t) (default-features #t) (kind 0) (package "m2-ctrl_asm")) (crate-dep (name "fsm") (req "^0.1.0") (optional #t) (default-features #t) (kind 0) (package "m2-ctrl_fsm")))) (hash "08kpj9qmjiqqksnhvq31jr3368528m1hxlqarw7ix4yari4yabfa")))

(define-public crate-m2-ctrl_asm-0.1 (crate (name "m2-ctrl_asm") (vers "0.1.0") (deps (list (crate-dep (name "cc") (req "^1.0.73") (default-features #t) (kind 1)) (crate-dep (name "paste") (req "^1.0.6") (default-features #t) (kind 0)) (crate-dep (name "simulink-binder") (req "^0.1.1") (default-features #t) (kind 0)))) (hash "0cmdnscw2zhrq66gwv5x0qn1dh8zfmj7adrzvdw4z07c8gmpxk1g")))

(define-public crate-m2-ctrl_fsm-0.1 (crate (name "m2-ctrl_fsm") (vers "0.1.0") (deps (list (crate-dep (name "cc") (req "^1.0.73") (default-features #t) (kind 1)) (crate-dep (name "paste") (req "^1.0.6") (default-features #t) (kind 0)) (crate-dep (name "simulink-binder") (req "^0.1.1") (default-features #t) (kind 0)))) (hash "16n086wik0jwp8n669ynpzkg2qz9zqn0q0bpqfl2lykmkm4m63jy")))

