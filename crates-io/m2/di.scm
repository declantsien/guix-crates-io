(define-module (crates-io m2 di) #:use-module (crates-io))

(define-public crate-m2dir-0.0.1 (crate (name "m2dir") (vers "0.0.1") (hash "1vj1fbbkf6wzfkan776jqhvyx67wyvqxgzf6l49fs5yb453bvkci") (features (quote (("default"))))))

(define-public crate-m2dir-0.1 (crate (name "m2dir") (vers "0.1.0") (hash "0q182n27s7x7kh2wisa69smjyprcl538s0lpm0s6262l2h2wlh22")))

