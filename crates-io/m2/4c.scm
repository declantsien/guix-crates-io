(define-module (crates-io m2 #{4c}#) #:use-module (crates-io))

(define-public crate-m24c64-0.1 (crate (name "m24c64") (vers "0.1.0") (deps (list (crate-dep (name "embedded-hal") (req "^0.2.7") (default-features #t) (kind 0)) (crate-dep (name "embedded-hal-mock") (req "^0.8.0") (default-features #t) (kind 2)))) (hash "1sjiiikkikbjdf9f8amx2xsfx2knq4989gfdq8kmsiqn26h4dmgg")))

(define-public crate-m24c64-0.1 (crate (name "m24c64") (vers "0.1.1") (deps (list (crate-dep (name "embedded-hal") (req "^0.2.7") (default-features #t) (kind 0)) (crate-dep (name "embedded-hal-mock") (req "^0.8.0") (default-features #t) (kind 2)))) (hash "1njc9rqh150pvyv4rm2kbrri0w8h6lnv5s5q54jpsgswar6dgyjc")))

(define-public crate-m24c64-0.1 (crate (name "m24c64") (vers "0.1.2") (deps (list (crate-dep (name "embedded-hal") (req "^0.2.7") (default-features #t) (kind 0)) (crate-dep (name "embedded-hal-mock") (req "^0.8.0") (default-features #t) (kind 2)))) (hash "1y74hpbaajj473zz156gkw279268pg6qmdkc6dd2zijnr8a3dhm1")))

(define-public crate-m24c64-0.1 (crate (name "m24c64") (vers "0.1.3") (deps (list (crate-dep (name "embedded-hal") (req "^0.2.7") (default-features #t) (kind 0)) (crate-dep (name "embedded-hal-mock") (req "^0.8.0") (default-features #t) (kind 2)))) (hash "0p87dmg5agfvzx5nfrpv3lf6hgx438fn70ij0rbg8ag5ddczfjhl")))

(define-public crate-m24c64-0.1 (crate (name "m24c64") (vers "0.1.4") (deps (list (crate-dep (name "embedded-hal") (req "^0.2.7") (default-features #t) (kind 0)) (crate-dep (name "embedded-hal-mock") (req "^0.8.0") (default-features #t) (kind 2)))) (hash "0r9r6p53ydy8d2q7pv7v2v75wkvig6p43qwizxmh8412jd5r0wnk")))

(define-public crate-m24c64-0.1 (crate (name "m24c64") (vers "0.1.5") (deps (list (crate-dep (name "embedded-hal") (req "^0.2.7") (default-features #t) (kind 0)) (crate-dep (name "embedded-hal-mock") (req "^0.8.0") (default-features #t) (kind 2)))) (hash "00n8jh8gsnxckqgf7783a5907cxid80r9d5hqqqg1sarc5r1iqi9")))

