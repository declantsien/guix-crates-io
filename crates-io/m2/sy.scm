(define-module (crates-io m2 sy) #:use-module (crates-io))

(define-public crate-m2sync-0.11 (crate (name "m2sync") (vers "0.11.0") (deps (list (crate-dep (name "clap") (req "^4") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "ctrlc") (req "^3.4") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "simplelog") (req "^0.12.0") (default-features #t) (kind 0)) (crate-dep (name "vomit-config") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "vomit-m2sync") (req "^0") (default-features #t) (kind 0)))) (hash "0h2db9mzhbs63qf86vhyf784vkijjrnfj0g11h5w6niqcmxnfpmg")))

