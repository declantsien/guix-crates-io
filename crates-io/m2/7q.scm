(define-module (crates-io m2 #{7q}#) #:use-module (crates-io))

(define-public crate-m27q-kvm-0.1 (crate (name "m27q-kvm") (vers "0.1.0") (deps (list (crate-dep (name "pico-args") (req "^0.4.2") (default-features #t) (kind 0)) (crate-dep (name "rusb") (req "^0.8.1") (default-features #t) (kind 0)))) (hash "0in2chcbv7kp0xch16y5vh2mlyyg11wjdkn12csav1kg4h05vcfr")))

