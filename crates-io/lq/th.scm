(define-module (crates-io lq th) #:use-module (crates-io))

(define-public crate-lqth-0.1 (crate (name "lqth") (vers "0.1.0") (deps (list (crate-dep (name "byteorder") (req "^1.5") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "x11") (req "^2.21") (features (quote ("xlib"))) (kind 0)))) (hash "0f8qhg4v0i7z1i576cff9i60w2fliq0rrri1mgi9a5xm9rjy9wh3") (rust-version "1.65.0")))

(define-public crate-lqth-0.1 (crate (name "lqth") (vers "0.1.1") (deps (list (crate-dep (name "byteorder") (req "^1.5") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.2") (default-features #t) (kind 0)) (crate-dep (name "x11") (req "^2.21") (features (quote ("xlib"))) (kind 0)))) (hash "0w7mfx5c15ci1cjjq051in5mis3z6qml9ckimimrlm3nzxc3v5jd") (rust-version "1.65.0")))

(define-public crate-lqth-0.1 (crate (name "lqth") (vers "0.1.2") (deps (list (crate-dep (name "byteorder") (req "^1.5") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.2") (default-features #t) (kind 0)) (crate-dep (name "x11") (req "^2.21") (features (quote ("xlib"))) (kind 0)))) (hash "0aya7yjl69qmz1fvmiy8a4li2cp17jn43vinpy1pbms23v6plkmn") (rust-version "1.65.0")))

(define-public crate-lqth-0.1 (crate (name "lqth") (vers "0.1.3") (deps (list (crate-dep (name "thiserror") (req "^1.0.2") (default-features #t) (kind 0)) (crate-dep (name "x11") (req "^2.21") (features (quote ("xlib"))) (kind 0)))) (hash "0x3bm81gvwx483vn4d5wxf0mn7rddwqm35ppqad00rcjissn0h2d") (rust-version "1.65.0")))

(define-public crate-lqth-0.2 (crate (name "lqth") (vers "0.2.0") (deps (list (crate-dep (name "thiserror") (req "^1.0.2") (default-features #t) (kind 0)) (crate-dep (name "x11") (req "^2.21") (features (quote ("xlib"))) (kind 0)))) (hash "0nyxghiw4934hsjhjmbwghkpm166lfpni244dj06i8wdk5yv206n") (yanked #t) (rust-version "1.65.0")))

(define-public crate-lqth-0.2 (crate (name "lqth") (vers "0.2.1") (deps (list (crate-dep (name "thiserror") (req ">=1.0.2") (default-features #t) (kind 0)) (crate-dep (name "x11") (req "^2.21") (features (quote ("xlib"))) (kind 0)))) (hash "02r5sw4cisnkvhssw501n1f1008magcgvwr238692a1lccxix606") (rust-version "1.72.1")))

(define-public crate-lqth-0.2 (crate (name "lqth") (vers "0.2.2") (deps (list (crate-dep (name "thiserror") (req ">=1.0.2") (default-features #t) (kind 0)) (crate-dep (name "x11") (req "^2.21") (features (quote ("xlib"))) (kind 0)))) (hash "06vsd6m14kjh7xs3nks2zjp48f4f95giyvnvgw4r6izrivqfsll4") (rust-version "1.72.1")))

(define-public crate-lqth-0.2 (crate (name "lqth") (vers "0.2.3-alpha.1") (deps (list (crate-dep (name "thiserror") (req ">=1.0.2") (default-features #t) (kind 0)) (crate-dep (name "x11") (req "^2.21") (features (quote ("xlib"))) (kind 0)))) (hash "0zxcsr9hy4kr8i7y500czidbdlbi1zx23a376ls3viqhbab0jwbi") (rust-version "1.72.1")))

