(define-module (crates-io ee ri) #:use-module (crates-io))

(define-public crate-eeric-0.0.0 (crate (name "eeric") (vers "0.0.0") (deps (list (crate-dep (name "itertools") (req "^0.11.0") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2.16") (default-features #t) (kind 0)))) (hash "0l8n5yxkk8khs88vavbhc6dfz8mr3yrfls35gz3k5pm03b4ixc7c")))

(define-public crate-eeric-0.0.1 (crate (name "eeric") (vers "0.0.1") (deps (list (crate-dep (name "itertools") (req "^0.11.0") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2.16") (default-features #t) (kind 0)))) (hash "02in25gi31xza2zgspwq7ly8x0b0v8kd8a1vrnszr9w5kljbcwc9")))

(define-public crate-eeric-0.0.2 (crate (name "eeric") (vers "0.0.2") (deps (list (crate-dep (name "itertools") (req "^0.11.0") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2.16") (default-features #t) (kind 0)))) (hash "0pqxkkrydlj1mvd7mnd9l3shs4az9vwi1200g8y1zlspzsxvzxjw")))

(define-public crate-eeric-0.0.3 (crate (name "eeric") (vers "0.0.3") (deps (list (crate-dep (name "itertools") (req "^0.11.0") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2.16") (default-features #t) (kind 0)))) (hash "1njr0zchs473yq6spbylqhh4j2qwlw2chmi1sia8jp4vc93f43g9")))

(define-public crate-eeric-0.0.4 (crate (name "eeric") (vers "0.0.4") (deps (list (crate-dep (name "itertools") (req "^0.11.0") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2.16") (default-features #t) (kind 0)))) (hash "0140gc35anrpnxvi9ldz76p076bykm42f52rql3bfij7l1ifhwm8")))

(define-public crate-eeric-0.0.5 (crate (name "eeric") (vers "0.0.5") (deps (list (crate-dep (name "itertools") (req "^0.11.0") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2.16") (default-features #t) (kind 0)))) (hash "0gzk27jb5is9nk72sqfb9gjrq8rw0rqpswcjzljfyvbxr0hn31y9")))

(define-public crate-eeric-0.0.6 (crate (name "eeric") (vers "0.0.6") (deps (list (crate-dep (name "itertools") (req "^0.11.0") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2.16") (default-features #t) (kind 0)))) (hash "03nn9mn8b2szyxiz6qh13xcssprmn3qjbl5v6p7yr8smmbcm67nw")))

(define-public crate-eeric-0.0.7 (crate (name "eeric") (vers "0.0.7") (deps (list (crate-dep (name "itertools") (req "^0.11.0") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2.16") (default-features #t) (kind 0)))) (hash "1rfpbw9d4l7njrnxcqm067vfr3nyjlz8fjsg9xwksh53y0sf3hh3")))

(define-public crate-eeric-0.0.8 (crate (name "eeric") (vers "0.0.8") (deps (list (crate-dep (name "itertools") (req "^0.11.0") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2.16") (default-features #t) (kind 0)))) (hash "1v5xdjmm298dp44phsk1z9jwxk2xrb0faph9r30scv91b4dwgsmq")))

(define-public crate-eeric-0.0.9 (crate (name "eeric") (vers "0.0.9") (deps (list (crate-dep (name "itertools") (req "^0.11.0") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2.16") (default-features #t) (kind 0)))) (hash "006jz62yd0d77741fhp2yjx43kkjl49b85a7bcv7xxzgj2521hb3")))

(define-public crate-eeric-0.0.10 (crate (name "eeric") (vers "0.0.10") (deps (list (crate-dep (name "itertools") (req "^0.11.0") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2.16") (default-features #t) (kind 0)))) (hash "1qqbqlgra4c83ix32prbgqr92arv9iwzrrkis96cyg3zimm9wb1h")))

(define-public crate-eeric-0.0.11 (crate (name "eeric") (vers "0.0.11") (deps (list (crate-dep (name "itertools") (req "^0.11.0") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2.16") (default-features #t) (kind 0)))) (hash "0nwcm2npbywj9zsyd68qwv0zpglpww0fy7h13zfcxb72l8vxiy71")))

(define-public crate-eeric-0.0.12 (crate (name "eeric") (vers "0.0.12") (deps (list (crate-dep (name "itertools") (req "^0.11.0") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2.16") (default-features #t) (kind 0)))) (hash "0psj063a37hi63yhl8ir0sm2jfdh63mkn76lz5hnlacjnfpz5lgr")))

(define-public crate-eeric-0.0.13 (crate (name "eeric") (vers "0.0.13") (deps (list (crate-dep (name "itertools") (req "^0.11.0") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2.16") (default-features #t) (kind 0)))) (hash "0x3w0lhwra6cgnxld377kzdlfa3l954s7scxxy3jj2abdmcqgkbl")))

(define-public crate-eeric-0.0.14 (crate (name "eeric") (vers "0.0.14") (deps (list (crate-dep (name "itertools") (req "^0.11.0") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2.16") (default-features #t) (kind 0)))) (hash "1ksg2mqi16rsj3py7fvhypr14lkn1d24lqyrd2dr3c9fvfn97axj")))

(define-public crate-eeric-0.0.15 (crate (name "eeric") (vers "0.0.15") (deps (list (crate-dep (name "itertools") (req "^0.11.0") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2.16") (default-features #t) (kind 0)))) (hash "09q955ibri6qwl8w87l820d8x4p4q21dhr3lnnxggzdff2c12bax")))

(define-public crate-eeric-0.0.16 (crate (name "eeric") (vers "0.0.16") (deps (list (crate-dep (name "itertools") (req "^0.11.0") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2.16") (default-features #t) (kind 0)))) (hash "0036yshwx05ymcn29zxdla2gbg7g33cjl5ldrlk8c530lqc5d32x")))

(define-public crate-eeric-0.0.17 (crate (name "eeric") (vers "0.0.17") (deps (list (crate-dep (name "itertools") (req "^0.11.0") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2.16") (default-features #t) (kind 0)))) (hash "0zqnvfykyzlfhhks6j8z5d3c8sc4srn65hayjcwkbhx2ryx4s1r4")))

(define-public crate-eeric-0.0.18 (crate (name "eeric") (vers "0.0.18") (deps (list (crate-dep (name "itertools") (req "^0.11.0") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2.16") (default-features #t) (kind 0)))) (hash "0x7cz6syqd0fmhm5i73kb8wyhspbpaxn6fm51n3sfv7m2wd0qig1")))

(define-public crate-eeric-0.0.19 (crate (name "eeric") (vers "0.0.19") (deps (list (crate-dep (name "itertools") (req "^0.11.0") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2.16") (default-features #t) (kind 0)))) (hash "1ffg10byxqw0p665x54nask0ygwz0jad4i02ymrg437w8aslrvrn")))

(define-public crate-eeric-0.0.20 (crate (name "eeric") (vers "0.0.20") (deps (list (crate-dep (name "itertools") (req "^0.11.0") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2.16") (default-features #t) (kind 0)))) (hash "0m2l6afzbx5jzlklagvrvyzgiim62azkd15f6yylmjd3mp1k2cx8")))

(define-public crate-eeric-0.0.21 (crate (name "eeric") (vers "0.0.21") (deps (list (crate-dep (name "derive_builder") (req "^0.12.0") (default-features #t) (kind 0)) (crate-dep (name "itertools") (req "^0.11.0") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2.16") (default-features #t) (kind 0)))) (hash "0y07rswxi7vjzqvfznybiwg4b78r52q6vsids18hlirv8g20bcg7")))

(define-public crate-eeric-0.0.22 (crate (name "eeric") (vers "0.0.22") (deps (list (crate-dep (name "derive_builder") (req "^0.12.0") (default-features #t) (kind 0)) (crate-dep (name "itertools") (req "^0.11.0") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2.16") (default-features #t) (kind 0)))) (hash "1nma36jb3mqv2s1xnp55708vi6avxxqzx07xlm5zc3jzcvcrs484")))

(define-public crate-eeric-0.0.23 (crate (name "eeric") (vers "0.0.23") (deps (list (crate-dep (name "derive_builder") (req "^0.12.0") (default-features #t) (kind 0)) (crate-dep (name "itertools") (req "^0.11.0") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2.16") (default-features #t) (kind 0)))) (hash "10kc1l3xvkwq3hl2fb5hsjzg9xyi2ly5v9jq8s6826yqa8g3mz3d")))

(define-public crate-eeric-0.0.24 (crate (name "eeric") (vers "0.0.24") (deps (list (crate-dep (name "derive_builder") (req "^0.12.0") (default-features #t) (kind 0)) (crate-dep (name "itertools") (req "^0.11.0") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2.16") (default-features #t) (kind 0)))) (hash "0cp6w8rgqnf8qcyqk9kq7h4pf1bzamgkmxh0l8vlz9lx75m3ay6j")))

(define-public crate-eeric-0.0.25 (crate (name "eeric") (vers "0.0.25") (deps (list (crate-dep (name "derive_builder") (req "^0.12.0") (default-features #t) (kind 0)) (crate-dep (name "itertools") (req "^0.11.0") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2.16") (default-features #t) (kind 0)))) (hash "0wiy5v4vjixx1p2hnsqmsqa5zc0676xjyd1jkd0gzymb2qc0qxwg")))

(define-public crate-eeric-0.0.26 (crate (name "eeric") (vers "0.0.26") (deps (list (crate-dep (name "derive_builder") (req "^0.12.0") (default-features #t) (kind 0)) (crate-dep (name "itertools") (req "^0.11.0") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2.16") (default-features #t) (kind 0)))) (hash "0jp98wdgzrv3klk1qna4vhq233819mdcaqvaw7zhmabanq2rvhr2")))

(define-public crate-eeric-0.0.27 (crate (name "eeric") (vers "0.0.27") (deps (list (crate-dep (name "derive_builder") (req "^0.12.0") (default-features #t) (kind 0)) (crate-dep (name "itertools") (req "^0.11.0") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2.16") (default-features #t) (kind 0)))) (hash "1m8dx6h7h3m7ilcq2viy9srjia9qqp54i559xkd8ns1xcld6vcf4")))

(define-public crate-eeric-0.0.28 (crate (name "eeric") (vers "0.0.28") (deps (list (crate-dep (name "derive_builder") (req "^0.12.0") (default-features #t) (kind 0)) (crate-dep (name "itertools") (req "^0.11.0") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2.16") (default-features #t) (kind 0)))) (hash "0530lgd3ssdnqiqhsv9czcfj84m4jhlwhvdf2xvm1b0pwwlsvfig")))

(define-public crate-eeric-0.0.29 (crate (name "eeric") (vers "0.0.29") (deps (list (crate-dep (name "derive_builder") (req "^0.12.0") (default-features #t) (kind 0)) (crate-dep (name "itertools") (req "^0.11.0") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2.16") (default-features #t) (kind 0)))) (hash "18k5rnmyz6kgmzaclwv1z1cdljpj9nklb6450mb8gzxnz27hmj8w")))

(define-public crate-eeric-0.0.30 (crate (name "eeric") (vers "0.0.30") (deps (list (crate-dep (name "derive_builder") (req "^0.12.0") (default-features #t) (kind 0)) (crate-dep (name "itertools") (req "^0.11.0") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2.16") (default-features #t) (kind 0)))) (hash "1hpmcwg1mz19fhxvm40p7516q9ygf1gc8rwv9gzqm5b8f0nz8g0h")))

(define-public crate-eeric-0.0.31 (crate (name "eeric") (vers "0.0.31") (deps (list (crate-dep (name "derive_builder") (req "^0.12.0") (default-features #t) (kind 0)) (crate-dep (name "itertools") (req "^0.11.0") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2.16") (default-features #t) (kind 0)))) (hash "0bw5f8ckrlxw4zl3grdks3dgg75jvbjpbzw1y1dp8xypawa17i8z")))

(define-public crate-eeric-0.0.32 (crate (name "eeric") (vers "0.0.32") (deps (list (crate-dep (name "derive_builder") (req "^0.12.0") (default-features #t) (kind 0)) (crate-dep (name "itertools") (req "^0.11.0") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2.16") (default-features #t) (kind 0)))) (hash "1fznyy7in5r8lqwgz6m0zi87mj14kqiazmn92if5qf7knh3n0791")))

(define-public crate-eeric-0.0.33 (crate (name "eeric") (vers "0.0.33") (deps (list (crate-dep (name "derive_builder") (req "^0.12.0") (default-features #t) (kind 0)) (crate-dep (name "itertools") (req "^0.11.0") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2.16") (default-features #t) (kind 0)))) (hash "0fmqf4kdi9n44wl9pzlm9w1i19q3lfc3zxspylcc1msskriqf7aw")))

(define-public crate-eeric-0.0.34 (crate (name "eeric") (vers "0.0.34") (deps (list (crate-dep (name "derive_builder") (req "^0.12.0") (default-features #t) (kind 0)) (crate-dep (name "itertools") (req "^0.11.0") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2.16") (default-features #t) (kind 0)))) (hash "1lhrmr4qx218jqxrzr3dgmhmmw2npg05x60d72jm61sfzji45abn")))

(define-public crate-eeric-0.0.35 (crate (name "eeric") (vers "0.0.35") (deps (list (crate-dep (name "derive_builder") (req "^0.12.0") (default-features #t) (kind 0)) (crate-dep (name "itertools") (req "^0.11.0") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2.16") (default-features #t) (kind 0)))) (hash "1jzz94kr2jpcxvwr81sdhbfl29ijc485hq879c2g8fhqdb3smzwn")))

(define-public crate-eeric-0.0.36 (crate (name "eeric") (vers "0.0.36") (deps (list (crate-dep (name "derive_builder") (req "^0.12.0") (default-features #t) (kind 0)) (crate-dep (name "itertools") (req "^0.11.0") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2.16") (default-features #t) (kind 0)))) (hash "16wnrfwh5kd9jwradkvfdmpbwrkwc7pxnm7cfax4yd0q4m6cppna")))

(define-public crate-eeric-0.0.37 (crate (name "eeric") (vers "0.0.37") (deps (list (crate-dep (name "derive_builder") (req "^0.12.0") (default-features #t) (kind 0)) (crate-dep (name "itertools") (req "^0.11.0") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2.16") (default-features #t) (kind 0)))) (hash "06lyp8sy4bkcgd33c71gvsyz16ndi22l0599sb8hfy59rxlj1mxa")))

(define-public crate-eeric-0.0.38 (crate (name "eeric") (vers "0.0.38") (deps (list (crate-dep (name "derive_builder") (req "^0.12.0") (default-features #t) (kind 0)) (crate-dep (name "itertools") (req "^0.11.0") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2.16") (default-features #t) (kind 0)))) (hash "1a26n0vmyf62fw7gm8mpbbyb0j4i6490z1xq3l7nsa1bw6l2k89y")))

(define-public crate-eeric-0.0.39 (crate (name "eeric") (vers "0.0.39") (deps (list (crate-dep (name "derive_builder") (req "^0.12.0") (default-features #t) (kind 0)) (crate-dep (name "itertools") (req "^0.11.0") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2.16") (default-features #t) (kind 0)))) (hash "15yfpy9r7x1bkfmzkk5yic9bjj6mc3qsdcvqhy2w8x3zki8zfm2r")))

(define-public crate-eeric-0.0.40 (crate (name "eeric") (vers "0.0.40") (deps (list (crate-dep (name "derive_builder") (req "^0.12.0") (default-features #t) (kind 0)) (crate-dep (name "itertools") (req "^0.11.0") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2.16") (default-features #t) (kind 0)))) (hash "0xw3jll6dmk71kgz09j189bmf624n6v8bsmid1xnsirarbkv3ay6")))

(define-public crate-eeric-0.0.41 (crate (name "eeric") (vers "0.0.41") (deps (list (crate-dep (name "derive_builder") (req "^0.12.0") (default-features #t) (kind 0)) (crate-dep (name "itertools") (req "^0.11.0") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2.16") (default-features #t) (kind 0)))) (hash "023hg1skida1q68iy4kvaxmfg64vgc1g06hn4b0i4pd6hswgk0m8")))

(define-public crate-eeric-0.0.42 (crate (name "eeric") (vers "0.0.42") (deps (list (crate-dep (name "derive_builder") (req "^0.12.0") (default-features #t) (kind 0)) (crate-dep (name "itertools") (req "^0.11.0") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2.16") (default-features #t) (kind 0)))) (hash "1ibm7bkn0fxmdvqgrvahki97fb6p7174ny29jjcxjfyddswhlj4q")))

(define-public crate-eeric-0.0.44 (crate (name "eeric") (vers "0.0.44") (deps (list (crate-dep (name "derive_builder") (req "^0.12.0") (default-features #t) (kind 0)) (crate-dep (name "itertools") (req "^0.11.0") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2.16") (default-features #t) (kind 0)))) (hash "1pvsj9j3svkf1bisajq1c4qkb7ps40vnply9q7mz2z8m6y3lvm30")))

(define-public crate-eeric-0.0.45 (crate (name "eeric") (vers "0.0.45") (deps (list (crate-dep (name "derive_builder") (req "^0.12.0") (default-features #t) (kind 0)) (crate-dep (name "itertools") (req "^0.11.0") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2.16") (default-features #t) (kind 0)))) (hash "08wrz9ncz7d8ma13gid9dy118yiilwhp1lhnbqknrvm7s8yf6cw8")))

(define-public crate-eeric-0.0.46 (crate (name "eeric") (vers "0.0.46") (deps (list (crate-dep (name "derive_builder") (req "^0.12.0") (default-features #t) (kind 0)) (crate-dep (name "itertools") (req "^0.11.0") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2.16") (default-features #t) (kind 0)))) (hash "003i5gjmy4g6rg3jsp0j5ghwx2lxwnn6hrnklmp5m4qz2r0blfdc")))

(define-public crate-eeric-0.0.47 (crate (name "eeric") (vers "0.0.47") (deps (list (crate-dep (name "derive_builder") (req "^0.12.0") (default-features #t) (kind 0)) (crate-dep (name "itertools") (req "^0.11.0") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2.16") (default-features #t) (kind 0)))) (hash "0q8hd8aipyigcis1h22q53m0v46njkrgw2lmmxg2vdq3w4vs51kc")))

(define-public crate-eeric-0.0.48 (crate (name "eeric") (vers "0.0.48") (deps (list (crate-dep (name "derive_builder") (req "^0.12.0") (default-features #t) (kind 0)) (crate-dep (name "itertools") (req "^0.11.0") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2.16") (default-features #t) (kind 0)))) (hash "1spbp6awx11sk7jvk8iv24d2l4fsafrzri5v46y31xyvz2x4kqiy")))

(define-public crate-eeric-0.0.49 (crate (name "eeric") (vers "0.0.49") (deps (list (crate-dep (name "derive_builder") (req "^0.12.0") (default-features #t) (kind 0)) (crate-dep (name "itertools") (req "^0.11.0") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2.16") (default-features #t) (kind 0)))) (hash "023g2ryg2842wl2phy6sc8mjhdwhsyx9bn2jga0ja6s2y5ynyf8x")))

(define-public crate-eeric-0.0.50 (crate (name "eeric") (vers "0.0.50") (deps (list (crate-dep (name "derive_builder") (req "^0.12.0") (default-features #t) (kind 0)) (crate-dep (name "instant") (req "^0.1.12") (features (quote ("stdweb"))) (default-features #t) (kind 0)) (crate-dep (name "itertools") (req "^0.11.0") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2.16") (default-features #t) (kind 0)))) (hash "1zkddklx7vdjng5ak42bwn7sz2azkplfc5bldirlzp05gd9381xp")))

(define-public crate-eeric-0.0.51 (crate (name "eeric") (vers "0.0.51") (deps (list (crate-dep (name "derive_builder") (req "^0.12.0") (default-features #t) (kind 0)) (crate-dep (name "itertools") (req "^0.11.0") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2.16") (default-features #t) (kind 0)))) (hash "1dfgvwq1zjx2al5zpdshjjy8qjikfvp3578yipcaf4c6jw6750ww")))

(define-public crate-eeric-0.0.52 (crate (name "eeric") (vers "0.0.52") (deps (list (crate-dep (name "derive_builder") (req "^0.12.0") (default-features #t) (kind 0)) (crate-dep (name "itertools") (req "^0.11.0") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2.16") (default-features #t) (kind 0)))) (hash "1h49d26vd3glr5s4rbbq5kjfqhnrg64d8v3hl19c05zcqp2vzlhf")))

(define-public crate-eeric-0.1 (crate (name "eeric") (vers "0.1.0-rc.1") (deps (list (crate-dep (name "derive_builder") (req "^0.12.0") (default-features #t) (kind 0)) (crate-dep (name "itertools") (req "^0.11.0") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2.16") (default-features #t) (kind 0)))) (hash "1558lqgvixq87h6ryjmsvf810cws4l7h4rpvwa2cnxf997kl9x4x")))

(define-public crate-eeric-0.1 (crate (name "eeric") (vers "0.1.0-rc.2") (deps (list (crate-dep (name "derive_builder") (req "^0.12.0") (default-features #t) (kind 0)) (crate-dep (name "itertools") (req "^0.11.0") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2.16") (default-features #t) (kind 0)))) (hash "145q9lkr1fnqvl3jnz4d620z0fhlpa419pb1qab9mad9gvbgvasx")))

(define-public crate-eeric-0.1 (crate (name "eeric") (vers "0.1.0-rc.3") (deps (list (crate-dep (name "derive_builder") (req "^0.12.0") (default-features #t) (kind 0)) (crate-dep (name "itertools") (req "^0.11.0") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2.16") (default-features #t) (kind 0)))) (hash "11rd6d7nchj82kl6mk4pa5cs9dbcwvkf5j9b8clybq2d3sqyddqf")))

(define-public crate-eeric-0.1 (crate (name "eeric") (vers "0.1.0-rc.4") (deps (list (crate-dep (name "derive_builder") (req "^0.12.0") (default-features #t) (kind 0)) (crate-dep (name "itertools") (req "^0.11.0") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2.16") (default-features #t) (kind 0)))) (hash "0manygcbn50116480409z31i9bn1qg9y0fzvlihqgysj2wj5n283")))

(define-public crate-eeric-0.1 (crate (name "eeric") (vers "0.1.0-rc.5") (deps (list (crate-dep (name "derive_builder") (req "^0.12.0") (default-features #t) (kind 0)) (crate-dep (name "itertools") (req "^0.11.0") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2.16") (default-features #t) (kind 0)))) (hash "071si7hnv0r1wvyagg0vg4f4p4132q77hgmxlnkkm734h5ac4bfc")))

(define-public crate-eeric-core-0.1 (crate (name "eeric-core") (vers "0.1.0") (deps (list (crate-dep (name "derive_builder") (req "^0.12.0") (default-features #t) (kind 0)) (crate-dep (name "itertools") (req "^0.11.0") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2.16") (default-features #t) (kind 0)))) (hash "0wvc3f195yw2dnwf85p2ac10d5a9z3kfl7rhdrf21f330g8jwk8x")))

(define-public crate-eeric-core-0.1 (crate (name "eeric-core") (vers "0.1.2") (deps (list (crate-dep (name "derive_builder") (req "^0.12.0") (default-features #t) (kind 0)) (crate-dep (name "itertools") (req "^0.11.0") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2.16") (default-features #t) (kind 0)))) (hash "02dyaqm9dk77l0x82qfl40lpr4wa9l8g7mydxnf1nmjdrr18j39x")))

(define-public crate-eeric-interpreter-0.0.0 (crate (name "eeric-interpreter") (vers "0.0.0") (deps (list (crate-dep (name "eeric") (req "^0.0.5") (default-features #t) (kind 0)))) (hash "0wm7yxk46ln8x1rkafqxx6x26a5hdmj0rb11pq50c232wqn2cjyq")))

(define-public crate-eeric-interpreter-0.0.1 (crate (name "eeric-interpreter") (vers "0.0.1") (deps (list (crate-dep (name "eeric") (req "^0.0.7") (default-features #t) (kind 0)))) (hash "013llzv2lppbhxn1v8lhd0h1vykn1z95fxmy72aws347lchgciim")))

(define-public crate-eeric-interpreter-0.0.2 (crate (name "eeric-interpreter") (vers "0.0.2") (deps (list (crate-dep (name "eeric") (req "^0.0.8") (default-features #t) (kind 0)))) (hash "11w0j9ngdb9qs6yr348q12rfb8xxl2k1h8a0j4ims6wd4wgvmj0g")))

(define-public crate-eeric-interpreter-0.0.3 (crate (name "eeric-interpreter") (vers "0.0.3") (deps (list (crate-dep (name "eeric") (req "^0.0.9") (default-features #t) (kind 0)))) (hash "08zdk5hsn9r202mwdw876dpa6404m1a58r864md8skmfy7gdfgq6")))

(define-public crate-eeric-interpreter-0.0.4 (crate (name "eeric-interpreter") (vers "0.0.4") (deps (list (crate-dep (name "eeric") (req "^0.0.10") (default-features #t) (kind 0)))) (hash "071jskdg49gj49mf5fwpc2w6d710c0yg134akic96l5kyjiqmi1f")))

(define-public crate-eeric-interpreter-0.0.5 (crate (name "eeric-interpreter") (vers "0.0.5") (deps (list (crate-dep (name "eeric") (req "^0.0.11") (default-features #t) (kind 0)))) (hash "017wqjmcq31558wk7d6yl2hibsnfxx5ddh3rf0glh45xa3ids029")))

(define-public crate-eeric-interpreter-0.0.6 (crate (name "eeric-interpreter") (vers "0.0.6") (deps (list (crate-dep (name "eeric") (req "^0.0.11") (default-features #t) (kind 0)))) (hash "0i63lsciri1rf6chgsxf4nwj456kvxgmvdkc6p4bcpqaim9i63ln")))

(define-public crate-eeric-interpreter-0.0.7 (crate (name "eeric-interpreter") (vers "0.0.7") (deps (list (crate-dep (name "eeric") (req "^0.0.12") (default-features #t) (kind 0)))) (hash "09x56xfpfz8jfam2f666qqh2m8ir1n38g75fppkwc7xvis77g40k")))

(define-public crate-eeric-interpreter-0.0.8 (crate (name "eeric-interpreter") (vers "0.0.8") (deps (list (crate-dep (name "eeric") (req "^0.0.12") (default-features #t) (kind 0)))) (hash "1i34j7pcv7b1zndcn48h7wrf1xqw4bfv274ccrg79459ak8rrfnc")))

(define-public crate-eeric-interpreter-0.0.9 (crate (name "eeric-interpreter") (vers "0.0.9") (deps (list (crate-dep (name "eeric") (req "^0.0.13") (default-features #t) (kind 0)))) (hash "0p6aak7yxjbkd10fkxibwgacrd9z7r0c8sk5mdrh16wmpx8d05hl")))

(define-public crate-eeric-interpreter-0.0.10 (crate (name "eeric-interpreter") (vers "0.0.10") (deps (list (crate-dep (name "eeric") (req "^0.0.14") (default-features #t) (kind 0)))) (hash "1bv90b9d7fwap61xz4gdgcj9qslm599gnv0a32y0v7dlx5ya4a28")))

(define-public crate-eeric-interpreter-0.0.11 (crate (name "eeric-interpreter") (vers "0.0.11") (deps (list (crate-dep (name "eeric") (req "^0.0.15") (default-features #t) (kind 0)))) (hash "0bz8qp468jrwpvx0i1sb531msml3bxrsscvfy4hxjpg162m9x2hc")))

(define-public crate-eeric-interpreter-0.0.12 (crate (name "eeric-interpreter") (vers "0.0.12") (deps (list (crate-dep (name "eeric") (req "^0.0.18") (default-features #t) (kind 0)))) (hash "0nvhmqm5wwhccfvw5cn1ckgxlnxlczmz85yv8dgl8bc6yh7raik3")))

(define-public crate-eeric-interpreter-0.0.13 (crate (name "eeric-interpreter") (vers "0.0.13") (deps (list (crate-dep (name "eeric") (req "^0.0.18") (default-features #t) (kind 0)))) (hash "03sfbg9cqdnadfzi5vjd5xzx7pr3jbc8z62xxl677sclfmkcy0rl")))

(define-public crate-eeric-interpreter-0.0.14 (crate (name "eeric-interpreter") (vers "0.0.14") (deps (list (crate-dep (name "eeric") (req "^0.0.19") (default-features #t) (kind 0)))) (hash "1486n1gga8n8jn5av6csybidkxy777km34g53sx0g4643awwlnl0")))

(define-public crate-eeric-interpreter-0.0.15 (crate (name "eeric-interpreter") (vers "0.0.15") (deps (list (crate-dep (name "eeric") (req "^0.0.19") (default-features #t) (kind 0)))) (hash "0az4k9xy4yvgi0kpl7nkd130d0yrdvbkn6ywlhb8iqgqr95417m3")))

(define-public crate-eeric-interpreter-0.0.16 (crate (name "eeric-interpreter") (vers "0.0.16") (deps (list (crate-dep (name "eeric") (req "^0.0.19") (default-features #t) (kind 0)))) (hash "07pbsrcfwxlzr6m27kpvnzwqiwwq61bswhwdw8pqj2a0pbfk265m")))

(define-public crate-eeric-interpreter-0.0.17 (crate (name "eeric-interpreter") (vers "0.0.17") (deps (list (crate-dep (name "eeric") (req "^0.0.19") (default-features #t) (kind 0)))) (hash "0jxx8jwz85gkmpyqxa2wmsg721b1f32pvvicpm3x7i3ll8zb7r53")))

(define-public crate-eeric-interpreter-0.0.18 (crate (name "eeric-interpreter") (vers "0.0.18") (deps (list (crate-dep (name "eeric") (req "^0.0.20") (default-features #t) (kind 0)))) (hash "1s3h1mp0gr66c96bf07552xgnrbd403i35g2gc1jx571fx06ky3c")))

(define-public crate-eeric-interpreter-0.0.19 (crate (name "eeric-interpreter") (vers "0.0.19") (deps (list (crate-dep (name "eeric") (req "^0.0.21") (default-features #t) (kind 0)))) (hash "0s44q7d77nsrklvd5r66343il9cqv59i5mipd3zzsnhz9m2qdl0d")))

(define-public crate-eeric-interpreter-0.0.20 (crate (name "eeric-interpreter") (vers "0.0.20") (deps (list (crate-dep (name "eeric") (req "^0.0.22") (default-features #t) (kind 0)))) (hash "0afjhm9gn18bc72vha0lmgm0prpsv0s001gn5k8i2jbjc4ks29lz")))

(define-public crate-eeric-interpreter-0.0.21 (crate (name "eeric-interpreter") (vers "0.0.21") (deps (list (crate-dep (name "eeric") (req "^0.0.23") (default-features #t) (kind 0)))) (hash "00q07vr9bmv7rbwayk2kxnmgb353rdlxnj7ickijs9rpzd517j0f")))

(define-public crate-eeric-interpreter-0.0.22 (crate (name "eeric-interpreter") (vers "0.0.22") (deps (list (crate-dep (name "eeric") (req "^0.0.26") (default-features #t) (kind 0)))) (hash "1gls2zawxff0gahb86jdx26xla4s4qy52lfqrwyj4nyay48acwxw")))

(define-public crate-eeric-interpreter-0.0.23 (crate (name "eeric-interpreter") (vers "0.0.23") (deps (list (crate-dep (name "eeric") (req "^0.0.28") (default-features #t) (kind 0)))) (hash "1hnigy153s5g9r3wcg94j4m107gyy08bnk6fjggkb2d2fj9kldmz")))

(define-public crate-eeric-interpreter-0.0.24 (crate (name "eeric-interpreter") (vers "0.0.24") (deps (list (crate-dep (name "eeric") (req "^0.0.29") (default-features #t) (kind 0)))) (hash "1prrqda4w8vhd72hmcyzv1kvcg4b6cm30567afb6scrj7pzzbjxm")))

(define-public crate-eeric-interpreter-0.0.25 (crate (name "eeric-interpreter") (vers "0.0.25") (deps (list (crate-dep (name "eeric") (req "^0.0.30") (default-features #t) (kind 0)))) (hash "0nc7zv2jl5vhhcyz50zmd5sn0g6v1rwghnbbq36cax4hmqp65rby")))

(define-public crate-eeric-interpreter-0.0.26 (crate (name "eeric-interpreter") (vers "0.0.26") (deps (list (crate-dep (name "eeric") (req "^0.0.31") (default-features #t) (kind 0)))) (hash "1lljwz9lcybg6918a426w6yka38mrnl9mszps4980fhpffp2wvmk")))

(define-public crate-eeric-interpreter-0.0.27 (crate (name "eeric-interpreter") (vers "0.0.27") (deps (list (crate-dep (name "eeric") (req "^0.0.31") (default-features #t) (kind 0)))) (hash "0fi70ygbh8c9kvwiifx217bj8f7va8qdyy3wwrrqsqgal1r8ia6i")))

(define-public crate-eeric-interpreter-0.0.28 (crate (name "eeric-interpreter") (vers "0.0.28") (deps (list (crate-dep (name "eeric") (req "^0.0.32") (default-features #t) (kind 0)))) (hash "172lzdpxrhgnc2jclhlk49v85mxxk11ipmyg28lwbcsg2z13q1xy")))

(define-public crate-eeric-interpreter-0.0.29 (crate (name "eeric-interpreter") (vers "0.0.29") (deps (list (crate-dep (name "eeric") (req "^0.0.33") (default-features #t) (kind 0)))) (hash "0zk003pp5xsckp1433sgkv14m69fcdlfm1ac5zfv63mj5sqharfj")))

(define-public crate-eeric-interpreter-0.0.30 (crate (name "eeric-interpreter") (vers "0.0.30") (deps (list (crate-dep (name "eeric") (req "^0.0.34") (default-features #t) (kind 0)))) (hash "1qr255x6xlh8190bshvzhrjdj1nd4fflqg3s2sadhiz5x2rwm3jh")))

(define-public crate-eeric-interpreter-0.0.31 (crate (name "eeric-interpreter") (vers "0.0.31") (deps (list (crate-dep (name "eeric") (req "^0.0.35") (default-features #t) (kind 0)))) (hash "1kz9czj4k13wisb88kfrdzvpfnd0pxfx7i12p0y1qhbagxmc8lli")))

(define-public crate-eeric-interpreter-0.0.32 (crate (name "eeric-interpreter") (vers "0.0.32") (deps (list (crate-dep (name "eeric") (req "^0.0.36") (default-features #t) (kind 0)))) (hash "05837slhc10v538akkjpbnbrkmgx1mjyw5bd6c0cxpyxbjaas65q")))

(define-public crate-eeric-interpreter-0.0.33 (crate (name "eeric-interpreter") (vers "0.0.33") (deps (list (crate-dep (name "eeric") (req "^0.0.36") (default-features #t) (kind 0)))) (hash "0rvrbqi0fm4apbqangqpivlh3vg1z38l9k5wsgq8qfh53al31s2l")))

(define-public crate-eeric-interpreter-0.0.34 (crate (name "eeric-interpreter") (vers "0.0.34") (deps (list (crate-dep (name "eeric") (req "^0.0.36") (default-features #t) (kind 0)))) (hash "0pw0dabk2hzsb1qkr0kbl4nzrb38xaqpxvxq389iyrkkvsnzxw67")))

(define-public crate-eeric-interpreter-0.0.35 (crate (name "eeric-interpreter") (vers "0.0.35") (deps (list (crate-dep (name "eeric") (req "^0.0.37") (default-features #t) (kind 0)))) (hash "1sa64sk6zn320s37qhjmjqmr7jgcxb1092mi9nx3gwh5l2by6y9l")))

(define-public crate-eeric-interpreter-0.0.36 (crate (name "eeric-interpreter") (vers "0.0.36") (deps (list (crate-dep (name "eeric") (req "^0.0.38") (default-features #t) (kind 0)))) (hash "0wpvzlwxssr7r50xa86fll9cy7jljlgm0pwvz40wx1fj8i8chslx")))

(define-public crate-eeric-interpreter-0.0.37 (crate (name "eeric-interpreter") (vers "0.0.37") (deps (list (crate-dep (name "eeric") (req "^0.0.39") (default-features #t) (kind 0)))) (hash "1zxgmgy137ash5x5zfv1zd6zkjr813xmizzv0qf1a8h2wla23aik")))

(define-public crate-eeric-interpreter-0.0.38 (crate (name "eeric-interpreter") (vers "0.0.38") (deps (list (crate-dep (name "eeric") (req "^0.0.39") (default-features #t) (kind 0)))) (hash "12dpfqyjcjj69630yx75w6jsmd0hcz1mk4lpq41jgwz0vapzsq82")))

(define-public crate-eeric-interpreter-0.0.39 (crate (name "eeric-interpreter") (vers "0.0.39") (deps (list (crate-dep (name "eeric") (req "^0.0.40") (default-features #t) (kind 0)))) (hash "02ympcd863rbyx1ccqznmx81477ahk6hh2ihpcvzadx0ynlvpzp9")))

(define-public crate-eeric-interpreter-0.0.40 (crate (name "eeric-interpreter") (vers "0.0.40") (deps (list (crate-dep (name "eeric") (req "^0.0.41") (default-features #t) (kind 0)))) (hash "1jndjnxlsl6d2y894hjll546fhbksp2qfblk5j277xd8k78w1ikp")))

(define-public crate-eeric-interpreter-0.0.41 (crate (name "eeric-interpreter") (vers "0.0.41") (deps (list (crate-dep (name "eeric") (req "^0.0.41") (default-features #t) (kind 0)))) (hash "0k7g12dsnwb3d5s0qq4h087r2h08662n2hx9ggx10my322y0hrlz")))

(define-public crate-eeric-interpreter-0.0.42 (crate (name "eeric-interpreter") (vers "0.0.42") (deps (list (crate-dep (name "eeric") (req "^0.0.41") (default-features #t) (kind 0)))) (hash "0wx8kprjjvy9agz0qpxmfhzz7aq6sqgj89csicdjwgzm0spl283h")))

(define-public crate-eeric-interpreter-0.0.43 (crate (name "eeric-interpreter") (vers "0.0.43") (deps (list (crate-dep (name "eeric") (req "^0.0.42") (default-features #t) (kind 0)))) (hash "131f9da1c6bs7mr3ijv3yq0047f4d7fh7vgaq7nm2b4lrbx6m7gk")))

(define-public crate-eeric-interpreter-0.0.44 (crate (name "eeric-interpreter") (vers "0.0.44") (deps (list (crate-dep (name "eeric") (req "^0.0.44") (default-features #t) (kind 0)))) (hash "0mp82d2rmvmax61hrcm0wlnjngpmgmcm180nhd3b3yw9b3val795")))

(define-public crate-eeric-interpreter-0.0.45 (crate (name "eeric-interpreter") (vers "0.0.45") (deps (list (crate-dep (name "eeric") (req "^0.0.45") (default-features #t) (kind 0)))) (hash "0f4iaxvnlacj9vppy6jqv95slq7l21s6dhraz6ilk54aj4i8gfr8")))

(define-public crate-eeric-interpreter-0.0.46 (crate (name "eeric-interpreter") (vers "0.0.46") (deps (list (crate-dep (name "eeric") (req "^0.0.46") (default-features #t) (kind 0)))) (hash "0qji057376mv1k79az4jgs3jlhyrywfa3bg2wxj2f8kxcn2xj8qf")))

(define-public crate-eeric-interpreter-0.0.47 (crate (name "eeric-interpreter") (vers "0.0.47") (deps (list (crate-dep (name "eeric") (req "^0.0.47") (default-features #t) (kind 0)))) (hash "0jrvg8plmqcn19vqrd2vqia1vqzx3msm1dpqvpndxc9a60d7cfgm")))

(define-public crate-eeric-interpreter-0.0.48 (crate (name "eeric-interpreter") (vers "0.0.48") (deps (list (crate-dep (name "eeric") (req "^0.0.48") (default-features #t) (kind 0)))) (hash "0k8yjdn5lbhz9x3y2kkhfh3v44c23gsn701lm3559cd246jbhghz")))

(define-public crate-eeric-interpreter-0.0.49 (crate (name "eeric-interpreter") (vers "0.0.49") (deps (list (crate-dep (name "eeric") (req "^0.0.49") (default-features #t) (kind 0)))) (hash "011rsfhgb76jz1140kdkcmkwrj5gycvhlpf6arc9z30fwfyqjlvp")))

(define-public crate-eeric-interpreter-0.0.50 (crate (name "eeric-interpreter") (vers "0.0.50") (deps (list (crate-dep (name "eeric") (req "^0.0.50") (default-features #t) (kind 0)))) (hash "02f21jvvmi28hdkwb68cbwcq2g4x6sg8gzkjb13w19q2zfslw2s1")))

(define-public crate-eeric-interpreter-0.0.51 (crate (name "eeric-interpreter") (vers "0.0.51") (deps (list (crate-dep (name "eeric") (req "^0.0.51") (default-features #t) (kind 0)))) (hash "01agsx351ahmwipn5flcbv54zvlkc0iy9fwhlma0d8smhnhjlvf0")))

(define-public crate-eeric-interpreter-0.0.52 (crate (name "eeric-interpreter") (vers "0.0.52") (deps (list (crate-dep (name "eeric") (req "^0.1.0-rc.1") (default-features #t) (kind 0)))) (hash "1zyqvv0jn8sbx1vsh013a3bpmkjaslac831jbz310v5z1ygk697b")))

(define-public crate-eeric-interpreter-0.0.53 (crate (name "eeric-interpreter") (vers "0.0.53") (deps (list (crate-dep (name "eeric") (req "^0.1.0-rc.1") (default-features #t) (kind 0)))) (hash "01vd78j2q6s8zrzcn4kw1v8xxf7hj5ah178652xr3sfqg9nyd8w0")))

(define-public crate-eeric-interpreter-0.0.54 (crate (name "eeric-interpreter") (vers "0.0.54") (deps (list (crate-dep (name "eeric") (req "^0.1.0-rc.2") (default-features #t) (kind 0)))) (hash "1dxynhwx5s6v8zrr6grffh47czswhkipjzqhxn2hcp9qrbfd8zjy")))

(define-public crate-eeric-interpreter-0.0.55 (crate (name "eeric-interpreter") (vers "0.0.55") (deps (list (crate-dep (name "eeric") (req "^0.1.0-rc.2") (default-features #t) (kind 0)))) (hash "09wh907k3zy57mg92bkfjfyd7rpvbxnhfc79mfj4qwc5467pqmrm")))

(define-public crate-eeric-interpreter-0.0.56 (crate (name "eeric-interpreter") (vers "0.0.56") (deps (list (crate-dep (name "eeric") (req "^0.1.0-rc.2") (default-features #t) (kind 0)))) (hash "1p7m3m4vwlmlgxlixq55a50pi1pb80xnxq3pn7k5vsw1x4q1jr06")))

(define-public crate-eeric-interpreter-0.1 (crate (name "eeric-interpreter") (vers "0.1.0-rc.1") (deps (list (crate-dep (name "eeric") (req "^0.1.0-rc.3") (default-features #t) (kind 0)))) (hash "0rv0110jjwijxa8v0166bp9fji8p8gzizj2yr86aqc84iq9pbhmp")))

(define-public crate-eeric-interpreter-0.1 (crate (name "eeric-interpreter") (vers "0.1.0-rc.2") (deps (list (crate-dep (name "eeric") (req "^0.1.0-rc.4") (default-features #t) (kind 0)))) (hash "0i824nifs6vz41m5r7wmn7w4nf10aipf7wsqgzlagcjxp5hngv4i")))

(define-public crate-eeric-interpreter-0.1 (crate (name "eeric-interpreter") (vers "0.1.0-rc.3") (deps (list (crate-dep (name "eeric") (req "^0.1.0-rc.5") (default-features #t) (kind 0)))) (hash "1niilybhzdjb92pnh8mpl6skll0vvpikkwbnq9iw7rymbyv4wwq4")))

(define-public crate-eeric-interpreter-0.1 (crate (name "eeric-interpreter") (vers "0.1.2") (deps (list (crate-dep (name "eeric-core") (req "^0.1.2") (default-features #t) (kind 0)))) (hash "0xr7c4b3fck5q4dl216sfaj6blvgahnkvlb8h9kf2q7hkf70vnbg")))

(define-public crate-eerie-0.1 (crate (name "eerie") (vers "0.1.0") (deps (list (crate-dep (name "anyhow") (req "^1.0.75") (default-features #t) (kind 2)) (crate-dep (name "eerie-sys") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "env_logger") (req "^0.10.1") (default-features #t) (kind 2)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "rusty-fork") (req "^0.3.0") (default-features #t) (kind 2)) (crate-dep (name "test-log") (req "^0.2.12") (default-features #t) (kind 2)) (crate-dep (name "thiserror") (req "^1.0.48") (default-features #t) (kind 0)))) (hash "1p55q7lbxg8cckb7ccn29y32gsz5c2827r81rqfy26y9zdhi0swy") (features (quote (("runtime" "eerie-sys/runtime") ("default" "runtime" "compiler") ("compiler" "eerie-sys/compiler"))))))

(define-public crate-eerie-0.2 (crate (name "eerie") (vers "0.2.0") (deps (list (crate-dep (name "anyhow") (req "^1.0.75") (default-features #t) (kind 2)) (crate-dep (name "eerie-sys") (req "^0.2.0") (kind 0)) (crate-dep (name "env_logger") (req "^0.11.1") (default-features #t) (kind 2)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "rusty-fork") (req "^0.3.0") (default-features #t) (kind 2)) (crate-dep (name "test-log") (req "^0.2.12") (default-features #t) (kind 2)) (crate-dep (name "thiserror") (req "^1.0.48") (optional #t) (default-features #t) (kind 0)))) (hash "1l2ld4wnxrqbvjrwc8l9lswihzs0myc3w9r3c94z772v65fl8pjp") (features (quote (("runtime" "eerie-sys/runtime") ("default" "runtime" "compiler" "std") ("compiler" "eerie-sys/compiler" "std")))) (v 2) (features2 (quote (("std" "dep:thiserror" "eerie-sys/std"))))))

(define-public crate-eerie-0.2 (crate (name "eerie") (vers "0.2.1") (deps (list (crate-dep (name "anyhow") (req "^1.0.75") (default-features #t) (kind 2)) (crate-dep (name "eerie-sys") (req "^0.2.1") (kind 0)) (crate-dep (name "env_logger") (req "^0.11.1") (default-features #t) (kind 2)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "rusty-fork") (req "^0.3.0") (default-features #t) (kind 2)) (crate-dep (name "test-log") (req "^0.2.12") (default-features #t) (kind 2)) (crate-dep (name "thiserror") (req "^1.0.48") (optional #t) (default-features #t) (kind 0)))) (hash "1pifffdz2xs1yqxclxa8db67brhbw7pgilis7887jm8zx1gk4lh4") (features (quote (("runtime" "eerie-sys/runtime") ("default" "runtime" "compiler" "std") ("compiler" "eerie-sys/compiler" "std")))) (v 2) (features2 (quote (("std" "dep:thiserror" "eerie-sys/std"))))))

(define-public crate-eerie-0.2 (crate (name "eerie") (vers "0.2.2") (deps (list (crate-dep (name "anyhow") (req "^1.0.75") (default-features #t) (kind 2)) (crate-dep (name "eerie-sys") (req "^0.2.1") (kind 0)) (crate-dep (name "env_logger") (req "^0.11.1") (default-features #t) (kind 2)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "rusty-fork") (req "^0.3.0") (default-features #t) (kind 2)) (crate-dep (name "test-log") (req "^0.2.12") (default-features #t) (kind 2)) (crate-dep (name "thiserror") (req "^1.0.48") (optional #t) (default-features #t) (kind 0)))) (hash "0sqwm7mfa59xmsqqfdim1apjng3i8vzk9hiq2axqal46vzfpwsag") (features (quote (("runtime" "eerie-sys/runtime") ("default" "runtime" "compiler" "std") ("compiler" "eerie-sys/compiler" "std")))) (v 2) (features2 (quote (("std" "dep:thiserror" "eerie-sys/std"))))))

(define-public crate-eerie-sys-0.1 (crate (name "eerie-sys") (vers "0.1.0") (deps (list (crate-dep (name "bindgen") (req "^0.68.1") (default-features #t) (kind 1)) (crate-dep (name "cmake") (req "^0.1.50") (default-features #t) (kind 1)))) (hash "0jzl3af8bdv676g619nd6nkdh4r5kx185ymzg3p6insz4qhzy99v") (features (quote (("runtime") ("default" "compiler" "runtime") ("compiler"))))))

(define-public crate-eerie-sys-0.2 (crate (name "eerie-sys") (vers "0.2.0") (deps (list (crate-dep (name "bindgen") (req "^0.69.4") (default-features #t) (kind 1)) (crate-dep (name "cmake") (req "^0.1.50") (default-features #t) (kind 1)))) (hash "04i7zbkwkgdrz7prw36v9c58w38jnjv7mxdlfmba6gfp32xb529x") (features (quote (("std") ("runtime") ("default" "compiler" "runtime" "std") ("compiler"))))))

(define-public crate-eerie-sys-0.2 (crate (name "eerie-sys") (vers "0.2.1") (deps (list (crate-dep (name "bindgen") (req "^0.69.4") (default-features #t) (kind 1)) (crate-dep (name "cmake") (req "^0.1.50") (default-features #t) (kind 1)))) (hash "18inw0nck0nlzqyr9nbfnzfzfv0j8hjzjsznwykkdxr80ssdfkk1") (features (quote (("std") ("runtime") ("default" "compiler" "runtime" "std") ("compiler"))))))

(define-public crate-eerie-sys-0.2 (crate (name "eerie-sys") (vers "0.2.2") (deps (list (crate-dep (name "bindgen") (req "^0.69.4") (default-features #t) (kind 1)) (crate-dep (name "cmake") (req "^0.1.50") (default-features #t) (kind 1)))) (hash "12fs9xk8hn4hf6vac04sqkxbhi6qxw87b7kibf4x5b61fnsq80a7") (features (quote (("std") ("runtime") ("default" "compiler" "runtime" "std") ("compiler"))))))

