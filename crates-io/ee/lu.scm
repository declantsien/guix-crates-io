(define-module (crates-io ee lu) #:use-module (crates-io))

(define-public crate-eelu-login-0.1 (crate (name "eelu-login") (vers "0.1.3") (deps (list (crate-dep (name "open") (req "^4.0.1") (default-features #t) (kind 0)) (crate-dep (name "sis-login") (req "^0.2.1") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.27.0") (default-features #t) (kind 0)))) (hash "1wvqm5r93j065kavsi18397ayns1nzbx4dfq3rmp0b1hlvqi6rjk")))

(define-public crate-eelu-login-0.2 (crate (name "eelu-login") (vers "0.2.0") (deps (list (crate-dep (name "open") (req "^4.0.1") (default-features #t) (kind 0)) (crate-dep (name "sis-login") (req "^0.2.3") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.27.0") (default-features #t) (kind 0)))) (hash "0aw9hxy3603zd7xyyhg42c2xbsq9dj3vxxxpf3di3k7rba15q7qz")))

(define-public crate-eelu-login-0.2 (crate (name "eelu-login") (vers "0.2.1") (deps (list (crate-dep (name "open") (req "^4") (default-features #t) (kind 0)) (crate-dep (name "sis-login") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1") (default-features #t) (kind 0)))) (hash "0pw642nmb8smbkwfxjkqg3aaaldmllb15q8cy9ysgbs38ppf098p")))

(define-public crate-eelu-login-0.3 (crate (name "eelu-login") (vers "0.3.0") (deps (list (crate-dep (name "open") (req "^4") (default-features #t) (kind 0)) (crate-dep (name "sis-login") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1") (default-features #t) (kind 0)))) (hash "037myvi8hw00phb2906r2061lnly9368z5nmjfv83i9jj9vghgpl")))

(define-public crate-eelu-login-0.3 (crate (name "eelu-login") (vers "0.3.1") (deps (list (crate-dep (name "open") (req "^5") (default-features #t) (kind 0)) (crate-dep (name "sis-login") (req "^0.2.6") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1") (default-features #t) (kind 0)))) (hash "0r1g420q1p68sm5mf3zqxszw6zvhziw0zn5kxbrvg4g8vphm67xr")))

(define-public crate-eelu-login-0.3 (crate (name "eelu-login") (vers "0.3.2") (deps (list (crate-dep (name "open") (req "^5") (default-features #t) (kind 0)) (crate-dep (name "sis-login") (req "^0.2.8") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1") (default-features #t) (kind 0)))) (hash "1sx9n13rbh2bqh70m6gc6r2rxpybc9zrq7yrpmxlkim796xzk2z4")))

