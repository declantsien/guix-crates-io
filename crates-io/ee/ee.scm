(define-module (crates-io ee ee) #:use-module (crates-io))

(define-public crate-eeeek-0.0.0 (crate (name "eeeek") (vers "0.0.0") (hash "1c6jq1d3jd6i54fcq5hf6fg7jydc81n4g854m0h8v2kz63k3lbhx")))

(define-public crate-eeeek-0.0.1 (crate (name "eeeek") (vers "0.0.1") (hash "1qj4s4vfgbdamiysf01vwz5ssglda05sk1hyig4ak2jxdf6q6yd6")))

