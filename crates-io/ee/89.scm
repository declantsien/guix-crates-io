(define-module (crates-io ee #{89}#) #:use-module (crates-io))

(define-public crate-ee895-0.1 (crate (name "ee895") (vers "0.1.0") (deps (list (crate-dep (name "embedded-hal") (req "^0.2.7") (default-features #t) (kind 0)))) (hash "0rgvr5f0r7qqw4v3kkf3l5qaxl3f0b83g43nb75c2f8m2vsxbc5q")))

