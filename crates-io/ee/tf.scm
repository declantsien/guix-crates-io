(define-module (crates-io ee tf) #:use-module (crates-io))

(define-public crate-eetf-0.1 (crate (name "eetf") (vers "0.1.0") (deps (list (crate-dep (name "byteorder") (req "^0.5") (default-features #t) (kind 0)) (crate-dep (name "flate2") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "num") (req "^0.1") (default-features #t) (kind 0)))) (hash "07mh604rd3az6s8kprliw409170n7cd8rqgf659089sh829lq37v")))

(define-public crate-eetf-0.2 (crate (name "eetf") (vers "0.2.0") (deps (list (crate-dep (name "byteorder") (req "^0.5") (default-features #t) (kind 0)) (crate-dep (name "flate2") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "num") (req "^0.1") (default-features #t) (kind 0)))) (hash "1w51hdd3agin9ihaf1n2pjv4ggbrninfijvlxq7y535rbklmcpjw")))

(define-public crate-eetf-0.3 (crate (name "eetf") (vers "0.3.0") (deps (list (crate-dep (name "byteorder") (req "^0.5") (default-features #t) (kind 0)) (crate-dep (name "flate2") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "num") (req "^0.1") (default-features #t) (kind 0)))) (hash "0h178dkj5dmhrmn4jz1n4yxc36m25n8p30drj98756zxwd49p1fa")))

(define-public crate-eetf-0.3 (crate (name "eetf") (vers "0.3.1") (deps (list (crate-dep (name "byteorder") (req "^0.5") (default-features #t) (kind 0)) (crate-dep (name "flate2") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "num") (req "^0.1") (default-features #t) (kind 0)))) (hash "1hk3rm2v4q3svg14rf7hirm8mp8v1kzjh8c5famxnjg3i8rdm30p")))

(define-public crate-eetf-0.3 (crate (name "eetf") (vers "0.3.2") (deps (list (crate-dep (name "byteorder") (req "^0.5") (default-features #t) (kind 0)) (crate-dep (name "flate2") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "num") (req "^0.1") (default-features #t) (kind 0)))) (hash "1qlwq1a5jjd942r9gb92qslwxvnq06q6ymv6k14x9zll52yahqj6")))

(define-public crate-eetf-0.3 (crate (name "eetf") (vers "0.3.3") (deps (list (crate-dep (name "byteorder") (req "^0.5") (default-features #t) (kind 0)) (crate-dep (name "flate2") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "num") (req "^0.1") (default-features #t) (kind 0)))) (hash "1a7039qxrd1jk4xbg0hn3q34d4ifq6cy1nvcbx6z9xxjlz8ndpdv")))

(define-public crate-eetf-0.3 (crate (name "eetf") (vers "0.3.4") (deps (list (crate-dep (name "byteorder") (req "^0.5") (default-features #t) (kind 0)) (crate-dep (name "libflate") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "num") (req "^0.1") (default-features #t) (kind 0)))) (hash "0ws7fnq8wx1854d33lklim52lxq381jzb899ax4yc8c5bbri90wf")))

(define-public crate-eetf-0.3 (crate (name "eetf") (vers "0.3.5") (deps (list (crate-dep (name "byteorder") (req "^0.5") (default-features #t) (kind 0)) (crate-dep (name "libflate") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "num") (req "^0.1") (default-features #t) (kind 0)))) (hash "0ciqrlzgbjam8ac76wdbsxf2fijn14ys3c1lf6q1pz29j5m90v4r")))

(define-public crate-eetf-0.3 (crate (name "eetf") (vers "0.3.6") (deps (list (crate-dep (name "byteorder") (req "^0.5") (default-features #t) (kind 0)) (crate-dep (name "libflate") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "num") (req "^0.1") (default-features #t) (kind 0)))) (hash "17b7i85rmgsbn88k0585y0fzgys8fbvlnkkgixmnk8fg47jd0zba")))

(define-public crate-eetf-0.4 (crate (name "eetf") (vers "0.4.0") (deps (list (crate-dep (name "byteorder") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "libflate") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "num") (req "^0.2") (default-features #t) (kind 0)))) (hash "02hv5047sb70v069111dqncm6lfg5nmbzkb0rja19sypxpr6kj1x")))

(define-public crate-eetf-0.5 (crate (name "eetf") (vers "0.5.0") (deps (list (crate-dep (name "byteorder") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "libflate") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "num") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1") (default-features #t) (kind 0)))) (hash "1zff821z4m61sx68mzil8fgbqnlvwi0d7p4vghlpn969g6xkpvfr")))

(define-public crate-eetf-0.6 (crate (name "eetf") (vers "0.6.0") (deps (list (crate-dep (name "byteorder") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "libflate") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "num") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1") (default-features #t) (kind 0)))) (hash "1flj59ba0w0g7sncjva8pnbc20aqcnzsnkhl8h5pihm4lbyyzqad")))

(define-public crate-eetf-0.7 (crate (name "eetf") (vers "0.7.0") (deps (list (crate-dep (name "byteorder") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "libflate") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "num") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1") (default-features #t) (kind 0)))) (hash "1lsrqjf6p7rwxidyhax1xs4l4mvz2qimg8q3x061li1k8nx8nnlg")))

(define-public crate-eetf-0.8 (crate (name "eetf") (vers "0.8.0") (deps (list (crate-dep (name "byteorder") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "libflate") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "num") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "ordered-float") (req "^2") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1") (default-features #t) (kind 0)))) (hash "11s8zlw0n98hjm5ndpj0p2lmmpg1r59rdwdzw2vzjwdagxsff8bl")))

(define-public crate-eetf-0.9 (crate (name "eetf") (vers "0.9.0") (deps (list (crate-dep (name "byteorder") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "libflate") (req "^2") (default-features #t) (kind 0)) (crate-dep (name "num") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "ordered-float") (req "^3") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1") (default-features #t) (kind 0)))) (hash "1szblq1iqb5rxfypcwxizx4fdm9alplhyqnd5wj2v17ygha8g3xi")))

