(define-module (crates-io oz _m) #:use-module (crates-io))

(define-public crate-oz_merkle_rs-0.1 (crate (name "oz_merkle_rs") (vers "0.1.0") (deps (list (crate-dep (name "anyhow") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "ethers") (req "^2.0") (default-features #t) (kind 0)))) (hash "0cyrkn3nz2scdbbmvfs8jwjjibr43vq2vrjch865apd4nsq68qnb")))

(define-public crate-oz_merkle_rs-0.1 (crate (name "oz_merkle_rs") (vers "0.1.1") (deps (list (crate-dep (name "alloy-primitives") (req "^0.7.0") (default-features #t) (kind 0)) (crate-dep (name "anyhow") (req "^1.0") (default-features #t) (kind 0)))) (hash "13hrshrckfksndynx981am0wz1iqpyyz0ai8lgqqlnldwdmr19rr")))

(define-public crate-oz_merkle_rs-0.1 (crate (name "oz_merkle_rs") (vers "0.1.2") (deps (list (crate-dep (name "anyhow") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "ethers") (req "^2.0") (default-features #t) (kind 0)))) (hash "0vz5v067srkxv0v5pkc02y0fql0bdqvnjxj29pl7dl7sks2hgx2q")))

(define-public crate-oz_merkle_rs-0.1 (crate (name "oz_merkle_rs") (vers "0.1.3") (deps (list (crate-dep (name "anyhow") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "ethers") (req "^2.0") (default-features #t) (kind 0)))) (hash "0yddd35pbdacwbgqkpkzvy3dwj0k6b8jqibd9igljci0vld3kz2l")))

