(define-module (crates-io oz ar) #:use-module (crates-io))

(define-public crate-ozarc-0.0.0 (crate (name "ozarc") (vers "0.0.0") (deps (list (crate-dep (name "deku") (req "^0.16.0") (features (quote ("logging"))) (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.20") (default-features #t) (kind 0)) (crate-dep (name "tracing") (req "^0.1.40") (default-features #t) (kind 0)))) (hash "004pg40jb2z0s3ppvsfkbjmdc9nh6gvj5yz5krc2k3w08kg1pl8y")))

