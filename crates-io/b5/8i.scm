(define-module (crates-io b5 #{8i}#) #:use-module (crates-io))

(define-public crate-b58ify-0.1 (crate (name "b58ify") (vers "0.1.0") (deps (list (crate-dep (name "once_cell") (req "^1.18.0") (default-features #t) (kind 0)))) (hash "1xa158sw6c88rgxl8r12yp1y3lsrhcxcb6jf51j0lnl1i62h17gm") (yanked #t)))

(define-public crate-b58ify-0.1 (crate (name "b58ify") (vers "0.1.1") (deps (list (crate-dep (name "once_cell") (req "^1.18.0") (default-features #t) (kind 0)))) (hash "11cfv83pxy800q451b5yb7kicwlicmns5l9mk9hz2mwbbm2zq0w7") (yanked #t)))

(define-public crate-b58ify-0.1 (crate (name "b58ify") (vers "0.1.2") (deps (list (crate-dep (name "once_cell") (req "^1.18.0") (default-features #t) (kind 0)))) (hash "1ny52xpp084vfmydm9ga95ik27p8kpxjrn8m6l8565mz9nikyc94") (yanked #t)))

