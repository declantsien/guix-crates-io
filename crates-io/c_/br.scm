(define-module (crates-io c_ br) #:use-module (crates-io))

(define-public crate-c_bridge-0.2 (crate (name "c_bridge") (vers "0.2.0") (hash "1kgxlvidxran30zgwljq7hl8mfny952a4l48mqlykv6v2wj58fxy") (yanked #t)))

(define-public crate-c_bridge-0.3 (crate (name "c_bridge") (vers "0.3.0") (hash "19c3f72v78md0lidrn13izwcpbh1s44ki27angs0zzn8fmlwjjrg") (yanked #t)))

(define-public crate-c_bridge-0.3 (crate (name "c_bridge") (vers "0.3.1") (hash "11hsg3a3c3vma2hgjs8i9l46x8acp4zy5631bwn0mhn31sd31fyl") (yanked #t)))

(define-public crate-c_bridge-0.3 (crate (name "c_bridge") (vers "0.3.2") (hash "1qn907kplbw1cg674swqq174r2m93fwf8aix7fvivlycbricayyf") (yanked #t)))

(define-public crate-c_bridge-0.4 (crate (name "c_bridge") (vers "0.4.0") (hash "1qzc9n1314nfdr6ji22y8k9bwx79mz250g0a0hs4i8477i309qs2") (yanked #t)))

(define-public crate-c_bridge-0.4 (crate (name "c_bridge") (vers "0.4.1") (hash "0abn1rwamii03s7v77amgddckf7dx80scrl59skb2vz54jvywmc9") (yanked #t)))

(define-public crate-c_bridge-0.4 (crate (name "c_bridge") (vers "0.4.2") (hash "0pxhicyzhagsqxz4i6iy7dkzjvni9fhp8413k8l460v2n5j47qg7") (yanked #t)))

(define-public crate-c_bridge-0.5 (crate (name "c_bridge") (vers "0.5.0") (hash "102cg07gxjbh5009ga8ay0vazx28g9di1gm0ivq08wmw0l986w9w") (yanked #t)))

(define-public crate-c_bridge-0.6 (crate (name "c_bridge") (vers "0.6.0") (deps (list (crate-dep (name "cc") (req "^1.0") (optional #t) (default-features #t) (kind 1)))) (hash "0laspv2r7g946vn2dmakv9s0vz96iz6k2d3gqhky6qx25a81pzlh") (features (quote (("test_build" "cc") ("default")))) (yanked #t)))

