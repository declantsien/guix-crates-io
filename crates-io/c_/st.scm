(define-module (crates-io c_ st) #:use-module (crates-io))

(define-public crate-c_str-1 (crate (name "c_str") (vers "1.0.0") (hash "06kafbhbph3qw0sqk6iikbk59snf7k308rk8a79jqa34s44yg8w1")))

(define-public crate-c_str-1 (crate (name "c_str") (vers "1.0.1") (hash "00jz9ij6nvm2vj344gd8srz9rcsjvf78q9cmdbv310qixxnky9gg")))

(define-public crate-c_str-1 (crate (name "c_str") (vers "1.0.2") (hash "193djj4yk2vkg7am6383zjp9apxrqysnzld7835xgw6j5rrv613s")))

(define-public crate-c_str-1 (crate (name "c_str") (vers "1.0.3") (hash "0ncjn7gkwc6xi6hgmzk3fhqr2hid4lqz1ri66nnsl6l7n7h42zlb")))

(define-public crate-c_str-1 (crate (name "c_str") (vers "1.0.4") (hash "1ayw4ddz540lx8a915aipi1mrw1lc9gzljnpgyakb22h3ah2dvqg")))

(define-public crate-c_str-1 (crate (name "c_str") (vers "1.0.5") (hash "0z09wbfxh36i9mygp3p73n4jp6q8whggxdrhkx5plb5mys6kxl86")))

(define-public crate-c_str-1 (crate (name "c_str") (vers "1.0.6") (hash "1lkfnik19bg1wcyy28j6cd8hl95rpbizskxz8npsx5i3qdh7gbw0")))

(define-public crate-c_str-1 (crate (name "c_str") (vers "1.0.7") (hash "0dn3lcm30vpq24rb3n8ghcphcblsqrp1mvbfs36xa46h89ik1y0m")))

(define-public crate-c_str-1 (crate (name "c_str") (vers "1.0.8") (hash "14gm8wqipk152p5idb55d3nhagw857mkk6w2grp81xnx1fp46di0")))

(define-public crate-c_str_macro-1 (crate (name "c_str_macro") (vers "1.0.0") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 2)))) (hash "1k9zb803r7937wsf9iia4vf0cfh7axvniavvzxlxhb0jyhhbmcc9")))

(define-public crate-c_str_macro-1 (crate (name "c_str_macro") (vers "1.0.1") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 2)))) (hash "1khgsis8yjmadzs9hbxwfp8d91dq1vwr5ww0g8hh50jxga6j9prk")))

(define-public crate-c_str_macro-1 (crate (name "c_str_macro") (vers "1.0.2") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 2)))) (hash "1gdmw8wc9d189sxyvyzabgwd6qspld4w84h3yycbzg3vsdl85q7j")))

(define-public crate-c_str_macro-1 (crate (name "c_str_macro") (vers "1.0.3") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 2)))) (hash "1rg51ngv70imiy3qdl2qlf7gnrrg0ncxfrvnbqi9w0b9qi8lkm66")))

(define-public crate-c_string-0.1 (crate (name "c_string") (vers "0.1.0") (hash "0ky7w4bhpqachaqw5xxyr358g32j04l78aydsg25r125iixqxidc")))

(define-public crate-c_string-0.2 (crate (name "c_string") (vers "0.2.0") (hash "0ir5yaw6cdlljrghflvsfkvyqf6bccr7l4dikpmn5ias1gsbyjzi")))

(define-public crate-c_string-0.3 (crate (name "c_string") (vers "0.3.0") (hash "1zl6n5vmyhjprm7p8v8ygif4pwfxq108b0a523vww9vpidzs6rf4")))

(define-public crate-c_string-0.3 (crate (name "c_string") (vers "0.3.1") (hash "1qywh3nkp7b4pj7h2lsgiab3pmpwk61q29y8ry819xs9jnnj35sv")))

(define-public crate-c_string-0.3 (crate (name "c_string") (vers "0.3.2") (hash "1dis4fy8xm99l4w2v0y6g8p4ifnzcsssvbmjwshxranvn3qc9i03")))

(define-public crate-c_string-0.4 (crate (name "c_string") (vers "0.4.0") (hash "0cn9kbq8lpzjyv9w97d8dzb00qpz8frwv1j7nsfayjxi1wwfw979")))

(define-public crate-c_string-0.4 (crate (name "c_string") (vers "0.4.1") (hash "0mhbwcvkr6nf5dvl67cxx4n074svdils0hd3xa07phgbabdaqgav")))

(define-public crate-c_string-0.5 (crate (name "c_string") (vers "0.5.0") (hash "0n48zaic7g6pdahgfilxbwz94p909r4n0jc6lby0nlxpp5h3pjga")))

(define-public crate-c_string-0.5 (crate (name "c_string") (vers "0.5.1") (hash "00d914p122ls0kf5hwm0ivxgq1q1zxwp3diwa79w7vwfdypw32pg")))

(define-public crate-c_string-0.6 (crate (name "c_string") (vers "0.6.0") (hash "1pyq029fy208xmv7by5psq235110ljads8mx4wcgsp4jbs155f9n")))

(define-public crate-c_string-0.6 (crate (name "c_string") (vers "0.6.1") (hash "1s4w4bafpzr9gf5pkl1w9473jsya45nh4x549ndxxhz7vi6wdwdw")))

(define-public crate-c_string-0.6 (crate (name "c_string") (vers "0.6.2") (hash "0kzawsdr97d3ascncyr1gsqnkwa7rlabwpbyqs7iv94xwh4bc5ab")))

(define-public crate-c_string-0.7 (crate (name "c_string") (vers "0.7.0") (deps (list (crate-dep (name "libc") (req "^0.1") (default-features #t) (kind 0)))) (hash "0vp6gqd5s0wj8cazv9qbpkwzbla3f7y60slhy2a5p16z1n9mc8h4")))

(define-public crate-c_string-0.7 (crate (name "c_string") (vers "0.7.1-obsolete") (deps (list (crate-dep (name "libc") (req "^0.1") (default-features #t) (kind 0)))) (hash "1617ya51yv118vnr95rwbknpl48zcampdmf73qbr2nlabxf1fviz")))

(define-public crate-c_string-0.7 (crate (name "c_string") (vers "0.7.2") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "1f0n2szh3jkj0db5l8pgilyqp4vzrn8izdkvc2pc0xpn5r2scl2r")))

