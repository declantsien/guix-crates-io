(define-module (crates-io c_ ut) #:use-module (crates-io))

(define-public crate-c_utf8-0.1 (crate (name "c_utf8") (vers "0.1.0") (deps (list (crate-dep (name "version_check") (req "^0.1") (default-features #t) (kind 1)))) (hash "0kgas4srhgcsfwww74bkb46n16iivdr5ifpwy35vf9nlfljysizp") (features (quote (("try_from") ("std") ("default" "std"))))))

