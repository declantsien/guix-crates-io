(define-module (crates-io c_ ap) #:use-module (crates-io))

(define-public crate-c_api_prefix-0.1 (crate (name "c_api_prefix") (vers "0.1.0") (deps (list (crate-dep (name "proc-macro2") (req "^1.0.36") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.15") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.86") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "1kybafyww4ay7nw8j4zv0i3spm29p590qdsx9snyldjjc65ma3cs")))

