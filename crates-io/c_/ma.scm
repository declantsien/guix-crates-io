(define-module (crates-io c_ ma) #:use-module (crates-io))

(define-public crate-c_macros-0.1 (crate (name "c_macros") (vers "0.1.0") (deps (list (crate-dep (name "col_macros") (req "^0.1") (features (quote ("owned_slice"))) (default-features #t) (kind 0)))) (hash "1vbx4q5h8qmhb8hxpjc1a9s3gr34jw0vvk945c2andy5bn0wc6ab") (yanked #t)))

(define-public crate-c_macros-0.1 (crate (name "c_macros") (vers "0.1.1") (deps (list (crate-dep (name "col_macros") (req "^0.1") (features (quote ("owned_slice"))) (default-features #t) (kind 0)))) (hash "02d1mawa7mzv6pv4i8166dkwrxx8w75xfhw1dsz4kx116fwxhm8n")))

