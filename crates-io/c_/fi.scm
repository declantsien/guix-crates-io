(define-module (crates-io c_ fi) #:use-module (crates-io))

(define-public crate-c_fixed_string-0.1 (crate (name "c_fixed_string") (vers "0.1.0") (deps (list (crate-dep (name "memchr") (req "^2.0.1") (default-features #t) (kind 0)))) (hash "17kamlqzhlvz9wc1s00lva0f0ql0kyvhrd8hq68jkpamjyqqcznk")))

(define-public crate-c_fixed_string-0.2 (crate (name "c_fixed_string") (vers "0.2.0") (deps (list (crate-dep (name "memchr") (req "^2.0.1") (default-features #t) (kind 0)))) (hash "1ks7gcr0klh5jmbi9g0livblnxwalz18vak7q6jd2cmnw6qk6s7y")))

