(define-module (crates-io c_ rs) #:use-module (crates-io))

(define-public crate-c_rs-0.1 (crate (name "c_rs") (vers "0.1.9") (deps (list (crate-dep (name "gcc") (req "^0.3") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "syntex_errors") (req "^0.42.0") (default-features #t) (kind 0)) (crate-dep (name "syntex_pos") (req "^0.42.0") (default-features #t) (kind 0)) (crate-dep (name "syntex_syntax") (req "^0.42") (optional #t) (default-features #t) (kind 0)))) (hash "01ia059d7rwgyprlqwkcyz3i2gy9204pdww17slxjz5rr33mjw8r") (features (quote (("macro") ("build" "gcc" "syntex_syntax")))) (yanked #t)))

