(define-module (crates-io c_ de) #:use-module (crates-io))

(define-public crate-c_defines_to_enum-0.1 (crate (name "c_defines_to_enum") (vers "0.1.0") (deps (list (crate-dep (name "quote") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (default-features #t) (kind 0)))) (hash "17p2xpb5zsbwca28wjqsdhpjbv4679xq71fagxvp07b3hqv3g9ac") (yanked #t)))

(define-public crate-c_defines_to_enum-0.1 (crate (name "c_defines_to_enum") (vers "0.1.1") (deps (list (crate-dep (name "quote") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (default-features #t) (kind 0)))) (hash "1z8ppjd6gf3wghbm2nzin44kbyr2bwvzfc6q5mad7x9mj8lb6gac")))

