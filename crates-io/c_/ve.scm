(define-module (crates-io c_ ve) #:use-module (crates-io))

(define-public crate-c_vec-1 (crate (name "c_vec") (vers "1.0.0") (hash "0qawpxkx71sy795f4x9zr57nfpyq1741fv7msjhx90d8wbbdr070")))

(define-public crate-c_vec-1 (crate (name "c_vec") (vers "1.0.1") (hash "0v1abrh6qp36yc83f9740fh9mvxnhllv02v8gs21rmm21rkqdsqi")))

(define-public crate-c_vec-1 (crate (name "c_vec") (vers "1.0.2") (hash "1y6lpb03y7qv15w7249fdzbq0nn5h50g70ld403m41ady4p53wk9")))

(define-public crate-c_vec-1 (crate (name "c_vec") (vers "1.0.3") (hash "1f027glrk89306av3zdj692gyrmy7w04n1gigp83c1nwbpxpacg7")))

(define-public crate-c_vec-1 (crate (name "c_vec") (vers "1.0.4") (hash "15j9warkdf39rz9lrqc312ynbrfv6a74n70ac0ncipinlwdghikm")))

(define-public crate-c_vec-1 (crate (name "c_vec") (vers "1.0.5") (hash "0n67xa8lqcmx1lpr5x182s0b5adrxbmxq0x81vp7abs5bnz83ywn")))

(define-public crate-c_vec-1 (crate (name "c_vec") (vers "1.0.6") (deps (list (crate-dep (name "libc") (req "^0.1") (default-features #t) (kind 2)))) (hash "06qglwc872zp07dpmbgcbhd02kpxy6l2kvqj714d3k4rbl81h9j7")))

(define-public crate-c_vec-1 (crate (name "c_vec") (vers "1.0.7") (deps (list (crate-dep (name "libc") (req "^0.1") (default-features #t) (kind 2)))) (hash "0qh1687r4p48gp74f577pr734wd623fq6xiz4410bhcg9h1p6j5n")))

(define-public crate-c_vec-1 (crate (name "c_vec") (vers "1.0.8") (deps (list (crate-dep (name "libc") (req "^0.1") (default-features #t) (kind 2)))) (hash "1kbivk3zsa4vqs32ln2k7za5kmn6f175q4v6rp1nxg6hh6j78v2z")))

(define-public crate-c_vec-1 (crate (name "c_vec") (vers "1.0.9") (deps (list (crate-dep (name "libc") (req "^0.1") (default-features #t) (kind 2)))) (hash "134hf704v24khf96msaf64qncspzh2wv08cs4ma7mmzgqlhzbhbv")))

(define-public crate-c_vec-1 (crate (name "c_vec") (vers "1.0.10") (deps (list (crate-dep (name "libc") (req "^0.1") (default-features #t) (kind 2)))) (hash "1sr6nimnrskcw4b4pqa5wfk10dzawqbnmm5ir8xyvhxg632p1cdk")))

(define-public crate-c_vec-1 (crate (name "c_vec") (vers "1.0.11") (deps (list (crate-dep (name "libc") (req "^0.1") (default-features #t) (kind 2)))) (hash "16yx7j5ljwrrmwxgjh2ixjdm6ymzqh21kz2lbgi327grgmp2qq4p")))

(define-public crate-c_vec-1 (crate (name "c_vec") (vers "1.1.0") (deps (list (crate-dep (name "libc") (req "^0.1") (default-features #t) (kind 2)))) (hash "02gkicm59prlhpc2igkvl4gbmclzg6wmancscwdk3z93p04rjqhc")))

(define-public crate-c_vec-1 (crate (name "c_vec") (vers "1.0.12") (deps (list (crate-dep (name "libc") (req "^0.1") (default-features #t) (kind 2)))) (hash "16jvxi6kgmclpv4s7izrxiih3qx5sprzpvqrdzrqkqj9gngiv7ma")))

(define-public crate-c_vec-1 (crate (name "c_vec") (vers "1.2.0") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 2)))) (hash "01y5rllqclky30mskrycfm5i9nng5k2xsyksjg9lnapnb2kgan80")))

(define-public crate-c_vec-1 (crate (name "c_vec") (vers "1.2.1") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 2)))) (hash "04xx43glcys8n554p48aj7gldrk19jb3fr2cq89w508y9ddaqdv2")))

(define-public crate-c_vec-1 (crate (name "c_vec") (vers "1.3.0") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 2)))) (hash "1gfyhjpj9gplh5i2k6i51fdnlj9v05q40rdvs571ma5wmn9npx8i")))

(define-public crate-c_vec-1 (crate (name "c_vec") (vers "1.3.1") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 2)))) (hash "1ias02m5ck6dy93v5xrj5hzqvcykpl4si2jxjb18ph3ym381k3zs")))

(define-public crate-c_vec-1 (crate (name "c_vec") (vers "1.3.2") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 2)))) (hash "1sdly4a13avfj1njkabr4qm7gfp6ah444h8sk6pnm0ffjmgb2ckc")))

(define-public crate-c_vec-1 (crate (name "c_vec") (vers "1.3.3") (deps (list (crate-dep (name "doc-comment") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 2)))) (hash "0c3wgb15h97k6lzfm9qgkwk35ij2ad7w8fb5rbqvalyf3n8ii8zq")))

(define-public crate-c_vec-2 (crate (name "c_vec") (vers "2.0.0") (deps (list (crate-dep (name "doc-comment") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 2)))) (hash "1s765fviy10q27b0wmkyk4q728z9v8v5pdlxv5k564y0mlks9mzx")))

