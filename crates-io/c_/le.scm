(define-module (crates-io c_ le) #:use-module (crates-io))

(define-public crate-c_lexer-0.1 (crate (name "c_lexer") (vers "0.1.0") (deps (list (crate-dep (name "internship") (req "^0.6") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.3") (default-features #t) (kind 0)) (crate-dep (name "phf") (req "^0.7") (features (quote ("macros"))) (default-features #t) (kind 0)))) (hash "0rhsrgc1ms7vlw7gigp4mdz2vfj90zl32r25glw58ds26dyz4xw8")))

(define-public crate-c_lexer-0.1 (crate (name "c_lexer") (vers "0.1.1") (deps (list (crate-dep (name "internship") (req "^0.6") (default-features #t) (kind 0)) (crate-dep (name "phf") (req "^0.7") (features (quote ("macros"))) (default-features #t) (kind 0)))) (hash "047ica7d1spaaf0ir2mxif71dhgz0l2ishwgnsxx92sbqfsqwjpv")))

(define-public crate-c_lexer_logos-0.1 (crate (name "c_lexer_logos") (vers "0.1.0") (deps (list (crate-dep (name "internship") (req "^0.6") (default-features #t) (kind 0)) (crate-dep (name "logos") (req "^0.13") (default-features #t) (kind 0)))) (hash "1pnpl9l4mymjsnvbbivq5b9mrxpx3xr85ifvmgaqp3yc6qy8ml56")))

(define-public crate-c_lexer_logos-0.1 (crate (name "c_lexer_logos") (vers "0.1.1") (deps (list (crate-dep (name "internship") (req "^0.6") (default-features #t) (kind 0)) (crate-dep (name "logos") (req "^0.13") (default-features #t) (kind 0)))) (hash "0rxf9nbzmzq1f9zdb6j6hy5428zmi8r3rp62hv8vzv4i243w0whz")))

