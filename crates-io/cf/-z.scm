(define-module (crates-io cf -z) #:use-module (crates-io))

(define-public crate-cf-zerotrust-gateway-ip-updater-0.1 (crate (name "cf-zerotrust-gateway-ip-updater") (vers "0.1.0") (deps (list (crate-dep (name "native-tls") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "ureq") (req "^2.6") (features (quote ("native-tls" "json"))) (kind 0)))) (hash "0008j6vnji1svlrvi75hh4764qn8ybj3z8r6pb8sx6ymc5jy3hdl")))

