(define-module (crates-io cf e_) #:use-module (crates-io))

(define-public crate-cfe_progmacro-0.1 (crate (name "cfe_progmacro") (vers "0.1.0") (deps (list (crate-dep (name "dotenv") (req "^0.15.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.21") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.105") (default-features #t) (kind 0)))) (hash "17ijqpzp2p98bnwwrqldmbss2i6ix5dhnnjs9xwd7nz919klk8wp")))

(define-public crate-cfe_progmacro-0.1 (crate (name "cfe_progmacro") (vers "0.1.1") (deps (list (crate-dep (name "dotenv") (req "^0.15.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.21") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.105") (default-features #t) (kind 0)))) (hash "1vnyp0chs663nw1kgzfvwliphbhhlh1iwxhg50w6pz3is9s7qbj6")))

