(define-module (crates-io cf p-) #:use-module (crates-io))

(define-public crate-cfp-rs-0.1 (crate (name "cfp-rs") (vers "0.1.0") (deps (list (crate-dep (name "isahc") (req "^0.8.2") (features (quote ("json"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.103") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.44") (default-features #t) (kind 0)))) (hash "1fmppclb60mg7yk8rbac4wh0660h5yk8yld1p5vg3vgak0kxmraf")))

(define-public crate-cfp-rs-0.2 (crate (name "cfp-rs") (vers "0.2.0") (deps (list (crate-dep (name "isahc") (req "^0.8.2") (features (quote ("json"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.103") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.44") (default-features #t) (kind 0)))) (hash "07hhsmsmayv128bmvn3x955ax0rkrmaq7iqhj3yki5ll7pa1fivv")))

