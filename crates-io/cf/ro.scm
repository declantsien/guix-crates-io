(define-module (crates-io cf ro) #:use-module (crates-io))

(define-public crate-cfront-0.1 (crate (name "cfront") (vers "0.1.3") (hash "16vm449ijcag62v7js6g541kk2hv41wjj3wrxlvwpxwjmisk7i30")))

(define-public crate-cfront-definition-0.1 (crate (name "cfront-definition") (vers "0.1.0") (deps (list (crate-dep (name "cfront-definition-keyword") (req "0.1.*") (default-features #t) (kind 0)))) (hash "14nq60a87ml84mxc5l7w9049jczdwsxl64ya7c4fab5lqf3g5r05")))

(define-public crate-cfront-definition-keyword-0.1 (crate (name "cfront-definition-keyword") (vers "0.1.0") (hash "1mrb9d8xcwsxvah5pwww4i8q5dr43vkj9rhz9aj3xcyrlz6q0s2p")))

(define-public crate-cfront-definition-keyword-0.1 (crate (name "cfront-definition-keyword") (vers "0.1.1") (hash "0n6wrf3v595z45gm51fps942ksbpm3hr7idzqcpaj1cg5amf5ggk")))

(define-public crate-cfront-definition-lexer-0.1 (crate (name "cfront-definition-lexer") (vers "0.1.0") (deps (list (crate-dep (name "cfront-definition") (req "0.1.*") (default-features #t) (kind 0)) (crate-dep (name "cfront-definition-keyword") (req "0.1.*") (default-features #t) (kind 0)))) (hash "0fhhj5qn3zpybryfmpfrpl33hp5bcgixs4gjfay11qqjwid435c4")))

