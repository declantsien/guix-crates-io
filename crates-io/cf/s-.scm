(define-module (crates-io cf s-) #:use-module (crates-io))

(define-public crate-cfs-sys-0.1 (crate (name "cfs-sys") (vers "0.1.0") (deps (list (crate-dep (name "bindgen") (req "^0.26.3") (default-features #t) (kind 1)))) (hash "1x0z0p79vv017sg5scyhyrhk077fx7gvxvzdpjhff8cx7ky4v2vy")))

