(define-module (crates-io cf g_) #:use-module (crates-io))

(define-public crate-cfg_aliases-0.1 (crate (name "cfg_aliases") (vers "0.1.0-alpha.0") (hash "0xh2hppjd5kvqmc6xirf5z9vlalyx3q4gnvsxv40jzrip5qwr6h4")))

(define-public crate-cfg_aliases-0.1 (crate (name "cfg_aliases") (vers "0.1.0-alpha.1") (hash "1yyy3g3axrlkz4ig5qdbn934da6fh0xddgh61izljvl298r4r6fs")))

(define-public crate-cfg_aliases-0.1 (crate (name "cfg_aliases") (vers "0.1.0") (hash "0rj8h4bk7grhr0s427pydhlg4sdkp89l7i4l7l65jbjs5xcjpnpn")))

(define-public crate-cfg_aliases-0.1 (crate (name "cfg_aliases") (vers "0.1.1") (hash "17p821nc6jm830vzl2lmwz60g3a30hcm33nk6l257i1rjdqw85px")))

(define-public crate-cfg_aliases-0.2 (crate (name "cfg_aliases") (vers "0.2.0") (hash "03zhvm5vf0bnfmlqjb8rp334l86dxsymkbafkwa7ac3dc69kdrbp")))

(define-public crate-cfg_aliases-0.2 (crate (name "cfg_aliases") (vers "0.2.1") (hash "092pxdc1dbgjb6qvh83gk56rkic2n2ybm4yvy76cgynmzi3zwfk1")))

(define-public crate-cfg_attrs-1 (crate (name "cfg_attrs") (vers "1.0.0") (deps (list (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2.0.39") (default-features #t) (kind 0)))) (hash "1mwfzz9j6fadfvimnq8pzpqsj0v3b2aaxxw3blfcv0mwwy51mg7l")))

(define-public crate-cfg_attrs-1 (crate (name "cfg_attrs") (vers "1.0.1") (deps (list (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2.0.39") (default-features #t) (kind 0)))) (hash "1b855bkkv8znynyyf21pgyzxr9z706qkfxwl2x4c15wa396yhpfg")))

(define-public crate-cfg_attrs-1 (crate (name "cfg_attrs") (vers "1.0.2") (deps (list (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2.0.39") (default-features #t) (kind 0)))) (hash "0xp62hfkc2iyzxgayzii8ga9bnhj6a8c353pslh7yjnb3xvkm7v4")))

(define-public crate-cfg_attrs-2 (crate (name "cfg_attrs") (vers "2.0.0") (deps (list (crate-dep (name "proc-macro2") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2") (default-features #t) (kind 0)))) (hash "0qfh8xm2xaw5n31pz9i9kk1aryg3xk20cq0b7f9945lsgy7lm5i3")))

(define-public crate-cfg_attrs-2 (crate (name "cfg_attrs") (vers "2.1.0") (deps (list (crate-dep (name "proc-macro2") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2") (default-features #t) (kind 0)))) (hash "0ga838sxkyyb3asfdzznwl5daghv87m6pa9mrxqm8y0lfrmri8vg")))

(define-public crate-cfg_attrs-2 (crate (name "cfg_attrs") (vers "2.1.1") (deps (list (crate-dep (name "proc-macro2") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2") (default-features #t) (kind 0)))) (hash "0lvsrzicmp1qbchlzzxp2j8l1lhg7rl4mrzmfzi37nc3hjy6iv6i")))

(define-public crate-cfg_attrs-2 (crate (name "cfg_attrs") (vers "2.1.2") (deps (list (crate-dep (name "proc-macro2") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2") (default-features #t) (kind 0)))) (hash "16xs3d4nfsahxick59b2wxx1w676n6by76c3am8rijpykz33grmx")))

(define-public crate-cfg_attrs-3 (crate (name "cfg_attrs") (vers "3.0.0") (deps (list (crate-dep (name "proc-macro2") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "1qnsrnpg1szs8xrjhcxcyygac98hm2hix6c79i0pq708jxhh9sxc")))

(define-public crate-cfg_block-0.1 (crate (name "cfg_block") (vers "0.1.0") (hash "0pj0166lkpym8dvxcqdy16dl5449imn6fj08yxqknfhwznj6fb7g")))

(define-public crate-cfg_block-0.1 (crate (name "cfg_block") (vers "0.1.1") (hash "11z47bfb5qylcp9ryqbbni6139kdzksqd0vw9wkc6r11jxa80x8q")))

(define-public crate-cfg_block-0.2 (crate (name "cfg_block") (vers "0.2.0") (hash "023im9v3nqhh2pap4cmfcjlk76ypj2v1cyafrl6q1zdlprd8j5lg")))

(define-public crate-cfg_boost-1 (crate (name "cfg_boost") (vers "1.0.0") (hash "0iwr2prq4zqbn7mywsjh28y5s5abahkd8nb8vvffbc4p78jkkxr7")))

(define-public crate-cfg_eval-0.0.0 (crate (name "cfg_eval") (vers "0.0.0") (hash "04dpmmqdai33w9dgdjczmd1h2brzjw55ncqpszinlhf3r3g1syfw")))

(define-public crate-cfg_eval-0.1 (crate (name "cfg_eval") (vers "0.1.0-rc1") (deps (list (crate-dep (name "macro_rules_attribute") (req "^0.2.0") (default-features #t) (kind 2)) (crate-dep (name "proc-macro2") (req "^1.0.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "=2.0.0") (features (quote ("parsing" "printing"))) (kind 0)))) (hash "03z6275dvrpffaj0v4n2dr0vs0ncwj1ivdzs546xqq6fddyj1wgv") (features (quote (("ui-tests" "better-docs") ("docs-rs" "better-docs") ("default") ("better-docs")))) (rust-version "1.61.0")))

(define-public crate-cfg_eval-0.1 (crate (name "cfg_eval") (vers "0.1.0-rc2") (deps (list (crate-dep (name "macro_rules_attribute") (req "^0.2.0") (default-features #t) (kind 2)) (crate-dep (name "proc-macro2") (req "^1.0.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "=2.0.0") (features (quote ("parsing" "printing"))) (kind 0)))) (hash "1783yv1czgzri1m89ldl7q7b62mkbjcag5bv1c0hyvdrpg221r71") (features (quote (("ui-tests" "better-docs") ("docs-rs" "better-docs") ("default") ("better-docs")))) (rust-version "1.61.0")))

(define-public crate-cfg_eval-0.1 (crate (name "cfg_eval") (vers "0.1.0") (deps (list (crate-dep (name "macro_rules_attribute") (req "^0.2.0") (default-features #t) (kind 2)) (crate-dep (name "proc-macro2") (req "^1.0.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "=2.0.0") (features (quote ("parsing" "printing"))) (kind 0)))) (hash "1ambczc2h69sn272z5cl2a2wdbi53mnkzdr644l9pxbynca1rmx8") (features (quote (("ui-tests" "better-docs") ("docs-rs" "better-docs") ("default") ("better-docs")))) (yanked #t) (rust-version "1.61.0")))

(define-public crate-cfg_eval-0.2 (crate (name "cfg_eval") (vers "0.2.0-rc1") (deps (list (crate-dep (name "macro_rules_attribute") (req "^0.2.0") (default-features #t) (kind 2)) (crate-dep (name "proc-macro2") (req "^1.0.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "=2.0.0") (features (quote ("parsing" "printing"))) (kind 0)))) (hash "11an6cq7i1gpq7k4nzaxbd656fqlbwpgbrxgamf1si21jx7sh9zh") (features (quote (("ui-tests" "better-docs") ("items") ("docs-rs" "better-docs") ("default") ("better-docs")))) (yanked #t) (rust-version "1.61.0")))

(define-public crate-cfg_eval-0.1 (crate (name "cfg_eval") (vers "0.1.1") (deps (list (crate-dep (name "macro_rules_attribute") (req "^0.2.0") (default-features #t) (kind 2)) (crate-dep (name "proc-macro2") (req "^1.0.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2.0.0") (features (quote ("parsing" "printing"))) (kind 0)))) (hash "0jz3w3psw1i6hdwv4j5nw8axi9qh650p63qgm52plnk152mqlvm0") (features (quote (("ui-tests" "better-docs") ("docs-rs" "better-docs") ("default") ("better-docs")))) (rust-version "1.61.0")))

(define-public crate-cfg_eval-0.1 (crate (name "cfg_eval") (vers "0.1.2") (deps (list (crate-dep (name "macro_rules_attribute") (req "^0.2.0") (default-features #t) (kind 2)) (crate-dep (name "proc-macro2") (req "^1.0.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2.0.0") (features (quote ("parsing" "printing"))) (kind 0)))) (hash "13zqm1jc0ghc0bj1f33kcqxfa3l1dxvsqcjp9w0rd63b874mymj5") (features (quote (("ui-tests" "better-docs") ("items") ("docs-rs" "better-docs") ("default") ("better-docs")))) (rust-version "1.61.0")))

(define-public crate-cfg_feature_groups-0.1 (crate (name "cfg_feature_groups") (vers "0.1.0") (deps (list (crate-dep (name "toml") (req "^0.5.0") (default-features #t) (kind 0)))) (hash "1bix7ka0b1g68sfa5w012vg0lgvcykwa1385mkrsmwzm4ziylx01")))

(define-public crate-cfg_feature_groups-0.1 (crate (name "cfg_feature_groups") (vers "0.1.1") (deps (list (crate-dep (name "toml") (req "^0.5.0") (default-features #t) (kind 0)))) (hash "1d5fkdf4dff12mp7qmg2kjb5rz7cbh6q4fl1mp7590yyn0kk2jhf")))

(define-public crate-cfg_log-0.1 (crate (name "cfg_log") (vers "0.1.0") (hash "1wxhddjf16z1ghm48kkixrrzdn8m3h393bdjhfzqxsibb55v4470")))

(define-public crate-cfg_log-0.1 (crate (name "cfg_log") (vers "0.1.1") (hash "0z90qqy96zim8ifjcw4xfg8yajdrsxx4bjf1l87jw4cmajk770gr")))

(define-public crate-cfg_matrix-0.1 (crate (name "cfg_matrix") (vers "0.1.0") (deps (list (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2.0") (features (quote ("full" "parsing" "proc-macro"))) (default-features #t) (kind 0)))) (hash "0xb2bm5p6xwm9ws7994fqhlj17nmbc30vd2iij5pa3f69qvspq99") (yanked #t)))

(define-public crate-cfg_matrix-0.1 (crate (name "cfg_matrix") (vers "0.1.1") (deps (list (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2.0") (features (quote ("full" "parsing" "proc-macro"))) (default-features #t) (kind 0)))) (hash "1n3lwn6ihk4cbircfxl0h8qsr0jcc7nh1kh0hh373arw9vhcsnqr")))

(define-public crate-cfg_me-0.1 (crate (name "cfg_me") (vers "0.1.0") (deps (list (crate-dep (name "configure_me") (req "^0.3.3") (default-features #t) (kind 0)) (crate-dep (name "configure_me_codegen") (req "^0.3.12") (features (quote ("man"))) (default-features #t) (kind 0)) (crate-dep (name "configure_me_codegen") (req "^0.3.12") (default-features #t) (kind 1)) (crate-dep (name "serde") (req "^1") (default-features #t) (kind 0)))) (hash "0qz01xd527klaf7l1vdg9004y72272wggvwgwc9wm25ginkjam35")))

(define-public crate-cfg_me-0.1 (crate (name "cfg_me") (vers "0.1.1") (deps (list (crate-dep (name "configure_me") (req "^0.4.0") (default-features #t) (kind 0)) (crate-dep (name "configure_me_codegen") (req "^0.4.1") (features (quote ("man"))) (default-features #t) (kind 0)) (crate-dep (name "configure_me_codegen") (req "^0.4.1") (default-features #t) (kind 1)) (crate-dep (name "serde") (req "^1") (default-features #t) (kind 0)))) (hash "1mw4vbbykh432ky7kdjycdsd6j34ax4xqmfls7kwrsjrq3p2jspv")))

(define-public crate-cfg_mgr-0.1 (crate (name "cfg_mgr") (vers "0.1.0") (hash "0gymbzfm0w10xbw1y0cvmdrzxc99kqs5sw9732xs6mi5rrhym638")))

(define-public crate-cfg_rust_features-0.1 (crate (name "cfg_rust_features") (vers "0.1.0") (deps (list (crate-dep (name "autocfg") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "version_check") (req "^0.9") (default-features #t) (kind 0)))) (hash "1p3f64sy4xzk2mcnvgx93ymar0ip3k1jdb4q3ywi9l3wfn1w9yaw") (rust-version "1.0.0")))

(define-public crate-cfg_rust_features-0.1 (crate (name "cfg_rust_features") (vers "0.1.1") (deps (list (crate-dep (name "autocfg") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "version_check") (req "^0.9") (default-features #t) (kind 0)))) (hash "1nq6pvw6na1a39jszvndmjkqv52182rn4rkzaw73hxik1f1kaz5b") (rust-version "1.0.0")))

(define-public crate-cfg_rust_features-0.1 (crate (name "cfg_rust_features") (vers "0.1.2") (deps (list (crate-dep (name "autocfg") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "version_check") (req "^0.9") (default-features #t) (kind 0)))) (hash "0aypjybdq7niz18i3n11y3v25q7wrak6273q1gw1p29lwc9pjahf") (rust-version "1.0.0")))

(define-public crate-cfg_table-0.1 (crate (name "cfg_table") (vers "0.1.0") (hash "0pq0ibskxwxa9vyazmlbxqnyxfxvzd311q5hi3h30s3rx6rg53k0") (yanked #t)))

(define-public crate-cfg_table-0.1 (crate (name "cfg_table") (vers "0.1.1") (hash "07hjdbfqjkpvxrp4gzb12fdz50hm5y0813pbkjl82j73j416kws9")))

(define-public crate-cfg_table-1 (crate (name "cfg_table") (vers "1.0.0") (hash "18ihbjac021j2w7pr79c97vv4cw09xcha6dxl1lhmisw9vpqnlbq")))

