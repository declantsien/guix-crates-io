(define-module (crates-io cf dt) #:use-module (crates-io))

(define-public crate-CFDTCP-0.1 (crate (name "CFDTCP") (vers "0.1.0") (deps (list (crate-dep (name "aho-corasick") (req "^1.0.1") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2.15") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)) (crate-dep (name "roaring") (req "^0.10.1") (default-features #t) (kind 0)))) (hash "15nznbfxjzazd5d8x0wcclhxqdxcmyry5ad5xx05lhzra0z7g2fa")))

(define-public crate-CFDTCP-0.1 (crate (name "CFDTCP") (vers "0.1.1") (deps (list (crate-dep (name "aho-corasick") (req "^1.0.1") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2.15") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)) (crate-dep (name "roaring") (req "^0.10.1") (default-features #t) (kind 0)))) (hash "06m1klc478mkwlhvv1ma8zf9cyik5g8jfgmnrnmljqnghznadb8w")))

