(define-module (crates-io cf or) #:use-module (crates-io))

(define-public crate-cfor-0.1 (crate (name "cfor") (vers "0.1.0") (hash "0f02v4wm3fdrcxpmngspaz2w6xjlyfy0i111xpckdq7ay90md2vf")))

(define-public crate-cfor-0.1 (crate (name "cfor") (vers "0.1.1") (hash "1x24d47v2pjldyvsysqhg534w938j6xh356hixpxqgx7n4kjyf45")))

(define-public crate-cfor-0.2 (crate (name "cfor") (vers "0.2.0") (hash "1iigxs1zhiba81ch85gnk6rqp72p5gqxdlf5q609ah6jr1hn45yf")))

(define-public crate-cfor-0.2 (crate (name "cfor") (vers "0.2.1") (hash "07dkwvhmyv2k3g6rsmmf07qc149yih129xilbjc5fz7mc8zxdvsv")))

(define-public crate-cfor-1 (crate (name "cfor") (vers "1.0.0") (deps (list (crate-dep (name "rand") (req "^0.3") (default-features #t) (kind 2)))) (hash "0grp6w091dk9waaa5mpzb7qmxxmgddxrzyg6h4zzd63cd38jc1gi") (features (quote (("unstable"))))))

(define-public crate-cfor-1 (crate (name "cfor") (vers "1.1.0") (deps (list (crate-dep (name "rand") (req "^0.3") (default-features #t) (kind 2)))) (hash "018j63r5sr2x5qdw8y5c9p66mpnlsa182h5sx1hk49bnryf02dg0") (features (quote (("unstable"))))))

(define-public crate-cforce-0.0.0 (crate (name "cforce") (vers "0.0.0") (hash "1m23hfn131p9q64skmskxkwsdns24pn0r13ib4pq9si4rm3h7yky")))

(define-public crate-cformat-0.1 (crate (name "cformat") (vers "0.1.0") (deps (list (crate-dep (name "diff-parse") (req "^0.1") (default-features #t) (kind 0)))) (hash "1pr162m9icqbq9cgpc575jxfhsrxljmwalhwclz9g73zvb3brrmy")))

