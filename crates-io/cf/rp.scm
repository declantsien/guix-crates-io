(define-module (crates-io cf rp) #:use-module (crates-io))

(define-public crate-cfrp-0.0.1 (crate (name "cfrp") (vers "0.0.1") (deps (list (crate-dep (name "log") (req "*") (default-features #t) (kind 0)))) (hash "0fc9qpdwk5bkvs2wfrar5pp9agpwbs2w23g93ka78g98f17z7q9c")))

(define-public crate-cfrp-0.0.2 (crate (name "cfrp") (vers "0.0.2") (deps (list (crate-dep (name "env_logger") (req "*") (default-features #t) (kind 0)) (crate-dep (name "log") (req "*") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "time") (req "*") (default-features #t) (kind 0)))) (hash "1phl6l9lxr4j2rpwrk55plgqi762mbfq08vslinz164wlschha82")))

(define-public crate-cfrp-0.0.3 (crate (name "cfrp") (vers "0.0.3") (deps (list (crate-dep (name "env_logger") (req "*") (default-features #t) (kind 0)) (crate-dep (name "log") (req "*") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "*") (default-features #t) (kind 0)) (crate-dep (name "time") (req "*") (default-features #t) (kind 0)))) (hash "1gjsg0alylhl47pqx4ny0bhqa8la0qmy16xady4g3n0x1i6w76rx")))

(define-public crate-cfrp-0.0.4 (crate (name "cfrp") (vers "0.0.4") (deps (list (crate-dep (name "env_logger") (req "*") (default-features #t) (kind 0)) (crate-dep (name "log") (req "*") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "*") (default-features #t) (kind 0)) (crate-dep (name "time") (req "*") (default-features #t) (kind 0)))) (hash "1z7r59c594fci8864f7vkr9rckgyw891dnn1583chcqibrz1mm3j")))

