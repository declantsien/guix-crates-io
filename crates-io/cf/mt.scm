(define-module (crates-io cf mt) #:use-module (crates-io))

(define-public crate-cfmt-0.1 (crate (name "cfmt") (vers "0.1.0") (hash "0ls830kqhvzbbbdb0nlpp2wbd5sbb7qah699sgia3l3jyklp9rgg")))

(define-public crate-cfmt-macros-0.1 (crate (name "cfmt-macros") (vers "0.1.0") (deps (list (crate-dep (name "proc-macro2") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "1mqw2ypja9m2y2547hkxmq4qipq879hwm8fsbzl86vcv95kzxg5d")))

(define-public crate-cfmt-macros-0.2 (crate (name "cfmt-macros") (vers "0.2.0") (deps (list (crate-dep (name "proc-macro2") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "1y9vb1hxb4ia3rn17gcf48p682gdd09c82nwvnrpqfw9lqq68g61")))

(define-public crate-cfmt-macros-0.2 (crate (name "cfmt-macros") (vers "0.2.1") (deps (list (crate-dep (name "proc-macro2") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "06zxjyr7piy7ngngr2dz39ffyw12cvnn8fswq93p28v9n4cvafny")))

