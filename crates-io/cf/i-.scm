(define-module (crates-io cf i-) #:use-module (crates-io))

(define-public crate-cfi-types-0.0.1 (crate (name "cfi-types") (vers "0.0.1") (hash "06fpkda9zk319pcscs3dkyn6w5alzqmnsdryx3ds7k4k4gdgk262")))

(define-public crate-cfi-types-0.0.2 (crate (name "cfi-types") (vers "0.0.2") (hash "1zkcxzvyqp6g3rdc4gmgf1xhr94rgwlvi6ikl2qwm98nw41h5vrd")))

