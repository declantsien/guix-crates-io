(define-module (crates-io cf _r) #:use-module (crates-io))

(define-public crate-cf_rh320u_93_reader-0.3 (crate (name "cf_rh320u_93_reader") (vers "0.3.0") (deps (list (crate-dep (name "rusb") (req "^0.9.1") (default-features #t) (kind 0)))) (hash "1dvl3asakch938c263c37nq36k7lizx03zm1ws2vyhmya9q2gicn")))

(define-public crate-cf_rh320u_93_reader-0.3 (crate (name "cf_rh320u_93_reader") (vers "0.3.1") (deps (list (crate-dep (name "rusb") (req "^0.9.1") (default-features #t) (kind 0)))) (hash "1qkzhlcxzlk3nf9sgcmaj9kiglih8bl6ii4v52gzc9l2v47q2jz0")))

