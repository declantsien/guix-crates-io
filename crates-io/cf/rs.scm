(define-module (crates-io cf rs) #:use-module (crates-io))

(define-public crate-cfrs-1 (crate (name "cfrs") (vers "1.0.0") (deps (list (crate-dep (name "clap") (req "^4.5.4") (features (quote ("derive" "string"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "image") (req "^0.25.1") (optional #t) (default-features #t) (kind 0)))) (hash "19wi8mn210m8ghvl6k36nf8dhbx922has85q91sski2pw9lglqin") (features (quote (("default" "image")))) (v 2) (features2 (quote (("image" "dep:image"))))))

(define-public crate-cfrs-1 (crate (name "cfrs") (vers "1.0.1") (deps (list (crate-dep (name "clap") (req "^4.5.4") (features (quote ("derive" "string"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "image") (req "^0.25.1") (optional #t) (default-features #t) (kind 0)))) (hash "1as4pk1c2i905j54indb3y12vk9npd9sf26jlj2nzxhaahkg6g24") (features (quote (("default" "image")))) (v 2) (features2 (quote (("image" "dep:image"))))))

(define-public crate-cfrs-1 (crate (name "cfrs") (vers "1.0.2") (deps (list (crate-dep (name "clap") (req "^4.5.4") (features (quote ("derive" "string"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "image") (req "^0.25.1") (optional #t) (default-features #t) (kind 0)))) (hash "11zi3bfldqf8l2kyvqmbzxr2dhw1jk4ivasc7jjmi3aca0iiqgv2") (features (quote (("default" "image")))) (v 2) (features2 (quote (("image" "dep:image"))))))

(define-public crate-cfrs-1 (crate (name "cfrs") (vers "1.1.0") (deps (list (crate-dep (name "clap") (req "^4.5.4") (features (quote ("derive" "string"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "image") (req "^0.25.1") (optional #t) (default-features #t) (kind 0)))) (hash "0bs3vac7cbzmpbbbb60sm7w7d2bz1ryzhsv35xkj5zd16q3w1dcj") (features (quote (("default" "image")))) (v 2) (features2 (quote (("image" "dep:image"))))))

