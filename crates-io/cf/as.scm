(define-module (crates-io cf as) #:use-module (crates-io))

(define-public crate-cfasttext-sys-0.1 (crate (name "cfasttext-sys") (vers "0.1.0") (deps (list (crate-dep (name "cc") (req "^1.0.3") (default-features #t) (kind 1)) (crate-dep (name "cmake") (req "^0.1.29") (default-features #t) (kind 1)))) (hash "1vb2jfbf3iyf3v280higmb1h5ggqzrzzxmzygcilxkwyv72p8zxy")))

(define-public crate-cfasttext-sys-0.1 (crate (name "cfasttext-sys") (vers "0.1.1") (deps (list (crate-dep (name "cmake") (req "^0.1") (default-features #t) (kind 1)))) (hash "1zmcaazrccv9dkyqah1lrxdz6q7rqps4dwyzdy79v9qbf04s8glw") (links "cfasttext_static")))

(define-public crate-cfasttext-sys-0.1 (crate (name "cfasttext-sys") (vers "0.1.2") (deps (list (crate-dep (name "cmake") (req "^0.1") (default-features #t) (kind 1)))) (hash "1id8b508xgzvn3avnn8sk99y5ka4ri35ybzm6pgddzkpplhyk11m") (links "cfasttext_static")))

(define-public crate-cfasttext-sys-0.1 (crate (name "cfasttext-sys") (vers "0.1.3") (deps (list (crate-dep (name "cmake") (req "^0.1") (default-features #t) (kind 1)))) (hash "1p87lpx3scgjqmm1bms3ipns4lmcjr2wf8g9lyjf34gw5nkvd9wd") (links "cfasttext_static")))

(define-public crate-cfasttext-sys-0.1 (crate (name "cfasttext-sys") (vers "0.1.4") (deps (list (crate-dep (name "cmake") (req "^0.1") (default-features #t) (kind 1)))) (hash "16782fs05vsax03fv3lshp77ahzx3bigpm46ndbsbbp2f0nrfib4") (links "cfasttext_static")))

(define-public crate-cfasttext-sys-0.1 (crate (name "cfasttext-sys") (vers "0.1.5") (deps (list (crate-dep (name "cmake") (req "^0.1") (default-features #t) (kind 1)))) (hash "0l9nirz7hi0m194r8yjc8df3v5jp24k8n2amh2kz5jd4kyl4rbac") (links "cfasttext_static")))

(define-public crate-cfasttext-sys-0.2 (crate (name "cfasttext-sys") (vers "0.2.0") (deps (list (crate-dep (name "cmake") (req "^0.1") (default-features #t) (kind 1)))) (hash "0vk535zkv83h8bhgk96gig27y42a60pcjq75wpfi6afgmz27cxlf") (links "cfasttext_static")))

(define-public crate-cfasttext-sys-0.3 (crate (name "cfasttext-sys") (vers "0.3.0") (deps (list (crate-dep (name "cmake") (req "^0.1") (default-features #t) (kind 1)))) (hash "0vnc73irpvnf6gw1a41kj89iqajgnyyy43lc3n4kc8n0ls9mlyw7") (links "cfasttext_static")))

(define-public crate-cfasttext-sys-0.4 (crate (name "cfasttext-sys") (vers "0.4.0") (deps (list (crate-dep (name "cmake") (req "^0.1") (default-features #t) (kind 1)))) (hash "07q5752fgzzb4dqzy78skw6wwzrfd5s4nqyjc754yxhdib5yai15") (links "cfasttext_static")))

(define-public crate-cfasttext-sys-0.4 (crate (name "cfasttext-sys") (vers "0.4.1") (deps (list (crate-dep (name "cmake") (req "^0.1") (default-features #t) (kind 1)))) (hash "1gvhbxkms90swjs0q95qwdc1a7rknyldgakifafqxw8zi7kv8anv") (links "cfasttext_static")))

(define-public crate-cfasttext-sys-0.5 (crate (name "cfasttext-sys") (vers "0.5.0") (deps (list (crate-dep (name "cmake") (req "^0.1") (default-features #t) (kind 1)))) (hash "1vn9rd6gjswdlia50iijkqmnkq7f8kvxkimc55piw4p1r7kvjc0w") (links "cfasttext_static")))

(define-public crate-cfasttext-sys-0.5 (crate (name "cfasttext-sys") (vers "0.5.1") (deps (list (crate-dep (name "cmake") (req "^0.1") (default-features #t) (kind 1)))) (hash "1ggkl81bqpk5w61jg3mq2503i5cq2sqdv5mrzc0qf9jih7shxl2v") (links "cfasttext_static")))

(define-public crate-cfasttext-sys-0.5 (crate (name "cfasttext-sys") (vers "0.5.2") (deps (list (crate-dep (name "cmake") (req "^0.1") (default-features #t) (kind 1)))) (hash "09y7kglk966q2xs19zv71dqq5w1rc52fz7ib51dpln8wmf8jqkm3") (links "cfasttext_static")))

(define-public crate-cfasttext-sys-0.6 (crate (name "cfasttext-sys") (vers "0.6.0") (deps (list (crate-dep (name "cmake") (req "^0.1") (default-features #t) (kind 1)))) (hash "1q2jh20hk25sfd1jalc6fj1sriwiyzwrimvy1ahj1qdivbacff39") (links "cfasttext_static")))

(define-public crate-cfasttext-sys-0.7 (crate (name "cfasttext-sys") (vers "0.7.0") (deps (list (crate-dep (name "cmake") (req "^0.1") (default-features #t) (kind 1)))) (hash "0pd3w7dzjbln8xbrwyxcicfrk6zjc60w1cw433imrmqa3gpmacq2") (links "cfasttext_static")))

(define-public crate-cfasttext-sys-0.7 (crate (name "cfasttext-sys") (vers "0.7.1") (deps (list (crate-dep (name "cmake") (req "^0.1") (default-features #t) (kind 1)))) (hash "0rdw86wsl6j2f8n902jl3gild2fwfsyx0im0n3wlqpwp0fkkcbhp") (links "cfasttext_static")))

(define-public crate-cfasttext-sys-0.7 (crate (name "cfasttext-sys") (vers "0.7.2") (deps (list (crate-dep (name "cmake") (req "^0.1") (default-features #t) (kind 1)))) (hash "14knm2njdkdwgkyjmq9l3nj278j8298plrhjim0zp0ljx7r9zp0l") (links "cfasttext_static")))

(define-public crate-cfasttext-sys-0.7 (crate (name "cfasttext-sys") (vers "0.7.3") (deps (list (crate-dep (name "cc") (req "^1.0") (default-features #t) (kind 1)))) (hash "0p5rdnkmzain2mz85qkgw96asw9zgdg0d4nna139xjmmf4nsa4dh") (links "cfasttext_static")))

(define-public crate-cfasttext-sys-0.7 (crate (name "cfasttext-sys") (vers "0.7.4") (deps (list (crate-dep (name "cc") (req "^1.0") (default-features #t) (kind 1)))) (hash "1kj20hf9z8p3ibsa5yfvc8s7vm901cwpgdzcmlg2mx53pipicd26") (links "cfasttext_static")))

(define-public crate-cfasttext-sys-0.7 (crate (name "cfasttext-sys") (vers "0.7.5") (deps (list (crate-dep (name "cc") (req "^1.0") (default-features #t) (kind 1)))) (hash "0nkbwslz65snd3rcx9inz0kfq2b3n04m6890jd2fvaxavalb39df") (links "cfasttext_static")))

(define-public crate-cfasttext-sys-0.7 (crate (name "cfasttext-sys") (vers "0.7.6") (deps (list (crate-dep (name "cc") (req "^1.0") (default-features #t) (kind 1)))) (hash "0annnxpcd9fbp2zha334gmmii1f6if325cjqgp3ffakrxr95j8dk") (links "cfasttext_static")))

(define-public crate-cfasttext-sys-0.7 (crate (name "cfasttext-sys") (vers "0.7.7") (deps (list (crate-dep (name "cc") (req "^1.0") (default-features #t) (kind 1)))) (hash "0sfjlvhpiqbpfgskpyy5h66rcvlzbf97zkvlaffjm9y7d6qkm9zp") (links "cfasttext_static")))

(define-public crate-cfasttext-sys-0.7 (crate (name "cfasttext-sys") (vers "0.7.8") (deps (list (crate-dep (name "cc") (req "^1.0") (default-features #t) (kind 1)))) (hash "0sdkzlbvz7qw6k8j0g2m1vbd4xz06rmy89cwcdhki8nv9spqa8cq") (links "cfasttext_static")))

