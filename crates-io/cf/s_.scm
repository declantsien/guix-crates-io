(define-module (crates-io cf s_) #:use-module (crates-io))

(define-public crate-cfs_openpgp-0.1 (crate (name "cfs_openpgp") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^3.0.0-beta.2") (default-features #t) (kind 0)) (crate-dep (name "nettle") (req "^7.0.1") (default-features #t) (kind 0)) (crate-dep (name "sequoia-openpgp") (req "^1.1.0") (default-features #t) (kind 0)) (crate-dep (name "sha2") (req "^0.9") (default-features #t) (kind 0)))) (hash "0pwrwya336i78kikr298bjzpjxn32xqq4nxggbiw488k7k37hnkl")))

