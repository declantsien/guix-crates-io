(define-module (crates-io cf di) #:use-module (crates-io))

(define-public crate-cfdi_use_mx-0.1 (crate (name "cfdi_use_mx") (vers "0.1.0") (hash "1vkpy99qmm8hrma5zb473j4jrwx2kl5hzy29w96fml8dkivxyn8z")))

(define-public crate-cfdi_use_mx-0.1 (crate (name "cfdi_use_mx") (vers "0.1.1") (hash "0arrky1sbc80f63in2jd7riyzrsby8yb33xj3bjm5k2scf7j4848")))

(define-public crate-cfdi_use_mx-0.2 (crate (name "cfdi_use_mx") (vers "0.2.0") (hash "1zxvmy00j4blq84pyh23nvh1qd4lrbd3pbif69yx2g62mlhqmqwz")))

(define-public crate-cfdi_use_mx-1 (crate (name "cfdi_use_mx") (vers "1.0.0") (deps (list (crate-dep (name "serde") (req "^1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1") (features (quote ("derive"))) (default-features #t) (kind 2)) (crate-dep (name "serde_json") (req "^1") (default-features #t) (kind 2)))) (hash "1javp9ijmgxbnadpf7yfw73q4c6iy6b1hf4a5as33yilkvypj2w0") (features (quote (("default" "serde")))) (v 2) (features2 (quote (("serde" "dep:serde"))))))

