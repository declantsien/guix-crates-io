(define-module (crates-io cf gl) #:use-module (crates-io))

(define-public crate-cfgloader-0.1 (crate (name "cfgloader") (vers "0.1.0") (deps (list (crate-dep (name "dirs") (req "^4") (default-features #t) (kind 0)) (crate-dep (name "polyglot") (req "^0.2.1") (features (quote ("json_fmt" "toml_fmt" "yaml_fmt"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "snafu") (req "^0.6") (default-features #t) (kind 0)))) (hash "0gj4hw8zh90aniwhh58w7ipr0yczs4zjzl8xjymzlxwf0gwv8hbr")))

(define-public crate-cfgloader-0.1 (crate (name "cfgloader") (vers "0.1.1") (deps (list (crate-dep (name "dirs") (req "^4") (default-features #t) (kind 0)) (crate-dep (name "polyglot") (req "^0.2.1") (features (quote ("json_fmt" "toml_fmt" "yaml_fmt"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "snafu") (req "^0.6") (default-features #t) (kind 0)))) (hash "1lmfzs4ffy8hf6l6zvqlvlb80w2m5wr16p6wf2vwmk7sgkzjxf00")))

