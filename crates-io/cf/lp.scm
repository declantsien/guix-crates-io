(define-module (crates-io cf lp) #:use-module (crates-io))

(define-public crate-cflp-0.1 (crate (name "cflp") (vers "0.1.0") (deps (list (crate-dep (name "cflp_macros") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "criterion") (req "^0.4.0") (features (quote ("html_reports"))) (default-features #t) (kind 2)))) (hash "0b6h34w3kgl3j6zr1hsjp1vlw7i5xydd8xjq7khr2mq1klj500i4") (yanked #t)))

(define-public crate-cflp-1 (crate (name "cflp") (vers "1.0.0") (deps (list (crate-dep (name "cflp_macros") (req "^1.0.0") (default-features #t) (kind 0)) (crate-dep (name "criterion") (req "^0.4.0") (features (quote ("html_reports"))) (default-features #t) (kind 2)))) (hash "12rnsmr4yz9vsrdplcfipimnl7kd5sm5l3x0jss2a8z2cl422mn2")))

(define-public crate-cflp-1 (crate (name "cflp") (vers "1.0.1") (deps (list (crate-dep (name "cflp_macros") (req "^1.0.1") (default-features #t) (kind 0)) (crate-dep (name "criterion") (req "^0.5") (features (quote ("html_reports"))) (default-features #t) (kind 2)))) (hash "0df35n64xaym9rigcrhrjdpkvh4h5x9gaav1g8x1mpv7a6hsx0yz")))

(define-public crate-cflp-1 (crate (name "cflp") (vers "1.0.2") (deps (list (crate-dep (name "cflp_macros") (req "^1.0.2") (default-features #t) (kind 0)) (crate-dep (name "criterion") (req "^0.5") (features (quote ("html_reports"))) (default-features #t) (kind 2)))) (hash "0rrq8lym9cz2cr0ybc4z88bn2z727n6vbx2d3h5npr9mdm26lxc3")))

(define-public crate-cflp_macros-0.1 (crate (name "cflp_macros") (vers "0.1.0") (deps (list (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "1dad63a0g2hrcbvz44g13652df7ywadif3fg1v22idc1xcv6hnhi") (yanked #t)))

(define-public crate-cflp_macros-1 (crate (name "cflp_macros") (vers "1.0.0") (deps (list (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2.0") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "0kf7lxcmxafmilqcf09c4qqvfvl8xn390z6r8pnkc4y4xj1xb5z9")))

(define-public crate-cflp_macros-1 (crate (name "cflp_macros") (vers "1.0.1") (deps (list (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2.0") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "0330mnx5djls7vh2p7g2xsj23wlpg2zg9i9hi16zys2p91pwjhfh")))

(define-public crate-cflp_macros-1 (crate (name "cflp_macros") (vers "1.0.2") (deps (list (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2.0") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "0ydz9fwz146xv2lvaqjwg8fzj5zdgf4frn962avs9j0jaz302icv")))

