(define-module (crates-io cf il) #:use-module (crates-io))

(define-public crate-cfile-0.1 (crate (name "cfile") (vers "0.1.0") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "175cd9v01f7zlml5bd0j2z3j6xnknmw3jvyiqwk8wbn6j000mdl3")))

(define-public crate-cfile-0.1 (crate (name "cfile") (vers "0.1.1") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "1g0iwa7z3793k02zkgbxmv9rwk77yq1lph7vlyjbfiqq16288nvr")))

(define-public crate-cfile-0.2 (crate (name "cfile") (vers "0.2.0") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "0gx11aw8k7k8ggcxphhmx0z7bmmal7nh2y44ad7xjx0550i1cnbr")))

(define-public crate-cfile-0.2 (crate (name "cfile") (vers "0.2.1") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "0avbkn395swrs4y1jdajy9g4kx77nm07j5wz0lapwddgpbry0ma2")))

(define-public crate-cfile-0.2 (crate (name "cfile") (vers "0.2.2") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "1y3x0m87n3r02iavpzqdciw1h3802316qm439nw089xak7wlwlr4")))

(define-public crate-cfile-0.2 (crate (name "cfile") (vers "0.2.3") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "0qfkikkl6d5kc94dyvgp71hg8sqykk73p3x6q9w8dgcyps6scw62")))

(define-public crate-cfile-0.4 (crate (name "cfile") (vers "0.4.0") (deps (list (crate-dep (name "cfg-if") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "winapi") (req "^0.3") (features (quote ("impl-debug" "impl-default" "minwindef" "minwinbase" "winbase" "winnt" "fileapi" "handleapi" "processenv"))) (default-features #t) (target "cfg(windows)") (kind 0)))) (hash "0583gv7pzb6f75vimr9d6nxbv7pqmg5h5syajs65aw68xsgb2a8m") (features (quote (("doc") ("default"))))))

(define-public crate-cfile-0.5 (crate (name "cfile") (vers "0.5.0") (deps (list (crate-dep (name "cfg-if") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "foreign-types") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "winapi") (req "^0.3") (features (quote ("impl-debug" "impl-default" "minwindef" "minwinbase" "winbase" "winnt" "fileapi" "handleapi" "processenv"))) (default-features #t) (target "cfg(windows)") (kind 0)))) (hash "1mzmblin4m4pxx8s4ffdkh9vg2q9ra9bzahzwf1gsiw4vyjnay7k") (features (quote (("doc") ("default"))))))

(define-public crate-cfile-0.5 (crate (name "cfile") (vers "0.5.1") (deps (list (crate-dep (name "cfg-if") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "foreign-types") (req "^0.5") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "winapi") (req "^0.3") (features (quote ("impl-debug" "impl-default" "minwindef" "minwinbase" "winbase" "winnt" "fileapi" "handleapi" "processenv"))) (default-features #t) (target "cfg(windows)") (kind 0)))) (hash "1lp5w3s3mbha7s4i1851a4jkm66ax5j7daymsljr4aqlhssm3il4") (features (quote (("doc") ("default"))))))

(define-public crate-cfile-rs-0.1 (crate (name "cfile-rs") (vers "0.1.0") (hash "00c5pvhakq1k8g0zl098bfv6win55whcqd2zcp0z5fy206pg9icj") (yanked #t)))

(define-public crate-cfile-rs-0.2 (crate (name "cfile-rs") (vers "0.2.0") (hash "03i947c2c3sa3in0ihc7kx5w3f7jvx6y3ykw2l7kh6f97qykx6n7") (yanked #t)))

(define-public crate-cfile-rs-0.3 (crate (name "cfile-rs") (vers "0.3.0") (hash "1wb7kr4kqrn819az1ni3dfr22d4w2wz9xwjrs8cshx78hvxqxxyr") (yanked #t)))

(define-public crate-cfile-rs-0.3 (crate (name "cfile-rs") (vers "0.3.1") (hash "0d1dzg6y6lml9h9j4n84ky0zkgxv9abbarfdvz20fxxby3byx4ws") (yanked #t)))

(define-public crate-cfile-rs-0.3 (crate (name "cfile-rs") (vers "0.3.2") (hash "1zx6nwmscbzlpf0lmam91dqqw047c7shw931r8x7n7a92zjynj86") (yanked #t)))

(define-public crate-cfile-rs-0.3 (crate (name "cfile-rs") (vers "0.3.3") (hash "1zm9v5qgx39q560xfqhr2qishm2qra0aj4gyx3ds1f17qsrpgf5b") (yanked #t)))

