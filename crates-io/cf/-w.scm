(define-module (crates-io cf -w) #:use-module (crates-io))

(define-public crate-cf-worker-http-client-0.1 (crate (name "cf-worker-http-client") (vers "0.1.0") (deps (list (crate-dep (name "cookie") (req "^0.16.1") (kind 0)) (crate-dep (name "cookie_store") (req "^0.18.0") (features (quote ("wasm-bindgen"))) (kind 0)) (crate-dep (name "parking_lot") (req "^0.12.1") (kind 0)) (crate-dep (name "url") (req "^2.3.1") (kind 0)) (crate-dep (name "worker") (req "^0.0.11") (kind 0)))) (hash "14hydfs1i7j5bgxj53xmf5hhc3g2p8xwdnv9imyl8939qzqbp1v9") (rust-version "1.63")))

