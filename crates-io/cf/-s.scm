(define-module (crates-io cf -s) #:use-module (crates-io))

(define-public crate-cf-services-0.1 (crate (name "cf-services") (vers "0.1.0") (deps (list (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)))) (hash "0lbi1na3dp1gp9nvvj7zfiiabn2v1cia1j0zpwmkbhg6zdpf0pcx")))

(define-public crate-cf-services-0.1 (crate (name "cf-services") (vers "0.1.1") (deps (list (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)))) (hash "1spfkc5iadj19jp71mwpzanw6qap46w0ymd0bsa6kl1ay3phhfbd")))

