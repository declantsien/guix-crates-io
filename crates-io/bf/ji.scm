(define-module (crates-io bf ji) #:use-module (crates-io))

(define-public crate-bfjit-0.1 (crate (name "bfjit") (vers "0.1.3") (deps (list (crate-dep (name "clap") (req "^4.1.8") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "dynasm") (req "^2.0.0") (default-features #t) (kind 0)) (crate-dep (name "dynasmrt") (req "^2.0.0") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.31") (default-features #t) (kind 0)))) (hash "08sphdy6hflvxhmylmpy0mpqp6qqwks24knl2rxhl11fm3s7mm8h")))

