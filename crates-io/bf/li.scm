(define-module (crates-io bf li) #:use-module (crates-io))

(define-public crate-bflib-0.1 (crate (name "bflib") (vers "0.1.0") (deps (list (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (default-features #t) (kind 0)))) (hash "15c5g28lmgj986785l5lyjvdpkh54yhv95ni07l0r4xr7a6c5kx5")))

(define-public crate-bflib-0.1 (crate (name "bflib") (vers "0.1.1") (deps (list (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (default-features #t) (kind 0)))) (hash "1y0c7xs9abngn7h04w1s11jf55i1s24iyjncslsxnxavpbskdjw0")))

(define-public crate-bflib-0.1 (crate (name "bflib") (vers "0.1.2") (deps (list (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (default-features #t) (kind 0)))) (hash "0ihkz5xg6sakl71jhlw5xpp57c5zxvzvjnl4any2bsaap924w8mx")))

(define-public crate-bflib-0.1 (crate (name "bflib") (vers "0.1.3") (deps (list (crate-dep (name "bflib_proc_macro") (req "^0.1.3") (default-features #t) (kind 0)))) (hash "1hhdv5ryhiwrg2w6fj5achpls79p3fhf6xkbc413i399gjv5zizc")))

(define-public crate-bflib-0.1 (crate (name "bflib") (vers "0.1.4") (deps (list (crate-dep (name "bflib_proc_macro") (req "^0.1.3") (default-features #t) (kind 0)))) (hash "04h0gm6ys0lv2i3wy3a2sax67kxz48fcdb5y1bwn0jjhv0iaglni")))

(define-public crate-bflib-0.1 (crate (name "bflib") (vers "0.1.5") (deps (list (crate-dep (name "bflib_proc_macro") (req "^0.1.5") (default-features #t) (kind 0)))) (hash "1m1gh1a0cv4yx0nlmdaznvbwmphdq8sk95q1gh40wxrnvnp8a5ji")))

(define-public crate-bflib-0.1 (crate (name "bflib") (vers "0.1.6") (deps (list (crate-dep (name "bflib_proc_macro") (req "^0.1.6") (default-features #t) (kind 0)))) (hash "180i29rsy0nwdgs91r3ckddd8yn4nxzimyragfrc83kw7f55ffqa")))

(define-public crate-bflib-0.1 (crate (name "bflib") (vers "0.1.7") (deps (list (crate-dep (name "bflib_proc_macro") (req "^0.1.7") (default-features #t) (kind 0)))) (hash "0qvawnlr7zm6j10sxl08zrph4lc4kc4z8h3jm00gkvj9jsrc08c6")))

(define-public crate-bflib-0.1 (crate (name "bflib") (vers "0.1.8") (deps (list (crate-dep (name "bflib_proc_macro") (req "^0.1.8") (default-features #t) (kind 0)))) (hash "1lgj016wa82kp02mqv021xzqfczvkdv47j81k83s1hcyhi6nahfm") (yanked #t)))

(define-public crate-bflib-0.1 (crate (name "bflib") (vers "0.1.9") (deps (list (crate-dep (name "bflib_proc_macro") (req "^0.1.9") (default-features #t) (kind 0)))) (hash "0avdjlzz5j72gkf80m5yxhpibnl0zvixkp6p3y11ziw7fjwq280d")))

(define-public crate-bflib_proc_macro-0.1 (crate (name "bflib_proc_macro") (vers "0.1.3") (deps (list (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (default-features #t) (kind 0)))) (hash "0yygpbg07j85b81fsg61bq6zzxjghkh5vw09bbcv1d6kcq2l2776")))

(define-public crate-bflib_proc_macro-0.1 (crate (name "bflib_proc_macro") (vers "0.1.4") (deps (list (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (default-features #t) (kind 0)))) (hash "07dm7651rladjxqhym9h09jb4167qxvbqb24prph78hrrz9xxsn3")))

(define-public crate-bflib_proc_macro-0.1 (crate (name "bflib_proc_macro") (vers "0.1.5") (deps (list (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (default-features #t) (kind 0)))) (hash "03jhzpnlpi9x967wylg4339b1nsan77jp1zp6nx9va9kl00gwjci")))

(define-public crate-bflib_proc_macro-0.1 (crate (name "bflib_proc_macro") (vers "0.1.6") (deps (list (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (default-features #t) (kind 0)))) (hash "12adbiwis9syrrwbl08zlf8qjs39g5skff1mdlkszz84izm9gyy7")))

(define-public crate-bflib_proc_macro-0.1 (crate (name "bflib_proc_macro") (vers "0.1.7") (deps (list (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (default-features #t) (kind 0)))) (hash "0cd9jvanddixd81hdc8drick8c6xfdlcqzqwrjfvyyn99lpgnprh")))

(define-public crate-bflib_proc_macro-0.1 (crate (name "bflib_proc_macro") (vers "0.1.8") (deps (list (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (default-features #t) (kind 0)))) (hash "1dnm3zcfc5c41xkdsx1fc2yl6a83rpvx3y8nm3gcw674qpfrh06i")))

(define-public crate-bflib_proc_macro-0.1 (crate (name "bflib_proc_macro") (vers "0.1.9") (deps (list (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (default-features #t) (kind 0)))) (hash "0kvi1sl85966m5w12s0pb1dhgd8yianl51fzqxikdllsc2hx9zhq")))

