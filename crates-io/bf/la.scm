(define-module (crates-io bf la) #:use-module (crates-io))

(define-public crate-bflags-0.1 (crate (name "bflags") (vers "0.1.2") (hash "1mrix6rm33yyfls7d9zcvrlck9hv76ni0hc4snzmprrx1yzahi17")))

(define-public crate-bflags-0.2 (crate (name "bflags") (vers "0.2.0") (hash "0qyrx3a7jx6928kcs0bdcw8hrqsl267p9a12crrs08jbjp25yb6j")))

