(define-module (crates-io bf -b) #:use-module (crates-io))

(define-public crate-bf-bot-0.1 (crate (name "bf-bot") (vers "0.1.0") (deps (list (crate-dep (name "bf-lib") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "serenity") (req "^0.9.0") (features (quote ("client" "gateway" "rustls_backend" "model"))) (kind 0)) (crate-dep (name "tokio") (req "^0.2") (features (quote ("macros"))) (default-features #t) (kind 0)))) (hash "1bkqavg8r9nw82fsj30c3cz3l9wv71i5ppac7dr77vbbk92px42y")))

(define-public crate-bf-bot-0.2 (crate (name "bf-bot") (vers "0.2.0") (deps (list (crate-dep (name "bf-lib") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serenity") (req "^0.9.0") (features (quote ("client" "gateway" "rustls_backend" "model"))) (kind 0)) (crate-dep (name "tokio") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "toml") (req "^0.5") (default-features #t) (kind 0)))) (hash "0xsbnbjfwhqffb0cp8cg4k72r50h5ma8rxrlcfr31gwmaqrl516c")))

