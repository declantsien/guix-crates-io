(define-module (crates-io bf c-) #:use-module (crates-io))

(define-public crate-bfc-rs-1 (crate (name "bfc-rs") (vers "1.1.0") (deps (list (crate-dep (name "argh") (req "^0.1.4") (default-features #t) (kind 0)))) (hash "09wryd6qk6bxk0s8jkjki093pgcsby6dkbax663azf8mqnabac8r")))

