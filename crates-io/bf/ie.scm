(define-module (crates-io bf ie) #:use-module (crates-io))

(define-public crate-bfield-0.3 (crate (name "bfield") (vers "0.3.0") (deps (list (crate-dep (name "bincode") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "criterion") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "mmap-bitvec") (req "^0.4.1") (default-features #t) (kind 0)) (crate-dep (name "murmurhash3") (req "^0.0.5") (default-features #t) (kind 0)) (crate-dep (name "once_cell") (req "^1.3.1") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "tempfile") (req "^3.3.0") (default-features #t) (kind 2)))) (hash "17ksrky17cv4w6r0kbir63ksbmm4z0k3j4lhby71d9kzlssy3w13")))

(define-public crate-bfieldcodec_derive-0.1 (crate (name "bfieldcodec_derive") (vers "0.1.0") (deps (list (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1") (features (quote ("full" "derive"))) (default-features #t) (kind 0)))) (hash "15yfrj9vii9g3m2abbh8qv7v43r87ahz3p1pvkflfyy5iwsa8xap")))

(define-public crate-bfieldcodec_derive-0.2 (crate (name "bfieldcodec_derive") (vers "0.2.0") (deps (list (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1") (features (quote ("full" "derive"))) (default-features #t) (kind 0)))) (hash "1g4m8gyda5ikw8d0dxy60a1ackcmqycbj7ndrzfqpab9x06n4qnc")))

(define-public crate-bfieldcodec_derive-0.3 (crate (name "bfieldcodec_derive") (vers "0.3.0") (deps (list (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1") (features (quote ("full" "derive"))) (default-features #t) (kind 0)))) (hash "0ybwd1nnl3k76bhr2vbipfglc4yddvn2p5s181w56ckqlrfw158m")))

(define-public crate-bfieldcodec_derive-0.4 (crate (name "bfieldcodec_derive") (vers "0.4.0") (deps (list (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1") (features (quote ("full" "derive"))) (default-features #t) (kind 0)))) (hash "1jyvai9vhwqb2yfav1y01cqlrvfgn8ms9rhbh9ws77bpyln3xf4k")))

(define-public crate-bfieldcodec_derive-0.4 (crate (name "bfieldcodec_derive") (vers "0.4.1") (deps (list (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1") (features (quote ("full" "derive"))) (default-features #t) (kind 0)))) (hash "1ivmyza92qi3zhxw9hc5lkvqwmlf6lxjlpsk9xqxc42cmxrs4ljq")))

(define-public crate-bfieldcodec_derive-0.4 (crate (name "bfieldcodec_derive") (vers "0.4.2") (deps (list (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1") (features (quote ("full" "derive"))) (default-features #t) (kind 0)))) (hash "1v27dmf9wm2azqh5wkl6gjnvg6ifqqanf86pr44ppm2c1skm73dh")))

(define-public crate-bfieldcodec_derive-0.5 (crate (name "bfieldcodec_derive") (vers "0.5.0") (deps (list (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2") (features (quote ("full" "derive"))) (default-features #t) (kind 0)))) (hash "0rkng6x676i0n1gg6d4hh5dwxg92s6x0gas4mss4lhn6hmlzxj64")))

(define-public crate-bfieldcodec_derive-0.5 (crate (name "bfieldcodec_derive") (vers "0.5.1") (deps (list (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2") (features (quote ("full" "derive"))) (default-features #t) (kind 0)))) (hash "1p22xs86alrby9zs294b2f63kqh7ahzww65r8bf99mjmifcv7lg7")))

(define-public crate-bfieldcodec_derive-0.5 (crate (name "bfieldcodec_derive") (vers "0.5.2") (deps (list (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2") (features (quote ("full" "derive"))) (default-features #t) (kind 0)))) (hash "0hk3ymzky2hrp4b2wj8mdhhkk95r9g0k321h549inzdrnkx3dclw")))

(define-public crate-bfieldcodec_derive-0.6 (crate (name "bfieldcodec_derive") (vers "0.6.0") (deps (list (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2") (features (quote ("full" "derive"))) (default-features #t) (kind 0)))) (hash "14xvhd0alprdn8mla8y1alhz0ygmjj59xmdk8n948xxifspqfiga")))

(define-public crate-bfieldcodec_derive-0.7 (crate (name "bfieldcodec_derive") (vers "0.7.0") (deps (list (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2") (features (quote ("full" "derive"))) (default-features #t) (kind 0)))) (hash "16vambszxgv62v0f97rbn6hqxmddiggn6s82jzq2n9ssy6lynra2")))

