(define-module (crates-io bf v1) #:use-module (crates-io))

(define-public crate-bfv12-0.1 (crate (name "bfv12") (vers "0.1.0") (deps (list (crate-dep (name "probability") (req "^0.17.0") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.6.5") (default-features #t) (kind 0)))) (hash "02qwfvimaj9xli1y69xsxafpvr5c762q7wrq8m0l1wkyh50iwyda")))

(define-public crate-bfv12-0.1 (crate (name "bfv12") (vers "0.1.1") (deps (list (crate-dep (name "probability") (req "^0.17.0") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.6.5") (default-features #t) (kind 0)))) (hash "1c1d11x074zflz52ww3rq4gixi4lnrrx37kl4gr8i7z7fy23h6ar")))

