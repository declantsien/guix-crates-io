(define-module (crates-io bf co) #:use-module (crates-io))

(define-public crate-bfcomp-0.1 (crate (name "bfcomp") (vers "0.1.0") (deps (list (crate-dep (name "bfcore") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^2.33") (default-features #t) (kind 0)))) (hash "0s720pxmbsc3wjnn2rz8rqnmjjn790qdmx506kj4d6c2z67ngl7d")))

(define-public crate-bfcomp-0.1 (crate (name "bfcomp") (vers "0.1.1") (deps (list (crate-dep (name "bfcore") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^2.33") (default-features #t) (kind 0)))) (hash "173iiia0y2h3dx7ic048gd41lzjij85cc2sr2d02ncc8ca0aijfh")))

(define-public crate-bfcomp-0.1 (crate (name "bfcomp") (vers "0.1.2") (deps (list (crate-dep (name "bfcore") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^2.33") (default-features #t) (kind 0)))) (hash "1v2jmq2bsfrbd5kff1g1qcxfwffxw9mfp8smnhajza9fpmn0qq8h")))

(define-public crate-bfcore-0.1 (crate (name "bfcore") (vers "0.1.0") (hash "0zngi7fx0chdm72qi6nmr0fby6n0k8z24b1zrxsnpf32hyrg1rsm")))

(define-public crate-bfcore-0.1 (crate (name "bfcore") (vers "0.1.1") (hash "014nszbk5hvi8ianv96bqmngi1ymj09irsijdrry0lhpbncimsg1")))

(define-public crate-bfcore-0.1 (crate (name "bfcore") (vers "0.1.2") (hash "0bir5rzqm121if3lq8fjdwp3363zih0djdh0mx1x0fgdym4rdbc2")))

(define-public crate-bfcore-0.2 (crate (name "bfcore") (vers "0.2.0") (hash "0d828gqj8f9jaldjnzl9s6x8rbp5cffxjyia5pnq86xbbngkj7vd")))

