(define-module (crates-io bf es) #:use-module (crates-io))

(define-public crate-bfes-0.1 (crate (name "bfes") (vers "0.1.0") (deps (list (crate-dep (name "itertools") (req "^0.10.3") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)) (crate-dep (name "rayon") (req "^1.5.3") (default-features #t) (kind 0)) (crate-dep (name "safer-ffi") (req "^0.0.10") (features (quote ("proc_macros"))) (default-features #t) (kind 0)))) (hash "18blf0sq079xplwznw06p0js6xl0j4ncwpw4m25j00xs1xz08p5g") (features (quote (("c-headers" "safer-ffi/headers"))))))

