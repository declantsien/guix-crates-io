(define-module (crates-io bf fl) #:use-module (crates-io))

(define-public crate-bfflib-0.6 (crate (name "bfflib") (vers "0.6.0") (deps (list (crate-dep (name "chrono") (req "^0.4.35") (features (quote ("now"))) (kind 0)) (crate-dep (name "file-mode") (req "^0.1.2") (default-features #t) (kind 0)) (crate-dep (name "filetime") (req "^0.2.23") (default-features #t) (kind 0)) (crate-dep (name "normalize-path") (req "^0.2.1") (default-features #t) (kind 0)))) (hash "1nk3zamsc7i7rw8yla51z46w69vcz6zdl392dflynrdnryd7fvzn")))

(define-public crate-bfflib-0.7 (crate (name "bfflib") (vers "0.7.0") (deps (list (crate-dep (name "chrono") (req "^0.4.38") (features (quote ("now"))) (kind 0)) (crate-dep (name "file-mode") (req "^0.1.2") (default-features #t) (kind 0)) (crate-dep (name "filetime") (req "^0.2.23") (default-features #t) (kind 0)) (crate-dep (name "normalize-path") (req "^0.2.1") (default-features #t) (kind 0)) (crate-dep (name "tempfile") (req "^3.10.1") (default-features #t) (kind 2)))) (hash "1y0n1d501j3rcfsgmn0bcaxmj2vsi0w4r11rj2487drxazjq31bb")))

