(define-module (crates-io bf #{3d}#) #:use-module (crates-io))

(define-public crate-bf3d-0.1 (crate (name "bf3d") (vers "0.1.0") (hash "0d8sbybinrzy3ic3dnq5nl4yg5w91i7pq4mr97drmi6ap77hasyf") (yanked #t)))

(define-public crate-bf3d-0.1 (crate (name "bf3d") (vers "0.1.1") (hash "0rlkj4r6wgrwi3jfqp6maigfwdnajd8mz7jm8nmmp5a6lcxx0xig") (yanked #t)))

(define-public crate-bf3d-0.1 (crate (name "bf3d") (vers "0.1.2") (hash "0zj76p3l0988q8dgvs60vf3dkb756j0dzg7lnfq6yhj62yfnr3if") (yanked #t)))

