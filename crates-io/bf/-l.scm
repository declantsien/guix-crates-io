(define-module (crates-io bf -l) #:use-module (crates-io))

(define-public crate-bf-lib-0.1 (crate (name "bf-lib") (vers "0.1.0") (hash "0x7wb2qj8ji1c7j3gsqsjni0nl1y1552f0gx415lsm1xdfbwl9q0")))

(define-public crate-bf-lib-0.1 (crate (name "bf-lib") (vers "0.1.1") (hash "03m3y6v93d8r2d610979y5i14zjd8q17wcrb6328iib8mip1915s")))

(define-public crate-bf-lib-0.2 (crate (name "bf-lib") (vers "0.2.0") (deps (list (crate-dep (name "rand") (req "^0.7") (default-features #t) (kind 0)) (crate-dep (name "subprocess") (req "^0.2.6") (default-features #t) (kind 0)))) (hash "0b8851c6mqq7x8nfy25pkm7bnqh1ha402bm2z2fn22pc7cjvh92m")))

(define-public crate-bf-lib-0.2 (crate (name "bf-lib") (vers "0.2.1") (deps (list (crate-dep (name "rand") (req "^0.7") (default-features #t) (kind 0)) (crate-dep (name "subprocess") (req "^0.2.6") (default-features #t) (kind 0)))) (hash "0b5za2kdx9q36wjzjnsw44iyd6jqmif4phphf3rvq67iqp7n8mmm")))

