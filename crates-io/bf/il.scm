(define-module (crates-io bf il) #:use-module (crates-io))

(define-public crate-bfile-0.1 (crate (name "bfile") (vers "0.1.0") (hash "0m2mznzvs9vfvg40pj0nxc0japyx460vdcgalswsj25sndjc2jv2")))

(define-public crate-bfilters-0.1 (crate (name "bfilters") (vers "0.1.2") (deps (list (crate-dep (name "fasthash") (req "^0.4.0") (default-features #t) (kind 0)))) (hash "0p38yn5xckca7jkgqy19rzz04ar1k853lbss6cayhl7q83v7nq7c")))

(define-public crate-bfilters-0.1 (crate (name "bfilters") (vers "0.1.3") (deps (list (crate-dep (name "fasthash") (req "^0.4.0") (default-features #t) (kind 0)))) (hash "0m2684475x3ld5nr93xj1i73np27abmlyyabvzn2dny256lrrc74")))

(define-public crate-bfilters-0.1 (crate (name "bfilters") (vers "0.1.4") (deps (list (crate-dep (name "fasthash") (req "^0.4.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.151") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.40") (default-features #t) (kind 0)))) (hash "1m04y2d8k0hdznpwwlkkszm7kbrv17jccx6ad5i3dnzyamcqdjk8")))

(define-public crate-bfilters-0.2 (crate (name "bfilters") (vers "0.2.0") (deps (list (crate-dep (name "bitarray-naive") (req "^0.1.1") (default-features #t) (kind 0)) (crate-dep (name "fasthash") (req "^0.4.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.151") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.40") (default-features #t) (kind 0)))) (hash "1625h10n6s38xg5my28n421ycyqjypgxpmy9vsyw6xndiajirlb2")))

