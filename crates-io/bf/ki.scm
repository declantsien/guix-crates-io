(define-module (crates-io bf ki) #:use-module (crates-io))

(define-public crate-bfkit-0.1 (crate (name "bfkit") (vers "0.1.2") (deps (list (crate-dep (name "clap") (req "^2.33.0") (default-features #t) (kind 0)))) (hash "0zy2in63nz3iy82qil6xxpwjqpqsb73l5r1qqf5x0bwl8bl0im9r")))

(define-public crate-bfkit-0.1 (crate (name "bfkit") (vers "0.1.3") (deps (list (crate-dep (name "clap") (req "^2.33.0") (default-features #t) (kind 0)))) (hash "0x3s75yd3h3npf6vdrb1ig2qvgmz9ynwrin73bf1wb1af8d4inqg")))

(define-public crate-bfkit-0.1 (crate (name "bfkit") (vers "0.1.4") (deps (list (crate-dep (name "clap") (req "^2.33.0") (default-features #t) (kind 0)))) (hash "0g5v5x3j9icwmwm4n834yww4s3x5cbarv4zmyrsjziyqqp50bi55")))

