(define-module (crates-io bf gs) #:use-module (crates-io))

(define-public crate-bfgs-0.1 (crate (name "bfgs") (vers "0.1.0") (deps (list (crate-dep (name "ndarray") (req "^0.11") (default-features #t) (kind 0)) (crate-dep (name "spectral") (req "^0.6") (default-features #t) (kind 2)))) (hash "00pzqi0m28f6x4dsqwj41lfcxha0xha73bayf2qkbwwdxd7f73r8")))

