(define-module (crates-io bf -f) #:use-module (crates-io))

(define-public crate-bf-fast-0.0.1 (crate (name "bf-fast") (vers "0.0.1") (hash "1vvqc63lphh99f6l07g5win29blp824wdwc4331spjvmxx5xx3d5")))

(define-public crate-bf-fast-0.0.2 (crate (name "bf-fast") (vers "0.0.2") (hash "1gwbykd5dd8hsg795wx4660ap4z265vz4fky4a80ba0ikzckcar7")))

(define-public crate-bf-fast-0.0.3 (crate (name "bf-fast") (vers "0.0.3") (hash "05q0rlrmdzchmjpacd1i2nk89s833f2sdfnrgqawclqvz6m7bcyf")))

(define-public crate-bf-fast-0.0.4 (crate (name "bf-fast") (vers "0.0.4") (hash "0lhff28vyfdb6hkizcd6iav02d71087p1zr1xmyp8cdsh79w3djp")))

(define-public crate-bf-fast-0.0.5 (crate (name "bf-fast") (vers "0.0.5") (hash "1bhm1jac9iw9l3dvngmqknmkszyk2cg4pgb3pg6fpqxig8himvkv")))

