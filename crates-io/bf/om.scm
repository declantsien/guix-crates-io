(define-module (crates-io bf om) #:use-module (crates-io))

(define-public crate-bfom-0.1 (crate (name "bfom") (vers "0.1.0") (deps (list (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "toml") (req "^0.5") (default-features #t) (kind 0)))) (hash "0vb1r2jblik6bs607z9fi4wl2fdpxlp11f0y6kkmf5hysyjrggx8")))

(define-public crate-bfom-0.1 (crate (name "bfom") (vers "0.1.1") (deps (list (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "toml") (req "^0.5") (default-features #t) (kind 0)))) (hash "1jj7s5ssdi55zq7k0fp5v6d4wkyqk865wypvxh3dd8w257fimixv")))

(define-public crate-bfom-0.1 (crate (name "bfom") (vers "0.1.2") (deps (list (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "toml") (req "^0.5") (default-features #t) (kind 0)))) (hash "1mn5lp5brqxdlg11xgckhbpa3p3ybf3fqbay8drq70nxjrlxz20c")))

(define-public crate-bfom-0.1 (crate (name "bfom") (vers "0.1.13") (deps (list (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "toml") (req "^0.5") (default-features #t) (kind 0)))) (hash "1cbw170k8n4wsagr5ly75vdqvfzpglp44cl90n0qvlls5i35vkk0")))

(define-public crate-bfom-0.1 (crate (name "bfom") (vers "0.1.14") (deps (list (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "toml") (req "^0.5") (default-features #t) (kind 0)))) (hash "0hk35kkn1b2g328rswhi35wjzxwql0sw527mj36fzj7nby9g3cbx")))

(define-public crate-bfom-0.1 (crate (name "bfom") (vers "0.1.17") (deps (list (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "toml") (req "^0.5") (default-features #t) (kind 0)))) (hash "1nvv2i2sl1n7jh0arb3qivqq7bivwcbgldb3140gnjxs5v0smkdy")))

(define-public crate-bfom-0.1 (crate (name "bfom") (vers "0.1.18") (deps (list (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "toml") (req "^0.5") (default-features #t) (kind 0)))) (hash "0hfmdw9wzqcrq06clizlhsfc7sa1g3dlzxbl8cbfmgi3wrjlhnd7")))

(define-public crate-bfom-0.1 (crate (name "bfom") (vers "0.1.19") (deps (list (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "toml") (req "^0.5") (default-features #t) (kind 0)))) (hash "0zd8a31y4383iailmb0df53b75qjfgfwd5cw2skj80y0dh7znvrh")))

(define-public crate-bfom-0.1 (crate (name "bfom") (vers "0.1.20") (deps (list (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "toml") (req "^0.5") (default-features #t) (kind 0)))) (hash "05ck8f0p8ggi0aky7943i5cm3021yh12v4swf0s3k7djrhjvpvf2")))

(define-public crate-bfom-0.1 (crate (name "bfom") (vers "0.1.21") (deps (list (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "toml") (req "^0.5") (default-features #t) (kind 0)))) (hash "0wrbyrc4awbq83c5dzm2q1hs5wl4wpwjhlzlddfqlnv5ax7bdig0")))

(define-public crate-bfom-0.1 (crate (name "bfom") (vers "0.1.22") (deps (list (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "toml") (req "^0.5") (default-features #t) (kind 0)))) (hash "1s0aw2w3y8b8m2nxxrm99saz943id4gsvnh72ih0wivwgpyf9z81")))

(define-public crate-bfom-0.1 (crate (name "bfom") (vers "0.1.26") (deps (list (crate-dep (name "bfom-lib") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "toml") (req "^0.5") (default-features #t) (kind 0)))) (hash "1w14dg4lvvlzss1ldlpmyb4hxsqpsi51i6s38jghcvhbmccfnna4")))

(define-public crate-bfom-0.1 (crate (name "bfom") (vers "0.1.27") (deps (list (crate-dep (name "bfom-lib") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "toml") (req "^0.5") (default-features #t) (kind 0)))) (hash "0kcl1g9n6kndzv36rp3j81dcwjzqq97pyg0m2mncl9bl55maln45")))

(define-public crate-bfom-0.1 (crate (name "bfom") (vers "0.1.28") (deps (list (crate-dep (name "bfom-lib") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "toml") (req "^0.5") (default-features #t) (kind 0)))) (hash "0ir9qq476ad362287fl3vx4v4s646rfwf0vwavykf72zrnmdhd58")))

(define-public crate-bfom-0.1 (crate (name "bfom") (vers "0.1.33") (deps (list (crate-dep (name "bfom-lib") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "toml") (req "^0.5") (default-features #t) (kind 0)))) (hash "1ycdqdy1fk9fddwdpqwzywsa2qbq1z6rlfz43ayv6pcbw0iqiy5m")))

(define-public crate-bfom-0.1 (crate (name "bfom") (vers "0.1.34") (deps (list (crate-dep (name "bfom-lib") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "toml") (req "^0.5") (default-features #t) (kind 0)))) (hash "0la3cdxg68sxpwlli40aqj184wz4a5c5x89yv8j8n90rbn7nmj7z")))

(define-public crate-bfom-0.1 (crate (name "bfom") (vers "0.1.35") (deps (list (crate-dep (name "bfom-lib") (req "^0.1.36") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "toml") (req "^0.5") (default-features #t) (kind 0)))) (hash "01d5nv26239a3kk45c5wkric3rb5npd608b9fmb3h42pqlp0ifpj")))

(define-public crate-bfom-0.1 (crate (name "bfom") (vers "0.1.36") (deps (list (crate-dep (name "bfom-lib") (req "^0.1.37") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "toml") (req "^0.5") (default-features #t) (kind 0)))) (hash "053ykk49xlpfnf6a16v666s6jpsny645xrigbkbvacy13bdybwwx")))

(define-public crate-bfom-0.1 (crate (name "bfom") (vers "0.1.37") (deps (list (crate-dep (name "bfom-lib") (req "^0.1.38") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "toml") (req "^0.5") (default-features #t) (kind 0)))) (hash "0hxrmb9hkcj2bgf6yws985l0kzw1rzv4d02kgc4y6jbw3cacqnb9")))

(define-public crate-bfom-0.1 (crate (name "bfom") (vers "0.1.38") (deps (list (crate-dep (name "bfom-lib") (req "^0.1.44") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "toml") (req "^0.5") (default-features #t) (kind 0)))) (hash "1bkyxkd6d7qvrinmw10pzxjpiw8ybyzbjm2gjsck73zsxzjap3gd")))

(define-public crate-bfom-0.1 (crate (name "bfom") (vers "0.1.39") (deps (list (crate-dep (name "bfom-lib") (req "^0.1.45") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "toml") (req "^0.5") (default-features #t) (kind 0)))) (hash "0lzgcp13z0488c0j86413rvhqypqxb5bbrqnrc0nqk40zyl62y1v")))

(define-public crate-bfom-0.1 (crate (name "bfom") (vers "0.1.40") (deps (list (crate-dep (name "bfom-lib") (req "^0.1.47") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "toml") (req "^0.5") (default-features #t) (kind 0)))) (hash "1p3y6lxipbii27p9wvdn8f19rdjp0c3fs6p401j15fbx9g3b1i05")))

(define-public crate-bfom-0.1 (crate (name "bfom") (vers "0.1.41") (deps (list (crate-dep (name "bfom-lib") (req "^0.1.48") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "toml") (req "^0.5") (default-features #t) (kind 0)))) (hash "19ayq39jw2zkqd2zwnvrgd32073xjcf9csrjqbzlmkwgg30m2276")))

(define-public crate-bfom-0.1 (crate (name "bfom") (vers "0.1.42") (deps (list (crate-dep (name "bfom-lib") (req "^0.1.48") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "toml") (req "^0.5") (default-features #t) (kind 0)))) (hash "1vjznrqymzbh220w905w3jafvqz059z3hbcszpmjj3nm91k91gab")))

(define-public crate-bfom-0.1 (crate (name "bfom") (vers "0.1.43") (deps (list (crate-dep (name "bfom-lib") (req "^0.1.48") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "toml") (req "^0.8") (default-features #t) (kind 0)))) (hash "13fpi5rqfd8ca4mz8yabgcpk2vazc1c2y2s5w63m7danzqiw5l5f")))

(define-public crate-bfom-blog-0.1 (crate (name "bfom-blog") (vers "0.1.0") (deps (list (crate-dep (name "bfom-lib") (req "^0.1.36") (default-features #t) (kind 0)))) (hash "183yrf3jznkggc6ykcfqzl2jrivbya660pwkhwkj8x2lwmbad8pn")))

(define-public crate-bfom-blog-0.1 (crate (name "bfom-blog") (vers "0.1.2") (deps (list (crate-dep (name "bfom-lib") (req "^0.1.37") (default-features #t) (kind 0)))) (hash "0wrz8mg2v52iid24vzflhjn2zjxmmavvsgy6j6q2gy4yjwp7nn4c")))

(define-public crate-bfom-blog-0.1 (crate (name "bfom-blog") (vers "0.1.3") (deps (list (crate-dep (name "bfom-lib") (req "^0.1.38") (default-features #t) (kind 0)))) (hash "021aqc5q77mypkh1qvmlnyhgky2lj1036lh352cmhjhskimcnm6l")))

(define-public crate-bfom-blog-0.1 (crate (name "bfom-blog") (vers "0.1.4") (deps (list (crate-dep (name "bfom-lib") (req "^0.1.40") (default-features #t) (kind 0)))) (hash "1i6xv9db1010k3az3bwn4hdahaqbqd5h9pcvdl47xpjisdln6d2x")))

(define-public crate-bfom-blog-0.1 (crate (name "bfom-blog") (vers "0.1.5") (deps (list (crate-dep (name "bfom-lib") (req "^0.1.41") (default-features #t) (kind 0)))) (hash "0wk19w5w6dyjbwhanvcbqay8dypd4ib21d1gdmhgb5d8hn1va8i3")))

(define-public crate-bfom-blog-0.1 (crate (name "bfom-blog") (vers "0.1.6") (deps (list (crate-dep (name "bfom-lib") (req "^0.1.42") (default-features #t) (kind 0)))) (hash "0dfxdwrp3wny4lvj0966bj5rlbr15vfpdwp0c6j1gkxfn8qnigbx")))

(define-public crate-bfom-blog-0.1 (crate (name "bfom-blog") (vers "0.1.7") (deps (list (crate-dep (name "bfom-lib") (req "^0.1.42") (default-features #t) (kind 0)))) (hash "0jn5asrpskji0y6kbnj5cy6zmdcp6ji2bxmb1r336m4p348hq74s")))

(define-public crate-bfom-blog-0.1 (crate (name "bfom-blog") (vers "0.1.8") (deps (list (crate-dep (name "bfom-lib") (req "^0.1.42") (default-features #t) (kind 0)))) (hash "0vh610r2w7y0zvnln8cqq8xfybi7i0qn99d7xkh5m8mj985zmca9")))

(define-public crate-bfom-blog-0.1 (crate (name "bfom-blog") (vers "0.1.9") (deps (list (crate-dep (name "bfom-lib") (req "^0.1.42") (default-features #t) (kind 0)))) (hash "0y1pmf7p9g88c2ygqiyz8dr4vl5xnm2k41w32qwqs0i6hjz3vyi6")))

(define-public crate-bfom-blog-0.1 (crate (name "bfom-blog") (vers "0.1.10") (deps (list (crate-dep (name "bfom-lib") (req "^0.1.42") (default-features #t) (kind 0)))) (hash "1lz12i9c0zninn56f0xbdd0j1mw26dxgkhs125ws0ldsrxqkki41")))

(define-public crate-bfom-blog-0.1 (crate (name "bfom-blog") (vers "0.1.11") (deps (list (crate-dep (name "bfom-lib") (req "^0.1.42") (default-features #t) (kind 0)))) (hash "1s59wplz695i3kvm65km3vssvh0r45syk23kyha5scb5dcw109qf")))

(define-public crate-bfom-blog-0.1 (crate (name "bfom-blog") (vers "0.1.12") (deps (list (crate-dep (name "bfom-lib") (req "^0.1.42") (default-features #t) (kind 0)))) (hash "1s3szkyldabxc68wq41v4pkf5ahnsy5q2hz5yb8z844c2496n9kl")))

(define-public crate-bfom-blog-0.1 (crate (name "bfom-blog") (vers "0.1.13") (deps (list (crate-dep (name "bfom-lib") (req "^0.1.42") (default-features #t) (kind 0)))) (hash "1w060945ycjqirfyas122bpwp7jjg9qg72hr3anrnnxbijdhl5y9")))

(define-public crate-bfom-blog-0.1 (crate (name "bfom-blog") (vers "0.1.14") (deps (list (crate-dep (name "bfom-lib") (req "^0.1.42") (default-features #t) (kind 0)))) (hash "0200rnpbf7zhmc2b7i6n475ps6pknyf3bcj0mfbyblhsk677265m")))

(define-public crate-bfom-blog-0.1 (crate (name "bfom-blog") (vers "0.1.15") (deps (list (crate-dep (name "bfom-lib") (req "^0.1.44") (default-features #t) (kind 0)))) (hash "1pkrq6gzrmbwl7sqfhd0950lij7p7v6pyxhxmysk8mxivn2y69l6")))

(define-public crate-bfom-blog-0.1 (crate (name "bfom-blog") (vers "0.1.16") (deps (list (crate-dep (name "bfom-lib") (req "^0.1.44") (default-features #t) (kind 0)))) (hash "0agq5cai3nyba2j53rpgbkfdq3dppa6pp0rr2nmhn3addrfsg23j")))

(define-public crate-bfom-blog-0.1 (crate (name "bfom-blog") (vers "0.1.17") (deps (list (crate-dep (name "bfom-lib") (req "^0.1.45") (default-features #t) (kind 0)))) (hash "043yyhyaf1vb6nnq7iy74mpzrhhpjxyjcdq1hs0ck15z64gkyslp")))

(define-public crate-bfom-blog-0.1 (crate (name "bfom-blog") (vers "0.1.18") (deps (list (crate-dep (name "bfom-lib") (req "^0.1.46") (default-features #t) (kind 0)))) (hash "1mdqjgx9ls2ricxs8npq6kvnlwwgby2m44ww9zp44kwkhv3zkara")))

(define-public crate-bfom-blog-0.1 (crate (name "bfom-blog") (vers "0.1.19") (deps (list (crate-dep (name "bfom-lib") (req "^0.1.47") (default-features #t) (kind 0)))) (hash "1cj1hlr7pczfkii3cjb4n4l60hbqp1wf691zhs3f4gp4lvb071fr")))

(define-public crate-bfom-blog-0.1 (crate (name "bfom-blog") (vers "0.1.20") (deps (list (crate-dep (name "bfom-lib") (req "^0.1.48") (default-features #t) (kind 0)))) (hash "1zr0zglarwjk5bfh098kq6l5vkay9knm46afkckr0nxf39nmlr2c")))

(define-public crate-bfom-blog-0.1 (crate (name "bfom-blog") (vers "0.1.21") (deps (list (crate-dep (name "bfom-lib") (req "^0.1.50") (default-features #t) (kind 0)))) (hash "1awzscgwfsg971hb2k2zxrhpzdfs8xgfishvszll5hchjnf7v4hp")))

(define-public crate-bfom-lib-0.1 (crate (name "bfom-lib") (vers "0.1.24") (hash "11bdh5kxnr1d64ygksp1dmcklsvr42x6nfpc069igkdy5r6k3i1g")))

(define-public crate-bfom-lib-0.1 (crate (name "bfom-lib") (vers "0.1.25") (hash "11k15m7my2z5k8k4hgdb2nvj6fmwn35lac32da95lyv0d25bflz6")))

(define-public crate-bfom-lib-0.1 (crate (name "bfom-lib") (vers "0.1.26") (hash "08scs3wgp2gks49c6hvd6icpjl2bnl1pc2cxdxivyvb7df53h11x")))

(define-public crate-bfom-lib-0.1 (crate (name "bfom-lib") (vers "0.1.27") (hash "1rdn76yabl542zwpy5n6jqq8rvb9jz00574bvxhqzf2476jldh3a")))

(define-public crate-bfom-lib-0.1 (crate (name "bfom-lib") (vers "0.1.28") (hash "0s5351r8mrfvmcfsp32rv5cinhvvpdaf02psv21nwns5jin53sxi")))

(define-public crate-bfom-lib-0.1 (crate (name "bfom-lib") (vers "0.1.31") (hash "1f8ppzp3x9cxgy5q68j21k67nyfrn96f90wn3q8jgln1np1c9rwn") (yanked #t)))

(define-public crate-bfom-lib-0.1 (crate (name "bfom-lib") (vers "0.1.33") (hash "1k38afg52m512ramzxq0pjhgw9g4m0izxip5aicg20g9kr40k4yn")))

(define-public crate-bfom-lib-0.1 (crate (name "bfom-lib") (vers "0.1.34") (hash "1a2nibmvh69djq0ag2wfb5dpn9a2lwr696f8ppaz6n5rnflpgv7i")))

(define-public crate-bfom-lib-0.1 (crate (name "bfom-lib") (vers "0.1.35") (hash "15d15kaqzkirw3wcfd5pghnx4yv9lw3np42y52784av92kg9mbhj")))

(define-public crate-bfom-lib-0.1 (crate (name "bfom-lib") (vers "0.1.36") (hash "01gjlqaislj7jl2n6lc6x685gy995mdmskyx90pa9fagkwnc9igf")))

(define-public crate-bfom-lib-0.1 (crate (name "bfom-lib") (vers "0.1.37") (hash "1sm40w6n7rpzwyndx72cccdlg835vvkjjika853i001pnm545d1b")))

(define-public crate-bfom-lib-0.1 (crate (name "bfom-lib") (vers "0.1.38") (hash "19b4p0nscq4c0w8kbmlnjvz45z8ifx3mz6j9rmnpnsmrdfaz1qfi")))

(define-public crate-bfom-lib-0.1 (crate (name "bfom-lib") (vers "0.1.39") (hash "1dq0irxh538y4lz0mmz31mz2kvmmvsbm2966dkhlg9jwqs5q41ww")))

(define-public crate-bfom-lib-0.1 (crate (name "bfom-lib") (vers "0.1.40") (hash "036cj7f8wcmdbx8zvq8r3i3c6wm13rnav3vdrxspaisqlr9ny8zp")))

(define-public crate-bfom-lib-0.1 (crate (name "bfom-lib") (vers "0.1.41") (hash "1gplb0hpw5irvac0vxjsd4pfn3vcsalb4f1za7px3hfaldqyx1q9")))

(define-public crate-bfom-lib-0.1 (crate (name "bfom-lib") (vers "0.1.42") (hash "05fjzlzsxpfsr0m6xyyrv0js3zrcbva10zps0psnccniygblg3si")))

(define-public crate-bfom-lib-0.1 (crate (name "bfom-lib") (vers "0.1.43") (hash "18xdfb3xksjqc02a2h1n95xyfn13ys6nx7lm2k53f6w6zy3qa78d")))

(define-public crate-bfom-lib-0.1 (crate (name "bfom-lib") (vers "0.1.44") (hash "1kms1yq0qwmhah48qsil9fjfbkdiik29x76zcainq7zjw2q6zcf1")))

(define-public crate-bfom-lib-0.1 (crate (name "bfom-lib") (vers "0.1.45") (hash "0cs1k533k5ck2ya6rnh5nx5f247y9ifsaryb8307dssq684vcq0f")))

(define-public crate-bfom-lib-0.1 (crate (name "bfom-lib") (vers "0.1.46") (hash "0ffbr1fglbwicvpxdbj8zbly7kvh0hrq3qpvjzwppfign02pxhxc")))

(define-public crate-bfom-lib-0.1 (crate (name "bfom-lib") (vers "0.1.47") (hash "1p1644ga5khiiqghr4gl6galsxspnra0f04h7g4bkq1kfpahdrvc")))

(define-public crate-bfom-lib-0.1 (crate (name "bfom-lib") (vers "0.1.48") (hash "0yy5jzhicmh1b9w9m24nj1a8vd2jq2g3y3f045ixp1fy73sfpdwm")))

(define-public crate-bfom-lib-0.1 (crate (name "bfom-lib") (vers "0.1.49") (hash "1abhiiyxjz7mgyd2bdq5l5vgny8349ls3lz1dqlnmrkj6k4jxrvr")))

(define-public crate-bfom-lib-0.1 (crate (name "bfom-lib") (vers "0.1.50") (hash "0ijgh3fnxgrknj2a212x4w5ns074q8g4rx7w79nydqsn1ggka2dg")))

