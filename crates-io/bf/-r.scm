(define-module (crates-io bf -r) #:use-module (crates-io))

(define-public crate-bf-rs-0.1 (crate (name "bf-rs") (vers "0.1.0") (hash "1n89wbvshvm0hkqalwy26yb7b95pzrvjkl0ar1sp5m965kwxzr1g")))

(define-public crate-bf-rs-0.1 (crate (name "bf-rs") (vers "0.1.1") (hash "0n2qdb6cxcv5nz4c3yl73ilajilx0cgrcdf2gw7r6ga93smxcgrj")))

