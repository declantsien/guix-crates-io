(define-module (crates-io bf #{-i}#) #:use-module (crates-io))

(define-public crate-bf-impl-1 (crate (name "bf-impl") (vers "1.0.0") (deps (list (crate-dep (name "proc-macro2") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^0.6") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^0.15") (default-features #t) (kind 0)))) (hash "10b7m8rrljnxrr6war6kdqwlnzlbs5dxq8vnxlgq5slkz1cdcxbs") (features (quote (("use_std") ("default" "use_std"))))))

