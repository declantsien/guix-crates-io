(define-module (crates-io bf fs) #:use-module (crates-io))

(define-public crate-bffs-0.0.1 (crate (name "bffs") (vers "0.0.1-prerelease") (hash "1vkw2834y5llk6w37k1m5znrfhw5m6ayb1dmhw1cav1r4jf9f25v")))

(define-public crate-bffs-macros-0.0.1 (crate (name "bffs-macros") (vers "0.0.1-prerelease") (hash "05mj7klj5gh1465d93na612sb7ny0nzqggbzp0358vy8xdgdkbbs")))

