(define-module (crates-io bf -c) #:use-module (crates-io))

(define-public crate-bf-compile-0.1 (crate (name "bf-compile") (vers "0.1.0") (hash "19ykwkvs6fha6c0lhydl77gi6lnc6mfi011jcl9hx5znjayg1vgl")))

(define-public crate-bf-compile-0.1 (crate (name "bf-compile") (vers "0.1.1") (deps (list (crate-dep (name "colored") (req "^2.0.0") (default-features #t) (kind 0)) (crate-dep (name "env_logger") (req "^0.8.3") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.14") (default-features #t) (kind 0)))) (hash "0805pxypws8alziaxkiwp9na6pvn3gqhy3n3lsh84w1mbllqm8wx")))

