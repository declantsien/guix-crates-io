(define-module (crates-io bf uc) #:use-module (crates-io))

(define-public crate-bfuck-0.1 (crate (name "bfuck") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^4.3.11") (features (quote ("cargo"))) (default-features #t) (kind 0)))) (hash "1bmkxlm6703iip9sbabncvzy3wfargk1plp2jxc9ivmq4j6zfp26")))

