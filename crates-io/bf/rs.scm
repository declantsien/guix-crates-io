(define-module (crates-io bf rs) #:use-module (crates-io))

(define-public crate-bfrs-0.0.1 (crate (name "bfrs") (vers "0.0.1") (deps (list (crate-dep (name "ascii") (req "^1.1.0") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^4.2.5") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "colored") (req "^2.0.0") (default-features #t) (kind 0)) (crate-dep (name "winresource") (req "^0.1") (default-features #t) (kind 1)))) (hash "013fvr3a732rbhnk5vbdai3zh06nrxvydxibp3aq69wdli09h7p7")))

(define-public crate-bfrs-0.0.2 (crate (name "bfrs") (vers "0.0.2") (deps (list (crate-dep (name "ascii") (req "^1.1.0") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^4.2.5") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "colored") (req "^2.0.0") (default-features #t) (kind 0)) (crate-dep (name "winresource") (req "^0.1") (default-features #t) (kind 1)))) (hash "1mi29707z6qgwzxwpc3faby3bz12nhrj75w1252kq110pin1djl2")))

(define-public crate-bfrs-0.0.3 (crate (name "bfrs") (vers "0.0.3") (deps (list (crate-dep (name "ascii") (req "^1.1.0") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^4.2.5") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "colored") (req "^2.0.0") (default-features #t) (kind 0)) (crate-dep (name "winresource") (req "^0.1") (default-features #t) (kind 1)))) (hash "1hj5dks2ilk0xrylhbg77yx3fqs2n7q88yzgn5x5vg6b6386w12i")))

