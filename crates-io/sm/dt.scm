(define-module (crates-io sm dt) #:use-module (crates-io))

(define-public crate-smdton-0.1 (crate (name "smdton") (vers "0.1.0") (deps (list (crate-dep (name "json") (req "^0.12.4") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)))) (hash "0f7czgq0vkhbhmg5xh99x339j94465j319sb8wr8zgj0lwdjnlld") (yanked #t)))

(define-public crate-smdton-0.1 (crate (name "smdton") (vers "0.1.1") (deps (list (crate-dep (name "base64") (req "^0.22.0") (default-features #t) (kind 0)) (crate-dep (name "json") (req "^0.12.4") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)))) (hash "0ypprmlgxvkywc2rkay168agq7p28lr1jgvbw9bjkmfdv73876sv")))

(define-public crate-smdton-0.1 (crate (name "smdton") (vers "0.1.2") (deps (list (crate-dep (name "base64") (req "^0.22.0") (default-features #t) (kind 0)) (crate-dep (name "json") (req "^0.12.4") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)))) (hash "0zfc4hgpmxpkx7w0a4i6357590ilq2jw72hi69d7i83wydw2xcyd")))

