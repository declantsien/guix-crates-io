(define-module (crates-io sm _p) #:use-module (crates-io))

(define-public crate-sm_primitives-0.1 (crate (name "sm_primitives") (vers "0.1.0") (deps (list (crate-dep (name "ethereum-types") (req "^0.8") (default-features #t) (kind 0)))) (hash "0gz7aj00a5rfbs8zzf10971mn28y3iysjy76lvj3zhp801nrm9xf")))

