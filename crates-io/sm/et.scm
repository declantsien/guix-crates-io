(define-module (crates-io sm et) #:use-module (crates-io))

(define-public crate-smetamath-3 (crate (name "smetamath") (vers "3.0.0") (deps (list (crate-dep (name "clap") (req "^2.5.2") (default-features #t) (kind 0)) (crate-dep (name "filetime") (req "^0.1.10") (default-features #t) (kind 0)) (crate-dep (name "fnv") (req "^1.0.2") (default-features #t) (kind 0)))) (hash "131f5c9b3pnpa31djbdz20iv8kvvvbmbjb7bd053an90dvbry4s9") (features (quote (("sysalloc"))))))

