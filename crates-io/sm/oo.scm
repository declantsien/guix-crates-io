(define-module (crates-io sm oo) #:use-module (crates-io))

(define-public crate-smoosh-0.1 (crate (name "smoosh") (vers "0.1.0") (deps (list (crate-dep (name "async-compression") (req "^0.4.0") (features (quote ("all-algorithms" "tokio"))) (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.28.2") (features (quote ("rt-multi-thread" "io-util" "test-util" "rt" "macros"))) (default-features #t) (kind 0)))) (hash "1dk3f6l4lq7l5ikf9w76c1m1a48inv97jrzdfcizh3m9fs23xcl4")))

(define-public crate-smoosh-0.1 (crate (name "smoosh") (vers "0.1.1") (deps (list (crate-dep (name "async-compression") (req "^0.4.0") (features (quote ("all-algorithms" "tokio"))) (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.28.2") (features (quote ("rt-multi-thread" "io-util" "test-util" "rt" "macros"))) (default-features #t) (kind 0)))) (hash "10ks7rvzcgm85kczmz780dyvz7g7nlhk0xmwbyg1ndn15ybb8abf")))

(define-public crate-smoosh-0.1 (crate (name "smoosh") (vers "0.1.2") (deps (list (crate-dep (name "async-compression") (req "^0.4.0") (features (quote ("all-algorithms" "tokio"))) (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.28.2") (features (quote ("rt-multi-thread" "io-util" "test-util" "rt" "macros"))) (default-features #t) (kind 0)))) (hash "02psap9ya7pp85mbl629jka8x58aqyjqpxyl2zjmwvpkn5y1fddp")))

(define-public crate-smoosh-0.1 (crate (name "smoosh") (vers "0.1.3") (deps (list (crate-dep (name "async-compression") (req "^0.4.0") (features (quote ("all-algorithms" "tokio"))) (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.28.2") (features (quote ("rt-multi-thread" "io-util" "test-util" "rt" "macros"))) (default-features #t) (kind 0)))) (hash "057dyrz0f3a12an0fb4ljsg3fcn265pcnl07q7qjb6lviv885nky")))

(define-public crate-smoosh-0.2 (crate (name "smoosh") (vers "0.2.0") (deps (list (crate-dep (name "async-compression") (req "^0.4.0") (features (quote ("all-algorithms" "tokio"))) (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.28.2") (features (quote ("rt-multi-thread" "io-util" "test-util" "rt" "macros"))) (default-features #t) (kind 0)))) (hash "00077sd6k3vkvzwyf3b3jsa2w4khgfvq3y187lb0n6qqf29nhg6f")))

(define-public crate-smoosh-0.2 (crate (name "smoosh") (vers "0.2.1") (deps (list (crate-dep (name "async-compression") (req "^0.4.0") (features (quote ("all-algorithms" "tokio"))) (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.28.2") (features (quote ("rt-multi-thread" "io-util" "test-util" "rt" "macros"))) (default-features #t) (kind 0)))) (hash "0kp21s1zxds6ch94ks85yd0kwyd77wm5d5drivfsdmlcnfrllnqc")))

(define-public crate-smooth-0.1 (crate (name "smooth") (vers "0.1.0") (hash "0skvp5q8z25l6rf3abcldi2m9k8qm6cgkw6rlm9iqycw1y4jbw6a")))

(define-public crate-smooth-0.1 (crate (name "smooth") (vers "0.1.1") (hash "0j02nhv0kmz6bmk2x2g823m4hhdyssy1ampaf6597sprjvsiwps5")))

(define-public crate-smooth-0.2 (crate (name "smooth") (vers "0.2.0") (hash "0s4br71lgwg6mhkm993igas6mjr34npmyhc81m4ab2ba3ba1nja0") (rust-version "1.56.1")))

(define-public crate-smooth-bevy-cameras-0.1 (crate (name "smooth-bevy-cameras") (vers "0.1.0") (deps (list (crate-dep (name "approx") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "bevy") (req "^0.5") (features (quote ("render"))) (kind 0)) (crate-dep (name "bevy") (req "^0.5") (features (quote ("bevy_winit" "bevy_wgpu" "bevy_gltf"))) (kind 2)) (crate-dep (name "bevy") (req "^0.5") (features (quote ("x11" "wayland"))) (target "cfg(target_os = \"linux\")") (kind 2)) (crate-dep (name "serde") (req "^1.0") (default-features #t) (kind 0)))) (hash "00pqd2r22dr3njy7l3lq8jhsnhi5c40sjwi5q8yqziz1av944j05")))

(define-public crate-smooth-bevy-cameras-0.2 (crate (name "smooth-bevy-cameras") (vers "0.2.0") (deps (list (crate-dep (name "approx") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "bevy") (req "^0.6") (features (quote ("render"))) (kind 0)) (crate-dep (name "bevy") (req "^0.6") (features (quote ("bevy_winit" "bevy_gltf"))) (kind 2)) (crate-dep (name "bevy") (req "^0.6") (features (quote ("x11" "wayland"))) (target "cfg(target_os = \"linux\")") (kind 2)) (crate-dep (name "serde") (req "^1.0") (default-features #t) (kind 0)))) (hash "1xhhcdwp2ygpgi10zsi39vfbf1isnwmhpr84x106xvpi9inymqk3")))

(define-public crate-smooth-bevy-cameras-0.3 (crate (name "smooth-bevy-cameras") (vers "0.3.0") (deps (list (crate-dep (name "approx") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "bevy") (req "^0.7") (features (quote ("render"))) (kind 0)) (crate-dep (name "bevy") (req "^0.7") (features (quote ("bevy_winit" "bevy_gltf"))) (kind 2)) (crate-dep (name "bevy") (req "^0.7") (features (quote ("x11" "wayland"))) (target "cfg(target_os = \"linux\")") (kind 2)) (crate-dep (name "serde") (req "^1.0") (default-features #t) (kind 0)))) (hash "11is12110iwn068hfj7ywc9lzrm5fhri5g4b5g9k1ywjpndzhk22")))

(define-public crate-smooth-bevy-cameras-0.4 (crate (name "smooth-bevy-cameras") (vers "0.4.0") (deps (list (crate-dep (name "approx") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "bevy") (req "^0.7") (features (quote ("bevy_render"))) (kind 0)) (crate-dep (name "bevy") (req "^0.7") (features (quote ("bevy_core_pipeline" "bevy_pbr" "bevy_winit" "bevy_gltf"))) (kind 2)) (crate-dep (name "bevy") (req "^0.7") (features (quote ("x11" "wayland"))) (target "cfg(target_os = \"linux\")") (kind 2)) (crate-dep (name "serde") (req "^1.0") (default-features #t) (kind 0)))) (hash "01ns83x5zwyxmkjsakrmfy07j0dl6ws88c9x1iw4n8va69rwbh2f")))

(define-public crate-smooth-bevy-cameras-0.5 (crate (name "smooth-bevy-cameras") (vers "0.5.0") (deps (list (crate-dep (name "approx") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "bevy") (req "^0.8") (kind 0)) (crate-dep (name "bevy") (req "^0.8") (features (quote ("bevy_asset" "bevy_core_pipeline" "bevy_pbr" "bevy_winit" "bevy_gltf" "bevy_render"))) (kind 2)) (crate-dep (name "bevy") (req "^0.8") (features (quote ("bevy_asset" "x11" "wayland"))) (target "cfg(target_os = \"linux\")") (kind 2)) (crate-dep (name "serde") (req "^1.0") (default-features #t) (kind 0)))) (hash "0326cz5x3w20y4pk4p6x65gw2wkx17jsdx1ncngx29qvfd3xlksm")))

(define-public crate-smooth-bevy-cameras-0.6 (crate (name "smooth-bevy-cameras") (vers "0.6.0") (deps (list (crate-dep (name "approx") (req "^0.5.1") (default-features #t) (kind 0)) (crate-dep (name "bevy") (req "^0.9") (kind 0)) (crate-dep (name "bevy") (req "^0.9") (features (quote ("bevy_asset" "bevy_core_pipeline" "bevy_pbr" "bevy_winit" "bevy_gltf" "bevy_render"))) (kind 2)) (crate-dep (name "bevy") (req "^0.9") (features (quote ("bevy_asset" "x11" "wayland"))) (target "cfg(target_os = \"linux\")") (kind 2)) (crate-dep (name "serde") (req "^1.0") (default-features #t) (kind 0)))) (hash "1p5ihrb6xqifgjnydx5ddn0knlcgl9v9smsg1mb9pgnc065yvxgk")))

(define-public crate-smooth-bevy-cameras-0.7 (crate (name "smooth-bevy-cameras") (vers "0.7.0") (deps (list (crate-dep (name "approx") (req "^0.5.1") (default-features #t) (kind 0)) (crate-dep (name "bevy") (req "^0.9") (kind 0)) (crate-dep (name "bevy") (req "^0.9") (features (quote ("bevy_asset" "bevy_core_pipeline" "bevy_pbr" "bevy_winit" "bevy_gltf" "bevy_render"))) (kind 2)) (crate-dep (name "bevy") (req "^0.9") (features (quote ("bevy_asset" "x11" "wayland"))) (target "cfg(target_os = \"linux\")") (kind 2)) (crate-dep (name "serde") (req "^1.0") (default-features #t) (kind 0)))) (hash "0dgkm98gx8fq728afb245vb6smk2mwsc3jdnqxc9hmzf0vzsprz8")))

(define-public crate-smooth-bevy-cameras-0.8 (crate (name "smooth-bevy-cameras") (vers "0.8.0") (deps (list (crate-dep (name "approx") (req "^0.5") (default-features #t) (kind 0)) (crate-dep (name "bevy") (req "^0.10") (kind 0)) (crate-dep (name "bevy") (req "^0.10") (features (quote ("bevy_asset" "bevy_core_pipeline" "bevy_pbr" "bevy_winit" "bevy_gltf" "bevy_render"))) (kind 2)) (crate-dep (name "bevy") (req "^0.10") (features (quote ("bevy_asset" "x11" "wayland"))) (target "cfg(target_os = \"linux\")") (kind 2)) (crate-dep (name "serde") (req "^1.0") (optional #t) (default-features #t) (kind 0)))) (hash "1xka2j2f2kvp4qf2rncfdgd93a2b525f9qjpcgmsa6d2f4q27sws")))

(define-public crate-smooth-bevy-cameras-0.9 (crate (name "smooth-bevy-cameras") (vers "0.9.0") (deps (list (crate-dep (name "approx") (req "^0.5") (default-features #t) (kind 0)) (crate-dep (name "bevy") (req "^0.11") (kind 0)) (crate-dep (name "bevy") (req "^0.11") (features (quote ("bevy_asset" "bevy_core_pipeline" "bevy_pbr" "bevy_render" "bevy_winit" "ktx2" "tonemapping_luts" "zstd"))) (kind 2)) (crate-dep (name "bevy") (req "^0.11") (features (quote ("bevy_asset" "bevy_core_pipeline" "bevy_pbr" "bevy_render" "bevy_winit" "ktx2" "tonemapping_luts" "wayland" "x11" "zstd"))) (target "cfg(target_os = \"linux\")") (kind 2)) (crate-dep (name "serde") (req "^1.0") (optional #t) (default-features #t) (kind 0)))) (hash "0x7xcgxa6a34z84i50cq9hj4jwj2wcr9d5808iix09s5354qhpam")))

(define-public crate-smooth-bevy-cameras-0.10 (crate (name "smooth-bevy-cameras") (vers "0.10.0") (deps (list (crate-dep (name "approx") (req "^0.5") (default-features #t) (kind 0)) (crate-dep (name "bevy") (req "^0.12") (kind 0)) (crate-dep (name "bevy") (req "^0.12") (features (quote ("bevy_asset" "bevy_core_pipeline" "bevy_pbr" "bevy_render" "bevy_winit" "ktx2" "tonemapping_luts" "zstd"))) (kind 2)) (crate-dep (name "bevy") (req "^0.12") (features (quote ("bevy_asset" "bevy_core_pipeline" "bevy_pbr" "bevy_render" "bevy_winit" "ktx2" "tonemapping_luts" "wayland" "x11" "zstd"))) (target "cfg(target_os = \"linux\")") (kind 2)) (crate-dep (name "serde") (req "^1.0") (optional #t) (default-features #t) (kind 0)))) (hash "0zvlzyjffcxyn5d9bn35j6453xyal7s51363vad5asyibqhcg0xb")))

(define-public crate-smooth-bevy-cameras-0.11 (crate (name "smooth-bevy-cameras") (vers "0.11.0") (deps (list (crate-dep (name "approx") (req "^0.5") (default-features #t) (kind 0)) (crate-dep (name "bevy") (req "^0.13") (kind 0)) (crate-dep (name "bevy") (req "^0.13") (features (quote ("bevy_asset" "bevy_core_pipeline" "bevy_pbr" "bevy_render" "bevy_winit" "ktx2" "tonemapping_luts" "zstd"))) (kind 2)) (crate-dep (name "bevy") (req "^0.13") (features (quote ("bevy_asset" "bevy_core_pipeline" "bevy_pbr" "bevy_render" "bevy_winit" "ktx2" "tonemapping_luts" "x11" "zstd"))) (target "cfg(target_os = \"linux\")") (kind 2)) (crate-dep (name "serde") (req "^1.0") (optional #t) (default-features #t) (kind 0)))) (hash "0cishq4kx4c4s037yp19madxidym41jm09ggi57nly6zl10prn1g")))

(define-public crate-smooth-json-0.1 (crate (name "smooth-json") (vers "0.1.0") (deps (list (crate-dep (name "serde_json") (req "^1.0.114") (default-features #t) (kind 0)))) (hash "0c0pv2qxqaw3bpgx8mf8cwmnj1fjyk1ryxxv0v5mbysqnhvn3mgn")))

(define-public crate-smooth-json-0.2 (crate (name "smooth-json") (vers "0.2.0") (deps (list (crate-dep (name "serde_json") (req "^1.0.114") (default-features #t) (kind 0)))) (hash "0p26s5bfmf8jyf4zz0jj2qk9kaz9761wyrpg07mr55fprf5js3dl")))

(define-public crate-smooth-json-0.2 (crate (name "smooth-json") (vers "0.2.1") (deps (list (crate-dep (name "serde_json") (req "^1.0.114") (default-features #t) (kind 0)))) (hash "0i9awllzq898nv9dwnl8dqkfw28511wrcv2pnglbwwsdzn06h89p")))

(define-public crate-smooth-json-0.2 (crate (name "smooth-json") (vers "0.2.2") (deps (list (crate-dep (name "serde_json") (req "^1.0.114") (default-features #t) (kind 0)))) (hash "1ynx25wdvkm897067r24bbpgw9kpjgzay0b9kxrc0hb3153jcs99")))

(define-public crate-smooth-json-0.2 (crate (name "smooth-json") (vers "0.2.3") (deps (list (crate-dep (name "serde_json") (req "^1.0.114") (default-features #t) (kind 0)))) (hash "164n59vrbmp9svghb3c4rzbzzr0brk7401v52cjylksjcv6ix7vi")))

(define-public crate-smooth-json-0.2 (crate (name "smooth-json") (vers "0.2.4") (deps (list (crate-dep (name "serde_json") (req "^1.0.114") (default-features #t) (kind 0)))) (hash "18ybnfcyr9hs34f5z8smcvn5nm9bgx51nrv0vcw3nljdv7k5jvki")))

(define-public crate-smooth-json-0.2 (crate (name "smooth-json") (vers "0.2.5") (deps (list (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)))) (hash "0nxn9vbi2v15bi21cnw5snjngzz7sg24m4g7r8sfwjhgq323xv0w")))

(define-public crate-smooth-json-0.2 (crate (name "smooth-json") (vers "0.2.6") (deps (list (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)))) (hash "0yjr27d1mrv6v40p79xc7gcml0mm27swncya4f6hy7vky8bi41c2")))

(define-public crate-smooth-numbers-0.1 (crate (name "smooth-numbers") (vers "0.1.0") (deps (list (crate-dep (name "primal") (req "^0.3.2") (default-features #t) (kind 0)))) (hash "11i67xg2qia59i37vj3xqf34dmag97vsks667aqw0bng1w0r0ych")))

(define-public crate-smooth-numbers-0.2 (crate (name "smooth-numbers") (vers "0.2.0") (deps (list (crate-dep (name "primal") (req "^0.3.2") (default-features #t) (kind 0)))) (hash "1ra6q07mkiqjjw7kfkcyjhsdibsia7vqdkd8wrc9jgxc760mmz8s")))

(define-public crate-smooth-numbers-0.2 (crate (name "smooth-numbers") (vers "0.2.1") (deps (list (crate-dep (name "primal") (req "^0.3.2") (default-features #t) (kind 0)))) (hash "1nzwjcd28khsnqd6ss46vqaj27mxdrwa1fn7638mkja4g2yk3y6m")))

(define-public crate-smooth-numbers-0.3 (crate (name "smooth-numbers") (vers "0.3.0") (deps (list (crate-dep (name "criterion") (req "^0.4.0") (features (quote ("html_reports"))) (default-features #t) (kind 2)) (crate-dep (name "primal") (req "^0.3.2") (default-features #t) (kind 0)))) (hash "0ardvv2qmx5zssjrmd0azal9222jkaiig7bpxv6hc1w9s6nc49f8")))

(define-public crate-smooth-numbers-0.3 (crate (name "smooth-numbers") (vers "0.3.1") (deps (list (crate-dep (name "criterion") (req "^0.4.0") (features (quote ("html_reports"))) (default-features #t) (kind 2)) (crate-dep (name "primal") (req "^0.3.2") (default-features #t) (kind 0)))) (hash "04fh7ziax4d6aa2nm3b78vj6f6j0dnfg5mlj9fnagbjc029zxrff")))

(define-public crate-smooth-numbers-0.4 (crate (name "smooth-numbers") (vers "0.4.0") (deps (list (crate-dep (name "criterion") (req "^0.4.0") (features (quote ("html_reports"))) (default-features #t) (kind 2)) (crate-dep (name "primal") (req "^0.3.2") (default-features #t) (kind 0)))) (hash "1cd86dniqffab5fx8754vv2mg2mjz6zc9vqdydynfg4w0r2zc2sm")))

(define-public crate-smooth-numbers-0.4 (crate (name "smooth-numbers") (vers "0.4.1") (deps (list (crate-dep (name "criterion") (req "^0.4.0") (features (quote ("html_reports"))) (default-features #t) (kind 2)) (crate-dep (name "primal") (req "^0.3.2") (default-features #t) (kind 0)))) (hash "1igl5cylf0n1kvxwy0wfv4ig5va919npgcgr20s5ijf0qkp6k3kp")))

(define-public crate-smooth-numbers-0.4 (crate (name "smooth-numbers") (vers "0.4.2") (deps (list (crate-dep (name "criterion") (req "^0.4.0") (features (quote ("html_reports"))) (default-features #t) (kind 2)) (crate-dep (name "primal") (req "^0.3.2") (default-features #t) (kind 0)))) (hash "1dks3bb4dvliig3ni1xp8fgx740bwn0c8bl9dbr70l3ay56ppj93")))

(define-public crate-smooth-stream-0.1 (crate (name "smooth-stream") (vers "0.1.0") (deps (list (crate-dep (name "async-stream") (req "^0.3.5") (default-features #t) (kind 0)) (crate-dep (name "futures") (req "^0.3.28") (default-features #t) (kind 0)) (crate-dep (name "pin-project") (req "^1.0.12") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.27.0") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "0vay4bxfr5cpvvi0rbjysbk2p1qp7fzvzmkbhvi9si6n04zr44n9")))

(define-public crate-smooth-stream-0.1 (crate (name "smooth-stream") (vers "0.1.1") (deps (list (crate-dep (name "async-stream") (req "^0.3.5") (default-features #t) (kind 0)) (crate-dep (name "futures") (req "^0.3.28") (default-features #t) (kind 0)) (crate-dep (name "pin-project") (req "^1.0.12") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.27.0") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "059agm2hh368gkadp0818bnxg05d7q9ynrsv0y3f5yw46mjr4iya")))

(define-public crate-smoothed_z_score-0.1 (crate (name "smoothed_z_score") (vers "0.1.3") (hash "16ccb80vp1lvcdmdcw1zgwgs5xl9f3bga7g0vb3wnf6vmc3j7jv1")))

(define-public crate-smoothie-0.1 (crate (name "smoothie") (vers "0.1.0") (hash "188y53gdfvd6gh7fq55nnxb66pj4hidsmayckp5kwwf5jg4ihix8")))

(define-public crate-smoothy-0.1 (crate (name "smoothy") (vers "0.1.0") (deps (list (crate-dep (name "pretty_assertions") (req "^1.4.0") (default-features #t) (kind 0)))) (hash "0k80myj7mpgcihhaymgzhxw69pjpw61i151hm5pysvpr1rz0wvym") (features (quote (("default") ("__private_readme_test"))))))

(define-public crate-smoothy-0.2 (crate (name "smoothy") (vers "0.2.0") (deps (list (crate-dep (name "pretty_assertions") (req "^1.4.0") (default-features #t) (kind 0)))) (hash "0la0d9239c6w4n77nxwjmjpbsl4njpa2vqn1naqxw1f8hcgn0ki1") (features (quote (("default") ("__private_readme_test"))))))

(define-public crate-smoothy-0.3 (crate (name "smoothy") (vers "0.3.0") (deps (list (crate-dep (name "pretty_assertions") (req "^1.4.0") (default-features #t) (kind 0)))) (hash "11qpiyz627w8kf5g0v2qqzqi75l7fa858llsyw86ibd1vw3dlmdz") (features (quote (("default") ("__private_readme_test"))))))

(define-public crate-smoothy-0.3 (crate (name "smoothy") (vers "0.3.1") (deps (list (crate-dep (name "pretty_assertions") (req "^1.4.0") (default-features #t) (kind 0)))) (hash "0vyr80vl4zrjq2fjx3kplskyphgx4gj653gqwhyw6z64n5v16m1q") (features (quote (("default") ("__private_readme_test"))))))

(define-public crate-smoothy-0.3 (crate (name "smoothy") (vers "0.3.2") (deps (list (crate-dep (name "pretty_assertions") (req "^1.4.0") (default-features #t) (kind 0)))) (hash "0c1pfl8xpddmr699i8sdbjkk10k5ybx6kgas38d68g6ps383v2b1") (features (quote (("default") ("__private_readme_test"))))))

(define-public crate-smoothy-0.3 (crate (name "smoothy") (vers "0.3.3") (deps (list (crate-dep (name "pretty_assertions") (req "^1.4.0") (default-features #t) (kind 0)))) (hash "14a6kcg4nxlp160hi5mh181z45wmh1nvv90iqmjc24j4kfhhyfja") (features (quote (("default") ("__private_readme_test"))))))

(define-public crate-smoothy-0.4 (crate (name "smoothy") (vers "0.4.0") (deps (list (crate-dep (name "pretty_assertions") (req "^1.4.0") (default-features #t) (kind 0)))) (hash "0bpahqhfhqbnhfzsdqqdvvzfbrx4rnj7x82rpj7iqlklb0dp3s7i") (features (quote (("default"))))))

(define-public crate-smoothy-0.4 (crate (name "smoothy") (vers "0.4.1") (deps (list (crate-dep (name "pretty_assertions") (req "^1.4.0") (default-features #t) (kind 0)))) (hash "0580hv26m8x5f72sdz39wn7lpg5hj1xd4kqkj76qwv67dpc5w0q5") (features (quote (("default"))))))

(define-public crate-smoothy-0.4 (crate (name "smoothy") (vers "0.4.2") (deps (list (crate-dep (name "pretty_assertions") (req "^1.4.0") (default-features #t) (kind 0)))) (hash "104hjvxqni623zmclvf4dapri7bm2w9lpsn8pjjpx6vra93z7x34") (features (quote (("default"))))))

(define-public crate-smoothy-0.4 (crate (name "smoothy") (vers "0.4.3") (deps (list (crate-dep (name "pretty_assertions") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)))) (hash "0i5d1p55i7jx9rwi1qb2j6aa559a4jyrg835zhp7b77mqrc3k3nc") (features (quote (("default"))))))

(define-public crate-smoothy-0.4 (crate (name "smoothy") (vers "0.4.4") (deps (list (crate-dep (name "pretty_assertions") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)))) (hash "1akanyn8r3yfiqbph8fd2lgnyc7z7l0qkh3cjzizz6mv8rjhi3aj") (features (quote (("default"))))))

