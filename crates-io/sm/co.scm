(define-module (crates-io sm co) #:use-module (crates-io))

(define-public crate-smcore-0.1 (crate (name "smcore") (vers "0.1.0") (deps (list (crate-dep (name "json") (req "^0.12.4") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "smdton") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "0r6n1gsp1drh20baksifdxshxz6v1vr8glcwjjqi3m1blw9nl8n0") (yanked #t)))

(define-public crate-smcore-0.1 (crate (name "smcore") (vers "0.1.1") (deps (list (crate-dep (name "json") (req "^0.12.4") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "smdton") (req "^0.1.1") (default-features #t) (kind 0)))) (hash "1hbffwd7wng8hn30837a17pd970wnvgg03863089yjvsyskbw8v2")))

(define-public crate-smcore-0.1 (crate (name "smcore") (vers "0.1.2") (deps (list (crate-dep (name "json") (req "^0.12.4") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "smdton") (req "^0.1.2") (default-features #t) (kind 0)))) (hash "14msy5zibh35i8bkqdsvqp5v5dj7jp3aa4ywg7l9y8ypayiaqhp4")))

