(define-module (crates-io sm hk) #:use-module (crates-io))

(define-public crate-smhkd-0.1 (crate (name "smhkd") (vers "0.1.0") (deps (list (crate-dep (name "docopt") (req "^1.1") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0") (default-features #t) (kind 0)))) (hash "0hd0lycrq4can5vhv7lnl39dbcqgxlq4cpzda597yxw6cg921f1f")))

(define-public crate-smhkd-0.2 (crate (name "smhkd") (vers "0.2.0") (deps (list (crate-dep (name "alsa") (req "^0.7") (default-features #t) (kind 0)) (crate-dep (name "directories-next") (req "^2.0") (default-features #t) (kind 0)) (crate-dep (name "docopt") (req "^1.1") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.6") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)))) (hash "1gp8j071kj8hi7gahvc47wmwamwnx86xkjcq85az856915fs6rrr")))

