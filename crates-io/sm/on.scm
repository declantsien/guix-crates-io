(define-module (crates-io sm on) #:use-module (crates-io))

(define-public crate-smonitor-0.1 (crate (name "smonitor") (vers "0.1.0") (deps (list (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)))) (hash "1yhm96jf685bmhs9y13z3667fs41hj0r4f7fap2hx3s7hi87xz1v") (rust-version "1.73")))

