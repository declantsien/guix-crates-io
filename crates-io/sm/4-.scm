(define-module (crates-io sm #{4-}#) #:use-module (crates-io))

(define-public crate-sm4-gcm-0.0.0 (crate (name "sm4-gcm") (vers "0.0.0") (deps (list (crate-dep (name "sm4") (req "^0.5.1") (default-features #t) (kind 0)))) (hash "1lc7gf2nlnzz5pddcsqd42468a9hdqpzd2mrw671lqsyz8ww43bs") (yanked #t)))

(define-public crate-sm4-gcm-0.1 (crate (name "sm4-gcm") (vers "0.1.0") (deps (list (crate-dep (name "benchmark-simple") (req "^0.1.8") (default-features #t) (kind 2)) (crate-dep (name "ghash") (req "^0.5.0") (features (quote ("zeroize"))) (default-features #t) (kind 0)) (crate-dep (name "hex") (req "^0.4.3") (default-features #t) (kind 2)) (crate-dep (name "sm4") (req "^0.5.1") (features (quote ("zeroize"))) (default-features #t) (kind 0)) (crate-dep (name "zeroize") (req "^1.6.0") (default-features #t) (kind 0)))) (hash "05d3p54nl3fvlmnamdzjs09b9747gvy0sgzk5j8d5927pylag17x")))

(define-public crate-sm4-gcm-0.1 (crate (name "sm4-gcm") (vers "0.1.2") (deps (list (crate-dep (name "benchmark-simple") (req "^0.1.8") (default-features #t) (kind 2)) (crate-dep (name "ghash") (req "^0.5.0") (features (quote ("zeroize"))) (default-features #t) (kind 0)) (crate-dep (name "hex") (req "^0.4.3") (default-features #t) (kind 2)) (crate-dep (name "sm4") (req "^0.5.1") (features (quote ("zeroize"))) (default-features #t) (kind 0)) (crate-dep (name "zeroize") (req "^1.6.0") (default-features #t) (kind 0)))) (hash "09z7jmg7jcnvh959bm9lhfqn1d9y6j3hzhgf9a7jkprkrbffkmn3")))

