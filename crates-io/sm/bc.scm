(define-module (crates-io sm bc) #:use-module (crates-io))

(define-public crate-smbc-0.1 (crate (name "smbc") (vers "0.1.0") (deps (list (crate-dep (name "libc") (req "^0.1.8") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.3.6") (default-features #t) (kind 0)) (crate-dep (name "smbclient-sys") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "0s0lbn0dk34fp0z4l2gzrdxx9m7mfc3pqncs1kpzqmvnaz73nns4")))

(define-public crate-smbclient-sys-0.1 (crate (name "smbclient-sys") (vers "0.1.0") (deps (list (crate-dep (name "libc") (req "^0.1.8") (default-features #t) (kind 0)) (crate-dep (name "nix") (req "^0.4.1") (default-features #t) (kind 0)) (crate-dep (name "pkg-config") (req "^0.3.5") (default-features #t) (kind 1)))) (hash "1bs198x8s7kd8r6i6mzsl50aiffb4kh7b6gm4h81civc0qrsbfav")))

