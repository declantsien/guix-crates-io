(define-module (crates-io sm li) #:use-module (crates-io))

(define-public crate-smlibrbase-0.1 (crate (name "smlibrbase") (vers "0.1.0") (deps (list (crate-dep (name "json") (req "^0.12.4") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "smcore") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "smdton") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "0r6k7yadnl11yk5whh0v36xl55541xafy912asdrjlg7pg90w107") (yanked #t)))

(define-public crate-smlibrbase-0.1 (crate (name "smlibrbase") (vers "0.1.1") (deps (list (crate-dep (name "json") (req "^0.12.4") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "smcore") (req "^0.1.1") (default-features #t) (kind 0)) (crate-dep (name "smdton") (req "^0.1.1") (default-features #t) (kind 0)))) (hash "058ww14rg1nxmmwh7ggz9r3s262k7fggrlldxcqs9dd1v4np39kp")))

(define-public crate-smlibrbase-0.1 (crate (name "smlibrbase") (vers "0.1.2") (deps (list (crate-dep (name "json") (req "^0.12.4") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "smcore") (req "^0.1.2") (default-features #t) (kind 0)) (crate-dep (name "smdton") (req "^0.1.2") (default-features #t) (kind 0)))) (hash "0qip12y3nmnhb0ykkzl4yilhw0qdkqm3cfjc4c1chlsxgh45v1vf")))

