(define-module (crates-io sm s-) #:use-module (crates-io))

(define-public crate-sms-2dm-0.1 (crate (name "sms-2dm") (vers "0.1.0") (deps (list (crate-dep (name "anyhow") (req "^1.0.75") (default-features #t) (kind 2)) (crate-dep (name "num-traits") (req "^0.2.17") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.193") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.50") (default-features #t) (kind 0)))) (hash "1h2hfvrh58akskfm0clayqd8xzkxgd5x8wa4y0lf0z6zldsig9an")))

(define-public crate-sms-2dm-0.2 (crate (name "sms-2dm") (vers "0.2.0") (deps (list (crate-dep (name "anyhow") (req "^1.0.75") (default-features #t) (kind 2)) (crate-dep (name "log") (req "^0.4.20") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2.17") (default-features #t) (kind 0)) (crate-dep (name "paste") (req "^1.0.14") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.193") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.50") (default-features #t) (kind 0)))) (hash "1h6qj43z65dvl1q5p9aqjiqrlkjklkhq5vxs3anj9knh0hfyggsm") (features (quote (("warn" "log") ("strict") ("default" "warn"))))))

(define-public crate-sms-receiver-1 (crate (name "sms-receiver") (vers "1.0.0") (deps (list (crate-dep (name "dbus") (req "^0.9.3") (default-features #t) (kind 0)))) (hash "09a3hpj1cml65d6zbb9q4k0aicxwrngy9q0zlrvp4j9qdaj50lp1")))

