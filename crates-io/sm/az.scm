(define-module (crates-io sm az) #:use-module (crates-io))

(define-public crate-smaz-0.1 (crate (name "smaz") (vers "0.1.0") (deps (list (crate-dep (name "lazy_static") (req "1.*") (default-features #t) (kind 0)))) (hash "1c0z18by0rsx2s91arpbh37m7zq7j9afic65zwd9fkd2fmkwr7hz")))

(define-public crate-smaz2-0.1 (crate (name "smaz2") (vers "0.1.1") (deps (list (crate-dep (name "lazy_static") (req "1.*") (default-features #t) (kind 0)))) (hash "1ap8agjal5i5bvqp1s3awvaq2vf8yqvapvkapx389rblhcz4np05")))

