(define-module (crates-io sm _m) #:use-module (crates-io))

(define-public crate-sm_macro-0.6 (crate (name "sm_macro") (vers "0.6.1") (deps (list (crate-dep (name "proc-macro2") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^0.6") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^0.15") (default-features #t) (kind 0)))) (hash "0x8dy69g4hi7grhphw6n70b44v82dfjpzhy7flypnvsd7lk151s2")))

(define-public crate-sm_macro-0.7 (crate (name "sm_macro") (vers "0.7.0") (deps (list (crate-dep (name "proc-macro2") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^0.6") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^0.15") (default-features #t) (kind 0)))) (hash "1x5rr3af31qdfrs2nrpr3gjbv496lzvajr7346954bhr9r7sdq37")))

(define-public crate-sm_macro-0.9 (crate (name "sm_macro") (vers "0.9.0") (deps (list (crate-dep (name "proc-macro2") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^0.6") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^0.15") (default-features #t) (kind 0)))) (hash "0qg8kvqfflc55cnchwvjrscrx6lj152z16lb9lp4adbl0v4mn95s")))

(define-public crate-sm_motion_photo-0.1 (crate (name "sm_motion_photo") (vers "0.1.0") (deps (list (crate-dep (name "boyer-moore-magiclen") (req "^0.2.4") (default-features #t) (kind 0)) (crate-dep (name "memmap") (req "^0.7.0") (default-features #t) (kind 0)) (crate-dep (name "mp4parse") (req "^0.11.4") (default-features #t) (kind 0)))) (hash "0nc9i4yhl65jx0w95iyr02292c2yiqqq3xg12n7p23a60brnif4l")))

(define-public crate-sm_motion_photo-0.1 (crate (name "sm_motion_photo") (vers "0.1.1") (deps (list (crate-dep (name "boyer-moore-magiclen") (req "^0.2.4") (default-features #t) (kind 0)) (crate-dep (name "memmap") (req "^0.7.0") (default-features #t) (kind 0)) (crate-dep (name "mp4parse") (req "^0.11.4") (default-features #t) (kind 0)))) (hash "1p8snjgjnai125j5g4a2f252ar1x46cj2m6qmswh568c85ndmi6v")))

(define-public crate-sm_motion_photo-0.1 (crate (name "sm_motion_photo") (vers "0.1.2") (deps (list (crate-dep (name "boyer-moore-magiclen") (req "^0.2.4") (default-features #t) (kind 0)) (crate-dep (name "memmap") (req "^0.7.0") (default-features #t) (kind 0)) (crate-dep (name "mp4parse") (req "^0.11.4") (default-features #t) (kind 0)))) (hash "07lnzxbp5ay05d0rj635fdi7xrygnayym99danli4igln04nalaa")))

(define-public crate-sm_motion_photo-0.1 (crate (name "sm_motion_photo") (vers "0.1.3") (deps (list (crate-dep (name "boyer-moore-magiclen") (req "^0.2.4") (default-features #t) (kind 0)) (crate-dep (name "memmap") (req "^0.7.0") (default-features #t) (kind 0)) (crate-dep (name "mp4parse") (req "^0.11.4") (default-features #t) (kind 0)))) (hash "04m0rikvk9ysbg0bl8hyx4171gr7b0d7xv83j32rm92gp79pixqm")))

(define-public crate-sm_motion_photo-0.1 (crate (name "sm_motion_photo") (vers "0.1.4") (deps (list (crate-dep (name "boyer-moore-magiclen") (req "^0.2.4") (default-features #t) (kind 0)) (crate-dep (name "memmap") (req "^0.7.0") (default-features #t) (kind 0)) (crate-dep (name "mp4parse") (req "^0.11.4") (default-features #t) (kind 0)))) (hash "15bwx08f1r788j80bzbm2kijbams7ly3f7b9zrj8zpsfq1812b9n")))

(define-public crate-sm_motion_photo-0.1 (crate (name "sm_motion_photo") (vers "0.1.5") (deps (list (crate-dep (name "boyer-moore-magiclen") (req "^0.2.11") (default-features #t) (kind 0)) (crate-dep (name "criterion") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "memmap") (req "^0.7.0") (default-features #t) (kind 0)) (crate-dep (name "mp4parse") (req "= 0.11.2") (default-features #t) (kind 0)))) (hash "0rfkdc0z8nc9wjmj5d7l5wq308wvyln8sv6llfxs9kdqxnjnl029")))

