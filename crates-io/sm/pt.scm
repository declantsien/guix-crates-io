(define-module (crates-io sm pt) #:use-module (crates-io))

(define-public crate-smpte2022-1-fec-0.1 (crate (name "smpte2022-1-fec") (vers "0.1.0") (deps (list (crate-dep (name "hex-literal") (req "^0.1") (default-features #t) (kind 2)) (crate-dep (name "mio") (req "^0.6") (default-features #t) (kind 2)) (crate-dep (name "rtp-rs") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "smpte2022-1-packet") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "socket2") (req "^0.3") (default-features #t) (kind 2)))) (hash "1p90vjzgqjgrgknwpdgxpc2hp1dgaw3xz8qx5j586kdlja95lpab")))

(define-public crate-smpte2022-1-fec-0.2 (crate (name "smpte2022-1-fec") (vers "0.2.0") (deps (list (crate-dep (name "arrayvec") (req "^0.4.10") (default-features #t) (kind 0)) (crate-dep (name "env_logger") (req "^0.6") (default-features #t) (kind 2)) (crate-dep (name "hex-literal") (req "^0.1") (default-features #t) (kind 2)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "mio") (req "^0.6") (default-features #t) (kind 2)) (crate-dep (name "mio-extras") (req "^2.0") (default-features #t) (kind 2)) (crate-dep (name "mpeg2ts-reader") (req "^0.10") (default-features #t) (kind 2)) (crate-dep (name "rtp-rs") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "smpte2022-1-packet") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "socket2") (req "^0.3") (default-features #t) (kind 2)))) (hash "1fjyx4b7wjrw0yv942x80q0c27w1khrd9i43wjdaikb4x6mvai9s")))

(define-public crate-smpte2022-1-fec-0.3 (crate (name "smpte2022-1-fec") (vers "0.3.0") (deps (list (crate-dep (name "arrayvec") (req "^0.5.1") (default-features #t) (kind 0)) (crate-dep (name "env_logger") (req "^0.8") (default-features #t) (kind 2)) (crate-dep (name "hex-literal") (req "^0.2") (default-features #t) (kind 2)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "mio") (req "^0.7") (features (quote ("udp" "os-poll"))) (default-features #t) (kind 2)) (crate-dep (name "mpeg2ts-reader") (req "^0.13") (default-features #t) (kind 2)) (crate-dep (name "rtp-rs") (req "^0.5.0") (default-features #t) (kind 0)) (crate-dep (name "smpte2022-1-packet") (req "^0.5") (default-features #t) (kind 0)) (crate-dep (name "socket2") (req "^0.3") (default-features #t) (kind 2)))) (hash "1mrpsi8qiv49zfl8cvfk2xswq4i799n011ikz49mp06nfjvph20z")))

(define-public crate-smpte2022-1-packet-0.1 (crate (name "smpte2022-1-packet") (vers "0.1.0") (deps (list (crate-dep (name "hex-literal") (req "^0.1") (default-features #t) (kind 2)))) (hash "04pab72x6rmvxqnscjpp2ydw0mpv2lf5llb6gpq45x310538qipk")))

(define-public crate-smpte2022-1-packet-0.2 (crate (name "smpte2022-1-packet") (vers "0.2.0") (deps (list (crate-dep (name "hex-literal") (req "^0.1") (default-features #t) (kind 2)))) (hash "18z2vm3fx75ld89zbl01g5gyjs91xik9ipik5xyl32k1k5vzhzln")))

(define-public crate-smpte2022-1-packet-0.3 (crate (name "smpte2022-1-packet") (vers "0.3.0") (deps (list (crate-dep (name "hex-literal") (req "^0.1") (default-features #t) (kind 2)) (crate-dep (name "rtp-rs") (req "^0.3.0") (default-features #t) (kind 0)))) (hash "010r737nir6gzdl7d015a06jv1hggvq14yawfdg6fq3qr0qbnksp")))

(define-public crate-smpte2022-1-packet-0.4 (crate (name "smpte2022-1-packet") (vers "0.4.0") (deps (list (crate-dep (name "hex-literal") (req "^0.1") (default-features #t) (kind 2)) (crate-dep (name "rtp-rs") (req "^0.3.0") (default-features #t) (kind 0)))) (hash "08fdxw4rncawgv4w489i1l7c0anw9m6c1h41qj34agq20cbhzdrq")))

(define-public crate-smpte2022-1-packet-0.5 (crate (name "smpte2022-1-packet") (vers "0.5.0") (deps (list (crate-dep (name "hex-literal") (req "^0.2") (default-features #t) (kind 2)) (crate-dep (name "rtp-rs") (req "^0.5.0") (default-features #t) (kind 0)))) (hash "06kwqxx54yicyn4gjzvisgng0ja794niqipbsy0ad9v0cjs5gkxp")))

(define-public crate-smptera-format-identifers-rust-0.1 (crate (name "smptera-format-identifers-rust") (vers "0.1.0") (deps (list (crate-dep (name "four-cc") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "085iybmr0i0yrfbl99xch5hiiz2lqn6ndfkd6qpcx8y2l27jw64d") (yanked #t)))

(define-public crate-smptera-format-identifers-rust-0.2 (crate (name "smptera-format-identifers-rust") (vers "0.2.0") (deps (list (crate-dep (name "four-cc") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "08z4k719b47dshxqmfdcfk21xrq96w0xrlgf1qaxa7rphq6hz0hq") (yanked #t)))

(define-public crate-smptera-format-identifiers-rust-0.2 (crate (name "smptera-format-identifiers-rust") (vers "0.2.0") (deps (list (crate-dep (name "four-cc") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "1ccqa596fnqaqdds5vcxzf699pz7yp0j5zmzk0ckcsx6j78b5b1g")))

(define-public crate-smptera-format-identifiers-rust-0.3 (crate (name "smptera-format-identifiers-rust") (vers "0.3.0") (deps (list (crate-dep (name "four-cc") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "0bkzw8dmasbwpvndv2avhsdv85s8xcnab0qzjqcdbazk938vadmq")))

(define-public crate-smptera-format-identifiers-rust-0.4 (crate (name "smptera-format-identifiers-rust") (vers "0.4.0") (deps (list (crate-dep (name "four-cc") (req "^0.3.0") (default-features #t) (kind 0)))) (hash "0l27i023i5a3r1975587a0k1pjv9c8qd0sbz6n97hfmgbxan56dp")))

