(define-module (crates-io sm em) #:use-module (crates-io))

(define-public crate-smem-0.1 (crate (name "smem") (vers "0.1.0") (deps (list (crate-dep (name "anyhow") (req "^1.0.66") (default-features #t) (kind 0)) (crate-dep (name "nix") (req "^0.25.0") (default-features #t) (target "cfg(unix)") (kind 0)) (crate-dep (name "thiserror") (req "^1.0.37") (default-features #t) (kind 0)) (crate-dep (name "windows") (req "^0.43.0") (features (quote ("Win32_System_Memory" "Win32_Foundation" "Win32_Security"))) (default-features #t) (target "cfg(windows)") (kind 0)))) (hash "1wwx29ljnnbi0g5ri0jpm7bj38cq2hg7gc45m68xd1g1z5xfvzib")))

