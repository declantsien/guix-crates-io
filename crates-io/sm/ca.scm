(define-module (crates-io sm ca) #:use-module (crates-io))

(define-public crate-smcan-0.1 (crate (name "smcan") (vers "0.1.0") (deps (list (crate-dep (name "anyhow") (req "^1.0.72") (default-features #t) (kind 2)) (crate-dep (name "embedded-can") (req "^0.4.1") (default-features #t) (kind 0)) (crate-dep (name "socketcan") (req "^2.0.0") (default-features #t) (kind 2)))) (hash "1wydsll185sr8waa32zm2asc73wwdzxqfkv45xglc06mr6b2znps")))

(define-public crate-smcan-0.1 (crate (name "smcan") (vers "0.1.1") (deps (list (crate-dep (name "anyhow") (req "^1.0.72") (default-features #t) (kind 2)) (crate-dep (name "embedded-can") (req "^0.4.1") (default-features #t) (kind 0)) (crate-dep (name "socketcan") (req "^2.0.0") (default-features #t) (kind 2)))) (hash "1fv73gyd0f64an7q541zlqk3i2p9xlmslx8lyprv6n4hj1hda60j")))

(define-public crate-smcan-0.1 (crate (name "smcan") (vers "0.1.2") (deps (list (crate-dep (name "anyhow") (req "^1.0.72") (default-features #t) (kind 2)) (crate-dep (name "binrw") (req "^0.11.2") (default-features #t) (kind 0)) (crate-dep (name "embedded-can") (req "^0.4.1") (default-features #t) (kind 0)) (crate-dep (name "socketcan") (req "^2.0.0") (default-features #t) (kind 2)))) (hash "0la2parac6wp8mnkvzav55xl1vawna0h1ws666gfp26bwhp9rnca")))

