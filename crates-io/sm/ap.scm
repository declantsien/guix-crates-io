(define-module (crates-io sm ap) #:use-module (crates-io))

(define-public crate-smap-0.1 (crate (name "smap") (vers "0.1.0") (hash "1idpm2z3b7bjvifjjv4n1x3s9j8r3clar7ma9msqa7in9rnzyxbl")))

(define-public crate-smap-0.1 (crate (name "smap") (vers "0.1.1") (hash "1cv140qrgmy2zmy6jkh2g8l1fhypf7dvi6can9lfz0z1nzk2q3k2")))

(define-public crate-smap-0.2 (crate (name "smap") (vers "0.2.0") (hash "0h1b5pgmnfjv0qmifqs9fp2nxq3ybxcismxpbj9avjcvxpd4b0w5")))

(define-public crate-smap-0.2 (crate (name "smap") (vers "0.2.1") (hash "0wx5nigdfs71bjh54as303pbgx66vv24p04y9rk7iilp28m5s903")))

(define-public crate-smap-0.2 (crate (name "smap") (vers "0.2.2") (hash "18aq49l1dmnw0s5sz7zyaizv73gj3sljzb4lwcjln5yrgrjjypcl")))

(define-public crate-smap-0.3 (crate (name "smap") (vers "0.3.0") (hash "0chapjjgxaa3cjcan27vk8zrfbw8hnxlk4856h849d3pz8c5h0n6")))

