(define-module (crates-io sm ec) #:use-module (crates-io))

(define-public crate-smecs-0.1 (crate (name "smecs") (vers "0.1.1") (hash "044jyx4n10awhsgkgjjqiw9999bb08fcr2m0d2a0pylb0ilbkj7w")))

(define-public crate-smecs-0.1 (crate (name "smecs") (vers "0.1.2") (hash "1nhz3xprzzqppz76338nplpy60z53f9vayxpr4vj1597az82zr9c")))

(define-public crate-smecs-0.1 (crate (name "smecs") (vers "0.1.3") (hash "1j7swacwqjs2b78n5yhcyipk9zar767vmm9ywqfv8lmg6qnhfhfw")))

(define-public crate-smecs-0.1 (crate (name "smecs") (vers "0.1.4") (hash "0pdgyridiijn0cy9vsy20vc2lphkix697gaal207i3clxcraqz62")))

(define-public crate-smecs-0.1 (crate (name "smecs") (vers "0.1.5") (deps (list (crate-dep (name "criterion") (req "^0.3") (default-features #t) (kind 2)))) (hash "0727c599dwbvj7dkzd5m99z1jm3cvxnca5975yx0d242mdbqpvaa") (features (quote (("static-dispatch") ("deafault-fatures"))))))

