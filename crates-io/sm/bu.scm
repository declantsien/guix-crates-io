(define-module (crates-io sm bu) #:use-module (crates-io))

(define-public crate-smbus-adapter-0.1 (crate (name "smbus-adapter") (vers "0.1.0") (deps (list (crate-dep (name "embedded-hal-async") (req "^1.0.0") (default-features #t) (kind 0)) (crate-dep (name "heapless") (req "^0.8") (default-features #t) (kind 0)))) (hash "1h0qlfxmmw9zxjxklgh9hzg6gs4y6h0mxhxkcwgjivakgkb5ir0b")))

(define-public crate-smbus-adapter-0.1 (crate (name "smbus-adapter") (vers "0.1.1") (deps (list (crate-dep (name "embedded-hal-async") (req "^1.0.0") (default-features #t) (kind 0)) (crate-dep (name "heapless") (req "^0.8") (default-features #t) (kind 0)))) (hash "1sm6zkkp5jypfma340wmlay4mhsrgc2dhla4gfmdvf51v7vw18bv")))

(define-public crate-smbus-pec-0.1 (crate (name "smbus-pec") (vers "0.1.0") (deps (list (crate-dep (name "criterion") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "embedded-crc-macros") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "embedded-crc-macros") (req "^0.1") (default-features #t) (kind 1)) (crate-dep (name "rand") (req "^0.6") (default-features #t) (kind 2)))) (hash "0hyih0xb2k4nkksksjnb8rxhcyr75r38fmdmzg1d8r41fln4yw17") (features (quote (("lookup-table"))))))

(define-public crate-smbus-pec-1 (crate (name "smbus-pec") (vers "1.0.0") (deps (list (crate-dep (name "criterion") (req "^0.3.4") (features (quote ("html_reports"))) (default-features #t) (kind 2)) (crate-dep (name "embedded-crc-macros") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "embedded-crc-macros") (req "^1") (default-features #t) (kind 1)) (crate-dep (name "rand") (req "^0.6") (default-features #t) (kind 2)))) (hash "1nm84rhjr5pjmxdqw9rpwdp6ygj72jck98fsfpsm4l4yjk9fwxrr") (features (quote (("lookup-table"))))))

(define-public crate-smbus-pec-1 (crate (name "smbus-pec") (vers "1.0.1") (deps (list (crate-dep (name "criterion") (req "^0.3.4") (features (quote ("html_reports"))) (default-features #t) (kind 2)) (crate-dep (name "embedded-crc-macros") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "embedded-crc-macros") (req "^1") (default-features #t) (kind 1)) (crate-dep (name "rand") (req "^0.6") (default-features #t) (kind 2)))) (hash "0gkx44s2mg855zvx8sha710qhz8i9h2qmz3viyr74pfdh2k661ya") (features (quote (("lookup-table"))))))

(define-public crate-smbus-request-parser-0.1 (crate (name "smbus-request-parser") (vers "0.1.0") (hash "0khi57ljzh8s0xvrm4pw25ywp95xjxsz32pgza6jdazbwmlja43x")))

(define-public crate-smbus-request-parser-0.1 (crate (name "smbus-request-parser") (vers "0.1.1") (hash "06pn53x3clq8amgsvpv5z7cvavlnzzvzf5dn1j7x6ww4l78h9g8w")))

(define-public crate-smbus-request-parser-0.1 (crate (name "smbus-request-parser") (vers "0.1.2") (hash "1cpipm1nc0fki3wj6f2kfm4svj08m50hypj4n58jxcply5a80mci")))

(define-public crate-smbus-request-parser-0.2 (crate (name "smbus-request-parser") (vers "0.2.0") (hash "0zbg9z5xcpx12dzpcwyn2x52sjdcczgk5q8n9j67fvakrawrdvlp")))

