(define-module (crates-io sm lr) #:use-module (crates-io))

(define-public crate-smlr-0.1 (crate (name "smlr") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^2.33.1") (default-features #t) (kind 0)) (crate-dep (name "eddie") (req "^0.4") (default-features #t) (kind 0)))) (hash "1200fsaspk971lgp079m8pyfrl1hd8awldhan6mgc2nbnzl4da8h")))

(define-public crate-smlr-0.1 (crate (name "smlr") (vers "0.1.1") (deps (list (crate-dep (name "clap") (req "^2.33.1") (default-features #t) (kind 0)) (crate-dep (name "eddie") (req "^0.4") (default-features #t) (kind 0)))) (hash "0ax30fhcb5lnm2hhqdhxfy83rmmdqsg31rhmgmmsb6mxkm9wzagg")))

(define-public crate-smlr-0.1 (crate (name "smlr") (vers "0.1.2") (deps (list (crate-dep (name "clap") (req "^2.33.1") (default-features #t) (kind 0)) (crate-dep (name "eddie") (req "^0.4") (default-features #t) (kind 0)))) (hash "0mr2vly53ldzjiprgvyr4g0p84368zz2dp38j8krainbln7k4w9d")))

(define-public crate-smlr-0.1 (crate (name "smlr") (vers "0.1.3") (deps (list (crate-dep (name "blake2") (req "^0.9.0") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^2.33.1") (default-features #t) (kind 0)) (crate-dep (name "eddie") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "hex") (req "^0.4.2") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)))) (hash "1zb5xkzw8y69ld3j6skqaq9p72i38lhmhjj42xc0dwhq8lvv7x90")))

