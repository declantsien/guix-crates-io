(define-module (crates-io sm s_) #:use-module (crates-io))

(define-public crate-sms_splitter-0.1 (crate (name "sms_splitter") (vers "0.1.0") (deps (list (crate-dep (name "serde_json") (req "^1.0.86") (default-features #t) (kind 2)))) (hash "1a3sqdqrvdaavpmy1pc235nyinqclc9fgv31fhbb6swd29fdvagy") (yanked #t)))

(define-public crate-sms_splitter-0.1 (crate (name "sms_splitter") (vers "0.1.1") (deps (list (crate-dep (name "serde_json") (req "^1.0.86") (default-features #t) (kind 2)))) (hash "0klibv9b2pp1gfjdlrqp12zjqi6qaziq0azw2g593izpn5i6pbcw") (yanked #t)))

(define-public crate-sms_splitter-0.1 (crate (name "sms_splitter") (vers "0.1.2") (deps (list (crate-dep (name "serde_json") (req "^1.0.86") (default-features #t) (kind 2)))) (hash "0ji46jrhxd61d4d11n8p5f4y9ycikafbkxixhn01wdsdcjkdpgs8") (yanked #t)))

(define-public crate-sms_splitter-0.1 (crate (name "sms_splitter") (vers "0.1.3") (deps (list (crate-dep (name "serde_json") (req "^1.0.86") (default-features #t) (kind 2)))) (hash "1dnra2lgf7nviynyj6abp61vx3hvyc0ybjbxzx4hg116jbms0310") (yanked #t)))

(define-public crate-sms_splitter-0.1 (crate (name "sms_splitter") (vers "0.1.4") (deps (list (crate-dep (name "serde_json") (req "^1.0.86") (default-features #t) (kind 2)))) (hash "1j7mjd4qicm3vkd2bdvs3bvlzyg4kmwsvi9m67rnkqfhgp3lfhfb") (yanked #t)))

(define-public crate-sms_splitter-0.1 (crate (name "sms_splitter") (vers "0.1.5") (deps (list (crate-dep (name "serde_json") (req "^1.0.86") (default-features #t) (kind 2)))) (hash "159lnvvnkynwqy0bnyqa2zgpyd63wp122sqprnwyzv54vsb9jdjs") (yanked #t)))

(define-public crate-sms_splitter-0.1 (crate (name "sms_splitter") (vers "0.1.6") (deps (list (crate-dep (name "serde_json") (req "^1.0.86") (default-features #t) (kind 2)))) (hash "1nhgm26dzjy1ijxfiw1hpbhxmr5k712yfd1g5sik8pkx6icxwnvb") (yanked #t)))

(define-public crate-sms_splitter-0.1 (crate (name "sms_splitter") (vers "0.1.7") (deps (list (crate-dep (name "serde_json") (req "^1.0.86") (default-features #t) (kind 2)))) (hash "0ap2dhzwzm3b2y3905s4r9zdvy273w49hnhd38p0c0z6dcfkxn4j")))

(define-public crate-sms_splitter-0.1 (crate (name "sms_splitter") (vers "0.1.8") (deps (list (crate-dep (name "serde_json") (req "^1.0.86") (default-features #t) (kind 2)))) (hash "1h951afbs0vma823bc6y460pm64az6bjrpdx45z2fkiyhvywsr7w")))

(define-public crate-sms_splitter-0.1 (crate (name "sms_splitter") (vers "0.1.9") (deps (list (crate-dep (name "serde_json") (req "^1.0.86") (default-features #t) (kind 2)))) (hash "0mhy11w22qk725qzk3qwyk77qjk30k6n7mvpi5yqi6m09xi0jazg")))

