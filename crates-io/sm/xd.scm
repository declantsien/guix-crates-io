(define-module (crates-io sm xd) #:use-module (crates-io))

(define-public crate-smxdasm-0.1 (crate (name "smxdasm") (vers "0.1.0") (deps (list (crate-dep (name "bitflags") (req "^1.2.1") (default-features #t) (kind 0)) (crate-dep (name "byteorder") (req "^1.3.2") (default-features #t) (kind 0)) (crate-dep (name "flate2") (req "^1.0") (features (quote ("zlib"))) (kind 0)) (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "num_enum") (req "^0.4.2") (default-features #t) (kind 0)))) (hash "1xi6f29fnd2gyxisvd40cpkzdcnyp196qlivfncclzdaxjjk4y74")))

(define-public crate-smxdasm-0.2 (crate (name "smxdasm") (vers "0.2.0") (deps (list (crate-dep (name "bitflags") (req "^1.2.1") (default-features #t) (kind 0)) (crate-dep (name "byteorder") (req "^1.3.2") (default-features #t) (kind 0)) (crate-dep (name "flate2") (req "^1.0") (features (quote ("zlib"))) (kind 0)) (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "num_enum") (req "^0.4.2") (default-features #t) (kind 0)))) (hash "0wg91pa1vg547g39jffrrrsxlfx52ffmc7vnn06ll3r231221fz4")))

