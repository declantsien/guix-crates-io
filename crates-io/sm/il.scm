(define-module (crates-io sm il) #:use-module (crates-io))

(define-public crate-smile-0.1 (crate (name "smile") (vers "0.1.0") (hash "1va9v1wmmdfwx2bblnipmy1g37j00rn774fwjx2d971ix5r95bkv")))

(define-public crate-smile-0.1 (crate (name "smile") (vers "0.1.1") (deps (list (crate-dep (name "cc") (req "^1.0") (features (quote ("parallel"))) (default-features #t) (kind 1)))) (hash "1j93fl9dd8asxskwhkxp67kgx420zz4brp8rr2m02s2ngn0krnw4")))

(define-public crate-smile-marco-0.1 (crate (name "smile-marco") (vers "0.1.0") (deps (list (crate-dep (name "proc-macro2") (req "^1.0.79") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.35") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2.0.53") (default-features #t) (kind 0)))) (hash "1pi5xya8p8w2ybgg8b1g0mdnzdyn8qvwyl71wm7wzfnqzmpi8pgi") (features (quote (("wither") ("setter") ("getter") ("full" "getter" "setter" "wither" "builder") ("default" "full") ("builder"))))))

(define-public crate-smile-marco-1 (crate (name "smile-marco") (vers "1.0.0") (deps (list (crate-dep (name "proc-macro2") (req "^1.0.79") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.35") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2.0.53") (default-features #t) (kind 0)))) (hash "1x0amlgsqd0lpk5i3shl6027b1n3i2blmz5pidphw97217f416k5") (features (quote (("wither") ("setter") ("getter") ("full" "getter" "setter" "wither" "builder") ("default" "full") ("builder"))))))

(define-public crate-smiles-0.0.1 (crate (name "smiles") (vers "0.0.1") (hash "087m6djlf76fa9yb17gz9m7mkr5zg9xycn9vwh5xk8fmmny90jmr")))

(define-public crate-smiles-parser-0.1 (crate (name "smiles-parser") (vers "0.1.0") (deps (list (crate-dep (name "basechem") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "nom") (req "^5.0.1") (default-features #t) (kind 0)))) (hash "18zx4wkm2csnkab8smp15csbaghs5d80p4x0l91kxmc2vy5qnra1")))

(define-public crate-smiles-parser-0.2 (crate (name "smiles-parser") (vers "0.2.0") (deps (list (crate-dep (name "nom") (req "^5.0.1") (default-features #t) (kind 0)))) (hash "080z2nsf23sjq6pxsvvvpbjl6y0znqc2nbd08rz8afbv2qzy39rv")))

(define-public crate-smiles-parser-0.2 (crate (name "smiles-parser") (vers "0.2.1") (deps (list (crate-dep (name "nom") (req "^5.0.1") (default-features #t) (kind 0)))) (hash "1sqwbl8mza55dw8r69xc26q6jc5sgcdswmqf9v9ri3fb77v0z0q9")))

(define-public crate-smiles-parser-0.3 (crate (name "smiles-parser") (vers "0.3.0") (deps (list (crate-dep (name "derive_more") (req "^0.99.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "itertools") (req "^0.9.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "nom") (req "^5.0.1") (default-features #t) (kind 0)) (crate-dep (name "petgraph") (req "^0.5.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "ptable") (req "^0.3") (default-features #t) (kind 0) (package "periodic-table-on-an-enum")))) (hash "1i38mhhqlv045l87d8sc0fmj0v52g0b5sryqyrh8sqnil4mk4cx0") (features (quote (("graph" "petgraph" "itertools" "derive_more"))))))

(define-public crate-smiles-parser-0.4 (crate (name "smiles-parser") (vers "0.4.0") (deps (list (crate-dep (name "derive_more") (req "^0.99.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "itertools") (req "^0.9.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "nom") (req "^6.0.0") (default-features #t) (kind 0)) (crate-dep (name "petgraph") (req "^0.5.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "ptable") (req "^0.3") (default-features #t) (kind 0) (package "periodic-table-on-an-enum")) (crate-dep (name "serde") (req "^1") (features (quote ("derive"))) (default-features #t) (kind 2)) (crate-dep (name "serde_json") (req "^1") (default-features #t) (kind 2)))) (hash "0542npwxy6zlf5i0k305klmsnbb2179cyz6y6gxn7nc5bjn54lay") (features (quote (("graph" "petgraph" "itertools" "derive_more"))))))

(define-public crate-smiles-parser-0.4 (crate (name "smiles-parser") (vers "0.4.1") (deps (list (crate-dep (name "derive_more") (req "^0.99.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "itertools") (req "^0.9.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "nom") (req "^6.0.0") (default-features #t) (kind 0)) (crate-dep (name "petgraph") (req "^0.5.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "ptable") (req "^0.3") (default-features #t) (kind 0) (package "periodic-table-on-an-enum")) (crate-dep (name "serde") (req "^1") (features (quote ("derive"))) (default-features #t) (kind 2)) (crate-dep (name "serde_json") (req "^1") (default-features #t) (kind 2)))) (hash "16kn9i1nnmivx753k8ivnvchq2k5spa8d0c0wvnii9izhdj9zs6p") (features (quote (("graph" "petgraph" "itertools" "derive_more"))))))

(define-public crate-smileypyramid-1 (crate (name "smileypyramid") (vers "1.0.0") (deps (list (crate-dep (name "docopt") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0") (default-features #t) (kind 0)))) (hash "0ncadrx2dlski6b7g51l4yc63b3c9iryxc52q3b1lbmd2xyq7990")))

(define-public crate-smileypyramid-1 (crate (name "smileypyramid") (vers "1.0.1") (deps (list (crate-dep (name "docopt") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0") (default-features #t) (kind 0)))) (hash "135w7dx33pihp8k2afq52qd3hn4krw8zsr4lz761bbvpxs0lhdj4")))

