(define-module (crates-io sm a-) #:use-module (crates-io))

(define-public crate-sma-rs-0.1 (crate (name "sma-rs") (vers "0.1.0") (deps (list (crate-dep (name "ta-common") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "0774jza6arszyhrij79ix30d3pykbarnwymyzb099a9x4vdb346z")))

(define-public crate-sma-rs-0.1 (crate (name "sma-rs") (vers "0.1.1") (deps (list (crate-dep (name "ta-common") (req ">=0.1.0, <0.2.0") (default-features #t) (kind 0)))) (hash "14yznmklp564c7z5jylkngsph4r7zwlngm86x4bcl7fy4ql364k4")))

(define-public crate-sma-rs-0.1 (crate (name "sma-rs") (vers "0.1.2") (deps (list (crate-dep (name "ta-common") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "1w42vb8j43vdn9hns9phz67fgxyb27c9rcs3ms9hzzbzx39iygiz")))

(define-public crate-sma-rs-0.1 (crate (name "sma-rs") (vers "0.1.3") (deps (list (crate-dep (name "ta-common") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "05m06jn7a6bn36kkw3k4hwjrmfip8gpqjkzy0i2v7v0h82cpvig2")))

(define-public crate-sma-rs-0.1 (crate (name "sma-rs") (vers "0.1.4") (deps (list (crate-dep (name "ta-common") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "1mf8sv56wh6267iybgzj09wwrdk5hhczw4pkj46mjyzak7arlnny")))

