(define-module (crates-io sm t_) #:use-module (crates-io))

(define-public crate-smt_map-0.0.1 (crate (name "smt_map") (vers "0.0.1") (deps (list (crate-dep (name "lazy_static") (req "^1.2.0") (default-features #t) (kind 0)) (crate-dep (name "tiny-keccak") (req "^1.4.2") (default-features #t) (kind 0)) (crate-dep (name "uint") (req "^0.5.0") (default-features #t) (kind 0)))) (hash "1175gn5g8zr1xfavc0z1p8xfm2dhrmwnpbs1l3mab41chm5wss1g")))

(define-public crate-smt_map-0.0.2 (crate (name "smt_map") (vers "0.0.2") (deps (list (crate-dep (name "lazy_static") (req "^1.2.0") (default-features #t) (kind 0)) (crate-dep (name "tiny-keccak") (req "^1.4.2") (default-features #t) (kind 0)) (crate-dep (name "uint") (req "^0.5.0") (default-features #t) (kind 0)))) (hash "09llx09bdpn35r76k0x3f0vqszv8zhs7yx7vyyiyg3anp695i7nq") (features (quote (("test" "std") ("std"))))))

(define-public crate-smt_map-0.0.3 (crate (name "smt_map") (vers "0.0.3") (deps (list (crate-dep (name "hex") (req "^0.3.2") (default-features #t) (kind 2)) (crate-dep (name "lazy_static") (req "^1.2.0") (default-features #t) (kind 0)) (crate-dep (name "tiny-keccak") (req "^1.4.2") (default-features #t) (kind 0)))) (hash "1nc47lizikjdvqwmzj7cj8gg2qpjz6vqny9fq01q2dcz3wk97znh") (features (quote (("test" "std") ("std"))))))

(define-public crate-smt_map-0.0.4 (crate (name "smt_map") (vers "0.0.4") (deps (list (crate-dep (name "hex") (req "^0.3.2") (default-features #t) (kind 2)) (crate-dep (name "lazy_static") (req "^1.2.0") (default-features #t) (kind 0)) (crate-dep (name "tiny-keccak") (req "^1.4.2") (default-features #t) (kind 0)))) (hash "1yxdpa4cs8nbd2l2vk5rcwn5jy4r3krars6nvq760xn9jydi1w3v")))

(define-public crate-smt_map-0.0.5 (crate (name "smt_map") (vers "0.0.5") (deps (list (crate-dep (name "hex") (req "^0.3.2") (default-features #t) (kind 2)) (crate-dep (name "lazy_static") (req "^1.2.0") (features (quote ("spin_no_std"))) (default-features #t) (kind 0)) (crate-dep (name "tiny-keccak") (req "^1.4.2") (default-features #t) (kind 0)))) (hash "01d61z2is3bk4mj625avlnd794q3ss5zy6dh27vam5h3aadrpy4m")))

(define-public crate-smt_sb-rs-0.1 (crate (name "smt_sb-rs") (vers "0.1.0") (hash "1k8icbi11dsm7bqanpys38p7068nfbsiff10iyj3j8skbbnhdhnw")))

(define-public crate-smt_sb-rs-0.1 (crate (name "smt_sb-rs") (vers "0.1.1") (hash "1gnym0smn7xi5n6p0lfzd1gzb21q9kakw3lz8adhj32jfki8gy55")))

(define-public crate-smt_sb-rs-0.1 (crate (name "smt_sb-rs") (vers "0.1.2") (hash "16h7jfsjfvxk9mq2n4yf3xk7d7fgb6wc6g2ngafxkcw0acpayd8a")))

(define-public crate-smt_sb-rs-0.1 (crate (name "smt_sb-rs") (vers "0.1.3") (hash "1z9jjzsgwh2cflxcqpdmsvhwbd4wzbqm3yzsk2lyls4cq984q136")))

(define-public crate-smt_sb-rs-0.1 (crate (name "smt_sb-rs") (vers "0.1.4") (hash "03imf723f0bjr9hn84l93cqhs87ijnp6zlww8q2zv6h66l6v0bv1")))

(define-public crate-smt_sb-rs-0.1 (crate (name "smt_sb-rs") (vers "0.1.5") (hash "0inxbl84dva1bjdgf0d4xd4n1hp7ad93vrfgq7qrqfnkc6lwbqlf")))

(define-public crate-smt_sb-rs-0.1 (crate (name "smt_sb-rs") (vers "0.1.6") (hash "01vap0m5myxknm4i3gw7npxjnr1fgnzihxsxmdrxj5ryfnpfb1ff")))

