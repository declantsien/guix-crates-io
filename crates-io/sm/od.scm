(define-module (crates-io sm od) #:use-module (crates-io))

(define-public crate-smodel-0.1 (crate (name "smodel") (vers "0.1.0") (deps (list (crate-dep (name "by_address") (req "^1.1.0") (default-features #t) (kind 0)) (crate-dep (name "smodel-proc") (req "^0.1") (default-features #t) (kind 0)))) (hash "1fraiqxflmrj7ma9cghw9293l5pzi8bnpb2y7ck1w9cpwkfk61q4")))

(define-public crate-smodel-1 (crate (name "smodel") (vers "1.0.0") (deps (list (crate-dep (name "by_address") (req "^1.1.0") (default-features #t) (kind 0)) (crate-dep (name "smodel-proc") (req "^1.0") (default-features #t) (kind 0)))) (hash "02ggnlq2r7dak1cqc086jh4n84kbvnbxmky55qm83kygq4xg3n20")))

(define-public crate-smodel-1 (crate (name "smodel") (vers "1.0.1") (deps (list (crate-dep (name "by_address") (req "^1.1.0") (default-features #t) (kind 0)) (crate-dep (name "smodel-proc") (req "^1.0") (default-features #t) (kind 0)))) (hash "1ij02hqpdg9nmcaxfd9ml8dap087q8z7knvmqanhhwcc3zb6nqbq")))

(define-public crate-smodel-1 (crate (name "smodel") (vers "1.0.2") (deps (list (crate-dep (name "by_address") (req "^1.1.0") (default-features #t) (kind 0)) (crate-dep (name "smodel-proc") (req "^1.0") (default-features #t) (kind 0)))) (hash "11v4sbz3p921378sdnfhycmc4jbycj08y7kdwnjdrh4l5k0gkliv")))

(define-public crate-smodel-1 (crate (name "smodel") (vers "1.0.3") (deps (list (crate-dep (name "by_address") (req "^1.1.0") (default-features #t) (kind 0)) (crate-dep (name "smodel-proc") (req "^1.0") (default-features #t) (kind 0)))) (hash "02ig6rmfnpxbr90mhl694s9by20j8lgl5idzml67br7ldi6jzknv")))

(define-public crate-smodel-1 (crate (name "smodel") (vers "1.0.4") (deps (list (crate-dep (name "by_address") (req "^1.1.0") (default-features #t) (kind 0)) (crate-dep (name "smodel-proc") (req "^1.0") (default-features #t) (kind 0)))) (hash "1bikf6wmgy0i2qcp3plb409455cx0yv41kl793mzp7a3bmnlqpbg")))

(define-public crate-smodel-1 (crate (name "smodel") (vers "1.0.5") (deps (list (crate-dep (name "by_address") (req "^1.1.0") (default-features #t) (kind 0)) (crate-dep (name "smodel-proc") (req "^1.0") (default-features #t) (kind 0)))) (hash "14plsyjknp0nqcvcpc1mv36j1256rmc3qx6gzfqn8rfagdg4v2dh")))

(define-public crate-smodel-1 (crate (name "smodel") (vers "1.0.6") (deps (list (crate-dep (name "by_address") (req "^1.1.0") (default-features #t) (kind 0)) (crate-dep (name "smodel-proc") (req "^1.0") (default-features #t) (kind 0)))) (hash "1z9c9d2ykllac9m6a5nnxm3w47n0qqqikjgmj74pksfs0dmiz77m")))

(define-public crate-smodel-1 (crate (name "smodel") (vers "1.0.7") (deps (list (crate-dep (name "by_address") (req "^1.1.0") (default-features #t) (kind 0)) (crate-dep (name "smodel-proc") (req "^1.0") (default-features #t) (kind 0)))) (hash "1k2lgb40i0xsf72gys0mprpv907k5rqxg2v25nxr6wl7s43jghkm")))

(define-public crate-smodel-1 (crate (name "smodel") (vers "1.0.8") (deps (list (crate-dep (name "by_address") (req "^1.1.0") (default-features #t) (kind 0)) (crate-dep (name "smodel-proc") (req "^1.0") (default-features #t) (kind 0)))) (hash "1r7abr8a3jwnhydxr9hhkbbljmksdh7hd79sxjagn25gwm1s17l8")))

(define-public crate-smodel-1 (crate (name "smodel") (vers "1.0.9") (deps (list (crate-dep (name "by_address") (req "^1.1.0") (default-features #t) (kind 0)) (crate-dep (name "smodel-proc") (req "^1.0") (default-features #t) (kind 0)))) (hash "141vvz1jdmwhhpdl833jhysx6z1qgw6b3p2s99piw0gbdmvyyi78")))

(define-public crate-smodel-1 (crate (name "smodel") (vers "1.0.10") (deps (list (crate-dep (name "by_address") (req "^1.1.0") (default-features #t) (kind 0)) (crate-dep (name "smodel-proc") (req "^1.0") (default-features #t) (kind 0)))) (hash "1p45ab01qwrn85sjjr37xacg281c1gdrk6h1m9j0hra235s4js3j")))

(define-public crate-smodel-1 (crate (name "smodel") (vers "1.0.11") (deps (list (crate-dep (name "by_address") (req "^1.1.0") (default-features #t) (kind 0)) (crate-dep (name "smodel-proc") (req "^1.0") (default-features #t) (kind 0)))) (hash "1ca0r1030lzgy7bi05gn4zb1gldw64hj23fzawgh3ss6g2gqa279")))

(define-public crate-smodel-1 (crate (name "smodel") (vers "1.0.12") (deps (list (crate-dep (name "by_address") (req "^1.1.0") (default-features #t) (kind 0)) (crate-dep (name "smodel-proc") (req "^1.0") (default-features #t) (kind 0)))) (hash "1azv5acshs3xsy9nycfsijgq7lp5kpay4g0n0rlgjvb93vlfyi5l")))

(define-public crate-smodel-1 (crate (name "smodel") (vers "1.0.13") (deps (list (crate-dep (name "by_address") (req "^1.1.0") (default-features #t) (kind 0)) (crate-dep (name "smodel-proc") (req "^1.0") (default-features #t) (kind 0)))) (hash "134gqbxbfj94svspy0gy2jhd5p2a4nv587ayx7slzd2j575bnsyz")))

(define-public crate-smodel-proc-0.1 (crate (name "smodel-proc") (vers "0.1.0") (deps (list (crate-dep (name "by_address") (req "^1.1.0") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1") (features (quote ("nightly"))) (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2.0.38") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "09i5sflzi41w2v2nvkbr5q1dj413ngki1yjdkdv6fm9nd5416pfl")))

(define-public crate-smodel-proc-1 (crate (name "smodel-proc") (vers "1.0.0") (deps (list (crate-dep (name "by_address") (req "^1.1.0") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1") (features (quote ("nightly"))) (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2.0.38") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "043fk0mjpb06ldmjvqwskxdmmdysvnj90fnazv8fay3fjbhvwnq3")))

(define-public crate-smodel-proc-1 (crate (name "smodel-proc") (vers "1.0.1") (deps (list (crate-dep (name "by_address") (req "^1.1.0") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1") (features (quote ("nightly"))) (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2.0.38") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "0w5pfid8fxb4x9mm6pizy62213lmj8jfk2xvmhl39h56mimaszwr")))

(define-public crate-smodel-proc-1 (crate (name "smodel-proc") (vers "1.0.2") (deps (list (crate-dep (name "by_address") (req "^1.1.0") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1") (features (quote ("nightly"))) (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2.0.38") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "0ca5jh5b14swh1i0la8xyb1lg40wx8n3x7qwnid3r16492fcyjad")))

(define-public crate-smodel-proc-1 (crate (name "smodel-proc") (vers "1.0.3") (deps (list (crate-dep (name "by_address") (req "^1.1.0") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1") (features (quote ("nightly"))) (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2.0.38") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "111h0d8nka2fzqa1yjxskq285id6az9bgim82hwj4v41mkxk9p00")))

(define-public crate-smodel-proc-1 (crate (name "smodel-proc") (vers "1.0.4") (deps (list (crate-dep (name "by_address") (req "^1.1.0") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1") (features (quote ("nightly"))) (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2.0.38") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "1swcnmli02yp4ggrd75zrgazngv4ikpcz3xd0mllwzydz2i1rzpy")))

(define-public crate-smodel-proc-1 (crate (name "smodel-proc") (vers "1.0.5") (deps (list (crate-dep (name "by_address") (req "^1.1.0") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1") (features (quote ("nightly"))) (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2.0.38") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "02fzd8q9z5lh4hfb6y802cw1q5ci61i1qn7w97g6v6gfa602k4bw")))

(define-public crate-smodel-proc-1 (crate (name "smodel-proc") (vers "1.0.6") (deps (list (crate-dep (name "by_address") (req "^1.1.0") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1") (features (quote ("nightly"))) (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2.0.38") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "15j0x0c7xl9slyxpal8pkzwx422rk0qmh86gqvsmqbas0chl2mf6")))

(define-public crate-smodel-proc-1 (crate (name "smodel-proc") (vers "1.0.7") (deps (list (crate-dep (name "by_address") (req "^1.1.0") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1") (features (quote ("nightly"))) (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2.0.38") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "19ailqm5cf5c2ifph9lxp7gw36gl3hhx63j3gdvalg08mrisd4wz")))

(define-public crate-smodel-proc-1 (crate (name "smodel-proc") (vers "1.0.8") (deps (list (crate-dep (name "by_address") (req "^1.1.0") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1") (features (quote ("nightly"))) (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2.0.38") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "1xw1kj7bdnz9l204zpl9b2f0yfzkn3jkcrbsndd269lrwdqsmqnp")))

(define-public crate-smodel-proc-1 (crate (name "smodel-proc") (vers "1.0.9") (deps (list (crate-dep (name "by_address") (req "^1.1.0") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1") (features (quote ("nightly"))) (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2.0.38") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "0k8jx3ydnap3if4j9mhwn0d0fiarn3k8y787b1ydgjbn52f6h30j")))

(define-public crate-smodel-proc-1 (crate (name "smodel-proc") (vers "1.0.10") (deps (list (crate-dep (name "by_address") (req "^1.1.0") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1") (features (quote ("nightly"))) (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2.0.38") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "0pbkbb9d2s31fdblj7rwq8fa4swxg6k5g1ka9fpmaivz01wf3gx3")))

(define-public crate-smodel-proc-1 (crate (name "smodel-proc") (vers "1.0.11") (deps (list (crate-dep (name "by_address") (req "^1.1.0") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1") (features (quote ("nightly"))) (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2.0.38") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "15kxn16xgmvhdjqsvysxm0blhg636ffd99x6z54xda0nh3g895lv")))

