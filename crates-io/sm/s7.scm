(define-module (crates-io sm s7) #:use-module (crates-io))

(define-public crate-sms77-client-0.1 (crate (name "sms77-client") (vers "0.1.0") (deps (list (crate-dep (name "rand") (req "^0.8.3") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "ureq") (req "^2.0.2") (features (quote ("json"))) (default-features #t) (kind 0)))) (hash "070p6is0l86hag6zs3c5nwz6wnwsalkyqrrms9z8zd2r4jc6ql2b")))

(define-public crate-sms77-client-0.2 (crate (name "sms77-client") (vers "0.2.0") (deps (list (crate-dep (name "rand") (req "^0.8.3") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "ureq") (req "^2.0.2") (features (quote ("json"))) (default-features #t) (kind 0)))) (hash "0w4hz0hb26qwy79ji1sgb5xnnj3g6xf9mknnhz29cykm6wdcqx93")))

