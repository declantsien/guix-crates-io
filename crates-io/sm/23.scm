(define-module (crates-io sm #{23}#) #:use-module (crates-io))

(define-public crate-sm2335egh-0.1 (crate (name "sm2335egh") (vers "0.1.0") (deps (list (crate-dep (name "embedded-hal") (req "=1.0.0-rc.1") (default-features #t) (kind 0)))) (hash "11nza8as1qcsrgx1xb6mrg83fz8449vs4kc787rmzdd1xksfvl36")))

(define-public crate-sm2335egh-0.1 (crate (name "sm2335egh") (vers "0.1.1") (deps (list (crate-dep (name "embedded-hal") (req "=1.0.0-rc.1") (default-features #t) (kind 0)))) (hash "1m0nh9hkzldzzad7sw2qcd1ly2yzcmn681dww1cisdcdg898pi7x")))

(define-public crate-sm2335egh-0.2 (crate (name "sm2335egh") (vers "0.2.0") (deps (list (crate-dep (name "embedded-hal") (req "^1.0.0") (default-features #t) (kind 0)))) (hash "02b76604l2jj2rsdvz1mzgf20qxvrkbfjzlp6cvzaz45jskd10jl")))

