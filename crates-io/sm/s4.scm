(define-module (crates-io sm s4) #:use-module (crates-io))

(define-public crate-sms4-0.1 (crate (name "sms4") (vers "0.1.0") (deps (list (crate-dep (name "block-cipher-trait") (req "^0.6") (default-features #t) (kind 0)) (crate-dep (name "block-cipher-trait") (req "^0.6") (features (quote ("dev"))) (default-features #t) (kind 2)) (crate-dep (name "byteorder") (req "^1") (kind 0)) (crate-dep (name "opaque-debug") (req "^0.2") (default-features #t) (kind 0)))) (hash "1crd4qbc1870phkiqvz6cx8wqnsh3fvv101a6kxrhp8qcipd1fkq")))

