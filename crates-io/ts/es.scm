(define-module (crates-io ts es) #:use-module (crates-io))

(define-public crate-tsesh-0.1 (crate (name "tsesh") (vers "0.1.2") (deps (list (crate-dep (name "anyhow") (req "^1.0.41") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^3.0.0-beta.2") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "shellexpand") (req "^2.1.0") (default-features #t) (kind 0)) (crate-dep (name "toml") (req "^0.5.8") (default-features #t) (kind 0)))) (hash "02mfvdf2npqy3dnzyyg3i1wbfi624jaibgyh7v0n34vr9qnvm0q8")))

