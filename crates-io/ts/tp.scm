(define-module (crates-io ts tp) #:use-module (crates-io))

(define-public crate-tstpmove-1 (crate (name "tstpmove") (vers "1.0.0") (deps (list (crate-dep (name "clap") (req "^4.5.4") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "1ys11hl8wn5w837q5awv1ykgabfc83mwylvv3c461ypqlivg52vk")))

(define-public crate-tstpmove-1 (crate (name "tstpmove") (vers "1.2.1") (deps (list (crate-dep (name "clap") (req "^4.5.4") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "1nv830lvafnac1hrsvzkdrq6q3vvn86fckbr2aycha0pzv26xgf4")))

(define-public crate-tstpmove-1 (crate (name "tstpmove") (vers "1.2.2") (deps (list (crate-dep (name "clap") (req "^4.5.4") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "13fa23qc3qswf26pwc6g67ym3p4rv2ck4w6z3vplb1ci8n8a8iqk")))

