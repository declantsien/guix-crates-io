(define-module (crates-io ts um) #:use-module (crates-io))

(define-public crate-tsum_calc-1 (crate (name "tsum_calc") (vers "1.0.0") (hash "07hgvbxndcgzklkr4ac1lsr9pnzklwrk8jgdnnm7phn8pc1qvivd")))

(define-public crate-tsumiki-0.0.1 (crate (name "tsumiki") (vers "0.0.1") (hash "1i9bg20rf7vqjib8h2v2h9fbzynd6jh24qhwmyhvyijr31g58n3l")))

