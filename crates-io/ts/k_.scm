(define-module (crates-io ts k_) #:use-module (crates-io))

(define-public crate-tsk_lib-0.0.0 (crate (name "tsk_lib") (vers "0.0.0") (deps (list (crate-dep (name "chrono") (req "^0.4") (features (quote ("serde"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)))) (hash "1x4ww7fvz5bfy5swrrr90hibzsfcy90x8k8d1ln0mjv9nds4jmr9")))

