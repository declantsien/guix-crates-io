(define-module (crates-io ts #{3-}#) #:use-module (crates-io))

(define-public crate-ts3-clientquery-lib-0.0.1 (crate (name "ts3-clientquery-lib") (vers "0.0.1-alpha") (deps (list (crate-dep (name "log") (req "^0.4.17") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.163") (default-features #t) (kind 0)) (crate-dep (name "serde-teamspeak-querystring") (req "^0.2.1") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0.163") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.28.1") (features (quote ("net" "time" "io-util"))) (default-features #t) (kind 0)))) (hash "0r8mg3bb3qjy1pqvny6dqmvrkgga01v679mznlsa9blrav9i6f90") (features (quote (("default"))))))

(define-public crate-ts3-clientquery-lib-0.0.1 (crate (name "ts3-clientquery-lib") (vers "0.0.1-beta") (deps (list (crate-dep (name "log") (req "^0.4.19") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.171") (default-features #t) (kind 0)) (crate-dep (name "serde-teamspeak-querystring") (req "^0.2.1") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0.171") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.29.1") (features (quote ("net" "time" "io-util"))) (default-features #t) (kind 0)))) (hash "19b40mb9h948zhjnjkg6gv1anbi003xxdp2gc69rfra5vj1bvpj3") (features (quote (("server") ("default") ("client") ("all" "log" "server" "client"))))))

(define-public crate-ts3-query-0.1 (crate (name "ts3-query") (vers "0.1.0") (deps (list (crate-dep (name "snafu") (req "^0.5") (default-features #t) (kind 0)))) (hash "13rm9aa5abksm7qrfh2k6gzbzi8zgp1pg4hyl1kf8rlvshdm100n")))

(define-public crate-ts3-query-0.1 (crate (name "ts3-query") (vers "0.1.1") (deps (list (crate-dep (name "snafu") (req "^0.5") (default-features #t) (kind 0)))) (hash "18c9cnf301d0sw02pkis0l6xzc9vwp73i4k8jyivm178bsq09q39")))

(define-public crate-ts3-query-0.1 (crate (name "ts3-query") (vers "0.1.2") (deps (list (crate-dep (name "snafu") (req "^0.5") (default-features #t) (kind 0)))) (hash "1ll99j3p47zma0dygihqiy4zg2bygyrkf2qa2maglf7k71ibzvbb")))

(define-public crate-ts3-query-0.1 (crate (name "ts3-query") (vers "0.1.3") (deps (list (crate-dep (name "snafu") (req "^0.5") (default-features #t) (kind 0)))) (hash "0i74mbdzcqbijbcsgpfclp1qvjagpcn9pg04m60lihzy1na211vc")))

(define-public crate-ts3-query-0.1 (crate (name "ts3-query") (vers "0.1.4") (deps (list (crate-dep (name "snafu") (req "^0.5") (default-features #t) (kind 0)))) (hash "18dj1axmqp3b3q6bmipxnmayn8s9a20q687c4hfmd3zqp4walhy6")))

(define-public crate-ts3-query-0.1 (crate (name "ts3-query") (vers "0.1.5") (deps (list (crate-dep (name "snafu") (req "^0.5") (default-features #t) (kind 0)))) (hash "1safqi6h2inn62vxq53c6fisbdvcdhkd29jkr98r3kw9l1awkn9n")))

(define-public crate-ts3-query-0.1 (crate (name "ts3-query") (vers "0.1.6") (deps (list (crate-dep (name "snafu") (req "^0.6") (default-features #t) (kind 0)))) (hash "131cyv67gvk0iaf0b23vf99d354nak037h3kay8ix7c0lxjbvwfs") (features (quote (("debug_response") ("backtrace" "snafu/backtraces")))) (yanked #t)))

(define-public crate-ts3-query-0.2 (crate (name "ts3-query") (vers "0.2.0") (deps (list (crate-dep (name "snafu") (req "^0.6") (default-features #t) (kind 0)))) (hash "1m8khmgj4vy1cxh7dn56njh2nirwawc85qpvg99h5m68qsfkl362") (features (quote (("debug_response") ("backtrace" "snafu/backtraces"))))))

(define-public crate-ts3-query-0.2 (crate (name "ts3-query") (vers "0.2.1") (deps (list (crate-dep (name "snafu") (req "^0.6") (default-features #t) (kind 0)))) (hash "1qf6m1b374agbnwj9a7ggx897gx7z79mvyhkdxi93q0dphyy1sc9") (features (quote (("debug_response") ("backtrace" "snafu/backtraces"))))))

(define-public crate-ts3-query-0.2 (crate (name "ts3-query") (vers "0.2.2") (deps (list (crate-dep (name "snafu") (req "^0.6") (default-features #t) (kind 0)))) (hash "1mw27jjqpf8ah591cpmd1zqix80jvsnws7w9zqkfn8q8ixzazjal") (features (quote (("managed") ("debug_response") ("backtrace" "snafu/backtraces"))))))

(define-public crate-ts3-query-0.2 (crate (name "ts3-query") (vers "0.2.3") (deps (list (crate-dep (name "snafu") (req "^0.6") (default-features #t) (kind 0)))) (hash "1kmamy49y5hwafz7yppx2m1k54qqryw2jz7zk9m662cmxrybfsm1") (features (quote (("managed") ("debug_response") ("backtrace" "snafu/backtraces"))))))

(define-public crate-ts3-query-0.3 (crate (name "ts3-query") (vers "0.3.0") (deps (list (crate-dep (name "snafu") (req "^0.6") (default-features #t) (kind 0)))) (hash "1ify9yiidm8rkz6rr5ps0xs25pw2cbvss3k8bpzqhri5gzifvsra") (features (quote (("managed") ("debug_response") ("backtrace" "snafu/backtraces"))))))

(define-public crate-ts3-query-0.3 (crate (name "ts3-query") (vers "0.3.1") (deps (list (crate-dep (name "snafu") (req "^0.6") (default-features #t) (kind 0)))) (hash "0z97szwqzzhfv84khjmajys2l0qklldra23k5bn4w0g1kn5n5yfd") (features (quote (("managed") ("debug_response") ("backtrace" "snafu/backtraces"))))))

(define-public crate-ts3-query-0.3 (crate (name "ts3-query") (vers "0.3.2") (deps (list (crate-dep (name "snafu") (req "^0.6") (default-features #t) (kind 0)))) (hash "1hk20r2a5mzk3mb9sai0vnw1977985zwhwx321dsc78pjgrny85n") (features (quote (("managed") ("debug_response") ("backtrace" "snafu/backtraces"))))))

