(define-module (crates-io ts uy) #:use-module (crates-io))

(define-public crate-tsuyopon_test_crate-0.0.1 (crate (name "tsuyopon_test_crate") (vers "0.0.1") (hash "1qmmzsj5wijyxrrizagm7v1a19dhykw3jz0w0ncgx780b5npk3mi") (yanked #t)))

(define-public crate-tsuyopon_test_crate-0.0.2 (crate (name "tsuyopon_test_crate") (vers "0.0.2") (hash "1zslk9p1nx0dzgrp1915d5ml659zcwnxqz4p856w72mhhfyfxprl") (yanked #t)))

(define-public crate-tsuyopon_test_crate-0.0.3 (crate (name "tsuyopon_test_crate") (vers "0.0.3") (hash "1b90vrx74jfflrcszn3300m19vx7d0q3f7bmf06niv873500zhmw")))

