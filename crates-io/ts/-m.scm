(define-module (crates-io ts -m) #:use-module (crates-io))

(define-public crate-ts-macros-0.1 (crate (name "ts-macros") (vers "0.1.0") (deps (list (crate-dep (name "proc-macro2") (req "^1.0.66") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2-diagnostics") (req "^0.10.1") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.33") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2") (features (quote ("parsing"))) (kind 0)))) (hash "1ivgrcz64r38aiq443ljjrkyj9354xfmb9l94nw1czrgzza375jj")))

(define-public crate-ts-macros-0.2 (crate (name "ts-macros") (vers "0.2.0") (deps (list (crate-dep (name "proc-macro2") (req "^1.0.66") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2-diagnostics") (req "^0.10.1") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.33") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2") (features (quote ("parsing"))) (kind 0)))) (hash "0mnp880h8prgbx4yrgqrj8pqphmpz48b28ibx0cpqpf7m8wha7wy")))

(define-public crate-ts-macros-0.3 (crate (name "ts-macros") (vers "0.3.0") (deps (list (crate-dep (name "heck") (req "^0.4.1") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1.0.66") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2-diagnostics") (req "^0.10.1") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.33") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2") (features (quote ("parsing"))) (kind 0)))) (hash "09n2spicb87kic46lvv2z40m5z06p2rp7xxs2pjl90apbg90mc35")))

(define-public crate-ts-macros-0.4 (crate (name "ts-macros") (vers "0.4.0") (deps (list (crate-dep (name "heck") (req "^0.4.1") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1.0.66") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2-diagnostics") (req "^0.10.1") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.33") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2") (features (quote ("parsing"))) (kind 0)))) (hash "1hflry7ylnkf4hha92mx6d69j56d5vvh970vpcah5bdy1c48rvn5")))

(define-public crate-ts-macros-0.4 (crate (name "ts-macros") (vers "0.4.1") (deps (list (crate-dep (name "heck") (req "^0.4.1") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1.0.66") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2-diagnostics") (req "^0.10.1") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.33") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2") (features (quote ("parsing"))) (kind 0)))) (hash "0yjmj1bzj5qcq0cpk512ywv721qyzm2mbwbhh3gwbfbq0ddb9xay")))

(define-public crate-ts-mem-pool-0.1 (crate (name "ts-mem-pool") (vers "0.1.0") (hash "0hn9hzwis0cgs61kr8hzampwg6klhhg3nysa1g599znqm7kvvlyq")))

(define-public crate-ts-mem-pool-0.1 (crate (name "ts-mem-pool") (vers "0.1.1") (hash "09x31r57r4il20870imw37hk18d3x9xqg81r1bbv35cnl1icqsf5")))

(define-public crate-ts-mem-pool-0.1 (crate (name "ts-mem-pool") (vers "0.1.2") (hash "09qsza3fq61rwa24szd0sq3bfmb3pf1dgyiralh3ccanzzk2cdix")))

(define-public crate-ts-mem-pool-0.1 (crate (name "ts-mem-pool") (vers "0.1.3") (hash "09x5mrg85gyy4hnwp819xsvi97w11nvjm42gklajmj2v8hvng8ia")))

