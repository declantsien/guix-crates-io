(define-module (crates-io ts -f) #:use-module (crates-io))

(define-public crate-ts-fmt-lite-0.1 (crate (name "ts-fmt-lite") (vers "0.1.0") (deps (list (crate-dep (name "derive_builder") (req "^0.7.2") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 2)))) (hash "0kyvpn1q3i4sgqalqsv65xbaqw6dd6g123h09syffl4asb093qzb") (features (quote (("naive-wrap") ("default" "naive-wrap"))))))

(define-public crate-ts-fmt-lite-0.1 (crate (name "ts-fmt-lite") (vers "0.1.1") (deps (list (crate-dep (name "derive_builder") (req "^0.7.2") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 2)))) (hash "0m0rz8x0mnd6zbcp2h8jl3hsijanis2kllfjy4smzlii3gydlda0") (features (quote (("naive-wrap") ("default" "naive-wrap"))))))

