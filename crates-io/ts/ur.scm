(define-module (crates-io ts ur) #:use-module (crates-io))

(define-public crate-tsur-0.1 (crate (name "tsur") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^2.0") (default-features #t) (kind 0)))) (hash "1sqvzaxmyasmmyxsl8ss5bmfk7kxz1zdpyab9s1nhpxf68a0nhb3")))

(define-public crate-tsur-0.1 (crate (name "tsur") (vers "0.1.1") (deps (list (crate-dep (name "clap") (req "^2.0.0") (default-features #t) (kind 0)))) (hash "0k1dsaync74bjz0fmx928xrni9gaq02dcab2ik6h5mwxbh3g285c")))

(define-public crate-tsur-0.1 (crate (name "tsur") (vers "0.1.2") (deps (list (crate-dep (name "clap") (req "^2.0.0") (default-features #t) (kind 0)))) (hash "1yknphlgfsv5zkkpaiby7i5nnz6lrzm3k56988ikd9acn79rk7n0")))

(define-public crate-tsur-0.1 (crate (name "tsur") (vers "0.1.3") (deps (list (crate-dep (name "clap") (req "^2.0.0") (default-features #t) (kind 0)))) (hash "1skksz23z2fzxgvh78a1hd4ys86b4fjywd1f63vh7kp6wsi30m71")))

(define-public crate-tsur-0.1 (crate (name "tsur") (vers "0.1.4") (deps (list (crate-dep (name "clap") (req "^2.0.0") (default-features #t) (kind 0)))) (hash "1amshs5s0b41mqr9j0ashlbwmnc04aqc95sy2ricih98032f7kf5")))

(define-public crate-tsur-0.1 (crate (name "tsur") (vers "0.1.6") (deps (list (crate-dep (name "clap") (req "^2.0.0") (default-features #t) (kind 0)))) (hash "0nzds9wxqdbllyby387xah2igimzr08jmxg3cl0yh7bpvyjgadzr")))

(define-public crate-tsuru-0.0.1 (crate (name "tsuru") (vers "0.0.1") (hash "1dp1dgcriv64gp6b929fc3sdkhm6wmd50lycs5dkjzpmm90mbb4d")))

