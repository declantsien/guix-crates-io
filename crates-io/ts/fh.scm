(define-module (crates-io ts fh) #:use-module (crates-io))

(define-public crate-tsfh-0.75 (crate (name "tsfh") (vers "0.75.0") (deps (list (crate-dep (name "embed-resource") (req "^2.2.0") (default-features #t) (target "cfg(windows)") (kind 1)))) (hash "1xq3zxwjvy150hs73zhas9kj0gw8p38bam3knckxvih3vkdmv5yj") (yanked #t) (rust-version "1.69")))

