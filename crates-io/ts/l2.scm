(define-module (crates-io ts l2) #:use-module (crates-io))

(define-public crate-tsl256x-0.1 (crate (name "tsl256x") (vers "0.1.0") (deps (list (crate-dep (name "embedded-hal") (req "^0.2.1") (default-features #t) (kind 0)))) (hash "1926hcbix6b54n6rd3qv4j08l6f6374xpw09a90cb8bk5dcl1hhy")))

(define-public crate-tsl2591-0.1 (crate (name "tsl2591") (vers "0.1.0") (deps (list (crate-dep (name "bitfield") (req "^0.13.2") (default-features #t) (kind 0)) (crate-dep (name "embedded-hal") (req "^0.2.4") (default-features #t) (kind 0)))) (hash "1g7kkaigq19cvmqzrzxwmz71xi6z679bdnl27x1lracdlgwah879")))

(define-public crate-tsl2591-0.2 (crate (name "tsl2591") (vers "0.2.0") (deps (list (crate-dep (name "bitfield") (req "^0.13.2") (default-features #t) (kind 0)) (crate-dep (name "embedded-hal") (req "^0.2.4") (default-features #t) (kind 0)))) (hash "1xv0v6i11xv7wqpahiasl5lcq037ziivayjx19rbyzy6dxqv5yqb")))

(define-public crate-tsl2591-eh-driver-0.3 (crate (name "tsl2591-eh-driver") (vers "0.3.0") (deps (list (crate-dep (name "bitfield") (req "^0.14.0") (default-features #t) (kind 0)) (crate-dep (name "embedded-hal") (req "^1.0.0-alpha.10") (default-features #t) (kind 0)))) (hash "12287jvrgj9q0dacvs1h4cnw7hzbn0h2ryxzqn4pg4dv7347nlhm")))

(define-public crate-tsl2591-eh-driver-0.4 (crate (name "tsl2591-eh-driver") (vers "0.4.0") (deps (list (crate-dep (name "bitfield") (req "^0.14.0") (default-features #t) (kind 0)) (crate-dep (name "embedded-hal") (req "=1.0.0-alpha.10") (default-features #t) (kind 0)))) (hash "0gd5dw69rhq80z11x42p18vqvd5h902h990dl95jjdrqvfvqh2a1")))

(define-public crate-tsl2591-eh-driver-0.5 (crate (name "tsl2591-eh-driver") (vers "0.5.0") (deps (list (crate-dep (name "bitfield") (req "^0.14.0") (default-features #t) (kind 0)) (crate-dep (name "embedded-hal") (req "^1.0.0") (default-features #t) (kind 0)))) (hash "1l0s5fgqxa67f6s2i06fxc6cz3z5myv5i09hb1x9jbr90mhc5sc1")))

