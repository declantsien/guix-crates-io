(define-module (crates-io ts c2) #:use-module (crates-io))

(define-public crate-tsc2046-0.1 (crate (name "tsc2046") (vers "0.1.0") (deps (list (crate-dep (name "bitflags") (req "^2.4.2") (default-features #t) (kind 0)) (crate-dep (name "embedded-hal") (req "^1.0.0") (default-features #t) (kind 0)) (crate-dep (name "mockall") (req "^0.12.1") (default-features #t) (kind 2)))) (hash "1qkx2n53xmxvxcxgm0x2gvyfalkcl94wadc9n50pfnfp3nc1anyg")))

