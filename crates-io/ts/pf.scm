(define-module (crates-io ts pf) #:use-module (crates-io))

(define-public crate-tspf-0.1 (crate (name "tspf") (vers "0.1.0") (hash "0gxdr9gc9l4kzni113kmc1yzri7wax28qkrn2m57asgvpdv0zgha")))

(define-public crate-tspf-0.1 (crate (name "tspf") (vers "0.1.1") (hash "1yqgq6ggfh9l9wr5ydfnjnlxhq9xgdfwn4kg43llxgfdf1yfr156")))

(define-public crate-tspf-0.2 (crate (name "tspf") (vers "0.2.0") (hash "0avnh6x9bn4ic22dn26rsbpsm2spic4mny95x9zvkdd6fr5swdm5")))

(define-public crate-tspf-0.3 (crate (name "tspf") (vers "0.3.0") (deps (list (crate-dep (name "getset") (req "^0.1.1") (default-features #t) (kind 0)))) (hash "02nkb2884r5ql0bkaphgdxy340p5n35zi8ihk1pjqq21hmhadag1")))

(define-public crate-tspf-0.3 (crate (name "tspf") (vers "0.3.1") (deps (list (crate-dep (name "approx") (req "^0.5.0") (default-features #t) (kind 2)) (crate-dep (name "getset") (req "^0.1.1") (default-features #t) (kind 0)))) (hash "1vxg6znh7swi70msjhng6fcaxf7l2k7hzqcqwmqf1iijcl39wdfd")))

