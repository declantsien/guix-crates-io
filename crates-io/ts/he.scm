(define-module (crates-io ts he) #:use-module (crates-io))

(define-public crate-tshell-0.1 (crate (name "tshell") (vers "0.1.0") (deps (list (crate-dep (name "env_logger") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "rustyline") (req "^1.0.0") (default-features #t) (kind 0)))) (hash "0ci5vfv396wn46y4pdb7jwh9xwghi2nr0jk6c751hyqlcqj4by5m") (yanked #t)))

(define-public crate-tshell-0.1 (crate (name "tshell") (vers "0.1.1") (deps (list (crate-dep (name "env_logger") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "rustyline") (req "^1.0.0") (default-features #t) (kind 0)))) (hash "1vi33668fxv5w9n03v4768b79fcl5gwigrxhw8cyjfv5g63707s3")))

(define-public crate-tshell-0.1 (crate (name "tshell") (vers "0.1.2") (deps (list (crate-dep (name "env_logger") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "rustyline") (req "^1.0.0") (default-features #t) (kind 0)))) (hash "0bfwydjm8j0c0183ffdv69k25ywcjhmib8aka0pbiv887zszhibl")))

(define-public crate-tshell-0.1 (crate (name "tshell") (vers "0.1.3") (deps (list (crate-dep (name "env_logger") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "rustyline") (req "^1.0.0") (default-features #t) (kind 0)))) (hash "0fizkngnflw4nk4v137w459x6gbhxs0bgqcrc7kpqaxgylwa6cx7")))

(define-public crate-tshell-0.2 (crate (name "tshell") (vers "0.2.1") (deps (list (crate-dep (name "env_logger") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "rustyline") (req "^1.0.0") (default-features #t) (kind 0)))) (hash "16v7bvi1km42h9vsyx820bb45vk497785s84628czv95q2xqcqfd")))

(define-public crate-tshell-0.2 (crate (name "tshell") (vers "0.2.2") (deps (list (crate-dep (name "env_logger") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "rustyline") (req "^1.0.0") (default-features #t) (kind 0)))) (hash "0463xvzsnqaam9whcvpgpb16fi3gv3rhn3szrdx5lrqrz7lqmk58")))

