(define-module (crates-io ts ot) #:use-module (crates-io))

(define-public crate-tsotest-0.1 (crate (name "tsotest") (vers "0.1.0") (hash "1g5d7x5djap0s9n61hrmaf5xj8bi2ypzfgxj1dn8cw5m7pi87sgp") (yanked #t)))

(define-public crate-tsotest-0.2 (crate (name "tsotest") (vers "0.2.0") (deps (list (crate-dep (name "sysctl") (req "^0.4") (optional #t) (default-features #t) (kind 0)))) (hash "1jjsdi4pyhjibhz9s2888f1kyny7jcrnsva61802hbbd52h1myx8") (features (quote (("default") ("apple-tso" "sysctl"))))))

