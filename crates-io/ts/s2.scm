(define-module (crates-io ts s2) #:use-module (crates-io))

(define-public crate-tss2-0.1 (crate (name "tss2") (vers "0.1.0") (deps (list (crate-dep (name "bindgen") (req "^0") (default-features #t) (kind 1)))) (hash "0l9i9vzqxkdzikbc4cvyzav619ihhda6bf605b10a33i2zliazg2") (features (quote (("sys") ("fapi") ("esys") ("default" "sys" "esys" "fapi"))))))

(define-public crate-tss2-0.1 (crate (name "tss2") (vers "0.1.1") (deps (list (crate-dep (name "bindgen") (req "^0") (default-features #t) (kind 1)))) (hash "0n8s8xybwj5pdzaq3z0r0nqkn9ij353krsl419g4kszxwwkllqw7") (features (quote (("sys") ("fapi") ("esys") ("default" "sys" "esys" "fapi"))))))

(define-public crate-tss2-0.1 (crate (name "tss2") (vers "0.1.2") (deps (list (crate-dep (name "bindgen") (req "^0") (default-features #t) (kind 1)))) (hash "19sckj5qf8z46jmsa4gm5zrqfccfb2qx2qcfpni9nx20k0rd9nkb") (features (quote (("sys") ("fapi") ("esys") ("default" "sys" "esys" "fapi"))))))

(define-public crate-tss2-0.1 (crate (name "tss2") (vers "0.1.3") (deps (list (crate-dep (name "bindgen") (req "^0") (default-features #t) (kind 1)))) (hash "0sayvswcs57i8h9qxmlcl6615rnsidjfqni3jyy3q5drbqk5d30n") (features (quote (("sys") ("fapi") ("esys") ("default" "sys" "esys" "fapi"))))))

