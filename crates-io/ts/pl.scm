(define-module (crates-io ts pl) #:use-module (crates-io))

(define-public crate-TSPL-0.0.1 (crate (name "TSPL") (vers "0.0.1") (deps (list (crate-dep (name "highlight_error") (req "^0.1.1") (default-features #t) (kind 0)))) (hash "13w0z9wamd153cjyq4nmnxvq8hb7a2nhwnbikdcz0wkl2qybrxyp")))

(define-public crate-TSPL-0.0.2 (crate (name "TSPL") (vers "0.0.2") (deps (list (crate-dep (name "highlight_error") (req "^0.1.1") (default-features #t) (kind 0)))) (hash "1w1jd8zsj652d81acn22skcpsl147zj6s2ikkm609mv82rcd7ikh")))

(define-public crate-TSPL-0.0.3 (crate (name "TSPL") (vers "0.0.3") (deps (list (crate-dep (name "highlight_error") (req "^0.1.1") (default-features #t) (kind 0)))) (hash "1iwprvvziknyqbvnpzrpvjdw87fdznw6gv7wv16j20491j87c5yk")))

(define-public crate-TSPL-0.0.4 (crate (name "TSPL") (vers "0.0.4") (deps (list (crate-dep (name "highlight_error") (req "^0.1.1") (default-features #t) (kind 0)))) (hash "1y854na39zznly1zp3wmywmw303zbvhra1a1sgmlif9g87kw73jv")))

(define-public crate-TSPL-0.0.5 (crate (name "TSPL") (vers "0.0.5") (deps (list (crate-dep (name "highlight_error") (req "^0.1.1") (default-features #t) (kind 0)))) (hash "0rcbkrkznbsb0704riz5si6zprr3z9s34fz7vz3rf3sbcq5a6ivm")))

(define-public crate-TSPL-0.0.6 (crate (name "TSPL") (vers "0.0.6") (deps (list (crate-dep (name "highlight_error") (req "^0.1.1") (default-features #t) (kind 0)))) (hash "1bmngib8f9yy8k9i2d2gbhkqj0mrmd8hl4p4kzm9wjzsv8xldvf1")))

(define-public crate-TSPL-0.0.7 (crate (name "TSPL") (vers "0.0.7") (deps (list (crate-dep (name "highlight_error") (req "^0.1.1") (default-features #t) (kind 0)))) (hash "0irs7i1wlks4vq8kgimp12cc4dz443z1n0qcz9ibv1gr0xa712i1")))

(define-public crate-TSPL-0.0.8 (crate (name "TSPL") (vers "0.0.8") (deps (list (crate-dep (name "highlight_error") (req "^0.1.1") (default-features #t) (kind 0)))) (hash "1w46ljxikagxhgzqz43nimci4d5b7v14pl16z6x64pqz2z36rwhw")))

(define-public crate-TSPL-0.0.9 (crate (name "TSPL") (vers "0.0.9") (deps (list (crate-dep (name "highlight_error") (req "^0.1.1") (default-features #t) (kind 0)))) (hash "09fwyz06grz66aaa5zisw5cm7x9hkj7zaq1nq2xw1mp2wsqj752s")))

(define-public crate-TSPL-0.0.10 (crate (name "TSPL") (vers "0.0.10") (deps (list (crate-dep (name "highlight_error") (req "^0.1.1") (default-features #t) (kind 0)))) (hash "197g98p688n3y5qrvx8ib3vkarar5d586xrkn6a1f34zj9z9gq07")))

(define-public crate-TSPL-0.0.11 (crate (name "TSPL") (vers "0.0.11") (deps (list (crate-dep (name "highlight_error") (req "^0.1.1") (default-features #t) (kind 0)))) (hash "0s9yvbln8gjrigrq7yfbzlq5nrcg06mi7mqgcg42g0b93xpj0liq")))

(define-public crate-TSPL-0.0.12 (crate (name "TSPL") (vers "0.0.12") (deps (list (crate-dep (name "highlight_error") (req "^0.1.1") (default-features #t) (kind 0)))) (hash "1dnzns8gbr8aahy1nj60diw9qysfbfl8ardj6nbbjq8licixdpsj")))

(define-public crate-tsplib-0.1 (crate (name "tsplib") (vers "0.1.0") (hash "1gkmyvhpbss54ds24h029j4j5i4i1vbv2xazblivz0j09a5yj7px")))

