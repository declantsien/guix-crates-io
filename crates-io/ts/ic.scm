(define-module (crates-io ts ic) #:use-module (crates-io))

(define-public crate-tsic-0.1 (crate (name "tsic") (vers "0.1.0") (deps (list (crate-dep (name "embedded-hal") (req "^0.2") (features (quote ("unproven"))) (default-features #t) (kind 0)))) (hash "10svjvnzz009zg976545np1kr8cr08crqgarnlj9vqjrcwd40258")))

(define-public crate-tsic-0.2 (crate (name "tsic") (vers "0.2.0") (deps (list (crate-dep (name "embedded-hal") (req "^0.2") (features (quote ("unproven"))) (default-features #t) (kind 0)))) (hash "0bfhvz7mask6z09kjxbaa9ydn9xd14q6mjxxwwrd0lscrxhak113")))

(define-public crate-tsic-0.2 (crate (name "tsic") (vers "0.2.1") (deps (list (crate-dep (name "embedded-hal") (req "^0.2") (features (quote ("unproven"))) (default-features #t) (kind 0)))) (hash "19ms5fpwyw84h4x8d0q8ypnmwiqadyv5gkc7i7xldyh3cwjla0fc")))

