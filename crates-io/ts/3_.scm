(define-module (crates-io ts #{3_}#) #:use-module (crates-io))

(define-public crate-ts3_derive-0.1 (crate (name "ts3_derive") (vers "0.1.0") (deps (list (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.9") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.67") (default-features #t) (kind 0)))) (hash "12f0yzl95hq7gidgczn4q331b3l799kfrdfnk861m3i75lksbcmw")))

(define-public crate-ts3_derive-0.1 (crate (name "ts3_derive") (vers "0.1.2") (deps (list (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.9") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.67") (default-features #t) (kind 0)))) (hash "110py7dhjbkhpjfr4didn4chqfvpijcnydrdflm53j1xi40y2wkp")))

(define-public crate-ts3_derive-0.2 (crate (name "ts3_derive") (vers "0.2.0") (deps (list (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.9") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.67") (default-features #t) (kind 0)))) (hash "05cwqwa2smx0f0q0mandb3yfxr49qc0b06a29y0kfljkmhf16dci")))

(define-public crate-ts3_derive-0.4 (crate (name "ts3_derive") (vers "0.4.0") (deps (list (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.9") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.67") (default-features #t) (kind 0)))) (hash "0fdzaz4zpbpsls6chblnyn2y4l7yn0jxx218av2ypxkwvfgy4hfd")))

