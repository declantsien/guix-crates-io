(define-module (crates-io ts pa) #:use-module (crates-io))

(define-public crate-tspa-0.1 (crate (name "tspa") (vers "0.1.0") (deps (list (crate-dep (name "actix-files") (req "^0.3.0-alpha.1") (default-features #t) (kind 0)) (crate-dep (name "actix-rt") (req "^1.1") (default-features #t) (kind 0)) (crate-dep (name "actix-web") (req "^3.0.0-alpha.3") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^2.33") (default-features #t) (kind 0)) (crate-dep (name "env_logger") (req "^0.7") (default-features #t) (kind 0)))) (hash "0k0s6xplprn46vv8q3yq0r2n3j58s11qis3bizjw0ra5imbxkxqn") (yanked #t)))

(define-public crate-tspa-0.1 (crate (name "tspa") (vers "0.1.1") (deps (list (crate-dep (name "actix-files") (req "^0.3.0-alpha.1") (default-features #t) (kind 0)) (crate-dep (name "actix-rt") (req "^1.1") (default-features #t) (kind 0)) (crate-dep (name "actix-web") (req "^3.0.0-alpha.3") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^2.33") (default-features #t) (kind 0)) (crate-dep (name "env_logger") (req "^0.7") (default-features #t) (kind 0)))) (hash "1bp91d1g6qk20rg7nf1fyrlb3hb4f1f2a96ggvwdpqz0hxngnbfv")))

