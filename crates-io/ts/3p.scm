(define-module (crates-io ts #{3p}#) #:use-module (crates-io))

(define-public crate-ts3plugin-0.1 (crate (name "ts3plugin") (vers "0.1.0") (deps (list (crate-dep (name "chrono") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "clippy") (req "^0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^0.2") (default-features #t) (kind 2)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "skeptic") (req "^0.6") (default-features #t) (kind 1)) (crate-dep (name "skeptic") (req "^0.6") (default-features #t) (kind 2)) (crate-dep (name "ts3plugin-sys") (req "^0.2") (default-features #t) (kind 0)))) (hash "10yw57wiik1vdzckg3l5912qchh96skf6pbfvc2aif0l7y22kjvd") (features (quote (("default"))))))

(define-public crate-ts3plugin-0.2 (crate (name "ts3plugin") (vers "0.2.0") (deps (list (crate-dep (name "chrono") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "skeptic") (req "^0.6") (default-features #t) (kind 1)) (crate-dep (name "skeptic") (req "^0.6") (default-features #t) (kind 2)) (crate-dep (name "ts3plugin-sys") (req "^0.3") (default-features #t) (kind 0)))) (hash "126w6kvxjm5gv6qkrxirr1n174d3nkymlifm8kshn4f4q76sc8sy") (features (quote (("default")))) (yanked #t)))

(define-public crate-ts3plugin-0.2 (crate (name "ts3plugin") (vers "0.2.1") (deps (list (crate-dep (name "chrono") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "skeptic") (req "^0.6") (default-features #t) (kind 1)) (crate-dep (name "skeptic") (req "^0.6") (default-features #t) (kind 2)) (crate-dep (name "ts3plugin-sys") (req "^0.3") (default-features #t) (kind 0)))) (hash "0avv4mi0px0ard50r07vqnxdggccpvwlvg1snpj32grp1bmnrh4r") (features (quote (("default"))))))

(define-public crate-ts3plugin-sys-0.1 (crate (name "ts3plugin-sys") (vers "0.1.0") (deps (list (crate-dep (name "bitflags") (req "^0.7") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "1qd7x2y9xl07670s6nhpjrk6a387x395ixfqyg9c61mcjjjhg8by")))

(define-public crate-ts3plugin-sys-0.2 (crate (name "ts3plugin-sys") (vers "0.2.0") (deps (list (crate-dep (name "bitflags") (req "^0.7") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "01wx2fg0hfflv61xbs28dpy9s2wgfby0czi19650g1n4482dmplk")))

(define-public crate-ts3plugin-sys-0.3 (crate (name "ts3plugin-sys") (vers "0.3.0") (deps (list (crate-dep (name "bitflags") (req "^0.7") (default-features #t) (kind 0)))) (hash "0pd54d4m2gyr2npzvch781n0aah6i2rxswqw6f7lz1rjyj7p3xbw")))

(define-public crate-ts3plugin-sys-0.4 (crate (name "ts3plugin-sys") (vers "0.4.0") (deps (list (crate-dep (name "bitflags") (req "^1") (default-features #t) (kind 0)))) (hash "1lkjkqijb8zph6b1x04ippls7bnb6k66p80pvwa9kifm4q38cayl")))

