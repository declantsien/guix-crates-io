(define-module (crates-io ts il) #:use-module (crates-io))

(define-public crate-tsil_cev-0.1 (crate (name "tsil_cev") (vers "0.1.0") (deps (list (crate-dep (name "criterion") (req "^0.3.3") (default-features #t) (kind 2)))) (hash "148xq1samsw60jsy7qnjxbqrjbmy4f4r092h9x93p4w77bb98fgb")))

(define-public crate-tsil_cev-0.2 (crate (name "tsil_cev") (vers "0.2.0") (deps (list (crate-dep (name "criterion") (req "^0.3.3") (default-features #t) (kind 2)))) (hash "1n9baj1fjiwrghyb371nyplplnqpikldqjlvax5dfhrlzh4smkg0")))

(define-public crate-tsil_cev-0.3 (crate (name "tsil_cev") (vers "0.3.0") (deps (list (crate-dep (name "criterion") (req "^0.3.3") (default-features #t) (kind 2)) (crate-dep (name "serde") (req "^1.0") (optional #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 2)))) (hash "0rjin3zvjsx78mlw445gwqskz5jf1qyfly9gwdpfrlqq3g1bpcnh")))

(define-public crate-tsil_cev-1 (crate (name "tsil_cev") (vers "1.0.0") (deps (list (crate-dep (name "criterion") (req "^0.3.3") (default-features #t) (kind 2)) (crate-dep (name "serde") (req "^1.0") (optional #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 2)))) (hash "1smvval6n32x39zrlqxp6dy7c08z84xnv2l88pqkhfcgyn7xfi5a")))

(define-public crate-tsil_cev-1 (crate (name "tsil_cev") (vers "1.1.0") (deps (list (crate-dep (name "criterion") (req "^0.3.3") (default-features #t) (kind 2)) (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 2)) (crate-dep (name "serde") (req "^1.0") (optional #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 2)))) (hash "0g5yd703giqkvmpfry8w712bm64id44k6kqwza6cnkyfapv8zk15")))

