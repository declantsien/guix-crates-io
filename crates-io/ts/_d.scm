(define-module (crates-io ts _d) #:use-module (crates-io))

(define-public crate-ts_deplint-0.0.1 (crate (name "ts_deplint") (vers "0.0.1") (deps (list (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_yaml") (req "^0.8") (default-features #t) (kind 0)))) (hash "0bz0z97d4kslimx1i6awq14j3qi5cpm042qgvcsjpi2mgy91y7hy")))

(define-public crate-ts_deplint-0.0.2 (crate (name "ts_deplint") (vers "0.0.2") (deps (list (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_yaml") (req "^0.8") (default-features #t) (kind 0)))) (hash "004z9jd2szdib318s4cdcz0kw0mj26nj7yhmv2mwmzyqfw6wzk2z")))

(define-public crate-ts_deplint-0.0.3 (crate (name "ts_deplint") (vers "0.0.3") (deps (list (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_yaml") (req "^0.8") (default-features #t) (kind 0)))) (hash "1zjq11k6llgq647zglja8ax29lzn15ww65lhyw76iqa8i20nc4gs")))

(define-public crate-ts_deplint-0.0.4 (crate (name "ts_deplint") (vers "0.0.4") (deps (list (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_yaml") (req "^0.8") (default-features #t) (kind 0)))) (hash "01jcv0a2yyic92pyacxhcbzqgisbilqvj1vl7qdmz2jgriyqa8ra")))

(define-public crate-ts_deplint-0.0.5 (crate (name "ts_deplint") (vers "0.0.5") (deps (list (crate-dep (name "clap") (req "^4.4.6") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_yaml") (req "^0.8") (default-features #t) (kind 0)))) (hash "03jnx09q3syjx7c8k1l0p68440hhcm80xsdlzxfpz60lzcpffhzm")))

