(define-module (crates-io ts in) #:use-module (crates-io))

(define-public crate-tsinfo-0.1 (crate (name "tsinfo") (vers "0.1.0") (deps (list (crate-dep (name "chrono") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "chrono-humanize") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^2.33") (default-features #t) (kind 0)) (crate-dep (name "colored") (req "^2.0") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2") (default-features #t) (kind 0)))) (hash "1q0mcrnfa532bn9rc5i21gd7xj479d5n1991i2wgqwjkmir2i7pb")))

