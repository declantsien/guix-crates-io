(define-module (crates-io ts c-) #:use-module (crates-io))

(define-public crate-tsc-time-0.1 (crate (name "tsc-time") (vers "0.1.0") (hash "0bnag8yh57bs0lpfcxv1chqys18gqcp9qyygphzwxwaxb69f8d4r")))

(define-public crate-tsc-timer-0.1 (crate (name "tsc-timer") (vers "0.1.0") (hash "1hl1238my75hxjnv6c0nlc2wrs6f4vkm5va8yq3bavlgaqy3wghv")))

(define-public crate-tsc-timer-0.1 (crate (name "tsc-timer") (vers "0.1.1") (hash "0al1qqlapl26qvv2n1lgvzmipadlrbz4g8i1qv9fhsc4yqqafzqg")))

(define-public crate-tsc-timer-0.1 (crate (name "tsc-timer") (vers "0.1.2") (hash "1c8cs2a870bh81pwnkf7aby44bk930r653w540715f4kz65amwcs")))

(define-public crate-tsc-timer-0.1 (crate (name "tsc-timer") (vers "0.1.3") (hash "17y671xb7basj372is5l7k7l69iljkcw992jgpfqbb5fb2mapap3")))

(define-public crate-tsc-timer-0.1 (crate (name "tsc-timer") (vers "0.1.4") (hash "0d26m2k5zb5z6928gy72v7rnznzr1hb7rwy734spajisnwsw7ns5")))

(define-public crate-tsc-timer-0.1 (crate (name "tsc-timer") (vers "0.1.5") (hash "1ipjyc54wqa58ak4dnpa0n6dfkcvs1zlyim5jl172b237fqgadbs")))

(define-public crate-tsc-trace-0.4 (crate (name "tsc-trace") (vers "0.4.0") (deps (list (crate-dep (name "bytemuck") (req "^1.13.1") (default-features #t) (kind 0)) (crate-dep (name "criterion") (req "^0.4") (features (quote ("html_reports"))) (default-features #t) (kind 2)))) (hash "1wmqdrbg3whih2jgrynnjnmbpa4valw8g5rsvd2f14l9ss6x9cya") (features (quote (("off") ("lfence") ("default") ("capacity_8_million") ("capacity_64_million") ("capacity_32_million") ("capacity_1_million") ("capacity_16_million"))))))

(define-public crate-tsc-trace-0.5 (crate (name "tsc-trace") (vers "0.5.0") (deps (list (crate-dep (name "bytemuck") (req "^1.13.1") (default-features #t) (kind 0)) (crate-dep (name "criterion") (req "^0.4") (features (quote ("html_reports"))) (default-features #t) (kind 2)))) (hash "1arxzfbm48jlw0101cmanjr8lgzkvmqw9xfih49flpxyqjkcznpl") (features (quote (("off") ("lfence") ("default") ("capacity_8_million") ("capacity_64_million") ("capacity_32_million") ("capacity_1_million") ("capacity_16_million"))))))

(define-public crate-tsc-trace-0.6 (crate (name "tsc-trace") (vers "0.6.0") (deps (list (crate-dep (name "bytemuck") (req "^1.13.1") (default-features #t) (kind 0)) (crate-dep (name "criterion") (req "^0.4") (features (quote ("html_reports"))) (default-features #t) (kind 2)))) (hash "126dll8qqi0plyx4w579nfblhpliyc0r9y7wjb5d6mw2p55fy6np") (features (quote (("off") ("lfence") ("default") ("const_array") ("capacity_8_million") ("capacity_64_million") ("capacity_32_million") ("capacity_1_million") ("capacity_16_million"))))))

