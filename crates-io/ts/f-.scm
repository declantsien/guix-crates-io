(define-module (crates-io ts f-) #:use-module (crates-io))

(define-public crate-tsf-rs-0.1 (crate (name "tsf-rs") (vers "0.1.0") (deps (list (crate-dep (name "sma-rs") (req "^0.1.4") (default-features #t) (kind 0)) (crate-dep (name "ta-common") (req "^0.1.2") (default-features #t) (kind 0)))) (hash "1kci8ziw4d42s56rxqw6fr40p514165wmz1xnad4j22shfhsmgwc")))

(define-public crate-tsf-rs-0.1 (crate (name "tsf-rs") (vers "0.1.1") (deps (list (crate-dep (name "sma-rs") (req "^0.1.4") (default-features #t) (kind 0)) (crate-dep (name "ta-common") (req "^0.1.2") (default-features #t) (kind 0)))) (hash "07sc7917ypf7famy70wg5xvf2mslffd190sv8y5vv2c3r583i9kp")))

(define-public crate-tsf-sys-0.1 (crate (name "tsf-sys") (vers "0.1.0") (deps (list (crate-dep (name "bindgen") (req "^0.59.1") (default-features #t) (kind 1)) (crate-dep (name "cc") (req "^1.0.69") (default-features #t) (kind 1)))) (hash "13sllimb0h47kxw4b13x8qb64w3vqxpp05ixz9144i027c8yw9gl")))

(define-public crate-tsf-sys-0.1 (crate (name "tsf-sys") (vers "0.1.1") (deps (list (crate-dep (name "bindgen") (req "^0.59.1") (default-features #t) (kind 1)) (crate-dep (name "cc") (req "^1.0.69") (default-features #t) (kind 1)))) (hash "0m4c568mph9m1r5nshmxzfg26a7w8krbq5d3nc5333b4wqinfgli") (links "tsf")))

(define-public crate-tsf-sys-0.2 (crate (name "tsf-sys") (vers "0.2.0") (deps (list (crate-dep (name "bindgen") (req "^0.59.1") (default-features #t) (kind 1)) (crate-dep (name "cc") (req "^1.0.69") (default-features #t) (kind 1)))) (hash "0cgnnz797wl35cn0h4cflpfj4yzynna1qx8y156lgsbqxix6sj2z") (links "tsf")))

