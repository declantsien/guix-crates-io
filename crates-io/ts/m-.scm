(define-module (crates-io ts m-) #:use-module (crates-io))

(define-public crate-tsm-screen-0.1 (crate (name "tsm-screen") (vers "0.1.0") (deps (list (crate-dep (name "bindgen") (req "^0.26.3") (default-features #t) (kind 1)) (crate-dep (name "bitflags") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "cc") (req "^1.0") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2") (kind 0)))) (hash "0z9qm9dcimcfd3kypk2ry4xzrad3x1p6qn9x6r61j907hyc15z13")))

(define-public crate-tsm-screen-0.1 (crate (name "tsm-screen") (vers "0.1.1") (deps (list (crate-dep (name "bindgen") (req "^0.26.3") (default-features #t) (kind 1)) (crate-dep (name "bitflags") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "cc") (req "^1.0") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2") (kind 0)))) (hash "0lrrzglqh9ify3mbwbbp4p6sxszz1sfj0b7l2iw5nf40lpgj2m0f")))

(define-public crate-tsm-screen-0.1 (crate (name "tsm-screen") (vers "0.1.2") (deps (list (crate-dep (name "bindgen") (req "^0.26.3") (default-features #t) (kind 1)) (crate-dep (name "bitflags") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "cc") (req "^1.0") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2") (kind 0)))) (hash "089f2xz0v7brs4g29pfhngd5mrx849d6hjyzlw71jgyv0fwmw140")))

(define-public crate-tsm-sys-0.1 (crate (name "tsm-sys") (vers "0.1.0") (deps (list (crate-dep (name "gcc") (req "^0.3") (default-features #t) (kind 1)) (crate-dep (name "glob") (req "^0.2.10") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.1") (default-features #t) (kind 1)) (crate-dep (name "regex") (req "^0.1.8") (default-features #t) (kind 2)))) (hash "134vzpn67rlmzppzp6gq8z34gi7scvxhg2a462y2qxjmpn4jlgri")))

