(define-module (crates-io jr db) #:use-module (crates-io))

(define-public crate-jrdb-0.0.2 (crate (name "jrdb") (vers "0.0.2") (deps (list (crate-dep (name "byteorder") (req "^1.3.4") (default-features #t) (kind 0)))) (hash "1r4qwghjkkinx0jpqc77whdf0l8gn53j426ld7x7mdg5ixxjy65z")))

