(define-module (crates-io jr in) #:use-module (crates-io))

(define-public crate-jrinx-abi-0.1 (crate (name "jrinx-abi") (vers "0.1.0") (deps (list (crate-dep (name "cfg-if") (req "^1.0.0") (default-features #t) (kind 0)))) (hash "077wg4s6hlmbskqkpvvqicc4w6rj6n4fd8g808p3l5zyl662i898") (yanked #t)))

(define-public crate-jrinx-abi-0.1 (crate (name "jrinx-abi") (vers "0.1.1") (deps (list (crate-dep (name "cfg-if") (req "^1.0.0") (default-features #t) (kind 0)))) (hash "0ss90q9g19byv1cg2lfcrxji456sgm9p2zm220xq3z0yydc8iwxh")))

(define-public crate-jrinx-abi-0.1 (crate (name "jrinx-abi") (vers "0.1.2") (deps (list (crate-dep (name "cfg-if") (req "^1.0.0") (default-features #t) (kind 0)) (crate-dep (name "jrinx-apex") (req "^0.1.0") (optional #t) (default-features #t) (kind 0)))) (hash "1hrh0drpqgi10hipm63mkz7ng3rypg9d38dhvfwdyn8xrxpwfva7") (v 2) (features2 (quote (("sysfn" "dep:jrinx-apex"))))))

(define-public crate-jrinx-abi-0.1 (crate (name "jrinx-abi") (vers "0.1.3") (deps (list (crate-dep (name "cfg-if") (req "^1.0.0") (default-features #t) (kind 0)) (crate-dep (name "jrinx-apex") (req "^0.2.0") (optional #t) (default-features #t) (kind 0)))) (hash "0ipgqkc69jwa4x9j0p3dda6svaqqf7pyj4s6i58q8zijpb4fk7dr") (v 2) (features2 (quote (("sysfn" "dep:jrinx-apex"))))))

(define-public crate-jrinx-apex-0.1 (crate (name "jrinx-apex") (vers "0.1.0") (hash "00cgzdyw166gjfsavsy25ilwakgk5ykmmkgkvhjadpahz60wlgqa")))

(define-public crate-jrinx-apex-0.2 (crate (name "jrinx-apex") (vers "0.2.0") (hash "07s24wspg8z6dgjlfa1gv0q635b2abrw0lcjc409ac246z7snb5x")))

