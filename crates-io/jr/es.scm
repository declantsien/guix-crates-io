(define-module (crates-io jr es) #:use-module (crates-io))

(define-public crate-jrest-0.1 (crate (name "jrest") (vers "0.1.0") (deps (list (crate-dep (name "num") (req "^0.4.0") (default-features #t) (kind 0)))) (hash "182w1x45n8visgr5pq9bysqyzirbixry7g09lggh16zpbqhg1p2l") (rust-version "1.70.0")))

(define-public crate-jrest-0.1 (crate (name "jrest") (vers "0.1.1") (deps (list (crate-dep (name "num") (req "^0.4.0") (default-features #t) (kind 0)))) (hash "0ynivrj2wg7vfqghynsb111aszk4fj2c1465rhjv3m5q5wdl6ndf") (rust-version "1.70.0")))

(define-public crate-jrest-0.1 (crate (name "jrest") (vers "0.1.2") (deps (list (crate-dep (name "num") (req "^0.4.0") (default-features #t) (kind 0)))) (hash "1s9dnj4s7pz5gcrcqcgbwcs13n148x48nm66lgz301i9x0402s0v") (rust-version "1.70.0")))

(define-public crate-jrest-0.2 (crate (name "jrest") (vers "0.2.0") (deps (list (crate-dep (name "clap") (req "^4.3.10") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "jrest_hooks") (req "=0.2.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "num") (req "^0.4.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.29") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.100") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2.0.23") (features (quote ("extra-traits" "fold" "full"))) (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.29.1") (features (quote ("process" "full"))) (default-features #t) (kind 0)))) (hash "092qc126yqi6cg457ar6262rb0m4wir9v41prgr7aaz5g8ay9mjp") (yanked #t) (rust-version "1.70.0")))

(define-public crate-jrest-0.2 (crate (name "jrest") (vers "0.2.1") (deps (list (crate-dep (name "jrest_hooks") (req "=0.2.1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "num") (req "^0.4.0") (default-features #t) (kind 0)))) (hash "1sp3zwkzi4hmfv8zqdxvpmxn5874xv6xykp4qm5brizvwv4aa8wa") (rust-version "1.70.0")))

(define-public crate-jrest-0.2 (crate (name "jrest") (vers "0.2.2") (deps (list (crate-dep (name "jrest_hooks") (req "=0.2.2") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "num") (req "^0.4.0") (default-features #t) (kind 0)))) (hash "0n8q4vm86ywq6lv55g35lj86bgkvwjn0nma45z533kps7r2f8mlm") (rust-version "1.70.0")))

(define-public crate-jrest-0.2 (crate (name "jrest") (vers "0.2.3") (deps (list (crate-dep (name "jrest_hooks") (req "=0.2.3") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "num") (req "^0.4.0") (default-features #t) (kind 0)))) (hash "171j1jxi8b684f8z25pxw4lyjljk3k15prckmxdvl2sa9fmcq9vb") (rust-version "1.70.0")))

(define-public crate-jrest_hooks-0.2 (crate (name "jrest_hooks") (vers "0.2.0") (deps (list (crate-dep (name "quote") (req "^1.0.29") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2.0.23") (features (quote ("extra-traits" "fold" "full"))) (default-features #t) (kind 0)))) (hash "05ygn02cq8psl0pb93p1yis1xrkdg50fp93rnr29r4jcbfqflprk") (yanked #t) (rust-version "1.70.0")))

(define-public crate-jrest_hooks-0.2 (crate (name "jrest_hooks") (vers "0.2.1") (deps (list (crate-dep (name "quote") (req "^1.0.29") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2.0.23") (features (quote ("extra-traits" "fold" "full"))) (default-features #t) (kind 0)))) (hash "1cqvhcvldjhvwsz1lkpaqs13dcshfbv3n5mrsvrn6rhybzhvc506") (rust-version "1.70.0")))

(define-public crate-jrest_hooks-0.2 (crate (name "jrest_hooks") (vers "0.2.2") (deps (list (crate-dep (name "quote") (req "^1.0.29") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2.0.23") (features (quote ("extra-traits" "fold" "full"))) (default-features #t) (kind 0)))) (hash "1q2rk9fwkxzz9gyrb0yhjjln9bjd9zrvv4cc3b4ipj39j16k9kcr") (rust-version "1.70.0")))

(define-public crate-jrest_hooks-0.2 (crate (name "jrest_hooks") (vers "0.2.3") (deps (list (crate-dep (name "quote") (req "^1.0.29") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2.0.23") (features (quote ("extra-traits" "fold" "full"))) (default-features #t) (kind 0)))) (hash "0dznhlvnx3vgdxazvdx5pp1x1p81g8baa1nk595ahx3ypana7r0h") (rust-version "1.70.0")))

