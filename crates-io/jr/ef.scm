(define-module (crates-io jr ef) #:use-module (crates-io))

(define-public crate-jreflection-0.0.0 (crate (name "jreflection") (vers "0.0.0-git") (deps (list (crate-dep (name "bitflags") (req "^1.1.0") (default-features #t) (kind 0)) (crate-dep (name "bugsalot") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "jimage") (req "^0.2.2") (default-features #t) (kind 0)) (crate-dep (name "zip") (req "^0.5") (default-features #t) (kind 0)))) (hash "16nr534mcvs86s6g7mca1x76dri9478aiazysfrksvdd9cd5xv2q")))

(define-public crate-jreflection-0.0.11 (crate (name "jreflection") (vers "0.0.11") (deps (list (crate-dep (name "bitflags") (req "^1.1.0") (default-features #t) (kind 0)) (crate-dep (name "bugsalot") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "jimage") (req "^0.2.2") (default-features #t) (kind 0)) (crate-dep (name "zip") (req "^0.5") (default-features #t) (kind 0)))) (hash "1zll4m3a1zj05vmix48sx68i3gwa19cmr4h7kd7cv7rc1zq3vnq0")))

