(define-module (crates-io jr ep) #:use-module (crates-io))

(define-public crate-jrep-0.1 (crate (name "jrep") (vers "0.1.3") (deps (list (crate-dep (name "atty") (req "~0.2.0") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "~2.33.0") (features (quote ("yaml"))) (default-features #t) (kind 0)) (crate-dep (name "exitcode") (req "~1.1.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.123") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.62") (default-features #t) (kind 0)) (crate-dep (name "term") (req "^0.7.0") (default-features #t) (kind 0)))) (hash "1x0izwg5909l647jy32qa2lhmkk0irip0cwqj862jcqa0q2v85xh")))

