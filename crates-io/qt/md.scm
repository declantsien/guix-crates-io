(define-module (crates-io qt md) #:use-module (crates-io))

(define-public crate-qtmd-0.1 (crate (name "qtmd") (vers "0.1.0") (deps (list (crate-dep (name "tqdm") (req "^0.6.0") (default-features #t) (kind 0)))) (hash "1f9g0sj1zz8if1xjwbdbzr0f005d4dv13flhq5c4dk8ci0kc1wic")))

(define-public crate-qtmd-0.1 (crate (name "qtmd") (vers "0.1.1") (deps (list (crate-dep (name "tqdm") (req "^0.6.0") (default-features #t) (kind 0)))) (hash "0pzana05gd6hplaxf8l2lrzbfwdljkil48mlcmbj5dmzm1k4vk0l")))

