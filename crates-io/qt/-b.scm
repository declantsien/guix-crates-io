(define-module (crates-io qt -b) #:use-module (crates-io))

(define-public crate-qt-build-utils-0.4 (crate (name "qt-build-utils") (vers "0.4.0") (deps (list (crate-dep (name "thiserror") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "versions") (req "^4.1.0") (default-features #t) (kind 0)))) (hash "0jjfw42pslznlznk6fih632zs632b64j6mx7wbqi6qq69fjb710j")))

(define-public crate-qt-build-utils-0.4 (crate (name "qt-build-utils") (vers "0.4.1") (deps (list (crate-dep (name "thiserror") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "versions") (req "^4.1.0") (default-features #t) (kind 0)))) (hash "1sr1isk0xksk33dymdpms07dn4lfl9mhy3mrry2l9f5f8718n1a4")))

(define-public crate-qt-build-utils-0.5 (crate (name "qt-build-utils") (vers "0.5.0") (deps (list (crate-dep (name "thiserror") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "versions") (req "^4.1.0") (default-features #t) (kind 0)))) (hash "04rdv59m71955rkn4d9wjl2bpi83k9h03k8j7bcg2pk6ky8fnwwv")))

(define-public crate-qt-build-utils-0.5 (crate (name "qt-build-utils") (vers "0.5.1") (deps (list (crate-dep (name "thiserror") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "versions") (req "^4.1.0") (default-features #t) (kind 0)))) (hash "0qiwp89hdhmyvhrb5j4g7sssgrggg0nqmjg7hkf43y7ssvs5y232")))

(define-public crate-qt-build-utils-0.5 (crate (name "qt-build-utils") (vers "0.5.2") (deps (list (crate-dep (name "thiserror") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "versions") (req "^4.1.0") (default-features #t) (kind 0)))) (hash "18ladbr0sx6h8icj7aahq02x6q9lfsv0q3069nrghvrfja62nhkm")))

(define-public crate-qt-build-utils-0.5 (crate (name "qt-build-utils") (vers "0.5.3") (deps (list (crate-dep (name "thiserror") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "versions") (req "^4.1.0") (default-features #t) (kind 0)))) (hash "1np1rfam6qh1gp0lqp835xcpk2p83f1mq9xrddlqg6l10mi81knm")))

(define-public crate-qt-build-utils-0.6 (crate (name "qt-build-utils") (vers "0.6.0") (deps (list (crate-dep (name "cc") (req "^1.0.79") (features (quote ("parallel"))) (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "versions") (req "^5.0.1") (default-features #t) (kind 0)))) (hash "1cbb8rlnv9akzc14ydpp51cxvfjzmy8j7jzp6ynp9xyjim5fzwzq") (features (quote (("link_qt_object_files"))))))

(define-public crate-qt-build-utils-0.6 (crate (name "qt-build-utils") (vers "0.6.1") (deps (list (crate-dep (name "cc") (req "^1.0.89") (features (quote ("parallel"))) (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "versions") (req "^5.0.1") (default-features #t) (kind 0)))) (hash "041gmbk9j9n41npmp9cjmlfvc2bm0fxa8c0cvlsaska3wa7q576m") (features (quote (("link_qt_object_files"))))))

