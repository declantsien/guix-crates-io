(define-module (crates-io qt _m) #:use-module (crates-io))

(define-public crate-qt_macros-0.1 (crate (name "qt_macros") (vers "0.1.0-alpha.1") (deps (list (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "152skryfcc75v5mlbqfdsripskjfqzc82zil32fvzvlrnxxqzzlg")))

(define-public crate-qt_macros-0.1 (crate (name "qt_macros") (vers "0.1.0-alpha.2") (deps (list (crate-dep (name "proc-macro-hack") (req "^0.5.11") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "0n4p8xzs6q7gma07z312jdyqh766rrxakhlf4y4z0na0qkwr00qb")))

(define-public crate-qt_macros-0.1 (crate (name "qt_macros") (vers "0.1.0") (deps (list (crate-dep (name "proc-macro-hack") (req "^0.5.11") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "0wda75gc5gsjdpy3hgbf2vrh44lg5qifvngq6zlm3md9nvlbf93d")))

(define-public crate-qt_macros-0.1 (crate (name "qt_macros") (vers "0.1.1") (deps (list (crate-dep (name "proc-macro-hack") (req "^0.5.11") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "1qxa5vmbppcvf0ac1s74a1mlnc00cnf4ksgg650dy95njphzrdj2")))

