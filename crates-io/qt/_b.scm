(define-module (crates-io qt _b) #:use-module (crates-io))

(define-public crate-qt_build_tools-0.0.0 (crate (name "qt_build_tools") (vers "0.0.0") (deps (list (crate-dep (name "cpp_to_rust") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^0.1") (default-features #t) (kind 0)))) (hash "00qjf47iw97hzxq5kgfq4d5h6gng351ml50b9g9l9zfn7a40rf20")))

(define-public crate-qt_build_tools-0.0.1 (crate (name "qt_build_tools") (vers "0.0.1") (deps (list (crate-dep (name "cpp_to_rust") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^0.1") (default-features #t) (kind 0)))) (hash "0il0y1p4gwhwk04ddj5l6yw1z5kvqx2r5rqbhim73p116x5xv1x5")))

(define-public crate-qt_build_tools-0.1 (crate (name "qt_build_tools") (vers "0.1.0") (deps (list (crate-dep (name "cpp_to_rust") (req "^0.5") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^0.1") (default-features #t) (kind 0)))) (hash "0iwx0knf0yh74yj8bxa118018vhwrkm3sl3pmn7ywxr65bvq6hvh")))

(define-public crate-qt_build_tools-0.1 (crate (name "qt_build_tools") (vers "0.1.1") (deps (list (crate-dep (name "compress") (req "^0.1.2") (default-features #t) (kind 0)) (crate-dep (name "cpp_to_rust") (req "^0.5") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "rusqlite") (req "^0.7") (default-features #t) (kind 0)) (crate-dep (name "select") (req "^0.3.0") (default-features #t) (kind 0)))) (hash "1yrn2ipfjgq3ak0m5800lryi7dy680raxnvbw2pmgr0w9baf31q3")))

(define-public crate-qt_build_tools-0.1 (crate (name "qt_build_tools") (vers "0.1.2") (deps (list (crate-dep (name "compress") (req "^0.1.2") (default-features #t) (kind 0)) (crate-dep (name "cpp_to_rust") (req "^0.5") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "rusqlite") (req "^0.7") (default-features #t) (kind 0)) (crate-dep (name "select") (req "^0.3.0") (default-features #t) (kind 0)))) (hash "19qcrxsdz15hcx83inz638hc45hgzfr6wlgmhah3yj1ms1ddkh4v")))

(define-public crate-qt_build_tools-0.1 (crate (name "qt_build_tools") (vers "0.1.3") (deps (list (crate-dep (name "compress") (req "^0.1.2") (default-features #t) (kind 0)) (crate-dep (name "cpp_to_rust") (req "^0.5") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "rusqlite") (req "^0.7") (default-features #t) (kind 0)) (crate-dep (name "select") (req "^0.3.0") (default-features #t) (kind 0)))) (hash "0a1c14n9ajhh6zfznk912wgqyvzkajz0q617h5wjrb5b0gb6srqm")))

(define-public crate-qt_build_tools-0.2 (crate (name "qt_build_tools") (vers "0.2.0") (deps (list (crate-dep (name "cpp_to_rust_build_tools") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "qt_generator_common") (req "^0.2.0") (default-features #t) (kind 0)))) (hash "02pr9ban1h1vjl25kym6az3pd3zxvdkihvbjhh2ba8r2a5cbmc5j")))

(define-public crate-qt_build_tools-0.2 (crate (name "qt_build_tools") (vers "0.2.1") (deps (list (crate-dep (name "cpp_to_rust_build_tools") (req "^0.2.1") (default-features #t) (kind 0)) (crate-dep (name "qt_generator_common") (req "^0.2.0") (default-features #t) (kind 0)))) (hash "0zlri4gx73a7f5xjd6lh6y6w9sgjnbsxspv4n0y3xwjkbpss97w4")))

(define-public crate-qt_build_tools-0.2 (crate (name "qt_build_tools") (vers "0.2.3") (deps (list (crate-dep (name "cpp_to_rust_build_tools") (req "^0.2.3") (default-features #t) (kind 0)) (crate-dep (name "qt_generator_common") (req "^0.2.3") (default-features #t) (kind 0)))) (hash "1nz2nrvv689s2aqb8chpnkcn8fgpa992hfa7s230yjgr5894hpxy")))

(define-public crate-qt_build_tools-0.2 (crate (name "qt_build_tools") (vers "0.2.4") (deps (list (crate-dep (name "cpp_to_rust_build_tools") (req "^0.2.3") (default-features #t) (kind 0)) (crate-dep (name "qt_generator_common") (req "^0.2.3") (default-features #t) (kind 0)))) (hash "1zdibdn7wk030pgap4flacjd1pmv3cv0cmck5j49vz0g9z9kbmwv")))

