(define-module (crates-io qt ru) #:use-module (crates-io))

(define-public crate-qtruss-0.1 (crate (name "qtruss") (vers "0.1.0") (deps (list (crate-dep (name "maria-linalg") (req "^0.4") (default-features #t) (kind 0)))) (hash "1csdnv6hacqzlc7dnz0zpv4mx4hsc5virq8kaxljkc92rx62fn62")))

(define-public crate-qtruss-0.2 (crate (name "qtruss") (vers "0.2.0") (deps (list (crate-dep (name "maria-linalg") (req "^0.4") (default-features #t) (kind 0)))) (hash "1i6q2gq2qgxqqsdf53b225xp94xn826jr6z7y3syypic59c40zz6")))

(define-public crate-qtruss-0.2 (crate (name "qtruss") (vers "0.2.1") (deps (list (crate-dep (name "maria-linalg") (req "^0.4") (default-features #t) (kind 0)))) (hash "1iw9vab3hv6frn8g9smzc5qvvdj73sdsp24gvfbind8y5qsnx9kq")))

(define-public crate-qtruss-0.2 (crate (name "qtruss") (vers "0.2.2") (deps (list (crate-dep (name "maria-linalg") (req "^0.5") (default-features #t) (kind 0)))) (hash "0gl0axzyn93d56hm00rf748pxm68mrcmr6l5k76b98rxi0h9d21x")))

(define-public crate-qtruss-0.3 (crate (name "qtruss") (vers "0.3.0") (deps (list (crate-dep (name "maria-linalg") (req "^0.6") (default-features #t) (kind 0)))) (hash "1k4198y4nfr85zw3zhpsivwdlm5p7b39j4rm95w1sfxx55jpdp5r")))

(define-public crate-qtruss-0.3 (crate (name "qtruss") (vers "0.3.1") (deps (list (crate-dep (name "maria-linalg") (req "^0.7") (default-features #t) (kind 0)))) (hash "0x9pxqzm290w1v8827kiyfjj185pdp1x8zsal8rxw91q94c1pbpy")))

(define-public crate-qtruss-0.4 (crate (name "qtruss") (vers "0.4.0") (deps (list (crate-dep (name "maria-linalg") (req "^0.7") (default-features #t) (kind 0)))) (hash "0bdv4i3r9saqvdym82a6vh4x03c7almxp8wvy27y3wa0pw7aincb")))

(define-public crate-qtruss-0.5 (crate (name "qtruss") (vers "0.5.0") (deps (list (crate-dep (name "maria-linalg") (req "^0.7") (default-features #t) (kind 0)))) (hash "0nd52rrh2ncj2ak7d7rx3ya42613ahvadvhp3c7gsv2h0bc741bv")))

(define-public crate-qtruss-0.6 (crate (name "qtruss") (vers "0.6.0") (deps (list (crate-dep (name "maria-linalg") (req "^0.7") (default-features #t) (kind 0)))) (hash "077jh3vf978pishq0hgkyli9q7szm8wqinv89zq5bw9xprr9zb5k")))

(define-public crate-qtruss-0.7 (crate (name "qtruss") (vers "0.7.0") (deps (list (crate-dep (name "maria-linalg") (req "^0.7") (default-features #t) (kind 0)))) (hash "1halg1p63zf53g76daarkgdczv93vclqjs59klsdvssxw6z2whqm")))

(define-public crate-qtruss-0.7 (crate (name "qtruss") (vers "0.7.1") (deps (list (crate-dep (name "maria-linalg") (req "^0.8") (default-features #t) (kind 0)))) (hash "0x70l33cw10l143z7rwxcridk128dpn3izam08mv6bq69475gfkc")))

(define-public crate-qtruss-0.7 (crate (name "qtruss") (vers "0.7.2") (deps (list (crate-dep (name "maria-linalg") (req "^0.8") (default-features #t) (kind 0)))) (hash "1az1ks1dj859x8vh181slc4awzyydrhc6vc7b5q21gjkri4smbbi")))

(define-public crate-qtruss-0.8 (crate (name "qtruss") (vers "0.8.0") (deps (list (crate-dep (name "maria-linalg") (req "^0.8") (default-features #t) (kind 0)))) (hash "0dbvwal3hq1zx9wk7l0k3wv1iih46w16j4ca230mzz4scb7m6fq6")))

(define-public crate-qtruss-0.8 (crate (name "qtruss") (vers "0.8.1") (deps (list (crate-dep (name "maria-linalg") (req "^0.8") (default-features #t) (kind 0)))) (hash "10s02zn1ia6hncab44rwby901p7rwbcs1rj36v2iz10srla5ykqz")))

(define-public crate-qtruss-0.9 (crate (name "qtruss") (vers "0.9.0") (deps (list (crate-dep (name "maria-linalg") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "raqote") (req "^0.8.3") (features (quote ("png"))) (kind 0)))) (hash "1dwzy5qy11qqggi44df5id9v53wg41bl6dnykk9pqrqxd4nqgmnv")))

(define-public crate-qtruss-0.9 (crate (name "qtruss") (vers "0.9.1") (deps (list (crate-dep (name "maria-linalg") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "raqote") (req "^0.8.3") (features (quote ("png"))) (kind 0)))) (hash "0nhxywp4sryqmpawd5k5cwvsmz3rcxa3qil7pb9v3gj9idzgnb5z")))

(define-public crate-qtruss-0.9 (crate (name "qtruss") (vers "0.9.2") (deps (list (crate-dep (name "maria-linalg") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "raqote") (req "^0.8.3") (features (quote ("png"))) (kind 0)))) (hash "1dvl37fvz6izbh2fsc68mnf7xmr40wrlclikj7lvz5n23gcd6b26")))

(define-public crate-qtruss-0.9 (crate (name "qtruss") (vers "0.9.3") (deps (list (crate-dep (name "maria-linalg") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "raqote") (req "^0.8.3") (features (quote ("png"))) (kind 0)))) (hash "180qpa1hrh6b4m80g6c9vfyw460ixfvfx1d85b4s34baa9lbli29")))

(define-public crate-qtruss-0.9 (crate (name "qtruss") (vers "0.9.4") (deps (list (crate-dep (name "maria-linalg") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "raqote") (req "^0.8.3") (features (quote ("png"))) (kind 0)))) (hash "1xj8rjig9y2acb93srg107kmr4yjz5pjqwmn1i8ixmsnc5aa268h")))

(define-public crate-qtruss-0.9 (crate (name "qtruss") (vers "0.9.5") (deps (list (crate-dep (name "maria-linalg") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "raqote") (req "^0.8.3") (features (quote ("png"))) (kind 0)))) (hash "00hbi37rff6rhrswqqk3sz8qqh5yvzyh1xicmd0wwdkibdjqvac9")))

(define-public crate-qtruss-0.9 (crate (name "qtruss") (vers "0.9.6") (deps (list (crate-dep (name "maria-linalg") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "raqote") (req "^0.8.3") (features (quote ("png"))) (kind 0)))) (hash "01b6szjsl2zxpp0rzb6kzgp1qzacbdx298w69ib3v5czdg26j8by")))

(define-public crate-qtruss-0.10 (crate (name "qtruss") (vers "0.10.0") (deps (list (crate-dep (name "maria-linalg") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "raqote") (req "^0.8.3") (features (quote ("png"))) (kind 0)))) (hash "08ds55l5davx8sqgdx92s83dwwb9gb3zykwy08vsd0j2za08n3q8")))

(define-public crate-qtruss-0.10 (crate (name "qtruss") (vers "0.10.1") (deps (list (crate-dep (name "maria-linalg") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "raqote") (req "^0.8.3") (features (quote ("png"))) (kind 0)))) (hash "1l43fkhwpa7582hjc26ij9fc5f4206vlkn2havydnpcakwrbbk9l")))

(define-public crate-qtruss-0.10 (crate (name "qtruss") (vers "0.10.2") (deps (list (crate-dep (name "maria-linalg") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "raqote") (req "^0.8.3") (features (quote ("png"))) (kind 0)))) (hash "1gzwbzg2ablcwiiazaf167c71lnrmlrkn1nrc696w2fk0rlv1qdp")))

(define-public crate-qtruss-0.10 (crate (name "qtruss") (vers "0.10.3") (deps (list (crate-dep (name "maria-linalg") (req "^0.9") (default-features #t) (kind 0)) (crate-dep (name "raqote") (req "^0.8.3") (features (quote ("png"))) (kind 0)))) (hash "07z47g1qj7939bcfv1grnhrfxwb1l8kn74879akwj55w90b012k5")))

(define-public crate-qtruss-0.10 (crate (name "qtruss") (vers "0.10.4") (deps (list (crate-dep (name "maria-linalg") (req "^0.10") (default-features #t) (kind 0)) (crate-dep (name "raqote") (req "^0.8.3") (features (quote ("png"))) (kind 0)))) (hash "13844hpqxbmg2all2h8zxkczpixk8kx3ms53r4nmj5j9jlya9c1i")))

(define-public crate-qtruss-0.11 (crate (name "qtruss") (vers "0.11.0") (deps (list (crate-dep (name "maria-linalg") (req "^0.10") (default-features #t) (kind 0)) (crate-dep (name "raqote") (req "^0.8.3") (features (quote ("png"))) (kind 0)))) (hash "0cpks4ip7v4lyq6cqka78vxzh3kismm2kwk5aizbc9fra8wsb4kr")))

(define-public crate-qtruss-0.12 (crate (name "qtruss") (vers "0.12.0") (deps (list (crate-dep (name "maria-linalg") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "pgfplots") (req "^0.5.1") (default-features #t) (kind 0)) (crate-dep (name "raqote") (req "^0.8.3") (features (quote ("png"))) (kind 0)))) (hash "0mvvnsx9wcm7zck9i8gm2xh6k80z3sybw3m2ikd7nzaafx2vnrmx")))

(define-public crate-qtruss-0.13 (crate (name "qtruss") (vers "0.13.0") (deps (list (crate-dep (name "maria-linalg") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "pgfplots") (req "^0.5.1") (default-features #t) (kind 0)) (crate-dep (name "raqote") (req "^0.8.3") (features (quote ("png"))) (kind 0)))) (hash "0ppgqs9ysv2x1nmdhzh36p42fs74mdpvrp4z64m3a79czch50hb1")))

