(define-module (crates-io qt ra) #:use-module (crates-io))

(define-public crate-qtrace-0.1 (crate (name "qtrace") (vers "0.1.0") (deps (list (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.10.4") (default-features #t) (kind 0)))) (hash "0pd2hidhwww384mvhc1lsb92xsfgyr6bxpji6hxlwfxjh52xsdxi")))

(define-public crate-qtranslation-0.1 (crate (name "qtranslation") (vers "0.1.0") (deps (list (crate-dep (name "qmetaobject") (req "^0.2.7") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.85") (default-features #t) (kind 0)) (crate-dep (name "sys-locale") (req "^0.2.1") (default-features #t) (kind 0)))) (hash "0s7d2w2clq3k4j08c75qq9bylhj0mfz4n3873agl57r1gaji00sp") (yanked #t)))

(define-public crate-qtranslation-0.1 (crate (name "qtranslation") (vers "0.1.1") (deps (list (crate-dep (name "qmetaobject") (req "^0.2.7") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.85") (default-features #t) (kind 0)) (crate-dep (name "sys-locale") (req "^0.2.1") (default-features #t) (kind 0)))) (hash "175yi9bwj4h4c071q15xw5gay41dgwa96hznkz2ff6p2p7rn3nj0") (yanked #t)))

(define-public crate-qtranslation-0.1 (crate (name "qtranslation") (vers "0.1.11") (deps (list (crate-dep (name "qmetaobject") (req "^0.2.7") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.85") (default-features #t) (kind 0)) (crate-dep (name "sys-locale") (req "^0.2.1") (default-features #t) (kind 0)))) (hash "063cwdhiyph0ixcpr1q1zb6c8p6zfzrg70rg55095jbsdn73lk8i") (yanked #t)))

(define-public crate-qtranslation-0.1 (crate (name "qtranslation") (vers "0.1.2") (deps (list (crate-dep (name "qmetaobject") (req "^0.2.7") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.85") (default-features #t) (kind 0)) (crate-dep (name "sys-locale") (req "^0.2.1") (default-features #t) (kind 0)))) (hash "1wcqgns469kznb1v0xnssmi3wk711wflq974b6s9crh3akmi2zni")))

(define-public crate-qtranslation-0.1 (crate (name "qtranslation") (vers "0.1.3") (deps (list (crate-dep (name "qmetaobject") (req "^0.2.7") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.85") (default-features #t) (kind 0)) (crate-dep (name "sys-locale") (req "^0.2.1") (default-features #t) (kind 0)))) (hash "10ksln1p3xfwvna6lax2wvqslp1w5pdx1pjppqvs4992nkxc597v")))

(define-public crate-qtranslation-0.1 (crate (name "qtranslation") (vers "0.1.4") (deps (list (crate-dep (name "qmetaobject") (req "^0.2.7") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.85") (default-features #t) (kind 0)) (crate-dep (name "sys-locale") (req "^0.2.1") (default-features #t) (kind 0)))) (hash "14icqm6ahi2zcvkz9m51chrzyhb6yp1p9hwxd8p0bvvrml6apqr6") (yanked #t)))

(define-public crate-qtranslation-0.1 (crate (name "qtranslation") (vers "0.1.5") (deps (list (crate-dep (name "qmetaobject") (req "^0.2.7") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.85") (default-features #t) (kind 0)) (crate-dep (name "sys-locale") (req "^0.2.1") (default-features #t) (kind 0)))) (hash "1lagr1sqgc7a27djsilzg06iscpmddlk35ln5icfzlncfcqqmwib")))

(define-public crate-qtranslation-0.1 (crate (name "qtranslation") (vers "0.1.6") (deps (list (crate-dep (name "qmetaobject") (req "^0.2.7") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.85") (default-features #t) (kind 0)) (crate-dep (name "sys-locale") (req "^0.2.1") (default-features #t) (kind 0)))) (hash "0adsc4z3pazn1nhbcwmilcg67fcll58320bir1kzhyfd69v7civ0")))

(define-public crate-qtranslation-0.1 (crate (name "qtranslation") (vers "0.1.7") (deps (list (crate-dep (name "qmetaobject") (req "^0.2.7") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.85") (default-features #t) (kind 0)) (crate-dep (name "sys-locale") (req "^0.2.1") (default-features #t) (kind 0)))) (hash "0sfqgyk9h8y32f2xbbp2irlqs680pgfv4znw47r5s4xf36j2rghk")))

