(define-module (crates-io g_ #{2_}#) #:use-module (crates-io))

(define-public crate-g_2_0_0-0.1 (crate (name "g_2_0_0") (vers "0.1.0") (deps (list (crate-dep (name "num-traits") (req "^0.2.16") (default-features #t) (kind 0)))) (hash "1m2p88ci5h00kgf8jg3c0k4alq5psc6cai3904hfpsivzay3a8qa")))

(define-public crate-g_2_0_0-0.1 (crate (name "g_2_0_0") (vers "0.1.1") (deps (list (crate-dep (name "num-traits") (req "^0.2.16") (default-features #t) (kind 0)))) (hash "0377cb1jrnm691f70hvdl3yajk3zkbr5ahdhishspckpsj9avhlk")))

(define-public crate-g_2_0_0-0.1 (crate (name "g_2_0_0") (vers "0.1.2") (deps (list (crate-dep (name "num-traits") (req "^0.2.16") (default-features #t) (kind 0)))) (hash "05fy851s0xykns69pmgjxj2xfpr4vwicl5w2lykhg8l6y5rssnyc")))

(define-public crate-g_2_0_0-0.1 (crate (name "g_2_0_0") (vers "0.1.3") (deps (list (crate-dep (name "num-traits") (req "^0.2.16") (default-features #t) (kind 0)))) (hash "0yvgnqybwh4sfn1fkhqdalm5pqjmy2nnn2scy6casggjg7ax9a9z")))

(define-public crate-g_2_0_0-0.1 (crate (name "g_2_0_0") (vers "0.1.4") (deps (list (crate-dep (name "num-traits") (req "^0.2.16") (default-features #t) (kind 0)))) (hash "19mqhrkbdibkbpnv1c9n0k0i9xbg7psgzs83smvpb5zprfx3524x")))

(define-public crate-g_2_0_0-0.2 (crate (name "g_2_0_0") (vers "0.2.0") (deps (list (crate-dep (name "num-traits") (req "^0.2.16") (default-features #t) (kind 0)) (crate-dep (name "radians") (req "^0.3.1") (default-features #t) (kind 0)))) (hash "0kdb644v2warikfc5q0nilqbh3s54c1cj04wx5b6wfqflihqawdr")))

(define-public crate-g_2_0_0-0.2 (crate (name "g_2_0_0") (vers "0.2.1") (deps (list (crate-dep (name "num-traits") (req "^0.2.16") (default-features #t) (kind 0)) (crate-dep (name "radians") (req "^0.3.1") (default-features #t) (kind 0)))) (hash "0wz4ab7kva1c1l06dlvba6cjcikrdhsby0l6qnwhzppaln5i5i6x")))

(define-public crate-g_2_0_0-0.2 (crate (name "g_2_0_0") (vers "0.2.2") (deps (list (crate-dep (name "num-traits") (req "^0.2.16") (default-features #t) (kind 0)) (crate-dep (name "radians") (req "^0.3.1") (default-features #t) (kind 0)))) (hash "1zghp71rd98bmcdirwna79l41py2ybr10x34zzph56zsyql59fjs")))

(define-public crate-g_2_0_0-0.2 (crate (name "g_2_0_0") (vers "0.2.3") (deps (list (crate-dep (name "num-traits") (req "^0.2.16") (default-features #t) (kind 0)) (crate-dep (name "radians") (req "^0.3.1") (default-features #t) (kind 0)))) (hash "0fazdy8fgawnbpf86is78a13n9ai6ii2q00idmk5zwxwhnbwm7sr")))

(define-public crate-g_2_0_0-0.3 (crate (name "g_2_0_0") (vers "0.3.0") (deps (list (crate-dep (name "num-traits") (req "^0.2.16") (default-features #t) (kind 0)) (crate-dep (name "radians") (req "^0.3.1") (default-features #t) (kind 0)))) (hash "0raicazr7iii8vprsckr1sxjp5dl25jaq949lsif75n35j33q06i")))

(define-public crate-g_2_0_0-0.3 (crate (name "g_2_0_0") (vers "0.3.1") (deps (list (crate-dep (name "num-traits") (req "^0.2.16") (default-features #t) (kind 0)) (crate-dep (name "radians") (req "^0.3.1") (default-features #t) (kind 0)))) (hash "06wd7n9jg0i143nwp4ns9rafik5z7b8jwnn42xam1adhx3374py9")))

(define-public crate-g_2_0_0-0.3 (crate (name "g_2_0_0") (vers "0.3.2") (deps (list (crate-dep (name "num-traits") (req "^0.2.16") (default-features #t) (kind 0)) (crate-dep (name "radians") (req "^0.3.1") (default-features #t) (kind 0)))) (hash "06dg78s558baq9qza8m1lm86ynbqkzixqz5762r2a33sz99m6qzd")))

(define-public crate-g_2_0_0-0.3 (crate (name "g_2_0_0") (vers "0.3.3") (deps (list (crate-dep (name "num-traits") (req "^0.2.16") (default-features #t) (kind 0)) (crate-dep (name "radians") (req "^0.3.1") (default-features #t) (kind 0)))) (hash "0j8pd7c0pjbqlpcps0g02is1zmzvyipjx18lv7sgigglwrlr90id")))

(define-public crate-g_2_0_0-0.4 (crate (name "g_2_0_0") (vers "0.4.0") (deps (list (crate-dep (name "num-traits") (req "^0.2.16") (default-features #t) (kind 0)) (crate-dep (name "radians") (req "^0.3.1") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)))) (hash "0gn5as35bxzh1bl98q290l7allg028spv2g50h5qc4521k26pk5q")))

(define-public crate-g_2_0_0-0.4 (crate (name "g_2_0_0") (vers "0.4.1") (deps (list (crate-dep (name "num-traits") (req "^0.2.16") (default-features #t) (kind 0)) (crate-dep (name "radians") (req "^0.3.1") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 2)))) (hash "0qwygj3vn7pviv1j89a2mhkfgx7xbbhddlxhjch72hvjz19s4q67")))

(define-public crate-g_2_0_0-0.4 (crate (name "g_2_0_0") (vers "0.4.2") (deps (list (crate-dep (name "num-traits") (req "^0.2.16") (default-features #t) (kind 0)) (crate-dep (name "radians") (req "^0.3.1") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 2)))) (hash "0y4q5fav1bgafs8xxgdfazwr3b1vs2pmin1aghim1mdjzd184hm9")))

(define-public crate-g_2_0_0-0.4 (crate (name "g_2_0_0") (vers "0.4.3") (deps (list (crate-dep (name "num-traits") (req "^0.2.16") (default-features #t) (kind 0)) (crate-dep (name "radians") (req "^0.3.1") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 2)))) (hash "02ar8yypkgjkipyjmrjyp8w65h5nmbxihfbdjhigkal9b78v88cg")))

(define-public crate-g_2_0_0-0.4 (crate (name "g_2_0_0") (vers "0.4.4") (deps (list (crate-dep (name "num-traits") (req "^0.2.16") (default-features #t) (kind 0)) (crate-dep (name "radians") (req "^0.3.1") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 2)))) (hash "01bq44jchbclx2zg7qhqiw8800q395zjjxg95s7nkd88wc9zkd6w")))

(define-public crate-g_2_0_0-0.4 (crate (name "g_2_0_0") (vers "0.4.5") (deps (list (crate-dep (name "num-traits") (req "^0.2.16") (default-features #t) (kind 0)) (crate-dep (name "radians") (req "^0.3.1") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 2)))) (hash "04sfjdz7h9amdkfplvpa4r262hck7ifgl4yc6xcjyr8qmky49ixf")))

(define-public crate-g_2_0_0-0.4 (crate (name "g_2_0_0") (vers "0.4.6") (deps (list (crate-dep (name "num-traits") (req "^0.2.16") (default-features #t) (kind 0)) (crate-dep (name "radians") (req "^0.3.1") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 2)))) (hash "0s1y69mdxgwss75plbwx0279xj9ww0936c2ww0x736mn68wvb1a9")))

(define-public crate-g_2_0_0-0.4 (crate (name "g_2_0_0") (vers "0.4.7") (deps (list (crate-dep (name "num-traits") (req "^0.2.16") (default-features #t) (kind 0)) (crate-dep (name "radians") (req "^0.3.1") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 2)))) (hash "11kcr4sf1irgmv2pna26gjzzn1f92722qylq97b67l2wqi7gmg33")))

(define-public crate-g_2_0_0-0.4 (crate (name "g_2_0_0") (vers "0.4.8") (deps (list (crate-dep (name "num-traits") (req "^0.2.16") (default-features #t) (kind 0)) (crate-dep (name "radians") (req "^0.3.1") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 2)))) (hash "0f5gaw14bgdnadj5bmh2lyi707yd4fnq6sq9ms629a075153hxxp")))

