(define-module (crates-io b2 hi) #:use-module (crates-io))

(define-public crate-b2histogram-1 (crate (name "b2histogram") (vers "1.0.0") (hash "0dwi1ifvlkcfkrwbzyk84rngqxfcf2k8l9jh76hsyq8py4d45hsb") (features (quote (("default"))))))

(define-public crate-b2histogram-1 (crate (name "b2histogram") (vers "1.0.1") (hash "0iak3326nmsy1dzm4c5hdk20gy5drrr6kk2fk42ivkn0zi9zbrid") (features (quote (("default"))))))

