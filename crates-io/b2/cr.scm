(define-module (crates-io b2 cr) #:use-module (crates-io))

(define-public crate-b2creds-0.1 (crate (name "b2creds") (vers "0.1.0") (deps (list (crate-dep (name "directories") (req "^3.0") (default-features #t) (kind 0)) (crate-dep (name "rusqlite") (req "^0.24.2") (features (quote ("bundled"))) (default-features #t) (kind 0)) (crate-dep (name "tempfile") (req "^3.2") (default-features #t) (kind 2)) (crate-dep (name "thiserror") (req "^1.0") (default-features #t) (kind 0)))) (hash "050whf28vzdj31dd1azd3f0lqkjv99fyslh545h3d0f508k12z4g")))

(define-public crate-b2creds-0.2 (crate (name "b2creds") (vers "0.2.0") (deps (list (crate-dep (name "directories") (req "^5.0.1") (default-features #t) (kind 0)) (crate-dep (name "rusqlite") (req "^0.29.0") (features (quote ("bundled"))) (default-features #t) (kind 0)) (crate-dep (name "tempfile") (req "^3.2") (default-features #t) (kind 2)) (crate-dep (name "thiserror") (req "^1.0") (default-features #t) (kind 0)))) (hash "1519cjlvwii3bzd5a3zppzagp3l3kqfv3cymiiqfjmrilnyn6d9g")))

