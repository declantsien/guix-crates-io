(define-module (crates-io b2 su) #:use-module (crates-io))

(define-public crate-b2sum-0.1 (crate (name "b2sum") (vers "0.1.0") (deps (list (crate-dep (name "docopt") (req "^0.7.0") (default-features #t) (kind 0)) (crate-dep (name "error-chain") (req "^0.9.0") (default-features #t) (kind 0)) (crate-dep (name "rust-crypto") (req "^0.2.36") (default-features #t) (kind 0)) (crate-dep (name "rustc-serialize") (req "^0.3.22") (default-features #t) (kind 0)))) (hash "0igxsrmh22kwmy6yfk05sdjx5jmsr7zpjxznwlrc5x4xqrwafffp")))

(define-public crate-b2sum-0.2 (crate (name "b2sum") (vers "0.2.0") (deps (list (crate-dep (name "blake2-rfc") (req "^0.2.17") (default-features #t) (kind 0)) (crate-dep (name "docopt") (req "^0.7.0") (default-features #t) (kind 0)) (crate-dep (name "error-chain") (req "^0.9.0") (default-features #t) (kind 0)) (crate-dep (name "rustc-serialize") (req "^0.3.22") (default-features #t) (kind 0)))) (hash "0xy4dkhl3b13sj9nzz9v56q01drjw2qzl3jrlb7b73zhwwld62sj")))

(define-public crate-b2sum-0.3 (crate (name "b2sum") (vers "0.3.0") (deps (list (crate-dep (name "blake2b_simd") (req "^0.5.2") (default-features #t) (kind 0)) (crate-dep (name "docopt") (req "^1.1.0") (default-features #t) (kind 0)) (crate-dep (name "error-chain") (req "^0.12.1") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.92") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "141aivsv3rc916in5ya3dimpxqa7v6mjwbgsq1mj231dqdwfrgwb")))

(define-public crate-b2sum-0.4 (crate (name "b2sum") (vers "0.4.0") (deps (list (crate-dep (name "blake2b_simd") (req "^0.5.11") (default-features #t) (kind 0)) (crate-dep (name "docopt") (req "^1.1.1") (default-features #t) (kind 0)) (crate-dep (name "eyre") (req "^0.6.5") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.125") (default-features #t) (kind 0)) (crate-dep (name "stable-eyre") (req "^0.2.2") (default-features #t) (kind 0)))) (hash "0sgl70k2bwwmqz3pf7m97k21f6szk305nxa918krrp4xwkcisg8c")))

(define-public crate-b2sum-rs-0.1 (crate (name "b2sum-rs") (vers "0.1.0") (deps (list (crate-dep (name "blake2-rfc") (req "^0.2.18") (default-features #t) (kind 0)) (crate-dep (name "filebuffer") (req "^0.4.0") (default-features #t) (kind 0)) (crate-dep (name "hex") (req "^0.4.2") (default-features #t) (kind 0)))) (hash "0hi0b42gxwycbii6xf3cccw93cwmf0l5rmilrvmawy9x4y66k45b")))

(define-public crate-b2sum-rs-0.1 (crate (name "b2sum-rs") (vers "0.1.1") (deps (list (crate-dep (name "blake2-rfc") (req "^0.2.18") (default-features #t) (kind 0)) (crate-dep (name "filebuffer") (req "^0.4.0") (default-features #t) (kind 0)) (crate-dep (name "hex") (req "^0.4.2") (default-features #t) (kind 0)))) (hash "0g5dfj7fy8nayfqqidmyxgwm7z8x9bpgmnsqja1xjidrv0s9fw6k")))

(define-public crate-b2sum-rust-0.1 (crate (name "b2sum-rust") (vers "0.1.0") (deps (list (crate-dep (name "blake2-rfc") (req "^0.2.18") (default-features #t) (kind 0)) (crate-dep (name "filebuffer") (req "^0.4.0") (default-features #t) (kind 0)) (crate-dep (name "hex") (req "^0.4.0") (default-features #t) (kind 0)))) (hash "146yj90bww5l7hni97j9a452wa2iqd0q8s0sid0qiyh2rvwhgxvn")))

(define-public crate-b2sum-rust-0.1 (crate (name "b2sum-rust") (vers "0.1.1") (deps (list (crate-dep (name "blake2-rfc") (req "^0.2.18") (default-features #t) (kind 0)) (crate-dep (name "filebuffer") (req "^0.4.0") (default-features #t) (kind 0)) (crate-dep (name "hex") (req "^0.4.2") (default-features #t) (kind 0)))) (hash "1w03lgj4jsalyddgjrvq1ajn5vhazxgnvdfamj0nwi8f1rglvvk6")))

(define-public crate-b2sum-rust-0.2 (crate (name "b2sum-rust") (vers "0.2.0") (deps (list (crate-dep (name "blake2-rfc") (req "^0.2.18") (default-features #t) (kind 0)) (crate-dep (name "filebuffer") (req "^0.4.0") (default-features #t) (kind 0)) (crate-dep (name "hex") (req "^0.4.2") (default-features #t) (kind 0)))) (hash "1n8gw7rpv1xja094xckpi5mmli59c46ivxz5xm6xj4cqbmj51rvd")))

(define-public crate-b2sum-rust-0.2 (crate (name "b2sum-rust") (vers "0.2.1") (deps (list (crate-dep (name "blake2-rfc") (req "^0.2.18") (default-features #t) (kind 0)) (crate-dep (name "filebuffer") (req "^0.4.0") (default-features #t) (kind 0)) (crate-dep (name "hex") (req "^0.4.2") (default-features #t) (kind 0)))) (hash "13mgmz1aywh3mvx8ridq60z4p0bh0h00pqpv0yx6vd5j802aa25f")))

(define-public crate-b2sum-rust-0.3 (crate (name "b2sum-rust") (vers "0.3.0") (deps (list (crate-dep (name "blake2-rfc") (req "^0.2.18") (default-features #t) (kind 0)) (crate-dep (name "filebuffer") (req "^0.4.0") (default-features #t) (kind 0)) (crate-dep (name "hex") (req "^0.4.2") (default-features #t) (kind 0)))) (hash "0396avv9jn517blg1nhh55k1wrqjkhp824d8crnvxn4kia4izdpc")))

