(define-module (crates-io mn td) #:use-module (crates-io))

(define-public crate-mntdf-0.1 (crate (name "mntdf") (vers "0.1.0") (deps (list (crate-dep (name "getopt") (req "^1.1.3") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2.113") (default-features #t) (kind 0)) (crate-dep (name "mnt") (req "^0.3.1") (default-features #t) (kind 0)))) (hash "0arzfciaz9k61xzd36k4f79wkj9a94ij9mhff9zvfs5mryw9sbrd")))

(define-public crate-mntdf-0.1 (crate (name "mntdf") (vers "0.1.1") (deps (list (crate-dep (name "getopt") (req "^1.1.3") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2.113") (default-features #t) (kind 0)) (crate-dep (name "mnt") (req "^0.3.1") (default-features #t) (kind 0)))) (hash "1slf4nj1fqsjzh4jri149das3fwbwj8lk8s50ss8b610l1vymy17")))

