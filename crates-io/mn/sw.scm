(define-module (crates-io mn sw) #:use-module (crates-io))

(define-public crate-mnswpr-0.1 (crate (name "mnswpr") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^4.0.32") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)) (crate-dep (name "termion") (req "^2.0.1") (default-features #t) (kind 0)))) (hash "1fjhc68gq096spqglg5c1wq585rk3xhzygkc1w7j4xk4j8lmsw6j")))

(define-public crate-mnswpr-0.1 (crate (name "mnswpr") (vers "0.1.1") (deps (list (crate-dep (name "clap") (req "^4.0.32") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)) (crate-dep (name "termion") (req "^2.0.1") (default-features #t) (kind 0)))) (hash "0f4mx3hmnk5rsxzifr7h6sd8fssrssak0hkllz5sn0xw0gyapavx")))

(define-public crate-mnswpr-0.1 (crate (name "mnswpr") (vers "0.1.2") (deps (list (crate-dep (name "clap") (req "^4.0.32") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)) (crate-dep (name "termion") (req "^2.0.1") (default-features #t) (kind 0)))) (hash "1irnl3c7581h5wx8a8cm16r6y3qxym4zlyfrcrl4h4686ihawrr8")))

(define-public crate-mnswpr-0.2 (crate (name "mnswpr") (vers "0.2.0") (deps (list (crate-dep (name "clap") (req "^4.0.32") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)) (crate-dep (name "termion") (req "^2.0.1") (default-features #t) (kind 0)))) (hash "0rchkjy3yrmks0pqllhny3v4h3micmnvwhqkvhhzvj32kw6mal7y")))

(define-public crate-mnswpr-0.3 (crate (name "mnswpr") (vers "0.3.0") (deps (list (crate-dep (name "anyhow") (req "^1.0.68") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^4.0.32") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.152") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_yaml") (req "^0.9.16") (default-features #t) (kind 0)) (crate-dep (name "termion") (req "^2.0.1") (default-features #t) (kind 0)))) (hash "0i6h9f3pvqm0jz3rwzhxvacpkaslll94f4i4wqr00nxhky2xg7ms")))

