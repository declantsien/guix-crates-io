(define-module (crates-io mn l-) #:use-module (crates-io))

(define-public crate-mnl-sys-0.1 (crate (name "mnl-sys") (vers "0.1.0") (deps (list (crate-dep (name "libc") (req "^0.2.40") (default-features #t) (kind 0)) (crate-dep (name "pkg-config") (req "^0.3") (default-features #t) (kind 1)))) (hash "1cs19n2i63w23ysc5x2r8z4ls9h1fqmysyj3dj5ydxpy2w8b0z2x") (features (quote (("mnl-1-0-4"))))))

(define-public crate-mnl-sys-0.2 (crate (name "mnl-sys") (vers "0.2.0") (deps (list (crate-dep (name "libc") (req "^0.2.41") (default-features #t) (kind 0)) (crate-dep (name "pkg-config") (req "^0.3") (default-features #t) (kind 1)))) (hash "08kilcrbj35f1ygvhlqc21nxq0f5fgzh4r9ap6jv68n7k5ah60jk") (features (quote (("mnl-1-0-4"))))))

(define-public crate-mnl-sys-0.2 (crate (name "mnl-sys") (vers "0.2.1") (deps (list (crate-dep (name "libc") (req "^0.2.41") (default-features #t) (kind 0)) (crate-dep (name "pkg-config") (req "^0.3.16") (default-features #t) (kind 1)))) (hash "0mq4yxas22h894dlik49b5bglw4iha3h6pdayymcy7hy41dnhl4p") (features (quote (("mnl-1-0-4"))))))

