(define-module (crates-io mn um) #:use-module (crates-io))

(define-public crate-mnumonic-0.1 (crate (name "mnumonic") (vers "0.1.0") (hash "1x3f2f8pk1d584dn4wc29wk0s9y9knc4xmaxlpfaqp0yc0ifhwqb") (features (quote (("en") ("default" "en"))))))

(define-public crate-mnumonic-0.2 (crate (name "mnumonic") (vers "0.2.0") (hash "10zfcxr5dm9k4bhbw4k3m0nvsxlrdjvh2nmmgcn2234w219r9v2k") (features (quote (("en") ("default" "en"))))))

