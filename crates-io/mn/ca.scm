(define-module (crates-io mn ca) #:use-module (crates-io))

(define-public crate-mncalc-0.1 (crate (name "mncalc") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^4.3.0") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "00w4g59sv2fnx6lk3dh16cds6ckh1ly3yp0wykxs1p3qamwkkqmb")))

