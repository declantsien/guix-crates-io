(define-module (crates-io e_ nu) #:use-module (crates-io))

(define-public crate-e_num-0.1 (crate (name "e_num") (vers "0.1.0") (deps (list (crate-dep (name "e_num_derive") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "02xd7hw5lb4n5fpx5ki5j5q1grvc2k2n3zhc8fhfggnayx8yrwh0")))

(define-public crate-e_num_derive-0.1 (crate (name "e_num_derive") (vers "0.1.0") (deps (list (crate-dep (name "darling") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^0.6") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^0.15") (default-features #t) (kind 0)))) (hash "0cwdjarj9jb20mkbwxw6r9p8rsis39121ff0273q9zfb1prvb9ld")))

