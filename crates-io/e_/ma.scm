(define-module (crates-io e_ ma) #:use-module (crates-io))

(define-public crate-e_macro-0.1 (crate (name "e_macro") (vers "0.1.0") (deps (list (crate-dep (name "quote") (req "^1.0.35") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2.0.48") (features (quote ("full" "fold"))) (default-features #t) (kind 0)))) (hash "1n71cgh284ic74sg8xx66xyi34wc6bhxd92ic8n2pf2z7k7833vk") (yanked #t)))

(define-public crate-e_macro-0.1 (crate (name "e_macro") (vers "0.1.1") (deps (list (crate-dep (name "quote") (req "^1.0.35") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2.0.48") (features (quote ("full" "fold"))) (default-features #t) (kind 0)))) (hash "1f6cxhfc84hx0dq752qiwa75a5798baq9d9ncdp2s9naypwpiixw") (yanked #t)))

(define-public crate-e_macro-0.2 (crate (name "e_macro") (vers "0.2.0") (deps (list (crate-dep (name "quote") (req "^1.0.35") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2.0.48") (features (quote ("full" "fold"))) (default-features #t) (kind 0)))) (hash "0rd269ivjxj38qjgif36kr3rld1mwjda1njd2w4gph38q2h59n90") (yanked #t)))

(define-public crate-e_macro-0.2 (crate (name "e_macro") (vers "0.2.1") (hash "0cwzy93kn6cw21shgc4fc2nlwrmyjdxyqcl1zyzqra6i11r38jh7") (yanked #t)))

(define-public crate-e_macro-0.0.0 (crate (name "e_macro") (vers "0.0.0") (hash "0jpq8da7ny76hf1dpxdm13nq2mr6rnqv696a8qz13vgd6wniw0ms")))

