(define-module (crates-io e_ dr) #:use-module (crates-io))

(define-public crate-e_drone-21 (crate (name "e_drone") (vers "21.3.1") (deps (list (crate-dep (name "num_enum") (req "^0.5.1") (default-features #t) (kind 0)))) (hash "1aqj7x8gbsmsqwyqrx5ngbv121m4wxkxbn66sxdhlfdv3qp8lpi5") (yanked #t)))

(define-public crate-e_drone-21 (crate (name "e_drone") (vers "21.3.2") (deps (list (crate-dep (name "bincode") (req "^1.3.2") (default-features #t) (kind 0)) (crate-dep (name "num_enum") (req "^0.5.1") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "02xj71s2jbp3np6h3jjzgv3qnfcmxhbnqh0aa3s2061fbbzc4dq0") (yanked #t)))

(define-public crate-e_drone-21 (crate (name "e_drone") (vers "21.3.3") (deps (list (crate-dep (name "bincode") (req "^1.3.2") (default-features #t) (kind 0)) (crate-dep (name "num_enum") (req "^0.5.1") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "0pi73cps7k56g7jhdifqi3f1s185z3lgrdc6vbh1amzsav8sli9p") (yanked #t)))

(define-public crate-e_drone-21 (crate (name "e_drone") (vers "21.3.4") (deps (list (crate-dep (name "bincode") (req "^1.3.2") (default-features #t) (kind 0)) (crate-dep (name "num_enum") (req "^0.5.1") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "04n46mygvblvr8cz4wi3pb0wsaj5hncz7gw4r7pdqnqwll96q12y") (yanked #t)))

(define-public crate-e_drone-21 (crate (name "e_drone") (vers "21.3.5") (deps (list (crate-dep (name "byteorder") (req "^1.4.2") (default-features #t) (kind 0)) (crate-dep (name "num_enum") (req "^0.5.1") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "166jv8s1pwnqa9mc83a30xrc6i6qhd56hi10sjf1r3a2vd9asy16") (yanked #t)))

(define-public crate-e_drone-21 (crate (name "e_drone") (vers "21.3.6") (deps (list (crate-dep (name "byteorder") (req "^1.4.2") (default-features #t) (kind 0)) (crate-dep (name "num_enum") (req "^0.5.1") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "0r4q626pfrkzwbngc12w6n0k8js3l5pw3vxp2p7ksdb12j8zg1hj") (yanked #t)))

(define-public crate-e_drone-21 (crate (name "e_drone") (vers "21.3.7") (deps (list (crate-dep (name "byteorder") (req "^1.4.2") (default-features #t) (kind 0)) (crate-dep (name "num_enum") (req "^0.5.1") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "1564nc4k6ryd76brgd4ac4cnxgr3hfzvp28m4d2v8s751ph9719m") (yanked #t)))

(define-public crate-e_drone-21 (crate (name "e_drone") (vers "21.3.8") (deps (list (crate-dep (name "byteorder") (req "^1.4.2") (default-features #t) (kind 0)) (crate-dep (name "num_enum") (req "^0.5.1") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "12pjmnqnah0azrdvrmbfbc96wi5nrbknzapsni11xbknn07l5brm") (yanked #t)))

(define-public crate-e_drone-21 (crate (name "e_drone") (vers "21.3.9") (deps (list (crate-dep (name "byteorder") (req "^1.4.2") (default-features #t) (kind 0)) (crate-dep (name "num_enum") (req "^0.5.1") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "16ggjqxh2i01950sh0arf167g0wnm4xnljdcscf0dkj7ny2f71mc") (yanked #t)))

(define-public crate-e_drone-21 (crate (name "e_drone") (vers "21.3.10") (deps (list (crate-dep (name "byteorder") (req "^1.4.2") (default-features #t) (kind 0)) (crate-dep (name "num_enum") (req "^0.5.1") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "16444xvz8i3fdpklqycax5261b9460ldrdn1abiimsya3b0nf52p") (yanked #t)))

(define-public crate-e_drone-21 (crate (name "e_drone") (vers "21.3.11") (deps (list (crate-dep (name "byteorder") (req "^1.4.2") (default-features #t) (kind 0)) (crate-dep (name "num_enum") (req "^0.5.1") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "1031s830aikki8dsylqrc20j06a5a3d6fc365is719xclff11fin") (yanked #t)))

(define-public crate-e_drone-21 (crate (name "e_drone") (vers "21.3.12") (deps (list (crate-dep (name "byteorder") (req "^1.4.2") (default-features #t) (kind 0)) (crate-dep (name "num_enum") (req "^0.5.1") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "1n5h3764cmxd6fvblffn78jl57ggv53zqm2fyvn9i8zic2ycb7j9") (yanked #t)))

(define-public crate-e_drone-21 (crate (name "e_drone") (vers "21.3.13") (deps (list (crate-dep (name "byteorder") (req "^1.4.2") (default-features #t) (kind 0)) (crate-dep (name "num_enum") (req "^0.5.1") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "03kyyh9vdwb3l9q8flfgmd9z31hf4jh044vl0779a6mid4i0fnij") (yanked #t)))

(define-public crate-e_drone-21 (crate (name "e_drone") (vers "21.3.14") (deps (list (crate-dep (name "byteorder") (req "^1.4.2") (default-features #t) (kind 0)) (crate-dep (name "num_enum") (req "^0.5.1") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "1hlphx8cvxmh2nwydd07a0kjnh18mibzgw1s2v3al9mimbq0y1i3") (yanked #t)))

(define-public crate-e_drone-21 (crate (name "e_drone") (vers "21.3.15") (deps (list (crate-dep (name "byteorder") (req "^1.4.2") (default-features #t) (kind 0)) (crate-dep (name "num_enum") (req "^0.5.1") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "1biyp03blzqnwb0jyf0w0kl4df5w0kh5i066wlm600lr4nmwpkff") (yanked #t)))

(define-public crate-e_drone-21 (crate (name "e_drone") (vers "21.3.16") (deps (list (crate-dep (name "byteorder") (req "^1.4.2") (default-features #t) (kind 0)) (crate-dep (name "num_enum") (req "^0.5.1") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "1vqmpv43gakaj5pkynfxpqxy7fhriy7dvfg2ja5kgdxbqb7yfcf6") (yanked #t)))

(define-public crate-e_drone-21 (crate (name "e_drone") (vers "21.3.17") (deps (list (crate-dep (name "byteorder") (req "^1.4.2") (default-features #t) (kind 0)) (crate-dep (name "num_enum") (req "^0.5.1") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serialport") (req "^4.0.0") (default-features #t) (kind 0)))) (hash "02nnc985bidviia95yyqi5rinbqwhq9sminpz2bpcva6a2w19k70") (yanked #t)))

(define-public crate-e_drone-21 (crate (name "e_drone") (vers "21.3.18") (deps (list (crate-dep (name "byteorder") (req "^1.4.2") (default-features #t) (kind 0)) (crate-dep (name "num_enum") (req "^0.5.1") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serialport") (req "^4.0.0") (default-features #t) (kind 0)))) (hash "0f3hgsawb2ampw7rrmqdg240sai5c3nmna4wdyjwyhnx8p2gsv87") (yanked #t)))

(define-public crate-e_drone-21 (crate (name "e_drone") (vers "21.3.19") (deps (list (crate-dep (name "byteorder") (req "^1.4.2") (default-features #t) (kind 0)) (crate-dep (name "num_enum") (req "^0.5.1") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "1wzz5v0pn57p3gy66kb9wigcjl21dhrzhpj6s37cimlz90bfgrlp") (yanked #t)))

(define-public crate-e_drone-21 (crate (name "e_drone") (vers "21.3.20") (deps (list (crate-dep (name "byteorder") (req "^1.4.2") (default-features #t) (kind 0)) (crate-dep (name "num_enum") (req "^0.5.1") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "0cxgcn8gk3m0ahbfai2l6bf80cybxgzg9d2ljmcan91cmxknm7zh") (yanked #t)))

(define-public crate-e_drone-21 (crate (name "e_drone") (vers "21.3.21") (deps (list (crate-dep (name "byteorder") (req "^1.4.2") (default-features #t) (kind 0)) (crate-dep (name "num_enum") (req "^0.5.1") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "1w397l1all6n1dnj2hyr3a6xsfrawla71x4n39p73s7d89abl9nj") (yanked #t)))

(define-public crate-e_drone-21 (crate (name "e_drone") (vers "21.3.22") (deps (list (crate-dep (name "byteorder") (req "^1.4.2") (default-features #t) (kind 0)) (crate-dep (name "num_enum") (req "^0.5.1") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "1wxx4c2kj9i0csymhlxgm4hqcajmm0f9sf5gm908qd2azz660fhg") (yanked #t)))

(define-public crate-e_drone-21 (crate (name "e_drone") (vers "21.3.23") (deps (list (crate-dep (name "byteorder") (req "^1.4.2") (default-features #t) (kind 0)) (crate-dep (name "num_enum") (req "^0.5.1") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "13kxnd0pl11dbi7xy5j4ald0ybvba91ckhq11hvabi42x75a2xld") (yanked #t)))

(define-public crate-e_drone-21 (crate (name "e_drone") (vers "21.3.24") (deps (list (crate-dep (name "byteorder") (req "^1.4.2") (default-features #t) (kind 0)) (crate-dep (name "num_enum") (req "^0.5.1") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "0w9nz38yjqrxrimdb9vd8m4sb5kc9rsppa970hgqcarzz81rwz7x") (yanked #t)))

(define-public crate-e_drone-21 (crate (name "e_drone") (vers "21.3.25") (deps (list (crate-dep (name "byteorder") (req "^1.4.2") (default-features #t) (kind 0)) (crate-dep (name "num_enum") (req "^0.5.1") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "0phwsnzfnn2k63is39ssvp069h8malc9sg8nikfq0acfnzn948fk") (yanked #t)))

(define-public crate-e_drone-21 (crate (name "e_drone") (vers "21.3.26") (deps (list (crate-dep (name "byteorder") (req "^1.4.2") (default-features #t) (kind 0)) (crate-dep (name "num_enum") (req "^0.5.1") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "16w7xw119v9n50dc4dmd3kr1f13kydp3azr3aij37s9lhw548b7b") (yanked #t)))

(define-public crate-e_drone-21 (crate (name "e_drone") (vers "21.3.27") (deps (list (crate-dep (name "byteorder") (req "^1.4.2") (default-features #t) (kind 0)) (crate-dep (name "num_enum") (req "^0.5.1") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "13gazpif2ra12m6hypdpmsi11a8qmm84c9si9lxyc3r94kmm5ahm") (yanked #t)))

(define-public crate-e_drone-21 (crate (name "e_drone") (vers "21.3.28") (deps (list (crate-dep (name "byteorder") (req "^1.4.2") (default-features #t) (kind 0)) (crate-dep (name "num_enum") (req "^0.5.1") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "19x5ini3n31g6xxv7fpn0imh7kg6x16grmzybsh6l639lxbbc6gw") (yanked #t)))

(define-public crate-e_drone-21 (crate (name "e_drone") (vers "21.5.1") (deps (list (crate-dep (name "byteorder") (req "^1.4.2") (default-features #t) (kind 0)) (crate-dep (name "num_enum") (req "^0.5.1") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "09v3q04jg12dwv6jbp402dgbnx3f5aq6yk9b900yf0c8wf5rz5vq") (yanked #t)))

(define-public crate-e_drone-21 (crate (name "e_drone") (vers "21.6.1") (deps (list (crate-dep (name "byteorder") (req "^1.4.2") (default-features #t) (kind 0)) (crate-dep (name "num_enum") (req "^0.5.1") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "08lfqfqmg96nynpdj9dyv4iqnv3rhf84lsnapylhrrzbjw9vsyjy") (yanked #t)))

(define-public crate-e_drone-21 (crate (name "e_drone") (vers "21.7.1") (deps (list (crate-dep (name "byteorder") (req "^1.4.2") (default-features #t) (kind 0)) (crate-dep (name "num_enum") (req "^0.5.1") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "0ya6d0jan1dccl5k7kqznayj478h85a6di3r7xvxrbkxqn30d7zz") (yanked #t)))

(define-public crate-e_drone-21 (crate (name "e_drone") (vers "21.7.2") (deps (list (crate-dep (name "byteorder") (req "^1.4.2") (default-features #t) (kind 0)) (crate-dep (name "num_enum") (req "^0.5.1") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "1y63xk1r4akr4yzbn9093nb8b3bqv0ps93amymgflsb18ms0l38z") (yanked #t)))

(define-public crate-e_drone-21 (crate (name "e_drone") (vers "21.8.1") (deps (list (crate-dep (name "byteorder") (req "^1.4.2") (default-features #t) (kind 0)) (crate-dep (name "num_enum") (req "^0.5.1") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "0g9kkwg0bcwgrpqwwvfk5yv206v7dn49p43c44kkf7nwrlnwllpz") (yanked #t)))

(define-public crate-e_drone-21 (crate (name "e_drone") (vers "21.9.1") (deps (list (crate-dep (name "byteorder") (req "^1.4.2") (default-features #t) (kind 0)) (crate-dep (name "num_enum") (req "^0.5.1") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "0sj4nclyrrb6w7c6hpsa5wfcjn9q5ajgxkvw487298arncgifxi3") (yanked #t)))

(define-public crate-e_drone-21 (crate (name "e_drone") (vers "21.9.2") (deps (list (crate-dep (name "byteorder") (req "^1.4.2") (default-features #t) (kind 0)) (crate-dep (name "num_enum") (req "^0.5.1") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "05zldgqcsl39c54ikz1pn5sal7gkxpjb5wd0b9z3sqpk5ja30c8n") (yanked #t)))

(define-public crate-e_drone-22 (crate (name "e_drone") (vers "22.1.1") (deps (list (crate-dep (name "byteorder") (req "^1.4.3") (default-features #t) (kind 0)) (crate-dep (name "num_enum") (req "^0.5.6") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "0f1di8ps3qi2pnaqsngd71an060vxkm01k01hlc0bf0jlimijqdl") (yanked #t)))

(define-public crate-e_drone-22 (crate (name "e_drone") (vers "22.4.1") (deps (list (crate-dep (name "byteorder") (req "^1.4.3") (default-features #t) (kind 0)) (crate-dep (name "num_enum") (req "^0.5.6") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "1piiiiz2rkndwqxhflkkxvinkcfwg3lh3qp2r5hfgw0sn82bibh3") (yanked #t)))

(define-public crate-e_drone-22 (crate (name "e_drone") (vers "22.4.2") (deps (list (crate-dep (name "byteorder") (req "^1.4.3") (default-features #t) (kind 0)) (crate-dep (name "num_enum") (req "^0.5.6") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "0kkariiqf3paymm96ihln9ky6d10zf7zny1cfp8v6190ahg37chp") (yanked #t)))

(define-public crate-e_drone-22 (crate (name "e_drone") (vers "22.4.3") (deps (list (crate-dep (name "byteorder") (req "^1.4.3") (default-features #t) (kind 0)) (crate-dep (name "num_enum") (req "^0.5.6") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "1585y2pd00dnnj52w4z7ghdhwqvnmwg2f15907h3x2k76l46dly6") (yanked #t)))

(define-public crate-e_drone-22 (crate (name "e_drone") (vers "22.4.4") (deps (list (crate-dep (name "byteorder") (req "^1.4.3") (default-features #t) (kind 0)) (crate-dep (name "num_enum") (req "^0.5.6") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "0l9fs8p3ryc4i5aawgb0v5xk1gzbli99lx2ld22bhvvrdv711ziv") (yanked #t)))

(define-public crate-e_drone-22 (crate (name "e_drone") (vers "22.5.1") (deps (list (crate-dep (name "byteorder") (req "^1.4") (default-features #t) (kind 0)) (crate-dep (name "num_enum") (req "^0.5") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "0665r11zlpjzhbkbsqxc18skxgl9khrhdq75pqp8ylbqkgzrj90q") (yanked #t)))

(define-public crate-e_drone-22 (crate (name "e_drone") (vers "22.5.2") (deps (list (crate-dep (name "byteorder") (req "^1.4") (default-features #t) (kind 0)) (crate-dep (name "num_enum") (req "^0.5") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "1sj9fpaas6jck8w1jmx4maa9cxjhydjvn5v970kbl1cnjx49w7ir") (yanked #t)))

(define-public crate-e_drone-22 (crate (name "e_drone") (vers "22.5.3") (deps (list (crate-dep (name "byteorder") (req "^1.4") (default-features #t) (kind 0)) (crate-dep (name "num_enum") (req "^0.5") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "0ixgl90y1037k3m52r5s6mx5a2sr220bdli3kgk221sn9snnd69j") (yanked #t)))

(define-public crate-e_drone-22 (crate (name "e_drone") (vers "22.6.1") (deps (list (crate-dep (name "byteorder") (req "^1.4") (default-features #t) (kind 0)) (crate-dep (name "num_enum") (req "^0.5") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "0hkbd4325xdgvcyqc45p8j97hvnfmarl9ky2jrwj00g7f0npsjjm") (yanked #t)))

(define-public crate-e_drone-22 (crate (name "e_drone") (vers "22.6.2") (deps (list (crate-dep (name "byteorder") (req "^1.4") (default-features #t) (kind 0)) (crate-dep (name "num_enum") (req "^0.5") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "1sr531ry41gyzc40n12wc2agw75a85xwh8pfabvx4vlikm870a51")))

(define-public crate-e_drone_base-0.1 (crate (name "e_drone_base") (vers "0.1.0") (hash "01w323zj95cblcfj99nv70dcwi61sgg0pgpb9xdqi7mhbsc2v472") (yanked #t)))

(define-public crate-e_drone_rpi-0.1 (crate (name "e_drone_rpi") (vers "0.1.0") (hash "1nfhqh3p75q8v2bacc11z3gclv4bq9dm9mhqr9y2c9rizw3b98ms") (yanked #t)))

(define-public crate-e_drone_rpi-21 (crate (name "e_drone_rpi") (vers "21.3.2") (deps (list (crate-dep (name "e_drone") (req "21.*") (default-features #t) (kind 0)) (crate-dep (name "rppal") (req "^0.11.3") (default-features #t) (kind 0)))) (hash "1r4mp7ci54jqrpa4r3bw12qivaq4didgnfb94a05w4bsfjbb0xw4") (yanked #t)))

(define-public crate-e_drone_rpi-21 (crate (name "e_drone_rpi") (vers "21.3.3") (deps (list (crate-dep (name "e_drone") (req "21.*") (default-features #t) (kind 0)) (crate-dep (name "rppal") (req "^0.11.3") (default-features #t) (kind 0)))) (hash "1wlpm0lf8rwdw9r2yzsfpm2if08hnlkdrzx6d48w9xmx0i72zgzk") (yanked #t)))

(define-public crate-e_drone_rpi-21 (crate (name "e_drone_rpi") (vers "21.3.4") (deps (list (crate-dep (name "e_drone") (req "21.*") (default-features #t) (kind 0)) (crate-dep (name "rppal") (req "^0.11.3") (default-features #t) (kind 0)))) (hash "1c5qxsrnsxkg6f5nn7dbvfll7gbcmrwmprbmf6ybfm2pchdcpwq3") (yanked #t)))

(define-public crate-e_drone_rpi-21 (crate (name "e_drone_rpi") (vers "21.3.5") (deps (list (crate-dep (name "chrono") (req "0.4.*") (default-features #t) (kind 0)) (crate-dep (name "e_drone") (req "21.*") (default-features #t) (kind 0)) (crate-dep (name "rppal") (req "^0.11.3") (default-features #t) (kind 0)))) (hash "0sb8pmrls95b3fzb6nvyb7ipm23j89fqmfaqi6i54iqm50vvdd8f") (yanked #t)))

(define-public crate-e_drone_rpi-21 (crate (name "e_drone_rpi") (vers "21.3.6") (deps (list (crate-dep (name "chrono") (req "0.4.*") (default-features #t) (kind 0)) (crate-dep (name "e_drone") (req "21.*") (default-features #t) (kind 0)) (crate-dep (name "rppal") (req "^0.11.3") (default-features #t) (kind 0)))) (hash "06hfdqzyiq7s1jwj2gjc2immspvnn2layhfqsn64pypcaxiixa3a") (yanked #t)))

(define-public crate-e_drone_rpi-21 (crate (name "e_drone_rpi") (vers "21.3.7") (deps (list (crate-dep (name "chrono") (req "0.4.*") (default-features #t) (kind 0)) (crate-dep (name "e_drone") (req "21.*") (default-features #t) (kind 0)) (crate-dep (name "rppal") (req "^0.11.3") (default-features #t) (kind 0)))) (hash "1issn095v9fp2rqzxa12mpzvxbbycqb5s9zy71wbpqz6dgl7hhxs") (yanked #t)))

(define-public crate-e_drone_rpi-21 (crate (name "e_drone_rpi") (vers "21.3.8") (deps (list (crate-dep (name "chrono") (req "0.4.*") (default-features #t) (kind 0)) (crate-dep (name "e_drone") (req "21.*") (default-features #t) (kind 0)) (crate-dep (name "rppal") (req "^0.11.3") (default-features #t) (kind 0)))) (hash "113kyx7iwyx29m69ydcpca9wklpkhwxn084c48z46hb009ir6bqh") (yanked #t)))

(define-public crate-e_drone_rpi-21 (crate (name "e_drone_rpi") (vers "21.7.1") (deps (list (crate-dep (name "chrono") (req "0.4.*") (default-features #t) (kind 0)) (crate-dep (name "e_drone") (req "21.*") (default-features #t) (kind 0)) (crate-dep (name "rppal") (req "^0.11.3") (default-features #t) (kind 0)))) (hash "138986gw2jgmsnphc4mfb5pnz44lcpa3srlsnfzr8054wzf7cz50") (yanked #t)))

(define-public crate-e_drone_rpi-22 (crate (name "e_drone_rpi") (vers "22.1.1") (deps (list (crate-dep (name "chrono") (req "0.4.*") (default-features #t) (kind 0)) (crate-dep (name "e_drone") (req "22.*") (default-features #t) (kind 0)) (crate-dep (name "rppal") (req "0.13.*") (default-features #t) (kind 0)))) (hash "1km1icg5kd3wq9da5fzqx0mlyi7p2al8pkzanf61w9wd9j6agpi8")))

(define-public crate-e_drone_sp-21 (crate (name "e_drone_sp") (vers "21.3.1") (deps (list (crate-dep (name "e_drone") (req "21.*") (default-features #t) (kind 0)) (crate-dep (name "serialport") (req "^4.0.0") (default-features #t) (kind 0)))) (hash "13zhp5janmxrljm22nyi9zw3cd385821664ldi2h5isgjhf0vsn6") (yanked #t)))

(define-public crate-e_drone_sp-21 (crate (name "e_drone_sp") (vers "21.3.2") (deps (list (crate-dep (name "e_drone") (req "21.*") (default-features #t) (kind 0)) (crate-dep (name "serialport") (req "^4.0.0") (default-features #t) (kind 0)))) (hash "13a7j7vk3k4ml8hmgnvyc82jh20k5r97v87l3p0ilw1ajf0qrd3l") (yanked #t)))

(define-public crate-e_drone_sp-21 (crate (name "e_drone_sp") (vers "21.3.3") (deps (list (crate-dep (name "e_drone") (req "21.*") (default-features #t) (kind 0)) (crate-dep (name "serialport") (req "^4.0.0") (default-features #t) (kind 0)))) (hash "1wf9md56k0jc9vp4wi20k483pww5a5211qpsq430238kqarp4phf") (yanked #t)))

(define-public crate-e_drone_sp-21 (crate (name "e_drone_sp") (vers "21.3.4") (deps (list (crate-dep (name "e_drone") (req "21.*") (default-features #t) (kind 0)) (crate-dep (name "serialport") (req "^4.0.0") (default-features #t) (kind 0)))) (hash "1cak7g99r9lya7npqv6f17nlv5dxvibba969shcdm4jqls1zjkp4") (yanked #t)))

(define-public crate-e_drone_sp-21 (crate (name "e_drone_sp") (vers "21.3.5") (deps (list (crate-dep (name "chrono") (req "0.4.*") (default-features #t) (kind 0)) (crate-dep (name "e_drone") (req "21.*") (default-features #t) (kind 0)) (crate-dep (name "serialport") (req "^4.0.0") (default-features #t) (kind 0)))) (hash "1j9yh05gd4gydfs0vkwman6g4nr94i40rgp6pvg96h9ncq2fgcni") (yanked #t)))

(define-public crate-e_drone_sp-21 (crate (name "e_drone_sp") (vers "21.3.6") (deps (list (crate-dep (name "chrono") (req "0.4.*") (default-features #t) (kind 0)) (crate-dep (name "e_drone") (req "21.*") (default-features #t) (kind 0)) (crate-dep (name "serialport") (req "^4.0.0") (default-features #t) (kind 0)))) (hash "1yxnpgjpdc8nwi2avqqbdgga2mr59pn019idralvqjikixg7xvag") (yanked #t)))

(define-public crate-e_drone_sp-21 (crate (name "e_drone_sp") (vers "21.3.7") (deps (list (crate-dep (name "chrono") (req "0.4.*") (default-features #t) (kind 0)) (crate-dep (name "e_drone") (req "21.*") (default-features #t) (kind 0)) (crate-dep (name "serialport") (req "^4.0.0") (default-features #t) (kind 0)))) (hash "1fn1azhgln4z41768sj5nagqd5w9jfy31j40jmwn3d1ziljf14wx") (yanked #t)))

(define-public crate-e_drone_sp-21 (crate (name "e_drone_sp") (vers "21.3.8") (deps (list (crate-dep (name "chrono") (req "0.4.*") (default-features #t) (kind 0)) (crate-dep (name "e_drone") (req "21.*") (default-features #t) (kind 0)) (crate-dep (name "serialport") (req "^4.0.0") (default-features #t) (kind 0)))) (hash "0gkikg6akzkyqj3hjjiqgx17ipr6bnf1lslnyn5p93hfj2szf7r7") (yanked #t)))

(define-public crate-e_drone_sp-21 (crate (name "e_drone_sp") (vers "21.3.9") (deps (list (crate-dep (name "chrono") (req "0.4.*") (default-features #t) (kind 0)) (crate-dep (name "e_drone") (req "21.*") (default-features #t) (kind 0)) (crate-dep (name "serialport") (req "^4.0.0") (default-features #t) (kind 0)))) (hash "0yln1nk7p2b0vg3mvfaymfb20a10k13v7i28c20vavg3lkmk7qm9") (yanked #t)))

(define-public crate-e_drone_sp-21 (crate (name "e_drone_sp") (vers "21.3.10") (deps (list (crate-dep (name "chrono") (req "0.4.*") (default-features #t) (kind 0)) (crate-dep (name "e_drone") (req "21.*") (default-features #t) (kind 0)) (crate-dep (name "serialport") (req "^4.0.0") (default-features #t) (kind 0)))) (hash "0k24px57xn6g1j9v5il00f05wqgappfdxnjvvdph4zkcj3kdk5bj") (yanked #t)))

(define-public crate-e_drone_sp-21 (crate (name "e_drone_sp") (vers "21.3.11") (deps (list (crate-dep (name "chrono") (req "0.4.*") (default-features #t) (kind 0)) (crate-dep (name "e_drone") (req "21.*") (default-features #t) (kind 0)) (crate-dep (name "serialport") (req "^4.0.0") (default-features #t) (kind 0)))) (hash "12wsq4aqi5vc4gc5h22zlasfi33g1wwnnbhzn61s5iklcf0aa3sz") (yanked #t)))

(define-public crate-e_drone_sp-22 (crate (name "e_drone_sp") (vers "22.1.1") (deps (list (crate-dep (name "chrono") (req "0.4.*") (default-features #t) (kind 0)) (crate-dep (name "e_drone") (req "22.*") (default-features #t) (kind 0)) (crate-dep (name "serialport") (req "^4.0.1") (default-features #t) (kind 0)))) (hash "11mmdiryi0w882j6z54za2sv17hnxx31qmnia3cknh39p5kpbici")))

(define-public crate-e_drone_sp-22 (crate (name "e_drone_sp") (vers "22.4.1") (deps (list (crate-dep (name "chrono") (req "0.4.*") (default-features #t) (kind 0)) (crate-dep (name "e_drone") (req "22.*") (default-features #t) (kind 0)) (crate-dep (name "serialport") (req "^4.0.1") (default-features #t) (kind 0)))) (hash "03wqh02fnnf62j9a59p4lby3vbc86if4i6rrnwjz6qr1nam0g4vi")))

