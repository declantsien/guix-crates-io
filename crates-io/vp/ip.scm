(define-module (crates-io vp ip) #:use-module (crates-io))

(define-public crate-vpipe-0.2 (crate (name "vpipe") (vers "0.2.1") (deps (list (crate-dep (name "clap") (req "^3.1.6") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "0wxr6vfadcmma63fgmyl2xxwn95szp6fqzriljzzf1symzhzq5fb") (rust-version "1.59")))

(define-public crate-vpipe-0.2 (crate (name "vpipe") (vers "0.2.2") (deps (list (crate-dep (name "clap") (req "^3.1.6") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "crossbeam") (req "^0.8.1") (default-features #t) (kind 0)))) (hash "1iavx9ggrzsamxs9zykbx56smi4f7b8yxhxj0sif206kny39synn") (rust-version "1.59")))

(define-public crate-vpipe-0.3 (crate (name "vpipe") (vers "0.3.0") (deps (list (crate-dep (name "clap") (req "^3.1.6") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "crossbeam") (req "^0.8.1") (default-features #t) (kind 0)) (crate-dep (name "crossterm") (req "^0.19.0") (default-features #t) (kind 0)))) (hash "1jb35gpi56l0741j3482fs7h898ly5hnqjip6m95ba7jpn3mr42v") (rust-version "1.59")))

