(define-module (crates-io vp ic) #:use-module (crates-io))

(define-public crate-vpicc-0.1 (crate (name "vpicc") (vers "0.1.0") (deps (list (crate-dep (name "env_logger") (req "^0.9.0") (default-features #t) (kind 2)) (crate-dep (name "log") (req "^0.4.14") (default-features #t) (kind 0)))) (hash "0c0y6wvszqrcga9yxgmxrzwfplmj18ph36mg3nhphdb889x2irav")))

