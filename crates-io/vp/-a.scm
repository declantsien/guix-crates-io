(define-module (crates-io vp -a) #:use-module (crates-io))

(define-public crate-vp-avl-0.1 (crate (name "vp-avl") (vers "0.1.0") (deps (list (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)))) (hash "0bjrhcsmiqw4lrn6c1mxzjx9lihwj88cb6396j97jdk8ln7xjb9c")))

(define-public crate-vp-avl-0.1 (crate (name "vp-avl") (vers "0.1.1") (deps (list (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)))) (hash "141pn293fxnwp18s0f3whxr6vjfxdysh9wf2g0cdn4a7g67lgzcl")))

(define-public crate-vp-avl-0.1 (crate (name "vp-avl") (vers "0.1.2") (deps (list (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)))) (hash "0wsjigf306zsyzqqk0yvplw129dkw0h1gwk7nflj4m65wj9szgn6")))

(define-public crate-vp-avl-0.1 (crate (name "vp-avl") (vers "0.1.3") (deps (list (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)))) (hash "1572a11zkxs6a64a57ncgvi0gnb8v4ylr2ywizj6l056jxaqyzrm")))

(define-public crate-vp-avl-0.1 (crate (name "vp-avl") (vers "0.1.4") (deps (list (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)))) (hash "047fw42g5difgg823icmda36ycvkbnj62vlh08664z7lc66nc7ch")))

(define-public crate-vp-avl-0.1 (crate (name "vp-avl") (vers "0.1.5") (deps (list (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)))) (hash "02c40k8ark0sf9f6yqpd84kz6fks9bxh16zbd6y5cl7aw3482my4")))

