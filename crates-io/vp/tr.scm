(define-module (crates-io vp tr) #:use-module (crates-io))

(define-public crate-vptr-0.1 (crate (name "vptr") (vers "0.1.0") (deps (list (crate-dep (name "vptr-macros") (req "= 0.1.0") (default-features #t) (kind 0)))) (hash "0k59x3mc9flbmdygn6zlan3fwq63slmjb10a7sil7s8w5qmahiy0") (features (quote (("std") ("default" "std"))))))

(define-public crate-vptr-0.2 (crate (name "vptr") (vers "0.2.0") (deps (list (crate-dep (name "vptr-macros") (req "= 0.2.0") (default-features #t) (kind 0)))) (hash "17dzq44n5yjxikl3h9ygahzpb0irkhbbr6q1qq6qhaplnzrmfd24") (features (quote (("std") ("default" "std"))))))

(define-public crate-vptr-0.2 (crate (name "vptr") (vers "0.2.1") (deps (list (crate-dep (name "vptr-macros") (req "= 0.2.1") (default-features #t) (kind 0)))) (hash "1ipi4z25yaskrfkwzg0d91jz1aanw4jgl9a4d9zy6zf4qjy04n07") (features (quote (("std") ("default" "std"))))))

(define-public crate-vptr-macros-0.1 (crate (name "vptr-macros") (vers "0.1.0") (deps (list (crate-dep (name "quote") (req "^0.6.12") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^0.15.34") (features (quote ("full" "extra-traits"))) (default-features #t) (kind 0)))) (hash "0cxwr2ljq3n56ya955nb7z3dsviwzms9fz36aj000wvrb7cnd4ck")))

(define-public crate-vptr-macros-0.2 (crate (name "vptr-macros") (vers "0.2.0") (deps (list (crate-dep (name "quote") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1") (features (quote ("full" "extra-traits"))) (default-features #t) (kind 0)))) (hash "1rvy5gyawlbv9rchjxav0j0rzjhijcj4nd55y334r9dyg521zz5v")))

(define-public crate-vptr-macros-0.2 (crate (name "vptr-macros") (vers "0.2.1") (deps (list (crate-dep (name "quote") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1") (features (quote ("full" "extra-traits"))) (default-features #t) (kind 0)))) (hash "03ims7a7ka58pnp0mdkmbrjnh2c6vdw8ajf2xv4ig60pkjrpxm8f")))

