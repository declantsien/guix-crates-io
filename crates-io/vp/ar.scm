(define-module (crates-io vp ar) #:use-module (crates-io))

(define-public crate-vparser-0.1 (crate (name "vparser") (vers "0.1.0") (hash "01ckzclg56swlx8xiy0fq3pz5180kqh9593d70wy5acn8x17sms9")))

(define-public crate-vparser-0.1 (crate (name "vparser") (vers "0.1.1") (hash "1z6rfxgq6vmnn30zyh1jcqr7cxqmg04snfg9svrfi7vvlmmzg1zv")))

(define-public crate-vparser-0.1 (crate (name "vparser") (vers "0.1.2") (hash "0ccdqnrylwn5iyqc4jqqhk6xgs2pw10z99r2a8cd2k7d1lsrq5hy")))

(define-public crate-vparser-0.1 (crate (name "vparser") (vers "0.1.3") (hash "0y2aiw0d44l2yywyaa9vnzd8ydqiqvw75f14lcqpaz1291zacqsg")))

(define-public crate-vparser-0.1 (crate (name "vparser") (vers "0.1.4") (hash "1366238x2xnrd52f68mcqdvvxnl391kmsinbgshnjvz3pz49vpkx")))

(define-public crate-vparser-1 (crate (name "vparser") (vers "1.0.0") (hash "0sh04sw3cyhyg9d328asrrrdkkklx8xxkrana5cb324fnqlgwpm4")))

