(define-module (crates-io vp r-) #:use-module (crates-io))

(define-public crate-vpr-audio-analyzer-0.1 (crate (name "vpr-audio-analyzer") (vers "0.1.0") (deps (list (crate-dep (name "tokio") (req "^1.29.1") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "12saq5d2p06gm24dc3yv7kljypkbsbv6jdbckwvlf461r637hzdz")))

(define-public crate-vpr-audio-player-0.1 (crate (name "vpr-audio-player") (vers "0.1.0") (deps (list (crate-dep (name "rodio") (req "^0.17.1") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.29.1") (features (quote ("full"))) (default-features #t) (kind 0)) (crate-dep (name "vpr-audio-analyzer") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "0jwblxskg646z203jw491ip1gxf9qfrcsjw795w44pfl6isa8z2g")))

