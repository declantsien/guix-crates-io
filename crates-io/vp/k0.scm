(define-module (crates-io vp k0) #:use-module (crates-io))

(define-public crate-vpk0-0.8 (crate (name "vpk0") (vers "0.8.1") (deps (list (crate-dep (name "bitstream-io") (req "^0.8.2") (default-features #t) (kind 0)) (crate-dep (name "slice-deque") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "smallvec") (req "^1.6.1") (features (quote ("union"))) (default-features #t) (kind 0)))) (hash "15y01yiix87fmd7z3w3x20wcz9w0dz2czkngrw1pndm8wlp6z1v2")))

(define-public crate-vpk0-0.8 (crate (name "vpk0") (vers "0.8.2") (deps (list (crate-dep (name "bitstream-io") (req "^0.8.2") (default-features #t) (kind 0)) (crate-dep (name "slice-deque") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "smallvec") (req "^1.6.1") (features (quote ("union"))) (default-features #t) (kind 0)))) (hash "0fndplabngdm8mslxjchiyk7ld6rg2ig05a762x8zcydbbykw08k")))

