(define-module (crates-io vp x-) #:use-module (crates-io))

(define-public crate-vpx-encode-0.3 (crate (name "vpx-encode") (vers "0.3.0") (deps (list (crate-dep (name "env-libvpx-sys") (req "^4.0.9") (default-features #t) (kind 0)))) (hash "1rnr2kif02r13lrdzxjhmv0gr2asq8f379hjp2g2snh6mspnyffg") (features (quote (("vp9"))))))

(define-public crate-vpx-encode-0.4 (crate (name "vpx-encode") (vers "0.4.0") (deps (list (crate-dep (name "env-libvpx-sys") (req "^4.0.10") (default-features #t) (kind 0)))) (hash "0rn15dscwz1rc2d34fidcx6j3haxrybqc4dadmkaa4lx08im92n1") (features (quote (("vp9"))))))

(define-public crate-vpx-encode-0.4 (crate (name "vpx-encode") (vers "0.4.1") (deps (list (crate-dep (name "env-libvpx-sys") (req "^5.0.0") (default-features #t) (kind 0)))) (hash "19iyl8yj279q76cgn90zr3xg7d9qyljy41rgv22z4l3avscdyym9") (features (quote (("vp9"))))))

(define-public crate-vpx-encode-0.5 (crate (name "vpx-encode") (vers "0.5.0") (deps (list (crate-dep (name "env-libvpx-sys") (req "^5.0.0") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0") (default-features #t) (kind 0)))) (hash "0c61v4688kdydank97k7swmfnd7b7czmqpxh05q3jrk5r81zc024") (features (quote (("vp9") ("backtrace"))))))

(define-public crate-vpx-encode-0.6 (crate (name "vpx-encode") (vers "0.6.0") (deps (list (crate-dep (name "env-libvpx-sys") (req "^5.1.0") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0") (default-features #t) (kind 0)))) (hash "0nzba0qdq0m6d9h28kz4bv1bqmm8pbk3cylxhqb4qvcc684r4c33") (features (quote (("vp9") ("backtrace"))))))

(define-public crate-vpx-encode-0.6 (crate (name "vpx-encode") (vers "0.6.1") (deps (list (crate-dep (name "env-libvpx-sys") (req "^5.1.0") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0") (default-features #t) (kind 0)))) (hash "0ss9mw9d8gmvqhdmjzmgdg6ibj5fxr9ypmr05lwxzqahq9lvmscr") (features (quote (("vp9") ("ffi-generate" "env-libvpx-sys/generate") ("backtrace"))))))

(define-public crate-vpx-encode-0.6 (crate (name "vpx-encode") (vers "0.6.2") (deps (list (crate-dep (name "env-libvpx-sys") (req "^5.1.0") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.33") (default-features #t) (kind 0)))) (hash "1dplns7fs3bwllpc748j1ybxx7kfqcw8qkm4vg5nfxny8apl27yd") (features (quote (("vp9") ("ffi-generate" "env-libvpx-sys/generate") ("backtrace"))))))

(define-public crate-vpx-sys-0.1 (crate (name "vpx-sys") (vers "0.1.0") (deps (list (crate-dep (name "bindgen") (req "^0.40") (default-features #t) (kind 1)) (crate-dep (name "metadeps") (req "^1.1") (default-features #t) (kind 1)))) (hash "19zknndzs3bkaf5s7z1a46h5gnzlx7iyn0l2gsazcn1qzakhpfhl")))

(define-public crate-vpx-sys-0.1 (crate (name "vpx-sys") (vers "0.1.1") (deps (list (crate-dep (name "bindgen") (req "^0.57") (default-features #t) (kind 1)) (crate-dep (name "system-deps") (req "^3.0") (default-features #t) (kind 1)))) (hash "0ha1p5mqcgp791b4pvsnmcvdfgcgcxb1jc0d7sl65nmamvzdj0x7")))

