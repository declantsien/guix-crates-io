(define-module (crates-io x6 #{4d}#) #:use-module (crates-io))

(define-public crate-x64dbg_sdk_sys-0.1 (crate (name "x64dbg_sdk_sys") (vers "0.1.0") (deps (list (crate-dep (name "bindgen") (req "^0.59") (default-features #t) (kind 1)) (crate-dep (name "cfg-if") (req "^1") (default-features #t) (kind 1)))) (hash "0rdq9vvpggqz2kqbbskdrw9crdb4if3mfnqv2m33l3gh8667jl1s") (features (quote (("x86") ("x64") ("default" "x64"))))))

