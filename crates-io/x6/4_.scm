(define-module (crates-io x6 #{4_}#) #:use-module (crates-io))

(define-public crate-x64_asm-0.1 (crate (name "x64_asm") (vers "0.1.0") (hash "0hjwgrcqbq85kawkqz8byf3rlh4jh5nihlp7vwgh9gscwa61kis6")))

(define-public crate-x64_asm-0.1 (crate (name "x64_asm") (vers "0.1.1") (hash "0y3fqd8l0scqcly0kkshwb5w727pwv2d66qlls5i6sfqw8ni612h")))

(define-public crate-x64_asm-0.1 (crate (name "x64_asm") (vers "0.1.2") (hash "11hqqmcrah23jqfpr78g94dy1cl0dfcjj5ajpl98s8kfrz4nrzbx")))

(define-public crate-x64_asm-0.1 (crate (name "x64_asm") (vers "0.1.4") (hash "16i0040szwd0qq2y4m0dzikrk4d8jd275wi7xdrr0crfpw9z1l2q")))

(define-public crate-x64_asm-0.1 (crate (name "x64_asm") (vers "0.1.5") (hash "0jmir737vq4kd68zvh452sz8fkl2p7hbgzv3xa02axmnq2amq0j2")))

(define-public crate-x64_asm-0.1 (crate (name "x64_asm") (vers "0.1.6") (hash "1225kyzmjfb7qmsihq9giyhzw3h4cpfhxwkz7nzw81crcnzal8r4")))

(define-public crate-x64_asm-0.1 (crate (name "x64_asm") (vers "0.1.7") (hash "0rslrwdmh7izdpfjxagp6vaqg1w5ddw3r83a900as60zjyamqrhr")))

(define-public crate-x64_asm-0.1 (crate (name "x64_asm") (vers "0.1.8") (hash "0x67hs1fp9nxbfm0ql3fi5jxlxj32ac40vvw7pqaxhp29smjq594")))

(define-public crate-x64_asm-0.1 (crate (name "x64_asm") (vers "0.1.10") (hash "1kgyrb3gdl16v9z8carwlgsqxljx3jadw429c8fsf58vfvbnr229")))

(define-public crate-x64_asm-0.1 (crate (name "x64_asm") (vers "0.1.11") (hash "03jp151d1mx11hv38xkpc7xsazv5kyh3pz9ckjb2d6a326swda0s")))

(define-public crate-x64_asm-0.1 (crate (name "x64_asm") (vers "0.1.12") (hash "1mgw6f1y61zsc682gz3yqqrassgibz3n6y39jk9ndpv8wlahzsls")))

(define-public crate-x64_asm-0.1 (crate (name "x64_asm") (vers "0.1.13") (hash "0qfd2vnywlcw88zfd31k5xy6qn8fjq4k9fg4si5zb32aqsbawki1")))

(define-public crate-x64_asm-0.1 (crate (name "x64_asm") (vers "0.1.14") (hash "084zif6vsl1gslna72q8x0v0islnpl3j48851km19f13b5725sc6")))

(define-public crate-x64_asm-0.1 (crate (name "x64_asm") (vers "0.1.15") (hash "0wnr0fbsa2xqd2dhw52jjcqh82w70y0h22wchds2h983l1xs5ca2")))

(define-public crate-x64_asm-0.1 (crate (name "x64_asm") (vers "0.1.16") (hash "149424152dbdssnnc6y44cs7vdhdlrr4lqq9438k99hc12bby303")))

(define-public crate-x64_asm-0.1 (crate (name "x64_asm") (vers "0.1.17") (hash "14hlxv0yqkipqb0y6mabd9b00845xq9r1dkdv7q9nbb2fk0yn1g4")))

(define-public crate-x64_asm-0.1 (crate (name "x64_asm") (vers "0.1.18") (hash "0ghr6am80gg64vzssphwqdi2arifb0szi396m3826gv8g1q4g06q")))

(define-public crate-x64_asm-0.1 (crate (name "x64_asm") (vers "0.1.19") (hash "1734n447vgic3kckx046c5jshj57crhxj5fdj8qsk7zxvg6s6b50")))

(define-public crate-x64_asm-0.1 (crate (name "x64_asm") (vers "0.1.20") (hash "0dmg4v1fjn614jc5vlqg34xwbn3ay3f745q8lskakvjdhg9xn8yr")))

(define-public crate-x64_asm-0.1 (crate (name "x64_asm") (vers "0.1.21") (deps (list (crate-dep (name "elf-utilities") (req "^0.1.47") (default-features #t) (kind 0)))) (hash "1zpi883zlqmq3x9b3i5vvsn78lxgjhw8rvjjxx72yyc21gz7iybg")))

(define-public crate-x64_asm-0.1 (crate (name "x64_asm") (vers "0.1.22") (deps (list (crate-dep (name "elf-utilities") (req "^0.1.47") (default-features #t) (kind 0)))) (hash "010gxhv9qf4554wiw051lgk3h2lgm9vz0nw863s6vrpli7i7hnzi")))

(define-public crate-x64_asm-0.1 (crate (name "x64_asm") (vers "0.1.23") (deps (list (crate-dep (name "elf-utilities") (req "^0.1.47") (default-features #t) (kind 0)))) (hash "0cqm5csgf0ygm3q7s22mkrpbww4i1qphb57zbwpciygsd7j9dmmk")))

(define-public crate-x64_asm-0.1 (crate (name "x64_asm") (vers "0.1.24") (deps (list (crate-dep (name "elf-utilities") (req "^0.1.47") (default-features #t) (kind 0)) (crate-dep (name "indexmap") (req "^1.3.2") (default-features #t) (kind 0)))) (hash "0nrdisj0aiiyghyxzs245vmxj6nm63h9zhn80lxy2dxn0j7qsrlk")))

(define-public crate-x64_asm-0.1 (crate (name "x64_asm") (vers "0.1.25") (deps (list (crate-dep (name "elf-utilities") (req "^0.1.47") (default-features #t) (kind 0)) (crate-dep (name "indexmap") (req "^1.3.2") (default-features #t) (kind 0)))) (hash "158gxijq43y2vm2bjy697i3r53jx8ylapg1gh7jlz9fhq9dvankn")))

(define-public crate-x64_asm-0.1 (crate (name "x64_asm") (vers "0.1.26") (deps (list (crate-dep (name "elf-utilities") (req "^0.1.47") (default-features #t) (kind 0)) (crate-dep (name "indexmap") (req "^1.3.2") (default-features #t) (kind 0)))) (hash "1nz6wziz17zjsplv8lzfilri4d99k23s69mcyvzcp2qlnl4m0ryk")))

(define-public crate-x64_asm-0.1 (crate (name "x64_asm") (vers "0.1.27") (deps (list (crate-dep (name "elf-utilities") (req "^0.1.47") (default-features #t) (kind 0)) (crate-dep (name "indexmap") (req "^1.3.2") (default-features #t) (kind 0)))) (hash "0q81x87kpwryb8ibfznjmjyx9psqfdlca6d5ix0wzyrrin5ah29x")))

(define-public crate-x64_asm-0.1 (crate (name "x64_asm") (vers "0.1.28") (deps (list (crate-dep (name "elf-utilities") (req "^0.1.47") (default-features #t) (kind 0)) (crate-dep (name "indexmap") (req "^1.3.2") (default-features #t) (kind 0)))) (hash "1cp6n2197xvlflmnz1hg98dsqb54maxg4yzcx6fx7n5rvpwaa4br")))

(define-public crate-x64_asm-0.1 (crate (name "x64_asm") (vers "0.1.29") (deps (list (crate-dep (name "elf-utilities") (req "^0.1.48") (default-features #t) (kind 0)) (crate-dep (name "indexmap") (req "^1.3.2") (default-features #t) (kind 0)))) (hash "1cli7bn5kqv9fdqf89q4w7yim6bjv2yhah97vm7nvnngxx7mlp8a")))

(define-public crate-x64_asm-0.1 (crate (name "x64_asm") (vers "0.1.30") (deps (list (crate-dep (name "elf-utilities") (req "^0.1.48") (default-features #t) (kind 0)) (crate-dep (name "indexmap") (req "^1.3.2") (default-features #t) (kind 0)))) (hash "0bdhsiqscf5cix0mafmzv6mxsf63hxv0y7wqc10k6i9mrp4ijhsq")))

(define-public crate-x64_asm-0.1 (crate (name "x64_asm") (vers "0.1.31") (deps (list (crate-dep (name "elf-utilities") (req "^0.1.48") (default-features #t) (kind 0)) (crate-dep (name "indexmap") (req "^1.3.2") (default-features #t) (kind 0)))) (hash "0s6bpr3vzhn26z8ywr9arrv3hznbynbjkhgaf45d8cyybk49n3p5")))

(define-public crate-x64_asm-0.1 (crate (name "x64_asm") (vers "0.1.32") (deps (list (crate-dep (name "elf-utilities") (req "^0.1.48") (default-features #t) (kind 0)) (crate-dep (name "indexmap") (req "^1.3.2") (default-features #t) (kind 0)))) (hash "04jbwh2qq5p2sqly0zc3s7jsk04f18pbilpmassmpr2yzy7wjaqa")))

(define-public crate-x64_asm-0.1 (crate (name "x64_asm") (vers "0.1.33") (deps (list (crate-dep (name "elf-utilities") (req "^0.1.48") (default-features #t) (kind 0)) (crate-dep (name "indexmap") (req "^1.3.2") (default-features #t) (kind 0)))) (hash "17i9izx35g67g9qryb8yakakai1f8d2byd2w82l6hsn996f71jvl")))

(define-public crate-x64_asm-0.1 (crate (name "x64_asm") (vers "0.1.34") (deps (list (crate-dep (name "elf-utilities") (req "^0.1.48") (default-features #t) (kind 0)) (crate-dep (name "indexmap") (req "^1.3.2") (default-features #t) (kind 0)))) (hash "0hhb9r9z2rzkn77jz2a2cmxxl296idc0l36im7al80xws2b918bi")))

(define-public crate-x64_asm-0.1 (crate (name "x64_asm") (vers "0.1.35") (deps (list (crate-dep (name "elf-utilities") (req "^0.1.48") (default-features #t) (kind 0)) (crate-dep (name "indexmap") (req "^1.5.1") (default-features #t) (kind 0)))) (hash "0r282p0i165jzddzjp2irnnzd1rllvlyhxj6y29ajrjqbnm7632l")))

(define-public crate-x64_asm-0.1 (crate (name "x64_asm") (vers "0.1.36") (deps (list (crate-dep (name "elf-utilities") (req "^0.1.48") (default-features #t) (kind 0)) (crate-dep (name "indexmap") (req "^1.5.1") (default-features #t) (kind 0)))) (hash "1j8v6z1z0bqlrakhwcsr53cl66q5iiwxicgcaxpyl55k3n8waw2i")))

(define-public crate-x64_asm-0.1 (crate (name "x64_asm") (vers "0.1.37") (deps (list (crate-dep (name "elf-utilities") (req "^0.1.48") (default-features #t) (kind 0)) (crate-dep (name "indexmap") (req "^1.5.1") (default-features #t) (kind 0)))) (hash "16lgsw4x3m7a9cd1rbjacy0zh2bhzbin0r1pa4qgimkx4v4h3jnd")))

(define-public crate-x64_asm-0.1 (crate (name "x64_asm") (vers "0.1.38") (deps (list (crate-dep (name "elf-utilities") (req "^0.1.50") (default-features #t) (kind 0)) (crate-dep (name "indexmap") (req "^1.5.1") (default-features #t) (kind 0)))) (hash "1i1j019xkvrhwkg2p53n8qqzfj66jbjlvv1gvcxdkxzrjp13qv4r")))

(define-public crate-x64_asm-0.1 (crate (name "x64_asm") (vers "0.1.39") (deps (list (crate-dep (name "elf-utilities") (req "^0.1.50") (default-features #t) (kind 0)) (crate-dep (name "indexmap") (req "^1.5.1") (default-features #t) (kind 0)))) (hash "07rcryqq3w7j79gmn3bs9wfrz0byckww0alkipz0c7iackcs0iys")))

(define-public crate-x64_asm-0.1 (crate (name "x64_asm") (vers "0.1.40") (deps (list (crate-dep (name "elf-utilities") (req "^0.1.50") (default-features #t) (kind 0)) (crate-dep (name "indexmap") (req "^1.5.1") (default-features #t) (kind 0)))) (hash "0xvayld05wschayicaf2iadl489nlwsvm8r3nbrx2w9g4jmcns2b")))

(define-public crate-x64_asm-0.1 (crate (name "x64_asm") (vers "0.1.41") (deps (list (crate-dep (name "elf-utilities") (req "^0.1.54") (default-features #t) (kind 0)) (crate-dep (name "indexmap") (req "^1.5.1") (default-features #t) (kind 0)))) (hash "196hk4dyps4531x0lb447f4xi5nr33922lafybbdh1a2dqdhk959")))

(define-public crate-x64_asm-0.1 (crate (name "x64_asm") (vers "0.1.43") (deps (list (crate-dep (name "elf-utilities") (req "^0.1.54") (default-features #t) (kind 0)) (crate-dep (name "indexmap") (req "^1.5.1") (default-features #t) (kind 0)))) (hash "042v92zwxhsdjihqwbr26lb3aqq120cjfjm2ql0y4aikl82jpfd3")))

(define-public crate-x64_asm-0.1 (crate (name "x64_asm") (vers "0.1.44") (deps (list (crate-dep (name "elf-utilities") (req "^0.1.67") (default-features #t) (kind 0)) (crate-dep (name "indexmap") (req "^1.5.1") (default-features #t) (kind 0)))) (hash "1biwxsd0l8fydkyhqxzy2wfkbnakazmxlb5f8i86b66iw3dj623j")))

(define-public crate-x64_asm-0.1 (crate (name "x64_asm") (vers "0.1.45") (deps (list (crate-dep (name "elf-utilities") (req "^0.1.73") (default-features #t) (kind 0)) (crate-dep (name "indexmap") (req "^1.5.1") (default-features #t) (kind 0)))) (hash "06bm53zbprj1dh6spm1vndmvxki13pjy46far4smqn0vwbsfac4b")))

(define-public crate-x64_static_linker-0.1 (crate (name "x64_static_linker") (vers "0.1.36") (deps (list (crate-dep (name "elf-utilities") (req "^0.1.48") (default-features #t) (kind 0)))) (hash "12ycvbf1633pa0ksf0ylp663807igivndvscba3d6csqgl5z84n5")))

(define-public crate-x64_static_linker-0.1 (crate (name "x64_static_linker") (vers "0.1.37") (deps (list (crate-dep (name "elf-utilities") (req "^0.1.48") (default-features #t) (kind 0)))) (hash "1l9dx1cwff5c5ynrkr963j69pa76kgdzzni0llx6y8pb4gi455yw")))

(define-public crate-x64_static_linker-0.1 (crate (name "x64_static_linker") (vers "0.1.38") (deps (list (crate-dep (name "elf-utilities") (req "^0.1.48") (default-features #t) (kind 0)))) (hash "0yjwy48s1hpcsh4xgl3p6wvpbrxcrd383xf8kvkszpspjhipz3b8")))

(define-public crate-x64_static_linker-0.1 (crate (name "x64_static_linker") (vers "0.1.39") (deps (list (crate-dep (name "elf-utilities") (req "^0.1.48") (default-features #t) (kind 0)))) (hash "033gmdnxmv16gjlpn0134krpapk4fmvvp57j5lpmafk1qc4j84wp")))

(define-public crate-x64_static_linker-0.1 (crate (name "x64_static_linker") (vers "0.1.40") (deps (list (crate-dep (name "elf-utilities") (req "^0.1.49") (default-features #t) (kind 0)))) (hash "0bakvjj8ghw5j443h7afj6gb6g9xfz2vlg5whb5xmfcckf7kdxzv")))

(define-public crate-x64_static_linker-0.1 (crate (name "x64_static_linker") (vers "0.1.41") (deps (list (crate-dep (name "elf-utilities") (req "^0.1.51") (default-features #t) (kind 0)))) (hash "0wc45npsg93qwbbjy5j7n8f8ljpla0aid56sf78mqlxjh6zssaa7")))

(define-public crate-x64_static_linker-0.1 (crate (name "x64_static_linker") (vers "0.1.42") (deps (list (crate-dep (name "elf-utilities") (req "^0.1.56") (default-features #t) (kind 0)))) (hash "0mlywr2ciq557m8p06xm6p5ffwsg2c53958nlanfjh1mzwgyhzgj")))

(define-public crate-x64_static_linker-0.1 (crate (name "x64_static_linker") (vers "0.1.43") (deps (list (crate-dep (name "elf-utilities") (req "^0.1.73") (default-features #t) (kind 0)))) (hash "1xh5jsnk5ki52n25vsim0mphsrziycvcyl1b4qv9sww334r17jm0")))

(define-public crate-x64_static_linker-0.1 (crate (name "x64_static_linker") (vers "0.1.44") (deps (list (crate-dep (name "elf-utilities") (req "^0.1.73") (default-features #t) (kind 0)))) (hash "0lrkw6b2bx13gjaiwi31cwvc0hrf7mfpdin3pr9jyhy1pk2g3pg9")))

