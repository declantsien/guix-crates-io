(define-module (crates-io x6 #{4a}#) #:use-module (crates-io))

(define-public crate-x64asm-0.1 (crate (name "x64asm") (vers "0.1.0") (hash "1a61gy3jlfzk9imqs9hxgpa59hbvkjbi7zbpflxckmgl92d941l1")))

(define-public crate-x64asm-0.1 (crate (name "x64asm") (vers "0.1.1") (hash "0hl6acbdkskzsafwh710wzzlf5qgbhj3kc58m3d5kl7kjrv21a5z")))

(define-public crate-x64asm-0.1 (crate (name "x64asm") (vers "0.1.2") (hash "1rvclp2i1y07xfmbs2ygy4gdvb4bpsypnrvvvjjbif23xij1srp8")))

(define-public crate-x64asm-0.1 (crate (name "x64asm") (vers "0.1.3") (hash "030sapgrp16kya42m531hkaqw78rw1lqccr94q5g15w75h8w7my9")))

(define-public crate-x64asm-0.1 (crate (name "x64asm") (vers "0.1.4") (hash "1vhwx5rgvbg7kmfv8s3jkpi3p6y93jy47cgx4q14d4g6n5if8gjq")))

(define-public crate-x64asm-0.1 (crate (name "x64asm") (vers "0.1.5") (hash "1ykjzckw4rgy1rnmnikamm0wr67mikb2h6awz6bv4gz4jnra78mc")))

(define-public crate-x64asm-0.1 (crate (name "x64asm") (vers "0.1.6") (hash "1l89vsc3sdnyqdv4v0q6g5z43l55ycbwv9dyf3a4jc0d92si7l1n")))

(define-public crate-x64asm-0.1 (crate (name "x64asm") (vers "0.1.7") (hash "178alwyzbi3zy0n2zvb71knqiwscdm3sa67w9j5pxl8835xcri5q")))

(define-public crate-x64asm-0.1 (crate (name "x64asm") (vers "0.1.9") (hash "1af5l1lr2cak9wnr6mksqf8h509zwqcja1svhmp7gy8kabic30da")))

(define-public crate-x64asm-0.2 (crate (name "x64asm") (vers "0.2.0") (hash "1cqjqi128rc4d4aggi59lqxa8d589lxqr0p26cx9nhh993l88pfv")))

