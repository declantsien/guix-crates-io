(define-module (crates-io pr gr) #:use-module (crates-io))

(define-public crate-prgrs-0.2 (crate (name "prgrs") (vers "0.2.0") (deps (list (crate-dep (name "terminal") (req "^0.2") (features (quote ("crossterm-backend"))) (default-features #t) (kind 0)))) (hash "1ixv33dj6jx4qsxx8k6k3y1fhpvw7l8s4ha7yf0isfpfbja9dlcc") (yanked #t)))

(define-public crate-prgrs-0.3 (crate (name "prgrs") (vers "0.3.0") (deps (list (crate-dep (name "terminal") (req "^0.2") (features (quote ("crossterm-backend"))) (default-features #t) (kind 0)))) (hash "0xcbm4lyqimvfyqayxgifq0sjbkwc2vg44ra1pk0527zdic0m7j6") (yanked #t)))

(define-public crate-prgrs-0.3 (crate (name "prgrs") (vers "0.3.1") (deps (list (crate-dep (name "terminal") (req "^0.2") (features (quote ("crossterm-backend"))) (default-features #t) (kind 0)))) (hash "1zicz2199w2gampwi0jgbq1z9pafmxi6c1cyq9g1n63yn1xbpzfh") (yanked #t)))

(define-public crate-prgrs-0.4 (crate (name "prgrs") (vers "0.4.0") (deps (list (crate-dep (name "terminal") (req "^0.2") (features (quote ("crossterm-backend"))) (default-features #t) (kind 0)))) (hash "1afkiypp4agcb01px2snm11n7ky4ws4p7px3074mxv509052xki5") (yanked #t)))

(define-public crate-prgrs-0.5 (crate (name "prgrs") (vers "0.5.0") (deps (list (crate-dep (name "terminal") (req "^0.2") (features (quote ("crossterm-backend"))) (default-features #t) (kind 0)) (crate-dep (name "terminal_size") (req "^0.1.10") (default-features #t) (kind 0)))) (hash "093fiy5d31iafmysfqgbc30bn74mdhn811jpxfw8cj5093dspqah")))

(define-public crate-prgrs-0.5 (crate (name "prgrs") (vers "0.5.1") (deps (list (crate-dep (name "terminal") (req "^0.2") (features (quote ("crossterm-backend"))) (default-features #t) (kind 0)) (crate-dep (name "terminal_size") (req "^0.1.10") (default-features #t) (kind 0)))) (hash "02923n27p6r96w3pqb75ywvvfhm8prqz755f4rlniqzcvv8lgyji")))

(define-public crate-prgrs-0.5 (crate (name "prgrs") (vers "0.5.2") (deps (list (crate-dep (name "terminal") (req "^0.2") (features (quote ("crossterm-backend"))) (default-features #t) (kind 0)) (crate-dep (name "terminal_size") (req "^0.1.10") (default-features #t) (kind 0)))) (hash "15g02745wcshdi29pfws1ycay62g4nmvh03s2yam7yaj3xdh4jf7")))

(define-public crate-prgrs-0.5 (crate (name "prgrs") (vers "0.5.3") (deps (list (crate-dep (name "terminal") (req "^0.2") (features (quote ("crossterm-backend"))) (default-features #t) (kind 0)) (crate-dep (name "terminal_size") (req "^0.1.10") (default-features #t) (kind 0)))) (hash "1x0zwg17nmz3rzawnr8j6pz264bnq1ip42qszhp9pb9iv98kd5pi")))

(define-public crate-prgrs-0.5 (crate (name "prgrs") (vers "0.5.4") (deps (list (crate-dep (name "terminal") (req "^0.2") (features (quote ("crossterm-backend"))) (default-features #t) (kind 0)) (crate-dep (name "terminal_size") (req "^0.1.10") (default-features #t) (kind 0)))) (hash "1rsmszpxm0yi68by8i20258fya43agawacsikh8zd9c0jxcxzzaw")))

(define-public crate-prgrs-0.6 (crate (name "prgrs") (vers "0.6.0") (deps (list (crate-dep (name "terminal_size") (req "^0.1.10") (default-features #t) (kind 0)))) (hash "12kbjzqcc67yh2cj7hh40y137cc42gqyl0qa5yrkpaiszrmdidj3")))

(define-public crate-prgrs-0.6 (crate (name "prgrs") (vers "0.6.1") (deps (list (crate-dep (name "terminal_size") (req "^0.1.10") (default-features #t) (kind 0)))) (hash "1jj34f796rxc0ykxy4h5mh51f9bljivqw80j90jb95b1b7rvi1dd")))

(define-public crate-prgrs-0.6 (crate (name "prgrs") (vers "0.6.2") (deps (list (crate-dep (name "terminal_size") (req "^0.1.10") (default-features #t) (kind 0)))) (hash "1gm4mky2jrx5iikmik4hapyfysnjkwhb2kvkxyjwdm2gf1i1p754")))

(define-public crate-prgrs-0.6 (crate (name "prgrs") (vers "0.6.3") (deps (list (crate-dep (name "terminal_size") (req "^0.1.10") (default-features #t) (kind 0)))) (hash "1y5rl922hpgs3aa9bk0wzx2nm76qi7y83byywdzaxnxmjibipbg9")))

(define-public crate-prgrs-0.6 (crate (name "prgrs") (vers "0.6.4") (deps (list (crate-dep (name "terminal_size") (req "^0.1") (default-features #t) (kind 0)))) (hash "1sb25hm22rbp3m6nbqghz8jdpi6fxg7ykhd7y9v7gdd5lpaj6nk5")))

