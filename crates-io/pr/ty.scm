(define-module (crates-io pr ty) #:use-module (crates-io))

(define-public crate-prty-0.1 (crate (name "prty") (vers "0.1.1") (deps (list (crate-dep (name "anycat") (req "^0.1.1") (default-features #t) (kind 0)) (crate-dep (name "bit-set") (req "^0.4.0") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^2.23.1") (default-features #t) (kind 0)) (crate-dep (name "compare") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "env_logger") (req "^0.5.6") (default-features #t) (kind 0)) (crate-dep (name "git-build-version") (req "^0.1.2") (default-features #t) (kind 1)) (crate-dep (name "log") (req "^0.4.1") (default-features #t) (kind 0)) (crate-dep (name "measure_time") (req "^0.6.0") (default-features #t) (kind 0)) (crate-dep (name "peg") (req "^0.5.4") (default-features #t) (kind 1)) (crate-dep (name "prefixopt") (req "^0.4.0") (default-features #t) (kind 0)) (crate-dep (name "prefixopt_derive") (req "^0.4.0") (default-features #t) (kind 0)) (crate-dep (name "prty_bitgraph") (req "^0.1.1") (default-features #t) (kind 0)) (crate-dep (name "prty_fmtmodifier") (req "^0.1.1") (default-features #t) (kind 0)) (crate-dep (name "quickcheck") (req "^0.6") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.4.2") (default-features #t) (kind 0)) (crate-dep (name "typenum") (req "^1.10.0") (default-features #t) (kind 0)))) (hash "0hn7r1qp57rhww6x66k2ghlzmqp51g9dcw9q8m7jqa1fhw5p3rgy")))

(define-public crate-prty_bitgraph-0.1 (crate (name "prty_bitgraph") (vers "0.1.0") (deps (list (crate-dep (name "fnv") (req "^1.0.6") (default-features #t) (kind 0)) (crate-dep (name "prty_fmtmodifier") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "0ak7x68y6wzyg3nrkrrw92rkx30v18mnh0lslbanq4nidyvbpg08")))

(define-public crate-prty_bitgraph-0.1 (crate (name "prty_bitgraph") (vers "0.1.1") (deps (list (crate-dep (name "fnv") (req "^1.0.6") (default-features #t) (kind 0)) (crate-dep (name "prty_fmtmodifier") (req "^0.1.1") (default-features #t) (kind 0)))) (hash "10y0yd51bdxnyzq4n6srxdfzjl8ni5940nyrby6jlribw6ynmwan")))

(define-public crate-prty_fmtmodifier-0.1 (crate (name "prty_fmtmodifier") (vers "0.1.0") (hash "0adz8gjcjhmb8gldc03j7bh6jzizg8fjypv9fp4cvdizc399mv12")))

(define-public crate-prty_fmtmodifier-0.1 (crate (name "prty_fmtmodifier") (vers "0.1.1") (hash "0n1s7mgi3wsksscqpv3aq2d6fhl9hp5fh97vcprpqpw5s9jhn2dr")))

