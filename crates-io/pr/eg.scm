(define-module (crates-io pr eg) #:use-module (crates-io))

(define-public crate-pregel-rs-0.0.1 (crate (name "pregel-rs") (vers "0.0.1") (deps (list (crate-dep (name "duckdb") (req "^0.7.1") (features (quote ("bundled"))) (default-features #t) (kind 0)) (crate-dep (name "polars") (req "^0.28.0") (features (quote ("lazy"))) (default-features #t) (kind 0)))) (hash "1l56k0j40j7fa5ppj7w9n9rchsgr8zsjprzggpanf59ch9gmffma")))

(define-public crate-pregel-rs-0.0.2 (crate (name "pregel-rs") (vers "0.0.2") (deps (list (crate-dep (name "duckdb") (req "^0.7.1") (default-features #t) (kind 0)) (crate-dep (name "duckdb") (req "^0.7.1") (features (quote ("bundled"))) (default-features #t) (kind 2)) (crate-dep (name "polars") (req "^0.28.0") (features (quote ("lazy"))) (default-features #t) (kind 0)))) (hash "0kbrms9h3q40625mkis3wx7ki7qsprdj3a9027rw7zns1j8ky67h")))

(define-public crate-pregel-rs-0.0.3 (crate (name "pregel-rs") (vers "0.0.3") (deps (list (crate-dep (name "polars") (req "^0.28.0") (features (quote ("lazy"))) (default-features #t) (kind 0)))) (hash "1b4ylrjhhmcjaywcd4lyvkgkpdkqvs1vpg6ghmjxxq9z2f0p2332")))

(define-public crate-pregel-rs-0.0.4 (crate (name "pregel-rs") (vers "0.0.4") (deps (list (crate-dep (name "polars") (req "^0.28.0") (features (quote ("lazy"))) (default-features #t) (kind 0)))) (hash "0b8wg14mzy5yv8gak496n5bi71r2km1yqw35sr025cdyf5b55vgn")))

(define-public crate-pregel-rs-0.0.5 (crate (name "pregel-rs") (vers "0.0.5") (deps (list (crate-dep (name "polars") (req "^0.28.0") (features (quote ("lazy"))) (default-features #t) (kind 0)))) (hash "1yd881a1jpq60fzg3j3bv1n6nfbm37g6vpv12np1kcdwpdl1cm70")))

(define-public crate-pregel-rs-0.0.6 (crate (name "pregel-rs") (vers "0.0.6") (deps (list (crate-dep (name "polars") (req "^0.29.0") (features (quote ("lazy"))) (default-features #t) (kind 0)))) (hash "0426z4dhr85xy89r512grxxxz7mb9pcmnjaj08ibhv875g25fk4i")))

(define-public crate-pregel-rs-0.0.7 (crate (name "pregel-rs") (vers "0.0.7") (deps (list (crate-dep (name "polars") (req "^0.29.0") (features (quote ("lazy"))) (default-features #t) (kind 0)))) (hash "15qpyx7ssyffadjfvqswy6b83rrxacmv9zv813qbrgg5f34xmph9")))

(define-public crate-pregel-rs-0.0.8 (crate (name "pregel-rs") (vers "0.0.8") (deps (list (crate-dep (name "polars") (req "^0.29.0") (features (quote ("lazy" "streaming"))) (default-features #t) (kind 0)))) (hash "0qbgp9h2d8zfbbanf2q0gyjl3c1ms05llyi8b8vvznca7011zkc5")))

(define-public crate-pregel-rs-0.0.9 (crate (name "pregel-rs") (vers "0.0.9") (deps (list (crate-dep (name "polars") (req "^0.29.0") (features (quote ("lazy" "streaming" "parquet" "performant" "chunked_ids"))) (default-features #t) (kind 0)))) (hash "0qvvv5wh18yxgxybincj0g9nw99c90nzvl9rf421k3y888pap3rm")))

(define-public crate-pregel-rs-0.0.10 (crate (name "pregel-rs") (vers "0.0.10") (deps (list (crate-dep (name "polars") (req "^0.30.0") (features (quote ("lazy" "streaming" "parquet" "performant" "chunked_ids"))) (default-features #t) (kind 0)))) (hash "0q44wvxcc2pp9pkbcn111sqim4g6hxz2xk19fmm5qzha41k0lbcd")))

(define-public crate-pregel-rs-0.0.11 (crate (name "pregel-rs") (vers "0.0.11") (deps (list (crate-dep (name "polars") (req "^0.30.0") (features (quote ("lazy" "streaming" "parquet" "performant" "chunked_ids"))) (default-features #t) (kind 0)))) (hash "1w5wlm6564jlwbs19cfwa13az5nas053cm8h5qis2lkhzi70l90p")))

(define-public crate-pregel-rs-0.0.12 (crate (name "pregel-rs") (vers "0.0.12") (deps (list (crate-dep (name "polars") (req "^0.30.0") (features (quote ("lazy" "streaming" "parquet" "performant" "chunked_ids"))) (default-features #t) (kind 0)))) (hash "18xz8ahgjiihvs5cfg371s9dbpcyybzfr1h4c4mp8if3dax4m0vc")))

(define-public crate-pregel-rs-0.0.13 (crate (name "pregel-rs") (vers "0.0.13") (deps (list (crate-dep (name "polars") (req "^0.30.0") (features (quote ("lazy" "streaming" "parquet" "performant" "chunked_ids"))) (default-features #t) (kind 0)))) (hash "1v2znm7iy1579hnmkmqimv0d14alqa5g4m4mcb68f687fw81qkby")))

