(define-module (crates-io pr ut) #:use-module (crates-io))

(define-public crate-prutoipa-0.0.0 (crate (name "prutoipa") (vers "0.0.0") (hash "0bqa1gflapj34imfzfwr772d5i4pz4zcnwz2220w0d47kxmi06bc")))

(define-public crate-prutoipa-build-0.0.0 (crate (name "prutoipa-build") (vers "0.0.0") (hash "1yg0bgm1jsm1a8g32lzha2l7h0qc5x10ak8iigv1i1v27mk9zkzg")))

(define-public crate-prutoipa-build-0.1 (crate (name "prutoipa-build") (vers "0.1.0") (deps (list (crate-dep (name "heck") (req "^0.4.1") (default-features #t) (kind 0)) (crate-dep (name "prost") (req "^0.11.7") (default-features #t) (kind 0)) (crate-dep (name "prost-types") (req "^0.11.7") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.38") (default-features #t) (kind 0)))) (hash "1ci91fpdi6jl5ah2fl5bv3gsrxvd67qj4yrwjyhj5raq4wymk3g7")))

(define-public crate-prutoipa-build-0.1 (crate (name "prutoipa-build") (vers "0.1.1") (deps (list (crate-dep (name "heck") (req "^0.4.1") (default-features #t) (kind 0)) (crate-dep (name "prost") (req "^0.11.7") (default-features #t) (kind 0)) (crate-dep (name "prost-types") (req "^0.11.7") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.38") (default-features #t) (kind 0)))) (hash "0dziqdc4dys0hwmcy7mdc1ygswhmkfr1r10rr6kryp9msddi4z6f")))

(define-public crate-prutoipa-build-0.1 (crate (name "prutoipa-build") (vers "0.1.2") (deps (list (crate-dep (name "heck") (req "^0.4.1") (default-features #t) (kind 0)) (crate-dep (name "prost") (req "^0.11.7") (default-features #t) (kind 0)) (crate-dep (name "prost-types") (req "^0.11.7") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.38") (default-features #t) (kind 0)))) (hash "0kiw4pyfwshcxdrwjv5h5fwrfjd3mxigjcgns26m6srcjs22x1p3")))

(define-public crate-prutoipa-build-0.1 (crate (name "prutoipa-build") (vers "0.1.3") (deps (list (crate-dep (name "heck") (req "^0.4.1") (default-features #t) (kind 0)) (crate-dep (name "prost") (req "^0.11.7") (default-features #t) (kind 0)) (crate-dep (name "prost-types") (req "^0.11.7") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.38") (default-features #t) (kind 0)))) (hash "0knddnccf46ycsy4md2lcbv87di1mbyln20jh2c0lc0mjym9scj2")))

(define-public crate-prutoipa-build-0.1 (crate (name "prutoipa-build") (vers "0.1.4") (deps (list (crate-dep (name "heck") (req "^0.4.1") (default-features #t) (kind 0)) (crate-dep (name "prost") (req "^0.11.7") (default-features #t) (kind 0)) (crate-dep (name "prost-types") (req "^0.11.7") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.38") (default-features #t) (kind 0)))) (hash "1vs1s69njl316cln6g3gki6sdcm6nq0b6bf0sz6a239kwrd15q8m")))

