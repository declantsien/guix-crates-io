(define-module (crates-io pr ou) #:use-module (crates-io))

(define-public crate-proudcat-0.0.1 (crate (name "proudcat") (vers "0.0.1") (hash "00h3p2qjhyn2xgss20qlhs8mgicx0lq5zrw7nzfanijn34sz99f5")))

(define-public crate-proudcat-0.1 (crate (name "proudcat") (vers "0.1.0") (hash "0js1wiyvb5bmybzdzba620afjld35b2sf45kfny93hwc8hvnlg50")))

(define-public crate-proudcat-0.1 (crate (name "proudcat") (vers "0.1.1") (hash "0vbrbvz07063lrlg8psqnb51cabd84js8zyl2lcpk451sgk6lm19")))

(define-public crate-proudcat-0.1 (crate (name "proudcat") (vers "0.1.2") (hash "16wc0p2f1k6613y44fyn2c12pcnq76basij4ynywrqizmyi6ca2a")))

(define-public crate-proudcat-0.2 (crate (name "proudcat") (vers "0.2.0") (deps (list (crate-dep (name "atty") (req "^0.2.14") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.3.1") (default-features #t) (kind 0)) (crate-dep (name "term_size") (req "^0.3.1") (default-features #t) (kind 0)))) (hash "0gaqdjns4m1p20pq8172h7l37fxn05xw1zq2gldq9ahin9rx7ci7")))

(define-public crate-prout-0.2 (crate (name "prout") (vers "0.2.0") (deps (list (crate-dep (name "chumsky") (req "^0.8") (default-features #t) (kind 0)))) (hash "1zyphsn76drh2z334w855cp4dsancsgqa4dhy1dzvj39ywy0g3fn") (yanked #t)))

(define-public crate-prout-0.3 (crate (name "prout") (vers "0.3.0") (deps (list (crate-dep (name "chumsky") (req "^0.8") (default-features #t) (kind 0)))) (hash "1dgi6carsw9vp3q29lw3imgqjl0p1p85viwg25g39g0ay3gsf20a") (yanked #t)))

(define-public crate-prout-0.3 (crate (name "prout") (vers "0.3.5") (hash "1p1w5ddmzqdkclw2j0hbciaqlsymwl3b2v4lnc42qbkj37z8klva")))

