(define-module (crates-io pr ng) #:use-module (crates-io))

(define-public crate-prng-0.1 (crate (name "prng") (vers "0.1.0") (deps (list (crate-dep (name "num_cpus") (req "^1.6") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "rng_trait") (req "^0.1") (default-features #t) (kind 0)))) (hash "1mym8p3jwrmnqlbbwkwm5nbvq8h7k4cxg5fw29vmgl3w2i0g79ih")))

(define-public crate-prng-0.1 (crate (name "prng") (vers "0.1.1") (deps (list (crate-dep (name "num_cpus") (req "^1.7") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "rng_trait") (req "^0.1") (default-features #t) (kind 0)))) (hash "1ikcjzyql72jxa5x6g9byy5vs8zvyhl024zyzm8xfi8psbkw48x9")))

(define-public crate-prng_mt-0.1 (crate (name "prng_mt") (vers "0.1.0") (hash "0dy907nsnq9f0fbn8dpd6y59zah7rmb9j780zrr1326vngjz40n1")))

