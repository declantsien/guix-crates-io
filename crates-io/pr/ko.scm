(define-module (crates-io pr ko) #:use-module (crates-io))

(define-public crate-prkorm-0.1 (crate (name "prkorm") (vers "0.1.0") (deps (list (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.33") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2.0.32") (default-features #t) (kind 0)))) (hash "0ba44fgyxa3wv2j6v1r7qgqnr8a15s53f2l40xwrxzp3mcdppw1z")))

(define-public crate-prkorm-0.2 (crate (name "prkorm") (vers "0.2.0") (deps (list (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.33") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2.0.32") (default-features #t) (kind 0)))) (hash "0wiwaq7ixqlawj54pqzskwrsqqaqf8p36md2b56d15s78szdcnlp")))

(define-public crate-prkorm-0.3 (crate (name "prkorm") (vers "0.3.0") (deps (list (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.33") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2.0.32") (default-features #t) (kind 0)))) (hash "0r1kn1k5h64y10rckhzad8n8m0vg2b609sza6ni84pvzqzx0d6m2")))

(define-public crate-prkorm-0.4 (crate (name "prkorm") (vers "0.4.0") (deps (list (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.33") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2.0.32") (default-features #t) (kind 0)))) (hash "1ljzsz4byl1zlsvhsc3z3rg62qv50q866p3mar9ic23zhm4hfdpp")))

(define-public crate-prkorm-0.5 (crate (name "prkorm") (vers "0.5.0") (deps (list (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.33") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2.0.32") (default-features #t) (kind 0)))) (hash "1ix3as1axa4zkhmjz54axcy9n9g46x5vl8hxnkavb2ws8a48l1hb")))

(define-public crate-prkorm-0.5 (crate (name "prkorm") (vers "0.5.1") (deps (list (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.33") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2.0.32") (default-features #t) (kind 0)))) (hash "0awp8f97jkwvzg11qfn4nksv3gfjbaz26fhs3ldch88ggban6kl8")))

(define-public crate-prkorm-0.5 (crate (name "prkorm") (vers "0.5.2") (deps (list (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.33") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2.0.32") (default-features #t) (kind 0)))) (hash "0ls47hvrkcfj3ggq3ik438sryqrwjwnh223bmay3qsklq6k1n4k0")))

(define-public crate-prkorm-0.5 (crate (name "prkorm") (vers "0.5.3") (deps (list (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.33") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2.0.32") (default-features #t) (kind 0)))) (hash "0xi31y7wssjx4sbg2z3bsla0pbyk9634rka03id02dfswr5avad6")))

