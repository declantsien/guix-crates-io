(define-module (crates-io pr _m) #:use-module (crates-io))

(define-public crate-pr_mod-1 (crate (name "pr_mod") (vers "1.0.0") (hash "07n5fikv118di20bq0gl8mw7hf3mp65kan3inbv3jwa328j591zs")))

(define-public crate-pr_mod-1 (crate (name "pr_mod") (vers "1.1.0") (hash "1628zyv5m8rvmiiwv4hfxjf68d1jxb38xfprm46aaixyrij7hfhm")))

(define-public crate-pr_mod-1 (crate (name "pr_mod") (vers "1.1.1") (hash "0m7qd4csp1dggqcz6270hwrl1nhw574mm9whp9z8jlsqavg0w5nf")))

(define-public crate-pr_mod-1 (crate (name "pr_mod") (vers "1.1.2") (hash "1wf6vigg42k9hwn86lcfp0idnq9268c8djanxw6nd50a4gqf528w")))

(define-public crate-pr_mod-1 (crate (name "pr_mod") (vers "1.1.3") (hash "1c8x9j3vl8l2bwmzizyb7yy3r43v8zqqi0jf3vfqd8bx9vmaim41")))

