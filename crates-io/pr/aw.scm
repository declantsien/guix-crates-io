(define-module (crates-io pr aw) #:use-module (crates-io))

(define-public crate-prawnypool-0.1 (crate (name "prawnypool") (vers "0.1.0") (hash "1xxh8wgzmzvknicljgni0inxs58nha3vf7hp3zvhmf6c3gdm15l0")))

(define-public crate-prawnypool-0.1 (crate (name "prawnypool") (vers "0.1.1") (hash "1x7dappqhn9gn66b94mhq1k2pf41xmzzq7w3zfzx8yg6mazi5v5b")))

(define-public crate-prawnypool-0.1 (crate (name "prawnypool") (vers "0.1.2") (hash "1zykhxd57rlcfcg0d6k220bsfblhyqf421773vjchyvzn2c4jplx")))

