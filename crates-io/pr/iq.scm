(define-module (crates-io pr iq) #:use-module (crates-io))

(define-public crate-priq-0.1 (crate (name "priq") (vers "0.1.0") (deps (list (crate-dep (name "bencher") (req "^0.1.5") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.8.4") (default-features #t) (kind 0)))) (hash "032m80al3k6m944dvd6h0dhhpwk16hxhga0fm5kygfbpi5ddgx8m") (yanked #t)))

(define-public crate-priq-0.1 (crate (name "priq") (vers "0.1.1") (deps (list (crate-dep (name "bencher") (req "^0.1.5") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.8.4") (default-features #t) (kind 0)))) (hash "0z7cbwkmd5bqqcrryicl1y62zlrjjirhis5ir67ajv0587jmx0yk")))

(define-public crate-priq-0.1 (crate (name "priq") (vers "0.1.5") (deps (list (crate-dep (name "bencher") (req "^0.1.5") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.8.4") (default-features #t) (kind 0)))) (hash "158hsycv0v3yz8p14yxbs3paqz8s7kyk0x822pqwcigvaynd71k2")))

(define-public crate-priq-0.1 (crate (name "priq") (vers "0.1.6") (deps (list (crate-dep (name "bencher") (req "^0.1.5") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.8.4") (default-features #t) (kind 0)))) (hash "0na1rwdqsc77ffa1lnjmhi0wdr0cshg61hb8ncs4zfgkr43j6d1a")))

(define-public crate-priq-0.2 (crate (name "priq") (vers "0.2.0") (deps (list (crate-dep (name "bencher") (req "^0.1.5") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.8.4") (default-features #t) (kind 0)))) (hash "1020p52qfd568qd41sy5gnm0h15ggv71iv61y02izbhsq419z3qs")))

