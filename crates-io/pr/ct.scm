(define-module (crates-io pr ct) #:use-module (crates-io))

(define-public crate-prctl-0.0.1 (crate (name "prctl") (vers "0.0.1") (hash "0ww3h7szgpjhwn6xpmclgpcpznlgif4qa3jzrvg68f3vn8a15wxm") (features (quote (("root_test") ("not_travis"))))))

(define-public crate-prctl-0.0.2 (crate (name "prctl") (vers "0.0.2") (hash "0isbv1g5gxkzswzbmxbg7pfqb5sgwmas9az8x6j3b5xgmy0c632k") (features (quote (("root_test") ("not_travis"))))))

(define-public crate-prctl-0.1 (crate (name "prctl") (vers "0.1.0") (hash "0h9m4rcibw9gyix8mbf57bn05xiwr8639f5rnbiw5089xwxkcd2i") (features (quote (("root_test") ("not_travis"))))))

(define-public crate-prctl-0.1 (crate (name "prctl") (vers "0.1.1") (hash "0c183lw16wc24fb0lqi1d70bkbpfpaxxfmls2fzd7prykzz7sbha") (features (quote (("root_test") ("not_travis"))))))

(define-public crate-prctl-0.2 (crate (name "prctl") (vers "0.2.0") (hash "1gpknzwnrwaqrl40x4yc20zb048b05gp2m4cqzlv7hm4qaavk49g") (features (quote (("root_test") ("not_travis"))))))

(define-public crate-prctl-1 (crate (name "prctl") (vers "1.0.0") (deps (list (crate-dep (name "libc") (req "*") (default-features #t) (kind 0)) (crate-dep (name "nix") (req "*") (default-features #t) (kind 0)))) (hash "0lkgnid3sjfbqf3sbcgyihlw80a6n9l6m0n23b7f5pm927qk96h5") (features (quote (("root_test") ("not_travis"))))))

