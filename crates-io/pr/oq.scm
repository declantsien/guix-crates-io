(define-module (crates-io pr oq) #:use-module (crates-io))

(define-public crate-proq-0.0.0 (crate (name "proq") (vers "0.0.0") (hash "1ri9qf340hnvfqvwdyh68zz319xkgp9fk73pyisyyl0baa13mqzz")))

(define-public crate-proq-0.1 (crate (name "proq") (vers "0.1.0") (deps (list (crate-dep (name "chrono") (req "^0.4.10") (default-features #t) (kind 0)) (crate-dep (name "failure") (req "^0.1.6") (default-features #t) (kind 0)) (crate-dep (name "futures") (req "^0.3.1") (default-features #t) (kind 2)) (crate-dep (name "http") (req "^0.1.21") (default-features #t) (kind 0)) (crate-dep (name "once_cell") (req "^1.2.0") (default-features #t) (kind 2)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.44") (default-features #t) (kind 0)) (crate-dep (name "serde_urlencoded") (req "^0.6.1") (default-features #t) (kind 0)) (crate-dep (name "surf") (req "^1.0.3") (default-features #t) (kind 0)) (crate-dep (name "url") (req "^1.7") (default-features #t) (kind 0)) (crate-dep (name "url_serde") (req "^0.2.0") (default-features #t) (kind 0)))) (hash "1iija6f982qjc0ac3cqp2bcb07gpvn1s0ba4lsgi4yn576y8svz8")))

(define-public crate-proqnt-0.1 (crate (name "proqnt") (vers "0.1.0") (deps (list (crate-dep (name "arrayvec") (req "^0.7") (kind 0)) (crate-dep (name "proptest") (req "^1.1") (default-features #t) (kind 2)))) (hash "1m4r4xqnw5j0dn1gclhi1akrivcv94f2bmryajbrs0qkby6b6c8n") (features (quote (("std") ("default" "std"))))))

(define-public crate-proquint-0.1 (crate (name "proquint") (vers "0.1.0") (hash "1yq1kv6lk1pxm4d46kw0409gmyz4445i6xk072al12pxwjvivlnj")))

