(define-module (crates-io pr um) #:use-module (crates-io))

(define-public crate-prumpt-0.1 (crate (name "prumpt") (vers "0.1.1") (deps (list (crate-dep (name "indicatif") (req "^0.17.8") (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.11") (features (quote ("json" "blocking"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "05w8nrs9kvgancwvfcd635x26wff32nn58bz1plaz4f6jfaq4cnr")))

(define-public crate-prumpt-0.1 (crate (name "prumpt") (vers "0.1.2") (deps (list (crate-dep (name "indicatif") (req "^0.17.8") (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.11") (features (quote ("json" "blocking"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "1nv74xbzvycsn72547cwfsnmn7nn7hgj16dxnp1ncgvgjwgznq32")))

