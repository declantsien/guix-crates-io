(define-module (crates-io pr e_) #:use-module (crates-io))

(define-public crate-pre_hf-0.0.0 (crate (name "pre_hf") (vers "0.0.0") (deps (list (crate-dep (name "wee_alloc") (req "^0.4.5") (default-features #t) (kind 0)))) (hash "1fm54k0r46cjks5bqsp4wdnfnzmw3shrwqz0li8gib9hi63v2arl")))

(define-public crate-pre_hf-0.0.1 (crate (name "pre_hf") (vers "0.0.1") (deps (list (crate-dep (name "wee_alloc") (req "^0.4.5") (default-features #t) (kind 0)))) (hash "0w98x4andgnyf6lq6wy0z388h39360rymfbz44i2ffvqx09qvqz3")))

