(define-module (crates-io pr sq) #:use-module (crates-io))

(define-public crate-prsqlite-0.1 (crate (name "prsqlite") (vers "0.1.0") (deps (list (crate-dep (name "anyhow") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "rusqlite") (req "^0.28.0") (default-features #t) (kind 2)) (crate-dep (name "tempfile") (req "^3") (default-features #t) (kind 2)))) (hash "1ll6i9cb2hv4f4cki06710mzgldw7ld786d6979jrbk7k138i9wv")))

