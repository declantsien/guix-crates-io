(define-module (crates-io pr gl) #:use-module (crates-io))

(define-public crate-prgl-0.0.1 (crate (name "prgl") (vers "0.0.1") (hash "0xnxdxnzb62izabjwganh2daxm90hg638x8r3m2mz6nn1b5yab7n")))

(define-public crate-prgl-0.0.2 (crate (name "prgl") (vers "0.0.2") (hash "1pi8migwcds7n9pjkvpyxlpnscv40lgyshwnlfkvc358f8vjpsbi")))

(define-public crate-prgl-0.0.3 (crate (name "prgl") (vers "0.0.3") (hash "0mfjfqxwyspv6ybmymhwmnf54q54h8wwa93mw0hx98dc165yjiw8")))

(define-public crate-prgl-0.0.4 (crate (name "prgl") (vers "0.0.4") (hash "1ix1hpv9wcz2j578wmszn2bv9jsfd2glrnyr215j580xraywwpl5")))

