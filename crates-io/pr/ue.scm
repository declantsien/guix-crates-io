(define-module (crates-io pr ue) #:use-module (crates-io))

(define-public crate-PRUEBA-0.1 (crate (name "PRUEBA") (vers "0.1.0") (hash "0q49wnb10xqz03f701wf3z1kx29nhhdj18f5n2q92jyg0ax36p3z")))

(define-public crate-pruefung-0.1 (crate (name "pruefung") (vers "0.1.0") (deps (list (crate-dep (name "block-buffer") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "byte-tools") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "crypto-tests") (req "^0.5") (default-features #t) (kind 2)) (crate-dep (name "digest") (req "^0.6") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "generic-array") (req "^0.8") (optional #t) (default-features #t) (kind 0)))) (hash "13igbfhr8qgx4g0m9n9bxzb995ishlhy7s6g5z20p3pcxxii8fsi") (features (quote (("generic" "generic-array" "digest") ("default" "generic"))))))

(define-public crate-pruefung-0.2 (crate (name "pruefung") (vers "0.2.0") (deps (list (crate-dep (name "crypto-tests") (req "^0.5") (default-features #t) (kind 2)) (crate-dep (name "digest") (req "^0.6") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "generic-array") (req "^0.8") (optional #t) (default-features #t) (kind 0)))) (hash "1jic9rczs1fsnspk23rjg456gw7g9qkkhfs3jkms16af4v2ahlv4") (features (quote (("generic" "generic-array" "digest") ("default" "generic"))))))

(define-public crate-pruefung-0.2 (crate (name "pruefung") (vers "0.2.1") (deps (list (crate-dep (name "crypto-tests") (req "^0.5") (default-features #t) (kind 2)) (crate-dep (name "digest") (req "^0.6") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "generic-array") (req "^0.8") (optional #t) (default-features #t) (kind 0)))) (hash "116pplp5awz2b90s0c2c0vj232gan524svcj310smr6lb37iykbk") (features (quote (("generic" "generic-array" "digest") ("default" "generic"))))))

(define-public crate-pruesse_guessing_game-0.1 (crate (name "pruesse_guessing_game") (vers "0.1.0") (hash "1hnxpz4pll8yy7d3pjkfv2ymfilvpaya1jifflx1rk38c1hpgwrz") (yanked #t)))

