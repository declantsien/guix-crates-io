(define-module (crates-io pr eo) #:use-module (crates-io))

(define-public crate-preoomkiller-0.0.1 (crate (name "preoomkiller") (vers "0.0.1") (deps (list (crate-dep (name "getopts") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^0.2") (default-features #t) (kind 0)))) (hash "1sshz8w6hc61rsgb3mdh3fzbjm3higc0vvhd22ihg9zlvyhz0a18")))

(define-public crate-preoomkiller-0.0.2 (crate (name "preoomkiller") (vers "0.0.2") (deps (list (crate-dep (name "getopts") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^0.2") (default-features #t) (kind 0)))) (hash "0zr61m9j3n43yzy5491nllq5a9kxzlwnvmrdxd4ggwiwmi147hx2")))

(define-public crate-preoomkiller-0.0.4 (crate (name "preoomkiller") (vers "0.0.4") (deps (list (crate-dep (name "getopts") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^0.2") (default-features #t) (kind 0)))) (hash "0gl9m6a7w8wyi7b1jn5l9zgwy22dn0zdvkzx70kn4d1yiw1vbbq8")))

(define-public crate-preoomkiller-0.0.5 (crate (name "preoomkiller") (vers "0.0.5") (deps (list (crate-dep (name "getopts") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^0.2") (default-features #t) (kind 0)))) (hash "08fayl7bx3qxk98k1zrxlmx82djx1j3wlx4rjdmqyah6fil2f3x7")))

(define-public crate-preoomkiller-0.1 (crate (name "preoomkiller") (vers "0.1.0") (deps (list (crate-dep (name "getopts") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "signal-hook") (req "^0.3.10") (default-features #t) (kind 0)))) (hash "06ayc9907gs34a94w48m6w8qdv8gghging2q84b8lsdqpjj8c3yy")))

