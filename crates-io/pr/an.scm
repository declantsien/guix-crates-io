(define-module (crates-io pr an) #:use-module (crates-io))

(define-public crate-pranav_minigrep-0.1 (crate (name "pranav_minigrep") (vers "0.1.0") (hash "0fgpak2pl6crywilg36cp51ppfnj8f53xkrj5y768bdqr5qkgibs") (yanked #t)))

(define-public crate-pranav_minigrep-0.1 (crate (name "pranav_minigrep") (vers "0.1.1") (hash "0saxn4zax4vrr4k5ipyrkypqhrwh02zzjzgnbywv5gb35bz8kjsw") (yanked #t)))

(define-public crate-pranav_minigrep-0.1 (crate (name "pranav_minigrep") (vers "0.1.2") (hash "1a139ac3dadv86v0f8fyxmhdp6ssfr8dabibyvlls03sfd4hvhip") (yanked #t)))

(define-public crate-pranav_minigrep-0.1 (crate (name "pranav_minigrep") (vers "0.1.3") (hash "1jc2h5ww5vmwvnv94qdmnr1ma5lq8j0kphp8awyhardwfms1vz8j") (yanked #t)))

(define-public crate-pranav_minigrep-0.1 (crate (name "pranav_minigrep") (vers "0.1.4") (hash "1q7xh22r8r4cshhrwpbmns01l867ali88aszpyrgg0ibf09ksnkx")))

(define-public crate-prancing_pony-0.1 (crate (name "prancing_pony") (vers "0.1.0") (hash "17vm9wm0yya194cvcmmss0i423lfjr0mkryyqzpkdvdfblrlvwz9")))

(define-public crate-prancing_pony-1 (crate (name "prancing_pony") (vers "1.0.0") (hash "1m2a42xlyfp06f1ag79vz45i6a1rfyapcz5kxasn9dyg05y0si14")))

(define-public crate-prand-1 (crate (name "prand") (vers "1.0.0") (deps (list (crate-dep (name "clap") (req "^2.33") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.5") (default-features #t) (kind 0)))) (hash "0q9cfczjj3xfxbk3r0pan71qxwp97358ghhy5mhmzxjca0b8dj8b")))

(define-public crate-prand-1 (crate (name "prand") (vers "1.0.1") (deps (list (crate-dep (name "clap") (req "^2.33") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.7") (default-features #t) (kind 0)))) (hash "0bjcm0ylcl4sdfjq33ddfi1zm88vbcfj84gf4ylw3y95k5ci87r7")))

(define-public crate-prandium-0.1 (crate (name "prandium") (vers "0.1.0") (deps (list (crate-dep (name "chrono") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "glob") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "handlebars") (req "^4.3.5") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "pulldown-cmark") (req "^0.9.2") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.6.0") (default-features #t) (kind 0)) (crate-dep (name "rusqlite") (req "^0.28.0") (features (quote ("bundled"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.82") (default-features #t) (kind 0)))) (hash "02h5np47r9x06h0l6p7wlcdfw1lnqmdhss92kv0n93272v51kvjx")))

(define-public crate-prandium-0.1 (crate (name "prandium") (vers "0.1.1") (deps (list (crate-dep (name "chrono") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "glob") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "handlebars") (req "^4.3.5") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "pulldown-cmark") (req "^0.9.2") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.6.0") (default-features #t) (kind 0)) (crate-dep (name "rusqlite") (req "^0.28.0") (features (quote ("bundled"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.82") (default-features #t) (kind 0)))) (hash "10sa58nihxyhjjck1b0iagxfrrrl4swi9nhp1mf76nqd8m0j3kkq")))

(define-public crate-prandom-0.1 (crate (name "prandom") (vers "0.1.0") (hash "1rmwq1scvg6smsncy84ygghy4gf30nas82am4ysnn0m9nslkc0qj")))

(define-public crate-prange-1 (crate (name "prange") (vers "1.0.0") (hash "1rd7pvnx0vc45yfd7lr568v7z7ncby4ilsbm22fy57p1ic43m1sq")))

(define-public crate-prange-1 (crate (name "prange") (vers "1.0.1") (hash "1svc8zpvyjm03bglpzyq9dabf45vnfsgqxpy47zmxnklym1mwk7w")))

(define-public crate-prange2-2 (crate (name "prange2") (vers "2.0.0") (hash "16a981jaiifnnq00spwra80cwdkwi5r51b7pkzmn0jrm27csmjld")))

(define-public crate-prange2-2 (crate (name "prange2") (vers "2.0.1") (hash "1gwqh0qwfnzrn9lfqikp6pb22xj850cpyy9v8aqp1ggqp435vz4w")))

