(define-module (crates-io pr em) #:use-module (crates-io))

(define-public crate-prem-0.1 (crate (name "prem") (vers "0.1.0") (hash "13phqhnmchs0ywrg07yjq3xqxr6fvjh24jh42ia7ahlaza4kjsmn")))

(define-public crate-premultiply-0.1 (crate (name "premultiply") (vers "0.1.0") (hash "0ravjvkcfdc5axfjp43a42vvb8nkpk3hn96qg9qyba0knmqp9r4x")))

