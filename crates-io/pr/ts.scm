(define-module (crates-io pr ts) #:use-module (crates-io))

(define-public crate-prts-0.1 (crate (name "prts") (vers "0.1.0") (deps (list (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.163") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "toml") (req "^0.7.4") (default-features #t) (kind 0)))) (hash "0nc3n5h1pw2h43lqn9xrhjw2zmm0ifbgjq5w7l9vqvr2nrq5ia26") (yanked #t)))

(define-public crate-prts-0.1 (crate (name "prts") (vers "0.1.1") (deps (list (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.163") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "toml") (req "^0.7.4") (default-features #t) (kind 0)))) (hash "1pmbfiddhlwrp785qd3pakrrlmgawvb5x4gjwks123kpx6vrk8w6") (yanked #t)))

(define-public crate-prts-0.1 (crate (name "prts") (vers "0.1.2") (deps (list (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.163") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "toml") (req "^0.7.4") (default-features #t) (kind 0)))) (hash "04vsjpphb5hda79dqbrh1q7ff9k4x9jxj1w0wyr8wkqfrvw9nsd8") (yanked #t)))

(define-public crate-prts-0.2 (crate (name "prts") (vers "0.2.0") (deps (list (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.163") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "toml") (req "^0.7.4") (default-features #t) (kind 0)))) (hash "09ja86rz68s6z74sg95vf9d4nn4dwgqayjraz30wwqrjspfk7pb7") (yanked #t)))

(define-public crate-prts-0.2 (crate (name "prts") (vers "0.2.1") (deps (list (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.163") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "toml") (req "^0.7.4") (default-features #t) (kind 0)))) (hash "0rwaa5v935lj339k10fl3gb6qj4ckgg432g7g9dvfiahrp2ds01v") (yanked #t)))

(define-public crate-prts-0.2 (crate (name "prts") (vers "0.2.2") (deps (list (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.163") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "toml") (req "^0.7.4") (default-features #t) (kind 0)))) (hash "1jsfnipy1wmizlqzf5qynjiafbdj9qwyz6n3k5m0xljkq14fki0m") (yanked #t)))

(define-public crate-prts-0.2 (crate (name "prts") (vers "0.2.3") (deps (list (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.163") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "toml") (req "^0.7.4") (default-features #t) (kind 0)))) (hash "13w8fadcjpdhqy4ws7vnpvn1xccndmqdgnd28nh1nmh198d27yva")))

(define-public crate-prts-0.2 (crate (name "prts") (vers "0.2.4") (deps (list (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.163") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "toml") (req "^0.7.4") (default-features #t) (kind 0)))) (hash "0ygnshrsnb6ds27rp902ywr7sh5jffsc1vz1lrmqfikykfixzzva")))

(define-public crate-prtsc-0.1 (crate (name "prtsc") (vers "0.1.0") (hash "0prciyvaq1gzdmxp9hwraba2h5zqcsh65hnb1z50q2lpz15iybkf")))

