(define-module (crates-io pr at) #:use-module (crates-io))

(define-public crate-prateek_pub_1-0.1 (crate (name "prateek_pub_1") (vers "0.1.0") (hash "0rikjdqrabz3l99flrnbzs6jx0kycgsf6jlyq11z7vs2j3qvaxw7")))

(define-public crate-pratt-0.1 (crate (name "pratt") (vers "0.1.0") (hash "056c0dn21dynsivaq5qgz8iawg2wincackb88jjpbqzd1bnp0944")))

(define-public crate-pratt-0.1 (crate (name "pratt") (vers "0.1.1") (hash "1093wakvmy7cfc42593m2vm7b0zyzxhwb1w65v6b1s0i7rj8a346")))

(define-public crate-pratt-0.1 (crate (name "pratt") (vers "0.1.2") (hash "18wmxxjcvijc0giipgbzd01m6i14pny35lv2qbdrav4bxwa5yyma")))

(define-public crate-pratt-0.1 (crate (name "pratt") (vers "0.1.3") (hash "0q2cd7n1bvjqdn9n6wh8xpx2k3swg9kxplhj60awmivi3bp7hbv7")))

(define-public crate-pratt-0.1 (crate (name "pratt") (vers "0.1.4") (hash "1ng5y1x32aw1wir41call0w4flr50n0y1xib9pvy6pnm6mjy4f1q")))

(define-public crate-pratt-0.2 (crate (name "pratt") (vers "0.2.0") (hash "0lfif3fyjqdzhvdqwnkv9lq8qlhdrj2l7c8drxy3vk8zbfy0sn4p")))

(define-public crate-pratt-0.2 (crate (name "pratt") (vers "0.2.1") (hash "1d0isbipdp81g9m1jllvx482z1rpdbhbpmhg6ij2m0024yssc7iv")))

(define-public crate-pratt-0.2 (crate (name "pratt") (vers "0.2.2") (hash "06vw8mcxn9667y7nk7a8786gi14jx7y4s4ak126wq2nwwm65d8lk")))

(define-public crate-pratt-0.3 (crate (name "pratt") (vers "0.3.0") (hash "1kid62n4ri15rzg6crzz8m7waribk2dnvpchawcpnslkyw9bq6z3")))

(define-public crate-pratt-0.4 (crate (name "pratt") (vers "0.4.0") (hash "1f4b5d3pxg3rvnqfw5qhqs070x7kzcw3lrr0p0c0fvq7bm1a9q0p")))

(define-public crate-prattle-0.1 (crate (name "prattle") (vers "0.1.0") (deps (list (crate-dep (name "failure") (req "^0.1.2") (default-features #t) (kind 0)))) (hash "0myh0vymn4wd779cdy8wfgysygy1ja1bf6f443nivgvqnd60adb4")))

(define-public crate-prattle-0.1 (crate (name "prattle") (vers "0.1.1") (deps (list (crate-dep (name "failure") (req "^0.1.2") (default-features #t) (kind 0)))) (hash "0vzwvdv0a4bcz55fmlpg8xxly8yq0rsfq5219nam2s8smvppy6vs")))

(define-public crate-prattle-0.1 (crate (name "prattle") (vers "0.1.2") (deps (list (crate-dep (name "failure") (req "^0.1.2") (default-features #t) (kind 0)))) (hash "0zlpqvc938hbybrs060pyxysclbkg0ql7s6jpnqdpr3966y3072g")))

(define-public crate-prattle-0.1 (crate (name "prattle") (vers "0.1.3") (deps (list (crate-dep (name "failure") (req "^0.1.2") (default-features #t) (kind 0)))) (hash "0wmyvlh53g7a4vyrmwxcv7gqjhdmp9z5xqbfyxfqd7qsmii37zir")))

