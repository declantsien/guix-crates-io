(define-module (crates-io pr ei) #:use-module (crates-io))

(define-public crate-prei-0.1 (crate (name "prei") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^3") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "13v340m5dyc87ayahaah6nbd7ypqd9l34qxpbbasdc4zngs231qx")))

(define-public crate-prei-0.1 (crate (name "prei") (vers "0.1.1") (deps (list (crate-dep (name "clap") (req "^3") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.11") (default-features #t) (kind 0)))) (hash "0na6rv506ipkq4yb60i1p5f600i7v8jysidbnlsapbzaiyrza5xq")))

(define-public crate-prei-0.1 (crate (name "prei") (vers "0.1.2") (deps (list (crate-dep (name "clap") (req "^3") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.11") (default-features #t) (kind 0)))) (hash "1v2w2wrv0gf6gz6v1lx6zbsva3m8fvzs83kiw9biv9iizgp3kkjc")))

(define-public crate-prei-0.1 (crate (name "prei") (vers "0.1.3") (deps (list (crate-dep (name "clap") (req "^3") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.11") (default-features #t) (kind 0)))) (hash "1a42npva1x4qqafm2l4gxr4ksfg7k0y0136fzsaf9bcq7w7n9y8p")))

(define-public crate-prei-0.1 (crate (name "prei") (vers "0.1.4") (deps (list (crate-dep (name "clap") (req "^4") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.11") (default-features #t) (kind 0)))) (hash "0ar7wk7qbcsh5js7x968vvqpk6r3jdr13wj9r40qbkkxvzcw13lv")))

(define-public crate-prei-0.1 (crate (name "prei") (vers "0.1.5") (deps (list (crate-dep (name "clap") (req "^4") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.11") (default-features #t) (kind 0)))) (hash "1y04nwqbivgmscaz7dn37fnzz6wln101g90cf0b0p5m0l7akz8hh")))

(define-public crate-preimage-server-0.1 (crate (name "preimage-server") (vers "0.1.0") (deps (list (crate-dep (name "anyhow") (req "^1.0.71") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^4.3.15") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "env_logger") (req "^0.10.0") (default-features #t) (kind 0)) (crate-dep (name "hex") (req "^0.4.3") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.19") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.103") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.29.1") (features (quote ("rt" "macros" "fs" "io-util" "signal" "sync" "time"))) (default-features #t) (kind 0)))) (hash "0s11hp795xk862gpwkz4b09k79ipjahafi6pgjfjv155ragwg7l2")))

