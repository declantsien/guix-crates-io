(define-module (crates-io pr ev) #:use-module (crates-io))

(define-public crate-prev-iter-0.1 (crate (name "prev-iter") (vers "0.1.0") (hash "0z3kv1i7pvnr2nw70y7a9887q304qid8ld0xqf3p50mmp20k3b4h")))

(define-public crate-prev-iter-0.1 (crate (name "prev-iter") (vers "0.1.1") (hash "1clm88q9kvpiah23q8cczw0a2w07wa7ijjz85d67l5myfcpvnak2")))

(define-public crate-prev-iter-0.1 (crate (name "prev-iter") (vers "0.1.2") (hash "10zc698ndz247lr3lafdbh8j2pggx2aaifgl8hh9nacqyx9b7b6f")))

(define-public crate-prev-iter-0.2 (crate (name "prev-iter") (vers "0.2.0") (hash "0yasin0nqh0kl6rnnir48jr893gpjp5q8841f438m3872pp0l6vd")))

(define-public crate-prevayler-rs-0.1 (crate (name "prevayler-rs") (vers "0.1.0") (deps (list (crate-dep (name "async-std") (req "^1.9") (features (quote ("attributes"))) (default-features #t) (kind 0)) (crate-dep (name "futures") (req "^0.3.14") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 2)) (crate-dep (name "serde_json") (req "^1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "temp_testdir") (req "^0.2") (default-features #t) (kind 2)))) (hash "0sm9ma2hirsm49li3njpi03x7i4h3d7ggmhl1zb3b8div2gq9rb0") (features (quote (("json_serializer" "serde_json" "serde") ("default" "json_serializer"))))))

(define-public crate-prevayler-rs-0.2 (crate (name "prevayler-rs") (vers "0.2.0") (deps (list (crate-dep (name "async-std") (req "^1.9") (features (quote ("attributes"))) (default-features #t) (kind 0)) (crate-dep (name "futures") (req "^0.3.14") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 2)) (crate-dep (name "serde_json") (req "^1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "temp_testdir") (req "^0.2") (default-features #t) (kind 2)))) (hash "1m2ykxmp81z54y3iyp6k7p8qk0713b1i1b2iwxjq97m4mz3gry9s") (features (quote (("json_serializer" "serde_json" "serde") ("default" "json_serializer"))))))

(define-public crate-prevayler-rs-0.2 (crate (name "prevayler-rs") (vers "0.2.1") (deps (list (crate-dep (name "async-std") (req "^1.9") (features (quote ("attributes"))) (default-features #t) (kind 0)) (crate-dep (name "futures") (req "^0.3.14") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 2)) (crate-dep (name "serde_json") (req "^1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "temp_testdir") (req "^0.2") (default-features #t) (kind 2)))) (hash "1kikpmslxkamaxf1j2dvm20vdlyg1whrg4c40gdaaylbrmcdk491") (features (quote (("json_serializer" "serde_json" "serde") ("default" "json_serializer"))))))

(define-public crate-prevayler-rs-0.2 (crate (name "prevayler-rs") (vers "0.2.2") (deps (list (crate-dep (name "async-std") (req "^1.9") (features (quote ("attributes"))) (default-features #t) (kind 0)) (crate-dep (name "futures") (req "^0.3.14") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 2)) (crate-dep (name "serde_json") (req "^1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "temp_testdir") (req "^0.2") (default-features #t) (kind 2)))) (hash "13g15006bmhv4l3sik4fma0wh76z3na9dal4kg7lys4qrd8fg3iq") (features (quote (("json_serializer" "serde_json" "serde") ("default" "json_serializer"))))))

(define-public crate-prevent-0.0.0 (crate (name "prevent") (vers "0.0.0") (hash "04vm6k0xrh0y1la75nxjpy3cglkkq83gx383iiq93w3764ihrf09")))

(define-public crate-preview-rs-0.1 (crate (name "preview-rs") (vers "0.1.0") (deps (list (crate-dep (name "reqwest") (req "^0.11.11") (features (quote ("json" "blocking"))) (default-features #t) (kind 0)) (crate-dep (name "scraper") (req "^0.12.0") (default-features #t) (kind 0)))) (hash "1q448w12z583490slaacq908j59716jz8gfm0fb1lqxk61i39wvd")))

(define-public crate-preview-rs-0.1 (crate (name "preview-rs") (vers "0.1.1") (deps (list (crate-dep (name "reqwest") (req "^0.11.11") (features (quote ("json" "blocking"))) (default-features #t) (kind 0)) (crate-dep (name "scraper") (req "^0.12.0") (default-features #t) (kind 0)))) (hash "0zn1cgyy8bzliyd2j3idsyhpl8zwl9bbpizrdq2lixl4mp3jicmd")))

(define-public crate-preview-rs-0.1 (crate (name "preview-rs") (vers "0.1.2") (deps (list (crate-dep (name "reqwest") (req "^0.11.11") (features (quote ("json" "blocking"))) (default-features #t) (kind 0)) (crate-dep (name "scraper") (req "^0.12.0") (default-features #t) (kind 0)))) (hash "03rsz3v3mk5liw8xz90xcdacamdr66rfan2yzfkm4zklw8cw4nlq")))

(define-public crate-preview-rs-0.1 (crate (name "preview-rs") (vers "0.1.3") (deps (list (crate-dep (name "reqwest") (req "^0.11.11") (features (quote ("json" "blocking"))) (default-features #t) (kind 0)) (crate-dep (name "scraper") (req "^0.12.0") (default-features #t) (kind 0)))) (hash "0jqp9smy156zyp1zgshkjy99mamzb7ailizgnjml51kg6vgp73ck")))

(define-public crate-previous-2 (crate (name "previous") (vers "2.1.0") (hash "0p8cf6dpr8d9ip31sw9y9bf5bg67ykkci3l5x3wag1ia7yl2bxpq")))

(define-public crate-previuwu-0.0.0 (crate (name "previuwu") (vers "0.0.0") (deps (list (crate-dep (name "anyhow") (req "^1.0.58") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^3.2.14") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "eframe") (req "^0.18.0") (default-features #t) (kind 0)) (crate-dep (name "egui") (req "^0.18.1") (default-features #t) (kind 0)) (crate-dep (name "egui_extras") (req "^0.18.0") (features (quote ("image"))) (default-features #t) (kind 0)) (crate-dep (name "image") (req "^0.24.3") (default-features #t) (kind 0)) (crate-dep (name "mime_guess") (req "^2.0.4") (default-features #t) (kind 0)))) (hash "19h9wdxynal5fa55bgbmgxsc9kv4kq6nsjjaq60gnxzqd8i9583n") (yanked #t)))

(define-public crate-prevmark-0.1 (crate (name "prevmark") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^3.0.0-beta.5") (default-features #t) (kind 0)) (crate-dep (name "pulldown-cmark") (req "^0.8.0") (default-features #t) (kind 0)) (crate-dep (name "scraper") (req "^0.12.0") (default-features #t) (kind 0)) (crate-dep (name "sixtyfps") (req "^0.1.5") (default-features #t) (kind 0)) (crate-dep (name "sixtyfps-build") (req "^0.1.5") (default-features #t) (kind 1)))) (hash "014ccfrmcvkgmqbg2xqvkiaii18iqhxsk8a5n9y934pxj1q6kdxl")))

