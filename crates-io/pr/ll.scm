(define-module (crates-io pr ll) #:use-module (crates-io))

(define-public crate-prll-ri-1 (crate (name "prll-ri") (vers "1.0.2") (deps (list (crate-dep (name "bio") (req "^0.9") (default-features #t) (kind 0)) (crate-dep (name "getopts") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "scoped_threadpool") (req "^0.1") (default-features #t) (kind 0)))) (hash "08l07f7y1lx7ppf9ciwb01iyny0kpbfgrjpwggi6z84fx8clfpnd")))

(define-public crate-prll-ri-1 (crate (name "prll-ri") (vers "1.0.3") (deps (list (crate-dep (name "bio") (req "^0.9") (default-features #t) (kind 0)) (crate-dep (name "getopts") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "scoped_threadpool") (req "^0.1") (default-features #t) (kind 0)))) (hash "1frvrl2x94gnibbij9bx9c31mz7bz0dksymr4wg5fqbl2gx6ffwr")))

(define-public crate-prll-ri-1 (crate (name "prll-ri") (vers "1.0.4") (deps (list (crate-dep (name "bio") (req "^0.9") (default-features #t) (kind 0)) (crate-dep (name "getopts") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "scoped_threadpool") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "time") (req "^0.1") (default-features #t) (kind 0)))) (hash "0fi9lcz22yilbbyzd4305pbklldydlnq374y5qjxal0xqhqsaj6n")))

(define-public crate-prll-ri-1 (crate (name "prll-ri") (vers "1.0.5") (deps (list (crate-dep (name "bio") (req "^0.9") (default-features #t) (kind 0)) (crate-dep (name "getopts") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "scoped_threadpool") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "time") (req "^0.1") (default-features #t) (kind 0)))) (hash "03krji790fdfy9f0m07svzs2m4sml6kdw1zjmr1avvvsz6sywasb")))

(define-public crate-prll-ri-1 (crate (name "prll-ri") (vers "1.0.6") (deps (list (crate-dep (name "bio") (req "^0.9") (default-features #t) (kind 0)) (crate-dep (name "getopts") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "scoped_threadpool") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "time") (req "^0.1") (default-features #t) (kind 0)))) (hash "00m590bxrjwsxr15wal2lzzw3rw3l3wqkyl6chzqy557hzz849mp")))

