(define-module (crates-io pr cs) #:use-module (crates-io))

(define-public crate-prcs-0.1 (crate (name "prcs") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^3.0.0-beta.2") (default-features #t) (kind 0)) (crate-dep (name "insta") (req "^1.7.1") (default-features #t) (kind 2)) (crate-dep (name "nom") (req "^6.1.0") (default-features #t) (kind 0)) (crate-dep (name "tempfile") (req "^3.2.0") (default-features #t) (kind 2)))) (hash "0dsk06j7iz84ka5xw44b8x8d3bi8g9g1hbfmjjby92mlp9ayy2vj")))

(define-public crate-prcs-0.2 (crate (name "prcs") (vers "0.2.0") (deps (list (crate-dep (name "clap") (req "^3.0.0-beta.2") (default-features #t) (kind 0)) (crate-dep (name "insta") (req "^1.7.1") (default-features #t) (kind 2)) (crate-dep (name "nom") (req "^6.1.0") (default-features #t) (kind 0)) (crate-dep (name "tempfile") (req "^3.2.0") (default-features #t) (kind 2)))) (hash "127ldij3paiqwl76iywi3h8qisyv2bz8bs03jpl2p55ia5s5fykx")))

