(define-module (crates-io pr eb) #:use-module (crates-io))

(define-public crate-prebuild-0.1 (crate (name "prebuild") (vers "0.1.0") (deps (list (crate-dep (name "anyhow") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "syntect") (req "^5") (default-features #t) (kind 0)) (crate-dep (name "walkdir") (req "^2.3.3") (default-features #t) (kind 0)))) (hash "06ny2p75j06ym0zq1x1rsflzgx7nc52hpnk7d2cybdpy8b3n6ygr")))

