(define-module (crates-io pr un) #:use-module (crates-io))

(define-public crate-prun-0.0.1 (crate (name "prun") (vers "0.0.1") (deps (list (crate-dep (name "clap") (req "^4.5.4") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.36.0") (features (quote ("full"))) (default-features #t) (kind 0)) (crate-dep (name "walkdir") (req "^2.5.0") (default-features #t) (kind 0)))) (hash "1ni2pfzsnsg62zmdfzc7vnc3x00f74p2qphqqyr2v24ga4h4ia0s") (yanked #t)))

(define-public crate-prun-0.0.2 (crate (name "prun") (vers "0.0.2") (deps (list (crate-dep (name "clap") (req "^4.5.4") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.36.0") (features (quote ("full"))) (default-features #t) (kind 0)) (crate-dep (name "walkdir") (req "^2.5.0") (default-features #t) (kind 0)))) (hash "0si3q6w74b7yn92gyx3vlyinw41ckb6w1wjb5q3b558hlnm01ks7") (yanked #t)))

(define-public crate-prun-0.0.3 (crate (name "prun") (vers "0.0.3") (deps (list (crate-dep (name "clap") (req "^4.5.4") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.36.0") (features (quote ("full"))) (default-features #t) (kind 0)) (crate-dep (name "walkdir") (req "^2.5.0") (default-features #t) (kind 0)))) (hash "1qqpaw463db529gzb0fdl0xa623r0x1bp9651lvvwiss7air344x") (yanked #t)))

(define-public crate-prun-0.0.4 (crate (name "prun") (vers "0.0.4") (deps (list (crate-dep (name "clap") (req "^4.5.4") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.36.0") (features (quote ("full"))) (default-features #t) (kind 0)) (crate-dep (name "walkdir") (req "^2.5.0") (default-features #t) (kind 0)))) (hash "035lprfdhcl79m0zscd0y1klwv3p5vvh4k0b75r88lngca3jz5ly")))

(define-public crate-prun-0.0.5 (crate (name "prun") (vers "0.0.5") (deps (list (crate-dep (name "clap") (req "^4.5.4") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.37.0") (features (quote ("full"))) (default-features #t) (kind 0)) (crate-dep (name "walkdir") (req "^2.5.0") (default-features #t) (kind 0)))) (hash "1wpxlpj9r5am0ala55ylma6cm83vpixx9mdmxgp5qvzfb14r7h3x")))

(define-public crate-prune-0.1 (crate (name "prune") (vers "0.1.0") (hash "1yffdpaxfvxb6ini6dd9jzlc16fj6jr2n1hnfzgidr425j38wpxj")))

(define-public crate-prune-0.1 (crate (name "prune") (vers "0.1.1") (deps (list (crate-dep (name "lazy_static") (req "^1.3.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.1.7") (default-features #t) (kind 0)))) (hash "1wqwrlislhzxwslwsv33nigl3zf9gip1q3x7qnwzyma8snbd3839")))

(define-public crate-prune-0.1 (crate (name "prune") (vers "0.1.2") (deps (list (crate-dep (name "lazy_static") (req "^1.3.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.1.7") (default-features #t) (kind 0)))) (hash "08yrslfx0zpz76dikw097ki3qvrmqbiqr7ys576yvb0rxazjna2m")))

(define-public crate-prune-0.1 (crate (name "prune") (vers "0.1.3") (deps (list (crate-dep (name "lazy_static") (req "^1.3.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.1.7") (default-features #t) (kind 0)))) (hash "0vrcqn5qd14hmdg66jy60yajzbq0z9qs2ihq896gbf1pxs1ris71")))

(define-public crate-prune-0.1 (crate (name "prune") (vers "0.1.4") (deps (list (crate-dep (name "lazy_static") (req "^1.3.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.1.7") (default-features #t) (kind 0)))) (hash "0wn7jzv0mx04iynghyll706fwl5yfmr8n4xdi4fkhvvf139wk84y")))

(define-public crate-prune-0.1 (crate (name "prune") (vers "0.1.5") (deps (list (crate-dep (name "lazy_static") (req "^1.3.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.1.7") (default-features #t) (kind 0)))) (hash "1h6va8z7mk0rqszg5243iaczfgnpd999jnpy1f98vf9sg78449qn")))

(define-public crate-prune-0.1 (crate (name "prune") (vers "0.1.6") (deps (list (crate-dep (name "lazy_static") (req "^1.3.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.1.7") (default-features #t) (kind 0)))) (hash "0yw3sawy23kw0dznjqnk9rsvqfqb6dhaf1vyjav8xml1zgmnwg6d")))

(define-public crate-prune_derive-0.1 (crate (name "prune_derive") (vers "0.1.0") (deps (list (crate-dep (name "proc-macro2") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "prune") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^0.6") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^0.15") (features (quote ("extra-traits"))) (default-features #t) (kind 0)))) (hash "13sg266w618irfq8yy6xzbncifadgwz0958zzj2db81mfa0379vz")))

(define-public crate-prune_derive-0.1 (crate (name "prune_derive") (vers "0.1.1") (deps (list (crate-dep (name "proc-macro2") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "prune") (req "^0.1.1") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^0.6") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^0.15") (features (quote ("extra-traits"))) (default-features #t) (kind 0)))) (hash "1b0qgyksw9fpdg1rh6dli60w71fw2ifk8cwb9zl5paqprkg87rvh")))

(define-public crate-prune_derive-0.1 (crate (name "prune_derive") (vers "0.1.2") (deps (list (crate-dep (name "proc-macro2") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "prune") (req "^0.1.2") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^0.6") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^0.15") (features (quote ("extra-traits"))) (default-features #t) (kind 0)))) (hash "0979wsx81p81wny1gz19nchg9fh5bl8mb72lgxgzfvhsff2ldvmg")))

(define-public crate-prune_derive-0.1 (crate (name "prune_derive") (vers "0.1.3") (deps (list (crate-dep (name "proc-macro2") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "prune") (req "^0.1.3") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^0.6") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^0.15") (features (quote ("extra-traits"))) (default-features #t) (kind 0)))) (hash "0qdjdg30b825czbihx711vs3x1l69wpq1rr0n3qwa3lk3nikrb9g")))

(define-public crate-prune_derive-0.1 (crate (name "prune_derive") (vers "0.1.4") (deps (list (crate-dep (name "proc-macro2") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "prune") (req "^0.1.4") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^0.6") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^0.15") (features (quote ("extra-traits"))) (default-features #t) (kind 0)))) (hash "0s9zzbqihj9wgc1i131wlmlqij4x6s2byhrdnyg6l7npv2kv16h9")))

(define-public crate-prune_derive-0.1 (crate (name "prune_derive") (vers "0.1.5") (deps (list (crate-dep (name "proc-macro2") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "prune") (req "^0.1.5") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^0.6") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^0.15") (features (quote ("extra-traits"))) (default-features #t) (kind 0)))) (hash "0xmjrzqj0rr489a5srg1cnhwzjqh7bhli7m6nzcvf46h1sbi1n8d")))

(define-public crate-prune_derive-0.1 (crate (name "prune_derive") (vers "0.1.6") (deps (list (crate-dep (name "proc-macro2") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "prune") (req "^0.1.6") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^0.6") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^0.15") (features (quote ("extra-traits"))) (default-features #t) (kind 0)))) (hash "1z56b4vvz7ydhc2gmcqvqyra01d4fmdghphpjlxh8jk8dmqpprl2")))

(define-public crate-pruner-0.4 (crate (name "pruner") (vers "0.4.0") (deps (list (crate-dep (name "chrono") (req "^0.4.19") (default-features #t) (kind 0)) (crate-dep (name "iso8601") (req "^0.4.0") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.3.22") (default-features #t) (kind 0)))) (hash "144qxrn09slz497a5jwpya5kpzhmpj81s2k9v1g18gvifxbjs6md")))

(define-public crate-pruner-0.5 (crate (name "pruner") (vers "0.5.0") (deps (list (crate-dep (name "chrono") (req "^0.4.19") (default-features #t) (kind 0)) (crate-dep (name "iso8601") (req "^0.4.0") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.3.22") (default-features #t) (kind 0)))) (hash "13f0zybd029yhzv4imsch3j2r53p5q8q7k1ks7wmync7nd359ihi")))

(define-public crate-pruner-0.6 (crate (name "pruner") (vers "0.6.0") (deps (list (crate-dep (name "chrono") (req "^0.4.19") (default-features #t) (kind 0)) (crate-dep (name "iso8601") (req "^0.4.0") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.3.22") (default-features #t) (kind 0)))) (hash "0p57cmx07v6m01gxfbfcw1kgyqilqkm00sixcflw51fsprfjjwb4")))

(define-public crate-pruner-0.12 (crate (name "pruner") (vers "0.12.0") (deps (list (crate-dep (name "chrono") (req "^0.4.19") (default-features #t) (kind 0)) (crate-dep (name "iso8601") (req "^0.4.0") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.3.22") (default-features #t) (kind 0)))) (hash "0qlfj3k00mddc3gd1xcgwnprn0gyynhwxyq1zgp2nlsxs2z7xgsd")))

(define-public crate-pruner-0.14 (crate (name "pruner") (vers "0.14.0") (deps (list (crate-dep (name "chrono") (req "^0.4.19") (default-features #t) (kind 0)) (crate-dep (name "iso8601") (req "^0.4.0") (default-features #t) (kind 0)) (crate-dep (name "proptest") (req "^1.0.0") (default-features #t) (kind 2)) (crate-dep (name "structopt") (req "^0.3.22") (default-features #t) (kind 0)))) (hash "093rajjr3ajcp1q43229rkakkz9203bmzk6swdp1f08d895s7pzq")))

(define-public crate-pruner-0.16 (crate (name "pruner") (vers "0.16.0") (deps (list (crate-dep (name "chrono") (req "^0.4.19") (default-features #t) (kind 0)) (crate-dep (name "iso8601") (req "^0.4.0") (default-features #t) (kind 0)) (crate-dep (name "proptest") (req "^1.0.0") (default-features #t) (kind 2)) (crate-dep (name "structopt") (req "^0.3.22") (default-features #t) (kind 0)))) (hash "0rv0k76nms2cpq38p3ym632jj5cdfnvgaq90bwjmgjm8yakm7244")))

(define-public crate-pruner-0.17 (crate (name "pruner") (vers "0.17.0") (deps (list (crate-dep (name "chrono") (req "^0.4.19") (default-features #t) (kind 0)) (crate-dep (name "iso8601") (req "^0.4.0") (default-features #t) (kind 0)) (crate-dep (name "proptest") (req "^1.0.0") (default-features #t) (kind 2)) (crate-dep (name "structopt") (req "^0.3.22") (default-features #t) (kind 0)))) (hash "1winlxsvbb89c9ajykqlgn1jfg8n23blr630jmhbi9y1c3r28rsf")))

(define-public crate-pruner-0.22 (crate (name "pruner") (vers "0.22.0") (deps (list (crate-dep (name "chrono") (req "^0.4.19") (default-features #t) (kind 0)) (crate-dep (name "iso8601") (req "^0.4.0") (default-features #t) (kind 0)) (crate-dep (name "proptest") (req "^1.0.0") (default-features #t) (kind 2)) (crate-dep (name "structopt") (req "^0.3.22") (default-features #t) (kind 0)))) (hash "00z29h36jgyskb2vblqy56hmgapygl8r29d2pl3y1j3fg7z827hs")))

(define-public crate-pruner-0.24 (crate (name "pruner") (vers "0.24.0") (deps (list (crate-dep (name "chrono") (req "^0.4.23") (default-features #t) (kind 0)) (crate-dep (name "iso8601") (req "^0.4.0") (default-features #t) (kind 0)) (crate-dep (name "proptest") (req "^1.0.0") (default-features #t) (kind 2)) (crate-dep (name "structopt") (req "^0.3.22") (default-features #t) (kind 0)))) (hash "1ym035jxg5narkpjq01xpaq7xd9f5j8phwvgk3n1za0idrrvwg2p")))

(define-public crate-pruner-0.26 (crate (name "pruner") (vers "0.26.0") (deps (list (crate-dep (name "chrono") (req "^0.4.23") (default-features #t) (kind 0)) (crate-dep (name "iso8601") (req "^0.4.0") (default-features #t) (kind 0)) (crate-dep (name "proptest") (req "^1.0.0") (default-features #t) (kind 2)) (crate-dep (name "structopt") (req "^0.3.26") (default-features #t) (kind 0)))) (hash "1iqxykk2qabyd8s9v61wvyln7idfg4z8ydc5nnahcjy9hwdlhca4")))

(define-public crate-pruner-0.28 (crate (name "pruner") (vers "0.28.0") (deps (list (crate-dep (name "chrono") (req "^0.4.23") (default-features #t) (kind 0)) (crate-dep (name "iso8601") (req "^0.5.1") (default-features #t) (kind 0)) (crate-dep (name "proptest") (req "^1.0.0") (default-features #t) (kind 2)) (crate-dep (name "structopt") (req "^0.3.26") (default-features #t) (kind 0)))) (hash "0vibm5liysmkc48d01nx38l87dv5kspg8k8s8fgb8kcj8cickldq")))

(define-public crate-pruner-0.30 (crate (name "pruner") (vers "0.30.0") (deps (list (crate-dep (name "chrono") (req "^0.4.23") (default-features #t) (kind 0)) (crate-dep (name "iso8601") (req "^0.5.1") (default-features #t) (kind 0)) (crate-dep (name "proptest") (req "^1.1.0") (default-features #t) (kind 2)) (crate-dep (name "structopt") (req "^0.3.26") (default-features #t) (kind 0)))) (hash "1f3v7bakdckf480c9q370vggflq77x2lr0c93qf9iqgyj8q25187")))

(define-public crate-pruner-0.32 (crate (name "pruner") (vers "0.32.0") (deps (list (crate-dep (name "chrono") (req "^0.4.23") (default-features #t) (kind 0)) (crate-dep (name "iso8601") (req "^0.6.1") (default-features #t) (kind 0)) (crate-dep (name "proptest") (req "^1.1.0") (default-features #t) (kind 2)) (crate-dep (name "structopt") (req "^0.3.26") (default-features #t) (kind 0)))) (hash "1m0yxd8s239jpz4p8r0fs5gp4x568zv3jqjv73ch4cwh79d16jdl")))

(define-public crate-pruner-0.34 (crate (name "pruner") (vers "0.34.0") (deps (list (crate-dep (name "chrono") (req "^0.4.24") (default-features #t) (kind 0)) (crate-dep (name "iso8601") (req "^0.6.1") (default-features #t) (kind 0)) (crate-dep (name "proptest") (req "^1.1.0") (default-features #t) (kind 2)) (crate-dep (name "structopt") (req "^0.3.26") (default-features #t) (kind 0)))) (hash "13kkaqa2wa5gq5pjb0ad1i7l460cj7w60844k4cmrwpra42is2ii")))

(define-public crate-pruner-0.36 (crate (name "pruner") (vers "0.36.0") (deps (list (crate-dep (name "chrono") (req "^0.4.24") (default-features #t) (kind 0)) (crate-dep (name "iso8601") (req "^0.6.1") (default-features #t) (kind 0)) (crate-dep (name "proptest") (req "^1.1.0") (default-features #t) (kind 2)) (crate-dep (name "structopt") (req "^0.3.26") (default-features #t) (kind 0)))) (hash "1a2vlqi1gfiw5sksk8v14cbgkdyf5l0qam28mxinavxp3g4shx8j")))

(define-public crate-pruner-0.38 (crate (name "pruner") (vers "0.38.0") (deps (list (crate-dep (name "chrono") (req "^0.4.24") (default-features #t) (kind 0)) (crate-dep (name "iso8601") (req "^0.6.1") (default-features #t) (kind 0)) (crate-dep (name "proptest") (req "^1.1.0") (default-features #t) (kind 2)) (crate-dep (name "structopt") (req "^0.3.26") (default-features #t) (kind 0)))) (hash "0mqw78mnbjr0pv508clq5kfxpzwl29q6pnj5rpjcyc65wbarj6aa")))

(define-public crate-pruner-0.40 (crate (name "pruner") (vers "0.40.0") (deps (list (crate-dep (name "chrono") (req "^0.4.24") (default-features #t) (kind 0)) (crate-dep (name "iso8601") (req "^0.6.1") (default-features #t) (kind 0)) (crate-dep (name "proptest") (req "^1.1.0") (default-features #t) (kind 2)) (crate-dep (name "structopt") (req "^0.3.26") (default-features #t) (kind 0)))) (hash "0l42njlv41wpvf6shma6yhy3j7g1x2z34yz311hw7zmn0lndb0n1")))

(define-public crate-pruner-0.42 (crate (name "pruner") (vers "0.42.0") (deps (list (crate-dep (name "chrono") (req "^0.4.24") (default-features #t) (kind 0)) (crate-dep (name "iso8601") (req "^0.6.1") (default-features #t) (kind 0)) (crate-dep (name "proptest") (req "^1.1.0") (default-features #t) (kind 2)) (crate-dep (name "structopt") (req "^0.3.26") (default-features #t) (kind 0)))) (hash "0y3b2nq7m0hh1xxbc9mf0inxsjf9va5b90c73g41c8bm514py6cg")))

(define-public crate-pruner-0.44 (crate (name "pruner") (vers "0.44.0") (deps (list (crate-dep (name "chrono") (req "^0.4.24") (default-features #t) (kind 0)) (crate-dep (name "iso8601") (req "^0.6.1") (default-features #t) (kind 0)) (crate-dep (name "proptest") (req "^1.1.0") (default-features #t) (kind 2)) (crate-dep (name "structopt") (req "^0.3.26") (default-features #t) (kind 0)))) (hash "1cfy0ygj089cxsfwns7r9asg2kcb3v437hxxfrl7qjjfg7ck3llr")))

(define-public crate-pruner-0.46 (crate (name "pruner") (vers "0.46.0") (deps (list (crate-dep (name "chrono") (req "^0.4.24") (default-features #t) (kind 0)) (crate-dep (name "iso8601") (req "^0.6.1") (default-features #t) (kind 0)) (crate-dep (name "proptest") (req "^1.1.0") (default-features #t) (kind 2)) (crate-dep (name "structopt") (req "^0.3.26") (default-features #t) (kind 0)))) (hash "0sshidh9phkayixpynfqp0bz43jhw7h163jviqrlim6g6g28bhy9")))

(define-public crate-pruner-0.48 (crate (name "pruner") (vers "0.48.0") (deps (list (crate-dep (name "chrono") (req "^0.4.24") (default-features #t) (kind 0)) (crate-dep (name "iso8601") (req "^0.6.1") (default-features #t) (kind 0)) (crate-dep (name "proptest") (req "^1.1.0") (default-features #t) (kind 2)) (crate-dep (name "structopt") (req "^0.3.26") (default-features #t) (kind 0)))) (hash "152dvbggg49pbgh23h6ya2jnlngafp3dgb8iv2ypq8kj2yy45sf9")))

(define-public crate-pruner-0.50 (crate (name "pruner") (vers "0.50.0") (deps (list (crate-dep (name "chrono") (req "^0.4.24") (default-features #t) (kind 0)) (crate-dep (name "iso8601") (req "^0.6.1") (default-features #t) (kind 0)) (crate-dep (name "proptest") (req "^1.1.0") (default-features #t) (kind 2)) (crate-dep (name "structopt") (req "^0.3.26") (default-features #t) (kind 0)))) (hash "0adzwikd7nb1gpz04a8ysxji0v8gflhdz0987q6fazbjkkc660yi")))

(define-public crate-pruning_radix_trie-0.2 (crate (name "pruning_radix_trie") (vers "0.2.2") (hash "057cqv6adyqi779s5zhqvqwg1v25h3xl2dsagpi6hz3bfqbqsl8m")))

(define-public crate-pruning_radix_trie-0.3 (crate (name "pruning_radix_trie") (vers "0.3.0") (hash "0nx26l0pwd051xhzn9w6j2wxaj1g5lagsar672pvmqnrqk3nkpxk")))

(define-public crate-pruning_radix_trie-1 (crate (name "pruning_radix_trie") (vers "1.0.0") (deps (list (crate-dep (name "compact_str") (req "^0.7.0") (default-features #t) (kind 0)) (crate-dep (name "criterion") (req "^0.4.0") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)))) (hash "0njxwcxnqndlxwnzm2rlvvcc5cfdhx613x3ymlzcyyif8zv9cx1y")))

(define-public crate-prunus-0.1 (crate (name "prunus") (vers "0.1.0") (hash "06xh5q01gl4c7622fax9rwclv6207ch7nfmqpsswnbalhmqg1s97")))

