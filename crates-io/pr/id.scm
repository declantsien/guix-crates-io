(define-module (crates-io pr id) #:use-module (crates-io))

(define-public crate-pride-0.1 (crate (name "pride") (vers "0.1.0") (hash "0c5mlqz17ilirprsrdilhbsf51h358b7aq10ymng81gb8krl0cp5")))

(define-public crate-pride-cli-0.1 (crate (name "pride-cli") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^4.4.8") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "terminal_size") (req "^0.3.0") (default-features #t) (kind 0)))) (hash "1yfvrz33cg0c0j0bm1b5ang8m8z25jmn2svm4bw85l3fjkjg1m4c")))

(define-public crate-pride-overlay-1 (crate (name "pride-overlay") (vers "1.0.0") (deps (list (crate-dep (name "image") (req "^0.24.5") (default-features #t) (kind 0)) (crate-dep (name "imageproc") (req "^0.23.0") (default-features #t) (kind 0)))) (hash "02488llky3xpsncbbhzn1sc68j29jva6vmzh6mj22hcy7ljncywc")))

(define-public crate-pride-overlay-1 (crate (name "pride-overlay") (vers "1.1.0") (deps (list (crate-dep (name "image") (req "^0.24.5") (default-features #t) (kind 0)) (crate-dep (name "imageproc") (req "^0.23.0") (default-features #t) (kind 0)) (crate-dep (name "strum") (req "^0.25.0") (default-features #t) (kind 0)) (crate-dep (name "strum_macros") (req "^0.25.1") (default-features #t) (kind 0)))) (hash "0i4k9db743r42xjl71dcv4ls1aq8q6vm0ibwmr8lgav9bi346frd")))

(define-public crate-pride-overlay-1 (crate (name "pride-overlay") (vers "1.2.0") (deps (list (crate-dep (name "image") (req "^0.24.5") (default-features #t) (kind 0)) (crate-dep (name "imageproc") (req "^0.23.0") (default-features #t) (kind 0)) (crate-dep (name "strum") (req "^0.25.0") (default-features #t) (kind 0)) (crate-dep (name "strum_macros") (req "^0.25.1") (default-features #t) (kind 0)))) (hash "010mnpxwicm8s6xvxna3iaq7y7rpqac6wimxp43m4fa6j3z22kig") (yanked #t)))

(define-public crate-pride-overlay-1 (crate (name "pride-overlay") (vers "1.2.1") (deps (list (crate-dep (name "image") (req "^0.24.5") (default-features #t) (kind 0)) (crate-dep (name "imageproc") (req "^0.23.0") (default-features #t) (kind 0)) (crate-dep (name "strum") (req "^0.25.0") (default-features #t) (kind 0)) (crate-dep (name "strum_macros") (req "^0.25.1") (default-features #t) (kind 0)))) (hash "05x75wg9fwic6910y0hvqvnqr8h1f0khy4q1i6v8l6v0dl41mihk")))

(define-public crate-pride-term-0.3 (crate (name "pride-term") (vers "0.3.0") (deps (list (crate-dep (name "pico-args") (req "^0.5.0") (default-features #t) (kind 0)) (crate-dep (name "termion") (req "^2.0.1") (default-features #t) (kind 0)))) (hash "1dxwqgn8rmnsypmcs1001rp8837b8kh58g53n76yfb36mmf4ghcm")))

(define-public crate-pride-term-0.3 (crate (name "pride-term") (vers "0.3.1") (deps (list (crate-dep (name "pico-args") (req "^0.5.0") (default-features #t) (kind 0)) (crate-dep (name "termion") (req "^2.0.1") (default-features #t) (kind 0)))) (hash "082v8ayzpc0bxnvax6gh1l5i0r647qiizwdmwz9662jcwpsb38mm")))

(define-public crate-prideful-0.1 (crate (name "prideful") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "~2.27.0") (default-features #t) (kind 0)) (crate-dep (name "colored") (req "^2") (default-features #t) (kind 0)) (crate-dep (name "directories") (req "^4.0") (default-features #t) (kind 0)) (crate-dep (name "termion") (req "^1.5") (default-features #t) (kind 0)) (crate-dep (name "tui") (req "^0.12") (default-features #t) (kind 0)) (crate-dep (name "xdg") (req "^2.1") (default-features #t) (kind 0)) (crate-dep (name "yaml-rust") (req "^0.4") (default-features #t) (kind 0)))) (hash "1q0bx9vjaglvrwi9njfsfmdcg21x3f202j9gyd566n2kxs6isxhp")))

