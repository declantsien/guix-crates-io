(define-module (crates-io pr -h) #:use-module (crates-io))

(define-public crate-pr-has-issues-action-2 (crate (name "pr-has-issues-action") (vers "2.0.0") (deps (list (crate-dep (name "anyhow") (req "^1.0.70") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.160") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.96") (default-features #t) (kind 0)))) (hash "02q7cy4ad5mc89dj0in4vwg2ybpflyhq8yqkfd1l86lif6n83pkn")))

