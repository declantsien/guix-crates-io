(define-module (crates-io bz fq) #:use-module (crates-io))

(define-public crate-bzfquery-0.1 (crate (name "bzfquery") (vers "0.1.0") (hash "0l6z2rimpkf38ccbffipp0p40lxa2gikn0s6vxv33ybsjm7ykzc7") (features (quote (("default" "color") ("color"))))))

(define-public crate-bzfquery-1 (crate (name "bzfquery") (vers "1.0.0") (hash "1qgkcqdznzk2pm15pgiysnl3pnn7q81aw097wf6c8x9vlffqhdkg") (features (quote (("default" "color") ("color"))))))

(define-public crate-bzfquery-1 (crate (name "bzfquery") (vers "1.0.1") (hash "1hwh9v1gk53a9vflhdqc8k5m79qc70ld1ha3aa2v5m3bv244d60l") (features (quote (("default" "color") ("color"))))))

