(define-module (crates-io bz lc) #:use-module (crates-io))

(define-public crate-bzlcfg-0.1 (crate (name "bzlcfg") (vers "0.1.0") (deps (list (crate-dep (name "prettytable-rs") (req "^0.8.0") (default-features #t) (kind 0)) (crate-dep (name "recap") (req "^0.1.1") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.99") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.3") (default-features #t) (kind 0)))) (hash "15bvh4yqzc2drgspwa2ybp81ji1ri8grjckkmp6c4azlzj03phiy")))

