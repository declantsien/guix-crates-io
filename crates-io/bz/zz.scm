(define-module (crates-io bz zz) #:use-module (crates-io))

(define-public crate-bzzz-1 (crate (name "bzzz") (vers "1.0.0") (deps (list (crate-dep (name "regex") (req "^1.5") (default-features #t) (kind 0)))) (hash "0qkpx346skvapl6wl4k6zvqh68bqlps32nwng1a3m2z8g3hxpcr9")))

