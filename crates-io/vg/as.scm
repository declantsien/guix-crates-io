(define-module (crates-io vg as) #:use-module (crates-io))

(define-public crate-vgastream-rs-0.1 (crate (name "vgastream-rs") (vers "0.1.0") (deps (list (crate-dep (name "cursor-rs") (req "^0.1.2") (default-features #t) (kind 0)) (crate-dep (name "vga-rs") (req "^0.1.4") (default-features #t) (kind 0)))) (hash "185flcbwfj7bwl5mcps39ssn2qvq1m036wal62hvvmrxz8h0gzgv")))

(define-public crate-vgastream-rs-0.1 (crate (name "vgastream-rs") (vers "0.1.1") (deps (list (crate-dep (name "cursor-rs") (req "^0.1.4") (default-features #t) (kind 0)) (crate-dep (name "vga-rs") (req "^0.1.4") (default-features #t) (kind 0)))) (hash "1z00j1cy6n3hw782j52gjl7i4jmmv6aqhmr1jbhzk6lc1zq0brm1")))

(define-public crate-vgastream-rs-0.1 (crate (name "vgastream-rs") (vers "0.1.2") (deps (list (crate-dep (name "cursor-rs") (req "^0.1.4") (default-features #t) (kind 0)) (crate-dep (name "vga-rs") (req "^0.1.4") (default-features #t) (kind 0)))) (hash "1kcj64vpvy83slx22jncrsiq60b59cdj73dyfykzgk9ic12n6cc9")))

