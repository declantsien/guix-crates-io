(define-module (crates-io vg _e) #:use-module (crates-io))

(define-public crate-vg_errortools-0.1 (crate (name "vg_errortools") (vers "0.1.0") (deps (list (crate-dep (name "thiserror") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.19") (features (quote ("fs"))) (optional #t) (default-features #t) (kind 0)))) (hash "19m484c9naf79bs0nig7ibk6yl5n41484pnnqymgi4w4d0r7j3cg") (v 2) (features2 (quote (("tokio" "dep:tokio"))))))

