(define-module (crates-io vg eb) #:use-module (crates-io))

(define-public crate-vgebrev-first-crate-0.1 (crate (name "vgebrev-first-crate") (vers "0.1.0") (hash "0lhl5qbhmqrg6gfpv0yfk9x7dy78yba31wy8v5af432nq3xj49zg") (yanked #t)))

(define-public crate-vgebrev-first-crate-0.2 (crate (name "vgebrev-first-crate") (vers "0.2.0") (hash "048w6snz478mkjqd96571dil6jw74qha0lyk483djm0fgcs50xps")))

