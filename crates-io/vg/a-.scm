(define-module (crates-io vg a-) #:use-module (crates-io))

(define-public crate-vga-emu-0.1 (crate (name "vga-emu") (vers "0.1.0") (deps (list (crate-dep (name "sdl2") (req "^0.34.5") (features (quote ("ttf"))) (kind 0)))) (hash "07c1f1jc9sg1m22zy8fis3v83ibflpzjvsd6pblgs4q19l5qa0ib")))

(define-public crate-vga-emu-0.2 (crate (name "vga-emu") (vers "0.2.0") (deps (list (crate-dep (name "sdl2") (req "^0.35.2") (features (quote ("ttf"))) (kind 0)))) (hash "01iqz11m8bb847fn8g8dfs1sv5iv8za5r7pzw0rjlq716yrdxib0")))

(define-public crate-vga-emu-0.2 (crate (name "vga-emu") (vers "0.2.1") (deps (list (crate-dep (name "sdl2") (req "^0.35.2") (features (quote ("ttf"))) (kind 0)))) (hash "104cb2ypgv6n6vf1a01z8bvl25mcckf7hzhpsbf14n84iixmmw28")))

(define-public crate-vga-emu-0.3 (crate (name "vga-emu") (vers "0.3.0") (deps (list (crate-dep (name "sdl2") (req "^0.35.2") (features (quote ("ttf"))) (kind 0)))) (hash "0h3yxcm08fkiaf918qnj0wxgpvxfjcn07rxgjxmydsmswmdzb1mk")))

(define-public crate-vga-emu-0.4 (crate (name "vga-emu") (vers "0.4.0") (deps (list (crate-dep (name "sdl2") (req "^0.35.2") (features (quote ("ttf"))) (kind 0)))) (hash "1x4fl4d212k0x7ppp7pyy5nfvc7wyxgflhpiq7dqd3j7906g17qm") (features (quote (("web") ("sdl"))))))

(define-public crate-vga-emu-0.4 (crate (name "vga-emu") (vers "0.4.1") (deps (list (crate-dep (name "sdl2") (req "^0.35.2") (features (quote ("ttf"))) (optional #t) (kind 0)) (crate-dep (name "wasm-bindgen") (req "^0.2.87") (optional #t) (default-features #t) (kind 0)))) (hash "0h617rhl2c0jzhppdwspdzkijsdmc0dz9gclgxgw4j6alzzzldmf") (v 2) (features2 (quote (("web" "dep:wasm-bindgen") ("sdl" "dep:sdl2"))))))

(define-public crate-vga-emu-0.5 (crate (name "vga-emu") (vers "0.5.0") (deps (list (crate-dep (name "async-std") (req "^1.12.0") (features (quote ("unstable"))) (default-features #t) (kind 0)) (crate-dep (name "js-sys") (req "^0.3.65") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "sdl2") (req "^0.35.2") (features (quote ("ttf"))) (optional #t) (kind 0)) (crate-dep (name "wasm-bindgen") (req "^0.2.88") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "wasm-bindgen-futures") (req "^0.4.38") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "web-sys") (req "^0.3.65") (features (quote ("Document" "Window" "CanvasRenderingContext2d" "HtmlCanvasElement" "ImageData" "KeyboardEvent" "console"))) (optional #t) (default-features #t) (kind 0)))) (hash "05g8mmg2xrjpll2353aap6hzb7x4n3r7d7yw139z1g9g16nwbvnb") (v 2) (features2 (quote (("web" "dep:wasm-bindgen" "dep:wasm-bindgen-futures" "dep:web-sys" "dep:js-sys") ("sdl" "dep:sdl2"))))))

(define-public crate-vga-emu-0.5 (crate (name "vga-emu") (vers "0.5.1") (deps (list (crate-dep (name "async-std") (req "^1.12.0") (features (quote ("unstable"))) (default-features #t) (kind 0)) (crate-dep (name "js-sys") (req "^0.3.65") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "sdl2") (req "^0.35.2") (features (quote ("ttf"))) (optional #t) (kind 0)) (crate-dep (name "wasm-bindgen") (req "^0.2.88") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "wasm-bindgen-futures") (req "^0.4.38") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "web-sys") (req "^0.3.65") (features (quote ("Document" "Window" "CanvasRenderingContext2d" "HtmlCanvasElement" "ImageData" "KeyboardEvent" "console"))) (optional #t) (default-features #t) (kind 0)))) (hash "0vbc5ak6qv8jphh1mrhi8wyarxwyh8ikvrg8rzxzv6vvk7iqbbpm") (v 2) (features2 (quote (("web" "dep:wasm-bindgen" "dep:wasm-bindgen-futures" "dep:web-sys" "dep:js-sys") ("sdl" "dep:sdl2"))))))

(define-public crate-vga-emu-0.5 (crate (name "vga-emu") (vers "0.5.3") (deps (list (crate-dep (name "async-std") (req "^1.12.0") (features (quote ("unstable"))) (default-features #t) (kind 0)) (crate-dep (name "js-sys") (req "^0.3.67") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "sdl2") (req "^0.36.0") (features (quote ("ttf"))) (optional #t) (kind 0)) (crate-dep (name "wasm-bindgen") (req "^0.2.90") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "wasm-bindgen-futures") (req "^0.4.40") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "web-sys") (req "^0.3.67") (features (quote ("Document" "Window" "CanvasRenderingContext2d" "HtmlCanvasElement" "ImageData" "KeyboardEvent" "console"))) (optional #t) (default-features #t) (kind 0)))) (hash "00sw7mcyjn4rs5n0a21lh09c8z39h90prmwh8302n4zhhis3wdnr") (v 2) (features2 (quote (("web" "dep:wasm-bindgen" "dep:wasm-bindgen-futures" "dep:web-sys" "dep:js-sys") ("sdl" "dep:sdl2"))))))

(define-public crate-vga-emu-0.6 (crate (name "vga-emu") (vers "0.6.0") (deps (list (crate-dep (name "async-std") (req "^1.12.0") (features (quote ("unstable"))) (default-features #t) (kind 0)) (crate-dep (name "js-sys") (req "^0.3.69") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "sdl2") (req "^0.36.0") (features (quote ("ttf"))) (optional #t) (kind 0)) (crate-dep (name "wasm-bindgen") (req "^0.2.92") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "wasm-bindgen-futures") (req "^0.4.42") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "web-sys") (req "^0.3.69") (features (quote ("Document" "Window" "CanvasRenderingContext2d" "HtmlCanvasElement" "ImageData" "KeyboardEvent" "console"))) (optional #t) (default-features #t) (kind 0)))) (hash "0551xdnhgs4gj2m3r201nymcz8rwa6yjnxhr5xazlbfc4x5qsqbr") (v 2) (features2 (quote (("web" "dep:wasm-bindgen" "dep:wasm-bindgen-futures" "dep:web-sys" "dep:js-sys") ("sdl" "dep:sdl2"))))))

(define-public crate-vga-figures-0.1 (crate (name "vga-figures") (vers "0.1.0") (deps (list (crate-dep (name "vga") (req "^0.2.4") (default-features #t) (kind 0)))) (hash "1r489crisa88irxab44q21qvkzflknr18i6dsbzq27qnmcqld3nm")))

(define-public crate-vga-figures-0.2 (crate (name "vga-figures") (vers "0.2.0") (deps (list (crate-dep (name "vga") (req "^0.2.4") (default-features #t) (kind 0)))) (hash "0j2bi2rz28yff3i0hvdqyirbwz8279a28sd9d39lyf96hw2rx99k")))

(define-public crate-vga-figures-0.2 (crate (name "vga-figures") (vers "0.2.1") (deps (list (crate-dep (name "vga") (req "^0.2.9") (default-features #t) (kind 0)))) (hash "1nw6900j7mssskkamb9gwwblvnnq5xnyfknpnd3f7v41dhl62flp")))

(define-public crate-vga-framebuffer-0.2 (crate (name "vga-framebuffer") (vers "0.2.0") (deps (list (crate-dep (name "console-traits") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "term") (req "^0.5") (default-features #t) (kind 2)))) (hash "00azxqzyy1lbgppjr33wxpx3b5vahrfdva6l8l4kmpnylm37qwv1")))

(define-public crate-vga-framebuffer-0.2 (crate (name "vga-framebuffer") (vers "0.2.1") (deps (list (crate-dep (name "console-traits") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "term") (req "^0.5") (default-features #t) (kind 2)))) (hash "0d5jqlar9frmpzwf8g3lcf0i55p6dld4k2a8qn6i3dmw6xf7wvqs")))

(define-public crate-vga-framebuffer-0.3 (crate (name "vga-framebuffer") (vers "0.3.0") (deps (list (crate-dep (name "console-traits") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "term") (req "^0.5") (default-features #t) (kind 2)))) (hash "108xs91adz7r1695gal20kab2mjkzfrk40dyw76af7ay8qzi5zmh")))

(define-public crate-vga-framebuffer-0.4 (crate (name "vga-framebuffer") (vers "0.4.0") (deps (list (crate-dep (name "console-traits") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "term") (req "^0.5") (default-features #t) (kind 2)))) (hash "1vkn5yrp8yrqpah6ffjr1mhqj4rvb386003762cbzilq1y5kbsrl")))

(define-public crate-vga-framebuffer-0.5 (crate (name "vga-framebuffer") (vers "0.5.0") (deps (list (crate-dep (name "console-traits") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "term") (req "^0.5") (default-features #t) (kind 2)))) (hash "0wkfzfflrsbfwvb30g8y9n68m3kqbdln175fpnxp99cpx8h5kr7w")))

(define-public crate-vga-framebuffer-0.6 (crate (name "vga-framebuffer") (vers "0.6.0") (deps (list (crate-dep (name "console-traits") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "term") (req "^0.5") (default-features #t) (kind 2)))) (hash "1b4wnvqmdkbfmn3h8xvbpw9pm7b426fa6qia9wb2w3y9vwczp5hz")))

(define-public crate-vga-framebuffer-0.7 (crate (name "vga-framebuffer") (vers "0.7.0") (deps (list (crate-dep (name "console-traits") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "term") (req "^0.5") (default-features #t) (kind 2)))) (hash "1sahla7bybgrpi35s540yjj61v0z6h3qcj3vg7b9ii4iazdgs99r")))

(define-public crate-vga-framebuffer-0.7 (crate (name "vga-framebuffer") (vers "0.7.2") (deps (list (crate-dep (name "console-traits") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "const-ft") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "term") (req "^0.5") (default-features #t) (kind 2)))) (hash "0mycijkv423y0n4c19s6iwjc7nmbg8dmg3qzg0b3r56gvp4sl3g1") (features (quote (("const_fn" "const-ft/const_fn"))))))

(define-public crate-vga-framebuffer-0.7 (crate (name "vga-framebuffer") (vers "0.7.3") (deps (list (crate-dep (name "console-traits") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "const-ft") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "term") (req "^0.5") (default-features #t) (kind 2)))) (hash "06ha5n5yl21n5wimh7n94bjh26achd4273r84pk1a5pfzpx42zag") (features (quote (("const_fn" "const-ft/const_fn"))))))

(define-public crate-vga-framebuffer-0.7 (crate (name "vga-framebuffer") (vers "0.7.4") (deps (list (crate-dep (name "console-traits") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "const-ft") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "term") (req "^0.5") (default-features #t) (kind 2)))) (hash "1mnp3gxlg4j889vzijjkhfgpyg9qdsw0csyhl84z8w3vam7zv1pk") (features (quote (("const_fn" "const-ft/const_fn"))))))

(define-public crate-vga-framebuffer-0.7 (crate (name "vga-framebuffer") (vers "0.7.5") (deps (list (crate-dep (name "console-traits") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "const-ft") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "term") (req "^0.5") (default-features #t) (kind 2)))) (hash "07xabm42fd07zg8h2lc2ryq80yrrvwj35r2r9qbsv98p098xzivw") (features (quote (("const_fn" "const-ft/const_fn"))))))

(define-public crate-vga-framebuffer-0.8 (crate (name "vga-framebuffer") (vers "0.8.0") (deps (list (crate-dep (name "console-traits") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "const-ft") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "term") (req "^0.5") (default-features #t) (kind 2)))) (hash "1si00y0pfj0l59irn1isqwl3bcmr6c8lqbhakqp011r4hhs62j1m") (features (quote (("const_fn" "const-ft/const_fn"))))))

(define-public crate-vga-framebuffer-0.8 (crate (name "vga-framebuffer") (vers "0.8.1") (deps (list (crate-dep (name "console-traits") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "const-ft") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "term") (req "^0.5") (default-features #t) (kind 2)))) (hash "199z7gngvmvl83qlvdn8imwdjw036xn6lndw8qmiw24a03b515fx") (features (quote (("const_fn" "const-ft/const_fn"))))))

(define-public crate-vga-framebuffer-0.9 (crate (name "vga-framebuffer") (vers "0.9.0") (deps (list (crate-dep (name "console-traits") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "const-ft") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "term") (req "^0.5") (default-features #t) (kind 2)))) (hash "1hdy5lrydparlsdp7szlgbarxm24xkyspvxwgfiivlj96c7604rf") (features (quote (("const_fn" "const-ft/const_fn"))))))

(define-public crate-vga-rs-0.1 (crate (name "vga-rs") (vers "0.1.0") (deps (list (crate-dep (name "vgainfo-rs") (req "^0.1.1") (default-features #t) (kind 0)))) (hash "1f7ym8c4q48ykvpd5dh4j8w5isfnaprxrg9aw12xhbqa8lwvjjzm") (yanked #t)))

(define-public crate-vga-rs-0.1 (crate (name "vga-rs") (vers "0.1.1") (deps (list (crate-dep (name "vgainfo-rs") (req "^0.1.1") (default-features #t) (kind 0)))) (hash "0xcghmc0mgs3k2x0iihxp243n4vs62gnqzc8jhqpmhl04x6810hj") (yanked #t)))

(define-public crate-vga-rs-0.1 (crate (name "vga-rs") (vers "0.1.2") (deps (list (crate-dep (name "vgainfo-rs") (req "^0.1.1") (default-features #t) (kind 0)))) (hash "1b3lfp958zp4wadlkspc5yfk82fqswr0zq1lc23h9mp5dgpxp6n1") (yanked #t)))

(define-public crate-vga-rs-0.1 (crate (name "vga-rs") (vers "0.1.3") (deps (list (crate-dep (name "vgainfo-rs") (req "^0.1.1") (default-features #t) (kind 0)))) (hash "0hr7z3q614a28r5nv54aamf10cqcbfy7sldrf35gnxw26wd9gn33") (yanked #t)))

(define-public crate-vga-rs-0.1 (crate (name "vga-rs") (vers "0.1.4") (deps (list (crate-dep (name "vgainfo-rs") (req "^0.1.1") (default-features #t) (kind 0)))) (hash "1nb3xhh226d495qisvnzwysgy4p6i63k622h07na6wdmaz7ddhvf")))

