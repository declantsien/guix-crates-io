(define-module (crates-io vg a_) #:use-module (crates-io))

(define-public crate-vga_buffer_rs-0.1 (crate (name "vga_buffer_rs") (vers "0.1.0") (deps (list (crate-dep (name "volatile") (req "^0.2") (default-features #t) (kind 0)))) (hash "1hhc4s4sahirh2bd4cdvf9da0yccpp8jjnrwhscf0nkzkjbnjgrk")))

(define-public crate-vga_buffer_rs-0.1 (crate (name "vga_buffer_rs") (vers "0.1.1") (deps (list (crate-dep (name "volatile") (req "^0.2") (default-features #t) (kind 0)))) (hash "19jdjxz0lnaabjdfhnl72w91n91l6plqf1bcgg3bdvfc2m8hlb5q")))

(define-public crate-vga_buffer_rs-0.1 (crate (name "vga_buffer_rs") (vers "0.1.2") (deps (list (crate-dep (name "volatile") (req "^0.2") (default-features #t) (kind 0)))) (hash "11h4gnykqzmqxxndn0il7s03cvd06aq363myvl4nw83gzjhmdjpj")))

