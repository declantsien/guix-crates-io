(define-module (crates-io mb _g) #:use-module (crates-io))

(define-public crate-mb_goban-0.0.0 (crate (name "mb_goban") (vers "0.0.0") (hash "1pqq70zvcn7b91lqifsil69ydr8k83hr3sk7453hfmg1ds32dcp7")))

(define-public crate-mb_goban-0.1 (crate (name "mb_goban") (vers "0.1.0") (deps (list (crate-dep (name "fxhash") (req "^0.2.1") (default-features #t) (kind 0)))) (hash "0f2yl7s0wn6nf40p3s6p4741jm4n28b2wq6h7ggcsmb7p13lv37f")))

(define-public crate-mb_goban-0.2 (crate (name "mb_goban") (vers "0.2.0") (deps (list (crate-dep (name "fxhash") (req "^0.2.1") (default-features #t) (kind 0)))) (hash "1maxbvv68sah0pv7n3yv63cvhi5xb8k19c16iza13vq9hxyd5fcp")))

