(define-module (crates-io mb on) #:use-module (crates-io))

(define-public crate-mbon-0.1 (crate (name "mbon") (vers "0.1.0") (deps (list (crate-dep (name "byteorder") (req "^1.4") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 2)))) (hash "0pkwv9a8nmv04477ypv02v5p0ifphlr2kc0xqjy93075wih64naf")))

(define-public crate-mbon-0.1 (crate (name "mbon") (vers "0.1.1") (deps (list (crate-dep (name "byteorder") (req "^1.4") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 2)))) (hash "1knnq9j8v10nl5r62zl7w8i6msrim52a6hzgc3qy9p9dl0xlmqg3")))

(define-public crate-mbon-0.2 (crate (name "mbon") (vers "0.2.0") (deps (list (crate-dep (name "async-recursion") (req "^1.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "byteorder") (req "^1.4") (default-features #t) (kind 0)) (crate-dep (name "futures") (req "^0.3") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 2)))) (hash "132q6x1yxwkgg4vvcbsi036vcgmvh40ss7zwxasvzl2yzjg3dmrx") (v 2) (features2 (quote (("async" "dep:futures" "dep:async-recursion"))))))

