(define-module (crates-io mb ox) #:use-module (crates-io))

(define-public crate-mbox-0.1 (crate (name "mbox") (vers "0.1.0") (deps (list (crate-dep (name "libc") (req "^0.2.12") (default-features #t) (kind 0)) (crate-dep (name "rustc_version") (req "^0.1.7") (default-features #t) (kind 1)) (crate-dep (name "semver") (req "^0.1.20") (default-features #t) (kind 1)))) (hash "1da16s5dh3j3fq37l11p3m00wvy8qb1jmny3rgj1k6fd94fkqm14") (features (quote (("no-std")))) (yanked #t)))

(define-public crate-mbox-0.1 (crate (name "mbox") (vers "0.1.1") (deps (list (crate-dep (name "libc") (req "^0.2.13") (default-features #t) (kind 0)) (crate-dep (name "rustc_version") (req "^0.1.7") (default-features #t) (kind 1)) (crate-dep (name "semver") (req "^0.1.20") (default-features #t) (kind 1)))) (hash "1qamf32rzv3d9rz3868djsv39srh6380qcp02clxfhv4kckczyiw") (features (quote (("no-std")))) (yanked #t)))

(define-public crate-mbox-0.1 (crate (name "mbox") (vers "0.1.2") (deps (list (crate-dep (name "libc") (req "^0.2.13") (default-features #t) (kind 0)) (crate-dep (name "rustc_version") (req "^0.1.7") (default-features #t) (kind 1)) (crate-dep (name "semver") (req "^0.1.20") (default-features #t) (kind 1)))) (hash "038jx224zknynds78qi0x87kyv0splih3lwz3lgm720hj1a8nz9b") (features (quote (("no-std")))) (yanked #t)))

(define-public crate-mbox-0.1 (crate (name "mbox") (vers "0.1.3") (deps (list (crate-dep (name "libc") (req "^0.2.15") (default-features #t) (kind 0)) (crate-dep (name "rustc_version") (req "^0.1.7") (default-features #t) (kind 1)) (crate-dep (name "semver") (req "^0.1.20") (default-features #t) (kind 1)))) (hash "17ass6fjbrg8xk40fc8xq032ijwp2qdamxis86p1nk1xqav5qw5q") (features (quote (("no-std")))) (yanked #t)))

(define-public crate-mbox-0.1 (crate (name "mbox") (vers "0.1.4") (deps (list (crate-dep (name "libc") (req "^0.2.17") (default-features #t) (kind 0)) (crate-dep (name "rustc_version") (req "^0.1.7") (default-features #t) (kind 1)) (crate-dep (name "semver") (req "^0.1.20") (default-features #t) (kind 1)))) (hash "0xxylfhxqlyfif0fxjdmrhz1hm24sp5497hqw7aa1a6f7xbhr73j") (features (quote (("no-std")))) (yanked #t)))

(define-public crate-mbox-0.1 (crate (name "mbox") (vers "0.1.6") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "rustc_version") (req "^0.2") (default-features #t) (kind 1)) (crate-dep (name "semver") (req "^0.6") (default-features #t) (kind 1)))) (hash "180158nv6zlar2lilywdh68bz8pqg5xyvy8daqmgm3p3ijgiwir6") (features (quote (("no-std")))) (yanked #t)))

(define-public crate-mbox-0.2 (crate (name "mbox") (vers "0.2.0") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "rustc_version") (req "^0.2") (default-features #t) (kind 1)) (crate-dep (name "semver") (req "^0.6") (default-features #t) (kind 1)))) (hash "0yfks16yaqgd4i11dz7lp870jxnc5ygjqbjv5im5jay406vz28xw") (features (quote (("no-std")))) (yanked #t)))

(define-public crate-mbox-0.3 (crate (name "mbox") (vers "0.3.0") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "rustc_version") (req "^0.2") (default-features #t) (kind 1)))) (hash "08fr4k67nps755w960k8ja99z9xjn1fc6cvmdbiya8ldv9m9ya35") (features (quote (("std") ("default" "std")))) (yanked #t)))

(define-public crate-mbox-0.3 (crate (name "mbox") (vers "0.3.1") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "rustc_version") (req "^0.2") (default-features #t) (kind 1)))) (hash "1n9dacmsd3w4yzz5p95vaizv5j740l80g6zkmzqyygirp8isxa5y") (features (quote (("std") ("default" "std")))) (yanked #t)))

(define-public crate-mbox-0.4 (crate (name "mbox") (vers "0.4.0") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "rustc_version") (req "^0.2") (default-features #t) (kind 1)) (crate-dep (name "stable_deref_trait") (req "^1.0") (kind 0)))) (hash "12xbcm1c1pwdrn2fn64zm317kik7pzj0pjxc2r6fgfdzmxsyvs3y") (features (quote (("std") ("default" "std")))) (yanked #t)))

(define-public crate-mbox-0.4 (crate (name "mbox") (vers "0.4.1") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "rustc_version") (req "^0.2") (default-features #t) (kind 1)) (crate-dep (name "stable_deref_trait") (req "^1.0") (kind 0)))) (hash "1cqk0lj16ki1mxmb4sai0scb23a3db5dgnclnkshdbrrg9kh5vn7") (features (quote (("std") ("default" "std")))) (yanked #t)))

(define-public crate-mbox-0.4 (crate (name "mbox") (vers "0.4.3") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "rustc_version") (req "^0.2") (default-features #t) (kind 1)) (crate-dep (name "stable_deref_trait") (req "^1.0") (kind 0)))) (hash "1ll9vqwkyd1097hj43g5l7f2jcsv6k0vzz9bi4b8ak7vb8pzs5jf") (features (quote (("std") ("default" "std")))) (yanked #t)))

(define-public crate-mbox-0.5 (crate (name "mbox") (vers "0.5.0") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "rustc_version") (req "^0.2") (default-features #t) (kind 1)) (crate-dep (name "stable_deref_trait") (req "^1.0") (kind 0)))) (hash "0q93y9n1fbnn2g6qzjfc9jkl7hzkl9f5m50ghk50n0bgkm3yafjy") (features (quote (("std") ("default" "std")))) (yanked #t)))

(define-public crate-mbox-0.4 (crate (name "mbox") (vers "0.4.4") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "rustc_version") (req "^0.2") (default-features #t) (kind 1)) (crate-dep (name "stable_deref_trait") (req "^1.0") (kind 0)))) (hash "1w5jfipbzkbx22shygr9j0s9d0z8y4v2ah0mbz05bifmhm8i0q9c") (features (quote (("std") ("default" "std")))) (yanked #t)))

(define-public crate-mbox-0.6 (crate (name "mbox") (vers "0.6.0") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "rustc_version") (req "^0.3") (default-features #t) (kind 1)) (crate-dep (name "stable_deref_trait") (req "^1.0") (kind 0)))) (hash "1mkmxgjq7gr2mrwi02fq7a5zajh6rdfga7ijlhdd3ak39p1xb20g") (features (quote (("std") ("default" "std")))) (yanked #t)))

(define-public crate-mbox-0.7 (crate (name "mbox") (vers "0.7.0") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "stable_deref_trait") (req "^1.0") (optional #t) (kind 0)))) (hash "1k1xgv7sbrm5m1xfm4bhjy7f471hz8r93bwrjlwp2xxg08zn27na") (features (quote (("std") ("nightly") ("default" "std" "stable_deref_trait")))) (yanked #t) (rust-version "1.36.0")))

(define-public crate-mbox-0.6 (crate (name "mbox") (vers "0.6.1") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "once_cell") (req ">=1, <=1.14.0") (default-features #t) (kind 1)) (crate-dep (name "pest") (req ">=2, <=2.2.0") (default-features #t) (kind 1)) (crate-dep (name "proc-macro2") (req ">=1, <=1.0.65") (default-features #t) (kind 1)) (crate-dep (name "rustc_version") (req "^0.3") (default-features #t) (kind 1)) (crate-dep (name "stable_deref_trait") (req "^1.0") (optional #t) (kind 0)) (crate-dep (name "syn") (req ">=1, <=1.0.107") (default-features #t) (kind 1)) (crate-dep (name "thiserror") (req ">=1, <=1.0.39") (default-features #t) (kind 1)) (crate-dep (name "ucd-trie") (req ">=0.1, <=0.1.4") (default-features #t) (kind 1)))) (hash "0blw3s50yb7a0pzwsi57ayf2rcrkl3xq509mmx3hdvbv5q877ywy") (features (quote (("std") ("nightly") ("default" "std" "stable_deref_trait")))) (rust-version "1.36.0")))

(define-public crate-mbox-0.7 (crate (name "mbox") (vers "0.7.1") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "stable_deref_trait") (req "^1.0") (optional #t) (kind 0)))) (hash "1pn67fg52nwi2pk2j1nqlwgh477ygsz3znf6kxkqqkmwmnp45l96") (features (quote (("std") ("nightly") ("default" "std" "stable_deref_trait")))) (rust-version "1.36.0")))

(define-public crate-mbox-reader-0.1 (crate (name "mbox-reader") (vers "0.1.0") (deps (list (crate-dep (name "memmap") (req "^0.5") (default-features #t) (kind 0)))) (hash "0sj4hfx3770k0mv94pmw9yllm503rhx523jhbwwlspwn2ygawl6y")))

(define-public crate-mbox-reader-0.2 (crate (name "mbox-reader") (vers "0.2.0") (deps (list (crate-dep (name "memmap") (req "^0.7") (default-features #t) (kind 0)))) (hash "1az2x4i61cwqna4mjdihw8zsfaq1amakbizsf7nwxjm8q1ryjcb2")))

