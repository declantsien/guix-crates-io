(define-module (crates-io mb _s) #:use-module (crates-io))

(define-public crate-mb_sgf-0.1 (crate (name "mb_sgf") (vers "0.1.0") (deps (list (crate-dep (name "pest") (req "^2.3.0") (default-features #t) (kind 0)) (crate-dep (name "pest_derive") (req "^2.3.0") (default-features #t) (kind 0)))) (hash "0j8wkd5kc01xrzgbkdm3qqcn9r6b6dz4sz7bng2lgc80kq2x8wqn")))

