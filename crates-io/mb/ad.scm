(define-module (crates-io mb ad) #:use-module (crates-io))

(define-public crate-mbaduk_ui-0.1 (crate (name "mbaduk_ui") (vers "0.1.0") (deps (list (crate-dep (name "eframe") (req "^0.19.0") (default-features #t) (kind 0)) (crate-dep (name "mb_goban") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "0j0165rw7bnjy4rs38bgvb156xv3j8fk94didgxp7z9pnvcb7jip")))

