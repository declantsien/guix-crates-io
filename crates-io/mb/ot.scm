(define-module (crates-io mb ot) #:use-module (crates-io))

(define-public crate-mbot-0.0.0 (crate (name "mbot") (vers "0.0.0") (hash "002bsmb6wvr4bw9sdrxq450r2pcphk274rgrpz2zrxl99kyg7mzq")))

(define-public crate-mbot_proc_macro_helpers-0.0.1 (crate (name "mbot_proc_macro_helpers") (vers "0.0.1") (deps (list (crate-dep (name "derivative") (req "^2.2.0") (default-features #t) (kind 0)) (crate-dep (name "derive-syn-parse") (req "^0.1.4") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1.0.24") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.9") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.64") (default-features #t) (kind 0)))) (hash "14v6prl4vmqjk56kapx5rb4d0zy0sipvx7b6ay2y8bjigg3l9nx5")))

(define-public crate-mbot_proc_macro_helpers-0.0.2 (crate (name "mbot_proc_macro_helpers") (vers "0.0.2") (deps (list (crate-dep (name "derivative") (req "^2.2.0") (default-features #t) (kind 0)) (crate-dep (name "derive-syn-parse") (req "^0.1.4") (default-features #t) (kind 0)) (crate-dep (name "indexmap") (req "^1.7.0") (default-features #t) (kind 0)) (crate-dep (name "itertools") (req "^0.10.1") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1.0.24") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.9") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.64") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "1lswxg4rxws31d43sm3ml8fkilr6dwz8x8bv9k87l29cxhpwd88w")))

