(define-module (crates-io mb r-) #:use-module (crates-io))

(define-public crate-mbr-nostd-0.1 (crate (name "mbr-nostd") (vers "0.1.0") (deps (list (crate-dep (name "byteorder") (req "^1.2.7") (kind 0)))) (hash "1xr98dg2ipipl5992pags57z1hazbfyn926w4dj4vqczacpb3rvh")))

