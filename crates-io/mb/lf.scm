(define-module (crates-io mb lf) #:use-module (crates-io))

(define-public crate-mblf-0.1 (crate (name "mblf") (vers "0.1.0") (deps (list (crate-dep (name "anyhow") (req "^1.0.44") (default-features #t) (kind 0)) (crate-dep (name "pest") (req "^2.1.3") (default-features #t) (kind 0)) (crate-dep (name "pest_derive") (req "^2.1.0") (default-features #t) (kind 0)) (crate-dep (name "string-builder") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.3.23") (default-features #t) (kind 0)))) (hash "08mcg5xxa6ddbzwqqbxqqisavy59sf995mb9nfl3g0pk5nkf07j8")))

