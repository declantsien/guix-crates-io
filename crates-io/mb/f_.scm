(define-module (crates-io mb f_) #:use-module (crates-io))

(define-public crate-mbf_gtf-0.1 (crate (name "mbf_gtf") (vers "0.1.3") (deps (list (crate-dep (name "hashbrown") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "pyo3") (req "^0.6.0-alpha.4") (features (quote ("extension-module"))) (default-features #t) (kind 0)))) (hash "1fgdzi0kzhf3llw88zzikbglf0n3xq5vpvs2za5bppaslqbcak84")))

(define-public crate-mbf_gtf-0.1 (crate (name "mbf_gtf") (vers "0.1.4") (deps (list (crate-dep (name "flate2") (req "^1.0.7") (default-features #t) (kind 0)) (crate-dep (name "hashbrown") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "pyo3") (req "^0.6.0-alpha.4") (features (quote ("extension-module"))) (default-features #t) (kind 0)))) (hash "12if2bygbwfgwrvhr5qr1v3frgaw1rmmvy83bdfxbysh1g6bnrxd")))

(define-public crate-mbf_gtf-0.2 (crate (name "mbf_gtf") (vers "0.2.0") (deps (list (crate-dep (name "flate2") (req "^1.0.7") (default-features #t) (kind 0)) (crate-dep (name "hashbrown") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "pyo3") (req "^0.6.0-alpha.4") (features (quote ("extension-module"))) (default-features #t) (kind 0)))) (hash "1k2mcbidnb0cyb9alhab3x3h2zmvpd11cx1bv3lbbdwvbf56nb28")))

