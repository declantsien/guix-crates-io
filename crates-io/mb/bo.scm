(define-module (crates-io mb bo) #:use-module (crates-io))

(define-public crate-mbbook-0.1 (crate (name "mbbook") (vers "0.1.0") (deps (list (crate-dep (name "mdbook-svgbob2") (req "^0.3.0") (default-features #t) (kind 0)))) (hash "09fc01lnwbhcxgw7kg9nz0r7y80fhaj9dcj83ay4906irymrgv38") (yanked #t)))

