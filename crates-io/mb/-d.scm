(define-module (crates-io mb -d) #:use-module (crates-io))

(define-public crate-mb-dev-0.1 (crate (name "mb-dev") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^3.0.0-beta.2") (default-features #t) (kind 0)) (crate-dep (name "dns-lookup") (req "^1.0.6") (default-features #t) (kind 0)))) (hash "08g0ldppii2cisvjvm7ccy57a6gzgxwbxkcgbh83n2gb2k2hnphf")))

(define-public crate-mb-dev-0.1 (crate (name "mb-dev") (vers "0.1.1") (deps (list (crate-dep (name "clap") (req "^3.0.0-beta.2") (default-features #t) (kind 0)) (crate-dep (name "dns-lookup") (req "^1.0.6") (default-features #t) (kind 0)))) (hash "1qzbczqyjfc51wyzndhm7crff5ghwmyr225qm9g1igk6zarrjdgr")))

(define-public crate-mb-dev-0.1 (crate (name "mb-dev") (vers "0.1.2") (deps (list (crate-dep (name "clap") (req "^3.0.0-beta.2") (default-features #t) (kind 0)) (crate-dep (name "dns-lookup") (req "^1.0.6") (default-features #t) (kind 0)))) (hash "0xb7jwi0iwi5v612dfzsjmp6lfz0nf36a6yyphwdkpipvva5j6ya")))

(define-public crate-mb-dev-0.1 (crate (name "mb-dev") (vers "0.1.3") (deps (list (crate-dep (name "clap") (req "^3.0.0-beta.4") (default-features #t) (kind 0)) (crate-dep (name "dns-lookup") (req "^1.0.8") (default-features #t) (kind 0)))) (hash "167207hz0s84p10m0gjwlx660bxx08bynyfl44w5bj6pnxnnvvhm")))

(define-public crate-mb-dev-0.1 (crate (name "mb-dev") (vers "0.1.4") (deps (list (crate-dep (name "clap") (req "^3.0.0-beta.4") (default-features #t) (kind 0)) (crate-dep (name "dns-lookup") (req "^1.0.8") (default-features #t) (kind 0)))) (hash "11w0fclysikak0i2shqpxpx2dlkvl31hq37d1554sc0v2ijfj8z6")))

(define-public crate-mb-dev-0.1 (crate (name "mb-dev") (vers "0.1.5") (deps (list (crate-dep (name "clap") (req "^3.0.0-beta.4") (default-features #t) (kind 0)) (crate-dep (name "dns-lookup") (req "^1.0.8") (default-features #t) (kind 0)))) (hash "1lv94x2vyashwq0imk5jhvada5viyq6vzivpc8v0klir0nd4bnbi")))

(define-public crate-mb-dev-0.1 (crate (name "mb-dev") (vers "0.1.6") (deps (list (crate-dep (name "clap") (req "^3.0.5") (features (quote ("cargo"))) (default-features #t) (kind 0)) (crate-dep (name "dns-lookup") (req "^1.0.8") (default-features #t) (kind 0)))) (hash "05qzqzd0f0ya3sfnr2drd3b4vckclgc445j1s2sqbqrq6jyv7lh6")))

(define-public crate-mb-dev-0.1 (crate (name "mb-dev") (vers "0.1.7") (deps (list (crate-dep (name "clap") (req "^3.0.5") (features (quote ("cargo"))) (default-features #t) (kind 0)) (crate-dep (name "dns-lookup") (req "^1.0.8") (default-features #t) (kind 0)))) (hash "07mvhdh03aj3l7q5kld2lfav17iga6jn8pdwd4s0h10n0zj6fqcy")))

(define-public crate-mb-dev-0.1 (crate (name "mb-dev") (vers "0.1.8") (deps (list (crate-dep (name "clap") (req "^3.1.17") (features (quote ("derive" "cargo"))) (default-features #t) (kind 0)) (crate-dep (name "dns-lookup") (req "^1.0.8") (default-features #t) (kind 0)))) (hash "0kcf408m0rir084s8w594b3r6ca1zvy9s21zag5vvg26yp57i1gw")))

(define-public crate-mb-dev-0.1 (crate (name "mb-dev") (vers "0.1.9") (deps (list (crate-dep (name "clap") (req "^3.1.18") (features (quote ("derive" "cargo"))) (default-features #t) (kind 0)) (crate-dep (name "dns-lookup") (req "^1.0.8") (default-features #t) (kind 0)))) (hash "0954s3211psnrbvi34z0s5rzxf927dzjmndmk4g3ncqdag1hr8ar")))

(define-public crate-mb-dev-0.1 (crate (name "mb-dev") (vers "0.1.10") (deps (list (crate-dep (name "clap") (req "^3.1.18") (features (quote ("derive" "cargo"))) (default-features #t) (kind 0)) (crate-dep (name "dns-lookup") (req "^1.0.8") (default-features #t) (kind 0)))) (hash "1g5krxzsjl8aih9mf0qlgdy80s523i7bxn02d36241ymh3xk22g6")))

(define-public crate-mb-dev-0.1 (crate (name "mb-dev") (vers "0.1.11") (deps (list (crate-dep (name "clap") (req "^3.2.12") (features (quote ("derive" "cargo"))) (default-features #t) (kind 0)) (crate-dep (name "dns-lookup") (req "^1.0.8") (default-features #t) (kind 0)))) (hash "070rqnqk9hwdvvm2apks3gna5j841y96b41q3jr5daizs2vnibi1")))

(define-public crate-mb-dev-0.1 (crate (name "mb-dev") (vers "0.1.12") (deps (list (crate-dep (name "clap") (req "^3.2.14") (features (quote ("derive" "cargo"))) (default-features #t) (kind 0)) (crate-dep (name "dns-lookup") (req "^1.0.8") (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.11.11") (features (quote ("json" "blocking"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.140") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "0ql8x05fmx70vk7z1j39amar2s8jwrgcwjyjzji6d3fmsskw901h")))

