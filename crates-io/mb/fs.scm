(define-module (crates-io mb fs) #:use-module (crates-io))

(define-public crate-mbfs-0.1 (crate (name "mbfs") (vers "0.1.0") (hash "07b4ihy4ac0wbl7ya6l90nh7axc7wyivy9bbayxczaxsnmic4dsm")))

(define-public crate-mbfs-0.1 (crate (name "mbfs") (vers "0.1.1") (hash "00bfrvjww7r8hm4inn8pi1riqyj40c445w6nql5z80z9x4hpy5b7")))

(define-public crate-mbfs-0.1 (crate (name "mbfs") (vers "0.1.2") (hash "07q6m2axyd4iaabwfll9d6yaqf737h5wv7nirffz7ggvfaaxn6a0")))

(define-public crate-mbfs-0.1 (crate (name "mbfs") (vers "0.1.3") (hash "0la9ssl0svxfhvvcvimdzwsncn6ymdlf4n9c8nmjbn01bay9zgxs")))

(define-public crate-mbfs-0.1 (crate (name "mbfs") (vers "0.1.4") (hash "04bm0wiy92agzprzgpf45nprirjm98s314s9342k4dn9mlksjap5")))

(define-public crate-mbfs-0.1 (crate (name "mbfs") (vers "0.1.5") (deps (list (crate-dep (name "crossbeam-channel") (req "^0.5.4") (default-features #t) (kind 0)))) (hash "1znr9xqfci7bh757680wilchnakwx254rm92mpw3izfvkg7qvpkj")))

(define-public crate-mbfs-0.1 (crate (name "mbfs") (vers "0.1.6") (deps (list (crate-dep (name "crossbeam-channel") (req "^0.5.4") (default-features #t) (kind 0)))) (hash "12psscfnr2s2935p13jnwjhrdy1qy679pwbyk6c39g5gyrpavg9x")))

(define-public crate-mbfs-0.2 (crate (name "mbfs") (vers "0.2.0") (deps (list (crate-dep (name "chashmap") (req "^2.2.2") (default-features #t) (kind 0)) (crate-dep (name "crossbeam-channel") (req "^0.5.4") (default-features #t) (kind 0)))) (hash "0q4xrlykc62k8rp7rkp4wwrs5l64mdbsvbhgsr304fdj0qjl2van")))

