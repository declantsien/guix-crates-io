(define-module (crates-io lc #{3-}#) #:use-module (crates-io))

(define-public crate-lc3-codec-0.1 (crate (name "lc3-codec") (vers "0.1.0") (deps (list (crate-dep (name "bitvec") (req "^1.0.0") (kind 0)) (crate-dep (name "byteorder") (req "^1.3.4") (kind 0)) (crate-dep (name "fast-math") (req "^0.1.1") (default-features #t) (kind 0)) (crate-dep (name "heapless") (req "^0.7.10") (kind 0)) (crate-dep (name "itertools") (req "^0.10.3") (kind 0)) (crate-dep (name "log") (req "^0.4") (kind 0)) (crate-dep (name "log") (req "^0.4") (kind 2)) (crate-dep (name "num-traits") (req "^0.2.14") (features (quote ("libm"))) (kind 0)) (crate-dep (name "simple_logger") (req "^1.11.0") (kind 2)))) (hash "0y6il9l2l3sjj6cp895jjwwplg3d2sin8ll9jgb2iyyds6q1a9ys")))

(define-public crate-lc3-codec-0.2 (crate (name "lc3-codec") (vers "0.2.0") (deps (list (crate-dep (name "bitvec") (req "^1.0.0") (kind 0)) (crate-dep (name "byteorder") (req "^1.3.4") (kind 0)) (crate-dep (name "fast-math") (req "^0.1.1") (default-features #t) (kind 0)) (crate-dep (name "heapless") (req "^0.7.10") (kind 0)) (crate-dep (name "itertools") (req "^0.10.3") (kind 0)) (crate-dep (name "log") (req "^0.4") (kind 0)) (crate-dep (name "log") (req "^0.4") (kind 2)) (crate-dep (name "num-traits") (req "^0.2.14") (features (quote ("libm"))) (kind 0)) (crate-dep (name "simple_logger") (req "^1.11.0") (kind 2)))) (hash "1x7j8yxz670cprsvynczljwrgr6qk9l8gng0455qccyznq4v40ds") (features (quote (("default" "alloc") ("alloc"))))))

(define-public crate-lc3-ensemble-0.1 (crate (name "lc3-ensemble") (vers "0.1.0") (deps (list (crate-dep (name "crossbeam-channel") (req "^0.5.12") (default-features #t) (kind 0)) (crate-dep (name "logos") (req "^0.14.0") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)))) (hash "0i36li2i9flhvxi7b0klqx0bc5x125cy7068nz6b3mm9p58lszip")))

(define-public crate-lc3-ensemble-0.2 (crate (name "lc3-ensemble") (vers "0.2.0") (deps (list (crate-dep (name "crossbeam-channel") (req "^0.5.12") (default-features #t) (kind 0)) (crate-dep (name "logos") (req "^0.14.0") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)))) (hash "1vpf4lih71apzva84x39w5jp0js8p232f10k63kqxwdy78lggcgr")))

(define-public crate-lc3-ensemble-0.2 (crate (name "lc3-ensemble") (vers "0.2.1") (deps (list (crate-dep (name "crossbeam-channel") (req "^0.5.12") (default-features #t) (kind 0)) (crate-dep (name "logos") (req "^0.14.0") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)))) (hash "02kbiykp9iljll6f79h3hsqdi7vpms72ci3fp1xsagcz62wmh2j3") (rust-version "1.70")))

(define-public crate-lc3-ensemble-0.3 (crate (name "lc3-ensemble") (vers "0.3.0") (deps (list (crate-dep (name "crossbeam-channel") (req "^0.5.12") (default-features #t) (kind 0)) (crate-dep (name "logos") (req "^0.14.0") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)))) (hash "08zgs6ziia0jqh2vc7za2qc1ymgilwrh3yd2wr1m3lzxlvn39242") (rust-version "1.70")))

(define-public crate-lc3-ensemble-0.4 (crate (name "lc3-ensemble") (vers "0.4.0") (deps (list (crate-dep (name "crossbeam-channel") (req "^0.5.12") (default-features #t) (kind 0)) (crate-dep (name "logos") (req "^0.14.0") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)) (crate-dep (name "slotmap") (req "^1.0.7") (default-features #t) (kind 0)))) (hash "16bvpnrs92ax4shyqvvghd9644394dp9bhjv14yjqakjnrfx6w29") (rust-version "1.70")))

(define-public crate-lc3-ensemble-0.4 (crate (name "lc3-ensemble") (vers "0.4.1") (deps (list (crate-dep (name "crossbeam-channel") (req "^0.5.12") (default-features #t) (kind 0)) (crate-dep (name "logos") (req "^0.14.0") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)) (crate-dep (name "slotmap") (req "^1.0.7") (default-features #t) (kind 0)))) (hash "1is1f4f5jxc8hk8g8zncb6mblip943p0char1f6y268f77flbs8w") (rust-version "1.70")))

(define-public crate-lc3-ensemble-0.4 (crate (name "lc3-ensemble") (vers "0.4.2") (deps (list (crate-dep (name "crossbeam-channel") (req "^0.5.12") (default-features #t) (kind 0)) (crate-dep (name "logos") (req "^0.14.0") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)))) (hash "1yf89q4chyhr57msml7clj85w9cwim4r7vgqnsb74x1rkfy05j5b") (rust-version "1.70")))

(define-public crate-lc3-isa-0.1 (crate (name "lc3-isa") (vers "0.1.0-alpha0") (deps (list (crate-dep (name "arbitrary") (req "^0.4.1") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "itertools") (req "^0.9.0") (default-features #t) (kind 2)) (crate-dep (name "lc3-macros") (req "^0.1.0-alpha0") (default-features #t) (kind 0)) (crate-dep (name "pretty_assertions") (req "^0.6.1") (default-features #t) (kind 2)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (kind 0)) (crate-dep (name "static_assertions") (req "^1.1.0") (default-features #t) (kind 0)))) (hash "1f50jkgqa6pmf16jkbp8pvligffmnb3i92207wfs4qhj9v5dgrfq") (features (quote (("strict") ("no_std") ("nightly-const") ("default" "no_std"))))))

(define-public crate-lc3-macros-0.1 (crate (name "lc3-macros") (vers "0.1.0-alpha0") (deps (list (crate-dep (name "pretty_assertions") (req "^0.6.1") (default-features #t) (kind 2)) (crate-dep (name "proc-macro2") (req "^1.0.3") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.2") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.5") (default-features #t) (kind 0)))) (hash "0lvs4hxb1az4j41bwjcz55wby1nprz7r55gn1jc6188zrlda55rp")))

(define-public crate-lc3-rs-0.1 (crate (name "lc3-rs") (vers "0.1.0") (deps (list (crate-dep (name "rust-embed") (req "^4.3") (default-features #t) (kind 0)))) (hash "17p636qv3z9z5c68hh1fi0pwv52hy6dcg0bky3jkqv4swmf5xq1l") (features (quote (("register-trace") ("instruction-trace") ("disable-crlf-compat-windows"))))))

(define-public crate-lc3-rs-0.1 (crate (name "lc3-rs") (vers "0.1.1") (hash "0wslirvxvincdgr59fjvv4crmfh2g3rijfyclv7frcpjfy04p90r") (features (quote (("register-trace") ("instruction-trace") ("disable-crlf-compat-windows"))))))

(define-public crate-lc3-rs-0.1 (crate (name "lc3-rs") (vers "0.1.2") (hash "17jg1zi4y450xj5jb7d7a3x09p204673md1sfxzj8bm0q2kgabky") (features (quote (("register-trace") ("instruction-trace") ("disable-crlf-compat-windows"))))))

(define-public crate-lc3-rs-0.1 (crate (name "lc3-rs") (vers "0.1.3") (hash "0rscz7c7z75p73m9rjcdpg1pxjjcvrhwj7c3xx7zr76h8fm7lnb3") (features (quote (("register-trace") ("instruction-trace") ("disable-crlf-compat-windows"))))))

(define-public crate-lc3-rs-0.2 (crate (name "lc3-rs") (vers "0.2.0") (hash "1n0w0jn5pl5i4fk1v1rhzy7vvm4sy41sscghraqi0k4ingsj9pxr") (features (quote (("register-trace") ("instruction-trace") ("disable-crlf-compat-windows"))))))

(define-public crate-lc3-rs-0.3 (crate (name "lc3-rs") (vers "0.3.0") (hash "1gzcmgnxh0q4vdwyk0cd214gsssgr5259f8zja39gniii6q21n1y") (features (quote (("register-trace") ("instruction-trace") ("disable-crlf-compat-windows"))))))

(define-public crate-lc3-rs-0.3 (crate (name "lc3-rs") (vers "0.3.1") (hash "00bnlkx00zns6907jzk7gyfzvklr1gc6drjbfc666qk7w14md9c7") (features (quote (("register-trace") ("instruction-trace") ("disable-crlf-compat-windows"))))))

(define-public crate-lc3-rs-0.4 (crate (name "lc3-rs") (vers "0.4.0") (hash "13c0rq3n51bpf9shxyv2mimqyrnbf7drdrh2bqsc1mqinqss00sc") (features (quote (("register-trace") ("instruction-trace") ("disable-crlf-compat-windows"))))))

(define-public crate-lc3-rs-0.4 (crate (name "lc3-rs") (vers "0.4.1") (hash "1aclrp4f89dawni3hfnawnisad1x7hwrjzk65ng727j1gldxf1b0") (features (quote (("register-trace") ("instruction-trace") ("disable-crlf-compat-windows"))))))

(define-public crate-lc3-rs-0.4 (crate (name "lc3-rs") (vers "0.4.2") (hash "1dy7ncbzhip20j175ksvsya5x9zpm1804l739a192j88w87pyc0r") (features (quote (("register-trace") ("instruction-trace") ("disable-crlf-compat-windows"))))))

(define-public crate-lc3-rs-0.4 (crate (name "lc3-rs") (vers "0.4.3") (hash "0dn7gb6faab7v8lyw3y97wiy29x6bzlyahzlcn5i55035fb86alg") (features (quote (("register-trace") ("instruction-trace") ("disable-crlf-compat-windows"))))))

(define-public crate-lc3-rs-0.5 (crate (name "lc3-rs") (vers "0.5.0") (hash "03w8jnk3rfs4q3kz3a1d6k60pjjh5p00jb14k1v5iicsxqhybgfd") (features (quote (("register-trace") ("instruction-trace") ("disable-crlf-compat-windows"))))))

(define-public crate-lc3-rs-0.6 (crate (name "lc3-rs") (vers "0.6.0") (hash "0cddqhj99yvz17ln166wy6v1zxwhcm1w2bbjj96mcqjm41bmfmcc") (features (quote (("register-trace") ("instruction-trace") ("disable-crlf-compat-windows"))))))

(define-public crate-lc3-sys-0.1 (crate (name "lc3-sys") (vers "0.1.0") (deps (list (crate-dep (name "bindgen") (req "^0.69") (default-features #t) (kind 1)) (crate-dep (name "cc") (req "^1.0") (default-features #t) (kind 1)))) (hash "0wx67bd2g1s7i6afdwgd8fl58x39bzbxqi02y49pp14gnzahld78") (links "lc3")))

