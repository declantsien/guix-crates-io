(define-module (crates-io lc ds) #:use-module (crates-io))

(define-public crate-lcdsmartie-rs-0.1 (crate (name "lcdsmartie-rs") (vers "0.1.0") (hash "106s3wp86jvpvcg93q6q58k240j6sznp9yf4kgr5m8sw7dwdxw7m") (features (quote (("std") ("default")))) (rust-version "1.64")))

