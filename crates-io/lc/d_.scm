(define-module (crates-io lc d_) #:use-module (crates-io))

(define-public crate-lcd_1602_i2c-0.2 (crate (name "lcd_1602_i2c") (vers "0.2.0") (deps (list (crate-dep (name "embedded-hal") (req "^0.2.4") (default-features #t) (kind 0)))) (hash "16fh350a17pb5pns1amvh4gks2vh9g697i04sbp3fyzn3dnfp2kx")))

(define-public crate-lcd_1602_i2c-0.2 (crate (name "lcd_1602_i2c") (vers "0.2.1") (deps (list (crate-dep (name "embedded-hal") (req "^0.2.4") (default-features #t) (kind 0)))) (hash "0nx7lagl5fmwab5kkxihnbd0ayyz1wxys7vsiyy5rs9r2q1swqf0")))

(define-public crate-lcd_1602_i2c-0.3 (crate (name "lcd_1602_i2c") (vers "0.3.0") (deps (list (crate-dep (name "embedded-hal") (req "^0.2.4") (default-features #t) (kind 0)) (crate-dep (name "embedded-hal-mock") (req "^0.7.2") (default-features #t) (kind 2)))) (hash "123251cnlb1jijmj7i8libsad70iig4hvxz4yjw7279ic173spyr")))

