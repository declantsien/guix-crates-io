(define-module (crates-io lc ov) #:use-module (crates-io))

(define-public crate-lcov-0.1 (crate (name "lcov") (vers "0.1.0") (deps (list (crate-dep (name "cargo-readme") (req "^2.0") (default-features #t) (kind 2)) (crate-dep (name "failure") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "glob") (req "^0.2") (default-features #t) (kind 2)) (crate-dep (name "matches") (req "^0.1") (default-features #t) (kind 2)) (crate-dep (name "version-sync") (req "^0.5") (default-features #t) (kind 2)))) (hash "1gkpbq857prja3v5dd3maa6g7pyxx2fgn7wx2iqzvqdhs2yg6dy3")))

(define-public crate-lcov-0.1 (crate (name "lcov") (vers "0.1.1") (deps (list (crate-dep (name "cargo-readme") (req "^2.0") (default-features #t) (kind 2)) (crate-dep (name "failure") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "glob") (req "^0.2") (default-features #t) (kind 2)) (crate-dep (name "matches") (req "^0.1") (default-features #t) (kind 2)) (crate-dep (name "version-sync") (req "^0.5") (default-features #t) (kind 2)))) (hash "14756v8sw9vr1g75nm2v5rq1s4qw1wz8xpg17q456ydkm32qxjay")))

(define-public crate-lcov-0.1 (crate (name "lcov") (vers "0.1.2") (deps (list (crate-dep (name "cargo-readme") (req "^2.0") (default-features #t) (kind 2)) (crate-dep (name "failure") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "glob") (req "^0.2") (default-features #t) (kind 2)) (crate-dep (name "matches") (req "^0.1") (default-features #t) (kind 2)) (crate-dep (name "version-sync") (req "^0.5") (default-features #t) (kind 2)))) (hash "1v0lfnfnvlxbyhcvbd4fq956mlcdz4p36gmgfdd26qrqk3blrgpp")))

(define-public crate-lcov-0.2 (crate (name "lcov") (vers "0.2.0") (deps (list (crate-dep (name "cargo-readme") (req "^2.0") (default-features #t) (kind 2)) (crate-dep (name "failure") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "glob") (req "^0.2") (default-features #t) (kind 2)) (crate-dep (name "matches") (req "^0.1") (default-features #t) (kind 2)) (crate-dep (name "version-sync") (req "^0.5") (default-features #t) (kind 2)))) (hash "1kfmfk0g961076kp65zlpx7qi180wsx3czzar05dwjl1y8yhd6k7")))

(define-public crate-lcov-0.3 (crate (name "lcov") (vers "0.3.0") (deps (list (crate-dep (name "cargo-readme") (req "^2.0") (default-features #t) (kind 2)) (crate-dep (name "failure") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "glob") (req "^0.2") (default-features #t) (kind 2)) (crate-dep (name "matches") (req "^0.1") (default-features #t) (kind 2)) (crate-dep (name "version-sync") (req "^0.5") (default-features #t) (kind 2)))) (hash "19n3gf7a1kq2bvbz6ghp8rr852j23h4ijzjdawbayac9wrbxwj2l")))

(define-public crate-lcov-0.4 (crate (name "lcov") (vers "0.4.0") (deps (list (crate-dep (name "cargo-readme") (req "^2.0") (default-features #t) (kind 2)) (crate-dep (name "failure") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "glob") (req "^0.2") (default-features #t) (kind 2)) (crate-dep (name "matches") (req "^0.1") (default-features #t) (kind 2)) (crate-dep (name "version-sync") (req "^0.5") (default-features #t) (kind 2)))) (hash "0kpzqv9vq3xm9jiz84n9ygpbddqgaf7cpv6zpiv135c0z5nhmx4d")))

(define-public crate-lcov-0.4 (crate (name "lcov") (vers "0.4.1") (deps (list (crate-dep (name "cargo-readme") (req "^2.0") (default-features #t) (kind 2)) (crate-dep (name "failure") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "glob") (req "^0.2") (default-features #t) (kind 2)) (crate-dep (name "matches") (req "^0.1") (default-features #t) (kind 2)) (crate-dep (name "version-sync") (req "^0.5") (default-features #t) (kind 2)))) (hash "112655mzv22s3xfbps7vycm5w3y84mbflnn3616888pqwg7qv4q7")))

(define-public crate-lcov-0.4 (crate (name "lcov") (vers "0.4.2") (deps (list (crate-dep (name "cargo-readme") (req "^2.0") (default-features #t) (kind 2)) (crate-dep (name "failure") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "glob") (req "^0.2") (default-features #t) (kind 2)) (crate-dep (name "matches") (req "^0.1") (default-features #t) (kind 2)) (crate-dep (name "version-sync") (req "^0.5") (default-features #t) (kind 2)))) (hash "1bwdrgpsxvk0i54s7rfl7zgr48ik1zzfqkzcb52x3vw7hxzr17jg")))

(define-public crate-lcov-0.5 (crate (name "lcov") (vers "0.5.0") (deps (list (crate-dep (name "cargo-readme") (req "^2.0") (default-features #t) (kind 2)) (crate-dep (name "failure") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "glob") (req "^0.2") (default-features #t) (kind 2)) (crate-dep (name "matches") (req "^0.1") (default-features #t) (kind 2)) (crate-dep (name "version-sync") (req "^0.5") (default-features #t) (kind 2)))) (hash "1nhk83ab8cc1vv9cllddnv2x85xk1hvcs90jl2y5v4723x3ic9s6")))

(define-public crate-lcov-0.6 (crate (name "lcov") (vers "0.6.0") (deps (list (crate-dep (name "cargo-readme") (req "^3.2.0") (default-features #t) (kind 2)) (crate-dep (name "glob") (req "^0.3.0") (default-features #t) (kind 2)) (crate-dep (name "matches") (req "^0.1.8") (default-features #t) (kind 2)) (crate-dep (name "thiserror") (req "^1.0.20") (default-features #t) (kind 0)) (crate-dep (name "version-sync") (req "^0.9.1") (default-features #t) (kind 2)))) (hash "0m0fpgd0qz585jly1qchds9asikknsf5rxnqq7j11bjqngx93cjk")))

(define-public crate-lcov-0.7 (crate (name "lcov") (vers "0.7.0") (deps (list (crate-dep (name "cargo-readme") (req "^3.2.0") (default-features #t) (kind 2)) (crate-dep (name "glob") (req "^0.3.0") (default-features #t) (kind 2)) (crate-dep (name "matches") (req "^0.1.9") (default-features #t) (kind 2)) (crate-dep (name "thiserror") (req "^1.0.29") (default-features #t) (kind 0)) (crate-dep (name "version-sync") (req "^0.9.3") (default-features #t) (kind 2)))) (hash "00yd7wfs4i7k2wf59v0qn581gaap2l9s4719jzcrf49jrxl67z73")))

(define-public crate-lcov-0.7 (crate (name "lcov") (vers "0.7.2") (deps (list (crate-dep (name "cargo-readme") (req "^3.2.0") (default-features #t) (kind 2)) (crate-dep (name "glob") (req "^0.3.0") (default-features #t) (kind 2)) (crate-dep (name "matches") (req "^0.1.9") (default-features #t) (kind 2)) (crate-dep (name "thiserror") (req "^1.0.29") (default-features #t) (kind 0)) (crate-dep (name "version-sync") (req "^0.9.3") (default-features #t) (kind 2)))) (hash "0rksiqvhb5dpcdhlha7c0xghqw5v060q2jn3nidwi09jr5w1wwcq")))

(define-public crate-lcov-0.8 (crate (name "lcov") (vers "0.8.0") (deps (list (crate-dep (name "cargo-readme") (req "^3.2.0") (default-features #t) (kind 2)) (crate-dep (name "glob") (req "^0.3.0") (default-features #t) (kind 2)) (crate-dep (name "matches") (req "^0.1.9") (default-features #t) (kind 2)) (crate-dep (name "thiserror") (req "^1.0.31") (default-features #t) (kind 0)) (crate-dep (name "version-sync") (req "^0.9.4") (default-features #t) (kind 2)))) (hash "0nx4w5f21zna90kcrgkb5y2bdcfya4w73s2pqp0rskp6y6rlr7dh") (rust-version "1.56.1")))

(define-public crate-lcov-0.8 (crate (name "lcov") (vers "0.8.1") (deps (list (crate-dep (name "cargo-readme") (req "^3.2.0") (default-features #t) (kind 2)) (crate-dep (name "glob") (req "^0.3.0") (default-features #t) (kind 2)) (crate-dep (name "matches") (req "^0.1.9") (default-features #t) (kind 2)) (crate-dep (name "thiserror") (req "^1.0.31") (default-features #t) (kind 0)) (crate-dep (name "version-sync") (req "^0.9.4") (default-features #t) (kind 2)))) (hash "08xph2prpzlh19fw6jl89pvylr239qc3hzxkcpdq9a45wpasdkqw") (rust-version "1.56.1")))

(define-public crate-lcov-diff-0.1 (crate (name "lcov-diff") (vers "0.1.0") (deps (list (crate-dep (name "lcov") (req "^0.6") (default-features #t) (kind 0)))) (hash "0glnvhx7137jxpp7hk783b6w52ggxly5w1aq4nwgxy12hjr7w93x")))

(define-public crate-lcov-diff-0.1 (crate (name "lcov-diff") (vers "0.1.1") (deps (list (crate-dep (name "lcov") (req "^0.6") (default-features #t) (kind 0)))) (hash "01hvqk1a54nfkrm7fay9bq4dfbxcpy66j2prqzl186z01k9m9frn")))

(define-public crate-lcov-diff-0.1 (crate (name "lcov-diff") (vers "0.1.2") (deps (list (crate-dep (name "lcov") (req "^0.8") (default-features #t) (kind 0)))) (hash "05w0f1pfhbmxvi8vzmfxv3w978inm5apsd7d5c8qzsmn10d4rxlq")))

(define-public crate-lcov-diff-util-0.2 (crate (name "lcov-diff-util") (vers "0.2.0") (deps (list (crate-dep (name "env_logger") (req "^0.8.3") (default-features #t) (kind 0)) (crate-dep (name "lcov") (req "^0.6") (default-features #t) (kind 0)) (crate-dep (name "lcov-diff") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.6") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "tempfile") (req "^3.2") (default-features #t) (kind 0)))) (hash "070rkyc4jwv7plzbzd4j4fqcmh0d5nkk2sp4wihrk5whqzwlvp1g")))

(define-public crate-lcov-diff-util-0.2 (crate (name "lcov-diff-util") (vers "0.2.1") (deps (list (crate-dep (name "env_logger") (req "^0.10") (default-features #t) (kind 0)) (crate-dep (name "lcov") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "lcov-diff") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.17") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "tempfile") (req "^3.3") (default-features #t) (kind 0)))) (hash "1wf3ivy3mrb26q3g82f9r9acqvk9w56iybbcgnbl86wf414qky0g")))

(define-public crate-lcov-parser-0.1 (crate (name "lcov-parser") (vers "0.1.0") (deps (list (crate-dep (name "lines") (req "^0.0.3") (default-features #t) (kind 0)) (crate-dep (name "nom") (req "~0.3.0") (default-features #t) (kind 0)))) (hash "07ww75llkcv29yd8kymld69rw70f3hxdqah4fr9x7gw7k65m1f2l")))

(define-public crate-lcov-parser-0.2 (crate (name "lcov-parser") (vers "0.2.0") (deps (list (crate-dep (name "lines") (req "^0.0.3") (default-features #t) (kind 0)) (crate-dep (name "nom") (req "~0.3.0") (default-features #t) (kind 0)))) (hash "167mvsh05pgg7gysnx2x8kgsxgdcwl601rn4khajim3gj257dv3q")))

(define-public crate-lcov-parser-0.3 (crate (name "lcov-parser") (vers "0.3.0") (deps (list (crate-dep (name "lines") (req "^0.0.3") (default-features #t) (kind 0)) (crate-dep (name "nom") (req "~0.3.0") (default-features #t) (kind 0)))) (hash "17bcvyk4d21c65y8gczzy2fzz0af2mhrs4nm3wqn3k5w2szrhk3j")))

(define-public crate-lcov-parser-0.4 (crate (name "lcov-parser") (vers "0.4.0") (deps (list (crate-dep (name "lines") (req "^0.0.3") (default-features #t) (kind 0)) (crate-dep (name "parser-combinators") (req "^0.5.1") (default-features #t) (kind 0)))) (hash "06ac20my6z5mzi2sldwzr4nrnjv1c9p7wc741286i4prqvfqlfyl")))

(define-public crate-lcov-parser-0.5 (crate (name "lcov-parser") (vers "0.5.0") (deps (list (crate-dep (name "lines") (req "^0.0.3") (default-features #t) (kind 0)) (crate-dep (name "parser-combinators") (req "^0.7.0") (default-features #t) (kind 0)))) (hash "0hbayg01789b58p0aydkg4lcmdfikkpr83rr93faba98zg0886xy")))

(define-public crate-lcov-parser-0.6 (crate (name "lcov-parser") (vers "0.6.0") (deps (list (crate-dep (name "combine") (req "^1.0.0") (default-features #t) (kind 0)) (crate-dep (name "lines") (req "^0.0.3") (default-features #t) (kind 0)))) (hash "1wblri26h77650gj80ahcgrj2idnvjmzxbn905bxk2d8pzccj8ba")))

(define-public crate-lcov-parser-0.6 (crate (name "lcov-parser") (vers "0.6.1") (deps (list (crate-dep (name "combine") (req "^1.0.0") (default-features #t) (kind 0)) (crate-dep (name "lines") (req "^0.0.3") (default-features #t) (kind 0)))) (hash "198g63hdd4lzlhwvyr4r85wm23222vq343j3xniz0938fmfzaqnv")))

(define-public crate-lcov-parser-0.6 (crate (name "lcov-parser") (vers "0.6.2") (deps (list (crate-dep (name "combine") (req "^1.2.0") (default-features #t) (kind 0)) (crate-dep (name "lines") (req "^0.0.3") (default-features #t) (kind 0)))) (hash "1i93s897qib9qiklrjbfy4p4ybq36065w6r4s3mh117zji9g83r1")))

(define-public crate-lcov-parser-0.7 (crate (name "lcov-parser") (vers "0.7.0") (deps (list (crate-dep (name "combine") (req "^1.2.0") (default-features #t) (kind 0)) (crate-dep (name "lines") (req "^0.0.3") (default-features #t) (kind 0)))) (hash "1a58dmf3gmm7j1m9yxlz1qx23y6z0gak4wv8cmp31nwmpg6hph8j")))

(define-public crate-lcov-parser-0.8 (crate (name "lcov-parser") (vers "0.8.0") (deps (list (crate-dep (name "combine") (req "^1.2.1") (default-features #t) (kind 0)) (crate-dep (name "lines") (req "^0.0.4") (default-features #t) (kind 0)))) (hash "0ya3dqqv8bcrpind2r44ggrrdqnsx3w1bxsq2qbys2fzc6227r1w")))

(define-public crate-lcov-parser-1 (crate (name "lcov-parser") (vers "1.0.0") (deps (list (crate-dep (name "combine") (req "^1.3.0") (default-features #t) (kind 0)))) (hash "1s2vg149d9i68mfkcqid4p5nmcrrbvm4wqk5xyild2mv5xra571j")))

(define-public crate-lcov-parser-1 (crate (name "lcov-parser") (vers "1.0.1") (deps (list (crate-dep (name "combine") (req "^1.3.0") (default-features #t) (kind 0)))) (hash "1rrdrf7rrlbbjh0r514g9dnn836cd1cdcnd2mkbd3b1ajjq7dwrq")))

(define-public crate-lcov-parser-1 (crate (name "lcov-parser") (vers "1.1.0") (deps (list (crate-dep (name "combine") (req "^1.3.0") (default-features #t) (kind 0)))) (hash "0jsby71qqa7v0r8z0q5hhsa4vbmqw1c1ckg1hdv4rl1a1j62ylgm")))

(define-public crate-lcov-parser-1 (crate (name "lcov-parser") (vers "1.1.1") (deps (list (crate-dep (name "combine") (req "^1.3.0") (default-features #t) (kind 0)))) (hash "1yj2zs3ik11xlcbrr9pk2d13n0w3wismrd02d97ixvbavr4wccw7")))

(define-public crate-lcov-parser-1 (crate (name "lcov-parser") (vers "1.1.2") (deps (list (crate-dep (name "combine") (req "^1.3.0") (default-features #t) (kind 0)))) (hash "1f86ivvkvk3zl3a923fv93496z8j19lp712sipyr3lb988ilpj21")))

(define-public crate-lcov-parser-2 (crate (name "lcov-parser") (vers "2.0.0") (deps (list (crate-dep (name "combine") (req "^1.3.0") (default-features #t) (kind 0)))) (hash "0wlymf605davwx6whcphrpmcy2jk9fdfy01bvsn2rys59m6xx972")))

(define-public crate-lcov-parser-2 (crate (name "lcov-parser") (vers "2.1.0") (deps (list (crate-dep (name "combine") (req "^2.0.0") (default-features #t) (kind 0)))) (hash "11hfgbl0600b9qriqrrgrlhf9gwf5h524wdxwc8350x6j2bi1f6v")))

(define-public crate-lcov-parser-2 (crate (name "lcov-parser") (vers "2.1.1") (deps (list (crate-dep (name "combine") (req "^2.0.0") (default-features #t) (kind 0)))) (hash "18gwpw91w7jjggnfmx9r4mhpgxqdkyadjl89cllkf66j589ab21b")))

(define-public crate-lcov-parser-3 (crate (name "lcov-parser") (vers "3.0.0-beta") (deps (list (crate-dep (name "combine") (req "^2.0.0") (default-features #t) (kind 0)))) (hash "08dykwypn9pml485v7d1998jv349zf4198z2n8x3q8yrmfpvywww")))

(define-public crate-lcov-parser-3 (crate (name "lcov-parser") (vers "3.0.1-beta") (deps (list (crate-dep (name "combine") (req "2.3.*") (default-features #t) (kind 0)) (crate-dep (name "tempdir") (req "^0.3") (default-features #t) (kind 2)))) (hash "11cypm14vgr5dkxpw7mpqfd0wkd8csjn9a59d8rbjial9i3bx2q9")))

(define-public crate-lcov-parser-3 (crate (name "lcov-parser") (vers "3.1.0") (deps (list (crate-dep (name "combine") (req "2.4.*") (default-features #t) (kind 0)) (crate-dep (name "tempdir") (req "^0.3") (default-features #t) (kind 2)))) (hash "1r52qv1hgpwffvkj45gickhhs8dzrja1qv34msp383gj7v5dba52")))

(define-public crate-lcov-parser-3 (crate (name "lcov-parser") (vers "3.2.0") (deps (list (crate-dep (name "combine") (req "2.4.*") (default-features #t) (kind 0)) (crate-dep (name "tempdir") (req "^0.3") (default-features #t) (kind 2)))) (hash "1dyjq368pgpi2ry7wx2nn87f5pqd4886cbj690ziksk47si1pkva")))

(define-public crate-lcov-parser-3 (crate (name "lcov-parser") (vers "3.2.1") (deps (list (crate-dep (name "combine") (req "2.4.*") (default-features #t) (kind 0)) (crate-dep (name "tempdir") (req "^0.3") (default-features #t) (kind 2)))) (hash "14gp1gwjpnc63qvmhvi2064w8hnb1yjb29bnzcbpk4dskxv8c61c")))

(define-public crate-lcov-parser-3 (crate (name "lcov-parser") (vers "3.2.2") (deps (list (crate-dep (name "combine") (req "2.5.*") (default-features #t) (kind 0)) (crate-dep (name "tempdir") (req "^0.3") (default-features #t) (kind 2)))) (hash "0qn9wl9x0lj4mj7kfvai33kxvabiiyckqmd6ican41l9ybq88mxq")))

(define-public crate-lcov-summary-0.1 (crate (name "lcov-summary") (vers "0.1.0") (deps (list (crate-dep (name "anyhow") (req "^1.0.70") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^4.2.4") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "colored") (req "^2.0.0") (default-features #t) (kind 0)) (crate-dep (name "nom") (req "^7.1.3") (default-features #t) (kind 0)) (crate-dep (name "prettytable-rs") (req "^0.10.0") (default-features #t) (kind 0)) (crate-dep (name "rustc-demangle") (req "^0.1.23") (default-features #t) (kind 0)))) (hash "17lm77b7gjqb8x410d78acl5an4smgvblx5lhnj24n0nh4viv1pf")))

(define-public crate-lcov-summary-0.2 (crate (name "lcov-summary") (vers "0.2.0") (deps (list (crate-dep (name "anyhow") (req "^1.0.70") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^4.2.4") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "colored") (req "^2.0.0") (default-features #t) (kind 0)) (crate-dep (name "nom") (req "^7.1.3") (default-features #t) (kind 0)) (crate-dep (name "prettytable-rs") (req "^0.10.0") (default-features #t) (kind 0)) (crate-dep (name "rustc-demangle") (req "^0.1.23") (default-features #t) (kind 0)))) (hash "0jd1p3x9d5k4x98s0fh3c2vnp9p92hrzphn36h6z09fx6wdc6aid")))

(define-public crate-lcov-summary-0.2 (crate (name "lcov-summary") (vers "0.2.1") (deps (list (crate-dep (name "anyhow") (req "^1.0.70") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^4.2.4") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "colored") (req "^2.0.0") (default-features #t) (kind 0)) (crate-dep (name "nom") (req "^7.1.3") (default-features #t) (kind 0)) (crate-dep (name "prettytable-rs") (req "^0.10.0") (default-features #t) (kind 0)) (crate-dep (name "rustc-demangle") (req "^0.1.23") (default-features #t) (kind 0)))) (hash "1vyam71z51agjaqlq407kjal9k7f8749qhvw8cvf8kmlgy1v7zsj")))

(define-public crate-lcov-tool-0.1 (crate (name "lcov-tool") (vers "0.1.1") (deps (list (crate-dep (name "clap") (req "^2.5.1") (default-features #t) (kind 0)) (crate-dep (name "lcov-parser") (req "^1.1.1") (default-features #t) (kind 0)))) (hash "1hipwlf9v9r9vc260b49mffy4c5as1np8712hx0bkidp6flrybqs")))

(define-public crate-lcov-tool-0.1 (crate (name "lcov-tool") (vers "0.1.2") (deps (list (crate-dep (name "clap") (req "^2.5.1") (default-features #t) (kind 0)) (crate-dep (name "lcov-parser") (req "^1.1.1") (default-features #t) (kind 0)))) (hash "15h8f9lnv8zqfbr81wvb62vvvq8qhfkkacq4n5giy75lwhbjxqmm")))

(define-public crate-lcov-tool-0.1 (crate (name "lcov-tool") (vers "0.1.3") (deps (list (crate-dep (name "clap") (req "^2.9.1") (default-features #t) (kind 0)) (crate-dep (name "lcov-parser") (req "^1.1.1") (default-features #t) (kind 0)))) (hash "0y6j175kblsk0i2brklhbv0907nwj634p9z00m99rmgpz32kdx2q")))

(define-public crate-lcov-tool-0.1 (crate (name "lcov-tool") (vers "0.1.4") (deps (list (crate-dep (name "clap") (req "^2.13.0") (default-features #t) (kind 0)) (crate-dep (name "lcov-parser") (req "^1.1.2") (default-features #t) (kind 0)))) (hash "1q28c432h19g1zn7r2hnlr6l365bkbkj78jq948j29vyzlr4bqcs")))

(define-public crate-lcov-tool-0.1 (crate (name "lcov-tool") (vers "0.1.5") (deps (list (crate-dep (name "clap") (req "^2.13.0") (default-features #t) (kind 0)) (crate-dep (name "lcov-parser") (req "^2.0.0") (default-features #t) (kind 0)))) (hash "1bc0xrp0vdp66skjl9hqhkg4n9z5b0xlxd69l3rn0g6bcg3ncn79")))

(define-public crate-lcov-util-0.1 (crate (name "lcov-util") (vers "0.1.0") (deps (list (crate-dep (name "failure") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "lcov") (req "^0.1") (default-features #t) (kind 0)))) (hash "0zl9lwhiv4yn53vd888lv9kp0lvip6lh5mi4fl7f2vn9c5lm8j84")))

(define-public crate-lcov-util-0.1 (crate (name "lcov-util") (vers "0.1.1") (deps (list (crate-dep (name "failure") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "lcov") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.2") (default-features #t) (kind 0)))) (hash "0s9kbnrdalv46p01zvx0qq078i73g7qd67i4qahxcbqrp3gzw874")))

(define-public crate-lcov-util-0.1 (crate (name "lcov-util") (vers "0.1.2") (deps (list (crate-dep (name "failure") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "lcov") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.2") (default-features #t) (kind 0)))) (hash "1fdqnd7dvfcz9wqiyjz3rl74z5bzjwh5klvx95249vnawaa22v6y")))

(define-public crate-lcov-util-0.1 (crate (name "lcov-util") (vers "0.1.3") (deps (list (crate-dep (name "failure") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "lcov") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.2") (default-features #t) (kind 0)))) (hash "14fa5jhbr8h2xvfz379pll7xd85h97mh9w45pfaspgy0r4g40nnv")))

(define-public crate-lcov-util-0.1 (crate (name "lcov-util") (vers "0.1.4") (deps (list (crate-dep (name "failure") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "lcov") (req "^0.5") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.2") (default-features #t) (kind 0)))) (hash "0xd59ff5ah34g821sajk1w8ahdy98f1v7i8qjd7r0ask85ndvdw0")))

(define-public crate-lcov-util-0.1 (crate (name "lcov-util") (vers "0.1.5") (deps (list (crate-dep (name "lcov") (req "^0.6") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.3.15") (default-features #t) (kind 0)))) (hash "09hvj2prpg551fslvap4f1100wjl4b5p5nan1mqmm9qiqnhlw9ds")))

(define-public crate-lcov-util-0.1 (crate (name "lcov-util") (vers "0.1.6") (deps (list (crate-dep (name "lcov") (req "^0.7") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.3.23") (default-features #t) (kind 0)))) (hash "1hjdwpqlz9k1dy6jf42g9d0fkv9g2ci5kvr6ma2ysm0gbxj0rp3b")))

(define-public crate-lcov-util-0.1 (crate (name "lcov-util") (vers "0.1.7") (deps (list (crate-dep (name "lcov") (req "^0.7") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.3.23") (default-features #t) (kind 0)))) (hash "07jgh3b2a2y1jqld5yal40968y9fr9cdxgpsmvvd4vdxrddipwpq")))

(define-public crate-lcov-util-0.1 (crate (name "lcov-util") (vers "0.1.8") (deps (list (crate-dep (name "lcov") (req "^0.7") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.3.23") (default-features #t) (kind 0)))) (hash "12w496l337p3mxg3dqlp7y3kb52b3iwzdfq13d6zwsf2ycgcpdh3")))

(define-public crate-lcov-util-0.1 (crate (name "lcov-util") (vers "0.1.9") (deps (list (crate-dep (name "lcov") (req "^0.7") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.3.23") (default-features #t) (kind 0)) (crate-dep (name "version-sync") (req "^0.9.4") (default-features #t) (kind 2)))) (hash "0526sb928na1skf9d00hjy1pp93sj463z883hwhbvc616zffs0k0")))

(define-public crate-lcov-util-0.2 (crate (name "lcov-util") (vers "0.2.0") (deps (list (crate-dep (name "lcov") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.3.26") (default-features #t) (kind 0)) (crate-dep (name "version-sync") (req "^0.9.4") (default-features #t) (kind 2)))) (hash "0azzlksy7r0v0jasmcgkx9c2sj81xib8iwa0zjxw7vk7l688q8h4") (rust-version "1.56.1")))

(define-public crate-lcov-util-0.2 (crate (name "lcov-util") (vers "0.2.1") (deps (list (crate-dep (name "lcov") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.3.26") (default-features #t) (kind 0)) (crate-dep (name "version-sync") (req "^0.9.4") (default-features #t) (kind 2)))) (hash "05bzim5awb7wl97q1vxf70z8apvia8i0xhbqiifvfc8wnwg329nz") (rust-version "1.56.1")))

(define-public crate-lcov2cobertura-1 (crate (name "lcov2cobertura") (vers "1.0.0") (deps (list (crate-dep (name "anyhow") (req "^1.0.66") (default-features #t) (kind 0)) (crate-dep (name "quick-xml") (req "^0.26.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.7.0") (default-features #t) (kind 0)) (crate-dep (name "rustc-demangle") (req "^0.1.21") (default-features #t) (kind 0)))) (hash "0pfqv99w3s7kvnyhapmz7vz6p3ap47gdmld7586yr89r5bq8qjsv")))

(define-public crate-lcov2cobertura-1 (crate (name "lcov2cobertura") (vers "1.0.1") (deps (list (crate-dep (name "anyhow") (req "^1.0.66") (default-features #t) (kind 0)) (crate-dep (name "quick-xml") (req "^0.26.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.7.0") (default-features #t) (kind 0)) (crate-dep (name "rustc-demangle") (req "^0.1.21") (default-features #t) (kind 0)))) (hash "0b8nvv1b8269sv4lckiazg87p7j8l8zn2ljhkg3sks6anhwyp26g") (rust-version "1.59")))

(define-public crate-lcov2cobertura-1 (crate (name "lcov2cobertura") (vers "1.0.2") (deps (list (crate-dep (name "anyhow") (req "^1.0.66") (default-features #t) (kind 0)) (crate-dep (name "quick-xml") (req "^0.30.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.7.0") (default-features #t) (kind 0)) (crate-dep (name "rustc-demangle") (req "^0.1.21") (default-features #t) (kind 0)))) (hash "1nx364adpfa6fmaklv20dsnh8mvni87b4wrs8hs2a2bh341yrmh7") (rust-version "1.59")))

(define-public crate-lcov2cobertura-1 (crate (name "lcov2cobertura") (vers "1.0.3") (deps (list (crate-dep (name "anyhow") (req "^1.0.79") (default-features #t) (kind 0)) (crate-dep (name "quick-xml") (req "^0.31.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.7.3") (default-features #t) (kind 0)) (crate-dep (name "rustc-demangle") (req "^0.1.23") (default-features #t) (kind 0)))) (hash "00swx6r497ij61s8yh88d2kxhiijwd2139s0nivw489ld6azknqk") (rust-version "1.59")))

(define-public crate-lcoviz-0.1 (crate (name "lcoviz") (vers "0.1.0") (deps (list (crate-dep (name "htmlescape") (req "^0.3.1") (default-features #t) (kind 0)) (crate-dep (name "lcov") (req "^0.8.1") (default-features #t) (kind 0)) (crate-dep (name "mockall") (req "^0.12.1") (default-features #t) (kind 2)) (crate-dep (name "pathdiff") (req "^0.2.1") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.198") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "toml") (req "^0.8.12") (default-features #t) (kind 0)))) (hash "1qy2xf2qiiqygjgywbr5h6cc2hs8fjgh0i0whnwckdjxjif381zq")))

(define-public crate-lcoviz-0.1 (crate (name "lcoviz") (vers "0.1.1") (deps (list (crate-dep (name "htmlescape") (req "^0.3.1") (default-features #t) (kind 0)) (crate-dep (name "lcov") (req "^0.8.1") (default-features #t) (kind 0)) (crate-dep (name "mockall") (req "^0.12.1") (default-features #t) (kind 2)) (crate-dep (name "pathdiff") (req "^0.2.1") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.198") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "toml") (req "^0.8.12") (default-features #t) (kind 0)))) (hash "0g6hw0774dbfzr1p9jimm8f6p3q7nd39sw9xnn6nkah0r94z64b7")))

