(define-module (crates-io lc id) #:use-module (crates-io))

(define-public crate-lcid-0.1 (crate (name "lcid") (vers "0.1.0") (deps (list (crate-dep (name "assert_matches") (req "^1.5") (default-features #t) (kind 2)) (crate-dep (name "thiserror") (req "^1.0") (default-features #t) (kind 0)))) (hash "1z6h1qykycsbwg1ry53mq9xbzabw0q17rii8l44mg1b6hnmf9il7")))

(define-public crate-lcid-0.2 (crate (name "lcid") (vers "0.2.0") (deps (list (crate-dep (name "assert_matches") (req "^1.5") (default-features #t) (kind 2)) (crate-dep (name "thiserror") (req "^1.0") (default-features #t) (kind 0)))) (hash "0igkzislnjc7pa8x6rp256h6b0xshk700q3fqblpdpiwg07pykx7")))

(define-public crate-lcid-0.2 (crate (name "lcid") (vers "0.2.1") (deps (list (crate-dep (name "assert_matches") (req "^1.5") (default-features #t) (kind 2)))) (hash "1qxgkqy2fjp7ga1aci3d6hkj22smm7359p9yhgqry2k9zdakicnp") (rust-version "1.56")))

(define-public crate-lcid-0.3 (crate (name "lcid") (vers "0.3.0") (hash "1kjj5z5hb55dibrb9rbshvg1vqyfxpz3xa5nvjh60ksf7x4x0r0s") (rust-version "1.56")))

