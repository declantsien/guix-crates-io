(define-module (crates-io lc pm) #:use-module (crates-io))

(define-public crate-LCPminigrep-0.1 (crate (name "LCPminigrep") (vers "0.1.0") (hash "0sxgcck5ws3jczcixg1cpwmgmlnns408mk7a8ifp6fbpy4dl9x60")))

(define-public crate-LCPminigrep-0.2 (crate (name "LCPminigrep") (vers "0.2.0") (hash "01ax2cw3n7yyabisv0ip9rfl7q1wcw3qqysldz8n9w49zfdkgjzi")))

(define-public crate-LCPminigrep-0.1 (crate (name "LCPminigrep") (vers "0.1.2") (hash "00vfcb7cni5r2ajsx5apsj79ygkd6x5k5gcp7wl5881xnwn4dz82")))

(define-public crate-LCPminigrep-0.2 (crate (name "LCPminigrep") (vers "0.2.1") (hash "1par5ipszlkj8vfzymfl1yqcpgb8zvxwkw6ly4qvss9l2nhml42j")))

