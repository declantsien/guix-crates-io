(define-module (crates-io lc m_) #:use-module (crates-io))

(define-public crate-lcm_gen-0.1 (crate (name "lcm_gen") (vers "0.1.0") (deps (list (crate-dep (name "glob") (req "^0.2") (default-features #t) (kind 0)))) (hash "091d068jvdpk3f7y932frg9ziqylw7rwiz0abjcpngn6hjpcvb4f")))

(define-public crate-lcm_gen-0.1 (crate (name "lcm_gen") (vers "0.1.1") (deps (list (crate-dep (name "glob") (req "^0.2") (default-features #t) (kind 0)))) (hash "0m8kw9kvfl7k3clp1mpsiph50b6djfhvh8yrb15ndlvsk4jnmvsq")))

