(define-module (crates-io lc tr) #:use-module (crates-io))

(define-public crate-lctree-0.1 (crate (name "lctree") (vers "0.1.0") (deps (list (crate-dep (name "rand") (req "^0.8") (default-features #t) (kind 2)) (crate-dep (name "rand_derive2") (req "^0.1.21") (default-features #t) (kind 2)))) (hash "1h8g6zdvqd412yai0lk0nknp0b6kb12kdj9nrxd4nr14q7v7czgn") (rust-version "1.66")))

(define-public crate-lctree-0.1 (crate (name "lctree") (vers "0.1.1") (deps (list (crate-dep (name "rand") (req "^0.8") (default-features #t) (kind 2)) (crate-dep (name "rand_derive2") (req "^0.1.21") (default-features #t) (kind 2)))) (hash "0bhid1nnjwkqyawqvcxzhg7g5a3x9s3pybjcbzm98mp0faj2d9nl") (rust-version "1.66")))

(define-public crate-lctree-0.1 (crate (name "lctree") (vers "0.1.2") (deps (list (crate-dep (name "rand") (req "^0.8") (default-features #t) (kind 2)) (crate-dep (name "rand_derive2") (req "^0.1.21") (default-features #t) (kind 2)))) (hash "116s2zhypkkc51mx0z493x1q0rg1zz7ds88p9y8hahvrhn0b87zs") (rust-version "1.63")))

(define-public crate-lctree-0.1 (crate (name "lctree") (vers "0.1.3") (deps (list (crate-dep (name "rand") (req "^0.8") (default-features #t) (kind 2)) (crate-dep (name "rand_derive2") (req "^0.1.21") (default-features #t) (kind 2)))) (hash "024p8zdbn5ka4gbkb3m2g1i6846r8261giqc0k62bwnyzw7wfkcm") (rust-version "1.63")))

(define-public crate-lctree-0.2 (crate (name "lctree") (vers "0.2.0") (deps (list (crate-dep (name "rand") (req "^0.8") (default-features #t) (kind 2)) (crate-dep (name "rand_derive2") (req "^0.1.21") (default-features #t) (kind 2)))) (hash "1m8m4a2yih3ipyyav5dzfl17nmq21mnn8b2nlibz1xb0l1lq4gdc") (rust-version "1.63")))

(define-public crate-lctree-0.2 (crate (name "lctree") (vers "0.2.1") (deps (list (crate-dep (name "rand") (req "^0.8") (default-features #t) (kind 2)) (crate-dep (name "rand_derive2") (req "^0.1.21") (default-features #t) (kind 2)))) (hash "1dnd92yfqpqx2s53bdzds9qlybpvhx9z8ldv51p2ahziyxaq429a") (rust-version "1.63")))

(define-public crate-lctree-0.2 (crate (name "lctree") (vers "0.2.2") (deps (list (crate-dep (name "rand") (req "^0.8") (default-features #t) (kind 2)) (crate-dep (name "rand_derive2") (req "^0.1.21") (default-features #t) (kind 2)))) (hash "12xpfh6mgyjlhx0xfqbd2x90xd1hzkgwnvizijjpf0l1q95srllp") (rust-version "1.63")))

(define-public crate-lctree-0.2 (crate (name "lctree") (vers "0.2.3") (deps (list (crate-dep (name "rand") (req "^0.8") (default-features #t) (kind 2)) (crate-dep (name "rand_derive2") (req "^0.1.21") (default-features #t) (kind 2)))) (hash "1dnksbnbr74wriqxrdnmqkxlgkq57dyfsflmnccqvz305izgygri") (rust-version "1.63")))

(define-public crate-lctree-0.2 (crate (name "lctree") (vers "0.2.4") (deps (list (crate-dep (name "criterion") (req "^0.4") (features (quote ("html_reports"))) (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.8") (default-features #t) (kind 2)) (crate-dep (name "rand_derive2") (req "^0.1.21") (default-features #t) (kind 2)))) (hash "1ax8i2qmg0l938453ynkdq2kp6frgmj3p29b9n9bkssp07jz6rx6") (rust-version "1.63")))

(define-public crate-lctree-0.3 (crate (name "lctree") (vers "0.3.0") (deps (list (crate-dep (name "criterion") (req "^0.4") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.8") (default-features #t) (kind 2)) (crate-dep (name "rand_derive2") (req "^0.1.21") (default-features #t) (kind 2)))) (hash "0vcgyimw4lzycvpybyd42ipfjwfng1i5jvmaibdxw30mdg0rwvg4") (rust-version "1.63")))

(define-public crate-lctree-0.3 (crate (name "lctree") (vers "0.3.1") (deps (list (crate-dep (name "criterion") (req "^0.4") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.8") (default-features #t) (kind 2)) (crate-dep (name "rand_derive2") (req "^0.1.21") (default-features #t) (kind 2)))) (hash "1gr0ab06a9nwvb3g6aaz7jabffd21s90rnjcv4am3j5g110jgzrw") (rust-version "1.63")))

(define-public crate-lctree-0.3 (crate (name "lctree") (vers "0.3.2") (deps (list (crate-dep (name "criterion") (req "^0.4") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.8") (default-features #t) (kind 2)) (crate-dep (name "rand_derive2") (req "^0.1.21") (default-features #t) (kind 2)))) (hash "09g3zmpmwrhfafvlwcds1yzyqgxhffig0h2sm2yf7mrim39sicam") (rust-version "1.63")))

(define-public crate-lctree-0.3 (crate (name "lctree") (vers "0.3.3") (deps (list (crate-dep (name "criterion") (req "^0.5.1") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.8") (default-features #t) (kind 2)) (crate-dep (name "rand_derive2") (req "^0.1.21") (default-features #t) (kind 2)))) (hash "1qys8bvrzd1x9r0h3ix09i168vdj0r3k2sp0p1qfmkpl5d326bfw") (rust-version "1.63")))

