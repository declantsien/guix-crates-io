(define-module (crates-io lc mx) #:use-module (crates-io))

(define-public crate-lcmx-0.1 (crate (name "lcmx") (vers "0.1.0") (deps (list (crate-dep (name "gcdx") (req "^0") (default-features #t) (kind 0)))) (hash "017p1ais4kgmjvjy7xkfbwh6r2aizpwsm03gkwcfaadisqpmv5s3")))

(define-public crate-lcmx-0.1 (crate (name "lcmx") (vers "0.1.1") (deps (list (crate-dep (name "gcdx") (req "^0") (default-features #t) (kind 0)))) (hash "1d22gbg6p0m6dcz01pag1l5nwc6wx637zq77312cy77hm2rffikj")))

(define-public crate-lcmx-0.1 (crate (name "lcmx") (vers "0.1.2") (deps (list (crate-dep (name "gcdx") (req "^0") (default-features #t) (kind 0)))) (hash "08ljyb3ppfajkqwv4gwbdrfmv5crm86dn41ymhljizzdydghbh2y")))

(define-public crate-lcmx-0.1 (crate (name "lcmx") (vers "0.1.3") (deps (list (crate-dep (name "gcdx") (req "^0") (default-features #t) (kind 0)))) (hash "1jgg1lljn836zjx494zsxkd8n4gf4yhr4x04ggqj5r0yn4npg54f")))

(define-public crate-lcmx-0.1 (crate (name "lcmx") (vers "0.1.4") (deps (list (crate-dep (name "gcdx") (req "^0") (default-features #t) (kind 0)))) (hash "1ayagiyb1ly5b3qrl3m7hbxl3sh6r04vzq3pj2567dkargjryayb")))

