(define-module (crates-io lc g-) #:use-module (crates-io))

(define-public crate-lcg-rand-1 (crate (name "lcg-rand") (vers "1.0.0") (hash "0wgynvbg555avfwnapabs9xzffg1vk7indcxlxzbp4n6dms4rji0")))

(define-public crate-lcg-tools-0.1 (crate (name "lcg-tools") (vers "0.1.0") (deps (list (crate-dep (name "itertools") (req "^0.9.0") (default-features #t) (kind 0)) (crate-dep (name "num") (req "^0.3.1") (default-features #t) (kind 0)) (crate-dep (name "num-bigint") (req "^0.3.1") (default-features #t) (kind 0)))) (hash "0198g0a3snashgylyhaglfbw0b2ga1vbihm8jgli066j3sgcpg4d")))

