(define-module (crates-io lc d-) #:use-module (crates-io))

(define-public crate-lcd-ili9341-0.1 (crate (name "lcd-ili9341") (vers "0.1.0") (hash "1xxza4wwm6l7lkj012dq96v6f9qpkhkk2r8zri9vpb377fk3cvbs")))

(define-public crate-lcd-lcm1602-i2c-0.1 (crate (name "lcd-lcm1602-i2c") (vers "0.1.0") (deps (list (crate-dep (name "embedded-hal") (req "^0.2.3") (default-features #t) (kind 0)) (crate-dep (name "ufmt") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "ufmt-write") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "158hcmamnw9759ydycxnf9mrkk14bkj442l8rvdbzy569xwky4kw")))

(define-public crate-lcd-pcf8574-0.1 (crate (name "lcd-pcf8574") (vers "0.1.2") (deps (list (crate-dep (name "i2cdev") (req "^0.5.0") (default-features #t) (kind 0)) (crate-dep (name "lcd") (req "^0.4.1") (default-features #t) (kind 0)))) (hash "0za8vyg2xxdhgjfyk6aqm35006gsdlijkxxzzg843na4hs4ry7dq")))

(define-public crate-lcd-pcf8574-0.2 (crate (name "lcd-pcf8574") (vers "0.2.0") (deps (list (crate-dep (name "i2cdev") (req "^0.5.0") (default-features #t) (kind 0)) (crate-dep (name "lcd") (req "^0.4.1") (default-features #t) (kind 0)))) (hash "0qfkw0pwvql3nq8l4a3xyvd0ls2hh6y21i1kfn6nm0x4ksr3mddj")))

