(define-module (crates-io lc p-) #:use-module (crates-io))

(define-public crate-lcp-add-one-0.1 (crate (name "lcp-add-one") (vers "0.1.0") (hash "0dx4rfsx89ilyhix8mg9zfjjqfrxgk7s8rxmhgw873bjgjkchm4g")))

(define-public crate-lcp-adder-0.1 (crate (name "lcp-adder") (vers "0.1.0") (deps (list (crate-dep (name "lcp-add-one") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "random") (req "^0.12.2") (default-features #t) (kind 0)))) (hash "11z3cipcj89405vdv42fn72mqwx6rn87mwyl62sqzgdx23ac52zx")))

