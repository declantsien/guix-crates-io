(define-module (crates-io lc #{70}#) #:use-module (crates-io))

(define-public crate-lc709203-0.1 (crate (name "lc709203") (vers "0.1.0") (deps (list (crate-dep (name "embedded-hal") (req "^0.2.3") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4") (optional #t) (default-features #t) (kind 0)))) (hash "1a6jj61yhn94isncw0j43kg4j0gzcy1ifprhj1j7y4hyj951b378") (features (quote (("default" "log"))))))

(define-public crate-lc709203-0.2 (crate (name "lc709203") (vers "0.2.0") (deps (list (crate-dep (name "embedded-hal") (req "^0.2.3") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4") (optional #t) (default-features #t) (kind 0)))) (hash "035p73hfky6bcywzrcxfybd0apqn111rny6i3bmj2mncq799l96r") (features (quote (("default" "log"))))))

(define-public crate-lc709203-0.2 (crate (name "lc709203") (vers "0.2.1") (deps (list (crate-dep (name "embedded-hal") (req "^0.2.3") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4") (optional #t) (default-features #t) (kind 0)))) (hash "03p5p1d821vzivdagk6rjlbacikpws2n6k5w454vdcrips3fyx7w") (features (quote (("default" "log"))))))

