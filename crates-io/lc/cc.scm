(define-module (crates-io lc cc) #:use-module (crates-io))

(define-public crate-lccc-0.1 (crate (name "lccc") (vers "0.1.0") (hash "0k1q3vd7mjbxdrrh228vfy09p5mqhv0303msnrvk5y9gvq4amanx")))

(define-public crate-lccc-siphash-0.1 (crate (name "lccc-siphash") (vers "0.1.0") (hash "1z3ivzllhhl4mnypgcaacldrx66fbr7f0rbcqr7lcz3q1khqbl8m")))

