(define-module (crates-io lc #{3_}#) #:use-module (crates-io))

(define-public crate-lc3_vm-0.1 (crate (name "lc3_vm") (vers "0.1.0") (deps (list (crate-dep (name "byteorder") (req "^1.2.7") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2.45") (default-features #t) (kind 0)) (crate-dep (name "signal-hook") (req "^0.1.6") (default-features #t) (kind 0)) (crate-dep (name "termios") (req "^0.3.1") (default-features #t) (kind 0)))) (hash "1b411b68hv3fd7g9rcjpm7g23lnyy4aic1ks68k87pgn3qy7nnsr")))

