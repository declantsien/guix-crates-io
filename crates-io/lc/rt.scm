(define-module (crates-io lc rt) #:use-module (crates-io))

(define-public crate-lcrt-0.1 (crate (name "lcrt") (vers "0.1.0") (hash "128z12fprjhvclk2w807dpad1npk0nblkm22kn0cziy7dzdliwpr")))

(define-public crate-lcrt-0.1 (crate (name "lcrt") (vers "0.1.1") (deps (list (crate-dep (name "cool_asserts") (req "^2.0.3") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "pretty_assertions") (req "^1.3.0") (optional #t) (default-features #t) (kind 0)))) (hash "10ssgb172476cq25vdb27n6w3m0mii0mvap3hm1f9p3mxiijz0ir") (features (quote (("default") ("all" "testing")))) (v 2) (features2 (quote (("testing" "dep:cool_asserts" "dep:pretty_assertions"))))))

(define-public crate-lcrt-0.1 (crate (name "lcrt") (vers "0.1.2") (deps (list (crate-dep (name "cool_asserts") (req "^2.0.3") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "pretty_assertions") (req "^1.3.0") (optional #t) (default-features #t) (kind 0)))) (hash "1vrgvyrjqlwwn0s8qymp63227dhakkc7jc5rrmrxh7bmmfm6afbm") (features (quote (("default") ("all" "testing")))) (v 2) (features2 (quote (("testing" "dep:cool_asserts" "dep:pretty_assertions"))))))

(define-public crate-lcrt-macro-0.1 (crate (name "lcrt-macro") (vers "0.1.0") (deps (list (crate-dep (name "darling") (req "^0.14.3") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1.0.50") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.38") (default-features #t) (kind 0)) (crate-dep (name "pretty_assertions") (req "^1.3.0") (default-features #t) (kind 2)) (crate-dep (name "prettyplease") (req "^0.1.23") (default-features #t) (kind 2)))) (hash "1rzzcbpgs8zb06x7zrv1hq9463jqry885zjf4daf1nl1yvl0l9dz")))

