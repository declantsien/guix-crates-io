(define-module (crates-io lc d1) #:use-module (crates-io))

(define-public crate-lcd1602-diver-0.1 (crate (name "lcd1602-diver") (vers "0.1.0") (deps (list (crate-dep (name "bitflags") (req "^2.4.0") (default-features #t) (kind 0)) (crate-dep (name "embedded-hal") (req "^0.2.7") (default-features #t) (kind 0)))) (hash "1ccvprcv8qhnlyqaswb192rjz7dllz3vg41bz9d8s1w9cq2wz00k")))

(define-public crate-lcd1602-diver-0.1 (crate (name "lcd1602-diver") (vers "0.1.1") (deps (list (crate-dep (name "bitflags") (req "^2.4.0") (default-features #t) (kind 0)) (crate-dep (name "embedded-hal") (req "^0.2.7") (default-features #t) (kind 0)))) (hash "1zjqvpdw7gaj9mc42nyv58ysyidh7a8djb8ibl40yajd3wf8ldl4")))

(define-public crate-lcd1602-driver-0.1 (crate (name "lcd1602-driver") (vers "0.1.0") (deps (list (crate-dep (name "embedded-hal") (req "0.2.*") (features (quote ("unproven"))) (default-features #t) (kind 0)))) (hash "166wcajy81khwhgaqv8wbh18cwaz5m7r7raf4wxr3g0rbh88f9mc")))

(define-public crate-lcd1602-driver-0.2 (crate (name "lcd1602-driver") (vers "0.2.0") (deps (list (crate-dep (name "embedded-hal") (req "^1") (default-features #t) (kind 0)))) (hash "0ac9dbaz58a9vn7ir7m66i51vpn1lz899dgh3d9lgjx6pb155dnd")))

(define-public crate-lcd1602-rs-0.1 (crate (name "lcd1602-rs") (vers "0.1.0") (deps (list (crate-dep (name "embedded-hal") (req "^0.2.5") (default-features #t) (kind 0)) (crate-dep (name "nb") (req "^1.0.0") (default-features #t) (kind 0)))) (hash "1hfn66sn70dxrrg6yrn0pp9makfdrz9qq8fjqz8ibi73ffhikish")))

(define-public crate-lcd1602_gpio-0.1 (crate (name "lcd1602_gpio") (vers "0.1.0") (deps (list (crate-dep (name "rppal") (req "^0.16.0") (default-features #t) (kind 0)))) (hash "0gg7hknhrg3lv9q8njfl1bipk1a6zwq1y347pa9ln5fvpj0yxhqd")))

(define-public crate-lcd1602rgb-rs-0.1 (crate (name "lcd1602rgb-rs") (vers "0.1.0") (deps (list (crate-dep (name "cortex-m") (req "^0.7") (default-features #t) (kind 0)) (crate-dep (name "cortex-m-rt") (req "^0.7") (default-features #t) (kind 0)) (crate-dep (name "embedded-hal") (req "^0.2.7") (default-features #t) (kind 0)) (crate-dep (name "rp-pico") (req "^0.5.0") (default-features #t) (kind 0)))) (hash "0x0bwzc71lw3cqdfz5f25npx0kndhf8f6k2siirnr78ip9xprr8x")))

(define-public crate-lcd1602rgb-rs-0.1 (crate (name "lcd1602rgb-rs") (vers "0.1.1") (deps (list (crate-dep (name "embedded-hal") (req "^0.2.7") (default-features #t) (kind 0)))) (hash "0b42nb0m6gcgndm8qi3j95f5rb0fnjcx1wci9rx47v0iwszn3w23")))

(define-public crate-lcd1602rgb-rs-0.2 (crate (name "lcd1602rgb-rs") (vers "0.2.0") (deps (list (crate-dep (name "embedded-hal") (req "^0.2.7") (default-features #t) (kind 0)))) (hash "0nimsd83cbsgr31qnf0sx1f037d08p6n9sv51r22v4zf3n0b4y19")))

(define-public crate-lcd1604_gpio-1 (crate (name "lcd1604_gpio") (vers "1.0.0") (deps (list (crate-dep (name "rppal") (req "^0.16.0") (default-features #t) (kind 0)))) (hash "1yfxsx5dhp4xnwqqd7xqwkxakhy7vqvd473yr9rq0df46kvn17ld")))

(define-public crate-lcd1604_gpio-1 (crate (name "lcd1604_gpio") (vers "1.0.1") (deps (list (crate-dep (name "rppal") (req "^0.16.0") (default-features #t) (kind 0)))) (hash "1rxq2aiz540c4d6s5zz441lra9fpvyf7h9l28f8a71wfis1nc8p6")))

