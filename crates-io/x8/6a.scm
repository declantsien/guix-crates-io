(define-module (crates-io x8 #{6a}#) #:use-module (crates-io))

(define-public crate-x86asm-0.1 (crate (name "x86asm") (vers "0.1.0") (deps (list (crate-dep (name "arrayvec") (req "^0.3.23") (default-features #t) (kind 0)) (crate-dep (name "byteorder") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "matches") (req "^0.1") (default-features #t) (kind 0)))) (hash "1k0pw8ans5zs8ir0xmwfhjx72s18lli6pb4li2l6z7ccbw8lqkb5")))

