(define-module (crates-io x8 #{6i}#) #:use-module (crates-io))

(define-public crate-x86intrin-0.1 (crate (name "x86intrin") (vers "0.1.0") (hash "0x80zmm7x9pyf9bvjj9cm08jjr17ql1x05z5fawmv5j7bpqh8h0w")))

(define-public crate-x86intrin-0.2 (crate (name "x86intrin") (vers "0.2.0") (hash "1jpg58mnmdpw92f8fxlwnvwf2insvvqxicjlhlyac7yzyi7s6z1w")))

(define-public crate-x86intrin-0.2 (crate (name "x86intrin") (vers "0.2.1") (hash "1kds5cz9bkx72nmb1pqw64v5jd9137mrwighgznzal5dlldfz6fp")))

(define-public crate-x86intrin-0.3 (crate (name "x86intrin") (vers "0.3.0") (hash "0fqic6igblnzrssdsygb6jpn3swd5arifinfafqjalh6wiv1bsxd") (features (quote (("doc"))))))

(define-public crate-x86intrin-0.3 (crate (name "x86intrin") (vers "0.3.1") (hash "1769fzsjlgia8ain61wn7sw1ns7q271bj4r67pbksfba329qz8dl") (features (quote (("doc"))))))

(define-public crate-x86intrin-0.4 (crate (name "x86intrin") (vers "0.4.0") (hash "0v0yp3y3yxxw6gvww4m7gkhzf2y55rz4cyjclqabrnrxvgv4snkg") (features (quote (("doc"))))))

(define-public crate-x86intrin-0.4 (crate (name "x86intrin") (vers "0.4.1") (hash "1m9s5fhba89mz7kpg7374hzhkcp4m66zccrgynjy7d69wgdz22lj") (features (quote (("doc"))))))

(define-public crate-x86intrin-0.4 (crate (name "x86intrin") (vers "0.4.2") (hash "1d3w4f14rimggf5nvpxvrdhfvwv2486li0w272sb3q90lyw56y9x") (features (quote (("doc"))))))

(define-public crate-x86intrin-0.4 (crate (name "x86intrin") (vers "0.4.3") (hash "0vbhi3azdpclapnls22c4h4r6z4l1w8favgky54581hr9jkyfraz") (features (quote (("doc"))))))

(define-public crate-x86intrin-0.4 (crate (name "x86intrin") (vers "0.4.4") (hash "0b1vw9rq2c1mq88vjzp8z3c46l39vsyzfsw4pdfmhxkkh215vdw9") (features (quote (("doc"))))))

(define-public crate-x86intrin-0.4 (crate (name "x86intrin") (vers "0.4.5") (hash "07nzn2zccdm3aklq3azvb6kw7k8zfwn3wzyix5q9p1zy44vm5v78") (features (quote (("doc"))))))

