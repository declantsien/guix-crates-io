(define-module (crates-io x8 #{6-}#) #:use-module (crates-io))

(define-public crate-x86-alignment-check-0.1 (crate (name "x86-alignment-check") (vers "0.1.1") (hash "0075vi0k0lpwvymwp6i8p553vygxai0kiphfialgv7v7h461s0c3") (yanked #t) (rust-version "1.59.0")))

(define-public crate-x86-alignment-check-0.1 (crate (name "x86-alignment-check") (vers "0.1.2") (hash "0xmnnprl89dxxmnplrif4lli96x1j0gdvjm19z2jasz25bw1y9jp") (rust-version "1.59.0")))

(define-public crate-x86-alignment-check-0.1 (crate (name "x86-alignment-check") (vers "0.1.3") (hash "1ah5iv1k6jg8j7gjjlxgqzplkf5zsqchy6jyqdwr0svam9amgzrm") (rust-version "1.59.0")))

(define-public crate-x86-alignment-check-0.1 (crate (name "x86-alignment-check") (vers "0.1.4") (hash "1f0lz5km7ssh4havrl1wh4c0x9vya8bwqg42j4ddkmxwxd24ib93") (rust-version "1.59.0")))

(define-public crate-x86-alignment-check-0.1 (crate (name "x86-alignment-check") (vers "0.1.5") (hash "075pq72jffz5w1ymq8b7qbblgp5c2ia2qmr1a0d4cpv2wnzi8a0l") (rust-version "1.59.0")))

(define-public crate-x86-alignment-check-0.1 (crate (name "x86-alignment-check") (vers "0.1.6") (hash "137l2qa4sik3m4lylzngqmnni89vkmlzz2s07n8w363d9hym65rz") (rust-version "1.59.0")))

(define-public crate-x86-instruction-set-analyzer-0.1 (crate (name "x86-instruction-set-analyzer") (vers "0.1.0") (deps (list (crate-dep (name "iced-x86") (req "^1.21.0") (default-features #t) (kind 0)) (crate-dep (name "object") (req "^0.35.0") (default-features #t) (kind 0)))) (hash "0bvb901gw1xya16l0lszdspd8y2n30d65a4wvxw9zls89kcq20nr")))

