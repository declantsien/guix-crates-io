(define-module (crates-io x8 #{6r}#) #:use-module (crates-io))

(define-public crate-x86reducer-0.1 (crate (name "x86reducer") (vers "0.1.0") (deps (list (crate-dep (name "byteorder") (req "^1.4.3") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^3.0.14") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "itertools") (req "^0.10.3") (default-features #t) (kind 0)))) (hash "1wmzjnpl2cpqwbhbh34dqbw550z02y13idiicg82vvgsgdxrlm00")))

