(define-module (crates-io u1 #{44}#) #:use-module (crates-io))

(define-public crate-u144-0.1 (crate (name "u144") (vers "0.1.0") (deps (list (crate-dep (name "uints") (req "^0.5.0") (default-features #t) (kind 0)))) (hash "04rypcprkg69qmr0vnjbwj4qr3z67pqkb356m4580d2hbflxk30h")))

(define-public crate-u144-0.2 (crate (name "u144") (vers "0.2.0") (deps (list (crate-dep (name "uints") (req "^0.6.0") (default-features #t) (kind 0)))) (hash "0k1i913p7kk45kasqv5lv3cvsmzzg3i6wpvbimap9cfzx6iq0wn3")))

(define-public crate-u144-0.3 (crate (name "u144") (vers "0.3.0") (deps (list (crate-dep (name "uints") (req "^0.6.0") (default-features #t) (kind 0)))) (hash "0xqv8kpk9v9sgc3by1cd6q8y3zsmfr6462z8jvyxhr54hzpji302")))

(define-public crate-u144-0.3 (crate (name "u144") (vers "0.3.1") (deps (list (crate-dep (name "uints") (req "^0.6.1") (default-features #t) (kind 0)))) (hash "1754c9k02301x42ld1sxfm4j7qm39500fkc0xkmf9y7gjpgmlidb")))

(define-public crate-u144-0.4 (crate (name "u144") (vers "0.4.0") (deps (list (crate-dep (name "uints") (req "^0.7.0") (default-features #t) (kind 0)))) (hash "0r5k3y7j8fagnc3d7yvzg12zkxy2k5x0yjn2y1y9cx3c0ssc5qfi")))

(define-public crate-u144-0.5 (crate (name "u144") (vers "0.5.0") (deps (list (crate-dep (name "uints") (req "^0.8.0") (default-features #t) (kind 0)))) (hash "1ijvlpbf0hpjv6chh71y2gaxwfax02rcx4mr6y6jpkgrsxj9ihih")))

(define-public crate-u144-0.5 (crate (name "u144") (vers "0.5.1") (deps (list (crate-dep (name "uints") (req "^0.8.1") (default-features #t) (kind 0)))) (hash "0hwh4vly25h9f7zmycpd0a3axlp2pib4dr6k5ypbd8rp211985hq")))

(define-public crate-u144-0.6 (crate (name "u144") (vers "0.6.0") (deps (list (crate-dep (name "uints") (req "^0.9.0") (default-features #t) (kind 0)))) (hash "0h1jypxhqb4jadfp7c357d9pnw54ilfcdmxklkljfdcbw8z3qnvj")))

(define-public crate-u144-0.7 (crate (name "u144") (vers "0.7.0") (deps (list (crate-dep (name "uints") (req "^0.10.0") (default-features #t) (kind 0)))) (hash "1wrj2al1akl98kpwib8xzx8ggdll8zq7cnhp9qh2mkrf62yvrd16")))

(define-public crate-u144-0.7 (crate (name "u144") (vers "0.7.1") (deps (list (crate-dep (name "uints") (req "^0.11.1") (default-features #t) (kind 0)))) (hash "1bznq4cdcgkb1kxxc9aa8vdvzz5xzfnbjw9cza75csmqwf2yasp9")))

(define-public crate-u144-0.7 (crate (name "u144") (vers "0.7.2") (deps (list (crate-dep (name "uints") (req "^0.11.2") (default-features #t) (kind 0)))) (hash "1cj1q98d4jr3happmpgjinvv0j8w5kzhg2axxqkxhfjhdjcry0yj")))

