(define-module (crates-io u1 #{28}#) #:use-module (crates-io))

(define-public crate-u128-0.1 (crate (name "u128") (vers "0.1.0") (deps (list (crate-dep (name "uint") (req "=0.9.1") (kind 0)))) (hash "1wyfdjl3wsbg6gp4ci738jjwc8mwiycwbixqpgqsi7mmwvqlp4ms")))

