(define-module (crates-io u1 #{60}#) #:use-module (crates-io))

(define-public crate-u160-0.1 (crate (name "u160") (vers "0.1.0") (hash "1ndhqhbjffk3v55fsj0bwbsvflr4kczqwqky5bd1ss7244pkx1cq")))

(define-public crate-u160-0.1 (crate (name "u160") (vers "0.1.1") (hash "191l9hjac9hz0zwf92f8jdv2k6ik8xfqzag6fhpwkw5ij5w1p70c")))

(define-public crate-u160-0.2 (crate (name "u160") (vers "0.2.0") (hash "1y4wm55ihzzrga802vhp5y8nbykkvpi3kzacajq2fa8vblicsb9v")))

(define-public crate-u160-0.2 (crate (name "u160") (vers "0.2.1") (hash "1aq8ksmwgn6r0d65nyviy2aqbqw8l3d1ard0nx8qims8zkikjz1h")))

