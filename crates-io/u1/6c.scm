(define-module (crates-io u1 #{6c}#) #:use-module (crates-io))

(define-public crate-u16cstr-0.1 (crate (name "u16cstr") (vers "0.1.0") (deps (list (crate-dep (name "wchar") (req "^0.10") (default-features #t) (kind 0)) (crate-dep (name "widestring") (req "^0.4") (default-features #t) (kind 0)))) (hash "0ljgkc4y7d7ys7j8ms5wjabdc2l2j7qfmf8wn2bb6xhqjraan956")))

(define-public crate-u16cstr-0.1 (crate (name "u16cstr") (vers "0.1.1") (deps (list (crate-dep (name "wchar") (req "^0.10") (default-features #t) (kind 0)) (crate-dep (name "widestring") (req "^0.4") (default-features #t) (kind 0)))) (hash "1xgp5nncwmgjzy36db962z3375kf02wv4hjb7qb5x1prgbyf7wh0")))

(define-public crate-u16cstr-0.2 (crate (name "u16cstr") (vers "0.2.0") (deps (list (crate-dep (name "wchar") (req "^0.10") (default-features #t) (kind 0)) (crate-dep (name "widestring") (req "^0.4") (default-features #t) (kind 0)))) (hash "1rygyr4fx6xjsqh077gwppdsvliyaig6qkznj922gpqdql5cjsp6")))

(define-public crate-u16cstr-0.2 (crate (name "u16cstr") (vers "0.2.1") (deps (list (crate-dep (name "wchar") (req "^0.10") (default-features #t) (kind 0)) (crate-dep (name "widestring") (req "^0.4") (default-features #t) (kind 0)))) (hash "1yqqr2vraadrnv7lk4cvdmy23acxxz2wblab511bl8j9gvp7jrlz")))

(define-public crate-u16cstr-0.2 (crate (name "u16cstr") (vers "0.2.2") (deps (list (crate-dep (name "wchar") (req "^0.11") (default-features #t) (kind 0)) (crate-dep (name "widestring") (req "^0.4") (default-features #t) (kind 0)))) (hash "1r3vxnl2bw93phv5r4nzk803x08ikv7qzjn3kk3yn1mrjpxniggg")))

(define-public crate-u16cstr-0.3 (crate (name "u16cstr") (vers "0.3.0") (deps (list (crate-dep (name "wchar") (req "^0.11") (default-features #t) (kind 0)) (crate-dep (name "widestring") (req "^0.5") (default-features #t) (kind 0)))) (hash "18s9ajg0phv0asx2cg56m0svq0hgfkjci48bhw913y7myzb6lbwc")))

(define-public crate-u16cstr-0.3 (crate (name "u16cstr") (vers "0.3.1") (deps (list (crate-dep (name "wchar") (req "^0.11") (default-features #t) (kind 0)) (crate-dep (name "widestring") (req "^0.5") (kind 0)))) (hash "0dfk290c4agkj4f7mcf3pw6wpkfqvza964ljsy6jg0prj0c7y0kx")))

(define-public crate-u16cstr-0.4 (crate (name "u16cstr") (vers "0.4.0") (deps (list (crate-dep (name "wchar") (req "^0.11") (kind 0)) (crate-dep (name "widestring") (req "^1.0") (kind 0)) (crate-dep (name "widestring") (req "^1.0") (features (quote ("alloc"))) (default-features #t) (kind 2)))) (hash "10fm18lpzgkqkszbbr2w8pl9ak4l2r61xzpam01zaasvgdym5yfj")))

(define-public crate-u16cstr-0.5 (crate (name "u16cstr") (vers "0.5.0") (deps (list (crate-dep (name "widestring") (req "^1.0") (kind 0)) (crate-dep (name "widestring") (req "^1.0") (features (quote ("alloc"))) (default-features #t) (kind 2)))) (hash "1srjwb2k16zv8qp57480p0ili91pzp9mikgqgf4rhjfrnyjjgm80") (yanked #t)))

