(define-module (crates-io cy rl) #:use-module (crates-io))

(define-public crate-cyrla-0.1 (crate (name "cyrla") (vers "0.1.0") (deps (list (crate-dep (name "trie-rs") (req "^0.1.1") (default-features #t) (kind 0)))) (hash "03xbq1w3x8x1g4781gnpg3cn5z9n142fpbvn65gbw17sz669xc1z")))

(define-public crate-cyrly-0.1 (crate (name "cyrly") (vers "0.1.0") (deps (list (crate-dep (name "ryu") (req "^1.0.13") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.159") (kind 0)) (crate-dep (name "serde") (req "^1.0.159") (features (quote ("derive"))) (default-features #t) (kind 2)) (crate-dep (name "serde_yaml") (req "^0.9.19") (default-features #t) (kind 2)) (crate-dep (name "urlencoding") (req "^2.1.2") (default-features #t) (kind 0)))) (hash "1njqx0kf2ki9wbr3b8yman5war3cd5zhmq1lhcv0l4vcbn7lpblk") (features (quote (("std" "serde/std") ("default" "std")))) (rust-version "1.62")))

