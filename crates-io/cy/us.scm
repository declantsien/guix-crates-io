(define-module (crates-io cy us) #:use-module (crates-io))

(define-public crate-cyusb-0.9 (crate (name "cyusb") (vers "0.9.0") (deps (list (crate-dep (name "rusb") (req "^0.5.1") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.3") (default-features #t) (kind 0)))) (hash "0lv3rdn6saqlnyyzdc6x67i0p5k73jllhvdvrswrgvxs0zjqb9p4")))

(define-public crate-cyusb-0.9 (crate (name "cyusb") (vers "0.9.1") (deps (list (crate-dep (name "env_logger") (req "^0.9.0") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.17") (default-features #t) (kind 0)) (crate-dep (name "rusb") (req "^0.9.1") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.3.26") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.31") (default-features #t) (kind 0)))) (hash "04bh7pz9c8z0b1yz4aidg9hsv05m8yinxx1wfr5wgbfrkk50w35b")))

