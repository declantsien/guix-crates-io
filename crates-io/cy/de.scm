(define-module (crates-io cy de) #:use-module (crates-io))

(define-public crate-cyder-0.1 (crate (name "cyder") (vers "0.1.0") (deps (list (crate-dep (name "windows") (req "^0.44.0") (features (quote ("Win32_Foundation" "Win32_Graphics_Direct3D" "Win32_Graphics_Direct3D_Fxc" "Win32_Graphics_Direct3D12" "Win32_Graphics_Dxgi" "Win32_Graphics_Dxgi_Common" "Win32_Graphics_Gdi" "Win32_Security" "Win32_System_LibraryLoader" "Win32_System_Memory" "Win32_System_Threading" "Win32_System_WindowsProgramming" "Win32_UI_WindowsAndMessaging"))) (default-features #t) (kind 0)))) (hash "18vdnbywrbn33n5596m0aiblidfd3ijfwbls98wy41aadkiprsfr")))

