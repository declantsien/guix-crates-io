(define-module (crates-io cy mr) #:use-module (crates-io))

(define-public crate-cymrust-0.3 (crate (name "cymrust") (vers "0.3.0") (deps (list (crate-dep (name "chrono") (req "^0.2.25") (default-features #t) (kind 0)) (crate-dep (name "resolve") (req "^0.1.2") (default-features #t) (kind 0)))) (hash "069fhz6wi2m4l513m2f2kj58i7zs72j7kpq6yliij065in3rgp2b")))

(define-public crate-cymrust-0.3 (crate (name "cymrust") (vers "0.3.1") (deps (list (crate-dep (name "chrono") (req "^0.2.25") (default-features #t) (kind 0)) (crate-dep (name "resolve") (req "^0.1.2") (default-features #t) (kind 0)))) (hash "040p14axid0adl8a5p8983w5hg9gahc71pz517pn1q4rjckkpzj3")))

(define-public crate-cymrust-0.3 (crate (name "cymrust") (vers "0.3.2") (deps (list (crate-dep (name "chrono") (req "^0.2.25") (default-features #t) (kind 0)) (crate-dep (name "resolve") (req "^0.1.2") (default-features #t) (kind 0)))) (hash "0km78shdhrdr8qiaqayqx75gldbyx04dh2kfrvb319q54sw93ldf")))

(define-public crate-cymrust-0.3 (crate (name "cymrust") (vers "0.3.3") (deps (list (crate-dep (name "chrono") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "resolve") (req "^0.1.2") (default-features #t) (kind 0)))) (hash "0cpvvymiwvpadfh4cyr14akfqfasq2h742a2ny9l0da7kzw6ssf6")))

(define-public crate-cymrust-0.3 (crate (name "cymrust") (vers "0.3.4") (deps (list (crate-dep (name "chrono") (req "^0.4.0") (default-features #t) (kind 0)) (crate-dep (name "resolve") (req "^0.1.2") (default-features #t) (kind 0)))) (hash "0p2s638kaa2kqq79w8m81ynp74ihxx1bl9h2fb22vlyas48am3h4")))

(define-public crate-cymrust-0.4 (crate (name "cymrust") (vers "0.4.0") (deps (list (crate-dep (name "chrono") (req ">=0.4.0, <0.5.0") (default-features #t) (kind 0)) (crate-dep (name "trust-dns-resolver") (req ">=0.19.0, <0.20.0") (default-features #t) (kind 0)))) (hash "0w759xqcylyr0kqsbj49qhr874jzgbq72afrb0bli7p5cm5lzhas")))

(define-public crate-cymrust-0.4 (crate (name "cymrust") (vers "0.4.1") (deps (list (crate-dep (name "chrono") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "trust-dns-resolver") (req "^0.20") (default-features #t) (kind 0)))) (hash "05jz0vin7nkca245dhi8m4xwr9x28qfrhdz9bcw4kv6ixaawflnz")))

