(define-module (crates-io cy oa) #:use-module (crates-io))

(define-public crate-cyoa-0.1 (crate (name "cyoa") (vers "0.1.0") (deps (list (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)))) (hash "0s6qrd3far09v6i4v5853khigcg2zpqicargqaz2i6xb3zbkd08r")))

(define-public crate-cyoa-0.1 (crate (name "cyoa") (vers "0.1.2") (deps (list (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "wasm-bindgen") (req "^0.2") (features (quote ("serde-serialize"))) (default-features #t) (target "cfg(target_arch = \"wasm32\")") (kind 0)))) (hash "1m28b419yhxys9ndkff6qq7nv145bgx3h41wx1m943wqahnwhbfq")))

(define-public crate-cyoa-0.1 (crate (name "cyoa") (vers "0.1.3") (deps (list (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "wasm-bindgen") (req "^0.2") (features (quote ("serde-serialize"))) (default-features #t) (target "cfg(target_arch = \"wasm32\")") (kind 0)))) (hash "0ig5y1dk06c8k8kk4sjiampcjdaqwgycjk1x6ndhb6wic3f4gq5y")))

