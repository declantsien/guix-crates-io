(define-module (crates-io cy t6) #:use-module (crates-io))

(define-public crate-cyt6bj-0.0.1 (crate (name "cyt6bj") (vers "0.0.1") (deps (list (crate-dep (name "cyt6bj_a") (req "^0.0.1") (optional #t) (default-features #t) (kind 0)))) (hash "07dlsd2dfkfb6dkkidalp1vq5rz76l4zdbvhb1j0ql67vhc1lw4m") (features (quote (("rev_a" "cyt6bj_a") ("default" "rev_a")))) (v 2) (features2 (quote (("rt" "cyt6bj_a?/rt") ("critical-section" "cyt6bj_a?/critical-section")))) (rust-version "1.64")))

(define-public crate-cyt6bj_a-0.0.1 (crate (name "cyt6bj_a") (vers "0.0.1") (deps (list (crate-dep (name "cortex-m") (req "^0.7.6") (default-features #t) (kind 0)) (crate-dep (name "cortex-m-rt") (req "^0.7") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "critical-section") (req "^1.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "vcell") (req "^0.1.2") (default-features #t) (kind 0)))) (hash "0sympwf6pj9ng4h6ksa7gcb9m1glvlgb13qjmn2rk61ck5kj3a5f") (features (quote (("rt" "cortex-m-rt/device")))) (rust-version "1.64")))

