(define-module (crates-io cy lu) #:use-module (crates-io))

(define-public crate-cylun-0.1 (crate (name "cylun") (vers "0.1.0") (deps (list (crate-dep (name "nix") (req "^0.11.0") (default-features #t) (kind 0)))) (hash "1fip37gjlyln6ix83kwfrd72q5giih912x7p4zbs7bvh86djzjrg")))

(define-public crate-cylus-0.1 (crate (name "cylus") (vers "0.1.0") (deps (list (crate-dep (name "mmap") (req "^0.1.1") (default-features #t) (kind 0)))) (hash "1zgrrz3q5p8yv5z179msyg5yjvknfnznsms9cqgwi0hhb8wfa05a")))

