(define-module (crates-io cy qu) #:use-module (crates-io))

(define-public crate-cyque-0.0.1 (crate (name "cyque") (vers "0.0.1") (deps (list (crate-dep (name "llq") (req "^0.1.1") (default-features #t) (kind 0)))) (hash "1p8sklzqjkzl6i0vmvbjxxbwq6gg217qjkj2j7w5wrwkxf1c5l67") (yanked #t)))

