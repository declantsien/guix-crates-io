(define-module (crates-io cy w4) #:use-module (crates-io))

(define-public crate-cyw43-0.0.0 (crate (name "cyw43") (vers "0.0.0") (hash "1bcf6x4wxphv91qnjaf72vxnm06s82xwyfvbj6rgy9fj6ysgmnwa")))

(define-public crate-cyw43-0.1 (crate (name "cyw43") (vers "0.1.0") (deps (list (crate-dep (name "cortex-m") (req "^0.7.6") (default-features #t) (kind 0)) (crate-dep (name "cortex-m-rt") (req "^0.7.0") (default-features #t) (kind 0)) (crate-dep (name "defmt") (req "^0.3") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "embassy-futures") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "embassy-net-driver-channel") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "embassy-sync") (req "^0.5.0") (default-features #t) (kind 0)) (crate-dep (name "embassy-time") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "embedded-hal-1") (req "^1.0") (default-features #t) (kind 0) (package "embedded-hal")) (crate-dep (name "futures") (req "^0.3.17") (features (quote ("async-await" "cfg-target-has-atomic" "unstable"))) (kind 0)) (crate-dep (name "log") (req "^0.4.17") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "num_enum") (req "^0.5.7") (kind 0)))) (hash "0phl4bbb5y7hlnbzx4v1jvxx13dlhn9i0sfrkc4bpzjqhxwyrmjr") (features (quote (("firmware-logs")))) (v 2) (features2 (quote (("log" "dep:log") ("defmt" "dep:defmt"))))))

(define-public crate-cyw43-bitbang-0.0.0 (crate (name "cyw43-bitbang") (vers "0.0.0") (hash "1xj5hrbdx1zhlhm9yvvw0lf6k489g82sca67204clh70j5bn8ki4")))

(define-public crate-cyw43-pio-0.0.0 (crate (name "cyw43-pio") (vers "0.0.0") (hash "0cvf0vnxplc522yfl165sxmz2j6hirm86qqv33bb6dapny9mf2wy")))

(define-public crate-cyw43-pio-0.1 (crate (name "cyw43-pio") (vers "0.1.0") (deps (list (crate-dep (name "cyw43") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "defmt") (req "^0.3") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "embassy-rp") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "fixed") (req "^1.23.1") (default-features #t) (kind 0)) (crate-dep (name "pio") (req "^0.2.1") (default-features #t) (kind 0)) (crate-dep (name "pio-proc") (req "^0.2") (default-features #t) (kind 0)))) (hash "03289qk4jdpn374bndb624r963x910fzpprzfjlf03j2x5pl8agl") (features (quote (("overclock"))))))

(define-public crate-cyw43-spi-0.0.0 (crate (name "cyw43-spi") (vers "0.0.0") (hash "1zmy1m8cvf954k7s6vxd8cnp3wf07vph77a5p66sfgmqi751662v")))

