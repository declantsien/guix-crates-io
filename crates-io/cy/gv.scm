(define-module (crates-io cy gv) #:use-module (crates-io))

(define-public crate-cygv-0.0.1 (crate (name "cygv") (vers "0.0.1") (deps (list (crate-dep (name "itertools") (req "^0.12.1") (default-features #t) (kind 0)) (crate-dep (name "nalgebra") (req "^0.32.3") (default-features #t) (kind 0)) (crate-dep (name "pyo3") (req "^0.21.1") (features (quote ("abi3-py38"))) (default-features #t) (kind 0)) (crate-dep (name "rug") (req "^1.24.0") (default-features #t) (kind 0)))) (hash "055dnw3r31y1nj50vgggcy6v6rnd2r21gnxs7smx34wghfl6zbz3")))

(define-public crate-cygv-0.0.2 (crate (name "cygv") (vers "0.0.2") (deps (list (crate-dep (name "itertools") (req "^0.13.0") (default-features #t) (kind 0)) (crate-dep (name "nalgebra") (req "^0.32.3") (default-features #t) (kind 0)) (crate-dep (name "pyo3") (req "^0.21.1") (features (quote ("abi3-py38"))) (default-features #t) (kind 0)) (crate-dep (name "rug") (req "^1.24.0") (default-features #t) (kind 0)))) (hash "028a5srg9bap27y3mayjwjadg5mfzr52jnpxbn0i1hyvc2vqlcdy")))

