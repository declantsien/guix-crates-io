(define-module (crates-io cy rc) #:use-module (crates-io))

(define-public crate-cyrconv-0.3 (crate (name "cyrconv") (vers "0.3.0") (deps (list (crate-dep (name "serde") (req "^1.0.87") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0.87") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.38") (default-features #t) (kind 0)))) (hash "1dyx6dw46dag0kn2p6g71mbhyq2lpq4aqbnkdpxjc198qav81fi7") (yanked #t)))

(define-public crate-cyrconv-0.3 (crate (name "cyrconv") (vers "0.3.1") (deps (list (crate-dep (name "serde") (req "^1.0.87") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0.87") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.38") (default-features #t) (kind 0)))) (hash "1b97x1mpxrwfxzmqli7nwdl4ipcd29lrphipv2y6sxqmap6dd43v")))

(define-public crate-cyrconv-0.3 (crate (name "cyrconv") (vers "0.3.2") (deps (list (crate-dep (name "serde") (req "^1.0.87") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0.87") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.38") (default-features #t) (kind 0)))) (hash "06pkirh9xw1j2rbr59v0zrpzckhia9ayfgxbw5afgh7j9fpcy3ws")))

(define-public crate-cyrconv-0.3 (crate (name "cyrconv") (vers "0.3.3") (deps (list (crate-dep (name "clap") (req "^2.33.0") (features (quote ("yaml"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.97") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0.97") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.40") (default-features #t) (kind 0)))) (hash "1m02gccafg2v8arbgsq0754p7pg1cwdalcjzll8zn9b0mbwgfi9h")))

(define-public crate-cyrconv-0.4 (crate (name "cyrconv") (vers "0.4.0") (deps (list (crate-dep (name "clap") (req "^2.33.0") (features (quote ("yaml"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.97") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0.97") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.40") (default-features #t) (kind 0)))) (hash "0wnhihpkvn078xicfgg5yf6wabbnz3byajy6n091lcz4vz0kpq9p")))

(define-public crate-cyrconv-0.4 (crate (name "cyrconv") (vers "0.4.1") (deps (list (crate-dep (name "clap") (req "^2.33.0") (features (quote ("yaml"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.97") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0.97") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.40") (default-features #t) (kind 0)))) (hash "0abzfz4fhamshy89264pxl28fdp6hxii0zwh476n9r4lhlx6lksx")))

