(define-module (crates-io oa c_) #:use-module (crates-io))

(define-public crate-oac_parser-0.1 (crate (name "oac_parser") (vers "0.1.0") (deps (list (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0") (default-features #t) (kind 0)))) (hash "00ymmd2gwzjaz5nv3ma9xdpv8s4zmw23rspqv7awbjw12r23yj2q")))

