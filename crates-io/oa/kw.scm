(define-module (crates-io oa kw) #:use-module (crates-io))

(define-public crate-oakwood-0.1 (crate (name "oakwood") (vers "0.1.0") (hash "15ck990i69zww0yczr6ccwcb9nld7kfjik1dglw8nkg20d0zbvsf")))

(define-public crate-oakwood-0.1 (crate (name "oakwood") (vers "0.1.1") (hash "0x5hsmmjfk5rrxn1bs8lpmdidhq79g8bs80kvx93x41i2gv5kb2w")))

(define-public crate-oakwood-0.1 (crate (name "oakwood") (vers "0.1.2") (deps (list (crate-dep (name "log") (req "^0.4.17") (default-features #t) (kind 0)))) (hash "0c42niwx31n6vbpxw5ms24xsjc486hp68rki5c63qcdms07japls")))

(define-public crate-oakwood-0.1 (crate (name "oakwood") (vers "0.1.3") (deps (list (crate-dep (name "log") (req "^0.4.17") (default-features #t) (kind 0)))) (hash "0qj428qqaxh6f0xx87bm8fb8kbrfmxvyw9bjdryzkwnl175fg8wi")))

(define-public crate-oakwood-0.1 (crate (name "oakwood") (vers "0.1.4") (deps (list (crate-dep (name "log") (req "^0.4.17") (default-features #t) (kind 0)))) (hash "1zdmkldydkjlvik0g456a804c2sfz7871d7ll9wlfqb862byx5jf")))

