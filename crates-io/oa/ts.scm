(define-module (crates-io oa ts) #:use-module (crates-io))

(define-public crate-oats-0.1 (crate (name "oats") (vers "0.1.0") (deps (list (crate-dep (name "rand") (req "^0.3.14") (default-features #t) (kind 0)) (crate-dep (name "termion") (req "^1.5.3") (default-features #t) (kind 0)) (crate-dep (name "uuid") (req "^0.8.1") (features (quote ("v4"))) (default-features #t) (kind 0)))) (hash "1q44zbd3yrvc416snflh72qccz40wxg28axx8qmgxmb0gqsmakss")))

(define-public crate-oats-rs-0.1 (crate (name "oats-rs") (vers "0.1.0") (deps (list (crate-dep (name "base64") (req "^0.20.0") (default-features #t) (kind 0)))) (hash "1j330b2pv6ha71xx9abagplr449js7rm31b6ps3ammppvp839qz6")))

(define-public crate-oats-rs-0.1 (crate (name "oats-rs") (vers "0.1.1") (deps (list (crate-dep (name "base64") (req "^0.20.0") (default-features #t) (kind 0)))) (hash "1yfjd6y9ddkbgnqny5qzrwk598bqij2fj0ly23y90b04y6jc6pml")))

(define-public crate-oats-rs-0.1 (crate (name "oats-rs") (vers "0.1.2") (deps (list (crate-dep (name "base64") (req "^0.20.0") (default-features #t) (kind 0)))) (hash "16xbcga44dvgdiv1d7zinm88cadvh79gk5p40i54jzxmqiwahjfn")))

(define-public crate-oats-rs-0.2 (crate (name "oats-rs") (vers "0.2.0") (deps (list (crate-dep (name "base64") (req "^0.20.0") (default-features #t) (kind 0)))) (hash "0wkcsnfhz37vxqwbhcfh42nqcpp5xbj9b3zrff3gi5k59lk0bzfg")))

(define-public crate-oats-rs-0.2 (crate (name "oats-rs") (vers "0.2.1") (deps (list (crate-dep (name "base64") (req "^0.20.0") (default-features #t) (kind 0)))) (hash "02258ycgkdarsl3r3inkxq5jslv3lb6hjfpc0yxxndpq2bnkrrcc")))

(define-public crate-oatstore-0.1 (crate (name "oatstore") (vers "0.1.0") (hash "09y8gl453ssdjxyvbca7mg7fmh6lijsa59dms42f6z0x0d28c3b5")))

(define-public crate-oatstore-0.1 (crate (name "oatstore") (vers "0.1.1") (hash "1l0idwm4gh99ffsadad4fkan2hflbbfv8k504hv6081frxbi1jiq")))

