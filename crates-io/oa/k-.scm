(define-module (crates-io oa k-) #:use-module (crates-io))

(define-public crate-oak-http-server-0.1 (crate (name "oak-http-server") (vers "0.1.0") (hash "0bm531bl0sydrr7kar68yz7x5r249jgiw2vnl2rjii6xw21md7ar")))

(define-public crate-oak-http-server-0.2 (crate (name "oak-http-server") (vers "0.2.0") (hash "19i08k6s62zbar0zq6dyjwwz8wv57a7pnjm0rg1v9c2526j2llr9")))

(define-public crate-oak-http-server-0.3 (crate (name "oak-http-server") (vers "0.3.0") (hash "0iqfdh9q5zs1j6d6yx5n0zpxdr56hj7207png00zfdakvk5xllx4")))

(define-public crate-oak-type-0.1 (crate (name "oak-type") (vers "0.1.0") (hash "03i84b1p94lxdippzj1h44jc2slakkd8g9dh7b544bghiv27ml8l") (yanked #t)))

(define-public crate-oak-type-0.1 (crate (name "oak-type") (vers "0.1.0-alpha") (hash "0g4bhag0hc27rkdx7xyl4cml677iin3s9slrnkxg33a3zih8s3f1")))

