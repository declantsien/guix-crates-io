(define-module (crates-io oa l-) #:use-module (crates-io))

(define-public crate-oal-sys-0.0.1 (crate (name "oal-sys") (vers "0.0.1") (deps (list (crate-dep (name "bindgen") (req "^0.55.1") (optional #t) (default-features #t) (kind 1)))) (hash "1dn2r8bi4slraqsx7vs3dw95kgdqf8aynjc1wybmlp6wr8haqrqv") (features (quote (("generate" "bindgen")))) (yanked #t) (links "openal")))

(define-public crate-oal-sys-0.0.2 (crate (name "oal-sys") (vers "0.0.2") (deps (list (crate-dep (name "bindgen") (req "^0.55.1") (optional #t) (default-features #t) (kind 1)))) (hash "1dzklycy31zfcbh8ilmlhk1g5qbnqh08wixsxm95y1z5xfa1lnmd") (features (quote (("generate" "bindgen")))) (links "openal")))

