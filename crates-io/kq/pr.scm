(define-module (crates-io kq pr) #:use-module (crates-io))

(define-public crate-kqpr-0.1 (crate (name "kqpr") (vers "0.1.0") (deps (list (crate-dep (name "gdk") (req "0.14.*") (default-features #t) (kind 0)) (crate-dep (name "glib") (req "0.14.*") (default-features #t) (kind 0)) (crate-dep (name "gtk") (req "0.14.*") (default-features #t) (kind 0)) (crate-dep (name "home") (req "^0.5.3") (default-features #t) (kind 0)) (crate-dep (name "keepass") (req "^0.4.9") (default-features #t) (kind 0)) (crate-dep (name "qrcode") (req "^0.12.0") (default-features #t) (kind 0)))) (hash "1g672mcagi94p583d18l2axn99632saqh90n3xh1677w28s06nq0")))

