(define-module (crates-io kq ue) #:use-module (crates-io))

(define-public crate-kqueue-0.1 (crate (name "kqueue") (vers "0.1.0") (deps (list (crate-dep (name "kqueue-sys") (req "^0.1.2") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2.17") (default-features #t) (kind 0)))) (hash "1jfsfz2d7m65c35ngqbj0c8ys2d15j1kk2dim6mn7bxzmldvr7yy")))

(define-public crate-kqueue-0.1 (crate (name "kqueue") (vers "0.1.1") (deps (list (crate-dep (name "kqueue-sys") (req "^0.1.2") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2.17") (default-features #t) (kind 0)))) (hash "0rr3nv6igrwhw0psdhqqwdmdj988cg9wab840s3hsvjy3xpv1c2x")))

(define-public crate-kqueue-0.1 (crate (name "kqueue") (vers "0.1.2") (deps (list (crate-dep (name "kqueue-sys") (req "^0.1.2") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2.17") (default-features #t) (kind 0)))) (hash "1gcnyjk9r00nn3qil3mslv3gz1mz0hzc98gfcgydqg2646am4139")))

(define-public crate-kqueue-0.1 (crate (name "kqueue") (vers "0.1.3") (deps (list (crate-dep (name "kqueue-sys") (req "^0.1.2") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2.17") (default-features #t) (kind 0)))) (hash "0m10gjf8ladq09pgf1m2ffvbj2ndkh7s1f98zbv0gh8fvcy11cca")))

(define-public crate-kqueue-0.2 (crate (name "kqueue") (vers "0.2.0") (deps (list (crate-dep (name "kqueue-sys") (req "^0.1.2") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2.17") (default-features #t) (kind 0)))) (hash "1pfmia6m0923pxpwn0x4dx3pk73sg5drnrdwdyn9zyvn5i84ahli")))

(define-public crate-kqueue-0.2 (crate (name "kqueue") (vers "0.2.1") (deps (list (crate-dep (name "kqueue-sys") (req "^0.1.3") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2.17") (default-features #t) (kind 0)))) (hash "0cwpbwi2baaicr405vvnzgcb6jk5n5nd9yfz8d441fkdxg8ix7wj")))

(define-public crate-kqueue-0.2 (crate (name "kqueue") (vers "0.2.2") (deps (list (crate-dep (name "kqueue-sys") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2.17") (default-features #t) (kind 0)))) (hash "1sfbc3b2bcqr5rwjv3yynh16qri9fg59kib2ls1czxpxlcjrxjpm")))

(define-public crate-kqueue-0.2 (crate (name "kqueue") (vers "0.2.3") (deps (list (crate-dep (name "kqueue-sys") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2.17") (default-features #t) (kind 0)))) (hash "1425gap5r738i7y4kqa4vk7w78ccq4rjv8g4vrkscqknq4dxvjcp")))

(define-public crate-kqueue-1 (crate (name "kqueue") (vers "1.0.0") (deps (list (crate-dep (name "kqueue-sys") (req "^1.0.0") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2.17") (default-features #t) (kind 0)) (crate-dep (name "tempfile") (req "^3.1.0") (default-features #t) (kind 2)))) (hash "0gv6rbqmjflwmwkqb1m0wjqbykv1w2fmx4cpycalxa5hvj31g50d")))

(define-public crate-kqueue-1 (crate (name "kqueue") (vers "1.0.1") (deps (list (crate-dep (name "kqueue-sys") (req "^1.0.1") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2.17") (default-features #t) (kind 0)) (crate-dep (name "tempfile") (req "^3.1.0") (default-features #t) (kind 2)))) (hash "1mrpjbrmm2mhfzwc4nw67q3s9j66cvbywdl6bigrrswpnq8i9yd3")))

(define-public crate-kqueue-1 (crate (name "kqueue") (vers "1.0.2") (deps (list (crate-dep (name "kqueue-sys") (req "^1.0.1") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2.17") (default-features #t) (kind 0)) (crate-dep (name "tempfile") (req "^3.1.0") (default-features #t) (kind 2)))) (hash "1g91ngs5b6a3ciw6sp3ckswbkznjnmmznn9py8adz2ij6206n3ic")))

(define-public crate-kqueue-1 (crate (name "kqueue") (vers "1.0.3") (deps (list (crate-dep (name "kqueue-sys") (req "^1.0.1") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2.17") (default-features #t) (kind 0)) (crate-dep (name "tempfile") (req "^3.1.0") (default-features #t) (kind 2)))) (hash "15lacy6vq6mikw7rdpwgqrgkay5rxafwdbbijwvlcggbrazha9si")))

(define-public crate-kqueue-1 (crate (name "kqueue") (vers "1.0.4") (deps (list (crate-dep (name "kqueue-sys") (req "^1.0.3") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2.17") (default-features #t) (kind 0)) (crate-dep (name "tempfile") (req "^3.1.0") (default-features #t) (kind 2)))) (hash "1ad4vifr0kmla8a5pgig7plx5kmc9w7k1h9mgm6fk2sgg1x112h5")))

(define-public crate-kqueue-1 (crate (name "kqueue") (vers "1.0.5") (deps (list (crate-dep (name "kqueue-sys") (req "^1.0.3") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2.17") (default-features #t) (kind 0)) (crate-dep (name "tempfile") (req "^3.1.0") (default-features #t) (kind 2)))) (hash "0ll30i6x2bp7wzgbk9rsn9zzrcgjs4n74l3lkf08cz1zp0lg9jlp")))

(define-public crate-kqueue-1 (crate (name "kqueue") (vers "1.0.6") (deps (list (crate-dep (name "kqueue-sys") (req "^1.0.3") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2.17") (default-features #t) (kind 0)) (crate-dep (name "tempfile") (req "^3.1.0") (default-features #t) (kind 2)))) (hash "13hzd65f4mg4bgy7z1f65zvvnn9s3x7x2hksqhx80nbvygl14qad")))

(define-public crate-kqueue-1 (crate (name "kqueue") (vers "1.0.7") (deps (list (crate-dep (name "kqueue-sys") (req "^1.0.3") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2.17") (default-features #t) (kind 0)) (crate-dep (name "tempfile") (req "^3.1.0") (default-features #t) (kind 2)))) (hash "165dvjbf7s0nv6q8r9zkzi1v0g81c65a81wqm9bi5xavl45wd3rc")))

(define-public crate-kqueue-1 (crate (name "kqueue") (vers "1.0.8") (deps (list (crate-dep (name "kqueue-sys") (req "^1.0.4") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2.17") (default-features #t) (kind 0)) (crate-dep (name "tempfile") (req "^3.1.0") (default-features #t) (kind 2)))) (hash "033x2knkbv8d3jy6i9r32jcgsq6zm3g97zh5la43amkv3g5g2ivl")))

(define-public crate-kqueue-sys-0.1 (crate (name "kqueue-sys") (vers "0.1.0") (deps (list (crate-dep (name "bitflags") (req "^0.7.0") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2.17") (default-features #t) (kind 0)))) (hash "1926asjn4d32frnjaah20hwf965qm9fxsgzw39dgg742m3rz8if0")))

(define-public crate-kqueue-sys-0.1 (crate (name "kqueue-sys") (vers "0.1.1") (deps (list (crate-dep (name "bitflags") (req "^0.7.0") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2.17") (default-features #t) (kind 0)))) (hash "17ix0lsvx6lky2k0apb42b0gdhqlv98mgskcgg5cgcanf7v7sq0v")))

(define-public crate-kqueue-sys-0.1 (crate (name "kqueue-sys") (vers "0.1.2") (deps (list (crate-dep (name "bitflags") (req "^0.7.0") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2.17") (default-features #t) (kind 0)))) (hash "0y3rdy71za8hln2j3lqw53xzq0xpn3fx3pz46ig3dh2wx50qlp5g")))

(define-public crate-kqueue-sys-0.1 (crate (name "kqueue-sys") (vers "0.1.3") (deps (list (crate-dep (name "bitflags") (req "^0.7.0") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2.17") (default-features #t) (kind 0)))) (hash "1bbnnqag3y0q77gqcbl2m1dgvzkz3f3fkfxz8zyfbfw55diy6ph6")))

(define-public crate-kqueue-sys-0.2 (crate (name "kqueue-sys") (vers "0.2.0") (deps (list (crate-dep (name "bitflags") (req "^0.7.0") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2.17") (default-features #t) (kind 0)))) (hash "0v2g6yrknzn2f4ngbgjdl1xd9pflwwdanvc2fwxb45q3ar4dd84m")))

(define-public crate-kqueue-sys-1 (crate (name "kqueue-sys") (vers "1.0.0") (deps (list (crate-dep (name "bitflags") (req "^1.2.1") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2.74") (default-features #t) (kind 0)))) (hash "1ms9nx3izsyagm2lpzmji0qlb35kmpn7r9cqfbqj1agiwhl00mx8")))

(define-public crate-kqueue-sys-1 (crate (name "kqueue-sys") (vers "1.0.1") (deps (list (crate-dep (name "bitflags") (req "^1.2.1") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2.74") (default-features #t) (kind 0)))) (hash "0v02n470wajsfk4pzhic4p3x0zxnv6jygwhbh6syjlivajmljil7")))

(define-public crate-kqueue-sys-1 (crate (name "kqueue-sys") (vers "1.0.2") (deps (list (crate-dep (name "bitflags") (req "^1.2.1") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2.74") (default-features #t) (kind 0)))) (hash "169dx5xdj1gnrvm8d7r45dkcknw4wbyvkzwpf9f0mhci40waw0wq")))

(define-public crate-kqueue-sys-1 (crate (name "kqueue-sys") (vers "1.0.3") (deps (list (crate-dep (name "bitflags") (req "^1.2.1") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2.74") (default-features #t) (kind 0)))) (hash "11z5labbms9vn9m6csi5383dmrlmdgsxq13ls9fwa6zhi5a5hrw3")))

(define-public crate-kqueue-sys-1 (crate (name "kqueue-sys") (vers "1.0.4") (deps (list (crate-dep (name "bitflags") (req "^1.2.1") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2.74") (default-features #t) (kind 0)))) (hash "12w3wi90y4kwis4k9g6fp0kqjdmc6l00j16g8mgbhac7vbzjb5pd")))

(define-public crate-kqueue2-0.2 (crate (name "kqueue2") (vers "0.2.2") (deps (list (crate-dep (name "kqueue2-sys") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "06z9r8nq34bmv5lbgi9ilj0np1fqqnwjqwmmzk5iwd828irc4nm2")))

(define-public crate-kqueue2-sys-0.1 (crate (name "kqueue2-sys") (vers "0.1.4") (deps (list (crate-dep (name "bitflags") (req "^0.7") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "0jh1wpc8sqr5cr6xswbmi9wqwk4mh5n6aj1dg5mi1i0rpcnm3c51")))

(define-public crate-kqueue2-sys-0.1 (crate (name "kqueue2-sys") (vers "0.1.5") (deps (list (crate-dep (name "bitflags") (req "^0.7") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "1lp7i891z7vaj5x0qzmg8cdsbxp6vi92djp1nvr0wi6jh2l4cxmh")))

