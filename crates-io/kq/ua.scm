(define-module (crates-io kq ua) #:use-module (crates-io))

(define-public crate-kquadprog-0.1 (crate (name "kquadprog") (vers "0.1.0") (deps (list (crate-dep (name "anyhow") (req "^1.0.41") (default-features #t) (kind 0)) (crate-dep (name "approx") (req "^0.5.0") (default-features #t) (kind 2)) (crate-dep (name "ndarray") (req "^0.14.0") (default-features #t) (kind 0)) (crate-dep (name "ndarray-linalg") (req "^0.13") (features (quote ("openblas"))) (default-features #t) (kind 0)))) (hash "19lcqh5v9ipnadz7bsg9hi3gx5f7920c4mqy6sffg28vsznl10pd")))

