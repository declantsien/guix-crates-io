(define-module (crates-io ux #{2-}#) #:use-module (crates-io))

(define-public crate-ux2-macros-0.1 (crate (name "ux2-macros") (vers "0.1.0") (deps (list (crate-dep (name "proc-macro2") (req "^1.0.50") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.23") (default-features #t) (kind 0)))) (hash "12x33gdngrr8nw9h1zkwnckialihk173j91xw3z7bhpa913g2xib")))

(define-public crate-ux2-macros-0.2 (crate (name "ux2-macros") (vers "0.2.0") (deps (list (crate-dep (name "proc-macro2") (req "^1.0.50") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.23") (default-features #t) (kind 0)))) (hash "16ikb7vb64ych1sj6sj96dfnljl9vrwrkz1a7wz55n33r4446wlc")))

(define-public crate-ux2-macros-0.3 (crate (name "ux2-macros") (vers "0.3.0") (deps (list (crate-dep (name "proc-macro2") (req "^1.0.50") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.23") (default-features #t) (kind 0)))) (hash "008dpif3yv78da7ybm8rz2r45298ll5s82yi7x4lk6sg5a2p5wj8")))

(define-public crate-ux2-macros-0.4 (crate (name "ux2-macros") (vers "0.4.0") (deps (list (crate-dep (name "proc-macro2") (req "^1.0.50") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.23") (default-features #t) (kind 0)))) (hash "03i9l43a0xd4pfqkm3kbzi02jm3zr1c7imw83cza5f39f8hyqgvj")))

(define-public crate-ux2-macros-0.5 (crate (name "ux2-macros") (vers "0.5.0") (deps (list (crate-dep (name "proc-macro2") (req "^1.0.50") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.23") (default-features #t) (kind 0)))) (hash "106hlxcfqyya503r7qyrp08i3y9z61m8abzy0jy5wvg6k5vni5kf")))

(define-public crate-ux2-macros-0.6 (crate (name "ux2-macros") (vers "0.6.0") (deps (list (crate-dep (name "proc-macro2") (req "^1.0.50") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.23") (default-features #t) (kind 0)))) (hash "01ix47j4kbhiwv5kc8iblj6srxnpk4rpc53skw4lb4y63adisl0b")))

(define-public crate-ux2-macros-0.7 (crate (name "ux2-macros") (vers "0.7.0") (deps (list (crate-dep (name "proc-macro2") (req "^1.0.50") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.23") (default-features #t) (kind 0)))) (hash "17a1j4rsbqy4bzjkxijfs82vg4dpbcwrbig23ia4c64i4nj38dd4")))

(define-public crate-ux2-macros-0.8 (crate (name "ux2-macros") (vers "0.8.0") (deps (list (crate-dep (name "proc-macro2") (req "^1.0.50") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.23") (default-features #t) (kind 0)))) (hash "0dl5c7lrggy0ic0c787f477ss8zwp8yna8vaharbgvj532n12fz3")))

(define-public crate-ux2-macros-0.9 (crate (name "ux2-macros") (vers "0.9.0") (deps (list (crate-dep (name "proc-macro2") (req "^1.0.50") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.23") (default-features #t) (kind 0)))) (hash "0rznx8q55528qnibc583zlxl802048r0zndv0f5c2xx4zhv24068")))

