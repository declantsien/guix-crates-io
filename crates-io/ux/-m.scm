(define-module (crates-io ux -m) #:use-module (crates-io))

(define-public crate-ux-macro-0.1 (crate (name "ux-macro") (vers "0.1.0") (deps (list (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (default-features #t) (kind 0)))) (hash "1k7vz9hlh7g73ggz0xfg8iapdn9vk7ywy5kr5v4ks0wzijbdil5c")))

(define-public crate-ux-mediapipe-0.1 (crate (name "ux-mediapipe") (vers "0.1.0") (deps (list (crate-dep (name "cgmath") (req "^0.18.0") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "opencv") (req "^0.63.0") (features (quote ("videoio" "highgui" "imgproc"))) (kind 0)) (crate-dep (name "protobuf") (req "^2.23.0") (default-features #t) (kind 0)))) (hash "0k4qz38w3vdjq0yjg1k0ms5bm65lkd88pvn64qphn8dnm3cprx6l")))

