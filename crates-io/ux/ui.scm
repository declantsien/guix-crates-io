(define-module (crates-io ux ui) #:use-module (crates-io))

(define-public crate-uxui-0.0.1 (crate (name "uxui") (vers "0.0.1-alpha") (hash "05pwri2iihdk2lhp196v76jf3gzs6jyv7v20ivv4y5s7dc89gwm2") (yanked #t)))

(define-public crate-uxui-0.0.2 (crate (name "uxui") (vers "0.0.2-alpha") (deps (list (crate-dep (name "wgpu") (req "^0.17") (default-features #t) (kind 0)) (crate-dep (name "winit") (req "^0.28") (default-features #t) (kind 0)))) (hash "12ldwqsldwgiaxnsnxdhyn7qnmzyfpczmhqfjb04kmb28j62xj3q") (yanked #t)))

(define-public crate-uxui-0.0.3 (crate (name "uxui") (vers "0.0.3-alpha") (deps (list (crate-dep (name "wgpu") (req "^0.17") (default-features #t) (kind 0)) (crate-dep (name "winit") (req "^0.28") (default-features #t) (kind 0)))) (hash "1m6swlr7bbqsrzcgi2im9wigk49jp5r48vs7g32yph3v2l1ifa0d") (yanked #t)))

(define-public crate-uxui-0.1 (crate (name "uxui") (vers "0.1.0-alpha.0") (deps (list (crate-dep (name "freetype-rs") (req "^0.34.0") (default-features #t) (kind 0)) (crate-dep (name "futures") (req "^0.3.29") (default-features #t) (kind 0)) (crate-dep (name "glm") (req "^0.2.3") (default-features #t) (kind 0)) (crate-dep (name "image") (req "^0.24.7") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2.17") (default-features #t) (kind 0)) (crate-dep (name "wgpu") (req "^0.18.0") (features (quote ("spirv"))) (kind 0)) (crate-dep (name "winit") (req "^0.29.4") (features (quote ("rwh_05"))) (kind 0)))) (hash "1vss8kqf6p7dmgp2bj6fbypllyjjmmhym7apff8rw1jd7msdvx3r")))

