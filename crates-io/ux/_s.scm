(define-module (crates-io ux _s) #:use-module (crates-io))

(define-public crate-ux_serde-0.2 (crate (name "ux_serde") (vers "0.2.0") (deps (list (crate-dep (name "serde") (req "^1") (optional #t) (default-features #t) (kind 0)))) (hash "1fb4khq7x5fxcqj4g3vc7aqlmp43zq5n4v2f2rqnc4h5xvbg1anq") (features (quote (("std") ("default" "std"))))))

