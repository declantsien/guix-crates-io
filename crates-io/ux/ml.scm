(define-module (crates-io ux ml) #:use-module (crates-io))

(define-public crate-uxmlwriter-0.1 (crate (name "uxmlwriter") (vers "0.1.0") (hash "1sa27hldyssf8hqkrk3ha4d9z7cr7msjaaix2fvs56p29wvnyrr2")))

(define-public crate-uxmlwriter-0.2 (crate (name "uxmlwriter") (vers "0.2.0") (hash "0l608dxk7nrf7p90g6diqw4f74cz56ld98vgf72qfy87ga58hp34")))

(define-public crate-uxmlwriter-1 (crate (name "uxmlwriter") (vers "1.0.0") (hash "0fzphmi84z3mj2pfbmwf5q3ibfl9mpxj1ycscmjj9b4y289n3znh")))

