(define-module (crates-io mp #{2c}#) #:use-module (crates-io))

(define-public crate-mp2c-0.1 (crate (name "mp2c") (vers "0.1.0") (hash "06bzb0hz8kgqjja728567bqs8i4yj19l265r8j5g72kc0a35n1bp")))

(define-public crate-mp2c-0.1 (crate (name "mp2c") (vers "0.1.1") (hash "1ck7v38aznl4djf13m4b715hj9lrf9gdcbh6vb82xg9yp9pnbswm")))

(define-public crate-mp2c-0.1 (crate (name "mp2c") (vers "0.1.2") (hash "064485yaf97ys97fhn79a1wb9f62vckczzq9jvgqga6x68clgpw8")))

