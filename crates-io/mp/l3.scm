(define-module (crates-io mp l3) #:use-module (crates-io))

(define-public crate-mpl3115-0.1 (crate (name "mpl3115") (vers "0.1.0") (deps (list (crate-dep (name "cast") (req "^0.2") (kind 0)) (crate-dep (name "embedded-hal") (req "^0.2") (default-features #t) (kind 0)))) (hash "18vblv4kzc9pm3d84rqy2vpk40748f7vjncidgljkkrnkpg77iy6")))

