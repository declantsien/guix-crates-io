(define-module (crates-io mp ch) #:use-module (crates-io))

(define-public crate-mpchash-0.1 (crate (name "mpchash") (vers "0.1.0") (deps (list (crate-dep (name "num-traits") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "xxhash-rust") (req "^0.8.6") (features (quote ("xxh3" "const_xxh3"))) (default-features #t) (kind 0)))) (hash "10r1icj5wgc9hjm8s6f1bzpqflqhv74ijpz396wi2gaapyyvcmf9")))

(define-public crate-mpchash-1 (crate (name "mpchash") (vers "1.1.0") (deps (list (crate-dep (name "num-traits") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "xxhash-rust") (req "^0.8.6") (features (quote ("xxh3" "const_xxh3"))) (default-features #t) (kind 0)))) (hash "1ps8534ryzw5kgpqn76alzjnz18sifahcibl0jrfbsbdscgb55cp")))

(define-public crate-mpchash-1 (crate (name "mpchash") (vers "1.2.0") (deps (list (crate-dep (name "num-traits") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "xxhash-rust") (req "^0.8.6") (features (quote ("xxh3" "const_xxh3"))) (default-features #t) (kind 0)))) (hash "1axx08k7hly431hkz28m5rbj7nb9mcg1qxhzld1bv4qz73c3anmr")))

(define-public crate-mpchash-1 (crate (name "mpchash") (vers "1.2.1") (deps (list (crate-dep (name "num-traits") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "xxhash-rust") (req "^0.8.6") (features (quote ("xxh3" "const_xxh3"))) (default-features #t) (kind 0)))) (hash "06dcsc6b68nmll8k50a06idmmq9my8f10qwgaz9q59i2b5lfxi6f")))

(define-public crate-mpchash-1 (crate (name "mpchash") (vers "1.2.2") (deps (list (crate-dep (name "num-traits") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "xxhash-rust") (req "^0.8.6") (features (quote ("xxh3" "const_xxh3"))) (default-features #t) (kind 0)))) (hash "0vx926wyrsp1m495261bxpz1dgq4hcib8grq11vrh4w8q58x28qf")))

(define-public crate-mpchash-1 (crate (name "mpchash") (vers "1.2.3") (deps (list (crate-dep (name "num-traits") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "xxhash-rust") (req "^0.8.6") (features (quote ("xxh3" "const_xxh3"))) (default-features #t) (kind 0)))) (hash "1abiv0shhk6798k1wkqlbgvs65j2r8zkiswa5wi1hlv4magikn5x")))

