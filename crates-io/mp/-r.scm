(define-module (crates-io mp -r) #:use-module (crates-io))

(define-public crate-mp-rs-0.1 (crate (name "mp-rs") (vers "0.1.0") (deps (list (crate-dep (name "gmp-mpfr-sys") (req "^1.4.10") (default-features #t) (kind 0)))) (hash "1azk0g6382zhfqdq7s8k11i7dagm1azgwwp94f72l806v2mwd08v") (yanked #t)))

(define-public crate-mp-rs-0.1 (crate (name "mp-rs") (vers "0.1.1") (deps (list (crate-dep (name "gmp-mpfr-sys") (req "^1.4.10") (default-features #t) (kind 0)))) (hash "14szzjijhdzm6zsignwng4zi5ssyi8wyvd6r2mv3fp4wvm8sw961") (yanked #t)))

(define-public crate-mp-rs-0.1 (crate (name "mp-rs") (vers "0.1.2") (deps (list (crate-dep (name "gmp-mpfr-sys") (req "^1.4.10") (default-features #t) (kind 0)))) (hash "15vwr0a1qhzvgpyiy4ha5p55ipcc53z1ryl0cgs6jm1hva7sdy0c") (yanked #t)))

