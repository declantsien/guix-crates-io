(define-module (crates-io mp fr) #:use-module (crates-io))

(define-public crate-mpfr-0.0.1 (crate (name "mpfr") (vers "0.0.1") (deps (list (crate-dep (name "bitflags") (req "*") (default-features #t) (kind 0)) (crate-dep (name "mpfr-sys") (req "*") (default-features #t) (kind 0)))) (hash "138bhmv71dzbg259b5xhfjf9j82bpdjnkd6w4avjbpjh3v0y0vra")))

(define-public crate-mpfr-0.0.2 (crate (name "mpfr") (vers "0.0.2") (deps (list (crate-dep (name "bitflags") (req "*") (default-features #t) (kind 0)) (crate-dep (name "mpfr-sys") (req "*") (default-features #t) (kind 0)))) (hash "0xdjpsw7qr8zj8n8rfkkrmbkvixvhpgpz15vd94m93admpn38991")))

(define-public crate-mpfr-0.0.3 (crate (name "mpfr") (vers "0.0.3") (deps (list (crate-dep (name "bitflags") (req "*") (default-features #t) (kind 0)) (crate-dep (name "mpfr-sys") (req "*") (default-features #t) (kind 0)))) (hash "15iwgqrk6m8knq7ydgwfjxs1nbx3wywjjiii5wsa5lv5dx3pq40z")))

(define-public crate-mpfr-0.0.4 (crate (name "mpfr") (vers "0.0.4") (deps (list (crate-dep (name "bitflags") (req "*") (default-features #t) (kind 0)) (crate-dep (name "mpfr-sys") (req "*") (default-features #t) (kind 0)))) (hash "0synd9gzzm7m56ghgd3dznkwiwj1yj8m354ggdjzbl49yamag3hc")))

(define-public crate-mpfr-0.0.5 (crate (name "mpfr") (vers "0.0.5") (deps (list (crate-dep (name "bitflags") (req "*") (default-features #t) (kind 0)) (crate-dep (name "mpfr-sys") (req "*") (default-features #t) (kind 0)) (crate-dep (name "num") (req "*") (default-features #t) (kind 0)))) (hash "16wrqryddkmnpxsbgrzcacsfn896sa57yg9acfbrz52w19gqps19")))

(define-public crate-mpfr-0.0.6 (crate (name "mpfr") (vers "0.0.6") (deps (list (crate-dep (name "bitflags") (req "*") (default-features #t) (kind 0)) (crate-dep (name "mpfr-sys") (req "*") (default-features #t) (kind 0)) (crate-dep (name "num") (req "*") (default-features #t) (kind 0)))) (hash "16nhprx60m6hp5pqmm3qwk5rx8aqbrm688gcbnbbdmhkc3mh9kay")))

(define-public crate-mpfr-0.0.7 (crate (name "mpfr") (vers "0.0.7") (deps (list (crate-dep (name "bitflags") (req "*") (default-features #t) (kind 0)) (crate-dep (name "mpfr-sys") (req "*") (default-features #t) (kind 0)) (crate-dep (name "num") (req "*") (default-features #t) (kind 0)))) (hash "0qbachj1grndh2hp7bsm5fq28ggh00qpjj005zzayvcm2biv25lj")))

(define-public crate-mpfr-0.0.8 (crate (name "mpfr") (vers "0.0.8") (deps (list (crate-dep (name "bitflags") (req "*") (default-features #t) (kind 0)) (crate-dep (name "mpfr-sys") (req "*") (default-features #t) (kind 0)) (crate-dep (name "num") (req "*") (default-features #t) (kind 0)))) (hash "1m28fnxlvakn3grwxp676fmh1acl32hn4ckbva321m7wlfj6imxa")))

(define-public crate-mpfr-sys-0.0.1 (crate (name "mpfr-sys") (vers "0.0.1") (deps (list (crate-dep (name "gmp-sys") (req "*") (default-features #t) (kind 0)))) (hash "0fxj72hxg5pnbjp37avjaimgk009l016q6yykgbzssgcr458f8p3")))

(define-public crate-mpfr-sys-0.0.2 (crate (name "mpfr-sys") (vers "0.0.2") (deps (list (crate-dep (name "gmp-sys") (req "*") (default-features #t) (kind 0)))) (hash "14pppvnb9nw5il7smv28224zy31mm2bscm6m1m2d5wa933krghmi")))

(define-public crate-mpfr-sys-0.0.3 (crate (name "mpfr-sys") (vers "0.0.3") (deps (list (crate-dep (name "gmp-sys") (req "*") (default-features #t) (kind 0)))) (hash "1jw04ml5arfn5k6wqhv978940bvzbw3rlav1z40dhbn9p2jylrs9")))

(define-public crate-mpfr-sys-0.0.4 (crate (name "mpfr-sys") (vers "0.0.4") (deps (list (crate-dep (name "gmp-sys") (req "*") (default-features #t) (kind 0)))) (hash "12nzxhcypxwn3bih2knilp2pf97ksza1kv6gnvp44wzzj4sjnqxm")))

(define-public crate-mpfr-sys-0.0.5 (crate (name "mpfr-sys") (vers "0.0.5") (deps (list (crate-dep (name "gmp-sys") (req "*") (default-features #t) (kind 0)))) (hash "1dm0a1qj0n0qdgqq81sfvgly70w5ha18a0scjdfvk4hq4y4pdv4w")))

(define-public crate-mpfr-sys-0.0.6 (crate (name "mpfr-sys") (vers "0.0.6") (deps (list (crate-dep (name "gmp-sys") (req "*") (default-features #t) (kind 0)))) (hash "19bdxbm90fibpyn3cikzg7q591a0dqh3r68h794kyjhh54573qn5")))

(define-public crate-mpfr-sys-0.0.7 (crate (name "mpfr-sys") (vers "0.0.7") (deps (list (crate-dep (name "gmp-sys") (req "*") (default-features #t) (kind 0)))) (hash "1n70ga4bw98irahawfr8sznyx8wz2bxy1xxms5fgaq2zjpy57k6q")))

(define-public crate-mpfr-sys-0.0.8 (crate (name "mpfr-sys") (vers "0.0.8") (deps (list (crate-dep (name "gmp-sys") (req "*") (default-features #t) (kind 0)))) (hash "0zsj9p23y9ixqj42m272m4ncx38dy4m7i4d153rp6a0qiy9pli1r")))

