(define-module (crates-io mp dr) #:use-module (crates-io))

(define-public crate-mpdrs-0.1 (crate (name "mpdrs") (vers "0.1.0") (deps (list (crate-dep (name "bufstream") (req "^0.1") (kind 0)) (crate-dep (name "tempdir") (req "^0.3.5") (default-features #t) (kind 2)) (crate-dep (name "unix_socket") (req "^0.5.0") (default-features #t) (kind 2)))) (hash "11i60g95n1w37l7hj8j39a986gm3fppbqb65fyph52wnx1z1pj6y")))

