(define-module (crates-io mp u6) #:use-module (crates-io))

(define-public crate-mpu6000-0.0.1 (crate (name "mpu6000") (vers "0.0.1") (deps (list (crate-dep (name "embedded-hal") (req "^0.2.3") (default-features #t) (kind 0)) (crate-dep (name "nalgebra") (req "^0.21") (kind 0)))) (hash "108lnmk6vpvypajyk4dxi8k8ciz02q8hzw17mvcfhbmyzxqs77g4")))

(define-public crate-mpu6000-0.0.2 (crate (name "mpu6000") (vers "0.0.2") (deps (list (crate-dep (name "embedded-hal") (req "^0.2.3") (default-features #t) (kind 0)) (crate-dep (name "nalgebra") (req "^0.21") (kind 0)))) (hash "0x4iqwrnnisdg9bbkm4aw7m6b1a2crkr2vp0m0gk0mbvci78y395")))

(define-public crate-mpu6000-0.0.3 (crate (name "mpu6000") (vers "0.0.3") (deps (list (crate-dep (name "nalgebra") (req "^0.21") (kind 0)))) (hash "0a7pmwvby8g5cg86vvfkp1vmmai7f8qs66izs7y1lp242xzjrgzi")))

(define-public crate-mpu6000-0.0.4 (crate (name "mpu6000") (vers "0.0.4") (deps (list (crate-dep (name "nalgebra") (req "^0.21") (kind 0)))) (hash "0cjspkyybsi07snrsh663i5y8dmx267wdrh8zjqky0q9zr93licn")))

(define-public crate-mpu6000-0.0.5 (crate (name "mpu6000") (vers "0.0.5") (deps (list (crate-dep (name "embedded-hal") (req "^0.2.3") (default-features #t) (kind 0)) (crate-dep (name "nalgebra") (req "^0.21") (kind 0)))) (hash "1wxnvklvf1n05m7n2zyx5x9l4kc5gidsz0q1xrbk7904j69djz58")))

(define-public crate-mpu6000-0.1 (crate (name "mpu6000") (vers "0.1.0") (deps (list (crate-dep (name "embedded-hal") (req "^0.2.3") (default-features #t) (kind 0)) (crate-dep (name "nalgebra") (req "^0.21") (kind 0)))) (hash "1bbgsfp23yxiy2dhasml5hwnfc9wqgzzv6g2scighh3i323kzgas")))

(define-public crate-mpu6000-0.1 (crate (name "mpu6000") (vers "0.1.2") (deps (list (crate-dep (name "embedded-hal") (req "^0.2.3") (default-features #t) (kind 0)) (crate-dep (name "nalgebra") (req "^0.21") (kind 0)))) (hash "090jinblx2pf8m85ix39nwf5691an9mmjbyqz58ms8jn7d7h0q97")))

(define-public crate-mpu6000-0.1 (crate (name "mpu6000") (vers "0.1.3") (deps (list (crate-dep (name "embedded-hal") (req "^0.2.3") (default-features #t) (kind 0)) (crate-dep (name "nalgebra") (req "^0.21") (kind 0)))) (hash "19g7gcd0xdz9s77lc5j53ihrai69w3nnpjywm36smacaxnfqm6n9")))

(define-public crate-mpu6000-0.1 (crate (name "mpu6000") (vers "0.1.4") (deps (list (crate-dep (name "embedded-hal") (req "^0.2.3") (default-features #t) (kind 0)) (crate-dep (name "nalgebra") (req "^0.21") (kind 0)))) (hash "13p5dd6m55i3fn0aggflwn577kc0zcbhvsr9rlh8b279bc0ngk1c")))

(define-public crate-mpu6000-0.1 (crate (name "mpu6000") (vers "0.1.5") (deps (list (crate-dep (name "embedded-hal") (req "^0.2.3") (default-features #t) (kind 0)) (crate-dep (name "nalgebra") (req "^0.21") (kind 0)))) (hash "1905m6kg1ls2flfvd4a9sh6fmym02mrwn5abmj4bywm3invz2nps")))

(define-public crate-mpu6000-0.1 (crate (name "mpu6000") (vers "0.1.6") (deps (list (crate-dep (name "embedded-hal") (req "^0.2.3") (default-features #t) (kind 0)) (crate-dep (name "nalgebra") (req "^0.21") (kind 0)))) (hash "1b8pkvq7yb55wzkm81hx7hnf3i2s0vw3samc7qs7qbv3kzj71ps6") (yanked #t)))

(define-public crate-mpu6000-0.1 (crate (name "mpu6000") (vers "0.1.7") (deps (list (crate-dep (name "embedded-hal") (req "^0.2.3") (default-features #t) (kind 0)) (crate-dep (name "nalgebra") (req "^0.21") (kind 0)))) (hash "1ywf558cx1r4y0h98g9rcljfk00kgfxzqgfc1j895cn2d3lwnjxq")))

(define-public crate-mpu6000-0.1 (crate (name "mpu6000") (vers "0.1.8") (deps (list (crate-dep (name "embedded-hal") (req "^0.2.3") (default-features #t) (kind 0)) (crate-dep (name "nalgebra") (req "^0.21") (kind 0)))) (hash "1275hj5z1862l2xv3kkcjxcr5m6xzz029dqwax30whaavw90zhmw")))

(define-public crate-mpu6000-0.1 (crate (name "mpu6000") (vers "0.1.9") (deps (list (crate-dep (name "embedded-hal") (req "^0.2") (default-features #t) (kind 0)))) (hash "03zza73gki4fmfa5qlhbsx6xjibs19mpqhk1pi2wm4gk6ggk7y35")))

(define-public crate-mpu6000-0.2 (crate (name "mpu6000") (vers "0.2.0") (deps (list (crate-dep (name "embedded-hal") (req "^0.2.4") (default-features #t) (kind 0)))) (hash "1y7chq9bz58rq7qhnfj8nvh3bmdpswh8vi34mm8spkx7lbyz9l5c")))

(define-public crate-mpu6000-0.2 (crate (name "mpu6000") (vers "0.2.1") (deps (list (crate-dep (name "embedded-hal") (req "^0.2.4") (default-features #t) (kind 0)))) (hash "00jjikrky6j03szfxlbvvcpbbzp6vl6bjrna4c5rpv0vv15q4b5w")))

(define-public crate-mpu6000-0.3 (crate (name "mpu6000") (vers "0.3.0") (deps (list (crate-dep (name "embedded-hal") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "fixed-point") (req "^1.0") (kind 0)))) (hash "1c3m89p83gcnq46alpi9vj6dnhzdzac6q3jrgbnrsgj9ynvd31w7")))

(define-public crate-mpu6050-0.1 (crate (name "mpu6050") (vers "0.1.0") (deps (list (crate-dep (name "embedded-hal") (req "^0.2.2") (default-features #t) (kind 0)) (crate-dep (name "i2cdev") (req "^0.4.1") (default-features #t) (kind 0)) (crate-dep (name "libm") (req "^0.1.2") (default-features #t) (kind 0)) (crate-dep (name "linux-embedded-hal") (req "^0.2.2") (default-features #t) (kind 0)))) (hash "070q3gihi7xfs1h7bkfa9f77yjf5rh418bf9iksxn9ar8nhzkj6s")))

(define-public crate-mpu6050-0.1 (crate (name "mpu6050") (vers "0.1.1") (deps (list (crate-dep (name "embedded-hal") (req "^0.2.2") (default-features #t) (kind 0)) (crate-dep (name "libm") (req "^0.1.2") (default-features #t) (kind 0)))) (hash "17dyqsp108l61b4xbs54g0gxzszd5767mkfrvilv370clnqpcp0g")))

(define-public crate-mpu6050-0.1 (crate (name "mpu6050") (vers "0.1.2") (deps (list (crate-dep (name "embedded-hal") (req "^0.2.2") (default-features #t) (kind 0)) (crate-dep (name "libm") (req "^0.1.2") (default-features #t) (kind 0)))) (hash "1jcd1q8hqndfc94l8g1m2yxsrivk6y84zppm62r2clxvm5yz2956")))

(define-public crate-mpu6050-0.1 (crate (name "mpu6050") (vers "0.1.3") (deps (list (crate-dep (name "embedded-hal") (req "^0.2.2") (default-features #t) (kind 0)) (crate-dep (name "i2cdev") (req "^0.4.2") (default-features #t) (kind 2)) (crate-dep (name "libm") (req "^0.1.3") (default-features #t) (kind 0)) (crate-dep (name "linux-embedded-hal") (req "^0.2.2") (default-features #t) (kind 2)) (crate-dep (name "nalgebra") (req "^0.18.0") (kind 0)))) (hash "0h8zafhm29lzwb2a6vvd08w4bk7qw7a128akcmpccp6g4mjzynzh")))

(define-public crate-mpu6050-0.1 (crate (name "mpu6050") (vers "0.1.4") (deps (list (crate-dep (name "embedded-hal") (req "^0.2.4") (default-features #t) (kind 0)) (crate-dep (name "i2cdev") (req "^0.4.4") (default-features #t) (kind 2)) (crate-dep (name "libm") (req "^0.2.1") (default-features #t) (kind 0)) (crate-dep (name "linux-embedded-hal") (req "^0.3.0") (default-features #t) (kind 2)) (crate-dep (name "nalgebra") (req "^0.24.1") (kind 0)))) (hash "0f5y8j949jqxk6433abbarcyv99hazhsx7l5z5ds4pd9gfijfx27")))

(define-public crate-mpu6050-0.1 (crate (name "mpu6050") (vers "0.1.5") (deps (list (crate-dep (name "embedded-hal") (req "^0.2.4") (default-features #t) (kind 0)) (crate-dep (name "i2cdev") (req "^0.4.4") (default-features #t) (kind 2)) (crate-dep (name "libm") (req "^0.2.1") (default-features #t) (kind 0)) (crate-dep (name "linux-embedded-hal") (req "^0.3.0") (default-features #t) (kind 2)) (crate-dep (name "nalgebra") (req "^0.24.1") (kind 0)))) (hash "1p4920ws0lpag6h4vc6nagk48shrjimv08wfxp0m0qzryvzcw9fh")))

(define-public crate-mpu6050-0.1 (crate (name "mpu6050") (vers "0.1.6") (deps (list (crate-dep (name "embedded-hal") (req "^0.2.4") (default-features #t) (kind 0)) (crate-dep (name "i2cdev") (req "^0.5.1") (default-features #t) (kind 2)) (crate-dep (name "libm") (req "^0.2.1") (default-features #t) (kind 0)) (crate-dep (name "linux-embedded-hal") (req "^0.3.0") (default-features #t) (kind 2)) (crate-dep (name "nalgebra") (req "^0.31.2") (kind 0)))) (hash "1wmrwgv66ikjw87mqhwfb7x6bh4m0zaqkp8lma1cmg24n997immb")))

(define-public crate-mpu6050-dmp-0.2 (crate (name "mpu6050-dmp") (vers "0.2.0") (deps (list (crate-dep (name "embedded-hal") (req "^0.2.7") (features (quote ("unproven"))) (default-features #t) (kind 0)) (crate-dep (name "libm") (req "^0.2.1") (default-features #t) (kind 0)))) (hash "09a798xrbhfd5hmx2qbwm5qlf9prc5i4zrlns25qpzvighpw79bx")))

(define-public crate-mpu6050-dmp-0.3 (crate (name "mpu6050-dmp") (vers "0.3.0") (deps (list (crate-dep (name "embedded-hal") (req "^0.2.7") (features (quote ("unproven"))) (default-features #t) (kind 0)) (crate-dep (name "libm") (req "^0.2.1") (default-features #t) (kind 0)))) (hash "1r2i8n20pv9da7am2fybi5g7qsirhfk8jw5v1a2pwzpi8aqrvbbq")))

(define-public crate-mpu6050-dmp-0.4 (crate (name "mpu6050-dmp") (vers "0.4.0") (deps (list (crate-dep (name "embedded-hal") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "libm") (req "^0.2") (default-features #t) (kind 0)))) (hash "1cwyrfchkwr20c2lnjbimvqn2p8wi4a9fl3fi2kl4ih8rv2ph1rq")))

(define-public crate-mpu6886-0.1 (crate (name "mpu6886") (vers "0.1.0") (deps (list (crate-dep (name "embedded-hal") (req "^0.2.4") (default-features #t) (kind 0)) (crate-dep (name "esp-println") (req "^0.3.1") (features (quote ("esp32"))) (default-features #t) (kind 0)) (crate-dep (name "libm") (req "^0.2.1") (default-features #t) (kind 0)) (crate-dep (name "nalgebra") (req "^0.31.2") (kind 0)) (crate-dep (name "i2cdev") (req "^0.5.1") (default-features #t) (kind 2)) (crate-dep (name "linux-embedded-hal") (req "^0.3.2") (default-features #t) (kind 2)))) (hash "0l6a6d0wdqimby399n8vm0cv9mrdglm2wn5rzv6p5dphaw17ik1w")))

