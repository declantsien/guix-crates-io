(define-module (crates-io mp #{3l}#) #:use-module (crates-io))

(define-public crate-mp3lame-encoder-0.1 (crate (name "mp3lame-encoder") (vers "0.1.0") (deps (list (crate-dep (name "libc") (req "^0.2") (kind 0)) (crate-dep (name "mp3lame-sys") (req "^0.1.2") (kind 0)) (crate-dep (name "symphonia") (req "^0.5.1") (features (quote ("ogg" "vorbis"))) (kind 2)))) (hash "0b97q20m1dhfz7cg2i9m1m05l6298gial4hyrgxricawhh7wxx52") (features (quote (("std")))) (yanked #t)))

(define-public crate-mp3lame-encoder-0.1 (crate (name "mp3lame-encoder") (vers "0.1.1") (deps (list (crate-dep (name "libc") (req "^0.2") (kind 0)) (crate-dep (name "mp3lame-sys") (req "^0.1.4") (kind 0)) (crate-dep (name "symphonia") (req "^0.5.2") (features (quote ("ogg" "vorbis"))) (kind 2)))) (hash "02qg47bsagn53k69hbnbk8cczk7p3xl30kgpizcwimxy07y9q4wy") (features (quote (("std"))))))

(define-public crate-mp3lame-encoder-0.1 (crate (name "mp3lame-encoder") (vers "0.1.2") (deps (list (crate-dep (name "libc") (req "^0.2") (kind 0)) (crate-dep (name "mp3lame-sys") (req "^0.1.4") (kind 0)) (crate-dep (name "symphonia") (req "^0.5.2") (features (quote ("ogg" "vorbis"))) (kind 2)))) (hash "0v1da3dk19n46dhazqq4ri9mphbfl6wljw2w4lr7jl28r8rj6bc1") (features (quote (("std"))))))

(define-public crate-mp3lame-encoder-0.1 (crate (name "mp3lame-encoder") (vers "0.1.3") (deps (list (crate-dep (name "libc") (req "^0.2") (kind 0)) (crate-dep (name "mp3lame-sys") (req "^0.1.4") (kind 0)) (crate-dep (name "symphonia") (req "^0.5.2") (features (quote ("ogg" "vorbis"))) (kind 2)))) (hash "01zv389qlwv6b1hs5nd1bpqvhralm1k4zlvm7z1m67f24m6lma61") (features (quote (("std"))))))

(define-public crate-mp3lame-encoder-0.1 (crate (name "mp3lame-encoder") (vers "0.1.4") (deps (list (crate-dep (name "libc") (req "^0.2") (kind 0)) (crate-dep (name "mp3lame-sys") (req "^0.1.4") (kind 0)) (crate-dep (name "symphonia") (req "^0.5.2") (features (quote ("ogg" "vorbis"))) (kind 2)))) (hash "04qc8fawjyvn5p8h8mdfn6ngvfz8gsprqm7n9npyssj2n11ckrna") (features (quote (("std"))))))

(define-public crate-mp3lame-encoder-0.1 (crate (name "mp3lame-encoder") (vers "0.1.5") (deps (list (crate-dep (name "libc") (req "^0.2") (kind 0)) (crate-dep (name "mp3lame-sys") (req "^0.1.4") (kind 0)) (crate-dep (name "symphonia") (req "^0.5.2") (features (quote ("ogg" "vorbis"))) (kind 2)))) (hash "1iw9jsa9204i5waz9whmznh1yc5hv0xzb9lpxqpjmvprw9j2wqk4") (features (quote (("std"))))))

(define-public crate-mp3lame-sys-0.1 (crate (name "mp3lame-sys") (vers "0.1.0") (deps (list (crate-dep (name "autotools") (req "^0.2.5") (default-features #t) (target "cfg(unix)") (kind 1)) (crate-dep (name "cc") (req "^1.0.77") (default-features #t) (target "cfg(windows)") (kind 1)) (crate-dep (name "libc") (req "^0.2") (kind 0)))) (hash "1azkqdzz159bin6l7hsxr84rfhn67a0b9ldvmkgv6rfps0lxb52a") (features (quote (("decoder")))) (yanked #t)))

(define-public crate-mp3lame-sys-0.1 (crate (name "mp3lame-sys") (vers "0.1.1") (deps (list (crate-dep (name "autotools") (req "^0.2.5") (default-features #t) (target "cfg(unix)") (kind 1)) (crate-dep (name "cc") (req "^1.0.77") (default-features #t) (target "cfg(windows)") (kind 1)) (crate-dep (name "libc") (req "^0.2") (kind 0)))) (hash "16n0wzq6zb77c1qx86h4n1kn0lh19nxb0wl19hl5j4xfxs2kmvc0") (features (quote (("decoder"))))))

(define-public crate-mp3lame-sys-0.1 (crate (name "mp3lame-sys") (vers "0.1.2") (deps (list (crate-dep (name "autotools") (req "^0.2.5") (default-features #t) (target "cfg(unix)") (kind 1)) (crate-dep (name "cc") (req "^1.0.77") (default-features #t) (target "cfg(windows)") (kind 1)) (crate-dep (name "libc") (req "^0.2") (kind 0)))) (hash "03llsqg303rf43gqiqjflijxd0z7fwnj30zgq8g1i8k8nrkh2nn7") (features (quote (("decoder"))))))

(define-public crate-mp3lame-sys-0.1 (crate (name "mp3lame-sys") (vers "0.1.3") (deps (list (crate-dep (name "libc") (req "^0.2") (kind 0)) (crate-dep (name "autotools") (req "^0.2.6") (default-features #t) (target "cfg(unix)") (kind 1)) (crate-dep (name "cc") (req "^1.0.77") (default-features #t) (target "cfg(windows)") (kind 1)))) (hash "1r884hnjb8cag3rm99xis8vhmvwwlwnbnqf3225dxq3zqdp87zyn") (features (quote (("decoder")))) (yanked #t)))

(define-public crate-mp3lame-sys-0.1 (crate (name "mp3lame-sys") (vers "0.1.4") (deps (list (crate-dep (name "libc") (req "^0.2") (kind 0)) (crate-dep (name "autotools") (req "^0.2.6") (default-features #t) (target "cfg(unix)") (kind 1)) (crate-dep (name "cc") (req "^1.0.77") (default-features #t) (target "cfg(windows)") (kind 1)))) (hash "01ki39nspdzan8h6kk1mi0shyxwfglg4lpsc2ry9b3nm7va6z2cr") (features (quote (("decoder"))))))

