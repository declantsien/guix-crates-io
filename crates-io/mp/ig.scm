(define-module (crates-io mp ig) #:use-module (crates-io))

(define-public crate-mpigdb-0.1 (crate (name "mpigdb") (vers "0.1.0") (deps (list (crate-dep (name "anyhow") (req "^1.0.66") (default-features #t) (kind 0)) (crate-dep (name "num_cpus") (req "^1.14.0") (default-features #t) (kind 0)))) (hash "00n2mdr27wnzsrn48p9z0k6zv1bk8irq85vzhfji2xspizrc25jg")))

(define-public crate-mpigdb-0.2 (crate (name "mpigdb") (vers "0.2.0") (deps (list (crate-dep (name "anyhow") (req "^1.0.66") (default-features #t) (kind 0)) (crate-dep (name "hostname") (req "^0.3.1") (default-features #t) (kind 0)) (crate-dep (name "num_cpus") (req "^1.14.0") (default-features #t) (kind 0)))) (hash "14ppk10a0gj7ri4r7cm56g9hgqwxacd0756kl9qvws80mbkcvn78")))

(define-public crate-mpigdb-0.4 (crate (name "mpigdb") (vers "0.4.0") (deps (list (crate-dep (name "anyhow") (req "^1.0.66") (default-features #t) (kind 0)) (crate-dep (name "hostname") (req "^0.3.1") (default-features #t) (kind 0)) (crate-dep (name "num_cpus") (req "^1.14.0") (default-features #t) (kind 0)))) (hash "1zgj34x3qbsr5d7gzd0r3wvgracr67fdpdjqv1d12nmi4hdxpqk2")))

(define-public crate-mpigdb-0.4 (crate (name "mpigdb") (vers "0.4.1") (deps (list (crate-dep (name "anyhow") (req "^1.0.66") (default-features #t) (kind 0)) (crate-dep (name "hostname") (req "^0.3.1") (default-features #t) (kind 0)) (crate-dep (name "num_cpus") (req "^1.14.0") (default-features #t) (kind 0)))) (hash "1hg3kavngkknb3k7g43m7glgjydz7z2blqkad35lxs7i30jchy9d")))

(define-public crate-mpigdb-0.5 (crate (name "mpigdb") (vers "0.5.0") (deps (list (crate-dep (name "anyhow") (req "^1.0.66") (default-features #t) (kind 0)) (crate-dep (name "hostname") (req "^0.3.1") (default-features #t) (kind 0)) (crate-dep (name "num_cpus") (req "^1.14.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.162") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.96") (default-features #t) (kind 0)))) (hash "00kwa840q1g7f836bycv0fqdvczw8g2zxrxmbcfmhdg2fcqf5v70")))

