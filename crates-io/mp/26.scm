(define-module (crates-io mp #{26}#) #:use-module (crates-io))

(define-public crate-mp2667-0.0.0 (crate (name "mp2667") (vers "0.0.0") (deps (list (crate-dep (name "embedded-hal") (req "^0.2.5") (features (quote ("unproven"))) (default-features #t) (kind 0)) (crate-dep (name "modular-bitfield") (req "^0.11.2") (default-features #t) (kind 0)))) (hash "17q9w2qvb6f468v2hxffci78b2n00xc6clldp2j79dzh5vvx8l8p")))

(define-public crate-mp2667-0.0.1 (crate (name "mp2667") (vers "0.0.1") (deps (list (crate-dep (name "embedded-hal") (req "^0.2.5") (features (quote ("unproven"))) (default-features #t) (kind 0)) (crate-dep (name "modular-bitfield") (req "^0.11.2") (default-features #t) (kind 0)))) (hash "1m4gvhvawziaivhwjvrlbixssv1nmj7fqqfnhapgqylf8r3mhdi3")))

(define-public crate-mp2667-0.0.2 (crate (name "mp2667") (vers "0.0.2") (deps (list (crate-dep (name "embedded-hal") (req "^0.2.5") (features (quote ("unproven"))) (default-features #t) (kind 0)) (crate-dep (name "modular-bitfield") (req "^0.11.2") (default-features #t) (kind 0)))) (hash "0czl8w0xiz6anzsdl10z9d1hzn8ikbd6niwnhjk600m6g9d8ajmk")))

(define-public crate-mp2667-0.0.3 (crate (name "mp2667") (vers "0.0.3") (deps (list (crate-dep (name "embedded-hal") (req "^0.2.5") (features (quote ("unproven"))) (default-features #t) (kind 0)) (crate-dep (name "modular-bitfield") (req "^0.11.2") (default-features #t) (kind 0)))) (hash "0mrvz9yq3nkza86c34wv7aa2hc3jglg2gm6d9w9x84yb9r75qsa2")))

(define-public crate-mp2667-0.0.4 (crate (name "mp2667") (vers "0.0.4") (deps (list (crate-dep (name "embedded-hal") (req "^0.2.5") (features (quote ("unproven"))) (default-features #t) (kind 0)) (crate-dep (name "modular-bitfield") (req "^0.11.2") (default-features #t) (kind 0)))) (hash "1bk731xk7l2l84gbpc1rs387jb5m98nmi846xj28i3h4dbixpmps")))

(define-public crate-mp2667-0.0.5 (crate (name "mp2667") (vers "0.0.5") (deps (list (crate-dep (name "embedded-hal") (req "^0.2.5") (features (quote ("unproven"))) (default-features #t) (kind 0)) (crate-dep (name "modular-bitfield") (req "^0.11.2") (default-features #t) (kind 0)))) (hash "17hqvqy54a2vz4jd4fckmzps7ndsbh1mk1nkb6jdnkyjmz8w4sjb")))

