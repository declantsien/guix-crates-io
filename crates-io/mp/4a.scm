(define-module (crates-io mp #{4a}#) #:use-module (crates-io))

(define-public crate-mp4ameta-0.1 (crate (name "mp4ameta") (vers "0.1.0") (deps (list (crate-dep (name "byteorder") (req "^1.3.4") (default-features #t) (kind 0)))) (hash "1yiayvnsry9nr2am0h6fr17nnllrwhqz6mcy2zi53naxfy123d5l")))

(define-public crate-mp4ameta-0.1 (crate (name "mp4ameta") (vers "0.1.1") (deps (list (crate-dep (name "byteorder") (req "^1.3.4") (default-features #t) (kind 0)))) (hash "1vi2q37vjkp3q5z2r6nwyz22gi89dkv3grf01s20m5m7215wm0c7")))

(define-public crate-mp4ameta-0.2 (crate (name "mp4ameta") (vers "0.2.0") (deps (list (crate-dep (name "byteorder") (req "^1.3.4") (default-features #t) (kind 0)))) (hash "0ag8aq1pzivvnp1sjamf7sfpxbhpx78mnzv7ncjvrbykslkz6f1m")))

(define-public crate-mp4ameta-0.2 (crate (name "mp4ameta") (vers "0.2.1") (deps (list (crate-dep (name "byteorder") (req "^1.3.4") (default-features #t) (kind 0)))) (hash "0gk2ilgxfg3bn1as2r24h10gqrickazy5l8z6ja4zcsvnf23f70c")))

(define-public crate-mp4ameta-0.2 (crate (name "mp4ameta") (vers "0.2.2") (deps (list (crate-dep (name "byteorder") (req "^1.3.4") (default-features #t) (kind 0)))) (hash "0y2q2c6xd17ms67r00yy92vx2vl65xqkdqi7q41y61b61r4ff39b")))

(define-public crate-mp4ameta-0.3 (crate (name "mp4ameta") (vers "0.3.0") (deps (list (crate-dep (name "byteorder") (req "^1.3.4") (default-features #t) (kind 0)))) (hash "0zsj4w9y5h3akraa4f5qpp5hazhkh39k8vlzjzi6r64n6x30fhqv")))

(define-public crate-mp4ameta-0.4 (crate (name "mp4ameta") (vers "0.4.0") (deps (list (crate-dep (name "byteorder") (req "^1.3.4") (default-features #t) (kind 0)))) (hash "0zg5q8xk7hb1a7x6409iwnjb8sjwiq6f2l5dlbm7mzd2h7iik7fq")))

(define-public crate-mp4ameta-0.4 (crate (name "mp4ameta") (vers "0.4.1") (deps (list (crate-dep (name "byteorder") (req "^1.3.4") (default-features #t) (kind 0)) (crate-dep (name "mp4ameta_proc") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "0y0gh40kzq7590lgmzwdxwcjf0c610amyk86b579647idy528p54")))

(define-public crate-mp4ameta-0.5 (crate (name "mp4ameta") (vers "0.5.0") (deps (list (crate-dep (name "byteorder") (req "^1.3.4") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "mp4ameta_proc") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "walkdir") (req "^2.3.1") (default-features #t) (kind 2)))) (hash "1fsxxa8j29f1ifwpwfldhmrd6r3r1lx4dlzx9yajzhgvr2mlp2ia")))

(define-public crate-mp4ameta-0.5 (crate (name "mp4ameta") (vers "0.5.1") (deps (list (crate-dep (name "byteorder") (req "^1.3.4") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "mp4ameta_proc") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "walkdir") (req "^2.3.1") (default-features #t) (kind 2)))) (hash "1930m9lbyf1sw0qnp16kh9fa35n0g67c3hcifdk81b9pp9lvds8m")))

(define-public crate-mp4ameta-0.6 (crate (name "mp4ameta") (vers "0.6.0") (deps (list (crate-dep (name "byteorder") (req "^1.3.4") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "mp4ameta_proc") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "walkdir") (req "^2.3.1") (default-features #t) (kind 2)))) (hash "1nwbv5769f815ksvqlh7pzi3n01cs5q93kwh4by7lw0wknilkmb9")))

(define-public crate-mp4ameta-0.6 (crate (name "mp4ameta") (vers "0.6.1") (deps (list (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "mp4ameta_proc") (req "^0.1.1") (default-features #t) (kind 0)) (crate-dep (name "walkdir") (req "^2.3.1") (default-features #t) (kind 2)))) (hash "1ch1jsp6vilkxlb8ds0cxwkq9l3jfifnx6r0w9xrxapya6xa5xqy")))

(define-public crate-mp4ameta-0.7 (crate (name "mp4ameta") (vers "0.7.0") (deps (list (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "mp4ameta_proc") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "walkdir") (req "^2.3.1") (default-features #t) (kind 2)))) (hash "13n8r5mhvadg0ygwg9x9migxrcayi98lcb038nxkzhkgn61l2283")))

(define-public crate-mp4ameta-0.7 (crate (name "mp4ameta") (vers "0.7.1") (deps (list (crate-dep (name "lazy_static") (req ">=1.4.0, <2.0.0") (default-features #t) (kind 0)) (crate-dep (name "mp4ameta_proc") (req ">=0.2.0, <0.3.0") (default-features #t) (kind 0)) (crate-dep (name "walkdir") (req ">=2.3.1, <3.0.0") (default-features #t) (kind 2)))) (hash "0cdf963v561sws5sfmwcjr2jnngvshn8awv436hs9d3j988x8hqd")))

(define-public crate-mp4ameta-0.7 (crate (name "mp4ameta") (vers "0.7.2") (deps (list (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "mp4ameta_proc") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "walkdir") (req "^2.3.1") (default-features #t) (kind 2)))) (hash "1zazrwv5jda7p9cyr2mfc5bdlqsi9ddw8vjzvhaa34wjjmwcx6n9") (yanked #t)))

(define-public crate-mp4ameta-0.7 (crate (name "mp4ameta") (vers "0.7.3") (deps (list (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "mp4ameta_proc") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "walkdir") (req "^2.3.1") (default-features #t) (kind 2)))) (hash "1i5j3n9a1bjp7zf22x3milll7s7l77m77nzw35qpjg9m77hlc176")))

(define-public crate-mp4ameta-0.8 (crate (name "mp4ameta") (vers "0.8.0") (deps (list (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "mp4ameta_proc") (req "^0.2.1") (default-features #t) (kind 0)) (crate-dep (name "walkdir") (req "^2.3.1") (default-features #t) (kind 2)))) (hash "1cvl5lxh115xfdglzsc97c9q52as69ksg67zrq5rlaz3xwmxl36q")))

(define-public crate-mp4ameta-0.9 (crate (name "mp4ameta") (vers "0.9.0") (deps (list (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "mp4ameta_proc") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "walkdir") (req "^2.3.1") (default-features #t) (kind 2)))) (hash "04j8mamcwq3bmvab9bvnpv4bdv129y8akbqzm9awlmyph5g0bcy6")))

(define-public crate-mp4ameta-0.9 (crate (name "mp4ameta") (vers "0.9.1") (deps (list (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "mp4ameta_proc") (req "^0.4.0") (default-features #t) (kind 0)) (crate-dep (name "walkdir") (req "^2.3.1") (default-features #t) (kind 2)))) (hash "0rjfkdivgqgc0nxf0irjp7wabwhskj5wrljk3jnyz446dgl6z7v1")))

(define-public crate-mp4ameta-0.10 (crate (name "mp4ameta") (vers "0.10.0") (deps (list (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "mp4ameta_proc") (req "^0.5.0") (default-features #t) (kind 0)) (crate-dep (name "walkdir") (req "^2.3.2") (default-features #t) (kind 2)))) (hash "0qjz7ixma56b7lgzgs0845yb4vzy8826i8dqk8nbgd1wyzbvhj84")))

(define-public crate-mp4ameta-0.10 (crate (name "mp4ameta") (vers "0.10.1") (deps (list (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "mp4ameta_proc") (req "^0.5.0") (default-features #t) (kind 0)) (crate-dep (name "walkdir") (req "^2.3.2") (default-features #t) (kind 2)))) (hash "0c0yms1343v4xpm9cz4l731p2h8g5n8q4abmcdpayj3m7c2izajn")))

(define-public crate-mp4ameta-0.10 (crate (name "mp4ameta") (vers "0.10.2") (deps (list (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "mp4ameta_proc") (req "^0.5.0") (default-features #t) (kind 0)) (crate-dep (name "walkdir") (req "^2.3.2") (default-features #t) (kind 2)))) (hash "1k2awwz8q7g4pb5k6lppwidirp55z1rzxx9r96d247bgaj1x3qa3")))

(define-public crate-mp4ameta-0.11 (crate (name "mp4ameta") (vers "0.11.0") (deps (list (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "mp4ameta_proc") (req "^0.6.0") (default-features #t) (kind 0)) (crate-dep (name "walkdir") (req "^2.3.2") (default-features #t) (kind 2)))) (hash "10by0ms6ml5wi4k9axls4gvcisk9jbm70z35g4zrladmiqpdc8zb")))

(define-public crate-mp4ameta_proc-0.1 (crate (name "mp4ameta_proc") (vers "0.1.0") (hash "15zc4aazrrsrm71fa03ygn63j7k9lm2qd6jzx6iz1si202lcbmbh")))

(define-public crate-mp4ameta_proc-0.1 (crate (name "mp4ameta_proc") (vers "0.1.1") (hash "1i82asxzdl4cxrb9y50s1y5j40qgx4c5ca89y2vy6rvb3x287bk4")))

(define-public crate-mp4ameta_proc-0.2 (crate (name "mp4ameta_proc") (vers "0.2.0") (hash "1c83r3nvf1s2dpizr96aybx5f6afjxbbvkq27lv12xlv4h57cy4v")))

(define-public crate-mp4ameta_proc-0.2 (crate (name "mp4ameta_proc") (vers "0.2.1") (hash "0ay02yjv0d1h3fpsizkxa8jfncjvxwv16icsl95rbd030r15c4yz")))

(define-public crate-mp4ameta_proc-0.3 (crate (name "mp4ameta_proc") (vers "0.3.0") (hash "04jybm1b8q7559pbdr8m520nkg4jyrdf77qfyb9mb4nb2jcjm1m4")))

(define-public crate-mp4ameta_proc-0.4 (crate (name "mp4ameta_proc") (vers "0.4.0") (hash "12jws7mqmhkps5kacs1nx6k5069xnnvqfgc0kyl9d7nn7hhcwxa9")))

(define-public crate-mp4ameta_proc-0.5 (crate (name "mp4ameta_proc") (vers "0.5.0") (hash "152was15fy9h193zbxsmww6l4vc4aiq9qhlbd4l4qwg7b89k535f")))

(define-public crate-mp4ameta_proc-0.6 (crate (name "mp4ameta_proc") (vers "0.6.0") (hash "01i8snk5kjd27zly5k1y6arvv80d6q1lh43pbxk0l33ls49wmp07")))

