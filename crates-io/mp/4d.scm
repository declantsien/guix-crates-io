(define-module (crates-io mp #{4d}#) #:use-module (crates-io))

(define-public crate-mp4decrypt-0.1 (crate (name "mp4decrypt") (vers "0.1.0+1.6.0-639") (deps (list (crate-dep (name "cc") (req "^1") (default-features #t) (kind 1)) (crate-dep (name "glob") (req "^0.3") (default-features #t) (kind 1)))) (hash "0bw61sgf44xvs8z2bb5jqz4hrc59rakyrzrv76cfw83nkz6a247f")))

(define-public crate-mp4decrypt-0.1 (crate (name "mp4decrypt") (vers "0.1.1+1.6.0-639") (deps (list (crate-dep (name "cc") (req "^1") (default-features #t) (kind 1)) (crate-dep (name "glob") (req "^0.3") (default-features #t) (kind 1)))) (hash "0q8rf6m779byb359h20v9bkkh1vn641i1qxbb8d6lg39xxm01kyh")))

(define-public crate-mp4decrypt-0.2 (crate (name "mp4decrypt") (vers "0.2.0+1.6.0-639") (deps (list (crate-dep (name "cc") (req "^1") (default-features #t) (kind 1)) (crate-dep (name "glob") (req "^0.3") (default-features #t) (kind 1)))) (hash "14dnzilwa2bgardy44d66najac4rjm7jpd1dwl17vlq2qccz2qpg")))

(define-public crate-mp4decrypt-0.2 (crate (name "mp4decrypt") (vers "0.2.1+1.6.0-639") (deps (list (crate-dep (name "cc") (req "^1") (default-features #t) (kind 1)) (crate-dep (name "glob") (req "^0.3") (default-features #t) (kind 1)))) (hash "18g21xq0x1gyjvn73pvkv7a6rzhdl0iy0f604a0rr26vklhma9i3")))

(define-public crate-mp4decrypt-0.3 (crate (name "mp4decrypt") (vers "0.3.0+1.6.0-639") (deps (list (crate-dep (name "cc") (req "^1") (default-features #t) (kind 1)))) (hash "1l253yfpym6z3xafdv685c5mkcv4p8p46l3d3mjng3gzyvj5cl51")))

(define-public crate-mp4decrypt-0.3 (crate (name "mp4decrypt") (vers "0.3.1+1.6.0-639") (deps (list (crate-dep (name "cc") (req "^1") (default-features #t) (kind 1)))) (hash "03zl3bn6ri323212zh26jj39yki82n2sddsvpj5xrs6ncdz3yfys")))

(define-public crate-mp4decrypt-0.4 (crate (name "mp4decrypt") (vers "0.4.0") (deps (list (crate-dep (name "bento4-src") (req "^0.1.0") (default-features #t) (kind 1)))) (hash "0crnw8lcciaphy9lqarw4hjls8wzf6fi4gk7zl95378kdy5jg6gm")))

(define-public crate-mp4decrypt-0.4 (crate (name "mp4decrypt") (vers "0.4.1") (deps (list (crate-dep (name "bento4-src") (req "^0.1") (default-features #t) (kind 1)))) (hash "1g3sj6ygnklsm4nyqqm42qxslvc4r3xgr0hnqw84r3nbl3yi5bwq")))

(define-public crate-mp4decrypt-0.4 (crate (name "mp4decrypt") (vers "0.4.2") (deps (list (crate-dep (name "bento4-src") (req "^0.1") (default-features #t) (kind 1)))) (hash "1khmdmww751slrm3x05h58mgy4rygr51sla1mjs1f6663xwbsqi3")))

