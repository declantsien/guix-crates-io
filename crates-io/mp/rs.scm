(define-module (crates-io mp rs) #:use-module (crates-io))

(define-public crate-mprs-0.1 (crate (name "mprs") (vers "0.1.8") (deps (list (crate-dep (name "colored") (req "^2.0.0") (default-features #t) (kind 0)) (crate-dep (name "mpd") (req "^0.0.12") (default-features #t) (kind 0)))) (hash "1mqi5azzr7mn4h46gmzpm26w4p37d9n9qf2i9xgmf35dbai7ryxa") (yanked #t)))

(define-public crate-mprs-0.1 (crate (name "mprs") (vers "0.1.9") (deps (list (crate-dep (name "colored") (req "^2.0.0") (default-features #t) (kind 0)) (crate-dep (name "configr") (req "^0.8.0") (default-features #t) (kind 0)) (crate-dep (name "mpd") (req "^0.0.12") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.130") (default-features #t) (kind 0)))) (hash "0966r968y5ngzpk2j4zn5rilbdwkan882c3q75pw1g2gz7i1q483") (yanked #t)))

(define-public crate-mprs-0.1 (crate (name "mprs") (vers "0.1.10") (deps (list (crate-dep (name "colored") (req "^2.0.0") (default-features #t) (kind 0)) (crate-dep (name "configr") (req "^0.8.0") (default-features #t) (kind 0)) (crate-dep (name "mpd") (req "^0.0.12") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.130") (default-features #t) (kind 0)))) (hash "0r6rsz1skzliy1qbjfbkhkvf8zc7kgh34jg3l0kisn452zagdi7c") (yanked #t)))

(define-public crate-mprs-0.2 (crate (name "mprs") (vers "0.2.0-beta") (deps (list (crate-dep (name "mpd") (req "^0.0.12") (default-features #t) (kind 0)) (crate-dep (name "prettytable-rs") (req "^0.8.0") (default-features #t) (kind 0)))) (hash "1h466g4jdvrh3dny66751anrw0hzs733qzv6vyan9wrzmcd82052")))

(define-public crate-mprs-0.2 (crate (name "mprs") (vers "0.2.0") (deps (list (crate-dep (name "colored") (req "^2.0.0") (default-features #t) (kind 0)) (crate-dep (name "mpd") (req "^0.0.12") (default-features #t) (kind 0)) (crate-dep (name "prettytable-rs") (req "^0.9.0") (default-features #t) (kind 0)))) (hash "0rn3nwhn173hb3kkcl82s0rl04r1isd02yj0vpzv96cihdnp57if")))

