(define-module (crates-io mp xr) #:use-module (crates-io))

(define-public crate-mpxrs-0.1 (crate (name "mpxrs") (vers "0.1.0") (deps (list (crate-dep (name "j4rs") (req "^0.17.2") (default-features #t) (kind 0)) (crate-dep (name "path-absolutize") (req "^3.1.1") (default-features #t) (kind 0)))) (hash "1rf6j2nsbj1agwbxhgfb6kl1v2m4k321viq7x5fkfnc2zzv9g3gf")))

