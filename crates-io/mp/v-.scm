(define-module (crates-io mp v-) #:use-module (crates-io))

(define-public crate-mpv-audio-0.1 (crate (name "mpv-audio") (vers "0.1.0") (hash "1ip60nj2vfglmgbsxkdkk9ac4m0558il5z901xsx3yssr93048ac")))

(define-public crate-mpv-audio-0.1 (crate (name "mpv-audio") (vers "0.1.1") (hash "1b12yini9ziq5xmh2561wbf64vzrbiws64s0xqgx3ra86wfvqp36")))

(define-public crate-mpv-client-0.1 (crate (name "mpv-client") (vers "0.1.0") (hash "1k8dsf1pkkgdz94lz6k37dk1fb83cf8g9g0zhdq0w4ic0dhky16d") (yanked #t)))

(define-public crate-mpv-client-0.1 (crate (name "mpv-client") (vers "0.1.1") (hash "0l7n43ff7ci6mjpihhm7lr5k80k81fj861w7rc5l7dpv38id2c59") (yanked #t)))

(define-public crate-mpv-client-0.1 (crate (name "mpv-client") (vers "0.1.2") (hash "0aaxg4q8bfha5x8anc4rx70fjfzk129z4yps33pbbl9h87mpd0li") (yanked #t)))

(define-public crate-mpv-client-0.1 (crate (name "mpv-client") (vers "0.1.3") (hash "160h6mm0gd87hmbvdsz05xdw5gm44cwja9fky49y48gq457dk7nm") (yanked #t)))

(define-public crate-mpv-client-0.1 (crate (name "mpv-client") (vers "0.1.4") (hash "0h9ka4azpq5i1b4xbhppyr0783wzaxc277ia7d9khmp8pja3c5dk") (yanked #t)))

(define-public crate-mpv-client-0.2 (crate (name "mpv-client") (vers "0.2.0") (hash "1jwkbd8y4q0rm7jcgh1qm3w4i15g6ldxdl7b9g098gkys70vgvgl") (yanked #t)))

(define-public crate-mpv-client-0.3 (crate (name "mpv-client") (vers "0.3.0") (hash "1vkdv2cr5z95jxfnp6sjcwzmd30l8wbjq31lgwc5ppwzgw797l1r") (yanked #t)))

(define-public crate-mpv-client-0.3 (crate (name "mpv-client") (vers "0.3.1") (hash "1da8nr40vafj27b0xa750ida2fh3cr6d8m5qd1jihrg5ib2zd8k6") (yanked #t)))

(define-public crate-mpv-client-0.3 (crate (name "mpv-client") (vers "0.3.2") (hash "1c1zf2r9a52s0j2y1lm5gi3nll0b6qprgbj1zqvm4as9mzlnchm7")))

(define-public crate-mpv-client-0.3 (crate (name "mpv-client") (vers "0.3.3") (hash "1yx1s96jb4klrxg0i9qylkjybj27qxf5jlbjvry3bv1y7lcdx6mp")))

(define-public crate-mpv-client-0.4 (crate (name "mpv-client") (vers "0.4.0") (hash "1gj5jpp62xhlhl7vbljpi81zg76vw612y1q9gz6mgasnbk61ryr0")))

(define-public crate-mpv-client-0.4 (crate (name "mpv-client") (vers "0.4.1") (hash "04arm8inmhqxva95adf0wnxm875iw9361axzg0jlbi3bhw79b4a2")))

(define-public crate-mpv-client-0.5 (crate (name "mpv-client") (vers "0.5.0") (hash "113yy6942w9kb441zmqwm2hmdvxaa70f01aiz25mmv0jlm759pmp")))

(define-public crate-mpv-client-0.6 (crate (name "mpv-client") (vers "0.6.0") (hash "12mvwsarldiw5yy7ma7w4x9k2q76rv13qg4va7kac1kp4w8hdxc9")))

(define-public crate-mpv-client-0.6 (crate (name "mpv-client") (vers "0.6.1") (hash "16dc4w4zbh9jwr9p08ij6ni94i74k4ilsrg0807dn22j2qb8xq29")))

(define-public crate-mpv-client-0.6 (crate (name "mpv-client") (vers "0.6.2") (hash "1vvz3z7vzkvdnbw7h1cnk9psmkaycq9ladlykp2vpindk0ih086c")))

(define-public crate-mpv-client-dyn-0.5 (crate (name "mpv-client-dyn") (vers "0.5.0") (deps (list (crate-dep (name "libloading") (req "^0.8.1") (default-features #t) (kind 0)) (crate-dep (name "once_cell") (req "^1.18.0") (default-features #t) (kind 0)))) (hash "068kp4xg8vyh9k8yvcmxb805qd0v89dgr9zs5xzdrbck8m2y03af")))

