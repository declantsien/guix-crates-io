(define-module (crates-io mp mc) #:use-module (crates-io))

(define-public crate-mpmc-0.1 (crate (name "mpmc") (vers "0.1.2") (hash "0sfs3lbjg09jjz7calr4agy4lhlyriigs86ib5mwxaw9da06qgp9")))

(define-public crate-mpmc-0.1 (crate (name "mpmc") (vers "0.1.3") (hash "19fhxbhj1xs1zkq9nrswrbnj2ww06k6sw43ir03dv58v0wb6ccjw")))

(define-public crate-mpmc-0.1 (crate (name "mpmc") (vers "0.1.4") (hash "0hap9g21rzqa91cr7m3xc9i3lwri3jmjypfdmnk5m577gqmjp073")))

(define-public crate-mpmc-0.1 (crate (name "mpmc") (vers "0.1.5") (hash "0fygw43hb5whrq14azakg1hrdssvc6q6jwnrn7392hkqimlpr56b")))

(define-public crate-mpmc-0.1 (crate (name "mpmc") (vers "0.1.6") (hash "1vfwd5xd5r9m72dqjydplb05b07wzn7yvy5m05pfjfwm58jb2y5z")))

(define-public crate-mpmc-async-0.1 (crate (name "mpmc-async") (vers "0.1.0") (deps (list (crate-dep (name "tokio") (req "^1.36.0") (features (quote ("rt" "macros" "time"))) (default-features #t) (kind 2)))) (hash "04zhm3r728hvfrcc8cmihm8mw6w6015jmk39y7hk4hiqm42dh1zj")))

(define-public crate-mpmc-async-0.1 (crate (name "mpmc-async") (vers "0.1.1") (deps (list (crate-dep (name "tokio") (req "^1.36.0") (features (quote ("rt" "macros" "time"))) (default-features #t) (kind 2)) (crate-dep (name "tokio-test") (req "^0.4.4") (default-features #t) (kind 2)))) (hash "161pxf5j5vp87dq7rg8y3k4651zz4ma6hr5g3jh79x380ljmhav0")))

(define-public crate-mpmc-async-0.1 (crate (name "mpmc-async") (vers "0.1.2") (deps (list (crate-dep (name "tokio") (req "^1.36.0") (features (quote ("rt" "macros" "time"))) (default-features #t) (kind 2)) (crate-dep (name "tokio-test") (req "^0.4.4") (default-features #t) (kind 2)))) (hash "0yf7jg2gzxkqj0p7wnprf7wqwla48qv85g8wwzbcsxn6j7djx75l")))

(define-public crate-mpmc-async-0.1 (crate (name "mpmc-async") (vers "0.1.3") (deps (list (crate-dep (name "tokio") (req "^1.36.0") (features (quote ("rt" "macros" "time"))) (default-features #t) (kind 2)) (crate-dep (name "tokio-test") (req "^0.4.4") (default-features #t) (kind 2)))) (hash "1pqghbxwljfkjq195wh351d6r379bdmzp3dcp2dgqkaxsj08pbcc")))

(define-public crate-mpmc-async-0.1 (crate (name "mpmc-async") (vers "0.1.4") (deps (list (crate-dep (name "tokio") (req "^1.36.0") (features (quote ("rt" "macros" "time"))) (default-features #t) (kind 2)) (crate-dep (name "tokio-test") (req "^0.4.4") (default-features #t) (kind 2)))) (hash "0i5i1fzvqg33mpgkr9lki8prasssjcmc0brhjpy09xj9pxhqj43b")))

(define-public crate-mpmc-async-0.1 (crate (name "mpmc-async") (vers "0.1.5") (deps (list (crate-dep (name "tokio") (req "^1.36.0") (features (quote ("rt" "macros" "time"))) (default-features #t) (kind 2)) (crate-dep (name "tokio-test") (req "^0.4.4") (default-features #t) (kind 2)))) (hash "1678p9wvj2wi30hm5bh2z8p5p4ihy56l7688ggynm71ypglsznyr")))

(define-public crate-mpmc-async-0.1 (crate (name "mpmc-async") (vers "0.1.6") (deps (list (crate-dep (name "tokio") (req "^1.36.0") (features (quote ("rt" "macros" "time"))) (default-features #t) (kind 2)) (crate-dep (name "tokio-test") (req "^0.4.4") (default-features #t) (kind 2)))) (hash "1v65v1q2yhz744igg6y4xgns5yh49236h9qj1ab3q63a9wrckh6w")))

(define-public crate-mpmc-async-0.1 (crate (name "mpmc-async") (vers "0.1.7") (deps (list (crate-dep (name "tokio") (req "^1.36.0") (features (quote ("rt" "macros" "time"))) (default-features #t) (kind 2)) (crate-dep (name "tokio-test") (req "^0.4.4") (default-features #t) (kind 2)))) (hash "0bjj7sj7d59h27b81wfcl5ny5z4jb9zqg30kqfi37h28san8ri1f")))

(define-public crate-mpmc-map-0.1 (crate (name "mpmc-map") (vers "0.1.0") (deps (list (crate-dep (name "arc-swap") (req "^1.2") (default-features #t) (kind 0)) (crate-dep (name "im") (req "^15.0") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1") (features (quote ("sync" "rt"))) (default-features #t) (kind 0)))) (hash "1cissi4wwhbhbx3dmha8iwg4zvixxj2ihpa4sraiilnwfn63qs10")))

(define-public crate-mpmc-map-0.1 (crate (name "mpmc-map") (vers "0.1.1") (deps (list (crate-dep (name "arc-swap") (req "^1.2") (default-features #t) (kind 0)) (crate-dep (name "im") (req "^15.0") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1") (features (quote ("sync" "rt"))) (default-features #t) (kind 0)))) (hash "1dkacrp2f2s39s5y3932h1jgjp0ajk3i9yg6dys10gxazwnnlvl4")))

(define-public crate-mpmc-map-0.1 (crate (name "mpmc-map") (vers "0.1.2") (deps (list (crate-dep (name "arc-swap") (req "^1.2") (default-features #t) (kind 0)) (crate-dep (name "im") (req "^15.0") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1") (features (quote ("sync" "rt"))) (default-features #t) (kind 0)))) (hash "0qbv7fhiq6gnqrfvmklqw5r263jx8pdwmsb635g2h7dhvp0y4j4b")))

(define-public crate-mpmc-map-0.1 (crate (name "mpmc-map") (vers "0.1.3") (deps (list (crate-dep (name "arc-swap") (req "^1.2") (default-features #t) (kind 0)) (crate-dep (name "im") (req "^15.0") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1") (features (quote ("sync" "rt"))) (default-features #t) (kind 0)))) (hash "1fpmvqa4sjly2m48dyyjsffgf4wg58kfx6w08m0lgc3ziy5ap5l1")))

(define-public crate-mpmc-map-0.1 (crate (name "mpmc-map") (vers "0.1.4") (deps (list (crate-dep (name "arc-swap") (req "^1.2") (default-features #t) (kind 0)) (crate-dep (name "im") (req "^15.0") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1") (features (quote ("sync" "rt"))) (default-features #t) (kind 0)))) (hash "034f9rngrg63yv6xy3fx980fmllgy2h5sc2wzc7r684h8rmbmkb7")))

(define-public crate-mpmc-map-0.1 (crate (name "mpmc-map") (vers "0.1.5") (deps (list (crate-dep (name "arc-swap") (req "^1.2") (default-features #t) (kind 0)) (crate-dep (name "im") (req "^15.0") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1") (features (quote ("sync" "rt"))) (default-features #t) (kind 0)))) (hash "06y14wmrmh29d90rm5vrvdr3ib18pbm97b020h7qx3y09h351qz9")))

(define-public crate-mpmc-map-0.1 (crate (name "mpmc-map") (vers "0.1.6") (deps (list (crate-dep (name "arc-swap") (req "^1.2") (default-features #t) (kind 0)) (crate-dep (name "im") (req "^15.0") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1") (features (quote ("sync" "rt"))) (default-features #t) (kind 0)))) (hash "0r4lr0wwl7h3n626bqis1cak00hhj3zz6bnnps19qgbh8120r58y")))

(define-public crate-mpmc-map-0.2 (crate (name "mpmc-map") (vers "0.2.0") (deps (list (crate-dep (name "arc-swap") (req "^1.2") (default-features #t) (kind 0)) (crate-dep (name "im") (req "^15.0") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1") (features (quote ("sync" "rt"))) (default-features #t) (kind 0)))) (hash "1qjlrmp6vwccizfsy1pwr9m24nf0kw0aq1hi082nqa3py4ys8nzj")))

(define-public crate-mpmc-ringbuf-0.1 (crate (name "mpmc-ringbuf") (vers "0.1.0") (deps (list (crate-dep (name "libmath") (req "^0.2.1") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.22.0") (default-features #t) (kind 0)))) (hash "190x801dsa553jadqx7kp8kkn14z7f3f64imb44lw696hyadxrrq")))

(define-public crate-mpmc-ringbuf-0.1 (crate (name "mpmc-ringbuf") (vers "0.1.1") (deps (list (crate-dep (name "libmath") (req "^0.2.1") (default-features #t) (kind 0)))) (hash "1blw5cd36pi6gg1968h1098ggag4d5np68xbxwhg938nfhxkxcsa")))

(define-public crate-mpmc-ringbuf-0.1 (crate (name "mpmc-ringbuf") (vers "0.1.2") (deps (list (crate-dep (name "libmath") (req "^0.2.1") (default-features #t) (kind 0)))) (hash "06wifznadz246rkh5ahj1hmq0w8ngm2zxxajad0c8mv0bq5qj0fc")))

(define-public crate-mpmc-ringbuf-0.1 (crate (name "mpmc-ringbuf") (vers "0.1.3") (deps (list (crate-dep (name "libmath") (req "^0.2.1") (default-features #t) (kind 0)))) (hash "00q2hw4n8dvj5va48is9h5l9fbxk7pafag9yzqqwxpxx678xzy4b")))

(define-public crate-mpmc-ringbuf-0.1 (crate (name "mpmc-ringbuf") (vers "0.1.4") (deps (list (crate-dep (name "libmath") (req "^0.2.1") (default-features #t) (kind 0)))) (hash "09jhz974m1y0gzsgl1v4z02nf7x7lvh3fsvww16kdymx9k6fbv72")))

(define-public crate-mpmc-scheduler-0.1 (crate (name "mpmc-scheduler") (vers "0.1.0") (deps (list (crate-dep (name "bus") (req "^2.0") (default-features #t) (kind 0)) (crate-dep (name "futures") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "npnc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^0.1") (default-features #t) (kind 2)))) (hash "07v3bk5rz6ip5l7a90in04clilsmk2fvyggsrlilphlgx0cckyrs")))

(define-public crate-mpmc-scheduler-0.1 (crate (name "mpmc-scheduler") (vers "0.1.1") (deps (list (crate-dep (name "bus") (req "^2.0") (default-features #t) (kind 0)) (crate-dep (name "futures") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "npnc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^0.1") (default-features #t) (kind 2)))) (hash "1d2n8vrsllvz3r5zkrc6xha7av6jvvcapbc97vng221yid2859sv")))

(define-public crate-mpmc-scheduler-0.1 (crate (name "mpmc-scheduler") (vers "0.1.2") (deps (list (crate-dep (name "bus") (req "^2.0") (default-features #t) (kind 0)) (crate-dep (name "futures") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "npnc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^0.1") (default-features #t) (kind 2)))) (hash "1f5r51wswbv3pfv988gzqjqinr97wgicms2vmgfjrvg64qq0967a")))

(define-public crate-mpmc-scheduler-0.2 (crate (name "mpmc-scheduler") (vers "0.2.0") (deps (list (crate-dep (name "bus") (req "^2.0") (default-features #t) (kind 0)) (crate-dep (name "futures") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "npnc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^0.1") (default-features #t) (kind 2)))) (hash "0g11s1ilnbpaaw8qh5x9zlckc1nmg3hl0qzg4vmmk4kd8qchlja7")))

(define-public crate-mpmc-scheduler-0.2 (crate (name "mpmc-scheduler") (vers "0.2.1") (deps (list (crate-dep (name "bus") (req "^2.0") (default-features #t) (kind 0)) (crate-dep (name "futures") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "npnc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^0.1") (default-features #t) (kind 2)))) (hash "1pzz0rvxplx4654g419307f1b2fg6zmxb0j851vnszzmi6ck3ihc")))

(define-public crate-mpmc-scheduler-0.2 (crate (name "mpmc-scheduler") (vers "0.2.2") (deps (list (crate-dep (name "bus") (req "^2.0") (default-features #t) (kind 0)) (crate-dep (name "futures") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "npnc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^0.1") (default-features #t) (kind 2)))) (hash "02iisylf9kj9gsl0bd3sxzwk3ny0f94kiqf2q08rkm2hvrmswv1a")))

(define-public crate-mpmc-scheduler-0.3 (crate (name "mpmc-scheduler") (vers "0.3.0") (deps (list (crate-dep (name "bus") (req "^2") (default-features #t) (kind 0)) (crate-dep (name "futures") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "npnc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^0.1") (default-features #t) (kind 2)))) (hash "07rcw4vpnxa4llkym2vvcjr1v9m5p23qvg3913wg5gjq6yy05wyd")))

(define-public crate-mpmc-scheduler-0.3 (crate (name "mpmc-scheduler") (vers "0.3.1") (deps (list (crate-dep (name "bus") (req "^2") (default-features #t) (kind 0)) (crate-dep (name "futures") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "npnc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^0.1") (default-features #t) (kind 2)))) (hash "06gd66jqa1cqsxzb8qsv8p5hdxdz2908gx79irhip1hrzzlxc3jq")))

(define-public crate-mpmcpq-0.1 (crate (name "mpmcpq") (vers "0.1.0") (deps (list (crate-dep (name "env_logger") (req "^0.9") (default-features #t) (kind 2)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 2)) (crate-dep (name "parking_lot") (req "^0.11") (default-features #t) (kind 0)))) (hash "1524bb8fhdkpip0wynlczddq6c2yfl3k96rc0kbpdq6rvr6ql73k")))

(define-public crate-mpmcpq-0.1 (crate (name "mpmcpq") (vers "0.1.1") (deps (list (crate-dep (name "env_logger") (req "^0.9") (default-features #t) (kind 2)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 2)) (crate-dep (name "parking_lot") (req "^0.11") (default-features #t) (kind 0)))) (hash "09ka4jdz20hkz50wdswkndhd3p6k7py5nvp45mgyqw31gbgr8s5v")))

(define-public crate-mpmcpq-0.2 (crate (name "mpmcpq") (vers "0.2.0") (deps (list (crate-dep (name "env_logger") (req "^0.9") (default-features #t) (kind 2)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 2)) (crate-dep (name "parking_lot") (req "^0.11") (default-features #t) (kind 0)))) (hash "0c2z3yc8pgfamis36z6lrxk1kkrp2l9mxl7hpgdmiviln6gqqjzf")))

(define-public crate-mpmcpq-0.3 (crate (name "mpmcpq") (vers "0.3.0") (deps (list (crate-dep (name "env_logger") (req "^0.9") (default-features #t) (kind 2)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 2)) (crate-dep (name "parking_lot") (req "^0.11") (default-features #t) (kind 0)))) (hash "1cfk81ybjkdd8f381rpyy75cgmsh1haw4gjwr8si0nw3bv9sj3kc")))

(define-public crate-mpmcpq-0.4 (crate (name "mpmcpq") (vers "0.4.0") (deps (list (crate-dep (name "env_logger") (req "^0.9") (default-features #t) (kind 2)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 2)) (crate-dep (name "parking_lot") (req "^0.11") (default-features #t) (kind 0)))) (hash "13132p2ainhj17dzvrhlvzavns6kzzny9dsbf9gwwj7hd7sbmfrv")))

(define-public crate-mpmcpq-0.5 (crate (name "mpmcpq") (vers "0.5.0") (deps (list (crate-dep (name "env_logger") (req "^0.9") (default-features #t) (kind 2)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 2)) (crate-dep (name "parking_lot") (req "^0.11") (default-features #t) (kind 0)))) (hash "03w3kn891rmr838jnbwm07lnxx4fz19346kiapzwm390arw2yriw")))

(define-public crate-mpmcpq-0.6 (crate (name "mpmcpq") (vers "0.6.0") (deps (list (crate-dep (name "env_logger") (req "^0.9") (default-features #t) (kind 2)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 2)) (crate-dep (name "parking_lot") (req "^0.11") (default-features #t) (kind 0)))) (hash "1syi3vqa1qhg4bbkllcq9lxy0ppj53i4av99sbm9rfx29lxjj9w9")))

(define-public crate-mpmcpq-0.7 (crate (name "mpmcpq") (vers "0.7.0") (deps (list (crate-dep (name "env_logger") (req "^0.9") (default-features #t) (kind 2)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 2)) (crate-dep (name "parking_lot") (req "^0.11") (default-features #t) (kind 0)))) (hash "0ng290y3jnkay3ym3hq678ghl5hrd0kkrzvs1rxkddinc48bs9j9")))

(define-public crate-mpmcpq-0.8 (crate (name "mpmcpq") (vers "0.8.0") (deps (list (crate-dep (name "env_logger") (req "^0.9") (default-features #t) (kind 2)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 2)) (crate-dep (name "parking_lot") (req "^0.11") (default-features #t) (kind 0)))) (hash "0kfjabfyc8b421y6plh51bn0d9aynxwmycbr1svhmyqjl5wi0r20")))

(define-public crate-mpmcpq-0.9 (crate (name "mpmcpq") (vers "0.9.1") (deps (list (crate-dep (name "env_logger") (req "^0.9") (default-features #t) (kind 2)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 2)) (crate-dep (name "parking_lot") (req ">=0.11, <=0.12") (default-features #t) (kind 0)))) (hash "1dcibz0vgj3bj6qh8gzwj1bg561gic1rw8krz1xr02w786s3fs23")))

(define-public crate-mpmcpq-0.9 (crate (name "mpmcpq") (vers "0.9.2") (deps (list (crate-dep (name "env_logger") (req "^0.9") (default-features #t) (kind 2)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 2)) (crate-dep (name "parking_lot") (req ">=0.11, <=0.13") (default-features #t) (kind 0)))) (hash "0qsizcwhg93bixbcxigsj2m8q2k06przfwcsxq3snw5vhc5ggrra")))

(define-public crate-mpmcpq-0.9 (crate (name "mpmcpq") (vers "0.9.3") (deps (list (crate-dep (name "env_logger") (req "^0.10") (default-features #t) (kind 2)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 2)) (crate-dep (name "parking_lot") (req ">=0.11, <=0.13") (default-features #t) (kind 0)))) (hash "0m3vd5vrjl62hlznw7xxgyhiwkxsv4n3j0vdja44pmz0hgphjb07")))

