(define-module (crates-io mp r1) #:use-module (crates-io))

(define-public crate-mpr121-0.1 (crate (name "mpr121") (vers "0.1.0") (hash "1zxiy111n7rxxqnj6lrwnnrlny8y24k9n5dprj2pcsx0x1chn9rr")))

(define-public crate-mpr121-hal-0.1 (crate (name "mpr121-hal") (vers "0.1.0") (deps (list (crate-dep (name "embedded-hal") (req "^0.2.7") (default-features #t) (kind 0)))) (hash "14y3v3flcnq3zxjdih8wypmn749vf34jmry5yzy5193fjpwvs3qi")))

(define-public crate-mpr121-hal-0.2 (crate (name "mpr121-hal") (vers "0.2.0") (deps (list (crate-dep (name "embedded-hal") (req "^0.2.7") (default-features #t) (kind 0)))) (hash "1361y8mpqi96b5szyyfawkpdqa7vdfcmxl0f1cfm00fhl4x5j0md")))

(define-public crate-mpr121-hal-0.3 (crate (name "mpr121-hal") (vers "0.3.0") (deps (list (crate-dep (name "embedded-hal") (req "^0.2.7") (default-features #t) (kind 0)))) (hash "07zp5kmv8g7832wv9byrijk3hc0pqwijrv1zkp4kxpgi0276kr49")))

