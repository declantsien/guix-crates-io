(define-module (crates-io mp r-) #:use-module (crates-io))

(define-public crate-mpr-sys-0.0.1 (crate (name "mpr-sys") (vers "0.0.1") (deps (list (crate-dep (name "winapi") (req "*") (default-features #t) (kind 0)))) (hash "0f349c4pvwlxc7lwml5cqlq8z211khx9vmvkwj5x41rwdwajzz06")))

(define-public crate-mpr-sys-0.1 (crate (name "mpr-sys") (vers "0.1.0") (deps (list (crate-dep (name "winapi") (req "*") (default-features #t) (kind 0)) (crate-dep (name "winapi-build") (req "*") (default-features #t) (kind 1)))) (hash "19a5whylkf3ddnnfblwsklignczv684rg8bmdcvwrywzy33shqaq")))

(define-public crate-mpr-sys-0.1 (crate (name "mpr-sys") (vers "0.1.1") (deps (list (crate-dep (name "winapi") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "winapi-build") (req "^0.1.1") (default-features #t) (kind 1)))) (hash "1sd5v6jx0fg50y008pb87ddv5b7acjczij0njlpvlmw57n3cn2s7")))

