(define-module (crates-io mp g1) #:use-module (crates-io))

(define-public crate-mpg123-0.1 (crate (name "mpg123") (vers "0.1.0") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "mpg123-sys") (req "^0.6.0") (default-features #t) (kind 0)))) (hash "1r0mcxa8qh2prqnmlkj3adwyp17cyf4gw9xi3snw3frryqvbng4r")))

(define-public crate-mpg123-0.1 (crate (name "mpg123") (vers "0.1.1") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "mpg123-sys") (req "^0.6.0") (default-features #t) (kind 0)))) (hash "1bvyp0nq5lar6n37rmw2iliv4iqipyzivpf5di25r1ym5kriainv")))

(define-public crate-mpg123-0.1 (crate (name "mpg123") (vers "0.1.2") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "mpg123-sys") (req "^0.6.0") (default-features #t) (kind 0)))) (hash "09102084awq9w3jq3blxnd07qgpgjidllpflshgd7q309xfx77aq")))

(define-public crate-mpg123-sys-0.0.1 (crate (name "mpg123-sys") (vers "0.0.1") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "pkg-config") (req "^0.3") (default-features #t) (kind 1)))) (hash "1a5xbncjjs6gs0h5ybfa0j5aprih90xkxkqq2js87w2fman4vay8")))

(define-public crate-mpg123-sys-0.0.2 (crate (name "mpg123-sys") (vers "0.0.2") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "pkg-config") (req "^0.3") (default-features #t) (kind 1)))) (hash "1q0k5prv8mwj8ccv6i00n03d8mvli2k1m2siylyngwg16f7knh9r")))

(define-public crate-mpg123-sys-0.0.3 (crate (name "mpg123-sys") (vers "0.0.3") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "pkg-config") (req "^0.3") (default-features #t) (kind 1)))) (hash "1g97pn36rmx1qrq1jm3j5vzdjzv04h09h6l521n772mhmbw262vq")))

(define-public crate-mpg123-sys-0.1 (crate (name "mpg123-sys") (vers "0.1.0") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "pkg-config") (req "^0.3") (default-features #t) (kind 1)))) (hash "1ci46k2qaaz25973w5nwndki3hp3x613rmj0ssyb8glwggwdanih")))

(define-public crate-mpg123-sys-0.2 (crate (name "mpg123-sys") (vers "0.2.0") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "pkg-config") (req "^0.3") (default-features #t) (kind 1)))) (hash "1m616vl87svz9dmgf7cf9zs70bj15gxj9v2qaf72dxk0fwcs4yym")))

(define-public crate-mpg123-sys-0.3 (crate (name "mpg123-sys") (vers "0.3.0") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "pkg-config") (req "^0.3") (default-features #t) (kind 1)))) (hash "0vallqfkb1d6d5xrpbr750c0gvv3g9i746qs0a61j3939dlsz6pw") (features (quote (("static"))))))

(define-public crate-mpg123-sys-0.3 (crate (name "mpg123-sys") (vers "0.3.1") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "pkg-config") (req "^0.3") (default-features #t) (kind 1)))) (hash "0ibj3d31fdz1gv5n7fkrc8j1gb8aasbw31mcm0ln2z56l80prr29") (features (quote (("static"))))))

(define-public crate-mpg123-sys-0.4 (crate (name "mpg123-sys") (vers "0.4.0") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "pkg-config") (req "^0.3") (default-features #t) (kind 1)))) (hash "057rh2wcibpvnn8xic8sjgd3b622hjilfh15jbzgvc0bar7c5nnl") (features (quote (("static"))))))

(define-public crate-mpg123-sys-0.4 (crate (name "mpg123-sys") (vers "0.4.1") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "pkg-config") (req "^0.3") (default-features #t) (kind 1)))) (hash "16pjmb80v5i8sn1sl65f0wys9smhrkbv19i3dvnjh10nrlsn3zm5") (features (quote (("static"))))))

(define-public crate-mpg123-sys-0.5 (crate (name "mpg123-sys") (vers "0.5.0") (deps (list (crate-dep (name "bitflags") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "pkg-config") (req "^0.3") (default-features #t) (kind 1)))) (hash "09qva7mvdqx10byivbfs9zsr2my15lwgwcfc4v44l068bzykfxq9") (features (quote (("static"))))))

(define-public crate-mpg123-sys-0.6 (crate (name "mpg123-sys") (vers "0.6.0") (deps (list (crate-dep (name "bitflags") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "pkg-config") (req "^0.3") (default-features #t) (kind 1)))) (hash "043nj0bp44srx6q1ih3daxlciangpgdgq2f1j4q45hc5klijcp57") (features (quote (("static"))))))

