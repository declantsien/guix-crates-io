(define-module (crates-io mp #{3-}#) #:use-module (crates-io))

(define-public crate-mp3-duration-0.1 (crate (name "mp3-duration") (vers "0.1.0") (deps (list (crate-dep (name "failure") (req "^0.1.1") (default-features #t) (kind 0)))) (hash "1j5sxpgz5jqj543izhg8whhndilsmg849nqz7f83646zywyrwk1s")))

(define-public crate-mp3-duration-0.1 (crate (name "mp3-duration") (vers "0.1.1") (deps (list (crate-dep (name "failure") (req "^0.1.1") (default-features #t) (kind 0)))) (hash "13ia6svabf79yg5866gzvhdgc0pjwxd4h6is1fhga93sknndq3b3")))

(define-public crate-mp3-duration-0.1 (crate (name "mp3-duration") (vers "0.1.2") (deps (list (crate-dep (name "failure") (req "^0.1.1") (default-features #t) (kind 0)))) (hash "0pm7k2dwyld78ryr4j5bqnbw89l8v3ldhs8afgzvflph3a36mxp2")))

(define-public crate-mp3-duration-0.1 (crate (name "mp3-duration") (vers "0.1.3") (deps (list (crate-dep (name "failure") (req "^0.1.1") (default-features #t) (kind 0)))) (hash "09nfdzlznpfpfzi8yjzy5hm8adsacqm0dgvfpw9k58j5a2knn093")))

(define-public crate-mp3-duration-0.1 (crate (name "mp3-duration") (vers "0.1.4") (deps (list (crate-dep (name "failure") (req "^0.1.5") (default-features #t) (kind 0)))) (hash "1kf5qfp7fwaafffakvkc1kn4gf1yqs00i1nzszhd2fdbjn96wryq")))

(define-public crate-mp3-duration-0.1 (crate (name "mp3-duration") (vers "0.1.5") (deps (list (crate-dep (name "failure") (req "^0.1.5") (default-features #t) (kind 0)))) (hash "1js3mv70bfik9pj0a6fs6sbzafqza8fz963z41wanv5ipijjzyfn")))

(define-public crate-mp3-duration-0.1 (crate (name "mp3-duration") (vers "0.1.6") (deps (list (crate-dep (name "failure") (req "^0.1.5") (default-features #t) (kind 0)))) (hash "1wq63vl6nmr90zgq0zjfa4vw1mn6pv1pcrng39gqrknln13sja90")))

(define-public crate-mp3-duration-0.1 (crate (name "mp3-duration") (vers "0.1.7") (deps (list (crate-dep (name "failure") (req "^0.1.5") (default-features #t) (kind 0)))) (hash "0kv0rm2k4kgqsspqcpdn9bzbwgymvz8m8ckk0mjshj8xpppc6z4v")))

(define-public crate-mp3-duration-0.1 (crate (name "mp3-duration") (vers "0.1.8") (deps (list (crate-dep (name "failure") (req "^0.1.5") (default-features #t) (kind 0)))) (hash "0sbp827ixlz26vn4wjm3cglzq3chw2jgkk8wfa0ac2l6zplvfz42")))

(define-public crate-mp3-duration-0.1 (crate (name "mp3-duration") (vers "0.1.9") (deps (list (crate-dep (name "failure") (req "^0.1.5") (default-features #t) (kind 0)))) (hash "0d9dgk8p620v7jlh3q94ks365h3xhccjg6pazixjrkb6qak1p9pd")))

(define-public crate-mp3-duration-0.1 (crate (name "mp3-duration") (vers "0.1.10") (deps (list (crate-dep (name "thiserror") (req "^1") (default-features #t) (kind 0)))) (hash "03qaz7yrh9ia4pd9pvs4nx1xhg3i2n448z5mwl0hhbsh01rxr2rl")))

(define-public crate-mp3-duration-sum-0.1 (crate (name "mp3-duration-sum") (vers "0.1.0") (deps (list (crate-dep (name "mp3-duration") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "rayon") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.2") (default-features #t) (kind 0)))) (hash "09lr4p9y642l1zbkkr6dq7ygfpx21rdxq5yr73czyxqyldz3iq5k")))

(define-public crate-mp3-duration-sum-0.1 (crate (name "mp3-duration-sum") (vers "0.1.1") (deps (list (crate-dep (name "mp3-duration") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "rayon") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.2") (default-features #t) (kind 0)))) (hash "171ciyvgmn43rkhqfjg9iy4wvsbmy6fgqz8i88sqqw065cpjw7k0")))

(define-public crate-mp3-folder-rename-0.1 (crate (name "mp3-folder-rename") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^2.33") (features (quote ("yaml"))) (default-features #t) (kind 0)) (crate-dep (name "clogger") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "exitcode") (req "^1.1.2") (default-features #t) (kind 0)) (crate-dep (name "id3") (req "^0.5.1") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.11") (default-features #t) (kind 0)) (crate-dep (name "mime_guess") (req "^2.0.3") (default-features #t) (kind 0)))) (hash "11gwyn15y6dnchwki3ndbi7jacv9bqrjzibm9ww57izz7mxfxszp")))

(define-public crate-mp3-metadata-0.1 (crate (name "mp3-metadata") (vers "0.1.0") (hash "1nsca034bqy6wgdwhpir06zzrlzn7xivhpvbjyk1iakvaqxdmp3n")))

(define-public crate-mp3-metadata-0.1 (crate (name "mp3-metadata") (vers "0.1.1") (hash "1xq84xalv0fhc5lqa3d3gzvwm1gckmv2aw9wsb8m660ykajqp729")))

(define-public crate-mp3-metadata-0.2 (crate (name "mp3-metadata") (vers "0.2.0") (hash "0d2q1paxqzb9hzfa30wrnw9amnqix3hbjahppv4srbw4l363p90z")))

(define-public crate-mp3-metadata-0.2 (crate (name "mp3-metadata") (vers "0.2.1") (hash "1ap7h76lxcyqnl25wmf7wipwzgczpp061rb0y1l7h6a3cl0hzswc")))

(define-public crate-mp3-metadata-0.2 (crate (name "mp3-metadata") (vers "0.2.2") (hash "0frpaqiyb5ayb43x2kcjkw3g4vxwsr04q5slba8fbw73psagwwaf")))

(define-public crate-mp3-metadata-0.2 (crate (name "mp3-metadata") (vers "0.2.3") (hash "1sqgrq7mr8jchb5r1qbwkb2wmdnf1g35l9asl61yqg7wywrcyq9g")))

(define-public crate-mp3-metadata-0.2 (crate (name "mp3-metadata") (vers "0.2.4") (hash "066afa0gm66zwj69xkpbnrm93gbs848p95adjskrj0c7hja2k9nw")))

(define-public crate-mp3-metadata-0.2 (crate (name "mp3-metadata") (vers "0.2.5") (hash "06a2acffv7xscsm9lr7g3067vmdwy8lm1h9q2w23wjxih2pk4iyx")))

(define-public crate-mp3-metadata-0.3 (crate (name "mp3-metadata") (vers "0.3.0") (deps (list (crate-dep (name "reqwest") (req "^0.7") (default-features #t) (kind 2)) (crate-dep (name "simplemad") (req "^0.8") (default-features #t) (kind 2)))) (hash "1pn78bzpl4w8c4bkbdjmy0k4gm0wlpjhq86i101451imd79g3daa")))

(define-public crate-mp3-metadata-0.3 (crate (name "mp3-metadata") (vers "0.3.1") (deps (list (crate-dep (name "reqwest") (req "^0.7") (default-features #t) (kind 2)) (crate-dep (name "simplemad") (req "^0.8") (default-features #t) (kind 2)))) (hash "1h3f5w2wr8rfdyal41wriycv48i7p5azxz33mnp1wbhiwckiyjnl")))

(define-public crate-mp3-metadata-0.3 (crate (name "mp3-metadata") (vers "0.3.2") (deps (list (crate-dep (name "reqwest") (req "^0.7") (default-features #t) (kind 2)) (crate-dep (name "simplemad") (req "^0.8") (default-features #t) (kind 2)))) (hash "1spidy6qrkwr3ybfnv63vc0qhcc8nsbbbbl35w84q0f2rlrznyaw")))

(define-public crate-mp3-metadata-0.3 (crate (name "mp3-metadata") (vers "0.3.3") (deps (list (crate-dep (name "reqwest") (req "^0.7") (default-features #t) (kind 2)) (crate-dep (name "simplemad") (req "^0.8") (default-features #t) (kind 2)))) (hash "16cn64br6c5q8zhb73bzx3ww0v5ay5q5v393c2zv09ipaz1rp5pb")))

(define-public crate-mp3-metadata-0.3 (crate (name "mp3-metadata") (vers "0.3.4") (deps (list (crate-dep (name "reqwest") (req "^0.11") (features (quote ("blocking"))) (default-features #t) (kind 2)) (crate-dep (name "simplemad") (req "^0.9") (default-features #t) (kind 2)))) (hash "1i4w9i3ljk79i5914a905walj3ril3iqrf39sw2q08fw52ia7sp4")))

