(define-module (crates-io mp cl) #:use-module (crates-io))

(define-public crate-mpcli-0.0.0 (crate (name "mpcli") (vers "0.0.0") (deps (list (crate-dep (name "clap") (req "^2.33.0") (default-features #t) (kind 0)) (crate-dep (name "codec") (req "^1.1.0") (features (quote ("derive"))) (kind 0) (package "parity-scale-codec")))) (hash "0qlx43a2hksg2mj6smzg14q1s03wlbb6mw512j9hpnshk929aw8f") (yanked #t)))

