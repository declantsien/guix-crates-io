(define-module (crates-io mp #{3c}#) #:use-module (crates-io))

(define-public crate-mp3cut-0.1 (crate (name "mp3cut") (vers "0.1.0") (deps (list (crate-dep (name "chrono") (req "^0.4.19") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^3.1.18") (features (quote ("cargo"))) (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "rmp3") (req "^0.3.1") (default-features #t) (kind 0)))) (hash "04wb095f431w57ygxjc53pipsl2pkh8msr7drkpas1gqv08cmrig")))

