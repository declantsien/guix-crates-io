(define-module (crates-io mp vi) #:use-module (crates-io))

(define-public crate-mpvi-0.1 (crate (name "mpvi") (vers "0.1.0") (deps (list (crate-dep (name "serde") (req "^1") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "serial_test") (req "^0") (default-features #t) (kind 0)) (crate-dep (name "strum") (req "^0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1") (features (quote ("net" "io-util" "sync" "rt" "macros" "time"))) (default-features #t) (kind 0)))) (hash "0zsp4256f663w4fshzxqyygqdcrmibq6732d9mr2s4wkw0xmh0l4")))

(define-public crate-mpvipc-1 (crate (name "mpvipc") (vers "1.0.0") (deps (list (crate-dep (name "serde") (req "^1.0.1") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.0") (default-features #t) (kind 0)))) (hash "0qj92jbxj32dwr6k7zxvc3hnjc448w3802yj84lh4idsyk21hr8m")))

(define-public crate-mpvipc-1 (crate (name "mpvipc") (vers "1.1.0") (deps (list (crate-dep (name "serde") (req "^1.0.1") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.0") (default-features #t) (kind 0)))) (hash "1d3axafp92mdkb2wv2klyrly317ca7nzcc4paijp03rpsk3hiq4k")))

(define-public crate-mpvipc-1 (crate (name "mpvipc") (vers "1.1.1") (deps (list (crate-dep (name "serde") (req "^1.0.1") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.0") (default-features #t) (kind 0)))) (hash "0j5ry6sdvap3jys1vdsbmiyqq835c0q26hx9mcpgzbln65rbih55")))

(define-public crate-mpvipc-1 (crate (name "mpvipc") (vers "1.1.2") (deps (list (crate-dep (name "serde") (req "^1.0.1") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.0") (default-features #t) (kind 0)))) (hash "0p57xq46yppkx48q984ydq74zcvwrmmclvr4hccqgxny29z3kk62")))

(define-public crate-mpvipc-1 (crate (name "mpvipc") (vers "1.1.3") (deps (list (crate-dep (name "serde") (req "^1.0.1") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.0") (default-features #t) (kind 0)))) (hash "1ilz5l2j3j1q3kjk5z0xj4ygcz9f8lim0bq74c479dflim33lpkb")))

(define-public crate-mpvipc-1 (crate (name "mpvipc") (vers "1.1.4") (deps (list (crate-dep (name "serde") (req "^1.0.1") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.0") (default-features #t) (kind 0)))) (hash "1575jbicagca5akzqri8dxa51k59m7k9zrcbl9ffmsyfdp8f44vb")))

(define-public crate-mpvipc-1 (crate (name "mpvipc") (vers "1.1.5") (deps (list (crate-dep (name "serde") (req "^1.0.1") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.0") (default-features #t) (kind 0)))) (hash "0wi2zyal0g19fj5s7h6769h18spsjin1fzl78fsfz7p1zvmhl912")))

(define-public crate-mpvipc-1 (crate (name "mpvipc") (vers "1.1.6") (deps (list (crate-dep (name "env_logger") (req "^0.6.1") (default-features #t) (kind 2)) (crate-dep (name "log") (req "^0.4.6") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.1") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.0") (default-features #t) (kind 0)))) (hash "0amr13grvcyf1is5kl02yghrlqffmjvzwcf6wy91lmcnj6yinsa8")))

(define-public crate-mpvipc-1 (crate (name "mpvipc") (vers "1.1.7") (deps (list (crate-dep (name "env_logger") (req "^0.6.1") (default-features #t) (kind 2)) (crate-dep (name "log") (req "^0.4.6") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.1") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.0") (default-features #t) (kind 0)))) (hash "11qmnlvgai62hykj00qvyw5d13q12lnxda6kqp73r7r7vi9778c1")))

(define-public crate-mpvipc-1 (crate (name "mpvipc") (vers "1.1.8") (deps (list (crate-dep (name "env_logger") (req "^0.6.1") (default-features #t) (kind 2)) (crate-dep (name "log") (req "^0.4.6") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.1") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.0") (default-features #t) (kind 0)))) (hash "1d9akbqkph0yimlygmw6mwcjy2aafhiwwdwybyyj58hd756wwc6p")))

(define-public crate-mpvipc-1 (crate (name "mpvipc") (vers "1.1.9") (deps (list (crate-dep (name "env_logger") (req "^0.6.1") (default-features #t) (kind 2)) (crate-dep (name "log") (req "^0.4.6") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.1") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.0") (default-features #t) (kind 0)))) (hash "1famqjzgsb5kh41zk1p6wl8c7hp4y5gcx2zxwds5941i8yabvnn4")))

(define-public crate-mpvipc-1 (crate (name "mpvipc") (vers "1.2.0") (deps (list (crate-dep (name "env_logger") (req "^0.9.0") (default-features #t) (kind 2)) (crate-dep (name "log") (req "^0.4.17") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.138") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.82") (default-features #t) (kind 0)))) (hash "134r3fzi1dwd2c1iw43nysk3z2wjipahl6jm2g2c83gwzhghgpv4")))

(define-public crate-mpvipc-1 (crate (name "mpvipc") (vers "1.2.1") (deps (list (crate-dep (name "env_logger") (req "^0.9.0") (default-features #t) (kind 2)) (crate-dep (name "log") (req "^0.4.17") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.138") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.82") (default-features #t) (kind 0)))) (hash "0i4qabwznxhqsyhzkb99mz8rjximmbqfl9p5yqr0bxviv40i7zcb")))

(define-public crate-mpvipc-1 (crate (name "mpvipc") (vers "1.2.2") (deps (list (crate-dep (name "env_logger") (req "^0.9.0") (default-features #t) (kind 2)) (crate-dep (name "log") (req "^0.4.17") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.140") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.82") (default-features #t) (kind 0)))) (hash "18v1a0zc4xa917mvj9x6vffb0jv81icpv9j4k8a8n7g8z7yba368")))

(define-public crate-mpvipc-1 (crate (name "mpvipc") (vers "1.3.0") (deps (list (crate-dep (name "env_logger") (req "^0.10.0") (default-features #t) (kind 2)) (crate-dep (name "log") (req "^0.4.19") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.104") (default-features #t) (kind 0)))) (hash "1gg2s7nk34pr3rc9s0ppkx4j8ldfmf713fd34gqm9fz571li64if")))

