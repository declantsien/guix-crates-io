(define-module (crates-io mp mf) #:use-module (crates-io))

(define-public crate-mpmfnum-0.1 (crate (name "mpmfnum") (vers "0.1.0") (deps (list (crate-dep (name "gmp-mpfr-sys") (req "~1.6") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2.17") (default-features #t) (kind 0)) (crate-dep (name "rug") (req "^1.22.0") (features (quote ("num-traits"))) (default-features #t) (kind 0)))) (hash "0r86gyf76ragyi7vmf1lkkkkxhbyb7rq9dl1cpylld71ldzdpqm6")))

(define-public crate-mpmfnum-0.2 (crate (name "mpmfnum") (vers "0.2.0") (deps (list (crate-dep (name "gmp-mpfr-sys") (req "~1.6") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2.17") (default-features #t) (kind 0)) (crate-dep (name "rug") (req "^1.22.0") (features (quote ("num-traits"))) (default-features #t) (kind 0)))) (hash "01mn7s31bgx05yw5nzdz8xjgmnndshin76c0bq25j7xia6g7vzaf")))

