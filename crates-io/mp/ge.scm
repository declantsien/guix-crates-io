(define-module (crates-io mp ge) #:use-module (crates-io))

(define-public crate-mpgen-0.1 (crate (name "mpgen") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^4.1.12") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)) (crate-dep (name "strum_macros") (req "^0.24.3") (default-features #t) (kind 0)))) (hash "1ci0ri8q9d96qdd7q4p9wa9mbkr79x3igr30lfbg7v1pyh85gdca")))

(define-public crate-mpgen-0.1 (crate (name "mpgen") (vers "0.1.1") (deps (list (crate-dep (name "clap") (req "^4.1.12") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)) (crate-dep (name "strum_macros") (req "^0.24.3") (default-features #t) (kind 0)))) (hash "16yl2gb1mci2nsa8v7yssq2dfkjb6b9sw5rxw817nm8rmd080h4h")))

(define-public crate-mpgen-0.1 (crate (name "mpgen") (vers "0.1.2") (deps (list (crate-dep (name "clap") (req "^4.1.12") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)) (crate-dep (name "strum_macros") (req "^0.24.3") (default-features #t) (kind 0)))) (hash "1zm8c9iknijvldyxhbn89gbw71y2bzmsqb6zwqyv8v1gmb1rdaac")))

