(define-module (crates-io mp ds) #:use-module (crates-io))

(define-public crate-mpdsh-0.1 (crate (name "mpdsh") (vers "0.1.0") (deps (list (crate-dep (name "getopts") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "rustyline") (req "^6.2.0") (default-features #t) (kind 0)) (crate-dep (name "rustyline-derive") (req "^0.3.1") (default-features #t) (kind 0)) (crate-dep (name "shell-words") (req "^1.0.0") (default-features #t) (kind 0)) (crate-dep (name "wildmatch") (req "^1.0.10") (default-features #t) (kind 0)))) (hash "0dibxaq9m1lssz4ph4swz8sw8g7964y9kgvawld2ainczlgq0zpc")))

