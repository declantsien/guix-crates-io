(define-module (crates-io mp oo) #:use-module (crates-io))

(define-public crate-mpool-0.1 (crate (name "mpool") (vers "0.1.0") (deps (list (crate-dep (name "async-trait") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^0.2") (features (quote ("rt-core" "time"))) (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^0.2") (features (quote ("full"))) (default-features #t) (kind 2)))) (hash "10zzi38iwggbcqi3yaqaqrpiaij13m4wzrrrlcacyrcvhz39xxvj")))

(define-public crate-mpool-0.1 (crate (name "mpool") (vers "0.1.1") (deps (list (crate-dep (name "async-trait") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^0.2") (features (quote ("rt-core" "time"))) (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^0.2") (features (quote ("full"))) (default-features #t) (kind 2)))) (hash "160w7yf6vldzs3h33dmk6ymnzyz85dkdcd0y5696312gx924cnax")))

(define-public crate-mpool-0.1 (crate (name "mpool") (vers "0.1.2") (deps (list (crate-dep (name "async-trait") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^0.2") (features (quote ("rt-core" "time"))) (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^0.2") (features (quote ("full"))) (default-features #t) (kind 2)))) (hash "1v08a20ffz9xgphfw6xfkggk4xiggw6axpknbkhwj1g0pyd20dyc")))

(define-public crate-mpool-0.1 (crate (name "mpool") (vers "0.1.3") (deps (list (crate-dep (name "async-trait") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^0.2") (features (quote ("rt-core" "time"))) (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^0.2") (features (quote ("full"))) (default-features #t) (kind 2)))) (hash "11sjj7a7gg4v5ggzanw46rdbk0drl95zbhmyf6r2cp7gxf6ahg47")))

(define-public crate-mpool-0.1 (crate (name "mpool") (vers "0.1.4") (deps (list (crate-dep (name "async-trait") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^0.2") (features (quote ("rt-core" "time"))) (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^0.2") (features (quote ("full"))) (default-features #t) (kind 2)))) (hash "1nwk6b6a323441n6xwdsj9p8g372q86cc4i5x9h24fxpx43qk9gr")))

