(define-module (crates-io mp #{4-}#) #:use-module (crates-io))

(define-public crate-mp4-merge-0.1 (crate (name "mp4-merge") (vers "0.1.0") (deps (list (crate-dep (name "byteorder") (req "^1.4.3") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.17") (default-features #t) (kind 0)))) (hash "0xa2wh73z7m00nlbapd8xd888if8kcyc9af0qs6h4f7w3spwhsad")))

(define-public crate-mp4-merge-0.1 (crate (name "mp4-merge") (vers "0.1.1") (deps (list (crate-dep (name "byteorder") (req "^1.4.3") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.17") (default-features #t) (kind 0)))) (hash "0d2xxj920fz2bds19r73gv4s9r2yc6gzc0pwniv98rsgb20bylpf")))

(define-public crate-mp4-merge-0.1 (crate (name "mp4-merge") (vers "0.1.2") (deps (list (crate-dep (name "byteorder") (req "^1.4.3") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.17") (default-features #t) (kind 0)))) (hash "02q7bqpbygag2s82vds03ffmi4p9qv2vmr3gbgwlg5ym6c2xqw4a")))

(define-public crate-mp4-merge-0.1 (crate (name "mp4-merge") (vers "0.1.3") (deps (list (crate-dep (name "byteorder") (req "^1.4.3") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.17") (default-features #t) (kind 0)))) (hash "1agnaa72bzpmn0657wxvgnjbd6k0gf1v8f289r93yw195rqv00q0")))

(define-public crate-mp4-merge-0.1 (crate (name "mp4-merge") (vers "0.1.5") (deps (list (crate-dep (name "byteorder") (req "^1.4.3") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.17") (default-features #t) (kind 0)))) (hash "1g4mbipdw61rjbvyymj2a0rfar53vbn350ssv86w62fds73dxkds")))

(define-public crate-mp4-merge-0.1 (crate (name "mp4-merge") (vers "0.1.6") (deps (list (crate-dep (name "byteorder") (req "^1.4.3") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)))) (hash "1dsd18fc34k0ndmjyf49jqy5libx9cqibsmshz76y48mkbcxb1jc")))

(define-public crate-mp4-merge-0.1 (crate (name "mp4-merge") (vers "0.1.7") (deps (list (crate-dep (name "byteorder") (req "^1.4.3") (default-features #t) (kind 0)) (crate-dep (name "filetime_creation") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)))) (hash "1y0z51rfqq55iy490pxnbhahhqx2my2fh3hqk2whc583hr7cs902")))

(define-public crate-mp4-merge-0.1 (crate (name "mp4-merge") (vers "0.1.8") (deps (list (crate-dep (name "byteorder") (req "^1.5.0") (default-features #t) (kind 0)) (crate-dep (name "filetime_creation") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)))) (hash "1k1cnn8azm10p2gqr2ifnvpw17h4yp3b4wn7f52ijaxl57nbn5dj")))

(define-public crate-mp4-stream-0.1 (crate (name "mp4-stream") (vers "0.1.0") (deps (list (crate-dep (name "bitflags") (req "^1.3") (default-features #t) (kind 0)) (crate-dep (name "bmff") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "chrono") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "fixed") (req "^1.23") (default-features #t) (kind 0)) (crate-dep (name "flume") (req "^0.10") (default-features #t) (kind 0)) (crate-dep (name "futures-lite") (req "^1.12") (default-features #t) (kind 0)) (crate-dep (name "quick-error") (req "^2.0") (default-features #t) (kind 0)) (crate-dep (name "rscam") (req "^0.5") (features (quote ("no_wrapper"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde_repr") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "tracing") (req "^0.1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "x264") (req "^0.5") (default-features #t) (kind 0)))) (hash "1abbvwhw3466xz8xivj01nm7nqmlhvx59bj9a80crzs2j0bi837d") (rust-version "1.66")))

