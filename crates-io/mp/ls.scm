(define-module (crates-io mp ls) #:use-module (crates-io))

(define-public crate-mpls-0.1 (crate (name "mpls") (vers "0.1.0") (deps (list (crate-dep (name "nom") (req "^5.1.1") (default-features #t) (kind 0)) (crate-dep (name "version-sync") (req "^0.9.0") (default-features #t) (kind 2)))) (hash "1lnk0fkcghxmzx1y5czv29bzhxx54j1jykr3w8jb6jy5vplgz7hp")))

(define-public crate-mpls-0.2 (crate (name "mpls") (vers "0.2.0") (deps (list (crate-dep (name "nom") (req "^5.1.1") (default-features #t) (kind 0)) (crate-dep (name "version-sync") (req "^0.9.0") (default-features #t) (kind 2)))) (hash "0ij7cc4689fjp3slfvd5xyl2s8apyxs7xrvmk3wy14xv3jm6h1ma")))

