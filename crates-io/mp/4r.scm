(define-module (crates-io mp #{4r}#) #:use-module (crates-io))

(define-public crate-mp4ra-rust-0.1 (crate (name "mp4ra-rust") (vers "0.1.0") (deps (list (crate-dep (name "four-cc") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "09kns5klmw1qh21n0k3pb1mf84gl82x64z4lc8lq9wrvnh1sz7dy")))

(define-public crate-mp4ra-rust-0.2 (crate (name "mp4ra-rust") (vers "0.2.0") (deps (list (crate-dep (name "four-cc") (req "^0.3.0") (default-features #t) (kind 0)))) (hash "1i80yrblqg447c0g7wi1sbsywm0y1vrfrpps8sp6i4nyj6ahvkzi")))

(define-public crate-mp4ra-rust-0.3 (crate (name "mp4ra-rust") (vers "0.3.0") (deps (list (crate-dep (name "four-cc") (req "^0.4.0") (default-features #t) (kind 0)))) (hash "1drnlm24isb9fanwjg8s8lc5lb6xydk2wj3hcan6cp88cww3vg7x")))

