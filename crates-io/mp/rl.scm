(define-module (crates-io mp rl) #:use-module (crates-io))

(define-public crate-mprls-0.1 (crate (name "mprls") (vers "0.1.0") (deps (list (crate-dep (name "embedded-hal") (req "^0.2.5") (default-features #t) (kind 0)) (crate-dep (name "linux-embedded-hal") (req "^0.3.0") (default-features #t) (kind 0)))) (hash "1k3y0f9d0jx3d2g559zmch8jnwxqn7z736hn6aj5nycng0nj7w14")))

