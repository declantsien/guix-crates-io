(define-module (crates-io pi c8) #:use-module (crates-io))

(define-public crate-pic8259-0.10 (crate (name "pic8259") (vers "0.10.0") (deps (list (crate-dep (name "x86_64") (req "^0.14.2") (features (quote ("instructions" "inline_asm"))) (kind 0)))) (hash "0dxqf2r6rxh9kc7gnfmjzrxvabzvgql4hji69b6m6a8aqmjplvfc")))

(define-public crate-pic8259-0.10 (crate (name "pic8259") (vers "0.10.1") (deps (list (crate-dep (name "x86_64") (req "^0.14.2") (features (quote ("instructions" "inline_asm"))) (kind 0)))) (hash "18cimh2hnwngc1ifp8srr482vgv8akkl391snzww0czfhc6r5k08")))

(define-public crate-pic8259-0.10 (crate (name "pic8259") (vers "0.10.2") (deps (list (crate-dep (name "x86_64") (req "^0.14.2") (features (quote ("instructions"))) (kind 0)))) (hash "1792wrssflh66rbc2yh35i8rn1m7lhf087czcja6xqg22ksj3v14") (features (quote (("stable" "x86_64/external_asm") ("nightly" "x86_64/inline_asm") ("default" "nightly"))))))

(define-public crate-pic8259-0.10 (crate (name "pic8259") (vers "0.10.3") (deps (list (crate-dep (name "x86_64") (req "^0.14.2") (features (quote ("instructions"))) (kind 0)))) (hash "1j49q76s0s0x51c2h4rpw879pnazvw4jclaaazdw7h0gghc947kn") (features (quote (("stable" "x86_64/external_asm") ("nightly" "x86_64/inline_asm") ("default" "nightly"))))))

(define-public crate-pic8259-0.10 (crate (name "pic8259") (vers "0.10.4") (deps (list (crate-dep (name "x86_64") (req "^0.14.2") (features (quote ("instructions"))) (kind 0)))) (hash "157183xan3mwg2rk52gi8qw91z1v267p71c6jcbhn7nv05dlp16b") (features (quote (("stable" "x86_64/external_asm") ("nightly" "x86_64/inline_asm") ("default" "nightly"))))))

(define-public crate-pic8259-0.11 (crate (name "pic8259") (vers "0.11.0") (deps (list (crate-dep (name "x86_64") (req "^0.15.0") (features (quote ("instructions"))) (kind 0)))) (hash "08nv6dyb49rp4329nq344ndiivsxhmkgvrs7grsmy5ib55nainb2") (features (quote (("stable") ("nightly") ("default" "nightly"))))))

(define-public crate-pic8259_simple-0.1 (crate (name "pic8259_simple") (vers "0.1.0") (deps (list (crate-dep (name "cpuio") (req "^0.2.0") (default-features #t) (kind 0)))) (hash "0q9dmmm53z35gq19ap6h1yq2hzwswyvymqf3q29mn80dzj8n38d0")))

(define-public crate-pic8259_simple-0.1 (crate (name "pic8259_simple") (vers "0.1.1") (deps (list (crate-dep (name "cpuio") (req "^0.2.0") (default-features #t) (kind 0)))) (hash "0wjwlcds67wxh5mk6k3d78m7sp9qg43bxnkc3d9ai3c223yv4r6w")))

(define-public crate-pic8259_simple-0.2 (crate (name "pic8259_simple") (vers "0.2.0") (deps (list (crate-dep (name "cpuio") (req "^0.3.0") (default-features #t) (kind 0)))) (hash "0lp9kgqaihsaqbd3xwh3br2fyxcd4gzpsyzn2n0byncfzfbm8amg")))

(define-public crate-pic8259_x86-0.1 (crate (name "pic8259_x86") (vers "0.1.0") (hash "1w9crd31ks0m06rp33a0h391igdsrbqi7m7hppx89wh2gp1pqggk")))

