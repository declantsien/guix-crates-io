(define-module (crates-io pi rc) #:use-module (crates-io))

(define-public crate-pircolate-0.1 (crate (name "pircolate") (vers "0.1.0") (deps (list (crate-dep (name "error-chain") (req "^0.8") (default-features #t) (kind 0)))) (hash "06ngkhykkv7yflqbs5g2fs4vx3m4hcrs90q45yzbjz9iajad1kbx")))

(define-public crate-pircolate-0.2 (crate (name "pircolate") (vers "0.2.0") (deps (list (crate-dep (name "error-chain") (req "^0.10") (default-features #t) (kind 0)))) (hash "13v5h1nriddv4r8707rq3hwazc5x4x3825f43hmf0n2dzn2783i0")))

(define-public crate-pircolate-0.2 (crate (name "pircolate") (vers "0.2.1") (deps (list (crate-dep (name "error-chain") (req "^0.10") (default-features #t) (kind 0)))) (hash "1jlxxgh132i6abf5py84ibdp5m9yinldpb0xrvxla0la2bs0z3vf")))

