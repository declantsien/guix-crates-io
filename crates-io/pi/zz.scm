(define-module (crates-io pi zz) #:use-module (crates-io))

(define-public crate-pizza-0.0.0 (crate (name "pizza") (vers "0.0.0") (hash "0l41ajxyjw2wzxhsshlahcbv4i9z6j606ia5y54j7vnd430j23v0")))

(define-public crate-pizza-0.1 (crate (name "pizza") (vers "0.1.3") (hash "1yza856g48vr1xny9d5j5isn4r89dgs7871p3r9rv79nanjsrs64")))

(define-public crate-pizzicato-0.1 (crate (name "pizzicato") (vers "0.1.0") (deps (list (crate-dep (name "anyhow") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "image") (req "^0.23") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "noise") (req "^0.7") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.8") (default-features #t) (kind 2)) (crate-dep (name "rsmpeg") (req "^0.7") (default-features #t) (kind 0)) (crate-dep (name "strum") (req "^0.23") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "0cm4g7as5bv2g1x93cxxhw4f423lk7bsw0kzsfn54zb8rd9vhiby")))

