(define-module (crates-io pi gl) #:use-module (crates-io))

(define-public crate-piglet-0.0.0 (crate (name "piglet") (vers "0.0.0") (deps (list (crate-dep (name "terminal") (req "^0.2.1") (default-features #t) (kind 0)))) (hash "0b0v44v43rah3hbqny0wfyh1gmi1igjaafn5hcikhk6p4wcmb3m7")))

(define-public crate-piglog-1 (crate (name "piglog") (vers "1.0.0") (deps (list (crate-dep (name "colored") (req "^2.0.4") (default-features #t) (kind 0)))) (hash "1i6d85pr1pbxpf87xg5bg3h3ip0hqh40aw4n2z26sxvm65dnikpr")))

(define-public crate-piglog-1 (crate (name "piglog") (vers "1.1.0") (deps (list (crate-dep (name "colored") (req "^2.0.4") (default-features #t) (kind 0)))) (hash "0w1igkqzd2rz8y0gxz4rrb65jf8sli06nh4x5mcv2gqqvhkydc10")))

(define-public crate-piglog-1 (crate (name "piglog") (vers "1.2.0") (deps (list (crate-dep (name "colored") (req "^2.0.4") (default-features #t) (kind 0)))) (hash "040rmfc1nsqfg5ig2fcpgiza8myi47634q08mhrlchmqr1xkixrc")))

(define-public crate-piglog-1 (crate (name "piglog") (vers "1.3.0") (deps (list (crate-dep (name "colored") (req "^2.0.4") (default-features #t) (kind 0)))) (hash "0a4z53va1b7ibvfgp3s7fpfcf3k4qia60q3hv7y56w8wcngn9yiz")))

(define-public crate-piglog-1 (crate (name "piglog") (vers "1.3.1") (deps (list (crate-dep (name "colored") (req "^2.0.4") (default-features #t) (kind 0)))) (hash "1xd1j7wcsbllg92nsl7z31vg1d8jpqrbhmni78l611gx2izymz9h")))

(define-public crate-piglog-1 (crate (name "piglog") (vers "1.3.2") (deps (list (crate-dep (name "colored") (req "^2.0.4") (default-features #t) (kind 0)))) (hash "01qzgff75668jpmabgzwzcq58k594cxvn4fa69rfpnk5rybnrygw")))

(define-public crate-piglog-1 (crate (name "piglog") (vers "1.4.0") (deps (list (crate-dep (name "clap") (req "^4.4.11") (features (quote ("derive" "std"))) (optional #t) (kind 0)) (crate-dep (name "colored") (req "^2.0.4") (default-features #t) (kind 0)))) (hash "0z879sb6aq65hin7qxq8xyg1clhvxzrq69bzfa41x1a083rdhcp2") (v 2) (features2 (quote (("clap_derive" "dep:clap"))))))

(define-public crate-piglog-1 (crate (name "piglog") (vers "1.4.1") (deps (list (crate-dep (name "clap") (req "^4.4.11") (features (quote ("derive" "std"))) (optional #t) (kind 0)) (crate-dep (name "colored") (req "^2.0.4") (default-features #t) (kind 0)))) (hash "05d8pagfi1g6z686kq1d9v753lg7qlab6ywb28a163n8cjmbq6c5") (v 2) (features2 (quote (("clap_derive" "dep:clap"))))))

(define-public crate-piglog-netget-config-1 (crate (name "piglog-netget-config") (vers "1.0.0") (deps (list (crate-dep (name "colored") (req "^2.0.4") (default-features #t) (kind 0)) (crate-dep (name "netget") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "0n9x3ngf0jjm2fmys1jg6z4acgbjyniiibl1m352zz287b96ngzj") (yanked #t)))

(define-public crate-piglog-netget-config-1 (crate (name "piglog-netget-config") (vers "1.0.1") (deps (list (crate-dep (name "colored") (req "^2.0.4") (default-features #t) (kind 0)) (crate-dep (name "netget") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "0aw7zgm9va6bngngfn22hwx7k45p9dz40qdir06jy4hywcwgzy4z")))

