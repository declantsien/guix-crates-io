(define-module (crates-io pi gp) #:use-module (crates-io))

(define-public crate-pigpio-0.1 (crate (name "pigpio") (vers "0.1.0") (deps (list (crate-dep (name "pigpio-sys") (req "^0.1") (default-features #t) (kind 0)))) (hash "0wbcalzfg5wc5izpb1ybvggrqp5j5pv1lhqgkxr66bscbw8mf0xg")))

(define-public crate-pigpio-sys-0.1 (crate (name "pigpio-sys") (vers "0.1.0") (deps (list (crate-dep (name "bindgen") (req "^0.42") (default-features #t) (kind 1)))) (hash "152a2anp0qa8h3rc6p17vmhv8kfa8l44zbpzr42126rr22l49d9n")))

(define-public crate-pigpio-sys-0.1 (crate (name "pigpio-sys") (vers "0.1.1") (deps (list (crate-dep (name "bindgen") (req "^0.49") (default-features #t) (kind 1)))) (hash "0f0crc7samqx6xf9lfmb6mqzk2lmf3vqfgb10m3kdbjl65jhrrd4")))

