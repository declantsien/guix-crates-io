(define-module (crates-io pi to) #:use-module (crates-io))

(define-public crate-PitosMarcianosSalamanca-0.1 (crate (name "PitosMarcianosSalamanca") (vers "0.1.0") (hash "0jzydb2705xd28y8pcdphdw6wnk3c8113y1rxzydrwb6zf0f7ync")))

(define-public crate-PitosMarcianosSalamanca-0.1 (crate (name "PitosMarcianosSalamanca") (vers "0.1.1") (hash "1bf0npj4bvcnmr7n5062wk10gk2lkmyc9dvxlrq64r06g12nhmza")))

(define-public crate-pitot-0.0.1 (crate (name "pitot") (vers "0.0.1") (deps (list (crate-dep (name "chrono") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "env_logger") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "inotify") (req "^0.4.1") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2.23") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.3.7") (default-features #t) (kind 0)) (crate-dep (name "nom") (req "^3.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serial") (req "^0.3.4") (default-features #t) (kind 0)) (crate-dep (name "time") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "ws") (req "^0.7.1") (default-features #t) (kind 0)))) (hash "1azm07sgwpqwhmigk15drvqrlyj2z8pdpa21iw3mpv9hd5inw4s3")))

(define-public crate-pitou-0.0.1 (crate (name "pitou") (vers "0.0.1") (hash "1vjqzkbyfi9p5kfw5bdilwdm4jp0d0jl5yl8b139hby5xmq47pch")))

