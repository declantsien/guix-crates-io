(define-module (crates-io pi xw) #:use-module (crates-io))

(define-public crate-pixwrapper-0.1 (crate (name "pixwrapper") (vers "0.1.0") (deps (list (crate-dep (name "bindgen") (req "^0.54.0") (default-features #t) (kind 1)) (crate-dep (name "cc") (req "^1.0.54") (default-features #t) (kind 1)))) (hash "1b9pdf9ildw1ajb232wwwkzvkxknnjb624gdjygr3jh2bacsdy46")))

(define-public crate-pixwrapper-0.1 (crate (name "pixwrapper") (vers "0.1.1") (deps (list (crate-dep (name "bindgen") (req "^0.54.0") (default-features #t) (kind 1)) (crate-dep (name "cc") (req "^1.0.54") (default-features #t) (kind 1)))) (hash "0dpm2ygcmcyv415f730aprvkl750kzybr9ds80fp85ya265ayk4x")))

(define-public crate-pixwrapper-0.1 (crate (name "pixwrapper") (vers "0.1.2") (deps (list (crate-dep (name "bindgen") (req "^0.54.0") (default-features #t) (kind 1)) (crate-dep (name "cc") (req "^1.0.54") (default-features #t) (kind 1)))) (hash "0x5amkld3r66djkvlyprci8wbdq8xg62rdllx21y4n8mg3m6hxgg")))

(define-public crate-pixwrapper-0.1 (crate (name "pixwrapper") (vers "0.1.3") (deps (list (crate-dep (name "bindgen") (req "^0.57") (default-features #t) (kind 1)) (crate-dep (name "cc") (req "^1.0.54") (default-features #t) (kind 1)))) (hash "04if56d8h6v49ny5gdzmn2z090ggywhngg4nxyhlzn5pf5rm2b3n")))

