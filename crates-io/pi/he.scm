(define-module (crates-io pi he) #:use-module (crates-io))

(define-public crate-pihex-0.1 (crate (name "pihex") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^2.21") (default-features #t) (kind 0)))) (hash "0n811qw9vibk5ng6w9jhshn4al9djnwm1h8zmnmhd6pxfvz1i8wx")))

(define-public crate-pihex-0.1 (crate (name "pihex") (vers "0.1.1") (deps (list (crate-dep (name "clap") (req "^2.21") (default-features #t) (kind 0)))) (hash "1gaa20mv8q7vdv17b4y2krp4cf1swws17idjimdgidh8l36icd5g")))

(define-public crate-pihex-0.1 (crate (name "pihex") (vers "0.1.2") (deps (list (crate-dep (name "clap") (req "^2.23.0") (default-features #t) (kind 0)))) (hash "1r91382djmaf5lkxrh9y2a7m3a9g021ilvra6z8j4pb2593zqfxx")))

(define-public crate-pihex-0.1 (crate (name "pihex") (vers "0.1.3") (deps (list (crate-dep (name "clap") (req "^2.23.0") (default-features #t) (kind 0)))) (hash "1k9a42p6b2pi1ljwyl6xdvnf5120w5nnfzy6zhl13cpp4psi5shr")))

(define-public crate-pihex-0.1 (crate (name "pihex") (vers "0.1.4") (deps (list (crate-dep (name "clap") (req "^2.33.0") (default-features #t) (kind 0)))) (hash "04yq1pap43vc68kv2cqb4gawdxq1rdq5wbqyrjfi5gcpsddy95fh")))

(define-public crate-pihex-0.1 (crate (name "pihex") (vers "0.1.5") (deps (list (crate-dep (name "clap") (req "^2.33.3") (default-features #t) (kind 0)))) (hash "1q5h85s790z5vxwidj00g86m174y2n80095ksqr0rmrk01qgp9rq")))

(define-public crate-pihex-0.1 (crate (name "pihex") (vers "0.1.6") (deps (list (crate-dep (name "clap") (req "^4.4.8") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.50") (default-features #t) (kind 0)))) (hash "0lgzb26xcwwh0a5m348byx9frmsr94ikvncykwm1phjwxy4xhgrz")))

(define-public crate-pihex-0.1 (crate (name "pihex") (vers "0.1.7") (deps (list (crate-dep (name "clap") (req "^4.4.8") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.50") (default-features #t) (kind 0)))) (hash "1ra01zfjvmkgk00vgh4pw4b5habhw6yai2hxsw3i59ydmpkxiyc2")))

(define-public crate-pihex-0.1 (crate (name "pihex") (vers "0.1.8") (deps (list (crate-dep (name "clap") (req "^4.5.4") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.58") (default-features #t) (kind 0)))) (hash "10fif9gjr6ys7dzb5492gg9s70carh7jjjjzqaspyx2v3xl0w7ah")))

