(define-module (crates-io pi az) #:use-module (crates-io))

(define-public crate-piazin-grrs-0.1 (crate (name "piazin-grrs") (vers "0.1.0") (deps (list (crate-dep (name "anyhow") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "assert_cmd") (req "^2.0.14") (default-features #t) (kind 2)) (crate-dep (name "assert_fs") (req "^1.1.1") (default-features #t) (kind 2)) (crate-dep (name "clap") (req "^4.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "predicates") (req "^3.1.0") (default-features #t) (kind 2)))) (hash "021cw2d8w2ny0jfpjd0kpi8695ga3ljwkhpswji538ziqfm0d4rc")))

