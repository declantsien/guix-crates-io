(define-module (crates-io pi ce) #:use-module (crates-io))

(define-public crate-picea-0.1 (crate (name "picea") (vers "0.1.0") (deps (list (crate-dep (name "picea-macro-tools") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 2)) (crate-dep (name "serde") (req "^1.0.158") (features (quote ("derive"))) (default-features #t) (kind 2)) (crate-dep (name "serde_json") (req "^1.0.113") (default-features #t) (kind 2)) (crate-dep (name "speedy2d") (req "^2.0.0") (default-features #t) (kind 2)))) (hash "06m4ih0si5wwy37aj4ndmvalxqfhgsw6m8rn17yvdb3lg19h561g")))

(define-public crate-picea-macro-tools-0.1 (crate (name "picea-macro-tools") (vers "0.1.0") (deps (list (crate-dep (name "quote") (req "^1.0.35") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2.0.55") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "0ghd0pfaqbsf3j2imji7wk50p87s15h1x20vkmk9m6mpwr2z1rgx")))

