(define-module (crates-io pi pv) #:use-module (crates-io))

(define-public crate-pipvwmdrn-0.1 (crate (name "pipvwmdrn") (vers "0.1.3") (deps (list (crate-dep (name "clap") (req "^4.4.6") (default-features #t) (kind 0)) (crate-dep (name "crossbeam") (req "^0.8.2") (default-features #t) (kind 0)) (crate-dep (name "crossterm") (req "^0.27.0") (default-features #t) (kind 0)))) (hash "1gnzfjlkwmnwjyff39x2a5b49xphwdxa9swbf3d9zxw74m95dbha")))

(define-public crate-pipvwmdrn-0.1 (crate (name "pipvwmdrn") (vers "0.1.31") (deps (list (crate-dep (name "clap") (req "^4.4.6") (default-features #t) (kind 0)) (crate-dep (name "crossbeam") (req "^0.8.2") (default-features #t) (kind 0)) (crate-dep (name "crossterm") (req "^0.27.0") (default-features #t) (kind 0)))) (hash "00frbmka36b959022xlmw714b7njl49s3vgdq3aipg3s1f53c7m5")))

