(define-module (crates-io pi ro) #:use-module (crates-io))

(define-public crate-pirox-0.0.2 (crate (name "pirox") (vers "0.0.2") (deps (list (crate-dep (name "string-builder") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "wasm-bindgen") (req "^0.2.87") (default-features #t) (kind 0)))) (hash "1mr936n421fnspdav3lv1wf681ca8mqjm1h3x0c7kbpil818vqyf")))

(define-public crate-pirox-0.0.3 (crate (name "pirox") (vers "0.0.3") (deps (list (crate-dep (name "string-builder") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "wasm-bindgen") (req "^0.2.87") (default-features #t) (kind 0)))) (hash "09iakl8y7zckh9hn0f740dfgnd77xpy6n9h3950gj7bafih459v0")))

