(define-module (crates-io pi xl) #:use-module (crates-io))

(define-public crate-pixls_grep_rust-0.1 (crate (name "pixls_grep_rust") (vers "0.1.0") (deps (list (crate-dep (name "anyhow") (req "^1.0.75") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^4.4.10") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "env_logger") (req "^0.10.1") (default-features #t) (kind 0)) (crate-dep (name "indicatif") (req "^0.17.7") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.20") (default-features #t) (kind 0)))) (hash "1b5m6agi5dnwh4kdcn3mim6j1ymrn6dfd9jahh5h9zidgbgdjgsx")))

(define-public crate-pixlzr-0.1 (crate (name "pixlzr") (vers "0.1.0") (deps (list (crate-dep (name "image") (req "^0.24") (default-features #t) (kind 0)))) (hash "1hpwhxdxyb818rrmsgqxjhbw4c80kbcw7jcv8ps7l6zcagr5l3ml")))

(define-public crate-pixlzr-0.1 (crate (name "pixlzr") (vers "0.1.1") (deps (list (crate-dep (name "image") (req "^0.24") (default-features #t) (kind 0)))) (hash "08ff132r01jsfbf0qwm1d1ilxw50w29sgx2i5x1xzgi6x81la34k")))

(define-public crate-pixlzr-0.2 (crate (name "pixlzr") (vers "0.2.0") (deps (list (crate-dep (name "image") (req "^0.24") (default-features #t) (kind 0)))) (hash "00z6rq0bd7p19kpg2hbywx7fywxajq6fxrj50vawx9bs3xgra5i1")))

(define-public crate-pixlzr-0.2 (crate (name "pixlzr") (vers "0.2.1") (deps (list (crate-dep (name "image") (req "^0.24") (default-features #t) (kind 0)))) (hash "0fplyidrb42z51hn79bi7c0xyr7b64cjyyvv4i0krkyny65n4n8q")))

(define-public crate-pixlzr-0.2 (crate (name "pixlzr") (vers "0.2.2") (deps (list (crate-dep (name "anyhow") (req "^1.0.69") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^4.0") (features (quote ("derive" "cargo"))) (default-features #t) (kind 0)) (crate-dep (name "criterion") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "image") (req "^0.24") (default-features #t) (kind 0)) (crate-dep (name "qoi") (req "^0.4") (default-features #t) (kind 0)))) (hash "1cwrf1pmwkpbyr3vncif6p9a6wnrpaw6zi95nq3dpai41cpg3pim")))

(define-public crate-pixlzr-0.3 (crate (name "pixlzr") (vers "0.3.0") (deps (list (crate-dep (name "anyhow") (req "^1.0.69") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^4.0") (features (quote ("derive" "cargo"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "criterion") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "fast_image_resize") (req "^2.7.3") (default-features #t) (kind 0)) (crate-dep (name "image") (req "^0.24") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "palette") (req "^0.7.3") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "phf") (req "^0.11.1") (features (quote ("macros"))) (default-features #t) (kind 0)) (crate-dep (name "qoi") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "rayon") (req "^1.8.0") (default-features #t) (kind 0)))) (hash "0mmipi29j0kb1inwiaj7ya9zp9qkh1ms0jh11335xcqvxrn5x47b") (features (quote (("default" "image-rs" "cli")))) (v 2) (features2 (quote (("image-rs" "dep:image" "dep:palette") ("cli" "dep:clap"))))))

