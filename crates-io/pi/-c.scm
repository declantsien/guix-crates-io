(define-module (crates-io pi -c) #:use-module (crates-io))

(define-public crate-pi-compression-3 (crate (name "pi-compression") (vers "3.1.4") (deps (list (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)))) (hash "0fv23dyz76qfb3l87lkplh7kh9ggpfdlg4w3wy4g1jpwmqqvvxwc")))

