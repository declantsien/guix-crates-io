(define-module (crates-io pi nb) #:use-module (crates-io))

(define-public crate-pinboard-0.9 (crate (name "pinboard") (vers "0.9.0") (deps (list (crate-dep (name "crossbeam") (req "^0.2.10") (default-features #t) (kind 0)))) (hash "18z10j47b7ingrixi5q6f9ijrj192y02p9dcbx5bqx160wgry7gy")))

(define-public crate-pinboard-0.10 (crate (name "pinboard") (vers "0.10.0") (deps (list (crate-dep (name "crossbeam") (req "^0.2.10") (default-features #t) (kind 0)))) (hash "00rc4iy15hi9msmhyfx2hyg2a2pwn9iliqfkahf5755mk4awb9wb")))

(define-public crate-pinboard-1 (crate (name "pinboard") (vers "1.0.0") (deps (list (crate-dep (name "crossbeam") (req "^0.2.10") (default-features #t) (kind 0)))) (hash "1b8mmrjbl87rgvmfn8hrsjbpfry5yf2yq2gcg2l554zq06ayqs7p")))

(define-public crate-pinboard-1 (crate (name "pinboard") (vers "1.1.0") (deps (list (crate-dep (name "crossbeam") (req "^0.2.10") (default-features #t) (kind 0)))) (hash "1xnhy9k0wy4y1wb22xnrv54xx35ky0yhkrxi417m69cfx694019w")))

(define-public crate-pinboard-1 (crate (name "pinboard") (vers "1.2.0") (deps (list (crate-dep (name "crossbeam") (req "^0.2.10") (default-features #t) (kind 0)))) (hash "0i73hff1278r48frprhg1z2p4xwig2wp0n4nymic6s4jixablcvi")))

(define-public crate-pinboard-1 (crate (name "pinboard") (vers "1.3.0") (deps (list (crate-dep (name "crossbeam") (req "^0.2.10") (default-features #t) (kind 0)))) (hash "0m9a9w5xx5bpd2fk52bwx7cing1akxra8pbqki9jbd5sl3knaymm")))

(define-public crate-pinboard-1 (crate (name "pinboard") (vers "1.4.0") (deps (list (crate-dep (name "crossbeam") (req "^0.3.0") (default-features #t) (kind 0)))) (hash "0j1v2fd8mq29ha68g0vpkjalvqfd6q1gi88an1i5ihifxl966w9r")))

(define-public crate-pinboard-1 (crate (name "pinboard") (vers "1.4.1") (deps (list (crate-dep (name "crossbeam") (req "^0.3.0") (default-features #t) (kind 0)))) (hash "1r13z50vj8g25ci85i79x7iydig51pwp33bbm2bd8w5n0k83vvs0")))

(define-public crate-pinboard-2 (crate (name "pinboard") (vers "2.0.0") (deps (list (crate-dep (name "crossbeam") (req "^0.3.0") (default-features #t) (kind 2)) (crate-dep (name "crossbeam-epoch") (req "^0.2.0") (default-features #t) (kind 0)))) (hash "0byrslg1ccmnirzz97f0b70wkkzfvdwf44x1w8gihr73gzqsig6l")))

(define-public crate-pinboard-2 (crate (name "pinboard") (vers "2.0.1") (deps (list (crate-dep (name "crossbeam") (req "^0.6.0") (default-features #t) (kind 2)) (crate-dep (name "crossbeam-epoch") (req "^0.7.0") (default-features #t) (kind 0)))) (hash "12j7palhgpv45m1ycn00zys1a9p3hy65113bckfc36avbn2600n0")))

(define-public crate-pinboard-2 (crate (name "pinboard") (vers "2.1.0") (deps (list (crate-dep (name "crossbeam") (req "^0.8.0") (default-features #t) (kind 2)) (crate-dep (name "crossbeam-epoch") (req "^0.9.0") (default-features #t) (kind 0)))) (hash "18v22rrf447050qarh6wknga119id3bhwazjr1ywvm4wxr7340k5")))

(define-public crate-pinboard-2 (crate (name "pinboard") (vers "2.2.0") (deps (list (crate-dep (name "crossbeam") (req "^0.8.0") (default-features #t) (kind 2)) (crate-dep (name "crossbeam-epoch") (req "^0.9.0") (default-features #t) (kind 0)))) (hash "17kk597byzimyh2kp4871148r60502mif2bd9s9jnx4spmngim8z")))

