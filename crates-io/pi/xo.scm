(define-module (crates-io pi xo) #:use-module (crates-io))

(define-public crate-pixops-0.0.0 (crate (name "pixops") (vers "0.0.0") (deps (list (crate-dep (name "criterion") (req "^0.2") (default-features #t) (kind 2)) (crate-dep (name "pix") (req "^0.4") (default-features #t) (kind 0)))) (hash "01vrpxlqg4ic9v0w6f7jfrvyxdf6k6ycpqz7z6vqk65wqv3gymqk") (features (quote (("use-simd") ("default" "use-simd"))))))

(define-public crate-pixops-0.0.1 (crate (name "pixops") (vers "0.0.1") (deps (list (crate-dep (name "criterion") (req "^0.2") (default-features #t) (kind 2)) (crate-dep (name "pix") (req "^0.6") (default-features #t) (kind 0)))) (hash "0hihx1fqyg6lixdw8j1295ah0rv66vsc5ks8vmzhyqgl86yjngr9") (features (quote (("use-simd") ("default" "use-simd"))))))

(define-public crate-pixops-0.0.2 (crate (name "pixops") (vers "0.0.2") (deps (list (crate-dep (name "criterion") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "pix") (req "^0.7.0") (default-features #t) (kind 0)))) (hash "0shdwhbb8sc0bmxrj19f5hpksw0gd80g0bv8h1i6lgiswvics3dn") (features (quote (("simd") ("default" "simd"))))))

(define-public crate-pixops-0.1 (crate (name "pixops") (vers "0.1.0") (deps (list (crate-dep (name "criterion") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "pix") (req "^0.8.0") (default-features #t) (kind 0)))) (hash "0slbfaydplmwf1i1cv6741xbalwzmmy46wq6q9f6k4957dpyw9jy") (features (quote (("simd") ("default" "simd"))))))

