(define-module (crates-io pi an) #:use-module (crates-io))

(define-public crate-piano-rs-0.1 (crate (name "piano-rs") (vers "0.1.0") (deps (list (crate-dep (name "rodio") (req "^0.5") (default-features #t) (kind 0)) (crate-dep (name "rustbox") (req "^0.9") (default-features #t) (kind 0)))) (hash "1dqkrm2lxaa2iz32c792njp1cb6fa86gs2kyasrmjha6yczmalp0")))

(define-public crate-piano_keyboard-0.1 (crate (name "piano_keyboard") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^2.32") (default-features #t) (kind 2)) (crate-dep (name "png") (req "^0.14.0") (default-features #t) (kind 2)))) (hash "1n663cv9x8sx89xsp0ysyc21hz3d8p637ynyc495gc1s8xs5wmcf")))

(define-public crate-piano_keyboard-0.1 (crate (name "piano_keyboard") (vers "0.1.1") (deps (list (crate-dep (name "clap") (req "^2.32") (default-features #t) (kind 2)) (crate-dep (name "png") (req "^0.14.0") (default-features #t) (kind 2)))) (hash "0h3fcpw6kih63knnswgg71rxhgbvkxckxbzhk77nvf03vb9bak4j")))

(define-public crate-piano_keyboard-0.2 (crate (name "piano_keyboard") (vers "0.2.2") (deps (list (crate-dep (name "clap") (req "^2.32") (default-features #t) (kind 2)) (crate-dep (name "png") (req "^0.14.0") (default-features #t) (kind 2)))) (hash "1w3adqk3zaqbiggl0mwc0cpngnkshhrckyy596jp3rqa1x17ym46")))

(define-public crate-piano_keyboard-0.2 (crate (name "piano_keyboard") (vers "0.2.3") (deps (list (crate-dep (name "clap") (req "^2.32") (default-features #t) (kind 2)) (crate-dep (name "png") (req "^0.14.0") (default-features #t) (kind 2)))) (hash "1ixynwmls14k4bd2k2b5lr1b466nm79mng5xdfhrqfc2ynvbqfji")))

