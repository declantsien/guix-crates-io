(define-module (crates-io pi nd) #:use-module (crates-io))

(define-public crate-pindakaas-0.0.1 (crate (name "pindakaas") (vers "0.0.1") (hash "07gmavfjxrr5y9w9wh2y8lxdn82pd3w8apb7159fs315mm0v7yzh")))

(define-public crate-pindash-0.0.0 (crate (name "pindash") (vers "0.0.0") (hash "0jcccqz45jqljw5ffv03xhpwqrm5d8i3g3zcjwvr3m026wypd0dc")))

