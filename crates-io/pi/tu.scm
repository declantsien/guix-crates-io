(define-module (crates-io pi tu) #:use-module (crates-io))

(define-public crate-pitufo-1 (crate (name "pitufo") (vers "1.0.0") (deps (list (crate-dep (name "argh") (req "^0.1.3") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "walkdir") (req "^2") (default-features #t) (kind 0)))) (hash "0izk8dzmfv76jyya99zx79nm56rivq80hpa2hc0cn17x7qrznfib")))

(define-public crate-pitufo-1 (crate (name "pitufo") (vers "1.1.0") (deps (list (crate-dep (name "argh") (req "^0.1.3") (default-features #t) (kind 0)) (crate-dep (name "pretty_assertions") (req "^0.6.1") (default-features #t) (kind 2)) (crate-dep (name "serde_json") (req "^1.0") (features (quote ("arbitrary_precision"))) (default-features #t) (kind 0)) (crate-dep (name "walkdir") (req "^2") (default-features #t) (kind 0)))) (hash "09lp27bm2rgcmqwl0nkrsi5rdcr29fza527bjkj9070x0mnnrgx9")))

(define-public crate-pitusya-0.0.0 (crate (name "pitusya") (vers "0.0.0") (hash "0ihn9ihcjz4hi82jc8zh0d1r9a6kd8bhlmq7l4l9a37kcd5kyyl0")))

(define-public crate-pitusya-0.0.1 (crate (name "pitusya") (vers "0.0.1") (hash "1crvvjz7izqv300471rb13k6f1hckg1rlc6nyx0ja4dj41q3d430")))

(define-public crate-pitusya-1 (crate (name "pitusya") (vers "1.0.0") (deps (list (crate-dep (name "cc") (req "^1.0.79") (default-features #t) (kind 1)) (crate-dep (name "dotenv") (req "^0.15.0") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2.146") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.8.3") (default-features #t) (kind 0)))) (hash "1ghks8ndvf74xvx3v94y77jjacn3gkhjylyc9qm1nhqaw90ip3fv")))

(define-public crate-pitusya-1 (crate (name "pitusya") (vers "1.0.1") (deps (list (crate-dep (name "cc") (req "^1.0.79") (default-features #t) (kind 1)) (crate-dep (name "dotenv") (req "^0.15.0") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2.146") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.8.3") (default-features #t) (kind 0)))) (hash "1m4gsayyb5r4mpbrzln43lkshcm3ary2dzndqp42hbvshvqlvg1x")))

(define-public crate-pitusya-1 (crate (name "pitusya") (vers "1.0.2") (deps (list (crate-dep (name "libc") (req "^0.2.146") (default-features #t) (kind 0)) (crate-dep (name "llvm-sys") (req "^160.1.3") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.8.3") (default-features #t) (kind 0)))) (hash "0ffk74fqh0lqs79nqqk595kar58s13nvnx90zfnnscz2sqlb34p7")))

(define-public crate-pitusya-1 (crate (name "pitusya") (vers "1.0.4") (deps (list (crate-dep (name "clap") (req "^4.3.22") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "llvm-sys") (req "^160.1.3") (default-features #t) (kind 0)) (crate-dep (name "pitusyastd") (req "^0.0.1") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.8.3") (default-features #t) (kind 0)))) (hash "0mc53xwggivibwncjva660zj3gldgbls7rfx6f612gys2zjk39sf")))

(define-public crate-pitusyastd-0.0.1 (crate (name "pitusyastd") (vers "0.0.1") (hash "1jhwvlgfd9lgkh2p7yc1bx1q9c9sb9hkfccnshjf8srgnn69d5di")))

