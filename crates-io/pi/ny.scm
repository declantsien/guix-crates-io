(define-module (crates-io pi ny) #:use-module (crates-io))

(define-public crate-piny-0.1 (crate (name "piny") (vers "0.1.0") (deps (list (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "modular-bitfield") (req "^0.11.2") (default-features #t) (kind 0)) (crate-dep (name "nohash-hasher") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "nom") (req "^7.1.3") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.152") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "strum") (req "^0.24.1") (default-features #t) (kind 0)) (crate-dep (name "strum_macros") (req "^0.24.3") (default-features #t) (kind 0)) (crate-dep (name "utoipa") (req "^2.4.2") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "itertools") (req "^0.10.5") (default-features #t) (kind 2)) (crate-dep (name "rstest") (req "^0.16.0") (kind 2)))) (hash "0n5rcjwq9xc518qrm25kl20kdq1fw587rgs57mx16n50y20kwslz") (features (quote (("swagger" "utoipa") ("default" "swagger"))))))

(define-public crate-piny-0.1 (crate (name "piny") (vers "0.1.1") (deps (list (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "modular-bitfield") (req "^0.11.2") (default-features #t) (kind 0)) (crate-dep (name "nohash-hasher") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "nom") (req "^7.1.3") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.152") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "strum") (req "^0.24.1") (default-features #t) (kind 0)) (crate-dep (name "strum_macros") (req "^0.24.3") (default-features #t) (kind 0)) (crate-dep (name "utoipa") (req "^2.4.2") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "itertools") (req "^0.10.5") (default-features #t) (kind 2)) (crate-dep (name "rstest") (req "^0.16.0") (kind 2)))) (hash "059dn4fyglyllhzmdpc0c89s9wqwvkgvkr0psn23wplib4igr720") (features (quote (("swagger" "utoipa") ("polyphone") ("default" "swagger" "polyphone"))))))

(define-public crate-pinyagrep-0.1 (crate (name "pinyagrep") (vers "0.1.0") (deps (list (crate-dep (name "ansi_term") (req "^0.12") (default-features #t) (kind 0)) (crate-dep (name "anyhow") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "assert_cmd") (req "^2.0.7") (default-features #t) (kind 2)) (crate-dep (name "assert_fs") (req "^1.0.10") (default-features #t) (kind 2)) (crate-dep (name "clap") (req "^4.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "env_logger") (req "^0.10.0") (default-features #t) (kind 0)) (crate-dep (name "indicatif") (req "^0.17.2") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.17") (default-features #t) (kind 0)) (crate-dep (name "predicates") (req "^2.1.4") (default-features #t) (kind 2)))) (hash "0xphcx7vvrp0zn1cgl9g99pyncl3d5ncqg3w299caf4acfdb94b4")))

(define-public crate-pinyagrep-0.1 (crate (name "pinyagrep") (vers "0.1.1") (deps (list (crate-dep (name "ansi_term") (req "^0.12") (default-features #t) (kind 0)) (crate-dep (name "anyhow") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "assert_cmd") (req "^2.0.7") (default-features #t) (kind 2)) (crate-dep (name "assert_fs") (req "^1.0.10") (default-features #t) (kind 2)) (crate-dep (name "clap") (req "^4.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "crossbeam-channel") (req "^0.5") (default-features #t) (kind 0)) (crate-dep (name "ctrlc") (req "^3.2.4") (default-features #t) (kind 0)) (crate-dep (name "env_logger") (req "^0.10.0") (default-features #t) (kind 0)) (crate-dep (name "indicatif") (req "^0.17.2") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.17") (default-features #t) (kind 0)) (crate-dep (name "predicates") (req "^2.1.4") (default-features #t) (kind 2)) (crate-dep (name "signal-hook") (req "^0.3.14") (default-features #t) (kind 0)))) (hash "17nsfz619h039gv948gja68z99pi8x9wvqgvihyp9amirlg5ighq")))

(define-public crate-pinyin-0.0.1 (crate (name "pinyin") (vers "0.0.1") (deps (list (crate-dep (name "phf") (req "~0.7.3") (default-features #t) (kind 0)) (crate-dep (name "phf_codegen") (req "~0.7.3") (default-features #t) (kind 1)) (crate-dep (name "regex") (req "~0.1.8") (default-features #t) (kind 0)))) (hash "08svf6k4a3737ajyb96jawnhs1pm89al7dkn1j19xydn6ricwjiq")))

(define-public crate-pinyin-0.0.2 (crate (name "pinyin") (vers "0.0.2") (deps (list (crate-dep (name "phf") (req "~0.7.3") (default-features #t) (kind 0)) (crate-dep (name "phf_codegen") (req "~0.7.3") (default-features #t) (kind 1)) (crate-dep (name "regex") (req "~0.1.8") (default-features #t) (kind 0)))) (hash "1fric7mlh8zwzivvs13kpwx5ffw6k15fvrdfdxk58bmnnhy2rzs5")))

(define-public crate-pinyin-0.0.4 (crate (name "pinyin") (vers "0.0.4") (deps (list (crate-dep (name "phf") (req "~0.7.3") (default-features #t) (kind 0)) (crate-dep (name "phf_codegen") (req "~0.7.3") (default-features #t) (kind 1)) (crate-dep (name "regex") (req "~0.1.8") (default-features #t) (kind 0)))) (hash "1xpwhhha5wqamchgk0zai5l3rcif541c2r3bzy2s7m5ijskwc665") (features (quote (("unstable"))))))

(define-public crate-pinyin-0.0.5 (crate (name "pinyin") (vers "0.0.5") (deps (list (crate-dep (name "phf") (req "~0.7.3") (default-features #t) (kind 0)) (crate-dep (name "phf_codegen") (req "~0.7.3") (default-features #t) (kind 1)) (crate-dep (name "regex") (req "~0.1.8") (default-features #t) (kind 0)))) (hash "0qgyf4rzriyish963z4nqg9jkchfxzdj7bdib9rx795mfjqzirc3") (features (quote (("unstable"))))))

(define-public crate-pinyin-0.0.6 (crate (name "pinyin") (vers "0.0.6") (deps (list (crate-dep (name "phf") (req "~0.7.3") (default-features #t) (kind 0)) (crate-dep (name "phf_codegen") (req "~0.7.3") (default-features #t) (kind 1)) (crate-dep (name "regex") (req "~0.1.8") (default-features #t) (kind 0)))) (hash "036v8sklnf6q07c7lh5kx1hyxk6gs4xb9ri75lkx3nz0sl41c6vf") (features (quote (("unstable"))))))

(define-public crate-pinyin-0.1 (crate (name "pinyin") (vers "0.1.0") (deps (list (crate-dep (name "phf") (req "~0.7.3") (default-features #t) (kind 0)) (crate-dep (name "phf_codegen") (req "~0.7.3") (default-features #t) (kind 1)) (crate-dep (name "regex") (req "~0.1.8") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "~0.1.8") (default-features #t) (kind 1)))) (hash "02mi5bnqs52vvx7dc4jdbr4fzsyd0fps1i164dw7f3a810pi7264") (features (quote (("unstable"))))))

(define-public crate-pinyin-0.2 (crate (name "pinyin") (vers "0.2.0") (deps (list (crate-dep (name "phf") (req "~0.7.3") (default-features #t) (kind 0)) (crate-dep (name "phf_codegen") (req "~0.7.3") (default-features #t) (kind 1)) (crate-dep (name "regex") (req "~0.1.8") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "~0.1.8") (default-features #t) (kind 1)))) (hash "1p3r9j55nw9q4q4m0kh39j63dxmgwrayibc26aflh7p3va7r0ks6") (features (quote (("unstable"))))))

(define-public crate-pinyin-0.3 (crate (name "pinyin") (vers "0.3.0") (deps (list (crate-dep (name "phf") (req "^0.7") (default-features #t) (kind 0)) (crate-dep (name "phf_codegen") (req "^0.7") (default-features #t) (kind 1)) (crate-dep (name "regex") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^0.2") (default-features #t) (kind 1)))) (hash "0hxp60270z46lqkw2k3vzsgmc972h3mmg4mx0al25zn25nsls38x") (features (quote (("unstable"))))))

(define-public crate-pinyin-0.4 (crate (name "pinyin") (vers "0.4.0") (hash "16dc6c5jj41bdwxkncd5ifxghxlb3879vpn4y2xw5lg89rjdjjsv") (features (quote (("unstable"))))))

(define-public crate-pinyin-0.5 (crate (name "pinyin") (vers "0.5.0") (hash "1f1i3x0nsm3qp1515448v7iph27icwdy7vzdm1xihbz33kywwwac") (features (quote (("unstable"))))))

(define-public crate-pinyin-0.6 (crate (name "pinyin") (vers "0.6.0") (deps (list (crate-dep (name "lazy_static") (req "^1") (default-features #t) (kind 0)))) (hash "010irv84rbmzia11di82a3b4bbcnjmi3536km55c53grxa2dbbfk") (features (quote (("unstable"))))))

(define-public crate-pinyin-0.7 (crate (name "pinyin") (vers "0.7.0") (hash "132s89n6h6bs0h8nxw6ks4gjihzs17snqzg8asyl7s1gm8nmcw3s") (features (quote (("with_tone_num_end") ("with_tone_num") ("with_tone") ("plain") ("heteronym") ("default" "compat" "plain" "with_tone" "with_tone_num" "with_tone_num_end" "heteronym") ("compat" "plain" "with_tone" "with_tone_num" "heteronym"))))))

(define-public crate-pinyin-0.8 (crate (name "pinyin") (vers "0.8.0") (hash "0n9s3j09cf6j18v2rps8bxppjs7lv5342c0v4la8zs8cgyb6idzc") (features (quote (("with_tone_num_end") ("with_tone_num") ("with_tone") ("plain") ("heteronym") ("default" "compat" "plain" "with_tone" "with_tone_num" "with_tone_num_end" "heteronym") ("compat" "plain" "with_tone" "with_tone_num" "heteronym"))))))

(define-public crate-pinyin-0.9 (crate (name "pinyin") (vers "0.9.0") (hash "1i30n9gpzf3k6hmm9sjrma6pfmkhgbrpvx82w19438xgwcv27l9v") (features (quote (("with_tone_num_end") ("with_tone_num") ("with_tone") ("plain") ("heteronym") ("default" "compat" "plain" "with_tone" "with_tone_num" "with_tone_num_end" "heteronym") ("compat" "plain" "with_tone" "with_tone_num" "heteronym"))))))

(define-public crate-pinyin-0.10 (crate (name "pinyin") (vers "0.10.0") (hash "1h2glzd6iaii0vh4wchhl5n8l1pbx4fm596fl0ww46kas0f63whn") (features (quote (("with_tone_num_end") ("with_tone_num") ("with_tone") ("plain") ("heteronym") ("default" "compat" "plain" "with_tone" "with_tone_num" "with_tone_num_end" "heteronym") ("compat" "plain" "with_tone" "with_tone_num" "heteronym"))))))

(define-public crate-pinyin-order-0.1 (crate (name "pinyin-order") (vers "0.1.0") (deps (list (crate-dep (name "phf") (req "^0.7") (default-features #t) (kind 0)) (crate-dep (name "phf_codegen") (req "^0.7") (default-features #t) (kind 1)) (crate-dep (name "regex") (req "^0.2") (default-features #t) (kind 1)))) (hash "1aq2x1368gins85cpkrwd2vjscqqhqhvpc960rbd2nwddip1bmnk")))

(define-public crate-pinyin-order-0.1 (crate (name "pinyin-order") (vers "0.1.1") (deps (list (crate-dep (name "phf") (req "^0.7") (default-features #t) (kind 0)) (crate-dep (name "phf_codegen") (req "^0.7") (default-features #t) (kind 1)) (crate-dep (name "regex") (req "^0.2") (default-features #t) (kind 1)))) (hash "014v26k8bq5gy6ymbd4wy7j6la0bd72yzrswrxik1jpng8d2c0zz")))

(define-public crate-pinyin-order-0.1 (crate (name "pinyin-order") (vers "0.1.2") (deps (list (crate-dep (name "phf") (req "^0.7") (default-features #t) (kind 0)) (crate-dep (name "phf_codegen") (req "^0.7") (default-features #t) (kind 1)) (crate-dep (name "regex") (req "^0.2") (default-features #t) (kind 1)))) (hash "1asy9274hnk5s8l4qw5ksg5hqlinhgf81sw8kxma73ngpbh8m2a3")))

(define-public crate-pinyin-parser-0.1 (crate (name "pinyin-parser") (vers "0.1.0") (deps (list (crate-dep (name "unicode-normalization") (req "^0.1.19") (default-features #t) (kind 0)) (crate-dep (name "unicode-segmentation") (req "^1.7.1") (default-features #t) (kind 0)))) (hash "0icqy4nninjri93m1r439cfm41aayh72fjcgsczp5ck801q98n7b")))

(define-public crate-pinyin-parser-0.1 (crate (name "pinyin-parser") (vers "0.1.1") (deps (list (crate-dep (name "unicode-normalization") (req "^0.1.19") (default-features #t) (kind 0)) (crate-dep (name "unicode-segmentation") (req "^1.7.1") (default-features #t) (kind 0)))) (hash "1lz2f5ss0x51wzw0sgh38djbybpl65bv0ivkhysj0c6dr9c9pavj")))

(define-public crate-pinyin-parser-0.1 (crate (name "pinyin-parser") (vers "0.1.2") (deps (list (crate-dep (name "unicode-normalization") (req "^0.1.19") (default-features #t) (kind 0)) (crate-dep (name "unicode-segmentation") (req "^1.7.1") (default-features #t) (kind 0)))) (hash "1z4allyhrks682ja8l0mxifmwy6i1ik93lwqywqa2bz8bpkdigfi")))

(define-public crate-pinyin-parser-0.1 (crate (name "pinyin-parser") (vers "0.1.3") (deps (list (crate-dep (name "unicode-normalization") (req "^0.1.19") (default-features #t) (kind 0)) (crate-dep (name "unicode-segmentation") (req "^1.7.1") (default-features #t) (kind 0)))) (hash "0x8mh7fy7as3m677wya18dhjvjrczkq5ca1b74145338d8pzanrr")))

(define-public crate-pinyin-parser-0.1 (crate (name "pinyin-parser") (vers "0.1.4") (deps (list (crate-dep (name "unicode-normalization") (req "^0.1.19") (default-features #t) (kind 0)) (crate-dep (name "unicode-segmentation") (req "^1.7.1") (default-features #t) (kind 0)))) (hash "1pjchxrzmca8i0rxw0dca6ci19anbr1xyfdir465b4c1a2d1z9x9")))

(define-public crate-pinyin-parser-0.1 (crate (name "pinyin-parser") (vers "0.1.5") (deps (list (crate-dep (name "unicode-normalization") (req "^0.1.19") (default-features #t) (kind 0)) (crate-dep (name "unicode-segmentation") (req "^1.7.1") (default-features #t) (kind 0)))) (hash "1qy36ljr3zk6m6zwrjg9rd5lak67dq56qy6ckagjafdvp61p0qvx")))

(define-public crate-pinyin-parser-0.1 (crate (name "pinyin-parser") (vers "0.1.6") (deps (list (crate-dep (name "unicode-normalization") (req "^0.1.19") (default-features #t) (kind 0)) (crate-dep (name "unicode-segmentation") (req "^1.7.1") (default-features #t) (kind 0)))) (hash "0wvyyryv25fzfn3z6wiipw54qg9xp62i7843lal76jd3zvgcsgvi")))

(define-public crate-pinyin-parser-0.1 (crate (name "pinyin-parser") (vers "0.1.7") (deps (list (crate-dep (name "unicode-normalization") (req "^0.1.19") (default-features #t) (kind 0)) (crate-dep (name "unicode-segmentation") (req "^1.7.1") (default-features #t) (kind 0)))) (hash "08x5gmigi83a2fbhnmqd4p7845wswidspnrx9sb420b9v9mj6fij")))

(define-public crate-pinyin-parser-0.1 (crate (name "pinyin-parser") (vers "0.1.8") (deps (list (crate-dep (name "unicode-normalization") (req "^0.1.19") (default-features #t) (kind 0)) (crate-dep (name "unicode-segmentation") (req "^1.7.1") (default-features #t) (kind 0)))) (hash "0fyzy9d6bi1nyhxlkn5m527j2xic0163fwvf3db5ffdvlhfh4x7i")))

(define-public crate-pinyin-parser-0.1 (crate (name "pinyin-parser") (vers "0.1.9") (deps (list (crate-dep (name "unicode-normalization") (req "^0.1.19") (default-features #t) (kind 0)) (crate-dep (name "unicode-segmentation") (req "^1.7.1") (default-features #t) (kind 0)))) (hash "0iq57yrv8apmqnj56rn71z10acm2qaf30cmkf7a645mcf1gpmsq8")))

(define-public crate-pinyin-tool-0.1 (crate (name "pinyin-tool") (vers "0.1.0") (deps (list (crate-dep (name "jieba-rs") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "pinyin") (req "^0.7") (default-features #t) (kind 0)) (crate-dep (name "regex-syntax") (req "^0.6.14") (default-features #t) (kind 0)))) (hash "1yibg7mdqr2b6bzhghgb5x22g152wi8bf7qn1zjbpl58mbrniv8z")))

(define-public crate-pinyin-tool-0.1 (crate (name "pinyin-tool") (vers "0.1.1") (deps (list (crate-dep (name "jieba-rs") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "pinyin") (req "^0.7") (default-features #t) (kind 0)) (crate-dep (name "regex-syntax") (req "^0.6.14") (default-features #t) (kind 0)))) (hash "04fwv8n7b0nj8qfl3gqxzzhmmwsd3qhlyc70sjwfzixs8hcr2c5p")))

(define-public crate-pinyin-tool-0.1 (crate (name "pinyin-tool") (vers "0.1.2") (deps (list (crate-dep (name "jieba-rs") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "pinyin") (req "^0.7") (default-features #t) (kind 0)) (crate-dep (name "regex-syntax") (req "^0.6.14") (default-features #t) (kind 0)))) (hash "0g0i3nllcbmlg42vqv43cp64vhp7gllljxgwmq3kh86gaakb0gbr")))

(define-public crate-pinyin-tool-0.1 (crate (name "pinyin-tool") (vers "0.1.3") (deps (list (crate-dep (name "jieba-rs") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "pinyin") (req "^0.7") (default-features #t) (kind 0)) (crate-dep (name "regex-syntax") (req "^0.6.14") (default-features #t) (kind 0)))) (hash "0j7s8mh1h9bsbxmyp1ksygw8whxag5ka809xyj8a2vk3d70bd7hd")))

(define-public crate-pinyin-translator-0.1 (crate (name "pinyin-translator") (vers "0.1.0") (hash "153sf67nfs5b0h9dxrv9pdlr4vrahp0agbib77w5cvny17w4m225")))

(define-public crate-pinyin-translator-0.1 (crate (name "pinyin-translator") (vers "0.1.1") (hash "03jbblr2in3klimmmb4s0rdcd41iw31lv4dg9yzmicjblwd0wczw")))

(define-public crate-pinyin-translator-0.2 (crate (name "pinyin-translator") (vers "0.2.0") (hash "1jf4s7qw6fsjgg79d1k37xi2krynpfr6rabz5frpvavvdhipg7sc")))

(define-public crate-pinyin-translator-0.2 (crate (name "pinyin-translator") (vers "0.2.1") (hash "0n3vqm52hnsx4h9p0wrcm4rv76crygfldzplykh4cakx3ssjjhbv")))

(define-public crate-pinyin-translator-0.2 (crate (name "pinyin-translator") (vers "0.2.2") (hash "0di5h597lzqd6lp2szn2gvbqnawx72qg2yxssi4vr4zpzcb9zdxr")))

(define-public crate-pinyin-translator-0.2 (crate (name "pinyin-translator") (vers "0.2.3") (deps (list (crate-dep (name "cbindgen") (req "^0.24.3") (default-features #t) (kind 1)))) (hash "1zaj64gk1jyk21f6sc6vhggf2a463hynqkq1y91izrbwlf7lh7b7")))

(define-public crate-pinyin_zhuyin-0.1 (crate (name "pinyin_zhuyin") (vers "0.1.0") (deps (list (crate-dep (name "phf") (req "^0.7") (default-features #t) (kind 0)) (crate-dep (name "phf_codegen") (req "^0.7") (default-features #t) (kind 1)))) (hash "09yq2i370hh1i14xgnldwxmi7y208a5zcak0camxr11njnzsd2l1")))

(define-public crate-pinyin_zhuyin-0.1 (crate (name "pinyin_zhuyin") (vers "0.1.1") (deps (list (crate-dep (name "phf") (req "^0.7") (default-features #t) (kind 0)) (crate-dep (name "phf_codegen") (req "^0.7") (default-features #t) (kind 1)))) (hash "1kq72m47qzl4h50grzk0af91av15zn5xgx86w8p0xd9cpzg5l7b0")))

(define-public crate-pinyin_zhuyin-0.1 (crate (name "pinyin_zhuyin") (vers "0.1.2") (deps (list (crate-dep (name "phf") (req "^0.7") (default-features #t) (kind 0)) (crate-dep (name "phf_codegen") (req "^0.7") (default-features #t) (kind 1)))) (hash "0cs26d9iq89a4xzklmsvdf1y3jcnffnwm61ndr1qald95nb96x73")))

(define-public crate-pinyin_zhuyin-0.2 (crate (name "pinyin_zhuyin") (vers "0.2.0") (deps (list (crate-dep (name "phf") (req "^0.11") (default-features #t) (kind 0)) (crate-dep (name "phf_codegen") (req "^0.11") (default-features #t) (kind 1)))) (hash "1jdasy6a4rwq5404gd5837n6hj4ygkqdp6jn16p8i9j2902r0b80")))

