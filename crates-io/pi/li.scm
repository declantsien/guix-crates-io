(define-module (crates-io pi li) #:use-module (crates-io))

(define-public crate-pili-0.0.0 (crate (name "pili") (vers "0.0.0") (hash "1a8vh3hxygz7idfsfy83q5knnx92gqawjqfihmykqc84l4ccj2wh")))

(define-public crate-pilipala-kernel-0.0.1 (crate (name "pilipala-kernel") (vers "0.0.1") (hash "0aa2q3phdl680yxp2s4hrsw3zswagkmjvwbv4b6x7gij7wg7wdqs")))

