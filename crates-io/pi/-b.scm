(define-module (crates-io pi -b) #:use-module (crates-io))

(define-public crate-pi-beep-0.1 (crate (name "pi-beep") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^4.0.29") (default-features #t) (kind 0)))) (hash "1pb4cww5rls3bkawaqf2mc297da8xhq9dvakmrxak5cq9n4mfrxs")))

