(define-module (crates-io pi dd) #:use-module (crates-io))

(define-public crate-piddiy-0.1 (crate (name "piddiy") (vers "0.1.0") (deps (list (crate-dep (name "num-traits") (req "^0.2") (default-features #t) (kind 0)))) (hash "1qmy45dxjcx4bmnxdkgsak1c6hjjkngarph5dfdfd156qxs1gvfg")))

(define-public crate-piddiy-0.1 (crate (name "piddiy") (vers "0.1.1") (deps (list (crate-dep (name "num-traits") (req "^0.2") (default-features #t) (kind 0)))) (hash "0am943sy7xzp77ycnfs77s44hs6ymf5k2x3v355sr091imqs6q7p")))

