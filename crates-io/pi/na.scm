(define-module (crates-io pi na) #:use-module (crates-io))

(define-public crate-pinar-0.1 (crate (name "pinar") (vers "0.1.0") (deps (list (crate-dep (name "failure") (req "^0.1.3") (default-features #t) (kind 0)) (crate-dep (name "napi-sys") (req "^0.1") (default-features #t) (kind 0) (package "pinar-napi-sys")) (crate-dep (name "pinar-derive") (req "^0.1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "1.*") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "1.*") (optional #t) (default-features #t) (kind 0)))) (hash "1wyps70wc815n38k9g1x0kdd7yagsqx34jjy6p6bdm3aqd3aijq9") (features (quote (("pinar-serde" "serde" "serde_derive" "pinar-derive") ("default" "pinar-serde"))))))

(define-public crate-pinar-derive-0.1 (crate (name "pinar-derive") (vers "0.1.0") (deps (list (crate-dep (name "quote") (req "^0.6.10") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^0.15.22") (default-features #t) (kind 0)))) (hash "0kbyi9f4hgzja8w54qrg06dd48adw3d62agkijbpjzzyb6wsx0ji")))

(define-public crate-pinar-derive-0.2 (crate (name "pinar-derive") (vers "0.2.0") (deps (list (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.5") (features (quote ("full" "extra-traits" "clone-impls" "visit" "visit-mut" "derive"))) (default-features #t) (kind 0)))) (hash "0hgqvn3bc32rpmvcj61y2ljpcr3xgc283f4z2nh9zwslz4jddggz")))

(define-public crate-pinar-napi-sys-0.1 (crate (name "pinar-napi-sys") (vers "0.1.0") (hash "0kdlfy6kfcn0hx6hpsbc65lzvi4l5b46q506lnn83pmrc4sps9ks")))

(define-public crate-pinarcmutex-0.1 (crate (name "pinarcmutex") (vers "0.1.0") (deps (list (crate-dep (name "tokio") (req "^1.0") (features (quote ("sync"))) (default-features #t) (kind 0)))) (hash "0j9mbfkb9lv8sjih7y39xjwypffh92qbysy2aiqhl28dary69lcl") (rust-version "1.49")))

(define-public crate-pinarcmutex-0.1 (crate (name "pinarcmutex") (vers "0.1.1") (deps (list (crate-dep (name "tokio") (req "^1.0") (features (quote ("sync"))) (default-features #t) (kind 0)))) (hash "177gbzzczn0i87q495y67niy6ld6f3mrqhx82mkrr3fkc5a9lw5r") (rust-version "1.49")))

(define-public crate-pinata-sdk-0.1 (crate (name "pinata-sdk") (vers "0.1.0") (deps (list (crate-dep (name "failure") (req "^0.1.7") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.8") (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.10") (features (quote ("json"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.105") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^0.2") (features (quote ("full"))) (default-features #t) (kind 0)) (crate-dep (name "walkdir") (req "^2.3.1") (default-features #t) (kind 0)))) (hash "0iza940c9k42ay92zfcgjvrjp5pcahxsskr3qijvafkgn4f309v7")))

(define-public crate-pinata-sdk-0.1 (crate (name "pinata-sdk") (vers "0.1.1") (deps (list (crate-dep (name "failure") (req "^0.1.7") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.8") (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.10") (features (quote ("json"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.105") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^0.2") (features (quote ("full"))) (default-features #t) (kind 0)) (crate-dep (name "walkdir") (req "^2.3.1") (default-features #t) (kind 0)))) (hash "1ix2dcpl07kb1phcd836fv7dbnnwhagr3d0bjcywhqwql4hnj09l")))

(define-public crate-pinata-sdk-1 (crate (name "pinata-sdk") (vers "1.0.0") (deps (list (crate-dep (name "derive_builder") (req "^0.9.0") (default-features #t) (kind 0)) (crate-dep (name "failure") (req "^0.1.7") (default-features #t) (kind 0)) (crate-dep (name "insta") (req "^0.16.0") (default-features #t) (kind 2)) (crate-dep (name "log") (req "^0.4.8") (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.10") (features (quote ("json"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.105") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^0.2") (features (quote ("full"))) (default-features #t) (kind 0)) (crate-dep (name "walkdir") (req "^2.3.1") (default-features #t) (kind 0)))) (hash "0ak41jsg9628caqf3zrblgk17z9hx1ck77jzgyhpi24546z05hnj")))

(define-public crate-pinata-sdk-1 (crate (name "pinata-sdk") (vers "1.1.0") (deps (list (crate-dep (name "derive_builder") (req "^0.10.2") (default-features #t) (kind 0)) (crate-dep (name "failure") (req "^0.1.8") (default-features #t) (kind 0)) (crate-dep (name "insta") (req "^1.8.0") (default-features #t) (kind 2)) (crate-dep (name "log") (req "^0.4.14") (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.11.7") (features (quote ("json" "multipart"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.130") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.72") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.14.0") (features (quote ("full"))) (default-features #t) (kind 0)) (crate-dep (name "walkdir") (req "^2.3.2") (default-features #t) (kind 0)))) (hash "0q474gbhyczkg0rghz0h1mdpw70waasxba64rv2108xhzpwchiyc")))

(define-public crate-pinata_ipfs-0.1 (crate (name "pinata_ipfs") (vers "0.1.0") (deps (list (crate-dep (name "reqwest") (req "^0.11.20") (features (quote ("multipart"))) (default-features #t) (kind 0)))) (hash "0fsjcp0drn5jjh1swga7kz0d0x82a9s7ff7di4ap5jyy4xi7n6nn")))

(define-public crate-pinata_ipfs-0.1 (crate (name "pinata_ipfs") (vers "0.1.1") (deps (list (crate-dep (name "reqwest") (req "^0.11.20") (features (quote ("multipart"))) (default-features #t) (kind 0)))) (hash "1yx75qx886mh6iakcc3lf4n5m2287d4bbs3pzra3gmjf7x77z142")))

(define-public crate-pinata_ipfs-0.1 (crate (name "pinata_ipfs") (vers "0.1.2") (deps (list (crate-dep (name "reqwest") (req "^0.11.20") (features (quote ("multipart"))) (default-features #t) (kind 0)))) (hash "1xc97wschxjjkg4s72v081m1b8jh78i4ximmy7sl44m3wr6bk3qm")))

(define-public crate-pinata_ipfs-0.1 (crate (name "pinata_ipfs") (vers "0.1.3") (deps (list (crate-dep (name "reqwest") (req "^0.11.20") (features (quote ("multipart"))) (default-features #t) (kind 0)))) (hash "0pynlrha3mih2921mmw3xqhkp02m2p03ldpvqmm7r98w6cv0a3gw")))

(define-public crate-pinata_ipfs-0.1 (crate (name "pinata_ipfs") (vers "0.1.4") (deps (list (crate-dep (name "reqwest") (req "^0.11.20") (features (quote ("multipart"))) (default-features #t) (kind 0)))) (hash "0iz97vz6qgm7z9rncrnc88fav57ywzvn8d2kmhj30852scbk5y4s")))

(define-public crate-pinata_ipfs-0.1 (crate (name "pinata_ipfs") (vers "0.1.5") (deps (list (crate-dep (name "reqwest") (req "^0.11.20") (features (quote ("multipart"))) (default-features #t) (kind 0)))) (hash "09f4qq7f0f6jnxkfbcmyr4va5mxpnva55lkwz0zwadrpz9g6w4k6")))

(define-public crate-pinata_ipfs-0.1 (crate (name "pinata_ipfs") (vers "0.1.6") (deps (list (crate-dep (name "reqwest") (req "^0.11.20") (features (quote ("multipart"))) (default-features #t) (kind 0)))) (hash "0pr8b989sm1i5pvgn8yyl95z9z056lps0z3yl8lpb6jnd24bfmcp")))

(define-public crate-pinata_ipfs-0.1 (crate (name "pinata_ipfs") (vers "0.1.7") (deps (list (crate-dep (name "reqwest") (req "^0.11.20") (features (quote ("multipart"))) (default-features #t) (kind 0)))) (hash "10bq46nkvz0dp34w9h4fc2knh3sj3blnglkslysr1530az887vaj")))

(define-public crate-pinata_ipfs-0.1 (crate (name "pinata_ipfs") (vers "0.1.8") (deps (list (crate-dep (name "reqwest") (req "^0.11.20") (features (quote ("multipart"))) (default-features #t) (kind 0)))) (hash "1fsmaai3n0shz1n3g9i3igjjr50fh651v3xbxc9lvg261kw2yf84")))

(define-public crate-pinata_ipfs-0.1 (crate (name "pinata_ipfs") (vers "0.1.9") (deps (list (crate-dep (name "reqwest") (req "^0.11.20") (features (quote ("multipart"))) (default-features #t) (kind 0)))) (hash "1883d3pi0zs7d3r0v5ayp4vwvm6q5wzfv85qhns3kvh1qmc36pcb")))

