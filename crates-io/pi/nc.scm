(define-module (crates-io pi nc) #:use-module (crates-io))

(define-public crate-pince-0.1 (crate (name "pince") (vers "0.1.0") (deps (list (crate-dep (name "hyper") (req "^0.14") (features (quote ("full"))) (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "1ws3387bzx724zfcihasfmi3vwdifz5ifl33gh28ica9zj1zwggn")))

(define-public crate-pincer-0.1 (crate (name "pincer") (vers "0.1.0") (hash "02a0pn5h3dgy4g6yhjdrd41bkdqlx0hwbi8xg30d401nmx3v3vmf")))

(define-public crate-pincer-1 (crate (name "pincer") (vers "1.0.0") (hash "0ijag0kbzf27v73ackn3vx21cxj1l342ysns40yhlsnc8jqx14xd")))

(define-public crate-pincer-2 (crate (name "pincer") (vers "2.0.0") (hash "1msc6bwz9qd1m0kmxam47cv4bp6lhjscjz7zl6fg27yyf2hl388d")))

(define-public crate-pincers-0.1 (crate (name "pincers") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^2.32.0") (default-features #t) (kind 0)) (crate-dep (name "digest") (req "^0.8.0") (default-features #t) (kind 0)) (crate-dep (name "hex") (req "^0.3.2") (default-features #t) (kind 0)) (crate-dep (name "md-5") (req "^0.8.0") (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.9.4") (default-features #t) (kind 0)) (crate-dep (name "sha-1") (req "^0.8.0") (default-features #t) (kind 0)) (crate-dep (name "sha2") (req "^0.8.0") (default-features #t) (kind 0)) (crate-dep (name "sha3") (req "^0.8.0") (default-features #t) (kind 0)))) (hash "1m7976ybcrbg1d1dwxkql51dkkic77nqvh09kkliz3yciv7na222")))

(define-public crate-pincers-0.2 (crate (name "pincers") (vers "0.2.0") (deps (list (crate-dep (name "clap") (req "^2.32.0") (default-features #t) (kind 0)) (crate-dep (name "digest") (req "^0.8.0") (default-features #t) (kind 0)) (crate-dep (name "failure") (req "^0.1.3") (default-features #t) (kind 0)) (crate-dep (name "hex") (req "^0.3.2") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.3.0") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.6") (default-features #t) (kind 0)) (crate-dep (name "maplit") (req "^1.0.1") (default-features #t) (kind 0)) (crate-dep (name "md-5") (req "^0.8.0") (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.9.4") (default-features #t) (kind 0)) (crate-dep (name "sha-1") (req "^0.8.0") (default-features #t) (kind 0)) (crate-dep (name "sha2") (req "^0.8.0") (default-features #t) (kind 0)) (crate-dep (name "sha3") (req "^0.8.0") (default-features #t) (kind 0)) (crate-dep (name "simplelog") (req "^0.5.3") (default-features #t) (kind 0)))) (hash "13wz4qf2xpymja2pnjldpc1j0ygk2lr7wq3332d3mqqaiwcmzhwk")))

(define-public crate-pinch-0.1 (crate (name "pinch") (vers "0.1.0") (deps (list (crate-dep (name "handlebars") (req "^4.1.2") (default-features #t) (kind 0)) (crate-dep (name "pulldown-cmark") (req "^0.8.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.66") (default-features #t) (kind 0)) (crate-dep (name "toml") (req "^0.4.2") (default-features #t) (kind 0)) (crate-dep (name "walkdir") (req "^2") (default-features #t) (kind 0)))) (hash "1lkz859k2krhyr45gdzaim52y90zi8mvdph7i5nmwznpia98wqs6")))

(define-public crate-pincli-0.1 (crate (name "pincli") (vers "0.1.0") (hash "0qpqn6mvki2pjls8ddhhzj8szqk7xv4dnscw7401377jqn3hi3az")))

(define-public crate-pincli-0.1 (crate (name "pincli") (vers "0.1.1") (hash "0nw10dj04xc96hpmsc9hi71d7y9qvvvq3habf9mwjfxhsd5kjdwg")))

