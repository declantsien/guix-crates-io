(define-module (crates-io pi -s) #:use-module (crates-io))

(define-public crate-pi-search-0.1 (crate (name "pi-search") (vers "0.1.0") (deps (list (crate-dep (name "bincode") (req "^2.0.0-rc.1") (default-features #t) (kind 0)) (crate-dep (name "itertools") (req "^0.10.3") (default-features #t) (kind 0)) (crate-dep (name "rayon") (req "^1.5.1") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.136") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0.136") (default-features #t) (kind 0)))) (hash "1g9jryk9rfg9frx4jab4471lwrvlckig6j7ncf6zxs6h3vxkzrly")))

(define-public crate-pi-search-0.2 (crate (name "pi-search") (vers "0.2.0") (deps (list (crate-dep (name "bincode") (req "^2.0.0-rc.1") (default-features #t) (kind 0)) (crate-dep (name "itertools") (req "^0.10.3") (default-features #t) (kind 0)) (crate-dep (name "rayon") (req "^1.5.1") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.136") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0.136") (default-features #t) (kind 0)))) (hash "1k95mr6j9bilnxripix24y3fwhzi8pwrhhccv8xwhxq4bb0jd196")))

(define-public crate-pi-search-0.2 (crate (name "pi-search") (vers "0.2.1") (deps (list (crate-dep (name "bincode") (req "^2.0.0-rc.1") (default-features #t) (kind 0)) (crate-dep (name "itertools") (req "^0.10.3") (default-features #t) (kind 0)) (crate-dep (name "rayon") (req "^1.5.1") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.136") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0.136") (default-features #t) (kind 0)))) (hash "1l75w06ha1ciz17yymkwv9gnpcprds2ijzd6bqk5nf30qhadbbz1")))

