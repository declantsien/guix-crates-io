(define-module (crates-io pi vo) #:use-module (crates-io))

(define-public crate-pivoine-0.1 (crate (name "pivoine") (vers "0.1.0") (hash "1kdq0nvriisy2ncv59h93cdwdyyd082x48dydjkrkkn68vdl4xv6")))

(define-public crate-pivot-0.1 (crate (name "pivot") (vers "0.1.0") (deps (list (crate-dep (name "nom") (req "^5.1.2") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.4.1") (default-features #t) (kind 0)) (crate-dep (name "ron") (req "^0.6.2") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.117") (default-features #t) (kind 0)) (crate-dep (name "wat") (req "^1.0.29") (default-features #t) (kind 0)))) (hash "1flrlvnhmdsw0g63zla94c6y1sglvidw6hdibgfiz46132inry7k")))

(define-public crate-pivotal-tracker-changelog-0.1 (crate (name "pivotal-tracker-changelog") (vers "0.1.0") (deps (list (crate-dep (name "futures") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "git2") (req "^0.13") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.10") (features (quote ("json" "blocking"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^0.2") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "1spgjn6birg4wf8r14wp0ajg0mmxbslzac07qczp0rajdp566nkk")))

