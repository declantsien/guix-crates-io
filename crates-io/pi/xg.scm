(define-module (crates-io pi xg) #:use-module (crates-io))

(define-public crate-pixglyph-0.1 (crate (name "pixglyph") (vers "0.1.0") (deps (list (crate-dep (name "ttf-parser") (req "^0.18") (default-features #t) (kind 0)))) (hash "12zx71y4qxjbnjp9yvsgzrnqm09vi5zmikfdr0ay6pvijg9svvwy")))

(define-public crate-pixglyph-0.2 (crate (name "pixglyph") (vers "0.2.0") (deps (list (crate-dep (name "ttf-parser") (req "^0.19") (default-features #t) (kind 0)))) (hash "13wg9dp7l6a2h3fnh9lngfy952mccsqdlnnq3hyfcs363zr92xgn")))

(define-public crate-pixglyph-0.3 (crate (name "pixglyph") (vers "0.3.0") (deps (list (crate-dep (name "ttf-parser") (req "^0.20") (default-features #t) (kind 0)))) (hash "04m7l6y49nimb8pw9x6mqxjqcy248p2c705q4n0v6z8r9jnziq72")))

