(define-module (crates-io pi d-) #:use-module (crates-io))

(define-public crate-pid-allocator-0.1 (crate (name "pid-allocator") (vers "0.1.0") (deps (list (crate-dep (name "spin") (req "^0.9.0") (default-features #t) (kind 0)))) (hash "1aqdpjhm4ccakp091sa88jmd7qi5429820hxap7ax9yqk229gmhq")))

(define-public crate-pid-allocator-0.1 (crate (name "pid-allocator") (vers "0.1.1") (deps (list (crate-dep (name "spin") (req "^0.9.8") (default-features #t) (kind 0)))) (hash "0x9zvdllvjzal5dbnaq5qbyq6i56f6l9g9azqykwinvrc2i2dy9i")))

(define-public crate-pid-allocator-0.1 (crate (name "pid-allocator") (vers "0.1.2") (deps (list (crate-dep (name "criterion") (req "^0.5.1") (default-features #t) (kind 2)) (crate-dep (name "spin") (req "^0.9.8") (default-features #t) (kind 0)))) (hash "04kk51dl4n20p19lz3cqfddpm879dgn8ldm2l3nrz9p75ng801sz")))

(define-public crate-pid-allocator-0.1 (crate (name "pid-allocator") (vers "0.1.3") (deps (list (crate-dep (name "criterion") (req "^0.5.1") (default-features #t) (kind 2)) (crate-dep (name "spin") (req "^0.9.8") (default-features #t) (kind 0)))) (hash "1sv00q1bzrz56iqivkqym7ffksf6h4c93xnjn96ybppbni5b74a7")))

(define-public crate-pid-allocator-0.1 (crate (name "pid-allocator") (vers "0.1.4") (deps (list (crate-dep (name "criterion") (req "^0.5.1") (default-features #t) (kind 2)) (crate-dep (name "spin") (req "^0.9.8") (default-features #t) (kind 0)))) (hash "06n25dnf7raf324ms7hvzhyla2lakikm12h2h9a8h4hyq3c6vzya")))

(define-public crate-pid-allocator-0.1 (crate (name "pid-allocator") (vers "0.1.5") (deps (list (crate-dep (name "criterion") (req "^0.5.1") (default-features #t) (kind 2)) (crate-dep (name "spin") (req "^0.9.8") (default-features #t) (kind 0)))) (hash "0dxsnsqqjx78mib8d74gv428rbc10vz4mrc5fm8wcq6fj12672vs")))

(define-public crate-pid-controller-0.1 (crate (name "pid-controller") (vers "0.1.0") (deps (list (crate-dep (name "num-traits") (req "^0.2.15") (default-features #t) (kind 0)))) (hash "0jx995cvq24jajvj3dwy55pfkxmmsv5nl7vxczkgrjilidzvrff7")))

(define-public crate-pid-ctrl-0.1 (crate (name "pid-ctrl") (vers "0.1.0") (deps (list (crate-dep (name "num-traits") (req "^0.2.15") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)))) (hash "1d6wxygbfp2clsakp41lk6nzcigj1kbmf0fgnaji186hlyp9h4w4") (yanked #t)))

(define-public crate-pid-ctrl-0.1 (crate (name "pid-ctrl") (vers "0.1.1") (deps (list (crate-dep (name "num-traits") (req "^0.2.15") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)))) (hash "0pq095d33wm660f6pf9axdkml2y7zhfjw3pd28ayhmjrwncbavzn") (yanked #t)))

(define-public crate-pid-ctrl-0.1 (crate (name "pid-ctrl") (vers "0.1.2") (deps (list (crate-dep (name "num-traits") (req "^0.2.15") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)))) (hash "1kh2cca6pvkl9ws1p8qysq5clig6z8yck4j5s8fycsfpgnk174vi") (yanked #t)))

(define-public crate-pid-ctrl-0.1 (crate (name "pid-ctrl") (vers "0.1.3") (deps (list (crate-dep (name "num-traits") (req "^0.2.15") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)))) (hash "0mqgdkd3yd8xk4x2yf1crw71wxjhr7arnqzkr4y8nmyb84k7d8nq") (yanked #t)))

(define-public crate-pid-ctrl-0.1 (crate (name "pid-ctrl") (vers "0.1.4") (deps (list (crate-dep (name "num-traits") (req "^0.2.15") (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)))) (hash "1jh2v1h1is4sjfzb3pdl4k8kjyr2cnqmn4xr3p0ja9h6a4rc7r6d")))

(define-public crate-pid-lite-1 (crate (name "pid-lite") (vers "1.0.0") (hash "04ax9k8sddvhh7yh5mgsi0xyyba2qd8b8l6p0b6v489v6zsy09rk")))

(define-public crate-pid-lite-1 (crate (name "pid-lite") (vers "1.1.0") (hash "0b9hq4iq9b84frr1zm5v3zqc9z2x3r263ifjzvnlaa1n8ky6q0nq") (features (quote (("std") ("default" "std"))))))

(define-public crate-pid-lite-1 (crate (name "pid-lite") (vers "1.1.1") (hash "1pv0vifrj0smha7glf5zkpv5vi7mn6wg97h8gbci52zwwx0kjmln") (features (quote (("std") ("default" "std"))))))

(define-public crate-pid-lite-1 (crate (name "pid-lite") (vers "1.1.2") (deps (list (crate-dep (name "defmt") (req "^0.2") (optional #t) (default-features #t) (kind 0)))) (hash "18zxb4v2l2fy3hvzyax3g7n4r1mb70f71b16p4g1axz4ncxsw87p") (features (quote (("std") ("default" "std"))))))

(define-public crate-pid-lite-1 (crate (name "pid-lite") (vers "1.2.0") (deps (list (crate-dep (name "defmt") (req "^0.2") (optional #t) (default-features #t) (kind 0)))) (hash "0gx7vwi9jjbg2cvkkpzmpxd5vaywj4fnqwqpkihnmzqzrs0ganjj") (features (quote (("std") ("default" "std"))))))

(define-public crate-pid-loop-0.0.1 (crate (name "pid-loop") (vers "0.0.1") (hash "19zhngg618fs9862ixh7fby7lv7qakv70k82qvs6qmg4m0y6h1r2")))

(define-public crate-pid-loop-0.0.2 (crate (name "pid-loop") (vers "0.0.2") (hash "18vwyjicy5937wvpi63z3mp1qs4vgd7klf6950rq8aypxa7i8waw")))

(define-public crate-pid-loop-0.0.3 (crate (name "pid-loop") (vers "0.0.3") (hash "0znq2y6f9hwl8i43xqpfl990wipm3s7h1g5wi0dk1hn6wc8507kk")))

(define-public crate-pid-loop-0.0.4 (crate (name "pid-loop") (vers "0.0.4") (hash "0cfplscfvmhpz7xkaj4jf07m1bkd0w8dhjph2rjm4bq8v5x5wbm7")))

(define-public crate-pid-loop-0.0.5 (crate (name "pid-loop") (vers "0.0.5") (hash "1y8j5cbgwk47ymhlc73z3zj7ncmyh0f6hx7vnkdk5m9gndg8fnfv")))

(define-public crate-pid-loop-0.0.6 (crate (name "pid-loop") (vers "0.0.6") (hash "177j6v28w1d5vy2025k9k6ixcapm8npqnvsnw90qrrxi9wx0ig9d")))

(define-public crate-pid-set-0.1 (crate (name "pid-set") (vers "0.1.0") (deps (list (crate-dep (name "libc") (req "^0.2.153") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.58") (default-features #t) (kind 0)))) (hash "03s0rjb7q3d71s7yc6pp5j27vmpl3c3sjspgbmabczdnxw4j7bw9")))

(define-public crate-pid-set-0.1 (crate (name "pid-set") (vers "0.1.1") (deps (list (crate-dep (name "libc") (req "^0.2.153") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.58") (default-features #t) (kind 0)))) (hash "0hmbwanmzjrl95pnwk1xghxm6lizg9p6yxyjzhb2hcbr512jwiqc")))

(define-public crate-pid-set-0.1 (crate (name "pid-set") (vers "0.1.3") (deps (list (crate-dep (name "libc") (req "^0.2.153") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.58") (default-features #t) (kind 0)))) (hash "0xp6m2z3dkfb2wmjl4j2asdqrg565h89w2n677nx9h4z1phhzbi0")))

