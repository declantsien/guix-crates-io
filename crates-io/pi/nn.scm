(define-module (crates-io pi nn) #:use-module (crates-io))

(define-public crate-pinnable-0.1 (crate (name "pinnable") (vers "0.1.0") (hash "1jp8ym9xs5d0sk9y6l8fx5qawndxnaayza0i3ipvg22kg1irxg4n")))

(define-public crate-pinnable-0.1 (crate (name "pinnable") (vers "0.1.1") (hash "08ixgwapxybyn06gpw81mk5ahg40140xvaj53s36sf1dv9sm4rsj")))

(define-public crate-pinnacle-0.0.1 (crate (name "pinnacle") (vers "0.0.1") (deps (list (crate-dep (name "chrono") (req "^0.4") (features (quote ("serde"))) (default-features #t) (kind 0)) (crate-dep (name "displaydoc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.11") (features (quote ("json"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1") (features (quote ("serde_derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "serde_path_to_error") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "serde_urlencoded") (req "^0.7") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "anyhow") (req "^1") (default-features #t) (kind 2)) (crate-dep (name "clap") (req "^4") (features (quote ("derive" "env"))) (default-features #t) (kind 2)) (crate-dep (name "dotenvy") (req "^0.15") (default-features #t) (kind 2)) (crate-dep (name "rusty-hook") (req "^0.11") (default-features #t) (kind 2)) (crate-dep (name "tokio") (req "^1") (features (quote ("full"))) (default-features #t) (kind 2)))) (hash "17xs6jnfaisslcxpa3vvd9ixrmp22ndwk55f9p2wxi8dsw5yp5g2")))

(define-public crate-pinnacle-0.1 (crate (name "pinnacle") (vers "0.1.0") (deps (list (crate-dep (name "async-trait") (req "^0.1.68") (default-features #t) (kind 0)) (crate-dep (name "chrono") (req "^0.4") (features (quote ("serde"))) (default-features #t) (kind 0)) (crate-dep (name "displaydoc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.11") (features (quote ("json"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1") (features (quote ("serde_derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "serde_path_to_error") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "serde_urlencoded") (req "^0.7") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "anyhow") (req "^1") (default-features #t) (kind 2)) (crate-dep (name "clap") (req "^4") (features (quote ("derive" "env"))) (default-features #t) (kind 2)) (crate-dep (name "dotenvy") (req "^0.15") (default-features #t) (kind 2)) (crate-dep (name "rusty-hook") (req "^0.11") (default-features #t) (kind 2)) (crate-dep (name "tokio") (req "^1") (features (quote ("full"))) (default-features #t) (kind 2)))) (hash "0arggykb3lz7yjbkyr88vxvagz6h8sdpfzi2zqlan96rs0kbgn90")))

(define-public crate-pinnacle-0.1 (crate (name "pinnacle") (vers "0.1.1") (deps (list (crate-dep (name "async-trait") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "chrono") (req "^0.4") (features (quote ("serde"))) (default-features #t) (kind 0)) (crate-dep (name "displaydoc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.11") (features (quote ("json"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1") (features (quote ("serde_derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "serde_path_to_error") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "serde_urlencoded") (req "^0.7") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "anyhow") (req "^1") (default-features #t) (kind 2)) (crate-dep (name "clap") (req "^4") (features (quote ("derive" "env"))) (default-features #t) (kind 2)) (crate-dep (name "dotenvy") (req "^0.15") (default-features #t) (kind 2)) (crate-dep (name "rusty-hook") (req "^0.11") (default-features #t) (kind 2)) (crate-dep (name "tokio") (req "^1") (features (quote ("full"))) (default-features #t) (kind 2)))) (hash "1hjd01s9gq5z7qfrjhb3y323hsfjc2ab4q7qfj9n8y2v7ahjgada")))

(define-public crate-pinnacle-0.1 (crate (name "pinnacle") (vers "0.1.2") (deps (list (crate-dep (name "async-trait") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "chrono") (req "^0.4") (features (quote ("serde"))) (default-features #t) (kind 0)) (crate-dep (name "displaydoc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.11") (features (quote ("json"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1") (features (quote ("serde_derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "serde_path_to_error") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "serde_urlencoded") (req "^0.7") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "anyhow") (req "^1") (default-features #t) (kind 2)) (crate-dep (name "clap") (req "^4") (features (quote ("derive" "env"))) (default-features #t) (kind 2)) (crate-dep (name "dotenvy") (req "^0.15") (default-features #t) (kind 2)) (crate-dep (name "rusty-hook") (req "^0.11") (default-features #t) (kind 2)) (crate-dep (name "tokio") (req "^1") (features (quote ("full"))) (default-features #t) (kind 2)))) (hash "1p0zp84qlhrrq8wlhrrz79nf1r5s593xq78mq00kl4vdafx9gdib")))

(define-public crate-pinnacle-0.1 (crate (name "pinnacle") (vers "0.1.3") (deps (list (crate-dep (name "anyhow") (req "^1") (default-features #t) (kind 2)) (crate-dep (name "async-trait") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "chrono") (req "^0.4") (features (quote ("serde"))) (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^4") (features (quote ("derive" "env"))) (default-features #t) (kind 2)) (crate-dep (name "displaydoc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "dotenvy") (req "^0.15") (default-features #t) (kind 2)) (crate-dep (name "reqwest") (req "^0.11") (features (quote ("json"))) (kind 0)) (crate-dep (name "rusty-hook") (req "^0.11") (default-features #t) (kind 2)) (crate-dep (name "serde") (req "^1") (features (quote ("serde_derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "serde_path_to_error") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "serde_urlencoded") (req "^0.7") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1") (features (quote ("full"))) (default-features #t) (kind 2)))) (hash "0cjv41p080mma8yds1aiv4r561vlk64mdzz0hcdf7m5j24xm1xmq") (features (quote (("rustls" "reqwest/rustls-tls") ("native-tls" "reqwest/native-tls") ("default" "native-tls"))))))

(define-public crate-pinned-0.1 (crate (name "pinned") (vers "0.1.0") (deps (list (crate-dep (name "futures") (req "^0.3.24") (features (quote ("std" "async-await"))) (kind 0)) (crate-dep (name "rustversion") (req "^1.0.9") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.35") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.21.1") (features (quote ("full"))) (default-features #t) (kind 2)))) (hash "0nsrxs49dhjjz1gvg0pvac2rcidnwwd8l99y7vhwym2yv5xh4ad8") (rust-version "1.60.0")))

(define-public crate-pinned-aliasable-0.1 (crate (name "pinned-aliasable") (vers "0.1.0") (deps (list (crate-dep (name "pin-project-lite") (req "^0.2.7") (default-features #t) (kind 0)) (crate-dep (name "pin-utils") (req "^0.1.0") (default-features #t) (kind 2)))) (hash "1qnzqz5np70f6gh5w31w2vqzsylpzikgkh4a64pnqzc34qi66j77")))

(define-public crate-pinned-aliasable-0.1 (crate (name "pinned-aliasable") (vers "0.1.1") (deps (list (crate-dep (name "pin-project") (req "^1.0.7") (default-features #t) (kind 2)) (crate-dep (name "pin-project-lite") (req "^0.2.7") (default-features #t) (kind 0)) (crate-dep (name "pin-utils") (req "^0.1.0") (default-features #t) (kind 2)))) (hash "080ibjq91dm77a2m9p372l3l6hrpshkdsbw6bbcvdp9ax825kfbk")))

(define-public crate-pinned-aliasable-0.1 (crate (name "pinned-aliasable") (vers "0.1.2") (deps (list (crate-dep (name "pin-project") (req "^1.0.7") (default-features #t) (kind 2)) (crate-dep (name "pin-utils") (req "^0.1.0") (default-features #t) (kind 2)))) (hash "1vla4shmxyq028icdwji8zk1qkzddj3gb7syzm3qpfwr951zp7x0")))

(define-public crate-pinned-aliasable-0.1 (crate (name "pinned-aliasable") (vers "0.1.3") (deps (list (crate-dep (name "pin-project") (req "^1.0.7") (default-features #t) (kind 2)) (crate-dep (name "pin-utils") (req "^0.1.0") (default-features #t) (kind 2)))) (hash "0yqbhlmiqn3xwvr1s0a8akxb82bf5vmggwy1kav07vghkgl9l3sx")))

(define-public crate-pinned-bucket-0.1 (crate (name "pinned-bucket") (vers "0.1.0") (hash "06yhndhj94k9v75nzwdi069x1kv0lx0a6awkkmnq74388zjv7r7g")))

(define-public crate-pinned-bucket-0.1 (crate (name "pinned-bucket") (vers "0.1.1") (hash "19792sblqmsi3vy44xf9sb013hkgsgy8h4xfw5xin1hjsncqfd5s")))

(define-public crate-pinned-bucket-0.1 (crate (name "pinned-bucket") (vers "0.1.2") (hash "0q2964b7f3nc3cafb8x10sybgj9pmcbig37kwng1r5a7p6w7pnll")))

(define-public crate-pinned-bucket-0.1 (crate (name "pinned-bucket") (vers "0.1.3") (hash "0i1qr6wvy65cmjixfiywwddbvmh9qmckk5d2vcnwh680z56ci01n")))

(define-public crate-pinned-bucket-0.1 (crate (name "pinned-bucket") (vers "0.1.4") (hash "0gylcp3i436mjl7z53kv3z9iy3mzm8m9nf0wzz48wnlfj0pff1k3")))

(define-public crate-pinned-bucket-0.1 (crate (name "pinned-bucket") (vers "0.1.5") (hash "0bwq7i5732sk338x6pg5i0g0cp5qm5bh2lj0ylfirw6sdclwjm64")))

(define-public crate-pinned-bucket-0.2 (crate (name "pinned-bucket") (vers "0.2.0") (hash "0nynf6ypbagy5xf36crv3q46f2fipvjnbl65j1nwmkm9adwb337d")))

(define-public crate-pinned-bucket-0.2 (crate (name "pinned-bucket") (vers "0.2.1") (hash "0szcnhn604prk0cl7f4h0s4mxzhyn8320v58h64a72b9f6zrz2yy")))

(define-public crate-pinned-bucket-0.2 (crate (name "pinned-bucket") (vers "0.2.2") (hash "0q9wfcn56k7jny9sczwf5l9g16n6wc6jh3dsgzz93v4zi8cmqf89")))

(define-public crate-pinned-bucket-0.3 (crate (name "pinned-bucket") (vers "0.3.0") (hash "105vax84739jvfam43ncj9npan03ajrqlljrj6cbzzrb1n5rapm0")))

(define-public crate-pinned-init-0.0.0 (crate (name "pinned-init") (vers "0.0.0") (deps (list (crate-dep (name "pin-project") (req "^1.0.0") (default-features #t) (kind 0)) (crate-dep (name "pinned-init-macro") (req "=0.0.0") (default-features #t) (kind 0)))) (hash "1q81kp1gzam6v646q5wmw885q904b44xnfpskrr47px5zngyppbn") (features (quote (("std" "alloc") ("default" "std") ("alloc")))) (yanked #t)))

(define-public crate-pinned-init-0.0.1 (crate (name "pinned-init") (vers "0.0.1") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 2)))) (hash "0djzd7jg06hy45dc5lfzdll32s5ppqqzji28f2mkckn42cihj871") (features (quote (("std") ("never_type") ("default" "std") ("alloc")))) (yanked #t)))

(define-public crate-pinned-init-0.0.2 (crate (name "pinned-init") (vers "0.0.2") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 2)))) (hash "11y0xfvaczk1y26ngna59z7b219hdjw3c9k6qfcz7yjrlq1yn7yr") (features (quote (("std") ("default" "std") ("alloc")))) (yanked #t)))

(define-public crate-pinned-init-0.0.3 (crate (name "pinned-init") (vers "0.0.3") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 2)) (crate-dep (name "pinned-init-macro") (req "=0.0.1") (default-features #t) (kind 0)))) (hash "19w9w2mnmy6si2pv00yj80bhqcach5mjhrf8jgmwj55rhw41ag9r") (features (quote (("std") ("default" "std") ("alloc"))))))

(define-public crate-pinned-init-0.0.4 (crate (name "pinned-init") (vers "0.0.4") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 2)) (crate-dep (name "pinned-init-macro") (req "=0.0.2") (default-features #t) (kind 0)))) (hash "0qxh8rk57h8aaqf89n3bij9vplsqvgf7mdvy44wgazslsh26zbh4") (features (quote (("std" "alloc") ("default" "std") ("alloc"))))))

(define-public crate-pinned-init-0.0.5 (crate (name "pinned-init") (vers "0.0.5") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 2)) (crate-dep (name "pinned-init-macro") (req "=0.0.3") (default-features #t) (kind 0)))) (hash "1hgw4a4ynpa57d57wk0mxxi1vrlfayyq5vj009yd52f4vl60pfsm") (features (quote (("std" "alloc") ("default" "std") ("alloc"))))))

(define-public crate-pinned-init-0.0.6 (crate (name "pinned-init") (vers "0.0.6") (deps (list (crate-dep (name "compiletest_rs") (req "^0.9") (default-features #t) (kind 2)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 2)) (crate-dep (name "pinned-init-macro") (req "=0.0.4") (default-features #t) (kind 0)))) (hash "17899aij40k3gmhjv4nrv5x4dri9ncy1am8ivkrwbsb1zxm1ji41") (features (quote (("std" "alloc") ("default" "std") ("alloc"))))))

(define-public crate-pinned-init-0.0.7 (crate (name "pinned-init") (vers "0.0.7") (deps (list (crate-dep (name "compiletest_rs") (req "^0.10") (default-features #t) (kind 2)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 2)) (crate-dep (name "paste") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "pinned-init-macro") (req "=0.0.5") (default-features #t) (kind 0)))) (hash "1plwyfrmrlrvqv6q4mc1fi56x2z6fz60q89czxbpbv3gh082c3fj") (features (quote (("std" "alloc") ("default" "std") ("alloc"))))))

(define-public crate-pinned-init-macro-0.0.0 (crate (name "pinned-init-macro") (vers "0.0.0") (deps (list (crate-dep (name "proc-macro-error") (req "^1.0.0") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1.0.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.0") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "0k1l5s40vcxinnxrj17vwpv80sr79qk0hpwg0mhncq7bxkj9digk") (yanked #t)))

(define-public crate-pinned-init-macro-0.0.1 (crate (name "pinned-init-macro") (vers "0.0.1") (hash "0g0jgf7mhrghwbdwqbla2is2qrbcv2nm4bghfaw86c8b7xqnz964")))

(define-public crate-pinned-init-macro-0.0.2 (crate (name "pinned-init-macro") (vers "0.0.2") (hash "0mkz71wbyvrfcxzjjv7jm9p45x52x3myfyd8614qzzmasdpdpghd")))

(define-public crate-pinned-init-macro-0.0.3 (crate (name "pinned-init-macro") (vers "0.0.3") (deps (list (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)))) (hash "0dfi1qll6g8z4fv4p3ail020mgsgwkpxm94cyqprwnp6p3whsjhf")))

(define-public crate-pinned-init-macro-0.0.4 (crate (name "pinned-init-macro") (vers "0.0.4") (deps (list (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)))) (hash "04dzrcm8m2w55rwrlnmd4k6bpf972xy39b01f3vlzr5akz8649vd")))

(define-public crate-pinned-init-macro-0.0.5 (crate (name "pinned-init-macro") (vers "0.0.5") (deps (list (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)))) (hash "1m4b2daypxs0rarbsqapvjfawc5sk4xgh1qmdk2ccd331v7kz6jn")))

(define-public crate-pinned-mutex-0.1 (crate (name "pinned-mutex") (vers "0.1.0") (deps (list (crate-dep (name "parking_lot") (req "^0.12") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "pin-project") (req "^1.1") (default-features #t) (kind 2)))) (hash "1xw29kkjs00a2hgj4lbkaa0gz8553dpf7fw863nqk5ixr7q5g2zn") (rust-version "1.60")))

(define-public crate-pinned-mutex-0.2 (crate (name "pinned-mutex") (vers "0.2.0") (deps (list (crate-dep (name "parking_lot") (req "^0.12") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "pin-project") (req "^1.1") (default-features #t) (kind 2)))) (hash "198d3nns5iabvwshkrd8wr506arv144g8w1mnwij8hz2mz0vmkin") (rust-version "1.60")))

(define-public crate-pinned-mutex-0.3 (crate (name "pinned-mutex") (vers "0.3.0") (deps (list (crate-dep (name "parking_lot") (req "^0.12") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "pin-project") (req "^1.1") (default-features #t) (kind 2)))) (hash "0hf3s1splhpahggnjwhgkv87rci8vjwma9lpcdm9fv7xzqc9hl5j") (rust-version "1.60")))

(define-public crate-pinned-queue-0.1 (crate (name "pinned-queue") (vers "0.1.0") (hash "0x7i19pk3gi8s0qvzj50hd03gh7c2f1jmbsdqxpf9n7s91zd8llb")))

(define-public crate-pinned-queue-0.1 (crate (name "pinned-queue") (vers "0.1.1") (hash "0yaxc121ynpc29wl2ygzingmpay3b97xfmq1py5pq8qzqzanwhx2")))

(define-public crate-pinned-repos-0.1 (crate (name "pinned-repos") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^3.2.21") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.11") (features (quote ("json"))) (default-features #t) (kind 0)) (crate-dep (name "scraper") (req "^0.13.0") (default-features #t) (kind 0)) (crate-dep (name "tabled") (req "^0.8.0") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "1vnysvpyabgy00lvpk1xmky8i4f5jgv6dl9xybxdac8q8z6jlmb7")))

(define-public crate-pinned_slab-0.1 (crate (name "pinned_slab") (vers "0.1.0") (deps (list (crate-dep (name "arrayvec") (req "^0.4") (default-features #t) (kind 0)))) (hash "12lx7ldxwppjcxnmyqq8ivpcdla1vparwc9x2p0hyb0c3r58md2z")))

(define-public crate-pinned_sync-0.0.1 (crate (name "pinned_sync") (vers "0.0.1") (deps (list (crate-dep (name "cfg-if") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (target "cfg(unix)") (kind 0)) (crate-dep (name "rand") (req "^0.8") (default-features #t) (kind 2)))) (hash "03153ccv1h2wc637b3maxxqbihbhl5bp0z2mk7vsm1zh22hgaz4n")))

(define-public crate-pinned_vec-0.1 (crate (name "pinned_vec") (vers "0.1.0") (hash "0ic2wwl97wcl1mc340nvbid9c8ssagvi4hxaczxkxddm1y4qhhvi")))

(define-public crate-pinned_vec-0.1 (crate (name "pinned_vec") (vers "0.1.1") (hash "12il2y234k9r4i3v9yh7qpavrwdhi48b057z96hb0bv2j8nxi2i6")))

