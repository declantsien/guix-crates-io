(define-module (crates-io pi -p) #:use-module (crates-io))

(define-public crate-pi-pinout-0.1 (crate (name "pi-pinout") (vers "0.1.0") (hash "0is9g957jahzfham662pyam9bbfbvfa249yddlgf60viqr5kgnc8")))

(define-public crate-pi-pinout-0.1 (crate (name "pi-pinout") (vers "0.1.1") (hash "1mdgnl8b9sy9pkkfs989nvn0ypmx0gc29lfzxcn492yyrar24c25")))

(define-public crate-pi-pinout-0.1 (crate (name "pi-pinout") (vers "0.1.2") (deps (list (crate-dep (name "serde") (req "^1.0.145") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "06ffgjry5kyfzzg6h725nir1zij5my4rar2b16zlbqgvpq8sh10z")))

(define-public crate-pi-pinout-0.1 (crate (name "pi-pinout") (vers "0.1.3") (deps (list (crate-dep (name "serde") (req "^1.0.145") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "0i3jkb0xga6wyw58afm22vaily5xi7py09h6nv0dwgn7f390sv9k")))

