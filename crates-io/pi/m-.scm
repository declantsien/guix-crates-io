(define-module (crates-io pi m-) #:use-module (crates-io))

(define-public crate-pim-eventsource-0.1 (crate (name "pim-eventsource") (vers "0.1.0") (deps (list (crate-dep (name "async-std") (req "^1.6.0") (features (quote ("attributes"))) (default-features #t) (kind 0)) (crate-dep (name "crossbeam-channel") (req "^0.4.4") (default-features #t) (kind 0)) (crate-dep (name "dirs") (req "^3.0.1") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.115") (default-features #t) (kind 0)) (crate-dep (name "serde_cbor") (req "^0.11.1") (default-features #t) (kind 0)) (crate-dep (name "sled") (req "^0.34.3") (default-features #t) (kind 0)) (crate-dep (name "tide") (req "^0.13.0") (default-features #t) (kind 0)))) (hash "00h7jccdzbhsmijpb090k7jzis0lilzk54h7aiwf3grgx47b4gyk")))

