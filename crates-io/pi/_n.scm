(define-module (crates-io pi _n) #:use-module (crates-io))

(define-public crate-pi_new-0.1 (crate (name "pi_new") (vers "0.1.1") (deps (list (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)))) (hash "1v46zg5rhw7fyx98mjx55n0n630yrcm6q5qmwpy00kvdb98rb9n6")))

(define-public crate-pi_new-0.1 (crate (name "pi_new") (vers "0.1.4") (deps (list (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)))) (hash "1s4pg0rq4j6qrh5i1c345y6s267y7qgklbkznh09r88crcyk1ifn")))

(define-public crate-pi_new-0.1 (crate (name "pi_new") (vers "0.1.5") (deps (list (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)))) (hash "0nyf2b2jwqslfawk8zqldxflj9y4svn6jj6xm0hvg5yyp5cx2xad")))

(define-public crate-pi_new-1 (crate (name "pi_new") (vers "1.1.6") (deps (list (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)))) (hash "1yg6misnyd8f2q3b3yv2ydl78dyclan0mg7ms5jl85qj15yv5apk")))

(define-public crate-pi_new-1 (crate (name "pi_new") (vers "1.1.7") (deps (list (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)))) (hash "0wg3ga6y2ikicjx8k0m0ps9hrwgkp999lvi4nidj5rr4v4qy71wh")))

(define-public crate-pi_new-1 (crate (name "pi_new") (vers "1.1.8") (deps (list (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)))) (hash "10fcymd90kr5lqkwnmn1x7gv70iczk0cmchppnd3xlfvxa0aaml2")))

(define-public crate-pi_new-0.1 (crate (name "pi_new") (vers "0.1.0") (deps (list (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)))) (hash "02mkqgr2n1hwpbn9fkj1pkhjidr6mj6bmdkz1ijxqnlxhmpmarbd")))

(define-public crate-pi_new-0.1 (crate (name "pi_new") (vers "0.1.2") (deps (list (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)))) (hash "1aqdlyzyaaggqd8alp98q3kagy1xrb902j2hza26c72fcrpnp8s0")))

(define-public crate-pi_new-0.1 (crate (name "pi_new") (vers "0.1.6") (deps (list (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)))) (hash "1mx6rrdp5bvdhinlrq31z4lcdnaqqmzm53njh0j6x8sqa8cbkh3v")))

(define-public crate-pi_new-0.1 (crate (name "pi_new") (vers "0.1.7") (deps (list (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)))) (hash "1nd8ix1lg2r69nmhwxwb0x1jhl6zaxnrsk16b61k51icp09igbyv")))

(define-public crate-pi_null-0.1 (crate (name "pi_null") (vers "0.1.0") (hash "1ydvvqnrgp4b4saz4xm76g2r1fvvh2dvxvqh9jdz58rqk253f97b")))

(define-public crate-pi_null-0.2 (crate (name "pi_null") (vers "0.2.0") (hash "0cx37nchsnn1wp70g19d0c76hznlg0x39slcrq3vw8basb1hns38") (yanked #t) (rust-version "1.70")))

(define-public crate-pi_null-0.2 (crate (name "pi_null") (vers "0.2.1") (hash "1gvkn6rpk89xdd68fimzf8fqp5c8w1zzspb7160gawn7a2q862ad") (yanked #t) (rust-version "1.72")))

(define-public crate-pi_null-0.1 (crate (name "pi_null") (vers "0.1.2") (hash "03anhc8hiqyv7czmwd11nz0nf2a474nl0brs6fxihkcq12qk0a89")))

(define-public crate-pi_null-0.1 (crate (name "pi_null") (vers "0.1.3") (hash "0am42cvlfj9d9xyyrwf4z1cjs2kddfa3yxk7h304ijx7gpxn2v0v")))

(define-public crate-pi_null-0.1 (crate (name "pi_null") (vers "0.1.4") (hash "02jka598773r6419jk98ymik8602vvd8m5bfwnjnyp8r7j00ggij")))

(define-public crate-pi_null-0.1 (crate (name "pi_null") (vers "0.1.5") (hash "0c5iiqbkydr64pl42kj1rkll4fhhv61gnvrj2mky9qns85gaxg37")))

(define-public crate-pi_null-0.1 (crate (name "pi_null") (vers "0.1.6") (hash "0mz4fr53kz80rd8n2dcdc0wc5ji2zlykqjs4nqrj59z7dmmnr3f3")))

(define-public crate-pi_null-0.1 (crate (name "pi_null") (vers "0.1.7") (hash "118wskggvc44qcr5lbvvamg49cgxd3hlir1q5wvkzhr6zvkfl9ks")))

(define-public crate-pi_null-0.1 (crate (name "pi_null") (vers "0.1.8") (hash "0h3m2d79i4c29wb2kkr60mx7r51kij18x8yps7b1h76cx9acvc2s")))

(define-public crate-pi_null-0.1 (crate (name "pi_null") (vers "0.1.9") (hash "1pb201r6xia730sk5522rzflqcg2fis3rwkcnhwppw57d2j7y8mf")))

(define-public crate-pi_null-0.1 (crate (name "pi_null") (vers "0.1.10") (hash "0fc96xjamnv2xhnjiz9r3hinynr5vvfh62dh0cqiq0i000f77lbm")))

(define-public crate-pi_null-0.1 (crate (name "pi_null") (vers "0.1.11") (hash "0swba1zpklb0zqr194m9fijzs23xcj965hv8n1amfph2p8xfka0r")))

