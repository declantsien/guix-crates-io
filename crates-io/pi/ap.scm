(define-module (crates-io pi ap) #:use-module (crates-io))

(define-public crate-piapprec-0.1 (crate (name "piapprec") (vers "0.1.0") (deps (list (crate-dep (name "arrayref") (req "^0.3.6") (default-features #t) (kind 0)) (crate-dep (name "sha2") (req "^0.10.0") (default-features #t) (kind 0)))) (hash "1mvwb5zcx4kvzlmd88jd6mg4x4kw675in5x00ahfhdm4kq3qlkzq")))

(define-public crate-piapprec-0.1 (crate (name "piapprec") (vers "0.1.1") (deps (list (crate-dep (name "arrayref") (req "^0.3.6") (default-features #t) (kind 0)) (crate-dep (name "sha2") (req "^0.10.0") (default-features #t) (kind 0)))) (hash "0nhklirqqc83lb3b1bsyw72v6vf7lvbsdab9y2s72gi3n8ps7iij")))

