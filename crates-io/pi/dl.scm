(define-module (crates-io pi dl) #:use-module (crates-io))

(define-public crate-pidlock-0.1 (crate (name "pidlock") (vers "0.1.0") (deps (list (crate-dep (name "libc") (req "^0.2.40") (default-features #t) (kind 0)))) (hash "1byvqabfh29d33gckjl1csf1qrmi2hh7h0v6ixxp28lahdj8fxb7")))

(define-public crate-pidlock-0.1 (crate (name "pidlock") (vers "0.1.1") (deps (list (crate-dep (name "libc") (req "^0.2.40") (default-features #t) (kind 0)))) (hash "1b7zqs11q6fxl3x55mb0zkzg5pd9x0rwqncbh86lk86whcipj9h9")))

(define-public crate-pidlock-0.1 (crate (name "pidlock") (vers "0.1.2") (deps (list (crate-dep (name "libc") (req "^0.2.40") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.1") (default-features #t) (kind 0)))) (hash "19ih1i40b0jgl71cyh5a3aa08304msssvb6ppgwsxzmpddhqczaf")))

(define-public crate-pidlock-0.1 (crate (name "pidlock") (vers "0.1.3") (deps (list (crate-dep (name "libc") (req "^0.2.40") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.1") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.2") (default-features #t) (kind 2)))) (hash "0ipbdjaqd242nzgx9l16kkbnqb2j0axkadqrkwdkwgqx6yp9d5zh")))

(define-public crate-pidlock-0.1 (crate (name "pidlock") (vers "0.1.4") (deps (list (crate-dep (name "libc") (req "^0.2.40") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.1") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.2") (default-features #t) (kind 2)))) (hash "1rnvm17bhm2c7rd610cvq422gpb986anxm7bbxgb78zy0sal40wq") (features (quote (("strict") ("default" "strict"))))))

(define-public crate-pidlock-0.1 (crate (name "pidlock") (vers "0.1.6") (deps (list (crate-dep (name "libc") (req "^0.2.40") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.1") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.2") (default-features #t) (kind 2)) (crate-dep (name "windows-sys") (req "^0.45.0") (features (quote ("Win32_System_Threading" "Win32_Foundation"))) (default-features #t) (target "cfg(windows)") (kind 0)))) (hash "1a26h0pjf8vl9nrvapf3582qv6067nm0jk30ivf58zcz9wdbslyz") (features (quote (("strict") ("default"))))))

