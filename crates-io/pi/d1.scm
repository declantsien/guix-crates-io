(define-module (crates-io pi d1) #:use-module (crates-io))

(define-public crate-pid1-0.1 (crate (name "pid1") (vers "0.1.0") (deps (list (crate-dep (name "nix") (req "^0.27.1") (features (quote ("process" "signal"))) (default-features #t) (kind 0)) (crate-dep (name "signal-hook") (req "^0.3.17") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1") (default-features #t) (kind 0)))) (hash "07xax72m751kwxbkq29ws75zlb0l3i24g9qlcc4v7i1560cii27p")))

(define-public crate-pid1-0.1 (crate (name "pid1") (vers "0.1.1") (deps (list (crate-dep (name "nix") (req "^0.27.1") (features (quote ("process" "signal"))) (default-features #t) (kind 0)) (crate-dep (name "signal-hook") (req "^0.3.17") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1") (default-features #t) (kind 0)))) (hash "0x0lz3zidzj4004yngxz46igq59vzq7lfcrhn24vwjhqbvi4lkyl")))

(define-public crate-pid1-exe-0.1 (crate (name "pid1-exe") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^4.4.6") (features (quote ("std" "derive" "help"))) (kind 0)) (crate-dep (name "pid1") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "signal-hook") (req "^0.3.17") (default-features #t) (kind 0)))) (hash "137r1r3ncgnyiz4m0halfm1a7ygnfxxbgnrsyvpw06igcnrh1bgb")))

(define-public crate-pid1-exe-0.1 (crate (name "pid1-exe") (vers "0.1.1") (deps (list (crate-dep (name "clap") (req "^4.4.6") (features (quote ("std" "derive" "help"))) (kind 0)) (crate-dep (name "pid1") (req "^0.1.1") (default-features #t) (kind 0)) (crate-dep (name "signal-hook") (req "^0.3.17") (default-features #t) (kind 0)))) (hash "1rbdx2np910n8r6p4xhqbh3l54fbq3ip7ga2n87x9hmzsz3nlabw")))

(define-public crate-pid1-exe-0.1 (crate (name "pid1-exe") (vers "0.1.2") (deps (list (crate-dep (name "clap") (req "^4.4.6") (features (quote ("std" "derive" "help"))) (kind 0)) (crate-dep (name "pid1") (req "^0.1.1") (default-features #t) (kind 0)) (crate-dep (name "signal-hook") (req "^0.3.17") (default-features #t) (kind 0)))) (hash "0qv982pwlr62z1c09ciin8lalqcgz56385dw5acqy5waiwp5ivwf")))

(define-public crate-pid1-exe-0.1 (crate (name "pid1-exe") (vers "0.1.3") (deps (list (crate-dep (name "clap") (req "^4.4.6") (features (quote ("std" "derive" "help"))) (kind 0)) (crate-dep (name "pid1") (req "^0.1.1") (default-features #t) (kind 0)) (crate-dep (name "signal-hook") (req "^0.3.17") (default-features #t) (kind 0)))) (hash "0g9k8vmjqv5s3mzbcgkqgxbpjxp2myv0c544cfq7gj1m3mzcskaf")))

