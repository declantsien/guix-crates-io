(define-module (crates-io pi ss) #:use-module (crates-io))

(define-public crate-piss-0.1 (crate (name "piss") (vers "0.1.0") (deps (list (crate-dep (name "image") (req "^0.23") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.3") (default-features #t) (kind 0)))) (hash "177br37szs860n99y2cqylv2nicgs3j9a1l6b030vzdranfrknah")))

(define-public crate-piss-0.1 (crate (name "piss") (vers "0.1.1") (deps (list (crate-dep (name "image") (req "^0.23") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.3") (default-features #t) (kind 0)))) (hash "1l1zab40jlmbm6kr0hwyarxkdfh7m05qwj1qsnm23wp6d7a52gx3")))

(define-public crate-piss-0.1 (crate (name "piss") (vers "0.1.2") (deps (list (crate-dep (name "image") (req "^0.23") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.3") (default-features #t) (kind 0)))) (hash "1c4jd1hzamqai8nwiz04si6hv65cjqzqip6ydzyzw9q1n4hbawxb")))

(define-public crate-pisswhacker-0.0.0 (crate (name "pisswhacker") (vers "0.0.0") (hash "1rcxha8h8l2qd13xa4q9i9h0kqbhlp1yzlqqw8f6f0hgi3bdsgv8")))

