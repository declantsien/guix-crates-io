(define-module (crates-io pi kc) #:use-module (crates-io))

(define-public crate-pikchr-0.1 (crate (name "pikchr") (vers "0.1.0") (deps (list (crate-dep (name "cc") (req "^1.0") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "0jjy15lsqsmzpahf39a36f5dd1p844xrpwsd5hn94mh4sbazy2sh")))

(define-public crate-pikchr-0.1 (crate (name "pikchr") (vers "0.1.1") (deps (list (crate-dep (name "cc") (req "^1.0") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "1lm6924k84jdwsyjf029r2xwz23dmm19mryb51jaj9q29f9n001w")))

(define-public crate-pikchr-0.1 (crate (name "pikchr") (vers "0.1.2") (deps (list (crate-dep (name "cc") (req "^1.0") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "1jybhjps3b7rksv3brzrcwflmbq0j9yp6yb8skckw51fa6rd09jf")))

(define-public crate-pikchr-0.1 (crate (name "pikchr") (vers "0.1.3") (deps (list (crate-dep (name "cc") (req "^1.0") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "0vn91nmxpp3b9pbp19mds6n4clq0xzii10i4rli4xb6zl1qb8c5l")))

(define-public crate-pikchr-cli-0.1 (crate (name "pikchr-cli") (vers "0.1.2") (deps (list (crate-dep (name "clap") (req "^4.3.21") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "pikchr") (req "^0.1.2") (default-features #t) (kind 0)))) (hash "0lh2qcbwpihaigf9h9psgn3qsmh6czqmnkllcg4qgm81cmic785k")))

(define-public crate-pikchr-sys-0.1 (crate (name "pikchr-sys") (vers "0.1.0") (deps (list (crate-dep (name "bindgen") (req "^0.53") (features (quote ("runtime"))) (kind 1)) (crate-dep (name "cc") (req "^1.0") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 2)))) (hash "1g5vba74qqbbikq0klqjhf644fmdydhll5f8a0nkfd1d14lcchrv")))

(define-public crate-pikchr-sys-0.1 (crate (name "pikchr-sys") (vers "0.1.1") (deps (list (crate-dep (name "bindgen") (req "^0.53") (features (quote ("runtime"))) (kind 1)) (crate-dep (name "cc") (req "^1.0") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 2)))) (hash "0gjmkllad1c1d00arcvz3w27nyp2q9nwc3pms6lpk21y8c26rsp7")))

