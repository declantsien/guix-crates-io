(define-module (crates-io pi c2) #:use-module (crates-io))

(define-public crate-pic2a4-0.1 (crate (name "pic2a4") (vers "0.1.0") (deps (list (crate-dep (name "anyhow") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^3.2") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "image") (req "^0.24") (default-features #t) (kind 0)) (crate-dep (name "printpdf") (req "^0.5") (features (quote ("embedded_images"))) (default-features #t) (kind 0)))) (hash "0zjwc0kifxrnfnpqw18am8w67862zaqvvj7i24jkim049vqvvxpj")))

(define-public crate-pic2lcd-0.1 (crate (name "pic2lcd") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^2.33") (default-features #t) (kind 0)) (crate-dep (name "image") (req "^0.23") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.3") (default-features #t) (kind 0)))) (hash "116fi4i6f8zi689i0g79x0bpcf9wy2zb21ns94wjxin40128cdpn")))

(define-public crate-pic2txt-1 (crate (name "pic2txt") (vers "1.0.0") (deps (list (crate-dep (name "futures") (req "^0.3.5") (default-features #t) (kind 0)) (crate-dep (name "windows") (req "^0.48.0") (features (quote ("Media_Ocr" "Graphics_Imaging" "Storage_Streams" "Foundation"))) (default-features #t) (kind 0)))) (hash "083aia6kl5jg53bf9469mdgkzcqvwgaj69d85sma1qdaippgk7cw")))

