(define-module (crates-io pi ks) #:use-module (crates-io))

(define-public crate-piksels-0.0.0 (crate (name "piksels") (vers "0.0.0") (hash "04iy94dvgsw14pbhdjn2h8n7k3ch50940h2divjydr6f4pjhb0gl") (rust-version "1.72")))

(define-public crate-piksels-backend-0.0.0 (crate (name "piksels-backend") (vers "0.0.0") (hash "0hh7lpccgzqhdws7jpbp286hilkw4xbi768igfi74yi7ww8d74gh") (rust-version "1.72")))

(define-public crate-piksels-core-0.0.0 (crate (name "piksels-core") (vers "0.0.0") (hash "14rwy2wicf6a55wnqhwah9qhggcz47ihyk3vvjkpajna7ycb8xkb") (rust-version "1.72")))

