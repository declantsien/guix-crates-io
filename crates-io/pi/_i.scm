(define-module (crates-io pi _i) #:use-module (crates-io))

(define-public crate-pi_idtree-0.1 (crate (name "pi_idtree") (vers "0.1.0") (deps (list (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "pi_map") (req "^0.1") (default-features #t) (kind 0)))) (hash "0rky3aysmajnwvhlf0dmj8h7dzsk5zjahpgf3affp5b6xwwmp1l4")))

(define-public crate-pi_info-0.1 (crate (name "pi_info") (vers "0.1.0") (hash "1hc6g7h3raf4ynkyx6afr3v1m72h86mdangvb8m9n76lrn2p43h1")))

(define-public crate-pi_info-0.1 (crate (name "pi_info") (vers "0.1.1") (hash "0fkxvylfwaxqy8had9c7bvr5p1swz6q4an5mqg1n0wq56qqlw1i7")))

(define-public crate-pi_info-0.1 (crate (name "pi_info") (vers "0.1.2") (hash "0447rkb7yf6020py03xqdl0kq5wrd8w4wb9nczb9cdm22bn6di9w")))

(define-public crate-pi_info-0.1 (crate (name "pi_info") (vers "0.1.3") (hash "1pibq8wmzwrs27nqsm0z2c3x8523iwf89pjys6llp6h94vslf0ya")))

(define-public crate-pi_ir_remote-0.1 (crate (name "pi_ir_remote") (vers "0.1.0") (deps (list (crate-dep (name "gpio-cdev") (req ">=0.4.0, <0.5.0") (default-features #t) (kind 0)) (crate-dep (name "stoppable_thread") (req ">=0.2.1, <0.3.0") (default-features #t) (kind 0)))) (hash "0ykj55nahdx7ifnc5y8cq6hn1lffv9c850iscln49pwxgp0nk8r7")))

