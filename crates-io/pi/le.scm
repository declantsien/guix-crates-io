(define-module (crates-io pi le) #:use-module (crates-io))

(define-public crate-pile-0.1 (crate (name "pile") (vers "0.1.0") (deps (list (crate-dep (name "stable_deref_trait") (req "^1") (default-features #t) (kind 0)))) (hash "0gja023y7lqplwdjrsxi0yqabjg6f8421mjx6bz0bxpx14gcql50")))

(define-public crate-pileks_uniffi-bindgen-cs-0.2 (crate (name "pileks_uniffi-bindgen-cs") (vers "0.2.1") (deps (list (crate-dep (name "anyhow") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "askama") (req "^0.11") (features (quote ("config"))) (kind 0)) (crate-dep (name "camino") (req "^1.0.8") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^3.1") (features (quote ("cargo" "std" "derive"))) (default-features #t) (kind 0)) (crate-dep (name "extend") (req "^1.1") (default-features #t) (kind 0)) (crate-dep (name "fs-err") (req "^2.7.0") (default-features #t) (kind 0)) (crate-dep (name "heck") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "paste") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.0") (default-features #t) (kind 0)) (crate-dep (name "toml") (req "^0.5") (default-features #t) (kind 0)) (crate-dep (name "uniffi_bindgen") (req "^0.23.0") (default-features #t) (kind 0)))) (hash "0l63xp5w0b922l62667jq4k3szjv0cxzdb47ihjyxhm85np4z7hj")))

(define-public crate-pilercr-parser-1 (crate (name "pilercr-parser") (vers "1.0.0") (deps (list (crate-dep (name "nom") (req "^7.1.3") (default-features #t) (kind 0)))) (hash "0jimpfwikp8212q5r34cwm0pdq6qgakxcnshkpf3b0x9kba3d6rd")))

(define-public crate-pilercr-parser-1 (crate (name "pilercr-parser") (vers "1.0.1") (deps (list (crate-dep (name "nom") (req "^7.1.3") (default-features #t) (kind 0)))) (hash "02lbkb33z39hs2d1lwkjc0nd4cpivrwa50ywjcxw80s36hy3a8ws")))

(define-public crate-pilercr-parser-1 (crate (name "pilercr-parser") (vers "1.0.2") (deps (list (crate-dep (name "nom") (req "^7.1.3") (default-features #t) (kind 0)))) (hash "01c1hrb76xw70lmm4z52dhhwynwaycqsprgrgr7540kbiq0h6v8y")))

(define-public crate-pilercr-parser-1 (crate (name "pilercr-parser") (vers "1.1.0") (deps (list (crate-dep (name "nom") (req "^7.1.3") (default-features #t) (kind 0)))) (hash "1nycha5shq3md6v618xrfnfni40s0ivli4zgpm3qf421ba349ihl")))

