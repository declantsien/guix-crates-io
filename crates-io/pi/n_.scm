(define-module (crates-io pi n_) #:use-module (crates-io))

(define-public crate-pin_array-0.1 (crate (name "pin_array") (vers "0.1.0") (deps (list (crate-dep (name "static_assertions") (req "^1.1.0") (default-features #t) (kind 2)))) (hash "1bjkpqk6l6951rxya11cdvw2m0addl7nppgsl9jsj5wi0frp8ydq")))

(define-public crate-pin_array-0.1 (crate (name "pin_array") (vers "0.1.1") (deps (list (crate-dep (name "static_assertions") (req "^1.1.0") (default-features #t) (kind 2)))) (hash "06ld9hmr60jhn84ayiqr8xh2hnaqgcqq8y239jd3cj7pp7zxsx18")))

(define-public crate-pin_tree-0.1 (crate (name "pin_tree") (vers "0.1.0") (hash "094i0asqvli68bqp0d0jpcdc0kzwg1pzpiypn98c5ns0wz3y3r9g")))

(define-public crate-pin_tree-0.2 (crate (name "pin_tree") (vers "0.2.0") (hash "08yrghya8gp6lflbn46dgvyq91ixb213snnigrpb5shb0d3cb4b3")))

