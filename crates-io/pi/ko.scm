(define-module (crates-io pi ko) #:use-module (crates-io))

(define-public crate-piko-0.1 (crate (name "piko") (vers "0.1.0") (deps (list (crate-dep (name "futures") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "gfx-hal") (req "^0.5") (default-features #t) (kind 0)))) (hash "12l25h0qh5z51kvjn0xlzqzazjgvsy7m7z4nqh7vlvw5mrg2g9ny")))

