(define-module (crates-io pi xm) #:use-module (crates-io))

(define-public crate-pixman-0.1 (crate (name "pixman") (vers "0.1.0") (deps (list (crate-dep (name "drm-fourcc") (req "^2.2.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "image") (req "^0.24.7") (default-features #t) (kind 2)) (crate-dep (name "paste") (req "^1.0.14") (default-features #t) (kind 0)) (crate-dep (name "pixman-sys") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.50") (default-features #t) (kind 0)))) (hash "1dji0jgvhj63fnyndzp769svhn63lcvihp297bjg857c1gd28jnj") (features (quote (("default")))) (v 2) (features2 (quote (("drm-fourcc" "dep:drm-fourcc")))) (rust-version "1.65.0")))

(define-public crate-pixman-sys-0.1 (crate (name "pixman-sys") (vers "0.1.0") (hash "1nja8kc7zs1w4lhllvsgssa0b07n4cgwb0zyvqapj7g8i4z4i851") (rust-version "1.65.0")))

