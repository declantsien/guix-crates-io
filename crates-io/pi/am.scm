(define-module (crates-io pi am) #:use-module (crates-io))

(define-public crate-piam-0.0.0 (crate (name "piam") (vers "0.0.0") (hash "0y6n4vx5f7nny0lnsn4135ll4dqy5nrdrkm67qci36k3gdkyran8") (rust-version "1.62.0")))

(define-public crate-piam-types-0.0.0 (crate (name "piam-types") (vers "0.0.0") (deps (list (crate-dep (name "anyhow") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "aws-sigv4") (req "^0.48.0") (default-features #t) (kind 0)) (crate-dep (name "aws-smithy-checksums") (req "^0.48.0") (default-features #t) (kind 0)) (crate-dep (name "cidr") (req "^0.2.1") (default-features #t) (kind 0)) (crate-dep (name "http") (req "^0.2.8") (default-features #t) (kind 0)) (crate-dep (name "hyper") (req "^0.14") (features (quote ("full"))) (default-features #t) (kind 0)) (crate-dep (name "uuid") (req "^1.1.2") (features (quote ("v4" "fast-rng" "macro-diagnostics"))) (default-features #t) (kind 0)))) (hash "06xwrpvijfcdr1c5q4zlk5mg39ckj13snxjs8hajl8xdl1mbalg9") (features (quote (("s3-policy") ("all-policy" "s3-policy"))))))

