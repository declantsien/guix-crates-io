(define-module (crates-io pi on) #:use-module (crates-io))

(define-public crate-Pion-0.0.0 (crate (name "Pion") (vers "0.0.0") (hash "0q81ngk2xbb4zf61v19h6vwb36g0fkshzrw3zhka1ll2kjzyhd7i") (yanked #t)))

(define-public crate-Pion-0.0.1 (crate (name "Pion") (vers "0.0.1") (hash "0351y5rkz0qhdi8q33d8knr4xj12a4q63b2ldsc0131zik2yq1dg")))

(define-public crate-Pion-0.0.2 (crate (name "Pion") (vers "0.0.2") (hash "1p52slnljrcaprak0l9wz1a2c3sdn24a3wr1wkzbs23zmiyjrn3x")))

(define-public crate-pioneer-0.0.1 (crate (name "pioneer") (vers "0.0.1") (hash "1inj55jil3711v2hjgbkrcs1azxnb81d3pixad53a3rlm6rzb5i2") (yanked #t)))

(define-public crate-pioneerctl-0.1 (crate (name "pioneerctl") (vers "0.1.0") (deps (list (crate-dep (name "colored") (req "^2.0") (default-features #t) (kind 0)) (crate-dep (name "die-exit") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "rustyline") (req "^8.0") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.3") (default-features #t) (kind 0)))) (hash "1ffy5fkxfapnvydqws73kfdp301jh53992g1mcz8a5gycz3i0vn5")))

(define-public crate-pioneerctl-0.1 (crate (name "pioneerctl") (vers "0.1.1") (deps (list (crate-dep (name "colored") (req "^2.0") (default-features #t) (kind 0)) (crate-dep (name "die-exit") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "rustyline") (req "^8.0") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.3") (default-features #t) (kind 0)))) (hash "0x62kqrp4325qzpvi7361qs16jd6bj1s6hh81n189nyq4ga12b4g")))

(define-public crate-pioneerctl-0.1 (crate (name "pioneerctl") (vers "0.1.2") (deps (list (crate-dep (name "colored") (req "^2.0") (default-features #t) (kind 0)) (crate-dep (name "die-exit") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "rustyline") (req "^8.0") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.3") (default-features #t) (kind 0)))) (hash "0icyhv8c7cqbgwccj5mm3ajavy1ygr3dn2px0j2bnmhg3l67kv1x")))

(define-public crate-pioneerctl-0.1 (crate (name "pioneerctl") (vers "0.1.3") (deps (list (crate-dep (name "colored") (req "^2.0") (default-features #t) (kind 0)) (crate-dep (name "die-exit") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "rustyline") (req "^8.0") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.3") (default-features #t) (kind 0)))) (hash "0gf9zjxp5qfpzgc4440wpz3pg5s1z6gbssd32x0v83ilfdr7v6aq")))

(define-public crate-pioneerctl-0.2 (crate (name "pioneerctl") (vers "0.2.0") (deps (list (crate-dep (name "clap") (req "^4.2.1") (features (quote ("derive" "env"))) (default-features #t) (kind 0)) (crate-dep (name "clap_complete") (req "^4.2.0") (default-features #t) (kind 0)) (crate-dep (name "colored") (req "^2.0") (default-features #t) (kind 0)) (crate-dep (name "die-exit") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "rustyline") (req "^8.0") (default-features #t) (kind 0)))) (hash "0d24aza1b7q8n0hhlg6j2w3hfi8nch1v9wrr0pbwhnkaapksc6s5")))

