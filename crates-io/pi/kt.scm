(define-module (crates-io pi kt) #:use-module (crates-io))

(define-public crate-pikt-0.1 (crate (name "pikt") (vers "0.1.0") (deps (list (crate-dep (name "bitflags") (req "^1.3") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "pikchr-sys") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0") (default-features #t) (kind 0)))) (hash "0501nc0spj2m65wa5ljq0bk1y538ss7v620kn2lfl8facdm6x8hx")))

