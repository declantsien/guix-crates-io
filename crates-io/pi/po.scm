(define-module (crates-io pi po) #:use-module (crates-io))

(define-public crate-pipo-0.1 (crate (name "pipo") (vers "0.1.0") (deps (list (crate-dep (name "criterion") (req "^0.3.6") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 2)))) (hash "0mwagsbpvckmlgpc76g6i23nq3kpb3dg0kkixzsv1ibiaafvycs7")))

(define-public crate-pipo-0.1 (crate (name "pipo") (vers "0.1.1") (deps (list (crate-dep (name "criterion") (req "^0.3.6") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 2)))) (hash "0c5z39q3m2bcmnr7j53ca8vjy5fzd24vmlwqrcgr8h0733xvr2va")))

