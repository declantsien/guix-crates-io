(define-module (crates-io pi xt) #:use-module (crates-io))

(define-public crate-pixterm-0.1 (crate (name "pixterm") (vers "0.1.0") (deps (list (crate-dep (name "ansipix") (req "^0.1.2") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.3.25") (default-features #t) (kind 0)))) (hash "1piszqflsnm720knvv4h1c0isk7c7hz6y0221ckf3yf2z2bd8hqm")))

(define-public crate-pixterm-0.1 (crate (name "pixterm") (vers "0.1.1") (deps (list (crate-dep (name "ansipix") (req "^0.1.2") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.3.25") (default-features #t) (kind 0)))) (hash "03fiqzcvdpqvn3ddpbjj04zm25bn3srnzizjkm0lmdrlwa3d86zx")))

(define-public crate-pixterm-0.1 (crate (name "pixterm") (vers "0.1.2") (deps (list (crate-dep (name "ansipix") (req "^0.1.2") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.3.25") (default-features #t) (kind 0)))) (hash "0ymn3csn3712d3ykzxx8iz9cfxr6jqk717h6ja2l255yxghwamsv")))

(define-public crate-pixterm-0.2 (crate (name "pixterm") (vers "0.2.0") (deps (list (crate-dep (name "ansipix") (req "^0.1.2") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^3.1.18") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "0ishxgw5b8xdm68s6qam9va68mxy3355044is6dcy8d8p8flxaqd")))

(define-public crate-pixterm-0.2 (crate (name "pixterm") (vers "0.2.1") (deps (list (crate-dep (name "ansipix") (req "^1.0.0") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^3.1.18") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "0sr99f4bqpx9kpwi0q5dji8qi8i9p9h6pmhlzyqb172wl6jlsk43")))

(define-public crate-pixterm-0.3 (crate (name "pixterm") (vers "0.3.0") (deps (list (crate-dep (name "ansipix") (req "^1.0.0") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^3.2.6") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "1fzl70g2099p40hl6gdkzz9vbgkypy3i5zfhsz9bmwmplp6509kb")))

(define-public crate-pixterm-0.4 (crate (name "pixterm") (vers "0.4.0") (deps (list (crate-dep (name "ansipix") (req "^1.0.0") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^3.2.6") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "1d4n987nf5xvmb7jjn0zc56vx5a2iwr6jkqx8p7ilgx65qlsjdh9")))

(define-public crate-pixtra-0.1 (crate (name "pixtra") (vers "0.1.0") (deps (list (crate-dep (name "image") (req "^0.24.3") (default-features #t) (kind 0)))) (hash "01yipiyzg4zsry7w8184a1wn3b9zbs88nx6p3ialjmdsjdx0qbk9")))

(define-public crate-pixtra-0.1 (crate (name "pixtra") (vers "0.1.1") (deps (list (crate-dep (name "image") (req "^0.24.3") (default-features #t) (kind 0)))) (hash "1sxpv398770j5x62rs7wmambw4j45nhlj43alcgnvsj76vybbqck")))

(define-public crate-pixtra-0.1 (crate (name "pixtra") (vers "0.1.2") (deps (list (crate-dep (name "image") (req "^0.24.3") (default-features #t) (kind 0)))) (hash "10zdwid86qfiimak28j1ivcnw2cdm83h536c67v1hgp0mz3250aa")))

(define-public crate-pixtra-0.1 (crate (name "pixtra") (vers "0.1.3") (deps (list (crate-dep (name "image") (req "^0.24.3") (default-features #t) (kind 0)))) (hash "036p3klvk5xmw6gvarww5ji8h2013nldshf79c55ajc545x4kvl3")))

(define-public crate-pixtra-0.1 (crate (name "pixtra") (vers "0.1.4") (deps (list (crate-dep (name "image") (req "^0.24.3") (default-features #t) (kind 0)))) (hash "1lldsqqn37q7dwmjb43akp6pq6m9263cqn262r3ycmvavg2jazzj")))

(define-public crate-pixtra-0.1 (crate (name "pixtra") (vers "0.1.5") (deps (list (crate-dep (name "image") (req "^0.24.3") (default-features #t) (kind 0)))) (hash "1dwzjz7a8kl0m6cxnrjj8hcylhiha603xarrmmb7f3dfi8rj7vpf")))

(define-public crate-pixtra-0.2 (crate (name "pixtra") (vers "0.2.1") (deps (list (crate-dep (name "image") (req "^0.24.3") (default-features #t) (kind 0)))) (hash "1h97z14m3js8kym0qxjwv07176i5acs9l91plwwahlmc8xq2mf7m") (features (quote (("pixtra"))))))

(define-public crate-pixtra-0.2 (crate (name "pixtra") (vers "0.2.2") (deps (list (crate-dep (name "image") (req "^0.24.3") (default-features #t) (kind 0)) (crate-dep (name "num") (req "^0.4.0") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)))) (hash "040z431psd735byzldxfnisdf25q25vh01ic2fkq0iaribp9f8vr") (features (quote (("pixtra"))))))

(define-public crate-pixtra-0.2 (crate (name "pixtra") (vers "0.2.3") (deps (list (crate-dep (name "image") (req "^0.24.3") (default-features #t) (kind 0)) (crate-dep (name "num") (req "^0.4.0") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)))) (hash "0y9ly31g5hk4j89c6b3n3fxs93mz9k09ljqg37688q1zln9i88ap") (features (quote (("pixtra"))))))

(define-public crate-pixtra-0.2 (crate (name "pixtra") (vers "0.2.4") (deps (list (crate-dep (name "image") (req "^0.24.3") (default-features #t) (kind 0)) (crate-dep (name "num") (req "^0.4.0") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)))) (hash "1h5zmqcpn5k8jmb124jpyz0vspnnkf9b28mmgpdx95l2fz7jy5mf") (features (quote (("pixtra"))))))

(define-public crate-pixtra-0.2 (crate (name "pixtra") (vers "0.2.5") (deps (list (crate-dep (name "image") (req "^0.24.3") (default-features #t) (kind 0)) (crate-dep (name "num") (req "^0.4.0") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)))) (hash "1gycpzlhws967nwp28an2nj49qzvsfimlkb9bc8qbir0iwpcvras") (features (quote (("pixtra"))))))

