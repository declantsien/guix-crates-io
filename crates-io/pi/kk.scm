(define-module (crates-io pi kk) #:use-module (crates-io))

(define-public crate-pikkr-0.0.1 (crate (name "pikkr") (vers "0.0.1") (hash "1h9g0g45zhb1a242c4i24rjdywdyxlqvr63889a8yb1z21dbp6gn")))

(define-public crate-pikkr-0.0.2 (crate (name "pikkr") (vers "0.0.2") (hash "01a63xcnxxxhhl9499p3088gxi4kncw3afx436ibqsalgbjc7s0k")))

(define-public crate-pikkr-0.0.3 (crate (name "pikkr") (vers "0.0.3") (hash "0sxwbgs93d7c9ik690gp1p3v65f547k4zfzlkb6jy9m1hdm0ybwn")))

(define-public crate-pikkr-0.0.4 (crate (name "pikkr") (vers "0.0.4") (hash "05l68xf24zays4by21kjym2rmavaw46lvwrf782nqqj6q4dhy63d")))

(define-public crate-pikkr-0.0.5 (crate (name "pikkr") (vers "0.0.5") (deps (list (crate-dep (name "x86intrin") (req "^0.4.3") (default-features #t) (kind 0)))) (hash "1mkm0p6yi5wpjrcg6dgfm4kry256dx5afwqg3h5crwzby5slw25m")))

(define-public crate-pikkr-0.0.6 (crate (name "pikkr") (vers "0.0.6") (deps (list (crate-dep (name "x86intrin") (req "^0.4.3") (default-features #t) (kind 0)))) (hash "1z4g73kpsmd6pmikkjjdmhdfkfnbpq8w01d1ncvvm2ik6hvmss16")))

(define-public crate-pikkr-0.0.7 (crate (name "pikkr") (vers "0.0.7") (deps (list (crate-dep (name "x86intrin") (req "^0.4.3") (default-features #t) (kind 0)))) (hash "0g6a51ppbck44l0kdclh8fwvc9hpfyps3alc9q8l0fhlsqfvcpvj")))

(define-public crate-pikkr-0.0.8 (crate (name "pikkr") (vers "0.0.8") (deps (list (crate-dep (name "x86intrin") (req "^0.4.3") (default-features #t) (kind 0)))) (hash "02aw07mvyp0bnc1kgc8f12bsjxnfabdgnj5agma7jiv7xqkrki18")))

(define-public crate-pikkr-0.0.9 (crate (name "pikkr") (vers "0.0.9") (deps (list (crate-dep (name "x86intrin") (req "^0.4.3") (default-features #t) (kind 0)))) (hash "122xpg78a1y0n7zjlsgg93ij6zjk8gkvkijs582pyc4w2n1s4v81")))

(define-public crate-pikkr-0.1 (crate (name "pikkr") (vers "0.1.0") (deps (list (crate-dep (name "x86intrin") (req "^0.4.3") (default-features #t) (kind 0)))) (hash "13rcvnsv4ck9fhwfm49drwl7ba5vb288jlavl6yx7yyifc3aq042")))

(define-public crate-pikkr-0.2 (crate (name "pikkr") (vers "0.2.0") (deps (list (crate-dep (name "x86intrin") (req "^0.4.3") (default-features #t) (kind 0)))) (hash "0k2zv88cpz7zfg88mc5q24avv108jzqwr7agbpgh9j8m1i2nf031")))

(define-public crate-pikkr-0.2 (crate (name "pikkr") (vers "0.2.1") (deps (list (crate-dep (name "x86intrin") (req "^0.4.3") (default-features #t) (kind 0)))) (hash "0m9zd4panc7rq8c9y151n91s9hzb01axc1c8rwy6r4j7vql8d3wg")))

(define-public crate-pikkr-0.2 (crate (name "pikkr") (vers "0.2.2") (deps (list (crate-dep (name "x86intrin") (req "^0.4.3") (default-features #t) (kind 0)))) (hash "05f17824kpky2j45n196z5p2nv3zv57kl8djci777j3q7zzyacw1")))

(define-public crate-pikkr-0.3 (crate (name "pikkr") (vers "0.3.0") (deps (list (crate-dep (name "x86intrin") (req "^0.4.3") (default-features #t) (kind 0)))) (hash "04prn7dijl6dr7nhx89q9dnb239nc6ic7alxjfj1mmrhhfnmh30m")))

(define-public crate-pikkr-0.3 (crate (name "pikkr") (vers "0.3.1") (deps (list (crate-dep (name "fnv") (req "^1.0.5") (default-features #t) (kind 0)) (crate-dep (name "x86intrin") (req "^0.4.3") (default-features #t) (kind 0)))) (hash "05xb3yx432a9835czx3i4a6pvi9g03jcw1cz6hj2nncyg2i8r3ak")))

(define-public crate-pikkr-0.3 (crate (name "pikkr") (vers "0.3.2") (deps (list (crate-dep (name "fnv") (req "^1.0.5") (default-features #t) (kind 0)) (crate-dep (name "x86intrin") (req "^0.4.3") (default-features #t) (kind 0)))) (hash "0gb0jm6r82gp2kngz6k0xhx0ij7wbwnpmp1jxi99s1srgxr82xpc")))

(define-public crate-pikkr-0.3 (crate (name "pikkr") (vers "0.3.3") (deps (list (crate-dep (name "fnv") (req "^1.0.5") (default-features #t) (kind 0)) (crate-dep (name "x86intrin") (req "^0.4.3") (default-features #t) (kind 0)))) (hash "1ybwvrxbkrkc4clzb246zmxww8yv1rd4jcyvjjx3cmnj3f8jglpa")))

(define-public crate-pikkr-0.3 (crate (name "pikkr") (vers "0.3.4") (deps (list (crate-dep (name "fnv") (req "^1.0.5") (default-features #t) (kind 0)) (crate-dep (name "x86intrin") (req "^0.4.3") (default-features #t) (kind 0)))) (hash "0iymr9ghjskl7winyp41xfdcwcrqakdlf87xchsk3kpk7bp9pfx4")))

(define-public crate-pikkr-0.3 (crate (name "pikkr") (vers "0.3.5") (deps (list (crate-dep (name "fnv") (req "^1.0.5") (default-features #t) (kind 0)) (crate-dep (name "x86intrin") (req "^0.4.3") (default-features #t) (kind 0)))) (hash "1zd8ml65j88xx8qvl5wgm0g4gaf4w95y3vwza45xvypkpp5g5dri")))

(define-public crate-pikkr-0.3 (crate (name "pikkr") (vers "0.3.6") (deps (list (crate-dep (name "fnv") (req "^1.0.5") (default-features #t) (kind 0)) (crate-dep (name "x86intrin") (req "^0.4.3") (default-features #t) (kind 0)))) (hash "08k5pbwqal3xyandm85lvfg34rkwl4ccwyll1k41jsgnpn79cjj8")))

(define-public crate-pikkr-0.3 (crate (name "pikkr") (vers "0.3.7") (deps (list (crate-dep (name "fnv") (req "^1.0.5") (default-features #t) (kind 0)) (crate-dep (name "x86intrin") (req "^0.4.3") (default-features #t) (kind 0)))) (hash "0cf9ilyfxyip90hxh1d0gll1rz66nc52mfwczx9lrgq2wvcjhp1p")))

(define-public crate-pikkr-0.3 (crate (name "pikkr") (vers "0.3.8") (deps (list (crate-dep (name "fnv") (req "^1.0.5") (default-features #t) (kind 0)) (crate-dep (name "x86intrin") (req "^0.4.3") (default-features #t) (kind 0)))) (hash "0ls4lhq1n0b72r770irprpnx2sh4z4z1c1zhjlmbcq95p9yf67pc")))

(define-public crate-pikkr-0.3 (crate (name "pikkr") (vers "0.3.9") (deps (list (crate-dep (name "fnv") (req "^1.0.5") (default-features #t) (kind 0)) (crate-dep (name "x86intrin") (req "^0.4.3") (default-features #t) (kind 0)))) (hash "1449krzx7g4khn1pkj7psvqi9m4zvn3v1sl82klc080d86f8nhsq")))

(define-public crate-pikkr-0.3 (crate (name "pikkr") (vers "0.3.10") (deps (list (crate-dep (name "fnv") (req "^1.0.5") (default-features #t) (kind 0)) (crate-dep (name "x86intrin") (req "^0.4.3") (default-features #t) (kind 0)))) (hash "1s6qp5djycq2zh7zrgwdnd270cqfd0s5k8xl7zvy2a70nspyw707")))

(define-public crate-pikkr-0.4 (crate (name "pikkr") (vers "0.4.0") (deps (list (crate-dep (name "fnv") (req "^1.0.5") (default-features #t) (kind 0)) (crate-dep (name "x86intrin") (req "^0.4.3") (default-features #t) (kind 0)))) (hash "0q3hs89gls7c65613x8i3yamw8sa453nlwmxwv1bxn84fk5ba8wy")))

(define-public crate-pikkr-0.4 (crate (name "pikkr") (vers "0.4.1") (deps (list (crate-dep (name "fnv") (req "^1.0.5") (default-features #t) (kind 0)) (crate-dep (name "x86intrin") (req "^0.4.3") (default-features #t) (kind 0)))) (hash "05w73ng871v0ggiipgg2qs0zqljhf8xvis7sxwpnh692cp4mkq25")))

(define-public crate-pikkr-0.4 (crate (name "pikkr") (vers "0.4.2") (deps (list (crate-dep (name "fnv") (req "^1.0.5") (default-features #t) (kind 0)) (crate-dep (name "x86intrin") (req "^0.4.3") (default-features #t) (kind 0)))) (hash "1x7rw8h13rghy95xbwzgh2aif7bq4465fc8pzdsa302s6sr1g5va")))

(define-public crate-pikkr-0.4 (crate (name "pikkr") (vers "0.4.3") (deps (list (crate-dep (name "fnv") (req "^1.0.5") (default-features #t) (kind 0)) (crate-dep (name "x86intrin") (req "^0.4.3") (default-features #t) (kind 0)))) (hash "0gv4cwscvkfn2pdq4wlpswqcp0fv18pi844z37viyr6d5rakm2v7")))

(define-public crate-pikkr-0.4 (crate (name "pikkr") (vers "0.4.4") (deps (list (crate-dep (name "fnv") (req "^1.0.5") (default-features #t) (kind 0)) (crate-dep (name "x86intrin") (req "^0.4.3") (default-features #t) (kind 0)))) (hash "00hw4b2hyh3vijib5z5n7m9scdwhhpw871fsfbxjfiq4d0z02lsz")))

(define-public crate-pikkr-0.4 (crate (name "pikkr") (vers "0.4.5") (deps (list (crate-dep (name "fnv") (req "^1.0.5") (default-features #t) (kind 0)) (crate-dep (name "x86intrin") (req "^0.4.3") (default-features #t) (kind 0)))) (hash "0rq03xnwyzzbw3i7awq1krkb8r3xii9ncbcbspp2j1pjy5k6r7a9")))

(define-public crate-pikkr-0.4 (crate (name "pikkr") (vers "0.4.6") (deps (list (crate-dep (name "fnv") (req "^1.0.5") (default-features #t) (kind 0)) (crate-dep (name "x86intrin") (req "^0.4.3") (default-features #t) (kind 0)))) (hash "0454bqxay186bd3knnycb1ldg6p44cg2vwcsz6vknbjd9ni0zq4d")))

(define-public crate-pikkr-0.4 (crate (name "pikkr") (vers "0.4.7") (deps (list (crate-dep (name "fnv") (req "^1.0.5") (default-features #t) (kind 0)) (crate-dep (name "x86intrin") (req "^0.4.3") (default-features #t) (kind 0)))) (hash "134a79jj18h99rkmm13yzwa8fhfyla0bx03bp1sniazawddss8vd")))

(define-public crate-pikkr-0.4 (crate (name "pikkr") (vers "0.4.8") (deps (list (crate-dep (name "fnv") (req "^1.0.5") (default-features #t) (kind 0)) (crate-dep (name "x86intrin") (req "^0.4.3") (default-features #t) (kind 0)))) (hash "12j96faqilif0icyi48khfss7g2isnxm44sz18wpd69rs5pdk3pq")))

(define-public crate-pikkr-0.5 (crate (name "pikkr") (vers "0.5.0") (deps (list (crate-dep (name "fnv") (req "^1.0.5") (default-features #t) (kind 0)) (crate-dep (name "x86intrin") (req "^0.4.3") (default-features #t) (kind 0)))) (hash "198s325xmjnm4jnjx8q89sc7y0mdf90jikfp1pf05q72r5kjzkk2")))

(define-public crate-pikkr-0.5 (crate (name "pikkr") (vers "0.5.1") (deps (list (crate-dep (name "fnv") (req "^1.0.5") (default-features #t) (kind 0)) (crate-dep (name "x86intrin") (req "^0.4.3") (default-features #t) (kind 0)))) (hash "1qyj5zf67jqzl1gni15003d55f53ikl33xr5ng2r5i05as3i3apf")))

(define-public crate-pikkr-0.5 (crate (name "pikkr") (vers "0.5.2") (deps (list (crate-dep (name "fnv") (req "^1.0.5") (default-features #t) (kind 0)) (crate-dep (name "x86intrin") (req "^0.4.3") (default-features #t) (kind 0)))) (hash "0jzrcsn7yagz5nyjx0snwhx66mn0rmmz5w4dwpbwz7jjmclfw9n8")))

(define-public crate-pikkr-0.5 (crate (name "pikkr") (vers "0.5.3") (deps (list (crate-dep (name "fnv") (req "^1.0.5") (default-features #t) (kind 0)) (crate-dep (name "x86intrin") (req "^0.4.3") (default-features #t) (kind 0)))) (hash "02qihkbvlvkkc3pvznflp21kz1ayk8xajhy2pmc9kq2mi3p68lyc")))

(define-public crate-pikkr-0.6 (crate (name "pikkr") (vers "0.6.0") (deps (list (crate-dep (name "fnv") (req "^1.0.5") (default-features #t) (kind 0)) (crate-dep (name "x86intrin") (req "^0.4.3") (default-features #t) (kind 0)))) (hash "016wzkns1kr3nfynf7d9y4sid0z166qnflpzdrs2w67zl24v3d58")))

(define-public crate-pikkr-0.7 (crate (name "pikkr") (vers "0.7.0") (deps (list (crate-dep (name "fnv") (req "^1.0.5") (default-features #t) (kind 0)) (crate-dep (name "x86intrin") (req "^0.4.3") (default-features #t) (kind 0)))) (hash "0pq6lqwj16yydb3ywmczcln1y4n29x43b4lw3rjd7s79np260qi3")))

(define-public crate-pikkr-0.8 (crate (name "pikkr") (vers "0.8.0") (deps (list (crate-dep (name "fnv") (req "^1.0.5") (default-features #t) (kind 0)) (crate-dep (name "x86intrin") (req "^0.4.3") (default-features #t) (kind 0)))) (hash "184c074cqlpdwyw030p5fx9v46q9w4rpi1mbnry9l4ahz3h6yc9m")))

(define-public crate-pikkr-0.8 (crate (name "pikkr") (vers "0.8.1") (deps (list (crate-dep (name "fnv") (req "^1.0.5") (default-features #t) (kind 0)) (crate-dep (name "x86intrin") (req "^0.4.3") (default-features #t) (kind 0)))) (hash "0ngljljkrrwhhyzaxrmazp09cqhnzz0paacyf0ax9ld3l24cqns8")))

(define-public crate-pikkr-0.9 (crate (name "pikkr") (vers "0.9.0") (deps (list (crate-dep (name "fnv") (req "^1.0.5") (default-features #t) (kind 0)) (crate-dep (name "x86intrin") (req "^0.4.3") (default-features #t) (kind 0)))) (hash "0y387dabq4yz3waywh90w52sky46sd9pq6d9q9c68lfldax2c48m")))

(define-public crate-pikkr-0.9 (crate (name "pikkr") (vers "0.9.3") (deps (list (crate-dep (name "fnv") (req "^1.0.5") (default-features #t) (kind 0)) (crate-dep (name "x86intrin") (req "^0.4.3") (default-features #t) (kind 0)))) (hash "1byf60319f37hdgf4x8lw7585xb7w2yzg1sjcbfkkzpx14y7aayz")))

(define-public crate-pikkr-0.10 (crate (name "pikkr") (vers "0.10.0") (deps (list (crate-dep (name "fnv") (req "^1.0.5") (default-features #t) (kind 0)) (crate-dep (name "x86intrin") (req "^0.4.3") (default-features #t) (kind 0)))) (hash "1zcm74yrfmzy8sfywbhn4n1ga79l3yfg2bd42jj8fd2abkahh19f")))

(define-public crate-pikkr-0.10 (crate (name "pikkr") (vers "0.10.1") (deps (list (crate-dep (name "fnv") (req "^1.0.5") (default-features #t) (kind 0)) (crate-dep (name "x86intrin") (req "^0.4.3") (default-features #t) (kind 0)))) (hash "0w3h3vgcmr0vfhj5hvfiz49asdi0vdaz29mv94zwr1zhnhzrk7p8")))

(define-public crate-pikkr-0.11 (crate (name "pikkr") (vers "0.11.0") (deps (list (crate-dep (name "fnv") (req "^1.0.5") (default-features #t) (kind 0)) (crate-dep (name "x86intrin") (req "^0.4.3") (default-features #t) (kind 0)))) (hash "1jyqmxbnaj2dwf12x5pqc6snmsl4fhpfs6q4f69jxkivcbdhrijm")))

(define-public crate-pikkr-0.12 (crate (name "pikkr") (vers "0.12.1") (deps (list (crate-dep (name "fnv") (req "^1.0.5") (default-features #t) (kind 0)) (crate-dep (name "x86intrin") (req "^0.4.3") (default-features #t) (kind 0)))) (hash "0lzxbr8ggqb08avacavbgsgy6v4a80vm0v79j75gv48lyk57cy7z")))

(define-public crate-pikkr-0.13 (crate (name "pikkr") (vers "0.13.0") (deps (list (crate-dep (name "fnv") (req "^1.0.5") (default-features #t) (kind 0)) (crate-dep (name "x86intrin") (req "^0.4.3") (default-features #t) (kind 0)))) (hash "0fywdfkf1vm8wlc3mssyyv63hkm9xq1gfm13k64bcj21f2j6ls2q")))

(define-public crate-pikkr-0.14 (crate (name "pikkr") (vers "0.14.0") (deps (list (crate-dep (name "fnv") (req "^1.0.5") (default-features #t) (kind 0)) (crate-dep (name "x86intrin") (req "^0.4.3") (default-features #t) (kind 0)))) (hash "04nrjzh93ybdabdif6cdlfgpsr3ncvd0rsr5acr45dkhi3jhwpz3")))

(define-public crate-pikkr-0.16 (crate (name "pikkr") (vers "0.16.0") (deps (list (crate-dep (name "fnv") (req "^1.0.5") (default-features #t) (kind 0)) (crate-dep (name "x86intrin") (req "^0.4.3") (default-features #t) (kind 0)))) (hash "11awia5dgyvmnqhqlzwmqr5zv4syp87v3fngljyky0mr0injdk6y")))

(define-public crate-pikkr-annika-0.16 (crate (name "pikkr-annika") (vers "0.16.1") (deps (list (crate-dep (name "fnv") (req "^1.0.5") (default-features #t) (kind 0)) (crate-dep (name "x86intrin") (req "^0.4.3") (optional #t) (default-features #t) (kind 0)))) (hash "0z9qqlg7zx1nzl2w4l7p9za8n5kbml5fq8cbk840gxg7jqq0nxb0") (features (quote (("default") ("avx-accel" "x86intrin"))))))

