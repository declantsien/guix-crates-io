(define-module (crates-io d_ pr) #:use-module (crates-io))

(define-public crate-d_print-0.1 (crate (name "d_print") (vers "0.1.0") (hash "0sbzrf3ph7fw07byl2v540bc5w90fv86c6d53n1640il2bm08nj5")))

(define-public crate-d_print-0.1 (crate (name "d_print") (vers "0.1.1") (hash "06nc441w53g4vnq645fjdv0kzm8psl0l2ai2i4fskc4rbjv127yf")))

(define-public crate-d_print-0.1 (crate (name "d_print") (vers "0.1.2") (hash "1hqwp898ki7qs52slmq8df8s421s2rr4gjla89fqhjys1gnagvzy")))

(define-public crate-d_print-0.1 (crate (name "d_print") (vers "0.1.3") (hash "1s45vxc09m413sk8y7m7cyb1rp1m4p354ywy7qirmc9isjiw7lpd")))

