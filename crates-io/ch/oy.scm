(define-module (crates-io ch oy) #:use-module (crates-io))

(define-public crate-choyen_5000-0.1 (crate (name "choyen_5000") (vers "0.1.0") (deps (list (crate-dep (name "pyo3") (req "^0.17.1") (features (quote ("auto-initialize"))) (default-features #t) (kind 0)))) (hash "0kjny2h5vwmym9yhcfb5118vnbg7wmyq3kx2q41bq7nvjyfhpvzz")))

(define-public crate-choyen_5000-0.1 (crate (name "choyen_5000") (vers "0.1.1") (deps (list (crate-dep (name "pyo3") (req "^0.17.1") (features (quote ("auto-initialize"))) (default-features #t) (kind 0)))) (hash "0v5wfikb6yjfrwn45sbjn0yb6h78a5cfj2qwzf34znldn13s5fm8")))

(define-public crate-choyen_5000-0.1 (crate (name "choyen_5000") (vers "0.1.2") (deps (list (crate-dep (name "pyo3") (req "^0.17.1") (features (quote ("auto-initialize"))) (default-features #t) (kind 0)))) (hash "0gg7syr2h70qk05114yvcmwy5phvmhh984gd4ipscc4b8ava3mp8")))

(define-public crate-choyen_5000-0.1 (crate (name "choyen_5000") (vers "0.1.3") (deps (list (crate-dep (name "pyo3") (req "^0.17.1") (features (quote ("auto-initialize"))) (default-features #t) (kind 0)))) (hash "05y4wi1nar6ps2i1krissmji79azrj8c443xnhdd3w2v9d8zrpgi")))

(define-public crate-choyen_5000-0.2 (crate (name "choyen_5000") (vers "0.2.0") (deps (list (crate-dep (name "pyo3") (req "^0.17.1") (features (quote ("auto-initialize"))) (default-features #t) (kind 0)))) (hash "11rrhrf1wp4lg5iabn787afsr1fgwn85b6mj3yd59gsn51zis9lp")))

(define-public crate-choyen_5000-0.2 (crate (name "choyen_5000") (vers "0.2.1") (deps (list (crate-dep (name "pyo3") (req "^0.17.1") (features (quote ("auto-initialize"))) (default-features #t) (kind 0)))) (hash "1gvck8fmr8x33sckqi7689g31ypkgpi6a2brqbqjnkp5rzb2a2ay")))

(define-public crate-choyen_bot-0.2 (crate (name "choyen_bot") (vers "0.2.1") (deps (list (crate-dep (name "choyen_5000") (req "^0.2.1") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "pretty_env_logger") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "teloxide") (req "^0.11.0") (features (quote ("macros" "ctrlc_handler"))) (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1") (features (quote ("rt-multi-thread" "macros"))) (default-features #t) (kind 0)))) (hash "1wkbdjczpc3aix9vkdrnjwb89r4yk5njpfgpza1khrym4bb6yx5h")))

