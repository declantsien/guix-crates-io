(define-module (crates-io ch ko) #:use-module (crates-io))

(define-public crate-chkoauth2-0.1 (crate (name "chkoauth2") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^4.4.14") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "env_logger") (req "^0.11") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "oauth2") (req "^4") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)))) (hash "1can73hfjpahxp4j3yydplq9xawplmjkscwgjq7ix7rqffhwwwzd") (features (quote (("indieauth" "serde") ("default" "indieauth"))))))

