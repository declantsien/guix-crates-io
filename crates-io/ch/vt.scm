(define-module (crates-io ch vt) #:use-module (crates-io))

(define-public crate-chvt-0.1 (crate (name "chvt") (vers "0.1.0") (deps (list (crate-dep (name "libc") (req "^0.2.65") (default-features #t) (kind 0)))) (hash "1pzxlz1jrasnbakagf4amq6655mr5mdmnfd6hgglk7afljxxzyb1")))

(define-public crate-chvt-0.1 (crate (name "chvt") (vers "0.1.1") (deps (list (crate-dep (name "libc") (req "^0.2.65") (default-features #t) (kind 0)))) (hash "0d8ycghgfdx08fwa0fc7l69syqqnlvv2fmhg7pxzpszf07mca73g")))

(define-public crate-chvt-0.1 (crate (name "chvt") (vers "0.1.2") (deps (list (crate-dep (name "libc") (req "^0.2.65") (default-features #t) (kind 0)))) (hash "15xn5a14s4kazpvfby5jjp39iqzvimfq4hzyb3sb922bf60kfgng")))

(define-public crate-chvt-0.1 (crate (name "chvt") (vers "0.1.3") (deps (list (crate-dep (name "libc") (req "^0.2.65") (default-features #t) (kind 0)))) (hash "16fc6rra1xc0wrq5jm0hcwfllfxsz4ih5jsif24nqmzbh00x7yn8")))

(define-public crate-chvt-0.2 (crate (name "chvt") (vers "0.2.0") (deps (list (crate-dep (name "libc") (req "^0.2.65") (default-features #t) (kind 0)) (crate-dep (name "nix") (req "^0.15.0") (default-features #t) (kind 0)))) (hash "1vs8n6vlgvrb106ahvlm1y8fd1ffz79ksyq410dcrgjah3awjpc8")))

