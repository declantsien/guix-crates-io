(define-module (crates-io ch a-) #:use-module (crates-io))

(define-public crate-cha-rs-0.0.1 (crate (name "cha-rs") (vers "0.0.1") (deps (list (crate-dep (name "clap") (req "^4.4.7") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.50") (default-features #t) (kind 0)))) (hash "18nklkx63vrh4l83c8j44q4sls57a49zbcfh2hskh173phcp7wqy")))

(define-public crate-cha-rs-0.0.2 (crate (name "cha-rs") (vers "0.0.2") (deps (list (crate-dep (name "clap") (req "^4.4.7") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.50") (default-features #t) (kind 0)))) (hash "0680z3jpdj0rxqrbgsbqi5b46pjmgfv3pmw0agkb7zi1fzq4y22f")))

(define-public crate-cha-rs-0.0.3 (crate (name "cha-rs") (vers "0.0.3") (deps (list (crate-dep (name "clap") (req "^4.4.7") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.50") (default-features #t) (kind 0)))) (hash "0q18an2dpb2vzapzcn4fslfcp8p7fr1nip0fb3kyk21ibia75v3p")))

