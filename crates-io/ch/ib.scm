(define-module (crates-io ch ib) #:use-module (crates-io))

(define-public crate-chibi-0.1 (crate (name "chibi") (vers "0.1.0") (deps (list (crate-dep (name "peg") (req "^0.8.0") (default-features #t) (kind 0)) (crate-dep (name "postcard") (req "^0.7.3") (features (quote ("use-std"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.137") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "18x403rhrhqx9gv61i885nn4w62xx6qipq19yvmysfbl3kc9a39p") (features (quote (("trace" "peg/trace"))))))

(define-public crate-chibi-scheme-0.1 (crate (name "chibi-scheme") (vers "0.1.0") (deps (list (crate-dep (name "bindgen") (req "^0.48.1") (default-features #t) (kind 1)))) (hash "1zwy21x67wqv807qcww73rf81sjzqkix169ajrlm23lxzbp25b5w")))

(define-public crate-chibi-scheme-rs-0.1 (crate (name "chibi-scheme-rs") (vers "0.1.0") (deps (list (crate-dep (name "bindgen") (req "^0.48.1") (default-features #t) (kind 1)))) (hash "0l5x4szfvrsg916lqwwz6xbflyf2i0a50dzifcib8q03jj41yvb6")))

(define-public crate-chibi-scheme-sys-0.1 (crate (name "chibi-scheme-sys") (vers "0.1.0") (deps (list (crate-dep (name "bindgen") (req "^0.48.1") (default-features #t) (kind 1)))) (hash "0n5sgbdnqh322ay8ym46gn78axnl1x56fdjmlgnyxygbw7xgi796")))

(define-public crate-chibiauth-0.0.0 (crate (name "chibiauth") (vers "0.0.0") (hash "1blsp1rd3an3kh6mbj3r710fpwyliqqnvqkmly2qg23hn8pv5abz") (yanked #t) (rust-version "1.64")))

(define-public crate-chibios-0.1 (crate (name "chibios") (vers "0.1.0") (hash "0zjg8kw7b1mhvfqnlgpyhz51aindbsw0319qn2in4yld9knn1nap")))

