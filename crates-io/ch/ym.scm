(define-module (crates-io ch ym) #:use-module (crates-io))

(define-public crate-chymist-0.1 (crate (name "chymist") (vers "0.1.0") (deps (list (crate-dep (name "fructose") (req "^0.3.4") (default-features #t) (kind 0)) (crate-dep (name "glucose") (req "^0.2.2") (default-features #t) (kind 0)))) (hash "1459x79knwgw38c2mvccgd9xxqhq6vih3a8fi3n43rp0gzy9zlxk")))

