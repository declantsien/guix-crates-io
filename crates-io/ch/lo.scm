(define-module (crates-io ch lo) #:use-module (crates-io))

(define-public crate-chloe-0.0.1 (crate (name "chloe") (vers "0.0.1") (hash "09nsqxbfgw7i1xn2jym2l915a3qcs1hlrx0rfp4n8wjj6s3ipak2")))

(define-public crate-chloe-0.0.2 (crate (name "chloe") (vers "0.0.2") (hash "0ak5mzkxav0fdypkn8gxksvsv7c67hrwzgl5rqhldnyv8h6cppfx")))

(define-public crate-chloe199719_unit_converter-0.1 (crate (name "chloe199719_unit_converter") (vers "0.1.0") (hash "1ypd3qh4mnyrkzp51pzg4w7c0mdd1f05rldprzwqinnpi4zshmas")))

(define-public crate-chloe_ignore-0.1 (crate (name "chloe_ignore") (vers "0.1.0") (hash "0vhgih75lyd3c9qjy887pa75h8isqarlnmlh797h5acf64dy9dvm")))

(define-public crate-chloe_ignore-0.1 (crate (name "chloe_ignore") (vers "0.1.1") (hash "10xhsakwfzi6vicmn8vgcwb1wasl0xggkxlc29gxy2kh9dhi3q6d")))

(define-public crate-chloe_ignore-0.1 (crate (name "chloe_ignore") (vers "0.1.2") (hash "1pkh2xhgcjyvmzhmyiwirvan82wzkb2a8dpi8bqhis1wa2wlqpda")))

(define-public crate-chloe_ignore-0.1 (crate (name "chloe_ignore") (vers "0.1.3") (hash "0jki14p0sz8453vb7z0gsjh7q2zfvfhzjyggzln9iah8znq6h5i0")))

(define-public crate-chloe_ignore-0.1 (crate (name "chloe_ignore") (vers "0.1.4") (hash "0648bd8pgsxdd7js4mb2jfsdji1zcmq1abl7bs4x9bkcb4w604xf")))

(define-public crate-chlog-0.1 (crate (name "chlog") (vers "0.1.0") (hash "0zrn4zyj7vicw3pjr3xwnir9cbn4rbdlkhg9qf5gybhmrsmdmd1l")))

(define-public crate-chlog-0.2 (crate (name "chlog") (vers "0.2.0") (hash "1xygvb9qkji3c7q2j3jkd3r2hyqiingvsjcrin0d1mg4sgg1zfrm")))

(define-public crate-chlog-0.2 (crate (name "chlog") (vers "0.2.1") (hash "0wpp47k25gknsf7rp4asn1hj8jnfz7b7pwim47yrv5iwzim71vcy")))

(define-public crate-chlog-0.2 (crate (name "chlog") (vers "0.2.2") (hash "1i67cqd870fs4xzl3ix7xg8kdj2dp65x0xxs8fasck7vpz1w2qk2")))

(define-public crate-chlog-0.2 (crate (name "chlog") (vers "0.2.3") (hash "0qiywnfp2c7lgnn6aj45wnx5c5s3ahxrdizga83ifwvw4cwayz98")))

(define-public crate-chlog-0.2 (crate (name "chlog") (vers "0.2.4") (hash "0cki2f1g87xvdkxf86xr9afhiafwbvy7li2ygy844jg5rdn733q3")))

(define-public crate-chlog-0.2 (crate (name "chlog") (vers "0.2.5-beta.0") (hash "1p8q0zdnmkikyy9vl1avjplznpmxz6blksidimvcx6pl3l422crj")))

(define-public crate-chlog-0.2 (crate (name "chlog") (vers "0.2.5") (hash "1cjxlqwpiqfbvc9bwhvxp36y9xkl8v6ypl275l30wkirzjhsfb0a")))

(define-public crate-chlog-0.3 (crate (name "chlog") (vers "0.3.0") (hash "1vb3dmjhmam7bkwq5yzqrzlrdj0h7z6iaarmygvvjjy84dkz74ql")))

(define-public crate-chlorate-0.1 (crate (name "chlorate") (vers "0.1.0") (deps (list (crate-dep (name "bytes") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "prost") (req "^0.7") (default-features #t) (kind 0)) (crate-dep (name "prost-build") (req "^0.7") (default-features #t) (kind 1)))) (hash "18md1qvaz888969jxhasw290m349ajwk812qw9696yy9671q85g5")))

(define-public crate-chlorate-0.1 (crate (name "chlorate") (vers "0.1.1") (deps (list (crate-dep (name "bytes") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "prost") (req "^0.7") (default-features #t) (kind 0)) (crate-dep (name "prost-build") (req "^0.7") (default-features #t) (kind 1)))) (hash "11apxxzn56jcbvxdpd9syz162c4638ai5fqksq47flri58n054rg")))

(define-public crate-chlorate-0.1 (crate (name "chlorate") (vers "0.1.3") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "prost") (req "^0.7") (default-features #t) (kind 0)) (crate-dep (name "prost-build") (req "^0.7") (default-features #t) (kind 1)))) (hash "16dldlhrx6y0jm50dykh6c7vs47h6l7kh0kk4vjz0lhdgjhii9y1")))

(define-public crate-chlorate-0.1 (crate (name "chlorate") (vers "0.1.4") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "prost") (req "^0.7") (default-features #t) (kind 0)) (crate-dep (name "prost-build") (req "^0.7") (default-features #t) (kind 1)))) (hash "13hv361shcsigkvsml3ibsqjsrg0apbs02fid7b7gh06mjgy68rz")))

(define-public crate-chlorate-0.1 (crate (name "chlorate") (vers "0.1.5") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "prost") (req "^0.7") (default-features #t) (kind 0)) (crate-dep (name "prost-build") (req "^0.7") (default-features #t) (kind 1)))) (hash "0fjqaplnyr2c1cxaxa1kii8w8z575zv5g17dad2zn3rdz4r9795x")))

(define-public crate-chlorate-0.1 (crate (name "chlorate") (vers "0.1.6") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "prost") (req "^0.7") (default-features #t) (kind 0)) (crate-dep (name "prost-build") (req "^0.7") (default-features #t) (kind 1)))) (hash "0kp81wvxcm1if3p0xnqxl221na1nbidwjfphfxiy1x11ysvh3ici")))

(define-public crate-chlorine-1 (crate (name "chlorine") (vers "1.0.0") (deps (list (crate-dep (name "libc") (req "^0.2") (target "cfg(not(windows))") (kind 0)))) (hash "0p8fc2la3037dpz9cw4204il2f4h8w8im21n5x705m5l9f2ss0gb")))

(define-public crate-chlorine-1 (crate (name "chlorine") (vers "1.0.1") (deps (list (crate-dep (name "libc") (req "^0.2") (target "cfg(not(windows))") (kind 0)))) (hash "1w089qgizw7ccw7zyczq3f6sjmddr6n4xkv66wq5qs01y9i2ph4l")))

(define-public crate-chlorine-1 (crate (name "chlorine") (vers "1.0.2") (deps (list (crate-dep (name "libc") (req "^0.2") (target "cfg(not(any(windows, all(target_arch = \"wasm32\", not(any(target_env = \"wasi\", target_env = \"emscripten\"))))))") (kind 0)))) (hash "15zv9d3gl8ybsdszxhs8p0p6vl8cvzpd8hiii7rj9nc76ij9a3k3")))

(define-public crate-chlorine-1 (crate (name "chlorine") (vers "1.0.3") (deps (list (crate-dep (name "libc") (req "^0.2") (target "cfg(not(any(windows, all(target_arch = \"wasm32\", not(any(target_env = \"wasi\", target_env = \"emscripten\"))))))") (kind 0)))) (hash "1jfzznfp7mdp08fzgafks2026knlqrn2jwgnnf1blw3zk6zy6jqd")))

(define-public crate-chlorine-1 (crate (name "chlorine") (vers "1.0.4") (hash "0rasb66z1qn7pk7gnb8zb6r5vysyx2zw3ssxrxnp1lx9m19xzcvf") (yanked #t)))

(define-public crate-chlorine-1 (crate (name "chlorine") (vers "1.0.5") (hash "0sfapvksknim81ry0n13xkjl5lpgy6dlhggkdqnj7gy2jn46skzj")))

(define-public crate-chlorine-1 (crate (name "chlorine") (vers "1.0.6") (hash "0zhvl5s5cs924wjw6qd94afh5cd43x8p5hd136l93nq7aad7jxs7") (features (quote (("int_extras"))))))

(define-public crate-chlorine-1 (crate (name "chlorine") (vers "1.0.7") (hash "1rhbmi70k1m2a2n7hrc385c3dlzcglbwz1l90p2vff8f2590ardx") (features (quote (("int_extras"))))))

(define-public crate-chlorine-1 (crate (name "chlorine") (vers "1.0.8") (hash "0xl544hdp3zl2ln765qbagswfz7ncmcl24hzij4na5zvyba97i2d") (features (quote (("int_extras")))) (yanked #t)))

(define-public crate-chlorine-1 (crate (name "chlorine") (vers "1.0.9") (hash "0s97jxylbgqzfqvlzfs5s18v83l0qh8kz0kawfbr6rcjld389v0i") (features (quote (("int_extras")))) (yanked #t)))

(define-public crate-chlorine-1 (crate (name "chlorine") (vers "1.0.10") (hash "1v2230qas1xapywgmk8zfi8l8zd8mwafb8z2x867rbx8cvlnyivm") (features (quote (("int_extras"))))))

(define-public crate-chlorine-1 (crate (name "chlorine") (vers "1.0.11") (hash "1a66cbnn6x6ml9cl8gg088zza9d2ylcws12rjpvynq2w9ff3h7cx") (features (quote (("int_extras"))))))

