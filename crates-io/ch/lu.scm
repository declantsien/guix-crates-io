(define-module (crates-io ch lu) #:use-module (crates-io))

(define-public crate-chlue-0.1 (crate (name "chlue") (vers "0.1.0") (deps (list (crate-dep (name "huelib") (req "^0.6.0") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.3.14") (default-features #t) (kind 0)) (crate-dep (name "termion") (req "^1.5.5") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.16") (default-features #t) (kind 0)))) (hash "0z71dg5iv7ak7iz9vazy81ixlhs8ahizybfmc0ygr3dywlh27758")))

