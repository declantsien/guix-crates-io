(define-module (crates-io ch ts) #:use-module (crates-io))

(define-public crate-chtsh-0.1 (crate (name "chtsh") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^3.0.14") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "futures") (req "^0.3.19") (default-features #t) (kind 0)) (crate-dep (name "hyper") (req "^0.14.16") (features (quote ("full"))) (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.16.1") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "053xlgydb4pzfz787mzjkbly4715clyln6h6nrmm48512ilb1mw2")))

(define-public crate-chtsh-0.1 (crate (name "chtsh") (vers "0.1.1") (deps (list (crate-dep (name "clap") (req "^3.0.14") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "futures") (req "^0.3.19") (default-features #t) (kind 0)) (crate-dep (name "hyper") (req "^0.14.16") (features (quote ("full"))) (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.16.1") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "0gpsbry7hpdynv20nws2q5kpg99r11icwiv958xwbs4jhr14pvmh")))

