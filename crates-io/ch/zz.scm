(define-module (crates-io ch zz) #:use-module (crates-io))

(define-public crate-chzzk-0.1 (crate (name "chzzk") (vers "0.1.0") (deps (list (crate-dep (name "reqwest") (req "^0.12.1") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.197") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.114") (default-features #t) (kind 0)))) (hash "1ynlpzhsybi4n6s3s0cv8szwkxqhl5rddkcwn7qv37xssn5iaj8w")))

(define-public crate-chzzk-0.2 (crate (name "chzzk") (vers "0.2.0") (deps (list (crate-dep (name "reqwest") (req "^0.12.1") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.197") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.114") (default-features #t) (kind 0)))) (hash "0f393rf7hbprvhrfb7i0kkqv9gl22zym2wai6nyjmz0hjxkrdsaz")))

