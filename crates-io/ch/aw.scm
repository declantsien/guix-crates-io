(define-module (crates-io ch aw) #:use-module (crates-io))

(define-public crate-chawuek-0.1 (crate (name "chawuek") (vers "0.1.0") (deps (list (crate-dep (name "anyhow") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.126") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.64") (default-features #t) (kind 0)) (crate-dep (name "tch") (req "^0.5.0") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0") (default-features #t) (kind 0)))) (hash "022f1kwwz1frl6vgbsdqd6r3p143d4kc0f80xm7s1wcw3sz6r79y")))

