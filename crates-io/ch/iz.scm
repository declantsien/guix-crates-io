(define-module (crates-io ch iz) #:use-module (crates-io))

(define-public crate-chizmo-0.1 (crate (name "chizmo") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^4.4.3") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "itertools") (req "^0.11.0") (default-features #t) (kind 0)) (crate-dep (name "nalgebra") (req "^0.32.3") (default-features #t) (kind 0)) (crate-dep (name "periodic_table") (req "^0.5.0") (default-features #t) (kind 0)) (crate-dep (name "phf") (req "^0.11.2") (features (quote ("macros"))) (default-features #t) (kind 0)) (crate-dep (name "pretty_assertions") (req "^1.4.0") (default-features #t) (kind 2)))) (hash "1vqf8bmn5ybc1nrhp5a8i72392fv2903lz916q9x8hhagymmfp3b")))

