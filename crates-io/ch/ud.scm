(define-module (crates-io ch ud) #:use-module (crates-io))

(define-public crate-chudnovsky-0.0.0 (crate (name "chudnovsky") (vers "0.0.0") (deps (list (crate-dep (name "dashu") (req "^0.4.0") (default-features #t) (kind 0)) (crate-dep (name "latexify") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "num") (req "^0.4.1") (default-features #t) (kind 0)))) (hash "05f12knx1wi75f6q3r2cf5cv6bj53widv7j9y3xmiybi6vf624mk") (features (quote (("default"))))))

(define-public crate-chudnovsky-0.0.1 (crate (name "chudnovsky") (vers "0.0.1") (deps (list (crate-dep (name "dashu") (req "^0.4.0") (default-features #t) (kind 0)) (crate-dep (name "latexify") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "num") (req "^0.4.1") (default-features #t) (kind 0)))) (hash "1j3z6wxx4grp3kkk7i7vsd8djwibnclf4xci57na49igd666c9hk") (features (quote (("default"))))))

