(define-module (crates-io ch ad) #:use-module (crates-io))

(define-public crate-chad-0.1 (crate (name "chad") (vers "0.1.0") (hash "07rh2a71c0mz1l5x2xl921414kkjf76pc3p6fiqb6hsmlb6c2y29")))

(define-public crate-chadboot-0.1 (crate (name "chadboot") (vers "0.1.0") (hash "0hx0mysfgf029a3pxkcszdi4xmf15463wwyhgw14a1an1i1brgz7")))

(define-public crate-chademo-rs-0.1 (crate (name "chademo-rs") (vers "0.1.0") (deps (list (crate-dep (name "embedded-can") (req "^0.4") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "embedded-hal") (req "^0.2.7") (optional #t) (default-features #t) (kind 0)))) (hash "0psrl5wqrs62pjh91algnbslpd84nwvzxd11q71klfj9rp71zjhh") (features (quote (("eh1" "embedded-can") ("eh0" "embedded-hal") ("default" "eh0"))))))

(define-public crate-chademo-rs-0.1 (crate (name "chademo-rs") (vers "0.1.1") (deps (list (crate-dep (name "embedded-can") (req "^0.4") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "embedded-hal") (req "^0.2.7") (optional #t) (default-features #t) (kind 0)))) (hash "13vf9080bjcr4nl1clw1kzrraiyqyfz6xjn1r8mm7pymnf49gm65") (features (quote (("eh1" "embedded-can") ("eh0" "embedded-hal") ("default" "eh0"))))))

(define-public crate-chadfetch-0.1 (crate (name "chadfetch") (vers "0.1.0") (deps (list (crate-dep (name "libc") (req "^0.2.152") (default-features #t) (kind 0)))) (hash "0pn3lb1lx4izxjczd8rxmxllfkj0x0qsyjlmy5cf0fyjn84d2749")))

(define-public crate-chadfetch-0.1 (crate (name "chadfetch") (vers "0.1.1") (deps (list (crate-dep (name "libc") (req "^0.2.152") (default-features #t) (kind 0)))) (hash "1nc94dcq5g2gv0q6m2m0yq9l2iqcsr6bk4v7dc24hfdzvkbrkkjg")))

(define-public crate-chadfetch-0.2 (crate (name "chadfetch") (vers "0.2.0") (deps (list (crate-dep (name "libc") (req "^0.2.152") (default-features #t) (kind 0)))) (hash "1dm55hnlnc99k6dirwczl27iglnjgkgvpb90h4avi8nw61hgd9n2")))

(define-public crate-chadfetch-0.2 (crate (name "chadfetch") (vers "0.2.1") (deps (list (crate-dep (name "libc") (req "^0.2.152") (default-features #t) (kind 0)))) (hash "11mdylg0d78cmnpxhbkrlpsbn664s3afsvg35cdicrc3csw8yn7k")))

(define-public crate-chadfetch-0.2 (crate (name "chadfetch") (vers "0.2.2") (deps (list (crate-dep (name "libc") (req "^0.2.152") (default-features #t) (kind 0)))) (hash "0dh44zjaxlydbq6rs33cvn1s36sjf01ywrqk54bihnvjmhvdsqn1")))

(define-public crate-chadfetch-0.2 (crate (name "chadfetch") (vers "0.2.3") (deps (list (crate-dep (name "libc") (req "^0.2.152") (default-features #t) (kind 0)))) (hash "1cb5yd1320yppqad4c16k1gifw9zxn0g0631pxchvrf2axx73y5g")))

(define-public crate-chadfetch-0.2 (crate (name "chadfetch") (vers "0.2.4") (deps (list (crate-dep (name "libc") (req "^0.2.152") (default-features #t) (kind 0)))) (hash "1dhhy5q2yinwfa5cg85hi828aszgp021akvdxsni0dyyv1yzg8sg")))

(define-public crate-chadfetch-0.2 (crate (name "chadfetch") (vers "0.2.5") (deps (list (crate-dep (name "libc") (req "^0.2.152") (default-features #t) (kind 0)))) (hash "06j6ba7p7c2bxv9b0brn33jhnczr5dvjgqaqhk3hxsj9sd0fkgf1")))

(define-public crate-chadfetch-0.2 (crate (name "chadfetch") (vers "0.2.6") (deps (list (crate-dep (name "libc") (req "^0.2.152") (default-features #t) (kind 0)))) (hash "118840dzgd9cz0ihklsragkrkhhbyrxwshkkhc0kpkcf6imqpgwm")))

(define-public crate-chadgpt-0.0.0 (crate (name "chadgpt") (vers "0.0.0") (deps (list (crate-dep (name "aio-cargo-info") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "anyhow") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "async-trait") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "bytes") (req "^1.1.0") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^4.2.2") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "crossterm") (req "^0.27") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "once_cell") (req "^1.18") (default-features #t) (kind 0)) (crate-dep (name "openssl") (req "^0.10") (features (quote ("vendored"))) (default-features #t) (target "cfg(target_os = \"linux\")") (kind 0)) (crate-dep (name "pin-project") (req "^1.1") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.7.3") (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.11") (features (quote ("gzip" "brotli" "deflate" "json" "stream" "default-tls"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.95") (default-features #t) (kind 0)) (crate-dep (name "serde_yaml") (req "^0.9") (default-features #t) (kind 0)) (crate-dep (name "smartstring") (req "^1.0") (features (quote ("serde"))) (default-features #t) (kind 0)) (crate-dep (name "tempfile") (req "^3.8") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1") (features (quote ("full"))) (default-features #t) (kind 0)) (crate-dep (name "tokio-stream") (req "^0.1.12") (default-features #t) (kind 0)) (crate-dep (name "tokio-util") (req "^0.7") (features (quote ("io"))) (default-features #t) (kind 0)))) (hash "1mh9snjibqn95caadigpslh9d0bpbcxkq6l25yrja4cnwg2mbygy")))

(define-public crate-chadinstall-0.1 (crate (name "chadinstall") (vers "0.1.0") (hash "0lamw5r5jbah8nixggw3laz9z6kr0fbx56siyf6kg9yiisbyi46l")))

(define-public crate-chados-0.1 (crate (name "chados") (vers "0.1.0") (hash "1h12k4m5362x12z6r7n5kx53krqkwwmzzp21j390cf0bkv1v6q1m")))

(define-public crate-chadrust-0.1 (crate (name "chadrust") (vers "0.1.0") (hash "197y89aqz1yr4bjxmr45rvr22vbamvx6yw8f2ljdckfj42i349f6")))

(define-public crate-chadscript-0.1 (crate (name "chadscript") (vers "0.1.0") (hash "0vqkz3ncw4rvahg7nv04nns97xim4qmh9jc4zli3iir2bpda6dpc")))

