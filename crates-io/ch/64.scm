(define-module (crates-io ch #{64}#) #:use-module (crates-io))

(define-public crate-ch641-0.0.1 (crate (name "ch641") (vers "0.0.1") (deps (list (crate-dep (name "critical-section") (req "^1.1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "riscv") (req "^0.10.1") (default-features #t) (kind 0)) (crate-dep (name "vcell") (req "^0.1") (default-features #t) (kind 0)))) (hash "0mkmxf7bl52ml8x1zv29jjvj2p54wyjsnbggbxrk3r8nk6hg1m30") (features (quote (("rt") ("default") ("ch641"))))))

(define-public crate-ch643-0.1 (crate (name "ch643") (vers "0.1.7") (deps (list (crate-dep (name "critical-section") (req "^1.1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "riscv") (req "^0.10.1") (default-features #t) (kind 0)) (crate-dep (name "vcell") (req "^0.1") (default-features #t) (kind 0)))) (hash "1m0s23q6411s90nmzblwjvhgval7imliifvq50b0wi8s1300y0yv") (features (quote (("rt") ("default") ("ch643"))))))

