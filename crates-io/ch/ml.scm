(define-module (crates-io ch ml) #:use-module (crates-io))

(define-public crate-chmlib-1 (crate (name "chmlib") (vers "1.0.0") (deps (list (crate-dep (name "bitflags") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "chmlib-sys") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0") (default-features #t) (kind 0)))) (hash "0ximpkl2ar1wsd0nsc096h4g0nmr114pynfhdry1yb1fnxnra388")))

(define-public crate-chmlib-sys-1 (crate (name "chmlib-sys") (vers "1.0.0") (deps (list (crate-dep (name "cc") (req "^1.0") (default-features #t) (kind 1)))) (hash "0clivyw5iqid6rx94k0dcr9libbibj0an75a6gdsq1913rqh245r") (links "chmlib")))

