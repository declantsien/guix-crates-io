(define-module (crates-io ch oo) #:use-module (crates-io))

(define-public crate-choo-0.1 (crate (name "choo") (vers "0.1.0") (hash "01wpd9amk0vyhqgfsrk4h6dhdh9cf2wgl0sxhkdgpsy082xgfnb8")))

(define-public crate-choochoo-0.0.0 (crate (name "choochoo") (vers "0.0.0") (hash "0bv09b9fsa84dsffxzjr8fbfv21naywd1z7iscqlpm9vi3jbfs3d")))

(define-public crate-choose-1 (crate (name "choose") (vers "1.1.2") (deps (list (crate-dep (name "lazy_static") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.3") (default-features #t) (kind 0)))) (hash "0080h3w75skxfsac8qflfhsn1ila92vkw4zpgsmjd99nsh9qhjqp")))

(define-public crate-choose-1 (crate (name "choose") (vers "1.2.0") (deps (list (crate-dep (name "lazy_static") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.3") (default-features #t) (kind 0)))) (hash "0qyvvdkyxdf3rbbxczf6d9yd78mir9hiihrmf8gijkmb84rm9c4b")))

(define-public crate-choose-1 (crate (name "choose") (vers "1.3.0") (deps (list (crate-dep (name "backslash") (req "^0") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.3") (default-features #t) (kind 0)))) (hash "1wxa1rjnc461scdlvafvv6srxp5sfy1jbdwdbgwhzpzybz6lkdvs")))

(define-public crate-choose-1 (crate (name "choose") (vers "1.3.1") (deps (list (crate-dep (name "backslash") (req "^0") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.3") (default-features #t) (kind 0)))) (hash "1hck74ipxq97mvfdvfc1v0dl66hhxms13s4vi6j1pa3q272b41fq")))

(define-public crate-choose-1 (crate (name "choose") (vers "1.3.2") (deps (list (crate-dep (name "backslash") (req "^0") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.3") (default-features #t) (kind 0)))) (hash "1xxlv15pj431c9ikjf8912iakdv9py92nmbcz7fazhqvd1c88b3s")))

(define-public crate-choose-1 (crate (name "choose") (vers "1.3.3") (deps (list (crate-dep (name "backslash") (req "^0") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.3") (default-features #t) (kind 0)))) (hash "1iq04npk9ln4ky37zkvp84bagcy20a19bgsakd1fpwh8c2nazzrm")))

(define-public crate-choose-1 (crate (name "choose") (vers "1.3.4") (deps (list (crate-dep (name "backslash") (req "^0") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.3") (default-features #t) (kind 0)))) (hash "0p14168ha5zg29b13km7j647f4wmddkw1hb3csqnlwxw3li0bc1c")))

(define-public crate-choose-rand-0.1 (crate (name "choose-rand") (vers "0.1.0") (deps (list (crate-dep (name "eq-float") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "fastrand") (req "^1.6") (default-features #t) (kind 0)))) (hash "19rpsw1rspi4a2rz5xmgkq7kjinbnwjr815gqya5hwcbwndcxm8r")))

(define-public crate-choose-rand-0.2 (crate (name "choose-rand") (vers "0.2.0") (deps (list (crate-dep (name "eq-float") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)) (crate-dep (name "weighted_rand") (req "^0.4.1") (default-features #t) (kind 0)))) (hash "0jr76y0a28magv5zwi10461avcc5z8bg9jkm34wi697rd30zn2d9") (features (quote (("eq_float") ("default"))))))

(define-public crate-choosen-0.0.1 (crate (name "choosen") (vers "0.0.1") (deps (list (crate-dep (name "reservoir-sampler") (req "^0.0.1") (default-features #t) (kind 0)))) (hash "1x14s8i7c7x6kbgi6a2xm01hjymlydzsxc2vlhb6vmbmpim0pjzm")))

(define-public crate-choosen-0.1 (crate (name "choosen") (vers "0.1.0") (deps (list (crate-dep (name "reservoir-sampler") (req "^0.0.1") (default-features #t) (kind 0)))) (hash "0cii4ibxlzfmyqzjvsx32rzpj4ldgp9wnv02876w66jm60wa0d2c")))

(define-public crate-chooser-0.1 (crate (name "chooser") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^4.0.22") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "pancurses") (req "^0.17.0") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)))) (hash "162wg7cpyi7pmnk6rx3kg85aivgb51xw7jcnhd9c89z4xxxn41kb")))

(define-public crate-chooser-0.2 (crate (name "chooser") (vers "0.2.0") (deps (list (crate-dep (name "clap") (req "^4.0.22") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "pancurses") (req "^0.17.0") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)))) (hash "1qsgb4qg9limpa0gvqnhbsi006vnhwp5bf50zrwr7308gzgir5jg")))

(define-public crate-choosier-0.1 (crate (name "choosier") (vers "0.1.0") (deps (list (crate-dep (name "serde") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "toml") (req "^0.5.6") (default-features #t) (kind 0)))) (hash "04b498ra50bgdh7f6fkgnp9yqzw71524qpvgh2gm9limzj26409n")))

