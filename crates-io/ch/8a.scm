(define-module (crates-io ch #{8a}#) #:use-module (crates-io))

(define-public crate-ch8alib-0.1 (crate (name "ch8alib") (vers "0.1.0") (deps (list (crate-dep (name "ch8-isa") (req "^0.1.1") (default-features #t) (kind 0)))) (hash "0cy7nvch4dvlcjjqwqsldaq8dwa8yxakwszq0apxa4s213sk9g71")))

(define-public crate-ch8asm-0.1 (crate (name "ch8asm") (vers "0.1.0") (deps (list (crate-dep (name "ch8alib") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "1xkp5ak0w5lhjji5z8hv1nxkk7gi8qiljx2ggfrcnw40apnymcna")))

(define-public crate-ch8asm-0.1 (crate (name "ch8asm") (vers "0.1.1") (deps (list (crate-dep (name "ch8alib") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "1c8q41pzldcmzzp1dqgs4xd7pj4rc545c2wk25b2nfv6qwk73x70")))

