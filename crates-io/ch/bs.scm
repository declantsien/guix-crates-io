(define-module (crates-io ch bs) #:use-module (crates-io))

(define-public crate-chbs-0.0.1 (crate (name "chbs") (vers "0.0.1") (deps (list (crate-dep (name "rand") (req "^0.5") (default-features #t) (kind 0)))) (hash "1vihpz999myzdzcmprj5qwhagaw8m9zv2sz2knzrpsnx9ihwz03m")))

(define-public crate-chbs-0.0.2 (crate (name "chbs") (vers "0.0.2") (deps (list (crate-dep (name "failure") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.5") (default-features #t) (kind 0)))) (hash "12pgg1wxj2hhzsdliv5zgxc088wmzvna792x51jfpm60nv3v9ml5")))

(define-public crate-chbs-0.0.3 (crate (name "chbs") (vers "0.0.3") (deps (list (crate-dep (name "derive_builder") (req "^0.6") (default-features #t) (kind 0)) (crate-dep (name "failure") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.5") (default-features #t) (kind 0)))) (hash "1r90pihky1bj0d4hb4ifmb2fpkinvbh55005ahai7cyfpcygxval")))

(define-public crate-chbs-0.0.4 (crate (name "chbs") (vers "0.0.4") (deps (list (crate-dep (name "derive_builder") (req "^0.6") (default-features #t) (kind 0)) (crate-dep (name "failure") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.5") (default-features #t) (kind 0)))) (hash "0995npgns6bhx6s09bza53k0n8lj1ix95s17bfsxxcw2wz13lk6a")))

(define-public crate-chbs-0.0.5 (crate (name "chbs") (vers "0.0.5") (deps (list (crate-dep (name "derive_builder") (req "^0.6") (default-features #t) (kind 0)) (crate-dep (name "failure") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.5") (default-features #t) (kind 0)))) (hash "1a8sk3xbg7rvbgc99wx4pbsibgkqwnm9pcm6xj2i1wxx6d2p1laq")))

(define-public crate-chbs-0.0.6 (crate (name "chbs") (vers "0.0.6") (deps (list (crate-dep (name "derive_builder") (req "^0.6") (default-features #t) (kind 0)) (crate-dep (name "failure") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.5") (default-features #t) (kind 0)))) (hash "19hrij3vkvhkw97hf1g574mlbgs37d458r2i4g80lkdajycn7k50")))

(define-public crate-chbs-0.0.7 (crate (name "chbs") (vers "0.0.7") (deps (list (crate-dep (name "derive_builder") (req "^0.6") (default-features #t) (kind 0)) (crate-dep (name "failure") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.5") (default-features #t) (kind 0)))) (hash "0hdl9cbaw3dgrkaavc4qgp05vi69z0jv31q8daxf97lng6ddk979")))

(define-public crate-chbs-0.0.8 (crate (name "chbs") (vers "0.0.8") (deps (list (crate-dep (name "derive_builder") (req "^0.6") (default-features #t) (kind 0)) (crate-dep (name "failure") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.5") (default-features #t) (kind 0)))) (hash "0dsw0ck901jiijhhs0wrzzwzy8czp4kd2443afi1s80zzcy7ydsh")))

(define-public crate-chbs-0.0.9 (crate (name "chbs") (vers "0.0.9") (deps (list (crate-dep (name "derive_builder") (req "^0.9") (default-features #t) (kind 0)) (crate-dep (name "failure") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.7") (default-features #t) (kind 0)))) (hash "0nli626g17d80xr6l1rs6z1hqm93zsv5r81bznr1fihlcpdzrbbh")))

(define-public crate-chbs-0.0.10 (crate (name "chbs") (vers "0.0.10") (deps (list (crate-dep (name "derive_builder") (req "^0.9") (default-features #t) (kind 0)) (crate-dep (name "failure") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.7") (default-features #t) (kind 0)))) (hash "1aw2k95xsi0gb888k0r3yf3dwgjlz5qmbg4ibyv8bq81552pk1cv")))

(define-public crate-chbs-0.1 (crate (name "chbs") (vers "0.1.0") (deps (list (crate-dep (name "derive_builder") (req "^0.9") (default-features #t) (kind 0)) (crate-dep (name "failure") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8") (default-features #t) (kind 0)))) (hash "1xzppmzdjazg608dasv275afc7gfg142w3vxncbh02dhg3ra2lrm")))

(define-public crate-chbs-0.1 (crate (name "chbs") (vers "0.1.1") (deps (list (crate-dep (name "derive_builder") (req "^0.10") (default-features #t) (kind 0)) (crate-dep (name "getrandom") (req "^0.2") (features (quote ("js"))) (default-features #t) (target "cfg(target_arch = \"wasm32\")") (kind 0)) (crate-dep (name "rand") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.31") (default-features #t) (kind 0)))) (hash "1ggaz9ia4ljcvdd601iwwkq7apl5kz7fhiiz5m13yi7ihy12k9s5")))

(define-public crate-chbs_password_checker-0.1 (crate (name "chbs_password_checker") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^2.33.3") (default-features #t) (kind 0)) (crate-dep (name "unicode-segmentation") (req "^1.7.1") (default-features #t) (kind 0)))) (hash "13q0gr0hwmh140firnk72s3sh1xyjll7b8mwc0vnw9hmq39nl21g")))

