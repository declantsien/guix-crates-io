(define-module (crates-io ch ii) #:use-module (crates-io))

(define-public crate-chiikawa-0.1 (crate (name "chiikawa") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^4.5.4") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "0dblv9k71zkph4kb2w3dqpzyi4sz8j7sf1x93v45fizvgzhp0a01")))

(define-public crate-chiisai-0.1 (crate (name "chiisai") (vers "0.1.0") (deps (list (crate-dep (name "futures") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "hyper") (req "^0.11") (default-features #t) (kind 0)))) (hash "1vn30f3vh39d9nnj4njdypxcvsbnkw05gk8fhzaih0rlbpqvs8bl") (yanked #t)))

(define-public crate-chiisai-0.1 (crate (name "chiisai") (vers "0.1.1") (deps (list (crate-dep (name "futures") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "hyper") (req "^0.11") (default-features #t) (kind 0)) (crate-dep (name "url") (req "^1.6") (default-features #t) (kind 0)))) (hash "0mv3vngfvlqgy5gd4s4wysg97y9cnxd2b045r2nbf0nca20y00dh")))

(define-public crate-chiisai-0.1 (crate (name "chiisai") (vers "0.1.2") (deps (list (crate-dep (name "futures") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "hyper") (req "^0.11") (default-features #t) (kind 0)) (crate-dep (name "url") (req "^1.6") (default-features #t) (kind 0)))) (hash "05xv05bvm65x59i1qknzv8ghwxj2m8590g4j3g0j4cirdvg1qxam") (yanked #t)))

(define-public crate-chiisai-0.1 (crate (name "chiisai") (vers "0.1.3") (deps (list (crate-dep (name "futures") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "hyper") (req "^0.11") (default-features #t) (kind 0)) (crate-dep (name "url") (req "^1.6") (default-features #t) (kind 0)))) (hash "1jmmqnrxy20llabps23spy9h8n1n744gfshv76sjk78m5drz59l2") (yanked #t)))

(define-public crate-chiisai-0.1 (crate (name "chiisai") (vers "0.1.4") (deps (list (crate-dep (name "futures") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "hyper") (req "^0.11") (default-features #t) (kind 0)) (crate-dep (name "url") (req "^1.6") (default-features #t) (kind 0)))) (hash "0kyv7ga1dgdis7mxyqiin6rz048b2ajwvw917j7r2fc2lnpb7rdf") (yanked #t)))

(define-public crate-chiisai-0.1 (crate (name "chiisai") (vers "0.1.5") (deps (list (crate-dep (name "futures") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "hyper") (req "^0.11") (default-features #t) (kind 0)) (crate-dep (name "url") (req "^1.6") (default-features #t) (kind 0)))) (hash "0zvh98z2ip73580j58xcs3vwpkxm41z7q3fdl2zbk9rvwcxlz9id") (yanked #t)))

(define-public crate-chiisai-0.1 (crate (name "chiisai") (vers "0.1.6") (deps (list (crate-dep (name "futures") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "hyper") (req "^0.11") (default-features #t) (kind 0)) (crate-dep (name "url") (req "^1.6") (default-features #t) (kind 0)))) (hash "01r66v3k151i3ksr2fzc2lp68d83a718g689nw6sh842dvazja81") (yanked #t)))

