(define-module (crates-io ch th) #:use-module (crates-io))

(define-public crate-chtholly-0.1 (crate (name "chtholly") (vers "0.1.0") (hash "1pbp3a1kb7jzvgg6ynbb8m1j9zwwwv939864jf6r62qzvv9qz272")))

(define-public crate-chtholly-0.2 (crate (name "chtholly") (vers "0.2.0") (hash "16f1jy62k8xkkznafp6d803bgpafdl7g9svlgrhjvafsb0rxg82d")))

(define-public crate-chtholly_tree-0.1 (crate (name "chtholly_tree") (vers "0.1.0") (hash "1rhvhm3hy1fb4pbk5kwmllksqbp472kx12sgsfsybmmpsw53vc58")))

(define-public crate-chtholly_tree-1 (crate (name "chtholly_tree") (vers "1.0.0") (hash "02y0aq2na6cwd54vvg55863xdgb94b3s4nny6cmdnswyizfy7x5n")))

