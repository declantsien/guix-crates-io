(define-module (crates-io ch ed) #:use-module (crates-io))

(define-public crate-cheddar-0.1 (crate (name "cheddar") (vers "0.1.0") (deps (list (crate-dep (name "glsl") (req "^0.9") (default-features #t) (kind 0)) (crate-dep (name "nom") (req "^3.2") (default-features #t) (kind 0)) (crate-dep (name "warmy") (req "^0.7") (default-features #t) (kind 0)))) (hash "0hb8hi3iag66b6wjykskjlnwsqrw74vf7433rynixfzzsszjn4xs")))

(define-public crate-cheddar-0.2 (crate (name "cheddar") (vers "0.2.0") (deps (list (crate-dep (name "glsl") (req "^0.9") (default-features #t) (kind 0)) (crate-dep (name "nom") (req "^3.2") (default-features #t) (kind 0)) (crate-dep (name "warmy") (req "^0.10") (default-features #t) (kind 0)))) (hash "1n2b7mrzaijay8cxs3dy62y3hb98ssmwszqfs3yg9271gi1frn7j")))

(define-public crate-cheddar-0.2 (crate (name "cheddar") (vers "0.2.1") (deps (list (crate-dep (name "glsl") (req "^0.9") (default-features #t) (kind 0)) (crate-dep (name "nom") (req "^3.2") (default-features #t) (kind 0)) (crate-dep (name "warmy") (req "^0.10") (default-features #t) (kind 0)))) (hash "08bmbj3n8grj2d2x2yvykvj4la2f29pl3v11b2s6xgm09qxi1grm")))

