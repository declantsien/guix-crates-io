(define-module (crates-io ch u-) #:use-module (crates-io))

(define-public crate-chu-liu-edmonds-0.1 (crate (name "chu-liu-edmonds") (vers "0.1.0") (deps (list (crate-dep (name "ndarray") (req "^0.15") (default-features #t) (kind 0)) (crate-dep (name "ndarray-rand") (req "^0.14") (default-features #t) (kind 2)) (crate-dep (name "ordered-float") (req "^4") (default-features #t) (kind 0)))) (hash "0alcnj1wz6c4vz0nv8h8zfmw604m0np2a7ciagcjlq070xvs3x03") (rust-version "1.66")))

