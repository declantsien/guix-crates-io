(define-module (crates-io ch on) #:use-module (crates-io))

(define-public crate-chongs_adder-0.1 (crate (name "chongs_adder") (vers "0.1.0") (hash "14rry4qp8bm1z05d5sz8gqqqz9wxcxva77gy7kmdh2kfnbqbmq8k")))

(define-public crate-chongs_adder-0.2 (crate (name "chongs_adder") (vers "0.2.0") (hash "07gycsx8s2kdyj99gc68ldd4fzjc2naiwzd2zrnghbwg0qrgwfqs")))

(define-public crate-chonk-0.1 (crate (name "chonk") (vers "0.1.0") (hash "0npc0s7f5r9rq96xjn26s9zchv40jypx74n8l7xhv9kz0zxsfznk")))

(define-public crate-chonk-0.2 (crate (name "chonk") (vers "0.2.0") (deps (list (crate-dep (name "paste") (req "^0.1") (default-features #t) (kind 0)))) (hash "04vc467iwgmslfbri0h3rxymsygpp0ky0babr0xcfi8ggnwb16ym")))

(define-public crate-chonk-0.3 (crate (name "chonk") (vers "0.3.0") (deps (list (crate-dep (name "paste") (req "^0.1") (default-features #t) (kind 0)))) (hash "14pq8r1m1z1bls2nqwc8bi9n30598wjysiqjldhjb2cpz7n091j4")))

(define-public crate-chonk-0.3 (crate (name "chonk") (vers "0.3.1") (deps (list (crate-dep (name "paste") (req "^0.1") (default-features #t) (kind 0)))) (hash "1v5wapjmavw802p3gdj5il85yn4jxk1419pxmbg13vda8lmjbxg2")))

(define-public crate-chonk-0.4 (crate (name "chonk") (vers "0.4.0") (deps (list (crate-dep (name "paste") (req "^0.1") (default-features #t) (kind 0)))) (hash "1pqdhr5kspwadz0vhhyjddn6ya03x1gnl97392qy5byfw4s4j82z")))

(define-public crate-chonk-0.5 (crate (name "chonk") (vers "0.5.0") (deps (list (crate-dep (name "paste") (req "^0.1") (default-features #t) (kind 0)))) (hash "0pkpprz9mrax5340hay53a9jfvzcppsj1gk7zl4z0wwwwilpjpc3")))

(define-public crate-chonk-cli-0.1 (crate (name "chonk-cli") (vers "0.1.0") (deps (list (crate-dep (name "ansi_term") (req "^0.12") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^3.1.10") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "exitcode") (req "^1.1.2") (default-features #t) (kind 0)))) (hash "195b65bny28w5qw0ghwimff7px3963ldigj3gcfj4kpm2jn3yg54")))

(define-public crate-chonk-cli-0.1 (crate (name "chonk-cli") (vers "0.1.1") (deps (list (crate-dep (name "ansi_term") (req "^0.12") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^3.1.10") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "exitcode") (req "^1.1.2") (default-features #t) (kind 0)))) (hash "1hzm623q1b0pg1ldndrvz33rwv0kf0sc0s61ws9y0s8gi63k5gjv")))

(define-public crate-chonker-0.0.0 (crate (name "chonker") (vers "0.0.0") (hash "07k387r4vskjz57k60q66nri4w00s04ipw8qsm33s212ghvj5mjl")))

