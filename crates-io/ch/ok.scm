(define-module (crates-io ch ok) #:use-module (crates-io))

(define-public crate-choki-1 (crate (name "choki") (vers "1.0.0") (deps (list (crate-dep (name "num_cpus") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "threadpool") (req "^1.8.1") (default-features #t) (kind 0)))) (hash "07rpgvac1kyfmmywa89xhd61ifz2dkz056g8s1ig5ywkxsw2m1xc")))

(define-public crate-choki-1 (crate (name "choki") (vers "1.0.1") (deps (list (crate-dep (name "num_cpus") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "threadpool") (req "^1.8.1") (default-features #t) (kind 0)))) (hash "002ajvidpksawprm7nbfgb1yjkrf6axhw3c0gx12x7f8qs54gkmr")))

(define-public crate-choki-1 (crate (name "choki") (vers "1.0.2") (deps (list (crate-dep (name "num_cpus") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "threadpool") (req "^1.8.1") (default-features #t) (kind 0)))) (hash "0g3kfrgqp5yrl89p3k2hahbpggzh41wqy2qscj60i7yzgdmzmpfi")))

(define-public crate-choki-1 (crate (name "choki") (vers "1.0.3") (deps (list (crate-dep (name "num_cpus") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "threadpool") (req "^1.8.1") (default-features #t) (kind 0)))) (hash "0iv96i5wxdjbsk4akcc9ikpfqwya9arhmcp8385i41wyb793m7z4")))

(define-public crate-choki-1 (crate (name "choki") (vers "1.0.4") (deps (list (crate-dep (name "num_cpus") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "threadpool") (req "^1.8.1") (default-features #t) (kind 0)))) (hash "0g5p3bgwv3cd5hp60rwyyiih6dll8pzbi4dj3iph8j1kqq8lk28k")))

