(define-module (crates-io ch ob) #:use-module (crates-io))

(define-public crate-chobitlibs-0.3 (crate (name "chobitlibs") (vers "0.3.1") (deps (list (crate-dep (name "criterion") (req "^0.4") (default-features #t) (kind 2)))) (hash "15wviir76ycrbcn9x0fcgqircsyxrd73f4b1l4kfsy6v8i1p7ka8")))

(define-public crate-chobitlibs-0.3 (crate (name "chobitlibs") (vers "0.3.2") (deps (list (crate-dep (name "criterion") (req "^0.4") (default-features #t) (kind 2)))) (hash "01vjnwpbf71cnnkww4daiqaq0fzi2534sbni9mmdvxfy872l5q4l")))

(define-public crate-chobitlibs-0.3 (crate (name "chobitlibs") (vers "0.3.3") (deps (list (crate-dep (name "criterion") (req "^0.4") (default-features #t) (kind 2)))) (hash "02ifpqy32fzpgrszxaxnh42l86g6zxn1pww5kvd1j6dl8dypcabk")))

(define-public crate-chobitlibs-0.4 (crate (name "chobitlibs") (vers "0.4.0") (deps (list (crate-dep (name "criterion") (req "^0.4") (default-features #t) (kind 2)))) (hash "0hc1ym73cp8w5y4hskdh66mbznflhvp5pyblspqmwrqjjrqzgvxz")))

(define-public crate-chobitlibs-0.4 (crate (name "chobitlibs") (vers "0.4.1") (deps (list (crate-dep (name "criterion") (req "^0.4") (default-features #t) (kind 2)))) (hash "1r9dcc2mzvj1h9fdqwkd0iy519nz87p1n3q4z2bvpkffbd472dnq")))

(define-public crate-chobitlibs-0.4 (crate (name "chobitlibs") (vers "0.4.2") (deps (list (crate-dep (name "criterion") (req "^0.4") (default-features #t) (kind 2)))) (hash "1c57lmh6fiwx2qp9i0k5h1s3w5qjdxqh8q536c24p2lcq1qqlxi2")))

(define-public crate-chobitlibs-0.5 (crate (name "chobitlibs") (vers "0.5.0") (deps (list (crate-dep (name "criterion") (req "^0.4") (default-features #t) (kind 2)))) (hash "19gym0xdhcq8hymf8s1nip8nlk3svn6sxfny28zhkg4q9g0qrgfd")))

(define-public crate-chobitlibs-0.5 (crate (name "chobitlibs") (vers "0.5.1") (deps (list (crate-dep (name "criterion") (req "^0.4") (default-features #t) (kind 2)))) (hash "1a4hqyd3smc749lpvzd1y9mvaw7l2491k3xqrpcgwjbnvyfys42p")))

(define-public crate-chobitlibs-0.6 (crate (name "chobitlibs") (vers "0.6.0") (deps (list (crate-dep (name "criterion") (req "^0.4") (default-features #t) (kind 2)))) (hash "1hn8685wdgrjx2lfar53jnhz5zmb98vvrnmg532yzbm8vdqn19pw")))

(define-public crate-chobitlibs-0.7 (crate (name "chobitlibs") (vers "0.7.0") (deps (list (crate-dep (name "criterion") (req "^0.4") (default-features #t) (kind 2)))) (hash "1n26c1g40sblw99161396ay4j6c3pmjzc0bpjvgycz87ygp3ml60")))

(define-public crate-chobitlibs-0.8 (crate (name "chobitlibs") (vers "0.8.0") (deps (list (crate-dep (name "criterion") (req "^0.4") (default-features #t) (kind 2)))) (hash "15k9iwn3d5xmvspspzn2inn6pswrc2z7mv3brskjl3yqj75qib5d")))

(define-public crate-chobs-0.1 (crate (name "chobs") (vers "0.1.0") (deps (list (crate-dep (name "anscape") (req "^0.2.2") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.147") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.89") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.3.26") (default-features #t) (kind 0)))) (hash "1vnspz2kxhbkb7v5lixrl0prx5nczyk94g61czxm0c98y1l2c040")))

(define-public crate-chobs-0.1 (crate (name "chobs") (vers "0.1.1") (deps (list (crate-dep (name "anscape") (req "^0.2.2") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.147") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.89") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.3.26") (default-features #t) (kind 0)))) (hash "0irjsx3ngys6fk3c2sy9i5i54aw750d1igzydhz1pb1h3rglgqf4")))

(define-public crate-chobs-0.1 (crate (name "chobs") (vers "0.1.2") (deps (list (crate-dep (name "anscape") (req "^0.2.2") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.147") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.89") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.3.26") (default-features #t) (kind 0)))) (hash "0gk62rff9wrhbbrffc3gphl134fsphn2mq8ygrldhsk9a07wsmdn")))

(define-public crate-chobs-0.1 (crate (name "chobs") (vers "0.1.3") (deps (list (crate-dep (name "anscape") (req "^0.2.2") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.147") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.89") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.3.26") (default-features #t) (kind 0)))) (hash "13p808jbd0pak01ib06l6hq0r21vv270s5rlnpcgqwsilf12vcj4")))

(define-public crate-chobs-0.1 (crate (name "chobs") (vers "0.1.4") (deps (list (crate-dep (name "anscape") (req "^0.2.2") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.147") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.89") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.3.26") (default-features #t) (kind 0)))) (hash "1m43dpa5gvjirqpdiybx7gsyfpf52vb4wsi5q5pfj0priw1341cn")))

(define-public crate-chobs-0.1 (crate (name "chobs") (vers "0.1.5") (deps (list (crate-dep (name "anscape") (req "^0.2.2") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.147") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.89") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.3.26") (default-features #t) (kind 0)))) (hash "1d1z21wy6g2xxvnb387bc6bl2cpv1j1yq8yz51lyhkwkwqjlwd1w")))

