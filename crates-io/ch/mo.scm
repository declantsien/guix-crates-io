(define-module (crates-io ch mo) #:use-module (crates-io))

(define-public crate-chmod-conversion-0.1 (crate (name "chmod-conversion") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^4.4.18") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "0hy6jbdkmqzkwjn0zacvpa15h6p0darlp0znmd66glxcxiprvd9f")))

(define-public crate-chmod-conversion-0.1 (crate (name "chmod-conversion") (vers "0.1.1") (deps (list (crate-dep (name "clap") (req "^4.4.18") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "051fvhxqcac9kj76xf7qmmjpa3n464pyyf4agjzpmmskqbhl97nq")))

(define-public crate-chmod-conversion-0.1 (crate (name "chmod-conversion") (vers "0.1.2") (deps (list (crate-dep (name "clap") (req "^4.4.18") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "1my5jfxwfw1drj27kriwfkdm13imihwxjpmmn9jlj2cycfbppmnv")))

(define-public crate-chmod-conversion-0.1 (crate (name "chmod-conversion") (vers "0.1.3") (deps (list (crate-dep (name "clap") (req "^4.4.18") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "031602d6akccw243cxy8g8c1fww4fb6gng3bcasq9w1ysdg5xjrl")))

(define-public crate-chmod-conversion-0.1 (crate (name "chmod-conversion") (vers "0.1.4") (deps (list (crate-dep (name "clap") (req "^4.4.18") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "1gd0ga5xyzf11hhppkpdn1qvq1cabrq0q4r469s4drh4l7jmzjy0")))

