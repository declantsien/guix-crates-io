(define-module (crates-io ch #{8-}#) #:use-module (crates-io))

(define-public crate-ch8-isa-0.1 (crate (name "ch8-isa") (vers "0.1.0") (deps (list (crate-dep (name "byteorder") (req "^1.3.2") (default-features #t) (kind 0)))) (hash "15ga6qshjjqsnvjlkl72hskmp0q9i1fskqh72p4xldhacywzc4ka")))

(define-public crate-ch8-isa-0.1 (crate (name "ch8-isa") (vers "0.1.1") (deps (list (crate-dep (name "byteorder") (req "^1.3.2") (default-features #t) (kind 0)))) (hash "1y5qsgx94p3q4iyj4swj107680kn2dmmlbmkbyg56bgc60r6x9zq")))

