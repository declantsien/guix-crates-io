(define-module (crates-io ch ic) #:use-module (crates-io))

(define-public crate-chic-0.0.0 (crate (name "chic") (vers "0.0.0") (hash "196k4s7nw1y4gc7dsb2wb3vrbqw3sbkin4n2wb9c1g73w6kf3d2a")))

(define-public crate-chic-1 (crate (name "chic") (vers "1.0.0") (deps (list (crate-dep (name "annotate-snippets") (req "^0.6.1") (default-features #t) (kind 0)))) (hash "0c74928i0bjv87lb7s9p9fcflvkfvd4axvqwar078qfzj8fmjkpa")))

(define-public crate-chic-1 (crate (name "chic") (vers "1.1.0") (deps (list (crate-dep (name "annotate-snippets") (req "^0.6.1") (default-features #t) (kind 0)))) (hash "1r4plh8c0glgabdavvqfbxhbk3r8np855dajpwpzxxqcqrdc2nwm")))

(define-public crate-chic-1 (crate (name "chic") (vers "1.1.1") (deps (list (crate-dep (name "annotate-snippets") (req "^0.6.1") (default-features #t) (kind 0)))) (hash "0zrsw1lbqn6q2avr2pbqln6fqzzrhvfjj6c4yiws1gnligxp67pb")))

(define-public crate-chic-1 (crate (name "chic") (vers "1.2.0") (deps (list (crate-dep (name "annotate-snippets") (req "^0.6.1") (default-features #t) (kind 0)))) (hash "06khfzra7bbracj6m1kwq0fj967jhj92fwpkqilygkr8bss0r7l4")))

(define-public crate-chic-1 (crate (name "chic") (vers "1.2.1") (deps (list (crate-dep (name "annotate-snippets") (req "^0.6.1") (default-features #t) (kind 0)))) (hash "12mc5igshk3g7racy2qdfx8ddklcsz5cy3ksdk37x7irfv584898")))

(define-public crate-chic-1 (crate (name "chic") (vers "1.2.2") (deps (list (crate-dep (name "annotate-snippets") (req "^0.6.1") (default-features #t) (kind 0)))) (hash "037pkdccj25gr4my8fq1qni9v87rydpyhfi2naf86mimkxhxpdd5")))

(define-public crate-chicken-0.0.0 (crate (name "chicken") (vers "0.0.0") (hash "1z8kjrg9jn6qibagi5lgnd7qlvxrsk54ac0jcnpxzmkxix5lfdnm") (yanked #t)))

(define-public crate-chicken_esolang-0.1 (crate (name "chicken_esolang") (vers "0.1.0") (deps (list (crate-dep (name "rstest") (req "^0.18.2") (default-features #t) (kind 2)))) (hash "0dzqiqgj6mk9qz507lrhkrq4f4sx87vx9f4rf45565451dqj7swf")))

(define-public crate-chicken_esolang-0.1 (crate (name "chicken_esolang") (vers "0.1.1") (deps (list (crate-dep (name "rstest") (req "^0.18.2") (default-features #t) (kind 2)))) (hash "1yir01gi1qnmrslnhwv3s1lhxjwsgx0y3xjdsgbcmaiw6j70vpgi")))

(define-public crate-chicken_esolang-0.1 (crate (name "chicken_esolang") (vers "0.1.2") (deps (list (crate-dep (name "rstest") (req "^0.18.2") (default-features #t) (kind 2)))) (hash "11ffmxsbl8pcqaa7yrv2d5wz83hjv7vb996py53imqr6x69a91bl")))

(define-public crate-chickenize-0.1 (crate (name "chickenize") (vers "0.1.1") (hash "0qbax7jsi0bhn6bniqjkqzg06806dil0lpz43qpx1is266kyydpb")))

(define-public crate-chickenize-0.2 (crate (name "chickenize") (vers "0.2.0") (hash "1fdqhr33jra681jgl8y9j3iih93w5x64kh686wrb9jfg3js674k2")))

(define-public crate-chickenize-0.2 (crate (name "chickenize") (vers "0.2.1") (hash "1yw6s1kkxblq45ydban5p5y5dxwf41h92yyn7nw7p3bg45jgh4bz")))

(define-public crate-chickenize-0.3 (crate (name "chickenize") (vers "0.3.0") (hash "0ij8lk29wshaf9ll8w62j9djc7311s626ivgr0fwqrlsaabyvxhf")))

(define-public crate-chickenize-0.3 (crate (name "chickenize") (vers "0.3.1") (hash "015w803hyw1l0spcdm9dg30fvvsd2kkbx8x2mhpa4k5zsmdyqnm7")))

(define-public crate-chickenize-0.3 (crate (name "chickenize") (vers "0.3.2") (hash "1mgnzfwqifbfiz21hyp4ikkx9qwmpfqvikcqdgfxr6hbbqiqgzas")))

(define-public crate-chickenize-0.3 (crate (name "chickenize") (vers "0.3.3") (hash "08dyhpsqrlb8cf4fi01hr6hds3fj0bfyl8q6kw4l93c0rvy31r6c")))

(define-public crate-chickensay-0.1 (crate (name "chickensay") (vers "0.1.0") (deps (list (crate-dep (name "assert_cmd") (req "^0.12.0") (default-features #t) (kind 0)) (crate-dep (name "colored") (req "^1.7.0") (default-features #t) (kind 0)) (crate-dep (name "exitfailure") (req "^0.5.1") (default-features #t) (kind 0)) (crate-dep (name "failure") (req "^0.1.5") (default-features #t) (kind 0)) (crate-dep (name "predicates") (req "^1.0.0") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.2.15") (default-features #t) (kind 0)))) (hash "0c6n67wbnba3wwpis8pv2nyj8q76vgb8adw1snh5c2gpyd6vk816")))

(define-public crate-chickensources-0.1 (crate (name "chickensources") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^4") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "fncli") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "image") (req "^0.24") (default-features #t) (kind 0)) (crate-dep (name "imageproc") (req "^0.23") (default-features #t) (kind 0)) (crate-dep (name "once_cell") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "rusttype") (req "^0.9") (default-features #t) (kind 0)))) (hash "0jnx4bq22i6limq6n38da2vy5d8zy421idsz1j53m1qhkmlmhv2g")))

(define-public crate-chickensources-0.1 (crate (name "chickensources") (vers "0.1.1") (deps (list (crate-dep (name "clap") (req "^4") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "fncli") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "image") (req "^0.24") (default-features #t) (kind 0)) (crate-dep (name "imageproc") (req "^0.23") (default-features #t) (kind 0)) (crate-dep (name "once_cell") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "rusttype") (req "^0.9") (default-features #t) (kind 0)))) (hash "03nfrqry58wm31xcsxbgbn9f9jgylgzvgslwx8kds62c4x7786yi")))

(define-public crate-chickensources-0.1 (crate (name "chickensources") (vers "0.1.2") (deps (list (crate-dep (name "clap") (req "^4") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "fncli") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "image") (req "^0.24") (default-features #t) (kind 0)) (crate-dep (name "imageproc") (req "^0.23") (default-features #t) (kind 0)) (crate-dep (name "once_cell") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "rusttype") (req "^0.9") (default-features #t) (kind 0)))) (hash "1c56k5b3b06vq1m6j7fl95gr8m9vfc65bnhgaj3sfdhhf3vlppsp")))

(define-public crate-chickenwire-0.1 (crate (name "chickenwire") (vers "0.1.0") (deps (list (crate-dep (name "criterion") (req "^0.2") (default-features #t) (kind 2)) (crate-dep (name "petgraph") (req "^0.4.13") (default-features #t) (kind 0)))) (hash "0w30bls9q0m3rs02881xvg8bpw0jqzlrh335sjmv2nfcvxw8c200")))

(define-public crate-chico-0.1 (crate (name "chico") (vers "0.1.0") (hash "15j638wb5wm155mb9qgsav3i72hc69s1wwbcyj6jgbq8sl0xaga4")))

(define-public crate-chico-0.2 (crate (name "chico") (vers "0.2.0") (deps (list (crate-dep (name "cliply") (req "^0.2.0") (default-features #t) (kind 0)))) (hash "0adwxqc67dbpdqx2frqk0358gqpc7jnbfzz4y6xnla6z1kbp0498")))

(define-public crate-chicon-0.1 (crate (name "chicon") (vers "0.1.0") (deps (list (crate-dep (name "rusoto_core") (req "^0.39.0") (default-features #t) (kind 0)) (crate-dep (name "rusoto_s3") (req "^0.39.0") (default-features #t) (kind 0)) (crate-dep (name "ssh2") (req "^0.3.3") (default-features #t) (kind 0)) (crate-dep (name "url") (req "^1.7.2") (default-features #t) (kind 0)))) (hash "1rnr9jxgfn5j6ifycrdqmlfgawbl9z8xdwfayqih3gn95gs64mds")))

(define-public crate-chicon-0.1 (crate (name "chicon") (vers "0.1.1") (deps (list (crate-dep (name "rusoto_core") (req "^0.39.0") (default-features #t) (kind 0)) (crate-dep (name "rusoto_s3") (req "^0.39.0") (default-features #t) (kind 0)) (crate-dep (name "ssh2") (req "^0.3.3") (default-features #t) (kind 0)) (crate-dep (name "url") (req "^1.7.2") (default-features #t) (kind 0)))) (hash "0f7yfma0r4lwrqrh9mx0vjqbjijq3913k0bmd8hvbw5nbiihkyby")))

(define-public crate-chicon-0.1 (crate (name "chicon") (vers "0.1.2") (deps (list (crate-dep (name "chrono") (req "^0.4.7") (features (quote ("serde"))) (default-features #t) (kind 0)) (crate-dep (name "env_logger") (req "^0.6.2") (default-features #t) (kind 0)) (crate-dep (name "failure") (req "^0.1.5") (default-features #t) (kind 0)) (crate-dep (name "futures") (req "^0.1.28") (default-features #t) (kind 0)) (crate-dep (name "osauth") (req "^0.2.4") (default-features #t) (kind 0)) (crate-dep (name "rusoto_core") (req "^0.39.0") (default-features #t) (kind 0)) (crate-dep (name "rusoto_s3") (req "^0.39.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.94") (default-features #t) (kind 0)) (crate-dep (name "ssh2") (req "^0.3.3") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^0.1.22") (default-features #t) (kind 0)) (crate-dep (name "url") (req "^1.7.2") (default-features #t) (kind 0)))) (hash "09k1c9w0kjb6bl545yyms4qw817mh74a0vlllnyccpwrm8nz6hzb")))

(define-public crate-chicon-0.1 (crate (name "chicon") (vers "0.1.3") (deps (list (crate-dep (name "chrono") (req "^0.4.7") (features (quote ("serde"))) (default-features #t) (kind 0)) (crate-dep (name "env_logger") (req "^0.6.2") (default-features #t) (kind 0)) (crate-dep (name "failure") (req "^0.1.5") (default-features #t) (kind 0)) (crate-dep (name "futures") (req "^0.1.28") (default-features #t) (kind 0)) (crate-dep (name "osauth") (req "^0.2.4") (default-features #t) (kind 0)) (crate-dep (name "rusoto_core") (req "^0.39.0") (default-features #t) (kind 0)) (crate-dep (name "rusoto_s3") (req "^0.39.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.94") (default-features #t) (kind 0)) (crate-dep (name "ssh2") (req "^0.3.3") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^0.1.22") (default-features #t) (kind 0)) (crate-dep (name "url") (req "^1.7.2") (default-features #t) (kind 0)))) (hash "0kbk2ipvx3k95n3fpgzaqpd1iv9sylx0vhscy3icbbj5c7zpr1jl")))

(define-public crate-chicon-0.1 (crate (name "chicon") (vers "0.1.4") (deps (list (crate-dep (name "chrono") (req "^0.4.7") (features (quote ("serde"))) (default-features #t) (kind 0)) (crate-dep (name "env_logger") (req "^0.6.2") (default-features #t) (kind 0)) (crate-dep (name "failure") (req "^0.1.5") (default-features #t) (kind 0)) (crate-dep (name "futures") (req "^0.1.28") (default-features #t) (kind 0)) (crate-dep (name "osauth") (req "^0.2.4") (default-features #t) (kind 0)) (crate-dep (name "rusoto_core") (req "^0.42.0") (default-features #t) (kind 0)) (crate-dep (name "rusoto_s3") (req "^0.42.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.94") (default-features #t) (kind 0)) (crate-dep (name "ssh2") (req "^0.3.3") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^0.1.22") (default-features #t) (kind 0)) (crate-dep (name "url") (req "^1.7.2") (default-features #t) (kind 0)))) (hash "1ll2676fw5vjklarf4n2vrsfqbi53wp514qf7knsr64hcx8pblja")))

