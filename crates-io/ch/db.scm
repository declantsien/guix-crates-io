(define-module (crates-io ch db) #:use-module (crates-io))

(define-public crate-chdb-0.1 (crate (name "chdb") (vers "0.1.0") (deps (list (crate-dep (name "chdb-bindings-rs") (req "^0.1.5") (default-features #t) (kind 0)) (crate-dep (name "enum2str") (req "^0.1.9") (default-features #t) (kind 0)))) (hash "16z8b2ac054iirqhncpp71wkaxnc1igqi9q956qjy3nm9889hxiq")))

(define-public crate-chdb-0.1 (crate (name "chdb") (vers "0.1.1") (deps (list (crate-dep (name "chdb-bindings-rs") (req "^0.1.5") (default-features #t) (kind 0)) (crate-dep (name "enum2str") (req "^0.1.9") (default-features #t) (kind 0)))) (hash "0crfnns7ly054cc9vpaf9lba3k80gsr6zk6vlz07z7q4q5hyb9z7")))

(define-public crate-chdb-0.1 (crate (name "chdb") (vers "0.1.2") (deps (list (crate-dep (name "chdb-bindings-rs") (req "^0.1.5") (default-features #t) (kind 0)) (crate-dep (name "enum2str") (req "^0.1.9") (default-features #t) (kind 0)))) (hash "08knggaph6vjcmmrn42p89y42psf3g79nww40mm1rssaslzjlpdn")))

(define-public crate-chdb-bindings-rs-0.1 (crate (name "chdb-bindings-rs") (vers "0.1.0") (deps (list (crate-dep (name "bindgen") (req "^0.69.1") (default-features #t) (kind 1)) (crate-dep (name "reqwest") (req "^0.11.22") (features (quote ("blocking"))) (default-features #t) (kind 1)) (crate-dep (name "zip") (req "^0.6.6") (default-features #t) (kind 1)))) (hash "0qlcdgblj5qnwmq1gjvc99vwin5qkf7fcgc8vj3p1db1bydh80sc") (yanked #t)))

(define-public crate-chdb-bindings-rs-0.1 (crate (name "chdb-bindings-rs") (vers "0.1.1") (deps (list (crate-dep (name "reqwest") (req "^0.11.22") (features (quote ("blocking"))) (default-features #t) (kind 1)) (crate-dep (name "zip") (req "^0.6.6") (default-features #t) (kind 1)))) (hash "1kb60if3mdkmvvbpmbx5f82fccghn8k4haichdxvidmqkrjfmjk1") (yanked #t)))

(define-public crate-chdb-bindings-rs-0.1 (crate (name "chdb-bindings-rs") (vers "0.1.2") (hash "1x49b62ihwl4l48nyjnjfy4lqz0kia56g1haadvlgp00wz6h0c2b") (yanked #t)))

(define-public crate-chdb-bindings-rs-0.1 (crate (name "chdb-bindings-rs") (vers "0.1.4") (hash "0xkwjmh6742zjwfl21gzqfbkwy8inglcw8z9wbnbdpglhmvkg7vd") (yanked #t)))

(define-public crate-chdb-bindings-rs-0.1 (crate (name "chdb-bindings-rs") (vers "0.1.5") (hash "18lzr8k05za8q50b7gr60bfpc9c8g94yr4gxijr52zxa010dnmcq")))

(define-public crate-chdb-bindings-rs-0.1 (crate (name "chdb-bindings-rs") (vers "0.1.6") (hash "0l4nj0qkys5w7g67a9dvrw7n17habbwcclhsm7zirzrzdmmlnzww")))

