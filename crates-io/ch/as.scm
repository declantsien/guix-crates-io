(define-module (crates-io ch as) #:use-module (crates-io))

(define-public crate-chasa-0.1 (crate (name "chasa") (vers "0.1.0") (deps (list (crate-dep (name "either") (req "^1.6.1") (default-features #t) (kind 0)))) (hash "1bmrxzh83sgjh9l46vjmqbd5874ckqvq3l7z3k60dc315acfpnly")))

(define-public crate-chasa-0.1 (crate (name "chasa") (vers "0.1.1") (deps (list (crate-dep (name "either") (req "^1.6.1") (default-features #t) (kind 0)))) (hash "08kdg9ivf6v2kb8knkzig73wjbqnvpg5kazppdbh6wny5fkki5vy")))

(define-public crate-chasa-0.1 (crate (name "chasa") (vers "0.1.2") (deps (list (crate-dep (name "either") (req "^1.6.1") (default-features #t) (kind 0)))) (hash "03dr9s46c3jar8srpm7k4wdgk3ay1xgrf1vcx388blaq3q3llsy6")))

(define-public crate-chasa-0.1 (crate (name "chasa") (vers "0.1.3") (deps (list (crate-dep (name "either") (req "^1.6.1") (default-features #t) (kind 0)))) (hash "1b8gz8i3h6ivgqkzpy7n5gv0lswy1qn2s7my85b7p7hnx3m1p997")))

(define-public crate-chasa-0.1 (crate (name "chasa") (vers "0.1.4") (deps (list (crate-dep (name "either") (req "^1.6.1") (default-features #t) (kind 0)))) (hash "1xj8h2vhhbqs5r7i0qlfg4fjg7jklhd1dh4pnlsrk4907rpnf5bf")))

(define-public crate-chasa-0.1 (crate (name "chasa") (vers "0.1.5") (deps (list (crate-dep (name "either") (req "^1.6.1") (default-features #t) (kind 0)))) (hash "1swrhvl90rwwmf807dfa53jnbmgpvrr9g6gwl8rbjnj0vz4dap6k")))

(define-public crate-chasa-0.1 (crate (name "chasa") (vers "0.1.6") (deps (list (crate-dep (name "either") (req "^1.6.1") (default-features #t) (kind 0)))) (hash "09wy5walnayibq4qwlkncnwdbivkmarrzy5bscvcb19a2h7rvmch")))

(define-public crate-chasa-0.1 (crate (name "chasa") (vers "0.1.7") (deps (list (crate-dep (name "either") (req "^1.6.1") (default-features #t) (kind 0)))) (hash "0m4jyy39dzgn9qq61jc3wqzx2vjfj5m5rggvs9xjwgab4k55m070")))

(define-public crate-chasa-0.1 (crate (name "chasa") (vers "0.1.8") (deps (list (crate-dep (name "either") (req "^1.6.1") (default-features #t) (kind 0)))) (hash "1xafzhf3f0ddrkpm3z9lyi8vqqcrmmv5fcgdsr999m3hrabnr20d")))

(define-public crate-chasa-0.1 (crate (name "chasa") (vers "0.1.9") (deps (list (crate-dep (name "either") (req "^1.6.1") (default-features #t) (kind 0)))) (hash "0ambsgj486970bnhpr5zqy1mvj06448b90ywbrb2gh2xg6sqcjw1")))

(define-public crate-chasa-0.1 (crate (name "chasa") (vers "0.1.10") (deps (list (crate-dep (name "either") (req "^1.6.1") (default-features #t) (kind 0)))) (hash "1avvhs8c4s2rlr39gadyk2hbfkdjybaqjjbb5ppy3l2jlqxr6m6i")))

(define-public crate-chasa-0.1 (crate (name "chasa") (vers "0.1.11") (deps (list (crate-dep (name "either") (req "^1.6.1") (default-features #t) (kind 0)))) (hash "09cdjywzi1ki75970655srzzk08f5l7axlvrmmymlwbyv4nvc7a3")))

(define-public crate-chasa-0.1 (crate (name "chasa") (vers "0.1.12") (deps (list (crate-dep (name "either") (req "^1.6.1") (default-features #t) (kind 0)))) (hash "0v2issplifxw1prv42fpc3716cmjhm3xxlxinr8658fqx55fdirj")))

(define-public crate-chasa-0.1 (crate (name "chasa") (vers "0.1.13") (deps (list (crate-dep (name "either") (req "^1.6.1") (default-features #t) (kind 0)))) (hash "0rpvfv8zrvxq1scsisg20hcfmfvn01caq8vzvkcl9768lg97yrsj")))

(define-public crate-chase-0.1 (crate (name "chase") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^2.30") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "futures") (req "^0.1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0") (optional #t) (default-features #t) (kind 0)))) (hash "1lw7fx7p98n5y66lqsq12s8kvac78m3mpxaysb2z9s8g2lgiskrz") (features (quote (("with-serde" "serde" "serde_derive") ("stream" "futures") ("binary" "clap"))))))

(define-public crate-chase-0.1 (crate (name "chase") (vers "0.1.1") (deps (list (crate-dep (name "clap") (req "^2.30") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "futures") (req "^0.1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0") (optional #t) (default-features #t) (kind 0)))) (hash "069hg8j9jy155cld5ddssn0hm936mqcq8cwvmdvj5l78h8dvqh9b") (features (quote (("with-serde" "serde" "serde_derive") ("stream" "futures") ("binary" "clap"))))))

(define-public crate-chase-0.1 (crate (name "chase") (vers "0.1.2") (deps (list (crate-dep (name "clap") (req "^2.30") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "futures") (req "^0.1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0") (optional #t) (default-features #t) (kind 0)))) (hash "1hi9qlz9dp9d378jqkc0qlck75652klhi8bxnnvzxn1097c39c4l") (features (quote (("with-serde" "serde" "serde_derive") ("stream" "futures") ("binary" "clap"))))))

(define-public crate-chase-0.1 (crate (name "chase") (vers "0.1.3") (deps (list (crate-dep (name "clap") (req "^2.30") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "futures") (req "^0.1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0") (optional #t) (default-features #t) (kind 0)))) (hash "0q8w72427n3v27jzgvqa2ggn24ypb8pw7k46gd1ia7yhmdl5gb0a") (features (quote (("with-serde" "serde" "serde_derive") ("stream" "futures") ("binary" "clap"))))))

(define-public crate-chase-0.1 (crate (name "chase") (vers "0.1.4") (deps (list (crate-dep (name "clap") (req "^2.30") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "futures") (req "^0.1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "tempdir") (req "^0.3") (default-features #t) (kind 2)))) (hash "0w92c747mfl5yj98xab5k6yhl1900vk2vr5y4kqbjb1yrfzkqpkk") (features (quote (("with-serde" "serde" "serde_derive") ("stream" "futures") ("binary" "clap"))))))

(define-public crate-chase-0.1 (crate (name "chase") (vers "0.1.5") (deps (list (crate-dep (name "clap") (req "^2.30") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "futures") (req "^0.1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "tempdir") (req "^0.3") (default-features #t) (kind 2)))) (hash "011w8skg01p2l482anyak7c305c5bqpnc4bj8b9lxxi4w1ip9wgi") (features (quote (("with-serde" "serde" "serde_derive") ("stream" "futures") ("binary" "clap"))))))

(define-public crate-chase-0.1 (crate (name "chase") (vers "0.1.6") (deps (list (crate-dep (name "clap") (req "^2.30") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "futures") (req "^0.1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "tempdir") (req "^0.3") (default-features #t) (kind 2)))) (hash "00lsf4bkicbi3p0yr1d07djs5cmjng3iqnkgxhaw787zck7vh96v") (features (quote (("with-serde" "serde" "serde_derive") ("stream" "futures") ("binary" "clap"))))))

(define-public crate-chase-0.1 (crate (name "chase") (vers "0.1.7") (deps (list (crate-dep (name "clap") (req "^2.30") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "futures") (req "^0.1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "tempdir") (req "^0.3") (default-features #t) (kind 2)))) (hash "0rvbc03vvkdqwr6vmaz04a52l1kdlc25wdmms5pkqh4cmiwm4pcc") (features (quote (("with-serde" "serde" "serde_derive") ("stream" "futures") ("binary" "clap"))))))

(define-public crate-chase-0.1 (crate (name "chase") (vers "0.1.8") (deps (list (crate-dep (name "clap") (req "^2.30") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "futures") (req "^0.1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "tempdir") (req "^0.3") (default-features #t) (kind 2)))) (hash "11298z06g9j7zngf83wvzg8fhgi097b2z9m6wx11l4dldzzpa68k") (features (quote (("with-serde" "serde" "serde_derive") ("stream" "futures") ("binary" "clap"))))))

(define-public crate-chasement-0.1 (crate (name "chasement") (vers "0.1.0") (deps (list (crate-dep (name "nohash") (req "^0.2.0") (default-features #t) (kind 0)))) (hash "1m3d4nbfr4haj39cqlph2bb07zf6kl48n8g21xq1yazsfxv6yn6n")))

(define-public crate-chash-0.1 (crate (name "chash") (vers "0.1.0") (hash "0vh06pi11qgqrncfw6zfmkjyqxi3mmlbzwn2i7p1bzhyidqayji4")))

(define-public crate-chashmap-0.1 (crate (name "chashmap") (vers "0.1.0") (deps (list (crate-dep (name "owning_ref") (req "^0") (default-features #t) (kind 0)) (crate-dep (name "parking_lot") (req "^0") (default-features #t) (kind 0)))) (hash "13dmhhlivw32d1s43f672czy9909yhfvdd0mj72p0jwry0k0dg58")))

(define-public crate-chashmap-0.1 (crate (name "chashmap") (vers "0.1.1") (deps (list (crate-dep (name "owning_ref") (req "^0") (default-features #t) (kind 0)) (crate-dep (name "parking_lot") (req "^0") (default-features #t) (kind 0)))) (hash "0wma2ismphrjiavkdqjs2lz7d7h5901m7xfpfq389ar4s92ibgk6")))

(define-public crate-chashmap-0.1 (crate (name "chashmap") (vers "0.1.2") (deps (list (crate-dep (name "owning_ref") (req "^0") (default-features #t) (kind 0)) (crate-dep (name "parking_lot") (req "^0") (default-features #t) (kind 0)))) (hash "1x7sjrjar5i5p8w6px8m03brc2qygxndy2a2akkx1xrr1zz9jfqx")))

(define-public crate-chashmap-0.2 (crate (name "chashmap") (vers "0.2.0") (deps (list (crate-dep (name "owning_ref") (req "^0") (default-features #t) (kind 0)) (crate-dep (name "parking_lot") (req "^0") (default-features #t) (kind 0)))) (hash "1yfq4y983i47y0v0g6rlz0i5prq87i84kqzmvpd7gxx4xvfpgbzp")))

(define-public crate-chashmap-1 (crate (name "chashmap") (vers "1.0.0") (deps (list (crate-dep (name "owning_ref") (req "^0") (default-features #t) (kind 0)) (crate-dep (name "parking_lot") (req "^0") (default-features #t) (kind 0)))) (hash "1j5ksabmsydfmm1bv9mdlm6w2cqsyxnffm7vb8lxc843iy967rnn")))

(define-public crate-chashmap-1 (crate (name "chashmap") (vers "1.1.0") (deps (list (crate-dep (name "owning_ref") (req "^0") (default-features #t) (kind 0)) (crate-dep (name "parking_lot") (req "^0") (default-features #t) (kind 0)))) (hash "1rll61r6fdhyhlij8cj7bic1j1a6s8rjx1r5a95b12hvbgl1b1i3")))

(define-public crate-chashmap-1 (crate (name "chashmap") (vers "1.2.0") (deps (list (crate-dep (name "owning_ref") (req "^0") (default-features #t) (kind 0)) (crate-dep (name "parking_lot") (req "^0") (default-features #t) (kind 0)))) (hash "16kk3ldyb6m3k96yj692bfqj6bccaw6ca767kxw5i3fjd2wi6brs")))

(define-public crate-chashmap-2 (crate (name "chashmap") (vers "2.0.0") (deps (list (crate-dep (name "owning_ref") (req "^0") (default-features #t) (kind 0)) (crate-dep (name "parking_lot") (req "^0") (default-features #t) (kind 0)))) (hash "0z6fllz28rsdaq8yw240d2wgz8pdjp115x9jfhnpc0hd87zfkxwl")))

(define-public crate-chashmap-2 (crate (name "chashmap") (vers "2.1.0") (deps (list (crate-dep (name "owning_ref") (req "^0") (default-features #t) (kind 0)) (crate-dep (name "parking_lot") (req "^0") (default-features #t) (kind 0)))) (hash "033sac0jz1i70ynfn8krsjbsfg4mnv1s6nh8sa1wr10gl9n06hpp")))

(define-public crate-chashmap-2 (crate (name "chashmap") (vers "2.1.1") (deps (list (crate-dep (name "owning_ref") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "parking_lot") (req "^0.3") (default-features #t) (kind 0)))) (hash "17nlar022qranickv6g4xl1c1z6d2vdbvph15a7gb0g5s76wwvsq")))

(define-public crate-chashmap-2 (crate (name "chashmap") (vers "2.2.0") (deps (list (crate-dep (name "owning_ref") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "parking_lot") (req "^0.3") (default-features #t) (kind 0)))) (hash "1y6z2d931z9ccq9fpqmvyb37clp764jmww0gffmbn37bq6l53rj7")))

(define-public crate-chashmap-2 (crate (name "chashmap") (vers "2.2.2") (deps (list (crate-dep (name "owning_ref") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "parking_lot") (req "^0.4") (default-features #t) (kind 0)))) (hash "0igsvpc2ajd6w68w4dwn0fln6yww8gq4pq9x02wj36g3q71a6hgz")))

(define-public crate-chashmap-async-0.1 (crate (name "chashmap-async") (vers "0.1.0") (deps (list (crate-dep (name "async-lock") (req "^2.4") (default-features #t) (kind 0)) (crate-dep (name "futures") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "owning_ref") (req "^0.4.4") (features (quote ("async"))) (default-features #t) (kind 0) (package "owning_ref_async")) (crate-dep (name "stable_deref_trait") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.0") (features (quote ("macros" "rt-multi-thread"))) (default-features #t) (kind 2)))) (hash "0a8jcs742xa0qr5yxny3xbynqlcy19y37cns2hkvasnwkywhw10f")))

(define-public crate-chashmap-async-0.1 (crate (name "chashmap-async") (vers "0.1.1") (deps (list (crate-dep (name "async-lock") (req "^3.0") (default-features #t) (kind 0)) (crate-dep (name "futures") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "futures") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "owning_ref") (req "^0.4.4") (features (quote ("async"))) (default-features #t) (kind 0) (package "owning_ref_async")) (crate-dep (name "stable_deref_trait") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.0") (features (quote ("macros" "rt-multi-thread"))) (default-features #t) (kind 2)))) (hash "0q2984qdillpsyndwmqbpkvwg4zkbfvbb8zh19hsvqjwrqn1g268")))

(define-public crate-chashmap-next-2 (crate (name "chashmap-next") (vers "2.2.3") (deps (list (crate-dep (name "owning_ref") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "parking_lot") (req "^0.11") (features (quote ("owning_ref"))) (default-features #t) (kind 0)))) (hash "189mw611n1cfqffz7aglz0ymdvyk5h4k5xl5w94pxjdbz8i3qif6")))

(define-public crate-chashmap-serde-0.1 (crate (name "chashmap-serde") (vers "0.1.0") (deps (list (crate-dep (name "owning_ref") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "parking_lot") (req "^0.12.1") (features (quote ("owning_ref"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.145") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.85") (default-features #t) (kind 2)))) (hash "1w8ysp3gv0ihrc7hfmyyc7nx6lj7f4pqpf6ynph6q0mns2q73rwi")))

(define-public crate-chashmap-serde-0.1 (crate (name "chashmap-serde") (vers "0.1.1") (deps (list (crate-dep (name "owning_ref") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "parking_lot") (req "^0.12.1") (features (quote ("owning_ref"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.145") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.85") (default-features #t) (kind 2)))) (hash "1yb56fjcx3j6jx84mdkq8aikfl6ibf6aqf07b5w45rsrcgpgn6dk")))

(define-public crate-chashmap-serde-2 (crate (name "chashmap-serde") (vers "2.2.3") (deps (list (crate-dep (name "owning_ref") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "parking_lot") (req "^0.12.1") (features (quote ("owning_ref"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.145") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.85") (default-features #t) (kind 2)))) (hash "05x86s6kxyysa0c4bf3vb9xivdgksdciq2jq1rwa4v0j1xm4wg2b")))

(define-public crate-chasm-0.0.0 (crate (name "chasm") (vers "0.0.0") (hash "0kkvcshsm3bpfmq1w61gzy5alfs45yj1qlv8bxh39lgq310cfz8b")))

(define-public crate-chasm-rs-0.1 (crate (name "chasm-rs") (vers "0.1.0") (deps (list (crate-dep (name "blake3") (req "^1") (default-features #t) (kind 2)) (crate-dep (name "leb128") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "logos") (req "^0.12") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "wasmi") (req "^0.9") (default-features #t) (kind 2)))) (hash "1qjixvvadgr2vhq8lrvyz5d7prjblbdkh7rnlvsbflacyhpk1wav")))

(define-public crate-chassis-0.1 (crate (name "chassis") (vers "0.1.0") (deps (list (crate-dep (name "assert_matches") (req "^1.3") (default-features #t) (kind 2)) (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (features (quote ("full"))) (default-features #t) (kind 0)) (crate-dep (name "trybuild") (req "^1.0.32") (default-features #t) (kind 2)) (crate-dep (name "version_check") (req "^0.9") (default-features #t) (kind 1)))) (hash "0w5bd230p7fjbyqh326k8fyfz43blf7sahjqyps5c4lzc480w6xn")))

(define-public crate-chassis-0.1 (crate (name "chassis") (vers "0.1.1") (deps (list (crate-dep (name "assert_matches") (req "^1.3") (default-features #t) (kind 2)) (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (features (quote ("full"))) (default-features #t) (kind 0)) (crate-dep (name "trybuild") (req "^1.0.32") (default-features #t) (kind 2)) (crate-dep (name "version_check") (req "^0.9") (default-features #t) (kind 1)))) (hash "1gwcbfzp575n20zbf3lgz87s5jpgza563bfp4zpk0d6z8n5bab4w")))

(define-public crate-chassis-0.2 (crate (name "chassis") (vers "0.2.0") (deps (list (crate-dep (name "assert_matches") (req "^1.5") (default-features #t) (kind 2)) (crate-dep (name "chassis-proc-macros") (req "=0.2.0") (default-features #t) (kind 0)) (crate-dep (name "trybuild") (req "^1.0.84") (default-features #t) (kind 2)) (crate-dep (name "version-sync") (req "^0.9") (default-features #t) (kind 2)))) (hash "0aqpal48dq1a4044vfi2hcwcb5bncim6d0lc0w6mdcw6aqjmdfl2")))

(define-public crate-chassis-proc-macros-0.2 (crate (name "chassis-proc-macros") (vers "0.2.0") (deps (list (crate-dep (name "darling") (req "^0.20.1") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2.0") (features (quote ("full"))) (default-features #t) (kind 0)) (crate-dep (name "version_check") (req "^0.9") (default-features #t) (kind 1)))) (hash "1bfwxr9zzkh11w9b5nw5yzrmm1f2d0ay98gagr8a4bgiy8kiyxnd")))

