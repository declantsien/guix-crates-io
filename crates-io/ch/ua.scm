(define-module (crates-io ch ua) #:use-module (crates-io))

(define-public crate-chua-0.1 (crate (name "chua") (vers "0.1.0") (deps (list (crate-dep (name "futures") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.4") (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.10") (features (quote ("json" "rustls-tls"))) (kind 0)) (crate-dep (name "structopt") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^0.2") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "0rrsp5x7kzsjpfh4ndc6jbrxy50zslga4adj51za3d4dfngi7dgd")))

