(define-module (crates-io ch wd) #:use-module (crates-io))

(define-public crate-chwd-0.1 (crate (name "chwd") (vers "0.1.0") (hash "03dhgbz5gls2k9irhjrhqyn1ib3sv2xs7fbw1ga7nz744vcf318p")))

(define-public crate-chwd-0.2 (crate (name "chwd") (vers "0.2.0") (hash "0bh0qgprqi852s5p60cb84bqfw6l5xh4jq1hwd1bla9079694jpx")))

