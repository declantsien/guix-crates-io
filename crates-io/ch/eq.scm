(define-module (crates-io ch eq) #:use-module (crates-io))

(define-public crate-cheque-0.1 (crate (name "cheque") (vers "0.1.0") (deps (list (crate-dep (name "num-traits") (req "0.1.*") (default-features #t) (kind 0)))) (hash "1d4cw7r64wp269z2zamdwgrrrlg2pnp3dy57vv7s5czj3fagaqpd")))

(define-public crate-cheque-0.2 (crate (name "cheque") (vers "0.2.0") (deps (list (crate-dep (name "num-traits") (req "0.1.*") (default-features #t) (kind 0)))) (hash "1qnqb4zg8hy7bsxclvjn3y5gvlb3gvizsxy3x950jxj16agv5wf5")))

(define-public crate-cheque-0.3 (crate (name "cheque") (vers "0.3.0") (deps (list (crate-dep (name "num-traits") (req "0.2.*") (default-features #t) (kind 0)))) (hash "0daiicbkbc2qwq9633d2ya9vrldsb9mci0vv3pb95162z7qwjfsg")))

