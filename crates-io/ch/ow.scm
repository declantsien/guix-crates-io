(define-module (crates-io ch ow) #:use-module (crates-io))

(define-public crate-chownr-0.1 (crate (name "chownr") (vers "0.1.0") (deps (list (crate-dep (name "error-chain") (req "^0.12.1") (default-features #t) (kind 0)) (crate-dep (name "nix") (req "^0.14.0") (default-features #t) (kind 0)))) (hash "0whpnm93har7fw4183gl4fmfkkx1mjg8ji3xxmbx165xhv01qqlg")))

(define-public crate-chownr-1 (crate (name "chownr") (vers "1.0.0") (deps (list (crate-dep (name "error-chain") (req "^0.12.1") (default-features #t) (kind 0)) (crate-dep (name "failure") (req "^0.1.5") (default-features #t) (kind 0)) (crate-dep (name "nix") (req "^0.14.0") (default-features #t) (kind 0)))) (hash "17gl42939sxgnslyc1ar37i1xxbpmpk7p4bp5jqkyxkrjzvb87f8")))

(define-public crate-chownr-2 (crate (name "chownr") (vers "2.0.0") (deps (list (crate-dep (name "failure") (req "^0.1.5") (default-features #t) (kind 0)) (crate-dep (name "nix") (req "^0.14.0") (default-features #t) (kind 0)))) (hash "1j8x3vj2gxswa6d0p06k7gqgp8fmzsbbaaiabfrs5kh28x8yxb2k")))

(define-public crate-chownr-3 (crate (name "chownr") (vers "3.0.0") (deps (list (crate-dep (name "anyhow") (req "^1.0.16") (default-features #t) (kind 0)) (crate-dep (name "nix") (req "^0.14.0") (default-features #t) (kind 0)))) (hash "1jqc1w71wf79fh2hvvjc7pip2bva285hcl8yq7j2x5bqbpb5r44p")))

