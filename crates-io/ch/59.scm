(define-module (crates-io ch #{59}#) #:use-module (crates-io))

(define-public crate-ch59x-0.1 (crate (name "ch59x") (vers "0.1.7") (deps (list (crate-dep (name "critical-section") (req "^1.1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "riscv") (req "^0.10.1") (default-features #t) (kind 0)) (crate-dep (name "vcell") (req "^0.1") (default-features #t) (kind 0)))) (hash "0ffyllkdqhm2cwwiv2za9jqg8vc6mvkpl3fy2yvf6i9jx8imypig") (features (quote (("rt") ("default") ("ch59x"))))))

(define-public crate-ch59x-0.1 (crate (name "ch59x") (vers "0.1.8") (deps (list (crate-dep (name "critical-section") (req "^1.1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "riscv") (req "^0.10.1") (default-features #t) (kind 0)) (crate-dep (name "vcell") (req "^0.1") (default-features #t) (kind 0)))) (hash "1xbcpk070637gcfvyzradmrh89313pkzzyqn4iai75kpzgmi6rz3") (features (quote (("rt") ("default") ("ch59x"))))))

(define-public crate-ch59x-hal-0.0.0 (crate (name "ch59x-hal") (vers "0.0.0") (hash "1k7gcpa5lgld92ydl55m72j0vr2x1brls3gg9hy4xz6d5jvbdi7a")))

