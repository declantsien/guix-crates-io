(define-module (crates-io ch #{14}#) #:use-module (crates-io))

(define-public crate-ch14-0.1 (crate (name "ch14") (vers "0.1.0") (hash "1hbv94lqbyxnlb3dp2662s1qsnakxyxiyc99mrxk19y1akw8mlp8") (yanked #t)))

(define-public crate-ch14-2-publishing-crate-0.1 (crate (name "ch14-2-publishing-crate") (vers "0.1.0") (hash "0h1y6gzkgc2bwilsknfv67710qdlq953zjcpvaka1cb58ym01281") (yanked #t)))

(define-public crate-ch14-cargo-0.1 (crate (name "ch14-cargo") (vers "0.1.0") (hash "116nq49zsy5pa8bra323hqm7yvyda1w84f4g8vfsfssdzz018als") (yanked #t)))

(define-public crate-ch14_2_test_publish-0.1 (crate (name "ch14_2_test_publish") (vers "0.1.1") (hash "0vcgcl5gaszf21llsi55r3x9hrzxlxni645mbxmigqi19w9i44af")))

(define-public crate-ch14_2_test_publish-0.1 (crate (name "ch14_2_test_publish") (vers "0.1.2") (hash "137w1bry8l6sgglcdr1ws3cf0l62g8j5wfysxwrhj8r6s8m1jyns")))

(define-public crate-ch14_cargo_and_crates-0.1 (crate (name "ch14_cargo_and_crates") (vers "0.1.0") (hash "0b0lgf07r0l8iw33j8ns5884m67xhhkl624pjv6p7fwbzafns5x4")))

(define-public crate-ch14_cartes-0.1 (crate (name "ch14_cartes") (vers "0.1.0") (hash "1m7fw0lpqbd88fb4dlmc7kvzb0c2k9c46k8my2d0zkcbcn0qg72c") (yanked #t)))

(define-public crate-ch14_guessing_game_yeomiji-0.1 (crate (name "ch14_guessing_game_yeomiji") (vers "0.1.0") (hash "04sypxp6mjjc8l8m5fcp0h1rr39bvn4b7ss1grijnh65p9ipldnk")))

(define-public crate-ch14test-0.1 (crate (name "ch14test") (vers "0.1.0") (hash "10rhpfgbd5jz5s7pakl0s48ds6n5k9l4mwjhisnkfdyd00yicq0y") (yanked #t)))

(define-public crate-ch14test173-0.1 (crate (name "ch14test173") (vers "0.1.0") (hash "0p0sw5as85w81i3f5az3a9jql0qz24dg9vm68dxwgcdxhrdxllnd")))

(define-public crate-ch14test173-0.1 (crate (name "ch14test173") (vers "0.1.1") (hash "18gqzryhra1wkjmp452v145p5llp5hcisxzb7qsj4i1kal3yvnqd")))

