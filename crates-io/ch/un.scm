(define-module (crates-io ch un) #:use-module (crates-io))

(define-public crate-chunin-0.0.1 (crate (name "chunin") (vers "0.0.1") (hash "06ljjglvaam2kv6r1yb0kc3mcwgshk6acn3mgdy1ns8jy4x5p87n")))

(define-public crate-chunk-0.1 (crate (name "chunk") (vers "0.1.0") (deps (list (crate-dep (name "rayon") (req "^1.1") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.2") (default-features #t) (kind 0)))) (hash "1vaiv8p480fc6dna623r80mmw1822hdfs1zm3pq4c82sn32qpg7g")))

(define-public crate-chunk-json-lite-0.1 (crate (name "chunk-json-lite") (vers "0.1.0") (deps (list (crate-dep (name "count-write") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "human-size") (req "^0.4.0") (default-features #t) (kind 0)) (crate-dep (name "json") (req "^0.12.0") (default-features #t) (kind 0)) (crate-dep (name "main_error") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "0pzrkbjvnj2b373xflc1f3kbv3wdh4naqqzdpr7q1dk83fmwjgzi")))

(define-public crate-chunk_iter-0.1 (crate (name "chunk_iter") (vers "0.1.0") (hash "0whp72wdlfk42sxdsirl41pz2nhh45z9qxmgyc8s0ifz0zhayrdz") (yanked #t)))

(define-public crate-chunk_iter-0.1 (crate (name "chunk_iter") (vers "0.1.1") (hash "0yqw5ayfqb17sjd19zm42v80n42cw6k27ygjzg7l2gp3wiz7pmcz")))

(define-public crate-chunk_iter-0.1 (crate (name "chunk_iter") (vers "0.1.2") (deps (list (crate-dep (name "testdrop") (req "^0.1.2") (default-features #t) (kind 2)))) (hash "08nqmb1visgr949kbkqm47390i7673sndd4ymg62y5k1y5952bna")))

(define-public crate-chunk_iter-0.1 (crate (name "chunk_iter") (vers "0.1.3") (deps (list (crate-dep (name "testdrop") (req "^0.1.2") (default-features #t) (kind 2)))) (hash "00hfdzn9qxx51kncfqdz3vnivcyrj888vwi8lir8m4hlfi8cbvvw")))

(define-public crate-chunk_iter-0.1 (crate (name "chunk_iter") (vers "0.1.4") (deps (list (crate-dep (name "testdrop") (req "^0.1.2") (default-features #t) (kind 2)))) (hash "0g4rds0wwflvghz4389ianksgc0f6ddcir3fqsv2qza89g0ck14n")))

(define-public crate-chunk_iter-0.1 (crate (name "chunk_iter") (vers "0.1.5") (deps (list (crate-dep (name "testdrop") (req "^0.1.2") (default-features #t) (kind 2)))) (hash "16jv8glz70r2fhbqj0a1pc8lmybm7znam3ykmfvn5sj33h0m28q7")))

(define-public crate-chunk_store-0.1 (crate (name "chunk_store") (vers "0.1.0") (deps (list (crate-dep (name "log") (req "~0.3.4") (default-features #t) (kind 0)) (crate-dep (name "maidsafe_utilities") (req "~0.1.5") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "~0.3.13") (default-features #t) (kind 2)) (crate-dep (name "rustc-serialize") (req "~0.3.16") (default-features #t) (kind 0)) (crate-dep (name "tempdir") (req "~0.3.4") (default-features #t) (kind 0)) (crate-dep (name "xor_name") (req "~0.0.1") (default-features #t) (kind 0)))) (hash "1rz09xkwmdws8mcqwj5jbw90pkm7l6xhkm8c4c8ihh2nqc7078gh")))

(define-public crate-chunk_store-0.2 (crate (name "chunk_store") (vers "0.2.0") (deps (list (crate-dep (name "clippy") (req "~0.0.37") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "log") (req "~0.3.5") (default-features #t) (kind 0)) (crate-dep (name "maidsafe_utilities") (req "~0.1.5") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "~0.3.13") (default-features #t) (kind 2)) (crate-dep (name "rustc-serialize") (req "~0.3.18") (default-features #t) (kind 0)) (crate-dep (name "tempdir") (req "~0.3.4") (default-features #t) (kind 0)) (crate-dep (name "xor_name") (req "~0.0.3") (default-features #t) (kind 0)))) (hash "0ma87cwkwida5y0pk3qkbv8dlsn07nwimrxinqjskl74a8wqa69a")))

(define-public crate-chunk_store-0.3 (crate (name "chunk_store") (vers "0.3.0") (deps (list (crate-dep (name "clippy") (req "~0.0.45") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "log") (req "~0.3.5") (default-features #t) (kind 0)) (crate-dep (name "maidsafe_utilities") (req "~0.4.0") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "~0.3.14") (default-features #t) (kind 2)) (crate-dep (name "rustc-serialize") (req "~0.3.18") (default-features #t) (kind 0)) (crate-dep (name "tempdir") (req "~0.3.4") (default-features #t) (kind 0)) (crate-dep (name "xor_name") (req "~0.1.0") (default-features #t) (kind 0)))) (hash "14szbdr7c36jnl7k861a174yl5ymf2nm2x8mnwb637fz2dv0x6nk")))

(define-public crate-chunk_store-0.4 (crate (name "chunk_store") (vers "0.4.0") (deps (list (crate-dep (name "clippy") (req "~0.0.63") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "maidsafe_utilities") (req "~0.5.1") (default-features #t) (kind 0)) (crate-dep (name "quick-error") (req "^1.0.0") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "~0.3.14") (default-features #t) (kind 2)) (crate-dep (name "rustc-serialize") (req "~0.3.19") (default-features #t) (kind 0)) (crate-dep (name "tempdir") (req "~0.3.4") (default-features #t) (kind 0)))) (hash "0c5ywhs2qgym06wyakb4qhc8r9qnnkr3jksh19013mlzqzvvlxfj")))

(define-public crate-chunk_store-0.4 (crate (name "chunk_store") (vers "0.4.1") (deps (list (crate-dep (name "clippy") (req "~0.0.76") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "maidsafe_utilities") (req "~0.6.0") (default-features #t) (kind 0)) (crate-dep (name "quick-error") (req "^1.0.0") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "~0.3.14") (default-features #t) (kind 2)) (crate-dep (name "rustc-serialize") (req "~0.3.19") (default-features #t) (kind 0)) (crate-dep (name "tempdir") (req "~0.3.4") (default-features #t) (kind 0)))) (hash "1n72xkck97iwm8xax2nh3s1hffb6787ln2niwxigpsj3jd1m9xjk")))

(define-public crate-chunked-0.1 (crate (name "chunked") (vers "0.1.0") (deps (list (crate-dep (name "async-trait") (req "^0.1.50") (default-features #t) (kind 0)) (crate-dep (name "bit-vec") (req "^0.6.3") (default-features #t) (kind 0)) (crate-dep (name "crossbeam-queue") (req "^0.3.1") (default-features #t) (kind 0)) (crate-dep (name "futures") (req "^0.3.14") (default-features #t) (kind 0)) (crate-dep (name "once_cell") (req "^1.7.2") (default-features #t) (kind 0)) (crate-dep (name "rayon") (req "^1.5.0") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.5.0") (features (quote ("rt"))) (default-features #t) (kind 0)))) (hash "14js5ayah9bai0hikk78kpzxahg7jq7pxprjnd98kak61yjvsplf")))

(define-public crate-chunked-0.1 (crate (name "chunked") (vers "0.1.1") (deps (list (crate-dep (name "async-trait") (req "^0.1.50") (default-features #t) (kind 0)) (crate-dep (name "bit-vec") (req "^0.6.3") (default-features #t) (kind 0)) (crate-dep (name "crossbeam-queue") (req "^0.3.1") (default-features #t) (kind 0)) (crate-dep (name "futures") (req "^0.3.14") (default-features #t) (kind 0)) (crate-dep (name "once_cell") (req "^1.7.2") (default-features #t) (kind 0)) (crate-dep (name "rayon") (req "^1.5.0") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.5.0") (features (quote ("rt"))) (default-features #t) (kind 0)))) (hash "02gapbkhgxs593ncibd0zpb6xjcib55b5b3a3kjjkq5jplq6kyji")))

(define-public crate-chunked-buffer-0.1 (crate (name "chunked-buffer") (vers "0.1.0") (hash "0f8iwr41539k5vw31wc6rw15d6fs44rglyvhymqz99pk5yd0jzbs") (rust-version "1.60.0")))

(define-public crate-chunked-buffer-0.1 (crate (name "chunked-buffer") (vers "0.1.1") (hash "1322cjsyzd1ik9gassxiyrndf7mzym9wn8rjqymd6x5d4vyvjx43") (rust-version "1.60.0")))

(define-public crate-chunked-buffer-0.1 (crate (name "chunked-buffer") (vers "0.1.2") (hash "093rwi031aql19vfbxcmhj0nqq3268parxm9wk0qh73s0pl1rs1m") (yanked #t) (rust-version "1.60.0")))

(define-public crate-chunked-buffer-0.1 (crate (name "chunked-buffer") (vers "0.1.3") (hash "1cc2hwwn68lmxdqp0gbyhrwdhabarqac0k962fvh3ficypv5bpmp") (rust-version "1.60.0")))

(define-public crate-chunked-buffer-0.2 (crate (name "chunked-buffer") (vers "0.2.0") (hash "129nwl3h87d3aq390dy90m3sknyh8h9lgbvnnwkb0pl32k9jnqps") (rust-version "1.60.0")))

(define-public crate-chunked-bytes-0.1 (crate (name "chunked-bytes") (vers "0.1.0-beta.1") (deps (list (crate-dep (name "bytes") (req "^0.5") (default-features #t) (kind 0)) (crate-dep (name "futures") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "pin-project") (req "^0.4") (default-features #t) (kind 2)) (crate-dep (name "tokio") (req "^0.2") (features (quote ("io-util" "rt-threaded" "macros"))) (default-features #t) (kind 2)))) (hash "0wvwjgqd6xa3g2dsvj6rkln1dfhqag5sa7sbg8cv6wzcv757gpjc")))

(define-public crate-chunked-bytes-0.1 (crate (name "chunked-bytes") (vers "0.1.0-beta.2") (deps (list (crate-dep (name "bytes") (req "^0.5") (default-features #t) (kind 0)) (crate-dep (name "futures") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "pin-project") (req "^0.4") (default-features #t) (kind 2)) (crate-dep (name "tokio") (req "^0.2") (features (quote ("io-util" "rt-threaded" "tcp" "macros"))) (default-features #t) (kind 2)))) (hash "1l028gmjm3w41pgzr48wzyklfakdjk6dbih9idb249ycd5b2qxhk")))

(define-public crate-chunked-bytes-0.1 (crate (name "chunked-bytes") (vers "0.1.0-beta.3") (deps (list (crate-dep (name "bytes") (req "^0.5") (default-features #t) (kind 0)) (crate-dep (name "futures") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "pin-project") (req "^0.4") (default-features #t) (kind 2)) (crate-dep (name "tokio") (req "^0.2") (features (quote ("io-util" "rt-threaded" "tcp" "macros"))) (default-features #t) (kind 2)))) (hash "0j2v19lw1i0cg9b51mkg8skbj1jivn09yga679xxw9c382cvzwnl")))

(define-public crate-chunked-bytes-0.1 (crate (name "chunked-bytes") (vers "0.1.0-beta.4") (deps (list (crate-dep (name "bytes") (req "^0.5") (default-features #t) (kind 0)) (crate-dep (name "futures") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "pin-project") (req "^0.4") (default-features #t) (kind 2)) (crate-dep (name "tokio") (req "^0.2") (features (quote ("io-util" "rt-threaded" "tcp" "macros"))) (default-features #t) (kind 2)))) (hash "17vibs9ijdz0p6icpv4j3ixbc1lp3qn8i9c9kaqr035m8svqwnhr")))

(define-public crate-chunked-bytes-0.1 (crate (name "chunked-bytes") (vers "0.1.0") (deps (list (crate-dep (name "bytes") (req "^0.5") (default-features #t) (kind 0)) (crate-dep (name "futures") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "generic-tests") (req "^0.1.0") (default-features #t) (kind 2)) (crate-dep (name "pin-project") (req "^0.4") (default-features #t) (kind 2)) (crate-dep (name "tokio") (req "^0.2") (features (quote ("io-util" "rt-threaded" "tcp" "macros"))) (default-features #t) (kind 2)))) (hash "17iv1x78d02qqc3k5np45prksl10acnisgdvn98j7plw3vj5xq92")))

(define-public crate-chunked-bytes-0.2 (crate (name "chunked-bytes") (vers "0.2.0") (deps (list (crate-dep (name "bytes") (req "^0.6") (default-features #t) (kind 0)) (crate-dep (name "futures") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "generic-tests") (req "^0.1.1") (default-features #t) (kind 2)) (crate-dep (name "pin-project") (req "^1.0") (default-features #t) (kind 2)) (crate-dep (name "tokio") (req "^0.3") (features (quote ("rt-multi-thread" "macros"))) (default-features #t) (kind 2)))) (hash "1gdy7x7my74azdmdji4345prjhkyf2s10xr1i08cxqf82x2k2hn8")))

(define-public crate-chunked-bytes-0.3 (crate (name "chunked-bytes") (vers "0.3.0") (deps (list (crate-dep (name "bytes") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "futures") (req "^0.3") (features (quote ("std"))) (kind 2)) (crate-dep (name "generic-tests") (req "^0.1.1") (default-features #t) (kind 2)) (crate-dep (name "pin-project") (req "^1.0") (default-features #t) (kind 2)) (crate-dep (name "tokio") (req "^1.1") (features (quote ("rt-multi-thread" "macros"))) (default-features #t) (kind 2)))) (hash "04klic06prfzy3j7x0b6cbs8gmq0i21c2458v1k6i3hz16979knz")))

(define-public crate-chunked-transfer-coding-0.1 (crate (name "chunked-transfer-coding") (vers "0.1.0") (hash "0p5b7ik30z90ia1x5hryv9jjxkcqhxxyd20k11i1cy6xlxs3jz5g") (yanked #t)))

(define-public crate-chunked_transfer-0.2 (crate (name "chunked_transfer") (vers "0.2.0") (hash "15yq75280f7mnlxb983jns4mn301c26yhk5wxyzfyw01z66r8dng")))

(define-public crate-chunked_transfer-0.3 (crate (name "chunked_transfer") (vers "0.3.0") (hash "12610sxinc6xmg9z7zbkssj81xiy72pvdm37phqji5fmimskjs65")))

(define-public crate-chunked_transfer-0.3 (crate (name "chunked_transfer") (vers "0.3.1") (hash "11yghnd24w0i9p8g368c3pg7qh9nfz7kgri6pywja9pnmakj13a9")))

(define-public crate-chunked_transfer-1 (crate (name "chunked_transfer") (vers "1.0.0") (hash "0v0xlp5kqwwyrd71ssy6l4jnm7f7cd4h3immsx5s226yaijyp2zr")))

(define-public crate-chunked_transfer-1 (crate (name "chunked_transfer") (vers "1.1.0") (hash "1pddhllvjarnl2wxlqa6yszi6kiyhjr9j9v2rcwcix5r15zn92av")))

(define-public crate-chunked_transfer-1 (crate (name "chunked_transfer") (vers "1.2.0") (hash "14vi1bj4bmilnjwp39vgvdxp3cs8p9yr93ysf4gkg0i72cayna8x")))

(define-public crate-chunked_transfer-1 (crate (name "chunked_transfer") (vers "1.3.0") (deps (list (crate-dep (name "criterion") (req "^0.3") (default-features #t) (kind 2)))) (hash "1jl2ms4ylf6hlaylkjhwp3y4rd996z6qpkzkgcb5gzm88mfhcxvl")))

(define-public crate-chunked_transfer-1 (crate (name "chunked_transfer") (vers "1.4.0") (deps (list (crate-dep (name "criterion") (req "^0.3") (default-features #t) (kind 2)))) (hash "0bkdlsrszfcscw3j6yhs7kj6jbp8id47jjk6h9k58px47na5gy7z")))

(define-public crate-chunked_transfer-1 (crate (name "chunked_transfer") (vers "1.4.1") (deps (list (crate-dep (name "criterion") (req "^0.3") (default-features #t) (kind 2)))) (hash "06kx4083avirgifmj192qy8hkyqcqkq60gxg91r4vq36hqw9396c")))

(define-public crate-chunked_transfer-1 (crate (name "chunked_transfer") (vers "1.5.0") (deps (list (crate-dep (name "criterion") (req "^0.3") (default-features #t) (kind 2)))) (hash "00a9h3csr1xwkqrzpz5kag4h92zdkrnxq4ppxidrhrx29syf6kbf")))

(define-public crate-chunked_transfer_cli-0.1 (crate (name "chunked_transfer_cli") (vers "0.1.0") (deps (list (crate-dep (name "assert_cmd") (req "^2.0.4") (default-features #t) (kind 0)) (crate-dep (name "chunked_transfer") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^3.2.16") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "0sgzaq2vlrxybwc24nhixk8jj1y3yvi6imx1lbbxgc8rfblyxsyj")))

(define-public crate-chunked_transfer_cli-0.1 (crate (name "chunked_transfer_cli") (vers "0.1.1") (deps (list (crate-dep (name "assert_cmd") (req "^2.0.4") (default-features #t) (kind 0)) (crate-dep (name "chunked_transfer") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^3.2.16") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "15f1i2i063bmll1vc9bzc8kv4vnzsv7va4c3ml17dxjmk10f0rxp") (rust-version "1.65")))

(define-public crate-chunked_transfer_encoding-0.0.0 (crate (name "chunked_transfer_encoding") (vers "0.0.0") (deps (list (crate-dep (name "http") (req "^0.2") (optional #t) (kind 0)))) (hash "0r49qh4hz9572bkady0k8y90557iay3znlmmjwwrv5aps2rjfha4") (yanked #t)))

(define-public crate-chunked_transfer_encoding-0.1 (crate (name "chunked_transfer_encoding") (vers "0.1.0") (deps (list (crate-dep (name "futures-executor") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "futures-util") (req "^0.3") (optional #t) (kind 0)) (crate-dep (name "http") (req "^0.2") (optional #t) (kind 0)) (crate-dep (name "pin-project-lite") (req "^0.2") (optional #t) (default-features #t) (kind 0)))) (hash "0vr2m257v3g013rkd74x250r4kk7zzl1cggnqgh9bd8frsnc0y3b") (features (quote (("stream" "futures-util" "pin-project-lite") ("default" "stream") ("_priv_test_http" "http"))))))

(define-public crate-chunker-0.1 (crate (name "chunker") (vers "0.1.0") (deps (list (crate-dep (name "num_cpus") (req "^1.15.0") (default-features #t) (kind 0)) (crate-dep (name "progression") (req "^0.1.9") (optional #t) (default-features #t) (kind 0)))) (hash "1clqzdyf0dvxlsfn6pdyvrhiyqhrwmrabf0a8y21kxws31fc17mx") (features (quote (("default" "progression")))) (yanked #t)))

(define-public crate-chunker-0.1 (crate (name "chunker") (vers "0.1.1") (deps (list (crate-dep (name "num_cpus") (req "^1.15.0") (default-features #t) (kind 0)) (crate-dep (name "progression") (req "^0.1.9") (optional #t) (default-features #t) (kind 0)))) (hash "18ii35j6ac3yrfcljkm9na1z08z8z2igwwqpvdg1626np5kqimvs") (features (quote (("default" "progression")))) (yanked #t)))

(define-public crate-chunker-0.1 (crate (name "chunker") (vers "0.1.2") (deps (list (crate-dep (name "num_cpus") (req "^1.15.0") (default-features #t) (kind 0)) (crate-dep (name "progression") (req "^0.1.9") (optional #t) (default-features #t) (kind 0)))) (hash "1jpkx9v89nl3anmxhr1314495xia0w834p9ni12mn7ar65a18n0j") (features (quote (("default" "progression")))) (yanked #t)))

(define-public crate-chunker-0.1 (crate (name "chunker") (vers "0.1.3") (deps (list (crate-dep (name "num_cpus") (req "^1.15.0") (default-features #t) (kind 0)) (crate-dep (name "progression") (req "^0.1.9") (optional #t) (default-features #t) (kind 0)))) (hash "0msxqprzgic7qqvmb8bjfwq82jmaxqssynp0qbavqmgw1fabl52z") (features (quote (("default" "progression")))) (yanked #t)))

(define-public crate-chunker-0.1 (crate (name "chunker") (vers "0.1.4") (deps (list (crate-dep (name "num_cpus") (req "^1.15.0") (default-features #t) (kind 0)) (crate-dep (name "progression") (req "^0.1.11") (optional #t) (default-features #t) (kind 0)))) (hash "18nlks85x73f13bp9fb2vjbq094j9fbrlfjpdqkh3v0qm9gw3ld5") (features (quote (("default" "progression")))) (yanked #t)))

(define-public crate-chunker-0.1 (crate (name "chunker") (vers "0.1.5") (deps (list (crate-dep (name "num_cpus") (req "^1.15.0") (default-features #t) (kind 0)) (crate-dep (name "progression") (req "^0.1.11") (optional #t) (default-features #t) (kind 0)))) (hash "0dv7m5936j5v5v5703z7v3w308l2i8sm6cw25pp7zg27bxz88ilh") (features (quote (("default" "progression"))))))

(define-public crate-chunkie-0.0.0 (crate (name "chunkie") (vers "0.0.0") (hash "0wr45id8n32rmx47mhq5xzl8vwvgcjlna9mh4m60vqimy9rvf1f2")))

(define-public crate-chunkio-0.0.1 (crate (name "chunkio") (vers "0.0.1") (deps (list (crate-dep (name "futures") (req "^0.3") (kind 0)) (crate-dep (name "thiserror") (req "^1.0") (kind 0)) (crate-dep (name "tokio") (req "^1.37") (kind 0)) (crate-dep (name "tokio-util") (req "^0.7") (features (quote ("codec"))) (kind 0)))) (hash "1rp3b0s8g8g0y8i1xa2ckh504918ii6s5abhfgrbykhkbv3xsqks")))

(define-public crate-chunkiu-0.1 (crate (name "chunkiu") (vers "0.1.0") (hash "1g9ckdfk25ffk59vmk20arl8lgb5wilvcwi69d1dzs6v841blbyv") (yanked #t)))

(define-public crate-chunkiu-0.0.1 (crate (name "chunkiu") (vers "0.0.1") (hash "0ksfs9wranwgvxkmpb0bxdd0cmj5w8vyj6nwddwhdxqxsdvdh8ij") (yanked #t)))

(define-public crate-chunknd-0.0.1 (crate (name "chunknd") (vers "0.0.1-placeholder") (hash "130vmr81ni1zw53q7nvzmxj5vvsdwb19d61x0xjmidqrcx581bdr")))

(define-public crate-chunkr-0.1 (crate (name "chunkr") (vers "0.1.10") (deps (list (crate-dep (name "lopdf") (req "^0.32.0") (features (quote ("pom" "pom_parser"))) (default-features #t) (kind 0)))) (hash "0lsy6038azb2dnmrb6acdwwhwkx4v2bjs8jpqh2jyss57f7n0z0r")))

(define-public crate-chunkr-0.1 (crate (name "chunkr") (vers "0.1.15") (deps (list (crate-dep (name "lopdf") (req "^0.32.0") (features (quote ("pom" "pom_parser"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.202") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.117") (default-features #t) (kind 0)))) (hash "19xvk1div9vc9k8040jirryannzk7kscli4d6py39xanv8295vhl")))

(define-public crate-chunkr-0.1 (crate (name "chunkr") (vers "0.1.17") (deps (list (crate-dep (name "lopdf") (req "^0.32.0") (features (quote ("pom" "pom_parser"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.202") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.117") (default-features #t) (kind 0)))) (hash "0jk34rm45n01sn75h4zcjz18zrgkg8shs349w349pah6p0c0cp0h")))

(define-public crate-chunks-0.1 (crate (name "chunks") (vers "0.1.0") (deps (list (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)))) (hash "1ij9w91h26nsiphfjax1r2f7m34lbai8w8qicm1gwr1z9nghmym2")))

(define-public crate-chunks-0.1 (crate (name "chunks") (vers "0.1.1") (deps (list (crate-dep (name "crossbeam-channel") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)))) (hash "1qbmk9c3jjcy02jwl9q4y822n5m73n1ggip6pypwgik7vzhaiz8w")))

(define-public crate-chunks-exacter-0.1 (crate (name "chunks-exacter") (vers "0.1.0") (hash "0fjvmqack4xk7qcg7idpyk84wr632xi5zhzcs16y0pk238q76ma5")))

(define-public crate-chunks-exacter-0.1 (crate (name "chunks-exacter") (vers "0.1.1") (hash "05ayc27ws6m5nzqf9w1gmpk1wh5wa6zys3dbn67rh31v98c1snl7")))

(define-public crate-chunky-0.1 (crate (name "chunky") (vers "0.1.0") (deps (list (crate-dep (name "simple_allocator_trait") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "1gzqiv0mcswvjxw2plm9blq2dwkr0ilkvsi51l47hgzk4ka0ycm0")))

(define-public crate-chunky-0.1 (crate (name "chunky") (vers "0.1.1") (deps (list (crate-dep (name "simple_allocator_trait") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "08y7fdhcfam3p186mxf80nr17i37kfs4l5sj43z11z9h0fi7a5pn")))

(define-public crate-chunky-0.1 (crate (name "chunky") (vers "0.1.2") (deps (list (crate-dep (name "simple_allocator_trait") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "1liy1y7g6mg07fjgcmqhd9y2pdmc6hj16pn9mjki5phmcvmbhpzb")))

(define-public crate-chunky-0.1 (crate (name "chunky") (vers "0.1.3") (deps (list (crate-dep (name "simple_allocator_trait") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "15bl1my7cb82y2bbgwd2mvi93n69yp8yjwrha8lbmxscl656nn7p")))

(define-public crate-chunky-0.1 (crate (name "chunky") (vers "0.1.4") (deps (list (crate-dep (name "simple_allocator_trait") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "01rvznkswing0qpjrpgfybixxkqw1n76gzzvqmv14nw9hakwf73z")))

(define-public crate-chunky-0.2 (crate (name "chunky") (vers "0.2.0") (deps (list (crate-dep (name "memmap") (req "^0.7.0") (default-features #t) (kind 0)))) (hash "0nlwkbs0nycbvwfqxwkqw5lxgdnml0iydijrmnbxynm653mi0kb2")))

(define-public crate-chunky-0.3 (crate (name "chunky") (vers "0.3.0") (deps (list (crate-dep (name "memmap") (req "^0.7.0") (default-features #t) (kind 0)))) (hash "1s5fa6crvqxl3r1dvy9fmxblb3lfi6r64bkvz8wxb0klrn9cwlgn")))

(define-public crate-chunky-0.3 (crate (name "chunky") (vers "0.3.1") (deps (list (crate-dep (name "memmap") (req "^0.7.0") (optional #t) (default-features #t) (kind 0)))) (hash "1175zb7b0n73fxrr4c3lyrma5vyzvi224n708nk9dx5sbf5sq6a0") (features (quote (("mmap" "memmap"))))))

(define-public crate-chunky-0.3 (crate (name "chunky") (vers "0.3.2") (deps (list (crate-dep (name "memmap") (req "^0.7.0") (optional #t) (default-features #t) (kind 0)))) (hash "1r6gqlz19vvnlwkl5qxphwy2y1c6p59brwbawsc992ry8qcz1sx7") (features (quote (("mmap" "memmap"))))))

(define-public crate-chunky-0.3 (crate (name "chunky") (vers "0.3.3") (deps (list (crate-dep (name "memmap") (req "^0.7.0") (optional #t) (default-features #t) (kind 0)))) (hash "1rbn8vprz9rx98m41g5v86dgyyax206vhlrhnnmj6rngn2zrfpmm") (features (quote (("mmap" "memmap"))))))

(define-public crate-chunky-0.3 (crate (name "chunky") (vers "0.3.4") (deps (list (crate-dep (name "memmap") (req "^0.7.0") (optional #t) (default-features #t) (kind 0)))) (hash "16v8rp59s2cy17zbq8bz910r22jzsvbwhgsjzr97v4c3kxzqa045") (features (quote (("mmap" "memmap"))))))

(define-public crate-chunky-0.3 (crate (name "chunky") (vers "0.3.5") (deps (list (crate-dep (name "memmap") (req "^0.7.0") (optional #t) (default-features #t) (kind 0)))) (hash "127sprdcbvjdw9invx0s2q3rsn235shz3qszqn2bqb8b847ajlj1") (features (quote (("mmap" "memmap"))))))

(define-public crate-chunky-0.3 (crate (name "chunky") (vers "0.3.6") (deps (list (crate-dep (name "memmap") (req "^0.7.0") (optional #t) (default-features #t) (kind 0)))) (hash "0wbb5qj3xvgs34d69wlbjwm9awfya3slf25kjj4xdkmvab83d9hd") (features (quote (("mmap" "memmap"))))))

(define-public crate-chunky-0.3 (crate (name "chunky") (vers "0.3.7") (deps (list (crate-dep (name "memmap") (req "^0.7.0") (optional #t) (default-features #t) (kind 0)))) (hash "0m1jp8q1ay79kyah5jq0h62kcd2dy39w9a0lykvr0vqq7rbshaif") (features (quote (("mmap" "memmap"))))))

(define-public crate-chunky-bits-0.1 (crate (name "chunky-bits") (vers "0.1.0") (deps (list (crate-dep (name "async-trait") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "bytes") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "futures") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "hex") (req "^0.4") (features (quote ("std" "serde"))) (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8") (features (quote ("serde"))) (default-features #t) (kind 0)) (crate-dep (name "reed-solomon-erasure") (req "^4.0") (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.11") (features (quote ("default-tls" "gzip" "brotli"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_yaml") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "sha2") (req "^0.9") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1") (features (quote ("fs" "io-std" "io-util" "macros" "process" "rt" "rt-multi-thread" "sync"))) (default-features #t) (kind 0)) (crate-dep (name "tokio-util") (req "^0.6") (default-features #t) (kind 0)) (crate-dep (name "url") (req "^2.2") (features (quote ("serde"))) (default-features #t) (kind 0)) (crate-dep (name "warp") (req "^0.3") (default-features #t) (kind 0)))) (hash "061ah18ybd9hc1iykrnmrp3xfgf5h1apzdzh7pgn71any4b8nv56")))

(define-public crate-chunky-bits-0.2 (crate (name "chunky-bits") (vers "0.2.0") (deps (list (crate-dep (name "async-trait") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "bytes") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "futures") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "hex") (req "^0.4") (features (quote ("std" "serde"))) (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.4") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8") (features (quote ("serde" "small_rng"))) (default-features #t) (kind 0)) (crate-dep (name "reed-solomon-erasure") (req "^4.0") (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.11") (features (quote ("default-tls" "stream" "gzip" "brotli"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive" "rc"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_yaml") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "sha2") (req "^0.9") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "tempfile") (req "^3.2") (default-features #t) (kind 2)) (crate-dep (name "tokio") (req "^1") (features (quote ("fs" "io-std" "io-util" "macros" "process" "rt" "rt-multi-thread" "sync" "signal"))) (default-features #t) (kind 0)) (crate-dep (name "tokio-stream") (req "^0.1") (features (quote ("fs"))) (default-features #t) (kind 0)) (crate-dep (name "tokio-util") (req "^0.6") (default-features #t) (kind 0)) (crate-dep (name "url") (req "^2.2") (features (quote ("serde"))) (default-features #t) (kind 0)) (crate-dep (name "warp") (req "^0.3") (default-features #t) (kind 0)))) (hash "1x5f7aqdknf8w8sy2mdb39ik7d3y9wcnpifn7mx2zcvyp10pgxx2")))

(define-public crate-chunky-vec-0.1 (crate (name "chunky-vec") (vers "0.1.0") (hash "05w0035b64qh2mxszg6jr2gn2lqwj85l70kv344qy0xfcjjdwyxv")))

