(define-module (crates-io ch ou) #:use-module (crates-io))

(define-public crate-chouten-0.1 (crate (name "chouten") (vers "0.1.0") (deps (list (crate-dep (name "reqwest") (req "^0.11") (features (quote ("json" "blocking"))) (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1") (features (quote ("full"))) (default-features #t) (kind 0)) (crate-dep (name "v8") (req "^0.92.0") (default-features #t) (kind 0)))) (hash "0vhfzk60386xdz3ncb180bjiig2b9w7lzwqiphb8nzp5xbb25ji4")))

