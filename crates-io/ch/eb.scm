(define-module (crates-io ch eb) #:use-module (crates-io))

(define-public crate-cheb_ineq-0.1 (crate (name "cheb_ineq") (vers "0.1.0") (hash "0rafdjh9wkwny82havsfflrsff7p9y76njnxgbd6zfpfal4gbsgi")))

(define-public crate-chebyshev-0.0.0 (crate (name "chebyshev") (vers "0.0.0") (hash "0zaa0dihvwzdw9jy4h7l5jdnn39nni794sx6270533pylch8rf3z")))

(define-public crate-chebyshev-0.0.1 (crate (name "chebyshev") (vers "0.0.1") (hash "12l1qzcyk42jxqkbzl0ivq4fvvkp9zpvshz4vr5nl32rkdj46jax")))

