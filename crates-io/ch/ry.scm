(define-module (crates-io ch ry) #:use-module (crates-io))

(define-public crate-chry_minigrep-0.1 (crate (name "chry_minigrep") (vers "0.1.0") (hash "12qn1r7qjpmlnfawrra94gvf9m7ad7i6y29xjl8snzqkvbfckcw2")))

(define-public crate-chrysanthemum-0.0.1 (crate (name "chrysanthemum") (vers "0.0.1") (deps (list (crate-dep (name "assert_matches") (req "^1") (default-features #t) (kind 2)) (crate-dep (name "fnv") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "pest") (req "^2") (default-features #t) (kind 0)) (crate-dep (name "pest_derive") (req "^2") (default-features #t) (kind 0)) (crate-dep (name "ron") (req "^0.5") (default-features #t) (kind 0)) (crate-dep (name "rspirv") (req "^0.5") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "spirv_headers") (req "^1.3") (default-features #t) (kind 0)))) (hash "014plg79azi5s0isd94qvvbrynxg1dlg6pd69cky00sna7jzlvkz")))

