(define-module (crates-io bs ab) #:use-module (crates-io))

(define-public crate-bsabin-0.1 (crate (name "bsabin") (vers "0.1.0") (deps (list (crate-dep (name "bsalib") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^3.0.0-beta.4") (default-features #t) (kind 0)) (crate-dep (name "glob") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.28") (default-features #t) (kind 0)))) (hash "1m220mnvjfyk81azmc1ljmy5dvrdi7nh2ssz62751d6hamvmmjz3")))

(define-public crate-bsabin-0.2 (crate (name "bsabin") (vers "0.2.0") (deps (list (crate-dep (name "bsa") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^3.0.0-beta.4") (default-features #t) (kind 0)) (crate-dep (name "glob") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.28") (default-features #t) (kind 0)))) (hash "14a08fh5mqc48hiiqwiajgnda0fjhxlb2rppxhq0r0sr9k7gz805")))

(define-public crate-bsabin-0.2 (crate (name "bsabin") (vers "0.2.1") (deps (list (crate-dep (name "bsa") (req "^0.2.1") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^3.0.0-beta.4") (default-features #t) (kind 0)) (crate-dep (name "glob") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.28") (default-features #t) (kind 0)))) (hash "02wrfh83vx7cgsk8c2ifkb9ypyj0r12cmpkk25pr6ai5710y98g5")))

