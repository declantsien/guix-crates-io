(define-module (crates-io bs or) #:use-module (crates-io))

(define-public crate-bsor-0.1 (crate (name "bsor") (vers "0.1.0") (deps (list (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 2)))) (hash "1wb3srdnd9ld1pmcwqzk2sc05nxyqb9d3y4q8d7f33rmahgfxhn2") (yanked #t)))

(define-public crate-bsor-0.1 (crate (name "bsor") (vers "0.1.1") (deps (list (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 2)))) (hash "1a8pk0jdr9mcx168hf6d52f5kzvq4qn7xppa4x6hm77x0l1avmjl")))

(define-public crate-bsor-0.2 (crate (name "bsor") (vers "0.2.0") (deps (list (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 2)))) (hash "1vdk7sgf323xrcm8hh4jwz7jnq993kgxazpibkjxiy6qs62md8gb")))

(define-public crate-bsor-0.2 (crate (name "bsor") (vers "0.2.1") (deps (list (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 2)))) (hash "05y25cm1i2vamzpd7yiyqkhbk4s69hhz1482h2g4y7l9y0ppfkb3")))

(define-public crate-bsor-0.3 (crate (name "bsor") (vers "0.3.0") (deps (list (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 2)))) (hash "0xw7qwlg9vvwpgimjks7www9fbfkb6zfsf2yk6lsdnbzp1333253")))

(define-public crate-bsort-0.1 (crate (name "bsort") (vers "0.1.0") (hash "1ddh7k12n8872dzcb5klwb1hwmxxryw2c7swn67772f0nqi5ys0j")))

