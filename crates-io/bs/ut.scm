(define-module (crates-io bs ut) #:use-module (crates-io))

(define-public crate-bsutils-0.1 (crate (name "bsutils") (vers "0.1.0") (hash "1ifjrz8ck6kdbf4lffyk3qhyskm12abdsg61nb2pszv0psbbc93q")))

(define-public crate-bsutils-0.1 (crate (name "bsutils") (vers "0.1.1") (hash "1wkav98xb1cfmqikjbigqxhxlxx5r450wcqkw19jwqgrahw1h30i")))

(define-public crate-bsutils-0.1 (crate (name "bsutils") (vers "0.1.2") (hash "1bck4z37yd8qcnnypkzd3hl6hvq5q0kjv5ks2v3w22skiynpy5nq")))

