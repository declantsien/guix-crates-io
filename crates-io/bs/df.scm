(define-module (crates-io bs df) #:use-module (crates-io))

(define-public crate-bsdf-0.1 (crate (name "bsdf") (vers "0.1.0") (deps (list (crate-dep (name "fastrand") (req "^2.0.0") (default-features #t) (kind 2)) (crate-dep (name "glam") (req "^0.24.1") (default-features #t) (kind 0)))) (hash "0vj7kfdd1b51vyxsy5qkj4zn6wlsq33w88sdyn1x6di2hl2nmhil") (features (quote (("rough-glass" "ggx") ("mix") ("lambert") ("ggx") ("emissive") ("disney" "ggx") ("default" "disney" "conductive" "emissive" "lambert" "mix" "rough-glass") ("conductive" "ggx")))) (yanked #t)))

(define-public crate-bsdf-0.1 (crate (name "bsdf") (vers "0.1.1") (deps (list (crate-dep (name "fastrand") (req "^2.0.0") (default-features #t) (kind 2)) (crate-dep (name "glam") (req "^0.24.1") (default-features #t) (kind 0)) (crate-dep (name "png") (req "^0.17.10") (default-features #t) (kind 2)) (crate-dep (name "rayon") (req "^1.8.0") (default-features #t) (kind 2)))) (hash "0n9rmd5pcsbzafcx11ns0fd3a8lj0si8d2qgax3jx1fs5y15mxw0") (features (quote (("rough-glass" "ggx") ("mix") ("lambert") ("ggx") ("emissive") ("disney" "ggx") ("default" "disney" "conductive" "emissive" "lambert" "mix" "rough-glass") ("conductive" "ggx"))))))

