(define-module (crates-io bs d-) #:use-module (crates-io))

(define-public crate-bsd-errnos-0.0.1 (crate (name "bsd-errnos") (vers "0.0.1") (hash "1ya0v9xjw8mzlwzswkfhviksy4xjkvcr3viwlsbjiva6rbm2rl68") (features (quote (("std") ("iter") ("default" "std"))))))

(define-public crate-bsd-kvm-0.1 (crate (name "bsd-kvm") (vers "0.1.0") (deps (list (crate-dep (name "bsd-kvm-sys") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0") (default-features #t) (kind 0)))) (hash "08raimisskq2fbvbvr3l19wh8ygdclklbzfmgf1zdcb5l07lqcw8")))

(define-public crate-bsd-kvm-0.1 (crate (name "bsd-kvm") (vers "0.1.1") (deps (list (crate-dep (name "bsd-kvm-sys") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2.149") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0") (default-features #t) (kind 0)))) (hash "1rm9cricb0qm7ppp0rvwpvv2ilh5igdcs1ssax9r4qgr5bnmd7cb")))

(define-public crate-bsd-kvm-0.1 (crate (name "bsd-kvm") (vers "0.1.2") (deps (list (crate-dep (name "bsd-kvm-sys") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2.149") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0") (default-features #t) (kind 0)))) (hash "1dqsbri9x6wk84gs76mj6cnrxhan9nn6jc5vjj1b256i210k8472")))

(define-public crate-bsd-kvm-0.1 (crate (name "bsd-kvm") (vers "0.1.3") (deps (list (crate-dep (name "bsd-kvm-sys") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2.149") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0") (default-features #t) (kind 0)))) (hash "042m2hx8kvf02p8jp4liy697a746qh5hpl17mj11ll7k3g7bd0nk")))

(define-public crate-bsd-kvm-0.1 (crate (name "bsd-kvm") (vers "0.1.4") (deps (list (crate-dep (name "bsd-kvm-sys") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2.149") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0") (default-features #t) (kind 0)))) (hash "1nzdmxplqg6mwsxdvicc6m7zsphcppkd8z57akb5zypywb75lm7z")))

(define-public crate-bsd-kvm-0.1 (crate (name "bsd-kvm") (vers "0.1.5") (deps (list (crate-dep (name "bsd-kvm-sys") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2.149") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0") (default-features #t) (kind 0)))) (hash "1gncwfwlx6mq47qc1siwaiqlsaccy7vsc1v39ybs4xvvn4lfpd4l")))

(define-public crate-bsd-kvm-sys-0.1 (crate (name "bsd-kvm-sys") (vers "0.1.0") (deps (list (crate-dep (name "bindgen") (req "^0.65.1") (default-features #t) (kind 1)))) (hash "125kdrxdja3v966qlnh6rchcjyvrs13c0pg27chi764iyc3wzagw")))

(define-public crate-bsd-kvm-sys-0.2 (crate (name "bsd-kvm-sys") (vers "0.2.0") (deps (list (crate-dep (name "bindgen") (req "^0.65.1") (default-features #t) (kind 1)))) (hash "0cn90d0kkfcs36v3sq3lpckyy0pdpdq0m7ihjlancripdn98yh35")))

