(define-module (crates-io bs t_) #:use-module (crates-io))

(define-public crate-bst_map_layer_derive-0.1 (crate (name "bst_map_layer_derive") (vers "0.1.0") (deps (list (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2.0") (default-features #t) (kind 0)))) (hash "1cknv3gy938mnvwxyjajjbqidynbc7wdik2i1w7k63qihvzllyni")))

