(define-module (crates-io bs a3) #:use-module (crates-io))

(define-public crate-bsa3-hash-1 (crate (name "bsa3-hash") (vers "1.0.0") (deps (list (crate-dep (name "criterion") (req "^0.2") (kind 2)))) (hash "0731lvw937l05610ahvc5x075sjbdgrkv9nypd2dyxykvyn55691")))

(define-public crate-bsa3-hash-1 (crate (name "bsa3-hash") (vers "1.0.1") (deps (list (crate-dep (name "criterion") (req "^0.2") (kind 2)))) (hash "1zks8bqq57696a90my3hask1qr15pdjhddiq17wbh6mma8g2gcql")))

(define-public crate-bsa3-hash-1 (crate (name "bsa3-hash") (vers "1.0.2") (deps (list (crate-dep (name "criterion") (req "^0.3.0") (default-features #t) (kind 2)))) (hash "0aipxwgmgci70jhrcrsaw8igy3lvxmnfp9giaq5ab8bwhnpdfrqm")))

(define-public crate-bsa3-hash-2 (crate (name "bsa3-hash") (vers "2.0.0") (deps (list (crate-dep (name "criterion") (req "^0.3.0") (default-features #t) (kind 2)))) (hash "02wckvb07hmhnmmqrw9vpdv9123i7npxpr9xilk3p264vbhsw4hj") (yanked #t)))

(define-public crate-bsa3-hash-3 (crate (name "bsa3-hash") (vers "3.0.0") (deps (list (crate-dep (name "criterion") (req "^0.3.0") (default-features #t) (kind 2)))) (hash "1dx0iqhkxjaha1kprhh8q5vd0522s1k43wpl0z2zdfcx8z3hhim5")))

