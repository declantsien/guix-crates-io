(define-module (crates-io bs di) #:use-module (crates-io))

(define-public crate-bsdiff-0.1 (crate (name "bsdiff") (vers "0.1.0") (deps (list (crate-dep (name "libc") (req "^0.2.24") (default-features #t) (kind 0)))) (hash "0w32aabcxd3rhqb1ygslb9sipszxjbmsm73l6aicb0sx0vw015sa") (yanked #t)))

(define-public crate-bsdiff-0.1 (crate (name "bsdiff") (vers "0.1.1") (deps (list (crate-dep (name "libc") (req "^0.2.24") (default-features #t) (kind 0)))) (hash "1xqz6l6rxizzf7jslxpc9gffn45zwlziac3dsi148rhx80apq3ms") (yanked #t)))

(define-public crate-bsdiff-0.1 (crate (name "bsdiff") (vers "0.1.2") (deps (list (crate-dep (name "libc") (req "^0.2.24") (default-features #t) (kind 0)))) (hash "0n08k0017hjhxaf8yfq1s7hc9ln0d0k0sp3kv0fx6drpx4jazsgx") (yanked #t)))

(define-public crate-bsdiff-0.1 (crate (name "bsdiff") (vers "0.1.3") (deps (list (crate-dep (name "libc") (req "^0.2.24") (default-features #t) (kind 0)))) (hash "0z50d365jwhkzas2xkjgsi6dlc8ysr0ywzdi4s7gpv9lxh6hha32")))

(define-public crate-bsdiff-0.1 (crate (name "bsdiff") (vers "0.1.4") (deps (list (crate-dep (name "libc") (req "^0.2.24") (default-features #t) (kind 0)))) (hash "06s01v9l04phpnpj9z1n3kppmzhkppfch7am5vnbmdwb92x3q5k3")))

(define-public crate-bsdiff-0.1 (crate (name "bsdiff") (vers "0.1.5") (hash "0rghcyyk9kw67qn9ag8ffalg3nwb76qab237h8hn8ri0gqwxfyp7")))

(define-public crate-bsdiff-0.1 (crate (name "bsdiff") (vers "0.1.6") (hash "14cfn23v86ym0lmmnrm7i9c4i8dczn9v2nm9h9nkwsc1x9n4b9ak")))

(define-public crate-bsdiff-0.2 (crate (name "bsdiff") (vers "0.2.0") (hash "1wkjdcpxfmpp1s2cmg4hx2im66q6gx1wrmqzb8xzc5x0yb2fdwpp")))

