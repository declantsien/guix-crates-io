(define-module (crates-io bs #{62}#) #:use-module (crates-io))

(define-public crate-bs62-0.1 (crate (name "bs62") (vers "0.1.0") (deps (list (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "num-bigint") (req "^0.4.0") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2.14") (default-features #t) (kind 0)))) (hash "0ny8y84l4w8v31d66qkrxdbq4bph9ry8ki9zdhpb37qhll152d0v")))

(define-public crate-bs62-0.1 (crate (name "bs62") (vers "0.1.1") (deps (list (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "num-bigint") (req "^0.4.0") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2.14") (default-features #t) (kind 0)))) (hash "0kv0fxw56g15z9wn5syv861i91i897n1w3aisn5jwic1c3xwfcwc")))

(define-public crate-bs62-0.1 (crate (name "bs62") (vers "0.1.2") (deps (list (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "num-bigint") (req "^0.4.0") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2.14") (default-features #t) (kind 0)))) (hash "19kksi1aga8hx43y74kphlcck1ck1sdqlads3bhqy3qjrqgcymk4")))

(define-public crate-bs62-0.1 (crate (name "bs62") (vers "0.1.3") (deps (list (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "num-bigint") (req "^0.4.0") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2.14") (default-features #t) (kind 0)))) (hash "0pbsr16rdr2vzfy4mcf8l9fbmpl59p3cbykmz6f5ng4d2rj2a62j")))

(define-public crate-bs62-0.1 (crate (name "bs62") (vers "0.1.4") (deps (list (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "num-bigint") (req "^0.4.0") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2.14") (default-features #t) (kind 0)))) (hash "1g9ilny6mviam2l0l8fr2pp4nfpsz15bwjdsdjp582a3i9x0zpxg")))

