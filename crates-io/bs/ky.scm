(define-module (crates-io bs ky) #:use-module (crates-io))

(define-public crate-bsky-0.0.0 (crate (name "bsky") (vers "0.0.0-placeholder.0") (hash "1j8xyhm276dvmhcyiq95k8iid446wb6l03rv0y98zawkpsrm6wzb") (rust-version "1.61")))

(define-public crate-bsky-rs-0.0.1 (crate (name "bsky-rs") (vers "0.0.1") (deps (list (crate-dep (name "bsky-rs-models") (req "^0.0.1") (default-features #t) (kind 0)))) (hash "095rz21anb78sp1z24vsvbj6y8la565x2gczvyjc5y5sx7acnzzp")))

(define-public crate-bsky-rs-models-0.0.1 (crate (name "bsky-rs-models") (vers "0.0.1") (deps (list (crate-dep (name "serde") (req "^1.0.159") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "1bkj9dkgsn185v16sx5hrwxsmcpv9fzkk92xl17a5flih5mxhva9")))

(define-public crate-bsky_cmdl-0.1 (crate (name "bsky_cmdl") (vers "0.1.0") (hash "0873sbf7p4p8xms08j3282fvd23z5b6mr9i97ja5vy7x6c7g7d3q")))

(define-public crate-bsky_jennings-0.1 (crate (name "bsky_jennings") (vers "0.1.0") (hash "04vq68931x8pwdylg48m8kkbr1204q5md1r54ghm1zjk9j4zxpzb")))

(define-public crate-bsky_tui-0.0.1 (crate (name "bsky_tui") (vers "0.0.1") (deps (list (crate-dep (name "atrium-api") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "atrium-xrpc") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "chrono") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "crossterm") (req "^0.27") (default-features #t) (kind 0)) (crate-dep (name "eyre") (req "^0.6") (default-features #t) (kind 0)) (crate-dep (name "ratatui") (req "^0.22") (features (quote ("all-widgets"))) (default-features #t) (kind 0)) (crate-dep (name "seahorse") (req "^2.1") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "0pz6qk6bblwp2x3ck4y2pzlnb84g5sw6yhabf8cz406gy5yvk8k1")))

(define-public crate-bsky_tui-0.0.2 (crate (name "bsky_tui") (vers "0.0.2") (deps (list (crate-dep (name "atrium-api") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "atrium-xrpc") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "chrono") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "crossterm") (req "^0.27") (default-features #t) (kind 0)) (crate-dep (name "eyre") (req "^0.6") (default-features #t) (kind 0)) (crate-dep (name "ratatui") (req "^0.22") (features (quote ("all-widgets"))) (default-features #t) (kind 0)) (crate-dep (name "seahorse") (req "^2.1") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1") (features (quote ("full"))) (default-features #t) (kind 0)) (crate-dep (name "webbrowser") (req "^0.8") (default-features #t) (kind 0)))) (hash "1df42r0xarrnvd15xc3bkgdv799qx6s2lhp2vzydz99rba7v1mns")))

(define-public crate-bsky_tui-0.0.3 (crate (name "bsky_tui") (vers "0.0.3") (deps (list (crate-dep (name "atrium-api") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "atrium-xrpc") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "chrono") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "crossterm") (req "^0.27") (default-features #t) (kind 0)) (crate-dep (name "eyre") (req "^0.6") (default-features #t) (kind 0)) (crate-dep (name "ratatui") (req "^0.22") (features (quote ("all-widgets"))) (default-features #t) (kind 0)) (crate-dep (name "seahorse") (req "^2.1") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1") (features (quote ("full"))) (default-features #t) (kind 0)) (crate-dep (name "webbrowser") (req "^0.8") (default-features #t) (kind 0)))) (hash "1gjixvi7k7wg3wisqkh36qmm4a1ym60gx4mj6w61w2qg21qwbm5r")))

