(define-module (crates-io bs sl) #:use-module (crates-io))

(define-public crate-bssl-0.0.0 (crate (name "bssl") (vers "0.0.0") (hash "022brrv2ainchfmcv9wmwmx54h1zcy0wgzr70bf9b4bi1rsrzcdi")))

(define-public crate-bssl-cmake-sys-0.1 (crate (name "bssl-cmake-sys") (vers "0.1.2403141") (deps (list (crate-dep (name "bindgen") (req "^0.69") (default-features #t) (kind 1)) (crate-dep (name "cc") (req "^1.0") (default-features #t) (kind 1)) (crate-dep (name "cmake") (req "^0.1") (default-features #t) (kind 1)))) (hash "0k002fwqc1qhigdxbdh58wmn31ajjkpxzvpydkcynbi9nj73jny3") (links "bssl")))

(define-public crate-bssl-cmake-sys-0.1 (crate (name "bssl-cmake-sys") (vers "0.1.2403142") (deps (list (crate-dep (name "bindgen") (req "^0.69") (default-features #t) (kind 1)) (crate-dep (name "cc") (req "^1.0") (default-features #t) (kind 1)) (crate-dep (name "cmake") (req "^0.1") (default-features #t) (kind 1)))) (hash "06drcs7dlmh3bi32xvxyag6842jqzlidba24cjnhxdwysirbl1kl") (links "bssl")))

(define-public crate-bssl-cmake-sys-0.1 (crate (name "bssl-cmake-sys") (vers "0.1.2403180") (deps (list (crate-dep (name "bindgen") (req "^0.69") (default-features #t) (kind 1)) (crate-dep (name "cc") (req "^1.0") (default-features #t) (kind 1)) (crate-dep (name "cmake") (req "^0.1") (default-features #t) (kind 1)))) (hash "1f2g9n35h0rmqjygqydjjwfxf5p0f78kg6xww1gnai72gnbkzv37") (links "bssl")))

(define-public crate-bssl-cmake-sys-0.1 (crate (name "bssl-cmake-sys") (vers "0.1.2403260") (deps (list (crate-dep (name "bindgen") (req "^0.69") (default-features #t) (kind 1)) (crate-dep (name "cc") (req "^1.0") (default-features #t) (kind 1)) (crate-dep (name "cmake") (req "^0.1") (default-features #t) (kind 1)))) (hash "0sgs6fjlm67n5xxnmpf89jqfq1sr47ghcycgn0ym3wzjs34av6pb") (links "bssl")))

(define-public crate-bssl-cmake-sys-0.1 (crate (name "bssl-cmake-sys") (vers "0.1.2404010") (deps (list (crate-dep (name "bindgen") (req "^0.69") (default-features #t) (kind 1)) (crate-dep (name "cc") (req "^1.0") (default-features #t) (kind 1)) (crate-dep (name "cmake") (req "^0.1") (default-features #t) (kind 1)))) (hash "0ad9drhavkny7ad00mjnl4y4l33kx63v1imc97fls88sp8cvczn1") (links "bssl")))

(define-public crate-bssl-cmake-sys-0.1 (crate (name "bssl-cmake-sys") (vers "0.1.2404150") (deps (list (crate-dep (name "bindgen") (req "^0.69") (default-features #t) (kind 1)) (crate-dep (name "cc") (req "^1.0") (default-features #t) (kind 1)) (crate-dep (name "cmake") (req "^0.1") (default-features #t) (kind 1)))) (hash "1dbkwidy8rhsgbv1mw2l7vhq8qph37gra37570jg5d6sx9dam9lj") (links "bssl")))

(define-public crate-bssl-cmake-sys-0.1 (crate (name "bssl-cmake-sys") (vers "0.1.2404220") (deps (list (crate-dep (name "bindgen") (req "^0.69") (default-features #t) (kind 1)) (crate-dep (name "cc") (req "^1.0") (default-features #t) (kind 1)) (crate-dep (name "cmake") (req "^0.1") (default-features #t) (kind 1)))) (hash "1awass99lhm3wlvh5rpj6kl5bzr2198q79lphfkfn0hixkr50ins") (links "bssl")))

(define-public crate-bssl-cmake-sys-0.1 (crate (name "bssl-cmake-sys") (vers "0.1.2405060") (deps (list (crate-dep (name "bindgen") (req "^0.69") (default-features #t) (kind 1)) (crate-dep (name "cc") (req "^1.0") (default-features #t) (kind 1)) (crate-dep (name "cmake") (req "^0.1") (default-features #t) (kind 1)))) (hash "1d8xvdlc6549c2svdsi7qmnjg8212y34fikbvr9hq2iq4s9j1bb8") (links "bssl")))

(define-public crate-bssl-cmake-sys-0.1 (crate (name "bssl-cmake-sys") (vers "0.1.2405130") (deps (list (crate-dep (name "bindgen") (req "^0.69") (default-features #t) (kind 1)) (crate-dep (name "cc") (req "^1.0") (default-features #t) (kind 1)) (crate-dep (name "cmake") (req "^0.1") (default-features #t) (kind 1)))) (hash "1zjw7nx956ic6rinljdi1k8gykadp41i30gxw7fxdm666vs30dp7") (links "bssl")))

(define-public crate-bssl-cmake-sys-0.1 (crate (name "bssl-cmake-sys") (vers "0.1.2405200") (deps (list (crate-dep (name "bindgen") (req "^0.69") (default-features #t) (kind 1)) (crate-dep (name "cc") (req "^1.0") (default-features #t) (kind 1)) (crate-dep (name "cmake") (req "^0.1") (default-features #t) (kind 1)))) (hash "08daz24l1v963zi0d7b10j3vp9mjw5lgczslrdkrchn5pg5adabr") (links "bssl")))

(define-public crate-bssl-cmake-sys-0.1 (crate (name "bssl-cmake-sys") (vers "0.1.2405270") (deps (list (crate-dep (name "bindgen") (req "^0.69") (default-features #t) (kind 1)) (crate-dep (name "cc") (req "^1.0") (default-features #t) (kind 1)) (crate-dep (name "cmake") (req "^0.1") (default-features #t) (kind 1)))) (hash "10h1zym1xs6r8m2ahlbp31vqssdnwcsj34xih9bwqlj2fbkjvjgg") (links "bssl")))

(define-public crate-bssl-sys-0.1 (crate (name "bssl-sys") (vers "0.1.0") (hash "0p5v3ad1paf12db4hmwq4j8dvcrppsscf57dwvr880q67hwi4b9i")))

