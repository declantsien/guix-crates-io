(define-module (crates-io bs ha) #:use-module (crates-io))

(define-public crate-bsharp-0.1 (crate (name "bsharp") (vers "0.1.0") (deps (list (crate-dep (name "fset") (req "^0.1.0") (default-features #t) (kind 0) (package "bsharp_fset")) (crate-dep (name "ir") (req "^0.1.0") (default-features #t) (kind 0) (package "bsharp_ir")))) (hash "1fj6zzcw49w1crnk2b2gz94sfllhqdmn4jbfxylfmqfq9wccij8w")))

(define-public crate-bsharp-0.2 (crate (name "bsharp") (vers "0.2.1") (deps (list (crate-dep (name "clap") (req "^4.1.13") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "fset") (req "^0.2.0") (default-features #t) (kind 0) (package "bsharp_fset")) (crate-dep (name "interp") (req "^0.2.0") (default-features #t) (kind 0) (package "bsharp_interp")) (crate-dep (name "ir") (req "^0.2.0") (default-features #t) (kind 0) (package "bsharp_ir")))) (hash "0fgzki40cq4bgrpih09m6qb6ajf2zy192ngyips1z9l5sk8j02kx")))

(define-public crate-bsharp-0.2 (crate (name "bsharp") (vers "0.2.2") (deps (list (crate-dep (name "bstar") (req "^0.2.2") (default-features #t) (kind 0) (package "bsharp_bstar")) (crate-dep (name "clap") (req "^4.1.13") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "fset") (req "^0.2.2") (default-features #t) (kind 0) (package "bsharp_fset")) (crate-dep (name "interp") (req "^0.2.2") (default-features #t) (kind 0) (package "bsharp_interp")) (crate-dep (name "ir") (req "^0.2.2") (default-features #t) (kind 0) (package "bsharp_ir")))) (hash "18swwrb6wj3w3iak70rxjgvy18a8cc47lb6c3lmxgsjpszi6rc4n")))

(define-public crate-bsharp-0.2 (crate (name "bsharp") (vers "0.2.3") (deps (list (crate-dep (name "bstar") (req "^0.2.2") (default-features #t) (kind 0) (package "bsharp_bstar")) (crate-dep (name "clap") (req "^4.1.13") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "fset") (req "^0.2.2") (default-features #t) (kind 0) (package "bsharp_fset")) (crate-dep (name "interp") (req "^0.2.2") (default-features #t) (kind 0) (package "bsharp_interp")) (crate-dep (name "ir") (req "^0.2.2") (default-features #t) (kind 0) (package "bsharp_ir")))) (hash "18zblbll6m34hykxldhw271dmmlfdiycgvzsy6x9rrqr8wm0calb")))

(define-public crate-bsharp_bstar-0.2 (crate (name "bsharp_bstar") (vers "0.2.2") (deps (list (crate-dep (name "ir") (req "^0.2.2") (default-features #t) (kind 0) (package "bsharp_ir")))) (hash "0c6i084bc0aahi40c43jplrjrwcg9izgmkwq6rs1534sax4ph0xd")))

(define-public crate-bsharp_bstar-0.2 (crate (name "bsharp_bstar") (vers "0.2.3") (deps (list (crate-dep (name "ir") (req "^0.2.2") (default-features #t) (kind 0) (package "bsharp_ir")))) (hash "1mdwn0203m2klhljz0jyiafc9xsia0yrblp092l5r522hn8hmxd8")))

(define-public crate-bsharp_fset-0.1 (crate (name "bsharp_fset") (vers "0.1.0") (deps (list (crate-dep (name "parser") (req "^0.1.0") (default-features #t) (kind 0) (package "bsharp_parser")) (crate-dep (name "tokens") (req "^0.1.0") (default-features #t) (kind 0) (package "bsharp_tokens")))) (hash "09jzxn3qk68789p4v1r7s7a3fjj5mlycfxd56am0w4wyzc1iznk1")))

(define-public crate-bsharp_fset-0.2 (crate (name "bsharp_fset") (vers "0.2.1") (deps (list (crate-dep (name "include_dir") (req "^0.7.3") (default-features #t) (kind 0)) (crate-dep (name "parser") (req "^0.2.0") (default-features #t) (kind 0) (package "bsharp_parser")) (crate-dep (name "tokens") (req "^0.2.0") (default-features #t) (kind 0) (package "bsharp_tokens")))) (hash "1mgjphnsjfq1s0b4a4ngjcdmz9bi7varppw5y0sfg04mgvz4gi40")))

(define-public crate-bsharp_fset-0.2 (crate (name "bsharp_fset") (vers "0.2.2") (deps (list (crate-dep (name "include_dir") (req "^0.7.3") (default-features #t) (kind 0)) (crate-dep (name "parser") (req "^0.2.2") (default-features #t) (kind 0) (package "bsharp_parser")) (crate-dep (name "tokens") (req "^0.2.2") (default-features #t) (kind 0) (package "bsharp_tokens")))) (hash "18cdsvk10nskdj8g5zghm2a89xgxrxxqm9jbdwvh08cs0y5a46cj")))

(define-public crate-bsharp_fset-0.2 (crate (name "bsharp_fset") (vers "0.2.3") (deps (list (crate-dep (name "include_dir") (req "^0.7.3") (default-features #t) (kind 0)) (crate-dep (name "parser") (req "^0.2.2") (default-features #t) (kind 0) (package "bsharp_parser")) (crate-dep (name "tokens") (req "^0.2.2") (default-features #t) (kind 0) (package "bsharp_tokens")))) (hash "0jqsr2mzd9v7pv1y7fwkshxzak12fsbwsh0rbnbv3xkv76049dqm")))

(define-public crate-bsharp_interp-0.1 (crate (name "bsharp_interp") (vers "0.1.0") (deps (list (crate-dep (name "ir") (req "^0.1.0") (default-features #t) (kind 0) (package "bsharp_ir")))) (hash "0jc6mgbs06468fi4145zw1y4njx6pkw60x97ncafskx65j5idg9l")))

(define-public crate-bsharp_interp-0.2 (crate (name "bsharp_interp") (vers "0.2.1") (deps (list (crate-dep (name "ir") (req "^0.2.0") (default-features #t) (kind 0) (package "bsharp_ir")))) (hash "12x1v7saqxhnkb72lyyzqg64k486i30zwfk81ddgs5189zjjjnaq")))

(define-public crate-bsharp_interp-0.2 (crate (name "bsharp_interp") (vers "0.2.2") (deps (list (crate-dep (name "ir") (req "^0.2.2") (default-features #t) (kind 0) (package "bsharp_ir")))) (hash "1gim6ziym52nk2djhglav8zv62z2q7aavxvrvxkdb62f66ar7v6j")))

(define-public crate-bsharp_interp-0.2 (crate (name "bsharp_interp") (vers "0.2.3") (deps (list (crate-dep (name "ir") (req "^0.2.2") (default-features #t) (kind 0) (package "bsharp_ir")))) (hash "1picq5asnalz93ryqwmv8n1i6w3rs9jl8dd2vx5lmna93ycagvxj")))

(define-public crate-bsharp_ir-0.1 (crate (name "bsharp_ir") (vers "0.1.0") (deps (list (crate-dep (name "fset") (req "^0.1.0") (default-features #t) (kind 0) (package "bsharp_fset")))) (hash "15f2i8qkw4pcypi71jq1xv2k6prh9k3gbxvrcb2pabirgna5hbpq")))

(define-public crate-bsharp_ir-0.2 (crate (name "bsharp_ir") (vers "0.2.1") (deps (list (crate-dep (name "fset") (req "^0.2.0") (default-features #t) (kind 0) (package "bsharp_fset")))) (hash "0lcb82raap6352a5xppbacc6mq176q5vlk8xlbr7mvf09nx48r1b")))

(define-public crate-bsharp_ir-0.2 (crate (name "bsharp_ir") (vers "0.2.2") (deps (list (crate-dep (name "fset") (req "^0.2.2") (default-features #t) (kind 0) (package "bsharp_fset")))) (hash "0kf9g2784v3hviq94fywm53v527j850arbrm8kp9gkcgk8icl7nz")))

(define-public crate-bsharp_ir-0.2 (crate (name "bsharp_ir") (vers "0.2.3") (deps (list (crate-dep (name "fset") (req "^0.2.2") (default-features #t) (kind 0) (package "bsharp_fset")))) (hash "1y03k1319ab5agddq3akxh58k19zvsv2sh4z4vi2663cz4dqm7ps")))

(define-public crate-bsharp_lsp-0.2 (crate (name "bsharp_lsp") (vers "0.2.2") (deps (list (crate-dep (name "fset") (req "^0.2.2") (default-features #t) (kind 0) (package "bsharp_fset")) (crate-dep (name "ir") (req "^0.2.2") (default-features #t) (kind 0) (package "bsharp_ir")) (crate-dep (name "tokio") (req "^1.32.0") (features (quote ("full"))) (default-features #t) (kind 0)) (crate-dep (name "tower-lsp") (req "^0.20.0") (default-features #t) (kind 0)))) (hash "0n5v5rzbscp1db9akr5i1q7323knmdi9wsxzylxcbs49anly5qg1")))

(define-public crate-bsharp_lsp-0.2 (crate (name "bsharp_lsp") (vers "0.2.3") (deps (list (crate-dep (name "fset") (req "^0.2.2") (default-features #t) (kind 0) (package "bsharp_fset")) (crate-dep (name "ir") (req "^0.2.2") (default-features #t) (kind 0) (package "bsharp_ir")) (crate-dep (name "tokens") (req "^0.2.2") (default-features #t) (kind 0) (package "bsharp_tokens")) (crate-dep (name "tokio") (req "^1.32.0") (features (quote ("full"))) (default-features #t) (kind 0)) (crate-dep (name "tower-lsp") (req "^0.20.0") (default-features #t) (kind 0)))) (hash "0g2iizzf2dgjlv3jcpa7mild9ymgbxkdirwna6wabah02pxkxlh4")))

(define-public crate-bsharp_parser-0.1 (crate (name "bsharp_parser") (vers "0.1.0") (deps (list (crate-dep (name "tokens") (req "^0.1.0") (default-features #t) (kind 0) (package "bsharp_tokens")))) (hash "0pj6s0kb2ny17ph8zm19wpjzpfqh49fi6r1vrrxx0xgn9jmlkl1w")))

(define-public crate-bsharp_parser-0.2 (crate (name "bsharp_parser") (vers "0.2.1") (deps (list (crate-dep (name "tokens") (req "^0.2.0") (default-features #t) (kind 0) (package "bsharp_tokens")))) (hash "1mq66yrrvy4dwm1zrvbxjnn8z5hzjmm71nwff4f4k1zbqa5blycc")))

(define-public crate-bsharp_parser-0.2 (crate (name "bsharp_parser") (vers "0.2.2") (deps (list (crate-dep (name "tokens") (req "^0.2.2") (default-features #t) (kind 0) (package "bsharp_tokens")))) (hash "053sr4nbswfv5z1bfq9zc36w3y53849pq38md94j71by8bhxm7z5")))

(define-public crate-bsharp_parser-0.2 (crate (name "bsharp_parser") (vers "0.2.3") (deps (list (crate-dep (name "tokens") (req "^0.2.2") (default-features #t) (kind 0) (package "bsharp_tokens")))) (hash "1bwj35pcl98aq0miahqprlhyv6cnxpii9shgwdz0q03aiclf73a5")))

(define-public crate-bsharp_tokens-0.1 (crate (name "bsharp_tokens") (vers "0.1.0") (hash "0mh5ilp8hnkl78vgcifcws5isfcq6kh62lcw5dzsd50sdzr1h87a")))

(define-public crate-bsharp_tokens-0.2 (crate (name "bsharp_tokens") (vers "0.2.1") (hash "0k9hdqmng73zdmjwncqq4swcffv8311p80j34qpbg7zdpmdjkx9g")))

(define-public crate-bsharp_tokens-0.2 (crate (name "bsharp_tokens") (vers "0.2.2") (hash "1js02y8hmh555yfpx88jipfn8lzw9zp9w0hi1gmrp4s74h7rkk9n")))

(define-public crate-bsharp_tokens-0.2 (crate (name "bsharp_tokens") (vers "0.2.3") (hash "13aj01rc4kfbqma51v6hnw4xsz61bpgxjfid3qb0i47gvx6d5xad")))

