(define-module (crates-io bs od) #:use-module (crates-io))

(define-public crate-bsod-0.1 (crate (name "bsod") (vers "0.1.0") (deps (list (crate-dep (name "windows") (req "^0.43.0") (features (quote ("Win32_Foundation" "Win32_System_LibraryLoader"))) (default-features #t) (kind 0)))) (hash "0hlyv2m8n4qfyqm5ggz7sgl8r27hi9aj8ql8l2wk85lfm9qd254l")))

(define-public crate-bsod-0.1 (crate (name "bsod") (vers "0.1.1") (deps (list (crate-dep (name "windows") (req "^0.43.0") (features (quote ("Win32_Foundation" "Win32_System_LibraryLoader"))) (default-features #t) (kind 0)))) (hash "1nwwg1p51x0k2v2bawsm7n1n5dxpvm0wpsvi2vhhphiqlcfybhp8")))

