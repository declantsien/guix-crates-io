(define-module (crates-io bs #{17}#) #:use-module (crates-io))

(define-public crate-bs1770-1 (crate (name "bs1770") (vers "1.0.0") (deps (list (crate-dep (name "claxon") (req "^0.4.3") (default-features #t) (kind 2)) (crate-dep (name "hound") (req "^3.4.0") (default-features #t) (kind 2)) (crate-dep (name "libc") (req "^0.2.76") (default-features #t) (kind 2)))) (hash "0nvp2qxijmnii1fy35hg41bra07rrmvbhb7zdwj6dw2z40fccck3")))

