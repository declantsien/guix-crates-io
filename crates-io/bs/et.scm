(define-module (crates-io bs et) #:use-module (crates-io))

(define-public crate-bset-0.1 (crate (name "bset") (vers "0.1.0") (deps (list (crate-dep (name "byte_set") (req "^0.1.3") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.8") (default-features #t) (kind 2)))) (hash "10fmd75f7acj850z81v1n7pqqcs0jaszj0vdxsiwhsivv7mg4ch8")))

