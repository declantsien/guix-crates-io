(define-module (crates-io bs al) #:use-module (crates-io))

(define-public crate-bsalib-0.1 (crate (name "bsalib") (vers "0.1.0") (deps (list (crate-dep (name "bytemuck") (req "^1.5.1") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "enumflags2") (req "^0.7.1") (default-features #t) (kind 0)) (crate-dep (name "libflate") (req "^1.1.0") (default-features #t) (kind 0)) (crate-dep (name "lz4") (req "^1.23.2") (default-features #t) (kind 0)) (crate-dep (name "macro-attr-2018") (req "^1.1.1") (default-features #t) (kind 0)) (crate-dep (name "newtype-derive-2018") (req "^0.0.3") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.28") (default-features #t) (kind 0)))) (hash "16vvhv0988h15qps0sllamhi92bbckm1lbcgm9vq1yv0x61giyca")))

(define-public crate-bsalloc-0.1 (crate (name "bsalloc") (vers "0.1.0") (deps (list (crate-dep (name "lazy_static") (req "^0.2") (features (quote ("spin_no_std"))) (default-features #t) (kind 0)) (crate-dep (name "mmap-alloc") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "1zsnjv77rmkv561n48y9b0l7hg2a7c8jx4kkr0vyq4d1z4knivsf")))

