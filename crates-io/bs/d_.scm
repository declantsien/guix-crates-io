(define-module (crates-io bs d_) #:use-module (crates-io))

(define-public crate-bsd_auth-0.0.1 (crate (name "bsd_auth") (vers "0.0.1") (deps (list (crate-dep (name "bsd_auth-sys") (req "^0.0.3") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2.98") (default-features #t) (kind 0)))) (hash "13jm9rwpa2hh90dws90j9xscgpqi08x7r43pwm8fvp09l208m6hm")))

(define-public crate-bsd_auth-0.0.2 (crate (name "bsd_auth") (vers "0.0.2") (deps (list (crate-dep (name "bsd_auth-sys") (req "^0.0.3") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2.98") (default-features #t) (kind 0)))) (hash "0r58k1n73l06lc2hhg0k5lkay1125jf4ad00fmm5w5cdl482qz88")))

(define-public crate-bsd_auth-0.0.3 (crate (name "bsd_auth") (vers "0.0.3") (deps (list (crate-dep (name "bsd_auth-sys") (req "^0.0.3") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2.98") (default-features #t) (kind 0)))) (hash "1mzb47y5wl3l54njh8bbll5fri8jlf52fmr0l41g20kwhzlakxq9")))

(define-public crate-bsd_auth-0.1 (crate (name "bsd_auth") (vers "0.1.0") (deps (list (crate-dep (name "bsd_auth-sys") (req "^0.0.3") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2.98") (default-features #t) (kind 0)))) (hash "04bk9zlqgincx1hbd25hbwanzhgabafvx804q46fpjz6hjrzq862")))

(define-public crate-bsd_auth-0.2 (crate (name "bsd_auth") (vers "0.2.0") (deps (list (crate-dep (name "bsd_auth-sys") (req "^0.0.6") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2.98") (default-features #t) (kind 0)))) (hash "05hvc948zzf633yf0iwgc1i702kpv80pasg98b5g3srbwvkrpy7w")))

(define-public crate-bsd_auth-0.2 (crate (name "bsd_auth") (vers "0.2.1") (deps (list (crate-dep (name "bsd_auth-sys") (req "^0.0.6") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2.98") (default-features #t) (kind 0)))) (hash "152pwfvf74ar71zzd3fwlm4zqk8lbbvhdigazpd4j3zzm1yywcm5")))

(define-public crate-bsd_auth-0.2 (crate (name "bsd_auth") (vers "0.2.2") (deps (list (crate-dep (name "bsd_auth-sys") (req "^0.0.6") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2.98") (default-features #t) (kind 0)))) (hash "1p1kyzp6jk1qwd6w2gdfihb6y2m420jlvm6h566nwmag4qkrihb1")))

(define-public crate-bsd_auth-0.3 (crate (name "bsd_auth") (vers "0.3.0") (deps (list (crate-dep (name "bsd_auth-sys") (req "^0.0.6") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2.98") (default-features #t) (kind 0)))) (hash "1iw68ik85gy6vf1lk3rqw6qqn48krm9sram3nnry7l3zivairwwz")))

(define-public crate-bsd_auth-0.3 (crate (name "bsd_auth") (vers "0.3.1") (deps (list (crate-dep (name "bsd_auth-sys") (req "^0.0.7") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2.98") (default-features #t) (kind 0)))) (hash "06bkwcpq41dbadpvr05p6p7f8xpyrvg1s7yx296f7jkxjhf6nfji")))

(define-public crate-bsd_auth-sys-0.0.1 (crate (name "bsd_auth-sys") (vers "0.0.1") (deps (list (crate-dep (name "bindgen") (req "^0.58.1") (default-features #t) (kind 1)))) (hash "12xdizh89gsx4036am0zh9iykrq7pcljxqi99djg9gwjv7ff28i1")))

(define-public crate-bsd_auth-sys-0.0.2 (crate (name "bsd_auth-sys") (vers "0.0.2") (deps (list (crate-dep (name "bindgen") (req "^0.58.1") (default-features #t) (kind 1)))) (hash "1wyqwqlqw721ixjdi0l3j0g0acwxhh05h19r8ziws51jahad2x5i")))

(define-public crate-bsd_auth-sys-0.0.3 (crate (name "bsd_auth-sys") (vers "0.0.3") (deps (list (crate-dep (name "bindgen") (req "^0.58.1") (default-features #t) (kind 1)))) (hash "1jiz7643a97zwkgzbwd4xqj9nc9ycwrr3hc8ak03kny0kc84vkky")))

(define-public crate-bsd_auth-sys-0.0.4 (crate (name "bsd_auth-sys") (vers "0.0.4") (deps (list (crate-dep (name "bindgen") (req "^0.58.1") (default-features #t) (kind 1)))) (hash "0chskldlw42zss3zzf777drbx9zj65qpfkjhh07fwp9g3y3xvgc2")))

(define-public crate-bsd_auth-sys-0.0.5 (crate (name "bsd_auth-sys") (vers "0.0.5") (deps (list (crate-dep (name "bindgen") (req "^0.58.1") (default-features #t) (kind 1)))) (hash "1i7jii2f9cyw0mylpzn74qdnj4vrfjjwbyppwzzb38brs0rb933r")))

(define-public crate-bsd_auth-sys-0.0.6 (crate (name "bsd_auth-sys") (vers "0.0.6") (deps (list (crate-dep (name "bindgen") (req "^0.58.1") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2.98") (default-features #t) (kind 0)))) (hash "0q02p3b6k94nwcna43wsp7igfiabpjvdk0ni05ly36hj7n84h9wh")))

(define-public crate-bsd_auth-sys-0.0.7 (crate (name "bsd_auth-sys") (vers "0.0.7") (deps (list (crate-dep (name "libc") (req "^0.2.98") (default-features #t) (kind 0)))) (hash "1xwvggm55zww35is1vl10jlad0a8ckm2q5za3dxkw4zc9kziy2y9")))

