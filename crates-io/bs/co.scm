(define-module (crates-io bs co) #:use-module (crates-io))

(define-public crate-bscore-0.1 (crate (name "bscore") (vers "0.1.0") (hash "0z4892fn27jyznnas8l80zgqpz9bkq45njyfdnkis7w1s9966yhz") (yanked #t)))

(define-public crate-bscore-0.1 (crate (name "bscore") (vers "0.1.1") (hash "0xrdnck7zd4faawqv0zns02lgfvqc0nd18h9gvcdggkl4p7i3hb5") (yanked #t)))

(define-public crate-bscore-0.1 (crate (name "bscore") (vers "0.1.2") (hash "1cf671ly28lbzllq1xkcn39imcbvcjig8jiq7g8ldvps7yd7sx4g") (yanked #t)))

(define-public crate-bscore-0.1 (crate (name "bscore") (vers "0.1.3") (hash "1qv7ji0hpyq2fgqxqpj91rmjj374km8a9qqnwqi63a1fw5dpzsxv") (yanked #t)))

(define-public crate-bscore-1 (crate (name "bscore") (vers "1.0.0") (hash "0jl81l7zbxav1p41pyx80ch2z3m84ka2j5z6svi0hyjls3vn067a") (yanked #t)))

(define-public crate-bscore-1 (crate (name "bscore") (vers "1.0.1") (hash "0d4lqr977837bzbx6j2c04kd5z9q8s9ij8bl286nzmiscf6zhzmm")))

(define-public crate-bscore-1 (crate (name "bscore") (vers "1.1.1") (hash "1fw5lqbn7c0fr5c25rkmljdx528anhxinhbcbvvssffd87xzs0yc")))

(define-public crate-bscore-1 (crate (name "bscore") (vers "1.2.1") (hash "0s5pjnhqac4bq845lp3pj4ffx2b09gfklgz1pga4gbawscsb4p3s")))

(define-public crate-bscore-lib-0.1 (crate (name "bscore-lib") (vers "0.1.0") (deps (list (crate-dep (name "bscore") (req "^0.1") (default-features #t) (kind 0)))) (hash "0x0niwmhmwsnkn4n5ymh9x8psv4r6na2xqyvjm4wim7i62xnd6zs") (yanked #t)))

(define-public crate-bscore-lib-1 (crate (name "bscore-lib") (vers "1.0.0") (deps (list (crate-dep (name "bscore") (req "^1.0") (default-features #t) (kind 0)))) (hash "1516hxf06lfgv5hpqkm1l4s04vizd8gd6c2bskm69x39dslab33v") (yanked #t)))

(define-public crate-bscore-lib-1 (crate (name "bscore-lib") (vers "1.0.1") (deps (list (crate-dep (name "bscore") (req "^1.0") (default-features #t) (kind 0)))) (hash "1aq4sjwnsxdimg275zish3iwhv1194fdmcrzrwbg2c524h5sd15r") (yanked #t)))

(define-public crate-bscore-lib-1 (crate (name "bscore-lib") (vers "1.0.2") (deps (list (crate-dep (name "bscore") (req "^1.0") (default-features #t) (kind 0)))) (hash "0i3x39gg6yphvlbkahm6pcyck63r2a4r3c651l29pnziyi2i0rld")))

(define-public crate-bscore-lib-1 (crate (name "bscore-lib") (vers "1.1.1") (deps (list (crate-dep (name "bscore") (req "^1.1") (default-features #t) (kind 0)))) (hash "1ygfyi5hpkmalzw8r7mm8bhqq91ar7ylpv2ayy7x4zdw7wzcqfhc")))

(define-public crate-bscore-lib-1 (crate (name "bscore-lib") (vers "1.2.1") (deps (list (crate-dep (name "bscore") (req "^1.2") (default-features #t) (kind 0)))) (hash "141bp0lwhk1i6nzyl5spb6fcdhxg293jpkwvjzrpn0k4jfvbkn3v")))

