(define-module (crates-io bs p_) #:use-module (crates-io))

(define-public crate-bsp_rs-0.1 (crate (name "bsp_rs") (vers "0.1.0") (deps (list (crate-dep (name "binrw") (req "^0.12.0") (default-features #t) (kind 0)) (crate-dep (name "com_goldsrc_formats") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "image") (req "^0.24.7") (features (quote ("tga"))) (kind 0)))) (hash "14qnisxvc8f9aln8clj53zh9s25czqankr8b5k51f404jqis8jrd")))

