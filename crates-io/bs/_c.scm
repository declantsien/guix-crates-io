(define-module (crates-io bs _c) #:use-module (crates-io))

(define-public crate-bs_crate-0.1 (crate (name "bs_crate") (vers "0.1.0") (hash "0gfk3xk4lk5gsp8301bf1j1kpw0a30wglbspqii3k39ijar089nx")))

(define-public crate-bs_crate-0.1 (crate (name "bs_crate") (vers "0.1.1") (hash "1l43mlwpmy5rbf4vbywc4cnpsnlsg9rqjlnzsp0cg0hxk1byzpjl")))

(define-public crate-bs_crate-0.1 (crate (name "bs_crate") (vers "0.1.2") (hash "0qbn7c5qlwhj2r2djc29f24nlx76kpg21y893by0clc0a3jmvjnz")))

(define-public crate-bs_crate-0.1 (crate (name "bs_crate") (vers "0.1.3") (hash "0lcixzyl2fqpfmkacng04bjzglkl2mw4lwcc22nn03is3j2npl23")))

(define-public crate-bs_crate-0.1 (crate (name "bs_crate") (vers "0.1.4") (hash "06wink8dikr93f523ml28rlqxphll1hax3cswgigjsl0fg9f9qrn")))

(define-public crate-bs_crate-0.1 (crate (name "bs_crate") (vers "0.1.5") (hash "1dh8fdvgj26nzwcpyqghw7mgz9azb80wxq56nr6ixbbl1459wzin")))

