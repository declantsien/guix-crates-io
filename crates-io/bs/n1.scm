(define-module (crates-io bs n1) #:use-module (crates-io))

(define-public crate-bsn1-0.1 (crate (name "bsn1") (vers "0.1.0") (hash "0vsnq6mqpqf57hgc5i97w57abjy0x40hfsqc7zz7j4yfahr4ga12")))

(define-public crate-bsn1-0.2 (crate (name "bsn1") (vers "0.2.0") (hash "06jqfn0fgs7pxxn4qgr6wrhg2vyms81yd1pr4sk2fp9g9vq6flyi")))

(define-public crate-bsn1-0.3 (crate (name "bsn1") (vers "0.3.0") (hash "142sxignqym8vcm1qrklisqy91y4f8xnq0vk0c65dgkzqys0jplb")))

(define-public crate-bsn1-0.3 (crate (name "bsn1") (vers "0.3.1") (hash "0bsp85zzlpbsjrdifqq7pqw4zg40yr9hfhwbyv33rpbxkyc078b3")))

(define-public crate-bsn1-0.4 (crate (name "bsn1") (vers "0.4.0") (deps (list (crate-dep (name "num") (req "^0.4.0") (default-features #t) (kind 0)))) (hash "0ypw7b0r8b7hmc3a7zc68kvsyafqzzz0882wda6f031mxxp6b2pc")))

(define-public crate-bsn1-0.9 (crate (name "bsn1") (vers "0.9.0") (deps (list (crate-dep (name "num") (req "^0.4.0") (default-features #t) (kind 0)))) (hash "1qbgv9qhgy3h3y53byid8gl4r9dqms2wi7y7yif5h6qzdxc848rb")))

(define-public crate-bsn1-1 (crate (name "bsn1") (vers "1.0.0") (deps (list (crate-dep (name "num") (req "^0.4.0") (default-features #t) (kind 0)))) (hash "0l95i7dkj0plvgwjfdny639icxzkzdwisw651q8rsy4zi3hxigid")))

(define-public crate-bsn1-2 (crate (name "bsn1") (vers "2.0.0") (deps (list (crate-dep (name "num") (req "^0.4.1") (default-features #t) (kind 0)))) (hash "0lmk72k9s43cac4s4wym5c6wnc5px9dimff9kj7vch7f1fqhp877")))

(define-public crate-bsn1-2 (crate (name "bsn1") (vers "2.0.1") (deps (list (crate-dep (name "num") (req "^0.4.1") (default-features #t) (kind 0)))) (hash "0na27v3v3agaazpls9pjgfl9j75jy46j11vb1zd46957xfin1jhp")))

(define-public crate-bsn1-3 (crate (name "bsn1") (vers "3.0.0") (deps (list (crate-dep (name "anyhow") (req "^1.0.81") (default-features #t) (kind 0)) (crate-dep (name "num") (req "^0.4.1") (default-features #t) (kind 0)))) (hash "1ircdcfkjj456v82s645r9v1id7iv86ah0dkivqja7cqvfhivsgz")))

(define-public crate-bsn1_serde-0.1 (crate (name "bsn1_serde") (vers "0.1.0") (deps (list (crate-dep (name "bsn1") (req "^2.0.0") (default-features #t) (kind 0)) (crate-dep (name "bsn1_serde_macros") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "trybuild") (req "^1.0.85") (default-features #t) (kind 2)))) (hash "121c214bd81dpzd5baq7kxnfnn45947zrwv6v3dl656rfdi0kx12")))

(define-public crate-bsn1_serde-0.2 (crate (name "bsn1_serde") (vers "0.2.0") (deps (list (crate-dep (name "anyhow") (req "^1.0.81") (default-features #t) (kind 0)) (crate-dep (name "bsn1") (req "^3.0.0") (default-features #t) (kind 0)) (crate-dep (name "bsn1_serde_macros") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "trybuild") (req "^1.0.85") (default-features #t) (kind 2)))) (hash "0gqx5pxgpxbadmxcj7fn4kb25mnzjf2g7lfa0w4jfblmbi5rax4g")))

(define-public crate-bsn1_serde_macros-0.1 (crate (name "bsn1_serde_macros") (vers "0.1.0") (deps (list (crate-dep (name "proc-macro2") (req "^1.0.72") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.33") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2.0.43") (default-features #t) (kind 0)))) (hash "0zlnx7dx7izbi1rlxchlqj3rwy7f6pf68ap0pp923n3nl68s11fi")))

(define-public crate-bsn1_serde_macros-0.2 (crate (name "bsn1_serde_macros") (vers "0.2.0") (deps (list (crate-dep (name "proc-macro2") (req "^1.0.72") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.33") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2.0.43") (default-features #t) (kind 0)))) (hash "11vl8dhzx9n85h4h8214fi4phzdl4bgzxi7xcc240l9c8jfw89xn")))

