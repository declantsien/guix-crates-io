(define-module (crates-io bs at) #:use-module (crates-io))

(define-public crate-bsatool_rs-0.1 (crate (name "bsatool_rs") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^2.27.1") (default-features #t) (kind 0)) (crate-dep (name "indicatif") (req "^0.14.0") (default-features #t) (kind 0)))) (hash "0zrw8h2n6m1b8px15sf0zcbmbv085n781cziypwzqvccckvq1yxr")))

(define-public crate-bsatool_rs-0.1 (crate (name "bsatool_rs") (vers "0.1.1") (deps (list (crate-dep (name "clap") (req "^2.27.1") (default-features #t) (kind 0)) (crate-dep (name "indicatif") (req "^0.14.0") (default-features #t) (kind 0)))) (hash "19bif61xx4yw5gsgm5czahxkz7gss2advjbcxnnzzr3glmbbix1a")))

(define-public crate-bsatool_rs-0.1 (crate (name "bsatool_rs") (vers "0.1.2") (deps (list (crate-dep (name "clap") (req "^2.27.1") (default-features #t) (kind 0)) (crate-dep (name "indicatif") (req "^0.14.0") (default-features #t) (kind 0)))) (hash "07alis9k7q96kh50rh2x8p468jxf1331imzjm7h957ys2y7xp1in")))

(define-public crate-bsatool_rs-0.1 (crate (name "bsatool_rs") (vers "0.1.3") (deps (list (crate-dep (name "clap") (req "^2.27.1") (default-features #t) (kind 0)) (crate-dep (name "indicatif") (req "^0.14.0") (default-features #t) (kind 0)))) (hash "1izjbhcwxl0nqha9a1p81f80s1nmrrxjj80basc7dj8xkgnwn8d3")))

(define-public crate-bsatool_rs-0.1 (crate (name "bsatool_rs") (vers "0.1.4") (deps (list (crate-dep (name "clap") (req "^2.27.1") (default-features #t) (kind 0)) (crate-dep (name "indicatif") (req "^0.14.0") (default-features #t) (kind 0)))) (hash "0anw01pnp7s67z8pjn5j7dcy3znzndma0ck9pmgg296wprw39zhq")))

(define-public crate-bsatool_rs-0.2 (crate (name "bsatool_rs") (vers "0.2.0") (deps (list (crate-dep (name "anyhow") (req "^1.0.71") (default-features #t) (kind 0)) (crate-dep (name "bsatoollib") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^3.1.18") (default-features #t) (kind 0)) (crate-dep (name "indicatif") (req "^0.16.0") (default-features #t) (kind 0)) (crate-dep (name "tabled") (req "^0.12.2") (default-features #t) (kind 0)))) (hash "1jv48r2xhzp8dgf694kz0ilh62g0nfy46ahkzf7zj3r5wyp6l0wk")))

(define-public crate-bsatool_rs-0.3 (crate (name "bsatool_rs") (vers "0.3.1") (deps (list (crate-dep (name "anyhow") (req "^1.0.71") (default-features #t) (kind 0)) (crate-dep (name "bsatoollib") (req "^0.2.1") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^4.3.8") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "indicatif") (req "^0.16.0") (default-features #t) (kind 0)) (crate-dep (name "tabled") (req "^0.12.2") (default-features #t) (kind 0)))) (hash "0pzbblzrq4c60h545ig9m21726awxzla2fqvwshy2ha0jfyrfkn3")))

(define-public crate-bsatoollib-0.1 (crate (name "bsatoollib") (vers "0.1.0") (deps (list (crate-dep (name "thiserror") (req "^1.0.40") (default-features #t) (kind 0)))) (hash "0n7cakd02i38n7smiihv3vzl26p2bbfiv2girhqfa25jx0x1qmcv")))

(define-public crate-bsatoollib-0.2 (crate (name "bsatoollib") (vers "0.2.0") (deps (list (crate-dep (name "thiserror") (req "^1.0.40") (default-features #t) (kind 0)))) (hash "0ddcf0mvlnxpzbhw9ys3fybnhlfcwlfymsczdphxdm0i3cfdw770")))

(define-public crate-bsatoollib-0.2 (crate (name "bsatoollib") (vers "0.2.1") (deps (list (crate-dep (name "thiserror") (req "^1.0.40") (default-features #t) (kind 0)))) (hash "1c8jrmyrbj04zal5zch75wlfk1d5m837qivmzdl60a9j7vchqgyh")))

