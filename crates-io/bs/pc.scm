(define-module (crates-io bs pc) #:use-module (crates-io))

(define-public crate-bspc-0.4 (crate (name "bspc") (vers "0.4.0") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "unix_socket") (req "^0.5") (default-features #t) (kind 0)))) (hash "178bb4dk46rr20ihdmgkqw8q572a1f76plyxsprirm6862cg91iw")))

(define-public crate-bspc-0.4 (crate (name "bspc") (vers "0.4.1") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "unix_socket") (req "^0.5") (default-features #t) (kind 0)))) (hash "0sxipvxkc15rli7awfja8krn3wjah19i3d9shxav6lwx46rz7wrk")))

(define-public crate-bspc-0.4 (crate (name "bspc") (vers "0.4.2") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "unix_socket") (req "^0.5") (default-features #t) (kind 0)))) (hash "0inlh4sh03qb76nh30y47xvy2lcpqn9z160jd7n8ycza04484az9")))

(define-public crate-bspc-0.4 (crate (name "bspc") (vers "0.4.3") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "unix_socket") (req "^0.5") (default-features #t) (kind 0)))) (hash "19hi8mpqq3zl4dj5b112sx6ppfcl214d3p7jck5p9y5bwsy6vk0l")))

(define-public crate-bspc-rs-0.1 (crate (name "bspc-rs") (vers "0.1.0") (deps (list (crate-dep (name "serde") (req "^1.0.152") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.94") (default-features #t) (kind 0)) (crate-dep (name "strum") (req "^0.24") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "strum_macros") (req "^0.24") (default-features #t) (kind 0)))) (hash "1vz46n2zlhzld53w2kvydyzf2pmpiybhsan25sccd377g1hzk7r7")))

(define-public crate-bspc-rs-0.1 (crate (name "bspc-rs") (vers "0.1.1") (deps (list (crate-dep (name "serde") (req "^1.0.152") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.94") (default-features #t) (kind 0)) (crate-dep (name "strum") (req "^0.24") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "strum_macros") (req "^0.24") (default-features #t) (kind 0)) (crate-dep (name "doc-comment") (req "^0.3.3") (default-features #t) (kind 2)))) (hash "1an3srxn15l1ndhspnfgg0m4i837138mm8gl61mf9dprzfvc1lds")))

