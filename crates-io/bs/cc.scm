(define-module (crates-io bs cc) #:use-module (crates-io))

(define-public crate-bsccontract-diff-0.1 (crate (name "bsccontract-diff") (vers "0.1.0") (deps (list (crate-dep (name "console") (req "^0.15.0") (default-features #t) (kind 0)) (crate-dep (name "futures") (req "^0.3.21") (default-features #t) (kind 0)) (crate-dep (name "hex") (req "^0.4.3") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.5.5") (default-features #t) (kind 0)) (crate-dep (name "similar") (req "^2.1.0") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.17.0") (features (quote ("full"))) (default-features #t) (kind 0)) (crate-dep (name "web3") (req "^0.18.0") (default-features #t) (kind 0)))) (hash "0lbpgxh2bwlllma8c8rcdgxxpw0la1iwnams0ddkl8s8nwp3qsz9")))

