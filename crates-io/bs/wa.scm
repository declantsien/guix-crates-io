(define-module (crates-io bs wa) #:use-module (crates-io))

(define-public crate-bswap-0.1 (crate (name "bswap") (vers "0.1.4") (hash "0r3xqv1mz1k7li8g25939rkb9f35s6bgikl9ljs1pldwmj935aik")))

(define-public crate-bswap-0.1 (crate (name "bswap") (vers "0.1.5") (hash "0z4f8fzh18q963apybyhg5whhggy62xp33acbchaqfz41jqhgp5z")))

(define-public crate-bswap-0.1 (crate (name "bswap") (vers "0.1.6") (hash "02cw3xmjnr2ldqphknks746wmpasqdki31k2l7f1r7yva7m2k0by")))

(define-public crate-bswap-0.1 (crate (name "bswap") (vers "0.1.7") (hash "1y2936n46l5av1v6hzf59dpycpjafaxrkn7h17qi7fl3v70fy5z1")))

(define-public crate-bswap-0.1 (crate (name "bswap") (vers "0.1.8") (hash "0krf1c21wdc5mkcj0ph3ai9rlrvmx77kiqqrixzf7x3f1wh857if")))

(define-public crate-bswap-0.2 (crate (name "bswap") (vers "0.2.0") (hash "0gkirni6l3r6fjmfryzpicg8b6ipvbb47i5qn9ahlxw8qw403g5l")))

(define-public crate-bswap-0.2 (crate (name "bswap") (vers "0.2.1") (hash "06lnwgmbdagk8r7xm73gz4yhk62d2pinhs64yv3qr0pz19fdasmp")))

(define-public crate-bswap-0.2 (crate (name "bswap") (vers "0.2.2") (hash "00zgpqcc3cvkswjbaszs0w6r085dv3dlk3c6bp4mpdrcsl4lc6sj")))

(define-public crate-bswap-0.4 (crate (name "bswap") (vers "0.4.0") (hash "1slfnhqn445qw2pqbrbzayrzf6kia4n3hvzi46k3l4i0v9ijxp0s")))

(define-public crate-bswap-1 (crate (name "bswap") (vers "1.0.0") (hash "1xbjph85zabbyxn6av6b06gwi5gg13wi6zw73gr8vrk0kk7cbb73")))

