(define-module (crates-io bs c-) #:use-module (crates-io))

(define-public crate-bsc-cli-0.2 (crate (name "bsc-cli") (vers "0.2.0") (deps (list (crate-dep (name "bsc") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^4.1.6") (features (quote ("derive" "env" "wrap_help"))) (default-features #t) (kind 0)) (crate-dep (name "eyre") (req "^0.6.8") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.93") (default-features #t) (kind 0)) (crate-dep (name "simple-eyre") (req "^0.3.1") (default-features #t) (kind 0)))) (hash "0mxvr3w9vz1kgf2dl98mlnqiz3fvmgmn7nq4l7d3jrhlrqs7vssg")))

