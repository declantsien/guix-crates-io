(define-module (crates-io qd ft) #:use-module (crates-io))

(define-public crate-qdft-0.1 (crate (name "qdft") (vers "0.1.0") (deps (list (crate-dep (name "ndarray") (req "0.15.*") (default-features #t) (kind 2)) (crate-dep (name "ndarray-npy") (req "0.8.*") (default-features #t) (kind 2)) (crate-dep (name "num") (req "0.4.*") (default-features #t) (kind 0)))) (hash "02iacjxwr4gxj8wzgsxq56m2g79280vxdgkvm60s2idw0pv200f9")))

