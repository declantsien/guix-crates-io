(define-module (crates-io qd b-) #:use-module (crates-io))

(define-public crate-qdb-storage-0.1 (crate (name "qdb-storage") (vers "0.1.0") (deps (list (crate-dep (name "futures") (req "^0.3.4") (features (quote ("alloc"))) (default-features #t) (kind 0)))) (hash "1m696d0lmlw6n23fgpqy370hg5mmxxaaxch22wb8ia987bygzin1") (yanked #t)))

