(define-module (crates-io os b-) #:use-module (crates-io))

(define-public crate-osb-pono-0.1 (crate (name "osb-pono") (vers "0.1.0") (hash "0b541f8b8ywkka6mn9qxarwdapi83vbnxzrj8g8j4g8n30vfldvh")))

(define-public crate-osb-pono-0.1 (crate (name "osb-pono") (vers "0.1.1") (deps (list (crate-dep (name "osb") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "178fh1mma7bj7bkpan8ncakfv273wr88p0x94n419qfm8ygx1ld9")))

(define-public crate-osb-pono-0.1 (crate (name "osb-pono") (vers "0.1.2") (deps (list (crate-dep (name "osb") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "19k8jsmczz07qakfymp6qid412kmrg25ngcyfcfmdyrxcbm2y7bn")))

