(define-module (crates-io os -x) #:use-module (crates-io))

(define-public crate-os-xtask-utils-0.0.0 (crate (name "os-xtask-utils") (vers "0.0.0") (deps (list (crate-dep (name "once_cell") (req "^1.15") (default-features #t) (kind 0)))) (hash "1gps1z5avqpf7c40qxy5dn41k8jsgwxji7p0k41q947ngg6wrsgm")))

