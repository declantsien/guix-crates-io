(define-module (crates-io os _t) #:use-module (crates-io))

(define-public crate-os_template-0.1 (crate (name "os_template") (vers "0.1.0") (deps (list (crate-dep (name "bootloader") (req "^0.9") (default-features #t) (kind 0)))) (hash "114mvy757ci6zlzi0s5xjcjqa1gkkiy8gd0c7cr7ri4zkis252m8")))

(define-public crate-os_template-0.1 (crate (name "os_template") (vers "0.1.1") (deps (list (crate-dep (name "bootloader") (req "^0.9") (default-features #t) (kind 0)))) (hash "1b9x8gkdly4y8rgi35753adrw46xyq0qy4340hyicl0sdk0yp4kb")))

(define-public crate-os_type-0.1 (crate (name "os_type") (vers "0.1.0") (hash "0lh0sfg8bgks5b6ph4a8j1if6br7wzwyn9sz953a330i0z00j0ck")))

(define-public crate-os_type-0.1 (crate (name "os_type") (vers "0.1.1") (hash "164087z8vmhll5zz06gk1bmjpd1080bvx8bnzggn72j7s6slcmj5")))

(define-public crate-os_type-0.2 (crate (name "os_type") (vers "0.2.0") (deps (list (crate-dep (name "regex") (req "^0.1.41") (default-features #t) (kind 0)))) (hash "1lc6svzzp6lqp0p62g2d2g1v27h08jnbz6vamkm48ysxjwfcpm1b")))

(define-public crate-os_type-0.2 (crate (name "os_type") (vers "0.2.1") (deps (list (crate-dep (name "regex") (req "^0.1.41") (default-features #t) (kind 0)))) (hash "1fn25d46ji8jnypkyjma5xbwls7fcnlhnffdn0mjq8lism1a5z7h")))

(define-public crate-os_type-0.2 (crate (name "os_type") (vers "0.2.2") (deps (list (crate-dep (name "regex") (req "^0.1.41") (default-features #t) (kind 0)))) (hash "0281pl6kk95mv3mgw72disx48gp6wkbvij6g31v9ghqzrhv8zrvp")))

(define-public crate-os_type-0.4 (crate (name "os_type") (vers "0.4.0") (deps (list (crate-dep (name "regex") (req "^0.1.41") (default-features #t) (kind 0)))) (hash "08fbs9afcbnri6j9j1ssmk7s1k05mvxj1v9fpxy1cpwkrmp0a16q")))

(define-public crate-os_type-0.5 (crate (name "os_type") (vers "0.5.0") (deps (list (crate-dep (name "regex") (req "^0.1.41") (default-features #t) (kind 0)))) (hash "0h0r5idfhwsi026c0gm7jhk777mz7qnysvmiciyy80x7jzd1d999")))

(define-public crate-os_type-0.6 (crate (name "os_type") (vers "0.6.0") (deps (list (crate-dep (name "regex") (req "^0.1.41") (default-features #t) (kind 0)))) (hash "0yp7smyn95v1qbj9l3z58nv5w7kx7sh51xh1pfzfla0l78w8y3ln")))

(define-public crate-os_type-1 (crate (name "os_type") (vers "1.0.0") (deps (list (crate-dep (name "regex") (req "^0.1.41") (default-features #t) (kind 0)))) (hash "0qc4ch3fkr15dmw8gra8hpflsm4hgqsywzshbxbanl9ypbv9hvmq")))

(define-public crate-os_type-2 (crate (name "os_type") (vers "2.0.0") (deps (list (crate-dep (name "regex") (req "^0.2.1") (default-features #t) (kind 0)))) (hash "028n8bbxzlab4mmvw5x1kplqbdnhibljzmz0xs7n64bnylwha7q8")))

(define-public crate-os_type-2 (crate (name "os_type") (vers "2.1.1") (deps (list (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)))) (hash "0qdw36nk0gy9c9rljrp3wjr7zfhxnclmxyxjwsziy1gl9jj2dxzd")))

(define-public crate-os_type-2 (crate (name "os_type") (vers "2.2.0") (deps (list (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)))) (hash "1sz90xi9br82zmp3s0fbp397ajm5f0xsir7pikwbg65fy0d03p3y")))

(define-public crate-os_type-2 (crate (name "os_type") (vers "2.3.0") (deps (list (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)))) (hash "146zx5mh84n5p2xhlwb8b75wbkqw87fvb87n3adl44lz5pifpsln")))

(define-public crate-os_type-2 (crate (name "os_type") (vers "2.4.0") (deps (list (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)))) (hash "1293gl2x38q7bhk3f7155k1aq0nvhq6vdksgz1cq6abhchgpdpy3")))

(define-public crate-os_type-2 (crate (name "os_type") (vers "2.5.0") (deps (list (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)))) (hash "1p3fs2qqwq2dl1lprr42qpp0aah7rb84qmk85fjv11cpqpbjvi6c")))

(define-public crate-os_type-2 (crate (name "os_type") (vers "2.6.0") (deps (list (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)))) (hash "19bv5jq9z04bw3kf9qdxw76yngjy9g5dmxnqdr8nf0d3xv048kg2")))

