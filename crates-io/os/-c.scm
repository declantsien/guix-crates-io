(define-module (crates-io os -c) #:use-module (crates-io))

(define-public crate-os-core-0.1 (crate (name "os-core") (vers "0.1.0") (deps (list (crate-dep (name "libc") (req "^0.2.121") (default-features #t) (kind 0)))) (hash "1a2k9gxcvixs5z9g62i0bz41g0rk5l2h2qfqdpkscnch38cwpnv5")))

(define-public crate-os-core-0.2 (crate (name "os-core") (vers "0.2.0") (deps (list (crate-dep (name "libc") (req "^0.2.121") (default-features #t) (kind 0)))) (hash "1vl2qf40bhzrc93jsz3m14d38f5zpxnirhnncqs78m5f62230957")))

