(define-module (crates-io os _d) #:use-module (crates-io))

(define-public crate-os_display-0.1 (crate (name "os_display") (vers "0.1.0") (deps (list (crate-dep (name "unicode-width") (req "^0.1.9") (default-features #t) (kind 0)))) (hash "1p72p0jyiy6p6kv6mxlfxivdxzww61wbzgxyd47qamdz48p06in3") (features (quote (("windows") ("unix") ("std" "alloc") ("native") ("default" "native" "alloc" "std") ("alloc"))))))

(define-public crate-os_display-0.1 (crate (name "os_display") (vers "0.1.1") (deps (list (crate-dep (name "unicode-width") (req "^0.1.9") (default-features #t) (kind 0)))) (hash "1kyqfx7lz4lvhk8y15gwdr0w4skbhacxd9dzf4bq59r451rxfx4f") (features (quote (("windows") ("unix") ("std" "alloc") ("native") ("default" "native" "alloc" "std") ("alloc"))))))

(define-public crate-os_display-0.1 (crate (name "os_display") (vers "0.1.2") (deps (list (crate-dep (name "unicode-width") (req "^0.1.9") (default-features #t) (kind 0)))) (hash "19f76bbhwz6f71mif0905q68m327qp48vpdyllb7692mvk8c333l") (features (quote (("windows") ("unix") ("std" "alloc") ("native") ("default" "native" "alloc" "std") ("alloc"))))))

(define-public crate-os_display-0.1 (crate (name "os_display") (vers "0.1.3") (deps (list (crate-dep (name "unicode-width") (req "^0.1.9") (default-features #t) (kind 0)))) (hash "0xfgfqvfg5nyidv5p85fb87l0mif1nniisxarw6npd4jv2x2jqks") (features (quote (("windows") ("unix") ("std" "alloc") ("native") ("default" "native" "alloc" "std") ("alloc"))))))

