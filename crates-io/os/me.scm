(define-module (crates-io os me) #:use-module (crates-io))

(define-public crate-osmesa-sys-0.0.0 (crate (name "osmesa-sys") (vers "0.0.0") (deps (list (crate-dep (name "gl") (req "*") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "*") (default-features #t) (kind 0)))) (hash "09622h4rbqa3m4rz7qb2l4vxwg0samzmq59d3q6z3ab6f2bfpmyg")))

(define-public crate-osmesa-sys-0.0.1 (crate (name "osmesa-sys") (vers "0.0.1") (deps (list (crate-dep (name "gl") (req "*") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "*") (default-features #t) (kind 0)))) (hash "0vvny6mq0gmqkwmj48ayxzfxp06jhgdsiw03pp2mz2rh688jj6xl")))

(define-public crate-osmesa-sys-0.0.2 (crate (name "osmesa-sys") (vers "0.0.2") (deps (list (crate-dep (name "gl") (req "*") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "*") (default-features #t) (kind 0)) (crate-dep (name "pkg-config") (req "*") (default-features #t) (kind 1)))) (hash "0hgvdi9q20fzhn13im1pzk37rbpfmwrffsfi4zrm0zmp19jgg76h") (features (quote (("force-link"))))))

(define-public crate-osmesa-sys-0.0.3 (crate (name "osmesa-sys") (vers "0.0.3") (deps (list (crate-dep (name "gl") (req "*") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "*") (default-features #t) (kind 0)) (crate-dep (name "pkg-config") (req "*") (default-features #t) (kind 1)))) (hash "1pcjhza5nrcsw21jj69gjfwn2k4y5mvngjlccxf7727bfm7a0ds9")))

(define-public crate-osmesa-sys-0.0.4 (crate (name "osmesa-sys") (vers "0.0.4") (deps (list (crate-dep (name "libc") (req "*") (default-features #t) (kind 0)) (crate-dep (name "shared_library") (req "^0.1") (default-features #t) (kind 0)))) (hash "10161qbiji7zlp9x1j38dkavjdb9yzw3ia6k3xb4qcqdw58qq9w2")))

(define-public crate-osmesa-sys-0.0.5 (crate (name "osmesa-sys") (vers "0.0.5") (deps (list (crate-dep (name "libc") (req "*") (default-features #t) (kind 0)) (crate-dep (name "shared_library") (req "^0.1") (default-features #t) (kind 0)))) (hash "179nqpaxaz1x48gs365v5cmwm76849n2rpw0q92ms9gsf26jsp72")))

(define-public crate-osmesa-sys-0.0.6 (crate (name "osmesa-sys") (vers "0.0.6") (deps (list (crate-dep (name "libc") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "shared_library") (req "^0.1") (default-features #t) (kind 0)))) (hash "0fgvw9pshzr0i174qc4lj112fnys6xi442n78palxgjsr9b5hna6")))

(define-public crate-osmesa-sys-0.1 (crate (name "osmesa-sys") (vers "0.1.0") (deps (list (crate-dep (name "shared_library") (req "^0.1") (default-features #t) (kind 0)))) (hash "0hmvi2f95qh51g9bf380pjqx03478b9g0jkxxa4fd5wc9262552n")))

(define-public crate-osmesa-sys-0.1 (crate (name "osmesa-sys") (vers "0.1.1") (deps (list (crate-dep (name "shared_library") (req "^0.1") (default-features #t) (kind 0)))) (hash "13c2iwphkvzn7b1vwqs21g4mkfgvq96hsbxmdfmwsd8ljy90615h")))

(define-public crate-osmesa-sys-0.1 (crate (name "osmesa-sys") (vers "0.1.2") (deps (list (crate-dep (name "shared_library") (req "^0.1") (default-features #t) (kind 0)))) (hash "0fq1q1zcgfb0qydrg9r2738jlwc4hqxgb9vj11z72bjxx7kfrkw8")))

