(define-module (crates-io os pf) #:use-module (crates-io))

(define-public crate-ospf-parser-0.1 (crate (name "ospf-parser") (vers "0.1.0") (deps (list (crate-dep (name "hex-literal") (req "^0.2") (default-features #t) (kind 2)) (crate-dep (name "nom") (req "^5.1") (default-features #t) (kind 0)) (crate-dep (name "nom-derive") (req "^0.5") (default-features #t) (kind 0)) (crate-dep (name "rusticata-macros") (req "^2.0.3") (default-features #t) (kind 0)))) (hash "1lv2l5jk8fwccvy6q7gj6cbm9sc7yzs2iwqzwz9ja15c7ljh6lnd")))

(define-public crate-ospf-parser-0.2 (crate (name "ospf-parser") (vers "0.2.0") (deps (list (crate-dep (name "hex-literal") (req "^0.2") (default-features #t) (kind 2)) (crate-dep (name "nom") (req "^5.1") (default-features #t) (kind 0)) (crate-dep (name "nom-derive") (req "^0.6.0") (default-features #t) (kind 0)) (crate-dep (name "rusticata-macros") (req "^2.0.3") (default-features #t) (kind 0)))) (hash "1nislcn4ir67j9hb7w2skf5gs69ga4x50pfzgnrnmp4354g3ldbi")))

(define-public crate-ospf-parser-0.3 (crate (name "ospf-parser") (vers "0.3.0") (deps (list (crate-dep (name "hex-literal") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "nom") (req "^6.0") (default-features #t) (kind 0)) (crate-dep (name "nom-derive") (req "^0.7") (default-features #t) (kind 0)) (crate-dep (name "rusticata-macros") (req "^3.0") (default-features #t) (kind 0)))) (hash "0lx5hmzfcjawhm0an4vy9aj64db5a2871qk36m9qbqkys5qs67vz")))

(define-public crate-ospf-parser-0.4 (crate (name "ospf-parser") (vers "0.4.0") (deps (list (crate-dep (name "hex-literal") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "nom") (req "^6.0") (default-features #t) (kind 0)) (crate-dep (name "nom-derive") (req "^0.9") (default-features #t) (kind 0)) (crate-dep (name "rusticata-macros") (req "^3.0") (default-features #t) (kind 0)))) (hash "0nda9jf2idx7kb9gc4ffj692x7d682pbhffmrya8q29yi4kf0h8j")))

(define-public crate-ospf-parser-0.5 (crate (name "ospf-parser") (vers "0.5.0") (deps (list (crate-dep (name "hex-literal") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "nom") (req "^7.0") (default-features #t) (kind 0)) (crate-dep (name "nom-derive") (req "^0.10") (default-features #t) (kind 0)) (crate-dep (name "rusticata-macros") (req "^4.0") (default-features #t) (kind 0)))) (hash "1vxfs0igvs1aczfic41ac5m4fv1ips6njjachkaja0afl6fwfqih")))

