(define-module (crates-io os c5) #:use-module (crates-io))

(define-public crate-osc52-0.1 (crate (name "osc52") (vers "0.1.0") (deps (list (crate-dep (name "anyhow") (req "^1.0.72") (default-features #t) (kind 0)) (crate-dep (name "base64") (req "^0.21.2") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^4.3.21") (features (quote ("derive" "env"))) (default-features #t) (kind 0)) (crate-dep (name "termios") (req "^0.3.3") (default-features #t) (kind 0)))) (hash "06swpqr40b7cxix41p785014m2asj9hn9572570yc0zfdjw6y5dv")))

