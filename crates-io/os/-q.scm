(define-module (crates-io os -q) #:use-module (crates-io))

(define-public crate-os-query-builder-rs-0.1 (crate (name "os-query-builder-rs") (vers "0.1.0") (deps (list (crate-dep (name "serde") (req "^1.0.190") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.107") (default-features #t) (kind 0)))) (hash "1c9wv2s7kvxmjx825j18sfhj990dvs8d105bxr6ps371wdlv6rb4")))

(define-public crate-os-query-builder-rs-0.1 (crate (name "os-query-builder-rs") (vers "0.1.1") (deps (list (crate-dep (name "serde") (req "^1.0.190") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.107") (default-features #t) (kind 0)))) (hash "0f1xiwj7cpwgqz12w6xvkbk3gf2q2j41cj5bhi41mra02yw9l2ix")))

(define-public crate-os-query-builder-rs-0.1 (crate (name "os-query-builder-rs") (vers "0.1.2") (deps (list (crate-dep (name "serde") (req "^1.0.190") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.107") (default-features #t) (kind 0)))) (hash "0lfhyqqsbc7gzwl41cnvgvp0qanjfzvb8zb1z8m04p74rapyznbg")))

(define-public crate-os-query-builder-rs-0.1 (crate (name "os-query-builder-rs") (vers "0.1.3") (deps (list (crate-dep (name "serde") (req "^1.0.190") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.107") (default-features #t) (kind 0)))) (hash "0qpfbllgyb53r9r8gdhwkmp043w295x7f7lb4qf81dq0mqb5ir6q")))

(define-public crate-os-query-builder-rs-0.1 (crate (name "os-query-builder-rs") (vers "0.1.4") (deps (list (crate-dep (name "serde") (req "^1.0.190") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.107") (default-features #t) (kind 0)))) (hash "1pbjb5zq68mmr4a5x8p2l95rf8csh0l67qv76imxyfyh1l75k4p2")))

(define-public crate-os-query-builder-rs-0.1 (crate (name "os-query-builder-rs") (vers "0.1.5") (deps (list (crate-dep (name "serde") (req "^1.0.190") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.107") (default-features #t) (kind 0)))) (hash "0dc5j17llvpi6c52jmmcm81pix2y43d7ygg8q17nbmj3gafcxhlz")))

