(define-module (crates-io os _p) #:use-module (crates-io))

(define-public crate-os_path-0.1 (crate (name "os_path") (vers "0.1.0") (deps (list (crate-dep (name "serde") (req "^1.0.163") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "07l9b72cn50invqqsm4r11yiyaaqnyckkmw3r2bspm5yir1bh9gs")))

(define-public crate-os_path-0.2 (crate (name "os_path") (vers "0.2.0") (deps (list (crate-dep (name "serde") (req "^1.0.163") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "1fa3k437gpcfi51sq8l669fdfjp5ad2rk5090an4afg97bfgk22q")))

(define-public crate-os_path-0.2 (crate (name "os_path") (vers "0.2.1") (deps (list (crate-dep (name "serde") (req "^1.0.163") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "1c3775s039w1q1xcfxbmd0kn2lhjgn9m7m138a8hd9x2vb6ap0w6")))

(define-public crate-os_path-0.2 (crate (name "os_path") (vers "0.2.2") (deps (list (crate-dep (name "serde") (req "^1.0.163") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "0qva3lh5n9dr4qvghv0pgrclh7zm4bryxw44fj0667zvxra4sf2a")))

(define-public crate-os_path-0.2 (crate (name "os_path") (vers "0.2.3") (deps (list (crate-dep (name "serde") (req "^1.0.163") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "12wrk4ry0679x49kk608jvgv2gd2i9w6lfmbd5j868v7njpjl00f")))

(define-public crate-os_path-0.3 (crate (name "os_path") (vers "0.3.0") (deps (list (crate-dep (name "serde") (req "^1.0.163") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "0adkb542sqlw9wb6f6gxjdgzdbni7dam1gc6kvg7rzbmqnkkr72x")))

(define-public crate-os_path-0.4 (crate (name "os_path") (vers "0.4.0") (deps (list (crate-dep (name "serde") (req "^1.0.163") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "0pzdjc9mz4ddxqwnsfdsj52s6ipnrinh4j0l0qjylgbra8m3nfg6")))

(define-public crate-os_path-0.5 (crate (name "os_path") (vers "0.5.0") (deps (list (crate-dep (name "serde") (req "^1.0.163") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "0bdsp91wiqhqnk3510k882q1xzx0qnc67flg7f1z4j1zmqbvbllm")))

(define-public crate-os_path-0.6 (crate (name "os_path") (vers "0.6.0") (deps (list (crate-dep (name "serde") (req "^1.0.163") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "1d54ywpbyxk6nk8xjzipzqxdjdmqxiwl4380rlrxlnpdw0r0wdik")))

(define-public crate-os_path-0.6 (crate (name "os_path") (vers "0.6.1") (deps (list (crate-dep (name "serde") (req "^1.0.163") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "03ngiaxqa7wzzjcsljykr83q0mni23mpg4pjzjbkdrgpxl6fbpaf")))

(define-public crate-os_path-0.6 (crate (name "os_path") (vers "0.6.3") (deps (list (crate-dep (name "serde") (req "^1.0.163") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "0hr8nz8wf3cqxwzdwfcmaljwhg35gxhh6ayhxd7z35rs1sj80g3a")))

(define-public crate-os_path-0.6 (crate (name "os_path") (vers "0.6.4") (deps (list (crate-dep (name "serde") (req "^1.0.163") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "0inlp7zd5fsz6066jz28kg753ivxdap4sm99nl01nahgvcdjk59z")))

(define-public crate-os_path-0.7 (crate (name "os_path") (vers "0.7.0") (deps (list (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "16s54x3yaqszp5pl3yvscm1d76xh0jpjk9vdhpjilxfl2yy9vbn0")))

(define-public crate-os_path-0.8 (crate (name "os_path") (vers "0.8.0") (deps (list (crate-dep (name "regex") (req "^1.10.4") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "0sqzfisflr4lzvpfjcw1kjbwzwwwfyl6qxqqmsjlnm4z2b5nw2in")))

(define-public crate-os_pipe-0.1 (crate (name "os_pipe") (vers "0.1.0") (deps (list (crate-dep (name "lazy_static") (req "0.2.*") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "0.2.*") (default-features #t) (kind 0)))) (hash "0y0i4i5c6d6d8w7ys01psx34k4z90fmkjmvf925n1al8w6r3gi4g")))

(define-public crate-os_pipe-0.1 (crate (name "os_pipe") (vers "0.1.1") (deps (list (crate-dep (name "lazy_static") (req "0.2.*") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "0.2.*") (default-features #t) (kind 0)))) (hash "1dklwi1nri7vi6ckpiakcnd8k6v15g52ix8nynjifwfrh04yvadq")))

(define-public crate-os_pipe-0.2 (crate (name "os_pipe") (vers "0.2.0") (deps (list (crate-dep (name "kernel32-sys") (req ">= 0.2.2") (default-features #t) (target "cfg(windows)") (kind 0)) (crate-dep (name "nix") (req ">= 0.7.0") (default-features #t) (target "cfg(not(windows))") (kind 0)) (crate-dep (name "winapi") (req ">= 0.2.8") (default-features #t) (target "cfg(windows)") (kind 0)))) (hash "1laj2cv4px1y1iakkrzm57zpld93g1fhb0aj66lai6m7qwdvj914")))

(define-public crate-os_pipe-0.2 (crate (name "os_pipe") (vers "0.2.1") (deps (list (crate-dep (name "kernel32-sys") (req ">= 0.2.2") (default-features #t) (target "cfg(windows)") (kind 0)) (crate-dep (name "nix") (req ">= 0.7.0") (default-features #t) (target "cfg(not(windows))") (kind 0)) (crate-dep (name "winapi") (req ">= 0.2.8") (default-features #t) (target "cfg(windows)") (kind 0)))) (hash "1j09zf9xs4gv5bhi26pz73nmy54kk993g883sar0gzps5llv3kqy")))

(define-public crate-os_pipe-0.3 (crate (name "os_pipe") (vers "0.3.0") (deps (list (crate-dep (name "kernel32-sys") (req "^0.2.2") (default-features #t) (target "cfg(windows)") (kind 0)) (crate-dep (name "nix") (req "^0.7.0") (default-features #t) (target "cfg(not(windows))") (kind 0)) (crate-dep (name "winapi") (req "^0.2.8") (default-features #t) (target "cfg(windows)") (kind 0)))) (hash "0ax0nx89wfapa26yw0dqdqc7b2rhlz4hfrqvhk3zrykl7icwi7jn")))

(define-public crate-os_pipe-0.4 (crate (name "os_pipe") (vers "0.4.0") (deps (list (crate-dep (name "kernel32-sys") (req "^0.2.2") (default-features #t) (target "cfg(windows)") (kind 0)) (crate-dep (name "nix") (req "^0.7.0") (default-features #t) (target "cfg(not(windows))") (kind 0)) (crate-dep (name "winapi") (req "^0.2.8") (default-features #t) (target "cfg(windows)") (kind 0)))) (hash "1jggdl64np9znjynjj2ng9kfbwnpi3kdjrsz8v646ifq6sh3ad52")))

(define-public crate-os_pipe-0.4 (crate (name "os_pipe") (vers "0.4.1") (deps (list (crate-dep (name "kernel32-sys") (req "^0.2.2") (default-features #t) (target "cfg(windows)") (kind 0)) (crate-dep (name "nix") (req "^0.7.0") (default-features #t) (target "cfg(not(windows))") (kind 0)) (crate-dep (name "winapi") (req "^0.2.8") (default-features #t) (target "cfg(windows)") (kind 0)))) (hash "04aqy9c1c5rkmimn4j2dgfw5r06sn6cca6l4x58ak3mjq8xf5bih")))

(define-public crate-os_pipe-0.5 (crate (name "os_pipe") (vers "0.5.0") (deps (list (crate-dep (name "kernel32-sys") (req "^0.2.2") (default-features #t) (target "cfg(windows)") (kind 0)) (crate-dep (name "nix") (req "^0.8.0") (default-features #t) (target "cfg(not(windows))") (kind 0)) (crate-dep (name "winapi") (req "^0.2.8") (default-features #t) (target "cfg(windows)") (kind 0)))) (hash "1g2ghx0nqryc1fyf1lki4phvxclwbj62brskmf9d3y9b7qs56qfh")))

(define-public crate-os_pipe-0.5 (crate (name "os_pipe") (vers "0.5.1") (deps (list (crate-dep (name "kernel32-sys") (req "^0.2.2") (default-features #t) (target "cfg(windows)") (kind 0)) (crate-dep (name "nix") (req "^0.8.0") (default-features #t) (target "cfg(not(windows))") (kind 0)) (crate-dep (name "winapi") (req "^0.2.8") (default-features #t) (target "cfg(windows)") (kind 0)))) (hash "1ccwc7caq3hhgxyrglkl2fw8qzkx0kxanh9azs852w9f0jrzp2wr")))

(define-public crate-os_pipe-0.6 (crate (name "os_pipe") (vers "0.6.0") (deps (list (crate-dep (name "kernel32-sys") (req "^0.2.2") (default-features #t) (target "cfg(windows)") (kind 0)) (crate-dep (name "nix") (req "^0.9.0") (default-features #t) (target "cfg(not(windows))") (kind 0)) (crate-dep (name "winapi") (req "^0.2.8") (default-features #t) (target "cfg(windows)") (kind 0)))) (hash "1h536ikx337smr11rpv0j4mfbqx8ahw5fg9kj2n1zwd1ms4pcm3z")))

(define-public crate-os_pipe-0.6 (crate (name "os_pipe") (vers "0.6.1") (deps (list (crate-dep (name "nix") (req "^0.10.0") (default-features #t) (target "cfg(not(windows))") (kind 0)) (crate-dep (name "winapi") (req "^0.3.4") (features (quote ("handleapi" "namedpipeapi" "processenv" "winbase"))) (default-features #t) (target "cfg(windows)") (kind 0)))) (hash "0mzipf61gqcw1s8529hr8g4a0jdxqpmfv21xysga7mvfz31nhj4k")))

(define-public crate-os_pipe-0.6 (crate (name "os_pipe") (vers "0.6.2") (deps (list (crate-dep (name "nix") (req "^0.11.0") (default-features #t) (target "cfg(not(windows))") (kind 0)) (crate-dep (name "winapi") (req "^0.3.5") (features (quote ("handleapi" "namedpipeapi" "processenv" "winbase"))) (default-features #t) (target "cfg(windows)") (kind 0)))) (hash "0gr59gkbmq69cjh52wg3wx4crvqsxb9d3zr2xcz2q133sljk40zy")))

(define-public crate-os_pipe-0.7 (crate (name "os_pipe") (vers "0.7.0") (deps (list (crate-dep (name "nix") (req "^0.11.0") (default-features #t) (target "cfg(not(windows))") (kind 0)) (crate-dep (name "winapi") (req "^0.3.5") (features (quote ("handleapi" "namedpipeapi" "processenv" "winbase"))) (default-features #t) (target "cfg(windows)") (kind 0)))) (hash "1iv4r9r3i9n8w7mqh0az8skqjwicmbgmy5pgakp3kvydcy9377fd")))

(define-public crate-os_pipe-0.8 (crate (name "os_pipe") (vers "0.8.0") (deps (list (crate-dep (name "nix") (req "^0.11.0") (default-features #t) (target "cfg(not(windows))") (kind 0)) (crate-dep (name "winapi") (req "^0.3.5") (features (quote ("handleapi" "namedpipeapi" "processenv" "winbase"))) (default-features #t) (target "cfg(windows)") (kind 0)))) (hash "0i2aarfzg981ygy3i8d58c3c8m30fr56bzz6kfkkg508qpv65wkl")))

(define-public crate-os_pipe-0.8 (crate (name "os_pipe") (vers "0.8.1") (deps (list (crate-dep (name "nix") (req "^0.13.0") (default-features #t) (target "cfg(not(windows))") (kind 0)) (crate-dep (name "winapi") (req "^0.3.5") (features (quote ("handleapi" "namedpipeapi" "processenv" "winbase"))) (default-features #t) (target "cfg(windows)") (kind 0)))) (hash "0yqmjndfhas4i82g76rmy2s9iz7ddwah5bhfaghal5a576fq27ff")))

(define-public crate-os_pipe-0.8 (crate (name "os_pipe") (vers "0.8.2") (deps (list (crate-dep (name "nix") (req "^0.15.0") (default-features #t) (target "cfg(not(windows))") (kind 0)) (crate-dep (name "winapi") (req "^0.3.5") (features (quote ("handleapi" "namedpipeapi" "processenv" "winbase"))) (default-features #t) (target "cfg(windows)") (kind 0)))) (hash "049ax8shxik7mm68r2nf7xnrcq3z3p7hz54sbrcxwywxqsjdzs41")))

(define-public crate-os_pipe-0.9 (crate (name "os_pipe") (vers "0.9.0") (deps (list (crate-dep (name "libc") (req "^0.2.62") (default-features #t) (target "cfg(not(windows))") (kind 0)) (crate-dep (name "winapi") (req "^0.3.5") (features (quote ("namedpipeapi"))) (default-features #t) (target "cfg(windows)") (kind 0)))) (hash "10dn8c1k6az20mml2sq29iybkfjm6mq4nva4ilahr2w67b489brb")))

(define-public crate-os_pipe-0.9 (crate (name "os_pipe") (vers "0.9.1") (deps (list (crate-dep (name "libc") (req "^0.2.62") (default-features #t) (target "cfg(not(windows))") (kind 0)) (crate-dep (name "winapi") (req "^0.3.5") (features (quote ("handleapi" "namedpipeapi" "processthreadsapi" "winnt"))) (default-features #t) (target "cfg(windows)") (kind 0)))) (hash "08ma8w5qsi11sz33h9j671a32v22267d1ck5562wx43hb8shckfv")))

(define-public crate-os_pipe-0.9 (crate (name "os_pipe") (vers "0.9.2") (deps (list (crate-dep (name "libc") (req "^0.2.62") (default-features #t) (target "cfg(not(windows))") (kind 0)) (crate-dep (name "winapi") (req "^0.3.5") (features (quote ("handleapi" "namedpipeapi" "processthreadsapi" "winnt"))) (default-features #t) (target "cfg(windows)") (kind 0)))) (hash "04yjs1hf88jjm17g8a2lr7ibxyyg460rzbgcw9f1yzihq833y8zv")))

(define-public crate-os_pipe-1 (crate (name "os_pipe") (vers "1.0.0") (deps (list (crate-dep (name "libc") (req "^0.2.62") (default-features #t) (target "cfg(not(windows))") (kind 0)) (crate-dep (name "winapi") (req "^0.3.5") (features (quote ("handleapi" "namedpipeapi" "processthreadsapi" "winnt"))) (default-features #t) (target "cfg(windows)") (kind 0)))) (hash "09xbp0wdpc2043qn8nf400p9ng81wbf2gm1yw9gqj6rkrbmr4d0f")))

(define-public crate-os_pipe-1 (crate (name "os_pipe") (vers "1.0.1") (deps (list (crate-dep (name "libc") (req "^0.2.62") (default-features #t) (target "cfg(not(windows))") (kind 0)) (crate-dep (name "winapi") (req "^0.3.5") (features (quote ("handleapi" "namedpipeapi" "processthreadsapi" "winnt"))) (default-features #t) (target "cfg(windows)") (kind 0)))) (hash "0mczqmqrkzmln4xg5ki1gwgykf4dsii0h4p7fxf667889ysz54ic")))

(define-public crate-os_pipe-1 (crate (name "os_pipe") (vers "1.1.0") (deps (list (crate-dep (name "libc") (req "^0.2.62") (default-features #t) (target "cfg(not(windows))") (kind 0)) (crate-dep (name "winapi") (req "^0.3.5") (features (quote ("handleapi" "namedpipeapi" "processthreadsapi" "winnt"))) (default-features #t) (target "cfg(windows)") (kind 0)))) (hash "1dfrn2b8vgqm2rrzy3i8r60vaksfqx2awqrbr71hzasyi14al4ni") (features (quote (("io_safety"))))))

(define-public crate-os_pipe-1 (crate (name "os_pipe") (vers "1.1.1") (deps (list (crate-dep (name "libc") (req "^0.2.62") (default-features #t) (target "cfg(not(windows))") (kind 0)) (crate-dep (name "winapi") (req "^0.3.5") (features (quote ("handleapi" "namedpipeapi" "processthreadsapi" "winnt"))) (default-features #t) (target "cfg(windows)") (kind 0)))) (hash "02lkqimqzp0kviyg1mp6djxs79a5573v4ic0akhmxhsr7zjbgkhd") (features (quote (("io_safety"))))))

(define-public crate-os_pipe-1 (crate (name "os_pipe") (vers "1.1.2") (deps (list (crate-dep (name "libc") (req "^0.2.62") (default-features #t) (target "cfg(not(windows))") (kind 0)) (crate-dep (name "windows-sys") (req "^0.42.0") (features (quote ("Win32_Foundation" "Win32_System_Pipes" "Win32_Security" "Win32_System_Threading"))) (default-features #t) (target "cfg(windows)") (kind 0)))) (hash "0fa640v9bi1qcq3jgq1p76lphi4fwj4a9msrmfrq87n1z3qm58n6") (features (quote (("io_safety"))))))

(define-public crate-os_pipe-1 (crate (name "os_pipe") (vers "1.1.3") (deps (list (crate-dep (name "libc") (req "^0.2.62") (default-features #t) (target "cfg(not(windows))") (kind 0)) (crate-dep (name "windows-sys") (req "^0.45.0") (features (quote ("Win32_Foundation" "Win32_System_Pipes" "Win32_Security" "Win32_System_Threading"))) (default-features #t) (target "cfg(windows)") (kind 0)))) (hash "04ls83i290scb8sfzdzj9b3kr9yplb5k864kg841cjzkz8hbngd5") (features (quote (("io_safety"))))))

(define-public crate-os_pipe-1 (crate (name "os_pipe") (vers "1.1.4") (deps (list (crate-dep (name "libc") (req "^0.2.62") (default-features #t) (target "cfg(not(windows))") (kind 0)) (crate-dep (name "windows-sys") (req "^0.48.0") (features (quote ("Win32_Foundation" "Win32_System_Pipes" "Win32_Security" "Win32_System_Threading"))) (default-features #t) (target "cfg(windows)") (kind 0)))) (hash "0xy1igr1jfd9ijhr4sccvl8mzp0jic7njdmr56lsk3220ym5ks0a") (features (quote (("io_safety"))))))

(define-public crate-os_pipe-1 (crate (name "os_pipe") (vers "1.1.5") (deps (list (crate-dep (name "libc") (req "^0.2.62") (default-features #t) (target "cfg(not(windows))") (kind 0)) (crate-dep (name "windows-sys") (req "^0.52.0") (features (quote ("Win32_Foundation" "Win32_System_Pipes" "Win32_Security" "Win32_System_Threading"))) (default-features #t) (target "cfg(windows)") (kind 0)))) (hash "1fcgfg3ddnsh6vfhkk579p7z786kh1khb1dar4g4k1iri4xrq4ap") (features (quote (("io_safety"))))))

