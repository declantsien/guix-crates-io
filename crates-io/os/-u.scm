(define-module (crates-io os -u) #:use-module (crates-io))

(define-public crate-os-unfair-lock-0.1 (crate (name "os-unfair-lock") (vers "0.1.0") (hash "18zakij26sqxzx0rj9l3gi5sk7rg7h0w33l24igcbk3i4bh72glp") (features (quote (("nightly")))) (yanked #t)))

(define-public crate-os-unfair-lock-0.2 (crate (name "os-unfair-lock") (vers "0.2.0") (hash "0dr7kq942hp6k9ikla4i0a1fgiq3b6qly48frcbnlj3gyka6rfv2") (features (quote (("nightly")))) (yanked #t)))

(define-public crate-os-unfair-lock-0.3 (crate (name "os-unfair-lock") (vers "0.3.0") (hash "13crvvf4jg16cvgwkzwhr07kbzlkbgpzch2cr0mxgk5djpa2kclr") (features (quote (("nightly"))))))

