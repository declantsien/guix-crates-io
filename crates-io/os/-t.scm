(define-module (crates-io os -t) #:use-module (crates-io))

(define-public crate-os-thread-local-0.1 (crate (name "os-thread-local") (vers "0.1.0") (deps (list (crate-dep (name "crossbeam-utils") (req "^0.6") (default-features #t) (kind 2)) (crate-dep (name "libc") (req "^0.2") (target "cfg(not(windows))") (kind 0)) (crate-dep (name "once_cell") (req "^1") (default-features #t) (kind 2)) (crate-dep (name "winapi") (req "^0.3") (features (quote ("fibersapi"))) (default-features #t) (target "cfg(windows)") (kind 0)))) (hash "1hvg11rbk0928axf07h59xgjkm0zimh3h6qh11y2x3x8n2yyi2by")))

(define-public crate-os-thread-local-0.1 (crate (name "os-thread-local") (vers "0.1.1") (deps (list (crate-dep (name "crossbeam-utils") (req "^0.6") (default-features #t) (kind 2)) (crate-dep (name "libc") (req "^0.2") (target "cfg(not(windows))") (kind 0)) (crate-dep (name "once_cell") (req "^1") (default-features #t) (kind 2)) (crate-dep (name "winapi") (req "^0.3") (features (quote ("fibersapi"))) (default-features #t) (target "cfg(windows)") (kind 0)))) (hash "1i09gmd48l77pr3g6sdflw1zdh6ayxy3sg3akvi5rmysab6nmgcs")))

(define-public crate-os-thread-local-0.1 (crate (name "os-thread-local") (vers "0.1.2") (deps (list (crate-dep (name "crossbeam-utils") (req "^0.6") (default-features #t) (kind 2)) (crate-dep (name "libc") (req "^0.2") (target "cfg(not(windows))") (kind 0)) (crate-dep (name "once_cell") (req "^1") (default-features #t) (kind 2)) (crate-dep (name "winapi") (req "^0.3") (features (quote ("fibersapi"))) (default-features #t) (target "cfg(windows)") (kind 0)))) (hash "13z2gjyah6h5wnyiz19x8j766252l3kz6hbnyvh9x72lb7m3fp57")))

(define-public crate-os-thread-local-0.1 (crate (name "os-thread-local") (vers "0.1.3") (deps (list (crate-dep (name "crossbeam-utils") (req "^0.6") (default-features #t) (kind 2)) (crate-dep (name "libc") (req "^0.2") (target "cfg(not(windows))") (kind 0)) (crate-dep (name "once_cell") (req "^1") (default-features #t) (kind 2)) (crate-dep (name "winapi") (req "^0.3") (features (quote ("fibersapi"))) (default-features #t) (target "cfg(windows)") (kind 0)))) (hash "0z1bmz8r3b1h4lwbnnkzds91207d0q0p63miz43qkp57kvxcfzyx")))

(define-public crate-os-timer-1 (crate (name "os-timer") (vers "1.0.0") (deps (list (crate-dep (name "cc") (req "^1") (default-features #t) (kind 1) (package "cc")) (crate-dep (name "libc") (req "^0.2") (target "cfg(all(unix, not(any(target_os = \"macos\", target_os = \"ios\"))))") (kind 0)) (crate-dep (name "wasm-bindgen") (req "^0.2") (default-features #t) (target "cfg(any(target_arch = \"wasm32\"))") (kind 0)))) (hash "1fax6p65qy8jbahcmdb4x24dsp3dbaaycbv626jhpxif2n4rh80w") (yanked #t)))

(define-public crate-os-timer-1 (crate (name "os-timer") (vers "1.0.1") (deps (list (crate-dep (name "cc") (req "^1") (default-features #t) (kind 1) (package "cc")) (crate-dep (name "libc") (req "^0.2") (target "cfg(all(unix, not(any(target_os = \"macos\", target_os = \"ios\"))))") (kind 0)))) (hash "07dy81z1lwxjw1mrfbprcaza0cnbdxssh5l4s6j1ky8m11r61zb8") (yanked #t)))

(define-public crate-os-timer-1 (crate (name "os-timer") (vers "1.0.2") (deps (list (crate-dep (name "cc") (req "^1") (default-features #t) (kind 1) (package "cc")) (crate-dep (name "libc") (req "^0.2") (target "cfg(all(unix, not(any(target_os = \"macos\", target_os = \"ios\"))))") (kind 0)))) (hash "0i8hjv4lw6b64vc31i1zxwa7z74a2hjj9iz5ym5jbac06w66dlq9") (yanked #t)))

(define-public crate-os-timer-1 (crate (name "os-timer") (vers "1.0.3") (deps (list (crate-dep (name "cc") (req "^1") (default-features #t) (kind 1) (package "cc")) (crate-dep (name "libc") (req "^0.2") (target "cfg(all(unix, not(any(target_os = \"macos\", target_os = \"ios\"))))") (kind 0)))) (hash "0gbzacb6kfyby2qg5g6n2947kpdbzwijkj49y3nw8x9mmjj80446") (yanked #t)))

(define-public crate-os-timer-1 (crate (name "os-timer") (vers "1.0.4") (deps (list (crate-dep (name "cc") (req "^1") (default-features #t) (kind 1) (package "cc")) (crate-dep (name "libc") (req "^0.2") (target "cfg(all(unix, not(any(target_os = \"macos\", target_os = \"ios\"))))") (kind 0)))) (hash "0k9gc2hkb6v77cphan1i2l34s636znd75cz97pxah89c5jjy1pcc") (yanked #t)))

(define-public crate-os-timer-1 (crate (name "os-timer") (vers "1.0.5") (deps (list (crate-dep (name "cc") (req "^1") (default-features #t) (kind 1) (package "cc")) (crate-dep (name "libc") (req "^0.2") (target "cfg(all(unix, not(any(target_os = \"macos\", target_os = \"ios\"))))") (kind 0)))) (hash "1ssipmkcvgdw0cifkmcdqgv3a3s2d7c30828vqs5qr6x87rwhi41")))

(define-public crate-os-timer-1 (crate (name "os-timer") (vers "1.0.6") (deps (list (crate-dep (name "cc") (req "^1") (default-features #t) (kind 1) (package "cc")) (crate-dep (name "libc") (req "^0.2") (target "cfg(all(unix, not(any(target_os = \"macos\", target_os = \"ios\"))))") (kind 0)))) (hash "1b637ygivcgfsd0cljiykcg9gjj0x8sc6irwwrp5bi4jqs442y2y")))

(define-public crate-os-timer-1 (crate (name "os-timer") (vers "1.0.7") (deps (list (crate-dep (name "cc") (req "^1") (default-features #t) (kind 1) (package "cc")) (crate-dep (name "libc") (req "^0.2") (target "cfg(all(unix, not(any(target_os = \"macos\", target_os = \"ios\"))))") (kind 0)))) (hash "0shv8bf468g8x34r0x7287i4dni3xadazd4yzas5kzpz96wd55y2")))

(define-public crate-os-timer-1 (crate (name "os-timer") (vers "1.0.8") (deps (list (crate-dep (name "cc") (req "^1") (default-features #t) (kind 1) (package "cc")) (crate-dep (name "libc") (req "^0.2") (target "cfg(all(unix, not(any(target_os = \"macos\", target_os = \"ios\"))))") (kind 0)))) (hash "1n9gqsq94a7ilwnhka0fypjis9r8zxcxsaypsdbvqzapwb978cky")))

(define-public crate-os-timer-1 (crate (name "os-timer") (vers "1.0.9") (deps (list (crate-dep (name "cc") (req "^1") (default-features #t) (kind 1) (package "cc")) (crate-dep (name "libc") (req "^0.2") (target "cfg(all(unix, not(any(target_os = \"macos\", target_os = \"ios\"))))") (kind 0)))) (hash "11zvngmnyzz7ccj5l7c1sni71g2076ff8vg6ndfdj2bjyzajkpsn") (yanked #t)))

(define-public crate-os-timer-1 (crate (name "os-timer") (vers "1.0.10") (deps (list (crate-dep (name "cc") (req "^1") (default-features #t) (kind 1) (package "cc")) (crate-dep (name "libc") (req "^0.2") (target "cfg(all(unix, not(any(target_os = \"macos\", target_os = \"ios\"))))") (kind 0)))) (hash "1y6fxcxyxrmpi4ab9zr012vpjzfpvwysys1nzyb3mvdm0wd6cxnl")))

