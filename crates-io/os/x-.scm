(define-module (crates-io os x-) #:use-module (crates-io))

(define-public crate-osx-sys-0.1 (crate (name "osx-sys") (vers "0.1.0") (deps (list (crate-dep (name "bindgen") (req "^0.65.1") (default-features #t) (kind 1)))) (hash "19xx3qlmpinrs1g8796yhims0837j3fmra40c6zpj6g5nd1zpjfp")))

