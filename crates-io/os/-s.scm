(define-module (crates-io os -s) #:use-module (crates-io))

(define-public crate-os-str-generic-0.1 (crate (name "os-str-generic") (vers "0.1.0") (hash "01ivdcbjjxyck4c8sf3mpg8b709yjm61pm0dg9gnhi5nvbzgxnn7")))

(define-public crate-os-str-generic-0.1 (crate (name "os-str-generic") (vers "0.1.1") (hash "0gm7hij62h3zyz5qkc8zikw4551yvc1mchz0ypnzfwrxm760ilpd")))

(define-public crate-os-str-generic-0.2 (crate (name "os-str-generic") (vers "0.2.0") (hash "0zhmlpfc02rv9mf1mrhsxfkhqkmx54qdmwdpahzpd2bvh8hddwvq")))

(define-public crate-os-str-manip-0.0.1 (crate (name "os-str-manip") (vers "0.0.1") (hash "1wywqb551zllklhm2446czcvdil9s300a2p8spqfv9rhv5nzpj45") (features (quote (("unchecked_index")))) (yanked #t) (rust-version "1.56.1")))

(define-public crate-os-str-manip-0.0.2 (crate (name "os-str-manip") (vers "0.0.2") (hash "0dngz2rjp01iar0iijpjmqpw28q99dpfphiwzyn73270l73frlfj") (features (quote (("unchecked_index")))) (yanked #t) (rust-version "1.56.1")))

(define-public crate-os-str-manip-0.0.3 (crate (name "os-str-manip") (vers "0.0.3") (hash "0y4s68wq2lzlr0jj5dcvl45xpwimk2sqr1v7s9hsvrk972p6g8jl") (features (quote (("unchecked_index")))) (yanked #t) (rust-version "1.56.1")))

(define-public crate-os-str-manip-0.0.4 (crate (name "os-str-manip") (vers "0.0.4") (deps (list (crate-dep (name "proptest") (req "^1.2.0") (default-features #t) (kind 2)))) (hash "1jvk7llwdv01d33mn935vh79aqh9hw68h39nrbkmk8b57g42hm4z") (features (quote (("unchecked_index")))) (yanked #t) (rust-version "1.56.1")))

(define-public crate-os-sync-0.1 (crate (name "os-sync") (vers "0.1.0") (deps (list (crate-dep (name "libc") (req "^0.2") (target "cfg(unix)") (kind 0)))) (hash "1za5m18q6f3km6akmj05wa3qm5x98mgg273kwm7vzk0r1ssyzkm1")))

(define-public crate-os-sync-0.2 (crate (name "os-sync") (vers "0.2.0") (deps (list (crate-dep (name "libc") (req "^0.2") (target "cfg(unix)") (kind 0)))) (hash "1inh3rxmw5vi84pgypkfzjcjk9b1q1jww0xci96gvk96mqi03cl6")))

(define-public crate-os-sync-0.3 (crate (name "os-sync") (vers "0.3.0") (deps (list (crate-dep (name "libc") (req "^0.2") (target "cfg(unix)") (kind 0)))) (hash "13n12vq92mshkdk9jnlgsqpamhgq9n34jygjv6xdyvl7f0mrkaki")))

(define-public crate-os-sync-0.3 (crate (name "os-sync") (vers "0.3.1") (deps (list (crate-dep (name "error-code") (req "^1") (default-features #t) (target "cfg(unix)") (kind 0)) (crate-dep (name "libc") (req "^0.2") (target "cfg(unix)") (kind 0)))) (hash "07iwvanny782fpm0dyq1wp2kmxkyf6xbqv45vlp595fwhfwvw3cq")))

(define-public crate-os-sync-0.3 (crate (name "os-sync") (vers "0.3.2") (deps (list (crate-dep (name "error-code") (req "^2") (default-features #t) (target "cfg(unix)") (kind 0)) (crate-dep (name "libc") (req "^0.2") (target "cfg(unix)") (kind 0)))) (hash "13prbsjxrhy7v173qahp59d0j43gwx683kl2wck4jjy5vi3h09mm")))

(define-public crate-os-sync-0.3 (crate (name "os-sync") (vers "0.3.3") (deps (list (crate-dep (name "error-code") (req "^2") (default-features #t) (target "cfg(unix)") (kind 0)) (crate-dep (name "libc") (req "^0.2") (target "cfg(unix)") (kind 0)))) (hash "07246vph01yvwx3xhsal0kqijna8w21wzrjkhzyz14k4h2k0vx0p")))

