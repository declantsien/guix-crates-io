(define-module (crates-io os _c) #:use-module (crates-io))

(define-public crate-os_clock-0.1 (crate (name "os_clock") (vers "0.1.0") (deps (list (crate-dep (name "bindgen") (req "^0.55.1") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2.76") (default-features #t) (kind 0)))) (hash "046m075f1zh5bibvqnjyq2dyfcxxqli2npz7cg41r0vd63ya5szh")))

(define-public crate-os_clock-0.2 (crate (name "os_clock") (vers "0.2.0") (deps (list (crate-dep (name "bindgen") (req "^0.55.1") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2.76") (default-features #t) (kind 0)))) (hash "1912ilpknc65nb0xb4ba1zck32gkjbb974anfdk7axjb0727lcqr")))

(define-public crate-os_clock-0.2 (crate (name "os_clock") (vers "0.2.1") (deps (list (crate-dep (name "bindgen") (req "^0.55.1") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2.76") (default-features #t) (kind 0)))) (hash "0dl8qq4gqqn3d69vjpfz257plmnn29xiyl57wa8y8gzwr4bv250z")))

(define-public crate-os_clock-0.3 (crate (name "os_clock") (vers "0.3.0") (deps (list (crate-dep (name "bindgen") (req "^0.55.1") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2.76") (default-features #t) (kind 0)))) (hash "0z8kzzki7y330pg1ali6hxhlv2csn4aybr5gqs7ss1c70q3fd64j")))

(define-public crate-os_clock-0.3 (crate (name "os_clock") (vers "0.3.1") (deps (list (crate-dep (name "bindgen") (req "^0.55.1") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2.76") (default-features #t) (kind 0)))) (hash "05kx9awpl79hxpdwsf5ygyz7j0q3mydl4cdlilbspqq0qjvy7w65")))

