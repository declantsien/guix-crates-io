(define-module (crates-io os -g) #:use-module (crates-io))

(define-public crate-os-gateway-contract-attributes-1 (crate (name "os-gateway-contract-attributes") (vers "1.0.0") (deps (list (crate-dep (name "cosmwasm-std") (req "=1.0.0") (default-features #t) (kind 2)))) (hash "0zmc6wsz67kaimx0jpiiq1l4f32qx3d449ll1r40mrh6bl5nd1li") (features (quote (("library"))))))

(define-public crate-os-gateway-contract-attributes-1 (crate (name "os-gateway-contract-attributes") (vers "1.0.1") (deps (list (crate-dep (name "cosmwasm-std") (req "=1.0.0") (default-features #t) (kind 2)))) (hash "0fyrbx4mj1b0bhqqs37gx1v4fh8rnkwq3017fba3kj9wbmwr6jq7") (features (quote (("library"))))))

