(define-module (crates-io os td) #:use-module (crates-io))

(define-public crate-ostdl-0.9 (crate (name "ostdl") (vers "0.9.0") (deps (list (crate-dep (name "clap") (req "^2.27.1") (default-features #t) (kind 0)) (crate-dep (name "libflate") (req "^0.1.11") (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.8.1") (default-features #t) (kind 0)) (crate-dep (name "xmlrpc") (req "^0.9.0") (default-features #t) (kind 0)))) (hash "1mg5mh6ydspgq80q7vikzp2mx2vjp1khm6a39rvwdxnpww9wicmi")))

(define-public crate-ostdl-0.9 (crate (name "ostdl") (vers "0.9.1") (deps (list (crate-dep (name "clap") (req "^2.27.1") (default-features #t) (kind 0)) (crate-dep (name "libflate") (req "^0.1.11") (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.8.1") (default-features #t) (kind 0)) (crate-dep (name "xmlrpc") (req "^0.9.0") (default-features #t) (kind 0)))) (hash "1q7nw94awdv8x39jjjcb71pvx5h2rgv8lddmjzkb30snbwv7sdcr")))

