(define-module (crates-io os as) #:use-module (crates-io))

(define-public crate-osascript-0.1 (crate (name "osascript") (vers "0.1.0") (deps (list (crate-dep (name "serde") (req "^0.9") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^0.9") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^0.9") (default-features #t) (kind 0)))) (hash "0wqqy0gbc29pp7v02v7m2wlxh52y4k23xgpz8c95h0gwa0g60n0j")))

(define-public crate-osascript-0.2 (crate (name "osascript") (vers "0.2.0") (deps (list (crate-dep (name "serde") (req "^0") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^0") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^0") (default-features #t) (kind 0)))) (hash "0pj4iyhvwk9vr0cwsvlhswfyl5bfhz6ypng6z7fvizi42j9ncqcp")))

(define-public crate-osascript-0.3 (crate (name "osascript") (vers "0.3.0") (deps (list (crate-dep (name "serde") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1") (default-features #t) (kind 0)))) (hash "1p47zqg463wrymv7yilqy91b89jr2ri9bjk6xhd9yrzgb6l1ywrq")))

