(define-module (crates-io os lo) #:use-module (crates-io))

(define-public crate-oslo-policy-0.1 (crate (name "oslo-policy") (vers "0.1.0") (deps (list (crate-dep (name "peg") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1") (default-features #t) (kind 0)))) (hash "1j9y0h9wrg1yyjxnrra7m1g8m0jpj25z3j2h21z8h1cw63zn64sz")))

(define-public crate-oslobike-0.1 (crate (name "oslobike") (vers "0.1.0") (deps (list (crate-dep (name "dotenv") (req "^0.13.0") (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.9.10") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.88") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0.88") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.38") (default-features #t) (kind 0)))) (hash "13gha246rgn2id5ncn7s3avrigavvqn4iij1wzwg17zhbn00xvh0")))

(define-public crate-oslog-0.0.1 (crate (name "oslog") (vers "0.0.1") (deps (list (crate-dep (name "cc") (req "^1.0") (default-features #t) (kind 1)) (crate-dep (name "log") (req "^0.4") (features (quote ("std"))) (optional #t) (default-features #t) (kind 0)))) (hash "0li2q5mp227qpn6psy9h8jdrm0bbixrjaxggzm9g29w4binx6l4h") (features (quote (("logger" "log") ("default" "logger"))))))

(define-public crate-oslog-0.0.2 (crate (name "oslog") (vers "0.0.2") (deps (list (crate-dep (name "cc") (req "^1.0") (default-features #t) (kind 1)) (crate-dep (name "log") (req "^0.4") (features (quote ("std"))) (optional #t) (default-features #t) (kind 0)))) (hash "12bjgxg4cnh41injb4yzvys5lk0k199kw2ppn3ywsz5y8awfvbiv") (features (quote (("logger" "log") ("default" "logger"))))))

(define-public crate-oslog-0.0.3 (crate (name "oslog") (vers "0.0.3") (deps (list (crate-dep (name "cc") (req "^1.0") (default-features #t) (kind 1)) (crate-dep (name "dashmap") (req "^3") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4") (features (quote ("std"))) (optional #t) (default-features #t) (kind 0)))) (hash "0zxwml9h803yr3f968jx8r42drlwap2w7my5gc2xkyskwshjq2g9") (features (quote (("logger" "dashmap" "log") ("default" "logger"))))))

(define-public crate-oslog-0.0.4 (crate (name "oslog") (vers "0.0.4") (deps (list (crate-dep (name "cc") (req "^1.0") (default-features #t) (kind 1)) (crate-dep (name "dashmap") (req "^4") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4") (features (quote ("std"))) (optional #t) (default-features #t) (kind 0)))) (hash "07021ak3gzf15hx9436zww4j18l77jfzja9mmw0lqsrp6rmkkic1") (features (quote (("logger" "dashmap" "log") ("default" "logger"))))))

(define-public crate-oslog-0.1 (crate (name "oslog") (vers "0.1.0") (deps (list (crate-dep (name "cc") (req "^1.0") (default-features #t) (kind 1)) (crate-dep (name "dashmap") (req "^4") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4") (features (quote ("std"))) (optional #t) (default-features #t) (kind 0)))) (hash "1cr8jxb1qrgasamsp5akd0sk0igcfskhxp870a6fdrqqbyawwhw3") (features (quote (("logger" "dashmap" "log") ("default" "logger"))))))

(define-public crate-oslog-0.2 (crate (name "oslog") (vers "0.2.0") (deps (list (crate-dep (name "cc") (req "^1.0.73") (default-features #t) (kind 1)) (crate-dep (name "dashmap") (req "^5.1.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.14") (features (quote ("std"))) (optional #t) (kind 0)))) (hash "0s9rgbwsa70ay65ir7cfbxzm0h2z56rbgxxiyjr7rmv13wyh9ll0") (features (quote (("logger" "dashmap" "log") ("default" "logger"))))))

