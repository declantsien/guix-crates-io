(define-module (crates-io os sl) #:use-module (crates-io))

(define-public crate-ossl-provider-0.1 (crate (name "ossl-provider") (vers "0.1.0") (deps (list (crate-dep (name "quote") (req "^1.0.33") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2.0.38") (default-features #t) (kind 0)))) (hash "1z5dzlxw307isbc75kfwl7iqpnraw9f3c7iys3jzvvn0r7kwcgjn")))

