(define-module (crates-io os st) #:use-module (crates-io))

(define-public crate-osstrtools-0.1 (crate (name "osstrtools") (vers "0.1.0") (hash "1mkgc8yfn8ihg52n6fab80phh23bdhnqcb8jnfsncr5jaga9vj3g")))

(define-public crate-osstrtools-0.1 (crate (name "osstrtools") (vers "0.1.1") (hash "0rlvmd4hkyfvgarcrklmcbqjw846hbzl9h66cmbh5bpq3q4872ks")))

(define-public crate-osstrtools-0.1 (crate (name "osstrtools") (vers "0.1.2") (hash "12c0x4hnpnwhdkvdv0iayz91w9ycs2qbrfxwgakj9ywk8zy9zbd5")))

(define-public crate-osstrtools-0.1 (crate (name "osstrtools") (vers "0.1.3") (hash "00qplhbw6haxzcivbm61575jgnpmjg3rxqnnw2zqb546wikvxk93")))

(define-public crate-osstrtools-0.1 (crate (name "osstrtools") (vers "0.1.4") (hash "0nqgfdyhz2bz4mv1rq3ypgpcy593z7aj9ilwvby6zdk5y88psmb3")))

(define-public crate-osstrtools-0.1 (crate (name "osstrtools") (vers "0.1.5") (hash "1xx19ar08sdmlwixyx5pmwgpq4xqg086azs973fqnkaq5vj352y6")))

(define-public crate-osstrtools-0.1 (crate (name "osstrtools") (vers "0.1.8") (hash "1g7c60h57w0wdhqmj4qjsbks7fba423ypb0nqjasjf0ax9cjp80g")))

(define-public crate-osstrtools-0.1 (crate (name "osstrtools") (vers "0.1.9") (hash "1w5san8n5m8j326q4zi959q2gi1mvggcc4gizr08rffnns11vb6d")))

(define-public crate-osstrtools-0.2 (crate (name "osstrtools") (vers "0.2.0") (deps (list (crate-dep (name "itertools") (req "^0.8.2") (default-features #t) (kind 0)) (crate-dep (name "os_str_bytes") (req "^1.0") (optional #t) (default-features #t) (kind 0)))) (hash "0mqniqpfr8yvl78lzriy0k9gac4jqyn49178fiy2k8z5sinnk5a0") (features (quote (("windows" "os_str_bytes") ("default"))))))

(define-public crate-osstrtools-0.2 (crate (name "osstrtools") (vers "0.2.1") (deps (list (crate-dep (name "itertools") (req "^0.8.2") (default-features #t) (kind 0)) (crate-dep (name "os_str_bytes") (req "^1.0") (optional #t) (default-features #t) (kind 0)))) (hash "150dpp0c6cpp38h4j62hx1ikdpz0p9wlbjj6vh3l3dgq18dizjvs") (features (quote (("windows" "os_str_bytes") ("default"))))))

(define-public crate-osstrtools-0.2 (crate (name "osstrtools") (vers "0.2.2") (deps (list (crate-dep (name "itertools") (req "^0.8.2") (default-features #t) (kind 0)) (crate-dep (name "os_str_bytes") (req "^1.0") (optional #t) (default-features #t) (kind 0)))) (hash "1fna5n3b6237dsp6cm8wi07v44iw6azxhryks9wbhfpfkvddla7k") (features (quote (("windows" "os_str_bytes") ("default"))))))

