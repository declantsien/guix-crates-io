(define-module (crates-io os pr) #:use-module (crates-io))

(define-public crate-ospre-0.1 (crate (name "ospre") (vers "0.1.0") (hash "1h5zk3x5lpaqa3kwrhqvd3z8qax3zz2gc0006h20bd1xdqv5d1jp")))

(define-public crate-ospre-0.1 (crate (name "ospre") (vers "0.1.1") (hash "1dwldpa277fl7a9iiss1czgc4g3vxzjl3jl1wb71miwga9prcb5y")))

(define-public crate-ospre-0.1 (crate (name "ospre") (vers "0.1.2") (hash "005vkh4i9krnd6l509cjjk9pv1xwnvvs3v4kxrd593b6wm7r3hxa")))

(define-public crate-ospre-0.1 (crate (name "ospre") (vers "0.1.4") (hash "0w1gfwcvfz6pz2jkmr9xhvapp8xwnaq4z4999hrxpk3zn6ki3vb9")))

(define-public crate-ospre-0.1 (crate (name "ospre") (vers "0.1.5") (hash "1fnkd0dp3vkjsjp4lbcs4pppbazyd2j17c0h7h6nnmbh9k6s9646")))

(define-public crate-ospre-0.1 (crate (name "ospre") (vers "0.1.6") (hash "1yk0gdni8kcm3nxp8m7qdh0hd5jn2z2mdi7g7bij447bhvwpqw7m")))

(define-public crate-osprey-0.2 (crate (name "osprey") (vers "0.2.1") (deps (list (crate-dep (name "clap") (req "^2.26") (default-features #t) (kind 0)) (crate-dep (name "falcon") (req "^0.2.1") (features (quote ("thread_safe"))) (default-features #t) (kind 0)) (crate-dep (name "gluon") (req "^0.6.2") (default-features #t) (kind 0)) (crate-dep (name "gluon_vm") (req "^0.6.2") (default-features #t) (kind 0)))) (hash "1w2zdv9wjmkgcrwndavrvfkdhkz3fj151n6b4c4gr5m7b5r3kn00")))

(define-public crate-osprey-0.2 (crate (name "osprey") (vers "0.2.4") (deps (list (crate-dep (name "clap") (req "^2.26") (default-features #t) (kind 0)) (crate-dep (name "falcon") (req "^0.2.4") (features (quote ("thread_safe"))) (default-features #t) (kind 0)) (crate-dep (name "gluon") (req "^0.7.0") (default-features #t) (kind 0)) (crate-dep (name "gluon_vm") (req "^0.7.0") (default-features #t) (kind 0)))) (hash "0a2dxirhkhkw0dgkgam3s384q95l14l154cxmmk9044lig8b1bch")))

(define-public crate-osprey-0.4 (crate (name "osprey") (vers "0.4.0") (deps (list (crate-dep (name "clap") (req "^2.26") (default-features #t) (kind 0)) (crate-dep (name "falcon") (req "^0.4.0") (features (quote ("thread_safe"))) (default-features #t) (kind 0)) (crate-dep (name "gluon") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "gluon_vm") (req "^0.8") (default-features #t) (kind 0)))) (hash "0kv8j6ad5y7m46wyqv94kcwq4likqqnwb5i6kyvmnwc15h86b3rj")))

(define-public crate-osprey-0.4 (crate (name "osprey") (vers "0.4.4") (deps (list (crate-dep (name "clap") (req "^2.26") (default-features #t) (kind 0)) (crate-dep (name "falcon") (req "^0.4.4") (features (quote ("thread_safe"))) (default-features #t) (kind 0)) (crate-dep (name "gluon") (req "^0.9.4") (default-features #t) (kind 0)) (crate-dep (name "gluon_vm") (req "^0.9.4") (default-features #t) (kind 0)))) (hash "1bw36r50j069ha9pv7sj1z6cwc0yw4wpz4amxj9mxj8s87qyxz67")))

(define-public crate-osproto-0.0.1 (crate (name "osproto") (vers "0.0.1") (deps (list (crate-dep (name "chrono") (req "^0.4") (features (quote ("serde"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 2)) (crate-dep (name "url") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "url_serde") (req "^0.2") (default-features #t) (kind 0)))) (hash "0gd7kmznvj5aiba8xnzmqz36mql38p01xpdnalh2zia6jkna1a9g")))

(define-public crate-osproto-0.0.2 (crate (name "osproto") (vers "0.0.2") (deps (list (crate-dep (name "chrono") (req "^0.4") (features (quote ("serde"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 2)) (crate-dep (name "url") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "url_serde") (req "^0.2") (default-features #t) (kind 0)))) (hash "0irf7441rv4405r4dd2420qc36j45pg9jr8hhaz6jxb35ca4iss5")))

(define-public crate-osproto-0.1 (crate (name "osproto") (vers "0.1.0") (deps (list (crate-dep (name "chrono") (req "^0.4") (features (quote ("serde"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 2)) (crate-dep (name "url") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "url_serde") (req "^0.2") (default-features #t) (kind 0)))) (hash "1vqffklz0bh88cjm92x4bk1ginpvfpr74ndw6cf6f834v6c46v6g")))

(define-public crate-osproto-0.1 (crate (name "osproto") (vers "0.1.1") (deps (list (crate-dep (name "chrono") (req "^0.4") (features (quote ("serde"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "url") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "url_serde") (req "^0.2") (default-features #t) (kind 0)))) (hash "0qmnavk8772hn5px7a6k8sv2x7ciyaw9wp8mz3r0jsg6x3njgjsx")))

(define-public crate-osproto-0.1 (crate (name "osproto") (vers "0.1.2") (deps (list (crate-dep (name "chrono") (req "^0.4") (features (quote ("serde"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "url") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "url_serde") (req "^0.2") (default-features #t) (kind 0)))) (hash "1w65i52h6g5r4hlbsms6alg60wlpia8vy2p7738798437nxj96wf")))

(define-public crate-osproto-0.2 (crate (name "osproto") (vers "0.2.0") (deps (list (crate-dep (name "chrono") (req "^0.4") (features (quote ("serde"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "url") (req "^2.1") (features (quote ("serde"))) (default-features #t) (kind 0)))) (hash "0ggx53phgbdv80jgs5zc7d4lvms36r3gcw6mrk6h5kivlnpff3gx")))

(define-public crate-osproto-0.2 (crate (name "osproto") (vers "0.2.1") (deps (list (crate-dep (name "chrono") (req "^0.4") (features (quote ("serde"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "url") (req "^2.1") (features (quote ("serde"))) (default-features #t) (kind 0)))) (hash "13g4snil37b0r1fdm34145y6d9pqdcfb4i2rqlb76xiydm83inls")))

(define-public crate-osproto-0.2 (crate (name "osproto") (vers "0.2.2") (deps (list (crate-dep (name "chrono") (req "^0.4") (features (quote ("serde"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "url") (req "^2.1") (features (quote ("serde"))) (default-features #t) (kind 0)))) (hash "1bkvckyrwbax9316mnbsr6lssjm90s58jbkrfi3rmg1ggnrzinn8")))

