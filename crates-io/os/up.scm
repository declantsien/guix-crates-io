(define-module (crates-io os up) #:use-module (crates-io))

(define-public crate-osuparse-0.1 (crate (name "osuparse") (vers "0.1.0") (deps (list (crate-dep (name "lazy_static") (req "^1.2.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)))) (hash "1b9hzi3mjngdzbwybq837v4rfl7wr29a0rfks159rlybq5lpnbix")))

(define-public crate-osuparse-0.1 (crate (name "osuparse") (vers "0.1.1") (deps (list (crate-dep (name "lazy_static") (req "^1.2.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)))) (hash "1mfdllwk9zckklmh2lcnzallz9a20cldgh4fxxwbf5hmf1dhrk6a")))

(define-public crate-osuparse-1 (crate (name "osuparse") (vers "1.0.0") (deps (list (crate-dep (name "unicase") (req "^2.2.0") (default-features #t) (kind 0)))) (hash "04209cy9s1g5m7ajzcbjmy7dg2cc80sradp3dpdyfyr45vw37p0c")))

(define-public crate-osuparse-2 (crate (name "osuparse") (vers "2.0.0") (deps (list (crate-dep (name "unicase") (req "^2.2.0") (default-features #t) (kind 0)))) (hash "1dv3xbr86n31iwjizw4cnj30fr8s397zd7ya47slb58c1yzfv89q")))

(define-public crate-osuparse-2 (crate (name "osuparse") (vers "2.0.1") (deps (list (crate-dep (name "unicase") (req "^2.2.0") (default-features #t) (kind 0)))) (hash "0mpbv732j3ghx512zd7mh83z1qyi3mpilncpkwvp13cx9s51bkin")))

