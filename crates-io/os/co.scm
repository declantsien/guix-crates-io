(define-module (crates-io os co) #:use-module (crates-io))

(define-public crate-oscoin-0.2 (crate (name "oscoin") (vers "0.2.0") (deps (list (crate-dep (name "lazy_static") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quickcheck") (req "^0.6") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.4") (default-features #t) (kind 0)))) (hash "1npmyj6y43y7d3sxdvhjvzcr8l0phwvj1wzlvaxsc33yxx2d92d0") (features (quote (("schedule") ("default"))))))

(define-public crate-oscopy-0.1 (crate (name "oscopy") (vers "0.1.0") (deps (list (crate-dep (name "base64") (req "^0.12.1") (default-features #t) (kind 0)))) (hash "0rimhlky4syrpg8zr4dipmk6cas9q1w9jmxc125pkqqrw222fr6m")))

(define-public crate-oscopy-0.2 (crate (name "oscopy") (vers "0.2.0") (deps (list (crate-dep (name "base64") (req "^0.12.1") (default-features #t) (kind 0)))) (hash "1c361f5wb5azkzs1mk6apxlxq0ijajjgvf8sq1gc1rswm21a4c4j")))

