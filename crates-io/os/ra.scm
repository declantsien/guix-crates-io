(define-module (crates-io os ra) #:use-module (crates-io))

(define-public crate-osrand-0.0.1 (crate (name "osrand") (vers "0.0.1") (hash "038kg7qzqnk39gz0c9rkgpp6mgp2ha0l528rz40zdzb1l3gmiz93") (yanked #t)))

(define-public crate-osrand-0.2 (crate (name "osrand") (vers "0.2.0") (hash "0fxi1r1zz156pm2ppddqfwm61ppzqfvp544x9wvqz985rj6wdnw8")))

(define-public crate-osrand-0.2 (crate (name "osrand") (vers "0.2.1") (hash "0m3bidhig96ignfynqyxcwpjq8mwr9zgyc64mhx5njqkbaspvx5p") (features (quote (("urandom") ("default" "urandom"))))))

(define-public crate-osrandom-0.1 (crate (name "osrandom") (vers "0.1.0") (deps (list (crate-dep (name "cc") (req "^1.0.73") (default-features #t) (kind 1)))) (hash "1ps0n4c7hr5h2x1fj88l2r40ngd6r7p1n47q813r4pyvv57pnd53") (features (quote (("default"))))))

