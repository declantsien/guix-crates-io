(define-module (crates-io os fe) #:use-module (crates-io))

(define-public crate-osfetch-rs-0.1 (crate (name "osfetch-rs") (vers "0.1.0") (deps (list (crate-dep (name "nixinfo") (req "^0.2.8") (default-features #t) (kind 0)) (crate-dep (name "whoami") (req "^1.1.2") (default-features #t) (kind 0)))) (hash "0cgiyv8mgj2ii1g9dfx7qq6x9bikxnvyrxx5y78r8yl57s3l5fa0")))

