(define-module (crates-io os su) #:use-module (crates-io))

(define-public crate-ossuary-0.5 (crate (name "ossuary") (vers "0.5.0") (deps (list (crate-dep (name "chacha20-poly1305-aead") (req "= 0.1.2") (features (quote ("simd" "simd_opt"))) (default-features #t) (kind 0)) (crate-dep (name "ed25519-dalek") (req "= 1.0.0-pre.1") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "= 0.6.5") (default-features #t) (kind 0)) (crate-dep (name "x25519-dalek") (req "= 0.5.2") (default-features #t) (kind 0)))) (hash "0glfv60dcwm9sgjskgczsiiis70wpf5ffsah3fscfpvd1yf65kah")))

(define-public crate-ossuary-0.5 (crate (name "ossuary") (vers "0.5.1") (deps (list (crate-dep (name "chacha20-poly1305-aead") (req "= 0.1.2") (features (quote ("simd" "simd_opt"))) (default-features #t) (kind 0)) (crate-dep (name "ed25519-dalek") (req "= 1.0.0-pre.1") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "= 0.6.5") (default-features #t) (kind 0)) (crate-dep (name "x25519-dalek") (req "= 0.5.2") (default-features #t) (kind 0)))) (hash "028a99nqvcx062qxjy02msclqksq3h2w9vv4sfq61466293x0a5h")))

