(define-module (crates-io os r-) #:use-module (crates-io))

(define-public crate-osr-parser-0.1 (crate (name "osr-parser") (vers "0.1.0") (hash "0d96qd7gc2na24cr3ndjb7szw1gh9b551hw0c85r1xd7hx5smrp4") (yanked #t)))

(define-public crate-osr-parser-0.2 (crate (name "osr-parser") (vers "0.2.0") (hash "1763qv205nsvhg8pfzkcnil5mplwb5ww8pndqw366h5i9pf94c1a") (yanked #t)))

(define-public crate-osr-parser-0.3 (crate (name "osr-parser") (vers "0.3.0") (hash "16939apjai7mznzxar8bmz753bwi3cssdr8il52rjyiqhrha3j5h") (yanked #t)))

(define-public crate-osr-parser-0.4 (crate (name "osr-parser") (vers "0.4.0") (hash "0cgxa5ig5rv470ddmp1zqx2jq9s56lchr1vw834frivap678x7mr") (yanked #t)))

(define-public crate-osr-parser-0.5 (crate (name "osr-parser") (vers "0.5.0") (deps (list (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)))) (hash "0pbirgnck7fda5ii1kj0j1nq6rbgbk6y4szmiwz84fl5js11fvxa") (yanked #t) (v 2) (features2 (quote (("serde" "dep:serde"))))))

(define-public crate-osr-parser-0.6 (crate (name "osr-parser") (vers "0.6.0") (deps (list (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)))) (hash "1kpq83pi9wal5jpxd2vwm9dz650zmhs73ksq21azmvdgg4g9wc8x") (v 2) (features2 (quote (("serde" "dep:serde"))))))

(define-public crate-osr-parser-0.7 (crate (name "osr-parser") (vers "0.7.0") (deps (list (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)))) (hash "0pnhnzaydiydkmjja88axgvn590wkn43smkyny4k7608c0d3arc6") (v 2) (features2 (quote (("serde" "dep:serde"))))))

(define-public crate-osr-parser-0.7 (crate (name "osr-parser") (vers "0.7.1") (deps (list (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)))) (hash "166c5n3cwd0dgz74plcinlyqh6hpgs6wh5j034izm3q5cd89i1mz") (v 2) (features2 (quote (("serde" "dep:serde"))))))

(define-public crate-osr-parser-0.7 (crate (name "osr-parser") (vers "0.7.2") (deps (list (crate-dep (name "lzma") (req "^0.2.2") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)))) (hash "065mvsh6z8wxan3yb9vvbd8brx5xv2zn5ysmhr2nqlgrc6qzspb7") (v 2) (features2 (quote (("serde" "dep:serde") ("lzma" "dep:lzma"))))))

(define-public crate-osr-parser-0.7 (crate (name "osr-parser") (vers "0.7.3") (deps (list (crate-dep (name "lzma") (req "^0.2.2") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)))) (hash "1bzpps0sa67vph27dm5l1ni4zzijzw25ll5ryb4cjydm9127nlw5") (v 2) (features2 (quote (("serde" "dep:serde") ("lzma" "dep:lzma"))))))

(define-public crate-osr-parser-0.7 (crate (name "osr-parser") (vers "0.7.4") (deps (list (crate-dep (name "lzma") (req "^0.2.2") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)))) (hash "1ymadyxbnh20yy7zzjb8fzjznvaphszndci471sdf26ap22gr5b2") (v 2) (features2 (quote (("serde" "dep:serde") ("lzma" "dep:lzma"))))))

