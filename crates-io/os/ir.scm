(define-module (crates-io os ir) #:use-module (crates-io))

(define-public crate-osiris-0.0.0 (crate (name "osiris") (vers "0.0.0") (hash "0qpzvwa33c0289s8ygz1aadvgmdalzz9iijnxkfqqifplikdq5gr")))

(define-public crate-osiris-data-0.1 (crate (name "osiris-data") (vers "0.1.0") (hash "0ccjz2mb74cnfgy6rk4sc9phzxzf5wwkgz1qv96b6351aj8pb0rf")))

(define-public crate-osiris-data-0.1 (crate (name "osiris-data") (vers "0.1.1") (hash "0xcz9xfc0hphrlsjqhdfs82fwgl9kdp66lml5vmwf11jllr17pmr")))

(define-public crate-osiris-data-0.1 (crate (name "osiris-data") (vers "0.1.2") (hash "0ql673pqb5qx6ihm87ajgcq6rkvq9a4q96gfkwia23hwhzvp111p")))

(define-public crate-osiris-data-0.1 (crate (name "osiris-data") (vers "0.1.3") (hash "1lm44xf8dvzn66bpylmp2ldmsisrbbqf9qdr2xbik213p8w7vrhv")))

(define-public crate-osiris-data-0.1 (crate (name "osiris-data") (vers "0.1.4") (hash "0zhl90zmz8k4980zrc4gzjv269w4z9yrcax9khmxid29hf6y5ya3")))

(define-public crate-osiris-data-0.1 (crate (name "osiris-data") (vers "0.1.4-withmoredoc") (hash "0dn2b9i29bddrbjr5yp0nadsq4j1z6gmv33y51jjamgv4nis8x0c") (yanked #t)))

(define-public crate-osiris-data-0.1 (crate (name "osiris-data") (vers "0.1.5") (hash "1l73cqmrn8fbvslhgmrdd3d69q8lz3inn1b15z2hs2wj3s3qyaf2") (features (quote (("memory" "data") ("default" "memory") ("data"))))))

(define-public crate-osiris-data-0.1 (crate (name "osiris-data") (vers "0.1.6") (hash "0h0bfkhngwfbd5h2c3kaq0jrn6kp1gkh2b3z4z3hv7idfn6c7hjx") (features (quote (("memory" "data") ("default" "memory") ("data"))))))

(define-public crate-osiris-data-0.2 (crate (name "osiris-data") (vers "0.2.0") (hash "13vy4iwsd46ny3nln672sww7dbfl0a1fx9p2vb5ws5qznrrns7br") (features (quote (("memory" "data") ("default" "memory") ("data") ("converters" "data"))))))

(define-public crate-osiris-data-0.2 (crate (name "osiris-data") (vers "0.2.1") (hash "15cs4d4jzzkrbv9pcxajij9mynab3sbgix9cqvxndgx7dy8g60lw") (features (quote (("memory" "data") ("default" "memory") ("data") ("converters" "data"))))))

(define-public crate-osiris-display-0.1 (crate (name "osiris-display") (vers "0.1.0") (deps (list (crate-dep (name "osiris-data") (req "0.*") (default-features #t) (kind 0)) (crate-dep (name "osiris-typed") (req "0.*") (default-features #t) (kind 0)))) (hash "0s93ifdwb0jpxv8rv9hbczmzr9k23402915m1w5rp4id2czv2zfa")))

(define-public crate-osiris-display-0.1 (crate (name "osiris-display") (vers "0.1.1") (deps (list (crate-dep (name "osiris-data") (req "0.*") (default-features #t) (kind 0)) (crate-dep (name "osiris-typed") (req "0.*") (default-features #t) (kind 0)))) (hash "0fic48fqs2l79igbqc9p2ai8hmfmz5ld8yxzkx719z6q1jijxbsb")))

(define-public crate-osiris-process-0.1 (crate (name "osiris-process") (vers "0.1.0") (deps (list (crate-dep (name "osiris-data") (req "^0.1.2") (default-features #t) (kind 0)))) (hash "1y3qirs3j09v20y13iwkalqqry7lzd8r54hni7kxn8p2v2fy351x") (yanked #t)))

(define-public crate-osiris-process-0.1 (crate (name "osiris-process") (vers "0.1.1") (deps (list (crate-dep (name "osiris-data") (req "^0.1.2") (default-features #t) (kind 0)))) (hash "1rds028fw3mq8pl82ay7xgxcz160zdhjy63a5k5nb3bmk2qgcbpl") (yanked #t)))

(define-public crate-osiris-process-0.1 (crate (name "osiris-process") (vers "0.1.2") (deps (list (crate-dep (name "osiris-data") (req "^0.1.2") (default-features #t) (kind 0)))) (hash "014hg46xl08n55a8zci192kgay4kjphpijf76y3ygqav04gkif37") (yanked #t)))

(define-public crate-osiris-process-0.1 (crate (name "osiris-process") (vers "0.1.3") (deps (list (crate-dep (name "osiris-data") (req "^0.1") (default-features #t) (kind 0)))) (hash "06scp8x40y7070ybm5dxvs6g5qyvhs9lld0w8jwhpx3yd8v8m18y") (yanked #t)))

(define-public crate-osiris-process-0.1 (crate (name "osiris-process") (vers "0.1.4") (deps (list (crate-dep (name "osiris-data") (req "^0.1") (default-features #t) (kind 0)))) (hash "129r84nlngdlhnnlsf4wigva61yxb2ny8kq2xy691xkb2q9d0syz")))

(define-public crate-osiris-process-0.2 (crate (name "osiris-process") (vers "0.2.0") (deps (list (crate-dep (name "osiris-data") (req "^0.1") (default-features #t) (kind 0)))) (hash "0pdz2q08yq09fiphn01is6g9j591nc0397kza4slxncib2kzr7r3")))

(define-public crate-osiris-process-0.2 (crate (name "osiris-process") (vers "0.2.1") (deps (list (crate-dep (name "osiris-data") (req "^0.1") (default-features #t) (kind 0)))) (hash "1iyf96dl4c9g06qbbrmsh0639mcjhqizy44jzgzr3l9482308wfl")))

(define-public crate-osiris-process-0.2 (crate (name "osiris-process") (vers "0.2.2") (deps (list (crate-dep (name "osiris-data") (req "0.*") (default-features #t) (kind 0)))) (hash "1m28131w7wdrj27v3qzwc7ydwk5izpwgm2r2dxymf37jn1ah4hcz")))

(define-public crate-osiris-process-0.2 (crate (name "osiris-process") (vers "0.2.3") (deps (list (crate-dep (name "osiris-data") (req "0.*") (default-features #t) (kind 0)))) (hash "1bmrwkizhnz8sspijk918f9lnmy0rfpv8wn8lkbwkpbrd4nmiy26")))

(define-public crate-osiris-process-0.2 (crate (name "osiris-process") (vers "0.2.4") (deps (list (crate-dep (name "osiris-data") (req "0.*") (default-features #t) (kind 0)))) (hash "11cwhgw3j8dr7m8mc2p4dz0d8is1jhs6azn1b3nffa7vsh1v8fr0")))

(define-public crate-osiris-process-0.3 (crate (name "osiris-process") (vers "0.3.0") (deps (list (crate-dep (name "osiris-data") (req "0.*") (default-features #t) (kind 0)))) (hash "1gw88k467qadzm1hm6774k9dsa5f347370villr9pq7ijnwd1kpc") (features (quote (("state" "base-integral") ("processor" "communication" "base-floating" "state") ("default" "processor") ("communication") ("base-integral") ("base-floating"))))))

(define-public crate-osiris-process-0.3 (crate (name "osiris-process") (vers "0.3.1") (deps (list (crate-dep (name "osiris-data") (req "0.*") (default-features #t) (kind 0)))) (hash "03hlh5jndbkh4qdmas8gzxdhpwnm1c47j0x0sk1lps45v9f07wgm") (features (quote (("state" "base-integral") ("processor" "communication" "base-floating" "state") ("default" "processor") ("communication") ("base-integral") ("base-floating"))))))

(define-public crate-osiris-set-std-0.1 (crate (name "osiris-set-std") (vers "0.1.1") (deps (list (crate-dep (name "osiris-data") (req "^0.1.2") (default-features #t) (kind 0)) (crate-dep (name "osiris-process") (req "^0.1.2") (default-features #t) (kind 0)))) (hash "1z8nax3f48vpb33qqb3qylcmpi2x2aain6cqr512j97camxla1l4")))

(define-public crate-osiris-set-std-0.1 (crate (name "osiris-set-std") (vers "0.1.2") (deps (list (crate-dep (name "osiris-data") (req "^0.1.2") (default-features #t) (kind 0)) (crate-dep (name "osiris-process") (req "^0.1.2") (default-features #t) (kind 0)))) (hash "12h034ircan46q416mpxg9rd2lhpcj5nwxc53qr49bhic0w0gpsk")))

(define-public crate-osiris-set-std-0.1 (crate (name "osiris-set-std") (vers "0.1.3") (deps (list (crate-dep (name "osiris-data") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "osiris-process") (req "^0.1") (default-features #t) (kind 0)))) (hash "0hglq4xi5cqd6nzq78kzbwr0x50bfi4s33aih2rm9pr29x9hs5a4")))

(define-public crate-osiris-set-std-0.1 (crate (name "osiris-set-std") (vers "0.1.4") (deps (list (crate-dep (name "osiris-data") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "osiris-process") (req "^0.1") (default-features #t) (kind 0)))) (hash "1p775dzlgaqmcrrm8k1b9lh10wyakq7b6hsxzvdh63lv5l14mkk1")))

(define-public crate-osiris-set-std-0.1 (crate (name "osiris-set-std") (vers "0.1.5") (deps (list (crate-dep (name "osiris-data") (req "^0") (default-features #t) (kind 0)) (crate-dep (name "osiris-process") (req "0.*") (default-features #t) (kind 0)))) (hash "1d5b2r8nljnxdy44myi9vnjzvs8sa8wwfaqr1xb1ikym0rm9wzxl")))

(define-public crate-osiris-set-std-0.1 (crate (name "osiris-set-std") (vers "0.1.6") (deps (list (crate-dep (name "osiris-data") (req "^0") (default-features #t) (kind 0)) (crate-dep (name "osiris-process") (req "0.*") (default-features #t) (kind 0)))) (hash "163ir3xjfzcnpmrkn9licg7f1z6hk74kqx732kljn870g10rnqdp")))

(define-public crate-osiris-set-std-0.1 (crate (name "osiris-set-std") (vers "0.1.7") (deps (list (crate-dep (name "osiris-data") (req "0.*") (default-features #t) (kind 0)) (crate-dep (name "osiris-process") (req "0.*") (default-features #t) (kind 0)))) (hash "08i3f8wascgin9xbjix2jq7xvyzy4px86z1av5qnm27gd1ccinzf")))

(define-public crate-osiris-set-std-0.1 (crate (name "osiris-set-std") (vers "0.1.8") (deps (list (crate-dep (name "osiris-data") (req "0.*") (default-features #t) (kind 0)) (crate-dep (name "osiris-process") (req "0.*") (default-features #t) (kind 0)))) (hash "1kv004h0267gnggiqvd73g5amprw34ngqm1rv0arrggiimbsr0zn")))

(define-public crate-osiris-set-std-0.1 (crate (name "osiris-set-std") (vers "0.1.9") (deps (list (crate-dep (name "osiris-data") (req "0.*") (default-features #t) (kind 0)) (crate-dep (name "osiris-process") (req "0.*") (default-features #t) (kind 0)))) (hash "05n2vmr2cxac27n4pylrrh5q9iwyljlparq65dyf3zvfsqdxc6cq")))

(define-public crate-osiris-set-std-0.1 (crate (name "osiris-set-std") (vers "0.1.10") (deps (list (crate-dep (name "osiris-data") (req "0.*") (default-features #t) (kind 0)) (crate-dep (name "osiris-process") (req "0.*") (default-features #t) (kind 0)))) (hash "06j6nncx6mgwyjn07f8abm59a4s7xqff7m1bydf9gx1k4pqfpysm")))

(define-public crate-osiris-set-std-0.1 (crate (name "osiris-set-std") (vers "0.1.11") (deps (list (crate-dep (name "osiris-data") (req "0.*") (default-features #t) (kind 0)) (crate-dep (name "osiris-process") (req "0.*") (default-features #t) (kind 0)))) (hash "0ldlhps4wdsp96mz1yjvqn2giqsj8lsll5ipxlwr8gmg9igfa8fl")))

(define-public crate-osiris-set-std-0.1 (crate (name "osiris-set-std") (vers "0.1.12") (deps (list (crate-dep (name "osiris-data") (req "0.*") (default-features #t) (kind 0)) (crate-dep (name "osiris-process") (req "0.*") (default-features #t) (kind 0)))) (hash "0m5w5pq13kp1idmscwzw4qlkry1d3hxclwanq59l77z53xw0428y") (features (quote (("unsigned") ("registers") ("memory") ("logic") ("floating-point") ("default" "control" "registers" "memory" "unsigned") ("control"))))))

(define-public crate-osiris-set-std-0.1 (crate (name "osiris-set-std") (vers "0.1.13") (deps (list (crate-dep (name "osiris-data") (req "0.*") (default-features #t) (kind 0)) (crate-dep (name "osiris-process") (req "0.*") (default-features #t) (kind 0)))) (hash "04fndgy36kp6zpj2bv0zzz64ckmgbvmrz19s2mbdfxdl45lxmmbb") (features (quote (("unsigned") ("registers") ("memory") ("logic") ("floating-point") ("default" "control" "registers" "memory" "unsigned" "logic") ("control"))))))

(define-public crate-osiris-set-std-0.1 (crate (name "osiris-set-std") (vers "0.1.14") (deps (list (crate-dep (name "osiris-data") (req "0.*") (default-features #t) (kind 0)) (crate-dep (name "osiris-process") (req "0.*") (default-features #t) (kind 0)))) (hash "057g4v7bwrias0icy8000g3dklypg42c1m9fndbdrrr6gpic2kz5") (features (quote (("unsigned") ("registers") ("memory") ("logic") ("floating-point") ("default" "control" "registers" "memory" "unsigned" "logic") ("control"))))))

(define-public crate-osiris-set-std-0.1 (crate (name "osiris-set-std") (vers "0.1.15") (deps (list (crate-dep (name "osiris-data") (req "0.*") (default-features #t) (kind 0)) (crate-dep (name "osiris-process") (req "0.*") (default-features #t) (kind 0)))) (hash "12r58sfx0vda6p643k3s1lkv184qyp4c7sm7j87h4xmc095nycv2") (features (quote (("unsigned") ("registers") ("memory") ("logic") ("floating-point") ("default" "control" "registers" "memory" "unsigned" "logic") ("control"))))))

(define-public crate-osiris-set-std-0.1 (crate (name "osiris-set-std") (vers "0.1.16") (deps (list (crate-dep (name "osiris-data") (req "0.*") (default-features #t) (kind 0)) (crate-dep (name "osiris-process") (req "0.*") (default-features #t) (kind 0)))) (hash "1dskm505sql1kryf5x45ln17fjd4v314gc3xq0vglgs329a5mycs") (features (quote (("unsigned") ("registers") ("memory") ("logic") ("floating-point") ("default" "control" "registers" "memory" "unsigned" "logic") ("control"))))))

(define-public crate-osiris-set-std-0.1 (crate (name "osiris-set-std") (vers "0.1.17") (deps (list (crate-dep (name "osiris-data") (req "0.*") (features (quote ("converters"))) (default-features #t) (kind 0)) (crate-dep (name "osiris-process") (req "0.*") (default-features #t) (kind 0)))) (hash "0j33s2ky0ca1dad5dxyr1jwami6zvqc9y00nra5iv81wnk5bhhm7") (features (quote (("unsigned") ("registers") ("memory") ("logic") ("floating-point") ("default" "control" "registers" "memory" "unsigned" "logic") ("control"))))))

(define-public crate-osiris-typed-0.1 (crate (name "osiris-typed") (vers "0.1.0") (deps (list (crate-dep (name "osiris-data") (req "0.*") (features (quote ("data"))) (kind 0)) (crate-dep (name "osiris-process") (req "0.*") (features (quote ("base-floating"))) (kind 0)))) (hash "1bnvcdwf5xi4g4jjkx8iqczlskjm7hmsiw00a0730x53szvmml0w")))

(define-public crate-osiris-typed-0.1 (crate (name "osiris-typed") (vers "0.1.1") (deps (list (crate-dep (name "osiris-data") (req "0.*") (features (quote ("data"))) (kind 0)) (crate-dep (name "osiris-process") (req "0.*") (features (quote ("base-floating"))) (kind 0)))) (hash "0q7gkk0cvhfdsd8fk6y2q6avzf2d2jq9spbsq1bw3z31lvppymv7")))

(define-public crate-Osirisbot-0.1 (crate (name "Osirisbot") (vers "0.1.0") (deps (list (crate-dep (name "error-chain") (req "^0.12.4") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.8") (default-features #t) (kind 0)) (crate-dep (name "pretty_env_logger") (req "^0.4.0") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.3") (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.11.3") (default-features #t) (kind 0)) (crate-dep (name "scraper") (req "^0.12.0") (default-features #t) (kind 0)) (crate-dep (name "select") (req "^0.5.0") (default-features #t) (kind 0)) (crate-dep (name "teloxide") (req "^0.4") (features (quote ("auto-send" "macros"))) (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1") (features (quote ("full"))) (default-features #t) (kind 0)) (crate-dep (name "urlshortener") (req "^3.0.0") (default-features #t) (kind 0)))) (hash "07jyshi229wvafp8xjykkpq826j566gi8iryd2293fmqidfrxqk0")))

(define-public crate-Osirisbot-0.1 (crate (name "Osirisbot") (vers "0.1.1") (deps (list (crate-dep (name "error-chain") (req "^0.12.4") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.17") (default-features #t) (kind 0)) (crate-dep (name "pretty_env_logger") (req "^0.4.0") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.11.11") (default-features #t) (kind 0)) (crate-dep (name "scraper") (req "^0.13.0") (default-features #t) (kind 0)) (crate-dep (name "select") (req "^0.5.0") (default-features #t) (kind 0)) (crate-dep (name "teloxide") (req "^0.10") (features (quote ("auto-send" "macros"))) (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.20.0") (features (quote ("full"))) (default-features #t) (kind 0)) (crate-dep (name "urlshortener") (req "^3.0.0") (default-features #t) (kind 0)))) (hash "0n7c3p29ych9wr41hjcxw02dldgn9m7dskwn939w5s08y0nhvscg")))

