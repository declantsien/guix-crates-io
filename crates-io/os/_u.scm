(define-module (crates-io os _u) #:use-module (crates-io))

(define-public crate-os_units-0.1 (crate (name "os_units") (vers "0.1.0") (deps (list (crate-dep (name "x86_64") (req "^0.11.4") (default-features #t) (kind 0)))) (hash "0ifd6804b5iqc4x5iw6fbck3zsh642l23q3rsa3d3igf0m13byby")))

(define-public crate-os_units-0.1 (crate (name "os_units") (vers "0.1.1") (deps (list (crate-dep (name "x86_64") (req "^0.11.4") (default-features #t) (kind 0)))) (hash "0lwga4af9yyp8jk0calwkvn1p7z5zv9q7bf0fypkp6cq11k00baq")))

(define-public crate-os_units-0.1 (crate (name "os_units") (vers "0.1.2") (deps (list (crate-dep (name "x86_64") (req "^0.12.0") (default-features #t) (kind 0)))) (hash "1s6377p3pbpfb3rn3jjvgizw7gy693ss34jgg0ha3j10d59f5ds7")))

(define-public crate-os_units-0.1 (crate (name "os_units") (vers "0.1.3") (deps (list (crate-dep (name "x86_64") (req "^0.12.0") (default-features #t) (kind 0)))) (hash "0gwbbdgx1l3dxbrisrm0ylqjmfi347fad8nffg4sa9p15gy35cma")))

(define-public crate-os_units-0.1 (crate (name "os_units") (vers "0.1.4") (deps (list (crate-dep (name "x86_64") (req "^0.12.0") (default-features #t) (kind 0)))) (hash "163c8d46m90xmyk0rzv33bcnn6j2fyy1538j9bfcad8sg15nr0b6")))

(define-public crate-os_units-0.2 (crate (name "os_units") (vers "0.2.0") (deps (list (crate-dep (name "x86_64") (req "^0.12.0") (default-features #t) (kind 0)))) (hash "0vcdpgbjgwwnqfl6cx1lxk25lqpgjn4gw7hy1dvl1c97cd4f5l9c")))

(define-public crate-os_units-0.2 (crate (name "os_units") (vers "0.2.1") (deps (list (crate-dep (name "x86_64") (req "^0.12.0") (default-features #t) (kind 0)))) (hash "12zvi0a1bcxslhma3bgmm338yxp8gfkqz6j6g7cmn3fvnflx6dma")))

(define-public crate-os_units-0.2 (crate (name "os_units") (vers "0.2.2") (deps (list (crate-dep (name "x86_64") (req "^0.12.0") (default-features #t) (kind 0)))) (hash "094babhwfi2nrs0sdh759adbb9rpi2xnwjixwlrxvw8l01zxvgds")))

(define-public crate-os_units-0.2 (crate (name "os_units") (vers "0.2.3") (deps (list (crate-dep (name "x86_64") (req "^0.12.0") (default-features #t) (kind 0)))) (hash "08yp3zqv974mbfi56w8npk495iv6scxw9ik61k42fq336grjhd7i")))

(define-public crate-os_units-0.2 (crate (name "os_units") (vers "0.2.4") (deps (list (crate-dep (name "x86_64") (req "^0.12.0") (default-features #t) (kind 0)))) (hash "0r8rzckn2drv40000jmdplddpjxryvyid0cnny5z41r8lif27h8s")))

(define-public crate-os_units-0.2 (crate (name "os_units") (vers "0.2.5") (deps (list (crate-dep (name "x86_64") (req "^0.12.0") (default-features #t) (kind 0)))) (hash "1599qx0xkldc01vrhmz6gmjmldr3jfcj32090wqhmi7wljgfkl8w")))

(define-public crate-os_units-0.2 (crate (name "os_units") (vers "0.2.6") (deps (list (crate-dep (name "x86_64") (req "^0.12.0") (default-features #t) (kind 0)))) (hash "1ga3mxviil8hyv593cxs1461wzn1qqk5n8hr6kjv3fnd6kaf6gyw")))

(define-public crate-os_units-0.2 (crate (name "os_units") (vers "0.2.7") (deps (list (crate-dep (name "x86_64") (req "^0.13.0") (default-features #t) (kind 0)))) (hash "197sq16sj14dk4l615s37c9hp3wm7lmqgy1g2b8qga89d125c24n")))

(define-public crate-os_units-0.3 (crate (name "os_units") (vers "0.3.0") (deps (list (crate-dep (name "x86_64") (req "^0.13.3") (default-features #t) (kind 0)))) (hash "11infy8vqkcd67nibacb3vyalrzq54lm71r6s69lph1zylbwbx7x")))

(define-public crate-os_units-0.3 (crate (name "os_units") (vers "0.3.1") (deps (list (crate-dep (name "x86_64") (req "^0.14.0") (default-features #t) (kind 0)))) (hash "0nih03h0b9r1j0a1k9wvhc393072m3z2mp933nfa0q28pm2vhqnk")))

(define-public crate-os_units-0.4 (crate (name "os_units") (vers "0.4.0") (deps (list (crate-dep (name "x86_64") (req "^0.14.0") (kind 0)))) (hash "0jlnqichck3imwshiprngi2gr4mvsmsxa8n3fs8yzfafqczicfig")))

(define-public crate-os_units-0.4 (crate (name "os_units") (vers "0.4.1") (deps (list (crate-dep (name "x86_64") (req "^0.14.3") (kind 0)))) (hash "0xp50jmlc8kb9xrdlvlfdnw2z5nc3n7bnzrb1qqvbji0iic16lva")))

(define-public crate-os_units-0.4 (crate (name "os_units") (vers "0.4.2") (deps (list (crate-dep (name "x86_64") (req "^0.14.3") (kind 0)))) (hash "1wyz465r2j2jhm2hshjw1fx63hjfvlnp50dl6484pn6av9mdbxbn")))

(define-public crate-os_utils-0.0.1 (crate (name "os_utils") (vers "0.0.1") (deps (list (crate-dep (name "cfg-if") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "cocoa") (req "^0.18") (default-features #t) (target "cfg(target_os = \"macos\")") (kind 0)) (crate-dep (name "objc") (req "^0.2") (default-features #t) (target "cfg(target_os = \"macos\")") (kind 0)) (crate-dep (name "winapi") (req "^0.3") (features (quote ("minwindef" "sysinfoapi" "winnt"))) (default-features #t) (target "cfg(windows)") (kind 0)))) (hash "126s79p2qjjjzrr2hc2av59bgx1bz2vjz27csbxmbq5iqy9ydx6i")))

