(define-module (crates-io os oy) #:use-module (crates-io))

(define-public crate-osoy-0.2 (crate (name "osoy") (vers "0.2.3") (deps (list (crate-dep (name "dirs") (req "^2.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)))) (hash "1hmqbh608m2wv97hab50wr2vj9nz5jc9m7iw31zdhd1ndrad1r1i")))

(define-public crate-osoy-0.2 (crate (name "osoy") (vers "0.2.5-1") (deps (list (crate-dep (name "dirs") (req "^2.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)))) (hash "0fgba2zw1zzm49d6hnqykjwq1dak6sg2b57brskn6v80wk1w3z8g")))

(define-public crate-osoy-0.2 (crate (name "osoy") (vers "0.2.5-2") (deps (list (crate-dep (name "dirs") (req "^2.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)))) (hash "133zh0z21ipd5cjgs9qrjkzald1abgc9hxa650hbr7mly931ipcq")))

(define-public crate-osoy-0.2 (crate (name "osoy") (vers "0.2.5") (deps (list (crate-dep (name "dirs") (req "^2.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)))) (hash "1drlxrii8hkhdbwxvj28ais4cf5p045367p9x0al7a3mv0v4md30")))

(define-public crate-osoy-0.3 (crate (name "osoy") (vers "0.3.0") (deps (list (crate-dep (name "git2") (req "^0.13") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "termion") (req "^1.5") (default-features #t) (kind 0)))) (hash "0nin5wv0h3mscg01dyl4sbhdlqn39vlhkjblh8bqjpfs5z7mmkyq")))

(define-public crate-osoy-0.3 (crate (name "osoy") (vers "0.3.1") (deps (list (crate-dep (name "git2") (req "^0.13") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "termion") (req "^1.5") (default-features #t) (kind 0)))) (hash "0mq0nglg72afkcz4zz7ykp6vi6ag9mx4s2408xsnms891yqsgj5f")))

(define-public crate-osoy-0.4 (crate (name "osoy") (vers "0.4.0") (deps (list (crate-dep (name "git2") (req "^0.13") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "termion") (req "^1.5") (default-features #t) (kind 0)))) (hash "12zngsnk6xm2bjr5vzk6vnq6ff06p8lhnhi954l4g0xkb05r75m7")))

(define-public crate-osoy-0.4 (crate (name "osoy") (vers "0.4.1") (deps (list (crate-dep (name "git2") (req "^0.13") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "termion") (req "^1.5") (default-features #t) (kind 0)))) (hash "0aik5vp0jx8rcayij4rcaqjlgmjm1g1hlaaxngjm83qrach3djzh")))

(define-public crate-osoy-0.5 (crate (name "osoy") (vers "0.5.0") (deps (list (crate-dep (name "git2") (req "^0.13") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "termion") (req "^1.5") (default-features #t) (kind 0)))) (hash "16cf5ng34kxmiij5jwcha7sy3f2adr85ad9ba9vwlcca5kqdmzfb")))

(define-public crate-osoy-0.5 (crate (name "osoy") (vers "0.5.1") (deps (list (crate-dep (name "git2") (req "^0.13") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "termion") (req "^1.5") (default-features #t) (kind 0)))) (hash "1y1cbs161hkn7km4wc65lj0ymsd6wh8nxbmrqh8h30v2bn8c42ii")))

