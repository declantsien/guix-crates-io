(define-module (crates-io os -p) #:use-module (crates-io))

(define-public crate-os-prober-lite-0.1 (crate (name "os-prober-lite") (vers "0.1.0") (deps (list (crate-dep (name "json") (req "^0.11.13") (default-features #t) (kind 0)))) (hash "11jdyy491ajyaw95m3x27pwdjhj7qrd30aa68ix1d62kwlbhqg07")))

