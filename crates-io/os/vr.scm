(define-module (crates-io os vr) #:use-module (crates-io))

(define-public crate-osvr-0.1 (crate (name "osvr") (vers "0.1.0") (deps (list (crate-dep (name "gl") (req "^0.6.3") (default-features #t) (kind 0)) (crate-dep (name "osvr-sys") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "sdl2") (req "^0.27") (default-features #t) (kind 0)))) (hash "08fw0j8bi7jms9wl17zy0593fpxpdfyiw1zv8s5hpya501ryxcr9")))

(define-public crate-osvr-0.1 (crate (name "osvr") (vers "0.1.1") (deps (list (crate-dep (name "gl") (req "^0.6.3") (default-features #t) (kind 0)) (crate-dep (name "osvr-sys") (req "^0.1.1") (default-features #t) (kind 0)) (crate-dep (name "sdl2") (req "^0.27") (default-features #t) (kind 0)))) (hash "1n69hb1vp317l1rcrrwvvbm8nj2n8za414cz7p7imldb333zynsj")))

(define-public crate-osvr-sys-0.1 (crate (name "osvr-sys") (vers "0.1.0") (deps (list (crate-dep (name "bindgen") (req "^0.29.0") (default-features #t) (kind 1)))) (hash "0ppgcnw5qsjd8ac8kpk5y8j7x55vhf66srr76z4dnq89qzagjmwm")))

(define-public crate-osvr-sys-0.1 (crate (name "osvr-sys") (vers "0.1.1") (deps (list (crate-dep (name "bindgen") (req "^0.29.0") (default-features #t) (kind 1)) (crate-dep (name "sdl2") (req "^0.27") (default-features #t) (kind 0)))) (hash "0l429s825xjz82xks7a7wvjihlr4z797f8nmlahi38ww7h94jcx6")))

