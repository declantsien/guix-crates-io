(define-module (crates-io os ex) #:use-module (crates-io))

(define-public crate-OSEXave-0.0.1 (crate (name "OSEXave") (vers "0.0.1") (hash "04g8yyd25wbs1v16wh8iyv8pny76ggaqmswpycfl4293917v9z6x") (features (quote (("default")))) (yanked #t)))

(define-public crate-OSEXave-0.0.2 (crate (name "OSEXave") (vers "0.0.2") (hash "18dx1xgyirxnpyzw094d13x7sh45b7869a3qg3sqjhfr6yr5aycl") (features (quote (("default")))) (yanked #t)))

(define-public crate-OSEXave-0.0.3 (crate (name "OSEXave") (vers "0.0.3") (hash "0j8bxw7xfj7rxk39girv4n38lj2xyrx3wajjrvg2m6m0cixp08ms") (features (quote (("default")))) (yanked #t)))

(define-public crate-OSEXave-0.0.4 (crate (name "OSEXave") (vers "0.0.4") (hash "1x52ilnxrl8x4qgfhiyvdq9gqqr5acayh2fi2shyq2ln393a3j9r") (features (quote (("default")))) (yanked #t)))

(define-public crate-OSEXave-0.0.5 (crate (name "OSEXave") (vers "0.0.5") (hash "0jxqa58f14anhy7am19r7bk5kwdvfriqjdmgkhip52d074kq9894") (features (quote (("default")))) (yanked #t)))

(define-public crate-OSEXave-0.0.6 (crate (name "OSEXave") (vers "0.0.6") (hash "0fh7ld57h0d5x2rajnnd55w689n019jqsm6dwak3n79vni5dx3nh") (features (quote (("default")))) (yanked #t)))

(define-public crate-OSEXave-0.0.7 (crate (name "OSEXave") (vers "0.0.7") (hash "0kw4xxrfw4468sgzzflhmfh02z2knkg64h5zkwxgh13jwzwg9l0w") (features (quote (("default"))))))

