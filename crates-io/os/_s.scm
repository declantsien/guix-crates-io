(define-module (crates-io os _s) #:use-module (crates-io))

(define-public crate-os_socketaddr-0.1 (crate (name "os_socketaddr") (vers "0.1.0") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "15s382qxd4dp5qpna2rknj3b1k0vv77npxpq0nqv51lw9i2bfx4a") (yanked #t)))

(define-public crate-os_socketaddr-0.1 (crate (name "os_socketaddr") (vers "0.1.1") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "0mr063ziyz4w5mvx1b04mzr2sq6xfh3wd9rpc0wns6xnhj7jhs5h") (yanked #t)))

(define-public crate-os_socketaddr-0.2 (crate (name "os_socketaddr") (vers "0.2.0") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "winapi") (req "^0.3") (features (quote ("winsock2" "ws2def" "ws2ipdef" "ws2tcpip"))) (default-features #t) (target "cfg(target_family = \"windows\")") (kind 0)))) (hash "1llv37bpwkj7ia222h6kgkksqqa7jcmp229zrkzzj0vrwvg8dzw7") (yanked #t)))

(define-public crate-os_socketaddr-0.2 (crate (name "os_socketaddr") (vers "0.2.1") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "winapi") (req "^0.3") (features (quote ("winsock2" "ws2def" "ws2ipdef" "ws2tcpip"))) (default-features #t) (target "cfg(target_family = \"windows\")") (kind 0)))) (hash "1ys5l3hrnkrpf4alp1ca2z0r8b86sak7w833v0aaj5dax5zqg410") (yanked #t)))

(define-public crate-os_socketaddr-0.2 (crate (name "os_socketaddr") (vers "0.2.2") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "winapi") (req "^0.3") (features (quote ("winsock2" "ws2def" "ws2ipdef" "ws2tcpip"))) (default-features #t) (target "cfg(target_family = \"windows\")") (kind 0)))) (hash "160nz7psyv32mgyy4xl02v5bsgwjnwz5dx90n6jkx99wpqi63dis")))

(define-public crate-os_socketaddr-0.2 (crate (name "os_socketaddr") (vers "0.2.3") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "winapi") (req "^0.3") (features (quote ("winsock2" "ws2def" "ws2ipdef" "ws2tcpip"))) (default-features #t) (target "cfg(target_family = \"windows\")") (kind 0)))) (hash "1gqxh9q7imh7gjz5s7rw85jii1af7l66k8r0xnwjpdylk48g0p26")))

(define-public crate-os_socketaddr-0.2 (crate (name "os_socketaddr") (vers "0.2.4") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "winapi") (req "^0.3") (features (quote ("winsock2" "ws2def" "ws2ipdef" "ws2tcpip"))) (default-features #t) (target "cfg(target_family = \"windows\")") (kind 0)))) (hash "0c9wqkgqrz93gd5042hcdgkj4c3n2hbkhx5i47kc9xy8nhgnl5g9")))

(define-public crate-os_socketaddr-0.2 (crate (name "os_socketaddr") (vers "0.2.5") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "winapi") (req "^0.3") (features (quote ("winsock2" "ws2def" "ws2ipdef" "ws2tcpip"))) (default-features #t) (target "cfg(target_family = \"windows\")") (kind 0)))) (hash "0i5jsj298k8g0zmx9lbklflki83f3vbzr443jr0ypbcwq0z9y57s")))

(define-public crate-os_stat-0.1 (crate (name "os_stat") (vers "0.1.0") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "1cpy624y28p4fy3l2iwls4jbhw50r8akmfjnpyhsh0dwdpw2c58w")))

(define-public crate-os_str-0.0.0 (crate (name "os_str") (vers "0.0.0") (hash "1czynx6a6zx49fisznw4ig4hrbpmlxlp7j7hi44lb9my5sbk5srk") (yanked #t)))

(define-public crate-os_str-0.0.1 (crate (name "os_str") (vers "0.0.1") (hash "1ibychpsnlkg3j4gfkbsh37irmbs4sil1i68r3mgf2a0q4lf6553") (yanked #t)))

(define-public crate-os_str_bytes-0.1 (crate (name "os_str_bytes") (vers "0.1.0") (deps (list (crate-dep (name "getrandom") (req "^0.1.13") (default-features #t) (kind 2)))) (hash "1iw0x2rahf8xwllqvi2sg3d1561caxnvlwnm1y1044ccpabqycdn") (yanked #t)))

(define-public crate-os_str_bytes-0.1 (crate (name "os_str_bytes") (vers "0.1.1") (deps (list (crate-dep (name "getrandom") (req "^0.1.13") (default-features #t) (kind 2)))) (hash "16sg2mcbdhzqlka6s8cc6pn1w5zpzq2ps7hc58gxll5xizynv33q") (yanked #t)))

(define-public crate-os_str_bytes-0.1 (crate (name "os_str_bytes") (vers "0.1.2") (deps (list (crate-dep (name "getrandom") (req "^0.1.13") (default-features #t) (kind 2)))) (hash "1xbikxs2sqsphjkv7nik4ywivirlizgp6h8h00qgplr1iqfbix8n") (yanked #t)))

(define-public crate-os_str_bytes-0.1 (crate (name "os_str_bytes") (vers "0.1.3") (deps (list (crate-dep (name "getrandom") (req "^0.1.13") (default-features #t) (kind 2)))) (hash "13z61l5i18sbzfi3ar1j7iz0kiy2viajais2cn5wg7j8vp5vid0g") (yanked #t)))

(define-public crate-os_str_bytes-0.2 (crate (name "os_str_bytes") (vers "0.2.0") (deps (list (crate-dep (name "getrandom") (req "^0.1.13") (default-features #t) (kind 2)))) (hash "05n3a6q8r691nqy66rh4zk0fq9fii4wfq7gv7gyap9h5c25jhhy1") (yanked #t)))

(define-public crate-os_str_bytes-0.2 (crate (name "os_str_bytes") (vers "0.2.1") (deps (list (crate-dep (name "getrandom") (req "^0.1.13") (default-features #t) (kind 2)))) (hash "0l3qvj8gw254s7s8qgi0xsh8wb36da87ma45ljnllv5wl3idv6g6") (yanked #t)))

(define-public crate-os_str_bytes-0.3 (crate (name "os_str_bytes") (vers "0.3.0") (deps (list (crate-dep (name "getrandom") (req "^0.1.13") (default-features #t) (kind 2)))) (hash "1rqb591jbmljila1jrlan6zrk2d2hjbvwpn54hz0zwb7mwai30h1") (yanked #t)))

(define-public crate-os_str_bytes-1 (crate (name "os_str_bytes") (vers "1.0.0") (deps (list (crate-dep (name "getrandom") (req "^0.1") (default-features #t) (kind 2)))) (hash "1ydpjczbc7blniva0j2nbg90v8xgjm77zxy6cgl6wlfpblhrxixz") (yanked #t)))

(define-public crate-os_str_bytes-1 (crate (name "os_str_bytes") (vers "1.0.1") (deps (list (crate-dep (name "getrandom") (req "^0.1") (default-features #t) (kind 2)))) (hash "0lbad4gdxh0w7ynsq6brss5px18qqbpnb4l64pzhq6nwkvsagl2i") (yanked #t)))

(define-public crate-os_str_bytes-1 (crate (name "os_str_bytes") (vers "1.0.2") (deps (list (crate-dep (name "getrandom") (req "^0.1") (default-features #t) (kind 2)))) (hash "06z9652lrbkmkjsf1xgvaq7y7d0rbwwkri8ybphcdqfgr83535l8") (yanked #t)))

(define-public crate-os_str_bytes-1 (crate (name "os_str_bytes") (vers "1.0.3") (deps (list (crate-dep (name "getrandom") (req "^0.1") (default-features #t) (kind 2)))) (hash "0wcjwvn804pw7mh844pcr2r5afli5k86mrv58yif3fsijji5w3ix") (yanked #t)))

(define-public crate-os_str_bytes-1 (crate (name "os_str_bytes") (vers "1.1.0") (deps (list (crate-dep (name "getrandom") (req "^0.1") (default-features #t) (kind 2)))) (hash "1554vj5ia6p7h6ggxjmd3df743qg2fc8lx9bswxjks9a7ah0jgpi") (yanked #t)))

(define-public crate-os_str_bytes-2 (crate (name "os_str_bytes") (vers "2.0.0") (deps (list (crate-dep (name "getrandom") (req "^0.1") (default-features #t) (kind 2)))) (hash "139vdl5g3j9jd2wmg0qyzadrk6sr4jp0sy7b9cj9hql5qr0c88kb")))

(define-public crate-os_str_bytes-2 (crate (name "os_str_bytes") (vers "2.1.0") (deps (list (crate-dep (name "getrandom") (req "^0.1") (default-features #t) (kind 2)))) (hash "00cmbpc3lwz177ffk8byczsm2jc0wh4z86474vpm0892rk9pkj94")))

(define-public crate-os_str_bytes-2 (crate (name "os_str_bytes") (vers "2.2.0") (deps (list (crate-dep (name "getrandom") (req "^0.1") (default-features #t) (kind 2)))) (hash "186vrahmmij55jfy9anjhj6y3lm73xfwwmjzvwfx6jwffrz97bas")))

(define-public crate-os_str_bytes-2 (crate (name "os_str_bytes") (vers "2.2.1") (deps (list (crate-dep (name "getrandom") (req "^0.1") (default-features #t) (kind 2)))) (hash "1r5hxdbcx27iix27aqkmjx0x5rpmsx0h5qq95vldjmkknlx242h3")))

(define-public crate-os_str_bytes-2 (crate (name "os_str_bytes") (vers "2.3.0") (deps (list (crate-dep (name "getrandom") (req "^0.1") (default-features #t) (kind 2)))) (hash "1m043n2l8g0fkk89baygm6j2dwkk6g1qff4l5cbshqwv8ghi5nff") (features (quote (("raw"))))))

(define-public crate-os_str_bytes-2 (crate (name "os_str_bytes") (vers "2.3.1") (deps (list (crate-dep (name "getrandom") (req "^0.1") (default-features #t) (kind 2)))) (hash "041m0bp3cbmfwrkpw1jbf0qh5faysgndi6i1ji68qz9l92w4gph6") (features (quote (("raw"))))))

(define-public crate-os_str_bytes-2 (crate (name "os_str_bytes") (vers "2.3.2") (deps (list (crate-dep (name "getrandom") (req "^0.1") (default-features #t) (kind 2)))) (hash "1b5fdm9an92vcdj6jgwapaxalhcrygjbngisjlwy60gp70szxiia") (features (quote (("raw"))))))

(define-public crate-os_str_bytes-2 (crate (name "os_str_bytes") (vers "2.4.0") (deps (list (crate-dep (name "getrandom") (req "^0.1") (default-features #t) (kind 2)))) (hash "11agh8n3x2l4sr3sxvx6byc1j3ryb1g6flb1ywn0qhq7xv1y3cmg") (features (quote (("raw"))))))

(define-public crate-os_str_bytes-3 (crate (name "os_str_bytes") (vers "3.0.0") (deps (list (crate-dep (name "getrandom") (req "^0.2") (features (quote ("js"))) (default-features #t) (kind 2)))) (hash "04phgadwzxqjc4fxa5f4fnickqrhf3gignqhy2yn38mfcn4md4z2") (features (quote (("raw"))))))

(define-public crate-os_str_bytes-3 (crate (name "os_str_bytes") (vers "3.1.0") (deps (list (crate-dep (name "getrandom") (req "^0.2") (features (quote ("js"))) (default-features #t) (kind 2)))) (hash "0bfgm53jgdacylwd6ynjhciczmnlrp45p98h0nsrmrhglrcfzjva") (features (quote (("raw"))))))

(define-public crate-os_str_bytes-4 (crate (name "os_str_bytes") (vers "4.0.0") (deps (list (crate-dep (name "getrandom") (req "^0.2") (features (quote ("js"))) (default-features #t) (kind 2)) (crate-dep (name "print_bytes") (req "^0.4") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "uniquote") (req "^2.0") (optional #t) (default-features #t) (kind 0)))) (hash "1yq1khxhz0c13plj421gqs5m9b27m9cj4rhy9p1m0vhjdg8jd7bl") (features (quote (("raw_os_str") ("default" "raw_os_str"))))))

(define-public crate-os_str_bytes-4 (crate (name "os_str_bytes") (vers "4.1.0") (deps (list (crate-dep (name "getrandom") (req "^0.2") (features (quote ("js"))) (default-features #t) (kind 2)) (crate-dep (name "print_bytes") (req "^0.4") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "uniquote") (req "^2.0") (optional #t) (default-features #t) (kind 0)))) (hash "12cxcnccc324ahkcjrhpbw6vjlx41waphqvx1a2cdk6f6ip4aabi") (features (quote (("raw_os_str") ("default" "raw_os_str"))))))

(define-public crate-os_str_bytes-4 (crate (name "os_str_bytes") (vers "4.1.1") (deps (list (crate-dep (name "getrandom") (req "^0.2") (features (quote ("js"))) (default-features #t) (kind 2)) (crate-dep (name "print_bytes") (req "^0.4") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "uniquote") (req "^2.0") (optional #t) (default-features #t) (kind 0)))) (hash "0ns7pa7nvhi9x4z46vrf0jhjs9763fnqzxmalryrp83fr4jbnjij") (features (quote (("raw_os_str") ("default" "raw_os_str"))))))

(define-public crate-os_str_bytes-4 (crate (name "os_str_bytes") (vers "4.2.0") (deps (list (crate-dep (name "getrandom") (req "^0.2") (features (quote ("js"))) (default-features #t) (kind 2)) (crate-dep (name "memchr") (req "^2.4") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "print_bytes") (req "^0.4") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "uniquote") (req "^3.0") (optional #t) (default-features #t) (kind 0)))) (hash "16d70qzd2g18i28i6znjcpck0r9hjd5gz5qcr1cl2l9s6d1sknmd") (features (quote (("raw_os_str") ("default" "memchr" "raw_os_str"))))))

(define-public crate-os_str_bytes-5 (crate (name "os_str_bytes") (vers "5.0.0") (deps (list (crate-dep (name "getrandom") (req "^0.2") (default-features #t) (kind 2)) (crate-dep (name "memchr") (req "^2.4") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "print_bytes") (req "^0.5") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "uniquote") (req "^3.0") (optional #t) (default-features #t) (kind 0)))) (hash "157lzhx2yyb793pgah486l3qbpdwm8r0zpir54wj0vl9xjlliigf") (features (quote (("raw_os_str") ("deprecated-byte-patterns") ("default" "memchr" "raw_os_str")))) (rust-version "1.51.0")))

(define-public crate-os_str_bytes-6 (crate (name "os_str_bytes") (vers "6.0.0") (deps (list (crate-dep (name "getrandom") (req "^0.2") (default-features #t) (kind 2)) (crate-dep (name "memchr") (req "^2.4") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "print_bytes") (req "^0.5") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "uniquote") (req "^3.0") (optional #t) (default-features #t) (kind 0)))) (hash "0r5z5xds2wzzqlqjaw96dpjsz5nqyzc1rflm4mh09aa32qyl88lf") (features (quote (("raw_os_str") ("default" "memchr" "raw_os_str")))) (rust-version "1.52.0")))

(define-public crate-os_str_bytes-6 (crate (name "os_str_bytes") (vers "6.0.1") (deps (list (crate-dep (name "getrandom") (req "^0.2") (default-features #t) (kind 2)) (crate-dep (name "memchr") (req "^2.4") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "print_bytes") (req "^0.5") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "uniquote") (req "^3.0") (optional #t) (default-features #t) (kind 0)))) (hash "0ddl4664vycbxd1fbzgkxm9gk3vky9v9d9yw57g2k0hr5w5qv782") (features (quote (("raw_os_str") ("default" "memchr" "raw_os_str")))) (rust-version "1.52.0")))

(define-public crate-os_str_bytes-6 (crate (name "os_str_bytes") (vers "6.1.0") (deps (list (crate-dep (name "getrandom") (req "^0.2") (default-features #t) (kind 2)) (crate-dep (name "memchr") (req "^2.4") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "print_bytes") (req "^0.5") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "uniquote") (req "^3.0") (optional #t) (default-features #t) (kind 0)))) (hash "1ykav0kv9152z4cmv8smwmd9pac9q42sihi4wphnrzlwx4c6hci1") (features (quote (("raw_os_str") ("default" "memchr" "raw_os_str")))) (rust-version "1.52.0")))

(define-public crate-os_str_bytes-6 (crate (name "os_str_bytes") (vers "6.2.0") (deps (list (crate-dep (name "getrandom") (req "^0.2") (default-features #t) (kind 2)) (crate-dep (name "memchr") (req "^2.4") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "print_bytes") (req "^0.6") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "uniquote") (req "^3.0") (optional #t) (default-features #t) (kind 0)))) (hash "1m1w958pj5cgj3m0n7x9ifpr0pd8ils8wxpaihni1h6mwpph3034") (features (quote (("raw_os_str") ("default" "memchr" "raw_os_str")))) (rust-version "1.57.0")))

(define-public crate-os_str_bytes-6 (crate (name "os_str_bytes") (vers "6.3.0") (deps (list (crate-dep (name "getrandom") (req "^0.2") (default-features #t) (kind 2)) (crate-dep (name "memchr") (req "^2.4") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "print_bytes") (req "^0.6") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "uniquote") (req "^3.0") (optional #t) (default-features #t) (kind 0)))) (hash "1zqfx1ajwmv3g9kp95v18zwpjm2fkq6rxpsib0ig3zz3k9g43xwz") (features (quote (("raw_os_str") ("default" "memchr" "raw_os_str") ("checked_conversions")))) (rust-version "1.57.0")))

(define-public crate-os_str_bytes-6 (crate (name "os_str_bytes") (vers "6.3.1") (deps (list (crate-dep (name "getrandom") (req "^0.2") (default-features #t) (kind 2)) (crate-dep (name "memchr") (req "^2.4") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "print_bytes") (req "^0.7") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "uniquote") (req "^3.0") (optional #t) (default-features #t) (kind 0)))) (hash "1afyx7wwn34y3ck0s5k8m9w9c7drc8nb9k6n1pmx4nakkkirdbrv") (features (quote (("raw_os_str") ("default" "memchr" "raw_os_str") ("checked_conversions")))) (rust-version "1.57.0")))

(define-public crate-os_str_bytes-6 (crate (name "os_str_bytes") (vers "6.4.0") (deps (list (crate-dep (name "getrandom") (req "^0.2") (default-features #t) (kind 2)) (crate-dep (name "memchr") (req "^2.4") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "print_bytes") (req "^0.7") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "uniquote") (req "^3.0") (optional #t) (default-features #t) (kind 0)))) (hash "0zpdkd14921yh96kzsfxxx0w65lfl3vnn62izzirw7j18xsg4nvv") (features (quote (("raw_os_str") ("default" "memchr" "raw_os_str") ("checked_conversions")))) (yanked #t) (rust-version "1.57.0")))

(define-public crate-os_str_bytes-6 (crate (name "os_str_bytes") (vers "6.4.1") (deps (list (crate-dep (name "getrandom") (req "^0.2") (default-features #t) (kind 2)) (crate-dep (name "memchr") (req "^2.4") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "print_bytes") (req "^0.7") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "uniquote") (req "^3.0") (optional #t) (default-features #t) (kind 0)))) (hash "1vi3wx4zs8wgfhhzbk1n279gn8yp0n4l8s8wyb4mfm7avawj0y4v") (features (quote (("raw_os_str") ("default" "memchr" "raw_os_str") ("checked_conversions")))) (rust-version "1.57.0")))

(define-public crate-os_str_bytes-6 (crate (name "os_str_bytes") (vers "6.5.0") (deps (list (crate-dep (name "fastrand") (req "^1.8") (default-features #t) (kind 2)) (crate-dep (name "memchr") (req "^2.4") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "print_bytes") (req "^1.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "uniquote") (req "^3.0") (optional #t) (default-features #t) (kind 0)))) (hash "0rz2711gl575ng6vm9a97q42wqnf4wk1165wn221jb8gn17z9vff") (features (quote (("raw_os_str") ("default" "memchr" "raw_os_str") ("checked_conversions")))) (rust-version "1.57.0")))

(define-public crate-os_str_bytes-6 (crate (name "os_str_bytes") (vers "6.5.1") (deps (list (crate-dep (name "fastrand") (req "^2.0") (default-features #t) (kind 2)) (crate-dep (name "memchr") (req "^2.4") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "print_bytes") (req "^1.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "uniquote") (req "^3.0") (optional #t) (default-features #t) (kind 0)))) (hash "1b1yrz32030z2xz3r1ybwyvd98ip8sww4vgr5smfjkhp9fqrwpad") (features (quote (("raw_os_str") ("default" "memchr" "raw_os_str") ("checked_conversions")))) (rust-version "1.57.0")))

(define-public crate-os_str_bytes-6 (crate (name "os_str_bytes") (vers "6.6.0") (deps (list (crate-dep (name "fastrand") (req "^2.0") (default-features #t) (kind 2)) (crate-dep (name "lazy_static") (req "^1.4") (default-features #t) (kind 2)) (crate-dep (name "memchr") (req "^2.4") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "print_bytes") (req "^1.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "uniquote") (req "^3.0") (optional #t) (default-features #t) (kind 0)))) (hash "1j2srbxn992qv1xjf2rrxvaxr2s23sr75v5lssd0jajj8k1vlzjv") (features (quote (("raw_os_str") ("nightly") ("default" "memchr" "raw_os_str") ("conversions") ("checked_conversions" "conversions")))) (rust-version "1.61.0")))

(define-public crate-os_str_bytes-6 (crate (name "os_str_bytes") (vers "6.6.1") (deps (list (crate-dep (name "fastrand") (req "^2.0") (default-features #t) (kind 2)) (crate-dep (name "lazy_static") (req "^1.4") (default-features #t) (kind 2)) (crate-dep (name "memchr") (req "^2.4") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "print_bytes") (req "^1.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "uniquote") (req "^3.0") (optional #t) (default-features #t) (kind 0)))) (hash "1885z1x4sm86v5p41ggrl49m58rbzzhd1kj72x46yy53p62msdg2") (features (quote (("raw_os_str") ("nightly") ("default" "memchr" "raw_os_str") ("conversions") ("checked_conversions" "conversions")))) (rust-version "1.61.0")))

(define-public crate-os_str_bytes-7 (crate (name "os_str_bytes") (vers "7.0.0-beta.0") (deps (list (crate-dep (name "fastrand") (req "^2.0") (default-features #t) (kind 2)) (crate-dep (name "lazy_static") (req "^1.4") (default-features #t) (kind 2)) (crate-dep (name "memchr") (req "^2.4") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "tempfile") (req "^3.2") (default-features #t) (kind 2)))) (hash "0k3qjiggjynlhn6chc133bmaad3aqf126adfrp3lhyk7c5hsx769") (features (quote (("raw_os_str") ("default" "memchr" "raw_os_str") ("conversions") ("checked_conversions" "conversions")))) (rust-version "1.74.0")))

(define-public crate-os_str_bytes-7 (crate (name "os_str_bytes") (vers "7.0.0") (deps (list (crate-dep (name "fastrand") (req "^2.0") (default-features #t) (kind 2)) (crate-dep (name "lazy_static") (req "^1.0.3") (default-features #t) (kind 2)) (crate-dep (name "memchr") (req "^2.3.5") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "tempfile") (req "^3.8") (default-features #t) (kind 2)))) (hash "1fg04rslyvzrrpb99n1shx4y60a747f81gdln6cwfxzm9aclri3s") (features (quote (("raw_os_str") ("default" "memchr" "raw_os_str") ("conversions") ("checked_conversions" "conversions")))) (rust-version "1.74.0")))

