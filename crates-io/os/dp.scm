(define-module (crates-io os dp) #:use-module (crates-io))

(define-public crate-osdp-0.1 (crate (name "osdp") (vers "0.1.0") (hash "0r8mh0cyq7kyba16qr6gwrp0875550l6a7q2d179aq6n0nv2iisp")))

(define-public crate-osdp-0.1 (crate (name "osdp") (vers "0.1.1") (hash "0hjmcln1l6923f4j0j240pcmcd6w0ydp86ssl2j5wp2gjnbpqx5l")))

(define-public crate-osdp-0.1 (crate (name "osdp") (vers "0.1.2") (hash "1lcfhcql2dmk5dvh6b03rkjni8pz4sz1xla5jl6hx9n02iqcjbd5")))

(define-public crate-osdp-0.1 (crate (name "osdp") (vers "0.1.3") (deps (list (crate-dep (name "crc16") (req "^0.4.0") (default-features #t) (kind 0)))) (hash "0zk9cqja0mj4r34bbrp1yq0vs5ar0hfanbgk0cfa0izndnxw7m9f")))

(define-public crate-osdp-0.1 (crate (name "osdp") (vers "0.1.4") (deps (list (crate-dep (name "crc16") (req "^0.4.0") (default-features #t) (kind 0)))) (hash "0j8p8q0hlif43zs4x60wsfxd8rcivc5mv24gzsr5ir4zg721b2zj")))

(define-public crate-osdp-0.1 (crate (name "osdp") (vers "0.1.5") (deps (list (crate-dep (name "crc16") (req "^0.4.0") (default-features #t) (kind 0)))) (hash "1r2h2zq8dn9ds9fw3ka2kfjxa1mfg8nql0rviky4illz8866zzvf")))

(define-public crate-osdp-0.1 (crate (name "osdp") (vers "0.1.6") (deps (list (crate-dep (name "crc16") (req "^0.4.0") (default-features #t) (kind 0)))) (hash "0dan0ccnhqq3bahjyi8hraj53labl64lipcvxnbb56s8wpf0gip7")))

(define-public crate-osdp-0.1 (crate (name "osdp") (vers "0.1.7") (deps (list (crate-dep (name "crc16") (req "^0.4.0") (default-features #t) (kind 0)))) (hash "0v14jmjrrsbxnlz9lm279hrjhwjd5h2xvgcyq3ymrjlkyqx8yh0h")))

(define-public crate-osdp-0.1 (crate (name "osdp") (vers "0.1.8") (deps (list (crate-dep (name "crc16") (req "^0.4.0") (default-features #t) (kind 0)))) (hash "1mvzbqi69zj0ia331lp85jafp60jivjxhmcdyfxlza85l2s98smr")))

(define-public crate-osdp-0.1 (crate (name "osdp") (vers "0.1.9") (deps (list (crate-dep (name "crc16") (req "^0.4.0") (default-features #t) (kind 0)))) (hash "1xmn9v8cp9bhcp5nawlcb1qjbikjfvlyvxz9n8wvrlhfsrmh48pm") (features (quote (("hid-iclass-osdp-v2") ("hid-iclass-osdp-v1") ("hid-iclass-hadp"))))))

(define-public crate-osdp-0.1 (crate (name "osdp") (vers "0.1.10") (deps (list (crate-dep (name "crc16") (req "^0.4.0") (default-features #t) (kind 0)))) (hash "1wiphvx2spsxbdfm18pkz8h8zsj0d35zs250z8nshnpqydjg6fs5") (features (quote (("hid-iclass-osdp-v2") ("hid-iclass-osdp-v1") ("hid-iclass-hadp"))))))

(define-public crate-osdp-0.1 (crate (name "osdp") (vers "0.1.11") (deps (list (crate-dep (name "crc16") (req "^0.4.0") (default-features #t) (kind 0)))) (hash "0cdsghwhhpl705slgz687nfi1kgxqn6hb1b73r0krihzd136b3lh") (features (quote (("hid-iclass-osdp-v2") ("hid-iclass-osdp-v1") ("hid-iclass-hadp"))))))

(define-public crate-osdp-0.1 (crate (name "osdp") (vers "0.1.12") (deps (list (crate-dep (name "crc16") (req "^0.4.0") (default-features #t) (kind 0)))) (hash "03qd9rcqd93qml5pvsrj9f068sma1jpczsdq0pr2knfvj0n0y3f1") (features (quote (("hid-iclass-osdp-v2") ("hid-iclass-osdp-v1") ("hid-iclass-hadp"))))))

(define-public crate-osdp-0.1 (crate (name "osdp") (vers "0.1.13") (deps (list (crate-dep (name "crc16") (req "^0.4.0") (default-features #t) (kind 0)))) (hash "03zv4n4f976bw7g7cgf0l9gc0jq0p3nwkvwpdgf5psbrsp98spr4") (features (quote (("hid-iclass-osdp-v2") ("hid-iclass-osdp-v1") ("hid-iclass-hadp"))))))

(define-public crate-osdp-0.1 (crate (name "osdp") (vers "0.1.14") (deps (list (crate-dep (name "crc16") (req "^0.4.0") (default-features #t) (kind 0)))) (hash "0nzhggk4fbzbgkg5f20pma3cqrn1wxrga4zn169m8xcpmw724y5l") (features (quote (("hid-iclass-osdp-v2") ("hid-iclass-osdp-v1") ("hid-iclass-hadp"))))))

(define-public crate-osdp-0.1 (crate (name "osdp") (vers "0.1.15") (deps (list (crate-dep (name "crc16") (req "^0.4.0") (default-features #t) (kind 0)))) (hash "0li95avgzrypa6v704b1khb84xd5rky4f9x3anvvmknxcri4vgbc") (features (quote (("hid-iclass-osdp-v2") ("hid-iclass-osdp-v1") ("hid-iclass-hadp"))))))

(define-public crate-osdp-0.1 (crate (name "osdp") (vers "0.1.16") (deps (list (crate-dep (name "crc16") (req "^0.4.0") (default-features #t) (kind 0)))) (hash "1jb8p6b217mpb9n2iikzwgb9s1a1gaqn4g0pm1nmn82ah9k8sr4j") (features (quote (("hid-iclass-osdp-v2") ("hid-iclass-osdp-v1") ("hid-iclass-hadp"))))))

(define-public crate-osdp-0.1 (crate (name "osdp") (vers "0.1.17") (deps (list (crate-dep (name "crc16") (req "^0.4.0") (default-features #t) (kind 0)))) (hash "0w1xklsg9brl6zagk38a226bhlvmh01861yyin9v2yjzrxkihrq9") (features (quote (("hid-iclass-osdp-v2") ("hid-iclass-osdp-v1") ("hid-iclass-hadp"))))))

(define-public crate-osdp-0.1 (crate (name "osdp") (vers "0.1.18") (deps (list (crate-dep (name "crc16") (req "^0.4.0") (default-features #t) (kind 0)) (crate-dep (name "num-derive") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2") (default-features #t) (kind 0)))) (hash "0r83dij9j4r72alcpncnhb1r7zzlp6r8ibimjfkwd4z0dc6269nc") (features (quote (("hid-iclass-osdp-v2") ("hid-iclass-osdp-v1") ("hid-iclass-hadp"))))))

(define-public crate-osdp-0.1 (crate (name "osdp") (vers "0.1.19") (deps (list (crate-dep (name "num-derive") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2") (default-features #t) (kind 0)))) (hash "197p93q2fraixvivbc14pia3mzw8nwb89m3bl3x7x5xvm87c39nh") (features (quote (("hid-iclass-osdp-v2") ("hid-iclass-osdp-v1") ("hid-iclass-hadp"))))))

(define-public crate-osdp-0.1 (crate (name "osdp") (vers "0.1.22") (deps (list (crate-dep (name "num-derive") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.40") (default-features #t) (kind 0)))) (hash "0k31gcl9hc1q4lpqjfhp5p903lcbicq3s6zagw7d122sl1fgrsc7") (features (quote (("hid-iclass-osdp-v2") ("hid-iclass-osdp-v1") ("hid-iclass-hadp"))))))

(define-public crate-osdpctl-0.0.1 (crate (name "osdpctl") (vers "0.0.1") (deps (list (crate-dep (name "anyhow") (req "^1.0.75") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^4.4.7") (default-features #t) (kind 0)) (crate-dep (name "configparser") (req "^3.0.2") (default-features #t) (kind 0)) (crate-dep (name "daemonize") (req "^0.5.0") (default-features #t) (kind 0)) (crate-dep (name "dirs") (req "^5.0.1") (default-features #t) (kind 0)) (crate-dep (name "libosdp") (req "^0.1.8") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.20") (default-features #t) (kind 0)) (crate-dep (name "log4rs") (req "^1.2.0") (default-features #t) (kind 0)) (crate-dep (name "nix") (req "^0.27.1") (features (quote ("signal"))) (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)) (crate-dep (name "toml") (req "^0.8.8") (default-features #t) (kind 0)))) (hash "14cin2d47q30ls6y6kfg52w0m01iic6iykqxdnda6v0p63vm059g")))

