(define-module (crates-io os ha) #:use-module (crates-io))

(define-public crate-oshash-0.1 (crate (name "oshash") (vers "0.1.0") (deps (list (crate-dep (name "util") (req "^0.1.3") (default-features #t) (kind 0)))) (hash "1qxnwdswyay8f1gsq2i4hws0wch8yihv2qggzi50rz6gfnbl4lq0")))

(define-public crate-oshash-0.1 (crate (name "oshash") (vers "0.1.1") (deps (list (crate-dep (name "util") (req "^0.1.3") (default-features #t) (kind 0)))) (hash "1p01ylpwbk41yxsiwhi7d0k0cxk4by3bq4rshvvncaiy9410jvp5")))

