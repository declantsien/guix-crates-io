(define-module (crates-io os ci) #:use-module (crates-io))

(define-public crate-oscillator-0.1 (crate (name "oscillator") (vers "0.1.0") (deps (list (crate-dep (name "stopwatch") (req "^0.0.6") (default-features #t) (kind 0)))) (hash "1wvyr1yl1djsh1yviqbyjhq01ylxb5f25d50w9m5r6rwnhwr15g0")))

(define-public crate-oscillator-0.1 (crate (name "oscillator") (vers "0.1.1") (hash "0scl6c9msyp6191k0h2245z2bkv93vspdxfiam4yrsah3f3ayi6h")))

(define-public crate-oscillatorsetups-0.0.1 (crate (name "oscillatorsetups") (vers "0.0.1") (deps (list (crate-dep (name "reqwest") (req "^0.11") (features (quote ("json"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "0l0m4sq4hmf4bdsianlgsf2bxjh7px9cb5z14d93jsgq1fsz2gxa")))

(define-public crate-oscillatorsetups-0.1 (crate (name "oscillatorsetups") (vers "0.1.0") (deps (list (crate-dep (name "chrono") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "rayon") (req "^1.7") (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.11") (features (quote ("json" "blocking"))) (default-features #t) (kind 0)) (crate-dep (name "rust_decimal") (req "^1.31.0") (default-features #t) (kind 0)) (crate-dep (name "rust_decimal_macros") (req "^1.31.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "sysinfo") (req "^0.29") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.29") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "1f0sdg3n4nshg0hy1f8d6b5dk4h40if4vqbx0yr96pscp28wspqw")))

(define-public crate-oscirs-0.1 (crate (name "oscirs") (vers "0.1.0") (deps (list (crate-dep (name "oscirs_linalg") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "oscirs_plot") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "oscirs_stats") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "06j4sfbsp2dkbnmp7c787j1j50vgwn52nh8iaf8a189pblaizk64")))

(define-public crate-oscirs-0.1 (crate (name "oscirs") (vers "0.1.1") (deps (list (crate-dep (name "oscirs_linalg") (req "^0.1.1") (default-features #t) (kind 0)) (crate-dep (name "oscirs_plot") (req "^0.1.1") (default-features #t) (kind 0)) (crate-dep (name "oscirs_stats") (req "^0.1.1") (default-features #t) (kind 0)))) (hash "15qjf1d8lz4hzyfmb0jd1pn0qv9v5rrrg79qg0fv95wnsjpywicc")))

(define-public crate-oscirs-0.2 (crate (name "oscirs") (vers "0.2.0") (deps (list (crate-dep (name "oscirs_linalg") (req "^0.2.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "oscirs_plot") (req "^0.2.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "oscirs_stats") (req "^0.2.0") (optional #t) (default-features #t) (kind 0)))) (hash "1h7rp2pq3xi0g0jmw5siy4vky4f7q0bdw8a9dbw8ni3z44xpd6jk") (features (quote (("all" "linalg" "plot" "stats")))) (v 2) (features2 (quote (("stats" "dep:oscirs_stats") ("plot" "dep:oscirs_plot") ("linalg" "dep:oscirs_linalg"))))))

(define-public crate-oscirs-0.3 (crate (name "oscirs") (vers "0.3.0") (deps (list (crate-dep (name "oscirs_linalg") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "oscirs_plot") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "oscirs_stats") (req "^0.3.0") (default-features #t) (kind 0)))) (hash "1nvrw5ybd77jnzjnrxmcnjh8jrz8xll6f1fjrzll1xi4y0w9raah")))

(define-public crate-oscirs_linalg-0.1 (crate (name "oscirs_linalg") (vers "0.1.0") (deps (list (crate-dep (name "opencl3") (req "^0.9.3") (default-features #t) (kind 0)))) (hash "1rsvqkpvbn6k2b623w0l2g7xs0dazwmr05xyjhfa20jlyzaf4psc")))

(define-public crate-oscirs_linalg-0.1 (crate (name "oscirs_linalg") (vers "0.1.1") (deps (list (crate-dep (name "opencl3") (req "^0.9.3") (default-features #t) (kind 0)))) (hash "0w091aas9p9my591iwrydn1wjsj4gf978l37rrv4z14yl7xj2wgm")))

(define-public crate-oscirs_linalg-0.2 (crate (name "oscirs_linalg") (vers "0.2.0-alpha") (deps (list (crate-dep (name "opencl3") (req "^0.9.3") (default-features #t) (kind 0)))) (hash "1n1ans2k9mnvmplrzs4l8l9cpk0pg3c4q2a5y2fmh0jhd9ngxl5s")))

(define-public crate-oscirs_linalg-0.2 (crate (name "oscirs_linalg") (vers "0.2.0-alpha.1") (deps (list (crate-dep (name "opencl3") (req "^0.9.3") (default-features #t) (kind 0)))) (hash "1knvn206xn638jxnszsqzwvnn7znnq04ixc7mlr0iik6zazmwasa")))

(define-public crate-oscirs_linalg-0.2 (crate (name "oscirs_linalg") (vers "0.2.0") (deps (list (crate-dep (name "opencl3") (req "^0.9.3") (default-features #t) (kind 0)))) (hash "1g3hgvnbi3hvlccjjhmgsxyrzf30j8iy7kjvqyfidiicwl9kzisd")))

(define-public crate-oscirs_linalg-0.3 (crate (name "oscirs_linalg") (vers "0.3.0") (deps (list (crate-dep (name "opencl3") (req "^0.9.3") (default-features #t) (kind 0)))) (hash "00x31zlkqxg856w7n0jq8r63lp6zji40zmppa3xh36bkll33zji7")))

(define-public crate-oscirs_linalg-0.4 (crate (name "oscirs_linalg") (vers "0.4.0-alpha") (deps (list (crate-dep (name "opencl3") (req "^0.9.3") (default-features #t) (kind 0)))) (hash "1fhrpnm8krqg97i9jgxzxqd8m9b0i5vhh4kdqdp8wssi8yj9jdq8")))

(define-public crate-oscirs_plot-0.1 (crate (name "oscirs_plot") (vers "0.1.0") (deps (list (crate-dep (name "open") (req "^4.1.0") (default-features #t) (kind 0)))) (hash "000al5l724ny3qdcnnvmfflk9dixb3hscw70q6gzy95d08f4jbyw")))

(define-public crate-oscirs_plot-0.1 (crate (name "oscirs_plot") (vers "0.1.1") (deps (list (crate-dep (name "open") (req "^4.1.0") (default-features #t) (kind 0)))) (hash "0v4yq2fpxfs7qr8rhfkkidwn7c145ymdzd9lm9d80a89yb0i9ffq")))

(define-public crate-oscirs_plot-0.2 (crate (name "oscirs_plot") (vers "0.2.0-alpha") (deps (list (crate-dep (name "open") (req "^4.1.0") (default-features #t) (kind 0)))) (hash "1jr5rbr82xjxzwwm7jfc92qysq0gbkxxqc26xjy4ki3z8lvi2pzz")))

(define-public crate-oscirs_plot-0.2 (crate (name "oscirs_plot") (vers "0.2.0") (deps (list (crate-dep (name "open") (req "^4.1.0") (default-features #t) (kind 0)))) (hash "0925cy3d5pc6nba4psgzlnh2dmgfdzyyxdvklvgg76ffkfgpbyy2")))

(define-public crate-oscirs_plot-0.3 (crate (name "oscirs_plot") (vers "0.3.0") (deps (list (crate-dep (name "open") (req "^4.1.0") (default-features #t) (kind 0)))) (hash "1xx96bx9ay41alqzhqbbi09cg19l77lyaxbpsly195k7kh4mr9dh")))

(define-public crate-oscirs_stats-0.1 (crate (name "oscirs_stats") (vers "0.1.0") (hash "044j0f4yck2h6pl2m02bjlj3g08kprwij28kxnlpjdb5i32iqfdn")))

(define-public crate-oscirs_stats-0.1 (crate (name "oscirs_stats") (vers "0.1.1") (hash "1dlwaklhl9cq5q7pdrfz5wdlqf6jlz9cvs8y4wqbqa6cbzp9jg93")))

(define-public crate-oscirs_stats-0.2 (crate (name "oscirs_stats") (vers "0.2.0") (hash "1ywhzayb79s9i7nn77c1nby17s64g9d5p0vlpbz27sx6j8r7iy21")))

(define-public crate-oscirs_stats-0.3 (crate (name "oscirs_stats") (vers "0.3.0") (hash "0x0cf9phdxv0iacqhih8rpw07izar6dwv827vcma1rk98g2j5mbd")))

