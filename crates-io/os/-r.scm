(define-module (crates-io os -r) #:use-module (crates-io))

(define-public crate-os-release-0.1 (crate (name "os-release") (vers "0.1.0") (deps (list (crate-dep (name "lazy_static") (req "^1.1.0") (default-features #t) (kind 0)))) (hash "09zckcazggm33cbhjgdajm8rg41x9y75yf13rhcyqlqvyzi9mwl2")))

(define-public crate-os-release-rs-0.1 (crate (name "os-release-rs") (vers "0.1.0") (hash "1jk88j2iyxrl6998mph4kz81l30l4r2jh4jvdi982cxhi13i8vfp")))

(define-public crate-os-rng-0.0.0 (crate (name "os-rng") (vers "0.0.0") (hash "0q5da2bhxdvhbsblv8939xjzba99f2ys48j427vjv8m7x359dxkq")))

(define-public crate-os-rs-0.1 (crate (name "os-rs") (vers "0.1.0") (hash "19nz775j5czazrp5s91lprjqfhd0sqmymxq922qwmgv2n2z4c5xj") (yanked #t)))

(define-public crate-os-rs-0.1 (crate (name "os-rs") (vers "0.1.1") (hash "12h58ida0bhcfg20zh6r1r09h7py074aaa3kd8nmnfycr6zsbhx6") (yanked #t)))

(define-public crate-os-rs-0.1 (crate (name "os-rs") (vers "0.1.2") (hash "09g1i4jj29d1nyms02rjkkhz2z8sl2dkfmy60a87bx179c795b7d") (yanked #t)))

(define-public crate-os-rs-0.1 (crate (name "os-rs") (vers "0.1.3") (hash "02z5hlrqw0f5kq2vqia6yj98sq7z2p4gbxq2zx0i2ywnp5mbiawn") (yanked #t)))

