(define-module (crates-io os qp) #:use-module (crates-io))

(define-public crate-osqp-0.0.1 (crate (name "osqp") (vers "0.0.1-pre.1") (deps (list (crate-dep (name "osqp-sys") (req "^0.2.1-pre.1") (default-features #t) (kind 0)) (crate-dep (name "static_assertions") (req "^0.2.3") (default-features #t) (kind 0)))) (hash "17d57kydpvv4fs6jdq57r523rs6dngakflzar961zcjjryhl3mzl")))

(define-public crate-osqp-0.0.1 (crate (name "osqp") (vers "0.0.1-pre.2") (deps (list (crate-dep (name "osqp-sys") (req "^0.2.1-pre.2") (default-features #t) (kind 0)) (crate-dep (name "static_assertions") (req "^0.2.3") (default-features #t) (kind 0)))) (hash "0wc6xfk8zklpsa5nnl10zb9j5pkw0ll8ngf7fh4z9p4gnakkyw7k")))

(define-public crate-osqp-0.0.1 (crate (name "osqp") (vers "0.0.1-pre.3") (deps (list (crate-dep (name "osqp-sys") (req "^0.2.1-pre.3") (default-features #t) (kind 0)) (crate-dep (name "static_assertions") (req "^0.2.3") (default-features #t) (kind 0)))) (hash "1hgajzb6cabf97xb374vnm7nhg383ici8cyr53x5qwzg9v80v221")))

(define-public crate-osqp-0.2 (crate (name "osqp") (vers "0.2.1-pre.4") (deps (list (crate-dep (name "osqp-sys") (req "^0.2.1-pre.3") (default-features #t) (kind 0)))) (hash "19fm0c6wcg0r1yzh89k28mwlqyvn4swqkcwsrg6rdpyzxb6b0mjy")))

(define-public crate-osqp-0.3 (crate (name "osqp") (vers "0.3.0-pre.1") (deps (list (crate-dep (name "osqp-sys") (req "^0.3.0") (default-features #t) (kind 0)))) (hash "1n2ppqfa4qxaxj3p4szmn9d637r3izyfkwrifn8d3vd4dracfbb2")))

(define-public crate-osqp-0.3 (crate (name "osqp") (vers "0.3.0-pre.2") (deps (list (crate-dep (name "osqp-sys") (req "^0.3.0") (default-features #t) (kind 0)))) (hash "1rg8hb41z0l25nln6n21jzdb4vp210d75wr1adgb0qz76mfnp2yi")))

(define-public crate-osqp-0.3 (crate (name "osqp") (vers "0.3.1") (deps (list (crate-dep (name "osqp-sys") (req "^0.3.1") (default-features #t) (kind 0)))) (hash "13dj1vagmbvw2sn8z12yw17rxrsbxiprzi4iis6yqw8pjwvrg8ja")))

(define-public crate-osqp-0.4 (crate (name "osqp") (vers "0.4.0") (deps (list (crate-dep (name "osqp-sys") (req "^0.4.0") (default-features #t) (kind 0)))) (hash "0475bng8qgijvi7lcnsln0z80va4p3r7yh9n6jybs5wfr5cb5png")))

(define-public crate-osqp-0.4 (crate (name "osqp") (vers "0.4.1") (deps (list (crate-dep (name "osqp-sys") (req "^0.4.1") (default-features #t) (kind 0)))) (hash "1ychyz9g3lgxwzinw1n16kmz5w3wc612pgiwz0rxdlibzjfyfy8z")))

(define-public crate-osqp-0.5 (crate (name "osqp") (vers "0.5.0") (deps (list (crate-dep (name "osqp-sys") (req "^0.5.0") (default-features #t) (kind 0)))) (hash "0c4135slpzmwjn4scc9gs8jn86nan9qw8mbi58kxa435wa56h27b")))

(define-public crate-osqp-0.6 (crate (name "osqp") (vers "0.6.0") (deps (list (crate-dep (name "osqp-sys") (req "^0.6.0") (default-features #t) (kind 0)))) (hash "0swlyqd05790cpca8799l0yfj1772pvfhz6044i0hvvkar5v4775")))

(define-public crate-osqp-0.6 (crate (name "osqp") (vers "0.6.1") (deps (list (crate-dep (name "osqp-sys") (req "^0.6.0") (default-features #t) (kind 0)))) (hash "0v94vgwjggklzgf0zs1l35zynhmpx5n2zdrbkqsiwa4srgfchg1v")))

(define-public crate-osqp-0.6 (crate (name "osqp") (vers "0.6.2-pre") (deps (list (crate-dep (name "osqp-sys") (req "^0.6.2-pre") (default-features #t) (kind 0)))) (hash "1ar0adzpgr50iqp3fzw8kizxknmmynzga7kh22fmjiw2xqq5wa4k")))

(define-public crate-osqp-0.6 (crate (name "osqp") (vers "0.6.2") (deps (list (crate-dep (name "osqp-sys") (req "^0.6.2") (default-features #t) (kind 0)))) (hash "07i7yizg5m550js01qvyrqlj8nf19npq5kjpj78f280jf0j5c1ry")))

(define-public crate-osqp-rust-0.6 (crate (name "osqp-rust") (vers "0.6.2") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "osqp-rust-sys") (req "^0.6.2") (default-features #t) (kind 0)))) (hash "0nnn12csb10hjvxfy5y1sg6icfhfp1mzv68ar65nrgnwxdgbbjmw")))

(define-public crate-osqp-rust-sys-0.6 (crate (name "osqp-rust-sys") (vers "0.6.2") (hash "06z0yz5xwwjyckx4nhsx41s3cyy9rahgqn46ybrkljq29k1636dz")))

(define-public crate-osqp-sys-0.2 (crate (name "osqp-sys") (vers "0.2.1-pre.1") (deps (list (crate-dep (name "cmake") (req "^0.1.28") (default-features #t) (kind 1)))) (hash "1giz8n73jzdaim38zc38r6b1lgpgksjwxfsa57vln6bxli5cfd2c")))

(define-public crate-osqp-sys-0.2 (crate (name "osqp-sys") (vers "0.2.1-pre.2") (deps (list (crate-dep (name "cmake") (req "^0.1.28") (default-features #t) (kind 1)))) (hash "09qj248l0s8g535qxszrw8cvp8barhfj1ckc5fv8p0hb1v9b6n44")))

(define-public crate-osqp-sys-0.2 (crate (name "osqp-sys") (vers "0.2.1-pre.3") (deps (list (crate-dep (name "cmake") (req "^0.1.28") (default-features #t) (kind 1)))) (hash "13iqnjxya4i0cv1xmcnxasp3a8pwhj1f86gv1384x59yvczkawd3")))

(define-public crate-osqp-sys-0.3 (crate (name "osqp-sys") (vers "0.3.0") (deps (list (crate-dep (name "cmake") (req "^0.1.28") (default-features #t) (kind 1)))) (hash "1vxnwxrdmy1zhbb9v5vkmrnhfj6wghf701rbbff00mspgd0h05gz")))

(define-public crate-osqp-sys-0.3 (crate (name "osqp-sys") (vers "0.3.1") (deps (list (crate-dep (name "cmake") (req "^0.1.28") (default-features #t) (kind 1)))) (hash "1g4z5yksx0r185mj2s5iak5y2k6r8j9l6dnc8xc02547zszyk9hv") (links "osqp")))

(define-public crate-osqp-sys-0.4 (crate (name "osqp-sys") (vers "0.4.0") (deps (list (crate-dep (name "cmake") (req "^0.1.28") (default-features #t) (kind 1)))) (hash "0xr7yi1avw68xqyia7viamcifd6in9izjg4p0zjqqab1wb3f3cad") (links "osqp")))

(define-public crate-osqp-sys-0.4 (crate (name "osqp-sys") (vers "0.4.1") (deps (list (crate-dep (name "cmake") (req "^0.1.28") (default-features #t) (kind 1)))) (hash "0jfc8r8vq6zc3fshaarl9950xmxb66hcx61iz6i22396z1c7aqnj") (links "osqp")))

(define-public crate-osqp-sys-0.5 (crate (name "osqp-sys") (vers "0.5.0") (deps (list (crate-dep (name "cmake") (req "^0.1.28") (default-features #t) (kind 1)))) (hash "07k3vn61haw25gv3993p2c1p51ci3yrxgrfvfpa16imbnl1sf4z6") (links "osqp")))

(define-public crate-osqp-sys-0.6 (crate (name "osqp-sys") (vers "0.6.0") (deps (list (crate-dep (name "cc") (req "^1.0.36") (default-features #t) (kind 1)) (crate-dep (name "cmake") (req "^0.1.28") (default-features #t) (kind 1)))) (hash "1ysmqjqg46fbhm6b626wsm6157wkzm5xdrs2nvn9xc476723m33v") (links "osqp")))

(define-public crate-osqp-sys-0.6 (crate (name "osqp-sys") (vers "0.6.2-pre") (deps (list (crate-dep (name "cc") (req "^1.0.36") (default-features #t) (kind 1)) (crate-dep (name "cmake") (req "^0.1.28") (default-features #t) (kind 1)) (crate-dep (name "fs_extra") (req "^1.2.0") (default-features #t) (kind 1)))) (hash "0q39xv3rpx2m1ap7wvn096kbllr0krkhm34sabsmmshh8lmwgn4w") (links "osqp")))

(define-public crate-osqp-sys-0.6 (crate (name "osqp-sys") (vers "0.6.2") (deps (list (crate-dep (name "cc") (req "^1.0.36") (default-features #t) (kind 1)) (crate-dep (name "cmake") (req "^0.1.28") (default-features #t) (kind 1)) (crate-dep (name "fs_extra") (req "^1.2.0") (default-features #t) (kind 1)))) (hash "1akzs6g087crq8bglk77ql7xn4177cn6dagrzgs4c2x7vf1slrvp") (links "osqp")))

