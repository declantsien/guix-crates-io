(define-module (crates-io os _e) #:use-module (crates-io))

(define-public crate-os_error-0.1 (crate (name "os_error") (vers "0.1.0") (hash "1q270frif5z2zs35vgipfivc8ky7290an8xx9yfjp90yjq5avyng") (features (quote (("nightly") ("default"))))))

