(define-module (crates-io os _b) #:use-module (crates-io))

(define-public crate-os_bootinfo-0.1 (crate (name "os_bootinfo") (vers "0.1.0") (deps (list (crate-dep (name "x86_64") (req "^0.2.0-alpha") (default-features #t) (kind 0)))) (hash "1i8qckk07mxc0d3hsp90axmnnmfsh43mji80mqc2yi5rbpj6dn5p")))

(define-public crate-os_bootinfo-0.2 (crate (name "os_bootinfo") (vers "0.2.0-alpha-001") (deps (list (crate-dep (name "arrayvec") (req "^0.4.7") (kind 0)) (crate-dep (name "x86_64") (req "^0.2.0-alpha") (default-features #t) (kind 0)))) (hash "17l3psvyvl8nmhsbnn69v72xmziwqqldg637k4skcn13iiqyf3vb")))

(define-public crate-os_bootinfo-0.2 (crate (name "os_bootinfo") (vers "0.2.0-alpha-002") (deps (list (crate-dep (name "arrayvec") (req "^0.4.7") (kind 0)) (crate-dep (name "x86_64") (req "^0.2.0-alpha") (default-features #t) (kind 0)))) (hash "0ag1akhldsfsjhmgjp22c12r4nazr3aj5jixgki1hz5f6bswmg51")))

(define-public crate-os_bootinfo-0.2 (crate (name "os_bootinfo") (vers "0.2.0-alpha-003") (deps (list (crate-dep (name "arrayvec") (req "^0.4.7") (kind 0)) (crate-dep (name "x86_64") (req "^0.2.0-alpha") (default-features #t) (kind 0)))) (hash "1fbbz5azxspx975yxigk2mhgbz958w42kp4vwcja568iixw7nz2k")))

(define-public crate-os_bootinfo-0.2 (crate (name "os_bootinfo") (vers "0.2.0-alpha-004") (deps (list (crate-dep (name "arrayvec") (req "^0.4.7") (kind 0)) (crate-dep (name "x86_64") (req "^0.2.0-alpha") (default-features #t) (kind 0)))) (hash "0xg9mgp5ahzr3r641j637k17kyzy4lp2fq615maqzr6i5dkgx63n")))

(define-public crate-os_bootinfo-0.2 (crate (name "os_bootinfo") (vers "0.2.0-alpha-005") (deps (list (crate-dep (name "arrayvec") (req "^0.4.7") (kind 0)) (crate-dep (name "x86_64") (req "^0.2.0-alpha") (default-features #t) (kind 0)))) (hash "0r7hxshdaw92zb9i7dsrgjx9m3zsgqfpajzz2i929m3mm8pdjlh4")))

(define-public crate-os_bootinfo-0.2 (crate (name "os_bootinfo") (vers "0.2.0-alpha-006") (deps (list (crate-dep (name "x86_64") (req "^0.2.0-alpha") (default-features #t) (kind 0)))) (hash "04rylbyn2dmznh9c5fr0sd14mp25i5x4ry442vi6v7yp1svd5k9c")))

(define-public crate-os_bootinfo-0.2 (crate (name "os_bootinfo") (vers "0.2.0-alpha-007") (deps (list (crate-dep (name "x86_64") (req "^0.2.0-alpha") (default-features #t) (kind 0)))) (hash "07a1zm41c5g3w8j4hhyszi05pb26429aakybxxq208p3ga4y372l")))

(define-public crate-os_bootinfo-0.2 (crate (name "os_bootinfo") (vers "0.2.0-alpha-008") (deps (list (crate-dep (name "x86_64") (req "^0.2.0-alpha") (default-features #t) (kind 0)))) (hash "0fpcsf5glg7kvw5zjva54cg75dhgbwfpz0j0g4n5qf4cag554hii")))

(define-public crate-os_bootinfo-0.2 (crate (name "os_bootinfo") (vers "0.2.0-alpha-009") (deps (list (crate-dep (name "x86_64") (req "^0.2.0-alpha") (default-features #t) (kind 0)))) (hash "0r81w9pkj4ba10s4iap2h2rbiwjjzby71m22zslpkmm4xys5ql9x")))

(define-public crate-os_bootinfo-0.2 (crate (name "os_bootinfo") (vers "0.2.0-alpha-010") (deps (list (crate-dep (name "x86_64") (req "^0.2.0-alpha") (default-features #t) (kind 0)))) (hash "0hqk5wgvnhsqyaigir82pziq2byyynd5iz2nlcxbr9pgq7r7zkfh")))

(define-public crate-os_bootinfo-0.2 (crate (name "os_bootinfo") (vers "0.2.0-alpha-011") (deps (list (crate-dep (name "x86_64") (req "^0.2.0-alpha") (default-features #t) (kind 0)))) (hash "1b0bg9z239ndbl4ipx6827la07l38fp8k8wk1l6s8l30n4qwnya8")))

(define-public crate-os_bootinfo-0.2 (crate (name "os_bootinfo") (vers "0.2.0") (hash "0mj3wyil2w2i0n18l889wihf677pxhzmnyyjf0w5q4zvw17a8lv9")))

(define-public crate-os_bootinfo-0.2 (crate (name "os_bootinfo") (vers "0.2.1") (hash "02invdrjcx7kqig8qa1q8f1nyy1h5h2dcg5nhnyyfwz7nnz1sj36")))

(define-public crate-os_bootinfo-0.3 (crate (name "os_bootinfo") (vers "0.3.0") (hash "0bjgygfjayx2rablv6jrwik4yvr3dpai2dii1p0k1l30d1mskzyk") (yanked #t)))

