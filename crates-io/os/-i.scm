(define-module (crates-io os #{-i}#) #:use-module (crates-io))

(define-public crate-os-id-1 (crate (name "os-id") (vers "1.0.0") (deps (list (crate-dep (name "libc") (req "^0.2") (target "cfg(unix)") (kind 0)))) (hash "163n52sjdmg09ksxsv61106mxmijrmgmczslr201z3sl5lssxfjg")))

(define-public crate-os-id-2 (crate (name "os-id") (vers "2.0.0") (deps (list (crate-dep (name "libc") (req "^0.2") (target "cfg(unix)") (kind 0)))) (hash "19dlqzhbhxgnqdc49b0v0vlz8d6f578y3sxhbdygqisslzzg8b78")))

(define-public crate-os-id-2 (crate (name "os-id") (vers "2.0.1") (deps (list (crate-dep (name "libc") (req "^0.2") (target "cfg(unix)") (kind 0)))) (hash "0zm34k07kgr3xbhi15rh9g0lpk7204kkjszx6f3z8j5cz1i1dyhd")))

(define-public crate-os-id-2 (crate (name "os-id") (vers "2.0.2") (deps (list (crate-dep (name "libc") (req "^0.2") (target "cfg(unix)") (kind 0)) (crate-dep (name "str-buf") (req "^2") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "winapi") (req "^0.3") (features (quote ("stringapiset" "processthreadsapi" "winbase" "winerror" "errhandlingapi"))) (optional #t) (target "cfg(windows)") (kind 0)))) (hash "1wqil70k16l69p2cqw4lqdsili1cga6qcy1vlhj00kmw66yg44yn") (features (quote (("thread-name" "str-buf" "winapi"))))))

(define-public crate-os-id-3 (crate (name "os-id") (vers "3.0.0") (deps (list (crate-dep (name "libc") (req "^0.2") (target "cfg(unix)") (kind 0)))) (hash "10axflay1ijq879bzgrk514ckzn26d1dzy50c9qp9zjnfp0qnddk")))

(define-public crate-os-id-3 (crate (name "os-id") (vers "3.0.1") (deps (list (crate-dep (name "libc") (req "^0.2") (target "cfg(unix)") (kind 0)))) (hash "1xm5l0wjqd52xg9lgp0wzs6g51rbqfaxyxfnn1nxhln5apn5c22i")))

