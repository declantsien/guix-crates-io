(define-module (crates-io os o-) #:use-module (crates-io))

(define-public crate-oso-cloud-0.1 (crate (name "oso-cloud") (vers "0.1.0") (hash "0g33z87b91sk9scrkd8h8cwbdh24g1kznld9z2cbwygyacghmm2m")))

(define-public crate-oso-cloud-0.2 (crate (name "oso-cloud") (vers "0.2.0") (deps (list (crate-dep (name "reqwest") (req "^0.11.9") (features (quote ("json" "rustls-tls"))) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.26") (features (quote ("rt" "macros"))) (default-features #t) (kind 2)) (crate-dep (name "tracing") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "tracing-subscriber") (req "^0.3") (features (quote ("env-filter"))) (default-features #t) (kind 2)))) (hash "0bp0i6as4x0lx8p60qa6kr9gh5vck34g3xcsaq9zhrl3s8yz92x3")))

(define-public crate-oso-cloud-0.2 (crate (name "oso-cloud") (vers "0.2.1") (deps (list (crate-dep (name "reqwest") (req "^0.11.9") (features (quote ("json" "rustls-tls"))) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.26") (features (quote ("rt" "macros"))) (default-features #t) (kind 2)) (crate-dep (name "tracing") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "tracing-subscriber") (req "^0.3") (features (quote ("env-filter"))) (default-features #t) (kind 2)))) (hash "09x1gqjdnyrl8kr7y68iywphpv7y68r03jvp742a25bnhw5z3mcp")))

(define-public crate-oso-cloud-0.4 (crate (name "oso-cloud") (vers "0.4.0") (deps (list (crate-dep (name "reqwest") (req "^0.11.9") (features (quote ("json" "rustls-tls"))) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.26") (features (quote ("rt" "macros"))) (default-features #t) (kind 2)) (crate-dep (name "tracing") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "tracing-subscriber") (req "^0.3") (features (quote ("env-filter"))) (default-features #t) (kind 2)))) (hash "0p1r8369w03az3xsmrnh5n092qfq5ydg1hmfk0778mvamxg3fdn5")))

(define-public crate-oso-cloud-sdk-0.1 (crate (name "oso-cloud-sdk") (vers "0.1.0") (hash "1c5f8s49rp4ih9nw01q5xj79gygz1cswg1i386bzxm5qjx1abdif")))

(define-public crate-oso-derive-0.1 (crate (name "oso-derive") (vers "0.1.0") (hash "1vzirmlj3m7w9lfl54jd9vwkjjj3as5v6y1rjhyn7znvjmy0iyxy")))

(define-public crate-oso-derive-0.1 (crate (name "oso-derive") (vers "0.1.0-alpha") (deps (list (crate-dep (name "quote") (req "^1.0.7") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.40") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "16wkjq8h37j29m90za4bhsd4d150p6li7bj4i3g39l2j3v6hfais")))

(define-public crate-oso-derive-0.6 (crate (name "oso-derive") (vers "0.6.0-alpha") (deps (list (crate-dep (name "quote") (req "^1.0.7") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.40") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "1byh7csy9fs2gg9rrpz35yx86zav7l98a0cjfdc7bw05favb25yi")))

(define-public crate-oso-derive-0.7 (crate (name "oso-derive") (vers "0.7.0") (deps (list (crate-dep (name "quote") (req "^1.0.7") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.43") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "1zsgjy0knyv6bwsmd2d23f8sdwsncbbdirkdjnlhjwf2jwi5abqg")))

(define-public crate-oso-derive-0.7 (crate (name "oso-derive") (vers "0.7.1") (deps (list (crate-dep (name "quote") (req "^1.0.7") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.43") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "1pd6r59606srd9yrgia5568fli9zxkdifryl2wa845f1h4kh5pl7")))

(define-public crate-oso-derive-0.8 (crate (name "oso-derive") (vers "0.8.0") (deps (list (crate-dep (name "quote") (req "^1.0.7") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.43") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "1ydsdhv57q7z0azrxy0pz52lirrd9fp207ljip51hg0lk4hvk5qj")))

(define-public crate-oso-derive-0.8 (crate (name "oso-derive") (vers "0.8.1") (deps (list (crate-dep (name "quote") (req ">=1.0.7, <2.0.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req ">=1.0.43, <2.0.0") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "1lvm83wa4pmb40a13cfzjp153hb1dd1li41122xd5zjk8m85bg56")))

(define-public crate-oso-derive-0.8 (crate (name "oso-derive") (vers "0.8.2") (deps (list (crate-dep (name "quote") (req ">=1.0.7, <2.0.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req ">=1.0.43, <2.0.0") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "1c1wnamfjmxa0x9nii0w5gq5kay6gx043j4mc6ddi0ffxpk5pj7s")))

(define-public crate-oso-derive-0.9 (crate (name "oso-derive") (vers "0.9.0") (deps (list (crate-dep (name "quote") (req "^1.0.7") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.43") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "1w6agrmh9x7qvbv69mclvgnxgngv7893ajyp6mw39fdzxii7ymla")))

(define-public crate-oso-derive-0.10 (crate (name "oso-derive") (vers "0.10.0") (deps (list (crate-dep (name "quote") (req "^1.0.8") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.58") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "0579jbiqwly3f0sqg7hs1siszy0k700qxv3l9hvj583zippmiasw")))

(define-public crate-oso-derive-0.10 (crate (name "oso-derive") (vers "0.10.1") (deps (list (crate-dep (name "quote") (req "^1.0.8") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.58") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "1f6m4mb5gz7kaxbxg2vr7kgcqjcrqk6jir5nkkfyv5v7ibs7l2za")))

(define-public crate-oso-derive-0.11 (crate (name "oso-derive") (vers "0.11.0") (deps (list (crate-dep (name "quote") (req "^1.0.8") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.58") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "0v2smrddxp0r6sqkc93s1lmq6y2gfxrpypvidnjafbwlf5jsm1bg")))

(define-public crate-oso-derive-0.11 (crate (name "oso-derive") (vers "0.11.1") (deps (list (crate-dep (name "quote") (req "^1.0.8") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.58") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "0xs3747bsshsi0i92s8n917n4b77q6d0m522b1fpldaiaynfjj3g")))

(define-public crate-oso-derive-0.11 (crate (name "oso-derive") (vers "0.11.2") (deps (list (crate-dep (name "quote") (req "^1.0.8") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.58") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "06n23jdczy5j3ccsd74f4dwhaaagwy3y7bi0hk3blxm1ykass044")))

(define-public crate-oso-derive-0.11 (crate (name "oso-derive") (vers "0.11.3") (deps (list (crate-dep (name "quote") (req "^1.0.8") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.58") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "09ij0h4mb1dimc5gyl34xvbv5bqx4kga55w7ny4j0milyl2pqk0n")))

(define-public crate-oso-derive-0.12 (crate (name "oso-derive") (vers "0.12.0") (deps (list (crate-dep (name "quote") (req "^1.0.8") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.58") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "0266n6czm25rf2122xbk7yvy2p39v6ah48jnsdam67miksi8yh8h")))

(define-public crate-oso-derive-0.12 (crate (name "oso-derive") (vers "0.12.1") (deps (list (crate-dep (name "quote") (req "^1.0.8") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.58") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "0cm1sa1ln1wpir5k4jws0bhfiwk8a7cvycajdkl0j0vv0fz1d44w")))

(define-public crate-oso-derive-0.12 (crate (name "oso-derive") (vers "0.12.2") (deps (list (crate-dep (name "quote") (req "^1.0.8") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.58") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "11car453xy1p8icgzpf4nzkwibzszhb7bcdvcqwqw2gndds8y2jr")))

(define-public crate-oso-derive-0.12 (crate (name "oso-derive") (vers "0.12.3") (deps (list (crate-dep (name "quote") (req "^1.0.8") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.58") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "1p709lw3b2mppb8dpi0l8wdvxm4fmzvv1k24i676a30ibl7ilvd3")))

(define-public crate-oso-derive-0.12 (crate (name "oso-derive") (vers "0.12.4") (deps (list (crate-dep (name "quote") (req "^1.0.8") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.58") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "0s41c1jggcvl5g67dy6icdjzixwbb555wzlqr29ja1gjpfb13ivh")))

(define-public crate-oso-derive-0.13 (crate (name "oso-derive") (vers "0.13.0") (deps (list (crate-dep (name "quote") (req "^1.0.8") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.58") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "0w7h5wrcaiwgfrxxrm10gds3581rxk1azcr3p6cf00i9ilbhmnh8")))

(define-public crate-oso-derive-0.13 (crate (name "oso-derive") (vers "0.13.1") (deps (list (crate-dep (name "quote") (req "^1.0.8") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.58") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "0izv1n21ilnjs0738kcrddlfidr31alkg8hbav3kacbi34wp9w1i")))

(define-public crate-oso-derive-0.14 (crate (name "oso-derive") (vers "0.14.0") (deps (list (crate-dep (name "quote") (req "^1.0.8") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.58") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "1hsr50cckag7j75q7y5kldfsch8ivpw6frqm0hfkmn1v973g5fjm")))

(define-public crate-oso-derive-0.14 (crate (name "oso-derive") (vers "0.14.1") (deps (list (crate-dep (name "quote") (req "^1.0.8") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.58") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "0ga440m4iqhy64fyb9z9sp8g1cf7h0h1n7lbni9x2q37q91hsmbd")))

(define-public crate-oso-derive-0.14 (crate (name "oso-derive") (vers "0.14.2") (deps (list (crate-dep (name "quote") (req "^1.0.8") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.58") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "1nqzkyhff8k97flywgp4hqvf5dwn2x31l1m84ia4r53md8691yrd") (yanked #t)))

(define-public crate-oso-derive-0.15 (crate (name "oso-derive") (vers "0.15.0") (deps (list (crate-dep (name "quote") (req "^1.0.8") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.58") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "0j2pifv171p4yr39sq2aif61ya1mslsnqap4h2wwkps18ca19qrl")))

(define-public crate-oso-derive-0.20 (crate (name "oso-derive") (vers "0.20.0-beta") (deps (list (crate-dep (name "quote") (req "^1.0.8") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.58") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "0mbbci2k0fh6g035zsshcq5ym83wj88lsll7d0jbswrg56glk8bk")))

(define-public crate-oso-derive-0.15 (crate (name "oso-derive") (vers "0.15.1") (deps (list (crate-dep (name "quote") (req "^1.0.8") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.58") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "1pfhnxqi1lwlcn8wxg8xzbslar8vcjrc87bfj5kvr6ns4c7dz7cy")))

(define-public crate-oso-derive-0.20 (crate (name "oso-derive") (vers "0.20.1-beta") (deps (list (crate-dep (name "quote") (req "^1.0.8") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.58") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "0g82c23pm7nmyd0ma9f7s2p4d19mb53xgvi8kaac9gq1v58ih638")))

(define-public crate-oso-derive-0.20 (crate (name "oso-derive") (vers "0.20.1") (deps (list (crate-dep (name "quote") (req "^1.0.8") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.58") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "1829yx4r240kzhid053g4v11ijpklc74pcrn257xm214h6fksvwm")))

(define-public crate-oso-derive-0.21 (crate (name "oso-derive") (vers "0.21.0") (deps (list (crate-dep (name "quote") (req "^1.0.8") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.58") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "0i7164c176iflxcqzqv7mkjjmak47cmzlr25znc2a2m295i0qzpp")))

(define-public crate-oso-derive-0.22 (crate (name "oso-derive") (vers "0.22.0") (deps (list (crate-dep (name "quote") (req "^1.0.8") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.58") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "0rj1l676llnja7m2x209iysqa8hpxcz5mfrlqf37dr07lbr2x41k")))

(define-public crate-oso-derive-0.22 (crate (name "oso-derive") (vers "0.22.1") (deps (list (crate-dep (name "quote") (req "^1.0.8") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.58") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "0vgr3wryccwjww3b02fkh2p03x8h1xiiz3c3j8p0iv0v1asmh4hb")))

(define-public crate-oso-derive-0.23 (crate (name "oso-derive") (vers "0.23.0") (deps (list (crate-dep (name "quote") (req "^1.0.8") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.58") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "104xqg0p15314wzlisabjlsnykdjhcfhh7kl8fv9a0wmj336a2rg")))

(define-public crate-oso-derive-0.24 (crate (name "oso-derive") (vers "0.24.0") (deps (list (crate-dep (name "quote") (req "^1.0.8") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.58") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "0zjrl0ypn9hgfgwcvl2gv5h372yvzd6c3jfym56ilv2s07jhld2l")))

(define-public crate-oso-derive-0.25 (crate (name "oso-derive") (vers "0.25.0") (deps (list (crate-dep (name "quote") (req "^1.0.8") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.58") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "0fms09vhwvb32xrdi67xn2hrdkkhvk7skzm1i5sbwkrqwjfpfxf8")))

(define-public crate-oso-derive-0.25 (crate (name "oso-derive") (vers "0.25.1") (deps (list (crate-dep (name "quote") (req "^1.0.8") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.58") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "1nig646mg35sk6cvck30a4v8gkgc1c0rgr6g1hnghmd53fni0gxh")))

(define-public crate-oso-derive-0.26 (crate (name "oso-derive") (vers "0.26.0") (deps (list (crate-dep (name "quote") (req "^1.0.8") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.58") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "1s2qbbp7ax88h95xbhj84hsirw5lybgwky8m8q3ppjnnivdjgl5n")))

(define-public crate-oso-derive-0.26 (crate (name "oso-derive") (vers "0.26.1") (deps (list (crate-dep (name "quote") (req "^1.0.8") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.58") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "18h0vprb5i4j896kk4j63bzc8097dg2gpahc4j17z96xs04cr9nc")))

(define-public crate-oso-derive-0.26 (crate (name "oso-derive") (vers "0.26.2") (deps (list (crate-dep (name "quote") (req "^1.0.8") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.58") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "1jjsqhxr7y1m7xqdcqc26vxamxffp99g53yqpp5w1jyava08knj7")))

(define-public crate-oso-derive-0.26 (crate (name "oso-derive") (vers "0.26.3") (deps (list (crate-dep (name "quote") (req "^1.0.8") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.58") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "192fcq13ybfbydvln8a7lwqmy4wdl7h9b284rynrsnz122zmhphm")))

(define-public crate-oso-derive-0.26 (crate (name "oso-derive") (vers "0.26.4") (deps (list (crate-dep (name "quote") (req "^1.0.8") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.58") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "0bbcz8bxnlydyvrw3lz12fq4vwy8dypb6il4vlh0vw7bx82khjim")))

(define-public crate-oso-derive-0.27 (crate (name "oso-derive") (vers "0.27.0") (deps (list (crate-dep (name "quote") (req "^1.0.8") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.58") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "1a7rzqn2rhy8i4rbbdvikqjy7kscsrbim3mrd9cyb33lhdzqarhp")))

(define-public crate-oso-derive-0.27 (crate (name "oso-derive") (vers "0.27.1") (deps (list (crate-dep (name "quote") (req "^1.0.8") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.58") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "0bddw48nawszcvyrhg75xhzydsbwyxhk11lykfj6jfk2jh9h8d4q")))

(define-public crate-oso-derive-0.27 (crate (name "oso-derive") (vers "0.27.2") (deps (list (crate-dep (name "quote") (req "^1.0.8") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.58") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "0h80md3sycpdnq5apxv8x2g27y82jv8pmhcw21qcmzvvkcpskqgy")))

(define-public crate-oso-derive-0.27 (crate (name "oso-derive") (vers "0.27.3") (deps (list (crate-dep (name "quote") (req "^1.0.8") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.58") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "174y77l6663mxjqq9ywl90iv2pra9fybqik1syyf3k30ginj7xd2")))

