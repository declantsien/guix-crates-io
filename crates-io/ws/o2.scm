(define-module (crates-io ws o2) #:use-module (crates-io))

(define-public crate-wso2-interceptor-types-0.1 (crate (name "wso2-interceptor-types") (vers "0.1.0") (deps (list (crate-dep (name "serde") (req "^1.0.196") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "1jla1d7kw6gxpqmh2figjqvja25zh1ngvav4pxx7zy76mvf5fg32")))

