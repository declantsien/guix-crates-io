(define-module (crates-io ws -o) #:use-module (crates-io))

(define-public crate-ws-oled-driver-0.0.2 (crate (name "ws-oled-driver") (vers "0.0.2") (deps (list (crate-dep (name "anyhow") (req "^1.0.69") (default-features #t) (kind 0)) (crate-dep (name "rppal") (req "^0.14.1") (default-features #t) (kind 0)))) (hash "03f4wlkdygvry50slkrzdkkr50my0acmabwjbl4sr6z5rd6m0ywv")))

(define-public crate-ws-oled-driver-0.0.3 (crate (name "ws-oled-driver") (vers "0.0.3") (deps (list (crate-dep (name "anyhow") (req "^1.0.69") (default-features #t) (kind 0)) (crate-dep (name "rppal") (req "^0.14.1") (default-features #t) (kind 0)))) (hash "0ikdjfldd6888pjk8293kha0a1kdpymm556ha7dg6fcwga4vsw9p")))

(define-public crate-ws-oled-driver-0.0.4 (crate (name "ws-oled-driver") (vers "0.0.4") (deps (list (crate-dep (name "anyhow") (req "^1.0.69") (default-features #t) (kind 0)) (crate-dep (name "rppal") (req "^0.14.1") (default-features #t) (kind 0)))) (hash "0yn1k1x5jbpimk1fmivjhhzhchqdx994dk592sfxpj6qc8amqcqz")))

(define-public crate-ws-oled-driver-0.0.5 (crate (name "ws-oled-driver") (vers "0.0.5") (deps (list (crate-dep (name "anyhow") (req "^1.0.69") (default-features #t) (kind 0)) (crate-dep (name "rppal") (req "^0.14.1") (default-features #t) (kind 0)))) (hash "0cq50pk54w9n4av0dbxdwmr6d9g40j9pcxpczn1lpa4hdlfav2cv")))

