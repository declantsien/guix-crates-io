(define-module (crates-io ws dc) #:use-module (crates-io))

(define-public crate-wsdclient-0.0.0 (crate (name "wsdclient") (vers "0.0.0") (deps (list (crate-dep (name "clap") (req "^2") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.9") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1") (default-features #t) (kind 0)))) (hash "1mh78q9j9nq0634fa9v9df8rb3ancfgw8xqxbrr439xwccc23ggm")))

