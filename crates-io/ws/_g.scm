(define-module (crates-io ws _g) #:use-module (crates-io))

(define-public crate-ws_guessing_game-0.1 (crate (name "ws_guessing_game") (vers "0.1.0") (deps (list (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)))) (hash "12dcnf5i8p445sl6w6c12yzrzxgzlypkyfaqhbv27g7pyqvr9a5h")))

