(define-module (crates-io ws cm) #:use-module (crates-io))

(define-public crate-wscml-0.1 (crate (name "wscml") (vers "0.1.1") (deps (list (crate-dep (name "chrono") (req "^0.4") (default-features #t) (kind 0)))) (hash "1xx3d5n7pvib6z23sqs8a4a4gnx3gfiv3kwy7ac36vbx0g94ch92") (yanked #t) (rust-version "1.67.1")))

(define-public crate-wscml-0.1 (crate (name "wscml") (vers "0.1.2") (deps (list (crate-dep (name "chrono") (req "^0.4") (default-features #t) (kind 0)))) (hash "07b9fscs4cra5p5z2cd1b2c4gxnvvz0d80wv7az7zlgnrlj67ibx") (yanked #t) (rust-version "1.67.1")))

(define-public crate-wscml-0.1 (crate (name "wscml") (vers "0.1.3") (deps (list (crate-dep (name "chrono") (req "^0.4") (default-features #t) (kind 0)))) (hash "1fgg6pi5ijnncs6964v2yxg54fp179hkrycf9ar12q0wr9dicmgr") (yanked #t) (rust-version "1.67.1")))

(define-public crate-wscml-0.1 (crate (name "wscml") (vers "0.1.4") (deps (list (crate-dep (name "chrono") (req "^0.4") (default-features #t) (kind 0)))) (hash "0sls4nwk9mfqz5fairlyxs97wq1hawh5n3sfl2wp25k8czqj5yab") (yanked #t) (rust-version "1.67.1")))

(define-public crate-wscml-0.1 (crate (name "wscml") (vers "0.1.5") (deps (list (crate-dep (name "chrono") (req "^0.4") (default-features #t) (kind 0)))) (hash "12y3jiqjnp3h4xy08gsjlgsapgvkmqgwgl37f8r9z9j51j3kbdi9") (yanked #t) (rust-version "1.67.1")))

(define-public crate-wscml-0.1 (crate (name "wscml") (vers "0.1.6") (deps (list (crate-dep (name "chrono") (req "^0.4") (default-features #t) (kind 0)))) (hash "1i8yx6wb2bsw42pqg35c65bbsn6lw0gp17vmmdaxqqd6j61pbg4c") (yanked #t) (rust-version "1.67.1")))

(define-public crate-wscml-0.1 (crate (name "wscml") (vers "0.1.7") (deps (list (crate-dep (name "chrono") (req "^0.4") (default-features #t) (kind 0)))) (hash "0vfwmv7l7clzf8rdvz22wxl335l3vlsqnnwy3i97j2wgv5jxwfw7") (yanked #t) (rust-version "1.67.1")))

(define-public crate-wscml-0.1 (crate (name "wscml") (vers "0.1.8") (deps (list (crate-dep (name "chrono") (req "^0.4") (default-features #t) (kind 0)))) (hash "0c24vmdwphmgqi8im61wg1bwvgn2jfpqb57abm5k47m0rh7vh8ph") (yanked #t) (rust-version "1.67.1")))

(define-public crate-wscml-0.1 (crate (name "wscml") (vers "0.1.9") (deps (list (crate-dep (name "chrono") (req "^0.4") (default-features #t) (kind 0)))) (hash "0ik74pfagpvgi997mprj0jviga5w44snkfwyaiw7n9nz6ryglabn") (yanked #t) (rust-version "1.67.1")))

(define-public crate-wscml-0.1 (crate (name "wscml") (vers "0.1.10") (deps (list (crate-dep (name "chrono") (req "^0.4") (default-features #t) (kind 0)))) (hash "01s7kvf417h7mi756p3nf32kd8lwdhc0hg8r36yk0vblhs04kha1") (yanked #t) (rust-version "1.68.2")))

(define-public crate-wscml-0.1 (crate (name "wscml") (vers "0.1.11") (deps (list (crate-dep (name "chrono") (req "^0.4") (default-features #t) (kind 0)))) (hash "13jv8fwy1mcbpxrynjqcn8nx2p0y6a1mxcbg6jhj1hk11ylzcsrm") (yanked #t) (rust-version "1.68.2")))

(define-public crate-wscml-0.2 (crate (name "wscml") (vers "0.2.1") (deps (list (crate-dep (name "chrono") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "futures") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "mini-redis") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "0c4gq5d0g8mlgicbfn1jq6zwyrylv9f8z6qf6cpdw8m1j85i2kb6") (yanked #t) (rust-version "1.68.2")))

(define-public crate-wscml-0.2 (crate (name "wscml") (vers "0.2.2") (deps (list (crate-dep (name "chrono") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "futures") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "mini-redis") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "0jrsl254g8yxlhp9p1ilhjssyx8qfq07l3zv2r46b3ai4pn7n1nl") (yanked #t) (rust-version "1.69.0")))

(define-public crate-wscml-0.2 (crate (name "wscml") (vers "0.2.3") (deps (list (crate-dep (name "chrono") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "futures") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "mini-redis") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "0klcakl006mahpmghnqb3lk1jpq791sp1kvmjxg6m396faj189yd") (rust-version "1.69.0")))

