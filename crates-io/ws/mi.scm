(define-module (crates-io ws mi) #:use-module (crates-io))

(define-public crate-wsmirror-0.1 (crate (name "wsmirror") (vers "0.1.0") (deps (list (crate-dep (name "anyhow") (req "^1.0.44") (default-features #t) (kind 0)) (crate-dep (name "futures") (req "^0.3.17") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.12.0") (features (quote ("net" "time" "rt" "macros"))) (default-features #t) (kind 0)) (crate-dep (name "tokio-tungstenite") (req "^0.15.0") (default-features #t) (kind 0)))) (hash "17sls06b8wkk2cgpsq568zx2m0h5dipgbdphhq5s2fiqizs32y8x")))

