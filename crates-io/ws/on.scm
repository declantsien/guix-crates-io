(define-module (crates-io ws on) #:use-module (crates-io))

(define-public crate-wson-0.1 (crate (name "wson") (vers "0.1.0") (deps (list (crate-dep (name "nom") (req "^7.1.1") (default-features #t) (kind 0)))) (hash "1529d33cbgfxkxck7pgjanahf1dni0my9vrmjk2395ypmqinn6q1")))

(define-public crate-wson-0.1 (crate (name "wson") (vers "0.1.1") (deps (list (crate-dep (name "nom") (req "^7.1.1") (default-features #t) (kind 0)))) (hash "0lx6s99a0g671n712hmmj1qb9lmd2p0d5jv57g9dv8hzaz25aipr")))

