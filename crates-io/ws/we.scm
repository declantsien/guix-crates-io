(define-module (crates-io ws we) #:use-module (crates-io))

(define-public crate-wsweep-0.1 (crate (name "wsweep") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^4.3.11") (default-features #t) (kind 0)) (crate-dep (name "enable-ansi-support") (req "^0.2.1") (default-features #t) (kind 0)))) (hash "1h54ys0w3160gkfb3qb3188andq4jyv9bbzmsr4ww94nyx4man6m")))

