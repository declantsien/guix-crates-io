(define-module (crates-io ws #{2m}#) #:use-module (crates-io))

(define-public crate-ws2markdown-0.2 (crate (name "ws2markdown") (vers "0.2.0") (deps (list (crate-dep (name "anyhow") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "pest") (req "^2.7") (default-features #t) (kind 0)) (crate-dep (name "pest_derive") (req "^2.7") (default-features #t) (kind 0)) (crate-dep (name "rfd") (req "^0.11") (default-features #t) (kind 0)))) (hash "0j34n2wj6apn7zbw2sjplkvi074ldik4l84575ff80cjiixn2pba")))

