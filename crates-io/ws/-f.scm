(define-module (crates-io ws -f) #:use-module (crates-io))

(define-public crate-ws-frame-0.1 (crate (name "ws-frame") (vers "0.1.1") (deps (list (crate-dep (name "byteorder") (req "^1.3.2") (default-features #t) (kind 0)))) (hash "1lnxx5hx3cfzb9cbvk8sw2dnw9ildx1js8bxm07vk22ma71c8jni") (features (quote (("std") ("default" "std"))))))

