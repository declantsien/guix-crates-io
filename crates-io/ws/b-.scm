(define-module (crates-io ws b-) #:use-module (crates-io))

(define-public crate-wsb-rs-0.1 (crate (name "wsb-rs") (vers "0.1.0") (hash "0ggpy2w4yrw3vl5gc1pynj1yrphbhjkhmln8rwqwfvjk9bi11b6d")))

(define-public crate-wsb-rs-0.1 (crate (name "wsb-rs") (vers "0.1.1") (hash "01jdggpx61sz01893p8vmlgz6mxr3s58bq0kkvv6rr7j15gir7xh")))

(define-public crate-wsb-rs-0.1 (crate (name "wsb-rs") (vers "0.1.2") (hash "0f3k8g0880ca9d20xird5indq493afvhbfspj43f5yyjqmjizlzp")))

