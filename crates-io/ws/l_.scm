(define-module (crates-io ws l_) #:use-module (crates-io))

(define-public crate-wsl_open_browser-2021 (crate (name "wsl_open_browser") (vers "2021.822.842") (deps (list (crate-dep (name "unwrap") (req "^1.2.1") (default-features #t) (kind 0)))) (hash "131hjnd7ynpg1xlmxg4na565d01whl33yxwr6zkxd3b85algh090") (yanked #t)))

(define-public crate-wsl_open_browser-2021 (crate (name "wsl_open_browser") (vers "2021.823.702") (deps (list (crate-dep (name "unwrap") (req "^1.2.1") (default-features #t) (kind 0)))) (hash "0xnycw3rv8if4n0gkwjzqqkq1nh7vwzd8dhh8gf15i2bvh12znn3")))

