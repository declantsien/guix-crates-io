(define-module (crates-io ws er) #:use-module (crates-io))

(define-public crate-wserver-0.1 (crate (name "wserver") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^2.33.3") (default-features #t) (kind 0)) (crate-dep (name "gotham") (req "^0.5.0") (default-features #t) (kind 0)))) (hash "1hk40f0dii9scpk40hvq5j3g1z3gwp1yzhfzf2gv7087hxcs3qdz")))

(define-public crate-wserver-0.1 (crate (name "wserver") (vers "0.1.1") (deps (list (crate-dep (name "clap") (req "^2.33.3") (default-features #t) (kind 0)) (crate-dep (name "gotham") (req "^0.5.0") (default-features #t) (kind 0)))) (hash "1xzqpdi4zfg93nmxlaw90bg0jnl1xshizpdid5yfrna3kvkcbd8a")))

(define-public crate-wserver-0.1 (crate (name "wserver") (vers "0.1.2") (deps (list (crate-dep (name "clap") (req "^2.33.3") (default-features #t) (kind 0)) (crate-dep (name "gotham") (req "^0.5.0") (default-features #t) (kind 0)))) (hash "147is9pqsp6jf458apr2qih5hsalw2cf7mplqrf1zjxjd78nldj1")))

(define-public crate-wserver-0.1 (crate (name "wserver") (vers "0.1.3") (deps (list (crate-dep (name "clap") (req "^2.33.3") (default-features #t) (kind 0)) (crate-dep (name "futures") (req "^0.3.1") (default-features #t) (kind 0)) (crate-dep (name "gotham") (req "^0.5.0") (default-features #t) (kind 0)))) (hash "1bjrgk25jzk14p6pl20paw70azkdpdm0y0kyns3rry8i62r5h3mg")))

(define-public crate-wserver-0.1 (crate (name "wserver") (vers "0.1.4") (deps (list (crate-dep (name "clap") (req "^2.34.0") (default-features #t) (kind 0)) (crate-dep (name "futures") (req "^0.3.21") (default-features #t) (kind 0)) (crate-dep (name "gotham") (req "^0.6.0") (default-features #t) (kind 0)))) (hash "01rj5bx46f4v4nyzbi8x5irid3f9zngwpxajdl98f85n0g558mc6")))

(define-public crate-wserver-0.1 (crate (name "wserver") (vers "0.1.5") (deps (list (crate-dep (name "clap") (req "^2.34.0") (default-features #t) (kind 0)) (crate-dep (name "futures") (req "^0.3.21") (default-features #t) (kind 0)) (crate-dep (name "gotham") (req "^0.7.1") (default-features #t) (kind 0)))) (hash "17nv7lkwy0wvlzc39hwc1rmz8zdvx8z673sp3xsm29kbxs2w6zpc")))

