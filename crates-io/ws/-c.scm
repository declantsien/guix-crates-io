(define-module (crates-io ws -c) #:use-module (crates-io))

(define-public crate-ws-cli-0.1 (crate (name "ws-cli") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^4.4.11") (features (quote ("cargo" "derive" "string"))) (default-features #t) (kind 0)) (crate-dep (name "toml_edit") (req "^0.21.0") (default-features #t) (kind 0)))) (hash "1vq876qhf8v00gqklxjh7igg8qnz2xa1bklaj0lyiqapczkdjaqz")))

(define-public crate-ws-cli-0.1 (crate (name "ws-cli") (vers "0.1.1") (deps (list (crate-dep (name "clap") (req "^4.4.11") (features (quote ("cargo" "derive" "string"))) (default-features #t) (kind 0)) (crate-dep (name "toml_edit") (req "^0.21.0") (default-features #t) (kind 0)))) (hash "05r90bf99dxiaa7hr9f6i1y1cqp41mfp5xrjy0zivifnjrs1skba")))

(define-public crate-ws-client-0.0.1 (crate (name "ws-client") (vers "0.0.1") (deps (list (crate-dep (name "ws-protocol") (req "^0.3") (default-features #t) (kind 0)))) (hash "1x2xvg5g2yaxsw24vnx30wfa8gwmjllyfxfwhlap5fiyv02rhj5j")))

