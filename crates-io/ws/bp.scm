(define-module (crates-io ws bp) #:use-module (crates-io))

(define-public crate-wsbps-0.1 (crate (name "wsbps") (vers "0.1.0") (deps (list (crate-dep (name "anyhow") (req "^1.0.56") (default-features #t) (kind 0)) (crate-dep (name "byteorder") (req "^1.4.3") (default-features #t) (kind 0)))) (hash "0s5i29r259gq959yp3vlq42xp6s3g3db9jyxxyw9dvimxcm8ka6a")))

(define-public crate-wsbps-0.1 (crate (name "wsbps") (vers "0.1.1") (deps (list (crate-dep (name "anyhow") (req "^1.0.56") (default-features #t) (kind 0)) (crate-dep (name "byteorder") (req "^1.4.3") (default-features #t) (kind 0)))) (hash "1f870dzl8qmpdhj45dv3z3nc2sh6990958pm9zv2h8b90l2k3v6w")))

(define-public crate-wsbps-0.1 (crate (name "wsbps") (vers "0.1.2") (deps (list (crate-dep (name "anyhow") (req "^1.0.56") (default-features #t) (kind 0)) (crate-dep (name "byteorder") (req "^1.4.3") (default-features #t) (kind 0)))) (hash "1nr47sk336yn5f97zini1q0nxd6p1nzz1lrvj4sjwcsfdd3w4hvx")))

(define-public crate-wsbps-0.1 (crate (name "wsbps") (vers "0.1.3") (deps (list (crate-dep (name "anyhow") (req "^1.0.56") (default-features #t) (kind 0)) (crate-dep (name "byteorder") (req "^1.4.3") (default-features #t) (kind 0)))) (hash "1vhcy2gxx72045rxpkl0r82hqhnj76szcscs5lx2agrywphp35ww")))

(define-public crate-wsbps-0.1 (crate (name "wsbps") (vers "0.1.4") (deps (list (crate-dep (name "anyhow") (req "^1.0.56") (default-features #t) (kind 0)) (crate-dep (name "byteorder") (req "^1.4.3") (default-features #t) (kind 0)))) (hash "1243g9myp2qql6gpqfgjwf94vwdy0zmbg4b7c5brq8dq5b04mm64")))

(define-public crate-wsbps-0.1 (crate (name "wsbps") (vers "0.1.5") (deps (list (crate-dep (name "anyhow") (req "^1.0.56") (default-features #t) (kind 0)) (crate-dep (name "byteorder") (req "^1.4.3") (default-features #t) (kind 0)))) (hash "1y98pjfkx9318v0divynpfkyg6x627q8b4grs4x76gqbg1v5rxx4")))

(define-public crate-wsbps-0.1 (crate (name "wsbps") (vers "0.1.6") (deps (list (crate-dep (name "anyhow") (req "^1.0.56") (default-features #t) (kind 0)) (crate-dep (name "byteorder") (req "^1.4.3") (default-features #t) (kind 0)))) (hash "0hy56wscv9k8mjmidh6za0r865wjl19yj1358617n884dmgh3m3h")))

(define-public crate-wsbps-0.1 (crate (name "wsbps") (vers "0.1.7") (deps (list (crate-dep (name "anyhow") (req "^1.0.56") (default-features #t) (kind 0)) (crate-dep (name "byteorder") (req "^1.4.3") (default-features #t) (kind 0)))) (hash "0iwiyrp2mpsrcglyp28jx4171a2c989yrcf0pxl05rz0d6rll7rx")))

(define-public crate-wsbps-0.2 (crate (name "wsbps") (vers "0.2.0") (deps (list (crate-dep (name "byteorder") (req "^1.4.3") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.30") (default-features #t) (kind 0)))) (hash "1jj0crdvx8322ay8k79mfk7lf5fbmf6zs1a6vjbj35fd9prvp09k")))

