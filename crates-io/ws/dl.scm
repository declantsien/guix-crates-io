(define-module (crates-io ws dl) #:use-module (crates-io))

(define-public crate-wsdl-0.1 (crate (name "wsdl") (vers "0.1.0") (deps (list (crate-dep (name "anyhow") (req "^1.0.44") (default-features #t) (kind 2)) (crate-dep (name "clap") (req "^2.33.3") (default-features #t) (kind 2)) (crate-dep (name "roxmltree") (req "^0.14.1") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.29") (default-features #t) (kind 0)))) (hash "0nkn75lg372f58w5pjj0wvndmwza9qzki1i6q8l4jdxpzc4f8i0h")))

(define-public crate-wsdl-0.1 (crate (name "wsdl") (vers "0.1.1") (deps (list (crate-dep (name "anyhow") (req "^1.0.72") (default-features #t) (kind 2)) (crate-dep (name "clap") (req "^4.3") (default-features #t) (kind 2)) (crate-dep (name "roxmltree") (req "^0.18") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.44") (default-features #t) (kind 0)))) (hash "13xsma3iv5algr9aacmkaclcnrz1phwmfp8s2jmv3p9fl2x0v13b")))

(define-public crate-wsdl-0.1 (crate (name "wsdl") (vers "0.1.2") (deps (list (crate-dep (name "anyhow") (req "^1.0.72") (default-features #t) (kind 2)) (crate-dep (name "clap") (req "^4.3") (default-features #t) (kind 2)) (crate-dep (name "roxmltree") (req "^0.18") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.44") (default-features #t) (kind 0)))) (hash "110zm1z93imq5y7wkgfjkvl72bb5iv0vy2i94g6i5kd7wvw5i8bg")))

(define-public crate-wsdl-0.1 (crate (name "wsdl") (vers "0.1.3") (deps (list (crate-dep (name "anyhow") (req "^1.0.72") (default-features #t) (kind 2)) (crate-dep (name "clap") (req "^4.3") (default-features #t) (kind 2)) (crate-dep (name "roxmltree") (req "^0.18") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.44") (default-features #t) (kind 0)))) (hash "0vb6cylwr6i5vx2k5x5hq6gmjhp6g7ypikvpmd03h32r3yljl3fl")))

