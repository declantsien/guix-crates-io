(define-module (crates-io ws tr) #:use-module (crates-io))

(define-public crate-wstr-0.1 (crate (name "wstr") (vers "0.1.0") (deps (list (crate-dep (name "proc-macro-hack") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "widestring") (req "^0.2") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "wstr_impl") (req "^0.1") (default-features #t) (kind 0)))) (hash "02cwnqd6wa3mb91w2yzhck9j2zb7x6c9kn87fhzpxba6bshbg8ab")))

(define-public crate-wstr-0.2 (crate (name "wstr") (vers "0.2.0") (deps (list (crate-dep (name "proc-macro-hack") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "widestring") (req "^0.2") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "wstr_impl") (req "^0.2") (default-features #t) (kind 0)))) (hash "1yfbcwdwhckjv182wc90zkql34m7g20yw23vqxv0bc2vqc0l6spx")))

(define-public crate-wstr_impl-0.1 (crate (name "wstr_impl") (vers "0.1.0") (deps (list (crate-dep (name "proc-macro-hack") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^0.11") (default-features #t) (kind 0)))) (hash "0mbhkdikz8bn9wwy90p3ldbhrm7gxrzz62r1bycd6dhaksgajh0f")))

(define-public crate-wstr_impl-0.2 (crate (name "wstr_impl") (vers "0.2.0") (deps (list (crate-dep (name "proc-macro-hack") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^0.11") (default-features #t) (kind 0)))) (hash "0dxk2dlxwkargjw1h0s6nimyh3mvm4dgyrzg78ngqcni171fval8")))

(define-public crate-wstring_tools-0.1 (crate (name "wstring_tools") (vers "0.1.0") (deps (list (crate-dep (name "former") (req "~0.1") (default-features #t) (kind 0)) (crate-dep (name "wtest_basic") (req "~0.1") (default-features #t) (kind 2)))) (hash "18lij2lxijsz12mj7npiy4bqrk8v72cjz4xyfbgsh77yv67ds5nj")))

(define-public crate-wstring_tools-0.1 (crate (name "wstring_tools") (vers "0.1.1") (deps (list (crate-dep (name "former") (req "~0.1") (default-features #t) (kind 0)) (crate-dep (name "test_tools") (req "~0.1") (default-features #t) (kind 2)) (crate-dep (name "woptions") (req "~0.1") (default-features #t) (kind 0)))) (hash "11r9n3fcw5l41jp3hgr18gw1jsq36qghk7zz91rgfcc240iqznhk")))

(define-public crate-wstring_tools-0.1 (crate (name "wstring_tools") (vers "0.1.2") (deps (list (crate-dep (name "former") (req "~0.1") (default-features #t) (kind 0)) (crate-dep (name "test_tools") (req "~0.1") (default-features #t) (kind 2)))) (hash "1dx07qjmb6j105bjf2gk37x2aajwn6bx4rvks7lnxsv7c1cvdbxr") (features (quote (("split") ("parse" "split") ("indentation") ("full" "indentation" "parse" "split") ("default" "indentation" "parse" "split"))))))

(define-public crate-wstring_tools-0.1 (crate (name "wstring_tools") (vers "0.1.4") (deps (list (crate-dep (name "strs_tools") (req "~0.1") (default-features #t) (kind 0)) (crate-dep (name "test_tools") (req "~0.1") (default-features #t) (kind 2)))) (hash "1pr9a6m8d5km99wsisaj73xnw53hcgzgysmfd05isp5y2m6xsi6x") (features (quote (("use_std") ("use_alloc") ("split") ("parse" "split") ("indentation") ("full" "indentation" "parse" "split" "use_alloc") ("default" "indentation" "parse" "split" "use_std"))))))

(define-public crate-wstring_tools-0.1 (crate (name "wstring_tools") (vers "0.1.5") (deps (list (crate-dep (name "strs_tools") (req "~0.1") (default-features #t) (kind 0)) (crate-dep (name "test_tools") (req "~0.1") (default-features #t) (kind 2)))) (hash "18cvc2jf627jpriajiqch2dpy5viyqh2884bh2ci8d05gp5m0m83") (features (quote (("use_std" "strs_tools/use_std") ("use_alloc" "strs_tools/use_alloc") ("split" "strs_tools/split") ("parse_number" "strs_tools/parse_number") ("parse" "split" "parse_number") ("indentation" "strs_tools/indentation") ("full" "use_std" "indentation" "parse" "split" "parse_number") ("default" "use_std" "indentation" "parse" "split" "parse_number"))))))

