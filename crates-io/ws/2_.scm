(define-module (crates-io ws #{2_}#) #:use-module (crates-io))

(define-public crate-ws2_32-sys-0.0.1 (crate (name "ws2_32-sys") (vers "0.0.1") (deps (list (crate-dep (name "winapi") (req "*") (default-features #t) (kind 0)))) (hash "0lm8w4172axd79d5pl2mfg44k4k62a1rr8p390mksgl02m1rrm4y")))

(define-public crate-ws2_32-sys-0.1 (crate (name "ws2_32-sys") (vers "0.1.0") (deps (list (crate-dep (name "winapi") (req "*") (default-features #t) (kind 0)) (crate-dep (name "winapi-build") (req "*") (default-features #t) (kind 1)))) (hash "0rhhjdvdm321kddnk41frw009li6mf4lb75sn66y65wpgai5xpay")))

(define-public crate-ws2_32-sys-0.2 (crate (name "ws2_32-sys") (vers "0.2.0") (deps (list (crate-dep (name "winapi") (req "^0.2.4") (default-features #t) (kind 0)) (crate-dep (name "winapi-build") (req "^0.1.1") (default-features #t) (kind 1)))) (hash "1n79driq2qvnaxqvfz8yr95jsn5vdpaf9p04c5p9nxjzbcw6qpzf")))

(define-public crate-ws2_32-sys-0.2 (crate (name "ws2_32-sys") (vers "0.2.1") (deps (list (crate-dep (name "winapi") (req "^0.2.5") (default-features #t) (kind 0)) (crate-dep (name "winapi-build") (req "^0.1.1") (default-features #t) (kind 1)))) (hash "0ppscg5qfqaw0gzwv2a4nhn5bn01ff9iwn6ysqnzm4n8s3myz76m")))

