(define-module (crates-io ws el) #:use-module (crates-io))

(define-public crate-wselector-0.1 (crate (name "wselector") (vers "0.1.0") (deps (list (crate-dep (name "failure") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "nom") (req "^5.1") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_yaml") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "toml_edit") (req "^0.2") (default-features #t) (kind 0)))) (hash "17j71rzb33y4inah87c79wrrlr9f71jcwh44s5z8l36wwnnax337")))

