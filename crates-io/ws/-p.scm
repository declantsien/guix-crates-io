(define-module (crates-io ws -p) #:use-module (crates-io))

(define-public crate-ws-protocol-0.1 (crate (name "ws-protocol") (vers "0.1.0") (deps (list (crate-dep (name "rand") (req "^0.8.3") (default-features #t) (kind 0)) (crate-dep (name "sha1") (req "^0.6") (default-features #t) (kind 0)))) (hash "0nsnayb0cm9yhs0l2g9pj3s4pfl4agbisrnk3i3m53yjyxj5m85n")))

(define-public crate-ws-protocol-0.2 (crate (name "ws-protocol") (vers "0.2.0") (deps (list (crate-dep (name "rand") (req "^0.8.3") (default-features #t) (kind 0)) (crate-dep (name "sha1") (req "^0.6") (default-features #t) (kind 0)))) (hash "0v7qrnam6r05xjml4b173x01pnnj8xja13lwvg9rg9xm8i043i37")))

(define-public crate-ws-protocol-0.3 (crate (name "ws-protocol") (vers "0.3.0") (deps (list (crate-dep (name "rand") (req "^0.8.3") (default-features #t) (kind 0)) (crate-dep (name "sha1") (req "^0.6") (default-features #t) (kind 0)))) (hash "1vqik11rb2fwygzqyy7k0j5mskwjci5jfsw012g97mn3zlpf8g1h")))

