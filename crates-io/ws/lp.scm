(define-module (crates-io ws lp) #:use-module (crates-io))

(define-public crate-wslpath-0.0.1 (crate (name "wslpath") (vers "0.0.1") (hash "0x7l3r0k36fmkmxq3cckfj23h54irgmc0l1d9x3m3saklhayzmk8")))

(define-public crate-wslpath-0.0.2 (crate (name "wslpath") (vers "0.0.2") (hash "18y7bq24l8090z6a4ac0vdf667xv02ycy6ypjdm3mly45kgyr8h4")))

(define-public crate-wslpath-rs-0.1 (crate (name "wslpath-rs") (vers "0.1.0") (deps (list (crate-dep (name "typed-path") (req "^0.7") (default-features #t) (kind 0)))) (hash "1knsjwd4scwmgd5x6gffmrnfm10d7ng350028n0kilmgkjb47y7f")))

(define-public crate-wslpath2-0.0.1 (crate (name "wslpath2") (vers "0.0.1") (hash "0g22ql7xqrm7h8ybhvzvrzj0aiy3bj8qrppzy4mifswhads37689") (yanked #t)))

(define-public crate-wslpath2-0.1 (crate (name "wslpath2") (vers "0.1.0") (hash "10mi5zzknnvqr1rfxwajr2n9kdqkbqk9rsc5fvlnc6nxzb9jzy06")))

(define-public crate-wslpath2-0.1 (crate (name "wslpath2") (vers "0.1.1") (hash "0skg57zifhvslayd87p5980j969qr0dn2xv436k26xg2izvzk9bl")))

