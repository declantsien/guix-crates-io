(define-module (crates-io zl el) #:use-module (crates-io))

(define-public crate-zlelaunch-0.1 (crate (name "zlelaunch") (vers "0.1.0") (deps (list (crate-dep (name "yaml-rust") (req "^0.4") (default-features #t) (kind 0)))) (hash "0k4l5xx58fw3qx1i0h9b2xkhafmbl64fk8l8fhml287qjss5wnps")))

