(define-module (crates-io zl og) #:use-module (crates-io))

(define-public crate-zlog-0.1 (crate (name "zlog") (vers "0.1.0") (deps (list (crate-dep (name "log") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "time") (req "^0.1") (default-features #t) (kind 0)))) (hash "0hy1kmdawdzq51b5ngb2dbplb11ny9m3hcdf9w27qgz7dih5qhkq")))

