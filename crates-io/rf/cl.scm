(define-module (crates-io rf cl) #:use-module (crates-io))

(define-public crate-rfclib-0.1 (crate (name "rfclib") (vers "0.1.0") (deps (list (crate-dep (name "chrono") (req "^0.4") (features (quote ("serde"))) (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.11") (features (quote ("json"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "tokio-test") (req "^0.4") (default-features #t) (kind 2)))) (hash "01yybikg6wkd8c1q2kg6ibyvnpgrwd32w2zd7k70dnzq0jazv88z")))

