(define-module (crates-io rf le) #:use-module (crates-io))

(define-public crate-rflex-0.1 (crate (name "rflex") (vers "0.1.0") (deps (list (crate-dep (name "fixedbitset") (req "^0.1.9") (default-features #t) (kind 0)) (crate-dep (name "liquid") (req "^0.17") (default-features #t) (kind 0)))) (hash "07jwhpl3h87274pmwk7q64kb4mf8xcqsvg897i2k327fr8wqg1li")))

(define-public crate-rflex-0.2 (crate (name "rflex") (vers "0.2.0") (deps (list (crate-dep (name "fixedbitset") (req "^0.1.9") (default-features #t) (kind 0)) (crate-dep (name "liquid") (req "^0.17") (default-features #t) (kind 0)))) (hash "0cy3h4q1cv6sb1mkzx4kasr3is8sprs0jyfrz9i92mxnjay5jq0h")))

(define-public crate-rflex-0.2 (crate (name "rflex") (vers "0.2.1") (deps (list (crate-dep (name "fixedbitset") (req "^0.1.9") (default-features #t) (kind 0)) (crate-dep (name "liquid") (req "^0.17") (default-features #t) (kind 0)))) (hash "0n3r34b4q2z56vgcjxplhs9whi1f2lpjhny5wvic36hkqazdq6f1")))

(define-public crate-rflex-0.3 (crate (name "rflex") (vers "0.3.0") (deps (list (crate-dep (name "fixedbitset") (req "^0.1.9") (default-features #t) (kind 0)) (crate-dep (name "liquid") (req "^0.17") (default-features #t) (kind 0)))) (hash "14lysy9p3lrya0krsqyzgifpg8rrbqh31vmflzbkh7xi3axj5q2m")))

(define-public crate-rflex-0.4 (crate (name "rflex") (vers "0.4.0") (deps (list (crate-dep (name "fixedbitset") (req "^0.1.9") (default-features #t) (kind 0)) (crate-dep (name "liquid") (req "^0.17") (default-features #t) (kind 0)))) (hash "0r5nh8zrjc4lvgcp1zkx3yqnv8qmragicfk25hsgpxn94fcm34nj")))

(define-public crate-rflex-0.5 (crate (name "rflex") (vers "0.5.0") (deps (list (crate-dep (name "failure") (req "^0.1.5") (default-features #t) (kind 0)) (crate-dep (name "fixedbitset") (req "^0.1.9") (default-features #t) (kind 0)) (crate-dep (name "liquid") (req "^0.17") (default-features #t) (kind 0)))) (hash "0vihsdwcd2wha1xzgmrmyz4khj83pj0ys6cr9jrijmnbrs203z1l")))

(define-public crate-rflex-0.6 (crate (name "rflex") (vers "0.6.0") (deps (list (crate-dep (name "failure") (req "^0.1.5") (default-features #t) (kind 0)) (crate-dep (name "fixedbitset") (req "^0.1.9") (default-features #t) (kind 0)) (crate-dep (name "liquid") (req "^0.17") (default-features #t) (kind 0)))) (hash "1qfdnazc5dvm605kgn5lii753drpwhwr9aikdh9j5kimzi7i0wba")))

(define-public crate-rflex-0.7 (crate (name "rflex") (vers "0.7.0") (deps (list (crate-dep (name "failure") (req "^0.1.5") (default-features #t) (kind 0)) (crate-dep (name "fixedbitset") (req "^0.1.9") (default-features #t) (kind 0)) (crate-dep (name "liquid") (req "^0.17") (default-features #t) (kind 0)))) (hash "1aj7vnrir7q53iwlczcp5285xzn9qvxgkg44b7iq6r2bh76fdkl8")))

(define-public crate-rflex-0.8 (crate (name "rflex") (vers "0.8.0") (deps (list (crate-dep (name "failure") (req "^0.1.5") (default-features #t) (kind 0)) (crate-dep (name "fixedbitset") (req "^0.1.9") (default-features #t) (kind 0)) (crate-dep (name "liquid") (req "^0.17") (default-features #t) (kind 0)))) (hash "1rjvzdx7jqc9bik6f0wvknqvphjp7p3vzl9380zivd2qr6cbdxw7")))

(define-public crate-rflex-0.8 (crate (name "rflex") (vers "0.8.1") (deps (list (crate-dep (name "failure") (req "^0.1.5") (default-features #t) (kind 0)) (crate-dep (name "fixedbitset") (req "^0.1.9") (default-features #t) (kind 0)) (crate-dep (name "liquid") (req "^0.17") (default-features #t) (kind 0)))) (hash "1v41q8ffdl8017gsk3hk7jzysg5sj177d6idllgp9b8ciimyc1ir")))

