(define-module (crates-io rf mt) #:use-module (crates-io))

(define-public crate-rfmt-0.1 (crate (name "rfmt") (vers "0.1.0") (deps (list (crate-dep (name "getopts") (req "^0.2.4") (default-features #t) (kind 0)) (crate-dep (name "rsyntax") (req "^1.10.0") (default-features #t) (kind 0)) (crate-dep (name "walkdir") (req "^0.1") (default-features #t) (kind 0)))) (hash "1c3aa19v8bj35c4fnp3f833k0al0j35n51kw3mdidnzss9hvxylc")))

