(define-module (crates-io rf m9) #:use-module (crates-io))

(define-public crate-rfm9x-0.1 (crate (name "rfm9x") (vers "0.1.0") (deps (list (crate-dep (name "bitflags") (req "^1.2.1") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.7.2") (default-features #t) (kind 0)) (crate-dep (name "rppal") (req "^0.11.3") (default-features #t) (kind 0)))) (hash "1v6gc5y09psvn05lx0bmh5cs6ca06qik81plpnmkr6zy5r8jrvhl")))

(define-public crate-rfm9x-0.1 (crate (name "rfm9x") (vers "0.1.1") (deps (list (crate-dep (name "bitflags") (req "^1.2.1") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.7.2") (default-features #t) (kind 0)) (crate-dep (name "rppal") (req "^0.11.3") (default-features #t) (kind 0)))) (hash "1rf5bvvn6mjs4vpc65lw0r4xk8lsb8nykiljwkmf2sxnsma9vz2g")))

(define-public crate-rfm9x-0.1 (crate (name "rfm9x") (vers "0.1.2") (deps (list (crate-dep (name "bitflags") (req "^1.2.1") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.7.2") (default-features #t) (kind 0)) (crate-dep (name "rppal") (req "^0.11.3") (default-features #t) (kind 0)))) (hash "0xschajw89vg0dsmafr6jx456g66ykv8sk9nmqbdpclip7sfkdl5")))

