(define-module (crates-io rf ge) #:use-module (crates-io))

(define-public crate-rfgen-0.1 (crate (name "rfgen") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^4.3.23") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.9.3") (default-features #t) (kind 0)))) (hash "1hcdxlcf9x5yc79kwxl38r6if0n3f0996wsn8nlgyr9k7ql6mm6q")))

(define-public crate-rfgen-0.2 (crate (name "rfgen") (vers "0.2.0") (deps (list (crate-dep (name "clap") (req "^4.3.23") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "env_logger") (req "^0.10.0") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.20") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.9.3") (default-features #t) (kind 0)))) (hash "1s8h5xjjawddchapd93iqdlzqgxx2wxlp66p6ypfwzsmr78jb1ar")))

