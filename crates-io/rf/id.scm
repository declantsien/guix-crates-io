(define-module (crates-io rf id) #:use-module (crates-io))

(define-public crate-rfid-0.0.1 (crate (name "rfid") (vers "0.0.1") (hash "0saaghvf8cvc46svk488jsznqf7cshlblaqzj8v65fqih7vsm2m0")))

(define-public crate-rfid-debug-0.0.1 (crate (name "rfid-debug") (vers "0.0.1") (deps (list (crate-dep (name "bit_field") (req "^0.10.1") (default-features #t) (kind 0)))) (hash "0hb2qab121pnyvsdyw9bqp4zc8cwhypjs7dj1rzq973cdl0f9ycq") (yanked #t)))

(define-public crate-rfid-debug-0.0.2 (crate (name "rfid-debug") (vers "0.0.2") (deps (list (crate-dep (name "bit_field") (req "^0.10.1") (default-features #t) (kind 0)))) (hash "1pfh4dzq0y9flf9wmvxg743yv4lsr8zwhfbcx2cs1ac41c7nnz8s")))

(define-public crate-rfid-debug-0.0.3 (crate (name "rfid-debug") (vers "0.0.3") (deps (list (crate-dep (name "bit_field") (req "^0.10.1") (default-features #t) (kind 0)))) (hash "0czxg7ci44c6nan0f8zc9h9qz003kx556v48iry4pfi4jyzbmka7")))

(define-public crate-rfid-debug-0.0.4 (crate (name "rfid-debug") (vers "0.0.4") (deps (list (crate-dep (name "bit_field") (req "^0.10.1") (default-features #t) (kind 0)))) (hash "0xs6m2aksqh5adbavfjy7x03d6wzh4qlv88622xgps85i412jdcr")))

(define-public crate-rfid-debug-0.0.5 (crate (name "rfid-debug") (vers "0.0.5") (deps (list (crate-dep (name "bit_field") (req "^0.10.1") (default-features #t) (kind 0)))) (hash "1i0jqjfk7567nl42cqqcb6jv3s41vnwj8mkyfs7x0ln1km3cfw2a")))

(define-public crate-rfid-debug-0.0.6 (crate (name "rfid-debug") (vers "0.0.6") (deps (list (crate-dep (name "bit_field") (req "^0.10.1") (default-features #t) (kind 0)))) (hash "15vq9q70nc0viwsfj46l5lsjadz9lfpp55c5s76s545787viwqw3")))

(define-public crate-rfid-debug-0.0.7 (crate (name "rfid-debug") (vers "0.0.7") (deps (list (crate-dep (name "bit_field") (req "^0.10.1") (default-features #t) (kind 0)))) (hash "11yk868dn6a2474c8gv0y9xnrq6jfx6smg7fbgh94wjs4dydrl3i")))

(define-public crate-rfid-rs-0.1 (crate (name "rfid-rs") (vers "0.1.0") (deps (list (crate-dep (name "spidev") (req "^0.4") (default-features #t) (kind 0)))) (hash "0cwrm7dwsb1l585vdw3cjjah8r5ksknnmzcpaw50l95888xs0xqf")))

(define-public crate-rfid-rs-0.1 (crate (name "rfid-rs") (vers "0.1.1") (deps (list (crate-dep (name "spidev") (req "^0.4") (default-features #t) (kind 0)))) (hash "12yjnd7sa2a4cqdhvk0rvkhlywfq8sld4j82byjyc06s41ssq0c8")))

(define-public crate-rfid-rs-0.1 (crate (name "rfid-rs") (vers "0.1.2") (deps (list (crate-dep (name "spidev") (req "^0.4") (default-features #t) (kind 0)))) (hash "1dig2rj6922jfv7f03shp4na4ga4ag62012w8bvbhlvlydbx7gnn")))

(define-public crate-rfid-rs-0.1 (crate (name "rfid-rs") (vers "0.1.3") (deps (list (crate-dep (name "spidev") (req "^0.4") (default-features #t) (kind 0)))) (hash "0m4758yk6lkd22ly4llhhr9nz95z1s00jwk9n6kfry0hky2l282f")))

(define-public crate-rfid-rs-0.2 (crate (name "rfid-rs") (vers "0.2.0") (deps (list (crate-dep (name "embedded-hal") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "linux-embedded-hal") (req "^0.3.2") (default-features #t) (kind 2)))) (hash "1hyadznakiv594dgf02rlw81akm7yxibphaim7nhvi2qahlwa57h")))

(define-public crate-rfid-rs-0.2 (crate (name "rfid-rs") (vers "0.2.1") (deps (list (crate-dep (name "embedded-hal") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "linux-embedded-hal") (req "^0.3.2") (default-features #t) (kind 2)))) (hash "0v30c1kyk2iq1x9ypk4s7mmi5nvampgvsi84m5gv9llq196200b1")))

