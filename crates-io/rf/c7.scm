(define-module (crates-io rf c7) #:use-module (crates-io))

(define-public crate-rfc7239-0.1 (crate (name "rfc7239") (vers "0.1.0") (deps (list (crate-dep (name "uncased") (req "^0.9.3") (default-features #t) (kind 0)))) (hash "0ixsyn8y2jfhfqnhwivgil3cvdr4jdr5s0nr7gqq3d3yryrifwq8")))

(define-public crate-rfc7239-0.1 (crate (name "rfc7239") (vers "0.1.1") (deps (list (crate-dep (name "uncased") (req "^0.9.10") (default-features #t) (kind 0)))) (hash "0qqhdbxriyv6k0irmsxizga62a7raywfl83adp8kc0svxdgah1mi")))

(define-public crate-rfc7468-0.0.0 (crate (name "rfc7468") (vers "0.0.0") (hash "09ydb5j15ll2kjnwps4q2xi49wlkqp4kn1kx1mrdpi86lizg4yvc")))

(define-public crate-rfc7693-0.1 (crate (name "rfc7693") (vers "0.1.0") (hash "09ppr9a5klh4ja137sps4pwcnqsmp1k2pfc6lsmbhyaylanrmwad")))

