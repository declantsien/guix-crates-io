(define-module (crates-io rf ki) #:use-module (crates-io))

(define-public crate-rfkillr-0.1 (crate (name "rfkillr") (vers "0.1.3") (deps (list (crate-dep (name "anyhow") (req "^1.0.44") (default-features #t) (kind 0)) (crate-dep (name "nix") (req "^0.26.2") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.160") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "tracing") (req "^0.1.37") (default-features #t) (kind 0)) (crate-dep (name "zvariant") (req "^3.12.0") (optional #t) (default-features #t) (kind 0)))) (hash "18m9i50l7nyjcd8l6lr2089mfnjmn7zgwss2hlxf9fsipa86x8aq") (features (quote (("serialization" "serde" "zvariant"))))))

