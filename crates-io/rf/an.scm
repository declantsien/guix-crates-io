(define-module (crates-io rf an) #:use-module (crates-io))

(define-public crate-rfang-0.1 (crate (name "rfang") (vers "0.1.0") (deps (list (crate-dep (name "lazy_static") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)))) (hash "0y872s5l493p8mp6jzq8pg0brg7mry1rmm003dfjfwslihj1hhl9")))

(define-public crate-rfang-0.1 (crate (name "rfang") (vers "0.1.1") (deps (list (crate-dep (name "lazy_static") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)))) (hash "1jfih2gclang4qxm9b6wr9r4v59wi49xlz3b51bkf87kiy535aar")))

(define-public crate-rfang-0.1 (crate (name "rfang") (vers "0.1.2") (deps (list (crate-dep (name "atty") (req "0.2.*") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)))) (hash "0dq4n0zy4ppbs9g1yrr4bqgb8r679hrm9ghik9wiqy0c4l7hpnba")))

(define-public crate-rfang-0.1 (crate (name "rfang") (vers "0.1.3") (deps (list (crate-dep (name "atty") (req "0.2.*") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)))) (hash "0cz3dxcfpzgpxzrvgikbd4sgm02m6vfk3s49kx6d2ma39hh9wx5g")))

(define-public crate-rfang-0.1 (crate (name "rfang") (vers "0.1.4") (deps (list (crate-dep (name "atty") (req "0.2.*") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)))) (hash "0xlrx6211i0wsrspsc4v81jzdg9i9bxilzyq548xiq6zgccwhh9j")))

(define-public crate-rfang-0.1 (crate (name "rfang") (vers "0.1.5") (deps (list (crate-dep (name "lazy_static") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)))) (hash "1rfykkvgwqc8g9rinqj5r2bv1gf42pjmv0wi7y2iyx1bjm8dp4kj")))

