(define-module (crates-io rf #{44}#) #:use-module (crates-io))

(define-public crate-rf4463-0.1 (crate (name "rf4463") (vers "0.1.0") (deps (list (crate-dep (name "embedded-hal") (req "^0.2.7") (default-features #t) (kind 0)))) (hash "0yj8xf78zklk963mwqclzkxkjpayagdgdc11s100bvfkg6617svq")))

