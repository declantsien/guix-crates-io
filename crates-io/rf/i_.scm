(define-module (crates-io rf i_) #:use-module (crates-io))

(define-public crate-rfi_codegen-0.1 (crate (name "rfi_codegen") (vers "0.1.0-dev") (deps (list (crate-dep (name "rfi") (req "^0.1.0-dev") (default-features #t) (kind 0)))) (hash "1xaxa9gz552d3yqg453p3mi8m91i8ghyi85shvy8mik4wyv2jrj7")))

