(define-module (crates-io rf m6) #:use-module (crates-io))

(define-public crate-rfm69-0.1 (crate (name "rfm69") (vers "0.1.0") (deps (list (crate-dep (name "embedded-hal") (req "^0.2") (default-features #t) (kind 0)))) (hash "1sxng8i6fqb8snzkaf5yj2jx7y4cqsjmfbw9aj6bqgkcgyax39l5") (yanked #t)))

(define-public crate-rfm69-0.1 (crate (name "rfm69") (vers "0.1.1") (deps (list (crate-dep (name "embedded-hal") (req "^0.2") (default-features #t) (kind 0)))) (hash "029mihjfjgsgg694ip52pfkv1089p0xspgil2gsha1faqlph87nr")))

(define-public crate-rfm69-0.2 (crate (name "rfm69") (vers "0.2.0") (deps (list (crate-dep (name "embedded-hal") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "linux-embedded-hal") (req "^0.3") (default-features #t) (kind 2)))) (hash "0swyaaij3y5z77pyzkgk2hcgap8rb9lh7ck8ryjw4dnhvjyszzr3")))

(define-public crate-rfm69-0.2 (crate (name "rfm69") (vers "0.2.1") (deps (list (crate-dep (name "embedded-hal") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "linux-embedded-hal") (req "^0.3") (default-features #t) (kind 2)))) (hash "0cv5r99i55m5x4klkb1a0krhki7w3vnx46l06lij1mwylwp96zrc")))

(define-public crate-rfm69-0.3 (crate (name "rfm69") (vers "0.3.0") (deps (list (crate-dep (name "embedded-hal") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "linux-embedded-hal") (req "^0.3") (default-features #t) (kind 2)))) (hash "1y97m8p3rh9fyg8xi9mnrp1jj2caz04867xgmpcmgwlkbzrlnn7h")))

(define-public crate-rfm69-0.3 (crate (name "rfm69") (vers "0.3.1") (deps (list (crate-dep (name "embedded-hal") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "linux-embedded-hal") (req "^0.3") (default-features #t) (kind 2)))) (hash "0plila0x327f0md9fj72l9ry76sz1ssi57bpifsfavdy9412w6kq")))

(define-public crate-rfm69-0.4 (crate (name "rfm69") (vers "0.4.0") (deps (list (crate-dep (name "embedded-hal") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "linux-embedded-hal") (req "^0.3") (default-features #t) (kind 2)))) (hash "0la3499xj190179p67cq4dinx2jd0ymbshv259gvvbbgcc9zwkps")))

(define-public crate-rfm69-0.4 (crate (name "rfm69") (vers "0.4.1") (deps (list (crate-dep (name "anyhow") (req "^1.0") (default-features #t) (kind 2)) (crate-dep (name "embedded-hal") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "linux-embedded-hal") (req "^0.3.1") (default-features #t) (kind 2)))) (hash "1qk6qmxs6vrkpmdfv570n87ask53qnjkcribhwypq8mbp4jcagpx")))

(define-public crate-rfm69-0.5 (crate (name "rfm69") (vers "0.5.0") (deps (list (crate-dep (name "anyhow") (req "^1.0") (default-features #t) (kind 2)) (crate-dep (name "embedded-hal") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "linux-embedded-hal") (req "^0.3.2") (default-features #t) (kind 2)))) (hash "0w2xagv6w1mw6dlwqxqw5s85df276pa3k0ihnlx1clab81q5ahbv")))

(define-public crate-rfm69-0.6 (crate (name "rfm69") (vers "0.6.0") (deps (list (crate-dep (name "anyhow") (req "^1.0") (default-features #t) (kind 2)) (crate-dep (name "embedded-hal") (req "^1.0.0") (default-features #t) (kind 0)) (crate-dep (name "embedded-hal-bus") (req "^0.1.0") (features (quote ("std"))) (default-features #t) (kind 2)) (crate-dep (name "linux-embedded-hal") (req "^0.4.0") (default-features #t) (kind 2)))) (hash "1i20hpa54334d8aa2279lc7lxyx6j7wfffjzdvvg4cllg5cw3j5j")))

(define-public crate-rfm69-async-0.0.1 (crate (name "rfm69-async") (vers "0.0.1") (deps (list (crate-dep (name "embedded-hal-1") (req "=1.0.0-alpha.10") (default-features #t) (kind 0) (package "embedded-hal")) (crate-dep (name "embedded-hal-async") (req "^0.2.0-alpha.1") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)))) (hash "16fs77167fjf9wim4gf1wjd5c29fvxx2scq2n5vsg0qzmb4w3nhw")))

(define-public crate-rfm69-async-0.0.2 (crate (name "rfm69-async") (vers "0.0.2") (deps (list (crate-dep (name "embassy-time") (req "^0.1.0") (features (quote ("defmt" "defmt-timestamp-uptime" "unstable-traits" "nightly"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "embedded-hal-1") (req "=1.0.0-alpha.10") (default-features #t) (kind 0) (package "embedded-hal")) (crate-dep (name "embedded-hal-async") (req "^0.2.0-alpha.1") (default-features #t) (kind 0)) (crate-dep (name "heapless") (req "^0.7.16") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)))) (hash "1fm2dz3myfhil10px1k7r2j2adfpi43w96hipwydbqs2a1wx70m1") (v 2) (features2 (quote (("embassy" "dep:embassy-time"))))))

