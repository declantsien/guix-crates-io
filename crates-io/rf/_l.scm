(define-module (crates-io rf _l) #:use-module (crates-io))

(define-public crate-rf_logger-0.2 (crate (name "rf_logger") (vers "0.2.0") (deps (list (crate-dep (name "chrono") (req "^0.4.31") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.20") (default-features #t) (kind 0)))) (hash "0qd4ad8bifmh2vwsd1041q2qmzmvmbcisvzlxd58348avn0p0vja")))

