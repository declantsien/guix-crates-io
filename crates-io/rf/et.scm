(define-module (crates-io rf et) #:use-module (crates-io))

(define-public crate-rfetch-0.1 (crate (name "rfetch") (vers "0.1.0") (hash "00kp8nvwm8sg6zcdl5a70nm4kk37ac8xxr4ppxwq1yzi7h8zc6jg") (yanked #t)))

(define-public crate-rfetch-0.1 (crate (name "rfetch") (vers "0.1.1") (hash "1fc68rxvsvlk53x0s592n45zsvnvwgywjxx5x9kncswdcacz69k6")))

(define-public crate-rfetch-0.2 (crate (name "rfetch") (vers "0.2.0") (hash "0az89lqifhxkl6b9qp0lwijs9vwp5ik45aic8m88ppracniw5v47")))

(define-public crate-rfetch-0.3 (crate (name "rfetch") (vers "0.3.0") (hash "0v13y9j1hk493hvd4s0yy8p61abrpcv2bkryirxr36i4lbpxibxc")))

(define-public crate-rfetch-0.4 (crate (name "rfetch") (vers "0.4.0") (hash "18mrqwbp0yb09iasmgdbniijl91aqy7v5dahl9x0w0jskdnz55kd")))

(define-public crate-rfetch-0.4 (crate (name "rfetch") (vers "0.4.1") (hash "070dr2bilgi8cljmpy1pzx53qqv6y1q3ri9faip1nk5arf6d9ca5")))

(define-public crate-rfetch-0.4 (crate (name "rfetch") (vers "0.4.2") (hash "0v0gydjjsa1r1vhsv43hx61yfz522l1ih6qs87gmkiaxrb42rrnl")))

