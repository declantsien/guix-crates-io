(define-module (crates-io rf in) #:use-module (crates-io))

(define-public crate-rfind-0.1 (crate (name "rfind") (vers "0.1.0") (deps (list (crate-dep (name "regex") (req "^1.5.5") (default-features #t) (kind 0)))) (hash "1xcbxghvm03x7079l2kzz84d1rnsdxy61vy8j46jzwrk57c5gj1f")))

(define-public crate-rfind-0.2 (crate (name "rfind") (vers "0.2.0") (deps (list (crate-dep (name "clap") (req "^3.1.12") (features (quote ("cargo"))) (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.5.5") (default-features #t) (kind 0)))) (hash "1qxjlmz1q6bk6by4nsrr4yxvwwkqn7d7fldlkbmbq65wkj5d8l1z")))

(define-public crate-rfind_url-0.1 (crate (name "rfind_url") (vers "0.1.0") (hash "1q8lvysprmfz43j9rhmag2knr4rh4jbikj0mw2a9n18d5xp7c7mx")))

(define-public crate-rfind_url-0.2 (crate (name "rfind_url") (vers "0.2.0") (hash "0mqczninlz51bx7ca26jdw668klimzcwq4dj0gpap88q2fhnaxby") (features (quote (("bench"))))))

(define-public crate-rfind_url-0.2 (crate (name "rfind_url") (vers "0.2.1") (hash "1pddvgmrfhyyyqmqs5rb4z77j36d61rhjma7kb4ny87fic452r4w") (features (quote (("bench"))))))

(define-public crate-rfind_url-0.3 (crate (name "rfind_url") (vers "0.3.0") (hash "124bg35rxiis1pbk5iwjvh00q166qf9bzim826vd1vrxfz5wcibl") (features (quote (("bench"))))))

(define-public crate-rfind_url-0.3 (crate (name "rfind_url") (vers "0.3.1") (hash "1jjdnm8w7nkmv7rqnv8p90rc7n063pcc1x88l8dsywp54k1pbznb") (features (quote (("bench")))) (yanked #t)))

(define-public crate-rfind_url-0.4 (crate (name "rfind_url") (vers "0.4.0") (hash "17vs08rglr0yi016pldm78anvi8zg9k6qlvgsdzhqfp502prz55m") (features (quote (("bench"))))))

(define-public crate-rfind_url-0.4 (crate (name "rfind_url") (vers "0.4.1") (hash "0ws990fasxkhii7jpq4q3p3n5kgkgv3w63xb6bbpkk6q2j6pk46h") (features (quote (("bench"))))))

(define-public crate-rfind_url-0.4 (crate (name "rfind_url") (vers "0.4.2") (hash "0syk8q2qbf9f4px8lp3g9214px65r4xrflhd563bksl6chrgdx64") (features (quote (("bench"))))))

(define-public crate-rfind_url-0.4 (crate (name "rfind_url") (vers "0.4.3") (hash "0qfgsy6ajvrziks0hnn66240zn2y7kq58x70dyh1f0vpnb1ss4hg") (features (quote (("bench"))))))

(define-public crate-rfind_url-0.4 (crate (name "rfind_url") (vers "0.4.4") (hash "1mlpy981a2xb17x744rid03i0b9b7h1fv8q4is2kkq1r82n2k7yd") (features (quote (("bench"))))))

(define-public crate-rfinger-0.0.0 (crate (name "rfinger") (vers "0.0.0") (hash "1gic1lq2bfvdgkdjzx10rymaqigaw7km1cny04vba17vh5b9fjgc")))

