(define-module (crates-io rf yl) #:use-module (crates-io))

(define-public crate-rfyl-0.1 (crate (name "rfyl") (vers "0.1.0") (deps (list (crate-dep (name "rand") (req "^0.3") (default-features #t) (kind 0)))) (hash "1xwac4sh4lf56kvmx1aayp89qhvrzqfna80xaj3ziiv3d22xybrb")))

(define-public crate-rfyl-0.1 (crate (name "rfyl") (vers "0.1.1") (deps (list (crate-dep (name "rand") (req "^0.3") (default-features #t) (kind 0)))) (hash "106dzqq55yv7lp41p770lp7g4ibvh79cg8xchja5rz31y6ad1fks")))

(define-public crate-rfyl-0.2 (crate (name "rfyl") (vers "0.2.0") (deps (list (crate-dep (name "rand") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "time") (req "^0.1") (default-features #t) (kind 0)))) (hash "15x6imxmks5vfnl58xqjbcch9yfnf5pca58hy9r20zgbdcvb2wzw")))

(define-public crate-rfyl-0.3 (crate (name "rfyl") (vers "0.3.0") (deps (list (crate-dep (name "rand") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "time") (req "^0.1") (default-features #t) (kind 0)))) (hash "0g2s17xgzayamg8pbjpmfl3wmjfihv6h9sbii9xlryqrj9f2m62l")))

(define-public crate-rfyl-0.3 (crate (name "rfyl") (vers "0.3.1") (deps (list (crate-dep (name "rand") (req "^0.6") (default-features #t) (kind 0)) (crate-dep (name "time") (req "^0.1") (default-features #t) (kind 0)))) (hash "1wk991jhsj4qmfqjq6s68vr4jfg4j1vvafyhgzghxl41z0zjxvpa")))

