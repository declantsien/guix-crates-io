(define-module (crates-io rf tr) #:use-module (crates-io))

(define-public crate-rftrace-0.1 (crate (name "rftrace") (vers "0.1.0") (hash "1pdy8q1yz72blfdcjdx3mw15rixa11wmnsjpnj799jwpgvby4bma") (features (quote (("interruptsafe") ("default" "interruptsafe") ("buildcore") ("autokernel"))))))

(define-public crate-rftrace-0.2 (crate (name "rftrace") (vers "0.2.0") (deps (list (crate-dep (name "llvm-tools") (req "^0.1") (default-features #t) (kind 1)))) (hash "1rji4jincwr227scrq0n2dv9pg43ffvq6wm9wg4g2bwpyy0k9y0s") (features (quote (("staticlib") ("interruptsafe") ("default"))))))

(define-public crate-rftrace-0.2 (crate (name "rftrace") (vers "0.2.1") (deps (list (crate-dep (name "llvm-tools") (req "^0.1") (default-features #t) (kind 1)))) (hash "17xw0pm97bli4y5h4mak0rzx018rl1apmlbzcb82zf3mm8la6kdp") (features (quote (("staticlib") ("interruptsafe") ("default"))))))

(define-public crate-rftrace-frontend-0.1 (crate (name "rftrace-frontend") (vers "0.1.0") (deps (list (crate-dep (name "byteorder") (req "^1.3.2") (default-features #t) (kind 0)))) (hash "16k3530yqrjcxpp2553zk2qagp182q3d8mkk9q3n30k8dlkmm6j1")))

(define-public crate-rftrace-frontend-0.2 (crate (name "rftrace-frontend") (vers "0.2.0") (deps (list (crate-dep (name "byteorder") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "cfg-if") (req "^1") (default-features #t) (kind 0)))) (hash "1nagns6f1d2wgnf6zsljcf65yn5v7q6qhpfdcm9i1pa0zb0r768j")))

(define-public crate-rftrace-frontend-0.2 (crate (name "rftrace-frontend") (vers "0.2.1") (deps (list (crate-dep (name "byteorder") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "cfg-if") (req "^1") (default-features #t) (kind 0)))) (hash "0a40rc2z3wb595y5ab9xc3fdpkwagnw9mjxz4cjk2knc28x1vvym")))

(define-public crate-rftrace-frontend-ffi-0.1 (crate (name "rftrace-frontend-ffi") (vers "0.1.0") (deps (list (crate-dep (name "rftrace-frontend") (req "^0.1") (default-features #t) (kind 0)))) (hash "0iddrp3qpkxw8yljs1s38sdifgw3ksykcdl0yrzng7mji680ky8h")))

(define-public crate-rftrace-frontend-ffi-0.2 (crate (name "rftrace-frontend-ffi") (vers "0.2.0") (deps (list (crate-dep (name "rftrace-frontend") (req "^0.2") (default-features #t) (kind 0)))) (hash "1p41cj9wnzbw9hayr869j7wrds8m1h3zn6v4b2z8cxrchhhac8q5")))

(define-public crate-rftrace-frontend-ffi-0.2 (crate (name "rftrace-frontend-ffi") (vers "0.2.1") (deps (list (crate-dep (name "rftrace-frontend") (req "^0.2") (default-features #t) (kind 0)))) (hash "197hrs86xqxpvfxcn3mhl4p87kami366sbx828zqvic2xym7w6zs")))

