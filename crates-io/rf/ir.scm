(define-module (crates-io rf ir) #:use-module (crates-io))

(define-public crate-rfirebird-0.1 (crate (name "rfirebird") (vers "0.1.0") (deps (list (crate-dep (name "argopt") (req "^0.3.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "byteorder") (req "^1.4.3") (default-features #t) (kind 0)) (crate-dep (name "num_enum") (req "^0.5.7") (default-features #t) (kind 0)) (crate-dep (name "tabled") (req "^0.8.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.24") (default-features #t) (kind 0)))) (hash "0v1dh78x2j91dhaqvqzhp47x8knk7mqpz4rlbw1qp22g6dyyzxbr") (features (quote (("default" "cli")))) (v 2) (features2 (quote (("cli" "dep:argopt" "dep:tabled"))))))

