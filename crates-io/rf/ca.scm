(define-module (crates-io rf ca) #:use-module (crates-io))

(define-public crate-rfcalc-0.1 (crate (name "rfcalc") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^4.3.4") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "1y0zflima984y5z8bhaffw2h7dvv4nyrwjxnrgcs7xiqcr1lvw0s")))

(define-public crate-rfcalc-0.1 (crate (name "rfcalc") (vers "0.1.1") (deps (list (crate-dep (name "clap") (req "^4.3.4") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "0l1vyqh02fmxmlz25kr1glhh0s0ixzdx5sbz9pgj48li9x1ib1zi")))

(define-public crate-rfcalc-0.2 (crate (name "rfcalc") (vers "0.2.0") (deps (list (crate-dep (name "clap") (req "^4.3.4") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "0wjl86j9j8ppfhwdrls773ja6iydygaq1781j9w7k87a9shds1l4")))

(define-public crate-rfcalc-0.2 (crate (name "rfcalc") (vers "0.2.1") (deps (list (crate-dep (name "clap") (req "^4.3.4") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "03iiwxv60xr99s3mi10pwb1f9ykr6imkfpajh6zg4jvd137gp7w3")))

(define-public crate-rfcalc-0.3 (crate (name "rfcalc") (vers "0.3.0") (deps (list (crate-dep (name "clap") (req "^4.3.4") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.10.3") (default-features #t) (kind 0)))) (hash "0wwfgli90c61ymzh4gcaakiq9kmwrcdhcnhx03b7ws5jq1pgfzvv")))

(define-public crate-rfcalc-0.3 (crate (name "rfcalc") (vers "0.3.1") (deps (list (crate-dep (name "clap") (req "^4.3.4") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.10.3") (default-features #t) (kind 0)))) (hash "1ihl5cd9idx4d27hpr1j3wsa6n7rr7j90n22qhnhcj4as75znb70")))

(define-public crate-rfcalc-0.3 (crate (name "rfcalc") (vers "0.3.2") (deps (list (crate-dep (name "clap") (req "^4.3.4") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.10.3") (default-features #t) (kind 0)))) (hash "0zfah7p6magza1sqi6v0a7k9lhfyargbiy5ddfkcq2rbk72n15z8")))

(define-public crate-rfcalc-0.3 (crate (name "rfcalc") (vers "0.3.3") (deps (list (crate-dep (name "clap") (req "^4.3.4") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.10.3") (default-features #t) (kind 0)))) (hash "18f68x02mkn1yz7z54gc7719p1ryzv431g9b5c29s22v6hgf89id")))

(define-public crate-rfcalcs-0.1 (crate (name "rfcalcs") (vers "0.1.0") (deps (list (crate-dep (name "assert_approx_eq") (req "^1.0.0") (default-features #t) (kind 0)))) (hash "01w2dnqgf30c55182nry0jwxd6hwf3b48zb9ya5y08f5pbp2cnq0")))

(define-public crate-rfcalcs-0.1 (crate (name "rfcalcs") (vers "0.1.1") (deps (list (crate-dep (name "assert_approx_eq") (req "^1.0.0") (default-features #t) (kind 0)))) (hash "0n16cpd1zxvliv95f0bmrracwglpj42hrdamwxxmspik9p0mqw34")))

