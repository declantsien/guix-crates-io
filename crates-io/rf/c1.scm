(define-module (crates-io rf c1) #:use-module (crates-io))

(define-public crate-rfc1700-0.1 (crate (name "rfc1700") (vers "0.1.0") (hash "0712qpzmmvpkmf9lq2bfvn8xn7y2ihkl9rncjij8vhl6ijvrzwjk")))

(define-public crate-rfc1700-0.1 (crate (name "rfc1700") (vers "0.1.1") (hash "0qnx4kb3fsrin1yl3q2b4w8a4816akndqghb50mawjw6n0w4yc4k")))

(define-public crate-rfc1700-0.1 (crate (name "rfc1700") (vers "0.1.2") (hash "0m59vgxz5i7iagdjpxacmbp9cqbx70c5g2rhzrs4kfba9h4b0hs3")))

(define-public crate-rfc1700-1 (crate (name "rfc1700") (vers "1.0.0") (hash "0i943v2l3yfwhn23dy4z00w0fwa59d6vppnn41p0s99jfscabbhq")))

(define-public crate-rfc1751-0.1 (crate (name "rfc1751") (vers "0.1.0") (deps (list (crate-dep (name "rand") (req "^0.3") (default-features #t) (kind 2)))) (hash "18k39cwb5infnbkaggsggzvahgpcjmlw6p6717misxna4600x5gm")))

(define-public crate-rfc1939-0.1 (crate (name "rfc1939") (vers "0.1.0") (deps (list (crate-dep (name "nom") (req "^7.1.3") (default-features #t) (kind 0)))) (hash "1w07mwn74csallvj2595mckvicaq2qra7p1bi223r4zgvb3gvg3w")))

(define-public crate-rfc1939-1 (crate (name "rfc1939") (vers "1.0.0") (deps (list (crate-dep (name "nom") (req "^7.1.3") (default-features #t) (kind 0)))) (hash "1ff4y5b78lc2sprdgpn2ckdag7hh49hwfm8hnnqy339nr95rz2g0")))

