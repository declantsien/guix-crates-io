(define-module (crates-io rf du) #:use-module (crates-io))

(define-public crate-rfdups-0.0.1 (crate (name "rfdups") (vers "0.0.1") (deps (list (crate-dep (name "clap") (req "^3.1.6") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "crc32fast") (req "^1.3.2") (default-features #t) (kind 0)) (crate-dep (name "crossterm") (req "^0.23.1") (default-features #t) (kind 0)) (crate-dep (name "num-format") (req "^0.4.0") (default-features #t) (kind 0)))) (hash "0hj8kplqszxz3gjfrirzz7hyg5pag398ibc0qx399ia6a14sqy33")))

