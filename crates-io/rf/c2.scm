(define-module (crates-io rf c2) #:use-module (crates-io))

(define-public crate-rfc2047-0.1 (crate (name "rfc2047") (vers "0.1.0") (hash "00zzvnn9kx6bpmnfzr0f18phb58sn7rd43pk9c28z8wng7zdla7y")))

(define-public crate-rfc2047-0.1 (crate (name "rfc2047") (vers "0.1.1") (hash "05gfrn5w3p4j5yv5ahyz8ac6sagrbgsfvz9002sr43539zgbb1k9")))

(define-public crate-rfc2047-decoder-0.1 (crate (name "rfc2047-decoder") (vers "0.1.0") (deps (list (crate-dep (name "base64") (req "^0.13.0") (default-features #t) (kind 0)) (crate-dep (name "charset") (req "^0.1.2") (default-features #t) (kind 0)) (crate-dep (name "quoted_printable") (req "^0.4.2") (default-features #t) (kind 0)))) (hash "10jr2rkxx9dvf02g1s38h1pl2dvmvyd56fy6i2nd448sflp825i3")))

(define-public crate-rfc2047-decoder-0.1 (crate (name "rfc2047-decoder") (vers "0.1.1") (deps (list (crate-dep (name "base64") (req "^0.13.0") (default-features #t) (kind 0)) (crate-dep (name "charset") (req "^0.1.2") (default-features #t) (kind 0)) (crate-dep (name "quoted_printable") (req "^0.4.2") (default-features #t) (kind 0)))) (hash "12pfi2nk8h59w85ry0fhdwb02rxw81k7lh885s3hc6w7fxnimglc")))

(define-public crate-rfc2047-decoder-0.1 (crate (name "rfc2047-decoder") (vers "0.1.2") (deps (list (crate-dep (name "base64") (req "^0.13.0") (default-features #t) (kind 0)) (crate-dep (name "charset") (req "^0.1.2") (default-features #t) (kind 0)) (crate-dep (name "quoted_printable") (req "^0.4.2") (default-features #t) (kind 0)))) (hash "0xwxl6ykyfkp476cjfnigm1f36kyfaxsm5k7w9an2i3z72xg5v47")))

(define-public crate-rfc2047-decoder-0.1 (crate (name "rfc2047-decoder") (vers "0.1.3") (deps (list (crate-dep (name "base64") (req "^0.13.0") (default-features #t) (kind 0)) (crate-dep (name "charset") (req "^0.1.2") (default-features #t) (kind 0)) (crate-dep (name "chumsky") (req "^0.8.0") (default-features #t) (kind 0)) (crate-dep (name "memchr") (req "^2.5.0") (default-features #t) (kind 0)) (crate-dep (name "quoted_printable") (req "^0.4.2") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.31") (default-features #t) (kind 0)))) (hash "0jrdvagyf9p2yhxi2g99ii8dfjvbsgxadpndwk87zmm716x5pk62")))

(define-public crate-rfc2047-decoder-0.2 (crate (name "rfc2047-decoder") (vers "0.2.0") (deps (list (crate-dep (name "base64") (req "^0.13.0") (default-features #t) (kind 0)) (crate-dep (name "charset") (req "^0.1.2") (default-features #t) (kind 0)) (crate-dep (name "chumsky") (req "^0.8.0") (default-features #t) (kind 0)) (crate-dep (name "memchr") (req "^2.5.0") (default-features #t) (kind 0)) (crate-dep (name "quoted_printable") (req "^0.4.2") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.31") (default-features #t) (kind 0)))) (hash "048f0f3pwwf7b62rrh71w9hv2wghgg1r34dgg8v1skp3980psd0i")))

(define-public crate-rfc2047-decoder-0.2 (crate (name "rfc2047-decoder") (vers "0.2.1") (deps (list (crate-dep (name "base64") (req "^0.13.0") (default-features #t) (kind 0)) (crate-dep (name "charset") (req "^0.1.2") (default-features #t) (kind 0)) (crate-dep (name "chumsky") (req "^0.8.0") (default-features #t) (kind 0)) (crate-dep (name "memchr") (req "^2.5.0") (default-features #t) (kind 0)) (crate-dep (name "quoted_printable") (req "^0.4.2") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.31") (default-features #t) (kind 0)))) (hash "10alfppkdlgqywzijnfffmsd2pr0va618h4qfz7332gbklyga3bf")))

(define-public crate-rfc2047-decoder-0.2 (crate (name "rfc2047-decoder") (vers "0.2.2") (deps (list (crate-dep (name "base64") (req "^0.21.0") (default-features #t) (kind 0)) (crate-dep (name "charset") (req "^0.1.2") (default-features #t) (kind 0)) (crate-dep (name "chumsky") (req "^0.9.2") (default-features #t) (kind 0)) (crate-dep (name "memchr") (req "^2.5.0") (default-features #t) (kind 0)) (crate-dep (name "quoted_printable") (req "^0.4.2") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.31") (default-features #t) (kind 0)))) (hash "0bpc2k7dp3nxc3pnsvz6zd3vc58j8q29nzibn4q3wz49a974pz31")))

(define-public crate-rfc2047-decoder-1 (crate (name "rfc2047-decoder") (vers "1.0.0") (deps (list (crate-dep (name "base64") (req "^0.21.0") (default-features #t) (kind 0)) (crate-dep (name "charset") (req "^0.1.2") (default-features #t) (kind 0)) (crate-dep (name "chumsky") (req "^0.9.2") (default-features #t) (kind 0)) (crate-dep (name "memchr") (req "^2.5.0") (default-features #t) (kind 0)) (crate-dep (name "quoted_printable") (req "^0.4.2") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.31") (default-features #t) (kind 0)))) (hash "045iv00nbqkkvzlllwksfxvhj4yyvqrg21a8229qpdmfr4icbh27")))

(define-public crate-rfc2047-decoder-1 (crate (name "rfc2047-decoder") (vers "1.0.1") (deps (list (crate-dep (name "base64") (req "^0.21.0") (default-features #t) (kind 0)) (crate-dep (name "charset") (req "^0.1.2") (default-features #t) (kind 0)) (crate-dep (name "chumsky") (req "^0.9.2") (default-features #t) (kind 0)) (crate-dep (name "memchr") (req "^2.5.0") (default-features #t) (kind 0)) (crate-dep (name "quoted_printable") (req "^0.4.2") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.31") (default-features #t) (kind 0)))) (hash "1qzpmcq62nhg1iv91s0i9w2pcbz3n0pdvxg2yn2731mghxjs9aad")))

(define-public crate-rfc2047-decoder-1 (crate (name "rfc2047-decoder") (vers "1.0.2") (deps (list (crate-dep (name "base64") (req "^0.21") (default-features #t) (kind 0)) (crate-dep (name "charset") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "chumsky") (req "^0.9") (default-features #t) (kind 0)) (crate-dep (name "memchr") (req "^2.5") (default-features #t) (kind 0)) (crate-dep (name "quoted_printable") (req "^0.5") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0") (default-features #t) (kind 0)))) (hash "0mbr3z593z9rkhya2sm81ddiwnyamzxw3c2jj0gigiazy49jcdwy")))

(define-public crate-rfc2047-decoder-1 (crate (name "rfc2047-decoder") (vers "1.0.5") (deps (list (crate-dep (name "base64") (req "^0.21") (default-features #t) (kind 0)) (crate-dep (name "charset") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "chumsky") (req "^0.9") (default-features #t) (kind 0)) (crate-dep (name "memchr") (req "^2.5") (default-features #t) (kind 0)) (crate-dep (name "quoted_printable") (req "^0.5") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0") (default-features #t) (kind 0)))) (hash "0wyc2w6bp91vvq37mbzm971i5n2kighq665f30qjqh9w8s66c2p9")))

(define-public crate-rfc2217-rs-0.1 (crate (name "rfc2217-rs") (vers "0.1.0") (deps (list (crate-dep (name "serialport") (req "^4.2.0") (optional #t) (kind 0)))) (hash "13hawbbk6fbsrcy0v62rhvd189v5n4m87llm0l7q62jc1rqhzjjn") (features (quote (("std" "serialport") ("default" "std"))))))

(define-public crate-rfc2253-0.1 (crate (name "rfc2253") (vers "0.1.0") (hash "01bkahhj5cdvk15j8wcrlxjsldcv7rs9fv3bwac09m3y1a8sngxp")))

(define-public crate-rfc2396-1 (crate (name "rfc2396") (vers "1.0.1") (deps (list (crate-dep (name "anyhow") (req "^1.0.36") (default-features #t) (kind 0)) (crate-dep (name "anyhow") (req "^1.0.36") (default-features #t) (kind 1)) (crate-dep (name "nom") (req "^6.0.1") (default-features #t) (kind 0)) (crate-dep (name "version-sync") (req "^0.9.1") (default-features #t) (kind 2)))) (hash "1xb03i6wfa7c2nj2963azagpnqpjv12x3pjx6plq7w1jgnxdk17r")))

(define-public crate-rfc2396-1 (crate (name "rfc2396") (vers "1.0.2") (deps (list (crate-dep (name "anyhow") (req "^1.0.36") (default-features #t) (kind 0)) (crate-dep (name "anyhow") (req "^1.0.36") (default-features #t) (kind 1)) (crate-dep (name "nom") (req "^6.0.1") (default-features #t) (kind 0)) (crate-dep (name "version-sync") (req "^0.9.1") (default-features #t) (kind 2)))) (hash "1b8sv6hz8d9gyl22dwb68hpq9myk83i9dsgc47xbnjhmlyw2ifc3")))

(define-public crate-rfc2396-1 (crate (name "rfc2396") (vers "1.0.3") (deps (list (crate-dep (name "anyhow") (req "^1.0.36") (default-features #t) (kind 0)) (crate-dep (name "anyhow") (req "^1.0.36") (default-features #t) (kind 1)) (crate-dep (name "nom") (req "^6.0.1") (default-features #t) (kind 0)) (crate-dep (name "version-sync") (req "^0.9.1") (default-features #t) (kind 2)))) (hash "1n0dn9hg7ydxz3a6m4l2s20y5a5wh9g10hj0mb0civc3axw0bx1j")))

(define-public crate-rfc2396-1 (crate (name "rfc2396") (vers "1.0.4") (deps (list (crate-dep (name "anyhow") (req "^1.0.37") (default-features #t) (kind 0)) (crate-dep (name "anyhow") (req "^1.0.37") (default-features #t) (kind 1)) (crate-dep (name "nom") (req "^6.0.1") (default-features #t) (kind 0)) (crate-dep (name "version-sync") (req "^0.9.1") (default-features #t) (kind 2)))) (hash "1sms65v37njncizqrfwjh0ildaf2ky7h7lsvz6bgyjd695m2anaq")))

(define-public crate-rfc2396-1 (crate (name "rfc2396") (vers "1.0.5") (deps (list (crate-dep (name "anyhow") (req "^1.0.38") (default-features #t) (kind 0)) (crate-dep (name "anyhow") (req "^1.0.38") (default-features #t) (kind 1)) (crate-dep (name "nom") (req "^6.0.1") (default-features #t) (kind 0)) (crate-dep (name "version-sync") (req "^0.9.1") (default-features #t) (kind 2)))) (hash "0446ij9p1sc128kzvb6p6qbm1281anbk1mly342z472yq5g0s6hj")))

(define-public crate-rfc2396-1 (crate (name "rfc2396") (vers "1.1.0") (deps (list (crate-dep (name "anyhow") (req "^1.0.38") (default-features #t) (kind 0)) (crate-dep (name "anyhow") (req "^1.0.38") (default-features #t) (kind 2)) (crate-dep (name "nom") (req "^6.1.0") (default-features #t) (kind 0)))) (hash "1yr7p5l5s4sl2g8kj4vp7x84az597ygnj0x57k8n43v9xnxn6hw5")))

(define-public crate-rfc2580-0.1 (crate (name "rfc2580") (vers "0.1.0") (hash "0bdamb38pffnw2chk4zvm7xzlch3jz7ya13vq1rnnq3nkm59v0ch")))

(define-public crate-rfc2580-0.1 (crate (name "rfc2580") (vers "0.1.1") (hash "1vfv18gbvgj1n0naga5fwyikgnki1s7h6j5mlqw8347m6wg82zpr")))

(define-public crate-rfc2580-0.2 (crate (name "rfc2580") (vers "0.2.0") (hash "108ny4l4wywn71dzw005h6vq0jfy9d4z1x0xzn20gwiflg4gy7q2")))

(define-public crate-rfc2580-0.3 (crate (name "rfc2580") (vers "0.3.0") (hash "1nkpa9jhkbyyj0ahacd74ci4ws60zbq23farnngdb9svk51i2r7p")))

