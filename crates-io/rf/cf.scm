(define-module (crates-io rf cf) #:use-module (crates-io))

(define-public crate-rfcfetch-0.1 (crate (name "rfcfetch") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^2.33.3") (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.11") (features (quote ("json"))) (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "1s8byg65ak1c623cl9lamzszngbmmcjj22jzhr6cfh3nqsvv0fpb")))

