(define-module (crates-io z4 -t) #:use-module (crates-io))

(define-public crate-z4-types-0.0.1 (crate (name "z4-types") (vers "0.0.1") (deps (list (crate-dep (name "anyhow") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "hex") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)))) (hash "1y23qign2l2gigcjlx5m630y2l7w0lbgsaz11ww0vcwi34cm2mkw")))

