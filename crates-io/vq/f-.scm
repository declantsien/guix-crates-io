(define-module (crates-io vq f-) #:use-module (crates-io))

(define-public crate-vqf-rs-0.1 (crate (name "vqf-rs") (vers "0.1.0") (deps (list (crate-dep (name "libm") (req "^0.2.8") (optional #t) (default-features #t) (kind 0)))) (hash "07i81qzcynnrzaiqhgrmasncpzmsbx28myqb6ymhpixy2p604fwf") (features (quote (("std") ("motion-bias-estimation") ("f32") ("default" "std" "motion-bias-estimation")))) (v 2) (features2 (quote (("libm" "dep:libm"))))))

(define-public crate-vqf-rs-0.1 (crate (name "vqf-rs") (vers "0.1.1") (deps (list (crate-dep (name "libm") (req "^0.2.8") (optional #t) (default-features #t) (kind 0)))) (hash "14ij7j5dyd529y09r4lf0m5wmvq7zlc7vjz3dkjd4sgxh4vdp4ah") (features (quote (("std") ("motion-bias-estimation") ("f32") ("default" "std" "motion-bias-estimation")))) (v 2) (features2 (quote (("libm" "dep:libm"))))))

(define-public crate-vqf-rs-0.2 (crate (name "vqf-rs") (vers "0.2.0") (deps (list (crate-dep (name "libm") (req "^0.2.8") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "micromath") (req "^2.1.0") (optional #t) (default-features #t) (kind 0)))) (hash "193yfdk0qxdkmx22iilk0n7k0d8yry94js1sn7sw5hkhfyvmii1y") (features (quote (("std") ("motion-bias-estimation") ("f32") ("default" "std" "motion-bias-estimation")))) (v 2) (features2 (quote (("micromath" "dep:micromath" "f32") ("libm" "dep:libm"))))))

