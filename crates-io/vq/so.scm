(define-module (crates-io vq so) #:use-module (crates-io))

(define-public crate-vqsort-rs-0.1 (crate (name "vqsort-rs") (vers "0.1.0") (deps (list (crate-dep (name "cc") (req "^1.0.98") (default-features #t) (kind 1)) (crate-dep (name "paste") (req "^1.0.15") (default-features #t) (kind 0)) (crate-dep (name "system-deps") (req "^6.2.2") (default-features #t) (kind 1)))) (hash "0b94zabg0w8x4wv669w20d4bgn3hqajld5s92lampqgsby0qsccv")))

(define-public crate-vqsort-rs-0.2 (crate (name "vqsort-rs") (vers "0.2.0") (deps (list (crate-dep (name "cc") (req "^1.0.98") (default-features #t) (kind 1)) (crate-dep (name "paste") (req "^1.0.15") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (kind 2)) (crate-dep (name "rand_pcg") (req "^0.3.1") (default-features #t) (kind 2)) (crate-dep (name "rustversion") (req "^1.0.17") (default-features #t) (kind 0)) (crate-dep (name "system-deps") (req "^6.2.2") (default-features #t) (kind 1)))) (hash "06151yymnf1vfm18c8g91yfkrqjj5swz9lbw6zh0szpg6hbmdj5h")))

