(define-module (crates-io ze nn) #:use-module (crates-io))

(define-public crate-zenn-0.1 (crate (name "zenn") (vers "0.1.0") (deps (list (crate-dep (name "reqwest") (req "^0.11.18") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.183") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.104") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.30.0") (features (quote ("macros" "rt-multi-thread"))) (default-features #t) (kind 0)))) (hash "1w7f2xqxpbs4appsn1wvldgbdfnsg5lf66x9xklpzqq9km51dwfl")))

