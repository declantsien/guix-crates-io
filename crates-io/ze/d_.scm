(define-module (crates-io ze d_) #:use-module (crates-io))

(define-public crate-zed_extension_api-0.0.1 (crate (name "zed_extension_api") (vers "0.0.1") (deps (list (crate-dep (name "wit-bindgen") (req "^0.18") (default-features #t) (kind 0)))) (hash "0klm7k4kjs0bnvkcqa3bxy6lmyqcpz4xk4pxs00yg9kmk7qsj211")))

(define-public crate-zed_extension_api-0.0.3 (crate (name "zed_extension_api") (vers "0.0.3") (deps (list (crate-dep (name "wit-bindgen") (req "^0.22") (default-features #t) (kind 0)))) (hash "1zihvag9g4blwqg6qy26qbiz32gvk0ifj7hjiqh0wf1pba55lfzl")))

(define-public crate-zed_extension_api-0.0.4 (crate (name "zed_extension_api") (vers "0.0.4") (deps (list (crate-dep (name "wit-bindgen") (req "^0.22") (default-features #t) (kind 0)))) (hash "0fwmgl0620xp4lz2vvd2aa4qvx1nzg5wvp10bfrmxfsj86nirifm")))

(define-public crate-zed_extension_api-0.0.5 (crate (name "zed_extension_api") (vers "0.0.5") (deps (list (crate-dep (name "wit-bindgen") (req "^0.22") (default-features #t) (kind 0)))) (hash "18clsfgrzfp4zmk0w1pxdk1grrpx6mmj76pg6lb5k01a617axx55")))

(define-public crate-zed_extension_api-0.0.6 (crate (name "zed_extension_api") (vers "0.0.6") (deps (list (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "wit-bindgen") (req "^0.22") (default-features #t) (kind 0)))) (hash "0ng4x5gkdc0pqha71ahz6lm32rc3whg0dsyvx4n2vsrzxb58pjkp")))

(define-public crate-zed_odin-0.1 (crate (name "zed_odin") (vers "0.1.0") (deps (list (crate-dep (name "zed_extension_api") (req "^0.0.6") (default-features #t) (kind 0)))) (hash "070a54cszg1n3hw842x481kkhgkgcf7jzgrdcw1akw55q04xslnv")))

(define-public crate-zed_script-0.1 (crate (name "zed_script") (vers "0.1.0") (deps (list (crate-dep (name "reqwest") (req "^0.10") (features (quote ("blocking" "json"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.116") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.58") (default-features #t) (kind 0)) (crate-dep (name "serial_test") (req "^0.4.0") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^0.2") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "1yk9564jmr6dkpfpva0n1608vy015vqvc5q2i77igb7s4xqwz90m")))

(define-public crate-zed_script-0.1 (crate (name "zed_script") (vers "0.1.1") (deps (list (crate-dep (name "reqwest") (req "^0.10") (features (quote ("blocking" "json"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.116") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.58") (default-features #t) (kind 0)) (crate-dep (name "serial_test") (req "^0.4.0") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^0.2") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "0qdp5iv1jw6s4vzxrwdkp0rb5y4hih8d4c4dn6i1q11k6qahmg2k")))

