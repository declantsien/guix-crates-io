(define-module (crates-io ze mu) #:use-module (crates-io))

(define-public crate-zemu-0.1 (crate (name "zemu") (vers "0.1.0") (hash "1mjm7icna08ryz86514r9yidhz9wh4qyfahf2zs97flm2k61j3yw")))

(define-public crate-zemu-core-0.1 (crate (name "zemu-core") (vers "0.1.0") (hash "07j26nl4fq9bfdx7g565wixbl3i51zxnyqfkbkk4ikix30pj9xk5")))

(define-public crate-zemu-gdbstub-0.1 (crate (name "zemu-gdbstub") (vers "0.1.0") (hash "0zmd687065z8ryf3dbijd0rhdxa68m35cn9bn1f95ay8fq23779b")))

(define-public crate-zemu-jolt-0.1 (crate (name "zemu-jolt") (vers "0.1.0") (hash "0kaj3696j5vmngg2vwaj77473rdsnqvbz5nfgv3m3bym84l7n5rn")))

(define-public crate-zemu-nexus-0.1 (crate (name "zemu-nexus") (vers "0.1.0") (hash "0rrqdwrnxagpidv19cmbnyrjkp1zyyhss3id1lcfhyxnq5vhcfb4")))

(define-public crate-zemu-r0-0.1 (crate (name "zemu-r0") (vers "0.1.0") (hash "1mzgrm1zciif8p9sjlvqnl1ccca973qnbf99g947qb3s6bl9kh38")))

(define-public crate-zemu-risc0-0.1 (crate (name "zemu-risc0") (vers "0.1.0") (hash "08sx2knqi6b8n4qssg9gv1rhr8f9mn6pz6mx14vwq02ns0ba94ri")))

(define-public crate-zemu-sp1-0.1 (crate (name "zemu-sp1") (vers "0.1.0") (hash "187kac666xykclzkgbv07qr7kawimyrmmfq32r52pnl826x8gmq7")))

