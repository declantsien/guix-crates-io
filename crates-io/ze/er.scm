(define-module (crates-io ze er) #:use-module (crates-io))

(define-public crate-zeerust-0.1 (crate (name "zeerust") (vers "0.1.0") (deps (list (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "stderrlog") (req "^0.4") (default-features #t) (kind 0)))) (hash "1wjvv63yy6vr6nhpz7b9wp6qwzjgqmmxw4x5fjaam9cyhdzigavb")))

(define-public crate-zeerust-0.1 (crate (name "zeerust") (vers "0.1.1") (deps (list (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "stderrlog") (req "^0.4") (default-features #t) (kind 0)))) (hash "0fr9gv79i4xv99lz87k3bdyb6ajrgqvyyfc5mrwnip4ijkdln10d")))

(define-public crate-zeerust-0.2 (crate (name "zeerust") (vers "0.2.0") (deps (list (crate-dep (name "enum-display-derive") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "stderrlog") (req "^0.4") (default-features #t) (kind 0)))) (hash "0jah0bl71n9nwfd75sgicvnav033137y5923d45lmnprkji0zk5i")))

(define-public crate-zeerust-0.2 (crate (name "zeerust") (vers "0.2.1") (deps (list (crate-dep (name "enum-display-derive") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "stderrlog") (req "^0.4") (default-features #t) (kind 0)))) (hash "0nx24myhq0vcq4rzysv7ll3ahjq6gmm383ambk7dgwm239dgdfn8")))

