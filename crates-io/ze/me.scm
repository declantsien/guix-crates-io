(define-module (crates-io ze me) #:use-module (crates-io))

(define-public crate-zemen-0.1 (crate (name "zemen") (vers "0.1.0") (deps (list (crate-dep (name "time") (req "^0.3.17") (default-features #t) (kind 0)))) (hash "08ivc15cnq71ibdjin7s8xxlqvjym7ffg8nkgsdy6rsciirkzkpr") (yanked #t) (rust-version "1.66.0")))

(define-public crate-zemen-0.1 (crate (name "zemen") (vers "0.1.1") (deps (list (crate-dep (name "time") (req "^0.3.17") (default-features #t) (kind 0)))) (hash "0rfvbsynm5f0bvf7ha173ws7l9knk840m575zlh5v6aazwzk4lw6") (rust-version "1.66.0")))

(define-public crate-zemen-0.1 (crate (name "zemen") (vers "0.1.2") (deps (list (crate-dep (name "thiserror") (req "^1.0.38") (default-features #t) (kind 0)) (crate-dep (name "time") (req "^0.3.17") (default-features #t) (kind 0)))) (hash "08zh6bqr1b5gqn3ckaqc2p69iczjb50f5djnkbq5l086p1j65zvh") (rust-version "1.66.0")))

(define-public crate-zemen-0.1 (crate (name "zemen") (vers "0.1.3") (deps (list (crate-dep (name "thiserror") (req "^1.0.38") (default-features #t) (kind 0)) (crate-dep (name "time") (req "^0.3.17") (default-features #t) (kind 0)))) (hash "0six33q518kzwj31gn4bx02r3kpbzfd344b7h48xw9s1kh4ng98j") (rust-version "1.66.0")))

(define-public crate-zemen-0.1 (crate (name "zemen") (vers "0.1.4") (deps (list (crate-dep (name "thiserror") (req "^1.0.56") (default-features #t) (kind 0)) (crate-dep (name "time") (req "^0.3.31") (default-features #t) (kind 0)))) (hash "1bn28m8nw5jp4yamhqs05lrx0gnx70pfyzijb1f8z4jdblgdb300") (rust-version "1.75.0")))

