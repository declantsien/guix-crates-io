(define-module (crates-io ze lb) #:use-module (crates-io))

(define-public crate-zelbet-0.0.0 (crate (name "zelbet") (vers "0.0.0") (hash "0car9i8qplnyv47zrbqs6l19jrnvm492laap20nmss05s4m1rwpk") (yanked #t)))

(define-public crate-zelbet-0.0.0 (crate (name "zelbet") (vers "0.0.0-next") (hash "0wb0nqcmsz86rdc5gbiyq37hi7j8c2p91imkq57k1r4cvxarp7l8")))

