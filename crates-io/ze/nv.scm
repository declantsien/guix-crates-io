(define-module (crates-io ze nv) #:use-module (crates-io))

(define-public crate-zenv-0.1 (crate (name "zenv") (vers "0.1.0") (deps (list (crate-dep (name "pico-args") (req "^0.4.0") (optional #t) (default-features #t) (kind 0)))) (hash "113hlil3b51shcw7p2vmpfcq1lj3ijvzp2qnxx20jph5sbhnrd7d") (features (quote (("cli" "pico-args"))))))

(define-public crate-zenv-0.2 (crate (name "zenv") (vers "0.2.1") (deps (list (crate-dep (name "pico-args") (req "^0.4.0") (optional #t) (default-features #t) (kind 0)))) (hash "1vsq8wkrwr05h3ipl6pw7w6iqzlgzfv3b105bn1sabcj58wk3bcp") (features (quote (("cli" "pico-args"))))))

(define-public crate-zenv-0.3 (crate (name "zenv") (vers "0.3.0") (deps (list (crate-dep (name "pico-args") (req "^0.4.0") (optional #t) (default-features #t) (kind 0)))) (hash "0fvq1na5h09vznmcrjfrlfrgdsjk9c3cm6s61di9z2c90myvcmdj") (features (quote (("cli" "pico-args"))))))

(define-public crate-zenv-0.4 (crate (name "zenv") (vers "0.4.0") (deps (list (crate-dep (name "pico-args") (req "^0.4.0") (optional #t) (default-features #t) (kind 0)))) (hash "0xbjrzw7mvpyhs316kalfw0458rwxnj8rrn9i2v317mk4spm6s3y") (features (quote (("cli" "pico-args"))))))

(define-public crate-zenv-0.5 (crate (name "zenv") (vers "0.5.0") (deps (list (crate-dep (name "pico-args") (req "^0.4.0") (optional #t) (default-features #t) (kind 0)))) (hash "1f97pkibgaldshc09f6dri893swrkxsgw89pgfij0v2wdw6r6rxz") (features (quote (("cli" "pico-args"))))))

(define-public crate-zenv-0.6 (crate (name "zenv") (vers "0.6.0") (deps (list (crate-dep (name "pico-args") (req "^0.4.1") (optional #t) (default-features #t) (kind 0)))) (hash "0vixpy21ha1m474sg7k0h6c7vv1lzrwliyzwpbjyx7lxig36cilh") (features (quote (("cli" "pico-args"))))))

(define-public crate-zenv-0.7 (crate (name "zenv") (vers "0.7.1") (deps (list (crate-dep (name "pico-args") (req "^0.4.2") (optional #t) (default-features #t) (kind 0)))) (hash "0rqg2xfrjbdgrhw8vapnkfzy6nzxkbvh7c1s8gzs5qpkpsswr851") (features (quote (("cli" "pico-args"))))))

(define-public crate-zenv-0.8 (crate (name "zenv") (vers "0.8.0") (deps (list (crate-dep (name "lexopt") (req "^0.2.0") (optional #t) (default-features #t) (kind 0)))) (hash "01v0k164aqi7ws3cd5184bbsyja6a43jjs49pwnl8jgya42815lp") (features (quote (("cli" "lexopt"))))))

