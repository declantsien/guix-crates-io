(define-module (crates-io ze di) #:use-module (crates-io))

(define-public crate-zedico-0.1 (crate (name "zedico") (vers "0.1.0") (hash "1imym241ds1kc1qi74jwgifknp5liml8x30jcck54gfggppkjz0s")))

(define-public crate-zedico-0.1 (crate (name "zedico") (vers "0.1.1") (hash "0j316x5iaybrprwxfzd8x7m1aj0hc5xnlz4hv2a8bv460q2wdy8r")))

(define-public crate-zedis-0.1 (crate (name "zedis") (vers "0.1.101") (deps (list (crate-dep (name "serde_derive") (req "^1.0.101") (default-features #t) (kind 0)) (crate-dep (name "sled") (req "^0.28.0") (default-features #t) (kind 0)) (crate-dep (name "zmq") (req "^0.9") (default-features #t) (kind 0)))) (hash "0ldd3qp82ij3npjpccgkjva29m7w869fgk07v332nhf9iyycvfr5")))

(define-public crate-zedis-0.1 (crate (name "zedis") (vers "0.1.102") (deps (list (crate-dep (name "actix-web") (req "^1.0.0") (default-features #t) (kind 0)) (crate-dep (name "bytes") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "colored") (req "^1.8") (default-features #t) (kind 0)) (crate-dep (name "env_logger") (req "^0.7.0") (default-features #t) (kind 0)) (crate-dep (name "futures") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "json") (req "^0.12.0") (default-features #t) (kind 0)) (crate-dep (name "rustyline") (req "^5.0.3") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0.101") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "sled") (req "^0.28.0") (default-features #t) (kind 0)) (crate-dep (name "sodiumoxide") (req "^0.2.4") (default-features #t) (kind 0)) (crate-dep (name "web-view") (req "^0.4.5") (default-features #t) (kind 0)) (crate-dep (name "zmq") (req "^0.9") (default-features #t) (kind 0)))) (hash "1zmya8fqmms3a97dnn4x69i048r0bm9zix2zvcbmds9jaxg2jrz3")))

(define-public crate-zedis-cli-0.1 (crate (name "zedis-cli") (vers "0.1.0") (deps (list (crate-dep (name "colored") (req "^1.8") (default-features #t) (kind 0)) (crate-dep (name "rustyline") (req "^5.0.3") (default-features #t) (kind 0)) (crate-dep (name "zmq") (req "^0.9") (default-features #t) (kind 0)))) (hash "174a15887hcxfzmzf5gmgrabyymdi0w24qijwqv95fb68rbjcl4q")))

