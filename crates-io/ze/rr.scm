(define-module (crates-io ze rr) #:use-module (crates-io))

(define-public crate-zerror-0.1 (crate (name "zerror") (vers "0.1.0") (hash "1m45jx8jygjmkg5rl44lj58s0k2b9v23cxmn127j5vy7xkjgv4pw")))

(define-public crate-zerror-0.1 (crate (name "zerror") (vers "0.1.1") (hash "10yv8i4ilbgva79m544qmccw7hrl3p1pm8x9pvwhj9gh3n36dlhb")))

(define-public crate-zerror-0.2 (crate (name "zerror") (vers "0.2.0") (hash "1f9877jay5csh798jhk4cz6h93rrh4bdqjichcxw21bg8pcgnxwk")))

(define-public crate-zerror-0.3 (crate (name "zerror") (vers "0.3.0") (hash "13hb2csjpcsbc1bfgsnyr3338fdvsrj1m29dvh778psfphndyxvv")))

(define-public crate-zerror-0.4 (crate (name "zerror") (vers "0.4.0") (hash "0zjqw5dydiws2bv6gadhw8bhywbynixbba8msi0njnshr3amwhn0")))

(define-public crate-zerror_core-0.2 (crate (name "zerror_core") (vers "0.2.0") (deps (list (crate-dep (name "biometrics") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "buffertk") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "prototk") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "prototk_derive") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "zerror") (req "^0.1") (default-features #t) (kind 0)))) (hash "1nqdbbwdbgx4p0vmjn6ls5nmikfjp2536pcr13afxx42rj0h26b8")))

(define-public crate-zerror_core-0.2 (crate (name "zerror_core") (vers "0.2.1") (deps (list (crate-dep (name "biometrics") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "buffertk") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "prototk") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "prototk_derive") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "zerror") (req "^0.1") (default-features #t) (kind 0)))) (hash "11qqpvn3j3acgc7n39m9dkvsx4v7vxs38hk800syb3b8i2f2hv3x")))

(define-public crate-zerror_core-0.2 (crate (name "zerror_core") (vers "0.2.2") (deps (list (crate-dep (name "biometrics") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "buffertk") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "prototk") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "prototk_derive") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "zerror") (req "^0.1") (default-features #t) (kind 0)))) (hash "1a92z5hfrz3g98h2zsq3hn9y6hd86jrpvkq42wl2i0ryp4ag17bg")))

(define-public crate-zerror_core-0.3 (crate (name "zerror_core") (vers "0.3.0") (deps (list (crate-dep (name "biometrics") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "buffertk") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "prototk") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "prototk_derive") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "tatl") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "zerror") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "zerror_derive") (req "^0.1") (default-features #t) (kind 0)))) (hash "0j3gphk3i0v562fs42dpmdwy9y9y02364dl0cf8ma4mdqfnb6nhf")))

(define-public crate-zerror_core-0.4 (crate (name "zerror_core") (vers "0.4.0") (deps (list (crate-dep (name "biometrics") (req "^0.5") (default-features #t) (kind 0)) (crate-dep (name "buffertk") (req "^0.5") (default-features #t) (kind 0)) (crate-dep (name "prototk") (req "^0.5") (default-features #t) (kind 0)) (crate-dep (name "prototk_derive") (req "^0.5") (default-features #t) (kind 0)) (crate-dep (name "tatl") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "zerror") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "zerror_derive") (req "^0.2") (default-features #t) (kind 0)))) (hash "1c5qjqbrmyi4hqlmn0d8kcw5zvl12j0n17ks2db0v45mmigdl4zk")))

(define-public crate-zerror_core-0.5 (crate (name "zerror_core") (vers "0.5.0") (deps (list (crate-dep (name "biometrics") (req "^0.6") (default-features #t) (kind 0)) (crate-dep (name "buffertk") (req "^0.6") (default-features #t) (kind 0)) (crate-dep (name "prototk") (req "^0.6") (default-features #t) (kind 0)) (crate-dep (name "prototk_derive") (req "^0.6") (default-features #t) (kind 0)) (crate-dep (name "tatl") (req "^0.5") (default-features #t) (kind 0)) (crate-dep (name "zerror") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "zerror_derive") (req "^0.3") (default-features #t) (kind 0)))) (hash "1kmfg8f1x684h4p4ifp9dqhv2djjrximsivx78hvbin63ml30fm6")))

(define-public crate-zerror_derive-0.1 (crate (name "zerror_derive") (vers "0.1.0") (deps (list (crate-dep (name "derive_util") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (default-features #t) (kind 0)))) (hash "1qdb74gnhaw60bnyjva0vni5y0q1npvf36x2na221l4lrhlk5kb5")))

(define-public crate-zerror_derive-0.2 (crate (name "zerror_derive") (vers "0.2.0") (deps (list (crate-dep (name "derive_util") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (default-features #t) (kind 0)))) (hash "0v9245ndq7xj56wvhgfbrpymkm0ss3biwr49nln0pralh59kjns2")))

(define-public crate-zerror_derive-0.3 (crate (name "zerror_derive") (vers "0.3.0") (deps (list (crate-dep (name "derive_util") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (default-features #t) (kind 0)))) (hash "1i0bbsd11fz27hfg3vjagsqph374ghr3w0v127bln83lyi88zjsy")))

