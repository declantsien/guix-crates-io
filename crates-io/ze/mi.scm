(define-module (crates-io ze mi) #:use-module (crates-io))

(define-public crate-zemi-identity-0.1 (crate (name "zemi-identity") (vers "0.1.0") (deps (list (crate-dep (name "base64-url") (req "^1.4.13") (default-features #t) (kind 0)) (crate-dep (name "ed25519-dalek") (req "^1.0.1") (default-features #t) (kind 0)) (crate-dep (name "rust-argon2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "sha2") (req "^0.10.6") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.37") (default-features #t) (kind 0)))) (hash "0f7yyggrvwl33m8m0iyvnd1vxzabqzys1rj7slkbv5fnh4aqjvym")))

(define-public crate-zemi-identity-0.1 (crate (name "zemi-identity") (vers "0.1.1") (deps (list (crate-dep (name "base64-url") (req "^1.4.13") (default-features #t) (kind 0)) (crate-dep (name "ed25519-dalek") (req "^1.0.1") (default-features #t) (kind 0)) (crate-dep (name "rust-argon2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "sha2") (req "^0.10.6") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.37") (default-features #t) (kind 0)))) (hash "109hg8hzk6pw1rph0wkc2vanp2hw601vva9fjjn2akjb0zb0q58h")))

