(define-module (crates-io ze te) #:use-module (crates-io))

(define-public crate-zetelverdeling-0.1 (crate (name "zetelverdeling") (vers "0.1.0") (hash "0c09minq687f6rxliw76bfsxgsc8q3zgn0riskvafdgra3z7qmzg") (yanked #t)))

(define-public crate-zetelverdeling-0.2 (crate (name "zetelverdeling") (vers "0.2.0") (hash "130cxjvmh3fjlf7l7lizxz9g4xall4qfydjfgjvclzvmxls9x2b3")))

(define-public crate-zetelverdeling-0.2 (crate (name "zetelverdeling") (vers "0.2.1") (hash "1sbl645da4kfpyhi3m877dlq88vr0mpg7v3phjgr2kd1a08743yz")))

(define-public crate-zetelverdeling-0.2 (crate (name "zetelverdeling") (vers "0.2.2") (hash "16prfv1nrksjz311hjr44kwl0q4vns2ljjplq8vq36b81l9q8fkp")))

