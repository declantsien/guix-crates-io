(define-module (crates-io ze nr) #:use-module (crates-io))

(define-public crate-zenroom-0.3 (crate (name "zenroom") (vers "0.3.0") (deps (list (crate-dep (name "bindgen") (req "^0.59.2") (default-features #t) (kind 1)) (crate-dep (name "serde_json") (req "^1.0.74") (default-features #t) (kind 2)))) (hash "0mww6i80j7qj8slfhfnv8cj68p9g7sdr87fz8qdsnkj9szkpsb42")))

(define-public crate-zenroom-0.3 (crate (name "zenroom") (vers "0.3.0-test") (deps (list (crate-dep (name "bindgen") (req "^0.59.2") (default-features #t) (kind 1)) (crate-dep (name "serde_json") (req "^1.0.74") (default-features #t) (kind 2)))) (hash "110x57y39v4afgqvr1gijpb2wq3mhll50s2ywl181dz5af4snzvr")))

(define-public crate-zenroom-0.3 (crate (name "zenroom") (vers "0.3.1") (deps (list (crate-dep (name "bindgen") (req "^0.59.2") (default-features #t) (kind 1)) (crate-dep (name "serde_json") (req "^1.0.74") (default-features #t) (kind 2)))) (hash "0vk2pic8z43siqwxj1z2faw5w66ivklvdvvg3779v3qk7qw5ibac")))

(define-public crate-zenroom-0.3 (crate (name "zenroom") (vers "0.3.2") (deps (list (crate-dep (name "bindgen") (req "^0.59.2") (default-features #t) (kind 1)) (crate-dep (name "serde_json") (req "^1.0.74") (default-features #t) (kind 2)))) (hash "1pd76d745aw04zp4sns345dpny435k3gcxcgj0wh227c8qs4cxa1")))

(define-public crate-zenroom-0.3 (crate (name "zenroom") (vers "0.3.3") (deps (list (crate-dep (name "bindgen") (req "^0.59.2") (default-features #t) (kind 1)) (crate-dep (name "serde_json") (req "^1.0.74") (default-features #t) (kind 2)))) (hash "1nib4rl9660qv04g5jaa0pr2li6iqb225gqckbhyiagprj4x307i")))

(define-public crate-zenroom_minimal-0.1 (crate (name "zenroom_minimal") (vers "0.1.0") (deps (list (crate-dep (name "base64") (req "^0.10") (default-features #t) (kind 0)) (crate-dep (name "criterion") (req "^0.2") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.7") (default-features #t) (kind 2)) (crate-dep (name "ring") (req "^0.14") (default-features #t) (kind 0)) (crate-dep (name "rlua") (req "^0.16") (default-features #t) (kind 0)) (crate-dep (name "rlua_serde") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "untrusted") (req "^0.6") (default-features #t) (kind 0)))) (hash "1vvvjdya8sbmnxxyzn6v1hm9vgj81959z9d9brrjfiqdls9h75fi")))

