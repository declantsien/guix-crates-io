(define-module (crates-io ze hn) #:use-module (crates-io))

(define-public crate-zehn-0.1 (crate (name "zehn") (vers "0.1.0") (hash "0p47rcchi2ibdv22xinjdac2qmglvd546whrsgda2v7q8ly9a68k")))

(define-public crate-zehn-0.1 (crate (name "zehn") (vers "0.1.1") (deps (list (crate-dep (name "indexmap") (req "^2.2.5") (default-features #t) (kind 0)))) (hash "11mh9mi76yc78p6k4v6cmr76sbczqj9lrz9fkgq68mznyi3fd9s4")))

