(define-module (crates-io ze n_) #:use-module (crates-io))

(define-public crate-zen_template-0.1 (crate (name "zen_template") (vers "0.1.0") (deps (list (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde_yaml") (req "^0.9") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "indoc") (req "^1.0") (default-features #t) (kind 2)) (crate-dep (name "serde") (req "^1.0") (default-features #t) (kind 2)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 2)) (crate-dep (name "serde_yaml") (req "^0.9") (default-features #t) (kind 2)) (crate-dep (name "simple_logger") (req "^4") (default-features #t) (kind 2)))) (hash "0j82mghgnwf3zxgnm7whsfx4fx46wxxbw7zzx057yx57l0xmz8ga") (features (quote (("yaml" "serde" "serde_yaml") ("json" "serde" "serde_json") ("default") ("all" "json" "yaml"))))))

(define-public crate-zen_utils-0.1 (crate (name "zen_utils") (vers "0.1.0") (deps (list (crate-dep (name "once_cell") (req "^1.9.0") (default-features #t) (kind 0)))) (hash "1zpgs3fnmr08bkc0a4vshxzk2n2dsqapz580yd7qwyclagn55g9h")))

(define-public crate-zen_utils-0.1 (crate (name "zen_utils") (vers "0.1.1") (deps (list (crate-dep (name "once_cell") (req "^1.9.0") (default-features #t) (kind 0)))) (hash "0hqifwpv44n21ma94p8dhkvd9x1rvg7gnxcmxpzpz06cjw7m11fd")))

(define-public crate-zen_utils-0.1 (crate (name "zen_utils") (vers "0.1.2") (deps (list (crate-dep (name "once_cell") (req "^1.9.0") (default-features #t) (kind 0)))) (hash "07h0xr917mwk9w877hk0kpgg5xidyqsgqqnvvvqxablkcvmaldq3")))

(define-public crate-zen_utils-0.1 (crate (name "zen_utils") (vers "0.1.3") (deps (list (crate-dep (name "once_cell") (req "^1.9.0") (default-features #t) (kind 0)))) (hash "0d8pip7fhgi3rc4zvfd1983qlzhnh0nrp3241pc5210wf00fmhmk")))

