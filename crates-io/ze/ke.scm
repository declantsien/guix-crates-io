(define-module (crates-io ze ke) #:use-module (crates-io))

(define-public crate-zeke-0.1 (crate (name "zeke") (vers "0.1.0") (hash "0jsw81b657m84vz13p15q34kvljd1dkxyisx7xnhgrgqjk5i31mf")))

(define-public crate-zeke-0.1 (crate (name "zeke") (vers "0.1.1") (deps (list (crate-dep (name "bincode") (req "^1.3.3") (default-features #t) (kind 0)) (crate-dep (name "chrono") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.200") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.37.0") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "03gzxdfagpccxsbz18fi2fck82bsw63k2xwk0spkij70a9pbakm3")))

(define-public crate-zeke-0.1 (crate (name "zeke") (vers "0.1.2") (deps (list (crate-dep (name "bincode") (req "^1.3.3") (default-features #t) (kind 0)) (crate-dep (name "chrono") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.200") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.37.0") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "0ai8fwgab7gn1vry2rqpf7v1yd94j5jc8j188q8aypx371yfr58h")))

