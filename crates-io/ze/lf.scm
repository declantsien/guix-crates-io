(define-module (crates-io ze lf) #:use-module (crates-io))

(define-public crate-zelf-0.0.1 (crate (name "zelf") (vers "0.0.1") (deps (list (crate-dep (name "clap") (req "^3.0.14") (features (quote ("derive"))) (default-features #t) (kind 2)) (crate-dep (name "derive_more") (req "^0.99.17") (default-features #t) (kind 0)))) (hash "1raxbbi2m4zmina4nwh7jy62zs6fh684rm89pgiw3fr5wmcmzkpw") (yanked #t)))

(define-public crate-zelf-0.1 (crate (name "zelf") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^3.0.14") (features (quote ("derive"))) (default-features #t) (kind 2)) (crate-dep (name "derive_more") (req "^0.99.17") (default-features #t) (kind 0)) (crate-dep (name "flate2") (req "^1.0") (default-features #t) (kind 2)))) (hash "10lhnkz42ngv5m1ciq373krv3644l9sgfi584ax1ifqp0ll8i1hf")))

