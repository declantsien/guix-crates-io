(define-module (crates-io ze ti) #:use-module (crates-io))

(define-public crate-zetik-0.0.1 (crate (name "zetik") (vers "0.0.1") (hash "1i8v9fdh5b6qx0b4s9h4hwz23c6qlm5asgkn7fpb0kdrglnqpqix")))

(define-public crate-zetik-0.0.2 (crate (name "zetik") (vers "0.0.2") (deps (list (crate-dep (name "num-integer") (req "^0.1") (default-features #t) (kind 0)))) (hash "1sa6fc1s69j3w98yc1w9rlqhw3abkswpcnpzi84m23xc5w8557x9")))

(define-public crate-zetik-0.0.3 (crate (name "zetik") (vers "0.0.3") (deps (list (crate-dep (name "num-integer") (req "^0.1") (default-features #t) (kind 0)))) (hash "012n87i5870793i1195jif77cbg23xz0n6d0c3xiilcsz7pq7l9s")))

(define-public crate-zetik-0.0.4 (crate (name "zetik") (vers "0.0.4") (deps (list (crate-dep (name "num-integer") (req "^0.1") (default-features #t) (kind 0)))) (hash "1i0mjql13cgpwmwdbsn3ix1v1rdlcaqcaik7iag34kynpqhajd0x")))

(define-public crate-zetik-0.0.5 (crate (name "zetik") (vers "0.0.5-fen") (deps (list (crate-dep (name "num-integer") (req "^0.1") (default-features #t) (kind 0)))) (hash "0rds8hgg8ib8nhlc9x0610rjasx8xnvbll5p2342xvy3sq271qxf")))

(define-public crate-zetik-0.0.5 (crate (name "zetik") (vers "0.0.5") (deps (list (crate-dep (name "num-integer") (req "^0.1") (default-features #t) (kind 0)))) (hash "0zacrpgbz1vpr6ps74c210dgs0x61gm5hfxkrk50ilyv6920xkvv")))

(define-public crate-zetik-0.0.6 (crate (name "zetik") (vers "0.0.6") (deps (list (crate-dep (name "num-integer") (req "^0.1") (default-features #t) (kind 0)))) (hash "0lwpvhhqshsrpkz8a4c2ga67wz1xkaj0dzcw210yamg0aph2j6h7")))

(define-public crate-zetik-0.0.7 (crate (name "zetik") (vers "0.0.7") (hash "0yn7ligln9s7gh6m3mzf28l1vshh7phgdzmqxbjp80qgj36q0ixb")))

(define-public crate-zetik-0.0.8 (crate (name "zetik") (vers "0.0.8") (hash "1qbgxj7y4db51sm903nqkwjaiq0rvasi1x17vrr24z1vpd5jnh10")))

(define-public crate-zetik-0.0.8 (crate (name "zetik") (vers "0.0.8-walter") (hash "1l2sb5s3kbx9463w0ml7hzl14m4wjl8b7bficw3pb6g6v12fww0b")))

(define-public crate-zetik-0.0.9 (crate (name "zetik") (vers "0.0.9-jesse") (hash "1cjva2w9m0l07wnwa3divqvps4a468l1aa52jacxs7w712xsiwqb")))

(define-public crate-zetik-0.0.9 (crate (name "zetik") (vers "0.0.9") (hash "0g9jp7p2pbyw9gixl8h4ps4cpfl0gr8awnqsy4h2b6szxyy5f9j0")))

(define-public crate-zetik-0.0.10 (crate (name "zetik") (vers "0.0.10") (hash "04hni6fk4vnqaqv1qr17bb3w93wspck4pd4q7m5w8aiw63z9d2mb")))

(define-public crate-zetik-0.0.11 (crate (name "zetik") (vers "0.0.11") (hash "0hgr5i5na0355w0b5ka81ghkyy2diddnk4ch1yg1j8flns45b7f5")))

(define-public crate-zetik-0.0.12 (crate (name "zetik") (vers "0.0.12") (hash "1czk4mqjl4j5l36hp2rlmvlmak67130l18qfifm2nbdlsdw4cyfd")))

(define-public crate-zetik-0.0.13 (crate (name "zetik") (vers "0.0.13") (hash "1dqhn9rvh1mzjcaimfxh6ijwy6y34cd6179wa03nxilajbcxf65b")))

(define-public crate-zetik-tailwind-0.1 (crate (name "zetik-tailwind") (vers "0.1.0") (hash "0w40z4pirkc4z5qm8w91p3mv64jk10axwssnngm473yhw2ij3rcq")))

(define-public crate-zetik-tailwind-0.1 (crate (name "zetik-tailwind") (vers "0.1.1") (hash "1j66a8pr1nahwl84c97c1rkq0j2nk2bsyk5k88vh4ggi9wxhmaaw")))

(define-public crate-zetik-tailwind-0.1 (crate (name "zetik-tailwind") (vers "0.1.2") (hash "077z7lkr1sld8ghjwzf3vbdn1har9mdvs1whl4wz67k1a52hjl67")))

(define-public crate-zetik-tailwind-0.1 (crate (name "zetik-tailwind") (vers "0.1.3") (hash "0jyc17i2wf8i4xqnlad54lqp7z3dwk3hwpvzawsp1hsvqzx9ssww")))

(define-public crate-zetik_prime-0.1 (crate (name "zetik_prime") (vers "0.1.0") (hash "1kdi9a86afgdfravr11x51w9m0d30id58jldpaccgjalwr40lqpx")))

(define-public crate-zetik_prime-0.1 (crate (name "zetik_prime") (vers "0.1.1") (hash "0kfwyxa6gmsq4h30d9w07i1p7snljpsgin93lyvjhzs055qr52d2")))

(define-public crate-zetik_prime-0.2 (crate (name "zetik_prime") (vers "0.2.0") (hash "1casfcj4ncjvqk8j097c5iiyg7rx8xx4mzrvwa30ydilxfl15z8q")))

(define-public crate-zetik_prime-0.2 (crate (name "zetik_prime") (vers "0.2.1") (hash "17r0ggy7116vdndgf6xbcv5yxf9894pjsssbnl20qzn3hvi707h6")))

