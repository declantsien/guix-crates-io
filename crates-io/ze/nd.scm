(define-module (crates-io ze nd) #:use-module (crates-io))

(define-public crate-zende-0.1 (crate (name "zende") (vers "0.1.0") (deps (list (crate-dep (name "version-sync") (req "^0.9") (default-features #t) (kind 2)))) (hash "016rkk8vhkvd3b6h8kc4asr5r0nxggwj2c6hy6an0qwlpqc33mff")))

(define-public crate-zende-0.1 (crate (name "zende") (vers "0.1.3") (deps (list (crate-dep (name "version-sync") (req "^0.9") (default-features #t) (kind 2)))) (hash "0cvq3kjsf8dyp6fszs39d9lmv9zqdx0rl8417bq907fi8cmdfzzm")))

(define-public crate-zende-0.1 (crate (name "zende") (vers "0.1.5") (deps (list (crate-dep (name "version-sync") (req "^0.9") (default-features #t) (kind 2)))) (hash "064ls0wcz0mncb8kxb2m8ss5j76xq5x5ahngcqh2w19yk8pqfmiz")))

(define-public crate-zende-0.1 (crate (name "zende") (vers "0.1.6") (deps (list (crate-dep (name "version-sync") (req "^0.9") (default-features #t) (kind 2)))) (hash "1043qnlsb0r7cw8f1rwh2q8vya0rkiv2cxbjhg48vmqi3vs2a11k")))

(define-public crate-zende-0.1 (crate (name "zende") (vers "0.1.8") (deps (list (crate-dep (name "version-sync") (req "^0.9") (default-features #t) (kind 2)))) (hash "0nahxag9372cpzpcc97y0g0jcjz10hwml49nl4sncxx25axx98r3")))

(define-public crate-zendesk-0.1 (crate (name "zendesk") (vers "0.1.0") (deps (list (crate-dep (name "anyhow") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "chrono") (req "^0.4") (features (quote ("serde"))) (default-features #t) (kind 0)) (crate-dep (name "parse-display") (req "^0.5.5") (default-features #t) (kind 0)) (crate-dep (name "pretty_assertions") (req "^1") (default-features #t) (kind 2)) (crate-dep (name "reqwest") (req "^0.11") (features (quote ("json" "rustls-tls"))) (kind 0)) (crate-dep (name "reqwest-middleware") (req "^0.1.6") (default-features #t) (kind 0)) (crate-dep (name "reqwest-retry") (req "^0.1.5") (default-features #t) (kind 0)) (crate-dep (name "reqwest-tracing") (req "^0.3.0") (features (quote ("opentelemetry_0_17"))) (default-features #t) (kind 0)) (crate-dep (name "schemars") (req "^0.8") (features (quote ("chrono" "uuid1"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1") (features (quote ("full" "time"))) (default-features #t) (kind 0)) (crate-dep (name "tracing") (req "^0.1") (default-features #t) (kind 0)))) (hash "0yiysf3j20d3f90vmnnbzjkpbqsmnac4nbj4ncyfpnn9nrw44089")))

