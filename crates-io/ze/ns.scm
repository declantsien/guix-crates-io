(define-module (crates-io ze ns) #:use-module (crates-io))

(define-public crate-zenscan-0.1 (crate (name "zenscan") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^2.27.0") (default-features #t) (kind 0)))) (hash "1l20asamdkxqysaqfzwvfpjj6msv31qmsr4n4lz0p3rymmmg4v89")))

(define-public crate-zenscan-0.1 (crate (name "zenscan") (vers "0.1.1") (deps (list (crate-dep (name "clap") (req "^2.27.0") (default-features #t) (kind 0)))) (hash "0rhxixhhg0q29bbdn7x6p6rkzr46maafw489icc2lysl3hyry8x1")))

(define-public crate-zenscan-0.1 (crate (name "zenscan") (vers "0.1.2") (deps (list (crate-dep (name "clap") (req "^2.27.0") (default-features #t) (kind 0)))) (hash "0za14khwhak1c8ha6fgdd380cwh55cfxsi5ijxjgf334x2fdqza3")))

(define-public crate-zenscan-0.1 (crate (name "zenscan") (vers "0.1.3") (deps (list (crate-dep (name "better-panic") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^2.27.0") (default-features #t) (kind 0)) (crate-dep (name "dns-lookup") (req "^1.0.3") (default-features #t) (kind 0)) (crate-dep (name "pnet") (req "^0.26.0") (default-features #t) (kind 0)) (crate-dep (name "rayon") (req "^1.3.0") (default-features #t) (kind 0)))) (hash "1q6npgvzrv50s5rmjcqr68nmq5q04crk478wckmv782q615hnqiv")))

(define-public crate-zensen-0.1 (crate (name "zensen") (vers "0.1.0") (hash "1yhihq2jas4xyy78dizk9p7n2i7pg9cqf60bl1bsjz98l6c7yia8")))

