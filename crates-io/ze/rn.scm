(define-module (crates-io ze rn) #:use-module (crates-io))

(define-public crate-zernike-0.1 (crate (name "zernike") (vers "0.1.0") (deps (list (crate-dep (name "plotters") (req "^0.3.0") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.8.3") (default-features #t) (kind 2)) (crate-dep (name "rayon") (req "^1.5.0") (default-features #t) (kind 0)))) (hash "0bhibsxh7268q9rwqxk4yz5z2ql99vbbr1z3avc6n0lbc6z6d136")))

(define-public crate-zernike-0.1 (crate (name "zernike") (vers "0.1.1") (deps (list (crate-dep (name "plotters") (req "^0.3.0") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.8.3") (default-features #t) (kind 2)) (crate-dep (name "rayon") (req "^1.5.0") (default-features #t) (kind 0)))) (hash "0z32diinh4jd2gcpjhbh5320hsh778mpvjwbh7hlz1ypqhxxnq64")))

(define-public crate-zernike-0.2 (crate (name "zernike") (vers "0.2.0") (deps (list (crate-dep (name "ndarray") (req "^0.14.0") (default-features #t) (kind 2)) (crate-dep (name "ndarray-npy") (req "^0.7.1") (default-features #t) (kind 2)) (crate-dep (name "plotters") (req "^0.3.0") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.8.3") (default-features #t) (kind 2)) (crate-dep (name "rayon") (req "^1.5.0") (default-features #t) (kind 0)))) (hash "0znsdndf4wwicn42vb2x1jmd584p7171yrkq76bls78pwvpnmb7k")))

(define-public crate-zernike-0.2 (crate (name "zernike") (vers "0.2.1") (deps (list (crate-dep (name "ndarray") (req "^0.14.0") (default-features #t) (kind 2)) (crate-dep (name "ndarray-npy") (req "^0.7.1") (default-features #t) (kind 2)) (crate-dep (name "plotters") (req "^0.3.0") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.8.3") (default-features #t) (kind 2)) (crate-dep (name "rayon") (req "^1.5.0") (default-features #t) (kind 0)))) (hash "1jdc78ks77h50lfs2nynxnlp5vpnn0xbjxh2n9mwrd8sz8il3ihb")))

