(define-module (crates-io ze dm) #:use-module (crates-io))

(define-public crate-zedmq-0.1 (crate (name "zedmq") (vers "0.1.0") (hash "07424crhl7qxi3g8j12dkgrmzwq1lackwdhhxy8msbs50877jnk4")))

(define-public crate-zedmq-0.2 (crate (name "zedmq") (vers "0.2.0") (hash "0q1y66r43h4awj36z0fxk79hb8d3q8pmlqpv6ap90dhb74c3rgvy")))

(define-public crate-zedmq-0.2 (crate (name "zedmq") (vers "0.2.1") (hash "15hklc8kfs7ld63h59fnbdywphv8zsvgkjijcjvsmx5w74khq8mg")))

(define-public crate-zedmq-0.3 (crate (name "zedmq") (vers "0.3.0") (hash "16fxnzz437xkgnybfj11aan0jxfzgz5sszzfz7hfb5m8ai8dr7ch")))

(define-public crate-zedmq-0.4 (crate (name "zedmq") (vers "0.4.0") (deps (list (crate-dep (name "zmq") (req ">=0.9.2, <0.10.0") (default-features #t) (kind 2)))) (hash "0wmfryiibc1q1485mhjnzgh24ycydx90dglymqg9z9nf7nd39dbr")))

(define-public crate-zedmq-0.5 (crate (name "zedmq") (vers "0.5.0") (deps (list (crate-dep (name "zmq") (req "^0.9.2") (default-features #t) (kind 2)))) (hash "09a1nkl662wj5azmjdmqmlhy97idbha3k2pkf9asm8p999z0f63h")))

(define-public crate-zedmq-0.6 (crate (name "zedmq") (vers "0.6.0") (deps (list (crate-dep (name "zmq") (req "^0.9.2") (default-features #t) (kind 2)))) (hash "0g1799h15a6gvbg465zn0l69jq177p9djcgf0ahdlfb7cfkmdd9p")))

(define-public crate-zedmq-0.7 (crate (name "zedmq") (vers "0.7.0") (deps (list (crate-dep (name "zmq") (req "^0.9.2") (default-features #t) (kind 2)))) (hash "06myxkaxyl7gwaqg6n5fzan63xhf9zaipyzb6br9l5g1vk74ji6x")))

