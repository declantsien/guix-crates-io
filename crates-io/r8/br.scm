(define-module (crates-io r8 br) #:use-module (crates-io))

(define-public crate-r8brain-rs-0.3 (crate (name "r8brain-rs") (vers "0.3.2") (deps (list (crate-dep (name "cmake") (req "^0.1") (default-features #t) (kind 1)))) (hash "16bqixjwx7dq4lib2nkhgf25q6nlfhfailpi9frbw4bd6j32alvf")))

(define-public crate-r8brain-rs-0.3 (crate (name "r8brain-rs") (vers "0.3.3") (deps (list (crate-dep (name "cmake") (req "^0.1") (default-features #t) (kind 1)))) (hash "0hq8jibq0i7516wjmykvrggnb2v37lssi12ysfm1132mj82c85bk")))

(define-public crate-r8brain-rs-0.3 (crate (name "r8brain-rs") (vers "0.3.4") (deps (list (crate-dep (name "cmake") (req "^0.1") (default-features #t) (kind 1)))) (hash "0p07vcrqna27f0rzvds9v3nnrmgkmq551d16z0x84md6ibdhngls")))

(define-public crate-r8brain-rs-0.3 (crate (name "r8brain-rs") (vers "0.3.5") (deps (list (crate-dep (name "cmake") (req "^0.1") (default-features #t) (kind 1)))) (hash "0i4bm0nwc4hl71yhd73l3hd1pr04yz6z300z0lvma21ma9dakn78")))

