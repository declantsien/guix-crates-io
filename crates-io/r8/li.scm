(define-module (crates-io r8 li) #:use-module (crates-io))

(define-public crate-r8lib-0.1 (crate (name "r8lib") (vers "0.1.0") (deps (list (crate-dep (name "log") (req "^0.4.17") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)))) (hash "0byjnlvnq5md7f8zrhyxafvjn3yc4jz6d0gzk6zy23f6d7g59mw4")))

(define-public crate-r8limit-0.1 (crate (name "r8limit") (vers "0.1.0") (hash "0v02zm7hjv8c9w02sgav9c0xsqwsr4mk0ld00sypy9nkp5famvab")))

(define-public crate-r8limit-0.1 (crate (name "r8limit") (vers "0.1.1") (hash "0j8r66gy0rsm7pwbn9rrznf3i99778n8b97lzpdxghm2ljnz677v")))

(define-public crate-r8limit-0.2 (crate (name "r8limit") (vers "0.2.0") (hash "0ssn5h70dyf81vfl29lv62xk8xxhpf7qkjc0663d595dfz1n4sfa")))

(define-public crate-r8limit-0.2 (crate (name "r8limit") (vers "0.2.1") (hash "13qw7lv70mik5hq0canjx5l1rnb3wrjdinq8mym0x1r1x6ya0lpm")))

(define-public crate-r8limit-0.2 (crate (name "r8limit") (vers "0.2.2") (hash "0m8d0707g7kbd7ja3fwi0lbbwk1ha36clg98absrbp2k3ha61k2h")))

