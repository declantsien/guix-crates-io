(define-module (crates-io yb aa) #:use-module (crates-io))

(define-public crate-ybaas-0.0.0 (crate (name "ybaas") (vers "0.0.0") (deps (list (crate-dep (name "warp") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "yubibomb") (req "^0.2") (default-features #t) (kind 0)))) (hash "05pb43l7dsaa5ag6gd83h4659x1azv0lrx4r4ldcx75f66ly17qg")))

(define-public crate-ybaas-0.0.1 (crate (name "ybaas") (vers "0.0.1") (deps (list (crate-dep (name "warp") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "yubibomb") (req "^0.2") (default-features #t) (kind 0)))) (hash "0kxi5nfrm5h52z6n69vxxz85vmdh17c06rrad600fzx26jg3ds7d")))

(define-public crate-ybaas-0.0.2 (crate (name "ybaas") (vers "0.0.2") (deps (list (crate-dep (name "tokio") (req "^0.2") (features (quote ("macros"))) (default-features #t) (kind 0)) (crate-dep (name "warp") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "yubibomb") (req "^0.2") (default-features #t) (kind 0)))) (hash "1gqlz0dr42sgp1jkfg1i0zw76n6bm2fb5icfc5y75gq6q01ajaab")))

(define-public crate-ybaas-0.0.3 (crate (name "ybaas") (vers "0.0.3") (deps (list (crate-dep (name "tokio") (req "^1.2") (features (quote ("macros" "rt-multi-thread"))) (default-features #t) (kind 0)) (crate-dep (name "warp") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "yubibomb") (req "^0.2") (default-features #t) (kind 0)))) (hash "1i1m0qigjx04c8f4sifima9dpbcnv0m846sdnalnb66j3nrfq4va")))

(define-public crate-ybaas-0.0.4 (crate (name "ybaas") (vers "0.0.4") (deps (list (crate-dep (name "tokio") (req "^1.2") (features (quote ("macros" "rt-multi-thread"))) (default-features #t) (kind 0)) (crate-dep (name "warp") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "yubibomb") (req "^0.2") (default-features #t) (kind 0)))) (hash "11x6zkd790irmz29w5n54p8nn731sl2alqwxp52v8pii51db9915")))

(define-public crate-ybaas-0.0.5 (crate (name "ybaas") (vers "0.0.5") (deps (list (crate-dep (name "tokio") (req "^1.5") (features (quote ("macros" "rt-multi-thread"))) (default-features #t) (kind 0)) (crate-dep (name "warp") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "yubibomb") (req "^0.2") (default-features #t) (kind 0)))) (hash "1glgpcg5y68wlln997qm2fcxm8h9cxcma3q3xkhkb9b9nh714rrc")))

(define-public crate-ybaas-0.0.6 (crate (name "ybaas") (vers "0.0.6") (deps (list (crate-dep (name "tokio") (req "^1.6") (features (quote ("macros" "rt-multi-thread"))) (default-features #t) (kind 0)) (crate-dep (name "warp") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "yubibomb") (req "^0.2") (default-features #t) (kind 0)))) (hash "0qf6cbc6i3jhf1n2cf351k1csl335bjzfbgqhszw44r6181fq1dr")))

(define-public crate-ybaas-0.0.7 (crate (name "ybaas") (vers "0.0.7") (deps (list (crate-dep (name "tokio") (req "^1.8") (features (quote ("macros" "rt-multi-thread"))) (default-features #t) (kind 0)) (crate-dep (name "warp") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "yubibomb") (req "^0.2") (default-features #t) (kind 0)))) (hash "1b4j1b7zrr8fl10jncqjh5c30f3zjcbngbamrjdsc10h29fp66ng")))

(define-public crate-ybaas-0.0.8 (crate (name "ybaas") (vers "0.0.8") (deps (list (crate-dep (name "tokio") (req "^1.9") (features (quote ("macros" "rt-multi-thread"))) (default-features #t) (kind 0)) (crate-dep (name "warp") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "yubibomb") (req "^0.2") (default-features #t) (kind 0)))) (hash "1hq3v7521yqhhqqzr8vn7pxq9r6ghncn2cvrgmg1h8mfq6w6k9jp")))

(define-public crate-ybaas-0.0.9 (crate (name "ybaas") (vers "0.0.9") (deps (list (crate-dep (name "tokio") (req "^1.11") (features (quote ("macros" "rt-multi-thread"))) (default-features #t) (kind 0)) (crate-dep (name "warp") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "yubibomb") (req "^0.2") (default-features #t) (kind 0)))) (hash "05hr6sp18y3ylk963f1n4ba9vbbmn4dvjwijz5g7qhzv20cv613g")))

(define-public crate-ybaas-0.0.10 (crate (name "ybaas") (vers "0.0.10") (deps (list (crate-dep (name "tokio") (req "^1.12") (features (quote ("macros" "rt-multi-thread"))) (default-features #t) (kind 0)) (crate-dep (name "warp") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "yubibomb") (req "^0.2") (default-features #t) (kind 0)))) (hash "1yp23xb5rb9d93cpvnp9bdmsqvraqhzrzsq7grid7pxdckd1d9iz")))

(define-public crate-ybaas-0.0.11 (crate (name "ybaas") (vers "0.0.11") (deps (list (crate-dep (name "tokio") (req "^1.16") (features (quote ("macros" "rt-multi-thread"))) (default-features #t) (kind 0)) (crate-dep (name "warp") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "yubibomb") (req "^0.2") (default-features #t) (kind 0)))) (hash "1fjk18y5maz34q38dyi33qb8sq4v9xgmjkilfcz3pi7yd7xaykpc")))

(define-public crate-ybaas-0.0.12 (crate (name "ybaas") (vers "0.0.12") (deps (list (crate-dep (name "tokio") (req "^1.16") (features (quote ("macros" "rt-multi-thread"))) (default-features #t) (kind 0)) (crate-dep (name "warp") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "yubibomb") (req "^0.2") (default-features #t) (kind 0)))) (hash "05ndwgpl2v9vilnk4dyrpsff07q8ha1b3r2f3ln7aj46xrj1hkmr")))

(define-public crate-ybaas-0.0.13 (crate (name "ybaas") (vers "0.0.13") (deps (list (crate-dep (name "tokio") (req "^1.19") (features (quote ("macros" "rt-multi-thread"))) (default-features #t) (kind 0)) (crate-dep (name "warp") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "yubibomb") (req "^0.2") (default-features #t) (kind 0)))) (hash "1svhq1n1xc3spbahy09l26dcvnjkmy5qyrnq8lp4iwrj6k810rsh")))

(define-public crate-ybaas-0.0.14 (crate (name "ybaas") (vers "0.0.14") (deps (list (crate-dep (name "tokio") (req "^1.21") (features (quote ("macros" "rt-multi-thread"))) (default-features #t) (kind 0)) (crate-dep (name "warp") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "yubibomb") (req "^0.2") (default-features #t) (kind 0)))) (hash "1qvsbw19dmgw4y2y7ipf641zblmpn859gaa8r4bi0zl290crgwz1")))

(define-public crate-ybaas-0.0.15 (crate (name "ybaas") (vers "0.0.15") (deps (list (crate-dep (name "tokio") (req "^1.24") (features (quote ("macros" "rt-multi-thread"))) (default-features #t) (kind 0)) (crate-dep (name "warp") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "yubibomb") (req "^0.2") (default-features #t) (kind 0)))) (hash "1znaf3pvyc1l0n5my66zv2wahgq2sfshrg7r4gbibpzc8sj2mvz5")))

(define-public crate-ybaas-0.0.16 (crate (name "ybaas") (vers "0.0.16") (deps (list (crate-dep (name "tokio") (req "^1.25") (features (quote ("macros" "rt-multi-thread"))) (default-features #t) (kind 0)) (crate-dep (name "warp") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "yubibomb") (req "^0.2") (default-features #t) (kind 0)))) (hash "0cd06nrr8c987vizz2ck4djv9s4cjz28h5mh4k72mvvc1zizydkk")))

(define-public crate-ybaas-0.0.17 (crate (name "ybaas") (vers "0.0.17") (deps (list (crate-dep (name "tokio") (req "^1.32") (features (quote ("macros" "rt-multi-thread"))) (default-features #t) (kind 0)) (crate-dep (name "warp") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "yubibomb") (req "^0.2") (default-features #t) (kind 0)))) (hash "0s3h8pwmim8z6q9a241n814kia5yrrbzxp2zh8119457v9sl465k") (rust-version "1.66")))

