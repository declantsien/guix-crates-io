(define-module (crates-io y2 hc) #:use-module (crates-io))

(define-public crate-y2hcl-0.1 (crate (name "y2hcl") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^4.2.7") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.162") (default-features #t) (kind 0)) (crate-dep (name "serde_yaml") (req "^0.9.21") (default-features #t) (kind 0)))) (hash "19wkfy92cyqlb1p5vsvf729kzayzhn5abf6z359zc6bqq8dw5pms")))

