(define-module (crates-io um l_) #:use-module (crates-io))

(define-public crate-uml_parser-0.1 (crate (name "uml_parser") (vers "0.1.0") (deps (list (crate-dep (name "bodyparser") (req "^0.7.0") (default-features #t) (kind 0)) (crate-dep (name "iron") (req "^0.5.1") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.3.6") (default-features #t) (kind 0)) (crate-dep (name "nom") (req "^2.0.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^0.1.77") (default-features #t) (kind 0)))) (hash "07ydsab93db36b4f5ip89k460hijqllvj116p83rqxdl9x55cpy8")))

(define-public crate-uml_parser-0.1 (crate (name "uml_parser") (vers "0.1.1") (deps (list (crate-dep (name "log") (req "^0.3.6") (default-features #t) (kind 0)) (crate-dep (name "nom") (req "^2.0.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^0.1.77") (default-features #t) (kind 0)))) (hash "1k2wr3k32g1qqg9blra7sbc2ckmqzcgmay9z95vqhpxds44m3ifc")))

