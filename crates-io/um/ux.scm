(define-module (crates-io um ux) #:use-module (crates-io))

(define-public crate-umux-1 (crate (name "umux") (vers "1.0.0") (deps (list (crate-dep (name "clippy") (req "^0.0.95") (default-features #t) (kind 0)) (crate-dep (name "error-chain") (req "^0.5.0") (default-features #t) (kind 0)) (crate-dep (name "ncurses") (req "^5.83.0") (default-features #t) (kind 0)) (crate-dep (name "nix") (req "^0.7.0") (default-features #t) (kind 0)))) (hash "187v61623jbb5k6fcih9qj5la8zna5paymm2anyhi1wnjrl2faw8")))

