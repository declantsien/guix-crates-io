(define-module (crates-io um as) #:use-module (crates-io))

(define-public crate-umash-0.1 (crate (name "umash") (vers "0.1.2") (deps (list (crate-dep (name "lazy_static") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "umash-sys") (req "^0.1.2") (default-features #t) (kind 0)))) (hash "0b1s96dfk4ypbf0cdnllh92xisddmawhk0z7ybcavclii9s8kh98")))

(define-public crate-umash-0.2 (crate (name "umash") (vers "0.2.0") (deps (list (crate-dep (name "lazy_static") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "umash-sys") (req "^0.2.0") (default-features #t) (kind 0)))) (hash "0ij6cm1a7v95sv6wfzs38cpbmrybq16d2dzl02j2w2yv2vfpji14")))

(define-public crate-umash-0.3 (crate (name "umash") (vers "0.3.0") (deps (list (crate-dep (name "getrandom") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "umash-sys") (req "^0.2") (default-features #t) (kind 0)))) (hash "13530a881ydp7pprg7f3lkqnhy7wg2523xyd4b4g47zz4dqnscbi")))

(define-public crate-umash-0.4 (crate (name "umash") (vers "0.4.0") (deps (list (crate-dep (name "getrandom") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "umash-sys") (req "^0.2") (default-features #t) (kind 0)))) (hash "09wmlpikc990b45wicikg67vk5vcik4b68ivijszgk65pd0slhgi")))

(define-public crate-umash-0.4 (crate (name "umash") (vers "0.4.1") (deps (list (crate-dep (name "getrandom") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "umash-sys") (req "^0.2") (default-features #t) (kind 0)))) (hash "02n6rn86hi0zqr67rdgi9gkamkwxd6ymrhwmhzhvmy84275j09sm")))

(define-public crate-umash-sys-0.1 (crate (name "umash-sys") (vers "0.1.1") (deps (list (crate-dep (name "cc") (req "^1.0") (default-features #t) (kind 1)))) (hash "119a1h4cgn8mps4w6kzqs0k987vyybl3x19ani0av0bvjlxvxrni")))

(define-public crate-umash-sys-0.1 (crate (name "umash-sys") (vers "0.1.2") (deps (list (crate-dep (name "cc") (req "^1.0") (default-features #t) (kind 1)))) (hash "14xrdh2146kpmf3x87h5w2b2nrkhi227c8v3xv2gl0hxjx793fja")))

(define-public crate-umash-sys-0.2 (crate (name "umash-sys") (vers "0.2.0") (deps (list (crate-dep (name "cc") (req "^1") (default-features #t) (kind 1)))) (hash "0h1wxkf7cmql5inr601hahw7hk1k5b22hqgg33qn943gd4y4j1b5")))

(define-public crate-umask-0.1 (crate (name "umask") (vers "0.1.0") (hash "1lyb0r3s1brfl2cb66rwf6zii81ramxkjqkq2ak93z7v3r5gxpr4")))

(define-public crate-umask-0.1 (crate (name "umask") (vers "0.1.1") (hash "1qihrky1wk1mp0sj45jixiyr7i3wlr4229z2cgx32myf5y0s29xd")))

(define-public crate-umask-0.1 (crate (name "umask") (vers "0.1.2") (hash "0ra9vmlhcx9yzbhim72axn1fzbs44v0rhbh8z2j5lj7x0lgiigsc")))

(define-public crate-umask-0.1 (crate (name "umask") (vers "0.1.3") (hash "0cqzandlz6m2l9mc7wldip0qf834728m0bibnmiwmp5rl2lzzx9q")))

(define-public crate-umask-0.1 (crate (name "umask") (vers "0.1.4") (hash "15bb6fjmqzdrri99yrv4ap1gm5h5dbcvlzxmwvs0fzmfkps1q8z8")))

(define-public crate-umask-0.1 (crate (name "umask") (vers "0.1.5") (hash "025ysm69rylr8kv776csng1n0ab7lia64bh3204p0zcri0ny5mlg")))

(define-public crate-umask-0.1 (crate (name "umask") (vers "0.1.6") (hash "1av94zv88r4wqc4lf2036f16sf4dffy2mxilis2m3c1hj50rbfx0")))

(define-public crate-umask-0.1 (crate (name "umask") (vers "0.1.7") (hash "1bwjiizlffd8x0w775mr06jqsw4jf8lnmrfjmwxx0i9df6gcm7x2")))

(define-public crate-umask-0.1 (crate (name "umask") (vers "0.1.8") (hash "0rvhf2rxlh4la6m5fzqv6cx9qjpcgwx5294ivc8dbbaaxdd2xv6k")))

(define-public crate-umask-1 (crate (name "umask") (vers "1.0.0") (hash "0ipyyv82lpy5xpqzmq3ra0d61vsd3bfh6b06c9w8zln41vvznblq")))

(define-public crate-umask-1 (crate (name "umask") (vers "1.0.1") (hash "1f5nkxmbq5dj0qwzx1fqzhfxlksscg5l6p11xdivb4s1965g7czg")))

(define-public crate-umask-2 (crate (name "umask") (vers "2.0.0") (hash "0rzswv5956m05kvjyjki6ssjw5gks6bzqw39rmx2qcgvmmpc3c26")))

(define-public crate-umask-2 (crate (name "umask") (vers "2.1.0") (deps (list (crate-dep (name "thiserror") (req "^1.0.40") (default-features #t) (kind 0)))) (hash "071xszsd6znk0ik11pxl7mwhf07clsiq3qpzw1ac0dcyak14d6pc")))

