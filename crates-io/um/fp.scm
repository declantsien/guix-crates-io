(define-module (crates-io um fp) #:use-module (crates-io))

(define-public crate-umfpack-rs-0.0.1 (crate (name "umfpack-rs") (vers "0.0.1") (deps (list (crate-dep (name "approx") (req "^0.5.1") (default-features #t) (kind 0)) (crate-dep (name "array-init") (req "^2.1.0") (default-features #t) (kind 0)) (crate-dep (name "cc") (req "^1.0.83") (features (quote ("parallel"))) (optional #t) (default-features #t) (kind 1)) (crate-dep (name "git2") (req "^0.18.0") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2.147") (default-features #t) (kind 0)) (crate-dep (name "num-complex") (req "^0.4.4") (default-features #t) (kind 0)))) (hash "1kxprpqycgvpz241b3yngy6vaz78fwzrsdgvnmgpbh3zd4mcvlz9") (features (quote (("default" "cc"))))))

