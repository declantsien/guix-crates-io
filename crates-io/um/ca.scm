(define-module (crates-io um ca) #:use-module (crates-io))

(define-public crate-umcan-0.1 (crate (name "umcan") (vers "0.1.0") (deps (list (crate-dep (name "anyhow") (req "^1.0.72") (default-features #t) (kind 2)) (crate-dep (name "embedded-can") (req "^0.4.1") (default-features #t) (kind 0)) (crate-dep (name "socketcan") (req "^2.0.0") (default-features #t) (kind 2)))) (hash "1ymjhynz0knxmqmcx1qj9k39hsxshmfqsr691s2ccdyddag3jkix")))

(define-public crate-umcan-0.1 (crate (name "umcan") (vers "0.1.1") (deps (list (crate-dep (name "anyhow") (req "^1.0.72") (default-features #t) (kind 2)) (crate-dep (name "binrw") (req "^0.11.2") (default-features #t) (kind 0)) (crate-dep (name "embedded-can") (req "^0.4.1") (default-features #t) (kind 0)) (crate-dep (name "socketcan") (req "^2.0.0") (default-features #t) (kind 2)))) (hash "0wpmsbgpln9rsz0449ds9yigiyxj26il11p4s9j73k1cx53y22nk")))

(define-public crate-umcan-0.2 (crate (name "umcan") (vers "0.2.0") (deps (list (crate-dep (name "anyhow") (req "^1.0.72") (default-features #t) (kind 2)) (crate-dep (name "binrw") (req "^0.11.2") (default-features #t) (kind 0)) (crate-dep (name "embedded-can") (req "^0.4.1") (default-features #t) (kind 0)) (crate-dep (name "socketcan") (req "^2.0.0") (default-features #t) (kind 2)))) (hash "00xahydb30szpy4s528nrci5ip48jbf2yd0k2kl20ywqx8xcn1py")))

