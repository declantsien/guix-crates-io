(define-module (crates-io um ik) #:use-module (crates-io))

(define-public crate-umiko-0.1 (crate (name "umiko") (vers "0.1.0") (deps (list (crate-dep (name "bitflags") (req "^1.2.1") (default-features #t) (kind 0)) (crate-dep (name "winapi") (req "^0.3.9") (features (quote ("winuser"))) (default-features #t) (target "cfg(windows)") (kind 0)))) (hash "1wlxwahny8vgynpyzkzqbk3k9nng7mbjkysp2mml417nv8qabrrd") (features (quote (("keys") ("hotkeys") ("default" "hotkeys" "keys"))))))

(define-public crate-umiko-0.1 (crate (name "umiko") (vers "0.1.1") (deps (list (crate-dep (name "bitflags") (req "^1.2.1") (default-features #t) (kind 0)) (crate-dep (name "winapi") (req "^0.3.9") (features (quote ("winuser"))) (default-features #t) (target "cfg(windows)") (kind 0)))) (hash "1fx84msp1dld72qwa51ai67w4378bw42sz733ifgg81mcndn0h7a") (features (quote (("keys") ("hotkeys") ("default" "hotkeys" "keys"))))))

(define-public crate-umiko-0.1 (crate (name "umiko") (vers "0.1.2") (deps (list (crate-dep (name "bitflags") (req "^1.2.1") (default-features #t) (kind 0)) (crate-dep (name "winapi") (req "^0.3.9") (features (quote ("winuser"))) (default-features #t) (target "cfg(windows)") (kind 0)))) (hash "1iqda48pq2d9zc8p5l3213l7ar8y65srvs5mm1cys521dxdpmxlz") (features (quote (("keys") ("hotkeys") ("default" "hotkeys" "keys"))))))

(define-public crate-umiko-0.1 (crate (name "umiko") (vers "0.1.3") (deps (list (crate-dep (name "bitflags") (req "^1.2.1") (default-features #t) (kind 0)) (crate-dep (name "winapi") (req "^0.3.9") (features (quote ("winuser"))) (default-features #t) (target "cfg(windows)") (kind 0)))) (hash "0ds16xrh7qx4xzy1iijjp4770ll18gf2hq560csf0qfzlkvyhbcb") (features (quote (("keys") ("hotkeys") ("default" "hotkeys" "keys"))))))

(define-public crate-umiko-0.1 (crate (name "umiko") (vers "0.1.4") (deps (list (crate-dep (name "bitflags") (req "^1.2.1") (default-features #t) (kind 0)) (crate-dep (name "winapi") (req "^0.3.9") (features (quote ("winuser"))) (default-features #t) (target "cfg(windows)") (kind 0)))) (hash "02p4d1vlm1dxhyqcncq98hf4xl97z5qn9ixxxdik4c8n5hvcsvg4") (features (quote (("keys") ("hotkeys") ("default" "hotkeys" "keys"))))))

