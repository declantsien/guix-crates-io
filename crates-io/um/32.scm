(define-module (crates-io um #{32}#) #:use-module (crates-io))

(define-public crate-um32-1 (crate (name "um32") (vers "1.0.0") (deps (list (crate-dep (name "byteorder") (req "1.0.*") (default-features #t) (kind 0)))) (hash "0n2d60f77l6amyx6bn1s05xvh8h8ciwqpzlrvq6jxj2qw77dz7l3") (yanked #t)))

(define-public crate-um32-1 (crate (name "um32") (vers "1.0.1") (deps (list (crate-dep (name "byteorder") (req "1.0.*") (default-features #t) (kind 0)))) (hash "0d0vfpgghjwmm8n2x52qwvwy81xpbbnbk25nf3gmlv8dpyasvirl") (yanked #t)))

(define-public crate-um32-1 (crate (name "um32") (vers "1.0.2") (deps (list (crate-dep (name "byteorder") (req "1.0.*") (default-features #t) (kind 0)))) (hash "1vcs80cs1b2l5dmj66fbi4hfaw32dr9dwz7la6kr4xr33gazm3cd") (yanked #t)))

