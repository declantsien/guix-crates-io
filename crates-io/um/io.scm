(define-module (crates-io um io) #:use-module (crates-io))

(define-public crate-umio-0.1 (crate (name "umio") (vers "0.1.0") (deps (list (crate-dep (name "mio") (req "^0.5.0") (default-features #t) (kind 0)))) (hash "0fqc8raw13mvgrr38gxvy6p9bwl6zzcvgklrszng5bfy8nyg5287") (features (quote (("unstable"))))))

(define-public crate-umio-0.2 (crate (name "umio") (vers "0.2.0") (deps (list (crate-dep (name "mio") (req "^0.5.0") (default-features #t) (kind 0)))) (hash "0xq4qvid5hs43dbc4w2z82ird8gjbbsd2adrfi3ps8m5q1icivj3") (features (quote (("unstable"))))))

(define-public crate-umio-0.3 (crate (name "umio") (vers "0.3.0") (deps (list (crate-dep (name "mio") (req "^0.5.0") (default-features #t) (kind 0)))) (hash "1db78yddda82mp8n2mb517nycqpb9bzzaihis1gbyg8qwysy9ql6") (features (quote (("unstable"))))))

