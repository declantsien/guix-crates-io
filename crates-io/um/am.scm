(define-module (crates-io um am) #:use-module (crates-io))

(define-public crate-umami-0.1 (crate (name "umami") (vers "0.1.0") (hash "0hn15vzq1p227clpfsp2iwaql180hz85lbh3pk1dn9v8df9xc251")))

(define-public crate-umami-0.1 (crate (name "umami") (vers "0.1.1") (hash "1lw25ayny9sz29ga41af9vv4x30yjhfxfah74nj7j4dpc6cz51a7")))

(define-public crate-umami_metrics-0.1 (crate (name "umami_metrics") (vers "0.1.0") (deps (list (crate-dep (name "anyhow") (req "^1.0.58") (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.11.11") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.140") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.82") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1") (features (quote ("full"))) (default-features #t) (kind 2)) (crate-dep (name "version-sync") (req "^0.9.4") (default-features #t) (kind 2)))) (hash "0fb8pjz2n2igw0ynx137s45cf9h1dvj6y1zvr8r4wl5h3d2yrjfb")))

