(define-module (crates-io um la) #:use-module (crates-io))

(define-public crate-umlauts-0.1 (crate (name "umlauts") (vers "0.1.0") (hash "0pqi8s316c6ajqzdidkpkiy9k2aj9anwfkb8bqjkk6ygjhs0q9mg")))

(define-public crate-umlauts-0.2 (crate (name "umlauts") (vers "0.2.0-alpha.1") (deps (list (crate-dep (name "memchr") (req "^2.3") (default-features #t) (kind 0)))) (hash "1ahr87xgykkgsmbjhqp22ha7y3isld5q6j6qsmw53d97l3fyg73j") (features (quote (("unsafe"))))))

(define-public crate-umlauts-0.2 (crate (name "umlauts") (vers "0.2.0-alpha.2") (deps (list (crate-dep (name "memchr") (req "^2.3") (default-features #t) (kind 0)))) (hash "05zqlr9d03jrsw8b5czn124sw0j65v7sdqj3f5k7lh274lrnd1np") (features (quote (("unsafe"))))))

(define-public crate-umlauts-0.1 (crate (name "umlauts") (vers "0.1.1") (hash "04mf8napvx37ghf1a69i561l16gl0j4lnsvijj9fm9jdrd2jry2n")))

(define-public crate-umlauts-0.2 (crate (name "umlauts") (vers "0.2.0-alpha.3") (deps (list (crate-dep (name "memchr") (req "^2.3") (default-features #t) (kind 0)))) (hash "1qf6bn4syyfk74w142fp623bhdw5dajz7slskghhbmj6wdg5pglf") (features (quote (("unsafe"))))))

