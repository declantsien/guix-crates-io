(define-module (crates-io xg co) #:use-module (crates-io))

(define-public crate-xgcode-0.1 (crate (name "xgcode") (vers "0.1.0") (deps (list (crate-dep (name "byteorder") (req "^1.4.3") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.37") (default-features #t) (kind 0)))) (hash "0rcq7ax8n7pcvpw1q2l15ghl7piqfi3368bp67zrajqbbd70x3yn")))

