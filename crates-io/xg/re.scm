(define-module (crates-io xg re) #:use-module (crates-io))

(define-public crate-xgrep-0.1 (crate (name "xgrep") (vers "0.1.0") (hash "17csi3cy22qxzcs4g4ji9x9p29qlf7jycnj6ll94k0006vcb7x75")))

(define-public crate-xgrepx-0.1 (crate (name "xgrepx") (vers "0.1.0") (hash "1d7jvhyhw4mrym29yn42gcgyd52vgl7rps1hgx7ccfqn7a857ph3")))

