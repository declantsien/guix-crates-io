(define-module (crates-io tp s6) #:use-module (crates-io))

(define-public crate-tps6507x-0.1 (crate (name "tps6507x") (vers "0.1.0-alpha.0") (deps (list (crate-dep (name "bitfield") (req "^0.13") (default-features #t) (kind 0)) (crate-dep (name "embedded-hal") (req "^0.2.2") (features (quote ("unproven"))) (default-features #t) (kind 0)) (crate-dep (name "embedded-hal-mock") (req "^0.7") (default-features #t) (kind 2)) (crate-dep (name "nb") (req "^1") (default-features #t) (kind 0)))) (hash "00drdw1sdwmxarfvpb2lw2ahmr988nms2hw6wnq2b9m35rpyz2fg") (yanked #t)))

(define-public crate-tps6507x-0.1 (crate (name "tps6507x") (vers "0.1.0-alpha.1") (deps (list (crate-dep (name "bitfield") (req "^0.13") (default-features #t) (kind 0)) (crate-dep (name "embedded-hal") (req "^0.2.2") (features (quote ("unproven"))) (default-features #t) (kind 0)) (crate-dep (name "embedded-hal-mock") (req "^0.7") (default-features #t) (kind 2)) (crate-dep (name "nb") (req "^1") (default-features #t) (kind 0)))) (hash "0kfvfv38w2k6x9hgfl3vdyyn5c5sqz06za2nv9vylbphl93a3cdz") (yanked #t)))

(define-public crate-tps6507x-0.1 (crate (name "tps6507x") (vers "0.1.0") (deps (list (crate-dep (name "bitfield") (req "^0.13") (default-features #t) (kind 0)) (crate-dep (name "embedded-hal") (req "^0.2.2") (features (quote ("unproven"))) (default-features #t) (kind 0)) (crate-dep (name "embedded-hal-mock") (req "^0.7") (default-features #t) (kind 2)) (crate-dep (name "nb") (req "^0.1.2") (default-features #t) (kind 0)))) (hash "1bassda28aw0h4wz1xkpzsjfayd6py8ssnn1sdrjwzmpjrlalapi")))

