(define-module (crates-io tp nt) #:use-module (crates-io))

(define-public crate-tpntree-0.1 (crate (name "tpntree") (vers "0.1.0") (deps (list (crate-dep (name "bitvec") (req "^0.22") (default-features #t) (kind 0)) (crate-dep (name "nalgebra") (req "^0.28") (optional #t) (default-features #t) (kind 0)))) (hash "0ybgppn12lh9l6mwn4qqs099nh9hrqkq05hqj3fjglavbbdlq17v") (features (quote (("default" "nalgebra"))))))

(define-public crate-tpntree-0.2 (crate (name "tpntree") (vers "0.2.0") (deps (list (crate-dep (name "bitvec") (req "^0.22") (default-features #t) (kind 0)) (crate-dep (name "nalgebra") (req "^0.28") (optional #t) (default-features #t) (kind 0)))) (hash "0290cisbgxrqsr5xgfxivz0svzcgfkywnj6khk8nl66j3qzd6jfc") (features (quote (("default" "nalgebra"))))))

(define-public crate-tpntree-0.3 (crate (name "tpntree") (vers "0.3.0") (deps (list (crate-dep (name "bitvec") (req "^0.22") (default-features #t) (kind 0)) (crate-dep (name "nalgebra") (req "^0.28") (optional #t) (default-features #t) (kind 0)))) (hash "1cqhgswqhczwwq0kzg5l98v5phaxy0fpim9gih87mszkxgckgzib") (features (quote (("default" "nalgebra"))))))

(define-public crate-tpntree-0.4 (crate (name "tpntree") (vers "0.4.0") (deps (list (crate-dep (name "bitvec") (req "^0.22") (default-features #t) (kind 0)) (crate-dep (name "nalgebra") (req "^0.28") (optional #t) (default-features #t) (kind 0)))) (hash "130if36hcmzaazcshhr602lw7y66n34ilnxbp4zlkagf1678jdcj") (features (quote (("default" "nalgebra"))))))

(define-public crate-tpntree-0.5 (crate (name "tpntree") (vers "0.5.0") (deps (list (crate-dep (name "bitvec") (req "^0.22") (default-features #t) (kind 0)) (crate-dep (name "nalgebra") (req "^0.28") (optional #t) (default-features #t) (kind 0)))) (hash "01p57h397kylnfgy53fz4vh2mc317b8afk843ds1v115im12mygx") (features (quote (("default" "nalgebra"))))))

(define-public crate-tpntree-0.5 (crate (name "tpntree") (vers "0.5.1") (deps (list (crate-dep (name "bitvec") (req "^0.22") (default-features #t) (kind 0)) (crate-dep (name "nalgebra") (req "^0.28") (optional #t) (default-features #t) (kind 0)))) (hash "1pj4j99c8lz7jniz0njypyaz4y2dz603jlf9wr5jm2dcb7i4lsns") (features (quote (("default" "nalgebra"))))))

(define-public crate-tpntree-0.5 (crate (name "tpntree") (vers "0.5.2") (deps (list (crate-dep (name "bitvec") (req "^1.0.1") (default-features #t) (kind 0)) (crate-dep (name "nalgebra") (req "^0.28") (optional #t) (default-features #t) (kind 0)))) (hash "1aks6lfhx7gjgjchdrkbsimmrx49zjdyyq6q2dmhyy3m3ll2zbrk") (features (quote (("default" "nalgebra"))))))

