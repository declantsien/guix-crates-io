(define-module (crates-io tp ng) #:use-module (crates-io))

(define-public crate-tpng-0.1 (crate (name "tpng") (vers "0.1.0") (deps (list (crate-dep (name "colored") (req "^2") (default-features #t) (kind 0)) (crate-dep (name "png") (req "^0.17.5") (default-features #t) (kind 0)))) (hash "1r5v9s52a2ip283jylkxpwzridwjdgd08z6hpg4hsfk26a9b5mj5")))

(define-public crate-tpng-0.1 (crate (name "tpng") (vers "0.1.1") (deps (list (crate-dep (name "colored") (req "^2") (default-features #t) (kind 0)) (crate-dep (name "png") (req "^0.17.5") (default-features #t) (kind 0)))) (hash "1ssliaw2bpfmmmzv02qbdlirgkfq7zx1dc8712hdc25b737605x7")))

(define-public crate-tpng-0.1 (crate (name "tpng") (vers "0.1.2") (deps (list (crate-dep (name "colored") (req "^2") (default-features #t) (kind 0)) (crate-dep (name "png") (req "^0.17.5") (default-features #t) (kind 0)))) (hash "1jh7adv2mvkqs1dyb9p3hb938kgxszbwg16kahhaws0jhf66v8cv")))

(define-public crate-tpng-0.1 (crate (name "tpng") (vers "0.1.3") (deps (list (crate-dep (name "colored") (req "^2") (default-features #t) (kind 0)) (crate-dep (name "png") (req "^0.17.5") (default-features #t) (kind 0)))) (hash "1mmkvv7zdafcvcr1sagh05w1i56pnys4k13h51nwjs2pxk3dz5hi")))

(define-public crate-tpng-0.1 (crate (name "tpng") (vers "0.1.4") (deps (list (crate-dep (name "colored") (req "^2") (default-features #t) (kind 0)) (crate-dep (name "png") (req "^0.17.5") (default-features #t) (kind 0)))) (hash "08h58jm52vb7z0qyxm7p40wm2makkrik5jcq1f3k129xz5cgwl6r")))

(define-public crate-tpng-0.1 (crate (name "tpng") (vers "0.1.5") (deps (list (crate-dep (name "colored") (req "^2") (default-features #t) (kind 0)) (crate-dep (name "png") (req "^0.17.5") (default-features #t) (kind 0)))) (hash "07wpnsljaf5cghfhjhsf2g8gv7xn32gdkh8x1blgja2qvyh4194i")))

(define-public crate-tpng-0.1 (crate (name "tpng") (vers "0.1.6") (deps (list (crate-dep (name "colored") (req "^2") (default-features #t) (kind 0)) (crate-dep (name "png") (req "^0.17.5") (default-features #t) (kind 0)))) (hash "0h879gzikn1qa4b29fldiixb004ygvpiing3ns0p50cw2mv9bzld") (rust-version "1.48")))

