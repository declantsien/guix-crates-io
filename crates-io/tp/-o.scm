(define-module (crates-io tp -o) #:use-module (crates-io))

(define-public crate-tp-offchain-2 (crate (name "tp-offchain") (vers "2.0.1") (deps (list (crate-dep (name "tet-core") (req "^2.0.1") (kind 0)) (crate-dep (name "tp-api") (req "^2.0.1") (kind 0)) (crate-dep (name "tp-runtime") (req "^2.0.1") (kind 0)) (crate-dep (name "tp-state-machine") (req "^0.8.1") (kind 2)))) (hash "13rvpkrsalycwr40zjr8b50k8bzpk7zj570nk2ygavlnpf3ph4w8") (features (quote (("std" "tet-core/std" "tp-api/std" "tp-runtime/std") ("default" "std"))))))

(define-public crate-tp-offchain-2 (crate (name "tp-offchain") (vers "2.0.2") (deps (list (crate-dep (name "tet-core") (req "^2.0.2") (kind 0)) (crate-dep (name "tp-api") (req "^2.0.2") (kind 0)) (crate-dep (name "tp-runtime") (req "^2.0.2") (kind 0)) (crate-dep (name "tp-state-machine") (req "^0.8.2") (kind 2)))) (hash "0wmgmasvw5qfww6ahffvhryhjimsa9jx454y21y2h6nk20qns948") (features (quote (("std" "tet-core/std" "tp-api/std" "tp-runtime/std") ("default" "std"))))))

