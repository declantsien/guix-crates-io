(define-module (crates-io tp a2) #:use-module (crates-io))

(define-public crate-tpa2016d2-0.1 (crate (name "tpa2016d2") (vers "0.1.0") (deps (list (crate-dep (name "embedded-hal") (req "^0.2.2") (default-features #t) (kind 0)))) (hash "02pzc0hqb0n5rhifl04yasgvg5z7yyvzmysk0zljw17azz03yq43")))

(define-public crate-tpa2016d2-0.1 (crate (name "tpa2016d2") (vers "0.1.1") (deps (list (crate-dep (name "embedded-hal") (req "^0.2.2") (default-features #t) (kind 0)))) (hash "1c1ip9f73ddcxwf5xfhag6z5ahqzzdarjyjndh4kbcwj03h3zzfj")))

(define-public crate-tpa2016d2-0.2 (crate (name "tpa2016d2") (vers "0.2.0") (deps (list (crate-dep (name "embedded-hal") (req "^0.2.2") (default-features #t) (kind 0)))) (hash "08dqhbkwl1v8yd6ynmxxkz7jaq650h34f1dc8ln2l6x4lw4bpwj1")))

