(define-module (crates-io tp om) #:use-module (crates-io))

(define-public crate-tpom-0.1 (crate (name "tpom") (vers "0.1.0") (deps (list (crate-dep (name "goblin") (req "^0.6.0") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2.137") (default-features #t) (kind 0)) (crate-dep (name "serial_test") (req "^0.9.0") (default-features #t) (kind 2)))) (hash "06y1q2937widds64mfngvbhz308pbc5bkz9d2qcsw84pxrkvqack")))

