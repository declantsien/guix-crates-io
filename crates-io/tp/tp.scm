(define-module (crates-io tp tp) #:use-module (crates-io))

(define-public crate-tptp-0.1 (crate (name "tptp") (vers "0.1.0") (hash "0n5hjg88yxw8d5y3sz32hdn7r32ah6gmjcing0fv9z9gsadfcvz7")))

(define-public crate-tptp-0.1 (crate (name "tptp") (vers "0.1.1") (hash "00g91bsxan2jqsmh7a52ra2f7ipy9vkhp60p3xkkjn1rsjjdm6w3")))

(define-public crate-tptp-0.2 (crate (name "tptp") (vers "0.2.0") (hash "0r3gwf69i0wzhr77nzvnffcscbbs42q49plggzbwfy45dzyvikl4")))

(define-public crate-tptp-0.3 (crate (name "tptp") (vers "0.3.0") (hash "0l4s6560kf6p83379pnkwl5fpl6pkrnb787nsmnwdb4yygz88yfj")))

(define-public crate-tptp-0.3 (crate (name "tptp") (vers "0.3.1") (hash "1cm5xz0g0j2araa47aijz73b3a1vhwc50ah6q68lk6z11vdx0lbq")))

(define-public crate-tptp-0.3 (crate (name "tptp") (vers "0.3.2") (hash "05y2p378safb4p9hripynl1l6nqm2h4003k719xriscymgkka4ll") (yanked #t)))

(define-public crate-tptp-0.3 (crate (name "tptp") (vers "0.3.3") (hash "1f66aklm8y7yivmhyckjdlflx40498h7ig423bmpwvwvizfzxdqf")))

(define-public crate-tptp-0.3 (crate (name "tptp") (vers "0.3.4") (hash "14kwlw39nz0zn3cz3fsiqj14h3vx55a794jv1fnz0cwf4gckdl3x")))

(define-public crate-tptp-0.4 (crate (name "tptp") (vers "0.4.0") (hash "181zg20pda07n5id90zdv3kj71wdn8643dkc3czvf4b5435y1rfi")))

(define-public crate-tptp-0.5 (crate (name "tptp") (vers "0.5.0") (hash "19xklvvsm32745l9wzx2gs2dxvhm15m7xamgqdap035ycjyccf1p")))

(define-public crate-tptp-0.5 (crate (name "tptp") (vers "0.5.1") (hash "1h41r7ix6rsyl59jaavf81nd8fxgfpnizcim9807kxvjxs55pc9z")))

(define-public crate-tptp-0.6 (crate (name "tptp") (vers "0.6.0") (hash "1qr3ykw56anhlxxr26mag9f5zpqgrvc62gk0a51308v97l2rgh8q")))

(define-public crate-tptp-0.7 (crate (name "tptp") (vers "0.7.0") (deps (list (crate-dep (name "nom") (req "^4") (default-features #t) (kind 0)))) (hash "0qvr7k1srcg9l4cblr0wihh7rybz3yv4silwinz8idxp17amgsxg")))

(define-public crate-tptp-0.7 (crate (name "tptp") (vers "0.7.1") (deps (list (crate-dep (name "nom") (req "^4") (default-features #t) (kind 0)))) (hash "1k6wnkj3nydcx23gkf8mjnkv7az940pykql7b4zybxa3xmi7cqg8")))

(define-public crate-tptp-0.7 (crate (name "tptp") (vers "0.7.2") (deps (list (crate-dep (name "nom") (req "^4") (default-features #t) (kind 0)))) (hash "15imy9a7n5p140w86va6vzklywhfgzk828i1pvqmczwxmgp57vrq")))

(define-public crate-tptp-0.8 (crate (name "tptp") (vers "0.8.0") (deps (list (crate-dep (name "nom") (req "^4") (default-features #t) (kind 0)))) (hash "068aifgwkknzd8qxp5fhb1b5b5q3srdsdipjw0jw5ihab5hn2hfy")))

(define-public crate-tptp-0.8 (crate (name "tptp") (vers "0.8.1") (deps (list (crate-dep (name "nom") (req "^4") (default-features #t) (kind 0)))) (hash "12k3dv21lz0awniyg6n4rcn5mbrrbfbic5za9x36d7lc0s2pyrq7")))

(define-public crate-tptp-0.9 (crate (name "tptp") (vers "0.9.0") (deps (list (crate-dep (name "nom") (req "^4") (default-features #t) (kind 0)))) (hash "184znl9yr7a1k453im561n4pmxh24p0yx4pbxy764i7aw0ky66bn")))

(define-public crate-tptp-0.9 (crate (name "tptp") (vers "0.9.1") (deps (list (crate-dep (name "nom") (req "^4") (default-features #t) (kind 0)))) (hash "1n5rmypicz0ff6k004l15hk05dig5i43gp44nb457mlzpd0j9yjr")))

(define-public crate-tptp-0.10 (crate (name "tptp") (vers "0.10.0") (deps (list (crate-dep (name "insta") (req "^0.8") (default-features #t) (kind 2)) (crate-dep (name "nom") (req "^4") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)))) (hash "0p0jgqcr34fn1alb5i6fzcs881qprz4fm18ff3p3029rys5p59l3")))

(define-public crate-tptp-0.11 (crate (name "tptp") (vers "0.11.0") (deps (list (crate-dep (name "insta") (req "^0.8") (default-features #t) (kind 2)) (crate-dep (name "nom") (req "^5") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)))) (hash "1b0kf1h5li1i3ka9275bd3kgbkxfbs49kx39vqcsbh1a791zpq4k")))

(define-public crate-tptp-0.12 (crate (name "tptp") (vers "0.12.0") (deps (list (crate-dep (name "insta") (req "^0.12") (default-features #t) (kind 2)) (crate-dep (name "nom") (req "^5.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)))) (hash "1m74fbdsmhllbbkbcc37n849kdjlvgzc062l2jswmmm96sil2imx")))

(define-public crate-tptp-0.13 (crate (name "tptp") (vers "0.13.0") (deps (list (crate-dep (name "insta") (req "^0.12") (default-features #t) (kind 2)) (crate-dep (name "nom") (req "^5.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)))) (hash "114ffyimi8lqbacm20w14lxf8jjamc8vp6yw8v341vry7w0hfwnp")))

(define-public crate-tptp-0.14 (crate (name "tptp") (vers "0.14.0") (deps (list (crate-dep (name "derive_more") (req "^0.99") (features (quote ("as_ref" "from" "display"))) (kind 0)) (crate-dep (name "insta") (req "^0.12") (default-features #t) (kind 2)) (crate-dep (name "nom") (req "^5.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)))) (hash "0mn9x0jz04whfkxldm2v2ny1cra44aw9pa6ffsxa4yxbylypw82c")))

(define-public crate-tptp-0.15 (crate (name "tptp") (vers "0.15.0") (deps (list (crate-dep (name "derive_more") (req "^0.99") (features (quote ("as_ref" "from" "display"))) (kind 0)) (crate-dep (name "insta") (req "^0.12") (default-features #t) (kind 2)) (crate-dep (name "nom") (req "^5.1") (features (quote ("lexical"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)))) (hash "1ncfl2rsqs9v04xvcglg51bxwzkinczlq79q6a4pss29v2f4hci3")))

(define-public crate-tptp-0.16 (crate (name "tptp") (vers "0.16.0") (deps (list (crate-dep (name "derive_more") (req "^0.99") (features (quote ("as_ref" "from" "display"))) (kind 0)) (crate-dep (name "insta") (req "^0.12") (default-features #t) (kind 2)) (crate-dep (name "nom") (req "^5.1") (features (quote ("lexical"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)))) (hash "1b8y7c5vx6w7ama601iyswd3lnvnwam2lvz7r2rcq7qmwr1d61gp")))

(define-public crate-tptp-0.17 (crate (name "tptp") (vers "0.17.0") (deps (list (crate-dep (name "derive_more") (req "^0.99") (features (quote ("as_ref" "from" "display"))) (kind 0)) (crate-dep (name "insta") (req "^0.12") (default-features #t) (kind 2)) (crate-dep (name "nom") (req "^5.1") (features (quote ("lexical"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 2)))) (hash "194j43mvrk4pij12hhasxrmzjnn1wv4srg8nna5imskhvbvirqqa")))

(define-public crate-tptp-0.18 (crate (name "tptp") (vers "0.18.0") (deps (list (crate-dep (name "derive_more") (req "^0.99") (features (quote ("as_ref" "from" "display"))) (kind 0)) (crate-dep (name "insta") (req "^0.12") (default-features #t) (kind 2)) (crate-dep (name "nom") (req "^5.1") (features (quote ("lexical"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)))) (hash "0vb2jk97qap2jhia78vg650wyhvj0dqp8dib9z2pnm6hyrpkibx8")))

(define-public crate-tptp-0.18 (crate (name "tptp") (vers "0.18.1") (deps (list (crate-dep (name "derive_more") (req "^0.99") (features (quote ("as_ref" "from" "display"))) (kind 0)) (crate-dep (name "insta") (req "^0.12") (default-features #t) (kind 2)) (crate-dep (name "nom") (req "^5.1") (features (quote ("lexical"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)))) (hash "0gj9smyf772h7fx3fxbj5hfdzklsdhgabpb1qwkf5jiiv6ihrrb5")))

(define-public crate-tptp-0.19 (crate (name "tptp") (vers "0.19.0") (deps (list (crate-dep (name "derive_more") (req "^0.99") (features (quote ("as_ref" "from" "display"))) (kind 0)) (crate-dep (name "insta") (req "^0.12") (default-features #t) (kind 2)) (crate-dep (name "nom") (req "^5.1") (features (quote ("lexical"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)))) (hash "0cg7jav5342smjfd00m67mns61fr9655jkb811vqkzywjqpv8z94")))

(define-public crate-tptp-0.20 (crate (name "tptp") (vers "0.20.0") (deps (list (crate-dep (name "derive_more") (req "^0.99") (features (quote ("as_ref" "from" "display"))) (kind 0)) (crate-dep (name "insta") (req "^0.12") (default-features #t) (kind 2)) (crate-dep (name "nom") (req "^5.1") (features (quote ("lexical"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)))) (hash "062ali1wa896ni5s02m8cidvsbf1axvv46pf24y803d5s54wbswv")))

(define-public crate-tptp-0.21 (crate (name "tptp") (vers "0.21.0") (deps (list (crate-dep (name "derive_more") (req "^0.99") (features (quote ("as_ref" "from" "display"))) (kind 0)) (crate-dep (name "insta") (req "^0.12") (default-features #t) (kind 2)) (crate-dep (name "nom") (req "^5.1") (features (quote ("lexical"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)))) (hash "16k0w0d79kvq1ymcq809gsnb8r05lq1r137b7n3bh1jr9d0r83jz")))

(define-public crate-tptp-0.22 (crate (name "tptp") (vers "0.22.0") (deps (list (crate-dep (name "derive_more") (req "^0.99") (features (quote ("as_ref" "from" "display"))) (kind 0)) (crate-dep (name "insta") (req "^0.12") (default-features #t) (kind 2)) (crate-dep (name "nom") (req "^5.1") (features (quote ("lexical"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)))) (hash "0khyd5fa637frgrab3zi8xjhg986z5v46nll4pbx5cmfdb1jc2s6")))

(define-public crate-tptp-0.23 (crate (name "tptp") (vers "0.23.0") (deps (list (crate-dep (name "derive_more") (req "^0.99") (features (quote ("as_ref" "from" "display"))) (kind 0)) (crate-dep (name "insta") (req "^0.16") (default-features #t) (kind 2)) (crate-dep (name "nom") (req "^6.0.0-alpha1") (features (quote ("lexical"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)))) (hash "0d5nz6iq1gvfaym28924ap5al3p3a47kkzvjhs9mmd3ipanh79cy")))

(define-public crate-tptp-0.23 (crate (name "tptp") (vers "0.23.1") (deps (list (crate-dep (name "derive_more") (req "^0.99") (features (quote ("as_ref" "from" "display"))) (kind 0)) (crate-dep (name "insta") (req "^0.16") (default-features #t) (kind 2)) (crate-dep (name "nom") (req "^6.0.0-alpha1") (features (quote ("lexical"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)))) (hash "0ial51fh3g56kiras7v8fwg64yryj4dsnrnh387yjfpdm7qqfxb3")))

(define-public crate-tptp-0.24 (crate (name "tptp") (vers "0.24.0") (deps (list (crate-dep (name "derive_more") (req "^0.99") (features (quote ("as_ref" "from" "display"))) (kind 0)) (crate-dep (name "insta") (req "^1.0") (default-features #t) (kind 2)) (crate-dep (name "nom") (req "^6.0.0-alpha2") (features (quote ("lexical"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)))) (hash "0db0lpm64fxgfvnid1bb2l90kc3imm27751q30xsvwj85px83a6s")))

(define-public crate-tptp-0.25 (crate (name "tptp") (vers "0.25.0") (deps (list (crate-dep (name "insta") (req "^1") (default-features #t) (kind 2)) (crate-dep (name "nom") (req "^5") (features (quote ("lexical"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)))) (hash "1vsh33pxm7glvlh9yzbimnrrghzx597jwmi2flfmlgaisqcnssxa")))

(define-public crate-tptp-0.26 (crate (name "tptp") (vers "0.26.0") (deps (list (crate-dep (name "insta") (req "^1") (default-features #t) (kind 2)) (crate-dep (name "nom") (req "^5") (features (quote ("lexical"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)))) (hash "14qybsgn2p48w1kwwmbgixayabv3b34r59c7aaisvpaxz3pvnyhx")))

(define-public crate-tptp-0.27 (crate (name "tptp") (vers "0.27.0") (deps (list (crate-dep (name "insta") (req "^1") (default-features #t) (kind 2)) (crate-dep (name "nom") (req "^6") (kind 0)) (crate-dep (name "serde") (req "^1") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)))) (hash "1ckmcbmlrc4143ml4pfxyv1dbaq2fxzy808ycxnw84f52q1lnqiw")))

(define-public crate-tptp-0.28 (crate (name "tptp") (vers "0.28.0") (deps (list (crate-dep (name "insta") (req "^1") (default-features #t) (kind 2)) (crate-dep (name "nom") (req "^6") (kind 0)) (crate-dep (name "serde") (req "^1") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)))) (hash "1qbf4h7jqpkg0x1fgmvaq0gdrwmpnfns9k8rcm8qdviklrbdc1yk")))

(define-public crate-tptp-0.29 (crate (name "tptp") (vers "0.29.0") (deps (list (crate-dep (name "insta") (req "^1") (default-features #t) (kind 2)) (crate-dep (name "nom") (req "^6") (features (quote ("alloc"))) (kind 0)) (crate-dep (name "serde") (req "^1") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)))) (hash "15l7qa171xnjsp2bkqv887rqnpjsy5k0c942zdc8v60mdfyzwipv")))

(define-public crate-tptp-0.29 (crate (name "tptp") (vers "0.29.1") (deps (list (crate-dep (name "insta") (req "^1") (default-features #t) (kind 2)) (crate-dep (name "nom") (req "^6") (features (quote ("alloc"))) (kind 0)) (crate-dep (name "serde") (req "^1") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)))) (hash "12bqibzvknpzhw52cl8f5xhnc972a411b5khiazfx7l4m4pfynjb")))

(define-public crate-tptp-0.30 (crate (name "tptp") (vers "0.30.0") (deps (list (crate-dep (name "insta") (req "^1") (default-features #t) (kind 2)) (crate-dep (name "nom") (req "^7") (features (quote ("alloc"))) (kind 0)) (crate-dep (name "serde") (req "^1") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)))) (hash "103pw4ii3s20nz7hjggdfp00sw3fxqkagc6q6hb3ai1zssmlcn28")))

(define-public crate-tptp-0.31 (crate (name "tptp") (vers "0.31.0") (deps (list (crate-dep (name "derive_more") (req "^0.99") (features (quote ("display"))) (kind 0)) (crate-dep (name "insta") (req "^1") (default-features #t) (kind 2)) (crate-dep (name "nom") (req "^7") (features (quote ("alloc"))) (kind 0)) (crate-dep (name "serde") (req "^1") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)))) (hash "11hs4sqzabspi31s6zq7pfzqhankm8fq8pm1xcld59y9q7ybxgxq") (yanked #t)))

(define-public crate-tptp-0.31 (crate (name "tptp") (vers "0.31.1") (deps (list (crate-dep (name "derive_more") (req "^0.99") (features (quote ("display"))) (kind 0)) (crate-dep (name "insta") (req "^1") (default-features #t) (kind 2)) (crate-dep (name "nom") (req "^7") (features (quote ("alloc"))) (kind 0)) (crate-dep (name "serde") (req "^1") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)))) (hash "1iq51yxr5dll8235fbq25k597q8jdf6zmy3c2bkcm0dfhs8y382z")))

