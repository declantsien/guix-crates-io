(define-module (crates-io tp kt) #:use-module (crates-io))

(define-public crate-tpkt-0.1 (crate (name "tpkt") (vers "0.1.0") (deps (list (crate-dep (name "bytes") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.40") (default-features #t) (kind 0)) (crate-dep (name "tokio-util") (req "^0.7.8") (features (quote ("codec"))) (default-features #t) (kind 0)))) (hash "0gqh05h0kiypghwh0xh6slxnrkr2f015fji03vs7s4wmn4qnx6ff")))

