(define-module (crates-io tp m2) #:use-module (crates-io))

(define-public crate-tpm2-0.0.1 (crate (name "tpm2") (vers "0.0.1") (deps (list (crate-dep (name "bitflags") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "cfg-if") (req "^0.1.6") (default-features #t) (kind 0)) (crate-dep (name "once_cell") (req "^1.0.1") (optional #t) (default-features #t) (kind 0)))) (hash "1jf1r34hn5h34k96vgm234ylgj1qgbsw9acgksa41fs8h9n0w3b9") (features (quote (("std" "alloc" "once_cell") ("default" "std") ("alloc"))))))

(define-public crate-tpm2-0.0.2 (crate (name "tpm2") (vers "0.0.2") (deps (list (crate-dep (name "cfg-if") (req "^1.0") (default-features #t) (kind 0)))) (hash "0jm7fp4q0aw7qi1fgqrwnp5b64x208s4ggv02d1d8kilk4zjvvjg") (features (quote (("std" "alloc") ("default" "std") ("alloc"))))))

(define-public crate-tpm2-0.0.3 (crate (name "tpm2") (vers "0.0.3") (deps (list (crate-dep (name "cfg-if") (req "^1.0") (default-features #t) (kind 0)))) (hash "0fh2w5hndfs3bkx1z2m3yyjbxf04p3j58zllkm97fwlqa3mcmcgy") (features (quote (("std" "alloc") ("default" "std") ("alloc"))))))

(define-public crate-tpm2-0.0.4 (crate (name "tpm2") (vers "0.0.4") (deps (list (crate-dep (name "bitflags") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "cfg-if") (req "^1.0") (default-features #t) (kind 0)))) (hash "0f9vyx29lkp59vrbv2asir40pc38q8r7bp75g8nhxj37sjm7i2rv") (features (quote (("std" "alloc") ("default" "std") ("alloc"))))))

(define-public crate-tpm2-policy-0.1 (crate (name "tpm2-policy") (vers "0.1.0") (deps (list (crate-dep (name "base64") (req "^0.12.1") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "tss-esapi") (req "^4.0.5-alpha.1") (default-features #t) (kind 0)))) (hash "0i2z6pac6ksnf74l28b9gjddkf21ijcisrins2icpy7hjb72sla6")))

(define-public crate-tpm2-policy-0.2 (crate (name "tpm2-policy") (vers "0.2.0") (deps (list (crate-dep (name "base64") (req "^0.12.1") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "tss-esapi") (req "^4.0.6-alpha.1") (default-features #t) (kind 0)))) (hash "008z8wx4yagzz7cp6fchvczsqw1b75icdy9n05pvl7q0223g1dxz")))

(define-public crate-tpm2-policy-0.3 (crate (name "tpm2-policy") (vers "0.3.0") (deps (list (crate-dep (name "base64") (req "^0.12.1") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.59") (default-features #t) (kind 2)) (crate-dep (name "tempfile") (req "^3.1.0") (default-features #t) (kind 2)) (crate-dep (name "tss-esapi") (req "^4.0.10-alpha.1") (default-features #t) (kind 0)))) (hash "1k800v0saxzbzjqhrkbnkqrjlkjm9cc9klqhlkw7xgrbrlgrzk8w")))

(define-public crate-tpm2-policy-0.3 (crate (name "tpm2-policy") (vers "0.3.1") (deps (list (crate-dep (name "base64") (req "^0.12.1") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.59") (default-features #t) (kind 2)) (crate-dep (name "tempfile") (req "^3.1.0") (default-features #t) (kind 2)) (crate-dep (name "tss-esapi") (req "^4.0.10-alpha.1") (default-features #t) (kind 0)))) (hash "0f03da27rgdp5in3yzmql2ya7x3zva9sbyw3l40fbfcn1hyxzn5x")))

(define-public crate-tpm2-policy-0.4 (crate (name "tpm2-policy") (vers "0.4.0") (deps (list (crate-dep (name "base64") (req "^0.12.1") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.59") (default-features #t) (kind 2)) (crate-dep (name "tempfile") (req "^3.1.0") (default-features #t) (kind 2)) (crate-dep (name "tss-esapi") (req "^5.0") (default-features #t) (kind 0)))) (hash "0wwrbd9rkczigmbi9kd77f7aq701yp7zpp21gfad2ynxib9y3wxs")))

(define-public crate-tpm2-policy-0.5 (crate (name "tpm2-policy") (vers "0.5.1") (deps (list (crate-dep (name "base64") (req "^0.12.1") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.59") (default-features #t) (kind 2)) (crate-dep (name "tempfile") (req "^3.1.0") (default-features #t) (kind 2)) (crate-dep (name "tss-esapi") (req "^6.1.1") (default-features #t) (kind 0)))) (hash "085885j1lhrs733xviq74488qk1yb79457xkfdv3v2casc3w6ac1")))

(define-public crate-tpm2-policy-0.5 (crate (name "tpm2-policy") (vers "0.5.2") (deps (list (crate-dep (name "base64") (req "^0.12.1") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.59") (default-features #t) (kind 2)) (crate-dep (name "tempfile") (req "^3.1.0") (default-features #t) (kind 2)) (crate-dep (name "tss-esapi") (req "^6.1.1") (default-features #t) (kind 0)))) (hash "1y805c7zhddsbrslyjrq5xj34hiarakm9y7jgh6ppapsk3qzibp3")))

(define-public crate-tpm2-policy-0.5 (crate (name "tpm2-policy") (vers "0.5.3") (deps (list (crate-dep (name "base64") (req "^0.12.1") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.59") (default-features #t) (kind 2)) (crate-dep (name "tempfile") (req "^3.1.0") (default-features #t) (kind 2)) (crate-dep (name "tss-esapi") (req "^6.1.1") (default-features #t) (kind 0)))) (hash "04qz87m9fibjcpqszmvjx2vvk0vi0yqkrxxr012dk7gprf3lpvkr")))

(define-public crate-tpm2-policy-0.6 (crate (name "tpm2-policy") (vers "0.6.0") (deps (list (crate-dep (name "base64") (req "^0.12.1") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.59") (default-features #t) (kind 2)) (crate-dep (name "tempfile") (req "^3.1.0") (default-features #t) (kind 2)) (crate-dep (name "tss-esapi") (req "^7.0.0") (default-features #t) (kind 0)))) (hash "0ais0fg1spn0iaq0lfx8zs46w4rixcs16jnjwy3isjzxhh017g87")))

(define-public crate-tpm2-simulator-0.0.3 (crate (name "tpm2-simulator") (vers "0.0.3") (hash "17d5azl5kv77b8jzggs6jy2n78hb96b1nlh0dwyjd8djxm8bsp3c")))

(define-public crate-tpm2-wolf-0.0.1 (crate (name "tpm2-wolf") (vers "0.0.1") (deps (list (crate-dep (name "bindgen") (req "^0") (default-features #t) (kind 1)))) (hash "1331d69p27jq46vkp2lg4f3gwxyjwrkfw4x1zz2qbpysr16i72r0")))

(define-public crate-tpm2_ek_cert_gen-0.1 (crate (name "tpm2_ek_cert_gen") (vers "0.1.0") (deps (list (crate-dep (name "anyhow") (req "^1.0.38") (default-features #t) (kind 0)) (crate-dep (name "openssl") (req "^0.10.32") (default-features #t) (kind 0)) (crate-dep (name "tss-esapi") (req "^4.0.10-alpha.2") (default-features #t) (kind 0)))) (hash "00m4rs735vphiz8s0w2595cjh9m09kabnj2wqzca5vsr5hkfnmn2")))

(define-public crate-tpm2_ek_cert_gen-0.1 (crate (name "tpm2_ek_cert_gen") (vers "0.1.1") (deps (list (crate-dep (name "anyhow") (req "^1.0.38") (default-features #t) (kind 0)) (crate-dep (name "openssl") (req "^0.10.32") (default-features #t) (kind 0)) (crate-dep (name "tss-esapi") (req "^4.0.10-alpha.2") (default-features #t) (kind 0)))) (hash "0r4s0wnibzwz5m05cm6v1kyq666nznygmm99sv476c59awr3rl5w")))

