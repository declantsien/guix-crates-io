(define-module (crates-io tp m-) #:use-module (crates-io))

(define-public crate-tpm-box-0.1 (crate (name "tpm-box") (vers "0.1.0") (deps (list (crate-dep (name "testresult") (req "^0.3.0") (default-features #t) (kind 2)) (crate-dep (name "tss-esapi") (req "^8.0.0-alpha") (features (quote ("generate-bindings"))) (default-features #t) (kind 0)))) (hash "0xjrwrasqv9l4vf0jca9cysi5sr0728p73xzddzks9nd7jx31y6r")))

(define-public crate-tpm-box-0.1 (crate (name "tpm-box") (vers "0.1.1") (deps (list (crate-dep (name "testresult") (req "^0.3.0") (default-features #t) (kind 2)) (crate-dep (name "tss-esapi") (req "^8.0.0-alpha") (features (quote ("generate-bindings"))) (default-features #t) (kind 0)))) (hash "0dq6b7iyikpkkj7dw2qwq9xzk6ap4qwhx8zlbn1bcx3zzqp12f3n")))

(define-public crate-tpm-change-pin-0.1 (crate (name "tpm-change-pin") (vers "0.1.0") (deps (list (crate-dep (name "pkcs11") (req "^0.5") (default-features #t) (kind 0)) (crate-dep (name "rpassword") (req "^7.2") (default-features #t) (kind 0)))) (hash "10yns8vsbrhlqscy8ilnbxv0xr8pr7c8aakk60zgl33cb00qjhwb")))

