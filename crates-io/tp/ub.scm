(define-module (crates-io tp ub) #:use-module (crates-io))

(define-public crate-tpublish-0.1 (crate (name "tpublish") (vers "0.1.0") (deps (list (crate-dep (name "github-rs") (req "^0.6") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)))) (hash "1bivyhs7s2ww0akz2n4vqahy0gc38i8p21rpcnpv0mig7csfqdd3")))

