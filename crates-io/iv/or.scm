(define-module (crates-io iv or) #:use-module (crates-io))

(define-public crate-ivories-0.1 (crate (name "ivories") (vers "0.1.0") (hash "0r7dfx95y7qdbcb59c2ryckjdgjhh9dxl2zj8cslbbakjg2s7lms")))

(define-public crate-ivory-0.0.0 (crate (name "ivory") (vers "0.0.0") (hash "0hzmigfl8rnklrd5drmsxsf1bnr0pvjliqidnil7gkj8cj10yvbv")))

(define-public crate-ivory_bus-0.1 (crate (name "ivory_bus") (vers "0.1.0") (deps (list (crate-dep (name "lapin") (req "^2.1.1") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.17") (features (quote ("std"))) (default-features #t) (kind 0)) (crate-dep (name "nalgebra") (req "^0.31.4") (features (quote ("serde-serialize"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.152") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.91") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.24.1") (features (quote ("full"))) (default-features #t) (kind 0)) (crate-dep (name "tokio-executor-trait") (req "^2.1.0") (default-features #t) (kind 0)) (crate-dep (name "tokio-reactor-trait") (req "^1.1.0") (default-features #t) (kind 0)))) (hash "0rzw7f25kqlx8l1rvn2axijgy3ms2y190yjn93ppdkph9f8xsm1v")))

(define-public crate-ivory_bus-0.1 (crate (name "ivory_bus") (vers "0.1.1") (deps (list (crate-dep (name "lapin") (req "^2.1.1") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.17") (features (quote ("std"))) (default-features #t) (kind 0)) (crate-dep (name "nalgebra") (req "^0.31.4") (features (quote ("serde-serialize"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.152") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.91") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.24.1") (features (quote ("full"))) (default-features #t) (kind 0)) (crate-dep (name "tokio-executor-trait") (req "^2.1.0") (default-features #t) (kind 0)) (crate-dep (name "tokio-reactor-trait") (req "^1.1.0") (default-features #t) (kind 0)))) (hash "03j1y3h24yamgkbhbl2v14j7maffxdyvd8676hgjc5jxfxg6hwc0")))

(define-public crate-ivory_bus-0.1 (crate (name "ivory_bus") (vers "0.1.2") (deps (list (crate-dep (name "lapin") (req "^2.1.1") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.17") (features (quote ("std"))) (default-features #t) (kind 0)) (crate-dep (name "nalgebra") (req "^0.31.4") (features (quote ("serde-serialize"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.152") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.91") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.24.1") (features (quote ("full"))) (default-features #t) (kind 0)) (crate-dep (name "tokio-executor-trait") (req "^2.1.0") (default-features #t) (kind 0)) (crate-dep (name "tokio-reactor-trait") (req "^1.1.0") (default-features #t) (kind 0)))) (hash "07p0j2ykrsc6ipy65sk79hiahsw5z0rqfq8kijijagb1xjvqsg2m")))

(define-public crate-ivory_bus-0.1 (crate (name "ivory_bus") (vers "0.1.3") (deps (list (crate-dep (name "lapin") (req "^2.1.1") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.17") (features (quote ("std"))) (default-features #t) (kind 0)) (crate-dep (name "nalgebra") (req "^0.31.4") (features (quote ("serde-serialize"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.152") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.91") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.24.1") (features (quote ("full"))) (default-features #t) (kind 0)) (crate-dep (name "tokio-executor-trait") (req "^2.1.0") (default-features #t) (kind 0)) (crate-dep (name "tokio-reactor-trait") (req "^1.1.0") (default-features #t) (kind 0)))) (hash "1lqlziiac07b00c892994ziajmrs0yvrfnzkwgyab1ndrh63zj6a")))

(define-public crate-ivory_bus-0.1 (crate (name "ivory_bus") (vers "0.1.4") (deps (list (crate-dep (name "lapin") (req "^2.1.1") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.17") (features (quote ("std"))) (default-features #t) (kind 0)) (crate-dep (name "nalgebra") (req "^0.31.4") (features (quote ("serde-serialize"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.152") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.91") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.24.1") (features (quote ("full"))) (default-features #t) (kind 0)) (crate-dep (name "tokio-executor-trait") (req "^2.1.0") (default-features #t) (kind 0)) (crate-dep (name "tokio-reactor-trait") (req "^1.1.0") (default-features #t) (kind 0)))) (hash "1cbj188v2rig21wjkryry659qh9ggw27mgfs3by3bgm32z7yxyzr")))

(define-public crate-ivory_bus-0.1 (crate (name "ivory_bus") (vers "0.1.5") (deps (list (crate-dep (name "lapin") (req "^2.1.1") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.17") (features (quote ("std"))) (default-features #t) (kind 0)) (crate-dep (name "nalgebra") (req "^0.31.4") (features (quote ("serde-serialize"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.152") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.91") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.24.1") (features (quote ("full"))) (default-features #t) (kind 0)) (crate-dep (name "tokio-executor-trait") (req "^2.1.0") (default-features #t) (kind 0)) (crate-dep (name "tokio-reactor-trait") (req "^1.1.0") (default-features #t) (kind 0)))) (hash "0yzbk1d47awimqfxyr3ws4l7addis4mlnqkrhhdvm2pgkcmwd2rh")))

(define-public crate-ivory_constants-0.1 (crate (name "ivory_constants") (vers "0.1.0") (hash "1drwn7z0mm0fdh4cj2dwfrp6a9n13xpwgk87bilbq8rnv2m7xfb4")))

(define-public crate-ivory_constants-0.1 (crate (name "ivory_constants") (vers "0.1.1") (hash "006ishwjb6xx4f5888xl1m17bmva2vsd7ic87fdr107awmc12312")))

(define-public crate-ivory_kinematics-0.1 (crate (name "ivory_kinematics") (vers "0.1.0") (deps (list (crate-dep (name "nalgebra") (req "^0.31.4") (features (quote ("serde"))) (default-features #t) (kind 0)))) (hash "0vasjfzyviavffrhcjqgw7y73450ss6wg4s8bc81y104wkws4qw5")))

(define-public crate-ivory_kinematics-0.1 (crate (name "ivory_kinematics") (vers "0.1.1") (deps (list (crate-dep (name "nalgebra") (req "^0.31.4") (features (quote ("serde"))) (default-features #t) (kind 0)))) (hash "05pw5i0ahx65980mnp38xy729nglf16zwjghbmmkk8rv2rr0sxmy")))

(define-public crate-ivory_kinematics-0.1 (crate (name "ivory_kinematics") (vers "0.1.2") (deps (list (crate-dep (name "nalgebra") (req "^0.31.4") (features (quote ("serde"))) (default-features #t) (kind 0)))) (hash "0b2wdgbws8q2yxn7wfjil69lkl5xb4x0aahwsr9xprzdki9qp4vi")))

(define-public crate-ivory_kinematics-0.1 (crate (name "ivory_kinematics") (vers "0.1.3") (deps (list (crate-dep (name "nalgebra") (req "^0.31.4") (features (quote ("serde"))) (default-features #t) (kind 0)))) (hash "1vcccbmjrjf8dssfw21pwy5fxrnyfcbyx7171i8k1qp47rh54als")))

(define-public crate-ivory_kinematics-0.1 (crate (name "ivory_kinematics") (vers "0.1.4") (deps (list (crate-dep (name "nalgebra") (req "^0.31.4") (features (quote ("serde"))) (default-features #t) (kind 0)))) (hash "14sgyj6q1jh35gyhxxhyg3c82xzdzpxasj236xksrk8ns2jk3pcc")))

(define-public crate-ivory_kinematics-0.1 (crate (name "ivory_kinematics") (vers "0.1.5") (deps (list (crate-dep (name "nalgebra") (req "^0.31.4") (features (quote ("serde"))) (default-features #t) (kind 0)))) (hash "1yd9cfrgdk6x6dwqchavnry89znpayczdz2h09gf2da66z40q1ra")))

(define-public crate-ivory_kinematics-0.1 (crate (name "ivory_kinematics") (vers "0.1.6") (deps (list (crate-dep (name "nalgebra") (req "^0.31.4") (features (quote ("serde"))) (default-features #t) (kind 0)))) (hash "1g7pcc8hjx5ihhyjvpydyhg87kkjhvnyz8avsqh6ba05bbnqirhh")))

(define-public crate-ivory_kinematics-0.1 (crate (name "ivory_kinematics") (vers "0.1.7") (deps (list (crate-dep (name "nalgebra") (req "^0.31.4") (features (quote ("serde"))) (default-features #t) (kind 0)))) (hash "1fhyp5fhd3v6nf7mi5km86ic6knqsb4hf5d1sllvwhbdbv7grz6y")))

(define-public crate-ivory_kinematics-0.1 (crate (name "ivory_kinematics") (vers "0.1.8") (deps (list (crate-dep (name "nalgebra") (req "^0.31.4") (features (quote ("serde"))) (default-features #t) (kind 0)))) (hash "02bld69w4jf2rx4micfvayy32g0h10768jps19z14ypi9qk5z2km")))

(define-public crate-ivory_kinematics-0.1 (crate (name "ivory_kinematics") (vers "0.1.9") (deps (list (crate-dep (name "nalgebra") (req "^0.31.4") (features (quote ("serde"))) (default-features #t) (kind 0)))) (hash "0pyc4hl1j9fzxg5nqssqzkgnfd1shx7hd9b9snp762psdj38hfrz")))

(define-public crate-ivory_kinematics-0.2 (crate (name "ivory_kinematics") (vers "0.2.0") (deps (list (crate-dep (name "nalgebra") (req "^0.31.4") (features (quote ("serde"))) (default-features #t) (kind 0)))) (hash "01i1lw6v4n2dxprmv7iffwk8vcnkcvgz73br4rmvdx0f4b60xdgj")))

(define-public crate-ivory_kinematics-0.2 (crate (name "ivory_kinematics") (vers "0.2.1") (deps (list (crate-dep (name "nalgebra") (req "^0.31.4") (features (quote ("serde"))) (default-features #t) (kind 0)))) (hash "0vlh72gqxfm19kc21ha85r9manrqwq28qwimswckvpk94y9r319g")))

(define-public crate-ivory_kinematics-0.2 (crate (name "ivory_kinematics") (vers "0.2.2") (deps (list (crate-dep (name "ivory_constants") (req "^0.1.1") (default-features #t) (kind 0)) (crate-dep (name "nalgebra") (req "^0.31.4") (features (quote ("serde"))) (default-features #t) (kind 0)))) (hash "1di2vhwi62v8pm4avg3kzk6zwfalk6hzcbd888mahp6ranz3kzlv")))

(define-public crate-ivory_kinematics-0.2 (crate (name "ivory_kinematics") (vers "0.2.3") (deps (list (crate-dep (name "ivory_constants") (req "^0.1.1") (default-features #t) (kind 0)) (crate-dep (name "nalgebra") (req "^0.31.4") (features (quote ("serde"))) (default-features #t) (kind 0)))) (hash "18sj2h7h1imb00xqrhy1vmg7dyfxrq0q561nnalzdj6b9znc1vsl")))

