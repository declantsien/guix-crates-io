(define-module (crates-io iv m-) #:use-module (crates-io))

(define-public crate-ivm-compile-0.1 (crate (name "ivm-compile") (vers "0.1.0") (hash "1vy3dfq39z88k60b9g1qa4ixxjzb67klrqiiv951z1fj1l01z0xi")))

(define-public crate-ivm-vm-0.1 (crate (name "ivm-vm") (vers "0.1.0") (deps (list (crate-dep (name "ivm-compile") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "0skb790zsfq9da8jvvdk5rpqbh60fbqrwbcl8zz512bb3ara532v")))

