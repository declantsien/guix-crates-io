(define-module (crates-io iv y_) #:use-module (crates-io))

(define-public crate-ivy_core-0.1 (crate (name "ivy_core") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "~2.33.0") (default-features #t) (kind 0)))) (hash "19nsilv218cj60a3carpdb1xj18d1qlxd9iacxygj6nz7ic718yp")))

