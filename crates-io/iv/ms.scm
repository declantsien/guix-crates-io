(define-module (crates-io iv ms) #:use-module (crates-io))

(define-public crate-ivms101-0.1 (crate (name "ivms101") (vers "0.1.0") (deps (list (crate-dep (name "chrono") (req "^0.4") (features (quote ("serde" "clock"))) (kind 0)) (crate-dep (name "lei") (req "^0.2") (default-features #t) (kind 0) (package "leim")) (crate-dep (name "serde") (req "^1") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.96") (default-features #t) (kind 2)) (crate-dep (name "serde_test") (req "^1.0.163") (default-features #t) (kind 2)) (crate-dep (name "thiserror") (req "^1") (default-features #t) (kind 0)))) (hash "0bdn1avh2gi98iqr8rrbh10yx39k6inblnpbh2lcm2ln99rmaq2y") (rust-version "1.70")))

