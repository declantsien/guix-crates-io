(define-module (crates-io me ku) #:use-module (crates-io))

(define-public crate-mekuteriya-0.1 (crate (name "mekuteriya") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^4.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "colored") (req "^2.0.0") (default-features #t) (kind 0)) (crate-dep (name "ethiopic-calendar") (req "^0.1.2") (default-features #t) (kind 0)) (crate-dep (name "time") (req "^0.3") (default-features #t) (kind 0)))) (hash "0gdnyixfp04pk5gaq9r3saa0ylmsx2lqh8m1w4kkg34srfsxf5g4")))

(define-public crate-mekuteriya-0.1 (crate (name "mekuteriya") (vers "0.1.2") (deps (list (crate-dep (name "clap") (req "^4.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "colored") (req "^2.0.0") (default-features #t) (kind 0)) (crate-dep (name "ethiopic-calendar") (req "^0.1.2") (default-features #t) (kind 0)) (crate-dep (name "time") (req "^0.3") (default-features #t) (kind 0)))) (hash "15c1ciihg9nr998lss719a4gkzfkqyq6d1yk5kk68b7vndvyjfvs")))

(define-public crate-mekuteriya-0.1 (crate (name "mekuteriya") (vers "0.1.4") (deps (list (crate-dep (name "clap") (req "^4.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "colored") (req "^2.0.0") (default-features #t) (kind 0)) (crate-dep (name "ethiopic-calendar") (req "^0.1.4") (default-features #t) (kind 0)) (crate-dep (name "time") (req "^0.3") (default-features #t) (kind 0)))) (hash "0ldv46d3mx7lksq7lb9971jjlq3prd46wkij9d9rr7k8m7k2rgvv")))

