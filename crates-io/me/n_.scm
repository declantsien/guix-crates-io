(define-module (crates-io me n_) #:use-module (crates-io))

(define-public crate-men_in_tights-0.0.1 (crate (name "men_in_tights") (vers "0.0.1") (hash "1gscix00x6rskfy8simqgqix1lawx53i99fm4scgdfap2jqx6w22")))

(define-public crate-men_in_tights-0.0.2 (crate (name "men_in_tights") (vers "0.0.2") (deps (list (crate-dep (name "chrono") (req "^0.4.31") (features (quote ("serde"))) (default-features #t) (kind 0)) (crate-dep (name "indoc") (req "^2.0.4") (default-features #t) (kind 2)) (crate-dep (name "reqwest") (req "^0.11.22") (features (quote ("json"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.189") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.107") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.33.0") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "19dy7v20jh52rj3wjqq83lmyd94jwna32jdka50qyhbbywkqgxln")))

