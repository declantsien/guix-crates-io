(define-module (crates-io me p-) #:use-module (crates-io))

(define-public crate-mep-vm-0.1 (crate (name "mep-vm") (vers "0.1.0") (deps (list (crate-dep (name "ethereum-types") (req "^0.8.0") (default-features #t) (kind 0)) (crate-dep (name "ethjson") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "keccak-hash") (req "^0.4.0") (default-features #t) (kind 0)) (crate-dep (name "parity-bytes") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "patricia-trie-ethereum") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "rlp") (req "^0.4.0") (default-features #t) (kind 0)))) (hash "0ddsghjr9kfp3bd88chs0hxz1m9z6nlyjj3qvv5zgljlhmnz3r6s")))

