(define-module (crates-io me ma) #:use-module (crates-io))

(define-public crate-memac-0.3 (crate (name "memac") (vers "0.3.0") (deps (list (crate-dep (name "rand") (req "^0.8") (default-features #t) (kind 2)) (crate-dep (name "synctools") (req "^0.3.0") (default-features #t) (kind 0)))) (hash "14bngsnl04w0cnhq5l4cmvf5cwmi00hs6avf1xda19pzhs2blljz") (features (quote (("default" "buddy_32m") ("buddy_8t") ("buddy_8g") ("buddy_64m") ("buddy_64g") ("buddy_512m") ("buddy_512g") ("buddy_4t") ("buddy_4g") ("buddy_32m") ("buddy_32g") ("buddy_2t") ("buddy_2g") ("buddy_256m") ("buddy_256g") ("buddy_1t") ("buddy_1g") ("buddy_16g") ("buddy_128m") ("buddy_128g"))))))

(define-public crate-memac-0.3 (crate (name "memac") (vers "0.3.1") (deps (list (crate-dep (name "rand") (req "^0.8") (default-features #t) (kind 2)) (crate-dep (name "synctools") (req "^0.3") (default-features #t) (kind 0)))) (hash "1qcfg5zwvqvfg2v0d7di4slds569c552im0mdgjh3kaaqv44cipa") (features (quote (("default" "buddy_32m") ("buddy_8t") ("buddy_8g") ("buddy_64m") ("buddy_64g") ("buddy_512m") ("buddy_512g") ("buddy_4t") ("buddy_4g") ("buddy_32m") ("buddy_32g") ("buddy_2t") ("buddy_2g") ("buddy_256m") ("buddy_256g") ("buddy_1t") ("buddy_1g") ("buddy_16g") ("buddy_128m") ("buddy_128g"))))))

(define-public crate-memac-0.3 (crate (name "memac") (vers "0.3.2") (deps (list (crate-dep (name "rand") (req "^0.8") (default-features #t) (kind 2)) (crate-dep (name "synctools") (req "^0.3") (default-features #t) (kind 0)))) (hash "1dyf6sbdws55v5sgf668b9ri8jqrc72a3591dawvx2zjh1l2c6kl") (features (quote (("default" "buddy_32m") ("buddy_8t") ("buddy_8g") ("buddy_64m") ("buddy_64g") ("buddy_512m") ("buddy_512g") ("buddy_4t") ("buddy_4g") ("buddy_32m") ("buddy_32g") ("buddy_2t") ("buddy_2g") ("buddy_256m") ("buddy_256g") ("buddy_1t") ("buddy_1g") ("buddy_16g") ("buddy_128m") ("buddy_128g"))))))

(define-public crate-memac-0.3 (crate (name "memac") (vers "0.3.3") (deps (list (crate-dep (name "rand") (req "^0.8") (default-features #t) (kind 2)) (crate-dep (name "synctools") (req "^0.3") (default-features #t) (kind 0)))) (hash "0x101g9714fnfcvvsqpdlww25qhmjh6q1jc720zrs8n3imvz3a0n") (features (quote (("default" "buddy_32m") ("buddy_8t") ("buddy_8g") ("buddy_64m") ("buddy_64g") ("buddy_512m") ("buddy_512g") ("buddy_4t") ("buddy_4g") ("buddy_32m") ("buddy_32g") ("buddy_2t") ("buddy_2g") ("buddy_256m") ("buddy_256g") ("buddy_1t") ("buddy_1g") ("buddy_16g") ("buddy_128m") ("buddy_128g"))))))

(define-public crate-memac-0.4 (crate (name "memac") (vers "0.4.0") (deps (list (crate-dep (name "rand") (req "^0.8") (default-features #t) (kind 2)) (crate-dep (name "synctools") (req "^0.3") (default-features #t) (kind 0)))) (hash "118i4gg67yp1slvm85havpih8p4324bb05ay6x032ha7hhyr5ljf") (features (quote (("default" "buddy_32m") ("buddy_8t") ("buddy_8g") ("buddy_64m") ("buddy_64g") ("buddy_512m") ("buddy_512g") ("buddy_4t") ("buddy_4g") ("buddy_32m") ("buddy_32g") ("buddy_2t") ("buddy_2g") ("buddy_256m") ("buddy_256g") ("buddy_1t") ("buddy_1g") ("buddy_16g") ("buddy_128m") ("buddy_128g"))))))

(define-public crate-memac-0.5 (crate (name "memac") (vers "0.5.0") (deps (list (crate-dep (name "rand") (req "^0.8") (default-features #t) (kind 2)) (crate-dep (name "synctools") (req "^0.3") (default-features #t) (kind 0)))) (hash "0v165w2xzqrfda8l0g1n0fhhc4n6ld1s2awmfq80626j5kbql2h4")))

(define-public crate-memac-0.5 (crate (name "memac") (vers "0.5.1") (deps (list (crate-dep (name "rand") (req "^0.8") (default-features #t) (kind 2)) (crate-dep (name "synctools") (req "^0.3") (default-features #t) (kind 0)))) (hash "1fv2c0gjfynsy9apj8dwmyvi33pr8srw17n6nqb4awmfdxin46za")))

(define-public crate-memac-0.5 (crate (name "memac") (vers "0.5.2") (deps (list (crate-dep (name "rand") (req "^0.8") (default-features #t) (kind 2)) (crate-dep (name "synctools") (req "^0.4") (default-features #t) (kind 0)))) (hash "1db9z32dqffzwlzc0bzsiyk2hxz98wgrs5i00d615jrqkz4hlsx7")))

(define-public crate-memac-0.5 (crate (name "memac") (vers "0.5.3") (deps (list (crate-dep (name "rand") (req "^0.8") (default-features #t) (kind 2)) (crate-dep (name "synctools") (req "^0.3") (default-features #t) (kind 0)))) (hash "150hwmp7giahd99zsbjml47952wx8bcxjk4p2rbwk0rp5v4pw15n")))

(define-public crate-memacc-0.1 (crate (name "memacc") (vers "0.1.0") (hash "06mjpkjjla4mbdjh4mc89z0fmgc5vp2z21n0im3hyzxg3qdq7l08")))

(define-public crate-memacc-0.1 (crate (name "memacc") (vers "0.1.1") (hash "17lygx2qjyb0ankgdm47645v71h2175xll3sckkyyw7mh06n00j4")))

(define-public crate-memacc-0.1 (crate (name "memacc") (vers "0.1.2") (hash "0aq9ivgnwyfrqbv2zqlqkyf4n7rvaqg7nzxrqpha8mh95v883vky") (features (quote (("no-std"))))))

(define-public crate-memacc-0.1 (crate (name "memacc") (vers "0.1.3") (hash "13xf5d4sr6yh94zhkrvvzc99sy2hs0gahp7rqwklpylc6iik4mk6") (features (quote (("no-std"))))))

(define-public crate-memacc-0.1 (crate (name "memacc") (vers "0.1.4") (hash "0a4nyrprinhqxaicng2svflys7mps0k24yc3lk5mf34k0rc4yd7a") (features (quote (("no-std"))))))

(define-public crate-memacc-0.1 (crate (name "memacc") (vers "0.1.5") (hash "08dy1rwjyf9y56cki5q122ys9k0vz0v308yjxhz8kirj2vfga999") (features (quote (("no-std"))))))

(define-public crate-memacc-0.1 (crate (name "memacc") (vers "0.1.6") (hash "1ic4k9bwb11g39k24haq76y04ca7qc40z3q4q548xxnln7i4ib36") (features (quote (("no-std"))))))

(define-public crate-memacc-0.1 (crate (name "memacc") (vers "0.1.8") (hash "0fmwdwhqs3v76yanhb79mnngnj1453whw66cl34k3qq1b8agl0b1") (features (quote (("no-std"))))))

(define-public crate-memacc-0.1 (crate (name "memacc") (vers "0.1.9") (hash "1v407dxpq3p5zmlv6jd8cxn0rc5nhxw6j0fxxj98nkhfflm4bxsw") (features (quote (("no-std"))))))

(define-public crate-memacc-0.1 (crate (name "memacc") (vers "0.1.10") (hash "1sh0lk5npd9ylhlsv2w2vhxmamng31s7a1iiynsaz7lhqsfbyiin") (features (quote (("no-std"))))))

(define-public crate-memacc-0.1 (crate (name "memacc") (vers "0.1.11") (hash "11zjbp3835g8s22c7scjrvjncak1hi9z6yjlqmm3kxd8gdlgmn92") (features (quote (("no-std"))))))

(define-public crate-memadvise-0.1 (crate (name "memadvise") (vers "0.1.1") (deps (list (crate-dep (name "kernel32-sys") (req "^0.2.2") (default-features #t) (target "cfg(target_os = \"windows\")") (kind 0)) (crate-dep (name "libc") (req "^0.2.30") (default-features #t) (target "cfg(unix)") (kind 0)) (crate-dep (name "page_size") (req "^0.4.0") (default-features #t) (kind 0)) (crate-dep (name "spin") (req "^0.4.5") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "winapi") (req "^0.2.8") (default-features #t) (target "cfg(target_os = \"windows\")") (kind 0)))) (hash "03hf89yqxaclyw7fqxay1l6m53m9lschbbw5k4rylqhcnkc02xnf") (features (quote (("no_std" "page_size/no_std" "spin"))))))

(define-public crate-memadvise-0.1 (crate (name "memadvise") (vers "0.1.2") (deps (list (crate-dep (name "kernel32-sys") (req "^0.2.2") (default-features #t) (target "cfg(target_os = \"windows\")") (kind 0)) (crate-dep (name "libc") (req "^0.2.30") (default-features #t) (target "cfg(unix)") (kind 0)) (crate-dep (name "page_size") (req "^0.4.0") (default-features #t) (kind 0)) (crate-dep (name "spin") (req "^0.4.5") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "winapi") (req "^0.2.8") (default-features #t) (target "cfg(target_os = \"windows\")") (kind 0)))) (hash "1x0cd2wcj05jdmj4j3yn8z9azhpaq050h5sxnq3v6w3j0rpzixma") (features (quote (("no_std" "page_size/no_std" "spin"))))))

(define-public crate-memalloc-0.0.1 (crate (name "memalloc") (vers "0.0.1") (hash "0icr0m6maywb0w0v2pc5y8a3n25gn7icmvnkg7g99x4rw79hixqx")))

(define-public crate-memalloc-0.1 (crate (name "memalloc") (vers "0.1.0") (hash "1hgc0didnclhzkq5c7nb0m0ha3155wn9j5h2q68hh2y4ylrd4ffz")))

(define-public crate-memap-0.1 (crate (name "memap") (vers "0.1.0") (hash "1g6l9f1mlfv3h603jrp5yx53hwwm7b57syx1i6gb3dnki7rmwkmj")))

(define-public crate-memap-0.1 (crate (name "memap") (vers "0.1.1") (hash "0nhvdjjgyxqmdrnhymkdrzbcfkcg5dx3xhkvxhazcavigkh1x6zl")))

(define-public crate-memap-0.1 (crate (name "memap") (vers "0.1.2") (hash "00s0hx667dzajgc98a8b9k6g2njc5g1pk10qdjc63is82cb3dbjl")))

(define-public crate-memap2-0.1 (crate (name "memap2") (vers "0.1.0") (hash "04q99c72j6nmd8ca9j46niii47i0gx2391qlmwwvp7cizrx7476j")))

(define-public crate-memap2-0.1 (crate (name "memap2") (vers "0.1.2") (hash "0xl5k09b94l02j6pn2xlcnb1qj4p9aldfma7n9cr086x9r8jl1bc")))

(define-public crate-memas-client-0.1 (crate (name "memas-client") (vers "0.1.0") (deps (list (crate-dep (name "reqwest") (req "^0.11") (features (quote ("json" "multipart"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "url") (req "^2.2") (default-features #t) (kind 0)) (crate-dep (name "uuid") (req "^1.0") (features (quote ("serde"))) (default-features #t) (kind 0)))) (hash "1ibbqr2a6b4vz5pfp0xbiwpsc2cda5sl65cvzf2ziiky52x6xy4q")))

(define-public crate-memas-sdk-0.1 (crate (name "memas-sdk") (vers "0.1.0") (deps (list (crate-dep (name "reqwest") (req "^0.11") (features (quote ("json" "multipart"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "url") (req "^2.2") (default-features #t) (kind 0)) (crate-dep (name "uuid") (req "^1.0") (features (quote ("serde"))) (default-features #t) (kind 0)))) (hash "0l2gcl1c5clxkbzgr5bp22i8ksnrr9fy19p7mcwvgx11c2x86lpc")))

(define-public crate-mematrica-0.1 (crate (name "mematrica") (vers "0.1.0") (deps (list (crate-dep (name "num") (req "^0.4.0") (default-features #t) (kind 0)))) (hash "1fkssg6vfjw55qvmhd99cmna8m6892lvi8g4mqjqbxk5jd487mc9") (yanked #t)))

(define-public crate-mematrica-0.1 (crate (name "mematrica") (vers "0.1.1") (deps (list (crate-dep (name "num") (req "^0.4.0") (default-features #t) (kind 0)))) (hash "0fp6q3kchr43s7dmhsf5q76r8l53r0lilkw006ffkzpihf6pg3ig") (yanked #t)))

(define-public crate-mematrica-0.1 (crate (name "mematrica") (vers "0.1.2") (deps (list (crate-dep (name "num") (req "^0.4.0") (default-features #t) (kind 0)))) (hash "0bypjg0mh0mj7b6qcd0bm5ni4dfg5f8zy8756vxd2wramxx256ad") (yanked #t)))

(define-public crate-mematrica-0.1 (crate (name "mematrica") (vers "0.1.3") (deps (list (crate-dep (name "num") (req "^0.4.0") (default-features #t) (kind 0)))) (hash "1bbkqyr44sgkn6cky8lkkadm142ki8b6q9dmwxv4n8ahbv2paq0n") (yanked #t)))

(define-public crate-mematrica-0.1 (crate (name "mematrica") (vers "0.1.4") (deps (list (crate-dep (name "num") (req "^0.4.0") (default-features #t) (kind 0)))) (hash "0h8igxrhvxfbicvmymi4lm8sw7vmzapbgah9mk3gvf6d26c7bcxf") (yanked #t)))

(define-public crate-mematrica-0.1 (crate (name "mematrica") (vers "0.1.5") (deps (list (crate-dep (name "num") (req "^0.4.0") (default-features #t) (kind 0)))) (hash "0j0q70751dimi891pnv9wyrblx3avq9ijz3fxhms568mf3rx4ryb")))

(define-public crate-mematrica-0.1 (crate (name "mematrica") (vers "0.1.6") (deps (list (crate-dep (name "num") (req "^0.4.0") (default-features #t) (kind 0)))) (hash "03022dfjlw2azmfgv4f79339c4x50v1dgx7894s63vi1kgyfscbk")))

(define-public crate-mematrica-0.1 (crate (name "mematrica") (vers "0.1.7") (deps (list (crate-dep (name "num") (req "^0.4.0") (default-features #t) (kind 0)))) (hash "1b5574fary1cfx8fxkdqbnp0zcg3cg9ds09y7ic4x6wmx4m94djd")))

(define-public crate-mematrica-0.1 (crate (name "mematrica") (vers "0.1.8") (deps (list (crate-dep (name "num") (req "^0.4.0") (default-features #t) (kind 0)))) (hash "16d28fznnrf2wfys309cgbh58ydmaskfh3g7lp4alvmfksrzmh85")))

(define-public crate-mematrica-0.2 (crate (name "mematrica") (vers "0.2.0") (deps (list (crate-dep (name "num") (req "^0.4.0") (default-features #t) (kind 0)))) (hash "17ms0x3mx7nnk5ljfg5rngnk9n4b9vq8x5ciyv9am2jc9qpqdfzk")))

(define-public crate-mematrica-0.2 (crate (name "mematrica") (vers "0.2.1") (deps (list (crate-dep (name "num") (req "^0.4.0") (default-features #t) (kind 0)))) (hash "1qgwj0264dd6c19p1zgpfj3a14z4smaxi0yl7awij2cvmgh18933")))

(define-public crate-mematrica-0.2 (crate (name "mematrica") (vers "0.2.2") (deps (list (crate-dep (name "num") (req "^0.4.0") (default-features #t) (kind 0)))) (hash "0qsjdmymymq7sp82d3c7xnays6rvi0lpfajfcwhhjv8m60zylw96")))

