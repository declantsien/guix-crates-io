(define-module (crates-io me ro) #:use-module (crates-io))

(define-public crate-meros-0.0.1 (crate (name "meros") (vers "0.0.1") (hash "18drxykj4zpn4vz00g50hf3y9s8ngl92dfkb72xfsqxyd2663x6c")))

(define-public crate-merosity-0.1 (crate (name "merosity") (vers "0.1.0") (deps (list (crate-dep (name "bevy") (req "^0.12.1") (features (quote ("android_shared_stdcxx" "bevy_asset" "bevy_audio" "bevy_core_pipeline" "bevy_gilrs" "bevy_gizmos" "bevy_render" "bevy_sprite" "bevy_text" "bevy_winit" "default_font" "hdr" "multi-threaded" "png" "tonemapping_luts" "vorbis" "webgl2" "x11" "zstd"))) (kind 0)) (crate-dep (name "fastrand") (req "^2.0.1") (default-features #t) (kind 0)) (crate-dep (name "getrandom") (req "^0.2.11") (default-features #t) (kind 0)))) (hash "04xcb4hh0j73hpsrr8ib6hs2vfbzafrd7j07nj9lgznpc1mihg2m") (features (quote (("dev" "bevy/dynamic_linking" "bevy/file_watcher"))))))

