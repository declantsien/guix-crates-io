(define-module (crates-io me ck) #:use-module (crates-io))

(define-public crate-meck_grrs-0.1 (crate (name "meck_grrs") (vers "0.1.0") (deps (list (crate-dep (name "anyhow") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "assert_cmd") (req "^0.10") (default-features #t) (kind 2)) (crate-dep (name "predicates") (req "^1") (default-features #t) (kind 2)) (crate-dep (name "structopt") (req "^0.3.13") (default-features #t) (kind 0)) (crate-dep (name "tempfile") (req "^3") (default-features #t) (kind 2)))) (hash "1vl6g6iiacb37nk9mf9nijidq46a4sajm6xy2rv434d10z39lz2w")))

