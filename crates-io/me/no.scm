(define-module (crates-io me no) #:use-module (crates-io))

(define-public crate-menoh-0.1 (crate (name "menoh") (vers "0.1.0") (deps (list (crate-dep (name "docopt") (req "^1.0") (default-features #t) (kind 2)) (crate-dep (name "image") (req "^0.19") (default-features #t) (kind 2)) (crate-dep (name "menoh-sys") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (default-features #t) (kind 2)) (crate-dep (name "serde_derive") (req "^1.0") (default-features #t) (kind 2)))) (hash "1gp0giixic63cwqgs4bzjk80phzsiy17is8lwr67qfxqmq3ifvas")))

(define-public crate-menoh-0.1 (crate (name "menoh") (vers "0.1.1") (deps (list (crate-dep (name "docopt") (req "^1.0") (default-features #t) (kind 2)) (crate-dep (name "image") (req "^0.19") (default-features #t) (kind 2)) (crate-dep (name "menoh-sys") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (default-features #t) (kind 2)) (crate-dep (name "serde_derive") (req "^1.0") (default-features #t) (kind 2)))) (hash "156fnwm9c3i2w1p8ig2hjyilpy3s45d66m2bz9pn0s6xbiz9vcm7")))

(define-public crate-menoh-0.1 (crate (name "menoh") (vers "0.1.2") (deps (list (crate-dep (name "docopt") (req "^1.0") (default-features #t) (kind 2)) (crate-dep (name "image") (req "^0.19") (default-features #t) (kind 2)) (crate-dep (name "menoh-sys") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (default-features #t) (kind 2)) (crate-dep (name "serde_derive") (req "^1.0") (default-features #t) (kind 2)))) (hash "1i4jr0j5yivnyqcsiz2hri767ya2dmrfl6nmyr6301w0pas3wvhn")))

(define-public crate-menoh-0.1 (crate (name "menoh") (vers "0.1.3") (deps (list (crate-dep (name "docopt") (req "^1.0") (default-features #t) (kind 2)) (crate-dep (name "image") (req "^0.19") (default-features #t) (kind 2)) (crate-dep (name "menoh-sys") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (default-features #t) (kind 2)) (crate-dep (name "serde_derive") (req "^1.0") (default-features #t) (kind 2)))) (hash "1gvbcg0fykvq02drry2w6k3daxk58ka92rac3m8gx8yxn6dwik8c")))

(define-public crate-menoh-0.1 (crate (name "menoh") (vers "0.1.4") (deps (list (crate-dep (name "docopt") (req "^1.0") (default-features #t) (kind 2)) (crate-dep (name "image") (req "^0.19") (default-features #t) (kind 2)) (crate-dep (name "menoh-sys") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (default-features #t) (kind 2)) (crate-dep (name "serde_derive") (req "^1.0") (default-features #t) (kind 2)))) (hash "0ki4r77mgb83ffymfpc2kzy1xmz1v7ysz9rp0xfxmbd1wm38nk40")))

(define-public crate-menoh-0.2 (crate (name "menoh") (vers "0.2.0") (deps (list (crate-dep (name "docopt") (req "^1.0") (default-features #t) (kind 2)) (crate-dep (name "image") (req "^0.20") (default-features #t) (kind 2)) (crate-dep (name "menoh-sys") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (default-features #t) (kind 2)) (crate-dep (name "serde_derive") (req "^1.0") (default-features #t) (kind 2)))) (hash "1ykish1zlbpfvz4bqcknx5dyhj48f7n32xgfg3wyn3k1ffkfs3gq")))

(define-public crate-menoh-0.2 (crate (name "menoh") (vers "0.2.1") (deps (list (crate-dep (name "docopt") (req "^1.0") (default-features #t) (kind 2)) (crate-dep (name "image") (req "^0.21") (default-features #t) (kind 2)) (crate-dep (name "menoh-sys") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (default-features #t) (kind 2)) (crate-dep (name "serde_derive") (req "^1.0") (default-features #t) (kind 2)))) (hash "1zh3ywkwsgr9fj614m0iij9ra3b9zz7ri864apypr7jw5yhhih1w")))

(define-public crate-menoh-0.2 (crate (name "menoh") (vers "0.2.2") (deps (list (crate-dep (name "docopt") (req "^1.0") (default-features #t) (kind 2)) (crate-dep (name "image") (req "^0.21") (default-features #t) (kind 2)) (crate-dep (name "menoh-sys") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (default-features #t) (kind 2)) (crate-dep (name "serde_derive") (req "^1.0") (default-features #t) (kind 2)))) (hash "1ddn1ln64gzj806x9w9xw0i9df2cp61fdqjjh9sk8arj8bbljh1v")))

(define-public crate-menoh-sys-0.1 (crate (name "menoh-sys") (vers "0.1.0") (deps (list (crate-dep (name "pkg-config") (req "^0.3") (default-features #t) (kind 1)))) (hash "1k6lzs2rj0rnb86di75fi7wm8iy7ci5wryhxp8gdk93dhddp1zz9")))

(define-public crate-menoh-sys-0.1 (crate (name "menoh-sys") (vers "0.1.1") (deps (list (crate-dep (name "pkg-config") (req "^0.3") (default-features #t) (kind 1)))) (hash "1xdlx02ng2gypkv99yvv5x86sm6dlh6v0npip8l8l20ifl6b7432")))

(define-public crate-menoh-sys-0.1 (crate (name "menoh-sys") (vers "0.1.2") (deps (list (crate-dep (name "pkg-config") (req "^0.3") (default-features #t) (kind 1)))) (hash "15hyp6gfz274qbfy2kdcb4mm545gjyb3a8hw4i62p3bbmg8d5za1")))

(define-public crate-menoh-sys-0.1 (crate (name "menoh-sys") (vers "0.1.3") (deps (list (crate-dep (name "pkg-config") (req "^0.3") (default-features #t) (kind 1)))) (hash "19dmx7d9x84bwxngnl99zy8081lf32qk1a2ja64k2m0mfqf1jn74")))

(define-public crate-menoh-sys-0.1 (crate (name "menoh-sys") (vers "0.1.4") (deps (list (crate-dep (name "pkg-config") (req "^0.3") (default-features #t) (kind 1)))) (hash "1b26a46941vpwsvc42ya8zq698y73zqby531a1rqcdrlhpsi1yn8")))

(define-public crate-menoh-sys-0.2 (crate (name "menoh-sys") (vers "0.2.0") (deps (list (crate-dep (name "pkg-config") (req "^0.3") (default-features #t) (kind 1)))) (hash "1afr298b31xw3852dj3ppb1fwgvc5f5fby2wjcyars2x8ihin19v")))

(define-public crate-menoh-sys-0.2 (crate (name "menoh-sys") (vers "0.2.1") (deps (list (crate-dep (name "pkg-config") (req "^0.3") (default-features #t) (kind 1)))) (hash "1f3xq2lr6g96i9afp7vxb6a3mr3a49qihf0xv7l9hkpjr0k6nq14")))

(define-public crate-menoh-sys-0.2 (crate (name "menoh-sys") (vers "0.2.2") (deps (list (crate-dep (name "pkg-config") (req "^0.3") (default-features #t) (kind 1)))) (hash "019i152ab044h68xl7ifhhqr0pd9ic6a19c29n6jfysjn3w79pa9")))

