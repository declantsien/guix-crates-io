(define-module (crates-io me nv) #:use-module (crates-io))

(define-public crate-menv-0.1 (crate (name "menv") (vers "0.1.0") (hash "0zkq7cqilkpbd32ivfid2igkyvb0l6v4bsdmgghn3v4q5bm6hxn2")))

(define-public crate-menv-0.2 (crate (name "menv") (vers "0.2.0") (hash "0awp4sa1n5fgknrq6v6dh5jdljyrap2h931b66yffvzx90jzldi1")))

(define-public crate-menv-0.2 (crate (name "menv") (vers "0.2.1") (hash "00bvbi2g57kr6d524grh25qa2i4wk5gs4dzzlc5w1sy9s7ds2vhx")))

(define-public crate-menv-0.2 (crate (name "menv") (vers "0.2.2") (hash "0gnd6qviq7j48pbmlar4b66h0zvp8g8skp3g75sny2mmd28mfawp")))

(define-public crate-menv-0.2 (crate (name "menv") (vers "0.2.3") (hash "1j56isxqrw5yk1zn7awk2mhb9c0dnhrc83m9gdwjd3ccqnlqwnxn")))

(define-public crate-menv-0.2 (crate (name "menv") (vers "0.2.4") (hash "0hsp4gbx9yhw940lyk9ij3shvpx0gj8d7155v9h6pqgxwq3sagrs")))

(define-public crate-menv-0.2 (crate (name "menv") (vers "0.2.5") (hash "189qw42m2hyqrax5lhilv86r7q1gzzw7qgj3cxm3jkfnhfr3g2cs")))

(define-public crate-menv-0.2 (crate (name "menv") (vers "0.2.6") (deps (list (crate-dep (name "menv_proc_macro") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "18n7ih8zvy3cn3f2862p7lc0a55c6hgg11pa5ybr1x22w4m0sdlf")))

(define-public crate-menv-0.2 (crate (name "menv") (vers "0.2.7") (deps (list (crate-dep (name "menv_proc_macro") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "1rh8sijqpk0jwdzk12pzjra11zhrr9brhyvy1f4jr0ngjn1ry17c")))

(define-public crate-menv_proc_macro-0.1 (crate (name "menv_proc_macro") (vers "0.1.0") (hash "01bcxjvyg7iwk2y3l3r8f26syzpwnbi5irz85lrf1vd8gyajq5zb")))

(define-public crate-menva-0.1 (crate (name "menva") (vers "0.1.0") (hash "0afq4dqr1i3p899msvq6ai8d5abcm4788wkl6mgp4agnvzrpiw3m")))

(define-public crate-menva-0.1 (crate (name "menva") (vers "0.1.1") (hash "1gmz7cg70aa3hrlcs36qi0ywiw72lp4md0wiwz788lmvb9c1cgha")))

(define-public crate-menva-1 (crate (name "menva") (vers "1.0.0") (hash "0b38kwsaib1xq44vpq1rs2184pa150i48zli5pmkkmz4cxxzivsa")))

