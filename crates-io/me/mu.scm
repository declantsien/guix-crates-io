(define-module (crates-io me mu) #:use-module (crates-io))

(define-public crate-memu-0.1 (crate (name "memu") (vers "0.1.0") (deps (list (crate-dep (name "serde") (req "^1.0.186") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "0ir7zj1dq5j1vbjm6nhrghjwds2x855c7lf2wfg08rcy91fidp4l") (features (quote (("units") ("serde") ("macro") ("default" "macro" "units"))))))

(define-public crate-memu-0.1 (crate (name "memu") (vers "0.1.1") (deps (list (crate-dep (name "serde") (req "^1.0.186") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "15k1zzqzmmi79ljgjgjf1ng2my0rhymy0asxf90aa54i7rwq2rr1") (features (quote (("units") ("serde") ("macro") ("default" "macro" "units"))))))

(define-public crate-memu-0.1 (crate (name "memu") (vers "0.1.3") (deps (list (crate-dep (name "serde") (req "^1.0.186") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "1icycihssvp4ama3cimyzxyyk75y4ndpkibkda5zv16fgk2zx9hh") (features (quote (("units") ("serde") ("macro") ("default" "macro" "units"))))))

(define-public crate-memur-0.1 (crate (name "memur") (vers "0.1.0") (hash "0b9v7iwjdb2pp1smhyhrhx93c4825z3hcigbga3i97jy5ilgz90i")))

(define-public crate-memur-0.1 (crate (name "memur") (vers "0.1.1") (deps (list (crate-dep (name "log") (req "^0.4") (optional #t) (default-features #t) (kind 0)))) (hash "0935qga30hg4xqbh4mpg1fn5mb0gzav8kkk74mhf54xy60gldpva") (features (quote (("logging" "log") ("default"))))))

(define-public crate-memur-0.1 (crate (name "memur") (vers "0.1.2") (deps (list (crate-dep (name "log") (req "^0.4") (optional #t) (default-features #t) (kind 0)))) (hash "0nxak50wa0hv5mi91k9xymv1k9fayzzwa8smfmy3rqwqnh1bjqzk") (features (quote (("logging" "log") ("default"))))))

(define-public crate-memusage-0.1 (crate (name "memusage") (vers "0.1.0") (hash "0k9ajmgv5ni5kfpmmfkmp5sh5lk7qz9xl09px2p9y7pv8a0qnmma")))

(define-public crate-memusage-0.2 (crate (name "memusage") (vers "0.2.0") (hash "1aqxq1l51cdf8j2spp9b4vjnms1rq3hzk8rg4ydamp2qlxmi21js") (features (quote (("std") ("default" "alloc" "std") ("alloc"))))))

(define-public crate-memuse-0.0.0 (crate (name "memuse") (vers "0.0.0") (hash "0b67yw68s64xz74b2w205299xpa2vqzh1035hvk3afy6yb29h4qa") (yanked #t)))

(define-public crate-memuse-0.1 (crate (name "memuse") (vers "0.1.0") (deps (list (crate-dep (name "nonempty") (req "^0.7") (optional #t) (default-features #t) (kind 0)))) (hash "1fgxr4yrmpsm0qbsyxh8bckdjc2baz2a3n81yfm8aqmdbijiqp91")))

(define-public crate-memuse-0.2 (crate (name "memuse") (vers "0.2.0") (deps (list (crate-dep (name "nonempty") (req "^0.7") (optional #t) (default-features #t) (kind 0)))) (hash "0fsqbd6a8l8396pi4l0n4f5zyjvp8awrxscpv39rlxi8fp6jb7gn")))

(define-public crate-memuse-0.2 (crate (name "memuse") (vers "0.2.1") (deps (list (crate-dep (name "nonempty") (req "^0.7") (optional #t) (default-features #t) (kind 0)))) (hash "06kgsfv8fnhqbwnq3q841ndfq5wkanz5jpykldpfmrdc6na8ci91")))

(define-public crate-memuse_derive-0.0.0 (crate (name "memuse_derive") (vers "0.0.0") (deps (list (crate-dep (name "proc-macro2") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "synstructure") (req "^0.12") (default-features #t) (kind 0)))) (hash "05v8xjdabx57iyl195h4gy6nm5m0s87vr5vj8d1k1s8h174jnaig")))

