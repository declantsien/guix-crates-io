(define-module (crates-io me tt) #:use-module (crates-io))

(define-public crate-mettle-0.1 (crate (name "mettle") (vers "0.1.0") (hash "1wfi20hp1507hm3nv3qdpaqr25pgs6h53w0frwgyacilvp16qmhv")))

(define-public crate-metty-2024 (crate (name "metty") (vers "2024.3.0") (deps (list (crate-dep (name "ascii_table") (req "^4.0.3") (features (quote ("color_codes"))) (default-features #t) (kind 0)) (crate-dep (name "cadmium-yellow") (req "^2024.3.2") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "~4.4.0") (features (quote ("derive" "env"))) (default-features #t) (kind 0)) (crate-dep (name "colored") (req "^2.1.0") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.36.0") (features (quote ("macros" "rt-multi-thread"))) (default-features #t) (kind 0)))) (hash "17jh9ixj1j9f9b5xia7ijpn70ikpv6aaq4b70mv23fxy3rbxndg6")))

(define-public crate-metty-2024 (crate (name "metty") (vers "2024.3.1") (deps (list (crate-dep (name "ascii_table") (req "^4.0.3") (features (quote ("color_codes"))) (default-features #t) (kind 0)) (crate-dep (name "cadmium-yellow") (req "^2024.3.2") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "~4.4.0") (features (quote ("derive" "env"))) (default-features #t) (kind 0)) (crate-dep (name "clap_complete") (req "~4.4.0") (default-features #t) (kind 0)) (crate-dep (name "colored") (req "^2.1.0") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.36.0") (features (quote ("macros" "rt-multi-thread"))) (default-features #t) (kind 0)))) (hash "1cj8n33v15g5vr0x38ajdmbg98wnkj7z2bdikwfqqrvm5cq456pa")))

