(define-module (crates-io me cs) #:use-module (crates-io))

(define-public crate-mecs-0.1 (crate (name "mecs") (vers "0.1.0") (deps (list (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)))) (hash "149acbjnr2ixp57qkf0pwyywa0xlkm6kaw21svfm385mszs3axwl") (features (quote (("serde-serialize" "serde") ("default"))))))

(define-public crate-mecs-0.1 (crate (name "mecs") (vers "0.1.1") (deps (list (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)))) (hash "1p19j7cwzdrsy2m6xd5inx447r00q924h08p42v500b7xs9zhpfg") (features (quote (("serde-serialize" "serde") ("default"))))))

