(define-module (crates-io me ap) #:use-module (crates-io))

(define-public crate-meap-0.1 (crate (name "meap") (vers "0.1.0") (deps (list (crate-dep (name "parse_duration") (req "^2.1.0") (default-features #t) (kind 2)))) (hash "16n51nm1sf7svmzv8fw9vfjls7ixlhjv9g1kcnynvmg31jklbn4q")))

(define-public crate-meap-0.1 (crate (name "meap") (vers "0.1.1") (deps (list (crate-dep (name "parse_duration") (req "^2.1.0") (default-features #t) (kind 2)))) (hash "06gxx544gv8af6whlvz2rif2fw3px7rp2xjx5mvfng02n1jlw8px")))

(define-public crate-meap-0.1 (crate (name "meap") (vers "0.1.2") (deps (list (crate-dep (name "parse_duration") (req "^2.1.0") (default-features #t) (kind 2)))) (hash "1f0vcvv9cqmqkxnmzap34nvzyf8cp86l0rrwx1lng29zpmd3c81l")))

(define-public crate-meap-0.1 (crate (name "meap") (vers "0.1.3") (deps (list (crate-dep (name "parse_duration") (req "^2.1.0") (default-features #t) (kind 2)))) (hash "1pwj5mcgx6pyb01hh5f4s06z7a95sys0x6vjy50n1ycm7zmiyi8q")))

(define-public crate-meap-0.1 (crate (name "meap") (vers "0.1.4") (deps (list (crate-dep (name "parse_duration") (req "^2.1.0") (default-features #t) (kind 2)))) (hash "01kdxm68568wjyjyhx6pk62ml4aynsc7kc7mwqkgpn0xkk5pm9ss")))

(define-public crate-meap-0.1 (crate (name "meap") (vers "0.1.5") (deps (list (crate-dep (name "parse_duration") (req "^2.1.0") (default-features #t) (kind 2)))) (hash "0hyza70s18j419v5cmxi61sfs4f8q16v7ixz58h8blrsapn3d40n")))

(define-public crate-meap-0.2 (crate (name "meap") (vers "0.2.0") (deps (list (crate-dep (name "parse_duration") (req "^2.1.0") (default-features #t) (kind 2)))) (hash "0fsyhl54l1k0n7xcfg18i96saawds2xkdra8drskdxlzk3bg5xim")))

(define-public crate-meap-0.3 (crate (name "meap") (vers "0.3.0") (deps (list (crate-dep (name "parse_duration") (req "^2.1.0") (default-features #t) (kind 2)))) (hash "1l724h80pnhvy765zd3nzh1n0y8hii4bhl97b3d19hsm8z92s9v1")))

(define-public crate-meap-0.3 (crate (name "meap") (vers "0.3.1") (deps (list (crate-dep (name "parse_duration") (req "^2.1.0") (default-features #t) (kind 2)))) (hash "1zpfbdx61nvl21sm5zn1jcvvhby5j168wzzp3vss23vnbqzhamla")))

(define-public crate-meap-0.3 (crate (name "meap") (vers "0.3.2") (deps (list (crate-dep (name "parse_duration") (req "^2.1.0") (default-features #t) (kind 2)))) (hash "1vndjyxskdpb8cv8ivkns3mk16w85x5v3cwfbvdpq6pni78vjw3j")))

(define-public crate-meap-0.3 (crate (name "meap") (vers "0.3.3") (deps (list (crate-dep (name "parse_duration") (req "^2.1.0") (default-features #t) (kind 2)))) (hash "13v9w8gw3fvpmd6v7i7nl81xg2slm7i09kij9lln5hqqdgafhkp0")))

(define-public crate-meap-0.4 (crate (name "meap") (vers "0.4.0") (deps (list (crate-dep (name "parse_duration") (req "^2.1.0") (default-features #t) (kind 2)))) (hash "1mw09nmwajbwlax8rc0nn9ylf4hnnih9xxiyga9g6zaphjihfs0i")))

(define-public crate-meap-0.4 (crate (name "meap") (vers "0.4.1") (deps (list (crate-dep (name "parse_duration") (req "^2.1.0") (default-features #t) (kind 2)))) (hash "0ng2wy069gh67cbkcqhj7z02wij356p0s835ipqkz24z4ff4kymj")))

(define-public crate-meap-0.4 (crate (name "meap") (vers "0.4.2") (deps (list (crate-dep (name "parse_duration") (req "^2.1.0") (default-features #t) (kind 2)))) (hash "1jqkgwcd4wpz0acmshfgf8s4qy6and8alp5wxp8f1dg6jqc2jcbi")))

(define-public crate-meap-0.4 (crate (name "meap") (vers "0.4.3") (deps (list (crate-dep (name "parse_duration") (req "^2.1.0") (default-features #t) (kind 2)))) (hash "1z0xmq2rdyd8c4xs7v6nfc0fawbnnvkzzk3gf06rnjlwr940vnh0")))

(define-public crate-meap-0.4 (crate (name "meap") (vers "0.4.4") (deps (list (crate-dep (name "parse_duration") (req "^2.1.0") (default-features #t) (kind 2)))) (hash "00g23kiwdxkq6nxk8n0mgfb45qlhnl3g41xxhlfpk5m32y6lld7s")))

(define-public crate-meap-0.5 (crate (name "meap") (vers "0.5.0") (deps (list (crate-dep (name "parse_duration") (req "^2.1.0") (default-features #t) (kind 2)))) (hash "121cxg3iw7macvc9zskmb6f6ji6b82cih5ph4a09gz0k1chhjlyf")))

(define-public crate-meap-0.5 (crate (name "meap") (vers "0.5.1") (deps (list (crate-dep (name "parse_duration") (req "^2.1.0") (default-features #t) (kind 2)))) (hash "0icbdnvdmv4zdlgkzi2k60vsv2yillk2vscwjig8zzpxs2fbin1m")))

(define-public crate-meap-0.5 (crate (name "meap") (vers "0.5.2") (deps (list (crate-dep (name "parse_duration") (req "^2.1.0") (default-features #t) (kind 2)))) (hash "1b898yglr16838czmvl6vpmzcspbab2nr66z7m1nihp7nm0wkd6p")))

(define-public crate-meap-0.6 (crate (name "meap") (vers "0.6.0") (deps (list (crate-dep (name "parse_duration") (req "^2.1.0") (default-features #t) (kind 2)))) (hash "03aw8vbxhszxjvzwm7rf81f47x8bchba7da5pgnf52ck0mk8897m")))

(define-public crate-meap-0.7 (crate (name "meap") (vers "0.7.0") (deps (list (crate-dep (name "parse_duration") (req "^2.1.0") (default-features #t) (kind 2)))) (hash "0b3prmr7jqkkxp8j7ksarmzrn8bym1152k5h14f8kp3j4b0mfwgv")))

(define-public crate-meap-0.7 (crate (name "meap") (vers "0.7.1") (deps (list (crate-dep (name "parse_duration") (req "^2.1.0") (default-features #t) (kind 2)))) (hash "0c4kfb17kjm20hsnclsanmzb6vq10x93zwm2m6dg1rdwvz12ixj9")))

(define-public crate-meap-0.8 (crate (name "meap") (vers "0.8.0") (deps (list (crate-dep (name "parse_duration") (req "^2.1.0") (default-features #t) (kind 2)))) (hash "1bz1ms8hmzyxacm4bsvzp1v57759wh839rfwjnhg4z88r7ldaxc8")))

(define-public crate-meap-0.8 (crate (name "meap") (vers "0.8.1") (deps (list (crate-dep (name "parse_duration") (req "^2.1.0") (default-features #t) (kind 2)))) (hash "042x0bggs17ahiagkwxv4jxrmwjpd6vmh8qqwias4xrb0zbdg7b1")))

