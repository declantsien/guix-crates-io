(define-module (crates-io me ii) #:use-module (crates-io))

(define-public crate-meiid-0.1 (crate (name "meiid") (vers "0.1.0") (deps (list (crate-dep (name "chrono") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)))) (hash "1c94g93qh1bcmr9z3n92gpjr2mi1k34jqaj32bkfl44adwd8pl1g") (yanked #t)))

(define-public crate-meiid-0.1 (crate (name "meiid") (vers "0.1.1") (deps (list (crate-dep (name "chrono") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)))) (hash "00k902p346486wxjm231ap2j3g7iaynwcy8w1m0k68h8xphglyvb")))

(define-public crate-meiid-0.1 (crate (name "meiid") (vers "0.1.2") (deps (list (crate-dep (name "chrono") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)))) (hash "1nj7ji35xfbakzzd5l821iz4l6vbfjsbwzhvadh7fchl3y7qdjgf")))

