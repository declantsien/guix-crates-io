(define-module (crates-io me mx) #:use-module (crates-io))

(define-public crate-memx-0.1 (crate (name "memx") (vers "0.1.0") (deps (list (crate-dep (name "criterion") (req "^0.3") (default-features #t) (kind 2)))) (hash "03pmcrsdl3rfxx647wjvhriapbpv4vhjvx31rlqhq5xbhsvk6nny") (features (quote (("default") ("cmp_bytes"))))))

(define-public crate-memx-0.1 (crate (name "memx") (vers "0.1.1") (deps (list (crate-dep (name "criterion") (req "^0.3") (default-features #t) (kind 2)))) (hash "08vy45bjvadrgkpza8j2mmy2s400868ldi2bqffby6qqlp9r5727") (features (quote (("default") ("cmp_bytes"))))))

(define-public crate-memx-0.1 (crate (name "memx") (vers "0.1.2") (deps (list (crate-dep (name "criterion") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "memchr") (req "^2.4") (default-features #t) (kind 2)))) (hash "0i4kcz0ss1xmcgc5lgkw1ay003ywlzblvpa7g2h8afwax7zii1aa") (features (quote (("default") ("cmp_bytes"))))))

(define-public crate-memx-0.1 (crate (name "memx") (vers "0.1.3") (deps (list (crate-dep (name "criterion") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "memchr") (req "^2.4") (default-features #t) (kind 2)))) (hash "1yhi3ry7qllgdm6j8jyw7vkv2l4yk8wsy7z6kqld527yps0h4bvh") (features (quote (("default") ("cmp_bytes"))))))

(define-public crate-memx-0.1 (crate (name "memx") (vers "0.1.4") (deps (list (crate-dep (name "criterion") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "memchr") (req "^2.4") (default-features #t) (kind 2)))) (hash "1sx2f9n82fypsz5kaqh57i40kcw1mbs40pcrv62zziyfmxqjsiza") (features (quote (("default") ("cmp_bytes"))))))

(define-public crate-memx-0.1 (crate (name "memx") (vers "0.1.5") (deps (list (crate-dep (name "criterion") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "memchr") (req "^2.4") (default-features #t) (kind 2)))) (hash "1k87fanka48rb9smmkjqqiws46028pa1nghlg2q99c6gkf4zqnpa") (features (quote (("default") ("cmp_bytes"))))))

(define-public crate-memx-0.1 (crate (name "memx") (vers "0.1.6") (deps (list (crate-dep (name "criterion") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "memchr") (req "^2.4") (default-features #t) (kind 2)))) (hash "0x7f61pg2h1ms2dl7v3qq6d65z7phfm77hzan2ikzxvm5m4jshmb") (features (quote (("default") ("cmp_bytes"))))))

(define-public crate-memx-0.1 (crate (name "memx") (vers "0.1.7") (deps (list (crate-dep (name "criterion") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "memchr") (req "^2.4") (default-features #t) (kind 2)))) (hash "0j8fwyj7dzm4hnahzbx84i90l8m7j7gyq0fsq4i3c7ddi50rc7di") (features (quote (("default") ("cmp_bytes"))))))

(define-public crate-memx-0.1 (crate (name "memx") (vers "0.1.8") (deps (list (crate-dep (name "criterion") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "memchr") (req "^2.4") (default-features #t) (kind 2)))) (hash "1kygjccx0ffhby8x7chp5hws4nshwj6f4msmf47r84fsz4j5ynwk") (features (quote (("std") ("default" "std") ("cmp_bytes"))))))

(define-public crate-memx-0.1 (crate (name "memx") (vers "0.1.9") (deps (list (crate-dep (name "criterion") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "memchr") (req "^2.4") (default-features #t) (kind 2)))) (hash "0k3xclwx6bsm3ffk3wrssi401qq43p60v6m6fbhj39q9dbl8q5al") (features (quote (("std") ("default" "std") ("cmp_bytes"))))))

(define-public crate-memx-0.1 (crate (name "memx") (vers "0.1.10") (deps (list (crate-dep (name "criterion") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "memchr") (req "^2.4") (default-features #t) (kind 2)))) (hash "1v7wvv4ngsxs4pxqhv2vvljqiv9j3awv4ryn93kmjbf3l5qxh117") (features (quote (("std") ("default" "std") ("cmp_bytes"))))))

(define-public crate-memx-0.1 (crate (name "memx") (vers "0.1.12") (hash "1iqz82j3lmwapbnc9fvgyq7j8kp4vrisqvq8zayml13i0vl5nn60") (features (quote (("std") ("default" "std") ("cmp_bytes"))))))

(define-public crate-memx-0.1 (crate (name "memx") (vers "0.1.13") (hash "0cqag11809k13m733bzbb757x5qqyaw4xm7la254bcxpdn9mqzxm") (features (quote (("std") ("default" "std") ("cmp_bytes"))))))

(define-public crate-memx-0.1 (crate (name "memx") (vers "0.1.14") (deps (list (crate-dep (name "rustc_version") (req "^0.4") (default-features #t) (kind 1)) (crate-dep (name "rustversion") (req "^1.0") (default-features #t) (kind 2)))) (hash "0mf3kw6l2ygiapxv6q6787vs7k2znjryscfm8nv2x2aba0ilzsns") (features (quote (("std") ("default" "std"))))))

(define-public crate-memx-0.1 (crate (name "memx") (vers "0.1.15") (deps (list (crate-dep (name "rustc_version") (req "^0.4") (default-features #t) (kind 1)) (crate-dep (name "rustversion") (req "^1.0") (default-features #t) (kind 2)))) (hash "17s2mk3y08ai2gc4i8fy4yha4qfai39a2a5j3j3j4rzhw4y3131x") (features (quote (("std") ("default" "std"))))))

(define-public crate-memx-0.1 (crate (name "memx") (vers "0.1.16") (deps (list (crate-dep (name "rustc_version") (req "^0.4") (default-features #t) (kind 1)) (crate-dep (name "rustversion") (req "^1.0") (default-features #t) (kind 2)))) (hash "1vw7hvfvnhk15yllj8226mpgx4pa41cqxfbk0i561h4mfss80g75") (features (quote (("std") ("default" "std")))) (yanked #t)))

(define-public crate-memx-0.1 (crate (name "memx") (vers "0.1.17") (deps (list (crate-dep (name "rustc_version") (req "^0.4") (default-features #t) (kind 1)) (crate-dep (name "rustversion") (req "^1.0") (default-features #t) (kind 2)))) (hash "0nlwcqypnhm39cln4y361lpnpv6qdhs7nhmnnh3i2gzg9w7p4b0c") (features (quote (("std") ("default" "std"))))))

(define-public crate-memx-0.1 (crate (name "memx") (vers "0.1.18") (deps (list (crate-dep (name "rustc_version") (req "^0.4") (default-features #t) (kind 1)) (crate-dep (name "rustversion") (req "^1.0") (default-features #t) (kind 2)))) (hash "06p0597sra6jzwvjfnjphd9p5yirlrp2mfs9szr1h3mia74gwawh") (features (quote (("std") ("default" "std"))))))

(define-public crate-memx-0.1 (crate (name "memx") (vers "0.1.19") (deps (list (crate-dep (name "rustc_version") (req "^0.4") (default-features #t) (kind 1)) (crate-dep (name "rustversion") (req "^1.0") (default-features #t) (kind 2)))) (hash "1ky023qgvyd7pw9dpdb6c9rk7idamvm07rnq4xaidzafq0j59hg3") (features (quote (("std") ("default" "std"))))))

(define-public crate-memx-0.1 (crate (name "memx") (vers "0.1.20") (deps (list (crate-dep (name "rustc_version") (req "^0.4") (default-features #t) (kind 1)) (crate-dep (name "rustversion") (req "^1.0") (default-features #t) (kind 2)))) (hash "07v0r577idb3qyd2d5hyzp8yprvsk09fi4vzlb1lnkikwx7jad6q") (features (quote (("std") ("default" "std"))))))

(define-public crate-memx-0.1 (crate (name "memx") (vers "0.1.21") (deps (list (crate-dep (name "rustc_version") (req "^0.4") (default-features #t) (kind 1)) (crate-dep (name "rustversion") (req "^1.0") (default-features #t) (kind 2)))) (hash "0y79ns080jyjy9a44fiy1qmamd01cbsxzk5prrdik641gqhxyr3l") (features (quote (("std") ("default" "std"))))))

(define-public crate-memx-0.1 (crate (name "memx") (vers "0.1.22") (deps (list (crate-dep (name "cpufeatures") (req "^0.2") (default-features #t) (target "cfg(any(target_arch = \"aarch64\", target_arch = \"x86_64\", target_arch = \"x86\"))") (kind 0)) (crate-dep (name "rustc_version") (req "^0.4") (default-features #t) (kind 1)) (crate-dep (name "rustversion") (req "^1.0") (default-features #t) (kind 2)))) (hash "0rhg560mvdayn6hba26z0wqm2i2cfv3lbmc542vlwyj0gqpqrpcs") (features (quote (("default"))))))

(define-public crate-memx-0.1 (crate (name "memx") (vers "0.1.23") (deps (list (crate-dep (name "cpufeatures") (req "^0.2") (default-features #t) (target "cfg(any(target_arch = \"aarch64\", target_arch = \"x86_64\", target_arch = \"x86\"))") (kind 0)) (crate-dep (name "rustc_version") (req "^0.4") (default-features #t) (kind 1)) (crate-dep (name "rustversion") (req "^1.0") (default-features #t) (kind 2)))) (hash "1aigb2n7a4ks9c38b5s86cigb0266xvk97hibdrvwy9a7rqn8nmp") (features (quote (("default"))))))

(define-public crate-memx-0.1 (crate (name "memx") (vers "0.1.24") (deps (list (crate-dep (name "cpufeatures") (req "^0.2") (default-features #t) (target "cfg(any(target_arch = \"aarch64\", target_arch = \"x86_64\", target_arch = \"x86\"))") (kind 0)) (crate-dep (name "rustc_version") (req "^0.4") (default-features #t) (kind 1)) (crate-dep (name "rustversion") (req "^1.0") (default-features #t) (kind 2)))) (hash "001pqv4yypj8jyk5ziwgk7ibchksvznprlc4f02nrc37bkriycfd") (features (quote (("default")))) (rust-version "1.56.0")))

(define-public crate-memx-0.1 (crate (name "memx") (vers "0.1.25") (deps (list (crate-dep (name "cpufeatures") (req "^0.2") (default-features #t) (target "cfg(any(target_arch = \"aarch64\", target_arch = \"x86_64\", target_arch = \"x86\"))") (kind 0)) (crate-dep (name "rustc_version") (req "^0.4") (default-features #t) (kind 1)) (crate-dep (name "rustversion") (req "^1.0") (default-features #t) (kind 2)))) (hash "1yp2gv2b7x2hza6p1pq0i9gfv1gxm8psiazbm7a7p91cfxqa4lqq") (features (quote (("default")))) (rust-version "1.56.0")))

(define-public crate-memx-0.1 (crate (name "memx") (vers "0.1.26") (deps (list (crate-dep (name "cpufeatures") (req "^0.2") (default-features #t) (target "cfg(any(target_arch = \"aarch64\", target_arch = \"x86_64\", target_arch = \"x86\"))") (kind 0)) (crate-dep (name "rustc_version") (req "^0.4") (default-features #t) (kind 1)) (crate-dep (name "rustversion") (req "^1.0") (default-features #t) (kind 2)))) (hash "0lda79p1zwzxw6hdc1ad9dcgf6200ypkm7xacsd3083kpzndy744") (features (quote (("default")))) (rust-version "1.56.0")))

(define-public crate-memx-0.1 (crate (name "memx") (vers "0.1.27") (deps (list (crate-dep (name "cpufeatures") (req "^0.2") (default-features #t) (target "cfg(any(target_arch = \"aarch64\", target_arch = \"x86_64\", target_arch = \"x86\"))") (kind 0)) (crate-dep (name "rustc_version") (req "^0.4") (default-features #t) (kind 1)) (crate-dep (name "rustversion") (req "^1.0") (default-features #t) (kind 2)))) (hash "0c577h7n1isii2djf1f0g0s368grjql5qkyql3ifyip5dzfs96bf") (features (quote (("test_pointer_width_64") ("test_pointer_width_32") ("test_pointer_width_128") ("default")))) (rust-version "1.56.0")))

(define-public crate-memx-0.1 (crate (name "memx") (vers "0.1.28") (deps (list (crate-dep (name "cpufeatures") (req "^0.2") (default-features #t) (target "cfg(any(target_arch = \"aarch64\", target_arch = \"x86_64\", target_arch = \"x86\"))") (kind 0)) (crate-dep (name "rustc_version") (req "^0.4") (default-features #t) (kind 1)) (crate-dep (name "rustversion") (req "^1.0") (default-features #t) (kind 2)))) (hash "0n1j8mf6hhjhf0dd3k6rkgbdyg6waqfr6m4rimbykp6ad6052sbp") (features (quote (("test_pointer_width_64") ("test_pointer_width_32") ("test_pointer_width_128") ("default")))) (rust-version "1.56.0")))

(define-public crate-memx-0.1 (crate (name "memx") (vers "0.1.29") (deps (list (crate-dep (name "cfg-iif") (req "^0.2") (default-features #t) (kind 2)) (crate-dep (name "cpufeatures") (req "^0.2") (default-features #t) (target "cfg(any(target_arch = \"aarch64\", target_arch = \"x86_64\", target_arch = \"x86\"))") (kind 0)) (crate-dep (name "rustc_version") (req "^0.4") (default-features #t) (kind 1)) (crate-dep (name "rustversion") (req "^1.0") (default-features #t) (kind 2)) (crate-dep (name "x86-alignment-check") (req "^0.1") (default-features #t) (target "cfg(any(target_arch = \"x86_64\", target_arch = \"x86\"))") (kind 2)))) (hash "1pnwjndsbqsqgc1r8dssn4kp6jikkmihs1zyqdsknnpr1c69lhjz") (features (quote (("test_pointer_width_64" "test_pointer_width") ("test_pointer_width_32" "test_pointer_width") ("test_pointer_width_128" "test_pointer_width") ("test_pointer_width" "test") ("test_alignment_check" "test") ("test") ("default")))) (rust-version "1.56.0")))

(define-public crate-memx-0.1 (crate (name "memx") (vers "0.1.30") (deps (list (crate-dep (name "cfg-iif") (req "^0.2") (default-features #t) (kind 2)) (crate-dep (name "cpufeatures") (req "^0.2") (default-features #t) (target "cfg(any(target_arch = \"aarch64\", target_arch = \"x86_64\", target_arch = \"x86\"))") (kind 0)) (crate-dep (name "rustversion") (req "^1.0") (default-features #t) (kind 2)) (crate-dep (name "x86-alignment-check") (req "^0.1") (default-features #t) (target "cfg(any(target_arch = \"x86_64\", target_arch = \"x86\"))") (kind 2)))) (hash "1j1h3q3025vbqcna9n4yw5h3v153k4xf2yg0pbjdxpmfl8pmjjs9") (features (quote (("test_pointer_width_64" "test_pointer_width") ("test_pointer_width_32" "test_pointer_width") ("test_pointer_width_128" "test_pointer_width") ("test_pointer_width" "test") ("test_alignment_check" "test") ("test") ("default")))) (rust-version "1.56.0")))

(define-public crate-memx-cdy-0.1 (crate (name "memx-cdy") (vers "0.1.0") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "memx") (req "^0.1") (default-features #t) (kind 0)))) (hash "126fnnb2l64hpkp8b9hwdl9imc32cy5qrzjpqh941m5awbzixdsx") (features (quote (("default") ("debian_build"))))))

(define-public crate-memx-cdy-0.1 (crate (name "memx-cdy") (vers "0.1.1") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "memx") (req "^0.1") (default-features #t) (kind 0)))) (hash "0wni3m603j8c9mwjk6x58mp8zw1n2zfpcfbs2aqzzprwmrj349bx") (features (quote (("default") ("debian_build"))))))

(define-public crate-memx-cdy-0.1 (crate (name "memx-cdy") (vers "0.1.2") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "memx") (req "^0.1") (default-features #t) (kind 0)))) (hash "0xicx794v1wcia03754n9xxl0z7b6wdh7lq5jbsrzc68wbw91s43") (features (quote (("default") ("debian_build"))))))

(define-public crate-memx-cdy-0.1 (crate (name "memx-cdy") (vers "0.1.3") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "memx") (req "^0.1") (default-features #t) (kind 0)))) (hash "17q929rkvr99qc2zpi7h7a72ywv136d43s4m8csknxh9vlv4rxq5") (features (quote (("default") ("debian_build"))))))

(define-public crate-memx-cdy-0.1 (crate (name "memx-cdy") (vers "0.1.4") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "memx") (req "^0.1") (default-features #t) (kind 0)))) (hash "0bpisfzb669a1slvsz8vaccrz25rmf6sqgik9xw0xybikmy9y8cd") (features (quote (("default") ("debian_build"))))))

(define-public crate-memx-cdy-0.1 (crate (name "memx-cdy") (vers "0.1.5") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "memx") (req "^0.1") (default-features #t) (kind 0)))) (hash "08pganb5h9zsvdply1hyrlpsr2iar7jrdrivd1z7fzs6076hp61m") (features (quote (("default") ("debian_build")))) (yanked #t)))

(define-public crate-memx-cdy-0.1 (crate (name "memx-cdy") (vers "0.1.6") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "memx") (req "^0.1") (default-features #t) (kind 0)))) (hash "1l64280nwhhmr1r8nyr5mkg43ly1nqgy4wj5z5bcmzl42cq1qjjj") (features (quote (("no_memcpy") ("default" "no_memcpy") ("debian_build"))))))

(define-public crate-memx-cdy-0.1 (crate (name "memx-cdy") (vers "0.1.7") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "memx") (req "^0.1") (default-features #t) (kind 0)))) (hash "0jc9ra2a10zfzq1hax2xczrk5jsf64mwa4lghxh7d1i4kjd3ipg3") (features (quote (("no_memcpy") ("default" "no_memcpy") ("debian_build"))))))

(define-public crate-memx-cdy-0.1 (crate (name "memx-cdy") (vers "0.1.8") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "memx") (req "^0.1") (default-features #t) (kind 0)))) (hash "1g9p1ymg4njcn63cj0jf8krsxqr7izkq6i6whssg798ankgf7bdm") (features (quote (("no_memcpy") ("default" "no_memcpy") ("debian_build"))))))

(define-public crate-memx-cdy-0.1 (crate (name "memx-cdy") (vers "0.1.9") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "memx") (req "^0.1") (default-features #t) (kind 0)))) (hash "0gna4v7gpxy2qxxb8qjdn2rml0mx4h5sf7wp6m6nc546h6pmcyj2") (features (quote (("no_memcpy") ("default" "no_memcpy") ("debian_build"))))))

(define-public crate-memx-cdy-0.1 (crate (name "memx-cdy") (vers "0.1.10") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "memx") (req "^0.1") (default-features #t) (kind 0)))) (hash "0sj8jl7f62ll89jlxca1mx7gsfhklqf3jx9qli3rryig0za36czd") (features (quote (("no_memcpy") ("default" "no_memcpy") ("debian_build"))))))

(define-public crate-memx-cdy-0.1 (crate (name "memx-cdy") (vers "0.1.11") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "memx") (req "^0.1") (default-features #t) (kind 0)))) (hash "07rk7sanip6nlyg6z8yaadi6140gp51nf3fp9wq8vlajzvsd1i50") (features (quote (("no_memcpy") ("default" "no_memcpy") ("debian_build")))) (rust-version "1.56.0")))

(define-public crate-memx-cdy-0.1 (crate (name "memx-cdy") (vers "0.1.12") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "memx") (req "^0.1") (default-features #t) (kind 0)))) (hash "0cg2bx35f4vvzr4m9mwxd579ja212q0js9xlw50d1a1r2pqslghw") (features (quote (("no_memcpy") ("default" "no_memcpy") ("debian_build")))) (rust-version "1.56.0")))

(define-public crate-memx-cdy-0.1 (crate (name "memx-cdy") (vers "0.1.13") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "memx") (req "^0.1") (default-features #t) (kind 0)))) (hash "1gfy18m97br7896py04y2548dmaiyl7rj4xk592r0xkc6cq8lp21") (features (quote (("no_memcpy") ("default" "no_memcpy") ("debian_build")))) (rust-version "1.56.0")))

