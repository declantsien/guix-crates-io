(define-module (crates-io me rm) #:use-module (crates-io))

(define-public crate-mermaid-0.1 (crate (name "mermaid") (vers "0.1.0") (hash "0sdwbbjs6ca3n352zyjm9yz05dhpwywa1yqiv4ag6q3dwdl12rl0")))

(define-public crate-mermaid-0.2 (crate (name "mermaid") (vers "0.2.0") (hash "00glszz4ls49zl38l19x9hj59yji2kdgvwnjdig9xm40a8i2d2dh")))

(define-public crate-mermaid-markdown-api-0.1 (crate (name "mermaid-markdown-api") (vers "0.1.0") (deps (list (crate-dep (name "enum-as-inner") (req "^0.4.0") (default-features #t) (kind 0)) (crate-dep (name "scanner-syn") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "strum") (req "^0.24") (default-features #t) (kind 0)) (crate-dep (name "strum_macros") (req "^0.24") (default-features #t) (kind 0)))) (hash "1cyiink7akfmxg79xh7nbv3q5kdl9xn88dzzc7za3crqj4wx2yr5")))

(define-public crate-mermaid-markdown-api-0.2 (crate (name "mermaid-markdown-api") (vers "0.2.0") (deps (list (crate-dep (name "enum-as-inner") (req "^0.4.0") (default-features #t) (kind 0)) (crate-dep (name "scanner-syn") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "strum") (req "^0.24") (default-features #t) (kind 0)) (crate-dep (name "strum_macros") (req "^0.24") (default-features #t) (kind 0)))) (hash "1k5a0rhr25a2w5sxxf8wsc0l9d8h9dn94bkh5ikd1i16lnkhh9pk")))

(define-public crate-mermaid-wasmbind-0.1 (crate (name "mermaid-wasmbind") (vers "0.1.0") (deps (list (crate-dep (name "serde") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "wasm-bindgen") (req "^0.2") (features (quote ("serde-serialize"))) (default-features #t) (kind 0)) (crate-dep (name "wasm-bindgen-test") (req "^0.3.0") (default-features #t) (kind 2)) (crate-dep (name "web-sys") (req "^0.3") (features (quote ("Element"))) (default-features #t) (kind 0)))) (hash "0c9bsyrc3gwzgd1grbrsjz0a8ngawx7jyn2b6p5czp1rxgpl837w")))

(define-public crate-mermer-rs-0.1 (crate (name "mermer-rs") (vers "0.1.0") (hash "04acasp71yd3wv76xwhzcis3j7b8id01r3gd30d2fzxzdpl2jp5a")))

