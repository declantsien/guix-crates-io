(define-module (crates-io me rs) #:use-module (crates-io))

(define-public crate-mers-0.3 (crate (name "mers") (vers "0.3.1") (deps (list (crate-dep (name "clap") (req "^4.3.19") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "mers_lib") (req "^0.3.1") (default-features #t) (kind 0)))) (hash "12bb11ypszhdkq3yj065555gk4f37npzr1f1h3cc8iyrcywmd0p6")))

(define-public crate-mers-0.3 (crate (name "mers") (vers "0.3.2") (deps (list (crate-dep (name "clap") (req "^4.3.19") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "mers_lib") (req "^0.3.2") (default-features #t) (kind 0)))) (hash "14njx3phyifv55zqj4bn2zpzm8hgc8xb1fnjigy7ap29655612wd")))

(define-public crate-mers-0.3 (crate (name "mers") (vers "0.3.3") (deps (list (crate-dep (name "clap") (req "^4.3.19") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "mers_lib") (req "^0.3.3") (default-features #t) (kind 0)))) (hash "1317xncjf4m2dbhycsp4v9zapwfgy0blx95bdnhi222mvqlcasri")))

(define-public crate-mers-0.3 (crate (name "mers") (vers "0.3.4") (deps (list (crate-dep (name "clap") (req "^4.3.19") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "mers_lib") (req "^0.3.4") (default-features #t) (kind 0)))) (hash "12y3357j0pjia6yf298l0jj30w6j7xc1nm409dbb9arvcknajsk1")))

(define-public crate-mers-0.3 (crate (name "mers") (vers "0.3.5") (deps (list (crate-dep (name "clap") (req "^4.3.19") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "mers_lib") (req "^0.3.4") (default-features #t) (kind 0)))) (hash "1gi1lqk8618934kmknbv76isa58sdj92jrnji1v2nw6qwnjwzz7b")))

(define-public crate-mers-0.3 (crate (name "mers") (vers "0.3.6") (deps (list (crate-dep (name "clap") (req "^4.3.19") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "mers_lib") (req "^0.3.5") (default-features #t) (kind 0)))) (hash "0gwjlrf0z13mb14vg558mj584sr1j2jxmx9w852cshksd1ngydl0")))

(define-public crate-mers-0.3 (crate (name "mers") (vers "0.3.7") (deps (list (crate-dep (name "clap") (req "^4.3.19") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "mers_lib") (req "^0.3.7") (default-features #t) (kind 0)))) (hash "0qqzcxh9dxlsbbgzdfd9amszbv2s74afs8ip0k58w9hsnsiws8sw")))

(define-public crate-mers-0.4 (crate (name "mers") (vers "0.4.0") (deps (list (crate-dep (name "clap") (req "^4.3.19") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "mers_lib") (req "^0.4.0") (default-features #t) (kind 0)))) (hash "01xivb2ky8f5p80yhljvs7ar6xvw08911cl1lilkxar36ad0499b")))

(define-public crate-mers-0.5 (crate (name "mers") (vers "0.5.0") (deps (list (crate-dep (name "clap") (req "^4.3.19") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "mers_lib") (req "^0.5.0") (default-features #t) (kind 0)))) (hash "1v8c2j5y2hw5wqli22ny1sn0gvrxaf055dnirh12ffxdl8n04cgj")))

(define-public crate-mers-0.6 (crate (name "mers") (vers "0.6.0") (deps (list (crate-dep (name "clap") (req "^4.3.19") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "colored") (req "^2.1.0") (default-features #t) (kind 0)) (crate-dep (name "mers_lib") (req "^0.6.0") (default-features #t) (kind 0)))) (hash "1nyiqmh3db28x4c8gvv8k7dbxwjbghnl0rc8jmyx4ll0anzcpda4")))

(define-public crate-mers-0.7 (crate (name "mers") (vers "0.7.0") (deps (list (crate-dep (name "clap") (req "^4.3.19") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "colored") (req "^2.1.0") (default-features #t) (kind 0)) (crate-dep (name "mers_lib") (req "^0.7.0") (default-features #t) (kind 0)))) (hash "0fyfr137rsny1x25rdas8qjrmdngilzyn4d9z5vjnd0y5dprlm9d")))

(define-public crate-mers-0.7 (crate (name "mers") (vers "0.7.3") (deps (list (crate-dep (name "clap") (req "^4.3.19") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "colored") (req "^2.1.0") (default-features #t) (kind 0)) (crate-dep (name "mers_lib") (req "^0.7.0") (default-features #t) (kind 0)))) (hash "1a9zmjf0ag9hypimqml78k24kyd7m5aka7zrd55knij1qma6018j")))

(define-public crate-mers-0.8 (crate (name "mers") (vers "0.8.0") (deps (list (crate-dep (name "clap") (req "^4.3.19") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "colored") (req "^2.1.0") (default-features #t) (kind 0)) (crate-dep (name "mers_lib") (req "^0.8.0") (default-features #t) (kind 0)))) (hash "0ck2d7mg5vg4h4ms21c7dill2x89y9gr69646iynxx7kmbaa0zn4")))

(define-public crate-mers-0.8 (crate (name "mers") (vers "0.8.1") (deps (list (crate-dep (name "clap") (req "^4.3.19") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "colored") (req "^2.1.0") (default-features #t) (kind 0)) (crate-dep (name "mers_lib") (req "^0.8.1") (default-features #t) (kind 0)))) (hash "0vpg4cpmhw9ihs87hr429vvrdfckbhnq7lrysh0l8cnl952c222d")))

(define-public crate-mers-0.8 (crate (name "mers") (vers "0.8.2") (deps (list (crate-dep (name "clap") (req "^4.3.19") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "colored") (req "^2.1.0") (default-features #t) (kind 0)) (crate-dep (name "mers_lib") (req "^0.8.2") (default-features #t) (kind 0)))) (hash "17sydhq00645jl3xl4zlfdlfksnkcpvjqxa1j0k4gyaf1qvpjrgg")))

(define-public crate-mers-0.8 (crate (name "mers") (vers "0.8.3") (deps (list (crate-dep (name "clap") (req "^4.3.19") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "colored") (req "^2.1.0") (default-features #t) (kind 0)) (crate-dep (name "mers_lib") (req "^0.8.3") (default-features #t) (kind 0)))) (hash "0ly21b8i0fb5gg9psxqdmrkkbhnx71sxc5ajqwwdibx4fbfv82rd")))

(define-public crate-mers_lib-0.3 (crate (name "mers_lib") (vers "0.3.1") (deps (list (crate-dep (name "colored") (req "^2.0.4") (default-features #t) (kind 0)) (crate-dep (name "line-span") (req "^0.1.5") (default-features #t) (kind 0)))) (hash "079a8kz0zli024yb480k5d8mx2gzd4jy34lywd92381hb6xl4kf2") (features (quote (("run") ("parse" "run") ("default" "parse"))))))

(define-public crate-mers_lib-0.3 (crate (name "mers_lib") (vers "0.3.2") (deps (list (crate-dep (name "colored") (req "^2.0.4") (default-features #t) (kind 0)) (crate-dep (name "line-span") (req "^0.1.5") (default-features #t) (kind 0)))) (hash "0jh200zxapyjjxzc8qx60x0wp8iv8nklk8812p79b0vy7n5pj2nk") (features (quote (("run") ("parse" "run") ("default" "parse"))))))

(define-public crate-mers_lib-0.3 (crate (name "mers_lib") (vers "0.3.3") (deps (list (crate-dep (name "colored") (req "^2.0.4") (default-features #t) (kind 0)) (crate-dep (name "line-span") (req "^0.1.5") (default-features #t) (kind 0)))) (hash "1fhnz6x7w4s2qigd9ywgpdf9fi6ddp507ika40cwmha376r95djb") (features (quote (("run") ("parse" "run") ("default" "parse"))))))

(define-public crate-mers_lib-0.3 (crate (name "mers_lib") (vers "0.3.4") (deps (list (crate-dep (name "colored") (req "^2.0.4") (default-features #t) (kind 0)) (crate-dep (name "line-span") (req "^0.1.5") (default-features #t) (kind 0)))) (hash "1s31wnlq7sb0zhmw5bf8pq0l780bpqp004znc26z6zmryh6vb6if") (features (quote (("run") ("parse" "run") ("default" "parse"))))))

(define-public crate-mers_lib-0.3 (crate (name "mers_lib") (vers "0.3.5") (deps (list (crate-dep (name "colored") (req "^2.0.4") (default-features #t) (kind 0)) (crate-dep (name "line-span") (req "^0.1.5") (default-features #t) (kind 0)))) (hash "03lxci5ibfg0h549qdhl64imjprs15zks7dpmdwvsq31qrqdd22d") (features (quote (("run") ("parse" "run") ("default" "parse"))))))

(define-public crate-mers_lib-0.3 (crate (name "mers_lib") (vers "0.3.7") (deps (list (crate-dep (name "colored") (req "^2.0.4") (default-features #t) (kind 0)) (crate-dep (name "line-span") (req "^0.1.5") (default-features #t) (kind 0)))) (hash "0m8qzdr4i3pv6rqw7lhrpxwyjyh9ghv926wkbmx9l6l36l2q3rzw") (features (quote (("run") ("parse" "run") ("default" "parse"))))))

(define-public crate-mers_lib-0.4 (crate (name "mers_lib") (vers "0.4.0") (deps (list (crate-dep (name "colored") (req "^2.0.4") (default-features #t) (kind 0)) (crate-dep (name "line-span") (req "^0.1.5") (default-features #t) (kind 0)))) (hash "16f9af0bwcb1gh8n0zyavhb6id6zi7cd6j0gzq7vil2yvachnn37") (features (quote (("run") ("parse" "run") ("default" "parse"))))))

(define-public crate-mers_lib-0.5 (crate (name "mers_lib") (vers "0.5.0") (deps (list (crate-dep (name "colored") (req "^2.0.4") (default-features #t) (kind 0)) (crate-dep (name "line-span") (req "^0.1.5") (default-features #t) (kind 0)))) (hash "1wnbf2ljjrvbs58l435kg10whg4a8b3dmava974z8xg6zqx7y0si") (features (quote (("run") ("parse" "run") ("default" "parse"))))))

(define-public crate-mers_lib-0.6 (crate (name "mers_lib") (vers "0.6.0") (deps (list (crate-dep (name "colored") (req "^2.0.4") (default-features #t) (kind 0)) (crate-dep (name "line-span") (req "^0.1.5") (default-features #t) (kind 0)))) (hash "0l098ln7xg91hz2i8s7h37zdcgh8rvbpdwkls3m1rns5g2daxym7") (features (quote (("run") ("parse" "run") ("default" "parse"))))))

(define-public crate-mers_lib-0.6 (crate (name "mers_lib") (vers "0.6.1") (deps (list (crate-dep (name "colored") (req "^2.0.4") (default-features #t) (kind 0)) (crate-dep (name "line-span") (req "^0.1.5") (default-features #t) (kind 0)))) (hash "1dwx4ryg210h7bgrxbkq2dy5r95x3xyzhxcmd94a96p61wsffhvv") (features (quote (("run") ("parse" "run") ("default" "parse"))))))

(define-public crate-mers_lib-0.7 (crate (name "mers_lib") (vers "0.7.0") (deps (list (crate-dep (name "colored") (req "^2.0.4") (default-features #t) (kind 0)) (crate-dep (name "line-span") (req "^0.1.5") (default-features #t) (kind 0)))) (hash "0983d9mkmmwav6fal304bch0fvg6mgmsswhdhk2brz8vvkg7qj1d") (features (quote (("run") ("parse" "run") ("default" "parse"))))))

(define-public crate-mers_lib-0.7 (crate (name "mers_lib") (vers "0.7.1") (deps (list (crate-dep (name "colored") (req "^2.0.4") (default-features #t) (kind 0)) (crate-dep (name "line-span") (req "^0.1.5") (default-features #t) (kind 0)))) (hash "0gkc7p8l1hkz28rh0p7m1nffn4h84nk39mll1jc3rybqlgznmjqn") (features (quote (("run") ("parse" "run") ("default" "parse"))))))

(define-public crate-mers_lib-0.7 (crate (name "mers_lib") (vers "0.7.2") (deps (list (crate-dep (name "colored") (req "^2.0.4") (default-features #t) (kind 0)) (crate-dep (name "line-span") (req "^0.1.5") (default-features #t) (kind 0)))) (hash "1jgi4kwvk61zy76nl0i2lwgagn8s372z5p38dh3g5qsz4xw4cklg") (features (quote (("run") ("parse" "run") ("default" "parse"))))))

(define-public crate-mers_lib-0.7 (crate (name "mers_lib") (vers "0.7.3") (deps (list (crate-dep (name "colored") (req "^2.0.4") (default-features #t) (kind 0)) (crate-dep (name "line-span") (req "^0.1.5") (default-features #t) (kind 0)))) (hash "0hcmqi7dszqifd8aa4ddsv465x6c41y9iv3q5aqkyf6lcj5xk6dx") (features (quote (("run") ("parse" "run") ("default" "parse"))))))

(define-public crate-mers_lib-0.8 (crate (name "mers_lib") (vers "0.8.0") (deps (list (crate-dep (name "colored") (req "^2.0.4") (default-features #t) (kind 0)) (crate-dep (name "line-span") (req "^0.1.5") (default-features #t) (kind 0)))) (hash "07ihhnh9766y3xxnjqhii7wwzn0hfgf826kl2kl0cn9r9fg3igq1") (features (quote (("run") ("parse" "run") ("default" "parse"))))))

(define-public crate-mers_lib-0.8 (crate (name "mers_lib") (vers "0.8.1") (deps (list (crate-dep (name "colored") (req "^2.0.4") (default-features #t) (kind 0)) (crate-dep (name "line-span") (req "^0.1.5") (default-features #t) (kind 0)))) (hash "1rgzwy2xjvh10xamh4zxk7i75rq27nx0p35cb2sz6y9mlf0jixm2") (features (quote (("run") ("parse" "run") ("default" "parse"))))))

(define-public crate-mers_lib-0.8 (crate (name "mers_lib") (vers "0.8.2") (deps (list (crate-dep (name "colored") (req "^2.0.4") (default-features #t) (kind 0)) (crate-dep (name "line-span") (req "^0.1.5") (default-features #t) (kind 0)))) (hash "0siap9v71b2cqqxnw2a9j3gykd42d13f6s5vvcdgz00dabyb7dij") (features (quote (("run") ("parse" "run") ("default" "parse"))))))

(define-public crate-mers_lib-0.8 (crate (name "mers_lib") (vers "0.8.3") (deps (list (crate-dep (name "colored") (req "^2.0.4") (default-features #t) (kind 0)) (crate-dep (name "line-span") (req "^0.1.5") (default-features #t) (kind 0)))) (hash "1frvwwns5fgcxxw4x9n601v5sv63svgpl9hkrf1vp536fi8978kn") (features (quote (("run") ("parse" "run") ("default" "parse"))))))

(define-public crate-mersenne-prime-number-miller-rabin-0.1 (crate (name "mersenne-prime-number-miller-rabin") (vers "0.1.0") (deps (list (crate-dep (name "num-bigint") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2") (default-features #t) (kind 0)))) (hash "0vnfy5m535kfmk4srii06gkkjbh6q61qhq19jlzlvdgpiaf392kr")))

(define-public crate-mersenne-twister-m-0.1 (crate (name "mersenne-twister-m") (vers "0.1.0") (hash "0wax48a1zapl3m2hgq00h67hzhpxh0zlbc9igr41bp9bb3nzwym1")))

(define-public crate-mersenne-twister-m-0.1 (crate (name "mersenne-twister-m") (vers "0.1.1") (hash "1i1wxnrd9fl6cnrngphbmalp61vfbc4qvy9fr05w7qm0j7ln0g29")))

(define-public crate-mersenne-twister-m-0.2 (crate (name "mersenne-twister-m") (vers "0.2.0") (deps (list (crate-dep (name "byteorder") (req "^1.4.3") (default-features #t) (kind 0)) (crate-dep (name "rand_core") (req "^0.6.4") (default-features #t) (kind 0)))) (hash "0xdlzbvb2s04ibrwvzpjr2ix5jxs39spjfckx6cahkf9jg01dxyz")))

(define-public crate-mersenne-twister-m-0.3 (crate (name "mersenne-twister-m") (vers "0.3.0") (deps (list (crate-dep (name "rand_core") (req "^0.6.4") (optional #t) (default-features #t) (kind 0)))) (hash "19v11xq877nvia6xzv68h4kjjmqdpjssb32g4k4pkakr47b4d123") (v 2) (features2 (quote (("rand" "dep:rand_core"))))))

(define-public crate-mersenne_twister-0.1 (crate (name "mersenne_twister") (vers "0.1.0") (hash "06zqf717gsw48niscba8gimqb92wa9zpl39zqzaizsh2x2nyq8ic")))

(define-public crate-mersenne_twister-0.1 (crate (name "mersenne_twister") (vers "0.1.1") (hash "0lyf2p1k3nard2aqsl29myfhjbcx5qflardp4ykkjiqjymjcwdli")))

(define-public crate-mersenne_twister-0.1 (crate (name "mersenne_twister") (vers "0.1.2") (hash "1ij3071ykxl5vbzd257i5q3211l5819xh5yzpcpka0z49c9c2vfr")))

(define-public crate-mersenne_twister-0.1 (crate (name "mersenne_twister") (vers "0.1.3") (hash "0g4g2314bfc3ngh0vh2q53z5dpknbimyi9wa3hb8vd61xd21wds9")))

(define-public crate-mersenne_twister-0.1 (crate (name "mersenne_twister") (vers "0.1.4") (hash "0fbhzl90f0cydq7zacxd5hacbr24mil8k2p2hcm0ljm09bzpfisf")))

(define-public crate-mersenne_twister-0.1 (crate (name "mersenne_twister") (vers "0.1.5") (hash "133wvk12d9zav4803akp7gmbi9r065ahrbz4dhcx1ml25sq5v53f")))

(define-public crate-mersenne_twister-0.2 (crate (name "mersenne_twister") (vers "0.2.0") (deps (list (crate-dep (name "rand") (req "^0.3.7") (default-features #t) (kind 0)))) (hash "168slr58qpw8dr54p3n17lqcbck22xn1b7c6dcpd72f26c4804h0")))

(define-public crate-mersenne_twister-0.3 (crate (name "mersenne_twister") (vers "0.3.0") (deps (list (crate-dep (name "rand") (req "^0.3") (default-features #t) (kind 0)))) (hash "0mfy6l96n3i5c15b60fxmcnj0ydbzgcmpmjz9rp4h9153v27nbb4")))

(define-public crate-mersenne_twister-1 (crate (name "mersenne_twister") (vers "1.0.0") (deps (list (crate-dep (name "rand") (req "^0.3") (default-features #t) (kind 0)))) (hash "1g83n706f381m9mrkgl81fnmzjybg0pglzp1y1r6mv80j1qql8is")))

(define-public crate-mersenne_twister-1 (crate (name "mersenne_twister") (vers "1.1.0") (deps (list (crate-dep (name "rand") (req "^0.3") (default-features #t) (kind 0)))) (hash "135b3r50p6iiaxzixpz3n9ibqnbl5gczgxsjrfcvii69b5xpmhsk")))

(define-public crate-mersenne_twister-1 (crate (name "mersenne_twister") (vers "1.1.1") (deps (list (crate-dep (name "rand") (req ">= 0.3, < 0.5") (default-features #t) (kind 0)))) (hash "1c0w7l2b7yi5nfis3fg88pp10sw9g5jikiglr2m2dh6zd0pvnpdq")))

