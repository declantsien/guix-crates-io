(define-module (crates-io me nt) #:use-module (crates-io))

(define-public crate-mental-poker-0.1 (crate (name "mental-poker") (vers "0.1.0") (deps (list (crate-dep (name "assert_matches") (req "^1.5") (default-features #t) (kind 2)) (crate-dep (name "derivative") (req "^2.2") (default-features #t) (kind 0)) (crate-dep (name "derive_more") (req "^0.99") (default-features #t) (kind 0)) (crate-dep (name "either") (req "^1.6") (default-features #t) (kind 0)) (crate-dep (name "either") (req "^1.6") (default-features #t) (kind 2)) (crate-dep (name "env_logger") (req "^0.9") (default-features #t) (kind 2)) (crate-dep (name "generic-array") (req "^0.14") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "mockall") (req "^0.11.0") (default-features #t) (kind 2)) (crate-dep (name "nom") (req "^7.1") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "sha2") (req "^0.10") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "test-log") (req "^0.2") (default-features #t) (kind 2)) (crate-dep (name "thiserror") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "unwrap-infallible") (req "^0.1") (default-features #t) (kind 2)))) (hash "18ar2fgyi5mc30wz987ww89nnyfjg44xysphb5hkv1krzrs5yvfl")))

(define-public crate-mentat-0.0.1 (crate (name "mentat") (vers "0.0.1") (deps (list (crate-dep (name "rand") (req "^0.6.3") (default-features #t) (kind 0)))) (hash "1gjf78c6npn4rh5w22fkym3pl40q1j6zhq2w4mznds73rzyvrlvp")))

(define-public crate-mentat-0.0.2 (crate (name "mentat") (vers "0.0.2") (deps (list (crate-dep (name "rand") (req "^0.6.4") (default-features #t) (kind 0)))) (hash "05sac1m484rv23008bh4m4djlxnhmqf9fypr7s610grll63mcvwa")))

(define-public crate-mentat-0.0.3 (crate (name "mentat") (vers "0.0.3") (deps (list (crate-dep (name "matplotrust") (req "^0.1.5") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.6.4") (default-features #t) (kind 0)))) (hash "01in1016j3gaiv1pxyq0wqd7yzwjnfcprvcnma4nwm40zxfcvy9f")))

(define-public crate-mentat-0.0.4 (crate (name "mentat") (vers "0.0.4") (deps (list (crate-dep (name "matplotrust") (req "^0.1.5") (default-features #t) (kind 2)) (crate-dep (name "probability") (req "^0.15.12") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.6.4") (default-features #t) (kind 0)))) (hash "091s83nz87apxh2y2xw5s3ipdm56a7284kaxwybbas42q0l415wz")))

(define-public crate-mentor-0.1 (crate (name "mentor") (vers "0.1.0") (hash "00dsrp8prx69lmyv02cvdvar36qz62g6zl09nfxr52rczjziih7j")))

(define-public crate-mentos-0.0.0 (crate (name "mentos") (vers "0.0.0") (hash "0mb07w2r96ydc8iy6wn3l4q9kdldm252gw336paayixbhxbq738i") (yanked #t)))

