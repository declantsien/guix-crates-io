(define-module (crates-io me ti) #:use-module (crates-io))

(define-public crate-meticulous-0.1 (crate (name "meticulous") (vers "0.1.0-pre.1") (hash "0qynwr5h0sb6z2m7b8hzfli06yds823zisbnlxd1s0p224mzsljd") (features (quote (("disallow-todo-on-release") ("default"))))))

(define-public crate-meticulous-0.1 (crate (name "meticulous") (vers "0.1.0") (deps (list (crate-dep (name "rand") (req "^0.8") (default-features #t) (kind 2)))) (hash "1sghzx8zn9qvm588gybljqrrc729f43lsqm0imii5mgvbk0rqj6g") (features (quote (("disallow-todo-on-release") ("default"))))))

(define-public crate-meticulous-0.2 (crate (name "meticulous") (vers "0.2.0-pre.1") (deps (list (crate-dep (name "rand") (req "^0.8") (default-features #t) (kind 2)))) (hash "0l73ln4chxyqqfh73cfkq6j0jxac4nl06p1q0c4g8prk44bdlz0v") (features (quote (("disallow-todo-on-release") ("default"))))))

(define-public crate-metio-0.1 (crate (name "metio") (vers "0.1.0") (deps (list (crate-dep (name "chrono") (req "^0.4.31") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.190") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.108") (default-features #t) (kind 2)))) (hash "0a565mj427mi4ihd4llc94c8mw8hayahmvfsxd3xh5qkw7lljzsl") (features (quote (("serde" "serde/derive" "chrono/serde"))))))

(define-public crate-metis-0.1 (crate (name "metis") (vers "0.1.0") (deps (list (crate-dep (name "metis-sys") (req "^0.1") (default-features #t) (kind 0)))) (hash "09k7maw4ms7b2iz7khal8g9nmb17djk3crkip4j1p2ls6sdybh0j")))

(define-public crate-metis-0.1 (crate (name "metis") (vers "0.1.1") (deps (list (crate-dep (name "metis-sys") (req "^0.1") (default-features #t) (kind 0)))) (hash "0p35d5rwkpcm56pfp1faf7ymvfcxwynxs9l2qf26sxiik514x74n")))

(define-public crate-metis-0.1 (crate (name "metis") (vers "0.1.2") (deps (list (crate-dep (name "metis-sys") (req "^0.2") (default-features #t) (kind 0)))) (hash "15y3h806x1668xjlv3i4wwhr8d38x4zlai4497jk9hamzmlm3mk2")))

(define-public crate-metis-0.2 (crate (name "metis") (vers "0.2.0") (deps (list (crate-dep (name "metis-sys") (req "^0.3") (kind 0)))) (hash "1hzvra2a694af9nb8w0sfg7bq2xacd7d4pxzrry46ma322v67ad8") (features (quote (("vendored" "metis-sys/vendored") ("use-system" "metis-sys/use-system") ("default" "metis-sys/default"))))))

(define-public crate-metis-0.2 (crate (name "metis") (vers "0.2.1") (deps (list (crate-dep (name "metis-sys") (req "^0.3") (kind 0)))) (hash "0pq4z1p3ww5597040hmj3fd3imlm9ark3g4pclkcvycvcpf05pm9") (features (quote (("vendored" "metis-sys/vendored") ("use-system" "metis-sys/use-system") ("default" "metis-sys/default"))))))

(define-public crate-metis-sys-0.1 (crate (name "metis-sys") (vers "0.1.0") (deps (list (crate-dep (name "bindgen") (req "^0.58") (kind 1)))) (hash "0npx72gn0nf8ryj1jl4wkr18xx6p6pkyqqyjf0b3grhk7x631915")))

(define-public crate-metis-sys-0.1 (crate (name "metis-sys") (vers "0.1.1") (deps (list (crate-dep (name "bindgen") (req "^0.58") (kind 1)))) (hash "0yxxcz0kfw6q11v1cphjdgdhiqz2q49mmalc3hvk9asyr1x94l5b")))

(define-public crate-metis-sys-0.2 (crate (name "metis-sys") (vers "0.2.0") (deps (list (crate-dep (name "bindgen") (req "^0.65") (kind 1)))) (hash "1xdskfbvalarqqlr3kzr1xwys555mgm2dkhjb4bacgb2bn2q1gs9")))

(define-public crate-metis-sys-0.2 (crate (name "metis-sys") (vers "0.2.1") (deps (list (crate-dep (name "bindgen") (req "^0.66") (features (quote ("runtime"))) (kind 1)))) (hash "0lqqz2v1b6lskdw4s241fbwd7vf7va9g1dalza544bnsz48qp2fc")))

(define-public crate-metis-sys-0.3 (crate (name "metis-sys") (vers "0.3.0") (deps (list (crate-dep (name "bindgen") (req "^0.69") (features (quote ("runtime"))) (optional #t) (kind 1)) (crate-dep (name "cc") (req "^1") (features (quote ("parallel"))) (optional #t) (default-features #t) (kind 1)))) (hash "00vysnmfjhkc3nyd8s0b15xwaywbks109bmammlbjx5lwzldwh9v") (features (quote (("use-system" "bindgen") ("generate-bindings" "vendored" "bindgen") ("default" "vendored")))) (v 2) (features2 (quote (("vendored" "dep:cc"))))))

(define-public crate-metis-sys-0.3 (crate (name "metis-sys") (vers "0.3.1") (deps (list (crate-dep (name "bindgen") (req "^0.69") (features (quote ("runtime"))) (optional #t) (kind 1)) (crate-dep (name "cc") (req "^1") (features (quote ("parallel"))) (optional #t) (default-features #t) (kind 1)))) (hash "10bmmb9m0vlkwmnq3rlcnsm0ff0k5g36v0q3wx6q142f8681hvmp") (features (quote (("use-system" "bindgen") ("generate-bindings" "vendored" "bindgen") ("force-optimize-vendor" "vendored") ("default" "vendored" "force-optimize-vendor")))) (v 2) (features2 (quote (("vendored" "dep:cc"))))))

