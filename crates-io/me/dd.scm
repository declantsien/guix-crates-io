(define-module (crates-io me dd) #:use-module (crates-io))

(define-public crate-meddl_translate-0.1 (crate (name "meddl_translate") (vers "0.1.0") (deps (list (crate-dep (name "rand") (req "^0.8.4") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.64") (default-features #t) (kind 0)))) (hash "07h43n4pfw0nhip4rdhyhgc4rkadm2h120c7w2lhf9lp3kl7zs06") (yanked #t)))

(define-public crate-meddl_translate-0.1 (crate (name "meddl_translate") (vers "0.1.1") (deps (list (crate-dep (name "rand") (req "^0.8.4") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.64") (default-features #t) (kind 0)))) (hash "0x1jly06qmgq3khp2bs67s2mybf4k9ank447imngzwi4mxwgnnfb")))

(define-public crate-meddl_translate-0.1 (crate (name "meddl_translate") (vers "0.1.2") (deps (list (crate-dep (name "rand") (req "^0.8.4") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.64") (default-features #t) (kind 0)))) (hash "17c0r9afd59vi0bf5pw7k0yxja3j833178j49sip1ss2bnchvsfb")))

(define-public crate-meddl_translate-0.1 (crate (name "meddl_translate") (vers "0.1.3") (deps (list (crate-dep (name "rand") (req "^0.8.4") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.64") (default-features #t) (kind 0)))) (hash "01ncjksyjncbgpwwkshqknj9w0px416xaip6vpp08iil0n09gka4")))

(define-public crate-meddl_translate-0.1 (crate (name "meddl_translate") (vers "0.1.4") (deps (list (crate-dep (name "rand") (req "^0.8.4") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.64") (default-features #t) (kind 0)))) (hash "1901zb56qq4dxpsq17hhvl5a1cab5b8xngwnp3p4gxhz9rm08f4w")))

(define-public crate-meddl_translate-0.2 (crate (name "meddl_translate") (vers "0.2.0") (deps (list (crate-dep (name "rand") (req "^0.8.4") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.64") (default-features #t) (kind 0)))) (hash "11mpm2jca3dwmmd5msl4rd7whjs0qkybn0vj75s84qa4xqb25fmn")))

(define-public crate-meddl_translate-0.2 (crate (name "meddl_translate") (vers "0.2.1") (deps (list (crate-dep (name "rand") (req "^0.8.4") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.64") (default-features #t) (kind 0)))) (hash "07bckfffrgcvfl370fnbrphsyxx8c4d6zyw8z8x21fj1klp8xa71")))

(define-public crate-meddl_translate-0.2 (crate (name "meddl_translate") (vers "0.2.2") (deps (list (crate-dep (name "rand") (req "^0.8.4") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.64") (default-features #t) (kind 0)))) (hash "08sw6vfh5fhxyrhfqx70xghbl23bq449h46a9kq9vpdsdp0r8lyb")))

(define-public crate-meddl_translate-0.2 (crate (name "meddl_translate") (vers "0.2.3") (deps (list (crate-dep (name "rand") (req "^0.8.4") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.64") (default-features #t) (kind 0)))) (hash "1f8x1w4xd7alpzlv05jnk1ky26rhhdsdadzl0rq7r6wnahbr3853") (features (quote (("interlude"))))))

(define-public crate-meddl_translate-0.2 (crate (name "meddl_translate") (vers "0.2.4") (deps (list (crate-dep (name "rand") (req "^0.8.4") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.64") (default-features #t) (kind 0)))) (hash "1dxj98d6ymvknmnm9qmbgbpdn60jg5lzjhvfrbbfbxfkpphjf5z5") (features (quote (("interlude"))))))

(define-public crate-meddl_translate-0.2 (crate (name "meddl_translate") (vers "0.2.5") (deps (list (crate-dep (name "rand") (req "^0.8.4") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.64") (default-features #t) (kind 0)))) (hash "0sj807d7vpy9pxb2kisr5b59gjcr4h6bq72798z61bx5wmd51ya0") (features (quote (("interlude")))) (yanked #t)))

(define-public crate-meddl_translate-0.2 (crate (name "meddl_translate") (vers "0.2.6") (deps (list (crate-dep (name "rand") (req "^0.8.4") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.64") (default-features #t) (kind 0)))) (hash "09x2yvb2sa149kpkp88qy4pkvmh6rd3j28r6dgvq3rilcygjlyxa") (features (quote (("interlude"))))))

(define-public crate-meddl_translate-0.2 (crate (name "meddl_translate") (vers "0.2.7") (deps (list (crate-dep (name "rand") (req "^0.8.4") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.64") (default-features #t) (kind 0)))) (hash "1jn4ny093qpggdaigvk8b3l5kfrih1n41inzvwv63hr8vjffdxzh") (features (quote (("interlude"))))))

(define-public crate-meddl_translate-0.2 (crate (name "meddl_translate") (vers "0.2.8") (deps (list (crate-dep (name "rand") (req "^0.8.4") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.64") (default-features #t) (kind 0)))) (hash "1mky9jk5w6pzbjknyrdqbvaf7k8rvhf7j817pm4102qrh2p9i3qc") (features (quote (("interlude"))))))

(define-public crate-meddl_translate-0.2 (crate (name "meddl_translate") (vers "0.2.9") (deps (list (crate-dep (name "rand") (req "^0.8.4") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.64") (default-features #t) (kind 0)))) (hash "0khl0igkq1pi9c7lzypn2j090kyhwc72pfxpaq93x5563z97j0f7") (features (quote (("interlude"))))))

(define-public crate-meddl_translate-0.3 (crate (name "meddl_translate") (vers "0.3.0") (deps (list (crate-dep (name "rand") (req "^0.8.4") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.64") (default-features #t) (kind 0)))) (hash "0y0w67pvz8wf055gjiann86j2gqdkfgpz9rr81ain0avchi1hrmk") (features (quote (("interlude"))))))

(define-public crate-meddl_translate-0.3 (crate (name "meddl_translate") (vers "0.3.1") (deps (list (crate-dep (name "rand") (req "^0.8.4") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.64") (default-features #t) (kind 0)))) (hash "0pm1jcad7s8nvrcd57s1vvsv8k3zv2xxgb21z7yb4mkn9px819a0") (features (quote (("interlude"))))))

(define-public crate-meddl_translate-0.4 (crate (name "meddl_translate") (vers "0.4.0") (deps (list (crate-dep (name "rand") (req "^0.8.4") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.64") (default-features #t) (kind 0)))) (hash "0lzlc2w4x9hb12g44nnmmwdpc9iimbr41yigl4yjj9ra779g29pv") (features (quote (("interlude"))))))

(define-public crate-meddl_translate-1 (crate (name "meddl_translate") (vers "1.0.0") (deps (list (crate-dep (name "rand") (req "^0.8.4") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.64") (default-features #t) (kind 0)))) (hash "0cgmrqzfdi38j2vj8092wyz7ahnscy6asxjx3ylg5vr5pmi3af62") (features (quote (("interlude")))) (yanked #t)))

(define-public crate-meddl_translate-1 (crate (name "meddl_translate") (vers "1.0.1") (deps (list (crate-dep (name "rand") (req "^0.8.4") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.64") (default-features #t) (kind 0)))) (hash "0mhivwlj9gs8nk0414prmk46y5b8dwfcm493ij5cp6f1al3ypyv3") (features (quote (("interlude")))) (yanked #t)))

(define-public crate-meddl_translate-1 (crate (name "meddl_translate") (vers "1.1.0") (deps (list (crate-dep (name "rand") (req "^0.8.4") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.64") (default-features #t) (kind 0)))) (hash "119faw203pv0m14cnf0hrnc1fvlq5bh43y49cb2pzq22rcck1ksh") (features (quote (("interlude")))) (yanked #t)))

(define-public crate-meddl_translate-1 (crate (name "meddl_translate") (vers "1.1.1") (deps (list (crate-dep (name "rand") (req "^0.8.4") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.64") (default-features #t) (kind 0)))) (hash "03h5mfkxi3s8x50i2w1jla6xlxnf9iqh4scbqqh7phn1jkmqn4n3") (features (quote (("interlude"))))))

(define-public crate-meddl_translate-1 (crate (name "meddl_translate") (vers "1.2.0") (deps (list (crate-dep (name "rand") (req "^0.8.4") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.64") (default-features #t) (kind 0)))) (hash "0z88hx4wkcjcc860j9sb51nqj22n35i5a2442xvxh0zv6ylrzswh") (features (quote (("interlude"))))))

(define-public crate-meddl_translate-1 (crate (name "meddl_translate") (vers "1.2.1") (deps (list (crate-dep (name "rand") (req "^0.8.4") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.64") (default-features #t) (kind 0)))) (hash "076c0yzm4fx3ygk6b44vx0hryy62agidh5b6kalfypwq2ncp0dkw") (features (quote (("interlude"))))))

