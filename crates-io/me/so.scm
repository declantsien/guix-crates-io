(define-module (crates-io me so) #:use-module (crates-io))

(define-public crate-meson-1 (crate (name "meson") (vers "1.0.0") (hash "149s6k33p46mybq6vjg5v9wj0v6v2drjvgw9qnkx0yj0d216hvny")))

(define-public crate-meson-next-1 (crate (name "meson-next") (vers "1.0.0") (hash "1db5a989v9frbari1p8xy6m9nh52s0d2wzbkrffv5hvyc8k9pifi")))

(define-public crate-meson-next-1 (crate (name "meson-next") (vers "1.0.1") (hash "0pvhvv2ipclhiwia9visqwvc9xdm1nhmxfzwv57a1jwm6b6hi16f")))

(define-public crate-meson-next-1 (crate (name "meson-next") (vers "1.1.0") (hash "1l3sy54yyi0946b5srs07xw5r2anbzz3qbqymcwji0mll4sqbjkh")))

(define-public crate-meson-next-1 (crate (name "meson-next") (vers "1.1.1") (hash "1lmvblqw16ni6pa6fgdacvfhpjbapdxvjisjf3apzjrn62dsxzja")))

(define-public crate-meson-next-1 (crate (name "meson-next") (vers "1.1.2") (hash "1bpglhjamwm7j1i3q6r6033x9vl1czz6h88807a24kcxw4x9l5hs")))

(define-public crate-meson-next-1 (crate (name "meson-next") (vers "1.1.3") (hash "1bn0f9zql9y8lsmaxaf6l2bi5z3yvqmmkdqfq8v78w702y1awzi8")))

(define-public crate-meson-next-1 (crate (name "meson-next") (vers "1.2.0") (hash "1pp3jviby4s341f3qnzych3j50w7z6zlwghypvz3n4f2dilmrj4n")))

(define-public crate-meson-next-1 (crate (name "meson-next") (vers "1.2.1") (hash "1gic2k1180k6v0xs9r20j1s197a8y2gr2dgmvgiwhfnp6jag9ang")))

(define-public crate-meson-next-1 (crate (name "meson-next") (vers "1.2.2") (hash "1q6jksxa90masdxwmkrc75jf1kfifpdx1zixywf9i0g5z9inib20")))

(define-public crate-mesos-0.2 (crate (name "mesos") (vers "0.2.3") (deps (list (crate-dep (name "hyper") (req "^0.6.15") (default-features #t) (kind 0)) (crate-dep (name "protobuf") (req "^1.0.4") (default-features #t) (kind 0)))) (hash "14qd7fh744z09360ckii53mg36zma2rghw8di3gz6wnb7f7kznp8")))

(define-public crate-mesos-0.2 (crate (name "mesos") (vers "0.2.4") (deps (list (crate-dep (name "hyper") (req "^0.6.15") (default-features #t) (kind 0)) (crate-dep (name "protobuf") (req "^1.0.4") (default-features #t) (kind 0)))) (hash "0g8irqpwn0q34jl8kj1in3bghc80k3smswcyb0779cmmziqdya4j")))

(define-public crate-mesos-0.2 (crate (name "mesos") (vers "0.2.5") (deps (list (crate-dep (name "hyper") (req "^0.6.15") (default-features #t) (kind 0)) (crate-dep (name "protobuf") (req "^1.0.4") (default-features #t) (kind 0)))) (hash "050ak8y8qfpaap9x9qn20ng6g1x6kski4bgk1nn1hxy5kaysipyb")))

(define-public crate-mesos-0.2 (crate (name "mesos") (vers "0.2.6") (deps (list (crate-dep (name "hyper") (req "^0.6.15") (default-features #t) (kind 0)) (crate-dep (name "itertools") (req "^0.4.3") (default-features #t) (kind 0)) (crate-dep (name "protobuf") (req "^1.0.4") (default-features #t) (kind 0)))) (hash "1r0gic0iv5brzdy16yxmx3kfv47x19q77jdrvprvzij9wlihcj69")))

(define-public crate-mesos-0.2 (crate (name "mesos") (vers "0.2.8") (deps (list (crate-dep (name "hyper") (req "^0.6.15") (default-features #t) (kind 0)) (crate-dep (name "itertools") (req "^0.4.3") (default-features #t) (kind 0)) (crate-dep (name "protobuf") (req "^1.0.4") (default-features #t) (kind 0)))) (hash "1qzxbqlck6iaisiqq35ps2y0hwz2l64m4ab9gafw2rfb77hxym0l")))

(define-public crate-mesos-0.2 (crate (name "mesos") (vers "0.2.9") (deps (list (crate-dep (name "hyper") (req "^0.6.15") (default-features #t) (kind 0)) (crate-dep (name "itertools") (req "^0.4.3") (default-features #t) (kind 0)) (crate-dep (name "protobuf") (req "^1.0.4") (default-features #t) (kind 0)))) (hash "1clfx914hp77rlpd28fpqnjl0g2xj2pjkk171a4xla5pmas41chh")))

(define-public crate-mesos-0.2 (crate (name "mesos") (vers "0.2.10") (deps (list (crate-dep (name "hyper") (req "^0.7.1") (default-features #t) (kind 0)) (crate-dep (name "itertools") (req "^0.4.3") (default-features #t) (kind 0)) (crate-dep (name "protobuf") (req "^1.0.10") (default-features #t) (kind 0)))) (hash "03lpqa5js3m4yg55y6i7njsl2v3842l4ajk6cn79gk1h95j6bx5b")))

