(define-module (crates-io me nb) #:use-module (crates-io))

(define-public crate-menbei-0.1 (crate (name "menbei") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^2.23.0") (default-features #t) (kind 0)) (crate-dep (name "failure") (req "^0.1.5") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.1.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_yaml") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "toml") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "url") (req "^1.7") (default-features #t) (kind 0)))) (hash "0xip60gbn809l4sfp48albkyy0s83npq98x4bwv3vdxgx82dch3g")))

