(define-module (crates-io me xs) #:use-module (crates-io))

(define-public crate-mexset-0.1 (crate (name "mexset") (vers "0.1.0") (deps (list (crate-dep (name "criterion") (req "^0.3.4") (default-features #t) (kind 2)) (crate-dep (name "num") (req "^0.4.0") (default-features #t) (kind 0)) (crate-dep (name "rustc-hash") (req "^1.1.0") (default-features #t) (kind 0)))) (hash "03xn80wb34z79mwnn9gkgph47j3gr450vygfng25ysy66s67w70q")))

