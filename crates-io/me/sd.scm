(define-module (crates-io me sd) #:use-module (crates-io))

(define-public crate-mesdoc-0.1 (crate (name "mesdoc") (vers "0.1.0") (deps (list (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.4.2") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.23") (default-features #t) (kind 0)))) (hash "0g7q7p0379q8dxv8qxg7akj0ndh5hg41aaljz3hmq1fqq75s96zb")))

(define-public crate-mesdoc-0.1 (crate (name "mesdoc") (vers "0.1.1") (deps (list (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.4.2") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.23") (default-features #t) (kind 0)))) (hash "1rw1r219np0myf88jbhnmapxvby54cw50aj2sc2pclxcmmlvvgkh")))

(define-public crate-mesdoc-0.1 (crate (name "mesdoc") (vers "0.1.2") (deps (list (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.4.2") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.23") (default-features #t) (kind 0)))) (hash "0hgl5vi5ra70sms069ygdzf0k724j7ndk226iiawl11y1rhdp1g0")))

(define-public crate-mesdoc-0.1 (crate (name "mesdoc") (vers "0.1.3") (deps (list (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.4.2") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.23") (default-features #t) (kind 0)))) (hash "1i8lny2w4z6w7y186yzzdlwmi2l98n78h0m35fkdla9ngilpkmp5")))

(define-public crate-mesdoc-0.1 (crate (name "mesdoc") (vers "0.1.4") (deps (list (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.4.2") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.23") (default-features #t) (kind 0)))) (hash "1i0jr6v8zv0cxg4sigqy0904wfqw2930gmlmskp9m80f0x8jwhsl")))

(define-public crate-mesdoc-0.1 (crate (name "mesdoc") (vers "0.1.5") (deps (list (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.4.2") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.23") (default-features #t) (kind 0)))) (hash "1y61mm22mdf7s51f9p4ldrjx62rm3czndg0la9dw3hvqj681ssfj")))

(define-public crate-mesdoc-0.1 (crate (name "mesdoc") (vers "0.1.6") (deps (list (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.4.2") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.23") (default-features #t) (kind 0)))) (hash "0hgp28mjv3x3sap4g0fdqa6v5gyrldpvl7jwb4rhxi79qkpp63gw")))

(define-public crate-mesdoc-0.1 (crate (name "mesdoc") (vers "0.1.7") (deps (list (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.4.2") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.23") (default-features #t) (kind 0)))) (hash "15svmhymm1yii8s9lx42s8150np0inmxq6hmzdvvyw2rgnv8ci36")))

(define-public crate-mesdoc-0.1 (crate (name "mesdoc") (vers "0.1.8") (deps (list (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.4.2") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.23") (default-features #t) (kind 0)))) (hash "0cqawx1wcm8g149ks9iq8khx8mhrdgaps7hanrbiz05jkdj1cm6v")))

(define-public crate-mesdoc-0.1 (crate (name "mesdoc") (vers "0.1.9") (deps (list (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.4.2") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.23") (default-features #t) (kind 0)))) (hash "1if57g4iga6k7w9absn55ggva2ffnxpx8gcqv8a9r1wc0vpnkq4j")))

(define-public crate-mesdoc-0.1 (crate (name "mesdoc") (vers "0.1.10") (deps (list (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.4.2") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.23") (default-features #t) (kind 0)))) (hash "0grwqdqf11nw9wsafvnxc93g8m0bb0333980wfnkxvpwxjb56j0i")))

(define-public crate-mesdoc-0.1 (crate (name "mesdoc") (vers "0.1.11") (deps (list (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.4.2") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.23") (default-features #t) (kind 0)))) (hash "0k9h48qvlljxifdywsc4wiy07lvvavwzq6jabsqhnirb2hah5lm9")))

(define-public crate-mesdoc-0.1 (crate (name "mesdoc") (vers "0.1.12") (deps (list (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.4.2") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.23") (default-features #t) (kind 0)))) (hash "0q8hm6xdij8zv0dwk7ggzk8xd16z5n0n5g3vwm9c20f61fda2hfr")))

(define-public crate-mesdoc-0.1 (crate (name "mesdoc") (vers "0.1.13") (deps (list (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.4.2") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.23") (default-features #t) (kind 0)))) (hash "13slghi6a5390rrqlklp5b80ms6kzvj8ifsl1vimi2hp2d9dfld2")))

(define-public crate-mesdoc-0.1 (crate (name "mesdoc") (vers "0.1.14") (deps (list (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.4.2") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.23") (default-features #t) (kind 0)))) (hash "1w4a1mzwmhlajrr5plr5gv03wy97nkn0mnpihq7r3sd7m6vfvaic")))

(define-public crate-mesdoc-0.1 (crate (name "mesdoc") (vers "0.1.15") (deps (list (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.4.2") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.23") (default-features #t) (kind 0)))) (hash "1s14hrm26a77vk7ia2045kkkx8907llap9aisx78d3r5yk66f5cp")))

(define-public crate-mesdoc-0.1 (crate (name "mesdoc") (vers "0.1.16") (deps (list (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.4.2") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.23") (default-features #t) (kind 0)))) (hash "1qpjm6aqip1g60lfcyg4krk298mgr6ic2fxkcl01fdb6z10n6ajz")))

(define-public crate-mesdoc-0.1 (crate (name "mesdoc") (vers "0.1.17") (deps (list (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.4.2") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.23") (default-features #t) (kind 0)))) (hash "1kb8cqzvh2ws3zm6sv32h1b2l6vrmwvgx0x6qmfvi5fh2pcy73wa")))

(define-public crate-mesdoc-0.1 (crate (name "mesdoc") (vers "0.1.18") (deps (list (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.4.2") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.23") (default-features #t) (kind 0)))) (hash "1aslk54s0dx2wpnswsbx7vdw1fin3316avwqanx1y54jm8pbs5xw")))

(define-public crate-mesdoc-0.1 (crate (name "mesdoc") (vers "0.1.19") (deps (list (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.4.2") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.23") (default-features #t) (kind 0)))) (hash "1g65cmylrpm7wpjdnyrk9qbkyjk3pfvp4m3fibpfl12c4sri9nfn")))

(define-public crate-mesdoc-0.1 (crate (name "mesdoc") (vers "0.1.20") (deps (list (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.4.2") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.23") (default-features #t) (kind 0)))) (hash "0bgi3533sx520v2znc76g2mqcnf5fhs0523gm2nbdz4p6n7j16q9")))

(define-public crate-mesdoc-0.1 (crate (name "mesdoc") (vers "0.1.21") (deps (list (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.4.2") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.23") (default-features #t) (kind 0)))) (hash "0czvig8xxqx6h6llmr2r6m6ksjadc2nspr08ys973kpxdw6y7rp3")))

(define-public crate-mesdoc-0.1 (crate (name "mesdoc") (vers "0.1.22") (deps (list (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.4.2") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.23") (default-features #t) (kind 0)))) (hash "0qccg4c1g6yfblpms4k2hlpcr21ki67rq04wdm9r7scjllmzjq7p")))

(define-public crate-mesdoc-0.1 (crate (name "mesdoc") (vers "0.1.23") (deps (list (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.4.2") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.23") (default-features #t) (kind 0)))) (hash "1ashzdlp5z3bkh25yifvb3ciqw07i67yl77zsisb13p5s4lvz50h")))

(define-public crate-mesdoc-0.1 (crate (name "mesdoc") (vers "0.1.24") (deps (list (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.4.2") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.23") (default-features #t) (kind 0)))) (hash "02ggdb3nsvg06kxva7wia3pi51ja45xjs44lwbdqw007k23ha4g7")))

(define-public crate-mesdoc-0.2 (crate (name "mesdoc") (vers "0.2.0") (deps (list (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.4.2") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.23") (default-features #t) (kind 0)))) (hash "0jx8w623dqj218rb2mlikp1dliqmibjdlm2v8l9x6w38zzkj2x7g")))

(define-public crate-mesdoc-0.2 (crate (name "mesdoc") (vers "0.2.1") (deps (list (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.4.2") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.23") (default-features #t) (kind 0)))) (hash "1x5nzc218ylakrfyya68qfnlbxxhzx0h2lw745zqkfmn54vbnidl")))

