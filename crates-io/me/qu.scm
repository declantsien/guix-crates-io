(define-module (crates-io me qu) #:use-module (crates-io))

(define-public crate-mequeue-0.0.0 (crate (name "mequeue") (vers "0.0.0") (hash "0pp27acm6qf3s2nynlz51pz366fy1mrm1v30lnln5qynab2prbqj")))

(define-public crate-mequeue-0.0.1 (crate (name "mequeue") (vers "0.0.1") (deps (list (crate-dep (name "async-channel") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "async-trait") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "parking_lot") (req "^0.12.1") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1") (features (quote ("rt" "sync"))) (default-features #t) (kind 0)))) (hash "0fci519wp989c11r51ysya8sdkrp120kkj4d3rnpb6b5xnaggfzb")))

