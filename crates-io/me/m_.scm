(define-module (crates-io me m_) #:use-module (crates-io))

(define-public crate-mem_64-0.1 (crate (name "mem_64") (vers "0.1.0") (hash "0yfriigr8ajasx3bn79sqn37lsanbr4bx5sy7yn9zijjfya92faf")))

(define-public crate-mem_btree-0.1 (crate (name "mem_btree") (vers "0.1.0") (deps (list (crate-dep (name "memory-stats") (req "^1.0.0") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 2)))) (hash "00gx9iqk2f3hb9n7rada3nxcyigpj926pfvz4pbfdvkq5c5x4slm")))

(define-public crate-mem_btree-0.2 (crate (name "mem_btree") (vers "0.2.0") (deps (list (crate-dep (name "memory-stats") (req "^1.0.0") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 2)))) (hash "1iwzkcnvjcav80bwax0h9nnaqbrzriz7lbvbrg31a68gimjf2zhh")))

(define-public crate-mem_btree-0.2 (crate (name "mem_btree") (vers "0.2.1") (deps (list (crate-dep (name "memory-stats") (req "^1.0.0") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 2)))) (hash "0d2mggr5i644ph3i3nf2m5c981ki38sgpzd55gppwq06hrgx7yw2")))

(define-public crate-mem_btree-0.2 (crate (name "mem_btree") (vers "0.2.2") (deps (list (crate-dep (name "memory-stats") (req "^1.0.0") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 2)))) (hash "01cyi8947cvsgk03k8n5r0yvdigsa91svzz4q9pzk79hxq1j5nxk")))

(define-public crate-mem_btree-0.3 (crate (name "mem_btree") (vers "0.3.0") (deps (list (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 2)))) (hash "0s0viy5dm7gz6r8g34fj56z6ipb3sbl9lcl0h5xkzrfhsqlggxia")))

(define-public crate-mem_btree-0.3 (crate (name "mem_btree") (vers "0.3.1") (deps (list (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 2)))) (hash "08ypswyv237a5b0isl98y4n04ws3fyi16j8v02g25s8wfcvrsxvq")))

(define-public crate-mem_cache-0.1 (crate (name "mem_cache") (vers "0.1.0") (deps (list (crate-dep (name "anyhow") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "tokio-test") (req "^0.4") (default-features #t) (kind 2)))) (hash "1k8vk419hwkayf3gwi179ik8sdbajyhlyk9iz0p5qiba1w38kx6v") (yanked #t)))

(define-public crate-mem_cache-0.1 (crate (name "mem_cache") (vers "0.1.1") (deps (list (crate-dep (name "anyhow") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "tokio-test") (req "^0.4") (default-features #t) (kind 2)))) (hash "0a9g4qz4vi0n03c6ynwzdrg66prg220lc6py1s6z7d25iq9r752k") (yanked #t)))

(define-public crate-mem_cache-0.1 (crate (name "mem_cache") (vers "0.1.2") (deps (list (crate-dep (name "anyhow") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "tokio-test") (req "^0.4") (default-features #t) (kind 2)))) (hash "1gi8vfmyh2xs6cc6icl475cmkhd21zr1wkha7q0rl93zamd3if1v")))

(define-public crate-mem_cmp-0.1 (crate (name "mem_cmp") (vers "0.1.0") (hash "10p9xv6z0w3ldj4zj8kac92rywimrsdhazxspkgpxn58aan1ilnk") (features (quote (("specialization") ("nightly" "specialization"))))))

(define-public crate-mem_cmp-0.1 (crate (name "mem_cmp") (vers "0.1.1") (hash "165gka00awxy3fwkmf0n5mgq7kdnfz6rny9ywna5p412flfik1zy") (features (quote (("specialization") ("nightly" "specialization"))))))

(define-public crate-mem_cmp-0.1 (crate (name "mem_cmp") (vers "0.1.2") (hash "0424wfr9wjfgw3miv44ang8jc3nbyyhi830hm2mm43ibvbyhgd5f") (features (quote (("specialization") ("nightly" "specialization"))))))

(define-public crate-mem_cmp-0.1 (crate (name "mem_cmp") (vers "0.1.3") (deps (list (crate-dep (name "simd") (req "^0.2.0") (optional #t) (default-features #t) (kind 0)))) (hash "0mb5pm7mx5qlr4mmj7bavyd250rnqlnbdmw2z21xap7csys04gys") (features (quote (("specialization") ("nightly" "specialization") ("avx" "simd"))))))

(define-public crate-mem_cmp-0.1 (crate (name "mem_cmp") (vers "0.1.4") (deps (list (crate-dep (name "simd") (req "^0.2.0") (optional #t) (default-features #t) (kind 0)))) (hash "0rqdyzr46ryksly1lb8c3z9m7fdy4hva3p506mdji699w0rf8afg") (features (quote (("specialization") ("nightly" "specialization") ("avx" "simd"))))))

(define-public crate-mem_dbg-0.1 (crate (name "mem_dbg") (vers "0.1.0") (deps (list (crate-dep (name "bitflags") (req "^2.4.1") (default-features #t) (kind 0)) (crate-dep (name "mem_dbg-derive") (req "=0.1.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "mmap-rs") (req "^0.6.0") (optional #t) (default-features #t) (kind 0)))) (hash "00qy4rnx7if6kbnawbmbhkdlpz95nn513ch43zll0j2x2915nnyg") (features (quote (("std" "alloc") ("derive" "mem_dbg-derive") ("default" "std" "mmap-rs" "derive") ("alloc"))))))

(define-public crate-mem_dbg-0.1 (crate (name "mem_dbg") (vers "0.1.1") (deps (list (crate-dep (name "bitflags") (req "^2.4.1") (default-features #t) (kind 0)) (crate-dep (name "mem_dbg-derive") (req "=0.1.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "mmap-rs") (req "^0.6.0") (optional #t) (default-features #t) (kind 0)))) (hash "0m9011j5y8l4l7m6d2zn6cp246ax28i2jhb323nhwq6p90m0wgg4") (features (quote (("std" "alloc") ("derive" "mem_dbg-derive") ("default" "std" "mmap-rs" "derive") ("alloc"))))))

(define-public crate-mem_dbg-0.1 (crate (name "mem_dbg") (vers "0.1.2") (deps (list (crate-dep (name "bitflags") (req "^2.4.1") (default-features #t) (kind 0)) (crate-dep (name "mem_dbg-derive") (req "=0.1.2") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "mmap-rs") (req "^0.6.0") (optional #t) (default-features #t) (kind 0)))) (hash "0qwr2r1qjdc4apf4zchdnsj6qnng3mjn04zk2d34kchfqvl4a9im") (features (quote (("std" "alloc") ("derive" "mem_dbg-derive") ("default" "std" "mmap-rs" "derive") ("alloc"))))))

(define-public crate-mem_dbg-0.1 (crate (name "mem_dbg") (vers "0.1.3") (deps (list (crate-dep (name "bitflags") (req "^2.4.1") (default-features #t) (kind 0)) (crate-dep (name "mem_dbg-derive") (req "=0.1.2") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "mmap-rs") (req "^0.6.0") (optional #t) (default-features #t) (kind 0)))) (hash "1ikz97n3p3bs1xli4i5g8kfafbnr7m79ca35djnfcb5awk1y2vx7") (features (quote (("std" "alloc") ("derive" "mem_dbg-derive") ("default" "std" "mmap-rs" "derive") ("alloc"))))))

(define-public crate-mem_dbg-0.1 (crate (name "mem_dbg") (vers "0.1.4") (deps (list (crate-dep (name "bitflags") (req "^2.4.1") (default-features #t) (kind 0)) (crate-dep (name "mem_dbg-derive") (req "=0.1.2") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "mmap-rs") (req "^0.6.0") (optional #t) (default-features #t) (kind 0)))) (hash "0nylw9d4863sh09kq2h218jhk67cpa38pbf02b7d4xc62wy2ckmp") (features (quote (("std" "alloc") ("derive" "mem_dbg-derive") ("default" "std" "mmap-rs" "derive") ("alloc"))))))

(define-public crate-mem_dbg-0.1 (crate (name "mem_dbg") (vers "0.1.5") (deps (list (crate-dep (name "bitflags") (req "^2.4.1") (default-features #t) (kind 0)) (crate-dep (name "mem_dbg-derive") (req "=0.1.2") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "mmap-rs") (req "^0.6.0") (optional #t) (default-features #t) (kind 0)))) (hash "0abgjr8l42wavmc18vmwy31vqhvj066x65x3df399fdld3dhqckv") (features (quote (("std" "alloc") ("derive" "mem_dbg-derive") ("default" "std" "mmap-rs" "derive") ("alloc"))))))

(define-public crate-mem_dbg-0.1 (crate (name "mem_dbg") (vers "0.1.6") (deps (list (crate-dep (name "bitflags") (req "^2.4.1") (default-features #t) (kind 0)) (crate-dep (name "half") (req "^2.0.4") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "maligned") (req "^0.2.1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "mem_dbg-derive") (req "=0.1.3") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "mmap-rs") (req "^0.6.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (features (quote ("small_rng"))) (optional #t) (default-features #t) (kind 0)))) (hash "0mbrxx7b998zvb3ggkm9j5bs20gma2i50755rfsfh98vvwn26i29") (features (quote (("std" "alloc") ("derive" "mem_dbg-derive") ("default" "std" "mmap-rs" "half" "rand" "maligned" "derive") ("alloc"))))))

(define-public crate-mem_dbg-0.1 (crate (name "mem_dbg") (vers "0.1.7") (deps (list (crate-dep (name "bitflags") (req "^2.4.1") (default-features #t) (kind 0)) (crate-dep (name "half") (req "^2.0.4") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "maligned") (req "^0.2.1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "mem_dbg-derive") (req "=0.1.3") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "mmap-rs") (req "^0.6.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (features (quote ("small_rng"))) (optional #t) (default-features #t) (kind 0)))) (hash "0a3n83c3nck4b8d2x03p4pxa5wj056ix68pk0pn03ziq9g8ximv3") (features (quote (("std" "alloc") ("derive" "mem_dbg-derive") ("default" "std" "mmap-rs" "half" "rand" "maligned" "derive") ("alloc"))))))

(define-public crate-mem_dbg-0.1 (crate (name "mem_dbg") (vers "0.1.8") (deps (list (crate-dep (name "bitflags") (req "^2.4.1") (default-features #t) (kind 0)) (crate-dep (name "half") (req "^2.0.4") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "maligned") (req "^0.2.1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "mem_dbg-derive") (req "=0.1.3") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "mmap-rs") (req "^0.6.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (features (quote ("small_rng"))) (optional #t) (default-features #t) (kind 0)))) (hash "0p17qq0akx7zkrria2l0wqxb1xmqvc059qkm1rf3pxp8ppgawl79") (features (quote (("std" "alloc") ("derive" "mem_dbg-derive") ("default" "std" "mmap-rs" "half" "rand" "maligned" "derive") ("alloc"))))))

(define-public crate-mem_dbg-0.2 (crate (name "mem_dbg") (vers "0.2.0") (deps (list (crate-dep (name "bitflags") (req "^2.4.1") (default-features #t) (kind 0)) (crate-dep (name "half") (req "^2.0.4") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "maligned") (req "^0.2.1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "mem_dbg-derive") (req "=0.1.4") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "mmap-rs") (req "^0.6.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (features (quote ("small_rng"))) (optional #t) (default-features #t) (kind 0)))) (hash "10wibkcc5a1hy1ipxaagfkaq3amizdbnwaz7kqpncpyilm5921xy") (features (quote (("std" "alloc") ("offset_of_enum") ("derive" "mem_dbg-derive") ("default" "std" "derive") ("alloc"))))))

(define-public crate-mem_dbg-0.2 (crate (name "mem_dbg") (vers "0.2.1") (deps (list (crate-dep (name "bitflags") (req "^2.4.1") (default-features #t) (kind 0)) (crate-dep (name "half") (req "^2.0.4") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "maligned") (req "^0.2.1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "mem_dbg-derive") (req "=0.1.4") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "mmap-rs") (req "^0.6.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (features (quote ("small_rng"))) (optional #t) (default-features #t) (kind 0)))) (hash "0x1vham3cpd1x26frvy3a4fvswr2vph4sxpzwyn81nd4dcx12ran") (features (quote (("std" "alloc") ("offset_of_enum") ("derive" "mem_dbg-derive") ("default" "std" "derive") ("alloc"))))))

(define-public crate-mem_dbg-derive-0.1 (crate (name "mem_dbg-derive") (vers "0.1.0") (deps (list (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2.0") (default-features #t) (kind 0)))) (hash "1jqgqzqmikdha69i0lxy83kknygw2qhr2q05lv3mxd1ilb8zhjqj")))

(define-public crate-mem_dbg-derive-0.1 (crate (name "mem_dbg-derive") (vers "0.1.2") (deps (list (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2.0") (default-features #t) (kind 0)))) (hash "063adl1sk1812yccvy4qp566impqyz73dqswhdpib9930wvsd43r")))

(define-public crate-mem_dbg-derive-0.1 (crate (name "mem_dbg-derive") (vers "0.1.3") (deps (list (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2.0") (default-features #t) (kind 0)))) (hash "18q9izy1995il7brn82sm628wbqdqkx28mkzngvk40z0mbf1fjkf")))

(define-public crate-mem_dbg-derive-0.1 (crate (name "mem_dbg-derive") (vers "0.1.4") (deps (list (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2.0") (default-features #t) (kind 0)))) (hash "00aykaliqqsr3isg6dmlmqsc87jx2c3237f6jy8b06w8xcwl9daq")))

(define-public crate-mem_file-0.1 (crate (name "mem_file") (vers "0.1.0") (deps (list (crate-dep (name "cfg-if") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (target "cfg(unix)") (kind 0)) (crate-dep (name "nix") (req "^0.10") (default-features #t) (target "cfg(unix)") (kind 0)) (crate-dep (name "winapi") (req "^0.3") (features (quote ("winbase" "handleapi" "fileapi" "memoryapi" "errhandlingapi" "synchapi"))) (default-features #t) (target "cfg(windows)") (kind 0)))) (hash "11naqc0psgh0pg26893s1yfnpvql63vfbdwx98x1a1jjz6ql5csi") (yanked #t)))

(define-public crate-mem_macros-0.1 (crate (name "mem_macros") (vers "0.1.0") (hash "0cipinkyccbbih56ccz0zxy7f2n2m9qmdy022qkhvxljlaza47wf")))

(define-public crate-mem_macros-0.1 (crate (name "mem_macros") (vers "0.1.1") (hash "14a70r9lska4l5xpnlh3jkb8z0wcgv33qg3hrfsa2d6pzndm8vr5")))

(define-public crate-mem_macros-0.1 (crate (name "mem_macros") (vers "0.1.2") (hash "0wfn3wzdn6i42mnhjcf1305xpd5qj4m7mwkjbm6mligxvh3c3830")))

(define-public crate-mem_macros-1 (crate (name "mem_macros") (vers "1.0.0") (hash "1x0zinj3i8m7c8nrkicwc643dmgr7aqyg2r125x4jpqq2q4wgc1i")))

(define-public crate-mem_macros-1 (crate (name "mem_macros") (vers "1.0.1") (hash "05x059m29hb20j9rh8ziy7pqvlw8dkz8123i8jx5x7s81baghcv5")))

(define-public crate-mem_println-0.1 (crate (name "mem_println") (vers "0.1.0") (hash "1r5mn3k7xkgqarx72p6wcy8gn3kcnr6zdgxj6p7yn4782dkx2dwd") (features (quote (("println_mem") ("default" "println_mem")))) (yanked #t) (rust-version "1.60.0")))

(define-public crate-mem_println-0.1 (crate (name "mem_println") (vers "0.1.1") (hash "1a52sjxdw10yfp1zdki7q492zqiqkj8gjcji1i7r2gr71biyms2n") (features (quote (("println_mem") ("default" "println_mem")))) (yanked #t) (rust-version "1.60.0")))

(define-public crate-mem_println-0.1 (crate (name "mem_println") (vers "0.1.2") (hash "0611fqac9w7cw9hf0kx8gqa7fahn0v66mil3ny0pk0qhindpwaa3") (features (quote (("println_mem") ("default" "println_mem")))) (yanked #t) (rust-version "1.60.0")))

(define-public crate-mem_println-0.1 (crate (name "mem_println") (vers "0.1.3") (hash "1lqf2y1pnclai891hw39b69fp9fdycnj1wqb24lzswgxf3ny7lq3") (features (quote (("println_mem") ("default" "println_mem")))) (yanked #t) (rust-version "1.60.0")))

(define-public crate-mem_println-0.1 (crate (name "mem_println") (vers "0.1.4") (hash "1znbr62h2pycrmalixx6g9p7k6400yc26rkwcjjx4c5a1najcgqs") (features (quote (("println_mem") ("default" "println_mem")))) (rust-version "1.60.0")))

(define-public crate-mem_println-0.1 (crate (name "mem_println") (vers "0.1.5") (hash "1sg7ysm2cdszidlfpi70nbkkggdqc2dxj21nqzmlm77hfmqvwnlg") (features (quote (("println_mem") ("get_size_of_val") ("default" "println_mem" "get_size_of_val")))) (rust-version "1.60.0")))

(define-public crate-mem_storage-0.1 (crate (name "mem_storage") (vers "0.1.0") (hash "1jj8afq4j98fzm5ajf03j4cpv8z9nrprs3gjzp4hyx00h15g9bl9")))

(define-public crate-mem_storage-0.1 (crate (name "mem_storage") (vers "0.1.1") (hash "1kiw25p3ydaw7wzd0nm0pkrm7j6hi5s04nvjg2jc8vic5pf28ili")))

(define-public crate-mem_tools-0.1 (crate (name "mem_tools") (vers "0.1.0") (deps (list (crate-dep (name "test_tools") (req "~0.1") (default-features #t) (kind 2)))) (hash "1p6yni1qy5ri0hb7b4mkr5ax85krxg4qq57p7l02h6lgg8kzk3f4") (features (quote (("use_std") ("use_alloc") ("full" "use_std" "use_alloc") ("default" "use_std"))))))

(define-public crate-mem_tools-0.1 (crate (name "mem_tools") (vers "0.1.1") (deps (list (crate-dep (name "test_tools") (req "~0.1") (default-features #t) (kind 2)))) (hash "0irifav33gml0bc36x48sq7rkc1rr6dp04cjl7w2066r7569l8zh") (features (quote (("use_std") ("use_alloc") ("full" "use_std" "use_alloc") ("default" "use_std"))))))

(define-public crate-mem_tools-0.2 (crate (name "mem_tools") (vers "0.2.0") (deps (list (crate-dep (name "test_tools") (req "~0.5.0") (default-features #t) (kind 2)))) (hash "16plyimiz7vfwnkrmafs2q23s3rsf1g3fr4npyj82cqf8rr0sj57") (features (quote (("use_alloc") ("no_std") ("full" "use_alloc" "enabled") ("enabled") ("default" "enabled"))))))

(define-public crate-mem_tools-0.3 (crate (name "mem_tools") (vers "0.3.0") (deps (list (crate-dep (name "test_tools") (req "~0.6.0") (default-features #t) (kind 2)))) (hash "0xa1ap45cwhwlsks7jsd86f4skzynpg8k9cmpi8639h0dsddras4") (features (quote (("use_alloc") ("no_std") ("full" "use_alloc" "enabled") ("enabled") ("default" "enabled"))))))

(define-public crate-mem_tools-0.4 (crate (name "mem_tools") (vers "0.4.0") (deps (list (crate-dep (name "test_tools") (req "~0.7.0") (default-features #t) (kind 2)))) (hash "0ir4dqghz4d5wf2m170mkgm0v3pmwhvfzxqzb0ha4l92sjbrd095") (features (quote (("use_alloc") ("no_std") ("full" "use_alloc" "enabled") ("enabled") ("default" "enabled"))))))

(define-public crate-mem_tools-0.5 (crate (name "mem_tools") (vers "0.5.0") (deps (list (crate-dep (name "test_tools") (req "~0.7.0") (default-features #t) (kind 2)))) (hash "1lvgq6wasdi9ayca64nc1y10abvifz0a0632rnkl1g13lwjfn0h0") (features (quote (("use_alloc") ("no_std") ("full" "use_alloc" "enabled") ("enabled") ("default" "enabled"))))))

(define-public crate-mem_tools-0.6 (crate (name "mem_tools") (vers "0.6.0") (deps (list (crate-dep (name "test_tools") (req "~0.8.0") (default-features #t) (kind 2)))) (hash "1wyr0wg7gab1qn4jl5as6z2g1k5rj4dz8qh4cx03hixffxsgysqj") (features (quote (("use_alloc" "no_std") ("no_std") ("full" "use_alloc" "enabled") ("enabled") ("default" "enabled"))))))

