(define-module (crates-io me tf) #:use-module (crates-io))

(define-public crate-metfairy_art-0.1 (crate (name "metfairy_art") (vers "0.1.0") (hash "1fkl19grv7iimh9yzpzhpbzgzwzns4lff7r4xqnmhxyabc6pnx9q")))

(define-public crate-metfor-0.2 (crate (name "metfor") (vers "0.2.0") (deps (list (crate-dep (name "failure") (req "^0.1.1") (default-features #t) (kind 0)) (crate-dep (name "failure_derive") (req "^0.1.1") (default-features #t) (kind 0)))) (hash "0jwch13j00l07syd833f4j3wqw7q7zcvybgxj2c76hn27gwiwlln")))

(define-public crate-metfor-0.2 (crate (name "metfor") (vers "0.2.1") (deps (list (crate-dep (name "failure") (req "^0.1.1") (default-features #t) (kind 0)) (crate-dep (name "failure_derive") (req "^0.1.1") (default-features #t) (kind 0)))) (hash "0lzvk365ybxh10zv6ln3kwpfklh865r4z3xg6j05d4yrsyv7friy")))

(define-public crate-metfor-0.2 (crate (name "metfor") (vers "0.2.2") (deps (list (crate-dep (name "failure") (req "^0.1.1") (default-features #t) (kind 0)) (crate-dep (name "failure_derive") (req "^0.1.1") (default-features #t) (kind 0)))) (hash "0z31hlxhmfc4v727jsb7gwinii5z1yps05pqr2haz9q6x6nm5x9x")))

(define-public crate-metfor-0.2 (crate (name "metfor") (vers "0.2.3") (deps (list (crate-dep (name "failure") (req "^0.1.1") (default-features #t) (kind 0)) (crate-dep (name "failure_derive") (req "^0.1.1") (default-features #t) (kind 0)))) (hash "122i5ghnmygczvdxrbbd1mppk4xn6ng72clvh09vvsz8s8m7xzw1")))

(define-public crate-metfor-0.2 (crate (name "metfor") (vers "0.2.4") (deps (list (crate-dep (name "failure") (req "^0.1.1") (default-features #t) (kind 0)) (crate-dep (name "failure_derive") (req "^0.1.1") (default-features #t) (kind 0)))) (hash "0svbwr76rs30399rzrlq442phvcw1lmn7a5nj2dkd4n6vss2hyg3")))

(define-public crate-metfor-0.3 (crate (name "metfor") (vers "0.3.0") (deps (list (crate-dep (name "failure") (req "^0.1.1") (default-features #t) (kind 0)) (crate-dep (name "failure_derive") (req "^0.1.1") (default-features #t) (kind 0)))) (hash "067zbyashcr8yplbd0si4kx7p6ls3kcp3kjyqnqrl5hym2bkggcj")))

(define-public crate-metfor-0.3 (crate (name "metfor") (vers "0.3.1") (deps (list (crate-dep (name "failure") (req "^0.1.1") (default-features #t) (kind 0)) (crate-dep (name "failure_derive") (req "^0.1.1") (default-features #t) (kind 0)))) (hash "0csyrrclw1dcw8pi83c6rs10vp5m62w7nmh4nqpjxf78dvzh529x")))

(define-public crate-metfor-0.3 (crate (name "metfor") (vers "0.3.2") (deps (list (crate-dep (name "failure") (req "^0.1.1") (default-features #t) (kind 0)) (crate-dep (name "failure_derive") (req "^0.1.1") (default-features #t) (kind 0)))) (hash "0dikascbf386zhnglagi8526grg73hl8zrcvh4cc44vsh4rqkcvd")))

(define-public crate-metfor-0.4 (crate (name "metfor") (vers "0.4.0") (hash "0iw6x99zkpn4v9slryb4g1wfsbkvcmmc197ss20sjil2fqf97amj")))

(define-public crate-metfor-0.4 (crate (name "metfor") (vers "0.4.1") (hash "15jvz695m0dhin679av1snrzrz8inqw2qyvi3y21idabmwd5p8vf")))

(define-public crate-metfor-0.5 (crate (name "metfor") (vers "0.5.0") (deps (list (crate-dep (name "optional") (req "^0.5.0") (optional #t) (default-features #t) (kind 0)))) (hash "0vil0779sbks9z2cr52r70rk501y4wf6vwbi6p8j9hm5yn3i1by6") (features (quote (("use_optional" "optional") ("default"))))))

(define-public crate-metfor-0.6 (crate (name "metfor") (vers "0.6.0") (deps (list (crate-dep (name "optional") (req "^0.5.0") (optional #t) (default-features #t) (kind 0)))) (hash "192jp3gxjin6m3jkqy0l29m94q34hxswb9fyp83hj8lh454wvbb9") (features (quote (("use_optional" "optional") ("default"))))))

(define-public crate-metfor-0.6 (crate (name "metfor") (vers "0.6.1") (deps (list (crate-dep (name "optional") (req "^0.5.0") (optional #t) (default-features #t) (kind 0)))) (hash "0sw1rx0v43s0ilb5yrwf0s78zhcx8kp1f50f0b8f19sayny5r0wf") (features (quote (("use_optional" "optional") ("default"))))))

(define-public crate-metfor-0.6 (crate (name "metfor") (vers "0.6.2") (deps (list (crate-dep (name "optional") (req "^0.5.0") (optional #t) (default-features #t) (kind 0)))) (hash "1759l0vhghgbl2i1gmg3zc12r3sc0ich17kxd45qpxnr96zkf905") (features (quote (("use_optional" "optional") ("default"))))))

(define-public crate-metfor-0.6 (crate (name "metfor") (vers "0.6.3") (deps (list (crate-dep (name "optional") (req "^0.5.0") (optional #t) (default-features #t) (kind 0)))) (hash "1ij9vv0471kazn3bncrm5mkb24zld8wh26lpcyynkr2mzvgbg37f") (features (quote (("use_optional" "optional") ("default"))))))

(define-public crate-metfor-0.7 (crate (name "metfor") (vers "0.7.0") (deps (list (crate-dep (name "optional") (req "^0.5.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0") (optional #t) (default-features #t) (kind 0)))) (hash "13d229a1cqacfbm6zn3chs6x020xjj42qmlfdmy989d3j2rvbcbv") (features (quote (("use_serde" "serde" "serde_derive") ("use_optional" "optional") ("default"))))))

(define-public crate-metfor-0.7 (crate (name "metfor") (vers "0.7.1") (deps (list (crate-dep (name "optional") (req "^0.5.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0") (optional #t) (default-features #t) (kind 0)))) (hash "1pbcjw385fj6h2ih0iik433rnnbich8l4izrdm7ydz9p94pdm3w0") (features (quote (("use_serde" "serde" "serde_derive") ("use_optional" "optional") ("default"))))))

(define-public crate-metfor-0.7 (crate (name "metfor") (vers "0.7.2") (deps (list (crate-dep (name "optional") (req "^0.5.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0") (optional #t) (default-features #t) (kind 0)))) (hash "0w4x9gmkws47hqgrh78idw5pqp0jsr7ln527zc1s4mx8gscjawl4") (features (quote (("use_serde" "serde" "serde_derive") ("use_optional" "optional") ("default"))))))

(define-public crate-metfor-0.7 (crate (name "metfor") (vers "0.7.3") (deps (list (crate-dep (name "optional") (req "^0.5.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0") (optional #t) (default-features #t) (kind 0)))) (hash "143wa5k985ylkpdf7dpx027xrxcrsqdwc66lbvp048hdpwv48qlg") (features (quote (("use_serde" "serde" "serde_derive") ("use_optional" "optional") ("default")))) (yanked #t)))

(define-public crate-metfor-0.7 (crate (name "metfor") (vers "0.7.4") (deps (list (crate-dep (name "optional") (req "^0.5.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0") (optional #t) (default-features #t) (kind 0)))) (hash "1raiq8775g0pf3bqikkk1mkf8mbibmx9n9iyqq7vcdwv710dv6k9") (features (quote (("use_serde" "serde" "serde_derive") ("use_optional" "optional") ("default"))))))

(define-public crate-metfor-0.7 (crate (name "metfor") (vers "0.7.5") (deps (list (crate-dep (name "optional") (req "^0.5.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0") (optional #t) (default-features #t) (kind 0)))) (hash "0z7rh5iy8qmbbjzj53ycn33sf3jcsadd82hshi81h6kbak3nljw3") (features (quote (("use_serde" "serde" "serde_derive") ("use_optional" "optional") ("default"))))))

(define-public crate-metfor-0.7 (crate (name "metfor") (vers "0.7.6") (deps (list (crate-dep (name "optional") (req "^0.5.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0") (optional #t) (default-features #t) (kind 0)))) (hash "03yvq97l4x7phkn3ww519dzqxaicv0nzi6fwzxini2kzmpv6i7i6") (features (quote (("use_serde" "serde" "serde_derive") ("use_optional" "optional") ("default"))))))

(define-public crate-metfor-0.8 (crate (name "metfor") (vers "0.8.1") (deps (list (crate-dep (name "criterion") (req "^0.3") (features (quote ("html_reports"))) (default-features #t) (kind 2)) (crate-dep (name "optional") (req "^0.5.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0") (optional #t) (default-features #t) (kind 0)))) (hash "02vbzzhvc4xnp3rh2nics6rfkp1gprlk2pf2xh5zd35qrlyx4msa") (features (quote (("use_serde" "serde" "serde_derive") ("use_optional" "optional") ("default"))))))

(define-public crate-metfor-0.8 (crate (name "metfor") (vers "0.8.2") (deps (list (crate-dep (name "criterion") (req "^0.3") (features (quote ("html_reports"))) (default-features #t) (kind 2)) (crate-dep (name "optional") (req "^0.5.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0") (optional #t) (default-features #t) (kind 0)))) (hash "0qfbf1yjsrrv4csafkka0kqcxf3wnzr2cysp1gia1ky5fxyb70jk") (features (quote (("use_serde" "serde" "serde_derive") ("use_optional" "optional") ("default"))))))

(define-public crate-metfor-0.8 (crate (name "metfor") (vers "0.8.3") (deps (list (crate-dep (name "criterion") (req "^0.3") (features (quote ("html_reports"))) (default-features #t) (kind 2)) (crate-dep (name "optional") (req "^0.5.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0") (optional #t) (default-features #t) (kind 0)))) (hash "0ilrnz1r0p8dxg8w19717cq69xk2vdpa9hmm33gih9bzbhl92r38") (features (quote (("use_serde" "serde" "serde_derive") ("use_optional" "optional") ("default"))))))

(define-public crate-metfor-0.9 (crate (name "metfor") (vers "0.9.0") (deps (list (crate-dep (name "criterion") (req "^0.3") (features (quote ("html_reports"))) (default-features #t) (kind 2)) (crate-dep (name "optional") (req "^0.5.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0") (optional #t) (default-features #t) (kind 0)))) (hash "1gpkwhgf016i8h5y40zcb055vazi22l5jm30z5p0sygx1bkirpaw") (features (quote (("use_serde" "serde" "serde_derive") ("use_optional" "optional") ("default"))))))

