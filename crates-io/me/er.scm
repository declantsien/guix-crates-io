(define-module (crates-io me er) #:use-module (crates-io))

(define-public crate-meerkat-0.1 (crate (name "meerkat") (vers "0.1.0") (hash "18w58x50axl4q3x8jscincijm9ajmm47cx0gm6rd1j0prg4avhqw") (yanked #t)))

(define-public crate-meerkat-0.1 (crate (name "meerkat") (vers "0.1.1") (hash "0mxk4nf4jwpicbj3i67vg6n6w39zdh6i806fmi63hc1lah0pym81") (yanked #t)))

(define-public crate-meerkat-cli-0.1 (crate (name "meerkat-cli") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^3") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "toml") (req "^0.5") (default-features #t) (kind 0)))) (hash "1p9h53h8yhqvi0wnsr0h0ah6vpgrwmjzdvb0gadfwqj2gjvp28l3")))

(define-public crate-meerkat-cli-0.1 (crate (name "meerkat-cli") (vers "0.1.1") (deps (list (crate-dep (name "clap") (req "^3") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "toml") (req "^0.5") (default-features #t) (kind 0)))) (hash "1gsshhgsmlsi6gsmrkkyz4hmnvv7ida84z3rw091lm8y66kv8m0w")))

