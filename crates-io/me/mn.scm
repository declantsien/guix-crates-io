(define-module (crates-io me mn) #:use-module (crates-io))

(define-public crate-memnom-0.1 (crate (name "memnom") (vers "0.1.0") (deps (list (crate-dep (name "sysinfo") (req "^0.5") (default-features #t) (kind 0)))) (hash "0zlgbyq9qlp17g4kvg7jklv8ql8byd9z2yy2h1w7z31p6zhh87az")))

(define-public crate-memnom-0.2 (crate (name "memnom") (vers "0.2.0") (deps (list (crate-dep (name "circular-queue") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "human-size") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "sysinfo") (req "^0.5") (default-features #t) (kind 0)))) (hash "1vqqaa9hi6c0ladqx5vg00zxh7icqmz4sr9938ibk8fqvj8bpc22")))

