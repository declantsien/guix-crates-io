(define-module (crates-io me we) #:use-module (crates-io))

(define-public crate-mewe-0.1 (crate (name "mewe") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^4.5.1") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.11.24") (default-features #t) (kind 0)) (crate-dep (name "scraper") (req "^0.19.0") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.36.0") (features (quote ("macros" "rt-multi-thread"))) (default-features #t) (kind 0)))) (hash "1cd35ar3v2frgspq70dw17vh0mldddqgn0qczyj5l8q83xabadiy")))

