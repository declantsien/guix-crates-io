(define-module (crates-io me tl) #:use-module (crates-io))

(define-public crate-metl-0.0.0 (crate (name "metl") (vers "0.0.0") (deps (list (crate-dep (name "cocoa") (req "^0.2.4") (default-features #t) (kind 0)) (crate-dep (name "core-foundation") (req "0.*") (default-features #t) (kind 0)) (crate-dep (name "metal-sys") (req "^0.0.0") (default-features #t) (kind 0)) (crate-dep (name "objc") (req "^0.1.8") (default-features #t) (kind 0)))) (hash "1fzi1gq1fja0nbfqv4kdndckiw1n4p551vsvwqhvf2ll0m4p70kb")))

