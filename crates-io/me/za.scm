(define-module (crates-io me za) #:use-module (crates-io))

(define-public crate-meza-0.1 (crate (name "meza") (vers "0.1.0") (hash "0bd3ka9id3phhrxfm93dcb1xaqm03bic6nlh8ra1j09sa1frja3n")))

(define-public crate-meza-0.2 (crate (name "meza") (vers "0.2.0") (hash "0brmn4gjmir5jdx3v2diy3y0syfv2hbj25kgpvkm95v55zg2i9vd")))

(define-public crate-meza-0.2 (crate (name "meza") (vers "0.2.1") (hash "0ccml2kia7hxn8vbfm8mcb85m3f4jfcdhwvbxzxp89nzpgrvmpz2")))

