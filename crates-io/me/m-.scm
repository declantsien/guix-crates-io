(define-module (crates-io me m-) #:use-module (crates-io))

(define-public crate-mem-aead-mrs-0.1 (crate (name "mem-aead-mrs") (vers "0.1.0") (deps (list (crate-dep (name "arrayref") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "byteorder") (req "^1.0") (kind 0)) (crate-dep (name "rand") (req "^0.5") (default-features #t) (kind 2)) (crate-dep (name "subtle") (req "^0.6") (kind 0)))) (hash "049vlirvmak0ql7mcdf0j1z667289i42jbs4drn5nwdiflwi0vh2")))

(define-public crate-mem-aead-mrs-0.1 (crate (name "mem-aead-mrs") (vers "0.1.1") (deps (list (crate-dep (name "arrayref") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "byteorder") (req "^1") (kind 0)) (crate-dep (name "rand") (req "^0.7") (default-features #t) (kind 2)) (crate-dep (name "subtle") (req "^2") (kind 0)))) (hash "0di4335rk9vpjfdvmzj29i7hxp5zaibk1l2zagkaln9rkm3x8211")))

(define-public crate-mem-db-0.0.0 (crate (name "mem-db") (vers "0.0.0") (hash "0q030w6xny2aivmi1gj1waprscs5fpcy374x5a1qbfiq7f8cncid")))

(define-public crate-mem-db-0.0.1 (crate (name "mem-db") (vers "0.0.1") (hash "1z16885l4vdmcwp8vkjpfcj2qapqc2hdyhkpd99qkffcygb119xc")))

(define-public crate-mem-db-0.0.2 (crate (name "mem-db") (vers "0.0.2") (hash "0nxbg1fcx22izasd48rdf3cvlw0p8xxa24qki21gmqa950rwdkpy")))

(define-public crate-mem-db-0.0.3 (crate (name "mem-db") (vers "0.0.3") (hash "0f6b0c50pc6krr98sw70xsjksqzb1iqbz951rb67lyqbbykql5fc")))

(define-public crate-mem-query-0.0.1 (crate (name "mem-query") (vers "0.0.1") (deps (list (crate-dep (name "either") (req "^1.5") (default-features #t) (kind 0)) (crate-dep (name "itertools") (req "^0.9.0") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "tylisp") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "0barad02zq35qdblnlxdykxpmncv292xwhwswwr852rhzimyynd1") (features (quote (("slow_tests") ("const" "tylisp/const"))))))

(define-public crate-mem-rs-0.1 (crate (name "mem-rs") (vers "0.1.0") (deps (list (crate-dep (name "windows") (req "^0.38.0") (features (quote ("Win32_Foundation" "Win32_System_Memory" "Win32_System_Diagnostics_Debug" "Win32_System_ProcessStatus" "Win32_System_Threading" "Win32_System_Console" "Win32_System_SystemServices" "Win32_System_LibraryLoader" "Win32_Security" "Win32_UI_Input_XboxController"))) (default-features #t) (kind 0)))) (hash "0ps2pp6l1s5yxmz18w9i80zzm5ns4sd7frgwzz5q1w16l9sw9qsq")))

(define-public crate-mem-rs-0.1 (crate (name "mem-rs") (vers "0.1.1") (deps (list (crate-dep (name "windows") (req "^0.38.0") (features (quote ("Win32_Foundation" "Win32_System_Memory" "Win32_System_Diagnostics_Debug" "Win32_System_ProcessStatus" "Win32_System_Threading" "Win32_System_Console" "Win32_System_SystemServices" "Win32_System_LibraryLoader" "Win32_Security" "Win32_UI_Input_XboxController"))) (default-features #t) (kind 0)))) (hash "0mvylcdjrfvh2rv8kjja6h4ywwpfykgqb0b2krj7b0xqf7xwdqkl")))

(define-public crate-mem-rs-0.1 (crate (name "mem-rs") (vers "0.1.2") (deps (list (crate-dep (name "windows") (req "^0.42.0") (features (quote ("Win32_Foundation" "Win32_System_Memory" "Win32_System_Diagnostics_Debug" "Win32_System_ProcessStatus" "Win32_System_Threading" "Win32_System_Console" "Win32_System_SystemServices" "Win32_System_LibraryLoader" "Win32_Security" "Win32_UI_Input_XboxController"))) (default-features #t) (kind 0)))) (hash "1baay446y4r2vhkzh2690jwql637ylrpqigxwpbnnm8zgk9hxaw8")))

(define-public crate-mem-rs-0.1 (crate (name "mem-rs") (vers "0.1.3") (deps (list (crate-dep (name "windows") (req "^0.42.0") (features (quote ("Win32_Foundation" "Win32_System_Memory" "Win32_System_Diagnostics_Debug" "Win32_System_ProcessStatus" "Win32_System_Threading" "Win32_System_Console" "Win32_System_SystemServices" "Win32_System_LibraryLoader" "Win32_Security" "Win32_UI_Input_XboxController"))) (default-features #t) (kind 0)))) (hash "0zl0nqpv5kq3422n8q9a20cxbmzbzzqmv9jh7y6sxz12w18paza7")))

(define-public crate-mem-rs-0.1 (crate (name "mem-rs") (vers "0.1.4") (deps (list (crate-dep (name "windows") (req "^0.56.0") (features (quote ("Win32_Foundation" "Win32_System_Memory" "Win32_System_Diagnostics_Debug" "Win32_System_ProcessStatus" "Win32_System_Threading" "Win32_System_Console" "Win32_System_SystemServices" "Win32_System_LibraryLoader" "Win32_Security" "Win32_UI_Input_XboxController"))) (default-features #t) (kind 0)))) (hash "11im6vbkcs6xyri4lkd7rccjh0rvrajqbzrlb4bg399773443sc1")))

(define-public crate-mem-rs-0.1 (crate (name "mem-rs") (vers "0.1.5") (deps (list (crate-dep (name "windows") (req "^0.56.0") (features (quote ("Win32_Foundation" "Win32_System_Memory" "Win32_System_Diagnostics_Debug" "Win32_System_ProcessStatus" "Win32_System_Threading" "Win32_System_Console" "Win32_System_SystemServices" "Win32_System_LibraryLoader" "Win32_Security" "Win32_UI_Input_XboxController"))) (default-features #t) (kind 0)))) (hash "03czaxwcdh2hw7y074wypl5ijs18dlh14jxkx45qih9bpzf4af0b")))

(define-public crate-mem-rs-0.1 (crate (name "mem-rs") (vers "0.1.6") (deps (list (crate-dep (name "windows") (req "^0.56.0") (features (quote ("Win32_Foundation" "Win32_System_Memory" "Win32_System_Diagnostics_Debug" "Win32_System_ProcessStatus" "Win32_System_Threading" "Win32_System_Console" "Win32_System_SystemServices" "Win32_System_LibraryLoader" "Win32_Security" "Win32_UI_Input_XboxController"))) (default-features #t) (kind 0)))) (hash "015d0a2281xx4f1a62klmyi9p6sa239ikh8b9bylx5kiiaxzgvdn")))

(define-public crate-mem-rs-0.1 (crate (name "mem-rs") (vers "0.1.7") (deps (list (crate-dep (name "windows") (req "^0.56.0") (features (quote ("Win32_Foundation" "Win32_System_Memory" "Win32_System_Diagnostics_Debug" "Win32_System_ProcessStatus" "Win32_System_Threading" "Win32_System_Console" "Win32_System_SystemServices" "Win32_System_LibraryLoader" "Win32_Security" "Win32_UI_Input_XboxController"))) (default-features #t) (kind 0)))) (hash "0y0g1lczlmx8bpi3jxyl9v991ks7shnians9y81p16ihi1bhnsbx")))

