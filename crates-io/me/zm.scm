(define-module (crates-io me zm) #:use-module (crates-io))

(define-public crate-mezmo-0.1 (crate (name "mezmo") (vers "0.1.0") (deps (list (crate-dep (name "gethostname") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.11.12") (features (quote ("blocking" "json"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.130") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "tokio-test") (req "^0.4.2") (default-features #t) (kind 2)))) (hash "1waaad3jjjn0454ancxfkxvhidrjp8n7jga8kn40a24k5y6zwr52")))

