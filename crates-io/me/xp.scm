(define-module (crates-io me xp) #:use-module (crates-io))

(define-public crate-mexprp-0.1 (crate (name "mexprp") (vers "0.1.0") (deps (list (crate-dep (name "failure") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "simplelog") (req "^0.5.1") (default-features #t) (kind 2)))) (hash "14w9b11xazqx5c748r0dhc75m3m8n99v4hfbhbm1zbkd39vi3p0w")))

(define-public crate-mexprp-0.2 (crate (name "mexprp") (vers "0.2.0") (deps (list (crate-dep (name "failure") (req "^0.1") (default-features #t) (kind 0)))) (hash "14nrsxg5p87x8qi04x68gwypd0li9dp5dm90ywzimk2bbi9130x8")))

(define-public crate-mexprp-0.3 (crate (name "mexprp") (vers "0.3.0") (deps (list (crate-dep (name "failure") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "rug") (req "^1.0") (optional #t) (default-features #t) (kind 0)))) (hash "00kml57301jggi4n1ppvnc2pxahb6s6sx5ml84qkf5v439583s5s") (features (quote (("default" "rug"))))))

(define-public crate-mexprp-0.3 (crate (name "mexprp") (vers "0.3.1") (deps (list (crate-dep (name "rug") (req "^1.9") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.26") (default-features #t) (kind 0)))) (hash "0s0hdg1gk1yq8vzms2yvsvakmg1kdgyag4xn13qrcp0713q1dfsh") (features (quote (("default" "rug"))))))

