(define-module (crates-io me nu) #:use-module (crates-io))

(define-public crate-menu-0.1 (crate (name "menu") (vers "0.1.0") (hash "01xl0g5c6bz8pwqa4hs3g88gz96v96cg0z5jwqwc1945nxqzx9bq")))

(define-public crate-menu-0.1 (crate (name "menu") (vers "0.1.1") (hash "1my04ckpxd6rpip52ni6m54xnvwv8yalzpwq881r1y6r18j4nnz7")))

(define-public crate-menu-0.2 (crate (name "menu") (vers "0.2.1") (hash "1f4x6848rgpbak1g8zwb5mvzbjfzxs40plq04y9swx5406x9904f")))

(define-public crate-menu-0.2 (crate (name "menu") (vers "0.2.0") (deps (list (crate-dep (name "pancurses") (req "^0.16") (default-features #t) (kind 2)))) (hash "1bbnnn6v6sifhq4ig904n358if19a447a7696gcisx54r9qsqilc") (yanked #t)))

(define-public crate-menu-0.3 (crate (name "menu") (vers "0.3.0") (deps (list (crate-dep (name "pancurses") (req "^0.16") (default-features #t) (kind 2)))) (hash "00ib9wpj9v2ywh0cngrv14aav64q73l2pmfnj6vi9fvbnmyyg1zh")))

(define-public crate-menu-0.3 (crate (name "menu") (vers "0.3.1") (deps (list (crate-dep (name "pancurses") (req "^0.16") (default-features #t) (kind 2)))) (hash "0z59vb66kiarihzhhzw8x7psim0nnf1a7cmpgj8slziarqzdd387")))

(define-public crate-menu-0.3 (crate (name "menu") (vers "0.3.2") (deps (list (crate-dep (name "pancurses") (req "^0.16") (default-features #t) (kind 2)))) (hash "1mazqy65xlida0871n1pdf497v1f2i8pk4yzssd355zyidwpygdh")))

(define-public crate-menu-0.4 (crate (name "menu") (vers "0.4.0") (deps (list (crate-dep (name "pancurses") (req "^0.16") (default-features #t) (kind 2)))) (hash "0gdms9kbdhrp8r6flnvpj2f7mp59n6h0p5hdyih7fb04c3b8n86m") (features (quote (("echo") ("default" "echo"))))))

(define-public crate-menu-0.5 (crate (name "menu") (vers "0.5.0") (deps (list (crate-dep (name "pancurses") (req "^0.16") (default-features #t) (kind 2)))) (hash "0ban35a1y325x7afmdsbh2vbr6pfxf49078qvmspgfdkk441366f") (features (quote (("echo") ("default" "echo"))))))

(define-public crate-menu_generator-0.1 (crate (name "menu_generator") (vers "0.1.2") (deps (list (crate-dep (name "rand") (req "^0.7.0") (default-features #t) (kind 0)))) (hash "0q58s5zr06kdim5yhw4f5h6m0l347lxfi9liypz9klzw59g6xl1q")))

(define-public crate-menu_generator-0.1 (crate (name "menu_generator") (vers "0.1.3") (deps (list (crate-dep (name "rand") (req "^0.7.0") (default-features #t) (kind 0)))) (hash "1grzgnjr7pa6s4z7c0mymxr7cysvhifamj5blpf24arh313l49q8")))

(define-public crate-menu_generator-0.1 (crate (name "menu_generator") (vers "0.1.4") (deps (list (crate-dep (name "rand") (req "^0.7.0") (default-features #t) (kind 0)))) (hash "1ml7c2bskvv5mv8jipr9r4m93jcqdhnygfnzphsa40dg07vkn7cv")))

(define-public crate-menu_genie-0.1 (crate (name "menu_genie") (vers "0.1.0") (hash "0b93ajypq3966sqlwjfpzsmmjvz8cwgxzwdz2ck057xm1h3pxdbb")))

(define-public crate-menu_genie-0.1 (crate (name "menu_genie") (vers "0.1.1") (hash "0dppif1lvmgmnxycrcrfrnr36ykjnjhc9ym653gha2p3z79mf1br")))

(define-public crate-menu_rs-0.1 (crate (name "menu_rs") (vers "0.1.0") (deps (list (crate-dep (name "console") (req "^0.15.2") (default-features #t) (kind 0)))) (hash "1q987z26nkb30kkks677anldglci7gm0ig1qnvm8xk70h56l2wi2") (yanked #t)))

(define-public crate-menu_rs-0.2 (crate (name "menu_rs") (vers "0.2.0") (deps (list (crate-dep (name "console") (req "^0.15.2") (default-features #t) (kind 0)))) (hash "0f6xhr9dnqmlrph15d82glnihw4h7gfk3rhs3hq9swq6c38hvn35") (yanked #t)))

(define-public crate-menu_rs-0.2 (crate (name "menu_rs") (vers "0.2.1") (deps (list (crate-dep (name "console") (req "^0.15.2") (default-features #t) (kind 0)))) (hash "12lzkd78clz19j1x3dy5i9i5lpwphhzjhnwlshr27558ydqdzv1v") (yanked #t)))

(define-public crate-menu_rs-0.2 (crate (name "menu_rs") (vers "0.2.2") (deps (list (crate-dep (name "console") (req "^0.15.2") (default-features #t) (kind 0)))) (hash "03ws3m809przrmnz901jyp4nnzd8fmiah2ilrh8adqbhz9v4ih5l")))

(define-public crate-menu_rs-0.3 (crate (name "menu_rs") (vers "0.3.0") (deps (list (crate-dep (name "console") (req "^0.15.2") (default-features #t) (kind 0)))) (hash "0a8fgnkd9fdifckmn3z0dvvrcpdg87g1h3v3lsi044v1jfy30any")))

(define-public crate-menu_rs-0.3 (crate (name "menu_rs") (vers "0.3.1") (deps (list (crate-dep (name "console") (req "^0.15.2") (default-features #t) (kind 0)))) (hash "1qfgny5j5k1mllcjh37qfhn2csz200hg6qd43xannxms2i1l9snn")))

(define-public crate-menubar-0.0.1 (crate (name "menubar") (vers "0.0.1") (deps (list (crate-dep (name "env_logger") (req "^0.8") (default-features #t) (kind 2)) (crate-dep (name "objc") (req "^0.2") (default-features #t) (target "cfg(target_os = \"macos\")") (kind 0)) (crate-dep (name "raw-window-handle") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "winapi") (req "^0.3") (features (quote ("winuser" "libloaderapi" "wincon"))) (default-features #t) (target "cfg(target_os = \"windows\")") (kind 0)) (crate-dep (name "winit") (req "^0.25") (default-features #t) (kind 2)))) (hash "1vh131rl273zrlbb5vc439a3dhwfb88l5s4hqyqk3crhmyzhdzdj")))

(define-public crate-menubar-0.0.2 (crate (name "menubar") (vers "0.0.2") (deps (list (crate-dep (name "env_logger") (req "^0.8") (default-features #t) (kind 2)) (crate-dep (name "objc") (req "=0.3.0-alpha.4") (default-features #t) (target "cfg(target_os = \"macos\")") (kind 0) (package "objc2")) (crate-dep (name "objc_foundation") (req "=0.2.0-alpha.2") (default-features #t) (target "cfg(target_os = \"macos\")") (kind 0) (package "objc2-foundation")) (crate-dep (name "raw-window-handle") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "winapi") (req "^0.3") (features (quote ("winuser" "libloaderapi" "wincon"))) (default-features #t) (target "cfg(target_os = \"windows\")") (kind 0)) (crate-dep (name "winit") (req "^0.25") (default-features #t) (kind 2)))) (hash "0rhr9p1m51l27hpmqvnxyarnqv1ivlr9fvigxw8lzrwjv0ws6cgf")))

(define-public crate-menugen-0.1 (crate (name "menugen") (vers "0.1.0") (hash "0fn0fg5xab97h0ymmk724mirvypqccr350dw6ppxwsfqd771sp3h")))

(define-public crate-menugen-0.1 (crate (name "menugen") (vers "0.1.1") (hash "14izbmflc68pc756ncc1g0lznrvxprarw09fdcnfi2ba389gqgl1")))

