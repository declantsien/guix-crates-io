(define-module (crates-io me th) #:use-module (crates-io))

(define-public crate-meth-0.1 (crate (name "meth") (vers "0.1.0") (deps (list (crate-dep (name "mouse-rs") (req "^0.4") (default-features #t) (kind 0)))) (hash "0hsw6h6648gl5nx2l5bxxm1p4wgz7yfm4fb7yw3ad7ii0in4bx8b")))

(define-public crate-method-0.0.0 (crate (name "method") (vers "0.0.0") (hash "0573mr1afz77amy351p6crb5bp29cggbjvlahdm78vqqlz89068v")))

(define-public crate-method_shorthands-0.1 (crate (name "method_shorthands") (vers "0.1.0") (hash "0ksfbnwq7z67mqkzdv2fmbv4q8p428p8qv5h1gaj3d806fzzz9n7")))

(define-public crate-method_shorthands-0.2 (crate (name "method_shorthands") (vers "0.2.0") (hash "0rgj7a5717qqkhvsz7c81cp1wmr6cjzb4g2dyns3r11zkqzwsxyz")))

(define-public crate-method_shorthands-0.2 (crate (name "method_shorthands") (vers "0.2.1") (hash "1vpgimd0gdms97ak5nmsdcgpgnb6cc2xv3mdyn6n917b0jnn8gpc")))

(define-public crate-methods-enum-0.1 (crate (name "methods-enum") (vers "0.1.1") (hash "02nnijj26ba4kq4gn90h8rmsrpy20g6i7bghqi48iw4ykz1sgd5s") (yanked #t)))

(define-public crate-methods-enum-0.1 (crate (name "methods-enum") (vers "0.1.2") (hash "1a3jwq1mnss2l6vc01m6q7nwpqxfkzf72yryzhlpgvapw6lr39yd") (yanked #t)))

(define-public crate-methods-enum-0.1 (crate (name "methods-enum") (vers "0.1.3") (hash "0fjl6j414laq1ppk1izswfr0if6xsj71c934m65ijx7wizaffxws")))

(define-public crate-methods-enum-0.1 (crate (name "methods-enum") (vers "0.1.4") (hash "1xyr012rsybyzbsz0zpsk6iqgcway3crclwpwsynzb1xwqqpk1nh")))

(define-public crate-methods-enum-0.1 (crate (name "methods-enum") (vers "0.1.5") (hash "1kwp077hbp1chk5yi5rn4jbkisbqzvjg5vk1j2i0wz08l5jn9vxx")))

(define-public crate-methods-enum-0.2 (crate (name "methods-enum") (vers "0.2.0") (hash "1rjmhmbxxiksz1clphpf47dv4x4il549ihqdpvvfqrjq00lx7rxk")))

(define-public crate-methods-enum-0.2 (crate (name "methods-enum") (vers "0.2.1") (hash "1vx6id09yscbhjdvyrqxb84nzfk3zsp6hmgs76r6rh9zlfigkvyq")))

(define-public crate-methods-enum-0.2 (crate (name "methods-enum") (vers "0.2.2") (hash "0ij0hhqagv7f2kc0a6dqfa8v814s7hb8j72s66fvsnax7q72gzjv")))

(define-public crate-methods-enum-0.2 (crate (name "methods-enum") (vers "0.2.3") (hash "09449c6apl8qzhp09cc75iisf5fvv2kl17zq6ixz7qi9a0zn6l7j")))

(define-public crate-methods-enum-0.2 (crate (name "methods-enum") (vers "0.2.4") (hash "0pjhiax8xqb3vh52v06iwni63496z5rhlq1zz6h32gdjfnmm08ih")))

(define-public crate-methods-enum-0.3 (crate (name "methods-enum") (vers "0.3.0") (hash "0v2cmm2f83vzdl2g3apcvshq8nzk45j3s3rnljdan0gcg0fgda5k")))

(define-public crate-methods-enum-0.3 (crate (name "methods-enum") (vers "0.3.1") (hash "1bwknq3cib0bm5kvx8v5jji3qz0lnvwhnawjdd4kq0i541bpfcil")))

(define-public crate-methods-enum-0.3 (crate (name "methods-enum") (vers "0.3.2") (hash "07acpl67wnnphxjzhph1vk3sn6jb5xw0kqn774shgi0gg4452qi8")))

(define-public crate-methylome-0.3 (crate (name "methylome") (vers "0.3.0") (deps (list (crate-dep (name "itertools") (req "^0.10.5") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.40") (default-features #t) (kind 0)))) (hash "10xi3m3cvzj13zkhsgz1mydbydjlnwmb7w23c1nhr7l7qqxkn3h3")))

