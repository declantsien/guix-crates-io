(define-module (crates-io me md) #:use-module (crates-io))

(define-public crate-memdatabase-0.1 (crate (name "memdatabase") (vers "0.1.0") (deps (list (crate-dep (name "futures") (req "^0.3.30") (features (quote ("std" "async-await" "executor"))) (kind 0)) (crate-dep (name "log") (req "^0.4.21") (kind 0)) (crate-dep (name "prost") (req "^0.12.6") (features (quote ("derive" "std"))) (kind 0)) (crate-dep (name "prost-types") (req "^0.12.6") (features (quote ("std"))) (kind 0)) (crate-dep (name "tokio") (req "^1.37.0") (features (quote ("macros" "rt-multi-thread"))) (kind 0)) (crate-dep (name "tokio-stream") (req "^0.1.15") (features (quote ("time"))) (kind 0)) (crate-dep (name "tonic") (req "^0.11.0") (features (quote ("transport" "codegen" "prost"))) (kind 0)) (crate-dep (name "tonic-build") (req "^0.11.0") (features (quote ("transport" "prost"))) (kind 1)))) (hash "1jfjbgp8i2v7f7v0g14mykl5xdalsi6m28wf90l7khrjgmc01m4c")))

(define-public crate-memdb-0.1 (crate (name "memdb") (vers "0.1.0") (deps (list (crate-dep (name "failure") (req "^0.1.2") (default-features #t) (kind 0)) (crate-dep (name "parking_lot") (req "^0.6.3") (default-features #t) (kind 0)))) (hash "1ql7ccfy96259znacmc566gx1jxpvd1kw7x782w7m93anr7n2m0c")))

(define-public crate-memdb-1 (crate (name "memdb") (vers "1.0.0") (deps (list (crate-dep (name "parking_lot") (req "^0.6.3") (default-features #t) (kind 0)) (crate-dep (name "runtime") (req "^0.3.0-alpha.4") (default-features #t) (kind 2)))) (hash "0zpsn1zlikmgxnkc7n7skjg80mn03rr6msd7479ha212ic4mm701")))

(define-public crate-memdbg-0.1 (crate (name "memdbg") (vers "0.1.0") (hash "0hrniz1iq8cylsymw1rnq64pl3hgiwwgklc2jg74lkcajhl8lqvl") (features (quote (("stringify") ("default" "debug") ("debug"))))))

(define-public crate-memdbg-0.1 (crate (name "memdbg") (vers "0.1.1") (hash "12ll2zpz56qzf52y51z9fccbb8qkwkdv8zsn0w1mla9wp9mnlp21") (features (quote (("stringify") ("default" "debug") ("debug"))))))

(define-public crate-memdbg-0.1 (crate (name "memdbg") (vers "0.1.2") (hash "1g4xr09y4q5imh170q162f1j9sdpi2zab0wrm3faad13m3nyid6v") (features (quote (("stringify") ("default" "debug") ("debug"))))))

(define-public crate-memdbg-0.1 (crate (name "memdbg") (vers "0.1.3") (hash "0aqjrrh9hdh86d2dnymx7v2gyyl8f6ryr4w4686y3zk3gkxv2hap") (features (quote (("stringify") ("default" "debug") ("debug"))))))

(define-public crate-memdbg-0.1 (crate (name "memdbg") (vers "0.1.4") (hash "0z55knq197ayxd8ip03dyzgy765fihginpn82a6qsfvl7zqdmjrw") (features (quote (("stringify") ("default" "debug") ("debug"))))))

(define-public crate-memdump-0.1 (crate (name "memdump") (vers "0.1.0") (hash "0ddk3dvlh0q74dzirp7mz967b8amjbilbi5m3nm1dx6332zwi4jp") (yanked #t)))

(define-public crate-memdump-0.1 (crate (name "memdump") (vers "0.1.1") (hash "1bwwl3ifkpm94jmyh5bblz8j89l4r6sm1q15biggckwaj2y1hflg") (yanked #t)))

(define-public crate-memdump-0.1 (crate (name "memdump") (vers "0.1.2") (hash "14s0w7fxlnxf1ifyf0gp79d2jy917r5a04210czi42qvylwp0b9p")))

(define-public crate-memdump-0.1 (crate (name "memdump") (vers "0.1.3") (hash "1fpngxpnfcrbp1w7nz4i7p6y4708sk9hm26n0fjbnh5j37nkg5b6")))

