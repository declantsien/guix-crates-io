(define-module (crates-io me mr) #:use-module (crates-io))

(define-public crate-memrange-0.1 (crate (name "memrange") (vers "0.1.0") (hash "0raxylxhx0ikz9gm2qsjqcg9z0gf3r69h7601h37b76417rvqvx6")))

(define-public crate-memrange-0.1 (crate (name "memrange") (vers "0.1.1") (deps (list (crate-dep (name "rustc-serialize") (req "0.3.*") (default-features #t) (kind 0)))) (hash "0jsw6lz7gyp1j6rc7agi5jz1njigqwph5lx2f0h92p20pnffplfq")))

(define-public crate-memrange-0.1 (crate (name "memrange") (vers "0.1.2") (deps (list (crate-dep (name "rustc-serialize") (req "0.3.*") (default-features #t) (kind 0)))) (hash "0gr7sk6pa1k3z0bzkpvdkw9q7iig8b1mspizsplv46n5bljnnvzk")))

(define-public crate-memrange-0.1 (crate (name "memrange") (vers "0.1.3") (deps (list (crate-dep (name "rustc-serialize") (req "0.3.*") (default-features #t) (kind 0)))) (hash "0c35jy29jml3r3p4dfr5palc2dszjb9ircrc4pf4zp4fi5jvlafc")))

(define-public crate-memreader-0.1 (crate (name "memreader") (vers "0.1.0") (deps (list (crate-dep (name "cfg-if") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "kernel32-sys") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "winapi") (req "^0.2") (default-features #t) (kind 0)))) (hash "0nfsm8n5qlifslr66bgy50cngpaiby9311m9n57vfbqp1cbp0bfg")))

(define-public crate-memreader-0.1 (crate (name "memreader") (vers "0.1.1") (deps (list (crate-dep (name "cfg-if") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "kernel32-sys") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "winapi") (req "^0.2") (default-features #t) (kind 0)))) (hash "1svafgvszzsk4ddx2221g25b6dn0cciqd13sjvkqj10kmx4vqg6d")))

