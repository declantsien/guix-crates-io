(define-module (crates-io me rg) #:use-module (crates-io))

(define-public crate-mergable-0.0.0 (crate (name "mergable") (vers "0.0.0") (deps (list (crate-dep (name "rand") (req "^0.7.3") (default-features #t) (kind 0)))) (hash "1rxxbjpc7jx08z30ym6wbicvqv1zz9x42zq5d0mxa6naz6g8v334")))

(define-public crate-mergable-0.2 (crate (name "mergable") (vers "0.2.0") (deps (list (crate-dep (name "rand") (req "^0.8.3") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.25") (default-features #t) (kind 0)))) (hash "199sng0nnkxll0yqxq5k8nl8rsrgrqc8wqja7nmbijv59ns7bryq")))

(define-public crate-mergable-0.4 (crate (name "mergable") (vers "0.4.0") (deps (list (crate-dep (name "rand") (req "^0.8.4") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.25") (default-features #t) (kind 0)))) (hash "1f9jzhj9w11fqyfdd3w6km65mbvr130y3av2sdhhf6hkgzqmx683")))

(define-public crate-mergable-0.6 (crate (name "mergable") (vers "0.6.0") (deps (list (crate-dep (name "rand") (req "^0.8.4") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.26") (default-features #t) (kind 0)))) (hash "0c8kmc4f8qs6w42qmbrfp7mim9cqcm2nvv8bl3sfcd8j9sjb22gk")))

(define-public crate-mergable-0.8 (crate (name "mergable") (vers "0.8.0") (deps (list (crate-dep (name "rand") (req "^0.8.4") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.28") (default-features #t) (kind 0)))) (hash "1igy6qya36j2yg0knzpmghbkvavbxa3mf01zrgc0n14sy0b9i70n")))

(define-public crate-mergable-0.10 (crate (name "mergable") (vers "0.10.0") (deps (list (crate-dep (name "rand") (req "^0.8.4") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.29") (default-features #t) (kind 0)))) (hash "1dq4fvpl6dcnn9nhc78ykgvid1kcrwyv1l8a2hnc6dqdscm7zfsq")))

(define-public crate-mergable-0.11 (crate (name "mergable") (vers "0.11.0") (deps (list (crate-dep (name "rand") (req "^0.8.4") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.29") (default-features #t) (kind 0)))) (hash "1ayqak638r7qmxya6schz1qvs8zcisf0ssf4ykl800c4y96ixvfn")))

(define-public crate-mergable-0.12 (crate (name "mergable") (vers "0.12.0") (deps (list (crate-dep (name "rand") (req "^0.8.4") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.29") (default-features #t) (kind 0)))) (hash "011p3bnijc4lv96y3x4s626k32b9xasg1h39d3v2nryn5v7azs7w")))

(define-public crate-mergable-0.14 (crate (name "mergable") (vers "0.14.0") (deps (list (crate-dep (name "rand") (req "^0.8.4") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.29") (default-features #t) (kind 0)))) (hash "0i6nfn363q23a30y90snkfvv8lzch8i5da5r5ihryn3nmn5l17z6")))

(define-public crate-mergable-0.15 (crate (name "mergable") (vers "0.15.0") (deps (list (crate-dep (name "rand") (req "^0.8.4") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.29") (default-features #t) (kind 0)))) (hash "0iflqqpd2m9p9gy1qid9kf5bp37q9gdgbyh9r7abmmjrmhrmr5rx")))

(define-public crate-mergable-0.17 (crate (name "mergable") (vers "0.17.0") (deps (list (crate-dep (name "rand") (req "^0.8.4") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.30") (default-features #t) (kind 0)))) (hash "075hq9yvaxjsr0969rlgfq5dlpzl5x20f8qcc1sg6yy03c1ridr3")))

(define-public crate-mergable-0.19 (crate (name "mergable") (vers "0.19.0") (deps (list (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.30") (default-features #t) (kind 0)))) (hash "0mwkhd9g4va5zdlibk8rlp5hsqb1wj0cvbnlzsn50ma28sihy1gd")))

(define-public crate-mergable-0.20 (crate (name "mergable") (vers "0.20.0") (deps (list (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.30") (default-features #t) (kind 0)))) (hash "1gqajk36wyy2l13jm8z5skl67fsr1zm5nab1q0ilja5d20c5kicc")))

(define-public crate-mergable-0.21 (crate (name "mergable") (vers "0.21.0") (deps (list (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.30") (default-features #t) (kind 0)))) (hash "15arirrv8g82w13fd495pb96sv47pmn20ip4zrhiay31dk4n1n7p")))

(define-public crate-mergable-0.23 (crate (name "mergable") (vers "0.23.0") (deps (list (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.33") (default-features #t) (kind 0)))) (hash "1gpvp4r6z2wrpr67ln2j1wpjci8iymf21qkphaz7i8lscbk334dx")))

(define-public crate-mergable-0.25 (crate (name "mergable") (vers "0.25.0") (deps (list (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.34") (default-features #t) (kind 0)))) (hash "0g7fpv7ivfqb2siza4kpi4i0jhiszsv5sr129hbj0vf25zf55lj3")))

(define-public crate-mergable-0.27 (crate (name "mergable") (vers "0.27.0") (deps (list (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.35") (default-features #t) (kind 0)))) (hash "1ca1x4yzd985k2ssfbhkx3ywy7h6l3bdqkn31ayqky7ghkikgj3v")))

(define-public crate-mergable-0.29 (crate (name "mergable") (vers "0.29.0") (deps (list (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.36") (default-features #t) (kind 0)))) (hash "1p7pkq87881kil2yabr1cczzrcccc1sv02bjsx717apvklrb07fl")))

(define-public crate-mergable-0.31 (crate (name "mergable") (vers "0.31.0") (deps (list (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.37") (default-features #t) (kind 0)))) (hash "08cd6qf0vvkzf2j0672ybq15nxalq26mrydr6w7xax1dlcn8vgnq")))

(define-public crate-mergable-0.33 (crate (name "mergable") (vers "0.33.0") (deps (list (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.38") (default-features #t) (kind 0)))) (hash "1wkrma55sqmd7kdhmnav66ljy3d8q5w681c8nhycbfwszw68gd6q")))

(define-public crate-mergable-0.35 (crate (name "mergable") (vers "0.35.0") (deps (list (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.39") (default-features #t) (kind 0)))) (hash "1macr7s6x7qyg670x5ldjvnk5hq7f0agi24g822l27m55dp29f0j")))

(define-public crate-mergable-0.37 (crate (name "mergable") (vers "0.37.0") (deps (list (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.39") (default-features #t) (kind 0)))) (hash "1iz58kpn0cdarlbcdj51bsk2nmi980skvn4c4ccrbwym8akal4yh")))

(define-public crate-mergable-0.39 (crate (name "mergable") (vers "0.39.0") (deps (list (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.39") (default-features #t) (kind 0)))) (hash "0mnf5zf4wwwkr3ah2xjh5njkh8gi1ylyykaql1y5fgwikfif5g34")))

(define-public crate-mergable-0.40 (crate (name "mergable") (vers "0.40.0") (deps (list (crate-dep (name "proptest") (req "^1.1.0") (default-features #t) (kind 2)) (crate-dep (name "proptest-derive") (req "^0.3.0") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.39") (default-features #t) (kind 0)))) (hash "1ggczx78mscnqmg1wdiy7jzkzbqnqfwb27yrwpkzlpg8dq2qryfb")))

(define-public crate-mergable-0.41 (crate (name "mergable") (vers "0.41.0") (deps (list (crate-dep (name "proptest") (req "^1.1.0") (default-features #t) (kind 2)) (crate-dep (name "proptest-derive") (req "^0.3.0") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.39") (default-features #t) (kind 0)))) (hash "1dqsqdbz6bm001n8a2ri9jfsbqw6708nq7zwfkc1bzckv6f8ibhh")))

(define-public crate-mergable-0.43 (crate (name "mergable") (vers "0.43.0") (deps (list (crate-dep (name "proptest") (req "^1.1.0") (default-features #t) (kind 2)) (crate-dep (name "proptest-derive") (req "^0.4.0") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.39") (default-features #t) (kind 0)))) (hash "073jh9a8gfxlaypci9ypjnjign586lki7babfiklwibhvbk9dymy")))

(define-public crate-merge-0.1 (crate (name "merge") (vers "0.1.0") (deps (list (crate-dep (name "envy") (req "^0.4") (default-features #t) (kind 2)) (crate-dep (name "merge_derive") (req "^0.1.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2.12") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 2)) (crate-dep (name "structopt") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "toml") (req "^0.5") (default-features #t) (kind 2)) (crate-dep (name "trybuild") (req "^1.0") (default-features #t) (kind 2)))) (hash "1sck0vhi9lk8a6mgky0rgn842fj7yspywidwbd963nmimf9yzfqh") (features (quote (("std") ("num" "num-traits") ("derive" "merge_derive") ("default" "derive" "num" "std"))))))

(define-public crate-merge-cfg-0.1 (crate (name "merge-cfg") (vers "0.1.0") (deps (list (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2.0") (default-features #t) (kind 0)))) (hash "1chl8wggsb29hd0mpqkbh6dc8sbnq0r18hyfd89kbaa00pxar0zm")))

(define-public crate-merge-cfg-0.1 (crate (name "merge-cfg") (vers "0.1.1") (deps (list (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2.0") (default-features #t) (kind 0)))) (hash "1bzajhk86cwil8vhyy7nw1xpy27cldc92g59dlslslb69ncs0kcg")))

(define-public crate-merge-hashmap-0.1 (crate (name "merge-hashmap") (vers "0.1.0") (deps (list (crate-dep (name "merge_derive") (req "^0.1.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2.12") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "envy") (req "^0.4") (default-features #t) (kind 2)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 2)) (crate-dep (name "structopt") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "toml") (req "^0.5") (default-features #t) (kind 2)) (crate-dep (name "trybuild") (req "^1.0") (default-features #t) (kind 2)))) (hash "0akh3mmc02yvgiwam78rf8ghv5yklpmvygx5xk4a58v3nmzyv39r") (features (quote (("std") ("num" "num-traits") ("derive" "merge_derive") ("default" "derive" "num" "std"))))))

(define-public crate-merge-hashmap-0.1 (crate (name "merge-hashmap") (vers "0.1.1") (deps (list (crate-dep (name "merge_derive") (req "^0.1.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2.12") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "envy") (req "^0.4") (default-features #t) (kind 2)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 2)) (crate-dep (name "structopt") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "toml") (req "^0.5") (default-features #t) (kind 2)) (crate-dep (name "trybuild") (req "^1.0") (default-features #t) (kind 2)))) (hash "0r0513rjgskcyabk6m1wfcm823gh3b52ph23czgbdccz1i5gcycn") (features (quote (("std") ("num" "num-traits") ("derive" "merge_derive") ("default" "derive" "num" "std"))))))

(define-public crate-merge-hashmap-0.1 (crate (name "merge-hashmap") (vers "0.1.2") (deps (list (crate-dep (name "merge_derive-hashmap") (req "^0.1.1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2.12") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "envy") (req "^0.4") (default-features #t) (kind 2)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 2)) (crate-dep (name "structopt") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "toml") (req "^0.5") (default-features #t) (kind 2)) (crate-dep (name "trybuild") (req "^1.0") (default-features #t) (kind 2)))) (hash "1zf1bjg3p9ry1in3s8ljmj3g9kdx6yq7x6mhhw83q4b2wx9bni9i") (features (quote (("std") ("num" "num-traits") ("derive" "merge_derive-hashmap") ("default" "derive" "num" "std"))))))

(define-public crate-merge-io-0.1 (crate (name "merge-io") (vers "0.1.0") (deps (list (crate-dep (name "futures-preview") (req "^0.3.0-alpha.17") (default-features #t) (kind 0)))) (hash "004hnwrg4pyqdim0z5lbazj09h7q6jgswa11c85sc7pj5fjqdi4y")))

(define-public crate-merge-io-0.1 (crate (name "merge-io") (vers "0.1.1") (deps (list (crate-dep (name "futures-preview") (req "^0.3.0-alpha.17") (default-features #t) (kind 0)))) (hash "0y08v62b6jlhqy2rj34jif86zq0wgcxbg0d11k02pbi3mb3hws2y")))

(define-public crate-merge-io-0.1 (crate (name "merge-io") (vers "0.1.2") (deps (list (crate-dep (name "futures-preview") (req "^0.3.0-alpha.17") (default-features #t) (kind 0)))) (hash "0wbcgjsbpdf9cq9vvfdydlb9f6z2dmkw5dxr6vidll5gbxi0n0hy")))

(define-public crate-merge-io-0.2 (crate (name "merge-io") (vers "0.2.0") (deps (list (crate-dep (name "futures-preview") (req "^0.3.0-alpha.19") (default-features #t) (kind 0)))) (hash "1v4j0mm6qh9kjvf8b93mh0rv1zq2y3n7yzjj6kgf6jv4a9by8dyc")))

(define-public crate-merge-io-0.3 (crate (name "merge-io") (vers "0.3.0") (deps (list (crate-dep (name "futures") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "futures-io") (req "^0.3") (default-features #t) (kind 0)))) (hash "0mv357fzs854jw4g7g32s27ww7nwqfchsb60i3q6mcszs26dl234")))

(define-public crate-merge-junit-0.1 (crate (name "merge-junit") (vers "0.1.0") (deps (list (crate-dep (name "anyhow") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "assert_cmd") (req "^2.0") (default-features #t) (kind 2)) (crate-dep (name "assert_fs") (req "^1.0") (default-features #t) (kind 2)) (crate-dep (name "clap") (req "^3.2") (features (quote ("cargo"))) (default-features #t) (kind 0)) (crate-dep (name "predicates") (req "^2.1") (default-features #t) (kind 2)) (crate-dep (name "quick-xml") (req "^0.23") (default-features #t) (kind 0)))) (hash "12hfm4rizmivpcn67ypal182yadix9ikfn19bf65x28ancplffxw")))

(define-public crate-merge-junit-0.1 (crate (name "merge-junit") (vers "0.1.1") (deps (list (crate-dep (name "anyhow") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "assert_cmd") (req "^2.0") (default-features #t) (kind 2)) (crate-dep (name "assert_fs") (req "^1.0") (default-features #t) (kind 2)) (crate-dep (name "clap") (req "^3.2") (features (quote ("cargo"))) (default-features #t) (kind 0)) (crate-dep (name "predicates") (req "^2.1") (default-features #t) (kind 2)) (crate-dep (name "quick-xml") (req "^0.23") (default-features #t) (kind 0)))) (hash "0c3jx5a538d772afkj7siapbyb9m5zbsc34c16s85bpcmy3xd42q")))

(define-public crate-merge-junit-0.1 (crate (name "merge-junit") (vers "0.1.3") (deps (list (crate-dep (name "anyhow") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "assert_cmd") (req "^2.0") (default-features #t) (kind 2)) (crate-dep (name "assert_fs") (req "^1.0") (default-features #t) (kind 2)) (crate-dep (name "clap") (req "^3.2") (features (quote ("cargo"))) (default-features #t) (kind 0)) (crate-dep (name "predicates") (req "^2.1") (default-features #t) (kind 2)) (crate-dep (name "quick-xml") (req "^0.23") (default-features #t) (kind 0)))) (hash "1kpymp5cakjz1d2vhz6bq703k76bi8xdzmq8mid8rnhlpq9camw9")))

(define-public crate-merge-junit-0.1 (crate (name "merge-junit") (vers "0.1.4") (deps (list (crate-dep (name "anyhow") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "assert_cmd") (req "^2.0") (default-features #t) (kind 2)) (crate-dep (name "assert_fs") (req "^1.0") (default-features #t) (kind 2)) (crate-dep (name "clap") (req "^3.2") (features (quote ("cargo"))) (default-features #t) (kind 0)) (crate-dep (name "predicates") (req "^2.1") (default-features #t) (kind 2)) (crate-dep (name "quick-xml") (req "^0.23") (default-features #t) (kind 0)))) (hash "08wp5h0925wlxvcmbdjav0xbil7xqkra7v5q8wswg2w4nha1q0mc")))

(define-public crate-merge-lang-0.1 (crate (name "merge-lang") (vers "0.1.0") (deps (list (crate-dep (name "bytestring") (req "^1.3.0") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.20") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.9.6") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.49") (default-features #t) (kind 0)))) (hash "18cn7wkh513j7vlq8wm8rwsw24a9yj9yqv755c5ip6lgpnwfgvgr")))

(define-public crate-merge-lang-0.1 (crate (name "merge-lang") (vers "0.1.1") (deps (list (crate-dep (name "bytestring") (req "^1.3.0") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.20") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.9.6") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.49") (default-features #t) (kind 0)))) (hash "0170isl0w0sv5swp0ysp8rmcydpngxgc9gf3fl52dqv46j8dhpk4")))

(define-public crate-merge-lang-0.1 (crate (name "merge-lang") (vers "0.1.2") (deps (list (crate-dep (name "bytestring") (req "^1.3.0") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.20") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.9.6") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.49") (default-features #t) (kind 0)))) (hash "19alwfvpha12skbzk63is6346bvwll876xqw7rlmgnhyq4alydxj")))

(define-public crate-merge-opt-0.1 (crate (name "merge-opt") (vers "0.1.0") (hash "1q9iwnrx7lhks2xlgbka68z2g7067x57w4lv9xnhlqaalb0vi5vh") (features (quote (("i9") ("i8") ("i7") ("i6") ("i5") ("i4") ("i3") ("i10") ("default" "i3" "i4" "i5" "i6" "i7" "i8" "i9" "i10"))))))

(define-public crate-merge-rs-0.1 (crate (name "merge-rs") (vers "0.1.0") (deps (list (crate-dep (name "derivative") (req "^2.2.0") (default-features #t) (kind 0)))) (hash "1cc27h0kg15dpw719z1xr2cmssxrf71h9344w8zhxblgkwlmggxf") (yanked #t) (rust-version "1.58.1")))

(define-public crate-merge-rs-0.1 (crate (name "merge-rs") (vers "0.1.1") (deps (list (crate-dep (name "derivative") (req "^2.2.0") (default-features #t) (kind 0)))) (hash "0i4vv8gdbmp06j9blygd0w5nnhvjslwbzw2nqb92q6rlwl27ni90") (yanked #t) (rust-version "1.58.1")))

(define-public crate-merge-rs-0.1 (crate (name "merge-rs") (vers "0.1.2") (deps (list (crate-dep (name "derivative") (req "^2.2.0") (default-features #t) (kind 0)))) (hash "1brqfsd50a549qynsr26277yvzqjgl92568dhz0hsaiqg7i0i6a9") (rust-version "1.58.1")))

(define-public crate-merge-rs-0.2 (crate (name "merge-rs") (vers "0.2.0") (deps (list (crate-dep (name "concat-idents") (req "^1.1.3") (default-features #t) (kind 2)) (crate-dep (name "derivative") (req "^2.2.0") (default-features #t) (kind 0)))) (hash "0vnr5mbsh1iz1d96gh21y9c6dagyxn32w3d3q366qaqyc0p7rkjd") (rust-version "1.58.1")))

(define-public crate-merge-rs-0.3 (crate (name "merge-rs") (vers "0.3.0") (deps (list (crate-dep (name "concat-idents") (req "^1.1.3") (default-features #t) (kind 2)) (crate-dep (name "derivative") (req "^2.2.0") (default-features #t) (kind 0)))) (hash "1b4bhj8ps3bqwg3kgqqkh16ggdkak0x3w90zffb2q9nvfj8bcywm") (rust-version "1.58.1")))

(define-public crate-merge-rs-0.4 (crate (name "merge-rs") (vers "0.4.0") (deps (list (crate-dep (name "concat-idents") (req "^1.1.3") (default-features #t) (kind 2)) (crate-dep (name "derivative") (req "^2.2.0") (default-features #t) (kind 0)) (crate-dep (name "merge-rs-derive") (req "^0.4.0") (default-features #t) (kind 0)))) (hash "0rvizsah7lgdl7pi4jawl4h5mzn3zriqqj11x4h4x1nyrjk76wa3") (rust-version "1.68.2")))

(define-public crate-merge-rs-derive-0.4 (crate (name "merge-rs-derive") (vers "0.4.0") (deps (list (crate-dep (name "darling") (req "^0.20.3") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1.0.69") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.33") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2.0.39") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "09x5k6mvlycw3zbnsca73iy0439yqva541g3973dnbjgjhknz9wz") (rust-version "1.68.2")))

(define-public crate-merge-source-map-0.1 (crate (name "merge-source-map") (vers "0.1.0") (deps (list (crate-dep (name "sourcemap") (req "^7.0.1") (default-features #t) (kind 0)))) (hash "1rjm1ll4j22ds750mmhf13pwizdnpmf7s76qdvlm3lm8lv9dm57n")))

(define-public crate-merge-source-map-0.1 (crate (name "merge-source-map") (vers "0.1.1") (deps (list (crate-dep (name "sourcemap") (req "^7.0.1") (default-features #t) (kind 0)))) (hash "0vadasmfhw97af4cqw1v5pd21qqcvxbwmlyahyzd723azi77zig0")))

(define-public crate-merge-source-map-1 (crate (name "merge-source-map") (vers "1.0.0") (deps (list (crate-dep (name "sourcemap") (req "^7.0.1") (default-features #t) (kind 0)))) (hash "1crdaqsnpqhg1vw54b1n96wg01cwr0xzzw7kfkd7ahw3836q3kwh") (yanked #t)))

(define-public crate-merge-source-map-1 (crate (name "merge-source-map") (vers "1.1.0") (deps (list (crate-dep (name "sourcemap") (req "^7.0.1") (default-features #t) (kind 0)))) (hash "1i7pzi461waag7i2vk7qmk7hzihqvx4wv4lwza51gkgmi2h7iz9n") (yanked #t)))

(define-public crate-merge-source-map-1 (crate (name "merge-source-map") (vers "1.2.0") (deps (list (crate-dep (name "sourcemap") (req "^7.0.1") (default-features #t) (kind 0)))) (hash "1p9lq262p2zns9iqrn3afhfkpbfgz6p185fm1x7pp3cvh7x461gj")))

(define-public crate-merge-streams-0.1 (crate (name "merge-streams") (vers "0.1.0") (deps (list (crate-dep (name "futures") (req "^0.3.19") (default-features #t) (kind 2)) (crate-dep (name "futures-core") (req "^0.3.16") (default-features #t) (kind 0)) (crate-dep (name "futures-lite") (req "^1.12.0") (default-features #t) (kind 2)) (crate-dep (name "pin-project") (req "^1.0.8") (default-features #t) (kind 0)))) (hash "0vp6ffq39j61z58mapnh4w0vxb1pmxvq3sfm1a3pssbmnal4dv9z")))

(define-public crate-merge-streams-0.1 (crate (name "merge-streams") (vers "0.1.1") (deps (list (crate-dep (name "futures") (req "^0.3.19") (default-features #t) (kind 2)) (crate-dep (name "futures-core") (req "^0.3.16") (default-features #t) (kind 0)) (crate-dep (name "futures-lite") (req "^1.12.0") (default-features #t) (kind 2)) (crate-dep (name "pin-project") (req "^1.0.8") (default-features #t) (kind 0)))) (hash "0z63w34pzn4k47ydnwd029fy5pandz8gq573qnqyz1bzl8w5vb19")))

(define-public crate-merge-streams-0.1 (crate (name "merge-streams") (vers "0.1.2") (deps (list (crate-dep (name "futures") (req "^0.3.19") (default-features #t) (kind 2)) (crate-dep (name "futures-core") (req "^0.3.16") (default-features #t) (kind 0)) (crate-dep (name "futures-lite") (req "^1.12.0") (default-features #t) (kind 2)) (crate-dep (name "pin-project") (req "^1.0.8") (default-features #t) (kind 0)))) (hash "1hm7nvry0dzgkbgrin48xkl7cv4hfpzf87xcwx3d5av9552zd12g")))

(define-public crate-merge-struct-0.1 (crate (name "merge-struct") (vers "0.1.0") (deps (list (crate-dep (name "insta") (req "^1.17.1") (features (quote ("backtrace" "redactions"))) (default-features #t) (kind 2)) (crate-dep (name "pretty_assertions") (req "^1") (default-features #t) (kind 2)) (crate-dep (name "serde") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1") (default-features #t) (kind 0)))) (hash "0jnx0inklv865ny9h507835z0zrbcayvxfdn76w3ahg244nh30hx")))

(define-public crate-merge-whitespace-0.1 (crate (name "merge-whitespace") (vers "0.1.0") (deps (list (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2.0.63") (default-features #t) (kind 0)))) (hash "1vqx120h9yv38ll0n2c01hi420ryzr1y1xjjwiby1sfdnqxpis5d")))

(define-public crate-merge-yaml-hash-0.1 (crate (name "merge-yaml-hash") (vers "0.1.1") (deps (list (crate-dep (name "linked-hash-map") (req "^0.5.2") (default-features #t) (kind 0)) (crate-dep (name "yaml-rust") (req "^0.4.3") (default-features #t) (kind 0)))) (hash "0af6yg6qw4m2jj7fbk3rvj9pf1lz0fv4czjnk0k1fggm4xz090rl")))

(define-public crate-merge-yaml-hash-0.1 (crate (name "merge-yaml-hash") (vers "0.1.2") (deps (list (crate-dep (name "linked-hash-map") (req "^0.5.2") (default-features #t) (kind 0)) (crate-dep (name "yaml-rust") (req "^0.4.3") (default-features #t) (kind 0)))) (hash "0i0vwq9w8y1hmafvnva7rj28ak7r0avgbdwdymvrhbv2ay20isns")))

(define-public crate-merge-yaml-hash-0.1 (crate (name "merge-yaml-hash") (vers "0.1.3") (deps (list (crate-dep (name "linked-hash-map") (req "^0.5.2") (default-features #t) (kind 0)) (crate-dep (name "yaml-rust") (req "^0.4.3") (default-features #t) (kind 0)))) (hash "00q4izqhhrv1202qby5aim2zfxzd6fayi3y67xg7yl05m24k34sc")))

(define-public crate-merge-yaml-hash-0.2 (crate (name "merge-yaml-hash") (vers "0.2.0") (deps (list (crate-dep (name "linked-hash-map") (req "^0.5.2") (default-features #t) (kind 0)) (crate-dep (name "yaml-rust") (req "^0.4.3") (default-features #t) (kind 0)))) (hash "0kjmr36dhki1a174cb62avcz8m68sqdqbqknv05954xxkyjgn7qk")))

(define-public crate-merge-yaml-hash-0.3 (crate (name "merge-yaml-hash") (vers "0.3.0") (deps (list (crate-dep (name "linked-hash-map") (req "^0.5.2") (default-features #t) (kind 0)) (crate-dep (name "yaml-rust") (req "^0.4.3") (default-features #t) (kind 0)))) (hash "1pcj6nm296lbqrabrmb188wxhd7s1fbmfbzrfg5xfw4j67vsairq")))

(define-public crate-merge2-0.2 (crate (name "merge2") (vers "0.2.0") (deps (list (crate-dep (name "merge2_derive") (req "^0.2") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2.12") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "trybuild") (req "^1.0") (default-features #t) (kind 2)))) (hash "002rbw1cr3sadqpbj4vd7izq7rxbhr4sxccx6b3m2mdyzlmbn37f") (features (quote (("std") ("num" "num-traits") ("derive" "merge2_derive") ("default" "derive" "std"))))))

(define-public crate-merge2-0.3 (crate (name "merge2") (vers "0.3.0") (deps (list (crate-dep (name "merge2_derive") (req "^0.2") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2.12") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "trybuild") (req "^1.0") (default-features #t) (kind 2)))) (hash "1vc9cji5w65yws0bvj6a3c5w13wbz6r1fix1nir0vz0qwjx5xrfw") (features (quote (("std") ("num" "num-traits") ("derive" "merge2_derive") ("default" "derive" "std"))))))

(define-public crate-merge2_derive-0.2 (crate (name "merge2_derive") (vers "0.2.0") (deps (list (crate-dep (name "manyhow") (req "^0.10") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2.0") (default-features #t) (kind 0)))) (hash "11d7n399953qq1rvxxp47zxhiy228rjhjh6grmbw5pr6hr83z315")))

(define-public crate-merge3-0.1 (crate (name "merge3") (vers "0.1.0") (hash "1472v7wg3q19z63vcd5kh8ghhj290nk3wn8p9znp7mhnvipgk1h9")))

(define-public crate-merge_config_files-0.1 (crate (name "merge_config_files") (vers "0.1.0") (deps (list (crate-dep (name "serde") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "toml") (req "^0.7") (default-features #t) (kind 0)))) (hash "1qgqc00y6pzr3n10byis6l3zl9k0gwr6zkijrpssyi2zgfzs9q65")))

(define-public crate-merge_config_files-0.1 (crate (name "merge_config_files") (vers "0.1.1") (deps (list (crate-dep (name "serde") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "toml") (req "^0.7") (default-features #t) (kind 0)))) (hash "0mrg7rg601zsss8gi71rncm18rgpn0kw2wxxrlpd8gajrfgdnfsv")))

(define-public crate-merge_config_files-0.1 (crate (name "merge_config_files") (vers "0.1.2") (deps (list (crate-dep (name "serde") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "toml") (req "^0.7") (default-features #t) (kind 0)))) (hash "12iwrhpjn5h70nykpxvbpvxcbc63b9cmx1l4pga1rf27ih7hxf8f")))

(define-public crate-merge_config_files-0.1 (crate (name "merge_config_files") (vers "0.1.3") (deps (list (crate-dep (name "serde") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "toml") (req "^0.7") (default-features #t) (kind 0)))) (hash "19drr90mq42aq5ix5dm6ziq22xnpvyxvprjifqm76kz0d98prva8")))

(define-public crate-merge_config_files-0.1 (crate (name "merge_config_files") (vers "0.1.5") (deps (list (crate-dep (name "serde") (req "^1.0.197") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.115") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.58") (default-features #t) (kind 0)) (crate-dep (name "toml") (req "^0.8.12") (default-features #t) (kind 0)))) (hash "106skpymlm4ml04c6qbpf6qcwn1krjc67v94cr1gsf6znyii92i6")))

(define-public crate-merge_derive-0.1 (crate (name "merge_derive") (vers "0.1.0") (deps (list (crate-dep (name "proc-macro-error") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (default-features #t) (kind 0)))) (hash "01wxhi7mqmp34l540wcfb24hb13vcbps4wlynas66bnsfra0g790")))

(define-public crate-merge_derive-hashmap-0.1 (crate (name "merge_derive-hashmap") (vers "0.1.1") (deps (list (crate-dep (name "proc-macro-error") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (default-features #t) (kind 0)))) (hash "1202xgy6pd3bf4wm9ihr0ksnbqdxla9j1z5x3591q64lkqn7qyhm")))

(define-public crate-merge_json-0.1 (crate (name "merge_json") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^4.0.18") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "json") (req "^0.12") (default-features #t) (kind 0)) (crate-dep (name "thousands") (req "^0") (default-features #t) (kind 0)))) (hash "0mr4j17m7dsdmb72n4h3iyh1nhvbv79g2vyyjixmp525zzjb5h8d")))

(define-public crate-merged_range-0.1 (crate (name "merged_range") (vers "0.1.0") (deps (list (crate-dep (name "clippy-utilities") (req "^0.2.0") (default-features #t) (kind 0)))) (hash "1fqd064zhjp4k05szcyqrdbk5azhcm7nmmf9qlai1jlbsxvk60ac")))

(define-public crate-mergekmer-0.1 (crate (name "mergekmer") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^4.1.1") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "csv") (req "^1.2") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "0a6s7x4qw3p28myzmfgnxv6ry2kh11nac8r8s530h03l913abxiz")))

(define-public crate-mergekmer-0.1 (crate (name "mergekmer") (vers "0.1.1") (deps (list (crate-dep (name "clap") (req "^4.1.1") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "csv") (req "^1.2") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "0xp23z790c49v945jm66rw2gn3l45w02vsr6r7mzg0w6niyjzacz")))

(define-public crate-mergekmer-0.1 (crate (name "mergekmer") (vers "0.1.2") (deps (list (crate-dep (name "clap") (req "^4.1.1") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "csv") (req "^1.2") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "10j0aq406mx911y4jirh7x0mi141738kncqh3a76krh0bpa3pp9g")))

(define-public crate-merger-0.1 (crate (name "merger") (vers "0.1.0") (deps (list (crate-dep (name "merge-yaml-hash") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "yaml-rust") (req "^0.3.5") (default-features #t) (kind 0)))) (hash "0rdrgifm3mvgiyjxwala069z44yvi1i28j48fpnxm95nivhrljmm")))

(define-public crate-mergereq-0.1 (crate (name "mergereq") (vers "0.1.8") (deps (list (crate-dep (name "clap") (req "^2.33.0") (features (quote ("yaml"))) (default-features #t) (kind 0)) (crate-dep (name "directories") (req "^2.0.2") (default-features #t) (kind 0)) (crate-dep (name "failure") (req "^0.1.6") (default-features #t) (kind 0)) (crate-dep (name "percent-encoding") (req "^2.1.0") (default-features #t) (kind 0)) (crate-dep (name "prettytable-rs") (req "^0.8.0") (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.9.22") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.103") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.42") (default-features #t) (kind 0)) (crate-dep (name "toml") (req "^0.5.5") (default-features #t) (kind 0)))) (hash "0m8kw0b83gkgj5qd7i6sk9z0q3gmjm5sbqxgkp4ipiralj14qgfq")))

(define-public crate-mergereq-0.1 (crate (name "mergereq") (vers "0.1.9") (deps (list (crate-dep (name "clap") (req "^2.33.0") (features (quote ("yaml"))) (default-features #t) (kind 0)) (crate-dep (name "directories") (req "^2.0.2") (default-features #t) (kind 0)) (crate-dep (name "failure") (req "^0.1.6") (default-features #t) (kind 0)) (crate-dep (name "percent-encoding") (req "^2.1.0") (default-features #t) (kind 0)) (crate-dep (name "prettytable-rs") (req "^0.8.0") (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.9.22") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.103") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.42") (default-features #t) (kind 0)) (crate-dep (name "toml") (req "^0.5.5") (default-features #t) (kind 0)))) (hash "11zydy2jrlrf7li1bamlhni2aa4hl9y3q95yb3q5h8lb0nkmnp1f")))

(define-public crate-mergereq-0.2 (crate (name "mergereq") (vers "0.2.0") (deps (list (crate-dep (name "clap") (req "^2.33.0") (features (quote ("yaml"))) (default-features #t) (kind 0)) (crate-dep (name "directories") (req "^2.0.2") (default-features #t) (kind 0)) (crate-dep (name "failure") (req "^0.1.6") (default-features #t) (kind 0)) (crate-dep (name "percent-encoding") (req "^2.1.0") (default-features #t) (kind 0)) (crate-dep (name "prettytable-rs") (req "^0.8.0") (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.9.22") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.103") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.42") (default-features #t) (kind 0)) (crate-dep (name "toml") (req "^0.5.5") (default-features #t) (kind 0)))) (hash "1274mxh624psvfzc46aj0a5v2j0m4hpm2b7lpmfa7nq6la6fwyrl")))

(define-public crate-mergereq-0.2 (crate (name "mergereq") (vers "0.2.1") (deps (list (crate-dep (name "clap") (req "^2.33.0") (features (quote ("yaml"))) (default-features #t) (kind 0)) (crate-dep (name "directories") (req "^2.0.2") (default-features #t) (kind 0)) (crate-dep (name "failure") (req "^0.1.6") (default-features #t) (kind 0)) (crate-dep (name "percent-encoding") (req "^2.1.0") (default-features #t) (kind 0)) (crate-dep (name "prettytable-rs") (req "^0.8.0") (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.9.22") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.103") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.42") (default-features #t) (kind 0)) (crate-dep (name "toml") (req "^0.5.5") (default-features #t) (kind 0)))) (hash "1pwqsdypc4hcmz8hnjas1yfd2cbwxsjd1wkzmsmgv16x4s91a4vz")))

(define-public crate-mergereq-0.2 (crate (name "mergereq") (vers "0.2.2") (deps (list (crate-dep (name "clap") (req "^2.33.0") (default-features #t) (kind 0)) (crate-dep (name "directories") (req "^2.0.2") (default-features #t) (kind 0)) (crate-dep (name "failure") (req "^0.1.6") (default-features #t) (kind 0)) (crate-dep (name "percent-encoding") (req "^2.1.0") (default-features #t) (kind 0)) (crate-dep (name "prettytable-rs") (req "^0.8.0") (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.9.24") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.104") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.44") (default-features #t) (kind 0)) (crate-dep (name "toml") (req "^0.5.5") (default-features #t) (kind 0)))) (hash "1qdj1d0h69n6y07kv2hh7qqzd3alfifk40yhwzxkyj90v72mmnp4")))

(define-public crate-merging-iterator-1 (crate (name "merging-iterator") (vers "1.0.0") (deps (list (crate-dep (name "proptest") (req "^0.8.7") (default-features #t) (kind 2)))) (hash "1622cxsivvdbaaif1vd4p3z07xcvzpavhghpr26022gdzmpjfgr2")))

(define-public crate-merging-iterator-1 (crate (name "merging-iterator") (vers "1.1.0") (deps (list (crate-dep (name "proptest") (req "^0.8.7") (default-features #t) (kind 2)))) (hash "1rhkszcy47dpzmam53zkhv7nf3isgr7p66in9k9zlqwdmmm1c0sk")))

(define-public crate-merging-iterator-1 (crate (name "merging-iterator") (vers "1.2.0") (deps (list (crate-dep (name "proptest") (req "^0.9.4") (default-features #t) (kind 2)))) (hash "0bmjvfvnrq1n35dis1wns7psmliczcin4zvlmh7v9q07p3jk7w9y")))

(define-public crate-merging-iterator-1 (crate (name "merging-iterator") (vers "1.3.0") (deps (list (crate-dep (name "proptest") (req "^0.9.4") (default-features #t) (kind 2)))) (hash "1bkylvliks45693drwmiyanq3bxypmf37d6hv7gjiq2h40bmbnhf")))

(define-public crate-mergle-0.1 (crate (name "mergle") (vers "0.1.0") (deps (list (crate-dep (name "bromberg_sl2") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "quickcheck") (req "^0.9") (default-features #t) (kind 2)))) (hash "0y0djwixz6iynyjbyzny529z9lk7f76lrxvkhanzf3ic13dz844g")))

(define-public crate-mergle-0.1 (crate (name "mergle") (vers "0.1.1") (deps (list (crate-dep (name "bromberg_sl2") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "quickcheck") (req "^0.9") (default-features #t) (kind 2)))) (hash "0djrv2d6ygsqygvb7zfkc3lii7ky9bn54q3kgzc7p4g1sj85fln8")))

(define-public crate-mergle-0.1 (crate (name "mergle") (vers "0.1.2") (deps (list (crate-dep (name "bromberg_sl2") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "quickcheck") (req "^0.9") (default-features #t) (kind 2)))) (hash "0hidrzsv1h42jlxdhbl1fpjqvnnzwxcflr9v23qd58yk2fgxxz5a")))

(define-public crate-mergle-0.1 (crate (name "mergle") (vers "0.1.3") (deps (list (crate-dep (name "bromberg_sl2") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "quickcheck") (req "^0.9") (default-features #t) (kind 2)))) (hash "1y3l1wyixx6xnhv5847xfgihm6dy75n3x8fj2ssbm9azpxjmlc8k")))

(define-public crate-mergle-0.1 (crate (name "mergle") (vers "0.1.4") (deps (list (crate-dep (name "bromberg_sl2") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "quickcheck") (req "^0.9") (default-features #t) (kind 2)))) (hash "0rgccrf25lglhr3dzxy76hx2g0wi62754jkmpzmnjlw30sxx0ggk")))

(define-public crate-mergle-0.1 (crate (name "mergle") (vers "0.1.5") (deps (list (crate-dep (name "bromberg_sl2") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "quickcheck") (req "^0.9") (default-features #t) (kind 2)))) (hash "0497yqpw3qkfh32yqhb59ccjiyvclvhm1v5xd3w8dqwf8q59rlzc")))

(define-public crate-mergle-0.2 (crate (name "mergle") (vers "0.2.0") (deps (list (crate-dep (name "bromberg_sl2") (req "^0.4.4") (default-features #t) (kind 0)) (crate-dep (name "fingertrees") (req "^0.2.4") (default-features #t) (kind 0)) (crate-dep (name "im-rc") (req "^15.0") (default-features #t) (kind 0)) (crate-dep (name "num-bigint") (req "^0.3.1") (default-features #t) (kind 0)) (crate-dep (name "quickcheck") (req "^0.9") (default-features #t) (kind 2)))) (hash "17k7x9ya8gwsx72jn5mxph2jwx2xzg7vwksmzfr716cw5x48ikab")))

(define-public crate-mergle-0.3 (crate (name "mergle") (vers "0.3.0") (deps (list (crate-dep (name "bromberg_sl2") (req "^0.4.4") (default-features #t) (kind 0)) (crate-dep (name "fingertrees") (req "^0.2.4") (default-features #t) (kind 0)) (crate-dep (name "num-bigint") (req "^0.3.1") (default-features #t) (kind 0)) (crate-dep (name "quickcheck") (req "^0.9") (default-features #t) (kind 2)))) (hash "1mvnyjbb74lll42k1cjfn5qsjksfpglgk0ybpshz0r2swmi7adlh")))

(define-public crate-mergle-0.3 (crate (name "mergle") (vers "0.3.1") (deps (list (crate-dep (name "bromberg_sl2") (req "^0.4.4") (default-features #t) (kind 0)) (crate-dep (name "fingertrees") (req "^0.2.4") (default-features #t) (kind 0)) (crate-dep (name "num-bigint") (req "^0.3.1") (default-features #t) (kind 0)) (crate-dep (name "quickcheck") (req "^0.9") (default-features #t) (kind 2)))) (hash "1280w05f3rd04wxbiqcpf31j0j2k2xk0akf4hnrisyj0yfwdyf8h")))

(define-public crate-mergle-0.3 (crate (name "mergle") (vers "0.3.2") (deps (list (crate-dep (name "bromberg_sl2") (req "^0.4.4") (default-features #t) (kind 0)) (crate-dep (name "fingertrees") (req "^0.2.4") (default-features #t) (kind 0)) (crate-dep (name "num-bigint") (req "^0.3.1") (default-features #t) (kind 0)) (crate-dep (name "quickcheck") (req "^0.9") (default-features #t) (kind 2)))) (hash "0cnknyvs26a0lbp81ws23xa192mb3f0q7fwn2c4537mzmqd0xaxy")))

(define-public crate-mergle-0.3 (crate (name "mergle") (vers "0.3.3") (deps (list (crate-dep (name "bromberg_sl2") (req "^0.5.0") (default-features #t) (kind 0)) (crate-dep (name "fingertrees") (req "^0.2.4") (default-features #t) (kind 0)) (crate-dep (name "num-bigint") (req "^0.3.1") (default-features #t) (kind 0)) (crate-dep (name "quickcheck") (req "^0.9") (default-features #t) (kind 2)))) (hash "1wsd3da9f1m1sxl73znryd10i80y617id67y265pr3mnz4wx6lri")))

(define-public crate-mergle-0.4 (crate (name "mergle") (vers "0.4.0") (deps (list (crate-dep (name "bromberg_sl2") (req "^0.5.0") (default-features #t) (kind 0)) (crate-dep (name "fingertrees") (req "^0.2.4") (default-features #t) (kind 0)) (crate-dep (name "num-bigint") (req "^0.3.1") (default-features #t) (kind 0)) (crate-dep (name "quickcheck") (req "^0.9") (default-features #t) (kind 2)))) (hash "0pi8fyyahpribia80r6hdl9p1345hn5xklqkxy27hajmg7vk3d3s")))

(define-public crate-mergle-0.5 (crate (name "mergle") (vers "0.5.0") (deps (list (crate-dep (name "bromberg_sl2") (req "^0.5.0") (default-features #t) (kind 0)) (crate-dep (name "quickcheck") (req "^0.9") (default-features #t) (kind 2)))) (hash "167jggb51qi44j3jsvaskh74g04vhvf9rgzf41sljc1n6cnlsn48")))

(define-public crate-mergle-0.4 (crate (name "mergle") (vers "0.4.1") (deps (list (crate-dep (name "bromberg_sl2") (req "^0.5.0") (default-features #t) (kind 0)) (crate-dep (name "quickcheck") (req "^0.9") (default-features #t) (kind 2)))) (hash "0jxssrjcivhri1jgn45nvbgmvllhvfmc4rpiihd2fwsakvg6g310") (yanked #t)))

(define-public crate-mergle-0.5 (crate (name "mergle") (vers "0.5.1") (deps (list (crate-dep (name "bromberg_sl2") (req "^0.5.0") (default-features #t) (kind 0)) (crate-dep (name "quickcheck") (req "^0.9") (default-features #t) (kind 2)))) (hash "0pa7ljrixllhlc10bwh17i871arqz8xdy9miclfz89aqiffji7gl")))

(define-public crate-mergle-0.5 (crate (name "mergle") (vers "0.5.2") (deps (list (crate-dep (name "bromberg_sl2") (req "^0.5.0") (default-features #t) (kind 0)) (crate-dep (name "quickcheck") (req "^0.9") (default-features #t) (kind 2)))) (hash "1c0i37bil1k9j3vwwk4pb2wqz723pi63lh3c80jid6cqkklhpar4")))

(define-public crate-mergui-0.0.1 (crate (name "mergui") (vers "0.0.1") (deps (list (crate-dep (name "indexmap") (req "^1.3.0") (default-features #t) (kind 0)) (crate-dep (name "quicksilver") (req "^0.3.20") (default-features #t) (kind 0)))) (hash "0vr7mib4jlv7xl33b516391j76gnhplz9mkbs629hzq989bz9mc5")))

(define-public crate-mergui-0.0.2 (crate (name "mergui") (vers "0.0.2") (deps (list (crate-dep (name "indexmap") (req "^1.3.0") (default-features #t) (kind 0)) (crate-dep (name "quicksilver") (req "^0.3.20") (default-features #t) (kind 0)))) (hash "1idacqymb596g73z6kh8wdk67dk3lmzkzaiq9r8hvjk78ynp5ivl")))

(define-public crate-mergui-0.0.3 (crate (name "mergui") (vers "0.0.3") (deps (list (crate-dep (name "indexmap") (req "^1.3.0") (default-features #t) (kind 0)) (crate-dep (name "quicksilver") (req "^0.3.20") (default-features #t) (kind 0)))) (hash "16ldmyxrhsnbf8mp0dk2fll33cqdcx16nhszna4xmj5cn5v87wi6")))

(define-public crate-mergui-0.0.4 (crate (name "mergui") (vers "0.0.4") (deps (list (crate-dep (name "indexmap") (req "^1.3.0") (default-features #t) (kind 0)) (crate-dep (name "quicksilver") (req "^0.3.20") (default-features #t) (kind 0)))) (hash "1vv2sm5i1favd3n59q2k8byyij06yipmjr2h7h6lrwd69g2lry40")))

(define-public crate-mergui-0.0.5 (crate (name "mergui") (vers "0.0.5") (deps (list (crate-dep (name "indexmap") (req "^1.3.0") (default-features #t) (kind 0)) (crate-dep (name "quicksilver") (req "^0.3.20") (default-features #t) (kind 0)))) (hash "0d8k07y4dgl905735c7c0qaddxpzgvfwpwcws7v4b2cb62jjs1qi")))

(define-public crate-mergui-0.1 (crate (name "mergui") (vers "0.1.0-alpha.1") (deps (list (crate-dep (name "indexmap") (req "^1.3.2") (default-features #t) (kind 0)) (crate-dep (name "quicksilver") (req "^0.4.0-alpha0.3") (features (quote ("ttf" "font"))) (kind 0)) (crate-dep (name "unicode-normalization") (req "^0.1.12") (default-features #t) (kind 0)))) (hash "0iyzh9a9n56601sz0mv6wwlqbzd7szdq38rwqjw2n9gmmhbl8shv") (features (quote (("web-sys" "quicksilver/web-sys") ("stdweb" "quicksilver/stdweb"))))))

(define-public crate-mergui-0.1 (crate (name "mergui") (vers "0.1.0-alpha0.2") (deps (list (crate-dep (name "indexmap") (req "^1.3.2") (default-features #t) (kind 0)) (crate-dep (name "quicksilver") (req "^0.4.0-alpha0.4") (features (quote ("ttf" "font"))) (kind 0)) (crate-dep (name "unicode-normalization") (req "^0.1.12") (default-features #t) (kind 0)))) (hash "1gfmvgqdg69g0zkb3irsivjcir4bq4fd770yy63a0kxhwwcgbwm6") (features (quote (("stdweb" "quicksilver/stdweb"))))))

(define-public crate-mergui-0.1 (crate (name "mergui") (vers "0.1.0-alpha0.3") (deps (list (crate-dep (name "indexmap") (req "^1.3.2") (default-features #t) (kind 0)) (crate-dep (name "quicksilver") (req "^0.4.0-alpha0.4") (features (quote ("ttf" "font"))) (kind 0)) (crate-dep (name "unicode-normalization") (req "^0.1.12") (default-features #t) (kind 0)))) (hash "0np345271v6zzfpk2l485m10qqkciybc8ly7hgn7ldij6ymi3yi9") (features (quote (("stdweb" "quicksilver/stdweb"))))))

(define-public crate-mergui-0.1 (crate (name "mergui") (vers "0.1.0-alpha0.4") (deps (list (crate-dep (name "indexmap") (req "^1.3.2") (default-features #t) (kind 0)) (crate-dep (name "quicksilver") (req "^0.4.0-alpha0.5") (features (quote ("ttf" "font"))) (kind 0)) (crate-dep (name "unicode-normalization") (req "^0.1.12") (default-features #t) (kind 0)))) (hash "0fxrqk2vshfsajl2025724fjgjkphvpw9pni2bh19snisa04573j") (features (quote (("stdweb" "quicksilver/stdweb"))))))

(define-public crate-mergui-0.1 (crate (name "mergui") (vers "0.1.0-alpha0.5") (deps (list (crate-dep (name "indexmap") (req "^1.3.2") (default-features #t) (kind 0)) (crate-dep (name "quicksilver") (req "^0.4.0-alpha0.5") (features (quote ("ttf" "font"))) (kind 0)) (crate-dep (name "unicode-normalization") (req "^0.1.12") (default-features #t) (kind 0)))) (hash "1djn3rjlfbbwwlvn0nm1bj5qk73py0riwiv52bjcb5qbsv0vc049") (features (quote (("stdweb" "quicksilver/stdweb"))))))

(define-public crate-mergui-0.1 (crate (name "mergui") (vers "0.1.0-alpha0.6") (deps (list (crate-dep (name "indexmap") (req "^1.3.2") (default-features #t) (kind 0)) (crate-dep (name "quicksilver") (req "^0.4.0-alpha0.6") (features (quote ("ttf" "font"))) (kind 0)) (crate-dep (name "unicode-normalization") (req "^0.1.12") (default-features #t) (kind 0)))) (hash "0i36h0dl70z9livqswdg2m8l7hqmfg1vqiyiiwvw4hjbdlkqg69y") (features (quote (("stdweb" "quicksilver/stdweb"))))))

(define-public crate-mergui-0.1 (crate (name "mergui") (vers "0.1.0-alpha0.7") (deps (list (crate-dep (name "indexmap") (req "^1.3.2") (default-features #t) (kind 0)) (crate-dep (name "quicksilver") (req "^0.4.0-alpha0.7") (features (quote ("ttf" "font"))) (kind 0)) (crate-dep (name "unicode-normalization") (req "^0.1.12") (default-features #t) (kind 0)))) (hash "15x65c39xzyd2ikf7kqh7hxhgv91m1cnlssdriaz569j9hrmh0ym") (features (quote (("stdweb" "quicksilver/stdweb"))))))

(define-public crate-mergui-0.1 (crate (name "mergui") (vers "0.1.0-alpha0.8") (deps (list (crate-dep (name "indexmap") (req "^1.3.2") (default-features #t) (kind 0)) (crate-dep (name "quicksilver") (req "^0.4.0") (features (quote ("ttf" "font"))) (kind 0)) (crate-dep (name "unicode-normalization") (req "^0.1.12") (default-features #t) (kind 0)))) (hash "1jkyp9s76mdq96xcmrbhj1dnxj8bcs3mi5crb383v947fvjh3xxv") (features (quote (("stdweb" "quicksilver/stdweb"))))))

