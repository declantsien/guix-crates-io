(define-module (crates-io me mc) #:use-module (crates-io))

(define-public crate-memcache-0.0.1 (crate (name "memcache") (vers "0.0.1") (hash "1zgbkclg779gwy5bm1ii439w15jl1lqgqdhhknv5llgdk6vkcj5g")))

(define-public crate-memcache-0.0.2 (crate (name "memcache") (vers "0.0.2") (hash "1pgmm3gl1h61f68g84xv8abdl4fmygw5idf9zrc06hxm7b281j9b")))

(define-public crate-memcache-0.0.3 (crate (name "memcache") (vers "0.0.3") (hash "18hasi7z6aq4qq0mvibcm0qgqcbnsnyy1grr7rci9bn3c215g5fl")))

(define-public crate-memcache-0.0.4 (crate (name "memcache") (vers "0.0.4") (hash "1d3rp4spz1fkidcpmn7rax4sn11xbgp3xlvswi51cbj2cm6fnjpp")))

(define-public crate-memcache-0.0.5 (crate (name "memcache") (vers "0.0.5") (hash "06sp30sv1yj986cj02ypscjgcd65d275mww4givk3sf8ibfj601k")))

(define-public crate-memcache-0.0.6 (crate (name "memcache") (vers "0.0.6") (hash "1v6dndlc9k3x71ijvrm97lgbxvnv630z2l73p0nb33vx3rqpdna0")))

(define-public crate-memcache-0.0.7 (crate (name "memcache") (vers "0.0.7") (deps (list (crate-dep (name "libc") (req "*") (default-features #t) (kind 0)))) (hash "00c97iw8zy46nv3lpvy1px6wm9fixacyjmqm4pm6grv9hamyd7rf")))

(define-public crate-memcache-0.0.8 (crate (name "memcache") (vers "0.0.8") (deps (list (crate-dep (name "libc") (req "*") (default-features #t) (kind 0)))) (hash "0vmzx3jgccv1ps8pjpsb8xnhcrg5lgklniydkslzgsdnvaxc3wim")))

(define-public crate-memcache-0.0.9 (crate (name "memcache") (vers "0.0.9") (deps (list (crate-dep (name "libc") (req "*") (default-features #t) (kind 0)))) (hash "1wgbvni5s4y2grg7d73dpqy9n0pzn9bgvl6dap6lf6l2lajypvch")))

(define-public crate-memcache-0.0.10 (crate (name "memcache") (vers "0.0.10") (hash "1a80wrzh4i1szndgnly8vlssmg3x096v9d046gi72lhghdrrlwl0")))

(define-public crate-memcache-0.0.11 (crate (name "memcache") (vers "0.0.11") (hash "1rsj2zixqiav4rasyl3hfid128ll1hk64a1ni4sb4wl12s5w6688")))

(define-public crate-memcache-0.0.12 (crate (name "memcache") (vers "0.0.12") (hash "11f1x8jpk9sqlz0j5isfcwx4pxw4krh3v43v01yr7ry12fmndn9g")))

(define-public crate-memcache-0.0.13 (crate (name "memcache") (vers "0.0.13") (hash "1pjgxjj9fvr7iqyijaq5pl0x09f7nfj5mh0mpbgw9kljw6xiy2hn")))

(define-public crate-memcache-0.0.14 (crate (name "memcache") (vers "0.0.14") (hash "0cni6drvnpap5a7q9y7yw0rg3snr13bzxgyz9zcl7kidssa399sf")))

(define-public crate-memcache-0.0.15 (crate (name "memcache") (vers "0.0.15") (hash "0v3mh66rh5d8zjqymhk4fag1zf4bzfj6phf85vrz3wd3x9146yhg")))

(define-public crate-memcache-0.1 (crate (name "memcache") (vers "0.1.0") (deps (list (crate-dep (name "byteorder") (req "^1") (default-features #t) (kind 0)))) (hash "0r8m35acw5l4r0r5x3hwvbyf562kb9wwgicwvjclczz14c9nih22")))

(define-public crate-memcache-0.2 (crate (name "memcache") (vers "0.2.0") (deps (list (crate-dep (name "byteorder") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "url") (req "^1") (default-features #t) (kind 0)))) (hash "1w0d5c6nl6w51g761qgjva946402knmgpsl51xkjim7jkwnanhpq")))

(define-public crate-memcache-0.3 (crate (name "memcache") (vers "0.3.0") (deps (list (crate-dep (name "byteorder") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "url") (req "^1") (default-features #t) (kind 0)))) (hash "063vsn6vwskcarf2axmsrkbsgp40xggn6j4h4xgjzrnlmzx1imrj")))

(define-public crate-memcache-0.3 (crate (name "memcache") (vers "0.3.1") (deps (list (crate-dep (name "byteorder") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "url") (req "^1") (default-features #t) (kind 0)))) (hash "0x7caw0xz2d8cwhjhnvgfbhym61na5bsgdc1y6ia24hkp8jgn25r")))

(define-public crate-memcache-0.4 (crate (name "memcache") (vers "0.4.0") (deps (list (crate-dep (name "byteorder") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "url") (req "^1") (default-features #t) (kind 0)))) (hash "1g0238af2x80qza8bzqb3pm2yg3xxlpg0wprrcpd35vf6bp4cl5g")))

(define-public crate-memcache-0.5 (crate (name "memcache") (vers "0.5.0") (deps (list (crate-dep (name "byteorder") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "url") (req "^1") (default-features #t) (kind 0)))) (hash "1cl7i9d2c8rv3vfrqdzg32p2ssm5vq8a5qw8sq90m94jdlhc6apk")))

(define-public crate-memcache-0.6 (crate (name "memcache") (vers "0.6.0") (deps (list (crate-dep (name "byteorder") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "url") (req "^1") (default-features #t) (kind 0)))) (hash "08ffag9zyfw19fczs5lbb77nc4vjlqniw7m07bwpx1ybi5k7l15s")))

(define-public crate-memcache-0.7 (crate (name "memcache") (vers "0.7.0") (deps (list (crate-dep (name "byteorder") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "url") (req "^1") (default-features #t) (kind 0)))) (hash "1vabmlfh24lj0fq43k2sfhf1z5bs90l1m5q607b5wy9xfjkxqqz3")))

(define-public crate-memcache-0.7 (crate (name "memcache") (vers "0.7.1") (deps (list (crate-dep (name "byteorder") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "url") (req "^1") (default-features #t) (kind 0)))) (hash "11yfszcmkbr5iyb5691pjy2xh0l225h2mzy36vx4vq566ngikkln")))

(define-public crate-memcache-0.8 (crate (name "memcache") (vers "0.8.0") (deps (list (crate-dep (name "byteorder") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "url") (req "^1") (default-features #t) (kind 0)))) (hash "0qx016w7yzz446mbqirrkjv9bv0qyvaynqym98y348ypacwsgrwv")))

(define-public crate-memcache-0.9 (crate (name "memcache") (vers "0.9.0") (deps (list (crate-dep (name "byteorder") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "url") (req "^1") (default-features #t) (kind 0)))) (hash "0v59kfivl0s6k1w533rnk0kybvl9xars48ip96gqb3vcwc5mvz6a")))

(define-public crate-memcache-0.9 (crate (name "memcache") (vers "0.9.1") (deps (list (crate-dep (name "byteorder") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "url") (req "^1") (default-features #t) (kind 0)))) (hash "0msdg3qjvhc51h17k4pfb3r3rly8wxfc048vh3xk69wxda9vxi2w")))

(define-public crate-memcache-0.9 (crate (name "memcache") (vers "0.9.2") (deps (list (crate-dep (name "byteorder") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "url") (req "^1") (default-features #t) (kind 0)))) (hash "1dgspvb1kkdhfkbb09zibvxryjbmclczwsv2fbzkj2c6h3rpqjsq")))

(define-public crate-memcache-0.10 (crate (name "memcache") (vers "0.10.0") (deps (list (crate-dep (name "byteorder") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.6") (default-features #t) (kind 0)) (crate-dep (name "url") (req "^1") (default-features #t) (kind 0)))) (hash "0rmy6fzzbhhr97nscbiz9x9f9y4ymdlmmsb2g8q6rxii4sd08vxx")))

(define-public crate-memcache-0.11 (crate (name "memcache") (vers "0.11.0") (deps (list (crate-dep (name "byteorder") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.6") (default-features #t) (kind 0)) (crate-dep (name "url") (req "^1") (default-features #t) (kind 0)))) (hash "0z85ph78v548r9i2q0320jcbkc5i39zjp3n584di0n32glfsi4kq")))

(define-public crate-memcache-0.12 (crate (name "memcache") (vers "0.12.0") (deps (list (crate-dep (name "byteorder") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.6") (default-features #t) (kind 0)) (crate-dep (name "url") (req "^1") (default-features #t) (kind 0)))) (hash "0s9n6s4c58xyxi3ckgrjbbzgjx312zva1x6fax4iyk43c1f4a2lp")))

(define-public crate-memcache-0.12 (crate (name "memcache") (vers "0.12.1") (deps (list (crate-dep (name "byteorder") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.6") (default-features #t) (kind 0)) (crate-dep (name "url") (req "^1") (default-features #t) (kind 0)))) (hash "1p97ajfmgbv523ak6x4fc01gmcq7bhcvl9w50rxw2ykq4ifa0yqc")))

(define-public crate-memcache-0.12 (crate (name "memcache") (vers "0.12.2") (deps (list (crate-dep (name "byteorder") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.6") (default-features #t) (kind 0)) (crate-dep (name "url") (req "^1") (default-features #t) (kind 0)))) (hash "17cb4m1fg8wh36sszm267xdhm6p8xfa1h7dny6ddgdgrqyi1fa4q")))

(define-public crate-memcache-0.12 (crate (name "memcache") (vers "0.12.3") (deps (list (crate-dep (name "byteorder") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.6") (default-features #t) (kind 0)) (crate-dep (name "url") (req "^1") (default-features #t) (kind 0)))) (hash "10zkkb23x76xqqqkr1hlwyci9gf8wmnmbymagn05fszqkfdmx44x")))

(define-public crate-memcache-0.13 (crate (name "memcache") (vers "0.13.0") (deps (list (crate-dep (name "byteorder") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "enum_dispatch") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.6") (default-features #t) (kind 0)) (crate-dep (name "url") (req "^1") (default-features #t) (kind 0)))) (hash "0zmlylf8wglv49qdsinfzmc39y7kzhnnv4w43ahnv0xllkzmb8jr")))

(define-public crate-memcache-0.13 (crate (name "memcache") (vers "0.13.1") (deps (list (crate-dep (name "byteorder") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "enum_dispatch") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.7") (default-features #t) (kind 0)) (crate-dep (name "url") (req "^1") (default-features #t) (kind 0)))) (hash "04cqnwrci6g0zsfk7m48mkbykndxfjysh26aznhb0ds380nqi845")))

(define-public crate-memcache-0.14 (crate (name "memcache") (vers "0.14.0") (deps (list (crate-dep (name "byteorder") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "enum_dispatch") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "openssl") (req "^0.10") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.7") (default-features #t) (kind 0)) (crate-dep (name "url") (req "^2.1") (default-features #t) (kind 0)))) (hash "01wwls5jn5fwqw8dhglfpvlbv0s3wm2c9kakmgnfhrkim3a126y7") (features (quote (("tls" "openssl") ("default" "tls"))))))

(define-public crate-memcache-0.15 (crate (name "memcache") (vers "0.15.0") (deps (list (crate-dep (name "byteorder") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "enum_dispatch") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "openssl") (req "^0.10") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "r2d2") (req "^0.8.8") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.7") (default-features #t) (kind 0)) (crate-dep (name "url") (req "^2.1.1") (default-features #t) (kind 0)))) (hash "18a8rna0zqrzhcjfqydznspw9psd6683y988d6yzqdbkvs7dd1bw") (features (quote (("tls" "openssl") ("default" "tls"))))))

(define-public crate-memcache-0.15 (crate (name "memcache") (vers "0.15.1") (deps (list (crate-dep (name "byteorder") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "enum_dispatch") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "openssl") (req "^0.10") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "r2d2") (req "^0.8.8") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "url") (req "^2.1.1") (default-features #t) (kind 0)))) (hash "0w8cvicyw87vf5f7s5w7s019wd9g0giavpk51735is33y83px3l6") (features (quote (("tls" "openssl") ("default" "tls"))))))

(define-public crate-memcache-0.15 (crate (name "memcache") (vers "0.15.2") (deps (list (crate-dep (name "byteorder") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "enum_dispatch") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "openssl") (req "^0.10") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "r2d2") (req "^0.8.8") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "url") (req "^2.1.1") (default-features #t) (kind 0)))) (hash "10ws99f51x2kqfbrgd31mplgscyb0jk9g1br9a95d1w1f6kq7nmb") (features (quote (("tls" "openssl") ("default" "tls"))))))

(define-public crate-memcache-0.16 (crate (name "memcache") (vers "0.16.0") (deps (list (crate-dep (name "byteorder") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "enum_dispatch") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "openssl") (req "^0.10") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "r2d2") (req "^0.8.8") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "url") (req "^2.1.1") (default-features #t) (kind 0)))) (hash "0qgs0hbapka0q62j1prbid3afxsr60mji2n9xzs3zihp53h4syvm") (features (quote (("tls" "openssl") ("default" "tls"))))))

(define-public crate-memcache-0.17 (crate (name "memcache") (vers "0.17.0") (deps (list (crate-dep (name "byteorder") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "enum_dispatch") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "openssl") (req "^0.10") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "r2d2") (req "^0.8.8") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "url") (req "^2.1.1") (default-features #t) (kind 0)))) (hash "0lnbxgaflmadny0p8afnscv5m00yryhkp5sxv5h79lnkrkqasg06") (features (quote (("tls" "openssl") ("default" "tls"))))))

(define-public crate-memcache-0.17 (crate (name "memcache") (vers "0.17.1") (deps (list (crate-dep (name "byteorder") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "enum_dispatch") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "openssl") (req "^0.10") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "r2d2") (req "^0.8.8") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "url") (req "^2.1.1") (default-features #t) (kind 0)))) (hash "1f4m7g38cqqjh62fzwcsbc7dj6flqacclm5yiqk5xdgpmjyi93zv") (features (quote (("tls" "openssl") ("default" "tls"))))))

(define-public crate-memcache-0.17 (crate (name "memcache") (vers "0.17.2") (deps (list (crate-dep (name "byteorder") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "enum_dispatch") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "openssl") (req "^0.10") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "r2d2") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "url") (req "^2.1") (default-features #t) (kind 0)))) (hash "0fhmqw9crjzzpgp14wk52j7qjwwndssk66d5h32g4bjybm60i5h3") (features (quote (("tls" "openssl") ("default" "tls"))))))

(define-public crate-memcache-async-0.1 (crate (name "memcache-async") (vers "0.1.0") (deps (list (crate-dep (name "tokio") (req "^0.1") (features (quote ("async-await-preview"))) (default-features #t) (kind 0)))) (hash "1l1cs6psphljd3wsgh9s37f373fv0hm9b6k6z18c02hn5kxw03i0")))

(define-public crate-memcache-async-0.2 (crate (name "memcache-async") (vers "0.2.0") (deps (list (crate-dep (name "futures-preview") (req "^0.3.0-alpha.16") (features (quote ("compat"))) (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^0.1") (default-features #t) (kind 0)))) (hash "1kbmb3jcrbraq3igjkgrds7a4fqjyh4wy2pfp237wa5s5llgsijl")))

(define-public crate-memcache-async-0.2 (crate (name "memcache-async") (vers "0.2.1") (deps (list (crate-dep (name "futures-preview") (req "^0.3.0-alpha.16") (features (quote ("compat"))) (default-features #t) (kind 0)))) (hash "0yqfyqspymkbb85ndbgci3866yzfm3b1kr83bmyzqv6g2wsrvy5z")))

(define-public crate-memcache-async-0.2 (crate (name "memcache-async") (vers "0.2.2") (deps (list (crate-dep (name "futures-preview") (req "^0.3.0-alpha.16") (features (quote ("compat"))) (default-features #t) (kind 0)) (crate-dep (name "futures-preview") (req "^0.3.0-alpha.16") (features (quote ("compat" "io-compat"))) (default-features #t) (kind 2)) (crate-dep (name "tokio") (req "^0.1") (default-features #t) (kind 2)))) (hash "1bccscpkw11ar4p8ds6s9cwfnpywq0l0hfwdkksb6vmgdlnf4ad6")))

(define-public crate-memcache-async-0.3 (crate (name "memcache-async") (vers "0.3.0") (deps (list (crate-dep (name "futures") (req "^0.3") (default-features #t) (kind 0)))) (hash "1mv5cm0xhrlkyv6xhby1hly7p6gkifq7qhmrz3ar8aci4wc92qn4")))

(define-public crate-memcache-async-0.3 (crate (name "memcache-async") (vers "0.3.1") (deps (list (crate-dep (name "futures") (req "^0.3") (default-features #t) (kind 0)))) (hash "1vrzwgdgbi7nh53rvfap7ps6lv39pa4s6z2n5i24iyhln6hjghpn")))

(define-public crate-memcache-async-0.4 (crate (name "memcache-async") (vers "0.4.0") (deps (list (crate-dep (name "futures") (req "^0.3") (default-features #t) (kind 0)))) (hash "0010v9614yzx3x7ysvdg352sab7aym9ms71j4f5j0ia6xdfxmbb6")))

(define-public crate-memcache-async-0.4 (crate (name "memcache-async") (vers "0.4.1") (deps (list (crate-dep (name "futures") (req "^0.3") (default-features #t) (kind 0)))) (hash "0l51prdkfyphmdllmbfcacin4n7zbpkikiz0320618gll57nhdrx")))

(define-public crate-memcache-async-0.4 (crate (name "memcache-async") (vers "0.4.2") (deps (list (crate-dep (name "futures") (req "^0.3") (default-features #t) (kind 0)))) (hash "1qxayvsfln85k49rkib22d29w70zy49qlxcy991c90agg9f7qcxd")))

(define-public crate-memcache-async-0.4 (crate (name "memcache-async") (vers "0.4.3") (deps (list (crate-dep (name "futures") (req "^0.3") (default-features #t) (kind 0)))) (hash "08a81gzrypysqs15sqs059gx8bi0l095hjmhs83gy5w2kfynn32k")))

(define-public crate-memcache-async-0.5 (crate (name "memcache-async") (vers "0.5.0") (deps (list (crate-dep (name "futures") (req "^0.3") (default-features #t) (kind 0)))) (hash "148cqr0ivwxl455aash08g4m1z3mn0bgfpcyy5k3674j51fr6hff")))

(define-public crate-memcache-async-0.6 (crate (name "memcache-async") (vers "0.6.0") (deps (list (crate-dep (name "futures") (req "^0.3") (default-features #t) (kind 0)))) (hash "090vq6vxw89rs4b7c653y65714l0rwaqbzhima01cl20v5shg338")))

(define-public crate-memcache-async-0.6 (crate (name "memcache-async") (vers "0.6.1") (deps (list (crate-dep (name "futures") (req "^0.3") (default-features #t) (kind 0)))) (hash "0q0vmmn8dp79s13z9svjnd44cbjrjq59gs0vifjgj2al14dpjk7g")))

(define-public crate-memcache-async-0.6 (crate (name "memcache-async") (vers "0.6.2") (deps (list (crate-dep (name "futures") (req "^0.3") (default-features #t) (kind 0)))) (hash "13i03d7x4x4mka3z5mkc6w6y5xk6l9qbi5yxppx3b280lw2d1m1z")))

(define-public crate-memcache-async-0.6 (crate (name "memcache-async") (vers "0.6.3") (deps (list (crate-dep (name "futures") (req "^0.3") (default-features #t) (kind 0)))) (hash "0irn5zblfv6iwlx13pab813h7c727ha9r7f3d3bvmcpdmzc904l9")))

(define-public crate-memcache-async-0.6 (crate (name "memcache-async") (vers "0.6.4") (deps (list (crate-dep (name "futures") (req "^0.3") (default-features #t) (kind 0)))) (hash "1rsfxacwkipjw97igizc5jgj5b632fp19kbln4vln08kd88198b0")))

(define-public crate-memcache-async-0.7 (crate (name "memcache-async") (vers "0.7.0") (deps (list (crate-dep (name "futures") (req "^0.3") (default-features #t) (kind 0)))) (hash "1lk6a0aa8wll68hj0gndsa81fndnx2vk9hvlfc778xccpa39xzwa")))

(define-public crate-memcache-proto-0.0.1 (crate (name "memcache-proto") (vers "0.0.1") (deps (list (crate-dep (name "clippy") (req "^0.0") (optional #t) (default-features #t) (kind 0)))) (hash "0r79pl8xyhhq9a2s9rxakijppzk9ppz7r1jy2yk0nxdfsy8gwzk5")))

(define-public crate-memcached-0.1 (crate (name "memcached") (vers "0.1.0") (hash "05nv5s0py17pgxn453irqd6jdprl9b4x57vdmjvbdlpk25zdn9is")))

(define-public crate-memcached-0.2 (crate (name "memcached") (vers "0.2.0") (deps (list (crate-dep (name "async-std") (req "^1.5") (features (quote ("attributes"))) (default-features #t) (kind 0)) (crate-dep (name "byteorder") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "mobc") (req "^0.5") (features (quote ("async-std"))) (default-features #t) (kind 0)) (crate-dep (name "url") (req "^2.1.1") (default-features #t) (kind 0)))) (hash "0g99p77kzg0i58456zb43j1kzlhfkfzmf337x3kmwsjd84p96nza")))

(define-public crate-memcached-0.3 (crate (name "memcached") (vers "0.3.0") (deps (list (crate-dep (name "async-std") (req "^1.5") (features (quote ("attributes"))) (default-features #t) (kind 0)) (crate-dep (name "bincode") (req "^1.2.1") (default-features #t) (kind 0)) (crate-dep (name "byteorder") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "mobc") (req "^0.5") (features (quote ("async-std"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.110") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "url") (req "^2.1.1") (default-features #t) (kind 0)))) (hash "02c62zns5ig4dqp1ji5gdwygyhz5apa4i66ilwrgnc90x3frnbyp")))

(define-public crate-memcached-0.4 (crate (name "memcached") (vers "0.4.0") (deps (list (crate-dep (name "anyhow") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "async-std") (req "^1") (features (quote ("attributes"))) (default-features #t) (kind 0)) (crate-dep (name "bincode") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "byteorder") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "mobc") (req "^0.5") (features (quote ("async-std"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "url") (req "^2") (default-features #t) (kind 0)))) (hash "1g8lvzb9byqwvkiy3sprzhcnc8yqanm7bz3pgkcq3jzqhwg0bjd2")))

(define-public crate-memcached-0.4 (crate (name "memcached") (vers "0.4.1") (deps (list (crate-dep (name "anyhow") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "async-std") (req "^1") (features (quote ("attributes"))) (default-features #t) (kind 0)) (crate-dep (name "bincode") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "byteorder") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "mobc") (req "^0.5") (features (quote ("async-std"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "url") (req "^2") (default-features #t) (kind 0)))) (hash "0qw9sc3d3kj1656al1a02x4ja1c3zkj0c3dr4fv8gcbbb810mx9m")))

(define-public crate-memcached-0.4 (crate (name "memcached") (vers "0.4.2") (deps (list (crate-dep (name "async-std") (req "^1") (features (quote ("attributes"))) (default-features #t) (kind 0)) (crate-dep (name "bincode") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "byteorder") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "mobc") (req "^0.5") (features (quote ("async-std"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "url") (req "^2") (default-features #t) (kind 0)))) (hash "104azi86wlnaybjs5vn9zlmfkhh1gdnxn9f1vjl573rngawyjs3g")))

(define-public crate-memcached-0.4 (crate (name "memcached") (vers "0.4.3") (deps (list (crate-dep (name "async-std") (req "^1") (features (quote ("attributes"))) (default-features #t) (kind 0)) (crate-dep (name "bincode") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "byteorder") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "mobc") (req "^0.5") (features (quote ("async-std"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "url") (req "^2") (default-features #t) (kind 0)))) (hash "041ms3snq5s19a03f3g95h0q5nksjrmhzyflh3s2dkihr73558sx")))

(define-public crate-memcached-0.4 (crate (name "memcached") (vers "0.4.4") (deps (list (crate-dep (name "async-std") (req "^1") (features (quote ("attributes"))) (default-features #t) (kind 0)) (crate-dep (name "bincode") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "byteorder") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1") (default-features #t) (kind 2)) (crate-dep (name "mobc") (req "^0.5") (features (quote ("async-std"))) (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.7") (default-features #t) (kind 2)) (crate-dep (name "serde") (req "^1") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "url") (req "^2") (default-features #t) (kind 0)))) (hash "05mi8fvbd0mq08sd7ayi8c4y467ygwjrnyzpdbz7cpznxymq070k")))

(define-public crate-memcached-0.4 (crate (name "memcached") (vers "0.4.5-beta") (deps (list (crate-dep (name "async-std") (req "^1") (features (quote ("attributes"))) (default-features #t) (kind 0)) (crate-dep (name "bincode") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "byteorder") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1") (default-features #t) (kind 2)) (crate-dep (name "mobc") (req "^0.5") (features (quote ("async-std"))) (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.7") (default-features #t) (kind 2)) (crate-dep (name "serde") (req "^1") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "url") (req "^2") (default-features #t) (kind 0)))) (hash "0x032zp77lyjvappp9dw0wzxzn6nn1f9rc3fi655qlhhxz8nialx")))

(define-public crate-memcached-0.4 (crate (name "memcached") (vers "0.4.5-beta.2") (deps (list (crate-dep (name "async-std") (req "^1") (features (quote ("attributes"))) (default-features #t) (kind 0)) (crate-dep (name "bincode") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "byteorder") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1") (default-features #t) (kind 2)) (crate-dep (name "mobc") (req "^0.5") (features (quote ("async-std"))) (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.7") (default-features #t) (kind 2)) (crate-dep (name "serde") (req "^1") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "url") (req "^2") (default-features #t) (kind 0)))) (hash "0xxdy8inqn2n7av1c92c4hvg6j0hvvyqwgrhb7bhz0mjbfmd8pv2")))

(define-public crate-memcached-protocal-0.1 (crate (name "memcached-protocal") (vers "0.1.0") (deps (list (crate-dep (name "error-chain") (req "^0.1.12") (default-features #t) (kind 0)))) (hash "07j4x419n2waz9wqz1w1x7mjqhv0300v4jcihgsa4xhlcg1dwmpd")))

(define-public crate-memcached-protocal-0.1 (crate (name "memcached-protocal") (vers "0.1.1") (deps (list (crate-dep (name "error-chain") (req "^0.1.12") (default-features #t) (kind 0)))) (hash "0rfcqfhpjpk42fckz9v83q9avr94cs5slx1lkh3jlwmy396v3f3h")))

(define-public crate-memcached-protocal-0.1 (crate (name "memcached-protocal") (vers "0.1.2") (deps (list (crate-dep (name "error-chain") (req "^0.1.12") (default-features #t) (kind 0)))) (hash "18wa4d97m6b7mai2a0aa9fx547ni5d18bn6nh5nn4sdfyg0jj2b7")))

(define-public crate-memcached-protocal-0.1 (crate (name "memcached-protocal") (vers "0.1.3") (deps (list (crate-dep (name "error-chain") (req "^0.1.12") (default-features #t) (kind 0)))) (hash "1jzcjd8q6d2ay2k9hapn0yhq1ha201b5y5w8664z9g9ydskvjaq3")))

(define-public crate-memcached-protocal-0.1 (crate (name "memcached-protocal") (vers "0.1.4") (deps (list (crate-dep (name "error-chain") (req "^0.1.12") (default-features #t) (kind 0)))) (hash "0fycfdx2d897wxyapn5pw0fjdd2n5h4b0v4sds9wk7psm03fmj9c")))

(define-public crate-memcached-protocal-0.1 (crate (name "memcached-protocal") (vers "0.1.5") (deps (list (crate-dep (name "error-chain") (req "^0.1.12") (default-features #t) (kind 0)))) (hash "1vyyklkp7rw9wjd1g80g0qkraymzkr89g97g2b8m2xjxsd9lkffc")))

(define-public crate-memcached-protocal-0.1 (crate (name "memcached-protocal") (vers "0.1.6") (deps (list (crate-dep (name "error-chain") (req "^0.2") (default-features #t) (kind 0)))) (hash "0r89arj2qsd0n9sk8by3gr1pr204nql6qr7kbsiy5wg6886y96bm")))

(define-public crate-memcached-protocal-0.1 (crate (name "memcached-protocal") (vers "0.1.7") (deps (list (crate-dep (name "error-chain") (req "^0.2") (default-features #t) (kind 0)))) (hash "1kzfzsdabrlr1niv14g9vyyhfb0n9v33bgf02iwk4dp3sa2ln79d")))

(define-public crate-memcached-protocal-0.1 (crate (name "memcached-protocal") (vers "0.1.8") (deps (list (crate-dep (name "error-chain") (req "^0.2") (default-features #t) (kind 0)))) (hash "1j31xmmid7c1jrllair6n63fq41gn3w64akipk0pvww5j4askf9d")))

(define-public crate-memcached-protocal-0.1 (crate (name "memcached-protocal") (vers "0.1.9") (deps (list (crate-dep (name "error-chain") (req "^0.2") (default-features #t) (kind 0)))) (hash "1b93w1jcmz4k8y8lqbw4srvvz2i84il6shjnc69vhbv2h9xv23yb")))

(define-public crate-memcached-protocal-0.1 (crate (name "memcached-protocal") (vers "0.1.10") (deps (list (crate-dep (name "error-chain") (req "^0.2") (default-features #t) (kind 0)))) (hash "0mz2d1mmprhyajjqaxpkn4w4vip7bns4bmav1qnq1x5cg1v963gm")))

(define-public crate-memcached-protocal-0.1 (crate (name "memcached-protocal") (vers "0.1.11") (deps (list (crate-dep (name "error-chain") (req "^0.2") (default-features #t) (kind 0)))) (hash "0rh3fmpxip8bv3mzzafp4jpkz68p8ij1g0w3fg9am8vybkg9wx4j")))

(define-public crate-memcached-rs-0.0.1 (crate (name "memcached-rs") (vers "0.0.1") (deps (list (crate-dep (name "crc32") (req "*") (default-features #t) (kind 0)))) (hash "1qqxr15bymiac8zv2h6f7rzsljci3qx6xl0akf3sll05isqr5gw4")))

(define-public crate-memcached-rs-0.0.3 (crate (name "memcached-rs") (vers "0.0.3") (deps (list (crate-dep (name "byteorder") (req "*") (default-features #t) (kind 0)) (crate-dep (name "crc") (req "*") (default-features #t) (kind 0)) (crate-dep (name "log") (req "*") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "*") (default-features #t) (kind 0)) (crate-dep (name "semver") (req "*") (default-features #t) (kind 0)))) (hash "12k3pxnpd4x9b36q5bs5ckgpg3lk4i7yz80lalyi1vklk75vzpmn")))

(define-public crate-memcached-rs-0.0.4 (crate (name "memcached-rs") (vers "0.0.4") (deps (list (crate-dep (name "byteorder") (req "*") (default-features #t) (kind 0)) (crate-dep (name "crc") (req "*") (default-features #t) (kind 0)) (crate-dep (name "log") (req "*") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "*") (default-features #t) (kind 0)) (crate-dep (name "semver") (req "*") (default-features #t) (kind 0)))) (hash "1igfcprbz1f9v56kkq5q8wiy4336fa5vhn6xxfh6mwngvsch2h2q")))

(define-public crate-memcached-rs-0.0.5 (crate (name "memcached-rs") (vers "0.0.5") (deps (list (crate-dep (name "byteorder") (req "*") (default-features #t) (kind 0)) (crate-dep (name "crc") (req "*") (default-features #t) (kind 0)) (crate-dep (name "log") (req "*") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "*") (default-features #t) (kind 0)) (crate-dep (name "semver") (req "*") (default-features #t) (kind 0)))) (hash "15j2pjlfp30ni019pzf2k0g6x8gdj9fm0afii5qd1hx2nrkrday3")))

(define-public crate-memcached-rs-0.1 (crate (name "memcached-rs") (vers "0.1.0") (deps (list (crate-dep (name "byteorder") (req "*") (default-features #t) (kind 0)) (crate-dep (name "conhash") (req "*") (default-features #t) (kind 0)) (crate-dep (name "env_logger") (req "*") (default-features #t) (kind 0)) (crate-dep (name "log") (req "*") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "*") (default-features #t) (kind 0)) (crate-dep (name "semver") (req "*") (default-features #t) (kind 0)) (crate-dep (name "unix_socket") (req "*") (default-features #t) (kind 0)))) (hash "132b11ns2z6bb2h4kv5bhjjc6mr6k2czyx5w5s0ypxns1kfp3a8j")))

(define-public crate-memcached-rs-0.1 (crate (name "memcached-rs") (vers "0.1.1") (deps (list (crate-dep (name "byteorder") (req "*") (default-features #t) (kind 0)) (crate-dep (name "conhash") (req "*") (default-features #t) (kind 0)) (crate-dep (name "env_logger") (req "*") (default-features #t) (kind 0)) (crate-dep (name "log") (req "*") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "*") (default-features #t) (kind 0)) (crate-dep (name "semver") (req "*") (default-features #t) (kind 0)) (crate-dep (name "unix_socket") (req "*") (default-features #t) (kind 0)))) (hash "13q9i5ajgq6lw0l29qahcn2l5p4zl105ajcp5yqawsrql8wff4wm")))

(define-public crate-memcached-rs-0.1 (crate (name "memcached-rs") (vers "0.1.2") (deps (list (crate-dep (name "bufstream") (req "^0.1.1") (default-features #t) (kind 0)) (crate-dep (name "byteorder") (req "^0.4.2") (default-features #t) (kind 0)) (crate-dep (name "conhash") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "env_logger") (req "*") (default-features #t) (kind 2)) (crate-dep (name "log") (req "^0.3.4") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.3.13") (default-features #t) (kind 0)) (crate-dep (name "semver") (req "^0.2.1") (default-features #t) (kind 0)) (crate-dep (name "unix_socket") (req "^0.5.0") (default-features #t) (kind 0)))) (hash "0hw46155pcd4whza50a8v6nrya2zikx1isapzl66r2zp7abpmvrk") (features (quote (("nightly"))))))

(define-public crate-memcached-rs-0.1 (crate (name "memcached-rs") (vers "0.1.3") (deps (list (crate-dep (name "bufstream") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "byteorder") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "conhash") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "env_logger") (req "^0.4") (default-features #t) (kind 2)) (crate-dep (name "log") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "semver") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "unix_socket") (req "^0.5") (default-features #t) (target "cfg(unix)") (kind 0)))) (hash "1x26dx63mfhcmn5ixn810h5rz0cj9ys7jn5d0nqfzi1hn8my0brx") (features (quote (("nightly"))))))

(define-public crate-memcached-rs-0.1 (crate (name "memcached-rs") (vers "0.1.4") (deps (list (crate-dep (name "bufstream") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "byteorder") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "conhash") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "env_logger") (req "^0.4") (default-features #t) (kind 2)) (crate-dep (name "log") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "semver") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "unix_socket") (req "^0.5") (default-features #t) (target "cfg(unix)") (kind 0)))) (hash "0wc6fiyi2vf5a38pv1w0yc5fyji4dbxg2qplzpl35ih2za08dfwj") (features (quote (("nightly"))))))

(define-public crate-memcached-rs-0.2 (crate (name "memcached-rs") (vers "0.2.0") (deps (list (crate-dep (name "bufstream") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "byteorder") (req "^1.2") (default-features #t) (kind 0)) (crate-dep (name "conhash") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "env_logger") (req "^0.4") (default-features #t) (kind 2)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "semver") (req "^0.9") (default-features #t) (kind 0)) (crate-dep (name "unix_socket") (req "^0.5") (default-features #t) (target "cfg(unix)") (kind 0)))) (hash "0hg1adzgimwna8qspqiw8flvs4zsazfw0g48myxp8wqcxfq19rg1") (features (quote (("nightly"))))))

(define-public crate-memcached-rs-0.3 (crate (name "memcached-rs") (vers "0.3.0") (deps (list (crate-dep (name "bufstream") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "byteorder") (req "^1.2") (default-features #t) (kind 0)) (crate-dep (name "conhash") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "env_logger") (req "^0.5") (default-features #t) (kind 2)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.5") (default-features #t) (kind 0)) (crate-dep (name "semver") (req "^0.9") (default-features #t) (kind 0)) (crate-dep (name "unix_socket") (req "^0.5") (default-features #t) (target "cfg(unix)") (kind 0)))) (hash "0wsg74lnkxpf0l597079vmxg3m506w7bdgdjcvg7jxhk342nji8g") (features (quote (("nightly"))))))

(define-public crate-memcached-rs-0.4 (crate (name "memcached-rs") (vers "0.4.0") (deps (list (crate-dep (name "bufstream") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "byteorder") (req "^1.2") (default-features #t) (kind 0)) (crate-dep (name "conhash") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "env_logger") (req "^0.5") (default-features #t) (kind 2)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.5") (default-features #t) (kind 0)) (crate-dep (name "semver") (req "^0.9") (default-features #t) (kind 0)) (crate-dep (name "unix_socket") (req "^0.5") (default-features #t) (target "cfg(unix)") (kind 0)))) (hash "06126wrrpblzn0np78svq7698zfv3mr4snn66pwlf3gl6g29b1a1") (features (quote (("nightly"))))))

(define-public crate-memcached-rs-0.4 (crate (name "memcached-rs") (vers "0.4.1") (deps (list (crate-dep (name "bufstream") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "byteorder") (req "^1.2") (default-features #t) (kind 0)) (crate-dep (name "conhash") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "env_logger") (req "^0.5") (default-features #t) (kind 2)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.5") (default-features #t) (kind 0)) (crate-dep (name "semver") (req "^0.9") (default-features #t) (kind 0)) (crate-dep (name "unix_socket") (req "^0.5") (default-features #t) (target "cfg(unix)") (kind 0)))) (hash "0mmgq5m2hkjp3sn16kp23ci6lw4khcs46ahjc4zrd2j64q7qvkkv") (features (quote (("nightly"))))))

(define-public crate-memcached-rs-0.4 (crate (name "memcached-rs") (vers "0.4.2") (deps (list (crate-dep (name "bufstream") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "byteorder") (req "^1.2") (default-features #t) (kind 0)) (crate-dep (name "conhash") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "env_logger") (req "^0.7") (default-features #t) (kind 2)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.7") (default-features #t) (kind 0)) (crate-dep (name "semver") (req "^0.9") (default-features #t) (kind 0)) (crate-dep (name "unix_socket") (req "^0.5") (default-features #t) (target "cfg(unix)") (kind 0)))) (hash "078bxv85gm7b5g2z8mrdjm1bcdnz3wc5mv5ksqwwsmmmm11ld073") (features (quote (("nightly"))))))

(define-public crate-memcell-0.1 (crate (name "memcell") (vers "0.1.0") (hash "0d96cx3g4j82fjnjs6cyfnzzz0xaqkjbqanaav9vxvccyn8l8jib")))

(define-public crate-memcell-0.1 (crate (name "memcell") (vers "0.1.1") (hash "0psfd6gi1fcq9sfm4b4fcpck530qknff21k1zvswgzmjx10qfjmq")))

(define-public crate-memchain-0.1 (crate (name "memchain") (vers "0.1.0") (deps (list (crate-dep (name "blockchain-traits") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "mantle-types") (req "^0.1") (default-features #t) (kind 0)))) (hash "1fklbm4vmdnz1vnlml48fxpvxg07k72z52xyrfyy04x9k68g91zx") (features (quote (("ffi"))))))

(define-public crate-memchain-0.2 (crate (name "memchain") (vers "0.2.0") (deps (list (crate-dep (name "blockchain-traits") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "mantle-types") (req "^0.2") (default-features #t) (kind 0)))) (hash "0xqpjqlxfvak4xwkkp0drrdj3s9w5mdcbiw28fb4k6c73yfv5bs6") (features (quote (("ffi"))))))

(define-public crate-memchain-0.2 (crate (name "memchain") (vers "0.2.1") (deps (list (crate-dep (name "blockchain-traits") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "oasis-types") (req "^0.2") (default-features #t) (kind 0)))) (hash "1l044px255rpdnalbvarkvpjlzmkk1w05z6wsz0yrzpcihmwwmhl") (features (quote (("ffi"))))))

(define-public crate-memchain-0.2 (crate (name "memchain") (vers "0.2.2") (deps (list (crate-dep (name "blockchain-traits") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "oasis-types") (req "^0.2") (default-features #t) (kind 0)))) (hash "07qyamjdvr44y5zirmk85dccba8d4pi9wzb0rmxa0z8hi23lfz4y") (features (quote (("ffi"))))))

(define-public crate-memchain-0.2 (crate (name "memchain") (vers "0.2.3") (deps (list (crate-dep (name "blockchain-traits") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "oasis-types") (req "^0.2") (default-features #t) (kind 0)))) (hash "05zl94fz9rkx4qpqyhivfq079x6pn2pxifbpna5s0j78fda6iwj3") (features (quote (("ffi"))))))

(define-public crate-memchain-0.2 (crate (name "memchain") (vers "0.2.4") (deps (list (crate-dep (name "blockchain-traits") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "oasis-types") (req "^0.3") (default-features #t) (kind 0)))) (hash "0vxlrr9b52914xdlq8iavv9m8yvs2069cf8zgck7rrg4gh4x1s15") (features (quote (("ffi"))))))

(define-public crate-memchain-0.3 (crate (name "memchain") (vers "0.3.0") (deps (list (crate-dep (name "blockchain-traits") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "oasis-types") (req "^0.3") (default-features #t) (kind 0)))) (hash "0z1f3lpf0flpxi76r2a4y4nd4b73akwsdhm81jv5cn1rsczhi8wn") (features (quote (("ffi"))))))

(define-public crate-memchain-0.4 (crate (name "memchain") (vers "0.4.0") (deps (list (crate-dep (name "blockchain-traits") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "oasis-types") (req "^0.3") (default-features #t) (kind 0)))) (hash "014szhaxb6mxkwxbzq6jqgvmj142407ngq8g87ah7crlpww53773") (features (quote (("ffi"))))))

(define-public crate-memchain-0.4 (crate (name "memchain") (vers "0.4.1") (deps (list (crate-dep (name "blockchain-traits") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "oasis-types") (req "^0.4") (default-features #t) (kind 0)))) (hash "02s7371gbsv7im2z23sf3qdknxrc116y82mgi64gkdb8dygzin2h") (features (quote (("ffi"))))))

(define-public crate-memchr-0.1 (crate (name "memchr") (vers "0.1.1") (deps (list (crate-dep (name "libc") (req "0.1.*") (default-features #t) (kind 0)) (crate-dep (name "quickcheck") (req "0.2.*") (default-features #t) (kind 2)))) (hash "009h10v4qqg7mdh29nfql9i1hq5x65028dwh1fd7rjij6cxk9jdr")))

(define-public crate-memchr-0.1 (crate (name "memchr") (vers "0.1.2") (deps (list (crate-dep (name "libc") (req "0.1.*") (default-features #t) (kind 0)) (crate-dep (name "quickcheck") (req "0.2.*") (default-features #t) (kind 2)))) (hash "1431iggbh7lpspq2s9i65rn3z9fjvibzlvfpv9qkxqsa4har0ngx")))

(define-public crate-memchr-0.1 (crate (name "memchr") (vers "0.1.3") (deps (list (crate-dep (name "libc") (req "0.1.*") (default-features #t) (kind 0)) (crate-dep (name "quickcheck") (req "0.2.*") (default-features #t) (kind 2)))) (hash "003hhmk6fjrv7rwawglj4hfhcmdm6ll50znij3fzbvhrhqkhj41n")))

(define-public crate-memchr-0.1 (crate (name "memchr") (vers "0.1.4") (deps (list (crate-dep (name "libc") (req "0.1.*") (default-features #t) (kind 0)) (crate-dep (name "quickcheck") (req "0.2.*") (default-features #t) (kind 2)))) (hash "1frsn2glhbi6awhxslz4qxn35xpmqqb0x9l59d6vd6y9mpchc9w7")))

(define-public crate-memchr-0.1 (crate (name "memchr") (vers "0.1.5") (deps (list (crate-dep (name "libc") (req "0.1.*") (default-features #t) (kind 0)) (crate-dep (name "quickcheck") (req "0.2.*") (default-features #t) (kind 2)))) (hash "1ga20m1hfi15iyn383fx39j9vha7mi0hx3viwgfdmwm27grxn41v")))

(define-public crate-memchr-0.1 (crate (name "memchr") (vers "0.1.6") (deps (list (crate-dep (name "libc") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "quickcheck") (req "^0.2") (default-features #t) (kind 2)))) (hash "0plwzhpk3zqbay7zhld0xqmxhbhr7lb75klj4d86qhy3i6bcqv2y")))

(define-public crate-memchr-0.1 (crate (name "memchr") (vers "0.1.7") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "quickcheck") (req "^0.2") (default-features #t) (kind 2)))) (hash "1arhmy1x8n765sprmm1p63a26s7i1ndw9shwd286bbpngfav0rnw")))

(define-public crate-memchr-0.1 (crate (name "memchr") (vers "0.1.8") (deps (list (crate-dep (name "libc") (req "^0.2.4") (default-features #t) (kind 0)) (crate-dep (name "quickcheck") (req "^0.2") (default-features #t) (kind 2)))) (hash "0q3blf458l92kyp8z3s4dm34i5536cfp4wsrfkgjwjlds9a9z50y")))

(define-public crate-memchr-0.1 (crate (name "memchr") (vers "0.1.9") (deps (list (crate-dep (name "libc") (req "^0.2.4") (default-features #t) (kind 0)) (crate-dep (name "quickcheck") (req "^0.2") (default-features #t) (kind 2)))) (hash "17y6fxqdknvkaa9cn53rkbhp8ywlcn7rm9ckk6ds5hv9hdk2hsix")))

(define-public crate-memchr-0.1 (crate (name "memchr") (vers "0.1.10") (deps (list (crate-dep (name "libc") (req "^0.2.4") (default-features #t) (kind 0)) (crate-dep (name "quickcheck") (req "^0.2") (default-features #t) (kind 2)))) (hash "1jqilk0567y2x6rygs2krjwaqlcb6c6wb3g4zz34bqk3f9cxp2n9")))

(define-public crate-memchr-0.1 (crate (name "memchr") (vers "0.1.11") (deps (list (crate-dep (name "libc") (req "^0.2.4") (default-features #t) (kind 0)) (crate-dep (name "quickcheck") (req "^0.2") (default-features #t) (kind 2)))) (hash "084d85hjfa3xf5kwdms2mhbkh78m1gl2254cp5swcxj3a7xjkdnq")))

(define-public crate-memchr-1 (crate (name "memchr") (vers "1.0.0") (deps (list (crate-dep (name "libc") (req "^0.2.18") (default-features #t) (kind 0)) (crate-dep (name "quickcheck") (req "^0.4.1") (default-features #t) (kind 2)))) (hash "0a5fvj7pr422jgdmfw1fmnz00k68lw1wwd0z7cwiqwzhk29894kl")))

(define-public crate-memchr-1 (crate (name "memchr") (vers "1.0.1") (deps (list (crate-dep (name "libc") (req "^0.2.18") (kind 0)) (crate-dep (name "quickcheck") (req "^0.4.1") (default-features #t) (kind 2)))) (hash "1d27b8kk2zijj6f9qlcwa4q71lkbm72nfvbyy6wlgspi8q7crg0x") (features (quote (("use_std" "libc/use_std") ("default" "use_std"))))))

(define-public crate-memchr-1 (crate (name "memchr") (vers "1.0.2") (deps (list (crate-dep (name "libc") (req "^0.2.18") (optional #t) (kind 0)) (crate-dep (name "quickcheck") (req "^0.4.1") (default-features #t) (kind 2)))) (hash "0yjyja34pzhipdl855q3m21w1lyih4lw79x2dp3czwdla4pap3ql") (features (quote (("use_std" "libc" "libc/use_std") ("default" "use_std" "libc"))))))

(define-public crate-memchr-2 (crate (name "memchr") (vers "2.0.0") (deps (list (crate-dep (name "libc") (req "^0.2.18") (optional #t) (kind 0)) (crate-dep (name "quickcheck") (req "^0.4.1") (kind 2)))) (hash "19pq4yl7rnqrjc0vi7wpkq0k2sgz507gxr6q17yff63x07cn87p0") (features (quote (("use_std" "libc" "libc/use_std") ("default" "use_std" "libc"))))))

(define-public crate-memchr-2 (crate (name "memchr") (vers "2.0.1") (deps (list (crate-dep (name "libc") (req "^0.2.18") (optional #t) (kind 0)) (crate-dep (name "quickcheck") (req "^0.5") (kind 2)))) (hash "17dsj7az388szacn3k6pzph6ky2s5wl4azyfsag5h4k6wxqblvvr") (features (quote (("use_std" "libc" "libc/use_std") ("default" "use_std" "libc"))))))

(define-public crate-memchr-2 (crate (name "memchr") (vers "2.0.2") (deps (list (crate-dep (name "libc") (req "^0.2.18") (optional #t) (kind 0)) (crate-dep (name "quickcheck") (req "^0.7") (kind 2)))) (hash "0gfra8akd9hscqmakadxy5zh9pqi7kw09xwn332pi2kkp0m19d53") (features (quote (("use_std" "libc" "libc/use_std") ("default" "use_std" "libc"))))))

(define-public crate-memchr-2 (crate (name "memchr") (vers "2.1.0") (deps (list (crate-dep (name "cfg-if") (req "^0.1.5") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2.18") (optional #t) (kind 0)) (crate-dep (name "quickcheck") (req "^0.7") (kind 2)) (crate-dep (name "version_check") (req "^0.1.4") (default-features #t) (kind 1)))) (hash "0fsz337jz0iv0sl1zl530p51lp1m11y0zf9kdjm6vzyvkzz2jdjb") (features (quote (("use_std" "libc" "libc/use_std") ("default" "use_std" "libc"))))))

(define-public crate-memchr-2 (crate (name "memchr") (vers "2.1.1") (deps (list (crate-dep (name "cfg-if") (req "^0.1.5") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2.18") (optional #t) (kind 0)) (crate-dep (name "quickcheck") (req "^0.7") (kind 2)) (crate-dep (name "version_check") (req "^0.1.4") (default-features #t) (kind 1)))) (hash "05kabmzrrsbbnhj44yhcvjm09spb55q430cn37qjjnaky01b0gha") (features (quote (("use_std" "libc" "libc/use_std") ("default" "use_std" "libc"))))))

(define-public crate-memchr-2 (crate (name "memchr") (vers "2.1.2") (deps (list (crate-dep (name "cfg-if") (req "^0.1.5") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2.18") (optional #t) (kind 0)) (crate-dep (name "quickcheck") (req "^0.7") (kind 2)) (crate-dep (name "version_check") (req "^0.1.4") (default-features #t) (kind 1)))) (hash "1s86jbrp02i4a7izgbmmmyfc1rddv6qjca7l91kfgxipi4ql2k6v") (features (quote (("use_std" "libc" "libc/use_std") ("default" "use_std" "libc"))))))

(define-public crate-memchr-2 (crate (name "memchr") (vers "2.1.3") (deps (list (crate-dep (name "cfg-if") (req "^0.1.5") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2.18") (optional #t) (kind 0)) (crate-dep (name "quickcheck") (req "^0.8") (kind 2)))) (hash "1f7wcg604blvnmnps2qpp5ajkn57xm195vdngvh2rhwqqam4xpg1") (features (quote (("use_std" "libc" "libc/use_std") ("default" "use_std" "libc"))))))

(define-public crate-memchr-2 (crate (name "memchr") (vers "2.2.0") (deps (list (crate-dep (name "libc") (req "^0.2.18") (optional #t) (kind 0)) (crate-dep (name "quickcheck") (req "^0.8") (kind 2)))) (hash "0f8wdra7yaggsr4jzlrvpd8yknnqhd990iijdr6llgc8gk2ppz1f") (features (quote (("use_std") ("default" "use_std"))))))

(define-public crate-memchr-2 (crate (name "memchr") (vers "2.2.1") (deps (list (crate-dep (name "libc") (req "^0.2.18") (optional #t) (kind 0)) (crate-dep (name "quickcheck") (req "^0.8") (kind 2)))) (hash "13j6ji9x9ydpi9grbss106gqqr3xn3bcfp28aydqfa4751qrfmw8") (features (quote (("use_std") ("default" "use_std"))))))

(define-public crate-memchr-2 (crate (name "memchr") (vers "2.3.0") (deps (list (crate-dep (name "libc") (req "^0.2.18") (optional #t) (kind 0)) (crate-dep (name "quickcheck") (req "^0.9") (kind 2)))) (hash "08r2qw1wwvvahicjj94ci05qx3ya5ixgrpbiq23kya6vgq6f55ri") (features (quote (("use_std" "std") ("std") ("default" "std"))))))

(define-public crate-memchr-2 (crate (name "memchr") (vers "2.3.1") (deps (list (crate-dep (name "libc") (req "^0.2.18") (optional #t) (kind 0)) (crate-dep (name "quickcheck") (req "^0.9") (kind 2)))) (hash "0c4ybd8mx5r63xvi84xm2ljjb0yvjrrih682kz68mfw09ryq21hl") (features (quote (("use_std" "std") ("std") ("default" "std")))) (yanked #t)))

(define-public crate-memchr-2 (crate (name "memchr") (vers "2.3.2") (deps (list (crate-dep (name "libc") (req "^0.2.18") (optional #t) (kind 0)) (crate-dep (name "quickcheck") (req "^0.9") (kind 2)))) (hash "0y1rjc2afcxdf8lg4h31ivlfil242s2ivilpfiwkdx51h7imsi2k") (features (quote (("use_std" "std") ("std") ("default" "std"))))))

(define-public crate-memchr-2 (crate (name "memchr") (vers "2.3.3") (deps (list (crate-dep (name "libc") (req "^0.2.18") (optional #t) (kind 0)) (crate-dep (name "quickcheck") (req "^0.9") (kind 2)))) (hash "0074pvsfl938ndl5js14ibc7i9q0k3zp390z843w8nlyv4bxha1p") (features (quote (("use_std" "std") ("std") ("default" "std"))))))

(define-public crate-memchr-2 (crate (name "memchr") (vers "2.3.4") (deps (list (crate-dep (name "libc") (req "^0.2.18") (optional #t) (kind 0)) (crate-dep (name "quickcheck") (req "^0.9") (kind 2)))) (hash "098m9clfs495illlw00hv2gg67mhm7jflld3msyclvi5m9xc9q8f") (features (quote (("use_std" "std") ("std") ("default" "std"))))))

(define-public crate-memchr-2 (crate (name "memchr") (vers "2.4.0") (deps (list (crate-dep (name "libc") (req "^0.2.18") (optional #t) (kind 0)) (crate-dep (name "quickcheck") (req "^1.0.3") (kind 2)))) (hash "1p478fqf4nia2ma0kv4npb8x1hli0zz6k16517ikb51jkryx8sxi") (features (quote (("use_std" "std") ("std") ("default" "std"))))))

(define-public crate-memchr-2 (crate (name "memchr") (vers "2.4.1") (deps (list (crate-dep (name "compiler_builtins") (req "^0.1.2") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "core") (req "^1.0.0") (optional #t) (default-features #t) (kind 0) (package "rustc-std-workspace-core")) (crate-dep (name "libc") (req "^0.2.18") (optional #t) (kind 0)) (crate-dep (name "quickcheck") (req "^1.0.3") (kind 2)))) (hash "0smq8xzd40njqpfzv5mghigj91fzlfrfg842iz8x0wqvw2dw731h") (features (quote (("use_std" "std") ("std") ("rustc-dep-of-std" "core" "compiler_builtins") ("default" "std"))))))

(define-public crate-memchr-2 (crate (name "memchr") (vers "2.5.0") (deps (list (crate-dep (name "compiler_builtins") (req "^0.1.2") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "core") (req "^1.0.0") (optional #t) (default-features #t) (kind 0) (package "rustc-std-workspace-core")) (crate-dep (name "libc") (req "^0.2.18") (optional #t) (kind 0)) (crate-dep (name "quickcheck") (req "^1.0.3") (kind 2)))) (hash "0vanfk5mzs1g1syqnj03q8n0syggnhn55dq535h2wxr7rwpfbzrd") (features (quote (("use_std" "std") ("std") ("rustc-dep-of-std" "core" "compiler_builtins") ("default" "std"))))))

(define-public crate-memchr-2 (crate (name "memchr") (vers "2.6.0") (deps (list (crate-dep (name "compiler_builtins") (req "^0.1.2") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "core") (req "^1.0.0") (optional #t) (default-features #t) (kind 0) (package "rustc-std-workspace-core")) (crate-dep (name "log") (req "^0.4.20") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "quickcheck") (req "^1.0.3") (kind 2)))) (hash "0b0y8n7w277y7nll8mr3nvd903r9xwmgcv1wpinl6nwdb3i49z3n") (features (quote (("use_std" "std") ("std" "alloc") ("rustc-dep-of-std" "core" "compiler_builtins") ("libc") ("default" "std") ("alloc")))) (v 2) (features2 (quote (("logging" "dep:log"))))))

(define-public crate-memchr-2 (crate (name "memchr") (vers "2.6.1") (deps (list (crate-dep (name "compiler_builtins") (req "^0.1.2") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "core") (req "^1.0.0") (optional #t) (default-features #t) (kind 0) (package "rustc-std-workspace-core")) (crate-dep (name "log") (req "^0.4.20") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "quickcheck") (req "^1.0.3") (kind 2)))) (hash "1bil0li3iav4x3vlxwfl9ddgpba61rj35x3vjs38x7sdv27r8y7l") (features (quote (("use_std" "std") ("std" "alloc") ("rustc-dep-of-std" "core" "compiler_builtins") ("libc") ("default" "std") ("alloc")))) (v 2) (features2 (quote (("logging" "dep:log")))) (rust-version "1.60")))

(define-public crate-memchr-2 (crate (name "memchr") (vers "2.6.2") (deps (list (crate-dep (name "compiler_builtins") (req "^0.1.2") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "core") (req "^1.0.0") (optional #t) (default-features #t) (kind 0) (package "rustc-std-workspace-core")) (crate-dep (name "log") (req "^0.4.20") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "quickcheck") (req "^1.0.3") (kind 2)))) (hash "0vnqrsfm5260gcxyb83ipfd68d51l3azpm81i8dyc6320b8ax1jl") (features (quote (("use_std" "std") ("std" "alloc") ("rustc-dep-of-std" "core" "compiler_builtins") ("libc") ("default" "std") ("alloc")))) (v 2) (features2 (quote (("logging" "dep:log")))) (rust-version "1.60")))

(define-public crate-memchr-2 (crate (name "memchr") (vers "2.6.3") (deps (list (crate-dep (name "compiler_builtins") (req "^0.1.2") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "core") (req "^1.0.0") (optional #t) (default-features #t) (kind 0) (package "rustc-std-workspace-core")) (crate-dep (name "log") (req "^0.4.20") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "quickcheck") (req "^1.0.3") (kind 2)))) (hash "0p6kn2awqf47m3brk0nmajarhwhylg9969il8dm9bq87yxp2s8wg") (features (quote (("use_std" "std") ("std" "alloc") ("rustc-dep-of-std" "core" "compiler_builtins") ("libc") ("default" "std") ("alloc")))) (v 2) (features2 (quote (("logging" "dep:log")))) (rust-version "1.61")))

(define-public crate-memchr-2 (crate (name "memchr") (vers "2.6.4") (deps (list (crate-dep (name "compiler_builtins") (req "^0.1.2") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "core") (req "^1.0.0") (optional #t) (default-features #t) (kind 0) (package "rustc-std-workspace-core")) (crate-dep (name "log") (req "^0.4.20") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "quickcheck") (req "^1.0.3") (kind 2)))) (hash "0rq1ka8790ns41j147npvxcqcl2anxyngsdimy85ag2api0fwrgn") (features (quote (("use_std" "std") ("std" "alloc") ("rustc-dep-of-std" "core" "compiler_builtins") ("libc") ("default" "std") ("alloc")))) (v 2) (features2 (quote (("logging" "dep:log")))) (rust-version "1.61")))

(define-public crate-memchr-2 (crate (name "memchr") (vers "2.7.0") (deps (list (crate-dep (name "compiler_builtins") (req "^0.1.2") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "core") (req "^1.0.0") (optional #t) (default-features #t) (kind 0) (package "rustc-std-workspace-core")) (crate-dep (name "log") (req "^0.4.20") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "quickcheck") (req "^1.0.3") (kind 2)))) (hash "0i1y9w504vnzz6ccc1wgv8np3jpccw7ghxgvgyd92ly809jmrn6b") (features (quote (("use_std" "std") ("std" "alloc") ("rustc-dep-of-std" "core" "compiler_builtins") ("libc") ("default" "std") ("alloc")))) (v 2) (features2 (quote (("logging" "dep:log")))) (rust-version "1.61")))

(define-public crate-memchr-2 (crate (name "memchr") (vers "2.7.1") (deps (list (crate-dep (name "compiler_builtins") (req "^0.1.2") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "core") (req "^1.0.0") (optional #t) (default-features #t) (kind 0) (package "rustc-std-workspace-core")) (crate-dep (name "log") (req "^0.4.20") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "quickcheck") (req "^1.0.3") (kind 2)))) (hash "0jf1kicqa4vs9lyzj4v4y1p90q0dh87hvhsdd5xvhnp527sw8gaj") (features (quote (("use_std" "std") ("std" "alloc") ("rustc-dep-of-std" "core" "compiler_builtins") ("libc") ("default" "std") ("alloc")))) (v 2) (features2 (quote (("logging" "dep:log")))) (rust-version "1.61")))

(define-public crate-memchr-2 (crate (name "memchr") (vers "2.7.2") (deps (list (crate-dep (name "compiler_builtins") (req "^0.1.2") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "core") (req "^1.0.0") (optional #t) (default-features #t) (kind 0) (package "rustc-std-workspace-core")) (crate-dep (name "log") (req "^0.4.20") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "quickcheck") (req "^1.0.3") (kind 2)))) (hash "07bcqxb0vx4ji0648ny5xsicjnpma95x1n07v7mi7jrhsz2l11kc") (features (quote (("use_std" "std") ("std" "alloc") ("rustc-dep-of-std" "core" "compiler_builtins") ("libc") ("default" "std") ("alloc")))) (v 2) (features2 (quote (("logging" "dep:log")))) (rust-version "1.61")))

(define-public crate-memcmp-0.0.1 (crate (name "memcmp") (vers "0.0.1") (hash "0qskhgwwmyppmmclhx1135xzrj0n1ammr8zslxjfgg3qc6bi3gfc")))

(define-public crate-memcmp-0.0.2 (crate (name "memcmp") (vers "0.0.2") (hash "067xzbwh55dw6swcf3ls1lm57sb63is99dp0hzld72zsn4v0xl8g")))

(define-public crate-memcmp-0.0.4 (crate (name "memcmp") (vers "0.0.4") (hash "0caap8yba1d3h8l56xx6m96wnczhh62yaf8yzkv0y69xhk7049mv")))

(define-public crate-memcmp-0.0.5 (crate (name "memcmp") (vers "0.0.5") (hash "1ry5wfi7vjg6hcq166hansrahwjary55adgpj6w4f0217v90n323")))

(define-public crate-memcmp-0.0.6 (crate (name "memcmp") (vers "0.0.6") (hash "1qa4b56avh3s4nah46aq9f25gghvdknlsm92krda1djpwgz486ws") (features (quote (("nightly"))))))

(define-public crate-memcom-0.1 (crate (name "memcom") (vers "0.1.0") (deps (list (crate-dep (name "ctrlc") (req "^3.2") (default-features #t) (kind 0)) (crate-dep (name "lapp") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "memmap") (req "^0.7") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.152") (features (quote ("derive"))) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0.152") (default-features #t) (kind 0)) (crate-dep (name "shared-mem-queue") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "toml") (req "^0.6.0") (default-features #t) (kind 0)) (crate-dep (name "try-ascii") (req "^1.0.0") (default-features #t) (kind 0)))) (hash "0m81bn9p9v4g7g8v194c3xscm3vgb0ylyiddklz3lcz3mrbig0d0")))

(define-public crate-memcomparable-0.1 (crate (name "memcomparable") (vers "0.1.0") (deps (list (crate-dep (name "bytes") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "criterion") (req "^0.4") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.8") (default-features #t) (kind 2)) (crate-dep (name "rust_decimal") (req "^1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "rust_decimal") (req "^1") (features (quote ("rand"))) (default-features #t) (kind 2)) (crate-dep (name "serde") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1") (features (quote ("derive"))) (default-features #t) (kind 2)) (crate-dep (name "thiserror") (req "^1") (default-features #t) (kind 0)))) (hash "1xfsxp77vjnyjr01h9q2w19hypjhz9f1p5rry1r96ss5ahmx0gcy") (features (quote (("decimal" "rust_decimal"))))))

(define-public crate-memcomparable-0.1 (crate (name "memcomparable") (vers "0.1.1") (deps (list (crate-dep (name "bytes") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "criterion") (req "^0.4") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.8") (default-features #t) (kind 2)) (crate-dep (name "rust_decimal") (req "^1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "rust_decimal") (req "^1") (features (quote ("rand"))) (default-features #t) (kind 2)) (crate-dep (name "serde") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1") (features (quote ("derive"))) (default-features #t) (kind 2)) (crate-dep (name "thiserror") (req "^1") (default-features #t) (kind 0)))) (hash "0xkfd2fc7jyimb8n1f5np9v2x3hhni2wr2a7yvmvgwnmhxcq8lyq") (features (quote (("decimal" "rust_decimal"))))))

(define-public crate-memcomparable-0.2 (crate (name "memcomparable") (vers "0.2.0") (deps (list (crate-dep (name "bytes") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "criterion") (req "^0.4") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.8") (default-features #t) (kind 2)) (crate-dep (name "rust_decimal") (req "^1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "rust_decimal") (req "^1") (features (quote ("rand"))) (default-features #t) (kind 2)) (crate-dep (name "serde") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1") (features (quote ("derive"))) (default-features #t) (kind 2)) (crate-dep (name "thiserror") (req "^1") (default-features #t) (kind 0)))) (hash "0xx6ziq59ss4w102wgfcnl1x1cwj1wc6w882b4nm1z34v7dh2q9p") (features (quote (("decimal" "rust_decimal"))))))

(define-public crate-memconstruct-0.1 (crate (name "memconstruct") (vers "0.1.0") (deps (list (crate-dep (name "memconstruct_macros") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "15qbcas2fzbvdm1g14bwv98ssqj10z6472k2x0s1mdz96gyaynf8")))

(define-public crate-memconstruct_macros-0.1 (crate (name "memconstruct_macros") (vers "0.1.0") (deps (list (crate-dep (name "proc-macro2") (req "^1.0.52") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.26") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2.0.0") (features (quote ("extra-traits" "full"))) (default-features #t) (kind 0)))) (hash "1bpikgakx1b4242ff0xmzh7baw8wjiq8jm851a5fwsphn4nym8h6")))

(define-public crate-memcrab-0.1 (crate (name "memcrab") (vers "0.1.0") (hash "1ylqfjsinlhjgxl666pv6bj7jnjsh52jq4n62c2zzmjiw5vy46nn")))

(define-public crate-memcrab-cache-0.1 (crate (name "memcrab-cache") (vers "0.1.0") (hash "0r5g4sv00bx0yc4z4sgxd5d5cs8zansh9finxsnhshngixsph5s2")))

(define-public crate-memcrab-cli-0.1 (crate (name "memcrab-cli") (vers "0.1.0") (hash "0gws5ij6smz7dnzzaxgdkfmygvp32h2i74f871qx7lg19h9rizbn")))

(define-public crate-memcrab-server-0.1 (crate (name "memcrab-server") (vers "0.1.0") (hash "12zging6xcgj6sbmisl38agsziaq4jp8szdj4gs5q1b3vfgwngrq")))

