(define-module (crates-io me dm) #:use-module (crates-io))

(define-public crate-medmodels-0.0.1 (crate (name "medmodels") (vers "0.0.1-a1") (deps (list (crate-dep (name "medmodels-core") (req "^0.0.1-a1") (default-features #t) (kind 0)))) (hash "0l8izncdxvbcn39miwwxh15ydqnhy3950vh6qin93n838hp0swhh")))

(define-public crate-medmodels-core-0.0.1 (crate (name "medmodels-core") (vers "0.0.1-a1") (deps (list (crate-dep (name "medmodels-utils") (req "^0.0.1-a1") (default-features #t) (kind 0)) (crate-dep (name "polars") (req "^0.39.2") (default-features #t) (kind 0)) (crate-dep (name "ron") (req "^0.8.1") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.199") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "0fq3l36rfnlkwnhnx4v2mf0xkflvkshyrnjxhdq2ap9bj9603b4w")))

(define-public crate-medmodels-utils-0.0.1 (crate (name "medmodels-utils") (vers "0.0.1-a1") (deps (list (crate-dep (name "hashbrown") (req "^0.14.5") (features (quote ("serde"))) (default-features #t) (kind 0)))) (hash "17ym7i1pdv7lxjgw69k29svz98fyfxg9h3mbjszanwidq071kdx4")))

