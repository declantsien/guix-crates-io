(define-module (crates-io me le) #:use-module (crates-io))

(define-public crate-melee_inject-0.1 (crate (name "melee_inject") (vers "0.1.0") (deps (list (crate-dep (name "gc-gcm") (req "^0.10") (default-features #t) (kind 0)))) (hash "0kc5x659zgsxlgncnd700a7rlyppfv76khj5vvnxi8hb6j55hqcn")))

