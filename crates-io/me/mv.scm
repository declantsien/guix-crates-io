(define-module (crates-io me mv) #:use-module (crates-io))

(define-public crate-memvec-0.0.1 (crate (name "memvec") (vers "0.0.1") (deps (list (crate-dep (name "memmap2") (req "^0.5.3") (default-features #t) (kind 0)) (crate-dep (name "static_assertions") (req "^1.1.0") (default-features #t) (kind 2)))) (hash "1y5mdq78dhv8lk32phvb3fczsxcj0rjwj7pk6pb8pxxm9i4dvpbx") (yanked #t)))

(define-public crate-memvec-0.0.2 (crate (name "memvec") (vers "0.0.2") (deps (list (crate-dep (name "memmap2") (req "^0.5.3") (default-features #t) (kind 0)) (crate-dep (name "static_assertions") (req "^1.1.0") (default-features #t) (kind 2)))) (hash "14w6gm2m7sdghj1sizar2j5irlh4i30cyn99i0yjrpnmlsgxddw3")))

(define-public crate-memvec-0.0.3 (crate (name "memvec") (vers "0.0.3") (deps (list (crate-dep (name "memmap2") (req "^0.5.3") (default-features #t) (kind 0)) (crate-dep (name "static_assertions") (req "^1.1.0") (default-features #t) (kind 2)))) (hash "1pasx5bxh1brip5cvrx3jydfaj29fldpn8wjqp5ki475h7rkxadq")))

(define-public crate-memvec-0.0.4 (crate (name "memvec") (vers "0.0.4") (deps (list (crate-dep (name "memmap2") (req "^0.5.3") (default-features #t) (kind 0)) (crate-dep (name "static_assertions") (req "^1.1.0") (default-features #t) (kind 2)))) (hash "004q8w9ngd4cqsbg37aisnxzif281sd33fkfzgdmx8g70bc691jg")))

(define-public crate-memvec-0.0.5 (crate (name "memvec") (vers "0.0.5") (deps (list (crate-dep (name "memmap2") (req "^0.5.3") (default-features #t) (kind 0)) (crate-dep (name "static_assertions") (req "^1.1.0") (default-features #t) (kind 2)))) (hash "1f09lx668is7hq6v82bl4s5nk6mx6q2swgdibk85avwjcw7pvwal")))

(define-public crate-memvec-0.1 (crate (name "memvec") (vers "0.1.0") (deps (list (crate-dep (name "memmap2") (req "^0.5.3") (default-features #t) (kind 0)) (crate-dep (name "static_assertions") (req "^1.1.0") (default-features #t) (kind 2)))) (hash "1pnwr50h6jdx5xc3xdz0971xz4j41ifkx3jzj2pnzx97w21q61wm")))

