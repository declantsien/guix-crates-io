(define-module (crates-io me ca) #:use-module (crates-io))

(define-public crate-mecab-0.0.1 (crate (name "mecab") (vers "0.0.1") (deps (list (crate-dep (name "libc") (req "*") (default-features #t) (kind 0)))) (hash "1jq5v40f76x86jkz47wnl4vsvgdsnn1fb64zxa8a96fnk1k8pspr")))

(define-public crate-mecab-0.1 (crate (name "mecab") (vers "0.1.0") (deps (list (crate-dep (name "libc") (req "*") (default-features #t) (kind 0)))) (hash "04ps06c070rbc26cdyd57yr85w3fzq42ihqw4x3yxc1j59xm4sr8")))

(define-public crate-mecab-0.1 (crate (name "mecab") (vers "0.1.1") (deps (list (crate-dep (name "libc") (req "*") (default-features #t) (kind 0)))) (hash "04fqcsmq60p6icailxkidfr2iq0xak0iqlc1vv5z6895l3505qva")))

(define-public crate-mecab-0.1 (crate (name "mecab") (vers "0.1.2") (deps (list (crate-dep (name "libc") (req "*") (default-features #t) (kind 0)))) (hash "18ijma357814dw2m2pkzfsxpiqb9m3c4ky5xm9xfdvxwr8j2nql4")))

(define-public crate-mecab-0.1 (crate (name "mecab") (vers "0.1.3") (deps (list (crate-dep (name "libc") (req "^0.2.1") (default-features #t) (kind 0)))) (hash "1hbxhmhrkz38ilyj4q7js9xsimj5iw0c3ws6b4ngsk4idqcsl1vw")))

(define-public crate-mecab-0.1 (crate (name "mecab") (vers "0.1.4") (hash "0pwg0zxkc2kdmbc5zkfffc78rzb29w2hyy87iji70mzlzvhiz1bh")))

(define-public crate-mecab-0.1 (crate (name "mecab") (vers "0.1.5") (hash "0x378nb51vwr000gy2p84mlh4zjgxr986vj2xycw7xlcm7k8422q")))

(define-public crate-mecab-0.1 (crate (name "mecab") (vers "0.1.6") (hash "1fyfhp1zqazii0h2l9j9smm7vak93i159r6favca0vh770q54iza")))

