(define-module (crates-io me ij) #:use-module (crates-io))

(define-public crate-meiji-0.0.0 (crate (name "meiji") (vers "0.0.0") (hash "052zyp4kdqbi0sy47a74kkdvm0pkj5xaqvngw4ivzr0dskm4lmq1")))

(define-public crate-meiji-executors-0.0.0 (crate (name "meiji-executors") (vers "0.0.0") (hash "0agm091icgfhkkv7nf0xn8b3k02c9jq8zr8c0fw22g610l9ppd8f")))

(define-public crate-meiji-fibers-0.0.0 (crate (name "meiji-fibers") (vers "0.0.0") (hash "0bh6byjn2j7lw57qp4c4imv5zvdvbblbzxmj5sfgha6l0l89xlsi")))

(define-public crate-meiji-futures-0.0.0 (crate (name "meiji-futures") (vers "0.0.0") (hash "11w07c17wdrb4likgqc6r7rny474arvm8ly2l9ffs4b5s6dkf7an")))

(define-public crate-meiji-primitives-0.0.0 (crate (name "meiji-primitives") (vers "0.0.0") (hash "1ya156k89fpr2ir05m4mklfy09bm2ma25w2hh1vkhpn18la3ixcc")))

(define-public crate-meiji-test-0.0.0 (crate (name "meiji-test") (vers "0.0.0") (hash "1582cjdy580mg9514gx9qc3imlzxbawd87l54izbls89x8z8j5bh")))

