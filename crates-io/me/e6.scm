(define-module (crates-io me e6) #:use-module (crates-io))

(define-public crate-mee6-0.0.1 (crate (name "mee6") (vers "0.0.1") (hash "0xwmwlgd59fq2v1xqivzgcmw03hx5gnvqnmak1m0isl5x3pm48c6")))

(define-public crate-mee6-0.0.2 (crate (name "mee6") (vers "0.0.2") (hash "18mi90c214g53ymdig8d890wgpmbsr0y4pb82darbz71ygx3wjfg")))

(define-public crate-mee6-0.0.3 (crate (name "mee6") (vers "0.0.3") (hash "0fjjdijqjijzg3add4wff4jlcp63wqybwzl294n3kfxb4dz4fp12")))

(define-public crate-mee6-0.1 (crate (name "mee6") (vers "0.1.0") (hash "073131shg28lrbrbmv2k4l8nl6si4sm18l9rhkchrz08rwkc5fn0")))

