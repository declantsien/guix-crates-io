(define-module (crates-io me al) #:use-module (crates-io))

(define-public crate-meal-1 (crate (name "meal") (vers "1.0.0") (deps (list (crate-dep (name "reqwest") (req "^0.11") (features (quote ("blocking"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.0") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0.0") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.0") (default-features #t) (kind 0)) (crate-dep (name "urlencoding") (req "^2.1.0") (default-features #t) (kind 0)))) (hash "05h255yamil79vq51xkj1c9rzlqdlrj3s3v76bb1j5gvwyhwy6xg")))

(define-public crate-mealdb-0.1 (crate (name "mealdb") (vers "0.1.0") (deps (list (crate-dep (name "async-trait") (req "^0.1.50") (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.11.4") (features (quote ("json"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.126") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.64") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.7.1") (features (quote ("full"))) (default-features #t) (kind 2)))) (hash "17s3lgcx02fl55jy51wb4p427sbdds2p4qxz31m4hm5acqkifpyp")))

(define-public crate-mealdb-0.1 (crate (name "mealdb") (vers "0.1.1") (deps (list (crate-dep (name "async-trait") (req "^0.1.50") (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.11.4") (features (quote ("json"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.126") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.64") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.7.1") (features (quote ("full"))) (default-features #t) (kind 2)))) (hash "0zr0psq7iq6gd1q9lxxy7drbgsa112q61hmw8kh7xqa9fm7p4dpi")))

(define-public crate-mealy-0.1 (crate (name "mealy") (vers "0.1.0") (hash "1282axj21fqvixxca7faxxnnckjh8xi7hzy5mpg7abi3baj3n4wr")))

(define-public crate-mealy-0.1 (crate (name "mealy") (vers "0.1.1") (hash "0w7wswdnji4ii14b7wz6kba4siv1h06nz5j21qwx51zhkp8pjz4b")))

(define-public crate-mealy-0.1 (crate (name "mealy") (vers "0.1.2") (hash "0n17p8rylgcr90rzvl7wpgk6fvrxjm87f5ld0mm51g9nhms29mcz")))

