(define-module (crates-io me ne) #:use-module (crates-io))

(define-public crate-menemen-0.1 (crate (name "menemen") (vers "0.1.0-alpha") (deps (list (crate-dep (name "anyhow") (req "^1.0.52") (default-features #t) (kind 0)) (crate-dep (name "dns-lookup") (req "^1.0.8") (default-features #t) (kind 0)) (crate-dep (name "openssl") (req "^0.10.36") (default-features #t) (kind 0)))) (hash "0fixc0bh1pnbsjyqwxcr951r46x0bl6069gy8h50w3shlc7hn208")))

(define-public crate-menemen-0.2 (crate (name "menemen") (vers "0.2.0-alpha") (deps (list (crate-dep (name "anyhow") (req "^1.0.52") (default-features #t) (kind 0)) (crate-dep (name "bufstream") (req "^0.1.4") (default-features #t) (kind 0)) (crate-dep (name "openssl") (req "^0.10.36") (default-features #t) (kind 0)))) (hash "17zzfr6vja2pj1hh4rww5w658sdsrr7bb7n92c46k83q831ggqnr")))

(define-public crate-menemen-0.2 (crate (name "menemen") (vers "0.2.1-alpha") (deps (list (crate-dep (name "anyhow") (req "^1.0.52") (default-features #t) (kind 0)) (crate-dep (name "bufstream") (req "^0.1.4") (default-features #t) (kind 0)) (crate-dep (name "openssl") (req "^0.10.36") (default-features #t) (kind 0)))) (hash "0778wg092w5iqh4v6mqd60wylf4mc0cwmrssdy547n52z4rxn4fz")))

(define-public crate-menemen-0.2 (crate (name "menemen") (vers "0.2.2-alpha") (deps (list (crate-dep (name "anyhow") (req "^1.0.52") (default-features #t) (kind 0)) (crate-dep (name "bufstream") (req "^0.1.4") (default-features #t) (kind 0)) (crate-dep (name "openssl") (req "^0.10.36") (default-features #t) (kind 0)))) (hash "1hfp1k9dg4qr4b509mvnp7y8hnmq383ypz8hs5lvdcyp4sracz0h")))

(define-public crate-menemen-0.3 (crate (name "menemen") (vers "0.3.2-alpha") (deps (list (crate-dep (name "anyhow") (req "^1.0.52") (default-features #t) (kind 0)) (crate-dep (name "bufstream") (req "^0.1.4") (default-features #t) (kind 0)) (crate-dep (name "openssl") (req "^0.10.36") (optional #t) (default-features #t) (kind 0)))) (hash "157kcxg4hr4y0dyc6zbfq75pzgzrpkzqy7vgkkprr3vn0nmmqc38") (features (quote (("https" "openssl") ("default"))))))

(define-public crate-menemen-1 (crate (name "menemen") (vers "1.0.0") (deps (list (crate-dep (name "anyhow") (req "^1.0.52") (default-features #t) (kind 0)) (crate-dep (name "bufstream") (req "^0.1.4") (default-features #t) (kind 0)) (crate-dep (name "native-tls") (req "^0.2") (default-features #t) (kind 0)))) (hash "1lxjzxwwyp8754jdxwb4amx75jn6ikaq2xb7pzb7f913l65n38ij")))

(define-public crate-menemen-1 (crate (name "menemen") (vers "1.0.1") (deps (list (crate-dep (name "anyhow") (req "^1.0.52") (default-features #t) (kind 0)) (crate-dep (name "bufstream") (req "^0.1.4") (default-features #t) (kind 0)) (crate-dep (name "native-tls") (req "^0.2") (default-features #t) (kind 0)))) (hash "1i25wav9w9grc2k79z0l1xa5cb5lyrp2bc9synbwfsbxjr1xnlrk")))

(define-public crate-menemen-1 (crate (name "menemen") (vers "1.0.2") (deps (list (crate-dep (name "anyhow") (req "^1.0.52") (default-features #t) (kind 0)) (crate-dep (name "bufstream") (req "^0.1.4") (default-features #t) (kind 0)) (crate-dep (name "libflate") (req "^1.3.0") (default-features #t) (kind 2)) (crate-dep (name "native-tls") (req "^0.2") (default-features #t) (kind 0)))) (hash "0n5d6xsll27hx6zp1dn2f9r4r8qbyv94xp9ngywn4wpn53zadb73") (yanked #t)))

(define-public crate-menemen-1 (crate (name "menemen") (vers "1.0.3") (deps (list (crate-dep (name "anyhow") (req "^1.0.52") (default-features #t) (kind 0)) (crate-dep (name "bufstream") (req "^0.1.4") (default-features #t) (kind 0)) (crate-dep (name "libflate") (req "^1.3.0") (default-features #t) (kind 2)) (crate-dep (name "native-tls") (req "^0.2") (default-features #t) (kind 0)))) (hash "1i4sb17y51da1xfmh398pw1xi2h6isc6a2g7c6lydmcmkybl3zbw")))

