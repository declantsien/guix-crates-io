(define-module (crates-io me pp) #:use-module (crates-io))

(define-public crate-mepple-0.1 (crate (name "mepple") (vers "0.1.0") (deps (list (crate-dep (name "sqlite") (req "^0.25.1") (default-features #t) (kind 1)))) (hash "00akhfbbirigsdkmlsznvqq7qd5vqjjz93wfb2gyaylbmh1f5z4n")))

(define-public crate-mepple-0.2 (crate (name "mepple") (vers "0.2.0") (deps (list (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "sqlite") (req "^0.25.1") (default-features #t) (kind 1)))) (hash "0fd95jji6cv2miwk0zkg1qw8g9hw18x72qrs7ainfv24h92vd248")))

