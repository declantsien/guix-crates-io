(define-module (crates-io me rx) #:use-module (crates-io))

(define-public crate-merx-0.0.1 (crate (name "merx") (vers "0.0.1") (deps (list (crate-dep (name "criterion") (req "^0.3") (features (quote ("real_blackbox"))) (default-features #t) (kind 2)) (crate-dep (name "quickcheck") (req "^0.9") (default-features #t) (kind 2)) (crate-dep (name "quickcheck_macros") (req "^0.9") (default-features #t) (kind 2)))) (hash "1y3167vicr8nydn518y2aq0n67xzqly53f8wyh32p43vc9ijgnlp")))

