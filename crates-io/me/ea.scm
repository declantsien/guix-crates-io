(define-module (crates-io me ea) #:use-module (crates-io))

(define-public crate-meealgi-0.0.1 (crate (name "meealgi") (vers "0.0.1") (deps (list (crate-dep (name "chrono") (req "~0.4") (default-features #t) (kind 0)))) (hash "1d6cgkwmz75d6k8mgnbkrff4h64j1jcjrj9jf574jhc68b48cmwx")))

(define-public crate-meealgi-0.0.2 (crate (name "meealgi") (vers "0.0.2") (deps (list (crate-dep (name "chrono") (req "~0.4") (default-features #t) (kind 0)))) (hash "0fm86cfmm6svgwfqf2l9pf732bg41p97zixd1zrci0y75xf7yx7c")))

(define-public crate-meealgi-0.0.3 (crate (name "meealgi") (vers "0.0.3") (deps (list (crate-dep (name "chrono") (req "~0.4") (default-features #t) (kind 0)))) (hash "0zxmfxkqcz66brfb3f1sci8ghlfz63i1yzhy6phcxlh3kxsqcvc8")))

(define-public crate-meealgi-0.0.5 (crate (name "meealgi") (vers "0.0.5") (deps (list (crate-dep (name "chrono") (req "~0.4") (default-features #t) (kind 0)) (crate-dep (name "clippy") (req "~0.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "spectral") (req "^0.6.0") (default-features #t) (kind 2)))) (hash "1vn9pg3p8izr9x3c8l5irkr9ycmivfrn1pshwxxlakmnkhyij7hr") (features (quote (("default"))))))

(define-public crate-meealgi-0.0.6 (crate (name "meealgi") (vers "0.0.6") (deps (list (crate-dep (name "chrono") (req "~0.4") (default-features #t) (kind 0)) (crate-dep (name "clippy") (req "~0.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "spectral") (req "^0.6.0") (default-features #t) (kind 2)))) (hash "0w7dlwsb7ys1576k1njpakv801cy17sf0nq1a21nq1my123azk63") (features (quote (("default"))))))

