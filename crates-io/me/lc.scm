(define-module (crates-io me lc) #:use-module (crates-io))

(define-public crate-melcloud-api-0.1 (crate (name "melcloud-api") (vers "0.1.0") (deps (list (crate-dep (name "mockito") (req "^0.17.0") (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.9.12") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.89") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0.89") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.39") (default-features #t) (kind 0)))) (hash "1r0qi85aglh1wrm1idaf7ylnjg5mrsp70c4nx330vh9dqgkdzfcl")))

(define-public crate-melcloud-api-0.1 (crate (name "melcloud-api") (vers "0.1.1") (deps (list (crate-dep (name "chrono") (req "^0.4.9") (default-features #t) (kind 0)) (crate-dep (name "mockito") (req "^0.17.0") (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.9.12") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.89") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0.89") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.39") (default-features #t) (kind 0)))) (hash "193la7khbkl4f3pgh5y0ywxj401j10nqq671p6b78mmigk5a0yxj")))

(define-public crate-melcome-0.0.1 (crate (name "melcome") (vers "0.0.1") (deps (list (crate-dep (name "ang") (req "^0.6.0") (default-features #t) (kind 0)) (crate-dep (name "uuid") (req "^1.1.2") (features (quote ("v4"))) (default-features #t) (kind 0)))) (hash "1gky75x60x7x9l93bdx9pj39ypc548rxyb6x0wslsc2jsbvl0ds2")))

