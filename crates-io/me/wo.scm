(define-module (crates-io me wo) #:use-module (crates-io))

(define-public crate-mewo-0.1 (crate (name "mewo") (vers "0.1.1") (deps (list (crate-dep (name "mewo_ecs") (req "^0.1.1") (default-features #t) (kind 0)) (crate-dep (name "mewo_ecs_derive") (req "^0.1.1") (optional #t) (default-features #t) (kind 0)))) (hash "1a3xm39l6awq9hnbv6pyhqfgpf6m5mqfrd2plx1a3sjil53qcnr7") (features (quote (("default" "derive")))) (v 2) (features2 (quote (("derive" "dep:mewo_ecs_derive"))))))

(define-public crate-mewo-0.1 (crate (name "mewo") (vers "0.1.2") (deps (list (crate-dep (name "mewo_ecs") (req "^0.1.2") (default-features #t) (kind 0)) (crate-dep (name "mewo_ecs_derive") (req "^0.1.2") (optional #t) (default-features #t) (kind 0)))) (hash "0lmix3v4lm9g3z0v01adz8945gak0pb3700mq8nac93b1vijiixw") (features (quote (("default" "derive")))) (v 2) (features2 (quote (("derive" "dep:mewo_ecs_derive"))))))

(define-public crate-mewo-0.1 (crate (name "mewo") (vers "0.1.3") (deps (list (crate-dep (name "mewo_ecs") (req "^0.1.3") (default-features #t) (kind 0)) (crate-dep (name "mewo_ecs_derive") (req "^0.1.3") (optional #t) (default-features #t) (kind 0)))) (hash "1fp804djxgqsz2q744840gxjr3fgkn5nrvq9pc5jirx6xmxhkm64") (features (quote (("default" "derive")))) (v 2) (features2 (quote (("derive" "dep:mewo_ecs_derive"))))))

(define-public crate-mewo-0.1 (crate (name "mewo") (vers "0.1.4") (deps (list (crate-dep (name "mewo_ecs") (req "^0.1.4") (default-features #t) (kind 0)) (crate-dep (name "mewo_ecs_derive") (req "^0.1.4") (optional #t) (default-features #t) (kind 0)))) (hash "0c85x6931rmqmg0jxbnchz8wd733wcnpzy3vdi1zcdgb4xgsd9hz") (features (quote (("default" "derive")))) (v 2) (features2 (quote (("derive" "dep:mewo_ecs_derive"))))))

(define-public crate-mewo_ecs-0.1 (crate (name "mewo_ecs") (vers "0.1.0") (deps (list (crate-dep (name "parking_lot") (req "^0.12") (default-features #t) (kind 0)))) (hash "0k8cxjz2y50af3923nhycg27h7153jhjgyas0cnsslag97nn6c0v") (yanked #t)))

(define-public crate-mewo_ecs-0.1 (crate (name "mewo_ecs") (vers "0.1.1") (deps (list (crate-dep (name "parking_lot") (req "^0.12") (default-features #t) (kind 0)))) (hash "0n83cm2cpf1s9zj47vnby18k5p39vhdyk61jaw0whs2ax1is1gn1")))

(define-public crate-mewo_ecs-0.1 (crate (name "mewo_ecs") (vers "0.1.2") (deps (list (crate-dep (name "parking_lot") (req "^0.12") (default-features #t) (kind 0)))) (hash "0ykydgbyas6fxz67l1ljvhq9s2cr4iwvf1qqhms7hpf0ph2ydplx")))

(define-public crate-mewo_ecs-0.1 (crate (name "mewo_ecs") (vers "0.1.3") (deps (list (crate-dep (name "parking_lot") (req "^0.12") (default-features #t) (kind 0)))) (hash "09y4w1iv4n3sz4w2i0295h9v69aj332824zzycly9jarfg4ln793")))

(define-public crate-mewo_ecs-0.1 (crate (name "mewo_ecs") (vers "0.1.4") (deps (list (crate-dep (name "parking_lot") (req "^0.12") (default-features #t) (kind 0)))) (hash "0kqbxpkws7r7swrf06kprzx84kda72xp8x19mgn0bh0jdb5mxl6c")))

(define-public crate-mewo_ecs_derive-0.1 (crate (name "mewo_ecs_derive") (vers "0.1.0") (deps (list (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (default-features #t) (kind 0)))) (hash "1gapigp07j7mgr7m8gnc2fz1slv9d38ri01ys5p68wnw6cm28dja") (yanked #t)))

(define-public crate-mewo_ecs_derive-0.1 (crate (name "mewo_ecs_derive") (vers "0.1.1") (deps (list (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (default-features #t) (kind 0)))) (hash "1c2j8w8m28l9fbxba7yk55wg8flc1j1mr9d8yq2v17jmm97cvvkr")))

(define-public crate-mewo_ecs_derive-0.1 (crate (name "mewo_ecs_derive") (vers "0.1.2") (deps (list (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (default-features #t) (kind 0)))) (hash "06xlk3lw0f3vjqajxr0xysdkd4xcwg6qrw4kmk64dbagap0cyiay")))

(define-public crate-mewo_ecs_derive-0.1 (crate (name "mewo_ecs_derive") (vers "0.1.3") (deps (list (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (default-features #t) (kind 0)))) (hash "1x33bqwdjqrx25wdnhilfxjva57p196phc5yfvxzajr3mfd1zq2x")))

(define-public crate-mewo_ecs_derive-0.1 (crate (name "mewo_ecs_derive") (vers "0.1.4") (deps (list (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (default-features #t) (kind 0)))) (hash "188shxfnq0cx20yagw9p33i0ah1klwwsbskiy52bh7srq9rgdfgl")))

