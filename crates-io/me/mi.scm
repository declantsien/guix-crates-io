(define-module (crates-io me mi) #:use-module (crates-io))

(define-public crate-meminfo-0.1 (crate (name "meminfo") (vers "0.1.0") (hash "12jfr8gi1qb64838qwl4y9l6dvxs7cid3f8105bf5qby91sy2qmf")))

(define-public crate-meminfo-0.1 (crate (name "meminfo") (vers "0.1.1") (deps (list (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)))) (hash "1qkgrbdyj8nfbwvpf3h9n9fibvrjjdg0hv3c57mvylh322b9lhkh") (features (quote (("default"))))))

(define-public crate-meminterval-0.1 (crate (name "meminterval") (vers "0.1.0") (deps (list (crate-dep (name "num") (req "^0.4") (kind 0)))) (hash "0syjnk4871q0qqdw4aj0y7dvm90i36ldzmzm8zpw7xp0a5grzljb")))

(define-public crate-meminterval-0.2 (crate (name "meminterval") (vers "0.2.0") (deps (list (crate-dep (name "num") (req "^0.4") (kind 0)))) (hash "1jjbz7i2vp7b7dk6h8qj2ifdr308992w7fxf40qp0yb4gnk81pzf")))

(define-public crate-meminterval-0.3 (crate (name "meminterval") (vers "0.3.0") (deps (list (crate-dep (name "num") (req "^0.4") (kind 0)))) (hash "09i6w7mbrhks6fvwix8gngcxs80nd5sx76hnk3z4zng2mi4ps7jz")))

(define-public crate-meminterval-0.4 (crate (name "meminterval") (vers "0.4.0") (deps (list (crate-dep (name "num") (req "^0.4") (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("alloc" "derive"))) (optional #t) (kind 0)))) (hash "1bw0x0sj205gncjfwdid7fjqgirnm5vvzfkd26fklciam321pbyp") (v 2) (features2 (quote (("serde" "dep:serde"))))))

(define-public crate-meminterval-0.4 (crate (name "meminterval") (vers "0.4.1") (deps (list (crate-dep (name "num-traits") (req "^0.2") (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("alloc" "derive"))) (optional #t) (kind 0)))) (hash "0j7jkd4sqn3gigydrh6yzlilj4sgq0qd6f113jz53ljmz1663y66") (v 2) (features2 (quote (("serde" "dep:serde"))))))

(define-public crate-memio-0.0.1 (crate (name "memio") (vers "0.0.1") (hash "0722dbmp0x3zg8vnpfrlm44g54b6m59dslbfgpcqwgq2ar3s92wm")))

