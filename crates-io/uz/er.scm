(define-module (crates-io uz er) #:use-module (crates-io))

(define-public crate-uzero-0.1 (crate (name "uzero") (vers "0.1.0") (deps (list (crate-dep (name "num-traits") (req "^0.2") (kind 0)))) (hash "09nxgm19f1aj88baclm80y3yfinsl86f4khacs318br2rz7ghzxr")))

(define-public crate-uzers-0.11 (crate (name "uzers") (vers "0.11.1") (deps (list (crate-dep (name "env_logger") (req "^0.7") (kind 2)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4") (optional #t) (kind 0)))) (hash "0rm4mfph8sj7vi70f71gd1fx6cclcjrkvcb36gqznvfxxw47zxvn") (features (quote (("mock") ("logging" "log") ("default" "cache" "mock" "logging") ("cache"))))))

(define-public crate-uzers-0.11 (crate (name "uzers") (vers "0.11.2") (deps (list (crate-dep (name "env_logger") (req "^0.7") (kind 2)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4") (optional #t) (kind 0)))) (hash "1k5wsfvxsvs8l65wspcwhbfrkw0k85kbk5db3bawl7grydi6am29") (features (quote (("mock") ("logging" "log") ("default" "cache" "mock" "logging") ("cache"))))))

(define-public crate-uzers-0.11 (crate (name "uzers") (vers "0.11.3") (deps (list (crate-dep (name "env_logger") (req "^0.7") (kind 2)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4") (optional #t) (kind 0)))) (hash "0qrzbhncbv4s52lgyzs2pxn1b6gmx9k7h1rdwdwix44cgvf87lkn") (features (quote (("mock") ("logging" "log") ("default" "cache" "mock" "logging") ("cache"))))))

(define-public crate-uzers-0.12 (crate (name "uzers") (vers "0.12.0") (deps (list (crate-dep (name "env_logger") (req "^0.7") (kind 2)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4") (optional #t) (kind 0)) (crate-dep (name "serial_test") (req "^2.0") (default-features #t) (kind 2)))) (hash "1r4d8i3ji1m1s2nr9vrvn0ilk9jihbzq7kpg94akp6ym2rg8g1bx") (features (quote (("test-integration") ("mock") ("logging" "log") ("default" "cache" "mock" "logging") ("cache"))))))

