(define-module (crates-io jh -r) #:use-module (crates-io))

(define-public crate-jh-rs-0.1 (crate (name "jh-rs") (vers "0.1.0") (deps (list (crate-dep (name "jh-ffi") (req "^0.5.0") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.8.3") (default-features #t) (kind 2)))) (hash "110nyls2aqm36j3xwq442q2qkwf8bifimbnincxb2nn7065kkd1v")))

