(define-module (crates-io jh -f) #:use-module (crates-io))

(define-public crate-jh-ffi-0.0.1 (crate (name "jh-ffi") (vers "0.0.1") (deps (list (crate-dep (name "gcc") (req "^0.3") (default-features #t) (kind 1)))) (hash "0x0kgam8z161v8wbhxd45dl96cms4y73b0qn7pmlskpg2ngxa4ff")))

(define-public crate-jh-ffi-0.0.2 (crate (name "jh-ffi") (vers "0.0.2") (deps (list (crate-dep (name "gcc") (req "^0.3") (default-features #t) (kind 1)))) (hash "0l920zvmbp3z8nxvjh63knkchlvazrybmjvycxc7qpg9wyww1kff")))

(define-public crate-jh-ffi-0.5 (crate (name "jh-ffi") (vers "0.5.0") (deps (list (crate-dep (name "cc") (req "^1.0.26") (default-features #t) (kind 1)))) (hash "1hacbzslymcazadjjmnygc9c6b97gp03gdy5ai30x4qb6yjfxkhv")))

