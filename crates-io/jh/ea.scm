(define-module (crates-io jh ea) #:use-module (crates-io))

(define-public crate-jheap_histo-0.1 (crate (name "jheap_histo") (vers "0.1.0") (deps (list (crate-dep (name "chrono") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "combine") (req "^4.4") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^0.2") (features (quote ("fs"))) (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^0.2") (features (quote ("macros" "rt-threaded"))) (default-features #t) (kind 2)))) (hash "05r731kx8lh44c5nspfcjv27gz8c8j0cq38i0jllpk52f8z4qysc")))

