(define-module (crates-io iz ia) #:use-module (crates-io))

(define-public crate-izia-0.1 (crate (name "izia") (vers "0.1.0") (deps (list (crate-dep (name "linefeed") (req "^0.5.3") (default-features #t) (kind 0)) (crate-dep (name "zia") (req "^0.1") (default-features #t) (kind 0)))) (hash "0d07fgagxzr69661v9b1s7w6ab5d5vwwzxvzsc77l70q7jw1kp70")))

(define-public crate-izia-0.2 (crate (name "izia") (vers "0.2.0") (deps (list (crate-dep (name "linefeed") (req "^0.5.3") (default-features #t) (kind 0)) (crate-dep (name "zia") (req "^0") (default-features #t) (kind 0)))) (hash "0dih9klm1gwvsn9ji3x7qvjaag3bjmf2kkmzrvhzh06ilw5sa9hn")))

(define-public crate-izia-0.3 (crate (name "izia") (vers "0.3.0") (deps (list (crate-dep (name "linefeed") (req "^0.5.3") (default-features #t) (kind 0)) (crate-dep (name "zia") (req "^0") (default-features #t) (kind 0)))) (hash "10aj2sbrs8a9xscnawkrnxrr9sb7nn7icr12lkz952pwvgfrfvbx")))

(define-public crate-izia-0.4 (crate (name "izia") (vers "0.4.0") (deps (list (crate-dep (name "linefeed") (req "^0.5.3") (default-features #t) (kind 0)) (crate-dep (name "zia") (req "^0.4.0") (default-features #t) (kind 0)))) (hash "0dpj10nxx845yhmw7isxi71j1hzw0ivd5qxyhp2x32y83pac5ln9")))

(define-public crate-izia-0.5 (crate (name "izia") (vers "0.5.0") (deps (list (crate-dep (name "linefeed") (req "^0.5.3") (default-features #t) (kind 0)) (crate-dep (name "zia") (req "^0.5.0") (default-features #t) (kind 0)))) (hash "0vzlysgs2nsvdgdlw28vrqpfx2yk8a85vgz09gjifnn2jdp9ia7h")))

