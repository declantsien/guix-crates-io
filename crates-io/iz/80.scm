(define-module (crates-io iz #{80}#) #:use-module (crates-io))

(define-public crate-iz80-0.1 (crate (name "iz80") (vers "0.1.0") (hash "1sv6ybhs3fmbyyysifn8f5kdihym23fmx6mll2p91j7y4q535063")))

(define-public crate-iz80-0.2 (crate (name "iz80") (vers "0.2.0") (hash "041kgldq3m8x4cqp807s1w2rwnvffrna4dac1hm4n1l9mk0xs2bm")))

(define-public crate-iz80-0.2 (crate (name "iz80") (vers "0.2.1") (hash "1kx5jab9j22gnpvwgmzfkfd9m8sv6mh4jli7h38lpvh60y02369q")))

(define-public crate-iz80-0.2 (crate (name "iz80") (vers "0.2.2") (hash "007czkhrzds166hqmibq882x74wpz2ii0i2432fj3hf1vp13qvv6")))

(define-public crate-iz80-0.3 (crate (name "iz80") (vers "0.3.0") (hash "14v00914v5vq1gwcgj8mivf4nqijzgxiwh8bvvw2sfjwsx3drrnr")))

(define-public crate-iz80-0.3 (crate (name "iz80") (vers "0.3.1") (hash "0qpw61ybvqz62bz03g1zn1y1zxqfcspsmm3wczb2kcwasw9sszrc")))

(define-public crate-iz80-0.3 (crate (name "iz80") (vers "0.3.2") (hash "13xr0f1j3a8b3f49x8ppqlskmc0856cdrba7h90gcvv6vd4n74m7")))

(define-public crate-iz80-0.3 (crate (name "iz80") (vers "0.3.4") (hash "0ka9l4951bxgndiqbfk6agk2l57ynlbkv8922gi68zk149a600j4")))

(define-public crate-iz80-0.3 (crate (name "iz80") (vers "0.3.5") (hash "0xrqx90dk4jkw119k5y99kry4na8jqai5bqi585zjczlq6bv75dn")))

(define-public crate-iz80-0.3 (crate (name "iz80") (vers "0.3.6") (hash "0gqnzp5qnc41b1s25xygh38rx6mxqcf9m60iml7rbsajp4z4iqlc")))

(define-public crate-iz80-0.3 (crate (name "iz80") (vers "0.3.7") (hash "1l5plp5msz41rnviszjgz9qfx4p6slm4ffp5j2rhd8dis73zsfdg")))

(define-public crate-iz80-0.3 (crate (name "iz80") (vers "0.3.8") (hash "0qgmjcqqi5qgq2qhj8i2pgflk2ijsnv5nq0rgc4h1pnwzgl7w5p0")))

(define-public crate-iz80-0.4 (crate (name "iz80") (vers "0.4.0") (hash "0mnabyzj6pzfzvlz63i17lqbvnvgvxy2yrq2g1br3bz2zv5yjs32")))

(define-public crate-iz80-0.4 (crate (name "iz80") (vers "0.4.1") (hash "0g2xmhbd07ph6b996a4ws9z35qw2dcj5ia0rgxj4nk0qb39krqbn")))

