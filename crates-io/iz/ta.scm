(define-module (crates-io iz ta) #:use-module (crates-io))

(define-public crate-izta-0.1 (crate (name "izta") (vers "0.1.1") (deps (list (crate-dep (name "chrono") (req "^0.4.10") (default-features #t) (kind 0)) (crate-dep (name "postgres") (req "^0.15.2") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)))) (hash "1v767jsr8wg9bdx9spf8qxggv31bsn6jdx7jzyjlbqynmlkym2xf")))

(define-public crate-izta-0.1 (crate (name "izta") (vers "0.1.2") (deps (list (crate-dep (name "chrono") (req "^0.4.10") (default-features #t) (kind 0)) (crate-dep (name "postgres") (req "^0.15.2") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)))) (hash "0mpnnybr0ifpms53n3fjgf9yg0lgb4l9k9l6fbka17qwkibhihv6")))

