(define-module (crates-io ba am) #:use-module (crates-io))

(define-public crate-baam-0.0.1 (crate (name "baam") (vers "0.0.1") (hash "1k8cgvhhp5g9l5vj13k19gs7jbqrlj8ly28swnlps2wafv0xgw5k") (yanked #t)))

(define-public crate-baam-0.0.1 (crate (name "baam") (vers "0.0.1-alpha") (hash "1wxzrdsq5ja49rhw4v2v4dzbsg8iill17sybbw3m1810ngzzkjw6") (yanked #t)))

(define-public crate-baam-0.0.1 (crate (name "baam") (vers "0.0.1-alphav1") (hash "0sscc4q664qgdnwhbnzafa9r7c93gyrwrjq26f9y7s56g2zc5dh1") (yanked #t)))

(define-public crate-baam-0.0.1 (crate (name "baam") (vers "0.0.1-alpha-v2") (hash "0l4dgbgxqrp57ni72jv1yjg3pq0vg5b84fmyfpqgnik7s2vm6pll") (yanked #t)))

(define-public crate-baam-0.0.1 (crate (name "baam") (vers "0.0.1-alpha-v3") (hash "0ngc2pkidsdc9sjjc1hg4n2aq54i0wszhwvgnfv35a6a4q1nv138")))

