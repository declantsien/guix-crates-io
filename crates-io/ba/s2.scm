(define-module (crates-io ba s2) #:use-module (crates-io))

(define-public crate-bas2wav-0.1 (crate (name "bas2wav") (vers "0.1.0") (deps (list (crate-dep (name "a2kit") (req "^2.4.1") (default-features #t) (kind 0)) (crate-dep (name "anyhow") (req "^1.0.75") (default-features #t) (kind 0)) (crate-dep (name "hound") (req "^3.5.1") (default-features #t) (kind 0)) (crate-dep (name "itertools") (req "^0.11.0") (default-features #t) (kind 0)))) (hash "1n6qbjpkwy4jw9lcl0x2qyl97gbmky3pr8v87wdc9vhgxjzxlhvw")))

(define-public crate-bas2wav-0.1 (crate (name "bas2wav") (vers "0.1.1") (deps (list (crate-dep (name "a2kit") (req "^2.4.1") (default-features #t) (kind 0)) (crate-dep (name "anyhow") (req "^1.0.75") (default-features #t) (kind 0)) (crate-dep (name "hound") (req "^3.5.1") (default-features #t) (kind 0)) (crate-dep (name "itertools") (req "^0.11.0") (default-features #t) (kind 0)))) (hash "1f8x5qyw5nsyd7418q18k9kxfcndw4bpfp4xw6w47c8zxymaba3a")))

