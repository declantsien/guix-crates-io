(define-module (crates-io ba nt) #:use-module (crates-io))

(define-public crate-bantamweight-0.1 (crate (name "bantamweight") (vers "0.1.0") (deps (list (crate-dep (name "tokio") (req "^1") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "0dmgsbk6mk3n1xsmqgmd652xlc9l3f55yy208prmjisddrlnrlmm") (yanked #t)))

(define-public crate-bantamweight-0.1 (crate (name "bantamweight") (vers "0.1.1") (deps (list (crate-dep (name "tokio") (req "^1") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "0kqsdn3by409qzjh1xmfwf7gqlb0zjw0r5s2ci83w13hv5d58iym")))

(define-public crate-bantamweight-0.1 (crate (name "bantamweight") (vers "0.1.2") (deps (list (crate-dep (name "plain-binary-stream") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "16mm6ikwh8frphxhdp9p8l9sj3wl7dpims4x8v44xmlv9z6r0qbv")))

(define-public crate-bantamweight-0.1 (crate (name "bantamweight") (vers "0.1.21") (deps (list (crate-dep (name "plain-binary-stream") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "0hy7nd35rnxr52gggc1jzhxh1f7gj5000c2hlmnpw2ln7pwcv1q2")))

