(define-module (crates-io ba ub) #:use-module (crates-io))

(define-public crate-bauble-0.0.0 (crate (name "bauble") (vers "0.0.0") (hash "1m888b7ky0w0c8czvxb6jw75ag3jp8asyy4angn761lncfricacg")))

(define-public crate-bauble_macros-0.0.0 (crate (name "bauble_macros") (vers "0.0.0") (hash "19igl4dm8yk1ql47hxs86nva9bkzpdlq7wkn4h8gdng2kkb30m05")))

