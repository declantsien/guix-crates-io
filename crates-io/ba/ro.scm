(define-module (crates-io ba ro) #:use-module (crates-io))

(define-public crate-baroque-0.1 (crate (name "baroque") (vers "0.1.0") (hash "1a9q99c0dvy3gx9gjrxv68lad8gg2a9sijnvngvkl5p3fgxmgxxi")))

(define-public crate-barotrauma-compress-1 (crate (name "barotrauma-compress") (vers "1.0.0") (deps (list (crate-dep (name "clap") (req "^4") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "flate2") (req "^1.0") (default-features #t) (kind 0)))) (hash "0bxl1v5pbc9l4rk69xpl3z1n421c5r53mkr16a8ih2r05r284m8d")))

