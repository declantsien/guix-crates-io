(define-module (crates-io ba ds) #:use-module (crates-io))

(define-public crate-badsort-0.1 (crate (name "badsort") (vers "0.1.0") (deps (list (crate-dep (name "rand") (req "^0.5") (default-features #t) (kind 0)))) (hash "0ngb3hi7z6s59x95agx60rpv312abnls0wlh704n2gil546bwpxg")))

(define-public crate-badsort-0.1 (crate (name "badsort") (vers "0.1.1") (deps (list (crate-dep (name "rand") (req "^0.5") (default-features #t) (kind 0)) (crate-dep (name "rayon") (req "^1.5.0") (default-features #t) (kind 0)))) (hash "09s0d6bsrbjxmbdmlb3isrgchd9x6x206i5hx29kka6ak1l5m9xd")))

(define-public crate-badsort-0.1 (crate (name "badsort") (vers "0.1.2") (deps (list (crate-dep (name "rand") (req "^0.8.4") (default-features #t) (kind 0)))) (hash "1g5wk0yf9am8qf9sb8ywrmxk3l556zik7kai32r8xim40yhdciaa")))

(define-public crate-badsort-0.1 (crate (name "badsort") (vers "0.1.3") (deps (list (crate-dep (name "rand") (req "^0.8.4") (default-features #t) (kind 0)))) (hash "02rw2h2k9h5dy680s3rx697qwkr53k1gdmf2ypg9n76lxqyrp4q7")))

(define-public crate-badsort-0.1 (crate (name "badsort") (vers "0.1.4") (deps (list (crate-dep (name "rand") (req "^0.8.4") (default-features #t) (kind 0)))) (hash "0r4f42q9md4n84fyy59lmigra1jm7s6gjyl5jp4qkkhhl1vy09n1")))

(define-public crate-badsort-0.1 (crate (name "badsort") (vers "0.1.5") (deps (list (crate-dep (name "rand") (req "^0.8.4") (default-features #t) (kind 0)))) (hash "07n9f248012askcr2vz7jppv80v8bmdf0ngy7756n3990c1qsihv")))

(define-public crate-badsort-0.2 (crate (name "badsort") (vers "0.2.0") (deps (list (crate-dep (name "rand") (req "^0.8.4") (default-features #t) (kind 0)))) (hash "1zvg4ziza8zav75lgn17sd298p0nc5ppz7qcgp16gbkfmjr9s7if")))

(define-public crate-badsort-0.2 (crate (name "badsort") (vers "0.2.1") (deps (list (crate-dep (name "rand") (req "^0.8.4") (default-features #t) (kind 0)))) (hash "0vwzm7iakh7cnsnrv05ksclbyn2yy1pdwsm6y8l2m4l8qlyzq7qw")))

(define-public crate-badsort-0.2 (crate (name "badsort") (vers "0.2.2") (deps (list (crate-dep (name "rand") (req "^0.8.4") (default-features #t) (kind 0)))) (hash "0wdwqbf27gs1g3yhalggy813y9k2m22v2ky0nyrvgwq7fsk444a5")))

