(define-module (crates-io ba ra) #:use-module (crates-io))

(define-public crate-bara-0.1 (crate (name "bara") (vers "0.1.0") (hash "1af6vaha7c1r1d0k0qaif3a4a9frd6gsmlw77nn2yny1fq22aqiz")))

(define-public crate-baram-0.1 (crate (name "baram") (vers "0.1.0") (hash "1mqf8nh7b3iaq5xybcn0l6p0aghqqwpr6kzjbj5alk13jq7mq545")))

(define-public crate-baram-core-0.0.1 (crate (name "baram-core") (vers "0.0.1") (hash "12wm95awgidyd9lniv6shnkpmrr73z3d4ffnv3cp5i137xm0m07n")))

