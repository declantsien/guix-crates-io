(define-module (crates-io ba al) #:use-module (crates-io))

(define-public crate-baal-0.1 (crate (name "baal") (vers "0.1.0") (deps (list (crate-dep (name "lazy_static") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "portaudio") (req "^0.6.0") (default-features #t) (kind 0)) (crate-dep (name "yaml-rust") (req "^0.3.0") (default-features #t) (kind 0)))) (hash "011639ca8wi6f9qhky93hliv9a7i3vbchy21pj3kw0hb9nn0g805")))

(define-public crate-baal-0.2 (crate (name "baal") (vers "0.2.0") (deps (list (crate-dep (name "libc") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "portaudio") (req "^0.6.0") (default-features #t) (kind 0)) (crate-dep (name "yaml-rust") (req "^0.3.0") (default-features #t) (kind 0)))) (hash "1djrkmzzb3x4wg0l0dimiv2n3n5bhac57ahhaazb34xg0hr828nd")))

(define-public crate-baal-0.3 (crate (name "baal") (vers "0.3.0") (deps (list (crate-dep (name "libc") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "portaudio") (req "^0.6.0") (default-features #t) (kind 0)) (crate-dep (name "rustc-serialize") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "toml") (req "^0.1.0") (default-features #t) (kind 2)) (crate-dep (name "yaml-rust") (req "^0.3.0") (default-features #t) (kind 2)))) (hash "0hfki79slzwxq08y09r2a7dhi4bplddbn993f3bkxcw5y7qz6kf7")))

(define-public crate-baal-0.4 (crate (name "baal") (vers "0.4.0") (deps (list (crate-dep (name "libc") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "portaudio") (req "^0.7.0") (default-features #t) (kind 0)) (crate-dep (name "rustc-serialize") (req "^0.3.0") (default-features #t) (kind 0)))) (hash "0zmxd188kmmmyik1g89kqaf9cl32s4j2vbdjph05s00pmdj43q00")))

(define-public crate-baal-0.4 (crate (name "baal") (vers "0.4.1") (deps (list (crate-dep (name "libc") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "portaudio") (req "^0.7.0") (default-features #t) (kind 0)) (crate-dep (name "rustc-serialize") (req "^0.3.0") (default-features #t) (kind 0)))) (hash "182bl81vdy456maz4fpsvbwl94w3ajbj4ggvkj089cii4fivvcnj")))

(define-public crate-baal-0.4 (crate (name "baal") (vers "0.4.2") (deps (list (crate-dep (name "libc") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "portaudio") (req "^0.7.0") (default-features #t) (kind 0)) (crate-dep (name "rustc-serialize") (req "^0.3.0") (default-features #t) (kind 0)))) (hash "12rcgrwib6ng6zb5bdisjjn2jp3fh7mcfyn7l4yxnb8ymj263hqp")))

(define-public crate-baal-0.5 (crate (name "baal") (vers "0.5.0") (deps (list (crate-dep (name "rodio") (req "^0.4.0") (default-features #t) (kind 0)) (crate-dep (name "rustc-serialize") (req "^0.3.0") (default-features #t) (kind 0)))) (hash "18h3d2hls9w15mgp8d15n1gn8izfjivnyqr38mx8y91922zkgd8p")))

(define-public crate-baal-0.5 (crate (name "baal") (vers "0.5.1") (deps (list (crate-dep (name "rodio") (req "^0.4.0") (default-features #t) (kind 0)))) (hash "1sswqgss0ynfghcn6wvdb1l4pf1l5a0w26xm5q9b06ldjlsz1m18")))

(define-public crate-baal-0.7 (crate (name "baal") (vers "0.7.0") (hash "0arcx98949g819lzri11iyi4ywid5dhilvnb8xvmrjr4y7bf0q24")))

