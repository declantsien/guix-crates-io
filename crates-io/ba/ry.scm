(define-module (crates-io ba ry) #:use-module (crates-io))

(define-public crate-bary-0.1 (crate (name "bary") (vers "0.1.0") (deps (list (crate-dep (name "anyhow") (req "^1.0.70") (default-features #t) (kind 0)) (crate-dep (name "bary-macros") (req "^0") (default-features #t) (kind 0)) (crate-dep (name "bary-server") (req "^0") (default-features #t) (kind 0)) (crate-dep (name "tar") (req "^0.4.38") (default-features #t) (kind 0)))) (hash "0lq3fc9rpvnwlmgjhcv1jsyp9qvq734kfzfqc70cidi565y8pr54")))

(define-public crate-bary-0.1 (crate (name "bary") (vers "0.1.1") (deps (list (crate-dep (name "anyhow") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "bary-macros") (req "^0") (default-features #t) (kind 0)) (crate-dep (name "bary-server") (req "^0") (default-features #t) (kind 0)) (crate-dep (name "tar") (req "^0.4") (default-features #t) (kind 0)))) (hash "14k579hy5rd1rca09xwckrhk3xxbjgdd8ix8mx3ixry1z8gcwkcg")))

(define-public crate-bary-macros-0.1 (crate (name "bary-macros") (vers "0.1.0") (deps (list (crate-dep (name "bary-server") (req "^0") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1.0.56") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.26") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.160") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_tokenstream") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2.0.14") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "15rg70dmzcyfg87d90wgs9kcinrh84gv1irfklljj4rb6nhd6bzb")))

(define-public crate-bary-macros-0.1 (crate (name "bary-macros") (vers "0.1.1") (deps (list (crate-dep (name "bary-server") (req "^0") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1.0.56") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.26") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.160") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_tokenstream") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2.0.14") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "1yz7yl2ax7qlckp43b05whi06yx95216p56q7hb650p68w26lan7")))

(define-public crate-bary-server-0.1 (crate (name "bary-server") (vers "0.1.0") (deps (list (crate-dep (name "anyhow") (req "^1.0.70") (default-features #t) (kind 0)) (crate-dep (name "rocket") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.160") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_yaml") (req "^0.9.21") (default-features #t) (kind 0)) (crate-dep (name "tar") (req "^0.4.38") (default-features #t) (kind 0)))) (hash "0c5k0r6x2x4mf1cpyjcbcvkqrbl8lga08jcfbaznb2fpg1s0fkii")))

(define-public crate-baryon-0.1 (crate (name "baryon") (vers "0.1.0") (hash "1x5pg50xb6nvl7gkqxibbmljm70haf48vmmp7pqc6i2dm2m5avg7") (yanked #t)))

(define-public crate-baryon-0.0.0 (crate (name "baryon") (vers "0.0.0") (hash "0y5662nf7k6k3li2bbi6phlmzcbrjjx6awm2snhsc5sr767zmrc4")))

(define-public crate-baryon-0.2 (crate (name "baryon") (vers "0.2.0") (deps (list (crate-dep (name "mint") (req "^0.5") (default-features #t) (kind 0)))) (hash "0lmkgv9g9iqnwj4q2rh8svrgzdpzjs37xzp919z8k88grafqh9lb")))

(define-public crate-baryon-0.3 (crate (name "baryon") (vers "0.3.0") (deps (list (crate-dep (name "bc") (req "^0.1") (default-features #t) (kind 0) (package "baryon-core")) (crate-dep (name "bytemuck") (req "^1.4") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "env_logger") (req "^0.8") (default-features #t) (kind 2)) (crate-dep (name "fxhash") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "glam") (req "^0.18") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "mint") (req "^0.5") (default-features #t) (kind 0)) (crate-dep (name "naga") (req "^0.6") (features (quote ("wgsl-in"))) (default-features #t) (kind 2)) (crate-dep (name "pollster") (req "^0.2") (default-features #t) (kind 2)) (crate-dep (name "raw-window-handle") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "wgpu") (req "^0.10") (default-features #t) (kind 0)) (crate-dep (name "winit") (req "^0.25") (default-features #t) (kind 0)))) (hash "171q4djs8kj1dw0s7anflb2b3hl5b8s1giddg98fl3c0w8vyxlrn")))

(define-public crate-baryon-core-0.1 (crate (name "baryon-core") (vers "0.1.0") (deps (list (crate-dep (name "bytemuck") (req "^1.4") (default-features #t) (kind 0)) (crate-dep (name "glam") (req "^0.18") (features (quote ("mint"))) (default-features #t) (kind 0)) (crate-dep (name "hecs") (req "^0.5") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "mint") (req "^0.5") (default-features #t) (kind 0)) (crate-dep (name "raw-window-handle") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "wgpu") (req "^0.10") (default-features #t) (kind 0)))) (hash "1mkch3zyhdv63ah04ssyin3a36r2ya0sx1h836qh6v47x8iflnmj") (features (quote (("default"))))))

(define-public crate-baryon_math-0.1 (crate (name "baryon_math") (vers "0.1.0") (hash "13y07v367rbvxz9z8vyq65pdqzyb45qwmwzggh1qzxfxgk3qp960") (yanked #t)))

(define-public crate-baryon_math-0.0.0 (crate (name "baryon_math") (vers "0.0.0") (hash "03da3nb46rfzs8m47yszhv0r52mm5ivm29x9ilp9f3y1ab4lfc61")))

(define-public crate-baryuxn-0.1 (crate (name "baryuxn") (vers "0.1.0") (hash "1fyl15y0mj3152ibbzq5h8x1qy0nbsgb43jjz0q9siwyk9iv7lp5")))

(define-public crate-baryuxn-0.1 (crate (name "baryuxn") (vers "0.1.1") (hash "0k58p4m418y0vfylgkri7fkiy7wyc7v3mqfc96jmnwddzpr172qp")))

