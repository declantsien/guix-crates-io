(define-module (crates-io ba tp) #:use-module (crates-io))

(define-public crate-batphone-0.0.0 (crate (name "batphone") (vers "0.0.0") (hash "07cxpzb06g9s8dvp40wfa4b4l659f438l2rilfck3i42ysp4xka8")))

(define-public crate-batphone-rs-0.0.0 (crate (name "batphone-rs") (vers "0.0.0") (hash "06y74j652zbr75n4g6n0a182d809cp1ajjidm932g2p5q5jqik71")))

