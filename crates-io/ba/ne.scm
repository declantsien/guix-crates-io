(define-module (crates-io ba ne) #:use-module (crates-io))

(define-public crate-bane-0.0.1 (crate (name "bane") (vers "0.0.1") (deps (list (crate-dep (name "parity-wasm") (req "^0.31") (default-features #t) (kind 0)) (crate-dep (name "wasmi") (req "^0.4") (default-features #t) (kind 0)))) (hash "1jp6hsw4lvvy87aff7h6gw4ss07z6kx4r32df7gyc1csvynsnj3q")))

(define-public crate-bane-0.0.2 (crate (name "bane") (vers "0.0.2") (deps (list (crate-dep (name "parity-wasm") (req "^0.31") (default-features #t) (kind 0)) (crate-dep (name "wasmi") (req "^0.4") (default-features #t) (kind 0)))) (hash "1h13i8dp9d6k96m19hnkqhccd9v9slda9mskfp9shy1d69v7y6wc")))

