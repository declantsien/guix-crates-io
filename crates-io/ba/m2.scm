(define-module (crates-io ba m2) #:use-module (crates-io))

(define-public crate-bam2seq-0.1 (crate (name "bam2seq") (vers "0.1.0") (deps (list (crate-dep (name "bam") (req "0.1.*") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "3.*") (features (quote ("derive" "wrap_help"))) (default-features #t) (kind 0)))) (hash "139nyjbv6hcjnv7323vkvmz72naswh6rlk83zp8cnls0jj0mcvvy")))

(define-public crate-bam2seq-0.1 (crate (name "bam2seq") (vers "0.1.1") (deps (list (crate-dep (name "bam") (req "0.1.*") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "3.*") (features (quote ("derive" "wrap_help"))) (default-features #t) (kind 0)))) (hash "1ci8dr1zlv734y1kkakddf7rqmy56m5isbl54ch4id7gy63vjx61")))

(define-public crate-bam2seq-0.1 (crate (name "bam2seq") (vers "0.1.2") (deps (list (crate-dep (name "bam") (req "0.1.*") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "3.*") (features (quote ("derive" "wrap_help"))) (default-features #t) (kind 0)))) (hash "0dbs8bm92rrnbxh7z96i6dsgp579wpdx6sh5r0shnc7kdrs11gj3")))

