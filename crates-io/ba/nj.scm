(define-module (crates-io ba nj) #:use-module (crates-io))

(define-public crate-banjin-0.1 (crate (name "banjin") (vers "0.1.0") (deps (list (crate-dep (name "quote") (req "^0.6") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^0.15") (default-features #t) (kind 0)))) (hash "1czcs7bnk76hsvyj2rk2ws8lbwvak18qjb3mq07a87p6a2qjbz3l") (features (quote (("no_std"))))))

(define-public crate-banjin-0.2 (crate (name "banjin") (vers "0.2.0") (deps (list (crate-dep (name "quote") (req "^0.6") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^0.15") (default-features #t) (kind 0)))) (hash "0kb6f6qfrp97l13cc9ld95as4l3f5j3qs24l7yi9vkimx7pl5sr0") (features (quote (("no_std"))))))

(define-public crate-banjin-0.3 (crate (name "banjin") (vers "0.3.0") (deps (list (crate-dep (name "quote") (req "^0.6") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^0.15") (default-features #t) (kind 0)))) (hash "04pq9s8x7d1lkay1z1jd7r7d6xy79bar4widl7a1jsm1yfq0j44d") (features (quote (("no_std"))))))

