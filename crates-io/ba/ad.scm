(define-module (crates-io ba ad) #:use-module (crates-io))

(define-public crate-baad-file-name-1 (crate (name "baad-file-name") (vers "1.0.0") (hash "1s5vw961dv37pwcbza0p5vjd35n14bk17fvagp9324h9mlbpvfzx")))

(define-public crate-baadii-add-one-0.1 (crate (name "baadii-add-one") (vers "0.1.0") (deps (list (crate-dep (name "rand") (req "^0.8.3") (default-features #t) (kind 0)))) (hash "196qcgb8afzyxhqgbyxnh9ih70625xbl7d8vvpg2fa235pha6qan")))

