(define-module (crates-io ba kk) #:use-module (crates-io))

(define-public crate-bakkesmod-0.1 (crate (name "bakkesmod") (vers "0.1.0") (deps (list (crate-dep (name "bakkesmod-macros") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "0ninmgziygdjpk6a93zkh3r20j85flbrkclrf9y6971xif8bs6xk")))

(define-public crate-bakkesmod-0.1 (crate (name "bakkesmod") (vers "0.1.1") (deps (list (crate-dep (name "bakkesmod-macros") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "1ihjab2h3h8a8janylpwmwjhydh405pmqipzdmazq72y03b6abxi")))

(define-public crate-bakkesmod-0.1 (crate (name "bakkesmod") (vers "0.1.2") (deps (list (crate-dep (name "bakkesmod-macros") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "1a5xr0mf41cx9qlaaw5p8y5iypqzpms33fykn9xrxgi3w2hmma8v")))

(define-public crate-bakkesmod-0.2 (crate (name "bakkesmod") (vers "0.2.0") (deps (list (crate-dep (name "bakkesmod-macros") (req "^0.1.1") (default-features #t) (kind 0)))) (hash "06yajd2hwik88r0yx87hgfvqj681rv477r0mja17bcvsd8mq8llc")))

(define-public crate-bakkesmod-0.2 (crate (name "bakkesmod") (vers "0.2.1") (deps (list (crate-dep (name "bakkesmod-macros") (req "^0.1.1") (default-features #t) (kind 0)))) (hash "1af7xy93pqamv81zz52lkzq8g8zixx1nhmc9l7x61y20f1mb7sj9")))

(define-public crate-bakkesmod-cli-0.1 (crate (name "bakkesmod-cli") (vers "0.1.0") (deps (list (crate-dep (name "convert_case") (req "^0.4.0") (default-features #t) (kind 0)) (crate-dep (name "glob") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.126") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0.126") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.3.13") (default-features #t) (kind 0)) (crate-dep (name "toml") (req "^0.5") (default-features #t) (kind 0)) (crate-dep (name "uuid") (req "^0.8") (features (quote ("v4"))) (default-features #t) (kind 0)) (crate-dep (name "websocket") (req "^0.26.2") (default-features #t) (kind 0)))) (hash "1m4qgy2j8g7nb7bp340lxg41af3lk98hdfc5dxzsd86pryd44011")))

(define-public crate-bakkesmod-macros-0.1 (crate (name "bakkesmod-macros") (vers "0.1.0") (deps (list (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (features (quote ("visit" "full"))) (default-features #t) (kind 0)))) (hash "0x92glha5z5yszviq9rjqn73dkxryp4103js0cn8zcb5y83qkaiy")))

(define-public crate-bakkesmod-macros-0.1 (crate (name "bakkesmod-macros") (vers "0.1.1") (deps (list (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (features (quote ("visit" "full"))) (default-features #t) (kind 0)))) (hash "0f0qz0bs0qqb0f6nd3r6zmrn1wh9pz1rsh6r4xfsjk44fvnw0xvp")))

