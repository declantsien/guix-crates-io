(define-module (crates-io ba gg) #:use-module (crates-io))

(define-public crate-baggie-0.1 (crate (name "baggie") (vers "0.1.0") (hash "1bh4ml3m2bhffbzy07gabkywr62q3mbqy67n00h2k8xqx2wmp68q")))

(define-public crate-baggie-0.2 (crate (name "baggie") (vers "0.2.0") (hash "1blnpa6r5fcf43j9ajnzgffy6l0aa57iapflm3ad3giidnasri51")))

(define-public crate-baggie-0.2 (crate (name "baggie") (vers "0.2.1") (hash "1ii051h4k7nbi51585d66ybgw28ndpsmqyc15fb5sbwj839xka5h")))

(define-public crate-baggins-0.1 (crate (name "baggins") (vers "0.1.4") (deps (list (crate-dep (name "actix-web-lab") (req "^0.20.1") (default-features #t) (kind 0)) (crate-dep (name "bigdecimal") (req "^0.4.2") (features (quote ("serde"))) (default-features #t) (kind 0)) (crate-dep (name "criterion") (req "^0.5.1") (features (quote ("html_reports"))) (default-features #t) (kind 2)) (crate-dep (name "env_logger") (req "^0.10.1") (default-features #t) (kind 0)) (crate-dep (name "futures") (req "^0.3.30") (default-features #t) (kind 0)) (crate-dep (name "futures-util") (req "^0.3.30") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "smartcore") (req "^0.2.1") (features (quote ("serde"))) (default-features #t) (kind 0)))) (hash "1jyhsbcblwf3f536kw4s3srb26g774lkdl1ryb4bxwfjjvyr3q3w") (yanked #t)))

(define-public crate-baggins-0.1 (crate (name "baggins") (vers "0.1.5") (deps (list (crate-dep (name "bigdecimal") (req "^0.4.2") (features (quote ("serde"))) (default-features #t) (kind 0)) (crate-dep (name "criterion") (req "^0.5.1") (features (quote ("html_reports"))) (default-features #t) (kind 2)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "0322k0i0ix0s2mbirw174jblpmjm186hkksc2xkj7cmn9iq0wj83")))

(define-public crate-baggins-0.1 (crate (name "baggins") (vers "0.1.6") (deps (list (crate-dep (name "bigdecimal") (req "^0.4.2") (features (quote ("serde"))) (default-features #t) (kind 0)) (crate-dep (name "criterion") (req "^0.5.1") (features (quote ("html_reports"))) (default-features #t) (kind 2)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "1mbbjqs0vxcpin6br17zkwc5a0si72axphx8ryp5qnicwsrzv144")))

(define-public crate-baggins-0.1 (crate (name "baggins") (vers "0.1.7") (deps (list (crate-dep (name "bigdecimal") (req "^0.4.2") (features (quote ("serde"))) (default-features #t) (kind 0)) (crate-dep (name "criterion") (req "^0.5.1") (features (quote ("html_reports"))) (default-features #t) (kind 2)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "0qsm5x98s1sryzsfai7h7ks13capf06v94h6fngci474jr3mrw7b")))

(define-public crate-baggins-0.1 (crate (name "baggins") (vers "0.1.8") (deps (list (crate-dep (name "bigdecimal") (req "^0.4.2") (features (quote ("serde"))) (default-features #t) (kind 0)) (crate-dep (name "criterion") (req "^0.5.1") (features (quote ("html_reports"))) (default-features #t) (kind 2)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "14cwzjxj4df8whgpajn88jl7pc77imwr45lkia7370kgx8y5p5c9")))

(define-public crate-baggins-0.1 (crate (name "baggins") (vers "0.1.10") (deps (list (crate-dep (name "bigdecimal") (req "^0.4.2") (features (quote ("serde"))) (default-features #t) (kind 0)) (crate-dep (name "criterion") (req "^0.5.1") (features (quote ("html_reports"))) (default-features #t) (kind 2)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "0xq0n6ixh6iqg55g12r1x4qh3wah1b8gz2wqkfknap6iwg6zx85c")))

(define-public crate-baggins-0.2 (crate (name "baggins") (vers "0.2.0") (deps (list (crate-dep (name "bigdecimal") (req "^0.4.2") (features (quote ("serde"))) (default-features #t) (kind 0)) (crate-dep (name "criterion") (req "^0.5.1") (features (quote ("html_reports"))) (default-features #t) (kind 2)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "0yp3kfshca30qz85vbhly762fgdrvrja955vwl4clwcypfjpznvm")))

