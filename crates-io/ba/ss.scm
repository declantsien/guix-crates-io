(define-module (crates-io ba ss) #:use-module (crates-io))

(define-public crate-bass-rs-0.1 (crate (name "bass-rs") (vers "0.1.0") (deps (list (crate-dep (name "bass-sys") (req "^2.1.0") (default-features #t) (kind 0)) (crate-dep (name "once_cell") (req "^1.8") (default-features #t) (kind 0)))) (hash "1kcaz8jsnhnq67gmf55gpzjgd0mfgni8i23a6d2qfw523vl8brd4") (features (quote (("drop_debug") ("bass_fx"))))))

(define-public crate-bass-rs-0.1 (crate (name "bass-rs") (vers "0.1.1") (deps (list (crate-dep (name "bass-sys") (req "^2.1.0") (default-features #t) (kind 0)) (crate-dep (name "once_cell") (req "^1.8") (default-features #t) (kind 0)))) (hash "0962882nkq2267vx2ggl0zw01ll0v4l37hr9hj49x205dl75i0ml") (features (quote (("drop_debug") ("bass_fx"))))))

(define-public crate-bass-rs-0.1 (crate (name "bass-rs") (vers "0.1.2") (deps (list (crate-dep (name "bass-sys") (req "^2.1.0") (default-features #t) (kind 0)) (crate-dep (name "once_cell") (req "^1.8") (default-features #t) (kind 0)))) (hash "17599aqbdrff82l987f1l0l5vhpvaqqb8yhrbkk78z4c3adhmkhm") (features (quote (("drop_debug") ("bass_fx"))))))

(define-public crate-bass-rs-0.1 (crate (name "bass-rs") (vers "0.1.3") (deps (list (crate-dep (name "bass-sys") (req "^2.1.0") (default-features #t) (kind 0)) (crate-dep (name "once_cell") (req "^1.8") (default-features #t) (kind 0)))) (hash "01nha19v8i8zjrh1ab99f4ldg7wgcyl30ikd5bwhvwpdd3cjbj6h") (features (quote (("drop_debug") ("bass_fx"))))))

(define-public crate-bass-rs-0.2 (crate (name "bass-rs") (vers "0.2.0") (deps (list (crate-dep (name "bass-sys") (req "^2.1.0") (default-features #t) (kind 0)) (crate-dep (name "once_cell") (req "^1.8") (default-features #t) (kind 0)))) (hash "0yyipszzvkl35xc0py839nkckrcm3li3dj4c3hh6kpx93a47yryc") (features (quote (("drop_debug") ("bass_fx"))))))

(define-public crate-bass-rs-0.2 (crate (name "bass-rs") (vers "0.2.1") (deps (list (crate-dep (name "bass-sys") (req "^2.1.0") (default-features #t) (kind 0)) (crate-dep (name "once_cell") (req "^1.8") (default-features #t) (kind 0)))) (hash "19bc9k9zdalc2ypamsdklrvnbhi7yl2rjgfvpy2jkaafaxq67b1k") (features (quote (("drop_debug") ("bass_fx"))))))

(define-public crate-bass-rs-0.2 (crate (name "bass-rs") (vers "0.2.2") (deps (list (crate-dep (name "bass-sys") (req "^2.1.0") (default-features #t) (kind 0)) (crate-dep (name "once_cell") (req "^1.8") (default-features #t) (kind 0)))) (hash "0pp5f6nby8nb8vf6j5859l71x86hvaj14yg6yb3fjy4cgkf9lrdn") (features (quote (("drop_debug") ("bass_fx"))))))

(define-public crate-bass-rs-0.2 (crate (name "bass-rs") (vers "0.2.3") (deps (list (crate-dep (name "bass-sys") (req "^2.2.0") (default-features #t) (kind 0)) (crate-dep (name "once_cell") (req "^1.16") (default-features #t) (kind 0)))) (hash "19l33b3hwv8836p7fixq1zlzgn601iw200dqrwmd5n2si0n56i1m") (features (quote (("drop_debug") ("bass_fx"))))))

(define-public crate-bass-sys-1 (crate (name "bass-sys") (vers "1.0.0") (deps (list (crate-dep (name "widestring") (req "^0.4") (default-features #t) (target "cfg(target_os = \"windows\")") (kind 0)))) (hash "0whs2i5mxrdkzdn9334g3w7lvwl2v3vv4mb9wab79f65k5gldlik")))

(define-public crate-bass-sys-1 (crate (name "bass-sys") (vers "1.1.0") (deps (list (crate-dep (name "widestring") (req "^0.4") (default-features #t) (target "cfg(target_os = \"windows\")") (kind 0)) (crate-dep (name "winapi") (req "^0.2.5") (default-features #t) (target "cfg(target_os = \"windows\")") (kind 0)))) (hash "0g9pxvvhirzbb8615rxrx726cgvn3wpcda7l8b9xfyvxz612yzw9")))

(define-public crate-bass-sys-1 (crate (name "bass-sys") (vers "1.1.1") (deps (list (crate-dep (name "widestring") (req "^0.4") (default-features #t) (target "cfg(target_os = \"windows\")") (kind 0)) (crate-dep (name "winapi") (req "^0.2.5") (default-features #t) (target "cfg(target_os = \"windows\")") (kind 0)))) (hash "024y1j0f6lw9w7qw25bdgh5xg3k0cz1vsvq7kiw6g1z9bx09nv77")))

(define-public crate-bass-sys-1 (crate (name "bass-sys") (vers "1.1.2") (deps (list (crate-dep (name "widestring") (req "^0.4") (default-features #t) (target "cfg(target_os = \"windows\")") (kind 0)) (crate-dep (name "winapi") (req "^0.2.5") (default-features #t) (target "cfg(target_os = \"windows\")") (kind 0)))) (hash "14g0svqin0hl3xxqdi3r79i0z4rcqz5lcz1li8f3ssar1phl748g")))

(define-public crate-bass-sys-1 (crate (name "bass-sys") (vers "1.1.3") (deps (list (crate-dep (name "widestring") (req "^0.4") (default-features #t) (target "cfg(target_os = \"windows\")") (kind 0)) (crate-dep (name "winapi") (req "^0.2.5") (default-features #t) (target "cfg(target_os = \"windows\")") (kind 0)))) (hash "16m1fhf61xvlysyr31wyaxw2s5275jz3sis96fppnf9xmxqddi2a")))

(define-public crate-bass-sys-1 (crate (name "bass-sys") (vers "1.1.4") (deps (list (crate-dep (name "widestring") (req "^0.4") (default-features #t) (target "cfg(target_os = \"windows\")") (kind 0)) (crate-dep (name "winapi") (req "^0.2.5") (default-features #t) (target "cfg(target_os = \"windows\")") (kind 0)))) (hash "1i4y94h4g9wpichvsfxs5kcr2aswa95khiy3s8fmmmz1gfh0lh9d")))

(define-public crate-bass-sys-2 (crate (name "bass-sys") (vers "2.0.0") (deps (list (crate-dep (name "libloading") (req "^0.7.0") (default-features #t) (kind 0)) (crate-dep (name "once_cell") (req "^1.8.0") (default-features #t) (kind 0)))) (hash "1pkpzsj33v58cphcirn8xq7j4iyic8553y9nri2sfxapx6bh8ayq")))

(define-public crate-bass-sys-2 (crate (name "bass-sys") (vers "2.0.1") (deps (list (crate-dep (name "libloading") (req "^0.7.0") (default-features #t) (kind 0)) (crate-dep (name "once_cell") (req "^1.8.0") (default-features #t) (kind 0)))) (hash "1mgv0cr2f1vsf4n8db2bjdgym5zdm422m37082sp9rfqqn5q6ylg")))

(define-public crate-bass-sys-2 (crate (name "bass-sys") (vers "2.1.0") (deps (list (crate-dep (name "libloading") (req "^0.7.0") (default-features #t) (kind 0)) (crate-dep (name "once_cell") (req "^1.8.0") (default-features #t) (kind 0)))) (hash "0ffxi9lcw5b38262dh9d96xaq5yc4zqgagkzkyd5qwhxzlmrd31d")))

(define-public crate-bass-sys-2 (crate (name "bass-sys") (vers "2.2.0") (deps (list (crate-dep (name "libloading") (req "^0.7.0") (default-features #t) (kind 0)) (crate-dep (name "once_cell") (req "^1.8.0") (default-features #t) (kind 0)))) (hash "0ri5sr2vg2wihzvyjh9s5vw7rq1r1gxjpqa54d9rli2lw3s59vr8")))

(define-public crate-bass-sys-2 (crate (name "bass-sys") (vers "2.2.1") (deps (list (crate-dep (name "libloading") (req "^0.7.0") (default-features #t) (kind 0)) (crate-dep (name "once_cell") (req "^1.8.0") (default-features #t) (kind 0)))) (hash "0i73lfw1a28q9kqdjrns5xpazwi3zcv7d6rrs201x0syh772n52w")))

(define-public crate-bass-sys-2 (crate (name "bass-sys") (vers "2.2.2") (deps (list (crate-dep (name "libloading") (req "^0.7.3") (default-features #t) (kind 0)) (crate-dep (name "once_cell") (req "^1.13.0") (default-features #t) (kind 0)))) (hash "1rmjixy2357klfgz7a4vwyzy9hmbd4rr8x2q91h4swfgf15fsmaj")))

(define-public crate-bass-sys-2 (crate (name "bass-sys") (vers "2.3.0") (deps (list (crate-dep (name "libloading") (req "^0.7.3") (default-features #t) (kind 0)) (crate-dep (name "once_cell") (req "^1.13.0") (default-features #t) (kind 0)))) (hash "19cx8h3f5r8vansvsggip2vmdiwrd6d6gic11q8428g0ybbhyxik")))

(define-public crate-bassert-0.0.2 (crate (name "bassert") (vers "0.0.2") (hash "11khvbkllrm7qlvghcqwf7dqq4iz9ayz20qmzrir6b2vmvrzkx3q")))

(define-public crate-bassert-0.0.3 (crate (name "bassert") (vers "0.0.3") (hash "0rzr0j2kq0wh6fi3scgijzgzz0lqmq8mfxm1jsfwq0ba01jgvjzq")))

(define-public crate-bassert-0.0.4 (crate (name "bassert") (vers "0.0.4") (hash "08f9qihm265dd5hqb872f8v0rl4s67a301gzw6vavdh70cpbd7k0")))

(define-public crate-bassment-0.0.1 (crate (name "bassment") (vers "0.0.1") (hash "181a96igwiki8ynnplk9nzaqf6lp8dmsgdami6kvhf6prfwapxvy")))

(define-public crate-bassment-0.1 (crate (name "bassment") (vers "0.1.0") (deps (list (crate-dep (name "actix-cors") (req "^0.6") (default-features #t) (kind 0)) (crate-dep (name "actix-files") (req "^0.6") (default-features #t) (kind 0)) (crate-dep (name "actix-multipart") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "actix-web") (req "^4") (default-features #t) (kind 0)) (crate-dep (name "bcrypt") (req "^0.13") (default-features #t) (kind 0)) (crate-dep (name "chrono") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^3.1") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "diesel") (req "^1.4") (features (quote ("postgres" "r2d2"))) (default-features #t) (kind 0)) (crate-dep (name "diesel-derive-enum") (req "^1.1") (features (quote ("postgres"))) (default-features #t) (kind 0)) (crate-dep (name "diesel_migrations") (req "^1.4") (default-features #t) (kind 0)) (crate-dep (name "dotenv") (req "^0.15") (default-features #t) (kind 0)) (crate-dep (name "futures-util") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "id3") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "jsonwebtoken") (req "^8.1") (default-features #t) (kind 0)) (crate-dep (name "once_cell") (req "^1.10") (default-features #t) (kind 0)) (crate-dep (name "passwords") (req "^3.1") (default-features #t) (kind 0)) (crate-dep (name "r2d2") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.5") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "tracing") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "tracing-actix-web") (req "^0.5") (default-features #t) (kind 0)) (crate-dep (name "tracing-subscriber") (req "^0.3") (default-features #t) (kind 0)))) (hash "152099g5f9gxvpsslm0x7q4kndqnfsvdpsksay0wvwvkw72d8nlw")))

