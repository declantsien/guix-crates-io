(define-module (crates-io ba du) #:use-module (crates-io))

(define-public crate-baduk-0.1 (crate (name "baduk") (vers "0.1.0") (deps (list (crate-dep (name "nom") (req "^2.2") (features (quote ("verbose-errors"))) (default-features #t) (kind 0)))) (hash "14l4258q575m469hl591c8aavchkvvpfrnqhh0kfhnqw19192nqj")))

