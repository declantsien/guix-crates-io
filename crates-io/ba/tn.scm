(define-module (crates-io ba tn) #:use-module (crates-io))

(define-public crate-batnotify-0.1 (crate (name "batnotify") (vers "0.1.0") (deps (list (crate-dep (name "battery") (req "^0.7") (default-features #t) (kind 0)) (crate-dep (name "notify-rust") (req "^3") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.3") (default-features #t) (kind 0)))) (hash "09527bww06ivaikzv9jfj7ck6am79yrs3cql7cbgkr966ylpmgyp")))

(define-public crate-batnotify-0.1 (crate (name "batnotify") (vers "0.1.1") (deps (list (crate-dep (name "battery") (req "^0.7") (default-features #t) (kind 0)) (crate-dep (name "notify-rust") (req "^3") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.3") (default-features #t) (kind 0)))) (hash "0j7nnql1l8k3akis8bl4zqyrkg1iiinwraqkhirdi2mbv29489i9")))

