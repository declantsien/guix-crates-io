(define-module (crates-io ba e2) #:use-module (crates-io))

(define-public crate-bae2-1 (crate (name "bae2") (vers "1.0.0") (deps (list (crate-dep (name "heck") (req "^0.4.1") (default-features #t) (kind 0)) (crate-dep (name "proc-macro-error") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2") (features (quote ("full" "extra-traits"))) (default-features #t) (kind 0)) (crate-dep (name "trybuild") (req "^1") (default-features #t) (kind 2)))) (hash "1fvcxg254lgzvpavgvkwhdvwfdj0vb2qydxw9hlyn38r70xdsh7x")))

