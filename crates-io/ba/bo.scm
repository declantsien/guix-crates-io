(define-module (crates-io ba bo) #:use-module (crates-io))

(define-public crate-baboon-0.1 (crate (name "baboon") (vers "0.1.0") (deps (list (crate-dep (name "tokio") (req "^1") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "19s4ji5pq9j9lxialciwlc2zz8yn6w7pb7x03rl56x1ab3sa61ap")))

