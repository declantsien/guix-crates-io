(define-module (crates-io ba bi) #:use-module (crates-io))

(define-public crate-babilado-types-0.1 (crate (name "babilado-types") (vers "0.1.0") (deps (list (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "01wq7ljlxilr7n7m2clq5jr96bhd7ng8ln19h4dmq9syp5l6l8vv")))

(define-public crate-babilado-types-0.2 (crate (name "babilado-types") (vers "0.2.0") (deps (list (crate-dep (name "rand") (req "^0.8.2") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "1pld81ikp8w1rsnad3wpj3r97imbkmwv34fyl5fvydl6l6nfic8n")))

(define-public crate-babilado-types-0.2 (crate (name "babilado-types") (vers "0.2.1") (deps (list (crate-dep (name "rand") (req "^0.8.2") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "062vlqzjfjfp78xa7jnm3pgi867akia7337d6cb8fbkgn8zm0q0d")))

(define-public crate-babilado-types-0.2 (crate (name "babilado-types") (vers "0.2.2") (deps (list (crate-dep (name "rand") (req "^0.8.2") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "0gw5vjr59g0qkbk6zdh5s1894g3042a0qh5ahd85plcf6ga59xj9")))

(define-public crate-babilado-types-0.2 (crate (name "babilado-types") (vers "0.2.3") (deps (list (crate-dep (name "rand") (req "^0.8.2") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "0fjnv3sic55y72c2g25d599706261ybbpdxxv8bvzzyxbx55b8dv")))

(define-public crate-babilado-types-0.3 (crate (name "babilado-types") (vers "0.3.0") (deps (list (crate-dep (name "oorandom") (req "^11.1") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "05ilcg9864i0yjdg9sw2ihz3d7biwbgm8mw88hwsllqsb1anb7mp")))

(define-public crate-babilado-types-0.4 (crate (name "babilado-types") (vers "0.4.0") (deps (list (crate-dep (name "oorandom") (req "^11.1") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "031vsgxaki7v0mvcda1zfn7y8djczjlry4kzf70g0cn95p0dp5k5")))

