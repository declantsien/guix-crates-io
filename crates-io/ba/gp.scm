(define-module (crates-io ba gp) #:use-module (crates-io))

(define-public crate-bagpipe-0.1 (crate (name "bagpipe") (vers "0.1.0") (deps (list (crate-dep (name "crossbeam") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "num_cpus") (req "^1.5") (default-features #t) (kind 0)))) (hash "0gnnv26id2my6jy4mbqh0n5mznx1blf4bf46006vriwp79nz43ym") (features (quote (("staggered_indexes") ("prime_schedules") ("huge_segments") ("default" "check_empty_yq") ("check_empty_yq"))))))

