(define-module (crates-io ba ff) #:use-module (crates-io))

(define-public crate-baffa-0.1 (crate (name "baffa") (vers "0.1.0") (hash "08g0alljxh7viaqnp8jgc3q3j3ismgl33ybbck70m98cspfl7p8g") (features (quote (("std") ("alloc"))))))

(define-public crate-baffa-0.1 (crate (name "baffa") (vers "0.1.1") (hash "1102jdslnj9j6rrdf7xg60afr40g0vq4k35k599kjar18kk643hf") (features (quote (("std") ("alloc"))))))

(define-public crate-baffa-0.1 (crate (name "baffa") (vers "0.1.2") (hash "1fjll70zpkx1apafaq5w1ax2d714apbkxfwsml87w3my4crvqcq8") (features (quote (("std") ("alloc"))))))

(define-public crate-baffao-0.0.1 (crate (name "baffao") (vers "0.0.1") (deps (list (crate-dep (name "anyhow") (req "^1.0.80") (default-features #t) (kind 0)) (crate-dep (name "axum-extra") (req "^0.9.2") (features (quote ("cookie"))) (default-features #t) (kind 0)) (crate-dep (name "base64") (req "^0.22.0") (default-features #t) (kind 0)) (crate-dep (name "chrono") (req "^0.4.35") (default-features #t) (kind 0)) (crate-dep (name "config") (req "^0.14.0") (default-features #t) (kind 0)) (crate-dep (name "cookie") (req "^0.18.0") (default-features #t) (kind 0)) (crate-dep (name "hex") (req "^0.4.3") (default-features #t) (kind 0)) (crate-dep (name "http") (req "^1.1.0") (default-features #t) (kind 0)) (crate-dep (name "jsonwebtoken") (req "^9.2.0") (default-features #t) (kind 0)) (crate-dep (name "oauth2") (req "^4.4.2") (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.11.24") (default-features #t) (kind 0)) (crate-dep (name "ring") (req "^0.17.8") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.197") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.114") (default-features #t) (kind 0)))) (hash "14l8jzl2l4ldg96a8vzcy47188linzysl6a2vspcczf4ir7a3z58")))

(define-public crate-baffle-0.1 (crate (name "baffle") (vers "0.1.0") (deps (list (crate-dep (name "rustc-hex") (req "^2.0.1") (default-features #t) (kind 0)) (crate-dep (name "web3") (req "^0.10.0") (default-features #t) (kind 0)))) (hash "02lzx098l13n82n8c6klwwk8sxpz592dpyk4w1bsybm3zxzafbm3")))

(define-public crate-baffle-0.1 (crate (name "baffle") (vers "0.1.1") (deps (list (crate-dep (name "web3") (req "^0.10.0") (default-features #t) (kind 0)))) (hash "1rbw16qlprm9iqylvfz2py9r5r09ab4k217d6dnflnmrnvs70whg")))

