(define-module (crates-io ba rs) #:use-module (crates-io))

(define-public crate-bars-0.1 (crate (name "bars") (vers "0.1.0") (hash "0h7035gx0zy703vwkaj1zm56x7hkdzl6a1zk93l7gxvjc1kmbik3") (yanked #t)))

(define-public crate-barse-0.1 (crate (name "barse") (vers "0.1.0") (deps (list (crate-dep (name "barse-derive") (req "^0.1.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "bytesize") (req "^1.2.0") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.40") (default-features #t) (kind 0)))) (hash "0nw2kw4yh01yhhnpgnqm0saaa4gpzc5yj729a6ny0blc4g8v4x1k") (v 2) (features2 (quote (("derive" "dep:barse-derive"))))))

(define-public crate-barse-0.1 (crate (name "barse") (vers "0.1.1") (deps (list (crate-dep (name "barse-derive") (req "^0.1.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "bytesize") (req "^1.2.0") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.40") (default-features #t) (kind 0)))) (hash "0l2cq8k8abgg5jmpgj6q72jpvch89bbf5pzqarqr9k4vlbfxdp3f") (v 2) (features2 (quote (("derive" "dep:barse-derive"))))))

(define-public crate-barse-0.1 (crate (name "barse") (vers "0.1.2") (deps (list (crate-dep (name "barse-derive") (req "^0.1.1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "bytesize") (req "^1.2.0") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.40") (default-features #t) (kind 0)))) (hash "0zy2ra8c1cgly9ny8hbia56ra48w4ljylzrzcshnvf4aqp3f68wv") (v 2) (features2 (quote (("derive" "dep:barse-derive"))))))

(define-public crate-barse-0.2 (crate (name "barse") (vers "0.2.0") (deps (list (crate-dep (name "anyhow") (req "^1.0.71") (default-features #t) (kind 0)) (crate-dep (name "barse-derive") (req "^0.2.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "bytesize") (req "^1.2.0") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.40") (default-features #t) (kind 0)))) (hash "1i2852l6q943hcskrpclhmhg1j9jn63g5vmxyljqvg690i1m4gg4") (features (quote (("default" "derive")))) (v 2) (features2 (quote (("derive" "dep:barse-derive"))))))

(define-public crate-barse-0.3 (crate (name "barse") (vers "0.3.0") (deps (list (crate-dep (name "anyhow") (req "^1.0.71") (default-features #t) (kind 0)) (crate-dep (name "barse-derive") (req "^0.3.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "bytesize") (req "^1.2.0") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.40") (default-features #t) (kind 0)))) (hash "1rd3ws1a8iy75wxp37sz5cjr0k6ap79cs5b8qjq5apf66wkgc2n4") (features (quote (("default" "derive")))) (v 2) (features2 (quote (("derive" "dep:barse-derive"))))))

(define-public crate-barse-derive-0.1 (crate (name "barse-derive") (vers "0.1.0") (deps (list (crate-dep (name "proc-macro2") (req "^1.0.56") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.26") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2.0.15") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "13gwicv27n014qya5vbsg29mpsqn1mhwc78kjwsmj1n4d35dxx9z")))

(define-public crate-barse-derive-0.1 (crate (name "barse-derive") (vers "0.1.1") (deps (list (crate-dep (name "proc-macro2") (req "^1.0.56") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.26") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2.0.15") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "1w3pkryy5429wyv7rcspvadapagaha24mki1mpdh0h29rl5ig50p")))

(define-public crate-barse-derive-0.2 (crate (name "barse-derive") (vers "0.2.0") (deps (list (crate-dep (name "barse-derive-impl") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1.0.56") (default-features #t) (kind 0)))) (hash "1v63kzcpc2zdripf6fcr492an8gl19xjv5c64aibxbqrd8xb32hl")))

(define-public crate-barse-derive-0.3 (crate (name "barse-derive") (vers "0.3.0") (deps (list (crate-dep (name "barse-derive-impl") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1.0.56") (default-features #t) (kind 0)))) (hash "1sic955w7dnqrllcy23l0gfs9240f35zw50h2jk0lrii9h5gqcnq")))

(define-public crate-barse-derive-impl-0.1 (crate (name "barse-derive-impl") (vers "0.1.0") (deps (list (crate-dep (name "either") (req "^1.8.1") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1.0.56") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.26") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2.0.15") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "1y6qrmg7nyyclkv093npgwlj0q8hs5rb8gmznzsh9a4iy8k6yhkc")))

(define-public crate-barse-derive-impl-0.2 (crate (name "barse-derive-impl") (vers "0.2.0") (deps (list (crate-dep (name "either") (req "^1.8.1") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1.0.56") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.26") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2.0.15") (features (quote ("full" "extra-traits"))) (default-features #t) (kind 0)))) (hash "01cbx1mdk45j6h9f78cr5fzqngvdvaf88c4y7lqxj4kl97z2248d")))

(define-public crate-barsh-0.0.0 (crate (name "barsh") (vers "0.0.0") (deps (list (crate-dep (name "insta") (req "^1.28.0") (default-features #t) (kind 2)))) (hash "0bgpn1fl8kyl999lm97ccz63qnij6fw1033r3csgpj390qmm95gj")))

