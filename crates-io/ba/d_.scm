(define-module (crates-io ba d_) #:use-module (crates-io))

(define-public crate-bad_cors-0.1 (crate (name "bad_cors") (vers "0.1.0") (deps (list (crate-dep (name "log") (req "^0.4.13") (default-features #t) (kind 0)) (crate-dep (name "rocket") (req "^0.4.4") (default-features #t) (kind 0)) (crate-dep (name "rocket_contrib") (req "^0.4.4") (default-features #t) (kind 0)))) (hash "0535a0z16igahd8zv0ac788390k8y2s5fq5xkcjydhx5maa5c4v3")))

(define-public crate-bad_email-0.1 (crate (name "bad_email") (vers "0.1.0") (deps (list (crate-dep (name "serde") (req "^1.0.195") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0.195") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.111") (default-features #t) (kind 0)) (crate-dep (name "toml") (req "^0.8.8") (default-features #t) (kind 0)))) (hash "0smfa57im0faqlv8690ki43vi0ipvnpzqzx0b7zrvz5zwn5fpi1x")))

(define-public crate-bad_email-0.1 (crate (name "bad_email") (vers "0.1.1") (deps (list (crate-dep (name "serde") (req "^1.0.195") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0.195") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.111") (default-features #t) (kind 0)) (crate-dep (name "toml") (req "^0.8.8") (default-features #t) (kind 0)))) (hash "1sdhbns4wch7sy3xrnfz0an85vmh14m3lk4gdqa0vhybnq5fqk1w")))

(define-public crate-bad_idea-0.1 (crate (name "bad_idea") (vers "0.1.0") (hash "0rbxyaaf05lh2w001w1hj62shppnckf74r5va8mq177cbg7hfq5b")))

