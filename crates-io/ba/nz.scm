(define-module (crates-io ba nz) #:use-module (crates-io))

(define-public crate-banzai-0.1 (crate (name "banzai") (vers "0.1.0") (deps (list (crate-dep (name "crc") (req "^3.0") (default-features #t) (kind 0)))) (hash "1a12cy8mrlz3h7ryv03y0yfywcv47w4113dswli78w4hwd0isxic")))

(define-public crate-banzai-0.1 (crate (name "banzai") (vers "0.1.1") (deps (list (crate-dep (name "crc") (req "^3.0") (default-features #t) (kind 0)))) (hash "0j97c6vh0zmwvarlrzgk4l2ksyshbghgs06vw5ix6adjaf2c87g8")))

(define-public crate-banzai-0.1 (crate (name "banzai") (vers "0.1.2") (deps (list (crate-dep (name "crc") (req "^3.0") (default-features #t) (kind 0)))) (hash "05j478ygqzrls5g1blw421iw027r84yqjy4kn9j3wihkpg7rsby0")))

(define-public crate-banzai-0.2 (crate (name "banzai") (vers "0.2.0") (deps (list (crate-dep (name "crc") (req "^3.0") (default-features #t) (kind 0)))) (hash "1mfd24c7648939arzyzn1v6v1ydwy2ha0fhajr29faw3xx0xdw77")))

(define-public crate-banzai-0.2 (crate (name "banzai") (vers "0.2.1") (deps (list (crate-dep (name "crc") (req "^3.0") (default-features #t) (kind 0)))) (hash "1fs9fb5cf7qjjbsi597298s17dd4vmszbbigg2b1cn4crspizkgx")))

(define-public crate-banzai-0.2 (crate (name "banzai") (vers "0.2.2") (deps (list (crate-dep (name "crc") (req "^3.0") (default-features #t) (kind 0)))) (hash "0rq6vkar2ga36ly1h5pp63705jfiychq3j4kaza6yy4dlsy99x8n")))

(define-public crate-banzai-0.2 (crate (name "banzai") (vers "0.2.3") (deps (list (crate-dep (name "crc") (req "^3.0") (default-features #t) (kind 0)))) (hash "00xf4hhf78m1q0sd29fqd2bv04hmxfnyknmnnip26520562b7mgs")))

(define-public crate-banzai-0.2 (crate (name "banzai") (vers "0.2.4") (deps (list (crate-dep (name "crc") (req "^3.0") (default-features #t) (kind 0)))) (hash "1lsyzky18brccxr3r29h4r2kyp8iw5zsn778f6410d47066vg0qb")))

(define-public crate-banzai-0.2 (crate (name "banzai") (vers "0.2.5") (deps (list (crate-dep (name "crc") (req "^3.0") (default-features #t) (kind 0)))) (hash "0sg33xd7zb6z3bsg17cppfvrxy66xdfkcnr7xxp7a3ivvwxi1768")))

(define-public crate-banzai-0.2 (crate (name "banzai") (vers "0.2.6") (deps (list (crate-dep (name "crc") (req "^3.0") (default-features #t) (kind 0)))) (hash "0kkvw1v2qcvrjcqhq3vfyfwwld7jn3chg8z4bsl172xny8y7rwb2")))

(define-public crate-banzai-0.3 (crate (name "banzai") (vers "0.3.0") (deps (list (crate-dep (name "crc") (req "^3.0") (default-features #t) (kind 0)))) (hash "1ak99x9nw8p9b02lwkn1mawx9hbqp8v1d9p9j9fzaxqvf08mybcl") (yanked #t)))

(define-public crate-banzai-0.3 (crate (name "banzai") (vers "0.3.1") (deps (list (crate-dep (name "crc") (req "^3.0") (default-features #t) (kind 0)))) (hash "13yan186kh98llixwq1xqdrr2mnbyamwa4w58db6lhrkxih94bbz")))

