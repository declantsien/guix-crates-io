(define-module (crates-io ba tr) #:use-module (crates-io))

(define-public crate-batrachia-0.1 (crate (name "batrachia") (vers "0.1.0") (deps (list (crate-dep (name "anyhow") (req "^1.0.52") (default-features #t) (kind 0)) (crate-dep (name "dotenv") (req "^0.15.0") (default-features #t) (kind 1)) (crate-dep (name "futures") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2.121") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.20.0") (features (quote ("sync" "rt"))) (default-features #t) (kind 0)))) (hash "016ldprpvjac866scc5m97m25qa7g5lp6v8m7wy4ivmahcdhyvib")))

(define-public crate-batrachia-0.1 (crate (name "batrachia") (vers "0.1.1") (deps (list (crate-dep (name "anyhow") (req "^1.0.52") (default-features #t) (kind 0)) (crate-dep (name "futures") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2.121") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.20.0") (features (quote ("sync" "rt"))) (default-features #t) (kind 0)))) (hash "0bzq79n9xgfbidyhszv8v44acchnzk0aswk3xjwdrd18651gjciw")))

