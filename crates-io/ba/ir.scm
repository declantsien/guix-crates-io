(define-module (crates-io ba ir) #:use-module (crates-io))

(define-public crate-bairstow-rs-0.1 (crate (name "bairstow-rs") (vers "0.1.0") (deps (list (crate-dep (name "approx_eq") (req "^0.1.8") (default-features #t) (kind 2)) (crate-dep (name "criterion") (req "^0.4") (default-features #t) (kind 2)) (crate-dep (name "num") (req "^0.4.0") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2.15") (default-features #t) (kind 0)) (crate-dep (name "rayon") (req "^1.7.0") (default-features #t) (kind 0)))) (hash "1z6ckyi9gzgb63jh2bbnxzk749xvc2d7svxv681f8c6hvgdhlc7g")))

