(define-module (crates-io ba ml) #:use-module (crates-io))

(define-public crate-bamlift-0.1 (crate (name "bamlift") (vers "0.1.0") (deps (list (crate-dep (name "env_logger") (req "^0.9.0") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "rust-htslib") (req "^0.43.1") (default-features #t) (kind 0)))) (hash "1m68k6252v52dbf9015w45sfsd9l7mnl72hjhwl04cj6r95rmaf5") (yanked #t)))

(define-public crate-bamlift-0.1 (crate (name "bamlift") (vers "0.1.1") (deps (list (crate-dep (name "env_logger") (req "^0.9.0") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "rust-htslib") (req "^0.44.1") (default-features #t) (kind 0)))) (hash "09r6kvayf7kvaw5kbikm8n2p28hp769napqn1dk45fmikc53lyzb")))

(define-public crate-bamlift-0.2 (crate (name "bamlift") (vers "0.2.0") (deps (list (crate-dep (name "env_logger") (req "^0.9.0") (default-features #t) (kind 0)) (crate-dep (name "itertools") (req "^0.10.5") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "rust-htslib") (req "^0.44.1") (default-features #t) (kind 0)))) (hash "0cmv2g4fryzhax1040wy7k8kyf18ia4iyg03ral4nbzf3dyhn9wr")))

(define-public crate-bamlift-0.3 (crate (name "bamlift") (vers "0.3.0") (deps (list (crate-dep (name "env_logger") (req "^0.9.0") (default-features #t) (kind 0)) (crate-dep (name "itertools") (req "^0.10.5") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "rust-htslib") (req "^0.44.1") (default-features #t) (kind 0)))) (hash "1l9jv5r9pv5nh115d8wjaw7km0981bi70mip0i1yalkpxj4xhcp1")))

