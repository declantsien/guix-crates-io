(define-module (crates-io ba p-) #:use-module (crates-io))

(define-public crate-bap-sys-0.1 (crate (name "bap-sys") (vers "0.1.0") (deps (list (crate-dep (name "bindgen") (req "^0.30") (default-features #t) (kind 1)) (crate-dep (name "pkg-config") (req "^0.3") (default-features #t) (kind 1)))) (hash "10xjg4ajircvismdb56zjanrg1wx4j254zhwm6i3h4hqy97k2gqb")))

