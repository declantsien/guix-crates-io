(define-module (crates-io ba rg) #:use-module (crates-io))

(define-public crate-barg-0.0.1 (crate (name "barg") (vers "0.0.1") (deps (list (crate-dep (name "fonterator") (req "^0.2.0-pre0") (default-features #t) (kind 0)) (crate-dep (name "footile") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "png") (req "^0.12") (default-features #t) (kind 2)))) (hash "14vkkv0aj2lc82i43rijp38nc2p4840ri0padmy44gpa0isyx1kh")))

(define-public crate-barg-0.0.2 (crate (name "barg") (vers "0.0.2") (deps (list (crate-dep (name "fonterator") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "footile") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "png") (req "^0.12") (default-features #t) (kind 2)))) (hash "0rh1il1kl35s0k42n6drzbj4vdv2v404bnnlx8abk7k7n6qngk89")))

(define-public crate-barg-0.0.3 (crate (name "barg") (vers "0.0.3") (deps (list (crate-dep (name "fonterator") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "footile") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "png") (req "^0.12") (default-features #t) (kind 2)))) (hash "0h8pb7sq05palrzl5x6wbfsmdqdzgla65kqvlwx7b1s716ymizrm")))

(define-public crate-barg-0.1 (crate (name "barg") (vers "0.1.0") (deps (list (crate-dep (name "fonterator") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "png") (req "^0.14") (default-features #t) (kind 2)) (crate-dep (name "rvg") (req "^0.0.1") (default-features #t) (kind 0)) (crate-dep (name "window") (req "^0.1") (default-features #t) (kind 0)))) (hash "1h2mi91pgdbpwwda9v2cdxa2bn0bcb8yaw0g7d6ckjvp40flkxg4")))

(define-public crate-barg-0.2 (crate (name "barg") (vers "0.2.0") (deps (list (crate-dep (name "fonterator") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "png") (req "^0.14") (default-features #t) (kind 2)) (crate-dep (name "res") (req "^0.4") (default-features #t) (kind 1)) (crate-dep (name "rvg") (req "^0.0.2") (features (quote ("footile"))) (default-features #t) (kind 0)) (crate-dep (name "window") (req "^0.2") (default-features #t) (kind 0)))) (hash "1zjz6d7phcapj1vadnnirvqdx39hfcbcvra6m1m1s3gf5mf7kimd")))

