(define-module (crates-io ba cn) #:use-module (crates-io))

(define-public crate-bacnet-0.1 (crate (name "bacnet") (vers "0.1.0") (hash "0rr95bmv8r3whgzvcfm3xbq4gfnw960d2j6kxj1638nayalhaxja")))

(define-public crate-bacnet_parse-0.1 (crate (name "bacnet_parse") (vers "0.1.0") (deps (list (crate-dep (name "arrayref") (req "^0.3.5") (default-features #t) (kind 0)))) (hash "1xmzby1j2k9k73g4157yz0jis38m466jhpaxyab2qw5m68g1aybz")))

(define-public crate-bacnet_parse-0.1 (crate (name "bacnet_parse") (vers "0.1.1") (deps (list (crate-dep (name "arrayref") (req "^0.3.5") (default-features #t) (kind 0)))) (hash "0nfkffrs1323y4ysw2j3a0amk07m074g8f65ihv92wsddbj3bw9i")))

(define-public crate-bacnet_parse-0.2 (crate (name "bacnet_parse") (vers "0.2.0") (deps (list (crate-dep (name "arrayref") (req "^0.3.5") (default-features #t) (kind 0)))) (hash "14p9b2g1dg676ld990fglsgs7v4qzwd4wj91drrlci1lq1dnv84c")))

