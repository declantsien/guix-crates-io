(define-module (crates-io ba sn) #:use-module (crates-io))

(define-public crate-basn1-0.1 (crate (name "basn1") (vers "0.1.0") (hash "1yp7jx4cf4i6ni12a4ajgpv1iqd1w3mkmx3jqig0hxjfpcm11hrf")))

(define-public crate-basn1-0.1 (crate (name "basn1") (vers "0.1.1") (hash "1a3jrjxgl0aic8swpndlypyr9bvjb70fzg9y29p4sfa4ggvdkj2l")))

(define-public crate-basn1-0.1 (crate (name "basn1") (vers "0.1.2") (hash "1xyp7cggqqwfpih00rc25nqw7bc31s7x2k24vdl2x6am8zqk9xjd")))

(define-public crate-basn1-0.1 (crate (name "basn1") (vers "0.1.3") (hash "1in6lv7161i4n5l8bfqcdrkdrciijizjidmd5xxpk97z3wi8xbvw")))

