(define-module (crates-io ba nq) #:use-module (crates-io))

(define-public crate-banquo-0.1 (crate (name "banquo") (vers "0.1.0") (deps (list (crate-dep (name "banquo-core") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "banquo-hybrid_distance") (req "^0.1.0") (optional #t) (default-features #t) (kind 0)))) (hash "151a7ycxc5xmxs30jyl51ar3p7krczp8j34kapm4invwmawff4cv") (features (quote (("default")))) (v 2) (features2 (quote (("hybrid-distance" "dep:banquo-hybrid_distance"))))))

(define-public crate-banquo-core-0.1 (crate (name "banquo-core") (vers "0.1.0") (deps (list (crate-dep (name "approx") (req "^0.5.1") (default-features #t) (kind 2)) (crate-dep (name "ordered-float") (req "^4.2.0") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.56") (default-features #t) (kind 0)))) (hash "1ikka2aiz4a7wr7haz4xx5rlvwzya6wfcailvxzqls23p1n831gi")))

(define-public crate-banquo-hybrid_distance-0.1 (crate (name "banquo-hybrid_distance") (vers "0.1.0") (deps (list (crate-dep (name "banquo-core") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "petgraph") (req "^0.6.4") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.56") (default-features #t) (kind 0)))) (hash "1aznbsjcf711b0qqs331xn9mar772k4h0yyskls623fg525ls9zm")))

