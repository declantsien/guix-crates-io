(define-module (crates-io ba ic) #:use-module (crates-io))

(define-public crate-baichat-rs-0.1 (crate (name "baichat-rs") (vers "0.1.0") (deps (list (crate-dep (name "ratmom") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.11.18") (features (quote ("json"))) (default-features #t) (kind 0)) (crate-dep (name "reqwest-eventsource") (req "^0.4.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.164") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.97") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "12avmxk5ykkq3n0x9is11vk2m6ij00zgp8bhn9ax4rlddv4jmrd6")))

(define-public crate-baicie_package_test-0.1 (crate (name "baicie_package_test") (vers "0.1.0") (hash "0bngvbfhdaikx94bzla3m8559g7s1hkv4c619khf6nvz75yxdbja")))

(define-public crate-baicie_package_test-0.1 (crate (name "baicie_package_test") (vers "0.1.1") (hash "0hcs15bkzd4yjlffgg8kpnq98ppngcz7j050q0l1ia80z7qg7x2g")))

