(define-module (crates-io ba ms) #:use-module (crates-io))

(define-public crate-bamsalvage-0.1 (crate (name "bamsalvage") (vers "0.1.3") (deps (list (crate-dep (name "byteorder") (req "^1.4.3") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^4.2.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "crc32fast") (req "^1.3.2") (default-features #t) (kind 0)) (crate-dep (name "flate2") (req "^1.0.25") (features (quote ("zlib-ng"))) (kind 0)) (crate-dep (name "regex") (req "^1.7.3") (default-features #t) (kind 0)))) (hash "06sqpmycg7m5mwwfnqi13jyr3zp3m7cgx5bfarxwy94ng2j497sy")))

