(define-module (crates-io ba us) #:use-module (crates-io))

(define-public crate-baus-0.1 (crate (name "baus") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^3.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "dirs") (req "^4.0") (default-features #t) (kind 0)) (crate-dep (name "miniserde") (req "^0.1") (default-features #t) (kind 0)))) (hash "00m7hs5xxmwdl6lrl2jpb78iwxddkzzy491rcc7hd209kaqzcqxi")))

(define-public crate-baus-0.2 (crate (name "baus") (vers "0.2.0") (deps (list (crate-dep (name "clap") (req "^3.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "dirs") (req "^4.0") (default-features #t) (kind 0)) (crate-dep (name "miniserde") (req "^0.1") (default-features #t) (kind 0)))) (hash "195zmmmd0hqd0i6l73nnhpyzw1v2zrbqgvdzs69b1y6w91xgklj2")))

