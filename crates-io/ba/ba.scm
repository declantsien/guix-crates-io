(define-module (crates-io ba ba) #:use-module (crates-io))

(define-public crate-babalcore-0.1 (crate (name "babalcore") (vers "0.1.0") (hash "1snq6yra7lsw1mgpq2g9h13yj0hbv4wyd0q85bf4104sbbhcj10l")))

(define-public crate-babalcore-0.2 (crate (name "babalcore") (vers "0.2.0") (deps (list (crate-dep (name "bincode") (req "^1.3") (default-features #t) (kind 0)) (crate-dep (name "euclid") (req "^0.22") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.7") (default-features #t) (kind 0)))) (hash "0srfifsb61k8a8ihqbv6923l9wvph3k6s1w8cpnl9w8az1rr0zd7")))

(define-public crate-babalcore-0.2 (crate (name "babalcore") (vers "0.2.1") (deps (list (crate-dep (name "bincode") (req "^1.3") (default-features #t) (kind 0)) (crate-dep (name "euclid") (req "^0.22") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.7") (default-features #t) (kind 0)))) (hash "163acz9h0dl839h7l0idd6yca3p7jrb6q9q43mxfjky1isd01v3y")))

(define-public crate-babalcore-0.2 (crate (name "babalcore") (vers "0.2.2") (deps (list (crate-dep (name "bincode") (req "^1.3") (default-features #t) (kind 0)) (crate-dep (name "euclid") (req "^0.22") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.7") (default-features #t) (kind 0)))) (hash "0sz1dlr3hl7p7gxbxbi7ab13z5m97l5biy58dqlp1xj2iw8qxkv0")))

(define-public crate-babalcore-0.2 (crate (name "babalcore") (vers "0.2.3") (deps (list (crate-dep (name "bincode") (req "^1.3") (default-features #t) (kind 0)) (crate-dep (name "euclid") (req "^0.22") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.7") (default-features #t) (kind 0)))) (hash "17zbcnh4m5l9h8dpggzy39sc6szb887h7wk0pfs50p4nis47n539")))

(define-public crate-babalcore-0.2 (crate (name "babalcore") (vers "0.2.4") (deps (list (crate-dep (name "bincode") (req "^1.3") (default-features #t) (kind 0)) (crate-dep (name "euclid") (req "^0.22") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.7") (default-features #t) (kind 0)))) (hash "0wpbp22w31n4f3v0csvzvnl0vqb80v6x28szv5y5f4vr64qlmgh0")))

(define-public crate-babalcore-0.3 (crate (name "babalcore") (vers "0.3.0") (deps (list (crate-dep (name "bincode") (req "^1.3") (default-features #t) (kind 0)) (crate-dep (name "euclid") (req "^0.22") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.7") (default-features #t) (kind 0)))) (hash "0q5pf13m4sby8r5rl078g8pbibnqn09jgqw014r1wxpmh5pyq051")))

(define-public crate-babalcore-0.4 (crate (name "babalcore") (vers "0.4.3") (deps (list (crate-dep (name "bincode") (req "^1.3") (default-features #t) (kind 0)) (crate-dep (name "euclid") (req "^0.22") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.7") (default-features #t) (kind 0)))) (hash "1s5xc53vz8sm32p3im06yha47ad65qcp04xa4cb6dhg13kg64vjz")))

(define-public crate-babalcore-0.5 (crate (name "babalcore") (vers "0.5.0") (deps (list (crate-dep (name "bincode") (req "^1.3") (default-features #t) (kind 0)) (crate-dep (name "euclid") (req "^0.22") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.7") (default-features #t) (kind 0)))) (hash "02kfj52p9hainq2yhk328ahwzr0sq21gqzp23y9s8vk6pr20x8k0")))

(define-public crate-babalcore-0.5 (crate (name "babalcore") (vers "0.5.1") (deps (list (crate-dep (name "bincode") (req "^1.3") (default-features #t) (kind 0)) (crate-dep (name "euclid") (req "^0.22") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.7") (default-features #t) (kind 0)))) (hash "1whidb8csyfdpd3f0g47h2jbzdfaw1dj8wi9xfln4261a8xa9j9m")))

(define-public crate-babalgame-0.2 (crate (name "babalgame") (vers "0.2.0") (deps (list (crate-dep (name "babalcore") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "gdnative") (req "^0.9.1") (default-features #t) (kind 0)))) (hash "1769kq1ahcmg5m341w7gbj8d8azhmscgwfihb1hanh1qcgncxd2i")))

(define-public crate-babalgame-0.2 (crate (name "babalgame") (vers "0.2.1") (deps (list (crate-dep (name "babalcore") (req "^0.2.1") (default-features #t) (kind 0)) (crate-dep (name "gdnative") (req "^0.9.1") (default-features #t) (kind 0)))) (hash "10h87r280n714pcjkpmb7v0i3rb2z67dlm24k0z2j9gscy9wrjyb")))

(define-public crate-babalgame-0.2 (crate (name "babalgame") (vers "0.2.2") (deps (list (crate-dep (name "babalcore") (req "^0.2.2") (default-features #t) (kind 0)) (crate-dep (name "gdnative") (req "^0.9.1") (default-features #t) (kind 0)))) (hash "10ma694ixwfmpsniaqcikmslq511nh44kxq03xg40c3dr0jl6yvc")))

(define-public crate-babalgame-0.2 (crate (name "babalgame") (vers "0.2.3") (deps (list (crate-dep (name "babalcore") (req "^0.2.3") (default-features #t) (kind 0)) (crate-dep (name "gdnative") (req "^0.9.1") (default-features #t) (kind 0)))) (hash "1pfri33azn2np931jk5hjs3vzm13ylkakpw2bms0lxwiwy1lxr49")))

(define-public crate-babalgame-0.2 (crate (name "babalgame") (vers "0.2.4") (deps (list (crate-dep (name "babalcore") (req "^0.2.4") (default-features #t) (kind 0)) (crate-dep (name "gdnative") (req "^0.9.1") (default-features #t) (kind 0)))) (hash "1aif6dk4mnnz581v6mhdnk38483mrz408bl4v691rr983204b9ga")))

(define-public crate-babalgame-0.3 (crate (name "babalgame") (vers "0.3.0") (deps (list (crate-dep (name "babalcore") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "gdnative") (req "^0.9.1") (default-features #t) (kind 0)))) (hash "1j3hhcsw3b5d8y6zb6xh300vhnml5k0iiqb7ha5173f8ixgs3d62")))

(define-public crate-babalgame-0.4 (crate (name "babalgame") (vers "0.4.3") (deps (list (crate-dep (name "babalcore") (req "^0.4.3") (default-features #t) (kind 0)) (crate-dep (name "gdnative") (req "^0.9.1") (default-features #t) (kind 0)))) (hash "0skhapganq6i2l39fxvrhad34vlqja47068gn4fddywcx6vwsh7l")))

(define-public crate-babalgame-0.5 (crate (name "babalgame") (vers "0.5.0") (deps (list (crate-dep (name "babalcore") (req "^0.5.0") (default-features #t) (kind 0)) (crate-dep (name "gdnative") (req "^0.9.3") (default-features #t) (kind 0)))) (hash "1hssr8c43czm3l0qilnlkk851wjaf2q84iz1h5vnrd9fbnb8kllb")))

(define-public crate-babalgame-0.5 (crate (name "babalgame") (vers "0.5.1") (deps (list (crate-dep (name "babalcore") (req "^0.5.1") (default-features #t) (kind 0)) (crate-dep (name "gdnative") (req "^0.9.3") (default-features #t) (kind 0)))) (hash "16rm00xixsacrwqgjj6h3p12ipdi7v1dh3h055zpyfmb30cxshpz")))

(define-public crate-babash-0.1 (crate (name "babash") (vers "0.1.1") (deps (list (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "stderrlog") (req "^0.5") (default-features #t) (kind 2)))) (hash "1bcrsdab02wwbfixsgm60wsp16i6j7hb5g515g0lqb8hvhkhmk4y")))

(define-public crate-babash-0.2 (crate (name "babash") (vers "0.2.0") (deps (list (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "stderrlog") (req "^0.5") (default-features #t) (kind 2)))) (hash "0d5qwkmpk5nfxiar36s24dv3qklg8nz2hbdac6i25kx70jm2fn4g")))

