(define-module (crates-io ba pp) #:use-module (crates-io))

(define-public crate-bappy-script-0.1 (crate (name "bappy-script") (vers "0.1.0") (deps (list (crate-dep (name "insta") (req "^1.29.0") (default-features #t) (kind 2)) (crate-dep (name "nom") (req "^7.1.0") (default-features #t) (kind 0)))) (hash "0cbyfdl5nhi3kg44x53k0fb89grcvgsckqxbf0llxzw8hd4hx38b")))

