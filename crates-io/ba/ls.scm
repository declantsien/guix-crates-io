(define-module (crates-io ba ls) #:use-module (crates-io))

(define-public crate-balsa-0.1 (crate (name "balsa") (vers "0.1.0") (deps (list (crate-dep (name "lyn") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "pretty_assertions") (req "^1.0.0") (default-features #t) (kind 0)))) (hash "07r5sfm6ynhkjkq55pqs0nb9ypf69zili2a9zgm2ca6a5f1vijnd")))

(define-public crate-balsa-0.1 (crate (name "balsa") (vers "0.1.1") (deps (list (crate-dep (name "lyn") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "pretty_assertions") (req "^1.0.0") (default-features #t) (kind 0)))) (hash "1pk1y6vl84r0csq5p81khip66dsrql30j25kyk474px7acpmzmad")))

(define-public crate-balsa-0.3 (crate (name "balsa") (vers "0.3.0") (deps (list (crate-dep (name "lyn") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "pretty_assertions") (req "^1.0.0") (default-features #t) (kind 0)))) (hash "1zg031fffbvxb6z8f3ww5yi30hz7qa3pigj0fz4i4zd5kvjz6am8")))

(define-public crate-balsa-0.3 (crate (name "balsa") (vers "0.3.2") (deps (list (crate-dep (name "lyn") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "pretty_assertions") (req "^1.0.0") (default-features #t) (kind 0)))) (hash "09j12m7w4h2gn6m3n110x81ngjzb6wvwabfdyybxzhhr8imwr4qp")))

