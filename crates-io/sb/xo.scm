(define-module (crates-io sb xo) #:use-module (crates-io))

(define-public crate-sbxor-0.1 (crate (name "sbxor") (vers "0.1.0") (deps (list (crate-dep (name "hex") (req "^0.3.2") (default-features #t) (kind 0)) (crate-dep (name "words") (req "^0.1.2") (default-features #t) (kind 0)))) (hash "1d21bskaj6347k49zz72mncm5x2mw27kfvbv9xdqwgx5293q8cbm")))

(define-public crate-sbxor-0.1 (crate (name "sbxor") (vers "0.1.1") (deps (list (crate-dep (name "hex") (req "^0.3.2") (default-features #t) (kind 0)) (crate-dep (name "words") (req "^0.1.2") (default-features #t) (kind 0)))) (hash "0i5jpzdk67pjwlbbpgwqgbri1zk0jyaiffmi9hvx1kf7i2fqpl9a")))

(define-public crate-sbxor-0.1 (crate (name "sbxor") (vers "0.1.2") (deps (list (crate-dep (name "hex") (req "^0.3.2") (default-features #t) (kind 0)) (crate-dep (name "words") (req "^0.1.2") (default-features #t) (kind 0)))) (hash "0ys0n5p0kmq66s4xa3gzfcqgm61bnzpj17pyxykvymnf0fip1zwz")))

