(define-module (crates-io sb ka) #:use-module (crates-io))

(define-public crate-sbkafka-0.1 (crate (name "sbkafka") (vers "0.1.0") (deps (list (crate-dep (name "anyhow") (req "^1.0.56") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^3.1.1") (features (quote ("env"))) (default-features #t) (kind 0)) (crate-dep (name "env_logger") (req "^0.9.0") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.13") (default-features #t) (kind 0)) (crate-dep (name "rdkafka") (req "^0.28.0") (features (quote ("cmake-build"))) (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.1.0") (features (quote ("macros" "rt-multi-thread" "time"))) (default-features #t) (kind 0)))) (hash "18mpnjxjpd7xg10hjlcwz3pzp5jz858xm15ggryvdrmxmrqk0dn7")))

