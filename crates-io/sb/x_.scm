(define-module (crates-io sb x_) #:use-module (crates-io))

(define-public crate-sbx_cloud-0.1 (crate (name "sbx_cloud") (vers "0.1.0") (deps (list (crate-dep (name "reqwest") (req "^0.11.16") (features (quote ("json"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.160") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.96") (default-features #t) (kind 0)))) (hash "14hprg0wb7gw601khm6yn7fjgqa0jd84ihalwm3bqcishk5m2lgi")))

(define-public crate-sbx_cloud-0.1 (crate (name "sbx_cloud") (vers "0.1.10") (deps (list (crate-dep (name "reqwest") (req "^0.11.16") (features (quote ("json"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.160") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.96") (default-features #t) (kind 0)))) (hash "0wfl7n0m9pr4bblzky9wqwgzdrn6z031jbrz56rkwndkkvz1fgkn")))

(define-public crate-sbx_cloud-0.1 (crate (name "sbx_cloud") (vers "0.1.11") (deps (list (crate-dep (name "reqwest") (req "^0.11.16") (features (quote ("json"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.160") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.96") (default-features #t) (kind 0)))) (hash "0gcq2qmqnjzfjx4m3df1c6cqpy2ilw71x2jviz54lk6cq09w7256")))

(define-public crate-sbx_cloud-0.1 (crate (name "sbx_cloud") (vers "0.1.12") (deps (list (crate-dep (name "reqwest") (req "^0.11.16") (features (quote ("json"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.160") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.96") (default-features #t) (kind 0)))) (hash "1hbap1fbgpnwgzazmp3ndwrqnvxkkdvjq07z7z5cfb2n2vq6j7mp")))

(define-public crate-sbx_cloud-0.1 (crate (name "sbx_cloud") (vers "0.1.13") (deps (list (crate-dep (name "reqwest") (req "^0.11.16") (features (quote ("json"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.160") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.96") (default-features #t) (kind 0)))) (hash "1w954zs4mfah6zfn9a3izxhvf1s7cmz25fjm9lpik93dar654rzl")))

(define-public crate-sbx_cloud-0.1 (crate (name "sbx_cloud") (vers "0.1.15") (deps (list (crate-dep (name "reqwest") (req "^0.11.18") (features (quote ("json"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.163") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.96") (default-features #t) (kind 0)))) (hash "1cgbg7q1lmrv25gipz8q2y9vc3sxi2hkf2gy3g31j21r2dah2mxm")))

(define-public crate-sbx_cloud-0.1 (crate (name "sbx_cloud") (vers "0.1.16") (deps (list (crate-dep (name "reqwest") (req "^0.11.18") (features (quote ("json"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.163") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.96") (default-features #t) (kind 0)))) (hash "18wni1sqp8sjjl6vgr750pjbyay21jx4il34ldg5nkzblzaisikz")))

