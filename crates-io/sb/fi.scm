(define-module (crates-io sb fi) #:use-module (crates-io))

(define-public crate-sbfiles-0.1 (crate (name "sbfiles") (vers "0.1.0") (deps (list (crate-dep (name "base64") (req "^0.21") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^4.3") (features (quote ("derive" "env"))) (default-features #t) (kind 0)) (crate-dep (name "duct") (req "^0.13") (default-features #t) (kind 2)) (crate-dep (name "flate2") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "gut") (req "^0.4.4") (default-features #t) (kind 0) (package "gchemol-gut")) (crate-dep (name "tar") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "tempfile") (req "^3") (default-features #t) (kind 2)))) (hash "1z51n34z5f6lrmd107hvhbvx5lz8ia6qgdm7nd0cb1y42054490c")))

