(define-module (crates-io sb i-) #:use-module (crates-io))

(define-public crate-sbi-rt-0.0.0 (crate (name "sbi-rt") (vers "0.0.0") (deps (list (crate-dep (name "sbi-spec") (req "^0.0.2") (default-features #t) (kind 0)))) (hash "07vf47k0f8712hjj219as1rbrrxm1dzrxqbi2aj16gn5xy3hi2sq")))

(define-public crate-sbi-rt-0.0.1 (crate (name "sbi-rt") (vers "0.0.1") (deps (list (crate-dep (name "sbi-spec") (req "^0.0.3") (default-features #t) (kind 0)))) (hash "137yb98r88sj8x4xl1l00l576kvnv6p3nw6d4y03yxsd7c2pzwh7")))

(define-public crate-sbi-rt-0.0.2 (crate (name "sbi-rt") (vers "0.0.2") (deps (list (crate-dep (name "sbi-spec") (req "^0.0.4") (default-features #t) (kind 0)))) (hash "1ds27aj9aip0h9xp6rmb05h8v2s8xlj35x013qaarf0x559kq4cc") (features (quote (("legacy" "sbi-spec/legacy") ("integer-impls") ("default"))))))

(define-public crate-sbi-rt-0.0.3 (crate (name "sbi-rt") (vers "0.0.3-rc.1") (deps (list (crate-dep (name "sbi-spec") (req "^0.0.5") (default-features #t) (kind 0)))) (hash "10s6q1anf2ar29g6sqjri28p9ivaxyk2qmd9j8r2vr59wx5gjd24") (features (quote (("legacy" "sbi-spec/legacy") ("integer-impls") ("default"))))))

(define-public crate-sbi-rt-0.0.3 (crate (name "sbi-rt") (vers "0.0.3-rc.2") (deps (list (crate-dep (name "sbi-spec") (req "^0.0.6") (default-features #t) (kind 0)))) (hash "0yp3xn8vi91wfgf5pcbjibjdxm4d33g8ynqzi3mqs6aghzq146ym") (features (quote (("legacy" "sbi-spec/legacy") ("integer-impls") ("default"))))))

(define-public crate-sbi-rt-0.0.3 (crate (name "sbi-rt") (vers "0.0.3-rc.3") (deps (list (crate-dep (name "sbi-spec") (req "^0.0.7-alpha.2") (default-features #t) (kind 0)))) (hash "0fp39wrrg540mj3bg3d1iv5bzl572k5r3f345v9il260w9m70qfn") (features (quote (("legacy" "sbi-spec/legacy") ("integer-impls") ("default"))))))

(define-public crate-sbi-rt-0.0.3 (crate (name "sbi-rt") (vers "0.0.3-rc.4") (deps (list (crate-dep (name "sbi-spec") (req "^0.0.7-alpha.3") (default-features #t) (kind 0)))) (hash "0ws91y1z1x6gkam0nr3czqxwbx4vyw2r9i9hidzh501kc09ifzw5") (features (quote (("legacy" "sbi-spec/legacy") ("integer-impls") ("default"))))))

(define-public crate-sbi-rt-0.0.3 (crate (name "sbi-rt") (vers "0.0.3-rc.5") (deps (list (crate-dep (name "sbi-spec") (req "^0.0.7-alpha.3") (default-features #t) (kind 0)))) (hash "1dnbvkamxxwb2r5nhsk58nwimbnv6bd35pid9w4wr8amja6irz8n") (features (quote (("legacy" "sbi-spec/legacy") ("integer-impls") ("default"))))))

(define-public crate-sbi-rt-0.0.3 (crate (name "sbi-rt") (vers "0.0.3") (deps (list (crate-dep (name "sbi-spec") (req "^0.0.7") (default-features #t) (kind 0)))) (hash "1l4blnbd8lhdj1v0adk5nhlfk0h44sr8km764v263p7fw6dsdfkz") (features (quote (("legacy" "sbi-spec/legacy") ("integer-impls") ("default"))))))

(define-public crate-sbi-spec-0.0.1 (crate (name "sbi-spec") (vers "0.0.1") (hash "1vlw504hg1m1nwq841jf152iqj3mg67jqpj49q4ph31nwpsanllh")))

(define-public crate-sbi-spec-0.0.2 (crate (name "sbi-spec") (vers "0.0.2") (hash "1kpwcwpkmk6bp69mk5zwxk2zn15hc2aflkqdb6qdxg6p0wxpsqbv")))

(define-public crate-sbi-spec-0.0.3 (crate (name "sbi-spec") (vers "0.0.3") (deps (list (crate-dep (name "static_assertions") (req "^1.1.0") (default-features #t) (kind 0)))) (hash "0z2wp80j1g2wshs18ydafrqgqz8djpdbsx7wm5z5i9jjk1f1lcxb")))

(define-public crate-sbi-spec-0.0.4 (crate (name "sbi-spec") (vers "0.0.4") (deps (list (crate-dep (name "static_assertions") (req "^1.1.0") (default-features #t) (kind 0)))) (hash "0xsk89zj533k4h79ygmpdklspib5c6z87qn01zysk4dmkg7jfh3d") (features (quote (("legacy") ("default"))))))

(define-public crate-sbi-spec-0.0.5 (crate (name "sbi-spec") (vers "0.0.5-rc.1") (deps (list (crate-dep (name "static_assertions") (req "^1.1.0") (default-features #t) (kind 0)))) (hash "1kb829s790jzcls8bikbrii7dwsna50v1kzjsxm3p83y12i90fpd") (features (quote (("legacy") ("default"))))))

(define-public crate-sbi-spec-0.0.5 (crate (name "sbi-spec") (vers "0.0.5-rc.2") (deps (list (crate-dep (name "static_assertions") (req "^1.1.0") (default-features #t) (kind 0)))) (hash "0bfplrmvhjsa58zacf5396vg6lawf7v82i2y15z4wlf8blmzx5f3") (features (quote (("legacy") ("default"))))))

(define-public crate-sbi-spec-0.0.5 (crate (name "sbi-spec") (vers "0.0.5") (deps (list (crate-dep (name "static_assertions") (req "^1.1.0") (default-features #t) (kind 0)))) (hash "0wnwybi08cq7sygjydfms1gd7yk1gvizfscmacha05smdpqv2dd5") (features (quote (("legacy") ("default"))))))

(define-public crate-sbi-spec-0.0.6 (crate (name "sbi-spec") (vers "0.0.6") (deps (list (crate-dep (name "static_assertions") (req "^1.1.0") (default-features #t) (kind 0)))) (hash "0952zkflhlx0w1sfy70i38nb8nb2s3p2pjcarxh6na4ivk1jdgry") (features (quote (("legacy") ("default"))))))

(define-public crate-sbi-spec-0.0.7 (crate (name "sbi-spec") (vers "0.0.7-alpha.1") (deps (list (crate-dep (name "static_assertions") (req "^1.1.0") (default-features #t) (kind 0)))) (hash "1p4fwzgnrm9yfzsy3fq51ahis2cz1mf1f2c2m5qk47fnc51bps18") (features (quote (("legacy") ("default"))))))

(define-public crate-sbi-spec-0.0.7 (crate (name "sbi-spec") (vers "0.0.7-alpha.2") (deps (list (crate-dep (name "static_assertions") (req "^1.1.0") (default-features #t) (kind 0)))) (hash "0arr8834aci6h2qldj1awr1fpwb467fx3axm1gga90ba3z1fnhc0") (features (quote (("legacy") ("default"))))))

(define-public crate-sbi-spec-0.0.7 (crate (name "sbi-spec") (vers "0.0.7-alpha.3") (deps (list (crate-dep (name "static_assertions") (req "^1.1.0") (default-features #t) (kind 0)))) (hash "0lg9pv3i9lyx1hif5jm2h4icvyk9s4h7z8mhghxva8x590693468") (features (quote (("legacy") ("default"))))))

(define-public crate-sbi-spec-0.0.7 (crate (name "sbi-spec") (vers "0.0.7") (deps (list (crate-dep (name "static_assertions") (req "^1.1.0") (default-features #t) (kind 2)))) (hash "146qb0isw8l5nz8vjp2q6jna9arb823m3indiv811p2xzc967qz6") (features (quote (("legacy") ("default"))))))

(define-public crate-sbi-testing-0.0.0 (crate (name "sbi-testing") (vers "0.0.0") (deps (list (crate-dep (name "log_crate") (req "^0.4.17") (optional #t) (default-features #t) (kind 0) (package "log")) (crate-dep (name "riscv") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "sbi-rt") (req "^0.0.1") (default-features #t) (kind 0)))) (hash "1wc48ghy0bl5nbhjhav9nmchj7b7i354nz667chlw4f068h0hjpy") (features (quote (("log" "log_crate"))))))

(define-public crate-sbi-testing-0.0.1 (crate (name "sbi-testing") (vers "0.0.1") (deps (list (crate-dep (name "log_crate") (req "^0.4.17") (optional #t) (default-features #t) (kind 0) (package "log")) (crate-dep (name "riscv") (req "^0.9.0") (default-features #t) (kind 0)) (crate-dep (name "sbi-rt") (req "^0.0.2") (default-features #t) (kind 0)) (crate-dep (name "sbi-spec") (req "^0.0.4") (default-features #t) (kind 0)))) (hash "0hvlspqxhfzy5p29pymi4y0dc2wh8irj4ir53hc2s7q1ykf26msb") (features (quote (("log" "log_crate"))))))

(define-public crate-sbi-testing-0.0.3 (crate (name "sbi-testing") (vers "0.0.3-alpha.1") (deps (list (crate-dep (name "log") (req "^0.4") (optional #t) (default-features #t) (kind 0) (package "log")) (crate-dep (name "riscv") (req "^0.11.0") (default-features #t) (kind 0)) (crate-dep (name "sbi-rt") (req "^0.0.3") (default-features #t) (kind 0)) (crate-dep (name "sbi-spec") (req "^0.0.7") (default-features #t) (kind 0)))) (hash "1khm1g6g2mgw1g0k7fhy6lgkg3iv8a16a7h23fxwcnm1mnps5ypd") (v 2) (features2 (quote (("log" "dep:log"))))))

(define-public crate-sbi-testing-0.0.3 (crate (name "sbi-testing") (vers "0.0.3-alpha.2") (deps (list (crate-dep (name "log") (req "^0.4") (optional #t) (default-features #t) (kind 0) (package "log")) (crate-dep (name "riscv") (req "^0.11.0") (default-features #t) (kind 0)) (crate-dep (name "sbi-rt") (req "^0.0.3") (default-features #t) (kind 0)) (crate-dep (name "sbi-spec") (req "^0.0.7") (default-features #t) (kind 0)))) (hash "07jk30aj6p2rp4mc0yh3s57vadqds8szznn3w6kpgpkyw0f0yp0k") (v 2) (features2 (quote (("log" "dep:log"))))))

