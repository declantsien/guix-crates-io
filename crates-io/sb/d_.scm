(define-module (crates-io sb d_) #:use-module (crates-io))

(define-public crate-sbd_lib-0.2 (crate (name "sbd_lib") (vers "0.2.0") (deps (list (crate-dep (name "byteorder") (req "^1.3") (default-features #t) (kind 0)) (crate-dep (name "chrono") (req "^0.4") (default-features #t) (kind 0)))) (hash "1abpy8cx2sq5p0yia45xlrgia92np3j1c186ig0i0p8my2v3804v")))

(define-public crate-sbd_lib-0.2 (crate (name "sbd_lib") (vers "0.2.1") (deps (list (crate-dep (name "byteorder") (req "^1.3") (default-features #t) (kind 0)) (crate-dep (name "chrono") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)))) (hash "1ji7nwnqb4mpdc5524pcdk7la2g527jpxv2gs1l5ps0pg38hym8a") (features (quote (("serde-derive" "chrono/serde" "serde"))))))

(define-public crate-sbd_lib-0.2 (crate (name "sbd_lib") (vers "0.2.2") (deps (list (crate-dep (name "byteorder") (req "^1.3") (default-features #t) (kind 0)) (crate-dep (name "chrono") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)))) (hash "020h121rvfbwar2x4m30js7zrpn4qlsjylwdg2yq3yg3q62cbj87") (features (quote (("serde-derive" "chrono/serde" "serde"))))))

