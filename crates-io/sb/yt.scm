(define-module (crates-io sb yt) #:use-module (crates-io))

(define-public crate-sbyte-0.1 (crate (name "sbyte") (vers "0.1.0") (deps (list (crate-dep (name "ctrlc") (req "^3.1.6") (default-features #t) (kind 0)) (crate-dep (name "wrecked") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "18nhp4bdz03n845c7mhibiki2hlzvbsdrfd0wz8vvxahzs5c83s7") (yanked #t)))

(define-public crate-sbyte-0.1 (crate (name "sbyte") (vers "0.1.1") (deps (list (crate-dep (name "ctrlc") (req "^3.1.6") (default-features #t) (kind 0)) (crate-dep (name "wrecked") (req "^0.1.9") (default-features #t) (kind 0)))) (hash "1kc10py0v7k66j3ya6a9v8wdy6i3fdlxx16l8nmwrjvls1zxn3z8") (yanked #t)))

(define-public crate-sbyte-0.1 (crate (name "sbyte") (vers "0.1.2") (deps (list (crate-dep (name "ctrlc") (req "^3.1.6") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.4.2") (default-features #t) (kind 0)) (crate-dep (name "wrecked") (req "^0.1.10") (default-features #t) (kind 0)))) (hash "0q9g9k4l0vk4q1m8skxmwfpa1xbjy6l9xqkkbs0a0s9wa5zxnq6l") (yanked #t)))

(define-public crate-sbyte-0.1 (crate (name "sbyte") (vers "0.1.3") (deps (list (crate-dep (name "ctrlc") (req "^3.1.6") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.4.2") (default-features #t) (kind 0)) (crate-dep (name "wrecked") (req "^0.1.10") (default-features #t) (kind 0)))) (hash "0zvqv59d1sqb0z5ma2xk0ci6zkpk972cshydcwbn6pifx5vmx7b0") (yanked #t)))

(define-public crate-sbyte-0.1 (crate (name "sbyte") (vers "0.1.4") (deps (list (crate-dep (name "ctrlc") (req "^3.1.6") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.4.2") (default-features #t) (kind 0)) (crate-dep (name "wrecked") (req "^0.1.10") (default-features #t) (kind 0)))) (hash "16b0476k0f0m9553v87v4qxc9szpsa4p95lv61bx59jh9gdch16p") (yanked #t)))

(define-public crate-sbyte-0.2 (crate (name "sbyte") (vers "0.2.0") (deps (list (crate-dep (name "ctrlc") (req "^3.1.6") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.4.2") (default-features #t) (kind 0)) (crate-dep (name "wrecked") (req "^0.1.13") (default-features #t) (kind 0)))) (hash "0fdsmgmbhz44pz3digbnriim41rbwfv76mhasywqbpxxl1ancgp1") (yanked #t)))

(define-public crate-sbyte-0.2 (crate (name "sbyte") (vers "0.2.4") (deps (list (crate-dep (name "ctrlc") (req "^3.1.6") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.4.2") (default-features #t) (kind 0)) (crate-dep (name "wrecked") (req "^0.1.13") (default-features #t) (kind 0)))) (hash "09igx6s7bi958dc6p22ij5mf1ghzhg4yicd0h9cj09ibk0pb43f5") (yanked #t)))

(define-public crate-sbyte-0.2 (crate (name "sbyte") (vers "0.2.5") (deps (list (crate-dep (name "ctrlc") (req "^3.1.6") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.4.2") (default-features #t) (kind 0)) (crate-dep (name "wrecked") (req "^0.1.13") (default-features #t) (kind 0)))) (hash "14ijs8wii6rdjzcnz98jdp8z0z1z78glz2sj005szxn07p9hjxvq") (yanked #t)))

(define-public crate-sbyte-0.2 (crate (name "sbyte") (vers "0.2.6") (deps (list (crate-dep (name "ctrlc") (req "^3.1.6") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.4.2") (default-features #t) (kind 0)) (crate-dep (name "wrecked") (req "^0.1.13") (default-features #t) (kind 0)))) (hash "0sdzv79jxzl55l15y0wgq3fbma9xa5l5cyhfhapqjyil0fzlnfc6") (yanked #t)))

(define-public crate-sbyte-0.2 (crate (name "sbyte") (vers "0.2.7") (deps (list (crate-dep (name "ctrlc") (req "^3.1.6") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.4.2") (default-features #t) (kind 0)) (crate-dep (name "wrecked") (req "^0.1.18") (default-features #t) (kind 0)))) (hash "14a7zxxh9s0566zw2jznssp39681lmfxzvcrq0agnnkzvbra2s2p") (yanked #t)))

(define-public crate-sbyte-0.2 (crate (name "sbyte") (vers "0.2.8") (deps (list (crate-dep (name "ctrlc") (req "^3.1.6") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.4.2") (default-features #t) (kind 0)) (crate-dep (name "wrecked") (req "^0.1.18") (default-features #t) (kind 0)))) (hash "0zcza7ldpc26j3r2vkf1n6kq71gdb4wl6fd9k8vpg89h4ga3ilrp") (yanked #t)))

(define-public crate-sbyte-0.2 (crate (name "sbyte") (vers "0.2.10") (deps (list (crate-dep (name "ctrlc") (req "^3.1.6") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.4.2") (default-features #t) (kind 0)) (crate-dep (name "wrecked") (req "^0.1.18") (default-features #t) (kind 0)))) (hash "1cd2aa4rkk836rhhipacxm1hxvfyr2pqwisxikxdkhl0gpmh9d67") (yanked #t)))

(define-public crate-sbyte-0.2 (crate (name "sbyte") (vers "0.2.12") (deps (list (crate-dep (name "ctrlc") (req "^3.1.6") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.4.2") (default-features #t) (kind 0)) (crate-dep (name "wrecked") (req "^0.1.18") (default-features #t) (kind 0)))) (hash "1sg3v5ibl3h0z8f0gawzfh7zgh6g07gqsb7bbmhi2f3bi2fqg7cp") (yanked #t)))

(define-public crate-sbyte-0.2 (crate (name "sbyte") (vers "0.2.13") (deps (list (crate-dep (name "ctrlc") (req "^3.1.6") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.4.2") (default-features #t) (kind 0)) (crate-dep (name "wrecked") (req "^0.1.18") (default-features #t) (kind 0)))) (hash "0f7j14z81jlz78fr7a2lb03d9rxl0q6hgl8s841d4xgld1hz0607") (yanked #t)))

(define-public crate-sbyte-0.2 (crate (name "sbyte") (vers "0.2.14") (deps (list (crate-dep (name "ctrlc") (req "^3.1.6") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.4.2") (default-features #t) (kind 0)) (crate-dep (name "wrecked") (req "^0.1.20") (default-features #t) (kind 0)))) (hash "0q8rnv9mhsg746ayrdgxr4sidwg5k509l9abcdi91pwlqsicw02i") (yanked #t)))

(define-public crate-sbyte-0.2 (crate (name "sbyte") (vers "0.2.15") (deps (list (crate-dep (name "ctrlc") (req "^3.1.6") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.4.2") (default-features #t) (kind 0)) (crate-dep (name "wrecked") (req "^0.1.21") (default-features #t) (kind 0)))) (hash "1akrp8k42jxafsfi64cnjp2lhi506vqavbs9rggfvqjg4rk94kxl") (yanked #t)))

(define-public crate-sbyte-0.2 (crate (name "sbyte") (vers "0.2.16") (deps (list (crate-dep (name "ctrlc") (req "^3.1.6") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.4.2") (default-features #t) (kind 0)) (crate-dep (name "wrecked") (req "^0.1.24") (default-features #t) (kind 0)))) (hash "0csssr7h3cvqs1jpz6cl9bg2y2p6xr5k79m5zdrjaw4v7xvfzllf") (yanked #t)))

(define-public crate-sbyte-0.2 (crate (name "sbyte") (vers "0.2.17") (deps (list (crate-dep (name "ctrlc") (req "^3.1.6") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.4.2") (default-features #t) (kind 0)) (crate-dep (name "wrecked") (req "^1.0.0") (default-features #t) (kind 0)))) (hash "1pgzw17zbwmr2iidkjs65ys7waz1jnczjyw40vmfhk6ckvvq4nkv") (yanked #t)))

(define-public crate-sbyte-0.2 (crate (name "sbyte") (vers "0.2.18") (deps (list (crate-dep (name "ctrlc") (req "^3.1.6") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.4.2") (default-features #t) (kind 0)) (crate-dep (name "wrecked") (req "^1.0.0") (default-features #t) (kind 0)))) (hash "1vrk1757ahay3hxmi9zf5r3qh0cmhra7x42z5m921fs4xfp6k76y") (yanked #t)))

(define-public crate-sbyte-0.2 (crate (name "sbyte") (vers "0.2.19") (deps (list (crate-dep (name "ctrlc") (req "^3.1.6") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.4.2") (default-features #t) (kind 0)) (crate-dep (name "wrecked") (req "^1.0.4") (default-features #t) (kind 0)))) (hash "0dvmp6v2195x23dir6m5a7hh7khpzjq7py3f554cya5ig8iryh05") (yanked #t)))

(define-public crate-sbyte-0.2 (crate (name "sbyte") (vers "0.2.20") (deps (list (crate-dep (name "ctrlc") (req "^3.1.6") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.4.2") (default-features #t) (kind 0)) (crate-dep (name "wrecked") (req "^1.0.5") (default-features #t) (kind 0)))) (hash "07f4fn0vbhhjaygh7k4n04jbz4j6ybn4zgm81ldps503jfv63cvj") (yanked #t)))

(define-public crate-sbyte-0.2 (crate (name "sbyte") (vers "0.2.21") (deps (list (crate-dep (name "ctrlc") (req "^3.1.6") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.4.2") (default-features #t) (kind 0)) (crate-dep (name "wrecked") (req "^1.0.7") (default-features #t) (kind 0)))) (hash "08y38q3cg9yb2dj9xihmnahsxwcrng7g9ayhdfskkvbml61i3b98") (yanked #t)))

(define-public crate-sbyte-0.2 (crate (name "sbyte") (vers "0.2.22") (deps (list (crate-dep (name "ctrlc") (req "^3.1.6") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.4.2") (default-features #t) (kind 0)) (crate-dep (name "wrecked") (req "^1.0.7") (default-features #t) (kind 0)))) (hash "1syrd2hdzf1q62sj7031xgq8hsgdkc3kdr3hz63iz4v2m94kika6") (yanked #t)))

(define-public crate-sbyte-0.2 (crate (name "sbyte") (vers "0.2.23") (deps (list (crate-dep (name "ctrlc") (req "^3.1.6") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.4.2") (default-features #t) (kind 0)) (crate-dep (name "wrecked") (req "^1.0.7") (default-features #t) (kind 0)))) (hash "1wxfk1cn39ik3fsrk3i9mh3bl70k2x0yd3vbs8mc0kj7r4y6jm8z") (yanked #t)))

(define-public crate-sbyte-0.2 (crate (name "sbyte") (vers "0.2.24") (deps (list (crate-dep (name "ctrlc") (req "^3.1.6") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.4.2") (default-features #t) (kind 0)) (crate-dep (name "wrecked") (req "^1.0.7") (default-features #t) (kind 0)))) (hash "1gv5yfzv5q053rsz25cfm29smjr0hf5vkacaxhyh39hm09ndmyjs") (yanked #t)))

(define-public crate-sbyte-0.2 (crate (name "sbyte") (vers "0.2.25") (deps (list (crate-dep (name "ctrlc") (req "^3.1.6") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.4.2") (default-features #t) (kind 0)) (crate-dep (name "wrecked") (req "^1.0.7") (default-features #t) (kind 0)))) (hash "0fhxwby1xk7574wqyq9pvk64qjwxa9w8ab8hf1b65rfq52vg4r1d") (yanked #t)))

(define-public crate-sbyte-0.2 (crate (name "sbyte") (vers "0.2.26") (deps (list (crate-dep (name "ctrlc") (req "^3.1.6") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.4.2") (default-features #t) (kind 0)) (crate-dep (name "wrecked") (req "^1.0.7") (default-features #t) (kind 0)))) (hash "0j012dq5gkb7lj12m01g53ds0wil84hjkkgmg1n9jrfj091lh3zj") (yanked #t)))

(define-public crate-sbyte-0.2 (crate (name "sbyte") (vers "0.2.27") (deps (list (crate-dep (name "ctrlc") (req "^3.1.6") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.4.2") (default-features #t) (kind 0)) (crate-dep (name "wrecked") (req "^1.0.7") (default-features #t) (kind 0)))) (hash "122x0vs492v3v0d780i6z223csa9pfcxcbcapw68n9lzq1s4jhn0") (yanked #t)))

(define-public crate-sbyte-0.2 (crate (name "sbyte") (vers "0.2.28") (deps (list (crate-dep (name "ctrlc") (req "^3.1.6") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.4.2") (default-features #t) (kind 0)) (crate-dep (name "wrecked") (req "^1.0.7") (default-features #t) (kind 0)))) (hash "11jwbxy7kswa9x29s3dlas1wmkww5465nx1lc6grivlcq8v4hay4") (yanked #t)))

(define-public crate-sbyte-0.2 (crate (name "sbyte") (vers "0.2.29") (deps (list (crate-dep (name "ctrlc") (req "^3.1.6") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.4.2") (default-features #t) (kind 0)) (crate-dep (name "wrecked") (req "^1.0.7") (default-features #t) (kind 0)))) (hash "18c8lzqjpaifgq344zqw112fhbraddb8vd0sdq5a7m1ly26zy5bb") (yanked #t)))

(define-public crate-sbyte-0.2 (crate (name "sbyte") (vers "0.2.30") (deps (list (crate-dep (name "ctrlc") (req "^3.1.6") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.4.2") (default-features #t) (kind 0)) (crate-dep (name "wrecked") (req "^1.0.7") (default-features #t) (kind 0)))) (hash "1pr2d2qvb67wm795xnbdk89wvmabwkchdmnfv9caqwagcpqscixl") (yanked #t)))

(define-public crate-sbyte-0.2 (crate (name "sbyte") (vers "0.2.31") (deps (list (crate-dep (name "ctrlc") (req "^3.1.6") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.4.2") (default-features #t) (kind 0)) (crate-dep (name "wrecked") (req "^1.0.7") (default-features #t) (kind 0)))) (hash "1dyd9d281dng5bkn54akah3ivkz8gkl2pmgsw9137nxmfnmysn32") (yanked #t)))

(define-public crate-sbyte-0.2 (crate (name "sbyte") (vers "0.2.32") (deps (list (crate-dep (name "ctrlc") (req "^3.1.6") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.4.2") (default-features #t) (kind 0)) (crate-dep (name "wrecked") (req "^1.0.7") (default-features #t) (kind 0)))) (hash "1fswm678ymkf5gqkpmj51asc2qqzyaamxn00naidby51kkqgync4")))

(define-public crate-sbyte-0.3 (crate (name "sbyte") (vers "0.3.0") (deps (list (crate-dep (name "ctrlc") (req "^3.1.6") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.4.2") (default-features #t) (kind 0)) (crate-dep (name "wrecked") (req "^1.1.0") (default-features #t) (kind 0)))) (hash "11x7bxm8fy27aavaisvilifb6rpl9zw7nips5qrwc0f7yasd3lm8")))

(define-public crate-sbyte-0.3 (crate (name "sbyte") (vers "0.3.1") (deps (list (crate-dep (name "ctrlc") (req "^3.1.6") (default-features #t) (kind 0)) (crate-dep (name "dirs") (req "^4.0.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.4.2") (default-features #t) (kind 0)) (crate-dep (name "windows") (req "^0.39.0") (features (quote ("Win32_System_Console" "Win32_System_Threading" "Win32_UI_Input_KeyboardAndMouse" "Win32_Foundation"))) (default-features #t) (target "cfg(windows)") (kind 0)) (crate-dep (name "wrecked") (req "^1.1.0") (default-features #t) (kind 0)))) (hash "0qw6sh226acpa2sgd80vg9sqi444ha9z1x0kjk2j4f27lgrx79k6")))

(define-public crate-sbyte-0.3 (crate (name "sbyte") (vers "0.3.2") (deps (list (crate-dep (name "ctrlc") (req "^3.1.6") (default-features #t) (kind 0)) (crate-dep (name "dirs") (req "^4.0.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.4.2") (default-features #t) (kind 0)) (crate-dep (name "windows") (req "^0.39.0") (features (quote ("Win32_System_Console" "Win32_System_Threading" "Win32_UI_Input_KeyboardAndMouse" "Win32_Foundation"))) (default-features #t) (target "cfg(windows)") (kind 0)) (crate-dep (name "wrecked") (req "^1.1.0") (default-features #t) (kind 0)))) (hash "0xfjs81n024gphqi9y2ra95phnpnmskh9ncsfmq7xl3cn8ryhsx0")))

(define-public crate-sbyte-0.3 (crate (name "sbyte") (vers "0.3.3") (deps (list (crate-dep (name "ctrlc") (req "^3.1.6") (default-features #t) (kind 0)) (crate-dep (name "dirs") (req "^4.0.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.4.2") (default-features #t) (kind 0)) (crate-dep (name "windows") (req "^0.39.0") (features (quote ("Win32_System_Console" "Win32_System_Threading" "Win32_UI_Input_KeyboardAndMouse" "Win32_Foundation"))) (default-features #t) (target "cfg(windows)") (kind 0)) (crate-dep (name "wrecked") (req "^1.1.0") (default-features #t) (kind 0)))) (hash "1l4bnnpwbljl9rc2by071cfbyfmayr00hbf6rvc0mdzdj5h7h35n")))

