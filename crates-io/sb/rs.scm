(define-module (crates-io sb rs) #:use-module (crates-io))

(define-public crate-sbrsk-0.1 (crate (name "sbrsk") (vers "0.1.0") (deps (list (crate-dep (name "syscall") (req "^0.2.1") (default-features #t) (kind 0)))) (hash "1s9chl6ycs514hlskazj8ywd4z00h9mpgqqj5np24ycijdmvyrpl")))

