(define-module (crates-io sb ox) #:use-module (crates-io))

(define-public crate-sbox-0.1 (crate (name "sbox") (vers "0.1.0") (deps (list (crate-dep (name "clone3") (req "^0.2.3") (default-features #t) (kind 0)) (crate-dep (name "nix") (req "^0.27.1") (features (quote ("signal" "user"))) (default-features #t) (kind 0)))) (hash "1p9wnr34mzsfh0h4la5gwk4zg8jpzj3904qwmzdcnni80y0hl9wv")))

(define-public crate-sbox-0.1 (crate (name "sbox") (vers "0.1.1") (deps (list (crate-dep (name "clone3") (req "^0.2.3") (default-features #t) (kind 0)) (crate-dep (name "nix") (req "^0.27.1") (features (quote ("signal" "user"))) (default-features #t) (kind 0)))) (hash "01w9i83cz87lbnnryzv2cq5z9y2dfhnp031l391z5lvvmi7g9hns")))

(define-public crate-sbox-0.1 (crate (name "sbox") (vers "0.1.2") (deps (list (crate-dep (name "nix") (req "^0.27.1") (features (quote ("signal" "user" "hostname" "fs" "mount"))) (default-features #t) (kind 0)))) (hash "1hw1slhh9cygfcfhapipncv9zx28x710mr61kp560yb24yswpkry")))

(define-public crate-sbox-0.1 (crate (name "sbox") (vers "0.1.3") (deps (list (crate-dep (name "nix") (req "^0.27.1") (features (quote ("signal" "user" "hostname" "fs" "mount"))) (default-features #t) (kind 0)))) (hash "132lmjx9s5pc6az5xnybywxz5mmsg43blzzgxn95wixn2di912sh")))

(define-public crate-sbox-0.1 (crate (name "sbox") (vers "0.1.4") (deps (list (crate-dep (name "nix") (req "^0.27.1") (features (quote ("signal" "user" "hostname" "fs" "mount"))) (default-features #t) (kind 0)))) (hash "0hwqfqq60x4pax044fipnm2m1ms1d4650c08bnx5y755bjk64rqi")))

(define-public crate-sbox-0.1 (crate (name "sbox") (vers "0.1.5") (deps (list (crate-dep (name "nix") (req "^0.27.1") (features (quote ("signal" "user" "hostname" "fs" "mount"))) (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 2)))) (hash "18ykdx1vw8261s5rjrcc87b6w1bnqk6zjyqg41x83b7lipwkcjl8")))

(define-public crate-sbox-0.1 (crate (name "sbox") (vers "0.1.6") (deps (list (crate-dep (name "nix") (req "^0.27.1") (features (quote ("signal" "user" "hostname" "fs" "mount" "sched"))) (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 2)))) (hash "10z5f13ms532ybq5qyk19y5r4vf3lxsfqgfj50r0hzgbszx4q446")))

(define-public crate-sbox-0.1 (crate (name "sbox") (vers "0.1.7") (deps (list (crate-dep (name "nix") (req "^0.27.1") (features (quote ("signal" "user" "hostname" "fs" "mount" "sched"))) (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 2)) (crate-dep (name "tar") (req "^0.4.40") (default-features #t) (kind 0)))) (hash "13yrh2ia4kwp35cw50xfm23w6kpf8lmga5g753bx0jrh10djs5wb")))

(define-public crate-sbox-0.1 (crate (name "sbox") (vers "0.1.8") (deps (list (crate-dep (name "nix") (req "^0.27.1") (features (quote ("signal" "user" "hostname" "fs" "mount" "sched"))) (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 2)) (crate-dep (name "tar") (req "^0.4.40") (default-features #t) (kind 0)))) (hash "1i1z54j49phj6mlrsw6zsi5c34kmq2lpn7da23v01rs0wza5lklf")))

