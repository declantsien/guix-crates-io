(define-module (crates-io sb s-) #:use-module (crates-io))

(define-public crate-sbs-api-0.1 (crate (name "sbs-api") (vers "0.1.0") (deps (list (crate-dep (name "sbs-api-internal") (req "^0.1.1") (default-features #t) (kind 0)) (crate-dep (name "sbs-api-macro") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "1k4m7sv6gb5jgrvzl0f01z8l0l2vdmq5qh8d09bi9kw6sby1k7wf")))

(define-public crate-sbs-api-0.1 (crate (name "sbs-api") (vers "0.1.12") (deps (list (crate-dep (name "sbs-api-internal") (req "^0.1.1") (default-features #t) (kind 0)) (crate-dep (name "sbs-api-macro") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "0fk9w3cjr7lx2h60a3ggw9bqa725f3z974s0wglc2m4jkr772r3d")))

(define-public crate-sbs-api-0.1 (crate (name "sbs-api") (vers "0.1.13") (deps (list (crate-dep (name "sbs-api-internal") (req "^0.1.1") (default-features #t) (kind 0)) (crate-dep (name "sbs-api-macro") (req "^0.1.12") (default-features #t) (kind 0)))) (hash "1gymzi0m5ns2fj5bwzpm4hr6hmapswv1w8ygr3iqxcgzmyfzv9fv")))

(define-public crate-sbs-api-0.1 (crate (name "sbs-api") (vers "0.1.14") (deps (list (crate-dep (name "sbs-api-internal") (req "^0.1.1") (default-features #t) (kind 0)) (crate-dep (name "sbs-api-macro") (req "^0.1.13") (default-features #t) (kind 0)))) (hash "0qhf4q6b6yrnxd4kaf6ifymikn1z9azz68y1lqjqc92vi5ii46gi")))

(define-public crate-sbs-api-0.1 (crate (name "sbs-api") (vers "0.1.15") (deps (list (crate-dep (name "sbs-api-internal") (req "^0.1.1") (default-features #t) (kind 0)) (crate-dep (name "sbs-api-macro") (req "^0.1.14") (default-features #t) (kind 0)))) (hash "0d4can5mjyy47zrxnrm5kri378iq4j5aqyaysgy1mbz2caqq4ihj")))

(define-public crate-sbs-api-internal-0.1 (crate (name "sbs-api-internal") (vers "0.1.1") (hash "17drmbvar1wqwgjrb4nvq235x7gkg8i7ya363vjkflfadi8ic9dn")))

(define-public crate-sbs-api-macro-0.1 (crate (name "sbs-api-macro") (vers "0.1.0") (deps (list (crate-dep (name "quote") (req "^1.0.36") (default-features #t) (kind 0)) (crate-dep (name "sbs-api-internal") (req "^0.1.1") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2.0.61") (default-features #t) (kind 0)))) (hash "159y69fxlxanjc0iwlp33sv6y249scbdbcwyn0wsd3vpk5bvbayn")))

(define-public crate-sbs-api-macro-0.1 (crate (name "sbs-api-macro") (vers "0.1.1") (deps (list (crate-dep (name "quote") (req "^1.0.36") (default-features #t) (kind 0)) (crate-dep (name "sbs-api-internal") (req "^0.1.1") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2.0.61") (default-features #t) (kind 0)))) (hash "17sgliyjhg04wk4ww1cqy8fg215jbhn5rb2xmxnhrm2fbqdwakmr")))

(define-public crate-sbs-api-macro-0.1 (crate (name "sbs-api-macro") (vers "0.1.11") (deps (list (crate-dep (name "quote") (req "^1.0.36") (default-features #t) (kind 0)) (crate-dep (name "sbs-api-internal") (req "^0.1.1") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2.0.61") (default-features #t) (kind 0)))) (hash "1qc783i6wm9vvyxqfwm1qmzndh2y69d9qf3wdzlnfv76awv4cczp")))

(define-public crate-sbs-api-macro-0.1 (crate (name "sbs-api-macro") (vers "0.1.12") (deps (list (crate-dep (name "quote") (req "^1.0.36") (default-features #t) (kind 0)) (crate-dep (name "sbs-api-internal") (req "^0.1.1") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2.0.61") (default-features #t) (kind 0)))) (hash "0ppg8my2a26cas06jb57h024dilyznk2hrxw0hxrjd0gaai8h92s")))

(define-public crate-sbs-api-macro-0.1 (crate (name "sbs-api-macro") (vers "0.1.13") (deps (list (crate-dep (name "quote") (req "^1.0.36") (default-features #t) (kind 0)) (crate-dep (name "sbs-api-internal") (req "^0.1.1") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2.0.61") (default-features #t) (kind 0)))) (hash "0mnvsk5jdbprnyhfiy87fhzv2n26dizn2pj3bg0qrjvb6yfrr26s")))

(define-public crate-sbs-api-macro-0.1 (crate (name "sbs-api-macro") (vers "0.1.14") (deps (list (crate-dep (name "quote") (req "^1.0.36") (default-features #t) (kind 0)) (crate-dep (name "sbs-api-internal") (req "^0.1.1") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2.0.61") (default-features #t) (kind 0)))) (hash "18n3f89xm27w72a75qk37j61y42g27agxr392lna58j9s1xyirza")))

