(define-module (crates-io sb -r) #:use-module (crates-io))

(define-public crate-sb-rotary-encoder-0.1 (crate (name "sb-rotary-encoder") (vers "0.1.0") (hash "0f1rl2zh4s10125dhwd57bz5si9746w5jg0l6l1hxxrjal7wms22") (rust-version "1.56")))

(define-public crate-sb-rust-library-0.1 (crate (name "sb-rust-library") (vers "0.1.0") (deps (list (crate-dep (name "image") (req "^0.23.12") (default-features #t) (kind 0)) (crate-dep (name "indicatif") (req "^0.15.0") (default-features #t) (kind 0)))) (hash "1hg02ma2lf76sfawpc7x8rbghag3q67988id5hpd28xcar5n98c5")))

(define-public crate-sb-rust-library-0.1 (crate (name "sb-rust-library") (vers "0.1.1") (deps (list (crate-dep (name "image") (req "^0.23.12") (default-features #t) (kind 0)) (crate-dep (name "indicatif") (req "^0.15.0") (default-features #t) (kind 0)))) (hash "1pnn32r1gyswx5fz2i8n98iwn3lvykm4sbva5533b82qk85pdczr")))

(define-public crate-sb-rust-library-0.1 (crate (name "sb-rust-library") (vers "0.1.2") (deps (list (crate-dep (name "image") (req "^0.23.12") (default-features #t) (kind 0)) (crate-dep (name "indicatif") (req "^0.15.0") (default-features #t) (kind 0)))) (hash "1p785xslbprjlq8bp161864781ri18awx23ffrqa35qmg45zidvc")))

(define-public crate-sb-rust-library-0.1 (crate (name "sb-rust-library") (vers "0.1.3") (deps (list (crate-dep (name "image") (req "^0.23.12") (default-features #t) (kind 0)) (crate-dep (name "indicatif") (req "^0.15.0") (default-features #t) (kind 0)))) (hash "1b1y5nsi6xas1sp9vws5j79qi4jjbj65xggk36ilpb5hapyzifkg")))

(define-public crate-sb-rust-library-0.1 (crate (name "sb-rust-library") (vers "0.1.4") (deps (list (crate-dep (name "image") (req "^0.23.12") (default-features #t) (kind 0)) (crate-dep (name "indicatif") (req "^0.15.0") (default-features #t) (kind 0)))) (hash "1szqzz35q19z46x9f3b0njzb9ajixbl62anc7rl10nxilb6l7dz5")))

(define-public crate-sb-rust-library-0.1 (crate (name "sb-rust-library") (vers "0.1.5") (deps (list (crate-dep (name "image") (req "^0.23.12") (default-features #t) (kind 0)) (crate-dep (name "indicatif") (req "^0.15.0") (default-features #t) (kind 0)))) (hash "18zd6db5hdp7446h9w2jfvlxarh6gqiqip9a9pj4g59my23b28yd")))

(define-public crate-sb-rust-library-0.1 (crate (name "sb-rust-library") (vers "0.1.6") (deps (list (crate-dep (name "image") (req "^0.23.12") (default-features #t) (kind 0)) (crate-dep (name "indicatif") (req "^0.15.0") (default-features #t) (kind 0)))) (hash "1qfa6kklzz0349k96qnqi9b616id3cwgmy7j16r2xicjxr4vkm0r")))

(define-public crate-sb-rust-library-0.1 (crate (name "sb-rust-library") (vers "0.1.7") (deps (list (crate-dep (name "image") (req "^0.23.12") (default-features #t) (kind 0)) (crate-dep (name "indicatif") (req "^0.15.0") (default-features #t) (kind 0)))) (hash "194sccrjay6zh7lfl9hxx98ahadhqgl5y126cah5rifvkhsqad8l")))

