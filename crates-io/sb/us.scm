(define-module (crates-io sb us) #:use-module (crates-io))

(define-public crate-sbus-0.1 (crate (name "sbus") (vers "0.1.0") (deps (list (crate-dep (name "arraydeque") (req "~0.4") (kind 0)))) (hash "0vn1hqwy0n95x529938d41pi9y3szpzjp76883rd10j0cbjpi2py")))

(define-public crate-sbus-0.2 (crate (name "sbus") (vers "0.2.0") (deps (list (crate-dep (name "arraydeque") (req "~0.4") (kind 0)))) (hash "1wmpqarpqc6m5kd28l9brmdwfcgwqq3zvvqqq5vsw99y04xv162x")))

(define-public crate-sbus-0.2 (crate (name "sbus") (vers "0.2.1") (deps (list (crate-dep (name "arraydeque") (req "~0.4") (kind 0)))) (hash "1hyqc0gnhvqcxdkb0m48pa90i5jjp76jn5z2hflsj1clw6sw1qyb")))

(define-public crate-sbus-0.2 (crate (name "sbus") (vers "0.2.2") (deps (list (crate-dep (name "arraydeque") (req "~0.4") (kind 0)))) (hash "1mzpczxzzmny96ry4wmrpbpyr1xcx1228rffmkjpls92rhcff42j")))

(define-public crate-sbus-parser-0.0.1 (crate (name "sbus-parser") (vers "0.0.1") (hash "04x8gjml8yi11h9wac6pfq9hgf4n5xncg4dv68jqjy22wwfxdp14")))

(define-public crate-sbus-parser-0.0.2 (crate (name "sbus-parser") (vers "0.0.2") (hash "1c13n9b6k2mglvy5jbabv84xrn30m4sa29kd29qs2268rj12pcq4")))

(define-public crate-sbus-parser-0.1 (crate (name "sbus-parser") (vers "0.1.0") (deps (list (crate-dep (name "hex-literal") (req "^0.3") (default-features #t) (kind 2)))) (hash "0vddn1fz271snsxz78jgn49qgnnfir3m106vxl3x7kp2sin7a06m")))

