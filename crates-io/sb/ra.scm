(define-module (crates-io sb ra) #:use-module (crates-io))

(define-public crate-sbrain-0.2 (crate (name "sbrain") (vers "0.2.1") (hash "0qhydqqpis3z7qi7qnp9lpyq5cjbcnsvjz4xfyxv2i5c8lzd203a")))

(define-public crate-sbrain-0.3 (crate (name "sbrain") (vers "0.3.0") (hash "0jx3zvffralibix9fgvpwkf7dh2ypk7xafvjpnf7rf63rs4mn0aa")))

(define-public crate-sbrain-0.3 (crate (name "sbrain") (vers "0.3.1") (hash "197pr836sy8lw33fsbqpj9rw2q3w1rknxypjm8z2dafhqqx60jnk") (yanked #t)))

(define-public crate-sbrain-0.3 (crate (name "sbrain") (vers "0.3.2") (hash "0r28716va0azp8zvlx9d156xigwgmgcvmpmkhindyyvgsa4cj4dr")))

(define-public crate-sbrain-0.4 (crate (name "sbrain") (vers "0.4.1") (hash "01ypi1gwygmk0lrrmpdz5kmic35bz3jmlzps6s49whql7m4w1krv")))

(define-public crate-sbrain-0.4 (crate (name "sbrain") (vers "0.4.2") (hash "1ssidzil03hacp5p2lm6cc77yr0hryzd7vjbwnxl90r54apj14x7")))

(define-public crate-sbrain-0.5 (crate (name "sbrain") (vers "0.5.0") (hash "065pkih9b10n3mlwcs0f7fyrhdfkic6i7yq71xn4ccjh17w3mq7i")))

(define-public crate-sbrain-0.99 (crate (name "sbrain") (vers "0.99.0") (hash "1jb48vp7454whhvz6f3shil0asx45q6cdfyp94pgv5rmrfjcjrs2")))

(define-public crate-sbral-0.1 (crate (name "sbral") (vers "0.1.0") (deps (list (crate-dep (name "arrayvec") (req "^0.4.10") (default-features #t) (kind 0)) (crate-dep (name "criterion") (req "^0.2") (default-features #t) (kind 2)) (crate-dep (name "dogged") (req "^0.2") (default-features #t) (kind 2)) (crate-dep (name "im-rc") (req "^13") (default-features #t) (kind 2)) (crate-dep (name "proptest") (req "^0.9") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.6") (default-features #t) (kind 2)) (crate-dep (name "rand_pcg") (req "^0.1") (default-features #t) (kind 2)) (crate-dep (name "rpds") (req "^0.6") (default-features #t) (kind 2)))) (hash "1k6vap1k6yi7mh1wr4b2dgwg6skgri6gw5l1hl88w34s27wixfza") (features (quote (("threadsafe"))))))

