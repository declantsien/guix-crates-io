(define-module (crates-io sb pd) #:use-module (crates-io))

(define-public crate-sbpdump-0.1 (crate (name "sbpdump") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^2.33.0") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)))) (hash "0yw81z2nraflyya3m3dbnn13y3kvn62af4hzrz3blhb0fhcnvkqd")))

(define-public crate-sbpdump-0.1 (crate (name "sbpdump") (vers "0.1.1") (deps (list (crate-dep (name "clap") (req "^2.33.0") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)))) (hash "0n6qbpdh67hsg8rjr7avbmjv43lfb07fk30m9j2kqpw0mdmwlkf5")))

