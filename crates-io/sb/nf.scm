(define-module (crates-io sb nf) #:use-module (crates-io))

(define-public crate-sbnf-0.1 (crate (name "sbnf") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "~2.27.0") (features (quote ("wrap_help"))) (kind 0)) (crate-dep (name "indexmap") (req "^1.3.2") (default-features #t) (kind 0)) (crate-dep (name "maplit") (req "^1.0.2") (default-features #t) (kind 2)) (crate-dep (name "matches") (req "^0.1.8") (default-features #t) (kind 2)))) (hash "14903bm1y5x2vp88vqdnkp5fhwk77nkifyq383z8161ss1h0k1ym")))

(define-public crate-sbnf-0.3 (crate (name "sbnf") (vers "0.3.0") (deps (list (crate-dep (name "base64") (req "^0.12.2") (default-features #t) (kind 0)) (crate-dep (name "indexmap") (req "^1.3.2") (default-features #t) (kind 0)) (crate-dep (name "maplit") (req "^1.0.2") (default-features #t) (kind 2)) (crate-dep (name "matches") (req "^0.1.8") (default-features #t) (kind 2)))) (hash "0xpd5rkaxzvpsdq7y1npr8bbd6cqf98x7q70wmfq6zjvs01z2jbs")))

(define-public crate-sbnf-0.3 (crate (name "sbnf") (vers "0.3.1") (deps (list (crate-dep (name "base64") (req "^0.12.2") (default-features #t) (kind 0)) (crate-dep (name "indexmap") (req "^1.3.2") (default-features #t) (kind 0)) (crate-dep (name "maplit") (req "^1.0.2") (default-features #t) (kind 2)) (crate-dep (name "matches") (req "^0.1.8") (default-features #t) (kind 2)))) (hash "045v9lq7vn19kxdnyzlfxj9mj5aj0kafb3x0zaj8n5bvvm9v1x1k")))

(define-public crate-sbnf-0.4 (crate (name "sbnf") (vers "0.4.0") (deps (list (crate-dep (name "base64") (req "^0.12.2") (default-features #t) (kind 0)) (crate-dep (name "indexmap") (req "^1.3.2") (default-features #t) (kind 0)) (crate-dep (name "maplit") (req "^1.0.2") (default-features #t) (kind 2)) (crate-dep (name "matches") (req "^0.1.8") (default-features #t) (kind 2)))) (hash "1szywn7di9h0plrww85c8lw1ygcc02q5h7ybb5326xmar91iil2l")))

(define-public crate-sbnf-0.5 (crate (name "sbnf") (vers "0.5.0") (deps (list (crate-dep (name "base64") (req "^0.12.2") (default-features #t) (kind 0)) (crate-dep (name "indexmap") (req "^1.3.2") (default-features #t) (kind 0)) (crate-dep (name "maplit") (req "^1.0.2") (default-features #t) (kind 2)) (crate-dep (name "matches") (req "^0.1.8") (default-features #t) (kind 2)))) (hash "0cfwq2a51yi6r838yj9g8cglp4adiyvjj42jn7vj0kziw80n0id0")))

(define-public crate-sbnf-0.5 (crate (name "sbnf") (vers "0.5.1") (deps (list (crate-dep (name "base64") (req "^0.12.2") (default-features #t) (kind 0)) (crate-dep (name "indexmap") (req "^1.3.2") (default-features #t) (kind 0)) (crate-dep (name "maplit") (req "^1.0.2") (default-features #t) (kind 2)) (crate-dep (name "matches") (req "^0.1.8") (default-features #t) (kind 2)))) (hash "1bwhxhwrgyprh000llky5mspl765d322insb400jf1pxkz0bvkj5")))

(define-public crate-sbnf-0.6 (crate (name "sbnf") (vers "0.6.0") (deps (list (crate-dep (name "base64") (req "^0.21.1") (default-features #t) (kind 0)) (crate-dep (name "bumpalo") (req "^3.13.0") (default-features #t) (kind 0)) (crate-dep (name "indexmap") (req "^1.9.3") (default-features #t) (kind 0)) (crate-dep (name "maplit") (req "^1.0.2") (default-features #t) (kind 2)) (crate-dep (name "matches") (req "^0.1.8") (default-features #t) (kind 2)) (crate-dep (name "symbol_table") (req "^0.3.0") (default-features #t) (kind 0)))) (hash "0h2s5lz7hxpir4mkdhxp0nalf0p9p9ilgxlh0fymmdxn30mja5p7")))

(define-public crate-sbnf-0.6 (crate (name "sbnf") (vers "0.6.1") (deps (list (crate-dep (name "base64") (req "^0.21.1") (default-features #t) (kind 0)) (crate-dep (name "bumpalo") (req "^3.13.0") (default-features #t) (kind 0)) (crate-dep (name "indexmap") (req "^1.9.3") (default-features #t) (kind 0)) (crate-dep (name "maplit") (req "^1.0.2") (default-features #t) (kind 2)) (crate-dep (name "matches") (req "^0.1.8") (default-features #t) (kind 2)) (crate-dep (name "symbol_table") (req "^0.3.0") (default-features #t) (kind 0)))) (hash "04s01mzjfg0i7if1xgmr7ihvpi3dpay818drg9a2l4yxg3igcg7c")))

(define-public crate-sbnf-0.6 (crate (name "sbnf") (vers "0.6.2") (deps (list (crate-dep (name "base64") (req "^0.21.4") (default-features #t) (kind 0)) (crate-dep (name "bumpalo") (req "^3.14.0") (default-features #t) (kind 0)) (crate-dep (name "hashbrown") (req "^0.14.0") (default-features #t) (kind 0)) (crate-dep (name "indexmap") (req "^2.0") (default-features #t) (kind 0)) (crate-dep (name "matches") (req "^0.1.8") (default-features #t) (kind 2)))) (hash "1k2nr56s264yyv7kpsj48f4gidhxwy47gb9k3dbdzq4724nr7041")))

(define-public crate-sbnf-0.6 (crate (name "sbnf") (vers "0.6.3") (deps (list (crate-dep (name "base64") (req "^0.21.4") (default-features #t) (kind 0)) (crate-dep (name "bumpalo") (req "^3.14.0") (default-features #t) (kind 0)) (crate-dep (name "hashbrown") (req "^0.14.0") (default-features #t) (kind 0)) (crate-dep (name "indexmap") (req "^2.0") (default-features #t) (kind 0)) (crate-dep (name "matches") (req "^0.1.8") (default-features #t) (kind 2)))) (hash "0nnzd3b0nh6xvhrf2njm7rzfd6c3zsq369z1rwlwz44wqjz7q1l0")))

(define-public crate-sbnf-0.6 (crate (name "sbnf") (vers "0.6.4") (deps (list (crate-dep (name "base64") (req "^0.21.4") (default-features #t) (kind 0)) (crate-dep (name "bumpalo") (req "^3.14.0") (default-features #t) (kind 0)) (crate-dep (name "hashbrown") (req "^0.14.0") (default-features #t) (kind 0)) (crate-dep (name "indexmap") (req "^2.0") (default-features #t) (kind 0)) (crate-dep (name "matches") (req "^0.1.8") (default-features #t) (kind 2)))) (hash "1329rdpyrmsh73clgq2y8qr8an1l5xf553wgi7j3f8k4qxqpimcw")))

(define-public crate-sbnfc-0.3 (crate (name "sbnfc") (vers "0.3.1") (deps (list (crate-dep (name "clap") (req "~2.27.0") (features (quote ("wrap_help"))) (kind 0)) (crate-dep (name "sbnf") (req "^0.3.1") (default-features #t) (kind 0)))) (hash "09qi7hrgnm3vj649s6kbvmx509sbcsm4icmvsn9qgp381nx6sk8j")))

(define-public crate-sbnfc-0.4 (crate (name "sbnfc") (vers "0.4.0") (deps (list (crate-dep (name "clap") (req "~2.27.0") (features (quote ("wrap_help"))) (kind 0)) (crate-dep (name "sbnf") (req "^0.4.0") (default-features #t) (kind 0)))) (hash "0fvlsnskh463qk6dz09b409iz89cfz1j5gzwmy3nlnwzylrbiv76")))

(define-public crate-sbnfc-0.5 (crate (name "sbnfc") (vers "0.5.0") (deps (list (crate-dep (name "clap") (req "~2.27.0") (features (quote ("wrap_help"))) (kind 0)) (crate-dep (name "sbnf") (req "^0.5.0") (default-features #t) (kind 0)))) (hash "0ips2h8is466bk42cnl52v5nxqnh0cajr24mvllp22iz0lrrigqs")))

(define-public crate-sbnfc-0.5 (crate (name "sbnfc") (vers "0.5.1") (deps (list (crate-dep (name "clap") (req "~2.27.0") (features (quote ("wrap_help"))) (kind 0)) (crate-dep (name "sbnf") (req "^0.5.1") (default-features #t) (kind 0)))) (hash "0hrgp2fdx06ckq0zyrzn6dl2jnfq4vvlhfixwlr9c0abd5y0w8yj")))

(define-public crate-sbnfc-0.6 (crate (name "sbnfc") (vers "0.6.0") (deps (list (crate-dep (name "clap") (req "~2.27.0") (features (quote ("wrap_help"))) (kind 0)) (crate-dep (name "sbnf") (req "^0.6.0") (default-features #t) (kind 0)))) (hash "06qjzk38x36pcjz7apd2p01rs2rjz48fl0iq9y61w3r4fifp521l")))

(define-public crate-sbnfc-0.6 (crate (name "sbnfc") (vers "0.6.1") (deps (list (crate-dep (name "clap") (req "~2.27.0") (features (quote ("wrap_help"))) (kind 0)) (crate-dep (name "sbnf") (req "^0.6.0") (default-features #t) (kind 0)))) (hash "1g5qf9mj1cw6vkk69m8pj5kmg60vm291jmmmfammczgilsg86ccq")))

(define-public crate-sbnfc-0.6 (crate (name "sbnfc") (vers "0.6.2") (deps (list (crate-dep (name "clap") (req "~2.27.0") (features (quote ("wrap_help"))) (kind 0)) (crate-dep (name "sbnf") (req "^0.6.2") (default-features #t) (kind 0)))) (hash "012q9a2zynma0qq2n0haq273zinxxqcpivc7kvpgjglsb73bkcd7")))

(define-public crate-sbnfc-0.6 (crate (name "sbnfc") (vers "0.6.3") (deps (list (crate-dep (name "clap") (req "~2.27.0") (features (quote ("wrap_help"))) (kind 0)) (crate-dep (name "sbnf") (req "^0.6.3") (default-features #t) (kind 0)))) (hash "176xcdrz1may15s553yv655smz8f6c60hh04wnn6bfv656g710py")))

(define-public crate-sbnfc-0.6 (crate (name "sbnfc") (vers "0.6.4") (deps (list (crate-dep (name "clap") (req "~2.27.0") (features (quote ("wrap_help"))) (kind 0)) (crate-dep (name "sbnf") (req "^0.6.4") (default-features #t) (kind 0)))) (hash "1w5z362rdmsv4c7kjhava0pj8lkk14hv25ad2wlfl2kk6j8vj01g")))

