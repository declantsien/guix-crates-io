(define-module (crates-io sb ac) #:use-module (crates-io))

(define-public crate-sback-0.1 (crate (name "sback") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^3.2.17") (default-features #t) (kind 0)) (crate-dep (name "flate2") (req "^1.0.24") (default-features #t) (kind 0)) (crate-dep (name "tar") (req "^0.4.38") (default-features #t) (kind 0)) (crate-dep (name "xdg") (req "^2.4.1") (default-features #t) (kind 0)))) (hash "1g41ws63cqw5gx8w5diinna8nz1ydqd5y4dw6bcpym6gws0q9il6")))

(define-public crate-sback-0.2 (crate (name "sback") (vers "0.2.0") (deps (list (crate-dep (name "clap") (req "^3.2.17") (default-features #t) (kind 0)) (crate-dep (name "flate2") (req "^1.0.24") (default-features #t) (kind 0)) (crate-dep (name "tar") (req "^0.4.38") (default-features #t) (kind 0)) (crate-dep (name "xdg") (req "^2.4.1") (default-features #t) (kind 0)))) (hash "0s4x5apymwdi16czq5mxgy59pd6yxinx3g10mizyscfnjzv68xrb")))

(define-public crate-sback-0.3 (crate (name "sback") (vers "0.3.0") (deps (list (crate-dep (name "clap") (req "^3.2.17") (default-features #t) (kind 0)) (crate-dep (name "flate2") (req "^1.0.24") (default-features #t) (kind 0)) (crate-dep (name "tar") (req "^0.4.38") (default-features #t) (kind 0)) (crate-dep (name "xdg") (req "^2.4.1") (default-features #t) (kind 0)))) (hash "10ry757sv6czx9v5hxaj8lnmih9ghkna9nz5qk78n4ybv13yyvpz")))

