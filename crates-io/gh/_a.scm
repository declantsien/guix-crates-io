(define-module (crates-io gh _a) #:use-module (crates-io))

(define-public crate-gh_actions-0.0.2 (crate (name "gh_actions") (vers "0.0.2") (deps (list (crate-dep (name "dotenv") (req "^0.15.0") (default-features #t) (kind 0)) (crate-dep (name "env_logger") (req "^0.9.0") (default-features #t) (kind 0)) (crate-dep (name "hubcaps") (req "^0.6.2") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.17") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_yaml") (req "^0.9.2") (default-features #t) (kind 0)))) (hash "0b5fvrblf1l397lcqib00xxkb7ql9gs4ac3wks3h5p0nbp7fi9pq") (rust-version "1.56")))

(define-public crate-gh_actions-0.0.3 (crate (name "gh_actions") (vers "0.0.3") (deps (list (crate-dep (name "dotenv") (req "^0.15.0") (default-features #t) (kind 0)) (crate-dep (name "env_logger") (req "^0.9.0") (default-features #t) (kind 0)) (crate-dep (name "hubcaps") (req "^0.6.2") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.17") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_yaml") (req "^0.9.2") (default-features #t) (kind 0)))) (hash "0x84wdckh91kzrck6lwsdh07bj9000nkp0c9p5nyqfbfaiy0cvqg") (rust-version "1.56")))

(define-public crate-gh_api-0.1 (crate (name "gh_api") (vers "0.1.0") (deps (list (crate-dep (name "reqwest") (req "^0.11.0") (default-features #t) (kind 0) (package "reqwest-wasm")))) (hash "0wnx2gqs5g4m6r6nvrdwx4rhb4ry1b6mvfx7vn1pdgqfghx9byxj") (features (quote (("gzip" "reqwest/gzip") ("default" "gzip"))))))

(define-public crate-gh_api-0.2 (crate (name "gh_api") (vers "0.2.0") (deps (list (crate-dep (name "reqwest") (req "^0.11.12") (default-features #t) (kind 0)))) (hash "1m3qyrxw1kcw2fyy818fqic38gagq7hjpyp9qifalr8b9r27fjpq") (features (quote (("gzip" "reqwest/gzip") ("default" "gzip"))))))

(define-public crate-gh_api-0.3 (crate (name "gh_api") (vers "0.3.0") (deps (list (crate-dep (name "reqwest") (req "^0.11.12") (default-features #t) (kind 0)))) (hash "1iflz6r4kc5mz5swlvlbhrmyisk94ic98pnjnhgicks8lyz53ljv") (features (quote (("gzip" "reqwest/gzip") ("default" "gzip"))))))

(define-public crate-gh_api-0.3 (crate (name "gh_api") (vers "0.3.1") (deps (list (crate-dep (name "reqwest") (req "^0.11.12") (default-features #t) (target "cfg(not(target_arch = \"wasm32\"))") (kind 0)) (crate-dep (name "reqwest") (req "^0.11.16") (default-features #t) (target "cfg(target_arch = \"wasm32\")") (kind 0) (package "reqwest-wasm")))) (hash "07mq9b7mvrrv4rh7zgpfb8kpqdrpf0b5934kb0ngqk0nr9wma7gi") (features (quote (("gzip" "reqwest/gzip") ("default" "gzip"))))))

