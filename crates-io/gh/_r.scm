(define-module (crates-io gh _r) #:use-module (crates-io))

(define-public crate-gh_release-0.1 (crate (name "gh_release") (vers "0.1.0") (deps (list (crate-dep (name "reqwest") (req "^0.11.11") (features (quote ("blocking" "serde_json" "json" "rustls-tls"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)))) (hash "10rwfzw07jiyai937k86n0mbp9b06y1arjs7jkbbd4r9qdnznzrm")))

(define-public crate-gh_release-0.1 (crate (name "gh_release") (vers "0.1.1") (deps (list (crate-dep (name "reqwest") (req "^0.11.11") (features (quote ("blocking" "serde_json" "json" "rustls-tls"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)))) (hash "0m8azsqavhbnh88nl52q79iwldrw39wjc1nnzrmnf96kk94rwsnd") (features (quote (("git"))))))

