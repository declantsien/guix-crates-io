(define-module (crates-io gh -e) #:use-module (crates-io))

(define-public crate-gh-emoji-1 (crate (name "gh-emoji") (vers "1.0.0") (deps (list (crate-dep (name "phf") (req "^0.7.23") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.1.0") (default-features #t) (kind 0)))) (hash "01y8yya9i8igag4w624kidfl82gyvafvggq30nqwmf80hwpcmsap")))

(define-public crate-gh-emoji-1 (crate (name "gh-emoji") (vers "1.0.2") (deps (list (crate-dep (name "phf") (req "^0.7.23") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.1.0") (default-features #t) (kind 0)))) (hash "1x1v1rv2qamyyv82bh7yyxc8bi1arsxr25ls0ak41ncakl4f9l1l")))

(define-public crate-gh-emoji-1 (crate (name "gh-emoji") (vers "1.0.3") (deps (list (crate-dep (name "phf") (req "^0.8.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.3.5") (default-features #t) (kind 0)))) (hash "1g6wkpfyx7hidmp4xdwqkv3q9dg892v1vkz18hrma85lgq5haym1")))

(define-public crate-gh-emoji-1 (crate (name "gh-emoji") (vers "1.0.6") (deps (list (crate-dep (name "phf") (req "^0.10.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.5.4") (features (quote ("std"))) (kind 0)))) (hash "1pvskjla5wbv5apf6z5wj53rr90v71a583rpnfap37b7kb7kkbyn")))

(define-public crate-gh-emoji-1 (crate (name "gh-emoji") (vers "1.0.7") (deps (list (crate-dep (name "phf") (req "^0.11.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.6.0") (features (quote ("std"))) (kind 0)))) (hash "17krrnwzcc8azilccml9xjg5ksqn70ibmrar01f79ha87ns69b9h")))

(define-public crate-gh-emoji-1 (crate (name "gh-emoji") (vers "1.0.8") (deps (list (crate-dep (name "phf") (req "^0.11.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.6.0") (features (quote ("std"))) (kind 0)))) (hash "0dwc7kr1dsb4bn8n87x7363c2c6vc4pi9fy6s74nk0r8vxhxz2pb")))

