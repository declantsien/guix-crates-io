(define-module (crates-io gh _g) #:use-module (crates-io))

(define-public crate-gh_gql_schema-0.1 (crate (name "gh_gql_schema") (vers "0.1.0") (deps (list (crate-dep (name "cynic") (req "^3") (features (quote ("rkyv"))) (default-features #t) (kind 0)) (crate-dep (name "cynic-codegen") (req "^3") (default-features #t) (kind 1)) (crate-dep (name "serde") (req "^1.0") (default-features #t) (kind 0)))) (hash "0l86snglsvxqk5mk60q3rf0wr64xbzwc7x93ppa14jg5n0pgqk2a")))

(define-public crate-gh_gql_schema-0.1 (crate (name "gh_gql_schema") (vers "0.1.1") (deps (list (crate-dep (name "cynic") (req "^3") (features (quote ("rkyv"))) (default-features #t) (kind 0)) (crate-dep (name "cynic-codegen") (req "^3") (default-features #t) (kind 1)) (crate-dep (name "serde") (req "^1.0") (default-features #t) (kind 0)))) (hash "10xa2w24il1xrf9gb6s8j89i3gpxdi11haspck5jbwa3x31qqwna")))

(define-public crate-gh_gql_schema-0.2 (crate (name "gh_gql_schema") (vers "0.2.0") (deps (list (crate-dep (name "cynic") (req "^3") (features (quote ("rkyv"))) (default-features #t) (kind 0)) (crate-dep (name "cynic-codegen") (req "^3") (default-features #t) (kind 1)) (crate-dep (name "serde") (req "^1.0") (default-features #t) (kind 0)))) (hash "1fi4gqf749gfmijdj1ajn5k1ridda4brzxy7yxdw1fd69pjah02c")))

