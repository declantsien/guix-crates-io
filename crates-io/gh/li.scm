(define-module (crates-io gh li) #:use-module (crates-io))

(define-public crate-ghlink-0.2 (crate (name "ghlink") (vers "0.2.10") (deps (list (crate-dep (name "gix") (req "^0.62.0") (default-features #t) (kind 0)))) (hash "1jnlq3xikk9lkya91maga0qih14nmnjdlclfg0ih3bi5rizkbbav")))

(define-public crate-ghlink-0.2 (crate (name "ghlink") (vers "0.2.11") (deps (list (crate-dep (name "gix") (req "^0.62.0") (default-features #t) (kind 0)))) (hash "1358yqw5kwsin5m8a2hkykfq5b6c2z3mzhf9fc6philp7bdvpc3b")))

(define-public crate-ghlink-0.2 (crate (name "ghlink") (vers "0.2.12") (deps (list (crate-dep (name "gix") (req "^0.62.0") (default-features #t) (kind 0)))) (hash "0rphy8bvkfpi4fkfc5j7fzy5dq9npayfm5dnfg18bz742h8g1nvp")))

(define-public crate-ghlink-0.2 (crate (name "ghlink") (vers "0.2.13") (deps (list (crate-dep (name "gix") (req "^0.62.0") (default-features #t) (kind 0)))) (hash "1w94fxbnc7gqgmk96c3g4vn7xwf63c8km0qz6vkrl1fm8pjfwbp4")))

(define-public crate-ghlink-0.2 (crate (name "ghlink") (vers "0.2.14") (deps (list (crate-dep (name "gix") (req "^0.62.0") (default-features #t) (kind 0)))) (hash "1f7sb7jv80azl73rrnggvkvjkjdf373g3hmivprdp4l2id4v517f")))

(define-public crate-ghlink-0.2 (crate (name "ghlink") (vers "0.2.15") (deps (list (crate-dep (name "gix") (req "^0.62.0") (default-features #t) (kind 0)))) (hash "1xw7snlzs6iv1b2cf300ryb37hhf8wmy1lham76v9llaii9k06pz")))

(define-public crate-ghlink-0.2 (crate (name "ghlink") (vers "0.2.16") (deps (list (crate-dep (name "gix") (req "^0.62.0") (default-features #t) (kind 0)))) (hash "1ly1fzllj37q5hbn70fvwndrj99z1x2jk9yd5w8gl78j314c8qzb")))

(define-public crate-ghlink-0.2 (crate (name "ghlink") (vers "0.2.17") (deps (list (crate-dep (name "gix") (req "^0.62.0") (default-features #t) (kind 0)) (crate-dep (name "gix-testtools") (req "^0.13.0") (default-features #t) (kind 2)) (crate-dep (name "tempfile") (req "^3.10.1") (default-features #t) (kind 2)))) (hash "0nnin52dfhxspvcha1c6ynrdld592x869vrg746z7mdfp8x5addc")))

(define-public crate-ghlink-0.2 (crate (name "ghlink") (vers "0.2.18") (deps (list (crate-dep (name "gix") (req "^0.62.0") (default-features #t) (kind 0)) (crate-dep (name "gix-testtools") (req "^0.13.0") (default-features #t) (kind 2)) (crate-dep (name "regex") (req "^1.10.4") (default-features #t) (kind 2)) (crate-dep (name "serial_test") (req "^3.1.1") (default-features #t) (kind 2)) (crate-dep (name "tempfile") (req "^3.10.1") (default-features #t) (kind 2)))) (hash "1w2nr5n71ag0fbq1vw20n0q9n3fsybj973j4a6gyk38wljin6j8x")))

