(define-module (crates-io gh wo) #:use-module (crates-io))

(define-public crate-ghworkflow-rust-test-0.1 (crate (name "ghworkflow-rust-test") (vers "0.1.0") (hash "0hi0qmqs4a8mbfq4i4icqrl27fsndd0il7plgv7dbz8my2qqml7h")))

(define-public crate-ghworkflow-rust-test-0.1 (crate (name "ghworkflow-rust-test") (vers "0.1.1") (hash "1bnvn80h2rbzr0rqmbra5cxhyngf6sxbkwhmx6zmn7n0m9rgynlh")))

(define-public crate-ghworkflow-rust-test-0.1 (crate (name "ghworkflow-rust-test") (vers "0.1.2") (hash "1sr8vxk6wny5y1b42x9knyl2w7kxbhnfrahlfych17rk3mv41d5n")))

