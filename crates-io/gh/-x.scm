(define-module (crates-io gh -x) #:use-module (crates-io))

(define-public crate-gh-xplr-0.2 (crate (name "gh-xplr") (vers "0.2.1") (hash "03aqpzk933cj4n1a5b376xcf2h896kp432yn70y9fdvb6pjr5jax") (yanked #t)))

(define-public crate-gh-xplr-0.2 (crate (name "gh-xplr") (vers "0.2.2") (hash "0s4fw08wawg6yqm7iiicvgfxpw0sskxydd01gf2fl4fvygk798nd")))

