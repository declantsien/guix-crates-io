(define-module (crates-io gh ak) #:use-module (crates-io))

(define-public crate-ghakuf-0.2 (crate (name "ghakuf") (vers "0.2.1") (deps (list (crate-dep (name "byteorder") (req "^1.0.0") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.3") (default-features #t) (kind 0)))) (hash "15hk6q4d53f5vrnzwcdzbn8cisxs98k1slgg57jyyffij5psz6wp")))

(define-public crate-ghakuf-0.2 (crate (name "ghakuf") (vers "0.2.2") (deps (list (crate-dep (name "byteorder") (req "^1.0.0") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.3") (default-features #t) (kind 0)))) (hash "0ayj11w7mi5bgvqwrgc458sscd1a83xifpsfdjikr59dlywpky9p")))

(define-public crate-ghakuf-0.3 (crate (name "ghakuf") (vers "0.3.0") (deps (list (crate-dep (name "byteorder") (req "^1.1") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.3") (default-features #t) (kind 0)))) (hash "1gfgjjsls6fcr8h8m33ywvqj55lj84pji2bs04qg282rjxbrl59q")))

(define-public crate-ghakuf-0.3 (crate (name "ghakuf") (vers "0.3.1") (deps (list (crate-dep (name "byteorder") (req "^1.1") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.3") (default-features #t) (kind 0)))) (hash "0z58rcvld7z9lny1z73nkyximrfks33f23gkd45hyjidaz3m6dyb")))

(define-public crate-ghakuf-0.3 (crate (name "ghakuf") (vers "0.3.2") (deps (list (crate-dep (name "byteorder") (req "^1.1") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.3") (default-features #t) (kind 0)))) (hash "1nh4v812zlpdm7ld69hn1bwjfjp122sd3qxa7gjsbvc8nby3mfb4")))

(define-public crate-ghakuf-0.4 (crate (name "ghakuf") (vers "0.4.0") (deps (list (crate-dep (name "byteorder") (req "^1.1") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.3") (default-features #t) (kind 0)))) (hash "0wakm8gi6i84a6vfigp926grabyf069cmhfkq6gccarn811iffwm")))

(define-public crate-ghakuf-0.5 (crate (name "ghakuf") (vers "0.5.0") (deps (list (crate-dep (name "byteorder") (req "^1.1") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.3") (default-features #t) (kind 0)))) (hash "0hg00m2r4z9nsgcgykydlvrna71f9ijb723xwj3sf9l1x5fwhfhx")))

(define-public crate-ghakuf-0.5 (crate (name "ghakuf") (vers "0.5.1") (deps (list (crate-dep (name "byteorder") (req "^1.1") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.3") (default-features #t) (kind 0)))) (hash "1xb24dskf7gjq8c0zwmiyqccd2rs601x0c3llsp2b3nba9swds8k")))

(define-public crate-ghakuf-0.5 (crate (name "ghakuf") (vers "0.5.2") (deps (list (crate-dep (name "byteorder") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)))) (hash "0csx8drvbmarg4asys27qbzk9zz22x43i9qb26dzvxscyfz394cz")))

(define-public crate-ghakuf-0.5 (crate (name "ghakuf") (vers "0.5.3") (deps (list (crate-dep (name "byteorder") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)))) (hash "1b260m39slmm1f3d7dy1m17z0zyysg7mbql6azh8ksxyx92yqik9")))

(define-public crate-ghakuf-0.5 (crate (name "ghakuf") (vers "0.5.4") (deps (list (crate-dep (name "byteorder") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)))) (hash "16h355xac7xaxdh6lm6jbaypf7icxiyd0s2cw3ip213w9aji7apq")))

(define-public crate-ghakuf-0.5 (crate (name "ghakuf") (vers "0.5.5") (deps (list (crate-dep (name "byteorder") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)))) (hash "0grrwb53fmwivpz682mmaggai3ramkdfddkv63i75ka65ar1vahx")))

(define-public crate-ghakuf-0.5 (crate (name "ghakuf") (vers "0.5.6") (deps (list (crate-dep (name "byteorder") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)))) (hash "1x33mpfj6m6y0rp95n77zly26128by7rh5yd2ap380j42570hh11")))

