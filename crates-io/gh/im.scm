(define-module (crates-io gh im) #:use-module (crates-io))

(define-public crate-ghimlink-0.1 (crate (name "ghimlink") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^2.31.2") (default-features #t) (kind 0)) (crate-dep (name "git2") (req "^0.7.1") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.0.0") (default-features #t) (kind 0)) (crate-dep (name "text_io") (req "^0.1.7") (default-features #t) (kind 0)))) (hash "0wcq5ykgsg8gq4bgpfy5ij9nnm217644bnr9lml6v43r7p8jky99")))

(define-public crate-ghimlink-0.2 (crate (name "ghimlink") (vers "0.2.0") (deps (list (crate-dep (name "clap") (req "^2.33.3") (default-features #t) (kind 0)) (crate-dep (name "git2") (req "^0.13.12") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.4.1") (default-features #t) (kind 0)) (crate-dep (name "text_io") (req "^0.1.8") (default-features #t) (kind 0)))) (hash "17mr441rgkrpj3r8fyjj9mak352032ya4s41wz3ncjc1wzia83av")))

