(define-module (crates-io gh c-) #:use-module (crates-io))

(define-public crate-ghc-rts-rs-0.0.1 (crate (name "ghc-rts-rs") (vers "0.0.1") (deps (list (crate-dep (name "cabal-rs") (req "^0.0.1") (default-features #t) (kind 1)) (crate-dep (name "duct") (req "^0.9.2") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2.36") (default-features #t) (kind 0)) (crate-dep (name "walkdir") (req "^2.0.1") (default-features #t) (kind 1)))) (hash "032lqw9iyabxl7zhsc5izdda14z997gkdslizff7c1yfw6g56pwm") (yanked #t)))

