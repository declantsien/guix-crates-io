(define-module (crates-io gh et) #:use-module (crates-io))

(define-public crate-ghetto-lock-0.1 (crate (name "ghetto-lock") (vers "0.1.0") (deps (list (crate-dep (name "log") (req "^0.4.8") (default-features #t) (kind 0)) (crate-dep (name "memcache") (req "^0.13.1") (default-features #t) (kind 0)))) (hash "0wbkr5m6r4dv7rr7v32n1kc8h9dxlb2rbcpmv572mgyjzp2z2gwa")))

(define-public crate-ghetto-lock-0.1 (crate (name "ghetto-lock") (vers "0.1.1") (deps (list (crate-dep (name "log") (req "^0.4.8") (default-features #t) (kind 0)) (crate-dep (name "memcache") (req "^0.13.1") (default-features #t) (kind 0)))) (hash "176yrrcf0w6cp8g1x5jhcwv273lf4c8lfsncgy6nm2vgs1cslv0b")))

(define-public crate-ghetto-lock-0.2 (crate (name "ghetto-lock") (vers "0.2.0") (deps (list (crate-dep (name "log") (req "^0.4.8") (default-features #t) (kind 0)) (crate-dep (name "memcache") (req "^0.14.0") (default-features #t) (kind 0)))) (hash "0mavf5dayxql282qc283fzb79xnld0lni62ys3v5i7bf4pyi8s93")))

(define-public crate-ghetto-lock-0.2 (crate (name "ghetto-lock") (vers "0.2.1") (deps (list (crate-dep (name "log") (req "^0.4.8") (default-features #t) (kind 0)) (crate-dep (name "memcache") (req "^0.14.0") (default-features #t) (kind 0)))) (hash "1cqkgixif2gbw37ibxqydypl1bq6xb1gz5q08aaq6ky6l9b1m6ci")))

