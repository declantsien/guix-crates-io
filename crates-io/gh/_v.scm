(define-module (crates-io gh _v) #:use-module (crates-io))

(define-public crate-gh_vm-0.0.1 (crate (name "gh_vm") (vers "0.0.1-alpha") (deps (list (crate-dep (name "getopts") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "0.1.*") (default-features #t) (kind 0)) (crate-dep (name "yaml-rust") (req "^0.2.2") (default-features #t) (kind 0)))) (hash "0giysw0f9614356sfz7mdaxjrfmb9bjmb86a41qy5j80bik52wwd") (yanked #t)))

