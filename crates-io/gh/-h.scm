(define-module (crates-io gh -h) #:use-module (crates-io))

(define-public crate-gh-hex-0.0.0 (crate (name "gh-hex") (vers "0.0.0") (deps (list (crate-dep (name "pager") (req "^0.16.0") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.4") (default-features #t) (kind 0)) (crate-dep (name "termion") (req "^1.5.6") (default-features #t) (kind 0)))) (hash "1rpg4fz22nw3jl19w3gvx8pp1nh80ahpgx9ilwj1ah0ri81wdl44")))

