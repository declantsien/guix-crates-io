(define-module (crates-io gh st) #:use-module (crates-io))

(define-public crate-ghstats-0.1 (crate (name "ghstats") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^4.4.6") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "graphql_client") (req "^0.13.0") (features (quote ("reqwest-blocking"))) (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.11.22") (features (quote ("blocking" "json"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.189") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.107") (default-features #t) (kind 0)) (crate-dep (name "upon") (req "^0.7.1") (default-features #t) (kind 0)))) (hash "0zr6fcmk06v9iba7sknfjnsyhm4kax4fl4ryll2y10alavc8llj5")))

