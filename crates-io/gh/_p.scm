(define-module (crates-io gh _p) #:use-module (crates-io))

(define-public crate-gh_page_tool-0.1 (crate (name "gh_page_tool") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^4.4.0") (default-features #t) (kind 0)) (crate-dep (name "fs_extra") (req "^1.2.0") (default-features #t) (kind 0)) (crate-dep (name "tempfile") (req "^3.2.0") (default-features #t) (kind 0)))) (hash "1dwnzr891jg38nlz4l81gibk6kifg3vm0l6pa3db1jz05r4jkbjz")))

(define-public crate-gh_page_tool-0.2 (crate (name "gh_page_tool") (vers "0.2.0") (deps (list (crate-dep (name "clap") (req "^4.4.0") (default-features #t) (kind 0)) (crate-dep (name "fs_extra") (req "^1.2.0") (default-features #t) (kind 0)) (crate-dep (name "tempfile") (req "^3.2.0") (default-features #t) (kind 0)))) (hash "0071z32iki1pwfym97c108xf3mwjzrg1ihdb277l3cgin3wnqrrp")))

(define-public crate-gh_page_tool-0.4 (crate (name "gh_page_tool") (vers "0.4.0") (deps (list (crate-dep (name "clap") (req "^4.4.0") (default-features #t) (kind 0)) (crate-dep (name "fs_extra") (req "^1.2.0") (default-features #t) (kind 0)))) (hash "1sy5zr0cs34q7asjk72kn181h9gbhqgv5m2k8rscb93bj0i0jg3d")))

