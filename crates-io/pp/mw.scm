(define-module (crates-io pp mw) #:use-module (crates-io))

(define-public crate-ppmwriter-0.1 (crate (name "ppmwriter") (vers "0.1.0") (hash "1dgrvpksdfxl9why56naj4m938g3s70mpd1sqb8dprh97ypdickl") (yanked #t)))

(define-public crate-ppmwriter-0.1 (crate (name "ppmwriter") (vers "0.1.1") (hash "080lf3pix0cliixspf2vdn070fphmdz80z0h3wis4l41a91l4d49") (yanked #t)))

(define-public crate-ppmwriter-0.1 (crate (name "ppmwriter") (vers "0.1.2") (hash "1izdni7zklz3ycfd2mmhjs7xhpp9f533mx9504a3i3qs70fby3ji") (yanked #t)))

