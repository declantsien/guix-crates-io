(define-module (crates-io pp -r) #:use-module (crates-io))

(define-public crate-pp-rs-0.1 (crate (name "pp-rs") (vers "0.1.0") (hash "0v8cj7rpgl6d2zr1dlayc002cd2782820khar5lw8c5a9rf3g9v6")))

(define-public crate-pp-rs-0.2 (crate (name "pp-rs") (vers "0.2.1") (deps (list (crate-dep (name "unicode-xid") (req "^0.2") (default-features #t) (kind 0)))) (hash "1vkd9lgwf5rxy7qgzl8mka7vnghaq6nnn0nmg7mycl72ysvqnidv")))

