(define-module (crates-io pp ot) #:use-module (crates-io))

(define-public crate-ppot-rs-0.1 (crate (name "ppot-rs") (vers "0.1.0") (deps (list (crate-dep (name "ark-bn254") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "ark-ff") (req "^0.3.0") (kind 0)) (crate-dep (name "byteorder") (req "^1.4.3") (default-features #t) (kind 0)) (crate-dep (name "hex") (req "^0.4.3") (default-features #t) (kind 2)))) (hash "0arv6gk4cksi9xf2j2v5f04n374yh4ccz2013fgfbhw1ymdxwkr7")))

(define-public crate-ppot-rs-0.1 (crate (name "ppot-rs") (vers "0.1.1") (deps (list (crate-dep (name "ark-bn254") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "ark-ff") (req "^0.3.0") (kind 0)) (crate-dep (name "byteorder") (req "^1.4.3") (default-features #t) (kind 0)) (crate-dep (name "hex") (req "^0.4.3") (default-features #t) (kind 2)))) (hash "106p848nwgbw3jza56adqr9zc7ka3bhykvcawx7aqqkrfvaqm07m")))

