(define-module (crates-io pp et) #:use-module (crates-io))

(define-public crate-ppeterson_crate_a-1 (crate (name "ppeterson_crate_a") (vers "1.0.0") (hash "1g3jpdfk272v23z67gnbj9jcxkcgqgqcmlkydlfw9pllkrr73yjq") (yanked #t)))

(define-public crate-ppeterson_crate_a-2 (crate (name "ppeterson_crate_a") (vers "2.0.0") (hash "12a538rmq69v3wk94f3jb664wqf5gy3il38790n50wp1fq6d7h33") (yanked #t)))

(define-public crate-ppeterson_crate_a-0.3 (crate (name "ppeterson_crate_a") (vers "0.3.0") (hash "161ik5zfmc4rxh37x0x15a8frb1ari4z70v7wv4rk83lgwq5f9p9")))

(define-public crate-ppeterson_crate_a-0.4 (crate (name "ppeterson_crate_a") (vers "0.4.0") (hash "0lnr6v965za1l04vwn763kmmwqa831l1m78mg6k1lw176sii2bdh")))

(define-public crate-ppeterson_crate_a-0.5 (crate (name "ppeterson_crate_a") (vers "0.5.0") (hash "08177slgkmnwviwi1skd38cc71rcd1jxw263w8jzj6sf8xyqj2sq")))

(define-public crate-ppeterson_crate_b-0.1 (crate (name "ppeterson_crate_b") (vers "0.1.0") (deps (list (crate-dep (name "ppeterson_crate_a") (req "^1.0.0") (default-features #t) (kind 0)))) (hash "1yw3c7x3q9kswk19mnfz1wdynym866hm6g86zy35dikwsrvkajw8") (yanked #t)))

(define-public crate-ppeterson_crate_b-0.2 (crate (name "ppeterson_crate_b") (vers "0.2.0") (deps (list (crate-dep (name "ppeterson_crate_a") (req "^1.0.0") (default-features #t) (kind 0)))) (hash "0q2yyf4fha6iawl0l0k4jldqn4kg1z8750az496ff72vr7fgvysm") (yanked #t)))

(define-public crate-ppeterson_crate_b-0.3 (crate (name "ppeterson_crate_b") (vers "0.3.0") (deps (list (crate-dep (name "ppeterson_crate_a") (req "^1.0.0") (default-features #t) (kind 0)))) (hash "1qrz51hy724sn19p8xrrhzf66b1s234p8hpsvjzxlqc91pnjsmzl") (yanked #t)))

(define-public crate-ppeterson_crate_b-0.8 (crate (name "ppeterson_crate_b") (vers "0.8.0") (hash "1rrxxjvpwg5kkcya0dzgfi533q043i5g5bm6hfg2496qr945cw40")))

(define-public crate-ppeterson_crate_c-0.1 (crate (name "ppeterson_crate_c") (vers "0.1.0") (deps (list (crate-dep (name "ppeterson_crate_a") (req "^2.0.0") (default-features #t) (kind 0)))) (hash "1widiisrxbbraz3xj495wh8b1wy2jnajaygwcdvwr86ksha5yyly") (yanked #t)))

(define-public crate-ppeterson_crate_c-0.3 (crate (name "ppeterson_crate_c") (vers "0.3.0") (deps (list (crate-dep (name "ppeterson_crate_a") (req "^2.0.0") (default-features #t) (kind 0)))) (hash "0gbg0lxvx7z4fhd327qgxfsv3c07m1hf5gjxiypvyahsdkwvvfi5") (yanked #t)))

