(define-module (crates-io pp er) #:use-module (crates-io))

(define-public crate-pperf-0.1 (crate (name "pperf") (vers "0.1.0") (hash "0dsgcxsqzl0wv4qjvm48352zsz0gq3bx8cklq72chgs8mnaklijh")))

(define-public crate-pperf-0.1 (crate (name "pperf") (vers "0.1.1") (hash "0xpldahyyffh52jwjkc0w841kqrd9h1lgv1c9vk7qd6wwhlvwd6z")))

(define-public crate-pperf-0.1 (crate (name "pperf") (vers "0.1.2") (hash "15crfczmn7r8myyhvhjyicnz6yp4pjd9ixl0i2f7h0bi6kaziwm8")))

