(define-module (crates-io pp co) #:use-module (crates-io))

(define-public crate-ppcore-0.2 (crate (name "ppcore") (vers "0.2.2") (deps (list (crate-dep (name "lazy_static") (req "^1.4") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "myutil") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "nix") (req "^0.18") (default-features #t) (kind 0)) (crate-dep (name "parking_lot") (req "^0.11") (default-features #t) (kind 0)) (crate-dep (name "ppcore_def") (req "^0.2") (default-features #t) (kind 0)))) (hash "14b6f0w5y6c49lwk3yg5v5l4167hf2gijsaaf21zyf9ghr0s3p2a") (features (quote (("zfs_snapshot") ("testmock") ("default" "backing_file") ("backing_file"))))))

(define-public crate-ppcore_def-0.2 (crate (name "ppcore_def") (vers "0.2.2") (deps (list (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "12ir7mfbbc3m05ml32fhbfhhqq8plwjj2xkp1as8kj4dldws7lr9")))

