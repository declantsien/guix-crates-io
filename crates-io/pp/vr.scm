(define-module (crates-io pp vr) #:use-module (crates-io))

(define-public crate-ppvr-0.1 (crate (name "ppvr") (vers "0.1.0") (hash "1v2j7jbrp4n97njqh8jxfcxa6rask95kcjgg7w9fa16jgj8m0pb4") (rust-version "1.59")))

(define-public crate-ppvr-0.1 (crate (name "ppvr") (vers "0.1.1") (hash "0hn018r63s45mjj3812ga9psjlmcblfj5h0vqlqyislxdbphxqah") (rust-version "1.59")))

(define-public crate-ppvr-0.1 (crate (name "ppvr") (vers "0.1.2") (hash "0j784liyrycq0b5ldmc6zv3zfakn04dlpn785armja59z1d40lim") (rust-version "1.59")))

(define-public crate-ppvr-0.1 (crate (name "ppvr") (vers "0.1.3") (hash "026xqgm3zf6h6i6mayiamhzv2kk62gngzq26xg85ydsk0dmxqwf8") (rust-version "1.59")))

(define-public crate-ppvr-0.1 (crate (name "ppvr") (vers "0.1.4") (deps (list (crate-dep (name "clap") (req "^3.1.6") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "028bkjihbxd89c02fbn1waw1w7z3pzs8vrss3hdk3v4szymhi8lr") (rust-version "1.59")))

(define-public crate-ppvr-0.1 (crate (name "ppvr") (vers "0.1.5") (deps (list (crate-dep (name "clap") (req "^3.1.6") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "1rys7k8c1aipi1fbkvc81121pm4zvssifv1l33yva54scysc2780") (rust-version "1.59")))

(define-public crate-ppvr-0.2 (crate (name "ppvr") (vers "0.2.0") (deps (list (crate-dep (name "clap") (req "^3.1.6") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "0yprx706rnl9lclphvmdpyqbnzp1gd8nsa6jyk8sjapkkm81ldfy") (rust-version "1.59")))

(define-public crate-ppvr-0.2 (crate (name "ppvr") (vers "0.2.1") (deps (list (crate-dep (name "clap") (req "^3.1.6") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "0rihkf5cbcv873bmpxhmffl2dlb738w9w31jyvy1x0vk2dpys97l") (rust-version "1.59")))

