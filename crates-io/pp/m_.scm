(define-module (crates-io pp m_) #:use-module (crates-io))

(define-public crate-ppm_decode-0.1 (crate (name "ppm_decode") (vers "0.1.0") (hash "03ywfqrvw2dshrlxrm4ryrpdlplsjjdm7iv52amj10nxpd0kc899")))

(define-public crate-ppm_decode-0.1 (crate (name "ppm_decode") (vers "0.1.1") (hash "1fggmq97yv4chgdng7wj2qhlqwlnsyajkf4bk99xxy4374fdkl6x")))

(define-public crate-ppm_decode-0.1 (crate (name "ppm_decode") (vers "0.1.2") (hash "0xwr55wlnbfznl33ixwnbgar9yxw0hpan047i4834ci3llq4v51i")))

(define-public crate-ppm_decode-0.1 (crate (name "ppm_decode") (vers "0.1.3") (hash "1mvmgvxjjinapsxd7hrnscshjnk9rph08zcyckizicj8dj1dzs6n")))

(define-public crate-ppm_steganography-0.1 (crate (name "ppm_steganography") (vers "0.1.0") (deps (list (crate-dep (name "image") (req "^0.24.3") (features (quote ("pnm"))) (default-features #t) (kind 0)))) (hash "09875zgv7p2z796rza25csjy38dql8a2knavw447i06lzylvpmh0")))

