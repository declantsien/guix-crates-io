(define-module (crates-io pp rt) #:use-module (crates-io))

(define-public crate-pprtmp-0.1 (crate (name "pprtmp") (vers "0.1.0") (deps (list (crate-dep (name "anyhow") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^4.1.4") (default-features #t) (kind 0)) (crate-dep (name "env_logger") (req "^0.10.0") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.0") (default-features #t) (kind 0)) (crate-dep (name "rtmp") (req "^0.4.0") (default-features #t) (kind 0)) (crate-dep (name "streamhub") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.26.0") (features (quote ("full"))) (kind 0)))) (hash "091l0slwgf1k0nkywjlj7d7q1abra1dkm6z3w3df7xxf5wkj43jq")))

