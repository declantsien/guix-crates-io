(define-module (crates-io pp at) #:use-module (crates-io))

(define-public crate-ppatch-0.1 (crate (name "ppatch") (vers "0.1.0") (deps (list (crate-dep (name "num") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "snafu") (req "^0.6.0") (default-features #t) (kind 0)))) (hash "0z35mv5jgc9x70wa4f68h1bwfx9cr16xvbrks9z7qzdaaxfbvdm3")))

(define-public crate-ppatcher-0.1 (crate (name "ppatcher") (vers "0.1.0") (deps (list (crate-dep (name "ppatch") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.3") (default-features #t) (kind 0)))) (hash "054cn45hgdg90906pk33im9nrg1aqbdf6z5pmp8m4whl0a98wh38")))

