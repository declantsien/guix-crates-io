(define-module (crates-io pp o-) #:use-module (crates-io))

(define-public crate-ppo-rs-0.1 (crate (name "ppo-rs") (vers "0.1.0") (deps (list (crate-dep (name "ema-rs") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "ta-common") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "1q6vw6vq1mkv25xaijszji418hrr0scdhkvz1y7zac0sy9n45y3i")))

