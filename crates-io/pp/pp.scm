(define-module (crates-io pp pp) #:use-module (crates-io))

(define-public crate-pppp-0.1 (crate (name "pppp") (vers "0.1.2") (hash "16yfwv0w8ck5n948zc5rwnvwmdy7fb2iq2cvf4qs21gdcmb183jv")))

(define-public crate-pppp-0.1 (crate (name "pppp") (vers "0.1.3") (hash "1v6zp58lfni73fi6kgy0rvagin51jzlrimih3s0y632lb69v5rm2")))

(define-public crate-ppppp-0.1 (crate (name "ppppp") (vers "0.1.0") (hash "19pyg8p2rynxj9l4l92ci6zk2gxd6h99f2gb13mz1zmm1c2fyww6")))

(define-public crate-ppppp-0.1 (crate (name "ppppp") (vers "0.1.1") (deps (list (crate-dep (name "ppppp") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "1z4hgfiavcs5fi1q63nsb036s9mvhl64q87gyr1hrv7lazap0jdl")))

(define-public crate-ppppp-0.1 (crate (name "ppppp") (vers "0.1.3") (deps (list (crate-dep (name "ppppp") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "0m0zvcivyqrzwmxqym0jrx53b4pivni9jwbcbxz3shys6cp8xh3j")))

(define-public crate-ppppp-0.1 (crate (name "ppppp") (vers "0.1.4") (hash "12j4ngf7i81al2zsbfv9f91jcxz0ai62d98fgprvf3lwkhbx09jl")))

(define-public crate-pppppp-0.0.0 (crate (name "pppppp") (vers "0.0.0") (hash "0v5rvlisg9sq0liv9rdv7x5qhimcvrf59giwakby6vf6v0354r52")))

