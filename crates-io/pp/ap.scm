(define-module (crates-io pp ap) #:use-module (crates-io))

(define-public crate-ppapi-0.0.0 (crate (name "ppapi") (vers "0.0.0") (hash "14fsp1ima8k0wh4b35wjq6h2pdnphpparc6rsqmb0nr2y402d8zn") (yanked #t)))

(define-public crate-ppapi-0.1 (crate (name "ppapi") (vers "0.1.0") (deps (list (crate-dep (name "hyper") (req "*") (default-features #t) (kind 0)) (crate-dep (name "log") (req "*") (default-features #t) (kind 0)) (crate-dep (name "openssl") (req "*") (default-features #t) (kind 0)) (crate-dep (name "pnacl-build-helper") (req "*") (default-features #t) (kind 1)))) (hash "1gi66a14dcjjis9cri2dwmbgxxhihp5nam30wrdzyp4dvpiw86bz")))

(define-public crate-ppapi-0.1 (crate (name "ppapi") (vers "0.1.1") (deps (list (crate-dep (name "hyper") (req "*") (default-features #t) (kind 0)) (crate-dep (name "log") (req "*") (default-features #t) (kind 0)) (crate-dep (name "openssl") (req "*") (default-features #t) (kind 0)) (crate-dep (name "pnacl-build-helper") (req "*") (default-features #t) (kind 1)))) (hash "17j53s38h8mdwkv5g8hnr1139q5nw3y9vqavrl9sdvib4pn1m88i")))

(define-public crate-ppapi-0.1 (crate (name "ppapi") (vers "0.1.2") (deps (list (crate-dep (name "hyper") (req "*") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.1.6") (default-features #t) (kind 0)) (crate-dep (name "log") (req "*") (default-features #t) (kind 0)) (crate-dep (name "openssl") (req "*") (default-features #t) (kind 0)) (crate-dep (name "pnacl-build-helper") (req "*") (default-features #t) (kind 1)) (crate-dep (name "url") (req "*") (default-features #t) (kind 0)))) (hash "0r00xgsn0pgi79c8xyqj10ngp7780pmpdr0psvdqq8r91x4fngjz") (features (quote (("pepper") ("default" "pepper"))))))

(define-public crate-ppapi-tester-0.0.0 (crate (name "ppapi-tester") (vers "0.0.0") (hash "1vrhky7qnjg0fzm3lnzpxh7wdg5s85drygvp6z3f8dbpnn817z4k") (yanked #t)))

(define-public crate-ppapi-tester-0.0.1 (crate (name "ppapi-tester") (vers "0.0.1") (hash "16bh6l75qg3d8yv8l60mqk6jv1c6vjx0jckvnd2h9mr48wbw92vv")))

