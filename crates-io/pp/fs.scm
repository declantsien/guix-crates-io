(define-module (crates-io pp fs) #:use-module (crates-io))

(define-public crate-ppfs-0.1 (crate (name "ppfs") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^3.1.12") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "rocket") (req "^0.4.10") (default-features #t) (kind 0)) (crate-dep (name "rocket_contrib") (req "^0.4.10") (features (quote ("serve"))) (kind 0)))) (hash "03cvijcsm6n11kyh59dnzcijzp94czk6ff8pp7h9rq394p5px5js")))

(define-public crate-ppfs-0.1 (crate (name "ppfs") (vers "0.1.1") (deps (list (crate-dep (name "clap") (req "^3.1.12") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "rocket") (req "^0.4.10") (default-features #t) (kind 0)) (crate-dep (name "rocket_contrib") (req "^0.4.10") (features (quote ("serve"))) (kind 0)))) (hash "0q7hrz7h5jnj0yfkaczlxmgw5lpjd5da4n4bpa37gm20s4jpxn0l")))

(define-public crate-ppfs-0.1 (crate (name "ppfs") (vers "0.1.2") (deps (list (crate-dep (name "clap") (req "^3.1.12") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "rocket") (req "^0.4.10") (default-features #t) (kind 0)) (crate-dep (name "rocket_contrib") (req "^0.4.10") (features (quote ("serve"))) (kind 0)))) (hash "0alwm5n5sy142r5wv51bd6r3zmq7j0qs03h5laj7hg921h5m2rqn")))

(define-public crate-ppfs-0.1 (crate (name "ppfs") (vers "0.1.3") (deps (list (crate-dep (name "clap") (req "^3.1.12") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "rocket") (req "^0.4.10") (default-features #t) (kind 0)) (crate-dep (name "rocket_contrib") (req "^0.4.10") (features (quote ("serve"))) (kind 0)))) (hash "0wdjrkwyqbid7kiwj41c4jk5dyaj9aj8fvj5njpdb04144fn1703")))

