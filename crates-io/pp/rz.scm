(define-module (crates-io pp rz) #:use-module (crates-io))

(define-public crate-pprzlink-0.1 (crate (name "pprzlink") (vers "0.1.0") (hash "14m2vfq2psv3js1wr29g1jcmmjizscqvjd4xx6yn9x8xrbjiwz5r")))

(define-public crate-pprzlink-0.2 (crate (name "pprzlink") (vers "0.2.0") (deps (list (crate-dep (name "byteorder") (req "^1.1.0") (default-features #t) (kind 0)) (crate-dep (name "ivyrust") (req "^0.1.1") (default-features #t) (kind 0)) (crate-dep (name "xml-rs") (req "^0.6") (default-features #t) (kind 0)))) (hash "06mnj1vac92pdwvlh2l6va95fj6mifayhychvxn1x056nzswbpjj")))

(define-public crate-pprzlink-0.3 (crate (name "pprzlink") (vers "0.3.0") (deps (list (crate-dep (name "byteorder") (req "^1.1.0") (default-features #t) (kind 0)) (crate-dep (name "ivyrust") (req "^0.1.1") (default-features #t) (kind 0)) (crate-dep (name "xml-rs") (req "^0.6") (default-features #t) (kind 0)))) (hash "0nmr9cn411d35c6851rq8j2k1x1r7dj3pzfqvah4pylrap7p2z09")))

(define-public crate-pprzlink-0.3 (crate (name "pprzlink") (vers "0.3.1") (deps (list (crate-dep (name "byteorder") (req "^1.1.0") (default-features #t) (kind 0)) (crate-dep (name "ivyrust") (req "^0.1.1") (default-features #t) (kind 0)) (crate-dep (name "xml-rs") (req "^0.6") (default-features #t) (kind 0)))) (hash "1wrl6h3bfz12k4dlb00y7sz5vkjg9yd01wqdragk5fpgg9dzwq0y")))

