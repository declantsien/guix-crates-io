(define-module (crates-io pp ri) #:use-module (crates-io))

(define-public crate-pprint-0.1 (crate (name "pprint") (vers "0.1.0") (deps (list (crate-dep (name "pprint_derive") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.7.3") (default-features #t) (kind 0)))) (hash "07r8ail9ylznanwr5vwray7rx8dcvwnzsbcx780gjf98c3042g41")))

(define-public crate-pprint-0.2 (crate (name "pprint") (vers "0.2.0") (deps (list (crate-dep (name "pprint_derive") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.9.3") (default-features #t) (kind 0)))) (hash "1cl97qsggsh5zw5r6h0zvldxi8iwlipq86w5q4h86nqydwy15pxc")))

(define-public crate-pprint-0.2 (crate (name "pprint") (vers "0.2.1") (deps (list (crate-dep (name "pprint_derive") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.9.3") (default-features #t) (kind 0)))) (hash "0cs246bxrhipk1lb6v7il42hbakdrkcqfp4a8a6blvayw6rxbhm4")))

(define-public crate-pprint_derive-0.1 (crate (name "pprint_derive") (vers "0.1.0") (deps (list (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (default-features #t) (kind 0)))) (hash "07sl68xa3yp1ay6h1npzjg8d86wvywgr9h537bl3mb5gjjkw1bsv")))

