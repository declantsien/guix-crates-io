(define-module (crates-io pp v-) #:use-module (crates-io))

(define-public crate-ppv-lite86-0.1 (crate (name "ppv-lite86") (vers "0.1.1") (deps (list (crate-dep (name "crypto-simd") (req "^0.1") (default-features #t) (kind 0)))) (hash "1b2r7bsn272v6y3ag8874582hin9n9hnpz2gkxhxw2d4lyhag0f2")))

(define-public crate-ppv-lite86-0.1 (crate (name "ppv-lite86") (vers "0.1.2") (deps (list (crate-dep (name "crypto-simd") (req "^0.1") (default-features #t) (kind 0)))) (hash "1lh2nr0gr1g8fmwsg531xcg6hnnwyg08zk1ir2iydbkwwi8cywz1")))

(define-public crate-ppv-lite86-0.2 (crate (name "ppv-lite86") (vers "0.2.0") (hash "0j03hsyawkzbcllgxhaki7gi3sm4pkcd6my5vn4rdhv50pgrkdyy") (features (quote (("std") ("default" "std"))))))

(define-public crate-ppv-lite86-0.2 (crate (name "ppv-lite86") (vers "0.2.1") (hash "134m6pd3dw3j5ksi8p1rmh6jrac7ixqjl2nn80fmcqqfh7sjd142") (features (quote (("std") ("simd") ("default" "std" "simd")))) (yanked #t)))

(define-public crate-ppv-lite86-0.2 (crate (name "ppv-lite86") (vers "0.2.2") (hash "1jkbqxd7ckkjaghpc3smrpw0291lj9df5gki24ssilbs6kk3f77y") (features (quote (("std") ("simd") ("default" "std" "simd"))))))

(define-public crate-ppv-lite86-0.2 (crate (name "ppv-lite86") (vers "0.2.3") (hash "0xvpb2bzql00cj77380rg5s1q4jypfllijm3bh98a28l4awwgj3g") (features (quote (("std") ("simd") ("default" "std" "simd"))))))

(define-public crate-ppv-lite86-0.2 (crate (name "ppv-lite86") (vers "0.2.4") (hash "143j7hz0a2vwyr746i7lmn5k6grzvxchb8rsks6i3c21hd2aazjq") (features (quote (("std") ("simd") ("default" "std" "simd"))))))

(define-public crate-ppv-lite86-0.2 (crate (name "ppv-lite86") (vers "0.2.5") (hash "06snnv338w341nicfqba2jgln5dsla72ndkgrw7h1dfdb3vgkjz3") (features (quote (("std") ("simd") ("default" "std" "simd"))))))

(define-public crate-ppv-lite86-0.2 (crate (name "ppv-lite86") (vers "0.2.6") (hash "06zs492wbms7j5qhy58cs3976c7kyc47rx0d6fn63rgvp580njbl") (features (quote (("std") ("simd") ("default" "std" "simd"))))))

(define-public crate-ppv-lite86-0.2 (crate (name "ppv-lite86") (vers "0.2.7") (hash "1w72wnbfx88w86h955gc8fa8dz4gr5h5kzij1v7cgi5yjqydzr6i") (features (quote (("std") ("simd") ("default" "std" "simd"))))))

(define-public crate-ppv-lite86-0.2 (crate (name "ppv-lite86") (vers "0.2.8") (hash "1shj4q7jwj0azssr8cg51dk3kh7d4lg9rmbbz1kbqk971vc5wyi3") (features (quote (("std") ("simd") ("no_simd") ("default" "std"))))))

(define-public crate-ppv-lite86-0.2 (crate (name "ppv-lite86") (vers "0.2.9") (hash "080sa1pllwljxyl3i5b1i7746sh1s16m8lmn6fkn4p0z253sjvy3") (features (quote (("std") ("simd") ("no_simd") ("default" "std"))))))

(define-public crate-ppv-lite86-0.2 (crate (name "ppv-lite86") (vers "0.2.10") (hash "0ms8198kclg4h96ggbziixxmsdl847s648kmbx11zlmjsqjccx5c") (features (quote (("std") ("simd") ("no_simd") ("default" "std"))))))

(define-public crate-ppv-lite86-0.2 (crate (name "ppv-lite86") (vers "0.2.11") (hash "1m4zyfm48rc4qlq8kinvdsn60qya2y0wwz465mflk9jqsjdgc2gj") (features (quote (("std") ("simd") ("no_simd") ("default" "std")))) (yanked #t)))

(define-public crate-ppv-lite86-0.2 (crate (name "ppv-lite86") (vers "0.2.12") (hash "1h0lgd7g75rwz8gh8r47n06xgqjxa6z522hsx4lg1my8i850r8la") (features (quote (("std") ("simd") ("no_simd") ("default" "std")))) (yanked #t)))

(define-public crate-ppv-lite86-0.2 (crate (name "ppv-lite86") (vers "0.2.13") (hash "18n33pk7i6l1iimr12i8b7f882cq7dl3fp86aa0i7x5iclbqhdd3") (features (quote (("std") ("simd") ("no_simd") ("default" "std")))) (yanked #t)))

(define-public crate-ppv-lite86-0.2 (crate (name "ppv-lite86") (vers "0.2.14") (hash "0hdp3m8f1d0rlfmlf7n2a5ndzjh2ybpc816d2npg97qjs0dh3jn3") (features (quote (("std") ("simd") ("no_simd") ("default" "std"))))))

(define-public crate-ppv-lite86-0.2 (crate (name "ppv-lite86") (vers "0.2.15") (hash "1fimwnyyh3wx33r5s77lw5g5vcxhw1p5j60pdvbbwr8l374gn37d") (features (quote (("std") ("simd") ("no_simd") ("default" "std"))))))

(define-public crate-ppv-lite86-0.2 (crate (name "ppv-lite86") (vers "0.2.16") (hash "0wkqwnvnfcgqlrahphl45vdlgi2f1bs7nqcsalsllp1y4dp9x7zb") (features (quote (("std") ("simd") ("no_simd") ("default" "std"))))))

(define-public crate-ppv-lite86-0.2 (crate (name "ppv-lite86") (vers "0.2.17") (hash "1pp6g52aw970adv3x2310n7glqnji96z0a9wiamzw89ibf0ayh2v") (features (quote (("std") ("simd") ("no_simd") ("default" "std"))))))

(define-public crate-ppv-null-0.1 (crate (name "ppv-null") (vers "0.1.1") (deps (list (crate-dep (name "crypto-simd") (req "^0.1") (default-features #t) (kind 0)))) (hash "1b9c96x72c0bwsm1nl1x7vsv9jgcyvrx4amr92lgf2wxak78smy0")))

(define-public crate-ppv-null-0.1 (crate (name "ppv-null") (vers "0.1.2") (deps (list (crate-dep (name "crypto-simd") (req "^0.1") (default-features #t) (kind 0)))) (hash "07j2bx3m1k7dbrwjh8qnlmbvdwx81q6z96wmwmxp4q605crx6vsm")))

