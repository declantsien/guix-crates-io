(define-module (crates-io pp c7) #:use-module (crates-io))

(define-public crate-ppc750cl-0.1 (crate (name "ppc750cl") (vers "0.1.0") (deps (list (crate-dep (name "num-traits") (req "^0.2.14") (default-features #t) (kind 0)) (crate-dep (name "ppc750cl-macros") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "112rmg8wsncagq0ahbqbx2b8fq70d25sd2sb0vz4brbf53avccia")))

(define-public crate-ppc750cl-fuzz-0.1 (crate (name "ppc750cl-fuzz") (vers "0.1.0") (deps (list (crate-dep (name "num_cpus") (req "^1.13") (default-features #t) (kind 0)) (crate-dep (name "ppc750cl") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "16ikvxpwr2jr2aswih56fnwgriab7vs7g8a19n45rx0cpg2xmgwd") (yanked #t)))

(define-public crate-ppc750cl-macros-0.1 (crate (name "ppc750cl-macros") (vers "0.1.0") (deps (list (crate-dep (name "proc-macro2") (req "^1.0.28") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.74") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "0m4hf2lv1w03w2hzll8rn5xncvx8j8m7dipr6sqdy80wain5lxyb")))

(define-public crate-ppc750cl-rand-0.1 (crate (name "ppc750cl-rand") (vers "0.1.0") (deps (list (crate-dep (name "ppc750cl") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "rand_core") (req "^0.5") (default-features #t) (kind 0)) (crate-dep (name "sfmt") (req "^0.6") (default-features #t) (kind 0)))) (hash "0pasq7rm8f3bjlj4jm4w989m0qm9rpgc3cl55x3jnhkq08zzw9fs") (yanked #t)))

