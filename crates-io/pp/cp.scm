(define-module (crates-io pp cp) #:use-module (crates-io))

(define-public crate-ppcp-0.1 (crate (name "ppcp") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^2.32.9") (default-features #t) (kind 0)) (crate-dep (name "failure") (req "^0.1.5") (default-features #t) (kind 0)) (crate-dep (name "indicatif") (req "^0.15.0") (default-features #t) (kind 0)) (crate-dep (name "path_abs") (req "^0.5.0") (default-features #t) (kind 0)) (crate-dep (name "pathdiff") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "walkdir") (req "^2.2.7") (default-features #t) (kind 0)))) (hash "1s2cy00lm0ac1vas7sd0qggb9hhz4cd1ymnp7xnxpr27fx4mzmhn")))

