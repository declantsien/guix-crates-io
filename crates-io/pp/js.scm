(define-module (crates-io pp js) #:use-module (crates-io))

(define-public crate-ppjson-0.1 (crate (name "ppjson") (vers "0.1.0") (deps (list (crate-dep (name "colored") (req "^1.9.0") (default-features #t) (kind 0)) (crate-dep (name "json-color") (req "^0.7.1") (default-features #t) (kind 0)) (crate-dep (name "pager") (req "^0.14.0") (default-features #t) (kind 0)))) (hash "1jwr1frjna7zj14w1x06xw09j05nynvmb9mdysixxdzi4iyq3ja7")))

