(define-module (crates-io pp cl) #:use-module (crates-io))

(define-public crate-ppclient-0.2 (crate (name "ppclient") (vers "0.2.2") (deps (list (crate-dep (name "clap") (req "^2.33") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.4") (default-features #t) (kind 0)) (crate-dep (name "myutil") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "pprexec") (req "^0.2") (features (quote ("client"))) (kind 0)) (crate-dep (name "ppserver_def") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)))) (hash "1rhwaa6f4p46wl41xpyg84ddk9ws8jw9g5cyjj8n1dpkw4vwhp3d")))

