(define-module (crates-io pp t-) #:use-module (crates-io))

(define-public crate-ppt-kit-0.1 (crate (name "ppt-kit") (vers "0.1.0") (deps (list (crate-dep (name "quick-xml") (req "^0.30.0") (default-features #t) (kind 0)))) (hash "13rri9hk53pj5j3kw1xiwpam28ifw2zqkl3zvvhj6lf6lfm0asvs") (yanked #t) (rust-version "1.64.0")))

(define-public crate-ppt-kit-0.1 (crate (name "ppt-kit") (vers "0.1.1") (deps (list (crate-dep (name "quick-xml") (req "^0.30.0") (default-features #t) (kind 0)))) (hash "00r2x92lkvbbpck096w2dj9ac197w804s5g4bnbjh7qj07by05fv") (rust-version "1.64.0")))

(define-public crate-ppt-kit-0.1 (crate (name "ppt-kit") (vers "0.1.2") (deps (list (crate-dep (name "quick-xml") (req "^0.30.0") (default-features #t) (kind 0)))) (hash "18qqrskax3cch0bx8778cli6h4iq2a6q1dw4v3q2lxpb575i8cjg") (rust-version "1.64.0")))

