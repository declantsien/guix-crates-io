(define-module (crates-io kl ee) #:use-module (crates-io))

(define-public crate-klee-bindings-0.1 (crate (name "klee-bindings") (vers "0.1.0") (deps (list (crate-dep (name "bindgen") (req "^0.57.0") (default-features #t) (kind 1)) (crate-dep (name "cty") (req "^0.2.1") (default-features #t) (kind 0)))) (hash "1m1g3k4nmri98dk5s1g7cpqr0y6rrwn094qismwcjifgplqq45w2")))

(define-public crate-klee-rs-0.1 (crate (name "klee-rs") (vers "0.1.0") (deps (list (crate-dep (name "cty") (req "^0.2.1") (default-features #t) (kind 0)))) (hash "1wl1hllrc4lh29f8pcm8kszijnmcwximvhk3nlg02c5019kbfdf8")))

(define-public crate-klee-rs-0.1 (crate (name "klee-rs") (vers "0.1.1") (deps (list (crate-dep (name "cty") (req "^0.2.1") (default-features #t) (kind 0)))) (hash "14vlzcgg852b7sra1vcqlm8gfvda994r66r72a3nk09y038h17i3")))

