(define-module (crates-io kl o-) #:use-module (crates-io))

(define-public crate-klo-routines-0.1 (crate (name "klo-routines") (vers "0.1.0") (deps (list (crate-dep (name "libc") (req "^0.2.98") (default-features #t) (kind 0)))) (hash "0fmgn6k9186j3ckqnmlv9xaswqb4b6fcmrclqijipmjywx1cqy4y")))

