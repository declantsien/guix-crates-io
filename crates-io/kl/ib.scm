(define-module (crates-io kl ib) #:use-module (crates-io))

(define-public crate-klib-0.0.1 (crate (name "klib") (vers "0.0.1") (deps (list (crate-dep (name "winapi") (req "^0.3.9") (features (quote ("windef" "winerror" "winuser" "wincon" "wingdi" "minwindef" "dxgi" "dxgitype" "dxgiformat" "d3dcommon" "d3dcompiler" "d3d11" "d3d12" "d3dx10math"))) (default-features #t) (kind 0)))) (hash "1h37r1wk8arm8b4n73bsr76gki7854x4xpd8xkfdjimwghypkpbr")))

(define-public crate-klib-0.0.2 (crate (name "klib") (vers "0.0.2") (deps (list (crate-dep (name "winapi") (req "^0.3.9") (features (quote ("windef" "winerror" "winuser" "wincon" "wingdi" "minwindef" "dxgi" "dxgitype" "dxgiformat" "d3dcommon" "d3dcompiler" "d3d11" "d3d12" "d3dx10math"))) (default-features #t) (kind 0)))) (hash "0skcsv1kkrdyakvqss7wmwvn44d33i965ajfzqj1i5bw0gvxpq4v")))

(define-public crate-klib-0.0.3 (crate (name "klib") (vers "0.0.3") (deps (list (crate-dep (name "winapi") (req "^0.3.9") (features (quote ("windef" "winerror" "winuser" "wincon" "wingdi" "minwindef" "dxgi" "dxgitype" "dxgiformat" "d3dcommon" "d3dcompiler" "d3d11" "d3d12" "d3dx10math"))) (default-features #t) (kind 0)))) (hash "0n226hw8v04cxcsc9gsjn0xhnpd1x223m9sdvvzaf7qqb4n9xba7")))

(define-public crate-klibs-0.0.0 (crate (name "klibs") (vers "0.0.0") (hash "0gp67lp525h593priwqbjp1ybyf6w8wi36hblhl4mhk2b68pnizz")))

