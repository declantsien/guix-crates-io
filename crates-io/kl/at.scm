(define-module (crates-io kl at) #:use-module (crates-io))

(define-public crate-klata-0.1 (crate (name "klata") (vers "0.1.0") (deps (list (crate-dep (name "crossterm") (req "^0.23") (default-features #t) (kind 0)) (crate-dep (name "tui") (req "^0.17") (default-features #t) (kind 0)))) (hash "0wq75s4x8mb7mxkhc75zmmvazy2q3rbym0lz2mv8ibca5bl353rc")))

(define-public crate-klatsche-1 (crate (name "klatsche") (vers "1.0.0") (deps (list (crate-dep (name "colored") (req "^2.0.0") (default-features #t) (kind 0)))) (hash "00vlc41ysswmjdpagbavmy39wfx55nyhpqyg4nvhb3cnsmbizy0b")))

(define-public crate-klatsche-1 (crate (name "klatsche") (vers "1.0.1") (deps (list (crate-dep (name "colored") (req "^2.0.0") (default-features #t) (kind 0)))) (hash "03pq99xhkj5xk3hiiz5pvp7y8ap45jbvlr1finm983j3pn129a9q")))

