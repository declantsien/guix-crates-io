(define-module (crates-io kl u_) #:use-module (crates-io))

(define-public crate-klu_core-0.1 (crate (name "klu_core") (vers "0.1.0") (deps (list (crate-dep (name "image") (req "^0.22.3") (default-features #t) (kind 2)))) (hash "1bzf10ld6qz8b8aq6v383j6d56fgi7ya0kpgyknhbwbv68s63sja")))

(define-public crate-klu_core-0.2 (crate (name "klu_core") (vers "0.2.0") (hash "0zc7imcim3cg85il2ybbkfsvp7vdpnkp7wnb10arya310fcxqs29") (features (quote (("virtual_fs") ("all" "virtual_fs"))))))

(define-public crate-klu_core-0.2 (crate (name "klu_core") (vers "0.2.1") (hash "1jvpbm9f99yjsvwflj07r1kigvpmwih4fxg40x76z6d9gmz86j6b") (features (quote (("virtual_fs") ("all" "virtual_fs"))))))

(define-public crate-klu_sys-0.1 (crate (name "klu_sys") (vers "0.1.0") (deps (list (crate-dep (name "cc") (req "^1.0") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "1crw2r1xjv0cj7g5dplgv0wrbisnrjimnvjd8yk75vh17fhy7fgk") (features (quote (("dynamic") ("default")))) (links "klu")))

(define-public crate-klu_sys-0.2 (crate (name "klu_sys") (vers "0.2.0") (deps (list (crate-dep (name "cc") (req "^1.0") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "12d6ygs2v7ghrksanc0ydy62pplbnwfydxaq62nzjljw56f74zxw") (features (quote (("dynamic") ("default")))) (links "klu")))

