(define-module (crates-io kl en) #:use-module (crates-io))

(define-public crate-klend-0.1 (crate (name "klend") (vers "0.1.0") (deps (list (crate-dep (name "anchor-gen") (req "^0.3.1") (default-features #t) (kind 0)) (crate-dep (name "anchor-lang") (req ">=0.20") (default-features #t) (kind 0)))) (hash "0mnyj9175s6jh907x3p46rk446im2wj9r8m74g616n58kjfjxkgv") (features (quote (("no-log-ix-name") ("no-idl") ("no-entrypoint") ("default" "cpi") ("cpi" "no-entrypoint"))))))

