(define-module (crates-io kl ph) #:use-module (crates-io))

(define-public crate-KLPhash-0.1 (crate (name "KLPhash") (vers "0.1.0") (hash "0hvhsr38vpdcbg6h5wlq7lk98cc4kn3wsfxwy52fb821pl6qqwwd")))

(define-public crate-KLPhash-0.1 (crate (name "KLPhash") (vers "0.1.1") (hash "1adaqghvflfyyy1g11z1478x4i4f2cdzisfj67jzq758bm4k46fc")))

(define-public crate-KLPhash-0.1 (crate (name "KLPhash") (vers "0.1.2") (hash "0zscvk4gnclvpnn592ad9mzpi015k8bk1jkba056rzx4w5za9kfv")))

(define-public crate-KLPhash-0.1 (crate (name "KLPhash") (vers "0.1.3") (hash "02alj8zcl5m5b4vmi5cb37968b6i5g6il9jb2ix8yfwy36f4b54i")))

(define-public crate-KLPhash-0.1 (crate (name "KLPhash") (vers "0.1.4") (hash "185w6069ksf78c2w56zfc2r7a5nxinawbv54dhqqdrrhcn9hgsgd")))

