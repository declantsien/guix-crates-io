(define-module (crates-io kl ed) #:use-module (crates-io))

(define-public crate-kled-0.0.0 (crate (name "kled") (vers "0.0.0") (hash "0sj7y7qwd1kg715i1zjv1g1h4pdj91azyaxcprhi71mhdjdqgrxs")))

(define-public crate-kled_derive-0.0.0 (crate (name "kled_derive") (vers "0.0.0") (hash "02jfhw1w5xbigf6ylgjrh1hx8837ys4wvif9qgydkjfc28f1c30j")))

