(define-module (crates-io kl ow) #:use-module (crates-io))

(define-public crate-klownie_matrice-0.1 (crate (name "klownie_matrice") (vers "0.1.2") (deps (list (crate-dep (name "num") (req "^0.4.0") (default-features #t) (kind 0)))) (hash "07vd9vw0lfw9r1wdkbb9haxbkmm0g1zy61yj18k1zjrrm8w73gdh")))

(define-public crate-klownie_matrice-0.1 (crate (name "klownie_matrice") (vers "0.1.3") (deps (list (crate-dep (name "num") (req "^0.4.0") (default-features #t) (kind 0)))) (hash "0zazz67fy3f1m76zih9mp31hxyk8w27klhhyzgv2f3rbqbcr912n")))

(define-public crate-klownie_matrix-0.1 (crate (name "klownie_matrix") (vers "0.1.0") (deps (list (crate-dep (name "num") (req "^0.4.0") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)))) (hash "1l6ahspwvdxn3kn5v5b7a3v0n34i9gi1lc6v60cmjdlndmm23h03") (yanked #t)))

(define-public crate-klownie_matrix-0.1 (crate (name "klownie_matrix") (vers "0.1.1") (deps (list (crate-dep (name "num") (req "^0.4.0") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)))) (hash "1b5rzqimwq0haa9syy9si0i8la7jrbzxcmmbrka1xprvb7vq9nv1") (yanked #t)))

