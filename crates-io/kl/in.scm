(define-module (crates-io kl in) #:use-module (crates-io))

(define-public crate-klingon-0.0.0 (crate (name "klingon") (vers "0.0.0") (hash "1314ch4nhnxh1fh84bpyjc8xmvg1irpsaigcld70hi1l2ab53i0m")))

(define-public crate-klinker-1 (crate (name "klinker") (vers "1.0.2") (deps (list (crate-dep (name "clap") (req "~2.27.0") (default-features #t) (kind 0)) (crate-dep (name "flate2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "kerbalobjects") (req "^2.0.5") (default-features #t) (kind 0)))) (hash "0xv0mhj0l68sxc2fr87m48vfglm06091ha6l3xl36a74w5f8a29d")))

(define-public crate-klinker-2 (crate (name "klinker") (vers "2.0.0") (deps (list (crate-dep (name "clap") (req "~2.27.0") (default-features #t) (kind 0)) (crate-dep (name "flate2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "kerbalobjects") (req "^3.1.8") (default-features #t) (kind 0)))) (hash "1xlyprayxbgccrqbjbw9ir737iq8746ykdljcpawmv58wyqy8yqv")))

(define-public crate-klinker-2 (crate (name "klinker") (vers "2.0.1") (deps (list (crate-dep (name "clap") (req "~2.27.0") (default-features #t) (kind 0)) (crate-dep (name "flate2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "kerbalobjects") (req "^3.1.8") (default-features #t) (kind 0)))) (hash "0gn0b6534w43ai9r26y8xkbgvfc5h4440ylriz82amx8x0f8i9aj")))

(define-public crate-klinker-2 (crate (name "klinker") (vers "2.0.2") (deps (list (crate-dep (name "clap") (req "~2.27.0") (default-features #t) (kind 0)) (crate-dep (name "flate2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "kerbalobjects") (req "^3.1.9") (default-features #t) (kind 0)))) (hash "0fi22li6b9vc7h61ncrr0svwjd7qrqwcxhd5amdxx99z67dszm0s")))

(define-public crate-klinker-2 (crate (name "klinker") (vers "2.0.3") (deps (list (crate-dep (name "clap") (req "~2.27.0") (default-features #t) (kind 0)) (crate-dep (name "flate2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "kerbalobjects") (req "^3.1.11") (default-features #t) (kind 0)))) (hash "07za8w3d667ygiyikn041j5hn6gcxzr33h0sj215f0j8wp8vb784")))

(define-public crate-klinker-2 (crate (name "klinker") (vers "2.0.4") (deps (list (crate-dep (name "clap") (req "~2.27.0") (default-features #t) (kind 0)) (crate-dep (name "flate2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "kerbalobjects") (req "^3.1.12") (default-features #t) (kind 0)))) (hash "1pgm0g3z3iaf7wivjfnywkcjpmpiafwaxv16qr08xw0nca89wc48")))

(define-public crate-klinker-2 (crate (name "klinker") (vers "2.0.5") (deps (list (crate-dep (name "clap") (req "~2.27.0") (default-features #t) (kind 0)) (crate-dep (name "flate2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "kerbalobjects") (req "^3.1.12") (default-features #t) (kind 0)))) (hash "1mi5vmwkmg894rm35b7fplmjb28yg3dw5a1y6lz0xz8d9zavmg3f")))

(define-public crate-klinker-2 (crate (name "klinker") (vers "2.0.6") (deps (list (crate-dep (name "clap") (req "~2.27.0") (default-features #t) (kind 0)) (crate-dep (name "flate2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "kerbalobjects") (req "^3.1.12") (default-features #t) (kind 0)))) (hash "0ga66h0skx69nswrkdldmbmdjfdnnjb3y4169jyiwg5czvpnw2ag")))

(define-public crate-klinker-2 (crate (name "klinker") (vers "2.0.7") (deps (list (crate-dep (name "clap") (req "~2.27.0") (default-features #t) (kind 0)) (crate-dep (name "flate2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "kerbalobjects") (req "^3.1.12") (default-features #t) (kind 0)))) (hash "0k6fd0kmc2bisvdzhsg2m9pr2132kkpipbnb5nkw4mvbrlpp8zzq")))

(define-public crate-klinker-2 (crate (name "klinker") (vers "2.0.8") (deps (list (crate-dep (name "clap") (req "~2.34.0") (default-features #t) (kind 0)) (crate-dep (name "flate2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "kerbalobjects") (req "^3.1.12") (default-features #t) (kind 0)))) (hash "0qp4hbi9fimdayzkqaaz1cmh15g74r7znwkkp1kar5b6qd8i7wrk")))

(define-public crate-klinker-2 (crate (name "klinker") (vers "2.0.9") (deps (list (crate-dep (name "clap") (req "~2.34.0") (default-features #t) (kind 0)) (crate-dep (name "flate2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "kerbalobjects") (req "^3.1.12") (default-features #t) (kind 0)))) (hash "1q4laa2vxvhz012acp4sjpi0hgxla162ypbi9c5bp33v1277w0kc")))

(define-public crate-klinker-2 (crate (name "klinker") (vers "2.0.10") (deps (list (crate-dep (name "clap") (req "~2.34.0") (default-features #t) (kind 0)) (crate-dep (name "flate2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "kerbalobjects") (req "^3.1.13") (default-features #t) (kind 0)))) (hash "0svxvm94iq6ihi9png5cfnmqslia803gzc6w3irvylrcvv169nnn")))

(define-public crate-klinker-2 (crate (name "klinker") (vers "2.0.11") (deps (list (crate-dep (name "clap") (req "^4.0.18") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "flate2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "kerbalobjects") (req "^3.1.13") (default-features #t) (kind 0)))) (hash "1c3qn64mk1lsxb9sqpjyffscpvh3miniqx3bmq03z142vka20589")))

(define-public crate-klinker-3 (crate (name "klinker") (vers "3.0.0") (deps (list (crate-dep (name "clap") (req "^4.0.18") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "flate2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "kerbalobjects") (req "^4.0") (default-features #t) (kind 0)))) (hash "0wwgaa46xd7azmacd9g89rndqgwcq6iqrr2v41r9hyqyycjg5agw")))

(define-public crate-klinker-3 (crate (name "klinker") (vers "3.0.1") (deps (list (crate-dep (name "clap") (req "^4.0.18") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "flate2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "kerbalobjects") (req "^4.0") (default-features #t) (kind 0)))) (hash "14x8qp2xbvx1wcp9cfgfgvwmrgab2ivq2ndlk5vh67rjjvscnq72")))

(define-public crate-klinker-3 (crate (name "klinker") (vers "3.0.2") (deps (list (crate-dep (name "clap") (req "^4.0.18") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "flate2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "kerbalobjects") (req "^4.0.2") (default-features #t) (kind 0)))) (hash "1lwiwarqnj5xrnhpxqrnmiyikwhlv79if7qfldni45p8ngfskrwp")))

(define-public crate-klint-0.1 (crate (name "klint") (vers "0.1.0") (hash "1qip7bgk2w7dhcamn1h24apa0ffcqy91ppb9f28sv3lcp42vl4qk")))

