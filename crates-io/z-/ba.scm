(define-module (crates-io z- ba) #:use-module (crates-io))

(define-public crate-z-base-32-0.1 (crate (name "z-base-32") (vers "0.1.0") (deps (list (crate-dep (name "pyo3") (req "^0.14.2") (features (quote ("extension-module"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "quickcheck") (req "^1") (default-features #t) (kind 2)))) (hash "0lxc6q9j83ah4b9sgfwc4df1cxg2fa95vfdrha5bmhrvq0fgdrj9") (features (quote (("python" "pyo3"))))))

(define-public crate-z-base-32-0.1 (crate (name "z-base-32") (vers "0.1.1") (deps (list (crate-dep (name "pyo3") (req "^0.14.2") (features (quote ("extension-module"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "quickcheck") (req "^1") (default-features #t) (kind 2)))) (hash "0d3kdgm0k1vwp8wgs8bncxs25m6q2qn14wrgmxs83a6x1jpyhl9n") (features (quote (("python" "pyo3"))))))

(define-public crate-z-base-32-0.1 (crate (name "z-base-32") (vers "0.1.2") (deps (list (crate-dep (name "pyo3") (req "^0.14.2") (features (quote ("extension-module"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "quickcheck") (req "^1") (default-features #t) (kind 2)))) (hash "11sy4h4l2fk2r8r50l1dyfl8kdqaakr1fj1j580bgyvx18jpr2cf") (features (quote (("python" "pyo3"))))))

(define-public crate-z-base-32-0.1 (crate (name "z-base-32") (vers "0.1.3") (deps (list (crate-dep (name "pyo3") (req "^0.20.0") (features (quote ("extension-module"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "quickcheck") (req "^1") (default-features #t) (kind 2)))) (hash "14hzhw3dg2kw4bmh6i54mg3chy3w5mq4f85x2n7gh2ip2f3dk840") (features (quote (("python" "pyo3"))))))

(define-public crate-z-base-32-0.1 (crate (name "z-base-32") (vers "0.1.4") (deps (list (crate-dep (name "anyhow") (req "^1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^4.5.1") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "pyo3") (req "^0.20.2") (features (quote ("extension-module"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "quickcheck") (req "^1") (default-features #t) (kind 2)))) (hash "0fqk028mzvgnx9phqzvhzrx3fbzv4r736cm3x3hid136g157pgr1") (v 2) (features2 (quote (("python" "dep:pyo3") ("cli" "dep:clap" "dep:anyhow"))))))

