(define-module (crates-io z- rs) #:use-module (crates-io))

(define-public crate-z-rs-0.1 (crate (name "z-rs") (vers "0.1.0") (deps (list (crate-dep (name "argh") (req "^0.1.3") (default-features #t) (kind 0)) (crate-dep (name "bstr") (req "^0.2.13") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.114") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_cbor") (req "^0.11.1") (default-features #t) (kind 0)))) (hash "1bkdfnr1s5ayw47pkga4scaaiphydlhi45dfc4z2vikisgy0r7v1")))

