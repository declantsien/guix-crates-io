(define-module (crates-io hj ul) #:use-module (crates-io))

(define-public crate-hjul-0.1 (crate (name "hjul") (vers "0.1.0") (deps (list (crate-dep (name "mio") (req "^0.6.19") (default-features #t) (kind 0)) (crate-dep (name "mio-extras") (req "^2.0.5") (default-features #t) (kind 0)) (crate-dep (name "spin") (req "^0.5.1") (default-features #t) (kind 0)))) (hash "14ykydydbbzc1lhzafldc32nbiz90yx573hmqr4cjaiwrjjg34rz")))

(define-public crate-hjul-0.1 (crate (name "hjul") (vers "0.1.1") (deps (list (crate-dep (name "mio") (req "^0.6.19") (default-features #t) (kind 0)) (crate-dep (name "mio-extras") (req "^2.0.5") (default-features #t) (kind 0)) (crate-dep (name "spin") (req "^0.5.1") (default-features #t) (kind 0)))) (hash "175qpj2jk01mi6l46xci1jdr9xsswq93dy23h70sksv88rbby4xk")))

(define-public crate-hjul-0.1 (crate (name "hjul") (vers "0.1.2") (deps (list (crate-dep (name "mio") (req "^0.6.19") (default-features #t) (kind 0)) (crate-dep (name "mio-extras") (req "^2.0.5") (default-features #t) (kind 0)) (crate-dep (name "spin") (req "^0.5.1") (default-features #t) (kind 0)))) (hash "0xx5xccg0nnphzr5rybqcrc4hzy2w7rw5f1053ys6ig6qffzp7za")))

(define-public crate-hjul-0.2 (crate (name "hjul") (vers "0.2.0") (deps (list (crate-dep (name "mio") (req "^0.6.19") (default-features #t) (kind 0)) (crate-dep (name "mio-extras") (req "^2.0.5") (default-features #t) (kind 0)) (crate-dep (name "spin") (req "^0.5.1") (default-features #t) (kind 0)))) (hash "1z65z07fp3wvzb85rx95q7b75jyzyh4snkg770b82w7218jnh58m")))

(define-public crate-hjul-0.2 (crate (name "hjul") (vers "0.2.1") (deps (list (crate-dep (name "mio") (req "^0.6.19") (default-features #t) (kind 0)) (crate-dep (name "mio-extras") (req "^2.0.5") (default-features #t) (kind 0)) (crate-dep (name "spin") (req "^0.5.1") (default-features #t) (kind 0)))) (hash "0yg16767d6m96z1867qbja4anwvjqwjg2yfhvj4jwf1hwc86sc7k")))

(define-public crate-hjul-0.2 (crate (name "hjul") (vers "0.2.2") (deps (list (crate-dep (name "mio") (req "^0.6") (default-features #t) (kind 0)) (crate-dep (name "mio-extras") (req "^2.0") (default-features #t) (kind 0)) (crate-dep (name "spin") (req "^0.5") (default-features #t) (kind 0)))) (hash "104ihqrk5c4pwm2daxd6cacrdz7abqmid512vnir8kdzi6axahxm")))

