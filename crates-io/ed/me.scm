(define-module (crates-io ed me) #:use-module (crates-io))

(define-public crate-edmeti-0.2 (crate (name "edmeti") (vers "0.2.0") (deps (list (crate-dep (name "clap") (req "^2.33.1") (default-features #t) (kind 0)))) (hash "159gg9v71x6423rzyp1nawxnw67dij20y6mwvw2pxymswl7zqx1d")))

