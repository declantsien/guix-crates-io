(define-module (crates-io ed ip) #:use-module (crates-io))

(define-public crate-edip-0.1 (crate (name "edip") (vers "0.1.0") (deps (list (crate-dep (name "criterion") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "gchemol") (req "^0.0.42") (features (quote ("adhoc"))) (default-features #t) (kind 0)) (crate-dep (name "gut") (req "^0.1.3") (default-features #t) (kind 0) (package "gchemol-gut")) (crate-dep (name "vecfx") (req "^0.1") (features (quote ("nalgebra"))) (default-features #t) (kind 0)))) (hash "0ljridizc01azvzaax6wz1k9w2pp0mbp74zr035v88k6x11f292c") (features (quote (("adhoc"))))))

(define-public crate-edip-0.1 (crate (name "edip") (vers "0.1.1") (deps (list (crate-dep (name "criterion") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "gchemol") (req "^0.0.42") (features (quote ("adhoc"))) (default-features #t) (kind 2)) (crate-dep (name "gut") (req "^0.3") (default-features #t) (kind 0) (package "gchemol-gut")) (crate-dep (name "vecfx") (req "^0.1") (features (quote ("nalgebra"))) (default-features #t) (kind 0)))) (hash "1kyr0h6swd5h4c4z2mmckhnf6ff7vgxx34q1vw5q345xyfgj9hd6") (features (quote (("adhoc"))))))

