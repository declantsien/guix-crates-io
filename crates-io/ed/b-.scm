(define-module (crates-io ed b-) #:use-module (crates-io))

(define-public crate-edb-derive-0.1 (crate (name "edb-derive") (vers "0.1.0") (deps (list (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2.0") (default-features #t) (kind 0)))) (hash "1c176pzqvm0zh756lhvfvf1f7qhsa6jz5xmhcv3gc7s5x4w7if3c")))

(define-public crate-edb-derive-0.1 (crate (name "edb-derive") (vers "0.1.1") (deps (list (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2.0") (default-features #t) (kind 0)))) (hash "1v598664pqzyq1w012vphsxsa72vd9yrmjgja1idj2mzzx5x64wp")))

(define-public crate-edb-derive-0.1 (crate (name "edb-derive") (vers "0.1.2") (deps (list (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2.0") (default-features #t) (kind 0)))) (hash "17y457n5263d0aqj1bjig3a7jsrjvb5lx26xpq3lqcjphax6ya28")))

