(define-module (crates-io ed fs) #:use-module (crates-io))

(define-public crate-edfsm-0.5 (crate (name "edfsm") (vers "0.5.0") (deps (list (crate-dep (name "event-driven-macros") (req "^0.5.0") (default-features #t) (kind 0)))) (hash "1vqp0qjdavnyp45mmzivwfdj8652rghy9al16c46s627irpiwmm8") (yanked #t)))

(define-public crate-edfsm-0.5 (crate (name "edfsm") (vers "0.5.1") (deps (list (crate-dep (name "event-driven-macros") (req "^0.5.0") (default-features #t) (kind 0)))) (hash "0fr2rh0khjxgnnijd6xnbqpfvvvpw1dajbzrjzpfxcgv6y03d18d")))

(define-public crate-edfsm-0.6 (crate (name "edfsm") (vers "0.6.0") (deps (list (crate-dep (name "event-driven-macros") (req "^0.6.0") (default-features #t) (kind 0)))) (hash "0br1l5621pxamd2isgv6lfsg3kbd723c9fqnv0ghr998sia6bvqc")))

(define-public crate-edfsm-0.7 (crate (name "edfsm") (vers "0.7.0") (deps (list (crate-dep (name "event-driven-macros") (req "^0.7.0") (default-features #t) (kind 0)))) (hash "1na1fx7ala3r2143xw4nam9ycyvapmn237kny2fpd2rw73w6232c")))

