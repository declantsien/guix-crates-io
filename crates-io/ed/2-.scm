(define-module (crates-io ed #{2-}#) #:use-module (crates-io))

(define-public crate-ed2-derive-0.1 (crate (name "ed2-derive") (vers "0.1.1") (deps (list (crate-dep (name "proc-macro2") (req "^1.0.10") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.3") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.17") (default-features #t) (kind 0)))) (hash "1cbqvlw56hajdmyyvvjarn6ycl0qqgn83c0795iv2kslqfryz461")))

