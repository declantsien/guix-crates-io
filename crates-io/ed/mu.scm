(define-module (crates-io ed mu) #:use-module (crates-io))

(define-public crate-edmunge-1 (crate (name "edmunge") (vers "1.0.0") (deps (list (crate-dep (name "clap") (req "^2.31.2") (default-features #t) (kind 0)) (crate-dep (name "executable-path") (req "^1.0.0") (default-features #t) (kind 2)) (crate-dep (name "regex") (req "^1.0.0") (default-features #t) (kind 0)) (crate-dep (name "tempfile") (req "^3.0.2") (default-features #t) (kind 0)))) (hash "162lx21k7r4823yb9p7h3n3ffiyavz7j8w0plrxhxackzjsbwwgd")))

