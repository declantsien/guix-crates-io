(define-module (crates-io ed rs) #:use-module (crates-io))

(define-public crate-edrs-0.1 (crate (name "edrs") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req ">=4.3.8") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "crossterm") (req "^0.26.1") (default-features #t) (kind 0)) (crate-dep (name "syntect") (req "^5.0.0") (default-features #t) (kind 0)))) (hash "0azakjg22sbziwf6d5f5yl5577691zf9fpzd5w3s0ir80kggik5y")))

