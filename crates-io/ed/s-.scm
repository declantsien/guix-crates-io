(define-module (crates-io ed s-) #:use-module (crates-io))

(define-public crate-eds-core-0.5 (crate (name "eds-core") (vers "0.5.1") (deps (list (crate-dep (name "crc16") (req "^0.4.0") (default-features #t) (kind 0)))) (hash "09rnnzs5nk82lnwxwh9mjmgap18gflg4i0gcg4dpgffqrabps0mc")))

(define-public crate-eds-reader-0.1 (crate (name "eds-reader") (vers "0.1.0") (deps (list (crate-dep (name "eds") (req "^0.5") (default-features #t) (kind 0)) (crate-dep (name "fixed-queue") (req "^0.4") (default-features #t) (kind 0)))) (hash "13naj2ma4rr62391zwcpb1yyfb7qpn6vb898nxhjwvjhq0x9q97l")))

(define-public crate-eds-reader-0.1 (crate (name "eds-reader") (vers "0.1.1") (deps (list (crate-dep (name "eds") (req "^0.5") (default-features #t) (kind 0)) (crate-dep (name "fixed-queue") (req "^0.5") (default-features #t) (kind 0)))) (hash "0yzg5cxl5ank1dyxq7f4wm5hx8ryyng62c4h40r3khqbh4rsiygy")))

(define-public crate-eds-reader-0.1 (crate (name "eds-reader") (vers "0.1.2") (deps (list (crate-dep (name "bytes") (req "^1.1") (default-features #t) (kind 0)) (crate-dep (name "eds") (req "^0.5") (default-features #t) (kind 0)))) (hash "0ks6sgzfwpk3dax2bni6vggiks14sjha2pp9md9rxr1zd11684n5")))

(define-public crate-eds-reader-0.1 (crate (name "eds-reader") (vers "0.1.3") (deps (list (crate-dep (name "bytes") (req "^1.1") (default-features #t) (kind 0)) (crate-dep (name "eds-core") (req "^0.5") (default-features #t) (kind 0)))) (hash "1jy0gfsq35qk9lvwrbb5r3sy99gnp5zc767f2gp336qxrjkkp1i3")))

(define-public crate-eds-reader-0.1 (crate (name "eds-reader") (vers "0.1.4") (deps (list (crate-dep (name "bytes") (req "^1.1") (kind 0)) (crate-dep (name "eds-core") (req "^0.5") (default-features #t) (kind 0)))) (hash "1ci60m17v67dsbmhi162kq25bq8crmmx1c9i7n7hxgbk6w92iqa6")))

(define-public crate-eds-writer-0.1 (crate (name "eds-writer") (vers "0.1.0") (deps (list (crate-dep (name "bytes") (req "^1.1") (default-features #t) (kind 0)) (crate-dep (name "eds") (req "^0.5") (default-features #t) (kind 0)))) (hash "19ska4cs06r6aqdc3w0639vfb7kj4rcn3874w7pxwv3mwxk14d8j")))

(define-public crate-eds-writer-0.1 (crate (name "eds-writer") (vers "0.1.1") (deps (list (crate-dep (name "bytes") (req "^1.1") (default-features #t) (kind 0)) (crate-dep (name "eds") (req "^0.5") (default-features #t) (kind 0)))) (hash "1friba8njw7d7xiky2dz5vx4va4lcas5qf0ma1fblqp7yp1ri57b")))

(define-public crate-eds-writer-0.1 (crate (name "eds-writer") (vers "0.1.2") (deps (list (crate-dep (name "bytes") (req "^1.1") (default-features #t) (kind 0)) (crate-dep (name "eds-core") (req "^0.5") (default-features #t) (kind 0)))) (hash "0h8xvr4lk9vx2fv23sgczjwpsybi0qg2bpmayxx0hhn0s6sgfia3")))

(define-public crate-eds-writer-0.1 (crate (name "eds-writer") (vers "0.1.3") (deps (list (crate-dep (name "bytes") (req "^1.1") (kind 0)) (crate-dep (name "eds-core") (req "^0.5") (default-features #t) (kind 0)))) (hash "18ffdy8x8wg8lsm3brapsvpr83x18mj5c5dnwzhlp8hc0ra0vldg")))

