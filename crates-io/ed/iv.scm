(define-module (crates-io ed iv) #:use-module (crates-io))

(define-public crate-edivisive-0.1 (crate (name "edivisive") (vers "0.1.0") (deps (list (crate-dep (name "itertools") (req "^0.10") (default-features #t) (kind 0)) (crate-dep (name "ndarray") (req "^0.14") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "rstest") (req "^0.6") (default-features #t) (kind 2)) (crate-dep (name "serde") (req "^1") (features (quote ("derive"))) (default-features #t) (kind 2)) (crate-dep (name "serde_json") (req "^1") (default-features #t) (kind 2)))) (hash "1w2izx1nnpr00g94m1l0chbs7kz08775c1kljc2gzxqgjc4x1a7q")))

