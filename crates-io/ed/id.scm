(define-module (crates-io ed id) #:use-module (crates-io))

(define-public crate-edid-0.1 (crate (name "edid") (vers "0.1.0") (deps (list (crate-dep (name "nom") (req "^3.2.0") (default-features #t) (kind 0)))) (hash "11z3qcmrgalx8j3j77gvx7mx562qpl5yak25q701a330rxjbh3mn")))

(define-public crate-edid-0.1 (crate (name "edid") (vers "0.1.1") (deps (list (crate-dep (name "nom") (req "^3.2.0") (default-features #t) (kind 0)))) (hash "18bzr90dq5bzjwphhx1glmyqyz7n75pj7mnv80zivmqi61f7fpwk")))

(define-public crate-edid-0.2 (crate (name "edid") (vers "0.2.0") (deps (list (crate-dep (name "nom") (req "^3.2.0") (features (quote ("verbose-errors"))) (default-features #t) (kind 0)))) (hash "1jpk6hz6vl9fscrzlviipf7vbfm03ha25fclk4whdiyz9kd17iwv")))

(define-public crate-edid-0.3 (crate (name "edid") (vers "0.3.0") (deps (list (crate-dep (name "nom") (req "^3.2.0") (features (quote ("verbose-errors"))) (default-features #t) (kind 0)))) (hash "1zi4md5sy60nlcy45d3w9r48ki6ywzh7rdivzvf39n4k119pbki4")))

(define-public crate-edid-rs-0.1 (crate (name "edid-rs") (vers "0.1.0") (hash "0b2jw5difvsywvzlrq1g67xgwq1klqcmhj6zak1mmn2w90rzmd9a") (features (quote (("no_std") ("default"))))))

