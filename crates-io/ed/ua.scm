(define-module (crates-io ed ua) #:use-module (crates-io))

(define-public crate-eduapi-port-0.1 (crate (name "eduapi-port") (vers "0.1.0") (deps (list (crate-dep (name "anyhow") (req "^1.0.75") (default-features #t) (kind 0)) (crate-dep (name "assert_cmd") (req "^2.0.12") (features (quote ("color-auto"))) (default-features #t) (kind 0)) (crate-dep (name "async-curl") (req "^0.1.6") (features (quote ("multi"))) (default-features #t) (kind 0)) (crate-dep (name "nix") (req "^0.27.1") (features (quote ("fs" "aio"))) (default-features #t) (kind 0)))) (hash "0haj7cc3758j0pywqi4yxb5yflrc39wvz0iilpx9kpafj8hd2kvw")))

(define-public crate-eduardo_functions-0.1 (crate (name "eduardo_functions") (vers "0.1.0") (hash "1pa729fyh4xs3k2b28fsa2vyfji00ihaiw0lmz43am5kz5mqa1r6")))

(define-public crate-eduardo_functions-0.1 (crate (name "eduardo_functions") (vers "0.1.1") (hash "1ybyr51wmbcl9yd6pfw7l17wji5a7jlanhzf9db9xr4gwamzcp9r")))

(define-public crate-eduardo_more_cargo-0.1 (crate (name "eduardo_more_cargo") (vers "0.1.0") (hash "0a6ldg5yha57qlxihg79ndwg9s4341igxhb4s38w42bq0xh26jvx") (yanked #t)))

(define-public crate-eduardo_more_cargo-0.1 (crate (name "eduardo_more_cargo") (vers "0.1.1") (hash "08my1qzikl8vmn4j3c4d0ddk0zv4q8yi5a7h5b2pzp2j6by8craj")))

