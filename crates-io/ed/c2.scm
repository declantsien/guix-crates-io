(define-module (crates-io ed c2) #:use-module (crates-io))

(define-public crate-edc2svd-0.1 (crate (name "edc2svd") (vers "0.1.0") (deps (list (crate-dep (name "fern") (req "^0.5.7") (default-features #t) (kind 0)) (crate-dep (name "getopts") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.6") (default-features #t) (kind 0)) (crate-dep (name "xmltree") (req "^0.8.0") (default-features #t) (kind 0)))) (hash "0zfs5x8xgpmh9mlz7dbmc15qmzf4h1bq8k34f5vxjfvjgnpp0rks")))

(define-public crate-edc2svd-0.2 (crate (name "edc2svd") (vers "0.2.0") (deps (list (crate-dep (name "fern") (req "^0.5.7") (default-features #t) (kind 0)) (crate-dep (name "getopts") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.6") (default-features #t) (kind 0)) (crate-dep (name "xmltree") (req "^0.8.0") (default-features #t) (kind 0)))) (hash "0cg2wl8rkmawc8ygdsm7ixs0qw83qyyxi5av11jbgkwa623964ma")))

(define-public crate-edc2svd-0.3 (crate (name "edc2svd") (vers "0.3.0") (deps (list (crate-dep (name "fern") (req "^0.5.7") (default-features #t) (kind 0)) (crate-dep (name "getopts") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.6") (default-features #t) (kind 0)) (crate-dep (name "xmltree") (req "^0.8.0") (default-features #t) (kind 0)))) (hash "00zqnhpfqhb3daklbbymgvii8sx235n1sjzcvpkp9zg2p5gj7b4k")))

(define-public crate-edc2svd-0.3 (crate (name "edc2svd") (vers "0.3.1") (deps (list (crate-dep (name "fern") (req "^0.6.1") (default-features #t) (kind 0)) (crate-dep (name "getopts") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.6") (default-features #t) (kind 0)) (crate-dep (name "xmltree") (req "^0.8.0") (default-features #t) (kind 0)))) (hash "0ng0r9633l4g8dnpkv915rldm7qlbwz6g0kkj6g8ycx7cvxx168q")))

(define-public crate-edc2svd-0.4 (crate (name "edc2svd") (vers "0.4.0") (deps (list (crate-dep (name "fern") (req "^0.6.1") (default-features #t) (kind 0)) (crate-dep (name "getopts") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.6") (default-features #t) (kind 0)) (crate-dep (name "xmltree") (req "^0.8.0") (default-features #t) (kind 0)))) (hash "0h2nnh31yj8chqgm3cc707fk59a4wm9rjgyqr64pwihhw2rfjb7c")))

(define-public crate-edc2svd-0.5 (crate (name "edc2svd") (vers "0.5.0") (deps (list (crate-dep (name "fern") (req "^0.6.1") (default-features #t) (kind 0)) (crate-dep (name "getopts") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.17") (default-features #t) (kind 0)) (crate-dep (name "xmltree") (req "^0.10.3") (default-features #t) (kind 0)))) (hash "0g4siyv60mzf5d1cpkxg2287q2vbb6sh1dh9bgzw3y6q5csl33xf")))

