(define-module (crates-io ed #{2k}#) #:use-module (crates-io))

(define-public crate-ed2k-1 (crate (name "ed2k") (vers "1.0.0") (deps (list (crate-dep (name "digest") (req "^0.10") (default-features #t) (kind 0)) (crate-dep (name "md4") (req "^0.10") (default-features #t) (kind 0)))) (hash "1hz54fj7kfcxx0q88xjgjhf59sa78x5pcyz044dkvcckjmz4qwcz")))

(define-public crate-ed2k-1 (crate (name "ed2k") (vers "1.0.1") (deps (list (crate-dep (name "digest") (req "^0.10") (default-features #t) (kind 0)) (crate-dep (name "md4") (req "^0.10") (default-features #t) (kind 0)))) (hash "0y53x3ivy9wdi32zdjn6w9kqz67s9pga51kab4q653m4nk2dz57r")))

