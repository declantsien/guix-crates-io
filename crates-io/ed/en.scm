(define-module (crates-io ed en) #:use-module (crates-io))

(define-public crate-eden-0.1 (crate (name "eden") (vers "0.1.0") (hash "0k8rq8syy0lrkh3dw9fg97phcpbzklvrgywdbl1k6vk9900rj086")))

(define-public crate-eden-0.1 (crate (name "eden") (vers "0.1.1") (deps (list (crate-dep (name "nom") (req "^5.1.2") (default-features #t) (kind 0)))) (hash "0f7pqsr9hgn97gz8mnr3z3r42bcs15225rgcp7d7k7bwy62yhrcg")))

