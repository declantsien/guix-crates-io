(define-module (crates-io ed gy) #:use-module (crates-io))

(define-public crate-edgy-0.1 (crate (name "edgy") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^2.32") (features (quote ("yaml"))) (default-features #t) (kind 0)) (crate-dep (name "image") (req "^0.21.0") (default-features #t) (kind 0)) (crate-dep (name "pbr") (req "^1.0.1") (default-features #t) (kind 0)))) (hash "086aj3m3c3fl4237wzj18gqqvfd830ssdhky0zv71ly7k57126md")))

(define-public crate-edgy-0.1 (crate (name "edgy") (vers "0.1.1") (deps (list (crate-dep (name "clap") (req "^2.32") (features (quote ("yaml"))) (default-features #t) (kind 0)) (crate-dep (name "image") (req "^0.21.0") (default-features #t) (kind 0)) (crate-dep (name "pbr") (req "^1.0.1") (default-features #t) (kind 0)))) (hash "1xkw966hjfzz6i9w4d404bpihga4wr9d1vz05bna7hrpmbj1wcxs")))

