(define-module (crates-io ed is) #:use-module (crates-io))

(define-public crate-edisp-0.0.1 (crate (name "edisp") (vers "0.0.1") (hash "0rxb44g3n1gixwr8wcgy95g95d0arry7m8q19vxy2wgk24avl6a0")))

(define-public crate-edisp-0.0.2 (crate (name "edisp") (vers "0.0.2") (hash "0zl5vxkwim1igyl5m6sidd0kcdzibf2lwlhwn22jz7j4b72v94a1")))

(define-public crate-edisp-0.0.3 (crate (name "edisp") (vers "0.0.3") (hash "02lff7m802sqfi7a046b7d3fwzfgh69sh9fnxha4bnqdqgl0chrw")))

