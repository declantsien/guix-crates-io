(define-module (crates-io ed fp) #:use-module (crates-io))

(define-public crate-edfp-0.1 (crate (name "edfp") (vers "0.1.0") (deps (list (crate-dep (name "tempfile") (req "^2.1.5") (default-features #t) (kind 0)))) (hash "01nx6k4d5z0m2hfr3jdn9kc75ygmw7abw2di1pi9l7xqvyq7kkmy")))

(define-public crate-edfp2-0.1 (crate (name "edfp2") (vers "0.1.0") (deps (list (crate-dep (name "tempfile") (req "^2.1.5") (default-features #t) (kind 0)))) (hash "07bzv465ss1qvv2k5yvd9csi1vymw5ka1qds1hzgr006cnvabnkw")))

