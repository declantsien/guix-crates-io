(define-module (crates-io xi ph) #:use-module (crates-io))

(define-public crate-xipher-0.0.0 (crate (name "xipher") (vers "0.0.0") (hash "1y8p72n6xld77rakc0gv7k7b1j9prrpsd8qpz2in80k1jx9c4p58")))

(define-public crate-xiphr-0.0.0 (crate (name "xiphr") (vers "0.0.0") (hash "0mx76jvdi2nancnmrsgm24mxgsc08h0f2gj4kvl6q1a0m4r5nhm9")))

