(define-module (crates-io xi rr) #:use-module (crates-io))

(define-public crate-xirr-0.1 (crate (name "xirr") (vers "0.1.0") (deps (list (crate-dep (name "chrono") (req "^0.4.2") (default-features #t) (kind 0)) (crate-dep (name "csv") (req "^1.0.0") (default-features #t) (kind 2)))) (hash "0h79zr1dmxclksjk8n632fw27szvvvc0pjrlzmz2zhqm9lj48c83")))

(define-public crate-xirr-0.1 (crate (name "xirr") (vers "0.1.1") (deps (list (crate-dep (name "chrono") (req "^0.4.2") (default-features #t) (kind 0)) (crate-dep (name "csv") (req "^1.0.0") (default-features #t) (kind 2)))) (hash "0vi404va74ia3fb1217c7jmb2781ndk4366nfr2ricb5rk0hmr7w")))

(define-public crate-xirr-0.1 (crate (name "xirr") (vers "0.1.2") (deps (list (crate-dep (name "chrono") (req "^0.4.2") (default-features #t) (kind 0)) (crate-dep (name "csv") (req "^1.0.0") (default-features #t) (kind 2)))) (hash "0rxk95q5z94lnfycjl5lmnz9y58rqk8864zvdlvx11njy8za7qvl")))

(define-public crate-xirr-0.1 (crate (name "xirr") (vers "0.1.3") (deps (list (crate-dep (name "chrono") (req "^0.4.2") (default-features #t) (kind 0)) (crate-dep (name "csv") (req "^1.0.0") (default-features #t) (kind 2)))) (hash "0hm6wva0p6iamxdnaiaglf8nkp5f6x316fnpsc9x2jlsk6mir6fq")))

(define-public crate-xirr-0.2 (crate (name "xirr") (vers "0.2.0") (deps (list (crate-dep (name "chrono") (req "^0.4.2") (default-features #t) (kind 0)) (crate-dep (name "csv") (req "^1.0.0") (default-features #t) (kind 2)))) (hash "04ljbgkml1sycv2nvw0rjyvr0n6y9fx55v7njh53ppqldhkvj1km")))

(define-public crate-xirr-0.2 (crate (name "xirr") (vers "0.2.1") (deps (list (crate-dep (name "chrono") (req "^0.4.6") (default-features #t) (kind 0)) (crate-dep (name "csv") (req "^1.0.0") (default-features #t) (kind 2)))) (hash "012gqb18sljyd379r9fjkjx6zxi839izqzw2ifnw1z8gc045f8dn")))

(define-public crate-xirr-0.2 (crate (name "xirr") (vers "0.2.2") (deps (list (crate-dep (name "chrono") (req "^0.4.6") (default-features #t) (kind 0)) (crate-dep (name "csv") (req "^1.0.0") (default-features #t) (kind 2)))) (hash "1bclggdhp1z243i0164fr990ksxp9dd9fr907n0visp8v7vcky81")))

(define-public crate-xirr-0.2 (crate (name "xirr") (vers "0.2.3") (deps (list (crate-dep (name "chrono") (req "^0.4.6") (default-features #t) (kind 0)) (crate-dep (name "csv") (req "^1.0.0") (default-features #t) (kind 2)))) (hash "1zdqhfi6dv0nwcqfdmwfsb2ba5m4960966dqmi21f339mnvic47p")))

