(define-module (crates-io xi nt) #:use-module (crates-io))

(define-public crate-xint-0.1 (crate (name "xint") (vers "0.1.0") (hash "1qnqzcvabig4mc0fqxcyj6frk490w8q7i12scg76ab7ix4smgfp5")))

(define-public crate-xinto-0.2 (crate (name "xinto") (vers "0.2.0") (deps (list (crate-dep (name "clap") (req "^2.33.0") (default-features #t) (kind 0)) (crate-dep (name "err-derive") (req "^0.2.2") (default-features #t) (kind 0)) (crate-dep (name "hex") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "tempfile") (req "^3.1.0") (default-features #t) (kind 2)))) (hash "0pxvljn1hwpqzr0m4mz7ijfg1wbmfxdd91rhglvmin81izv95y07")))

