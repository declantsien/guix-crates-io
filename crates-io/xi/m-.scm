(define-module (crates-io xi m-) #:use-module (crates-io))

(define-public crate-xim-ctext-0.1 (crate (name "xim-ctext") (vers "0.1.0") (hash "0hx7ff1aw6mcfdvp08bf28srhswvrh137z1fbqcmk0a0vh0rk8nq")))

(define-public crate-xim-ctext-0.2 (crate (name "xim-ctext") (vers "0.2.0") (deps (list (crate-dep (name "encoding_rs") (req "^0.8.28") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.28") (default-features #t) (kind 0)))) (hash "0bidbhyvkz1s842vfpdpx65yqi0x9h3knj6x46jdd5xz5b5wrc7v")))

(define-public crate-xim-ctext-0.3 (crate (name "xim-ctext") (vers "0.3.0") (deps (list (crate-dep (name "encoding_rs") (req "^0.8.28") (default-features #t) (kind 0)))) (hash "0k186cy9dcw999w74ak39f1cfp2xhgpywbp8nqvkq3y4c9q1miia") (features (quote (("std") ("default" "std"))))))

(define-public crate-xim-gen-0.1 (crate (name "xim-gen") (vers "0.1.0") (deps (list (crate-dep (name "convert_case") (req "^0.4.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.118") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_yaml") (req "^0.8.14") (default-features #t) (kind 0)))) (hash "1kqkz8snpb34c6af6hki13cq90vis2frhzgxkdm066xl09z6lzn2")))

(define-public crate-xim-parser-0.1 (crate (name "xim-parser") (vers "0.1.0") (deps (list (crate-dep (name "bitflags") (req "^1.2.1") (default-features #t) (kind 0)) (crate-dep (name "pretty_assertions") (req "^0.6.1") (default-features #t) (kind 2)) (crate-dep (name "thiserror") (req "^1.0.22") (default-features #t) (kind 0)) (crate-dep (name "xim-ctext") (req "^0.1.0") (default-features #t) (kind 2)) (crate-dep (name "xim-gen") (req "^0.1.0") (optional #t) (default-features #t) (kind 1)))) (hash "01v6nxyziifs4jyvgjywvmw58dp07hykks36p16pykb9lngj1fyv") (features (quote (("bootstrap" "xim-gen"))))))

(define-public crate-xim-parser-0.1 (crate (name "xim-parser") (vers "0.1.1") (deps (list (crate-dep (name "bitflags") (req "^1.3.2") (default-features #t) (kind 0)) (crate-dep (name "pretty_assertions") (req "^0.7.2") (default-features #t) (kind 2)) (crate-dep (name "thiserror") (req "^1.0.28") (default-features #t) (kind 0)) (crate-dep (name "xim-ctext") (req "^0.2.0") (default-features #t) (kind 2)) (crate-dep (name "xim-gen") (req "^0.1.0") (optional #t) (default-features #t) (kind 1)))) (hash "1pcijgs5sa3y7mfl5hq3jpf4ygi0ws3vv7jnrvzmz3mb90q3drap") (features (quote (("bootstrap" "xim-gen"))))))

(define-public crate-xim-parser-0.2 (crate (name "xim-parser") (vers "0.2.0") (deps (list (crate-dep (name "bitflags") (req "^1.3.2") (kind 0)) (crate-dep (name "pretty_assertions") (req "^1") (default-features #t) (kind 2)) (crate-dep (name "xim-ctext") (req "^0.3.0") (default-features #t) (kind 2)) (crate-dep (name "xim-gen") (req "^0.1.0") (optional #t) (default-features #t) (kind 1)))) (hash "1736nwganwwnqnpf5q43xvyc40lc92ygqg8nvrgf1kw8wn7ix2hm") (features (quote (("std") ("default" "std") ("bootstrap" "xim-gen")))) (yanked #t)))

(define-public crate-xim-parser-0.2 (crate (name "xim-parser") (vers "0.2.1") (deps (list (crate-dep (name "bitflags") (req "^1.3.2") (kind 0)) (crate-dep (name "pretty_assertions") (req "^1") (default-features #t) (kind 2)) (crate-dep (name "xim-ctext") (req "^0.3.0") (default-features #t) (kind 2)) (crate-dep (name "xim-gen") (req "^0.1.0") (optional #t) (default-features #t) (kind 1)))) (hash "0lzysrpqs4bwgbqkc80yvl4bqx6l1p62ll1945hbzvwgfnavh05i") (features (quote (("std") ("default" "std") ("bootstrap" "xim-gen"))))))

