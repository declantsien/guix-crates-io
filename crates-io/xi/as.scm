(define-module (crates-io xi as) #:use-module (crates-io))

(define-public crate-xias-0.1 (crate (name "xias") (vers "0.1.0") (hash "0c5d089388njv6a28yaz1aah3c49zfqnd65bwapdn5a1nbw1mc06")))

(define-public crate-xias-0.2 (crate (name "xias") (vers "0.2.0") (deps (list (crate-dep (name "paste") (req "^1.0.6") (default-features #t) (kind 2)))) (hash "0wz8r4k2j8fmf70sr6bl721jxn6g7rcs8i1lgss80sv99j3zg7zv")))

(define-public crate-xias-0.2 (crate (name "xias") (vers "0.2.1") (deps (list (crate-dep (name "paste") (req "^1.0.6") (default-features #t) (kind 2)))) (hash "12x0wdpfwssp6117f3ixrfqv058aladag2q07a1zvaf97dvwn0aj")))

(define-public crate-xias-0.3 (crate (name "xias") (vers "0.3.0") (deps (list (crate-dep (name "paste") (req "^1.0.6") (default-features #t) (kind 2)))) (hash "0kjm5lgj43vx7kfnqcayq1g4plnsmvc6zxs0wfp5km157aidkac7")))

