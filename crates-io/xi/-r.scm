(define-module (crates-io xi -r) #:use-module (crates-io))

(define-public crate-xi-rope-0.1 (crate (name "xi-rope") (vers "0.1.0") (hash "17qqmnh126sh1m4flq95n0shvrvdkmdjccwmsj5japnqdrdx7wn4")))

(define-public crate-xi-rope-0.1 (crate (name "xi-rope") (vers "0.1.1") (deps (list (crate-dep (name "bytecount") (req "^0.1.2") (default-features #t) (kind 0)))) (hash "1ny4q5i0hashqvgw0jlpijr97mpps22ihqk408847h2lq0i9wygm") (features (quote (("simd-accel" "bytecount/simd-accel") ("avx-accel" "bytecount/avx-accel"))))))

(define-public crate-xi-rope-0.2 (crate (name "xi-rope") (vers "0.2.0") (deps (list (crate-dep (name "bytecount") (req "^0.1.2") (default-features #t) (kind 0)))) (hash "165cma9vms7jjp4wcikf78hzcwsb7cpkm5jbx7qib3jz1a9h9421") (features (quote (("simd-accel" "bytecount/simd-accel") ("avx-accel" "bytecount/avx-accel"))))))

(define-public crate-xi-rope-0.3 (crate (name "xi-rope") (vers "0.3.0") (deps (list (crate-dep (name "bytecount") (req "^0.5") (default-features #t) (kind 0)) (crate-dep (name "memchr") (req "^2.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 2)) (crate-dep (name "serde_test") (req "^1.0") (default-features #t) (kind 2)) (crate-dep (name "unicode-segmentation") (req "^1.2.1") (default-features #t) (kind 0)))) (hash "0njxqqmljdaaxa16pni670zpx269pg3jndq55538cjhr29k6q9n1") (features (quote (("default"))))))

(define-public crate-xi-rpc-0.1 (crate (name "xi-rpc") (vers "0.1.0") (deps (list (crate-dep (name "crossbeam") (req "^0.2.9") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^0.8") (default-features #t) (kind 0)))) (hash "16a4wb66vv43zcjipxbnfkipgck8fz48gdir26657sxmxflcad79")))

(define-public crate-xi-rpc-0.2 (crate (name "xi-rpc") (vers "0.2.0") (deps (list (crate-dep (name "crossbeam") (req "^0.2.9") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^0.9") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^0.9") (default-features #t) (kind 0)))) (hash "0mxx9lw926kd12jkrhjxiivpzwi7xmrdr8lsr5psnmvs0d12pyck")))

(define-public crate-xi-rpc-0.3 (crate (name "xi-rpc") (vers "0.3.0") (deps (list (crate-dep (name "crossbeam") (req "^0.7") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.3") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "xi-trace") (req "^0.2.0") (default-features #t) (kind 0)))) (hash "03wraxd9215z0pzybrzkqa0w3scl35s9c9796i8zl5jylhlc2p5y")))

