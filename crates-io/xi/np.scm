(define-module (crates-io xi np) #:use-module (crates-io))

(define-public crate-xinput-0.1 (crate (name "xinput") (vers "0.1.0") (deps (list (crate-dep (name "winapi") (req "^0.2.8") (default-features #t) (kind 0)) (crate-dep (name "xinput-sys") (req "^0.2.0") (default-features #t) (kind 0)))) (hash "0ik8f9avnzxfg6p4qi1yxklgsiknpzsk6lkxm2bd25lbil4jwfrh")))

(define-public crate-xinput-0.1 (crate (name "xinput") (vers "0.1.1") (deps (list (crate-dep (name "winapi") (req "^0.2.8") (default-features #t) (kind 0)) (crate-dep (name "xinput-sys") (req "^0.2.0") (default-features #t) (kind 0)))) (hash "0yw5mlswhjx1w20v93kj0k31mxbfvprm44vqdyl2vb8jh4chnzfj")))

(define-public crate-xinput-sys-0.0.1 (crate (name "xinput-sys") (vers "0.0.1") (deps (list (crate-dep (name "winapi") (req "*") (default-features #t) (kind 0)))) (hash "1yid1wqwz14xk875kb6ws6fymvb6c8b6vhiyg18w9ncvwyfv0ixh")))

(define-public crate-xinput-sys-0.2 (crate (name "xinput-sys") (vers "0.2.0") (deps (list (crate-dep (name "winapi") (req "^0.2.5") (default-features #t) (kind 0)) (crate-dep (name "winapi-build") (req "^0.1.1") (default-features #t) (kind 1)))) (hash "082a140fzmzh6g1r68p9x14sw38cb1hsnj8y4n095v8al9p5gk1a")))

(define-public crate-xinput_visualiser-0.1 (crate (name "xinput_visualiser") (vers "0.1.0") (deps (list (crate-dep (name "crossterm") (req "^0.17") (default-features #t) (kind 0)) (crate-dep (name "rusty-xinput") (req "^1.2.0") (default-features #t) (kind 0)) (crate-dep (name "tui") (req "^0.19") (features (quote ("crossterm"))) (kind 0)))) (hash "0z2xlx1cn0dgn3rwkgsii99wh3imkxjiqyf7zql8ds2wk78xps28")))

