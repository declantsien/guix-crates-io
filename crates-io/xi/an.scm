(define-module (crates-io xi an) #:use-module (crates-io))

(define-public crate-xiang-0.0.0 (crate (name "xiang") (vers "0.0.0") (hash "1bcnm5hwmg0mqpsp0h15jyb02b979ky0nbj8idsf67hs7zbsigd4")))

(define-public crate-xiangyun-0.0.1 (crate (name "xiangyun") (vers "0.0.1") (hash "14k9k29ckvqk87nnphmgw1jmq4rxh20x12jds6z6870qw8avpz39")))

(define-public crate-xiangyun-0.0.2 (crate (name "xiangyun") (vers "0.0.2") (hash "1l7irmfh9m3zhqi6mn3kv577pzhj7005w8k6iidfvaglriv9rd3j")))

(define-public crate-xiangyun-0.0.3 (crate (name "xiangyun") (vers "0.0.3") (hash "19k5h95c2ljinv2mxwfrdpgrqkzvnpfssc2khriwqv5zrsdkjxf7")))

(define-public crate-xiangyun-0.0.4 (crate (name "xiangyun") (vers "0.0.4") (hash "02csghamcy7jk513qkw1wgmwsdymw6zzhw662w6ps7h6zd2w40p9")))

(define-public crate-xiangyun-0.0.5 (crate (name "xiangyun") (vers "0.0.5") (hash "1k26r6znzsc2qw9mhrydhs6xa9clclhrz954gmhmxr1gvz3mn325")))

(define-public crate-xiangyun-0.1 (crate (name "xiangyun") (vers "0.1.0") (hash "06i806snpil7lvi17sizxlppb6fb0q7fa668c52yv1qfgj5f0fq2")))

(define-public crate-xiangyun-0.1 (crate (name "xiangyun") (vers "0.1.1-alpha") (hash "0wanahfbagms7iim1bp4wzcijcd7k9k42n1crdqh2xnwwp36pq09")))

(define-public crate-xiangyun-0.1 (crate (name "xiangyun") (vers "0.1.1") (hash "1xxv56q09ry45y00ayl8zd0ivgpq04b4vzlcn704r239jvqpgzm5")))

(define-public crate-xiangyun-0.1 (crate (name "xiangyun") (vers "0.1.2") (hash "081nbvfli0sdrbj2ygb34hyjp07n70vmymh0akgcmd1fxrfxzn5d")))

(define-public crate-xiangyun-0.2 (crate (name "xiangyun") (vers "0.2.0") (hash "1r12zwijda7l31jk8riail00bwwyd666ir3d58xidv9j8jfxdb3a")))

(define-public crate-xiangyun-0.2 (crate (name "xiangyun") (vers "0.2.1") (hash "00mqby0zyih223xr0mxnj0nv4xdv85iy9pmnjcddpn60i9n5q9js")))

(define-public crate-xiangyun-0.2 (crate (name "xiangyun") (vers "0.2.2") (hash "1vz9d96cadk128ngl1bcfa8w8va99n31jixsn43dg47bvf4fsm36")))

(define-public crate-xiangyun-0.2 (crate (name "xiangyun") (vers "0.2.3") (hash "148r49k5ggivg56wnqmk2rsyvkykvnb5hc2yg7188sj2knhzf3pi")))

(define-public crate-xiangyun-0.2 (crate (name "xiangyun") (vers "0.2.4") (hash "0vngx62v674b8ksm6mvrqicb4v0hjri87b4ra0blk37g0fkp45gh")))

(define-public crate-xiangyun-0.2 (crate (name "xiangyun") (vers "0.2.5") (hash "0rznnvxl7n8g0mrgqjjgkkfr582qjawhi0iw4xs82gmjjbg5rigv")))

(define-public crate-xiangyun-0.2 (crate (name "xiangyun") (vers "0.2.6") (hash "0w4snqvk4jbb3vwwx9nvif6bp3dynl4j4myd03dxpfpcvr9jldac")))

(define-public crate-xiangyun-0.2 (crate (name "xiangyun") (vers "0.2.7") (hash "18f8067x4d7w35dwvwss00041f602m26ydw8jhl4y4iwqdxxl9z6")))

