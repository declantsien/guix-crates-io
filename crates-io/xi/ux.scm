(define-module (crates-io xi ux) #:use-module (crates-io))

(define-public crate-xiuxiu-echor-0.1 (crate (name "xiuxiu-echor") (vers "0.1.0") (deps (list (crate-dep (name "assert_cmd") (req "^2") (default-features #t) (kind 2)) (crate-dep (name "clap") (req "^2.33") (default-features #t) (kind 0)) (crate-dep (name "predicates") (req "^2") (default-features #t) (kind 2)))) (hash "07x3qm8qfcwd9sgiwvzxf7bignixss8b1yxcqpi0dqz10mw05rli")))

