(define-module (crates-io xi fe) #:use-module (crates-io))

(define-public crate-xifeng-0.0.1 (crate (name "xifeng") (vers "0.0.1") (hash "09agdj1qw0lx78jcigv3b3sl5sxj1nba24af9m38paxdr7lmck7m")))

(define-public crate-xifetch-0.1 (crate (name "xifetch") (vers "0.1.0") (hash "05i11i7sgh59wq075cr0vgvv0g4qkcbz7yd1ah34yxsfn1rfvccq")))

(define-public crate-xifetch-0.1 (crate (name "xifetch") (vers "0.1.1") (hash "0flnyaz2c8qq3js9ihzsyj5frmmbsbih3lx9l36yjsbxaank9phs")))

(define-public crate-xifetch-0.1 (crate (name "xifetch") (vers "0.1.2") (hash "1vm19x5rnz48v5vxlwigw74nw1mfil5w4ww5kx1xia57r27w7arq")))

(define-public crate-xifetch-0.1 (crate (name "xifetch") (vers "0.1.3") (hash "1ajj4x6mp9hypkb55n997nl1icgpli3q9ijx3nz8xi4hb8x0mchb")))

(define-public crate-xifetch-0.1 (crate (name "xifetch") (vers "0.1.4") (hash "041i73fgv52bldk36qbgs82nhk23bs9vdlfnnkz0bz5fxyc593g8")))

(define-public crate-xifetch-2 (crate (name "xifetch") (vers "2.0.0") (deps (list (crate-dep (name "colored") (req "^2.0.0") (default-features #t) (kind 0)))) (hash "0m2gislz1nyr6b9fmm7yjy30bh7pkhlb1yz0nk64q86qx6nppz5l")))

(define-public crate-xifetch-2 (crate (name "xifetch") (vers "2.0.1") (deps (list (crate-dep (name "colored") (req "^2.0.0") (default-features #t) (kind 0)))) (hash "1l255pf3bx3mc08w841hla19l5764qc50lg5fjj0cj5k8dj9g8k0")))

