(define-module (crates-io xi le) #:use-module (crates-io))

(define-public crate-xilem-0.1 (crate (name "xilem") (vers "0.1.0") (deps (list (crate-dep (name "accesskit") (req "^0.14.0") (default-features #t) (kind 0)) (crate-dep (name "accesskit_winit") (req "^0.20.0") (default-features #t) (kind 0)) (crate-dep (name "masonry") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "smallvec") (req "^1.13.2") (default-features #t) (kind 0)) (crate-dep (name "tracing") (req "^0.1.40") (default-features #t) (kind 0)) (crate-dep (name "vello") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "winit") (req "^0.30.0") (default-features #t) (kind 0)))) (hash "1vq3ns037f5c0vx69j1ik0ywnpm100k55qzjb4mr52n7dzsz9xs5")))

