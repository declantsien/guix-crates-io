(define-module (crates-io xi ng) #:use-module (crates-io))

(define-public crate-xingapi-0.0.0 (crate (name "xingapi") (vers "0.0.0") (hash "085sxska0bgjrnq1gdjxzymlpazgp5aqlndr2kiv8y7igi4b1xaj") (yanked #t)))

(define-public crate-xingapi-res-0.0.0 (crate (name "xingapi-res") (vers "0.0.0") (hash "12chc0j3w5c6pc9jysxxvyphpjklz6lhp5ya0p5az6abbrfmd0ly") (yanked #t)))

(define-public crate-xingke_minigrep-0.1 (crate (name "xingke_minigrep") (vers "0.1.0") (hash "0dddvq3la8cqy04ii716f8zsc5q4n1flrygvkrv6rynfnrqh2c01")))

(define-public crate-xingwei-0.1 (crate (name "xingwei") (vers "0.1.0") (hash "1gak3yxhn15ig07d7lmn9vg02c1bl070wb3r8lm2jq174fy9ni80")))

