(define-module (crates-io br di) #:use-module (crates-io))

(define-public crate-brdiff-0.1 (crate (name "brdiff") (vers "0.1.0") (deps (list (crate-dep (name "image") (req "^0.23") (default-features #t) (kind 0)))) (hash "1n4zkvmamabdikqfbfqh8ksaxbmx9klpjm7cfmlvflia70s0im2l")))

(define-public crate-brdiff-0.2 (crate (name "brdiff") (vers "0.2.0") (deps (list (crate-dep (name "image") (req "^0.23") (default-features #t) (kind 0)))) (hash "0qh5b3jzf01kfc9af4rsrnn52rqbdrfdmwz1s4714kcr1gvbndj7")))

