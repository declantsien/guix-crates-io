(define-module (crates-io br in) #:use-module (crates-io))

(define-public crate-brine-0.1 (crate (name "brine") (vers "0.1.0") (hash "1vihz079gfb9afigzzyaf72g176hnmmq20lf9x1whs6s5ggml2ci")))

(define-public crate-bring-0.0.1 (crate (name "bring") (vers "0.0.1") (hash "1h1rmj1np69iyddg9lwwryxdg0zjp1ikqb0d86krdqk0blkkp17x")))

(define-public crate-brinicle_deinterleaved-1 (crate (name "brinicle_deinterleaved") (vers "1.0.0") (deps (list (crate-dep (name "smallvec") (req "^0.6") (default-features #t) (kind 0)))) (hash "0zm2kd9ycksyyav90snz1pid6krwfshvdcxyf9lz335m1v4307g0")))

(define-public crate-brinicle_glue-1 (crate (name "brinicle_glue") (vers "1.0.0") (deps (list (crate-dep (name "brinicle_kernel") (req "^1.0.0") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "smallvec") (req "^0.6") (default-features #t) (kind 0)))) (hash "1dzdk69si77nfd78q2z9170jidx720n4gj55anj2hp7pwayp14hi")))

(define-public crate-brinicle_kernel-1 (crate (name "brinicle_kernel") (vers "1.0.0") (deps (list (crate-dep (name "brinicle_deinterleaved") (req "^1.0.0") (default-features #t) (kind 0)))) (hash "0b39arliq2wryzm9qzak2bv7hk33npf3km8xvyppyv9da6lw81xm")))

(define-public crate-brinicle_midi-1 (crate (name "brinicle_midi") (vers "1.0.0") (hash "0fi1qxvz2dqv9harlvcrzq3zsymldmhm3ylrsq9nlpq1prplhhhm")))

(define-public crate-brinicle_pitch-1 (crate (name "brinicle_pitch") (vers "1.0.0") (deps (list (crate-dep (name "log") (req "^0.3.2") (default-features #t) (kind 0)))) (hash "15dmr5f6n1snxkjf6b33b02fcxp8d35fgi69hl9h3ngl6wbvx3x3")))

(define-public crate-brinicle_voices-1 (crate (name "brinicle_voices") (vers "1.0.0") (deps (list (crate-dep (name "bit-set") (req "^0.2.0") (default-features #t) (kind 0)))) (hash "0852qnsis9p3yqy4mf50varfz3rc9h1qk8i9142jzrl4mwdp4np9")))

