(define-module (crates-io br -m) #:use-module (crates-io))

(define-public crate-br-maths-0.1 (crate (name "br-maths") (vers "0.1.1") (deps (list (crate-dep (name "json") (req "^0.12.4") (default-features #t) (kind 0)))) (hash "1i61jfnidl1p745kcjc75dpp3iyhcwdrf8izdjy9giamqwyy1ndb")))

(define-public crate-br-maths-0.1 (crate (name "br-maths") (vers "0.1.2") (deps (list (crate-dep (name "json") (req "^0.12.4") (default-features #t) (kind 0)))) (hash "1gc1j3rw8l7mkxli2qqw5ziyj7wagacvs9hh2azn47c9xxnxmv3w")))

(define-public crate-br-maths-0.1 (crate (name "br-maths") (vers "0.1.3") (deps (list (crate-dep (name "json") (req "^0.12.4") (default-features #t) (kind 0)))) (hash "17ydgw2zwl6n2zl0dkk0wrxb6wldczbli7kh412c271xcypkfzcd")))

(define-public crate-br-maths-0.1 (crate (name "br-maths") (vers "0.1.4") (deps (list (crate-dep (name "json") (req "^0.12.4") (default-features #t) (kind 0)))) (hash "1ar6sy7dkl15k97l5219qw0a14f5vy111447bsr0c00dgvd4jnd6")))

(define-public crate-br-maths-0.1 (crate (name "br-maths") (vers "0.1.5") (deps (list (crate-dep (name "json") (req "^0.12.4") (default-features #t) (kind 0)))) (hash "04irhk18jjab50dr8mc0fpgssnw9v1zcvgvbrl14gwxpw0a32r4s")))

(define-public crate-br-maths-0.1 (crate (name "br-maths") (vers "0.1.6") (deps (list (crate-dep (name "json") (req "^0.12.4") (default-features #t) (kind 0)))) (hash "0wl7jzkd62ya3s5is1w0n1qghmzvx4ajv2ybphiy124lavclpr1h")))

(define-public crate-br-maths-0.1 (crate (name "br-maths") (vers "0.1.8") (deps (list (crate-dep (name "json") (req "^0.12.4") (default-features #t) (kind 0)))) (hash "0n0j1adysawvzbwkfr2d146fgngrkzr5s92cssw6ffll9p2qqcha")))

(define-public crate-br-maths-0.1 (crate (name "br-maths") (vers "0.1.9") (deps (list (crate-dep (name "json") (req "^0.12.4") (default-features #t) (kind 0)))) (hash "1z71pkh7q965dyh3mcf77j1qddbybbsvh1zrwzgab9kxc7a25j5j")))

(define-public crate-br-maths-0.1 (crate (name "br-maths") (vers "0.1.10") (deps (list (crate-dep (name "json") (req "^0.12.4") (default-features #t) (kind 0)))) (hash "0kgq11w44ymnvijz28j3rj9l2caj30v3pkg1ya21bjc6bqsiywh5")))

(define-public crate-br-maths-0.1 (crate (name "br-maths") (vers "0.1.11") (deps (list (crate-dep (name "json") (req "^0.12.4") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.21") (default-features #t) (kind 0)))) (hash "0sw3rmyw25yvl3w61l6nb6nq5jlfh5snj4l07zxz03h9kyzwwkjz")))

(define-public crate-br-mysql-0.0.1 (crate (name "br-mysql") (vers "0.0.1") (deps (list (crate-dep (name "env_logger") (req "^0.10.1") (default-features #t) (kind 2)) (crate-dep (name "hex") (req "^0.4.3") (default-features #t) (kind 0)) (crate-dep (name "json") (req "^0.12.4") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.20") (default-features #t) (kind 0)) (crate-dep (name "sha1") (req "^0.10.6") (default-features #t) (kind 0)) (crate-dep (name "sha2") (req "^0.10.8") (default-features #t) (kind 0)))) (hash "0qlwn6hgg5wa6d6zzqnsp3dg0lshz821qjljx2m7afcdhnw1wby9")))

(define-public crate-br-mysql-0.0.2 (crate (name "br-mysql") (vers "0.0.2") (deps (list (crate-dep (name "env_logger") (req "^0.10.1") (default-features #t) (kind 2)) (crate-dep (name "hex") (req "^0.4.3") (default-features #t) (kind 0)) (crate-dep (name "json") (req "^0.12.4") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.20") (default-features #t) (kind 0)) (crate-dep (name "sha1") (req "^0.10.6") (default-features #t) (kind 0)) (crate-dep (name "sha2") (req "^0.10.8") (default-features #t) (kind 0)))) (hash "1jfj9i54xi83ddppfm2ayj93625q2l4sg4jpw59kkb95wdkjmqma")))

(define-public crate-br-mysql-0.0.3 (crate (name "br-mysql") (vers "0.0.3") (deps (list (crate-dep (name "env_logger") (req "^0.10.1") (default-features #t) (kind 2)) (crate-dep (name "hex") (req "^0.4.3") (default-features #t) (kind 0)) (crate-dep (name "json") (req "^0.12.4") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.20") (default-features #t) (kind 0)) (crate-dep (name "sha1") (req "^0.10.6") (default-features #t) (kind 0)) (crate-dep (name "sha2") (req "^0.10.8") (default-features #t) (kind 0)))) (hash "1gavsq2ldb7kmyb9kgsf0qgnpiy88gwf7kpsa8cqv47yh40c0cw0")))

(define-public crate-br-mysql-0.0.4 (crate (name "br-mysql") (vers "0.0.4") (deps (list (crate-dep (name "env_logger") (req "^0.10.1") (default-features #t) (kind 2)) (crate-dep (name "hex") (req "^0.4.3") (default-features #t) (kind 0)) (crate-dep (name "json") (req "^0.12.4") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.20") (default-features #t) (kind 0)) (crate-dep (name "sha1") (req "^0.10.6") (default-features #t) (kind 0)) (crate-dep (name "sha2") (req "^0.10.8") (default-features #t) (kind 0)))) (hash "14qxz1g3z3hrllfx9x76rpq64cl7kdfgpzk0455sr93aq7kjrmr6")))

