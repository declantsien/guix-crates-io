(define-module (crates-io br ts) #:use-module (crates-io))

(define-public crate-brtsky-0.1 (crate (name "brtsky") (vers "0.1.0") (deps (list (crate-dep (name "chrono") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)))) (hash "1fsqd4imdg2p89aai9191h5xx11x8fj6bxalrzwj1dz742rm97rm")))

(define-public crate-brtsky-0.1 (crate (name "brtsky") (vers "0.1.1") (deps (list (crate-dep (name "chrono") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)))) (hash "0ixfqal8qrjgq1nx8vab02f9n1xrz2d0m9hg4ikaprcsypzlw2kh")))

