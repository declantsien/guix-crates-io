(define-module (crates-io br -k) #:use-module (crates-io))

(define-public crate-br-kafka-0.0.1 (crate (name "br-kafka") (vers "0.0.1") (deps (list (crate-dep (name "env_logger") (req "^0.10.0") (default-features #t) (kind 2)) (crate-dep (name "json") (req "^0.12.4") (default-features #t) (kind 0)) (crate-dep (name "kafka") (req "^0.9.0") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.17") (default-features #t) (kind 0)))) (hash "10hvssjqfawkrp0lmxyma74fl2xpb17laaxp4i9gpd9vsjj1f0j6")))

(define-public crate-br-kafka-0.0.2 (crate (name "br-kafka") (vers "0.0.2") (deps (list (crate-dep (name "env_logger") (req "^0.10.0") (default-features #t) (kind 2)) (crate-dep (name "json") (req "^0.12.4") (default-features #t) (kind 0)) (crate-dep (name "kafka") (req "^0.9.0") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.17") (default-features #t) (kind 0)))) (hash "02hd7b3ycj09cl9vld333wfvimmwr73qhf3z0nnxr53mrcr5gy30")))

