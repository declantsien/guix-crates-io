(define-module (crates-io br ut) #:use-module (crates-io))

(define-public crate-brutal-types-0.1 (crate (name "brutal-types") (vers "0.1.0") (hash "1ypisp1zyqfpsipialvfq6mvicagqm748pwd65a1y8pnqph9bm3w")))

(define-public crate-brute-bits-0.1 (crate (name "brute-bits") (vers "0.1.0") (hash "14d0kramkqgp3mwd3hxcyk06dps3lckvl8c4sh7kdl9lrra5y385")))

(define-public crate-brute-force-0.1 (crate (name "brute-force") (vers "0.1.0") (deps (list (crate-dep (name "blake2") (req "^0.9.1") (default-features #t) (kind 2)) (crate-dep (name "crossbeam-channel") (req "^0.5.0") (default-features #t) (kind 0)) (crate-dep (name "crossbeam-utils") (req "^0.8.1") (default-features #t) (kind 0)) (crate-dep (name "curve25519-dalek") (req "^3.0.2") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "hex") (req "^0.4.2") (default-features #t) (kind 2)) (crate-dep (name "log") (req "^0.4.14") (default-features #t) (kind 0)) (crate-dep (name "num_cpus") (req "^1.13.0") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.7") (optional #t) (default-features #t) (kind 0)))) (hash "0sj15vqrp1rw9s0akx7blhwjdxxzc8x73510dxqfrlq3xq2ynwiq") (features (quote (("nightly") ("curve25519" "curve25519-dalek" "rand"))))))

(define-public crate-brute-force-0.1 (crate (name "brute-force") (vers "0.1.1") (deps (list (crate-dep (name "blake2") (req "^0.9.1") (default-features #t) (kind 2)) (crate-dep (name "crossbeam-channel") (req "^0.5.0") (default-features #t) (kind 0)) (crate-dep (name "crossbeam-utils") (req "^0.8.1") (default-features #t) (kind 0)) (crate-dep (name "curve25519-dalek") (req "^3.0.2") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "hex") (req "^0.4.2") (default-features #t) (kind 2)) (crate-dep (name "log") (req "^0.4.14") (default-features #t) (kind 0)) (crate-dep (name "num_cpus") (req "^1.13.0") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.7") (optional #t) (default-features #t) (kind 0)))) (hash "1b1ryg6m0qp328ljrsjmzjjw2knm068pklncxmvv7f5himkvznay") (features (quote (("nightly") ("curve25519" "curve25519-dalek" "rand"))))))

(define-public crate-brute-force-0.2 (crate (name "brute-force") (vers "0.2.0") (deps (list (crate-dep (name "blake2") (req "^0.9.1") (default-features #t) (kind 2)) (crate-dep (name "crossbeam-channel") (req "^0.5.0") (default-features #t) (kind 0)) (crate-dep (name "crossbeam-utils") (req "^0.8.1") (default-features #t) (kind 0)) (crate-dep (name "curve25519-dalek") (req "^4.0.1") (optional #t) (default-features #t) (kind 0) (package "curve25519-dalek-ng")) (crate-dep (name "hex") (req "^0.4.2") (default-features #t) (kind 2)) (crate-dep (name "log") (req "^0.4.14") (default-features #t) (kind 0)) (crate-dep (name "num_cpus") (req "^1.13.0") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.1") (optional #t) (default-features #t) (kind 0)))) (hash "0463k0gjhgw13fdmc0w81shb3vg8xd1zwxj5zcy77759zcvpib4j") (features (quote (("nightly") ("curve25519" "curve25519-dalek" "rand"))))))

(define-public crate-brute-force-print-0.1 (crate (name "brute-force-print") (vers "0.1.0") (deps (list (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)))) (hash "15wqq3xl3k8s038bby4bzmq5icw4pjrkghfz1yll44a50jpnvlrc")))

(define-public crate-brute_forcing-0.1 (crate (name "brute_forcing") (vers "0.1.0") (hash "0234acvjzjb9n0mxy7x8m09hs0j8n8vh2yyxv2m0gjzipiq8jwj8")))

(define-public crate-brute_forcing-0.1 (crate (name "brute_forcing") (vers "0.1.1") (deps (list (crate-dep (name "rayon") (req "^1.5.3") (default-features #t) (kind 0)))) (hash "0gzj88v3jd6ki7xyl3v0pp6zm22hn949hgf8afmc3l46r0wc9gl7")))

(define-public crate-bruteforce-0.1 (crate (name "bruteforce") (vers "0.1.0") (deps (list (crate-dep (name "no-std-compat") (req "^0.2.0") (features (quote ("alloc"))) (default-features #t) (kind 0)))) (hash "03726bycs53gfnlbcf7jdi6m7qv9zibvz2myppk2m6agq55g2fy7") (features (quote (("std" "no-std-compat/std") ("default" "std" "constants") ("constants"))))))

(define-public crate-bruteforce-0.1 (crate (name "bruteforce") (vers "0.1.1") (deps (list (crate-dep (name "no-std-compat") (req "^0.2.0") (features (quote ("alloc"))) (default-features #t) (kind 0)))) (hash "1kkn18ilck1d854j01h9z5z5532k4spcpn661ngb8yf6my04wfi3") (features (quote (("std" "no-std-compat/std") ("default" "std" "constants") ("constants"))))))

(define-public crate-bruteforce-0.1 (crate (name "bruteforce") (vers "0.1.2") (deps (list (crate-dep (name "no-std-compat") (req "^0.2.0") (features (quote ("alloc"))) (default-features #t) (kind 0)))) (hash "19qvylkzv0hwls760n4l3h0kppjm6vgdzr0wkqniq4b00ff73kd0") (features (quote (("std" "no-std-compat/std") ("default" "std" "constants") ("constants"))))))

(define-public crate-bruteforce-0.1 (crate (name "bruteforce") (vers "0.1.3") (deps (list (crate-dep (name "no-std-compat") (req "^0.2.0") (features (quote ("alloc"))) (default-features #t) (kind 0)))) (hash "1zzmb4vrwnvs48xchx5ga82p7319jfrvlxkm1a2n1pyi6nm5zfgh") (features (quote (("std" "no-std-compat/std") ("default" "std" "constants") ("constants"))))))

(define-public crate-bruteforce-0.1 (crate (name "bruteforce") (vers "0.1.4") (deps (list (crate-dep (name "no-std-compat") (req "^0.2.0") (features (quote ("alloc"))) (default-features #t) (kind 0)))) (hash "1vk7i8wxw1kwb4bim36bf6h89ki2w753rm57qji97iln6x5lydp3") (features (quote (("std" "no-std-compat/std") ("default" "std" "constants") ("constants"))))))

(define-public crate-bruteforce-0.1 (crate (name "bruteforce") (vers "0.1.5") (deps (list (crate-dep (name "no-std-compat") (req "^0.2.0") (features (quote ("alloc"))) (default-features #t) (kind 0)))) (hash "023r0fpb1c45c03qwbya5k662d0g4di22vd7s67cqj835dckvvin") (features (quote (("std" "no-std-compat/std") ("default" "std" "constants") ("constants"))))))

(define-public crate-bruteforce-0.1 (crate (name "bruteforce") (vers "0.1.6") (deps (list (crate-dep (name "no-std-compat") (req "^0.2.0") (features (quote ("alloc"))) (default-features #t) (kind 0)))) (hash "17iba4lhgblag07b8s09a25ldqsqffhsq0iawhq9abd3jjx8f2xs") (features (quote (("std" "no-std-compat/std") ("default" "std" "constants") ("constants"))))))

(define-public crate-bruteforce-0.1 (crate (name "bruteforce") (vers "0.1.7") (deps (list (crate-dep (name "no-std-compat") (req "^0.3.0") (features (quote ("alloc"))) (default-features #t) (kind 0)))) (hash "0rg43pgly94ca3rm6nn9d5j5wl60bxhd8xfb2qg9sb63z6lrl8dq") (features (quote (("std" "no-std-compat/std" "no-std-compat/unstable") ("generators") ("default" "std" "constants") ("constants"))))))

(define-public crate-bruteforce-0.2 (crate (name "bruteforce") (vers "0.2.0") (deps (list (crate-dep (name "bruteforce-macros") (req "^0.2.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "no-std-compat") (req "^0.3.0") (features (quote ("alloc"))) (default-features #t) (kind 0)))) (hash "03g3bww5ps4pxapwg4sz7vmdwliqik4033xksxb4v1rza1dcgv89") (features (quote (("std" "no-std-compat/std" "no-std-compat/unstable") ("generators") ("default" "std" "constants" "bruteforce-macros") ("constants"))))))

(define-public crate-bruteforce-0.2 (crate (name "bruteforce") (vers "0.2.1") (deps (list (crate-dep (name "bruteforce-macros") (req "^0.2.1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "criterion") (req "^0.3.6") (default-features #t) (kind 2)) (crate-dep (name "no-std-compat") (req "^0.4.1") (features (quote ("alloc"))) (default-features #t) (kind 0)))) (hash "02crb90zvv6x36as6jn2kfa1b5skjy2y1avvlw7km9accsx3smfc") (features (quote (("std" "no-std-compat/std" "no-std-compat/unstable") ("generators") ("default" "std" "constants" "bruteforce-macros") ("constants"))))))

(define-public crate-bruteforce-0.3 (crate (name "bruteforce") (vers "0.3.0") (deps (list (crate-dep (name "bruteforce-macros") (req "^0.3.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "criterion") (req "^0.5.1") (default-features #t) (kind 2)) (crate-dep (name "no-std-compat") (req "^0.4.1") (features (quote ("alloc"))) (default-features #t) (kind 0)))) (hash "121nich2qn9i4zqbwnij8kxkcz6pvn90blv9nm0cbnxdmjcinh8v") (features (quote (("std" "no-std-compat/std" "no-std-compat/unstable") ("generators") ("default" "std" "constants" "bruteforce-macros") ("constants"))))))

(define-public crate-bruteforce-macros-0.2 (crate (name "bruteforce-macros") (vers "0.2.0") (deps (list (crate-dep (name "no-std-compat") (req "^0.3.0") (features (quote ("alloc"))) (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)))) (hash "10zrch5yl1ssk1ixhwdxrwb3z9m23if0zyjrzhnqjldjnici21pr") (features (quote (("std" "no-std-compat/std" "no-std-compat/unstable") ("default" "std"))))))

(define-public crate-bruteforce-macros-0.2 (crate (name "bruteforce-macros") (vers "0.2.1") (deps (list (crate-dep (name "no-std-compat") (req "^0.4.1") (features (quote ("alloc"))) (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.21") (default-features #t) (kind 0)))) (hash "1zddc7nmkwvxx6x7nk7rfvqb3x33gbabqsgdgh0w972p1l22cvzh") (features (quote (("std" "no-std-compat/std" "no-std-compat/unstable") ("default" "std"))))))

(define-public crate-bruteforce-macros-0.3 (crate (name "bruteforce-macros") (vers "0.3.0") (deps (list (crate-dep (name "no-std-compat") (req "^0.4.1") (features (quote ("alloc"))) (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.31") (default-features #t) (kind 0)))) (hash "12q7856m6jn59rkfgngbc0q49w8qc8lfl70i2x0fkbzyw0j7rxjs") (features (quote (("std" "no-std-compat/std" "no-std-compat/unstable") ("default" "std"))))))

(define-public crate-bruteforus-0.1 (crate (name "bruteforus") (vers "0.1.0") (deps (list (crate-dep (name "chrono") (req "^0.4.19") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^2.33.3") (features (quote ("suggestions" "color" "wrap_help"))) (kind 0)) (crate-dep (name "clap") (req "^2.33.3") (default-features #t) (kind 1)) (crate-dep (name "colored") (req "^2.0.0") (default-features #t) (kind 0)) (crate-dep (name "indicatif") (req "^0.15.0") (default-features #t) (kind 0)) (crate-dep (name "multiqueue") (req "^0.3.2") (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.10.4") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^0.2.21") (features (quote ("macros"))) (default-features #t) (kind 0)))) (hash "0vmxp2z4nvgpwxx6c8wa0kqdai9np7kcg73kahx4h2dz315izxph")))

(define-public crate-brutemoji-0.1 (crate (name "brutemoji") (vers "0.1.0") (hash "1n9lj7fv3asv61fgap0141hkya84jflxs5yydabpmng59a94bn8c")))

(define-public crate-brutemoji-0.2 (crate (name "brutemoji") (vers "0.2.0") (deps (list (crate-dep (name "image") (req "^0.23.8") (features (quote ("gif" "jpeg" "png" "webp"))) (kind 0)) (crate-dep (name "rand") (req "^0.7.3") (default-features #t) (kind 0)))) (hash "1qkvv4676qcc6zgyh04g5crz5ybxxzh0rh8icfalr7nkwrvzx8qm")))

(define-public crate-brutemoji-0.3 (crate (name "brutemoji") (vers "0.3.0") (deps (list (crate-dep (name "image") (req "^0.23.8") (features (quote ("gif" "jpeg" "png" "webp"))) (kind 0)) (crate-dep (name "rand") (req "^0.7.3") (default-features #t) (kind 0)))) (hash "15m34gba19nv2vbylshirwgipvqp7nc5gpj700ddajkcj944zhws")))

(define-public crate-brutemoji-0.3 (crate (name "brutemoji") (vers "0.3.1") (deps (list (crate-dep (name "image") (req "^0.23.8") (features (quote ("gif" "jpeg" "png" "webp"))) (kind 0)) (crate-dep (name "rand") (req "^0.7.3") (default-features #t) (kind 0)))) (hash "0nd1pai6ffpwj5jpfqyc9j270v693p5x101hxn62y6xfrbdd4n57")))

(define-public crate-brutemoji-0.3 (crate (name "brutemoji") (vers "0.3.2") (deps (list (crate-dep (name "image") (req "^0.23.8") (features (quote ("gif" "jpeg" "png" "webp"))) (kind 0)) (crate-dep (name "rand") (req "^0.7.3") (default-features #t) (kind 0)))) (hash "095si049306p04y8qg5aqgbmqk4hmvnw7rg2rafif8r7q10iqqxs")))

(define-public crate-brutemoji-0.4 (crate (name "brutemoji") (vers "0.4.0") (deps (list (crate-dep (name "image") (req "^0.23.8") (features (quote ("gif" "jpeg" "png" "webp"))) (kind 0)) (crate-dep (name "rand") (req "^0.7.3") (default-features #t) (kind 0)))) (hash "0cl4zblh90q08im2jqgmr7f1bs5jlipwqpp5vwlsi7dcv18qv7j4")))

(define-public crate-brutemoji-0.5 (crate (name "brutemoji") (vers "0.5.0") (deps (list (crate-dep (name "image") (req "^0.23.8") (features (quote ("gif" "jpeg" "png" "webp"))) (kind 0)) (crate-dep (name "rand") (req "^0.7.3") (default-features #t) (kind 0)))) (hash "0xdpd5n7h1cj7yrf8ay8maggdsdvdlgc3vd1nhc45i8fipcsgk98")))

(define-public crate-brutils-0.1 (crate (name "brutils") (vers "0.1.0") (deps (list (crate-dep (name "cc") (req "^1.0") (default-features #t) (kind 1)))) (hash "00lvx45nam4n6id9k00az2hqiwr0dzgyrnqgqy463avd174552d2")))

(define-public crate-brutils-0.1 (crate (name "brutils") (vers "0.1.1") (deps (list (crate-dep (name "cc") (req "^1.0") (default-features #t) (kind 1)))) (hash "12nkv0xxjyslglk2pm7py1aclgkqh9xwxhybj6b26z2cld2sqa2c")))

(define-public crate-brutils-0.1 (crate (name "brutils") (vers "0.1.2") (deps (list (crate-dep (name "libc") (req "^0.2.55") (default-features #t) (kind 0)))) (hash "04gzbpx61b3nf7s3r421dbkj27zfzsr2i6d9zdazcxmvzajdgrrq")))

(define-public crate-brutils-0.1 (crate (name "brutils") (vers "0.1.21") (deps (list (crate-dep (name "libc") (req "^0.2.55") (default-features #t) (kind 0)))) (hash "0vhlb56rmhkan8bax478vyfzw6hbs7r1c4s5z5bcf3264qxjgw8f")))

(define-public crate-brutils-0.1 (crate (name "brutils") (vers "0.1.22") (deps (list (crate-dep (name "libc") (req "^0.2.55") (default-features #t) (kind 0)))) (hash "1vj3hi8w07fi3s72bgpggjf137w4piwqifi3di01yx3n9ifxjrji")))

(define-public crate-brutils-0.1 (crate (name "brutils") (vers "0.1.3") (deps (list (crate-dep (name "libc") (req "^0.2.55") (default-features #t) (kind 0)))) (hash "1s5cr2xirii06ldyplwp0j6h0mrg5m7kas09nsajhms6qb1y5qfk")))

(define-public crate-brutils-0.1 (crate (name "brutils") (vers "0.1.31") (deps (list (crate-dep (name "libc") (req "^0.2.55") (default-features #t) (kind 0)))) (hash "1ys30ikdm6gfd8mww28f0ij32ngyicngkza5qwj6yxf5yggz6kmi")))

(define-public crate-brutils-0.1 (crate (name "brutils") (vers "0.1.4") (deps (list (crate-dep (name "libc") (req "^0.2.55") (default-features #t) (kind 0)))) (hash "16xrf30mhdfyyhdkb960bl5rk68ihajwv7f595d7brqmyzndcjqn")))

(define-public crate-brutils-0.1 (crate (name "brutils") (vers "0.1.41") (deps (list (crate-dep (name "libc") (req "^0.2.55") (default-features #t) (kind 0)))) (hash "0419a6qyjykkr7n6qvf8585x9msc6jp4lcgfr6mxz8nars4mz7gv")))

(define-public crate-brutils-0.1 (crate (name "brutils") (vers "0.1.5") (deps (list (crate-dep (name "libc") (req "^0.2.55") (default-features #t) (kind 0)))) (hash "1w27910wj6y0v0vfdc39vfzs83q378jck267s4c4mxbr1fv8hq6r")))

(define-public crate-brutils-0.1 (crate (name "brutils") (vers "0.1.51") (deps (list (crate-dep (name "libc") (req "^0.2.55") (default-features #t) (kind 0)))) (hash "1f5h20i7md8w5a1kwm0ri4mssdfs00jpk4prkz0gd3s09hk52j9d")))

(define-public crate-bruto-0.1 (crate (name "bruto") (vers "0.1.0") (hash "05y38ylm563z50f234qhnpak93abswm303r0sv1p2gjlz1rgsrpz")))

(define-public crate-brutus-0.0.1 (crate (name "brutus") (vers "0.0.1") (hash "17yqiqbdsqbv7m1kf07d3lna7283vsrdry37dnk8glg4b304pnjb")))

