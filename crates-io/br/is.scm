(define-module (crates-io br is) #:use-module (crates-io))

(define-public crate-brisk-0.0.1 (crate (name "brisk") (vers "0.0.1") (hash "1fdrcyvqkmq373xy73xh63fbwij029smkyzm7bvslyxq309ijgwc") (yanked #t)))

(define-public crate-bristol-0.1 (crate (name "bristol") (vers "0.1.0") (hash "0xhz6yy8656f18dg4pcf3r3gymqbxlqrbjinp39sldaa0q3ls1gr")))

