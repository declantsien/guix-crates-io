(define-module (crates-io br -s) #:use-module (crates-io))

(define-public crate-br-serial-0.0.1 (crate (name "br-serial") (vers "0.0.1") (deps (list (crate-dep (name "env_logger") (req "^0.10.0") (default-features #t) (kind 2)) (crate-dep (name "hex") (req "^0.4.3") (default-features #t) (kind 0)) (crate-dep (name "json") (req "^0.12.4") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.18") (default-features #t) (kind 0)) (crate-dep (name "serial2") (req "^0.2.1") (default-features #t) (kind 0)) (crate-dep (name "serialport") (req "^4.2.0") (default-features #t) (kind 0)))) (hash "07ymgdfwk2bn928hafn08wqva27rwhvwp1qrv164qg4p8rvay1d0")))

(define-public crate-br-system-0.0.1 (crate (name "br-system") (vers "0.0.1") (deps (list (crate-dep (name "env_logger") (req "^0.11.3") (default-features #t) (kind 2)) (crate-dep (name "log") (req "^0.4.21") (default-features #t) (kind 2)))) (hash "06s9xdfqslgbcy9w6g8x0vfxgnrhc3xvb78fiq7ffnlqai9w8mga")))

(define-public crate-br-system-0.0.2 (crate (name "br-system") (vers "0.0.2") (deps (list (crate-dep (name "env_logger") (req "^0.11.3") (default-features #t) (kind 2)) (crate-dep (name "log") (req "^0.4.21") (default-features #t) (kind 2)))) (hash "1ig0ark1jg01jl6ifq5m1wqlwfwi7rrgc982gyiln7xx2qsd0hrq")))

(define-public crate-br-system-0.0.3 (crate (name "br-system") (vers "0.0.3") (deps (list (crate-dep (name "env_logger") (req "^0.11.3") (default-features #t) (kind 2)) (crate-dep (name "log") (req "^0.4.21") (default-features #t) (kind 2)))) (hash "0nllxl4y70kwrlf6aqjffzbrdp5b9121ym4fwybfjfx95siqph0x")))

(define-public crate-br-system-0.0.4 (crate (name "br-system") (vers "0.0.4") (deps (list (crate-dep (name "env_logger") (req "^0.11.3") (default-features #t) (kind 2)) (crate-dep (name "log") (req "^0.4.21") (default-features #t) (kind 2)) (crate-dep (name "regex") (req "^1.10.4") (default-features #t) (kind 0)) (crate-dep (name "sysinfo") (req "^0.30.12") (default-features #t) (kind 0)))) (hash "08b44i7g9bxzqn366jpkl826fhmhcb3mb3bf4cidym7vxixdx6v5")))

