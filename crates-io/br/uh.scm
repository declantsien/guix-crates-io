(define-module (crates-io br uh) #:use-module (crates-io))

(define-public crate-bruh-0.1 (crate (name "bruh") (vers "0.1.0") (deps (list (crate-dep (name "flate2") (req "^1.0.20") (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.11") (features (quote ("blocking"))) (default-features #t) (kind 0)) (crate-dep (name "sha2") (req "^0.9.3") (default-features #t) (kind 0)) (crate-dep (name "tar") (req "^0.4.32") (default-features #t) (kind 0)) (crate-dep (name "toml") (req "^0.5.8") (default-features #t) (kind 0)))) (hash "1svayzdqq1pq9wkgnidlf3jd7hyygw7q64lq6y0q6rjfllrsvzbz") (yanked #t)))

(define-public crate-bruh_moment-0.1 (crate (name "bruh_moment") (vers "0.1.0") (deps (list (crate-dep (name "anyhow") (req "^1.0.65") (default-features #t) (kind 0)))) (hash "0kx7s37kbfzki804bfjjhiyzbn6q5cwgy6p04y5zgdy6rvrmsayx")))

(define-public crate-bruh_moment-0.1 (crate (name "bruh_moment") (vers "0.1.1") (deps (list (crate-dep (name "anyhow") (req "^1.0.65") (default-features #t) (kind 0)))) (hash "0js8jbvlhxh77s7jnbglfhnadaa7dzkz6i99ckabfpq66hln6i65")))

