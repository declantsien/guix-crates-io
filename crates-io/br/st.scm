(define-module (crates-io br st) #:use-module (crates-io))

(define-public crate-brstm-0.1 (crate (name "brstm") (vers "0.1.0") (deps (list (crate-dep (name "binrw") (req "^0.10.0") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.37") (default-features #t) (kind 0)))) (hash "0wga9zfx2i18hdycml42jrlxh6ylncndvhlfxw6i8rc66fa7mzbr")))

(define-public crate-brstm-0.2 (crate (name "brstm") (vers "0.2.0") (deps (list (crate-dep (name "binrw") (req "^0.10.0") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.37") (default-features #t) (kind 0)))) (hash "07z65klrazwf8zdm45lqwqjbiz6ngyzpidckls3rb41zr97n1376")))

(define-public crate-brstm-0.3 (crate (name "brstm") (vers "0.3.0") (deps (list (crate-dep (name "binrw") (req "^0.10.0") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.37") (default-features #t) (kind 0)))) (hash "06g6fiy7qi23xhys5gpf23cmdm4i78i8w9v2zc7qnw4hzy9gg7gk")))

(define-public crate-brstm-0.4 (crate (name "brstm") (vers "0.4.0") (deps (list (crate-dep (name "binrw") (req "^0.11.1") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.39") (default-features #t) (kind 0)))) (hash "1zc9fzy13fk0hnwy4c5wpgqn57azkq3sv68gm5sry6kmpz7aaa4h")))

(define-public crate-brstm-0.4 (crate (name "brstm") (vers "0.4.1") (deps (list (crate-dep (name "binrw") (req "^0.11.1") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.40") (default-features #t) (kind 0)))) (hash "031gcdw5g5jhia2s2gii92qbl27fx4fsr8frj9h130yzkaa99baz")))

(define-public crate-brstm-0.4 (crate (name "brstm") (vers "0.4.2") (deps (list (crate-dep (name "binrw") (req "^0.11.1") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.40") (default-features #t) (kind 0)))) (hash "0d2jn2bd7zmrph9xrxdpv1g9c89a380wpg3hi473fdwx1fp6rvw3")))

