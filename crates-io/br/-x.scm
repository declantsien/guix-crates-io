(define-module (crates-io br -x) #:use-module (crates-io))

(define-public crate-br-xml-0.0.1 (crate (name "br-xml") (vers "0.0.1") (deps (list (crate-dep (name "env_logger") (req "^0.10.0") (default-features #t) (kind 2)) (crate-dep (name "json") (req "^0.12.4") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.17") (default-features #t) (kind 0)) (crate-dep (name "xml2json-rs") (req "^1.0.1") (default-features #t) (kind 0)))) (hash "0i196a75zj7n2l74j8vq24wfxdvkxgk232fn5k4dfk3ndj6f2vlm")))

