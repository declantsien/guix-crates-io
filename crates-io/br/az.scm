(define-module (crates-io br az) #:use-module (crates-io))

(define-public crate-brazier-0.0.1 (crate (name "brazier") (vers "0.0.1") (deps (list (crate-dep (name "async-trait") (req "^0.1.52") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1") (features (quote ("macros" "rt-multi-thread"))) (default-features #t) (kind 2)))) (hash "0lvkrg8542r0larqf9ympb2c5gqck1sskkainms3g5ixdczcbqzs")))

(define-public crate-brazier-0.1 (crate (name "brazier") (vers "0.1.0") (deps (list (crate-dep (name "async-trait") (req "^0.1.68") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1") (features (quote ("macros" "rt-multi-thread"))) (default-features #t) (kind 2)))) (hash "1dybcgp7csf5nj0z8ylwp4yrx4bn4iyv3wy4yylmjaj318fjhimy")))

