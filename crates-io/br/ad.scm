(define-module (crates-io br ad) #:use-module (crates-io))

(define-public crate-brado-0.1 (crate (name "brado") (vers "0.1.0") (hash "05qf9z449362y33lz2y6sbj86mrbcwjzk799brzkdsrh23s0g918")))

(define-public crate-brado-0.2 (crate (name "brado") (vers "0.2.0") (hash "1lav8xhrfsqvs5bhdx8sglhnwqlxl3pl40bn43v20n9csn3285dr")))

(define-public crate-brado-0.3 (crate (name "brado") (vers "0.3.0") (deps (list (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)))) (hash "0w0z8ycp8ahhc64i9q0vlp36c9miwcj56rm29dpvdqlqj4j41bw2")))

(define-public crate-brado-0.3 (crate (name "brado") (vers "0.3.1") (deps (list (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)))) (hash "1pma96w97q058cndl4w9rns1jxsxq8l9pb3b7gip7gag06q4mfx4")))

(define-public crate-brado-0.4 (crate (name "brado") (vers "0.4.0") (deps (list (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)))) (hash "1pk5m6q53is9vpb9p7vm2625ghfgwbb2cb49i6nd27s7dxi4rizd")))

(define-public crate-brads_leet_code_string_to_integer-0.1 (crate (name "brads_leet_code_string_to_integer") (vers "0.1.0") (hash "1nz3633jyzprzfi2cc7kannyk8d5nl95aci7bv7n8mmj1ccydaqb")))

(define-public crate-brads_leet_code_string_to_integer-0.1 (crate (name "brads_leet_code_string_to_integer") (vers "0.1.1") (hash "197qv0clims998cwnvzqxkl1f74n1wkq2bsiywv28ikl9v71zmi9")))

