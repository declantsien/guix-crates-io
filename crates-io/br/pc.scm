(define-module (crates-io br pc) #:use-module (crates-io))

(define-public crate-brpc-build-0.1 (crate (name "brpc-build") (vers "0.1.0-alpha") (deps (list (crate-dep (name "cc") (req "^1.0.38") (default-features #t) (kind 0)) (crate-dep (name "prost-build") (req "^0.5.0") (default-features #t) (kind 0)))) (hash "0wcsk61hw0dyxf0blhqz7d0wmxax8jgx6lc58k5mzp5jxsdhamx0") (yanked #t)))

(define-public crate-brpc-build-0.1 (crate (name "brpc-build") (vers "0.1.0") (deps (list (crate-dep (name "cc") (req "^1.0.38") (default-features #t) (kind 0)) (crate-dep (name "prost-build") (req "^0.5.0") (default-features #t) (kind 0)))) (hash "0dk1vkcpfgc4a6721sn6ji8bczj6dzvkac1sr01p65s1p27xxvym")))

(define-public crate-brpc-protoc-plugin-0.1 (crate (name "brpc-protoc-plugin") (vers "0.1.0-alpha") (deps (list (crate-dep (name "cc") (req "^1.0.38") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2.60") (default-features #t) (kind 0)))) (hash "1p44qc19z5z9q1c2rx94jb3i91lza2wrfhz1hdd6lig9z80nkz1p") (yanked #t)))

(define-public crate-brpc-protoc-plugin-0.1 (crate (name "brpc-protoc-plugin") (vers "0.1.0-alpha1") (deps (list (crate-dep (name "cc") (req "^1.0.38") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2.60") (default-features #t) (kind 0)))) (hash "0qihxm9xzpfjgzl8gli2ppbbpiri019b7n82zr9x84hh0r4d0nzy") (yanked #t)))

(define-public crate-brpc-protoc-plugin-0.1 (crate (name "brpc-protoc-plugin") (vers "0.1.0-alpha2") (deps (list (crate-dep (name "cc") (req "^1.0.38") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2.60") (default-features #t) (kind 0)))) (hash "0jxfh2fkw1ypff80ignv2zcm689m6gfa1qivrdhjh524g46h57cl") (yanked #t)))

(define-public crate-brpc-protoc-plugin-0.1 (crate (name "brpc-protoc-plugin") (vers "0.1.0-alpha3") (deps (list (crate-dep (name "cc") (req "^1.0.38") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2.60") (default-features #t) (kind 0)))) (hash "02f9ciraqq2ijh5nkvzpwc3h9238588sbl127hsds8bbl410k61x") (yanked #t)))

(define-public crate-brpc-protoc-plugin-0.1 (crate (name "brpc-protoc-plugin") (vers "0.1.0-alpha4") (deps (list (crate-dep (name "cc") (req "^1.0.38") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2.60") (default-features #t) (kind 0)))) (hash "1hhj7h99hhi8n43lgar88kk34bg8nyzkd9mivsk4phnbq7cx43ry") (yanked #t)))

(define-public crate-brpc-protoc-plugin-0.1 (crate (name "brpc-protoc-plugin") (vers "0.1.0") (deps (list (crate-dep (name "cc") (req "^1.0.38") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2.60") (default-features #t) (kind 0)))) (hash "06w0gp3xjsmrr6bkaxdrdm5kgwbf8r2s446km2g65fj6cm88gi26")))

(define-public crate-brpc-rs-0.1 (crate (name "brpc-rs") (vers "0.1.0-alpha") (deps (list (crate-dep (name "cc") (req "^1.0.37") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2.58") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.92") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0.92") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.39") (default-features #t) (kind 0)))) (hash "02fwaycfjmkmbgwygrxkzspw56x1px6h13m1v209s3f2napak60w") (yanked #t)))

(define-public crate-brpc-rs-0.1 (crate (name "brpc-rs") (vers "0.1.0") (deps (list (crate-dep (name "brpc-sys") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2.60") (default-features #t) (kind 0)))) (hash "0kfkm6mj4cs5vkha9v8fj71k5sx9mxxg4xahkdl39r0ly6f3m8sg")))

(define-public crate-brpc-sys-0.1 (crate (name "brpc-sys") (vers "0.1.0-alpha") (deps (list (crate-dep (name "cc") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "cc") (req "^1.0") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "1kcpn0m9dprjwsifnp3m58lq7y85dhlm74xs9mmna5hclvk5nraq") (yanked #t)))

(define-public crate-brpc-sys-0.1 (crate (name "brpc-sys") (vers "0.1.0") (deps (list (crate-dep (name "bytes") (req "^0.4.12") (default-features #t) (kind 0)) (crate-dep (name "cc") (req "^1.0.38") (default-features #t) (kind 0)) (crate-dep (name "cc") (req "^1.0") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "16dsrmv4hn9ig866b5jpaa11v7xbxfnfwslf7izw5fsx1mmb5a71")))

