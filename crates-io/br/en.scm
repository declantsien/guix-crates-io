(define-module (crates-io br en) #:use-module (crates-io))

(define-public crate-bren-0.1 (crate (name "bren") (vers "0.1.0") (deps (list (crate-dep (name "crossbeam") (req "^0.7.1") (default-features #t) (kind 0)) (crate-dep (name "ignore") (req "^0.4.7") (default-features #t) (kind 0)) (crate-dep (name "parking_lot") (req "^0.8.0") (default-features #t) (kind 0)) (crate-dep (name "rayon") (req "^1.0.3") (default-features #t) (kind 0)) (crate-dep (name "tempfile") (req "^3.0.7") (default-features #t) (kind 0)))) (hash "007h2ggpg20x2ma94czhsnknsm20krw4ccfzbnzpjgyk5pzxkxbf")))

