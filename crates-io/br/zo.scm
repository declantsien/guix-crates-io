(define-module (crates-io br zo) #:use-module (crates-io))

(define-public crate-brzozowski-0.1 (crate (name "brzozowski") (vers "0.1.0") (deps (list (crate-dep (name "criterion") (req "^0.5.1") (features (quote ("html_reports"))) (default-features #t) (kind 2)) (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 2)) (crate-dep (name "regex") (req "^1.10.4") (default-features #t) (kind 2)) (crate-dep (name "test-case") (req "^3.3.1") (features (quote ("regex" "with-regex"))) (default-features #t) (kind 2)))) (hash "1incqa9ch5d4kdd3zh998jqh87wjm005yhvmbbp7infkzfkv8jsp") (yanked #t)))

(define-public crate-brzozowski-0.1 (crate (name "brzozowski") (vers "0.1.1") (deps (list (crate-dep (name "criterion") (req "^0.5.1") (features (quote ("html_reports"))) (default-features #t) (kind 2)) (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 2)) (crate-dep (name "regex") (req "^1.10.4") (default-features #t) (kind 2)) (crate-dep (name "test-case") (req "^3.3.1") (features (quote ("regex" "with-regex"))) (default-features #t) (kind 2)))) (hash "1f9lghwmi5pfyf5385ym835hdp75267rnxqd8kh3rr8sfzph6bzk")))

(define-public crate-brzozowski-0.1 (crate (name "brzozowski") (vers "0.1.2") (deps (list (crate-dep (name "criterion") (req "^0.5.1") (features (quote ("html_reports"))) (default-features #t) (kind 2)) (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 2)) (crate-dep (name "regex") (req "^1.10.4") (default-features #t) (kind 2)) (crate-dep (name "test-case") (req "^3.3.1") (features (quote ("regex" "with-regex"))) (default-features #t) (kind 2)))) (hash "1plsvgfa2nkyjq6rs2sqk3kzfchh8sg9vd8js6hclpxk8y50wwvy")))

(define-public crate-brzozowski-regex-0.1 (crate (name "brzozowski-regex") (vers "0.1.0") (hash "09khpj8bvrqqlf4dkvki28w0cmja8h9vk9r85iiidr040bidjlxk")))

(define-public crate-brzozowski-regex-0.2 (crate (name "brzozowski-regex") (vers "0.2.0") (deps (list (crate-dep (name "itertools") (req "^0.12") (default-features #t) (kind 0)) (crate-dep (name "smallvec") (req "^1.11") (default-features #t) (kind 0)))) (hash "0bn14x09h95zmg0zkds5a4aakywjkgqcfb3ary76rjyq0vnfkkcf")))

