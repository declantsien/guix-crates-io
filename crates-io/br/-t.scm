(define-module (crates-io br -t) #:use-module (crates-io))

(define-public crate-br-tpl-0.1 (crate (name "br-tpl") (vers "0.1.0") (deps (list (crate-dep (name "futures") (req "^0.3.25") (default-features #t) (kind 0)) (crate-dep (name "handlebars") (req "^4.3.5") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0") (default-features #t) (kind 0)))) (hash "0zd865q0il64gvv2gjyd75d6kwy4ysbylfmc0d1ap61976hhk5cg")))

