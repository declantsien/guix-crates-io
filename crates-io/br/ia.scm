(define-module (crates-io br ia) #:use-module (crates-io))

(define-public crate-brian_minigrep-0.1 (crate (name "brian_minigrep") (vers "0.1.0") (hash "1dwk8h6c20w50giw8gbr46njn9z4sk6kdx4npl64aqyyzrg0cq9c")))

(define-public crate-briar-0.0.0 (crate (name "briar") (vers "0.0.0") (hash "0fb9d20hqxc6fk5w29pxdapmfi0lrxl7ml5x0cqna54bqq4r5ya9")))

