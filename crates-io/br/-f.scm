(define-module (crates-io br -f) #:use-module (crates-io))

(define-public crate-br-fields-0.0.1 (crate (name "br-fields") (vers "0.0.1") (deps (list (crate-dep (name "chrono") (req "^0.4.23") (default-features #t) (kind 0)) (crate-dep (name "json") (req "^0.12.4") (default-features #t) (kind 0)))) (hash "1lh0ya2c9q1p0zdcw27srmvbzcj3q4ikcpz7vil0hc6mch0jabqq")))

(define-public crate-br-fields-0.0.2 (crate (name "br-fields") (vers "0.0.2") (deps (list (crate-dep (name "chrono") (req "^0.4.23") (default-features #t) (kind 0)) (crate-dep (name "json") (req "^0.12.4") (default-features #t) (kind 0)))) (hash "0p44w25rd8j9gxp0wv8bzwff75d3m0hjxh7wbhhcknajq7n3fj92")))

(define-public crate-br-fields-0.0.3 (crate (name "br-fields") (vers "0.0.3") (deps (list (crate-dep (name "chrono") (req "^0.4.23") (default-features #t) (kind 0)) (crate-dep (name "json") (req "^0.12.4") (default-features #t) (kind 0)))) (hash "0n6a9nbc9bydvqh8v7kc7qs9kl5hl0znzya2pgszdbcz81ng9dm6")))

(define-public crate-br-fields-0.0.4 (crate (name "br-fields") (vers "0.0.4") (deps (list (crate-dep (name "chrono") (req "^0.4.23") (default-features #t) (kind 0)) (crate-dep (name "json") (req "^0.12.4") (default-features #t) (kind 0)))) (hash "132qxc4ydhr6b1vlmxpnr4k7ybr45g7sg4lxwf71k1ikd4lpvr4j")))

(define-public crate-br-fields-0.0.5 (crate (name "br-fields") (vers "0.0.5") (deps (list (crate-dep (name "chrono") (req "^0.4.23") (default-features #t) (kind 0)) (crate-dep (name "json") (req "^0.12.4") (default-features #t) (kind 0)))) (hash "0bb9ilrz2w1w0q8c0rdaklhkwzdfcdk97wdvpkmj9wkfdyns4qxc")))

(define-public crate-br-fields-0.0.6 (crate (name "br-fields") (vers "0.0.6") (deps (list (crate-dep (name "chrono") (req "^0.4.23") (default-features #t) (kind 0)) (crate-dep (name "json") (req "^0.12.4") (default-features #t) (kind 0)))) (hash "00pdgfhmxx8x08dlmmm0miz36m8vdi3ikld94wv29slhvv89xj03")))

(define-public crate-br-fields-0.0.7 (crate (name "br-fields") (vers "0.0.7") (deps (list (crate-dep (name "chrono") (req "^0.4.23") (default-features #t) (kind 0)) (crate-dep (name "json") (req "^0.12.4") (default-features #t) (kind 0)))) (hash "00042pzg41d1m3hx75pghad5j0xjs82amfp6p7srvqbqqrymh4yn")))

(define-public crate-br-fields-0.0.8 (crate (name "br-fields") (vers "0.0.8") (deps (list (crate-dep (name "chrono") (req "^0.4.23") (default-features #t) (kind 0)) (crate-dep (name "json") (req "^0.12.4") (default-features #t) (kind 0)))) (hash "1a9k7v410j2rxgvx81az2wzbl9227i25nsbwh4jl0g36v96ghqrq")))

(define-public crate-br-fields-0.0.9 (crate (name "br-fields") (vers "0.0.9") (deps (list (crate-dep (name "chrono") (req "^0.4.23") (default-features #t) (kind 0)) (crate-dep (name "json") (req "^0.12.4") (default-features #t) (kind 0)))) (hash "1rpilx6ilv16vzl9ndfj1524w2kwp901x0jcp96yfgd3hfdkqlvj")))

(define-public crate-br-fields-0.0.10 (crate (name "br-fields") (vers "0.0.10") (deps (list (crate-dep (name "chrono") (req "^0.4.23") (default-features #t) (kind 0)) (crate-dep (name "json") (req "^0.12.4") (default-features #t) (kind 0)))) (hash "0wl45rfd5z715mwwmnhgayww323xr1zspliv8irxs14xrdvp8b1r")))

(define-public crate-br-fields-0.0.11 (crate (name "br-fields") (vers "0.0.11") (deps (list (crate-dep (name "chrono") (req "^0.4.23") (default-features #t) (kind 0)) (crate-dep (name "json") (req "^0.12.4") (default-features #t) (kind 0)))) (hash "1n1nnb7ah2j7bjhhn0gvdx177f8fhmkb34r8kbbqiksf9xnkbw7v")))

(define-public crate-br-fields-0.0.12 (crate (name "br-fields") (vers "0.0.12") (deps (list (crate-dep (name "chrono") (req "^0.4.23") (default-features #t) (kind 0)) (crate-dep (name "json") (req "^0.12.4") (default-features #t) (kind 0)))) (hash "0mmcvlmk8s1ji0va5b6kgfybkp9fnw71mp4rnz6jwgn0kwm1bxnl")))

(define-public crate-br-fields-0.0.13 (crate (name "br-fields") (vers "0.0.13") (deps (list (crate-dep (name "chrono") (req "^0.4.23") (default-features #t) (kind 0)) (crate-dep (name "json") (req "^0.12.4") (default-features #t) (kind 0)))) (hash "12l97yfija15lxzdk27gwkpsndxr6nchczhq9703vmab9l69wfvv")))

(define-public crate-br-fields-0.0.14 (crate (name "br-fields") (vers "0.0.14") (deps (list (crate-dep (name "chrono") (req "^0.4.23") (default-features #t) (kind 0)) (crate-dep (name "json") (req "^0.12.4") (default-features #t) (kind 0)))) (hash "1lhld9znci4npdk1bz9gjp5zic9hhnfzny88p8d3zgkdihbw4mrq")))

(define-public crate-br-fields-0.0.15 (crate (name "br-fields") (vers "0.0.15") (deps (list (crate-dep (name "chrono") (req "^0.4.23") (default-features #t) (kind 0)) (crate-dep (name "json") (req "^0.12.4") (default-features #t) (kind 0)))) (hash "0lbwwp25khxcgcwacg43qbskh71lg8rdmhvfip7w17wpmy4waq21")))

(define-public crate-br-fields-0.0.16 (crate (name "br-fields") (vers "0.0.16") (deps (list (crate-dep (name "chrono") (req "^0.4.23") (default-features #t) (kind 0)) (crate-dep (name "json") (req "^0.12.4") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.17") (default-features #t) (kind 0)))) (hash "12kabp7nfgm3ijc4r43fm488qnypsxrnfhqpwi6rksry4zn8djcq")))

(define-public crate-br-fields-0.0.17 (crate (name "br-fields") (vers "0.0.17") (deps (list (crate-dep (name "chrono") (req "^0.4.23") (default-features #t) (kind 0)) (crate-dep (name "json") (req "^0.12.4") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.17") (default-features #t) (kind 0)))) (hash "1j4c9nv8iygvcd4sqmhb141sxx0f99d886f262yn6avcnjxccwjs")))

(define-public crate-br-fields-0.0.18 (crate (name "br-fields") (vers "0.0.18") (deps (list (crate-dep (name "chrono") (req "^0.4.23") (default-features #t) (kind 0)) (crate-dep (name "json") (req "^0.12.4") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.17") (default-features #t) (kind 0)))) (hash "1flhmm1nkq07s30h23hws5gj94m9zfnwmsmdvwcd3irj0lg4r2ly")))

(define-public crate-br-fields-0.0.19 (crate (name "br-fields") (vers "0.0.19") (deps (list (crate-dep (name "chrono") (req "^0.4.23") (default-features #t) (kind 0)) (crate-dep (name "json") (req "^0.12.4") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.17") (default-features #t) (kind 0)))) (hash "0qrmwx04lfs4z635pf909lgcxw6l4wis2n9n60v5208cffj9i1id")))

(define-public crate-br-fields-0.0.20 (crate (name "br-fields") (vers "0.0.20") (deps (list (crate-dep (name "chrono") (req "^0.4.23") (default-features #t) (kind 0)) (crate-dep (name "json") (req "^0.12.4") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.17") (default-features #t) (kind 0)))) (hash "09c0z6l3nnq7rm5689p9q0sdlplh0zg3xnjcni4dbl84j46drkhw")))

(define-public crate-br-fields-0.0.21 (crate (name "br-fields") (vers "0.0.21") (deps (list (crate-dep (name "chrono") (req "^0.4.23") (default-features #t) (kind 0)) (crate-dep (name "json") (req "^0.12.4") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.17") (default-features #t) (kind 0)))) (hash "09jlzhqn3ydj87z51xsrlw6fr6nswkdnjc5hdyinyms2q6pqf8sl")))

(define-public crate-br-fields-0.0.22 (crate (name "br-fields") (vers "0.0.22") (deps (list (crate-dep (name "chrono") (req "^0.4.23") (default-features #t) (kind 0)) (crate-dep (name "json") (req "^0.12.4") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.17") (default-features #t) (kind 0)))) (hash "056mqkiz4mxjcqfnq7p541in8k7b3x7kdipklmim4p7nld681g30")))

(define-public crate-br-fields-0.0.23 (crate (name "br-fields") (vers "0.0.23") (deps (list (crate-dep (name "chrono") (req "^0.4.23") (default-features #t) (kind 0)) (crate-dep (name "json") (req "^0.12.4") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.17") (default-features #t) (kind 0)))) (hash "1hhr2zm7cv2xsbaach7ii0iq2wkwx1cqfnag94prackflqnc14dn")))

(define-public crate-br-fields-0.0.24 (crate (name "br-fields") (vers "0.0.24") (deps (list (crate-dep (name "chrono") (req "^0.4.23") (default-features #t) (kind 0)) (crate-dep (name "json") (req "^0.12.4") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.17") (default-features #t) (kind 0)))) (hash "113ysycn20rv547z99l75bcb3x4iv79ic792g3zlzgim8zsavgfh")))

(define-public crate-br-fields-0.0.25 (crate (name "br-fields") (vers "0.0.25") (deps (list (crate-dep (name "chrono") (req "^0.4.23") (default-features #t) (kind 0)) (crate-dep (name "json") (req "^0.12.4") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.17") (default-features #t) (kind 0)))) (hash "0fi5l8kmfghf40wra22z00cv73n1933f5gy54qrsdp9sqlkdvrnb")))

(define-public crate-br-fields-0.0.26 (crate (name "br-fields") (vers "0.0.26") (deps (list (crate-dep (name "chrono") (req "^0.4.23") (default-features #t) (kind 0)) (crate-dep (name "json") (req "^0.12.4") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.17") (default-features #t) (kind 0)))) (hash "1pzi6np99vhdvg5i1gjdn4k381cm7y2ca3isbkj5qbhy0zhib47p")))

(define-public crate-br-fields-0.0.27 (crate (name "br-fields") (vers "0.0.27") (deps (list (crate-dep (name "chrono") (req "^0.4.23") (default-features #t) (kind 0)) (crate-dep (name "json") (req "^0.12.4") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.17") (default-features #t) (kind 0)))) (hash "0d88d8s0q7jzdhl6vl6w7xg12s9wzjb1idgyaikiddshggmwjd8a")))

(define-public crate-br-fields-0.1 (crate (name "br-fields") (vers "0.1.0") (deps (list (crate-dep (name "chrono") (req "^0.4.23") (default-features #t) (kind 0)) (crate-dep (name "json") (req "^0.12.4") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.17") (default-features #t) (kind 0)))) (hash "1cff038rnapxixclffx8yhivxxd7gpmiqc0fbqry3lknm1imnvmn")))

(define-public crate-br-fields-0.1 (crate (name "br-fields") (vers "0.1.1") (deps (list (crate-dep (name "chrono") (req "^0.4.23") (default-features #t) (kind 0)) (crate-dep (name "json") (req "^0.12.4") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.17") (default-features #t) (kind 0)))) (hash "16y75svcs5rdvzvdc2k14zxyff60czs9vb40kdq698vwfh61dfar")))

(define-public crate-br-fields-0.1 (crate (name "br-fields") (vers "0.1.2") (deps (list (crate-dep (name "chrono") (req "^0.4.23") (default-features #t) (kind 0)) (crate-dep (name "json") (req "^0.12.4") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.17") (default-features #t) (kind 0)))) (hash "0bvkaddj3rqg0vz74jixhqjb355f792mjjzgqkkn6b6rh14irc1s")))

(define-public crate-br-fields-0.1 (crate (name "br-fields") (vers "0.1.3") (deps (list (crate-dep (name "chrono") (req "^0.4.23") (default-features #t) (kind 0)) (crate-dep (name "json") (req "^0.12.4") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.17") (default-features #t) (kind 0)))) (hash "04p5ikng0995pjsp9haxgsyakr8sb1xnbkbyl0pfi5xgy956khrw")))

(define-public crate-br-fields-0.1 (crate (name "br-fields") (vers "0.1.4") (deps (list (crate-dep (name "chrono") (req "^0.4.23") (default-features #t) (kind 0)) (crate-dep (name "json") (req "^0.12.4") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.17") (default-features #t) (kind 0)))) (hash "09bpbvjs9ljmda0gsa7cj21b22byqly3a1w76mhh5bs3gx43ryhr")))

(define-public crate-br-fields-0.1 (crate (name "br-fields") (vers "0.1.5") (deps (list (crate-dep (name "chrono") (req "^0.4.23") (default-features #t) (kind 0)) (crate-dep (name "json") (req "^0.12.4") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.17") (default-features #t) (kind 0)))) (hash "0fnpazbgkry0xjlyirgfwi55n0rscd0kdyayw3fg9sis1bl378kn")))

(define-public crate-br-fields-1 (crate (name "br-fields") (vers "1.0.0") (deps (list (crate-dep (name "chrono") (req "^0.4.23") (default-features #t) (kind 0)) (crate-dep (name "json") (req "^0.12.4") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.17") (default-features #t) (kind 0)))) (hash "0ikm4d9x0czwdm4ir5kncfqkirzk5jq0v2vbpa4kv42pb6maxlli")))

(define-public crate-br-fields-1 (crate (name "br-fields") (vers "1.0.1") (deps (list (crate-dep (name "chrono") (req "^0.4.23") (default-features #t) (kind 0)) (crate-dep (name "json") (req "^0.12.4") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.17") (default-features #t) (kind 0)))) (hash "0hzcb1rfgwk3q5ghgp4kcywir9nvp7r0mrjicppndavnw5lzd990")))

(define-public crate-br-fields-1 (crate (name "br-fields") (vers "1.0.2") (deps (list (crate-dep (name "chrono") (req "^0.4.23") (default-features #t) (kind 0)) (crate-dep (name "json") (req "^0.12.4") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.17") (default-features #t) (kind 0)))) (hash "0k32faxbwkzjksy4zbpk5r77zzckvcnv3jrgapi4lpx9k77238ad")))

(define-public crate-br-fields-1 (crate (name "br-fields") (vers "1.0.3") (deps (list (crate-dep (name "chrono") (req "^0.4.23") (default-features #t) (kind 0)) (crate-dep (name "json") (req "^0.12.4") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.17") (default-features #t) (kind 0)))) (hash "1781p2wkczgxajvkv1vs5h4nz6q0wn11gin3y73yf9fx1vsyb96c")))

(define-public crate-br-fields-1 (crate (name "br-fields") (vers "1.0.4") (deps (list (crate-dep (name "chrono") (req "^0.4.31") (default-features #t) (kind 0)) (crate-dep (name "json") (req "^0.12.4") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.17") (default-features #t) (kind 0)))) (hash "1my0piy21x9xjpdb5bp9xng4kkmv3r4yxg4d863r7pssvqjya6f9")))

(define-public crate-br-fields-2 (crate (name "br-fields") (vers "2.0.0") (deps (list (crate-dep (name "chrono") (req "^0.4.31") (default-features #t) (kind 0)) (crate-dep (name "json") (req "^0.12.4") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.17") (default-features #t) (kind 0)))) (hash "1wb4z8i1q9vvil0jdpba8izgzz6q08cyxhmjxrr2j7awplzk9j0v")))

(define-public crate-br-fields-2 (crate (name "br-fields") (vers "2.0.1") (deps (list (crate-dep (name "chrono") (req "^0.4.31") (default-features #t) (kind 0)) (crate-dep (name "json") (req "^0.12.4") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.17") (default-features #t) (kind 0)))) (hash "1dc3bhm3bg75p8d9lab2j775w2pjl9mfjn62h1mxkzk2qv39yz4x")))

(define-public crate-br-fields-2 (crate (name "br-fields") (vers "2.0.2") (deps (list (crate-dep (name "chrono") (req "^0.4.31") (default-features #t) (kind 0)) (crate-dep (name "json") (req "^0.12.4") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.17") (default-features #t) (kind 0)))) (hash "1qgyyjnvadsj6v66kpbf0z28w33kjmb6kgfw3gcpwx67wv5avn0a")))

(define-public crate-br-fields-2 (crate (name "br-fields") (vers "2.0.3") (deps (list (crate-dep (name "chrono") (req "^0.4.31") (default-features #t) (kind 0)) (crate-dep (name "json") (req "^0.12.4") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.17") (default-features #t) (kind 0)))) (hash "09k0x1b5jc2xmbcr47fay49w0l3fi0d306lapsf2a3b3y996k7gm")))

(define-public crate-br-fields-2 (crate (name "br-fields") (vers "2.0.4") (deps (list (crate-dep (name "chrono") (req "^0.4.31") (default-features #t) (kind 0)) (crate-dep (name "json") (req "^0.12.4") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.17") (default-features #t) (kind 0)))) (hash "00nbam3q7s6k75yl46v8jqprzdp6ds7nld8wa7x6rg9ymhzl2jxb")))

(define-public crate-br-file-0.0.1 (crate (name "br-file") (vers "0.0.1") (deps (list (crate-dep (name "json") (req "^0.12.4") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.17") (default-features #t) (kind 0)) (crate-dep (name "oxipng") (req "^8.0.0") (default-features #t) (kind 0)) (crate-dep (name "walkdir") (req "^2.3.2") (default-features #t) (kind 0)))) (hash "0bs8273wz1p55q8kppbbin79vam0v7p0qcgx83czzmbz549q6pzy")))

(define-public crate-br-file-0.0.3 (crate (name "br-file") (vers "0.0.3") (deps (list (crate-dep (name "log") (req "^0.4.17") (default-features #t) (kind 0)))) (hash "0mgri93mr08xpbaxhldc7y7ndc8p399l2nla123g8nsxxn4xhad0")))

