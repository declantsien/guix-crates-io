(define-module (crates-io br aq) #:use-module (crates-io))

(define-public crate-braque-0.1 (crate (name "braque") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^3.0") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "eyre") (req "^0.6") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "hex-literal") (req "^0.3.4") (default-features #t) (kind 2)) (crate-dep (name "image") (req "^0.24") (features (quote ("jpeg_rayon" "png"))) (kind 0)) (crate-dep (name "num-integer") (req "^0.1") (default-features #t) (kind 0)))) (hash "0cgm6r3rm2lq7ml8kr8gbpmq38aslqiias5mbjp3ljl3ws9bg4y4") (features (quote (("cli" "clap" "eyre"))))))

