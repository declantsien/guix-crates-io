(define-module (crates-io br ie) #:use-module (crates-io))

(define-public crate-brief-0.1 (crate (name "brief") (vers "0.1.0") (hash "02g6xkhyf5vlzf1ai6f2sqmghzamkp5wiiamfxibknc3lh3ay11f")))

(define-public crate-briefcase-0.1 (crate (name "briefcase") (vers "0.1.0") (deps (list (crate-dep (name "anyhow") (req "^1.0.79") (default-features #t) (kind 0)) (crate-dep (name "fancy-regex") (req "^0.13.0") (default-features #t) (kind 0)))) (hash "0srpnjxwpkvd65dm38aajkrw9b9aj4x2142lb29sk7a4jpjbdhk4")))

