(define-module (crates-io br an) #:use-module (crates-io))

(define-public crate-branca-0.1 (crate (name "branca") (vers "0.1.0") (hash "1gahd825idf8b9rhdbkn37nqhy1xjl4kqnhi7bq6918a2d52p76v")))

(define-public crate-branca-0.1 (crate (name "branca") (vers "0.1.1") (deps (list (crate-dep (name "base-x") (req "^0.2.3") (default-features #t) (kind 0)) (crate-dep (name "byteorder") (req "^1.2.7") (default-features #t) (kind 0)) (crate-dep (name "chrono") (req "^0.4.6") (features (quote ("serde"))) (default-features #t) (kind 0)) (crate-dep (name "sodiumoxide-xchacha20poly1305") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "1j3v44y6y4nlxr0i20sdkw0jsqj83y51c2qs6b7an6la056ad9cx")))

(define-public crate-branca-0.2 (crate (name "branca") (vers "0.2.0") (deps (list (crate-dep (name "base-x") (req "^0.2.3") (default-features #t) (kind 0)) (crate-dep (name "byteorder") (req "^1.2.7") (default-features #t) (kind 0)) (crate-dep (name "chrono") (req "^0.4.6") (features (quote ("serde"))) (default-features #t) (kind 0)) (crate-dep (name "sodiumoxide") (req "^0.2.0") (default-features #t) (kind 0)))) (hash "185szmzyfap6fqlirx9drjxwfpkfpyjrbidhg0rknp1vwbl1p06r")))

(define-public crate-branca-0.5 (crate (name "branca") (vers "0.5.0") (deps (list (crate-dep (name "base-x") (req "^0.2.3") (default-features #t) (kind 0)) (crate-dep (name "byteorder") (req "^1.2.7") (default-features #t) (kind 0)) (crate-dep (name "chacha20-poly1305-aead") (req "^0.1.2") (default-features #t) (kind 0)) (crate-dep (name "ring") (req "^0.13.5") (default-features #t) (kind 2)) (crate-dep (name "serde") (req "^1.0") (default-features #t) (kind 2)) (crate-dep (name "serde_derive") (req "^1.0.83") (default-features #t) (kind 2)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 2)))) (hash "0sn72sxq2y70slbrxk5fsnpfsdf6h3xa4bf1k6dcyfiik9hip118")))

(define-public crate-branca-0.8 (crate (name "branca") (vers "0.8.0") (deps (list (crate-dep (name "base-x") (req "^0.2.3") (default-features #t) (kind 0)) (crate-dep (name "byteorder") (req "^1.2.7") (default-features #t) (kind 0)) (crate-dep (name "chacha20-poly1305-aead") (req "^0.1.2") (default-features #t) (kind 0)) (crate-dep (name "ring") (req "^0.13.5") (default-features #t) (kind 0)) (crate-dep (name "ring") (req "^0.13.5") (default-features #t) (kind 2)) (crate-dep (name "serde") (req "^1.0") (default-features #t) (kind 2)) (crate-dep (name "serde_derive") (req "^1.0.83") (default-features #t) (kind 2)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 2)))) (hash "0y90q52p18d80kfy7m713nnr3w0rpilzqj9mjzgx5pr6sx1bb059")))

(define-public crate-branca-0.9 (crate (name "branca") (vers "0.9.0") (deps (list (crate-dep (name "base-x") (req "^0.2.3") (default-features #t) (kind 0)) (crate-dep (name "byteorder") (req "^1.3.2") (default-features #t) (kind 0)) (crate-dep (name "orion") (req "^0.14.2") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (default-features #t) (kind 2)) (crate-dep (name "serde_derive") (req "^1.0.97") (default-features #t) (kind 2)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 2)))) (hash "1yk5nsdddbzjg4bbb753gbn1amz06s34086mishfrib9bly0mh64")))

(define-public crate-branca-0.9 (crate (name "branca") (vers "0.9.1") (deps (list (crate-dep (name "base-x") (req "^0.2.6") (default-features #t) (kind 0)) (crate-dep (name "byteorder") (req "^1.3.4") (default-features #t) (kind 0)) (crate-dep (name "orion") (req "^0.15.1") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (default-features #t) (kind 2)) (crate-dep (name "serde_derive") (req "^1.0.97") (default-features #t) (kind 2)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 2)))) (hash "1vhfbrca52qj85xijw5cz51mw4nzh4z3gz24mrrdc2hg8f52zvcl")))

(define-public crate-branca-0.9 (crate (name "branca") (vers "0.9.2") (deps (list (crate-dep (name "base-x") (req "^0.2.6") (default-features #t) (kind 0)) (crate-dep (name "byteorder") (req "^1.3.4") (default-features #t) (kind 0)) (crate-dep (name "orion") (req "^0.15.3") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (default-features #t) (kind 2)) (crate-dep (name "serde_derive") (req "^1.0.115") (default-features #t) (kind 2)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 2)))) (hash "0wpfmjgng0jm4y7fvw98any3hqn46j10dxxk5g2bbqfb2zq3f8mv")))

(define-public crate-branca-0.10 (crate (name "branca") (vers "0.10.0") (deps (list (crate-dep (name "base-x") (req "^0.2.6") (default-features #t) (kind 0)) (crate-dep (name "byteorder") (req "^1.3.4") (default-features #t) (kind 0)) (crate-dep (name "orion") (req "~0.15.4") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (default-features #t) (kind 2)) (crate-dep (name "serde_derive") (req "^1.0.115") (default-features #t) (kind 2)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 2)))) (hash "0gwzm3ysawa2z50bq3p3rwhfnarick79fsm1k3brb5bgk8bmms05")))

(define-public crate-branca-0.10 (crate (name "branca") (vers "0.10.1") (deps (list (crate-dep (name "base-x") (req "^0.2.8") (default-features #t) (kind 0)) (crate-dep (name "byteorder") (req "^1.4.3") (default-features #t) (kind 0)) (crate-dep (name "getrandom") (req "^0.2.3") (default-features #t) (kind 2)) (crate-dep (name "hex") (req "^0.4.2") (default-features #t) (kind 2)) (crate-dep (name "orion") (req "^0.17.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (default-features #t) (kind 2)) (crate-dep (name "serde_derive") (req "^1.0.115") (default-features #t) (kind 2)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 2)))) (hash "0gszbzbl7zc7rsbyd28d0xb4hya7qk4k6kqdvbvbviv4v2knrph4")))

(define-public crate-branch-0.1 (crate (name "branch") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^2.31.2") (default-features #t) (kind 0)))) (hash "1ab9bi7cfj29vmdnidf44y2z08yd2j9j3bc3zhib68ix2z0ckg0v")))

(define-public crate-branch-d-0.1 (crate (name "branch-d") (vers "0.1.1") (deps (list (crate-dep (name "clap") (req "^2.31.2") (default-features #t) (kind 0)))) (hash "0fx594c2gs6k2mgnqafiy6mg4znrjcg03wf8y2sdcs367gs45nwq")))

(define-public crate-branch-d-0.2 (crate (name "branch-d") (vers "0.2.0") (deps (list (crate-dep (name "clap") (req "^2.31.2") (default-features #t) (kind 0)))) (hash "1wp1p21z2q8kgrk2z4s0mj5mf917vb33sp8aryhjmaldn7j9s5qb")))

(define-public crate-branch_bound_method-0.1 (crate (name "branch_bound_method") (vers "0.1.0") (deps (list (crate-dep (name "log") (req "^0.4.14") (default-features #t) (kind 0)) (crate-dep (name "ndarray") (req "^0.15.3") (default-features #t) (kind 0)) (crate-dep (name "simplex_method") (req "^0.1.1") (default-features #t) (kind 0)))) (hash "07n9kjxzsjhwiaz1l4lphb7vp94ljjijbsrbmqfb6vwalhqzc7hs")))

(define-public crate-branch_bound_method-0.1 (crate (name "branch_bound_method") (vers "0.1.1") (deps (list (crate-dep (name "log") (req "^0.4.14") (default-features #t) (kind 0)) (crate-dep (name "ndarray") (req "^0.15.3") (default-features #t) (kind 0)) (crate-dep (name "simplex_method") (req "^0.1.3") (default-features #t) (kind 0)))) (hash "1f47rxsmz5mf4x36i2klfxjxhlwhrnl23f4wsyii353bnni89xb6")))

(define-public crate-branch_hints-0.1 (crate (name "branch_hints") (vers "0.1.0") (deps (list (crate-dep (name "rand") (req "^0.8") (default-features #t) (kind 2)))) (hash "0129rcrv4qrk6625zwhs41g1llk5rgvyl9j3g0qwv1xynixp7hw9") (yanked #t)))

(define-public crate-branch_hints-0.1 (crate (name "branch_hints") (vers "0.1.1") (deps (list (crate-dep (name "rand") (req "^0.8") (default-features #t) (kind 2)))) (hash "1f4dayhmk7gbq85fbz2ad75lf6m61avjr5nzhf140r0qp7k5sz9g") (yanked #t)))

(define-public crate-branch_hints-0.1 (crate (name "branch_hints") (vers "0.1.2") (deps (list (crate-dep (name "rand") (req "^0.8") (default-features #t) (kind 2)))) (hash "0vjbky6lq2jhs79gd2y8myh4ar19zqbl2qcrl9gi0b8y3rvab4cc") (yanked #t)))

(define-public crate-branch_hints-0.2 (crate (name "branch_hints") (vers "0.2.0") (deps (list (crate-dep (name "rand") (req "^0.8") (default-features #t) (kind 2)))) (hash "0fa0drp3p85w6s4qam52a942sh16bnkp7cffqdq2cfa4x5cr45ch")))

(define-public crate-branch_hints-0.2 (crate (name "branch_hints") (vers "0.2.1") (deps (list (crate-dep (name "rand") (req "^0.8") (default-features #t) (kind 2)))) (hash "10dvvsglm5jlarzlyii8pfk1fv3x1sf9145ig6i9y56002mw12mc")))

(define-public crate-branch_hints-0.3 (crate (name "branch_hints") (vers "0.3.0") (deps (list (crate-dep (name "rand") (req "^0.8") (default-features #t) (kind 2)))) (hash "1j46r0v514c0vh2aj3xsqp8p5gc60w9z7s02djqyxrdai0wgmrcl")))

(define-public crate-branch_hints-0.3 (crate (name "branch_hints") (vers "0.3.1") (deps (list (crate-dep (name "rand") (req "^0.8") (default-features #t) (kind 2)))) (hash "1wrsvz7hw40vcms57mh1zlgf6z62s4yr1gfkkiijdmhz1dkvjwn0")))

(define-public crate-branch_hints-0.4 (crate (name "branch_hints") (vers "0.4.0") (deps (list (crate-dep (name "rand") (req "^0.8") (default-features #t) (kind 2)))) (hash "1ds5s7fls51p1572fdjypd7lw70m5b4dhmkq15w8scihm3vh252c")))

(define-public crate-branches-0.1 (crate (name "branches") (vers "0.1.0") (hash "0l1pqj2wvmbanyphdrsrv65p4prar1s7gdvvg7yl29miyvbn1yi2")))

(define-public crate-branches-0.1 (crate (name "branches") (vers "0.1.1") (hash "13f9grgxiwbwfpr06nhmb2zhij3b681x19vqm090gfs40px0dmbh")))

(define-public crate-branches-0.1 (crate (name "branches") (vers "0.1.2") (hash "003h77ipbd3p00m6m2xn7kjs85vzgxz17l9aj4g6y360j573q5ml")))

(define-public crate-branches-0.1 (crate (name "branches") (vers "0.1.3") (hash "0alxmzmms4s8vv1v94y2kdq49a4p7728fgkpxx36z2m092bznn3r")))

(define-public crate-branchify-0.1 (crate (name "branchify") (vers "0.1.0") (deps (list (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)))) (hash "101qck26s6aw0sk5gjvxc7gbkxh71wq7xxsp1rkadnkx3g7jxf2d")))

(define-public crate-branchless-0.1 (crate (name "branchless") (vers "0.1.0") (hash "1fax8af69rjd0h9gf8bg3lf9g95qirwzc1zkxyj30dmcqdc4n7q2")))

(define-public crate-branchless-0.1 (crate (name "branchless") (vers "0.1.1") (deps (list (crate-dep (name "branchless_core") (req "^0.1") (default-features #t) (kind 0)))) (hash "0d6wwgzn46xc76wrxqqkcsa049miqryxf2slx3pzs2xrkknbs7bc")))

(define-public crate-branchless_core-0.1 (crate (name "branchless_core") (vers "0.1.0") (hash "1b2igbjiq9zjn2ggjdfq4f739xzq59z5g122ddsq1jd324brjcvp")))

(define-public crate-branchout-0.1 (crate (name "branchout") (vers "0.1.0") (hash "0al5hd870s8xzjnlqp156frlqgp8ipi90jskd7dx0xlvwr83d61c")))

(define-public crate-branchout-0.1 (crate (name "branchout") (vers "0.1.1") (hash "15dnsq1bsxf9n09ja6815jab733q0mynrca326cdgj2ck2kjwhca")))

(define-public crate-branchout-0.1 (crate (name "branchout") (vers "0.1.2") (hash "0wlngngkkii39gm9jvcfk2baqdgzmgh57gqyzid20lz0m80p3a5f")))

(define-public crate-branchy-0.1 (crate (name "branchy") (vers "0.1.0") (deps (list (crate-dep (name "rand") (req "^0.7.2") (default-features #t) (kind 0)))) (hash "1sm8mnp8m9030ndc4cnv97x0q7b6zqc0jqdsh63bfgp7b2gqj9mk")))

(define-public crate-branchy-0.1 (crate (name "branchy") (vers "0.1.1") (deps (list (crate-dep (name "rand") (req "^0.7.2") (default-features #t) (kind 0)))) (hash "0pi2hq9ws1ksry1rbd91lr83mf6pyzb9598mf4hx382wy0zbxqn5")))

(define-public crate-branchy-0.2 (crate (name "branchy") (vers "0.2.0") (deps (list (crate-dep (name "rand") (req "^0.7.2") (default-features #t) (kind 0)))) (hash "08yxy3p8xy6y9f9p6jziwz3y334zqxi32zwl7l7afrgcjm7n04mw")))

(define-public crate-branchy-0.2 (crate (name "branchy") (vers "0.2.1") (deps (list (crate-dep (name "rand") (req "^0.7.2") (default-features #t) (kind 0)))) (hash "0k5wjcwa2qmwv02ga31rs0bmj2xcysy4zqqcc7vx041kxall0gpy")))

(define-public crate-brane-0.0.1 (crate (name "brane") (vers "0.0.1") (hash "1qf3dkyzwrzqf9is2sdcvbac0sm0raly6ngvdy5n4l7m0wx13kqv")))

