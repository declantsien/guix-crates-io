(define-module (crates-io br #{4i}#) #:use-module (crates-io))

(define-public crate-br4infuck-0.1 (crate (name "br4infuck") (vers "0.1.0") (hash "0q52fhdk7gilx3qfhymgixhzrfxmzi7k7a1nnd0a4kmgwwnfkwhi")))

(define-public crate-br4infuck-0.1 (crate (name "br4infuck") (vers "0.1.1") (hash "0m4rnhi0l76kjlw4n3p35ap687ybm5hva48af9c13f1w8cpym09r")))

(define-public crate-br4infuck-0.1 (crate (name "br4infuck") (vers "0.1.2") (hash "1j15y4bag0119bgywj400sz6myxxwihc20b0drvara4zbkp5jyka")))

(define-public crate-br4infuck-0.1 (crate (name "br4infuck") (vers "0.1.3") (hash "1q8gjljanvx6bdyir178karg6z9q89l2whg6wx1nh32n27klkx43")))

(define-public crate-br4infuck-0.1 (crate (name "br4infuck") (vers "0.1.4") (hash "13s3s5hsz0q5f7i1mvs8l3wzsyzlrmkizhp9fdbrb11kbyd4xm0v")))

(define-public crate-br4infuck-0.1 (crate (name "br4infuck") (vers "0.1.5") (deps (list (crate-dep (name "clap") (req "^2.33.0") (default-features #t) (kind 0)))) (hash "1x2wjd0xc1qrn408vq3pi4vc0w1jsfk5ypzcjlkcn2b0aszvg5k6")))

