(define-module (crates-io br so) #:use-module (crates-io))

(define-public crate-brson-rs-0.1 (crate (name "brson-rs") (vers "0.1.0") (deps (list (crate-dep (name "brotli") (req "^3.4.0") (default-features #t) (kind 0)) (crate-dep (name "bson") (req "^2.8.1") (default-features #t) (kind 0)))) (hash "1a7rw73jh3vif2hbskzw3pp54pq97rmxyi4fniiz56wfa6vp8qb8")))

