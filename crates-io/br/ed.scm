(define-module (crates-io br ed) #:use-module (crates-io))

(define-public crate-bred-0.1 (crate (name "bred") (vers "0.1.2") (deps (list (crate-dep (name "clap") (req "^4.0.10") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "0n9dzdkfl1azqhbmvf8nfq0y5xifzqf4cayxl57p3xlvx5701xkw")))

(define-public crate-bred-0.2 (crate (name "bred") (vers "0.2.1") (deps (list (crate-dep (name "clap") (req "^4.0.10") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "04wq730mzr31w0lwrjhvp0wr1pa1c70fzya55jff0fjfrhzpms17")))

(define-public crate-bred-0.2 (crate (name "bred") (vers "0.2.2") (deps (list (crate-dep (name "clap") (req "^4.0.10") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "1fx9mlxhy83l56ipnqlwn0hhb70ffn7xzjqh7cnssbxnd00p7mhq")))

(define-public crate-bred-0.3 (crate (name "bred") (vers "0.3.1") (deps (list (crate-dep (name "clap") (req "^4.0.26") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "1iir3ixax0ck6b75gjfm3avbsf8g28p9jxqih82bv3dzdms8vxxq")))

(define-public crate-bred-0.3 (crate (name "bred") (vers "0.3.2") (deps (list (crate-dep (name "clap") (req "^4.0.26") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "15g55i7k5ryz5p01n1w4gmib5cbd8lx2940d9y7c1fc37xgvkq96")))

(define-public crate-breda-0.1 (crate (name "breda") (vers "0.1.0") (hash "01nim9a4285pz4kjv7j3zacn44d2fh3fxgcqxszind53ll97dh4q")))

(define-public crate-breda-acceleration-structure-test-macro-0.0.0 (crate (name "breda-acceleration-structure-test-macro") (vers "0.0.0") (hash "13fp9lpn7d27k4llv5mjxxfj31irrcn5nr4xm1rgwc2708j8mz12")))

(define-public crate-breda-acceleration-structure-testing-0.0.0 (crate (name "breda-acceleration-structure-testing") (vers "0.0.0") (hash "01gpac34ym20phc8nqw76b793ksq78q3bfifpy3icrzlhld6y4fb")))

(define-public crate-breda-acceleration-structures-0.0.0 (crate (name "breda-acceleration-structures") (vers "0.0.0") (hash "1p9ysqkpxq87pgpbldxg3f496s5c8cw7rr6qrdm5yl8343pr10gl")))

(define-public crate-breda-accumulation-tracker-0.0.0 (crate (name "breda-accumulation-tracker") (vers "0.0.0") (hash "198a3kf8y5m0wcnh70h5v2dg63hm45lbk1x0id2jgjzrkf9pmzln")))

(define-public crate-breda-allocator-visualizer-0.0.0 (crate (name "breda-allocator-visualizer") (vers "0.0.0") (hash "0wn4zpjz6f87qz6ssm5xy9p60v25svpj8gzpjxggfk11ajwixl3x")))

(define-public crate-breda-animation-0.0.0 (crate (name "breda-animation") (vers "0.0.0") (hash "1flb6y8a0f305mki0pk6ylj731cjzj29rkykd1g8jb38nh80asc9")))

(define-public crate-breda-arc-final-owner-0.0.0 (crate (name "breda-arc-final-owner") (vers "0.0.0") (hash "1mh8kyr07b0wg7scdn3vcgz1rik5fc9a28qjinz5qb5ahp6j1k4h")))

(define-public crate-breda-as-playground-0.0.0 (crate (name "breda-as-playground") (vers "0.0.0") (hash "1iyfh79hq7b49rj5jvxndk14b83xcfmax6zjdng5izqi072vbida")))

(define-public crate-breda-asset-0.0.0 (crate (name "breda-asset") (vers "0.0.0") (hash "1yd2fzcfswi5s52sxq9c3912sx3csfy7l4496za1y4w81alrvwmf")))

(define-public crate-breda-asset-crate-hash-0.0.0 (crate (name "breda-asset-crate-hash") (vers "0.0.0") (hash "061zij0bna3l8hl4bdr50kp1w015ka8mylgw7v8pmspav8kpry6s")))

(define-public crate-breda-asset-macros-0.0.0 (crate (name "breda-asset-macros") (vers "0.0.0") (hash "1famawky9mw9k5v68s62m7frkzainhv1r5ki0fqyh7a2rlljnp77")))

(define-public crate-breda-asset-pipeline-0.0.0 (crate (name "breda-asset-pipeline") (vers "0.0.0") (hash "153c7dfiws2l6kf1z0dnvc2i9xlv5w1bkcb14czalbdbfa7c6sxh")))

(define-public crate-breda-asset-ref-0.0.0 (crate (name "breda-asset-ref") (vers "0.0.0") (hash "1v9zz43izqggnh5y20wlwi38jxjidbsxpjk1l5q8syhfvzxbc8xm")))

(define-public crate-breda-asset-server-0.0.0 (crate (name "breda-asset-server") (vers "0.0.0") (hash "0n40f7ifm7y5qc0vkai30wga91mdf4irp1nrpfh5kg4nvw1sbxmi")))

(define-public crate-breda-asset-server-daemon-0.0.0 (crate (name "breda-asset-server-daemon") (vers "0.0.0") (hash "0mhzwbcck4a0blw5948ngz985w41phk23vg2klcrqlhkcbirr6xk")))

(define-public crate-breda-asset-streamer-0.0.0 (crate (name "breda-asset-streamer") (vers "0.0.0") (hash "0f1ndpn91ah2lfaljsk22jiin0rd4mh3bz39jbjqas6dlcy718c6")))

(define-public crate-breda-btf-data-gen-0.0.0 (crate (name "breda-btf-data-gen") (vers "0.0.0") (hash "1lyan135f196cgbkbnq9kr3yzs5cvmdghz2shnwib92qswdanwl0")))

(define-public crate-breda-btf-generator-0.0.0 (crate (name "breda-btf-generator") (vers "0.0.0") (hash "0bfw6v4xfjhxkws8hdhqwwy92wfrgbjmcp1zg2w32di44a4p3373")))

(define-public crate-breda-build-info-0.0.0 (crate (name "breda-build-info") (vers "0.0.0") (hash "1hnfb2478j6ra7xa2ny5swvsnqvi5y4vr1928bvpk2zrf8nvf66x")))

(define-public crate-breda-bvh-0.0.0 (crate (name "breda-bvh") (vers "0.0.0") (hash "1lmyabgirnpchdgqim9fcs6c67yk6lyfvany7whxavrsyn2zm77z")))

(define-public crate-breda-camera-0.0.0 (crate (name "breda-camera") (vers "0.0.0") (hash "0r1w1xcf31gxgmmb9n8rnb53l98f5j90p8bcwkv5fjj1cgalccsz")))

(define-public crate-breda-camera-preprocess-0.0.0 (crate (name "breda-camera-preprocess") (vers "0.0.0") (hash "1k2a0w343ii2s6wjyzx1iy76r1asdpzvj75x03dmv0a6r4hnqaam")))

(define-public crate-breda-camera-spline-api-0.0.0 (crate (name "breda-camera-spline-api") (vers "0.0.0") (hash "19243k9qs0z9wdq63bnpkjzhzfkbqfgfnd5wxg02wvfx6gccgf7y")))

(define-public crate-breda-canvas-0.0.0 (crate (name "breda-canvas") (vers "0.0.0") (hash "11bd4bi7v4jzz09frkw85vkxn2256460j04gbzni601qsmqp776g")))

(define-public crate-breda-canvas-derive-0.0.0 (crate (name "breda-canvas-derive") (vers "0.0.0") (hash "0siwnpxcr1sh6k1lsrca2430slb13akshwggk2b3rm4shhaazjfh")))

(define-public crate-breda-canvas-test-0.0.0 (crate (name "breda-canvas-test") (vers "0.0.0") (hash "0sz8w2csks44587n0cc8g4bzpl0a56y7vili0l0r1kkkhrcddf9i")))

(define-public crate-breda-canvas-types-0.0.0 (crate (name "breda-canvas-types") (vers "0.0.0") (hash "1acq39d52i5c9l7xbjzgwnf9zlajn9ky5vxs6mhxxqf8xm7wqv6r")))

(define-public crate-breda-car-0.0.0 (crate (name "breda-car") (vers "0.0.0") (hash "0apfhg37kbwfkmgps07997iyv0z47ffyw40lhhhfl25igp0nqi6q")))

(define-public crate-breda-car-api-0.0.0 (crate (name "breda-car-api") (vers "0.0.0") (hash "1yhd1i2czfw48ysw7lzis6p9zfj7vlddra25nlbfvxnzsfgy6svm")))

(define-public crate-breda-ci-asset-verifier-0.0.0 (crate (name "breda-ci-asset-verifier") (vers "0.0.0") (hash "1jmgz7h6c2s4zgli63wcrd5b4idir8nbnzbim1jm440yd5bmj8xk")))

(define-public crate-breda-collections-0.0.0 (crate (name "breda-collections") (vers "0.0.0") (hash "0xdrzkmhjaqnkxx7b0s9fj567z5k6bkgg5cxmk4f6l4gy246fszq")))

(define-public crate-breda-color-grading-0.0.0 (crate (name "breda-color-grading") (vers "0.0.0") (hash "0crkwddmk88jcmka2303ww3gnh74mcyy8nzwc7vv62yxhml61cir")))

(define-public crate-breda-color-grading-api-0.0.0 (crate (name "breda-color-grading-api") (vers "0.0.0") (hash "1cdb0ccdd81f3psjwjs0x9x6k97c2hnyli9kscf0q60s0y3flsib")))

(define-public crate-breda-color-grading-streamer-0.0.0 (crate (name "breda-color-grading-streamer") (vers "0.0.0") (hash "1h0vwrb9wipi2djdb2vnfm2jlx82c23x3jmqd5ij1fxmpkvmx4s0")))

(define-public crate-breda-color-space-0.0.0 (crate (name "breda-color-space") (vers "0.0.0") (hash "001pxgiakwv1pg6gc809hgvwi2xm2cp1vw2011h3jbz3msxjx21r")))

(define-public crate-breda-content-addressable-storage-0.0.0 (crate (name "breda-content-addressable-storage") (vers "0.0.0") (hash "0c4ngzw3fhwz6yaj544fvnhvq516036wskcz467y3d2wz6x85zx2")))

(define-public crate-breda-cpu-marker-0.0.0 (crate (name "breda-cpu-marker") (vers "0.0.0") (hash "0imaspc0c3g8z96m84fslgdz63ak3gw9gdnh9a5cr5dxizvldh79")))

(define-public crate-breda-crowd-api-0.0.0 (crate (name "breda-crowd-api") (vers "0.0.0") (hash "0s0960qjl4lc5iylgmw91j58asz5hkbiyqydkqkczxnsgifbc1r0")))

(define-public crate-breda-debug-renderer-0.0.0 (crate (name "breda-debug-renderer") (vers "0.0.0") (hash "1nqcrsqxrfgyvm0l7wzlkx3xy7n9bjmlaqvbbaz8dnqrd9wb8h3c")))

(define-public crate-breda-editor-server-daemon-0.0.0 (crate (name "breda-editor-server-daemon") (vers "0.0.0") (hash "19jq34k8hrvn3bbzzabxa220jzk8i1y1rri8jrhnfdca0f57ddzj")))

(define-public crate-breda-feature-toggles-0.0.0 (crate (name "breda-feature-toggles") (vers "0.0.0") (hash "1shd9jcpdpqq9qq19mlf8v9bsry19nf3j08h8ha2xd6i0mj6yghp")))

(define-public crate-breda-file-system-0.0.0 (crate (name "breda-file-system") (vers "0.0.0") (hash "0daw4rskdg68ya6wgym0whmjpbrcibfd5g4fr89947dqvx5lgjlm")))

(define-public crate-breda-flowmap-tool-0.0.0 (crate (name "breda-flowmap-tool") (vers "0.0.0") (hash "04938dmw97jhc55lfhai2klx2gjlsch3g521b194m3fv6mgwhb56")))

(define-public crate-breda-foundation-0.0.0 (crate (name "breda-foundation") (vers "0.0.0") (hash "05hcahpbp0pblm5z6aiqk03kpnqx16nwzsih4k6pg3av6q1qq1ck")))

(define-public crate-breda-global-illumination-0.0.0 (crate (name "breda-global-illumination") (vers "0.0.0") (hash "0kw6igp825f3yypbp7l4f21qqaysa60cwbcbalnqky003r9d7cr2")))

(define-public crate-breda-gpu-reduce-0.0.0 (crate (name "breda-gpu-reduce") (vers "0.0.0") (hash "1ry0y7zwblid2lsq5kknaz78jfih92bfg0dcxq2d9bs1jj571mlc")))

(define-public crate-breda-gpu-shared-0.0.0 (crate (name "breda-gpu-shared") (vers "0.0.0") (hash "07aaia20d35yfb0dym265vk0x96gjsd6g6d7axags68wa1zhr2j2")))

(define-public crate-breda-gpu-sort-0.0.0 (crate (name "breda-gpu-sort") (vers "0.0.0") (hash "1h9gcpk4mngchc7wjlwj3zz01grlksv613szlvzlaw5w26yxnl6z")))

(define-public crate-breda-hair-0.0.0 (crate (name "breda-hair") (vers "0.0.0") (hash "1npj6v4ybkygxjl14rzyvli6z4rx16w2md10rqxjb5knjlzprykq")))

(define-public crate-breda-hair-asset-0.0.0 (crate (name "breda-hair-asset") (vers "0.0.0") (hash "0yg2z9psl7r5l2y7zj3lalmylhzhb8wh34ygfcadckrlcvsgb1z6")))

(define-public crate-breda-hair-runtime-0.0.0 (crate (name "breda-hair-runtime") (vers "0.0.0") (hash "118l7r6lxa9h6gkf2z8862xj8lv9gzl5nc0hbbdcrf14abr97lfx")))

(define-public crate-breda-hash-0.0.0 (crate (name "breda-hash") (vers "0.0.0") (hash "06afcmp004m5iqc595gxfqz55l4a9sr0l66a36jvsa5g7n52wp47")))

(define-public crate-breda-imgui-0.0.0 (crate (name "breda-imgui") (vers "0.0.0") (hash "13isix81jzwj5si6li5jwhx1m6nzslrwzk59frh201nqys2rf8hm")))

(define-public crate-breda-imgui-frame-breakdown-0.0.0 (crate (name "breda-imgui-frame-breakdown") (vers "0.0.0") (hash "07rhjgdh1ppax9kh0wvph00j1kf4kfvm3b4x74kkvspclzibici8")))

(define-public crate-breda-inference-0.0.0 (crate (name "breda-inference") (vers "0.0.0") (hash "0qc976rrq4ndqwv6vh7fq0x3v4rg7i16g92xia5k7rywjrmnnccx")))

(define-public crate-breda-input-0.0.0 (crate (name "breda-input") (vers "0.0.0") (hash "0mqxx51jxdzanc6qv8wg4qkl5hnz3pv3dvl11qpr1ij9w7dgcwbr")))

(define-public crate-breda-key-value-store-api-0.0.0 (crate (name "breda-key-value-store-api") (vers "0.0.0") (hash "0jr7mks4aw7dkl40j5k0szyj8rkv7yfqja465j80b5r6xxd1pkji")))

(define-public crate-breda-key-value-store-cache-0.0.0 (crate (name "breda-key-value-store-cache") (vers "0.0.0") (hash "18mc1c6kr7l7hw8z75c9j4k6ic8lv2aw2lr5jq84z6ljswhdba5d")))

(define-public crate-breda-key-value-store-filesystem-0.0.0 (crate (name "breda-key-value-store-filesystem") (vers "0.0.0") (hash "1gibqvs8aqv5jz2ygb482j0v7cvxbi3fv65p7hqd2h1kgvvrjx5j")))

(define-public crate-breda-key-value-store-lmdb-0.0.0 (crate (name "breda-key-value-store-lmdb") (vers "0.0.0") (hash "1rpxr4q8brjc1n6wb3fgcws734rwyrq3jqbjb56ycynxirybkh6h")))

(define-public crate-breda-key-value-store-s3-0.0.0 (crate (name "breda-key-value-store-s3") (vers "0.0.0") (hash "0axcw3ynxybvj0kyhm7r28wjas5zvrah2v7ns7m55dqbvwjhirvv")))

(define-public crate-breda-light-sampling-0.0.0 (crate (name "breda-light-sampling") (vers "0.0.0") (hash "0payv5iasz5s9j7fil51awml2zhj0w0jfwjj044v24d7ixs0h47k")))

(define-public crate-breda-lightmapper-0.0.0 (crate (name "breda-lightmapper") (vers "0.0.0") (hash "0x279scbvx5pjac8lj3ad0r07a7sk0qac354g7ffj3j1k2v4a2yc")))

(define-public crate-breda-local-persistent-data-0.0.0 (crate (name "breda-local-persistent-data") (vers "0.0.0") (hash "173npsqf5z9gc8d13apqmi6mkga7sa16amiqs3firzas0k7rba4l")))

(define-public crate-breda-manifest-0.0.0 (crate (name "breda-manifest") (vers "0.0.0") (hash "1h5yii14pq01c2a0dka8j74y5yddxhk0zsf02l7h64jbbfm7y0p9")))

(define-public crate-breda-manifest-scene-api-0.0.0 (crate (name "breda-manifest-scene-api") (vers "0.0.0") (hash "178jq49rsndy4izlyl4gxkvim1gbq6aha8xnb66p466vbhwyhkx8")))

(define-public crate-breda-material-api-0.0.0 (crate (name "breda-material-api") (vers "0.0.0") (hash "0b1qnawjxmp1i559sz711fv3xrwlwvvmlhgxaj4shi09k3g2dpzy")))

(define-public crate-breda-material-diffuse-only-test-0.0.0 (crate (name "breda-material-diffuse-only-test") (vers "0.0.0") (hash "0n92z0hsjs5srybj58vfa2vrafr9hdpsi3vd38valc5s5203p1iw")))

(define-public crate-breda-material-diffuse-specular-0.0.0 (crate (name "breda-material-diffuse-specular") (vers "0.0.0") (hash "0p9cr7isdqfp91ixnff29nsmhjdyayp4id6gnv91jhysjp0hqwwy")))

(define-public crate-breda-material-overrides-0.0.0 (crate (name "breda-material-overrides") (vers "0.0.0") (hash "055mzzimxprkn75ynhsjnkx4r8qwmva2zrf97n01ijhnhwrclbj7")))

(define-public crate-breda-material-overrides-api-0.0.0 (crate (name "breda-material-overrides-api") (vers "0.0.0") (hash "1gr7fj44hdn8yx2v7vxw4igh6b4cd43fdpckxpwm91g86aq64448")))

(define-public crate-breda-material-system-0.0.0 (crate (name "breda-material-system") (vers "0.0.0") (hash "02dijizzy36n2b2xhrh4a02fd1l0mh3xikm5gfyi2bpx6559ai5a")))

(define-public crate-breda-material-weidlich-wilkie-0.0.0 (crate (name "breda-material-weidlich-wilkie") (vers "0.0.0") (hash "1259x7arp06bgq0m9qb1pgznihalclhkdjprq148i7h8gmc072q9")))

(define-public crate-breda-math-0.0.0 (crate (name "breda-math") (vers "0.0.0") (hash "1cz1hn3qx6vc51qjnwn66vz31g8lh00046rxgqpz3gs67lfbjic9")))

(define-public crate-breda-mesh-outliner-0.0.0 (crate (name "breda-mesh-outliner") (vers "0.0.0") (hash "08qr9y207jlf2kmdjpm6vrxbp8l2ir68kqi2ipmq6lai0nynjf0r")))

(define-public crate-breda-mesh-picker-0.0.0 (crate (name "breda-mesh-picker") (vers "0.0.0") (hash "0yz9yz52nmd2qmw17v089m4k2qmsymlnm3l4d3czlh8w9pygbv48")))

(define-public crate-breda-mesh-selection-0.0.0 (crate (name "breda-mesh-selection") (vers "0.0.0") (hash "019cq0fnn7slyfxfx59i6mb7jbmw8f94jbzcphmrm49qxxrhywsw")))

(define-public crate-breda-nerf-viewer-0.0.0 (crate (name "breda-nerf-viewer") (vers "0.0.0") (hash "100b7b8p29p5922i6g3yrwy8d0dnnwwclfywyzrn2npmf3d2kc2w")))

(define-public crate-breda-nn-test-0.0.0 (crate (name "breda-nn-test") (vers "0.0.0") (hash "05yjf08w4wc721fs2k60034bzzckx3kxlxfqghs70pkpvvm1a79k")))

(define-public crate-breda-noise-sampler-0.0.0 (crate (name "breda-noise-sampler") (vers "0.0.0") (hash "1nnkkmvmlwwbgymfl0y88qyhccdjk912nhzsvp33v0b70p956qh9")))

(define-public crate-breda-oidn-test-0.0.0 (crate (name "breda-oidn-test") (vers "0.0.0") (hash "03323n3cy25an4349kj6ikpqkdhgzx65jgx35wx9lc2sfn7nbzh9")))

(define-public crate-breda-onnx-0.0.0 (crate (name "breda-onnx") (vers "0.0.0") (hash "184x0m6iy3rkxxq5lf2s06v94zbwzmlbgc9ml0qd27ghvy0fgsgl")))

(define-public crate-breda-pipeline-camera-spline-0.0.0 (crate (name "breda-pipeline-camera-spline") (vers "0.0.0") (hash "0z5xxmpgi3fbi0jjynj4nj6iqsyzvmspq8s2na0j0fj5fbjaw276")))

(define-public crate-breda-pipeline-gltf-0.0.0 (crate (name "breda-pipeline-gltf") (vers "0.0.0") (hash "0lmqf9p3hjj06cj6gp93vrypa9chcrdvia6phj23797g71nqw8kz")))

(define-public crate-breda-pipeline-hair-0.0.0 (crate (name "breda-pipeline-hair") (vers "0.0.0") (hash "1p5ck3cg8s1f9na3n4m7dw2831qgiz4fx4r6nq0x9qg6ws8gyvgk")))

(define-public crate-breda-pipeline-manifest-scene-0.0.0 (crate (name "breda-pipeline-manifest-scene") (vers "0.0.0") (hash "1i7h5xsa00fnrm2f22qfzgssa6b09qb4wz1f6fpddnd92fv8w456")))

(define-public crate-breda-pipeline-shader-database-0.0.0 (crate (name "breda-pipeline-shader-database") (vers "0.0.0") (hash "08nf1ygf4j2jak2bkhj6n1cwn8kk0qablc5s6a2x71p1y9amb66w")))

(define-public crate-breda-pipeline-texture-0.0.0 (crate (name "breda-pipeline-texture") (vers "0.0.0") (hash "1py372kcz8bfqmcjb3jlr650025ngkjs7hrybkry44qzhsaj9yjv")))

(define-public crate-breda-platform-0.0.0 (crate (name "breda-platform") (vers "0.0.0") (hash "0zkbjv3wrwdsyn924r263vb80qqznvnm46rqdycawx8dh7bcjgc8")))

(define-public crate-breda-point-cloud-0.0.0 (crate (name "breda-point-cloud") (vers "0.0.0") (hash "0gnsm7j30hz9d5v79lcvjmwcfclavcqraz0v7h23lyslrji87xwv")))

(define-public crate-breda-procedural-geometry-renderer-0.0.0 (crate (name "breda-procedural-geometry-renderer") (vers "0.0.0") (hash "1dfcwwwcvgx45583x7fwgi9xic8v4bdfd05ahjykiaz29lq99izb")))

(define-public crate-breda-ray-tracer-0.0.0 (crate (name "breda-ray-tracer") (vers "0.0.0") (hash "1bvqfi9yyc893cj018s8iyw0r58bjcqca7jc4z6wyqdkpi7qlzhr")))

(define-public crate-breda-ray-tracer-android-0.0.0 (crate (name "breda-ray-tracer-android") (vers "0.0.0") (hash "178bkddz51izyyzhizhczhi4wm9kmp9kv3rsljkja5zmblkkmhqr")))

(define-public crate-breda-ray-tracer-cs-0.0.0 (crate (name "breda-ray-tracer-cs") (vers "0.0.0") (hash "1d775rwlcb5z17m2c8b97ci8rb8m81s13sjzf3ws3m80n0rwgn4s")))

(define-public crate-breda-render-backend-api-0.0.0 (crate (name "breda-render-backend-api") (vers "0.0.0") (hash "021jc0gvv98di3chk5sp2vrdiviaqqxv0whkpqwy3sskba2mps3i")))

(define-public crate-breda-render-backend-api-types-0.0.0 (crate (name "breda-render-backend-api-types") (vers "0.0.0") (hash "1zm9d9r7w7rq95gkwrkdqaii5i8frj1qjas9y148mz56265sn3jg")))

(define-public crate-breda-render-backend-dx12-0.0.0 (crate (name "breda-render-backend-dx12") (vers "0.0.0") (hash "1z1kchhxfxkcg756838r37dbp0hg52nqd8ziw2k1z4kx23mdjfff")))

(define-public crate-breda-render-backend-tests-0.0.0 (crate (name "breda-render-backend-tests") (vers "0.0.0") (hash "1g4apsyk4p4vdiycdnka10bisrbildbcd5v7ffb3zanirvji584s")))

(define-public crate-breda-render-backend-vulkan-0.0.0 (crate (name "breda-render-backend-vulkan") (vers "0.0.0") (hash "17mkjd1bacvbrlff78qjff2k87m4k36pn69qpqlap7xylkaa56lj")))

(define-public crate-breda-render-graph-0.0.0 (crate (name "breda-render-graph") (vers "0.0.0") (hash "0ahbgdkqr398f6242rqni13skb57m53r2z6hlicbbah6pdjzbd6w")))

(define-public crate-breda-render-graph-derive-0.0.0 (crate (name "breda-render-graph-derive") (vers "0.0.0") (hash "07657mzqffyxw94qxrcmp1xslvczwsggh1281rvz7i782yngf4cn")))

(define-public crate-breda-render-graph-readback-0.0.0 (crate (name "breda-render-graph-readback") (vers "0.0.0") (hash "16hxx0pkh7arf9wiii138l5kqvh1iyx05wdvkjxxh4jwy7gp22s1")))

(define-public crate-breda-render-loop-0.0.0 (crate (name "breda-render-loop") (vers "0.0.0") (hash "0ljaf9j6kya121pik3q3kybdgmr4cy1i35yai6m83n76iamb9ffc")))

(define-public crate-breda-render-pass-bloom-0.0.0 (crate (name "breda-render-pass-bloom") (vers "0.0.0") (hash "1khq8wr05zlhh9wf1j18f3qmmq75r4422hw5zbp7sa6nhr9iwh7r")))

(define-public crate-breda-render-pass-exposure-0.0.0 (crate (name "breda-render-pass-exposure") (vers "0.0.0") (hash "1isc27bs3fipsyv54y45nlh66zn0wpcnvh0yciac7rlblwk7jpbi")))

(define-public crate-breda-render-pass-taa-0.0.0 (crate (name "breda-render-pass-taa") (vers "0.0.0") (hash "1a7is23lprbkqlnrqkwpxzg49fq87fwqgnlpc9hrywpbk1g203jh")))

(define-public crate-breda-restir-0.0.0 (crate (name "breda-restir") (vers "0.0.0") (hash "1w51phpjmw9w4fvzavm3lszpsh9d497b2ha463vsaclbcsqn45qx")))

(define-public crate-breda-rust-gpu-experimental-0.0.0 (crate (name "breda-rust-gpu-experimental") (vers "0.0.0") (hash "1xiyi7213gvxsm2sc8zdbmmh3lcki2np7vdnr0jdhp2lqaaqkj45")))

(define-public crate-breda-rust-gpu-experimental-shaders-0.0.0 (crate (name "breda-rust-gpu-experimental-shaders") (vers "0.0.0") (hash "0ir1w252i8b4jl3ydib9xl85w3ryv4lilvgnkmqb31gp473djn1v")))

(define-public crate-breda-scene-api-0.0.0 (crate (name "breda-scene-api") (vers "0.0.0") (hash "0gc4j2v3x1qz99wfsr0hz3h17a7pbwgcx5dg5jjfd5r15nhcah2i")))

(define-public crate-breda-scene-runtime-0.0.0 (crate (name "breda-scene-runtime") (vers "0.0.0") (hash "020i59rp37n3wi09w4amyhfx0sjpipfsbqwa58a3krmkdjncb9wn")))

(define-public crate-breda-scene-runtime-api-0.0.0 (crate (name "breda-scene-runtime-api") (vers "0.0.0") (hash "1m8p1dld4b4i9p0vvd452i828fvlpc2hr5pg7xmb12pyfirqa42d")))

(define-public crate-breda-shader-compiler-0.0.0 (crate (name "breda-shader-compiler") (vers "0.0.0") (hash "1gqn46faczhvx9zi7ha09w4xpvm49gg6058sc5rgxpmzag7sx61j")))

(define-public crate-breda-shader-database-0.0.0 (crate (name "breda-shader-database") (vers "0.0.0") (hash "0rqr56j6igcsdy08pakf9rd6sm7sks9nyjxbkk1n8746f07g8czi")))

(define-public crate-breda-shader-database-api-0.0.0 (crate (name "breda-shader-database-api") (vers "0.0.0") (hash "0aba0psrd860jsik6bcgcbhb7a2m4q30xrbj4r08c7jg50js3c0d")))

(define-public crate-breda-shader-reflect-0.0.0 (crate (name "breda-shader-reflect") (vers "0.0.0") (hash "1l8l90a1mymp27jqwvvx9476fyn059sgjyj55yxryhpbzkgcxrz5")))

(define-public crate-breda-simple-scene-0.0.0 (crate (name "breda-simple-scene") (vers "0.0.0") (hash "012lcx47bb6618hl12pi67dnnai8qf5ax3s7ncv5xnvvwvbjwsa0")))

(define-public crate-breda-single-pass-downsample-0.0.0 (crate (name "breda-single-pass-downsample") (vers "0.0.0") (hash "1rr1i99y2jq1wwx1dsp2xfhhn1m0gwv5p6hyap4prgwbbpf91pdx")))

(define-public crate-breda-sky-0.0.0 (crate (name "breda-sky") (vers "0.0.0") (hash "11yfwj0mxvpa6d5b4n0j9i2zscmv4scpikbqcii8v27yx92jgpss")))

(define-public crate-breda-streaming-system-0.0.0 (crate (name "breda-streaming-system") (vers "0.0.0") (hash "0fc7s4nzslr7vqy70zabicwvajphxxw9hvy42rzqhjj3af3zxjw6")))

(define-public crate-breda-table-of-contents-0.0.0 (crate (name "breda-table-of-contents") (vers "0.0.0") (hash "0jgafr8vk4zz6fx2fxvyy855wmxlj0j9ghv1prgq7dq94154bwph")))

(define-public crate-breda-telemetry-0.0.0 (crate (name "breda-telemetry") (vers "0.0.0") (hash "04gy7l2021xmr7img7743wlap6qk8321d594yl15vd048p82ivnq")))

(define-public crate-breda-temp-asset-builder-0.0.0 (crate (name "breda-temp-asset-builder") (vers "0.0.0") (hash "09g79j8cs89ac3w7ga9bay1f5cb52kgjhx2x5i42x7r9501g24dl")))

(define-public crate-breda-texture-asset-0.0.0 (crate (name "breda-texture-asset") (vers "0.0.0") (hash "0zcmryb7xzpjx5fbwxa0b34qdc56nar82v0xiyawqcy82nr5dgll")))

(define-public crate-breda-texture-compression-0.0.0 (crate (name "breda-texture-compression") (vers "0.0.0") (hash "14d5lq0a69qfn5a6ywcwx9kbfn92pi3dm8vswjl99xdrzmby44cb")))

(define-public crate-breda-texture-streamer-0.0.0 (crate (name "breda-texture-streamer") (vers "0.0.0") (hash "036nd9883pv97c1ydqqixfghvqv2236kch1876d4l8r62fymfpca")))

(define-public crate-breda-texture-transcode-0.0.0 (crate (name "breda-texture-transcode") (vers "0.0.0") (hash "1iv5v84s8agpxv619dkg8whbbamjmwrn9bx32cminpd8g75w9hrh")))

(define-public crate-breda-threading-0.0.0 (crate (name "breda-threading") (vers "0.0.0") (hash "06ms9mg3gisxc9xsf714k06x11vnl9b00rksp1k7kckq6v00apiw")))

(define-public crate-breda-validation-0.0.0 (crate (name "breda-validation") (vers "0.0.0") (hash "0jyii0h4v7qhg400xfabcd0swbp1apvdcbyk3g2n4cynwxwp9asn")))

(define-public crate-breda-validation-api-0.0.0 (crate (name "breda-validation-api") (vers "0.0.0") (hash "1cpb9xyj661mpnqmxi9z7bf2578n3v7180q1jsgi4jzi7abfx0n6")))

(define-public crate-breda-vendor-dx12-agility-sdk-0.0.0 (crate (name "breda-vendor-dx12-agility-sdk") (vers "0.0.0") (hash "0y7hjgdf9c4cbm2mdijcb9z5sn8yn74gimylqly2rvbpnbhzlad5")))

(define-public crate-breda-wavefront-path-tracer-0.0.0 (crate (name "breda-wavefront-path-tracer") (vers "0.0.0") (hash "1qlm859vjbxhpz7i61dk40g5pgzapshkjwm4ap8w7z1xf12jddqb")))

(define-public crate-breda-winit-0.0.0 (crate (name "breda-winit") (vers "0.0.0") (hash "155br5kvz42wkwqy8z15hpc9xps9i8qnlm9rad7a68ac0d2db65x")))

(define-public crate-breda-workspace-0.0.0 (crate (name "breda-workspace") (vers "0.0.0") (hash "067x8wcpq7zk2r3fwq1srj1s2cnw5xg8gji41wdsbqwa8h9d9s6w")))

(define-public crate-breda-workspace-recipe-0.0.0 (crate (name "breda-workspace-recipe") (vers "0.0.0") (hash "1jsi4c1pi22r2qw6999cbgwh0di6pfig6zfbpdwnjiypk0vw8l61")))

(define-public crate-breda-zmq-api-0.0.0 (crate (name "breda-zmq-api") (vers "0.0.0") (hash "0rj02mfzwmlf8p0m9rkm468gbgclg30vdg8myyi1knd2gnlrw70s")))

(define-public crate-breda-zmq-client-0.0.0 (crate (name "breda-zmq-client") (vers "0.0.0") (hash "0ccpja6i8rxdmisvbwlkscc47g0v3fjmv5x18h160qshddlzq5vw")))

(define-public crate-breda-zmq-server-0.0.0 (crate (name "breda-zmq-server") (vers "0.0.0") (hash "1a6jk2kd496psk1ddpkv37vl6h4gwcxy5dlyn5f4ycr7fi5xx0c2")))

