(define-module (crates-io br f-) #:use-module (crates-io))

(define-public crate-brf-macro-0.1 (crate (name "brf-macro") (vers "0.1.0") (deps (list (crate-dep (name "proc-macro2") (req "^1.0.46") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.21") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.102") (default-features #t) (kind 0)))) (hash "13hn3wz65jga73hfv1zxfv45fp5iz9x1m3xy73nyy0g10aplnl4a")))

(define-public crate-brf-macro-0.1 (crate (name "brf-macro") (vers "0.1.1") (deps (list (crate-dep (name "proc-macro2") (req "^1.0.46") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.21") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.102") (default-features #t) (kind 0)))) (hash "17ihfqj1y2l19nmyq8f624prrcgjg4yzdfk0v0v2kma454nqf0wi")))

(define-public crate-brf-macro-0.1 (crate (name "brf-macro") (vers "0.1.2") (deps (list (crate-dep (name "braille-ascii") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1.0.46") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.21") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.102") (default-features #t) (kind 0)))) (hash "0rkpnwl1zqzg41k4yini7rlx8f0gyns2q5g97xwm3w17mrcw6qwr")))

(define-public crate-brf-macro-0.1 (crate (name "brf-macro") (vers "0.1.3") (deps (list (crate-dep (name "braille-ascii") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1.0.46") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.21") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2.0.52") (default-features #t) (kind 0)))) (hash "1ib0310fg9l2hwwh8dbmi9yfi30raj03pwwhgcgp87wyzywkfil4")))

