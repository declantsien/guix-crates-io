(define-module (crates-io br im) #:use-module (crates-io))

(define-public crate-brim-2 (crate (name "brim") (vers "2.1.0") (deps (list (crate-dep (name "sarge") (req "^3.0.1") (default-features #t) (kind 0)))) (hash "16fmrh73lq6jq4f4mj2yfqv5pw53scxvq5hl5w919c242iw3q6b0") (features (quote (("default") ("debug"))))))

(define-public crate-brim-2 (crate (name "brim") (vers "2.2.0") (deps (list (crate-dep (name "sarge") (req "^3.0.1") (default-features #t) (kind 0)))) (hash "00f1d412jzrcbx1flgwr516hcq4g71sh4wlfvnc9ww5vbf7grn7v") (features (quote (("default") ("debug"))))))

(define-public crate-brim-2 (crate (name "brim") (vers "2.2.1") (deps (list (crate-dep (name "sarge") (req "^3.0.1") (default-features #t) (kind 0)))) (hash "01l4y7394lxk6y0x10pblxpcsahqhw7d40n3yzciv8iwrysyzssa") (features (quote (("default") ("debug"))))))

(define-public crate-brim-2 (crate (name "brim") (vers "2.2.2") (deps (list (crate-dep (name "sarge") (req "^3.0.1") (default-features #t) (kind 0)))) (hash "11fhpram4yax33p03mnz9y7ca3rvvzkl6kwb0lzxlkavx2iay81v") (features (quote (("default") ("debug"))))))

(define-public crate-brim-3 (crate (name "brim") (vers "3.0.0") (deps (list (crate-dep (name "sarge") (req "^3.0.1") (default-features #t) (kind 0)))) (hash "13i0kqvvxpmjv6d0ghcygn2hlz2njwvq9ck7k7z3b9bs0k3kbn0w") (features (quote (("default") ("debug"))))))

(define-public crate-brim-3 (crate (name "brim") (vers "3.0.1") (deps (list (crate-dep (name "sarge") (req "^4.0.2") (default-features #t) (kind 0)))) (hash "1zm84w0nifagdi43l8yjj9h8f6f94qlgdx37y7iqnlxaqydj5x6z") (features (quote (("default") ("debug"))))))

(define-public crate-brim-3 (crate (name "brim") (vers "3.0.3") (deps (list (crate-dep (name "sarge") (req "^4.0.2") (default-features #t) (kind 0)))) (hash "0mkcfdvv2x8yk4hzpy4r3qpgzgdqahgr4c78binnjlxxn2zqa9kd") (features (quote (("default") ("debug"))))))

(define-public crate-brim-4 (crate (name "brim") (vers "4.0.0") (deps (list (crate-dep (name "sarge") (req "^7.0") (default-features #t) (kind 0)))) (hash "00wrzz9k3hdjjc44lgnhd8gaabv70c6jssszzybd3si9hbcbfv9a") (features (quote (("wide_cell") ("signed_cell") ("nowrap") ("dynamic_array") ("default") ("debug"))))))

(define-public crate-brim-4 (crate (name "brim") (vers "4.0.1") (deps (list (crate-dep (name "sarge") (req "^7.0") (default-features #t) (kind 0)))) (hash "1x5kvxp7ksg78pav3mva3ipla80q4d0q70kjkmrd6bsz66shwcrh") (features (quote (("wide_cell") ("signed_cell") ("nowrap") ("dynamic_array") ("default") ("debug"))))))

(define-public crate-brim-4 (crate (name "brim") (vers "4.1.0") (deps (list (crate-dep (name "sarge") (req "^7.0") (default-features #t) (kind 0)))) (hash "1wr4wv0sjmpzv6zxxf8q8yzjlkhl9g82jwlci0yd608m0yyxpakx") (features (quote (("wide_cell") ("signed_cell") ("nowrap") ("dynamic_array") ("default") ("debug"))))))

