(define-module (crates-io br ah) #:use-module (crates-io))

(define-public crate-brahe-0.0.1 (crate (name "brahe") (vers "0.0.1") (deps (list (crate-dep (name "approx") (req "^0.5.1") (default-features #t) (kind 2)) (crate-dep (name "kd-tree") (req "^0.5.1") (default-features #t) (kind 0)) (crate-dep (name "nalgebra") (req "^0.32.3") (default-features #t) (kind 0)) (crate-dep (name "once_cell") (req "^1.19.0") (default-features #t) (kind 0)) (crate-dep (name "rsofa") (req "^0.5.0") (default-features #t) (kind 0)) (crate-dep (name "serial_test") (req "^2.0.0") (default-features #t) (kind 2)))) (hash "1r92xns2pnl4lrcq240j569185gl8kpx2g65gm81ifbmmg8z5jjw")))

(define-public crate-brahma-0.0.1 (crate (name "brahma") (vers "0.0.1") (hash "0dxkxn8d6j9c6k4ma2bfhyl5x3wynymkfn5mafkra74wrr5giz0z")))

(define-public crate-brahma_graph-0.0.1 (crate (name "brahma_graph") (vers "0.0.1") (hash "12i8cycvpn2y2ya8ar8saryv69d56y20b49rh20xdrgfv9bzcpnf")))

(define-public crate-brahma_lsp-0.0.1 (crate (name "brahma_lsp") (vers "0.0.1") (hash "06k6l1xpx0ryqhia3scqxnm3qz87vwxanrly4gak8a9l723rssyh")))

(define-public crate-brahma_scribe-0.0.1 (crate (name "brahma_scribe") (vers "0.0.1") (hash "0s9b1p0f3pf3v0dpvlmv75gkpq0cx4qzcr9xp688pjvypana9jcd")))

(define-public crate-brahma_ui-0.1 (crate (name "brahma_ui") (vers "0.1.0") (hash "1dp9rysrkiqifiq8xi7j56mpngdfl2rbhp7f118b5i2fjmqwni3y") (yanked #t)))

(define-public crate-brahma_ui-0.0.1 (crate (name "brahma_ui") (vers "0.0.1") (hash "00hbwi99si56xhkrkjk5hmm7n9spbjgxa0bdmr9bkh4vkf8h9ynr")))

(define-public crate-brahma_yantra-0.0.1 (crate (name "brahma_yantra") (vers "0.0.1") (hash "0p0z2cvrwxc2wckif8g74fg24hnifsrk6ysh1wr6i2bqvwxf28gm")))

