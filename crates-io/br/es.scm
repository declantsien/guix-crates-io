(define-module (crates-io br es) #:use-module (crates-io))

(define-public crate-bresenham-0.1 (crate (name "bresenham") (vers "0.1.0") (hash "16d8xxwwcbzzad35nrxh2pyy9r729nj9mp3zmcrcjs304bjdhk4z")))

(define-public crate-bresenham-0.1 (crate (name "bresenham") (vers "0.1.1") (hash "1mvg3zcyll0m3z79jwbg183ha4kb7bw06rd286ijwvgn4mi13hdz")))

(define-public crate-bresenham_zip-0.1 (crate (name "bresenham_zip") (vers "0.1.0") (deps (list (crate-dep (name "line_drawing") (req "^1.0.0") (default-features #t) (kind 0)))) (hash "0ll1sji6w4dmbqf2qmh6y0q8fda0zy0c2cjafb6zci4cx9jypbmp")))

(define-public crate-bresenham_zip-0.1 (crate (name "bresenham_zip") (vers "0.1.1") (deps (list (crate-dep (name "line_drawing") (req "^1.0.0") (default-features #t) (kind 0)))) (hash "0miyap05j6aj92h70phabgpwdl6x5y2rd56ya0i4m63gx3nlpfig")))

(define-public crate-bresenham_zip-1 (crate (name "bresenham_zip") (vers "1.0.0") (deps (list (crate-dep (name "line_drawing") (req "^1.0.0") (default-features #t) (kind 0)))) (hash "1gk8j7p1x81cyn6adf3q7zjsq0v2ld057j8raar1bfhvlcq93pfg")))

(define-public crate-brest-0.1 (crate (name "brest") (vers "0.1.0") (deps (list (crate-dep (name "num-traits") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "1.*") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "09xcyll0pgaa5xysg66a8dlv6vqhlfnqmym7c7x1frjxp1k321xf")))

(define-public crate-brest-0.1 (crate (name "brest") (vers "0.1.1") (deps (list (crate-dep (name "num-traits") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "1.*") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "1ch6p64gg6lvicqyd9qd5x99fk2z5sr22b5kpadqdx1p82ahqbyf")))

(define-public crate-brest-0.1 (crate (name "brest") (vers "0.1.2") (deps (list (crate-dep (name "num-traits") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "1.*") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "1dl0vvjk703vj7bl4grz4gzjm4j4rzz355sqq8bcrr6c6d4a61ym")))

(define-public crate-brest-0.1 (crate (name "brest") (vers "0.1.3") (deps (list (crate-dep (name "num-traits") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "1.*") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "0w34baasm3mg2wyc95dy0yc89gg06jijbbl0bf79pjpakrs5icwf")))

(define-public crate-brest-0.1 (crate (name "brest") (vers "0.1.4") (deps (list (crate-dep (name "num-traits") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "1.*") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "1d0labddw08ynplmsb8rn7b9yc8d1g3qyflqs80vaq1sipzdksz0")))

(define-public crate-brest-0.1 (crate (name "brest") (vers "0.1.5") (deps (list (crate-dep (name "num-traits") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "schemars") (req "^0.8") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "1.*") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "080q5qkkch0ylh8nadz35h6a4dzqlwwlbb1dgwqgrwwf7fb2iiwx") (v 2) (features2 (quote (("schemars" "dep:schemars"))))))

