(define-module (crates-io rl vm) #:use-module (crates-io))

(define-public crate-rlvm-0.0.1 (crate (name "rlvm") (vers "0.0.1") (deps (list (crate-dep (name "cc") (req "^1.0.37") (default-features #t) (kind 1)))) (hash "1l3mh40b9nxricbifjksis9n4wp8n64008b7sa6y7p3kwwbif3d8")))

(define-public crate-rlvm-0.0.2 (crate (name "rlvm") (vers "0.0.2") (deps (list (crate-dep (name "cc") (req "^1.0.37") (default-features #t) (kind 1)))) (hash "1m2b7jy4kq4fhj72dwwbkf1zg40bs2ibxha76ds668zz74y9bm1c")))

