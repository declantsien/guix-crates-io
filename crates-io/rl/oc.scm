(define-module (crates-io rl oc) #:use-module (crates-io))

(define-public crate-rlocc-0.2 (crate (name "rlocc") (vers "0.2.1") (deps (list (crate-dep (name "crossbeam-channel") (req "^0.4.2") (default-features #t) (kind 0)) (crate-dep (name "crossbeam-utils") (req "^0.7.2") (default-features #t) (kind 0)) (crate-dep (name "num_cpus") (req "^1.12.0") (default-features #t) (kind 0)) (crate-dep (name "once_cell") (req "^1.3.1") (default-features #t) (kind 0)))) (hash "1s1iqghrb9mfja72n1ida6hmsk9afp21yl376digjvcvdwsrndja")))

