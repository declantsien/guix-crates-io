(define-module (crates-io rl ap) #:use-module (crates-io))

(define-public crate-rlapack-0.0.1 (crate (name "rlapack") (vers "0.0.1") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "num") (req "^0.1") (default-features #t) (kind 0)))) (hash "1dfiy399p3bkyi8rlgnzmm0ncrps72v3vq8509dz2gmhmb7vnbbq")))

(define-public crate-rlapack-0.0.2 (crate (name "rlapack") (vers "0.0.2") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "num") (req "^0.1") (default-features #t) (kind 0)))) (hash "0qfg2m5pghrni25c2460fm9f1y67vzqxa8r3r1rpn2bw4wi39wzm")))

(define-public crate-rlapack-0.0.3 (crate (name "rlapack") (vers "0.0.3") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "num") (req "^0.1") (default-features #t) (kind 0)))) (hash "1dbig9kxrjyiqp2l0k7r4gksyi2dphdhsshyzyfk9i3qsqpk4p9h")))

(define-public crate-rlapack-0.0.4 (crate (name "rlapack") (vers "0.0.4") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "num") (req "^0.1") (default-features #t) (kind 0)))) (hash "0p71saz935pcyqv9h8gzyr5d8b38sa6hdidcw517illcnj3rqnbm")))

(define-public crate-rlapack-0.0.5 (crate (name "rlapack") (vers "0.0.5") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "num") (req "^0.1") (default-features #t) (kind 0)))) (hash "0qsd6rajpl83sra9i82csyb0pa96ma846fzf2pwqpg0g8z8ig7rj")))

