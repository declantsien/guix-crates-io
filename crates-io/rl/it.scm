(define-module (crates-io rl it) #:use-module (crates-io))

(define-public crate-rlite-0.0.1 (crate (name "rlite") (vers "0.0.1") (deps (list (crate-dep (name "libc") (req "^0.1.10") (default-features #t) (kind 0)))) (hash "13bdbr1j872fhl0wy9c8bvfnlgnj76yiyfz15p18wjvq2lv3vmwh")))

