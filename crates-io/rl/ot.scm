(define-module (crates-io rl ot) #:use-module (crates-io))

(define-public crate-rlottie-0.1 (crate (name "rlottie") (vers "0.1.0") (deps (list (crate-dep (name "rlottie-sys") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "1z9prh6f5hrpd1ff8p0f4m4cs185cxflyaxhimic9fwblhwrk9hx") (rust-version "1.56")))

(define-public crate-rlottie-0.2 (crate (name "rlottie") (vers "0.2.0") (deps (list (crate-dep (name "rlottie-sys") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "1v79c5yiz6dj5a8l46bf3ngcbp4iqnxv3v7pxbm555xkd4zms15w") (rust-version "1.56")))

(define-public crate-rlottie-0.2 (crate (name "rlottie") (vers "0.2.1") (deps (list (crate-dep (name "rlottie-sys") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "19wkp7bybkmcv9znrl2cr7qs0mhr6dm34g625c39pamqmcs2s1sn") (yanked #t) (rust-version "1.56")))

(define-public crate-rlottie-0.2 (crate (name "rlottie") (vers "0.2.2") (deps (list (crate-dep (name "rlottie-sys") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "0331xs7q6s2d4plpax7rhxbn7l96m8dmrp2bzxv5wi7xjnd83n1h") (rust-version "1.56")))

(define-public crate-rlottie-0.3 (crate (name "rlottie") (vers "0.3.0") (deps (list (crate-dep (name "rgb") (req "^0.8.32") (kind 0)) (crate-dep (name "rlottie-sys") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "1g1x4naavjia4lbnimc7xj5cqf84nkfd06hl8mx4cn2d12l2iba0") (rust-version "1.56")))

(define-public crate-rlottie-0.3 (crate (name "rlottie") (vers "0.3.1") (deps (list (crate-dep (name "rgb") (req "^0.8.32") (kind 0)) (crate-dep (name "rlottie-sys") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "0g5yj5n14z1bbb8889rvpzcqmc8m6i33g5l2301fsh1symbdxwy9") (rust-version "1.56")))

(define-public crate-rlottie-0.4 (crate (name "rlottie") (vers "0.4.0") (deps (list (crate-dep (name "rgb") (req "^0.8.32") (kind 0)) (crate-dep (name "rlottie-sys") (req "^0.2.0") (default-features #t) (kind 0)))) (hash "0sfyby3566795crfhhs149v9sl3i45m9c7lg36j10m06fncs5vw9") (rust-version "1.56")))

(define-public crate-rlottie-0.4 (crate (name "rlottie") (vers "0.4.1") (deps (list (crate-dep (name "rgb") (req "^0.8.32") (kind 0)) (crate-dep (name "rlottie-sys") (req "^0.2.0") (default-features #t) (kind 0)))) (hash "1pkb2440ayga70vgkkms3akxb7rhnvfwvg8hs97h9yz2y783dza1") (rust-version "1.56")))

(define-public crate-rlottie-0.5 (crate (name "rlottie") (vers "0.5.0") (deps (list (crate-dep (name "rgb") (req "^0.8.32") (kind 0)) (crate-dep (name "rlottie-sys") (req "^0.2") (default-features #t) (kind 0)))) (hash "147p8h7ijnaxyr62a8n2j3dpc9hd3m0xzibifkwxlhgnfc17bn4b") (rust-version "1.56")))

(define-public crate-rlottie-0.5 (crate (name "rlottie") (vers "0.5.1") (deps (list (crate-dep (name "rgb") (req "^0.8.32") (kind 0)) (crate-dep (name "rlottie-sys") (req "^0.2") (default-features #t) (kind 0)))) (hash "0wl6axzbk6ky8bg4bb2gf14flfjxc3qnb26pybx22p079ddzy3qb") (rust-version "1.56")))

(define-public crate-rlottie-0.5 (crate (name "rlottie") (vers "0.5.2") (deps (list (crate-dep (name "rgb") (req "^0.8.32") (kind 0)) (crate-dep (name "rlottie-sys") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)))) (hash "093ls2snr1ydks5s0mfvb6a9ca43d8mapc7p3wa05g2k5da3vcdp") (v 2) (features2 (quote (("serde" "dep:serde" "rgb/serde")))) (rust-version "1.60")))

(define-public crate-rlottie-rs-0.1 (crate (name "rlottie-rs") (vers "0.1.0") (deps (list (crate-dep (name "bindgen") (req "^0.53.1") (default-features #t) (kind 1)))) (hash "1ycrnzh0spil4f4hpj5sad2678ayphl32zh6pk34g2mickifsdvx") (yanked #t)))

(define-public crate-rlottie-sys-0.1 (crate (name "rlottie-sys") (vers "0.1.0") (deps (list (crate-dep (name "bindgen") (req "^0.59.1") (default-features #t) (kind 1)) (crate-dep (name "pkg-config") (req "^0.3.22") (default-features #t) (kind 1)))) (hash "1qwp4f7kpfyrvxd9paf83awg6c30b6wvgmlrypxd6yxvj5b4wngk") (links "rlottie") (rust-version "1.56")))

(define-public crate-rlottie-sys-0.2 (crate (name "rlottie-sys") (vers "0.2.0") (deps (list (crate-dep (name "bindgen") (req "^0.59.1") (default-features #t) (kind 1)) (crate-dep (name "pkg-config") (req "^0.3.22") (default-features #t) (kind 1)))) (hash "1y3w61qzjv9gxszzhvnjm2vb88p8mwyb25pwppccwpza5bp3jif4") (links "rlottie") (rust-version "1.56")))

(define-public crate-rlottie-sys-0.2 (crate (name "rlottie-sys") (vers "0.2.1") (deps (list (crate-dep (name "bindgen") (req "^0.60.1") (kind 1)) (crate-dep (name "pkg-config") (req "^0.3.22") (default-features #t) (kind 1)))) (hash "1024xxcqmv29sa2kyzl5dyd4xmi8brxmrj1q1x1kaj55jm38p5qh") (yanked #t) (links "rlottie") (rust-version "1.56")))

(define-public crate-rlottie-sys-0.2 (crate (name "rlottie-sys") (vers "0.2.2") (deps (list (crate-dep (name "bindgen") (req "^0.60.1") (features (quote ("runtime"))) (kind 1)) (crate-dep (name "pkg-config") (req "^0.3.22") (default-features #t) (kind 1)))) (hash "1m180wbrb21ybfgcr9mdqpq5pwbh89snxvz7d4yd87k25i8iq94y") (links "rlottie") (rust-version "1.56")))

(define-public crate-rlottie-sys-0.2 (crate (name "rlottie-sys") (vers "0.2.3") (deps (list (crate-dep (name "bindgen") (req "^0.60.1") (features (quote ("runtime"))) (kind 1)) (crate-dep (name "pkg-config") (req "^0.3.22") (default-features #t) (kind 1)))) (hash "18bx80wgmh4163qain88l7idrz69aaamqkar0z7mgzwbrix294js") (links "rlottie") (rust-version "1.56")))

(define-public crate-rlottie-sys-0.2 (crate (name "rlottie-sys") (vers "0.2.4") (deps (list (crate-dep (name "bindgen") (req "^0.61.0") (features (quote ("runtime"))) (kind 1)) (crate-dep (name "pkg-config") (req "^0.3.22") (default-features #t) (kind 1)))) (hash "0whqdqbfin8j1fqr33gps9iyv99jjcsbpn8838irdpamvxgqjjzg") (links "rlottie") (rust-version "1.56")))

(define-public crate-rlottie-sys-0.2 (crate (name "rlottie-sys") (vers "0.2.5") (deps (list (crate-dep (name "bindgen") (req "^0.63.0") (features (quote ("runtime"))) (kind 1)) (crate-dep (name "pkg-config") (req "^0.3.22") (default-features #t) (kind 1)))) (hash "10ivjha1smz14kfkqmj7ycxhrdr4y35gahi911x02a5ihcfk7gnb") (links "rlottie") (rust-version "1.56")))

(define-public crate-rlottie-sys-0.2 (crate (name "rlottie-sys") (vers "0.2.6") (deps (list (crate-dep (name "bindgen") (req "^0.64.0") (features (quote ("runtime"))) (kind 1)) (crate-dep (name "pkg-config") (req "^0.3.22") (default-features #t) (kind 1)))) (hash "1qcw803dcf7jm7d850yf260lpmn6gh0853cvfjicspqipw2iaxq0") (links "rlottie") (rust-version "1.56")))

(define-public crate-rlottie-sys-0.2 (crate (name "rlottie-sys") (vers "0.2.7") (deps (list (crate-dep (name "bindgen") (req "^0.65.1") (features (quote ("runtime"))) (kind 1)) (crate-dep (name "pkg-config") (req "^0.3.22") (default-features #t) (kind 1)))) (hash "0zl80cyni97f41j4w49icxf21vl0rakbc50jv6qngw14x9s24sln") (links "rlottie") (rust-version "1.56")))

(define-public crate-rlottie-sys-0.2 (crate (name "rlottie-sys") (vers "0.2.8") (deps (list (crate-dep (name "bindgen") (req "^0.66.1") (features (quote ("prettyplease" "runtime"))) (kind 1)) (crate-dep (name "pkg-config") (req "^0.3.22") (default-features #t) (kind 1)))) (hash "00zyk6rp4ma6hckdwbz2mq4mvr4844pi7lk2ys0b8bhlq6dmhsjr") (links "rlottie") (rust-version "1.56")))

(define-public crate-rlottie-sys-0.2 (crate (name "rlottie-sys") (vers "0.2.9") (deps (list (crate-dep (name "bindgen") (req "^0.68.1") (features (quote ("prettyplease" "runtime"))) (kind 1)) (crate-dep (name "pkg-config") (req "^0.3.22") (default-features #t) (kind 1)))) (hash "1jrxmjnjs5bcvgd4zj2zq6kjr6mqq32j9y6j7y0j83bzlxz3bvr4") (links "rlottie") (rust-version "1.56")))

