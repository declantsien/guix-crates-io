(define-module (crates-io rl we) #:use-module (crates-io))

(define-public crate-rlwe-0.1 (crate (name "rlwe") (vers "0.1.0") (deps (list (crate-dep (name "generic-array") (req "^0.14.5") (default-features #t) (kind 0)) (crate-dep (name "num-bigint") (req "^0.4.3") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2.14") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 2)) (crate-dep (name "typenum") (req "^1.15.0") (default-features #t) (kind 0)))) (hash "0ab6a2amb4mc1q8b24sfvclhl8zq14dn1kb07w0wxqxlns77l6c2")))

