(define-module (crates-io rl -b) #:use-module (crates-io))

(define-public crate-rl-bandit-1 (crate (name "rl-bandit") (vers "1.0.0") (deps (list (crate-dep (name "rand") (req "^0.7.3") (default-features #t) (kind 0)))) (hash "14mr4a15ci9bvn7cqa6xri3ypmg5w334hc179d0p0pm39il2ppnr")))

(define-public crate-rl-bandit-1 (crate (name "rl-bandit") (vers "1.0.1") (deps (list (crate-dep (name "rand") (req "^0.7.3") (default-features #t) (kind 0)))) (hash "0vc9vxvqayfxvhba254yka236i0lxqkkv9win5i06yngc787839b")))

(define-public crate-rl-bandit-1 (crate (name "rl-bandit") (vers "1.0.2") (deps (list (crate-dep (name "rand") (req "^0.7.3") (default-features #t) (kind 0)))) (hash "0dkpk86a0dsdyxn6wg3p1vk49q2jb8fvwp1cd1rkzsrns9wc82mb")))

