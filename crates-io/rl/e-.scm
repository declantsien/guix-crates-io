(define-module (crates-io rl e-) #:use-module (crates-io))

(define-public crate-rle-bitset-0.1 (crate (name "rle-bitset") (vers "0.1.0") (deps (list (crate-dep (name "primitive_traits") (req "^0.1") (default-features #t) (kind 0)))) (hash "15diclc7ifns2md8by6raw9mfkzabnrnpdczb1isl53x1n0mh6if")))

(define-public crate-rle-decode-fast-1 (crate (name "rle-decode-fast") (vers "1.0.0") (deps (list (crate-dep (name "criterion") (req "^0.2") (optional #t) (default-features #t) (kind 0)))) (hash "0xpxkbnj2rz7066hhb46wiq67w5lky9k4qyaar8j0n881l4y9ldg") (features (quote (("bench" "criterion"))))))

(define-public crate-rle-decode-fast-1 (crate (name "rle-decode-fast") (vers "1.0.1") (deps (list (crate-dep (name "criterion") (req "^0.2") (optional #t) (default-features #t) (kind 0)))) (hash "1b4h7qs4mssc5dnlhs3f91ya8pb40bv72zzshl18gify2jllzgna") (features (quote (("bench" "criterion"))))))

(define-public crate-rle-decode-fast-1 (crate (name "rle-decode-fast") (vers "1.0.2") (deps (list (crate-dep (name "criterion") (req "^0.2") (optional #t) (default-features #t) (kind 0)))) (hash "0647n1p09237j62y1qfz4lqsxaib09661x5wz3vrf0fc4l7khvj6") (features (quote (("bench" "criterion"))))))

(define-public crate-rle-decode-fast-1 (crate (name "rle-decode-fast") (vers "1.0.3") (deps (list (crate-dep (name "criterion") (req "^0.2") (optional #t) (default-features #t) (kind 0)))) (hash "08kljzl29rpm12fiz0qj5pask49aiswdvcjigdcq73s224rgd0im") (features (quote (("bench" "criterion"))))))

(define-public crate-rle-decode-helper-1 (crate (name "rle-decode-helper") (vers "1.0.0-alpha") (deps (list (crate-dep (name "criterion") (req "^0.2") (default-features #t) (kind 2)))) (hash "0jh8h4rqkmckchp40grzkxgkxbjbrgk2gm8fd2fzcdj9qanvdyvd") (yanked #t)))

(define-public crate-rle-decode-helper-1 (crate (name "rle-decode-helper") (vers "1.0.0-alpha2") (deps (list (crate-dep (name "criterion") (req "^0.2") (default-features #t) (kind 2)))) (hash "1rhvi778dpz2kvm6g6in5ilbialy8g337hbrxyz1mjcb34awkizw") (yanked #t)))

