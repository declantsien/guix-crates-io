(define-module (crates-io rl p-) #:use-module (crates-io))

(define-public crate-rlp-compress-0.1 (crate (name "rlp-compress") (vers "0.1.0") (deps (list (crate-dep (name "elastic-array") (req "^0.10") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "tetsy-rlp") (req "^0.4.5") (default-features #t) (kind 0)))) (hash "13142zva6cmcvhyy9qy3757h11fskvid0sna9j6ckq0wgpwcw5jy")))

(define-public crate-rlp-derive-0.1 (crate (name "rlp-derive") (vers "0.1.0") (deps (list (crate-dep (name "proc-macro2") (req "^1.0.8") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.2") (default-features #t) (kind 0)) (crate-dep (name "rlp") (req "^0.4.4") (default-features #t) (kind 2)) (crate-dep (name "syn") (req "^1.0.14") (default-features #t) (kind 0)))) (hash "0ak73xi7zpw0zs5n30w6j2jmwfqdyb9hfagyjy3hsd0cpqm7ngg3")))

(define-public crate-rlp-iter-0.2 (crate (name "rlp-iter") (vers "0.2.0") (deps (list (crate-dep (name "bit-vec") (req "^0.6.3") (default-features #t) (kind 0)))) (hash "0zxwsl8fibzsawsnz9idvy8603q4hvdwykbgsimawfipp2j4bqqd")))

(define-public crate-rlp-iter-0.2 (crate (name "rlp-iter") (vers "0.2.1") (deps (list (crate-dep (name "bit-vec") (req "^0.6.3") (default-features #t) (kind 0)))) (hash "0ag1svl9bhkj8zb2c5xqkf0k4w6hr6s65pkbhnl8w373yvccgjw6")))

