(define-module (crates-io rl -s) #:use-module (crates-io))

(define-public crate-rl-sys-0.0.1 (crate (name "rl-sys") (vers "0.0.1") (hash "0v7a0scwxbixchwm6w1cvihjd612nbdncdxp5j00ckdj53d9b9pm")))

(define-public crate-rl-sys-0.0.2 (crate (name "rl-sys") (vers "0.0.2") (hash "00fr46ccq9fy47z54nk55ckw3y8x07v3868bhvwj1cg1il8dbr6d")))

(define-public crate-rl-sys-0.0.3 (crate (name "rl-sys") (vers "0.0.3") (hash "16i16n6y105fcnqc364zjrw3zrbzgwn8rjn0h0pf3wippvn973pa")))

(define-public crate-rl-sys-0.0.4 (crate (name "rl-sys") (vers "0.0.4") (hash "14y9v7zy3msz47awbza1l74ndmd27fs523svh6cnkpv7scd5wh0w")))

(define-public crate-rl-sys-0.0.5 (crate (name "rl-sys") (vers "0.0.5") (hash "00cppf84im5a1inb0lr02dzg7mdbnh104xnsdcanzkaavak74n34")))

(define-public crate-rl-sys-0.1 (crate (name "rl-sys") (vers "0.1.0") (deps (list (crate-dep (name "libc") (req "*") (default-features #t) (kind 0)))) (hash "0x0f7ryih6631qyd417clzpdzwnq3nqdqhsx9nzzl52rs01krn51")))

(define-public crate-rl-sys-0.1 (crate (name "rl-sys") (vers "0.1.1") (deps (list (crate-dep (name "libc") (req "*") (default-features #t) (kind 0)))) (hash "1mjzbi8wzpfn9mjddndpr8kxrzmyd394iba0v9szlaa2xgb2spcp")))

(define-public crate-rl-sys-0.1 (crate (name "rl-sys") (vers "0.1.2") (deps (list (crate-dep (name "libc") (req "*") (default-features #t) (kind 0)))) (hash "0zxg6n2lgqvw6fxkgjbmb8qc88gxhz1nppm3h5ih9kg0hmcr0kn5")))

(define-public crate-rl-sys-0.2 (crate (name "rl-sys") (vers "0.2.0") (deps (list (crate-dep (name "libc") (req "~0.2.2") (default-features #t) (kind 0)))) (hash "18xx694zcj8i1pvx2jcrg57kvyg83nkfqyw4qw8d57fjvs3p0ryk")))

(define-public crate-rl-sys-0.2 (crate (name "rl-sys") (vers "0.2.1") (deps (list (crate-dep (name "libc") (req "~0.2.2") (default-features #t) (kind 0)))) (hash "0hy6lyk8qqmzq2dcpydyf8069npg96kzr4v8jjfr9lk9b0y7ascx")))

(define-public crate-rl-sys-0.2 (crate (name "rl-sys") (vers "0.2.2") (deps (list (crate-dep (name "libc") (req "~0.2.2") (default-features #t) (kind 0)) (crate-dep (name "log") (req "~0.3.4") (default-features #t) (kind 0)))) (hash "14lmn10ljzamzkvjj9cj8v1zgif8jhf1ldqpskf30rpqdi8wdjn7")))

(define-public crate-rl-sys-0.3 (crate (name "rl-sys") (vers "0.3.0") (deps (list (crate-dep (name "libc") (req "~0.2.2") (default-features #t) (kind 0)) (crate-dep (name "log") (req "~0.3.4") (default-features #t) (kind 0)) (crate-dep (name "sodium-sys") (req "~0.0.4") (default-features #t) (kind 2)) (crate-dep (name "vergen") (req "~0.0.16") (default-features #t) (kind 1)))) (hash "1xxl5whjcj9k2vbqhmibbdnkwjsazxxwp65f5ag25r4a0lvh9s84")))

(define-public crate-rl-sys-0.4 (crate (name "rl-sys") (vers "0.4.0") (deps (list (crate-dep (name "libc") (req "~0.2.2") (default-features #t) (kind 0)) (crate-dep (name "log") (req "~0.3.4") (default-features #t) (kind 0)) (crate-dep (name "sodium-sys") (req "~0.0.4") (default-features #t) (kind 2)) (crate-dep (name "vergen") (req "~0.0.16") (default-features #t) (kind 1)))) (hash "1wwqi7p11zb25k16sgzwcn8qj4jdq0jqp9krhmq98w5c5q3hb82f")))

(define-public crate-rl-sys-0.4 (crate (name "rl-sys") (vers "0.4.1") (deps (list (crate-dep (name "blastfig") (req "~0.3.3") (default-features #t) (kind 0)) (crate-dep (name "clippy") (req "~0.0.30") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "errno") (req "~0.1.5") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "~0.2.2") (default-features #t) (kind 0)) (crate-dep (name "log") (req "~0.3.4") (default-features #t) (kind 0)) (crate-dep (name "pkg-config") (req "~0.3.6") (default-features #t) (kind 1)) (crate-dep (name "sodium-sys") (req "~0.0.4") (default-features #t) (kind 2)) (crate-dep (name "time") (req "~0.1.34") (default-features #t) (kind 0)) (crate-dep (name "vergen") (req "~0.1.0") (default-features #t) (kind 1)))) (hash "0h50n672936v5scfybknzjmrlviwzbm6zlhpafh1kf6j4s7n69l9") (features (quote (("latest") ("default"))))))

(define-public crate-rl-sys-0.5 (crate (name "rl-sys") (vers "0.5.0") (deps (list (crate-dep (name "bitflags") (req "~0.7.0") (default-features #t) (kind 0)) (crate-dep (name "clippy") (req "~0.0.30") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "errno") (req "~0.1.5") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "~0.2.2") (default-features #t) (kind 0)) (crate-dep (name "log") (req "~0.3.4") (default-features #t) (kind 0)) (crate-dep (name "pkg-config") (req "~0.3.6") (default-features #t) (kind 1)) (crate-dep (name "sodium-sys") (req "~0.0.4") (default-features #t) (kind 2)) (crate-dep (name "time") (req "~0.1.34") (default-features #t) (kind 0)) (crate-dep (name "vergen") (req "~0.1.0") (default-features #t) (kind 1)))) (hash "13a5pqc5lgzaq29j6nyh9r50xb56a93a5zwsvcaifpqrmwpc905j") (features (quote (("lint" "clippy") ("default"))))))

(define-public crate-rl-sys-0.5 (crate (name "rl-sys") (vers "0.5.1") (deps (list (crate-dep (name "bitflags") (req "~0.7.0") (default-features #t) (kind 0)) (crate-dep (name "clippy") (req "~0.0.76") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "errno") (req "~0.1.5") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "~0.2.2") (default-features #t) (kind 0)) (crate-dep (name "log") (req "~0.3.4") (default-features #t) (kind 0)) (crate-dep (name "pkg-config") (req "~0.3.6") (default-features #t) (kind 1)) (crate-dep (name "sodium-sys") (req "~0.0.4") (default-features #t) (kind 2)) (crate-dep (name "time") (req "~0.1.34") (default-features #t) (kind 0)) (crate-dep (name "vergen") (req "~0.1.0") (default-features #t) (kind 1)))) (hash "17ayfznv7j0pk76xiacwdkiknsj7wmsfaaccj2vmy1x8296znphw") (features (quote (("lint" "clippy") ("default"))))))

(define-public crate-rl-sys-0.5 (crate (name "rl-sys") (vers "0.5.2") (deps (list (crate-dep (name "bitflags") (req "~0.7.0") (default-features #t) (kind 0)) (crate-dep (name "clippy") (req "~0.0.76") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "errno") (req "~0.1.5") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "~0.2.2") (default-features #t) (kind 0)) (crate-dep (name "log") (req "~0.3.4") (default-features #t) (kind 0)) (crate-dep (name "pkg-config") (req "~0.3.6") (default-features #t) (kind 1)) (crate-dep (name "sodium-sys") (req "~0.0.4") (default-features #t) (kind 2)) (crate-dep (name "time") (req "~0.1.34") (default-features #t) (kind 0)) (crate-dep (name "vergen") (req "~0.1.0") (default-features #t) (kind 1)))) (hash "13ipd15gj2x00k29zzs2nq1bmrjxz50v0c5jvbnrd0gwxskcmara") (features (quote (("lint" "clippy") ("default"))))))

