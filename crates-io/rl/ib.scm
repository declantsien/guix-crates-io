(define-module (crates-io rl ib) #:use-module (crates-io))

(define-public crate-rlib-0.0.1 (crate (name "rlib") (vers "0.0.1") (hash "1vycv2b38fhkdfz91va7c94ahrnx3w3nanfj4dgfd301fa8w8493")))

(define-public crate-rlibc-0.0.1 (crate (name "rlibc") (vers "0.0.1") (hash "1ln88v17xyy7nz7iyv0pj7wzfpx7281qcfiz2xnpsz467jr9sdf5")))

(define-public crate-rlibc-0.0.2 (crate (name "rlibc") (vers "0.0.2") (hash "0sdx0rp9p645cqxsvaqvpn3n3d33wv5fly2njk56zmw89az99a1s")))

(define-public crate-rlibc-0.1 (crate (name "rlibc") (vers "0.1.0") (hash "0iqg09mys6w6gf2ispck683vq0l758a5r068bh3srl11lcq5knni")))

(define-public crate-rlibc-0.1 (crate (name "rlibc") (vers "0.1.1") (hash "0aa8518dcv2w1b4xsyxkbaj8sc2gx5029rr1wfzmxayd42n021q2")))

(define-public crate-rlibc-0.1 (crate (name "rlibc") (vers "0.1.2") (hash "0rgcf5xcvyh795p4l4hsyyna6q7rvicmg5p4xbvkvx67wmlbzlhy")))

(define-public crate-rlibc-0.1 (crate (name "rlibc") (vers "0.1.3") (hash "0l2kjc608shqvi06lkdwk02v4l10qf7l1czcp6vgrv6z09g3h0vm")))

(define-public crate-rlibc-0.1 (crate (name "rlibc") (vers "0.1.4") (hash "0157rcxqvm911bi092h8qlk0m1v2ai2f6rjynaqmbqdnh3aif9f2")))

(define-public crate-rlibc-0.1 (crate (name "rlibc") (vers "0.1.5") (hash "0jxh6g7a3fs40r6yxwlpimnbnadnasqsvwb1kj34mv80psh5ywjg")))

(define-public crate-rlibc-1 (crate (name "rlibc") (vers "1.0.0") (hash "1zh8xs0j9s799vnj3n9a1r881as52al66rzijbbi9w35fw94p1zw")))

(define-public crate-rlibcex-0.1 (crate (name "rlibcex") (vers "0.1.0") (hash "17za30djzjivph29iyhnrjk2ab6glp9x38by19sb7hwm58rrc8is")))

(define-public crate-rlibcex-0.1 (crate (name "rlibcex") (vers "0.1.2") (hash "1kjazzkpb2qjv51n7apd1xr722pln85jm5jf29h0fq0mah12shs7")))

(define-public crate-rlibproject-0.1 (crate (name "rlibproject") (vers "0.1.0") (hash "1bm1wwrbs7paqcj294jyxjxjmz3dnz105b656rp3pcr4m64d6d06") (yanked #t)))

(define-public crate-rlibproject-0.1 (crate (name "rlibproject") (vers "0.1.1") (hash "0qjh3jkplq3fpyr3xzf3mbwpil4fikb2xx9qqfsv6ydf03jw96p4") (yanked #t)))

