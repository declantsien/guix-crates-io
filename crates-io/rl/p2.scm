(define-module (crates-io rl p2) #:use-module (crates-io))

(define-public crate-rlp2-0.2 (crate (name "rlp2") (vers "0.2.0") (deps (list (crate-dep (name "byteorder") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "elastic-array") (req "^0.7.0") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "rustc-serialize") (req "^0.3") (default-features #t) (kind 0)))) (hash "08pnac68m5fmvssmfdzf3wqqg2w2p78rfjk910fmc0ihdcn4fnv1")))

