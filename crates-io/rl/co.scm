(define-module (crates-io rl co) #:use-module (crates-io))

(define-public crate-rlcount-0.1 (crate (name "rlcount") (vers "0.1.0") (hash "152jdk4mdi5gbxmnz3y9mf33mdr7sa51mj8fy46frd3i0lx4qqxs")))

(define-public crate-rlcount-0.1 (crate (name "rlcount") (vers "0.1.1") (hash "1jbz1z068carb6x5c0i6kjrjiyvrspkhq6j6lpwixfgsszd3y0s1")))

(define-public crate-rlcount-0.2 (crate (name "rlcount") (vers "0.2.0") (deps (list (crate-dep (name "clap") (req "^2.33.3") (features (quote ("suggestions" "color"))) (kind 0)) (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "rayon") (req "^1.5.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.4.5") (default-features #t) (kind 0)) (crate-dep (name "walkdir") (req "^2.3.1") (default-features #t) (kind 0)))) (hash "1akl695y39jjrjszdc028klfds6r2vzap6mfh4p9j36rrl167bj7")))

(define-public crate-rlcount-0.2 (crate (name "rlcount") (vers "0.2.1") (deps (list (crate-dep (name "clap") (req "^2.33.3") (features (quote ("suggestions" "color"))) (kind 0)) (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "rayon") (req "^1.5.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.4.5") (default-features #t) (kind 0)) (crate-dep (name "walkdir") (req "^2.3.1") (default-features #t) (kind 0)))) (hash "1xdvljvgdvf7hs7dahc1fv846dpg0n7y48swl982kq6dmh35z31f")))

