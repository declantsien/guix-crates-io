(define-module (crates-io rl ig) #:use-module (crates-io))

(define-public crate-rlight-0.0.1 (crate (name "rlight") (vers "0.0.1") (hash "0flwbq80b1cwkyx63xamgqi2j0r7k3znzcyjsbcszkkwpal3z8rz")))

(define-public crate-rlight-0.1 (crate (name "rlight") (vers "0.1.0") (deps (list (crate-dep (name "at_debug") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "confy") (req "^0.5.1") (features (quote ("ron_conf"))) (kind 0)) (crate-dep (name "serde") (req "^1.0.183") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0.183") (default-features #t) (kind 0)) (crate-dep (name "v4l") (req "^0.14.0") (features (quote ("libv4l"))) (kind 0)))) (hash "14kaq82015zzx5rs597xwcjly024cx9ihp1mz6k4z95m6pji7awy")))

(define-public crate-rlight-0.1 (crate (name "rlight") (vers "0.1.1") (deps (list (crate-dep (name "at_debug") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "confy") (req "^0.5.1") (features (quote ("ron_conf"))) (kind 0)) (crate-dep (name "serde") (req "^1.0.183") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0.183") (default-features #t) (kind 0)) (crate-dep (name "v4l") (req "^0.14.0") (features (quote ("libv4l"))) (kind 0)))) (hash "0ygn66r8zwdqmdjb1vmypm8ghq710wdgwxy6c8ka0xxy5pp9sq5y")))

(define-public crate-rlight-0.2 (crate (name "rlight") (vers "0.2.0") (deps (list (crate-dep (name "brightness") (req "^0.5.0") (default-features #t) (kind 0)) (crate-dep (name "confy") (req "^0.5.1") (features (quote ("ron_conf"))) (kind 0)) (crate-dep (name "nokhwa") (req "^0.10.4") (features (quote ("input-native"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.183") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0.183") (default-features #t) (kind 0)))) (hash "0rqnk41nfq7n8sra03m08bmvf2zq7cc71xdhylniyd94kbqy306b")))

