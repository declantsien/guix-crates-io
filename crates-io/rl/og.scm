(define-module (crates-io rl og) #:use-module (crates-io))

(define-public crate-rlog-0.1 (crate (name "rlog") (vers "0.1.0") (deps (list (crate-dep (name "chrono") (req "^0.4") (default-features #t) (kind 0)))) (hash "016dj9smjc0nfl1sp9iigyzsdpifm6cwbx79s1s2jdswwhbw2swm")))

(define-public crate-rlog-0.1 (crate (name "rlog") (vers "0.1.1") (deps (list (crate-dep (name "chrono") (req "^0.4") (default-features #t) (kind 0)))) (hash "0l2wa095h2njnb434bcq9vi7hhkajy7kraqsik41clsk72iy0xm3")))

(define-public crate-rlog-0.1 (crate (name "rlog") (vers "0.1.2") (deps (list (crate-dep (name "chrono") (req "^0.4") (default-features #t) (kind 0)))) (hash "1jabadx8y2hwsyks8jils4f6lxgfc1lbvcb6sh3vd8m4lmfv5gb6")))

(define-public crate-rlog-0.1 (crate (name "rlog") (vers "0.1.3") (deps (list (crate-dep (name "chrono") (req "^0.4") (default-features #t) (kind 0)))) (hash "1n6zgh4r3bx2b6s0zyi345d0m3dn8spd4pzivaj200lp2d4d1d6g")))

(define-public crate-rlog-0.1 (crate (name "rlog") (vers "0.1.4") (deps (list (crate-dep (name "chrono") (req "^0.4") (default-features #t) (kind 0)))) (hash "004m5jisk7ajqx0ygprqhplw3sfrladmp7i1sfglzr58m2a49vj3")))

(define-public crate-rlog-0.2 (crate (name "rlog") (vers "0.2.0") (deps (list (crate-dep (name "chrono") (req "^0.4") (default-features #t) (kind 0)))) (hash "0ijh5734d2m3kxq94g7if1wmp8gy4kkvqjfwfaq816g5lyiqnvw2")))

(define-public crate-rlog-1 (crate (name "rlog") (vers "1.0.0") (deps (list (crate-dep (name "chrono") (req "^0.4") (default-features #t) (kind 0)))) (hash "1kvp52a1n6wffqxyjfzgiv4yfa51wivfkx26jmaxczb9dliyky8v")))

(define-public crate-rlogin-0.1 (crate (name "rlogin") (vers "0.1.0") (deps (list (crate-dep (name "nix") (req "^0.15.0") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.2.10") (default-features #t) (kind 0)))) (hash "16by9vmjy8pz9kal2q5x9f989gmc74605h621s6q0kdz4xgqrw5l") (yanked #t)))

(define-public crate-rlogs-0.1 (crate (name "rlogs") (vers "0.1.0") (deps (list (crate-dep (name "ansi_term") (req "^0.12.1") (default-features #t) (kind 0)) (crate-dep (name "chrono") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "env_logger") (req "^0.8.4") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.0") (default-features #t) (kind 0)))) (hash "04nx4kzivp3lwa88bl4dyd2vvgjf21n9jifylpxbw56cpsa8cr8d")))

(define-public crate-rlogs-0.1 (crate (name "rlogs") (vers "0.1.1") (deps (list (crate-dep (name "ansi_term") (req "^0.12.1") (default-features #t) (kind 0)) (crate-dep (name "chrono") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "env_logger") (req "^0.8.4") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.0") (default-features #t) (kind 0)))) (hash "1ssb8wcqpc0qrnlk002pjxv93szcipxzs6ls0cwllcaijbgiq091")))

(define-public crate-rlogs-0.1 (crate (name "rlogs") (vers "0.1.2") (deps (list (crate-dep (name "ansi_term") (req "^0.12.1") (default-features #t) (kind 0)) (crate-dep (name "chrono") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "env_logger") (req "^0.8.4") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.0") (default-features #t) (kind 0)))) (hash "14ficasvnwavm2bkmh21gzjpnkngvrny77372dafvl2bxhjmn9fc")))

