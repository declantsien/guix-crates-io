(define-module (crates-io rl da) #:use-module (crates-io))

(define-public crate-rldap-0.1 (crate (name "rldap") (vers "0.1.0") (deps (list (crate-dep (name "ldap3") (req "^0.11.1") (default-features #t) (kind 0)) (crate-dep (name "openssl") (req "^0.10.45") (features (quote ("vendored"))) (default-features #t) (kind 0)))) (hash "0k6lyhx4xfr3mk3481f1mlvbflp616dwvics86khmyr3xss802h3")))

