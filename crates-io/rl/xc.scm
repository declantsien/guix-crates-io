(define-module (crates-io rl xc) #:use-module (crates-io))

(define-public crate-rlxc-0.1 (crate (name "rlxc") (vers "0.1.0") (deps (list (crate-dep (name "anyhow") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^2.33.3") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^2.32") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "lxc-sys") (req ">=0.1.0") (default-features #t) (kind 0)) (crate-dep (name "prettytable-rs") (req "^0.8.0") (default-features #t) (kind 0)) (crate-dep (name "rayon") (req "^1.1") (default-features #t) (kind 0)) (crate-dep (name "xdg") (req "^2.1") (default-features #t) (kind 0)))) (hash "0apcwi56awssylax2p2vm7ii3pl03fbcz487idi39zr4gf2g3al5")))

