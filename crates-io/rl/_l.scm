(define-module (crates-io rl _l) #:use-module (crates-io))

(define-public crate-rl_localtime-0.1 (crate (name "rl_localtime") (vers "0.1.0") (deps (list (crate-dep (name "cc") (req "^1.0.72") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2.117") (default-features #t) (kind 0)))) (hash "0cngrscrgy6jxz6k0p7j6v5z1qj7jdi52ybzgpj6hcjr0qx95hd9")))

(define-public crate-rl_localtime-0.1 (crate (name "rl_localtime") (vers "0.1.1") (deps (list (crate-dep (name "cc") (req "^1.0.72") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2.117") (default-features #t) (kind 0)))) (hash "0a4czv44dc19f0khc2i9nlslpk2n35igmnjhxm70p8ir9hhbzn90")))

(define-public crate-rl_localtime-0.1 (crate (name "rl_localtime") (vers "0.1.2") (deps (list (crate-dep (name "cc") (req "^1.0.72") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2.117") (default-features #t) (kind 0)))) (hash "0pvp9qjrhmbg1vcnbb3hwm9gm77sgy0x6dz42116snkn8b2ghx7q")))

