(define-module (crates-io rl r-) #:use-module (crates-io))

(define-public crate-rlr-gtk-0.1 (crate (name "rlr-gtk") (vers "0.1.0") (deps (list (crate-dep (name "gtk") (req "^0.14.3") (default-features #t) (kind 0)))) (hash "1s0j4zkayh970h0i8i54pjl9kf5v0x6xbvn0m6gk0fadpb07kiaq")))

(define-public crate-rlr-gtk-0.1 (crate (name "rlr-gtk") (vers "0.1.1") (deps (list (crate-dep (name "gtk") (req "^0.14") (default-features #t) (kind 0)))) (hash "1apk7054pnnqv3rjvwhygb6ykv6k1p925jdkhbvkgz4y17brn43m")))

(define-public crate-rlr-gtk-0.1 (crate (name "rlr-gtk") (vers "0.1.2") (deps (list (crate-dep (name "gtk") (req "^0.14") (default-features #t) (kind 0)))) (hash "049kgrr6wm2vxgwwhnymv63lzw4af2sm0x3x81mvbm25cywbcg6c")))

