(define-module (crates-io rl nk) #:use-module (crates-io))

(define-public crate-rlnk-0.1 (crate (name "rlnk") (vers "0.1.0") (hash "1j6raha2k1b6wa8ddn68lyf0b9hg5jj7f8v6k6rza82b30bhmaan") (yanked #t)))

(define-public crate-rlnk-0.1 (crate (name "rlnk") (vers "0.1.1") (hash "01x1hzkwivjq8jidxn9f4chsyab99fa9100isyz1y4bpl3garz13") (yanked #t)))

(define-public crate-rlnk-0.1 (crate (name "rlnk") (vers "0.1.2") (hash "1lcdrbk887y4pxkpywxqsliyn83yfkb90g5rcj7zyscnm8lbbidp") (yanked #t)))

(define-public crate-rlnk-0.1 (crate (name "rlnk") (vers "0.1.3") (hash "199kqcah9yx0wj9nhgvkw63waad6knafwpnhky0kf69hm6f491kv") (yanked #t)))

(define-public crate-rlnk-0.1 (crate (name "rlnk") (vers "0.1.4") (hash "037idhl73arh0bpsw8a8vfc9q9wwpia8fm9z0vq96207npyl6grj") (yanked #t)))

(define-public crate-rlnk-0.1 (crate (name "rlnk") (vers "0.1.5") (hash "0dqm738dn0hq7b63y4i0x5j1llinzb8jbpp00a4n0393aggqcn3i") (yanked #t)))

(define-public crate-rlnk-0.1 (crate (name "rlnk") (vers "0.1.6") (hash "14zrb0nd65rw80g0mzx7n4313sbmi48ppw5zwmsk7l91pgm0iqzk") (yanked #t)))

(define-public crate-rlnk-0.1 (crate (name "rlnk") (vers "0.1.7") (hash "01371r48kqw2pvgkn6mladf315b4gwc77h7rzkckxh18ypgjld8w") (yanked #t)))

