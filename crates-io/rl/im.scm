(define-module (crates-io rl im) #:use-module (crates-io))

(define-public crate-rlimit-0.1 (crate (name "rlimit") (vers "0.1.0") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "11hdm982jri2abb4prc15p4703gd5sccy1imgld203qdgjjkdnym") (yanked #t)))

(define-public crate-rlimit-0.2 (crate (name "rlimit") (vers "0.2.0") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "05ivlm5p0a85rxn2ndi221zpk87y2nq71fakvlbmr69h4cy7l7x9") (yanked #t)))

(define-public crate-rlimit-0.2 (crate (name "rlimit") (vers "0.2.1") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "1czk6g94qb81da8dky4kkvcdrl6z4krf81ps5gf3h553micjz3q0") (yanked #t)))

(define-public crate-rlimit-0.3 (crate (name "rlimit") (vers "0.3.0") (deps (list (crate-dep (name "cfg-if") (req "^0.1.9") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "1s7ac6fi4q4f2djg5ljbch7ab3shyh9fq2gjqll2c7iax1jnxlp6") (yanked #t)))

(define-public crate-rlimit-0.4 (crate (name "rlimit") (vers "0.4.0") (deps (list (crate-dep (name "cfg-if") (req "^0.1.9") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "0dhm9jm1bvm4fk3839jw95gxs99yg0cwl9awwkyaclw3qdi2vc29") (yanked #t)))

(define-public crate-rlimit-0.5 (crate (name "rlimit") (vers "0.5.0") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)))) (hash "1kpibg8glj8rnnd954lk2m2niy1i5z553gkdyxm9by4jxwalpm4q")))

(define-public crate-rlimit-0.5 (crate (name "rlimit") (vers "0.5.1") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)))) (hash "034a4aazwr0xf6ksk40x6qm9ar04nyxliryiqjfx7sagzmpqwgc5")))

(define-public crate-rlimit-0.5 (crate (name "rlimit") (vers "0.5.2") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)))) (hash "05j3kqz4awl63ncw504fxsd7y8a9i715k6jf9rcmjlddyp3smrra")))

(define-public crate-rlimit-0.5 (crate (name "rlimit") (vers "0.5.3") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)))) (hash "0izqnfkc2wrbahnj2d149m650zarwfr03cyjj51z0la9gdslhwbf")))

(define-public crate-rlimit-0.5 (crate (name "rlimit") (vers "0.5.4") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)))) (hash "0807zkwsch3dxniv3w7nh3xvbxxm3b3r483wi7b4km5yxl1yvac1")))

(define-public crate-rlimit-0.6 (crate (name "rlimit") (vers "0.6.0") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "0dgxssi98sq1n4fm7w09kq6m2nv1l01i760gqiz81282j5vy9cp6") (yanked #t)))

(define-public crate-rlimit-0.6 (crate (name "rlimit") (vers "0.6.1") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "1qh687bm6xa2vv9ha2gjcw3bwqmmas2vmay6b90l9fcih4fl2zgf") (yanked #t)))

(define-public crate-rlimit-0.6 (crate (name "rlimit") (vers "0.6.2") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "09hpr6dfrx7xzlps7g25akqp2p32190vhcj3ymid6vrpaiaz42yc")))

(define-public crate-rlimit-0.7 (crate (name "rlimit") (vers "0.7.0") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "02s8a8zas1vlmrf951fl5g3kiphx4fz5fh8ljgkg3ba7msjh6xrl")))

(define-public crate-rlimit-0.8 (crate (name "rlimit") (vers "0.8.0") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "06rm11nzb7iqzz99k1dj61adbgjsb7pqylidzmm090vrgqj3s6c2") (yanked #t)))

(define-public crate-rlimit-0.8 (crate (name "rlimit") (vers "0.8.1") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "1bgqchjb7qi6k82bg9fdcc9kikaa5pxx1s3d183a9410czwwdg0g") (yanked #t)))

(define-public crate-rlimit-0.8 (crate (name "rlimit") (vers "0.8.2") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "0xdcvrr8hr2d9cv7gl9h490vgc2ryb915vf775vma4aln7kbc6vq")))

(define-public crate-rlimit-0.8 (crate (name "rlimit") (vers "0.8.3") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "18vsz3fdj4s8yjp96wwq7wvrlc3vzzsqki8mfpha9m5zr0g8l9zp")))

(define-public crate-rlimit-0.9 (crate (name "rlimit") (vers "0.9.0") (deps (list (crate-dep (name "libc") (req "^0.2.139") (default-features #t) (kind 0)) (crate-dep (name "once_cell") (req "^1.16.0") (default-features #t) (kind 2)))) (hash "09996hcr9hjn95w8hz8nmdfdkncy1ar4w21crxgypgz2wph7fp7a") (rust-version "1.59.0")))

(define-public crate-rlimit-0.9 (crate (name "rlimit") (vers "0.9.1") (deps (list (crate-dep (name "libc") (req "^0.2.139") (default-features #t) (kind 0)) (crate-dep (name "once_cell") (req "^1.16.0") (default-features #t) (kind 2)))) (hash "13mb3ligflqb4h7m76pkyc8z5pswpc38fcl6qm1lvp2jls3rv8pq") (rust-version "1.59.0")))

(define-public crate-rlimit-0.10 (crate (name "rlimit") (vers "0.10.0") (deps (list (crate-dep (name "libc") (req "^0.2.147") (default-features #t) (kind 0)) (crate-dep (name "once_cell") (req "^1.18.0") (default-features #t) (kind 2)))) (hash "16xnqyl72p4hzalgja9v6jr7jd26lfrkda4g9z931xhfpkh8nnwv") (rust-version "1.60.0")))

(define-public crate-rlimit-0.10 (crate (name "rlimit") (vers "0.10.1") (deps (list (crate-dep (name "libc") (req "^0.2.147") (default-features #t) (kind 0)) (crate-dep (name "once_cell") (req "^1.18.0") (default-features #t) (kind 2)))) (hash "1n6346gms599n33wgav7jr46pzj0fyh7il0ys08nvwd0607zfq1m") (rust-version "1.60.0")))

