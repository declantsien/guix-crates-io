(define-module (crates-io rl -c) #:use-module (crates-io))

(define-public crate-rl-core-0.0.0 (crate (name "rl-core") (vers "0.0.0") (deps (list (crate-dep (name "serde_derive") (req "^1.0.132") (optional #t) (default-features #t) (kind 0)))) (hash "15ir2065z1l2rariwmjfgshf218wsz7hw29f71hkhb1b4kms498n") (features (quote (("serde" "serde_derive"))))))

(define-public crate-rl-core-0.1 (crate (name "rl-core") (vers "0.1.0") (deps (list (crate-dep (name "serde") (req "^1.0.132") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0.132") (optional #t) (default-features #t) (kind 0)))) (hash "070yid5b2xdvsx8fs314c4d8295ic3hrscc8qnf340al9f9k25cs") (features (quote (("use_serde" "serde" "serde_derive"))))))

(define-public crate-rl-core-0.2 (crate (name "rl-core") (vers "0.2.0") (deps (list (crate-dep (name "serde") (req "^1.0.132") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0.132") (optional #t) (default-features #t) (kind 0)))) (hash "0zz9kzk7273qcbjkrljknqmi6pawx4l8f4nabyqc4bzw05ja6zak") (features (quote (("use_serde" "serde" "serde_derive"))))))

(define-public crate-rl-core-0.3 (crate (name "rl-core") (vers "0.3.0") (deps (list (crate-dep (name "serde") (req "^1.0.132") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0.132") (optional #t) (default-features #t) (kind 0)))) (hash "0a2ngl4nc04mjsngs9dmbv1p85dcy3094hbjbkzx2k7s4p5wnwf0") (features (quote (("use_serde" "serde" "serde_derive"))))))

(define-public crate-rl-core-0.4 (crate (name "rl-core") (vers "0.4.0") (deps (list (crate-dep (name "serde") (req "^1.0.132") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0.132") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.81") (default-features #t) (kind 2)))) (hash "0s3l2a859xnh56jb69206yszcm2r7ia7g9w1i0q3gl22a09ywzv1") (features (quote (("use_serde" "serde" "serde_derive"))))))

(define-public crate-rl-core-0.7 (crate (name "rl-core") (vers "0.7.0") (deps (list (crate-dep (name "serde") (req "^1.0.132") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0.132") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.81") (default-features #t) (kind 2)))) (hash "14lv3vdwslyjfwdk9kfsyv9p4l0hiklhbcw0irpxmhnp2xziw03h") (features (quote (("use_serde" "serde" "serde_derive"))))))

(define-public crate-rl-core-0.9 (crate (name "rl-core") (vers "0.9.0") (deps (list (crate-dep (name "serde") (req "^1.0.132") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0.132") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.81") (default-features #t) (kind 2)))) (hash "154p0h8xs83y6fblrsk32lkgifvq67k68lschzd027j45y6dqmbz") (features (quote (("use_serde" "serde" "serde_derive"))))))

(define-public crate-rl-core-0.10 (crate (name "rl-core") (vers "0.10.0") (deps (list (crate-dep (name "serde") (req "^1.0.132") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0.132") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.81") (default-features #t) (kind 2)))) (hash "03w5bq2b8dasmx60ygj4398d34c4snmk7kvigscywlvs5sc65gdy") (features (quote (("use_serde" "serde" "serde_derive"))))))

(define-public crate-rl-core-0.11 (crate (name "rl-core") (vers "0.11.0") (deps (list (crate-dep (name "serde") (req "^1.0.132") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.81") (default-features #t) (kind 2)))) (hash "0srfb6k0v9zlaxnhf9asxyqh4z6kbz352zqdk5ixydg449c21hhn")))

(define-public crate-rl-core-0.13 (crate (name "rl-core") (vers "0.13.0") (deps (list (crate-dep (name "serde") (req "^1.0.132") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.81") (default-features #t) (kind 2)))) (hash "0wzv6590l4dc6l2kzv9idp8nhbpxk4hrnfnqdj6cbzhs6fsgmw11")))

(define-public crate-rl-core-0.14 (crate (name "rl-core") (vers "0.14.0") (deps (list (crate-dep (name "serde") (req "^1.0.132") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.81") (default-features #t) (kind 2)))) (hash "069049vc10ds9qs75xam4k1j07r7js0pzbi1sx0rdg441z5yavwh")))

