(define-module (crates-io a- ma) #:use-module (crates-io))

(define-public crate-A-Mazed-0.1 (crate (name "A-Mazed") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^4.0.19") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "colored") (req "^2.0.0") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)))) (hash "00b101pa4dvl3kkzp15920wlgdr8xd5c6iy8xnhg84ijrynlbpn3")))

