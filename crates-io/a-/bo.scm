(define-module (crates-io a- bo) #:use-module (crates-io))

(define-public crate-a-bot-0.1 (crate (name "a-bot") (vers "0.1.0") (hash "1sdxr1m63h77rmrlbrrj46a9kxylw0hy7b7c70ssvhsbw6048gnk") (yanked #t)))

(define-public crate-a-bot-0.1 (crate (name "a-bot") (vers "0.1.1") (hash "086qymj76669ydbkdb5w3zs4iaghpjpjwss6wgc2pvrj3j87rhjx") (yanked #t)))

(define-public crate-a-bot-0.0.1 (crate (name "a-bot") (vers "0.0.1") (deps (list (crate-dep (name "bindgen") (req "^0.51.1") (default-features #t) (kind 0)))) (hash "12b5p7pl4bqb1jsqhgb8qzz3yn0wl1sshg6cvn9qvrygx6bj8wz8") (yanked #t)))

