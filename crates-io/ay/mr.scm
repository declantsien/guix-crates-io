(define-module (crates-io ay mr) #:use-module (crates-io))

(define-public crate-aymr-0.0.1 (crate (name "aymr") (vers "0.0.1") (deps (list (crate-dep (name "sled") (req "^0.34.7") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "zerocopy") (req "^0.7.32") (default-features #t) (kind 0)))) (hash "00jg57hrknjq8ib3vrbq5dw3f6vr0zgm14jqlgd8rczl48cgr4z4") (features (quote (("sled_pre" "sled") ("hashmap") ("default" "hashmap") ("btreemap"))))))

