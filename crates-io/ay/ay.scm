(define-module (crates-io ay ay) #:use-module (crates-io))

(define-public crate-ayaya-1 (crate (name "ayaya") (vers "1.0.0") (hash "1fv03ps5accihxvf16d9dddjdkiaqlafxflk1kw0cbv7sir3cff2")))

(define-public crate-ayaya-2 (crate (name "ayaya") (vers "2.0.0") (deps (list (crate-dep (name "miniz_oxide") (req "^0.4") (default-features #t) (kind 0)))) (hash "0px1mv14s1cx61dmqjspy4g01ripbl312rxrk019h98bn0wac62i")))

(define-public crate-ayaya-2 (crate (name "ayaya") (vers "2.0.1") (deps (list (crate-dep (name "miniz_oxide") (req "^0.4") (default-features #t) (kind 0)))) (hash "1frvx6sanpb6wcjirn770lzcq005x2qcq94jlsg9cb6lyhr9bywf")))

(define-public crate-ayaya-2 (crate (name "ayaya") (vers "2.0.2") (deps (list (crate-dep (name "miniz_oxide") (req "^0.5") (default-features #t) (kind 0)))) (hash "0lb9xshh2czkbqichgmvnq43xkycp4madmmy9ybgq42cb7z5kq55")))

