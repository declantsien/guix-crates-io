(define-module (crates-io ay ti) #:use-module (crates-io))

(define-public crate-aytina-1 (crate (name "aytina") (vers "1.0.0") (deps (list (crate-dep (name "clap") (req "^2.33") (features (quote ("yaml"))) (default-features #t) (kind 0)) (crate-dep (name "notify-rust") (req "^4.0") (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.10") (features (quote ("blocking" "json"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)))) (hash "0nlkxw4vasl6m3p73jj5p8y0k1dh045l2zk8q9v3ly6sq32d8zmy")))

