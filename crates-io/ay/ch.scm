(define-module (crates-io ay ch) #:use-module (crates-io))

(define-public crate-aych-delay-0.1 (crate (name "aych-delay") (vers "0.1.0") (deps (list (crate-dep (name "rodio") (req "^0.16.0") (default-features #t) (kind 2)))) (hash "1z8w8qriyl18s88hwr76n6zpvkv1109mgypn51xvc47jk8jnwjcc")))

(define-public crate-aych-delay-0.1 (crate (name "aych-delay") (vers "0.1.1") (deps (list (crate-dep (name "rodio") (req "^0.16.0") (default-features #t) (kind 2)))) (hash "1w5w28zgg81cq9qdvpm0k4vhckai0ykvl80zz1qxjg0yq81frl62")))

