(define-module (crates-io no -b) #:use-module (crates-io))

(define-public crate-no-bitches-0.1 (crate (name "no-bitches") (vers "0.1.0") (deps (list (crate-dep (name "image") (req "^0.24.2") (default-features #t) (kind 0)) (crate-dep (name "imageproc") (req "^0.23.0") (default-features #t) (kind 0)) (crate-dep (name "rust-embed") (req "^6.2.0") (default-features #t) (kind 0)) (crate-dep (name "rusttype") (req "^0.9.2") (default-features #t) (kind 0)))) (hash "0zq2rcsm4s8cm34b3sxfynh4xib1yva04sa1bjjkdfyv1l2ca1rz") (features (quote (("no-reexport") ("default"))))))

