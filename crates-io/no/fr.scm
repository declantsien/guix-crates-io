(define-module (crates-io no fr) #:use-module (crates-io))

(define-public crate-noframe-0.0.1 (crate (name "noframe") (vers "0.0.1") (deps (list (crate-dep (name "ggez") (req "^0.4.4") (default-features #t) (kind 0)))) (hash "13brkph0gmpgnygd8wwqj6gqii24k4ri5j92a8shif1am50fhhlb")))

(define-public crate-noframe-0.0.2 (crate (name "noframe") (vers "0.0.2") (deps (list (crate-dep (name "ggez") (req "^0.4.4") (default-features #t) (kind 0)))) (hash "1pvqchgg1f5x8l9yphd1j1znj2ywcnwffllnqlcmv6x4wl491vfj")))

(define-public crate-noframe-0.0.3 (crate (name "noframe") (vers "0.0.3") (deps (list (crate-dep (name "ggez") (req "^0.4.4") (default-features #t) (kind 0)))) (hash "1km73fdb1nmb5rk28dss8yyikbbgi5fxgw0myvfyx79fwdj1z6ih")))

(define-public crate-noframe-0.0.4 (crate (name "noframe") (vers "0.0.4") (deps (list (crate-dep (name "ggez") (req "^0.4.4") (default-features #t) (kind 0)))) (hash "07dfj5fq6xvs4in1nir02vvdnhj35lx47gbj0600fl60jmndwiwa")))

(define-public crate-noframe-0.0.5 (crate (name "noframe") (vers "0.0.5") (deps (list (crate-dep (name "ggez") (req "^0.4.4") (default-features #t) (kind 0)))) (hash "12rhgy36limv0hz8rqlbgwjllc2kmkn8a9dqzmyf2v7aaipzks7b")))

(define-public crate-noframe-0.0.6 (crate (name "noframe") (vers "0.0.6") (deps (list (crate-dep (name "ggez") (req "^0.4.4") (default-features #t) (kind 0)))) (hash "06m2c32ra1hswp6l9k98r5ww2mkx54m6jf7r25f59h6s6q7wvc8m")))

(define-public crate-noframe-0.0.7 (crate (name "noframe") (vers "0.0.7") (deps (list (crate-dep (name "ggez") (req "^0.4.4") (default-features #t) (kind 0)))) (hash "1y3kfhrpxsdkpqicwldildhwf9yfnmxh0h77vf4pzhpbfx8kg9h0")))

(define-public crate-noframe-0.0.8 (crate (name "noframe") (vers "0.0.8") (deps (list (crate-dep (name "ggez") (req "^0.4.4") (default-features #t) (kind 0)))) (hash "0wx0lxqga0kcjn98l0wgvh2bw3f3vhzap495xpy340383y0wnq43")))

