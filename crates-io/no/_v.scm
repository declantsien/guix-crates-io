(define-module (crates-io no _v) #:use-module (crates-io))

(define-public crate-no_vec-0.1 (crate (name "no_vec") (vers "0.1.0") (hash "1gqjw28f06p66kivysci2rlkr5jfiijvpbznnvinwk2w4iwnqj0n")))

(define-public crate-no_vec-0.3 (crate (name "no_vec") (vers "0.3.0") (hash "1h52l6mnmx4c6z8bw6j9p6373z40962cqsq7dys779vdxcd036hs")))

(define-public crate-no_vpn-0.1 (crate (name "no_vpn") (vers "0.1.0") (deps (list (crate-dep (name "base16") (req "^0.1.1") (default-features #t) (kind 0)) (crate-dep (name "chrono") (req "^0.4.6") (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.9.9") (default-features #t) (kind 0)) (crate-dep (name "rust-crypto") (req "^0.2.36") (default-features #t) (kind 0)))) (hash "1lr2i44s1x0drviwflb131vk1qbsi66fj8pcsm1rl7704mr482ig")))

(define-public crate-no_vpn-0.1 (crate (name "no_vpn") (vers "0.1.1") (deps (list (crate-dep (name "base16") (req "^0.1.1") (default-features #t) (kind 0)) (crate-dep (name "chrono") (req "^0.4.6") (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.9.9") (default-features #t) (kind 0)) (crate-dep (name "rust-crypto") (req "^0.2.36") (default-features #t) (kind 0)))) (hash "09kcaimjd99yf1kfhzbhmwfz4rjwa8sdxn607rznh5i6kbnmmjz4")))

