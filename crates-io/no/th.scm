(define-module (crates-io no th) #:use-module (crates-io))

(define-public crate-nothing-0.1 (crate (name "nothing") (vers "0.1.0") (hash "1q7a9f2z80d1rmhjwmjsyvs9gi8ggky3whxwbgk4sdlpvnhy7d5y")))

(define-public crate-nothing-0.1 (crate (name "nothing") (vers "0.1.1") (hash "0j2y1sgafschn26ghnwlf2bpx6if5lrd26g1xbyg5kc1yi4qn3q9")))

(define-public crate-nothing-0.1 (crate (name "nothing") (vers "0.1.2") (hash "162y4rgpl5q5apq54xm8bdb64i7ppd8xjwkis8y4bs4qf4vypaaa")))

(define-public crate-nothing-0.1 (crate (name "nothing") (vers "0.1.3") (hash "0krx631snagigviawb2mv482xfkcbriiw6rijvgzls4i6pql6c94")))

(define-public crate-nothing-0.1 (crate (name "nothing") (vers "0.1.4") (hash "1ix0c7p2xs2ywkqmgkyhnkq97z5mlqmhs3fvqy60jffamxfmhx6b")))

(define-public crate-nothing-0.1 (crate (name "nothing") (vers "0.1.5") (hash "0ma78syicp9224vavhmk7xrgkx0mcaqghls3ykvw555m3sngppkz")))

(define-public crate-nothing-0.1 (crate (name "nothing") (vers "0.1.6") (hash "198iprmq5kslllizhygaddb0cgi7cbrh91c9yl70m6xbflxl361x")))

(define-public crate-nothing-0.1 (crate (name "nothing") (vers "0.1.7") (hash "15c2vlsxblm9fsplp3zfy58jv0lgcwgvrxbdna704i7i01w3219q")))

(define-public crate-nothing-0.1 (crate (name "nothing") (vers "0.1.8") (hash "1xg314nw3d6qk8llaj4bd3fdidb7bk36wfv3js01db98xfghw7nr")))

(define-public crate-nothing-0.1 (crate (name "nothing") (vers "0.1.9") (hash "0jzrgi37ddrrmqcr7nrrwz6s9pbvggpqkhq1qnfa8jbs4k97ll4v")))

(define-public crate-nothing-0.1 (crate (name "nothing") (vers "0.1.10") (deps (list (crate-dep (name "is-macro") (req "^0.2") (default-features #t) (kind 0)))) (hash "1i67xnsbggabd85fdcgcdxyndcnkzzdy3nigxjsw36crvj240rw8")))

(define-public crate-nothing-0.1 (crate (name "nothing") (vers "0.1.11") (deps (list (crate-dep (name "is-macro") (req "^0.2") (default-features #t) (kind 0)))) (hash "0nh5wj9n4xhjf6sijxmgvvd249qpii7rg68hp888ylk3g4rvpz1b")))

(define-public crate-nothing-0.1 (crate (name "nothing") (vers "0.1.12") (deps (list (crate-dep (name "is-macro") (req "^0.2") (default-features #t) (kind 0)))) (hash "021i459i6qvb8n64h953diqarhz0zi5jdk8ll4r3jzf0i2kwcqv1")))

