(define-module (crates-io no nm) #:use-module (crates-io))

(define-public crate-nonmax-0.2 (crate (name "nonmax") (vers "0.2.0") (hash "1pm7267yscaq330gyqr6ihybairvkays47rzg5dsgy07mbap622z")))

(define-public crate-nonmax-0.3 (crate (name "nonmax") (vers "0.3.0") (hash "0riy83grrbws9w48yh5q58kya8la2qfpxq76ibnwbymnbcnm1iv6")))

(define-public crate-nonmax-0.4 (crate (name "nonmax") (vers "0.4.0") (hash "1fy90q6vk7s0jzc5h1kgm15v34a4g8vz0jx7ld3dqqgn8n4zd05x")))

(define-public crate-nonmax-0.5 (crate (name "nonmax") (vers "0.5.0") (hash "1knhqjszba18a0pn3x50ld12v8fk7z40jy7x8q08sfcyqch6syi8") (features (quote (("std") ("default" "std"))))))

(define-public crate-nonmax-0.5 (crate (name "nonmax") (vers "0.5.1") (deps (list (crate-dep (name "bincode") (req "^1.3") (default-features #t) (kind 2)) (crate-dep (name "serde") (req "^1.0") (optional #t) (default-features #t) (kind 0)))) (hash "1x0pls3s01jnqi4yd753i9p3wwx2yb3l42cgmncjswy2jia78mb4") (features (quote (("std") ("default" "std"))))))

(define-public crate-nonmax-0.5 (crate (name "nonmax") (vers "0.5.2") (deps (list (crate-dep (name "bincode") (req "^1.3") (default-features #t) (kind 2)) (crate-dep (name "serde") (req "^1.0") (optional #t) (default-features #t) (kind 0)))) (hash "0cisin0ms9rnkilwdi3qsmdq2mj0v0y6zdpl2j7a24xxk0bgghyg") (features (quote (("std") ("default" "std")))) (yanked #t)))

(define-public crate-nonmax-0.5 (crate (name "nonmax") (vers "0.5.3") (deps (list (crate-dep (name "bincode") (req "^1.3") (default-features #t) (kind 2)) (crate-dep (name "serde") (req "^1.0") (optional #t) (default-features #t) (kind 0)))) (hash "0s9nxcbj6b5vjbz5wmm3cnxl59f3niksqq6n1j7m4dg1jda6yxcr") (features (quote (("std") ("default" "std"))))))

(define-public crate-nonmax-0.5 (crate (name "nonmax") (vers "0.5.4") (deps (list (crate-dep (name "bincode") (req "^1.3") (default-features #t) (kind 2)) (crate-dep (name "serde") (req "^1.0") (optional #t) (default-features #t) (kind 0)))) (hash "08p4lxazaw4ybgc0wzlli0mxrdph5k47agfi99ldizpzlhz5jjkf") (features (quote (("std") ("default" "std"))))))

(define-public crate-nonmax-0.5 (crate (name "nonmax") (vers "0.5.5") (deps (list (crate-dep (name "bincode") (req "^1.3") (default-features #t) (kind 2)) (crate-dep (name "serde") (req "^1.0") (optional #t) (kind 0)))) (hash "0lfvyfz4falgmc9g1cbfi2wkys9wka2nfmdyga87zikf636ml2k1") (features (quote (("std") ("default" "std"))))))

(define-public crate-nonminmax-0.1 (crate (name "nonminmax") (vers "0.1.0") (hash "00ja39q5ph5nbab4ih16qh5vdk451f0b9f19s8b8v7gbwjj3fxlb")))

(define-public crate-nonminmax-0.1 (crate (name "nonminmax") (vers "0.1.1") (hash "1hb4i8rjq9kfhr4q68rx9fmg87sjkr7g5kamrpwwdwl516iin6nl")))

