(define-module (crates-io no np) #:use-module (crates-io))

(define-public crate-nonparallel-0.1 (crate (name "nonparallel") (vers "0.1.0") (deps (list (crate-dep (name "lazy_static") (req "^1") (default-features #t) (kind 2)) (crate-dep (name "quote") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "0xmpk06gv9afdcc5700qrirh593xxdpymj8v488p6q6hjjfkc5hj")))

(define-public crate-nonparallel-0.1 (crate (name "nonparallel") (vers "0.1.1") (deps (list (crate-dep (name "lazy_static") (req "^1") (default-features #t) (kind 2)) (crate-dep (name "quote") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "0b9n0m582h5a7br6i0p80bvw7c3vcqvypjvc2mrb9sk9jy6hn7zr")))

(define-public crate-nonparallel-async-0.1 (crate (name "nonparallel-async") (vers "0.1.0") (deps (list (crate-dep (name "quote") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1") (features (quote ("full"))) (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1") (features (quote ("macros" "sync" "parking_lot" "rt-multi-thread"))) (default-features #t) (kind 2)))) (hash "0z1i3cf22r1yyjwkq5wbxn7v181aba4b3lb6ngzm67daigbgxy0x")))

(define-public crate-nonparallelex-0.2 (crate (name "nonparallelex") (vers "0.2.0") (deps (list (crate-dep (name "quote") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1") (features (quote ("full"))) (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1") (features (quote ("macros" "rt-multi-thread" "sync" "parking_lot"))) (default-features #t) (kind 2)))) (hash "0hkqg8g2m83im93jhqf65725g6ha58ccn9hrxg3x019nxxxii4vg")))

