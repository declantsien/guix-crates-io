(define-module (crates-io no _p) #:use-module (crates-io))

(define-public crate-no_proto-0.0.0 (crate (name "no_proto") (vers "0.0.0-beta.1") (deps (list (crate-dep (name "json") (req "^0.12.2") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.7.3") (default-features #t) (kind 0)))) (hash "1aw46170rznhgxalqgdinwd14h15lqxyc7f7wd1yjizllfrvcpkq")))

(define-public crate-no_proto-0.0.0 (crate (name "no_proto") (vers "0.0.0-beta.2") (deps (list (crate-dep (name "json") (req "^0.12.2") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.7.3") (default-features #t) (kind 0)))) (hash "1anqsfb5c0rc6cilkpzcrppp3clynxr8kdpjx40z2699pif8c8g0")))

(define-public crate-no_proto-0.0.0 (crate (name "no_proto") (vers "0.0.0-beta.3") (hash "1n6h13iq5mzic78cvrlfj1ldzvw2dblpn9hr2f59vijgaqcznigs")))

(define-public crate-no_proto-0.0.0 (crate (name "no_proto") (vers "0.0.0-beta.4") (hash "0pg8mm6073m7nlkl8r8zbv2knav83hhdjdbviwkn4zswn4bw92n6")))

(define-public crate-no_proto-0.0.0 (crate (name "no_proto") (vers "0.0.0-beta.5") (hash "1zql2vr6kv4x47xb9qpf6rcx2siknc3gzx48zh4hli7q2yj3llgd")))

(define-public crate-no_proto-0.0.1 (crate (name "no_proto") (vers "0.0.1") (hash "0dlr6knhnf1mxqjdywrn69ljsvmjn8vnnn92l984pkn5f4ibnkaf")))

(define-public crate-no_proto-0.0.2 (crate (name "no_proto") (vers "0.0.2") (hash "0s1xlpqgsmxhny74jcncidgsd9975dqzgxzikp36kc6lbrb7k2h5")))

(define-public crate-no_proto-0.0.3 (crate (name "no_proto") (vers "0.0.3") (hash "17ngpr11c8yr5snn80z2k7h2yzfdycji146mbl251y7ikgl69ipj")))

(define-public crate-no_proto-0.0.4 (crate (name "no_proto") (vers "0.0.4") (hash "02j3drqk8642plyrxvkrsmmbzna8h67hs8d6anlansarj17v97m2")))

(define-public crate-no_proto-0.0.5 (crate (name "no_proto") (vers "0.0.5") (hash "108wc7pxqxa1fa4m6d586php9knlnqhcq7xschgh6ca7nkvnyp54")))

(define-public crate-no_proto-0.1 (crate (name "no_proto") (vers "0.1.0") (hash "1sq0x4qd674syqigzwi2dg4b6x54x3brpbgs809hmcmfhqkaj1p9")))

(define-public crate-no_proto-0.1 (crate (name "no_proto") (vers "0.1.1") (hash "1vvyrr5pcx1gwp25xw4qfddjgsj8643p0gr1zzxhdc62c5xsz3ip")))

(define-public crate-no_proto-0.1 (crate (name "no_proto") (vers "0.1.2") (hash "024x227bq0q92584la2qdjgvraw75hp733gb0w7y5zlqzsrb3phn")))

(define-public crate-no_proto-0.2 (crate (name "no_proto") (vers "0.2.0") (hash "03cxl1jqggmjfgzx5fsz14cvgszfhhygsrc6hanpcgkx1imzhjjg")))

(define-public crate-no_proto-0.2 (crate (name "no_proto") (vers "0.2.1") (hash "1fgsarw8hp671wldyrjjwdi0qqshcgbljk4wz2v81dn1g6xd3cpm")))

(define-public crate-no_proto-0.2 (crate (name "no_proto") (vers "0.2.2") (hash "09wa1rc4fnmlvz7dds717aqxps144n1wfchnqdikslnr9wxjrrkq")))

(define-public crate-no_proto-0.3 (crate (name "no_proto") (vers "0.3.0") (hash "11i57a1wcy09721m4bla1gni1fi9d315l97cg6x3i3hpixz7fwx8")))

(define-public crate-no_proto-0.4 (crate (name "no_proto") (vers "0.4.0") (hash "1vhnmfb12vy97pqbcybc21lrb9kci58kqr7fbgzw8ipsjds6pn26")))

(define-public crate-no_proto-0.4 (crate (name "no_proto") (vers "0.4.1") (hash "1rdyhkk1p756yycm8j025pqzd7hislrwijpmf9n60h22pag3vvhf")))

(define-public crate-no_proto-0.4 (crate (name "no_proto") (vers "0.4.2") (hash "1haf6251jbdks9mgbz86kg36npgy9nlnpvjy3ypahp57zph288z0")))

(define-public crate-no_proto-0.5 (crate (name "no_proto") (vers "0.5.0") (hash "1r4i286a6w06a1d6vjdlw9cs1jb6w1l0z7j8yd9r15rwnyzgmwmp")))

(define-public crate-no_proto-0.5 (crate (name "no_proto") (vers "0.5.1") (hash "0lavwa4zlp2zx3da9ai7plwn4llg1x62kcvrcm9qsarpdbc42kfm")))

(define-public crate-no_proto-0.6 (crate (name "no_proto") (vers "0.6.0") (hash "19hsfw7bpspn0lxlhkyygbwskmxksfl0s7aawg96wlrxrpwqp62h")))

(define-public crate-no_proto-0.6 (crate (name "no_proto") (vers "0.6.1") (hash "1mpf9dxbbmqyxj8r69n0jvwk6cwkwc4kmds112ma51i491q098vh")))

(define-public crate-no_proto-0.6 (crate (name "no_proto") (vers "0.6.2") (hash "12vr8fc3hjphp6v53373fzr4lf1gxgqph4xrwnr4iz05x4sky70b") (features (quote (("np_rpc") ("default" "np_rpc"))))))

(define-public crate-no_proto-0.6 (crate (name "no_proto") (vers "0.6.3") (hash "003ryik189c2nvbva4i92mhgmnh2az6fc0r325h6hgm2w1hhwvib") (features (quote (("np_rpc") ("default" "np_rpc"))))))

(define-public crate-no_proto-0.7 (crate (name "no_proto") (vers "0.7.0") (hash "0s2bq6n99mzx2gmbz1x6ix9h93zj01mp3nd0lciqlhrwwzfh4m8n") (features (quote (("np_rpc") ("default" "np_rpc"))))))

(define-public crate-no_proto-0.7 (crate (name "no_proto") (vers "0.7.1") (hash "1qlcy22l5k7ibl3lq43i1yb9i1rd4p44knqg4jx6r596l384q952") (features (quote (("np_rpc") ("default" "np_rpc"))))))

(define-public crate-no_proto-0.7 (crate (name "no_proto") (vers "0.7.2") (hash "0h11kh3qw7wz2hmkdl3kfylkcs9sgy74zncijm8w9xv7gi3pfd3w") (features (quote (("np_rpc") ("default" "np_rpc"))))))

(define-public crate-no_proto-0.7 (crate (name "no_proto") (vers "0.7.3") (hash "1lklk3wzid187kipvvgsfdaabxfng5g8zmg8c7r1gmksf2bzqqvd") (features (quote (("np_rpc") ("default" "np_rpc"))))))

(define-public crate-no_proto-0.7 (crate (name "no_proto") (vers "0.7.4") (hash "0rkcz37bkgjkf482qy38w2f7dba8n8qy9yh99nws3v3cn87pcqbn") (features (quote (("np_rpc") ("default" "np_rpc"))))))

(define-public crate-no_proto-0.8 (crate (name "no_proto") (vers "0.8.0") (hash "1iyrpxzbf7wdzy1v6xa37fxvdj2rv6ca1xnxp4a7cfjmqj2r2gsh") (features (quote (("np_rpc") ("default" "np_rpc"))))))

(define-public crate-no_proto-0.9 (crate (name "no_proto") (vers "0.9.0") (hash "0cl8yx2r0wpn47n47hk79mz52f0f5ia7c1ja7k5r3iwa13ls8m69") (features (quote (("np_rpc") ("default" "np_rpc"))))))

(define-public crate-no_proto-0.9 (crate (name "no_proto") (vers "0.9.1") (hash "1h4zz7dicr5hxfxqcvlcd4vz35hr1zg2igxhw69a633wmc76izpb") (features (quote (("np_rpc") ("default" "np_rpc"))))))

(define-public crate-no_proto-0.9 (crate (name "no_proto") (vers "0.9.2") (hash "1w8da8868gdmzr1cl6frxx444dxbzsyyp6cvrayjkvilvawjghwy") (features (quote (("np_rpc") ("default" "np_rpc"))))))

(define-public crate-no_proto-0.9 (crate (name "no_proto") (vers "0.9.3") (hash "0bvrqllqnvf39qvdk7wvah7cnrk55j1s0r24740idfrxax3wd4m6") (features (quote (("np_rpc") ("default" "np_rpc"))))))

(define-public crate-no_proto-0.9 (crate (name "no_proto") (vers "0.9.4") (hash "19s72yp1pwys9739d38apg83iivghcpipr2mlb9zg1wsyiv62cdz") (features (quote (("np_rpc") ("default" "np_rpc"))))))

(define-public crate-no_proto-0.9 (crate (name "no_proto") (vers "0.9.5") (hash "1f43838rv51zf52bq2gw140g2y5lz73xqyjb97yij317siwmq43i") (features (quote (("np_rpc") ("default" "np_rpc"))))))

(define-public crate-no_proto-0.9 (crate (name "no_proto") (vers "0.9.51") (hash "1wy1v53vawpqi7icqlb0n8g73g37ndgkhci3bh0h4ccqjwnsb67z") (features (quote (("np_rpc") ("default" "np_rpc"))))))

(define-public crate-no_proto-0.9 (crate (name "no_proto") (vers "0.9.6") (hash "1g8mw1a164266f38aji33d8npgrhm77m6p99yvcvln2ywmphqq3b") (features (quote (("np_rpc") ("default" "np_rpc"))))))

(define-public crate-no_proto-0.9 (crate (name "no_proto") (vers "0.9.60") (hash "06nifnhpnnfcdnj0kx68jis25w9lmp8fy2z5qqqa2zbwrdfcm4kd") (features (quote (("np_rpc") ("default" "np_rpc"))))))

