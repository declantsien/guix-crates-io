(define-module (crates-io no tm) #:use-module (crates-io))

(define-public crate-notmecab-0.1 (crate (name "notmecab") (vers "0.1.0") (deps (list (crate-dep (name "byteorder") (req "^1.2") (default-features #t) (kind 0)) (crate-dep (name "pathfinding") (req "^1.1") (default-features #t) (kind 0)))) (hash "11n8cvs39njfmngqhsnxx9lsj4xdwlpy08g7yzv5pskxkl27apc8")))

(define-public crate-notmecab-0.1 (crate (name "notmecab") (vers "0.1.1") (deps (list (crate-dep (name "byteorder") (req "^1.2") (default-features #t) (kind 0)) (crate-dep (name "pathfinding") (req "^1.1") (default-features #t) (kind 0)))) (hash "1kd5kq4h6ixad9z9gp6jjz9hbsnghrlfgd9183bbppg8cnf9yxfv")))

(define-public crate-notmecab-0.2 (crate (name "notmecab") (vers "0.2.0") (deps (list (crate-dep (name "byteorder") (req "^1.2") (default-features #t) (kind 0)) (crate-dep (name "pathfinding") (req "^1.1") (default-features #t) (kind 0)))) (hash "091sy9vw8626jqif8vd90x95j4a78jf297pbxpqsxc8d57x2wfgm")))

(define-public crate-notmecab-0.3 (crate (name "notmecab") (vers "0.3.0") (deps (list (crate-dep (name "byteorder") (req "^1.2") (default-features #t) (kind 0)) (crate-dep (name "hashbrown") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "pathfinding") (req "^1.1") (default-features #t) (kind 0)))) (hash "0na09x9lzfkz4bv3bkf4b01jksdrc9qlfv3ab6g8khyb4gjfwlxc")))

(define-public crate-notmecab-0.3 (crate (name "notmecab") (vers "0.3.1") (deps (list (crate-dep (name "byteorder") (req "^1.2") (default-features #t) (kind 0)) (crate-dep (name "hashbrown") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "pathfinding") (req "^1.1") (default-features #t) (kind 0)))) (hash "1j6z92jkmqpbaysybyq0dshhrpsjrn1z43ggvvz4l75388dy08kf")))

(define-public crate-notmecab-0.3 (crate (name "notmecab") (vers "0.3.2") (deps (list (crate-dep (name "byteorder") (req "^1.2") (default-features #t) (kind 0)) (crate-dep (name "hashbrown") (req "^0.5") (default-features #t) (kind 0)) (crate-dep (name "pathfinding") (req "^1.1") (default-features #t) (kind 0)))) (hash "1cgi9nkaksbxj3wl0fhfr6a3iphjqcwn7fi1bd8b96qs3q3wapnj")))

(define-public crate-notmecab-0.4 (crate (name "notmecab") (vers "0.4.0") (deps (list (crate-dep (name "byteorder") (req "^1.2") (default-features #t) (kind 0)) (crate-dep (name "hashbrown") (req "^0.5") (default-features #t) (kind 0)) (crate-dep (name "memmap") (req "^0.7") (default-features #t) (kind 0)) (crate-dep (name "pathfinding") (req "^1.1") (default-features #t) (kind 0)))) (hash "1k6wg5pqmlz7lr0440hddnphmzwzrd9m77fa4irpvqz4wbhih63b")))

(define-public crate-notmecab-0.5 (crate (name "notmecab") (vers "0.5.0") (deps (list (crate-dep (name "hashbrown") (req "^0.6") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "memmap") (req "^0.7") (default-features #t) (kind 0)))) (hash "14k82cmdlakw4a9s03wbfvbqiihhhiz6vda3b5n73zdcbg4865rf") (features (quote (("default" "hashbrown"))))))

(define-public crate-notmecab-0.5 (crate (name "notmecab") (vers "0.5.1") (deps (list (crate-dep (name "hashbrown") (req "^0.6") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "memmap") (req "^0.7") (default-features #t) (kind 0)))) (hash "0fmn6v972bw3k86y1mz35b6pijbmxam57ixfd2daq1wfg0k9jxsf") (features (quote (("default" "hashbrown"))))))

(define-public crate-notmongo-0.1 (crate (name "notmongo") (vers "0.1.5") (deps (list (crate-dep (name "bson") (req "^2.4.0") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2.132") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.85") (default-features #t) (kind 0)) (crate-dep (name "zip") (req "^0.6.6") (default-features #t) (kind 0)))) (hash "0f74a01783px5kp8g5r3kzi065mz3vin15qyh5vcn963f6nj9fi6")))

(define-public crate-notmongo-0.1 (crate (name "notmongo") (vers "0.1.6") (deps (list (crate-dep (name "bson") (req "^2.4.0") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2.132") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.85") (default-features #t) (kind 0)) (crate-dep (name "zip") (req "^0.6.6") (default-features #t) (kind 0)))) (hash "1am4cwlix3qv4vrjrbdy5zcf7fhjhr9bxbzc13j6i2xhmwrwf41h")))

(define-public crate-notmongo-0.1 (crate (name "notmongo") (vers "0.1.7") (deps (list (crate-dep (name "bson") (req "^2.4.0") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2.132") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.85") (default-features #t) (kind 0)) (crate-dep (name "zip") (req "^0.6.6") (default-features #t) (kind 0)))) (hash "0ynf2mgdkajf9y3160jrfc0g6lwyjgdbnlp59mj6iwvh5an7lr5d")))

(define-public crate-notmuch-0.0.1 (crate (name "notmuch") (vers "0.0.1") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "0q3gn63mpswd0qbzi7q6dsk2cwgyj2dvajr0ip1k796ssifiqnfz")))

(define-public crate-notmuch-0.0.2 (crate (name "notmuch") (vers "0.0.2") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "1mjv856ncrb0ccr8l016jxwydz3mxc7sdn5j91jif8nwdbg80mzj")))

(define-public crate-notmuch-0.0.3 (crate (name "notmuch") (vers "0.0.3") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "104dpa8ywz46lspfzp29gg0ngsvqsx9hw5q18h2yshjk4hdg9xc8")))

(define-public crate-notmuch-0.0.4 (crate (name "notmuch") (vers "0.0.4") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "0p7vcygp213bxy9dl5525y1c389sn0snyrzdfbya54jpk3zjqajp")))

(define-public crate-notmuch-0.0.5 (crate (name "notmuch") (vers "0.0.5") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "1grd79ibnqkvqwql0l41lrjm2zq3hyrbkzmsw9jflsirzd2vs6hd")))

(define-public crate-notmuch-0.0.6 (crate (name "notmuch") (vers "0.0.6") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "0ijaycbw68fj609d6fjy3ms3hx30g1qf14cidmk23pcvrxv8xbz8")))

(define-public crate-notmuch-0.0.7 (crate (name "notmuch") (vers "0.0.7") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "034hc5jllc469xn29lazl46441y5jambglwjiqgfiad4fsc8qdf0")))

(define-public crate-notmuch-0.0.8 (crate (name "notmuch") (vers "0.0.8") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "1pw2p8lh18lq2qa0mxnliwcs7lymzc994qxh8980vag5ifpfhis4")))

(define-public crate-notmuch-0.0.9 (crate (name "notmuch") (vers "0.0.9") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "1w4jr6k98z7ymzkp7ivz7dh33wha1k5vavzizwmqsz1qja34j64q")))

(define-public crate-notmuch-0.0.10 (crate (name "notmuch") (vers "0.0.10") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "0ciqf7h0s27k86map1w22xs8wsq02vlhq0g29140binck6iz6qm4")))

(define-public crate-notmuch-0.0.11 (crate (name "notmuch") (vers "0.0.11") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "1h4wsswhml15w88vrc20pqkhlapjbf0rkphwdizjhwbnwlfa4wsy")))

(define-public crate-notmuch-0.0.12 (crate (name "notmuch") (vers "0.0.12") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "1x25x8aqxv4wlkyyy7gy8d68bvm7zn2rlsl0ry52sms7c7b2rbv6")))

(define-public crate-notmuch-0.1 (crate (name "notmuch") (vers "0.1.0") (deps (list (crate-dep (name "clippy") (req "^0.0.193") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "06nz0v0w0swdzbv4g2v83rdyw9x4p073d874hq5mgl5llxx4xc3a")))

(define-public crate-notmuch-0.1 (crate (name "notmuch") (vers "0.1.1") (deps (list (crate-dep (name "clippy") (req "^0.0.193") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "0sy1zxfgd90gbam98pmnc1rxn7fy0g112vviwxk12qkiyahryx9i") (features (quote (("v0_26" "v0_21") ("v0_21") ("default" "v0_26"))))))

(define-public crate-notmuch-0.2 (crate (name "notmuch") (vers "0.2.0") (deps (list (crate-dep (name "clippy") (req "^0.0.193") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "1r0jl5q9822rq19fhsbfg5p09grva6bl8x40npibqq92gj4snc0q") (features (quote (("v0_26" "v0_21") ("v0_21") ("default" "v0_26"))))))

(define-public crate-notmuch-0.2 (crate (name "notmuch") (vers "0.2.1") (deps (list (crate-dep (name "clippy") (req "^0.0.193") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "1finzvwr6cxq2z23v6k47mk7yn161ggm0dzsiz3f79m93np7n3wb") (features (quote (("v0_26" "v0_21") ("v0_21") ("default" "v0_26"))))))

(define-public crate-notmuch-0.3 (crate (name "notmuch") (vers "0.3.0") (deps (list (crate-dep (name "clippy") (req "^0.0.193") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "dirs") (req "^1.0") (default-features #t) (kind 2)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "supercow") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "1a2vp3cl58fb54rj7h52jzc7avzilzjzxz50bkli900q5njnzpxb") (features (quote (("v0_26" "v0_21") ("v0_21") ("default" "v0_26"))))))

(define-public crate-notmuch-0.3 (crate (name "notmuch") (vers "0.3.1") (deps (list (crate-dep (name "clippy") (req "^0.0.193") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "dirs") (req "^1.0") (default-features #t) (kind 2)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "supercow") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "17m753idvpmjhrcwb54mvi872xnj6q3zwgqrimkzxg2j7xknga1x") (features (quote (("v0_26" "v0_21") ("v0_21") ("default" "v0_26"))))))

(define-public crate-notmuch-0.4 (crate (name "notmuch") (vers "0.4.0") (deps (list (crate-dep (name "clippy") (req "^0.0.193") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "dirs") (req "^1.0") (default-features #t) (kind 2)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "supercow") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "0vzz3ii4kfvsf9a5fhsgvg47m2nxzpvjgx18x7lnxb1fvx1vkw70") (features (quote (("v0_26" "v0_21") ("v0_21") ("default" "v0_26"))))))

(define-public crate-notmuch-0.4 (crate (name "notmuch") (vers "0.4.1") (deps (list (crate-dep (name "clippy") (req "^0.0.193") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "dirs") (req "^1.0") (default-features #t) (kind 2)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "supercow") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "0mjrqlazj4pp9gip3mrnpx1wqczl99fhxyq6i47l6xvkdkypy3wz") (features (quote (("v0_26" "v0_21") ("v0_21") ("default" "v0_26"))))))

(define-public crate-notmuch-0.4 (crate (name "notmuch") (vers "0.4.2") (deps (list (crate-dep (name "clippy") (req "^0.0.193") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "dirs") (req "^1.0") (default-features #t) (kind 2)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "supercow") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "0hiffw2k0fxdhghvrqm464vbq9hi6ggmf63f84j5gzd920gglcy0") (features (quote (("v0_26" "v0_21") ("v0_21") ("default" "v0_26"))))))

(define-public crate-notmuch-0.5 (crate (name "notmuch") (vers "0.5.0") (deps (list (crate-dep (name "clippy") (req "^0.0.193") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "dirs") (req "^1.0") (default-features #t) (kind 2)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "supercow") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "1s92w3j77ij8z5fw1vs3ypd119ksi3f38gs24r8m0s49nv0j65l3") (features (quote (("v0_26" "v0_21") ("v0_21") ("default" "v0_26"))))))

(define-public crate-notmuch-0.6 (crate (name "notmuch") (vers "0.6.0") (deps (list (crate-dep (name "dirs") (req "^1.0") (default-features #t) (kind 2)) (crate-dep (name "gethostname") (req "^0.2.0") (default-features #t) (kind 2)) (crate-dep (name "lettre") (req "^0.9.2") (default-features #t) (kind 2)) (crate-dep (name "lettre_email") (req "^0.9.2") (default-features #t) (kind 2)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "maildir") (req "^0.3.2") (default-features #t) (kind 2)) (crate-dep (name "supercow") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "tempfile") (req "^3") (default-features #t) (kind 2)))) (hash "19q93iyvx4liksm09mhq9ibm8zj7i3dizc1s40f916z0kbpn9k5w") (features (quote (("v0_26" "v0_21") ("v0_21") ("default" "v0_26"))))))

(define-public crate-notmuch-0.7 (crate (name "notmuch") (vers "0.7.0") (deps (list (crate-dep (name "dirs") (req "^1.0") (default-features #t) (kind 2)) (crate-dep (name "from_variants") (req "^0.6.0") (default-features #t) (kind 0)) (crate-dep (name "gethostname") (req "^0.2.0") (default-features #t) (kind 2)) (crate-dep (name "lettre") (req "^0.9.2") (default-features #t) (kind 2)) (crate-dep (name "lettre_email") (req "^0.9.2") (default-features #t) (kind 2)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "maildir") (req "^0.3.2") (default-features #t) (kind 2)) (crate-dep (name "tempfile") (req "^3") (default-features #t) (kind 2)))) (hash "1ahd11y36jisfjssr2056857dywzyd79rsyx815x42nkzlm9np8d") (features (quote (("v0_26" "v0_21") ("v0_21") ("default" "v0_26"))))))

(define-public crate-notmuch-0.7 (crate (name "notmuch") (vers "0.7.1") (deps (list (crate-dep (name "dirs") (req "^1.0") (default-features #t) (kind 2)) (crate-dep (name "from_variants") (req "^0.6.0") (default-features #t) (kind 0)) (crate-dep (name "gethostname") (req "^0.2.0") (default-features #t) (kind 2)) (crate-dep (name "lettre") (req "^0.9.2") (default-features #t) (kind 2)) (crate-dep (name "lettre_email") (req "^0.9.2") (default-features #t) (kind 2)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "maildir") (req "^0.3.2") (default-features #t) (kind 2)) (crate-dep (name "tempfile") (req "^3") (default-features #t) (kind 2)))) (hash "1y8c76by7mp9rmaa99whc6vhkfdgxxb4yja27ng55f7mkbyl22fa") (features (quote (("v0_26" "v0_21") ("v0_21") ("default" "v0_26"))))))

(define-public crate-notmuch-0.7 (crate (name "notmuch") (vers "0.7.2") (deps (list (crate-dep (name "dirs") (req "^1.0") (default-features #t) (kind 2)) (crate-dep (name "from_variants") (req "^0.6.0") (default-features #t) (kind 0)) (crate-dep (name "gethostname") (req "^0.2.0") (default-features #t) (kind 2)) (crate-dep (name "lettre") (req "^0.9.2") (default-features #t) (kind 2)) (crate-dep (name "lettre_email") (req "^0.9.2") (default-features #t) (kind 2)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "maildir") (req "^0.3.2") (default-features #t) (kind 2)) (crate-dep (name "tempfile") (req "^3") (default-features #t) (kind 2)))) (hash "11dzsbbs3nd8nf4wbkk4cgsp1xx7dpgmvmb1s954ddxmxmyb4yld") (features (quote (("v0_26" "v0_21") ("v0_21") ("default" "v0_26"))))))

(define-public crate-notmuch-0.8 (crate (name "notmuch") (vers "0.8.0") (deps (list (crate-dep (name "dirs") (req "^1.0") (default-features #t) (kind 2)) (crate-dep (name "from_variants") (req "^0.6.0") (default-features #t) (kind 0)) (crate-dep (name "gethostname") (req "^0.2.0") (default-features #t) (kind 2)) (crate-dep (name "lettre") (req "^0.9.2") (default-features #t) (kind 2)) (crate-dep (name "lettre_email") (req "^0.9.2") (default-features #t) (kind 2)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "maildir") (req "^0.3.2") (default-features #t) (kind 2)) (crate-dep (name "tempfile") (req "^3") (default-features #t) (kind 2)))) (hash "0i6xc7lv10m2sq6vlpjr5wxmlxihvd0v4f5if75r2kwz8ji12pg2") (features (quote (("v0_32" "v0_26") ("v0_26" "v0_21") ("v0_21") ("default" "v0_32"))))))

(define-public crate-notmuch-more-0.0.0 (crate (name "notmuch-more") (vers "0.0.0") (deps (list (crate-dep (name "ammonia") (req "^3.1.2") (default-features #t) (kind 0)) (crate-dep (name "anyhow") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "email") (req "^0.0.21") (default-features #t) (kind 0)) (crate-dep (name "itertools") (req "^0.10.1") (default-features #t) (kind 0)) (crate-dep (name "lettre") (req "=0.10.0-alpha.1") (features (quote ("builder" "rustls-tls" "smtp-transport"))) (kind 0)) (crate-dep (name "mailparse") (req "^0.13.5") (default-features #t) (kind 0)) (crate-dep (name "notmuch") (req "^0.6.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.5.4") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "tempfile") (req "^3.2.0") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.26") (default-features #t) (kind 0)))) (hash "19rscfn1c7qjymyq3j0wbnalcb775qlq77wzx1jmzd9z6c41gcay")))

(define-public crate-notmuch-sys-4 (crate (name "notmuch-sys") (vers "4.3.0") (deps (list (crate-dep (name "libc") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "tempdir") (req "^0.3") (default-features #t) (kind 2)))) (hash "1l9a1vyy3kcd2szh32581yn0w38gfif992qzk625j6l6qyrh635i")))

(define-public crate-notmuch-sys-4 (crate (name "notmuch-sys") (vers "4.3.1") (deps (list (crate-dep (name "libc") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "tempdir") (req "^0.3") (default-features #t) (kind 2)))) (hash "1h15ini0gad6zz2pfs0n669y12vxvz74pb4ycpj23mh1rqjr9540")))

(define-public crate-notmuch-sys-4 (crate (name "notmuch-sys") (vers "4.3.2") (deps (list (crate-dep (name "libc") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "tempdir") (req "^0.3") (default-features #t) (kind 2)))) (hash "08kwvhw9f8rksjjfx3v090vy9jlxmydzh9p4k3773gqh6xpvv325")))

(define-public crate-notmuch-sys-4 (crate (name "notmuch-sys") (vers "4.3.3") (deps (list (crate-dep (name "libc") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "tempdir") (req "^0.3") (default-features #t) (kind 2)))) (hash "1z99ja9513cf1aszsbjq902h7i8j0n1rj24x50pshgzzl3nf1nf5")))

(define-public crate-notmuch-sys-4 (crate (name "notmuch-sys") (vers "4.3.4") (deps (list (crate-dep (name "libc") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "tempdir") (req "^0.3") (default-features #t) (kind 2)))) (hash "161cdghr02k5iky8lkm1szwz5p0gg2yaad9add0aziaswyn4n6a4")))

(define-public crate-notmuch-sys-4 (crate (name "notmuch-sys") (vers "4.3.5") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "tempdir") (req "^0.3") (default-features #t) (kind 2)))) (hash "0qmkd11xz7dpik10kzphw4mvqbv4ky7ndkj7y48k3dblgi5kb41s")))

(define-public crate-notmuch-sys-4 (crate (name "notmuch-sys") (vers "4.4.0") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "tempdir") (req "^0.3") (default-features #t) (kind 2)))) (hash "14s8dsc030n8fjm56py3rvk583j4dadbq5dlyh63bd0af6sy0jzr")))

(define-public crate-notmuch-sys-4 (crate (name "notmuch-sys") (vers "4.4.1") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "tempdir") (req "^0.3") (default-features #t) (kind 2)))) (hash "1gdc0hhnxbf2mfzxndr644k70xv3jgkz1ywznq2gan4xllqpm7kr")))

(define-public crate-notmuch-sys-4 (crate (name "notmuch-sys") (vers "4.4.2") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "tempdir") (req "^0.3") (default-features #t) (kind 2)))) (hash "0qr43liaxky5gfpav6aksgwp8z40g5y6hxb3ykszynar7pz6fbzf")))

(define-public crate-notmykdb-0.1 (crate (name "notmykdb") (vers "0.1.0") (deps (list (crate-dep (name "mini-v8") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "1dfr781hl04dvwjzj2dlv1g2qkwsy8zms2kbck10zgpx88fzkfaf") (yanked #t)))

