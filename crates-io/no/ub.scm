(define-module (crates-io no ub) #:use-module (crates-io))

(define-public crate-noubeacon-0.1 (crate (name "noubeacon") (vers "0.1.0") (deps (list (crate-dep (name "anyhow") (req "^1.0.26") (default-features #t) (kind 0)) (crate-dep (name "cpal") (req "^0.11.0") (default-features #t) (kind 0)))) (hash "06fank1wmpbvcc0vizq5ax6lvm25fhacdxmy2swnhnd1i3cn3cl2")))

(define-public crate-noubeacon-0.1 (crate (name "noubeacon") (vers "0.1.1") (deps (list (crate-dep (name "anyhow") (req "^1.0.26") (default-features #t) (kind 0)) (crate-dep (name "cpal") (req "^0.11.0") (default-features #t) (kind 0)))) (hash "1acfnkq8m60naca16yilq3m9xarq7da8qs1fx5hqq45pm0rmrqj5")))

