(define-module (crates-io no m8) #:use-module (crates-io))

(define-public crate-nom8-0.1 (crate (name "nom8") (vers "0.1.0") (deps (list (crate-dep (name "doc-comment") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "memchr") (req "^2.3") (kind 0)) (crate-dep (name "minimal-lexical") (req "^0.2.0") (kind 0)) (crate-dep (name "proptest") (req "^1.0.0") (default-features #t) (kind 2)))) (hash "0b16yz3z25ybzizlsj8n0ixkvzpf0y5l73a79v9jcdbw57q0inbm") (features (quote (("unstable-doc" "alloc" "std") ("std" "alloc" "memchr/std" "minimal-lexical/std") ("default" "std") ("alloc")))) (rust-version "1.51.0")))

(define-public crate-nom8-0.2 (crate (name "nom8") (vers "0.2.0") (deps (list (crate-dep (name "doc-comment") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "memchr") (req "^2.3") (kind 0)) (crate-dep (name "proptest") (req "^1.0.0") (default-features #t) (kind 2)))) (hash "1y6jzabxyrl05vxnh63r66ac2fh0symg5fnynxm4ii3zkif580df") (features (quote (("unstable-doc" "alloc" "std") ("std" "alloc" "memchr/std") ("default" "std") ("alloc")))) (rust-version "1.51.0")))

