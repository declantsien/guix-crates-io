(define-module (crates-io no bc) #:use-module (crates-io))

(define-public crate-nobcd-0.1 (crate (name "nobcd") (vers "0.1.0") (deps (list (crate-dep (name "defmt") (req "^0.3") (optional #t) (default-features #t) (kind 0)))) (hash "097sb0h34d1gys3vla0j6a4170i5j994yl3rcxxnyc9y014xik40")))

(define-public crate-nobcd-0.1 (crate (name "nobcd") (vers "0.1.1") (deps (list (crate-dep (name "defmt") (req "^0.3") (optional #t) (default-features #t) (kind 0)))) (hash "10b0rlj533ryiiqgyny4bi03gxxnf8r1b4g8ncvy4afw97fvd9xj")))

(define-public crate-nobcd-0.2 (crate (name "nobcd") (vers "0.2.0") (deps (list (crate-dep (name "defmt") (req "^0.3") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2") (kind 0)))) (hash "1wsbz4blhyxx8gnbrzrxwaqadavkmkisvh991ddjdl45smd63l9f")))

