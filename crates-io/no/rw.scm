(define-module (crates-io no rw) #:use-module (crates-io))

(define-public crate-norwegian_id_validator-0.1 (crate (name "norwegian_id_validator") (vers "0.1.0") (hash "0c1b049jipxc3s7x53z1nl2vxa49bvzf43f4vzr4xyssp2x04j4i")))

(define-public crate-norwegian_id_validator-0.2 (crate (name "norwegian_id_validator") (vers "0.2.0") (hash "0czrid613fk5rihfmkmq7v7r1g8vp8qksq3784xwkqg1c270h159")))

