(define-module (crates-io no nn) #:use-module (crates-io))

(define-public crate-nonn-0.1 (crate (name "nonn") (vers "0.1.0") (deps (list (crate-dep (name "version_check") (req "^0.9.4") (default-features #t) (kind 1)))) (hash "1x4hmvpfhfi6x7yrl1i7n4j24ql8cbwr6rvx19sxilhq1zz553ys") (yanked #t) (rust-version "1.57.0")))

(define-public crate-nonn-0.1 (crate (name "nonn") (vers "0.1.1") (deps (list (crate-dep (name "version_check") (req "^0.9.4") (default-features #t) (kind 1)))) (hash "15129qvq6qvs9rczk328dp0rasai5gjgsdyqy9a334nc842m8m9g") (yanked #t) (rust-version "1.57.0")))

(define-public crate-nonn-0.1 (crate (name "nonn") (vers "0.1.2") (deps (list (crate-dep (name "version_check") (req "^0.9.4") (default-features #t) (kind 1)))) (hash "0bl97cdj00z6q2p72wsl1s3l16w00n4m6kfikf0km4zpffc95kfw") (yanked #t) (rust-version "1.57.0")))

(define-public crate-nonn-0.1 (crate (name "nonn") (vers "0.1.3") (deps (list (crate-dep (name "version_check") (req "^0.9.4") (default-features #t) (kind 1)))) (hash "14cmp1lb8vm251hn3ddilwzlvv9ax4cvfjhg2ydq0336vhz4h9bf") (yanked #t) (rust-version "1.57.0")))

(define-public crate-nonn-0.1 (crate (name "nonn") (vers "0.1.4") (deps (list (crate-dep (name "version_check") (req "^0.9.4") (default-features #t) (kind 1)))) (hash "043fk675y87vhlc97yw06k4v4j5cnd7kr8jdyyr75zac2mi4a0p8") (rust-version "1.57.0")))

