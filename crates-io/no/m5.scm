(define-module (crates-io no m5) #:use-module (crates-io))

(define-public crate-nom5_locate-0.1 (crate (name "nom5_locate") (vers "0.1.0") (deps (list (crate-dep (name "bytecount") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "memchr") (req ">= 1.0.1, < 3.0.0") (default-features #t) (kind 0)) (crate-dep (name "nom") (req "^5.0.0-beta2") (default-features #t) (kind 0)))) (hash "0z8vnyy4711wknjhxb421wi6an5r377r69xzzf0ba658msv5pzsw") (features (quote (("std" "nom/std" "alloc") ("simd-accel" "bytecount/simd-accel") ("default" "std") ("avx-accel" "bytecount/avx-accel") ("alloc" "nom/alloc"))))))

(define-public crate-nom5_locate-0.1 (crate (name "nom5_locate") (vers "0.1.1") (deps (list (crate-dep (name "bytecount") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "memchr") (req ">= 1.0.1, < 3.0.0") (default-features #t) (kind 0)) (crate-dep (name "nom") (req "^5.0.0-beta2") (default-features #t) (kind 0)))) (hash "0g92csk8c2wb8bvvzwddlfjjy882wl3l54sb6h4xja4bgx314hrx") (features (quote (("std" "nom/std" "alloc") ("simd-accel" "bytecount/simd-accel") ("default" "std") ("avx-accel" "bytecount/avx-accel") ("alloc" "nom/alloc"))))))

