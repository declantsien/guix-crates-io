(define-module (crates-io no _a) #:use-module (crates-io))

(define-public crate-no_alloc-0.1 (crate (name "no_alloc") (vers "0.1.0") (deps (list (crate-dep (name "heapless") (req "^0.5.5") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "heapless") (req "^0.5.5") (features (quote ("x86-sync-pool"))) (default-features #t) (kind 2)))) (hash "1ph3j3b2k7j2f5lg6i139rrhx22704fr07x4aq0kkcl8gqy1dchc") (features (quote (("pool" "heapless") ("default") ("const_generics") ("coerce_unsized"))))))

