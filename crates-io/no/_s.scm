(define-module (crates-io no _s) #:use-module (crates-io))

(define-public crate-no_std_io-0.5 (crate (name "no_std_io") (vers "0.5.0") (deps (list (crate-dep (name "memchr") (req "^2") (kind 0)))) (hash "056mixy7cshxn7w1cyl7f4shpqifs3i5lq9q3wcdpamrvzlp13ad") (features (quote (("std" "alloc") ("nightly") ("default") ("alloc")))) (yanked #t)))

(define-public crate-no_std_io-0.5 (crate (name "no_std_io") (vers "0.5.1") (deps (list (crate-dep (name "memchr") (req "^2") (kind 0)))) (hash "1x98cx9pa9sbi5xn36n4kplhmxjp4n6xmkzvizx9nxkhhys66kka") (features (quote (("std" "alloc") ("nightly") ("default") ("alloc"))))))

(define-public crate-no_std_io-0.5 (crate (name "no_std_io") (vers "0.5.2") (deps (list (crate-dep (name "memchr") (req "^2") (kind 0)))) (hash "1jxb2zms61jq4rr1wryh5xsqrk4kg9yg46r803ss7sjbwsvi7n85") (features (quote (("std" "alloc") ("nightly") ("default") ("alloc"))))))

(define-public crate-no_std_io-0.5 (crate (name "no_std_io") (vers "0.5.3") (deps (list (crate-dep (name "memchr") (req "^2") (kind 0)))) (hash "0haky3jjwmmzjw6m4lnp76bnz1nhjqgdz1awvz7c3w0103i9jbvn") (features (quote (("std" "alloc") ("nightly") ("default") ("alloc"))))))

(define-public crate-no_std_io-0.6 (crate (name "no_std_io") (vers "0.6.0") (deps (list (crate-dep (name "memchr") (req "^2") (kind 0)))) (hash "02422zrmvxbgx5snr4mzc507dccmc6s9pwkjs57iph7jlq3g79bz") (features (quote (("std" "alloc") ("nightly") ("default") ("alloc"))))))

(define-public crate-no_std_strings-0.1 (crate (name "no_std_strings") (vers "0.1.0") (deps (list (crate-dep (name "serde") (req "^1") (optional #t) (default-features #t) (kind 0)))) (hash "0p5gz2yhyz8d04pm2g1yq9kv25zwnp6zrv0rrv78drd08cx6m0h8") (yanked #t) (v 2) (features2 (quote (("serde" "dep:serde"))))))

(define-public crate-no_std_strings-0.1 (crate (name "no_std_strings") (vers "0.1.1") (deps (list (crate-dep (name "serde") (req "^1") (optional #t) (default-features #t) (kind 0)))) (hash "0fvk56yr1kfqmidbam4g54na7ni09kjbjnamy24s2hjcj64m8qq0") (yanked #t) (v 2) (features2 (quote (("serde" "dep:serde"))))))

(define-public crate-no_std_strings-0.1 (crate (name "no_std_strings") (vers "0.1.2") (deps (list (crate-dep (name "serde") (req "^1") (optional #t) (default-features #t) (kind 0)))) (hash "1pfigwz8jp54w1174m0mgmbc981rv1vb3ap557x7piwrfdpibmd3") (v 2) (features2 (quote (("serde" "dep:serde"))))))

(define-public crate-no_std_strings-0.1 (crate (name "no_std_strings") (vers "0.1.3") (deps (list (crate-dep (name "serde") (req "^1") (optional #t) (default-features #t) (kind 0)))) (hash "0vm7k1969fzamcxk0cikp555h1rcxfdf6cwsfjy2423q3dycgc55") (v 2) (features2 (quote (("serde" "dep:serde"))))))

(define-public crate-no_std_strings-0.1 (crate (name "no_std_strings") (vers "0.1.31") (deps (list (crate-dep (name "serde") (req "^1") (optional #t) (default-features #t) (kind 0)))) (hash "1ywqlsk9v74044kizzcf84sf7djm7n5slvxhwm9y00k9jjmi57kl") (yanked #t) (v 2) (features2 (quote (("serde" "dep:serde"))))))

