(define-module (crates-io no ns) #:use-module (crates-io))

(define-public crate-nons-0.1 (crate (name "nons") (vers "0.1.0") (deps (list (crate-dep (name "bio") (req "0.14.*") (default-features #t) (kind 0)) (crate-dep (name "rust-htslib") (req "0.12.*") (default-features #t) (kind 0)))) (hash "09h14n00a1v7ms63il1iwswfbhmjbnwyjw5x8kvg86i4ns7bvda9")))

(define-public crate-nonscalar-0.1 (crate (name "nonscalar") (vers "0.1.0") (hash "03ygsj9dkkw2zjrgz7jjx8z6h7jlddbic8kbs34wbb2zjigakvh1")))

(define-public crate-nonscalar-0.2 (crate (name "nonscalar") (vers "0.2.0") (hash "0g0kimm236h1qxxc2yxr9nj76g2ycng4nj0s08a4mm3psfc362mp")))

(define-public crate-nonsense_package_to_test_crates_io_behaviour-0.1 (crate (name "nonsense_package_to_test_crates_io_behaviour") (vers "0.1.0") (hash "1dkipb1m88zw36qcgm0anpvl4p3q7wr9d2l1kmx4l8ckc9wkxsyz") (yanked #t)))

(define-public crate-nonsense_package_to_test_crates_io_behaviour-0.1 (crate (name "nonsense_package_to_test_crates_io_behaviour") (vers "0.1.1") (hash "08lji5l9q6vp0r2l3dyqpfkhiblqknvwpl00d956fsn7x02f9306") (yanked #t)))

(define-public crate-nonsmallnum-0.0.1 (crate (name "nonsmallnum") (vers "0.0.1") (deps (list (crate-dep (name "quickcheck") (req "^0.4") (default-features #t) (kind 2)) (crate-dep (name "quickcheck_macros") (req "^0.4") (default-features #t) (kind 2)))) (hash "0nc2bfx848gbz33q2glp3f54h2p3xiba92i3rg1nldn8msjzd9xw") (yanked #t)))

(define-public crate-nonsmallnum-0.0.2 (crate (name "nonsmallnum") (vers "0.0.2") (deps (list (crate-dep (name "quickcheck") (req "^0.4") (default-features #t) (kind 2)) (crate-dep (name "quickcheck_macros") (req "^0.4") (default-features #t) (kind 2)))) (hash "1z87xpljfwn23n6j3yplvh5ssn3n0r3gz1ii4z7ww8y76qrnkjqr") (yanked #t)))

(define-public crate-nonsmallnum-0.0.3 (crate (name "nonsmallnum") (vers "0.0.3") (deps (list (crate-dep (name "quickcheck") (req "^0.4") (default-features #t) (kind 2)) (crate-dep (name "quickcheck_macros") (req "^0.4") (default-features #t) (kind 2)))) (hash "1bbjlngp21s6jkaj53l0sfp3ajzykys7zlgwcvy9f63w3p8y404v") (yanked #t)))

(define-public crate-nonsmallnum-0.0.4 (crate (name "nonsmallnum") (vers "0.0.4") (deps (list (crate-dep (name "quickcheck") (req "^0.4") (default-features #t) (kind 2)) (crate-dep (name "quickcheck_macros") (req "^0.4") (default-features #t) (kind 2)))) (hash "1d2h6y0lw38sg2wfkyrzc017nkj7i2ljzabja4n9h605xsblil48") (yanked #t)))

(define-public crate-nonsmallnum-0.0.5 (crate (name "nonsmallnum") (vers "0.0.5") (deps (list (crate-dep (name "quickcheck") (req "^0.4") (default-features #t) (kind 2)) (crate-dep (name "quickcheck_macros") (req "^0.4") (default-features #t) (kind 2)))) (hash "07nrdk1xl9iia56yk8ika0mfmfbs2ll6gg2b8a47b8kn7rsvibf2") (yanked #t)))

(define-public crate-nonsmallnum-0.0.6 (crate (name "nonsmallnum") (vers "0.0.6-alpha.1") (deps (list (crate-dep (name "quickcheck") (req "^0.4") (default-features #t) (kind 2)) (crate-dep (name "quickcheck_macros") (req "^0.4") (default-features #t) (kind 2)))) (hash "0d9p6dmldfbv86lp09i0xrp3aq4k6xjg0c0w26pczhfv52d95rsw") (yanked #t)))

(define-public crate-nonsmallnum-0.0.6 (crate (name "nonsmallnum") (vers "0.0.6-alpha.3") (deps (list (crate-dep (name "quickcheck") (req "^0.4") (default-features #t) (kind 2)) (crate-dep (name "quickcheck_macros") (req "^0.4") (default-features #t) (kind 2)))) (hash "0fb7vpc94rkz387nycyss0llv4pnk567yl2pb42hcsslr4cbgqpg") (yanked #t)))

(define-public crate-nonsmallnum-0.0.6 (crate (name "nonsmallnum") (vers "0.0.6") (deps (list (crate-dep (name "quickcheck") (req "^0.4") (default-features #t) (kind 2)) (crate-dep (name "quickcheck_macros") (req "^0.4") (default-features #t) (kind 2)))) (hash "1pl73zqvgpl2vr52s6pf7ch6168nky3ixw3wrql99ars3aw0c6w0") (yanked #t)))

(define-public crate-nonsmallnum-0.0.7 (crate (name "nonsmallnum") (vers "0.0.7-alpha.1") (deps (list (crate-dep (name "quickcheck") (req "^0.4") (default-features #t) (kind 2)) (crate-dep (name "quickcheck_macros") (req "^0.4") (default-features #t) (kind 2)))) (hash "1h9yia4pbfwc9pgqlmd2m9pcmcrb4vahjz74j2qsazi3cq8m2kqq") (yanked #t)))

(define-public crate-nonsmallnum-0.0.7 (crate (name "nonsmallnum") (vers "0.0.7-alpha.2") (deps (list (crate-dep (name "quickcheck") (req "^0.4") (default-features #t) (kind 2)) (crate-dep (name "quickcheck_macros") (req "^0.4") (default-features #t) (kind 2)))) (hash "1s6li8p2p76dz6qwv0j12prfk199854s2qw7kajsxjv6r2vl8fjd") (yanked #t)))

(define-public crate-nonsmallnum-0.0.7 (crate (name "nonsmallnum") (vers "0.0.7-alpha.3") (deps (list (crate-dep (name "quickcheck") (req "^0.3") (default-features #t) (kind 2)))) (hash "1irlpq0b82cimv18lj5skxyg3fl5bj7xfilbghm2z1p1gab495r0") (yanked #t)))

(define-public crate-nonstd-0.1 (crate (name "nonstd") (vers "0.1.0") (hash "18xjdg7x776hpx6qq680s0ak98l2qdwp7i0fr9py0ilb39rb7vvg")))

(define-public crate-nonstdfloat-0.1 (crate (name "nonstdfloat") (vers "0.1.0") (deps (list (crate-dep (name "bitvec") (req "~1") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "~0.2") (default-features #t) (kind 0)) (crate-dep (name "proptest") (req "~1") (default-features #t) (kind 2)) (crate-dep (name "simple-soft-float") (req "~0.1") (optional #t) (default-features #t) (kind 0)))) (hash "1jnxpp8ic8br85h5prqyxp3z3981w538vc3ndk0w825ddlncr3jw") (features (quote (("f64") ("f128" "simple-soft-float") ("extendable" "simple-soft-float") ("default")))) (yanked #t)))

(define-public crate-nonstdfloat-0.1 (crate (name "nonstdfloat") (vers "0.1.1") (deps (list (crate-dep (name "bitvec") (req "~1") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "~0.2") (default-features #t) (kind 0)) (crate-dep (name "proptest") (req "~1") (default-features #t) (kind 2)) (crate-dep (name "simple-soft-float") (req "~0.1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "strafe-testing") (req "~0.1") (default-features #t) (kind 2)))) (hash "0zdcj38bgaj0k8c243yv124xqarhx8vnfw6w4xdq87yf9l2bpmvk") (features (quote (("f64") ("f128" "simple-soft-float") ("extendable" "simple-soft-float") ("default"))))))

