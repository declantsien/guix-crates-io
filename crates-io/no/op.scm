(define-module (crates-io no op) #:use-module (crates-io))

(define-public crate-noop-0.1 (crate (name "noop") (vers "0.1.0") (hash "1pv4w83ssw310pkz1mahhjl5wkz44ywldnifv4f1bway9a5424hn")))

(define-public crate-noop-attr-0.1 (crate (name "noop-attr") (vers "0.1.0") (hash "028a7zrvkdddh0c189917ywsw39dw5hzlrsv8zy45k5yg84fgi30")))

(define-public crate-noop-waker-0.1 (crate (name "noop-waker") (vers "0.1.0") (deps (list (crate-dep (name "futures-lite") (req "^1") (default-features #t) (kind 2)))) (hash "0qaj7l52i57di03wfh90q3rk700hwg9dz26hq7ngnynf54zjr42n")))

(define-public crate-noop_proc_macro-0.1 (crate (name "noop_proc_macro") (vers "0.1.0") (hash "0wfv666i0am2ph8nyydv6q1bs81q4dq7vnb6zpphlm1jvdp3wiqs")))

(define-public crate-noop_proc_macro-0.2 (crate (name "noop_proc_macro") (vers "0.2.0") (hash "0si51sfsbh7h5ghsrdhg41x5ikhsf6pqn4wpzs849p5q5qxfzbdr")))

(define-public crate-noop_proc_macro-0.2 (crate (name "noop_proc_macro") (vers "0.2.1") (hash "0in1l0rjxzs4fylb6zad484z1c58jxyzchhc12k0cjrvm0y6zwsz")))

(define-public crate-noop_proc_macro-0.3 (crate (name "noop_proc_macro") (vers "0.3.0") (hash "1j2v1c6ric4w9v12h34jghzmngcwmn0hll1ywly4h6lcm4rbnxh6")))

