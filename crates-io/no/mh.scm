(define-module (crates-io no mh) #:use-module (crates-io))

(define-public crate-nomhttp-0.1 (crate (name "nomhttp") (vers "0.1.0") (deps (list (crate-dep (name "nom") (req "^7.1.3") (features (quote ("std"))) (default-features #t) (kind 0)))) (hash "1rii4rs0kjyklcxzch8mqlmgimggfra0qkabi16nlg1zzxv8wfrp")))

