(define-module (crates-io no ha) #:use-module (crates-io))

(define-public crate-nohash-0.0.0 (crate (name "nohash") (vers "0.0.0") (hash "023ml856f18wzwly08mwi80vpwvill8szd158x9414cw4dvqbqx4")))

(define-public crate-nohash-0.2 (crate (name "nohash") (vers "0.2.0") (hash "1ji8ylndxnq02pjwxfbac0yfs7xmcibpfd9c8j1xzb7pcvxqky50") (features (quote (("std") ("default" "std"))))))

(define-public crate-nohash-hasher-0.1 (crate (name "nohash-hasher") (vers "0.1.0") (hash "01vls6vzik4z59hnnnsj9l3430y3fcm7k4lywydfr31b8dr3qn97")))

(define-public crate-nohash-hasher-0.1 (crate (name "nohash-hasher") (vers "0.1.1") (hash "1f0vfk8rcvc5v4ycblyg6gbc0sm9x2qxjlzbnv61klljrvy8l4qd")))

(define-public crate-nohash-hasher-0.1 (crate (name "nohash-hasher") (vers "0.1.2") (hash "13hfpr4xl0xwnxs11vr53nvxbz5rdpm38w3gdyj3p6kzr5p7lraf")))

(define-public crate-nohash-hasher-0.1 (crate (name "nohash-hasher") (vers "0.1.3") (hash "13skkr5aimyx5za44fzcqpzb5x31rv20p60agvqynnb1qbqjn6kj") (features (quote (("std") ("default" "std"))))))

(define-public crate-nohash-hasher-0.2 (crate (name "nohash-hasher") (vers "0.2.0") (hash "0lf4p6k01w4wm7zn4grnihzj8s7zd5qczjmzng7wviwxawih5x9b") (features (quote (("std") ("default" "std"))))))

