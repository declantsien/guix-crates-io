(define-module (crates-io no sr) #:use-module (crates-io))

(define-public crate-nosrond_http-0.1 (crate (name "nosrond_http") (vers "0.1.0") (deps (list (crate-dep (name "mockito") (req "^1.4.0") (default-features #t) (kind 2)) (crate-dep (name "reqwest") (req "^0.12") (features (quote ("json"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.37") (features (quote ("rt" "macros"))) (kind 2)))) (hash "1mmnc8pk1f1f30bcif4nna0mwf14ys4cjjzc89q04ahp5nbvghwn") (features (quote (("blocking" "reqwest/blocking"))))))

