(define-module (crates-io no tz) #:use-module (crates-io))

(define-public crate-notzero-1 (crate (name "notzero") (vers "1.0.0") (deps (list (crate-dep (name "typewit") (req "^1.8") (kind 0)))) (hash "0a2bbvm8qr8iraliyd5lddsg6mr83w2xgrqz5gp2mwc0ffkk4syg")))

(define-public crate-notzero-1 (crate (name "notzero") (vers "1.0.1") (deps (list (crate-dep (name "trybuild") (req "^1.0") (default-features #t) (kind 2)) (crate-dep (name "typewit") (req "^1.8") (kind 0)))) (hash "13casw3a8dasblkr2z1cfav9a16j680jsvwj6bdrfr26sca18iki")))

