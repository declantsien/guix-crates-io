(define-module (crates-io no xu) #:use-module (crates-io))

(define-public crate-noxue-compiler-0.1 (crate (name "noxue-compiler") (vers "0.1.0") (hash "1gjmj7m4vs3j9v39fk0ik1c2vmv3f46w8kr9wcgzhavfdlfxjfpf")))

(define-public crate-noxue-compiler-0.1 (crate (name "noxue-compiler") (vers "0.1.1") (hash "18pm6xh5c0z0v0sgpnmyidqhdavd1v70k120lrncsi03kqs4abm8")))

(define-public crate-noxue-compiler-0.1 (crate (name "noxue-compiler") (vers "0.1.2") (hash "0pa4l13r4cia34aikhhh3f2ya0vqq5cnpb9g79kkikisqnizd20a")))

(define-public crate-noxue-compiler-1 (crate (name "noxue-compiler") (vers "1.0.0") (deps (list (crate-dep (name "serde") (req "^1") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "uuid") (req "^0.8.2") (features (quote ("v4"))) (default-features #t) (kind 0)))) (hash "1xqz8jkdfbfxkz7ih0jzqclrz9mc1k1lh73d7d7mm6y17pyxc0bv")))

(define-public crate-noxue-compiler-1 (crate (name "noxue-compiler") (vers "1.0.1") (deps (list (crate-dep (name "serde") (req "^1") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "uuid") (req "^0.8.2") (features (quote ("v4"))) (default-features #t) (kind 0)))) (hash "1j712z42fbava2b2ls8i04fndw1qiwxr7isbias88gj0v6vjkpch")))

(define-public crate-noxue-compiler-1 (crate (name "noxue-compiler") (vers "1.0.2") (deps (list (crate-dep (name "serde") (req "^1") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "uuid") (req "^0.8.2") (features (quote ("v4"))) (default-features #t) (kind 0)))) (hash "171bb45wk2hb6h9vq18b9ygnbm4svym9qh3ajfs1504xz67vvi3n")))

(define-public crate-noxue-compiler-1 (crate (name "noxue-compiler") (vers "1.0.3") (deps (list (crate-dep (name "serde") (req "^1") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "uuid") (req "^0.8.2") (features (quote ("v4"))) (default-features #t) (kind 0)))) (hash "045x2pzzz5myzwqah3phgcy8vbwfc62bsp851wc1dqcbz2mbddfp")))

(define-public crate-noxue-compiler-1 (crate (name "noxue-compiler") (vers "1.0.4") (deps (list (crate-dep (name "log") (req "^0.4.14") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "uuid") (req "^0.8.2") (features (quote ("v4"))) (default-features #t) (kind 0)))) (hash "1mh12s3q8qc8ykjaq33xixs2gvcpk9d0621x2q2nzhn0r1k3s69k")))

