(define-module (crates-io no il) #:use-module (crates-io))

(define-public crate-noilib-simple-0.5 (crate (name "noilib-simple") (vers "0.5.0") (deps (list (crate-dep (name "image") (req "^0.24.2") (default-features #t) (kind 2)))) (hash "1h3qw493rbb1bc9y6xnfwkp31bg21hcvxbq2r2qbdfd89drsq85w")))

(define-public crate-noilib-simple-0.5 (crate (name "noilib-simple") (vers "0.5.1") (deps (list (crate-dep (name "image") (req "^0.24.2") (default-features #t) (kind 2)))) (hash "0i7v7b0v8hp2zr69r2d7dyh18ibplmr78xjmxjjwdllsz6yjs8dq")))

