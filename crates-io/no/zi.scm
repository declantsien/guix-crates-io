(define-module (crates-io no zi) #:use-module (crates-io))

(define-public crate-nozio-0.1 (crate (name "nozio") (vers "0.1.0") (hash "08b5z2h1vhfwlc2km9llvvdqvhpicgqalb6b2i1wjy49rh183sz1") (yanked #t)))

(define-public crate-nozio-0.1 (crate (name "nozio") (vers "0.1.1") (hash "1kr5ax2ckxxn5rbhd2kar8ha64hnrhqzzqq4j3jadxswvk34k22a") (yanked #t)))

