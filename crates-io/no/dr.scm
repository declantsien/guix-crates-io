(define-module (crates-io no dr) #:use-module (crates-io))

(define-public crate-nodrop-0.1 (crate (name "nodrop") (vers "0.1.0") (hash "1r5pxz6489amfiyf8crym7drkhw26pysz62kg1ln27spl0l3j3l8")))

(define-public crate-nodrop-0.1 (crate (name "nodrop") (vers "0.1.1") (hash "1d9s9m55yr621bxb8dkzpazy7qkrdxn7rwh248i8g17gspgmji9i")))

(define-public crate-nodrop-0.1 (crate (name "nodrop") (vers "0.1.2") (hash "0a1a1h2ci6ksxswfcf6ldbiyg0gqzbnnzzizq9c25glx89yz3zqw") (features (quote (("no_drop_flag"))))))

(define-public crate-nodrop-0.1 (crate (name "nodrop") (vers "0.1.3") (deps (list (crate-dep (name "odds") (req "^0.1") (default-features #t) (kind 0)))) (hash "14abqcfv91xhm95fn40093ia48s7hm4b3dkbb59x6l9zsljsrpl5") (features (quote (("no_drop_flag"))))))

(define-public crate-nodrop-0.1 (crate (name "nodrop") (vers "0.1.4") (deps (list (crate-dep (name "odds") (req "^0.2") (default-features #t) (kind 0)))) (hash "1k4v2qc5klpiv8vf2avrllg85jqrqglyd2f1l5xks7ipnlpyrdbc") (features (quote (("no_drop_flag"))))))

(define-public crate-nodrop-0.1 (crate (name "nodrop") (vers "0.1.5") (deps (list (crate-dep (name "odds") (req "^0.2") (default-features #t) (kind 0)))) (hash "0xm1h4si6awlpg18xx4ais4vj9438532q11c2q5h6h1rbbqz1fg0") (features (quote (("use_needs_drop") ("no_drop_flag"))))))

(define-public crate-nodrop-0.1 (crate (name "nodrop") (vers "0.1.6") (deps (list (crate-dep (name "odds") (req "^0.2.12") (kind 0)))) (hash "13yx41qq386vh0nvg1z1zsifgm11cm6l9gsw4yzzgvmxrvdj56jd") (features (quote (("use_needs_drop") ("std" "odds/std") ("no_drop_flag") ("default" "std"))))))

(define-public crate-nodrop-0.1 (crate (name "nodrop") (vers "0.1.7") (deps (list (crate-dep (name "odds") (req "^0.2.12") (kind 0)))) (hash "0zbfxgy5hza1rz6yy56j0lynnyb4fmjjjkvbzkvn296i0khpz2dc") (features (quote (("use_needs_drop") ("std" "odds/std") ("default" "std"))))))

(define-public crate-nodrop-0.1 (crate (name "nodrop") (vers "0.1.8") (deps (list (crate-dep (name "nodrop-union") (req "^0.1.8") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "odds") (req "^0.2.12") (kind 0)))) (hash "1ibwvmddp0rrrqgy9cjw0dbspwfs1i6bxd6rsc5ym3f9yk9svfqd") (features (quote (("use_union" "nodrop-union") ("use_needs_drop") ("std" "odds/std") ("default" "std"))))))

(define-public crate-nodrop-0.1 (crate (name "nodrop") (vers "0.1.9") (deps (list (crate-dep (name "nodrop-union") (req "^0.1.8") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "odds") (req "^0.2.12") (kind 0)))) (hash "1chpa6liwidsy1i14vi4asd1c1s0ny8k0vnc61j5kfmy176p9kaj") (features (quote (("use_union" "nodrop-union") ("use_needs_drop") ("std" "odds/std") ("default" "std"))))))

(define-public crate-nodrop-0.1 (crate (name "nodrop") (vers "0.1.10") (deps (list (crate-dep (name "nodrop-union") (req "^0.1.8") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "odds") (req "^0.2.12") (kind 0)))) (hash "1vk9zh754lbklcq9j0q3vdz65jqp9frj1r4npvm5pfqjq75n43y2") (features (quote (("use_union" "nodrop-union") ("use_needs_drop") ("std" "odds/std") ("default" "std"))))))

(define-public crate-nodrop-0.1 (crate (name "nodrop") (vers "0.1.11") (deps (list (crate-dep (name "nodrop-union") (req "^0.1.8") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "odds") (req "^0.2.12") (kind 0)))) (hash "0xxz5az4gpf878aap6ddqxsn4wgs3g7qd6d4h645aipkazpl8jxa") (features (quote (("use_union" "nodrop-union") ("use_needs_drop") ("std" "odds/std") ("default" "std"))))))

(define-public crate-nodrop-0.1 (crate (name "nodrop") (vers "0.1.12") (deps (list (crate-dep (name "nodrop-union") (req "^0.1.8") (optional #t) (default-features #t) (kind 0)))) (hash "18p0d9xniqbwrhf6v83lw399c978sa5yvwk2aad0c23ilpf2h8ls") (features (quote (("use_union" "nodrop-union") ("use_needs_drop") ("std") ("default" "std"))))))

(define-public crate-nodrop-0.1 (crate (name "nodrop") (vers "0.1.13") (deps (list (crate-dep (name "nodrop-union") (req "^0.1.8") (optional #t) (default-features #t) (kind 0)))) (hash "0if9ifn6rvar5jirx4b3qh4sl5kjkmcifycvzhxa9j3crkfng5ig") (features (quote (("use_union" "nodrop-union") ("use_needs_drop") ("std") ("default" "std"))))))

(define-public crate-nodrop-0.1 (crate (name "nodrop") (vers "0.1.14") (deps (list (crate-dep (name "nodrop-union") (req "^0.1.8") (optional #t) (default-features #t) (kind 0)))) (hash "1fz1v9r8ijacf0hlq0pdv5l9mz8vgqg1snmhvpjmi9aci1b4mvvj") (features (quote (("use_union" "nodrop-union") ("use_needs_drop") ("std") ("default" "std"))))))

(define-public crate-nodrop-union-0.1 (crate (name "nodrop-union") (vers "0.1.8") (hash "1n2rh3pac8fkdawzlfn91lcx5dkjkhq07lqs4cv889ha6mb1hk8n")))

(define-public crate-nodrop-union-0.1 (crate (name "nodrop-union") (vers "0.1.9") (hash "07zhhmk0yygarmzv4zxm9vrkm35c53k82q6v6y1gd9dn9rq625z3")))

(define-public crate-nodrop-union-0.1 (crate (name "nodrop-union") (vers "0.1.10") (hash "0jsnkdn9l8jlmb9h4wssi76sxnyxwnyi00p6y1p2gdq7c1gdw2b7")))

(define-public crate-nodrop-union-0.1 (crate (name "nodrop-union") (vers "0.1.11") (hash "1h59pph19rxanyqcaid8pg73s7wmzdx3zhjv5snlim5qx606zxkc")))

