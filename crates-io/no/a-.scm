(define-module (crates-io no a-) #:use-module (crates-io))

(define-public crate-noa-cli-0.100 (crate (name "noa-cli") (vers "0.100.0") (hash "1s19p6fax6z2d6ialdxvyi3ppky9bw6pfv20zfyqi00jq4pws5z4")))

(define-public crate-noa-core-0.100 (crate (name "noa-core") (vers "0.100.0") (hash "127cp89406j77pmjkmgrrfd151qy3qpxasxqbvzcjqbgaayn93gy")))

