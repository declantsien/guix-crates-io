(define-module (crates-io no ex) #:use-module (crates-io))

(define-public crate-noexcept-0.0.1 (crate (name "noexcept") (vers "0.0.1") (deps (list (crate-dep (name "noexcept-impl") (req "^0.0.1") (default-features #t) (kind 0)))) (hash "1155kp6766ljv77rs0mp8kijamjw19m19443milnd1z7yjg9kfsa")))

(define-public crate-noexcept-0.0.2 (crate (name "noexcept") (vers "0.0.2") (deps (list (crate-dep (name "noexcept-impl") (req "^0.0.2") (default-features #t) (kind 0)))) (hash "005vxv8f4iczg47fyz5prwfi5xmmv6p9l5gmh6wy4h9jkjb6c26w")))

(define-public crate-noexcept-impl-0.0.1 (crate (name "noexcept-impl") (vers "0.0.1") (deps (list (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2.0") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "0l3ys7lijpzk0syjkfa6xqh7v1gp2kfhm6yaddvmyy3j9z171x5f")))

(define-public crate-noexcept-impl-0.0.2 (crate (name "noexcept-impl") (vers "0.0.2") (deps (list (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2.0") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "151s2fdp9vfxd3ci5mafgz20ph8n0w75yc9zkxbipzmbh8abp94l")))

