(define-module (crates-io no wo) #:use-module (crates-io))

(define-public crate-nowo-0.0.1 (crate (name "nowo") (vers "0.0.1") (hash "0d9xicfsymn7cmfw6ngmjfbyyb4nvfghvj8wggwv9n680sy9f7hp")))

(define-public crate-nowo-0.0.2 (crate (name "nowo") (vers "0.0.2") (hash "1kxpxbi0fnvb63y56iv95vrdg6j25i75mifmfyajj2wy8x604bp5")))

(define-public crate-nowo-0.0.3 (crate (name "nowo") (vers "0.0.3") (hash "0pxkxxvmaf7fbazajxax8wk9nw4pdzwzp0k15s0f0an7mcd89zl5")))

