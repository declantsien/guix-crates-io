(define-module (crates-io no ca) #:use-module (crates-io))

(define-public crate-nocargo-0.0.0 (crate (name "nocargo") (vers "0.0.0") (hash "1s4f14xcw693xpnfg2g4sw2q7b6nklbrn466vm6s6xs9y0pqdxf0")))

(define-public crate-nocash_gba_log-0.1 (crate (name "nocash_gba_log") (vers "0.1.0") (deps (list (crate-dep (name "log") (req "^0.4.18") (default-features #t) (kind 0)))) (hash "100sc07kr0sd8nnyc7mbwhwq0c8qf161f4qbdrblhlsql1j6ls48")))

