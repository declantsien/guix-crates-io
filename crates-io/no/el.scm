(define-module (crates-io no el) #:use-module (crates-io))

(define-public crate-noel-0.1 (crate (name "noel") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^4.4.2") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2.149") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.50") (default-features #t) (kind 0)))) (hash "02wilk0bklk46qjm7byqhx5ii2i6sh5iffn7ly74lr6iqxq0qz9b")))

(define-public crate-noel-0.2 (crate (name "noel") (vers "0.2.1") (deps (list (crate-dep (name "clap") (req "^4.4.2") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2.149") (default-features #t) (kind 0)) (crate-dep (name "num_cpus") (req "^1.13.0") (default-features #t) (kind 0)) (crate-dep (name "rayon") (req "^1.8.0") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.50") (default-features #t) (kind 0)))) (hash "0vdzfpnzkvfbcz39964vm67ia295azbk0s0f5sv10q23svg6fk04")))

