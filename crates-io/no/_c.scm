(define-module (crates-io no _c) #:use-module (crates-io))

(define-public crate-no_code_download_counter-1 (crate (name "no_code_download_counter") (vers "1.0.0") (hash "176r6nnhgwl5bp4i9c6q1cwpj0piwl9s18lbr1d9cnqzcz4zprkm")))

(define-public crate-no_color-0.1 (crate (name "no_color") (vers "0.1.0") (hash "1m0x6i5q1jav2jgajkg4irrkfgv4lakpdvmj310b90wswdyy9xdx")))

