(define-module (crates-io no zo) #:use-module (crates-io))

(define-public crate-nozo-0.0.0 (crate (name "nozo") (vers "0.0.0") (hash "14agj9kcakcksmrsj7ikkj6wy5mry89qf1nmhfbpf658aqrpy9k7")))

(define-public crate-nozomi-1 (crate (name "nozomi") (vers "1.0.0") (deps (list (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)))) (hash "0l0z34agcbx2n8ayybh39jz3bvphc0ka1z03m66a7z6swn5wxqbg") (yanked #t)))

(define-public crate-nozomi-1 (crate (name "nozomi") (vers "1.0.1") (deps (list (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)))) (hash "1jsyfgkvin09ciwg2pz1wqpav29nvyc1gnj9mk89yc2z432spcs8") (yanked #t)))

(define-public crate-nozomi-1 (crate (name "nozomi") (vers "1.0.2") (deps (list (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)))) (hash "091srrkwjsnzafzzpnmgphhf1b61hmd54dy1100qx9djcv04hrr6")))

(define-public crate-nozomi-2 (crate (name "nozomi") (vers "2.0.0") (deps (list (crate-dep (name "env_logger") (req "^0.10.0") (default-features #t) (kind 0)) (crate-dep (name "error-stack") (req "^0.3.1") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.17") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)))) (hash "0py0w5cskllw0bjxfw7j856c4kv85q9izjwxnxfmjhmjxkhpagki")))

(define-public crate-nozomi-2 (crate (name "nozomi") (vers "2.0.1") (deps (list (crate-dep (name "env_logger") (req "^0.11.2") (default-features #t) (kind 0)) (crate-dep (name "error-stack") (req "^0.4.1") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.20") (default-features #t) (kind 0)) (crate-dep (name "pretty_assertions") (req "^1.4.0") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)))) (hash "18kpdxdqji5xyzvvikkk0jwxflin5bj2ss640chr9c2wiac383xf")))

(define-public crate-nozomi-i3-workspace-0.1 (crate (name "nozomi-i3-workspace") (vers "0.1.0") (deps (list (crate-dep (name "anyhow") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "chrono") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "dotenv") (req "^0.15") (default-features #t) (kind 0)) (crate-dep (name "env_logger") (req "^0.7.1") (default-features #t) (kind 0)) (crate-dep (name "i3ipc") (req "^0.10") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "which") (req "^3.1") (default-features #t) (kind 0)))) (hash "1idf24j50cmk2c6ly9w64rkawh22hh9shvpgpxywrdqb4bf10279")))

