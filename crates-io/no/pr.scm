(define-module (crates-io no pr) #:use-module (crates-io))

(define-public crate-noproto-0.0.0 (crate (name "noproto") (vers "0.0.0") (hash "14g9h0ma3rlcbsgiklsm8hwcpqxws9s2ldrkrhmhqb9pikr6hx91")))

(define-public crate-noproto-0.1 (crate (name "noproto") (vers "0.1.0") (deps (list (crate-dep (name "heapless") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "noproto-derive") (req "^0.1.0") (optional #t) (default-features #t) (kind 0)))) (hash "0c4llfs1an2kv4b5znjfb0pg32bp821fsd7jahj7czyggfs8rbip") (features (quote (("default" "derive")))) (v 2) (features2 (quote (("derive" "dep:noproto-derive"))))))

(define-public crate-noproto-build-0.0.0 (crate (name "noproto-build") (vers "0.0.0") (hash "1g6wl3b1dmk49v645jl7c6j34a4b32am1pxwqkvz18hld91zf0sd")))

(define-public crate-noproto-derive-0.0.0 (crate (name "noproto-derive") (vers "0.0.0") (hash "03blqraib56zh8zghqcgyj8srm2ivy60asrzrdzbkfnxgb6xq96l")))

(define-public crate-noproto-derive-0.1 (crate (name "noproto-derive") (vers "0.1.0") (deps (list (crate-dep (name "anyhow") (req "^1.0.1") (default-features #t) (kind 0)) (crate-dep (name "itertools") (req "^0.10") (features (quote ("use_alloc"))) (kind 0)) (crate-dep (name "proc-macro2") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.3") (features (quote ("extra-traits"))) (default-features #t) (kind 0)))) (hash "19c60js7szmpyh4rb1jyh4hd4z51djc3qv9ikwyx95c3n5bz6j18")))

