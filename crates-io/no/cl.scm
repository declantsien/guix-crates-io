(define-module (crates-io no cl) #:use-module (crates-io))

(define-public crate-noclip-0.1 (crate (name "noclip") (vers "0.1.0") (deps (list (crate-dep (name "clipboard") (req "^0.5") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.2") (default-features #t) (kind 0)))) (hash "1593i9h8innljshnfxc6g83i9pq6vnxh9c0sbvv23nx8arb3v014")))

