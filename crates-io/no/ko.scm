(define-module (crates-io no ko) #:use-module (crates-io))

(define-public crate-noko-0.1 (crate (name "noko") (vers "0.1.0") (hash "1imbsdpgl6zzn5r1ksajgxx0yzlwx6dszn8xqnpin3absmfmb6a0")))

(define-public crate-noko-0.2 (crate (name "noko") (vers "0.2.0") (deps (list (crate-dep (name "chumsky") (req "^0.9.2") (kind 0)) (crate-dep (name "insta") (req "^1.31.0") (features (quote ("yaml"))) (default-features #t) (kind 2)) (crate-dep (name "miette") (req "^5.9.0") (features (quote ("fancy"))) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.43") (default-features #t) (kind 0)))) (hash "0if8s4qz2kvqvpj3jxbq0digs8z2mw4cf2as1x08ysdcl76m66hj")))

(define-public crate-noko-0.3 (crate (name "noko") (vers "0.3.0") (deps (list (crate-dep (name "chumsky") (req "^0.9.2") (kind 0)) (crate-dep (name "insta") (req "^1.31.0") (features (quote ("yaml"))) (default-features #t) (kind 2)) (crate-dep (name "miette") (req "^5.9.0") (features (quote ("fancy"))) (kind 0)) (crate-dep (name "proptest") (req "^1.2.0") (default-features #t) (kind 2)) (crate-dep (name "thiserror") (req "^1.0.43") (default-features #t) (kind 0)))) (hash "043g40839206wh396s1igg0n1nr2din8icwiw0001jaqvlsgdgl0")))

(define-public crate-noko-sort-0.1 (crate (name "noko-sort") (vers "0.1.0") (hash "1w2mxi8am0rk6aw895miadyc6lz0akrylw3ss67xglkhsxl823nv")))

