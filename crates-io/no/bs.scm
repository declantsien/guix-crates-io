(define-module (crates-io no bs) #:use-module (crates-io))

(define-public crate-nobs-vk-0.1 (crate (name "nobs-vk") (vers "0.1.0") (deps (list (crate-dep (name "shared_library") (req "^0.1.7") (default-features #t) (kind 0)))) (hash "04b7kqwlpqbhy3iwk74xqd73pc0ssrvyvfvhagw30zswh0c080aj")))

(define-public crate-nobs-vk-0.1 (crate (name "nobs-vk") (vers "0.1.1") (deps (list (crate-dep (name "shared_library") (req "^0.1.7") (default-features #t) (kind 0)))) (hash "0szzvlrzx7v0a3c8nbijh08rl510nhg8n6yykl831ix7jz2i0w72")))

(define-public crate-nobs-vk-0.1 (crate (name "nobs-vk") (vers "0.1.2") (deps (list (crate-dep (name "shared_library") (req "^0.1.7") (default-features #t) (kind 0)))) (hash "1s1h1m7rz53gzry53xsikpdcmv5jas4nz3vpizsw22hdbm2c2v7k")))

(define-public crate-nobs-vk-0.1 (crate (name "nobs-vk") (vers "0.1.3") (deps (list (crate-dep (name "shared_library") (req "^0.1.7") (default-features #t) (kind 0)))) (hash "1w3f2688rkkn91calphnwyz3cg6kad8sikfd3g1f2a7b66aca8lg")))

(define-public crate-nobs-vk-0.1 (crate (name "nobs-vk") (vers "0.1.4") (deps (list (crate-dep (name "shared_library") (req "^0.1.7") (default-features #t) (kind 0)))) (hash "12a6fac3k566g4wg46v3q49z7x7svl986z8x0f6jhm4sch9iahf0")))

(define-public crate-nobs-vk-0.1 (crate (name "nobs-vk") (vers "0.1.5") (deps (list (crate-dep (name "shared_library") (req "^0.1.7") (default-features #t) (kind 0)))) (hash "0d1a0wlzydqshv76c69xjjy1fl9f6xplzqddk00rmyrmxikqwzg8")))

(define-public crate-nobs-vk-0.1 (crate (name "nobs-vk") (vers "0.1.6") (deps (list (crate-dep (name "shared_library") (req "^0.1.7") (default-features #t) (kind 0)))) (hash "1r3c860my73ddjzj6x56x7dxsh7zmddb4k8l7x848pvq4jbzq5ld")))

(define-public crate-nobs-vk-0.1 (crate (name "nobs-vk") (vers "0.1.7") (deps (list (crate-dep (name "shared_library") (req "^0.1.7") (default-features #t) (kind 0)))) (hash "1x0qg8v6aci415x25pgq0ipn20n13fpjyma351nr8irpfg3lqf3x")))

(define-public crate-nobs-vk-0.1 (crate (name "nobs-vk") (vers "0.1.8") (deps (list (crate-dep (name "shared_library") (req "^0.1.7") (default-features #t) (kind 0)))) (hash "1si6lprd6gx4mladi6hg07hg95i6jj2idm36p2waz8p4p3bpgar0")))

(define-public crate-nobs-vk-0.1 (crate (name "nobs-vk") (vers "0.1.9") (deps (list (crate-dep (name "shared_library") (req "^0.1.7") (default-features #t) (kind 0)))) (hash "10qmkf9zkzwmldhrwx9xyazsabil632r6rczqd9i7d0viz10c51s")))

(define-public crate-nobs-vk-0.1 (crate (name "nobs-vk") (vers "0.1.10") (deps (list (crate-dep (name "shared_library") (req "^0.1.7") (default-features #t) (kind 0)))) (hash "1z50n0ply6s44sdlis04cqirxy6dl72k8iwfn95ykk702bs4ih5q")))

(define-public crate-nobs-vk-0.2 (crate (name "nobs-vk") (vers "0.2.0") (deps (list (crate-dep (name "shared_library") (req "^0.1.7") (default-features #t) (kind 0)))) (hash "0gkrsjc2ahbkilb9fz62a1009kqa4ndqba9v657ckygvmb8qd52j")))

(define-public crate-nobs-vkmem-0.1 (crate (name "nobs-vkmem") (vers "0.1.0") (deps (list (crate-dep (name "nobs-vk") (req "^0.1.8") (default-features #t) (kind 0)))) (hash "0670m086bh9asdj274awmfs26wchrp0cv4c4s62bwrvvy5jxb72g")))

(define-public crate-nobs-vkmem-0.2 (crate (name "nobs-vkmem") (vers "0.2.0") (deps (list (crate-dep (name "nobs-vk") (req "^0.2.0") (default-features #t) (kind 0)))) (hash "0plfwa403749w8rjm7ni9dkpw9aa1dvq1ynnzikhlx6xdcz3wyvl")))

(define-public crate-nobs-vkpipes-0.1 (crate (name "nobs-vkpipes") (vers "0.1.0") (deps (list (crate-dep (name "nobs-vk") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "nobs-vkpipes-macro") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "0n5iyrws71mpdv2vs1wj9y2hfxw5b1bpwnb8h8c451zd630wh198")))

(define-public crate-nobs-vkpipes-macro-0.1 (crate (name "nobs-vkpipes-macro") (vers "0.1.0") (deps (list (crate-dep (name "nobs-vk") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "shaderc") (req "^0.3") (default-features #t) (kind 0)))) (hash "0qv07md9wyjnhvh4ii6vys2v2cx4cxbl8x7pp0ca9pvg3rdq0czx")))

(define-public crate-nobs-vulkanism-0.1 (crate (name "nobs-vulkanism") (vers "0.1.0") (deps (list (crate-dep (name "nobs-vulkanism-headless") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "winit") (req "^0.18.1") (default-features #t) (kind 0)))) (hash "1riy22ivylr54dbd18nykykzdz9f77126bfhzyklkb96lax1ndvg")))

(define-public crate-nobs-vulkanism-headless-0.1 (crate (name "nobs-vulkanism-headless") (vers "0.1.0") (deps (list (crate-dep (name "nobs-vk") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "nobs-vkmem") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "nobs-vkpipes") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "1s5dzz3n4z6gw389aq3caa7fv9iwsqgwd0cpdg78in9798h0c56k")))

(define-public crate-nobsign-0.1 (crate (name "nobsign") (vers "0.1.0") (deps (list (crate-dep (name "byteorder") (req "^0.5.0") (default-features #t) (kind 0)) (crate-dep (name "ring") (req "^0.2.3") (default-features #t) (kind 0)) (crate-dep (name "rustc-serialize") (req "^0.3.19") (default-features #t) (kind 0)) (crate-dep (name "time") (req "^0.1.35") (default-features #t) (kind 0)))) (hash "0fc7qdy6p3lq1axxz7cdl91d4jq7yvc4r7mkc8wnmnzq2s9hf79p")))

(define-public crate-nobsign-0.2 (crate (name "nobsign") (vers "0.2.0") (deps (list (crate-dep (name "base64") (req "^0.10") (default-features #t) (kind 0)) (crate-dep (name "byteorder") (req "^1.2") (default-features #t) (kind 0)) (crate-dep (name "ring") (req "^0.14") (default-features #t) (kind 0)))) (hash "14mb845kyzfacfbkdb7i86vhyfl4r1slmhw7zjvfnfsqfai9k4m3")))

