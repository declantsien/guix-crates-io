(define-module (crates-io no se) #:use-module (crates-io))

(define-public crate-nosejob-1 (crate (name "nosejob") (vers "1.0.0") (hash "00av3al05gx5svk7is5kpgmqhc5r2bviisccn4pjdpmn52j0z851") (features (quote (("obj-iter") ("iter" "obj-iter" "arr-iter") ("full" "iter") ("default" "full") ("arr-iter")))) (yanked #t)))

