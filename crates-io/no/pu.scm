(define-module (crates-io no pu) #:use-module (crates-io))

(define-public crate-nopu-0.1 (crate (name "nopu") (vers "0.1.0") (deps (list (crate-dep (name "anyhow") (req "^1.0.66") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^4.0.18") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1") (features (quote ("full"))) (default-features #t) (kind 0)) (crate-dep (name "tokio-stream") (req "^0.1.11") (features (quote ("fs"))) (default-features #t) (kind 0)))) (hash "06zqj7d4684wfiqaqvxi6rb73ny16vgn5l8psmfv6fcp2g7krzib")))

