(define-module (crates-io no ot) #:use-module (crates-io))

(define-public crate-noot-0.1 (crate (name "noot") (vers "0.1.0") (deps (list (crate-dep (name "anyhow") (req "^1.0.82") (default-features #t) (kind 0)) (crate-dep (name "axum") (req "^0.7.5") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^4.5.4") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.37.0") (features (quote ("full"))) (default-features #t) (kind 0)) (crate-dep (name "tracing") (req "^0.1.40") (default-features #t) (kind 0)) (crate-dep (name "tracing-subscriber") (req "^0.3.18") (features (quote ("env-filter"))) (default-features #t) (kind 0)))) (hash "16va2pm9zy6scwcs5qnwyxnjpam7q5b7kbb0ki3p4c9nj1npg5wv")))

