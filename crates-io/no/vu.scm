(define-module (crates-io no vu) #:use-module (crates-io))

(define-public crate-novusk_syscalls-0.1 (crate (name "novusk_syscalls") (vers "0.1.0") (deps (list (crate-dep (name "register") (req "1.*.*") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "uefi") (req "^0.8.1") (default-features #t) (kind 0)))) (hash "1ysxj67xm481hws3ydm1a0ca47jhlmppznvch617ms66n1nbdy4k") (features (quote (("novusk_uefi" "register") ("baremetal_aarch64" "register"))))))

(define-public crate-novusk_syscalls-0.2 (crate (name "novusk_syscalls") (vers "0.2.0") (hash "0w8ikwnsbw59yb04msi7rp1kw84x4yppz7rk66q7whapmpnhrnzq")))

(define-public crate-novusk_syscalls-0.2 (crate (name "novusk_syscalls") (vers "0.2.1") (hash "0pgh15l4a5acnw1n6pf91hg6y3ml7nymi7qnl51s76n4pkdjfk1a")))

(define-public crate-novuskinc-0.1 (crate (name "novuskinc") (vers "0.1.0") (deps (list (crate-dep (name "cfg-if") (req "^1.0.0") (default-features #t) (kind 0)))) (hash "12rsb4mj2ahrp2c6qg8p6h5a9kjcvgw19x15v4gf8q9sx7hlxq4q")))

(define-public crate-novuskinc-0.1 (crate (name "novuskinc") (vers "0.1.1") (deps (list (crate-dep (name "cfg-if") (req "^1.0.0") (default-features #t) (kind 0)))) (hash "0lf8aqmclg24kyrbcmkrpxj3nqpymd3p51n9fj9x8mwmnww1pfz6")))

