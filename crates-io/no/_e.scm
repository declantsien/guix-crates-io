(define-module (crates-io no _e) #:use-module (crates-io))

(define-public crate-no_error-0.0.0 (crate (name "no_error") (vers "0.0.0") (deps (list (crate-dep (name "no_error_macro") (req "^0.0") (default-features #t) (kind 0)) (crate-dep (name "proc-macro-hack") (req "^0.5") (default-features #t) (kind 0)))) (hash "18xbv81ay956hd6h7mwznj9wh3ybznkxmydsrg5rynhmkzk4g2xb")))

(define-public crate-no_error-0.0.1 (crate (name "no_error") (vers "0.0.1") (deps (list (crate-dep (name "no_error_macro") (req "^0.0") (default-features #t) (kind 0)) (crate-dep (name "proc-macro-hack") (req "^0.5") (default-features #t) (kind 0)))) (hash "0f015c4vvsv99hywzcbmzm30hpbi0ghqhiw0197kyi9n5as5cfjm")))

(define-public crate-no_error-0.0.2 (crate (name "no_error") (vers "0.0.2") (deps (list (crate-dep (name "no_error_macro") (req "^0.0") (default-features #t) (kind 0)) (crate-dep (name "proc-macro-hack") (req "^0.5") (default-features #t) (kind 0)))) (hash "1dj5558rdzcc5wnin7qdrm63d9xk27cdp3xjw4amivmd4gypf01n")))

(define-public crate-no_error_macro-0.0.0 (crate (name "no_error_macro") (vers "0.0.0") (deps (list (crate-dep (name "proc-macro-hack") (req "^0.5") (default-features #t) (kind 0)))) (hash "0y7204208savnfpy341zh8r95k37cy1rrw9dv0vn20vxn5gx5ksh")))

(define-public crate-no_error_macro-0.0.1 (crate (name "no_error_macro") (vers "0.0.1") (deps (list (crate-dep (name "proc-macro-hack") (req "^0.5") (default-features #t) (kind 0)))) (hash "0hlfmbblrch0122j4r73phfzfyrg10z88jy3gcbsfzh1wa1vqswl")))

