(define-module (crates-io no bi) #:use-module (crates-io))

(define-public crate-nobility-0.1 (crate (name "nobility") (vers "0.1.0") (deps (list (crate-dep (name "byteorder") (req "*") (default-features #t) (kind 0)))) (hash "1d3miikb9vzsabs06hf400nqxik40shag4abpjdl95vlgd3m692g")))

(define-public crate-nobility-0.2 (crate (name "nobility") (vers "0.2.0") (deps (list (crate-dep (name "byteorder") (req "^1.3.4") (default-features #t) (kind 0)) (crate-dep (name "cesu8") (req "^1.1") (default-features #t) (kind 0)) (crate-dep (name "flate2") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "uuid") (req "^0.8.1") (optional #t) (default-features #t) (kind 0)))) (hash "1507cs0n6dbydjnnl0d0fyfyz05ld8q9dzsh261yxafma4ajmilh") (features (quote (("default" "uuid"))))))

