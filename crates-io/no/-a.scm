(define-module (crates-io no -a) #:use-module (crates-io))

(define-public crate-no-adjacent-0.1 (crate (name "no-adjacent") (vers "0.1.0") (hash "1f6zwgb50ywvrs2q0aay0n9v01jvbjfprajvqc11h9krkpbyy8hp")))

(define-public crate-no-adjacent-0.1 (crate (name "no-adjacent") (vers "0.1.1") (hash "0wmghq1i3kizldvws87kc5q3bdjy9fsd2l114dfcwrhrxbb6lhgy")))

(define-public crate-no-ansi-0.1 (crate (name "no-ansi") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^3") (default-features #t) (kind 0)) (crate-dep (name "strip-ansi-escapes") (req "^0.1.1") (default-features #t) (kind 0)))) (hash "0i9ci3c0k1caj4m0k324c8id10ldhzwmpmsq1nznivsrfkv6jzq7")))

