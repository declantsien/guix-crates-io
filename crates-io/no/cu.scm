(define-module (crates-io no cu) #:use-module (crates-io))

(define-public crate-nocurses_rust-0.1 (crate (name "nocurses_rust") (vers "0.1.0") (hash "17rjp3lyc87qnknrim5mq15jhrfxdm8lli5i0sjqb1ll20hkagpm")))

(define-public crate-nocurses_rust-0.2 (crate (name "nocurses_rust") (vers "0.2.0") (deps (list (crate-dep (name "crossterm") (req "^0.26") (features (quote ("filedescriptor"))) (default-features #t) (kind 0)))) (hash "10vj55n3hkl096wc29p4r7im4h516winyazy4l88da69vmdlcin0")))

(define-public crate-nocurses_rust-0.2 (crate (name "nocurses_rust") (vers "0.2.1") (deps (list (crate-dep (name "crossterm") (req "^0.26") (features (quote ("filedescriptor"))) (default-features #t) (kind 0)))) (hash "1v4qxzjh0cfsbdly30ydib52f6yydvikxm18hnqpdh22s3sqk2xp")))

