(define-module (crates-io no ak) #:use-module (crates-io))

(define-public crate-noak-0.1 (crate (name "noak") (vers "0.1.0") (deps (list (crate-dep (name "bitflags") (req "^1.0") (default-features #t) (kind 0)))) (hash "0blyy85cn5my9cn390k2l2k562f23jy6aavd5r176d4vhs69z78r")))

(define-public crate-noak-0.2 (crate (name "noak") (vers "0.2.0") (deps (list (crate-dep (name "bitflags") (req "^1.1.0") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2.8") (default-features #t) (kind 0)))) (hash "1gpfaljpjv62zs3y5f59xnfd0nc3rp62gmbzj5541nwbgs6bmal9")))

(define-public crate-noak-0.2 (crate (name "noak") (vers "0.2.1") (deps (list (crate-dep (name "bitflags") (req "^1.1.0") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2.8") (default-features #t) (kind 0)))) (hash "1cbm8240ippk7xw93hc6in85mjqx06aiqj2yz4mdynpn2p0vgy8f")))

(define-public crate-noak-0.3 (crate (name "noak") (vers "0.3.0") (deps (list (crate-dep (name "bitflags") (req "^1.1.0") (default-features #t) (kind 0)) (crate-dep (name "indexmap") (req "^1.1.0") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2.8") (default-features #t) (kind 0)))) (hash "14ac53hs3kl6n5azgkfcpayisrw688fz8aaxw48x8kvdxzbr8zbc")))

(define-public crate-noak-0.4 (crate (name "noak") (vers "0.4.0-alpha.1") (deps (list (crate-dep (name "bitflags") (req "^1.2.0") (default-features #t) (kind 0)) (crate-dep (name "indexmap") (req "^1.2.0") (default-features #t) (kind 0)))) (hash "18ssgvs1jvxs6cmkhhfqk243ad2z7nakk72sasblfi6g22cjy71r")))

(define-public crate-noak-0.4 (crate (name "noak") (vers "0.4.0-alpha.2") (deps (list (crate-dep (name "bitflags") (req "^1.2.0") (default-features #t) (kind 0)) (crate-dep (name "indexmap") (req "^1.2.0") (default-features #t) (kind 0)))) (hash "1ak57znp0sx1qnkjl3cin1yb21d02jwfg57cx6azxks10isrka1k")))

(define-public crate-noak-0.4 (crate (name "noak") (vers "0.4.0-alpha.3") (deps (list (crate-dep (name "bitflags") (req "^1.2.1") (default-features #t) (kind 0)) (crate-dep (name "indexmap") (req "^1.3.2") (default-features #t) (kind 0)))) (hash "1a5lclxmrls1xyhc2kn5ggass0s3w5bvzs0hfdmmr1ih0lin7hfh")))

(define-public crate-noak-0.4 (crate (name "noak") (vers "0.4.0") (deps (list (crate-dep (name "bitflags") (req "^1.3.2") (default-features #t) (kind 0)) (crate-dep (name "indexmap") (req "^1.8.1") (default-features #t) (kind 0)))) (hash "156zd37g80h6w3rznns3aklly7rc4rzjhpr2rmhb1vf2jpjkw0nk")))

(define-public crate-noak-0.5 (crate (name "noak") (vers "0.5.0") (deps (list (crate-dep (name "bitflags") (req "^1.3.2") (default-features #t) (kind 0)) (crate-dep (name "indexmap") (req "^1.8.1") (default-features #t) (kind 0)))) (hash "0gdn5kmqgvmqklmq7367d1iln3nssm95wz2kl4sdh57xy349n3kf")))

(define-public crate-noak-0.6 (crate (name "noak") (vers "0.6.0") (deps (list (crate-dep (name "bitflags") (req "^1.3.2") (default-features #t) (kind 0)) (crate-dep (name "indexmap") (req "^1.8.2") (default-features #t) (kind 0)))) (hash "0v6q19sanbm5qvjrm252ywwvdxi9bg5ajhx65rvnsdd9k6xr52xg")))

