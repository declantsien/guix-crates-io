(define-module (crates-io no gp) #:use-module (crates-io))

(define-public crate-nogpt-0.1 (crate (name "nogpt") (vers "0.1.0-pre1") (deps (list (crate-dep (name "bitflags") (req "^1.3") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "block_device") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "crc") (req "^1.8") (kind 0)) (crate-dep (name "err-derive") (req "^0.3") (kind 0)) (crate-dep (name "nom") (req "^6.1") (default-features #t) (kind 2)))) (hash "1ynwpg2pz4qz1lgvg2bwfv80m0vczwgmcybarcqqnzf8pfk4irjq") (features (quote (("std" "alloc" "err-derive/std") ("default" "bitflags") ("alloc"))))))

