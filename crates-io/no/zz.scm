(define-module (crates-io no zz) #:use-module (crates-io))

(define-public crate-nozzle-bindings-1 (crate (name "nozzle-bindings") (vers "1.0.0+1.90.632-b035") (deps (list (crate-dep (name "bitflags") (req "^1.2.1") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2.93") (default-features #t) (kind 0)) (crate-dep (name "os_socketaddr") (req "^0.2.0") (default-features #t) (kind 0)))) (hash "03s08li63slqpn7qv11bnjq8xd313lnakvlzhpsz5y9xdda4k9nw")))

