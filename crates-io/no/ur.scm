(define-module (crates-io no ur) #:use-module (crates-io))

(define-public crate-nourl-0.1 (crate (name "nourl") (vers "0.1.0") (hash "0h6x43ms37l7xwm441fvbi7in30xdjf025l56598p913l0ld0igs")))

(define-public crate-nourl-0.1 (crate (name "nourl") (vers "0.1.1") (deps (list (crate-dep (name "defmt") (req "^0.3") (optional #t) (default-features #t) (kind 0)))) (hash "0sajfbvka39v3id8f1shic3hgb0qzn9vvlw13n81dzk5vbgjxhdk")))

