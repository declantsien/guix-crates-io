(define-module (crates-io no ck) #:use-module (crates-io))

(define-public crate-nock-0.0.0 (crate (name "nock") (vers "0.0.0") (hash "0faycwb04jmjc81p1syhjdmx9jli8zyhcgckqzi2v514cga5mg8f")))

(define-public crate-nock-0.1 (crate (name "nock") (vers "0.1.0") (hash "00s415nkwq12fvk1m6d7ganq9dwi2kvzm31c09akjgf60z608rqy")))

(define-public crate-nock-0.2 (crate (name "nock") (vers "0.2.0") (deps (list (crate-dep (name "num") (req "^0.1") (default-features #t) (kind 0)))) (hash "1bfk7jpp41xjhl551l1cb0zcz26hlmjffkaxcqx02z811p2ri43q")))

(define-public crate-nock-0.2 (crate (name "nock") (vers "0.2.1") (deps (list (crate-dep (name "num") (req "^0.1") (default-features #t) (kind 0)))) (hash "0p9sccagq9xlfqkawz46aqc1yy4dfkm8klxxdmmm1sqyq0nz2q1s")))

(define-public crate-nock-0.3 (crate (name "nock") (vers "0.3.0") (deps (list (crate-dep (name "fnv") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "num") (req "^0.1") (default-features #t) (kind 0)))) (hash "15zq3i8ihxgvf943ha892hgn5m3ah43x4hqq3lv218qg1j5skw4h")))

(define-public crate-nock-0.4 (crate (name "nock") (vers "0.4.0") (deps (list (crate-dep (name "fnv") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "num") (req "^0.1") (default-features #t) (kind 0)))) (hash "17w4681lq5krxip4f0j5shy13m4ckp7sj4187v4y9s72ni2infcj")))

