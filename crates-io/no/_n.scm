(define-module (crates-io no _n) #:use-module (crates-io))

(define-public crate-no_need_sudo-0.1 (crate (name "no_need_sudo") (vers "0.1.0") (hash "1h1v4crqkj01zqhz11s30fbdggnkwh710z6sr4ddi8p9mi1wk6aw")))

(define-public crate-no_need_sudo-0.1 (crate (name "no_need_sudo") (vers "0.1.1") (hash "0mwxqmmfw5h63fzkx6z34k91xamr4aw1s44x6n984yrbs0pf6153")))

