(define-module (crates-io no mt) #:use-module (crates-io))

(define-public crate-nomt-0.1 (crate (name "nomt") (vers "0.1.0") (hash "1q7q7wcbdz6s0pzj6bcn8mzj94b3m9k24azz8rbcgxhbq5n0ifsk")))

(define-public crate-nomt-backend-rocksdb-0.1 (crate (name "nomt-backend-rocksdb") (vers "0.1.0") (hash "1km1b5la08qjf9x15l4l8vfkl50kibhmim760yp0mw6am13xfl6y")))

(define-public crate-nomt-backend-ssd-map-0.1 (crate (name "nomt-backend-ssd-map") (vers "0.1.0") (hash "1cb9im2xnli4iik21lp12vgainr02z20gl473yx76pas9amdk8xh")))

(define-public crate-nomt-core-0.1 (crate (name "nomt-core") (vers "0.1.0") (hash "1naxkqamf22hn5z9qahflk2ial4089p931p7sacf9l50f117m2rw")))

