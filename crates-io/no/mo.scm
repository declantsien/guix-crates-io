(define-module (crates-io no mo) #:use-module (crates-io))

(define-public crate-nomore-0.1 (crate (name "nomore") (vers "0.1.0") (deps (list (crate-dep (name "fastrand") (req "^1.7.0") (default-features #t) (kind 0)))) (hash "1qr24rys5fjg9gcl1f0i1yrid0vl117wrrfjil8kbkcabmh78raw") (yanked #t)))

(define-public crate-nomore-0.1 (crate (name "nomore") (vers "0.1.2") (deps (list (crate-dep (name "fastrand") (req "^1.7.0") (default-features #t) (kind 0)))) (hash "1szx153bdd1sfhiw6jwg6dnxx7kvg9f4jbyp84d9kwqr3bgj97dv") (yanked #t)))

(define-public crate-nomore-0.1 (crate (name "nomore") (vers "0.1.3") (deps (list (crate-dep (name "fastrand") (req "^1.7.0") (default-features #t) (kind 0)))) (hash "18f08lkgakww134bx9pc5rf3smjfr6gk13wi4ljgm09dm5wqhl09")))

(define-public crate-nomore-0.1 (crate (name "nomore") (vers "0.1.4") (deps (list (crate-dep (name "fastrand") (req "^1.7.0") (default-features #t) (kind 0)))) (hash "0c92gdm4rrazld5j5vvxrqix2cmid5f3p5aq9l92k520k2900bdl")))

(define-public crate-nomore-0.2 (crate (name "nomore") (vers "0.2.0") (deps (list (crate-dep (name "fastrand") (req "^1.7.0") (default-features #t) (kind 0)))) (hash "0ymdq431v5mqw96aybgi83i5kr5qqj3q9fyl54mhwms31jm28qbg") (yanked #t)))

(define-public crate-nomore-0.2 (crate (name "nomore") (vers "0.2.1") (deps (list (crate-dep (name "fastrand") (req "^1.7.0") (default-features #t) (kind 0)))) (hash "1z6k9dgfmwziln11d7c7wyc1sr3pbx2d8cnag61rryxgxzm5cy1q")))

(define-public crate-nomos-0.1 (crate (name "nomos") (vers "0.1.0") (deps (list (crate-dep (name "bincode") (req "^1.2.0") (default-features #t) (kind 0)) (crate-dep (name "byteorder") (req "^1.3.2") (default-features #t) (kind 0)) (crate-dep (name "nomos-runtime") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "wasmer-runtime") (req "^0.5.7") (features (quote ("singlepass"))) (default-features #t) (kind 0)))) (hash "0l75zg9lrq1k4h10a5vd407j8vckx430kxl648djwc88f3grqffp")))

(define-public crate-nomos-runtime-0.1 (crate (name "nomos-runtime") (vers "0.1.0") (deps (list (crate-dep (name "bincode") (req "^1.2.0") (default-features #t) (kind 0)) (crate-dep (name "byteorder") (req "^1.3.2") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "03bvb41q0i2f84dabnchi8q1z22ik5z80jmjsll5nvpml9ck2wib")))

