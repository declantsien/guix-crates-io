(define-module (crates-io no ni) #:use-module (crates-io))

(define-public crate-nonicle-0.0.0 (crate (name "nonicle") (vers "0.0.0") (hash "1z305zxqyfm1hyz5p9jn5cb8y9imqh7rgpcg6cny4ny43jh4s4m6") (features (quote (("std") ("default" "std"))))))

(define-public crate-nonicle-0.0.1 (crate (name "nonicle") (vers "0.0.1") (hash "0hs4n0vfw34rbwzvakvmqrfcywhnbc6c1lm7y26pzixvc8xqxy24") (features (quote (("std") ("default" "std"))))))

(define-public crate-nonicle-0.1 (crate (name "nonicle") (vers "0.1.0") (hash "1kbacmpvg6awk087vpbkvgb3aw0kz5cqpry55f7famda8lklhx91") (features (quote (("std") ("default" "std"))))))

(define-public crate-nonicle-0.1 (crate (name "nonicle") (vers "0.1.1") (hash "16wp63rlv2smvssyswrbb13pa87zwmp11nv722npn3ryrq0d6n0k") (features (quote (("std") ("default" "std"))))))

