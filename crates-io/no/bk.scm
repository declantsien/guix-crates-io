(define-module (crates-io no bk) #:use-module (crates-io))

(define-public crate-nobkz-bicycle-book-wordcount-0.1 (crate (name "nobkz-bicycle-book-wordcount") (vers "0.1.0") (deps (list (crate-dep (name "regex") (req "^1.0") (default-features #t) (kind 0)))) (hash "0n72pbjc3kgyvvdcfpx4647p26ca68m3c3smi8apmzlrgpp4arpm")))

