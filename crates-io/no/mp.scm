(define-module (crates-io no mp) #:use-module (crates-io))

(define-public crate-nompdf-0.0.1 (crate (name "nompdf") (vers "0.0.1") (deps (list (crate-dep (name "matches") (req "^0.1.9") (default-features #t) (kind 2)) (crate-dep (name "nom") (req "^7.1.0") (default-features #t) (kind 0)) (crate-dep (name "pretty_assertions") (req "^1.1.0") (default-features #t) (kind 2)))) (hash "03c0cd8766201l3rgsi2729lv4ykhsvfn1n5l7pqch6vx3ikarn2")))

