(define-module (crates-io no n-) #:use-module (crates-io))

(define-public crate-non-blank-string-rs-1 (crate (name "non-blank-string-rs") (vers "1.0.1") (deps (list (crate-dep (name "fake") (req "^2") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.82") (default-features #t) (kind 2)))) (hash "0hcfnccm9ihjgr55qa9mgplnmw6a0scshxpph32r4svm5ab8h4ad") (features (quote (("utils") ("default"))))))

(define-public crate-non-blank-string-rs-1 (crate (name "non-blank-string-rs") (vers "1.0.2") (deps (list (crate-dep (name "fake") (req "^2") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.82") (default-features #t) (kind 2)))) (hash "0qanibnnpm5nh6f33sqgyzw8kzn6lin12bixbw2yqx4jxz94sfzc") (features (quote (("utils") ("default"))))))

(define-public crate-non-blank-string-rs-1 (crate (name "non-blank-string-rs") (vers "1.0.3") (deps (list (crate-dep (name "fake") (req "^2.6.1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "fake") (req "^2.6.1") (default-features #t) (kind 2)) (crate-dep (name "serde") (req "^1.0.178") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.104") (default-features #t) (kind 2)))) (hash "16rbsy27d84lhkjcz5bq4xrixnin0d6myyriq3m1y8la7327bhkf") (features (quote (("default")))) (v 2) (features2 (quote (("utils" "dep:fake"))))))

(define-public crate-non-dominated-sort-0.1 (crate (name "non-dominated-sort") (vers "0.1.0") (hash "0w89wj44nvazl0kfpr8bgqyhvq4nxa20l98d376mk1vj3ljg8ai3")))

(define-public crate-non-dominated-sort-0.2 (crate (name "non-dominated-sort") (vers "0.2.0") (hash "0bsqn400hsfn095in3hgxzli03f49flngi3kh433rkxrfya3q1x0")))

(define-public crate-non-dominated-sort-0.3 (crate (name "non-dominated-sort") (vers "0.3.0") (deps (list (crate-dep (name "criterion") (req "^0.2") (default-features #t) (kind 2)) (crate-dep (name "dominance-ord") (req "^0.1") (default-features #t) (kind 0)))) (hash "0sc18iyakpyydbv0cpmpx606a8xz332h8y9v6zmxvl9c2ffsj87h")))

(define-public crate-non-dominated-sort-0.3 (crate (name "non-dominated-sort") (vers "0.3.1") (deps (list (crate-dep (name "criterion") (req "^0.2") (default-features #t) (kind 2)) (crate-dep (name "dominance-ord") (req "^0.1") (default-features #t) (kind 0)))) (hash "1cn28lz1hk6w8qkqpzbgyszi1pcl7vpg1i3d25ciwv9q0lxfjkdw")))

(define-public crate-non-empty-0.1 (crate (name "non-empty") (vers "0.1.0") (hash "0bnvbdaj09vzzn3js4xdvcpfshj762cqksxxznnsbkacdircrw2q")))

(define-public crate-non-empty-collections-0.1 (crate (name "non-empty-collections") (vers "0.1.0") (deps (list (crate-dep (name "indexmap") (req "^1.0") (default-features #t) (kind 0)))) (hash "1cp0d2wwqna34s9iqvbhakvs97b1w4c0fw6g721hbicqmdyzkcvj")))

(define-public crate-non-empty-collections-0.1 (crate (name "non-empty-collections") (vers "0.1.1") (deps (list (crate-dep (name "indexmap") (req "^1.0") (default-features #t) (kind 0)))) (hash "10pdy9yzrdxjcxhhvd7sc45v8ihaap59znkrs8z6fqfwc4x5nl3v") (yanked #t)))

(define-public crate-non-empty-collections-0.1 (crate (name "non-empty-collections") (vers "0.1.2") (deps (list (crate-dep (name "indexmap") (req "^1.0") (default-features #t) (kind 0)))) (hash "15j0mjpr9q5lrj7ybgjkaxfhzb0gxs48w5vk6cxhkvmlb1lz403k")))

(define-public crate-non-empty-collections-0.1 (crate (name "non-empty-collections") (vers "0.1.3") (deps (list (crate-dep (name "indexmap") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (optional #t) (default-features #t) (kind 0)))) (hash "0c1ddqrf8f20z1k14lkh5844x8k9gzbnbi1f0v7l95wicd8177g6") (features (quote (("serde_support" "serde") ("default" "serde_support"))))))

(define-public crate-non-empty-collections-0.1 (crate (name "non-empty-collections") (vers "0.1.5") (deps (list (crate-dep (name "indexmap") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (optional #t) (default-features #t) (kind 0)))) (hash "0070xy6v17vi5pb4ll4bjwqrlbihzcvsdivrj956b8da7im3dkdf") (features (quote (("serde_support" "serde") ("default" "serde_support"))))))

(define-public crate-non-empty-collections-0.1 (crate (name "non-empty-collections") (vers "0.1.6") (deps (list (crate-dep (name "indexmap") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (optional #t) (default-features #t) (kind 0)))) (hash "1k7hich5gv32qva176s0r97wcv9q7rbbmfqp40gps4dv3n6wvhhh") (features (quote (("serde_support" "serde") ("default" "serde_support"))))))

(define-public crate-non-empty-collections-0.1 (crate (name "non-empty-collections") (vers "0.1.7") (deps (list (crate-dep (name "indexmap") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (optional #t) (default-features #t) (kind 0)))) (hash "0w4scrri6ly6vhjscxqss69i1i9yfv8ah6qnpxxf7qxjx2a3qs4m") (features (quote (("serde_support" "serde") ("default" "serde_support"))))))

(define-public crate-non-empty-collections-0.1 (crate (name "non-empty-collections") (vers "0.1.8") (deps (list (crate-dep (name "indexmap") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (optional #t) (default-features #t) (kind 0)))) (hash "0gar1x6js3jb6d7hm3fdx67abdfs8j9k9434h6dli8fhmyy690y9") (features (quote (("serde_support" "serde") ("default" "serde_support"))))))

(define-public crate-non-empty-collections-0.1 (crate (name "non-empty-collections") (vers "0.1.9") (deps (list (crate-dep (name "indexmap") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1") (optional #t) (default-features #t) (kind 0)))) (hash "11b4rr4xy5k2mh5wqk3cal9dg6qdnhf1bbjmvjsiznvlrhybahjw") (features (quote (("serde_support" "serde") ("default" "serde_support"))))))

(define-public crate-non-empty-string-0.1 (crate (name "non-empty-string") (vers "0.1.0") (deps (list (crate-dep (name "assert_matches") (req "^1.5.0") (default-features #t) (kind 2)))) (hash "07cjifrasmgyq2610vr18zy29qyd46dvlkgwl5hcnils12fj4m82")))

(define-public crate-non-empty-string-0.2 (crate (name "non-empty-string") (vers "0.2.0") (deps (list (crate-dep (name "assert_matches") (req "^1.5.0") (default-features #t) (kind 2)) (crate-dep (name "serde") (req "^1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1") (default-features #t) (kind 2)))) (hash "0ws48img7lr1mb5db1wfghs7fywg1ij8y1yhhn5ghs9pw6gji5rd") (features (quote (("default")))) (v 2) (features2 (quote (("serde" "dep:serde"))))))

(define-public crate-non-empty-string-0.2 (crate (name "non-empty-string") (vers "0.2.1") (deps (list (crate-dep (name "assert_matches") (req "^1.5.0") (default-features #t) (kind 2)) (crate-dep (name "serde") (req "^1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1") (default-features #t) (kind 2)))) (hash "1rp1ff43023y647q1cxc04zia1dacv28zw6mj6m1fnff5iwmvckr") (features (quote (("default")))) (v 2) (features2 (quote (("serde" "dep:serde"))))))

(define-public crate-non-empty-string-0.2 (crate (name "non-empty-string") (vers "0.2.2") (deps (list (crate-dep (name "assert_matches") (req "^1.5.0") (default-features #t) (kind 2)) (crate-dep (name "delegate") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1") (features (quote ("derive"))) (default-features #t) (kind 2)) (crate-dep (name "serde_json") (req "^1") (default-features #t) (kind 2)))) (hash "1gdak4awvm26li80ks9y926qizw0w10w9nnm90lpfflp8dzdacan") (features (quote (("default")))) (v 2) (features2 (quote (("serde" "dep:serde"))))))

(define-public crate-non-empty-string-0.2 (crate (name "non-empty-string") (vers "0.2.3") (deps (list (crate-dep (name "assert_matches") (req "^1.5.0") (default-features #t) (kind 2)) (crate-dep (name "delegate") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1") (features (quote ("derive"))) (default-features #t) (kind 2)) (crate-dep (name "serde_json") (req "^1") (default-features #t) (kind 2)))) (hash "0yparanfxa87yxlhhnm4mvx584w21l2wwrrzyv6k5jqqnhg8yhxv") (features (quote (("default")))) (v 2) (features2 (quote (("serde" "dep:serde"))))))

(define-public crate-non-empty-string-0.2 (crate (name "non-empty-string") (vers "0.2.4") (deps (list (crate-dep (name "assert_matches") (req "^1.5.0") (default-features #t) (kind 2)) (crate-dep (name "delegate") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1") (features (quote ("derive"))) (default-features #t) (kind 2)) (crate-dep (name "serde_json") (req "^1") (default-features #t) (kind 2)))) (hash "0cxvjibhv3sjdamacj56b10mcl0iralkv18ra98awig3c100zksm") (features (quote (("default")))) (v 2) (features2 (quote (("serde" "dep:serde"))))))

(define-public crate-non-empty-vec-0.1 (crate (name "non-empty-vec") (vers "0.1.0") (hash "11b0s9zxwqimrxglm80lkk1911qdqm3jzj5l6pzmdg1jwkws7w8v")))

(define-public crate-non-empty-vec-0.1 (crate (name "non-empty-vec") (vers "0.1.1") (hash "1avlz1dri5c7zkswzi4szsd7s11b2svp9hydwnnyxlq1q4q2irw6")))

(define-public crate-non-empty-vec-0.1 (crate (name "non-empty-vec") (vers "0.1.2") (hash "1qzzh3vzfdhwv1491m7x80pz60qkwazcvd2xx5jfcym4hna1vgzr")))

(define-public crate-non-empty-vec-0.2 (crate (name "non-empty-vec") (vers "0.2.0") (deps (list (crate-dep (name "serde") (req "^1.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 2)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 2)))) (hash "10m8v63ym4glir5ly61jskk6mz6yai4xk0w2szclh03w1yscj4g4")))

(define-public crate-non-empty-vec-0.2 (crate (name "non-empty-vec") (vers "0.2.1") (deps (list (crate-dep (name "serde") (req "^1.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 2)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 2)))) (hash "1xnjcz7q3w3d9ib9jdmf4sixhyirh36hgwq11grb37dg6zvsbryd")))

(define-public crate-non-empty-vec-0.2 (crate (name "non-empty-vec") (vers "0.2.2") (deps (list (crate-dep (name "serde") (req "^1.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 2)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 2)))) (hash "0b6946xpdbdki4h7b0nm35kplz9csmxf3h8i73yqcmdvar884csx")))

(define-public crate-non-empty-vec-0.2 (crate (name "non-empty-vec") (vers "0.2.3") (deps (list (crate-dep (name "serde") (req "^1.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 2)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 2)))) (hash "12zc4vmgvv9lkn4c1rmvd2s8ifsn4vmig3xcxvf3riflm2dadsyf")))

(define-public crate-non-exhaustive-0.1 (crate (name "non-exhaustive") (vers "0.1.0") (hash "02axw2gk96gmzwlaqwwihfd6lb85bay8zar72l78pc3lpd306j3f")))

(define-public crate-non-exhaustive-0.1 (crate (name "non-exhaustive") (vers "0.1.1") (hash "1zw13252rn85nzg1xkv4pfn6nzyhnc93a7r73yq6z6ckp49lxvns")))

(define-public crate-non-zero-byte-slice-0.1 (crate (name "non-zero-byte-slice") (vers "0.1.0") (deps (list (crate-dep (name "serde") (req "^1.0.103") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "0940wdjqvdwfh04ppms465aghrl0dn9x1g4125fz178wl7da3nl9")))

