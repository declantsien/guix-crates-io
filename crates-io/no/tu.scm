(define-module (crates-io no tu) #:use-module (crates-io))

(define-public crate-notugly-0.2 (crate (name "notugly") (vers "0.2.0") (hash "025qnvxbj8p94l3jc0f3ndkh9gj9yf808kprgafzqf2gwhdcx228")))

(define-public crate-notugly-0.2 (crate (name "notugly") (vers "0.2.1") (hash "1b1blpc3vfv1nm2q8rydkkyj6pnwsdpm1nkrp4hrzws9qfbywiyh")))

(define-public crate-notugly-0.2 (crate (name "notugly") (vers "0.2.2") (hash "1gw2lvm7bgsb96b56fjxv05xbgz9fh64i53xx2f2icf3px0q01bk")))

(define-public crate-notus-mempool-0.1 (crate (name "notus-mempool") (vers "0.1.1") (deps (list (crate-dep (name "bincode") (req "^1.3.3") (default-features #t) (kind 0)) (crate-dep (name "env_logger") (req "^0.10.2") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.21") (default-features #t) (kind 0)) (crate-dep (name "md5") (req "^0.7.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.197") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "0shjm95l12x2y1kqazrkf5whkcyymlm7phvc9jv79wc6ybxjxhvz")))

