(define-module (crates-io no nz) #:use-module (crates-io))

(define-public crate-nonzero-0.1 (crate (name "nonzero") (vers "0.1.0") (deps (list (crate-dep (name "num-traits") (req "^0.2.14") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1.0.28") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.9") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.74") (default-features #t) (kind 0)))) (hash "0lh0l77a7k2xaqwlgvaxdzc3w1cnyamxvpmgq1znkb01y0ywi0k9")))

(define-public crate-nonzero-0.2 (crate (name "nonzero") (vers "0.2.0") (deps (list (crate-dep (name "num-traits") (req "^0.2.16") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1.0.66") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.33") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2.0.29") (default-features #t) (kind 0)))) (hash "0zhj60rz5yycrklv5h6ji80vhzmivcbqxjfaaivky2lkcv6rm6qd")))

(define-public crate-nonzero-const-param-0.0.1 (crate (name "nonzero-const-param") (vers "0.0.1-alpha.1") (deps (list (crate-dep (name "serde") (req "^1") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "zerocopy") (req "^0.7") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)))) (hash "0k9p0bn1hhhb87bfl72hlc4d3d12j4mrcyfrjg1qjjwg92k82yzc")))

(define-public crate-nonzero_ext-0.0.1 (crate (name "nonzero_ext") (vers "0.0.1") (hash "0g8dxv3xag5nz497yxjj976yzdpqws6nd419vw74qb059fjdi1bz")))

(define-public crate-nonzero_ext-0.0.2 (crate (name "nonzero_ext") (vers "0.0.2") (hash "0qzj4khacch9gw1x1r74njkxl7gk857vfxnkqf0q7lf9309jmz53")))

(define-public crate-nonzero_ext-0.1 (crate (name "nonzero_ext") (vers "0.1.0") (hash "164vshfpfs946mw57ywmypk9b0vv6ncz6hl1xggsh63840nfv36z")))

(define-public crate-nonzero_ext-0.1 (crate (name "nonzero_ext") (vers "0.1.1") (hash "1n20q215p46chfy9jqmlika05vijzdb923wma0wdkj901lclnkrj")))

(define-public crate-nonzero_ext-0.1 (crate (name "nonzero_ext") (vers "0.1.2") (deps (list (crate-dep (name "compiletest_rs") (req "^0.3.14") (features (quote ("tmp"))) (optional #t) (default-features #t) (kind 0)))) (hash "1x73wr2ndnxq5szrviwjcfl15gzsq2xhhjc1hmda9a85c7l6c349") (features (quote (("stable" "compiletest_rs/stable") ("nightly" "compiletest_rs") ("default") ("beta" "compiletest_rs/stable"))))))

(define-public crate-nonzero_ext-0.1 (crate (name "nonzero_ext") (vers "0.1.3") (deps (list (crate-dep (name "compiletest_rs") (req "^0.3.18") (features (quote ("tmp"))) (optional #t) (default-features #t) (kind 0)))) (hash "01yq7094c2f3i5asan9ya9m0vpapka8lc9d23smpj2gz2nqsiz37") (features (quote (("std") ("stable" "compiletest_rs/stable") ("nightly" "compiletest_rs") ("default" "std") ("beta" "compiletest_rs/stable")))) (yanked #t)))

(define-public crate-nonzero_ext-0.1 (crate (name "nonzero_ext") (vers "0.1.4") (deps (list (crate-dep (name "compiletest_rs") (req "^0.3.19") (features (quote ("tmp"))) (optional #t) (default-features #t) (kind 0)))) (hash "14i1lzh561y4k9i2gsmh3jpjdnnnd2fbcfgvwzs9nb8zbrzpfxh5") (features (quote (("std") ("stable" "compiletest_rs/stable") ("nightly" "compiletest_rs") ("default" "std") ("beta" "compiletest_rs/stable")))) (yanked #t)))

(define-public crate-nonzero_ext-0.1 (crate (name "nonzero_ext") (vers "0.1.5") (deps (list (crate-dep (name "compiletest_rs") (req "^0.3.19") (features (quote ("tmp"))) (optional #t) (default-features #t) (kind 0)))) (hash "1sd85jx8r8xk67r1jx08vi0clk6qsjp14r50wgk7n81bjdil26yv") (features (quote (("std") ("stable" "compiletest_rs/stable") ("nightly" "compiletest_rs") ("default" "std") ("beta" "compiletest_rs/stable"))))))

(define-public crate-nonzero_ext-0.2 (crate (name "nonzero_ext") (vers "0.2.0") (deps (list (crate-dep (name "trybuild") (req "^1.0") (default-features #t) (kind 2)))) (hash "0i7lhlzfv62756q2zwsqiqr3wdh37yy0rbv0x9ivmfpak43jk8a4") (features (quote (("std") ("default" "std"))))))

(define-public crate-nonzero_ext-0.3 (crate (name "nonzero_ext") (vers "0.3.0") (deps (list (crate-dep (name "rustversion") (req "^1.0.2") (default-features #t) (kind 2)) (crate-dep (name "trybuild") (req "^1.0.42") (default-features #t) (kind 2)))) (hash "08fghyinb07xwhbj7vwvlhg45g5cvhvld2min25njidir12rdgrq") (features (quote (("std") ("default" "std"))))))

(define-public crate-nonzero_lit-0.1 (crate (name "nonzero_lit") (vers "0.1.0") (hash "09hqr8ywlbzq1j9gc69cxk9w5q0xv7w5qybjd53jkvfr6q5ww69r")))

(define-public crate-nonzero_lit-0.1 (crate (name "nonzero_lit") (vers "0.1.1") (hash "1l93snp090diwzf2qrir6x8q7a33spipp96fhgxrvvxshn454wf9")))

(define-public crate-nonzero_lit-0.1 (crate (name "nonzero_lit") (vers "0.1.2") (hash "1yh8nggqlpb3glaykxrryla1qg2jdj6jc63j0qywxf29b0vf1a9h")))

(define-public crate-nonzero_signed-1 (crate (name "nonzero_signed") (vers "1.0.0") (hash "1vvi7b9v2kw8k3lb7awkq1dc4in9ijwimzacn9s7x1nfychlydif") (yanked #t)))

(define-public crate-nonzero_signed-1 (crate (name "nonzero_signed") (vers "1.0.1") (hash "0kmrcl2zjnww2z7x83yz8dzn9w9xnvrbvk48sq695a4v33xzvp89") (yanked #t)))

(define-public crate-nonzero_signed-1 (crate (name "nonzero_signed") (vers "1.0.2") (hash "1phq7swdqwkkjqq711asqk7ypdwffdcqvk7f82fbz2m3kd2kr0fr") (yanked #t)))

(define-public crate-nonzero_signed-1 (crate (name "nonzero_signed") (vers "1.0.3") (hash "1n1pdqzfzj8a8yh1s16sjrk5jjj5c3xvh4albwzhsfrkh823ly02")))

(define-public crate-nonzero_signed-1 (crate (name "nonzero_signed") (vers "1.0.4") (deps (list (crate-dep (name "rustversion") (req "^1.0.17") (default-features #t) (kind 0)))) (hash "1h54bw67rdcxg6niagbxkj7kr715sbpaf1mphhbcx3a1j6r7apqi")))

