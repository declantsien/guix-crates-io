(define-module (crates-io no mm) #:use-module (crates-io))

(define-public crate-nommy-0.1 (crate (name "nommy") (vers "0.1.0") (deps (list (crate-dep (name "nommy_derive") (req "=0.1.0") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0") (default-features #t) (kind 0)))) (hash "0h72crmyp4ks0q33z6x9dyx18k15rjml6vhjm5557djhkp0f1xha")))

(define-public crate-nommy-0.1 (crate (name "nommy") (vers "0.1.1") (deps (list (crate-dep (name "nommy_derive") (req "=0.1.0") (default-features #t) (kind 0)))) (hash "01i8a15lqp5ajiilhyddqfjwfx0sag9p05xvq4xnqllzq7j2sk32")))

(define-public crate-nommy-0.1 (crate (name "nommy") (vers "0.1.2") (deps (list (crate-dep (name "nommy_derive") (req "=0.1.1") (default-features #t) (kind 0)))) (hash "1s0nb5g2if0i03p3qj62k54xfi0a27m6r72acmbzrihjbgh2ndm5")))

(define-public crate-nommy-0.1 (crate (name "nommy") (vers "0.1.3") (deps (list (crate-dep (name "nommy_derive") (req "=0.1.1") (default-features #t) (kind 0)))) (hash "1z8zpnf95gi8j6c2z9i264vdvazvd6h6wgnc6x43w4z38criqf6y")))

(define-public crate-nommy-0.1 (crate (name "nommy") (vers "0.1.4") (deps (list (crate-dep (name "nommy_derive") (req "=0.1.1") (default-features #t) (kind 0)))) (hash "0nqx14s1zqwxvp6dp0k6mp5yg03p8n38p30fvgyz09m72b76hvlm")))

(define-public crate-nommy-0.2 (crate (name "nommy") (vers "0.2.0") (deps (list (crate-dep (name "eyre") (req "^0.6") (default-features #t) (kind 0)) (crate-dep (name "nommy_derive") (req "=0.2.0") (default-features #t) (kind 0)))) (hash "1wm1z3a7kzcy0hghj72j2wfzvaiqmwzlvwqdqyrgn0z343cjc1z4")))

(define-public crate-nommy-0.2 (crate (name "nommy") (vers "0.2.1") (deps (list (crate-dep (name "eyre") (req "^0.6") (default-features #t) (kind 0)) (crate-dep (name "nommy_derive") (req "=0.2.1") (default-features #t) (kind 0)))) (hash "1knii89d69z7ifmlcxidpm4iw0g8rqhrj1hsrbyqkjd88icm48sz")))

(define-public crate-nommy-0.3 (crate (name "nommy") (vers "0.3.0") (deps (list (crate-dep (name "eyre") (req "^0.6") (default-features #t) (kind 0)) (crate-dep (name "nommy_derive") (req "=0.3.0") (default-features #t) (kind 0)))) (hash "109nk55d1rgj3crz2z3k3xgbl90lwg95g5lccg3k5m994yvkssf4")))

(define-public crate-nommy-0.3 (crate (name "nommy") (vers "0.3.1") (deps (list (crate-dep (name "eyre") (req "^0.6") (default-features #t) (kind 0)) (crate-dep (name "nommy_derive") (req "=0.3.0") (default-features #t) (kind 0)))) (hash "13a7c5by6wll1fn6vq8iwhzcq8sff3mn4dcv241h8idxa6sy8bbx")))

(define-public crate-nommy-0.3 (crate (name "nommy") (vers "0.3.2") (deps (list (crate-dep (name "eyre") (req "^0.6") (default-features #t) (kind 0)) (crate-dep (name "nommy_derive") (req "=0.3.0") (default-features #t) (kind 0)))) (hash "1fbhkwvynv13likxfbfx9x47vz73kw8mmdqbqsw177jkxxppj3yq")))

(define-public crate-nommy-0.3 (crate (name "nommy") (vers "0.3.3") (deps (list (crate-dep (name "eyre") (req "^0.6") (default-features #t) (kind 0)) (crate-dep (name "nommy_derive") (req "=0.3.0") (default-features #t) (kind 0)))) (hash "1h73mmnk6z73yvx0a9fx6cc3k7l47vcxzqyd5v2991x1gi41ddgc")))

(define-public crate-nommy-0.3 (crate (name "nommy") (vers "0.3.4") (deps (list (crate-dep (name "eyre") (req "^0.6") (default-features #t) (kind 0)) (crate-dep (name "nommy_derive") (req "=0.3.0") (default-features #t) (kind 0)))) (hash "17irc48bfa51g51khn7i5fimnc7409lpap7raq9i5fvl3ym3pp2w")))

(define-public crate-nommy-0.4 (crate (name "nommy") (vers "0.4.0") (deps (list (crate-dep (name "eyre") (req "^0.6") (default-features #t) (kind 0)) (crate-dep (name "nommy_derive") (req "=0.4.0") (default-features #t) (kind 0)))) (hash "0n00yip2khvlrbh6ycdl1xbdahrb2p89anhgs97w376a7pakm745")))

(define-public crate-nommy-0.4 (crate (name "nommy") (vers "0.4.1") (deps (list (crate-dep (name "eyre") (req "^0.6") (default-features #t) (kind 0)) (crate-dep (name "nommy_derive") (req "=0.4.1") (default-features #t) (kind 0)))) (hash "0ab4chsx108d1kr7xcjb1sssgx76fi38sl7600dfwvly5wigbidn")))

(define-public crate-nommy_derive-0.1 (crate (name "nommy_derive") (vers "0.1.0") (deps (list (crate-dep (name "heck") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (features (quote ("full" "extra-traits"))) (default-features #t) (kind 0)))) (hash "0zgs64wc1zh00csrm1mg7d5mlqq8a9cwbzcfjqjln6fvjx3iqbk2")))

(define-public crate-nommy_derive-0.1 (crate (name "nommy_derive") (vers "0.1.1") (deps (list (crate-dep (name "heck") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (features (quote ("full" "extra-traits"))) (default-features #t) (kind 0)))) (hash "08gpi09x8141q40jw4bg4y3hwpcwxcry1h8gbv6y8401168xwrwn")))

(define-public crate-nommy_derive-0.2 (crate (name "nommy_derive") (vers "0.2.0") (deps (list (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (features (quote ("full" "extra-traits"))) (default-features #t) (kind 0)))) (hash "0ml3bfr36myzyaczn42k8vqxmz3ry3p5129hb94456kmhjncwj1i")))

(define-public crate-nommy_derive-0.2 (crate (name "nommy_derive") (vers "0.2.1") (deps (list (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (features (quote ("full" "extra-traits"))) (default-features #t) (kind 0)))) (hash "1n60lxlwnckvqdi6v0xnpw0w7crl6nh84b9j56ssjgmq56pr1f3i")))

(define-public crate-nommy_derive-0.3 (crate (name "nommy_derive") (vers "0.3.0") (deps (list (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (default-features #t) (kind 0)))) (hash "0bnyixsp57lqyvaydgrqyk2v46kmq5clc3smfkwm2q5cb213jaha")))

(define-public crate-nommy_derive-0.4 (crate (name "nommy_derive") (vers "0.4.0") (deps (list (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (default-features #t) (kind 0)))) (hash "0929mbfn38vzdjybsd6mfgfnlcdrpikaklp4rbmvz5i7zldzyvyj")))

(define-public crate-nommy_derive-0.4 (crate (name "nommy_derive") (vers "0.4.1") (deps (list (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (default-features #t) (kind 0)))) (hash "0sf09m5s6asbn4g75xv0kjji5077ivzbdgg1dp6ncdfvlzy9jh6v")))

