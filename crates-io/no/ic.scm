(define-module (crates-io no ic) #:use-module (crates-io))

(define-public crate-noice-0.7 (crate (name "noice") (vers "0.7.0") (deps (list (crate-dep (name "criterion") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "image") (req "^0.23") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.7") (default-features #t) (kind 0)) (crate-dep (name "rand_xorshift") (req "^0.2") (default-features #t) (kind 0)))) (hash "1xb08fzpgyx55335lha7f5nhi33l2bwn2zr790sn14rr6hanc8l0") (features (quote (("default" "image"))))))

(define-public crate-noice-0.7 (crate (name "noice") (vers "0.7.1") (deps (list (crate-dep (name "criterion") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "image") (req "^0.23") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.7") (default-features #t) (kind 0)) (crate-dep (name "rand_xorshift") (req "^0.2") (default-features #t) (kind 0)))) (hash "0y4sr3p9zzrymxv57qzvmygww4xj9r84npjsnavzp9870mkl5lpl") (features (quote (("default" "image"))))))

