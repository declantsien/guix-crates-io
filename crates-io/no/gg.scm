(define-module (crates-io no gg) #:use-module (crates-io))

(define-public crate-noggin-0.1 (crate (name "noggin") (vers "0.1.0") (deps (list (crate-dep (name "noggin-derive") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "noggin-parser") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "rstest") (req "^0.18.2") (default-features #t) (kind 2)))) (hash "04d5i61l3wjmlps0wjak5jzr21vhir9g7fsbng5dzjqm1blk4jnb")))

(define-public crate-noggin-derive-0.1 (crate (name "noggin-derive") (vers "0.1.0") (deps (list (crate-dep (name "proc-macro2") (req "^1.0.67") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.33") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2.0.37") (default-features #t) (kind 0)))) (hash "1xwaqx5hys74dpbzkm7k0kz1lzllm6yfwc21bq5c175srannrjqw")))

(define-public crate-noggin-parser-0.1 (crate (name "noggin-parser") (vers "0.1.0") (deps (list (crate-dep (name "memchr") (req "^2.6.4") (default-features #t) (kind 0)) (crate-dep (name "rstest") (req "^0.18.2") (default-features #t) (kind 2)) (crate-dep (name "thiserror") (req "^1.0.49") (default-features #t) (kind 0)))) (hash "091jfkyjk01ksvng30a5rpjkdwhf4xi331r53qr3nz4hxmivj9hg")))

