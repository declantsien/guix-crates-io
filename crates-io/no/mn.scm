(define-module (crates-io no mn) #:use-module (crates-io))

(define-public crate-nomnom-0.0.1 (crate (name "nomnom") (vers "0.0.1") (hash "1yj54ykbmp9vkk8rvizbsgjz9v5jpdyf32bdidh8ili8jxg0w134") (yanked #t)))

(define-public crate-nomnom-0.0.2 (crate (name "nomnom") (vers "0.0.2") (hash "0qql9l8qcq5lwxh4x2k9rkvn8ay8d5a9qxh27pbq5nljzbmhwy51") (yanked #t)))

(define-public crate-nomnom-0.0.3 (crate (name "nomnom") (vers "0.0.3") (hash "0rdrw9xlca6kfc7z3li33zrm8w8m7ch9v6m78mnlvr5lqffb3dwz") (yanked #t)))

(define-public crate-nomnom-0.0.4 (crate (name "nomnom") (vers "0.0.4") (hash "0ds3c13wfqc7qbwg3601g1jal5kx6ymlkx81dsl2wry5i81lsfrr") (yanked #t)))

