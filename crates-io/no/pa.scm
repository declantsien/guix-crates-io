(define-module (crates-io no pa) #:use-module (crates-io))

(define-public crate-nopanic-0.0.0 (crate (name "nopanic") (vers "0.0.0") (hash "09b6z5vp6cg7ffvjzyhldxc5axlfcr6v2l9z4vx1a65fidj7sqix") (yanked #t)))

(define-public crate-nopanick-0.1 (crate (name "nopanick") (vers "0.1.0") (hash "12kajca95dw33d0v841b0mhw0kahg32hg3al2z1di4w9hw9szzq2") (yanked #t)))

(define-public crate-nopanick-0.1 (crate (name "nopanick") (vers "0.1.1") (hash "0gbz73srnr4zymi3vgkbdp95vaf8b46jkg1mp8d4x1in07hbnq24") (yanked #t)))

(define-public crate-nopanick-0.1 (crate (name "nopanick") (vers "0.1.2") (hash "0ff1gqq9cxyz3yr29azm24nvd83hcr55zlizqrjf5v3fg5l1rxxa") (yanked #t)))

(define-public crate-nopanick-0.1 (crate (name "nopanick") (vers "0.1.3") (hash "1igfz1p6iwba3nsmmrivv4q5jzrzb6aacqs4fsmpdnb12rwq8zyj") (yanked #t)))

(define-public crate-nopanick-0.1 (crate (name "nopanick") (vers "0.1.4") (hash "1cq9ykx5b4yq6qm1v6kyr2nl3cdjjvv4mbk4a6ywmm3iaf0y2814")))

(define-public crate-nopanick-0.1 (crate (name "nopanick") (vers "0.1.5") (hash "1hsmazjvsmdfyv1203h7idzin19xcg2cn9bl2kp50k5s9pmwdmw6") (yanked #t)))

(define-public crate-nopanick-0.1 (crate (name "nopanick") (vers "0.1.6") (hash "1s0ar1b0bcv9zs81sfz2fbsn6ia4nd6k3s0xs5jzrsc1swg7b792")))

(define-public crate-nopanick-0.1 (crate (name "nopanick") (vers "0.1.7") (hash "17nn9vrqhm266dxycc5fwz9qa7d0ri1hkhbcvb15y2m3jk26c0sm")))

(define-public crate-nopanick-0.2 (crate (name "nopanick") (vers "0.2.0") (hash "1av617lg20398dxnisj1f56iw2p240v1ky4wjk11ybzajvjvpw0x")))

