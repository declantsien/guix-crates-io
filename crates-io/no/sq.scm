(define-module (crates-io no sq) #:use-module (crates-io))

(define-public crate-nosql-0.0.1 (crate (name "nosql") (vers "0.0.1") (hash "04m8xvlxhknkxc3gy2wfaj01zvjwwa0hx58bkvd820189k2d85c9")))

(define-public crate-nosql_db-0.0.1 (crate (name "nosql_db") (vers "0.0.1") (hash "1076lpzkxwahypah221mp3ikm133i0i3249dnn5y72pvfc533wgz")))

(define-public crate-nosql_db-0.0.2 (crate (name "nosql_db") (vers "0.0.2") (hash "171rclmnbi4sadsa56n04kxirbicfpdf5iyagr13s6qlzaci9jh7")))

(define-public crate-nosql_rocksdb-0.0.1 (crate (name "nosql_rocksdb") (vers "0.0.1") (deps (list (crate-dep (name "nosql_db") (req "^0.0.1") (default-features #t) (kind 0)) (crate-dep (name "rocksdb") (req "^0.19.0") (features (quote ("lz4"))) (default-features #t) (kind 0)))) (hash "057yplsrnzz77v1qin7sbmhlzvgjdzbz74fyqjs3mfg5kjpj3q60")))

(define-public crate-nosql_rocksdb-0.0.2 (crate (name "nosql_rocksdb") (vers "0.0.2") (deps (list (crate-dep (name "nosql_db") (req "^0.0.2") (default-features #t) (kind 0)) (crate-dep (name "rocksdb") (req "^0.19.0") (features (quote ("lz4"))) (default-features #t) (kind 0)))) (hash "0vjc09dnq69ls1g92pcdk4kij6z7pli6xcw6wb9v8rqc94657mrn")))

