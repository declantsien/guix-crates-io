(define-module (crates-io no x_) #:use-module (crates-io))

(define-public crate-nox_lib-0.1 (crate (name "nox_lib") (vers "0.1.0") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "0ikaris25f1f8dxnrap574ccbc7657a3rjzd8q0cy9sgr66mlnxw")))

(define-public crate-nox_lib-0.1 (crate (name "nox_lib") (vers "0.1.1") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "1kiknf4dgb2qkk8i742akfkqks3ygbx4zk9v4bcnq1w6bbqma85k")))

