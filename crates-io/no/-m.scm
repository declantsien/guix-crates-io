(define-module (crates-io no -m) #:use-module (crates-io))

(define-public crate-no-mangle-if-debug-0.1 (crate (name "no-mangle-if-debug") (vers "0.1.0") (deps (list (crate-dep (name "proc-macro2") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "0z07lisvv1lijqkf86l8x4rv7x3d3fp275skh15j9pdv70lknv2c")))

(define-public crate-no-more-edge-rs-0.1 (crate (name "no-more-edge-rs") (vers "0.1.0") (deps (list (crate-dep (name "percent-encoding") (req "^2.1.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.6.0") (default-features #t) (kind 0)) (crate-dep (name "winreg") (req "^0.10.1") (default-features #t) (kind 0)))) (hash "0ywbg1qx2jd2iw184sn6xinm7kxajmjdrv9h6zvndbcxs8yh515v")))

