(define-module (crates-io no p-) #:use-module (crates-io))

(define-public crate-nop-json-0.0.2 (crate (name "nop-json") (vers "0.0.2") (deps (list (crate-dep (name "nop-json-derive") (req "^0.0") (default-features #t) (kind 0)) (crate-dep (name "numtoa") (req "^0.2") (default-features #t) (kind 0)))) (hash "0jmqaj7vgj0bm74ynh6alzjxzxx326bdkl22cvrz9srxkxvrdnl3")))

(define-public crate-nop-json-0.0.3 (crate (name "nop-json") (vers "0.0.3") (deps (list (crate-dep (name "nop-json-derive") (req "^0.0") (default-features #t) (kind 0)) (crate-dep (name "numtoa") (req "^0.2") (default-features #t) (kind 0)))) (hash "0m8s1fv9wr01yz4mpif6sf1ilqsbzzfxmgq4pqprrsj21qnkb9l4")))

(define-public crate-nop-json-0.0.4 (crate (name "nop-json") (vers "0.0.4") (deps (list (crate-dep (name "nop-json-derive") (req "^0.0") (default-features #t) (kind 0)) (crate-dep (name "numtoa") (req "^0.2") (default-features #t) (kind 0)))) (hash "0mg05kl8mq8j3n666985hwc6sc4srl6zyfdq8dpygf9rs2ii2qns")))

(define-public crate-nop-json-1 (crate (name "nop-json") (vers "1.0.0") (deps (list (crate-dep (name "nop-json-derive") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "numtoa") (req "^0.2") (default-features #t) (kind 0)))) (hash "1gpbh1ygmqaaglx3f8j9lgg911vpnsph9676nd6l3ly1l1qivpzr")))

(define-public crate-nop-json-1 (crate (name "nop-json") (vers "1.0.1") (deps (list (crate-dep (name "nop-json-derive") (req "^1.1") (default-features #t) (kind 0)) (crate-dep (name "numtoa") (req "^0.2") (default-features #t) (kind 0)))) (hash "0h385fss6q7z2wccf12k7wzxi8lkgd7b96dmpcpr12l6jbzc646a")))

(define-public crate-nop-json-1 (crate (name "nop-json") (vers "1.0.2") (deps (list (crate-dep (name "nop-json-derive") (req "^1.1") (default-features #t) (kind 0)) (crate-dep (name "numtoa") (req "^0.2") (default-features #t) (kind 0)))) (hash "0h2vc6kp552i97zwdppd934h6a9piww30zzsmkfmlf2sxq123sap")))

(define-public crate-nop-json-1 (crate (name "nop-json") (vers "1.0.3") (deps (list (crate-dep (name "nop-json-derive") (req "^1.1") (default-features #t) (kind 0)) (crate-dep (name "numtoa") (req "^0.2") (default-features #t) (kind 0)))) (hash "0b086jkwvq7xpivnpr4k98l4nphvyr9n0cr0ah1dy927msd06sr0")))

(define-public crate-nop-json-1 (crate (name "nop-json") (vers "1.0.4") (deps (list (crate-dep (name "nop-json-derive") (req "^1.1") (default-features #t) (kind 0)) (crate-dep (name "numtoa") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "read_iter") (req "^0.1") (default-features #t) (kind 2)))) (hash "16775bi9waxfn7yanb063h41qlsr0s07jdh8057s5lrpsz9fsjyw")))

(define-public crate-nop-json-1 (crate (name "nop-json") (vers "1.0.5") (deps (list (crate-dep (name "nop-json-derive") (req "^1.1") (default-features #t) (kind 0)) (crate-dep (name "numtoa") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "read_iter") (req "^0.1") (default-features #t) (kind 2)))) (hash "1y6j045gsrivgwn6569db91sqihw9c4nv76n4s3k6c8jb7zc7lsz")))

(define-public crate-nop-json-1 (crate (name "nop-json") (vers "1.0.6") (deps (list (crate-dep (name "nop-json-derive") (req "^1.1") (default-features #t) (kind 0)) (crate-dep (name "numtoa") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "read_iter") (req "^0.1") (default-features #t) (kind 2)))) (hash "1xdwk2gsvvm01mnnmjri2asw3mpzxahz5983qan6lcx6w26nhgg2")))

(define-public crate-nop-json-1 (crate (name "nop-json") (vers "1.0.7") (deps (list (crate-dep (name "nop-json-derive") (req "^1.1") (default-features #t) (kind 0)) (crate-dep (name "numtoa") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "read_iter") (req "^0.1") (default-features #t) (kind 2)))) (hash "08vihiwhfjcifkh4wj9snhkyhdhm6x3bk49iyqmwwqy85vlfkzka")))

(define-public crate-nop-json-1 (crate (name "nop-json") (vers "1.0.8") (deps (list (crate-dep (name "nop-json-derive") (req "^1.1") (default-features #t) (kind 0)) (crate-dep (name "numtoa") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "read_iter") (req "^0.1") (default-features #t) (kind 2)))) (hash "03ahcb92pig34kf097a0vdjdnajm69p5pnh4922g74z2jcwjql40")))

(define-public crate-nop-json-1 (crate (name "nop-json") (vers "1.0.9") (deps (list (crate-dep (name "nop-json-derive") (req "^1.1") (default-features #t) (kind 0)) (crate-dep (name "numtoa") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "read_iter") (req "^0.1") (default-features #t) (kind 2)))) (hash "15jw8gl2wfvil02nz9nxmyk5cmrzwaqcq6jidcq4cddnanq6m75w")))

(define-public crate-nop-json-2 (crate (name "nop-json") (vers "2.0.0") (deps (list (crate-dep (name "nop-json-derive") (req "^2.0") (default-features #t) (kind 0)) (crate-dep (name "numtoa") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "read_iter") (req "^0.1") (default-features #t) (kind 2)))) (hash "02sghykrvh4fdv21g9r2rqh23hl1f1nc2pa96p8pp2fv9plzahas")))

(define-public crate-nop-json-2 (crate (name "nop-json") (vers "2.0.1") (deps (list (crate-dep (name "nop-json-derive") (req "^2.0") (default-features #t) (kind 0)) (crate-dep (name "numtoa") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "read_iter") (req "^0.1") (default-features #t) (kind 2)))) (hash "1pwyvrvqfamz0c5rfq684kxwkxdwxwmn2v3l9dq0h3jhmxrdpwvg")))

(define-public crate-nop-json-2 (crate (name "nop-json") (vers "2.0.2") (deps (list (crate-dep (name "nop-json-derive") (req "^2.0") (default-features #t) (kind 0)) (crate-dep (name "numtoa") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "read_iter") (req "^0.1") (default-features #t) (kind 2)))) (hash "17c3swvq7ns0gshw6ypcskhm2ayb52afjn4s6r5w7pvq3vlm17wn")))

(define-public crate-nop-json-2 (crate (name "nop-json") (vers "2.0.3") (deps (list (crate-dep (name "nop-json-derive") (req "^2.0") (default-features #t) (kind 0)) (crate-dep (name "numtoa") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "read_iter") (req "^0.1") (default-features #t) (kind 2)))) (hash "1yy5846jyxncx9y8jbq4y05if02jl8izc049280fjl86wnrgx6x4")))

(define-public crate-nop-json-2 (crate (name "nop-json") (vers "2.0.4") (deps (list (crate-dep (name "nop-json-derive") (req "^2.0") (default-features #t) (kind 0)) (crate-dep (name "numtoa") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "read_iter") (req "^0.1") (default-features #t) (kind 2)))) (hash "0m668af2zll17vkxhkpk9rbdvgn3i97cl2ih6j1n5sbyf43hfix8")))

(define-public crate-nop-json-2 (crate (name "nop-json") (vers "2.0.5") (deps (list (crate-dep (name "nop-json-derive") (req "^2.0") (default-features #t) (kind 0)) (crate-dep (name "numtoa") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "read_iter") (req "^0.1") (default-features #t) (kind 2)))) (hash "1rqqg6vg64wchfvzkzfnirz3dnvb1laa8n2aawflnbrwhs7pfd70")))

(define-public crate-nop-json-derive-0.0.1 (crate (name "nop-json-derive") (vers "0.0.1") (deps (list (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (default-features #t) (kind 0)))) (hash "178xcf8y86p7z7w9np5pd9d1i1dhgwrw2z6v9ws1p6i1yyn39wis")))

(define-public crate-nop-json-derive-1 (crate (name "nop-json-derive") (vers "1.0.0") (deps (list (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (default-features #t) (kind 0)))) (hash "1a48rfg1vh4hvj1f0kam3igb3hlvq57cikqzzy5kb85mpp25awrx")))

(define-public crate-nop-json-derive-1 (crate (name "nop-json-derive") (vers "1.1.0") (deps (list (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (default-features #t) (kind 0)))) (hash "0fxcmz00jh53sra4f0nq923kyzpxz3cdawwmp066ap3d33n3a8vr")))

(define-public crate-nop-json-derive-1 (crate (name "nop-json-derive") (vers "1.1.1") (deps (list (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (default-features #t) (kind 0)))) (hash "19f9c9wmngwl3dmvrvl9911136gwv24wv5g4gfnhvv6a3l98r9ir")))

(define-public crate-nop-json-derive-1 (crate (name "nop-json-derive") (vers "1.1.2") (deps (list (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (default-features #t) (kind 0)))) (hash "0n1xx8qx7mpanzjddv3y5h4gnpccggn8i01m0gzlsjq51n99dwjx")))

(define-public crate-nop-json-derive-1 (crate (name "nop-json-derive") (vers "1.1.3") (deps (list (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (default-features #t) (kind 0)))) (hash "1bdaic2pnw3i7b54q9ykiygqhiahb7sjhyw0l6j0hxk62vinpm35")))

(define-public crate-nop-json-derive-1 (crate (name "nop-json-derive") (vers "1.1.4") (deps (list (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (default-features #t) (kind 0)))) (hash "115czinczx5a1lzr37a474ff1zjhlp7q190zfnqy728q1l775843")))

(define-public crate-nop-json-derive-2 (crate (name "nop-json-derive") (vers "2.0.0") (deps (list (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (default-features #t) (kind 0)))) (hash "0542rxncpdwmc6zaxj399yyq881caxbzmka1mzy718ca89dqka3n")))

(define-public crate-nop-json-derive-2 (crate (name "nop-json-derive") (vers "2.0.1") (deps (list (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (default-features #t) (kind 0)))) (hash "19rkc27qz7w0jik5i6kmbrs9iwp13w8n6hp7mfk44s7srb7isv7l")))

(define-public crate-nop-json-derive-2 (crate (name "nop-json-derive") (vers "2.0.2") (deps (list (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (default-features #t) (kind 0)))) (hash "1b4fzrcc4qm38nwskzzc25a1p5dp6yy0xbd4aay6wjffi5jp758s")))

(define-public crate-nop-json-derive-2 (crate (name "nop-json-derive") (vers "2.0.3") (deps (list (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (default-features #t) (kind 0)))) (hash "0igi3p7fxa8qyw61haw1pl0mq1hlflnqw5h6ci3s2q4zv8q4i6az")))

(define-public crate-nop-json-derive-2 (crate (name "nop-json-derive") (vers "2.0.4") (deps (list (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (default-features #t) (kind 0)))) (hash "1v12ixz3aq89bdk6jjaiszn5wjshn6ymlwf1r5fm34c98k70q3ys")))

(define-public crate-nop-json-derive-2 (crate (name "nop-json-derive") (vers "2.0.5") (deps (list (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (default-features #t) (kind 0)))) (hash "0yz0qr27rc17zxkqs8003r6cakn2qi5ln0x6rwyz5kpgjss0i4lx")))

