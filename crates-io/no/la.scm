(define-module (crates-io no la) #:use-module (crates-io))

(define-public crate-nolan-0.1 (crate (name "nolan") (vers "0.1.0") (deps (list (crate-dep (name "bincode") (req "^1.3.3") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "1yqqbhd4mnfpjv62031yv2xcy37q3ha91vb2pds9fig98vsrplsw")))

(define-public crate-nolan-0.1 (crate (name "nolan") (vers "0.1.1") (deps (list (crate-dep (name "bincode") (req "^1.3.3") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "0ihp7xkw9fr530mdwl3960ll5xgshydjcjmxkbf7mchxrnqwbswj")))

