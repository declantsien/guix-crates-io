(define-module (crates-io no po) #:use-module (crates-io))

(define-public crate-nopony-0.0.0 (crate (name "nopony") (vers "0.0.0") (hash "16zrc02xgi2h39chixampm3lmjx5izjb5avj6vvc8kk6074yh5f8") (yanked #t)))

(define-public crate-nopony-0.0.1 (crate (name "nopony") (vers "0.0.1") (hash "1lmawsn6dc0h6ilhvzsnk4wdirycrpscsmd8g3fgmdvd43arv68p") (yanked #t)))

