(define-module (crates-io no t_) #:use-module (crates-io))

(define-public crate-not_copy-1 (crate (name "not_copy") (vers "1.0.0") (hash "0mkifslyllri2a660fvdjqm5jhl3l1amq9pcwgks740lwl2njv4g")))

(define-public crate-not_copy-1 (crate (name "not_copy") (vers "1.0.1") (deps (list (crate-dep (name "serde") (req "^1.0") (optional #t) (default-features #t) (kind 0)))) (hash "12sr4drw6lcbvqmnfdvy8k5wgkmibzm8n081rgyv8mgmi0j8gk0c")))

(define-public crate-not_empty-0.1 (crate (name "not_empty") (vers "0.1.0") (deps (list (crate-dep (name "serde") (req "^1") (optional #t) (kind 0)) (crate-dep (name "version-sync") (req "^0.9.4") (default-features #t) (kind 2)))) (hash "1l891il124m7iq4g5q1xa72xivqc6krvm6hbyvw3hdkayill0jx9") (features (quote (("default" "std")))) (v 2) (features2 (quote (("std" "serde?/std") ("serde" "dep:serde") ("alloc" "serde?/alloc"))))))

(define-public crate-not_empty-0.1 (crate (name "not_empty") (vers "0.1.1") (deps (list (crate-dep (name "serde") (req "^1") (optional #t) (kind 0)) (crate-dep (name "version-sync") (req "^0.9.4") (default-features #t) (kind 2)))) (hash "046j159lza3dj7qpq9pqq6r029fcha5iyy9swzw6vms7a75waszm") (features (quote (("default" "std")))) (v 2) (features2 (quote (("std" "serde?/std") ("serde" "dep:serde") ("alloc" "serde?/alloc"))))))

(define-public crate-not_empty-0.1 (crate (name "not_empty") (vers "0.1.2") (deps (list (crate-dep (name "serde") (req "^1") (optional #t) (kind 0)) (crate-dep (name "version-sync") (req "^0.9.4") (default-features #t) (kind 2)))) (hash "0ch0a28rs1rlfmsdqc60ig24i6g11v6kb4g9ivnwg9zz2bp8gy3g") (features (quote (("default" "std")))) (v 2) (features2 (quote (("std" "serde?/std") ("serde" "dep:serde") ("alloc" "serde?/alloc"))))))

(define-public crate-not_empty-0.1 (crate (name "not_empty") (vers "0.1.3") (deps (list (crate-dep (name "serde") (req "^1") (optional #t) (kind 0)) (crate-dep (name "version-sync") (req "^0.9.4") (default-features #t) (kind 2)))) (hash "02r8rkf7mdjc8m3q3h6qv2kah1afgz8sk9z5z1g1wahggz9r49b0") (features (quote (("default" "std")))) (v 2) (features2 (quote (("std" "serde?/std") ("serde" "dep:serde") ("alloc" "serde?/alloc"))))))

(define-public crate-not_lci-0.0.0 (crate (name "not_lci") (vers "0.0.0") (hash "0gy9zql5xvc1zgnfs2icfv9harh5a0zlra2bghja0p1aijn21775") (yanked #t)))

