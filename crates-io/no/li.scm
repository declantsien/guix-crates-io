(define-module (crates-io no li) #:use-module (crates-io))

(define-public crate-nolibc-0.1 (crate (name "nolibc") (vers "0.1.0") (hash "0mv9w3djxlhklv62fpkaq2h0n171sacxmwrkjffi52c1hcpnfs1b")))

(define-public crate-nolife-0.1 (crate (name "nolife") (vers "0.1.0") (hash "09fss3y8f3jgb60hs6gnapxjyw99759rmdqj5h2yngqqg79dhzrn")))

(define-public crate-nolife-0.2 (crate (name "nolife") (vers "0.2.0") (hash "0dfpla74qmf0fnd7c2a5lm5kcqj0mn81p276alxswy4yx9g7fz31")))

(define-public crate-nolife-0.3 (crate (name "nolife") (vers "0.3.0") (hash "0mnxm439c7j0cb2dswglib3v87l6zxd6ypck0zq6gqm9siiwjjpx")))

(define-public crate-nolife-0.3 (crate (name "nolife") (vers "0.3.1") (hash "1p2h7vz5qqwnqim9rgrn06fzzivamhnkzy4j4rx2x9g8hzqallmw")))

(define-public crate-nolife-0.3 (crate (name "nolife") (vers "0.3.2") (hash "102mrnh81nml86zbx1mr4si8m7grgjxycgcvvs125j0z0542xf8y")))

(define-public crate-nolife-0.3 (crate (name "nolife") (vers "0.3.3") (hash "07rr7qhiqalm5sgmk907fdig0m10qsbm7hqhjsim6ljhhrn53sj5")))

(define-public crate-nolife-0.4 (crate (name "nolife") (vers "0.4.0") (hash "0wav02d661pzckbzs30zdxawd8znd100rgqs32nzq9kdg8msjv2l")))

(define-public crate-noline-0.1 (crate (name "noline") (vers "0.1.0") (deps (list (crate-dep (name "crossbeam") (req "^0.8.1") (default-features #t) (kind 2)) (crate-dep (name "embedded-hal") (req "^0.2.6") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "nb") (req "^1.0.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "nb") (req "^1.0.0") (default-features #t) (kind 2)) (crate-dep (name "num_enum") (req "^0.5.6") (kind 0)) (crate-dep (name "termion") (req "^1.5.6") (default-features #t) (kind 2)) (crate-dep (name "tokio") (req "^1.16.1") (features (quote ("io-util" "sync"))) (optional #t) (default-features #t) (kind 0)))) (hash "0w331cmq7cclhnrp27gwdw2clxv2wbrb6ha66gbr9cfkkrdhrcib") (features (quote (("std") ("embedded" "embedded-hal" "nb") ("default") ("alloc"))))))

(define-public crate-noline-0.2 (crate (name "noline") (vers "0.2.0") (deps (list (crate-dep (name "crossbeam") (req "^0.8.1") (default-features #t) (kind 2)) (crate-dep (name "embedded-hal") (req "^0.2.6") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "nb") (req "^1.0.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "nb") (req "^1.0.0") (default-features #t) (kind 2)) (crate-dep (name "num_enum") (req "^0.5.6") (kind 0)) (crate-dep (name "termion") (req "^1.5.6") (default-features #t) (kind 2)) (crate-dep (name "tokio") (req "^1.16.1") (features (quote ("io-util" "sync"))) (optional #t) (default-features #t) (kind 0)))) (hash "0g6i3jjpr1mb4m7qj6f871ss7g7v13snk9jzyzc5kqivac25dpx9") (features (quote (("std") ("embedded" "embedded-hal" "nb") ("default") ("alloc"))))))

