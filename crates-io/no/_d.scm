(define-module (crates-io no _d) #:use-module (crates-io))

(define-public crate-no_deadlocks-1 (crate (name "no_deadlocks") (vers "1.0.0") (deps (list (crate-dep (name "backtrace") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "vector-map") (req "^0.5") (optional #t) (default-features #t) (kind 0)))) (hash "1gpwsqyhn1d352fmw9n53m3gr0bamy2xkmcx5xlla8qgqhazipax") (features (quote (("use_vecmap" "vector-map") ("default" "use_vecmap"))))))

(define-public crate-no_deadlocks-1 (crate (name "no_deadlocks") (vers "1.1.0") (deps (list (crate-dep (name "backtrace") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "vector-map") (req "^0.5") (optional #t) (default-features #t) (kind 0)))) (hash "026vcyvyalksq9aahns30dkxq486b4rml1bjzpqhx5lbwkkszy6d") (features (quote (("use_vecmap" "vector-map") ("default" "use_vecmap"))))))

(define-public crate-no_deadlocks-1 (crate (name "no_deadlocks") (vers "1.1.1") (deps (list (crate-dep (name "backtrace") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "vector-map") (req "^1.0") (optional #t) (default-features #t) (kind 0)))) (hash "17a2bzyxal6axi69w2fanqyrwjk4rzrldj0942g5ppl4ms7rkmxm") (features (quote (("use_vecmap" "vector-map") ("default" "use_vecmap"))))))

(define-public crate-no_deadlocks-1 (crate (name "no_deadlocks") (vers "1.1.2") (deps (list (crate-dep (name "backtrace") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "vector-map") (req "^1.0") (optional #t) (default-features #t) (kind 0)))) (hash "1l10zfxcy538x78drkqbgddy5nd4ngxsl7lj2ancbzwsjza51vly") (features (quote (("use_vecmap" "vector-map") ("default" "use_vecmap"))))))

(define-public crate-no_deadlocks-1 (crate (name "no_deadlocks") (vers "1.2.0") (deps (list (crate-dep (name "backtrace") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "vector-map") (req "^1.0") (optional #t) (default-features #t) (kind 0)))) (hash "1jjwibxcgcxiclf523z6dbiqnbjdrv6idlx8wx11lx37la6l7ssj") (features (quote (("use_vecmap" "vector-map") ("default" "use_vecmap"))))))

(define-public crate-no_deadlocks-1 (crate (name "no_deadlocks") (vers "1.3.0") (deps (list (crate-dep (name "backtrace") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "vector-map") (req "^1.0") (optional #t) (default-features #t) (kind 0)))) (hash "10y5lb6i8099x4djxihkf4p410ja0d5bm248p92q561k1flarswl") (features (quote (("use_vecmap" "vector-map") ("default" "use_vecmap"))))))

(define-public crate-no_deadlocks-1 (crate (name "no_deadlocks") (vers "1.3.1") (deps (list (crate-dep (name "backtrace") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "vector-map") (req "^1.0") (optional #t) (default-features #t) (kind 0)))) (hash "198lmjw0gprzjdvd3dmmypcws20qnsdjkmgyjrjij5ykjixlij0f") (features (quote (("use_vecmap" "vector-map") ("default" "use_vecmap"))))))

(define-public crate-no_deadlocks-1 (crate (name "no_deadlocks") (vers "1.3.2") (deps (list (crate-dep (name "backtrace") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "vector-map") (req "^1.0") (optional #t) (default-features #t) (kind 0)))) (hash "0pnfsb09h3cdgnrx06ndl5dbhl5p9xgbzbsc49gnj3fg9w7azq70") (features (quote (("use_vecmap" "vector-map") ("default" "use_vecmap"))))))

(define-public crate-no_debug-0.1 (crate (name "no_debug") (vers "0.1.0") (hash "07xi7c1g3pw6pa482jlgdwc8dlrc4wzxi636mspjj67p7fqnqg04") (yanked #t)))

(define-public crate-no_debug-1 (crate (name "no_debug") (vers "1.0.0") (hash "016brn86h619pb1ywxycp1lxrvihk40giwkdjsjbyyh9248axd4s") (yanked #t)))

(define-public crate-no_debug-1 (crate (name "no_debug") (vers "1.0.1") (hash "024y996zyjqnfqcjgpf3nj9s229b06mnghpjks2l5dx0s70jmixm") (yanked #t)))

(define-public crate-no_debug-1 (crate (name "no_debug") (vers "1.0.2") (hash "1757jismm72qjhp9fbfksp3xnw78s366kyb0dw9ybv50ikdj3d8h") (yanked #t)))

(define-public crate-no_debug-2 (crate (name "no_debug") (vers "2.0.0") (hash "0hiqgk7vjzifx6vv09dbac3m6jdmikbzzyd7nnnj2hav8fz4qpfb")))

(define-public crate-no_debug-2 (crate (name "no_debug") (vers "2.0.1") (hash "01fn6ws9y8rzvlq849qkla80jq3q5cbsa8fisvkx63arz7p40lyc")))

(define-public crate-no_debug-3 (crate (name "no_debug") (vers "3.0.0") (hash "01lnbj5h3w1r9vzzx94r3frqsm53qp3nrz27lnxw2552ynkxmcym")))

(define-public crate-no_debug-3 (crate (name "no_debug") (vers "3.1.0") (hash "192j780ihhaq8d10nd95hz8pvp6gw1952hyr3py4848fhl6ac8rz")))

(define-public crate-no_denormals-0.1 (crate (name "no_denormals") (vers "0.1.0") (hash "15k3bk2yrjnn8fknplrmqj8a1c3hix91k2x860fkp190cqkrsrc3")))

(define-public crate-no_denormals-0.1 (crate (name "no_denormals") (vers "0.1.1") (hash "14zwyc3frdmqsd0wldvli7vk9ic5947s4xly47yn4dlr1b93ma14")))

(define-public crate-no_denormals-0.1 (crate (name "no_denormals") (vers "0.1.2") (hash "1y8bdw739lg2h9xn60sdpg15460zh77xbp9nfwfyafrsw3f4x3gw")))

