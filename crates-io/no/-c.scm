(define-module (crates-io no -c) #:use-module (crates-io))

(define-public crate-no-comment-0.0.1 (crate (name "no-comment") (vers "0.0.1") (hash "1wva0524y2r7svwfp99cpwpxa6li3sg79am4dyyvlkp6yjlangsb")))

(define-public crate-no-comment-0.0.2 (crate (name "no-comment") (vers "0.0.2") (deps (list (crate-dep (name "derive_more") (req "^0.99.5") (default-features #t) (kind 0)))) (hash "1xav5vnmzr86hrc7ns2f651adi1qaxns7n8hx7n5m79cbgzpy2ca")))

(define-public crate-no-comment-0.0.3 (crate (name "no-comment") (vers "0.0.3") (deps (list (crate-dep (name "derive_more") (req "^0.99.5") (default-features #t) (kind 0)))) (hash "1kn2bb2whn7n12jq6rw9rjwrcrfqaii5gn1rv3z0abjqkrw3mwhg")))

