(define-module (crates-io no fo) #:use-module (crates-io))

(define-public crate-nofollow-0.1 (crate (name "nofollow") (vers "0.1.0") (deps (list (crate-dep (name "libc") (req "^0.2.86") (target "cfg(unix)") (kind 0)) (crate-dep (name "tempfile") (req "^3.2.0") (default-features #t) (kind 2)))) (hash "0z9wl2sz7f3f2bb84q5qi24sqq1w8cd26q7vl3y8jbjcqjs16yix") (yanked #t)))

(define-public crate-nofollow-0.1 (crate (name "nofollow") (vers "0.1.1") (deps (list (crate-dep (name "libc") (req "^0.2.86") (target "cfg(unix)") (kind 0)) (crate-dep (name "tempfile") (req "^3.2.0") (default-features #t) (kind 2)))) (hash "1nqlhk9n69v6ch0195fg51xh2ysgkj98w6w4f61a0x7wjzm3klzk") (yanked #t)))

(define-public crate-nofollow-0.1 (crate (name "nofollow") (vers "0.1.2") (deps (list (crate-dep (name "libc") (req "^0.2.93") (target "cfg(unix)") (kind 0)) (crate-dep (name "tempfile") (req "^3.2.0") (default-features #t) (kind 2)))) (hash "0idhb5nnfk2j9kijd2ffpdwzl9fw4nxx132aphnd032xhjmp3fdn") (yanked #t)))

(define-public crate-nofollow-0.1 (crate (name "nofollow") (vers "0.1.3") (deps (list (crate-dep (name "libc") (req "^0.2.94") (target "cfg(unix)") (kind 0)) (crate-dep (name "tempfile") (req "^3.2.0") (default-features #t) (kind 2)))) (hash "0fa6k88a2irz5v80a8cfw38lcv7d57zdi6vni1jrdiw08vc8gny6") (yanked #t)))

(define-public crate-nofollow-0.1 (crate (name "nofollow") (vers "0.1.4") (deps (list (crate-dep (name "libc") (req "^0.2.96") (target "cfg(unix)") (kind 0)) (crate-dep (name "tempfile") (req "^3.2.0") (default-features #t) (kind 2)))) (hash "0j5gjrv9clahybfxv469vmcfkgbhdz53j8hpplc6x89jgxzgbnsd") (yanked #t)))

