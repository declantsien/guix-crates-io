(define-module (crates-io tv #{1d}#) #:use-module (crates-io))

(define-public crate-tv1d-0.1 (crate (name "tv1d") (vers "0.1.0") (deps (list (crate-dep (name "num") (req "^0.1.37") (default-features #t) (kind 0)))) (hash "1lf9zjqzrl2lmpgm83zqgv77gkf8cj3sqjnbl1mlb9n21zb8i1jy")))

