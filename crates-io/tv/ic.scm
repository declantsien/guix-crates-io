(define-module (crates-io tv ic) #:use-module (crates-io))

(define-public crate-tvic-0.1 (crate (name "tvic") (vers "0.1.0") (hash "1id24bdb77qk6lv1j3mf4ib8bpxib05rpc5yppwfp8r8fygf2c3g")))

(define-public crate-tvic-client-0.1 (crate (name "tvic-client") (vers "0.1.0") (hash "0zf6y7m5216176g70k1al26ykdl5i39zpk3khnggrsfmnhqikpp7")))

(define-public crate-tvic-quinn-0.1 (crate (name "tvic-quinn") (vers "0.1.0") (hash "0ldmfvr465i3sk2x5mcygxs83y2jm476x9sv24nxi1f4hcqaihp7")))

(define-public crate-tvic-server-0.1 (crate (name "tvic-server") (vers "0.1.0") (hash "0aylk8srg6jb3k23mj1w3s27d9p3qaibx4029hjmb7gwl8cyhd8j")))

