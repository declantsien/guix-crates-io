(define-module (crates-io tv is) #:use-module (crates-io))

(define-public crate-tvis-0.1 (crate (name "tvis") (vers "0.1.0") (hash "0izvg3548vp5rji44wz4lvfib7h2v6ah8sqvjg92gbcgahfqsyrl")))

(define-public crate-tvis-0.1 (crate (name "tvis") (vers "0.1.1") (hash "05dsppqpp3y4by1qm89az4v56hw8g6kh7larswsqrmja23k5786d")))

(define-public crate-tvis-0.2 (crate (name "tvis") (vers "0.2.0") (hash "1bgnizk6gri0vyss4mchj5ww877q6qbw3fa3r1vanr3ywika76vn")))

(define-public crate-tvis-0.3 (crate (name "tvis") (vers "0.3.0") (hash "1cz4a7kwj0zs0qylijk2a97kz1sdbdkv42nivmvwj3cz7hz8wqcg")))

(define-public crate-tvis-0.3 (crate (name "tvis") (vers "0.3.1") (hash "0bx7rx5xhcal063x56qiffyvy96zkncn0g6nq68f3mxdnn9gmxsj")))

(define-public crate-tvis-0.5 (crate (name "tvis") (vers "0.5.0") (hash "0b36lbhxgxh245kg2s2xsb77zvkmvraqnjp52lgg6d3jrzab40cy")))

(define-public crate-tvis-0.5 (crate (name "tvis") (vers "0.5.1") (hash "0asdinz56l01qmclbx8p5h87clcrsgzxi8y09ssi43qwwnyzfwm0")))

(define-public crate-tvis-0.6 (crate (name "tvis") (vers "0.6.0") (hash "13v0n9ibn7acl1zhlxvnmx0q2g8hrd5nf5d0ka33bhx3p93qkkda")))

(define-public crate-tvis-0.7 (crate (name "tvis") (vers "0.7.0") (hash "1sg5nqn86611jda7zq8g7rpkqhzv1x7nvalhbqzsl9raficffrzd")))

(define-public crate-tvis-0.10 (crate (name "tvis") (vers "0.10.0") (hash "09g9nd3ykl7qw979p8bf301ingpprfyah1p7wdkdygl1zpx2ydmk")))

(define-public crate-tvis-0.10 (crate (name "tvis") (vers "0.10.1") (deps (list (crate-dep (name "tinf") (req "^0.10.0") (default-features #t) (kind 0)))) (hash "1h6fi2n2d3i10zd1wm7nlhwz6wfmksa5kmi00xamawchg869y873")))

(define-public crate-tvis-0.11 (crate (name "tvis") (vers "0.11.0") (deps (list (crate-dep (name "tinf") (req "^0") (default-features #t) (kind 0)))) (hash "1d3p217mwrsc30jxj96g14rgkh80zal80icgw8y6mn7jqbi4wja7")))

(define-public crate-tvis-0.12 (crate (name "tvis") (vers "0.12.0") (deps (list (crate-dep (name "tinf") (req "^0") (default-features #t) (kind 0)))) (hash "0ijppm3plfr7yfhfxb6i392cjyfg6ix7ci5326xq6plalhlm9j89")))

(define-public crate-tvis-0.14 (crate (name "tvis") (vers "0.14.1") (deps (list (crate-dep (name "bitflags") (req "^0.9.1") (default-features #t) (kind 0)) (crate-dep (name "kernel32-sys") (req "^0.2.2") (default-features #t) (target "cfg(windows)") (kind 0)) (crate-dep (name "libc") (req "^0.2.23") (default-features #t) (kind 0)) (crate-dep (name "tinf") (req "^0") (default-features #t) (kind 0)) (crate-dep (name "tvis_util") (req "^0") (default-features #t) (kind 0)) (crate-dep (name "user32-sys") (req "^0.2.0") (default-features #t) (target "cfg(windows)") (kind 0)) (crate-dep (name "winapi") (req "^0.2.8") (default-features #t) (target "cfg(windows)") (kind 0)))) (hash "15ixqqyb5710cf465dkvnzfy5wjh7szy1sp8dn6dmrd8s7y3hj28")))

(define-public crate-tvis-0.15 (crate (name "tvis") (vers "0.15.0") (deps (list (crate-dep (name "bitflags") (req "^1.0.0") (default-features #t) (kind 0)) (crate-dep (name "kernel32-sys") (req "^0.2.2") (default-features #t) (target "cfg(windows)") (kind 0)) (crate-dep (name "lazy_static") (req "^0.2.9") (default-features #t) (target "cfg(not(windows))") (kind 0)) (crate-dep (name "libc") (req "^0.2.32") (default-features #t) (kind 0)) (crate-dep (name "tinf") (req "^0") (default-features #t) (kind 0)) (crate-dep (name "tvis_util") (req "^0") (default-features #t) (kind 0)) (crate-dep (name "user32-sys") (req "^0.2.0") (default-features #t) (target "cfg(windows)") (kind 0)) (crate-dep (name "winapi") (req "^0.2.8") (default-features #t) (target "cfg(windows)") (kind 0)))) (hash "0ynz6ms8v1dd5qahn2j4aiilxkk4xnc03f3k3xq66m5ck4a6pvl9")))

(define-public crate-tvis-0.15 (crate (name "tvis") (vers "0.15.2") (deps (list (crate-dep (name "bitflags") (req "^1.0.0") (default-features #t) (kind 0)) (crate-dep (name "kernel32-sys") (req "^0.2.2") (default-features #t) (target "cfg(windows)") (kind 0)) (crate-dep (name "lazy_static") (req "^0.2.9") (default-features #t) (target "cfg(not(windows))") (kind 0)) (crate-dep (name "libc") (req "^0.2.32") (default-features #t) (kind 0)) (crate-dep (name "tinf") (req "^0") (default-features #t) (target "cfg(not(windows))") (kind 0)) (crate-dep (name "tvis_util") (req "^0") (default-features #t) (kind 0)) (crate-dep (name "user32-sys") (req "^0.2.0") (default-features #t) (target "cfg(windows)") (kind 0)) (crate-dep (name "winapi") (req "^0.2.8") (default-features #t) (target "cfg(windows)") (kind 0)))) (hash "0aw7mf5ir8m85jakpl5zly5b1v9qv1kcam0arsxqy5307yky6p2w")))

(define-public crate-tvis-0.15 (crate (name "tvis") (vers "0.15.3") (deps (list (crate-dep (name "bitflags") (req "^1.0.0") (default-features #t) (kind 0)) (crate-dep (name "dlx") (req "^0.1.0") (default-features #t) (kind 2)) (crate-dep (name "kernel32-sys") (req "^0.2.2") (default-features #t) (target "cfg(windows)") (kind 0)) (crate-dep (name "lazy_static") (req "^0.2.9") (default-features #t) (target "cfg(not(windows))") (kind 0)) (crate-dep (name "lazy_static") (req "^0.2.9") (default-features #t) (kind 2)) (crate-dep (name "libc") (req "^0.2.32") (default-features #t) (kind 0)) (crate-dep (name "tinf") (req "^0") (default-features #t) (target "cfg(not(windows))") (kind 0)) (crate-dep (name "tvis_util") (req "^0") (default-features #t) (kind 0)) (crate-dep (name "user32-sys") (req "^0.2.0") (default-features #t) (target "cfg(windows)") (kind 0)) (crate-dep (name "winapi") (req "^0.2.8") (default-features #t) (target "cfg(windows)") (kind 0)))) (hash "0amcjql6iyqkvzhlj26ryhxghagzv00k66gnsslldmil6pzg8h3y")))

(define-public crate-tvis_util-0.1 (crate (name "tvis_util") (vers "0.1.0") (deps (list (crate-dep (name "libc") (req "^0.2.23") (default-features #t) (kind 0)))) (hash "0g9506b9jkf4yxkp9px274xmaydbgxjij54cf6f1mcgxnk0rz9z0")))

(define-public crate-tvis_util-0.2 (crate (name "tvis_util") (vers "0.2.0") (deps (list (crate-dep (name "lazy_static") (req "^0.2.8") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2.23") (default-features #t) (kind 0)))) (hash "05d8pg3591d5rvvv36w8rm7c23wgai1n1wk4g4cvk8g41flz3g1k")))

(define-public crate-tvis_util-0.3 (crate (name "tvis_util") (vers "0.3.0") (deps (list (crate-dep (name "lazy_static") (req "^0.2.8") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2.23") (default-features #t) (kind 0)))) (hash "12g09v21dj3iad7kmldzzhnv7lljaz3n4258fjxp6f2ainnpz5h3")))

(define-public crate-tvis_util-0.3 (crate (name "tvis_util") (vers "0.3.1") (deps (list (crate-dep (name "lazy_static") (req "^0.2.8") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2.23") (default-features #t) (kind 0)))) (hash "0yvyh45a972dx2wmx5ykmhv3y5lvvz56gfacw1s8clagr6vpcj4q")))

(define-public crate-tvis_util-0.3 (crate (name "tvis_util") (vers "0.3.2") (deps (list (crate-dep (name "advapi32-sys") (req "^0.2.0") (default-features #t) (target "cfg(windows)") (kind 0)) (crate-dep (name "kernel32-sys") (req "^0.2.2") (default-features #t) (target "cfg(windows)") (kind 0)) (crate-dep (name "lazy_static") (req "^0.2.8") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2.23") (default-features #t) (kind 0)) (crate-dep (name "winapi") (req "^0.2.8") (default-features #t) (target "cfg(windows)") (kind 0)))) (hash "0ab2w0vb8hyqw279a0qpbv7yfx8xzg6hnfnv0hxkwah5zq34vp9b")))

(define-public crate-tvis_util-0.4 (crate (name "tvis_util") (vers "0.4.0") (deps (list (crate-dep (name "advapi32-sys") (req "^0.2.0") (default-features #t) (target "cfg(windows)") (kind 0)) (crate-dep (name "kernel32-sys") (req "^0.2.2") (default-features #t) (target "cfg(windows)") (kind 0)) (crate-dep (name "lazy_static") (req "^0.2.8") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2.23") (default-features #t) (kind 0)) (crate-dep (name "winapi") (req "^0.2.8") (default-features #t) (target "cfg(windows)") (kind 0)))) (hash "19k5jss5zi4jkfwvcrl1dd1rsmwk0vdfbkczvahr9vn0sbq386jy")))

(define-public crate-tvis_util-0.5 (crate (name "tvis_util") (vers "0.5.0") (deps (list (crate-dep (name "advapi32-sys") (req "^0.2.0") (default-features #t) (target "cfg(windows)") (kind 0)) (crate-dep (name "kernel32-sys") (req "^0.2.2") (default-features #t) (target "cfg(windows)") (kind 0)) (crate-dep (name "lazy_static") (req "^0.2.9") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2.32") (default-features #t) (kind 0)) (crate-dep (name "winapi") (req "^0.2.8") (default-features #t) (target "cfg(windows)") (kind 0)))) (hash "0zdk4vggpjr3syrhqcmlzc7x2mhr7b6h52kx7j64b3x72169098h")))

(define-public crate-tvis_util-0.5 (crate (name "tvis_util") (vers "0.5.2") (deps (list (crate-dep (name "advapi32-sys") (req "^0.2.0") (default-features #t) (target "cfg(windows)") (kind 0)) (crate-dep (name "kernel32-sys") (req "^0.2.2") (default-features #t) (target "cfg(windows)") (kind 0)) (crate-dep (name "lazy_static") (req "^0.2.9") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2.32") (default-features #t) (kind 0)) (crate-dep (name "winapi") (req "^0.2.8") (default-features #t) (target "cfg(windows)") (kind 0)))) (hash "0kfsfg4f7mm40my4xb2rmx7yh1cs0wd8s3kiyx48nvrf0rdmvlkj")))

