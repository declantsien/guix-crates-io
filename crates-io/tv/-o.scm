(define-module (crates-io tv -o) #:use-module (crates-io))

(define-public crate-tv-organizer-0.1 (crate (name "tv-organizer") (vers "0.1.1") (deps (list (crate-dep (name "clap") (req "^2") (default-features #t) (kind 0)) (crate-dep (name "crossbeam-channel") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "crossbeam-utils") (req "^0.7") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "tvdb") (req "^0.6") (default-features #t) (kind 0)))) (hash "0iqzp01brmvfkdrzcp68h8f385n6gjdjpb18b4l5p9577mbigpfg")))

