(define-module (crates-io wx ml) #:use-module (crates-io))

(define-public crate-wxml-0.1 (crate (name "wxml") (vers "0.1.0") (deps (list (crate-dep (name "wasm-bindgen") (req "^0.2") (default-features #t) (kind 0)))) (hash "0bbns4lwj3y6nmh5b3s33ncidvzb5g6rqhi7nvdfdjpzyc9px5j9")))

(define-public crate-wxml_parser-0.1 (crate (name "wxml_parser") (vers "0.1.0") (deps (list (crate-dep (name "wasm-bindgen") (req "^0.2") (default-features #t) (kind 0)))) (hash "0400qjz1nf3n19km51w1930zabyx3cibsrrbjc8wj8gkf976rd4y")))

