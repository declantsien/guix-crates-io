(define-module (crates-io wx pa) #:use-module (crates-io))

(define-public crate-wxpay-0.0.0 (crate (name "wxpay") (vers "0.0.0") (hash "11f0hikq17953l5flqa14ivdm3lqdaxyr8114v9plpp112kyyswh")))

(define-public crate-wxpay-sdk-rs-0.0.0 (crate (name "wxpay-sdk-rs") (vers "0.0.0") (hash "0dqlyq6cr2sfvsd2zi6fhc4jxw1ggcckpwrds6j1spin5943mfg6")))

