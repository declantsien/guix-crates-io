(define-module (crates-io wx ru) #:use-module (crates-io))

(define-public crate-wxrust-0.0.1 (crate (name "wxrust") (vers "0.0.1-alpha") (deps (list (crate-dep (name "cc") (req "^1.0.72") (default-features #t) (kind 1)) (crate-dep (name "wx-base") (req "^0.0.1-alpha") (default-features #t) (kind 0) (package "wxrust-base")) (crate-dep (name "wxrust-config") (req "^0.0.1-alpha2") (default-features #t) (kind 1)))) (hash "1m02ckgymhh5vrwbx2ipdd2p87h95nqflqg74cyc79gz6s63v438") (features (quote (("vendored" "wxrust-config/vendored"))))))

(define-public crate-wxrust-base-0.0.1 (crate (name "wxrust-base") (vers "0.0.1-alpha") (deps (list (crate-dep (name "cc") (req "^1.0.72") (default-features #t) (kind 1)) (crate-dep (name "wxrust-config") (req "^0.0.1-alpha2") (default-features #t) (kind 1)))) (hash "1akza35j3cra5i1zgnih5yhzhzm6c5v8ncjdldav8mqdafs13m77") (features (quote (("vendored" "wxrust-config/vendored"))))))

(define-public crate-wxrust-config-0.0.1 (crate (name "wxrust-config") (vers "0.0.1-alpha") (deps (list (crate-dep (name "cc") (req "^1.0.72") (default-features #t) (kind 0)))) (hash "19qdrnjhxkcrz8kv2wz2mdppcq7qhihysbn8q1rn57mkwy7snc37") (features (quote (("vendored"))))))

(define-public crate-wxrust-config-0.0.1 (crate (name "wxrust-config") (vers "0.0.1-alpha2") (deps (list (crate-dep (name "cc") (req "^1.0.72") (default-features #t) (kind 0)))) (hash "1lmxdn5pnzigmaz67hwgvqay3a0wb2xnsvxdbzwj9fydn6i034gw") (features (quote (("vendored"))))))

