(define-module (crates-io wx f-) #:use-module (crates-io))

(define-public crate-wxf-converter-0.3 (crate (name "wxf-converter") (vers "0.3.0") (deps (list (crate-dep (name "clap") (req "^2.27") (default-features #t) (kind 0)) (crate-dep (name "wolfram_wxf") (req "^0.2.2") (features (quote ("json" "pickle" "yaml"))) (kind 0)))) (hash "0w2qycbbhfzwg1747qyq4anf8qzrxrpy3fvfs10l1d2pwjly8gr6")))

(define-public crate-wxf-converter-0.3 (crate (name "wxf-converter") (vers "0.3.1") (deps (list (crate-dep (name "clap") (req "^3.1.6") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "wolfram_wxf") (req "^0.6.3") (features (quote ("json" "pickle" "yaml" "toml"))) (kind 0)))) (hash "1szw6d046ld6j6xn6f905i1zdpv74xaiv393skhlcfryrg9jdfzk")))

(define-public crate-wxf-converter-0.3 (crate (name "wxf-converter") (vers "0.3.2") (deps (list (crate-dep (name "clap") (req "^3.1.6") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "wolfram_wxf") (req "^0.6.3") (features (quote ("json" "pickle" "yaml" "toml"))) (kind 0)))) (hash "0qbphj75ysr42y87x8xc2w0wl4h5nrh3x4lhjr4yfp5j6ajy1b60")))

