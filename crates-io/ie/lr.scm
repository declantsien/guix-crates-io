(define-module (crates-io ie lr) #:use-module (crates-io))

(define-public crate-ielr-0.1 (crate (name "ielr") (vers "0.1.0") (deps (list (crate-dep (name "fnv") (req "^1.0.7") (default-features #t) (kind 0)))) (hash "1rhrgy12z897f2n1gr3xd65ld427bv3qrdca0p5dgrvh8hlcdaky") (rust-version "1.58")))

(define-public crate-ielr-0.1 (crate (name "ielr") (vers "0.1.1") (deps (list (crate-dep (name "fnv") (req "^1.0.5") (default-features #t) (kind 0)))) (hash "17wfldppfmdyxkxad3q5v71b057n86qh3h5xkzs0vms03i5wxkpc") (rust-version "1.58")))

