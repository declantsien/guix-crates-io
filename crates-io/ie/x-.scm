(define-module (crates-io ie x-) #:use-module (crates-io))

(define-public crate-iex-rs-0.1 (crate (name "iex-rs") (vers "0.1.0") (deps (list (crate-dep (name "log") (req "~0.4") (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.9.2") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "~1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "~1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "~1.0") (default-features #t) (kind 0)))) (hash "1cp5yy40fbnsdf7jm0n22mccdpig0gr3cylyibnc9gf6lynramn3")))

(define-public crate-iex-rs-0.1 (crate (name "iex-rs") (vers "0.1.1") (deps (list (crate-dep (name "log") (req "~0.4") (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.9.2") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "~1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "~1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "~1.0") (default-features #t) (kind 0)))) (hash "00kngjz5liyd1s6wjsz8d4c80h9icn6663p497r1ixfb59r0hz40")))

