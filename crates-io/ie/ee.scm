(define-module (crates-io ie ee) #:use-module (crates-io))

(define-public crate-ieee-apsqrt-0.1 (crate (name "ieee-apsqrt") (vers "0.1.0") (deps (list (crate-dep (name "rustc_apfloat") (req "^0.2.0") (default-features #t) (kind 0)))) (hash "0fhhy5z4bqifkp3mplbw73nlf3as0cm581z02qmx9y1d5j7b4drs")))

(define-public crate-ieee-apsqrt-0.1 (crate (name "ieee-apsqrt") (vers "0.1.1") (deps (list (crate-dep (name "rustc_apfloat") (req "^0.2.0") (default-features #t) (kind 0)))) (hash "1v3cbi6cd16i7rgcgy7sfj4a6bwk1vqx2322r8lfmbjaalf98a23")))

(define-public crate-ieee-registry-0.1 (crate (name "ieee-registry") (vers "0.1.0") (deps (list (crate-dep (name "expanduser") (req "^1.2.2") (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.11.23") (features (quote ("blocking"))) (default-features #t) (kind 0)) (crate-dep (name "tempfile") (req "^3.9.0") (default-features #t) (kind 2)) (crate-dep (name "tokio") (req "^1.35.1") (default-features #t) (kind 0)))) (hash "1yrn0pbbp3fns0f72wwhy1zl65fk2h3v72i36ha4z2sjdzk5vid1")))

(define-public crate-ieee-registry-0.1 (crate (name "ieee-registry") (vers "0.1.1") (deps (list (crate-dep (name "expanduser") (req "^1.2.2") (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.11.23") (features (quote ("blocking"))) (default-features #t) (kind 0)) (crate-dep (name "tempfile") (req "^3.9.0") (default-features #t) (kind 2)))) (hash "0rmqk04bnf8f7y6vbl80iwy9bdsv70mjgv935xsdwfj6wi6xa60w")))

(define-public crate-ieee-registry-0.2 (crate (name "ieee-registry") (vers "0.2.0") (deps (list (crate-dep (name "expanduser") (req "^1.2.2") (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.11.23") (features (quote ("blocking"))) (default-features #t) (kind 0)) (crate-dep (name "tempfile") (req "^3.9.0") (default-features #t) (kind 2)))) (hash "1lnwcg8hr72i4wd16zwn68fs3bzv75i15sqg24q9clw661819m8v")))

(define-public crate-ieee1212-config-rom-0.0.90 (crate (name "ieee1212-config-rom") (vers "0.0.90") (hash "1aqja7i9k5ihzfgyrwxy5dgh7cp8x9i01nrq5z5hr7rz8d1smn88") (yanked #t)))

(define-public crate-ieee1212-config-rom-0.1 (crate (name "ieee1212-config-rom") (vers "0.1.0") (hash "08z1dhmvrflx9la3fshfzn0sdkryzhajbd8x84r5fh9f24w8zx3i") (yanked #t)))

(define-public crate-ieee1212-config-rom-0.1 (crate (name "ieee1212-config-rom") (vers "0.1.1") (hash "0vhfbbnnxlx7rw8cc7r6maj34i0gql5qxyjmfgg1wz1qqx4wxb50") (yanked #t)))

(define-public crate-ieee1212-config-rom-0.1 (crate (name "ieee1212-config-rom") (vers "0.1.2") (hash "12m9ag2w2kn4a5d072f5sy8abjq4fjyiw5cd44c6657i4pc2l1bl")))

(define-public crate-ieee1275-0.1 (crate (name "ieee1275") (vers "0.1.0") (deps (list (crate-dep (name "compiler_builtins") (req "^0.1") (features (quote ("mem"))) (target "powerpc-unknown-linux-gnu") (kind 0)))) (hash "0j0ccbnvjmvwb603dx14k3w7gjxban5vh4h63235q7sc6305xq3d") (features (quote (("no_panic_handler") ("no_global_allocator"))))))

(define-public crate-ieee1275-0.1 (crate (name "ieee1275") (vers "0.1.1") (deps (list (crate-dep (name "compiler_builtins") (req "^0.1") (features (quote ("mem"))) (target "powerpc-unknown-linux-gnu") (kind 0)))) (hash "0z34mpkgr6hz4q30q4lgqdhi6s3fam943bc4797ck8m9vzgniniy") (features (quote (("no_panic_handler") ("no_global_allocator"))))))

(define-public crate-ieee754-0.1 (crate (name "ieee754") (vers "0.1.0") (hash "02hp891vzxwbrk1849l24kc06sjq23gc3159zx1qh99k2mz7xvmq") (features (quote (("unstable"))))))

(define-public crate-ieee754-0.1 (crate (name "ieee754") (vers "0.1.1") (hash "14xzm6k74mni1r8cckk9wq1fcjb6l1sjykv01q9xp55i3zg1c6bz") (features (quote (("unstable"))))))

(define-public crate-ieee754-0.2 (crate (name "ieee754") (vers "0.2.0") (hash "1pvgsf0g4rk5phs6jpwkv7h5w9spla3rzk7ng5kkazjc8hkcqmws") (features (quote (("unstable"))))))

(define-public crate-ieee754-0.2 (crate (name "ieee754") (vers "0.2.1") (hash "14sc4cr4b8czjjwz6ycmilzh6fbsfcn4zpn9c3jqcxf8y4ly62ab") (features (quote (("unstable"))))))

(define-public crate-ieee754-0.2 (crate (name "ieee754") (vers "0.2.2") (hash "1bafcqrms8a2frsjm3w3hjgiq3lwkmqrp6lcwa6zr5nx5vv7lir4") (features (quote (("unstable"))))))

(define-public crate-ieee754-0.2 (crate (name "ieee754") (vers "0.2.3") (deps (list (crate-dep (name "criterion") (req "^0.2") (default-features #t) (kind 2)))) (hash "0mj74359zf46hh3i60qfjslmdpni0hqm536d6d5nzb9xh9lzh5yz") (features (quote (("unstable"))))))

(define-public crate-ieee754-0.2 (crate (name "ieee754") (vers "0.2.4") (deps (list (crate-dep (name "criterion") (req "^0.2") (default-features #t) (kind 2)))) (hash "0g27wvry1plsia1f325s1f07p01v1vim8h36y7b5bn5rr2iq57y0") (features (quote (("unstable")))) (yanked #t)))

(define-public crate-ieee754-0.2 (crate (name "ieee754") (vers "0.2.5") (deps (list (crate-dep (name "criterion") (req "^0.2") (default-features #t) (kind 2)))) (hash "1zrfkck70lvdi40qfp7zwvf533fdih0j5hqmp5pcg4mzjsqqjqxg") (features (quote (("unstable"))))))

(define-public crate-ieee754-0.2 (crate (name "ieee754") (vers "0.2.6") (deps (list (crate-dep (name "criterion") (req "^0.2") (default-features #t) (kind 2)))) (hash "1771d2kvw1wga65yrg9m7maky0fzsaq9hvhkv91n6gmxmjfdl1wh") (features (quote (("unstable"))))))

(define-public crate-ieee754-0.3 (crate (name "ieee754") (vers "0.3.0-alpha.2") (deps (list (crate-dep (name "criterion") (req "^0.2") (default-features #t) (kind 2)) (crate-dep (name "rayon") (req "^1.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "rustc_version") (req "^0.2") (default-features #t) (kind 1)))) (hash "127ncx8wknxjs126jrpfyqzasx9iygg6vhh35kab68vh9sww1sq3") (features (quote (("unstable"))))))

(define-public crate-ieee80211-0.1 (crate (name "ieee80211") (vers "0.1.0") (deps (list (crate-dep (name "crc32fast") (req "^1.3.2") (kind 0)) (crate-dep (name "either") (req "^1.9.0") (kind 0)) (crate-dep (name "mac-parser") (req "^0.1.4") (default-features #t) (kind 0)) (crate-dep (name "macro-bits") (req "^0.1.4") (default-features #t) (kind 0)) (crate-dep (name "scroll") (req "^0.12.0") (kind 0)) (crate-dep (name "tlv-rs") (req "^0.2.3") (default-features #t) (kind 0)))) (hash "0sqicg2jwrjqdhh4pdj0ab53vy13j7hjshqbyyc05rvw3larn1lz")))

(define-public crate-ieee80211-0.1 (crate (name "ieee80211") (vers "0.1.1") (deps (list (crate-dep (name "crc32fast") (req "^1.3.2") (kind 0)) (crate-dep (name "criterion") (req "^0.5.1") (features (quote ("html_reports"))) (default-features #t) (kind 2)) (crate-dep (name "either") (req "^1.9.0") (kind 0)) (crate-dep (name "mac-parser") (req "^0.1.4") (default-features #t) (kind 0)) (crate-dep (name "macro-bits") (req "^0.1.4") (default-features #t) (kind 0)) (crate-dep (name "scroll") (req "^0.12.0") (kind 0)) (crate-dep (name "tlv-rs") (req "^0.2.3") (default-features #t) (kind 0)))) (hash "05swya6pjwn8k4y0lq849krlhj73ix49gs65m1d58ydchrp0kkiy")))

(define-public crate-ieee80211-0.1 (crate (name "ieee80211") (vers "0.1.3") (deps (list (crate-dep (name "bitfield-struct") (req "^0.6.0") (default-features #t) (kind 0)) (crate-dep (name "const_soft_float") (req "^0.1.4") (features (quote ("no_std"))) (default-features #t) (kind 0)) (crate-dep (name "crc32fast") (req "^1.3.2") (kind 0)) (crate-dep (name "criterion") (req "^0.5.1") (features (quote ("html_reports"))) (default-features #t) (kind 2)) (crate-dep (name "itertools") (req "^0.12.1") (default-features #t) (kind 0)) (crate-dep (name "mac-parser") (req "^0.1.4") (default-features #t) (kind 0)) (crate-dep (name "macro-bits") (req "^0.1.4") (default-features #t) (kind 0)) (crate-dep (name "scroll") (req "^0.12.0") (kind 0)) (crate-dep (name "tlv-rs") (req "^0.2.3") (default-features #t) (kind 0)))) (hash "1p22arj06jr156qz3pmmvxgi67qxx8mys2hia3q4xcx8irv9z2jh") (features (quote (("std") ("default" "std"))))))

(define-public crate-ieee80211-0.2 (crate (name "ieee80211") (vers "0.2.0") (deps (list (crate-dep (name "bitfield-struct") (req "^0.6.0") (default-features #t) (kind 0)) (crate-dep (name "const_soft_float") (req "^0.1.4") (features (quote ("no_std"))) (default-features #t) (kind 0)) (crate-dep (name "crc32fast") (req "^1.3.2") (kind 0)) (crate-dep (name "criterion") (req "^0.5.1") (features (quote ("html_reports"))) (default-features #t) (kind 2)) (crate-dep (name "mac-parser") (req "^0.1.4") (default-features #t) (kind 0)) (crate-dep (name "macro-bits") (req "^0.1.4") (default-features #t) (kind 0)) (crate-dep (name "scroll") (req "^0.12.0") (kind 0)) (crate-dep (name "tlv-rs") (req "^0.2.3") (default-features #t) (kind 0)))) (hash "1b0fyjn84f0npy84r9gqchhvb7312br43q3h31safbby9513701z") (features (quote (("std") ("default" "std"))))))

(define-public crate-ieee802154-0.1 (crate (name "ieee802154") (vers "0.1.0") (deps (list (crate-dep (name "byteorder") (req "^1") (kind 0)) (crate-dep (name "hash32") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "hash32-derive") (req "^0.1") (default-features #t) (kind 0)))) (hash "12yx5agvnsrd2cl3lrixnfdhva7crx5pnbmgmwhbs77kgwxgl6r3")))

(define-public crate-ieee802154-0.1 (crate (name "ieee802154") (vers "0.1.1") (deps (list (crate-dep (name "byteorder") (req "^1") (kind 0)) (crate-dep (name "hash32") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "hash32-derive") (req "^0.1") (default-features #t) (kind 0)))) (hash "0jmyxpgn7bydpryf752cdsn7l8jz7xh3fdniv3n12ylzsvzh1a1c")))

(define-public crate-ieee802154-0.2 (crate (name "ieee802154") (vers "0.2.0") (deps (list (crate-dep (name "byteorder") (req "^1") (kind 0)) (crate-dep (name "hash32") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "hash32-derive") (req "^0.1") (default-features #t) (kind 0)))) (hash "19yns9y8j5cbj03wmdlqnly50bjl0pgy1rvjz7v86imrdc9m6cdf")))

(define-public crate-ieee802154-0.3 (crate (name "ieee802154") (vers "0.3.0") (deps (list (crate-dep (name "byteorder") (req "^1") (kind 0)) (crate-dep (name "hash32") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "hash32-derive") (req "^0.1") (default-features #t) (kind 0)))) (hash "10fjf035wlxqfw41zgld2s85d927k2dm4hrdc79r74540102nqxg")))

(define-public crate-ieee802154-0.4 (crate (name "ieee802154") (vers "0.4.0") (deps (list (crate-dep (name "bytes") (req "^0.5") (kind 0)) (crate-dep (name "hash32") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "hash32-derive") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "static-bytes") (req "^0.1") (default-features #t) (kind 2)))) (hash "1zfypm0v4ykn67hb2mawbpn87sxj1b74whw2xkrdif6zl127bgi0")))

(define-public crate-ieee802154-0.5 (crate (name "ieee802154") (vers "0.5.0") (deps (list (crate-dep (name "byte") (req "^0.2.4") (default-features #t) (kind 0)) (crate-dep (name "hash32") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "hash32-derive") (req "^0.1") (default-features #t) (kind 0)))) (hash "0fmyyjh3l9gbw8v7m9f81xc03x843l77paz70b7a3fwsr43r3bqg")))

(define-public crate-ieee802154-0.5 (crate (name "ieee802154") (vers "0.5.1") (deps (list (crate-dep (name "aes") (req "^0.7.0") (kind 2)) (crate-dep (name "byte") (req "^0.2.4") (default-features #t) (kind 0)) (crate-dep (name "ccm") (req "^0.4.0") (kind 0)) (crate-dep (name "cipher") (req "^0.3.0") (kind 0)) (crate-dep (name "defmt") (req ">=0.2.0, <0.4") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "hash32") (req "^0.2.1") (default-features #t) (kind 0)) (crate-dep (name "hash32-derive") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.3") (default-features #t) (kind 2)))) (hash "101j32f2h34rff2hfr8xawk7dv41dh84y36xclmql3igh6ancrwp") (yanked #t)))

(define-public crate-ieee802154-0.6 (crate (name "ieee802154") (vers "0.6.0") (deps (list (crate-dep (name "aes") (req "^0.7.0") (kind 2)) (crate-dep (name "byte") (req "^0.2.4") (default-features #t) (kind 0)) (crate-dep (name "ccm") (req "^0.4.0") (kind 0)) (crate-dep (name "cipher") (req "^0.3.0") (kind 0)) (crate-dep (name "defmt") (req ">=0.2.0, <0.4") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "hash32") (req "^0.2.1") (default-features #t) (kind 0)) (crate-dep (name "hash32-derive") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.3") (default-features #t) (kind 2)))) (hash "0m6riv06x865zrslkys0jgvpccy0034073c4blg7y2rkm5lnvxxb")))

(define-public crate-ieee802154-0.6 (crate (name "ieee802154") (vers "0.6.1") (deps (list (crate-dep (name "aes") (req "^0.7.0") (kind 2)) (crate-dep (name "byte") (req "^0.2.4") (default-features #t) (kind 0)) (crate-dep (name "ccm") (req "^0.4.0") (kind 0)) (crate-dep (name "cipher") (req "^0.3.0") (kind 0)) (crate-dep (name "defmt") (req ">=0.2.0, <0.4") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "hash32") (req "^0.2.1") (default-features #t) (kind 0)) (crate-dep (name "hash32-derive") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.3") (default-features #t) (kind 2)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (optional #t) (kind 0)))) (hash "19ahnhvy8c57m17mb0b5azw6cal572rjmbhrvfahf6105zk6vjsg")))

(define-public crate-ieee802_3_mii-0.1 (crate (name "ieee802_3_mii") (vers "0.1.0") (deps (list (crate-dep (name "bitflags") (req "^1.3") (default-features #t) (kind 0)) (crate-dep (name "num_enum") (req "^0.5") (default-features #t) (kind 0)))) (hash "1ahcz90rz5rlcfk2cvzi06kwlb54rbf6v42v25a32acbp3axavas") (yanked #t)))

(define-public crate-ieee802_3_mii-0.2 (crate (name "ieee802_3_mii") (vers "0.2.0-ALPHA1") (deps (list (crate-dep (name "bitflags") (req "^1.3") (default-features #t) (kind 0)))) (hash "0igdy00hcbwyify2y6qz55h9mkk62fma2pgzy6ix7s3fc9r468xp") (yanked #t)))

(define-public crate-ieee802_3_mii-0.2 (crate (name "ieee802_3_mii") (vers "0.2.0") (deps (list (crate-dep (name "bitflags") (req "^1.3") (default-features #t) (kind 0)))) (hash "1ij6k0nxr1f4h0b6p0sh8xq0y7gc34i9c2n88lvvlpnay2f3nrd6") (features (quote (("phy") ("lan8742a" "phy") ("lan8720a" "phy") ("kzs8081r" "phy") ("default" "lan8720a" "lan8742a" "kzs8081r")))) (yanked #t)))

(define-public crate-ieee802_3_mii-0.3 (crate (name "ieee802_3_mii") (vers "0.3.0") (hash "0vv416c7q9y7904bwwbzi47vqqf53m8rrn3jcxvcd719vk8cc0xj") (yanked #t)))

(define-public crate-ieee802_3_mii-0.3 (crate (name "ieee802_3_mii") (vers "0.3.1") (hash "0xwf0xyfsmarr0mphpn6yv7wiy6x0n8rbp4ag3i9wcpvhppnxs7g")))

(define-public crate-ieee802_3_miim-0.3 (crate (name "ieee802_3_miim") (vers "0.3.0-ALPHA1") (deps (list (crate-dep (name "bitflags") (req "^1.3") (default-features #t) (kind 0)))) (hash "1jhssinzbxq2hrkwqxkl5nv6a53kyiz3zh9hp69psr1l7hzszm4k") (features (quote (("phy") ("lan8742a" "phy") ("lan8720a" "phy") ("kzs8081r" "phy") ("default" "lan8720a" "lan8742a" "kzs8081r"))))))

(define-public crate-ieee802_3_miim-0.3 (crate (name "ieee802_3_miim") (vers "0.3.0-ALPHA2") (deps (list (crate-dep (name "bitflags") (req "^1.3") (default-features #t) (kind 0)))) (hash "0cp7z7czwilj6bjzp3yh8rn4j1kmrivcnj20czjnjlnlfl8isa9q") (features (quote (("phy") ("lan8742a" "phy") ("lan8720a" "phy") ("kzs8081r" "phy") ("default" "lan8720a" "lan8742a" "kzs8081r"))))))

(define-public crate-ieee802_3_miim-0.3 (crate (name "ieee802_3_miim") (vers "0.3.0-ALPHA3") (deps (list (crate-dep (name "bitflags") (req "^1.3") (default-features #t) (kind 0)))) (hash "1ry4xr71b7ll96hwbzg4kkx0bn8kgwl77634vd370k2w20s7gyp1") (features (quote (("phy") ("lan8742a" "phy") ("lan8720a" "phy") ("kzs8081r" "phy") ("default" "lan8720a" "lan8742a" "kzs8081r"))))))

(define-public crate-ieee802_3_miim-0.3 (crate (name "ieee802_3_miim") (vers "0.3.0-ALPHA4") (deps (list (crate-dep (name "bitflags") (req "^1.3") (default-features #t) (kind 0)))) (hash "1f6wpc3sgfz7chxq45zrk5w4dxz2awbvfm57ar0mdnspbq6mahgp") (features (quote (("phy") ("lan8742a" "phy") ("lan8720a" "phy") ("kzs8081r" "phy") ("default" "lan8720a" "lan8742a" "kzs8081r"))))))

(define-public crate-ieee802_3_miim-0.4 (crate (name "ieee802_3_miim") (vers "0.4.0") (deps (list (crate-dep (name "bitflags") (req "^1.3") (default-features #t) (kind 0)))) (hash "16ipm4mk4wx46l6f94ihwi0azyv1w9h1zqj989rpkcfh14xhi199") (features (quote (("phy") ("lan8742a" "phy") ("lan8720a" "phy") ("kzs8081r" "phy") ("default" "lan8720a" "lan8742a" "kzs8081r"))))))

(define-public crate-ieee802_3_miim-0.4 (crate (name "ieee802_3_miim") (vers "0.4.1") (deps (list (crate-dep (name "bitflags") (req "^1.3") (default-features #t) (kind 0)))) (hash "1sndvb0cjha931b6k6766nvsdsfah2kidpws9rcdhpk54md1call") (features (quote (("phy") ("lan8742a" "phy") ("lan8720a" "phy") ("kzs8081r" "phy") ("default" "lan8720a" "lan8742a" "kzs8081r"))))))

(define-public crate-ieee802_3_miim-0.5 (crate (name "ieee802_3_miim") (vers "0.5.0") (deps (list (crate-dep (name "bitflags") (req "^1.3") (default-features #t) (kind 0)) (crate-dep (name "defmt") (req "^0.3") (default-features #t) (kind 0)))) (hash "0xdd7mz98p8mjb0g717n49f196m11w1ybn2nlsargjy87a6cfrp7") (features (quote (("phy") ("lan8742a" "phy") ("lan8720a" "phy") ("kzs8081r" "phy") ("default" "lan8720a" "lan8742a" "kzs8081r"))))))

(define-public crate-ieee802_3_miim-0.6 (crate (name "ieee802_3_miim") (vers "0.6.0") (deps (list (crate-dep (name "bitflags") (req "^1.3") (default-features #t) (kind 0)) (crate-dep (name "defmt") (req "^0.3") (optional #t) (default-features #t) (kind 0)))) (hash "0y5gnl7670637pq2y8738zbdk34m6admqhisl4vfw1wylmf4isyd") (features (quote (("phy") ("mmd") ("lan8742a" "phy" "mmd") ("lan8720a" "phy" "mmd") ("kzs8081r" "phy") ("default" "lan8720a" "lan8742a" "kzs8081r"))))))

(define-public crate-ieee802_3_miim-0.7 (crate (name "ieee802_3_miim") (vers "0.7.0") (deps (list (crate-dep (name "bitflags") (req "^1.3") (default-features #t) (kind 0)) (crate-dep (name "defmt") (req "^0.3") (optional #t) (default-features #t) (kind 0)))) (hash "1wqps449icja962sw8mi693yjrk34lfakknfxqzx2h9nnwf2np8l") (features (quote (("phy") ("mmd") ("lan8742a" "phy" "mmd") ("lan8720a" "phy" "mmd") ("kzs8081r" "phy") ("default" "lan8720a" "lan8742a" "kzs8081r"))))))

(define-public crate-ieee802_3_miim-0.7 (crate (name "ieee802_3_miim") (vers "0.7.1") (deps (list (crate-dep (name "bitflags") (req "^1.3") (default-features #t) (kind 0)) (crate-dep (name "defmt") (req "^0.3") (optional #t) (default-features #t) (kind 0)))) (hash "12j32sgjvbk45rzvb68gxf2fhhfxw13s2vr1nfqfikrfm050q777") (features (quote (("phy") ("mmd") ("lan8742a" "phy" "mmd") ("lan8720a" "phy" "mmd") ("ksz8081r" "phy") ("default" "lan8720a" "lan8742a" "ksz8081r")))) (yanked #t)))

(define-public crate-ieee802_3_miim-0.7 (crate (name "ieee802_3_miim") (vers "0.7.2") (deps (list (crate-dep (name "bitflags") (req "^1.3") (default-features #t) (kind 0)) (crate-dep (name "defmt") (req "^0.3") (optional #t) (default-features #t) (kind 0)))) (hash "0gj4wdfklyizb0jc3dz21fb0lwbz3lkskbd854qzzk5gzslyjvzs") (features (quote (("phy") ("mmd") ("lan8742a" "phy" "mmd") ("lan8720a" "phy" "mmd") ("kzs8081r" "phy") ("default" "lan8720a" "lan8742a" "kzs8081r"))))))

(define-public crate-ieee802_3_miim-0.8 (crate (name "ieee802_3_miim") (vers "0.8.0") (deps (list (crate-dep (name "bitflags") (req "^1.3") (default-features #t) (kind 0)) (crate-dep (name "defmt") (req "^0.3") (optional #t) (default-features #t) (kind 0)))) (hash "0a6lzz7mbr8zm1dg8jni12qb6c2gsydf2yqa1pfr8avspkl23ppv") (features (quote (("phy") ("mmd") ("lan8742a" "phy" "mmd") ("lan8720a" "phy" "mmd") ("ksz8081r" "phy") ("default" "lan8720a" "lan8742a" "ksz8081r"))))))

