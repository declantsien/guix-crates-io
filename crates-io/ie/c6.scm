(define-module (crates-io ie c6) #:use-module (crates-io))

(define-public crate-iec60909-0.1 (crate (name "iec60909") (vers "0.1.0") (deps (list (crate-dep (name "anyhow") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "approx") (req "^0.5") (default-features #t) (kind 0)) (crate-dep (name "derive_builder") (req "^0.12") (default-features #t) (kind 0)) (crate-dep (name "num-complex") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "sparsetools") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "spsolve") (req "^0.1") (features (quote ("rlu"))) (default-features #t) (kind 0)))) (hash "0fakcj5fl9vkd68ny9nx29sp6cha44l928p6qm2dj6s9lr7d5hvi")))

