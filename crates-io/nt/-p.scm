(define-module (crates-io nt -p) #:use-module (crates-io))

(define-public crate-nt-packet-0.1 (crate (name "nt-packet") (vers "0.1.0") (deps (list (crate-dep (name "bytes") (req "^0.4.9") (default-features #t) (kind 0)) (crate-dep (name "nt-leb128") (req "^0.2.4") (features (quote ("use-bytes"))) (kind 0)))) (hash "178yrlbhhfrjcji12v2r957wib0kga1vhwqxzzc0a2g10jjfd9v2")))

(define-public crate-nt-packet-0.1 (crate (name "nt-packet") (vers "0.1.1") (deps (list (crate-dep (name "bytes") (req "^0.4.9") (default-features #t) (kind 0)) (crate-dep (name "failure") (req "^0.1.2") (default-features #t) (kind 0)) (crate-dep (name "nt-leb128") (req "^0.2.4") (features (quote ("use-bytes"))) (kind 0)))) (hash "00w2s9h3lx64ivxxk7nbx9kkq3sjf5zk8hrby94cz5p5jasxllpg")))

(define-public crate-nt-packet-0.1 (crate (name "nt-packet") (vers "0.1.2") (deps (list (crate-dep (name "bytes") (req "^0.4.9") (default-features #t) (kind 0)) (crate-dep (name "failure") (req "^0.1.2") (default-features #t) (kind 0)) (crate-dep (name "nt-leb128") (req "^0.2.4") (features (quote ("use-bytes"))) (kind 0)))) (hash "04qnk8glb8fi4qzp57v7r8f9lf7m21m7gzg80jp7x4gqijfn5pfh")))

(define-public crate-nt-packet-0.1 (crate (name "nt-packet") (vers "0.1.3") (deps (list (crate-dep (name "bytes") (req "^0.4.9") (default-features #t) (kind 0)) (crate-dep (name "failure") (req "^0.1.2") (default-features #t) (kind 0)) (crate-dep (name "nt-leb128") (req "^0.2.4") (features (quote ("use-bytes"))) (kind 0)))) (hash "1hcw110c9x4gk8p00s5sr7kbrlaizv1x0cnnrp07vmi4j5m2njgz") (yanked #t)))

(define-public crate-nt-packet-derive-0.1 (crate (name "nt-packet-derive") (vers "0.1.0") (deps (list (crate-dep (name "nt-packet") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^0.4.9") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^0.6.5") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^0.14.7") (default-features #t) (kind 0)))) (hash "0m65ss79lx4fby36d5kxbiqlx00jkzhv0f10sx70nz7al6lqa8h2") (yanked #t)))

(define-public crate-nt-packet-derive-0.1 (crate (name "nt-packet-derive") (vers "0.1.1") (deps (list (crate-dep (name "nt-packet") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^0.4.9") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^0.6.5") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^0.14.7") (default-features #t) (kind 0)))) (hash "0rymmgaqf9qg5s4abaz1lh3698pqd5gw69cbd446sah2ls0yy4fn")))

(define-public crate-nt-packet-derive-0.1 (crate (name "nt-packet-derive") (vers "0.1.2") (deps (list (crate-dep (name "nt-packet") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^0.4.9") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^0.6.5") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^0.14.7") (default-features #t) (kind 0)))) (hash "0wka642lsbqil29465vaw8636lpcbhr1y2ajl0p1hf9z847jkbg5")))

(define-public crate-nt-packet-derive-0.1 (crate (name "nt-packet-derive") (vers "0.1.3") (deps (list (crate-dep (name "nt-packet") (req "^0.1.1") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^0.4.9") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^0.6.5") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^0.14.7") (default-features #t) (kind 0)))) (hash "0l1g3ycq2bjxq3yd7x48qfhk9arvi5xdmj3ygy26v91x3dhdngph")))

(define-public crate-nt-packet-derive-0.1 (crate (name "nt-packet-derive") (vers "0.1.4") (deps (list (crate-dep (name "nt-packet") (req "^0.1.2") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^0.4.9") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^0.6.5") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^0.14.7") (default-features #t) (kind 0)))) (hash "17zq3wfb9p5n66lmvyzw9jf77npmwsmxmd9n8yn98i190fk7w6zx") (yanked #t)))

(define-public crate-nt-primes-0.0.0 (crate (name "nt-primes") (vers "0.0.0") (deps (list (crate-dep (name "number-theory") (req "^0.0.4") (default-features #t) (kind 0)))) (hash "00jkwrrhyawsybmxjlixarlxd8bv2d6z289qly2v0ml0wqmcs96g")))

(define-public crate-nt-primes-0.1 (crate (name "nt-primes") (vers "0.1.0") (deps (list (crate-dep (name "number-theory") (req "^0.0.9") (default-features #t) (kind 0)))) (hash "1m1hw1d03k6mqh8kq2hrcbjlp0lkfk6shi46v78apa6r3qy37nnc")))

(define-public crate-nt-primes-0.1 (crate (name "nt-primes") (vers "0.1.1") (deps (list (crate-dep (name "number-theory") (req "^0.0.24") (default-features #t) (kind 0)))) (hash "02yr2l81xp73vfrcizsd4ph42sgjlmsfk9clwiqsrkhb2m6rbjcq")))

