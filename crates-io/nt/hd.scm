(define-module (crates-io nt hd) #:use-module (crates-io))

(define-public crate-nthD_Matrix-0.1 (crate (name "nthD_Matrix") (vers "0.1.0") (deps (list (crate-dep (name "num-traits") (req "^0.2.15") (default-features #t) (kind 0)))) (hash "02s8ignl3589dqlm4zk9w34x7pfr9n9ihn9fpc6gwdizz3rji9q8")))

(define-public crate-nthD_Matrix-0.1 (crate (name "nthD_Matrix") (vers "0.1.1") (deps (list (crate-dep (name "num-traits") (req "^0.2.15") (default-features #t) (kind 0)))) (hash "0rfk4w5fzj9q4jxrghfilv8g8wnw5r8fqp19p6shsw67cnsqnbzq")))

(define-public crate-nthD_Matrix-0.1 (crate (name "nthD_Matrix") (vers "0.1.2") (deps (list (crate-dep (name "num-traits") (req "^0.2.15") (default-features #t) (kind 0)))) (hash "16wlr6w3i06xzr71svnbkvir9lhlrd401spv9yjx9110nwpznhbd")))

