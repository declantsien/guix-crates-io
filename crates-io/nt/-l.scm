(define-module (crates-io nt -l) #:use-module (crates-io))

(define-public crate-nt-leb128-0.2 (crate (name "nt-leb128") (vers "0.2.4") (deps (list (crate-dep (name "bytes") (req "^0.4.9") (default-features #t) (kind 0)))) (hash "1h4rizd2gshgy0jaxrwmy0gwr7jl320rdac4chgyix049zf1zv4x") (features (quote (("use-bytes") ("std") ("nightly") ("default" "std"))))))

(define-public crate-nt-leb128-0.3 (crate (name "nt-leb128") (vers "0.3.0") (deps (list (crate-dep (name "bytes") (req "^0.5.2") (default-features #t) (kind 0)))) (hash "18g8ysw100v6zh602fplmjf1b7n7dhzgiv22vcxynx7lqcn1zqwq")))

(define-public crate-nt-leb128-0.3 (crate (name "nt-leb128") (vers "0.3.1") (deps (list (crate-dep (name "bytes") (req "^0.5.2") (default-features #t) (kind 0)))) (hash "0iassqbnxx6pnq51prk59li58j6pmvncafavm53f0lhg0niaf5h1")))

(define-public crate-nt-list-0.1 (crate (name "nt-list") (vers "0.1.0") (deps (list (crate-dep (name "moveit") (req "^0.5.0") (default-features #t) (kind 0)) (crate-dep (name "nt-list_macros") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "199sxb6r0sdw4xh6d7klcbyy07m1rbqnczgx9spw56kggnfgavl2") (features (quote (("default" "alloc") ("alloc")))) (rust-version "1.56")))

(define-public crate-nt-list-0.2 (crate (name "nt-list") (vers "0.2.0") (deps (list (crate-dep (name "moveit") (req "^0.5.0") (default-features #t) (kind 0)) (crate-dep (name "nt-list_macros") (req "^0.2.0") (default-features #t) (kind 0)))) (hash "0syfdxbp8gq2i3rmq3w98c3sm5bnw26q32yqi9x1l9br727mvmqx") (features (quote (("default" "alloc") ("alloc")))) (rust-version "1.56")))

(define-public crate-nt-list-0.2 (crate (name "nt-list") (vers "0.2.1") (deps (list (crate-dep (name "moveit") (req "^0.5.0") (default-features #t) (kind 0)) (crate-dep (name "nt-list_macros") (req "^0.2.0") (default-features #t) (kind 0)))) (hash "1273a16vccan9792d0xnfx5j1bpigi2yrff29z106ccjilm4xw9m") (features (quote (("default" "alloc") ("alloc")))) (rust-version "1.56")))

(define-public crate-nt-list-0.3 (crate (name "nt-list") (vers "0.3.0") (deps (list (crate-dep (name "moveit") (req "^0.6.0") (default-features #t) (kind 0)) (crate-dep (name "nt-list_macros") (req "^0.3.0") (default-features #t) (kind 0)))) (hash "1s2rcizxn7a6r41336b7cjx1fr0a58ivjxlfkkm8paaq2y1xsyjx") (features (quote (("default" "alloc") ("alloc")))) (rust-version "1.56")))

(define-public crate-nt-list_macros-0.1 (crate (name "nt-list_macros") (vers "0.1.0") (deps (list (crate-dep (name "proc-macro2") (req "^1.0.40") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.20") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.98") (features (quote ("derive" "parsing" "printing" "proc-macro"))) (kind 0)))) (hash "000p6rc3l0a3jkx1g9awy3cd5zc42vd6ygff04zypjk3xzr81n8l") (rust-version "1.56")))

(define-public crate-nt-list_macros-0.2 (crate (name "nt-list_macros") (vers "0.2.0") (deps (list (crate-dep (name "proc-macro2") (req "^1.0.40") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.20") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.98") (features (quote ("derive" "parsing" "printing" "proc-macro"))) (kind 0)))) (hash "0jrmq265a3dw0ah7a5z7vkmqs5xmwdpqszvz05yg8xndi5fg5l0w") (rust-version "1.56")))

(define-public crate-nt-list_macros-0.3 (crate (name "nt-list_macros") (vers "0.3.0") (deps (list (crate-dep (name "proc-macro2") (req "^1.0.69") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.33") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2.0.38") (features (quote ("derive" "parsing" "printing" "proc-macro"))) (kind 0)))) (hash "0l8lwkn1gv0ap4vnmzbgymzn6wh7s7nzz5lszkng4q9khabpl09z") (rust-version "1.56")))

