(define-module (crates-io nt ha) #:use-module (crates-io))

(define-public crate-nthash-0.2 (crate (name "nthash") (vers "0.2.0") (deps (list (crate-dep (name "criterion") (req "^0.2") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.5") (default-features #t) (kind 2)))) (hash "0zsmmr7qdy03pfvlgsjvjh9mahzwjrs2nvd7d4anrbfd7bsc61n1")))

(define-public crate-nthash-0.2 (crate (name "nthash") (vers "0.2.1") (deps (list (crate-dep (name "criterion") (req "^0.2") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.5") (default-features #t) (kind 2)))) (hash "1jzx9i1g5nandhh7wdcq7cygb3phgr3wzp0z8l3l8717cmp1kanf")))

(define-public crate-nthash-0.2 (crate (name "nthash") (vers "0.2.2") (deps (list (crate-dep (name "criterion") (req "^0.2") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.5") (default-features #t) (kind 2)))) (hash "0ib3f6mzfaf442p50zamv8rsays0aj78bv41i1rlzg67r3an2raw")))

(define-public crate-nthash-0.3 (crate (name "nthash") (vers "0.3.0") (deps (list (crate-dep (name "criterion") (req "^0.2") (default-features #t) (kind 2)) (crate-dep (name "quickcheck") (req "^0.7.1") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.5") (default-features #t) (kind 2)))) (hash "0y4p17f84l0y7d06jrwgpfj5hs5gqqniy5ilnvgznglif2140a2v")))

(define-public crate-nthash-0.4 (crate (name "nthash") (vers "0.4.0") (deps (list (crate-dep (name "criterion") (req "^0.2") (default-features #t) (kind 2)) (crate-dep (name "error-chain") (req "^0.12.0") (default-features #t) (kind 0)) (crate-dep (name "quickcheck") (req "^0.7.1") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.5") (default-features #t) (kind 2)))) (hash "0x6vick243zl8s3ncv1159gngi8w2444c2ac0ymkj152parxb9d1")))

(define-public crate-nthash-0.4 (crate (name "nthash") (vers "0.4.1") (deps (list (crate-dep (name "criterion") (req "^0.2") (default-features #t) (kind 2)) (crate-dep (name "error-chain") (req "^0.12.0") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.1.0") (default-features #t) (kind 0)) (crate-dep (name "quickcheck") (req "^0.7.1") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.5") (default-features #t) (kind 2)))) (hash "15fyrkk9wppp44jxwwb6b9rmllkb1kirxpyc4n91p7ngzfyabhss")))

(define-public crate-nthash-0.4 (crate (name "nthash") (vers "0.4.3") (deps (list (crate-dep (name "criterion") (req "^0.2") (default-features #t) (kind 2)) (crate-dep (name "error-chain") (req "^0.12.0") (default-features #t) (kind 0)) (crate-dep (name "quickcheck") (req "^0.7.1") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.5") (default-features #t) (kind 2)))) (hash "0qxnrflx6wbqp8bzddl5n4sifq6h36qvind4yd97sa9kfrf7830m")))

(define-public crate-nthash-0.5 (crate (name "nthash") (vers "0.5.0") (deps (list (crate-dep (name "criterion") (req "^0.2") (default-features #t) (kind 2)) (crate-dep (name "quickcheck") (req "^0.7.1") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.5") (default-features #t) (kind 2)) (crate-dep (name "thiserror") (req "^1.0") (default-features #t) (kind 0)))) (hash "0dvipw8gl3vqw5gbvj8w5qrxzq0iqskqq2pd1yzh5525v7768mnz")))

(define-public crate-nthash-0.5 (crate (name "nthash") (vers "0.5.1") (deps (list (crate-dep (name "criterion") (req "^0.2") (default-features #t) (kind 2)) (crate-dep (name "quickcheck") (req "^0.7.1") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.5") (default-features #t) (kind 2)) (crate-dep (name "thiserror") (req "^1.0") (default-features #t) (kind 0)))) (hash "1l62hvjnrwlqm9834zck7a8l77vv222gdkbg2siinflqcngdabkm")))

