(define-module (crates-io nt _v) #:use-module (crates-io))

(define-public crate-nt_version-0.1 (crate (name "nt_version") (vers "0.1.0") (deps (list (crate-dep (name "winapi") (req "^0.3") (features (quote ("libloaderapi"))) (optional #t) (default-features #t) (kind 0)))) (hash "0zfjy3h795911qisccskkkbcpa13ca6wcx9rfgcm3k5fyjcmxwwm") (features (quote (("fallback" "winapi") ("default")))) (yanked #t)))

(define-public crate-nt_version-0.1 (crate (name "nt_version") (vers "0.1.1") (deps (list (crate-dep (name "winapi") (req "^0.3") (features (quote ("libloaderapi"))) (optional #t) (default-features #t) (kind 0)))) (hash "0cvwk6inpq3l34i8z0hz43v4bncaiclswf9r4chz77flam5g18l3") (features (quote (("fallback" "winapi") ("default")))) (yanked #t)))

(define-public crate-nt_version-0.1 (crate (name "nt_version") (vers "0.1.2") (deps (list (crate-dep (name "winapi") (req "^0.3") (features (quote ("libloaderapi"))) (optional #t) (default-features #t) (kind 0)))) (hash "0kxnfc9dwldlkh4kbf9k8pbrvdmsjb92wpysp5j1827p8j9g9pc6") (features (quote (("fallback" "winapi") ("default")))) (yanked #t)))

(define-public crate-nt_version-0.1 (crate (name "nt_version") (vers "0.1.3") (deps (list (crate-dep (name "winapi") (req "^0.3") (features (quote ("libloaderapi"))) (optional #t) (default-features #t) (kind 0)))) (hash "19hl3ggrv1732z4l8cf78xcimfgm4j0jl1agr6aiin81dbyv73cm") (features (quote (("fallback" "winapi") ("default"))))))

