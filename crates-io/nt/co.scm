(define-module (crates-io nt co) #:use-module (crates-io))

(define-public crate-ntcore-sys-0.1 (crate (name "ntcore-sys") (vers "0.1.0") (deps (list (crate-dep (name "bindgen") (req "^0.33") (default-features #t) (kind 1)))) (hash "0m12zmmsvkz2271my6v2risa143nzagd506ciw2j916f5hncsf5k")))

(define-public crate-ntcore-sys-0.1 (crate (name "ntcore-sys") (vers "0.1.1") (deps (list (crate-dep (name "bindgen") (req "^0.33") (default-features #t) (kind 1)))) (hash "0aakcfiawaz94h3pxqd699fjf66p8nymxlc0dqi3aimbqwkb882h")))

