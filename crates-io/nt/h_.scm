(define-module (crates-io nt h_) #:use-module (crates-io))

(define-public crate-nth_rs-0.1 (crate (name "nth_rs") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^2.33.3") (default-features #t) (kind 0)))) (hash "0i3zg78gr619p1ckcpcrc3wp7mvqrgy984a70g8shs3hfip5x5mb")))

(define-public crate-nth_rs-0.1 (crate (name "nth_rs") (vers "0.1.1") (deps (list (crate-dep (name "clap") (req "^2.33.3") (default-features #t) (kind 0)))) (hash "1s8q4x0vpr206z5ipyvjdxa2qjh7nlbgrm3s748gxnq7dnkww4im")))

(define-public crate-nth_rs-0.1 (crate (name "nth_rs") (vers "0.1.2") (deps (list (crate-dep (name "clap") (req "^2.33.3") (default-features #t) (kind 0)))) (hash "1w60xba1vc2jg765ypn5zrg4p8saxlak8wm6jhis132vzjxkr5kv")))

(define-public crate-nth_rs-0.1 (crate (name "nth_rs") (vers "0.1.3") (deps (list (crate-dep (name "clap") (req "^2.33.3") (default-features #t) (kind 0)))) (hash "12q1pz4l2kiykjqyfddx7lj649nmgqfy8n3q2f4002f2vygxkpf0")))

(define-public crate-nth_rs-0.1 (crate (name "nth_rs") (vers "0.1.4") (deps (list (crate-dep (name "clap") (req "^2.33.3") (default-features #t) (kind 0)))) (hash "03vikhlrc0n98wrdncppcg1bvs2nng9jqkpbmwk8f035yg6cclzy")))

(define-public crate-nth_rs-0.2 (crate (name "nth_rs") (vers "0.2.0") (deps (list (crate-dep (name "byte_string") (req "^1.0.0") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^2.33.3") (default-features #t) (kind 0)))) (hash "0sxwsxiqdjprgn8j3d1dx5i2s79wsks4bihxqbs184h4kchw8kzp")))

(define-public crate-nth_rs-0.2 (crate (name "nth_rs") (vers "0.2.1") (deps (list (crate-dep (name "byte_lines") (req "^0.1.1") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^2.33.3") (default-features #t) (kind 0)))) (hash "0g8h8x16q2kk869jr21m0frha9n90sw17wkkfw6p8q3q3ysgksp0")))

(define-public crate-nth_rs-0.2 (crate (name "nth_rs") (vers "0.2.2") (deps (list (crate-dep (name "byte_lines") (req "^0.1.1") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^2.33.3") (default-features #t) (kind 0)))) (hash "12gqlwqvnn732m0391xj9nc83gjmyn3bnqvwj1lbl9hkbzs9n8fy")))

