(define-module (crates-io nt pa) #:use-module (crates-io))

(define-public crate-ntpalert-0.1 (crate (name "ntpalert") (vers "0.1.1") (deps (list (crate-dep (name "chrono") (req "^0.4.11") (default-features #t) (kind 0)) (crate-dep (name "sntpc") (req "^0.2.0") (default-features #t) (kind 0)))) (hash "1p2aksnc1vv0rw7afgxnn1y1wnpjak924smgd74cx6lg0h01jdrq")))

