(define-module (crates-io nt dl) #:use-module (crates-io))

(define-public crate-ntdll-0.0.1 (crate (name "ntdll") (vers "0.0.1") (deps (list (crate-dep (name "winapi") (req "^0.3.5") (features (quote ("ntdef"))) (default-features #t) (kind 0)))) (hash "1m6lw3wp9jxzmlf1v9mn824ab4jly8fdhszjqnz525cgyrf247vb") (links "ntdll")))

(define-public crate-ntdll-0.0.2 (crate (name "ntdll") (vers "0.0.2") (deps (list (crate-dep (name "winapi") (req "^0.3.5") (features (quote ("ntdef"))) (default-features #t) (kind 0)))) (hash "0k95mpxg8zdxwifrgyi6c5s3wywinbrkydr59sa04ivwayzn666k") (links "ntdll")))

(define-public crate-ntdll-0.0.3 (crate (name "ntdll") (vers "0.0.3") (deps (list (crate-dep (name "winapi") (req "^0.3.5") (features (quote ("ntdef"))) (default-features #t) (kind 0)))) (hash "1clv17bg3qw36mjv9nb3mjpks6aqb3igiyfq9pwx7pzyvz1hr446") (links "ntdll")))

