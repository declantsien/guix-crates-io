(define-module (crates-io nt ri) #:use-module (crates-io))

(define-public crate-ntriple-0.1 (crate (name "ntriple") (vers "0.1.0") (deps (list (crate-dep (name "peg") (req "^0.5") (default-features #t) (kind 1)))) (hash "05skqdm4k0w2v0zpa333c5md4vyj8zg63gfd4knajv7mkrhx1cby")))

(define-public crate-ntriple-0.1 (crate (name "ntriple") (vers "0.1.1") (deps (list (crate-dep (name "peg") (req "^0.5") (default-features #t) (kind 1)))) (hash "0c07dcnl8wc72nlq4fddgk8y8fj9s8hk5qc4pbj33wfxfk7vf3q2")))

