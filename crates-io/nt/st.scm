(define-module (crates-io nt st) #:use-module (crates-io))

(define-public crate-ntstatus-0.1 (crate (name "ntstatus") (vers "0.1.0") (deps (list (crate-dep (name "num_enum") (req "^0.6.1") (default-features #t) (kind 0)))) (hash "0dcqdg5485fpmlg9ijyvb2cqgcdfridpgyip443kkr3l5jjp2dq6")))

(define-public crate-ntstatus-0.1 (crate (name "ntstatus") (vers "0.1.2") (deps (list (crate-dep (name "num_enum") (req "^0.7.0") (kind 0)))) (hash "105i5y974qsi1is4ski3pyq42jzc8nyk5dlrzvzfijx8m6k8xsln") (features (quote (("std") ("default" "std" "num_enum/default"))))))

