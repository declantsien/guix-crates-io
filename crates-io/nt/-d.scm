(define-module (crates-io nt -d) #:use-module (crates-io))

(define-public crate-nt-dll-sys-0.1 (crate (name "nt-dll-sys") (vers "0.1.0") (deps (list (crate-dep (name "bindgen") (req "^0.57.0") (default-features #t) (kind 1)))) (hash "14fq6k5pscnwpmds1h83x8njayai26izq2i5a55az1wamgp38z1d")))

(define-public crate-nt-dll-sys-0.1 (crate (name "nt-dll-sys") (vers "0.1.1") (deps (list (crate-dep (name "bindgen") (req "^0.57.0") (default-features #t) (kind 1)))) (hash "0pkr9vfhnzp295xa2mryc9zq3bb8v4h8dag4rmf38hf5aiaiimnb")))

(define-public crate-nt-dll-sys-0.1 (crate (name "nt-dll-sys") (vers "0.1.2") (deps (list (crate-dep (name "bindgen") (req "^0.57.0") (default-features #t) (kind 1)))) (hash "0cgwwvcc6dl4bya9gizv68d3pn3p7kp9dkbdjja7qm14gvn1k35s")))

