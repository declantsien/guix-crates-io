(define-module (crates-io gq lr) #:use-module (crates-io))

(define-public crate-gqlrequest-0.1 (crate (name "gqlrequest") (vers "0.1.1") (deps (list (crate-dep (name "eyre") (req "^0.6") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)))) (hash "1il1vdjfdjxy0plsp0gyyhaq2s8lbp60m3w9qslz5syq61sjyk9l")))

