(define-module (crates-io gq lo) #:use-module (crates-io))

(define-public crate-gqlog-0.1 (crate (name "gqlog") (vers "0.1.0") (deps (list (crate-dep (name "graphql-parser") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.13") (default-features #t) (kind 0)))) (hash "1myjs9p6vpvrikd65sydxd3q57ppqjxwyq1djsw6kz0l0zc71dxd")))

(define-public crate-gqlog-0.1 (crate (name "gqlog") (vers "0.1.1") (deps (list (crate-dep (name "graphql-parser") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.13") (default-features #t) (kind 0)))) (hash "0p4xyhwahhhswflbvwlj7kfg81i9hxxm1kdm3x17j1fbfr88fkqi")))

(define-public crate-gqlog-1 (crate (name "gqlog") (vers "1.0.0") (deps (list (crate-dep (name "graphql-parser") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1") (default-features #t) (kind 0)))) (hash "1npfaxp98rwnh2hvs4sdb7dgmw1ma955vlpq2wr61frpaa5i5h0i")))

(define-public crate-gqlog-1 (crate (name "gqlog") (vers "1.0.1") (deps (list (crate-dep (name "graphql-parser") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1") (default-features #t) (kind 0)))) (hash "0brw9az74yzi6zzzlc5wzr2gx0mi6x5mcscn0nf6cwn3sxgyxrha")))

(define-public crate-gqlog-1 (crate (name "gqlog") (vers "1.0.2") (deps (list (crate-dep (name "graphql-parser") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1") (default-features #t) (kind 0)))) (hash "0z03hrvvi0d88mzpk53fcbs8qfz9y5613kbyfh7ig3g1zyjh694k")))

(define-public crate-gqlog-1 (crate (name "gqlog") (vers "1.0.3") (deps (list (crate-dep (name "graphql-parser") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1") (default-features #t) (kind 0)))) (hash "0n929m4mgxky8mbm1plaxhw5y7icllagwga0h6p7b5jnsd6gyvxp")))

