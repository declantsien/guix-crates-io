(define-module (crates-io gq ai) #:use-module (crates-io))

(define-public crate-gqair-0.1 (crate (name "gqair") (vers "0.1.0") (deps (list (crate-dep (name "gdk") (req "^0.12") (default-features #t) (kind 0)) (crate-dep (name "gio") (req "^0.8.1") (features (quote ("v2_44"))) (default-features #t) (kind 0)) (crate-dep (name "gtk") (req "^0.8.0") (features (quote ("v3_16"))) (default-features #t) (kind 0)) (crate-dep (name "qair") (req "^0.6.0") (kind 0)) (crate-dep (name "qr2cairo") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "14gi31f4xyb6hap0agbf2r6iifpchvs33iq1jg183ilqbskc68qf")))

(define-public crate-gqair-0.1 (crate (name "gqair") (vers "0.1.1") (deps (list (crate-dep (name "gdk") (req "^0.12") (default-features #t) (kind 0)) (crate-dep (name "gio") (req "^0.8.1") (features (quote ("v2_44"))) (default-features #t) (kind 0)) (crate-dep (name "gtk") (req "^0.8.0") (features (quote ("v3_16"))) (default-features #t) (kind 0)) (crate-dep (name "qair") (req "^0.7.0") (kind 0)) (crate-dep (name "qr2cairo") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "0kml5l3vgqwlp7m9q26g155j7asjyjxb6vcfpb9ncpwjx5grbfl5")))

