(define-module (crates-io wm _a) #:use-module (crates-io))

(define-public crate-wm_add1-0.1 (crate (name "wm_add1") (vers "0.1.0") (deps (list (crate-dep (name "text-blind-watermark") (req "^0.1.3") (default-features #t) (kind 0)))) (hash "1mq4ix4gq05m7sij58hnd7inbhvaxanqp7aa62k29b6m9f47zgd6")))

