(define-module (crates-io wm id) #:use-module (crates-io))

(define-public crate-wmidi-0.1 (crate (name "wmidi") (vers "0.1.0") (hash "0m2i01vyw66s1ilj5xfmpa5gjxbc7gsnch6fsmq55zbwad7al9li")))

(define-public crate-wmidi-0.1 (crate (name "wmidi") (vers "0.1.1") (hash "0h4qfv6isfvys70hqig95r30nwvyp9i6lql0mda6c1m0rsb6rrwr")))

(define-public crate-wmidi-0.1 (crate (name "wmidi") (vers "0.1.2") (hash "0ciw90y2hz2n450n6l82r6nhmbssd6qy8bdvavvpbxllj9pkrq61")))

(define-public crate-wmidi-0.2 (crate (name "wmidi") (vers "0.2.0") (hash "1kddvbr7c58vb8vmzr8mnslk89gdk8c8676jwp5p3w17qyswfaqy")))

(define-public crate-wmidi-1 (crate (name "wmidi") (vers "1.0.0") (hash "13a398xklh2wmsnzsvza3cb9r6r2fp60sm303hg53r6x5f25k688")))

(define-public crate-wmidi-1 (crate (name "wmidi") (vers "1.1.0") (hash "15hlfcndd46b5552zwhgv653b9bva80n1f533dnilzy6fa437xck")))

(define-public crate-wmidi-1 (crate (name "wmidi") (vers "1.2.0") (hash "003i5x4adp0g29sq3ni03r4hbp9b58fgv6vkcjnawdsc9714n800")))

(define-public crate-wmidi-2 (crate (name "wmidi") (vers "2.0.0") (hash "1vild5qkz60m8flq0v2lfbvfz229jy4jq0z2s92f5mgf71prwbhg")))

(define-public crate-wmidi-2 (crate (name "wmidi") (vers "2.0.1") (hash "1hi54biwpmypshk0zb64bm0fdp44grxfflc2bb16sliz85889jrs")))

(define-public crate-wmidi-1 (crate (name "wmidi") (vers "1.2.1") (hash "12ykp08il15yjw1wxs29r233lv0prr7r4rlvb0v8r6947nnq4b8f")))

(define-public crate-wmidi-2 (crate (name "wmidi") (vers "2.0.2") (hash "1bgk18b1zrciab1fsfnaqxp8zrbpfz1nm199lhv0v0cy804k61zk")))

(define-public crate-wmidi-2 (crate (name "wmidi") (vers "2.0.3") (hash "0zqp061x03w7rj7gvxf3gxsw3hd14s285nz4bn6hp2zw4qy1aryr")))

(define-public crate-wmidi-2 (crate (name "wmidi") (vers "2.0.4") (hash "0crfnk45i983qg00vgnma364p9s86as4wz0k1yb5rr27632yd70h")))

(define-public crate-wmidi-3 (crate (name "wmidi") (vers "3.0.0") (hash "183lb2n5l0hjv9llkv668j5qyg8g81cl3yy4cfv7r929l23898bm")))

(define-public crate-wmidi-3 (crate (name "wmidi") (vers "3.0.1") (hash "0gkcgi9w77fg871g0fnpppbgmky64z0k2yh9z8qy78mm0m2v02d2")))

(define-public crate-wmidi-3 (crate (name "wmidi") (vers "3.1.0") (hash "1kxnbs18nmpzm2hfwaaa5h2s77cmk5w53srzxqmrqlkdpdcrjafa")))

(define-public crate-wmidi-4 (crate (name "wmidi") (vers "4.0.0") (deps (list (crate-dep (name "criterion") (req "^0.3") (default-features #t) (kind 2)))) (hash "0ssm4qgxiy2z6apd2yqx20l8jg3qjxg35004jrsp9xk87z5v8nm8") (features (quote (("std") ("default" "std"))))))

(define-public crate-wmidi-4 (crate (name "wmidi") (vers "4.0.1") (deps (list (crate-dep (name "criterion") (req "^0.3") (default-features #t) (kind 2)))) (hash "08kbsr588914qi7rz9wjhyzvmpdaz75qsb0lsamibsir3bz1362s") (features (quote (("std") ("default" "std"))))))

(define-public crate-wmidi-4 (crate (name "wmidi") (vers "4.0.2") (deps (list (crate-dep (name "criterion") (req "^0.3") (default-features #t) (kind 2)))) (hash "0qxn6c65js12yncxnmkzm330pbb2lgscrdikcmd9yd0c445s51i4") (features (quote (("std") ("default" "std"))))))

(define-public crate-wmidi-4 (crate (name "wmidi") (vers "4.0.3") (deps (list (crate-dep (name "criterion") (req "^0.3") (default-features #t) (kind 2)))) (hash "1vz4dnjkm9kr5b1pipa17waglyrcn6z0lak74xj2cqc0b1fn07h7") (features (quote (("std") ("default" "std"))))))

(define-public crate-wmidi-4 (crate (name "wmidi") (vers "4.0.4") (deps (list (crate-dep (name "criterion") (req "^0.3") (default-features #t) (kind 2)))) (hash "0zmp22phpblng45qi9n72d6vwknhqhggq6q2l78kz6n4h4iqrczm") (features (quote (("std") ("default" "std"))))))

(define-public crate-wmidi-4 (crate (name "wmidi") (vers "4.0.5") (deps (list (crate-dep (name "criterion") (req "^0.3") (default-features #t) (kind 2)))) (hash "1a6r3qkk9brsfy90wlvm1r8a1jbzkppqkss93ply1zr6m0nz6lr4") (features (quote (("std") ("default" "std"))))))

(define-public crate-wmidi-4 (crate (name "wmidi") (vers "4.0.6") (deps (list (crate-dep (name "criterion") (req "^0.3") (default-features #t) (kind 2)))) (hash "19blm898lii4szrp4vajpjlrkp05wav9z1cmrqfwmp3wl1bz0zwb") (features (quote (("std") ("default" "std"))))))

(define-public crate-wmidi-4 (crate (name "wmidi") (vers "4.0.8") (deps (list (crate-dep (name "criterion") (req "^0.3") (default-features #t) (kind 2)))) (hash "06viaqsvgwqyzsncr9xj29r2fvajpbah2xahnb980mnapcd22wp6") (features (quote (("std") ("default" "std"))))))

(define-public crate-wmidi-4 (crate (name "wmidi") (vers "4.0.9") (deps (list (crate-dep (name "criterion") (req "^0.3") (default-features #t) (kind 2)))) (hash "0i2w2x4p7hwm1zcv7kf4gr0prjnnvbq9bybilsg5c2dwy5i1gwch") (features (quote (("std") ("default" "std"))))))

(define-public crate-wmidi-4 (crate (name "wmidi") (vers "4.0.10") (deps (list (crate-dep (name "criterion") (req "^0.3") (default-features #t) (kind 2)))) (hash "04n745m0z3cjvqgvj9s6y3xg5fj1a22bms865m17h0dd81dz6maf") (features (quote (("std") ("default" "std"))))))

