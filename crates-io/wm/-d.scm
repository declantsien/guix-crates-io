(define-module (crates-io wm -d) #:use-module (crates-io))

(define-public crate-wm-daemons-0.1 (crate (name "wm-daemons") (vers "0.1.1") (deps (list (crate-dep (name "clap") (req "~1.2.0") (default-features #t) (kind 0)) (crate-dep (name "config") (req "~0.1.2") (default-features #t) (kind 0)) (crate-dep (name "xdg-basedir") (req "~0.2.0") (default-features #t) (kind 0)))) (hash "1gvymnd7w2qichn8imn09bkkxh3zg10zzdvr6a3blc86ws0sh5cf")))

