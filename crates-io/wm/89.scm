(define-module (crates-io wm #{89}#) #:use-module (crates-io))

(define-public crate-wm8960-0.1 (crate (name "wm8960") (vers "0.1.0") (deps (list (crate-dep (name "eh02") (req "^0.2") (optional #t) (default-features #t) (kind 0) (package "embedded-hal")) (crate-dep (name "eh10") (req "^1.0.0-rc.2") (optional #t) (default-features #t) (kind 0) (package "embedded-hal")) (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 1)) (crate-dep (name "regex") (req "^1.5") (default-features #t) (kind 1)) (crate-dep (name "serde_yaml") (req "^0.8") (default-features #t) (kind 1)))) (hash "19f0k3ls4msbqn33zahf7c1f1bhjfq5gzrpvy5i0vzqf16v145lq") (features (quote (("default" "eh02")))) (v 2) (features2 (quote (("eh10" "dep:eh10") ("eh02" "dep:eh02"))))))

(define-public crate-wm8994-0.1 (crate (name "wm8994") (vers "0.1.0") (deps (list (crate-dep (name "embedded-hal") (req "^0.2") (default-features #t) (kind 0)))) (hash "1jhmlbw5rkb30fa5q20a0k2sizk99r3sv0jasrkw1y88bs7yrsns")))

