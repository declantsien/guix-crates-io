(define-module (crates-io wm #{87}#) #:use-module (crates-io))

(define-public crate-wm8731-0.1 (crate (name "wm8731") (vers "0.1.0") (hash "1a32mna2wmgkf62mzx1b4w4nyapl1ah6c501p8kkj8828w66i3ld")))

(define-public crate-wm8731-another-hal-0.1 (crate (name "wm8731-another-hal") (vers "0.1.0") (deps (list (crate-dep (name "embedded-hal") (req "^0.2.3") (default-features #t) (kind 0)))) (hash "088ssiy9qkn2zyj5lsrglb8ph1cl267z82rdlg7l8n7iyjp9zygk")))

