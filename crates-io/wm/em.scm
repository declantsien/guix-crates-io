(define-module (crates-io wm em) #:use-module (crates-io))

(define-public crate-wmem-0.1 (crate (name "wmem") (vers "0.1.0") (deps (list (crate-dep (name "memmem") (req "^0.1.1") (default-features #t) (kind 0)) (crate-dep (name "winapi") (req "^0.3.9") (features (quote ("minwindef" "consoleapi" "handleapi" "processthreadsapi" "memoryapi" "tlhelp32"))) (default-features #t) (kind 0)))) (hash "09ni77kxs8dr0jjgw4xzg205fhqff48n4b9i6j4ygn3vfhj0f6rb")))

(define-public crate-wmemchr-0.1 (crate (name "wmemchr") (vers "0.1.0") (deps (list (crate-dep (name "autocfg") (req "^1.0") (default-features #t) (kind 1)) (crate-dep (name "cfg-if") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quickcheck") (req "^1.0") (default-features #t) (kind 2)) (crate-dep (name "wchar") (req "^0.10") (default-features #t) (kind 2)))) (hash "0flb2yxh492xzzs33dwl38dq4h2jkscvbsy34bvmjlgvdfn0mah1") (features (quote (("unstable") ("std") ("default" "std"))))))

