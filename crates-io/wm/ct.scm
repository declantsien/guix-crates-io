(define-module (crates-io wm ct) #:use-module (crates-io))

(define-public crate-wmctl-0.0.41 (crate (name "wmctl") (vers "0.0.41") (deps (list (crate-dep (name "chrono") (req "^0.4") (default-features #t) (kind 1)) (crate-dep (name "clap") (req "^2.33") (features (quote ("suggestions"))) (kind 0)) (crate-dep (name "gory") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "libwmctl") (req "^0.0.40") (default-features #t) (kind 0)) (crate-dep (name "tracing") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "tracing-subscriber") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "witcher") (req "^0.1.19") (default-features #t) (kind 0)))) (hash "1a5fd0y16xlb7h8ylybyrfj1v6h1hnhqbm8xfhg9g9br4rn3kkvk")))

(define-public crate-wmctl-0.0.45 (crate (name "wmctl") (vers "0.0.45") (deps (list (crate-dep (name "chrono") (req "^0.4") (default-features #t) (kind 1)) (crate-dep (name "clap") (req "^2.33") (features (quote ("suggestions"))) (kind 0)) (crate-dep (name "gory") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "libwmctl") (req "^0.0.45") (default-features #t) (kind 0)) (crate-dep (name "tracing") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "tracing-subscriber") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "witcher") (req "^0.1.19") (default-features #t) (kind 0)))) (hash "0p34im6rs6jd8zi17wm6spp8sm1k10nxvfg4gga315wa4sjzw5fl")))

(define-public crate-wmctrl-0.1 (crate (name "wmctrl") (vers "0.1.3") (hash "1ygc78j4jbsxra9cxvff38q2lsnrm0gghh22m2cx8lfa3pzsc412")))

(define-public crate-wmctrl-0.1 (crate (name "wmctrl") (vers "0.1.4") (hash "0nxxhv5k5srwwkdcipsfqyxgq4n9w8wl8mf61sqj97wd34bmmh7z")))

(define-public crate-wmctrl-0.1 (crate (name "wmctrl") (vers "0.1.5") (hash "0476gvcw1i2jgqq010qd75112zcfbanw8f4bgb8b40hvwpmzc2k5")))

(define-public crate-wmctrl-0.1 (crate (name "wmctrl") (vers "0.1.6") (hash "0yjg13drr2wbgy93agqysx3mcdh82i94c68xplgqima2zylqbbwd")))

(define-public crate-wmctrl-0.1 (crate (name "wmctrl") (vers "0.1.7") (deps (list (crate-dep (name "regex") (req "^1.3.4") (default-features #t) (kind 0)))) (hash "164smwvhqi83rcmlq27c4s8c52znvwy8xgi9gy2gzib2bk7qxa0w")))

(define-public crate-wmctrl-0.1 (crate (name "wmctrl") (vers "0.1.8") (deps (list (crate-dep (name "regex") (req "^1.3.4") (default-features #t) (kind 0)))) (hash "0awrwgk77ibyivw04kxkrn2qbyrc2580adxyir4pfmsa64lxkdbl")))

