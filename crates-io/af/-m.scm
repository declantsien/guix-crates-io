(define-module (crates-io af -m) #:use-module (crates-io))

(define-public crate-af-macros-0.1 (crate (name "af-macros") (vers "0.1.0") (deps (list (crate-dep (name "af-proc-macros") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "0mz09cg5h1vjv64ywsgi9azmp158qp0pcwbbsh9h2m5l5nhb7qk3") (features (quote (("logger" "af-proc-macros/logger"))))))

(define-public crate-af-macros-0.1 (crate (name "af-macros") (vers "0.1.3") (deps (list (crate-dep (name "af-proc-macros") (req "^0.1") (default-features #t) (kind 0)))) (hash "0halcxikcp43mksnarhlbc5dcs5r0bs02lq7qqqp9qx3qf4hlfb9") (features (quote (("logger" "af-proc-macros/logger"))))))

