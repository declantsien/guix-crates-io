(define-module (crates-io af _b) #:use-module (crates-io))

(define-public crate-af_bevy_plugin_macro-0.1 (crate (name "af_bevy_plugin_macro") (vers "0.1.0") (deps (list (crate-dep (name "convert_case") (req "^0.6.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.35") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2.0.48") (default-features #t) (kind 0)))) (hash "19nip66smmapjmp9qyjyn2k19m1v4ynqcx728biws9796xy7dlzs")))

(define-public crate-af_bevy_plugin_macro-0.1 (crate (name "af_bevy_plugin_macro") (vers "0.1.1") (deps (list (crate-dep (name "convert_case") (req "^0.6.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.35") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2.0.48") (default-features #t) (kind 0)))) (hash "0m77h29yb6k6prd1wv6v2w8jkazqkk4iql3lvq2hn75915i9gghn")))

(define-public crate-af_bevy_plugin_macro-0.1 (crate (name "af_bevy_plugin_macro") (vers "0.1.2") (deps (list (crate-dep (name "convert_case") (req "^0.6.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.35") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2.0.48") (default-features #t) (kind 0)))) (hash "1fkj5q6yp4i8hxil3lqffdy2abj3idc9rhsa8xl7a2rgcs7ss7vf")))

