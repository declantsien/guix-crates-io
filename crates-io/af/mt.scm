(define-module (crates-io af mt) #:use-module (crates-io))

(define-public crate-afmt-0.1 (crate (name "afmt") (vers "0.1.0") (deps (list (crate-dep (name "devise") (req "^0.2.0") (default-features #t) (kind 0)))) (hash "1qcbmz3j4yazwnhvhl12gjy5y0rqsl3ldnm82pki72aawamf9xzv")))

(define-public crate-afmt-0.1 (crate (name "afmt") (vers "0.1.1") (deps (list (crate-dep (name "devise") (req "^0.2.0") (default-features #t) (kind 0)))) (hash "1gk8shbwb1m89y0jk1c1h4nlm6g6pysagmnmc1qpc2rg9049l87s")))

