(define-module (crates-io af ch) #:use-module (crates-io))

(define-public crate-afch-logger-0.1 (crate (name "afch-logger") (vers "0.1.0-alpha.0") (deps (list (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)))) (hash "06m5ps6g469hww29av9hyk8czgd8pvdzzpik0x5ib2i9d44l9nsp") (yanked #t)))

(define-public crate-afch-logger-0.1 (crate (name "afch-logger") (vers "0.1.0") (deps (list (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)))) (hash "0k384612yc91in52z4mqpzbwwnsvlaxg7ims3x185692jh5li9kq") (yanked #t)))

(define-public crate-afch-logger-0.2 (crate (name "afch-logger") (vers "0.2.0") (deps (list (crate-dep (name "base64") (req "^0.13") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)))) (hash "0grjanwvcqciy4d06vsfkdyy3xcpqxawwkz0lbz4vp5w9bgi3gpz")))

