(define-module (crates-io af -o) #:use-module (crates-io))

(define-public crate-af-opencl-interop-3 (crate (name "af-opencl-interop") (vers "3.7.1") (deps (list (crate-dep (name "arrayfire") (req "^3.7") (default-features #t) (kind 0)) (crate-dep (name "cl-sys") (req "^0.4.2") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "ocl-core") (req "^0.11.2") (default-features #t) (kind 2)))) (hash "1n00dm9hbgf4nhbbd2zdqm8ypnh8nzdagq1yc95lnn4f00vdx9x5")))

