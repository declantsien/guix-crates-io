(define-module (crates-io af ia) #:use-module (crates-io))

(define-public crate-afia-0.1 (crate (name "afia") (vers "0.1.0") (hash "0xyba4r7xvff6r8dmfg45539yb2ifzbs5hg3zn7s5fikbsrvcma7")))

(define-public crate-afia-cli-0.1 (crate (name "afia-cli") (vers "0.1.0") (hash "16qq2ppbxp3bn6jbfzrvl8zq01vfasjnjxx6q1skr6z2bay401iq")))

