(define-module (crates-io af sp) #:use-module (crates-io))

(define-public crate-afsplitter-0.1 (crate (name "afsplitter") (vers "0.1.0") (deps (list (crate-dep (name "rand") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "sha1") (req "^0.6") (default-features #t) (kind 0)))) (hash "1cnx30z7gdlwqrzxcwzlr1wkspxjppf68a1zjnsa2894c43fpfzf") (yanked #t)))

