(define-module (crates-io af l-) #:use-module (crates-io))

(define-public crate-afl-plugin-0.1 (crate (name "afl-plugin") (vers "0.1.0") (hash "1iggk5zz1q99f7pn6w813xk7g26s88zds2sh6sry0g8188ap2mjp") (yanked #t)))

(define-public crate-afl-plugin-0.1 (crate (name "afl-plugin") (vers "0.1.1") (hash "0hfyfn06mjxsfivyd1v7r0w5fva76kqji6wkprycprn5dfnizakm") (yanked #t)))

(define-public crate-afl-plugin-0.1 (crate (name "afl-plugin") (vers "0.1.2") (deps (list (crate-dep (name "gcc") (req "^0.3") (default-features #t) (kind 1)) (crate-dep (name "quale") (req "^1.0") (default-features #t) (kind 1)))) (hash "0sgygrz4843i28f98a6bnj00fl53wh4vly73kw0nzb6c5x4ryw4g") (yanked #t)))

(define-public crate-afl-plugin-0.1 (crate (name "afl-plugin") (vers "0.1.3") (deps (list (crate-dep (name "gcc") (req "^0.3") (default-features #t) (kind 1)) (crate-dep (name "quale") (req "^1.0") (default-features #t) (kind 1)))) (hash "16hpqlm4ychgjyqfb935v8mszp89bs4hyz8wfvsv72fnayv4jsgn") (yanked #t)))

(define-public crate-afl-plugin-0.1 (crate (name "afl-plugin") (vers "0.1.4") (deps (list (crate-dep (name "gcc") (req "^0.3") (default-features #t) (kind 1)) (crate-dep (name "quale") (req "^1.0") (default-features #t) (kind 1)))) (hash "04nd4kgx7r5i9c03zn4k7xwpklflmr0r9xpdzxkvikk9y7cdzvr3") (yanked #t)))

(define-public crate-afl-plugin-0.1 (crate (name "afl-plugin") (vers "0.1.5") (deps (list (crate-dep (name "gcc") (req "^0.3") (default-features #t) (kind 1)) (crate-dep (name "quale") (req "^1.0") (default-features #t) (kind 1)))) (hash "103ba3r1y285w7vmqfnsya5wdq2v8jlsc9wbrl6hbxsp8z9spkab") (yanked #t)))

(define-public crate-afl-stat-0.1 (crate (name "afl-stat") (vers "0.1.0") (hash "0l8qjisk5pjswijmr66bgrd5fyky6d9b5c4sp4vl5g9b8yzg7x0q")))

(define-public crate-afl-sys-0.1 (crate (name "afl-sys") (vers "0.1.2") (deps (list (crate-dep (name "gcc") (req "^0.3") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "18riri4194dd8pg373f67bkj3325x54687mb4kv5r47bpw7qrqy3") (yanked #t)))

(define-public crate-afl-sys-0.1 (crate (name "afl-sys") (vers "0.1.3") (deps (list (crate-dep (name "gcc") (req "^0.3") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "0m17866lyyymfnwiiqagkkwiq9pl6109pq58lnznw3mqfzi7sabm") (yanked #t)))

(define-public crate-afl-sys-0.1 (crate (name "afl-sys") (vers "0.1.4") (deps (list (crate-dep (name "gcc") (req "^0.3") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "1d21vyy51rnvn1azmm0y6ln1a84bcf8fdbl1jj5cvxp3qd2pvvvd") (yanked #t)))

(define-public crate-afl-sys-0.1 (crate (name "afl-sys") (vers "0.1.5") (deps (list (crate-dep (name "gcc") (req "^0.3") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "0kipfjhbypflv0bbrvrccm0jan0jampl13d2f68pjxj1ymhcwpid") (yanked #t)))

