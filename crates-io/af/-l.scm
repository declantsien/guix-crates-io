(define-module (crates-io af -l) #:use-module (crates-io))

(define-public crate-af-lib-0.1 (crate (name "af-lib") (vers "0.1.1") (deps (list (crate-dep (name "af-core") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "af-postgres") (req "^0.1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "af-sentry") (req "^0.1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "af-slack") (req "^0.1") (optional #t) (default-features #t) (kind 0)))) (hash "006sw6igz1ardf2b9sfma2ypwmkwpd3985iyi5myp0mfzjq0zyp7") (features (quote (("tokio" "af-core/tokio") ("test-runner" "af-core/test-runner") ("slack" "af-slack") ("sentry" "af-sentry") ("postgres" "af-postgres") ("logger" "af-core/logger") ("default" "logger"))))))

