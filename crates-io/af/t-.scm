(define-module (crates-io af t-) #:use-module (crates-io))

(define-public crate-aft-crypto-1 (crate (name "aft-crypto") (vers "1.2.2") (deps (list (crate-dep (name "aes-gcm") (req "^0.10") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "rand_core") (req "^0.6") (features (quote ("getrandom"))) (kind 0)) (crate-dep (name "scrypt") (req "^0.11") (default-features #t) (kind 0)) (crate-dep (name "x25519-dalek") (req "^2") (default-features #t) (kind 0)) (crate-dep (name "zeroize") (req "^1.7") (default-features #t) (kind 0)))) (hash "19wjj2bnjy1zyd4w849b2g1c6m1l1dx6fagdll036gm24lys1sfd")))

