(define-module (crates-io af tr) #:use-module (crates-io))

(define-public crate-aftr-0.1 (crate (name "aftr") (vers "0.1.0") (deps (list (crate-dep (name "insta") (req "^1.10.0") (default-features #t) (kind 2)) (crate-dep (name "nom") (req "^7.1.0") (default-features #t) (kind 0)))) (hash "10vd9jh6d9jv5ygmlvknz3bqwzwibvbrzr0vh00al8hrl5pp2fzg")))

