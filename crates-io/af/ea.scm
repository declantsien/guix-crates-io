(define-module (crates-io af ea) #:use-module (crates-io))

(define-public crate-afeather-0.0.1 (crate (name "afeather") (vers "0.0.1") (deps (list (crate-dep (name "downcast-rs") (req "^1.0.4") (default-features #t) (kind 0)) (crate-dep (name "extend-lifetime") (req "^0.2.0") (default-features #t) (kind 0)))) (hash "1rylnm399xykx39jmm202xaifvlgwsxyw928zrkn1q3bff9d8an9")))

