(define-module (crates-io af -s) #:use-module (crates-io))

(define-public crate-af-sentry-0.1 (crate (name "af-sentry") (vers "0.1.0") (deps (list (crate-dep (name "af-core") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "sentry") (req "^0.21") (default-features #t) (kind 0)))) (hash "007avyixd5l81mplszl6c72swm3kxjvy3jf9g9hvq3i87va23gg2")))

(define-public crate-af-sentry-0.1 (crate (name "af-sentry") (vers "0.1.1") (deps (list (crate-dep (name "af-core") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "sentry") (req "^0.21") (default-features #t) (kind 0)))) (hash "14l7m6vq2vbl9vs1fz04pahzb7rvx9l5qadszlsbsmnxd9k5i62p")))

(define-public crate-af-slack-0.1 (crate (name "af-slack") (vers "0.1.0") (deps (list (crate-dep (name "af-core") (req "^0.1") (features (quote ("tokio"))) (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.11") (features (quote ("json"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "serde_qs") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.3") (default-features #t) (kind 0)))) (hash "1wrpacrxkmv1x352yzcy5rz0f7vv9v0cfh5yj0nm3i5dhpk5p84d")))

