(define-module (crates-io af o3) #:use-module (crates-io))

(define-public crate-afo3c4hnlo78d_publish_test_a-0.0.1 (crate (name "afo3c4hnlo78d_publish_test_a") (vers "0.0.1-alpha.0") (hash "09lpgmnlnj8hqzyryq8nvsa0gh62j6k05bxy5zca258jwv22p80d") (yanked #t) (rust-version "1.69")))

(define-public crate-afo3c4hnlo78d_publish_test_a-0.0.1 (crate (name "afo3c4hnlo78d_publish_test_a") (vers "0.0.1-alpha.2") (hash "0pvylp2yigdb1ddgr5623izxfz4bszxlq0fy6pklw53xqx30s80l") (rust-version "1.69")))

(define-public crate-afo3c4hnlo78d_publish_test_a-0.0.1 (crate (name "afo3c4hnlo78d_publish_test_a") (vers "0.0.1-alpha.3") (hash "03lnvaj9qp868ckfkg2r5760d162nwdkqq3sz07j4v71lifdlrm6") (rust-version "1.69")))

(define-public crate-afo3c4hnlo78d_publish_test_a-0.0.1 (crate (name "afo3c4hnlo78d_publish_test_a") (vers "0.0.1-alpha.5") (hash "1jlgs8w3cd6ch26pj2lxb0jxfs375b40b0jamfqk91dry3g017lx") (rust-version "1.69")))

(define-public crate-afo3c4hnlo78d_publish_test_a-0.0.1 (crate (name "afo3c4hnlo78d_publish_test_a") (vers "0.0.1-alpha.6") (hash "0vkvcn16jjs9rcpxb00kfn9m5blppq1gp5fh4l2j9nzk7j7mh46g") (rust-version "1.69")))

(define-public crate-afo3c4hnlo78d_publish_test_a-0.0.1 (crate (name "afo3c4hnlo78d_publish_test_a") (vers "0.0.1-alpha.7") (hash "09hyx9ah0bljgw7a3vl4sz3p8rjxfx2mmma4ynk2p0r05046fdwc") (rust-version "1.69")))

(define-public crate-afo3c4hnlo78d_publish_test_b-0.0.1 (crate (name "afo3c4hnlo78d_publish_test_b") (vers "0.0.1-alpha.0") (hash "05wj2g0p455l4i3a1n96x2kxv4m9bladllpj2g1mwmm3dqckjwk2") (yanked #t) (rust-version "1.69")))

(define-public crate-afo3c4hnlo78d_publish_test_b-0.0.1 (crate (name "afo3c4hnlo78d_publish_test_b") (vers "0.0.1-alpha.7") (deps (list (crate-dep (name "afo3c4hnlo78d_publish_test_a") (req "^0.0.1-alpha.7") (default-features #t) (kind 0)))) (hash "06yb2ls182ir7vsj8cchxwrgvmxnz1ccjwa9izbfkrwk7djpqqsn") (rust-version "1.69")))

