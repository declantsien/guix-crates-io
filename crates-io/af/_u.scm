(define-module (crates-io af _u) #:use-module (crates-io))

(define-public crate-af_unix-0.1 (crate (name "af_unix") (vers "0.1.0") (deps (list (crate-dep (name "errno") (req "*") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "*") (default-features #t) (kind 0)))) (hash "0znc1s8qy2nciccsak5g73s1l63zx4yyrlvgx57ypjl0ia8pxhy0")))

