(define-module (crates-io af so) #:use-module (crates-io))

(define-public crate-afsort-0.1 (crate (name "afsort") (vers "0.1.0") (deps (list (crate-dep (name "quickcheck") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "regex") (req "^0.2") (default-features #t) (kind 2)))) (hash "1rd3wbazsy9x66xsy7371dxfwvaaml55did45rv8zsljph4140pc")))

(define-public crate-afsort-0.1 (crate (name "afsort") (vers "0.1.1") (deps (list (crate-dep (name "quickcheck") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "regex") (req "^0.2") (default-features #t) (kind 2)))) (hash "0ira351yjfcxhqiknym3fzbchy2xbmqkj0alr6n71bpnw27zyyy4")))

(define-public crate-afsort-0.2 (crate (name "afsort") (vers "0.2.0") (deps (list (crate-dep (name "quickcheck") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "regex") (req "^0.2") (default-features #t) (kind 2)))) (hash "1srxd5am9bh9kwjvfxwa0bnaw8w0xfrjhgpq8f89da1l4vwcacag")))

(define-public crate-afsort-0.3 (crate (name "afsort") (vers "0.3.0") (deps (list (crate-dep (name "quickcheck") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "regex") (req "^0.2") (default-features #t) (kind 2)))) (hash "1qrfzscqpn902s8m8j5k13ffnz514vi66bk5bwipgnnhpjbyywq2")))

(define-public crate-afsort-0.3 (crate (name "afsort") (vers "0.3.1") (deps (list (crate-dep (name "quickcheck") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "regex") (req "^0.2") (default-features #t) (kind 2)))) (hash "0kfv5mxgmi1k0l4bcwrq09sgmghs7f8r5kj7iwz219dm6dx0ijjm")))

