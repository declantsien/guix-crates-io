(define-module (crates-io af ir) #:use-module (crates-io))

(define-public crate-afire-0.1 (crate (name "afire") (vers "0.1.0") (hash "136jfin5a0cl0rpcff4ffzibmgnazs0z0ps7hwyaxzw2jrmn1zls")))

(define-public crate-afire-0.1 (crate (name "afire") (vers "0.1.1") (hash "0ndginlybqdhg2x4afb0q66d2wzbafrh821q54cs2zr1g42l4jis") (features (quote (("rate_limit") ("logging") ("default" "rate_limit" "logging"))))))

(define-public crate-afire-0.1 (crate (name "afire") (vers "0.1.2") (hash "08jk0b305nfrynb8514l69ar1jhj6zx0b510ax46z5rkg1i275yi") (features (quote (("rate_limit") ("logging") ("default" "rate_limit" "logging"))))))

(define-public crate-afire-0.1 (crate (name "afire") (vers "0.1.3") (hash "0hm9nhycwiy6jq09rmgy950r6r3ggdd91fwh1fag9m20bxhb5w6m") (features (quote (("rate_limit") ("logging") ("default" "rate_limit" "logging"))))))

(define-public crate-afire-0.1 (crate (name "afire") (vers "0.1.4") (hash "0z08971wjxz1akkw3zr4z84fz41gmpkd9lh6ghhvklsvdpyvj6ga") (features (quote (("rate_limit") ("logging") ("default" "rate_limit" "logging"))))))

(define-public crate-afire-0.1 (crate (name "afire") (vers "0.1.5") (hash "18fxkqs44qgmmjygpxpgvlfiky1smdpk5clirdkz2ydnvyn1rghn") (features (quote (("thread_pool") ("rate_limit") ("panic_handler") ("logging") ("default" "panic_handler" "thread_pool" "cookies") ("cookies"))))))

(define-public crate-afire-0.1 (crate (name "afire") (vers "0.1.6") (hash "04w6p4nrpaga2jnlw4rx7zbwxcsfqzm52nmi01w64sf2blc16ig0") (features (quote (("thread_pool") ("rate_limit") ("panic_handler") ("logging") ("default" "panic_handler" "thread_pool" "cookies") ("cookies"))))))

(define-public crate-afire-0.1 (crate (name "afire") (vers "0.1.7") (hash "054jqaz15p548q3mgbc2r7ynhcrrx0zf5jpnng9lnh87scy2rk9f") (features (quote (("thread_pool") ("rate_limit") ("panic_handler") ("logging") ("default" "panic_handler" "thread_pool" "cookies") ("cookies"))))))

(define-public crate-afire-0.2 (crate (name "afire") (vers "0.2.0") (hash "0g89ck77dsyxxmi8bvx7iz2ir81k627dpylpnvdy9pr42qi8m33y") (features (quote (("thread_pool") ("rate_limit") ("panic_handler") ("logging") ("dynamic_resize") ("default" "panic_handler" "thread_pool" "cookies" "dynamic_resize") ("cookies"))))))

(define-public crate-afire-0.2 (crate (name "afire") (vers "0.2.1") (hash "0zk457dabdsdfiy040xs297pyhjs0zl3qxy7prbzn9zxhrlds1yh") (features (quote (("thread_pool") ("serve_static") ("rate_limit") ("panic_handler") ("logging") ("dynamic_resize") ("default" "panic_handler" "thread_pool" "cookies" "dynamic_resize") ("cookies")))) (yanked #t)))

(define-public crate-afire-0.2 (crate (name "afire") (vers "0.2.2") (hash "16mp1vp9sccgc079sam65mqr97bxbg8wp5iyhv5vi25m1avrm25f") (features (quote (("thread_pool") ("serve_static") ("rate_limit") ("panic_handler") ("logging") ("dynamic_resize") ("default" "panic_handler" "thread_pool" "cookies" "dynamic_resize") ("cookies"))))))

(define-public crate-afire-0.3 (crate (name "afire") (vers "0.3.0") (hash "1v50hlf21n2vd718r0ibxacd9d8wwxndah8dfcc8h5ckj4hn3n60") (features (quote (("thread_pool") ("serve_static") ("rate_limit") ("path_patterns") ("panic_handler") ("logging") ("ignore_trailing_path_slash") ("dynamic_resize") ("default" "panic_handler" "cookies" "dynamic_resize" "path_patterns" "ignore_trailing_path_slash") ("cookies"))))))

(define-public crate-afire-0.4 (crate (name "afire") (vers "0.4.0") (hash "1vi3haqivwvbqln96vmb41x3lsc04znn6v33i71p57k8sd3wc0yf") (features (quote (("serve_static") ("rate_limit") ("path_patterns") ("path_decode_url") ("panic_handler") ("logging") ("ignore_trailing_path_slash") ("dynamic_resize") ("default" "cookies" "panic_handler" "path_patterns" "dynamic_resize" "path_decode_url" "ignore_trailing_path_slash") ("cookies"))))))

(define-public crate-afire-1 (crate (name "afire") (vers "1.0.0") (hash "107yvy3rzl4jxqdg22mhwmklwji3r8cylfvijlys75qflpgjp58c") (features (quote (("tracing") ("serve_static") ("rate_limit") ("path_patterns") ("path_decode_url") ("panic_handler") ("logging") ("ignore_trailing_path_slash") ("dynamic_resize") ("default" "cookies" "panic_handler" "path_patterns" "dynamic_resize" "path_decode_url" "ignore_trailing_path_slash") ("cookies"))))))

(define-public crate-afire-1 (crate (name "afire") (vers "1.1.0") (hash "1p1f68gsfbqf3n2zcjav79pbih933cqal2gz6gy2ggifdlv3lkm7") (features (quote (("tracing") ("serve_static") ("rate_limit") ("path_patterns") ("path_decode_url") ("panic_handler") ("logging") ("ignore_trailing_path_slash") ("dynamic_resize") ("default" "cookies" "panic_handler" "path_patterns" "dynamic_resize" "path_decode_url" "ignore_trailing_path_slash") ("cookies"))))))

(define-public crate-afire-1 (crate (name "afire") (vers "1.2.0") (hash "1ppqdcfqdbin9i4rphh0vadwmfl6jjnc5j7glc993ia173yj1nyg") (features (quote (("tracing") ("path_patterns") ("path_decode_url") ("panic_handler") ("extensions") ("dynamic_resize") ("default" "cookies" "panic_handler" "path_patterns" "dynamic_resize" "path_decode_url") ("cookies"))))))

(define-public crate-afire-2 (crate (name "afire") (vers "2.0.0") (hash "0kc8byg1aq3pfz7aq8iglyjw2r2hwgpq6z8iqp7mcpfip83fr7bl") (features (quote (("tracing") ("extensions") ("default" "tracing"))))))

(define-public crate-afire-2 (crate (name "afire") (vers "2.1.0") (hash "072jj24a8pf9jxsgzpkgsavvz1q97vzc36zlgim9ga7xgirz8igk") (features (quote (("tracing") ("extensions") ("emoji-logging") ("default" "tracing" "emoji-logging"))))))

(define-public crate-afire-2 (crate (name "afire") (vers "2.2.0") (hash "1k2as2k6d6l052k2k29yg8z7q5dmkcfqm9g1f3yln867prciwcs0") (features (quote (("tracing") ("extensions") ("emoji-logging") ("default" "tracing" "emoji-logging"))))))

(define-public crate-afire-2 (crate (name "afire") (vers "2.2.1") (hash "0sbz5a658jkwrnrhra4pfdpfy5fpxf7k480xbf9c0yw5yzcbcla1") (features (quote (("tracing") ("extensions") ("emoji-logging") ("default" "tracing" "emoji-logging"))))))

(define-public crate-afire-3 (crate (name "afire") (vers "3.0.0-alpha.1") (hash "1yfnxwapiyvxzc6xz08lm8bsslrr1mjyp0kc7va0yzxz5971iymd") (features (quote (("tracing") ("extensions") ("emoji-logging") ("default" "tracing" "emoji-logging"))))))

(define-public crate-afire-3 (crate (name "afire") (vers "3.0.0-alpha.2") (hash "0hyqx7f9kgvzpjfq5lpri1swrxwcywfhvghc02dc325ds21r8hbj") (features (quote (("tracing") ("extensions") ("emoji-logging") ("default" "tracing" "emoji-logging"))))))

(define-public crate-afire-3 (crate (name "afire") (vers "3.0.0-alpha.3") (hash "0jink1x241vizgxvrhzkam0xi8dg8iq429whxfa999cavvscgl1b") (features (quote (("tracing") ("extensions") ("emoji-logging") ("default" "tracing" "emoji-logging"))))))

(define-public crate-afire_compress-0.1 (crate (name "afire_compress") (vers "0.1.0") (deps (list (crate-dep (name "afire") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "afire") (req "^0.3.0") (default-features #t) (kind 2)) (crate-dep (name "brotli2") (req "^0.3.2") (default-features #t) (kind 0)) (crate-dep (name "flate2") (req "^1.0.22") (default-features #t) (kind 0)) (crate-dep (name "libflate") (req "^1.1.2") (default-features #t) (kind 0)))) (hash "0q1796xf8jdf7v9j7das75qcagq98bsvnc5n8fsx5nqparsgl4k8")))

(define-public crate-afire_compress-0.1 (crate (name "afire_compress") (vers "0.1.1-alpha") (deps (list (crate-dep (name "afire") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "afire") (req "^0.3.0") (default-features #t) (kind 2)) (crate-dep (name "brotli2") (req "^0.3.2") (default-features #t) (kind 0)) (crate-dep (name "flate2") (req "^1.0.22") (default-features #t) (kind 0)) (crate-dep (name "libflate") (req "^1.1.2") (default-features #t) (kind 0)))) (hash "1xm5cxkly5cw2xz7013ac69gxpw335k6pmv9hsvg60dd1s6r1a7q")))

