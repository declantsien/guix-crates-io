(define-module (crates-io af _p) #:use-module (crates-io))

(define-public crate-af_packet-0.1 (crate (name "af_packet") (vers "0.1.0") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "0dn0cd9xm44hyg3ghlsgwq31333k5f2ad07bxrinl2d1qcwd7b9s")))

(define-public crate-af_packet-0.1 (crate (name "af_packet") (vers "0.1.1") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "0vv21l3xvpxm6da10i1ydldp5xwq6pjdywgi8m3m0llmavpk59zc")))

(define-public crate-af_packet-0.1 (crate (name "af_packet") (vers "0.1.3") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "113h1hcb5bfca6za93yzwbjmn9qax2a544yww8f5l5szgdyppmg8")))

(define-public crate-af_packet-0.1 (crate (name "af_packet") (vers "0.1.4") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "nom") (req "^3.2") (default-features #t) (kind 0)))) (hash "144qx8n6cy90vqb1x3xp3m0v15da2g08a2rl1rxkr6xch9ks89dx")))

(define-public crate-af_packet-0.1 (crate (name "af_packet") (vers "0.1.5") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "nom") (req "^3.2") (default-features #t) (kind 0)))) (hash "0mb7xgi98qvm3awwqwjjjir9ydaqraardls49zannwljvwm9r37f")))

(define-public crate-af_packet-0.1 (crate (name "af_packet") (vers "0.1.6") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "nom") (req "^3.2") (default-features #t) (kind 0)))) (hash "1qciyd1g7l1mych6m05zfdn0zgasg3xna2ny413m27wzmhw608fm")))

(define-public crate-af_packet-0.2 (crate (name "af_packet") (vers "0.2.0") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "nom") (req "^3.2") (default-features #t) (kind 0)))) (hash "0mnxgvf5c02ggphcm5v79pfy4zpwkm4476x8l3srby9wxksazgy1")))

(define-public crate-af_packet-0.2 (crate (name "af_packet") (vers "0.2.1") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "nom") (req "^3.2") (default-features #t) (kind 0)))) (hash "06bch6gn0rkzj2vljl6ffnl2qgg1xwqv727rx5cvmz9hkm5pnnxk")))

(define-public crate-af_packet-0.2 (crate (name "af_packet") (vers "0.2.2") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "nom") (req "^3.2") (default-features #t) (kind 0)))) (hash "1ncyji18h0nl6hcvpmppc9nxwidf8i69djqzs6bzp5fz5xz3b4hl")))

(define-public crate-af_packet-0.2 (crate (name "af_packet") (vers "0.2.3") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "nom") (req "^3.2") (default-features #t) (kind 0)))) (hash "05m2s6am39j919dh3mxfybp043n72cfhif1v52hihhn3xlzzqd6g")))

(define-public crate-af_packet-0.2 (crate (name "af_packet") (vers "0.2.5") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "nom") (req "^3.2") (default-features #t) (kind 0)))) (hash "09abw0nx21fkpzjklfwxbidj9826yr67lhx2a1dlic4ivgddz1v3")))

(define-public crate-af_packet-0.2 (crate (name "af_packet") (vers "0.2.6") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "nom") (req "^3.2") (default-features #t) (kind 0)))) (hash "0knixj74vslas82mmdi7zjv92wjl620yhfxhaffjwb17zh5kgzc7")))

(define-public crate-af_packet-0.2 (crate (name "af_packet") (vers "0.2.7") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "nom") (req "^3.2") (default-features #t) (kind 0)))) (hash "0dwd0f5m9fm0n7pd4lrycwmj2xzdlya6s1bhr7psg5d1x1njq2v9")))

(define-public crate-af_packet-0.3 (crate (name "af_packet") (vers "0.3.0") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "nom") (req "^3.2") (default-features #t) (kind 0)))) (hash "1fwnj1nqhjg33l30sa8if4zc21l3a0d2hk21i7b1vz1rlx89m9hv")))

(define-public crate-af_packet-0.3 (crate (name "af_packet") (vers "0.3.1") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "nom") (req "^5.1") (default-features #t) (kind 0)))) (hash "10z298qd1iw3w7w87d179s2zh9rgjyz7yplkrng7a3fvnl6cxffn")))

(define-public crate-af_path-0.1 (crate (name "af_path") (vers "0.1.0") (hash "1xbpavb3s2xiwipkj69qi1wfxzzgazl5mhzn5vc1p1jbkvlww67q")))

(define-public crate-af_path-0.1 (crate (name "af_path") (vers "0.1.1") (hash "1xbmc83ygr04nvij2xxdl0g97shdqfbgqn6w077vp3xlldc4zswi")))

(define-public crate-af_path-0.1 (crate (name "af_path") (vers "0.1.2") (hash "0411lrdr8zxmzj9g67fw57iqd19fyy97ip16h02858avmn1pjyf8")))

(define-public crate-af_path-0.1 (crate (name "af_path") (vers "0.1.3") (hash "00wn6r1b1ysqw0fqlhvz7f56xrcirp5ang0sg974bb2vvpvvrlnx")))

(define-public crate-af_path-0.1 (crate (name "af_path") (vers "0.1.4") (hash "0xg6icn5s1xifr2d1qck9a5l3a5z1kwggi3lwx6kzpqi2dvzdf61")))

(define-public crate-af_path-0.2 (crate (name "af_path") (vers "0.2.0") (deps (list (crate-dep (name "iso8601") (req "^0.4.0") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1.0.28") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.130") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0.130") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.66") (default-features #t) (kind 0)) (crate-dep (name "serde_yaml") (req "^0.8.21") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (features (quote ("extra-traits" "full"))) (default-features #t) (kind 0)) (crate-dep (name "yaml-rust") (req "^0.4.5") (default-features #t) (kind 0)))) (hash "0g3k4a6rhdjpy6m8474ikd03mkj01ld9py8apbh94kvgz0lhccma")))

(define-public crate-af_path-0.2 (crate (name "af_path") (vers "0.2.1") (deps (list (crate-dep (name "iso8601") (req "^0.4.0") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1.0.28") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.130") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0.130") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.66") (default-features #t) (kind 0)) (crate-dep (name "serde_yaml") (req "^0.8.21") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (features (quote ("extra-traits" "full"))) (default-features #t) (kind 0)) (crate-dep (name "yaml-rust") (req "^0.4.5") (default-features #t) (kind 0)))) (hash "1pd15h5gd2yd9jl661wvh2ndj7ywagmfl2ivsr84raanhj9bg6vm")))

(define-public crate-af_path-0.2 (crate (name "af_path") (vers "0.2.2") (deps (list (crate-dep (name "iso8601") (req "^0.4.0") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1.0.28") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.130") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0.130") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.66") (default-features #t) (kind 0)) (crate-dep (name "serde_yaml") (req "^0.8.21") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (features (quote ("extra-traits" "full"))) (default-features #t) (kind 0)) (crate-dep (name "yaml-rust") (req "^0.4.5") (default-features #t) (kind 0)))) (hash "0dl0wba8c291gyj7n5ril61nwmkhbmbnbmfjmpdph5gsz4bccxcs")))

(define-public crate-af_path-0.2 (crate (name "af_path") (vers "0.2.3") (deps (list (crate-dep (name "iso8601") (req "^0.4.0") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1.0.28") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.130") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0.130") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.66") (default-features #t) (kind 0)) (crate-dep (name "serde_yaml") (req "^0.8.21") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (features (quote ("extra-traits" "full"))) (default-features #t) (kind 0)) (crate-dep (name "yaml-rust") (req "^0.4.5") (default-features #t) (kind 0)))) (hash "0yxpia4bsdyywj6kyk90d2sb9bsw7y0wba0cl5ky57q52ldwywqj")))

