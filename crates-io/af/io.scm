(define-module (crates-io af io) #:use-module (crates-io))

(define-public crate-afio-0.1 (crate (name "afio") (vers "0.1.0") (deps (list (crate-dep (name "libc") (req "0.2.*") (default-features #t) (kind 0)) (crate-dep (name "mio") (req "^0.6.1") (default-features #t) (kind 0)))) (hash "0lwlmgdy9pinf6m6qan7f0hj88h1jnl02v17aybj0v0q61jg7ynq")))

