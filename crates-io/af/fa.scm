(define-module (crates-io af fa) #:use-module (crates-io))

(define-public crate-affably-0.1 (crate (name "affably") (vers "0.1.0") (hash "0n48gmx4shjadcsjwy2vyg6k8grlazcmn8634i2hd5ims7qcjpv0")))

(define-public crate-affably-0.1 (crate (name "affably") (vers "0.1.1") (hash "0ldczrzjswpkldqizaran9zmph9bsfihap12mgbg37w0wcqvygi8")))

(define-public crate-affair-0.0.0 (crate (name "affair") (vers "0.0.0") (hash "067rb864rxhb1yj7xn62fvvnxgp806dc2mwy3f62x584kisnf78v")))

(define-public crate-affair-0.1 (crate (name "affair") (vers "0.1.0") (deps (list (crate-dep (name "async-trait") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "futures") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.28") (features (quote ("rt" "sync" "macros"))) (default-features #t) (kind 0)))) (hash "19lwysivpwrjnc8scbwl3hvd7wyvjfw5v8vy4ag8hrhshr5lvnnb")))

(define-public crate-affair-0.1 (crate (name "affair") (vers "0.1.1") (deps (list (crate-dep (name "async-trait") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "futures") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.28") (features (quote ("rt" "sync" "macros"))) (default-features #t) (kind 0)))) (hash "06csm04y0cm9ymf848iqwfxxp29za3ba258vl2g6k4kwy3nsqb9x")))

(define-public crate-affair-0.1 (crate (name "affair") (vers "0.1.2") (deps (list (crate-dep (name "async-trait") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "futures") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.28") (features (quote ("rt" "sync" "macros"))) (default-features #t) (kind 0)))) (hash "0qz1jidj3wjf35yjpvkgbrhydbhihb5j0whahlgq75c0shb8qjkd")))

