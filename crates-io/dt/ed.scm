(define-module (crates-io dt ed) #:use-module (crates-io))

(define-public crate-dted-0.1 (crate (name "dted") (vers "0.1.0") (deps (list (crate-dep (name "failure") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "failure_derive") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "nom") (req "4.*") (default-features #t) (kind 0)))) (hash "1vrxck6l2q71h2b6m8qbmkgbxyq4msisb1mvkg3ii23bdcrsdkjv")))

(define-public crate-dted-0.2 (crate (name "dted") (vers "0.2.0") (deps (list (crate-dep (name "failure") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "failure_derive") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "nom") (req "4.*") (default-features #t) (kind 0)))) (hash "1pjk0n77gpgv72bji09k6ikcbxfrvjf66626dw571hkg73smvnqm")))

(define-public crate-dted2-0.1 (crate (name "dted2") (vers "0.1.0") (deps (list (crate-dep (name "nom") (req "^7") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2") (default-features #t) (kind 0)))) (hash "07ry8sk6dlk9adhqgxb3nnfcalc9qpha8clnrsmr6v25369g8i58") (yanked #t)))

(define-public crate-dted2-0.1 (crate (name "dted2") (vers "0.1.1") (deps (list (crate-dep (name "nom") (req "^7") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2") (default-features #t) (kind 0)))) (hash "1md6vraijbzkgprjb0f299bss851fqffqh67lji7qf3k7wwrbg13") (yanked #t)))

(define-public crate-dted2-0.2 (crate (name "dted2") (vers "0.2.0") (deps (list (crate-dep (name "nom") (req "^7") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2") (default-features #t) (kind 0)))) (hash "038f5v9i3a0cfl2b60f38xnj6cvgj2cpvdz5mb5iv6hg01nib09a")))

(define-public crate-dted2-0.2 (crate (name "dted2") (vers "0.2.1") (deps (list (crate-dep (name "nom") (req "^7") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2") (default-features #t) (kind 0)))) (hash "187kgkrbxkjy3fkp5qlbqm5hgik82bjbyb4536k2w0rmnplk8hk2")))

