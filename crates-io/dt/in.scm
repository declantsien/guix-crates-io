(define-module (crates-io dt in) #:use-module (crates-io))

(define-public crate-dtinfer-0.1 (crate (name "dtinfer") (vers "0.1.0") (deps (list (crate-dep (name "assert_approx_eq") (req "^1.1.0") (default-features #t) (kind 2)) (crate-dep (name "chrono") (req "^0.4") (default-features #t) (kind 2)) (crate-dep (name "nom") (req "^5") (default-features #t) (kind 0)))) (hash "02nj2gkzpnvmq5hxpk3ch81imb5s90j4aqmqvyllh94n89c7bcyd")))

(define-public crate-dtinfer-0.1 (crate (name "dtinfer") (vers "0.1.1") (deps (list (crate-dep (name "assert_approx_eq") (req "^1.1.0") (default-features #t) (kind 2)) (crate-dep (name "chrono") (req "^0.4") (default-features #t) (kind 2)) (crate-dep (name "nom") (req "^5") (default-features #t) (kind 0)))) (hash "0il5l632qgmfm9lng9ca2pmm9cn10ifsabvckk9x1g6zkq35r9dg")))

