(define-module (crates-io dt ab) #:use-module (crates-io))

(define-public crate-dtab-0.0.2 (crate (name "dtab") (vers "0.0.2") (deps (list (crate-dep (name "pretty_assertions") (req "^0.2.0") (default-features #t) (kind 2)) (crate-dep (name "serde") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0") (default-features #t) (kind 0)))) (hash "0r6cbf2lvl44vgm861klnsf0c2i6cig3mz51zc3yvkps7vdan2zm")))

