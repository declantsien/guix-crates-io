(define-module (crates-io dt ls) #:use-module (crates-io))

(define-public crate-dtls-0.0.0 (crate (name "dtls") (vers "0.0.0") (hash "0pbr3w1a5hwjq0w91xjkm8j2xkwg1z394ag7yznlvdx01945n897")))

(define-public crate-dtls-proto-0.0.0 (crate (name "dtls-proto") (vers "0.0.0") (hash "072iy43hz5yg3sdn3l8pgjs7g7lk2p7p3ymxi9z8mnjm85ahdrhy")))

