(define-module (crates-io dt w_) #:use-module (crates-io))

(define-public crate-dtw_rs-0.9 (crate (name "dtw_rs") (vers "0.9.0") (deps (list (crate-dep (name "float-cmp") (req "^0.9.0") (default-features #t) (kind 2)))) (hash "07nksckl57nhp9k0zhqhbri020hirjxaywjx7f3pz7i4ph4m203k") (yanked #t)))

(define-public crate-dtw_rs-0.9 (crate (name "dtw_rs") (vers "0.9.1") (deps (list (crate-dep (name "float-cmp") (req "^0.9.0") (default-features #t) (kind 2)))) (hash "16ykx540w5iimhfr7k8prkca2xk9dkcsx1bv19npch18mx6yw5cv")))

(define-public crate-dtw_rs-0.9 (crate (name "dtw_rs") (vers "0.9.2") (deps (list (crate-dep (name "float-cmp") (req "^0.9.0") (default-features #t) (kind 2)))) (hash "01b9l7750f6rdmdw9a5x5cjjhysvb7fxzxpd5iikdqc1x0wlqx8f")))

(define-public crate-dtw_rs-0.9 (crate (name "dtw_rs") (vers "0.9.3") (deps (list (crate-dep (name "float-cmp") (req "^0.9.0") (default-features #t) (kind 2)))) (hash "16fq1wivsmd3bdv5nrw501fhql4vcz1jdvd07pm60ka0pclb2qms")))

(define-public crate-dtw_rs-0.9 (crate (name "dtw_rs") (vers "0.9.4") (deps (list (crate-dep (name "float-cmp") (req "^0.9.0") (default-features #t) (kind 2)))) (hash "1lb4fs6qmfrlaxjj1ffzfimj1ayaaj1vmrb2s7xnjq1yr11bqh1f")))

(define-public crate-dtw_rs-0.9 (crate (name "dtw_rs") (vers "0.9.5") (deps (list (crate-dep (name "float-cmp") (req "^0.9.0") (default-features #t) (kind 2)))) (hash "0n8byi10bx95gp87j3mwzk89ma341vga4zj50s2qwn1xk6r7axil")))

(define-public crate-dtw_rs_band_fork-0.9 (crate (name "dtw_rs_band_fork") (vers "0.9.5") (deps (list (crate-dep (name "float-cmp") (req "^0.9.0") (default-features #t) (kind 2)))) (hash "05cj1kl6d8kxl8nam5vrvbc0r885yqwdf3sii29bdhdpbi3l7k35")))

(define-public crate-dtw_rs_band_fork-0.9 (crate (name "dtw_rs_band_fork") (vers "0.9.6") (deps (list (crate-dep (name "float-cmp") (req "^0.9.0") (default-features #t) (kind 2)))) (hash "18ra67z61kng2ydlx530hf6bq4rmbwxpigarbn4w7pmwc3g1gapl")))

(define-public crate-dtw_rs_band_fork-0.9 (crate (name "dtw_rs_band_fork") (vers "0.9.7") (deps (list (crate-dep (name "float-cmp") (req "^0.9.0") (default-features #t) (kind 2)))) (hash "1x33m6pwnjgp2pbgfg1pqhawqqj8iniy3a4kk5s5mldq6q14i9j5")))

(define-public crate-dtw_rs_band_fork-0.9 (crate (name "dtw_rs_band_fork") (vers "0.9.8") (deps (list (crate-dep (name "float-cmp") (req "^0.9.0") (default-features #t) (kind 2)))) (hash "03sjldgczwqpp56v580va152r3drb051vd08p6lsa41zv005b34q")))

(define-public crate-dtw_rs_band_fork-0.9 (crate (name "dtw_rs_band_fork") (vers "0.9.9") (deps (list (crate-dep (name "float-cmp") (req "^0.9.0") (default-features #t) (kind 2)))) (hash "1rnilb6ibksgqb2sjk2qhkwqlb30zxnax844y72lb8qpxz9q02kj")))

(define-public crate-dtw_rs_band_fork-1 (crate (name "dtw_rs_band_fork") (vers "1.0.0") (deps (list (crate-dep (name "float-cmp") (req "^0.9.0") (default-features #t) (kind 2)))) (hash "0jb31afqj8rvxzjzpaa1k91hdn1kp1k70nlxwk2mlm1xfkpxnm2a")))

(define-public crate-dtw_rs_band_fork-1 (crate (name "dtw_rs_band_fork") (vers "1.0.1") (deps (list (crate-dep (name "float-cmp") (req "^0.9.0") (default-features #t) (kind 2)))) (hash "0vkp7qfa7r46byg90laplzjc2661mn7w3n9bldjfwcj27wfi93hr")))

