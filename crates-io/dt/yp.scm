(define-module (crates-io dt yp) #:use-module (crates-io))

(define-public crate-dtypei-0.0.1 (crate (name "dtypei") (vers "0.0.1") (deps (list (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "wasm-bindgen") (req "^0.2") (features (quote ("serde-serialize"))) (default-features #t) (kind 0)))) (hash "0kzn307qmmw0nwdccn8adk6ryk3sbnbnkx7fscrpx1n3iyhrqk4s")))

(define-public crate-dtypei-derive-0.0.1 (crate (name "dtypei-derive") (vers "0.0.1") (deps (list (crate-dep (name "dtypei") (req "^0.0.1") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.2") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.13") (features (quote ("full" "derive"))) (default-features #t) (kind 0)))) (hash "19y4fmzhrp0yvhw1dfq018f66lb4spxpyrffwblg39dw2lpxkzqh")))

