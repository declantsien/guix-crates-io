(define-module (crates-io dt ee) #:use-module (crates-io))

(define-public crate-dtee-0.0.0 (crate (name "dtee") (vers "0.0.0") (hash "0xn59w7lzpmbswnqqr5qyzvcdh75pr3cp0dkasvc8hci50ds1881")))

(define-public crate-dtee-0.0.1 (crate (name "dtee") (vers "0.0.1") (hash "14503z7h38yrrxpjip7mjm8vncm6qpnh4k0bqp9bwcbwdr5c4sdz")))

(define-public crate-dtee-0.0.2 (crate (name "dtee") (vers "0.0.2") (hash "1pwplahsfydi0pzgczjm1amlqfqifl4c481b689dq2ka5wxkrr2r")))

(define-public crate-dtee-0.0.3 (crate (name "dtee") (vers "0.0.3") (hash "0rjw1ziwln8p1n4gpjgb40gdyhxmbn1vw4fkpwk99dhqc2hqpz1k")))

