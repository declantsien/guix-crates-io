(define-module (crates-io dt gr) #:use-module (crates-io))

(define-public crate-dtgrep-0.1 (crate (name "dtgrep") (vers "0.1.0") (hash "1c4wz48sb9hp6rw9sjjaa7zq74icmcpl6rh567igndnnc9cc46rd")))

(define-public crate-dtgrep-0.1 (crate (name "dtgrep") (vers "0.1.1") (hash "0y2nzp7jz742hcxsi5scz4ydaxqa69lqk7v6m6w72gwlw1qy5aph")))

(define-public crate-dtgrep-0.1 (crate (name "dtgrep") (vers "0.1.2") (hash "11d07m89rjjsm2snbbjki7ws16qfqj3xggy7hd4pgz1dilp049ji")))

