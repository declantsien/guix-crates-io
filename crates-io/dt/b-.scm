(define-module (crates-io dt b-) #:use-module (crates-io))

(define-public crate-dtb-walker-0.1 (crate (name "dtb-walker") (vers "0.1.0") (hash "1sjaclayd36894fy05l7am96wi18qlbh9l03zqyx7c5829zlimq3")))

(define-public crate-dtb-walker-0.1 (crate (name "dtb-walker") (vers "0.1.1") (hash "0kg0f1hnm1s8jg8d5sb2vvdy7qgkvmga886cil487iplj1il7lxj")))

(define-public crate-dtb-walker-0.1 (crate (name "dtb-walker") (vers "0.1.2") (hash "1i2n0na4d0llxjjhkhzkkzw3glxr1fn0lx6x1x11dni3gc6x8mbc") (yanked #t)))

(define-public crate-dtb-walker-0.1 (crate (name "dtb-walker") (vers "0.1.3") (hash "1n0cshqfmikypb0gjydppwrq738iww8bylsz5sbhay24831lafmw")))

(define-public crate-dtb-walker-0.2 (crate (name "dtb-walker") (vers "0.2.0-alpha.1") (hash "08zlv1wddhcl3gwz51k373xainxm3acwj0s9ga8chh9f1ya88nn8")))

(define-public crate-dtb-walker-0.2 (crate (name "dtb-walker") (vers "0.2.0-alpha.2") (hash "181zwk3gqy2dk97kwrivbnvxpb26ilz6593bfnb055gfik7l2z6s")))

(define-public crate-dtb-walker-0.2 (crate (name "dtb-walker") (vers "0.2.0-alpha.3") (hash "0ci3nla8rqyr875fjbw200lngh0qwc1akma4pvvmk9hsm8fd814l")))

