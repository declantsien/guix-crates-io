(define-module (crates-io dt oa) #:use-module (crates-io))

(define-public crate-dtoa-0.1 (crate (name "dtoa") (vers "0.1.0") (hash "0nfj2jz9wzxyw2vfii2nlxxchq56gz9igd85l93k9pnmzqys7rf0")))

(define-public crate-dtoa-0.1 (crate (name "dtoa") (vers "0.1.1") (hash "1c7sfksg80822rjy91g5va88pr05b40ppj8m4iiw5mj2nj7mysmm")))

(define-public crate-dtoa-0.2 (crate (name "dtoa") (vers "0.2.0") (hash "19hib98xiwam44g3wa891vsshv3xshrkm1rzxx0mh1la0c4jy9md")))

(define-public crate-dtoa-0.2 (crate (name "dtoa") (vers "0.2.1") (hash "12g7wh2iigf9i81s54662kng11hqcdrkl776n7jrbqrg2p45vs59")))

(define-public crate-dtoa-0.2 (crate (name "dtoa") (vers "0.2.2") (hash "0g96cap6si1g6wi62hsdk2fnj3sf5vd4i97zj6163j8hhnsl3n0d")))

(define-public crate-dtoa-0.3 (crate (name "dtoa") (vers "0.3.0") (hash "1bv58mg3q8dq4417gnfj4di15wfsjhwqprmgn27k9grb9dxdrrc0")))

(define-public crate-dtoa-0.3 (crate (name "dtoa") (vers "0.3.1") (hash "0ajxji7v1r7467l023i6p8sq1bs8rrvsc95vfz3635ss6zzjg9c7")))

(define-public crate-dtoa-0.4 (crate (name "dtoa") (vers "0.4.0") (hash "0kyvq94y7pk6950ydinp7ihmcffb6j56nzlvc88hk3iggg36kpay")))

(define-public crate-dtoa-0.4 (crate (name "dtoa") (vers "0.4.1") (hash "144cazs64lf3fbjx82ky4qd1c6fypgf0dz22jw59jihiswgvgj40")))

(define-public crate-dtoa-0.4 (crate (name "dtoa") (vers "0.4.2") (hash "1ashiv223ns5p94ip8r5zxxmv2c3hh0pdsm4rcax4x5m7ly7bhq9")))

(define-public crate-dtoa-0.4 (crate (name "dtoa") (vers "0.4.3") (hash "1pc9fkpali122caa7pz9mm0vbijwr1iaby8m64yz26j1xd012c3d")))

(define-public crate-dtoa-0.4 (crate (name "dtoa") (vers "0.4.4") (hash "0phbm7i0dpn44gzi07683zxaicjap5064w62pidci4fhhciv8mza")))

(define-public crate-dtoa-0.4 (crate (name "dtoa") (vers "0.4.5") (hash "18qycvcp0vaqzw0j784ansbxgs39l54ini9v719cy2cs3ghsjn23")))

(define-public crate-dtoa-0.4 (crate (name "dtoa") (vers "0.4.6") (hash "0as2dja7bpv5mnqbji2il9yjggzgh4k27x5shjdxpnlb0bs52j8k")))

(define-public crate-dtoa-0.4 (crate (name "dtoa") (vers "0.4.7") (hash "0kpagin5jx9khw1ylardzm9hp1g8k0i87qrkgsrwchfp6hlyvmw8")))

(define-public crate-dtoa-0.4 (crate (name "dtoa") (vers "0.4.8") (hash "1c5j0wz118dhrczx6spc5za7dnbfxablr4adyahg9aknrsc9i2an")))

(define-public crate-dtoa-1 (crate (name "dtoa") (vers "1.0.0") (hash "0h3fx8k1iim5xkbxjfwx4z51xsdbs4qaqxmgxpskdpfw2if7yi63") (rust-version "1.36")))

(define-public crate-dtoa-1 (crate (name "dtoa") (vers "1.0.1") (hash "10lbz4rkvc0qqsax2x8jp1w3z5qrz01hr4kh4gv0qk8jgb51asbm") (rust-version "1.36")))

(define-public crate-dtoa-1 (crate (name "dtoa") (vers "1.0.2") (hash "19ijjzwnf4jxgcmz7h9gw7zpywa43zxjqb9rwpqhz5ibpmfagajw") (rust-version "1.36")))

(define-public crate-dtoa-1 (crate (name "dtoa") (vers "1.0.3") (hash "0mh6v0lsb148kkbyyw1il29q6rhli565ma2n2ywwwfandgs3y1f6") (rust-version "1.36")))

(define-public crate-dtoa-1 (crate (name "dtoa") (vers "1.0.4") (deps (list (crate-dep (name "no-panic") (req "^0.1") (optional #t) (default-features #t) (kind 0)))) (hash "1yhvks46dhm9zk46gmbbv1jdilcb07d110v82pq17lfhspifx9pq") (rust-version "1.36")))

(define-public crate-dtoa-1 (crate (name "dtoa") (vers "1.0.5") (deps (list (crate-dep (name "no-panic") (req "^0.1") (optional #t) (default-features #t) (kind 0)))) (hash "04skv0ynkzh59sj44ibi19xraj901kij854il26xzs3xd8ah81y0") (rust-version "1.36")))

(define-public crate-dtoa-1 (crate (name "dtoa") (vers "1.0.6") (deps (list (crate-dep (name "no-panic") (req "^0.1") (optional #t) (default-features #t) (kind 0)))) (hash "0s9iccyvvr68g57gnb0ybclm6nw8ypbpkckr8q8pkamcpxkr1l35") (rust-version "1.36")))

(define-public crate-dtoa-1 (crate (name "dtoa") (vers "1.0.7") (deps (list (crate-dep (name "no-panic") (req "^0.1") (optional #t) (default-features #t) (kind 0)))) (hash "1kzy2yqhid541238iixylii48445w1gkwp8yv22dxv55z3mvn169") (rust-version "1.36")))

(define-public crate-dtoa-1 (crate (name "dtoa") (vers "1.0.8") (deps (list (crate-dep (name "no-panic") (req "^0.1") (optional #t) (default-features #t) (kind 0)))) (hash "168rdzismkh6b1jlrdnqiiasbgl2a5rryh2sc9lykxpm236q76si") (rust-version "1.36")))

(define-public crate-dtoa-1 (crate (name "dtoa") (vers "1.0.9") (deps (list (crate-dep (name "no-panic") (req "^0.1") (optional #t) (default-features #t) (kind 0)))) (hash "0lv6zzgrd3hfh83n9jqhzz8645729hv1wclag8zw4dbmx3w2pfyw") (rust-version "1.36")))

(define-public crate-dtoa-short-0.1 (crate (name "dtoa-short") (vers "0.1.0") (deps (list (crate-dep (name "dtoa") (req "^0.4.1") (default-features #t) (kind 0)) (crate-dep (name "float-cmp") (req "^0.2.3") (default-features #t) (kind 2)))) (hash "117p8bnsx7ncc40w555q7hfrc5z3a8nh3acasyz7ifprfyair665")))

(define-public crate-dtoa-short-0.2 (crate (name "dtoa-short") (vers "0.2.0") (deps (list (crate-dep (name "dtoa") (req "^0.4.1") (default-features #t) (kind 0)) (crate-dep (name "float-cmp") (req "^0.2.3") (default-features #t) (kind 2)))) (hash "0dpnigza4s5vfi754hj2h07rf43v74qwpawsfd9y873w6m67d47p")))

(define-public crate-dtoa-short-0.3 (crate (name "dtoa-short") (vers "0.3.0") (deps (list (crate-dep (name "dtoa") (req "^0.4.1") (default-features #t) (kind 0)) (crate-dep (name "float-cmp") (req "^0.2.3") (default-features #t) (kind 2)))) (hash "1j2wq1kh7sm9vdd01b7w6q0mzyyvn38q9vazr5bzsqk481xp4vzy")))

(define-public crate-dtoa-short-0.3 (crate (name "dtoa-short") (vers "0.3.1") (deps (list (crate-dep (name "dtoa") (req "^0.4.1") (default-features #t) (kind 0)) (crate-dep (name "float-cmp") (req "^0.2.3") (default-features #t) (kind 2)))) (hash "078dj8l83mjs4d6bzfgdv281n58c3fnzr35vn3q1h6kwd4k41386")))

(define-public crate-dtoa-short-0.3 (crate (name "dtoa-short") (vers "0.3.2") (deps (list (crate-dep (name "dtoa") (req "^0.4.1") (default-features #t) (kind 0)) (crate-dep (name "float-cmp") (req "^0.3") (default-features #t) (kind 2)))) (hash "1wkn7ziqffq8hj0a411lgn7674ackzdk734ikp230rmp2f2hn0jr")))

(define-public crate-dtoa-short-0.3 (crate (name "dtoa-short") (vers "0.3.3") (deps (list (crate-dep (name "dtoa") (req "^0.4.1") (default-features #t) (kind 0)) (crate-dep (name "float-cmp") (req "^0.4") (default-features #t) (kind 2)))) (hash "1mh22nwja3v8922h0hq77c29k1da634lvkn9cvg9xrqhmqlk7q5x")))

(define-public crate-dtoa-short-0.3 (crate (name "dtoa-short") (vers "0.3.4") (deps (list (crate-dep (name "dtoa") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "float-cmp") (req "^0.4") (default-features #t) (kind 2)))) (hash "0x4c8hxc041j9a85371gxdmi09q5d2whz05iwxwiq8g4qv1yxb6v")))

