(define-module (crates-io dt mf) #:use-module (crates-io))

(define-public crate-dtmf-0.1 (crate (name "dtmf") (vers "0.1.0") (deps (list (crate-dep (name "microfft") (req "^0.4.0") (default-features #t) (kind 0)) (crate-dep (name "num_enum") (req "^0.5") (default-features #t) (kind 0)) (crate-dep (name "wav") (req "^1.0") (default-features #t) (kind 2)))) (hash "1mm2dcpnk858pib4x1gam9p1dd5scvbfr38ms02z3j1x05ig389d")))

(define-public crate-dtmf-0.1 (crate (name "dtmf") (vers "0.1.1") (deps (list (crate-dep (name "microfft") (req "^0.4.0") (default-features #t) (kind 0)) (crate-dep (name "num_enum") (req "^0.5") (default-features #t) (kind 0)) (crate-dep (name "wav") (req "^1.0") (default-features #t) (kind 2)))) (hash "1ncwsd8wjf96ki6fjl1y2n1508k7g3ajzkw9cqkyyjdflpdam1xh")))

(define-public crate-dtmf-0.1 (crate (name "dtmf") (vers "0.1.2") (deps (list (crate-dep (name "microfft") (req "^0.4.0") (default-features #t) (kind 0)) (crate-dep (name "num_enum") (req "^0.5") (default-features #t) (kind 0)) (crate-dep (name "wav") (req "^1.0") (default-features #t) (kind 2)))) (hash "0vzh7arc4lw33nadfkjf0hjgls14zc2idpmjcx43b7p11dwa7k5s")))

(define-public crate-dtmf-0.1 (crate (name "dtmf") (vers "0.1.3") (deps (list (crate-dep (name "criterion") (req "^0.3.5") (default-features #t) (kind 2)) (crate-dep (name "goertzel-nostd") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "num_enum") (req "^0.5") (default-features #t) (kind 0)) (crate-dep (name "rodio") (req "^0.15.0") (default-features #t) (kind 2)))) (hash "12wlcfd1yblq8f569421b9bd1f23hrw0r595nmy76q1z2a4fix5f")))

(define-public crate-dtmf-0.1 (crate (name "dtmf") (vers "0.1.4") (deps (list (crate-dep (name "criterion") (req "^0.3.5") (default-features #t) (kind 2)) (crate-dep (name "goertzel-nostd") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "num_enum") (req "^0.5") (default-features #t) (kind 0)) (crate-dep (name "rodio") (req "^0.15.0") (default-features #t) (kind 2)))) (hash "0vax5wfpm320dmmbn80wf6k215wdb0frj24i3hk28brjyrl2wi3k")))

(define-public crate-dtmf-0.1 (crate (name "dtmf") (vers "0.1.5") (deps (list (crate-dep (name "criterion") (req "^0.3.5") (default-features #t) (kind 2)) (crate-dep (name "goertzel-nostd") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "rodio") (req "^0.15.0") (default-features #t) (kind 2)))) (hash "0zipg7xn2mmhghxd045x2ddk95fc6rwl6wykw73qf50fw0y5v1j2")))

