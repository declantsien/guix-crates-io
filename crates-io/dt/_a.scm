(define-module (crates-io dt _a) #:use-module (crates-io))

(define-public crate-dt_analysis-0.0.1 (crate (name "dt_analysis") (vers "0.0.1") (deps (list (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "num") (req "^0.2.1") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.7.3") (default-features #t) (kind 0)))) (hash "053rm4qsp2caz4bpphi40rp4fi443kh3b0ds7hclh9mxls03dbis") (yanked #t)))

(define-public crate-dt_analysis-0.0.2 (crate (name "dt_analysis") (vers "0.0.2") (deps (list (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "num") (req "^0.2.1") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.7.3") (default-features #t) (kind 0)))) (hash "107a8rdj4gcrlx41wfnkg64ajjf0z7sd9jdyhbzcch4ilrdc6vm3") (yanked #t)))

(define-public crate-dt_analysis-0.0.3 (crate (name "dt_analysis") (vers "0.0.3") (deps (list (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "num") (req "^0.2.1") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.7.3") (default-features #t) (kind 0)))) (hash "0zcmn2s3b77napwzbl80iyq1cx0ybp4d503vkspc7mb4hm7pyn57") (yanked #t)))

(define-public crate-dt_analysis-0.1 (crate (name "dt_analysis") (vers "0.1.0") (deps (list (crate-dep (name "csv") (req "^1.1.3") (default-features #t) (kind 2)) (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "num") (req "^0.2.1") (default-features #t) (kind 0)) (crate-dep (name "plotly") (req "^0.4.0") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.7.3") (default-features #t) (kind 0)))) (hash "0i4zvrs85x48y18hbs8rrclqp9y0p98cjgyn8k0im0c9vhfjfqis") (yanked #t)))

