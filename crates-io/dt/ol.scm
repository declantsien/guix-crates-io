(define-module (crates-io dt ol) #:use-module (crates-io))

(define-public crate-dtolnay-0.0.1 (crate (name "dtolnay") (vers "0.0.1") (hash "0l037aslhapfz7nvw79amr7y2c1ddiccpnsdlll2b1japybfyzcd")))

(define-public crate-dtolnay-0.0.2 (crate (name "dtolnay") (vers "0.0.2") (hash "1ma56kpxc532m6jbcyffdp4vz88cpq1p7ss7b2a74javn4yc1nw8")))

(define-public crate-dtolnay-0.0.3 (crate (name "dtolnay") (vers "0.0.3") (hash "02521bqm63jbdhmz56fc85g6cn1v0dxhlsgk7v69xwzqcv24q15b")))

(define-public crate-dtolnay-0.0.4 (crate (name "dtolnay") (vers "0.0.4") (hash "0wmv1gnxvs14ak4j5pi6w4nvc6f9sn8wjac7zy803rqc41587ms0")))

(define-public crate-dtolnay-0.0.5 (crate (name "dtolnay") (vers "0.0.5") (hash "1nc84l9md7hp9s9inh7hqddrmmdnkzz6sc3i96q68p6avqnaldph")))

(define-public crate-dtolnay-0.0.6 (crate (name "dtolnay") (vers "0.0.6") (deps (list (crate-dep (name "futures") (req "^0.1") (default-features #t) (kind 2)))) (hash "0r5zv3j87fgm1xvrpxv2z9j70ddfhn11kkq5lan368jln25wznki")))

(define-public crate-dtolnay-0.0.7 (crate (name "dtolnay") (vers "0.0.7") (deps (list (crate-dep (name "futures") (req "^0.1") (default-features #t) (kind 2)))) (hash "09npf7c1sgp1xz4swzzdvp0k0j60s8p3j5d04q6qbrdphjcwpp61")))

(define-public crate-dtolnay-0.0.8 (crate (name "dtolnay") (vers "0.0.8") (deps (list (crate-dep (name "futures") (req "^0.1") (default-features #t) (kind 2)))) (hash "06wjc732rwmym84wa5jbjm00a22ksavkszpmij122ka9zs9csv6q")))

(define-public crate-dtolnay-0.0.9 (crate (name "dtolnay") (vers "0.0.9") (deps (list (crate-dep (name "futures") (req "^0.1") (default-features #t) (kind 2)))) (hash "179y6sqcdr6kwaq6izw4y405mh58y3h6g5qc97ppmwz7bldp4aa5")))

(define-public crate-dtolnay-0.0.10 (crate (name "dtolnay") (vers "0.0.10") (deps (list (crate-dep (name "futures") (req "^0.1") (default-features #t) (kind 2)))) (hash "0xqqslk9y5mqszshnzszi51lh0pdhz78h5qs04ffmp26zyjglg66")))

(define-public crate-dtolnay-0.0.11 (crate (name "dtolnay") (vers "0.0.11") (deps (list (crate-dep (name "futures01") (req "^0.1") (default-features #t) (kind 2) (package "futures")))) (hash "1jgq4125q6rn292pfgbzgywqv1d5jsbrvpdds90zwqbfh1mbmpww")))

