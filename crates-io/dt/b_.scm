(define-module (crates-io dt b_) #:use-module (crates-io))

(define-public crate-dtb_parser-0.1 (crate (name "dtb_parser") (vers "0.1.0") (hash "1ib4cgzfb0zdy0ki78h843dh648pi24qpjri3k1qfb5xadbig5vv") (features (quote (("default"))))))

(define-public crate-dtb_parser-0.1 (crate (name "dtb_parser") (vers "0.1.1") (hash "1f14zyhy725n8jg8wpf2ajnzbxvfippcchw4v8nnwr32z6pgd31r") (features (quote (("default"))))))

(define-public crate-dtb_parser-0.1 (crate (name "dtb_parser") (vers "0.1.2") (hash "1vw0i32pckrj9qmcjvmxg8gy96g8dhkvx3irdd8kzib8kjvk05g2")))

(define-public crate-dtb_parser-0.2 (crate (name "dtb_parser") (vers "0.2.0") (hash "0lmg8m6qwqfpb79apw46znx63irg3j1priiq49h3ab1iv5ldcykj")))

(define-public crate-dtb_parser-0.2 (crate (name "dtb_parser") (vers "0.2.1") (hash "0i9di95hvv2xaza18xv9zvhhgwp5plnl7h87xyls0frv1h1cnkc2") (features (quote (("std") ("default" "std"))))))

(define-public crate-dtb_parser-0.2 (crate (name "dtb_parser") (vers "0.2.2") (hash "1z3hyhaxl9scnia0g9v7a2wcbwm1h6b4i8z6rp76nirp9c5f0kkk") (features (quote (("std") ("default" "std"))))))

(define-public crate-dtb_parser-0.2 (crate (name "dtb_parser") (vers "0.2.3") (hash "0b5mi3djrv3jqdp3ykzg6z2m0znkzzn4smkm7fkay6szidsj8s47") (features (quote (("std") ("default" "std"))))))

