(define-module (crates-io dt s_) #:use-module (crates-io))

(define-public crate-dts_viewer-0.1 (crate (name "dts_viewer") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^2") (default-features #t) (kind 0)) (crate-dep (name "nom") (req "^2") (features (quote ("verbose-errors"))) (default-features #t) (kind 0)))) (hash "0hhdcq4y3ks0mj0a0qg9jq0dscifppgn1gim5psx461wgknnwjgc")))

(define-public crate-dts_viewer-0.2 (crate (name "dts_viewer") (vers "0.2.0") (deps (list (crate-dep (name "clap") (req "^2") (default-features #t) (kind 0)) (crate-dep (name "device_tree_source") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "nom") (req "^2") (features (quote ("verbose-errors"))) (default-features #t) (kind 0)))) (hash "1kda3bffy9slfjsi9jpyh5m3a2zy5d834rb7sfvy4khpxnxmr6s4")))

(define-public crate-dts_viewer-0.3 (crate (name "dts_viewer") (vers "0.3.0") (deps (list (crate-dep (name "clap") (req "^2") (default-features #t) (kind 0)) (crate-dep (name "device_tree_source") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "mktemp") (req "^0.3") (default-features #t) (kind 0)))) (hash "1ibgz2vgdhis175xw4vs733dxyyqnmg3gp49c7091b6qvsbmfack")))

