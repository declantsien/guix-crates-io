(define-module (crates-io ue ig) #:use-module (crates-io))

(define-public crate-ueight-0.1 (crate (name "ueight") (vers "0.1.0") (hash "1dgzzjqwiwzwkwhjcv8v15scbwj8wcknjcb1ip5c76sbix2y96p0") (yanked #t)))

(define-public crate-ueight-0.1 (crate (name "ueight") (vers "0.1.1") (hash "041ssg5xz7rb2aaiad7jh3rb76s4lm45c389v6gqgs6s6arlkm8x") (yanked #t)))

(define-public crate-ueight-0.2 (crate (name "ueight") (vers "0.2.0") (hash "0s3ihiv2gs82xgdlgyys7dhbq4g3pbcl46b9snlpak81q14yqq3y") (yanked #t)))

(define-public crate-ueight-0.2 (crate (name "ueight") (vers "0.2.1") (hash "1c8m2j20sz3z18jhllmaj38gii9ihf5x4y39xhq7w2r5f4hp4kcy") (yanked #t)))

(define-public crate-ueight-0.2 (crate (name "ueight") (vers "0.2.2") (hash "15fdy5dw14s33bnphx8fd9ll2n65i2228qh2jiw4hmcqqd6l4vla") (yanked #t)))

(define-public crate-ueight-0.2 (crate (name "ueight") (vers "0.2.3") (hash "0f8m1964mqbxj181k0vlbamw9in721l1j1imgvbg4hq3mnlhbf4l") (yanked #t)))

(define-public crate-ueight-0.2 (crate (name "ueight") (vers "0.2.4") (hash "0i46qi3q4rdpz8ilqw414l328vmzhcjiakliznxf06zf606cw3lx") (yanked #t)))

(define-public crate-ueight-0.2 (crate (name "ueight") (vers "0.2.5") (hash "1difkpp5p5ink3ap7c0pqziwz188bwg6xgm7z53fan95lj5f62pc") (yanked #t)))

(define-public crate-ueight-0.2 (crate (name "ueight") (vers "0.2.6") (hash "0rvkjl2b8a7d32dlvxr3wjrxin55iw5d1w3wkzd0w7rf6i0gx0dy") (yanked #t)))

(define-public crate-ueight-0.2 (crate (name "ueight") (vers "0.2.7") (hash "0lf03c65xzz8k8w5djviifnwir0wh6qqhj7hswdlqghxs2l12ffi")))

