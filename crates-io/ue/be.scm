(define-module (crates-io ue be) #:use-module (crates-io))

(define-public crate-uebersetzt-0.1 (crate (name "uebersetzt") (vers "0.1.0") (deps (list (crate-dep (name "serde") (req "^1.0.39") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.39") (default-features #t) (kind 0)))) (hash "059bg8g1kdvzmbmj9z8g610jfd7a3804i632gip034isjq702iqv")))

(define-public crate-uebersetzt-0.1 (crate (name "uebersetzt") (vers "0.1.1") (deps (list (crate-dep (name "serde") (req "^1.0.39") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.39") (default-features #t) (kind 0)))) (hash "0n2l1lcymg648gipinkq37g9fkb7j35x8d1qy5h22hh4i2qwfbx8")))

(define-public crate-uebersetzt-0.1 (crate (name "uebersetzt") (vers "0.1.2") (deps (list (crate-dep (name "serde") (req "^1.0.39") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.39") (default-features #t) (kind 0)) (crate-dep (name "strfmt") (req "^0.1.6") (default-features #t) (kind 0)))) (hash "10pcwj19wzv582sminwljhca2jq0jh37pbg22ryv55gbvqd1hxfa")))

(define-public crate-uebersetzt-0.1 (crate (name "uebersetzt") (vers "0.1.3") (deps (list (crate-dep (name "serde") (req "^1.0.39") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.39") (default-features #t) (kind 0)) (crate-dep (name "strfmt") (req "^0.1.6") (default-features #t) (kind 0)))) (hash "18y0arhjnpa4hb8ra830viqzjc976dysjngjnmqgkjax49m0aa8k")))

(define-public crate-ueberzug-0.1 (crate (name "ueberzug") (vers "0.1.0") (hash "1smavwh3n3q3fmd3z5ag6dd8jj4p412125xbnjahqa457brxynk5")))

