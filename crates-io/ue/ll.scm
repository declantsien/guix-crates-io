(define-module (crates-io ue ll) #:use-module (crates-io))

(define-public crate-uell-0.1 (crate (name "uell") (vers "0.1.0") (deps (list (crate-dep (name "bumpalo") (req "^3.5.0") (features (quote ("boxed"))) (default-features #t) (kind 0)) (crate-dep (name "quickcheck") (req "^1") (default-features #t) (kind 2)) (crate-dep (name "quickcheck_macros") (req "^1") (default-features #t) (kind 2)))) (hash "1msg9czq995d345w2zrwmxngcx4vapqq2zg7601y44l6wa15kpj0")))

