(define-module (crates-io ue nc) #:use-module (crates-io))

(define-public crate-uenc-0.1 (crate (name "uenc") (vers "0.1.1") (deps (list (crate-dep (name "url") (req "^0.5.7") (default-features #t) (kind 0)))) (hash "0aal35704d2kjsnkac9imp7r1rviviq0jq4b2m62dny3l2w9a9v3")))

(define-public crate-uenc-0.1 (crate (name "uenc") (vers "0.1.2") (deps (list (crate-dep (name "getopts") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "url") (req "^0.5.7") (default-features #t) (kind 0)))) (hash "0ikdnhav91cgwy8a2vgnqmpll22wm76rk47q851g6mbpb11cprw0")))

