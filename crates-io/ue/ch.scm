(define-module (crates-io ue ch) #:use-module (crates-io))

(define-public crate-uecho-0.1 (crate (name "uecho") (vers "0.1.0") (deps (list (crate-dep (name "itertools") (req "^0.10.3") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.3.26") (default-features #t) (kind 0)))) (hash "0g5sf4j3lnp1a9i79msm63pzm96684rmj3baybxzrxp5ljs8zkjl")))

