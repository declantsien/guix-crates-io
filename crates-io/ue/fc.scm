(define-module (crates-io ue fc) #:use-module (crates-io))

(define-public crate-uefc-0.1 (crate (name "uefc") (vers "0.1.0") (deps (list (crate-dep (name "indicatif") (req "^0.17.3") (features (quote ("rayon"))) (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)) (crate-dep (name "rayon") (req "^1.7.0") (default-features #t) (kind 0)))) (hash "1kbvlsfbfby4jxdcx73q4yfpk4wain7mia2cxih88xvpsl6nkpfw")))

(define-public crate-uefc-0.2 (crate (name "uefc") (vers "0.2.0") (deps (list (crate-dep (name "indicatif") (req "^0.17.3") (features (quote ("rayon"))) (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)) (crate-dep (name "rayon") (req "^1.7.0") (default-features #t) (kind 0)))) (hash "1q0r59fc497wpx2gnzidy80hg1d9zlhx6b7lbk8zx09pxd40gr60")))

(define-public crate-uefc-0.2 (crate (name "uefc") (vers "0.2.1") (deps (list (crate-dep (name "indicatif") (req "^0.17.3") (features (quote ("rayon"))) (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)) (crate-dep (name "rayon") (req "^1.7.0") (default-features #t) (kind 0)))) (hash "184g7wgwk9r6g0mwz0rxbyfp57zhkpjcddjjh1ahxjw74c0ndawk")))

