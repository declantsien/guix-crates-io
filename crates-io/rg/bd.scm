(define-module (crates-io rg bd) #:use-module (crates-io))

(define-public crate-rgbd-0.1 (crate (name "rgbd") (vers "0.1.0") (deps (list (crate-dep (name "anyhow") (req "^1.0.82") (default-features #t) (kind 0)) (crate-dep (name "directories") (req "^5.0.1") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.21") (default-features #t) (kind 0)) (crate-dep (name "ureq") (req "^2.9.7") (default-features #t) (kind 0)) (crate-dep (name "url") (req "^2.5.0") (default-features #t) (kind 0)) (crate-dep (name "xz2") (req "^0.1.7") (default-features #t) (kind 0)))) (hash "1z10lf5ddgp5r2yrwsmnfyhv39zrs4mgwb6q3hazc06p81bxzn7w")))

(define-public crate-rgbd-0.2 (crate (name "rgbd") (vers "0.2.0") (deps (list (crate-dep (name "anyhow") (req "^1.0.82") (default-features #t) (kind 0)) (crate-dep (name "directories") (req "^5.0.1") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.21") (default-features #t) (kind 0)) (crate-dep (name "rusqlite") (req "^0.31.0") (features (quote ("bundled"))) (default-features #t) (kind 0)) (crate-dep (name "ureq") (req "^2.9.7") (default-features #t) (kind 0)) (crate-dep (name "url") (req "^2.5.0") (default-features #t) (kind 0)) (crate-dep (name "xz2") (req "^0.1.7") (default-features #t) (kind 0)))) (hash "0m6fsy9i8hs2qnxahdyarqnf1njpx1q4zkjbnddxzha6yhkcxcfb")))

(define-public crate-rgbd-0.2 (crate (name "rgbd") (vers "0.2.1") (deps (list (crate-dep (name "anyhow") (req "^1.0.82") (default-features #t) (kind 0)) (crate-dep (name "directories") (req "^5.0.1") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.21") (default-features #t) (kind 0)) (crate-dep (name "rusqlite") (req "^0.31.0") (features (quote ("bundled"))) (default-features #t) (kind 0)) (crate-dep (name "ureq") (req "^2.9.7") (default-features #t) (kind 0)) (crate-dep (name "url") (req "^2.5.0") (default-features #t) (kind 0)) (crate-dep (name "xz2") (req "^0.1.7") (default-features #t) (kind 0)))) (hash "0mmasyr8xd9yrw9bn3a0ypc7jnprhkz7hi4fzbjfvx2ia9r7mh8d")))

(define-public crate-rgbds-obj-0.1 (crate (name "rgbds-obj") (vers "0.1.0") (hash "0cmj6wn3gm08nk7dijgm886l3p77nzfc80hg810xj76gg49dxyn1")))

(define-public crate-rgbds-obj-0.1 (crate (name "rgbds-obj") (vers "0.1.1") (hash "1w7wjhzdzqh2l4w26dj07rkj9sd3rws67ki2zn7yzb6crf9dggg3")))

