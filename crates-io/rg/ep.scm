(define-module (crates-io rg ep) #:use-module (crates-io))

(define-public crate-rgep-0.1 (crate (name "rgep") (vers "0.1.0") (deps (list (crate-dep (name "ansi_term") (req "^0.6.3") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.3.16") (default-features #t) (kind 0)))) (hash "1wm3gb19xqpa35fa52m2zch7rghfm2w6p9fhhd1gm9cvsbwfzpng")))

