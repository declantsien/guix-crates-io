(define-module (crates-io rg mk) #:use-module (crates-io))

(define-public crate-rgmk-0.1 (crate (name "rgmk") (vers "0.1.0") (deps (list (crate-dep (name "byteorder") (req "^0.3.13") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.3.3") (features (quote ("release_max_level_info"))) (default-features #t) (kind 0)) (crate-dep (name "quick-error") (req "^0.1.4") (default-features #t) (kind 0)))) (hash "0psnb88bnh4kpnpy3yzwx94vd0z5kc3sqsr8qq8i24ckpwvgm4qh")))

(define-public crate-rgmk-0.2 (crate (name "rgmk") (vers "0.2.0") (deps (list (crate-dep (name "byteorder") (req "^0.3.13") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.3.3") (features (quote ("release_max_level_info"))) (default-features #t) (kind 0)) (crate-dep (name "quick-error") (req "^0.1.4") (default-features #t) (kind 0)))) (hash "18296d4r796fnf81ws729lb1n03ywdyirayn9423ksq6cv2l2syl")))

(define-public crate-rgmk-0.3 (crate (name "rgmk") (vers "0.3.0") (deps (list (crate-dep (name "byteorder") (req "^1") (default-features #t) (kind 0)))) (hash "11g96gkfnvld2id7swc3g7hcmqzh7s82rvdw1hvrj6m617gls64k")))

