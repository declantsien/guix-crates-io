(define-module (crates-io rg pa) #:use-module (crates-io))

(define-public crate-rgparse-0.1 (crate (name "rgparse") (vers "0.1.0") (hash "0znni1zr08xvks7fnn3i37yhav0wpar2rnc2360fag3x8lmjpng5")))

(define-public crate-rgparse-0.1 (crate (name "rgparse") (vers "0.1.1") (hash "14fysbig5a3x98ccxs8j179kgaij6vg86qrdgk09nmdd44g0swmb")))

(define-public crate-rgparse-0.1 (crate (name "rgparse") (vers "0.1.2") (hash "1a0m3q1fxhl4igi7p29j5b94n79aq0svfgqljy1ikhzg346lzjbg")))

