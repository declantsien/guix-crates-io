(define-module (crates-io rg fs) #:use-module (crates-io))

(define-public crate-rgfs-0.1 (crate (name "rgfs") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^4.4.6") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.33.0") (features (quote ("fs" "macros" "rt" "rt-multi-thread"))) (default-features #t) (kind 0)))) (hash "1kc01chigw9qwajdv4pc3zydlq037nwqrm2prr2h5ihim9sw2isz")))

