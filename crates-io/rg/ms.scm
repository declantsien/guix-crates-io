(define-module (crates-io rg ms) #:use-module (crates-io))

(define-public crate-rgmsh-0.1 (crate (name "rgmsh") (vers "0.1.0") (deps (list (crate-dep (name "criterion") (req "^0.2") (default-features #t) (kind 2)) (crate-dep (name "gmsh-sys") (req "^0.1") (default-features #t) (kind 0)))) (hash "1zh27hsy22s9nldv4khpzzsxkhnr7yihyrhrlcpqkprp1gma0z72")))

(define-public crate-rgmsh-0.1 (crate (name "rgmsh") (vers "0.1.1") (deps (list (crate-dep (name "criterion") (req "^0.2") (default-features #t) (kind 2)) (crate-dep (name "gmsh-sys") (req "^0.1.2") (default-features #t) (kind 0)))) (hash "0d0v8wyirvljxn184vrs5kjpr805c383iiflbfcn65qxvggd5l32")))

(define-public crate-rgmsh-0.1 (crate (name "rgmsh") (vers "0.1.2") (deps (list (crate-dep (name "criterion") (req "^0.2") (default-features #t) (kind 2)) (crate-dep (name "gmsh-sys") (req "^0.1.2") (default-features #t) (kind 0)))) (hash "0s657fn52n4953qqgk351gf55hgyf2fqdkz3n8xzhzmh5msf7fhg")))

