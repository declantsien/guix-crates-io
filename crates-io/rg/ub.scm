(define-module (crates-io rg ub) #:use-module (crates-io))

(define-public crate-rgubin-0.1 (crate (name "rgubin") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^3.1.18") (default-features #t) (kind 0)) (crate-dep (name "jlogger") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.17") (default-features #t) (kind 0)))) (hash "0g4rsb46c04lxg5hmiq9b2qzfi5airxfzw2q32pm11sq8ylg4z2l")))

(define-public crate-rgubin-0.1 (crate (name "rgubin") (vers "0.1.1") (deps (list (crate-dep (name "clap") (req "^3.1.18") (default-features #t) (kind 0)) (crate-dep (name "jlogger") (req "^0.1.7") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.17") (default-features #t) (kind 0)))) (hash "177z6nf55xcmw3i3m9d0lxgd5zsb1lkpy0rkxmhm59car524055r")))

