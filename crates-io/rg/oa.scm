(define-module (crates-io rg oa) #:use-module (crates-io))

(define-public crate-rgoap-0.1 (crate (name "rgoap") (vers "0.1.0") (deps (list (crate-dep (name "pathfinding") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^0.9") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^0.9") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^0.9") (default-features #t) (kind 2)))) (hash "0cx07smdzrnghb0mnnaw3v8jcbhf331s1718nh8y8bbnf9jkk9sf")))

