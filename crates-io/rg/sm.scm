(define-module (crates-io rg sm) #:use-module (crates-io))

(define-public crate-rgsm-1 (crate (name "rgsm") (vers "1.0.0") (deps (list (crate-dep (name "clap") (req "^4.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.10.4") (default-features #t) (kind 0)))) (hash "1px8vw04c8vbmb0r436r72jvprl51vrvhdz5r7sjfrcwiq5kvcly")))

(define-public crate-rgsm-1 (crate (name "rgsm") (vers "1.0.1") (deps (list (crate-dep (name "clap") (req "^4.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.10.4") (default-features #t) (kind 0)))) (hash "19gr9zfrxmbadhgnjsp3l7d550dw1zbh3326k4hclzmqnkh3hqiq")))

(define-public crate-rgsm-1 (crate (name "rgsm") (vers "1.0.2") (deps (list (crate-dep (name "clap") (req "^4.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.10.4") (default-features #t) (kind 0)))) (hash "1103jph04r44irfkpcwb35rxhkf9n9qysn3xf66dafyix461qxaf")))

(define-public crate-rgsm-1 (crate (name "rgsm") (vers "1.0.3") (deps (list (crate-dep (name "clap") (req "^4.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.10.4") (default-features #t) (kind 0)))) (hash "1d824fyz962kbmhd6i46d8radd2j06q648nk1q0bi3cfpq5jd5lr")))

(define-public crate-rgsm-1 (crate (name "rgsm") (vers "1.0.4") (deps (list (crate-dep (name "clap") (req "^4.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.10.4") (default-features #t) (kind 0)))) (hash "007gfbwy5bqk6dgy80l8gxybrr2x9q5jfjn5198r522j96n24gdy")))

(define-public crate-rgsm-1 (crate (name "rgsm") (vers "1.0.5") (deps (list (crate-dep (name "clap") (req "^4.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.10.4") (default-features #t) (kind 0)))) (hash "1hzxqg1flil1fc2q0i04knmvkbk2b78igvhml3nsmy0qp2iwxgm7")))

