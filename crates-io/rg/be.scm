(define-module (crates-io rg be) #:use-module (crates-io))

(define-public crate-rgbe-0.0.1 (crate (name "rgbe") (vers "0.0.1") (deps (list (crate-dep (name "bytemuck") (req "^1.14.3") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "half") (req "^2.3.1") (features (quote ("bytemuck"))) (default-features #t) (kind 0)) (crate-dep (name "image") (req "^0.24.8") (features (quote ("png" "hdr"))) (kind 0)))) (hash "1sd4b4p6c6jnwq588bj6vszywkwsgz0j71bv20z1f7aami2g9s1d")))

