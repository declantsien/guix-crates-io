(define-module (crates-io rg ui) #:use-module (crates-io))

(define-public crate-rgui-0.1 (crate (name "rgui") (vers "0.1.0") (deps (list (crate-dep (name "druid") (req "^0.7.0") (features (quote ("png"))) (default-features #t) (kind 0)) (crate-dep (name "err-derive") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "pest") (req "^2.1") (default-features #t) (kind 0)) (crate-dep (name "pest_derive") (req "^2.1") (default-features #t) (kind 0)))) (hash "10j2pzidylsp3vakj13wv1df3sqfa5s83c13lvdynlr5gaiywccn")))

