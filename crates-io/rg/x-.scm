(define-module (crates-io rg x-) #:use-module (crates-io))

(define-public crate-rgx-lyon-0.1 (crate (name "rgx-lyon") (vers "0.1.0") (deps (list (crate-dep (name "lyon") (req "^0.16") (default-features #t) (kind 0)) (crate-dep (name "rgx") (req "^0.8") (features (quote ("renderer"))) (default-features #t) (kind 0)) (crate-dep (name "winit") (req "^0.22") (default-features #t) (kind 2)))) (hash "1ss8arxvbsnxmi8k958dkf9fbq4zg9gmw61j3s4hc54hv7v2jd5n")))

(define-public crate-rgx-lyon-0.1 (crate (name "rgx-lyon") (vers "0.1.1") (deps (list (crate-dep (name "lyon_tessellation") (req "^0.16") (default-features #t) (kind 0)) (crate-dep (name "rgx") (req "^0.8") (features (quote ("renderer"))) (default-features #t) (kind 0)) (crate-dep (name "winit") (req "^0.22") (default-features #t) (kind 2)))) (hash "14kafrxndhndj3ik5f8lab1v9yk6lppiis97prp1ykb7vnc3ry9y")))

(define-public crate-rgx-lyon-0.1 (crate (name "rgx-lyon") (vers "0.1.2") (deps (list (crate-dep (name "lyon_tessellation") (req "^0.16") (default-features #t) (kind 0)) (crate-dep (name "rgx") (req "^0.8") (features (quote ("renderer"))) (default-features #t) (kind 0)) (crate-dep (name "winit") (req "^0.22") (default-features #t) (kind 2)))) (hash "1az6w6fvlbgilv5dyk93rd3hqb1wzl5f8xwm8dhkp7z8bwpcciz3")))

