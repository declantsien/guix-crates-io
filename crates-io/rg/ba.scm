(define-module (crates-io rg ba) #:use-module (crates-io))

(define-public crate-rgba32-0.1 (crate (name "rgba32") (vers "0.1.0") (deps (list (crate-dep (name "serde") (req "^1.0") (features (quote ("serde_derive"))) (optional #t) (default-features #t) (kind 0)))) (hash "13k69vz5aqy2qkizd2y1lcqkwnyvj2gi2p6q0g6yd167lypdpds1") (features (quote (("serialize" "serde"))))))

(define-public crate-rgba32-0.1 (crate (name "rgba32") (vers "0.1.1") (deps (list (crate-dep (name "serde") (req "^1.0") (features (quote ("serde_derive"))) (optional #t) (default-features #t) (kind 0)))) (hash "0npj68x17ds76vgmls0cvs0wygjln9b8g0zlsrj4aaa6bhbsr7gr") (features (quote (("serialize" "serde"))))))

(define-public crate-rgba32-0.1 (crate (name "rgba32") (vers "0.1.2") (deps (list (crate-dep (name "serde") (req "^1.0") (features (quote ("serde_derive"))) (optional #t) (default-features #t) (kind 0)))) (hash "1ac3s3057qijj38yspj7d24cdh0b2i8mf7caann9nh7b10m9kqnc") (features (quote (("serialize" "serde"))))))

(define-public crate-rgba32-0.1 (crate (name "rgba32") (vers "0.1.3") (deps (list (crate-dep (name "serde") (req "^1.0") (features (quote ("serde_derive"))) (optional #t) (default-features #t) (kind 0)))) (hash "1f9idspq3mn87v451c8ds3bd6b9yp8s39sbiq5zrl0lb5kdr5rrn") (features (quote (("serialize" "serde"))))))

(define-public crate-rgba32-0.1 (crate (name "rgba32") (vers "0.1.4") (deps (list (crate-dep (name "serde") (req "^1.0") (features (quote ("serde_derive"))) (optional #t) (default-features #t) (kind 0)))) (hash "0karfwil7s2xask342kg2wcnws8d8m0l0x05xlaina1xjrv0hsn8") (features (quote (("serialize" "serde"))))))

(define-public crate-rgba32-0.1 (crate (name "rgba32") (vers "0.1.5") (deps (list (crate-dep (name "serde") (req "^1.0") (features (quote ("serde_derive"))) (optional #t) (default-features #t) (kind 0)))) (hash "17ymjgzzycvpyqyw8ks1g103wxnmi9r5zam2299fvhimspss58dh") (features (quote (("serialize" "serde"))))))

(define-public crate-rgba32-0.1 (crate (name "rgba32") (vers "0.1.6") (deps (list (crate-dep (name "serde") (req "^1.0") (features (quote ("serde_derive"))) (optional #t) (default-features #t) (kind 0)))) (hash "0xdz1dizqzvvihbvbps1r7gsngmh8mc9n0k3h2p1cr4rvxmc071n") (features (quote (("serialize" "serde"))))))

(define-public crate-rgba32-0.1 (crate (name "rgba32") (vers "0.1.7") (deps (list (crate-dep (name "serde") (req "^1.0") (features (quote ("serde_derive"))) (optional #t) (default-features #t) (kind 0)))) (hash "0ng4k0sa10dii2pdzmhxhr4ys7yq4147zaf94v6pjxm3qh72qrg6") (features (quote (("serialize" "serde"))))))

(define-public crate-rgba32-0.1 (crate (name "rgba32") (vers "0.1.8") (deps (list (crate-dep (name "serde") (req "^1.0") (features (quote ("serde_derive"))) (optional #t) (default-features #t) (kind 0)))) (hash "12ylgyk25fzvwg7r8344zqhfjhm46vi75zpk5vw237nb8684iam0") (features (quote (("serialize" "serde"))))))

(define-public crate-rgba32-0.1 (crate (name "rgba32") (vers "0.1.9") (deps (list (crate-dep (name "serde") (req "^1.0") (features (quote ("serde_derive"))) (optional #t) (default-features #t) (kind 0)))) (hash "0qxz62vlgq7r1zkrw2yv83naj138hy8497jnjd4y49f20hpklf8s") (features (quote (("serialize" "serde"))))))

(define-public crate-rgba32-0.1 (crate (name "rgba32") (vers "0.1.10") (deps (list (crate-dep (name "serde") (req "^1.0") (features (quote ("serde_derive"))) (optional #t) (default-features #t) (kind 0)))) (hash "0qxkmkkyq5hq3yq9zgbn7yg82w11m2p11lga71m8b7gfnfpbnlbg") (features (quote (("serialize" "serde"))))))

(define-public crate-rgba8888-to-rgb332-0.1 (crate (name "rgba8888-to-rgb332") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^4.3.22") (features (quote ("derive" "cargo"))) (default-features #t) (kind 0)) (crate-dep (name "image") (req "^0.24.7") (features (quote ("png"))) (kind 0)))) (hash "1lhnz0ivqd9c5snvvh9gw6bh6rdy5snr19i9ssr90pa1wcj7jli1")))

(define-public crate-rgba_simple-0.3 (crate (name "rgba_simple") (vers "0.3.2") (deps (list (crate-dep (name "gtk") (req "~0.3") (optional #t) (default-features #t) (kind 0) (package "gtk4")) (crate-dep (name "hex") (req "~0.4") (features (quote ("std"))) (kind 0)) (crate-dep (name "serde") (req "~1.0") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "1gnawvx7402zhqczp7rz0grkmlzi6y82qbgdb05kndrxirh4acmj")))

(define-public crate-rgba_simple-0.3 (crate (name "rgba_simple") (vers "0.3.3") (deps (list (crate-dep (name "gtk") (req "~0.3") (optional #t) (default-features #t) (kind 0) (package "gtk4")) (crate-dep (name "hex") (req "~0.4") (features (quote ("std"))) (kind 0)) (crate-dep (name "serde") (req "~1.0") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "101lczqxgc6ql9p9fwvqs0n6ymaakain12mrn0a9wag9jjdm12g2")))

(define-public crate-rgba_simple-0.4 (crate (name "rgba_simple") (vers "0.4.0") (deps (list (crate-dep (name "gdk") (req "~0.3") (optional #t) (default-features #t) (kind 0) (package "gdk4")) (crate-dep (name "serde") (req "~1.0") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "1jag10sbz7ji3fb3gik493a285j051vcvdh5kq8a9yavs6qy5ybn")))

(define-public crate-rgba_simple-0.4 (crate (name "rgba_simple") (vers "0.4.1") (deps (list (crate-dep (name "gdk") (req "~0.3") (optional #t) (default-features #t) (kind 0) (package "gdk4")) (crate-dep (name "serde") (req "~1.0") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "15w0b7rgylzcsja98j9dd22by9sbyk0pcb2gqbrzc7g2sy1h8awm")))

(define-public crate-rgba_simple-0.5 (crate (name "rgba_simple") (vers "0.5.0") (deps (list (crate-dep (name "gdk") (req "~0.4") (optional #t) (default-features #t) (kind 0) (package "gdk4")) (crate-dep (name "serde") (req "~1.0") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "1cnnrx7k68fq7acgyqcyy7ldwm2yahk18rjxai8z3h7aga5fcjs2")))

(define-public crate-rgba_simple-0.5 (crate (name "rgba_simple") (vers "0.5.1") (deps (list (crate-dep (name "gdk") (req "~0.4") (optional #t) (default-features #t) (kind 0) (package "gdk4")) (crate-dep (name "serde") (req "~1.0") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "172xfjdi7dikkzv0jkhwm5ncn72p5qahwqbhm60dm6fjwamj57f0")))

(define-public crate-rgba_simple-0.6 (crate (name "rgba_simple") (vers "0.6.0") (deps (list (crate-dep (name "gdk") (req "~0.4") (optional #t) (default-features #t) (kind 0) (package "gdk4")) (crate-dep (name "serde") (req "~1.0") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "06jdjyvakflvwsiwvb9jw5qmj5019afhla77q77qy5jk2syybxlk")))

(define-public crate-rgba_simple-0.6 (crate (name "rgba_simple") (vers "0.6.1") (deps (list (crate-dep (name "gdk") (req "~0.4") (optional #t) (default-features #t) (kind 0) (package "gdk4")) (crate-dep (name "serde") (req "~1.0") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "1y52i5hx1h636ambjpgwcqmgbvp479ajgp9bw4jj3y1xbg21mbx7")))

(define-public crate-rgba_simple-0.7 (crate (name "rgba_simple") (vers "0.7.0") (deps (list (crate-dep (name "gdk") (req "~0.4") (optional #t) (default-features #t) (kind 0) (package "gdk4")) (crate-dep (name "serde") (req "~1.0") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "1j3gyl8mcd1j6vaxg8k9gw1n989fqv7q1lb5b08lb16p0am6z41q")))

(define-public crate-rgba_simple-0.8 (crate (name "rgba_simple") (vers "0.8.0") (deps (list (crate-dep (name "gdk") (req "^0.5") (optional #t) (default-features #t) (kind 0) (package "gdk4")) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)))) (hash "150x42j5silssd2xkfnll5sviiyvhmyjl9km8zjfidiappagrb8l")))

(define-public crate-rgba_simple-0.9 (crate (name "rgba_simple") (vers "0.9.0") (deps (list (crate-dep (name "gdk") (req "^0.6") (optional #t) (default-features #t) (kind 0) (package "gdk4")) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)))) (hash "18slk64bhcv1y0dnvadfgswpa5lp6kmh7dja4i0klh1n7qqq73c1")))

(define-public crate-rgba_simple-0.10 (crate (name "rgba_simple") (vers "0.10.0") (deps (list (crate-dep (name "gdk") (req "^0.7") (optional #t) (default-features #t) (kind 0) (package "gdk4")) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)))) (hash "12n55b1kh72p5xl6gai10ydppywjk3rhv439gw48a5vh4danbkg6")))

