(define-module (crates-io rg ra) #:use-module (crates-io))

(define-public crate-rgraph-0.1 (crate (name "rgraph") (vers "0.1.0") (deps (list (crate-dep (name "dot") (req "^0.1.2") (default-features #t) (kind 0)))) (hash "0gcdfxsgh4rcdrn7cfxfwa9zl6l2f6hi195bw09d7vrmr3vzxrqr")))

(define-public crate-rgraph-0.1 (crate (name "rgraph") (vers "0.1.1") (deps (list (crate-dep (name "dot") (req "^0.1.2") (default-features #t) (kind 0)))) (hash "0b7inl9yhlbs0g7jmpg65mgl43yy58bhwh2pgh6rgfs8np25cz56")))

(define-public crate-rgraph-0.2 (crate (name "rgraph") (vers "0.2.1") (deps (list (crate-dep (name "dot") (req "^0.1.2") (default-features #t) (kind 0)))) (hash "16ifra182zcymrrjk89bf8inhnakb05lfwsxqx3phazhsx8gnypf")))

(define-public crate-rgraph-0.2 (crate (name "rgraph") (vers "0.2.2") (deps (list (crate-dep (name "dot") (req "^0.1.4") (default-features #t) (kind 0)))) (hash "0pg0579sr51jhlmyypyh56629vyp6pxh36c31qj6nlcffx53pnm0")))

