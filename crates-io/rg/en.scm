(define-module (crates-io rg en) #:use-module (crates-io))

(define-public crate-rgen-0.5 (crate (name "rgen") (vers "0.5.0") (deps (list (crate-dep (name "array_tool") (req "^1.0.3") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.5") (default-features #t) (kind 0)) (crate-dep (name "signifix") (req "^0.9") (default-features #t) (kind 0)))) (hash "0brwhf1z1n6cilwy2k9p5slvvczll89a9lzch4jffjc6afi898i8")))

(define-public crate-rgen3-save-0.1 (crate (name "rgen3-save") (vers "0.1.0") (deps (list (crate-dep (name "byteorder") (req "^0.5") (default-features #t) (kind 0)) (crate-dep (name "env_logger") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "log") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "rgen3-string") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "16rbmyfx25vmxlf8dfqarj1ls9lih83j6yg596j4filpzz6jhk3c")))

(define-public crate-rgen3-string-0.1 (crate (name "rgen3-string") (vers "0.1.0") (hash "1fypg8acrypxggd4lml6vazgnrxpxp7v43dvhx5sn79h3rb8n5j5")))

(define-public crate-rgend-1 (crate (name "rgend") (vers "1.3.1") (deps (list (crate-dep (name "clap") (req "^4.4.2") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)))) (hash "0a96m0x0brh1na5yjv9f4jmiynz3jkpd9qr9xy8ci2h34yhcvd0r")))

(define-public crate-rgenpass-0.1 (crate (name "rgenpass") (vers "0.1.0") (deps (list (crate-dep (name "crossterm") (req "^0.19.0") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.3") (default-features #t) (kind 0)))) (hash "1am9pnk49n27xzy08kvcagxqrnailal90fsl2aspw0gxibwagqbf")))

(define-public crate-rgenpass-0.1 (crate (name "rgenpass") (vers "0.1.1") (deps (list (crate-dep (name "crossterm") (req "^0.19.0") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.3") (default-features #t) (kind 0)))) (hash "1vsp8szpnrp4fgcxznrkrhcysxpansm621jqqfdy45hgi4b58q7v")))

(define-public crate-rgenpass-0.1 (crate (name "rgenpass") (vers "0.1.2") (deps (list (crate-dep (name "crossterm") (req "^0.19.0") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.3") (default-features #t) (kind 0)))) (hash "108wqgk5rr6djznr2bzqpd2ki32nqjp9xgcjsv8x6c0alnjyg7gp")))

(define-public crate-rgenpass-0.1 (crate (name "rgenpass") (vers "0.1.3") (deps (list (crate-dep (name "crossterm") (req "^0.19.0") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.3") (default-features #t) (kind 0)))) (hash "0v5dhj8m1ivchk85cfppf031h7k3bk4c2079020gggvbi28r9izx")))

(define-public crate-rgenpass-0.1 (crate (name "rgenpass") (vers "0.1.4") (deps (list (crate-dep (name "crossterm") (req "^0.19.0") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.3") (default-features #t) (kind 0)))) (hash "1bw9x1kz4jvshl8c0z479hgcxc0g1svfw6vrg4pwwgmvncr6jyyb")))

(define-public crate-rgenpass-0.1 (crate (name "rgenpass") (vers "0.1.5") (deps (list (crate-dep (name "crossterm") (req "^0.19.0") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.3") (default-features #t) (kind 0)))) (hash "04cq6bv25m725hvfgzvpga2m3bvx3ws02iq05jbkm0xbx0fvgfxj")))

(define-public crate-rgenpass-0.1 (crate (name "rgenpass") (vers "0.1.6") (deps (list (crate-dep (name "crossterm") (req "^0.19.0") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.3") (default-features #t) (kind 0)))) (hash "1rgp9ijk4nkda9qpvcg7xf2szzg73nk5p8j5id51b1jl2lbg4viy")))

(define-public crate-rgenpass-0.1 (crate (name "rgenpass") (vers "0.1.7") (deps (list (crate-dep (name "crossterm") (req "^0.19.0") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.3") (default-features #t) (kind 0)))) (hash "1kcn89fli3cak49pyaq8gv1xn3cviivcrwmvnmv5530dh26mkzj4")))

(define-public crate-rgenpass-0.1 (crate (name "rgenpass") (vers "0.1.8") (deps (list (crate-dep (name "crossterm") (req "^0.19.0") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.3") (default-features #t) (kind 0)))) (hash "0acn7gipbh33kvm09rnxg2m5pabc27psllwk9iz9rgbhl7k06zl9")))

(define-public crate-rgenpass-0.1 (crate (name "rgenpass") (vers "0.1.9") (deps (list (crate-dep (name "crossterm") (req "^0.19.0") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.3") (default-features #t) (kind 0)))) (hash "1ir018xjd18ndmlyl08s2w0ai077m91c28iqgws1fxqj7lglsnjg")))

(define-public crate-rgenpass-0.1 (crate (name "rgenpass") (vers "0.1.10") (deps (list (crate-dep (name "crossterm") (req "^0.19.0") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.3") (default-features #t) (kind 0)))) (hash "0zxbpr5cmqk8x36f8dlkl3vdhgf2g13q411vmjynaks3ch46nq0k")))

(define-public crate-rgenpass-0.1 (crate (name "rgenpass") (vers "0.1.11") (deps (list (crate-dep (name "crossterm") (req "^0.19.0") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.3") (default-features #t) (kind 0)))) (hash "15j6ki8yq1n5a23pscg2afpdbbrychcp5hylx8qk76vacmgkjdz9")))

(define-public crate-rgenpass-0.1 (crate (name "rgenpass") (vers "0.1.12") (deps (list (crate-dep (name "crossterm") (req "^0.19.0") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.3") (default-features #t) (kind 0)))) (hash "1qdyx7w61fgcv94xymllrr21cfwmfx8c74r01walkfa8xc8cppml")))

