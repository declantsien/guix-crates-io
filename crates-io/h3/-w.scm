(define-module (crates-io h3 -w) #:use-module (crates-io))

(define-public crate-h3-webtransport-0.1 (crate (name "h3-webtransport") (vers "0.1.0") (deps (list (crate-dep (name "bytes") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "futures-util") (req "^0.3") (kind 0)) (crate-dep (name "h3") (req "^0.0.3") (features (quote ("i-implement-a-third-party-backend-and-opt-into-breaking-changes"))) (default-features #t) (kind 0)) (crate-dep (name "http") (req "^0.2.9") (default-features #t) (kind 0)) (crate-dep (name "pin-project-lite") (req "^0.2") (kind 0)) (crate-dep (name "tokio") (req "^1.28") (kind 0)) (crate-dep (name "tracing") (req "^0.1.37") (default-features #t) (kind 0)))) (hash "1g7m7llnykjxkz878y3vqhglb1fkrzfd2s8ic07xpr6k81k99y1h")))

