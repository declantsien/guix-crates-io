(define-module (crates-io le t_) #:use-module (crates-io))

(define-public crate-let_or_return-0.1 (crate (name "let_or_return") (vers "0.1.0") (deps (list (crate-dep (name "macrotest") (req "^1.0.9") (default-features #t) (kind 2)))) (hash "1wg39vyq8zbdavyklc0w5qvpjpzq3qkj3k1dvi9qg9blgrrah4c4")))

(define-public crate-let_or_return-1 (crate (name "let_or_return") (vers "1.0.0") (deps (list (crate-dep (name "macrotest") (req "^1.0.9") (default-features #t) (kind 2)))) (hash "1nwnwwc7w81hqr2kpch2q9bbxq95r862vacn8l74d5n3r6xgvmi1")))

(define-public crate-let_or_return-1 (crate (name "let_or_return") (vers "1.0.1") (deps (list (crate-dep (name "macrotest") (req "^1.0.9") (default-features #t) (kind 2)))) (hash "119yqg9brzmlkj8pdxdb0ygvqq3mwgqblrfkn7cy308s7amy2v1n")))

(define-public crate-let_or_return-1 (crate (name "let_or_return") (vers "1.0.2") (deps (list (crate-dep (name "macrotest") (req "^1.0.9") (default-features #t) (kind 2)))) (hash "1wl8i3fd0m0r7jlc9mkgq9wixavvck3bawpij440rxswrriq5ax8")))

