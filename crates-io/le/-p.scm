(define-module (crates-io le -p) #:use-module (crates-io))

(define-public crate-le-petit-lapin-0.1 (crate (name "le-petit-lapin") (vers "0.1.0") (deps (list (crate-dep (name "x11") (req "^2.21.0") (default-features #t) (kind 0)) (crate-dep (name "xcb") (req "^1.2.0") (features (quote ("xkb" "xinerama" "debug_atom_names"))) (default-features #t) (kind 0)))) (hash "0c5i0rkkrrpwgpynf7w8klxjgknvfj0xkldv9kl4fiww1ialgwdm") (links "x11")))

(define-public crate-le-petit-lapin-0.1 (crate (name "le-petit-lapin") (vers "0.1.1") (deps (list (crate-dep (name "x11") (req "^2.21.0") (default-features #t) (kind 0)) (crate-dep (name "xcb") (req "^1.2.0") (features (quote ("xkb" "xinerama" "debug_atom_names"))) (default-features #t) (kind 0)))) (hash "052alsp3c6a0gfbcc9118jz4w5kn0hdrwnf9q9k0lihh0jc5f6kb") (links "x11")))

(define-public crate-le-petit-lapin-0.1 (crate (name "le-petit-lapin") (vers "0.1.2") (deps (list (crate-dep (name "x11") (req "^2.21.0") (default-features #t) (kind 0)) (crate-dep (name "xcb") (req "^1.2.0") (features (quote ("xkb" "xinerama" "debug_atom_names"))) (default-features #t) (kind 0)))) (hash "1nykr2ixb6gkvlvvrc96c2jyy6ij2vb82c4b8ralcs6z83wwncx7") (links "x11")))

