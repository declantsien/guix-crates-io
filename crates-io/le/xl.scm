(define-module (crates-io le xl) #:use-module (crates-io))

(define-public crate-lexlib-0.4 (crate (name "lexlib") (vers "0.4.0") (hash "140ivwv7179g74318w9gdf9ksxqjrry3rfyamn29cj6x7ibggbww")))

(define-public crate-lexlib-0.5 (crate (name "lexlib") (vers "0.5.0") (deps (list (crate-dep (name "egui") (req "^0.20.1") (optional #t) (default-features #t) (kind 0)))) (hash "0rss0bxig3fnap8pb5b3npd4y7bxwx9x172rh5fzsy6k4gqlllmm") (v 2) (features2 (quote (("egui" "dep:egui"))))))

(define-public crate-lexlib-0.5 (crate (name "lexlib") (vers "0.5.1") (deps (list (crate-dep (name "egui") (req "^0.20.1") (optional #t) (default-features #t) (kind 0)))) (hash "0g317azd9y5k7a00pd8p1zdcxmq6zvdc5zrxl0dm46vdrajazjp1") (v 2) (features2 (quote (("egui" "dep:egui"))))))

(define-public crate-lexlib-0.6 (crate (name "lexlib") (vers "0.6.0-dev") (deps (list (crate-dep (name "egui") (req "^0.20.1") (optional #t) (default-features #t) (kind 0)))) (hash "1xbd1ssyjyisxyzx2pn4kprrw9jbksg6w9ljv9g44iqyrvhy7mah") (yanked #t) (v 2) (features2 (quote (("egui" "dep:egui"))))))

(define-public crate-lexlib-0.7 (crate (name "lexlib") (vers "0.7.0") (deps (list (crate-dep (name "egui") (req "^0.20.1") (optional #t) (default-features #t) (kind 0)))) (hash "023g39ixxngr92xlkxajinj0m8010k0qan9jrm4r1i9k97hxq5li") (v 2) (features2 (quote (("egui" "dep:egui"))))))

(define-public crate-lexlib-0.7 (crate (name "lexlib") (vers "0.7.1") (deps (list (crate-dep (name "egui") (req "^0.20.1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "sdl2") (req "^0.35.2") (optional #t) (default-features #t) (kind 0)))) (hash "1vm2fkzj7fh2whmgxq78n1qf1slr1hb0w2f74dj2fkrld8z91lnd") (v 2) (features2 (quote (("sdl2" "dep:sdl2") ("egui" "dep:egui"))))))

(define-public crate-lexlib-0.7 (crate (name "lexlib") (vers "0.7.2") (deps (list (crate-dep (name "egui") (req "^0.20.1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "sdl2") (req "^0.35.2") (optional #t) (default-features #t) (kind 0)))) (hash "0gcvw8vss8kd06w3ymm8pp1si1vmlbdn2r5by0baq1yxnpzyrjv6") (features (quote (("reprC")))) (v 2) (features2 (quote (("sdl2" "dep:sdl2") ("egui" "dep:egui"))))))

(define-public crate-lexlib-0.7 (crate (name "lexlib") (vers "0.7.3") (deps (list (crate-dep (name "egui") (req "^0.20.1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "sdl2") (req "^0.35.2") (optional #t) (default-features #t) (kind 0)))) (hash "0v511bk55383kw87q6jar8czmyzj1s1d01n92n2x1fk7rvsm0cyw") (features (quote (("reprC")))) (v 2) (features2 (quote (("sdl2" "dep:sdl2") ("egui" "dep:egui"))))))

(define-public crate-lexlib-0.7 (crate (name "lexlib") (vers "0.7.4") (deps (list (crate-dep (name "egui") (req "^0.21") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "sdl2") (req "^0.35.2") (optional #t) (default-features #t) (kind 0)))) (hash "0f82qncnpzs04hi2hdxb6sbl7iiszdbkvxs2ck6liw8l9gs7sfcb") (features (quote (("reprC")))) (v 2) (features2 (quote (("sdl2" "dep:sdl2") ("egui" "dep:egui"))))))

(define-public crate-lexlib-1 (crate (name "lexlib") (vers "1.6.0-dev") (deps (list (crate-dep (name "cc") (req "^1.0") (features (quote ("parallel"))) (default-features #t) (kind 1)))) (hash "197w4gv2zliw5bnp17s4hrlvzmb1ghjxwrdwcsakpfs8h66229zv")))

(define-public crate-lexlib-2 (crate (name "lexlib") (vers "2.0.0") (deps (list (crate-dep (name "cc") (req "^1.0") (features (quote ("parallel"))) (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "14q5aph4ywk0k03di4mppg8ffrhlr6di5babzcww9wpc248xxn2f") (features (quote (("libpng") ("experimental"))))))

(define-public crate-lexlib-2 (crate (name "lexlib") (vers "2.0.1") (deps (list (crate-dep (name "cc") (req "^1.0") (features (quote ("parallel"))) (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "1fdb0430qsr979jlz17mrwx206mhkqlx9nr769akqymw18cws50g") (features (quote (("libpng") ("experimental"))))))

