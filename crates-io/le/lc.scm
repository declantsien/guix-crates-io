(define-module (crates-io le lc) #:use-module (crates-io))

(define-public crate-lelcat-0.1 (crate (name "lelcat") (vers "0.1.0") (hash "1lwjavlwz3qwwfgmc8a2a2hlynjwx90im2b8m66fb10hw2zgsd0z")))

(define-public crate-lelcat-0.2 (crate (name "lelcat") (vers "0.2.0") (hash "16g4sad0lamrl4d6gfj5h68sn930fb864s59r1xzf3ms2811r3md")))

