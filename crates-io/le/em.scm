(define-module (crates-io le em) #:use-module (crates-io))

(define-public crate-leemaze-0.1 (crate (name "leemaze") (vers "0.1.0") (hash "0098wqpvxwijg0qjqwvgq7wmm2s4ny2cfmvhzfd9wpngdb4d4llk")))

(define-public crate-leemaze-0.1 (crate (name "leemaze") (vers "0.1.1") (hash "03h48sg8qjkhgxilhih2pbk02076mh15sfhgszlb6ni8qlmvs6ml")))

(define-public crate-leemaze-0.1 (crate (name "leemaze") (vers "0.1.11") (hash "0098xkmnhzgd3mwhs25bx1fk1fws4l1jc6mz0avbhf1283i3indn")))

