(define-module (crates-io le mo) #:use-module (crates-io))

(define-public crate-LemoGUI-0.0.1 (crate (name "LemoGUI") (vers "0.0.1-nightly") (deps (list (crate-dep (name "ab_glyph") (req "^0.2.11") (default-features #t) (kind 0)) (crate-dep (name "anyhow") (req "^1.0") (default-features #t) (kind 1)) (crate-dep (name "async-trait") (req "^0.1.51") (default-features #t) (kind 0)) (crate-dep (name "bytemuck") (req "^1.4") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "cgmath") (req "^0.17") (default-features #t) (kind 0)) (crate-dep (name "font-kit") (req "^0.10.0") (default-features #t) (kind 0)) (crate-dep (name "fs_extra") (req "^1.1") (default-features #t) (kind 1)) (crate-dep (name "futures") (req "^0.3.10") (default-features #t) (kind 0)) (crate-dep (name "glob") (req "^0.3") (default-features #t) (kind 1)) (crate-dep (name "image") (req "^0.23.12") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.14") (default-features #t) (kind 0)) (crate-dep (name "pathfinder_geometry") (req "^0.5") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.2") (default-features #t) (kind 0)) (crate-dep (name "shaderc") (req "^0.7.0") (default-features #t) (kind 1)) (crate-dep (name "simple_logger") (req "^1.11.0") (default-features #t) (kind 0)) (crate-dep (name "wgpu") (req "^0.9.0") (default-features #t) (kind 0)) (crate-dep (name "winit") (req "^0.24.0") (default-features #t) (kind 0)))) (hash "13k4gd9ypa2m377q9ml2abv3mdryjbd6krdkvbdwvvddl3vzrck5") (yanked #t)))

(define-public crate-lemon-0.0.1 (crate (name "lemon") (vers "0.0.1") (hash "1g1wx5r230rhba9pjpc2hc8dvdjqgnn2g717f3nv4cvs3nfb78bg")))

(define-public crate-lemon-agent-0.0.0 (crate (name "lemon-agent") (vers "0.0.0") (hash "08kqw4h7gyaibpcr8svnhx47dn1qjnfsf5iw7qjhh0pfg6kwgjcs")))

(define-public crate-lemon-agent-0.0.1 (crate (name "lemon-agent") (vers "0.0.1") (hash "1qrnqp4vwi2srgm8xpk4cdbq5sjs7g99cs2w92jghlh65kmm90ik")))

(define-public crate-lemon-graph-0.0.0 (crate (name "lemon-graph") (vers "0.0.0") (hash "0jnfgnaly2y4l334pzlvixhkq1m4q2h3cx4r9vwwpikn7d3r01zh")))

(define-public crate-lemon-graph-0.0.1 (crate (name "lemon-graph") (vers "0.0.1") (deps (list (crate-dep (name "petgraph") (req "^0.6.4") (kind 0)) (crate-dep (name "thiserror") (req "^1.0.57") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.36.0") (features (quote ("full"))) (default-features #t) (kind 0)) (crate-dep (name "tracing") (req "^0.1.40") (default-features #t) (kind 0)) (crate-dep (name "tracing-test") (req "^0.2.4") (default-features #t) (kind 2)))) (hash "0wggmj9pdwic27gy0vvy9c5ky93l7y5rs599r9h8anxm2wpj8ymm")))

(define-public crate-lemon-llm-0.0.0 (crate (name "lemon-llm") (vers "0.0.0") (hash "14zmkai8r64wxa4ybxvmh1sbm021kmq2n2kqr8x8r5kgqwnpvna8")))

(define-public crate-lemon-llm-0.0.1 (crate (name "lemon-llm") (vers "0.0.1") (deps (list (crate-dep (name "lemon-graph") (req "^0.0.1") (default-features #t) (kind 0)) (crate-dep (name "petgraph") (req "^0.6.4") (kind 0)) (crate-dep (name "replicate-rust") (req "^0.0.5") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.11.24") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.114") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.57") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.36.0") (features (quote ("full"))) (default-features #t) (kind 2)) (crate-dep (name "tracing") (req "^0.1.40") (default-features #t) (kind 0)) (crate-dep (name "tracing-subscriber") (req "^0.3.18") (default-features #t) (kind 2)))) (hash "1yqzqxvsd9wdnwdqkhskvnm1c64hdlxf3psxvl33cks5vy7icpcd") (features (quote (("default" "ollama" "replicate")))) (v 2) (features2 (quote (("replicate" "dep:replicate-rust") ("ollama" "dep:reqwest" "dep:serde_json"))))))

(define-public crate-lemon-memory-0.0.0 (crate (name "lemon-memory") (vers "0.0.0") (hash "1d6hjkbxkr1c79m8aqnx84b2jwviyykg7lr074c58gm0ij8z89j2")))

(define-public crate-lemon-memory-0.0.1 (crate (name "lemon-memory") (vers "0.0.1") (hash "1z0pk9bpxlv9p8cwc3a1w1ms1pfhgqzxsn1msfimvccrs454x99d")))

(define-public crate-lemon-mint-0.1 (crate (name "lemon-mint") (vers "0.1.0") (hash "0fld1v6wski1nznh5zn0xjcks4z7i796iwknc106qzhq0xagdzxq") (features (quote (("with-debug"))))))

(define-public crate-lemon-mint-0.1 (crate (name "lemon-mint") (vers "0.1.1") (hash "1abdcdai2jz14z8w9h4lmql6wxdh76x9gb04956lj56rl5vwbmas") (features (quote (("with-debug"))))))

(define-public crate-lemon-mint-0.1 (crate (name "lemon-mint") (vers "0.1.2") (hash "118ql1abp0m1vab65sivc38s4f8wqwzaz0gp0kixiiax2sm9lfhi") (features (quote (("with-debug"))))))

(define-public crate-lemon-mint-0.1 (crate (name "lemon-mint") (vers "0.1.3") (hash "0k9pvggdpw31vjyliz8h533kpvcmyj9dn2nd571k8vllvcln3k0b") (features (quote (("with-debug"))))))

(define-public crate-lemon-mint-0.1 (crate (name "lemon-mint") (vers "0.1.4") (hash "015n06faj69v99pgzm6r8s6flvdin21np1kkvwi1y0vhfgxd59gs") (features (quote (("with-debug"))))))

(define-public crate-lemon-tree-0.1 (crate (name "lemon-tree") (vers "0.1.0") (deps (list (crate-dep (name "lemon-mint") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "lemon-tree-derive") (req "^0.1") (default-features #t) (kind 0)))) (hash "0xf31a9w185px93icsw89n5b27mwkh4ksv7d04sqg0w6q09h07l0") (features (quote (("dump-lemon-grammar" "lemon-tree-derive/dump-lemon-grammar") ("dump-grammar" "lemon-tree-derive/dump-grammar"))))))

(define-public crate-lemon-tree-0.1 (crate (name "lemon-tree") (vers "0.1.1") (deps (list (crate-dep (name "lemon-mint") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "lemon-tree-derive") (req "^0.1") (default-features #t) (kind 0)))) (hash "123jyinw270fn23wnpzx7h5zgjcyyv4p8p14sd1w8vab7vqc8blj") (features (quote (("dump-lemon-grammar" "lemon-tree-derive/dump-lemon-grammar") ("dump-grammar" "lemon-tree-derive/dump-grammar"))))))

(define-public crate-lemon-tree-0.1 (crate (name "lemon-tree") (vers "0.1.2") (deps (list (crate-dep (name "lemon-mint") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "lemon-tree-derive") (req "^0.1") (default-features #t) (kind 0)))) (hash "14v7sxd0rw7xikn1ljfsnd2d08v5h19w0m78ry40cmrhgj4nvili") (features (quote (("dump-lemon-grammar" "lemon-tree-derive/dump-lemon-grammar") ("dump-grammar" "lemon-tree-derive/dump-grammar"))))))

(define-public crate-lemon-tree-0.1 (crate (name "lemon-tree") (vers "0.1.3") (deps (list (crate-dep (name "lemon-mint") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "lemon-tree-derive") (req "^0.1") (default-features #t) (kind 0)))) (hash "07wjj0qs16g30j777r4zyp6msjwzzjwql73v2cy4qpwg50k5c7z5") (features (quote (("dump-lemon-grammar" "lemon-tree-derive/dump-lemon-grammar") ("dump-grammar" "lemon-tree-derive/dump-grammar"))))))

(define-public crate-lemon-tree-0.1 (crate (name "lemon-tree") (vers "0.1.4") (deps (list (crate-dep (name "lemon-tree-derive") (req "^0.1") (default-features #t) (kind 0)))) (hash "0blb7w5x0323d14aysdldzmlys088xqy13ji9v0x14x020li23qg") (features (quote (("dump-lemon-grammar" "lemon-tree-derive/dump-lemon-grammar") ("dump-grammar" "lemon-tree-derive/dump-grammar"))))))

(define-public crate-lemon-tree-0.1 (crate (name "lemon-tree") (vers "0.1.5") (deps (list (crate-dep (name "lemon-tree-derive") (req "^1.0") (default-features #t) (kind 0)))) (hash "01cm5mmzydjs9fsxhqcrn46sqcdfwidw6frrsls1y1vrzmnj6lpx") (features (quote (("dump-lemon-grammar" "lemon-tree-derive/dump-lemon-grammar") ("dump-grammar" "lemon-tree-derive/dump-grammar"))))))

(define-public crate-lemon-tree-0.1 (crate (name "lemon-tree") (vers "0.1.6") (deps (list (crate-dep (name "lemon-tree-derive") (req "^1.0") (default-features #t) (kind 0)))) (hash "0y6lg8cyvc89js79k8gq4jyhgza5fydsqkzvkzhsjrfjqmygrbin") (features (quote (("dump-lemon-grammar" "lemon-tree-derive/dump-lemon-grammar") ("dump-grammar" "lemon-tree-derive/dump-grammar"))))))

(define-public crate-lemon-tree-0.1 (crate (name "lemon-tree") (vers "0.1.7") (deps (list (crate-dep (name "lemon-tree-derive") (req "^1.0") (default-features #t) (kind 0)))) (hash "0qkrqgi3wyk2c2ipn10fi1g9524s018jsas26pd7cdabhqi2myn2") (features (quote (("dump-lemon-grammar" "lemon-tree-derive/dump-lemon-grammar") ("dump-grammar" "lemon-tree-derive/dump-grammar") ("debug-parser-to-file" "lemon-tree-derive/debug-parser-to-file"))))))

(define-public crate-lemon-tree-derive-0.1 (crate (name "lemon-tree-derive") (vers "0.1.0") (deps (list (crate-dep (name "lemon-mint") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "once_cell") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "1g6zh3rbmkly6m8zkycrzhq9yyg7k76l7l7njnd47q1h1c3375sy") (features (quote (("dump-lemon-grammar") ("dump-grammar"))))))

(define-public crate-lemon-tree-derive-0.1 (crate (name "lemon-tree-derive") (vers "0.1.1") (deps (list (crate-dep (name "lemon-mint") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "once_cell") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "0kqk18ki02pp0ixvv41r3pvws4pq8irkl17ky3hgwvy3m0smipf2") (features (quote (("dump-lemon-grammar") ("dump-grammar"))))))

(define-public crate-lemon-tree-derive-1 (crate (name "lemon-tree-derive") (vers "1.0.0") (deps (list (crate-dep (name "lemon-mint") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "once_cell") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "0l4cgqg62pm3d2fs7kn11bz2cdrz7562m861rs04lw30dfm7lspv") (features (quote (("dump-lemon-grammar") ("dump-grammar"))))))

(define-public crate-lemon-tree-derive-1 (crate (name "lemon-tree-derive") (vers "1.0.1") (deps (list (crate-dep (name "lemon-mint") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "once_cell") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "11259lrhpzcy9dpj52idkyaw66bfk10hi6s6zs2dk43adggl9vxh") (features (quote (("dump-lemon-grammar") ("dump-grammar"))))))

(define-public crate-lemon-tree-derive-1 (crate (name "lemon-tree-derive") (vers "1.0.2") (deps (list (crate-dep (name "lemon-mint") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "once_cell") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "0v13v2hkhfkfaz4d94pyqqkhkh7zpjz0k8aln58f0g3x50iz9z71") (features (quote (("dump-lemon-grammar") ("dump-grammar"))))))

(define-public crate-lemon-tree-derive-1 (crate (name "lemon-tree-derive") (vers "1.0.3") (deps (list (crate-dep (name "lemon-mint") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "once_cell") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "1y6njd4nav0nqr8z34pvc7lvjbwyxyzi2mc00l9600xda3qqx412") (features (quote (("dump-lemon-grammar") ("dump-grammar") ("debug-parser-to-file"))))))

(define-public crate-lemon-tree-derive-1 (crate (name "lemon-tree-derive") (vers "1.0.4") (deps (list (crate-dep (name "lemon-mint") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "once_cell") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "0g7my5icrvipdsik46sca2h654r6b7klasgs0zrzdfcmi1jcl79f") (features (quote (("dump-lemon-grammar") ("dump-grammar") ("debug-parser-to-file"))))))

(define-public crate-lemon_engine-0.1 (crate (name "lemon_engine") (vers "0.1.0") (deps (list (crate-dep (name "glium") (req "^0.30.1") (default-features #t) (kind 0)))) (hash "1dlj60cck2xc8vndxfcc7n1ls56gnbqfmzdy8f0hlpy9dnw4w3w5")))

(define-public crate-lemon_engine-0.1 (crate (name "lemon_engine") (vers "0.1.1") (deps (list (crate-dep (name "glium") (req "^0.30.1") (default-features #t) (kind 0)))) (hash "07ppcgkp3syrc7s7z08jkjfdq5xbffmg1pnb89b597qk8ap9hh1q")))

(define-public crate-lemon_engine-0.1 (crate (name "lemon_engine") (vers "0.1.2") (deps (list (crate-dep (name "glium") (req "^0.30.1") (default-features #t) (kind 0)))) (hash "0i6n0205nygbsxrc7qs29jgg17prz4xhhmx8dciivsn982hqsz10")))

(define-public crate-lemon_engine-0.1 (crate (name "lemon_engine") (vers "0.1.3") (deps (list (crate-dep (name "glium") (req "^0.30.1") (default-features #t) (kind 0)))) (hash "08789sj1ni3h7mdsmw0cg682dz8wd494x698gl22bcgvvgzq6g7k")))

(define-public crate-lemon_engine-0.1 (crate (name "lemon_engine") (vers "0.1.4") (deps (list (crate-dep (name "glium") (req "^0.30.1") (default-features #t) (kind 0)))) (hash "0wjgvmcqv7qrxqmc9cmww4sicx3y41z20zk43f3kpfrrhqxayl10")))

(define-public crate-lemon_engine-0.1 (crate (name "lemon_engine") (vers "0.1.5") (deps (list (crate-dep (name "glium") (req "^0.30.1") (default-features #t) (kind 0)))) (hash "0jixkqngg5y715fwb2kddlqw3xl7q0zcgm1f10y569ddgf0y0rz5")))

(define-public crate-lemonade-0.0.0 (crate (name "lemonade") (vers "0.0.0") (hash "0vfiblr9hhd38i1lxp4c7nv4i9jil5bsamgk4x768yd1016r8fia")))

(define-public crate-lemonade-0.0.1 (crate (name "lemonade") (vers "0.0.1") (hash "08vy4zcjbq5znwng0sr03klfpzxylbyiyaf6vzrfrr0qc1vil6l3")))

(define-public crate-lemonaide-0.0.0 (crate (name "lemonaide") (vers "0.0.0") (hash "1zikc07s4xhy094fm4c5q8g5qs7w5zysv8q9y85gjrydvgh5x07v")))

(define-public crate-lemonmath-0.1 (crate (name "lemonmath") (vers "0.1.0") (hash "0sr7chdxzdk38x03f58drd1d1xyzrbc8cv2sj16w01r8y0z6yx9h")))

(define-public crate-lemonmath-0.1 (crate (name "lemonmath") (vers "0.1.1") (hash "1jh15iw4s6bal3rpbb4ms6sv1k8sbvyghfdsgal6b3zx9scl067l")))

(define-public crate-lemonmath-0.1 (crate (name "lemonmath") (vers "0.1.2") (hash "1065nrvs1c0n5hrd4pchpk4gsbg443j67xkx84k5jmmzyv0hgx6r")))

(define-public crate-lemonmath-0.1 (crate (name "lemonmath") (vers "0.1.3") (hash "0d02y0sfwcdbg311zkf8zh6xwbhs6b9bagpxd0halp9gxamr5d9q")))

(define-public crate-lemonmath-0.1 (crate (name "lemonmath") (vers "0.1.4") (hash "0isfnjp1043xlr76ajz9wysrbxwplqf4cphcsbwzz0bl67z3x607")))

(define-public crate-lemonsqueezy-0.1 (crate (name "lemonsqueezy") (vers "0.1.0") (deps (list (crate-dep (name "anyhow") (req "^1.0.75") (default-features #t) (kind 0)) (crate-dep (name "dotenv") (req "^0.15.0") (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.11") (features (quote ("blocking" "json"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.48") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "0g9zmd2lkcy5jnms9zcgdg5rb1x8nlacxkbgprcyigfm91jfwjmk")))

(define-public crate-lemonsqueezy-0.1 (crate (name "lemonsqueezy") (vers "0.1.1") (deps (list (crate-dep (name "anyhow") (req "^1.0.75") (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.11") (features (quote ("blocking" "json"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.48") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "0w1fpwggbwj8qpr067jg1h13gjn4l95mivm2qxgp1f7kx0niaqyi")))

(define-public crate-lemonsqueezy-0.1 (crate (name "lemonsqueezy") (vers "0.1.2") (deps (list (crate-dep (name "anyhow") (req "^1.0.75") (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.11") (features (quote ("blocking" "json"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.48") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "1hw0gx2b4jbwxj9hki63idya4zdzq4vb72qsdzkzsagmjh8yyl7d")))

(define-public crate-lemonwood-0.1 (crate (name "lemonwood") (vers "0.1.0") (deps (list (crate-dep (name "function_name") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.17") (features (quote ("max_level_trace" "release_max_level_warn"))) (default-features #t) (kind 0)) (crate-dep (name "pretty_env_logger") (req "^0.4.0") (default-features #t) (kind 0)) (crate-dep (name "volmark") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "0cjrm8xcwa9w2f2l1c5sm56yn1mf1g78flr2n14n7y80sjxa9wck")))

