(define-module (crates-io le rp) #:use-module (crates-io))

(define-public crate-lerp-0.1 (crate (name "lerp") (vers "0.1.0") (deps (list (crate-dep (name "num-traits") (req "^0.1.34") (default-features #t) (kind 0)))) (hash "1fdkjbjrbxwrda7gwf6iv3xjr67ccf5iy58z8cd0nh2600k5xdvw")))

(define-public crate-lerp-0.1 (crate (name "lerp") (vers "0.1.1") (deps (list (crate-dep (name "num-traits") (req "^0.1.34") (default-features #t) (kind 0)))) (hash "042m9csmyv21l3n7gfd04lzvnzsv8559y3d32yqk16kcb8mpvpnp")))

(define-public crate-lerp-0.2 (crate (name "lerp") (vers "0.2.0") (deps (list (crate-dep (name "num-traits") (req "^0.1.34") (default-features #t) (kind 0)))) (hash "0saf8pj958v8grknzhiks893gqmdmh12v50d1h5wkq5dvxbxiiza")))

(define-public crate-lerp-0.3 (crate (name "lerp") (vers "0.3.0") (deps (list (crate-dep (name "num-traits") (req "^0.2") (default-features #t) (kind 0)))) (hash "01q1c7gngwlbgayklym15x21452afd084rac8wic7750zpjd8kky")))

(define-public crate-lerp-0.4 (crate (name "lerp") (vers "0.4.0") (deps (list (crate-dep (name "lerp_derive") (req "^0.4.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2") (default-features #t) (kind 0)))) (hash "0k4yn0kz7pnkdcpm0wilzbw1qv7fzd5h7f4f9811mzwv846wb6mi") (features (quote (("derive" "lerp_derive") ("default"))))))

(define-public crate-lerp-0.5 (crate (name "lerp") (vers "0.5.0") (deps (list (crate-dep (name "lerp_derive") (req "^0.4.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2") (default-features #t) (kind 0)))) (hash "0q32f07gngpg81b560dpx65b17kirr58ykxvnsnbrv4k8l16migc") (features (quote (("derive" "lerp_derive") ("default"))))))

(define-public crate-lerp_derive-0.4 (crate (name "lerp_derive") (vers "0.4.0") (deps (list (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "0w81rs5i85ygsrqi3wzjl3f2ngflys04s45z49i489gj1yvmgk9n")))

(define-public crate-lerp_ol-0.1 (crate (name "lerp_ol") (vers "0.1.0") (hash "1dxlw7slk6bmyjnv09xrnh9q7k8cw5h4ap8gl01gwr34k9l2iyi1")))

