(define-module (crates-io le a2) #:use-module (crates-io))

(define-public crate-lea256-0.1 (crate (name "lea256") (vers "0.1.0") (hash "1vqdk8f2xvq0zkkwcw4hcq7jnh8z8rp8zgd128ps07spxj7g4xd9") (yanked #t)))

(define-public crate-lea256-0.1 (crate (name "lea256") (vers "0.1.1") (hash "1md94gbf4naxxrnskp0cfb7mc9w6inff4xxjhh4rbr38raajf3h7") (yanked #t)))

(define-public crate-lea256-0.1 (crate (name "lea256") (vers "0.1.2") (hash "0hb56z8r7fhjq7b05rp409pzp3n2j2g66vg4cvbfrpdyzp7hs00n") (yanked #t)))

(define-public crate-lea256-0.2 (crate (name "lea256") (vers "0.2.0") (deps (list (crate-dep (name "block-cipher-trait") (req "0.6.*") (default-features #t) (kind 0)))) (hash "1ihd0rr04rkz10d331xbzk24sby4mdw2wrsvn1l4fhg36z3pcf9l") (yanked #t)))

(define-public crate-lea256-0.2 (crate (name "lea256") (vers "0.2.1") (deps (list (crate-dep (name "block-cipher-trait") (req "0.6.*") (default-features #t) (kind 0)))) (hash "155ch29xks89qrzwn6lacmib2qs3kk7f7kra4r92b7y7dpi3k6yj") (yanked #t)))

