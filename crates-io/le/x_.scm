(define-module (crates-io le x_) #:use-module (crates-io))

(define-public crate-lex_derive-0.1 (crate (name "lex_derive") (vers "0.1.0") (deps (list (crate-dep (name "quote") (req "^1.0.33") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2.0.37") (default-features #t) (kind 0)))) (hash "0s1snfgk10gh6ay9611zn4c5gnchx1nnf3jzpg1smbs829ah6yf9")))

(define-public crate-lex_lua-0.1 (crate (name "lex_lua") (vers "0.1.0") (deps (list (crate-dep (name "bstr") (req "^0.2") (default-features #t) (kind 0)))) (hash "015ayfaf83i9hdxn1p2fnykixfiqrs8krbm92snifv9q400m8fv4")))

(define-public crate-lex_lua-0.1 (crate (name "lex_lua") (vers "0.1.1") (deps (list (crate-dep (name "bstr") (req "^0.2") (default-features #t) (kind 0)))) (hash "0jw7bw7qv485k7idqxlhsiv8wnjrygn7kfxjsi6vcdj6w6xm0s93")))

(define-public crate-lex_lua-0.1 (crate (name "lex_lua") (vers "0.1.2") (deps (list (crate-dep (name "bstr") (req "^0.2") (default-features #t) (kind 0)))) (hash "1l481r0c418czdsbqbyc4jv21ywlkmds15p5gy0k9vxb0ff79mr9")))

(define-public crate-lex_lua-0.1 (crate (name "lex_lua") (vers "0.1.3") (deps (list (crate-dep (name "bstr") (req "^0.2") (default-features #t) (kind 0)))) (hash "0b10hr4fsd9px438pjmcjc29chbq0k3kmzzwxi49zs8prn5bbnwp")))

(define-public crate-lex_lua-0.1 (crate (name "lex_lua") (vers "0.1.4") (deps (list (crate-dep (name "bstr") (req "^0.2") (default-features #t) (kind 0)))) (hash "05h91iqabmld75rchdyjw42zhbwkwkhp3pv8bf2z43y798swhq8x")))

(define-public crate-lex_lua-0.1 (crate (name "lex_lua") (vers "0.1.5") (deps (list (crate-dep (name "bstr") (req "^0.2") (default-features #t) (kind 0)))) (hash "15yy7kkdmnlqbj5kr5bz63kh4hpvwv892hpnhwfn3w9ixjdjk631")))

(define-public crate-lex_lua-0.1 (crate (name "lex_lua") (vers "0.1.6") (deps (list (crate-dep (name "bstr") (req "^0.2") (default-features #t) (kind 0)))) (hash "1j2sq67q94k5rwbfq6nkyj9scrd45xn78j4qd63l1wz0l87rnrha")))

(define-public crate-lex_lua-0.1 (crate (name "lex_lua") (vers "0.1.7") (deps (list (crate-dep (name "bstr") (req "^0.2") (default-features #t) (kind 0)))) (hash "0i0r0fwj0r95nzz2bqdm9d5qj5sqy1vvdxwr7w97z6f44lmr5nfw")))

(define-public crate-lex_lua-0.1 (crate (name "lex_lua") (vers "0.1.8") (deps (list (crate-dep (name "bstr") (req "^0.2") (default-features #t) (kind 0)))) (hash "1q564cb2am4izgf4hvk6733lligp62sr5lgrsrqh2rj0l4dx2kjl")))

(define-public crate-lex_lua-0.1 (crate (name "lex_lua") (vers "0.1.9") (deps (list (crate-dep (name "bstr") (req "^0.2") (default-features #t) (kind 0)))) (hash "002xj846z90sql8q8v0nqqbirx4dpvk6zaz9inawji50r0lra63m")))

(define-public crate-lex_lua-0.2 (crate (name "lex_lua") (vers "0.2.0") (deps (list (crate-dep (name "bstr") (req "^1.9") (default-features #t) (kind 0)))) (hash "1l03zl0hcp60rzlf8k6r0xw7f2lamq5337plvh0vprw6r79pliyk")))

