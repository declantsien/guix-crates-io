(define-module (crates-io le tr) #:use-module (crates-io))

(define-public crate-letr-0.1 (crate (name "letr") (vers "0.1.0") (hash "1da7pnw3wz67wvwpni313xwl750nm5snwmrvsgkw9kfrg3l9pbwi") (yanked #t)))

(define-public crate-letr-0.1 (crate (name "letr") (vers "0.1.1") (hash "093m9qs41vvcxpr4zdsc04pd9kwlv72ivpxg3k8m39aj0cfz7wh1")))

(define-public crate-letr-0.2 (crate (name "letr") (vers "0.2.0") (hash "0gxpa68hr8kw2jazkshq8szskbafdpd8q93rmcnhci44cw6m7zq9")))

(define-public crate-letr-0.2 (crate (name "letr") (vers "0.2.1") (hash "06xd4mlr3d8wr6ilb4pjd8r6mwfagprv3f0yrmbjryylmmps0ylr")))

