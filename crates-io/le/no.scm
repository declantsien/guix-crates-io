(define-module (crates-io le no) #:use-module (crates-io))

(define-public crate-lenovo-0.0.1 (crate (name "lenovo") (vers "0.0.1") (hash "1fcc8y2y057vm1b8smfhalq5vhz2h3s65q6341dp43ab7a21yrp6")))

(define-public crate-lenovo-cm-0.1 (crate (name "lenovo-cm") (vers "0.1.0") (hash "1m4zil1vy9dqs1kn011bvh5x76afk4i5nb8g09ibl17hfdhdmr5r")))

