(define-module (crates-io le dm) #:use-module (crates-io))

(define-public crate-ledmatrix_widgets-0.1 (crate (name "ledmatrix_widgets") (vers "0.1.0") (deps (list (crate-dep (name "battery") (req "^0.7.8") (default-features #t) (kind 0)) (crate-dep (name "serialport") (req "^4.3.0") (default-features #t) (kind 0)) (crate-dep (name "sysinfo") (req "^0.30.9") (default-features #t) (kind 0)))) (hash "08balllqzwf6pmla21165c08lf2r6pfgsspx586m5pn63j1w0bs7")))

