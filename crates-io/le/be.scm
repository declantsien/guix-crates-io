(define-module (crates-io le be) #:use-module (crates-io))

(define-public crate-lebe-0.5 (crate (name "lebe") (vers "0.5.0") (deps (list (crate-dep (name "bencher") (req "^0.1.5") (default-features #t) (kind 2)) (crate-dep (name "byteorder") (req "^1.3.2") (default-features #t) (kind 2)))) (hash "1anr45bm029xyaarmlan3sn9z1bk4l2vz176sfq0swr5jnz1swax")))

(define-public crate-lebe-0.5 (crate (name "lebe") (vers "0.5.1") (deps (list (crate-dep (name "bencher") (req "^0.1.5") (default-features #t) (kind 2)) (crate-dep (name "byteorder") (req "^1.3.2") (default-features #t) (kind 2)))) (hash "1zr6g3d35h349j0dsx6722lrjws00x2d8z0sy5p9wxdhimlivzby")))

(define-public crate-lebe-0.5 (crate (name "lebe") (vers "0.5.2") (deps (list (crate-dep (name "bencher") (req "^0.1.5") (default-features #t) (kind 2)) (crate-dep (name "byteorder") (req "^1.4.3") (default-features #t) (kind 2)))) (hash "1j2l6chx19qpa5gqcw434j83gyskq3g2cnffrbl3842ymlmpq203")))

(define-public crate-leben-0.1 (crate (name "leben") (vers "0.1.0") (deps (list (crate-dep (name "leb128") (req "^0.2.1") (default-features #t) (kind 2)))) (hash "1yh1d8sqgr3wcs3scgbyx8ij65pkv8j1vnwvyjkg5minkp9xh2pv") (yanked #t)))

