(define-module (crates-io le rr) #:use-module (crates-io))

(define-public crate-lerr-0.1 (crate (name "lerr") (vers "0.1.0") (deps (list (crate-dep (name "unicode-width") (req "^0.1.10") (default-features #t) (kind 0)))) (hash "00r9a3n56858g6sf4q2vmxwlj1bi2yg0sn445w6ax2vn9a2damhg")))

(define-public crate-lerr-0.1 (crate (name "lerr") (vers "0.1.1") (deps (list (crate-dep (name "unicode-width") (req "^0.1.10") (default-features #t) (kind 0)))) (hash "17w7vcgp59y3nk9f739b537p9k11gc66p9zpydqdlj3vq05bia8j")))

(define-public crate-lerr-0.1 (crate (name "lerr") (vers "0.1.2") (deps (list (crate-dep (name "unicode-width") (req "^0.1.10") (default-features #t) (kind 0)))) (hash "0pn5nv2pz1vc2f04fxd7ndwc3813b7flkpdkhzhnm1srs3cxrcr8")))

(define-public crate-lerr-0.1 (crate (name "lerr") (vers "0.1.3") (deps (list (crate-dep (name "anstream") (req "^0.5.0") (kind 2)) (crate-dep (name "comat") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "unicode-width") (req "^0.1.10") (default-features #t) (kind 0)))) (hash "0vdladwxwvdzkv0mdjgixknjnpaig7cmcnvdcx9lq8zdl1sigd51")))

(define-public crate-lerr-0.1 (crate (name "lerr") (vers "0.1.4") (deps (list (crate-dep (name "anstream") (req "^0.5.0") (kind 0)) (crate-dep (name "comat") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "unicode-width") (req "^0.1.10") (default-features #t) (kind 0)))) (hash "1c6l8mbdn17y5crl7f37l51881asj9iqg3ghv3vj9zdvragx3w7w")))

(define-public crate-lerr-0.1 (crate (name "lerr") (vers "0.1.5") (deps (list (crate-dep (name "anstream") (req "^0.5.0") (kind 0)) (crate-dep (name "comat") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "unicode-width") (req "^0.1.10") (default-features #t) (kind 0)))) (hash "1d22l22s324vglclrmy563g9cf3x0hnmiwrl5lmy8l01f6cpb2ri")))

