(define-module (crates-io le ad) #:use-module (crates-io))

(define-public crate-lead-0.0.1 (crate (name "lead") (vers "0.0.1") (hash "089qbs5aq2p7vqp8bqhda8dazn8bsx5vzmzgmg1v5sprxfx9kfdn")))

(define-public crate-lead-oxide-0.1 (crate (name "lead-oxide") (vers "0.1.0") (deps (list (crate-dep (name "chrono") (req "^0.4") (features (quote ("serde"))) (default-features #t) (kind 0)) (crate-dep (name "iso_country") (req "^0.1.4") (features (quote ("serde"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_repr") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "serde_urlencoded") (req "^0.7") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "ureq") (req "^1.3") (kind 0)))) (hash "0w8gx4hc4bigcz7lywaih70h7qbvyj5jy23v089jmibb7nzbvx3g")))

(define-public crate-lead-oxide-0.2 (crate (name "lead-oxide") (vers "0.2.0") (deps (list (crate-dep (name "chrono") (req "^0.4") (features (quote ("serde"))) (default-features #t) (kind 0)) (crate-dep (name "iso_country") (req "^0.1.4") (features (quote ("serde"))) (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.4") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_repr") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "serde_urlencoded") (req "^0.7") (default-features #t) (kind 0)) (crate-dep (name "serial_test") (req "^0.5") (default-features #t) (kind 2)) (crate-dep (name "thiserror") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "ureq") (req "^1.3") (kind 0)))) (hash "08n79qxb48mqmciakcdsjfqhlc2nlb7ms1n6yalq8h7jfhnb6z3r")))

(define-public crate-lead_lang_core-0.0.1 (crate (name "lead_lang_core") (vers "0.0.1") (deps (list (crate-dep (name "lead_lang_interpreter") (req "^0.0.1") (default-features #t) (kind 0)))) (hash "1q4gc6rin8v8hkdhhpf3appis8vi9yg9yjavbd5cchnfyrfzmvc1") (yanked #t)))

(define-public crate-lead_lang_interpreter-0.0.1 (crate (name "lead_lang_interpreter") (vers "0.0.1") (deps (list (crate-dep (name "chalk_rs") (req "^1.0.1") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)))) (hash "17510qvrhaj4gf19ggrn8khlr4qiwqvi42ph0b90a9ksc84vynaa")))

(define-public crate-lead_lang_interpreter-0.0.2 (crate (name "lead_lang_interpreter") (vers "0.0.2") (deps (list (crate-dep (name "chalk_rs") (req "^1.0.1") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)))) (hash "0jp73bwpz6dlpgk1ny89c9dz7y32iz918igc0fbzc8krrs8069jr")))

(define-public crate-lead_lang_interpreter-0.0.3 (crate (name "lead_lang_interpreter") (vers "0.0.3") (deps (list (crate-dep (name "chalk_rs") (req "^1.0.1") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)))) (hash "02lbwd6pqy27x61hbjrwnf8r7vhldx4dm72mcknl3l89gjq9b1qa")))

(define-public crate-lead_lang_interpreter-0.0.4 (crate (name "lead_lang_interpreter") (vers "0.0.4") (deps (list (crate-dep (name "chalk_rs") (req "^1.0.1") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)))) (hash "1ayyzzhfigm4zs3p9pf7ym42ihbjdi3vmjizsfz8bbsqflwcb80s")))

(define-public crate-lead_lang_interpreter-0.0.5 (crate (name "lead_lang_interpreter") (vers "0.0.5") (deps (list (crate-dep (name "chalk_rs") (req "^1.0.1") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)))) (hash "0lxb30mfq5l36ahxy9nn89ymcfqxx2iarhiq9snqb03wxvx99xd5")))

(define-public crate-lead_lang_interpreter-0.0.6 (crate (name "lead_lang_interpreter") (vers "0.0.6") (deps (list (crate-dep (name "chalk_rs") (req "^1.0.1") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)))) (hash "10cyh6hvgs5s0pqs885dvipwn7j3jp5x1q2s9dcbhxn3k8z3q95m")))

(define-public crate-lead_lang_interpreter-0.0.7 (crate (name "lead_lang_interpreter") (vers "0.0.7") (deps (list (crate-dep (name "chalk_rs") (req "^1.0.1") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)))) (hash "13v18542rfncsj4plzidxx0s9h58fwp9qspj9vkdnzqvyywx1z9w")))

(define-public crate-lead_lang_interpreter-0.0.8 (crate (name "lead_lang_interpreter") (vers "0.0.8") (deps (list (crate-dep (name "chalk_rs") (req "^1.0.1") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)))) (hash "01fx1m73akgvcgsa2247h5qkcwgbnijcl0msx3im51sl018djava")))

(define-public crate-lead_lang_interpreter-0.0.9 (crate (name "lead_lang_interpreter") (vers "0.0.9") (deps (list (crate-dep (name "chalk_rs") (req "^1.0.1") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)))) (hash "1sq8xljkgjf228q47zljybxfnqpvcv7j5rps6yga3cg3aqcw03lj")))

(define-public crate-lead_lang_std-0.0.1 (crate (name "lead_lang_std") (vers "0.0.1") (deps (list (crate-dep (name "lead_lang_interpreter") (req "^0.0.1") (default-features #t) (kind 0)))) (hash "0482i3zljqfjm73nlsmmdppfb6chh2n7xig2vp6qr49cicwpvax6") (yanked #t)))

(define-public crate-lead_lang_std-0.0.2 (crate (name "lead_lang_std") (vers "0.0.2") (deps (list (crate-dep (name "lead_lang_interpreter") (req "^0.0.1") (default-features #t) (kind 0)))) (hash "1xfckii9sv1wmzj33k8mqd1rg9j0dp9nq7z2r2l600r9pln0pyb8") (yanked #t)))

(define-public crate-leaderboard-0.1 (crate (name "leaderboard") (vers "0.1.0") (hash "11bbhpm56gzlm9zzz21yhylcr042kq78iwfp18pqxjlln2hig951")))

(define-public crate-leaderboard-0.1 (crate (name "leaderboard") (vers "0.1.1") (hash "1k59ilr282s2amavm0f2v4pjq1ahq685kfbaflb2ii60h5q82nrw")))

(define-public crate-leaderboard-0.1 (crate (name "leaderboard") (vers "0.1.2") (hash "1rkbhgfnsyz3wpjkg7m7rym8y1xdf950xmazjxm10d86kvjfgrj4")))

(define-public crate-leadlight-0.1 (crate (name "leadlight") (vers "0.1.0") (hash "0grljajj4q9pxlfqk1ci5qhx8k0f5cgzykhmk5vcd07amqgjwrv9")))

