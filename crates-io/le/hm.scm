(define-module (crates-io le hm) #:use-module (crates-io))

(define-public crate-lehmer-1 (crate (name "lehmer") (vers "1.0.0") (deps (list (crate-dep (name "bencher") (req "^0.1.5") (default-features #t) (kind 2)))) (hash "1rjj5rjr5f5nxniwx8vyw3d9p39w2qz7gq44cfcg5x2x0iwdvxyg")))

(define-public crate-lehmer-1 (crate (name "lehmer") (vers "1.0.1") (deps (list (crate-dep (name "bencher") (req "^0.1.5") (default-features #t) (kind 2)))) (hash "12iwq5j71qflb1hjqza1vj3n91z563n8ilwybq7mfxbn5hcw2dhn")))

(define-public crate-lehmer-1 (crate (name "lehmer") (vers "1.0.2") (deps (list (crate-dep (name "bencher") (req "^0.1.5") (default-features #t) (kind 2)))) (hash "1xd7a3zmwy3xgks7511k9fgcbg12px3m24i7wb0pqc7cqiy1qcjl")))

(define-public crate-lehmer-1 (crate (name "lehmer") (vers "1.1.0") (deps (list (crate-dep (name "bencher") (req "^0.1.5") (default-features #t) (kind 2)))) (hash "0clpyfm1i4mkdrvbdsyhap0zw36vy3f7amg6zvgmrw7cg3gmb0zs")))

(define-public crate-lehmer-2 (crate (name "lehmer") (vers "2.0.0") (deps (list (crate-dep (name "bencher") (req "^0.1.5") (default-features #t) (kind 2)))) (hash "1h4qbxigzkjiya1w2h9an2f94bh2lhbd4sdbhwxwviibf4yaxkz5")))

(define-public crate-lehmer-3 (crate (name "lehmer") (vers "3.0.0") (deps (list (crate-dep (name "bencher") (req "^0.1.5") (default-features #t) (kind 2)))) (hash "12nrqf4s02qlfjzxn7n4inn2m1fr567akzalnvz4q4dybgv694f2")))

