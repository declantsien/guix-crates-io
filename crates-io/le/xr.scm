(define-module (crates-io le xr) #:use-module (crates-io))

(define-public crate-lexr-0.1 (crate (name "lexr") (vers "0.1.0") (deps (list (crate-dep (name "concat-idents") (req "^1.1.5") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.10.2") (default-features #t) (kind 0)))) (hash "0skmvhrs0y8vkf9sj1y6divp7wnmrvjwj9c3r8g3rkrgqxjgvrw5")))

