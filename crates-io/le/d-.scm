(define-module (crates-io le d-) #:use-module (crates-io))

(define-public crate-led-matrix-arduino-0.1 (crate (name "led-matrix-arduino") (vers "0.1.0") (hash "1cq1nin4h46ij5q1ac439r25sb48ks5xrabzlimcbhx3bf6pk5hi")))

(define-public crate-led-matrix-arduino-0.1 (crate (name "led-matrix-arduino") (vers "0.1.1") (hash "1win6yhj5ywnymkpb1z23ihw1k7y58pwx87c2z5r96h87wbwpdp7")))

(define-public crate-led-matrix-arduino-0.1 (crate (name "led-matrix-arduino") (vers "0.1.2") (hash "08ifiia76is866bvrvcd9i61y14l9qhvllhabjv9qdpgf15i26nk")))

(define-public crate-led-matrix-arduino-0.1 (crate (name "led-matrix-arduino") (vers "0.1.3") (hash "0a31prrl90sf9jacv2as2z81ba0v8c2f8wc0jb27bj4lmikjlwbg")))

(define-public crate-led-rs-0.1 (crate (name "led-rs") (vers "0.1.0") (deps (list (crate-dep (name "anyhow") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1") (default-features #t) (kind 0)))) (hash "1p61lcddr09nndrg7zah9aaalr754kx41qcxck6yr4nl3za44zc8")))

