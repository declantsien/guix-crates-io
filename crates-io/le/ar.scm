(define-module (crates-io le ar) #:use-module (crates-io))

(define-public crate-lear-0.1 (crate (name "lear") (vers "0.1.0") (deps (list (crate-dep (name "rand") (req "^0.8.4") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.68") (default-features #t) (kind 0)) (crate-dep (name "termion") (req "^1.5.6") (default-features #t) (kind 0)))) (hash "14abh0vchcs48aqfn7cdibi23bi9i1hngq8hymi3gfcf9nyizy7y")))

(define-public crate-lear-0.2 (crate (name "lear") (vers "0.2.0") (deps (list (crate-dep (name "clap") (req "^2.33.3") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.4") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.68") (default-features #t) (kind 0)) (crate-dep (name "termion") (req "^1.5.6") (default-features #t) (kind 0)))) (hash "1jasf3fj2881rh9yrrlxr67vznhzjqnn17q1dhp2jrzh3qaa5fjg")))

(define-public crate-learn-0.1 (crate (name "learn") (vers "0.1.1") (hash "03vzzm2jl9mqgywk4f67gxchdcgw9rxrjpv837930cc7fqmjia1j")))

(define-public crate-learn-alg-0.1 (crate (name "learn-alg") (vers "0.1.0") (hash "0jv7djqhg9ndqnxyaz1xi7r77mdglc6i8xyr3fs1091bcylk4421")))

(define-public crate-learn-alg-0.1 (crate (name "learn-alg") (vers "0.1.1") (hash "1fkx1jfkylbc4zg2x8i4balyjc5hk6a68l72a1zj05f4sda2d9c2")))

(define-public crate-learn-opengl-0.0.0 (crate (name "learn-opengl") (vers "0.0.0") (deps (list (crate-dep (name "beryllium") (req "^0.2.0-alpha.2") (default-features #t) (kind 2)) (crate-dep (name "bytemuck") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "imagine") (req "^0.0.2") (default-features #t) (kind 2)) (crate-dep (name "ogl33") (req "^0.1") (features (quote ("debug_error_checks" "debug_trace_messages"))) (default-features #t) (kind 0)) (crate-dep (name "ultraviolet") (req "^0.3") (default-features #t) (kind 2)))) (hash "155q1w5b281xk96rxslg4aag0jhwqwr1p7nrxfj4jprj6vmw3lic")))

(define-public crate-learn-rust-123-0.1 (crate (name "learn-rust-123") (vers "0.1.0") (hash "0ms2acldywgpnp331mnama9s737jrzs4fwh3ygsp6hr51ddikwz6")))

(define-public crate-learn-rust-publish-0.1 (crate (name "learn-rust-publish") (vers "0.1.0") (hash "03im3hd8hyg45ajnapjc026z730mrrj3gl08scvldr1vg50vkcbv")))

(define-public crate-learn-rust-publish-0.1 (crate (name "learn-rust-publish") (vers "0.1.1") (hash "00k487jbzyqqpbjimk919d876rkfnz47901jmd8rp7wgnf1ddq94")))

(define-public crate-learn-rust-with-bubble-sort-0.1 (crate (name "learn-rust-with-bubble-sort") (vers "0.1.0") (hash "0mracm41zm4hjw0ygm45yvmandlx77x13qiaym7w624nrcp0a884")))

(define-public crate-learn-rust-with-bubble-sort-0.1 (crate (name "learn-rust-with-bubble-sort") (vers "0.1.1") (hash "0r2lfghb0jqqx8w7n4ivblkj766jrsim49v89qvmh7pc3x7y7l00")))

(define-public crate-learn-wgpu-0.1 (crate (name "learn-wgpu") (vers "0.1.0") (deps (list (crate-dep (name "env_logger") (req "^0.9.0") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.17") (default-features #t) (kind 0)) (crate-dep (name "pollster") (req "^0.2.5") (default-features #t) (kind 0)) (crate-dep (name "wgpu") (req "^0.13.1") (default-features #t) (kind 0)) (crate-dep (name "winit") (req "^0.26") (default-features #t) (kind 0)))) (hash "00gn5k2n87d9dq556dswva50razky197jl27x7zzvamfj2qkpf8p")))

(define-public crate-learn_25_cargo-0.1 (crate (name "learn_25_cargo") (vers "0.1.0") (hash "0cazy47nv38jmj4pi7c42wxsmbswkj75hhi9zgycpx7cfw15g3nq")))

(define-public crate-learn_cargo-0.1 (crate (name "learn_cargo") (vers "0.1.0") (hash "1y2bbzzkjy1qqlq6azv984jcbdy8hkr5ahxvibllx41js2i8f90n") (yanked #t)))

(define-public crate-learn_crate_io-0.1 (crate (name "learn_crate_io") (vers "0.1.0") (hash "0pihvbm2pw07af1iaa34jlf0iy5q8r94m46xycr7crlfnszhw5bh")))

(define-public crate-learn_crates-0.1 (crate (name "learn_crates") (vers "0.1.0") (hash "136p7sv62f2wm7gd9xwki6anry2a2ynfbcyb40pplj6vyfb3v2y6")))

(define-public crate-learn_itor-0.1 (crate (name "learn_itor") (vers "0.1.0") (hash "166f6f8dmgscl93xxyhq7i116nwdszd3d3vmjwcgnlmsrlqbdvl7")))

(define-public crate-learn_publish-0.1 (crate (name "learn_publish") (vers "0.1.0") (hash "0skhmfkd7d7i426mms1a3higz5rlh9lcwama97vjjqbzadwd3wq1")))

(define-public crate-learn_rust-0.1 (crate (name "learn_rust") (vers "0.1.0") (hash "00dd1fslavp66hbsplxw96fp4fyaa8rns2gapg62mdmaskvs92ac")))

(define-public crate-learn_rust-0.1 (crate (name "learn_rust") (vers "0.1.1") (hash "1sxhjp6p0p7fcvplszsc15kbry1vl6yg9b7pmvw2yg3j856qd5ih")))

(define-public crate-learn_rust-0.1 (crate (name "learn_rust") (vers "0.1.2") (hash "1pyq7v3l56gm0wzgca3qmnnwgqigqkg015s0bin9by8w6cqwm8j8")))

(define-public crate-learn_rust-0.1 (crate (name "learn_rust") (vers "0.1.3") (hash "1p28283866g300y6jpyglgkqlq490ngfswyqx1sb9l55rx08zc93")))

(define-public crate-learn_rust_bugs-0.1 (crate (name "learn_rust_bugs") (vers "0.1.0") (deps (list (crate-dep (name "env_logger") (req "^0.9") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)))) (hash "051wz971xa5vl04yhxvspdj697b5yrsrbssax9cwh0j46gbbyrh8")))

(define-public crate-learn_rust_demo-0.1 (crate (name "learn_rust_demo") (vers "0.1.0") (hash "0qlivvkiw1bzk496wqjda303hd9plm4vlghg0q167fmr4alms8nr")))

(define-public crate-learn_rust_idiot-0.1 (crate (name "learn_rust_idiot") (vers "0.1.0") (hash "0bljs2ppzh4l5glpfn562jbfjb1jyzwpy1mpks6np1bdqivcq3qb")))

(define-public crate-learn_rust_perceptron-0.1 (crate (name "learn_rust_perceptron") (vers "0.1.0") (hash "0ddw9x0v1djwa97kdpi4qx8q404xjw0j2h4d3kyydj4af60amsj0")))

(define-public crate-learn_rust_project_guofei9987-0.1 (crate (name "learn_rust_project_guofei9987") (vers "0.1.0") (hash "17i1awinb8wvi2r2zvb0brniqjhxvlzmljfab31zacsbs8ddfjns") (yanked #t)))

(define-public crate-learn_rust_together-0.1 (crate (name "learn_rust_together") (vers "0.1.1") (hash "080krg3s0y92gbh3fpra0jn01fby75pjrzspyra1x9ih1marlz95")))

(define-public crate-learn_sunny_demo_aaa-0.1 (crate (name "learn_sunny_demo_aaa") (vers "0.1.0") (deps (list (crate-dep (name "rand") (req "^0.8.4") (default-features #t) (kind 0)))) (hash "0x1jqs3cjapj6yj1qldjh8rqv45zn1kav6kbagbs528kpljijman") (yanked #t)))

(define-public crate-learn_together-0.1 (crate (name "learn_together") (vers "0.1.0") (hash "0ba64xj0jivdpw5fyp2jkwbn4g49g5b5n7z1khr3a6mn4jrwv024")))

(define-public crate-learn_together-0.1 (crate (name "learn_together") (vers "0.1.1") (hash "0wkf5hqirvhbm9pw0v43q4darfq2mbzxdd3cz1wa7s9y06md6g5c")))

(define-public crate-learning-0.1 (crate (name "learning") (vers "0.1.0") (deps (list (crate-dep (name "help") (req "^0.0.0") (default-features #t) (kind 0)) (crate-dep (name "walkdir") (req "^2.3.2") (default-features #t) (kind 0)))) (hash "0lhjgl7i0n71p17b66pgyxvb65kfyf5y35yamhrrqvfm6m69dm7z") (yanked #t)))

(define-public crate-learning-the-book-0.1 (crate (name "learning-the-book") (vers "0.1.0") (hash "1zimy754bajsm9f8sclxl5msrmi5fjx3rzz9agskrk206kr96pa8")))

(define-public crate-learning-the-book-0.1 (crate (name "learning-the-book") (vers "0.1.1") (hash "0ri9qc4m2asbv8rxssm24lx47sf899z8k5595zm6swgxc1vnwjlx")))

(define-public crate-learning-trait-0.1 (crate (name "learning-trait") (vers "0.1.0") (hash "02shc1bx28q85mysg2l655aw9yxvck311glgdgfmz2yap7vfh043")))

(define-public crate-learning_art-0.1 (crate (name "learning_art") (vers "0.1.0") (hash "0mv2kfpq8m48cvxrn4mqfg5llv3kb6n2j45y8fxirsndnm4wn31q")))

(define-public crate-learning_fromdocs_minigrep-0.1 (crate (name "learning_fromdocs_minigrep") (vers "0.1.0") (hash "0m62kwwrmns5c9q76xj1cappb4ih619wcj8xmq0jh8cdnh8q1zih")))

(define-public crate-learning_rust-0.1 (crate (name "learning_rust") (vers "0.1.0") (hash "02sgfiz3psmg44l0n6h48i7wbw74yvl127rns2nirz0y2rrrwv4v")))

(define-public crate-learning_rust_pa-0.1 (crate (name "learning_rust_pa") (vers "0.1.0") (hash "12hlihfnmgwan67cwjjcdx6yjycscmmfm7ikwpi7zn34p6pqgppv") (yanked #t)))

(define-public crate-learnpra-0.0.1 (crate (name "learnpra") (vers "0.0.1") (deps (list (crate-dep (name "doc-comment") (req "^0.3") (default-features #t) (kind 0)))) (hash "0iwl30chy5j7262ym5f27cm8ns07acmdp178m42lx70ychisfsfw")))

(define-public crate-learnrust-0.1 (crate (name "learnrust") (vers "0.1.0") (deps (list (crate-dep (name "rand") (req "^0.3.14") (default-features #t) (kind 0)))) (hash "0rkdd20xmm2yxp4pqxjfs3wvfg7f9gv03mhyf6nhg93m9i2rw0am")))

(define-public crate-learntk-0.1 (crate (name "learntk") (vers "0.1.0") (deps (list (crate-dep (name "indicatif") (req "^0.17.3") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)))) (hash "14p8bl5pgpf55k30xycg30yc77wd56rwsgi7gyjxbcli30cigjbf")))

(define-public crate-learntk-0.2 (crate (name "learntk") (vers "0.2.0") (deps (list (crate-dep (name "indicatif") (req "^0.17.3") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)))) (hash "1az9qbz92ihfn8dbm5crc1fmk10hqq1z4l54hzmfhsvgk1fqv7ik")))

(define-public crate-learntk-0.2 (crate (name "learntk") (vers "0.2.1") (deps (list (crate-dep (name "indicatif") (req "^0.17.3") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)))) (hash "1z34c867z4yg2v1rdaamb07a1ag5040dx73zl5va572arj9js0c6")))

(define-public crate-learntk-0.3 (crate (name "learntk") (vers "0.3.0") (deps (list (crate-dep (name "indicatif") (req "^0.17.3") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)))) (hash "12l4hqici8acaabv1wh38r9x5p7zi5pml4cr0p2jg3z7nnrmlg66")))

(define-public crate-learnwell-0.1 (crate (name "learnwell") (vers "0.1.0") (deps (list (crate-dep (name "fastrand") (req "^1.8.0") (default-features #t) (kind 0)) (crate-dep (name "fxhash") (req "^0.2.1") (default-features #t) (kind 0)) (crate-dep (name "show-image") (req "^0.13.1") (default-features #t) (kind 0)))) (hash "1lx42lpnfq9lk02vnnyj1fk4jwvyzn0m7z648j3jrz37niiz382a")))

(define-public crate-learnwell-0.2 (crate (name "learnwell") (vers "0.2.0") (deps (list (crate-dep (name "fastrand") (req "^1.9.0") (default-features #t) (kind 0)) (crate-dep (name "fxhash") (req "^0.2.1") (default-features #t) (kind 0)) (crate-dep (name "runnt") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "show-image") (req "^0.13.1") (default-features #t) (kind 0)) (crate-dep (name "strum") (req "^0.24.1") (features (quote ("strum_macros"))) (default-features #t) (kind 0)))) (hash "1q5xhnkl5q7sy5rka0qqa4kf68zjk3rasa666hc2vz8qjhfshjcp")))

(define-public crate-learnwell-0.2 (crate (name "learnwell") (vers "0.2.1") (deps (list (crate-dep (name "fastrand") (req "^1.9.0") (default-features #t) (kind 0)) (crate-dep (name "fxhash") (req "^0.2.1") (default-features #t) (kind 0)) (crate-dep (name "runnt") (req "^0.6.0") (default-features #t) (kind 0)) (crate-dep (name "show-image") (req "^0.13.1") (default-features #t) (kind 0)) (crate-dep (name "strum") (req "^0.24.1") (features (quote ("strum_macros"))) (default-features #t) (kind 0)))) (hash "1p8gz81r7bgyzgpnssyglzb7wiwcqp13dw1sxw8d49f64jb15xml")))

(define-public crate-learnwy_bin_utils-0.1 (crate (name "learnwy_bin_utils") (vers "0.1.0") (deps (list (crate-dep (name "confy") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "human-panic") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.7") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)))) (hash "0mcpcn5rqvqkxc1ih079grw7a8ahrcpq53lr6h610j7rawpji91l")))

(define-public crate-learnwy_bin_utils-0.1 (crate (name "learnwy_bin_utils") (vers "0.1.1") (deps (list (crate-dep (name "confy") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "human-panic") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.7") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)))) (hash "0n5ri7v3h4934j85bpz295brxlkm1qhipa2z6xfrayrvlh61csj6")))

(define-public crate-learnwy_bin_utils-0.1 (crate (name "learnwy_bin_utils") (vers "0.1.2") (deps (list (crate-dep (name "chrono") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "confy") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "dirs") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "human-panic") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.7") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)))) (hash "0d9r5fpb5g8mv3fqy2aqraspf2qrcy1yk08q3922d9wj95dzh8yb")))

(define-public crate-learnwy_bin_utils-0.2 (crate (name "learnwy_bin_utils") (vers "0.2.0") (deps (list (crate-dep (name "anyhow") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "base64") (req "^0") (default-features #t) (kind 0)) (crate-dep (name "chrono") (req "^0") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^3") (features (quote ("derive" "env"))) (default-features #t) (kind 0)) (crate-dep (name "clap_complete") (req "^3") (default-features #t) (kind 0)) (crate-dep (name "colored") (req "^2") (default-features #t) (kind 0)) (crate-dep (name "confy") (req "^0") (default-features #t) (kind 0)) (crate-dep (name "dirs") (req "^4") (default-features #t) (kind 0)) (crate-dep (name "env_logger") (req "^0") (default-features #t) (kind 0)) (crate-dep (name "fnm") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "fs_extra") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "human-panic") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "indoc") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0") (default-features #t) (kind 0)) (crate-dep (name "pretty_assertions") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0") (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0") (features (quote ("blocking" "json" "rustls-tls" "rustls-tls-native-roots" "brotli"))) (kind 0)) (crate-dep (name "semver") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "toml") (req "^0") (default-features #t) (kind 0)) (crate-dep (name "url") (req "^2") (default-features #t) (kind 0)))) (hash "1lfkdbx6hfkxrc5g50b8xclbq9fhbb4nklx02h1mqlddis4fdadx")))

