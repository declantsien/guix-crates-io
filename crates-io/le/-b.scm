(define-module (crates-io le -b) #:use-module (crates-io))

(define-public crate-le-bitset-0.1 (crate (name "le-bitset") (vers "0.1.0") (hash "0v7xmq94qhnciag3icrw8f68y1lcpkrwd4nqk7mxjcb1hsw7z2hc")))

(define-public crate-le-bitset-0.1 (crate (name "le-bitset") (vers "0.1.1") (hash "1kn7lrr7l01l2cm6z8dr361lxvjhlipm4sz626rmy0ql0h23xwcj")))

(define-public crate-le-bitset-0.1 (crate (name "le-bitset") (vers "0.1.2") (deps (list (crate-dep (name "serde") (req "^1.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0") (optional #t) (default-features #t) (kind 0)))) (hash "044acisqrbgmjn0ra3f1xqvxc967qfd949nssjcr3fcgjc4sxsh5") (v 2) (features2 (quote (("serde" "dep:serde" "serde_derive"))))))

(define-public crate-le-bitset-0.1 (crate (name "le-bitset") (vers "0.1.3") (deps (list (crate-dep (name "serde") (req "^1.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0") (optional #t) (default-features #t) (kind 0)))) (hash "0fw17k2xpzkz2wfdhf365vlf4j7jp79h5l9ggn63w6rca99yb86b") (v 2) (features2 (quote (("serde" "dep:serde" "serde_derive"))))))

