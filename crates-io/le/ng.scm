(define-module (crates-io le ng) #:use-module (crates-io))

(define-public crate-lenght_aware_paginator-0.1 (crate (name "lenght_aware_paginator") (vers "0.1.0") (deps (list (crate-dep (name "diesel") (req "^1.4.5") (features (quote ("postgres"))) (default-features #t) (kind 0)) (crate-dep (name "dotenv") (req "^0.15.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "1j5a17bwvmqla5dzmwd67n2iabvvkl8n70nqqcp23yfr0h51gngm") (yanked #t)))

(define-public crate-length-0.1 (crate (name "length") (vers "0.1.0") (deps (list (crate-dep (name "lazy_static") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)))) (hash "190pk5sibfjrbj2vj3233ghvfiw45f2lzvjl2q3ddn7686af75d4")))

(define-public crate-length-0.2 (crate (name "length") (vers "0.2.0") (deps (list (crate-dep (name "lazy_static") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)))) (hash "05p6c5ac1xaa4hlyfldvnzy8di7n1qlzn4jmc3vb1p8lns1ajprv")))

(define-public crate-length-0.2 (crate (name "length") (vers "0.2.1") (deps (list (crate-dep (name "lazy_static") (req "^1.4") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.6") (default-features #t) (kind 0)))) (hash "00rv0dqyq60zpji7raqbkppxw8crxrp7a379pqh5m6pp4aajpf7w")))

(define-public crate-length-0.2 (crate (name "length") (vers "0.2.2") (deps (list (crate-dep (name "lazy_static") (req "^1.4") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.7") (default-features #t) (kind 0)))) (hash "0pz8zwzxn2j77avff2gkfq74qqnz0cwxsvs546ynh5gcradwxsmi")))

(define-public crate-length-prefixed-stream-1 (crate (name "length-prefixed-stream") (vers "1.0.0") (deps (list (crate-dep (name "async-std") (req "^1.9.0") (default-features #t) (kind 0)) (crate-dep (name "desert") (req "^2.0.0") (default-features #t) (kind 0)) (crate-dep (name "futures") (req "^0.3.12") (default-features #t) (kind 0)) (crate-dep (name "futures-core") (req "^0.3.12") (default-features #t) (kind 0)) (crate-dep (name "pin-project-lite") (req "^0.2.4") (default-features #t) (kind 0)))) (hash "17mzv0c78hksla41201fps75l6d6nmdm2r7l0bk249l3wk2qmacs")))

(define-public crate-length_aware_paginator-0.1 (crate (name "length_aware_paginator") (vers "0.1.0") (deps (list (crate-dep (name "diesel") (req "^1.4.5") (features (quote ("postgres"))) (default-features #t) (kind 0)) (crate-dep (name "dotenv") (req "^0.15.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "1ha930fjyk80ilhrlw7kz0fl32ifhx1150cqm0qy9j8wc27igial")))

(define-public crate-length_aware_paginator-0.3 (crate (name "length_aware_paginator") (vers "0.3.0") (deps (list (crate-dep (name "diesel") (req "^1.4.5") (features (quote ("postgres"))) (default-features #t) (kind 0)) (crate-dep (name "dotenv") (req "^0.15.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "0x5mpwr400f6a92r0gs6kvy0i3jirvn8sx60ig1v0kf4x3zyvawg")))

(define-public crate-length_aware_paginator-1 (crate (name "length_aware_paginator") (vers "1.0.0") (deps (list (crate-dep (name "diesel") (req "^2.0.0-rc.0") (features (quote ("postgres"))) (default-features #t) (kind 0)) (crate-dep (name "dotenv") (req "^0.15.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "1249wk7b4sl2vdvcq6wwhlb8013zc2v4dp8lw175wrmpr32ii5xz")))

