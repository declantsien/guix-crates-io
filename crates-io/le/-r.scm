(define-module (crates-io le -r) #:use-module (crates-io))

(define-public crate-le-robert-0.1 (crate (name "le-robert") (vers "0.1.0") (deps (list (crate-dep (name "reqwest") (req "^0.11.18") (features (quote ("blocking"))) (default-features #t) (kind 0)) (crate-dep (name "scraper") (req "^0.16.0") (default-features #t) (kind 0)))) (hash "1my2sv02r9awpma8kawhcappbmb94x834hhpdywhya8nl4pz72hi")))

