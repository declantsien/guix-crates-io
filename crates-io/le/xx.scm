(define-module (crates-io le xx) #:use-module (crates-io))

(define-public crate-lexx-0.1 (crate (name "lexx") (vers "0.1.0") (deps (list (crate-dep (name "syntex_errors") (req "^0.46.0") (default-features #t) (kind 0)) (crate-dep (name "syntex_syntax") (req "^0.46.0") (default-features #t) (kind 0)))) (hash "1zzh2dd2cz1dll2kv6r367md4fcgrpcqlmjgz676mipy43lh0wm5")))

