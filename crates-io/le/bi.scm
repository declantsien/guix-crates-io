(define-module (crates-io le bi) #:use-module (crates-io))

(define-public crate-lebicon-1 (crate (name "lebicon") (vers "1.0.0") (deps (list (crate-dep (name "codicon") (req "^1.0.0") (default-features #t) (kind 0)) (crate-dep (name "leb128") (req "^0.2.1") (default-features #t) (kind 2)) (crate-dep (name "signrel") (req "^1.0.0") (default-features #t) (kind 0)) (crate-dep (name "uabs") (req "^1.0.0") (default-features #t) (kind 0)))) (hash "1s1m5v12m68bhxs4j4wy9w8y9gbi8nryfg1wfd911lh2racps2ag")))

(define-public crate-lebicon-2 (crate (name "lebicon") (vers "2.0.0") (deps (list (crate-dep (name "codicon") (req "^1.0.0") (default-features #t) (kind 0)) (crate-dep (name "leb128") (req "^0.2.1") (default-features #t) (kind 2)) (crate-dep (name "signrel") (req "^1.0.0") (default-features #t) (kind 0)) (crate-dep (name "uabs") (req "^1.0.0") (default-features #t) (kind 0)))) (hash "1px34xicqzzpc87vh9rrjhwgzdahq6s520d809l5cqysqh6as83z")))

(define-public crate-lebicon-3 (crate (name "lebicon") (vers "3.0.0") (deps (list (crate-dep (name "codicon") (req "^2.0.0") (default-features #t) (kind 0)) (crate-dep (name "leb128") (req "^0.2.1") (default-features #t) (kind 2)) (crate-dep (name "signrel") (req "^1.0.0") (default-features #t) (kind 0)) (crate-dep (name "uabs") (req "^1.0.0") (default-features #t) (kind 0)))) (hash "1690sjrgqlw2zsckdmn02x1yyal5mraagl3hpnbmvzmkvw714i51")))

(define-public crate-lebicon-4 (crate (name "lebicon") (vers "4.0.0") (deps (list (crate-dep (name "codicon") (req "^3.0") (default-features #t) (kind 0)) (crate-dep (name "leb128") (req "^0.2") (default-features #t) (kind 2)) (crate-dep (name "signrel") (req "^2.0") (default-features #t) (kind 0)) (crate-dep (name "uabs") (req "^3.0") (default-features #t) (kind 0)))) (hash "080ihmyfh2vkasxz7bvy0988wnl64kwg77a8yccp9sari6dbf660")))

