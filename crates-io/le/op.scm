(define-module (crates-io le op) #:use-module (crates-io))

(define-public crate-leopard-0.1 (crate (name "leopard") (vers "0.1.0") (hash "0m12fcz1w58sxag90p52ykz7byrvccvmv4qwinw8aqss8siq5hbf")))

(define-public crate-leopard-codec-0.1 (crate (name "leopard-codec") (vers "0.1.0") (deps (list (crate-dep (name "bytes") (req "^1.5") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.4") (default-features #t) (kind 0)) (crate-dep (name "proptest") (req "^1.4.0") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.8") (default-features #t) (kind 2)) (crate-dep (name "test-strategy") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "thiserror") (req "^1") (default-features #t) (kind 0)))) (hash "078s3ffq4afbyag1d9jhr4gnvcqqfr2y05d9gmfqh8xx2k2dnn7f")))

