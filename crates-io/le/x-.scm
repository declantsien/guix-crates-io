(define-module (crates-io le x-) #:use-module (crates-io))

(define-public crate-lex-lib-0.1 (crate (name "lex-lib") (vers "0.1.0") (hash "0bbpkvzxmq9l9m0jfsqwi07d90j3jjh5wzknz7ab8lrn9klc42yz")))

(define-public crate-lex-lib-0.2 (crate (name "lex-lib") (vers "0.2.0") (hash "1xhgskcn8yhpbklyp2hqvcsvj6w937ngkg8zph611sw32hqcnw2g")))

(define-public crate-lex-lib-0.3 (crate (name "lex-lib") (vers "0.3.0") (hash "0h59np2ixaihkb0xrpxy2wbw6jlxj8vk0yjx1j9sl4yibjcdjskh")))

(define-public crate-lex-lib-0.3 (crate (name "lex-lib") (vers "0.3.1") (hash "0gchgs50n76c2casd62px3vxpg4mkjbdg3xr1w47ywg6lqgy58sw")))

(define-public crate-lex-lib-0.3 (crate (name "lex-lib") (vers "0.3.2") (hash "022r92n05bp7j7f1gvv2lqs1s434x84xnx6h972yldgkigz6ax7c")))

(define-public crate-lex-map_editor_std-0.1 (crate (name "lex-map_editor_std") (vers "0.1.0") (hash "1iyk84rd8jb6gysdq88ipbcgggl3bkbkg5z7h2a2r9kxamr4b5yc")))

(define-public crate-lex-map_editor_std-0.1 (crate (name "lex-map_editor_std") (vers "0.1.1") (hash "0a60mjshm0hpqyzrfvyhpigvvl43814n0lisan0vjbgkalcnnjfg")))

(define-public crate-lex-map_editor_std-0.2 (crate (name "lex-map_editor_std") (vers "0.2.0") (hash "1vfn75z7pkzqkl3hvq5ck6acrk7hc2c9s7rv2l56hfiabkvlx1cj")))

