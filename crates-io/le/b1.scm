(define-module (crates-io le b1) #:use-module (crates-io))

(define-public crate-leb128-0.1 (crate (name "leb128") (vers "0.1.0") (deps (list (crate-dep (name "clippy") (req "^0.0.76") (optional #t) (default-features #t) (kind 0)))) (hash "0a3dhzhw3dla9i9yabwg60pk8ncfmm6xg17b311601kdqpfl9ah3") (features (quote (("nightly" "clippy"))))))

(define-public crate-leb128-0.2 (crate (name "leb128") (vers "0.2.0") (hash "0lh04gbg8lqswp84m2pf62413zix9qfvdxw9acyk4w21xdaykqgs") (features (quote (("nightly"))))))

(define-public crate-leb128-0.2 (crate (name "leb128") (vers "0.2.1") (hash "04njpamfckg1sgvcnby8wdr1vz1xs99lyziqkvsv1r476ykj88kr") (features (quote (("nightly"))))))

(define-public crate-leb128-0.2 (crate (name "leb128") (vers "0.2.2") (hash "079l6bwilz01zavhnhmczq1lmkq7xkpv4l6ihr66is84fqjl408c") (features (quote (("nightly"))))))

(define-public crate-leb128-0.2 (crate (name "leb128") (vers "0.2.3") (hash "1579s5by5ag85nfal5jdcrcxylp3n6z274q4ahgjslq01g95hy36") (features (quote (("nightly"))))))

(define-public crate-leb128-0.2 (crate (name "leb128") (vers "0.2.4") (deps (list (crate-dep (name "quickcheck") (req "^0.8.0") (default-features #t) (kind 2)))) (hash "0aid6qy7qlyqr9rhb3md4s5693b93pdidkfzdw86y3x05dzshxim") (features (quote (("nightly"))))))

(define-public crate-leb128-0.2 (crate (name "leb128") (vers "0.2.5") (deps (list (crate-dep (name "quickcheck") (req "^0.8.0") (default-features #t) (kind 2)))) (hash "0rxxjdn76sjbrb08s4bi7m4x47zg68f71jzgx8ww7j0cnivjckl8") (features (quote (("nightly"))))))

(define-public crate-leb128-u64-0.1 (crate (name "leb128-u64") (vers "0.1.0") (deps (list (crate-dep (name "bytes") (req "^1.4") (kind 0)) (crate-dep (name "proptest") (req "^1.1") (default-features #t) (kind 2)))) (hash "0sjv2p6nkbkjmq32cxzfpc9jzkmmfpysawaps7bw0z5lhrnxvm6n")))

(define-public crate-leb128-u64-0.1 (crate (name "leb128-u64") (vers "0.1.1") (deps (list (crate-dep (name "bytes") (req "^1.4") (kind 0)) (crate-dep (name "proptest") (req "^1.1") (default-features #t) (kind 2)))) (hash "05576pdywhlav9q37c3qi0gcim5gk7v8ipkqlf5r8fjmqf23g7v5")))

(define-public crate-leb128fmt-0.1 (crate (name "leb128fmt") (vers "0.1.0") (hash "1chxm1484a0bly6anh6bd7a99sn355ymlagnwj3yajafnpldkv89") (features (quote (("std") ("default" "std") ("alloc")))) (rust-version "1.56.0")))

(define-public crate-leb128plus-0.2 (crate (name "leb128plus") (vers "0.2.0") (hash "03hp859xz4fvbkb60ks5p0125d59sl56sxmdqcphjxfis8hd1dix")))

(define-public crate-leb128plus-0.3 (crate (name "leb128plus") (vers "0.3.0") (hash "0zb2kffls7bwy3hcpinfhfqj9gs00rik7hhlj1d7si4f0n613y10")))

(define-public crate-leb128plus-0.4 (crate (name "leb128plus") (vers "0.4.0") (hash "0i9ds206l7msf637yknaz8h3dxg2abb0lp57w533kg1vj7pib23z")))

(define-public crate-leb128plus-0.5 (crate (name "leb128plus") (vers "0.5.0") (deps (list (crate-dep (name "int") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2.6") (default-features #t) (kind 0)))) (hash "04w9z5xrnz505v647gc94n3zssslzajb1kaw2rck44gzwbf8icfc")))

(define-public crate-leb128plus-0.5 (crate (name "leb128plus") (vers "0.5.1") (deps (list (crate-dep (name "int") (req "^0.2.0") (default-features #t) (kind 0)))) (hash "0dp9mj8rkhjm9h4fq49chcvhf4aa80x0irsgqymn7d8n6fr3nvdl")))

(define-public crate-leb128plus-0.6 (crate (name "leb128plus") (vers "0.6.0") (deps (list (crate-dep (name "int") (req "^0.2.10") (default-features #t) (kind 0)))) (hash "1cm298gyjm9d1ay4kz8zh1rpsnc4dq2a3c4zmls9rbckzay7b6qf")))

