(define-module (crates-io le ic) #:use-module (crates-io))

(define-public crate-leicester-0.0.0 (crate (name "leicester") (vers "0.0.0") (deps (list (crate-dep (name "rust-iptables") (req "^0.0.2") (default-features #t) (kind 0)))) (hash "02ygyr1l45q59cbcaar0ymxq14m2c9lqrd9i1w42dlx14fliyn7g")))

(define-public crate-leicester-0.0.1 (crate (name "leicester") (vers "0.0.1") (deps (list (crate-dep (name "rust-iptables") (req "^0.0.2") (default-features #t) (kind 0)))) (hash "1p8nysqzkj5grqs320qsnz27p4l7xf9h5h4lh0z20dzqndan9qsk")))

