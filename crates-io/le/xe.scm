(define-module (crates-io le xe) #:use-module (crates-io))

(define-public crate-lexeme-0.0.0 (crate (name "lexeme") (vers "0.0.0") (hash "1fcwjymh2668w2bmp23d5h3iplh2ypbnhshsaj9mfja1l2bnqw4z")))

(define-public crate-lexer-0.1 (crate (name "lexer") (vers "0.1.0") (hash "132qb5yid78ixw7gqfvla9nlp7gflwkbq2gjfcwmk5syivac6bxn")))

(define-public crate-lexer-0.1 (crate (name "lexer") (vers "0.1.1") (hash "1disg87jiyglm2jmd09sap87qxwlvz4922i1a77sz3nij5shlb2x")))

(define-public crate-lexer-0.1 (crate (name "lexer") (vers "0.1.2") (deps (list (crate-dep (name "serde") (req "^1.0") (kind 0)) (crate-dep (name "serde_derive") (req "^1.0") (default-features #t) (kind 0)))) (hash "0k9szgrsdaf0d0xpccn19lxmmpxl5rhbxybchanfymla3d802389") (features (quote (("use_std") ("default" "use_std"))))))

(define-public crate-lexer-0.1 (crate (name "lexer") (vers "0.1.3") (deps (list (crate-dep (name "serde") (req "^1.0") (kind 0)) (crate-dep (name "serde_derive") (req "^1.0") (default-features #t) (kind 0)))) (hash "1lpdicawggqw55lqracv8dsrry3xzhp4v3nz1dqirhksyy3f4sa5") (features (quote (("use_std") ("default" "use_std"))))))

(define-public crate-lexer-0.1 (crate (name "lexer") (vers "0.1.4") (deps (list (crate-dep (name "serde") (req "^1.0") (kind 0)) (crate-dep (name "serde_derive") (req "^1.0") (default-features #t) (kind 0)))) (hash "1kg1h04n7xc122589wpy7inmflvsrzpy33fkbcibj1dwll4llrap") (features (quote (("use_std") ("default" "use_std"))))))

(define-public crate-lexer-0.1 (crate (name "lexer") (vers "0.1.5") (deps (list (crate-dep (name "serde") (req "^1.0") (kind 0)) (crate-dep (name "serde_derive") (req "^1.0") (default-features #t) (kind 0)))) (hash "1bc03kycqk14pckrlhnk669sqj753b2wfh8zk95vaqf8l8yg7phz") (features (quote (("use_std") ("default" "use_std"))))))

(define-public crate-lexer-0.1 (crate (name "lexer") (vers "0.1.6") (deps (list (crate-dep (name "chars_input") (req "^0.2") (kind 0)) (crate-dep (name "serde") (req "^1.0") (kind 0)) (crate-dep (name "serde_derive") (req "^1.0") (default-features #t) (kind 0)))) (hash "1z9r316ynymgpmyplghjzqdpmc6hpxbxjv43z58hjbd5712fwis0") (features (quote (("std" "chars_input/std") ("default" "std"))))))

(define-public crate-lexer-0.1 (crate (name "lexer") (vers "0.1.7") (deps (list (crate-dep (name "chars_input") (req "^0.2") (kind 0)) (crate-dep (name "serde") (req "^1.0") (kind 0)) (crate-dep (name "serde_derive") (req "^1.0") (default-features #t) (kind 0)))) (hash "0bpfipkdabn6whp63n6f012xgac8dwjghxzfn13dxpgagxzr6jdd") (features (quote (("std" "chars_input/std") ("default" "std"))))))

(define-public crate-lexer-0.1 (crate (name "lexer") (vers "0.1.8") (deps (list (crate-dep (name "chars_input") (req "^0.2") (kind 0)) (crate-dep (name "serde") (req "^1.0") (kind 0)) (crate-dep (name "serde_derive") (req "^1.0") (default-features #t) (kind 0)))) (hash "0n5cpkgd76v27n9c4pr80sxvf7ws7aa3sha8aw92fy5bkrhnsijj") (features (quote (("std" "chars_input/std") ("default" "std"))))))

(define-public crate-lexer-0.1 (crate (name "lexer") (vers "0.1.9") (deps (list (crate-dep (name "chars_input") (req "^0.2") (kind 0)) (crate-dep (name "serde") (req "^1.0") (kind 0)) (crate-dep (name "serde_derive") (req "^1.0") (default-features #t) (kind 0)))) (hash "1i31k3z20ygi3bcs3l39hmh44b3k8vq14hr223vl6kaaaqinz03q") (features (quote (("std" "chars_input/std") ("default" "std"))))))

(define-public crate-lexer-0.1 (crate (name "lexer") (vers "0.1.10") (deps (list (crate-dep (name "peek-nth") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (kind 0)) (crate-dep (name "serde_derive") (req "^1.0") (default-features #t) (kind 0)))) (hash "00mcal434xvzfyh0a6f8m7hvia5irk72a1k8xikh4hlcfwy0099s")))

(define-public crate-lexer-0.1 (crate (name "lexer") (vers "0.1.11") (deps (list (crate-dep (name "peek-nth") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (kind 0)) (crate-dep (name "serde_derive") (req "^1.0") (default-features #t) (kind 0)))) (hash "1s2dllw5cyp2hn6xw5andhwx1hj40i130dmmyy1f25dimbvnr2bz")))

(define-public crate-lexer-0.1 (crate (name "lexer") (vers "0.1.12") (deps (list (crate-dep (name "peek-nth") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (kind 0)) (crate-dep (name "serde_derive") (req "^1.0") (default-features #t) (kind 0)))) (hash "0g9y9pw1fyfad32g3q6xlgvfr7kcvfs9d3mpbm60w0hrib8q2za2")))

(define-public crate-lexer-0.1 (crate (name "lexer") (vers "0.1.13") (deps (list (crate-dep (name "peek-nth") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (kind 0)) (crate-dep (name "serde_derive") (req "^1.0") (default-features #t) (kind 0)))) (hash "1b2pgid7n21y7vk6df43ap3ijbp8jpcnnm453mhiqb02hkawlnn8")))

(define-public crate-lexer-0.1 (crate (name "lexer") (vers "0.1.15") (deps (list (crate-dep (name "peek-nth") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (kind 0)) (crate-dep (name "serde_derive") (req "^1.0") (default-features #t) (kind 0)))) (hash "159abp6anb4bp561sn3s362n1qw1zi92y0sfadldb3p6bbprhlxi")))

(define-public crate-lexer-0.1 (crate (name "lexer") (vers "0.1.16") (deps (list (crate-dep (name "peek-nth") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (kind 0)) (crate-dep (name "serde_derive") (req "^1.0") (default-features #t) (kind 0)))) (hash "1aa44zv7wnaw027pzky6v3ir68gzpg5n1r373gzak61ziymvc0dq")))

(define-public crate-lexer-0.1 (crate (name "lexer") (vers "0.1.18") (deps (list (crate-dep (name "peek-nth") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "1yvphbqi3n0ffraij889fljc2c39myd1a3v01rkvnsmrj51x90jp")))

(define-public crate-lexer-generator-0.1 (crate (name "lexer-generator") (vers "0.1.0") (deps (list (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.136") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (features (quote ("alloc"))) (kind 0)))) (hash "0p9xj4pcapvrwg553afqsmwxfrhyq1vdnh7bfaivz3g598wzq16v")))

(define-public crate-lexer-generator-0.1 (crate (name "lexer-generator") (vers "0.1.1") (deps (list (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.136") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (features (quote ("alloc"))) (kind 0)))) (hash "0pyspz24wah1b8kslw2qsyhb28z5jq6yzmpwlvdl209zjfqj2r94")))

(define-public crate-lexer-generator-0.1 (crate (name "lexer-generator") (vers "0.1.2") (deps (list (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.136") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (features (quote ("alloc"))) (kind 0)))) (hash "1f40ivvx45ngs46a7vp1sn7jd0i2n1kazi7x8vjzxg2h682kpiyn")))

(define-public crate-lexer-generator-0.1 (crate (name "lexer-generator") (vers "0.1.3") (deps (list (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.136") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (features (quote ("alloc"))) (kind 0)))) (hash "063fylydqr380gvpnr1z1cxpfz06xxrhnfjxwqpprl3h6sayr5p9")))

(define-public crate-lexer-generator-0.1 (crate (name "lexer-generator") (vers "0.1.4") (deps (list (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.136") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (features (quote ("alloc"))) (kind 0)))) (hash "0rmgcvqf8cw4af1shpii4i6khi2h74zahymfh65163ba8n3ksdp8")))

(define-public crate-lexer-rs-0.0.2 (crate (name "lexer-rs") (vers "0.0.2") (deps (list (crate-dep (name "paste") (req "^1.0") (default-features #t) (kind 0)))) (hash "0mrv24fp10mckxl5qwy77a3sx7pjl2mc23ipmscb8mvvbljlkv77")))

(define-public crate-lexer-rs-0.0.3 (crate (name "lexer-rs") (vers "0.0.3") (deps (list (crate-dep (name "paste") (req "^1.0") (default-features #t) (kind 0)))) (hash "0njw622xxr6wsjq80rja2jhbk5r5mpk4csp91hg3grcrpyfx18wp")))

(define-public crate-lexer-rs-0.0.4 (crate (name "lexer-rs") (vers "0.0.4") (deps (list (crate-dep (name "paste") (req "^1.0") (default-features #t) (kind 0)))) (hash "1cs8pxx0wy1dxnal7cp0rmvm83gi04c2bqhd7xckcrmvyn15lc2a")))

(define-public crate-lexers-0.0.3 (crate (name "lexers") (vers "0.0.3") (hash "0gmivkdaslgdkd46c5ic740la8bwq25bl128003vc0yzfqjlzb2h")))

(define-public crate-lexers-0.0.4 (crate (name "lexers") (vers "0.0.4") (hash "0jca8arncyi5f3yll62iv0md80mj09zx6hp8nxaghpsna7fn89dv")))

(define-public crate-lexers-0.0.5 (crate (name "lexers") (vers "0.0.5") (hash "1djrlp2rp2lr64cbrc815nfv1c3aqwb43irm28fr164mivcxsq9l")))

(define-public crate-lexers-0.0.6 (crate (name "lexers") (vers "0.0.6") (hash "0wvdl39z1gp6if3azbazgg3qgh7pw9ygjn1dbig0a8n7xwnhrq1r")))

(define-public crate-lexers-0.0.7 (crate (name "lexers") (vers "0.0.7") (hash "051b1k2cgam42a1j38pfpx2zwq10d37q8f44f2485pcq827bi22g")))

(define-public crate-lexers-0.0.8 (crate (name "lexers") (vers "0.0.8") (hash "1bdwfp01nif53dn6hn30g1ka0g1r5y48iggpmrpr06vnary5f3y4")))

(define-public crate-lexers-0.1 (crate (name "lexers") (vers "0.1.0") (hash "0mnz6whmgnv992zcgw4b994n1dz1y8z0c2vd1kph6d1fp3v427mg") (yanked #t)))

(define-public crate-lexers-0.1 (crate (name "lexers") (vers "0.1.1") (hash "1axdn3sf7g3zk776ka5pgw56qly6x3zgbxsk24njfi3h5mc4v7ld") (yanked #t)))

(define-public crate-lexers-0.1 (crate (name "lexers") (vers "0.1.2") (hash "0szgfyizffjabhd1il3xyv4khb30hjc8gvb331myyxzli63i51rv")))

(define-public crate-lexers-0.1 (crate (name "lexers") (vers "0.1.3") (hash "1syxbxxzsnf233b2925h0k9y4nbsfc7dq05k7905pwnrzkavfgcs")))

(define-public crate-lexers-0.1 (crate (name "lexers") (vers "0.1.4") (hash "1yla78rqbwnim9fpbvzglsw21qkjm4z5hi9aac9mxzz8v6srppl2")))

