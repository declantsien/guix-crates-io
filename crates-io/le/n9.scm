(define-module (crates-io le n9) #:use-module (crates-io))

(define-public crate-len9-0.3 (crate (name "len9") (vers "0.3.3") (deps (list (crate-dep (name "clap") (req "^2.33.3") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "walkdir") (req "^2.3.2") (default-features #t) (kind 0)))) (hash "0wxn4p8gvry7i6cdqw0cn0km4csc56xrrms4jy935yi3ghqsbyil") (features (quote (("application" "clap")))) (yanked #t)))

(define-public crate-len9-0.3 (crate (name "len9") (vers "0.3.4") (deps (list (crate-dep (name "clap") (req "^2.33.3") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "walkdir") (req "^2.3.2") (default-features #t) (kind 0)))) (hash "0riqvmwd7if6vnwrgnsr36c23jkpyvky5j574898j654nbvzvm1i") (features (quote (("application" "clap")))) (yanked #t)))

