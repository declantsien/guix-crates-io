(define-module (crates-io le er) #:use-module (crates-io))

(define-public crate-leer-0.1 (crate (name "leer") (vers "0.1.0") (deps (list (crate-dep (name "leer-macros") (req "=0.0.1") (optional #t) (default-features #t) (kind 0)))) (hash "0qzqh02wpla9s92bnskhxnxjdjhsfjs674ls82ls2jjzincd6n94") (features (quote (("derive" "leer-macros"))))))

(define-public crate-leer-0.1 (crate (name "leer") (vers "0.1.1") (deps (list (crate-dep (name "leer-macros") (req "=0.0.1") (optional #t) (default-features #t) (kind 0)))) (hash "1y392pncdzris6knvdxgx24y4cd4rlpzncaci1gyf3cdqr4zc747") (features (quote (("derive" "leer-macros"))))))

(define-public crate-leer-macros-0.0.1 (crate (name "leer-macros") (vers "0.0.1") (deps (list (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (default-features #t) (kind 0)))) (hash "0gzzmprv56c1sq8y6vrrxgvsymw1fg4wli89ss96blhr3v78h06z")))

(define-public crate-leeroy-0.0.1 (crate (name "leeroy") (vers "0.0.1") (hash "0b38a2vpwana76w1ardxp4hxd2a6lkm314m4cnykpw4pk45qggpp")))

