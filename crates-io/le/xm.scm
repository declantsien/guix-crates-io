(define-module (crates-io le xm) #:use-module (crates-io))

(define-public crate-lexmatch-0.1 (crate (name "lexmatch") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^2.33.3") (default-features #t) (kind 0)) (crate-dep (name "suffix") (req "^1.2.0") (default-features #t) (kind 0)))) (hash "0k2slgsi56bl9vzdmflqgr8fqsi0j574advrh0200lzhr9jhbqab")))

(define-public crate-lexmatch-0.2 (crate (name "lexmatch") (vers "0.2.0") (deps (list (crate-dep (name "clap") (req "^2.33.3") (default-features #t) (kind 0)) (crate-dep (name "suffix") (req "^1.3.0") (default-features #t) (kind 0)))) (hash "0j577n0mpacjigb1wxp7r3nfdj0yg7iy2m0pgf1jc5n5bzb8j5np")))

