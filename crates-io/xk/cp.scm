(define-module (crates-io xk cp) #:use-module (crates-io))

(define-public crate-xkcp-rs-0.0.0 (crate (name "xkcp-rs") (vers "0.0.0") (deps (list (crate-dep (name "hex-literal") (req "^0.4") (default-features #t) (kind 2)) (crate-dep (name "xkcp-sys") (req "^0.0.0") (default-features #t) (kind 0)))) (hash "0a2yr47cpnpfzz7j9jizjf8jfciz52nxcw1rw36vcdwqqmmf1dhi") (features (quote (("std") ("default" "std")))) (rust-version "1.65")))

(define-public crate-xkcp-rs-0.0.1 (crate (name "xkcp-rs") (vers "0.0.1") (deps (list (crate-dep (name "hex-literal") (req "^0.4") (default-features #t) (kind 2)) (crate-dep (name "xkcp-sys") (req "=0.0.1") (default-features #t) (kind 0)))) (hash "1yacap84a8rsmw5c5p4qcgn0yp4f1nk8v6s6090b20z15i5f328x") (features (quote (("std") ("generic-lc" "xkcp-sys/generic-lc") ("force-generic" "xkcp-sys/force-generic") ("force-compact" "xkcp-sys/force-compact") ("default" "std") ("avr8" "xkcp-sys/avr8")))) (rust-version "1.65")))

(define-public crate-xkcp-rs-0.0.2 (crate (name "xkcp-rs") (vers "0.0.2") (deps (list (crate-dep (name "hex-literal") (req "^0.4") (default-features #t) (kind 2)) (crate-dep (name "xkcp-sys") (req "=0.0.2") (default-features #t) (kind 0)))) (hash "1fq5p5gas8r1b2wl0iyr12lpqp6znp84rdfkfc0g19f9ww67cqh0") (features (quote (("std") ("generic-lc" "xkcp-sys/generic-lc") ("force-generic" "xkcp-sys/force-generic") ("force-compact" "xkcp-sys/force-compact") ("default" "std") ("avr8" "xkcp-sys/avr8")))) (rust-version "1.65")))

(define-public crate-xkcp-rs-0.0.3 (crate (name "xkcp-rs") (vers "0.0.3") (deps (list (crate-dep (name "hex-literal") (req "^0.4") (default-features #t) (kind 2)) (crate-dep (name "xkcp-sys") (req "=0.0.3") (default-features #t) (kind 0)))) (hash "1d4hw3n28fkji0cfwf6gbd42w347g3w871ks1dmfwy3xxy0bscs8") (features (quote (("std") ("generic-lc" "xkcp-sys/generic-lc") ("force-generic" "xkcp-sys/force-generic") ("force-compact" "xkcp-sys/force-compact") ("default" "std") ("avr8" "xkcp-sys/avr8")))) (rust-version "1.65")))

(define-public crate-xkcp-sys-0.0.0 (crate (name "xkcp-sys") (vers "0.0.0") (deps (list (crate-dep (name "bindgen") (req "^0.68") (default-features #t) (kind 1)) (crate-dep (name "cc") (req "^1") (default-features #t) (kind 1)))) (hash "133ajgymgyxln4x9rgx5wpnkc48bl4vjz5pnjxj4ly7c6kvwpf7i") (rust-version "1.65")))

(define-public crate-xkcp-sys-0.0.1 (crate (name "xkcp-sys") (vers "0.0.1") (deps (list (crate-dep (name "bindgen") (req "^0.68") (default-features #t) (kind 1)) (crate-dep (name "cc") (req "^1") (default-features #t) (kind 1)))) (hash "0h4g6930g914gd054jrxqzz9mrq5ls8xwh8m9n0i22599gjg9ky1") (features (quote (("generic-lc") ("force-generic") ("force-compact") ("avr8")))) (rust-version "1.65")))

(define-public crate-xkcp-sys-0.0.2 (crate (name "xkcp-sys") (vers "0.0.2") (deps (list (crate-dep (name "bindgen") (req "^0.68") (default-features #t) (kind 1)) (crate-dep (name "cc") (req "^1") (default-features #t) (kind 1)))) (hash "058kia0i2ysc50jfwkxjk7q83a6izaxyaaypzprnmddiggqfr5d0") (features (quote (("generic-lc") ("force-generic") ("force-compact") ("avr8")))) (rust-version "1.65")))

(define-public crate-xkcp-sys-0.0.3 (crate (name "xkcp-sys") (vers "0.0.3") (deps (list (crate-dep (name "bindgen") (req "^0.68") (default-features #t) (kind 1)) (crate-dep (name "cc") (req "^1") (default-features #t) (kind 1)))) (hash "11ahgh51crq29hwrnrh00d4rv36maayw40nlglg7xz8spcrg2i7s") (features (quote (("generic-lc") ("force-generic") ("force-compact") ("avr8")))) (rust-version "1.65")))

