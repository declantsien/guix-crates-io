(define-module (crates-io xk br) #:use-module (crates-io))

(define-public crate-xkbregistry-0.1 (crate (name "xkbregistry") (vers "0.1.0") (deps (list (crate-dep (name "bindgen") (req "^0.63.0") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2.138") (default-features #t) (kind 0)) (crate-dep (name "pkg-config") (req "^0.3.26") (default-features #t) (kind 1)))) (hash "12lvaxd0vy87fy3x6lk0p88i9qkvsslrwl8v9b54b247aiqp4x4q") (features (quote (("static")))) (yanked #t)))

(define-public crate-xkbregistry-0.1 (crate (name "xkbregistry") (vers "0.1.1") (deps (list (crate-dep (name "bindgen") (req "^0.63.0") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2.138") (default-features #t) (kind 0)) (crate-dep (name "pkg-config") (req "^0.3.26") (default-features #t) (kind 1)))) (hash "1y5w1k4ir8ia8mn15kna05c5m85w6lh40q9scb7mbggd8mfk10bn") (features (quote (("static"))))))

