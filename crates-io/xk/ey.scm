(define-module (crates-io xk ey) #:use-module (crates-io))

(define-public crate-xkeysym-0.1 (crate (name "xkeysym") (vers "0.1.0") (deps (list (crate-dep (name "bytemuck") (req "^1.12.3") (default-features #t) (kind 2)) (crate-dep (name "x11rb") (req "^0.11.0") (default-features #t) (kind 2)))) (hash "0dnydfq3h8hzi0i0sqgwp9n2z5az44cwmrqg9s65gybil30jlmfd") (rust-version "1.32.0")))

(define-public crate-xkeysym-0.1 (crate (name "xkeysym") (vers "0.1.1") (deps (list (crate-dep (name "bytemuck") (req "^1.12.3") (default-features #t) (kind 2)) (crate-dep (name "x11rb") (req "^0.11.0") (default-features #t) (kind 2)))) (hash "0s32yq56vsz3jigbdk2b2m0qgzy8cfg5vrrrhgc1mr1rzaigr7sg") (rust-version "1.32.0")))

(define-public crate-xkeysym-0.2 (crate (name "xkeysym") (vers "0.2.0") (deps (list (crate-dep (name "bytemuck") (req "^1.12.3") (features (quote ("derive"))) (optional #t) (kind 0)) (crate-dep (name "bytemuck") (req "^1.12.3") (default-features #t) (kind 2)) (crate-dep (name "serde") (req "^1.0.160") (features (quote ("derive"))) (optional #t) (kind 0)) (crate-dep (name "x11rb") (req "^0.11.0") (default-features #t) (kind 2)))) (hash "0886dn1rlkiazcp5n6ayqfg0ibpiny62dlbiyr9v4l32nxl8wjh5") (rust-version "1.48.0")))

