(define-module (crates-io xk pa) #:use-module (crates-io))

(define-public crate-xkpass-0.1 (crate (name "xkpass") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^3.2.16") (features (quote ("derive" "wrap_help"))) (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)))) (hash "11819j8yjvra1xv4lzimmzmbpyrw70v9ax24b7k8n91bq5rlqhhb")))

