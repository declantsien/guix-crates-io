(define-module (crates-io ip -f) #:use-module (crates-io))

(define-public crate-ip-family-0.1 (crate (name "ip-family") (vers "0.1.0") (hash "1vz8ysf8jbvij533pm2iq2brzbmaijd34aqyj6v2jrwl9dcknl4c")))

(define-public crate-ip-family-0.2 (crate (name "ip-family") (vers "0.2.0") (hash "0mif8m8l9v8dl6x66h5sw9xwlq6ya8ad02agm31wkm3k65wr9670")))

