(define-module (crates-io ip ri) #:use-module (crates-io))

(define-public crate-iprint-0.1 (crate (name "iprint") (vers "0.1.0") (deps (list (crate-dep (name "log") (req "^0.4.20") (optional #t) (default-features #t) (kind 0)))) (hash "1n873983ha5p5fbk699g83pngqnmiwg4k2c72iy1xnkrx61y66s2") (v 2) (features2 (quote (("log" "dep:log"))))))

(define-public crate-iprint-0.1 (crate (name "iprint") (vers "0.1.1") (deps (list (crate-dep (name "log") (req "^0.4.20") (optional #t) (default-features #t) (kind 0)))) (hash "00wlqjidxrkajw97blwmpz7ljifwj61gs5pdcv8j1g9hif64knq1") (v 2) (features2 (quote (("log" "dep:log"))))))

(define-public crate-iprint-0.1 (crate (name "iprint") (vers "0.1.2") (deps (list (crate-dep (name "log") (req "^0.4.20") (optional #t) (default-features #t) (kind 0)))) (hash "0xhw59wc57m6w6c706virlshwcflrgc9qf918576x9lfzav0vfwf") (v 2) (features2 (quote (("log" "dep:log"))))))

(define-public crate-iprint-0.1 (crate (name "iprint") (vers "0.1.3") (deps (list (crate-dep (name "log") (req "^0.4.20") (optional #t) (default-features #t) (kind 0)))) (hash "002wh9a367h7j2if1i2c0w84k3gbd7da5r2n702q99wljbcgkgf5") (v 2) (features2 (quote (("log" "dep:log"))))))

(define-public crate-iprint-0.1 (crate (name "iprint") (vers "0.1.4") (hash "0wvhb7mrylayxzgyqa50mwwv1vc951cwxkam2f0cidndx39rd4wl") (features (quote (("log"))))))

