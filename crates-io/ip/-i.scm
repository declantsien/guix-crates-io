(define-module (crates-io ip #{-i}#) #:use-module (crates-io))

(define-public crate-ip-in-subnet-0.1 (crate (name "ip-in-subnet") (vers "0.1.0") (deps (list (crate-dep (name "netaddr2") (req "^0.7.1") (default-features #t) (kind 0)))) (hash "0nw6aigf474g21209mndh8kkdal34dysrp5bdshcy1mzbr9lxgcs")))

(define-public crate-ip-in-subnet-0.1 (crate (name "ip-in-subnet") (vers "0.1.1") (deps (list (crate-dep (name "netaddr2") (req "^0.7.1") (default-features #t) (kind 0)))) (hash "1ywzckx923kmxjygxz78fybvl8hn20v9i81zwmrnbwnrpyrlka0n")))

(define-public crate-ip-in-subnet-0.1 (crate (name "ip-in-subnet") (vers "0.1.2") (deps (list (crate-dep (name "netaddr2") (req "^0.7.1") (default-features #t) (kind 0)))) (hash "0n59zlnpdripjypwp3svkli578ncz9q1sqx6ckwkms724f36cqnl")))

(define-public crate-ip-in-subnet-0.1 (crate (name "ip-in-subnet") (vers "0.1.3") (deps (list (crate-dep (name "netaddr2") (req "^0.7.1") (default-features #t) (kind 0)))) (hash "1mar5n95w9c5kvc5a0nyrllspfmjj4kl0xhavvxp8kc25hprhf5g")))

(define-public crate-ip-in-subnet-0.1 (crate (name "ip-in-subnet") (vers "0.1.4") (deps (list (crate-dep (name "netaddr2") (req "^0.7.1") (default-features #t) (kind 0)))) (hash "0c46pm31ys8y07i3405jixmcy4jqlxh3gnbwyx721nnmzyrh026s")))

