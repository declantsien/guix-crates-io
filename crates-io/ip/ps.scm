(define-module (crates-io ip ps) #:use-module (crates-io))

(define-public crate-ippsec-0.1 (crate (name "ippsec") (vers "0.1.0") (deps (list (crate-dep (name "anyhow") (req "^1.0.52") (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.11") (features (quote ("blocking" "json"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.133") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.74") (default-features #t) (kind 0)) (crate-dep (name "skim") (req "^0.9.4") (default-features #t) (kind 0)) (crate-dep (name "webbrowser") (req "^0.5.5") (default-features #t) (kind 0)))) (hash "163ph41y31gdrlq2lzdv9cvykyf6wqpfcm0qnnyl78b8dnc6scw1") (features (quote (("nix") ("default")))) (rust-version "1.57")))

