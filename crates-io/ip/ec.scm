(define-module (crates-io ip ec) #:use-module (crates-io))

(define-public crate-ipecho-0.0.1 (crate (name "ipecho") (vers "0.0.1") (deps (list (crate-dep (name "hyper") (req "^0.9.10") (default-features #t) (kind 0)) (crate-dep (name "url") (req "^1.2.0") (default-features #t) (kind 0)))) (hash "13mvb7sayd1f0yaqxp3pddkqs2dnrw1gyzi9bqdlplbd1q3ih0g0")))

