(define-module (crates-io ip -s) #:use-module (crates-io))

(define-public crate-ip-server-0.1 (crate (name "ip-server") (vers "0.1.0") (deps (list (crate-dep (name "getopts") (req "^0.2.21") (default-features #t) (kind 0)))) (hash "0q93clvx570sb3svpagxplir2ars3dnk4src0ldpqg37sdz6gi4p")))

(define-public crate-ip-server-0.1 (crate (name "ip-server") (vers "0.1.1") (deps (list (crate-dep (name "getopts") (req "^0.2.21") (default-features #t) (kind 0)))) (hash "16q8354x03sm92rbly8hri2dlfvgmn80f58z76rj72jfn4xwd460")))

(define-public crate-ip-server-0.1 (crate (name "ip-server") (vers "0.1.2") (deps (list (crate-dep (name "getopts") (req "^0.2.21") (default-features #t) (kind 0)))) (hash "1jmwpck67lpd44sjlyys9af88i4845361ba7p5l4jsq4jkrhh6qb")))

(define-public crate-ip-server-0.1 (crate (name "ip-server") (vers "0.1.3") (deps (list (crate-dep (name "getopts") (req "^0.2.21") (default-features #t) (kind 0)))) (hash "036h5hgw93h4v6xx27a5kzxw399p9nwgnj9617665qr7l7d29a8q")))

(define-public crate-ip-server-0.1 (crate (name "ip-server") (vers "0.1.5") (deps (list (crate-dep (name "getopts") (req "^0.2.21") (default-features #t) (kind 0)))) (hash "1kicsdfcrwfnf80va8998cgw956878pa3ny2sw8j4m90ljrk3qdx")))

(define-public crate-ip-server-0.1 (crate (name "ip-server") (vers "0.1.4") (deps (list (crate-dep (name "getopts") (req "^0.2.21") (default-features #t) (kind 0)))) (hash "0wq9j21kpcjpy4h2r9bcf81cqsx8kj2im5yjbh7g9khgz2hqj0ix")))

(define-public crate-ip-server-0.1 (crate (name "ip-server") (vers "0.1.7") (deps (list (crate-dep (name "getopts") (req "^0.2.21") (default-features #t) (kind 0)))) (hash "0c10qigpyak4n2khnlljhxvvnqy5zxkrg4rbymwwfrmv5ssx3md7")))

(define-public crate-ip-server-0.1 (crate (name "ip-server") (vers "0.1.8") (deps (list (crate-dep (name "getopts") (req "^0.2.21") (default-features #t) (kind 0)))) (hash "17g5ypxlcp61ljsla5l7fld8p5dq8nhw35yvcnycqw7nip1lqycq")))

(define-public crate-ip-server-0.1 (crate (name "ip-server") (vers "0.1.10") (deps (list (crate-dep (name "getopts") (req "^0.2.21") (default-features #t) (kind 0)))) (hash "0lpp0va3wsshjj4yvq103bfnnakx6ik5r76kh7cc7pbp3bbcdnk5")))

(define-public crate-ip-server-0.1 (crate (name "ip-server") (vers "0.1.9") (deps (list (crate-dep (name "getopts") (req "^0.2.21") (default-features #t) (kind 0)))) (hash "1d3125hcxa0qfiip3h3jkjz1ymy72dg8h47bsvfk7lfb146apbjz")))

(define-public crate-ip-server-0.1 (crate (name "ip-server") (vers "0.1.11") (deps (list (crate-dep (name "getopts") (req "^0.2.21") (default-features #t) (kind 0)))) (hash "1v43kshirl7q59l3z49sry4kmpcf3b2paynkhc8vfj26iacs3kcw")))

(define-public crate-ip-server-0.1 (crate (name "ip-server") (vers "0.1.12") (deps (list (crate-dep (name "getopts") (req "^0.2.21") (default-features #t) (kind 0)))) (hash "09zjww51bvll53kvfb51nhvp32fzvpy11sja3ag0sfyyvq07a6yv")))

(define-public crate-ip-server-0.1 (crate (name "ip-server") (vers "0.1.13") (deps (list (crate-dep (name "getopts") (req "^0.2.21") (default-features #t) (kind 0)))) (hash "04hkqh1c3cznlznyvpgks7by6s5b06mzvaahfgq5g8vvwnlkh0az")))

(define-public crate-ip-spoofing-0.1 (crate (name "ip-spoofing") (vers "0.1.0") (deps (list (crate-dep (name "etherparse") (req "^0.13.0") (default-features #t) (kind 0)) (crate-dep (name "nix") (req "^0.26.1") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0") (default-features #t) (kind 0)))) (hash "073lycdvq6ajcq4icm0qfpsy4b8hq4yfhjvh24dfaxlpk37n5q8x")))

