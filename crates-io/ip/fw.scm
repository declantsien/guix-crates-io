(define-module (crates-io ip fw) #:use-module (crates-io))

(define-public crate-ipfw-rs-0.1 (crate (name "ipfw-rs") (vers "0.1.0") (deps (list (crate-dep (name "cdns-rs") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "mac_address") (req "^1.1.5") (default-features #t) (kind 0)) (crate-dep (name "nix") (req "^0.28.0") (features (quote ("socket"))) (default-features #t) (kind 0)))) (hash "1d633bnn73a8cxp0phlk4npxhhg89yknyzxdh9qn63r6rmfzswvk") (features (quote (("default"))))))

