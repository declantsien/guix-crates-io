(define-module (crates-io ip pa) #:use-module (crates-io))

(define-public crate-ippacket-0.1 (crate (name "ippacket") (vers "0.1.0") (deps (list (crate-dep (name "byteorder") (req "^1.0") (default-features #t) (kind 0)))) (hash "1816rdisfr1fkl5bnkia84phrrlqssgw960zh3rkj4whphamwbmh")))

