(define-module (crates-io ip zo) #:use-module (crates-io))

(define-public crate-ipzone-0.1 (crate (name "ipzone") (vers "0.1.0") (deps (list (crate-dep (name "serde_json") (req "^1.0.89") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.89") (default-features #t) (kind 2)))) (hash "1095jjp5xmdkmffikl6fpfwxfv3fx2gnwvd5qdgpla01i05hql12") (features (quote (("json" "serde_json") ("default"))))))

(define-public crate-ipzone-0.2 (crate (name "ipzone") (vers "0.2.0") (deps (list (crate-dep (name "once_cell") (req "^1.16.0") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.37") (default-features #t) (kind 0)))) (hash "1v1grvcqdrxjhrzccfz3fa09r8hjvcp14x8a2kzx0lkxdm5nij4i")))

(define-public crate-ipzone-0.3 (crate (name "ipzone") (vers "0.3.0") (deps (list (crate-dep (name "once_cell") (req "^1.16") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0") (default-features #t) (kind 0)))) (hash "178x0bsh10ik6d3sqrq3lffrc9i1pkhr4i3vk81z0irmhnv8f6km")))

(define-public crate-ipzone-0.4 (crate (name "ipzone") (vers "0.4.0") (deps (list (crate-dep (name "once_cell") (req "^1.16") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0") (default-features #t) (kind 0)))) (hash "0n0gx0ag4wr56i193ma32fixf10mwd97hw7awf4smdlxn05iv2n1")))

