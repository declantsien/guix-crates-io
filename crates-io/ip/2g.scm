(define-module (crates-io ip #{2g}#) #:use-module (crates-io))

(define-public crate-ip2geo-0.1 (crate (name "ip2geo") (vers "0.1.0") (deps (list (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "rmp-serde") (req "^1.1") (default-features #t) (kind 0)) (crate-dep (name "rust-embed") (req "^6.4.2") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "1g1chbx5rykg1firrfr3far1hy8dmiiqgrzlnwz5i9gz97r55bdn")))

