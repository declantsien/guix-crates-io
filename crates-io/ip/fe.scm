(define-module (crates-io ip fe) #:use-module (crates-io))

(define-public crate-ipfetch-1 (crate (name "ipfetch") (vers "1.0.0") (deps (list (crate-dep (name "clap") (req "^3.1.8") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.11") (features (quote ("blocking"))) (default-features #t) (kind 0)) (crate-dep (name "seeip") (req "^3.0.0") (default-features #t) (kind 0)) (crate-dep (name "tabled") (req "^0.6.0") (default-features #t) (kind 0)))) (hash "0yva0xh3dx6y236az017mwliji8zm1bj4svwdcij5ydwxw1fn38k")))

