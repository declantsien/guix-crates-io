(define-module (crates-io ip #{6g}#) #:use-module (crates-io))

(define-public crate-ip6gen-0.0.1 (crate (name "ip6gen") (vers "0.0.1") (deps (list (crate-dep (name "clap") (req "^2.13.0") (default-features #t) (kind 0)) (crate-dep (name "ipgen") (req "^0.0.1") (default-features #t) (kind 0)))) (hash "0d03fq8yn0lxr3irfwga38848q11mw6kv826r3qgw342gysz6cxp") (yanked #t)))

(define-public crate-ip6gen-0.0.2 (crate (name "ip6gen") (vers "0.0.2") (deps (list (crate-dep (name "clap") (req "^2.13.0") (default-features #t) (kind 0)) (crate-dep (name "ipgen") (req "^0.0.1") (default-features #t) (kind 0)))) (hash "06x3zc3h3frvcb2aan6dqb3dxd6cxw6462gcjdn5n3zz5kg4dmak") (yanked #t)))

