(define-module (crates-io ip #{2r}#) #:use-module (crates-io))

(define-public crate-ip2region-0.1 (crate (name "ip2region") (vers "0.1.0") (deps (list (crate-dep (name "bencher") (req "^0.1.5") (default-features #t) (kind 2)) (crate-dep (name "speedy") (req "^0.8.2") (default-features #t) (kind 0)))) (hash "1qhv34jr536d46baih1y7b96avmqbs5al72hdgrvh0han0xcczhp")))

