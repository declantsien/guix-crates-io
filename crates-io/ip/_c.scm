(define-module (crates-io ip _c) #:use-module (crates-io))

(define-public crate-ip_crate-0.1 (crate (name "ip_crate") (vers "0.1.0") (hash "0rw9r2xn2r6f49hjaldy3rzkirgxjm45sfc9xqnfklmgbzw2q1rf")))

(define-public crate-ip_crate-0.2 (crate (name "ip_crate") (vers "0.2.0") (hash "0gfn7bmzhhvjks8mmdvizqk9s9z5k6k5mn1sg3z9bjf6mcw2wpkj")))

(define-public crate-ip_crate-0.2 (crate (name "ip_crate") (vers "0.2.1") (hash "1250g2pw8vwzxw33dicdrcrps95ix4fvhic8lk82zhqbxg2321r1")))

(define-public crate-ip_crate-1 (crate (name "ip_crate") (vers "1.2.1") (hash "0z7r0l2aljwnk7z22l68fww9mx5mnw6l1jx659g0526byg38dfys")))

(define-public crate-ip_crate-1 (crate (name "ip_crate") (vers "1.2.2") (hash "03sfl3d0awi568gmfylzwrgm04idfnmfy261sc1ka3l6czjng4x0")))

