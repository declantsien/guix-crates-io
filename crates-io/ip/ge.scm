(define-module (crates-io ip ge) #:use-module (crates-io))

(define-public crate-ipgen-0.0.1 (crate (name "ipgen") (vers "0.0.1") (deps (list (crate-dep (name "ipnetwork") (req "^0.10.0") (default-features #t) (kind 0)) (crate-dep (name "rust-crypto") (req "^0.2.36") (default-features #t) (kind 0)))) (hash "1yaxx0c9dniyzs3yxz492057zjd7mx9ldjjlxskn5i8i4aw26asa")))

(define-public crate-ipgen-0.0.2 (crate (name "ipgen") (vers "0.0.2") (deps (list (crate-dep (name "ipnetwork") (req "^0.10.0") (default-features #t) (kind 0)) (crate-dep (name "rust-crypto") (req "^0.2.36") (default-features #t) (kind 0)))) (hash "1c4kxyrv94swb2c7vnab05an3m0cfkxc48cwya9bydpz7rw2ci1i")))

(define-public crate-ipgen-0.0.3 (crate (name "ipgen") (vers "0.0.3") (deps (list (crate-dep (name "ipnetwork") (req "^0.10.0") (default-features #t) (kind 0)) (crate-dep (name "rust-crypto") (req "^0.2.36") (default-features #t) (kind 0)))) (hash "060s34j9rnmjnlhz4sfvybif6z5693hmn69zb2xi0rg8hgvw5iby")))

(define-public crate-ipgen-0.0.4 (crate (name "ipgen") (vers "0.0.4") (deps (list (crate-dep (name "ipnetwork") (req "^0.10.0") (default-features #t) (kind 0)) (crate-dep (name "rust-crypto") (req "^0.2.36") (default-features #t) (kind 0)))) (hash "05r0rh0ypyiw3h59w5qfdk1k1qag2h9hxv2pc59hbqqiqpxlvdzn")))

(define-public crate-ipgen-0.0.5 (crate (name "ipgen") (vers "0.0.5") (deps (list (crate-dep (name "blake2") (req "^0.9.1") (kind 0)) (crate-dep (name "ipnetwork") (req "^0.17.0") (default-features #t) (kind 0)))) (hash "0qq81p9a80v3ibmzqha8zrqp4yqp7b1z1amy2li2lgs0x4h4682r")))

(define-public crate-ipgen-1 (crate (name "ipgen") (vers "1.0.0") (deps (list (crate-dep (name "blake2") (req "^0.9.1") (kind 0)) (crate-dep (name "ipnetwork") (req "^0.17.0") (kind 0)))) (hash "0aw769c3mkl9bmsz1mxap073n09a8zwxl25rqgjzwh4y9fal0kpd")))

(define-public crate-ipgen-1 (crate (name "ipgen") (vers "1.0.1") (deps (list (crate-dep (name "blake2") (req "^0.9.1") (kind 0)) (crate-dep (name "ipnetwork") (req "^0.17.0") (kind 0)))) (hash "18vk83pnfzy1qyxb9mlj5hyv03pxh2sm8fi5pvqsfg5q2kyzc4rq")))

(define-public crate-ipgen-1 (crate (name "ipgen") (vers "1.0.2") (deps (list (crate-dep (name "blake2") (req "^0.10.4") (kind 0)) (crate-dep (name "ipnetwork") (req "^0.20.0") (kind 0)))) (hash "1lw7y4p9zpfc0lc016w9f0v71dxrigx9n4c7m05i01vk5l7gkvrg")))

(define-public crate-ipgen-cli-0.0.1 (crate (name "ipgen-cli") (vers "0.0.1") (deps (list (crate-dep (name "clap") (req "^2.13.0") (default-features #t) (kind 0)) (crate-dep (name "ipgen") (req "^0.0.2") (default-features #t) (kind 0)))) (hash "1fb6kicwi5hccix18j7ai8yz024s81cmi0fm58h8j377v8fgvjq1")))

(define-public crate-ipgen-cli-0.0.2 (crate (name "ipgen-cli") (vers "0.0.2") (deps (list (crate-dep (name "clap") (req "^2.13.0") (default-features #t) (kind 0)) (crate-dep (name "ipgen") (req "^0.0.4") (default-features #t) (kind 0)))) (hash "0ni4kypvdaj27ahr3wmgc5rzs2a738asr1m4isjmlgiq32ghkwgi")))

(define-public crate-ipgen-cli-0.0.3 (crate (name "ipgen-cli") (vers "0.0.3") (deps (list (crate-dep (name "clap") (req "^2.33.3") (default-features #t) (kind 0)) (crate-dep (name "ipgen") (req "^0.0.5") (default-features #t) (kind 0)))) (hash "1bap7j6aag44s58kjnz61mxam7gnqzjdnrx8ks5bcp7l35l2w06d")))

(define-public crate-ipgen-cli-1 (crate (name "ipgen-cli") (vers "1.0.0") (deps (list (crate-dep (name "ipgen") (req "^1.0.1") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.3.21") (default-features #t) (kind 0)))) (hash "09vzsq3n1r7x0a9syp4jykah1z3hn4a3sla702mvxkb7mx6in3lr")))

(define-public crate-ipgen-cli-1 (crate (name "ipgen-cli") (vers "1.0.1") (deps (list (crate-dep (name "ipgen") (req "^1.0.1") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.3.21") (default-features #t) (kind 0)))) (hash "0h152slfk7f9mpn4l057z886v12yq9j6gh859r9jply5dg7vz6b7")))

(define-public crate-ipgen-cli-1 (crate (name "ipgen-cli") (vers "1.0.2") (deps (list (crate-dep (name "ipgen") (req "^1.0.1") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.3.21") (default-features #t) (kind 0)))) (hash "1n37rwclwinzb3p9dmlfx23wxqq0y51csnw22dba942bkcs9ijjs")))

(define-public crate-ipgen-cli-1 (crate (name "ipgen-cli") (vers "1.0.3") (deps (list (crate-dep (name "ipgen") (req "^1.0.2") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.3.26") (default-features #t) (kind 0)))) (hash "0a202g0bnpkkcqbifwynddm39xr49xjdrz6pwixzmb0ac4plw68r")))

(define-public crate-ipgeo-0.1 (crate (name "ipgeo") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^2.33.3") (default-features #t) (kind 0)) (crate-dep (name "ipgeolocate") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "ureq") (req "^1.5.1") (default-features #t) (kind 0)))) (hash "0s8h124j5m0rfmxp77694vxgipir5g68mv6n9gx01kgw165i93c6")))

(define-public crate-ipgeo-0.1 (crate (name "ipgeo") (vers "0.1.1") (deps (list (crate-dep (name "clap") (req "^2.33.3") (default-features #t) (kind 0)) (crate-dep (name "dns-lookup") (req "^1.0.5") (default-features #t) (kind 0)) (crate-dep (name "ipgeolocate") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "ureq") (req "^1.5.1") (default-features #t) (kind 0)))) (hash "1a87kfv90fjs41x7kyhrl5vjhscs53174fg6h8zs4dx9y1ps818n")))

(define-public crate-ipgeo-0.1 (crate (name "ipgeo") (vers "0.1.2") (deps (list (crate-dep (name "clap") (req "^2.33.3") (default-features #t) (kind 0)) (crate-dep (name "dns-lookup") (req "^1.0.5") (default-features #t) (kind 0)) (crate-dep (name "ipgeolocate") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "ureq") (req "^1.5.1") (default-features #t) (kind 0)))) (hash "0hf5y4m8ykmbikmma82pgxl1izfning5b3r0qmn7xprza0iz9nmm")))

(define-public crate-ipgeo-0.1 (crate (name "ipgeo") (vers "0.1.3") (deps (list (crate-dep (name "clap") (req "^2.33.3") (default-features #t) (kind 0)) (crate-dep (name "dns-lookup") (req "^1.0.5") (default-features #t) (kind 0)) (crate-dep (name "ipgeolocate") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "ureq") (req "^1.5.1") (default-features #t) (kind 0)))) (hash "14v68csr037xyhrb9anbiijwq7sbqhgxqf9jmnbyvfd0w8ida25m")))

(define-public crate-ipgeo-0.1 (crate (name "ipgeo") (vers "0.1.4") (deps (list (crate-dep (name "clap") (req "^2.33.3") (default-features #t) (kind 0)) (crate-dep (name "colored") (req "^2") (default-features #t) (kind 0)) (crate-dep (name "dns-lookup") (req "^1.0.5") (default-features #t) (kind 0)) (crate-dep (name "ipgeolocate") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "ureq") (req "^1.5.1") (default-features #t) (kind 0)))) (hash "05b5dcabpswxxfcdrsv0rh2hd70w9gbq4gyxq126qpvyi812w3xs")))

(define-public crate-ipgeo-0.1 (crate (name "ipgeo") (vers "0.1.6") (deps (list (crate-dep (name "clap") (req "^2.33.3") (default-features #t) (kind 0)) (crate-dep (name "colored") (req "^2") (default-features #t) (kind 0)) (crate-dep (name "dns-lookup") (req "^1.0.5") (default-features #t) (kind 0)) (crate-dep (name "ipgeolocate") (req "^0.3.4") (default-features #t) (kind 0)) (crate-dep (name "ureq") (req "^1.5.1") (default-features #t) (kind 0)))) (hash "0kqxa28kpczlyamh63yjaybwmmdk4s44hiwc2mf032wjldlmckh9")))

(define-public crate-ipgeo-0.1 (crate (name "ipgeo") (vers "0.1.7") (deps (list (crate-dep (name "async-std") (req "^1.9.0") (features (quote ("attributes"))) (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^2.33.3") (default-features #t) (kind 0)) (crate-dep (name "colored") (req "^2") (default-features #t) (kind 0)) (crate-dep (name "dns-lookup") (req "^1.0.5") (default-features #t) (kind 0)) (crate-dep (name "ipgeolocate") (req "^0.3.5") (default-features #t) (kind 0)) (crate-dep (name "ureq") (req "^2.0.2") (default-features #t) (kind 0)))) (hash "1ph4m8q84w4w6zwcxyb2zvyz5fsc0fv4y218w6wq7xxxkc4x0rnk")))

(define-public crate-ipgeo-0.1 (crate (name "ipgeo") (vers "0.1.8") (deps (list (crate-dep (name "async-std") (req "^1.9.0") (features (quote ("attributes"))) (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^2.33.3") (default-features #t) (kind 0)) (crate-dep (name "colored") (req "^2") (default-features #t) (kind 0)) (crate-dep (name "dns-lookup") (req "^1.0.5") (default-features #t) (kind 0)) (crate-dep (name "ipgeolocate") (req "^0.3.5") (default-features #t) (kind 0)) (crate-dep (name "surf") (req "^2.2.0") (default-features #t) (kind 0)))) (hash "1fjm55lihg74mq5mrblw3q2n44ds6sjlhyxfwsbg7z8gwi5s02as")))

(define-public crate-ipgeolocate-0.2 (crate (name "ipgeolocate") (vers "0.2.2") (deps (list (crate-dep (name "serde_json") (req "^1.0.59") (default-features #t) (kind 0)) (crate-dep (name "ureq") (req "^1.5.1") (default-features #t) (kind 0)))) (hash "145anxxb4w3x6by1yxd8l4d2qgvpiqmb276n4vwc9p6xq01x3sk6")))

(define-public crate-ipgeolocate-0.2 (crate (name "ipgeolocate") (vers "0.2.3") (deps (list (crate-dep (name "serde_json") (req ">=1.0.59, <2.0.0") (default-features #t) (kind 0)) (crate-dep (name "ureq") (req ">=1.5.1, <2.0.0") (default-features #t) (kind 0)))) (hash "1565qpw207vgzynh8vv6ig3lankxmihkbfjfdys2q5ww6yg64cmp")))

(define-public crate-ipgeolocate-0.2 (crate (name "ipgeolocate") (vers "0.2.4") (deps (list (crate-dep (name "serde_json") (req ">=1.0.59, <2.0.0") (default-features #t) (kind 0)) (crate-dep (name "ureq") (req ">=1.5.1, <2.0.0") (default-features #t) (kind 0)))) (hash "1sqmy6kb6dzzib3qxnl8hf34vyqgqr6phrx3wgsinqqa9h3wxvvr")))

(define-public crate-ipgeolocate-0.2 (crate (name "ipgeolocate") (vers "0.2.5") (deps (list (crate-dep (name "serde_json") (req ">=1.0.59, <2.0.0") (default-features #t) (kind 0)) (crate-dep (name "ureq") (req ">=1.5.1, <2.0.0") (default-features #t) (kind 0)))) (hash "1prhqv1nds5h6q7hivdznz251vs941qd1xqa3z5ffn137n7jlari")))

(define-public crate-ipgeolocate-0.2 (crate (name "ipgeolocate") (vers "0.2.6") (deps (list (crate-dep (name "serde_json") (req ">=1.0.59, <2.0.0") (default-features #t) (kind 0)) (crate-dep (name "ureq") (req ">=1.5.1, <2.0.0") (default-features #t) (kind 0)))) (hash "01902lw4z6f2a5c01hpxp414s0zxkalqkcnr5hgl22ach9sdvcfl")))

(define-public crate-ipgeolocate-0.2 (crate (name "ipgeolocate") (vers "0.2.7") (deps (list (crate-dep (name "serde_json") (req "^1.0.59") (default-features #t) (kind 0)) (crate-dep (name "ureq") (req "^1.5.1") (default-features #t) (kind 0)))) (hash "10k57zs5myqm7s20i2620p8czpv2kdib0kz67rwhpka3x88qxb9x")))

(define-public crate-ipgeolocate-0.2 (crate (name "ipgeolocate") (vers "0.2.8") (deps (list (crate-dep (name "serde_json") (req "^1.0.59") (default-features #t) (kind 0)) (crate-dep (name "ureq") (req "^1.5.1") (default-features #t) (kind 0)))) (hash "1pygwbz5s6xn9gd494lqj6niigr99dlnfd73a6jfwvgjr28msphi")))

(define-public crate-ipgeolocate-0.2 (crate (name "ipgeolocate") (vers "0.2.9") (deps (list (crate-dep (name "serde_json") (req "^1.0.59") (default-features #t) (kind 0)) (crate-dep (name "ureq") (req "^1.5.1") (default-features #t) (kind 0)))) (hash "08llnk1dh5qqi5ywymmxvxn1xi7mjy0051gsrm52jd8prk8q13il")))

(define-public crate-ipgeolocate-0.3 (crate (name "ipgeolocate") (vers "0.3.0") (deps (list (crate-dep (name "clap") (req "^2.33.3") (default-features #t) (kind 2)) (crate-dep (name "serde_json") (req "^1.0.59") (default-features #t) (kind 0)) (crate-dep (name "ureq") (req "^1.5.1") (default-features #t) (kind 0)))) (hash "046hqy6wa705pj435z7s5vpi4k023jkkjlcvp0r4qdj96rk6w1nd")))

(define-public crate-ipgeolocate-0.3 (crate (name "ipgeolocate") (vers "0.3.1") (deps (list (crate-dep (name "serde_json") (req "^1.0.59") (default-features #t) (kind 0)) (crate-dep (name "ureq") (req "^1.5.1") (default-features #t) (kind 0)))) (hash "0bw4m1nmlw967yfvq4jpc9jb2l4lid71crknjncv9mx3cqppzrfd")))

(define-public crate-ipgeolocate-0.3 (crate (name "ipgeolocate") (vers "0.3.2") (deps (list (crate-dep (name "serde_json") (req "^1.0.59") (default-features #t) (kind 0)) (crate-dep (name "ureq") (req "^1.5.1") (default-features #t) (kind 0)))) (hash "0crws940lpqb2vm75zyad25spwg3pqdii5gly9pcpah6jvgp2gw7")))

(define-public crate-ipgeolocate-0.3 (crate (name "ipgeolocate") (vers "0.3.3") (deps (list (crate-dep (name "serde_json") (req "^1.0.59") (default-features #t) (kind 0)) (crate-dep (name "ureq") (req "^1.5.1") (default-features #t) (kind 0)))) (hash "1s1dqbdwcavpx4h9lxdgmkga9v0xhsnz5sybrm0agb73i0y9rbmi")))

(define-public crate-ipgeolocate-0.3 (crate (name "ipgeolocate") (vers "0.3.4") (deps (list (crate-dep (name "serde_json") (req "^1.0.59") (default-features #t) (kind 0)) (crate-dep (name "ureq") (req "^1.5.1") (default-features #t) (kind 0)))) (hash "0vmgmz8vnnmqgd6yid544bx2xm24g1xacsaq0zcnqpyw03h12qxd")))

(define-public crate-ipgeolocate-0.3 (crate (name "ipgeolocate") (vers "0.3.5") (deps (list (crate-dep (name "futures") (req "^0.3.13") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.64") (default-features #t) (kind 0)) (crate-dep (name "surf") (req "^2.2.0") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.4.0") (features (quote ("full"))) (default-features #t) (kind 2)))) (hash "0hpzqmmzr8zrl5flx8ckggzbg0nfslysqj4bhm7c9pcspsva6gyd")))

(define-public crate-ipgeolocate-0.3 (crate (name "ipgeolocate") (vers "0.3.6") (deps (list (crate-dep (name "futures") (req "^0.3.26") (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.11") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.91") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.25.0") (features (quote ("full"))) (default-features #t) (kind 2)) (crate-dep (name "tracing") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "tracing-subscriber") (req "^0.3") (default-features #t) (kind 0)))) (hash "1k6nlrnrwn71m98hs7dv23wifz0pql0i3ragmz16amq922kh5vq3")))

