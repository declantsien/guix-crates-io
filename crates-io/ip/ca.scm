(define-module (crates-io ip ca) #:use-module (crates-io))

(define-public crate-ipcap-0.0.0 (crate (name "ipcap") (vers "0.0.0") (hash "1yzwkr0a011iynm14ljkrffwmybkhc9s948f3krn18zminj19y05")))

(define-public crate-ipcap-0.0.1 (crate (name "ipcap") (vers "0.0.1") (deps (list (crate-dep (name "once_cell") (req "^1.19.0") (default-features #t) (kind 0)))) (hash "1jhkj93pic7gypla094n138yqv49ch147sbchrjm0iqrxc6mrh4y")))

(define-public crate-ipcap-0.0.2 (crate (name "ipcap") (vers "0.0.2") (deps (list (crate-dep (name "once_cell") (req "^1.19.0") (default-features #t) (kind 0)))) (hash "13klkz28szmqwv1l1fv2z4xc2mwzrgljb0sb4wwn0kyqxwq0gc55")))

(define-public crate-ipcap-0.1 (crate (name "ipcap") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^4.4.12") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "dirs") (req "^5.0.1") (default-features #t) (kind 0)) (crate-dep (name "once_cell") (req "^1.19.0") (default-features #t) (kind 0)))) (hash "1a9sahlb7wh8k709ywjs91jmz1pds3mx7zy2jnvncqikbi5vqnad")))

(define-public crate-ipcap-0.1 (crate (name "ipcap") (vers "0.1.1") (deps (list (crate-dep (name "clap") (req "^4.4.12") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "dirs") (req "^5.0.1") (default-features #t) (kind 0)) (crate-dep (name "once_cell") (req "^1.19.0") (default-features #t) (kind 0)))) (hash "0nvyif58l15s5vb398z4nv2l6qiwxf85mfpzc4q0l6i6yzr5l09c") (features (quote (("cli" "clap"))))))

(define-public crate-ipcap-0.1 (crate (name "ipcap") (vers "0.1.2") (deps (list (crate-dep (name "clap") (req "^4.4.12") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "dirs") (req "^5.0.1") (default-features #t) (kind 0)) (crate-dep (name "once_cell") (req "^1.19.0") (default-features #t) (kind 0)))) (hash "1mlr9dc03f8hyval3lk4487brwvcb5hw8idm9xqrm4y6sm6gh70m") (features (quote (("cli" "clap"))))))

(define-public crate-ipcap-0.1 (crate (name "ipcap") (vers "0.1.3") (deps (list (crate-dep (name "clap") (req "^4.4.12") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "dirs") (req "^5.0.1") (default-features #t) (kind 0)) (crate-dep (name "once_cell") (req "^1.19.0") (default-features #t) (kind 0)))) (hash "151v03mhffqhf8y3hik45b0nfxvkkddljp7lm0kvvsqr0jdpjzdw") (features (quote (("cli" "clap"))))))

(define-public crate-ipcap-0.1 (crate (name "ipcap") (vers "0.1.4") (deps (list (crate-dep (name "clap") (req "^4.4.12") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "dirs") (req "^5.0.1") (default-features #t) (kind 0)) (crate-dep (name "once_cell") (req "^1.19.0") (default-features #t) (kind 0)))) (hash "1psfzffhywxnmpb24ph7d2lazzld07j96shg090inw6zarf0szcz") (features (quote (("cli" "clap"))))))

(define-public crate-ipcap-0.1 (crate (name "ipcap") (vers "0.1.5") (deps (list (crate-dep (name "clap") (req "^4.4.12") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "dirs") (req "^5.0.1") (default-features #t) (kind 0)) (crate-dep (name "ipcap-codegen") (req "^0.1.0") (default-features #t) (kind 1)) (crate-dep (name "once_cell") (req "^1.19.0") (default-features #t) (kind 0)))) (hash "0rq8540z3s8biilm872lcl3z10ilw66xcddaxyggskwkjj95m4v8") (features (quote (("cli" "clap"))))))

(define-public crate-ipcap-0.1 (crate (name "ipcap") (vers "0.1.6") (deps (list (crate-dep (name "clap") (req "^4.4.12") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "dirs") (req "^5.0.1") (default-features #t) (kind 0)) (crate-dep (name "ipcap-codegen") (req "^0.1.0") (default-features #t) (kind 1)) (crate-dep (name "once_cell") (req "^1.19.0") (default-features #t) (kind 0)))) (hash "0h89683jkkcy5mj78jk4afp1slzj9q8qy3cp92lkhh8m4jncxifw") (features (quote (("cli" "clap"))))))

(define-public crate-ipcap-0.1 (crate (name "ipcap") (vers "0.1.7") (deps (list (crate-dep (name "bump2version") (req "^0.1.3") (default-features #t) (kind 2)) (crate-dep (name "clap") (req "^4.4.12") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "dirs") (req "^5.0.1") (default-features #t) (kind 0)) (crate-dep (name "ipcap-codegen") (req "^0.1.0") (default-features #t) (kind 1)) (crate-dep (name "once_cell") (req "^1.19.0") (default-features #t) (kind 0)))) (hash "1800iawdrwhi2gp051lhnf5ayzcs2kg5r5a8sqxwp54q0r5ml40v") (features (quote (("cli" "clap"))))))

(define-public crate-ipcap-codegen-0.1 (crate (name "ipcap-codegen") (vers "0.1.0") (hash "0agbj5bbz7q9hz7yibnm5b9ywzidhcdl30nbrgpp4xck3mafv17n")))

