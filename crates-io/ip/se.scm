(define-module (crates-io ip se) #:use-module (crates-io))

(define-public crate-ipsec-parser-0.1 (crate (name "ipsec-parser") (vers "0.1.0") (deps (list (crate-dep (name "enum_primitive") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "nom") (req "^3") (default-features #t) (kind 0)) (crate-dep (name "rusticata-macros") (req "^0.4.0") (default-features #t) (kind 0)))) (hash "095fv1fz71vnx8w7i2c4wb5za8scgydxpx7r39jxy44r6s1h9xss")))

(define-public crate-ipsec-parser-0.2 (crate (name "ipsec-parser") (vers "0.2.0") (deps (list (crate-dep (name "nom") (req "^3") (default-features #t) (kind 0)) (crate-dep (name "rusticata-macros") (req "^0.4.0") (default-features #t) (kind 0)))) (hash "1lcn8fj2idmxwdqyrzk330w7z0nh4w70lam8bv3kq30zpn8h0kcz")))

(define-public crate-ipsec-parser-0.3 (crate (name "ipsec-parser") (vers "0.3.0") (deps (list (crate-dep (name "nom") (req "^3") (default-features #t) (kind 0)) (crate-dep (name "rusticata-macros") (req "^0.4.0") (default-features #t) (kind 0)))) (hash "0x81qaw6jda7kzq9z06ri1d4dmqg9lss9dxxy1qwqa3ndlx1hq35")))

(define-public crate-ipsec-parser-0.4 (crate (name "ipsec-parser") (vers "0.4.0") (deps (list (crate-dep (name "nom") (req "^4.0") (default-features #t) (kind 0)) (crate-dep (name "rusticata-macros") (req "^1.0") (default-features #t) (kind 0)))) (hash "072594gmwdd3sgnhm2s2l5h850a4qi42vmak0459cm909y7n34ak")))

(define-public crate-ipsec-parser-0.4 (crate (name "ipsec-parser") (vers "0.4.1") (deps (list (crate-dep (name "nom") (req "^4.0") (default-features #t) (kind 0)) (crate-dep (name "rusticata-macros") (req "^1.0") (default-features #t) (kind 0)))) (hash "0dyv2qffpl6gsgkhmgdfc85nxwng7s12y64hpbgzgrcp4k51s5m1")))

(define-public crate-ipsec-parser-0.5 (crate (name "ipsec-parser") (vers "0.5.0") (deps (list (crate-dep (name "nom") (req "^5.0") (default-features #t) (kind 0)) (crate-dep (name "rusticata-macros") (req "^2.0") (default-features #t) (kind 0)))) (hash "02p99y7bjl71v1pkccnm62zqqhivyk4b0wl72z4sy9ykn62rg82g")))

(define-public crate-ipsec-parser-0.6 (crate (name "ipsec-parser") (vers "0.6.0") (deps (list (crate-dep (name "nom") (req "^6.0") (default-features #t) (kind 0)) (crate-dep (name "rusticata-macros") (req "^3.0") (default-features #t) (kind 0)))) (hash "1fpd0mvyd08ygf93pgdgnfq64ba6pwg6032amwljc5785vgrv3ka")))

(define-public crate-ipsec-parser-0.7 (crate (name "ipsec-parser") (vers "0.7.0") (deps (list (crate-dep (name "nom") (req "^7.0") (default-features #t) (kind 0)) (crate-dep (name "rusticata-macros") (req "^4.0") (default-features #t) (kind 0)))) (hash "1lfxjk27sg3xhx5qpyvfpsd721a1nvs73zw0332wp2z7blz43y1c")))

(define-public crate-ipset-0.1 (crate (name "ipset") (vers "0.1.0") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "0bjaf4rf24m7jqf442khsszyyhmbkjvgyr8yrxqrn9i4jly6lhan")))

(define-public crate-ipset-0.1 (crate (name "ipset") (vers "0.1.1") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "13nv2fd59m0k46jprh2gb5g8hy9s1syfh5a5incph5lqy79jziip")))

(define-public crate-ipset-0.1 (crate (name "ipset") (vers "0.1.2") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "00q5gr7bmf0nzz81krbqiig5bln8gnisdnaqx9vydgmkp74gryvj")))

(define-public crate-ipset-0.2 (crate (name "ipset") (vers "0.2.0") (deps (list (crate-dep (name "derive_more") (req "^0.99") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "0s7wdwf9hicysvyq317dwiw28v57sd4yk396fm5qrysz7zf3b7kg")))

(define-public crate-ipset-0.2 (crate (name "ipset") (vers "0.2.1") (deps (list (crate-dep (name "derive_more") (req "^0.99") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "1z5rjp087b3snbjk0f58n6bmml8icmkp56g321kn8ppcrkr2nr4v")))

(define-public crate-ipset-0.2 (crate (name "ipset") (vers "0.2.2") (deps (list (crate-dep (name "derive_more") (req "^0.99") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "0h612y5pk9lfj8zfd2mhcq6xfn2aa7hr3k9l25qf79qa5s5yvzv4")))

(define-public crate-ipset-0.2 (crate (name "ipset") (vers "0.2.3") (deps (list (crate-dep (name "derive_more") (req "^0.99") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "0hj6319d67046pf18sgj7nx10cdf52p2gj7bv3v4fnbicnvmhpqm")))

(define-public crate-ipset-0.2 (crate (name "ipset") (vers "0.2.4") (deps (list (crate-dep (name "derive_more") (req "^0.99") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "1bkvdsfh6abqnai3ckzl1bsv450y08ryw3ixvkscl63rnhaz4bsr")))

(define-public crate-ipset-0.3 (crate (name "ipset") (vers "0.3.0") (deps (list (crate-dep (name "derive_more") (req "^0.99") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "1z06zwqlwc0rwhknxq13ni3aw3skk66y2i34k0zhhrxsa7q8zkw2")))

(define-public crate-ipset-0.4 (crate (name "ipset") (vers "0.4.0") (deps (list (crate-dep (name "derive_more") (req "^0.99") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "1xdddrxmfm9jvw2027vd5pasf66l666rsl31w3z86pflk68rvvz1")))

(define-public crate-ipset-0.5 (crate (name "ipset") (vers "0.5.0") (deps (list (crate-dep (name "cc") (req "^1.0") (default-features #t) (kind 1)) (crate-dep (name "derive_more") (req "^0.99") (default-features #t) (kind 0)) (crate-dep (name "ipset_derive") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "13jhqnrc0wph6rrdcif5pz57w2d3a81hbmh54wdsdyjinvpl420r") (links "ipset")))

(define-public crate-ipset-0.6 (crate (name "ipset") (vers "0.6.0") (deps (list (crate-dep (name "cc") (req "^1.0") (default-features #t) (kind 1)) (crate-dep (name "derive_more") (req "^0.99") (default-features #t) (kind 0)) (crate-dep (name "ipset_derive") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "037hv1m559bxn0p19c7a7ldn23pfn6sk8igrpafjppjb5cq54ykm") (links "ipset")))

(define-public crate-ipset-0.7 (crate (name "ipset") (vers "0.7.1") (deps (list (crate-dep (name "bindgen") (req "^0.69") (default-features #t) (kind 1)) (crate-dep (name "cc") (req "^1.0") (default-features #t) (kind 1)) (crate-dep (name "derive_more") (req "^0.99") (default-features #t) (kind 0)) (crate-dep (name "ipset_derive") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "0mwy0qa0pjfr006a37r4wmdm7mj2q78cpamxng6yjp5slvfmqpmm") (links "ipset")))

(define-public crate-ipset-sys-0.1 (crate (name "ipset-sys") (vers "0.1.0") (hash "0s007l5b4cq8fvmp3gjgmfs7q1ca8kvi2lf7a0c2ljmllmvmq9n7")))

(define-public crate-ipset-sys-0.1 (crate (name "ipset-sys") (vers "0.1.1") (deps (list (crate-dep (name "anyhow") (req "^1.0.44") (default-features #t) (kind 0)) (crate-dep (name "bindgen") (req "^0.53.1") (default-features #t) (kind 1)))) (hash "1l2zijgd9717qkipz1vd4kjbj8ymq7lkrbjslv3hwinh3z2cnyii")))

(define-public crate-ipset-sys-0.1 (crate (name "ipset-sys") (vers "0.1.2") (deps (list (crate-dep (name "anyhow") (req "^1.0.44") (default-features #t) (kind 0)) (crate-dep (name "bindgen") (req "^0.53.1") (default-features #t) (kind 1)))) (hash "050bqbp0mpgxhdm17p90sinllxsmamklv9qb3m5kq6p8cfrldwlr")))

(define-public crate-ipset-sys-0.1 (crate (name "ipset-sys") (vers "0.1.4") (deps (list (crate-dep (name "bindgen") (req "^0.53.1") (default-features #t) (kind 1)) (crate-dep (name "thiserror") (req "^1.0") (default-features #t) (kind 0)))) (hash "14sfqxzz9ljkyl6ggabv9njzmnssy2bcmb3bzpymg113bxw5fypj")))

(define-public crate-ipset-sys-0.1 (crate (name "ipset-sys") (vers "0.1.5") (deps (list (crate-dep (name "bindgen") (req "^0.53.1") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0") (default-features #t) (kind 0)))) (hash "1r8p5cznqy24kn6540xhxzk69n2bmxdljknjcp0q77gmv6f54bx3")))

(define-public crate-ipset_derive-0.1 (crate (name "ipset_derive") (vers "0.1.0") (deps (list (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2.0") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "1hrj595yy255xmkglflrvplpb0vj09bg6ij5js1z0cam254bxlff")))

(define-public crate-ipset_derive-0.1 (crate (name "ipset_derive") (vers "0.1.1") (deps (list (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2.0") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "1q01lsi839lry938l7xn5l52jbchn8f87wrxx4wxzfkrx1j6vny4")))

(define-public crate-ipset_lookup-0.3 (crate (name "ipset_lookup") (vers "0.3.0") (deps (list (crate-dep (name "clap") (req "^2.0.0") (default-features #t) (kind 0)) (crate-dep (name "git2") (req "^0.10.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "glob") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "ipnetwork") (req "^0.16.0") (default-features #t) (kind 0)) (crate-dep (name "rayon") (req "^1.3.0") (default-features #t) (kind 0)))) (hash "0wqdzj8xa5s8mdd40d5hwnr3ajzqwck54x6l4cfrh36y992f7ygd") (features (quote (("update" "git2") ("default" "bench") ("bench"))))))

(define-public crate-ipset_lookup-0.3 (crate (name "ipset_lookup") (vers "0.3.1") (deps (list (crate-dep (name "clap") (req "^2.0.0") (default-features #t) (kind 0)) (crate-dep (name "git2") (req "^0.10.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "glob") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "ipnetwork") (req "^0.16.0") (default-features #t) (kind 0)) (crate-dep (name "rayon") (req "^1.3.0") (default-features #t) (kind 0)))) (hash "10zf00qxdyyx2fwx31lz9g1bnqypsnal2fgkfbcmiqpyc5qqwq5x") (features (quote (("update" "git2") ("default" "bench") ("bench"))))))

(define-public crate-ipset_lookup-0.3 (crate (name "ipset_lookup") (vers "0.3.2") (deps (list (crate-dep (name "clap") (req "^2.0.0") (default-features #t) (kind 0)) (crate-dep (name "git2") (req "^0.10.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "glob") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "ipnetwork") (req "^0.16.0") (default-features #t) (kind 0)) (crate-dep (name "rayon") (req "^1.3.0") (default-features #t) (kind 0)))) (hash "1dlnxp5q7syw5c4y31wq1kx0m91jy5m7fcf506n9vp3g95lkgx1q") (features (quote (("update" "git2") ("default" "bench") ("bench"))))))

(define-public crate-ipset_lookup-0.3 (crate (name "ipset_lookup") (vers "0.3.3") (deps (list (crate-dep (name "clap") (req "^2.0.0") (default-features #t) (kind 0)) (crate-dep (name "git2") (req "^0.10.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "glob") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "ipnetwork") (req "^0.16.0") (default-features #t) (kind 0)) (crate-dep (name "rayon") (req "^1.3.0") (default-features #t) (kind 0)))) (hash "11rkaxxf51k614srsm6kmblwa2i9adrcniw7j8m466kjmwndqg7p") (features (quote (("update" "git2") ("default" "bench") ("bench"))))))

(define-public crate-ipset_lookup-0.4 (crate (name "ipset_lookup") (vers "0.4.1") (deps (list (crate-dep (name "clap") (req "^2.0.0") (default-features #t) (kind 0)) (crate-dep (name "failure") (req "^0.1.6") (default-features #t) (kind 0)) (crate-dep (name "git2") (req "^0.10.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "glob") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "ipnetwork") (req "^0.16.0") (default-features #t) (kind 0)) (crate-dep (name "rayon") (req "^1.3.0") (default-features #t) (kind 0)))) (hash "1i6lpimmck9f90igqk8vplnrsjsl7wymd7gad2nz6l5w7iasc17v") (features (quote (("update" "git2") ("default" "bench") ("bench")))) (yanked #t)))

(define-public crate-ipset_lookup-0.4 (crate (name "ipset_lookup") (vers "0.4.2") (deps (list (crate-dep (name "clap") (req "^2.0.0") (default-features #t) (kind 0)) (crate-dep (name "failure") (req "^0.1.6") (default-features #t) (kind 0)) (crate-dep (name "git2") (req "^0.10.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "glob") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "ipnetwork") (req "^0.16.0") (default-features #t) (kind 0)) (crate-dep (name "rayon") (req "^1.3.0") (default-features #t) (kind 0)))) (hash "11j0ngz98g7yxxjy0zlskrblja338ndf6r7mil2xfzrzq4pzdsjc") (features (quote (("update" "git2") ("default" "bench") ("bench"))))))

(define-public crate-ipset_lookup-0.4 (crate (name "ipset_lookup") (vers "0.4.3") (deps (list (crate-dep (name "clap") (req "^2.0.0") (default-features #t) (kind 0)) (crate-dep (name "failure") (req "^0.1.6") (default-features #t) (kind 0)) (crate-dep (name "git2") (req "^0.10.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "glob") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "ipnetwork") (req "^0.16.0") (default-features #t) (kind 0)) (crate-dep (name "rayon") (req "^1.3.0") (default-features #t) (kind 0)))) (hash "0acfawzgwg70k1459lq3pjwln893v79hxnbxnc23x1cczxkxbfi5") (features (quote (("update" "git2") ("default" "bench") ("bench"))))))

(define-public crate-ipset_lookup-0.4 (crate (name "ipset_lookup") (vers "0.4.4") (deps (list (crate-dep (name "clap") (req "^2.0.0") (default-features #t) (kind 0)) (crate-dep (name "failure") (req "^0.1.6") (default-features #t) (kind 0)) (crate-dep (name "git2") (req "^0.10.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "glob") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "ipnetwork") (req "^0.16.0") (default-features #t) (kind 0)) (crate-dep (name "rayon") (req "^1.3.0") (default-features #t) (kind 0)) (crate-dep (name "zmq") (req "^0.9") (optional #t) (default-features #t) (kind 0)))) (hash "194jjzs8axylkswc1jbg6hc20zn5rw15dcfzn2c7x55vbm73v29z") (features (quote (("windows-all" "bench" "update") ("update" "git2") ("microservice" "zmq") ("default" "bench") ("bench"))))))

(define-public crate-ipset_lookup-0.4 (crate (name "ipset_lookup") (vers "0.4.6") (deps (list (crate-dep (name "anyhow") (req "^1.0.68") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^2.0.0") (default-features #t) (kind 0)) (crate-dep (name "git2") (req "^0.10.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "glob") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "ipnetwork") (req "^0.16.0") (default-features #t) (kind 0)) (crate-dep (name "rayon") (req "^1.3.0") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.38") (kind 0)) (crate-dep (name "zmq") (req "^0.9") (features (quote ("vendored"))) (optional #t) (default-features #t) (kind 0)))) (hash "00lyqppzvrszawsmb2kfp03fqykynq3y6cw4vzxf6lls1iqbbs84") (features (quote (("windows-all" "bench" "update") ("vendored-zmq" "zmq/vendored") ("update" "git2") ("microservice" "zmq") ("default" "bench" "update") ("bench"))))))

