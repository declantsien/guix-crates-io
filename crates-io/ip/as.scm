(define-module (crates-io ip as) #:use-module (crates-io))

(define-public crate-ipasir-0.1 (crate (name "ipasir") (vers "0.1.0") (hash "1nx8kh09dqmgl7xwdriinawvmxb7k41khbb9zvgw7zvlw6rz8kj3")))

(define-public crate-ipasir-0.2 (crate (name "ipasir") (vers "0.2.0") (hash "0ll8f10qm1k6vpch651hl2s47a21lax2z4dwg5slqdaqpphnbrxa")))

(define-public crate-ipasir-0.3 (crate (name "ipasir") (vers "0.3.0") (hash "19pcbk5njrq1q6yc4qnhm8vzav69zsmic3lnjk323vr98xk5izlw") (features (quote (("ffi") ("default"))))))

(define-public crate-ipasir-0.3 (crate (name "ipasir") (vers "0.3.1") (hash "0csfzc1n6j4awjkwj1ndf2sh6g7fmbk9ll9qjlls5v02g7mb6m2b") (features (quote (("ffi") ("default"))))))

(define-public crate-ipasir-sys-0.1 (crate (name "ipasir-sys") (vers "0.1.0") (deps (list (crate-dep (name "bindgen") (req "^0.49.3") (default-features #t) (kind 1)) (crate-dep (name "copy_dir") (req "^0.1.2") (default-features #t) (kind 1)))) (hash "1ab6dqyphid5hc2n6lbxm1cl7bizsq8rhmyfpp68jdbbx6kxbgk9")))

(define-public crate-ipasir-sys-0.1 (crate (name "ipasir-sys") (vers "0.1.1") (deps (list (crate-dep (name "bindgen") (req "^0.49.3") (default-features #t) (kind 1)) (crate-dep (name "copy_dir") (req "^0.1.2") (default-features #t) (kind 1)))) (hash "1s5ggw5xk4kphcagwmv9m2r9gs7vclrsbc74ilz4h3q5m35mipx4")))

(define-public crate-ipasir-sys-0.2 (crate (name "ipasir-sys") (vers "0.2.0") (deps (list (crate-dep (name "bindgen") (req "^0.51.0") (default-features #t) (kind 1)) (crate-dep (name "copy_dir") (req "^0.1.2") (default-features #t) (kind 1)))) (hash "0dvkfl8922zxvja52l7xaszm3950vq6jb8bch16ipznqjxzyc7lg")))

(define-public crate-ipasir-sys-0.3 (crate (name "ipasir-sys") (vers "0.3.0") (deps (list (crate-dep (name "bindgen") (req "^0.51.0") (default-features #t) (kind 1)) (crate-dep (name "copy_dir") (req "^0.1.2") (default-features #t) (kind 1)))) (hash "1v12kajfjs9qvwlf80nikar8c842mgwxxf6gvnrarmzwjdxjrkdd")))

(define-public crate-ipassgen-0.2 (crate (name "ipassgen") (vers "0.2.0") (deps (list (crate-dep (name "clap") (req "^4.3.4") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "pwhash") (req "^1.0.0") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)) (crate-dep (name "rand_xorshift") (req "^0.3.0") (default-features #t) (kind 0)))) (hash "1m65ly5ff4c6km260najg03wrd5fdik5h4wn8cf3jqbjh4gbq4wd")))

(define-public crate-ipassgen-0.2 (crate (name "ipassgen") (vers "0.2.1") (deps (list (crate-dep (name "anyhow") (req "^1.0.71") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^4.3.4") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "pwhash") (req "^1.0.0") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)) (crate-dep (name "rand_xorshift") (req "^0.3.0") (default-features #t) (kind 0)))) (hash "0qmr70alj58c7j14mna548m40db0z4dmprxgkq78km1bdfz4032s")))

