(define-module (crates-io ip sl) #:use-module (crates-io))

(define-public crate-ipslim-1 (crate (name "ipslim") (vers "1.0.0") (deps (list (crate-dep (name "clippy") (req "^0.0.79") (optional #t) (default-features #t) (kind 0)))) (hash "0jzidlq8sm45qfjwdh1h81v1rriw143g2x5sjjl49453yv04i9ln") (features (quote (("dev" "clippy") ("default"))))))

