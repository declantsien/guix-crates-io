(define-module (crates-io ip v4) #:use-module (crates-io))

(define-public crate-ipv4-display-0.1 (crate (name "ipv4-display") (vers "0.1.0") (deps (list (crate-dep (name "criterion") (req "^0.3") (default-features #t) (kind 2)))) (hash "1nhn2w2gl2lm7bkdfb7qkhkplbz2f78fqbnprns42vkbpqja4xzw")))

(define-public crate-ipv4-display-0.2 (crate (name "ipv4-display") (vers "0.2.0") (deps (list (crate-dep (name "criterion") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "ufmt") (req "^0.1.0") (features (quote ("std"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "ufmt") (req "^0.1.0") (features (quote ("std"))) (default-features #t) (kind 2)))) (hash "0ijjlxxfh8k17a8h7wmd6rmxi4fpshzji6whmdiwrs5w11579vp2")))

(define-public crate-ipv4-display-0.2 (crate (name "ipv4-display") (vers "0.2.1") (deps (list (crate-dep (name "criterion") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "ufmt") (req "^0.1.0") (features (quote ("std"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "ufmt") (req "^0.1.0") (features (quote ("std"))) (default-features #t) (kind 2)))) (hash "1jpcpljaifn12nl6qyl3f2ypin58dsj2f3406ypkl1m68z4cgif0")))

