(define-module (crates-io ip ug) #:use-module (crates-io))

(define-public crate-ipug-0.2 (crate (name "ipug") (vers "0.2.0") (deps (list (crate-dep (name "pest") (req "^2.7") (default-features #t) (kind 0)) (crate-dep (name "pest_derive") (req "^2.7") (default-features #t) (kind 0)) (crate-dep (name "wasm-bindgen") (req "^0.2") (default-features #t) (target "cfg(target_arch = \"wasm32\")") (kind 0)))) (hash "0hmd89rf48zy9v5p1254n6nvzwj8nc9863m7ncxc0r2hfcmcrx0m")))

(define-public crate-ipug-0.2 (crate (name "ipug") (vers "0.2.1") (deps (list (crate-dep (name "pest") (req "^2.7") (default-features #t) (kind 0)) (crate-dep (name "pest_derive") (req "^2.7") (default-features #t) (kind 0)) (crate-dep (name "wasm-bindgen") (req "^0.2") (default-features #t) (target "cfg(target_arch = \"wasm32\")") (kind 0)))) (hash "14as191qyxcyafrmnpd4vwdpy1rk1xzdz1ib5rsc22pdvkzj2dgi")))

