(define-module (crates-io ip #{2c}#) #:use-module (crates-io))

(define-public crate-ip2c-0.1 (crate (name "ip2c") (vers "0.1.0") (hash "1qbh9kqx6a5ckfwfk9vc8hmadd1fd769ll947b2ipkwzb9hmmpay")))

(define-public crate-ip2c-0.1 (crate (name "ip2c") (vers "0.1.1") (hash "17bm7cgkqi1460jxmh159xrr14vj3j7zhhv6r8bnb31gwgccgnvd")))

(define-public crate-ip2c-0.1 (crate (name "ip2c") (vers "0.1.3") (hash "0k18sr56ns6g4msf2yb47cwqzlmmk914i3cx711j8yffc942jymf")))

(define-public crate-ip2c-0.1 (crate (name "ip2c") (vers "0.1.4") (hash "0biww7iqazpx42c49fq3iafyhjjbqhdbal1qb7c4pm2nbysb4w3q")))

(define-public crate-ip2c-0.1 (crate (name "ip2c") (vers "0.1.5") (hash "1qs4ssiia2d1wsvnfxck132p7vxq9iskgvsyvfs42kyfs7bfd5si")))

(define-public crate-ip2country-0.1 (crate (name "ip2country") (vers "0.1.0") (deps (list (crate-dep (name "criterion") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "static_assertions") (req "^1.1") (default-features #t) (kind 0)))) (hash "00kmksldk2vfpppckbjinz16bkb6crdgagfz4708xcrhgxrpgmng")))

(define-public crate-ip2country-0.2 (crate (name "ip2country") (vers "0.2.0") (deps (list (crate-dep (name "ascii") (req "^1.0") (kind 0)) (crate-dep (name "criterion") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "static_assertions") (req "^1.1") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0") (default-features #t) (kind 0)))) (hash "0wqnw7shiaf64609ns0hbs5pspzayxsmxb31rk9kaab0dsbsn205")))

(define-public crate-ip2country-0.3 (crate (name "ip2country") (vers "0.3.0") (deps (list (crate-dep (name "ascii") (req "^1.0") (kind 0)) (crate-dep (name "criterion") (req "^0.5") (default-features #t) (kind 2)) (crate-dep (name "static_assertions") (req "^1.1") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0") (default-features #t) (kind 0)))) (hash "02ydq3bv3jf3ws86a7gl16gxdb64dcpbm0a96yypd41vigi9f4rp")))

