(define-module (crates-io ip af) #:use-module (crates-io))

(define-public crate-ipafair-sys-1 (crate (name "ipafair-sys") (vers "1.0.0") (deps (list (crate-dep (name "bindgen") (req "^0.61.0") (default-features #t) (kind 1)))) (hash "0c59jxcyxpcid0p7faf3hghnzi3z3l3f3zcs2lb2mwkp400vb9wj")))

