(define-module (crates-io ip sa) #:use-module (crates-io))

(define-public crate-ipsae-core-0.1 (crate (name "ipsae-core") (vers "0.1.0") (deps (list (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)))) (hash "1f6xw32i617ns1n1w8z0kxbig4h0gyr2bi74fa9im6ql5wpqigzd")))

(define-public crate-ipsae-core-0.1 (crate (name "ipsae-core") (vers "0.1.1") (deps (list (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)))) (hash "1kwk9lyc599iznmcinwqkj31b5ak5walhg72ll4xxn1kn1hkgb91")))

