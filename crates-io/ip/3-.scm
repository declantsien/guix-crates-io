(define-module (crates-io ip #{3-}#) #:use-module (crates-io))

(define-public crate-ip3-cli-1 (crate (name "ip3-cli") (vers "1.0.0") (deps (list (crate-dep (name "clap") (req "^4.4.6") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "colored") (req "^2.0.4") (default-features #t) (kind 0)) (crate-dep (name "ip3") (req "^1.0.0") (default-features #t) (kind 0)) (crate-dep (name "local-ip-address") (req "^0.5.6") (default-features #t) (kind 0)) (crate-dep (name "public-ip") (req "^0.2.2") (features (quote ("dns-resolver"))) (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.32.0") (features (quote ("macros"))) (default-features #t) (kind 0)))) (hash "18gw68igifazy2s42khxkzqkx1kpdf79rb809fnkwignscga5v83")))

