(define-module (crates-io ip wh) #:use-module (crates-io))

(define-public crate-ipwhois-1 (crate (name "ipwhois") (vers "1.0.0") (hash "1h33kcx5nr3sl0rqs97ikzas6wiai7gkagvv08fdya33qxyxwgji")))

(define-public crate-ipwhois-1 (crate (name "ipwhois") (vers "1.0.1") (hash "063mdh76f1jsd9l1rsh4h4zcp70qjwb7pzcslc3ar3rj4i68msiy")))

(define-public crate-ipwhois-1 (crate (name "ipwhois") (vers "1.0.2") (hash "0f3g7s2r1z2z0paihwqzh40619jq46qz050ms7w3l2jvf20rdv3c")))

