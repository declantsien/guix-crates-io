(define-module (crates-io ip ho) #:use-module (crates-io))

(define-public crate-iphone_organizer-1 (crate (name "iphone_organizer") (vers "1.0.0") (hash "14wh4jpd581nnqrka4rya9kwrdpyza7xrvas2rfqx6gjz0kxnva5") (yanked #t)))

(define-public crate-iphone_organizer-1 (crate (name "iphone_organizer") (vers "1.0.1") (hash "1lifk0434n4ff3df5z5gfadhr9pv9i35jfyg3sbgi5scx0araldw")))

(define-public crate-iphone_organizer-1 (crate (name "iphone_organizer") (vers "1.0.2") (hash "0jb2f10qnimxmw4rb4fv8agkxlsp2zicmri36v4l3dv9adj2h375")))

(define-public crate-iphone_organizer-1 (crate (name "iphone_organizer") (vers "1.0.3") (hash "08178bn1a7v9hr2w6vhrsbfrwynj61n6pb0s639209zxi4v49i0h")))

(define-public crate-iphone_organizer-1 (crate (name "iphone_organizer") (vers "1.1.0") (hash "1z1p17qv6cjwxsq5s6q6mgd99mwlndvffqz1sxw137z2v24wa8vd") (yanked #t)))

(define-public crate-iphone_organizer-1 (crate (name "iphone_organizer") (vers "1.1.1") (hash "1c6bdv6nj3q6s0l5y98y4x5v4sdjhq0czam0lkqqrmdvdjz71d70") (yanked #t)))

(define-public crate-iphone_organizer-1 (crate (name "iphone_organizer") (vers "1.1.2") (hash "1pmjjyl8y17385j9gkw5gqhrvmmv26bz9fd3r8m4px360aky0vcs") (yanked #t)))

(define-public crate-iphone_organizer-1 (crate (name "iphone_organizer") (vers "1.1.3") (hash "044fip3kw25p6c65iaqk1l0wi77a1dag36i1zbldnabz1h6jph5v") (yanked #t)))

(define-public crate-iphone_organizer-1 (crate (name "iphone_organizer") (vers "1.2.0") (hash "05h85z0cnak4g7bn94ya51xmvp9wj9g9rj8drkbwnslaj8dpnkdd") (yanked #t)))

(define-public crate-iphone_organizer-1 (crate (name "iphone_organizer") (vers "1.2.1") (hash "1wzk0sfk7b28n7fhbcp2q4ik4i7xh9ibzhy02k3zk3vq9c0zwdwf") (yanked #t)))

(define-public crate-iphone_organizer-1 (crate (name "iphone_organizer") (vers "1.2.2") (hash "1p17k95x2xqy2rn7ap64swmdwpjbak5ibmd3x6vcnrp3v1hqkhzi")))

(define-public crate-iphone_organizer-1 (crate (name "iphone_organizer") (vers "1.2.3") (hash "1sj2mjz1mc2hdxzq5r5a0wjmb9rq86jzwydkrvz2ydwb6wdcdxkx")))

(define-public crate-iphone_organizer-1 (crate (name "iphone_organizer") (vers "1.2.4") (hash "1pzdjhx6nw0hb2s86h70h8l0zmn9l0mq7x9jxqxa63y05wb4676k")))

(define-public crate-iphone_organizer-1 (crate (name "iphone_organizer") (vers "1.3.0") (hash "05vg2c2kpm1i9k9xybc4ccc7c56h1r33jckagzm01ih55c2kkjhm")))

