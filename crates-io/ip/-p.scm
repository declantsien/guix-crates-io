(define-module (crates-io ip -p) #:use-module (crates-io))

(define-public crate-ip-packet-0.0.0 (crate (name "ip-packet") (vers "0.0.0") (hash "1qar2byz3ippxsb2qr9ix6bj5xz0p7r4mz10kvwg97vajqjs6viq")))

(define-public crate-ip-parser-0.1 (crate (name "ip-parser") (vers "0.1.0") (deps (list (crate-dep (name "ipnetwork") (req "^0.18.0") (default-features #t) (kind 0)) (crate-dep (name "itertools") (req "^0.10.3") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.3.26") (default-features #t) (kind 0)))) (hash "05704bmdy7kc6mlzw0yl1d4w2mznprvmv0yrjycv2d91jn4xvk4b")))

(define-public crate-ip-parser-0.1 (crate (name "ip-parser") (vers "0.1.1") (deps (list (crate-dep (name "ipnetwork") (req "^0.18.0") (default-features #t) (kind 0)) (crate-dep (name "itertools") (req "^0.10.3") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.3.26") (default-features #t) (kind 0)))) (hash "1yypxdbn3iaiyp2bgb33rvp2b0jgy72kr5fmwcg252sqwqx5yh0i")))

(define-public crate-ip-parser-0.1 (crate (name "ip-parser") (vers "0.1.2") (deps (list (crate-dep (name "ipnetwork") (req "^0.18.0") (default-features #t) (kind 0)) (crate-dep (name "itertools") (req "^0.10.3") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.3.26") (default-features #t) (kind 0)))) (hash "1gqnbbdvqza31l5h468bzvqd9s8h8pz6jsfhssr51l5p6qwms179")))

(define-public crate-ip-part-0.1 (crate (name "ip-part") (vers "0.1.0") (deps (list (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.5") (default-features #t) (kind 0)))) (hash "01fpl119bal76wgdl4b9zw1yyp66sycljy3h9np3aibbdskix01l") (yanked #t)))

(define-public crate-ip-part-0.1 (crate (name "ip-part") (vers "0.1.1") (deps (list (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.5") (default-features #t) (kind 0)))) (hash "1iqwcdy7z2hs8yzkf46ma08zgglan5l57gyg7avz0xb5vpzbspi1")))

