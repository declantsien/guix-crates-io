(define-module (crates-io ip da) #:use-module (crates-io))

(define-public crate-ipdata-0.1 (crate (name "ipdata") (vers "0.1.0") (deps (list (crate-dep (name "reqwest") (req "^0.9.9") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.87") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0.87") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.38") (default-features #t) (kind 0)))) (hash "16bsklv2sca3scl8zql095h8vvb1czxip1f8bah2ypn6zvvdcvl9")))

