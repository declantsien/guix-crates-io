(define-module (crates-io ip qs) #:use-module (crates-io))

(define-public crate-ipqs_db_reader-1 (crate (name "ipqs_db_reader") (vers "1.0.0") (deps (list (crate-dep (name "serde") (req "^1.0.160") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.96") (optional #t) (default-features #t) (kind 0)))) (hash "0sva8sbjskv23kpana2xd9x68g3l3mmc7rrb2mz1naz2qr8llr5w") (features (quote (("default" "json")))) (v 2) (features2 (quote (("json" "dep:serde" "dep:serde_json"))))))

