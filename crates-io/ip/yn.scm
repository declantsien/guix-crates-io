(define-module (crates-io ip yn) #:use-module (crates-io))

(define-public crate-ipynb-0.0.1 (crate (name "ipynb") (vers "0.0.1") (deps (list (crate-dep (name "clap") (req "^2.34.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.131") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.73") (default-features #t) (kind 0)))) (hash "0kvzismxjv6cr1xd5jirbvxx0mvr55xzjn8wi16s549wiazgzswj")))

