(define-module (crates-io ip fy) #:use-module (crates-io))

(define-public crate-ipfy-public-ip-0.1 (crate (name "ipfy-public-ip") (vers "0.1.0") (deps (list (crate-dep (name "reqwest") (req "^0.11.19") (features (quote ("json"))) (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.31.0") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "1kn0bwgk4khh08kp2kr1dfki5vp79lc08fapyfcpp1fy6ii8x5z1")))

(define-public crate-ipfy-public-ip-0.1 (crate (name "ipfy-public-ip") (vers "0.1.1") (deps (list (crate-dep (name "reqwest") (req "^0.11.19") (features (quote ("json"))) (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.31.0") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "03ynxyk4vi5gp69vl9k470d4c7sckwcriav09wf8inl1qfdy07p4")))

(define-public crate-ipfy-public-ip-0.1 (crate (name "ipfy-public-ip") (vers "0.1.2") (deps (list (crate-dep (name "reqwest") (req "^0.11.19") (features (quote ("json"))) (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.31.0") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "0g9sz901ffd5ci2yd745sa5pfmv3gjsqzr5xrww8i90nw4bmhpmw")))

