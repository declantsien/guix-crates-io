(define-module (crates-io ip -m) #:use-module (crates-io))

(define-public crate-ip-macro-0.1 (crate (name "ip-macro") (vers "0.1.0") (deps (list (crate-dep (name "quote") (req "^0.3.15") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^0.11.9") (default-features #t) (kind 0)))) (hash "0cyp7x4qia86lkxnlwggrlqvyyj4k15h49syqbwbhhb23hhd9lh1")))

