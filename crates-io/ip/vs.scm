(define-module (crates-io ip vs) #:use-module (crates-io))

(define-public crate-ipvs-0.1 (crate (name "ipvs") (vers "0.1.0") (hash "1flyqibikdr9nj40lcq81pyj5pyir7f1q6i1xr6zicm7xhvbkhpx")))

(define-public crate-ipvs-0.1 (crate (name "ipvs") (vers "0.1.1") (deps (list (crate-dep (name "bitflags") (req "^0.9") (default-features #t) (kind 0)) (crate-dep (name "byteorder") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "121v2q2xp7fkgfyp13zjjw5aw7v6kx617q5q27hhin6z2jmmsc6x")))

