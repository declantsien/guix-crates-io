(define-module (crates-io ip ne) #:use-module (crates-io))

(define-public crate-ipnet-0.1 (crate (name "ipnet") (vers "0.1.0") (hash "0xzk2006f2cprd6k4f48l68ylrkwrqxzw9ys411qixa2gz1gdqvq")))

(define-public crate-ipnet-0.11 (crate (name "ipnet") (vers "0.11.0") (hash "1dr1ipr92546ml89w2yy86bpcdwg47xswkjbf84chr6xzlnwzq9j")))

(define-public crate-ipnet-0.11 (crate (name "ipnet") (vers "0.11.1") (hash "1vn9rhadh7p3nagzbg9rizhiinxpbcq2rbs1ym15bv7pgrwbkfbv")))

(define-public crate-ipnet-0.12 (crate (name "ipnet") (vers "0.12.0") (hash "0qnfp9zlamj32hnvwg949h39fypbksw3hn0zvp9cc5zas3nr97b5")))

(define-public crate-ipnet-0.13 (crate (name "ipnet") (vers "0.13.0") (hash "07xydvwyr15fnkb6jj026hq4vwcr5fkfwfc38pnqaf9y69s07hzz")))

(define-public crate-ipnet-0.13 (crate (name "ipnet") (vers "0.13.1") (hash "0q6jar6n95gir9blwfqyl1fvjqsmsq773m3vw6xd9rx0mqb4rbl7")))

(define-public crate-ipnet-0.13 (crate (name "ipnet") (vers "0.13.2") (hash "0ixy6nan4ygzlk4kwbkn152adpc3mmjvvxn1aal7fqslrjk78qqy")))

(define-public crate-ipnet-0.13 (crate (name "ipnet") (vers "0.13.3") (hash "0yyj3h3xrrmg74k87sdm0m429n1nxvfj1gm5wymrnn11myb0zb20")))

(define-public crate-ipnet-0.14 (crate (name "ipnet") (vers "0.14.0") (hash "1xxz8bsr8a20zma4zh9f0psirgcqdh3nl3w5wbaf5ml4v4r6wiir")))

(define-public crate-ipnet-0.15 (crate (name "ipnet") (vers "0.15.0") (hash "0xzdcfkiisf984q8ma4lq61ndps0cpsfyyg1hc1kdvx11klalivx")))

(define-public crate-ipnet-0.15 (crate (name "ipnet") (vers "0.15.1") (hash "0h5c4frrwl1am53nl00xavvch9y083r4j9s6bmrlnrrhjng4ba9n")))

(define-public crate-ipnet-0.15 (crate (name "ipnet") (vers "0.15.2") (hash "1r464mxvc8jqf29asmrfgi21q49pi7yk17sii13pkx1nl1zzdnl5")))

(define-public crate-ipnet-0.15 (crate (name "ipnet") (vers "0.15.3") (hash "0829c0r9wfd4sk0xnhzfxvcdp6krn2ykjklyq1c3m15pdrw7dbib")))

(define-public crate-ipnet-0.16 (crate (name "ipnet") (vers "0.16.0") (hash "0pqfd3q87620pmgns59d5cbw89g40d5mqchykvwxbmhc6fxh1kh9")))

(define-public crate-ipnet-0.16 (crate (name "ipnet") (vers "0.16.1") (hash "1q1ilww76grmskvmxn1liw683x0q647cjkp75vji7732xf6a58fv")))

(define-public crate-ipnet-0.16 (crate (name "ipnet") (vers "0.16.2") (hash "0l1x71xjsn828vigbrk281k1m0b6qmbvc3s6yv143xml6d7v398r")))

(define-public crate-ipnet-0.17 (crate (name "ipnet") (vers "0.17.0") (hash "0y9a1ihz733vxyvaq1k4h0byf9yxicpscpl3vm1p32qd4p0lj7q7")))

(define-public crate-ipnet-0.17 (crate (name "ipnet") (vers "0.17.1") (hash "03m6z3ix0xx0afyadcah3yp42crc4vgihml0vfrs18c8v245vz4r")))

(define-public crate-ipnet-0.18 (crate (name "ipnet") (vers "0.18.0") (hash "0rmh8iby55slw5r9xpcsfqh3j4c1g2v5k6vyywlcz75bl5zg0wy7")))

(define-public crate-ipnet-0.18 (crate (name "ipnet") (vers "0.18.1") (hash "18pbm4b0gvl70ms932qm056wbsfnyj624piqh56i7pdnvz6wh9qg")))

(define-public crate-ipnet-0.18 (crate (name "ipnet") (vers "0.18.2") (hash "0qlm4i342xsdg93x6yhkq2vxzicghinbg7fxfspv1iwz9vqjcgyf")))

(define-public crate-ipnet-0.18 (crate (name "ipnet") (vers "0.18.3") (hash "03hc64l4dnz0fh5jnywjzx5l0m64ql6srywcmarh8c4p6vpiq9f5")))

(define-public crate-ipnet-0.19 (crate (name "ipnet") (vers "0.19.0") (hash "1jldpcql7kga7xwx72a2d8p0x72xgi877ihk7lmkan45x5fjr03v")))

(define-public crate-ipnet-0.20 (crate (name "ipnet") (vers "0.20.0") (hash "0bb6a3fnng4qyaw4nm39ma6ykm2d8r1ds72yks9r3kns5hvz8p8h")))

(define-public crate-ipnet-0.20 (crate (name "ipnet") (vers "0.20.1") (hash "0n6lmq6sml210splxbdzzfyxz8cczpj15625srlf6prqrijflirv")))

(define-public crate-ipnet-0.21 (crate (name "ipnet") (vers "0.21.0") (hash "01cznf0mcqc1wd7011lxkb39c95n93adqbfr07hz63zm17rad0jf")))

(define-public crate-ipnet-0.21 (crate (name "ipnet") (vers "0.21.1") (hash "0k67h0ldnj3zcs6zybhzdk12n7mxki0bxx6ccnp8ws3499naa85n")))

(define-public crate-ipnet-0.22 (crate (name "ipnet") (vers "0.22.0") (hash "19738nqy36rvmaszn1hwh9yj4zb9cgq767f967av0rv3xhf1m5ay")))

(define-public crate-ipnet-0.23 (crate (name "ipnet") (vers "0.23.0") (hash "1g3my1fzl0jil4ych16bpx5qrkrfms8crgc0vqgdscxqnj89gbzs")))

(define-public crate-ipnet-0.23 (crate (name "ipnet") (vers "0.23.1") (hash "0gbg0dzgygnhh9fzylay21fjsj1w26i45pf49s274kw2zly3wrwd")))

(define-public crate-ipnet-0.24 (crate (name "ipnet") (vers "0.24.0") (hash "09s03x50zxvakp7msr1q1c62r2fkgpbs2i530mgyq8qiimsfmabw")))

(define-public crate-ipnet-0.24 (crate (name "ipnet") (vers "0.24.1") (hash "17j7hlqikgwxli898y6vd3z30yfgrh8dsksvv6s7izb3nrc84nlq")))

(define-public crate-ipnet-0.24 (crate (name "ipnet") (vers "0.24.2") (hash "10xrzj7vw6x57mwrpqg8mdaq8c6gmw83my393axxqm65jx80mc6g")))

(define-public crate-ipnet-0.24 (crate (name "ipnet") (vers "0.24.3") (hash "0c14c4lqnjb6b95kb08jsifng4sm0bx5bq92cdxxajxh810420d9")))

(define-public crate-ipnet-0.25 (crate (name "ipnet") (vers "0.25.0") (hash "0c04nh4n4n8njnllilr93vk3c7hf138hghrhrq293mzxb8d1dp6m")))

(define-public crate-ipnet-0.25 (crate (name "ipnet") (vers "0.25.1") (hash "1gj2i34xfm4apzfa5b2h0p0n69lip616ifss893rpab1gv1618ci")))

(define-public crate-ipnet-0.26 (crate (name "ipnet") (vers "0.26.0") (hash "1a9dms6i71ja7z3wzwvnnlr41iayk2vr5v7laryd5xdmvcarfanh")))

(define-public crate-ipnet-0.26 (crate (name "ipnet") (vers "0.26.1") (hash "0faj3784akzmq1952m1jkvgjwf0vzjbv17i6qjgic0ij7hvcrpf4")))

(define-public crate-ipnet-0.26 (crate (name "ipnet") (vers "0.26.2") (hash "0vazmnvi88vinicmaw1nsv5659k6xkkgcpa7zabvmvfa5d4ldyf8")))

(define-public crate-ipnet-0.26 (crate (name "ipnet") (vers "0.26.3") (hash "1wic7l53gvjw91hr37l5lwnvc8dka0xav6n6wf4bvy9cm1pw7xzn")))

(define-public crate-ipnet-0.26 (crate (name "ipnet") (vers "0.26.4") (hash "1zqwmw0hqjv2i7if44ylfkcp62zn13gazz4lhi4gpxbd7ph98975")))

(define-public crate-ipnet-0.26 (crate (name "ipnet") (vers "0.26.5") (hash "1dgc28cy8m27hr7vmghyfsic3vyb456f2q12qswxiyk089f8k182")))

(define-public crate-ipnet-0.27 (crate (name "ipnet") (vers "0.27.0") (hash "0wnxrfqfpwfs9hmrzy5mgb4ar4f6rw12lmb85zplv10qvl89l1wa")))

(define-public crate-ipnet-0.28 (crate (name "ipnet") (vers "0.28.0") (hash "1ir7l6jz51jdjwx2a3l0kgyp6b2jn6f8n4s79ivy5n6bl3m7lw3w")))

(define-public crate-ipnet-0.28 (crate (name "ipnet") (vers "0.28.1") (hash "1djjb05g83h5kp5wfgh9ylj86lhzzrk2qjbk6ampqahrn0cizq36")))

(define-public crate-ipnet-1 (crate (name "ipnet") (vers "1.0.0-rc1") (hash "1lrvfkgjqh2va3rp3iqlyrc3dawb522asjg6a410y1l5jvsb5pj2")))

(define-public crate-ipnet-1 (crate (name "ipnet") (vers "1.0.0") (hash "1vv3nwsgiw4mq4v4flx7wv9rybmylliz9fx0rk8syimd4wx8q9ji")))

(define-public crate-ipnet-1 (crate (name "ipnet") (vers "1.1.0") (deps (list (crate-dep (name "serde") (req "^1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde_test") (req "^1") (default-features #t) (kind 2)))) (hash "0ck0gwsl26y1j87ymqyi477zq78ww0bmqli04vnkf3ka9mj8y5n8") (features (quote (("with-serde" "serde" "serde_derive") ("default")))) (yanked #t)))

(define-public crate-ipnet-1 (crate (name "ipnet") (vers "1.2.0") (deps (list (crate-dep (name "serde") (req "^1") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde_test") (req "^1") (default-features #t) (kind 2)))) (hash "0ny7hddkv9i54bh4gfcsskljjv9vil1870lv7ppn4v024dqm25p1") (features (quote (("with-serde" "serde"))))))

(define-public crate-ipnet-1 (crate (name "ipnet") (vers "1.2.1") (deps (list (crate-dep (name "serde") (req "^1") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde_test") (req "^1") (default-features #t) (kind 2)))) (hash "1n4sxvdxak56y8ki3bbwfxpq7lk05sqjfp5prj01x6bz17yz9j27") (features (quote (("with-serde" "serde"))))))

(define-public crate-ipnet-2 (crate (name "ipnet") (vers "2.0.0-rc1") (deps (list (crate-dep (name "serde") (req "^1") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde_test") (req "^1") (default-features #t) (kind 2)))) (hash "0zx90i8q0skmkjy0r71969s5zkdy4wasdxps5kdiqm6x2g2301wn")))

(define-public crate-ipnet-2 (crate (name "ipnet") (vers "2.0.0") (deps (list (crate-dep (name "serde") (req "^1") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde_test") (req "^1") (default-features #t) (kind 2)))) (hash "1by2yinh68b6w3z82ricl5gj67g4jd7vyg9i5mywf07ps2h2s776")))

(define-public crate-ipnet-2 (crate (name "ipnet") (vers "2.0.1") (deps (list (crate-dep (name "serde") (req "^1") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde_test") (req "^1") (default-features #t) (kind 2)))) (hash "0j3a562796hsgxlaak5hn1x9j8xb41lyz2c90yx64vc8w315mhbc")))

(define-public crate-ipnet-2 (crate (name "ipnet") (vers "2.1.0") (deps (list (crate-dep (name "serde") (req "^1") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde_test") (req "^1") (default-features #t) (kind 2)))) (hash "017g9pc4flwq6r07liq7pp3cfjbc2391gzcd1j32h0nv45mv1x7j")))

(define-public crate-ipnet-2 (crate (name "ipnet") (vers "2.2.0") (deps (list (crate-dep (name "serde") (req "^1") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde_test") (req "^1") (default-features #t) (kind 2)))) (hash "1yqn565vp0xq8pl0dva6q1ghfa96i6hrhvw13s63ilb3qmyhand8")))

(define-public crate-ipnet-2 (crate (name "ipnet") (vers "2.3.0") (deps (list (crate-dep (name "serde") (req "^1") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde_test") (req "^1") (default-features #t) (kind 2)))) (hash "0db147nh8jnxr23yxa7hwqn7dcjivdqi3aq4mgf2zgkqqqa2zgj7")))

(define-public crate-ipnet-2 (crate (name "ipnet") (vers "2.3.1") (deps (list (crate-dep (name "serde") (req "^1") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde_test") (req "^1") (default-features #t) (kind 2)))) (hash "1ad32j3kkbb0bgf5whzfkdw6843ywr48245dhk7c9gny5r7xdwk8")))

(define-public crate-ipnet-2 (crate (name "ipnet") (vers "2.4.0") (deps (list (crate-dep (name "schemars") (req "^0.8") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0) (package "serde")) (crate-dep (name "serde_test") (req "^1") (default-features #t) (kind 2)))) (hash "0z5wa0m27w3clja717kwmkbdn8hg1504kbgx2ffgs0nwjkh0xrrm") (features (quote (("json" "serde" "schemars") ("default"))))))

(define-public crate-ipnet-2 (crate (name "ipnet") (vers "2.5.0") (deps (list (crate-dep (name "schemars") (req "^0.8") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0) (package "serde")) (crate-dep (name "serde_test") (req "^1") (default-features #t) (kind 2)))) (hash "0asr5bwhbfxgxwappmvs0rvb0ncc5adnhfi9yiz4axlc9j1m97c7") (features (quote (("json" "serde" "schemars") ("default"))))))

(define-public crate-ipnet-2 (crate (name "ipnet") (vers "2.5.1") (deps (list (crate-dep (name "schemars") (req "^0.8") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0) (package "serde")) (crate-dep (name "serde_test") (req "^1") (default-features #t) (kind 2)))) (hash "0ic7pm4df3waxc0vi9vy1wq5i5azzlccz2yrz6fyd28i2xhmb37q") (features (quote (("json" "serde" "schemars") ("default"))))))

(define-public crate-ipnet-2 (crate (name "ipnet") (vers "2.6.0") (deps (list (crate-dep (name "schemars") (req "^0.8") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0) (package "serde")) (crate-dep (name "serde_test") (req "^1") (default-features #t) (kind 2)))) (hash "1v8da7lr5kqbrjjsxifnqyv2bl14w5yaxaskwf3knbp19ix7p57c") (features (quote (("json" "serde" "schemars") ("default"))))))

(define-public crate-ipnet-2 (crate (name "ipnet") (vers "2.7.0") (deps (list (crate-dep (name "schemars") (req "^0.8") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0) (package "serde")) (crate-dep (name "serde_test") (req "^1") (default-features #t) (kind 2)))) (hash "0zli80q3brzac2nikawxldnzazbqz4606n0y4lxm95h6crpdkc0i") (features (quote (("json" "serde" "schemars") ("default"))))))

(define-public crate-ipnet-2 (crate (name "ipnet") (vers "2.7.1") (deps (list (crate-dep (name "schemars") (req "^0.8") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0) (package "serde")) (crate-dep (name "serde_test") (req "^1") (default-features #t) (kind 2)))) (hash "0ilidjg9v561449x7cd3jqdmcl68fsksg7mma1a8jnckcbc2pqih") (features (quote (("json" "serde" "schemars") ("default"))))))

(define-public crate-ipnet-2 (crate (name "ipnet") (vers "2.7.2") (deps (list (crate-dep (name "schemars") (req "^0.8") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0) (package "serde")) (crate-dep (name "serde_test") (req "^1") (default-features #t) (kind 2)))) (hash "0zxmnidy5qha1i384fzjfxcsi0qvkbcp730h26q4z3dg54hyxdhj") (features (quote (("json" "serde" "schemars") ("default"))))))

(define-public crate-ipnet-2 (crate (name "ipnet") (vers "2.8.0") (deps (list (crate-dep (name "schemars") (req "^0.8") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1") (features (quote ("derive"))) (optional #t) (kind 0) (package "serde")) (crate-dep (name "serde_test") (req "^1") (default-features #t) (kind 2)))) (hash "1rhlmw1cpgpba6414m05d2s0xbiblkmkmzjfjfc4a3sgswy9mci8") (features (quote (("std") ("json" "serde" "schemars") ("default" "std"))))))

(define-public crate-ipnet-2 (crate (name "ipnet") (vers "2.9.0") (deps (list (crate-dep (name "heapless") (req "^0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "schemars") (req "^0.8") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1") (features (quote ("derive"))) (optional #t) (kind 0) (package "serde")) (crate-dep (name "serde_test") (req "^1") (default-features #t) (kind 2)))) (hash "1hzrcysgwf0knf83ahb3535hrkw63mil88iqc6kjaryfblrqylcg") (features (quote (("std") ("ser_as_str" "heapless") ("json" "serde" "schemars") ("default" "std"))))))

(define-public crate-ipnet-trie-0.0.1 (crate (name "ipnet-trie") (vers "0.0.1") (deps (list (crate-dep (name "criterion") (req "^0.5") (default-features #t) (kind 2)) (crate-dep (name "ip_network_table-deps-treebitmap") (req "^0.5") (default-features #t) (kind 0)) (crate-dep (name "ipnet") (req "^2.8") (default-features #t) (kind 0)))) (hash "0c4x2igfv7hv4lkg6cdsfy6vcjwsn6nkhhv1rkiq7aawqh06sh00")))

(define-public crate-ipnet-trie-0.0.2 (crate (name "ipnet-trie") (vers "0.0.2") (deps (list (crate-dep (name "criterion") (req "^0.5") (default-features #t) (kind 2)) (crate-dep (name "ip_network_table-deps-treebitmap") (req "^0.5") (default-features #t) (kind 0)) (crate-dep (name "ipnet") (req "^2.8") (default-features #t) (kind 0)))) (hash "10ibvdw1ff58bsvxix8sb6zgqlxcsqx1m0mikng7jhz2ag9w1z58")))

(define-public crate-ipnet-trie-0.1 (crate (name "ipnet-trie") (vers "0.1.0") (deps (list (crate-dep (name "bgpkit-parser") (req "^0.10.0-beta.1") (default-features #t) (kind 2)) (crate-dep (name "bincode") (req "^2.0.0-rc") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "criterion") (req "^0.5") (default-features #t) (kind 2)) (crate-dep (name "ip_network_table-deps-treebitmap") (req "^0.5") (default-features #t) (kind 0)) (crate-dep (name "ipnet") (req "^2.8") (default-features #t) (kind 0)) (crate-dep (name "oneio") (req "^0.15") (default-features #t) (kind 2)) (crate-dep (name "tempdir") (req "^0.3") (default-features #t) (kind 2)))) (hash "0r2vr81ffdmpj7hprnx89ilzl3wvx1zqkc4k6k864f8zrdnb8vdh") (features (quote (("export" "bincode") ("default"))))))

(define-public crate-ipnet-trie-0.2 (crate (name "ipnet-trie") (vers "0.2.0-beta.1") (deps (list (crate-dep (name "bgpkit-parser") (req "^0.10.0") (default-features #t) (kind 2)) (crate-dep (name "bincode") (req "^2.0.0-rc") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "criterion") (req "^0.5") (default-features #t) (kind 2)) (crate-dep (name "ipnet") (req "^2.8") (default-features #t) (kind 0)) (crate-dep (name "oneio") (req "^0.16") (default-features #t) (kind 2)) (crate-dep (name "prefix-trie") (req "^0.2.5") (default-features #t) (kind 0)) (crate-dep (name "tempdir") (req "^0.3") (default-features #t) (kind 2)))) (hash "0915b1gq7as3450xhilcbv3pwjfzhhpv707qvqkkm8kymcirfh6l") (features (quote (("export" "bincode") ("default"))))))

(define-public crate-ipnet-trie-0.2 (crate (name "ipnet-trie") (vers "0.2.0-beta.2") (deps (list (crate-dep (name "anyhow") (req "^1.0") (default-features #t) (kind 2)) (crate-dep (name "bgpkit-parser") (req "^0.10") (default-features #t) (kind 2)) (crate-dep (name "bincode") (req "^2.0.0-rc") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "criterion") (req "^0.5") (default-features #t) (kind 2)) (crate-dep (name "ipnet") (req "^2.8") (default-features #t) (kind 0)) (crate-dep (name "oneio") (req "^0.16") (default-features #t) (kind 2)) (crate-dep (name "prefix-trie") (req "^0.2.5") (default-features #t) (kind 0)) (crate-dep (name "tempdir") (req "^0.3") (default-features #t) (kind 2)))) (hash "0dr2mxs31gal3bk4p73hvis1b94p3yr0kcxkcj2npr3b3qac2bl5") (features (quote (("export" "bincode") ("default"))))))

(define-public crate-ipnetwork-0.1 (crate (name "ipnetwork") (vers "0.1.0") (hash "1b93lm5h18nih4nigw2bd0mqiw72wb06ff3076zwgxarh9hnbahz")))

(define-public crate-ipnetwork-0.1 (crate (name "ipnetwork") (vers "0.1.1") (hash "1nabrc8akx88axqliiyk5crbni9244iqj83w0d40hphi5h43risv")))

(define-public crate-ipnetwork-0.1 (crate (name "ipnetwork") (vers "0.1.2") (hash "0x0xgdz07l7gs3ykd66zgh5npnnr1i8q2xlmg04nqi8x71iliak6")))

(define-public crate-ipnetwork-0.1 (crate (name "ipnetwork") (vers "0.1.3") (hash "08zbz5rgsdb6cnpiifplb3i9ys5sxvh5w67ryk6k394llp5ifbx5")))

(define-public crate-ipnetwork-0.1 (crate (name "ipnetwork") (vers "0.1.4") (hash "1s4pdpnmaz5fikyz5n12mn2zdx55sajcd0g6ynm5rly1yf0gwl48")))

(define-public crate-ipnetwork-0.1 (crate (name "ipnetwork") (vers "0.1.5") (hash "1zn350kf5b3j34q5rf44lk13igc8zd9im5z2h3qc1mdmviidl3k8")))

(define-public crate-ipnetwork-0.1 (crate (name "ipnetwork") (vers "0.1.6") (hash "0ry54scm8gjbfxg6gvz967j7m7d44n0y9zc2h64ly97jj4k83zvv")))

(define-public crate-ipnetwork-0.1 (crate (name "ipnetwork") (vers "0.1.7") (hash "07z1a7y33w05kgh1ajdm2ljcgdsys064zzmbhgky5n763n0hg8sb")))

(define-public crate-ipnetwork-0.1 (crate (name "ipnetwork") (vers "0.1.8") (hash "03kkn2ryd3hhgkp08219wv2f7biczyndc9pkf6ah36ddq60awglk")))

(define-public crate-ipnetwork-0.1 (crate (name "ipnetwork") (vers "0.1.9") (hash "1p9dx7xzxw4dcrykjdl9pbajrhf979vmcv0nk6rmv373n6drbbmx")))

(define-public crate-ipnetwork-0.1 (crate (name "ipnetwork") (vers "0.1.10") (hash "0i3h2fglvqcayfzlid1g4l36a4djw62c3dszq8wxgx0jm4kn3mj2")))

(define-public crate-ipnetwork-0.1 (crate (name "ipnetwork") (vers "0.1.11") (hash "0v29dsxmqbz4fz3q98cmh9la7r99cpqadd29qg6gmiq8ga12dz49")))

(define-public crate-ipnetwork-0.2 (crate (name "ipnetwork") (vers "0.2.1") (hash "07875f1p5r0n16wc38k99y8cc56g9azdghgzabbn402qpih0w9sz")))

(define-public crate-ipnetwork-0.2 (crate (name "ipnetwork") (vers "0.2.2") (hash "1bbhijh5650j5qhyw775y69pch4mwk6ywqizvvb3gdih4y9wmyny")))

(define-public crate-ipnetwork-0.3 (crate (name "ipnetwork") (vers "0.3.0") (deps (list (crate-dep (name "clippy") (req "^0.0.21") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "ip") (req "^1.0.0") (default-features #t) (kind 0)))) (hash "08wz2n2n815jaqagx91sfnva40alf6d6zd79k67if2s44bi1y9va") (features (quote (("dev" "clippy") ("default"))))))

(define-public crate-ipnetwork-0.4 (crate (name "ipnetwork") (vers "0.4.0") (deps (list (crate-dep (name "clippy") (req "^0.0.37") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "ip") (req "^1.0.0") (default-features #t) (kind 0)))) (hash "1l7bl8wfkj8fshw1c671xn5l080ca653jzckyz31q33hgzx25vgw") (features (quote (("dev" "clippy") ("default"))))))

(define-public crate-ipnetwork-0.5 (crate (name "ipnetwork") (vers "0.5.0") (deps (list (crate-dep (name "clippy") (req "^0.0.37") (optional #t) (default-features #t) (kind 0)))) (hash "103cpsc0xvp82aivjlkwjm2cg3889c7ngc038qbfrjk5yw9byagi") (features (quote (("dev" "clippy") ("default"))))))

(define-public crate-ipnetwork-0.6 (crate (name "ipnetwork") (vers "0.6.0") (deps (list (crate-dep (name "clippy") (req "^0.0.37") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "ip") (req "^1.0.0") (default-features #t) (kind 0)))) (hash "08v2shrqd2kv9cahj6waymw5qvbjxgq5phsz2jpq0v53v54ijrwl") (features (quote (("dev" "clippy") ("default"))))))

(define-public crate-ipnetwork-0.6 (crate (name "ipnetwork") (vers "0.6.1") (deps (list (crate-dep (name "clippy") (req "^0.0.37") (optional #t) (default-features #t) (kind 0)))) (hash "1c9bvrdz9br270lksdxs7nix998xkrwy7dj623kgf0707hxq0403") (features (quote (("dev" "clippy") ("default"))))))

(define-public crate-ipnetwork-0.6 (crate (name "ipnetwork") (vers "0.6.2") (deps (list (crate-dep (name "clippy") (req "^0.0.37") (optional #t) (default-features #t) (kind 0)))) (hash "0zzwj0babvlfly0hbv2w542ikpj8n66dgm90fgsmzd3749bmhf1y") (features (quote (("dev" "clippy") ("default"))))))

(define-public crate-ipnetwork-0.7 (crate (name "ipnetwork") (vers "0.7.0") (deps (list (crate-dep (name "clippy") (req "^0.0.37") (optional #t) (default-features #t) (kind 0)))) (hash "02cgvjyjmld2l998q0ips15mkwy1wc930mbvzabni5wwdyvw38qg") (features (quote (("dev" "clippy") ("default"))))))

(define-public crate-ipnetwork-0.8 (crate (name "ipnetwork") (vers "0.8.0") (deps (list (crate-dep (name "clippy") (req "^0.0.37") (optional #t) (default-features #t) (kind 0)))) (hash "1nhjs95arw13455adx0ancnlb16840nw71913f0y5c92mdkxj932") (features (quote (("dev" "clippy") ("default"))))))

(define-public crate-ipnetwork-0.9 (crate (name "ipnetwork") (vers "0.9.0") (deps (list (crate-dep (name "clippy") (req "^0.0.37") (optional #t) (default-features #t) (kind 0)))) (hash "1xp5crsc156qc57mn125am5v9pi1gn06wzzhi5c1cl71cdnapva6") (features (quote (("dev" "clippy") ("default"))))))

(define-public crate-ipnetwork-0.10 (crate (name "ipnetwork") (vers "0.10.0") (deps (list (crate-dep (name "clippy") (req "^0.0.37") (optional #t) (default-features #t) (kind 0)))) (hash "0rnnkm9hlcrzd7p0j59l29lq688phzjhhf6mlypvn20gjxpa2frs") (features (quote (("dev" "clippy") ("default"))))))

(define-public crate-ipnetwork-0.11 (crate (name "ipnetwork") (vers "0.11.0") (deps (list (crate-dep (name "clippy") (req "^0.0.37") (optional #t) (default-features #t) (kind 0)))) (hash "030jij4mfwhkygwnhaf6cdjb88p400q3586rsn6n8gz1af10w42r") (features (quote (("dev" "clippy") ("default"))))))

(define-public crate-ipnetwork-0.12 (crate (name "ipnetwork") (vers "0.12.0") (deps (list (crate-dep (name "clippy") (req "^0.0.104") (optional #t) (default-features #t) (kind 0)))) (hash "0iaplvhxj6y9bhm2dz662ps22221l4j9prgy2s56ypadfvyk86by") (features (quote (("dev" "clippy") ("default"))))))

(define-public crate-ipnetwork-0.12 (crate (name "ipnetwork") (vers "0.12.1") (deps (list (crate-dep (name "clippy") (req "^0.0.104") (optional #t) (default-features #t) (kind 0)))) (hash "00fasz01q5lk0mq18zv95xya798ki9pn8m4g6ydykm0q0czy3qwj") (features (quote (("dev" "clippy") ("default"))))))

(define-public crate-ipnetwork-0.12 (crate (name "ipnetwork") (vers "0.12.2") (deps (list (crate-dep (name "clippy") (req "^0.0.104") (optional #t) (default-features #t) (kind 0)))) (hash "1rng1508rblscfb5kmqd51bcax91qv85jfbbamli04ng6rcpn1gz") (features (quote (("dev" "clippy") ("default"))))))

(define-public crate-ipnetwork-0.12 (crate (name "ipnetwork") (vers "0.12.3") (deps (list (crate-dep (name "clippy") (req "^0.0.104") (optional #t) (default-features #t) (kind 0)))) (hash "06a4mg9prry53676hasz5nqh1xm95p4ag77y31ash0nyrw4zfhp8") (features (quote (("dev" "clippy") ("default"))))))

(define-public crate-ipnetwork-0.12 (crate (name "ipnetwork") (vers "0.12.4") (deps (list (crate-dep (name "clippy") (req "^0.0.104") (optional #t) (default-features #t) (kind 0)))) (hash "0fvlcpsfsh61ip5zs8ibsggfgd6qxz22w4i3khb1x26n6bi0kn10") (features (quote (("dev" "clippy") ("default"))))))

(define-public crate-ipnetwork-0.12 (crate (name "ipnetwork") (vers "0.12.5") (deps (list (crate-dep (name "clippy") (req "^0.0.104") (optional #t) (default-features #t) (kind 0)))) (hash "0q1r0381h06ai6sa9vk3s2931k69b44xxbcgrai9v2j4hfpgkgll") (features (quote (("dev" "clippy") ("default"))))))

(define-public crate-ipnetwork-0.12 (crate (name "ipnetwork") (vers "0.12.6") (deps (list (crate-dep (name "clippy") (req "^0.0.104") (optional #t) (default-features #t) (kind 0)))) (hash "0w7k2rxh8d8n7bgk0yzpr50mac7h3rrqy1iix20560435297cbi3") (features (quote (("ipv6-methods") ("ipv6-iterator") ("dev" "clippy") ("default"))))))

(define-public crate-ipnetwork-0.12 (crate (name "ipnetwork") (vers "0.12.7") (deps (list (crate-dep (name "clippy") (req "^0.0.104") (optional #t) (default-features #t) (kind 0)))) (hash "0dmkcdpv6zy64jcsrs8jidzwx8bibxnmbqch9xlba950w88f4d11") (features (quote (("ipv6-methods") ("ipv6-iterator") ("dev" "clippy") ("default"))))))

(define-public crate-ipnetwork-0.12 (crate (name "ipnetwork") (vers "0.12.8") (deps (list (crate-dep (name "clippy") (req "^0.0.104") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde") (req ">= 0.8.0, < 2.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req ">= 0.8.0, < 2.0") (optional #t) (default-features #t) (kind 0)))) (hash "1aq1109s1vrqa9z678zfin1vihf66bdkkbhym6mqm0lhmhck2y3h") (features (quote (("with-serde" "serde" "serde_derive") ("ipv6-methods") ("ipv6-iterator") ("dev" "clippy") ("default"))))))

(define-public crate-ipnetwork-0.13 (crate (name "ipnetwork") (vers "0.13.0") (deps (list (crate-dep (name "clippy") (req "^0.0.104") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde") (req ">= 0.8.0, < 2.0") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req ">= 0.8.0, < 2.0") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 2)))) (hash "12cbcriz1x4g400y4zh7fg5v9jz0zrj3w5qa5388czy9bhphnxbv") (features (quote (("dev" "clippy") ("default"))))))

(define-public crate-ipnetwork-0.13 (crate (name "ipnetwork") (vers "0.13.1") (deps (list (crate-dep (name "clippy") (req "^0.0.104") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde") (req ">= 0.8.0, < 2.0") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req ">= 0.8.0, < 2.0") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 2)))) (hash "0770gaf64b4bpl82sw15kq54qfl3s5qvygdcdy0b1d910scqn78x") (features (quote (("dev" "clippy") ("default"))))))

(define-public crate-ipnetwork-0.14 (crate (name "ipnetwork") (vers "0.14.0") (deps (list (crate-dep (name "clippy") (req "^0.0.302") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde") (req ">= 0.8.0, < 2.0") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req ">= 0.8.0, < 2.0") (default-features #t) (kind 2)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 2)))) (hash "1laaf6h7aml156vbjh227pjq4b95w1jngj1yd6dz2rvqdz465n5k") (features (quote (("dev" "clippy") ("default"))))))

(define-public crate-ipnetwork-0.15 (crate (name "ipnetwork") (vers "0.15.0") (deps (list (crate-dep (name "clippy") (req "^0.0.302") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "criterion") (req "^0.3.0") (default-features #t) (kind 2)) (crate-dep (name "serde") (req ">= 0.8.0, < 2.0") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req ">= 0.8.0, < 2.0") (default-features #t) (kind 2)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 2)))) (hash "0h3zrz822g8ardxfbn5xd109spd12m7isalrxz5q1b9hnki64xxz") (features (quote (("dev" "clippy") ("default"))))))

(define-public crate-ipnetwork-0.15 (crate (name "ipnetwork") (vers "0.15.1") (deps (list (crate-dep (name "clippy") (req "^0.0.302") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "criterion") (req "^0.3.0") (default-features #t) (kind 2)) (crate-dep (name "serde") (req ">= 0.8.0, < 2.0") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req ">= 0.8.0, < 2.0") (default-features #t) (kind 2)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 2)))) (hash "0kdx1lfy5la37nk1wr0pmp83pgis2m81f98wm16yfx1kc7ixb7d6") (features (quote (("dev" "clippy") ("default"))))))

(define-public crate-ipnetwork-0.16 (crate (name "ipnetwork") (vers "0.16.0") (deps (list (crate-dep (name "clippy") (req "^0.0.302") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "criterion") (req "^0.3.0") (default-features #t) (kind 2)) (crate-dep (name "serde") (req ">= 0.8.0, < 2.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req ">= 0.8.0, < 2.0") (default-features #t) (kind 2)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 2)))) (hash "07nkh9djfmkkwd0phkgrv977kfmvw4hmrn1xxw4cjyx23psskv5q") (features (quote (("dev" "clippy") ("default" "serde"))))))

(define-public crate-ipnetwork-0.17 (crate (name "ipnetwork") (vers "0.17.0") (deps (list (crate-dep (name "criterion") (req "^0.3.0") (default-features #t) (kind 2)) (crate-dep (name "serde") (req "^1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1") (default-features #t) (kind 2)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 2)))) (hash "0sviri9ksb3cmhx3h0rcfy8pvpx7f0cx5ba1z87ydvf07amymhq2") (features (quote (("default" "serde"))))))

(define-public crate-ipnetwork-0.18 (crate (name "ipnetwork") (vers "0.18.0") (deps (list (crate-dep (name "criterion") (req "^0.3.4") (features (quote ("html_reports"))) (default-features #t) (kind 2)) (crate-dep (name "serde") (req "^1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1") (default-features #t) (kind 2)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 2)))) (hash "0mfkcrw8dxys6vi9bpvk2x1dyc8qi5wvrpc8jqinnm43n4wxg220") (features (quote (("default" "serde"))))))

(define-public crate-ipnetwork-0.19 (crate (name "ipnetwork") (vers "0.19.0") (deps (list (crate-dep (name "criterion") (req "^0.3.4") (features (quote ("html_reports"))) (default-features #t) (kind 2)) (crate-dep (name "serde") (req "^1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1") (default-features #t) (kind 2)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 2)))) (hash "09yil123s063qqn9n9m6fg2v9rbgzlp9lkjs40zpbwq64rhz310z") (features (quote (("default" "serde"))))))

(define-public crate-ipnetwork-0.20 (crate (name "ipnetwork") (vers "0.20.0") (deps (list (crate-dep (name "criterion") (req "^0.3.4") (features (quote ("html_reports"))) (default-features #t) (kind 2)) (crate-dep (name "does-it-json") (req "^0.0.3") (default-features #t) (kind 2)) (crate-dep (name "schemars") (req "^0.8.10") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 2)))) (hash "03hhmxyimz0800z44wl3z1ak8iw91xcnk7sgx5p5jinmx50naimz") (features (quote (("default" "serde"))))))

