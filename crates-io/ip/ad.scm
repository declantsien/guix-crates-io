(define-module (crates-io ip ad) #:use-module (crates-io))

(define-public crate-ipad-0.1 (crate (name "ipad") (vers "0.1.0") (hash "0ld3sdd7ynply4rdsanz23yxx6997k5bfm095v7jrk05rh5gc130")))

(define-public crate-ipaddr-0.0.1 (crate (name "ipaddr") (vers "0.0.1") (hash "0i43aci4as569rqr1a84wxhk1bag562a6qyc1p6q79yp2w2fyl1r") (yanked #t)))

(define-public crate-ipaddr-0.0.2 (crate (name "ipaddr") (vers "0.0.2") (hash "0v3ff4pqadkwsvnz07rphvd3qfni1h33lwrf94xhmalsiqwlzpyk") (yanked #t)))

(define-public crate-ipaddr-0.0.3 (crate (name "ipaddr") (vers "0.0.3") (hash "16alhb19b0dh3ms3pldxwq5pq48k2rj5fdq5kdv5dpgm6c5h14s6") (yanked #t)))

(define-public crate-ipaddress-0.1 (crate (name "ipaddress") (vers "0.1.0") (deps (list (crate-dep (name "num") (req "^0.1.32") (default-features #t) (kind 0)) (crate-dep (name "num-integer") (req "^0.1.32") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.1.32") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^0.1.71") (default-features #t) (kind 0)))) (hash "0iybs740akrl64wavwzich09nijgy4q0zmady25bqs7p4hbq0w7w")))

(define-public crate-ipaddress-0.1 (crate (name "ipaddress") (vers "0.1.1") (deps (list (crate-dep (name "num") (req "^0.1.32") (default-features #t) (kind 0)) (crate-dep (name "num-integer") (req "^0.1.32") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.1.32") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^0.1.71") (default-features #t) (kind 0)))) (hash "1v7ylxljcg4wpzd471xjv67bl6w4y7y6vl3bksmvswc8nz4nxs2s")))

(define-public crate-ipaddress-0.1 (crate (name "ipaddress") (vers "0.1.2") (deps (list (crate-dep (name "lazy_static") (req "^1.0.0") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2.15") (default-features #t) (kind 0)) (crate-dep (name "num") (req "^0.1.32") (default-features #t) (kind 0)) (crate-dep (name "num-integer") (req "^0.1.32") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.1.32") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^0.2.5") (default-features #t) (kind 0)))) (hash "1x85d87m93wigz34k4ccwkg8lk8mwmh6f0i1indspl54apk9ksf9")))

(define-public crate-ipaddress-0.1 (crate (name "ipaddress") (vers "0.1.3") (deps (list (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2.141") (default-features #t) (kind 0)) (crate-dep (name "num") (req "^0.4.0") (default-features #t) (kind 0)) (crate-dep (name "num-integer") (req "^0.1.45") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2.15") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.7.3") (default-features #t) (kind 0)))) (hash "1gvw8zf2f2hpd8bpr7c196bsfsl6nh2m37grdprvfssxckrvjywm")))

