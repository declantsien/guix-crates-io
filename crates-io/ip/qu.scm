(define-module (crates-io ip qu) #:use-module (crates-io))

(define-public crate-ipquery-0.1 (crate (name "ipquery") (vers "0.1.0") (deps (list (crate-dep (name "failure") (req "^0.1.5") (default-features #t) (kind 0)) (crate-dep (name "trust-dns") (req "^0.16") (default-features #t) (kind 0)))) (hash "1vq1jp5pnv7834laqm6r81j39rgdfm6i2r12cn9i8yb1r7lbp6hq")))

(define-public crate-ipquery-0.1 (crate (name "ipquery") (vers "0.1.1") (deps (list (crate-dep (name "failure") (req "^0.1.5") (default-features #t) (kind 0)) (crate-dep (name "trust-dns") (req "^0.16") (default-features #t) (kind 0)) (crate-dep (name "trust-dns-resolver") (req "^0.11.1") (default-features #t) (kind 0)))) (hash "1lhzfm8f4b5w213a6d63gw6c105fzjmidnzn9gadh1lswcydzisn")))

