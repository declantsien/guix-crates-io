(define-module (crates-io g8 -e) #:use-module (crates-io))

(define-public crate-g8-error-0.0.0 (crate (name "g8-error") (vers "0.0.0") (deps (list (crate-dep (name "tex-parser") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "1ps6vk2l5zr3a1gb6jb80vnx43clin1hmplnqhsbmg98qgls7nc3") (features (quote (("default"))))))

