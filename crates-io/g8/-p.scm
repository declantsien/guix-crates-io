(define-module (crates-io g8 -p) #:use-module (crates-io))

(define-public crate-g8-parser-0.0.0 (crate (name "g8-parser") (vers "0.0.0") (deps (list (crate-dep (name "pest") (req "^2.1.3") (default-features #t) (kind 0)))) (hash "1k5zylzsp219mbs1s5vwvdhya77dq5h3pv614602kz4n4rjdjhsi") (features (quote (("default"))))))

