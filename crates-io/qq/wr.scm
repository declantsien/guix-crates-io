(define-module (crates-io qq wr) #:use-module (crates-io))

(define-public crate-qqwry-0.1 (crate (name "qqwry") (vers "0.1.0") (deps (list (crate-dep (name "textcode") (req "^0.2.1") (default-features #t) (kind 0)))) (hash "1bk6n51k21hsaramz7mb8zwqrl4plgn57ykxizryn23w8plnj5v0")))

(define-public crate-qqwry-0.1 (crate (name "qqwry") (vers "0.1.1") (deps (list (crate-dep (name "textcode") (req "^0.2.1") (default-features #t) (kind 0)))) (hash "0na1is8spnrhylkx0sa34b11yzx27is60m8dbk8xqdngiqp1jf93")))

