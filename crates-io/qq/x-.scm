(define-module (crates-io qq x-) #:use-module (crates-io))

(define-public crate-qqx-macro-0.0.1 (crate (name "qqx-macro") (vers "0.0.1") (deps (list (crate-dep (name "quote") (req "^1.0.8") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.57") (features (quote ("full" "fold"))) (default-features #t) (kind 0)))) (hash "0ywc33vzxmq5yr14wamwdb47ab2d72izfgyz1wdmdzg8l8rwrdyz")))

(define-public crate-qqx-macro-0.0.2 (crate (name "qqx-macro") (vers "0.0.2") (deps (list (crate-dep (name "quote") (req "^1.0.8") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.57") (features (quote ("full" "fold"))) (default-features #t) (kind 0)))) (hash "1qv8igvc53xbk06d758100i1rznbwwrrvy1db0n6s9nf5x3dk641")))

(define-public crate-qqx-macro-0.0.3 (crate (name "qqx-macro") (vers "0.0.3") (deps (list (crate-dep (name "quote") (req "^1.0.8") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.57") (features (quote ("full" "fold"))) (default-features #t) (kind 0)))) (hash "0szbkjds67mx9x4lnln6ic8gjdrs57mk1n3dd4rdcidr7zvhk221")))

