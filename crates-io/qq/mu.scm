(define-module (crates-io qq mu) #:use-module (crates-io))

(define-public crate-qqmusic-rs-0.1 (crate (name "qqmusic-rs") (vers "0.1.0") (deps (list (crate-dep (name "anyhow") (req "^1.0.72") (default-features #t) (kind 2)) (crate-dep (name "reqwest") (req "^0.12.4") (default-features #t) (kind 2)) (crate-dep (name "serde") (req "^1.0.183") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.104") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.30.0") (features (quote ("full"))) (default-features #t) (kind 2)) (crate-dep (name "url") (req "^2.4.0") (default-features #t) (kind 0)))) (hash "0hgkjcc4i7lgm3r2yfp89rn1mq7d8n9lcw90b9gsg8fgayrqqsf7")))

