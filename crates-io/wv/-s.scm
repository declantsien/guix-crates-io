(define-module (crates-io wv -s) #:use-module (crates-io))

(define-public crate-wv-sys-0.1 (crate (name "wv-sys") (vers "0.1.0") (deps (list (crate-dep (name "cc") (req "^1") (default-features #t) (kind 1)) (crate-dep (name "pkg-config") (req "^0.3") (default-features #t) (kind 1)))) (hash "1gnl1yyrr4ch9vc2pbkd9fch19l6c2rnaabsrjliaa0zay6by4hm")))

(define-public crate-wv-sys-0.1 (crate (name "wv-sys") (vers "0.1.1") (deps (list (crate-dep (name "cc") (req "^1") (default-features #t) (kind 1)) (crate-dep (name "pkg-config") (req "^0.3") (default-features #t) (kind 1)))) (hash "1lhc5n3ri4dz4lbqky6z8fnmv85d9ijpz2qh1zc8xnhk108w8xk1")))

(define-public crate-wv-sys-0.1 (crate (name "wv-sys") (vers "0.1.2") (deps (list (crate-dep (name "cc") (req "^1") (default-features #t) (kind 1)) (crate-dep (name "pkg-config") (req "^0.3") (default-features #t) (kind 1)))) (hash "0jvpzkdj3sp383v98ljmpppi6wjpnc8aiwg9q3j9xk62hfm21h3y")))

(define-public crate-wv-sys-0.1 (crate (name "wv-sys") (vers "0.1.3") (deps (list (crate-dep (name "cc") (req "^1") (default-features #t) (kind 1)) (crate-dep (name "pkg-config") (req "^0.3") (default-features #t) (kind 1)))) (hash "1m574dkrp88f9im3nipp7mc1a8ifs326dly6nd5vxcky00m97k8c")))

