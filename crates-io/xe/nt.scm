(define-module (crates-io xe nt) #:use-module (crates-io))

(define-public crate-xentrace-parser-0.1 (crate (name "xentrace-parser") (vers "0.1.0") (hash "0mzn8hpdx6c57rd4q64jxzfawygyfhi5vw1xmvn4ws914h5x1avd") (yanked #t)))

(define-public crate-xentrace-parser-0.1 (crate (name "xentrace-parser") (vers "0.1.1") (hash "0asvnj6pdmh7vdcb3hlp1vfpa6iq2skgjfw5nk2mr7iwqqx9padg") (yanked #t)))

(define-public crate-xentrace-parser-0.1 (crate (name "xentrace-parser") (vers "0.1.2") (hash "092c4v2a7jdwwmckwyvcb3bnsdb6azpf3qd4msw33ijynpjs1rrs") (yanked #t)))

(define-public crate-xentrace-parser-0.1 (crate (name "xentrace-parser") (vers "0.1.3") (hash "0sr7mf6alfvq14mrqphg5s0m94xd43mklbiz2x5wy06jcbhwplbf") (yanked #t)))

(define-public crate-xentrace-parser-0.1 (crate (name "xentrace-parser") (vers "0.1.4") (hash "0c2ibm9afq0dg0wf81msmckal9bs7if79ws15x4fh64f0b3z7yd3") (yanked #t)))

(define-public crate-xentrace-parser-0.1 (crate (name "xentrace-parser") (vers "0.1.5") (hash "11xnphq9pn35p9cg2vsd0hizvaqzrhjzjmzawgvyy75vz4n6mdbr") (yanked #t)))

(define-public crate-xentrace-parser-0.1 (crate (name "xentrace-parser") (vers "0.1.6") (hash "1zwvfn15d285kwsfzw3gn9z93zvlcf4av2xghvydq6hgz3zl7zx7") (yanked #t)))

(define-public crate-xentrace-parser-0.1 (crate (name "xentrace-parser") (vers "0.1.7") (hash "09sc1ggy0p70dzs2zcx0skr9bw5giy45b39i1cnvq3kwy3njw3cs") (yanked #t)))

(define-public crate-xentrace-parser-0.2 (crate (name "xentrace-parser") (vers "0.2.0") (hash "0prkv3c30ibgs1rgyjcsr8mi9q7w30n0kc65hrcacss9ab011kvr") (yanked #t)))

(define-public crate-xentrace-parser-0.2 (crate (name "xentrace-parser") (vers "0.2.1") (hash "0kyn31bzf6vzaidc5b3q7lwwlvgc2x0j2pmdpq0zq6ydqiyngr86") (yanked #t)))

(define-public crate-xentrace-parser-0.3 (crate (name "xentrace-parser") (vers "0.3.0") (hash "0yyfgnbwycvh7h6k52cnh3hdmp7hx6w3k497ylpdzxaz2b2pckh2") (yanked #t)))

(define-public crate-xentrace-parser-0.3 (crate (name "xentrace-parser") (vers "0.3.1") (hash "181h09brkzzq2n3m9nmab2azn9jqafw4jbs3jh8xn974bawxdyr6") (yanked #t)))

(define-public crate-xentrace-parser-0.3 (crate (name "xentrace-parser") (vers "0.3.2") (hash "0jdrjwajq0hc4lz1wmkgh3a8i21b4fa9sjmsh2p2z8g44p5w5p5p") (yanked #t)))

(define-public crate-xentrace-parser-0.3 (crate (name "xentrace-parser") (vers "0.3.2-rev1") (hash "1gb7ciicpfv2vk49xs8bmw0hndvcyivkya6zj99517066bll5rca") (yanked #t)))

(define-public crate-xentrace-parser-0.3 (crate (name "xentrace-parser") (vers "0.3.2-rev2") (hash "0ll0p1as0jns6f5kk649by33qjj0998jnwj3pqnc2746xk1sqb4k") (yanked #t)))

(define-public crate-xentrace-parser-0.3 (crate (name "xentrace-parser") (vers "0.3.3") (hash "0jfcavi7dhjc0ccn4l9hr0yzpwfjyb0gmfgk2vy0j7zh2ykh4vpj") (yanked #t)))

(define-public crate-xentrace-parser-0.3 (crate (name "xentrace-parser") (vers "0.3.4") (hash "10p04wicwr8isxj72pggzdfbkvqd3sk5jri9pkv5raiwz3jpak3f") (yanked #t)))

(define-public crate-xentrace-parser-0.4 (crate (name "xentrace-parser") (vers "0.4.0") (hash "0w9dbk294yrniky2n269s3c64d02ylj8npfnsra3rxgi2gqirf7j") (yanked #t)))

(define-public crate-xentrace-parser-0.4 (crate (name "xentrace-parser") (vers "0.4.1") (hash "0r87dsya7hrpak4x558jk2jvh0mmjdqc1rjw9ka962s5846wid8x") (yanked #t)))

(define-public crate-xentrace-parser-0.5 (crate (name "xentrace-parser") (vers "0.5.0") (hash "0d4qnmjgqpbkd21ggpk5d9v646hd86dfjhvs3d0lq0qs8p68lnx1") (yanked #t)))

(define-public crate-xentrace-parser-0.6 (crate (name "xentrace-parser") (vers "0.6.0") (hash "0vfcxm4vir85grz3wclzsaskwf4f2sjd2kb4jlg3xwm1gzmsdpz6") (yanked #t)))

(define-public crate-xentrace-parser-0.6 (crate (name "xentrace-parser") (vers "0.6.1") (hash "1w47g114v9lch5291i8rk1cjz9mjdxn1082rgrmw4nddc0f9zrcn") (yanked #t)))

(define-public crate-xentrace-parser-0.7 (crate (name "xentrace-parser") (vers "0.7.0") (hash "1b0jhx1i8w9ib78ahgrh27lpabsz5hrd7s3zb3hyayps4wld8fi7") (yanked #t)))

(define-public crate-xentrace-parser-0.7 (crate (name "xentrace-parser") (vers "0.7.1") (hash "11h5hbylnjrzfi7nm30i2hibfnq75hbd7z3m1a9rgana5q7va6ml") (yanked #t)))

(define-public crate-xentrace-parser-0.7 (crate (name "xentrace-parser") (vers "0.7.2") (hash "0l7n1slcgz471ix999gv353h6fm12kw1vkk5w3i041s4wdi47rww") (yanked #t)))

(define-public crate-xentrace-parser-0.7 (crate (name "xentrace-parser") (vers "0.7.3") (hash "0wqqdbv0x9m0hnq7ydg6ifqp65mbg4srm72vcd8vh72i3q58cv4b") (yanked #t)))

(define-public crate-xentrace-parser-0.7 (crate (name "xentrace-parser") (vers "0.7.4") (hash "0zwizsmpm9cnyja9zid1k9xbpry7ncp5bqs6iwzc6wlnypghw93b") (yanked #t)))

(define-public crate-xentrace-parser-0.8 (crate (name "xentrace-parser") (vers "0.8.0") (hash "10dvcg10x57fglvxrbk59i5h9ps0g538n0v93ab0488im36zskl5") (yanked #t)))

(define-public crate-xentrace-parser-0.8 (crate (name "xentrace-parser") (vers "0.8.1") (hash "05jvn0k65y5i1jgrwy225jvdh37i7wwjnxr6xsydxhfdklfm0fky") (yanked #t)))

(define-public crate-xentrace-parser-0.9 (crate (name "xentrace-parser") (vers "0.9.0") (hash "15vfgffwshbh1av5v9c7dbligl4q82632rfppc59sj41l5c7s7s9") (yanked #t)))

(define-public crate-xentrace-parser-0.9 (crate (name "xentrace-parser") (vers "0.9.1") (hash "1jvmdi2fkxrgs748hs96fyffd8iikzss5zdiqf5n608ibpbzsb8g") (yanked #t)))

(define-public crate-xentrace-parser-0.10 (crate (name "xentrace-parser") (vers "0.10.0") (hash "0jhvbsd0pq9v2hrwp6nhk6r7gf0hmissfvzb3d24jzm32a31gqmk") (yanked #t)))

(define-public crate-xentrace-parser-0.10 (crate (name "xentrace-parser") (vers "0.10.1") (hash "0slzvwygr3sqccm16zc92sghfiwa866l8ia1rc3nkinrr3jr711c") (yanked #t)))

(define-public crate-xentrace-parser-0.10 (crate (name "xentrace-parser") (vers "0.10.2") (hash "1313pm465kazglpz7s94dn2qk4bvfiyilq4vc4fw2p5hwkvvxgl2") (yanked #t)))

(define-public crate-xentrace-parser-0.10 (crate (name "xentrace-parser") (vers "0.10.3") (hash "0psqza7931yl611ypg7rmpa068n968jp3lyg4qdgipj2pfr4215l") (yanked #t) (rust-version "1.56.1")))

(define-public crate-xentrace-parser-0.10 (crate (name "xentrace-parser") (vers "0.10.4") (hash "0d6fiwprw0idynk8j6a84mx18kg4ykqgpkrz4ghhv2gr2md8lwih") (yanked #t) (rust-version "1.56.1")))

(define-public crate-xentrace-parser-1 (crate (name "xentrace-parser") (vers "1.0.0") (hash "0z05ac5kh3l1hwskpj3kfygwiiyiblqc7ibn3a2pyv076a83ykli") (yanked #t) (rust-version "1.56.1")))

(define-public crate-xentrace-parser-1 (crate (name "xentrace-parser") (vers "1.1.0") (hash "0rcbvkbqwlbs1cqkrsqmx69n7v9r0cx2c4368c7k4qkimq8kqh13") (yanked #t) (rust-version "1.56.1")))

(define-public crate-xentrace-parser-1 (crate (name "xentrace-parser") (vers "1.2.0") (hash "1d7mqpw02igqwbr7y3fl8m28xii00cxvvn6jp75raarhjrnv7cjs") (rust-version "1.56.1")))

(define-public crate-xentrace-parser-2 (crate (name "xentrace-parser") (vers "2.0.0") (hash "12qsahm7cgd2g84bw48lgsqi51w0lf0phqbac4l1b5j3c9pydnmk") (yanked #t) (rust-version "1.65")))

(define-public crate-xentrace-parser-2 (crate (name "xentrace-parser") (vers "2.0.1") (hash "0ccv8xp2qb6pfwdvnfwf0r9ix1avhlg1w2iixmzlkkg3fnf0nkc3") (rust-version "1.65")))

(define-public crate-xentrace-parser-2 (crate (name "xentrace-parser") (vers "2.0.2") (deps (list (crate-dep (name "fxhash") (req "^0.2") (default-features #t) (kind 0)))) (hash "0r9sw7q2miz2hfxbljh8cxrw6s624sqlh3xcswgpjk5h7ygf2i8x") (rust-version "1.65")))

(define-public crate-xentrace-parser-2 (crate (name "xentrace-parser") (vers "2.1.0") (deps (list (crate-dep (name "fxhash") (req "^0.2") (default-features #t) (kind 0)))) (hash "1xay3yaa43d4d67kjhx550wd2fbmsfga19aa0x2p7kz3l6gwd6rw") (rust-version "1.65")))

