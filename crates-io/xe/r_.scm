(define-module (crates-io xe r_) #:use-module (crates-io))

(define-public crate-xer_add_one-0.0.1 (crate (name "xer_add_one") (vers "0.0.1") (hash "1wmpr7za0srnbw4sf3787k6d0jkldfssqim62cvxaw39603mv497")))

(define-public crate-xer_add_one-0.1 (crate (name "xer_add_one") (vers "0.1.0") (hash "1r4k3pmmy9lfjlmgay3nqdgzzvlwraabv9j5w54fcw9aiad95dr4")))

(define-public crate-xer_adder-0.0.1 (crate (name "xer_adder") (vers "0.0.1") (deps (list (crate-dep (name "xer_add_one") (req "^0.0.1") (default-features #t) (kind 0)))) (hash "12xk4v10bdax3k067fbj5dl8h6l03471qil05n5blm30aiw585bb")))

(define-public crate-xer_adder-0.1 (crate (name "xer_adder") (vers "0.1.0") (deps (list (crate-dep (name "xer_add_one") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "0w54psxd2wbx3ykn8dk4dxj6ximmaxwr06pp1yqvcqqs2zkqaq22")))

(define-public crate-xer_minigrep-0.0.0 (crate (name "xer_minigrep") (vers "0.0.0") (hash "111vr1mdy99k2a40k936m32dlk4prahk3pmiy4q02ywfwlmv5an7")))

