(define-module (crates-io xe nu) #:use-module (crates-io))

(define-public crate-xenu-background-0.1 (crate (name "xenu-background") (vers "0.1.1") (deps (list (crate-dep (name "clap") (req "^2") (default-features #t) (kind 0)) (crate-dep (name "picto") (req "^0.1.2") (default-features #t) (kind 0)) (crate-dep (name "xcb") (req "^0.7") (default-features #t) (kind 0)) (crate-dep (name "xcb-util") (req "^0.1") (features (quote ("image" "shm"))) (default-features #t) (kind 0)))) (hash "1nwa1lbiraqzr69saxrpn0kic0a201k9ac7sdf14fj6yi5fiqii2") (features (quote (("nightly" "picto/nightly") ("default"))))))

