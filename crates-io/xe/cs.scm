(define-module (crates-io xe cs) #:use-module (crates-io))

(define-public crate-xecs-0.2 (crate (name "xecs") (vers "0.2.6") (hash "095q8kdz6ws9sqzy9ck4q1bb3gksm0ylfj7cysll92fgra0ylx9s")))

(define-public crate-xecs-0.2 (crate (name "xecs") (vers "0.2.7") (hash "08ibr0kkgb7cb0f6i5zwc6v1lg4339sp59xmnw1a6j0x0dxq39qm")))

(define-public crate-xecs-0.2 (crate (name "xecs") (vers "0.2.8") (hash "1y2m0gs021zqsd72xj7g8qsndcb4zif8h0hbdpjs83rxdjz3qac8")))

(define-public crate-xecs-0.2 (crate (name "xecs") (vers "0.2.9") (hash "1k12cn6vlpnxkhm8w0bybn7xm8ms019ra9krg5f2axm2358bhn4r")))

(define-public crate-xecs-0.2 (crate (name "xecs") (vers "0.2.10") (hash "0m1p38d8qbmgcsyg16drj7806vzffb4zzxg8an734llq0s8m1s49")))

(define-public crate-xecs-0.2 (crate (name "xecs") (vers "0.2.11") (hash "0i2ncg0pmdbwikdp1drkq2ghxssrgbj0a6cc7lhv0wy1r08x7zhi")))

(define-public crate-xecs-0.2 (crate (name "xecs") (vers "0.2.12") (hash "0in4nv08d0wx5xp160jy5b2qg3w2xgs6b1ki4013lgrs92adc03r")))

(define-public crate-xecs-0.2 (crate (name "xecs") (vers "0.2.13") (hash "1iw6mykfjn25cqxasw0jw73rd59kxbmkzkjgsi4k0r5k4lipgvfl")))

(define-public crate-xecs-0.3 (crate (name "xecs") (vers "0.3.0") (hash "1zzy6z7nqg1ilhq26fm2294haf1s5l3by5l0d1kv0c1l3cdwnwvd")))

(define-public crate-xecs-0.3 (crate (name "xecs") (vers "0.3.1") (hash "0j9sbs4k4a0912qlyflb27dgwlv81zp0zbfdxh5m3c7wqvs1l5xj")))

(define-public crate-xecs-0.3 (crate (name "xecs") (vers "0.3.2") (hash "1y1v3z0n4afbhf85s4ap5yydm8vvs8w0mk3cb02iafh87lpm7cjj")))

(define-public crate-xecs-0.3 (crate (name "xecs") (vers "0.3.3") (hash "0ag1cm4665zkm1ccm32851xqbv2vqzp7iiibpg7hyz5qdqv3h2gn")))

(define-public crate-xecs-0.4 (crate (name "xecs") (vers "0.4.0") (deps (list (crate-dep (name "futures") (req "^0.3") (default-features #t) (kind 0)))) (hash "1jb2drvyg57xvhqvj4k4b3c9k6rqghyf4wma1hm4jqrcxfggn48i")))

(define-public crate-xecs-0.4 (crate (name "xecs") (vers "0.4.1") (deps (list (crate-dep (name "futures") (req "^0.3") (default-features #t) (kind 0)))) (hash "02jdfz35bxwdv2raflh582yxma58pra4z9qhlwsm8xsp8b6zgqnn")))

(define-public crate-xecs-0.4 (crate (name "xecs") (vers "0.4.2") (deps (list (crate-dep (name "futures") (req "^0.3") (default-features #t) (kind 0)))) (hash "03g035v31ssacijxmqkfhrwghh6mgvblkhm8xv9j6d9kcqb47yzp")))

(define-public crate-xecs-0.5 (crate (name "xecs") (vers "0.5.0") (deps (list (crate-dep (name "futures") (req "^0.3") (default-features #t) (kind 0)))) (hash "0j2bj1qmx6b1mvjsqrvay0zy45z09n9ayndkinn9hr2dqbw98z6a")))

(define-public crate-xecs-0.5 (crate (name "xecs") (vers "0.5.1") (deps (list (crate-dep (name "futures") (req "^0.3") (default-features #t) (kind 0)))) (hash "1qbfx82h6r3633psh0m738r7i02kvnwxv16x9l5ciaq1xgjn4461")))

(define-public crate-xecs-0.5 (crate (name "xecs") (vers "0.5.2") (deps (list (crate-dep (name "futures") (req "^0.3") (default-features #t) (kind 0)))) (hash "1dd7hp6g8ayrcl94s6lk2d2qfa3wxbklqwiqvvgl8db3iy30kxxp")))

(define-public crate-xecs-0.5 (crate (name "xecs") (vers "0.5.3") (deps (list (crate-dep (name "futures") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "no_deadlocks") (req "^1.3") (default-features #t) (kind 0)))) (hash "1ydh7dafsb2fq0wy109dn1p48fx817z8qjllsxxmpyvz2kacvwnf") (features (quote (("default") ("deadlocks"))))))

(define-public crate-xecs-0.5 (crate (name "xecs") (vers "0.5.4") (deps (list (crate-dep (name "futures") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "parking_lot") (req "^0.12") (default-features #t) (kind 0)))) (hash "0l8cjj25hf5acbf6r1jvwl7x77n9jmizxfgnrbk2qc3ig3zxg2jv")))

(define-public crate-xecs-0.5 (crate (name "xecs") (vers "0.5.5") (deps (list (crate-dep (name "futures") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "parking_lot") (req "^0.12") (default-features #t) (kind 0)))) (hash "1dv56faw83qzpv7g54b8r3mjzlcbkbfxigf19cfw1qdxcch6k6df")))

(define-public crate-xecs-0.5 (crate (name "xecs") (vers "0.5.6") (deps (list (crate-dep (name "futures") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "parking_lot") (req "^0.12") (default-features #t) (kind 0)))) (hash "11m78ax0nvya7zivvqsx1kaxd06xkdacl2bk9gnn09n5044s4m2l")))

(define-public crate-xecs-0.5 (crate (name "xecs") (vers "0.5.7") (deps (list (crate-dep (name "futures") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "parking_lot") (req "^0.12") (default-features #t) (kind 0)))) (hash "1injk09icm0ncri9cwg29j9daswn3dn2z1dwy7qxbwwcdrrzcibq")))

(define-public crate-xecs-0.5 (crate (name "xecs") (vers "0.5.8") (deps (list (crate-dep (name "parking_lot") (req "^0.12") (default-features #t) (kind 0)) (crate-dep (name "tokio-stream") (req "^0.1") (default-features #t) (kind 0)))) (hash "11ffy69lxnq72xi07wvfzgib274vlgi95h8ffbzqa6qx1haby77j")))

(define-public crate-xecs-0.5 (crate (name "xecs") (vers "0.5.9") (deps (list (crate-dep (name "futures") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "parking_lot") (req "^0.12") (default-features #t) (kind 0)))) (hash "0007ya8k1xvdhh3f0jjf3w0zyipsjb6xjfdv5l1i6ldqy3djh55b")))

(define-public crate-xecs-0.5 (crate (name "xecs") (vers "0.5.10") (deps (list (crate-dep (name "futures") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "parking_lot") (req "^0.12") (default-features #t) (kind 0)))) (hash "05ch02sbbgccdx1iw7yj7pmr96yrzsrv45zj4s54gxv94z5vz5zj")))

(define-public crate-xecs_derive-0.5 (crate (name "xecs_derive") (vers "0.5.6") (deps (list (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "xecs") (req "^0.5.6") (default-features #t) (kind 0)))) (hash "0s72pw1ldj6q9b95pxn6rirkahrk2y78paayf7hrc8s2xjq6vc1b")))

