(define-module (crates-io xe rr) #:use-module (crates-io))

(define-public crate-xerr-0.1 (crate (name "xerr") (vers "0.1.0") (deps (list (crate-dep (name "tracing") (req "^0.1.37") (default-features #t) (kind 0)))) (hash "15wirv4gmbb88k8yvcrmbc7w964xsrwan6j7iv54bh2zlgky9jip")))

(define-public crate-xerr-0.1 (crate (name "xerr") (vers "0.1.1") (deps (list (crate-dep (name "tracing") (req "^0.1.40") (default-features #t) (kind 0)))) (hash "1xs5az2ba0sjvb42igzb6gkws2gh79b4afymfbg8j1glzrjz6gxb")))

(define-public crate-xerr-0.1 (crate (name "xerr") (vers "0.1.2") (deps (list (crate-dep (name "tracing") (req "^0.1.40") (default-features #t) (kind 0)))) (hash "03zaqlrwp3hxv84h1dj0783k399wvm4hpm5fiyy30lwv7jkmk6wc")))

(define-public crate-xerr-0.1 (crate (name "xerr") (vers "0.1.3") (deps (list (crate-dep (name "tracing") (req "^0.1.40") (default-features #t) (kind 0)))) (hash "0jywa858snsfwvkk9f0skday7ly5f4d2ipglbns74whj8fqqb9zx")))

(define-public crate-xerr-0.1 (crate (name "xerr") (vers "0.1.4") (deps (list (crate-dep (name "tracing") (req "^0.1.40") (default-features #t) (kind 0)))) (hash "0bl2fpn1bkanscmqqym2771k2pxgzw8fm04w2ypixlvka2ambdc0")))

(define-public crate-xerr-0.1 (crate (name "xerr") (vers "0.1.5") (deps (list (crate-dep (name "tracing") (req "^0.1.40") (default-features #t) (kind 0)))) (hash "0wmhgsnaj7jcmrk7n0jbwzy9783wsn3hx4gyni2njrnqs8vlpfca")))

(define-public crate-xerr-0.1 (crate (name "xerr") (vers "0.1.6") (deps (list (crate-dep (name "tracing") (req "^0.1.40") (default-features #t) (kind 0)))) (hash "1ws5vzgcnmg2sqhh79117iw4kr28kvi95gszra4y07mmrj7phr0b")))

(define-public crate-xerr-0.1 (crate (name "xerr") (vers "0.1.7") (deps (list (crate-dep (name "tracing") (req "^0.1.40") (default-features #t) (kind 0)))) (hash "0p15rnyq56d4h4wzb327xc8ihffcicxdwwkb2jdskb6snv2rl1wl")))

(define-public crate-xerr-0.1 (crate (name "xerr") (vers "0.1.9") (deps (list (crate-dep (name "anyhow") (req "^1.0.79") (default-features #t) (kind 0)) (crate-dep (name "tracing") (req "^0.1.40") (default-features #t) (kind 0)))) (hash "0z03lyfd5cairqbzr92f6mq6ph9ycg3sgzfjrjw7ddxw601qgrm5")))

(define-public crate-xerr-0.1 (crate (name "xerr") (vers "0.1.10") (deps (list (crate-dep (name "anyhow") (req "^1.0.79") (default-features #t) (kind 0)) (crate-dep (name "tracing") (req "^0.1.40") (default-features #t) (kind 0)))) (hash "0vy0hxdz09d20zv5zqn1awdrackmvx376z5i95qb2m8mnqpqgkxy")))

(define-public crate-xerr-0.1 (crate (name "xerr") (vers "0.1.11") (deps (list (crate-dep (name "anyhow") (req "^1.0.79") (default-features #t) (kind 0)) (crate-dep (name "tracing") (req "^0.1.40") (default-features #t) (kind 0)))) (hash "0sy0gpyran5yvvsmhiks9jvhhba6mcs248jq2j1dhvi18vc6k2xf")))

(define-public crate-xerror-0.0.1 (crate (name "xerror") (vers "0.0.1") (hash "0rhyb529ccsqs3h48ckzqwdb3p8zgipi7d40zrpizvyxfl4bg9jw")))

(define-public crate-xerror-derive-0.0.1 (crate (name "xerror-derive") (vers "0.0.1") (hash "18170w8782y35ppar34xh8nnl97wa5xa0bb4ans3rgw4gkj0mlia")))

