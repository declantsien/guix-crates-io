(define-module (crates-io xe ni) #:use-module (crates-io))

(define-public crate-xenia-0.1 (crate (name "xenia") (vers "0.1.0") (hash "06n8igk1rh97m79yknlixgawaax3b55jld8ncpmam6ln9yl1y9xf") (yanked #t)))

(define-public crate-xenia-0.0.1 (crate (name "xenia") (vers "0.0.1") (hash "01vmqkhniqqmhj0lmmpv41bx5b354ix6iyx4m2b28fsghji17afm")))

(define-public crate-xenia-0.0.2 (crate (name "xenia") (vers "0.0.2") (deps (list (crate-dep (name "bitflags") (req "^2.5.0") (default-features #t) (kind 0)) (crate-dep (name "linux-raw-sys") (req "^0.6.4") (features (quote ("errno" "general" "system" "no_std"))) (kind 0)))) (hash "1pksch14c968g5pf05fiwdlg52qqswbmam8aajj39bibb5s367mc")))

