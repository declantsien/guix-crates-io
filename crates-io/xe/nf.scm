(define-module (crates-io xe nf) #:use-module (crates-io))

(define-public crate-xenforeignmemory-0.1 (crate (name "xenforeignmemory") (vers "0.1.0") (deps (list (crate-dep (name "failure") (req "^0.1.7") (default-features #t) (kind 0)) (crate-dep (name "libloading") (req "^0.6.1") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.8") (default-features #t) (kind 0)) (crate-dep (name "xenforeignmemory-sys") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "0gwh4f5ffric4w3njlgp1acnb158pqwjjnz2va4np5swkcdmczcj")))

(define-public crate-xenforeignmemory-0.2 (crate (name "xenforeignmemory") (vers "0.2.1") (deps (list (crate-dep (name "libloading") (req "^0.6.1") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.8") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "xenforeignmemory-sys") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "0yxiib2y01w471z4d0kphy63kfca7csylswpnbkbc1jfmm7acpl0")))

(define-public crate-xenforeignmemory-0.2 (crate (name "xenforeignmemory") (vers "0.2.2") (deps (list (crate-dep (name "libloading") (req "^0.7.0") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.8") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "xenforeignmemory-sys") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "02dlvrz6y1hqxgycpf97v98xfn7jjbgyb030jd20m4i86n9lcqxs")))

(define-public crate-xenforeignmemory-0.2 (crate (name "xenforeignmemory") (vers "0.2.3") (deps (list (crate-dep (name "libloading") (req "^0.7.0") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.8") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "xenforeignmemory-sys") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "0xvckqqsm4s0dah3yydj6fy0pplfd237a6azf3pmx0h0adaiacri")))

(define-public crate-xenforeignmemory-sys-0.1 (crate (name "xenforeignmemory-sys") (vers "0.1.0") (deps (list (crate-dep (name "bindgen") (req "^0.49.2") (default-features #t) (kind 1)))) (hash "0r077kml50dsrb5v4yfrlg1wslkh6llk6s4pp1jsxn3f00a70wpq") (links "xenforeignmemory")))

