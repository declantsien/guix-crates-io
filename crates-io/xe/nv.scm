(define-module (crates-io xe nv) #:use-module (crates-io))

(define-public crate-xenv-0.1 (crate (name "xenv") (vers "0.1.0") (deps (list (crate-dep (name "dirs") (req "^3.0.1") (default-features #t) (kind 0)) (crate-dep (name "eyre") (req "^0.6.0") (default-features #t) (kind 0)))) (hash "0gnhmj61gz4d0h9nynw4m3y3k0ff522ylndszy7j2vgarz16mxxd")))

(define-public crate-xenv-0.2 (crate (name "xenv") (vers "0.2.0") (deps (list (crate-dep (name "eyre") (req "^0.6.0") (default-features #t) (kind 0)))) (hash "0nf9qzrwmzgwkkw84rfvxzgkr0cvnsn7ysk3syh8gplj86f3d4lc")))

(define-public crate-xenvmevent-sys-0.1 (crate (name "xenvmevent-sys") (vers "0.1.3") (deps (list (crate-dep (name "bindgen") (req "^0.52.0") (default-features #t) (kind 1)))) (hash "09pn2m0n5c69xa184xi5zwma69adgkk05g6rc4wxv5zmaalyzgw3")))

