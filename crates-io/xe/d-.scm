(define-module (crates-io xe d-) #:use-module (crates-io))

(define-public crate-xed-sys-0.1 (crate (name "xed-sys") (vers "0.1.0") (deps (list (crate-dep (name "bindgen") (req "^0.26.3") (default-features #t) (kind 1)))) (hash "0riaxshj3922a7xhxg24avyb8d7mrrwfhyibkbi4l0j5hqwl3qvm")))

(define-public crate-xed-sys-0.1 (crate (name "xed-sys") (vers "0.1.1") (deps (list (crate-dep (name "bindgen") (req "^0.26.3") (default-features #t) (kind 1)))) (hash "1i61bzxfingvnp2pl5vi50kfna6c5703c5jgndvb3wdzmjc73rw6")))

(define-public crate-xed-sys-0.2 (crate (name "xed-sys") (vers "0.2.0") (deps (list (crate-dep (name "bindgen") (req "^0.43") (default-features #t) (kind 1)) (crate-dep (name "fs_extra") (req "^1.1") (default-features #t) (kind 1)) (crate-dep (name "num_cpus") (req "^1.8") (default-features #t) (kind 1)) (crate-dep (name "target-lexicon") (req "^0.2") (default-features #t) (kind 1)))) (hash "11z8vi237bjkwxgk66v1sknkzkzwbcxgqlvqp0v0lsr3hw17cpbb") (yanked #t)))

(define-public crate-xed-sys-0.2 (crate (name "xed-sys") (vers "0.2.1") (deps (list (crate-dep (name "bindgen") (req "^0.43") (default-features #t) (kind 1)) (crate-dep (name "fs_extra") (req "^1.1") (default-features #t) (kind 1)) (crate-dep (name "num_cpus") (req "^1.8") (default-features #t) (kind 1)) (crate-dep (name "target-lexicon") (req "^0.2") (default-features #t) (kind 1)))) (hash "0xhrxvc3p12hjmigrpv3w9jg4av07w4l7zdsg34p3qg7k6xh7qva") (yanked #t)))

(define-public crate-xed-sys-0.2 (crate (name "xed-sys") (vers "0.2.2") (deps (list (crate-dep (name "bindgen") (req "^0.43") (default-features #t) (kind 1)) (crate-dep (name "fs_extra") (req "^1.1") (default-features #t) (kind 1)) (crate-dep (name "num_cpus") (req "^1.8") (default-features #t) (kind 1)) (crate-dep (name "target-lexicon") (req "^0.2") (default-features #t) (kind 1)))) (hash "16l0rvwd4ha88qjvqrahfxmymbzi5wg51js4q87bgvyxkwy49xgh")))

(define-public crate-xed-sys-0.2 (crate (name "xed-sys") (vers "0.2.3") (deps (list (crate-dep (name "bindgen") (req "^0.43") (default-features #t) (kind 1)) (crate-dep (name "fs_extra") (req "^1.1") (default-features #t) (kind 1)) (crate-dep (name "num_cpus") (req "^1.8") (default-features #t) (kind 1)) (crate-dep (name "target-lexicon") (req "^0.2") (default-features #t) (kind 1)))) (hash "1zhnh6mqd6d4cpyzcxfdsa7ibp2yhksrwl96v0r4shm1hdwrv87w")))

(define-public crate-xed-sys-0.2 (crate (name "xed-sys") (vers "0.2.4") (deps (list (crate-dep (name "bindgen") (req "^0.51") (default-features #t) (kind 1)) (crate-dep (name "target-lexicon") (req "^0.8") (default-features #t) (kind 1)))) (hash "1rdhvll3g7iigqay3i97pkl0x2zi81azz2b30ilsj8ql1dfl6m9y")))

(define-public crate-xed-sys-0.3 (crate (name "xed-sys") (vers "0.3.0") (deps (list (crate-dep (name "bindgen") (req "^0.54") (default-features #t) (kind 1)) (crate-dep (name "target-lexicon") (req "^0.10") (default-features #t) (kind 1)))) (hash "07b8vngjxz09wkg2yyrg9ksz4dx5py65m2h95mlayc6zacqcjpbm")))

(define-public crate-xed-sys-0.4 (crate (name "xed-sys") (vers "0.4.0") (deps (list (crate-dep (name "bindgen") (req "^0.58") (default-features #t) (kind 1)) (crate-dep (name "target-lexicon") (req "^0.12") (default-features #t) (kind 1)))) (hash "1kkggjvs6n0phwp0wyxyl304cpcg82xv7fcl9bi570m3q5bmx64k")))

(define-public crate-xed-sys-0.5 (crate (name "xed-sys") (vers "0.5.0+xed-2023.12.19") (deps (list (crate-dep (name "bindgen") (req "^0.69") (features (quote ("experimental"))) (optional #t) (default-features #t) (kind 1)) (crate-dep (name "cc") (req "^1.0") (default-features #t) (kind 1)) (crate-dep (name "target-lexicon") (req "^0.12") (default-features #t) (kind 1)))) (hash "0z87xgxg5jah5y5wgjhjvrsaa6nl7i4z84i19i50r8k3f6ncybsc") (v 2) (features2 (quote (("bindgen" "dep:bindgen")))) (rust-version "1.64")))

(define-public crate-xed-sys2-0.1 (crate (name "xed-sys2") (vers "0.1.0") (deps (list (crate-dep (name "bindgen") (req "^0.69.1") (features (quote ("experimental"))) (default-features #t) (kind 1)) (crate-dep (name "cc") (req "^1.0.83") (default-features #t) (kind 1)))) (hash "07nd1ka1xpwsb96pp3ddnn17wfrmj4d5y9nmkzd1w6v09msw5pis") (links "xed")))

(define-public crate-xed-sys2-0.1 (crate (name "xed-sys2") (vers "0.1.1") (deps (list (crate-dep (name "bindgen") (req "^0.69.1") (features (quote ("experimental"))) (default-features #t) (kind 1)) (crate-dep (name "cc") (req "^1.0.83") (default-features #t) (kind 1)))) (hash "0d9nfb19ng3d5b1r2cgd3wfkkkfc0lcj6n8z7ng1rhzlgcagcflq") (links "xed")))

