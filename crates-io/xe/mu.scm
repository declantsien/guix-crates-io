(define-module (crates-io xe mu) #:use-module (crates-io))

(define-public crate-xemurs-0.0.1 (crate (name "xemurs") (vers "0.0.1") (deps (list (crate-dep (name "clap") (req "^4.2.7") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "1jxfpl4dxni5yr7qsgxv02pdibi12yv489c7jfavwcpqkrr883sp")))

