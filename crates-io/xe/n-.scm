(define-module (crates-io xe n-) #:use-module (crates-io))

(define-public crate-xen-sys-0.0.0 (crate (name "xen-sys") (vers "0.0.0-pre") (hash "11wvrggl6nj8f3k5vncs3grn0qlacp3was1rw3nd6jmiqv7m88z5")))

(define-public crate-xen-sys-0.0.0 (crate (name "xen-sys") (vers "0.0.0-pre1") (hash "0p1ikkw8hays15n64c79jr70197f8i8n00kzcgm0sn4sy531qbmj")))

(define-public crate-xen-sys-0.0.0 (crate (name "xen-sys") (vers "0.0.0-pre2") (deps (list (crate-dep (name "cty") (req "=0.2.0") (default-features #t) (kind 0)))) (hash "1gqz2c6ib0x177rrkhz55wngg5rf0ngy18rliqmirippgv8h80pa")))

