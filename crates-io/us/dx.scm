(define-module (crates-io us dx) #:use-module (crates-io))

(define-public crate-usdx_parser-0.1 (crate (name "usdx_parser") (vers "0.1.0") (deps (list (crate-dep (name "anyhow") (req "^1.0.56") (default-features #t) (kind 0)) (crate-dep (name "tracing") (req "^0.1.32") (default-features #t) (kind 0)))) (hash "19vz7g2523cbyx353kg7r2fqf5254q8fa93naarqnyzibxzc731z")))

(define-public crate-usdx_parser-0.2 (crate (name "usdx_parser") (vers "0.2.0") (deps (list (crate-dep (name "anyhow") (req "^1.0.56") (default-features #t) (kind 0)) (crate-dep (name "tracing") (req "^0.1.32") (default-features #t) (kind 0)))) (hash "09cx4hvbccnjhl8nnik97mfv7n7mm0swq3qri9h5b20nm35pc50d")))

(define-public crate-usdx_parser-0.2 (crate (name "usdx_parser") (vers "0.2.1") (deps (list (crate-dep (name "anyhow") (req "^1.0.56") (default-features #t) (kind 0)) (crate-dep (name "tracing") (req "^0.1.32") (default-features #t) (kind 0)))) (hash "1anmxxinbcl50whgzxz97q381sly5d1dp10jkr213jjl0ykd2rjg")))

(define-public crate-usdx_parser-0.2 (crate (name "usdx_parser") (vers "0.2.2") (deps (list (crate-dep (name "anyhow") (req "^1.0.56") (default-features #t) (kind 0)) (crate-dep (name "tracing") (req "^0.1.32") (default-features #t) (kind 0)))) (hash "00yvck352fl7pcpnfy6fwyr9k6xvjbspswj538by58h3lk9ghxix")))

