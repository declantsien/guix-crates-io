(define-module (crates-io us d-) #:use-module (crates-io))

(define-public crate-usd-cpp-0.1 (crate (name "usd-cpp") (vers "0.1.0") (hash "1sx7bv645qr7kjxlw5vq58l2zl8ymgn6dsr5i10jzn3271vi6k05")))

(define-public crate-usd-cpp-0.1 (crate (name "usd-cpp") (vers "0.1.1") (hash "06a5rvj18fmf1yq84magkvg7ljn7jqhmbxh3ikw8z5jrznn98c8z")))

(define-public crate-usd-sys-0.0.0 (crate (name "usd-sys") (vers "0.0.0") (hash "0chdm2x0hamnczfj7m5nhp6v442vkr63l1mv8d4bzma6wxcpyyhm")))

