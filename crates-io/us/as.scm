(define-module (crates-io us as) #:use-module (crates-io))

(define-public crate-usask_cba_calc-0.2 (crate (name "usask_cba_calc") (vers "0.2.1") (deps (list (crate-dep (name "serde_json") (req "^1.0.107") (default-features #t) (kind 0)))) (hash "1d5p090xrmk71i830zg4qbfbg48bx1vfsa6f2dxyg32izr23a8wv")))

(define-public crate-usask_cba_calc-0.2 (crate (name "usask_cba_calc") (vers "0.2.3") (deps (list (crate-dep (name "serde_json") (req "^1.0.107") (default-features #t) (kind 0)))) (hash "0njqrlvp13lvgimnk7hqfmz02199x3l9xnfn9c6gwlk3bfvzv61z")))

(define-public crate-usask_cba_calc-0.2 (crate (name "usask_cba_calc") (vers "0.2.4") (deps (list (crate-dep (name "serde_json") (req "^1.0.107") (default-features #t) (kind 0)))) (hash "1mirfdh279s2b50r1vznb6iarnr2wdmxwd288piwxr5ys68qlwdk")))

(define-public crate-usask_cba_calc-0.2 (crate (name "usask_cba_calc") (vers "0.2.6") (deps (list (crate-dep (name "ansi_term") (req "^0.12.1") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.107") (default-features #t) (kind 0)))) (hash "1yv4qwx5g38b4jk876m6vd7vsh5xp3n46lh58b6wbzimqin2yw48")))

(define-public crate-usask_cba_calc-0.2 (crate (name "usask_cba_calc") (vers "0.2.7") (deps (list (crate-dep (name "ansi_term") (req "^0.12.1") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.107") (default-features #t) (kind 0)))) (hash "0djbkk9pys95jfhvrhba22cgii95b6ymhmqzsrxz8iawcbkkccb3")))

(define-public crate-usask_cba_calc-0.2 (crate (name "usask_cba_calc") (vers "0.2.8") (deps (list (crate-dep (name "ansi_term") (req "^0.12.1") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.107") (default-features #t) (kind 0)))) (hash "08l5wwzdhwbrj83qy8ckfq1aky2i38d69hhmdhjmh1qh7ddkdcg6")))

(define-public crate-usask_cba_calc-0.2 (crate (name "usask_cba_calc") (vers "0.2.9") (deps (list (crate-dep (name "ansi_term") (req "^0.12.1") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.107") (default-features #t) (kind 0)))) (hash "0pkgs2f235r5wv5gcvwxgia0ddlanh01z0h9cmgr5vjmzja97laz")))

(define-public crate-usask_cba_calc-0.3 (crate (name "usask_cba_calc") (vers "0.3.0") (deps (list (crate-dep (name "ansi_term") (req "^0.12.1") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.107") (default-features #t) (kind 0)))) (hash "07qyq4mwlgxvg6gidwh1j2yw6bqaf80pbhh8idn578r2rkvxh09n")))

(define-public crate-usask_cba_calc-0.3 (crate (name "usask_cba_calc") (vers "0.3.1") (deps (list (crate-dep (name "ansi_term") (req "^0.12.1") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.107") (default-features #t) (kind 0)))) (hash "1816m2rr5l3nvhynncfd0slsaq7iw7rg3vg23acfgn4l7l7ni2g7")))

(define-public crate-usask_cba_calc-0.3 (crate (name "usask_cba_calc") (vers "0.3.2") (deps (list (crate-dep (name "ansi_term") (req "^0.12.1") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.107") (default-features #t) (kind 0)))) (hash "05fx300qfrasdvil7pjbnkbdf1h79n2bw8qap4x0chiw9yzgrrvf")))

(define-public crate-usask_cba_calc-0.3 (crate (name "usask_cba_calc") (vers "0.3.3") (deps (list (crate-dep (name "ansi_term") (req "^0.12.1") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.107") (default-features #t) (kind 0)))) (hash "14n8cn592pcxdl010grd87fhn413k3p8jbjkykdxl604lgj03c9i")))

(define-public crate-usask_cba_calc-0.3 (crate (name "usask_cba_calc") (vers "0.3.4") (deps (list (crate-dep (name "ansi_term") (req "^0.12.1") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.107") (default-features #t) (kind 0)))) (hash "01sg1llr7ml0p1849jwqixlph1fdy482pfgzdfx8vvzd6iqqgiww")))

(define-public crate-usask_cba_calc-0.3 (crate (name "usask_cba_calc") (vers "0.3.5") (deps (list (crate-dep (name "ansi_term") (req "^0.12.1") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.107") (default-features #t) (kind 0)))) (hash "1mm9315frg4dkk3y75g767lx237mgzlva16q94046zajjmc5c0qb")))

(define-public crate-usask_cba_calc-0.3 (crate (name "usask_cba_calc") (vers "0.3.6") (deps (list (crate-dep (name "ansi_term") (req "^0.12.1") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.107") (default-features #t) (kind 0)))) (hash "1y14imsym3zrsn7mavj3v8s5gr93abc770gf9p6ivr62c0gav5c1")))

(define-public crate-usask_cba_calc-0.4 (crate (name "usask_cba_calc") (vers "0.4.0") (deps (list (crate-dep (name "ansi_term") (req "^0.12.1") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.107") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.33.0") (features (quote ("io-std" "time" "rt-multi-thread" "macros" "io-util"))) (default-features #t) (kind 0)))) (hash "1va3xvf8wcavb3qcqha73vkvl5dg90hi0jp2cqnfv45cz3fy62s9")))

(define-public crate-usask_cba_calc-0.4 (crate (name "usask_cba_calc") (vers "0.4.1") (deps (list (crate-dep (name "ansi_term") (req "^0.12.1") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.107") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.33.0") (features (quote ("io-std" "time" "rt-multi-thread" "macros" "io-util"))) (default-features #t) (kind 0)))) (hash "17jgbl4l73f2yfrg12q96wm1j8lg96maybf0lxim0aqwynxcd85g")))

(define-public crate-usask_cba_calc-0.4 (crate (name "usask_cba_calc") (vers "0.4.2") (deps (list (crate-dep (name "ansi_term") (req "^0.12.1") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.107") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.33.0") (features (quote ("io-std" "time" "rt-multi-thread" "macros" "io-util"))) (default-features #t) (kind 0)))) (hash "140z35n7834s9zbsz5w1pwwjhq4cl7qk5fjvvysgwanj551kqnb4")))

