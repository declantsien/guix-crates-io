(define-module (crates-io us dz) #:use-module (crates-io))

(define-public crate-usdz-0.0.0 (crate (name "usdz") (vers "0.0.0") (hash "03b4glqhmd875q2y2v5qxk6wqkzgn6inpp65bkpmyjqgq0lbzvm6")))

(define-public crate-usdz-0.0.1 (crate (name "usdz") (vers "0.0.1") (deps (list (crate-dep (name "nom") (req "^7.1.1") (default-features #t) (kind 0)))) (hash "0j13fm3ykkdd3gq6rva8jq2fy3zqikq24pcx7lcjmbb3xc8cgan0")))

