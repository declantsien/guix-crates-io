(define-module (crates-io us ps) #:use-module (crates-io))

(define-public crate-usps-0.1 (crate (name "usps") (vers "0.1.0") (hash "04d05378bypyki6zcirbhhjrmxf2wd0jmzbjcql6ws3f9hwyxfgv")))

(define-public crate-usps-api-0.0.1 (crate (name "usps-api") (vers "0.0.1") (deps (list (crate-dep (name "reqwest") (req "^0.9.19") (default-features #t) (kind 0)) (crate-dep (name "roxmltree") (req "^0.7.0") (default-features #t) (kind 0)))) (hash "12l7ljmq95rqb7a9l8vhjzas3197w37mryjgga3zqf4zf8bcp272")))

