(define-module (crates-io us e_) #:use-module (crates-io))

(define-public crate-use_cargo-0.1 (crate (name "use_cargo") (vers "0.1.0") (hash "00n45wd2x0arwvzax6ymw4z36prhcjayxmxjzlsrxyda6zf86agm")))

(define-public crate-use_crates-0.1 (crate (name "use_crates") (vers "0.1.0") (hash "1910d0bb4k7hrazj78aclxbkd9m5lgrdsfxw5p6arxj6v3mdr7d8")))

(define-public crate-use_css-0.1 (crate (name "use_css") (vers "0.1.0") (deps (list (crate-dep (name "proc-macro2") (req "^1.0.52") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.26") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.8.1") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2.0.3") (default-features #t) (kind 0)))) (hash "0bclfk04lasr2g6fbhf0yyiphd3p1bj8nq55cwwplz39iygrv8zp") (yanked #t) (rust-version "1.67.1")))

(define-public crate-use_css-0.2 (crate (name "use_css") (vers "0.2.0") (deps (list (crate-dep (name "stylist") (req "^0.12.0") (features (quote ("yew" "parser"))) (default-features #t) (kind 0)) (crate-dep (name "use_css_procmacros") (req "0.*") (default-features #t) (kind 0)))) (hash "0mwlcmyyhp7mckwa76b683i8h0ldn3hnj4cawkg2lmfl11jvxgjc")))

(define-public crate-use_css_procmacros-0.2 (crate (name "use_css_procmacros") (vers "0.2.0") (deps (list (crate-dep (name "proc-macro2") (req "^1.0.52") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.26") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2.0.3") (default-features #t) (kind 0)))) (hash "1ins610ib308gcxsr3r6fj923g8m2yz6iywn1sk6mbply4dncgj5")))

