(define-module (crates-io us ze) #:use-module (crates-io))

(define-public crate-usze-0.1 (crate (name "usze") (vers "0.1.0") (deps (list (crate-dep (name "atty") (req "^0.2.14") (default-features #t) (kind 0)) (crate-dep (name "insta") (req "^1.39.0") (default-features #t) (kind 2)))) (hash "01ycfzx5458sqhrbqq5b8xxp5l0w7iwgbi18pp1cbkjgxmill1x7")))

