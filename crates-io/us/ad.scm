(define-module (crates-io us ad) #:use-module (crates-io))

(define-public crate-usaddress-0.1 (crate (name "usaddress") (vers "0.1.0") (deps (list (crate-dep (name "crfs") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.4") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.5") (default-features #t) (kind 0)))) (hash "1h9bp1fndqpxk9sw88664rd6ydvv6844700w9pv896jrznfqfc4d")))

