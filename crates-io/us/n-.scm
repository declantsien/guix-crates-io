(define-module (crates-io us n-) #:use-module (crates-io))

(define-public crate-usn-journal-0.1 (crate (name "usn-journal") (vers "0.1.0") (deps (list (crate-dep (name "thiserror") (req "^1.0.21") (default-features #t) (kind 0)) (crate-dep (name "winapi") (req "^0.3.9") (default-features #t) (kind 0)))) (hash "02rfxh0vchphs4g0adfnkflaqb0853wk8sn9l67iwb9wmqknhh4l")))

(define-public crate-usn-journal-watcher-0.1 (crate (name "usn-journal-watcher") (vers "0.1.0") (deps (list (crate-dep (name "windows") (req "^0.29.0") (features (quote ("alloc" "Win32_Foundation" "Win32_System_Time" "Win32_System_SystemInformation" "Win32_Security" "Win32_System_IO" "Win32_Storage_FileSystem" "Win32_System_Ioctl"))) (default-features #t) (kind 0)))) (hash "14s7404vsrhknwh0v1db6wf9yxaj6qywjg8pp3wgg7vnc1bj9cpc") (yanked #t)))

(define-public crate-usn-journal-watcher-0.1 (crate (name "usn-journal-watcher") (vers "0.1.1") (deps (list (crate-dep (name "windows") (req "^0.29.0") (features (quote ("alloc" "Win32_Foundation" "Win32_System_Time" "Win32_System_SystemInformation" "Win32_Security" "Win32_System_IO" "Win32_Storage_FileSystem" "Win32_System_Ioctl"))) (default-features #t) (kind 0)))) (hash "10zhsfj0p9vi4kcffc7p5qgwjklg1askfmwhpbp34n4lzlcjav5c") (yanked #t)))

(define-public crate-usn-journal-watcher-0.1 (crate (name "usn-journal-watcher") (vers "0.1.2") (deps (list (crate-dep (name "windows") (req "^0.29.0") (features (quote ("alloc" "Win32_Foundation" "Win32_System_Time" "Win32_System_SystemInformation" "Win32_Security" "Win32_System_IO" "Win32_Storage_FileSystem" "Win32_System_Ioctl"))) (default-features #t) (kind 0)))) (hash "09ml2aql68nzbh8lf56c25g255h2v92i8v7afb9j9m2cc2nw491j") (yanked #t)))

(define-public crate-usn-journal-watcher-0.1 (crate (name "usn-journal-watcher") (vers "0.1.3") (deps (list (crate-dep (name "windows") (req "^0.29.0") (features (quote ("alloc" "Win32_Foundation" "Win32_System_Time" "Win32_System_SystemInformation" "Win32_Security" "Win32_System_IO" "Win32_Storage_FileSystem" "Win32_System_Ioctl"))) (default-features #t) (kind 0)))) (hash "045xjbnvhyadzy6yh3wwlkbdp31md9y7sqb39kskf9z1kgv8xsm9") (yanked #t)))

