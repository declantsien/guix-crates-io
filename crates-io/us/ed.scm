(define-module (crates-io us ed) #:use-module (crates-io))

(define-public crate-usedby-0.1 (crate (name "usedby") (vers "0.1.0") (deps (list (crate-dep (name "procfs") (req "^0.16.0") (default-features #t) (kind 0)))) (hash "02vwgc9my31pqncnhj50k4c5dlv2d78nn1zyh27w8q0dbxiy0h2y")))

(define-public crate-usedby-0.1 (crate (name "usedby") (vers "0.1.1") (deps (list (crate-dep (name "procfs") (req "^0.16.0") (default-features #t) (kind 0)))) (hash "0pkm80sbv9g7sbsr9vryndzgnhi2jw8nf881zs71zr17w793ral5")))

(define-public crate-usedby-0.1 (crate (name "usedby") (vers "0.1.2") (deps (list (crate-dep (name "procfs") (req "^0.16.0") (default-features #t) (kind 0)))) (hash "0a0yi9wjb3dzpc8kd68sdpxdgfx5wq0b0ckbwpbh75hf9yp7qmfw")))

(define-public crate-usedby-0.1 (crate (name "usedby") (vers "0.1.3") (deps (list (crate-dep (name "procfs") (req "^0.16.0") (default-features #t) (kind 0)))) (hash "1wc5iabw0qfg7cwvhxajjm9dcp57njp9sbgscnk42s24hpjldkax")))

(define-public crate-usedby-0.1 (crate (name "usedby") (vers "0.1.4") (deps (list (crate-dep (name "procfs") (req "^0.16.0") (default-features #t) (kind 0)))) (hash "189rdjfl0w5m2ly74nnb5k9xn11v9ps03k3s252impkpfdxaz4s9")))

(define-public crate-usedby-0.2 (crate (name "usedby") (vers "0.2.0") (deps (list (crate-dep (name "clap") (req "^4.5.0") (default-features #t) (kind 0)) (crate-dep (name "procfs") (req "^0.16.0") (default-features #t) (kind 0)))) (hash "0cwlnhhg7qxml859sqykmi3w7y4si1pi4r4a46mkkp6409pd9ac4")))

