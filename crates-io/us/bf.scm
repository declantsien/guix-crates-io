(define-module (crates-io us bf) #:use-module (crates-io))

(define-public crate-usbfs-0.1 (crate (name "usbfs") (vers "0.1.0") (deps (list (crate-dep (name "nix") (req "^0.27") (features (quote ("ioctl"))) (default-features #t) (kind 0)))) (hash "0g53s7vpz70sx4n8kpxq4fdfwhnadyy3lz8s96y2gxrqfk1i8lvy")))

(define-public crate-usbfs-0.1 (crate (name "usbfs") (vers "0.1.1") (deps (list (crate-dep (name "nix") (req "^0.27") (features (quote ("ioctl"))) (default-features #t) (kind 0)))) (hash "1aslwilpk40ax0ags2gyln2qcrf8c78ihy23mjgmxwk5grxl7jf7")))

(define-public crate-usbfs-sys-0.1 (crate (name "usbfs-sys") (vers "0.1.0") (deps (list (crate-dep (name "bindgen") (req "^0.46") (default-features #t) (kind 1)) (crate-dep (name "nix") (req "^0.12") (default-features #t) (kind 0)))) (hash "08xqslz2bwckd8zcjq58l60zwcpgkdjvr90kxlffpdzs4zph5wf8")))

