(define-module (crates-io us bh) #:use-module (crates-io))

(define-public crate-usbh-0.1 (crate (name "usbh") (vers "0.1.0") (deps (list (crate-dep (name "defmt") (req "^0.3.5") (default-features #t) (kind 0)) (crate-dep (name "embed-doc-image") (req "^0.1.4") (default-features #t) (kind 0)) (crate-dep (name "fugit") (req "^0.3.7") (default-features #t) (kind 0)) (crate-dep (name "nom") (req "^7.1.3") (kind 0)) (crate-dep (name "usb-device") (req "^0.2.9") (features (quote ("defmt"))) (default-features #t) (kind 0)))) (hash "0s2rrrqyqzgpcaqbn07j2zd86z2z49k43inndikk6z6lg7h1wsi2")))

(define-public crate-usbhid-0.1 (crate (name "usbhid") (vers "0.1.0") (deps (list (crate-dep (name "serde") (req "^1.0.190") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.108") (default-features #t) (kind 0)) (crate-dep (name "strum") (req "^0.25.0") (default-features #t) (kind 0)) (crate-dep (name "strum_macros") (req "^0.25.3") (default-features #t) (kind 0)))) (hash "1xjp8ik2alx7087pqjshp1dxhmpr7cxkzbnanfcwrxpmd88mwk3j")))

