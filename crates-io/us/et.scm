(define-module (crates-io us et) #:use-module (crates-io))

(define-public crate-uset-0.1 (crate (name "uset") (vers "0.1.0") (deps (list (crate-dep (name "criterion") (req "^0.3.0") (default-features #t) (kind 2)) (crate-dep (name "itertools") (req "^0.8.0") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "quickcheck") (req "^0.9.0") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.7") (default-features #t) (kind 0)) (crate-dep (name "spectral") (req "^0.6.0") (default-features #t) (kind 2)))) (hash "0gczccx5p73c5ydrw6w9ryjk71glmj4jbgy9wv8ikrglrlsz3igf")))

