(define-module (crates-io us wi) #:use-module (crates-io))

(define-public crate-uswitch-0.1 (crate (name "uswitch") (vers "0.1.0") (deps (list (crate-dep (name "users") (req "^0.9") (default-features #t) (kind 0)))) (hash "12wsyd7x4aagxayygrr9wdr6phfq2qxciypn4q755hr76y0i5sfh") (yanked #t)))

(define-public crate-uswitch-0.1 (crate (name "uswitch") (vers "0.1.1") (deps (list (crate-dep (name "users") (req "^0.9") (default-features #t) (kind 0)))) (hash "1pwd4b1clj9qr0db6j42zqn44dzjsfz7f101fnp83z6l6izrwpv1")))

