(define-module (crates-io us in) #:use-module (crates-io))

(define-public crate-using-0.0.0 (crate (name "using") (vers "0.0.0") (hash "0v6ixrjgs1rh4ffqlr9gvzf1586qmplswvh96fd472yiv6harr6j")))

(define-public crate-using-0.1 (crate (name "using") (vers "0.1.0") (hash "0bijl96fndqqzilbmqmwfw6i0kjl0flflr0faw79mv308byxgjx0")))

(define-public crate-using-queue-0.1 (crate (name "using-queue") (vers "0.1.0") (hash "1lcmm19m0q3v3wv8wgzii2cnxablwjqdy9yw7qdqsm5f429rns6x")))

