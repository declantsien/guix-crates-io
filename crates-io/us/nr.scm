(define-module (crates-io us nr) #:use-module (crates-io))

(define-public crate-usnrs-0.1 (crate (name "usnrs") (vers "0.1.0") (deps (list (crate-dep (name "binrw") (req "^0.12.0") (default-features #t) (kind 0)) (crate-dep (name "bitflags") (req "^2.4.0") (default-features #t) (kind 0)) (crate-dep (name "chrono") (req "^0.4.31") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^4.4.6") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "mft") (req "^0.6.1") (default-features #t) (kind 0)))) (hash "1bj75pcwc21b1b8ygf4qhvmgcs4n9gq2cdymmm9am1g4qnhw37ym") (features (quote (("usnrs-cli" "clap"))))))

