(define-module (crates-io us iz) #:use-module (crates-io))

(define-public crate-usize_cast-1 (crate (name "usize_cast") (vers "1.0.0") (hash "0hlz49vanysx3nxb563gbvqahlgj527wa79n6a893pvcm88pm7d3")))

(define-public crate-usize_cast-1 (crate (name "usize_cast") (vers "1.1.0") (hash "0lcp7y70b0y3hfr0myk55n00wjb6minslc8k5any6isim2c3l3w1")))

(define-public crate-usize_conversions-0.1 (crate (name "usize_conversions") (vers "0.1.0") (hash "0nlrp42zndnlllqm0aclr0kmlsvgmd8r8gn2zsm2al82569088h1")))

(define-public crate-usize_conversions-0.2 (crate (name "usize_conversions") (vers "0.2.0") (hash "1r95im6miszgiyhs5dgd3179mk9l1kaalb8ilnbnqpg4rgi2j0zp")))

