(define-module (crates-io us yn) #:use-module (crates-io))

(define-public crate-usync-0.0.0 (crate (name "usync") (vers "0.0.0") (hash "1nmxdafizl36l5x3bsyzbhnv56j91bppl1m00bvzv99s6d4pbf4w")))

(define-public crate-usync-0.1 (crate (name "usync") (vers "0.1.0") (deps (list (crate-dep (name "lock_api") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.3") (default-features #t) (kind 2)))) (hash "1q977v79hwa9iyjmb5xlq9vjnvlgw6r44zl5qcdjvcsgdxyn7v1w") (features (quote (("send_guard") ("nightly" "lock_api/nightly") ("default"))))))

(define-public crate-usync-0.1 (crate (name "usync") (vers "0.1.1") (deps (list (crate-dep (name "lock_api") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.3") (default-features #t) (kind 2)))) (hash "1bykn0d4pdh5xi3iyhj9lankgy1g7czh3wayhd2lwgal4nfs07mk") (features (quote (("send_guard") ("nightly" "lock_api/nightly") ("default"))))))

(define-public crate-usync-0.2 (crate (name "usync") (vers "0.2.0") (deps (list (crate-dep (name "lock_api") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.3") (default-features #t) (kind 2)))) (hash "0h0af0q45p0cz29slnk690v3ls7k6yk9nf583kzyhw78gx0a95dr") (features (quote (("send_guard") ("nightly" "lock_api/nightly") ("default"))))))

(define-public crate-usync-0.2 (crate (name "usync") (vers "0.2.1") (deps (list (crate-dep (name "lock_api") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.3") (default-features #t) (kind 2)))) (hash "1q00zvd82b1c90bpwrm5d3rvmwvjq06bv2diaskilarxd8zbdhq8") (features (quote (("send_guard") ("nightly" "lock_api/nightly") ("default"))))))

