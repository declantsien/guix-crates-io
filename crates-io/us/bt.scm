(define-module (crates-io us bt) #:use-module (crates-io))

(define-public crate-usbtmc-0.1 (crate (name "usbtmc") (vers "0.1.0") (hash "1xnf6fx61k4c8fc49svf1cznk1cjsyi9nllmizdj51srr61ljiaq")))

(define-public crate-usbtmc-0.0.1 (crate (name "usbtmc") (vers "0.0.1") (hash "0z7nfkby0kkk4fkgdlkmw1wx87vp9kigncrprkaakp2qn7ca70kd")))

(define-public crate-usbtree-0.1 (crate (name "usbtree") (vers "0.1.0") (deps (list (crate-dep (name "ansi_term") (req "^0.12.1") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^2.33.3") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.8") (default-features #t) (kind 0)) (crate-dep (name "rusb") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "usb-ids") (req "^0.2.1") (default-features #t) (kind 0)))) (hash "16x85zb8z8a3bfv0xg5d24p65zk4nhbmbsqis199k0s932viv3x3")))

