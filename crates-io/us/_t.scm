(define-module (crates-io us _t) #:use-module (crates-io))

(define-public crate-us_time_parser-0.1 (crate (name "us_time_parser") (vers "0.1.0") (deps (list (crate-dep (name "anyhow") (req "^1.0.75") (default-features #t) (kind 0)) (crate-dep (name "peg") (req "^0.8.2") (default-features #t) (kind 0)) (crate-dep (name "pest") (req "^2.7.5") (default-features #t) (kind 0)) (crate-dep (name "pest_derive") (req "^2.7.5") (default-features #t) (kind 0)))) (hash "036rgnllivmm3p6508m3x1k69x1vb59zsm024b5h2zyy86sw9jsa")))

(define-public crate-us_time_parser-0.1 (crate (name "us_time_parser") (vers "0.1.1") (deps (list (crate-dep (name "anyhow") (req "^1.0.75") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^2.33.3") (default-features #t) (kind 0)) (crate-dep (name "pest") (req "^2.7.5") (default-features #t) (kind 0)) (crate-dep (name "pest_derive") (req "^2.7.5") (default-features #t) (kind 0)))) (hash "0bv0bw5lh4v6nab56qvr68zja17xamd1r94wpz1i4my689ljhbqs")))

(define-public crate-us_time_parser-0.1 (crate (name "us_time_parser") (vers "0.1.2") (deps (list (crate-dep (name "anyhow") (req "^1.0.75") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^2.33.3") (default-features #t) (kind 0)) (crate-dep (name "pest") (req "^2.7.5") (default-features #t) (kind 0)) (crate-dep (name "pest_derive") (req "^2.7.5") (default-features #t) (kind 0)))) (hash "0rc09djszrvmjqf0yd9yndipb2vy2flzi2wx5wf7pswdmy4xlpnh")))

