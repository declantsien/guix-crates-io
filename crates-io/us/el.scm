(define-module (crates-io us el) #:use-module (crates-io))

(define-public crate-useless_minigrep-4 (crate (name "useless_minigrep") (vers "4.2.0") (hash "1mrrc1y8hmz3y06c82kmn00x60wf029jhg8j02mpgc9pn3ksyfd7")))

(define-public crate-useless_prefix_useless_prefix_test-0.1 (crate (name "useless_prefix_useless_prefix_test") (vers "0.1.0") (hash "1690raljy069h9pr0v1zbhmfy0cdb2xzb18kli8q35b5mbmix7gb") (yanked #t)))

(define-public crate-uselog-rs-0.1 (crate (name "uselog-rs") (vers "0.1.0") (deps (list (crate-dep (name "log") (req "^0.4.0") (default-features #t) (kind 2)) (crate-dep (name "pretty_env_logger") (req "^0.4.0") (default-features #t) (kind 2)))) (hash "1a9np6bsic0fm86hs9pi335f0wgk7qvn0z8nhqkq0568r7ccya6x")))

(define-public crate-uselog-rs-0.2 (crate (name "uselog-rs") (vers "0.2.0") (deps (list (crate-dep (name "log") (req "^0.4.0") (default-features #t) (kind 2)) (crate-dep (name "pretty_env_logger") (req "^0.4.0") (default-features #t) (kind 2)))) (hash "02vk1bmysc89zk0xg7nvg7bhsjc39w3pzj4kg98dd3cr8s2iqlzg")))

(define-public crate-uselog-rs-0.3 (crate (name "uselog-rs") (vers "0.3.0") (deps (list (crate-dep (name "log") (req "^0.4.0") (default-features #t) (kind 2)) (crate-dep (name "pretty_env_logger") (req "^0.4.0") (default-features #t) (kind 2)))) (hash "0wwghrfvh3dq2wd82akh5jhxqzm3vnfrk23qhv55irqzan5dgvc6")))

