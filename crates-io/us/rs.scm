(define-module (crates-io us rs) #:use-module (crates-io))

(define-public crate-usrsctp-sys-0.1 (crate (name "usrsctp-sys") (vers "0.1.0") (deps (list (crate-dep (name "bindgen") (req "^0.53.2") (default-features #t) (kind 1)) (crate-dep (name "cmake") (req "^0.1.42") (default-features #t) (kind 1)))) (hash "0x2qizw1nhrasv5qrak1nvw6q4g9r96p8gkw2zz88amj79cvh9cz") (links "usrsctp")))

(define-public crate-usrsctp-sys-0.2 (crate (name "usrsctp-sys") (vers "0.2.0") (deps (list (crate-dep (name "bindgen") (req "^0.53.2") (default-features #t) (kind 1)) (crate-dep (name "cmake") (req "^0.1.42") (default-features #t) (kind 1)))) (hash "0qxykc93filzy4vc6f2483acg95160bxzx88h4a874gm7n4wli83") (links "usrsctp")))

