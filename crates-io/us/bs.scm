(define-module (crates-io us bs) #:use-module (crates-io))

(define-public crate-usbsdmux-0.1 (crate (name "usbsdmux") (vers "0.1.0") (deps (list (crate-dep (name "bitflags") (req "^1.2") (default-features #t) (kind 0)) (crate-dep (name "num-derive") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "usb2642-i2c") (req "^0.1") (default-features #t) (kind 0)))) (hash "1a9adha0jzng5gnlskzlwrwdkmjkbzb2nb3mq50i2wb5px1vlw5h")))

