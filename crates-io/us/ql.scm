(define-module (crates-io us ql) #:use-module (crates-io))

(define-public crate-usql-0.0.0 (crate (name "usql") (vers "0.0.0") (hash "1vpw5zscz8fgiajkxw2nz2jgrc8mh5rzhcf2rakkphsdwlfxh07h")))

(define-public crate-usql-ast-0.0.0 (crate (name "usql-ast") (vers "0.0.0") (hash "1iga5sb6kr4fpm5wcgdjjgwhpk33pd4d619hvykxhxjc19n3vaml") (rust-version "1.56")))

(define-public crate-usql-core-0.0.0 (crate (name "usql-core") (vers "0.0.0") (hash "0qpkmph9k8hdk2jkw78jgqbxvmm7rr7a4nm7x8lf2ray4al0wmiz") (rust-version "1.56")))

(define-public crate-usql-lexer-0.0.0 (crate (name "usql-lexer") (vers "0.0.0") (hash "0vcpnsi2w5l0q2czdmy3rhjz6a2rgcf3c7qx9dy7z6jsah7ql303")))

(define-public crate-usql-parser-0.0.0 (crate (name "usql-parser") (vers "0.0.0") (hash "0g0pwybil4712k7dvwfwapbznn7s1yq438plx2svk64p9f4xp8dk")))

