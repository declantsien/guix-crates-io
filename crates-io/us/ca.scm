(define-module (crates-io us ca) #:use-module (crates-io))

(define-public crate-uscan-0.1 (crate (name "uscan") (vers "0.1.0") (hash "0j00pm13n8sax738r86ynjrf529n1sky4kph4pv2wczj5kc1s0kp") (yanked #t)))

(define-public crate-uscan-0.1 (crate (name "uscan") (vers "0.1.1") (hash "130qlyxf95fa8qjiq9sygc1hvbbfq65cifshwhdvm7mi211nsh28")))

(define-public crate-uscan-0.1 (crate (name "uscan") (vers "0.1.2") (hash "0mai8rpadxhp5v2mg2prdrjnj2wfvk87k0hci19ni129zmp3xwp0")))

(define-public crate-uscan-0.1 (crate (name "uscan") (vers "0.1.3") (hash "06ag2vxsf754p9i7d2c537yhfx510l3g6mh8b7chl8ws2cd1lyz7")))

