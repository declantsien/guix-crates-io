(define-module (crates-io us -s) #:use-module (crates-io))

(define-public crate-us-state-info-0.2 (crate (name "us-state-info") (vers "0.2.0") (deps (list (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 2)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 2)))) (hash "1pzahqz5ffwx91yq9xijj47vfw2064dfj7smm4ys62181sbc8wvd") (features (quote (("serde_abbreviation" "serde") ("serde1" "serde"))))))

(define-public crate-us-state-info-0.2 (crate (name "us-state-info") (vers "0.2.1") (deps (list (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 2)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 2)))) (hash "1z6i6wgz9wpr9h6n9pdlrn4apw3h67mf92613xj2mklfppf6aqrl") (features (quote (("serde_abbreviation" "serde") ("serde1" "serde"))))))

(define-public crate-us-state-info-0.2 (crate (name "us-state-info") (vers "0.2.2") (deps (list (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 2)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 2)))) (hash "0a6qbhalzw7cqv2p7jyiqrqgdwi9zhfvxs5v5h7d37b8rlj8mnc1") (features (quote (("serde_abbreviation" "serde") ("serde1" "serde"))))))

(define-public crate-us-state-info-0.2 (crate (name "us-state-info") (vers "0.2.3") (deps (list (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 2)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 2)))) (hash "0pb1f2465gxkcyy4p4j0q0vpwzhabn7hd7nxmwl5mwyr5qqzijya") (features (quote (("serde_abbreviation" "serde") ("serde1" "serde"))))))

(define-public crate-us-state-info-0.2 (crate (name "us-state-info") (vers "0.2.4") (deps (list (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 2)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 2)))) (hash "0a8ng1cg7rxad60mljwyv0914sb0fahjdlg9y4scvywdla26p12z") (features (quote (("serde_abbreviation" "serde") ("serde1" "serde"))))))

(define-public crate-us-state-info-rs-0.1 (crate (name "us-state-info-rs") (vers "0.1.0") (hash "1hjlhdbsqz85dydhiyfj5cxm2hmzramakqkrp316jf4hns85xaw9")))

(define-public crate-us-state-info-rs-0.1 (crate (name "us-state-info-rs") (vers "0.1.1") (hash "1p4jxwrmsdyq67azsnbw5szxzbx2ibayagn6k8f0l7yyahvyv5x6")))

