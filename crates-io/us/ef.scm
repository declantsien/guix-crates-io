(define-module (crates-io us ef) #:use-module (crates-io))

(define-public crate-useful-2 (crate (name "useful") (vers "2.1.0") (hash "0iy6z7l00fkfwnyh3x3q30b78chigw455nlzl8lrj2pcay1cl0rf")))

(define-public crate-useful-serenity-0.1 (crate (name "useful-serenity") (vers "0.1.0") (deps (list (crate-dep (name "serenity") (req "^0.10") (features (quote ("client" "gateway" "model" "rustls_backend" "unstable_discord_api"))) (kind 0)) (crate-dep (name "tokio") (req "^1.17.0") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "1lw5av4q3ixjha5yv861y6g2rkhp5mhbc6qs5d59g4yhp1w5r90q")))

(define-public crate-useful-serenity-0.1 (crate (name "useful-serenity") (vers "0.1.2") (deps (list (crate-dep (name "serenity") (req "^0.10") (features (quote ("client" "gateway" "model" "rustls_backend" "unstable_discord_api"))) (kind 0)) (crate-dep (name "tokio") (req "^1.17.0") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "0m5af40f4midnkv2aca2lgg4xq6wkdhaxzrycxlrxb1nld1m5j2n")))

(define-public crate-useful-serenity-0.1 (crate (name "useful-serenity") (vers "0.1.3") (deps (list (crate-dep (name "serenity") (req "^0.10") (features (quote ("client" "gateway" "model" "rustls_backend" "unstable_discord_api"))) (kind 0)) (crate-dep (name "tokio") (req "^1.17.0") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "0a41cx5amvlwfd78fwq327q4mnm5i2zj0lxbbbadx58gm8ic1w5h")))

(define-public crate-useful-serenity-0.1 (crate (name "useful-serenity") (vers "0.1.4") (deps (list (crate-dep (name "serenity") (req "^0.10") (features (quote ("client" "gateway" "model" "rustls_backend" "unstable_discord_api"))) (kind 0)) (crate-dep (name "tokio") (req "^1.17.0") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "0kffij58a71kqprn6r259iv219g8hy3s1c6sa67ikh76hfhxppgp")))

(define-public crate-useful_macro-0.1 (crate (name "useful_macro") (vers "0.1.0") (hash "1zz2d4fm2syqdykflyw9qchad1i93jp7pvjy0nwrwj731c6xfxm6")))

(define-public crate-useful_macro-0.1 (crate (name "useful_macro") (vers "0.1.1") (hash "06qi5bv5mmcb0q0ilxg353gs3dhz7i8d7z3xb1mlkkdm8d7wd8a5")))

(define-public crate-useful_macro-0.1 (crate (name "useful_macro") (vers "0.1.2") (hash "0crmhwn6w0ypinafvc1az4rjgwxgaayjgwdwacjih6m66pkyhxxa")))

(define-public crate-useful_macro-0.1 (crate (name "useful_macro") (vers "0.1.3") (hash "06i67k5ci04605db0ylrzrsj32sk5x9klf8gm0cmci7qb91azm61")))

(define-public crate-useful_macro-0.1 (crate (name "useful_macro") (vers "0.1.4") (hash "0gpv3v7b86b7q982mx0757i8g6nynlsdhdvcjyz0kpbm0gx4fqf5")))

(define-public crate-useful_macro-0.1 (crate (name "useful_macro") (vers "0.1.5") (hash "0g1rvdyidb1dygwsw0q0m84czls2ms7pa12r1q2m0zx7cmnk358h")))

(define-public crate-useful_macro-0.1 (crate (name "useful_macro") (vers "0.1.6") (hash "15gy9kxxc04zfs0flf31raqbckablbx6mar81b0y8cnigwa6r99f")))

(define-public crate-useful_macro-0.1 (crate (name "useful_macro") (vers "0.1.7") (hash "04g2fjllq97cv05czx25gg49xqh8vv2p07fm4wsbkhgar1scky28")))

(define-public crate-useful_macro-0.1 (crate (name "useful_macro") (vers "0.1.8") (hash "0i3lb24p7lh7aqks9rxvbdnnbm3bm0vbdd57c7x5ls5w4fzahxq7")))

(define-public crate-useful_macro-0.1 (crate (name "useful_macro") (vers "0.1.9") (hash "0bhwwqxp9p78xnh2b9vr4cxgzx83vw10a89gpjg86fhmzkhkbp5l")))

(define-public crate-useful_macro-0.1 (crate (name "useful_macro") (vers "0.1.10") (hash "0g8c7hb36lasrbn1f3s44afhw6w88dmszqf3zfqni26fbjf15zyp")))

(define-public crate-useful_macro-0.1 (crate (name "useful_macro") (vers "0.1.11") (hash "0cpr8skgrzl3d419qpl1s6kwflcqs1ybxj2ymh3bdx4gypf89vsq")))

(define-public crate-useful_macro-0.1 (crate (name "useful_macro") (vers "0.1.12") (hash "0xgvpqhzjbgf7fpgv65r4vm4xyr42125h4hqry2mdq98ab0hc7a1")))

(define-public crate-useful_macro-0.1 (crate (name "useful_macro") (vers "0.1.13") (hash "0n4iblqjqmwddh0b693accalr589g9vp6hg2qkgbylv40lc982gr")))

(define-public crate-useful_macro-0.1 (crate (name "useful_macro") (vers "0.1.14") (hash "1adx6m86jhm14m1ibjzwgz781x5d4ya9fra8788w8i8niy46abx5")))

(define-public crate-useful_macro-0.1 (crate (name "useful_macro") (vers "0.1.15") (hash "0q6nf5xay1hjmqwk29vxh9naqvlrkdpsrpsks7slk7yc3g64figy")))

(define-public crate-useful_macro-0.1 (crate (name "useful_macro") (vers "0.1.16") (hash "1b6rrdbqc2757bdgcqgl3q6k3xjzr7iy5b364hvg927djms394dl")))

(define-public crate-useful_macro-0.1 (crate (name "useful_macro") (vers "0.1.17") (hash "0x3k9a9fp2ipgxabl1g22645gqqqwaggqznkgz41c37kv7v3mj4c")))

(define-public crate-useful_macro-0.1 (crate (name "useful_macro") (vers "0.1.18") (hash "0j8fa3h66wad7rg7fi0x4szpxzb47pg5zzdjksf0zndnhnqb2l9r")))

(define-public crate-useful_macro-0.1 (crate (name "useful_macro") (vers "0.1.19") (hash "1z5rdcb39fasy285v09fkspmvy33nhgxvqy0w6cg1dxv5s28va2s")))

(define-public crate-useful_macro-0.1 (crate (name "useful_macro") (vers "0.1.20") (hash "17agsp9ilx5lifplsb8r4nfg0icvhdjl6v85sz0kql8cx7vi0900")))

(define-public crate-useful_macro-0.1 (crate (name "useful_macro") (vers "0.1.21") (hash "09j7dys4wf7ik2iigxwyiry48ih6k8zvxskhkcyngyrm051hi97i")))

(define-public crate-useful_macro-0.1 (crate (name "useful_macro") (vers "0.1.22") (hash "0l7k20n55pm7f14g0qqwv1585ny06gihkgcr4ihm75yxvhmgkp86")))

(define-public crate-useful_macro-0.1 (crate (name "useful_macro") (vers "0.1.23") (hash "0fdbqfqd1svgnwsjcrcs2ya98w2hfjmixiv0154xw6z86hpyksn9")))

(define-public crate-useful_macro-0.1 (crate (name "useful_macro") (vers "0.1.24") (hash "0fikb4mh27hyklns3hk51czip47rs0amd60l76qh8pbs0qdr0b7g")))

(define-public crate-useful_macro-0.1 (crate (name "useful_macro") (vers "0.1.25") (hash "1x37mzrqbycrg2c0jk6pnyrxyllrwlr1db4x78i6g7f6j8h7mvnd")))

(define-public crate-useful_macro-0.1 (crate (name "useful_macro") (vers "0.1.26") (hash "0cb91dr3kimqfas2bd5z24s0829isz32rcbpbv5bglwhqzcfdklp")))

(define-public crate-useful_macro-0.1 (crate (name "useful_macro") (vers "0.1.27") (hash "11a90zqdwz3w9z56l147n40rfx951q295vh6vlgp74bbh745zbs9")))

(define-public crate-useful_macro-0.1 (crate (name "useful_macro") (vers "0.1.28") (hash "0rc71yh6vlbliyn41f1lk0gn3g0skw0h1pnlni1wkg5v7wsmy0rf")))

(define-public crate-useful_macro-0.1 (crate (name "useful_macro") (vers "0.1.29") (hash "0wakc0h1yk98abdrgd7zaa0737297s6969ya83pl6x5r5m1305zz")))

(define-public crate-useful_macro-0.1 (crate (name "useful_macro") (vers "0.1.30") (hash "165bcc17yfmb1lakggzlszrc87hxlwgis6sjrspgpgv0b8gqvx7s")))

(define-public crate-useful_macro-0.1 (crate (name "useful_macro") (vers "0.1.31") (hash "09zbc6hirxncb5y6xvncr3z23b70h9and36szc2k4bd2x2imvp0x")))

(define-public crate-useful_macro-0.1 (crate (name "useful_macro") (vers "0.1.32") (hash "1vp6skjbijyjjy5z1p71c515z35vwq32bkpadw2prfswd82hilja")))

(define-public crate-useful_macro-0.1 (crate (name "useful_macro") (vers "0.1.33") (hash "0arwfzx04aq9wbjsa6vq1n4yg8hp081vjylw5p7sj6nsf6ynh4m8")))

(define-public crate-useful_macro-0.1 (crate (name "useful_macro") (vers "0.1.34") (hash "15la33kjr0pnvwpbad3ix6a066irma6x7zm7m4682v1pdqh3dsby")))

(define-public crate-useful_macro-0.1 (crate (name "useful_macro") (vers "0.1.35") (hash "16xw099cmik111a8g1728d7ylix7h65j0ifs7av8xyyparfhcji5")))

(define-public crate-useful_macro-0.1 (crate (name "useful_macro") (vers "0.1.37") (hash "111zk68gs39nhyxrhmcg9cxldsrzqfwh4dc588bg49yzf33ph7gj")))

(define-public crate-useful_macro-0.1 (crate (name "useful_macro") (vers "0.1.38") (hash "1ljb0rhnw1yb094zhdpzilra1yvqgk18bi503ns0jxjcqgv3ng4k")))

(define-public crate-useful_macro-0.2 (crate (name "useful_macro") (vers "0.2.0") (hash "1yy4q1lyhksh77792asr1jl73ahh30iigps7hqajr7j01q96nn36")))

(define-public crate-useful_macro-0.2 (crate (name "useful_macro") (vers "0.2.1") (hash "1imy68rqi3hb5lvyyrdjdl9zfrd8bhjbb3gwv6cln7rvzy98m9jk")))

(define-public crate-useful_macro-0.2 (crate (name "useful_macro") (vers "0.2.2") (hash "19a1g9rjqx6a2p4kwqcl15qiqxi0pqaic64znim38j9ybikpxm3d")))

(define-public crate-useful_macro-0.2 (crate (name "useful_macro") (vers "0.2.3") (hash "0bncs16p0fxg6v8pzilzdp2g1f6rcgvrd89fky2zx8ijji9fwyks")))

(define-public crate-useful_macro-0.2 (crate (name "useful_macro") (vers "0.2.4") (hash "02pn8x8m7lg55cc4v3bmf5s6xbzqplzd5kc7rlb600h66f1jf9cd")))

(define-public crate-useful_macro-0.2 (crate (name "useful_macro") (vers "0.2.5") (hash "0pc888cmhbs7d645g6aqahq0ghsbicdbx8pc6a0l4l3zh97miszw")))

(define-public crate-useful_macro-0.2 (crate (name "useful_macro") (vers "0.2.6") (hash "0j74h9rhp1mipsiq9q6cjg59cmd5rsw1p9qlw09r1a280bvbcr19")))

(define-public crate-useful_macro-0.2 (crate (name "useful_macro") (vers "0.2.8") (hash "08plq4d4hnyhn1fnvk3lnhwbgnnsdw1vhi54rribx242s5g4zf67")))

(define-public crate-useful_macro-0.2 (crate (name "useful_macro") (vers "0.2.9") (hash "0dn83s86d6zxr6hxlqnm8wl7jlifj9ybpg6l6mrlc9bkm103ad7y")))

(define-public crate-useful_macro-0.2 (crate (name "useful_macro") (vers "0.2.10") (hash "00jph9rfql3z9l6jbhdhs7cal8xdvhvicyg4v5lclpk61kjprlgx")))

(define-public crate-useful_macro-0.2 (crate (name "useful_macro") (vers "0.2.11") (hash "1gpwrz9j7bpd763nc50672r14klkdrnx88wwii56jg0qxyxzss28")))

(define-public crate-useful_macro-0.2 (crate (name "useful_macro") (vers "0.2.12") (hash "00h5amzzrszdhsjbq10iadngpz0cyngkibl330qn27c29jgamdsz")))

(define-public crate-useful_macro-0.2 (crate (name "useful_macro") (vers "0.2.13") (hash "0x9svsc2h3d5y3d4iyx0sm6fbfgkxn7skppr2pklia4wfcwci3wb")))

(define-public crate-useful_macro-0.2 (crate (name "useful_macro") (vers "0.2.14") (hash "07ylb768qnkg8dkc3fcfm84z2jk9m6agwrvkvhamjslnd5440zly")))

(define-public crate-useful_macro-0.2 (crate (name "useful_macro") (vers "0.2.15") (hash "01gn3hd8p03769243agdnikg1iw7dp1byadjq7gq1m9pmwznl5yj")))

(define-public crate-useful_macro-0.2 (crate (name "useful_macro") (vers "0.2.16") (hash "07y3xczvw6bn1cy2blf5id0l7z0cdhlrd1npyryn9yqcnfpmr76b")))

(define-public crate-useful_macro-0.2 (crate (name "useful_macro") (vers "0.2.17") (hash "1cs6l3cs71ifm6sc8lwshzjna68ckd74gx7k9hbw3yahp9q5x6yf")))

(define-public crate-useful_macro-0.2 (crate (name "useful_macro") (vers "0.2.18") (hash "1al3x74kgglghq26wb0z2q46aqkjh0fj1yr9kgyk7gacc86aka50")))

(define-public crate-useful_macro-0.2 (crate (name "useful_macro") (vers "0.2.19") (hash "0zrpa5sw75h1md8mraa76iksxjm5jyrf80ijbc9x1f7ii36pls4l")))

(define-public crate-useful_macro-0.2 (crate (name "useful_macro") (vers "0.2.20") (hash "16zrzqbqj98s0qchnapmj3syjfmxka94aiwqs1zvakgq7a0k0p8x")))

(define-public crate-useful_macro-0.2 (crate (name "useful_macro") (vers "0.2.21") (hash "03lm443rc0hcvjmvb9n2hrddr3lrc1w8z2l8ycky6vjbmvmk4imy")))

(define-public crate-useful_macro-0.2 (crate (name "useful_macro") (vers "0.2.22") (hash "0fv4nnfx6yf5rpkjw68vg49x77ciwwbjxkypgb9hvhxpryqyf657")))

(define-public crate-useful_macro-0.2 (crate (name "useful_macro") (vers "0.2.23") (hash "1q080dg6il9rx1cgkmmk5rgv50mv9dhva5lcax5zjkkvdz20sykl")))

(define-public crate-useful_macro-0.2 (crate (name "useful_macro") (vers "0.2.24") (hash "042lin7rfb0wq6cjkqddapnk1af0m3pw54a285dv9h78kv6bl0f2")))

(define-public crate-useful_macro-0.2 (crate (name "useful_macro") (vers "0.2.25") (hash "1hs4ncclb5kdji2y8gd2sy5yxq6az29049gvr42idhxl7zi6cx2n")))

(define-public crate-useful_macro-0.2 (crate (name "useful_macro") (vers "0.2.26") (hash "1rjzsc9k489j8w52p8viigvcdflpqx8samv55m2kpszszkvpb3mz")))

(define-public crate-useful_macro-0.2 (crate (name "useful_macro") (vers "0.2.27") (hash "1jfqbb5hwks77l5wjki05rq260vgpfjniglmbg47v3iv9dnf32l1")))

(define-public crate-useful_macro-0.2 (crate (name "useful_macro") (vers "0.2.28") (hash "17ng5jj1jbz6cfd4kj5z3jxcfmg7bf0lcvcc3b1l8ykp97i7kym3")))

(define-public crate-useful_macro-0.2 (crate (name "useful_macro") (vers "0.2.29") (hash "15zvbl1ljxqvjgydqmm7y9s9lggm7ibamaywdhpd8cczj8jph0cs")))

(define-public crate-useful_macros-0.1 (crate (name "useful_macros") (vers "0.1.0") (deps (list (crate-dep (name "proc_macros") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "15w5lnbaixjyk39lhfqxn2g9702hybrsakdykqnc11dgn3zczdzc")))

(define-public crate-useful_macs-1 (crate (name "useful_macs") (vers "1.0.0") (hash "121sxqra1zpaa1jg6xbd18cgm6lqb7dx4vn4s73y1fvrzcwnkg9l") (yanked #t) (rust-version "1.65.0")))

(define-public crate-Useful_Math-0.0.1 (crate (name "Useful_Math") (vers "0.0.1") (hash "0brfa0vhlkkzkj7flgdprsvp7l64pbp86bslnlbcy6i2267ids94")))

(define-public crate-Useful_Math-0.0.2 (crate (name "Useful_Math") (vers "0.0.2") (hash "1agrhrbh2vf6rfk88rz6rykmzsyisqm4b2c6jg8phd5j57c0hjjk")))

(define-public crate-Useful_Math-0.0.3 (crate (name "Useful_Math") (vers "0.0.3") (hash "0j7630acynqp78scwmv81myi3x6gsg7x9mg6z1i4533imdbpvxhk")))

(define-public crate-Useful_Math-0.0.4 (crate (name "Useful_Math") (vers "0.0.4") (hash "0xkpwdqpjcf4md1bkw16kq2xl0jwxyvixc53px45x38jgpgpm0kj")))

(define-public crate-Useful_Math-0.0.5 (crate (name "Useful_Math") (vers "0.0.5") (hash "0mwb9plq0gdqiqf8nyry66s8k74gha2ziwbj77jzmryc8zv4lkjv")))

(define-public crate-Useful_Math-0.0.6 (crate (name "Useful_Math") (vers "0.0.6") (hash "05csxfznk0r4m1i4glb93ri9svlp0nfcc60v3ns5ifwxkrz7yxz6")))

(define-public crate-Useful_Math-0.0.7 (crate (name "Useful_Math") (vers "0.0.7") (deps (list (crate-dep (name "num") (req "^0.2.0") (default-features #t) (kind 0)))) (hash "0bdbr60d4nr8apqh89jwiszc0gw2mhlg5ld3lqg2b6n6zfnhij3s")))

(define-public crate-Useful_Math-0.0.8 (crate (name "Useful_Math") (vers "0.0.8") (deps (list (crate-dep (name "num") (req "^0.2.0") (default-features #t) (kind 0)))) (hash "1cibivpynq41g3lmxm2hxwgw62ilhr6p179aahwhdi390kp0a11n")))

(define-public crate-useful_static-0.1 (crate (name "useful_static") (vers "0.1.0") (hash "03qqmghmz19g6qigzkpcxr38b9jyp6b03ifk0h0b1vj07v0vm6dl")))

(define-public crate-useful_static-0.1 (crate (name "useful_static") (vers "0.1.1") (hash "0vqpbsj765gdam5ycf8qq8sncmx1fxa2cjq7jcqmd1qxmhqdjhd1")))

(define-public crate-useful_static-0.2 (crate (name "useful_static") (vers "0.2.0") (hash "0v335prvvwncvm9bfggjhgg91h3qn1xcv2cx4xikyzvsl39ax6y1")))

(define-public crate-useful_static-0.2 (crate (name "useful_static") (vers "0.2.1") (hash "197h76fbxldw45hyjdqwzmnmiwdqc67q8vyz8hx2npfjhz20f2cf")))

