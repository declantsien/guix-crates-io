(define-module (crates-io us le) #:use-module (crates-io))

(define-public crate-uslegalpro-0.1 (crate (name "uslegalpro") (vers "0.1.0") (deps (list (crate-dep (name "reqwest") (req "^0.11.20") (kind 0)) (crate-dep (name "serde") (req "^1.0.188") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.107") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.48") (default-features #t) (kind 0)))) (hash "1q8cx7ds39c6wjxmbfj16s097j3sv2fxmxaq5rz74maklh7na2wd")))

