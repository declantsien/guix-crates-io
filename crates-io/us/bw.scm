(define-module (crates-io us bw) #:use-module (crates-io))

(define-public crate-usbw-0.0.1 (crate (name "usbw") (vers "0.0.1") (deps (list (crate-dep (name "driver_async") (req "^0.0.2") (default-features #t) (kind 0)) (crate-dep (name "rusb") (req "^0.5.5") (default-features #t) (kind 0)))) (hash "12lhnpq0r6y2vkbw83k8svi5sm7hswrymzvhf09vmdqpcxkd34kd")))

(define-public crate-usbw-0.0.2 (crate (name "usbw") (vers "0.0.2") (deps (list (crate-dep (name "driver_async") (req "^0.0.3") (default-features #t) (kind 0)) (crate-dep (name "futures-util") (req "^0.3.4") (kind 0)) (crate-dep (name "libc") (req "^0.2") (optional #t) (kind 0)) (crate-dep (name "libusb1-sys") (req "^0.3.7") (optional #t) (kind 0)) (crate-dep (name "tokio") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "winapi") (req "^0.3.8") (optional #t) (kind 0)))) (hash "1d5rlbh54yw3m6i2p8x27xf17dnqyi5m492idjm3d4vm3mgb4fx6") (features (quote (("winusb" "winapi/winusb" "std") ("std") ("libusb" "libusb1-sys" "std" "libc") ("default" "libusb"))))))

