(define-module (crates-io us tu) #:use-module (crates-io))

(define-public crate-ustulation-test0-0.1 (crate (name "ustulation-test0") (vers "0.1.0") (hash "1194xr1z0xwaclpvjnj23bhl6waar1m83cs3dn5s99c7pidjyszm")))

(define-public crate-ustulation-test1-0.1 (crate (name "ustulation-test1") (vers "0.1.0") (deps (list (crate-dep (name "ustulation-test0") (req "= 0.1.0") (default-features #t) (kind 0)))) (hash "09dwmc2znpnrh2zhn9d8k3xpvlfj37l9y28ldccv740xdfm7cz68")))

(define-public crate-ustulation-test1-0.2 (crate (name "ustulation-test1") (vers "0.2.0") (deps (list (crate-dep (name "ustulation-test0") (req "= 0.1.0") (default-features #t) (kind 0)))) (hash "1dnp5dcvpim54gv2axxg8jlhqmp3qw2259cmdv9jcmjy5fd21ixw")))

