(define-module (crates-io k- on) #:use-module (crates-io))

(define-public crate-k-onfig-0.0.1 (crate (name "k-onfig") (vers "0.0.1") (deps (list (crate-dep (name "k-onfig-derive") (req "^0.0.1") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.197") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "08qmashky8nyry2aw4vlzsywhr4qf4xp5ival7in6yssls79zvjp")))

(define-public crate-k-onfig-derive-0.0.1 (crate (name "k-onfig-derive") (vers "0.0.1") (deps (list (crate-dep (name "proc-macro2") (req "^1.0.78") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.35") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2.0.50") (default-features #t) (kind 0)))) (hash "15z3cypc1vdcagydkp72szywdg80xx70c8vai7ixc1r0bgigv6c4")))

