(define-module (crates-io k- li) #:use-module (crates-io))

(define-public crate-k-lighter-1 (crate (name "k-lighter") (vers "1.0.0") (deps (list (crate-dep (name "clap") (req "^4.3.22") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "configparser") (req "^3.0.2") (default-features #t) (kind 0)))) (hash "0p5g96h38772mgp2qv627nrjnsw1w64dzf1qfr45zwivpncjs27p")))

(define-public crate-k-lighter-1 (crate (name "k-lighter") (vers "1.0.1") (deps (list (crate-dep (name "clap") (req "^4.3.22") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "configparser") (req "^3.0.2") (default-features #t) (kind 0)))) (hash "1ps4lcdizvcx2qj5pl2xl10hffh1lfav0dz1vpd5sn10qdlnbsbb")))

(define-public crate-k-lighter-1 (crate (name "k-lighter") (vers "1.0.2") (deps (list (crate-dep (name "clap") (req "^4.3.22") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "configparser") (req "^3.0.2") (default-features #t) (kind 0)) (crate-dep (name "dirs") (req "^5.0.1") (default-features #t) (kind 0)))) (hash "0l5757xasj1g4y8j3j6c7fr8lxpdqf0jky6vnp2hj9np1xnilp1n")))

(define-public crate-k-lighter-1 (crate (name "k-lighter") (vers "1.0.3") (deps (list (crate-dep (name "clap") (req "^4.3.22") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "configparser") (req "^3.0.2") (default-features #t) (kind 0)) (crate-dep (name "dirs") (req "^5.0.1") (default-features #t) (kind 0)))) (hash "0b6bnqmv2md9ndcz1nzg055846rsvf5srrvzvj1z48k83v2i4sp6")))

(define-public crate-k-lighter-1 (crate (name "k-lighter") (vers "1.0.4") (deps (list (crate-dep (name "clap") (req "^4.3.22") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "configparser") (req "^3.0.2") (default-features #t) (kind 0)) (crate-dep (name "dirs") (req "^5.0.1") (default-features #t) (kind 0)))) (hash "09wc8f98dpw2qxdn7rwxnl8w2k0hy1lllf81id3z274m3xsijqyb")))

