(define-module (crates-io k- sq) #:use-module (crates-io))

(define-public crate-k-sql-0.1 (crate (name "k-sql") (vers "0.1.0") (hash "0gxacdd341y0f4fql8ispk26gzx5w9l4yip10kzpm7k2xrk1f57b")))

(define-public crate-k-sql-script-0.1 (crate (name "k-sql-script") (vers "0.1.0") (hash "0mv3b9zvcqjjlm4jv5pg920fir615jz96qj8nj7iqyhd0y0sw9ai")))

