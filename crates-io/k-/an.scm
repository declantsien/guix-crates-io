(define-module (crates-io k- an) #:use-module (crates-io))

(define-public crate-k-anon-hash-0.1 (crate (name "k-anon-hash") (vers "0.1.0") (deps (list (crate-dep (name "hex") (req "^0.4.2") (default-features #t) (kind 0)) (crate-dep (name "sha2") (req "^0.9.3") (default-features #t) (kind 0)))) (hash "10mmbsf350fb7hdlfw7xm53ncv6nf2b11vcy22knbs0f6b961pvr")))

(define-public crate-k-anon-hash-0.1 (crate (name "k-anon-hash") (vers "0.1.1") (deps (list (crate-dep (name "hex") (req "^0.4.2") (default-features #t) (kind 0)) (crate-dep (name "sha2") (req "^0.9.3") (default-features #t) (kind 0)))) (hash "0hppq25vkly5w6r6bmjqmrn5pb585fikk61zpv9923ibxscidqdi")))

(define-public crate-k-anon-hash-0.1 (crate (name "k-anon-hash") (vers "0.1.2") (deps (list (crate-dep (name "hex") (req "^0.4.2") (default-features #t) (kind 0)) (crate-dep (name "sha2") (req "^0.9.3") (default-features #t) (kind 0)))) (hash "12v86ddnpgwf08chk623ipigg7mjgxqwqjdcd7wly4rwbb3jvxsf")))

(define-public crate-k-anon-hash-0.1 (crate (name "k-anon-hash") (vers "0.1.3") (deps (list (crate-dep (name "hex") (req "^0.4.2") (default-features #t) (kind 0)) (crate-dep (name "sha2") (req "^0.9.3") (default-features #t) (kind 0)))) (hash "1n05yf0wk1bvm3n4zjapl5jrc0gfdqj1glwj6xff9q0bjnmx15gn")))

