(define-module (crates-io zx -b) #:use-module (crates-io))

(define-public crate-zx-bip44-0.1 (crate (name "zx-bip44") (vers "0.1.0") (deps (list (crate-dep (name "byteorder") (req "^1.3.4") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.20") (default-features #t) (kind 0)))) (hash "1q5dy75gdh8gnk9nf78ps1n82py4dln60qf0blyr19hykmb7pl9f")))

