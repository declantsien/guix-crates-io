(define-module (crates-io zx #{0d}#) #:use-module (crates-io))

(define-public crate-zx0dec-0.1 (crate (name "zx0dec") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^4.3.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "zx0decompress") (req "^0.1.2") (default-features #t) (kind 0)))) (hash "1i6xhflhx72rlc06byw88jz1pjvpdfil3a93531sbpn19cnskihb")))

(define-public crate-zx0decompress-0.1 (crate (name "zx0decompress") (vers "0.1.0") (deps (list (crate-dep (name "thiserror") (req "^1.0.40") (default-features #t) (kind 0)))) (hash "1qnlg2x1wv81nkx9bfnvv4k0kgpwmd5i6iwlclidbqmxyhwvvyl4")))

(define-public crate-zx0decompress-0.1 (crate (name "zx0decompress") (vers "0.1.1") (deps (list (crate-dep (name "thiserror") (req "^1.0.40") (default-features #t) (kind 0)))) (hash "08bqighk5b91n4v7gg4w80lniycqzlgi9j013i8ryz4l4753dwa8")))

(define-public crate-zx0decompress-0.1 (crate (name "zx0decompress") (vers "0.1.2") (deps (list (crate-dep (name "thiserror") (req "^1.0.40") (default-features #t) (kind 0)))) (hash "10nmh1vfr53hjni8311501h596l8fks4w78g6jnj7k5s2zfygm75")))

