(define-module (crates-io fz _t) #:use-module (crates-io))

(define-public crate-fz_tod_r-0.1 (crate (name "fz_tod_r") (vers "0.1.0") (deps (list (crate-dep (name "reqwest") (req "^0.11") (features (quote ("json" "blocking"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)))) (hash "0b65k4shazk8rvsi1bgdz2acshb0pyva0xl8mg45i4q0gnm22l7h")))

(define-public crate-fz_tod_r-0.1 (crate (name "fz_tod_r") (vers "0.1.1") (deps (list (crate-dep (name "crossterm") (req "^0.27") (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.11") (features (quote ("json" "blocking"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)))) (hash "033h6sq5srayv22ai2mr02cnxhnpi70iq11mpf1vlqrkdf1law0k")))

(define-public crate-fz_tod_r-1 (crate (name "fz_tod_r") (vers "1.0.0") (deps (list (crate-dep (name "fp_server") (req "^1.2") (default-features #t) (kind 0)))) (hash "0jkm27118yng5fi8vpzrprpgq6s33m3cs6r4mbg0j5gbzl7dv3rb")))

(define-public crate-fz_tod_r-1 (crate (name "fz_tod_r") (vers "1.1.0") (deps (list (crate-dep (name "fp_server") (req "^1.2") (default-features #t) (kind 0)))) (hash "10hr1wphsxki7729krj5mx495zqd1nbjj7cf4j45rixsh0y0qwi9")))

(define-public crate-fz_tod_r-1 (crate (name "fz_tod_r") (vers "1.1.1") (deps (list (crate-dep (name "fp_server") (req "^1.2") (default-features #t) (kind 0)))) (hash "10b8dcc0lipg57pkknb7g8iac32swr1gvivnjs6fzw66i5mqpymn")))

(define-public crate-fz_tod_r-1 (crate (name "fz_tod_r") (vers "1.1.2") (deps (list (crate-dep (name "fp_server") (req "^1.2") (default-features #t) (kind 0)))) (hash "1gkzdfz4979wcy7xkwiccas4q8m42256676d3a8gmazbg5dpgx3d")))

