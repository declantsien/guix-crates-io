(define-module (crates-io lb nf) #:use-module (crates-io))

(define-public crate-lbnf-0.1 (crate (name "lbnf") (vers "0.1.0") (deps (list (crate-dep (name "lalrpop") (req "^0.20.0") (default-features #t) (kind 1)) (crate-dep (name "lalrpop-util") (req "^0.20.0") (features (quote ("lexer"))) (default-features #t) (kind 0)) (crate-dep (name "proptest") (req "^1.2.0") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)))) (hash "026wn17r0ld7gwnkdarm4awnjx11q28krr0dvvpaaivfkglsx670")))

