(define-module (crates-io lb -p) #:use-module (crates-io))

(define-public crate-lb-pdf-0.1 (crate (name "lb-pdf") (vers "0.1.0") (deps (list (crate-dep (name "pdfium-render") (req "^0.8.11") (default-features #t) (kind 0)))) (hash "182wdz59x5z34hizz7mx81149616dqra3x1rli4rm6d8h05ykcr8")))

(define-public crate-lb-pdf-0.1 (crate (name "lb-pdf") (vers "0.1.1") (deps (list (crate-dep (name "pdfium-render") (req "^0.8.11") (default-features #t) (kind 0)))) (hash "1i8y7kql42msfi5cd7km5mhdjvpahicc6vhpz3vx1j5k3px9n5il")))

