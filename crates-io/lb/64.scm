(define-module (crates-io lb #{64}#) #:use-module (crates-io))

(define-public crate-lb64-0.1 (crate (name "lb64") (vers "0.1.0") (deps (list (crate-dep (name "criterion") (req "^0.2") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.6") (default-features #t) (kind 0)))) (hash "06qbxi5vmd6nch7ch6kx6wswhx5w2gmjc6wfdzwjmbpybizlmz1r")))

(define-public crate-lb64-0.1 (crate (name "lb64") (vers "0.1.1") (deps (list (crate-dep (name "criterion") (req "^0.2") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.6") (default-features #t) (kind 0)))) (hash "07yl1cmm1052f8ynkq0gpkvz17nhqiybkl29izs9pxqwl5qwy6z2")))

(define-public crate-lb64-0.1 (crate (name "lb64") (vers "0.1.2") (deps (list (crate-dep (name "criterion") (req "^0.2") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.6") (default-features #t) (kind 0)))) (hash "0mbmkph4qc3753yp2jw98kgmj1xfwab9b4za8p3q32jaqx64spqn")))

