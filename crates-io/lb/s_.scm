(define-module (crates-io lb s_) #:use-module (crates-io))

(define-public crate-lbs_derive-0.1 (crate (name "lbs_derive") (vers "0.1.0") (deps (list (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (default-features #t) (kind 0)))) (hash "09r2l96vxwv2yhqprzhqgbamc761pl9yzrv56pz5v9kzmabvi19x")))

(define-public crate-lbs_derive-0.2 (crate (name "lbs_derive") (vers "0.2.0") (deps (list (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (default-features #t) (kind 0)))) (hash "1hh4amnlv6nvilg3iaz78ggayrixn4knp49yfq0pldy0pbbj0d2f")))

(define-public crate-lbs_derive-0.3 (crate (name "lbs_derive") (vers "0.3.1") (deps (list (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2.0") (default-features #t) (kind 0)))) (hash "0qpg3x4ida00iz765bszkwl6nm84ws034w1y01481h6zds9j8c39")))

(define-public crate-lbs_derive-0.4 (crate (name "lbs_derive") (vers "0.4.0") (deps (list (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2.0") (default-features #t) (kind 0)))) (hash "1ii9yf85kqyq6zzfirdvimw9ljzky62nhvglpaq1dqhvh971f19k")))

(define-public crate-lbs_derive-0.4 (crate (name "lbs_derive") (vers "0.4.1") (deps (list (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2.0") (default-features #t) (kind 0)))) (hash "119p3x2jrdnr5n8hhizzmbjbnympsc5dai2aj7c2xrpwazqfwc8p")))

