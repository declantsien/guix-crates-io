(define-module (crates-io lb er) #:use-module (crates-io))

(define-public crate-lber-0.1 (crate (name "lber") (vers "0.1.2") (deps (list (crate-dep (name "byteorder") (req "^1.0.0") (default-features #t) (kind 0)) (crate-dep (name "bytes") (req "^0.4.1") (default-features #t) (kind 0)) (crate-dep (name "nom") (req "^2.0.1") (default-features #t) (kind 0)))) (hash "0yrk8nbgxsvg5yn75mcq3l8vrgfx9ma726psfhj2n1isnwchkx5h")))

(define-public crate-lber-0.1 (crate (name "lber") (vers "0.1.3") (deps (list (crate-dep (name "byteorder") (req "^1.0.0") (default-features #t) (kind 0)) (crate-dep (name "bytes") (req "^0.4.1") (default-features #t) (kind 0)) (crate-dep (name "nom") (req "^2.0.1") (default-features #t) (kind 0)))) (hash "0cnzvhx6d34q3i5pcm0vksc9mxipww4gh96haziaiv3a5cg8nrv2")))

(define-public crate-lber-0.1 (crate (name "lber") (vers "0.1.4") (deps (list (crate-dep (name "byteorder") (req "^1.0.0") (default-features #t) (kind 0)) (crate-dep (name "bytes") (req "^0.4.1") (default-features #t) (kind 0)) (crate-dep (name "nom") (req "^2.0.1") (default-features #t) (kind 0)))) (hash "1vkbgfcc0c5vnbgh2j5qa9scpr5pxzw61bynh0vvbxaz49h1yr4s")))

(define-public crate-lber-0.1 (crate (name "lber") (vers "0.1.5") (deps (list (crate-dep (name "byteorder") (req "^1.0.0") (default-features #t) (kind 0)) (crate-dep (name "bytes") (req "^0.4.1") (default-features #t) (kind 0)) (crate-dep (name "nom") (req "^2.0.1") (default-features #t) (kind 0)))) (hash "0pldihkdzwgi7wmmjiwasryqvkxvfymb5dcw09hdlkv00fg3vfmj")))

(define-public crate-lber-0.1 (crate (name "lber") (vers "0.1.6") (deps (list (crate-dep (name "byteorder") (req "^1.0.0") (default-features #t) (kind 0)) (crate-dep (name "bytes") (req "^0.4.1") (default-features #t) (kind 0)) (crate-dep (name "nom") (req "^2.0.1") (default-features #t) (kind 0)))) (hash "0dhv2i5r2zzb74krn1522qnk57hqb9rqbl03nn9laxr6vlsjl5xa")))

(define-public crate-lber-0.1 (crate (name "lber") (vers "0.1.7") (deps (list (crate-dep (name "byteorder") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "bytes") (req "^0.5") (default-features #t) (kind 0)) (crate-dep (name "nom") (req "^2") (default-features #t) (kind 0)))) (hash "00l50r29c69wncbhmjmm8nmm0xcagbfdd2n6fkra6s10z0bphssy") (yanked #t)))

(define-public crate-lber-0.2 (crate (name "lber") (vers "0.2.0") (deps (list (crate-dep (name "byteorder") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "bytes") (req "^0.5") (default-features #t) (kind 0)) (crate-dep (name "nom") (req "^2") (default-features #t) (kind 0)))) (hash "19bqhaysk4q3hd7jyqhdb7h56l06j7r6q35ah51qvyzw8d6rajd7")))

(define-public crate-lber-0.3 (crate (name "lber") (vers "0.3.0") (deps (list (crate-dep (name "byteorder") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "bytes") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "nom") (req "^2") (default-features #t) (kind 0)))) (hash "0c69hs628kdjps6d9wjcr8cdqwv5fbs3qr1jmgxac89vk4hbb6ca")))

(define-public crate-lber-0.4 (crate (name "lber") (vers "0.4.0") (deps (list (crate-dep (name "bytes") (req "^1.3.0") (default-features #t) (kind 0)) (crate-dep (name "nom") (req "^7.1.1") (default-features #t) (kind 0)))) (hash "0ih4kr9qmqwhm78492v09kmlcrgzvbqc3cf3f06bab7101g5zn5m")))

(define-public crate-lber-0.4 (crate (name "lber") (vers "0.4.2") (deps (list (crate-dep (name "bytes") (req "^1.3.0") (default-features #t) (kind 0)) (crate-dep (name "nom") (req "^7.1.1") (default-features #t) (kind 0)))) (hash "02pghdykbsffimswayixinrsaxfmwwzpb854w5cqzkv4kzyzkxrd")))

(define-public crate-lber-serde-0.4 (crate (name "lber-serde") (vers "0.4.2") (deps (list (crate-dep (name "bytes") (req "^1.3.0") (default-features #t) (kind 0)) (crate-dep (name "nom") (req "^7.1.1") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.198") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)))) (hash "1x7z5wpa0zhpyy2hp8h6raphmb9sbhv21p8gmvhjy0za5ydwxyx9") (yanked #t) (v 2) (features2 (quote (("serde" "dep:serde"))))))

(define-public crate-lber-serde-0.4 (crate (name "lber-serde") (vers "0.4.3") (deps (list (crate-dep (name "bytes") (req "^1.3.0") (default-features #t) (kind 0)) (crate-dep (name "nom") (req "^7.1.1") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.198") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)))) (hash "15y4x57gvr9852aaqdi4435vxxf4fw8nwnkvhxv88kipshvzazx0") (v 2) (features2 (quote (("serde" "dep:serde"))))))

(define-public crate-lber-serde-0.4 (crate (name "lber-serde") (vers "0.4.4") (deps (list (crate-dep (name "bytes") (req "^1.3.0") (default-features #t) (kind 0)) (crate-dep (name "nom") (req "^7.1.1") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.198") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)))) (hash "0l2ly97bz6y9w18g1clf9s318rxbvhr58lak2zj1vdr1j6wc9rca") (v 2) (features2 (quote (("serde" "dep:serde"))))))

(define-public crate-lber-serde-0.4 (crate (name "lber-serde") (vers "0.4.5") (deps (list (crate-dep (name "bytes") (req "^1.3.0") (default-features #t) (kind 0)) (crate-dep (name "nom") (req "^7.1.1") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.198") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)))) (hash "1kcyrdq6skc93dyb7kp4gqfvbapfciyr3izifislj4qyfy3wvb3k") (v 2) (features2 (quote (("serde" "dep:serde"))))))

