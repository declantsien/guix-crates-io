(define-module (crates-io lb fg) #:use-module (crates-io))

(define-public crate-lbfgs-0.1 (crate (name "lbfgs") (vers "0.1.1") (deps (list (crate-dep (name "num") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "unit_test_utils") (req "^0.1.3") (default-features #t) (kind 2)))) (hash "0g0cpssv7ns9i6gj95hrban7wqj9x93vl5nhppwv1z3y8s4a3ssa")))

(define-public crate-lbfgs-0.2 (crate (name "lbfgs") (vers "0.2.0") (deps (list (crate-dep (name "num-traits") (req "^0.2.8") (default-features #t) (kind 0)) (crate-dep (name "unit_test_utils") (req "^0.1.3") (default-features #t) (kind 2)))) (hash "18iqbal080dw7i4qnmdpk6l77hvzbwf225d1s5xsd0c52z8xqyjj")))

(define-public crate-lbfgs-0.2 (crate (name "lbfgs") (vers "0.2.1") (deps (list (crate-dep (name "num-traits") (req "^0.2.8") (default-features #t) (kind 0)) (crate-dep (name "unit_test_utils") (req "^0.1.3") (default-features #t) (kind 2)))) (hash "1x0904g114g6i2s3lrjch7pl5nmq7jap8d34vbxw7lskapv9dp8b")))

(define-public crate-lbfgsb-0.1 (crate (name "lbfgsb") (vers "0.1.0") (deps (list (crate-dep (name "anyhow") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "bindgen") (req "^0.65.1") (default-features #t) (kind 1)) (crate-dep (name "cc") (req "^1") (default-features #t) (kind 1)) (crate-dep (name "vecfx") (req "^0.1") (default-features #t) (kind 2)))) (hash "18nh5dcfn68wqv91g67grph8jwraipqfp5cnx8v6hkix24lzq58a")))

(define-public crate-lbfgsb-rs-0.1 (crate (name "lbfgsb-rs") (vers "0.1.0") (deps (list (crate-dep (name "anyhow") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "bindgen") (req "^0.65.1") (default-features #t) (kind 1)) (crate-dep (name "cc") (req "^1") (default-features #t) (kind 1)) (crate-dep (name "vecfx") (req "^0.1") (default-features #t) (kind 2)))) (hash "0cqyc2yx3z43n9lykvkis7c909da160c0ddkxsrcvlm8ll2cn4dc") (yanked #t)))

(define-public crate-lbfgsb-sys-0.1 (crate (name "lbfgsb-sys") (vers "0.1.0") (deps (list (crate-dep (name "libc") (req "^0.2.11") (default-features #t) (kind 0)))) (hash "11d81jakli0bfqja382yb2a3i0qa9mnzlj7rgx2ndmkydbl9znfc") (features (quote (("static"))))))

