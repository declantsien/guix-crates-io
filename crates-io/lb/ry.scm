(define-module (crates-io lb ry) #:use-module (crates-io))

(define-public crate-lbry-rs-0.0.1 (crate (name "lbry-rs") (vers "0.0.1") (deps (list (crate-dep (name "jsonrpc-client-core") (req "^0.5.0") (default-features #t) (kind 0)) (crate-dep (name "jsonrpc-client-http") (req "^0.5.0") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.34") (default-features #t) (kind 0)))) (hash "071l7859hzvgmbbibgc9la7g7bqm30bjwl9xfws8bivk7l9amq4i")))

