(define-module (crates-io ke tr) #:use-module (crates-io))

(define-public crate-ketree-0.1 (crate (name "ketree") (vers "0.1.0") (deps (list (crate-dep (name "ketos") (req "^0.10") (default-features #t) (kind 0)) (crate-dep (name "ketos_derive") (req "^0.10") (default-features #t) (kind 0)))) (hash "149cbk3d4q1w18impxad4wh1z64qwa0ylchr4hs5c3njxl0hmnmx")))

(define-public crate-ketree-0.1 (crate (name "ketree") (vers "0.1.1") (deps (list (crate-dep (name "ketos") (req "^0.10") (default-features #t) (kind 0)) (crate-dep (name "ketos_derive") (req "^0.10") (default-features #t) (kind 0)))) (hash "0b90g3mil596f7v0sanszlpp7p7k36qsg6x83c56rakdlih7kzdx")))

(define-public crate-ketree-0.1 (crate (name "ketree") (vers "0.1.2") (deps (list (crate-dep (name "ketos") (req "^0.10") (default-features #t) (kind 0)) (crate-dep (name "ketos_derive") (req "^0.10") (default-features #t) (kind 0)))) (hash "15j9svz0j27lm0j3irw383ag4g0azzjsiq1g6fh6yg6frb03wb3f")))

(define-public crate-ketree-0.1 (crate (name "ketree") (vers "0.1.3") (deps (list (crate-dep (name "ketos") (req "^0.10") (default-features #t) (kind 0)) (crate-dep (name "ketos_derive") (req "^0.10") (default-features #t) (kind 0)))) (hash "1vz11d3534j5j4jr8fmb154damrivgh3aniz9gq7y0lxc9dqz0il")))

(define-public crate-ketree-0.2 (crate (name "ketree") (vers "0.2.0") (deps (list (crate-dep (name "ketos") (req "^0.10") (default-features #t) (kind 0)) (crate-dep (name "ketos_derive") (req "^0.10") (default-features #t) (kind 0)))) (hash "1qh2r652117fsj1smfh55ap4nj4nr71w1c564spgqmgnw01wbcjp")))

(define-public crate-ketree-0.3 (crate (name "ketree") (vers "0.3.0") (deps (list (crate-dep (name "ketos") (req "^0.10") (default-features #t) (kind 0)) (crate-dep (name "ketos_derive") (req "^0.10") (default-features #t) (kind 0)))) (hash "0kijkg58xlxcc097bbhgv9qk4mdjrd5mg6mqwygzx6vji1aay3sp")))

(define-public crate-ketree-0.3 (crate (name "ketree") (vers "0.3.1") (deps (list (crate-dep (name "ketos") (req "^0.10") (default-features #t) (kind 0)) (crate-dep (name "ketos_derive") (req "^0.10") (default-features #t) (kind 0)))) (hash "12qwdhnp63zpqdfyhk0r9f9n30v19jxx55w4p3kwz68mrxdk1cmb")))

(define-public crate-ketree-0.3 (crate (name "ketree") (vers "0.3.2") (deps (list (crate-dep (name "ketos") (req "^0.10") (default-features #t) (kind 0)) (crate-dep (name "ketos_derive") (req "^0.10") (default-features #t) (kind 0)))) (hash "15lpqnszmka1684wg2i3fgsdaand3ymmzka5pqhbdb5s65cm6iv7")))

(define-public crate-ketree-0.4 (crate (name "ketree") (vers "0.4.0") (deps (list (crate-dep (name "ketos") (req "^0.10") (default-features #t) (kind 0)) (crate-dep (name "ketos_derive") (req "^0.10") (default-features #t) (kind 0)))) (hash "1rl0pw2vrs6rq15rfrnknypa2k8fisvrj3mv75zz7psjln2kmfd4")))

(define-public crate-ketree-0.4 (crate (name "ketree") (vers "0.4.1") (deps (list (crate-dep (name "ketos") (req "^0.10") (default-features #t) (kind 0)) (crate-dep (name "ketos_derive") (req "^0.10") (default-features #t) (kind 0)))) (hash "1zf9cn79vh8hhcd6qw674sagadmy001gj40m3zv8a8z10r5c2ypb")))

(define-public crate-ketree-0.4 (crate (name "ketree") (vers "0.4.2") (deps (list (crate-dep (name "ketos") (req "^0.10") (default-features #t) (kind 0)) (crate-dep (name "ketos_derive") (req "^0.10") (default-features #t) (kind 0)))) (hash "08gkg24y5iyr9b53ca0ijcn5jmjpdw196a93wn27b9r9nm472mh7")))

(define-public crate-ketree-0.4 (crate (name "ketree") (vers "0.4.3") (deps (list (crate-dep (name "ketos") (req "^0.10") (default-features #t) (kind 0)) (crate-dep (name "ketos_derive") (req "^0.10") (default-features #t) (kind 0)))) (hash "0fki2ccksk5vcnv43k9yiydkan7c918a08r1yaxkyzic13sh1jc8")))

(define-public crate-ketree-0.4 (crate (name "ketree") (vers "0.4.4") (deps (list (crate-dep (name "ketos") (req "^0.10") (default-features #t) (kind 0)) (crate-dep (name "ketos_derive") (req "^0.10") (default-features #t) (kind 0)))) (hash "0p9pqj6hg52wjmwqsa8gkmh0nppkq0gzs8mw9gi0pq6li7zawsya")))

(define-public crate-ketree-0.5 (crate (name "ketree") (vers "0.5.0") (deps (list (crate-dep (name "ketos") (req "^0.10") (default-features #t) (kind 0)) (crate-dep (name "ketos_derive") (req "^0.10") (default-features #t) (kind 0)))) (hash "0q6sqi1653xf5p2xcqgqz8q3qmsv762a78lyd156323kzzqda39g")))

(define-public crate-ketree-0.5 (crate (name "ketree") (vers "0.5.1") (deps (list (crate-dep (name "ketos") (req "^0.10") (default-features #t) (kind 0)) (crate-dep (name "ketos_derive") (req "^0.10") (default-features #t) (kind 0)))) (hash "0bjzzg43zzd2hbkpvn840xd38qff9c0jr5bw95bybkbbfarsa7k4")))

(define-public crate-ketree-0.5 (crate (name "ketree") (vers "0.5.2") (deps (list (crate-dep (name "ketos") (req "^0.10") (default-features #t) (kind 0)) (crate-dep (name "ketos_derive") (req "^0.10") (default-features #t) (kind 0)))) (hash "0sdxk9w55jwvbkyxzk7sn387h0nlinnj1394djzrpb8j7ngf5x21")))

