(define-module (crates-io ke co) #:use-module (crates-io))

(define-public crate-keco-0.1 (crate (name "keco") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^4.4.3") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "17d7cxy6savr9m6lv7pmq21wcqcgbiq9nmazh2bck7mb6n73n7d6")))

(define-public crate-keco-0.1 (crate (name "keco") (vers "0.1.1") (deps (list (crate-dep (name "clap") (req "^4.4.3") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "16qcz2s71vylzqlzyqyk9133zbcsdb80f9khdqqik49rsy7gj7ml")))

