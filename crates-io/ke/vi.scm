(define-module (crates-io ke vi) #:use-module (crates-io))

(define-public crate-kevin-0.0.1 (crate (name "kevin") (vers "0.0.1") (hash "12ghav8082kq3a7ybmpl28s1l6cpgawzk8zfy2mxrk1ycs408jk0")))

(define-public crate-kevinh_guessing_game-0.1 (crate (name "kevinh_guessing_game") (vers "0.1.0") (deps (list (crate-dep (name "rand") (req "^0.8.3") (default-features #t) (kind 0)))) (hash "1yr2ym5fyh9pqjfhqap9fqg4gjg9hm548jx4b65k5k7rzs58njnc")))

(define-public crate-kevinleptons-0.1 (crate (name "kevinleptons") (vers "0.1.0") (deps (list (crate-dep (name "rand") (req "^0.3.14") (default-features #t) (kind 0)))) (hash "1rcp0dsi3zyy6m3s16pb4l673vn387a0l2xv1krxsl1vka5nwgas") (yanked #t)))

