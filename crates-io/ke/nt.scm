(define-module (crates-io ke nt) #:use-module (crates-io))

(define-public crate-kent-0.0.0 (crate (name "kent") (vers "0.0.0") (hash "0351wcg52zlk27qnfj098d12vifbjfnh2405rxkb5mdkka0y56cf")))

(define-public crate-kent_test_crate-0.1 (crate (name "kent_test_crate") (vers "0.1.0") (hash "1rf1rq2q7zmxx8fq69j12fjkh0idc8wz2pln2srzm46big80lib0") (yanked #t)))

