(define-module (crates-io ke y-) #:use-module (crates-io))

(define-public crate-key-agreement-0.0.0 (crate (name "key-agreement") (vers "0.0.0") (hash "00ykwhzs3cwd54091iy7nxdmhf069p8c80skc65pf4nx8fsqzlag")))

(define-public crate-key-derive-0.1 (crate (name "key-derive") (vers "0.1.0") (deps (list (crate-dep (name "automod") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "dotenv") (req "^0.15") (default-features #t) (kind 2)) (crate-dep (name "envy") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "eyre") (req "^0.6.8") (default-features #t) (kind 0)) (crate-dep (name "hmac") (req "^0.12.1") (default-features #t) (kind 0)) (crate-dep (name "insta") (req "^1") (default-features #t) (kind 2)) (crate-dep (name "once_cell") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "secp256k1") (req "^0.27") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1") (features (quote ("serde_derive"))) (kind 0)) (crate-dep (name "sha2") (req "^0.10.6") (default-features #t) (kind 0)))) (hash "1531isxk5wdz8hl287wij0fl4q201lsmzbyp1234dihwi3i6fmlh") (yanked #t) (rust-version "1.70.0")))

(define-public crate-key-derive-0.1 (crate (name "key-derive") (vers "0.1.1") (deps (list (crate-dep (name "automod") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "dotenv") (req "^0.15") (default-features #t) (kind 2)) (crate-dep (name "envy") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "eyre") (req "^0.6.8") (default-features #t) (kind 0)) (crate-dep (name "hex") (req "^0.4.3") (default-features #t) (kind 0)) (crate-dep (name "hmac") (req "^0.12.1") (default-features #t) (kind 0)) (crate-dep (name "insta") (req "^1") (default-features #t) (kind 2)) (crate-dep (name "once_cell") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "secp256k1") (req "^0.27") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1") (features (quote ("serde_derive"))) (kind 0)) (crate-dep (name "sha2") (req "^0.10.6") (default-features #t) (kind 0)))) (hash "0vj9xjl9w29xyra8cnygly18yy4zawqg3j3wkajk3f2s99sxay2d") (rust-version "1.70.0")))

(define-public crate-key-exchange-0.0.0 (crate (name "key-exchange") (vers "0.0.0") (hash "0a55576ydv0f6py16lf4x80gnxnr7ndih0s2qbkc7qhwfkq51kw6")))

(define-public crate-key-format-0.0.0 (crate (name "key-format") (vers "0.0.0") (hash "07la7xy8fhxj46fn24bi67b2gxi3a8lfdv4mvs3kr9w925wigzzv")))

(define-public crate-key-lang-0.1 (crate (name "key-lang") (vers "0.1.0") (hash "0s4r7dnjmf9l5hk00ilk7x6sqrqbizjfrf42q89d73z0sjak8n9b")))

(define-public crate-key-lang-0.1 (crate (name "key-lang") (vers "0.1.1") (hash "1i1h5a76i4hg64h9bvcjxw8kfrs76dyzrqnyig15j69hvvbi4nla")))

(define-public crate-key-list-1 (crate (name "key-list") (vers "1.0.0") (hash "1av85ybnh2hq0q90dczqigp40z19yr6cg8h0sl1ki3g1v7qi03lw")))

(define-public crate-key-lock-0.1 (crate (name "key-lock") (vers "0.1.0") (deps (list (crate-dep (name "ntest") (req "^0.9.0") (default-features #t) (kind 2)) (crate-dep (name "tokio") (req "^1.23.0") (features (quote ("sync"))) (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.23.0") (features (quote ("rt-multi-thread" "macros"))) (default-features #t) (kind 2)))) (hash "0wqq0pdy6nglkgqck1wfnjz1582nw8kq2b6s9iq3ya1gs3gd4bvr")))

(define-public crate-key-mapping-0.0.0 (crate (name "key-mapping") (vers "0.0.0") (hash "0hkw50dcgnrbjhrfx0n46mwr8nch3qr0yfmbxarz3ij6dzcyn3rg")))

(define-public crate-key-mapping-0.1 (crate (name "key-mapping") (vers "0.1.0") (deps (list (crate-dep (name "phf") (req "^0.11") (kind 0)) (crate-dep (name "phf_codegen") (req "^0.11") (default-features #t) (kind 1)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (optional #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 1)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 1)))) (hash "01bjsv1pcrimacppazl1wmgxp4s25ja7cszs99i7wiymrscq6d8k") (features (quote (("std") ("default" "std")))) (rust-version "1.77.0")))

(define-public crate-key-mapping-0.2 (crate (name "key-mapping") (vers "0.2.0") (deps (list (crate-dep (name "phf") (req "^0.11") (kind 0)) (crate-dep (name "phf_codegen") (req "^0.11") (default-features #t) (kind 1)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (optional #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 1)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 1)) (crate-dep (name "usbd-hid") (req "^0.7") (optional #t) (default-features #t) (kind 0)))) (hash "00diiwfjhhja7wpsyyawjsy7fdpy933shn7wjslj1x0gvbpj971x") (features (quote (("std") ("default" "std")))) (rust-version "1.77.0")))

(define-public crate-key-mapping-0.2 (crate (name "key-mapping") (vers "0.2.1") (deps (list (crate-dep (name "phf") (req "^0.11") (kind 0)) (crate-dep (name "phf_codegen") (req "^0.11") (default-features #t) (kind 1)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (optional #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 1)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 1)) (crate-dep (name "usbd-hid") (req "^0.7") (optional #t) (default-features #t) (kind 0)))) (hash "1lap5n1gqiwpchhgpp8r0k8hc7y6vhflzwgxqw18ymqghlmhl64m") (features (quote (("std") ("default" "std")))) (rust-version "1.77.0")))

(define-public crate-key-mapping-0.2 (crate (name "key-mapping") (vers "0.2.2") (deps (list (crate-dep (name "hex") (req "^0.4.3") (default-features #t) (kind 1)) (crate-dep (name "phf") (req "^0.11") (kind 0)) (crate-dep (name "phf_codegen") (req "^0.11") (default-features #t) (kind 1)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (optional #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 1)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 1)) (crate-dep (name "usbd-hid") (req "^0.7") (optional #t) (default-features #t) (kind 0)))) (hash "1ars8d8p072hkqlzjdkwdzcx8lvrm9560bsybfs4qnzy8f6791jn") (features (quote (("std") ("default" "std")))) (rust-version "1.77.0")))

(define-public crate-key-mapping-0.3 (crate (name "key-mapping") (vers "0.3.0") (deps (list (crate-dep (name "hex") (req "^0.4.3") (default-features #t) (kind 1)) (crate-dep (name "phf") (req "^0.11") (kind 0)) (crate-dep (name "phf_codegen") (req "^0.11") (default-features #t) (kind 1)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (optional #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 1)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 1)) (crate-dep (name "usbd-hid") (req "^0.7") (optional #t) (default-features #t) (kind 0)))) (hash "05684l6qc3wsb1c475nm62p336p9hxs10ayg6d5h79r73mid015d") (features (quote (("std") ("default" "std")))) (rust-version "1.77.0")))

(define-public crate-key-mapping-0.4 (crate (name "key-mapping") (vers "0.4.0") (deps (list (crate-dep (name "hex") (req "^0.4.3") (default-features #t) (kind 1)) (crate-dep (name "phf") (req "^0.11") (kind 0)) (crate-dep (name "phf_codegen") (req "^0.11") (default-features #t) (kind 1)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (optional #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 1)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 1)) (crate-dep (name "usbd-hid") (req "^0.7") (optional #t) (default-features #t) (kind 0)))) (hash "015whh65p76gzi5acigyai5d6ny4l9bmzwsi8rsxm5iaakpm2r0g") (features (quote (("std") ("default" "std")))) (rust-version "1.77.0")))

(define-public crate-key-message-channel-0.1 (crate (name "key-message-channel") (vers "0.1.0") (deps (list (crate-dep (name "loom") (req "^0.5") (default-features #t) (target "cfg(loom)") (kind 0)) (crate-dep (name "parking_lot") (req "^0.12") (default-features #t) (kind 0)) (crate-dep (name "riffy") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "1m5aksbcjxmvjppahmblyxvdm2cpp4931wlrlwsf7xrdm52ldn10")))

(define-public crate-key-mutex-0.1 (crate (name "key-mutex") (vers "0.1.0") (deps (list (crate-dep (name "dashmap") (req "^5.5.3") (default-features #t) (kind 0)) (crate-dep (name "guardian") (req "^1.1.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.36.0") (features (quote ("sync"))) (optional #t) (default-features #t) (kind 0)))) (hash "13qplghk5hpkcq2c1l89vdn552q07na2jxyg46xzq4mypfdh3ks6") (features (quote (("default" "std")))) (v 2) (features2 (quote (("tokio" "dep:tokio") ("std" "dep:guardian"))))))

(define-public crate-key-mutex-0.1 (crate (name "key-mutex") (vers "0.1.1") (deps (list (crate-dep (name "dashmap") (req "^5.5.3") (default-features #t) (kind 0)) (crate-dep (name "guardian") (req "^1.1.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.36.0") (features (quote ("sync"))) (optional #t) (default-features #t) (kind 0)))) (hash "17qml9a494r1m86hlg7dh04r22ap4gq9vp8x3crz3pgp9fl8rdgk") (features (quote (("default" "std")))) (v 2) (features2 (quote (("tokio" "dep:tokio") ("std" "dep:guardian"))))))

(define-public crate-key-mutex-0.1 (crate (name "key-mutex") (vers "0.1.2") (deps (list (crate-dep (name "dashmap") (req "^5.5.3") (default-features #t) (kind 0)) (crate-dep (name "guardian") (req "^1.1.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.36.0") (features (quote ("sync"))) (optional #t) (default-features #t) (kind 0)))) (hash "1a1fs8cxqh6059w7c72pdlxb29vm3fv07wlzakamqddn3kzgplgs") (features (quote (("default" "std")))) (v 2) (features2 (quote (("tokio" "dep:tokio") ("std" "dep:guardian"))))))

(define-public crate-key-mutex-0.1 (crate (name "key-mutex") (vers "0.1.3") (deps (list (crate-dep (name "dashmap") (req "^5.5.3") (default-features #t) (kind 0)) (crate-dep (name "guardian") (req "^1.1.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.36.0") (features (quote ("sync"))) (optional #t) (default-features #t) (kind 0)))) (hash "0ysm30l12q1vplh3kh3fp3fv3y0d75yi96926dlyxh85zxmg5vxn") (features (quote (("default" "std")))) (v 2) (features2 (quote (("tokio" "dep:tokio") ("std" "dep:guardian"))))))

(define-public crate-key-native-0.1 (crate (name "key-native") (vers "0.1.0") (hash "0dqc4m8mpcxhcf010l6fk6b8rxgywr1c635m3dhbng2hzs5al5ix")))

(define-public crate-key-native-0.1 (crate (name "key-native") (vers "0.1.1") (hash "0zkfflaihyc7fc46qhi591klp0rrhlzmdjclbin3ma8pz38j4wy3")))

(define-public crate-key-native-0.1 (crate (name "key-native") (vers "0.1.5") (hash "0rk7ib53b3m5qj8bfm1h7gay7cxzn7c8nrnln2g7h6jh690j0crh")))

(define-public crate-key-native-0.1 (crate (name "key-native") (vers "0.1.6") (hash "18h3ms4m1ri1l6wm0x80ggq2ykrv4k0fmfgj4wy7nwf85rq1varf")))

(define-public crate-key-native-0.1 (crate (name "key-native") (vers "0.1.61") (hash "1dj2661946xqjqncy4r6a15k6lh7gmcigmwri3as594q75cks2a5")))

(define-public crate-key-node-list-0.0.1 (crate (name "key-node-list") (vers "0.0.1") (hash "1mzmdcv47ywdqi5gb3i43z6ka96lwgn7xdk9c28dr1i1s17595c4")))

(define-public crate-key-node-list-0.0.2 (crate (name "key-node-list") (vers "0.0.2") (hash "1yhyz4bzm75ss02p201h7v6zxgmwr95p2nhzribwmjayj0gq411v")))

(define-public crate-key-node-list-0.0.3 (crate (name "key-node-list") (vers "0.0.3") (hash "1nfiz3npjkvw31ja9rmkjrp6yzw9plqda4117ycaazk18i9n5bkb")))

(define-public crate-key-node-list-0.0.4 (crate (name "key-node-list") (vers "0.0.4") (hash "11byl2aw670zav1s4fyiigrlq546pxj2134dw4f3ra2inbs4nq29")))

(define-public crate-key-node-list-0.0.5 (crate (name "key-node-list") (vers "0.0.5") (hash "15b2ljkk54cwi5462v2m6j72snxyafaipa8wpfnk5bln4m9fwgv5")))

(define-public crate-key-path-0.1 (crate (name "key-path") (vers "0.1.0") (hash "188644p48f01dscpb4568y250j0lm28a63vnbsdrrlpr9jqf6a8c")))

(define-public crate-key-path-0.1 (crate (name "key-path") (vers "0.1.1") (hash "0mj3xc6bfhmj800xx4hlxkiykxd3cgar9r0rsnyiv4mgs0sdhkvf")))

(define-public crate-key-path-0.1 (crate (name "key-path") (vers "0.1.2") (hash "0h1bsvzppqkczldhi8krzc3gppk67f83ssj3cw2qapx1w7z28k6a")))

(define-public crate-key-path-0.1 (crate (name "key-path") (vers "0.1.3") (hash "0kjzzzvkbf76bz9x06kzq6bbjb45y9xhb2zam901akmlff5hapgl")))

(define-public crate-key-path-0.1 (crate (name "key-path") (vers "0.1.4") (hash "0nkm1b9kb2cgbvm5q88p5h7jn7449lnh79gv9r7bs9vpny5dydiv")))

(define-public crate-key-path-0.1 (crate (name "key-path") (vers "0.1.5") (hash "1ak0b98rjqafkjbvk1alb0jlrlk28vx4zqwd6zh4yqbp2czshiyb")))

(define-public crate-key-path-0.1 (crate (name "key-path") (vers "0.1.6") (hash "0r4af3jg9w6rvilcd6gfl32mbj05bfp9jy9520b38xshpkm4pjrf")))

(define-public crate-key-path-0.1 (crate (name "key-path") (vers "0.1.7") (hash "0kp1hd3vq1jm517anym2p9q4dvda9wvz3yljql43k973pzgshlkp")))

(define-public crate-key-path-0.1 (crate (name "key-path") (vers "0.1.8") (hash "0zw1gyvbv1ab862j156rn6sqcxfm3197hqknim6nxkr080y5r6qm")))

(define-public crate-key-path-0.1 (crate (name "key-path") (vers "0.1.9") (hash "0m46mj72bbwlsklp2h7514n50wk447w133gkcg2kmvzhgl6dl66s")))

(define-public crate-key-path-0.1 (crate (name "key-path") (vers "0.1.10") (hash "185zim7qqdh1p0swk3mrb0ivxdy0kswis0shq7pljkaj3z6dkrkf")))

(define-public crate-key-path-0.1 (crate (name "key-path") (vers "0.1.11") (hash "1f0bzxxslmc4rbngvljgcici9q93x3jdn3qlm93my7b3817vvvim")))

(define-public crate-key-path-0.2 (crate (name "key-path") (vers "0.2.0") (hash "0kp8j6m1l94aaahfh8w3ardgiqzxayarb0qxyfb121mpksya6faw")))

(define-public crate-key-path-0.2 (crate (name "key-path") (vers "0.2.1") (hash "0xz0lzlh36f5qd3ka3lcfwxnk4rvbwl8xvfkf5s14qm8fdh330km")))

(define-public crate-key-path-0.2 (crate (name "key-path") (vers "0.2.2") (hash "1m2imw8biwiac04kfiwjkxjlx3jjcn11iqwj4swbi1dh2alddgrd")))

(define-public crate-key-rwlock-0.1 (crate (name "key-rwlock") (vers "0.1.0") (deps (list (crate-dep (name "tokio") (req "^1.27.0") (features (quote ("sync"))) (kind 0)) (crate-dep (name "tokio") (req "^1.27.0") (features (quote ("rt-multi-thread" "macros"))) (kind 2)))) (hash "15q0iwcxn67skg2nskw5zd9dnss7hlshgd76y5a45n5dqh278jwm") (rust-version "1.63.0")))

(define-public crate-key-rwlock-0.1 (crate (name "key-rwlock") (vers "0.1.1") (deps (list (crate-dep (name "tokio") (req "^1.27.0") (features (quote ("sync"))) (kind 0)) (crate-dep (name "tokio") (req "^1.27.0") (features (quote ("rt-multi-thread" "macros"))) (kind 2)))) (hash "1c1ab4y5522x36143xphk4fjrwkjp3wbb80diyr46vqwxy30hwdf") (rust-version "1.63.0")))

(define-public crate-key-rwlock-0.1 (crate (name "key-rwlock") (vers "0.1.2") (deps (list (crate-dep (name "tokio") (req "^1.27.0") (features (quote ("sync"))) (kind 0)) (crate-dep (name "tokio") (req "^1.27.0") (features (quote ("rt-multi-thread" "macros"))) (kind 2)))) (hash "0bbwbcc6v6ldamxmzq0cbg9yag7v93wda6x3x2gm97ryvwdn0myf") (rust-version "1.63.0")))

(define-public crate-key-share-0.1 (crate (name "key-share") (vers "0.1.0") (deps (list (crate-dep (name "generic-ec") (req "^0.1.3") (default-features #t) (kind 0)) (crate-dep (name "generic-ec-zkp") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "hex") (req "^0.4") (features (quote ("serde"))) (optional #t) (kind 0)) (crate-dep (name "rand_core") (req "^0.6") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "rand_core") (req "^0.6") (features (quote ("getrandom"))) (default-features #t) (kind 2)) (crate-dep (name "serde") (req "^1") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde_with") (req "^2") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "slip-10") (req "^0.1") (features (quote ("std"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "udigest") (req "^0.1") (features (quote ("derive"))) (optional #t) (kind 0)))) (hash "1yr1fvj9s177wavc29gx5ihshk4idmlvk23fqqj2bzp2hq36y2y5") (features (quote (("hd-wallets" "slip-10")))) (yanked #t) (v 2) (features2 (quote (("udigest" "dep:udigest") ("spof" "dep:rand_core") ("serde" "dep:serde" "serde_with" "hex"))))))

(define-public crate-key-share-0.2 (crate (name "key-share") (vers "0.2.0") (deps (list (crate-dep (name "generic-ec") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "generic-ec-zkp") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "hex") (req "^0.4") (features (quote ("serde"))) (optional #t) (kind 0)) (crate-dep (name "rand_core") (req "^0.6") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "rand_core") (req "^0.6") (features (quote ("getrandom"))) (default-features #t) (kind 2)) (crate-dep (name "serde") (req "^1") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde_with") (req "^2") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "slip-10") (req "^0.2") (features (quote ("std"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "udigest") (req "^0.1") (features (quote ("derive"))) (optional #t) (kind 0)))) (hash "060r07x9fjlp4n05v114gp562v0anvfdjz4jygnbwrm58zc4diq5") (features (quote (("hd-wallets" "slip-10")))) (yanked #t) (v 2) (features2 (quote (("udigest" "dep:udigest") ("spof" "dep:rand_core") ("serde" "dep:serde" "serde_with" "hex"))))))

(define-public crate-key-share-0.2 (crate (name "key-share") (vers "0.2.1") (deps (list (crate-dep (name "generic-ec") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "generic-ec-zkp") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "hex") (req "^0.4") (features (quote ("serde"))) (optional #t) (kind 0)) (crate-dep (name "rand_core") (req "^0.6") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "rand_core") (req "^0.6") (features (quote ("getrandom"))) (default-features #t) (kind 2)) (crate-dep (name "serde") (req "^1") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde_with") (req "^2") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "slip-10") (req "^0.2") (features (quote ("std"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "udigest") (req "^0.1") (features (quote ("derive"))) (optional #t) (kind 0)))) (hash "1n2ffcqrfiwq884v2vkdx82ydkc18jgp5l7gk13sl0in4qp30yf7") (features (quote (("hd-wallets" "slip-10")))) (v 2) (features2 (quote (("udigest" "dep:udigest") ("spof" "dep:rand_core") ("serde" "dep:serde" "serde_with" "hex"))))))

(define-public crate-key-share-0.2 (crate (name "key-share") (vers "0.2.2") (deps (list (crate-dep (name "displaydoc") (req "^0.2") (kind 0)) (crate-dep (name "generic-ec") (req "^0.2") (features (quote ("alloc"))) (kind 0)) (crate-dep (name "generic-ec-zkp") (req "^0.2") (features (quote ("alloc"))) (kind 0)) (crate-dep (name "hex") (req "^0.4") (features (quote ("serde"))) (optional #t) (kind 0)) (crate-dep (name "rand_core") (req "^0.6") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "rand_core") (req "^0.6") (features (quote ("getrandom"))) (default-features #t) (kind 2)) (crate-dep (name "serde") (req "^1") (features (quote ("alloc" "derive"))) (optional #t) (kind 0)) (crate-dep (name "serde_with") (req "^2") (features (quote ("alloc"))) (optional #t) (kind 0)) (crate-dep (name "slip-10") (req "^0.2") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "udigest") (req "^0.1") (features (quote ("alloc" "derive"))) (optional #t) (kind 0)))) (hash "0naa5dpxk8lj2aza2v31lqslsw1d9ckmsdjjnag341vmvr46lbdr") (features (quote (("hd-wallets" "slip-10") ("default" "std")))) (v 2) (features2 (quote (("udigest" "dep:udigest" "generic-ec/udigest") ("std" "dep:thiserror") ("spof" "dep:rand_core") ("serde" "dep:serde" "serde_with" "hex" "generic-ec/serde"))))))

(define-public crate-key-share-0.2 (crate (name "key-share") (vers "0.2.3") (deps (list (crate-dep (name "displaydoc") (req "^0.2") (kind 0)) (crate-dep (name "generic-ec") (req "^0.2.3") (features (quote ("alloc"))) (kind 0)) (crate-dep (name "generic-ec-zkp") (req "^0.2") (features (quote ("alloc"))) (kind 0)) (crate-dep (name "hex") (req "^0.4") (features (quote ("serde"))) (optional #t) (kind 0)) (crate-dep (name "rand_core") (req "^0.6") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "rand_core") (req "^0.6") (features (quote ("getrandom"))) (default-features #t) (kind 2)) (crate-dep (name "serde") (req "^1") (features (quote ("alloc" "derive"))) (optional #t) (kind 0)) (crate-dep (name "serde_with") (req "^2") (features (quote ("alloc"))) (optional #t) (kind 0)) (crate-dep (name "slip-10") (req "^0.2") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "udigest") (req "^0.1") (features (quote ("alloc" "derive"))) (optional #t) (kind 0)))) (hash "1k0168c9z748d3pvlvnfcy8a27n2xsnx58sachlqbcawzj9py8rh") (features (quote (("hd-wallets" "slip-10") ("default" "std")))) (v 2) (features2 (quote (("udigest" "dep:udigest" "generic-ec/udigest") ("std" "dep:thiserror") ("spof" "dep:rand_core") ("serde" "dep:serde" "serde_with" "hex" "generic-ec/serde"))))))

(define-public crate-key-utils-1 (crate (name "key-utils") (vers "1.0.0") (deps (list (crate-dep (name "bs58") (req "^0.4.0") (features (quote ("check"))) (default-features #t) (kind 0)) (crate-dep (name "secp256k1") (req "^0.28.2") (features (quote ("alloc" "rand" "rand-std"))) (kind 0)) (crate-dep (name "serde") (req "^1.0.89") (features (quote ("derive" "alloc"))) (kind 0)) (crate-dep (name "toml") (req "^0.5.6") (kind 2)))) (hash "13h5kg3wqidlb3g490zyg2f6szrbj8lki87q4jds9ivhjbjysdhk")))

(define-public crate-key-utils-1 (crate (name "key-utils") (vers "1.1.0") (deps (list (crate-dep (name "bs58") (req "^0.4.0") (features (quote ("check"))) (default-features #t) (kind 0)) (crate-dep (name "secp256k1") (req "^0.28.2") (features (quote ("alloc" "rand" "rand-std"))) (kind 0)) (crate-dep (name "serde") (req "^1.0.89") (features (quote ("derive" "alloc"))) (kind 0)) (crate-dep (name "toml") (req "^0.5.6") (kind 2)))) (hash "0p53sksfy83wsawwsnxk4ivqmgjca2wzv5dzlqi9fm2ffxhp3s01")))

(define-public crate-key-vec-0.2 (crate (name "key-vec") (vers "0.2.0") (hash "1mjdad8mlrhzfsnbwr2mdsgxkgfi4crnwizfi0hxb31ca8acwzfc")))

(define-public crate-key-vec-0.2 (crate (name "key-vec") (vers "0.2.1") (hash "03bxyb14y5zdqn3f524vnn78hz0q7586sq8qjb5ima1ll54djcbj")))

(define-public crate-key-vec-0.2 (crate (name "key-vec") (vers "0.2.2") (deps (list (crate-dep (name "serde") (req "1.*") (features (quote ("serde_derive"))) (optional #t) (default-features #t) (kind 0)))) (hash "0ialli2qz5w0sssn6hs7awcgkjp89sbwjp8ymw48bzkqdlxh3dmz")))

(define-public crate-key-vec-0.2 (crate (name "key-vec") (vers "0.2.3") (deps (list (crate-dep (name "serde") (req "1.*") (features (quote ("serde_derive"))) (optional #t) (default-features #t) (kind 0)))) (hash "0i8vv2zc1lb0xx8hppc8c717lbby4mldmnmw67jkswaibgm9a99d")))

(define-public crate-key-vec-0.2 (crate (name "key-vec") (vers "0.2.4") (deps (list (crate-dep (name "serde") (req "1.*") (features (quote ("serde_derive"))) (optional #t) (default-features #t) (kind 0)))) (hash "07gl8s267v7vnc5f02vm87ha3gj6qfcx5rflqdj9ydkpwgyjxqs6")))

(define-public crate-key-vec-0.2 (crate (name "key-vec") (vers "0.2.5") (deps (list (crate-dep (name "serde") (req "1.*") (features (quote ("serde_derive"))) (optional #t) (default-features #t) (kind 0)))) (hash "13na85li1aam8b2zp95f7z42ddnk8l3qcf2yczi4583p7hcwnbff")))

(define-public crate-key-vec-0.2 (crate (name "key-vec") (vers "0.2.6") (deps (list (crate-dep (name "serde") (req "1.*") (features (quote ("serde_derive"))) (optional #t) (default-features #t) (kind 0)))) (hash "1d1b5ya6g6biixdnqnq9pml7a4kz1xwidphvcbg06phi8bik0q0f")))

(define-public crate-key-vec-0.3 (crate (name "key-vec") (vers "0.3.0") (deps (list (crate-dep (name "serde") (req "1.*") (features (quote ("serde_derive"))) (optional #t) (default-features #t) (kind 0)))) (hash "1kigbnycgwridi8qbb54c092zj34nn2cjs3a28z3gnp66a653cs5")))

(define-public crate-key-vec-0.3 (crate (name "key-vec") (vers "0.3.1") (deps (list (crate-dep (name "serde") (req "1.*") (features (quote ("serde_derive"))) (optional #t) (default-features #t) (kind 0)))) (hash "0wb0f3i5i875ifmikan7x54h3hsckh4pis0qr6waavqbhwih8414")))

(define-public crate-key-vec-0.4 (crate (name "key-vec") (vers "0.4.0") (deps (list (crate-dep (name "serde") (req "1.*") (features (quote ("serde_derive"))) (optional #t) (default-features #t) (kind 0)))) (hash "1s3jhb1wnl6dkl0xjpwp9jn962hpyg7pp37shyr06n0s75pzd34x")))

(define-public crate-key-vec-0.4 (crate (name "key-vec") (vers "0.4.1") (deps (list (crate-dep (name "serde") (req "1.*") (features (quote ("serde_derive"))) (optional #t) (default-features #t) (kind 0)))) (hash "017i29y3kbx9ch6nm8d16d8srnf0wn13scpvn3l1wx59qb5qm6df")))

(define-public crate-key-wasm-0.1 (crate (name "key-wasm") (vers "0.1.0") (hash "0133a5lnr64d2lyf8v1a0vl0d8ca0fsnpa9w5s3wdf8gdqppcggz")))

