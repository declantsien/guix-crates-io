(define-module (crates-io ke rn) #:use-module (crates-io))

(define-public crate-kern-0.1 (crate (name "kern") (vers "0.1.0") (hash "0d94hgwvyaqf7g9agmy42wikkcd2mjd26mvyi4g0vkzq681wrgjf")))

(define-public crate-kern-0.1 (crate (name "kern") (vers "0.1.1") (hash "13d3s0pravxf39d3jvwflv5qqvy7l7w5rz37jy34r122hy7l5hj7")))

(define-public crate-kern-0.2 (crate (name "kern") (vers "0.2.0") (hash "061b6kws7kg88yci0bwsgprkiknjzqbi33wnpmdjard3wh2znf1y")))

(define-public crate-kern-0.2 (crate (name "kern") (vers "0.2.1") (hash "1qyc16zj3b5rmqyfklmfd8xdpjwymybqqlnffz9v2kyfl7j6lp9h")))

(define-public crate-kern-0.2 (crate (name "kern") (vers "0.2.2") (hash "08h3kyy6khdbkrdykqlm79l30c3bwbblmz4bja11gv1hzmwmlvfs")))

(define-public crate-kern-0.3 (crate (name "kern") (vers "0.3.0") (hash "0dc4kd4pizakamr3fkjiny1grl845747w4lbr1bj9yhfykyzwa80")))

(define-public crate-kern-0.3 (crate (name "kern") (vers "0.3.1") (hash "1si78gvxjdqkkqvks6lf3f7rvrmjpq6qwvw1lf8dvrlqlyynz03k")))

(define-public crate-kern-0.3 (crate (name "kern") (vers "0.3.2") (hash "1390h89chvzda60rg3z1bycvxnyxj40iwws8m73pzx669gh3qvs3")))

(define-public crate-kern-0.4 (crate (name "kern") (vers "0.4.0") (hash "0395zivcihaj62c5z596m75i9kmj9lw1s82484543zz240wgrwws")))

(define-public crate-kern-1 (crate (name "kern") (vers "1.0.0") (hash "0qn3pi04g00620ylcdmjrwx5bhhsjkn799ljq5r5wgq2lbwif304")))

(define-public crate-kern-1 (crate (name "kern") (vers "1.0.1") (hash "0z5gidzkjfc3bbghlgzzkqla0ngljygd1jw6qf83zb5am5n785i6")))

(define-public crate-kern-1 (crate (name "kern") (vers "1.0.2") (hash "1h788l9dyl2yjdmjk9xi0cl3bmpbpgm7hkfag9bac8jdciakh04x")))

(define-public crate-kern-1 (crate (name "kern") (vers "1.0.3") (hash "0g03nnjkp06q0rnfrzkpq8srm3n6md93i2i62l0d6856c7nzbxin")))

(define-public crate-kern-1 (crate (name "kern") (vers "1.1.0") (hash "1l8zr8cp8j0gk1dklzqdpdjv9jlxc4m853gm4whs81dl6yx5q0g2")))

(define-public crate-kern-1 (crate (name "kern") (vers "1.1.1") (hash "14vgd6wp7hnm4vvkdh6bv0qrg64h60xjywng56wkl199a65qhkzc")))

(define-public crate-kern-1 (crate (name "kern") (vers "1.1.2") (hash "133yqjj0pvbxpdhkanng5lzdkk1a6rs3wnrav630rm6yw3igs6h8")))

(define-public crate-kern-1 (crate (name "kern") (vers "1.1.3") (hash "0ywa1ixs5v9ybzbm8zqdi4sbjqg7ffdbjivjng8i0x8l2hmym8lf")))

(define-public crate-kern-1 (crate (name "kern") (vers "1.1.4") (hash "1xdzc5anfz416qy3yyjdjizgnmn1gb0gnk7gmcj1qdrkkh8sq67x")))

(define-public crate-kern-1 (crate (name "kern") (vers "1.1.5") (hash "10svfv9hyy15vs3mrvi4jc3zhydjjhq8ap900x56frcwackmyh8i")))

(define-public crate-kern-1 (crate (name "kern") (vers "1.1.6") (hash "0v9lj1ma7wzmxyryifg4d6128rbklx9gwprlnd441gcmad32j493")))

(define-public crate-kern-1 (crate (name "kern") (vers "1.2.0") (deps (list (crate-dep (name "rustls") (req "^0.18.1") (optional #t) (default-features #t) (kind 0)))) (hash "198wk4k2isp4g8gnnqlmqp8jmammlqzc795iajzdd4bfc2rjai8m") (features (quote (("http" "rustls") ("default"))))))

(define-public crate-kern-1 (crate (name "kern") (vers "1.2.1") (deps (list (crate-dep (name "rustls") (req "^0.19.0") (optional #t) (default-features #t) (kind 0)))) (hash "0vv5m5dc4q1g4wbs11sf1ldyy6sx9vwr8irx87apg5d1icydyhy3") (features (quote (("http" "rustls") ("default"))))))

(define-public crate-kern-1 (crate (name "kern") (vers "1.2.2") (deps (list (crate-dep (name "rustls") (req "^0.19.0") (optional #t) (default-features #t) (kind 0)))) (hash "17ql2cn8fzh0fc3mhd8z9rk3zgsdaib23zg1lrnacnnc89qfznfw") (features (quote (("http" "rustls") ("default"))))))

(define-public crate-kern-1 (crate (name "kern") (vers "1.2.3") (deps (list (crate-dep (name "rustls") (req "^0.19.0") (optional #t) (default-features #t) (kind 0)))) (hash "04lv57zgi0ks9mdisy4v5qsr12b9w77x1x77jf7fzfrd380f1135") (features (quote (("http" "rustls") ("default"))))))

(define-public crate-kern-1 (crate (name "kern") (vers "1.3.0") (deps (list (crate-dep (name "rustls") (req "^0.19.1") (optional #t) (default-features #t) (kind 0)))) (hash "17yzj8rk32bmrfqdzqw35arphy0d4dcb5j18kf1m95m6rmsbalqb") (features (quote (("http" "rustls") ("default"))))))

(define-public crate-kern-1 (crate (name "kern") (vers "1.3.1") (deps (list (crate-dep (name "rustls") (req "^0.19.1") (optional #t) (default-features #t) (kind 0)))) (hash "1h9sl50wsr73283pw43cbz1d8am0fqinzrz6hbp9md7xspba418i") (features (quote (("http" "rustls") ("default"))))))

(define-public crate-kern-1 (crate (name "kern") (vers "1.3.2") (deps (list (crate-dep (name "rustls") (req "^0.19.1") (optional #t) (default-features #t) (kind 0)))) (hash "0kxn7y9nmr8hfs29gaiw7vdw5x65gvzls41laj1r8c0gpvvqr2bv") (features (quote (("http" "rustls") ("default"))))))

(define-public crate-kern-1 (crate (name "kern") (vers "1.3.3") (deps (list (crate-dep (name "rustls") (req "^0.19.1") (optional #t) (default-features #t) (kind 0)))) (hash "1bvv0q5hqzv9lv3x227k06jvx9bv3knb52c18s5rjii5fbrm9cnp") (features (quote (("http" "rustls") ("default"))))))

(define-public crate-kern-1 (crate (name "kern") (vers "1.3.4") (deps (list (crate-dep (name "rustls") (req "^0.20.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "rustls-pemfile") (req "^0.2.1") (optional #t) (default-features #t) (kind 0)))) (hash "1rzn86dap2kyqdjj2riz6gikd6dsvl0y6mqmcfxg6w19gp56fsz5") (features (quote (("http" "rustls" "rustls-pemfile") ("default"))))))

(define-public crate-kern-1 (crate (name "kern") (vers "1.4.0") (deps (list (crate-dep (name "rustls") (req "^0.20.2") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "rustls-pemfile") (req "^0.2.1") (optional #t) (default-features #t) (kind 0)))) (hash "0vdlmq2xmb1m76jc992jgrw5lkx1nzphsz94ggpzzd8q500vxwm9") (features (quote (("http" "rustls" "rustls-pemfile") ("default"))))))

(define-public crate-kern-1 (crate (name "kern") (vers "1.4.1") (deps (list (crate-dep (name "rustls") (req "^0.20.6") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "rustls-pemfile") (req "^1.0.1") (optional #t) (default-features #t) (kind 0)))) (hash "0szlqy8fx5mr02qwba5j323cq8dk1sq864b6w5mp119skspx7sha") (features (quote (("http" "rustls" "rustls-pemfile") ("default"))))))

(define-public crate-kern-1 (crate (name "kern") (vers "1.5.0") (deps (list (crate-dep (name "rustls") (req "^0.20.6") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "rustls-pemfile") (req "^1.0.1") (optional #t) (default-features #t) (kind 0)))) (hash "0lm0pvw072h24jrdyk53a8fhi0h98k28iic2k70v68cr8ac6dnl7") (features (quote (("http" "rustls" "rustls-pemfile") ("default"))))))

(define-public crate-kern-1 (crate (name "kern") (vers "1.5.1") (deps (list (crate-dep (name "rustls") (req "^0.20.6") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "rustls-pemfile") (req "^1.0.1") (optional #t) (default-features #t) (kind 0)))) (hash "18jpl0an4h6smzkzah55h9c1p722hb9p19pp7wh09nds281bniqp") (features (quote (("http" "rustls" "rustls-pemfile") ("default"))))))

(define-public crate-kern-1 (crate (name "kern") (vers "1.6.0") (deps (list (crate-dep (name "rustls") (req "^0.21.1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "rustls-pemfile") (req "^1.0.2") (optional #t) (default-features #t) (kind 0)))) (hash "1429ia49f8jjks0ikqahiyznz9rnq1l1rlhmkyrzavxmwl3pfbsb") (features (quote (("http" "rustls" "rustls-pemfile") ("default"))))))

(define-public crate-kern-1 (crate (name "kern") (vers "1.6.1") (deps (list (crate-dep (name "rustls") (req "^0.22.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "rustls-pemfile") (req "^2.0.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "rustls-pki-types") (req "^1.0.0") (optional #t) (default-features #t) (kind 0)))) (hash "1cqv0r4qss346bbdhfxsxyqw2fwjvlh1r4gy2pwpjb2jkd3fyg3m") (features (quote (("http" "rustls" "rustls-pemfile" "rustls-pki-types") ("default"))))))

(define-public crate-kern-1 (crate (name "kern") (vers "1.6.2") (deps (list (crate-dep (name "rustls") (req "^0.22.1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "rustls-pemfile") (req "^2.0.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "rustls-pki-types") (req "^1.0.1") (optional #t) (default-features #t) (kind 0)))) (hash "0px2v63ipnxbpmnf5x179q8aq6fhjh9lb1j98gyh1p8x6mc5738n") (features (quote (("http" "rustls" "rustls-pemfile" "rustls-pki-types") ("default"))))))

(define-public crate-kern-1 (crate (name "kern") (vers "1.6.3") (deps (list (crate-dep (name "rustls") (req "^0.22.2") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "rustls-pemfile") (req "^2.0.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "rustls-pki-types") (req "^1.1.0") (optional #t) (default-features #t) (kind 0)))) (hash "0y50752xsswrw3mr3v1gqgyrz0wlryamg4pac716641js0mqc3j3") (features (quote (("http" "rustls" "rustls-pemfile" "rustls-pki-types") ("default"))))))

(define-public crate-kern-1 (crate (name "kern") (vers "1.6.4") (deps (list (crate-dep (name "rustls") (req "^0.22.2") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "rustls-pemfile") (req "^2.0.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "rustls-pki-types") (req "^1.1.0") (optional #t) (default-features #t) (kind 0)))) (hash "0p85l453w0maa57ihvpcq4md7fdy3s4s5inhj04iwqibq11rfgnd") (features (quote (("http" "rustls" "rustls-pemfile" "rustls-pki-types") ("default"))))))

(define-public crate-kern-1 (crate (name "kern") (vers "1.7.0") (deps (list (crate-dep (name "rustls") (req "^0.22.2") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "rustls-pemfile") (req "^2.0.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "rustls-pki-types") (req "^1.2.0") (optional #t) (default-features #t) (kind 0)))) (hash "072169mlw41fn1fjrfhibw2dks57aac50qn2lsxaph5yywxc2nj6") (features (quote (("tls" "rustls" "rustls-pemfile" "rustls-pki-types") ("default"))))))

(define-public crate-kern-1 (crate (name "kern") (vers "1.7.1") (deps (list (crate-dep (name "rustls") (req "^0.23.1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "rustls-pemfile") (req "^2.1.1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "rustls-pki-types") (req "^1.3.1") (optional #t) (default-features #t) (kind 0)))) (hash "0p58ccb4vrfvlzrbzn5qjjm6khmkw681lclm7s0rddrkqc5xcr6j") (features (quote (("tls" "rustls" "rustls-pemfile" "rustls-pki-types") ("default"))))))

(define-public crate-kernal-0.1 (crate (name "kernal") (vers "0.1.0") (hash "197yr9wgiwa72mpww115haq5wqfv1jh96s44sv0sm68wbkwbdg42")))

(define-public crate-kernal-0.2 (crate (name "kernal") (vers "0.2.0") (deps (list (crate-dep (name "criterion") (req "^0.5") (default-features #t) (kind 2)))) (hash "0q2s7vnwlns4x4rkpvdwizqkfz9h7m4ai6h8d6sg5dpaxngxxfmb")))

(define-public crate-kernal-0.3 (crate (name "kernal") (vers "0.3.0") (deps (list (crate-dep (name "criterion") (req "^0.5") (default-features #t) (kind 2)))) (hash "0c58kclnshgw15z419km4x2ap5nm4kam758cfm8pgjlf866zci1r")))

(define-public crate-kernaux-0.0.0 (crate (name "kernaux") (vers "0.0.0") (hash "1byj84rllv2501rwjvp35y5znmx63l0fz62qj81zj328xyrs6phv") (yanked #t)))

(define-public crate-kernaux-0.1 (crate (name "kernaux") (vers "0.1.0") (hash "1m4k94yja91pqpwrpb67kd73j75h1q4xb5r6by7hjdsx9wcqa6sr") (yanked #t)))

(define-public crate-kernaux-0.2 (crate (name "kernaux") (vers "0.2.0") (deps (list (crate-dep (name "libc") (req "^0.2.113") (default-features #t) (kind 0)))) (hash "09rd2dpwfh93lyga2bjbipsz3b4ddcvx51a92n6rp5bz23r0qzww") (yanked #t)))

(define-public crate-kernaux-0.3 (crate (name "kernaux") (vers "0.3.0") (deps (list (crate-dep (name "kernaux-sys") (req "^0.3.0") (default-features #t) (kind 0)))) (hash "0k4k8rzvc1yy2r4k55gsl9qrj2baqz4vlbi19qmnppalm1c5lkjn") (yanked #t)))

(define-public crate-kernaux-0.4 (crate (name "kernaux") (vers "0.4.0") (deps (list (crate-dep (name "ctor") (req "^0.1.22") (default-features #t) (kind 0)) (crate-dep (name "kernaux-sys") (req "^0.4.0") (kind 0)) (crate-dep (name "libc") (req "^0.2.113") (kind 0)))) (hash "1cnmpfk22y8gw2hj6khly749rm7d31h0bc6dk24n71wgclnxf9zb") (features (quote (("ntoa" "kernaux-sys/ntoa") ("default" "cmdline" "ntoa") ("cmdline" "kernaux-sys/cmdline")))) (yanked #t)))

(define-public crate-kernaux-0.5 (crate (name "kernaux") (vers "0.5.0") (deps (list (crate-dep (name "ctor") (req "^0.1.22") (default-features #t) (kind 0)) (crate-dep (name "kernaux-sys") (req "^0.5.0") (kind 0)) (crate-dep (name "libc") (req "^0.2.113") (kind 0)))) (hash "1z53cm8rfwhwysxr7qi3vr50a15dd1h3cmi3j6bkg2cnqg9hxg60") (features (quote (("ntoa" "kernaux-sys/ntoa") ("default" "cmdline" "ntoa") ("cmdline" "kernaux-sys/cmdline"))))))

(define-public crate-kernaux-0.6 (crate (name "kernaux") (vers "0.6.0") (deps (list (crate-dep (name "ctor") (req "^0.1.22") (default-features #t) (kind 0)) (crate-dep (name "kernaux-sys") (req "^0.6.0") (kind 0)) (crate-dep (name "libc") (req "^0.2.113") (kind 0)))) (hash "0z27iz34lk57q2savi2mchz0310vq0dbdsbjxg6gbraaahgj8d17") (features (quote (("ntoa" "kernaux-sys/ntoa") ("default" "cmdline" "ntoa") ("cmdline" "kernaux-sys/cmdline"))))))

(define-public crate-kernaux-0.6 (crate (name "kernaux") (vers "0.6.1") (deps (list (crate-dep (name "ctor") (req "^0.1.22") (default-features #t) (kind 0)) (crate-dep (name "kernaux-sys") (req "^0.6.1") (kind 0)) (crate-dep (name "libc") (req "^0.2.113") (kind 0)))) (hash "125ss3420ywl07cgcd6z7zigy44v0a44ngwzjz2yb4nk0jfz2c73") (features (quote (("ntoa" "kernaux-sys/ntoa") ("default" "cmdline" "ntoa") ("cmdline" "kernaux-sys/cmdline"))))))

(define-public crate-kernaux-0.7 (crate (name "kernaux") (vers "0.7.0") (deps (list (crate-dep (name "ctor") (req "^0.1.22") (default-features #t) (kind 0)) (crate-dep (name "kernaux-sys") (req "^0.7.0") (kind 0)) (crate-dep (name "libc") (req "^0.2.113") (kind 0)))) (hash "1k6gssb3b8vms4qrdr8vivhrm7y4vk29gql296pcjyp3jbnxfm5b") (features (quote (("ntoa" "kernaux-sys/ntoa") ("default" "cmdline" "ntoa") ("cmdline" "kernaux-sys/cmdline"))))))

(define-public crate-kernaux-sys-0.0.0 (crate (name "kernaux-sys") (vers "0.0.0") (deps (list (crate-dep (name "libc") (req "^0.2.113") (default-features #t) (kind 0)))) (hash "1pvd68i2dlnjk4xxv8wkrmblxgq1d4ixhn9yp0d03nm2rskl92b5") (yanked #t)))

(define-public crate-kernaux-sys-0.1 (crate (name "kernaux-sys") (vers "0.1.0") (deps (list (crate-dep (name "libc") (req "^0.2.113") (default-features #t) (kind 0)))) (hash "1nlgivsaj3j6m9b5nms1rjgwr5q0qamy33v21qjp0r3smlicx0hi") (yanked #t)))

(define-public crate-kernaux-sys-0.2 (crate (name "kernaux-sys") (vers "0.2.0") (deps (list (crate-dep (name "libc") (req "^0.2.113") (features (quote ("extra_traits"))) (kind 0)))) (hash "1828m3ng4hmazfn707f97mfliy6nipz7jflpg8r1aqy4sq3h58zh") (yanked #t)))

(define-public crate-kernaux-sys-0.3 (crate (name "kernaux-sys") (vers "0.3.0") (deps (list (crate-dep (name "libc") (req "^0.2.113") (features (quote ("extra_traits"))) (kind 0)))) (hash "098ajvqf9nihikixly5gzir3fg8mqz8gyprh7wkiis6c7lwd1p8z") (yanked #t)))

(define-public crate-kernaux-sys-0.4 (crate (name "kernaux-sys") (vers "0.4.0") (deps (list (crate-dep (name "libc") (req "^0.2.113") (features (quote ("extra_traits"))) (kind 0)))) (hash "1vsh5qdqjxvrjjlvpgdmjvj7pnkzla6r0ys2abvg64v1z38n63gf") (features (quote (("ntoa") ("default" "cmdline" "ntoa") ("cmdline")))) (yanked #t)))

(define-public crate-kernaux-sys-0.5 (crate (name "kernaux-sys") (vers "0.5.0") (deps (list (crate-dep (name "libc") (req "^0.2.113") (features (quote ("extra_traits"))) (kind 0)))) (hash "0sllpqlgybggvr43jgxa9r978adm9ak6fzprlvhy3f7g1cdjyswq") (features (quote (("ntoa") ("default" "cmdline" "ntoa") ("cmdline"))))))

(define-public crate-kernaux-sys-0.6 (crate (name "kernaux-sys") (vers "0.6.0") (deps (list (crate-dep (name "libc") (req "^0.2.113") (features (quote ("extra_traits"))) (kind 0)))) (hash "0xqls90cy6klhcpfm9rwbs3lv0drnpc39czykv7q27wx42cf1nac") (features (quote (("ntoa") ("default" "cmdline" "ntoa") ("cmdline"))))))

(define-public crate-kernaux-sys-0.6 (crate (name "kernaux-sys") (vers "0.6.1") (deps (list (crate-dep (name "libc") (req "^0.2.113") (features (quote ("extra_traits"))) (kind 0)))) (hash "0j06qv1pzac6gy1kfdai48vgswd8zjfh5w24lj740n25v3c205gj") (features (quote (("ntoa") ("default" "cmdline" "ntoa") ("cmdline"))))))

(define-public crate-kernaux-sys-0.7 (crate (name "kernaux-sys") (vers "0.7.0") (deps (list (crate-dep (name "libc") (req "^0.2.113") (features (quote ("extra_traits"))) (kind 0)))) (hash "0fk9vzb06h7x57rpkdrya6n72wf8rm1w4p6zhaj39ykhwa3irm0m") (features (quote (("ntoa") ("default" "cmdline" "ntoa") ("cmdline"))))))

(define-public crate-kernel-0.1 (crate (name "kernel") (vers "0.1.0") (deps (list (crate-dep (name "env_logger") (req "^0.3") (kind 2)) (crate-dep (name "log") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "mio") (req "^0.6") (default-features #t) (kind 0)))) (hash "03j6bidp41s2r895ip27qwl84x564w6i2sgk52g7wwizqs9pqmai")))

(define-public crate-kernel-0.1 (crate (name "kernel") (vers "0.1.1") (deps (list (crate-dep (name "env_logger") (req "^0.3") (kind 2)) (crate-dep (name "log") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "mio") (req "^0.6") (default-features #t) (kind 0)) (crate-dep (name "slab") (req "^0.3") (default-features #t) (kind 0)))) (hash "1hgb409alfz8bv2azx7yqdjii13asqpp4kh47df4gcmsc3342z09")))

(define-public crate-kernel-0.11 (crate (name "kernel") (vers "0.11.0") (deps (list (crate-dep (name "bitflags") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "byteorder") (req "^0.5.3") (default-features #t) (kind 0)) (crate-dep (name "hwloc") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2.14") (default-features #t) (target "cfg(unix)") (kind 0)) (crate-dep (name "log") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "net2") (req "^0.2.19") (default-features #t) (kind 0)) (crate-dep (name "nix") (req "^0.6.0") (default-features #t) (target "cfg(unix)") (kind 0)) (crate-dep (name "num") (req "^0.1.36") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.3.14") (default-features #t) (kind 0)))) (hash "1bm3342wj071hs4hsn1vvz6adcik5ccvasmxpvnyvw2jlsnrjv0g")))

(define-public crate-kernel-0.12 (crate (name "kernel") (vers "0.12.0") (deps (list (crate-dep (name "bitflags") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "byteorder") (req "^0.5.3") (default-features #t) (kind 0)) (crate-dep (name "deque") (req "^0.3.1") (default-features #t) (kind 0)) (crate-dep (name "env_logger") (req "^0.3.5") (default-features #t) (kind 0)) (crate-dep (name "fnv") (req "^1.0.5") (default-features #t) (kind 0)) (crate-dep (name "hwloc") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "lalrpop") (req "^0.12.0") (default-features #t) (kind 1)) (crate-dep (name "lalrpop-util") (req "^0.12.0") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2.14") (default-features #t) (target "cfg(unix)") (kind 0)) (crate-dep (name "log") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "net2") (req "^0.2.19") (default-features #t) (kind 0)) (crate-dep (name "nix") (req "^0.6.0") (default-features #t) (target "cfg(unix)") (kind 0)) (crate-dep (name "num") (req "^0.1.36") (default-features #t) (kind 0)) (crate-dep (name "num_cpus") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.3.14") (default-features #t) (kind 0)) (crate-dep (name "time") (req "^0.1.35") (default-features #t) (kind 0)))) (hash "15lrf9aknsncrm41c5s3ck3cgk6igifdyv66idglz8m38fb258lv")))

(define-public crate-kernel-1 (crate (name "kernel") (vers "1.1.0") (deps (list (crate-dep (name "bitflags") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "env_logger") (req "^0.3.5") (default-features #t) (kind 0)) (crate-dep (name "http-muncher") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "lalrpop") (req "^0.12.0") (default-features #t) (kind 1)) (crate-dep (name "lalrpop-util") (req "^0.12.0") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2.14") (default-features #t) (target "cfg(unix)") (kind 0)) (crate-dep (name "log") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "net2") (req "^0.2.19") (default-features #t) (kind 0)) (crate-dep (name "nix") (req "^0.6.0") (default-features #t) (target "cfg(unix)") (kind 0)) (crate-dep (name "rustc-serialize") (req "^0.3.22") (default-features #t) (kind 0)) (crate-dep (name "sha1") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "time") (req "^0.1.35") (default-features #t) (kind 0)))) (hash "0g7plg2mf38y33z1370cmprydpm77fq58x1fy3r51pw7scqyk560")))

(define-public crate-kernel-alloc-0.1 (crate (name "kernel-alloc") (vers "0.1.0") (hash "0rzqrv8v6qzk1pbv37jcbnvba24xvqqfblnz6iayawclf4mv1myl")))

(define-public crate-kernel-alloc-0.1 (crate (name "kernel-alloc") (vers "0.1.1") (hash "1n3b478q73sipn3iw35ng63bjs09hmdxv5y812mkl4a27n5yscbc")))

(define-public crate-kernel-alloc-0.1 (crate (name "kernel-alloc") (vers "0.1.2") (hash "0mrxvw27kgq0ph01a93n3g63aknn41ryhvp4arfxs9p3k34av4qk") (features (quote (("pool-tag") ("default"))))))

(define-public crate-kernel-alloc-0.2 (crate (name "kernel-alloc") (vers "0.2.0") (deps (list (crate-dep (name "winapi") (req "^0.3.9") (features (quote ("ntdef"))) (kind 0)))) (hash "14ml8pfpsidacn7zhjsazkh9c2zcvhy0c5vqafknfmswfjz8vk46") (features (quote (("pool-tag") ("default"))))))

(define-public crate-kernel-alloc-0.2 (crate (name "kernel-alloc") (vers "0.2.1") (deps (list (crate-dep (name "winapi") (req "^0.3.9") (features (quote ("ntdef"))) (kind 0)))) (hash "1v32my1051yhhym5sm07vvlj7cpn87p4wyybnf3n7ifxc79krh35") (features (quote (("pool-tag") ("default"))))))

(define-public crate-kernel-alloc-0.2 (crate (name "kernel-alloc") (vers "0.2.2") (deps (list (crate-dep (name "winapi") (req "^0.3.9") (features (quote ("ntdef"))) (kind 0)))) (hash "17dkr4aznw1g2jlxmcdyzr8jdv0y4fn58jbpgn31c24kwqlv4ms1") (features (quote (("pool-tag") ("default"))))))

(define-public crate-kernel-alloc-0.2 (crate (name "kernel-alloc") (vers "0.2.3") (deps (list (crate-dep (name "winapi") (req "^0.3.9") (features (quote ("ntdef"))) (kind 0)))) (hash "06ibhvz839gahxjphbfxnmxg4myxgi90w1q7krb4043kbwz0zf3v") (features (quote (("pool-tag") ("default"))))))

(define-public crate-kernel-asound-sys-0.1 (crate (name "kernel-asound-sys") (vers "0.1.1") (deps (list (crate-dep (name "bindgen") (req "^0.57.0") (default-features #t) (kind 1)) (crate-dep (name "nix") (req "^0.20") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 1)))) (hash "17q1fljb69rz1lx3rfhh86pgyr2pnmwhs16nd17kfcjqhvp8wqw6")))

(define-public crate-kernel-asound-sys-0.1 (crate (name "kernel-asound-sys") (vers "0.1.2") (deps (list (crate-dep (name "bindgen") (req "^0.57.0") (default-features #t) (kind 1)) (crate-dep (name "nix") (req "^0.20") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 1)))) (hash "0qvbsbnmg7qjgzjfrmnci0fa97s3zh2y8dz932n4pnkhlyly8473")))

(define-public crate-kernel-asound-sys-0.2 (crate (name "kernel-asound-sys") (vers "0.2.0") (deps (list (crate-dep (name "bindgen") (req "^0.57.0") (default-features #t) (kind 1)) (crate-dep (name "ioctl-sys") (req "^0.6") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 1)))) (hash "0xd91949krpdyb7wp6lry9lmvqssxlw5jz4gzgms4j4783jjh8gy")))

(define-public crate-kernel-asound-sys-0.2 (crate (name "kernel-asound-sys") (vers "0.2.1") (deps (list (crate-dep (name "bindgen") (req "^0.57.0") (default-features #t) (kind 1)) (crate-dep (name "ioctl-sys") (req "^0.6") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 1)))) (hash "1si985j511iy9j5chqb0g188zw9x2fp7mcivkikvdgb6f06iqk51")))

(define-public crate-kernel-asound-sys-0.3 (crate (name "kernel-asound-sys") (vers "0.3.0") (deps (list (crate-dep (name "bindgen") (req "^0.57.0") (default-features #t) (kind 1)) (crate-dep (name "ioctl-sys") (req "^0.7.1") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 1)))) (hash "0favp1by09jrf132d3h5ain35gwvb06scdv6yh733i2w0y7i7cif")))

(define-public crate-kernel-build-0.1 (crate (name "kernel-build") (vers "0.1.0") (deps (list (crate-dep (name "failure") (req "^0.1.8") (default-features #t) (kind 1)) (crate-dep (name "winreg") (req "^0.10.1") (default-features #t) (kind 1)))) (hash "11q8znv4i4yw1phwh3kcx75zap84csvd1kgwizjvi4s1x94xngm4")))

(define-public crate-kernel-builder-0.1 (crate (name "kernel-builder") (vers "0.1.3") (deps (list (crate-dep (name "config") (req "^0.13") (default-features #t) (kind 0)) (crate-dep (name "dialoguer") (req "^0.10") (default-features #t) (kind 0)) (crate-dep (name "indicatif") (req "^0.17") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "sudo") (req "^0.6") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0") (default-features #t) (kind 0)))) (hash "1lasp1xh063krylm3f7qcnq4923c49s4x4vmfl9wzmc35rpnd6zv") (features (quote (("dracut"))))))

(define-public crate-kernel-builder-0.1 (crate (name "kernel-builder") (vers "0.1.4") (deps (list (crate-dep (name "config") (req "^0.13") (default-features #t) (kind 0)) (crate-dep (name "dialoguer") (req "^0.10") (default-features #t) (kind 0)) (crate-dep (name "indicatif") (req "^0.17") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "sudo") (req "^0.6") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0") (default-features #t) (kind 0)))) (hash "0lrs791fywr4lkwm4xs26pxgq7s4rq1jy0b63qsjrilv6s5w3d3i") (features (quote (("dracut"))))))

(define-public crate-kernel-builder-0.1 (crate (name "kernel-builder") (vers "0.1.5") (deps (list (crate-dep (name "config") (req "^0.13") (default-features #t) (kind 0)) (crate-dep (name "dialoguer") (req "^0.10") (default-features #t) (kind 0)) (crate-dep (name "indicatif") (req "^0.17") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "sudo") (req "^0.6") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0") (default-features #t) (kind 0)))) (hash "1mq08sf0bz112zpv9x47kqkqy7parq2sjzh9w7xgmfzy08n9bj59") (features (quote (("dracut"))))))

(define-public crate-kernel-builder-0.1 (crate (name "kernel-builder") (vers "0.1.6") (deps (list (crate-dep (name "config") (req "^0.13") (default-features #t) (kind 0)) (crate-dep (name "dialoguer") (req "^0.10") (default-features #t) (kind 0)) (crate-dep (name "indicatif") (req "^0.17") (default-features #t) (kind 0)) (crate-dep (name "pico-args") (req "^0.5") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "sudo") (req "^0.6") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0") (default-features #t) (kind 0)))) (hash "1ip8ffknmvj1w84qwvvhlk8hpmv2fdypjzaqlvmk961dl2krpj9y") (features (quote (("dracut"))))))

(define-public crate-kernel-builder-0.1 (crate (name "kernel-builder") (vers "0.1.7") (deps (list (crate-dep (name "config") (req "^0.13") (default-features #t) (kind 0)) (crate-dep (name "dialoguer") (req "^0.10") (default-features #t) (kind 0)) (crate-dep (name "indicatif") (req "^0.17") (default-features #t) (kind 0)) (crate-dep (name "pico-args") (req "^0.5") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "sudo") (req "^0.6") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0") (default-features #t) (kind 0)))) (hash "0z9p293b9xax8p4gi5ici5ibkhpvdn7v9wjlxh12qpkhi9yhmh6x") (features (quote (("dracut"))))))

(define-public crate-kernel-builder-0.1 (crate (name "kernel-builder") (vers "0.1.8") (deps (list (crate-dep (name "config") (req "^0.13") (default-features #t) (kind 0)) (crate-dep (name "dialoguer") (req "^0.10") (default-features #t) (kind 0)) (crate-dep (name "indicatif") (req "^0.17") (default-features #t) (kind 0)) (crate-dep (name "pico-args") (req "^0.5") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "sudo") (req "^0.6") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0") (default-features #t) (kind 0)))) (hash "16iclv2a33ipsqk2m1jn3las5r3bwsfidvp3c39mw47ibvpikdbd") (features (quote (("dracut"))))))

(define-public crate-kernel-builder-0.1 (crate (name "kernel-builder") (vers "0.1.9") (deps (list (crate-dep (name "config") (req "^0.13") (default-features #t) (kind 0)) (crate-dep (name "dialoguer") (req "^0.10") (default-features #t) (kind 0)) (crate-dep (name "indicatif") (req "^0.17") (default-features #t) (kind 0)) (crate-dep (name "pico-args") (req "^0.5") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "sudo") (req "^0.6") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0") (default-features #t) (kind 0)))) (hash "11f57k27rj1zaspdmgbymi5p13k1zdr3yvr90wq8sivkyvv3pl1a") (features (quote (("dracut"))))))

(define-public crate-kernel-builder-0.2 (crate (name "kernel-builder") (vers "0.2.0") (deps (list (crate-dep (name "config") (req "^0.13") (default-features #t) (kind 0)) (crate-dep (name "dialoguer") (req "^0.10") (default-features #t) (kind 0)) (crate-dep (name "indicatif") (req "^0.17") (default-features #t) (kind 0)) (crate-dep (name "pico-args") (req "^0.5") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "sudo") (req "^0.6") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0") (default-features #t) (kind 0)))) (hash "0pnmsc6mx8iv361krf3ckky0910acrydi0b5dh32zxkrn6s5qmw3") (features (quote (("dracut"))))))

(define-public crate-kernel-builder-0.2 (crate (name "kernel-builder") (vers "0.2.1") (deps (list (crate-dep (name "config") (req "^0.13") (default-features #t) (kind 0)) (crate-dep (name "dialoguer") (req "^0.10") (default-features #t) (kind 0)) (crate-dep (name "indicatif") (req "^0.17") (default-features #t) (kind 0)) (crate-dep (name "pico-args") (req "^0.5") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "sudo") (req "^0.6") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0") (default-features #t) (kind 0)))) (hash "1w6fsnv8i8ajyb3wb9axj3ahy62hbb65szi89p4knbmm115xp4yb") (features (quote (("dracut"))))))

(define-public crate-kernel-builder-0.2 (crate (name "kernel-builder") (vers "0.2.2") (deps (list (crate-dep (name "config") (req "^0.13") (default-features #t) (kind 0)) (crate-dep (name "dialoguer") (req "^0.10") (default-features #t) (kind 0)) (crate-dep (name "indicatif") (req "^0.17") (default-features #t) (kind 0)) (crate-dep (name "pico-args") (req "^0.5") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "sudo") (req "^0.6") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0") (default-features #t) (kind 0)))) (hash "11bz963fg4l1ahrw4zrvx4m65pjh7ywsajqqp89320sphb16sm0g") (features (quote (("dracut"))))))

(define-public crate-kernel-builder-0.2 (crate (name "kernel-builder") (vers "0.2.3") (deps (list (crate-dep (name "config") (req "^0.13") (default-features #t) (kind 0)) (crate-dep (name "dialoguer") (req "^0.11") (default-features #t) (kind 0)) (crate-dep (name "indicatif") (req "^0.17") (default-features #t) (kind 0)) (crate-dep (name "pico-args") (req "^0.5") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "sudo") (req "^0.6") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0") (default-features #t) (kind 0)))) (hash "0skzs5nvlpxr8yv3qz44c0zw4jj3mm5iiq3yvksjh86dydc4hjf0") (features (quote (("dracut"))))))

(define-public crate-kernel-density-estimation-0.1 (crate (name "kernel-density-estimation") (vers "0.1.0") (deps (list (crate-dep (name "approx") (req "^0.5") (default-features #t) (kind 2)) (crate-dep (name "criterion") (req "^0.4") (default-features #t) (kind 2)) (crate-dep (name "fastrand") (req "^1.8") (default-features #t) (kind 0)) (crate-dep (name "plotly") (req "^0.8") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.8") (default-features #t) (kind 2)))) (hash "0251197w5jnjmx1wcs1v7342fnicy98szi0qj7d264ph5i4vf02n") (features (quote (("f64"))))))

(define-public crate-kernel-density-estimation-0.2 (crate (name "kernel-density-estimation") (vers "0.2.0") (deps (list (crate-dep (name "approx") (req "^0.5") (default-features #t) (kind 2)) (crate-dep (name "criterion") (req "^0.4") (default-features #t) (kind 2)) (crate-dep (name "fastrand") (req "^1.8") (default-features #t) (kind 0)) (crate-dep (name "nalgebra") (req "^0.31") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "plotly") (req "^0.8") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.8") (default-features #t) (kind 2)))) (hash "137yfx954sy3jwmn0g4va30m5arb646fr04cyfd1rvfy4l9gnxcc")))

(define-public crate-kernel-log-0.1 (crate (name "kernel-log") (vers "0.1.0") (deps (list (crate-dep (name "log") (req "^0.4.14") (default-features #t) (kind 0)) (crate-dep (name "ntapi") (req "^0.3.4") (features (quote ("kernel"))) (kind 0)))) (hash "0hb0wksnw6wi3s5csq8qa5jpdv4j3bqq1rwfn1vnzhpidar6nh34") (features (quote (("default"))))))

(define-public crate-kernel-log-0.1 (crate (name "kernel-log") (vers "0.1.1") (deps (list (crate-dep (name "log") (req "^0.4.14") (default-features #t) (kind 0)) (crate-dep (name "ntapi") (req "^0.3.6") (features (quote ("kernel"))) (kind 0)))) (hash "1n9bamxxr2jnlwgs377yrndnsjm07dqrb35r9gp7d4by852nx35l") (features (quote (("default"))))))

(define-public crate-kernel-log-0.1 (crate (name "kernel-log") (vers "0.1.2") (deps (list (crate-dep (name "log") (req "^0.4.17") (default-features #t) (kind 0)) (crate-dep (name "ntapi") (req "^0.4.0") (features (quote ("kernel"))) (kind 0)))) (hash "1gag3ia4qnjfipw78l77lk2im2nngrixdzs6g2fk4fydcdbq5h7g") (features (quote (("default"))))))

(define-public crate-kernel-log-0.1 (crate (name "kernel-log") (vers "0.1.3") (deps (list (crate-dep (name "log") (req "^0.4.17") (default-features #t) (kind 0)) (crate-dep (name "ntapi") (req "^0.4.0") (features (quote ("kernel"))) (kind 0)))) (hash "1i6r0db03yxmahpkii1cb2h34k0csby92sk873r4m06xjmbfxd86") (features (quote (("default"))))))

(define-public crate-kernel-print-0.1 (crate (name "kernel-print") (vers "0.1.0") (deps (list (crate-dep (name "ntapi") (req "^0.3.4") (features (quote ("kernel"))) (kind 0)))) (hash "18216sp7v1gwrxksc953p0v42gp6jv0fv6w656d5y7qsyvcbkgnp") (features (quote (("std_name") ("format") ("default" "format"))))))

(define-public crate-kernel-sidecar-0.1 (crate (name "kernel-sidecar") (vers "0.1.0") (deps (list (crate-dep (name "async-trait") (req "^0.1.74") (default-features #t) (kind 0)) (crate-dep (name "bytes") (req "^1.5.0") (features (quote ("serde"))) (default-features #t) (kind 0)) (crate-dep (name "chrono") (req "^0.4.31") (features (quote ("serde"))) (default-features #t) (kind 0)) (crate-dep (name "enum-as-inner") (req "^0.6.0") (default-features #t) (kind 0)) (crate-dep (name "hex") (req "^0.4.3") (default-features #t) (kind 0)) (crate-dep (name "indoc") (req "^2.0.4") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)) (crate-dep (name "ring") (req "^0.17.5") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.190") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.108") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.33.0") (features (quote ("full" "signal"))) (default-features #t) (kind 0)) (crate-dep (name "uuid") (req "^1.5.0") (features (quote ("v4" "serde" "fast-rng"))) (default-features #t) (kind 0)) (crate-dep (name "zeromq") (req "^0.3.4") (default-features #t) (kind 0)))) (hash "0vsl7dc3zn5fkg57s98bxnzk4kin2nijimjcjsfm6l9b265j61m0") (features (quote (("test_irkernel") ("test_ipython") ("test_evcxr") ("test_deno"))))))

(define-public crate-kernel32-sys-0.0.1 (crate (name "kernel32-sys") (vers "0.0.1") (deps (list (crate-dep (name "winapi") (req "*") (default-features #t) (kind 0)))) (hash "13hsrn7pj8vlyqpazchwaqdmzyf291rqam7dhaf1gi8cbw02lrss")))

(define-public crate-kernel32-sys-0.0.2 (crate (name "kernel32-sys") (vers "0.0.2") (deps (list (crate-dep (name "winapi") (req "*") (default-features #t) (kind 0)))) (hash "1b5kqfa6a82b33hgknmjyh9q6abf42wm7917713wk9wr74wa9nnn")))

(define-public crate-kernel32-sys-0.0.3 (crate (name "kernel32-sys") (vers "0.0.3") (deps (list (crate-dep (name "winapi") (req "*") (default-features #t) (kind 0)))) (hash "1cz645xwf3k0whg50lc87pw59nr7l4nwhidfjlazsf1ln40m3w6i")))

(define-public crate-kernel32-sys-0.0.4 (crate (name "kernel32-sys") (vers "0.0.4") (deps (list (crate-dep (name "winapi") (req "*") (default-features #t) (kind 0)))) (hash "1bnazajdkmk0drxwn34iskkr47b4iqzplg23p1sifj3ln26qrjy1")))

(define-public crate-kernel32-sys-0.0.5 (crate (name "kernel32-sys") (vers "0.0.5") (deps (list (crate-dep (name "winapi") (req "*") (default-features #t) (kind 0)))) (hash "09jy3k3icpymalx1inag6knsqgj5bgmds8p5pxkz0bycgximzjnf")))

(define-public crate-kernel32-sys-0.0.6 (crate (name "kernel32-sys") (vers "0.0.6") (deps (list (crate-dep (name "winapi") (req "*") (default-features #t) (kind 0)))) (hash "0dqj0vgf3d9fg18qpm527hcwfqjrvg5gas7gxm44iyxfa7yfldqd")))

(define-public crate-kernel32-sys-0.0.7 (crate (name "kernel32-sys") (vers "0.0.7") (deps (list (crate-dep (name "winapi") (req "*") (default-features #t) (kind 0)))) (hash "0s8m55m3bvjyalfqwjv8qjrpwgifpn68qhcxa3b7y59imr5amlrn")))

(define-public crate-kernel32-sys-0.0.8 (crate (name "kernel32-sys") (vers "0.0.8") (deps (list (crate-dep (name "winapi") (req "*") (default-features #t) (kind 0)))) (hash "1yp0mmz8b5igl2lr82b3mwvx2bkfgfkizswmsl4lp2xp67y379jq")))

(define-public crate-kernel32-sys-0.0.9 (crate (name "kernel32-sys") (vers "0.0.9") (deps (list (crate-dep (name "winapi") (req "*") (default-features #t) (kind 0)))) (hash "12s17qy73h7b9y1q7rpxxmny43lgkh5s9k9jrs997hw52gaxrwsf")))

(define-public crate-kernel32-sys-0.0.10 (crate (name "kernel32-sys") (vers "0.0.10") (deps (list (crate-dep (name "winapi") (req "*") (default-features #t) (kind 0)))) (hash "1ivgfirl20gk8skzlbszayg3qqh5bkk2a9dcmrq9qnswnsvqibq4")))

(define-public crate-kernel32-sys-0.0.11 (crate (name "kernel32-sys") (vers "0.0.11") (deps (list (crate-dep (name "winapi") (req "*") (default-features #t) (kind 0)))) (hash "0xq1q0mmmxv2975xrdzbnwc5q0rdp9n2w6f0qfcswnl2cfqbkb2q")))

(define-public crate-kernel32-sys-0.0.12 (crate (name "kernel32-sys") (vers "0.0.12") (deps (list (crate-dep (name "winapi") (req "*") (default-features #t) (kind 0)))) (hash "00f6hvlrayhyxfdpxzc2c09pszzb4klqrxwd0gn4gh60qh8wai07") (yanked #t)))

(define-public crate-kernel32-sys-0.1 (crate (name "kernel32-sys") (vers "0.1.0") (deps (list (crate-dep (name "winapi") (req "*") (default-features #t) (kind 0)))) (hash "0gh2xr9819bvrysnc10jsarm4gf8qib5v4nl159n9q5bp95ylqky")))

(define-public crate-kernel32-sys-0.1 (crate (name "kernel32-sys") (vers "0.1.1") (deps (list (crate-dep (name "winapi") (req "*") (default-features #t) (kind 0)) (crate-dep (name "winapi-build") (req "*") (default-features #t) (kind 1)))) (hash "14lk989z86fb5wh7f9dsgkkqwj48ckrq3nlm81dgvcq8r1c9g5ih")))

(define-public crate-kernel32-sys-0.1 (crate (name "kernel32-sys") (vers "0.1.2") (deps (list (crate-dep (name "winapi") (req "*") (default-features #t) (kind 0)) (crate-dep (name "winapi-build") (req "*") (default-features #t) (kind 1)))) (hash "0w8sw5cijh9wcanpirqcvdvlmy3dcwn2ig7f447ilcz3isph6rdk")))

(define-public crate-kernel32-sys-0.1 (crate (name "kernel32-sys") (vers "0.1.3") (deps (list (crate-dep (name "winapi") (req "*") (default-features #t) (kind 0)) (crate-dep (name "winapi-build") (req "*") (default-features #t) (kind 1)))) (hash "0hf38ypgdp4wm6m0019rbrbipfpgdnrkzqrif4gxp8vjsi65hg4d")))

(define-public crate-kernel32-sys-0.1 (crate (name "kernel32-sys") (vers "0.1.4") (deps (list (crate-dep (name "winapi") (req "*") (default-features #t) (kind 0)) (crate-dep (name "winapi-build") (req "*") (default-features #t) (kind 1)))) (hash "0gh8vld701xmicci8ak126skxva73ckcnvzsl47din9g12qxl570")))

(define-public crate-kernel32-sys-0.2 (crate (name "kernel32-sys") (vers "0.2.0") (deps (list (crate-dep (name "winapi") (req "^0.2.4") (default-features #t) (kind 0)) (crate-dep (name "winapi-build") (req "^0.1.1") (default-features #t) (kind 1)))) (hash "1npa4wj6bqb6bsp89q3a5pzk9si73mgim3i8h98rgzcvnj2a075d")))

(define-public crate-kernel32-sys-0.2 (crate (name "kernel32-sys") (vers "0.2.1") (deps (list (crate-dep (name "winapi") (req "^0.2.4") (default-features #t) (kind 0)) (crate-dep (name "winapi-build") (req "^0.1.1") (default-features #t) (kind 1)))) (hash "1wa3h8jjgzg10rq9pz36lzs4c57dspqp5qdx8did5rkmygnygddm")))

(define-public crate-kernel32-sys-0.2 (crate (name "kernel32-sys") (vers "0.2.2") (deps (list (crate-dep (name "winapi") (req "^0.2.5") (default-features #t) (kind 0)) (crate-dep (name "winapi-build") (req "^0.1.1") (default-features #t) (kind 1)))) (hash "1389av0601a9yz8dvx5zha9vmkd6ik7ax0idpb032d28555n41vm")))

(define-public crate-kernel_density-0.0.1 (crate (name "kernel_density") (vers "0.0.1") (deps (list (crate-dep (name "ordered-float") (req "^0.2.1") (default-features #t) (kind 0)) (crate-dep (name "quickcheck") (req "^0.2") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.3.12") (default-features #t) (kind 0)))) (hash "1cd1s8i86lk2j7xg3wkpljcjx2rwchydp7b6dzqmfpxmdfyf8l26")))

(define-public crate-kernel_guard-0.1 (crate (name "kernel_guard") (vers "0.1.0") (deps (list (crate-dep (name "cfg-if") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "crate_interface") (req "^0.1") (default-features #t) (kind 0)))) (hash "1p5gd0p27abpdbc6f1fmrh6556w41j4p5lwsb22g53ink21cqf2n") (features (quote (("preempt") ("default"))))))

(define-public crate-kernel_web-0.1 (crate (name "kernel_web") (vers "0.1.0") (deps (list (crate-dep (name "colorful") (req "^0.2.2") (default-features #t) (kind 0)))) (hash "1s2x806yzcp72zaagxbn1kabkj6rlvcvh1k8mmag0cdjdbxl00r8")))

(define-public crate-kernel_web-0.1 (crate (name "kernel_web") (vers "0.1.1") (deps (list (crate-dep (name "colorful") (req "^0.2.2") (default-features #t) (kind 0)))) (hash "1zb6nswaald1lq9hyxnhg1ciflnm7pxp6zf4f8mk99hsyjim57nj")))

(define-public crate-kernel_web-0.1 (crate (name "kernel_web") (vers "0.1.2") (deps (list (crate-dep (name "colorful") (req "^0.2.2") (default-features #t) (kind 0)))) (hash "05qawdndjfbfaa2xx72iw5vz9169gf1jpl6zxa04dhi2wam917gj")))

(define-public crate-kernel_web-0.1 (crate (name "kernel_web") (vers "0.1.4") (deps (list (crate-dep (name "colorful") (req "^0.2.2") (default-features #t) (kind 0)))) (hash "0gmbh6wm7r0l995kpjlgdrf7ar1akxvl3vp5mzc44yj1mv5ycgl8")))

(define-public crate-kernel_web-0.1 (crate (name "kernel_web") (vers "0.1.5") (deps (list (crate-dep (name "colorful") (req "^0.2.2") (default-features #t) (kind 0)))) (hash "1zwzddrqdqdn3nfk4r84lf401cx2y47lv2ldmi2a1sq9cl90j1cx")))

(define-public crate-kernel_web-0.1 (crate (name "kernel_web") (vers "0.1.6") (deps (list (crate-dep (name "colorful") (req "^0.2.2") (default-features #t) (kind 0)))) (hash "0dwir8nyplfir6hlykbg8ksabaywh95fyb92rqxhsw7hiqpp02xk")))

(define-public crate-kernel_web-0.1 (crate (name "kernel_web") (vers "0.1.7") (deps (list (crate-dep (name "colorful") (req "^0.2.2") (default-features #t) (kind 0)))) (hash "1krl73jrc9ilj8lz0x2p7zq21clm145vfvbvdqv7f7kpkpbvb772")))

(define-public crate-kernel_web-0.1 (crate (name "kernel_web") (vers "0.1.8") (deps (list (crate-dep (name "colorful") (req "^0.2.2") (default-features #t) (kind 0)))) (hash "114xd6b2kcjfckqhvjffkbsad7zcgs5a32rxabpmizsh7q8abxjb")))

(define-public crate-kernlog-0.1 (crate (name "kernlog") (vers "0.1.0") (deps (list (crate-dep (name "log") (req "*") (default-features #t) (kind 0)))) (hash "0ir02109xlm6x3pjnyxzix0ljr6sxnib5vlx84lk0w4f8w5anhyf") (features (quote (("nightly"))))))

(define-public crate-kernlog-0.1 (crate (name "kernlog") (vers "0.1.1") (deps (list (crate-dep (name "log") (req "*") (default-features #t) (kind 0)))) (hash "1iij8ywdcagrd9bq8ysr3k3ja1dsms0y7wrf2ag0jnlqbjnzjz83") (features (quote (("nightly"))))))

(define-public crate-kernlog-0.1 (crate (name "kernlog") (vers "0.1.2") (deps (list (crate-dep (name "log") (req "*") (default-features #t) (kind 0)))) (hash "0g6s6crzpqpgw385zfd4kcd9rdbpfmzj82q3hhyb7qkl4z961f8g") (features (quote (("nightly"))))))

(define-public crate-kernlog-0.2 (crate (name "kernlog") (vers "0.2.0") (deps (list (crate-dep (name "log") (req "*") (default-features #t) (kind 0)))) (hash "1rihymd7jak81k7gl6sar7ac1ja71c6vf45ah4q94zs21zgzqdmj") (features (quote (("nightly"))))))

(define-public crate-kernlog-0.2 (crate (name "kernlog") (vers "0.2.1") (deps (list (crate-dep (name "log") (req "*") (default-features #t) (kind 0)))) (hash "0j1crn4xnmpcrvvxwylzxh0g47k3c6dzrc4fn0mnw08fz0qb966r") (features (quote (("nightly"))))))

(define-public crate-kernlog-0.3 (crate (name "kernlog") (vers "0.3.0") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4") (features (quote ("std"))) (default-features #t) (kind 0)))) (hash "0i0x01274rbb1mkzpx9fy4lsvvhis3i6al2qg8630gwihbz93djb")))

(define-public crate-kernlog-0.3 (crate (name "kernlog") (vers "0.3.1") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4") (features (quote ("std"))) (default-features #t) (kind 0)))) (hash "05vd5v8p0s1wzgixadcxjcxp4vgdqajgp1smpj9pra86xav7kpgh")))

