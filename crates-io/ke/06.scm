(define-module (crates-io ke #{06}#) #:use-module (crates-io))

(define-public crate-ke06z4-pac-0.1 (crate (name "ke06z4-pac") (vers "0.1.1") (deps (list (crate-dep (name "cortex-m") (req "^0.7") (default-features #t) (kind 0)) (crate-dep (name "cortex-m-rt") (req "^0.6.13") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "vcell") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "1llr48mhg77szzcxnl466v0v0dwp40nbj9pb28dylpvzcv5yz46l") (features (quote (("rt" "cortex-m-rt/device"))))))

