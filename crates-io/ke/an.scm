(define-module (crates-io ke an) #:use-module (crates-io))

(define-public crate-kean_minigrep-0.0.1 (crate (name "kean_minigrep") (vers "0.0.1") (hash "1ia8fmpxwn5bgscrfb77bjvilp91nsn8bz9p8b5vdk8hpv8wnsc9")))

(define-public crate-keanu-0.0.0 (crate (name "keanu") (vers "0.0.0") (hash "0l6q36xagdi2r3k1rva0bchywg8l3zpwgagiilv6l7wy9fg5p8ck")))

