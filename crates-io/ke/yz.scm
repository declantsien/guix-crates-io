(define-module (crates-io ke yz) #:use-module (crates-io))

(define-public crate-keyz-1 (crate (name "keyz") (vers "1.0.2") (deps (list (crate-dep (name "flate2") (req "^1.0.25") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.7.1") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.21.2") (features (quote ("full" "time"))) (default-features #t) (kind 0)))) (hash "03xmp64yczanrygzbqgkyz3qzfnspfz286d6g6a48as63hywfx5k")))

(define-public crate-keyz-1 (crate (name "keyz") (vers "1.0.3") (deps (list (crate-dep (name "flate2") (req "^1.0.25") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.7.1") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.21.2") (features (quote ("full" "time"))) (default-features #t) (kind 0)))) (hash "10q2n339bqqxhq43q09a9aiqknhzzwpl0ds5kmraszz7yf3b0dcs")))

(define-public crate-keyz-1 (crate (name "keyz") (vers "1.0.4") (deps (list (crate-dep (name "flate2") (req "^1.0.25") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.7.1") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.21.2") (features (quote ("full" "time"))) (default-features #t) (kind 0)))) (hash "05f7955qzbd5gzsv00v5gmdf1bvqj1h8y3xmp6xzvqhjz12kmyi6")))

(define-public crate-keyz_rust_client-1 (crate (name "keyz_rust_client") (vers "1.0.0") (deps (list (crate-dep (name "tokio") (req "^1.21.2") (features (quote ("full" "time"))) (default-features #t) (kind 0)))) (hash "1d5iwy1dldb7fhzpz41xfvnc6kw9f0zlpsy314nv8x3hzw27igsd")))

(define-public crate-keyz_rust_client-1 (crate (name "keyz_rust_client") (vers "1.0.1") (deps (list (crate-dep (name "tokio") (req "^1.21.2") (features (quote ("full" "time"))) (default-features #t) (kind 0)))) (hash "0fg56alfiz8s6fcbxrni0fddfqb5s8b8jrinbp0ysgk733kqhjdb")))

(define-public crate-keyz_rust_client-1 (crate (name "keyz_rust_client") (vers "1.0.2") (deps (list (crate-dep (name "tokio") (req "^1.21.2") (features (quote ("full" "time"))) (default-features #t) (kind 0)))) (hash "16ly29s8ilmji6gykz3g9yy5qpfc2z6blfsmpqx4sx69h3ri61kd")))

(define-public crate-keyz_rust_client-1 (crate (name "keyz_rust_client") (vers "1.0.3") (deps (list (crate-dep (name "tokio") (req "^1.21.2") (features (quote ("full" "time"))) (default-features #t) (kind 0)))) (hash "025c1bqskfgyh7j4kq0amrx4625py4xmwlhcj8di36ncy02x4iwl")))

(define-public crate-keyz_rust_client-1 (crate (name "keyz_rust_client") (vers "1.0.4") (deps (list (crate-dep (name "tokio") (req "^1.21.2") (features (quote ("full" "time"))) (default-features #t) (kind 0)))) (hash "19d14n7idn5gxrz53yqnq8mfgpa4aqdwhlf7yim7qw5kns0ck96c")))

(define-public crate-keyz_rust_client-1 (crate (name "keyz_rust_client") (vers "1.0.5") (deps (list (crate-dep (name "tokio") (req "^1.21.2") (features (quote ("full" "time"))) (default-features #t) (kind 0)))) (hash "1y2pazra8f684hmmigmlqh5w06qqvhpgvky1l2szzm3m9kdf2977")))

(define-public crate-keyzcli-0.1 (crate (name "keyzcli") (vers "0.1.0") (deps (list (crate-dep (name "ctrlc") (req "^3.2.4") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.21.2") (features (quote ("full" "time"))) (default-features #t) (kind 0)))) (hash "0x52zzwxg01annpiq1k3b83rf033il2qc9riqj2sz5968qs7f2gm")))

(define-public crate-keyzcli-0.1 (crate (name "keyzcli") (vers "0.1.1") (deps (list (crate-dep (name "ctrlc") (req "^3.2.4") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.21.2") (features (quote ("full" "time"))) (default-features #t) (kind 0)))) (hash "18pj406cl7wd3vdpnq4chhjk4zjh9avmkdp57l51bda1pcb18qp5")))

(define-public crate-keyzcli-0.1 (crate (name "keyzcli") (vers "0.1.2") (deps (list (crate-dep (name "ctrlc") (req "^3.2.4") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.21.2") (features (quote ("full" "time"))) (default-features #t) (kind 0)))) (hash "067mc9h16msppfwsy9lckciwzc0k4akiv5cvhpfjznwkkyl078fx")))

