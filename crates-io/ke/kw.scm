(define-module (crates-io ke kw) #:use-module (crates-io))

(define-public crate-kekw-0.0.0 (crate (name "kekw") (vers "0.0.0") (hash "1is383in0x59n221d629w8z0dyla436fh9125g26ifb9s5rgwlb7")))

(define-public crate-kekw_api-0.0.0 (crate (name "kekw_api") (vers "0.0.0") (hash "0z0fbkqgpr97hm4rayh0n560583qh2jmipa4haf8bajvigrmvhqn")))

(define-public crate-kekw_bot-0.0.0 (crate (name "kekw_bot") (vers "0.0.0") (hash "1kz6yhw1sr9k5aiicfd6d77pzr7wvln22s0rq112mcp2frwbnv7f")))

(define-public crate-kekw_client-0.0.0 (crate (name "kekw_client") (vers "0.0.0") (hash "12cmx47jarwx62p80nyjdi1k60p3km41i4qp8msssifl0mwc579s")))

(define-public crate-kekw_endpoints-0.0.0 (crate (name "kekw_endpoints") (vers "0.0.0") (hash "1h4wkmm4gml5kgqpvhzd6f2bipm5k0dlqjnhqs8qr5bhgsvv7nrv")))

(define-public crate-kekw_eventsub-0.0.0 (crate (name "kekw_eventsub") (vers "0.0.0") (hash "06s2w127h7ji43f8xvnaciiz86ld45v6h51zd62qc91jq31hmlrh") (features (quote (("docs"))))))

(define-public crate-kekw_macros-0.0.0 (crate (name "kekw_macros") (vers "0.0.0") (hash "0rjjnwwb5w6377gjk9rbvhr2f8hwhgn8y7di75wdabh6bs1nc0yp")))

(define-public crate-kekw_oauth2-0.0.0 (crate (name "kekw_oauth2") (vers "0.0.0") (hash "1dni5yq6hf7z3pb6lmb96lzp8r6y08fy8rddb4rpilfvx5cm1qpk")))

(define-public crate-kekw_types-0.0.0 (crate (name "kekw_types") (vers "0.0.0") (hash "1gh96lfb9q4j0bq170kg1qg12ywqlshvj6w6plpsry652m4gb994") (features (quote (("docs"))))))

