(define-module (crates-io ke yu) #:use-module (crates-io))

(define-public crate-keyuri-0.0.0 (crate (name "keyuri") (vers "0.0.0") (hash "1bmfr0z6dhk8b1njh5axmgfjf3v33dzbz3a8h2r5l7krmdrjjgxw")))

(define-public crate-keyutils-0.1 (crate (name "keyutils") (vers "0.1.0") (deps (list (crate-dep (name "bitflags") (req "~0.3.2") (default-features #t) (kind 0)) (crate-dep (name "errno") (req "~0.1.4") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "~0.2.2") (default-features #t) (kind 0)))) (hash "18pc2fkpi4v8crldcl5398l3hyz5wkcwswvq0z6s6jc0as1kiw3f")))

(define-public crate-keyutils-0.1 (crate (name "keyutils") (vers "0.1.1") (deps (list (crate-dep (name "bitflags") (req "~0.3.2") (default-features #t) (kind 0)) (crate-dep (name "errno") (req "~0.1.4") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "~0.2.2") (default-features #t) (kind 0)))) (hash "1a1krjx7jdacgh2r2v9g89vh8iia6b96gr3qfxwsi6szm9f6qsia")))

(define-public crate-keyutils-0.2 (crate (name "keyutils") (vers "0.2.0") (deps (list (crate-dep (name "bitflags") (req "~0.3.2") (default-features #t) (kind 0)) (crate-dep (name "errno") (req "~0.1.4") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "~0.2.2") (default-features #t) (kind 0)))) (hash "1793q8ry1al0dvrriy4bh0j3dll61jl18gfpm3r518lyd20ginfr")))

(define-public crate-keyutils-0.2 (crate (name "keyutils") (vers "0.2.1") (deps (list (crate-dep (name "bitflags") (req "~0.3.2") (default-features #t) (kind 0)) (crate-dep (name "errno") (req "~0.1.4") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "~0.2.2") (default-features #t) (kind 0)))) (hash "1k5jribk0bgvv3mdk8hw1m08qb9p55h0yybs8gqv9fzdkf2fgnyb")))

(define-public crate-keyutils-0.4 (crate (name "keyutils") (vers "0.4.0") (deps (list (crate-dep (name "bitflags") (req "^1.0.4") (default-features #t) (kind 0)) (crate-dep (name "errno") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "keyutils-raw") (req "^0.4.0") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1") (default-features #t) (kind 2)) (crate-dep (name "libc") (req "^0.2.68") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.4") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 2)) (crate-dep (name "semver") (req "^1") (default-features #t) (kind 2)) (crate-dep (name "uninit") (req "^0.3") (default-features #t) (kind 0)))) (hash "0drnfw5y1cr8dk5wa5c7jg8aifnxr6xzjmypmjbslmg2j9djfp8w")))

(define-public crate-keyutils-raw-0.4 (crate (name "keyutils-raw") (vers "0.4.0") (deps (list (crate-dep (name "errno") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.4") (default-features #t) (kind 0)) (crate-dep (name "uninit") (req "^0.3") (default-features #t) (kind 0)))) (hash "0arrg043ddrqdwvpf8xmg1ggzlk6i01xkafbgs6zvb4b6rwjwcnv")))

