(define-module (crates-io ke ed) #:use-module (crates-io))

(define-public crate-keedump-0.1 (crate (name "keedump") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^4.3.0") (features (quote ("derive" "env"))) (default-features #t) (kind 0)) (crate-dep (name "color-eyre") (req "^0.6.2") (default-features #t) (kind 0)) (crate-dep (name "itertools") (req "^0.10.5") (default-features #t) (kind 0)))) (hash "1nljkjqjpnaj3lbllqs63xncfbc453n6b8szqk68ddr9ngvagzpq")))

