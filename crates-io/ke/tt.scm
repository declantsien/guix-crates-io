(define-module (crates-io ke tt) #:use-module (crates-io))

(define-public crate-kettle-0.1 (crate (name "kettle") (vers "0.1.0") (deps (list (crate-dep (name "dirs") (req "^3.0.1") (default-features #t) (kind 0)) (crate-dep (name "rust-ini") (req "^0.15.3") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.20") (default-features #t) (kind 0)))) (hash "043wky20msrm3cd8f19av2ah5i0is7ligcpxxrmx2b7y3mb8gvsv") (yanked #t)))

(define-public crate-kettle-0.1 (crate (name "kettle") (vers "0.1.1") (deps (list (crate-dep (name "dirs") (req "^3.0.1") (default-features #t) (kind 0)) (crate-dep (name "rust-ini") (req "^0.15.3") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.20") (default-features #t) (kind 0)))) (hash "1yvsvvixpmw67dpp9rdkvd510g390l1mrd1264qm5s0am4883vhv") (yanked #t)))

(define-public crate-kettle-0.2 (crate (name "kettle") (vers "0.2.0") (deps (list (crate-dep (name "dirs") (req "^3.0.1") (default-features #t) (kind 0)) (crate-dep (name "paste") (req "^1.0.5") (default-features #t) (kind 0)) (crate-dep (name "rust-ini") (req "^0.15.3") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.20") (default-features #t) (kind 0)))) (hash "1wjrif9glmq46p2whi4yqhc2skqcmh391jb7g1z9vv95qj3imdm9") (yanked #t)))

(define-public crate-kettle-0.2 (crate (name "kettle") (vers "0.2.1") (deps (list (crate-dep (name "dirs") (req "^3.0.1") (default-features #t) (kind 0)) (crate-dep (name "paste") (req "^1.0.5") (default-features #t) (kind 0)) (crate-dep (name "rust-ini") (req "^0.15.3") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.20") (default-features #t) (kind 0)))) (hash "0lx9l8xqzhy13agr386k1vmgpzlam9qgjzd27r6frx949w55jfwq") (yanked #t)))

(define-public crate-kettle-0.2 (crate (name "kettle") (vers "0.2.2") (deps (list (crate-dep (name "dirs") (req "^3.0.1") (default-features #t) (kind 0)) (crate-dep (name "rust-ini") (req "^0.15.3") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.20") (default-features #t) (kind 0)))) (hash "18i4mc890mp623aw1h66g261117as0q4n7agmch5vn6gcgq24vnh") (yanked #t)))

