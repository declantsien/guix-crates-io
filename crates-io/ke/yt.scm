(define-module (crates-io ke yt) #:use-module (crates-io))

(define-public crate-keytar-0.1 (crate (name "keytar") (vers "0.1.0") (deps (list (crate-dep (name "cc") (req "^1.0.54") (default-features #t) (kind 1)) (crate-dep (name "cxx") (req "^0.3.4") (default-features #t) (kind 0)) (crate-dep (name "cxx-build") (req "^0.3.4") (default-features #t) (kind 1)) (crate-dep (name "pkg-config") (req "^0.3.17") (default-features #t) (kind 1)))) (hash "182cwhbqp1ypqy9xgazl81i9sg2pppv25l3zzd0a2rwmrg5qdi1d") (yanked #t)))

(define-public crate-keytar-0.1 (crate (name "keytar") (vers "0.1.1") (deps (list (crate-dep (name "cc") (req "^1.0.54") (default-features #t) (kind 1)) (crate-dep (name "cxx") (req "^0.3.4") (default-features #t) (kind 0)) (crate-dep (name "cxx-build") (req "^0.3.4") (default-features #t) (kind 1)) (crate-dep (name "pkg-config") (req "^0.3.17") (default-features #t) (kind 1)))) (hash "0d8mw2h97vfhlg8kfhpqbrysjrgq77mwn243jj15gqlh849f3x4r") (features (quote (("docs-rs")))) (yanked #t)))

(define-public crate-keytar-0.1 (crate (name "keytar") (vers "0.1.2") (deps (list (crate-dep (name "keytar-sys") (req "^0.1.2") (default-features #t) (kind 0)))) (hash "1c25ik8g1i250bq274d48rd31kl6lkw8jyis90zqnibdk9i7n70x")))

(define-public crate-keytar-0.1 (crate (name "keytar") (vers "0.1.3") (deps (list (crate-dep (name "keytar-sys") (req "^0.1.2") (default-features #t) (kind 0)))) (hash "138r71h2yx2kw7if2ssbal1xjb9s25d8saycz67r81n6v8aclmw0") (features (quote (("docs-rs" "keytar-sys/docs-rs"))))))

(define-public crate-keytar-0.1 (crate (name "keytar") (vers "0.1.4") (deps (list (crate-dep (name "keytar-sys") (req "^0.1.4") (default-features #t) (kind 0)))) (hash "1c7gps24m1r8klnnn371pabakr8fkiivw7jbchn029dzkmm6jxyz") (features (quote (("docs-rs" "keytar-sys/docs-rs"))))))

(define-public crate-keytar-0.1 (crate (name "keytar") (vers "0.1.5") (deps (list (crate-dep (name "keytar-sys") (req "^0.1.5") (default-features #t) (kind 0)))) (hash "1525mrj7bn8xxdkjra2w63ld77zp6hxsvijjjssh81wbrh5njigl") (features (quote (("docs-rs" "keytar-sys/docs-rs"))))))

(define-public crate-keytar-0.1 (crate (name "keytar") (vers "0.1.6") (deps (list (crate-dep (name "keytar-sys") (req "^0.1.6") (default-features #t) (kind 0)))) (hash "0kfagn3dmj6a9al20s1x1h1v2fgjbd1gah5h4339m0h9p9gwaqfk") (features (quote (("docs-rs" "keytar-sys/docs-rs"))))))

(define-public crate-keytar-sys-0.1 (crate (name "keytar-sys") (vers "0.1.2") (deps (list (crate-dep (name "cc") (req "^1.0.54") (default-features #t) (kind 1)) (crate-dep (name "cxx") (req "^0.4.0") (default-features #t) (kind 0)) (crate-dep (name "cxx-build") (req "^0.4.0") (default-features #t) (kind 1)) (crate-dep (name "pkg-config") (req "^0.3.17") (default-features #t) (kind 1)))) (hash "11cdjd9vn9wd1b850fm7saa8fiym6hcchrlpj20h25693q1h0p2m") (features (quote (("docs-rs"))))))

(define-public crate-keytar-sys-0.1 (crate (name "keytar-sys") (vers "0.1.4") (deps (list (crate-dep (name "cc") (req "^1.0.54") (default-features #t) (kind 1)) (crate-dep (name "cxx") (req "^1.0.0") (default-features #t) (kind 0)) (crate-dep (name "cxx-build") (req "^1.0.0") (default-features #t) (kind 1)) (crate-dep (name "pkg-config") (req "^0.3.17") (default-features #t) (kind 1)))) (hash "1zhzkhia45wxkz2nsyr8s8ja7svx2k1jfwjqdykcc1cckjz0p88y") (features (quote (("docs-rs"))))))

(define-public crate-keytar-sys-0.1 (crate (name "keytar-sys") (vers "0.1.5") (deps (list (crate-dep (name "cc") (req "^1.0.54") (default-features #t) (kind 1)) (crate-dep (name "cxx") (req "^1.0.0") (default-features #t) (kind 0)) (crate-dep (name "cxx-build") (req "^1.0.0") (default-features #t) (kind 1)) (crate-dep (name "pkg-config") (req "^0.3.17") (default-features #t) (kind 1)))) (hash "1j4y70zjy40p63gxsqalx9nbcb18jnkx25l90cmxrpyhrjn6j9s8") (features (quote (("docs-rs"))))))

(define-public crate-keytar-sys-0.1 (crate (name "keytar-sys") (vers "0.1.6") (deps (list (crate-dep (name "cc") (req "^1.0.54") (default-features #t) (kind 1)) (crate-dep (name "cxx") (req "^1.0.0") (default-features #t) (kind 0)) (crate-dep (name "cxx-build") (req "^1.0.0") (default-features #t) (kind 1)) (crate-dep (name "pkg-config") (req "^0.3.17") (default-features #t) (kind 1)))) (hash "0nm6wvhr7kcq78jr66z8mjaqvw33djamssnd2ssiqnkhjrl8r47y") (features (quote (("docs-rs"))))))

(define-public crate-keythai-0.1 (crate (name "keythai") (vers "0.1.0") (hash "0cs91x54qdwh8ibmy0fqc56vsms4hp7565f34vsg3w9vrg70jjdf")))

(define-public crate-keytokey-0.1 (crate (name "keytokey") (vers "0.1.0") (deps (list (crate-dep (name "heapless") (req "^0.4.4") (default-features #t) (kind 0)) (crate-dep (name "no-std-compat") (req "^0.1.0") (features (quote ("alloc"))) (default-features #t) (kind 0)) (crate-dep (name "num_enum") (req "^0.3.1") (default-features #t) (kind 0)) (crate-dep (name "smallbitvec") (req "^2.4.0") (kind 0)) (crate-dep (name "spin") (req "^0.5") (default-features #t) (kind 0)))) (hash "0b9s4ayyh28k0c9926yiwm1d1ljd45gbxvbhxh3g5rh0xfbxri4i")))

(define-public crate-keytokey-0.2 (crate (name "keytokey") (vers "0.2.0") (deps (list (crate-dep (name "heapless") (req "^0.4.4") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.3.0") (features (quote ("spin_no_std"))) (default-features #t) (kind 0)) (crate-dep (name "no-std-compat") (req "^0.1.0") (features (quote ("alloc"))) (default-features #t) (kind 0)) (crate-dep (name "num_enum") (req "^0.3.1") (default-features #t) (kind 0)) (crate-dep (name "smallbitvec") (req "^2.4.0") (kind 0)) (crate-dep (name "spin") (req "^0.5") (default-features #t) (kind 0)))) (hash "03nzdxv69sxbbmd3q6pcx0fa1796xcl49ch2z5lnykidym6xjcab")))

(define-public crate-keytokey-0.3 (crate (name "keytokey") (vers "0.3.0") (deps (list (crate-dep (name "heapless") (req "^0.4.4") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.3.0") (features (quote ("spin_no_std"))) (default-features #t) (kind 0)) (crate-dep (name "no-std-compat") (req "^0.1.0") (features (quote ("alloc"))) (default-features #t) (kind 0)) (crate-dep (name "num_enum") (req "^0.3.1") (default-features #t) (kind 0)) (crate-dep (name "smallbitvec") (req "^2.5.0") (default-features #t) (kind 0)) (crate-dep (name "spin") (req "^0.5") (default-features #t) (kind 0)))) (hash "0vvmdbp272592h762b07924184k4m3k4gjrr627f92gsidrafnza")))

(define-public crate-keytree-0.2 (crate (name "keytree") (vers "0.2.0") (hash "1aiclmj1v1x8i4dwiybkv2b2q0491aly3gldqzqi7qhv3k51ywb8")))

(define-public crate-keytree-0.2 (crate (name "keytree") (vers "0.2.1") (hash "0kc4m57dmdyybq88wmlzc067q87cyxslx1s46y18pvn57slv16sn")))

(define-public crate-keytree-0.2 (crate (name "keytree") (vers "0.2.2") (hash "1rqrmz5dla16cpv06mq34445vfql4cjfzyc3s46zj0khx6s1dw1k")))

(define-public crate-keytree-0.2 (crate (name "keytree") (vers "0.2.3") (hash "0ihq9q46r4209xw3hbawiq69knxkbypva1mcf9mpgsdy4hndlfyp")))

(define-public crate-keytree-0.2 (crate (name "keytree") (vers "0.2.4") (hash "07d19y7f7w00024k8snl4vm41108vd39j9by2qhiz7dan6b5cdyd")))

(define-public crate-keyturn-0.1 (crate (name "keyturn") (vers "0.1.0") (hash "1p78fnjhbvwmnlha775mq81x5jxwqz33q1xdrmykmab5q9vccpil")))

