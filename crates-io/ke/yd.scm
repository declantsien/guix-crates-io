(define-module (crates-io ke yd) #:use-module (crates-io))

(define-public crate-keydagger-0.1 (crate (name "keydagger") (vers "0.1.0") (hash "1mbshc39f99ip3ifk1x5rsdxqnq2rkm1mgh8dnl4i5i8rnfjrimp")))

(define-public crate-keyde-0.1 (crate (name "keyde") (vers "0.1.0") (deps (list (crate-dep (name "glam") (req "^0.22") (optional #t) (default-features #t) (kind 0)))) (hash "162i6jk65na60jqn2a3jbs8saxmb4z3dcqniqlcj7622pjswpx6f") (features (quote (("default")))) (yanked #t) (v 2) (features2 (quote (("glam" "dep:glam"))))))

(define-public crate-keyde-0.1 (crate (name "keyde") (vers "0.1.1") (deps (list (crate-dep (name "glam") (req "^0.14") (optional #t) (default-features #t) (kind 0)))) (hash "1y3f0cic3j0j9ba2q95pm0z2wv12w62d1n8p2n2nrfd1pjibn07h") (features (quote (("default")))) (yanked #t) (v 2) (features2 (quote (("glam" "dep:glam"))))))

(define-public crate-keyde-0.1 (crate (name "keyde") (vers "0.1.4") (deps (list (crate-dep (name "glam") (req "^0.21") (optional #t) (default-features #t) (kind 0)))) (hash "0kwgdkhnfz3f9mv5w6x7g8q7xi82b8ck42vw5nw0v1v9h33llfim") (features (quote (("default")))) (v 2) (features2 (quote (("glam" "dep:glam"))))))

(define-public crate-keyde-0.2 (crate (name "keyde") (vers "0.2.0") (deps (list (crate-dep (name "glam") (req "^0.21") (optional #t) (default-features #t) (kind 0)))) (hash "0g9b84rn3cpw671k0gmy950j8jhkkjdfr2fm0pdmdxrrv81k2lhx") (features (quote (("default")))) (v 2) (features2 (quote (("glam" "dep:glam"))))))

(define-public crate-keyde-0.2 (crate (name "keyde") (vers "0.2.1") (deps (list (crate-dep (name "glam") (req "^0.21") (optional #t) (default-features #t) (kind 0)))) (hash "12mrsv5fa1bjvw6f05bvn66dw290m02rlwvxcmxcawz7zhm8b00d") (features (quote (("default")))) (v 2) (features2 (quote (("glam" "dep:glam"))))))

(define-public crate-keyde-0.2 (crate (name "keyde") (vers "0.2.2") (deps (list (crate-dep (name "glam") (req "^0.21") (optional #t) (default-features #t) (kind 0)))) (hash "0flpjv6j2h5fh3j3g7r1rhhlmkg1lj2xd26733hg3aciwxavljgc") (features (quote (("default")))) (v 2) (features2 (quote (("glam" "dep:glam"))))))

(define-public crate-keyde-0.2 (crate (name "keyde") (vers "0.2.3") (deps (list (crate-dep (name "glam") (req "^0.21") (optional #t) (default-features #t) (kind 0)))) (hash "1i0rjhnbqqapq34pszxgsx7wclgsz6m3l3lhp9agbm4fkikw8q7y") (features (quote (("default")))) (v 2) (features2 (quote (("glam" "dep:glam"))))))

(define-public crate-keyde-0.2 (crate (name "keyde") (vers "0.2.4") (deps (list (crate-dep (name "glam") (req "^0.21") (optional #t) (default-features #t) (kind 0)))) (hash "0hwxcdllx2j8nc5k5lkh8vw56nfp0sisp68sja03g62n8dx9wbj9") (features (quote (("default")))) (v 2) (features2 (quote (("glam" "dep:glam"))))))

