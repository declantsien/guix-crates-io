(define-module (crates-io ke wb) #:use-module (crates-io))

(define-public crate-kewb-0.1 (crate (name "kewb") (vers "0.1.0") (deps (list (crate-dep (name "bincode") (req "^2.0.0-rc") (features (quote ("serde"))) (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^4.2.5") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.3") (default-features #t) (kind 0)) (crate-dep (name "spinners") (req "^4.1.0") (default-features #t) (kind 0)))) (hash "1scigyh6f3y84iwdsrgfdy47zdrkn4bwnx130bpmhr4zayygc6gr")))

(define-public crate-kewb-0.2 (crate (name "kewb") (vers "0.2.0") (deps (list (crate-dep (name "bincode") (req "^2.0.0-rc") (features (quote ("serde"))) (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^4.2.5") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.3") (default-features #t) (kind 0)) (crate-dep (name "spinners") (req "^4.1.0") (default-features #t) (kind 0)))) (hash "1b2i75k212qs008ggsjgmrpyz4dxg8j49ybggi6zambx245s5b91")))

(define-public crate-kewb-0.2 (crate (name "kewb") (vers "0.2.1") (deps (list (crate-dep (name "bincode") (req "^2.0.0-rc") (features (quote ("serde"))) (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^4.2.5") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.3") (default-features #t) (kind 0)) (crate-dep (name "spinners") (req "^4.1.0") (default-features #t) (kind 0)))) (hash "14w91k4bkkbpapci4a8dcb2z50xdaph1l8whfds96djfm21mlc7i")))

(define-public crate-kewb-0.3 (crate (name "kewb") (vers "0.3.0") (deps (list (crate-dep (name "bincode") (req "^2.0.0-rc") (features (quote ("serde"))) (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.3") (default-features #t) (kind 0)))) (hash "1825fl55xhnh3i9hfa6j7b7mc96a1kwi1fvsk8gcn9n9mwj215d4")))

(define-public crate-kewb-0.4 (crate (name "kewb") (vers "0.4.0") (deps (list (crate-dep (name "bincode") (req "^2.0.0-rc") (features (quote ("serde"))) (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.3") (default-features #t) (kind 0)))) (hash "1gadcdbl5wpavlgm2ka0fy7yh4s82d66pm99ficpcn14k1s0b3mj")))

(define-public crate-kewb-0.4 (crate (name "kewb") (vers "0.4.1") (deps (list (crate-dep (name "bincode") (req "^2.0.0-rc") (features (quote ("serde"))) (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.3") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.50") (default-features #t) (kind 0)))) (hash "0rzyq1jig93scc0h7yb7wq4l3xzlhyq9gkwjparlvr9n51cigwd8")))

(define-public crate-kewb-0.4 (crate (name "kewb") (vers "0.4.2") (deps (list (crate-dep (name "bincode") (req "^2.0.0-rc") (features (quote ("serde"))) (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.3") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.50") (default-features #t) (kind 0)))) (hash "1rngkbgq0jg2wy1xh8iw0v1yygij9b8lzifgks3l1vcabb1qax3l")))

