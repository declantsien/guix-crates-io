(define-module (crates-io ke yp) #:use-module (crates-io))

(define-public crate-keypad-0.1 (crate (name "keypad") (vers "0.1.0") (deps (list (crate-dep (name "embedded-hal") (req "^0.2.1") (features (quote ("unproven"))) (default-features #t) (kind 0)))) (hash "1sgdhp5pc17bhlkq6iz3hlzx7kd2fdza1kh5h6h9lmi39qsg5n72")))

(define-public crate-keypad-0.1 (crate (name "keypad") (vers "0.1.1") (deps (list (crate-dep (name "embedded-hal") (req "^0.2.1") (features (quote ("unproven"))) (default-features #t) (kind 0)))) (hash "0g14wv6zhc8j4djf1783jv5yb0j6mf1ygvnamsvrcm8gxrj0kldi")))

(define-public crate-keypad-0.1 (crate (name "keypad") (vers "0.1.2") (deps (list (crate-dep (name "embedded-hal") (req "^0.2.1") (features (quote ("unproven"))) (default-features #t) (kind 0)))) (hash "0kpnm334382jgrahcgphfkncglydfzdz6vl5lsvvgzq5l4705gfb")))

(define-public crate-keypad-0.1 (crate (name "keypad") (vers "0.1.3") (deps (list (crate-dep (name "embedded-hal") (req "^0.2.1") (features (quote ("unproven"))) (default-features #t) (kind 0)))) (hash "098904h0jb869y0pgzlkyngxamd7krnfk210l0zispplvizh69sb")))

(define-public crate-keypad-0.1 (crate (name "keypad") (vers "0.1.4") (deps (list (crate-dep (name "embedded-hal") (req "^0.2.1") (features (quote ("unproven"))) (default-features #t) (kind 0)))) (hash "076fj2p1c26z72ph7zviwpvmh335i18xf3pw5jrw7riwfbzy26rd") (features (quote (("example_generated"))))))

(define-public crate-keypad-0.2 (crate (name "keypad") (vers "0.2.0") (deps (list (crate-dep (name "embedded-hal") (req "^0.2.7") (features (quote ("unproven"))) (default-features #t) (kind 0)))) (hash "0dagly0hkkf2jh43z4zkihgpqimqibn72wq0fg1r1gql2li48nc0") (features (quote (("example_generated")))) (rust-version "1.56")))

(define-public crate-keypad-0.2 (crate (name "keypad") (vers "0.2.1") (deps (list (crate-dep (name "embedded-hal") (req "^0.2.7") (features (quote ("unproven"))) (default-features #t) (kind 0)))) (hash "020afcwflh4lv4a59adw1s8aa211jifn902wx3jykl2km5ivvcmz") (features (quote (("example_generated")))) (rust-version "1.56")))

(define-public crate-keypad-0.2 (crate (name "keypad") (vers "0.2.2") (deps (list (crate-dep (name "embedded-hal") (req "^0.2.7") (features (quote ("unproven"))) (default-features #t) (kind 0)))) (hash "0dqd6afab7rjfda18a7n4lpv8ym5gd2hlcpaf8bafy9wgxsril3c") (features (quote (("example_generated")))) (rust-version "1.56")))

(define-public crate-keypad2-0.1 (crate (name "keypad2") (vers "0.1.0") (deps (list (crate-dep (name "embedded-hal") (req "^0.2.4") (features (quote ("unproven"))) (default-features #t) (kind 0)))) (hash "0ki3s2182dkgpsvx14is8lkxkcbh6rrvh0gh4xczr21hns6qrxnn")))

(define-public crate-keypad2-0.1 (crate (name "keypad2") (vers "0.1.1") (deps (list (crate-dep (name "embedded-hal") (req "^0.2.4") (features (quote ("unproven"))) (default-features #t) (kind 0)))) (hash "1x1nf2ci39cydrf74faj752bpwznijayb94siwgflsk1hmk4j5n4")))

(define-public crate-keypair-rs-0.1 (crate (name "keypair-rs") (vers "0.1.0") (deps (list (crate-dep (name "blake2") (req "^0.9.2") (default-features #t) (kind 0)) (crate-dep (name "hex") (req "^0.4.3") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.4") (default-features #t) (kind 0)) (crate-dep (name "secp256k1") (req "^0.26.0") (features (quote ("bitcoin_hashes" "rand"))) (default-features #t) (kind 0)))) (hash "1y9i1myws3sv292j0cngmzjaic7jrr93yr8ax9kd2z5wi47r5r9y")))

(define-public crate-keypair-rs-0.1 (crate (name "keypair-rs") (vers "0.1.1") (deps (list (crate-dep (name "blake2") (req "^0.9.2") (default-features #t) (kind 0)) (crate-dep (name "hex") (req "^0.4.3") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.4") (default-features #t) (kind 0)) (crate-dep (name "secp256k1") (req "^0.26.0") (features (quote ("bitcoin_hashes" "rand"))) (default-features #t) (kind 0)))) (hash "1j3mk22ini5xc3qcls8r6abnnnj1yzak1v6l08n9ixq66lacfbkr")))

(define-public crate-keypaste-0.1 (crate (name "keypaste") (vers "0.1.0") (deps (list (crate-dep (name "clipboard") (req "^0.5.0") (default-features #t) (kind 0)) (crate-dep (name "enigo") (req "^0.0.14") (default-features #t) (kind 0)) (crate-dep (name "maplit") (req "^1.0.2") (default-features #t) (kind 0)))) (hash "15iidy1f6p7myhxkwhnpgh0dzy5dk2mxwb6x22ppxd13g048zmq5")))

(define-public crate-keypath-0.1 (crate (name "keypath") (vers "0.1.0") (hash "0w81vk7hc0gsbx82rwv1c8pvl6dzd9gwispg9c0ghpllanyfjcxv")))

(define-public crate-keypath-0.1 (crate (name "keypath") (vers "0.1.1") (deps (list (crate-dep (name "keypath-proc-macros") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "0ycvwnkkds0451mbjsqqbc3895v4cx2f9i6difyj1z9449b8qlg7")))

(define-public crate-keypath-0.2 (crate (name "keypath") (vers "0.2.0") (deps (list (crate-dep (name "keypath-proc-macros") (req "^0.2.0") (default-features #t) (kind 0)))) (hash "0n0qg1f5bi0frxvxffz9k0mx201s59pn3jhvif9nk906r7kzslqc")))

(define-public crate-keypath-proc-macros-0.1 (crate (name "keypath-proc-macros") (vers "0.1.0") (deps (list (crate-dep (name "keypath") (req "^0.1.0") (default-features #t) (kind 2)) (crate-dep (name "litrs") (req "^0.2.3") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.39") (default-features #t) (kind 0)))) (hash "1riz2bw4m9hnvr8zhvay4fx6hlygaqqgk1c1d6dgc80adb4g55ha")))

(define-public crate-keypath-proc-macros-0.2 (crate (name "keypath-proc-macros") (vers "0.2.0") (deps (list (crate-dep (name "keypath") (req "^0.2.0") (default-features #t) (kind 2)) (crate-dep (name "litrs") (req "^0.2.3") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.39") (default-features #t) (kind 0)) (crate-dep (name "trybuild") (req "^1.0") (features (quote ("diff"))) (default-features #t) (kind 2)))) (hash "0zl63kh98c4wp2v9ssnr4f1jz2g1789xypydhlnyyv1md3q43bwv")))

(define-public crate-keyphrase-0.0.1 (crate (name "keyphrase") (vers "0.0.1") (deps (list (crate-dep (name "better-panic") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "failure") (req "^0.1.3") (default-features #t) (kind 0)) (crate-dep (name "hashbrown") (req "^0.1.7") (default-features #t) (kind 0)) (crate-dep (name "hmac") (req "^0.7.0") (default-features #t) (kind 0)) (crate-dep (name "once_cell") (req "^0.1.6") (features (quote ("parking_lot"))) (default-features #t) (kind 0)) (crate-dep (name "pbkdf2") (req "^0.3.0") (features (quote ("parallel"))) (kind 0)) (crate-dep (name "rand") (req "^0.6.1") (default-features #t) (kind 0)) (crate-dep (name "sha2") (req "^0.8.0") (default-features #t) (kind 0)))) (hash "0y228kdvj0lf3wsv5qv9zysv4xb8njnl0rckbi9k8s8k55m3c4hv") (features (quote (("spanish") ("korean") ("japanese") ("italian") ("french") ("default" "chinese-simplified" "chinese-traditional" "french" "italian" "japanese" "korean" "spanish") ("chinese-traditional") ("chinese-simplified"))))))

(define-public crate-keyphrase-0.0.2 (crate (name "keyphrase") (vers "0.0.2") (deps (list (crate-dep (name "better-panic") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "failure") (req "^0.1.3") (default-features #t) (kind 0)) (crate-dep (name "hashbrown") (req "^0.1.7") (default-features #t) (kind 0)) (crate-dep (name "hmac") (req "^0.7.0") (default-features #t) (kind 0)) (crate-dep (name "once_cell") (req "^0.1.6") (features (quote ("parking_lot"))) (default-features #t) (kind 0)) (crate-dep (name "pbkdf2") (req "^0.3.0") (features (quote ("parallel"))) (kind 0)) (crate-dep (name "rand") (req "^0.6.1") (default-features #t) (kind 0)) (crate-dep (name "sha2") (req "^0.8.0") (default-features #t) (kind 0)))) (hash "0529fx5lmqwmrx8rn6wwmy5a1fnxiap706c9nxrc9l8ya94a6rk8") (features (quote (("spanish") ("korean") ("japanese") ("italian") ("french") ("default" "chinese-simplified" "chinese-traditional" "french" "italian" "japanese" "korean" "spanish") ("chinese-traditional") ("chinese-simplified"))))))

(define-public crate-keyphrase-0.0.3 (crate (name "keyphrase") (vers "0.0.3") (deps (list (crate-dep (name "better-panic") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "failure") (req "^0.1.3") (default-features #t) (kind 0)) (crate-dep (name "hashbrown") (req "^0.1.7") (default-features #t) (kind 0)) (crate-dep (name "hmac") (req "^0.7.0") (default-features #t) (kind 0)) (crate-dep (name "once_cell") (req "^0.1.6") (features (quote ("parking_lot"))) (default-features #t) (kind 0)) (crate-dep (name "pbkdf2") (req "^0.3.0") (features (quote ("parallel"))) (kind 0)) (crate-dep (name "rand") (req "^0.6.1") (default-features #t) (kind 0)) (crate-dep (name "sha2") (req "^0.8.0") (default-features #t) (kind 0)))) (hash "0s8ayyf5bwxx2kc1rwj2ygk34v3fc5rbryvcls014szv1gq6wm9z") (features (quote (("spanish") ("korean") ("japanese") ("italian") ("french") ("default" "chinese-simplified" "chinese-traditional" "french" "italian" "japanese" "korean" "spanish") ("chinese-traditional") ("chinese-simplified"))))))

(define-public crate-keyphrase-0.1 (crate (name "keyphrase") (vers "0.1.0") (deps (list (crate-dep (name "better-panic") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "failure") (req "^0.1.3") (default-features #t) (kind 0)) (crate-dep (name "hashbrown") (req "^0.1.7") (default-features #t) (kind 0)) (crate-dep (name "hmac") (req "^0.7.0") (default-features #t) (kind 0)) (crate-dep (name "once_cell") (req "^0.1.6") (features (quote ("parking_lot"))) (default-features #t) (kind 0)) (crate-dep (name "pbkdf2") (req "^0.3.0") (features (quote ("parallel"))) (kind 0)) (crate-dep (name "rand") (req "^0.6.1") (default-features #t) (kind 0)) (crate-dep (name "sha2") (req "^0.8.0") (default-features #t) (kind 0)))) (hash "0xiabzk8dj5pxlsjql9824ig6fqfas0vai0jwlrjl5is1z86y4d3") (features (quote (("spanish") ("korean") ("japanese") ("italian") ("french") ("default" "chinese-simplified" "chinese-traditional" "french" "italian" "japanese" "korean" "spanish") ("chinese-traditional") ("chinese-simplified"))))))

(define-public crate-keyphrase-0.1 (crate (name "keyphrase") (vers "0.1.1") (deps (list (crate-dep (name "better-panic") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "failure") (req "^0.1.3") (default-features #t) (kind 0)) (crate-dep (name "hashbrown") (req "^0.1.7") (default-features #t) (kind 0)) (crate-dep (name "hmac") (req "^0.7.0") (default-features #t) (kind 0)) (crate-dep (name "once_cell") (req "^0.1.6") (features (quote ("parking_lot"))) (default-features #t) (kind 0)) (crate-dep (name "pbkdf2") (req "^0.3.0") (features (quote ("parallel"))) (kind 0)) (crate-dep (name "rand") (req "^0.6.1") (default-features #t) (kind 0)) (crate-dep (name "sha2") (req "^0.8.0") (default-features #t) (kind 0)))) (hash "1vrd881wpnb9fzs9anzi16g6ryzwq5cr94kwkziv1r225wrh4460") (features (quote (("spanish") ("korean") ("japanese") ("italian") ("french") ("default" "chinese-simplified" "chinese-traditional" "french" "italian" "japanese" "korean" "spanish") ("chinese-traditional") ("chinese-simplified"))))))

(define-public crate-keyphrase-0.1 (crate (name "keyphrase") (vers "0.1.2") (deps (list (crate-dep (name "better-panic") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "failure") (req "^0.1.3") (default-features #t) (kind 0)) (crate-dep (name "hashbrown") (req "^0.1.7") (default-features #t) (kind 0)) (crate-dep (name "hmac") (req "^0.7.0") (default-features #t) (kind 0)) (crate-dep (name "once_cell") (req "^0.1.6") (features (quote ("parking_lot"))) (default-features #t) (kind 0)) (crate-dep (name "pbkdf2") (req "^0.3.0") (features (quote ("parallel"))) (kind 0)) (crate-dep (name "rand") (req "^0.6.1") (default-features #t) (kind 0)) (crate-dep (name "sha2") (req "^0.8.0") (default-features #t) (kind 0)))) (hash "0115k532jg24p5wvjrdjnz3mjsy4pphp8vqb1n3yiq8vsrf89295") (features (quote (("spanish") ("korean") ("japanese") ("italian") ("french") ("default" "chinese-simplified" "chinese-traditional" "french" "italian" "japanese" "korean" "spanish") ("chinese-traditional") ("chinese-simplified"))))))

(define-public crate-keyphrase-0.1 (crate (name "keyphrase") (vers "0.1.3") (deps (list (crate-dep (name "better-panic") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "failure") (req "^0.1.3") (default-features #t) (kind 0)) (crate-dep (name "hashbrown") (req "^0.1.7") (default-features #t) (kind 0)) (crate-dep (name "hmac") (req "^0.7.0") (default-features #t) (kind 0)) (crate-dep (name "once_cell") (req "^0.1.6") (features (quote ("parking_lot"))) (default-features #t) (kind 0)) (crate-dep (name "pbkdf2") (req "^0.3.0") (features (quote ("parallel"))) (kind 0)) (crate-dep (name "rand") (req "^0.6.1") (default-features #t) (kind 0)) (crate-dep (name "sha2") (req "^0.8.0") (default-features #t) (kind 0)))) (hash "0r54m1xn383n4plvajdhs6ladsc2835lvajdf1mbp8d34nakr669") (features (quote (("spanish") ("korean") ("japanese") ("italian") ("french") ("default" "chinese-simplified" "chinese-traditional" "french" "italian" "japanese" "korean" "spanish") ("chinese-traditional") ("chinese-simplified"))))))

(define-public crate-keyphrase-0.1 (crate (name "keyphrase") (vers "0.1.4") (deps (list (crate-dep (name "better-panic") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "failure") (req "^0.1.3") (default-features #t) (kind 0)) (crate-dep (name "hashbrown") (req "^0.1.7") (default-features #t) (kind 0)) (crate-dep (name "hmac") (req "^0.7.0") (default-features #t) (kind 0)) (crate-dep (name "once_cell") (req "^0.1.6") (features (quote ("parking_lot"))) (default-features #t) (kind 0)) (crate-dep (name "pbkdf2") (req "^0.3.0") (features (quote ("parallel"))) (kind 0)) (crate-dep (name "rand") (req "^0.6.1") (default-features #t) (kind 0)) (crate-dep (name "sha2") (req "^0.8.0") (default-features #t) (kind 0)))) (hash "11rlw3b0l31rmr64ijhd7lg7yd0mgm6p0vdmdgkk6mw8m8dnzfjb") (features (quote (("spanish") ("korean") ("japanese") ("italian") ("french") ("default" "chinese-simplified" "chinese-traditional" "french" "italian" "japanese" "korean" "spanish") ("chinese-traditional") ("chinese-simplified"))))))

(define-public crate-keyphrases-0.1 (crate (name "keyphrases") (vers "0.1.0") (deps (list (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)))) (hash "030n94gdy1qa4d2ylhy5pnalqfh3g3a8hnb70bfhj0s1w4d4cr3j") (yanked #t)))

(define-public crate-keyphrases-0.1 (crate (name "keyphrases") (vers "0.1.1") (deps (list (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)))) (hash "0pni48yxiklzfqnp308kslkx0z3xv3c3nafr4cryqls9ri96ab2m") (yanked #t)))

(define-public crate-keyphrases-0.1 (crate (name "keyphrases") (vers "0.1.2") (deps (list (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)))) (hash "1kc6mpcsb4887m8q6kgi133dqz7limfybxxz1ywfflpzw1rcpqi5") (yanked #t)))

(define-public crate-keyphrases-0.1 (crate (name "keyphrases") (vers "0.1.3") (deps (list (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)))) (hash "02xipklmvjnia2yhd3pszdkrmr9s24hz9s6m06czk2g1wx8m63wy")))

(define-public crate-keyphrases-0.1 (crate (name "keyphrases") (vers "0.1.4") (deps (list (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)))) (hash "0aaydqqqzbdngw6lfz2a17c8ach1w4d1cckywxki9dmzrqihkncg")))

(define-public crate-keyphrases-0.1 (crate (name "keyphrases") (vers "0.1.5") (deps (list (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)))) (hash "163vsh0z1mw7pf11m2j13x0wdg338zzzh34c43ffy27i981nnqrp")))

(define-public crate-keyphrases-0.1 (crate (name "keyphrases") (vers "0.1.6") (deps (list (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)))) (hash "05a9p95q8b88g6v4zzq0wy4fzmb2nq1mc6wpfkcix36wjf6g26mx")))

(define-public crate-keyphrases-0.2 (crate (name "keyphrases") (vers "0.2.0") (deps (list (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)))) (hash "0qkzj305adjwh8hs0fxsas2lmh0j9lhgdw46622q3bsy9z6nxiyw")))

(define-public crate-keyphrases-0.3 (crate (name "keyphrases") (vers "0.3.0") (deps (list (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)))) (hash "1grlrjcj9qy92m5yfysfjwmm6pnzcg24ysjr96g0nkdjkj707r7h") (yanked #t)))

(define-public crate-keyphrases-0.3 (crate (name "keyphrases") (vers "0.3.1") (deps (list (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)))) (hash "1h1c3h9xjhv6c7azr6b0nvmdqy6qb3hyv69gyz3k42x8a0k4iwc4") (yanked #t)))

(define-public crate-keyphrases-0.3 (crate (name "keyphrases") (vers "0.3.2") (deps (list (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)))) (hash "18m46yvkjvf3nwqfhap5yb5v52l6xid7ja2kdqkp8mnya2m9nink")))

(define-public crate-keyphrases-0.3 (crate (name "keyphrases") (vers "0.3.3") (deps (list (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)))) (hash "02jdv9azpsvpl2d4710pddbgqlvkxbry53ik3dnmbz5k6wa1fkja")))

(define-public crate-keyplace-0.1 (crate (name "keyplace") (vers "0.1.0") (deps (list (crate-dep (name "base64") (req "^0.12.3") (default-features #t) (kind 0)) (crate-dep (name "bincode") (req "^1.3") (default-features #t) (kind 0)) (crate-dep (name "constant_time_eq") (req "^0.1.5") (default-features #t) (kind 0)) (crate-dep (name "crypto-mac") (req "^0.9") (default-features #t) (kind 0)) (crate-dep (name "ed25519-dalek") (req "^1.0") (features (quote ("serde"))) (default-features #t) (kind 0)) (crate-dep (name "hmac") (req "^0.9") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.7") (default-features #t) (kind 0)) (crate-dep (name "scrypt") (req "^0.4.1") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "sha2") (req "^0.9") (default-features #t) (kind 0)) (crate-dep (name "toboggan-kv") (req "^0.1.2") (default-features #t) (kind 0)) (crate-dep (name "wasm-bindgen") (req "^0.2") (default-features #t) (target "cfg(any(target_arch = \"wasm32\"))") (kind 0)) (crate-dep (name "wasm-bindgen-console-logger") (req "^0.1.1") (default-features #t) (target "cfg(any(target_arch = \"wasm32\"))") (kind 0)) (crate-dep (name "wasm-bindgen-futures") (req "^0.4.5") (default-features #t) (target "cfg(any(target_arch = \"wasm32\"))") (kind 0)) (crate-dep (name "wasm-bindgen-test") (req "^0.3") (default-features #t) (target "cfg(any(target_arch = \"wasm32\"))") (kind 0)) (crate-dep (name "zeroize") (req "^1.1") (features (quote ("zeroize_derive"))) (default-features #t) (kind 0)))) (hash "017im9c3ic0bdczsdzmv1n4x7gf4fbiajkvslmds2wn2zk5rh68x")))

(define-public crate-keyplace-0.1 (crate (name "keyplace") (vers "0.1.1") (deps (list (crate-dep (name "base64") (req "^0.12.3") (default-features #t) (kind 0)) (crate-dep (name "bincode") (req "^1.3") (default-features #t) (kind 0)) (crate-dep (name "constant_time_eq") (req "^0.1.5") (default-features #t) (kind 0)) (crate-dep (name "crypto-mac") (req "^0.9") (default-features #t) (kind 0)) (crate-dep (name "ed25519-dalek") (req "^1.0") (features (quote ("serde"))) (default-features #t) (kind 0)) (crate-dep (name "hmac") (req "^0.9") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.7") (default-features #t) (kind 0)) (crate-dep (name "scrypt") (req "^0.4.1") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "sha2") (req "^0.9") (default-features #t) (kind 0)) (crate-dep (name "toboggan-kv") (req "^0.1.2") (default-features #t) (kind 0)) (crate-dep (name "wasm-bindgen") (req "^0.2") (default-features #t) (target "cfg(any(target_arch = \"wasm32\"))") (kind 0)) (crate-dep (name "wasm-bindgen-console-logger") (req "^0.1.1") (default-features #t) (target "cfg(any(target_arch = \"wasm32\"))") (kind 0)) (crate-dep (name "wasm-bindgen-futures") (req "^0.4.5") (default-features #t) (target "cfg(any(target_arch = \"wasm32\"))") (kind 0)) (crate-dep (name "wasm-bindgen-test") (req "^0.3") (default-features #t) (target "cfg(any(target_arch = \"wasm32\"))") (kind 0)) (crate-dep (name "zeroize") (req "^1.1") (features (quote ("zeroize_derive"))) (default-features #t) (kind 0)))) (hash "0cijcfz904sdw9jd0i07ng3zq0sqc9mr1zgdl5x5rj6aiasyc3qb")))

(define-public crate-keypropdecode-0.0.0 (crate (name "keypropdecode") (vers "0.0.0") (hash "0n4g767b3by69hqzzw0q0bwjsf1x7qgri6ylnss57vcflxkxz6ng") (yanked #t)))

(define-public crate-keypropdecode-0.1 (crate (name "keypropdecode") (vers "0.1.0") (hash "0rdw131ya0hiif85zvp6902mm826kj2kjhy8sk4ml9jb0jrnhkcf") (yanked #t)))

(define-public crate-keypropdecode-1 (crate (name "keypropdecode") (vers "1.0.0") (hash "0sa4k3rcghv1sx2679sv31cqbjqvfwdvignx422lyhcfbzgpvfbw") (yanked #t)))

(define-public crate-keypropdecode-1 (crate (name "keypropdecode") (vers "1.0.1") (hash "1194190892m83j5bawh5bb7c89wa8q5ilr5dn45h5glg79ax536r") (yanked #t)))

(define-public crate-keypropdecode-1 (crate (name "keypropdecode") (vers "1.0.2") (hash "1gpjrzvg0bnv5y8yv3dc8jxb0m9raw8ny8bnwhwd6c2gymfr3hs2")))

(define-public crate-keypropdecode-1 (crate (name "keypropdecode") (vers "1.0.3") (hash "0j83g1c54f1ip9bh1jp761jq5m2b60racpn48vlkvfmip3s9zh13")))

(define-public crate-keypropdecode-1 (crate (name "keypropdecode") (vers "1.1.0") (hash "1yc9cdyr6ads04apdyzn9997cdpvxxpvnyfk4c96vgl6y8qkgx0i") (yanked #t)))

(define-public crate-keypropdecode-1 (crate (name "keypropdecode") (vers "1.1.1") (hash "0s7qyc62g36rwszw95ng9kmsizxkmhaglxc6j3cqra9ydspipyvk") (yanked #t)))

(define-public crate-keypropdecode-1 (crate (name "keypropdecode") (vers "1.1.2") (hash "0qzyazwwr4qram1xsmggc0s41fsk5ffrhns2kbf0wd64mvayh260") (yanked #t)))

(define-public crate-keypropdecode-1 (crate (name "keypropdecode") (vers "1.1.3") (hash "1d0b6inmxgb7f1hcdqnshqz2zbzl2scg8h71xaq7nw02q7a3r584") (yanked #t)))

(define-public crate-keypropdecode-1 (crate (name "keypropdecode") (vers "1.1.4") (hash "0ypj86jrfda96sjf1iynsiqxsasm47bhpm634aipncaypbxvihdj") (yanked #t)))

(define-public crate-keypropdecode-1 (crate (name "keypropdecode") (vers "1.1.5") (hash "0cisicmghimpsh1cavbdnp63naw16hmfq5kqclyflx4nklah9waj")))

(define-public crate-keypropdecode-1 (crate (name "keypropdecode") (vers "1.1.6") (hash "0lwf4w9r8glp31fbhmkmwk8rlrfq5965iady90rypklgf89v2083")))

(define-public crate-keypropdecode-1 (crate (name "keypropdecode") (vers "1.1.7") (hash "05i37w9v423bkdz99kz1yqmhkhhqrjf7hdafmx5kdg8y3wqxpmig")))

(define-public crate-keypropdecode-1 (crate (name "keypropdecode") (vers "1.1.8") (hash "0d6zi3jcadk8ivhsq65d6gwi3zn670z9zgh4qi44cp0bwsjyhm1j")))

(define-public crate-keypropdecode-2 (crate (name "keypropdecode") (vers "2.0.0") (hash "1nc417a7sx5w63iginr3ygcji0mmz93c8fc4v8nfz0dzpzrz5pbn")))

(define-public crate-keypropdecode-2 (crate (name "keypropdecode") (vers "2.0.1") (hash "01d7gsnpp9gnvgzhixnxjw3gm5sy1fqwvn1m4p8iiv7vdy4hkacl")))

(define-public crate-keypropdecode-2 (crate (name "keypropdecode") (vers "2.0.2") (hash "0b8hj4fa3y92zmsz1f54r0751pfsmxkv5j6drga5wr69x3qg1hlf")))

(define-public crate-keypropdecode-2 (crate (name "keypropdecode") (vers "2.0.3") (hash "0cz6iy2klx8v2mmancs7rkh90adh24pnmq1g1hjnpskmc2y7c54m")))

