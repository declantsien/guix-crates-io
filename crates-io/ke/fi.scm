(define-module (crates-io ke fi) #:use-module (crates-io))

(define-public crate-kefia-0.1 (crate (name "kefia") (vers "0.1.0") (deps (list (crate-dep (name "lazysort") (req "^0.1.1") (default-features #t) (kind 0)) (crate-dep (name "qml") (req "^0.0.9") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^0.2.1") (default-features #t) (kind 0)))) (hash "1fsgza3pz87zyj4hyfx6zjr21m6xnl1magc75zcb8pxpi9164mnr")))

