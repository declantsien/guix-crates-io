(define-module (crates-io ke et) #:use-module (crates-io))

(define-public crate-keetree-0.0.1 (crate (name "keetree") (vers "0.0.1") (deps (list (crate-dep (name "criterion") (req "^0.5.1") (default-features #t) (kind 2)) (crate-dep (name "hashbrown") (req "^0.14.3") (features (quote ("ahash" "inline-more"))) (kind 0)) (crate-dep (name "matchit") (req "^0.7.3") (default-features #t) (kind 2)) (crate-dep (name "regex") (req "^1.10.2") (features (quote ("perf" "unicode"))) (kind 0)))) (hash "1cnwbmnp72rjylsq1gjq1nnjm6qvm97bg2hnq2b7mn8bgsv163x2") (features (quote (("std" "regex/std") ("default" "std"))))))

