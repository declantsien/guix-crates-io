(define-module (crates-io ke mk) #:use-module (crates-io))

(define-public crate-kemkem-1 (crate (name "kemkem") (vers "1.0.0") (deps (list (crate-dep (name "bitvec") (req "^1.0.1") (default-features #t) (kind 0)) (crate-dep (name "criterion") (req "^0.5.1") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)) (crate-dep (name "sha3") (req "^0.10.8") (default-features #t) (kind 0)))) (hash "03586q4m02pqrxfjbcfgh5cd5wfjwd7g5bx2nyg1p1h09nx9kipy")))

(define-public crate-kemkem-1 (crate (name "kemkem") (vers "1.0.1") (deps (list (crate-dep (name "bitvec") (req "^1.0.1") (default-features #t) (kind 0)) (crate-dep (name "criterion") (req "^0.5.1") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)) (crate-dep (name "sha3") (req "^0.10.8") (default-features #t) (kind 0)))) (hash "0dvagl5xwygjjsbiqkbqyci2z98b9x2vbvhqg1zxny7q2zlr0ibh")))

(define-public crate-kemkem-1 (crate (name "kemkem") (vers "1.0.2") (deps (list (crate-dep (name "bitvec") (req "^1.0.1") (default-features #t) (kind 0)) (crate-dep (name "criterion") (req "^0.5.1") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)) (crate-dep (name "sha3") (req "^0.10.8") (default-features #t) (kind 0)))) (hash "1r4xs4n8sky581qmvm16ppvcaglmc247mrz4f6bgrngxzy9h6im7")))

