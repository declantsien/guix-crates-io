(define-module (crates-io ke cc) #:use-module (crates-io))

(define-public crate-keccak-0.0.0 (crate (name "keccak") (vers "0.0.0") (hash "1nm5ddirs2sk4cc2666qyizncv0azn0fzyq39vrbn41ssr0b5ihd") (yanked #t)))

(define-public crate-keccak-0.1 (crate (name "keccak") (vers "0.1.0") (hash "19ybbvxrdk9yy65rk7f5ad0hcxszkjwph68yzkj3954lnir1bhk7") (features (quote (("no_unroll"))))))

(define-public crate-keccak-0.1 (crate (name "keccak") (vers "0.1.1") (deps (list (crate-dep (name "packed_simd") (req "^0.3.7") (optional #t) (default-features #t) (kind 0) (package "packed_simd_2")))) (hash "1kgp4vxsfpnp2rnv2dn48kr1sv5w83bjrjc3qnpdsx4kkqqpw4a8") (features (quote (("simd" "packed_simd") ("no_unroll"))))))

(define-public crate-keccak-0.1 (crate (name "keccak") (vers "0.1.2") (hash "0f78zsqzdjwhwfgzgkcqk7r8m4kgn3k9b6bjx5mlsd58limxbdzr") (features (quote (("simd") ("no_unroll"))))))

(define-public crate-keccak-0.1 (crate (name "keccak") (vers "0.1.3") (deps (list (crate-dep (name "cpufeatures") (req "^0.2") (default-features #t) (target "cfg(target_arch = \"aarch64\")") (kind 0)))) (hash "0s47r5bgllyhapri9gbsmk5wiw67xqji2q5kz67rvkprxyvg7zis") (features (quote (("simd") ("no_unroll") ("asm"))))))

(define-public crate-keccak-0.1 (crate (name "keccak") (vers "0.1.4") (deps (list (crate-dep (name "cpufeatures") (req "^0.2") (default-features #t) (target "cfg(target_arch = \"aarch64\")") (kind 0)))) (hash "0h7rcvwvf20g4k8cx2brnrqvah6jwzs84w09vrj4743dczc5wvcg") (features (quote (("simd") ("no_unroll") ("asm"))))))

(define-public crate-keccak-0.1 (crate (name "keccak") (vers "0.1.5") (deps (list (crate-dep (name "cpufeatures") (req "^0.2") (default-features #t) (target "cfg(target_arch = \"aarch64\")") (kind 0)))) (hash "0m06swsyd58hvb1z17q6picdwywprf1yf1s6l491zi8r26dazhpc") (features (quote (("simd") ("no_unroll") ("asm"))))))

(define-public crate-keccak-0.2 (crate (name "keccak") (vers "0.2.0-pre.0") (deps (list (crate-dep (name "cpufeatures") (req "^0.2") (default-features #t) (target "cfg(target_arch = \"aarch64\")") (kind 0)))) (hash "143y5m19fvp3rlgg8ixxwxpn0qp8b0zqmm2xnaibj1sqvkqd9kd7") (features (quote (("simd") ("no_unroll") ("asm")))) (rust-version "1.60")))

(define-public crate-keccak-asm-0.1 (crate (name "keccak-asm") (vers "0.1.0") (deps (list (crate-dep (name "digest") (req "^0.10") (default-features #t) (kind 0)) (crate-dep (name "digest") (req "^0.10") (features (quote ("dev"))) (default-features #t) (kind 2)) (crate-dep (name "hex") (req "^1.10") (default-features #t) (kind 2) (package "const-hex")) (crate-dep (name "sha3-asm") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "zeroize") (req "^1.6.0") (optional #t) (kind 0)))) (hash "0i7lrab929pmxdlf2bx800m3w04wa4p5ynd1ljp51n0fz3zib1dv") (features (quote (("std" "digest/std") ("oid" "digest/oid") ("default" "std")))) (v 2) (features2 (quote (("zeroize" "dep:zeroize")))) (rust-version "1.64")))

(define-public crate-keccak-asm-0.1 (crate (name "keccak-asm") (vers "0.1.1") (deps (list (crate-dep (name "digest") (req "^0.10") (default-features #t) (kind 0)) (crate-dep (name "digest") (req "^0.10") (features (quote ("dev"))) (default-features #t) (kind 2)) (crate-dep (name "hex") (req "^1.10") (default-features #t) (kind 2) (package "const-hex")) (crate-dep (name "sha3-asm") (req "^0.1.1") (default-features #t) (kind 0)) (crate-dep (name "zeroize") (req "^1.6.0") (optional #t) (kind 0)))) (hash "0n47j8lr3myw4s79pzqq2harvv043fyaqww6pyz4yk43j4r678s7") (features (quote (("std" "digest/std") ("oid" "digest/oid") ("default" "std")))) (v 2) (features2 (quote (("zeroize" "dep:zeroize")))) (rust-version "1.64")))

(define-public crate-keccak-const-0.2 (crate (name "keccak-const") (vers "0.2.0") (hash "1shkvzib7cd63wga2mgp7bdpan4nfwxgzfz0b1ii603jhz7din2p")))

(define-public crate-keccak-hash-0.1 (crate (name "keccak-hash") (vers "0.1.0") (deps (list (crate-dep (name "cc") (req "^1.0") (default-features #t) (kind 1)) (crate-dep (name "ethcore-bigint") (req "^0.2.1") (default-features #t) (kind 0)) (crate-dep (name "tempdir") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "tiny-keccak") (req "^1.3") (default-features #t) (kind 0)))) (hash "1hcy4lbajjl2k5ab3f0g88bpan9sf5p4zlpf2i9cmncw2hghqc0z")))

(define-public crate-keccak-hash-0.1 (crate (name "keccak-hash") (vers "0.1.1") (deps (list (crate-dep (name "cc") (req "^1.0") (default-features #t) (kind 1)) (crate-dep (name "ethereum-types") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "tempdir") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "tiny-keccak") (req "^1.3") (default-features #t) (kind 0)))) (hash "1rrsb8wvr4jjlriji2pypwyzp380i2iadi5frim571kr1prm2zqb")))

(define-public crate-keccak-hash-0.1 (crate (name "keccak-hash") (vers "0.1.2") (deps (list (crate-dep (name "ethereum-types") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "tempdir") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "tiny-keccak") (req "^1.4") (default-features #t) (kind 0)))) (hash "11jahlbkbp9alqbss1cp2fxyxvdgiwj8hnpsb2zidj1j7ijbwfr5")))

(define-public crate-keccak-hash-0.2 (crate (name "keccak-hash") (vers "0.2.0") (deps (list (crate-dep (name "primitive-types") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "tempdir") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "tiny-keccak") (req "^1.4") (default-features #t) (kind 0)))) (hash "0r8asz83xbn0qgj5jnvscahn69q5lmgmcmvn9p1xr9lsgdlyxs39")))

(define-public crate-keccak-hash-0.3 (crate (name "keccak-hash") (vers "0.3.0") (deps (list (crate-dep (name "primitive-types") (req "^0.5") (default-features #t) (kind 0)) (crate-dep (name "tempdir") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "tiny-keccak") (req "^1.4") (default-features #t) (kind 0)))) (hash "00q9yak0rr05r81r72f951hyz39sn0kjdznx60klr5xj3p54ydh9")))

(define-public crate-keccak-hash-0.4 (crate (name "keccak-hash") (vers "0.4.0") (deps (list (crate-dep (name "primitive-types") (req "^0.6") (kind 0)) (crate-dep (name "tempdir") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "tiny-keccak") (req "^1.4") (default-features #t) (kind 0)))) (hash "167rqwsyis4vl2mw7infp3k7avrfrgr8n4a6904qc9ibwmpzlqz5") (features (quote (("std") ("default" "std"))))))

(define-public crate-keccak-hash-0.4 (crate (name "keccak-hash") (vers "0.4.1") (deps (list (crate-dep (name "primitive-types") (req "^0.6") (kind 0)) (crate-dep (name "tempdir") (req "^0.3.7") (default-features #t) (kind 2)) (crate-dep (name "tiny-keccak") (req "^1.5.0") (default-features #t) (kind 0)))) (hash "116731dlnmlbngc34qnc2zfvcpd2c04xfk0wqa0w5za2ybc5a3jg") (features (quote (("std") ("default" "std"))))))

(define-public crate-keccak-hash-0.5 (crate (name "keccak-hash") (vers "0.5.0") (deps (list (crate-dep (name "criterion") (req "^0.3.0") (default-features #t) (kind 2)) (crate-dep (name "primitive-types") (req "^0.7") (kind 0)) (crate-dep (name "tempdir") (req "^0.3.7") (default-features #t) (kind 2)) (crate-dep (name "tiny-keccak") (req "^2.0") (features (quote ("keccak"))) (default-features #t) (kind 0)))) (hash "1q7c5cj3fpizjy9sqjda42hk4pal314fpmb5dlfwjp74n2gr8r94") (features (quote (("std") ("default" "std"))))))

(define-public crate-keccak-hash-0.5 (crate (name "keccak-hash") (vers "0.5.1") (deps (list (crate-dep (name "criterion") (req "^0.3.0") (default-features #t) (kind 2)) (crate-dep (name "primitive-types") (req "^0.7") (kind 0)) (crate-dep (name "tempdir") (req "^0.3.7") (default-features #t) (kind 2)) (crate-dep (name "tiny-keccak") (req "^2.0") (features (quote ("keccak"))) (default-features #t) (kind 0)))) (hash "13sc1899lmw8fgw725gb1y6p9yv1xgaaigil4k7ri4yzycgaan0z") (features (quote (("std") ("default" "std"))))))

(define-public crate-keccak-hash-0.6 (crate (name "keccak-hash") (vers "0.6.0") (deps (list (crate-dep (name "criterion") (req "^0.3.0") (default-features #t) (kind 2)) (crate-dep (name "primitive-types") (req "^0.8") (kind 0)) (crate-dep (name "tempfile") (req "^3.1.0") (default-features #t) (kind 2)) (crate-dep (name "tiny-keccak") (req "^2.0") (features (quote ("keccak"))) (default-features #t) (kind 0)))) (hash "0wh0j0vkiw6dzp1397mmkkvvblslvpmi4s174v3abngsyvwa621j") (features (quote (("std") ("default" "std"))))))

(define-public crate-keccak-hash-0.7 (crate (name "keccak-hash") (vers "0.7.0") (deps (list (crate-dep (name "criterion") (req "^0.3.0") (default-features #t) (kind 2)) (crate-dep (name "primitive-types") (req "^0.9") (kind 0)) (crate-dep (name "tempfile") (req "^3.1.0") (default-features #t) (kind 2)) (crate-dep (name "tiny-keccak") (req "^2.0") (features (quote ("keccak"))) (default-features #t) (kind 0)))) (hash "0lfc9ighklgq0farbdkwxvrny58pm3rjnhd3m8hxfvf2k3n8c0xf") (features (quote (("std") ("default" "std"))))))

(define-public crate-keccak-hash-0.8 (crate (name "keccak-hash") (vers "0.8.0") (deps (list (crate-dep (name "criterion") (req "^0.3.0") (default-features #t) (kind 2)) (crate-dep (name "primitive-types") (req "^0.10") (kind 0)) (crate-dep (name "tempfile") (req "^3.1.0") (default-features #t) (kind 2)) (crate-dep (name "tiny-keccak") (req "^2.0") (features (quote ("keccak"))) (default-features #t) (kind 0)))) (hash "0l98snc1pkddbs2inxzdq0gj5x0af34bvxxdxb9j9rvhjb1d8ayf") (features (quote (("std") ("default" "std"))))))

(define-public crate-keccak-hash-0.9 (crate (name "keccak-hash") (vers "0.9.0") (deps (list (crate-dep (name "criterion") (req "^0.3.0") (default-features #t) (kind 2)) (crate-dep (name "primitive-types") (req "^0.11") (kind 0)) (crate-dep (name "tempfile") (req "^3.1.0") (default-features #t) (kind 2)) (crate-dep (name "tiny-keccak") (req "^2.0") (features (quote ("keccak"))) (default-features #t) (kind 0)))) (hash "1qy9irq81m0f72y81l42xdkslz9yz732mxhmjrhngc25ldf5vg42") (features (quote (("std") ("default" "std")))) (rust-version "1.56.1")))

(define-public crate-keccak-hash-0.10 (crate (name "keccak-hash") (vers "0.10.0") (deps (list (crate-dep (name "criterion") (req "^0.4.0") (default-features #t) (kind 2)) (crate-dep (name "primitive-types") (req "^0.12") (kind 0)) (crate-dep (name "tempfile") (req "^3.1.0") (default-features #t) (kind 2)) (crate-dep (name "tiny-keccak") (req "^2.0") (features (quote ("keccak"))) (default-features #t) (kind 0)))) (hash "0g3d9hayhni1n8him1v5dmnd83mpkkk2i1dnxvhjdf9zcrmnwa2b") (features (quote (("std") ("default" "std")))) (rust-version "1.56.1")))

(define-public crate-keccak-hasher-0.1 (crate (name "keccak-hasher") (vers "0.1.0") (deps (list (crate-dep (name "ethereum-types") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "hashdb") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "plain_hasher") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "tiny-keccak") (req "^1.4.2") (default-features #t) (kind 0)))) (hash "0l8mkl2aammnhaskfhlfcxxsxws3y2rbd6s5i4f9x96251086f8j")))

(define-public crate-keccak-hasher-0.1 (crate (name "keccak-hasher") (vers "0.1.1") (deps (list (crate-dep (name "ethereum-types") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "hashdb") (req "^0.2.1") (default-features #t) (kind 0)) (crate-dep (name "plain_hasher") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "tiny-keccak") (req "^1.4.2") (default-features #t) (kind 0)))) (hash "0flqd89k95fapn861vqzvigg17nqx89mv9pwsix3vx0cir65ivjx")))

(define-public crate-keccak-hasher-0.2 (crate (name "keccak-hasher") (vers "0.2.0") (deps (list (crate-dep (name "hash-db") (req "^0.9.0") (default-features #t) (kind 0)) (crate-dep (name "hash256-std-hasher") (req "^0.9.0") (default-features #t) (kind 0)) (crate-dep (name "tiny-keccak") (req "^1.4.2") (default-features #t) (kind 0)))) (hash "0dcqn78jmlmqrr5qfrj3aj4q9w5zm2q1md4ibw2s85bqn1r9pci2")))

(define-public crate-keccak-hasher-0.2 (crate (name "keccak-hasher") (vers "0.2.1") (deps (list (crate-dep (name "hash-db") (req "^0.9.0") (default-features #t) (kind 0)) (crate-dep (name "hash256-std-hasher") (req "^0.9.1") (default-features #t) (kind 0)) (crate-dep (name "tiny-keccak") (req "^1.4.2") (default-features #t) (kind 0)))) (hash "0y5p9w400z02rlfsbasksz9sj3vs3zdyvy91455j1r8hdi370ln7")))

(define-public crate-keccak-hasher-0.11 (crate (name "keccak-hasher") (vers "0.11.0") (deps (list (crate-dep (name "hash-db") (req "^0.11.0") (default-features #t) (kind 0)) (crate-dep (name "hash256-std-hasher") (req "^0.11.0") (default-features #t) (kind 0)) (crate-dep (name "tiny-keccak") (req "^1.4.2") (default-features #t) (kind 0)))) (hash "04dnspmgyyyghxd03fq2sxzb5l92m1bsbf4h7lah8k1z09q3d7fb")))

(define-public crate-keccak-hasher-0.12 (crate (name "keccak-hasher") (vers "0.12.0") (deps (list (crate-dep (name "hash-db") (req "^0.12.0") (default-features #t) (kind 0)) (crate-dep (name "hash256-std-hasher") (req "^0.12.0") (default-features #t) (kind 0)) (crate-dep (name "tiny-keccak") (req "^1.4.2") (default-features #t) (kind 0)))) (hash "0bcsqgpv0ff1cj9v6vvc3h9cwmj704y05wmmd4h544xnq56vfbx0")))

(define-public crate-keccak-hasher-0.12 (crate (name "keccak-hasher") (vers "0.12.2") (deps (list (crate-dep (name "hash-db") (req "^0.12.2") (kind 0)) (crate-dep (name "hash256-std-hasher") (req "^0.12.2") (default-features #t) (kind 0)) (crate-dep (name "tiny-keccak") (req "^1.4.2") (default-features #t) (kind 0)))) (hash "1a8wpjralnrali5dp5hxr6mvcf06i1wjrmlzqa31rb5bn99jarxg") (features (quote (("std" "hash-db/std") ("default" "std"))))))

(define-public crate-keccak-hasher-0.12 (crate (name "keccak-hasher") (vers "0.12.3") (deps (list (crate-dep (name "hash-db") (req "^0.12.3") (kind 0)) (crate-dep (name "hash256-std-hasher") (req "^0.12.3") (default-features #t) (kind 0)) (crate-dep (name "tiny-keccak") (req "^1.4.2") (default-features #t) (kind 0)))) (hash "0f8r4rvwkxm53g38xd15d8pypx65bizcqikdwjn57822hqr9k2vh") (features (quote (("std" "hash-db/std") ("default" "std")))) (yanked #t)))

(define-public crate-keccak-hasher-0.12 (crate (name "keccak-hasher") (vers "0.12.4") (deps (list (crate-dep (name "hash-db") (req "^0.12.4") (kind 0)) (crate-dep (name "hash256-std-hasher") (req "^0.12.4") (default-features #t) (kind 0)) (crate-dep (name "tiny-keccak") (req "^1.4.2") (default-features #t) (kind 0)))) (hash "0zgb1gnrwf8hmpnfld5z1a60w6hmhgszlxa2qf9hasbrgmrnr4vc") (features (quote (("std" "hash-db/std") ("default" "std"))))))

(define-public crate-keccak-hasher-0.14 (crate (name "keccak-hasher") (vers "0.14.0") (deps (list (crate-dep (name "hash-db") (req "^0.14.0") (kind 0)) (crate-dep (name "hash256-std-hasher") (req "^0.14.0") (default-features #t) (kind 0)) (crate-dep (name "tiny-keccak") (req "^1.4.2") (default-features #t) (kind 0)))) (hash "1f8wdc8gj6ay5nw3j98sbs7cwjln8y1g64jzjwgw95v7w4pifrrp") (features (quote (("std" "hash-db/std") ("default" "std"))))))

(define-public crate-keccak-hasher-0.15 (crate (name "keccak-hasher") (vers "0.15.0") (deps (list (crate-dep (name "hash-db") (req "^0.15.0") (kind 0)) (crate-dep (name "hash256-std-hasher") (req "^0.15.0") (default-features #t) (kind 0)) (crate-dep (name "tiny-keccak") (req "^1.4.2") (default-features #t) (kind 0)))) (hash "1vxb782zqs293npsxwkm6pn1pgd8fapa3i4g3w28ksbwzmj83w9v") (features (quote (("std" "hash-db/std") ("default" "std"))))))

(define-public crate-keccak-hasher-0.15 (crate (name "keccak-hasher") (vers "0.15.1") (deps (list (crate-dep (name "hash-db") (req "^0.15.1") (kind 0)) (crate-dep (name "hash256-std-hasher") (req "^0.15.1") (default-features #t) (kind 0)) (crate-dep (name "tiny-keccak") (req "^1.4.2") (default-features #t) (kind 0)))) (hash "03b26fnjbx9yd1fi3r7nv9mr65vbspnax31634291nyqz91avwpm") (features (quote (("std" "hash-db/std") ("default" "std"))))))

(define-public crate-keccak-hasher-0.15 (crate (name "keccak-hasher") (vers "0.15.2") (deps (list (crate-dep (name "hash-db") (req "^0.15.2") (kind 0)) (crate-dep (name "hash256-std-hasher") (req "^0.15.2") (default-features #t) (kind 0)) (crate-dep (name "tiny-keccak") (req "^1.4.2") (default-features #t) (kind 0)))) (hash "1y1cyiczsk4rvbpa5mmr9vck71wj9jdsx48mj879ldd1xryj0s1l") (features (quote (("std" "hash-db/std") ("default" "std"))))))

(define-public crate-keccak-hasher-0.15 (crate (name "keccak-hasher") (vers "0.15.3") (deps (list (crate-dep (name "hash-db") (req "^0.15.2") (kind 0)) (crate-dep (name "hash256-std-hasher") (req "^0.15.2") (default-features #t) (kind 0)) (crate-dep (name "tiny-keccak") (req "^2.0.2") (features (quote ("keccak"))) (default-features #t) (kind 0)))) (hash "0pphpq6nzqmzaqifdk2qsni89jpalp028mzwfi1ry0qajjlxn6ki") (features (quote (("std" "hash-db/std") ("default" "std"))))))

(define-public crate-keccak-hasher-0.16 (crate (name "keccak-hasher") (vers "0.16.0") (deps (list (crate-dep (name "hash-db") (req "^0.16.0") (kind 0)) (crate-dep (name "hash256-std-hasher") (req "^0.15.2") (default-features #t) (kind 0)) (crate-dep (name "tiny-keccak") (req "^2.0.2") (features (quote ("keccak"))) (default-features #t) (kind 0)))) (hash "0j10j03qajfjh28zk0mlajlff8iz5mckz7a1hrm2d8lwhm9ldshr") (features (quote (("std" "hash-db/std") ("default" "std"))))))

(define-public crate-keccak-p-0.1 (crate (name "keccak-p") (vers "0.1.0") (deps (list (crate-dep (name "criterion") (req "^0.3.5") (features (quote ("html_reports"))) (default-features #t) (kind 2)) (crate-dep (name "keccak") (req "^0.1.0") (default-features #t) (kind 2)) (crate-dep (name "tiny-keccak") (req "^2.0.2") (features (quote ("keccak" "k12"))) (default-features #t) (kind 2)))) (hash "1k43zxqf4fwpwnjp35hyixf72izwm3mydb4359qk3nlm1yc9bmdi") (yanked #t)))

(define-public crate-keccak-p-0.1 (crate (name "keccak-p") (vers "0.1.1") (deps (list (crate-dep (name "criterion") (req "^0.4.0") (features (quote ("html_reports"))) (default-features #t) (kind 2)) (crate-dep (name "keccak") (req "^0.1.2") (default-features #t) (kind 2)) (crate-dep (name "tiny-keccak") (req "^2.0.2") (features (quote ("keccak" "k12"))) (default-features #t) (kind 2)))) (hash "1np0j4wz4y6k32f3m749l8cvnrfi0qrmbl1594sv15zwnpszj767") (yanked #t)))

(define-public crate-keccak-rust-1 (crate (name "keccak-rust") (vers "1.0.0") (deps (list (crate-dep (name "criterion") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "crunchy") (req "^0.2") (default-features #t) (kind 0)))) (hash "0rilxyxrph10rd9swz3zas0857hai7aj2fgw7pcn1p8mjh9bc6xk") (yanked #t)))

(define-public crate-keccak-rust-1 (crate (name "keccak-rust") (vers "1.0.1") (deps (list (crate-dep (name "criterion") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "crunchy") (req "^0.2") (default-features #t) (kind 0)))) (hash "07lnfc7yyakpbywj5k2bx03qg8l43b9i8ld3k02ybvz3wz1lc0y4")))

(define-public crate-keccak-rust-1 (crate (name "keccak-rust") (vers "1.0.2") (deps (list (crate-dep (name "criterion") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "crunchy") (req "^0.2") (default-features #t) (kind 0)))) (hash "10qszp3z27iid6dr0llcsw1lf2g2ql2y42ky31b04a8v6vr7nyaa")))

(define-public crate-keccak256-cli-0.1 (crate (name "keccak256-cli") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^3.1.8") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "sha3") (req "^0.10.1") (default-features #t) (kind 0)))) (hash "1afkg68dmbdxg8w6abdkcxln835d91lx4s3scw5ka8zfm2fj3b4p")))

(define-public crate-keccak256-cli-0.1 (crate (name "keccak256-cli") (vers "0.1.1") (deps (list (crate-dep (name "clap") (req "^3.1.8") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "sha3") (req "^0.10.1") (default-features #t) (kind 0)))) (hash "11v3898firjvz8hypm58m7dqnjbj45va0mhhgf5klkh5d7wn2rsy") (yanked #t)))

(define-public crate-keccak256-cli-0.1 (crate (name "keccak256-cli") (vers "0.1.2") (deps (list (crate-dep (name "clap") (req "^3.1.8") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "sha3") (req "^0.10.1") (default-features #t) (kind 0)))) (hash "1dy68zlsg5kw9rgx2yqykh134yb3jxk0a9adrmhfkk9vjml4c566")))

(define-public crate-keccak256-cli-0.2 (crate (name "keccak256-cli") (vers "0.2.0") (deps (list (crate-dep (name "clap") (req "^3.1.8") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "sha3") (req "^0.10.1") (default-features #t) (kind 0)))) (hash "034b6rpc14bcizr8ah4v9jlfv4v5n1gbjgl68wvbiafy3larpjdv")))

(define-public crate-keccak256-cli-0.2 (crate (name "keccak256-cli") (vers "0.2.1") (deps (list (crate-dep (name "clap") (req "^3.1.8") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "sha3") (req "^0.10.1") (default-features #t) (kind 0)))) (hash "12wz8xjp7ls83djyn5mcc9qpk12rlaq7zn82hkypafj6542bs77w")))

(define-public crate-keccak_prime-0.1 (crate (name "keccak_prime") (vers "0.1.0") (deps (list (crate-dep (name "aes-gcm-siv") (req "^0.9.0") (default-features #t) (kind 0)) (crate-dep (name "crunchy") (req "^0.2.2") (default-features #t) (kind 0)) (crate-dep (name "hex") (req "^0.4.2") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.4") (default-features #t) (kind 0)) (crate-dep (name "num-bigint") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2") (default-features #t) (kind 0)))) (hash "0msbh914wsi6zrvr98gi55rnraydyydcbw715hamgp0vmik4k6a5") (features (quote (("tuple_hash" "cshake") ("sp800" "cshake" "kmac" "tuple_hash") ("shake") ("sha3") ("parallel_hash" "cshake") ("kmac" "cshake") ("keccak") ("k12") ("fips202" "keccak" "shake" "sha3") ("default" "fips202") ("cshake"))))))

(define-public crate-keccakf-0.1 (crate (name "keccakf") (vers "0.1.0") (deps (list (crate-dep (name "crunchy") (req "^0.2.2") (default-features #t) (kind 0)))) (hash "1fk6dr143b8in7nxvxw88in1wz3v85hjfm7b67jckdm2dz3c657y") (yanked #t)))

(define-public crate-keccakf-0.1 (crate (name "keccakf") (vers "0.1.1") (deps (list (crate-dep (name "crunchy") (req "^0.2.2") (default-features #t) (kind 0)))) (hash "1jyg3hqn5ann3p8hp27aj1j8i4jbrzhnm7zcmnawizhxr7k34mr3") (yanked #t)))

(define-public crate-keccakf-0.1 (crate (name "keccakf") (vers "0.1.2") (deps (list (crate-dep (name "crunchy") (req "^0.2.2") (default-features #t) (kind 0)))) (hash "0rsgxp4p3mgq02yg7dsam7x0aswfqqf812hvsqrj8hhk49spf7yc") (yanked #t)))

(define-public crate-keccakf-0.1 (crate (name "keccakf") (vers "0.1.3") (deps (list (crate-dep (name "crunchy") (req "^0.2.2") (default-features #t) (kind 0)))) (hash "1qjdiamd7hlzw46vx9nw7qj8d7xrhibx82ydkpqpncn9lj0xwjhx")))

(define-public crate-keccakf-0.2 (crate (name "keccakf") (vers "0.2.0") (deps (list (crate-dep (name "crunchy") (req "^0.2.2") (default-features #t) (kind 0)))) (hash "1gmw9ykiky77lap2wa2dgylbp5bh0v5pbgg3y8hhc3c03gwbmap3") (yanked #t)))

(define-public crate-keccakf-0.2 (crate (name "keccakf") (vers "0.2.1") (deps (list (crate-dep (name "crunchy") (req "^0.2.2") (default-features #t) (kind 0)))) (hash "1swvs7pas3ag5d5bkcfgzdrfmcr2hl67spn5sdvixjbm0njh2aby")))

(define-public crate-keccakrs-0.1 (crate (name "keccakrs") (vers "0.1.0") (hash "104gz5ih1i04wsk3cw5rymiszw9jhq837whcr6ny6dfzj3wpx05i")))

(define-public crate-keccakrs-0.1 (crate (name "keccakrs") (vers "0.1.1") (hash "1kd33kazzxmwvnz4s5hm429nijjcnjsmwdndd5zgbvyydvxqj7ln")))

(define-public crate-keccakrs-0.1 (crate (name "keccakrs") (vers "0.1.2") (hash "1mmgv149kf3cibdsjil9nnkjqfyxi3kqy6qpqxhzk5790819bd67")))

(define-public crate-keccakrs-0.1 (crate (name "keccakrs") (vers "0.1.3") (hash "08arn5lq79sgz2j25kzrz7q5rxvarv6rmsnhnplv3rhz67k7qjmk")))

(define-public crate-keccakrs-0.1 (crate (name "keccakrs") (vers "0.1.4") (hash "0ciljjp3h5hz4clw5dggf4cka0n1ysjb0rm965h9z3lg21p990gg")))

(define-public crate-keccakrs-0.1 (crate (name "keccakrs") (vers "0.1.5") (hash "19dqbqyg88ppyp06wah5adk3x4zc5xczzdml5h2xy6fmkxyqsbjh")))

(define-public crate-keccakrs-0.1 (crate (name "keccakrs") (vers "0.1.6") (deps (list (crate-dep (name "crunchy") (req "^0.1.6") (default-features #t) (kind 0)))) (hash "18cy0nq6zsq44h7y0w9x9g9jw0yxj0v2jj4acirqp8630v1fbfg0")))

(define-public crate-keccakrs-wasm-0.1 (crate (name "keccakrs-wasm") (vers "0.1.0") (deps (list (crate-dep (name "keccakrs") (req "^0.1.4") (default-features #t) (kind 0)))) (hash "1ysd9r1p5b3i8bwdfc4bq9ckzv2hxnd21sf1ccgl8cc0hb9d484z")))

