(define-module (crates-io ke ye) #:use-module (crates-io))

(define-public crate-keyed-0.1 (crate (name "keyed") (vers "0.1.0") (hash "095v7j4far3gykf3nx6p5jg3k4q9nsydm5qarvh0qzrcj9mhb5z8") (yanked #t)))

(define-public crate-keyed-0.1 (crate (name "keyed") (vers "0.1.1") (hash "176mdcwmxpwnv76bwd6czajzca30p3fyw1rjrpk0rwnh2lb1nbm0") (yanked #t)))

(define-public crate-keyed-0.1 (crate (name "keyed") (vers "0.1.2") (hash "1lbqmpdymr8zpxhgdrbh0f4sxkm5cq3hzwiv68m4iizmnwbmz2m8") (yanked #t)))

(define-public crate-keyed-0.1 (crate (name "keyed") (vers "0.1.3") (hash "0k85s81qnv4i38y6f1pfpy2r0mdxa9k0hplsaa6955nj8cwdg4af")))

(define-public crate-keyed-set-0.1 (crate (name "keyed-set") (vers "0.1.0") (deps (list (crate-dep (name "hashbrown") (req "^0.12") (features (quote ("raw"))) (default-features #t) (kind 0)))) (hash "13m8pcl2zzvmvbdc6j41mhdszvvx4l4if1lm94abzzbrc3ybzqfs")))

(define-public crate-keyed-set-0.2 (crate (name "keyed-set") (vers "0.2.0") (deps (list (crate-dep (name "hashbrown") (req "^0.12") (features (quote ("raw"))) (default-features #t) (kind 0)))) (hash "1r0nkx1rlclxzgk6r79zmni9c3igznlr912nm9bxksbzn4zmxybh")))

(define-public crate-keyed-set-0.3 (crate (name "keyed-set") (vers "0.3.0") (deps (list (crate-dep (name "hashbrown") (req "^0.12") (features (quote ("raw"))) (default-features #t) (kind 0)))) (hash "0v6a3hk7vmyj4mhfwlx8fpk2dn2k4sn3cdalsykl72pfw274wn7z")))

(define-public crate-keyed-set-0.4 (crate (name "keyed-set") (vers "0.4.0") (deps (list (crate-dep (name "hashbrown") (req "^0.12") (features (quote ("raw"))) (default-features #t) (kind 0)))) (hash "0jy2b9i52fm5kkqy7x8bmprxpnav0gj3wvrd984yrkarqxl6xp5r")))

(define-public crate-keyed-set-0.4 (crate (name "keyed-set") (vers "0.4.1") (deps (list (crate-dep (name "hashbrown") (req "^0.12") (features (quote ("raw"))) (default-features #t) (kind 0)))) (hash "0s5q3fa0pdnyvmv1z0q9x9w5lk3hiypq1vl8xsx2vkinn4j9wg45")))

(define-public crate-keyed-set-0.4 (crate (name "keyed-set") (vers "0.4.2") (deps (list (crate-dep (name "hashbrown") (req "^0.13.1") (features (quote ("raw"))) (default-features #t) (kind 0)))) (hash "0nss74l6knyklgh5ijpfvw3iha75jvikm4qckbz01vvg5k5glc1j")))

(define-public crate-keyed-set-0.4 (crate (name "keyed-set") (vers "0.4.3") (deps (list (crate-dep (name "hashbrown") (req "^0.13.1") (features (quote ("raw"))) (default-features #t) (kind 0)))) (hash "0pbdn3vs8g0035rm4yv3n6kimw5agch30gps4zyyh573gfycmg3n")))

(define-public crate-keyed-set-0.4 (crate (name "keyed-set") (vers "0.4.4") (deps (list (crate-dep (name "hashbrown") (req "^0.13.1") (features (quote ("raw"))) (default-features #t) (kind 0)))) (hash "076pk79z8kyl2mzsmsr6nnxxh9mp8d85yx2lh09hc43cmfcyfjsy")))

(define-public crate-keyed-set-0.4 (crate (name "keyed-set") (vers "0.4.5") (deps (list (crate-dep (name "hashbrown") (req "^0.13.1") (features (quote ("raw"))) (default-features #t) (kind 0)))) (hash "0nk1cs1l7k1r9ikk4jjc9mnhq9q9jwxcz254kj083470hc1137mp")))

(define-public crate-keyed-set-1 (crate (name "keyed-set") (vers "1.0.0") (deps (list (crate-dep (name "hashbrown") (req "^0.14.3") (features (quote ("raw"))) (default-features #t) (kind 0)))) (hash "0d5f0hwxxjavsr73p0s65sdzg21025m90qqdajhm6yf15nfw6gha")))

(define-public crate-keyed_priority_queue-0.1 (crate (name "keyed_priority_queue") (vers "0.1.0") (hash "1k4bfpr7rqfl2crqygcd92fhyx54mij3c8f0fhdn7pig3wqjg078") (yanked #t)))

(define-public crate-keyed_priority_queue-0.1 (crate (name "keyed_priority_queue") (vers "0.1.1") (hash "1cym5y792sa90jb8gl2prdhay8bk8y4bxbgsj4hfrkbkxpsxg603") (yanked #t)))

(define-public crate-keyed_priority_queue-0.1 (crate (name "keyed_priority_queue") (vers "0.1.2") (hash "03wmypkryhl9lxqxbs18sihgn0b0x336hj501p3if8dj0zgcqi1b") (yanked #t)))

(define-public crate-keyed_priority_queue-0.1 (crate (name "keyed_priority_queue") (vers "0.1.3") (hash "0ai2k07mnrymqjxyrzaqm81jg56wgilvjypw72qxi7c4c5by35za") (yanked #t)))

(define-public crate-keyed_priority_queue-0.2 (crate (name "keyed_priority_queue") (vers "0.2.0") (deps (list (crate-dep (name "indexmap") (req "^1.3") (default-features #t) (kind 0)))) (hash "1l79zjb3i4bb140drypwxfbii2xa1hnfvs3s85cvc46cbdc1chmz") (yanked #t)))

(define-public crate-keyed_priority_queue-0.2 (crate (name "keyed_priority_queue") (vers "0.2.1") (deps (list (crate-dep (name "indexmap") (req "^1.3") (default-features #t) (kind 0)))) (hash "078jfnib46kv3kznwjrdkavpd8j77bqzlrgyjclpzksjisfxdvjb") (yanked #t)))

(define-public crate-keyed_priority_queue-0.3 (crate (name "keyed_priority_queue") (vers "0.3.0") (deps (list (crate-dep (name "indexmap") (req "^1.3") (default-features #t) (kind 0)))) (hash "0waigs31xl02xbp005hwa3hb9knfhig54sxjg532dfi2qp8kp68k") (yanked #t)))

(define-public crate-keyed_priority_queue-0.3 (crate (name "keyed_priority_queue") (vers "0.3.1") (deps (list (crate-dep (name "indexmap") (req "^1.3") (default-features #t) (kind 0)))) (hash "05c4mzvlc28vpqdxbhq6q6ym2j3ds8n1kwjpl1s5jwpv6b4visbr") (yanked #t)))

(define-public crate-keyed_priority_queue-0.3 (crate (name "keyed_priority_queue") (vers "0.3.2") (deps (list (crate-dep (name "indexmap") (req "^1.3") (default-features #t) (kind 0)))) (hash "1m78lgw7rhq7z9w941kr6ms05a1x980799m52m9nagb8ikq9v9d7")))

(define-public crate-keyed_priority_queue-0.4 (crate (name "keyed_priority_queue") (vers "0.4.0") (deps (list (crate-dep (name "indexmap") (req "^1.3") (default-features #t) (kind 0)))) (hash "06l6m31hhnzl91vbmwdgjp847bb3742bgs4i8dn3rdf4xj5m53s6") (rust-version "1.56")))

(define-public crate-keyed_priority_queue-0.4 (crate (name "keyed_priority_queue") (vers "0.4.1") (deps (list (crate-dep (name "indexmap") (req "^1.3") (default-features #t) (kind 0)))) (hash "1s06k84rav4pwcmnl6qhqn9z6sdnxkfz7k4xagy83z36gd0bcqrd") (rust-version "1.56")))

(define-public crate-keyed_priority_queue-0.4 (crate (name "keyed_priority_queue") (vers "0.4.2") (deps (list (crate-dep (name "indexmap") (req "^2.1.0") (default-features #t) (kind 0)))) (hash "03b452v0b56xa8msf1hvn087qcmayipz4wq1kmgswi1fmcyqkrsf") (rust-version "1.63")))

(define-public crate-keyed_vec-0.1 (crate (name "keyed_vec") (vers "0.1.0") (deps (list (crate-dep (name "delegate") (req "^0.10.0") (default-features #t) (kind 0)))) (hash "0a2205dih9ai75h3gblir027zrl7130bdyci7xv1b9m8af9jrb55")))

(define-public crate-keyedes-0.1 (crate (name "keyedes") (vers "0.1.0") (deps (list (crate-dep (name "erased-serde") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "once_cell") (req "^1.0.0") (default-features #t) (kind 2)) (crate-dep (name "serde") (req "^1.0.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.0") (features (quote ("derive"))) (default-features #t) (kind 2)) (crate-dep (name "serde-value") (req "^0.7.0") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.0") (default-features #t) (kind 2)))) (hash "1fxspwqn1iy1y4l97ad8y0p95glasmlsv6vs8m8qvyr9awqvhhkd")))

(define-public crate-keyezinput-0.1 (crate (name "keyezinput") (vers "0.1.2") (hash "1fwfy0izgydkwy40l4689qzyzm2f7f4761mnlvynqfn1139w8b05")))

(define-public crate-keyezinput-0.1 (crate (name "keyezinput") (vers "0.1.21") (hash "0zqdc9fgfn6mfnal4hczzf2zajmzmqscjhrsmw6rcp9vqadxmfk4") (yanked #t)))

(define-public crate-keyezinput-0.1 (crate (name "keyezinput") (vers "0.1.3") (hash "13w4vfjisvj2nyxbhvb43gxymyzsflhxsjf0c8snbrbwbq528y4a")))

