(define-module (crates-io ke mp) #:use-module (crates-io))

(define-public crate-kempt-0.1 (crate (name "kempt") (vers "0.1.0") (deps (list (crate-dep (name "serde") (req "^1.0.163") (optional #t) (kind 0)) (crate-dep (name "serde_test") (req "^1.0.163") (default-features #t) (kind 2)))) (hash "10fd80mqm6ll53n5gy1xpc8i07wswmjvv4jl47qi2pm7pa5dmxxr") (rust-version "1.65.0")))

(define-public crate-kempt-0.2 (crate (name "kempt") (vers "0.2.0") (deps (list (crate-dep (name "serde") (req "^1.0.163") (optional #t) (kind 0)) (crate-dep (name "serde_test") (req "^1.0.163") (default-features #t) (kind 2)))) (hash "1vz5jd2np1l94z0lzpmi8l6yb6a5iqsdhkav0vxgwpd348gidsbq") (rust-version "1.65.0")))

(define-public crate-kempt-0.2 (crate (name "kempt") (vers "0.2.1") (deps (list (crate-dep (name "serde") (req "^1.0.163") (optional #t) (kind 0)) (crate-dep (name "serde_test") (req "^1.0.163") (default-features #t) (kind 2)))) (hash "04jljrjyjm786c2jhbawpxc9qsa67hfjbbl6lahbigzq5nqpmyg8") (rust-version "1.65.0")))

(define-public crate-kempt-0.2 (crate (name "kempt") (vers "0.2.2") (deps (list (crate-dep (name "serde") (req "^1.0.163") (optional #t) (kind 0)) (crate-dep (name "serde_test") (req "^1.0.163") (default-features #t) (kind 2)))) (hash "003j0vg8z6a5rsp5a31mj13xrpk6hw09d66jxb5wf7myggli5a8k") (rust-version "1.65.0")))

(define-public crate-kempt-0.2 (crate (name "kempt") (vers "0.2.3") (deps (list (crate-dep (name "serde") (req "^1.0.163") (optional #t) (kind 0)) (crate-dep (name "serde_test") (req "^1.0.163") (default-features #t) (kind 2)))) (hash "0w9h7ma86sw793yq08vifxnmlmb6yyd0dazv3v5nmbifnnyscdsa") (rust-version "1.65.0")))

(define-public crate-kempt-0.2 (crate (name "kempt") (vers "0.2.4") (deps (list (crate-dep (name "serde") (req "^1.0.163") (optional #t) (kind 0)) (crate-dep (name "serde_test") (req "^1.0.163") (default-features #t) (kind 2)))) (hash "06qz9fpg1mr09ja9cjqpaiqs10xivk1lx5ni98z3r4xlrwrl9grm") (rust-version "1.65.0")))

