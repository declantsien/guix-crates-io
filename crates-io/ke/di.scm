(define-module (crates-io ke di) #:use-module (crates-io))

(define-public crate-kedi-0.1 (crate (name "kedi") (vers "0.1.0") (hash "1jjzrfsjz6igscq14bg90h3fkf7hv4mmw7fmwrgg98bgmbjpqb3s")))

(define-public crate-kedit-0.1 (crate (name "kedit") (vers "0.1.0") (deps (list (crate-dep (name "structopt") (req "^0.3") (kind 0)) (crate-dep (name "termion") (req "^1.5.5") (default-features #t) (kind 0)))) (hash "1hb43cxqca5n2g0m3wflw2nlw3bsnl1z6cvwrwhrywhzg6l27ac7")))

