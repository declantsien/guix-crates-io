(define-module (crates-io ke it) #:use-module (crates-io))

(define-public crate-keithley-2230-series-0.1 (crate (name "keithley-2230-series") (vers "0.1.0") (deps (list (crate-dep (name "thiserror") (req "^1.0.50") (default-features #t) (kind 0)) (crate-dep (name "visa-api") (req "^0.1.1") (default-features #t) (kind 0)))) (hash "1cw6d6ym2zaxjdrybpcnjrwn868v3hwa6kxdls4gyxvw8s1rknip")))

(define-public crate-keithley-2230-series-0.1 (crate (name "keithley-2230-series") (vers "0.1.1") (deps (list (crate-dep (name "thiserror") (req "^1.0.50") (default-features #t) (kind 0)) (crate-dep (name "visa-api") (req "^0.1.1") (default-features #t) (kind 0)))) (hash "1dmsr408jq603ycmdypz6mrgbqk0mrzvsrq0szwfs6jlxr7x3qia")))

(define-public crate-keithley-2230-series-0.1 (crate (name "keithley-2230-series") (vers "0.1.2") (deps (list (crate-dep (name "thiserror") (req "^1.0.50") (default-features #t) (kind 0)) (crate-dep (name "visa-api") (req "^0.1.1") (default-features #t) (kind 0)))) (hash "1r1dlpchphvljq9vd9a9mrjb659hs7axm2xcppvlbpa0rfd6z59h")))

(define-public crate-keithley-2230-series-0.1 (crate (name "keithley-2230-series") (vers "0.1.3") (deps (list (crate-dep (name "thiserror") (req "^1.0.50") (default-features #t) (kind 0)) (crate-dep (name "visa-api") (req "^0.1.1") (default-features #t) (kind 0)))) (hash "0cd14c2akwancq108fg0n4h657gi5y2j9vd34yfrw8jd7m90nwar")))

(define-public crate-keithley-2230-series-0.1 (crate (name "keithley-2230-series") (vers "0.1.4") (deps (list (crate-dep (name "thiserror") (req "^1.0.50") (default-features #t) (kind 0)) (crate-dep (name "visa-api") (req "^0.1.1") (default-features #t) (kind 0)))) (hash "0j4kn4a2azbd7awiigf3smb0fpr9wyjk2g49bx4has1g84krrk9z")))

(define-public crate-keithley-2230-series-0.1 (crate (name "keithley-2230-series") (vers "0.1.5") (deps (list (crate-dep (name "thiserror") (req "^1.0.50") (default-features #t) (kind 0)) (crate-dep (name "visa-api") (req "^0.1.1") (default-features #t) (kind 0)))) (hash "1xjv4vv1bgpvc7mnf54zrmhldp83yvfj8dmrz2maiyixwh6cka9f")))

(define-public crate-keithley-2230-series-0.2 (crate (name "keithley-2230-series") (vers "0.2.0") (deps (list (crate-dep (name "strum") (req "^0.25.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.50") (default-features #t) (kind 0)) (crate-dep (name "visa-api") (req "^0.2.1") (default-features #t) (kind 0)))) (hash "0zk9fh2zngb8c05425kjln0ln6zzy37m9lfs7rrv6irpr7adiks6")))

(define-public crate-keithley-2230-series-0.2 (crate (name "keithley-2230-series") (vers "0.2.1") (deps (list (crate-dep (name "strum") (req "^0.25.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.50") (default-features #t) (kind 0)) (crate-dep (name "visa-api") (req "^0.2.2") (default-features #t) (kind 0)))) (hash "1lil54pvg0l2b940byx8k7qbvzjvqmykmk4v2f36q1w8w76wd9ic")))

(define-public crate-keithley-2230-series-0.2 (crate (name "keithley-2230-series") (vers "0.2.2") (deps (list (crate-dep (name "strum") (req "^0.25.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.50") (default-features #t) (kind 0)) (crate-dep (name "visa-api") (req "^0.2.2") (default-features #t) (kind 0)))) (hash "1xbrjrck1d87ayb4jhx6lsvlgiiysla4jkkcw2swassvir4cdb1v")))

(define-public crate-keithley-2230-series-0.2 (crate (name "keithley-2230-series") (vers "0.2.3") (deps (list (crate-dep (name "strum") (req "^0.25.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.50") (default-features #t) (kind 0)) (crate-dep (name "visa-api") (req "^0.2.2") (default-features #t) (kind 0)))) (hash "0yncgj0227f3sfxlrgxrggs2a4z3sr6rk1fwdfv40wk3jfm38kd7")))

(define-public crate-keithley-2230-series-0.2 (crate (name "keithley-2230-series") (vers "0.2.4") (deps (list (crate-dep (name "strum") (req "^0.25.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.50") (default-features #t) (kind 0)) (crate-dep (name "visa-api") (req "^0.2.2") (default-features #t) (kind 0)))) (hash "1bygxn0fzx6wm8p4pdlmk86lcqgh7w3x6sd04cax535a6336xizb")))

