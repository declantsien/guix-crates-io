(define-module (crates-io ke rr) #:use-module (crates-io))

(define-public crate-kerr-0.1 (crate (name "kerr") (vers "0.1.0") (hash "0gfk774jjiagglc377p1hpv8yn2f4saw6pkxksklhniay47wrsfy")))

(define-public crate-kerr-0.1 (crate (name "kerr") (vers "0.1.1") (hash "0lpkqzb9zlx0h8cjj29ihvmm7w3gjhfgi8asd9jwwlpcbdzqx1s2")))

(define-public crate-kerr-0.1 (crate (name "kerr") (vers "0.1.2") (hash "1v35j38dnf5dq75mrhh8aimb6aw4dx3qshffafzyx4jgvw86523j")))

(define-public crate-kerr-0.1 (crate (name "kerr") (vers "0.1.3") (hash "0ynnqa5kq7m1c5rla0wr8b9qfw61hax9lx38fbs2hafqnc025wkg")))

(define-public crate-kerr-0.1 (crate (name "kerr") (vers "0.1.4") (hash "0jg3l1f2z7z9f4c48z1wqikdg94arz097d3ybz6lw0krcmbifkam")))

(define-public crate-kerrex-gdnative-sys-0.1 (crate (name "kerrex-gdnative-sys") (vers "0.1.2") (hash "029bqxiajs2x02gm9fbbzl27zf7pxhd9a35y3xpc6lfjm5snq5jz")))

(define-public crate-kerrex-gdnative-sys-0.1 (crate (name "kerrex-gdnative-sys") (vers "0.1.3") (hash "1d442xsl188fip52gnqfh3q99gr8ci1mq4azs1kywn5cvbafmkn3")))

(define-public crate-kerror-0.1 (crate (name "kerror") (vers "0.1.0") (hash "1q8r3kxbg5qvk3hhr56y9xql877lv5ksb4nc7jijwnij2cz446iv")))

