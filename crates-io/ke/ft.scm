(define-module (crates-io ke ft) #:use-module (crates-io))

(define-public crate-kefta-0.0.1 (crate (name "kefta") (vers "0.0.1") (deps (list (crate-dep (name "kefta_core") (req "^0.0.1") (default-features #t) (kind 0)) (crate-dep (name "kefta_macro") (req "^0.0.1") (default-features #t) (kind 0)))) (hash "05ywd97hxn7nz5lmzqin51bg8xgssc9j994dikz6lwi1ybnalpq5") (features (quote (("util" "kefta_core/syn") ("syn" "kefta_core/util") ("literal" "kefta_core/literal") ("default" "literal"))))))

(define-public crate-kefta_core-0.0.1 (crate (name "kefta_core") (vers "0.0.1") (deps (list (crate-dep (name "litrs") (req "^0.2.3") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1.0.43") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.98") (features (quote ("full"))) (optional #t) (default-features #t) (kind 0)))) (hash "02z2m3q21f4jqsnksw1rl5qh9fvy4km7dpwf5mrs2h1j6wc01525") (features (quote (("util") ("literal" "litrs") ("default" "literal")))) (v 2) (features2 (quote (("syn" "dep:syn"))))))

(define-public crate-kefta_macro-0.0.1 (crate (name "kefta_macro") (vers "0.0.1") (deps (list (crate-dep (name "kefta_core") (req "^0.0.1") (features (quote ("syn"))) (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1.0.42") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.20") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.98") (features (quote ("full" "extra-traits"))) (default-features #t) (kind 0)))) (hash "0iwf3swjfkyzxyap8dfgydll9cpf141i7846m3lxcjwgnljcy0kl")))

