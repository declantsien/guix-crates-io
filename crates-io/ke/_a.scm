(define-module (crates-io ke _a) #:use-module (crates-io))

(define-public crate-ke_auto_profile_switcher-0.1 (crate (name "ke_auto_profile_switcher") (vers "0.1.2") (deps (list (crate-dep (name "clap") (req "^3.1.18") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "usb_enumeration") (req "^0.1.2") (default-features #t) (kind 0)))) (hash "1b0jk22hh3bxk0bbhadqngr1gwqrcx76jgh7gdy10l6bj5d0pzyk")))

(define-public crate-ke_auto_profile_switcher-0.1 (crate (name "ke_auto_profile_switcher") (vers "0.1.3") (deps (list (crate-dep (name "clap") (req "^3.1.18") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "usb_enumeration") (req "^0.1.2") (default-features #t) (kind 0)))) (hash "1rgnrvxd0qa58wm58ciiy3ygmv4sn015nkqh8kpngjwwmbk2i6jc")))

