(define-module (crates-io ke nd) #:use-module (crates-io))

(define-public crate-kendalls-0.1 (crate (name "kendalls") (vers "0.1.0") (deps (list (crate-dep (name "num-traits") (req "^0.2.5") (default-features #t) (kind 0)))) (hash "0l3j4bpxp0c0q5xk5m3kd9d0b47mwa3s61wriwbx3hclp4qwan7i")))

(define-public crate-kendalls-0.1 (crate (name "kendalls") (vers "0.1.1") (deps (list (crate-dep (name "num-traits") (req "^0.2.5") (default-features #t) (kind 0)))) (hash "0svw8xn8w3wn751kv8q0fmakqg9npwlscyxdv8b0wbf123gb05gn")))

(define-public crate-kendalls-0.1 (crate (name "kendalls") (vers "0.1.2") (hash "0y71n46rqqavmlkzl2vajs5rcixlkxn6fap3f3mm3ffj4k6s1jp1")))

(define-public crate-kendalls-0.1 (crate (name "kendalls") (vers "0.1.3") (deps (list (crate-dep (name "approx") (req "^0.4") (default-features #t) (kind 2)))) (hash "1z4gs40c901ms40q9j1xpnjbsygqqa3ci2w8wfc8fx4ycx21l096")))

(define-public crate-kendalls-0.1 (crate (name "kendalls") (vers "0.1.4") (deps (list (crate-dep (name "approx") (req "^0.4") (default-features #t) (kind 2)))) (hash "0wirl9xj1nsrfjspqjsa55ym65jcii9rwr6q4kv0qz6w63gdpx82")))

(define-public crate-kendalls-0.1 (crate (name "kendalls") (vers "0.1.5") (deps (list (crate-dep (name "approx") (req "^0.4") (default-features #t) (kind 2)))) (hash "1mmq663n3xv03f7xykkvzypz8cdq99z1w1grjghkgrbck9krqv23")))

(define-public crate-kendalls-0.2 (crate (name "kendalls") (vers "0.2.0") (deps (list (crate-dep (name "approx") (req "^0.4") (default-features #t) (kind 2)))) (hash "1dxwiqx97y2rawkrvx8qsamz5pjaxj3lgnmn997wwiyxr94px91s")))

(define-public crate-kendalls-0.2 (crate (name "kendalls") (vers "0.2.1") (deps (list (crate-dep (name "approx") (req "^0.4") (default-features #t) (kind 2)))) (hash "18fmcdgafh30fldinljx3w79mfj1yj9fwp2v3gmh95pq28k70c7r")))

