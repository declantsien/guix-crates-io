(define-module (crates-io ke rl) #:use-module (crates-io))

(define-public crate-kerl-0.1 (crate (name "kerl") (vers "0.1.0") (hash "1f34plcvwwgfqpwj436k34sh9ki9zqici43hnbgzch4w3hffqgn6") (yanked #t)))

(define-public crate-kerl-p-0.1 (crate (name "kerl-p") (vers "0.1.0") (deps (list (crate-dep (name "crunchy") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "sponge-preview") (req "^0.2.0") (default-features #t) (kind 0)))) (hash "1q0x04c31dfqpvfbybsmjvp7lqlsam2pis5nqi84w78yy24vbjd2")))

