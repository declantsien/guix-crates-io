(define-module (crates-io ke nn) #:use-module (crates-io))

(define-public crate-kennitala-0.0.3 (crate (name "kennitala") (vers "0.0.3") (deps (list (crate-dep (name "regex") (req "^0.1.30") (default-features #t) (kind 0)))) (hash "1iqhis0qigc5d97qgvf1avdphnz1cwa744xdjbg4m1axl6y6161m")))

(define-public crate-kennitolur-0.1 (crate (name "kennitolur") (vers "0.1.0") (deps (list (crate-dep (name "chrono") (req "^0.4") (optional #t) (default-features #t) (kind 0)))) (hash "1phb94gxv08wk68fd89f109m7jf9bqmb195npff03im8g9lgppac") (features (quote (("default" "chrono"))))))

(define-public crate-kennitolur-0.1 (crate (name "kennitolur") (vers "0.1.1") (deps (list (crate-dep (name "chrono") (req "^0.4") (optional #t) (default-features #t) (kind 0)))) (hash "1cn0wxzjq4vpnfdkr6z3nbjwphnmp4x89nz96qg3p0157f87apdl") (features (quote (("default" "chrono"))))))

(define-public crate-kennitolur-0.1 (crate (name "kennitolur") (vers "0.1.2") (deps (list (crate-dep (name "chrono") (req "^0.4") (optional #t) (default-features #t) (kind 0)))) (hash "1fcx0w8gayw36knnly2n48jhnxj5sli28ng6xcyy0y0zmq3jz1bp") (features (quote (("default" "chrono"))))))

(define-public crate-kennitolur-0.1 (crate (name "kennitolur") (vers "0.1.3") (deps (list (crate-dep (name "chrono") (req "^0.4") (optional #t) (default-features #t) (kind 0)))) (hash "047arp0ybl83vq7ig2cgs0qfl4wr938cqcizyyf99rgr5sxig8ac") (features (quote (("default" "chrono"))))))

(define-public crate-kennitolur-0.1 (crate (name "kennitolur") (vers "0.1.4") (deps (list (crate-dep (name "chrono") (req "^0.4") (optional #t) (default-features #t) (kind 0)))) (hash "02ism6vfgyyp2x1rr8s15gmg67az3dpsas6r5i8pa4xzvg0nyva4") (features (quote (("default" "chrono"))))))

(define-public crate-kennitolur-0.2 (crate (name "kennitolur") (vers "0.2.0") (deps (list (crate-dep (name "chrono") (req "^0.4") (optional #t) (default-features #t) (kind 0)))) (hash "0np8gbf480rxygsc7zfci322k30hksw8x0k9ykxr8y0rxq3i90h4") (features (quote (("default" "chrono"))))))

(define-public crate-kennitolur-0.2 (crate (name "kennitolur") (vers "0.2.1") (deps (list (crate-dep (name "chrono") (req "^0.4") (optional #t) (default-features #t) (kind 0)))) (hash "1jd9kmrccgninx975n3vdgq23kar292zqs2akb52vq4gaya5mabw") (features (quote (("default" "chrono"))))))

(define-public crate-kenny_first_cargo_document_example-0.1 (crate (name "kenny_first_cargo_document_example") (vers "0.1.0") (hash "11r602jz671g3hfknk21a158rkm39y1dkjkd85n2wvxka2p0b5ml")))

