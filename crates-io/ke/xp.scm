(define-module (crates-io ke xp) #:use-module (crates-io))

(define-public crate-kexplain-0.1 (crate (name "kexplain") (vers "0.1.0") (deps (list (crate-dep (name "proc-macro2") (req "^1.0.39") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.18") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (features (quote ("full" "extra-traits"))) (default-features #t) (kind 0)) (crate-dep (name "trybuild") (req "^1.0.49") (features (quote ("diff"))) (default-features #t) (kind 2)))) (hash "0q9f6x6jxbz95dapgvqqq6s2qmbz6dnyw52ijgsdxmsndxrgxikj")))

