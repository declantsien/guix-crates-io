(define-module (crates-io ke #{04}#) #:use-module (crates-io))

(define-public crate-ke04z4-pac-0.1 (crate (name "ke04z4-pac") (vers "0.1.0") (deps (list (crate-dep (name "cortex-m") (req "^0.7") (default-features #t) (kind 0)) (crate-dep (name "cortex-m-rt") (req "^0.6") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "vcell") (req "^0.1") (default-features #t) (kind 0)))) (hash "16v9343mjh3yk0f1108vmxxsh567ckv8m9q1kxr67jdngjxd8ysz") (features (quote (("rt" "cortex-m-rt/device"))))))

