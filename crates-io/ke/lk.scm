(define-module (crates-io ke lk) #:use-module (crates-io))

(define-public crate-kelk-0.3 (crate (name "kelk") (vers "0.3.0") (deps (list (crate-dep (name "doc-comment") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "fnv") (req "^1.0.7") (kind 0)) (crate-dep (name "kelk-allocator") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "kelk-derive") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "kelk-env") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "minicbor") (req "^0.18") (features (quote ("half" "derive" "alloc"))) (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8") (features (quote ("small_rng"))) (kind 0)) (crate-dep (name "wee_alloc") (req "^0.4") (kind 0)))) (hash "0c4lygbj1hdfnl1fbqij9pnzrmq4f4yhsfy640zjbn9s2smyms7j")))

(define-public crate-kelk-allocator-0.3 (crate (name "kelk-allocator") (vers "0.3.0") (deps (list (crate-dep (name "wee_alloc") (req "^0.4") (kind 0)))) (hash "1ka9yhhfzvd4m3bvywp1w380vcmnc43dfg31vrndd5mjis2mb6kh")))

(define-public crate-kelk-derive-0.2 (crate (name "kelk-derive") (vers "0.2.0") (deps (list (crate-dep (name "proc-macro2") (req "^1.0.6") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "12hdvl11dvlyf03wk0ca9wwzan8xpisin7rh9y62gfwdgcx2b2m6") (yanked #t)))

(define-public crate-kelk-derive-0.3 (crate (name "kelk-derive") (vers "0.3.0") (deps (list (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "10b4dhqgq5g15pm7vvq7nw7qpfzqsw73hlwbba7wy107d1pl06sr")))

(define-public crate-kelk-env-0.2 (crate (name "kelk-env") (vers "0.2.2") (deps (list (crate-dep (name "doc-comment") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "doc-comment") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "kelk-derive") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "minicbor") (req "^0.11") (features (quote ("half" "derive"))) (default-features #t) (kind 0)) (crate-dep (name "minicbor-derive") (req "^0.7") (default-features #t) (kind 0)) (crate-dep (name "wasm-bindgen-test") (req "^0.3") (default-features #t) (kind 2)))) (hash "1730p11860hhja13z1m7gzcivzmv8bq9ngbjqcp3hwgbsld38svz") (yanked #t)))

(define-public crate-kelk-env-0.3 (crate (name "kelk-env") (vers "0.3.0") (deps (list (crate-dep (name "minicbor") (req "^0.18") (features (quote ("half" "derive" "alloc"))) (default-features #t) (kind 0)) (crate-dep (name "wasm-bindgen-test") (req "^0.3") (default-features #t) (kind 2)))) (hash "1hqsm227yr524l4adwywymwb2lxgnnmk10agck26lw9yzrwkm9gy")))

(define-public crate-kelk-lib-0.2 (crate (name "kelk-lib") (vers "0.2.0") (deps (list (crate-dep (name "kelk-env") (req "^0.2.2") (default-features #t) (kind 0)))) (hash "00k0x63j7w91d9flzxbx96dwdw1zk982mb7qdnaihxi3nkfsw9wq") (yanked #t)))

