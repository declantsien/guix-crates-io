(define-module (crates-io zk sn) #:use-module (crates-io))

(define-public crate-zksnark-0.0.1 (crate (name "zksnark") (vers "0.0.1") (deps (list (crate-dep (name "bigint") (req "^4.4.0") (default-features #t) (kind 0)) (crate-dep (name "bn") (req "^0.4.3") (default-features #t) (kind 0)) (crate-dep (name "itertools") (req "^0.6.0") (default-features #t) (kind 0)) (crate-dep (name "quickcheck") (req "^0.7.1") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "~0.3.14") (default-features #t) (kind 0)))) (hash "0h83kpg8y5plpy49rrplbc11ib71sq4d6c9lzrz6wrhlpmkryl46")))

(define-public crate-zksnark-0.0.2 (crate (name "zksnark") (vers "0.0.2") (deps (list (crate-dep (name "bigint") (req "^4.4.0") (default-features #t) (kind 0)) (crate-dep (name "bn") (req "^0.4.3") (default-features #t) (kind 0)) (crate-dep (name "itertools") (req "^0.6.0") (default-features #t) (kind 0)) (crate-dep (name "quickcheck") (req "^0.7.1") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "~0.3.14") (default-features #t) (kind 0)))) (hash "1fg8aqqk7q8qjnm3xqnkag33q2gx0mnqfm4fh6fx7ycbn14swfng")))

(define-public crate-zksnarks-0.0.9 (crate (name "zksnarks") (vers "0.0.9") (hash "1i3rl3b08nx4xi9k5gn5pb10b4g9qpg565yjampxb0zcizcdcyk0")))

(define-public crate-zksnarks-0.0.1 (crate (name "zksnarks") (vers "0.0.1") (deps (list (crate-dep (name "bls-12-381") (req "^0.0.23") (kind 0)) (crate-dep (name "ec-pairing") (req "^0.0.14") (kind 0)) (crate-dep (name "jub-jub") (req "^0.0.20") (kind 0)) (crate-dep (name "merlin") (req "^3.0") (kind 0)) (crate-dep (name "parity-scale-codec") (req "^2.0.0") (features (quote ("derive"))) (kind 0)) (crate-dep (name "poly-commit") (req "^0.0.13") (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (features (quote ("getrandom"))) (kind 0)) (crate-dep (name "zkstd") (req "^0.0.22") (kind 0)))) (hash "0d94ab621qcz8ginj0l6vy7ssdw4lrlg7h1d7n6jqpmq8gfdis4w")))

