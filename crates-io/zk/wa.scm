(define-module (crates-io zk wa) #:use-module (crates-io))

(define-public crate-zkwasm-0.1 (crate (name "zkwasm") (vers "0.1.0") (hash "1yykrgzwkdqnqjsn5r7jsf0f2brh63h89hdm5iqligrkb3iwmngw")))

(define-public crate-zkwasm-rs-0.1 (crate (name "zkwasm-rs") (vers "0.1.0") (hash "0bhfz907c8nj5mv8anr5pmpan4s60xjl53ryjzj8n5z9jf5c7axg")))

(define-public crate-zkwasmer-0.0.1 (crate (name "zkwasmer") (vers "0.0.1") (deps (list (crate-dep (name "wasmer") (req "^2") (default-features #t) (kind 0)))) (hash "1gmwmbn0hah6qavsy1s2b33rzxg2as4pcb32vb59a45yv2r43hrm")))

(define-public crate-zkwasmtime-0.0.1 (crate (name "zkwasmtime") (vers "0.0.1") (deps (list (crate-dep (name "wasmtime") (req "^0") (default-features #t) (kind 0)))) (hash "08mxy6dkx1xx9lqn8mipfdz6fnvjwv2bcqbaffzfn2yy9yp75r9p")))

(define-public crate-zkwasmvm-0.1 (crate (name "zkwasmvm") (vers "0.1.0") (hash "1xjx29bdxacz8dc0vrljrdd27nlgavl657p9qmk1jqysgbwfry6m")))

(define-public crate-zkwavm-0.1 (crate (name "zkwavm") (vers "0.1.0") (hash "19pz1anqa7a8q7393rl8r9srrhx1h3frngi3c4sp9cdgbya8hd4z")))

