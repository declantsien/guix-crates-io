(define-module (crates-io zk vm) #:use-module (crates-io))

(define-public crate-zkvm-0.0.0 (crate (name "zkvm") (vers "0.0.0") (hash "16bwjszzqns2z9lq20vw7g9a3qyc00v6inkdslhgxdhcp1p0w1xj")))

(define-public crate-zkvmemu-0.1 (crate (name "zkvmemu") (vers "0.1.0") (hash "0ppiadi6g8g0p04skrbn811j7x4s5a0qqv3i0mc32c35c11dyhvz")))

(define-public crate-zkvmemu-core-0.1 (crate (name "zkvmemu-core") (vers "0.1.0") (hash "1v0h86rlamv0l4ww2rqh5dn74wpppz1k7706i9486s82gy00qsab")))

(define-public crate-zkvmemu-risc0-0.1 (crate (name "zkvmemu-risc0") (vers "0.1.0") (hash "09vf3qwfx20fsdid5zm3r13nkcgl6dpm2nzhw7rrbgrr2fkpnb25")))

(define-public crate-zkvmemu-sp1-0.1 (crate (name "zkvmemu-sp1") (vers "0.1.0") (hash "089xdyxh2d9q92rny2bm276k9zhylc997x8w6mnx26s53qs8zw69")))

