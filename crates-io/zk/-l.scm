(define-module (crates-io zk -l) #:use-module (crates-io))

(define-public crate-zk-loader-0.1 (crate (name "zk-loader") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^2.33") (default-features #t) (kind 0)) (crate-dep (name "flate2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "tar") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "zookeeper") (req "^0.5") (default-features #t) (kind 0)))) (hash "0z1k3gzwkvbas728inaa6c4rg4cnxgr7wh429axkjnkfxzlszhid")))

(define-public crate-zk-loader-0.1 (crate (name "zk-loader") (vers "0.1.1") (deps (list (crate-dep (name "clap") (req "^2.33") (default-features #t) (kind 0)) (crate-dep (name "flate2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "tar") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "zookeeper") (req "^0.5") (default-features #t) (kind 0)))) (hash "1igh7ravzyp3mmdf115a6m3p497szzil3ic4xgw0n1qbdbdi8cv4")))

(define-public crate-zk-loader-0.1 (crate (name "zk-loader") (vers "0.1.2") (deps (list (crate-dep (name "clap") (req "^2.33") (default-features #t) (kind 0)) (crate-dep (name "flate2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "tar") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "zookeeper") (req "^0.5") (default-features #t) (kind 0)))) (hash "19yxbhkl1mzlpxzm0ljwi2vaym1sssrs71c9qq5f5lwcq7a7nydg")))

(define-public crate-zk-loader-0.2 (crate (name "zk-loader") (vers "0.2.0") (deps (list (crate-dep (name "clap") (req "^2.33.3") (default-features #t) (kind 0)) (crate-dep (name "flate2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serial_test") (req "^0.5.1") (default-features #t) (kind 0)) (crate-dep (name "tar") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "zookeeper") (req "^0.5") (default-features #t) (kind 0)))) (hash "0dadfnh44dxkakixm7480sm8gmf06a34srcf908d82f739hr4iy1")))

