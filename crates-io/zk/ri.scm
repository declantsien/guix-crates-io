(define-module (crates-io zk ri) #:use-module (crates-io))

(define-public crate-zkrisc-0.1 (crate (name "zkrisc") (vers "0.1.0") (hash "0ha32cisx4bd745dgyabmxhgc3wyvk4injlxkvzw3p51qw7wqz0p")))

(define-public crate-zkrisc-rs-0.1 (crate (name "zkrisc-rs") (vers "0.1.0") (hash "12lp9cpchd5cdqkrzxla4b6gb5prw89iw0z47vy1yhiwzfydff21")))

(define-public crate-zkriscv-0.1 (crate (name "zkriscv") (vers "0.1.0") (hash "08flpyykwwfraw5kdw7h6skp7dh6017zvq2m5ccn4hx0562rx2fx")))

(define-public crate-zkriscvm-0.1 (crate (name "zkriscvm") (vers "0.1.0") (hash "0nriwcg38brccks1168n35xkxvm4x4jzvy6kca3k3d87fzcpgdqs")))

