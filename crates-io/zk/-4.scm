(define-module (crates-io zk #{-4}#) #:use-module (crates-io))

(define-public crate-zk-4lw-0.1 (crate (name "zk-4lw") (vers "0.1.0") (deps (list (crate-dep (name "failure") (req "^0.1.1") (default-features #t) (kind 0)))) (hash "0vghlc0b9jxhvzjlrkfp6jrcml3h4ka16ksxgcpp5dmz307ysw23") (features (quote (("client-tests"))))))

