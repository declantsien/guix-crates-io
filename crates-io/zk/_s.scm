(define-module (crates-io zk _s) #:use-module (crates-io))

(define-public crate-zk_secp256k1-0.1 (crate (name "zk_secp256k1") (vers "0.1.0") (deps (list (crate-dep (name "uint") (req "^0.9") (features (quote ("std"))) (default-features #t) (kind 0)))) (hash "0c6qsy8dvnm9703gxv9k8x91qbhsbnvx4vcvdwlh1c2hqggdzir3")))

