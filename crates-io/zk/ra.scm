(define-module (crates-io zk ra) #:use-module (crates-io))

(define-public crate-zkraken-lib-0.1 (crate (name "zkraken-lib") (vers "0.1.0") (deps (list (crate-dep (name "color-eyre") (req "^0.6") (default-features #t) (kind 0)) (crate-dep (name "hidapi") (req "^1.3.4") (default-features #t) (kind 0)))) (hash "01dfd5fwnj926spi7p0xjikq6rambyds34821yhxy39n495cvr08")))

(define-public crate-zkraken-lib-0.2 (crate (name "zkraken-lib") (vers "0.2.0-alpha") (deps (list (crate-dep (name "color-eyre") (req "^0.6") (default-features #t) (kind 0)) (crate-dep (name "hidapi-rusb") (req "^1.3.0") (default-features #t) (kind 0)) (crate-dep (name "image") (req "^0.24") (default-features #t) (kind 0)) (crate-dep (name "rusb") (req "^0.9") (default-features #t) (kind 0)))) (hash "16iih0sxal25wp5z1mmmds8z3q13a0cm1dxyz1605znc09751qnq")))

(define-public crate-zkraken-lib-0.2 (crate (name "zkraken-lib") (vers "0.2.0") (deps (list (crate-dep (name "color-eyre") (req "^0.6") (default-features #t) (kind 0)) (crate-dep (name "image") (req "^0.24") (default-features #t) (kind 0)) (crate-dep (name "mockall") (req "^0.11") (default-features #t) (kind 2)) (crate-dep (name "rusb") (req "^0.9") (default-features #t) (kind 0)))) (hash "1mppj375mm4sm75jfa4npma28qwsfhbglj5xg563snh96sn8lkj6")))

