(define-module (crates-io zk cr) #:use-module (crates-io))

(define-public crate-zkcranelift-0.1 (crate (name "zkcranelift") (vers "0.1.0") (hash "1rf4rm90vmsrhb9qv0xqsvkldhzbwg90gk3h0fzqpp9i09k0xjf9")))

(define-public crate-zkcross-0.0.1 (crate (name "zkcross") (vers "0.0.1") (deps (list (crate-dep (name "zkwasmer") (req "0.0.*") (default-features #t) (kind 0)))) (hash "1xd58xnrcz7p7xga6y1a97xac29c3kgsbgyllg1w1r01g3iqd613")))

