(define-module (crates-io zk me) #:use-module (crates-io))

(define-public crate-zkmemory-0.1 (crate (name "zkmemory") (vers "0.1.0") (hash "1c982nwn3nc5kdxv6133521d11k8kpc2hk1w9sfl34rqqgi7gc96")))

(define-public crate-zkmemory-0.1 (crate (name "zkmemory") (vers "0.1.1") (deps (list (crate-dep (name "cargo-llvm-cov") (req "^0.5.27") (default-features #t) (kind 0)) (crate-dep (name "ethnum") (req "^1.3.2") (default-features #t) (kind 0)) (crate-dep (name "hex") (req "^0.4.3") (default-features #t) (kind 0)) (crate-dep (name "rbtree") (req "^0.1.5") (default-features #t) (kind 0)))) (hash "1mq0gm9bj2wm7qs6jw8720pvbpz2xc5knqzw224yh42d9s6xv3ra")))

