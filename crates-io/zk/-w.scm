(define-module (crates-io zk -w) #:use-module (crates-io))

(define-public crate-zk-wasm-0.1 (crate (name "zk-wasm") (vers "0.1.0") (hash "0m1gj7zbjc615nmgvqp9m4fmw9d66lxqqrk76yn0lvk88yk7hi2i")))

(define-public crate-zk-wasmer-0.0.1 (crate (name "zk-wasmer") (vers "0.0.1") (deps (list (crate-dep (name "wasmer") (req "^2") (default-features #t) (kind 0)))) (hash "0w745q2nk4n9vkn0gpa63abf0r4hyqnc8l76nkisyhjg6an16zfh")))

(define-public crate-zk-wasmi-0.0.1 (crate (name "zk-wasmi") (vers "0.0.1") (deps (list (crate-dep (name "wasmi") (req "^0") (default-features #t) (kind 0)))) (hash "0k4k4r1g5a9kzrxvfniqdsjjsrgnd3rbvzjkbg5szm569wk13irf")))

(define-public crate-zk-wasmtime-0.0.1 (crate (name "zk-wasmtime") (vers "0.0.1") (deps (list (crate-dep (name "wasmtime") (req "^0") (default-features #t) (kind 0)))) (hash "1y1wavd4f78wc7wv29zf9q03grz8wjk4br7hpvaf2am1103h20bd")))

