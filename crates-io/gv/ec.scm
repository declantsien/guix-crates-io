(define-module (crates-io gv ec) #:use-module (crates-io))

(define-public crate-gvec-0.1 (crate (name "gvec") (vers "0.1.0") (hash "1l31szq0xxqb1wfmlj8nvim19jazi5nssq0fz3241zwwksb6x8kw")))

(define-public crate-gvec-0.2 (crate (name "gvec") (vers "0.2.0") (hash "0kcm26g0wx3wlmwzrvssw9df59fs05bhc2gy9lsm7m67hksfkbxm")))

(define-public crate-gvec-0.3 (crate (name "gvec") (vers "0.3.0") (hash "1p01328k8lj25275naz075dspil2b47781yy95wm5flmz18d5w2h")))

(define-public crate-gvec-0.4 (crate (name "gvec") (vers "0.4.0") (hash "1ghpfn0d3mijjlyqgs1qhr4l0vskqa5yiy5k4in84q6ybqaxdz8s")))

(define-public crate-gvec-0.5 (crate (name "gvec") (vers "0.5.0") (hash "1iipx6glg1fwh8mwbvdw579h1g2kd9ibgj3bnccbc56ba3lpsh0c")))

