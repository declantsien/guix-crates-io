(define-module (crates-io gv ox) #:use-module (crates-io))

(define-public crate-gvox-rs-0.1 (crate (name "gvox-rs") (vers "0.1.0") (hash "0gvdcvm6sslr909m5z68h3hbg3hr4qqfr7m7hdn60rym7xaaazjr")))

(define-public crate-gvox-rs-0.1 (crate (name "gvox-rs") (vers "0.1.1") (hash "01zpcbkw74va0lbzgzpvkl42ln6x10avlzy14g4jphj336pq4q0l")))

(define-public crate-gvox-rs-0.1 (crate (name "gvox-rs") (vers "0.1.2") (hash "0gaaawff3lz3am2a43pksqpjhgx3prba07cslfr40v1dc9aksp46")))

(define-public crate-gvox-rs-0.1 (crate (name "gvox-rs") (vers "0.1.3") (hash "1h5gggm43vfcyvkmgmj5vq0pgim6c52hsyp5jg5qgjkmkwrsimkp")))

(define-public crate-gvox-rs-0.1 (crate (name "gvox-rs") (vers "0.1.4") (deps (list (crate-dep (name "cmake") (req "^0.1.49") (default-features #t) (kind 1)))) (hash "1yj8jrz517w24x7kqia8kqpl2q4h5ndjf688a9s3yj49clah1h0q")))

(define-public crate-gvox-rs-0.1 (crate (name "gvox-rs") (vers "0.1.5") (deps (list (crate-dep (name "gvox-sys") (req "^0.0.1") (kind 0)))) (hash "1g3zdflijnn6wjlgb99sisd51mxmr8ccqv1v9c8wbp9psl5sji9l")))

(define-public crate-gvox-rs-0.1 (crate (name "gvox-rs") (vers "0.1.6") (deps (list (crate-dep (name "gvox-sys") (req "^0.1.0") (kind 0)))) (hash "1xrn95gadr3ghrjv38ry2w51hb6p4akx4i0mk95c1akqmf17845a")))

(define-public crate-gvox-rs-0.1 (crate (name "gvox-rs") (vers "0.1.7") (deps (list (crate-dep (name "gvox-sys") (req "^1.0.0") (kind 0)))) (hash "1ymbszvrgsa9wkdzkhgf6fvhwgjf7brcbdd3743lzz265jq7cazq")))

(define-public crate-gvox-rs-1 (crate (name "gvox-rs") (vers "1.0.0") (deps (list (crate-dep (name "bitflags") (req "^1.3.2") (default-features #t) (kind 0)) (crate-dep (name "fxhash") (req "^0.2.1") (default-features #t) (kind 0)) (crate-dep (name "gvox-sys") (req "^1.0.1") (kind 0)) (crate-dep (name "int-enum") (req "^0.5.0") (default-features #t) (kind 0)))) (hash "0d5vw2nmffbnnck7qjks7sgz60wj2bkq0ijwnq79q8gal1mvqs8l")))

(define-public crate-gvox-rs-1 (crate (name "gvox-rs") (vers "1.1.0") (deps (list (crate-dep (name "bitflags") (req "^1.3.2") (default-features #t) (kind 0)) (crate-dep (name "fxhash") (req "^0.2.1") (default-features #t) (kind 0)) (crate-dep (name "gvox-sys") (req "^1.1.0") (kind 0)) (crate-dep (name "int-enum") (req "^0.5.0") (default-features #t) (kind 0)))) (hash "1ch00cjxqjxkmxwc4m1ldmw3yqwhas2jq331wx5gn11dpksnfhms")))

(define-public crate-gvox-rs-1 (crate (name "gvox-rs") (vers "1.2.0") (deps (list (crate-dep (name "bitflags") (req "^1.3.2") (default-features #t) (kind 0)) (crate-dep (name "fxhash") (req "^0.2.1") (default-features #t) (kind 0)) (crate-dep (name "gvox-sys") (req "^1.2.0") (kind 0)) (crate-dep (name "int-enum") (req "^0.5.0") (default-features #t) (kind 0)))) (hash "1xf3cackq38zc0di2v644x74af4crvfr4h9qr1l2n3zrngcg77fh")))

(define-public crate-gvox-rs-1 (crate (name "gvox-rs") (vers "1.2.1") (deps (list (crate-dep (name "bitflags") (req "^1.3.2") (default-features #t) (kind 0)) (crate-dep (name "fxhash") (req "^0.2.1") (default-features #t) (kind 0)) (crate-dep (name "gvox-sys") (req "^1.2.1") (kind 0)) (crate-dep (name "int-enum") (req "^0.5.0") (default-features #t) (kind 0)))) (hash "07jhymkp4lza1vlh1516l46952s97knsb816m21lrrl2b56qpb4b")))

(define-public crate-gvox-rs-1 (crate (name "gvox-rs") (vers "1.2.2") (deps (list (crate-dep (name "bitflags") (req "^1.3.2") (default-features #t) (kind 0)) (crate-dep (name "fxhash") (req "^0.2.1") (default-features #t) (kind 0)) (crate-dep (name "gvox-sys") (req "^1.2.2") (kind 0)) (crate-dep (name "int-enum") (req "^0.5.0") (default-features #t) (kind 0)))) (hash "1ins2j5wsg401mnd9m9whn8adm3gkdn563zlnmrq6vv496nmqhbn")))

(define-public crate-gvox-rs-1 (crate (name "gvox-rs") (vers "1.2.3") (deps (list (crate-dep (name "bitflags") (req "^1.3.2") (default-features #t) (kind 0)) (crate-dep (name "fxhash") (req "^0.2.1") (default-features #t) (kind 0)) (crate-dep (name "gvox-sys") (req "^1.2.3") (kind 0)) (crate-dep (name "int-enum") (req "^0.5.0") (default-features #t) (kind 0)))) (hash "0j4pw16nmjcmxxfnvs8y3qfwk9d4l0pva4bj314c9bx6x3p0iam4")))

(define-public crate-gvox-rs-1 (crate (name "gvox-rs") (vers "1.2.4") (deps (list (crate-dep (name "bitflags") (req "^1.3.2") (default-features #t) (kind 0)) (crate-dep (name "fxhash") (req "^0.2.1") (default-features #t) (kind 0)) (crate-dep (name "gvox-sys") (req "^1.2.4") (kind 0)) (crate-dep (name "int-enum") (req "^0.5.0") (default-features #t) (kind 0)))) (hash "1436wr8l3nlw74djcljgaw27441dgwdl01qvg8xbpvagmg1f7882")))

(define-public crate-gvox-rs-1 (crate (name "gvox-rs") (vers "1.2.5") (deps (list (crate-dep (name "bitflags") (req "^1.3.2") (default-features #t) (kind 0)) (crate-dep (name "fxhash") (req "^0.2.1") (default-features #t) (kind 0)) (crate-dep (name "gvox-sys") (req "^1.2.5") (kind 0)) (crate-dep (name "int-enum") (req "^0.5.0") (default-features #t) (kind 0)))) (hash "0dczfpwzfrcwljxirq3s17g0f8vd7fyl957djfqb8qq5kppjsklk")))

(define-public crate-gvox-rs-1 (crate (name "gvox-rs") (vers "1.2.6") (deps (list (crate-dep (name "bitflags") (req "^1.3.2") (default-features #t) (kind 0)) (crate-dep (name "fxhash") (req "^0.2.1") (default-features #t) (kind 0)) (crate-dep (name "gvox-sys") (req "^1.2.6") (kind 0)) (crate-dep (name "int-enum") (req "^0.5.0") (default-features #t) (kind 0)))) (hash "018xa0wizmp3n8znfmxap3hhhscic4icmmr8lg35iyigh5pqzgw7")))

(define-public crate-gvox-rs-1 (crate (name "gvox-rs") (vers "1.2.7") (deps (list (crate-dep (name "bitflags") (req "^1.3.2") (default-features #t) (kind 0)) (crate-dep (name "fxhash") (req "^0.2.1") (default-features #t) (kind 0)) (crate-dep (name "gvox-sys") (req "^1.2.7") (kind 0)) (crate-dep (name "int-enum") (req "^0.5.0") (default-features #t) (kind 0)))) (hash "0nqqhc4j9mkdnx48ms67h5mjzkxzk5pws9a9rlw77x10c0w1q962")))

(define-public crate-gvox-rs-1 (crate (name "gvox-rs") (vers "1.2.8") (deps (list (crate-dep (name "bitflags") (req "^1.3.2") (default-features #t) (kind 0)) (crate-dep (name "fxhash") (req "^0.2.1") (default-features #t) (kind 0)) (crate-dep (name "gvox-sys") (req "^1.2.8") (kind 0)) (crate-dep (name "int-enum") (req "^0.5.0") (default-features #t) (kind 0)))) (hash "15251ybwc0aiafw1fq2bsd43h936fjj2hmwkwa60ni618zvsj7cr")))

(define-public crate-gvox-rs-1 (crate (name "gvox-rs") (vers "1.2.9") (deps (list (crate-dep (name "bitflags") (req "^1.3.2") (default-features #t) (kind 0)) (crate-dep (name "fxhash") (req "^0.2.1") (default-features #t) (kind 0)) (crate-dep (name "gvox-sys") (req "^1.2.9") (kind 0)) (crate-dep (name "int-enum") (req "^0.5.0") (default-features #t) (kind 0)))) (hash "0d7a02g4x3bgcf0p085r42qqf5vmkkdyx18vsvhdizm9hwxxglgg")))

(define-public crate-gvox-rs-1 (crate (name "gvox-rs") (vers "1.2.10") (deps (list (crate-dep (name "bitflags") (req "^1.3.2") (default-features #t) (kind 0)) (crate-dep (name "fxhash") (req "^0.2.1") (default-features #t) (kind 0)) (crate-dep (name "gvox-sys") (req "^1.2.10") (kind 0)))) (hash "13cl9yiwkf7z1lz5y5fjp4mhyglnwbpbig0pjvnkkpxp6m3dpvv1")))

(define-public crate-gvox-rs-1 (crate (name "gvox-rs") (vers "1.2.11") (deps (list (crate-dep (name "bitflags") (req "^1.3.2") (default-features #t) (kind 0)) (crate-dep (name "fxhash") (req "^0.2.1") (default-features #t) (kind 0)) (crate-dep (name "gvox-sys") (req "^1.2.11") (kind 0)))) (hash "004kdx78xnf40wiynzn97s1r0g1r7i43gwjbk5r2bm13ngn15rw3")))

(define-public crate-gvox-rs-1 (crate (name "gvox-rs") (vers "1.3.0") (deps (list (crate-dep (name "bitflags") (req "^1.3.2") (default-features #t) (kind 0)) (crate-dep (name "fxhash") (req "^0.2.1") (default-features #t) (kind 0)) (crate-dep (name "gvox-sys") (req "^1.3.0") (kind 0)))) (hash "1r362y5shxz9qmbywg41jnlif4443jcl3pig85zxvssbwhnkjp8z")))

(define-public crate-gvox-sys-0.0.1 (crate (name "gvox-sys") (vers "0.0.1") (deps (list (crate-dep (name "bindgen") (req "^0.63.0") (default-features #t) (kind 1)) (crate-dep (name "cmake") (req "^0.1.49") (default-features #t) (kind 1)))) (hash "19q2qkkppnr55whxfvcdp597nfmdr9y3770x0cng8x2lacg8jgnd")))

(define-public crate-gvox-sys-0.1 (crate (name "gvox-sys") (vers "0.1.0") (deps (list (crate-dep (name "bindgen") (req "^0.63.0") (default-features #t) (kind 1)) (crate-dep (name "cmake") (req "^0.1.49") (default-features #t) (kind 1)))) (hash "0dy8xcr5jkwf0jc6xgaj5gr8pn938h0vszacj3cmiivayc7rzw93")))

(define-public crate-gvox-sys-1 (crate (name "gvox-sys") (vers "1.0.0") (deps (list (crate-dep (name "bindgen") (req "^0.63.0") (default-features #t) (kind 1)) (crate-dep (name "cmake") (req "^0.1.49") (default-features #t) (kind 1)))) (hash "0v8441dzacrznrivq50kdzsrvh0wx2czq2rnm35a56d7spij42s1")))

(define-public crate-gvox-sys-1 (crate (name "gvox-sys") (vers "1.0.1") (deps (list (crate-dep (name "bindgen") (req "^0.63.0") (default-features #t) (kind 1)) (crate-dep (name "cmake") (req "^0.1.49") (default-features #t) (kind 1)))) (hash "15ysy1vczbj9jn0ylr6lbc899zk325s4d6swklrwwvymxgg05537")))

(define-public crate-gvox-sys-1 (crate (name "gvox-sys") (vers "1.1.0") (deps (list (crate-dep (name "bindgen") (req "^0.63.0") (default-features #t) (kind 1)) (crate-dep (name "cmake") (req "^0.1.49") (default-features #t) (kind 1)))) (hash "1xbqdgg2k8dabv5vc38rb5x4bjwqb8g7sh7nychs6hnyc1vz6g5l")))

(define-public crate-gvox-sys-1 (crate (name "gvox-sys") (vers "1.2.0") (deps (list (crate-dep (name "bindgen") (req "^0.63.0") (default-features #t) (kind 1)) (crate-dep (name "cmake") (req "^0.1.49") (default-features #t) (kind 1)))) (hash "109dxjw0gdqdxyyk5r9m23f86wa8qpsy072nvcnzwnzh48cy4c0n")))

(define-public crate-gvox-sys-1 (crate (name "gvox-sys") (vers "1.2.1") (deps (list (crate-dep (name "bindgen") (req "^0.63.0") (default-features #t) (kind 1)) (crate-dep (name "cmake") (req "^0.1.49") (default-features #t) (kind 1)))) (hash "0dp9qgj4dbqi28cch8yi86k3zgfmf04walcrq4ic7gzgchk5zji2")))

(define-public crate-gvox-sys-1 (crate (name "gvox-sys") (vers "1.2.2") (deps (list (crate-dep (name "bindgen") (req "^0.63.0") (default-features #t) (kind 1)) (crate-dep (name "cmake") (req "^0.1.49") (default-features #t) (kind 1)))) (hash "15rj5h7qhlak2jcap6jr5qkwndhqz1245z4d4d09abx58zvv2pva")))

(define-public crate-gvox-sys-1 (crate (name "gvox-sys") (vers "1.2.3") (deps (list (crate-dep (name "bindgen") (req "^0.63.0") (default-features #t) (kind 1)) (crate-dep (name "cmake") (req "^0.1.49") (default-features #t) (kind 1)))) (hash "13fb7z98yxh44r8wnbaqrzn7q3w53yv6a3mm2c6dh7bpdppc98ss")))

(define-public crate-gvox-sys-1 (crate (name "gvox-sys") (vers "1.2.4") (deps (list (crate-dep (name "bindgen") (req "^0.63.0") (default-features #t) (kind 1)) (crate-dep (name "cmake") (req "^0.1.49") (default-features #t) (kind 1)))) (hash "175rglxz2gl25wnhkizc7gdkqvchj5xjr97msq1shdfdif45607d")))

(define-public crate-gvox-sys-1 (crate (name "gvox-sys") (vers "1.2.5") (deps (list (crate-dep (name "bindgen") (req "^0.63.0") (default-features #t) (kind 1)) (crate-dep (name "cmake") (req "^0.1.49") (default-features #t) (kind 1)))) (hash "1xp6l4nj4l4ljmbm8a6pkd5ci37x722dpfbafgjhqdf28rb7gi88")))

(define-public crate-gvox-sys-1 (crate (name "gvox-sys") (vers "1.2.6") (deps (list (crate-dep (name "bindgen") (req "^0.63.0") (default-features #t) (kind 1)) (crate-dep (name "cmake") (req "^0.1.49") (default-features #t) (kind 1)))) (hash "1phbp5pn91ga7cswbk49xj9irxh12hbfvk5jhrrajlz1r5g32bvf")))

(define-public crate-gvox-sys-1 (crate (name "gvox-sys") (vers "1.2.7") (deps (list (crate-dep (name "bindgen") (req "^0.63.0") (default-features #t) (kind 1)) (crate-dep (name "cmake") (req "^0.1.49") (default-features #t) (kind 1)))) (hash "0qm571x6bmx94dsgwqrg4b3wsnmpycgb0pk4b8gsq8dgwzhv4556")))

(define-public crate-gvox-sys-1 (crate (name "gvox-sys") (vers "1.2.8") (deps (list (crate-dep (name "bindgen") (req "^0.63.0") (default-features #t) (kind 1)) (crate-dep (name "cmake") (req "^0.1.49") (default-features #t) (kind 1)))) (hash "195qwb5cp54hpimaxy228bf6p29wz8q7rdwdhzykp9h3iwa8i0ay")))

(define-public crate-gvox-sys-1 (crate (name "gvox-sys") (vers "1.2.9") (deps (list (crate-dep (name "bindgen") (req "^0.63.0") (default-features #t) (kind 1)) (crate-dep (name "cmake") (req "^0.1.49") (default-features #t) (kind 1)))) (hash "13c6243wi7ar7g2l4k7k6vlyg010mw43lwk84brpk959vdar100c")))

(define-public crate-gvox-sys-1 (crate (name "gvox-sys") (vers "1.2.10") (deps (list (crate-dep (name "bindgen") (req "^0.63.0") (features (quote ("runtime"))) (optional #t) (kind 1)) (crate-dep (name "cmake") (req "^0.1.49") (default-features #t) (kind 1)))) (hash "13ccx4bz9s5bp0dskyna2as5i3abj7yphsj6rq4sjfq72rk29280")))

(define-public crate-gvox-sys-1 (crate (name "gvox-sys") (vers "1.2.11") (deps (list (crate-dep (name "bindgen") (req "^0.63.0") (features (quote ("runtime"))) (optional #t) (kind 1)) (crate-dep (name "cmake") (req "^0.1.49") (default-features #t) (kind 1)))) (hash "0nifc0vm4dj4ifm8yk98xm0mc7574lzcv30da8nj94vdnsrjmzj1")))

(define-public crate-gvox-sys-1 (crate (name "gvox-sys") (vers "1.3.0") (deps (list (crate-dep (name "bindgen") (req "^0.63.0") (features (quote ("runtime"))) (optional #t) (kind 1)) (crate-dep (name "cmake") (req "^0.1.49") (default-features #t) (kind 1)))) (hash "1b8cx8fzkh4750gc705pgx77svql74fb8vpiww82n9sa5vi4niss")))

