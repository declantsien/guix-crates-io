(define-module (crates-io gv fs) #:use-module (crates-io))

(define-public crate-gvfs-0.1 (crate (name "gvfs") (vers "0.1.0") (deps (list (crate-dep (name "directories") (req "^2") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "skeptic") (req "^0.13") (default-features #t) (kind 1)) (crate-dep (name "skeptic") (req "^0.13") (default-features #t) (kind 2)) (crate-dep (name "zip") (req "^0.5") (kind 0)))) (hash "13gizvzmsn78cr6a0bcv7as453adplhqnwgfc7a7ckj8v31p6sdx")))

(define-public crate-gvfs-0.1 (crate (name "gvfs") (vers "0.1.1") (deps (list (crate-dep (name "directories") (req "^2") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "skeptic") (req "^0.13") (default-features #t) (kind 1)) (crate-dep (name "skeptic") (req "^0.13") (default-features #t) (kind 2)) (crate-dep (name "zip") (req "^0.5") (kind 0)))) (hash "0yiiklvz94nwp4j2dd45ckrj2yb2216y4zfjacn124pdf0mi3xyx")))

(define-public crate-gvfs-0.1 (crate (name "gvfs") (vers "0.1.2") (deps (list (crate-dep (name "directories") (req "^2") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "skeptic") (req "^0.13") (default-features #t) (kind 1)) (crate-dep (name "skeptic") (req "^0.13") (default-features #t) (kind 2)) (crate-dep (name "zip") (req "^0.5") (kind 0)))) (hash "0s6kfla380js9gkbjxy19qpnpcxpqzkwjplp8rjr8v3v3ad4na9r")))

(define-public crate-gvfs-zbus-0.1 (crate (name "gvfs-zbus") (vers "0.1.0") (deps (list (crate-dep (name "zbus") (req "^3") (default-features #t) (kind 0)))) (hash "1pxh8xzi82crk4hv3kd0c3r8lz6p5sjdnvzhxbvbbk3y2g2vdscl")))

