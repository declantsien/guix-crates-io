(define-module (crates-io gv r-) #:use-module (crates-io))

(define-public crate-gvr-sys-0.1 (crate (name "gvr-sys") (vers "0.1.0") (deps (list (crate-dep (name "bindgen") (req "^0.22.0") (default-features #t) (kind 1)))) (hash "1zzv6gfsfc9gk5qxyf2c4bh3jkq3ir2lj57vh6wd5zngb4zqk9d3")))

(define-public crate-gvr-sys-0.2 (crate (name "gvr-sys") (vers "0.2.0") (deps (list (crate-dep (name "bindgen") (req "^0.22.0") (default-features #t) (kind 1)))) (hash "14vdl1hhk6g17g4msbx8w82xxwg5wbc6mkff99phzsrhw5q0pyh4")))

(define-public crate-gvr-sys-0.3 (crate (name "gvr-sys") (vers "0.3.0") (deps (list (crate-dep (name "bindgen") (req "^0.22.0") (default-features #t) (kind 1)))) (hash "01vd25skjndmv9pyvm1cd1kvmi7sn3b9wrvyi4vf24pw7byhj5bd")))

(define-public crate-gvr-sys-0.4 (crate (name "gvr-sys") (vers "0.4.0") (hash "10p6qzxb8fv6dxrcb7mja4jk2hvfs00bwr56sh5cvdbdjfl8c796")))

(define-public crate-gvr-sys-0.5 (crate (name "gvr-sys") (vers "0.5.0") (hash "0k0pprmxjxx0n7xj9nvxwqvfky2nz4jmaiv9ns3xw9fr7khsajz8")))

(define-public crate-gvr-sys-0.6 (crate (name "gvr-sys") (vers "0.6.0") (hash "1cr4kmgs38mvjxmjwfad4320y41ab350vjrn9zh0rv91bx865plj")))

(define-public crate-gvr-sys-0.7 (crate (name "gvr-sys") (vers "0.7.0") (hash "16jv13cxp0f960jmc4csc8w1b31xynp3srj4vjfk2ryfv2a4ncxi")))

(define-public crate-gvr-sys-0.7 (crate (name "gvr-sys") (vers "0.7.1") (hash "12lmc4zij98if9srbrc9imnpxkg1vbr37cmypzk8qlvknd6l6zs9")))

(define-public crate-gvr-sys-0.7 (crate (name "gvr-sys") (vers "0.7.2") (hash "15zpllq0g1lx7qsmf9pqjp7m17w3yvj1j06mv1grwvvm2bnyda6j")))

