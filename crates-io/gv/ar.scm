(define-module (crates-io gv ar) #:use-module (crates-io))

(define-public crate-gvariant-0.1 (crate (name "gvariant") (vers "0.1.0") (deps (list (crate-dep (name "gvariant-macro") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "ref-cast") (req "^1.0") (default-features #t) (kind 0)))) (hash "1dx5avb27sx9qkv062chd2ca6wvr4xlz9yki2ac4l2nqc6ks4igl")))

(define-public crate-gvariant-0.2 (crate (name "gvariant") (vers "0.2.0") (deps (list (crate-dep (name "gvariant-macro") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "ref-cast") (req "^1.0") (default-features #t) (kind 0)))) (hash "0lm7cvw8c2jsdx41dlf8syf4ynw91xvsklyzj66pi2lg3bk08gfm") (features (quote (("std" "alloc") ("default" "std" "alloc") ("alloc"))))))

(define-public crate-gvariant-0.3 (crate (name "gvariant") (vers "0.3.0") (deps (list (crate-dep (name "gvariant-macro") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "memchr") (req "^2") (kind 0)) (crate-dep (name "ref-cast") (req "^1.0") (default-features #t) (kind 0)))) (hash "10afypw3znjnzgd9f3m0vilb8ibs06pmv73qjan6y16101g32bk6") (features (quote (("std" "alloc" "memchr/std") ("default" "std" "alloc") ("alloc"))))))

(define-public crate-gvariant-0.4 (crate (name "gvariant") (vers "0.4.0") (deps (list (crate-dep (name "gvariant-macro") (req "^0.4.0") (default-features #t) (kind 0)) (crate-dep (name "memchr") (req "^2") (kind 0)) (crate-dep (name "ref-cast") (req "^1.0") (default-features #t) (kind 0)))) (hash "0fp1h5vj035g2qpgzkksj6n1vdgmnw64h6qvxa027zrqdcmrhykz") (features (quote (("std" "alloc" "memchr/std") ("default" "std" "alloc") ("alloc"))))))

(define-public crate-gvariant-0.5 (crate (name "gvariant") (vers "0.5.0") (deps (list (crate-dep (name "gvariant-macro") (req "^0.5.0") (default-features #t) (kind 0)) (crate-dep (name "memchr") (req "^2") (kind 0)) (crate-dep (name "ref-cast") (req "^1.0") (default-features #t) (kind 0)))) (hash "03j6nmwxdhnyk9baappgpaaa1yladdign8ci6r4imfdkhsasb3mx") (features (quote (("std" "alloc" "memchr/std") ("default" "std" "alloc") ("alloc"))))))

(define-public crate-gvariant-macro-0.1 (crate (name "gvariant-macro") (vers "0.1.0") (deps (list (crate-dep (name "syn") (req "^1.0") (default-features #t) (kind 0)))) (hash "1g4qmgd0byic98f4dav8qign4vwaxp8hsj7a3qwiavdx46vnlbyv")))

(define-public crate-gvariant-macro-0.2 (crate (name "gvariant-macro") (vers "0.2.0") (deps (list (crate-dep (name "syn") (req "^1.0") (default-features #t) (kind 0)))) (hash "0f6sn874822hr73wc8bajf4x1czjcckyii8v81pn689ac4k79577")))

(define-public crate-gvariant-macro-0.3 (crate (name "gvariant-macro") (vers "0.3.0") (deps (list (crate-dep (name "syn") (req "^1.0") (default-features #t) (kind 0)))) (hash "0kqlsxla5mnpzyvcsacmslwwmi8mndmpk310labkbg1n5i6ll3dx")))

(define-public crate-gvariant-macro-0.4 (crate (name "gvariant-macro") (vers "0.4.0") (deps (list (crate-dep (name "syn") (req "^1.0") (default-features #t) (kind 0)))) (hash "0vgbsb34kfvxnirh5irparfs4dfgifkxpa7f0f0s4dblrnf725cr")))

(define-public crate-gvariant-macro-0.5 (crate (name "gvariant-macro") (vers "0.5.0") (deps (list (crate-dep (name "syn") (req "^1.0") (default-features #t) (kind 0)))) (hash "1wdr97syw6imvfb4n5w27h97ccaz1fvs822kfrn6blln6wxw7mi6")))

