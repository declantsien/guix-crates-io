(define-module (crates-io gv #{10}#) #:use-module (crates-io))

(define-public crate-gv100ad-0.1 (crate (name "gv100ad") (vers "0.1.0") (deps (list (crate-dep (name "chrono") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "tracing") (req "^0.1") (default-features #t) (kind 0)))) (hash "13lq0giw0r1w84paaxmszaih1011fhy85kw86xhm77krm4l2hhl1")))

(define-public crate-gv100ad-0.2 (crate (name "gv100ad") (vers "0.2.0") (deps (list (crate-dep (name "chrono") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "tracing") (req "^0.1") (default-features #t) (kind 0)))) (hash "0c90x7pxijlbczhv177vi6k9qmnmhva5i5wz107dgyhrlv441fxy")))

