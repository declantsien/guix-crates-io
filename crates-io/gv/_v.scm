(define-module (crates-io gv _v) #:use-module (crates-io))

(define-public crate-gv_video-0.1 (crate (name "gv_video") (vers "0.1.7") (deps (list (crate-dep (name "byteorder") (req "^1.5.0") (default-features #t) (kind 0)) (crate-dep (name "lz4_flex") (req "^0.11") (kind 0)) (crate-dep (name "paste") (req "^1.0.12") (default-features #t) (kind 0)) (crate-dep (name "texture2ddecoder") (req "^0.0.5") (default-features #t) (kind 0)))) (hash "1vdp3fryf656qyb73wdhp7aafqb7crqc69j6fvaiw58nn4zavbni")))

(define-public crate-gv_video-0.1 (crate (name "gv_video") (vers "0.1.8") (deps (list (crate-dep (name "byteorder") (req "^1.5.0") (default-features #t) (kind 0)) (crate-dep (name "lz4_flex") (req "^0.11") (kind 0)) (crate-dep (name "paste") (req "^1.0.12") (default-features #t) (kind 0)) (crate-dep (name "texture2ddecoder") (req "^0.0.5") (default-features #t) (kind 0)))) (hash "1krzw2v7q3yhgljj88i3phgkff2m1hcj0919zzv3cim4wqg09a8h")))

(define-public crate-gv_video-0.1 (crate (name "gv_video") (vers "0.1.9") (deps (list (crate-dep (name "byteorder") (req "^1.5.0") (default-features #t) (kind 0)) (crate-dep (name "lz4_flex") (req "^0.11") (kind 0)) (crate-dep (name "paste") (req "^1.0.12") (default-features #t) (kind 0)) (crate-dep (name "texture2ddecoder") (req "^0.0.5") (default-features #t) (kind 0)))) (hash "09mvbls68xx73h3lrmlqg2wj3c32845lfkmm7znwfjl3r82y0b8s")))

(define-public crate-gv_video-0.1 (crate (name "gv_video") (vers "0.1.10") (deps (list (crate-dep (name "byteorder") (req "^1.5.0") (default-features #t) (kind 0)) (crate-dep (name "lz4_flex") (req "^0.11") (kind 0)) (crate-dep (name "paste") (req "^1.0.12") (default-features #t) (kind 0)) (crate-dep (name "texture2ddecoder") (req "^0.0.5") (default-features #t) (kind 0)))) (hash "1njg38d10q8s7597c7kly68yjpzbplikp273c7afavj0a0lc8b5h") (features (quote (("unsafe") ("default" "unsafe"))))))

