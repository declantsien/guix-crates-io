(define-module (crates-io rb ta) #:use-module (crates-io))

(define-public crate-rbtables-0.1 (crate (name "rbtables") (vers "0.1.1") (deps (list (crate-dep (name "crossbeam") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "md5") (req "^0.3.6") (default-features #t) (kind 0)) (crate-dep (name "num_cpus") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)))) (hash "1i10y0csh10zl5i7jhr6k2p9hyf38qk1bb4a2c2hpd3xgx3n4mb9")))

(define-public crate-rbtables-0.1 (crate (name "rbtables") (vers "0.1.2") (deps (list (crate-dep (name "crossbeam") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.1.0") (default-features #t) (kind 0)) (crate-dep (name "md5") (req "^0.3.6") (default-features #t) (kind 2)) (crate-dep (name "num_cpus") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.5") (default-features #t) (kind 2)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)))) (hash "1ayq1xbi7kp8c9l8q4rgn04z37cn5dzrm2d3sf48mh8dqidx59ww")))

(define-public crate-rbtables-0.1 (crate (name "rbtables") (vers "0.1.3") (deps (list (crate-dep (name "crossbeam") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.1.0") (default-features #t) (kind 0)) (crate-dep (name "md5") (req "^0.3.6") (default-features #t) (kind 2)) (crate-dep (name "num_cpus") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.5") (default-features #t) (kind 2)))) (hash "0rgi6hbbwhxc6hn6kc63l5gpgrjxwh6z1s6gpz26w4nd1z587yhq")))

(define-public crate-rbtables-0.1 (crate (name "rbtables") (vers "0.1.4") (deps (list (crate-dep (name "crossbeam") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.1.0") (default-features #t) (kind 0)) (crate-dep (name "md5") (req "^0.3.6") (default-features #t) (kind 2)) (crate-dep (name "num_cpus") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.5") (default-features #t) (kind 2)))) (hash "0gngwgl8z4m1zhabbrb8gw0yvfc1ck4lnckqbjczx4kxjgypnny7")))

(define-public crate-rbtag-0.1 (crate (name "rbtag") (vers "0.1.0") (deps (list (crate-dep (name "rbtag_derive") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "11pqryma48kmchsj72vhq4myxd519ws34yxjf6650736yvcq6vcb")))

(define-public crate-rbtag-0.2 (crate (name "rbtag") (vers "0.2.0") (deps (list (crate-dep (name "rbtag_derive") (req "^0.2.0") (default-features #t) (kind 0)))) (hash "1jsxbxv36kyd5dikg7c8wcqlfgpz86qq2crl6g86hgyrf0rvqdk8")))

(define-public crate-rbtag-0.3 (crate (name "rbtag") (vers "0.3.0") (deps (list (crate-dep (name "rbtag_derive") (req "^0.3.0") (default-features #t) (kind 0)))) (hash "18rja4v5v8xngnpqippg0k3ckkpm7l7h544x1a4i3f60zhv4kikj")))

(define-public crate-rbtag_derive-0.1 (crate (name "rbtag_derive") (vers "0.1.0") (deps (list (crate-dep (name "chrono") (req "^0.4.6") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^0.4.24") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^0.6.10") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^0.15.24") (default-features #t) (kind 0)))) (hash "0zyysry3splsf4s2rq3sypw5gnkdqfb5lnvkpcsi4589yqix37zm")))

(define-public crate-rbtag_derive-0.2 (crate (name "rbtag_derive") (vers "0.2.0") (deps (list (crate-dep (name "proc-macro2") (req "^0.4.24") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^0.6.10") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^0.15.24") (default-features #t) (kind 0)))) (hash "0i1ycg6ljg2zvh1d0kg4bn5m34qdh621iql1c69lg42f1jad6h12")))

(define-public crate-rbtag_derive-0.3 (crate (name "rbtag_derive") (vers "0.3.0") (deps (list (crate-dep (name "proc-macro2") (req "^0.4.24") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^0.6.10") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^0.15.24") (default-features #t) (kind 0)))) (hash "1i37apchv6zd4251iv5hhpazwy4cpmilgq0inbdqmjnc22vi2mdp")))

