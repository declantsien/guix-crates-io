(define-module (crates-io rb fi) #:use-module (crates-io))

(define-public crate-rbfi-1 (crate (name "rbfi") (vers "1.2.0") (hash "13qp7sfhax0h5p328rqa0l0qlqj7n0ci02b1axh4x0kaibfk501y") (yanked #t)))

(define-public crate-rbfi-1 (crate (name "rbfi") (vers "1.2.1") (hash "0w2831rqymsx3fpizbbi2yviy7bjjfhcav8pi7xbh99728pvzkb1") (yanked #t)))

