(define-module (crates-io rb ts) #:use-module (crates-io))

(define-public crate-rbtset-1 (crate (name "rbtset") (vers "1.0.0") (deps (list (crate-dep (name "criterion") (req "^0.2") (default-features #t) (kind 2)) (crate-dep (name "lazy_static") (req "^1.3") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.6") (default-features #t) (kind 2)))) (hash "1kzl6wzbhlc3rdvillzbff5krv6ivghvcmmdlp40zndhvfzl31wv")))

(define-public crate-rbtset-1 (crate (name "rbtset") (vers "1.0.1") (deps (list (crate-dep (name "criterion") (req "^0.2") (default-features #t) (kind 2)) (crate-dep (name "lazy_static") (req "^1.3") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.6") (default-features #t) (kind 2)))) (hash "1maaha2c14b091d4w34zs2r7s4h8ggcf39g3wiiak9w0jnahjh9k")))

(define-public crate-rbtset-1 (crate (name "rbtset") (vers "1.0.2") (deps (list (crate-dep (name "criterion") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.8") (default-features #t) (kind 2)))) (hash "1nl098yq2rxc0zd2k4klfm0mcll4ki0smh23dl6388hjiipqvjmw")))

