(define-module (crates-io rb d-) #:use-module (crates-io))

(define-public crate-rbd-2d-0.1 (crate (name "rbd-2d") (vers "0.1.0") (deps (list (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)))) (hash "1wgrf658sqpi94s0jz5pmwi30vy9yxjgnc7lk8jjxjwvvpq93wcg") (yanked #t)))

