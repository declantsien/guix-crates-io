(define-module (crates-io rb ot) #:use-module (crates-io))

(define-public crate-rbothal-0.0.1 (crate (name "rbothal") (vers "0.0.1") (deps (list (crate-dep (name "bindgen") (req "^0.51.1") (default-features #t) (kind 1)))) (hash "1q09p69pa1zc7h7gpg6av6il16ifk48yljyd0zz2az12mhqi5yx5")))

(define-public crate-rbothal-0.0.2 (crate (name "rbothal") (vers "0.0.2") (deps (list (crate-dep (name "bindgen") (req "^0.51.1") (default-features #t) (kind 1)))) (hash "0yyphkgd9ncb7jdplv8jb882l7dqmk3jvh0ibavdkag7qsypay19")))

(define-public crate-rbotlib-0.0.1 (crate (name "rbotlib") (vers "0.0.1") (deps (list (crate-dep (name "bindgen") (req "^0.51.1") (default-features #t) (kind 1)) (crate-dep (name "rbothal") (req "^0.0.1") (default-features #t) (kind 0)))) (hash "0lnpk6bqrxhwyjamypiwc36s195y2gb9g6kbndvpqwiccy52fhr7")))

(define-public crate-rbotlib-0.0.2 (crate (name "rbotlib") (vers "0.0.2") (deps (list (crate-dep (name "rbothal") (req "^0.0.2") (default-features #t) (kind 0)))) (hash "100czwwl0nznszfvjlm864x5yi2hn9cr6ssx9q69lrx4n1vkx6yw")))

