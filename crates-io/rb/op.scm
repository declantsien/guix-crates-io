(define-module (crates-io rb op) #:use-module (crates-io))

(define-public crate-rbop-0.1 (crate (name "rbop") (vers "0.1.0") (deps (list (crate-dep (name "rust_decimal") (req "^1.14") (features (quote ("maths"))) (kind 0)) (crate-dep (name "rust_decimal_macros") (req "^1.14") (kind 0)) (crate-dep (name "termion") (req "^1.5.6") (default-features #t) (kind 2)))) (hash "149faikx0y9bwkyxy3vc5g85bp338dmf5q9qx1gvn27wqaljfy7s")))

(define-public crate-rbop-0.2 (crate (name "rbop") (vers "0.2.0") (deps (list (crate-dep (name "num-integer") (req "^0.1.44") (kind 0)) (crate-dep (name "num-traits") (req "^0.2.14") (kind 0)) (crate-dep (name "rust_decimal") (req "=1.23.1") (features (quote ("maths"))) (kind 0)) (crate-dep (name "speedy2d") (req "^1.1.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "termion") (req "^1.5.6") (optional #t) (default-features #t) (kind 0)))) (hash "04m43zn561s3q7zsgbys415kcs0hiq9anpi0b736331cdc2lcv3m") (features (quote (("examples" "termion" "speedy2d"))))))

