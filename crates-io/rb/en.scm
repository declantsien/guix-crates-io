(define-module (crates-io rb en) #:use-module (crates-io))

(define-public crate-rbench-0.1 (crate (name "rbench") (vers "0.1.0") (hash "0r72730hzb92xl6yq20xrx4xnzwci4zimapp557jwsw7s93w1bv7")))

(define-public crate-rbench-1 (crate (name "rbench") (vers "1.0.0") (deps (list (crate-dep (name "clap") (req "^2.33.3") (default-features #t) (kind 0)))) (hash "0pw7vrwzqj8byq7vs2r5f7fbc06bzvxhns7vbgy0ch75fsm9say3")))

(define-public crate-rbenchmark-0.1 (crate (name "rbenchmark") (vers "0.1.0") (deps (list (crate-dep (name "anyhow") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "byte-unit") (req "^4") (default-features #t) (kind 0)) (crate-dep (name "crossbeam") (req "^0.8") (features (quote ("crossbeam-channel"))) (default-features #t) (kind 0)) (crate-dep (name "lzma-rs") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "num_cpus") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "sha2") (req "^0.10") (default-features #t) (kind 0)) (crate-dep (name "sqlite") (req "^0.30") (default-features #t) (kind 0)) (crate-dep (name "threadpool") (req "^1") (default-features #t) (kind 0)))) (hash "13slajdh11i0vbg5q1j5jcmpb0gs3jhcgskg0xhfr2cbvm9n1sd7")))

