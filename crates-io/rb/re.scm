(define-module (crates-io rb re) #:use-module (crates-io))

(define-public crate-rbrep-0.1 (crate (name "rbrep") (vers "0.1.0") (deps (list (crate-dep (name "anyhow") (req "^1.0.68") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^4.0.32") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "console") (req "^0.15.3") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.38") (default-features #t) (kind 0)))) (hash "0wsnlslhr7mqx3cxk6wf171kyn5fb3nzqjma313g73i2yr2y23cc")))

(define-public crate-rbrew-0.0.1 (crate (name "rbrew") (vers "0.0.1") (deps (list (crate-dep (name "argp") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "json") (req "^0.12.4") (default-features #t) (kind 0)))) (hash "1sl188wx8hwpnfsa9jf0x3gq361p92l7di85z6254js7a8ncj9jm") (yanked #t)))

(define-public crate-rbrew-0.0.2 (crate (name "rbrew") (vers "0.0.2") (deps (list (crate-dep (name "argp") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "json") (req "^0.12.4") (default-features #t) (kind 0)))) (hash "05s6dcwsk2lnnd3gg9593zv8r0p2dx6v9m1kvf6v5cs5lwh6pyyy") (yanked #t)))

