(define-module (crates-io rb t3) #:use-module (crates-io))

(define-public crate-rbt3-0.1 (crate (name "rbt3") (vers "0.1.0") (deps (list (crate-dep (name "glam") (req "^0") (default-features #t) (kind 0)))) (hash "1f1127wbrkpac6468l3p9nrsnqzna8sh7wkgvpcf5k2arxqnc2s5")))

(define-public crate-rbt3-0.1 (crate (name "rbt3") (vers "0.1.1") (deps (list (crate-dep (name "glam") (req "^0") (default-features #t) (kind 0)))) (hash "1rddrxndq7y6gryl5i3ikc7xdlakfmzcbwwgxs9hgmxjdp2rqxp2")))

