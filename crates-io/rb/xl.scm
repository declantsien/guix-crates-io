(define-module (crates-io rb xl) #:use-module (crates-io))

(define-public crate-rbxl-0.1 (crate (name "rbxl") (vers "0.1.0") (hash "0vda4gvgj47bhr0h7rmvl3xwbzfjv6c08dqb9winyrxsin45a46i")))

(define-public crate-rbxlx-0.1 (crate (name "rbxlx") (vers "0.1.0") (hash "04aa0m6s0r0iajbp24hyd00xa8c87snnrfxzdwk54g1fm1jsymlh")))

