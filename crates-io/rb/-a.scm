(define-module (crates-io rb -a) #:use-module (crates-io))

(define-public crate-rb-allocator-0.9 (crate (name "rb-allocator") (vers "0.9.0") (deps (list (crate-dep (name "rb-sys") (req "^0.9.0") (default-features #t) (kind 0)))) (hash "1zcrigdgycra2yhsl2sr9b8wxkmffm6zllsqdcs8v2rb2sw0pk56")))

(define-public crate-rb-allocator-0.9 (crate (name "rb-allocator") (vers "0.9.1") (deps (list (crate-dep (name "rb-sys") (req "^0.9.1") (default-features #t) (kind 0)))) (hash "0h2wr9718y6an8wkx6j3gpid4xyxbz5gnylhir27lq2srgiqcdjg")))

(define-public crate-rb-allocator-0.9 (crate (name "rb-allocator") (vers "0.9.2") (deps (list (crate-dep (name "rb-sys") (req "^0.9.2") (default-features #t) (kind 0)))) (hash "1srzgwlmnf4r6lf0vvyk2p583cmclns7gbkjq67j88wrbakcg3z1")))

(define-public crate-rb-allocator-0.9 (crate (name "rb-allocator") (vers "0.9.3") (deps (list (crate-dep (name "rb-sys") (req "^0.9.2") (default-features #t) (kind 0)))) (hash "000j3smmanviqjmvxx251by45z3cyi38gfxvj6piwd0plmcy8sgd")))

(define-public crate-rb-allocator-0.9 (crate (name "rb-allocator") (vers "0.9.4") (deps (list (crate-dep (name "rb-sys") (req "^0.9.2") (default-features #t) (kind 0)))) (hash "1vvsin9biry10phs44w9vnj80a5a4c4jxnmgl345v7b6wzhjbvms")))

(define-public crate-rb-allocator-0.9 (crate (name "rb-allocator") (vers "0.9.5") (deps (list (crate-dep (name "rb-sys") (req "^0.9.2") (default-features #t) (kind 0)))) (hash "1v9axyrvlq236jwwzcqcfxr3d2ajl4mjicxcjaaqb2mpvdcya1dr")))

(define-public crate-rb-allocator-0.9 (crate (name "rb-allocator") (vers "0.9.6") (deps (list (crate-dep (name "rb-sys") (req "^0.9.2") (default-features #t) (kind 0)))) (hash "12s22wjgrlzi54bjl1jm9wfhjvs610a0nx0qqfj0y566hn7mplbp")))

