(define-module (crates-io rb l_) #:use-module (crates-io))

(define-public crate-rbl_circular_buffer-0.1 (crate (name "rbl_circular_buffer") (vers "0.1.0") (deps (list (crate-dep (name "criterion") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "proptest") (req "^0.9.6") (default-features #t) (kind 2)))) (hash "02s4gip2gddnjqyjfmdql5vnhl2xalxi2rlassqxm2k41p3zxbpw")))

(define-public crate-rbl_circular_buffer-0.1 (crate (name "rbl_circular_buffer") (vers "0.1.1") (deps (list (crate-dep (name "criterion") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "proptest") (req "^0.9.6") (default-features #t) (kind 2)))) (hash "0vnixzn9hq4c3x1j64pm7kgyl1q50ylnx4pz6clm002dydw5l9nc")))

(define-public crate-rbl_circular_buffer-0.1 (crate (name "rbl_circular_buffer") (vers "0.1.2") (deps (list (crate-dep (name "criterion") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "proptest") (req "^0.9.6") (default-features #t) (kind 2)))) (hash "180ssaram1rnhbz20msasr1p12npyzdm0swggksfzwmsgah5fmgi")))

