(define-module (crates-io rb uf) #:use-module (crates-io))

(define-public crate-rbuf-0.0.0 (crate (name "rbuf") (vers "0.0.0") (hash "1nqqcaz8fzxksvj7p3qbqrzh85iyxmkim7gkvsndwri5czb1wc5z")))

(define-public crate-rbuf-0.1 (crate (name "rbuf") (vers "0.1.0") (hash "0j6drwxb81da7prkpvvvv0vji1aamhl10kriy003qmvyydwdzihl")))

(define-public crate-rbuf-0.1 (crate (name "rbuf") (vers "0.1.1") (hash "0z0grsb161966qy1g1jwynw922l0phy926m75apcn3inpfhkrpgv")))

(define-public crate-rbuf-0.1 (crate (name "rbuf") (vers "0.1.2") (hash "01m7rnmv40w17w8mc9rz0b6an3yby095zybwiidv86rhzf5ibdqh")))

(define-public crate-rbuf-0.1 (crate (name "rbuf") (vers "0.1.3") (hash "0nrdc4crg4a6pi9lnjy0gfk6j19kncad65v0j9cm3mlrpkkqxwj6")))

