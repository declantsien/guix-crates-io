(define-module (crates-io rb ui) #:use-module (crates-io))

(define-public crate-rbuild-0.1 (crate (name "rbuild") (vers "0.1.0") (deps (list (crate-dep (name "paragraphs") (req ">= 0.3.0") (default-features #t) (kind 0)) (crate-dep (name "rgparse") (req ">= 0.1.2") (default-features #t) (kind 0)))) (hash "0piknbgcfm819ycddj33q1zmi5h9r2mly1xbxlk4bw1afm4ybh8k")))

(define-public crate-rbuild-0.2 (crate (name "rbuild") (vers "0.2.0") (deps (list (crate-dep (name "paragraphs") (req ">= 0.3.1") (default-features #t) (kind 0)) (crate-dep (name "rgparse") (req ">= 0.1.2") (default-features #t) (kind 0)))) (hash "0dsa0h752qkkzzwxkxx1c2qnlbgx41y5376biy2v66kp86z15pfc")))

(define-public crate-rbuild-0.3 (crate (name "rbuild") (vers "0.3.0") (deps (list (crate-dep (name "paragraphs") (req ">= 0.3.1") (default-features #t) (kind 0)) (crate-dep (name "rgparse") (req ">= 0.1.2") (default-features #t) (kind 0)))) (hash "0jiqwc384z16cw3vbp6ii2qnfbdd5b5jdwkk4wx0szcb3sl4dwn3")))

(define-public crate-rbuild-0.3 (crate (name "rbuild") (vers "0.3.1") (deps (list (crate-dep (name "paragraphs") (req ">= 0.3.1") (default-features #t) (kind 0)) (crate-dep (name "rgparse") (req ">= 0.1.2") (default-features #t) (kind 0)))) (hash "1sws42yzdvyiri66b7y6waxbim4xc6laji8vqdsgz2245yghccnz")))

