(define-module (crates-io rb it) #:use-module (crates-io))

(define-public crate-rbitset-0.1 (crate (name "rbitset") (vers "0.1.0") (deps (list (crate-dep (name "num-traits") (req "^0.2.8") (kind 0)))) (hash "1a2svnq6x1y3gbjnilfr3q47zpkadflf2fxgw7aifgikv4h75w5y")))

(define-public crate-rbitset-0.2 (crate (name "rbitset") (vers "0.2.0") (deps (list (crate-dep (name "num-traits") (req "^0.2.8") (kind 0)))) (hash "1d52szwlnsksc47xa4hxq2k5lz54xkddhkhjgihxxkfcj1kbvlfw")))

(define-public crate-rbitset-0.2 (crate (name "rbitset") (vers "0.2.1") (deps (list (crate-dep (name "num-traits") (req "^0.2.8") (kind 0)) (crate-dep (name "serde") (req "^1.0.0") (optional #t) (kind 0)))) (hash "0khj0c9c5ia0iywrrcpqn8swq6a90w1m1jabamsrlbbq69zibq5n") (v 2) (features2 (quote (("serde" "dep:serde"))))))

(define-public crate-rbitset-0.3 (crate (name "rbitset") (vers "0.3.0") (deps (list (crate-dep (name "num-traits") (req "^0.2.8") (kind 0)) (crate-dep (name "serde") (req "^1.0.0") (optional #t) (kind 0)))) (hash "0h7s9hhi9m3f6nkmiw3fi5hf361kgx10h7zy5q84yys7c48nn7ij") (v 2) (features2 (quote (("serde" "dep:serde"))))))

(define-public crate-rbitset-0.3 (crate (name "rbitset") (vers "0.3.1") (deps (list (crate-dep (name "num-traits") (req "^0.2.8") (kind 0)) (crate-dep (name "serde") (req "^1.0.0") (optional #t) (kind 0)))) (hash "0dqnbxdvnfigfbxywd1xzb63g078hrwpjk53zal9win67s93ahhx") (v 2) (features2 (quote (("serde" "dep:serde"))))))

