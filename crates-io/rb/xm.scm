(define-module (crates-io rb xm) #:use-module (crates-io))

(define-public crate-rbxm-0.1 (crate (name "rbxm") (vers "0.1.0") (deps (list (crate-dep (name "lz4_flex") (req "^0.7") (features (quote ("safe-decode" "safe-encode"))) (kind 0)) (crate-dep (name "rbxm-proc") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "02y434i7n27cilrsnkfn4aqfs28rpxas526khrks3fdsb8rdfqr2") (features (quote (("std" "lz4_flex/std") ("mesh-format") ("default" "std"))))))

(define-public crate-rbxm-0.1 (crate (name "rbxm") (vers "0.1.1") (deps (list (crate-dep (name "lz4_flex") (req "^0.7") (features (quote ("safe-decode" "safe-encode"))) (kind 0)) (crate-dep (name "rbxm-proc") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "1asz1mwig13kc6d28li5nqjr5xv0w1n471vpfkiwir3klsqyh5p5") (features (quote (("std" "lz4_flex/std") ("mesh-format") ("default" "std"))))))

(define-public crate-rbxm-0.1 (crate (name "rbxm") (vers "0.1.2") (deps (list (crate-dep (name "lz4_flex") (req "^0.7") (features (quote ("safe-decode" "safe-encode"))) (kind 0)) (crate-dep (name "rbxm-proc") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "1y31f9lw8irx4hl38pd25s1svvig20jm387i91kq7a78h6gw6kd1") (features (quote (("std" "lz4_flex/std") ("mesh-format") ("default" "std"))))))

(define-public crate-rbxm-0.1 (crate (name "rbxm") (vers "0.1.3") (deps (list (crate-dep (name "lz4_flex") (req "^0.7") (features (quote ("safe-decode" "safe-encode"))) (kind 0)) (crate-dep (name "rbxm-proc") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "1fayihl2laqjx74fmv3z1c2gbw0yayhrh3w7sad3mb6fy7lj7sf6") (features (quote (("std" "lz4_flex/std") ("mesh-format") ("default" "std"))))))

(define-public crate-rbxm-0.2 (crate (name "rbxm") (vers "0.2.0") (deps (list (crate-dep (name "lz4_flex") (req "^0.9") (features (quote ("safe-decode" "safe-encode"))) (kind 0)) (crate-dep (name "rbxm-proc") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "slotmap") (req "^1.0") (kind 0)) (crate-dep (name "uuid") (req "^0.8") (kind 0)))) (hash "1nmchhf2whkyjb8af7wpd3k4gnr5n8cp1spiinz9w8xcsddbbkzn") (features (quote (("unstable") ("std" "lz4_flex/std" "uuid/std") ("mesh-format") ("default" "std"))))))

(define-public crate-rbxm-0.2 (crate (name "rbxm") (vers "0.2.1") (deps (list (crate-dep (name "lz4_flex") (req "^0.9") (features (quote ("safe-decode" "safe-encode"))) (kind 0)) (crate-dep (name "rbxm-proc") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "slotmap") (req "^1.0") (kind 0)) (crate-dep (name "uuid") (req "^0.8") (kind 0)))) (hash "1pq5vf6bs1bvjbvl3w07z3ly1pmlf7kh04mqp6d270n3r0y24y3x") (features (quote (("unstable") ("std" "lz4_flex/std" "uuid/std") ("mesh-format") ("default" "std"))))))

(define-public crate-rbxm-0.3 (crate (name "rbxm") (vers "0.3.0") (deps (list (crate-dep (name "lz4_flex") (req "^0.9") (features (quote ("safe-decode" "safe-encode"))) (kind 0)) (crate-dep (name "num") (req "^0.4") (features (quote ("libm"))) (kind 0)) (crate-dep (name "rbxm-proc") (req "^0.2.1") (default-features #t) (kind 0)) (crate-dep (name "slotmap") (req "^1.0") (kind 0)) (crate-dep (name "uuid") (req "^1.1") (kind 0)))) (hash "0xih2lipagq0gnzx53610r5n0j3mlsrci3xl9pw2qg1s6hm4r51h") (features (quote (("unstable") ("std" "lz4_flex/std" "uuid/std" "num/std") ("mesh-format") ("default" "std"))))))

(define-public crate-rbxm-proc-0.1 (crate (name "rbxm-proc") (vers "0.1.0") (deps (list (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (default-features #t) (kind 0)))) (hash "022rz18yqy8ndh5bg59r0lv6h61x5cgin1plgx66vahr0611na86")))

(define-public crate-rbxm-proc-0.2 (crate (name "rbxm-proc") (vers "0.2.0") (deps (list (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (default-features #t) (kind 0)))) (hash "1jfyybybkswysa6s8n6r47z9k300svs68nzfbg4indqq93yafp54")))

(define-public crate-rbxm-proc-0.2 (crate (name "rbxm-proc") (vers "0.2.1") (deps (list (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (default-features #t) (kind 0)))) (hash "1gfpwbyi79j0k7whh8p8sx0d8d4h7vvcjyai3sr5zlcpavpglmc4")))

