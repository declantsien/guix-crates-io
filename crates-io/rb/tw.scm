(define-module (crates-io rb tw) #:use-module (crates-io))

(define-public crate-rbtw-1 (crate (name "rbtw") (vers "1.0.0") (deps (list (crate-dep (name "efibootnext") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "sudo") (req "^0.6") (default-features #t) (kind 0)))) (hash "1md6ss1y8bs8xwlzq6rfssm9xhgf6wf313fs11bssvhwfy7za03x")))

(define-public crate-rbtw-1 (crate (name "rbtw") (vers "1.0.1") (deps (list (crate-dep (name "efibootnext") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "sudo") (req "^0.6") (default-features #t) (kind 0)) (crate-dep (name "rustc_version") (req "^0.4") (default-features #t) (kind 1)))) (hash "0aln43x94i5km2qhjkrb5prrrj4z11q28q17kg1qszhmbm7pxbvr")))

(define-public crate-rbtw-1 (crate (name "rbtw") (vers "1.0.2") (deps (list (crate-dep (name "efibootnext") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "sudo") (req "^0.6") (default-features #t) (kind 0)) (crate-dep (name "rustc_version") (req "^0.4") (default-features #t) (kind 1)))) (hash "0cc03g67s9gyj7ha0j9dhf6klr1drd63z696026ngxblq7q59dwq")))

