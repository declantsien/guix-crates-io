(define-module (crates-io rb fs) #:use-module (crates-io))

(define-public crate-rbfs-0.1 (crate (name "rbfs") (vers "0.1.0") (deps (list (crate-dep (name "rust-format") (req "^0.3.4") (optional #t) (default-features #t) (kind 0)))) (hash "032pyhw75zppvhlgmmmydk3ya2hq0hxzbgxwwyb1a82ma056s1l3") (v 2) (features2 (quote (("format" "dep:rust-format"))))))

(define-public crate-rbfs-0.1 (crate (name "rbfs") (vers "0.1.1") (deps (list (crate-dep (name "rust-format") (req "^0.3.4") (optional #t) (default-features #t) (kind 0)))) (hash "1wf1qh3vwkf1ccd3fvrr1g0xzj8v089hjfqx7svjcddikccss28g") (v 2) (features2 (quote (("format" "dep:rust-format"))))))

(define-public crate-rbfs-0.1 (crate (name "rbfs") (vers "0.1.2") (deps (list (crate-dep (name "rust-format") (req "^0.3.4") (optional #t) (default-features #t) (kind 0)))) (hash "139m93hsi2jahymcm244rxlk2bvbb169kpbsmis7csad5xri3qhw") (v 2) (features2 (quote (("format" "dep:rust-format"))))))

(define-public crate-rbfs-0.1 (crate (name "rbfs") (vers "0.1.3") (deps (list (crate-dep (name "rust-format") (req "^0.3.4") (optional #t) (default-features #t) (kind 0)))) (hash "0pf2jwy4n84mcn5yf16qx1cpkhmsjvf8n6cf6782iv2n7zcfar5w") (v 2) (features2 (quote (("format" "dep:rust-format"))))))

(define-public crate-rbfs-0.1 (crate (name "rbfs") (vers "0.1.4") (deps (list (crate-dep (name "rust-format") (req "^0.3.4") (optional #t) (default-features #t) (kind 0)))) (hash "0y0q4jfhh65hirbjv818bg7831sqjwmhs1rmg986qi6g80ya0v65") (v 2) (features2 (quote (("format" "dep:rust-format"))))))

(define-public crate-rbfs-0.1 (crate (name "rbfs") (vers "0.1.5") (deps (list (crate-dep (name "rust-format") (req "^0.3.4") (optional #t) (default-features #t) (kind 0)))) (hash "07c220ylsy4hf9sp8bhay0fnli9vlnlr67mkd1xhzc63rz04idrw") (v 2) (features2 (quote (("format" "dep:rust-format"))))))

(define-public crate-rbfs-0.1 (crate (name "rbfs") (vers "0.1.6") (deps (list (crate-dep (name "rust-format") (req "^0.3.4") (optional #t) (default-features #t) (kind 0)))) (hash "182d0278ibn7p9zqfkx6jgb3x6wx0cyz1s9amjiymxcfs3iy7cx4") (v 2) (features2 (quote (("format" "dep:rust-format"))))))

(define-public crate-rbfs-0.1 (crate (name "rbfs") (vers "0.1.7") (deps (list (crate-dep (name "rust-format") (req "^0.3.4") (optional #t) (default-features #t) (kind 0)))) (hash "1qcf8l09bgx37fcznhmmycmjr5755wdld50dvz7xnbvxb1xrr5qm") (v 2) (features2 (quote (("format" "dep:rust-format"))))))

