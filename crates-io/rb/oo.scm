(define-module (crates-io rb oo) #:use-module (crates-io))

(define-public crate-rbook-0.0.0 (crate (name "rbook") (vers "0.0.0") (hash "1h9445ln9x4x5l5xmi65rja1j4sivfkx8hm6jmhbx9ld06s5vh0x") (yanked #t)))

(define-public crate-rbook-0.1 (crate (name "rbook") (vers "0.1.0") (deps (list (crate-dep (name "lol_html") (req "^0.3.2") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.38") (default-features #t) (kind 0)) (crate-dep (name "zip") (req "^0.6.3") (default-features #t) (kind 0)))) (hash "0vjryvby8gs3ndgf6w0n0mpgm1dwfm51wrz5vdxvdxg119mzs2yz") (features (quote (("statistics") ("reader") ("default" "reader" "statistics")))) (yanked #t)))

(define-public crate-rbook-0.1 (crate (name "rbook") (vers "0.1.1") (deps (list (crate-dep (name "lol_html") (req "^0.3.2") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.38") (default-features #t) (kind 0)) (crate-dep (name "zip") (req "^0.6.3") (default-features #t) (kind 0)))) (hash "0cj9wsnjpnl2cdgx8avs1873igx0dcvgs2xq7dv3mbd1q2836mbz") (features (quote (("statistics") ("reader") ("default" "reader" "statistics")))) (yanked #t)))

(define-public crate-rbook-0.1 (crate (name "rbook") (vers "0.1.2") (deps (list (crate-dep (name "lol_html") (req "^0.3.2") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.38") (default-features #t) (kind 0)) (crate-dep (name "zip") (req "^0.6.3") (default-features #t) (kind 0)))) (hash "0wsimxpkfqd198fb5dzaqp7dlv3nvp39w4kcm1l2m8f4rg4alsdh") (features (quote (("statistics") ("reader") ("default" "reader" "statistics")))) (yanked #t)))

(define-public crate-rbook-0.1 (crate (name "rbook") (vers "0.1.3") (deps (list (crate-dep (name "lol_html") (req "^0.3.2") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.38") (default-features #t) (kind 0)) (crate-dep (name "zip") (req "^0.6.3") (default-features #t) (kind 0)))) (hash "0acg0n4f5505c1147kiyy5s4sq7z2bpxx59ybmb9vnj9c95aagfg") (features (quote (("statistics") ("reader") ("default" "reader" "statistics"))))))

(define-public crate-rbook-0.1 (crate (name "rbook") (vers "0.1.4") (deps (list (crate-dep (name "lol_html") (req "^0.3.2") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.38") (default-features #t) (kind 0)) (crate-dep (name "zip") (req "^0.6.3") (default-features #t) (kind 0)))) (hash "13v7mzh07d5p7gk2vhfi2r6jzb20ah903cimsk90dd9rphggwrnx") (features (quote (("statistics") ("reader") ("default" "reader" "statistics"))))))

(define-public crate-rbook-0.1 (crate (name "rbook") (vers "0.1.5") (deps (list (crate-dep (name "lol_html") (req "^0.3.2") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.38") (default-features #t) (kind 0)) (crate-dep (name "zip") (req "^0.6.3") (default-features #t) (kind 0)))) (hash "0ga6rhb93fgrk0lf8y2nh41cbcvif2j3hhsf963ljrkk0jl5zwj0") (features (quote (("statistics") ("reader") ("default" "reader" "statistics"))))))

(define-public crate-rbook-0.2 (crate (name "rbook") (vers "0.2.0") (deps (list (crate-dep (name "lol_html") (req "^0.3.2") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.38") (default-features #t) (kind 0)) (crate-dep (name "zip") (req "^0.6.3") (default-features #t) (kind 0)))) (hash "02dyfaxh48648ghg8zmh40cybjh6gmabggj8rzd4ydnq1scsz9sb") (features (quote (("statistics") ("reader") ("default" "reader" "statistics"))))))

(define-public crate-rbook-0.3 (crate (name "rbook") (vers "0.3.0") (deps (list (crate-dep (name "lol_html") (req "^0.3.2") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.38") (default-features #t) (kind 0)) (crate-dep (name "zip") (req "^0.6.3") (default-features #t) (kind 0)))) (hash "09l3x4m4y6c4jqn0ml9kn8d7rym0yqjf1czj703gvp7q2sy69qzf") (features (quote (("statistics") ("reader") ("default" "reader" "statistics"))))))

(define-public crate-rbook-0.3 (crate (name "rbook") (vers "0.3.1") (deps (list (crate-dep (name "lol_html") (req "^0.3.2") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.38") (default-features #t) (kind 0)) (crate-dep (name "zip") (req "^0.6.3") (default-features #t) (kind 0)))) (hash "00xs92pp829gxfas7bikyyrzk28f8yjpsda1xndjmb11vwcbxra2") (features (quote (("statistics") ("reader") ("default" "reader" "statistics"))))))

(define-public crate-rbook-0.3 (crate (name "rbook") (vers "0.3.2") (deps (list (crate-dep (name "lol_html") (req "^0.3.2") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.38") (default-features #t) (kind 0)) (crate-dep (name "zip") (req "^0.6.3") (default-features #t) (kind 0)))) (hash "0bcwgkbv5pd420jddxm0q132xxslhj2n1v60sg8v7mvbwq60jzji") (features (quote (("statistics") ("reader") ("default" "reader" "statistics"))))))

(define-public crate-rbook-0.4 (crate (name "rbook") (vers "0.4.0") (deps (list (crate-dep (name "lol_html") (req "^0.3.3") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.38") (default-features #t) (kind 0)) (crate-dep (name "zip") (req "^0.6.3") (default-features #t) (kind 0)))) (hash "000r9hq4ljk84qsxmgs2qr2km0krpl9lp5a8n5qvmc6ai2wf57fl") (features (quote (("statistics") ("reader") ("multi-thread") ("default" "reader" "statistics"))))))

(define-public crate-rbook-0.5 (crate (name "rbook") (vers "0.5.0") (deps (list (crate-dep (name "lol_html") (req "^0.3.3") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.38") (default-features #t) (kind 0)) (crate-dep (name "zip") (req "^0.6.3") (default-features #t) (kind 0)))) (hash "0pv0yi7mrrsq8b9fbi5wwddri8xhhv03bhkhih85raraglq3n8gp") (features (quote (("statistics") ("reader") ("multi-thread") ("default" "reader" "statistics"))))))

(define-public crate-rbook-14-cargo-crate-0.1 (crate (name "rbook-14-cargo-crate") (vers "0.1.0") (hash "0c4qxziqfw52hx6snpb9hhld35py08qynfpnzx3fr1z34sic7ggk") (yanked #t)))

(define-public crate-rboot-0.1 (crate (name "rboot") (vers "0.1.0") (deps (list (crate-dep (name "env_logger") (req "^0") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0") (default-features #t) (kind 0)))) (hash "1ab8abvg75dgh7aq6my249lcwnkyc5bs6jkqqlnwlinpkgbfrdbb") (rust-version "1.56")))

(define-public crate-rboot-0.1 (crate (name "rboot") (vers "0.1.1") (deps (list (crate-dep (name "env_logger") (req "^0") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0") (default-features #t) (kind 0)))) (hash "1925d797l2d1ylw46hx715ansilinvn3ss2ay7k9l1xgzw2qaf6i") (rust-version "1.56")))

(define-public crate-rboot-0.1 (crate (name "rboot") (vers "0.1.2") (deps (list (crate-dep (name "env_logger") (req "^0") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0") (default-features #t) (kind 0)))) (hash "1540l57myp0lh7y6v58g0lcmmli7v0grigcb1iiszv512iy2gkjp") (rust-version "1.56")))

