(define-module (crates-io rb ay) #:use-module (crates-io))

(define-public crate-rbay-0.1 (crate (name "rbay") (vers "0.1.0") (deps (list (crate-dep (name "chrono") (req "^0.4.31") (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.11.22") (features (quote ("rustls-tls" "json"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.188") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.107") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.32.0") (features (quote ("full"))) (default-features #t) (kind 2)))) (hash "1ihdydv7cdvzhl5jq9q1h4fbpfi4mh53b4wchqql9nk947bpl2hx")))

