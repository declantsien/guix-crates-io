(define-module (crates-io rb la) #:use-module (crates-io))

(define-public crate-rblake2sum-0.1 (crate (name "rblake2sum") (vers "0.1.0") (deps (list (crate-dep (name "base64") (req "^0.9.0") (default-features #t) (kind 0)) (crate-dep (name "blake2") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "common_failures") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "crev-recursive-digest") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "failure") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "hex") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.2") (default-features #t) (kind 0)))) (hash "1w6zlwq4ximd20fg1lhy2pl6rrd3m1gmvxdan0r8vfxw91p3ik59")))

(define-public crate-rblake2sum-0.2 (crate (name "rblake2sum") (vers "0.2.0") (deps (list (crate-dep (name "base64") (req "^0.9.0") (default-features #t) (kind 0)) (crate-dep (name "blake2") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "common_failures") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "crev-recursive-digest") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "failure") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "hex") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.2") (default-features #t) (kind 0)))) (hash "1w76kjs7yfkafk6wi66pxlikh1f5ayzh7a5jqc3gvan3q8hvnqz4")))

(define-public crate-rblake2sum-0.2 (crate (name "rblake2sum") (vers "0.2.1") (deps (list (crate-dep (name "base64") (req "^0.11") (default-features #t) (kind 0)) (crate-dep (name "blake2") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "common_failures") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "crev-recursive-digest") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "hex") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.3") (default-features #t) (kind 0)))) (hash "0k0907wjwsc92f27jk3dnb71zswar7vhirf9dyx539asd88738lw")))

(define-public crate-rblake2sum-0.3 (crate (name "rblake2sum") (vers "0.3.0") (deps (list (crate-dep (name "base64") (req "^0.21") (default-features #t) (kind 0)) (crate-dep (name "blake2") (req "^0.10.6") (default-features #t) (kind 0)) (crate-dep (name "common_failures") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "crev-recursive-digest") (req "^0.6.0") (default-features #t) (kind 0)) (crate-dep (name "hex") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.3") (default-features #t) (kind 0)))) (hash "0nvzr1l7cdrh9mlajm5crvislxxy3pjsxbr1b58dimn7mvm1m579")))

(define-public crate-rblake2sum-0.3 (crate (name "rblake2sum") (vers "0.3.1") (deps (list (crate-dep (name "anyhow") (req "^1.0.71") (default-features #t) (kind 0)) (crate-dep (name "base64") (req "^0.21") (default-features #t) (kind 0)) (crate-dep (name "blake2") (req "^0.10.6") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^4.2.7") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "crev-recursive-digest") (req "^0.6.0") (default-features #t) (kind 0)) (crate-dep (name "hex") (req "^0.4") (default-features #t) (kind 0)))) (hash "08pzmd6c7b6dx9pzv1chivp40vfnl3ykcih0726376fng81z52h2")))

(define-public crate-rblake3sum-0.4 (crate (name "rblake3sum") (vers "0.4.0") (deps (list (crate-dep (name "anyhow") (req "^1.0.71") (default-features #t) (kind 0)) (crate-dep (name "base64") (req "^0.21") (default-features #t) (kind 0)) (crate-dep (name "blake3") (req "^1.5.0") (features (quote ("traits-preview"))) (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^4.2.7") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "crev-recursive-digest") (req "^0.6.0") (default-features #t) (kind 0)) (crate-dep (name "hex") (req "^0.4") (default-features #t) (kind 0)))) (hash "16i5jwcryc3bbamsv6y05kq90i10k1zal879lsahpj7x44kzzixx")))

(define-public crate-rblas-0.0.1 (crate (name "rblas") (vers "0.0.1") (deps (list (crate-dep (name "num") (req "*") (default-features #t) (kind 0)))) (hash "12lv3agzjnnfyfn7gnj94x41982h7wqsl9k08rk746hvwaqq0s17")))

(define-public crate-rblas-0.0.2 (crate (name "rblas") (vers "0.0.2") (deps (list (crate-dep (name "num") (req "*") (default-features #t) (kind 0)))) (hash "0v7y6z6n7iqj2srmhr8zaix2wwk947dml3a5qjbwj1asw2v87qri")))

(define-public crate-rblas-0.0.4 (crate (name "rblas") (vers "0.0.4") (deps (list (crate-dep (name "num") (req "*") (default-features #t) (kind 0)))) (hash "09c7bk02wp8i6s6v2gb9bhb1zd3m2miqvyrn9r54zjzhgly9g5c9")))

(define-public crate-rblas-0.0.5 (crate (name "rblas") (vers "0.0.5") (deps (list (crate-dep (name "num") (req "*") (default-features #t) (kind 0)))) (hash "09slmzylwzpavnizsszi6v74l7q0z15mqv7phyk5dvsnyvv40q9a")))

(define-public crate-rblas-0.0.6 (crate (name "rblas") (vers "0.0.6") (deps (list (crate-dep (name "num") (req "*") (default-features #t) (kind 0)))) (hash "0a3pmv21xz4pch3vp2xmfcygg2dj9cd5wmynzmn6zblfzmqpy6cd")))

(define-public crate-rblas-0.0.7 (crate (name "rblas") (vers "0.0.7") (deps (list (crate-dep (name "num") (req "*") (default-features #t) (kind 0)))) (hash "01kawxv6asf4lbb547armls3h52pwkdyafx02v276kvzcni1rb8d")))

(define-public crate-rblas-0.0.8 (crate (name "rblas") (vers "0.0.8") (deps (list (crate-dep (name "libc") (req "*") (default-features #t) (kind 0)) (crate-dep (name "num") (req "*") (default-features #t) (kind 0)))) (hash "1kbkj62w84hyx36hpbnhwgirxyz1221rlbygzxqbg2s11yxnzhbb")))

(define-public crate-rblas-0.0.9 (crate (name "rblas") (vers "0.0.9") (deps (list (crate-dep (name "libc") (req "*") (default-features #t) (kind 0)) (crate-dep (name "num") (req "*") (default-features #t) (kind 0)))) (hash "1gi2wgwvy7931pxflgn3igpqmigbzyzkzixh4ip8r0yr3psidlpv")))

(define-public crate-rblas-0.0.10 (crate (name "rblas") (vers "0.0.10") (deps (list (crate-dep (name "libc") (req "~0.1.10") (default-features #t) (kind 0)) (crate-dep (name "num") (req "~0.1.27") (default-features #t) (kind 0)))) (hash "1jqsrxm5zcz2qpsdib2yivd71g52ir96b2blq54a7w11wfrk3b8j")))

(define-public crate-rblas-0.0.11 (crate (name "rblas") (vers "0.0.11") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "num") (req "^0.1") (default-features #t) (kind 0)))) (hash "00ydr9hixslxw9wykzjiqbspa0jz24bwywck14w5sgbbmjx6k0r1")))

(define-public crate-rblas-0.0.12 (crate (name "rblas") (vers "0.0.12") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "num") (req "^0.1") (features (quote ("complex"))) (kind 0)))) (hash "10hifv4pymij52dhv6z0hf6is87qb9q6pp5cr7lq21x6yrmqiqrr")))

(define-public crate-rblas-0.0.13 (crate (name "rblas") (vers "0.0.13") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "num") (req "^0.1") (features (quote ("complex"))) (kind 0)))) (hash "07c4wrwi6wp6fg2077kzxx3vh72jjfbdwak7yj9hmd63256kmx52")))

