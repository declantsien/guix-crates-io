(define-module (crates-io rb #{64}#) #:use-module (crates-io))

(define-public crate-rb64-0.1 (crate (name "rb64") (vers "0.1.0") (hash "10b2swmpxmcv59ixirvjfq4615h7gialmrgd9sqiz402m7lgjkd4")))

(define-public crate-rb64-0.1 (crate (name "rb64") (vers "0.1.1") (hash "14qjbs12r9c7xc0sdrs32vbpw7cmx6lv35l8ki9xw9b1x0qmwp5v")))

(define-public crate-rb64-0.1 (crate (name "rb64") (vers "0.1.2") (hash "18103kkgi0jzp5js7dbx6g5r1l1xz5hflgr876hfbv06hv4v0xfr")))

(define-public crate-rb64-0.1 (crate (name "rb64") (vers "0.1.3") (hash "0h8f9xh3sp6cgi81lvr38jjqwyi0a5hjvnr6d5a2g08kb3gcb113")))

(define-public crate-rb64-0.1 (crate (name "rb64") (vers "0.1.4") (hash "0kadd3058p6r16aili99x7zw2c527iqhk9jnrihn48srazvpavmh")))

