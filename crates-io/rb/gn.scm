(define-module (crates-io rb gn) #:use-module (crates-io))

(define-public crate-rbgn-0.1 (crate (name "rbgn") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^4.4.11") (features (quote ("derive" "cargo"))) (default-features #t) (kind 0)) (crate-dep (name "linereader") (req "^0.4.0") (default-features #t) (kind 0)))) (hash "0jkrfldqx2kz444843b2x6hzvvyw9ydj7rjwbaqphi9ylj0h88bl") (rust-version "1.75")))

(define-public crate-rbgn-0.1 (crate (name "rbgn") (vers "0.1.1") (deps (list (crate-dep (name "clap") (req "^4.4.11") (features (quote ("derive" "cargo"))) (default-features #t) (kind 0)) (crate-dep (name "linereader") (req "^0.4.0") (default-features #t) (kind 0)))) (hash "0mbn5048031b8gcdzfmhqc58gvpg4x4h5r8kiq4i0gd8hqqlzih5") (rust-version "1.75")))

(define-public crate-rbgn-0.2 (crate (name "rbgn") (vers "0.2.0") (deps (list (crate-dep (name "clap") (req "^4.4.11") (features (quote ("derive" "cargo"))) (default-features #t) (kind 0)) (crate-dep (name "linereader") (req "^0.4.0") (default-features #t) (kind 0)))) (hash "1fnkfcdlbrfjchw0iy1gb0i3agckch0zksijpblyhjrgy9k1nnpd") (rust-version "1.75")))

