(define-module (crates-io rb f-) #:use-module (crates-io))

(define-public crate-rbf-interp-0.1 (crate (name "rbf-interp") (vers "0.1.0") (deps (list (crate-dep (name "nalgebra") (req "^0.18") (default-features #t) (kind 0)))) (hash "01rdkgxzlds5r6bdxx2grwx9pajwwfrq9g36wa9i235w5l4az5im")))

(define-public crate-rbf-interp-0.1 (crate (name "rbf-interp") (vers "0.1.1") (deps (list (crate-dep (name "nalgebra") (req "^0.18") (default-features #t) (kind 0)))) (hash "1965r4hnz813igm6b4pry3qsq241dc033a482xfi9cfxffn59kkn")))

(define-public crate-rbf-interp-0.1 (crate (name "rbf-interp") (vers "0.1.2") (deps (list (crate-dep (name "nalgebra") (req "^0.18") (default-features #t) (kind 0)))) (hash "0d5dqjvr42sl9w1xlfrkskyyrq23494rbzqbhcrc5nkkfzsrvw5x")))

(define-public crate-rbf-interp-0.1 (crate (name "rbf-interp") (vers "0.1.3") (deps (list (crate-dep (name "nalgebra") (req "^0.18") (default-features #t) (kind 0)))) (hash "1zjgxm2vx9p2av8vi24mm4za6plc85pbswiwk3i9i67wsj28bi3r")))

