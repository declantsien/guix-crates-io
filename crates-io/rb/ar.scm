(define-module (crates-io rb ar) #:use-module (crates-io))

(define-public crate-rbar-1 (crate (name "rbar") (vers "1.0.0") (deps (list (crate-dep (name "terminal_size") (req "^0.2.6") (default-features #t) (kind 0)))) (hash "085k6gfzhrm4nq07bj6y9qfvyks7287ycjmahfi6bcjc597iijr9") (yanked #t)))

(define-public crate-rbar-1 (crate (name "rbar") (vers "1.0.1") (deps (list (crate-dep (name "terminal_size") (req "^0.2.6") (default-features #t) (kind 0)))) (hash "125z7z5z855f4n5cji0pqdpfjhkiy25ffpwfi73bq28jzy3a9bd1") (yanked #t)))

(define-public crate-rbar-1 (crate (name "rbar") (vers "1.0.2") (deps (list (crate-dep (name "terminal_size") (req "^0.2.6") (default-features #t) (kind 0)))) (hash "167vq5f47fjfzppq805yskhms84hq3znw9yxgwr2gagj7bsgif9c") (yanked #t)))

(define-public crate-rbar-1 (crate (name "rbar") (vers "1.0.3") (deps (list (crate-dep (name "terminal_size") (req "^0.2.6") (default-features #t) (kind 0)))) (hash "11sspdyvw4ankwyknj7lfw44xkqbyfb7l17jd7j1x5nw94vc71c8") (yanked #t)))

(define-public crate-rbar-1 (crate (name "rbar") (vers "1.0.4") (deps (list (crate-dep (name "terminal_size") (req "^0.2.6") (default-features #t) (kind 0)))) (hash "1akmbxdid4mb9r9zad76p7psnyv9sp3d32faaakm49zp4g58c9cl") (yanked #t)))

(define-public crate-rbar-1 (crate (name "rbar") (vers "1.1.0") (deps (list (crate-dep (name "terminal_size") (req "^0.2.6") (default-features #t) (kind 0)))) (hash "0bscsqji6llqs7m8vbglvi34176y0z5p7n5qxq1sfv4caag7jg7i") (yanked #t)))

(define-public crate-rbar-1 (crate (name "rbar") (vers "1.1.1") (deps (list (crate-dep (name "terminal_size") (req "^0.2.6") (default-features #t) (kind 0)))) (hash "1lc35lp2y3skd2kk6wizpl105prcx1sl9hh8rnfacj4zz551ip57") (yanked #t)))

(define-public crate-rbar-0.0.0 (crate (name "rbar") (vers "0.0.0") (deps (list (crate-dep (name "terminal_size") (req "^0.2.6") (default-features #t) (kind 0)))) (hash "1c4q13x4a2pax98kia4x71ljcgyfnwmm6gqxk346f23lahhj4p11")))

(define-public crate-rbar-0.1 (crate (name "rbar") (vers "0.1.0") (deps (list (crate-dep (name "terminal_size") (req "^0.2.6") (default-features #t) (kind 0)))) (hash "11c28dlca1wylblcifksw34ffivkhqic0gcj6w1as891ypna411j")))

(define-public crate-rbar-0.1 (crate (name "rbar") (vers "0.1.1") (deps (list (crate-dep (name "terminal_size") (req "^0.2.6") (default-features #t) (kind 0)))) (hash "0v656vih5pb6b5ygbhqh210z9im6s07a64mckhj2fdz633z36227")))

(define-public crate-rbar-0.1 (crate (name "rbar") (vers "0.1.2") (deps (list (crate-dep (name "terminal_size") (req "^0.2.6") (default-features #t) (kind 0)))) (hash "0i6pfhc32yfs6jflq9d2izn7v784cq2d1ns9zdradpgmbn3cvcps")))

(define-public crate-rbar-0.2 (crate (name "rbar") (vers "0.2.0") (deps (list (crate-dep (name "termcolor") (req "^1.2.0") (default-features #t) (kind 0)) (crate-dep (name "terminal_size") (req "^0.2.6") (default-features #t) (kind 0)))) (hash "1s1babdl89j218q0n2cnzjmi3pp2wp80x697yljayapl5s9vlrh8")))

(define-public crate-rbar-0.2 (crate (name "rbar") (vers "0.2.1") (deps (list (crate-dep (name "terminal_size") (req "^0.2.6") (default-features #t) (kind 0)))) (hash "1d4n14r96x4spad8v2hzkfhjklplir3v6v3cjm7bwx4wzskxxn31")))

(define-public crate-rbar-0.2 (crate (name "rbar") (vers "0.2.2") (deps (list (crate-dep (name "terminal_size") (req "^0.2.6") (default-features #t) (kind 0)))) (hash "0pgw5wm07h93mfa49pl9srz7hd3nwshgjbnmpprrg8j5djich7iy")))

