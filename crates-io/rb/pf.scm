(define-module (crates-io rb pf) #:use-module (crates-io))

(define-public crate-rbpf-0.0.1 (crate (name "rbpf") (vers "0.0.1") (deps (list (crate-dep (name "libc") (req "^0.2.0") (default-features #t) (kind 0)))) (hash "1gbl08vc1gdm8dgsl17cprp5d7f6zr50h1lpn7w93z94ng6jilql")))

(define-public crate-rbpf-0.0.2 (crate (name "rbpf") (vers "0.0.2") (deps (list (crate-dep (name "libc") (req "^0.2.0") (default-features #t) (kind 0)))) (hash "19p4xm5mv00cbrjf9imkwsf00sff3k44nhw3f9laikwchwjdg1ja")))

(define-public crate-rbpf-0.1 (crate (name "rbpf") (vers "0.1.0") (deps (list (crate-dep (name "byteorder") (req "^1.2") (default-features #t) (kind 0)) (crate-dep (name "combine") (req "^2.5") (default-features #t) (kind 0)) (crate-dep (name "elf") (req "^0.0.10") (default-features #t) (kind 2)) (crate-dep (name "json") (req "^0.11") (default-features #t) (kind 2)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "time") (req "^0.1") (default-features #t) (kind 0)))) (hash "1i6ij4bscki9jazl73k3abid6pq2hgpyb8kyw7nx54ks5qb1374w")))

(define-public crate-rbpf-0.2 (crate (name "rbpf") (vers "0.2.0") (deps (list (crate-dep (name "byteorder") (req "^1.2") (default-features #t) (kind 0)) (crate-dep (name "combine") (req "^2.5") (default-features #t) (kind 0)) (crate-dep (name "elf") (req "^0.0.10") (default-features #t) (kind 2)) (crate-dep (name "hex") (req "^0.4.3") (default-features #t) (kind 2)) (crate-dep (name "json") (req "^0.11") (default-features #t) (kind 2)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "time") (req "^0.2") (default-features #t) (kind 0)))) (hash "0rhwy28n50m0a3qxb6vs18j6dkfczgqiv93qql30swrsgrfdqdmm")))

