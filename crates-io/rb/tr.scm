(define-module (crates-io rb tr) #:use-module (crates-io))

(define-public crate-rbtree-0.1 (crate (name "rbtree") (vers "0.1.0") (hash "1f280z7qxd0pfj39pzzw24qvy9ad2qrs8yx09yf748vsr4alx7z5")))

(define-public crate-rbtree-0.1 (crate (name "rbtree") (vers "0.1.1") (hash "0pfskwparvjzcw4ss9fyhr0vrl8h9n6zvbswx3lbxxzfwki7yxx1")))

(define-public crate-rbtree-0.1 (crate (name "rbtree") (vers "0.1.2") (hash "15jsbzxk30ph9agzwygfy91grzmlp8vaz7nigrqjr8rhdw1g0qgp")))

(define-public crate-rbtree-0.1 (crate (name "rbtree") (vers "0.1.3") (hash "06pvqwgyy7m2ckxzn142nrazblh1fafy6cgsn85qizb6p11x9gk6")))

(define-public crate-rbtree-0.1 (crate (name "rbtree") (vers "0.1.4") (hash "0aps0bsknds303cd526420a141j20cgnms71jsb7xz8dfi611nf9")))

(define-public crate-rbtree-0.1 (crate (name "rbtree") (vers "0.1.5") (hash "0q279mnnlij9gn9pwwc7wg76w93sc1zsnxrmkg71vmwdp2zh40y3")))

(define-public crate-rbtree-0.1 (crate (name "rbtree") (vers "0.1.6") (hash "0r75nyfj5p3f5zd7skwvm31ygc9gahg03c4ra54as6x8ki63dcza")))

(define-public crate-rbtree-0.1 (crate (name "rbtree") (vers "0.1.7") (hash "0nkcdkzfpic7974k4lsga0gbmh528yqgfw545qp8pfww5hrc87qc")))

(define-public crate-rbtree-0.2 (crate (name "rbtree") (vers "0.2.0") (hash "133h0cmx03cyhpx85wb21c95g361vf6hlqbq1lj7icfvykwhjk81")))

(define-public crate-rbtree-arena-0.1 (crate (name "rbtree-arena") (vers "0.1.0") (deps (list (crate-dep (name "thiserror") (req "^1.0.40") (default-features #t) (kind 0)))) (hash "0g6bv2dliyqa74h29wb53ghjkqghs70njwy3h35v2ilwi39ybcnm")))

(define-public crate-rbtree-defrag-buffer-0.1 (crate (name "rbtree-defrag-buffer") (vers "0.1.0") (deps (list (crate-dep (name "anyhow") (req "^1.0.75") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^4.4.4") (features (quote ("derive"))) (default-features #t) (kind 2)) (crate-dep (name "intrusive-collections") (req "^0.9.6") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 2)))) (hash "08mpd26z10v5db3wcrx5qb64xwlvbjcra4kzb9y8whk818mh3s61")))

