(define-module (crates-io rb sg) #:use-module (crates-io))

(define-public crate-rbsg-backend-0.0.0 (crate (name "rbsg-backend") (vers "0.0.0") (hash "08q3yv79p4k8993dffbkxf6v56czxrg3lzb5p89ay1skzsx0lrzz")))

(define-public crate-rbsg-backend-0.0.1 (crate (name "rbsg-backend") (vers "0.0.1") (hash "16smq887m8x3lc9mk0mhsr4cy63rhdkdaihhiifyp3p3vgmp4bpc")))

(define-public crate-rbsg-frontend-0.0.0 (crate (name "rbsg-frontend") (vers "0.0.0") (hash "1jxjrzy67cp5lwl1rh0hbapg9iagvd8w74d7nmqhi6gh90kd3pzr")))

