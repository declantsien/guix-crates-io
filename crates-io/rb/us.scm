(define-module (crates-io rb us) #:use-module (crates-io))

(define-public crate-rbuster-0.1 (crate (name "rbuster") (vers "0.1.0") (deps (list (crate-dep (name "quicli") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "rayon") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.8") (default-features #t) (kind 0)))) (hash "0n0idylmzqnxqlg8f07h31qiy8ijc609nsadrrnskz5f4fp88v3l")))

(define-public crate-rbuster-0.1 (crate (name "rbuster") (vers "0.1.1") (deps (list (crate-dep (name "quicli") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "rayon") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.8") (default-features #t) (kind 0)))) (hash "0jsjpg07vkzz9ildqcdk4mz4idzjqxa7m2yr62acpz42g9yjkwy0")))

(define-public crate-rbuster-0.1 (crate (name "rbuster") (vers "0.1.2") (deps (list (crate-dep (name "quicli") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "rayon") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.8") (default-features #t) (kind 0)))) (hash "0jy7nb3h3p98bab1s5llgq5rhjvf56mgawwbzs9vxj66s6vh0w83")))

(define-public crate-rbuster-0.2 (crate (name "rbuster") (vers "0.2.0") (deps (list (crate-dep (name "quicli") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "rayon") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.9") (default-features #t) (kind 0)))) (hash "1a0jwn95rsrcyb83fars3bxpm4dlrnama2h2mnfzqm6zmzj0f7rh")))

(define-public crate-rbuster-0.2 (crate (name "rbuster") (vers "0.2.1") (deps (list (crate-dep (name "nanoid") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "quicli") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "rayon") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.9") (default-features #t) (kind 0)))) (hash "0f1fcrj19g8fxwsph9ri3vz125iqqni6safp8abld1k6wqdzldir")))

(define-public crate-rbuster-0.3 (crate (name "rbuster") (vers "0.3.0") (deps (list (crate-dep (name "futures") (req "^0.3.10") (default-features #t) (kind 0)) (crate-dep (name "nanoid") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "quicli") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.11") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "0kvs0da1zdi28wnwg4l2khmpxmgn1hckli8h1fdxm7ncmzdbi5mr")))

