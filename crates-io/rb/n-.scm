(define-module (crates-io rb n-) #:use-module (crates-io))

(define-public crate-rbn-lib-0.1 (crate (name "rbn-lib") (vers "0.1.3") (deps (list (crate-dep (name "failure") (req "^0.1.8") (default-features #t) (kind 0)) (crate-dep (name "hambands") (req "^0.1.1") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.4.3") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.125") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "1ns5qjfd5029chylw4qcwls4lrclvcmhxr0hc7n5gwfc81yz4253")))

(define-public crate-rbn-lib-0.1 (crate (name "rbn-lib") (vers "0.1.4") (deps (list (crate-dep (name "failure") (req "^0.1.8") (default-features #t) (kind 0)) (crate-dep (name "hambands") (req "^0.1.1") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.4.3") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.125") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "0q2g68wnvvqc0njiprcz6zn4apyd6sjmwsl5sy4y6qwlmnrl7mqg")))

