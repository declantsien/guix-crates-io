(define-module (crates-io rb _t) #:use-module (crates-io))

(define-public crate-rb_tree-0.1 (crate (name "rb_tree") (vers "0.1.0") (hash "0mbyngmjzmwsmknhxabw5mk6plljfpjqfqk059lljd0pbaz2zf51")))

(define-public crate-rb_tree-0.1 (crate (name "rb_tree") (vers "0.1.1") (hash "092yvqapbwy1w91y44zxr12n8mssk1gvrpc2qbf6vk8gaw8lx9fj")))

(define-public crate-rb_tree-0.2 (crate (name "rb_tree") (vers "0.2.0") (hash "1a9d747k4qh3zbjg9gz1q61n30wmlq5s1mj775gj8w49yrnrzkwh")))

(define-public crate-rb_tree-0.3 (crate (name "rb_tree") (vers "0.3.0") (hash "13w4mgscs7six9j4hkqvrprwpmabivbb9mdfgbm3zl36gfpc05gn")))

(define-public crate-rb_tree-0.3 (crate (name "rb_tree") (vers "0.3.1") (hash "0pa9abrx5l2d6w2y7smpnmlcmzyayx211h99padswb67b3zdrxic")))

(define-public crate-rb_tree-0.3 (crate (name "rb_tree") (vers "0.3.2") (hash "0rigzc92pv9kqb0bxcvvx99jcpc4ycsh907yhsmid6zxkzfppqcq")))

(define-public crate-rb_tree-0.3 (crate (name "rb_tree") (vers "0.3.3") (hash "0r69vfh612a6pbzlk8pvhsc32s7ff8gjgh28dwnqrrz4rk1lwbl9") (yanked #t)))

(define-public crate-rb_tree-0.3 (crate (name "rb_tree") (vers "0.3.4") (hash "0iiyph8rwhwrcmqc41bgr10yb14jb3qh0agkqdxv79zb5y9sxg7l") (yanked #t)))

(define-public crate-rb_tree-0.3 (crate (name "rb_tree") (vers "0.3.5") (hash "0lakgn665m7grg9w4bj318pi1cwbwpyksrz3807x0jgkj0zk1qd7")))

(define-public crate-rb_tree-0.4 (crate (name "rb_tree") (vers "0.4.0") (deps (list (crate-dep (name "fnv") (req "^1.0.7") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.8.3") (default-features #t) (kind 2)) (crate-dep (name "rand_chacha") (req "^0.3.0") (default-features #t) (kind 2)))) (hash "1wqkqcp1882ay191n8f1h73nrzqqjz6d651395rwpx15qav1qh23")))

(define-public crate-rb_tree-0.4 (crate (name "rb_tree") (vers "0.4.1") (deps (list (crate-dep (name "criterion") (req "^0.3.4") (default-features #t) (kind 2)) (crate-dep (name "fnv") (req "^1.0.7") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.8.3") (default-features #t) (kind 2)) (crate-dep (name "rand_chacha") (req "^0.3.0") (default-features #t) (kind 2)))) (hash "09acvan4c8s04crh3r5iqlnbpgi4pxlwabvkn839ax1j65lp8qmk") (features (quote (("set") ("queue") ("map" "set") ("default" "set" "queue" "map"))))))

(define-public crate-rb_tree-0.5 (crate (name "rb_tree") (vers "0.5.0") (deps (list (crate-dep (name "criterion") (req "^0.3.4") (default-features #t) (kind 2)) (crate-dep (name "fnv") (req "^1.0.7") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.8.3") (default-features #t) (kind 2)) (crate-dep (name "rand_chacha") (req "^0.3.0") (default-features #t) (kind 2)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 2)))) (hash "1zmfn5ajzgf2vld6r666753kib897mcpcgsdbcaw9q9kw1w4qwp6") (features (quote (("set") ("queue") ("map" "set") ("default" "set" "queue" "map"))))))

