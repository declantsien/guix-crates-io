(define-module (crates-io rb sn) #:use-module (crates-io))

(define-public crate-rbsnrust-0.1 (crate (name "rbsnrust") (vers "0.1.0") (deps (list (crate-dep (name "robson_compiler") (req "^0.1.4") (default-features #t) (kind 0)) (crate-dep (name "robson_compiler") (req "^0.1.4") (default-features #t) (kind 1)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 1)) (crate-dep (name "toml") (req "^0.4.2") (default-features #t) (kind 1)))) (hash "1cslsm269253zdidpj2k7hgs4nlw9z7imbjgarw5fbf1ikv2dy13")))

