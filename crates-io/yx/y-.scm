(define-module (crates-io yx y-) #:use-module (crates-io))

(define-public crate-yxy-cli-0.1 (crate (name "yxy-cli") (vers "0.1.0-alpha.1") (deps (list (crate-dep (name "clap") (req "^3.2") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_yaml") (req "^0.9") (default-features #t) (kind 0)) (crate-dep (name "yxy") (req "^0.2.0-alpha.1") (default-features #t) (kind 0)))) (hash "02v4g4rsi02d8fvfp8zslmdnammvylirqhdmaxwz2byi49fzkcpg")))

(define-public crate-yxy-cli-0.1 (crate (name "yxy-cli") (vers "0.1.0-alpha.2") (deps (list (crate-dep (name "clap") (req "^3.2") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_yaml") (req "^0.9") (default-features #t) (kind 0)) (crate-dep (name "yxy") (req "^0.2.0-alpha.2") (default-features #t) (kind 0)))) (hash "0ga5g2n8y6xr1x5mh2gaw0qdvb44l8igr6vkcx70al21r0087wg7")))

(define-public crate-yxy-cli-0.1 (crate (name "yxy-cli") (vers "0.1.0-alpha.3") (deps (list (crate-dep (name "clap") (req "^3.2") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_yaml") (req "^0.9") (default-features #t) (kind 0)) (crate-dep (name "yxy") (req "^0.2.0-alpha.3") (default-features #t) (kind 0)))) (hash "17f0rpbyz3kk13m35snwvppi86v3xxnwscy9wk6g01k48fyhr1pv")))

(define-public crate-yxy-cli-0.1 (crate (name "yxy-cli") (vers "0.1.0-alpha.4") (deps (list (crate-dep (name "clap") (req "^3.2") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_yaml") (req "^0.9") (default-features #t) (kind 0)) (crate-dep (name "yxy") (req "^0.2.0-alpha.3") (default-features #t) (kind 0)))) (hash "10xja22ymhfq61vqg7caqi0qpzh2gf07qr46ss6kjg90svjyd2pb")))

(define-public crate-yxy-cli-0.1 (crate (name "yxy-cli") (vers "0.1.0-alpha.5") (deps (list (crate-dep (name "clap") (req "^3.2") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_yaml") (req "^0.9") (default-features #t) (kind 0)) (crate-dep (name "yxy") (req "^0.2.0-alpha.5") (default-features #t) (kind 0)))) (hash "09hxswi0h0isv5awpb3x0hlj9vxyn4jnc3k8ny5f0dwc45397jns")))

(define-public crate-yxy-cli-0.1 (crate (name "yxy-cli") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^4.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.11") (features (quote ("blocking" "json"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_yaml") (req "^0.9") (default-features #t) (kind 0)) (crate-dep (name "yxy") (req "^0.2.0") (default-features #t) (kind 0)))) (hash "1aqdk1z08jjg5f32b007kxf4xcwp0pm00kjyy843fzzyxyn7hd8r")))

(define-public crate-yxy-cli-0.1 (crate (name "yxy-cli") (vers "0.1.1") (deps (list (crate-dep (name "clap") (req "^4.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.11") (features (quote ("blocking" "json"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_yaml") (req "^0.9") (default-features #t) (kind 0)) (crate-dep (name "yxy") (req "^0.2") (default-features #t) (kind 0)))) (hash "1g0lhs4zszbxf22cdld5620k6kc7xq7vf4zjj9k6yzrab6y1hll2")))

(define-public crate-yxy-cli-0.2 (crate (name "yxy-cli") (vers "0.2.0") (hash "1mcy4qgv0w1wym08m2xfplz1zd423ah6v41drrff4sa8v4f8zliz")))

(define-public crate-yxy-ffi-0.1 (crate (name "yxy-ffi") (vers "0.1.0-alpha.1") (deps (list (crate-dep (name "yxy") (req "^0.2.0-alpha.1") (default-features #t) (kind 0)))) (hash "1gxfbd96dli024ygsy71dxihqij2ixm73xf3g7lazmpn2arfxzkh") (yanked #t)))

(define-public crate-yxy-ffi-0.1 (crate (name "yxy-ffi") (vers "0.1.0-alpha.2") (deps (list (crate-dep (name "yxy") (req "^0.2.0-alpha.2") (default-features #t) (kind 0)))) (hash "1ipsnqjws1lsqc158rrgs4alq4ghm9ravj63211vyqyb0458sic7") (yanked #t)))

(define-public crate-yxy-ffi-0.1 (crate (name "yxy-ffi") (vers "0.1.0-alpha.3") (deps (list (crate-dep (name "yxy") (req "^0.2.0-alpha.3") (default-features #t) (kind 0)))) (hash "0n2l678jlhxdc9d62vwy70j2kvlqvx7h1a3pjcblvx2ddazyhdmx") (yanked #t)))

(define-public crate-yxy-ffi-0.1 (crate (name "yxy-ffi") (vers "0.1.0-alpha.4") (deps (list (crate-dep (name "yxy") (req "^0.2.0-alpha.3") (default-features #t) (kind 0)))) (hash "1iqs9kymspihhrhryqjxc4d7hy322hk6jici9vyfzr09z94hs874") (yanked #t)))

(define-public crate-yxy-ffi-0.1 (crate (name "yxy-ffi") (vers "0.1.0-alpha.5") (deps (list (crate-dep (name "yxy") (req "^0.2.0-alpha.5") (default-features #t) (kind 0)))) (hash "13a7x92x2v7rsmc6s2d08mvrn0m4ys9p12l5qvyp97qxm0yi21pc") (yanked #t)))

(define-public crate-yxy-ffi-0.1 (crate (name "yxy-ffi") (vers "0.1.0") (hash "08mdy3mlicj9cvjnircxm2dgk9f5axrs7pap9vnxd8fr936fpd77")))

