(define-module (crates-io f4 ju) #:use-module (crates-io))

(define-public crate-f4jumble-0.0.0 (crate (name "f4jumble") (vers "0.0.0") (hash "0b5ccb06fc3ak0ncjj14fv59kh39k2xqvnrmzm2af9chwl4yxmyg")))

(define-public crate-f4jumble-0.1 (crate (name "f4jumble") (vers "0.1.0") (deps (list (crate-dep (name "blake2b_simd") (req "^1") (kind 0)) (crate-dep (name "hex") (req "^0.4") (default-features #t) (kind 2)) (crate-dep (name "proptest") (req "^1") (default-features #t) (kind 2)))) (hash "0xgwbbhazppc9b3scxm7v2g6kqj1zv4vg4yqmbs6llhczpbyi0qa") (features (quote (("std" "blake2b_simd/std") ("default" "std"))))))

