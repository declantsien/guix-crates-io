(define-module (crates-io vm f_) #:use-module (crates-io))

(define-public crate-vmf_parser_nom-0.1 (crate (name "vmf_parser_nom") (vers "0.1.0") (deps (list (crate-dep (name "nom") (req "^7") (default-features #t) (kind 0)) (crate-dep (name "traversal") (req "^0.1.2") (default-features #t) (kind 2)))) (hash "0rfmf1pdlksfjvvpldhw6mhqs7jchvrwz0szj1807831gwf2j4iy")))

(define-public crate-vmf_parser_nom-0.1 (crate (name "vmf_parser_nom") (vers "0.1.1") (deps (list (crate-dep (name "nom") (req "^7") (default-features #t) (kind 0)) (crate-dep (name "traversal") (req "^0.1.2") (default-features #t) (kind 2)))) (hash "1rs585m6cmrqb5flc8q971d935224nm8g6hp9l7mq4jd2b5wqz15")))

