(define-module (crates-io vm e-) #:use-module (crates-io))

(define-public crate-vme-pkix-0.1 (crate (name "vme-pkix") (vers "0.1.0") (deps (list (crate-dep (name "lazy_static") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "pkix") (req "^0.1.1") (default-features #t) (kind 0)))) (hash "10dq5jy7l332j7m0vfba19gvlnh827w7p3nj0sm55sz6mhkd7500")))

