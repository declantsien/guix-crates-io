(define-module (crates-io vm st) #:use-module (crates-io))

(define-public crate-vmstat-1 (crate (name "vmstat") (vers "1.6.0") (deps (list (crate-dep (name "clap") (req "^4.3.4") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "prettytable-rs") (req "^0.10") (default-features #t) (kind 0)) (crate-dep (name "ssh2") (req "^0.9.4") (default-features #t) (kind 0)))) (hash "1r4j41fd5amr1b9jan1wh4ik44qjarjcjncvk94m4m6810krc30j") (yanked #t)))

