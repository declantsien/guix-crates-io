(define-module (crates-io vm d_) #:use-module (crates-io))

(define-public crate-vmd_parser-0.1 (crate (name "vmd_parser") (vers "0.1.0") (deps (list (crate-dep (name "byteorder") (req "^1.5") (default-features #t) (kind 0)) (crate-dep (name "encoding_rs") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0") (default-features #t) (kind 0)))) (hash "1r5qh9d1d7m6i62nml2r5hzqcpr37aqm3b5cykzrbqklvr1dgzgx")))

