(define-module (crates-io vm od) #:use-module (crates-io))

(define-public crate-vmode-0.1 (crate (name "vmode") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^2.33.3") (default-features #t) (kind 0)))) (hash "01p9q4vp18z11zh63r7iykshpil819rkbn39i8f47r8y5ys1afnf")))

