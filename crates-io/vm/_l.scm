(define-module (crates-io vm _l) #:use-module (crates-io))

(define-public crate-vm_lang-0.1 (crate (name "vm_lang") (vers "0.1.0") (deps (list (crate-dep (name "lalrpop") (req "^0.19.7") (default-features #t) (kind 1)) (crate-dep (name "lalrpop-util") (req "^0.19") (features (quote ("lexer"))) (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0") (default-features #t) (kind 0)))) (hash "0z0xswq1n0s90cama979zm2vb3z97xs2gp0avmb7zqfsfmngj76c") (yanked #t)))

(define-public crate-vm_lang-0.1 (crate (name "vm_lang") (vers "0.1.1") (deps (list (crate-dep (name "lalrpop") (req "^0.19.7") (default-features #t) (kind 1)) (crate-dep (name "lalrpop-util") (req "^0.19") (features (quote ("lexer"))) (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0") (default-features #t) (kind 0)))) (hash "0d2jh1lnzwin7319ynsyk1iihsdisqsf1qn6c3jd19a5vb39nm8s")))

