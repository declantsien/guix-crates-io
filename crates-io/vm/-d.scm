(define-module (crates-io vm -d) #:use-module (crates-io))

(define-public crate-vm-detect-0.1 (crate (name "vm-detect") (vers "0.1.0") (deps (list (crate-dep (name "bitflags") (req "^2.4.0") (default-features #t) (kind 0)))) (hash "0jks0l1r41c1ih012h88bq6bczbd42macypb1da5898whwnnxcc0")))

(define-public crate-vm-device-0.1 (crate (name "vm-device") (vers "0.1.0") (hash "1dlfi9s56f3ga8j88hrmvb69jd3zwjwd51f04g54fjgavpddp6jr")))

