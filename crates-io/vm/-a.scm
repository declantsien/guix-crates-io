(define-module (crates-io vm -a) #:use-module (crates-io))

(define-public crate-vm-allocator-0.1 (crate (name "vm-allocator") (vers "0.1.0") (deps (list (crate-dep (name "libc") (req ">=0.2.39") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0") (default-features #t) (kind 0)))) (hash "1r25bljzv5fc5yjwl5i47jlg5d3xm683s9699vrkn6yxny36hnsn")))

(define-public crate-vm-allocator-0.1 (crate (name "vm-allocator") (vers "0.1.1") (deps (list (crate-dep (name "libc") (req "^0.2.39") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0") (default-features #t) (kind 0)))) (hash "0h92f57bzbax88hwp5qmjzb1nsssf5zy4qvkfjqp93afplcffk2y")))

