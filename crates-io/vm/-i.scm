(define-module (crates-io vm #{-i}#) #:use-module (crates-io))

(define-public crate-vm-info-0.1 (crate (name "vm-info") (vers "0.1.0") (deps (list (crate-dep (name "byteorder") (req "^1.2.1") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.0.0") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2.36") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^0.2.5") (default-features #t) (kind 0)))) (hash "1wzwbnkyhmlg1lfya4g965x7yjparnay332vwjbyvrw62mxk70wx")))

(define-public crate-vm-info-0.1 (crate (name "vm-info") (vers "0.1.1") (deps (list (crate-dep (name "byteorder") (req "^1.3.4") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2.71") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.3.9") (default-features #t) (kind 0)))) (hash "0jc4jq9zdbp9iq2akfz46fc223q5yjld9r5ligdgl3d43nd5y8cp")))

(define-public crate-vm-info-0.2 (crate (name "vm-info") (vers "0.2.0") (deps (list (crate-dep (name "byteorder") (req "^1.3.4") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2.77") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.3.9") (default-features #t) (kind 0)))) (hash "1c04w4zcwrai3zhz247i8bv90v10kp52wqrmwgkwxn6q9dh862wy")))

