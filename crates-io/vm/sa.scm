(define-module (crates-io vm sa) #:use-module (crates-io))

(define-public crate-vmsavedstatedump_rs-0.1 (crate (name "vmsavedstatedump_rs") (vers "0.1.0") (deps (list (crate-dep (name "kernel32-sys") (req "^0.2.2") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2.44") (default-features #t) (kind 0)) (crate-dep (name "widestring") (req "^0.4.0") (default-features #t) (kind 0)))) (hash "0fi0hqc4z429r7gvcpv5rh4706xi3bkhy2m88d94lj5nrb1ia03i") (yanked #t)))

(define-public crate-vmsavedstatedump_rs-0.1 (crate (name "vmsavedstatedump_rs") (vers "0.1.1") (deps (list (crate-dep (name "kernel32-sys") (req "^0.2.2") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2.44") (default-features #t) (kind 0)) (crate-dep (name "widestring") (req "^0.4.0") (default-features #t) (kind 0)))) (hash "0yfblzgmdc641la0zi5nzky8021qh1ag9c9fdnvqbmf8vr1nin73") (yanked #t)))

(define-public crate-vmsavedstatedump_rs-0.1 (crate (name "vmsavedstatedump_rs") (vers "0.1.2") (deps (list (crate-dep (name "libc") (req "^0.2.44") (default-features #t) (kind 0)) (crate-dep (name "widestring") (req "^0.4.0") (default-features #t) (kind 0)) (crate-dep (name "winapi") (req "^0.3.6") (features (quote ("winbase"))) (default-features #t) (kind 0)))) (hash "1rkx477n6yq4nzqwa5b0w51q87ayhv7lmfw97767frzsd7f25808") (yanked #t)))

(define-public crate-vmsavedstatedump_rs-0.1 (crate (name "vmsavedstatedump_rs") (vers "0.1.3") (deps (list (crate-dep (name "widestring") (req "^0.4.0") (default-features #t) (kind 0)) (crate-dep (name "winapi") (req "^0.3.6") (features (quote ("winbase"))) (default-features #t) (kind 0)))) (hash "0zip20ckqhaij2zmpnlf2fyx21d8nsbibn9qkg96ph69m62nvvd4") (yanked #t)))

(define-public crate-vmsavedstatedump_rs-0.2 (crate (name "vmsavedstatedump_rs") (vers "0.2.0") (deps (list (crate-dep (name "widestring") (req "^0.4.0") (default-features #t) (kind 0)) (crate-dep (name "winapi") (req "^0.3.6") (features (quote ("winbase"))) (default-features #t) (kind 0)))) (hash "1igjmgjj95b4aszfy40szpc93rd7f7lb3kz02s7jhzz2wp7l2ccg")))

