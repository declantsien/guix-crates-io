(define-module (crates-io vm bc) #:use-module (crates-io))

(define-public crate-vmbc-sys-0.1 (crate (name "vmbc-sys") (vers "0.1.0") (deps (list (crate-dep (name "anyhow") (req "^1") (default-features #t) (kind 2)) (crate-dep (name "libloading") (req "^0.8.3") (default-features #t) (kind 0)) (crate-dep (name "windows") (req "^0.54.0") (features (quote ("Win32_System_LibraryLoader"))) (default-features #t) (target "cfg(target_os = \"windows\")") (kind 0)))) (hash "1qdn5wfkv8895cwjg839l7c0fkllh7q8zr1vlvvd81l9hi29xmyl") (rust-version "1.56")))

(define-public crate-vmbc-sys-0.1 (crate (name "vmbc-sys") (vers "0.1.1") (deps (list (crate-dep (name "anyhow") (req "^1") (default-features #t) (kind 2)) (crate-dep (name "libloading") (req "^0.8.3") (default-features #t) (kind 0)) (crate-dep (name "windows") (req "^0.54.0") (features (quote ("Win32_System_LibraryLoader"))) (default-features #t) (target "cfg(target_os = \"windows\")") (kind 0)))) (hash "14gq62axzizfw6mcbjzdqn8iph8yb3lmmmm9i4va2mhsf1xyxq00") (rust-version "1.56")))

