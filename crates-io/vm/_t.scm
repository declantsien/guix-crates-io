(define-module (crates-io vm _t) #:use-module (crates-io))

(define-public crate-vm_test_fixture-0.1 (crate (name "vm_test_fixture") (vers "0.1.0") (deps (list (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 1)) (crate-dep (name "simple-logging") (req "^2") (default-features #t) (kind 1)) (crate-dep (name "vm_runner") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "vm_runner") (req "^0.1") (default-features #t) (kind 1)))) (hash "1h1a95y4wm7nj73r5lh1a437f6m313hyrbgi24ic9nkpixcg7jc4")))

(define-public crate-vm_test_fixture-0.1 (crate (name "vm_test_fixture") (vers "0.1.1") (deps (list (crate-dep (name "fs2") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "simple-logging") (req "^2") (default-features #t) (kind 0)) (crate-dep (name "vm_runner") (req "^0.1") (default-features #t) (kind 0)))) (hash "06a0nk94vvy6ni3zm1f5vhpyqvspgmfv0vp46ajzkymwg54672sv")))

(define-public crate-vm_translator-0.1 (crate (name "vm_translator") (vers "0.1.0") (deps (list (crate-dep (name "phf") (req "^0.11.2") (features (quote ("macros"))) (default-features #t) (kind 0)))) (hash "1agx5rqy2whlh37b0lfqvqjmwmwp4ag5951mxhxfval2r4qvxl20")))

(define-public crate-vm_translator-0.1 (crate (name "vm_translator") (vers "0.1.1") (deps (list (crate-dep (name "phf") (req "^0.11.2") (features (quote ("macros"))) (default-features #t) (kind 0)))) (hash "0inxp88z486s69n74g20rw3r8715hcq0hhjpqy8yf8a0i2mf8ggr")))

(define-public crate-vm_translator-0.1 (crate (name "vm_translator") (vers "0.1.2") (deps (list (crate-dep (name "phf") (req "^0.11.2") (features (quote ("macros"))) (default-features #t) (kind 0)))) (hash "16gpby4cpzp8nppv1crg3bxnsppk6h6lpr43rxcbr5whvq6jcw0r")))

