(define-module (crates-io vm ml) #:use-module (crates-io))

(define-public crate-vmml-0.1 (crate (name "vmml") (vers "0.1.0") (deps (list (crate-dep (name "pest") (req "^2.4") (default-features #t) (kind 0)) (crate-dep (name "pest_derive") (req "^2.4") (default-features #t) (kind 0)))) (hash "1lhxkh26myl19q39hgb9drkn6dpsjq81hppcar24krlwb8k06xy2")))

(define-public crate-vmml-1 (crate (name "vmml") (vers "1.0.0") (deps (list (crate-dep (name "pest") (req "^2.4") (default-features #t) (kind 0)) (crate-dep (name "pest_derive") (req "^2.4") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.145") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "0fliy57pkaxpi64dczmd769mi75klabxc8kph1w39jiajcbx4y9d")))

(define-public crate-vmml-1 (crate (name "vmml") (vers "1.0.1") (deps (list (crate-dep (name "pest") (req "^2.4") (default-features #t) (kind 0)) (crate-dep (name "pest_derive") (req "^2.4") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.145") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "08vd8hnqld4r880xpcmx732hsq8gwbyqfs4j2pnb0rb1x8qajrwy")))

