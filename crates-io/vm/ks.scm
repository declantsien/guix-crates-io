(define-module (crates-io vm ks) #:use-module (crates-io))

(define-public crate-vmks-exam-generator-0.1 (crate (name "vmks-exam-generator") (vers "0.1.3") (deps (list (crate-dep (name "rand") (req "^0.7.3") (default-features #t) (kind 0)) (crate-dep (name "rand_pcg") (req "^0.2.1") (default-features #t) (kind 0)))) (hash "1yb6nqljfaz21dvrrrjbm7yy1bmpyf47c34qvrwvzhrhvrriyqp7")))

(define-public crate-vmks-exam-generator-0.1 (crate (name "vmks-exam-generator") (vers "0.1.4") (deps (list (crate-dep (name "rand") (req "^0.7.3") (default-features #t) (kind 0)) (crate-dep (name "rand_pcg") (req "^0.2.1") (default-features #t) (kind 0)))) (hash "0b8r0g88s5zbfc44badjr5i46bwy2x65xrii1qdy2jl7ghs10w0i")))

(define-public crate-vmks-exam-generator-1 (crate (name "vmks-exam-generator") (vers "1.0.0") (deps (list (crate-dep (name "clap") (req "^2.33.3") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.7.3") (default-features #t) (kind 0)) (crate-dep (name "rand_pcg") (req "^0.2.1") (default-features #t) (kind 0)) (crate-dep (name "xml-rs") (req "^0.8.4") (default-features #t) (kind 0)))) (hash "1xc8gs2d07f96il030wc1a50l51df31yw7giplap6x7fd8m1ii19")))

(define-public crate-vmks-exam-generator-1 (crate (name "vmks-exam-generator") (vers "1.0.1") (deps (list (crate-dep (name "clap") (req "^2.33.3") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.7.3") (default-features #t) (kind 0)) (crate-dep (name "rand_pcg") (req "^0.2.1") (default-features #t) (kind 0)) (crate-dep (name "xml-rs") (req "^0.8.4") (default-features #t) (kind 0)))) (hash "1dq8q6sb0cm2y222k07wnw11qkx2zifxgwy0yy9krw7mvn1sqkg8")))

(define-public crate-vmks-exam-generator-1 (crate (name "vmks-exam-generator") (vers "1.3.0") (deps (list (crate-dep (name "clap") (req "^4.4.16") (default-features #t) (kind 0)) (crate-dep (name "latex") (req "^0.3.1") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)) (crate-dep (name "rand_pcg") (req "^0.3.1") (default-features #t) (kind 0)) (crate-dep (name "xml-rs") (req "^0.8.19") (default-features #t) (kind 0)))) (hash "0df31v2cffdpiynkihrcyajmzhy5a39ybp340ahdcj0y4ajdvxd6")))

