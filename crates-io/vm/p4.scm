(define-module (crates-io vm p4) #:use-module (crates-io))

(define-public crate-vmp4-dump-1 (crate (name "vmp4-dump") (vers "1.0.0") (deps (list (crate-dep (name "byteorder") (req "^1.4.3") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^3.1.6") (default-features #t) (kind 0)) (crate-dep (name "flate2") (req "^1.0.22") (default-features #t) (kind 0)) (crate-dep (name "hxdmp") (req "^0.2.1") (default-features #t) (kind 0)))) (hash "1pl2sdi78wggxyjy41hd74vwqg285kz036lp9mcnw4xb7z71dd70")))

(define-public crate-vmp4-dump-1 (crate (name "vmp4-dump") (vers "1.0.1") (deps (list (crate-dep (name "byteorder") (req "^1.4.3") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^3.1.6") (default-features #t) (kind 0)) (crate-dep (name "flate2") (req "^1.0.22") (default-features #t) (kind 0)) (crate-dep (name "hxdmp") (req "^0.2.1") (default-features #t) (kind 0)))) (hash "1rc19bs9nhvirck9ah0ifvqwc59816gbahhxgmmmnmkjhp0dfvqx")))

(define-public crate-vmp4-dump-1 (crate (name "vmp4-dump") (vers "1.0.2") (deps (list (crate-dep (name "byteorder") (req "^1.4.3") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^3.1.6") (default-features #t) (kind 0)) (crate-dep (name "flate2") (req "^1.0.22") (default-features #t) (kind 0)) (crate-dep (name "hxdmp") (req "^0.2.1") (default-features #t) (kind 0)))) (hash "1dcx42idwiqdm90q20fcn74ivfy1514asa07i3m6i04md50hqjfx")))

(define-public crate-vmp4-dump-1 (crate (name "vmp4-dump") (vers "1.0.3") (deps (list (crate-dep (name "byteorder") (req "^1.4.3") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^3.1.6") (default-features #t) (kind 0)) (crate-dep (name "flate2") (req "^1.0.22") (default-features #t) (kind 0)) (crate-dep (name "hxdmp") (req "^0.2.1") (default-features #t) (kind 0)))) (hash "13hal33fg0j832dk6fgqycngk4n64kx0c7yy9x0sgkbwy1323qbb")))

(define-public crate-vmp4-dump-1 (crate (name "vmp4-dump") (vers "1.0.4-alpha.0") (deps (list (crate-dep (name "byteorder") (req "^1.5") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^4") (default-features #t) (kind 0)) (crate-dep (name "flate2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "hxdmp") (req "^0.2") (default-features #t) (kind 0)))) (hash "1zrgixdm1d44qiz7q8jmbchjsclh6lirjp749iaghhjmxvhq4jj9")))

(define-public crate-vmp4-dump-1 (crate (name "vmp4-dump") (vers "1.0.5-alpha.0") (deps (list (crate-dep (name "byteorder") (req "^1.5") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^4") (default-features #t) (kind 0)) (crate-dep (name "flate2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "hxdmp") (req "^0.2") (default-features #t) (kind 0)))) (hash "1bgj0pcxccp6nd5dngzr7xlizvz6xk9vyz664dl1b4k2p8x709il")))

(define-public crate-vmp4-dump-1 (crate (name "vmp4-dump") (vers "1.1.0") (deps (list (crate-dep (name "byteorder") (req "^1.5") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^4") (default-features #t) (kind 0)) (crate-dep (name "flate2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "hxdmp") (req "^0.2") (default-features #t) (kind 0)))) (hash "1a8cya99ihhvkvjq2hqmp6qj403j03hjvsy7d73n0vb480w4d0vw")))

