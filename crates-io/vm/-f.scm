(define-module (crates-io vm -f) #:use-module (crates-io))

(define-public crate-vm-fdt-0.1 (crate (name "vm-fdt") (vers "0.1.0") (hash "0mrlrq0qfq5j8zrgz25719mxxsdh7yqfskvv3hcb56llvx7nz65x") (features (quote (("long_running_test"))))))

(define-public crate-vm-fdt-0.2 (crate (name "vm-fdt") (vers "0.2.0") (hash "19j51mdmymz4iwzi0yl4rx37b6vi6q8800i8swx44z8spnkbagzl") (features (quote (("long_running_test"))))))

(define-public crate-vm-fdt-0.3 (crate (name "vm-fdt") (vers "0.3.0") (deps (list (crate-dep (name "hashbrown") (req "^0.14") (optional #t) (default-features #t) (kind 0)))) (hash "08rgjni8bp7y7g222nnd48v6028z94f49s3wc9ibnnd084l2h8by") (features (quote (("std") ("long_running_test") ("default" "std") ("alloc" "hashbrown"))))))

