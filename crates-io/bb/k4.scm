(define-module (crates-io bb k4) #:use-module (crates-io))

(define-public crate-bbk47_toolbox-0.1 (crate (name "bbk47_toolbox") (vers "0.1.0") (deps (list (crate-dep (name "uuid") (req "^0.8") (features (quote ("serde" "v4"))) (default-features #t) (kind 0)))) (hash "1hmbvf7p1qzd838xrgw5fh3hi7dkqlk94f6znhd03r5h1al2xs4g")))

