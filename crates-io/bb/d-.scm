(define-module (crates-io bb d-) #:use-module (crates-io))

(define-public crate-bbd-lib-0.1 (crate (name "bbd-lib") (vers "0.1.0") (deps (list (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)))) (hash "1va8vjsahx48lc45bsirwzj1pr4xpfv6v1g2bfjcf24v7fibmcb1")))

(define-public crate-bbd-lib-0.1 (crate (name "bbd-lib") (vers "0.1.1") (deps (list (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)))) (hash "0d50gnz2s8pjfxhl61qp0y3cdbwc1jbkxcv0lchs8mn5vm6v2wnx")))

(define-public crate-bbd-lib-0.2 (crate (name "bbd-lib") (vers "0.2.0") (deps (list (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)))) (hash "1l5hd2pw7x09x5lk9pyasa3n85w576p25s9c63g57kgkyw97448g")))

