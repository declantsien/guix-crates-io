(define-module (crates-io bb ec) #:use-module (crates-io))

(define-public crate-bbecs-0.1 (crate (name "bbecs") (vers "0.1.0") (deps (list (crate-dep (name "eyre") (req "^0.6.5") (default-features #t) (kind 0)) (crate-dep (name "ggez") (req "^0.5.1") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.24") (default-features #t) (kind 0)))) (hash "1pm75wpkgqbrp12zllb9jgd9xsmxhiqjx1rimi3wfn0prmbvf8kf")))

(define-public crate-bbecs-0.1 (crate (name "bbecs") (vers "0.1.1") (deps (list (crate-dep (name "eyre") (req "^0.6.5") (default-features #t) (kind 0)) (crate-dep (name "ggez") (req "^0.5.1") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.24") (default-features #t) (kind 0)))) (hash "187l72q8ymk7ali9n73v9cc23912414yjf97ryq7nkhgv7cx60m8")))

(define-public crate-bbecs-0.1 (crate (name "bbecs") (vers "0.1.2") (deps (list (crate-dep (name "eyre") (req "^0.6.5") (default-features #t) (kind 0)) (crate-dep (name "ggez") (req "^0.5.1") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.24") (default-features #t) (kind 0)))) (hash "1yx3lzb7ql83vqflnkvs6ck8304b55y2k6isynglkh4rnz1ga64q")))

(define-public crate-bbecs-0.1 (crate (name "bbecs") (vers "0.1.3") (deps (list (crate-dep (name "eyre") (req "^0.6.5") (default-features #t) (kind 0)) (crate-dep (name "ggez") (req "^0.5.1") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.24") (default-features #t) (kind 0)))) (hash "0nyak79ggjsszc5mwqqm9n902yjqvvg9xn33scc1939xyyvz6l32")))

(define-public crate-bbecs-0.1 (crate (name "bbecs") (vers "0.1.4") (deps (list (crate-dep (name "eyre") (req "^0.6.5") (default-features #t) (kind 0)) (crate-dep (name "ggez") (req "^0.5.1") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.24") (default-features #t) (kind 0)))) (hash "1ic8lzrh4w505lv0hlqg3r9nksss97db56vrxsigxwq75rln93b3")))

(define-public crate-bbecs-0.1 (crate (name "bbecs") (vers "0.1.5") (deps (list (crate-dep (name "eyre") (req "^0.6.5") (default-features #t) (kind 0)) (crate-dep (name "ggez") (req "^0.5.1") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.24") (default-features #t) (kind 0)))) (hash "0vccis27psv29v82x260az3y4i33zprg0bd428m89p9hyrka436x")))

(define-public crate-bbecs-0.1 (crate (name "bbecs") (vers "0.1.6") (deps (list (crate-dep (name "eyre") (req "^0.6.5") (default-features #t) (kind 0)) (crate-dep (name "ggez") (req "^0.5.1") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.24") (default-features #t) (kind 0)))) (hash "1mdqjvmhna76a9wc39hpjmaq02vv8jzpppcn2xs7k8dimhbl315c")))

(define-public crate-bbecs-0.1 (crate (name "bbecs") (vers "0.1.7") (deps (list (crate-dep (name "eyre") (req "^0.6.5") (default-features #t) (kind 0)) (crate-dep (name "ggez") (req "^0.5.1") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.24") (default-features #t) (kind 0)))) (hash "1vk2w6nv92i295297c0k587j6bik11y000rrjca60hx64v6adnfk")))

(define-public crate-bbecs-0.1 (crate (name "bbecs") (vers "0.1.8") (deps (list (crate-dep (name "eyre") (req "^0.6.5") (default-features #t) (kind 0)) (crate-dep (name "ggez") (req "^0.5.1") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.24") (default-features #t) (kind 0)))) (hash "0ls3y7ca0y4475kmvcx547lc7d0n56k076s9b4m19nay68rmlrgf")))

(define-public crate-bbecs-0.1 (crate (name "bbecs") (vers "0.1.9") (deps (list (crate-dep (name "eyre") (req "^0.6.5") (default-features #t) (kind 0)) (crate-dep (name "ggez") (req "^0.5.1") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.24") (default-features #t) (kind 0)))) (hash "0j2bqb6qfmz8rjl7gvqrn01vdrw2qx140dysfz9vw5y1aj5r82ir")))

(define-public crate-bbecs-0.1 (crate (name "bbecs") (vers "0.1.10") (deps (list (crate-dep (name "eyre") (req "^0.6.5") (default-features #t) (kind 0)) (crate-dep (name "ggez") (req "^0.5.1") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.24") (default-features #t) (kind 0)))) (hash "1k1pajn14hd9747r5rm9pi0wh2amw45b9vyhh4k15v8cr4ljzba9")))

(define-public crate-bbecs-0.1 (crate (name "bbecs") (vers "0.1.11") (deps (list (crate-dep (name "eyre") (req "^0.6.5") (default-features #t) (kind 0)) (crate-dep (name "ggez") (req "^0.5.1") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.24") (default-features #t) (kind 0)))) (hash "04slmaa18hgalc0wx88kvjq9b4bj45iz5fngcr7ps7dgxghw8rf9")))

(define-public crate-bbecs-0.1 (crate (name "bbecs") (vers "0.1.12") (deps (list (crate-dep (name "eyre") (req "^0.6.5") (default-features #t) (kind 0)) (crate-dep (name "ggez") (req "^0.5.1") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.24") (default-features #t) (kind 0)))) (hash "0v3pds2yx1xc5wammcprsga2r5gzhypjwyzkw7dp80cbzd0n9k3y")))

(define-public crate-bbecs-0.1 (crate (name "bbecs") (vers "0.1.14") (deps (list (crate-dep (name "eyre") (req "^0.6.5") (default-features #t) (kind 0)) (crate-dep (name "ggez") (req "^0.5.1") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.24") (default-features #t) (kind 0)))) (hash "119j1wqvzwxbb0r1vl7c97c5974q63hx47k1lhb2p4m3vz6c53gx")))

(define-public crate-bbecs-0.2 (crate (name "bbecs") (vers "0.2.0") (deps (list (crate-dep (name "eyre") (req "^0.6.5") (default-features #t) (kind 0)) (crate-dep (name "ggez") (req "^0.5.1") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.24") (default-features #t) (kind 0)))) (hash "0ba1kdg44bgazbmxxniz66dbxdbxjk97807h2fjy05kqcjvykrny")))

(define-public crate-bbecs-0.3 (crate (name "bbecs") (vers "0.3.0") (deps (list (crate-dep (name "eyre") (req "^0.6.5") (default-features #t) (kind 0)) (crate-dep (name "ggez") (req "^0.5.1") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.24") (default-features #t) (kind 0)))) (hash "08j8kij62hhbshq695x881yf86v9jadf32d1rrbfrpbcx9j5kdsr")))

(define-public crate-bbecs_tutorial-0.1 (crate (name "bbecs_tutorial") (vers "0.1.0") (deps (list (crate-dep (name "eyre") (req "^0.6.5") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.26") (default-features #t) (kind 0)))) (hash "1dbf5c4g4l9pjx1hpm3rbyfirlqz9wph5msy6v89slbzsz7aj21g")))

(define-public crate-bbecs_tutorial-1 (crate (name "bbecs_tutorial") (vers "1.0.0") (deps (list (crate-dep (name "eyre") (req "^0.6.5") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.26") (default-features #t) (kind 0)))) (hash "19i1awb155kvcf1xgf6qj54wm87fjxra7113sy7y586ws3y6hzcb")))

(define-public crate-bbecs_tutorial-1 (crate (name "bbecs_tutorial") (vers "1.0.1") (deps (list (crate-dep (name "eyre") (req "^0.6.5") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.26") (default-features #t) (kind 0)))) (hash "1miaxjn42aghr8xpgm0mvwaa9iwrwk927fdhl5viykkhwynhsc0a")))

(define-public crate-bbecs_tutorial-1 (crate (name "bbecs_tutorial") (vers "1.0.2") (deps (list (crate-dep (name "eyre") (req "^0.6.5") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.26") (default-features #t) (kind 0)))) (hash "0ykk90sagk7mpvifg2b9dfyv9js637hbjl19k87hxbpjk2snxw53")))

(define-public crate-bbecs_tutorial-1 (crate (name "bbecs_tutorial") (vers "1.0.3") (deps (list (crate-dep (name "eyre") (req "^0.6.5") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.26") (default-features #t) (kind 0)))) (hash "15zhl2vy2vy943dr49drnxw674vh8a1dwhmdw3sy7n21l77y29yh")))

