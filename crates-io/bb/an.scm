(define-module (crates-io bb an) #:use-module (crates-io))

(define-public crate-bband-rs-0.1 (crate (name "bband-rs") (vers "0.1.0") (deps (list (crate-dep (name "sd-rs") (req ">=0.1.0, <0.2.0") (default-features #t) (kind 0)) (crate-dep (name "ta-common") (req ">=0.1.0, <0.2.0") (default-features #t) (kind 0)))) (hash "009nyra14wd8jmvdy417gsbmwgkhgi6z8iz8nab6154b1rh4w8hz")))

(define-public crate-bbangcat_encryption-0.1 (crate (name "bbangcat_encryption") (vers "0.1.0") (hash "01djna64ri0ahdkarmkbrpdb8w73dbwzcx4ljzlsvk7slylm4p5i")))

(define-public crate-bbangcat_encryption-0.1 (crate (name "bbangcat_encryption") (vers "0.1.1") (hash "0w2i5xhs9pg8qd3cw93dchlw8w8bz6w0rcy914b2bzyswn8mi5yv")))

(define-public crate-bbanglog-0.1 (crate (name "bbanglog") (vers "0.1.0") (deps (list (crate-dep (name "chrono") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "colored") (req "^2.0.0") (default-features #t) (kind 0)))) (hash "0rh28qhiwj9jq8kms1gq53smmrxpali6wnimy1f763skvsnq1409")))

