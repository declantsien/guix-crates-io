(define-module (crates-io bb te) #:use-module (crates-io))

(define-public crate-bbte_optim_tzim1773_genetic-0.1 (crate (name "bbte_optim_tzim1773_genetic") (vers "0.1.0") (deps (list (crate-dep (name "rand") (req "^0.7.2") (default-features #t) (kind 0)))) (hash "05ksjx09zq8d0d1sdmzl1mxx8am714b772a40fwvgf598ppn3imj")))

