(define-module (crates-io bb ha) #:use-module (crates-io))

(define-public crate-bbhash-0.1 (crate (name "bbhash") (vers "0.1.0") (deps (list (crate-dep (name "bbhash-sys") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "tempfile") (req "^3.0.7") (default-features #t) (kind 0)))) (hash "0c46bj2n54lhvaw7bxys37xm9cd6xj9mlfbm1f7cprzjaj61iahy")))

(define-public crate-bbhash-0.1 (crate (name "bbhash") (vers "0.1.1") (deps (list (crate-dep (name "bbhash-sys") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "tempfile") (req "^3.0.7") (default-features #t) (kind 0)))) (hash "0hpy9n1x49gywqcpp45mn2jia0vjqqmid4zx078ivi0f08smfb2a")))

(define-public crate-bbhash-sys-0.1 (crate (name "bbhash-sys") (vers "0.1.0") (deps (list (crate-dep (name "cc") (req "^1.0.3") (default-features #t) (kind 1)) (crate-dep (name "tempfile") (req "^3.0.7") (default-features #t) (kind 2)))) (hash "1kn6q9vxg5awn9vlrhp1bj1v6ij8s1q0036ik5wia50gjpj9427h") (links "bbhash")))

