(define-module (crates-io bb -s) #:use-module (crates-io))

(define-public crate-bb-sys-0.2 (crate (name "bb-sys") (vers "0.2.0") (deps (list (crate-dep (name "bindgen") (req "^0.64.0") (default-features #t) (kind 1)) (crate-dep (name "cc") (req "^1.0.79") (default-features #t) (kind 1)) (crate-dep (name "color-eyre") (req "^0.6.2") (default-features #t) (kind 1)) (crate-dep (name "hex") (req "^0.4.2") (default-features #t) (kind 2)) (crate-dep (name "link-cplusplus") (req "^1.0.8") (default-features #t) (kind 0)) (crate-dep (name "pkg-config") (req "^0.3.26") (default-features #t) (kind 1)) (crate-dep (name "thiserror") (req "^1.0.21") (default-features #t) (kind 1)))) (hash "1vmffp3j24gkprfyk4fbi7idcwlw6xfvqh1jblyn372nnxp8vj50") (links "barretenberg") (rust-version "1.66")))

