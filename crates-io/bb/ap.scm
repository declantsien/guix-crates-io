(define-module (crates-io bb ap) #:use-module (crates-io))

(define-public crate-bbapi-0.1 (crate (name "bbapi") (vers "0.1.0") (deps (list (crate-dep (name "env_logger") (req "^0.6.1") (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.9.16") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.90") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0.90") (default-features #t) (kind 0)))) (hash "0xysng5nk42d49gj60nxnhvy6ag0spc09s1z15l9bz376bh0d3fj")))

