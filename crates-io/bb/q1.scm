(define-module (crates-io bb q1) #:use-module (crates-io))

(define-public crate-bbq10kbd-0.1 (crate (name "bbq10kbd") (vers "0.1.0") (deps (list (crate-dep (name "embedded-hal") (req "^0.2.3") (default-features #t) (kind 0)))) (hash "0dlhkm5yfngns2baaw6dl4lar2hl684x7lzipv3zk12z1b8gxsgf")))

