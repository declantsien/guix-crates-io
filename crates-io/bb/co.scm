(define-module (crates-io bb co) #:use-module (crates-io))

(define-public crate-bbcode-1 (crate (name "bbcode") (vers "1.0.0") (deps (list (crate-dep (name "lazy_static") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^0.2") (default-features #t) (kind 0)))) (hash "0scf00ssclw95zkkxymv4azdm0ll7hzfharppmwn8wgk782ah57c")))

(define-public crate-bbcode-1 (crate (name "bbcode") (vers "1.0.1") (deps (list (crate-dep (name "lazy_static") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^0.2") (default-features #t) (kind 0)))) (hash "0f5rnll5yv1fj6cpbdg9kq4ya3m8dwr6dc3awf4ifda8cmzgrqrf")))

(define-public crate-bbcode-1 (crate (name "bbcode") (vers "1.0.2") (deps (list (crate-dep (name "lazy_static") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^0.2") (default-features #t) (kind 0)))) (hash "0w8gqa7vm557gpwmkn6fk8qgq3xqxz175fcgh9ick4m30xrv4p6w")))

(define-public crate-bbcode-tagger-0.1 (crate (name "bbcode-tagger") (vers "0.1.0") (deps (list (crate-dep (name "regex") (req "^1.7.3") (default-features #t) (kind 0)))) (hash "0f1yzr9xxy0qrfnyk738r8q57bzlw86yznv89a4xdcjdnmnccqlk")))

(define-public crate-bbcode-tagger-0.1 (crate (name "bbcode-tagger") (vers "0.1.1") (deps (list (crate-dep (name "regex") (req "^1.7.3") (default-features #t) (kind 0)))) (hash "0adc7lgsj7lyxyg7v26gxh5wz172dmi84lmgxdm4xyikyr443c40")))

(define-public crate-bbcode-tagger-0.1 (crate (name "bbcode-tagger") (vers "0.1.2") (deps (list (crate-dep (name "regex") (req "^1.7.3") (default-features #t) (kind 0)))) (hash "0my8czkcah7ndm3vmdk7cx1jpgdx2sli8qqdc81iagldypqxp3dw")))

(define-public crate-bbcode-tagger-0.1 (crate (name "bbcode-tagger") (vers "0.1.3") (deps (list (crate-dep (name "regex") (req "^1.7.3") (default-features #t) (kind 0)))) (hash "0898ic12papfrqd41bi4b80ym9jh05qkp17zzw5vlx424q4sci4j")))

(define-public crate-bbcode-tagger-0.1 (crate (name "bbcode-tagger") (vers "0.1.4") (deps (list (crate-dep (name "regex") (req "^1.7.3") (default-features #t) (kind 0)))) (hash "1vz9yb4s2k2a1982mkhisrh4inwacwyz9x5qaf57glca4jb32z8w")))

(define-public crate-bbcode-tagger-0.2 (crate (name "bbcode-tagger") (vers "0.2.0") (deps (list (crate-dep (name "regex") (req "^1.7.3") (default-features #t) (kind 0)))) (hash "19x05wa4c3mn55mzhs12n48021gmz2iwsigbmhazzi46gml35lj9")))

