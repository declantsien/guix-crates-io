(define-module (crates-io bb qu) #:use-module (crates-io))

(define-public crate-bbqueue-0.0.1 (crate (name "bbqueue") (vers "0.0.1") (hash "0cj10fiaakdnjz01w5hnlp07vcinqp4kiiymdnhq6j5a74vgl8cb")))

(define-public crate-bbqueue-0.0.2 (crate (name "bbqueue") (vers "0.0.2") (deps (list (crate-dep (name "generic-array") (req "^0.12") (default-features #t) (kind 0)))) (hash "17v069wvh7xdk93cwam8s7vga73myfv34xjmpqpv0qx056sirph8")))

(define-public crate-bbqueue-0.1 (crate (name "bbqueue") (vers "0.1.0") (deps (list (crate-dep (name "generic-array") (req "^0.12") (default-features #t) (kind 0)))) (hash "0lpg7hh3ylbpf14y9g9c34x567kbcxhy0ard4qd1dpshx7x8nvcr")))

(define-public crate-bbqueue-0.1 (crate (name "bbqueue") (vers "0.1.1") (deps (list (crate-dep (name "generic-array") (req "^0.12") (default-features #t) (kind 0)))) (hash "0z17g8x30man8i3dr44hhq3p8yayyg7s41v00yxqlfwmp2rvv7k3")))

(define-public crate-bbqueue-0.2 (crate (name "bbqueue") (vers "0.2.0") (hash "0gvsrj2ppvsh6i7i650hisji72bcvskrmqq8araidf82w9f71gg0") (features (quote (("std") ("default") ("cortex-m"))))))

(define-public crate-bbqueue-0.2 (crate (name "bbqueue") (vers "0.2.1") (hash "10b2hswxd3vdhc2d3svy15y9v2zn4vp1gl0k2j7vcacx48739ghv") (features (quote (("std") ("default") ("cortex-m"))))))

(define-public crate-bbqueue-0.3 (crate (name "bbqueue") (vers "0.3.0") (hash "06s647k7p8vxnkc05gqmp3kxaynjyjdfk6cfxh11pb6iyyvwhllj") (features (quote (("std") ("default") ("cortex-m"))))))

(define-public crate-bbqueue-0.3 (crate (name "bbqueue") (vers "0.3.1") (hash "1a5wmyadd1ngzpw0ciil4mw48hkda2glykmbq48kyhb22q8xlvdn") (features (quote (("std") ("default") ("cortex-m"))))))

(define-public crate-bbqueue-0.3 (crate (name "bbqueue") (vers "0.3.2") (hash "1x0h8gpvl51sv3b3cdmcj2n303x3zny37sk1bblnsvqysbrd26gx") (features (quote (("std") ("default") ("cortex-m"))))))

(define-public crate-bbqueue-0.4 (crate (name "bbqueue") (vers "0.4.0-alpha1") (deps (list (crate-dep (name "generic-array") (req "^0.13") (default-features #t) (kind 0)))) (hash "19yzq00dc61hlqjgh2z5mc084zkwmk1gh4j8zm898d62xp3357wq") (features (quote (("std") ("default") ("cortex-m")))) (yanked #t)))

(define-public crate-bbqueue-0.4 (crate (name "bbqueue") (vers "0.4.0-alpha2") (deps (list (crate-dep (name "generic-array") (req "^0.13") (default-features #t) (kind 0)))) (hash "1l347928yi8xpknwg0jkkfpxz21pw02rcyvdyxlgzjm25hwyvccq") (features (quote (("std") ("default") ("cortex-m")))) (yanked #t)))

(define-public crate-bbqueue-0.4 (crate (name "bbqueue") (vers "0.4.0-alpha3") (deps (list (crate-dep (name "cortex-m") (req "^0.6.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "generic-array") (req "^0.13") (default-features #t) (kind 0)))) (hash "19lkp9f5jvvb8n5c8h1zdyya27y9v4rha98lcklvar6am9c7kz8i") (features (quote (("thumbv6" "cortex-m") ("std") ("default" "atomic") ("atomic")))) (yanked #t)))

(define-public crate-bbqueue-0.4 (crate (name "bbqueue") (vers "0.4.0-alpha4") (deps (list (crate-dep (name "cortex-m") (req "^0.6.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "generic-array") (req "^0.13") (default-features #t) (kind 0)))) (hash "00fh9lli4kick7qq9j7piymr5zszkd2b72nvas9z2ylid7376amh") (features (quote (("thumbv6" "cortex-m") ("std") ("default" "atomic") ("atomic")))) (yanked #t)))

(define-public crate-bbqueue-0.4 (crate (name "bbqueue") (vers "0.4.0-alpha5") (deps (list (crate-dep (name "cortex-m") (req "^0.6.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "generic-array") (req "^0.13") (default-features #t) (kind 0)))) (hash "1rs545knajjr2255vlpwhdf9g4bx82jjv0ak2v9frvf0yryb8ni6") (features (quote (("thumbv6" "cortex-m") ("std") ("default" "atomic") ("atomic")))) (yanked #t)))

(define-public crate-bbqueue-0.4 (crate (name "bbqueue") (vers "0.4.0") (deps (list (crate-dep (name "cortex-m") (req "^0.6.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "generic-array") (req "^0.13") (default-features #t) (kind 0)))) (hash "10ki7yqqilkfqy869vn2k3z06l3s4wi8k0yxszi2x4mazmyn8b15") (features (quote (("thumbv6" "cortex-m") ("std") ("default" "atomic") ("atomic")))) (yanked #t)))

(define-public crate-bbqueue-0.4 (crate (name "bbqueue") (vers "0.4.1") (deps (list (crate-dep (name "cortex-m") (req "^0.6.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "generic-array") (req "^0.13") (default-features #t) (kind 0)))) (hash "0yq5qhg5ka9f21n9zcwabp8q4c3d6rrvjfp8h1v5jvsb06rclydk") (features (quote (("thumbv6" "cortex-m") ("std") ("default" "atomic") ("atomic")))) (yanked #t)))

(define-public crate-bbqueue-0.4 (crate (name "bbqueue") (vers "0.4.2") (deps (list (crate-dep (name "cortex-m") (req "^0.6.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "generic-array") (req "^0.13") (default-features #t) (kind 0)))) (hash "0ch9p2arf1my3zadfwkj9b81s17nfwmpj6fhv78ibl334g94p7bx") (features (quote (("thumbv6" "cortex-m") ("std") ("default" "atomic") ("atomic")))) (yanked #t)))

(define-public crate-bbqueue-0.4 (crate (name "bbqueue") (vers "0.4.3") (deps (list (crate-dep (name "cortex-m") (req "^0.6.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "generic-array") (req "^0.13") (default-features #t) (kind 0)))) (hash "0pfndvkvphbj0wl0nyh0s7x1x8lkvv208i35l77a6qnnjfh319wq") (features (quote (("thumbv6" "cortex-m") ("std") ("default" "atomic") ("atomic")))) (yanked #t)))

(define-public crate-bbqueue-0.4 (crate (name "bbqueue") (vers "0.4.4") (deps (list (crate-dep (name "cortex-m") (req "^0.6.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "generic-array") (req "^0.13") (default-features #t) (kind 0)))) (hash "1pj50cnc06b0y5p42r5inm4grg7kchk5pyyzr0j7z6n7fqvih603") (features (quote (("thumbv6" "cortex-m") ("std") ("atomic"))))))

(define-public crate-bbqueue-0.4 (crate (name "bbqueue") (vers "0.4.5") (deps (list (crate-dep (name "cortex-m") (req "^0.6.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "generic-array") (req "^0.13") (default-features #t) (kind 0)))) (hash "0zgf4vc4h201qh3k18wjx6nf2kwyv2gmrq5ka5aanxna7pprks9m") (features (quote (("thumbv6" "cortex-m") ("std") ("atomic"))))))

(define-public crate-bbqueue-0.4 (crate (name "bbqueue") (vers "0.4.6") (deps (list (crate-dep (name "cortex-m") (req "^0.6.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "generic-array") (req "^0.13") (default-features #t) (kind 0)))) (hash "1sb5vqja2br1ng1lznm9hpcwm3s72d8983454njpf28h4227pfjr") (features (quote (("thumbv6" "cortex-m") ("std") ("atomic"))))))

(define-public crate-bbqueue-0.4 (crate (name "bbqueue") (vers "0.4.7") (deps (list (crate-dep (name "cortex-m") (req "^0.6.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "generic-array") (req "^0.13") (default-features #t) (kind 0)))) (hash "0y1favc4kqgq0nzhwxhim531zq8vrvfs40w7iq1ms90vkm3xhszk") (features (quote (("thumbv6" "cortex-m") ("std") ("atomic"))))))

(define-public crate-bbqueue-0.4 (crate (name "bbqueue") (vers "0.4.8") (deps (list (crate-dep (name "cortex-m") (req "^0.6.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "generic-array") (req "^0.13") (default-features #t) (kind 0)))) (hash "10lf2nr25wypqnj7imfjn22qnpqrnbwwz0piikd8axyskzrqrlr7") (features (quote (("thumbv6" "cortex-m") ("std") ("atomic"))))))

(define-public crate-bbqueue-0.4 (crate (name "bbqueue") (vers "0.4.9") (deps (list (crate-dep (name "cortex-m") (req "^0.6.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "generic-array") (req "^0.13") (default-features #t) (kind 0)))) (hash "09y9rz46zp68kgvhbhgpxzvixqs04v96pqnm1xhr36iziczr3d3z") (features (quote (("thumbv6" "cortex-m") ("std") ("atomic"))))))

(define-public crate-bbqueue-0.4 (crate (name "bbqueue") (vers "0.4.10") (deps (list (crate-dep (name "cortex-m") (req "^0.6.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "generic-array") (req "^0.13") (default-features #t) (kind 0)))) (hash "02lc52kiwbgycfi8mfmmd0a9jbwcwkyx1ijsbnsqwa84dndgsvrf") (features (quote (("thumbv6" "cortex-m") ("std") ("atomic"))))))

(define-public crate-bbqueue-0.4 (crate (name "bbqueue") (vers "0.4.11") (deps (list (crate-dep (name "cortex-m") (req ">=0.6.0, <0.7.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "generic-array") (req ">=0.13.0, <0.14.0") (default-features #t) (kind 0)))) (hash "1k147b7g5dvlwkjrwb57pxd0pzk2vdxnvddhxp2mm0w50dlcf1jk") (features (quote (("thumbv6" "cortex-m") ("std") ("atomic")))) (yanked #t)))

(define-public crate-bbqueue-0.4 (crate (name "bbqueue") (vers "0.4.12") (deps (list (crate-dep (name "cortex-m") (req "^0.6.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "generic-array") (req "^0.13") (default-features #t) (kind 0)))) (hash "1icl5xx6q316j7rfg6ajb95n6mxm90dmz3ypxy2gp0lx8mqjm88s") (features (quote (("thumbv6" "cortex-m") ("std") ("atomic"))))))

(define-public crate-bbqueue-0.5 (crate (name "bbqueue") (vers "0.5.0") (deps (list (crate-dep (name "cortex-m") (req "^0.6.0") (optional #t) (default-features #t) (kind 0)))) (hash "0n0y8j2ycyvla4rbpp1vkjanlgqmm9kapzaa4h96d7pnbanw1045") (features (quote (("thumbv6" "cortex-m"))))))

(define-public crate-bbqueue-0.5 (crate (name "bbqueue") (vers "0.5.1") (deps (list (crate-dep (name "cortex-m") (req "^0.6.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "defmt") (req "^0.3.0") (optional #t) (default-features #t) (kind 0)))) (hash "0m8azrn0xc158cg83z58r7qhwr54k42mr9rr210wg96ib64alfzx") (features (quote (("thumbv6" "cortex-m") ("defmt_0_3" "defmt"))))))

(define-public crate-bbqueue-ng-0.4 (crate (name "bbqueue-ng") (vers "0.4.11") (deps (list (crate-dep (name "cortex-m") (req ">=0.6.0, <0.7.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "generic-array") (req ">=0.13.0, <0.14.0") (default-features #t) (kind 0)))) (hash "0f7jxkcni9359pf5w58h02425g989dzbfb4ra868pwn5db9qhkja") (features (quote (("thumbv6" "cortex-m") ("std") ("atomic"))))))

(define-public crate-bbqueue-ng-0.100 (crate (name "bbqueue-ng") (vers "0.100.0") (deps (list (crate-dep (name "cortex-m") (req "^0.6.0") (optional #t) (default-features #t) (kind 0)))) (hash "17n0zfhsd0vc8b8jnpmlkq4x0v8hcdsyxkjl5rn40c8z0bydy0zs") (features (quote (("thumbv6" "cortex-m") ("std") ("atomic"))))))

(define-public crate-bbqueue-ng-0.100 (crate (name "bbqueue-ng") (vers "0.100.1") (deps (list (crate-dep (name "cortex-m") (req "^0.6.0") (optional #t) (default-features #t) (kind 0)))) (hash "1zvr3zx5li4r07w0xawlaxm4s2maks4a3n3y6l5vnhknq12swcwp") (features (quote (("thumbv6" "cortex-m") ("std") ("atomic"))))))

(define-public crate-bbqueue-ng-0.101 (crate (name "bbqueue-ng") (vers "0.101.0") (deps (list (crate-dep (name "cortex-m") (req "^0.6.0") (optional #t) (default-features #t) (kind 0)))) (hash "1vy8jrfiywsa6mkxnsrjwq1rwlxh1wv2abigdb3v70c99ahg9lra") (features (quote (("thumbv6" "cortex-m"))))))

(define-public crate-bbqueue-ng-0.101 (crate (name "bbqueue-ng") (vers "0.101.1") (deps (list (crate-dep (name "cortex-m") (req "^0.6.0") (optional #t) (default-features #t) (kind 0)))) (hash "0wx24j5bsw9m30kzp3m6z2lxbabhnsa9cvzr3rja0qgf5c84gsgm") (features (quote (("thumbv6" "cortex-m"))))))

(define-public crate-bbqueue-sync-0.5 (crate (name "bbqueue-sync") (vers "0.5.1") (deps (list (crate-dep (name "cortex-m") (req "^0.6.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "defmt") (req "^0.3.0") (optional #t) (default-features #t) (kind 0)))) (hash "1igfx3aj12c3gjn1mxzh34p8y306i1n1szgv845zvk7hqyh26npc") (features (quote (("thumbv6" "cortex-m") ("defmt_0_3" "defmt"))))))

