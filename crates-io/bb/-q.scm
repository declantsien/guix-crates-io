(define-module (crates-io bb -q) #:use-module (crates-io))

(define-public crate-bb-qol-0.1 (crate (name "bb-qol") (vers "0.1.0") (deps (list (crate-dep (name "itertools") (req "^0.12.1") (default-features #t) (kind 0)))) (hash "0fjap628yi748kfxmrbhay50bci5km6qdq1sq9rpb1hjk0naacrg")))

