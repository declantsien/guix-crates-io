(define-module (crates-io bb q-) #:use-module (crates-io))

(define-public crate-bbq-rs-0.1 (crate (name "bbq-rs") (vers "0.1.0") (deps (list (crate-dep (name "anyhow") (req "^1.0.69") (default-features #t) (kind 0)))) (hash "1iws63d0ykhx6s2xnfanm9nmm4y7b8yddi5nk01n61v8dc30biip")))

(define-public crate-bbq-rs-0.1 (crate (name "bbq-rs") (vers "0.1.1") (deps (list (crate-dep (name "anyhow") (req "^1.0.69") (default-features #t) (kind 0)))) (hash "1vrzwf6hb11yin3xn7qyw0ja5xzsshl5l2gmfci3psfpjgijxndd")))

