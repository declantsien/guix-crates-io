(define-module (crates-io rw ce) #:use-module (crates-io))

(define-public crate-rwcell-0.1 (crate (name "rwcell") (vers "0.1.0") (hash "0scxwblrx8h8m74lkazaw85fm1lh13z2i2zfsr6vw56hywcpcxaa")))

(define-public crate-rwcell-0.1 (crate (name "rwcell") (vers "0.1.1") (hash "0mkxj5mxcpdhbxnp2568p3wh79ag3kpzwn43h8kda2dr9r1h626k")))

(define-public crate-rwcell-0.1 (crate (name "rwcell") (vers "0.1.2") (hash "1j6rlv79zzl71nygq4jrb2js4yg0lb1di6il6ss95g7sxj73izlh")))

