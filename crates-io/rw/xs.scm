(define-module (crates-io rw xs) #:use-module (crates-io))

(define-public crate-rwxs-0.1 (crate (name "rwxs") (vers "0.1.0") (deps (list (crate-dep (name "array_tool") (req "^1.0.3") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.5.5") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.3.26") (kind 0)))) (hash "19xfr5al0lk0c0yhppjirbn401bqrbwd5zzra48kcvfc8iw8ai8z") (yanked #t)))

(define-public crate-rwxs-0.1 (crate (name "rwxs") (vers "0.1.1") (deps (list (crate-dep (name "array_tool") (req "^1.0.3") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.5.5") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.3.26") (kind 0)))) (hash "1cwjclph6075ff5z51vdm4sjrdn752wrkm2k4cfsfqc1iqri1yk3") (yanked #t)))

