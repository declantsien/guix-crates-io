(define-module (crates-io rw ut) #:use-module (crates-io))

(define-public crate-rwutil-0.3 (crate (name "rwutil") (vers "0.3.0") (deps (list (crate-dep (name "byteorder") (req "~0") (default-features #t) (kind 0)))) (hash "0bf3wdjl02x6i2gkq07irjkrbz7pv3861d7261kjp2inzn7m4xhs")))

(define-public crate-rwutil-0.3 (crate (name "rwutil") (vers "0.3.1") (deps (list (crate-dep (name "byteorder") (req "^0.4") (default-features #t) (kind 0)))) (hash "19w834gp12k259h5vdqhb6v8s6mczqwg3w7pskv7nggyw38gn8mp")))

(define-public crate-rwutil-0.4 (crate (name "rwutil") (vers "0.4.0") (deps (list (crate-dep (name "byteorder") (req "^0.4") (default-features #t) (kind 0)))) (hash "0z5kagrrsavfmv1gyb7521fnb8csd4imv419r5d26xp7yk78i8ba")))

(define-public crate-rwutil-1 (crate (name "rwutil") (vers "1.0.0") (deps (list (crate-dep (name "byteorder") (req "^0.4") (default-features #t) (kind 0)))) (hash "1cksabcywda1y0xagpapj8d0kxqr9zbhaafjsvfxxqz87x41dxn9")))

(define-public crate-rwutil-1 (crate (name "rwutil") (vers "1.0.1") (deps (list (crate-dep (name "byteorder") (req "^0.4") (default-features #t) (kind 0)))) (hash "1xdw3zvzbnhzd6hjmriv4r7a0nl9f7xc2cxlydkwi4vazx90v427")))

(define-public crate-rwutil-2 (crate (name "rwutil") (vers "2.0.0") (deps (list (crate-dep (name "byteorder") (req "^1.0.0") (default-features #t) (kind 0)))) (hash "1nqr67krpjk6ibcpbbnnp07094dsqv5pjvvl3lrngq6fqf616vmr")))

