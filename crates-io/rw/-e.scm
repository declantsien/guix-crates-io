(define-module (crates-io rw -e) #:use-module (crates-io))

(define-public crate-rw-exact-ext-0.1 (crate (name "rw-exact-ext") (vers "0.1.0") (deps (list (crate-dep (name "num-traits") (req "^0.2.17") (optional #t) (default-features #t) (kind 0)))) (hash "1azk6146amh407jc6wj4mmdsgfsxxr7lqn5v1qas04var29jwj4x") (yanked #t)))

(define-public crate-rw-exact-ext-0.1 (crate (name "rw-exact-ext") (vers "0.1.1") (deps (list (crate-dep (name "num-traits") (req "^0.2.17") (optional #t) (default-features #t) (kind 0)))) (hash "0dfkvm19mw28zk64198y3lw2b34fyylgn17nq9fqy2pb2801px14") (yanked #t)))

(define-public crate-rw-exact-ext-0.1 (crate (name "rw-exact-ext") (vers "0.1.2") (deps (list (crate-dep (name "num-traits") (req "^0.2.17") (optional #t) (default-features #t) (kind 0)))) (hash "1v148a9xlmp0f2a36fd4gjqakkch1hj1292yibql3wc4x04xz8fy") (yanked #t)))

(define-public crate-rw-exact-ext-0.1 (crate (name "rw-exact-ext") (vers "0.1.3") (deps (list (crate-dep (name "num-traits") (req "^0.2.17") (optional #t) (default-features #t) (kind 0)))) (hash "12vy5zfjpmia1z3c6m8nmyhgmqhgwn3x545dc0llscfqmxj2vlqy") (yanked #t)))

(define-public crate-rw-exact-ext-0.1 (crate (name "rw-exact-ext") (vers "0.1.4") (deps (list (crate-dep (name "num-traits") (req "^0.2.17") (optional #t) (default-features #t) (kind 0)))) (hash "0l937qs9fcj7ri3g6k1b9jjwq5004s399932z3g8f8jv5979nc6m")))

(define-public crate-rw-exact-ext-0.1 (crate (name "rw-exact-ext") (vers "0.1.5") (deps (list (crate-dep (name "num-traits") (req "^0.2.17") (optional #t) (default-features #t) (kind 0)))) (hash "11hcnbr7ifr98bhhajd7mnvkrqcxvk66kzwvrx8p5lhs9gbmicqr") (yanked #t)))

(define-public crate-rw-exact-ext-0.1 (crate (name "rw-exact-ext") (vers "0.1.6") (deps (list (crate-dep (name "num-traits") (req "^0.2.17") (optional #t) (default-features #t) (kind 0)))) (hash "19spxqav2my169g7pn634y8inpvkp6gvhf19h221bn7mlrs1jzr9")))

(define-public crate-rw-exact-ext-0.1 (crate (name "rw-exact-ext") (vers "0.1.7") (deps (list (crate-dep (name "num-traits") (req "^0.2.17") (optional #t) (default-features #t) (kind 0)))) (hash "0dikhc52sbbbi0i64h7kjd7gdz643nq2iay6fbmnrd5w132l7jyl")))

