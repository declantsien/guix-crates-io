(define-module (crates-io rw c-) #:use-module (crates-io))

(define-public crate-rwc-rs-0.1 (crate (name "rwc-rs") (vers "0.1.0") (deps (list (crate-dep (name "colored") (req "^2") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.3.21") (default-features #t) (kind 0)))) (hash "1b1dpmacy2xasihplz9phx6wix71vplip185v1z36l2w55d661l0")))

