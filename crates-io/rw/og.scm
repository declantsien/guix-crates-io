(define-module (crates-io rw og) #:use-module (crates-io))

(define-public crate-rwog-0.1 (crate (name "rwog") (vers "0.1.0") (deps (list (crate-dep (name "caps") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^2.31.2") (default-features #t) (kind 0)) (crate-dep (name "groups") (req "^0.1.1") (default-features #t) (kind 0)) (crate-dep (name "nix") (req "^0.10.0") (default-features #t) (kind 0)) (crate-dep (name "passwd") (req "^0.0.1") (default-features #t) (kind 0)))) (hash "0gmfz0vb64x24ki6nwh86g6lf0kd9i4wmlpb8g3g119b3r1h1qda")))

(define-public crate-rwog-0.1 (crate (name "rwog") (vers "0.1.1") (deps (list (crate-dep (name "caps") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^2.31.2") (default-features #t) (kind 0)) (crate-dep (name "groups") (req "^0.1.1") (default-features #t) (kind 0)) (crate-dep (name "nix") (req "^0.10.0") (default-features #t) (kind 0)) (crate-dep (name "passwd") (req "^0.0.1") (default-features #t) (kind 0)))) (hash "0mnq245c8q8g8nngcbphyipsjcb6v3jjz3qvjwd22jjvq2127qh0")))

(define-public crate-rwog-0.2 (crate (name "rwog") (vers "0.2.1") (deps (list (crate-dep (name "caps") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^2.31.2") (default-features #t) (kind 0)) (crate-dep (name "nix") (req "^0.10.0") (default-features #t) (kind 0)) (crate-dep (name "users") (req "^0.6.1") (default-features #t) (kind 0)))) (hash "1k2asr4i2wq66s58hd424xn9jj22vx2kzagaidi0s72k2p8361za")))

(define-public crate-rwog-0.2 (crate (name "rwog") (vers "0.2.3") (deps (list (crate-dep (name "caps") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^2.31.2") (default-features #t) (kind 0)) (crate-dep (name "nix") (req "^0.10.0") (default-features #t) (kind 0)) (crate-dep (name "users") (req "^0.6.1") (default-features #t) (kind 0)))) (hash "0hr98rmnb3vjnnv60z8zkmwg9qg2azrvrcmshldbiq1n8qbqk7dk")))

