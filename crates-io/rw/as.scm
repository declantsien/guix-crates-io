(define-module (crates-io rw as) #:use-module (crates-io))

(define-public crate-rwasm-2020 (crate (name "rwasm") (vers "2020.1.0") (deps (list (crate-dep (name "backtrace") (req "^0.3.55") (default-features #t) (kind 0)) (crate-dep (name "once_cell") (req "^1.5.2") (default-features #t) (kind 0)))) (hash "1kldn4yv07lwls2lw63wb0nfkfj2lnh1zjh0ra0jhjsr2jba9l0q") (yanked #t)))

(define-public crate-rwasm-2020 (crate (name "rwasm") (vers "2020.1.1") (deps (list (crate-dep (name "backtrace") (req "^0.3.55") (default-features #t) (kind 0)) (crate-dep (name "once_cell") (req "^1.5.2") (default-features #t) (kind 0)))) (hash "14m75a38xgn7a8kxl5mgm4xh44njkvkwcfjnwd0l2ajirbav27nr") (yanked #t)))

(define-public crate-rwasm-0.0.1 (crate (name "rwasm") (vers "0.0.1") (deps (list (crate-dep (name "backtrace") (req "^0.3.55") (default-features #t) (kind 0)) (crate-dep (name "once_cell") (req "^1.5.2") (default-features #t) (kind 0)) (crate-dep (name "rwasm_macro") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "001ycg9ywilgzml7nl682av1cmikvl3km3sgk4q0zwiicjnmdiq9")))

(define-public crate-rwasm_macro-0.1 (crate (name "rwasm_macro") (vers "0.1.0") (hash "18ysl3kw74za5hhka87a6svixrannvycpc34ril6zqmh2kldn0sx")))

