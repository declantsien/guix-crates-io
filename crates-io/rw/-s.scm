(define-module (crates-io rw -s) #:use-module (crates-io))

(define-public crate-rw-stream-sink-0.1 (crate (name "rw-stream-sink") (vers "0.1.0") (deps (list (crate-dep (name "bytes") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "futures") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "tokio-io") (req "^0.1") (default-features #t) (kind 0)))) (hash "19a64cir3m2rm371al407ln6hv6bn2rvf952a3n6da2vsv1xg2hh")))

(define-public crate-rw-stream-sink-0.1 (crate (name "rw-stream-sink") (vers "0.1.1") (deps (list (crate-dep (name "bytes") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "futures") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "tokio-io") (req "^0.1") (default-features #t) (kind 0)))) (hash "1mi2dlwaq7daypxj1jh9sn4a5f69ziwpnic29gapffkww47s8j6m")))

(define-public crate-rw-stream-sink-0.1 (crate (name "rw-stream-sink") (vers "0.1.2") (deps (list (crate-dep (name "bytes") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "futures") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "tokio-io") (req "^0.1") (default-features #t) (kind 0)))) (hash "0y7yh5ssrc22nbsm4nlf7cl108w7w4wvxdrbbc0x6m84q9hvx71g")))

(define-public crate-rw-stream-sink-0.2 (crate (name "rw-stream-sink") (vers "0.2.0") (deps (list (crate-dep (name "async-std") (req "^1.0") (default-features #t) (kind 2)) (crate-dep (name "futures") (req "^0.3.1") (default-features #t) (kind 0)) (crate-dep (name "static_assertions") (req "^1") (default-features #t) (kind 0)))) (hash "029syz6j7qy5l9lwj612zpx3x5jkznd7g82mapp09h4xchklf7bn")))

(define-public crate-rw-stream-sink-0.2 (crate (name "rw-stream-sink") (vers "0.2.1") (deps (list (crate-dep (name "async-std") (req "^1.0") (default-features #t) (kind 2)) (crate-dep (name "futures") (req "^0.3.1") (default-features #t) (kind 0)) (crate-dep (name "pin-project") (req "^0.4.6") (default-features #t) (kind 0)) (crate-dep (name "static_assertions") (req "^1") (default-features #t) (kind 0)))) (hash "0810glqcrs36r3k3a5skg40kygd9hn914fw3zxfmlvy4ajqgr9ad")))

(define-public crate-rw-stream-sink-0.3 (crate (name "rw-stream-sink") (vers "0.3.0") (deps (list (crate-dep (name "async-std") (req "^1.0") (default-features #t) (kind 2)) (crate-dep (name "futures") (req "^0.3.1") (default-features #t) (kind 0)) (crate-dep (name "pin-project") (req "^1.0.10") (default-features #t) (kind 0)) (crate-dep (name "static_assertions") (req "^1") (default-features #t) (kind 0)))) (hash "016wwxj9sfja9xyhhkyyyr90pjb7fyphbsimn62inwmv15g8ycr6")))

(define-public crate-rw-stream-sink-0.4 (crate (name "rw-stream-sink") (vers "0.4.0") (deps (list (crate-dep (name "async-std") (req "^1.0") (default-features #t) (kind 2)) (crate-dep (name "futures") (req "^0.3.28") (default-features #t) (kind 0)) (crate-dep (name "pin-project") (req "^1.1.0") (default-features #t) (kind 0)) (crate-dep (name "static_assertions") (req "^1") (default-features #t) (kind 0)))) (hash "18g9bxl1lb6har1f9i09zcdh10v32lzjig2vwjjkvwnjymph5jfq") (rust-version "1.65.0")))

