(define-module (crates-io rw ms) #:use-module (crates-io))

(define-public crate-rwmstatus-0.1 (crate (name "rwmstatus") (vers "0.1.0") (deps (list (crate-dep (name "chrono") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "chrono-tz") (req "^0.5") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "x11") (req "^2") (default-features #t) (kind 0)))) (hash "18ssf2ws1gr2njl8mippzk5fgwpkkqp95yl1n3aa2dncvyy8w8ax") (links "X11")))

(define-public crate-rwmstatus-1 (crate (name "rwmstatus") (vers "1.0.0") (deps (list (crate-dep (name "chrono") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "chrono-tz") (req "^0.5") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "x11") (req "^2") (default-features #t) (kind 0)))) (hash "1fardnrhjprm6rhxhll0h8nq5h6nc55n3bmmcr32i0z9f5bhc78i") (links "X11")))

