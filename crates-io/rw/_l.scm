(define-module (crates-io rw _l) #:use-module (crates-io))

(define-public crate-rw_lease-0.1 (crate (name "rw_lease") (vers "0.1.0") (deps (list (crate-dep (name "atomic_prim_traits") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "primitive_traits") (req "^0.2") (default-features #t) (kind 0)))) (hash "04969d6db0b17gxsjp2irdkfrvnsd4aw1lyj8irk1x9snkzm98yk") (features (quote (("default"))))))

