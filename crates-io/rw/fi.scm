(define-module (crates-io rw fi) #:use-module (crates-io))

(define-public crate-rwfile-0.1 (crate (name "rwfile") (vers "0.1.0") (deps (list (crate-dep (name "rand") (req "^0.7.3") (default-features #t) (kind 2)) (crate-dep (name "tempfile") (req "^3.1.0") (default-features #t) (kind 2)))) (hash "031hc5ngwq4lwipqmm9xxvacjxmr7bd3gpxj591yzlz7bmi8rci3")))

