(define-module (crates-io rw lo) #:use-module (crates-io))

(define-public crate-rwlock2-0.1 (crate (name "rwlock2") (vers "0.1.0") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.3") (default-features #t) (kind 2)))) (hash "0q3hhz13cgaxng4p836fgzy5zl3b2rj9yfzymrfibninfz3vvf59")))

(define-public crate-rwlock2-0.1 (crate (name "rwlock2") (vers "0.1.1") (deps (list (crate-dep (name "kernel32-sys") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "winapi") (req "^0.2") (default-features #t) (kind 0)))) (hash "0vyshw3lg84vyrg0j5df94b19y65iq9ilqg5cdd687d4dwkzpr78")))

(define-public crate-rwlock2-0.1 (crate (name "rwlock2") (vers "0.1.2") (deps (list (crate-dep (name "kernel32-sys") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "winapi") (req "^0.2") (default-features #t) (kind 0)))) (hash "1w4fis415l7vr6g6s0l94jdikl1d3a6c4y02hs4mwwbyf8wp7f9f")))

(define-public crate-rwlock2-0.2 (crate (name "rwlock2") (vers "0.2.0") (deps (list (crate-dep (name "kernel32-sys") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "winapi") (req "^0.2") (default-features #t) (kind 0)))) (hash "1p5w4fn2rbnz135lc3x6z7hcv60g2xw1rzr9r36kqi88bdhvi0i8")))

(define-public crate-rwlock2-0.2 (crate (name "rwlock2") (vers "0.2.1") (deps (list (crate-dep (name "kernel32-sys") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "winapi") (req "^0.2") (default-features #t) (kind 0)))) (hash "1qfzq3idkv03d4pmlxh7jpnp7wwcrmilqhxn34izmy1mamz6cmq1")))

(define-public crate-rwlock2-0.2 (crate (name "rwlock2") (vers "0.2.2") (deps (list (crate-dep (name "kernel32-sys") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "winapi") (req "^0.2") (default-features #t) (kind 0)))) (hash "17xih7441bpbkmlzq7hc92nk8lay5fs6wwlha7l44pakk2sycxn9")))

(define-public crate-rwlock2-0.2 (crate (name "rwlock2") (vers "0.2.3") (deps (list (crate-dep (name "kernel32-sys") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "winapi") (req "^0.2") (default-features #t) (kind 0)))) (hash "1gzay4a02cssxdiyy6gxl9l90zjvxy0cl8i675jz988vsfpjlp5m")))

(define-public crate-rwlock2-0.2 (crate (name "rwlock2") (vers "0.2.4") (deps (list (crate-dep (name "kernel32-sys") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "winapi") (req "^0.2") (default-features #t) (kind 0)))) (hash "0rabivn4c1a44xgg1xdzz12qlfj8zqj978nz7dvp7g0wlhqvhaly")))

(define-public crate-rwlock2-0.2 (crate (name "rwlock2") (vers "0.2.5") (deps (list (crate-dep (name "kernel32-sys") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "winapi") (req "^0.2") (default-features #t) (kind 0)))) (hash "1znjwlz3hn5fs7y1qaiypn53n5vcgvq2yp61rih2aysy9kqp0fm8")))

