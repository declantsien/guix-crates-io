(define-module (crates-io rw ed) #:use-module (crates-io))

(define-public crate-rwedid-0.3 (crate (name "rwedid") (vers "0.3.1") (deps (list (crate-dep (name "clap") (req "^4.3.19") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "i2cdev") (req "^0.5.1") (default-features #t) (kind 0)))) (hash "1gg2mvxwf4g2jis9q5ibgdxw8cr4gsdgxzdipy46a8iir9fin85h")))

(define-public crate-rwedid-0.3 (crate (name "rwedid") (vers "0.3.2") (deps (list (crate-dep (name "clap") (req "^4.3.19") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "i2cdev") (req "^0.5.1") (default-features #t) (kind 0)))) (hash "0vaxkap9lv03bqjha2jvvbgdrk4d7qqxyw2w8fgfl6b3nfj039vm")))

