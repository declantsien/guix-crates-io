(define-module (crates-io rw -c) #:use-module (crates-io))

(define-public crate-rw-cell-0.1 (crate (name "rw-cell") (vers "0.1.2") (hash "1rghvb3p4k5ibckz78dhc2fc1jv9y3gmf8757zn8aa512p3acir9") (yanked #t)))

(define-public crate-rw-cell-0.1 (crate (name "rw-cell") (vers "0.1.3") (hash "0iaadyx944gyrhgza160pb3hd6bq35y7cr53pd48777vs6ijqy6s") (yanked #t)))

(define-public crate-rw-cell-0.1 (crate (name "rw-cell") (vers "0.1.4") (hash "0zbvwhcixivgy28gg3lbdzp4ifiwqkfpm4qjn9v4sgajldvagfyc") (yanked #t)))

(define-public crate-rw-cell-0.1 (crate (name "rw-cell") (vers "0.1.5") (hash "0g95d7j7b8c01d4w8q9cjrr4khx72yp463iqx46m39a2pw0id779") (yanked #t)))

(define-public crate-rw-cell-0.2 (crate (name "rw-cell") (vers "0.2.0") (hash "1g3v33s9b08xjzxwqg7q69p7gvfd50ylsbqajvql1igc06334g0g")))

(define-public crate-rw-cell-0.3 (crate (name "rw-cell") (vers "0.3.0") (hash "0m34rchsh2305p5rq4alkd19kdkah961rp7kjak05r8cvryjhcjq")))

(define-public crate-rw-cell-0.3 (crate (name "rw-cell") (vers "0.3.1") (hash "1g7c19n5m3p0zmmbhmx1xgqr9hf9smn2bsigp0qihp964fplb8af")))

(define-public crate-rw-cell-0.3 (crate (name "rw-cell") (vers "0.3.2") (hash "1chyxj38yg9cqrxm5kfpmsczlbkwsksjs71mjjrk3c1dk1m48sxb")))

(define-public crate-rw-cell-1 (crate (name "rw-cell") (vers "1.0.0") (hash "19i65fb8v71nwjwms2vivb81y50vdy8ca90m1qh9glmraf27wwvg")))

(define-public crate-rw-cell-2 (crate (name "rw-cell") (vers "2.0.0") (hash "1srdnhfh51d3ql48llvysx0ygsdmm19hpcyd30myn4ij64z86dx1")))

(define-public crate-rw-cell-2 (crate (name "rw-cell") (vers "2.0.1") (hash "1szf6hvh4zrn2pjyxj3h0gm5hgdzq5yhm6g2nkjl7l1fz1xi23lw")))

