(define-module (crates-io rw in) #:use-module (crates-io))

(define-public crate-rwini-0.1 (crate (name "rwini") (vers "0.1.0") (deps (list (crate-dep (name "serde") (req "^1.0.190") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "0xc65whwb1v5k0gx590mnl106wm0k20s1jd62n6rxfa1r93az6by")))

(define-public crate-rwini-0.1 (crate (name "rwini") (vers "0.1.1") (deps (list (crate-dep (name "serde") (req "^1") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "0b6n8ys2r5hvxyyd3lh9xxzh8m0wf86cfym7a76lvnqsrnadg8q6")))

