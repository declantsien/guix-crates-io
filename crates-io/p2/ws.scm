(define-module (crates-io p2 ws) #:use-module (crates-io))

(define-public crate-p2wsh-utxo-0.1 (crate (name "p2wsh-utxo") (vers "0.1.0") (deps (list (crate-dep (name "anyhow") (req "^1.0.56") (default-features #t) (kind 0)) (crate-dep (name "bincode") (req "^1.3.3") (default-features #t) (kind 0)) (crate-dep (name "csv") (req "^1.1") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.136") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)))) (hash "001fnnv2qhi0rclspmmsknfnf43vwwciwbhwammli52j4jrv9fzb")))

