(define-module (crates-io p2 -m) #:use-module (crates-io))

(define-public crate-p2-models-rs-0.1 (crate (name "p2-models-rs") (vers "0.1.0") (deps (list (crate-dep (name "chrono") (req "^0.4.19") (features (quote ("serde" "rustc-serialize"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.123") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0.123") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.64") (default-features #t) (kind 0)) (crate-dep (name "sqlx") (req "^0.5.7") (features (quote ("runtime-actix-rustls" "postgres" "chrono" "offline"))) (default-features #t) (kind 0)))) (hash "0c2v5arigg1q81sha0pqc1d18zrlbfs9dwh1vacp6nl2si7j085j")))

