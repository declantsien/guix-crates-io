(define-module (crates-io p2 p_) #:use-module (crates-io))

(define-public crate-p2p_communication-0.1 (crate (name "p2p_communication") (vers "0.1.0") (deps (list (crate-dep (name "tokio") (req "^1") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "0b2d4zldl0rn2bfrsfvknixyarwrjy94pay9ih6cmxx2wpviv0f8")))

