(define-module (crates-io fb en) #:use-module (crates-io))

(define-public crate-fbenv-0.1 (crate (name "fbenv") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^2.32.0") (default-features #t) (kind 0)) (crate-dep (name "toml") (req "^0.4.6") (default-features #t) (kind 0)))) (hash "12s34pcj61x172x7sbgmp1f262damlrlw0l9sqixn2zysbkb53fv")))

(define-public crate-fbenv-0.1 (crate (name "fbenv") (vers "0.1.1") (deps (list (crate-dep (name "clap") (req "^2.32.0") (default-features #t) (kind 0)) (crate-dep (name "colored") (req "^1.6") (default-features #t) (kind 0)) (crate-dep (name "toml") (req "^0.4.6") (default-features #t) (kind 0)))) (hash "02fn76pbhppkbr3wnwcr848nrrc8vwkwk4xzjihhnp29vkxny6jb")))

(define-public crate-fbenv-0.1 (crate (name "fbenv") (vers "0.1.2") (deps (list (crate-dep (name "clap") (req "^2.32.0") (default-features #t) (kind 0)) (crate-dep (name "colored") (req "^1.6") (default-features #t) (kind 0)) (crate-dep (name "toml") (req "^0.4.6") (default-features #t) (kind 0)))) (hash "1jzc0kqwzxp1fjkrfqznrb6pimfxcizma7nbwvp48ar6p5qqx75g")))

(define-public crate-fbenv-0.1 (crate (name "fbenv") (vers "0.1.3") (deps (list (crate-dep (name "clap") (req "^2.32.0") (default-features #t) (kind 0)) (crate-dep (name "colored") (req "^1.6") (default-features #t) (kind 0)) (crate-dep (name "toml") (req "^0.4.6") (default-features #t) (kind 0)))) (hash "05mx4daqxgiyr5qqh1dma5fr48n4f8pi39s0zdr5inyyln8h13ag")))

(define-public crate-fbenv-0.1 (crate (name "fbenv") (vers "0.1.4") (deps (list (crate-dep (name "clap") (req "^2.32.0") (default-features #t) (kind 0)) (crate-dep (name "colored") (req "^1.6") (default-features #t) (kind 0)) (crate-dep (name "maplit") (req "^1.0.1") (default-features #t) (kind 0)) (crate-dep (name "spectral") (req "^0.6.0") (default-features #t) (kind 0)) (crate-dep (name "spectral") (req "^0.6.0") (default-features #t) (kind 2)) (crate-dep (name "toml") (req "^0.4.6") (default-features #t) (kind 0)))) (hash "05sxkwb8h5vagzhpjivrbz3wnrj75x6yl3w0n8fcinymiz1fqsx2")))

(define-public crate-fbenv-0.1 (crate (name "fbenv") (vers "0.1.5") (deps (list (crate-dep (name "clap") (req "^2.32.0") (default-features #t) (kind 0)) (crate-dep (name "colored") (req "^1.6") (default-features #t) (kind 0)) (crate-dep (name "maplit") (req "^1.0.1") (default-features #t) (kind 0)) (crate-dep (name "spectral") (req "^0.6.0") (default-features #t) (kind 0)) (crate-dep (name "spectral") (req "^0.6.0") (default-features #t) (kind 2)) (crate-dep (name "toml") (req "^0.4.6") (default-features #t) (kind 0)))) (hash "0bxyaa438jzhqyy1aqxflancd2pd9vvy4r4b478pj7ks5851jidg")))

