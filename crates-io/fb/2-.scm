(define-module (crates-io fb #{2-}#) #:use-module (crates-io))

(define-public crate-fb2-rs-0.1 (crate (name "fb2-rs") (vers "0.1.0") (deps (list (crate-dep (name "xml-rs") (req "^0.8") (default-features #t) (kind 0)))) (hash "0j1gn5m4jw256r3ig1h9vg5qlpypcgfijfgnxkajmspcl954fwpv")))

