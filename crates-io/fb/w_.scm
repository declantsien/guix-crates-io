(define-module (crates-io fb w_) #:use-module (crates-io))

(define-public crate-fbw_map_parser-1 (crate (name "fbw_map_parser") (vers "1.0.0") (deps (list (crate-dep (name "zune-inflate") (req "^0.2") (features (quote ("gzip"))) (kind 0)))) (hash "09bjsihrwh27sknlzfbnkgjw3akbzkyv834c9h3ril6c13z93yh2")))

