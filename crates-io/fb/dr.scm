(define-module (crates-io fb dr) #:use-module (crates-io))

(define-public crate-fbdraw-0.1 (crate (name "fbdraw") (vers "0.1.0") (deps (list (crate-dep (name "minifb") (req "^0.22") (default-features #t) (kind 0)))) (hash "05f599n81ac970bds53n6yibzvhm5wj2l45cdw3y3pcqdshzzjgw")))

