(define-module (crates-io fb xs) #:use-module (crates-io))

(define-public crate-fbxsdk-sys-0.0.0 (crate (name "fbxsdk-sys") (vers "0.0.0") (hash "19x2pmfirf9kl367nlafplmjgh7cavk3mf093597p06lqqa11828")))

(define-public crate-fbxsdk-sys-0.1 (crate (name "fbxsdk-sys") (vers "0.1.0") (deps (list (crate-dep (name "bindgen") (req "^0.51.1") (default-features #t) (kind 1)) (crate-dep (name "cc") (req "^1.0.45") (default-features #t) (kind 1)) (crate-dep (name "env_logger") (req "^0.7.0") (default-features #t) (kind 1)))) (hash "1yf4xl86jfjlp5mk16y49cqrds2gd3ckdpy01ahspld5yral05y6")))

