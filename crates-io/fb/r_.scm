(define-module (crates-io fb r_) #:use-module (crates-io))

(define-public crate-fbr_cache-0.1 (crate (name "fbr_cache") (vers "0.1.0") (deps (list (crate-dep (name "intrusive-collections") (req "^0.9.4") (default-features #t) (kind 0)))) (hash "1a36g62pizb1cc1pwgp1frgj4dl6s2ns1saq0mqy8hjpklv38zg2")))

(define-public crate-fbr_cache-0.1 (crate (name "fbr_cache") (vers "0.1.1") (deps (list (crate-dep (name "intrusive-collections") (req "^0.9.4") (default-features #t) (kind 0)))) (hash "1gb42zmmfb2mlngdjnrxsqnr6k85mflmsrrjvlwvs8b7jp25v6wm")))

