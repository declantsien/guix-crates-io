(define-module (crates-io fb -t) #:use-module (crates-io))

(define-public crate-fb-test-0.1 (crate (name "fb-test") (vers "0.1.0") (deps (list (crate-dep (name "find-big-file") (req "^0") (default-features #t) (kind 0)))) (hash "1v2m7r09171dn1pniq3v31lmbh9lk6c8j2n4py55di6kbsv6srpv")))

(define-public crate-fb-test-0.1 (crate (name "fb-test") (vers "0.1.1") (deps (list (crate-dep (name "find-big-file") (req "^0") (default-features #t) (kind 0)))) (hash "0jyw9v6kdji6gz5r795fvrxl35359b0rya50pawd9h64sdm1fkqg")))

(define-public crate-fb-test-0.1 (crate (name "fb-test") (vers "0.1.2") (deps (list (crate-dep (name "find-big-file") (req "^0") (default-features #t) (kind 0)))) (hash "0n5v925956yckv1kq3mgs7n6m5l0dwx9asl78shvma6gfwhsjr98")))

(define-public crate-fb-test-0.1 (crate (name "fb-test") (vers "0.1.3") (deps (list (crate-dep (name "find-big-file") (req "^0.1.6") (default-features #t) (kind 0)))) (hash "0zvzs5vhidls949fvdihr1yy8nip6j48xvkbwq2kahfi2qjq708a")))

