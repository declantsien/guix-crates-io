(define-module (crates-io fb sd) #:use-module (crates-io))

(define-public crate-fbsd-geom-sys-0.1 (crate (name "fbsd-geom-sys") (vers "0.1.0") (deps (list (crate-dep (name "bindgen") (req "^0.55.1") (default-features #t) (kind 1)))) (hash "02iw756shvfkzi50w3bnir9jzgql0xy47nzlz81jk57snswgrvcd")))

(define-public crate-fbsd-geom-sys-0.1 (crate (name "fbsd-geom-sys") (vers "0.1.1") (deps (list (crate-dep (name "bindgen") (req "^0.55.1") (default-features #t) (kind 1)))) (hash "0xlsl661mij0qnzmdjp7z2cknwg897g2jn58jlpz89f1capf7yr3")))

