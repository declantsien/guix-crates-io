(define-module (crates-io fb vi) #:use-module (crates-io))

(define-public crate-fbvideo-0.1 (crate (name "fbvideo") (vers "0.1.0") (deps (list (crate-dep (name "lazy_static") (req "^1.3") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.9") (default-features #t) (kind 0)))) (hash "0k0qib31a3w64jgcrrhrb1kkk4jjmqgr97iq24yn0s46w83ch62w")))

(define-public crate-fbvideo-0.1 (crate (name "fbvideo") (vers "0.1.1") (deps (list (crate-dep (name "lazy_static") (req "^1.3") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.9") (default-features #t) (kind 0)))) (hash "1bxbq5dzhh5jqyy330k38qhhmd7rpyyvbzh1ikmps6aj453qmd68")))

(define-public crate-fbvideo-0.2 (crate (name "fbvideo") (vers "0.2.0") (deps (list (crate-dep (name "lazy_static") (req "^1.3") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.9") (default-features #t) (kind 0)))) (hash "1b7jb72d0s82wlb1qn73hggxbywlkkka99jwpp1r40dnr7dvhfgg")))

(define-public crate-fbvideo-0.3 (crate (name "fbvideo") (vers "0.3.0") (deps (list (crate-dep (name "lazy_static") (req "^1.3") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.9") (default-features #t) (kind 0)))) (hash "1a9wrngkfdnaj0pis4qg5cfhhxas7xdzfk7ymqrrms96wg5drs49")))

(define-public crate-fbvideo-0.3 (crate (name "fbvideo") (vers "0.3.1") (deps (list (crate-dep (name "lazy_static") (req "^1.3") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.9") (default-features #t) (kind 0)))) (hash "1h1cyz7i4ahqdqsnwpmqkfqkhz4x3d3mfkqyyaqa1xv2y27gly0s")))

(define-public crate-fbvideo-0.3 (crate (name "fbvideo") (vers "0.3.2") (deps (list (crate-dep (name "lazy_static") (req "^1.3") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.9") (default-features #t) (kind 0)))) (hash "09l9f36h9zlkvq7zxf32bp47ic9vc1qcggsjc1vvpbfqwylcf8m3")))

(define-public crate-fbvideo-0.4 (crate (name "fbvideo") (vers "0.4.0") (deps (list (crate-dep (name "once_cell") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1") (features (quote ("std" "perf"))) (kind 0)) (crate-dep (name "reqwest") (req "^0.10.0") (features (quote ("default-tls" "gzip"))) (kind 0)))) (hash "1q0msgkiqwhqkxl0d53fa01vww3s5jwjjc1a183lpyk25ya4f79h")))

(define-public crate-fbvideo-0.4 (crate (name "fbvideo") (vers "0.4.1") (deps (list (crate-dep (name "once_cell") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1") (features (quote ("std" "perf"))) (kind 0)) (crate-dep (name "reqwest") (req "^0.10.0") (features (quote ("default-tls" "gzip"))) (kind 0)))) (hash "1v69capcpqby7vpylp4kbzns3f9ymjn11p1076r5dnj29z73hpha")))

