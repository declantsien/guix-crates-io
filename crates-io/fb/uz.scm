(define-module (crates-io fb uz) #:use-module (crates-io))

(define-public crate-fbuzhash-0.0.1 (crate (name "fbuzhash") (vers "0.0.1") (deps (list (crate-dep (name "env_logger") (req "^0.7") (default-features #t) (kind 2)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)))) (hash "1k4chr4ky7sg9mz8xddpvx1zp6q5bxy3iainaljixrg2xq4j04fd")))

