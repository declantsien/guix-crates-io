(define-module (crates-io fb d-) #:use-module (crates-io))

(define-public crate-fbd-rs-0.1 (crate (name "fbd-rs") (vers "0.1.0") (deps (list (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)))) (hash "171f8xfbp3ydwv1z1xkahc5il1dsr0l4qn636dhmh4kg17zwz7px")))

(define-public crate-fbd-rs-0.1 (crate (name "fbd-rs") (vers "0.1.1") (deps (list (crate-dep (name "chrono") (req "^0.4.38") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)))) (hash "1mnlpjj1a8jccsj250rprrnaw1g254p59i9w9jq2l4zj17idgnhg")))

(define-public crate-fbd-rs-0.1 (crate (name "fbd-rs") (vers "0.1.2") (deps (list (crate-dep (name "chrono") (req "^0.4.38") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)))) (hash "02nzd0rwl940vihfcyv9zy15g09k0khpl5d64xqz35ag3y7m1bww")))

(define-public crate-fbd-rs-0.1 (crate (name "fbd-rs") (vers "0.1.3") (deps (list (crate-dep (name "chrono") (req "^0.4.38") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)))) (hash "1m97zym6c0b5b3ni3bqs45nm2inwnys0n8ckfvclpispsqvpvh6k")))

