(define-module (crates-io fb sp) #:use-module (crates-io))

(define-public crate-fbspinner-0.1 (crate (name "fbspinner") (vers "0.1.0") (deps (list (crate-dep (name "flate2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "0ygwm60wkc3fk0ksr2a06vzdfrbv92azawh2k8big7jnm3ksznx3")))

