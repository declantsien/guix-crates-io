(define-module (crates-io fb x3) #:use-module (crates-io))

(define-public crate-fbx3d-0.1 (crate (name "fbx3d") (vers "0.1.0") (deps (list (crate-dep (name "bytepack") (req "^0.1.1") (default-features #t) (kind 0)) (crate-dep (name "flate2") (req "^0.2") (default-features #t) (kind 0)))) (hash "0rjgcb5jw7r4i6z165p9cd7r69kbis0m139iyzln7xz4z1mnxhph")))

