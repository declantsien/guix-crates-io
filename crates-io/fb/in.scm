(define-module (crates-io fb in) #:use-module (crates-io))

(define-public crate-fbinit-0.1 (crate (name "fbinit") (vers "0.1.0") (deps (list (crate-dep (name "fbinit_macros") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "quickcheck") (req "^0.9") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "=0.2.13") (features (quote ("full"))) (default-features #t) (kind 2)))) (hash "1p3amqnp3f5yxhwpvhqwdc7xw9s9nrqqq818k76kfghy1w9s37bf")))

(define-public crate-fbinit-0.1 (crate (name "fbinit") (vers "0.1.1") (deps (list (crate-dep (name "fbinit_macros") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "quickcheck") (req "^0.9") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "=0.2.13") (features (quote ("full"))) (default-features #t) (kind 2)))) (hash "1ypvn8k0ry7530xykk8khj3595bf3m2k7l9dlvgsxpd6ah1cpndw")))

(define-public crate-fbinit-0.1 (crate (name "fbinit") (vers "0.1.2") (deps (list (crate-dep (name "fbinit_macros") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "quickcheck") (req "^0.9") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "=0.2.13") (features (quote ("full"))) (default-features #t) (kind 2)))) (hash "1m1h9gwbc4vxc1jllmamq14v7lr9g04912f8fdch5zr6dl6cc6v6")))

(define-public crate-fbinit-0.2 (crate (name "fbinit") (vers "0.2.0") (deps (list (crate-dep (name "fbinit-tokio") (req "^0.1") (default-features #t) (kind 2)) (crate-dep (name "fbinit_macros") (req "=0.2.0") (default-features #t) (kind 0)) (crate-dep (name "quickcheck") (req "^1.0") (default-features #t) (kind 0)))) (hash "1652411qfjrjr017rnlixp44qay502ypb3rypb54ld933k7b5ypr")))

(define-public crate-fbinit-tokio-0.1 (crate (name "fbinit-tokio") (vers "0.1.0") (deps (list (crate-dep (name "tokio") (req "^1.29.1") (features (quote ("full" "test-util" "tracing"))) (default-features #t) (kind 0)))) (hash "14jwk634746mlfdsbfalbmjqld4i3liz7ip4yyjzhidsyakjyz9i")))

(define-public crate-fbinit_macros-0.1 (crate (name "fbinit_macros") (vers "0.1.0") (deps (list (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (features (quote ("full" "visit" "visit-mut" "fold" "extra-traits"))) (default-features #t) (kind 0)))) (hash "0fz5frsh1cjk8radw76hbhx9i06y110dg0cqdlwhkncj7mgmi0lr")))

(define-public crate-fbinit_macros-0.1 (crate (name "fbinit_macros") (vers "0.1.1") (deps (list (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (features (quote ("full" "visit" "visit-mut" "fold" "extra-traits"))) (default-features #t) (kind 0)))) (hash "1afcczhcanq5gwlgwgja3660san2kbpjd15sc36czzfmgi9f02cx")))

(define-public crate-fbinit_macros-0.1 (crate (name "fbinit_macros") (vers "0.1.2") (deps (list (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (features (quote ("full" "visit" "visit-mut" "fold" "extra-traits"))) (default-features #t) (kind 0)))) (hash "1mc9calg0xf0ygfs439l9z3b2dgw6j8565ymmqffdxa8afwal2da")))

(define-public crate-fbinit_macros-0.2 (crate (name "fbinit_macros") (vers "0.2.0") (deps (list (crate-dep (name "proc-macro2") (req "^1.0.70") (features (quote ("span-locations"))) (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.29") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.109") (features (quote ("extra-traits" "fold" "full" "visit" "visit-mut"))) (default-features #t) (kind 0)))) (hash "10vhbhmmicgkx0b5pkk95b0vl9c4c2kb8q68jbr8hd2cna0q46qf")))

