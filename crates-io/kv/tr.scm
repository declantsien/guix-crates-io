(define-module (crates-io kv tr) #:use-module (crates-io))

(define-public crate-kvtree-0.1 (crate (name "kvtree") (vers "0.1.0") (deps (list (crate-dep (name "ahash") (req "^0.8.3") (default-features #t) (kind 0)) (crate-dep (name "indexmap") (req "^2.1.0") (default-features #t) (kind 0)) (crate-dep (name "itertools") (req "^0.11.0") (default-features #t) (kind 0)) (crate-dep (name "rayon") (req "^1.7.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "tuplify") (req "^1.1.4") (default-features #t) (kind 0)))) (hash "1zzq1xf7z908dlw8cb9id6a1sbrhk0far4grxl0i3zkcydp4hvyi") (features (quote (("default" "rayon" "core-storage") ("core-storage")))) (v 2) (features2 (quote (("rayon" "dep:rayon" "indexmap/rayon"))))))

