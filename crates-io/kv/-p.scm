(define-module (crates-io kv -p) #:use-module (crates-io))

(define-public crate-kv-par-merge-sort-0.1 (crate (name "kv-par-merge-sort") (vers "0.1.0") (deps (list (crate-dep (name "bytemuck") (req "^1.9") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^3.1") (features (quote ("derive"))) (default-features #t) (kind 2)) (crate-dep (name "crossbeam-channel") (req "^0.5") (default-features #t) (kind 0)) (crate-dep (name "env_logger") (req "^0.9") (default-features #t) (kind 2)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8") (features (quote ("small_rng"))) (default-features #t) (kind 2)) (crate-dep (name "tempfile") (req "^3.3") (default-features #t) (kind 0)) (crate-dep (name "tracing") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "tracing-chrome") (req "^0.6") (default-features #t) (kind 2)) (crate-dep (name "tracing-subscriber") (req "^0.3") (default-features #t) (kind 2)))) (hash "1w833x4wrb3ds869bwkspbv1di9f0nyi7513dkqmdvbdrnhddayp")))

(define-public crate-kv-parser-0.1 (crate (name "kv-parser") (vers "0.1.0") (hash "0fr3szv46kxbr3rlly5kcricss14i9hqw0vlrg29c95g2glkr7sz")))

