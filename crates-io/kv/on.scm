(define-module (crates-io kv on) #:use-module (crates-io))

(define-public crate-kvon-rs-0.1 (crate (name "kvon-rs") (vers "0.1.0") (deps (list (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.6.0") (default-features #t) (kind 0)))) (hash "1jgazi63rhgr04rz1ij1fcdwxch54j1cnd29dcqzi11fyfcb9q5b")))

(define-public crate-kvon-rs-0.2 (crate (name "kvon-rs") (vers "0.2.0") (deps (list (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.6.0") (default-features #t) (kind 0)))) (hash "1jy3p275dx2qln83zhw7dmbbqsj083zhgpvx56bp5nh8fv57gqd9")))

(define-public crate-kvon-rs-0.3 (crate (name "kvon-rs") (vers "0.3.0") (deps (list (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.6.0") (default-features #t) (kind 0)))) (hash "0jk65xjh0r485glcp58s4xmb4clbi21jg4580ayd8dfx2skmbsh4")))

(define-public crate-kvon-rs-0.3 (crate (name "kvon-rs") (vers "0.3.1") (deps (list (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.6.0") (default-features #t) (kind 0)))) (hash "1zy39xp6f49gfhsj4vkbk8j4jcjkws6wqc6ab8w01652nvlwqkcv")))

