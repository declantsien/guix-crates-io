(define-module (crates-io kv pt) #:use-module (crates-io))

(define-public crate-kvptree-0.1 (crate (name "kvptree") (vers "0.1.0") (deps (list (crate-dep (name "anyhow") (req "^1.0.71") (default-features #t) (kind 0)))) (hash "0bdn5skpv0cbacl3l6ylrz4cn83wxch2rdag89zbk8al24a51c9d")))

