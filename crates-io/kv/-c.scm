(define-module (crates-io kv -c) #:use-module (crates-io))

(define-public crate-kv-cli-0.1 (crate (name "kv-cli") (vers "0.1.0") (deps (list (crate-dep (name "chrono") (req "^0.4.22") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^4.0.10") (features (quote ("derive" "env"))) (default-features #t) (kind 0)) (crate-dep (name "clap_complete") (req "^4.0.6") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.17") (default-features #t) (kind 0)) (crate-dep (name "pretty_env_logger") (req "^0.4.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.145") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.85") (default-features #t) (kind 0)) (crate-dep (name "shellexpand") (req "^2.1.2") (default-features #t) (kind 0)) (crate-dep (name "walkdir") (req "^2.3.2") (default-features #t) (kind 0)))) (hash "080mnz2in4idqqaiaq2lrhjwdfhvjh1zv5mw48h2xxj7glm90xi4")))

(define-public crate-kv-crud-core-0.1 (crate (name "kv-crud-core") (vers "0.1.0") (hash "1357qfkjkgbsfrjcny96fnk0arfz3l1nkskb0nfnlpcvslff35lh")))

(define-public crate-kv-crud-core-0.1 (crate (name "kv-crud-core") (vers "0.1.1") (hash "1929hcs6ffkkmxk20s1g7rddsfy3cy4s168igxv63dpjd84skb8b")))

(define-public crate-kv-crud-memory-0.1 (crate (name "kv-crud-memory") (vers "0.1.0") (deps (list (crate-dep (name "failure") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "kv-crud-core") (req "^0.1") (default-features #t) (kind 0)))) (hash "1fx6nxwb0ipv88yzi9i971nmsjvqmyiq5gq4004yx77bp5qj83rg")))

