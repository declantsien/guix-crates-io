(define-module (crates-io kv fi) #:use-module (crates-io))

(define-public crate-kvfilter-0.2 (crate (name "kvfilter") (vers "0.2.0") (deps (list (crate-dep (name "slog") (req "^2") (default-features #t) (kind 0)))) (hash "1drsijcn9wfdfh2ms454giv6br6m123frwwjylznz0rh6n3ifwwa") (yanked #t)))

(define-public crate-kvfilter-0.3 (crate (name "kvfilter") (vers "0.3.0") (deps (list (crate-dep (name "slog") (req "^2") (default-features #t) (kind 0)))) (hash "1aa3cafvx0c7k4crdmm0np9vmsqhkjsxll8vs4j78j3igj8xydn8") (yanked #t)))

(define-public crate-kvfilter-0.3 (crate (name "kvfilter") (vers "0.3.1") (deps (list (crate-dep (name "slog") (req "^2") (default-features #t) (kind 0)))) (hash "1pvlbfwzw1g48676qr66m62bmdqx0drp087sac3nplz1yyq80vyf") (yanked #t)))

