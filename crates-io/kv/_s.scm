(define-module (crates-io kv _s) #:use-module (crates-io))

(define-public crate-kv_store-0.1 (crate (name "kv_store") (vers "0.1.0") (deps (list (crate-dep (name "async-trait") (req "^0.1.74") (default-features #t) (kind 0)) (crate-dep (name "parking_lot") (req "^0.12.1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1") (features (quote ("full"))) (default-features #t) (kind 2)))) (hash "10f1221ilzx07h1ydcy9pgx9x9hn31h241ml1cdrmdp4nrk9wv8j") (v 2) (features2 (quote (("impl" "dep:parking_lot"))))))

(define-public crate-kv_store-0.1 (crate (name "kv_store") (vers "0.1.1") (deps (list (crate-dep (name "async-trait") (req "^0.1.74") (default-features #t) (kind 0)) (crate-dep (name "parking_lot") (req "^0.12.1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1") (features (quote ("full"))) (default-features #t) (kind 2)))) (hash "0l3g9waz3ag8ad68zap7x62jj386r4hcdz1xbrxdyjhzvgjrj25m") (v 2) (features2 (quote (("impl" "dep:parking_lot"))))))

