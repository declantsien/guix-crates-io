(define-module (crates-io kv ds) #:use-module (crates-io))

(define-public crate-kvds-0.1 (crate (name "kvds") (vers "0.1.0") (hash "1vj008g1qx2y2rm8iwdpn8gsgkhc7rdhlrh4ddv025k7pkxpgv0f")))

(define-public crate-kvds-0.1 (crate (name "kvds") (vers "0.1.1") (hash "01qkrr6rbjsahiqs1zad7w2hj6swza07lqhazv7h47z5snnqxdfp")))

(define-public crate-kvds-0.1 (crate (name "kvds") (vers "0.1.2") (hash "0b4flr8d1xxwslnzrarmwqddjv2x3hbbi3dbnm98f2saawwlvnm4")))

(define-public crate-kvds-0.2 (crate (name "kvds") (vers "0.2.0") (hash "19jm4sndwz8s1rnqg34z66y99030y93l8a27xwnn611szd0vgj6k")))

