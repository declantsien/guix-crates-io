(define-module (crates-io kv st) #:use-module (crates-io))

(define-public crate-kvstore-0.1 (crate (name "kvstore") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^4.4.2") (default-features #t) (kind 0)) (crate-dep (name "home") (req "^0.5.5") (default-features #t) (kind 0)) (crate-dep (name "simple_logger") (req "^4.2.0") (default-features #t) (kind 0)))) (hash "01icr8ih6yjv0f4qrlnvbjxp3hpz74kwd8w3p7xhds3537fl781a")))

(define-public crate-kvstore-0.2 (crate (name "kvstore") (vers "0.2.0") (deps (list (crate-dep (name "clap") (req "^4.4.2") (default-features #t) (kind 0)) (crate-dep (name "home") (req "^0.5.5") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.20") (default-features #t) (kind 0)) (crate-dep (name "simple_logger") (req "^4.2.0") (default-features #t) (kind 0)))) (hash "12zq9bgj7a8wfgp8af8qidypdislg58bjqqj5pxml91dc69avhk7")))

(define-public crate-kvstore-0.2 (crate (name "kvstore") (vers "0.2.1") (deps (list (crate-dep (name "clap") (req "^4.4.2") (default-features #t) (kind 0)) (crate-dep (name "home") (req "^0.5.5") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.20") (default-features #t) (kind 0)) (crate-dep (name "simple_logger") (req "^4.2.0") (default-features #t) (kind 0)))) (hash "01f97m2dg667dmsvkrmagmrb4scsbc4xc1a8s9y746f8f71i3fqh")))

(define-public crate-kvstructs-0.0.1 (crate (name "kvstructs") (vers "0.0.1") (deps (list (crate-dep (name "bytes") (req "^1.1") (kind 0)))) (hash "1hbylcz8vc00ddw2bn0507s8fh2i3y0wzp8wlp8xcm1ilr2ngb49") (features (quote (("std" "bytes/std") ("default" "std"))))))

(define-public crate-kvstructs-0.0.2 (crate (name "kvstructs") (vers "0.0.2") (deps (list (crate-dep (name "bitflags") (req "^1.3.2") (default-features #t) (kind 0)) (crate-dep (name "bytes") (req "^1.1") (kind 0)))) (hash "0mbd8x041g02fxdcixd7m2vhlqnlsn1c9yzi7nm00cb21rph0bq0") (features (quote (("std" "bytes/std") ("nightly") ("default" "std"))))))

(define-public crate-kvstructs-0.0.3 (crate (name "kvstructs") (vers "0.0.3") (deps (list (crate-dep (name "bitflags") (req "^1.3.2") (default-features #t) (kind 0)) (crate-dep (name "bytes") (req "^1.1") (kind 0)))) (hash "1mrjib7435lqycx1263i8i3g8w3158pb9d53iabq7l8rb1qqyd61") (features (quote (("std" "bytes/std") ("nightly") ("default" "std"))))))

(define-public crate-kvstructs-0.0.4 (crate (name "kvstructs") (vers "0.0.4") (deps (list (crate-dep (name "bitflags") (req "^1.3.2") (default-features #t) (kind 0)) (crate-dep (name "bytes") (req "^1.1") (kind 0)))) (hash "0hhahd3xgsvyfcg87rz0i4ln3pd9lcp8pvf05n7p26zzkh9qbkrf") (features (quote (("std" "bytes/std") ("nightly") ("default" "std"))))))

(define-public crate-kvstructs-0.0.5 (crate (name "kvstructs") (vers "0.0.5") (deps (list (crate-dep (name "bitflags") (req "^1.3.2") (default-features #t) (kind 0)) (crate-dep (name "bytes") (req "^1.1") (kind 0)))) (hash "1kbq913r2lllfvmczj0pvc36kv05zq14ji2q852k0v9ad5c0x8r6") (features (quote (("std" "bytes/std") ("nightly") ("default" "std"))))))

(define-public crate-kvstructs-0.0.6 (crate (name "kvstructs") (vers "0.0.6") (deps (list (crate-dep (name "bitflags") (req "^1.3.2") (default-features #t) (kind 0)) (crate-dep (name "bytes") (req "^1.1") (kind 0)))) (hash "0hshh2gwq6kszwv3xjvfd3qhjr5i70178s3bi10y6zrwggjg7g94") (features (quote (("std" "bytes/std") ("nightly") ("default" "std"))))))

(define-public crate-kvstructs-0.0.7 (crate (name "kvstructs") (vers "0.0.7") (deps (list (crate-dep (name "bitflags") (req "^1.3.2") (default-features #t) (kind 0)) (crate-dep (name "bytes") (req "^1.1") (kind 0)))) (hash "0syghkcaz84qb26p3zbci3diiwikgvh8s5h022crh06cy4m406y1") (features (quote (("std" "bytes/std") ("nightly") ("default" "std"))))))

(define-public crate-kvstructs-0.0.8 (crate (name "kvstructs") (vers "0.0.8") (deps (list (crate-dep (name "bitflags") (req "^1.3.2") (default-features #t) (kind 0)) (crate-dep (name "bytes") (req "^1.1") (kind 0)))) (hash "10dp1x95zx46wzak34d8km78s8as1yy3z005h6gy6z1cmp724hki") (features (quote (("std" "bytes/std") ("nightly") ("default" "std"))))))

(define-public crate-kvstructs-0.0.9 (crate (name "kvstructs") (vers "0.0.9") (deps (list (crate-dep (name "bitflags") (req "^1.3.2") (default-features #t) (kind 0)) (crate-dep (name "bytes") (req "^1.1") (kind 0)))) (hash "1hlpa6ljb7lsi2rikpbs98x9nhk193df12hhv2cgq1jfmx6zi30z") (features (quote (("std" "bytes/std") ("nightly") ("default" "std"))))))

(define-public crate-kvstructs-0.0.10 (crate (name "kvstructs") (vers "0.0.10") (deps (list (crate-dep (name "bitflags") (req "^1.3.2") (default-features #t) (kind 0)) (crate-dep (name "bytes") (req "^1.1") (kind 0)))) (hash "1jr0j1rczl0r7djnqlbd9y59bvmabzd8lpg3va27aisj4rfxqs7k") (features (quote (("std" "bytes/std") ("nightly") ("default" "std"))))))

(define-public crate-kvstructs-0.0.11 (crate (name "kvstructs") (vers "0.0.11") (deps (list (crate-dep (name "bitflags") (req "^1.3.2") (default-features #t) (kind 0)) (crate-dep (name "bytes") (req "^1.1") (kind 0)))) (hash "0qy3ng255jzy7aknw5qsl1a4xdxnp02qqxvym5mf1zvcaqbsqsh7") (features (quote (("std" "bytes/std") ("nightly") ("default" "std"))))))

(define-public crate-kvstructs-0.0.12 (crate (name "kvstructs") (vers "0.0.12") (deps (list (crate-dep (name "bitflags") (req "^1.3.2") (default-features #t) (kind 0)) (crate-dep (name "bytes") (req "^1.1") (kind 0)))) (hash "16hcz2a1kjxlrdnw35yy0vi6mjw8cf03dgxi2nnn9903ra23c97v") (features (quote (("std" "bytes/std") ("nightly") ("default" "std"))))))

(define-public crate-kvstructs-0.0.13 (crate (name "kvstructs") (vers "0.0.13") (deps (list (crate-dep (name "bitflags") (req "^1.3.2") (default-features #t) (kind 0)) (crate-dep (name "bytes") (req "^1.1") (kind 0)))) (hash "07w6qwq24fqy3frinasr8c8pj0xjscrmabsl169dvc9pnjar2ywx") (features (quote (("std" "bytes/std") ("nightly") ("default" "std"))))))

(define-public crate-kvstructs-0.0.14 (crate (name "kvstructs") (vers "0.0.14") (deps (list (crate-dep (name "bitflags") (req "^1.3.2") (default-features #t) (kind 0)) (crate-dep (name "bytes") (req "^1.1") (kind 0)))) (hash "08mjmncfdkz555yb77s6b7c4zjbz4sglny7wyh5x1d1ackn7xrnl") (features (quote (("std" "bytes/std") ("nightly") ("default" "std"))))))

(define-public crate-kvstructs-0.0.15 (crate (name "kvstructs") (vers "0.0.15") (deps (list (crate-dep (name "bitflags") (req "^1.3.2") (default-features #t) (kind 0)) (crate-dep (name "bytes") (req "^1.1") (kind 0)) (crate-dep (name "enum_dispatch") (req "^0.3") (default-features #t) (kind 0)))) (hash "03lz59nk7lr9sg5wm5gg7pq9ifg3pgqckjjhndrsd3vfnzvbdj0q") (features (quote (("std" "bytes/std") ("nightly") ("default" "std"))))))

(define-public crate-kvstructs-0.0.16 (crate (name "kvstructs") (vers "0.0.16") (deps (list (crate-dep (name "bitflags") (req "^1.3.2") (default-features #t) (kind 0)) (crate-dep (name "bytes") (req "^1.1") (kind 0)) (crate-dep (name "enum_dispatch") (req "^0.3") (default-features #t) (kind 0)))) (hash "1ws10484fxj3zyz02s8v60ybgan2gypd4hlbccabr159q6j0il7r") (features (quote (("std" "bytes/std") ("nightly") ("default" "std"))))))

(define-public crate-kvstructs-0.0.17 (crate (name "kvstructs") (vers "0.0.17") (deps (list (crate-dep (name "bitflags") (req "^1.3.2") (default-features #t) (kind 0)) (crate-dep (name "bytes") (req "^1.1") (kind 0)) (crate-dep (name "enum_dispatch") (req "^0.3") (default-features #t) (kind 0)))) (hash "15dq96c5kw2qaq5cc36yknyi37clwgbvnf9zg9xqb02xmd9qgfk3") (features (quote (("std" "bytes/std") ("nightly") ("default" "std"))))))

(define-public crate-kvstructs-0.0.18 (crate (name "kvstructs") (vers "0.0.18") (deps (list (crate-dep (name "bitflags") (req "^1.3") (default-features #t) (kind 0)) (crate-dep (name "bytes") (req "^1.1") (kind 0)) (crate-dep (name "enum_dispatch") (req "^0.3") (default-features #t) (kind 0)))) (hash "1vycblf4l3nhqnrfg2cyh5nbrw2dzbgjmwkrsrhfvfrlw0iyvk25") (features (quote (("std" "bytes/std") ("nightly") ("default" "std"))))))

(define-public crate-kvstructs-0.0.19 (crate (name "kvstructs") (vers "0.0.19") (deps (list (crate-dep (name "bitflags") (req "^1.3") (default-features #t) (kind 0)) (crate-dep (name "bytes") (req "^1.1") (kind 0)) (crate-dep (name "enum_dispatch") (req "^0.3") (default-features #t) (kind 0)))) (hash "0wddv5717xykaifx6wbrhf9s5g0zczlf6j74n7y8fa25vylpyfyk") (features (quote (("std" "bytes/std") ("nightly") ("default" "std"))))))

(define-public crate-kvstructs-0.0.20 (crate (name "kvstructs") (vers "0.0.20") (deps (list (crate-dep (name "bitflags") (req "^1.3") (default-features #t) (kind 0)) (crate-dep (name "bytes") (req "^1.1") (kind 0)) (crate-dep (name "enum_dispatch") (req "^0.3") (default-features #t) (kind 0)))) (hash "1mqlg75vb0qxnrh0mg0m02kfspzsi61v6qc5ajwpcm2gxz7idkcm") (features (quote (("std" "bytes/std") ("nightly") ("default" "std"))))))

(define-public crate-kvstructs-0.0.21 (crate (name "kvstructs") (vers "0.0.21") (deps (list (crate-dep (name "bitflags") (req "^1.3") (default-features #t) (kind 0)) (crate-dep (name "bytes") (req "^1.1") (kind 0)) (crate-dep (name "enum_dispatch") (req "^0.3") (default-features #t) (kind 0)))) (hash "1gqvb4rjwicjpzlbywwjx6gzjk8wsg4549idbpgshcgsifz2y1fp") (features (quote (("std" "bytes/std") ("nightly") ("default" "std"))))))

(define-public crate-kvstructs-0.0.22 (crate (name "kvstructs") (vers "0.0.22") (deps (list (crate-dep (name "bitflags") (req "^1.3") (default-features #t) (kind 0)) (crate-dep (name "bytes") (req "^1.1") (kind 0)) (crate-dep (name "enum_dispatch") (req "^0.3") (default-features #t) (kind 0)))) (hash "1k720z1i30ifvd8vy0ydcsacslfalsx9cyci659li10r7f7y7f3c") (features (quote (("std" "bytes/std") ("nightly") ("default" "std"))))))

(define-public crate-kvstructs-0.0.23 (crate (name "kvstructs") (vers "0.0.23") (deps (list (crate-dep (name "bitflags") (req "^1.3") (default-features #t) (kind 0)) (crate-dep (name "bytes") (req "^1.1") (kind 0)) (crate-dep (name "enum_dispatch") (req "^0.3") (default-features #t) (kind 0)))) (hash "0swx0p1s9wm0vrxjpkflik53ni5b3if2xpccadvpa0hywjssv9yd") (features (quote (("std" "bytes/std") ("nightly") ("default" "std"))))))

(define-public crate-kvstructs-0.0.24 (crate (name "kvstructs") (vers "0.0.24") (deps (list (crate-dep (name "bitflags") (req "^1.3") (default-features #t) (kind 0)) (crate-dep (name "bytes") (req "^1.1") (kind 0)) (crate-dep (name "enum_dispatch") (req "^0.3") (default-features #t) (kind 0)))) (hash "0dk65gxy3ch3b2v5ij4zqs77bbmc02z2a91wff7bnyx472k7d47r") (features (quote (("std" "bytes/std") ("nightly") ("default" "std"))))))

(define-public crate-kvstructs-0.1 (crate (name "kvstructs") (vers "0.1.0") (deps (list (crate-dep (name "bitflags") (req "^1.3") (default-features #t) (kind 0)) (crate-dep (name "bytes") (req "^1.2") (kind 0)) (crate-dep (name "enum_dispatch") (req "^0.3") (default-features #t) (kind 0)))) (hash "1w6qhh988jpni0df5rn0qfdgqgd841y0r4hl7zddcwlvjfi2krgw") (features (quote (("std" "bytes/std") ("nightly") ("default" "std"))))))

(define-public crate-kvstructs-0.1 (crate (name "kvstructs") (vers "0.1.1") (deps (list (crate-dep (name "bitflags") (req "^1.3") (default-features #t) (kind 0)) (crate-dep (name "bytes") (req "^1.2") (kind 0)) (crate-dep (name "enum_dispatch") (req "^0.3") (default-features #t) (kind 0)))) (hash "0g608phzi6h5m9cxs1z5jpnd34dcn59d4b7mz1am94fsyqibipmr") (features (quote (("std" "bytes/std") ("nightly") ("default" "std"))))))

