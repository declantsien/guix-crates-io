(define-module (crates-io kv -d) #:use-module (crates-io))

(define-public crate-kv-derive-0.1 (crate (name "kv-derive") (vers "0.1.0-alpha.1") (deps (list (crate-dep (name "anyhow") (req "^1.0.57") (default-features #t) (kind 0)) (crate-dep (name "darling") (req "^0.14.1") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.18") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.94") (default-features #t) (kind 0)))) (hash "0m5za0hzc75fglws50imlnfyn8cl58zs5phiwwy8c4192zzpija4") (features (quote (("default"))))))

(define-public crate-kv-derive-0.1 (crate (name "kv-derive") (vers "0.1.0-alpha.2") (deps (list (crate-dep (name "anyhow") (req "^1.0.57") (default-features #t) (kind 0)) (crate-dep (name "darling") (req "^0.14.1") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.18") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.94") (default-features #t) (kind 0)))) (hash "16p1mhs1x9s3hxvgd1w81kfidqz8961qd011rhabnyzxmf1f7dvj") (features (quote (("default"))))))

(define-public crate-kv-derive-0.1 (crate (name "kv-derive") (vers "0.1.0-alpha.14") (deps (list (crate-dep (name "anyhow") (req "^1.0.57") (default-features #t) (kind 0)) (crate-dep (name "darling") (req "^0.14.1") (default-features #t) (kind 0)) (crate-dep (name "kv-derive-impl") (req "^0.1.0-alpha.13") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.18") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.94") (default-features #t) (kind 0)))) (hash "17mkih19npvzzsq6959vbbh3w8f4dv9k0nqxq80bz4g0iy4zihrn") (features (quote (("default"))))))

(define-public crate-kv-derive-0.1 (crate (name "kv-derive") (vers "0.1.0-alpha.17") (deps (list (crate-dep (name "anyhow") (req "^1.0.57") (default-features #t) (kind 0)) (crate-dep (name "darling") (req "^0.14.1") (default-features #t) (kind 0)) (crate-dep (name "kv-derive-impl") (req "^0.1.0-alpha.17") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.18") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.94") (default-features #t) (kind 0)))) (hash "04mswqb2plr4dfbz9sf52556rhl9rwp047ha3zpfcgqdbcwj7nm9") (features (quote (("default"))))))

(define-public crate-kv-derive-0.2 (crate (name "kv-derive") (vers "0.2.0-alpha.1") (deps (list (crate-dep (name "anyhow") (req "^1.0.57") (default-features #t) (kind 0)) (crate-dep (name "darling") (req "^0.14.1") (default-features #t) (kind 0)) (crate-dep (name "kv-derive-impl") (req "^0.2.0-alpha.1") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.18") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.94") (default-features #t) (kind 0)))) (hash "0kxbf8p0z6cdgc2iwskfrdbc6jb1254k31dvsbia38k2ay4hzcc2") (features (quote (("default"))))))

(define-public crate-kv-derive-0.2 (crate (name "kv-derive") (vers "0.2.0-alpha.2") (deps (list (crate-dep (name "darling") (req "^0.14.1") (default-features #t) (kind 0)) (crate-dep (name "kv-derive-impl") (req "^0.2.0-alpha.2") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.18") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.94") (default-features #t) (kind 0)))) (hash "1asnm2z86ri71al06mn7k5f8q31485b5kl12yg75dgx6jv7ypci2") (features (quote (("default"))))))

(define-public crate-kv-derive-0.2 (crate (name "kv-derive") (vers "0.2.0-alpha.3") (deps (list (crate-dep (name "darling") (req "^0.14.1") (default-features #t) (kind 0)) (crate-dep (name "kv-derive-impl") (req "^0.2.0-alpha.3") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1.0.39") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.18") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.94") (default-features #t) (kind 0)))) (hash "1d5pbjjam2qvqnfil8ygq4m17rkh3rdfxw2pc1b1lbzsf8spvz0a") (features (quote (("default"))))))

(define-public crate-kv-derive-0.3 (crate (name "kv-derive") (vers "0.3.0-alpha.1") (deps (list (crate-dep (name "kv-derive-impl") (req "^0.3.0-alpha.1") (default-features #t) (kind 0)) (crate-dep (name "kv-derive-macro") (req "^0.3.0-alpha.1") (default-features #t) (kind 0)))) (hash "1sqcsqjx8y9cn8h35n4shysk6yair4swisqj0rmz4qqp120v8z44") (features (quote (("default"))))))

(define-public crate-kv-derive-1 (crate (name "kv-derive") (vers "1.0.0-rc.1") (deps (list (crate-dep (name "kv-derive-impl") (req "^1.0.0-rc.1") (default-features #t) (kind 0)) (crate-dep (name "kv-derive-macro") (req "^1.0.0-rc.1") (default-features #t) (kind 0)))) (hash "1r4wyww9bbf1w1h84i04fb4ik0gxrsg399aygnsp81s3ilmqamlp") (features (quote (("default"))))))

(define-public crate-kv-derive-1 (crate (name "kv-derive") (vers "1.0.0-rc.4") (deps (list (crate-dep (name "kv-derive-impl") (req "^1.0.0-rc.4") (default-features #t) (kind 0)) (crate-dep (name "kv-derive-macro") (req "^1.0.0-rc.4") (default-features #t) (kind 0)))) (hash "0lg71jsvf9z69gj5n2fzs00df6a85smafbgjjx9g5qp9ab25v445") (features (quote (("default"))))))

(define-public crate-kv-derive-1 (crate (name "kv-derive") (vers "1.0.0-rc.5") (deps (list (crate-dep (name "kv-derive-impl") (req "^1.0.0-rc.5") (default-features #t) (kind 0)) (crate-dep (name "kv-derive-macro") (req "^1.0.0-rc.5") (default-features #t) (kind 0)))) (hash "10w1k6f6qny1snbbcv5pq4maq7yb6rdh2yrrq5vh9hfb9c1ia2yl") (features (quote (("default"))))))

(define-public crate-kv-derive-1 (crate (name "kv-derive") (vers "1.0.0-rc.6") (deps (list (crate-dep (name "kv-derive-impl") (req "^1.0.0-rc.6") (default-features #t) (kind 0)) (crate-dep (name "kv-derive-macro") (req "^1.0.0-rc.6") (default-features #t) (kind 0)))) (hash "0krm222iavl6n2wj9fkw9z0d9005n8ivx2q7fbjh89vj0gcrfkn2") (features (quote (("default"))))))

(define-public crate-kv-derive-1 (crate (name "kv-derive") (vers "1.0.0-rc.7") (deps (list (crate-dep (name "kv-derive-impl") (req "^1.0.0-rc.7") (default-features #t) (kind 0)) (crate-dep (name "kv-derive-macro") (req "^1.0.0-rc.7") (default-features #t) (kind 0)))) (hash "1c773w97s6j02q1pvx5lg0bf9wk26yzaadcgy5a9526gs4zvxmjk") (features (quote (("default"))))))

(define-public crate-kv-derive-1 (crate (name "kv-derive") (vers "1.0.0-rc.8") (deps (list (crate-dep (name "kv-derive-impl") (req "^1.0.0-rc.8") (default-features #t) (kind 0)) (crate-dep (name "kv-derive-macro") (req "^1.0.0-rc.8") (default-features #t) (kind 0)))) (hash "1zxscn4jq3v007l18d55zxv4yrxc1fyvd70qg8c5vmz20ckcv80v") (features (quote (("default"))))))

(define-public crate-kv-derive-1 (crate (name "kv-derive") (vers "1.0.0-rc.9") (deps (list (crate-dep (name "kv-derive-impl") (req "^1.0.0-rc.9") (default-features #t) (kind 0)) (crate-dep (name "kv-derive-macro") (req "^1.0.0-rc.9") (default-features #t) (kind 0)))) (hash "1rm001lzrpr5w15l7d5mczk6957kjmw28rck8k84cgalwx6blr1a") (features (quote (("default"))))))

(define-public crate-kv-derive-1 (crate (name "kv-derive") (vers "1.0.0") (deps (list (crate-dep (name "kv-derive-impl") (req "^1.0.0") (default-features #t) (kind 0)) (crate-dep (name "kv-derive-macro") (req "^1.0.0") (default-features #t) (kind 0)))) (hash "10lsmsf76fhr262l3wm42yjq55aiwqjvqf9bf4gi1x0infgwy76s") (features (quote (("default"))))))

(define-public crate-kv-derive-1 (crate (name "kv-derive") (vers "1.0.1") (deps (list (crate-dep (name "kv-derive-impl") (req "^1.0.1") (default-features #t) (kind 0)) (crate-dep (name "kv-derive-macro") (req "^1.0.1") (default-features #t) (kind 0)))) (hash "11g043r1nw4msbi8vk2n5phk523sc0gkfxgwkfqd4rrj48rphs1v") (features (quote (("default"))))))

(define-public crate-kv-derive-impl-0.1 (crate (name "kv-derive-impl") (vers "0.1.0-alpha.3") (deps (list (crate-dep (name "anyhow") (req "^1.0.57") (default-features #t) (kind 0)))) (hash "0l3vj7i1hnxi84nvgm06x3wq70zkjf7ay56wmiyzxq4rzr3dahrk") (features (quote (("default")))) (yanked #t)))

(define-public crate-kv-derive-impl-0.1 (crate (name "kv-derive-impl") (vers "0.1.0-alpha.4") (deps (list (crate-dep (name "anyhow") (req "^1.0.57") (default-features #t) (kind 0)))) (hash "0wxy8y3rrm9iyk2dyqwfrp84pax3xs6r5dv1sgavyivvk9mv3kam") (features (quote (("default")))) (yanked #t)))

(define-public crate-kv-derive-impl-0.1 (crate (name "kv-derive-impl") (vers "0.1.0-alpha.5") (deps (list (crate-dep (name "anyhow") (req "^1.0.57") (default-features #t) (kind 0)))) (hash "0qqycmv5y8mm255lgj6zis0g2hra09mkd7sls0jiv26j3a66bv12") (features (quote (("default"))))))

(define-public crate-kv-derive-impl-0.1 (crate (name "kv-derive-impl") (vers "0.1.0-alpha.6") (deps (list (crate-dep (name "anyhow") (req "^1.0.57") (default-features #t) (kind 0)))) (hash "1gnxajdl9fmcv8bwmwqzhcc6jxgf81nfl8zj3kpvn7hdy9idr7jr") (features (quote (("default"))))))

(define-public crate-kv-derive-impl-0.1 (crate (name "kv-derive-impl") (vers "0.1.0-alpha.7") (deps (list (crate-dep (name "anyhow") (req "^1.0.57") (default-features #t) (kind 0)))) (hash "181qdwhr0pzgq4wi1szyynh68vpgch7xiz7pv4wmkp7i110kr2r2") (features (quote (("default"))))))

(define-public crate-kv-derive-impl-0.1 (crate (name "kv-derive-impl") (vers "0.1.0-alpha.8") (deps (list (crate-dep (name "anyhow") (req "^1.0.57") (default-features #t) (kind 0)))) (hash "0dp53f5fvwky6v3555yqdzv4jdc5qihsshn5kqvw7lphnxzlz3nx") (features (quote (("default"))))))

(define-public crate-kv-derive-impl-0.1 (crate (name "kv-derive-impl") (vers "0.1.0-alpha.9") (deps (list (crate-dep (name "anyhow") (req "^1.0.57") (default-features #t) (kind 0)))) (hash "19in2zxpjmayzajcgnzy9n1xf05wnrmikxp2q34b30hlnab1p9m9") (features (quote (("default"))))))

(define-public crate-kv-derive-impl-0.1 (crate (name "kv-derive-impl") (vers "0.1.0-alpha.10") (deps (list (crate-dep (name "anyhow") (req "^1.0.57") (default-features #t) (kind 0)))) (hash "0kmbjxk3x3q65gi3qxp0cknwqs6avwf09b73d7dkxbjx3wc717s4") (features (quote (("default"))))))

(define-public crate-kv-derive-impl-0.1 (crate (name "kv-derive-impl") (vers "0.1.0-alpha.11") (deps (list (crate-dep (name "anyhow") (req "^1.0.57") (default-features #t) (kind 0)))) (hash "1jrindbrr83lmb2sgzmdy4mly5v1rfmz423qx056q9m2hwn591m1") (features (quote (("default"))))))

(define-public crate-kv-derive-impl-0.1 (crate (name "kv-derive-impl") (vers "0.1.0-alpha.12") (deps (list (crate-dep (name "anyhow") (req "^1.0.57") (default-features #t) (kind 0)))) (hash "0cj5r1lzmjd53rhacb0zm47q2i87k0klg7d3rsiprwxq551vjj5z") (features (quote (("default"))))))

(define-public crate-kv-derive-impl-0.1 (crate (name "kv-derive-impl") (vers "0.1.0-alpha.13") (deps (list (crate-dep (name "anyhow") (req "^1.0.57") (default-features #t) (kind 0)))) (hash "0fmp7s1sba0hc15mfscabjkjn13hw6mm91v5abjzzq5z7ddx7v14") (features (quote (("default"))))))

(define-public crate-kv-derive-impl-0.1 (crate (name "kv-derive-impl") (vers "0.1.0-alpha.15") (deps (list (crate-dep (name "anyhow") (req "^1.0.57") (default-features #t) (kind 0)))) (hash "011w9dkiqls64jcacvww41vhcag8c38i2dbhzv81inhwqb43s4i2") (features (quote (("default"))))))

(define-public crate-kv-derive-impl-0.1 (crate (name "kv-derive-impl") (vers "0.1.0-alpha.16") (deps (list (crate-dep (name "anyhow") (req "^1.0.57") (default-features #t) (kind 0)))) (hash "0ll07fi0xym7pl1kyqzrazjnbc4g8a1sz7ddhjs90jbg5nzq4892") (features (quote (("default"))))))

(define-public crate-kv-derive-impl-0.1 (crate (name "kv-derive-impl") (vers "0.1.0-alpha.17") (deps (list (crate-dep (name "anyhow") (req "^1.0.57") (default-features #t) (kind 0)))) (hash "056mhbn9f2n0fwffapmdkqng6lx5xd5gkl7v9ahj0hg685xc06ww") (features (quote (("default"))))))

(define-public crate-kv-derive-impl-0.2 (crate (name "kv-derive-impl") (vers "0.2.0-alpha.1") (deps (list (crate-dep (name "thiserror") (req "^1.0.31") (default-features #t) (kind 0)))) (hash "0vaciwf27qyi743hkqry8y7asynkkhfnyyzxic7500jzgw7qn809") (features (quote (("default"))))))

(define-public crate-kv-derive-impl-0.2 (crate (name "kv-derive-impl") (vers "0.2.0-alpha.2") (deps (list (crate-dep (name "thiserror") (req "^1.0.31") (default-features #t) (kind 0)))) (hash "0klr06gcfpldjflblq5vyn3il1074q5jmn47liy2j75bcwk6i2s4") (features (quote (("default"))))))

(define-public crate-kv-derive-impl-0.2 (crate (name "kv-derive-impl") (vers "0.2.0-alpha.3") (deps (list (crate-dep (name "thiserror") (req "^1.0.31") (default-features #t) (kind 0)))) (hash "1k13ayw1zjjidcl7gyc6kb1rqvljj6ia6fv3qjmg23aw642qda7a") (features (quote (("default"))))))

(define-public crate-kv-derive-impl-0.3 (crate (name "kv-derive-impl") (vers "0.3.0-alpha.1") (deps (list (crate-dep (name "thiserror") (req "^1.0.31") (default-features #t) (kind 0)))) (hash "0nffpa35177iqkihv74m7azh1r3s3692nylqv46rs6xw85qzyqsf") (features (quote (("default"))))))

(define-public crate-kv-derive-impl-1 (crate (name "kv-derive-impl") (vers "1.0.0-rc.1") (deps (list (crate-dep (name "thiserror") (req "^1.0.31") (default-features #t) (kind 0)))) (hash "0rxq5r29sk4clfbw32bkw4qzrnr4jl1fg6ciywxmbhpj3arbskcm") (features (quote (("default"))))))

(define-public crate-kv-derive-impl-1 (crate (name "kv-derive-impl") (vers "1.0.0-rc.2") (deps (list (crate-dep (name "thiserror") (req "^1.0.31") (default-features #t) (kind 0)))) (hash "1lxrzscjbnsikf91acpf16skadkqnkcr6jm73y0d0nnfilghrpfh") (features (quote (("default"))))))

(define-public crate-kv-derive-impl-1 (crate (name "kv-derive-impl") (vers "1.0.0-rc.3") (deps (list (crate-dep (name "thiserror") (req "^1.0.31") (default-features #t) (kind 0)))) (hash "0ggjv51whvwm0bw6iisikqx0dkfpy42808bsh6yzbc7f7hid8p9z") (features (quote (("default"))))))

(define-public crate-kv-derive-impl-1 (crate (name "kv-derive-impl") (vers "1.0.0-rc.4") (deps (list (crate-dep (name "thiserror") (req "^1.0.31") (default-features #t) (kind 0)))) (hash "0h549wv0lbd3d1vqxar8jcji9ap1mksfadzy4p4nk6yn8s26hxgh") (features (quote (("default"))))))

(define-public crate-kv-derive-impl-1 (crate (name "kv-derive-impl") (vers "1.0.0-rc.5") (deps (list (crate-dep (name "thiserror") (req "^1.0.31") (default-features #t) (kind 0)))) (hash "18az07gx9d4lgkbazldndgscsw0xgy2lmb6b68kdwa7didq5v5b8") (features (quote (("default"))))))

(define-public crate-kv-derive-impl-1 (crate (name "kv-derive-impl") (vers "1.0.0-rc.6") (deps (list (crate-dep (name "thiserror") (req "^1.0.31") (default-features #t) (kind 0)))) (hash "10z8wi9k6ij463mk5mijz6zrp0wajbqpnlwsxxfnd7qhjdvm1d7n") (features (quote (("default"))))))

(define-public crate-kv-derive-impl-1 (crate (name "kv-derive-impl") (vers "1.0.0-rc.7") (deps (list (crate-dep (name "thiserror") (req "^1.0.31") (default-features #t) (kind 0)))) (hash "02jms16rhm9cvf9w9hxv3cd94d7dhrhfbcpj7ah627kyd3aq28zk") (features (quote (("default"))))))

(define-public crate-kv-derive-impl-1 (crate (name "kv-derive-impl") (vers "1.0.0-rc.8") (deps (list (crate-dep (name "thiserror") (req "^1.0.31") (default-features #t) (kind 0)))) (hash "07ach59mskikmd38j065wjmacwy0xhsrcgz0qf08raqazdb533im") (features (quote (("default"))))))

(define-public crate-kv-derive-impl-1 (crate (name "kv-derive-impl") (vers "1.0.0-rc.9") (deps (list (crate-dep (name "thiserror") (req "^1.0.31") (default-features #t) (kind 0)))) (hash "1b9nfn8043vqh1lc21nvxp9ddfnxjpgqvas9b5y069dcpc2f6mry") (features (quote (("default"))))))

(define-public crate-kv-derive-impl-1 (crate (name "kv-derive-impl") (vers "1.0.0") (deps (list (crate-dep (name "thiserror") (req "^1.0.31") (default-features #t) (kind 0)))) (hash "1gcbygvsgsa9p9j49r2w6m4jdmvb8dvyqxshzrqh5c51vd9wpn20") (features (quote (("default"))))))

(define-public crate-kv-derive-impl-1 (crate (name "kv-derive-impl") (vers "1.0.1") (deps (list (crate-dep (name "thiserror") (req "^1.0.31") (default-features #t) (kind 0)))) (hash "049xpxm6b4i34j9qs3dz66kkd8xdgvx36i0jrjbjai1jb75d2vdf") (features (quote (("default"))))))

(define-public crate-kv-derive-macro-0.3 (crate (name "kv-derive-macro") (vers "0.3.0-alpha.1") (deps (list (crate-dep (name "darling") (req "^0.14.1") (default-features #t) (kind 0)) (crate-dep (name "kv-derive-impl") (req "^0.3.0-alpha.1") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1.0.39") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.18") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.94") (default-features #t) (kind 0)))) (hash "1fbk7ghs87k22dlm391c280qfwp7s01sypdnkbaf0lh5xrfp9kd6")))

(define-public crate-kv-derive-macro-1 (crate (name "kv-derive-macro") (vers "1.0.0-rc.1") (deps (list (crate-dep (name "darling") (req "^0.14.1") (default-features #t) (kind 0)) (crate-dep (name "kv-derive-impl") (req "^1.0.0-rc.1") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1.0.39") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.18") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.94") (default-features #t) (kind 0)))) (hash "0npqxxqcdqnvmhl8y7b9wrl25prdibmrmnsd35xzk3sb02ys0cj5")))

(define-public crate-kv-derive-macro-1 (crate (name "kv-derive-macro") (vers "1.0.0-rc.2") (deps (list (crate-dep (name "darling") (req "^0.14.1") (default-features #t) (kind 0)) (crate-dep (name "kv-derive-impl") (req "^1.0.0-rc.2") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1.0.39") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.18") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.94") (default-features #t) (kind 0)))) (hash "10dwfl77bgf5zy6zb90dwq79mnda64p6a48fmafaj64wa3w7vfc7")))

(define-public crate-kv-derive-macro-1 (crate (name "kv-derive-macro") (vers "1.0.0-rc.3") (deps (list (crate-dep (name "darling") (req "^0.14.1") (default-features #t) (kind 0)) (crate-dep (name "kv-derive-impl") (req "^1.0.0-rc.3") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1.0.39") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.18") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.94") (default-features #t) (kind 0)))) (hash "1ph031bm7x2v239hn0jgr7cqm75xrjqjhcbp962mwyplz5mz8cgl")))

(define-public crate-kv-derive-macro-1 (crate (name "kv-derive-macro") (vers "1.0.0-rc.4") (deps (list (crate-dep (name "darling") (req "^0.14.1") (default-features #t) (kind 0)) (crate-dep (name "kv-derive-impl") (req "^1.0.0-rc.4") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1.0.39") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.18") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.94") (default-features #t) (kind 0)))) (hash "15fn018lqz6fwpdmk0bkmm2d5paxldw9vxbrk6ydfbmqx9i2vh3l")))

(define-public crate-kv-derive-macro-1 (crate (name "kv-derive-macro") (vers "1.0.0-rc.5") (deps (list (crate-dep (name "darling") (req "^0.14.1") (default-features #t) (kind 0)) (crate-dep (name "kv-derive-impl") (req "^1.0.0-rc.5") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1.0.39") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.18") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.94") (default-features #t) (kind 0)))) (hash "0i6b26ppixrkffd5nw2l44p0fyiz93zwf6bqgvbb8mlambv14kv8")))

(define-public crate-kv-derive-macro-1 (crate (name "kv-derive-macro") (vers "1.0.0-rc.6") (deps (list (crate-dep (name "darling") (req "^0.14.1") (default-features #t) (kind 0)) (crate-dep (name "kv-derive-impl") (req "^1.0.0-rc.6") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1.0.39") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.18") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.94") (default-features #t) (kind 0)))) (hash "0d99wmswn9g519v1gp5yhba1xaglmlm5r7cpqjxv9kpngrnpj9mx")))

(define-public crate-kv-derive-macro-1 (crate (name "kv-derive-macro") (vers "1.0.0-rc.7") (deps (list (crate-dep (name "darling") (req "^0.14.1") (default-features #t) (kind 0)) (crate-dep (name "kv-derive-impl") (req "^1.0.0-rc.7") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1.0.39") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.18") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.94") (default-features #t) (kind 0)))) (hash "1lv7kfr5vrnpdxsp8jyv7ffkh2lx8civsq9k6g7sw6j6wk354qmq")))

(define-public crate-kv-derive-macro-1 (crate (name "kv-derive-macro") (vers "1.0.0-rc.8") (deps (list (crate-dep (name "darling") (req "^0.14.1") (default-features #t) (kind 0)) (crate-dep (name "kv-derive-impl") (req "^1.0.0-rc.8") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1.0.39") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.18") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.94") (default-features #t) (kind 0)))) (hash "0aff5yq4ycczz1bylk05c7kcnz335jqxgj50kfbs8li86g7aw66i")))

(define-public crate-kv-derive-macro-1 (crate (name "kv-derive-macro") (vers "1.0.0-rc.9") (deps (list (crate-dep (name "darling") (req "^0.14.1") (default-features #t) (kind 0)) (crate-dep (name "kv-derive-impl") (req "^1.0.0-rc.9") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1.0.39") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.18") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.94") (default-features #t) (kind 0)))) (hash "1dh1jwdgywr1cyxxpzvnjz7i2xxjg00fybijy3brlbcs3il0q6z0")))

(define-public crate-kv-derive-macro-1 (crate (name "kv-derive-macro") (vers "1.0.0") (deps (list (crate-dep (name "darling") (req "^0.14.1") (default-features #t) (kind 0)) (crate-dep (name "kv-derive-impl") (req "^1.0.0") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1.0.39") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.18") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.94") (default-features #t) (kind 0)))) (hash "1cjhv6jbnkzhkxxg4z2izwgkj0f0053kh399ji34ggf6vxxnl55y")))

(define-public crate-kv-derive-macro-1 (crate (name "kv-derive-macro") (vers "1.0.1") (deps (list (crate-dep (name "darling") (req "^0.14.1") (default-features #t) (kind 0)) (crate-dep (name "kv-derive-impl") (req "^1.0.1") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1.0.39") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.18") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.94") (default-features #t) (kind 0)))) (hash "18df7n9s9qg073dca5gv7v3qkngn49wbwykllgg1q6a1dbsqjv3h")))

