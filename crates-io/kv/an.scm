(define-module (crates-io kv an) #:use-module (crates-io))

(define-public crate-kvantti-0.1 (crate (name "kvantti") (vers "0.1.0") (deps (list (crate-dep (name "float-cmp") (req "^0.5.3") (default-features #t) (kind 0)) (crate-dep (name "num-complex") (req "^0.2") (default-features #t) (kind 0)))) (hash "0lj6iyac35bgr5h3mgijvzwwsg29ds6b0bnv2psn0b75lnhz50sv") (yanked #t)))

