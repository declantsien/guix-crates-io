(define-module (crates-io kv -t) #:use-module (crates-io))

(define-public crate-kv-trie-rs-0.1 (crate (name "kv-trie-rs") (vers "0.1.1") (deps (list (crate-dep (name "louds-rs") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "criterion") (req "^0.2") (default-features #t) (kind 2)) (crate-dep (name "lazy_static") (req "^1.3") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.6") (default-features #t) (kind 2)))) (hash "057ji3ldlw1l76lxvkgwg32sxvqlz8ylzn6c8f0izhb73niygfkk")))

(define-public crate-kv-trie-rs-0.1 (crate (name "kv-trie-rs") (vers "0.1.2") (deps (list (crate-dep (name "louds-rs") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "criterion") (req "^0.4.0") (default-features #t) (kind 2)) (crate-dep (name "lazy_static") (req "^1.3") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.6") (default-features #t) (kind 2)))) (hash "0nsphjfwl9cxg9d5ppff6mg3hhzcl8hsn1dhb276lxkfaq3bixaq")))

