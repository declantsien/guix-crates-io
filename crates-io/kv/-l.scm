(define-module (crates-io kv -l) #:use-module (crates-io))

(define-public crate-kv-log-macro-1 (crate (name "kv-log-macro") (vers "1.0.0") (deps (list (crate-dep (name "femme") (req "^1.2.0") (default-features #t) (kind 2)) (crate-dep (name "log") (req "^0.4.8") (default-features #t) (kind 0)))) (hash "0cb3q0albb94xhczgnsd2g1shf8mm064rhib1ybw2vrlld69p45i")))

(define-public crate-kv-log-macro-1 (crate (name "kv-log-macro") (vers "1.0.1") (deps (list (crate-dep (name "femme") (req "^1.2.0") (default-features #t) (kind 2)) (crate-dep (name "log") (req "^0.4.8") (default-features #t) (kind 0)))) (hash "0v6kgvq8hqlrga6pakb6dlbxlcbqa99fqkiqcsgsi3qg00fmawcm")))

(define-public crate-kv-log-macro-1 (crate (name "kv-log-macro") (vers "1.0.2") (deps (list (crate-dep (name "femme") (req "^1.2.0") (default-features #t) (kind 2)) (crate-dep (name "log") (req "^0.4.8") (features (quote ("kv_unstable"))) (default-features #t) (kind 0)))) (hash "08xna7scaxxlysd7v1cijazrj8jbr32m6v73gv65pcwrnf939vff")))

(define-public crate-kv-log-macro-1 (crate (name "kv-log-macro") (vers "1.0.3") (deps (list (crate-dep (name "femme") (req "^1.2.0") (default-features #t) (kind 2)) (crate-dep (name "log") (req "^0.4.8") (features (quote ("kv_unstable"))) (default-features #t) (kind 0)))) (hash "0kwsc89w0bl5z5ix87v5c97g4kxs2fnx9jlb3xqr43i37sqjzjz2")))

(define-public crate-kv-log-macro-1 (crate (name "kv-log-macro") (vers "1.0.4") (deps (list (crate-dep (name "femme") (req "^1.2.0") (default-features #t) (kind 2)) (crate-dep (name "log") (req "^0.4.8") (features (quote ("kv_unstable"))) (default-features #t) (kind 0)))) (hash "1fxar5mvl8gf19phy86b915n2yhs11z23p7bwr9afc6mcpsdjm4c")))

(define-public crate-kv-log-macro-1 (crate (name "kv-log-macro") (vers "1.0.5") (deps (list (crate-dep (name "femme") (req "^1.2.0") (default-features #t) (kind 2)) (crate-dep (name "log") (req "^0.4.8") (features (quote ("kv_unstable"))) (default-features #t) (kind 0)))) (hash "0l9spn4rm2m4dhqf240lad386fp0dpg3ksw1gn486m3ysgp3nb9a")))

(define-public crate-kv-log-macro-1 (crate (name "kv-log-macro") (vers "1.0.6") (deps (list (crate-dep (name "femme") (req "^1.2.0") (default-features #t) (kind 2)) (crate-dep (name "log") (req "^0.4.8") (features (quote ("kv_unstable"))) (default-features #t) (kind 0)))) (hash "066lypi7b3h413f43mqly6zd5nd4ddjlv9m96pmsfz2z45npvxag")))

(define-public crate-kv-log-macro-1 (crate (name "kv-log-macro") (vers "1.0.7") (deps (list (crate-dep (name "femme") (req "^1.2.0") (default-features #t) (kind 2)) (crate-dep (name "log") (req "^0.4.8") (features (quote ("kv_unstable"))) (default-features #t) (kind 0)))) (hash "0zwp4bxkkp87rl7xy2dain77z977rvcry1gmr5bssdbn541v7s0d")))

