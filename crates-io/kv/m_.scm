(define-module (crates-io kv m_) #:use-module (crates-io))

(define-public crate-kvm_wrapper-0.1 (crate (name "kvm_wrapper") (vers "0.1.0") (hash "10l3ifkjxw7phri4r5zvklwyhp05s23d08fv14hsp3hzm9fn0zxs") (features (quote (("kvm-v4_20_0") ("kvm-v4_14_0")))) (yanked #t)))

(define-public crate-kvm_wrapper-0.1 (crate (name "kvm_wrapper") (vers "0.1.1") (hash "0gpwj8gq9s63g3flzkpqdgm8kinrxh3i6819xwapp8033nxjjycy") (features (quote (("kvm-v4_20_0") ("kvm-v4_14_0")))) (yanked #t)))

(define-public crate-kvm_wrapper-0.1 (crate (name "kvm_wrapper") (vers "0.1.2") (hash "1vld91r9hmpm3r4226a0zc6kaya3d4fsvnzxihls3anmam3p3pzj") (features (quote (("kvm-v4_20_0") ("kvm-v4_14_0")))) (yanked #t)))

