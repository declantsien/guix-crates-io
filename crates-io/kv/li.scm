(define-module (crates-io kv li) #:use-module (crates-io))

(define-public crate-kvlite-0.1 (crate (name "kvlite") (vers "0.1.0") (deps (list (crate-dep (name "rustc-serialize") (req "^0.3.22") (default-features #t) (kind 0)))) (hash "0wk1wscngwyvfb5vm9pvgd3wcinp8w3dl0yzcqc4vjxzqlsqm560")))

(define-public crate-kvlite-0.1 (crate (name "kvlite") (vers "0.1.1") (deps (list (crate-dep (name "nix") (req "^0.7.0") (default-features #t) (kind 0)))) (hash "0ica033fdg4i0ghk0dr249zrkjshihrklg8rpv9cim7hljwr44ii")))

(define-public crate-kvlite-0.1 (crate (name "kvlite") (vers "0.1.2") (deps (list (crate-dep (name "nix") (req "^0.7.0") (default-features #t) (kind 0)))) (hash "0niabasd4m94p6an9rvcm23a6zhr0marw8sjy5z26s8djq9d03q2")))

(define-public crate-kvlite-0.1 (crate (name "kvlite") (vers "0.1.3") (deps (list (crate-dep (name "nix") (req "^0.7.0") (default-features #t) (kind 0)))) (hash "1l9pqnirfc3nib4wycy5w9kfzq3xqcvwz9sbqsd66mfvaibxr9dg")))

