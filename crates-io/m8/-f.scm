(define-module (crates-io m8 -f) #:use-module (crates-io))

(define-public crate-m8-files-0.1 (crate (name "m8-files") (vers "0.1.0") (deps (list (crate-dep (name "arr_macro") (req "^0.2.1") (default-features #t) (kind 0)) (crate-dep (name "byteorder") (req "^1.4.3") (default-features #t) (kind 0)))) (hash "1ajppcqf718cc924405mmfqy6i7ngprq7zk74mwlcwqgqsym7mcy")))

(define-public crate-m8-files-0.1 (crate (name "m8-files") (vers "0.1.1") (deps (list (crate-dep (name "arr_macro") (req "^0.2.1") (default-features #t) (kind 0)) (crate-dep (name "byteorder") (req "^1.4.3") (default-features #t) (kind 0)))) (hash "0jz62xxmfpjri2aniqj8dq85a9if0k96rplvx197yjrfgfh8z0mw")))

(define-public crate-m8-files-0.1 (crate (name "m8-files") (vers "0.1.2") (deps (list (crate-dep (name "arr_macro") (req "^0.2.1") (default-features #t) (kind 0)) (crate-dep (name "byteorder") (req "^1.4.3") (default-features #t) (kind 0)))) (hash "1fi9i61mn8jrkwfnn8xbvp1kkl8d2gb2x4fpnv8nwb8qrm1vl5il")))

(define-public crate-m8-files-0.1 (crate (name "m8-files") (vers "0.1.3") (deps (list (crate-dep (name "arr_macro") (req "^0.2.1") (default-features #t) (kind 0)) (crate-dep (name "byteorder") (req "^1.4.3") (default-features #t) (kind 0)))) (hash "1z9zms3a7jyvclbbrra3ybsl3xs34yfxlq5wkigr2nr9i0a24kzc")))

(define-public crate-m8-files-0.2 (crate (name "m8-files") (vers "0.2.0") (deps (list (crate-dep (name "arr_macro") (req "^0.2.1") (default-features #t) (kind 0)) (crate-dep (name "byteorder") (req "^1.4.3") (default-features #t) (kind 0)))) (hash "1klw5nx9b5bb6f24k757fz62ahz8claa4s6vgj6mrks3nk6nian0")))

