(define-module (crates-io zi ss) #:use-module (crates-io))

(define-public crate-zissou-0.0.0 (crate (name "zissou") (vers "0.0.0") (deps (list (crate-dep (name "cortex-m") (req "^0.6.0") (default-features #t) (kind 0)) (crate-dep (name "cortex-m-rt") (req "^0.6.8") (default-features #t) (kind 0)) (crate-dep (name "panic-halt") (req "^0.2.0") (default-features #t) (kind 0)))) (hash "1qcq98h4m92qvbvyc9qpbmc6ka3bwkz2xcpfbyaxxw80abdsrs60")))

