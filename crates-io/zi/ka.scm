(define-module (crates-io zi ka) #:use-module (crates-io))

(define-public crate-zika-3 (crate (name "zika") (vers "3.0.0") (deps (list (crate-dep (name "base64") (req "^0.21.5") (default-features #t) (kind 0)) (crate-dep (name "bytes") (req "^1.5.0") (default-features #t) (kind 0)) (crate-dep (name "env_logger") (req "^0.10.1") (default-features #t) (kind 0)) (crate-dep (name "etherparse") (req "^0.13.0") (default-features #t) (kind 0)) (crate-dep (name "futures") (req "^0.3.29") (default-features #t) (kind 0)) (crate-dep (name "ipnetwork") (req "^0.20.0") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.20") (default-features #t) (kind 0)) (crate-dep (name "lru") (req "^0.12.0") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)) (crate-dep (name "rumqttc") (req "^0.23.0") (features (quote ("use-rustls"))) (default-features #t) (kind 0)) (crate-dep (name "rustls") (req "^0.21.8") (default-features #t) (kind 0)) (crate-dep (name "rustls-pemfile") (req "^2.0.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.193") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.108") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.34.0") (features (quote ("full"))) (default-features #t) (kind 0)) (crate-dep (name "tokio-rustls") (req "^0.25.0") (default-features #t) (kind 0)) (crate-dep (name "tokio-util") (req "^0.7.10") (default-features #t) (kind 0)) (crate-dep (name "tun") (req "^0.6.1") (features (quote ("async"))) (default-features #t) (kind 0)))) (hash "0a7vvjysnls2i7pp4azj1kxkyd0cqwzlrd40znrkla3r48qc1855")))

(define-public crate-zika-3 (crate (name "zika") (vers "3.0.1") (deps (list (crate-dep (name "base64") (req "^0.21.5") (default-features #t) (kind 0)) (crate-dep (name "bytes") (req "^1.5.0") (default-features #t) (kind 0)) (crate-dep (name "env_logger") (req "^0.10.1") (default-features #t) (kind 0)) (crate-dep (name "etherparse") (req "^0.13.0") (default-features #t) (kind 0)) (crate-dep (name "futures") (req "^0.3.29") (default-features #t) (kind 0)) (crate-dep (name "ipnetwork") (req "^0.20.0") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.20") (default-features #t) (kind 0)) (crate-dep (name "lru") (req "^0.12.0") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)) (crate-dep (name "rumqttc") (req "^0.23.0") (features (quote ("use-rustls"))) (default-features #t) (kind 0)) (crate-dep (name "rustls") (req "^0.21.8") (default-features #t) (kind 0)) (crate-dep (name "rustls-pemfile") (req "^2.0.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.193") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.108") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.34.0") (features (quote ("full"))) (default-features #t) (kind 0)) (crate-dep (name "tokio-rustls") (req "^0.25.0") (default-features #t) (kind 0)) (crate-dep (name "tokio-util") (req "^0.7.10") (default-features #t) (kind 0)) (crate-dep (name "tun") (req "^0.6.1") (features (quote ("async"))) (default-features #t) (kind 0)))) (hash "1bmdchiwaik7irr29dsgz5xmz5jx5yq6q4ll5sf3zqf31g0afv30")))

(define-public crate-zika-3 (crate (name "zika") (vers "3.1.0") (deps (list (crate-dep (name "base64") (req "^0.21.5") (default-features #t) (kind 0)) (crate-dep (name "bytes") (req "^1.5.0") (default-features #t) (kind 0)) (crate-dep (name "env_logger") (req "^0.10.1") (default-features #t) (kind 0)) (crate-dep (name "etherparse") (req "^0.13.0") (default-features #t) (kind 0)) (crate-dep (name "futures") (req "^0.3.29") (default-features #t) (kind 0)) (crate-dep (name "ipnetwork") (req "^0.20.0") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.20") (default-features #t) (kind 0)) (crate-dep (name "lru") (req "^0.12.0") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)) (crate-dep (name "rumqttc") (req "^0.23.0") (features (quote ("use-rustls"))) (default-features #t) (kind 0)) (crate-dep (name "rustls") (req "^0.21.8") (default-features #t) (kind 0)) (crate-dep (name "rustls-pemfile") (req "^2.0.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.193") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.108") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.34.0") (features (quote ("full"))) (default-features #t) (kind 0)) (crate-dep (name "tokio-rustls") (req "^0.25.0") (default-features #t) (kind 0)) (crate-dep (name "tokio-util") (req "^0.7.10") (default-features #t) (kind 0)) (crate-dep (name "tun") (req "^0.6.1") (features (quote ("async"))) (default-features #t) (kind 0)))) (hash "1lv1dn4dpci3szc9gv52j8y7i77z9sw1k3613hfy9s55wf9b7557")))

(define-public crate-zika-3 (crate (name "zika") (vers "3.2.0") (deps (list (crate-dep (name "base64") (req "^0.21.5") (default-features #t) (kind 0)) (crate-dep (name "bytes") (req "^1.5.0") (default-features #t) (kind 0)) (crate-dep (name "env_logger") (req "^0.10.1") (default-features #t) (kind 0)) (crate-dep (name "etherparse") (req "^0.13.0") (default-features #t) (kind 0)) (crate-dep (name "futures") (req "^0.3.29") (default-features #t) (kind 0)) (crate-dep (name "ipnetwork") (req "^0.20.0") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.20") (default-features #t) (kind 0)) (crate-dep (name "lru") (req "^0.12.0") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)) (crate-dep (name "rumqttc") (req "^0.23.0") (features (quote ("use-rustls"))) (default-features #t) (kind 0)) (crate-dep (name "rustls") (req "^0.21.8") (default-features #t) (kind 0)) (crate-dep (name "rustls-pemfile") (req "^2.0.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.193") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.108") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.34.0") (features (quote ("full"))) (default-features #t) (kind 0)) (crate-dep (name "tokio-rustls") (req "^0.25.0") (default-features #t) (kind 0)) (crate-dep (name "tokio-util") (req "^0.7.10") (default-features #t) (kind 0)) (crate-dep (name "tun") (req "^0.6.1") (features (quote ("async"))) (default-features #t) (kind 0)))) (hash "18h7j64s18n5hm84765kwpgy46dzw3qgczzxxrkibm2fr9mqyngx")))

(define-public crate-zika-3 (crate (name "zika") (vers "3.2.2") (deps (list (crate-dep (name "base64") (req "^0.21.5") (default-features #t) (kind 0)) (crate-dep (name "bytes") (req "^1.5.0") (default-features #t) (kind 0)) (crate-dep (name "env_logger") (req "^0.10.1") (default-features #t) (kind 0)) (crate-dep (name "etherparse") (req "^0.13.0") (default-features #t) (kind 0)) (crate-dep (name "futures") (req "^0.3.29") (default-features #t) (kind 0)) (crate-dep (name "ipnetwork") (req "^0.20.0") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.20") (default-features #t) (kind 0)) (crate-dep (name "lru") (req "^0.12.0") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)) (crate-dep (name "rumqttc") (req "^0.23.0") (features (quote ("use-rustls"))) (default-features #t) (kind 0)) (crate-dep (name "rustls") (req "^0.21.8") (default-features #t) (kind 0)) (crate-dep (name "rustls-pemfile") (req "^2.0.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.193") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.108") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.34.0") (features (quote ("full"))) (default-features #t) (kind 0)) (crate-dep (name "tokio-rustls") (req "^0.25.0") (default-features #t) (kind 0)) (crate-dep (name "tokio-util") (req "^0.7.10") (default-features #t) (kind 0)) (crate-dep (name "tun") (req "^0.6.1") (features (quote ("async"))) (default-features #t) (kind 0)))) (hash "0xmkwcvax2iq7nsaxgd6lq1s0gmdpcsl67z5jq9jqc8axanaf9bx")))

(define-public crate-zika-3 (crate (name "zika") (vers "3.3.0") (deps (list (crate-dep (name "base64") (req "^0.21.5") (default-features #t) (kind 0)) (crate-dep (name "bytes") (req "^1.5.0") (default-features #t) (kind 0)) (crate-dep (name "env_logger") (req "^0.10.1") (default-features #t) (kind 0)) (crate-dep (name "etherparse") (req "^0.13.0") (default-features #t) (kind 0)) (crate-dep (name "futures") (req "^0.3.29") (default-features #t) (kind 0)) (crate-dep (name "ipnetwork") (req "^0.20.0") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.20") (default-features #t) (kind 0)) (crate-dep (name "lru") (req "^0.12.0") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)) (crate-dep (name "rumqttc") (req "^0.23.0") (features (quote ("use-rustls"))) (default-features #t) (kind 0)) (crate-dep (name "rustls") (req "^0.21.10") (default-features #t) (kind 0)) (crate-dep (name "rustls-pemfile") (req "^2.0.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.193") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_default_utils") (req "^0.2.1") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.34.0") (features (quote ("full"))) (default-features #t) (kind 0)) (crate-dep (name "tokio-rustls") (req "^0.25.0") (default-features #t) (kind 0)) (crate-dep (name "tokio-util") (req "^0.7.10") (default-features #t) (kind 0)) (crate-dep (name "toml") (req "^0.8.8") (default-features #t) (kind 0)) (crate-dep (name "tun") (req "^0.6.1") (features (quote ("async"))) (default-features #t) (kind 0)))) (hash "0g12ps86cy5pk2ibh4d084jhvp97jbgxbcplm5yllgdhh1hx1jm7")))

(define-public crate-zika-3 (crate (name "zika") (vers "3.3.1") (deps (list (crate-dep (name "base64") (req "^0.21.5") (default-features #t) (kind 0)) (crate-dep (name "bytes") (req "^1.5.0") (default-features #t) (kind 0)) (crate-dep (name "env_logger") (req "^0.10.1") (default-features #t) (kind 0)) (crate-dep (name "etherparse") (req "^0.13.0") (default-features #t) (kind 0)) (crate-dep (name "futures") (req "^0.3.29") (default-features #t) (kind 0)) (crate-dep (name "ipnetwork") (req "^0.20.0") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.20") (default-features #t) (kind 0)) (crate-dep (name "lru") (req "^0.12.0") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)) (crate-dep (name "rumqttc") (req "^0.23.0") (features (quote ("use-rustls"))) (default-features #t) (kind 0)) (crate-dep (name "rustls") (req "^0.21.10") (default-features #t) (kind 0)) (crate-dep (name "rustls-pemfile") (req "^2.0.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.193") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_default_utils") (req "^0.2.1") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.34.0") (features (quote ("full"))) (default-features #t) (kind 0)) (crate-dep (name "tokio-rustls") (req "^0.25.0") (default-features #t) (kind 0)) (crate-dep (name "tokio-util") (req "^0.7.10") (default-features #t) (kind 0)) (crate-dep (name "toml") (req "^0.8.8") (default-features #t) (kind 0)) (crate-dep (name "tun") (req "^0.6.1") (features (quote ("async"))) (default-features #t) (kind 0)))) (hash "0ax3g9pwl2snmx22shkc10y9arv3bmzwwg2zwygx8ms9ac5mgl7m")))

(define-public crate-zika-3 (crate (name "zika") (vers "3.3.2") (deps (list (crate-dep (name "base64") (req "^0.21.5") (default-features #t) (kind 0)) (crate-dep (name "bytes") (req "^1.5.0") (default-features #t) (kind 0)) (crate-dep (name "env_logger") (req "^0.10.1") (default-features #t) (kind 0)) (crate-dep (name "etherparse") (req "^0.13.0") (default-features #t) (kind 0)) (crate-dep (name "futures") (req "^0.3.29") (default-features #t) (kind 0)) (crate-dep (name "ipnetwork") (req "^0.20.0") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.20") (default-features #t) (kind 0)) (crate-dep (name "lru") (req "^0.12.0") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)) (crate-dep (name "rumqttc") (req "^0.23.0") (features (quote ("use-rustls"))) (default-features #t) (kind 0)) (crate-dep (name "rustls") (req "^0.21.10") (default-features #t) (kind 0)) (crate-dep (name "rustls-pemfile") (req "^2.0.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.193") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_default_utils") (req "^0.2.1") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.34.0") (features (quote ("full"))) (default-features #t) (kind 0)) (crate-dep (name "tokio-rustls") (req "^0.25.0") (default-features #t) (kind 0)) (crate-dep (name "tokio-util") (req "^0.7.10") (default-features #t) (kind 0)) (crate-dep (name "toml") (req "^0.8.8") (default-features #t) (kind 0)) (crate-dep (name "tun") (req "^0.6.1") (features (quote ("async"))) (default-features #t) (kind 0)))) (hash "1yjcxk4ihzxny4a6r7fiaazq1k3asq9j6ifpl1ch1nsy5mb6b01m")))

(define-public crate-zika-3 (crate (name "zika") (vers "3.3.3") (deps (list (crate-dep (name "base64") (req "^0.21.5") (default-features #t) (kind 0)) (crate-dep (name "bytes") (req "^1.5.0") (default-features #t) (kind 0)) (crate-dep (name "env_logger") (req "^0.10.1") (default-features #t) (kind 0)) (crate-dep (name "etherparse") (req "^0.13.0") (default-features #t) (kind 0)) (crate-dep (name "futures") (req "^0.3.29") (default-features #t) (kind 0)) (crate-dep (name "ipnetwork") (req "^0.20.0") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.20") (default-features #t) (kind 0)) (crate-dep (name "lru") (req "^0.12.0") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)) (crate-dep (name "rumqttc") (req "^0.23.0") (features (quote ("use-rustls"))) (default-features #t) (kind 0)) (crate-dep (name "rustls") (req "^0.21.10") (default-features #t) (kind 0)) (crate-dep (name "rustls-pemfile") (req "^2.0.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.193") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_default_utils") (req "^0.2.1") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.34.0") (features (quote ("full"))) (default-features #t) (kind 0)) (crate-dep (name "tokio-rustls") (req "^0.25.0") (default-features #t) (kind 0)) (crate-dep (name "tokio-util") (req "^0.7.10") (default-features #t) (kind 0)) (crate-dep (name "toml") (req "^0.8.8") (default-features #t) (kind 0)) (crate-dep (name "tun") (req "^0.6.1") (features (quote ("async"))) (default-features #t) (kind 0)))) (hash "0cjd6vamrdqsmvhy7kgq88ryh58g1r5vxwn4z8ashsqy9bxr9dmd")))

(define-public crate-zika-3 (crate (name "zika") (vers "3.3.4") (deps (list (crate-dep (name "base64") (req "^0.21.5") (default-features #t) (kind 0)) (crate-dep (name "bytes") (req "^1.5.0") (default-features #t) (kind 0)) (crate-dep (name "env_logger") (req "^0.10.1") (default-features #t) (kind 0)) (crate-dep (name "etherparse") (req "^0.13.0") (default-features #t) (kind 0)) (crate-dep (name "futures") (req "^0.3.29") (default-features #t) (kind 0)) (crate-dep (name "ipnetwork") (req "^0.20.0") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.20") (default-features #t) (kind 0)) (crate-dep (name "lru") (req "^0.12.0") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)) (crate-dep (name "rumqttc") (req "^0.23.0") (features (quote ("use-rustls"))) (default-features #t) (kind 0)) (crate-dep (name "rustls") (req "^0.21.10") (default-features #t) (kind 0)) (crate-dep (name "rustls-pemfile") (req "^2.0.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.193") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_default_utils") (req "^0.2.1") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.34.0") (features (quote ("full"))) (default-features #t) (kind 0)) (crate-dep (name "tokio-rustls") (req "^0.25.0") (default-features #t) (kind 0)) (crate-dep (name "tokio-util") (req "^0.7.10") (default-features #t) (kind 0)) (crate-dep (name "toml") (req "^0.8.8") (default-features #t) (kind 0)) (crate-dep (name "tun") (req "^0.6.1") (features (quote ("async"))) (default-features #t) (kind 0)))) (hash "01xlgfmd4hxrw465mz8vxqrq3xilbcy3jdc6xrl72rqbqq8qpqg2")))

(define-public crate-zika-3 (crate (name "zika") (vers "3.3.5") (deps (list (crate-dep (name "base64") (req "^0.21.5") (default-features #t) (kind 0)) (crate-dep (name "bytes") (req "^1.5.0") (default-features #t) (kind 0)) (crate-dep (name "env_logger") (req "^0.10.1") (default-features #t) (kind 0)) (crate-dep (name "etherparse") (req "^0.13.0") (default-features #t) (kind 0)) (crate-dep (name "futures") (req "^0.3.29") (default-features #t) (kind 0)) (crate-dep (name "ipnetwork") (req "^0.20.0") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.20") (default-features #t) (kind 0)) (crate-dep (name "lru") (req "^0.12.0") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)) (crate-dep (name "rumqttc") (req "^0.23.0") (features (quote ("use-rustls"))) (default-features #t) (kind 0)) (crate-dep (name "rustls") (req "^0.21.10") (default-features #t) (kind 0)) (crate-dep (name "rustls-pemfile") (req "^2.0.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.193") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_default_utils") (req "^0.2.1") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.34.0") (features (quote ("full"))) (default-features #t) (kind 0)) (crate-dep (name "tokio-rustls") (req "^0.25.0") (default-features #t) (kind 0)) (crate-dep (name "tokio-util") (req "^0.7.10") (default-features #t) (kind 0)) (crate-dep (name "toml") (req "^0.8.8") (default-features #t) (kind 0)) (crate-dep (name "tun") (req "^0.6.1") (features (quote ("async"))) (default-features #t) (kind 0)))) (hash "0gg6mccnbv7f6g29wxf3r1san83yblnyybvlmkc19l72cpmr51g3")))

(define-public crate-zika-3 (crate (name "zika") (vers "3.3.6") (deps (list (crate-dep (name "base64") (req "^0.21.5") (default-features #t) (kind 0)) (crate-dep (name "bytes") (req "^1.5.0") (default-features #t) (kind 0)) (crate-dep (name "env_logger") (req "^0.10.1") (default-features #t) (kind 0)) (crate-dep (name "etherparse") (req "^0.13.0") (default-features #t) (kind 0)) (crate-dep (name "futures") (req "^0.3.29") (default-features #t) (kind 0)) (crate-dep (name "ipnetwork") (req "^0.20.0") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.20") (default-features #t) (kind 0)) (crate-dep (name "lru") (req "^0.12.0") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)) (crate-dep (name "rumqttc") (req "^0.23.0") (features (quote ("use-native-tls"))) (default-features #t) (kind 0)) (crate-dep (name "rustls") (req "^0.21.10") (default-features #t) (kind 0)) (crate-dep (name "rustls-pemfile") (req "^2.0.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.193") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_default_utils") (req "^0.2.1") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.34.0") (features (quote ("full"))) (default-features #t) (kind 0)) (crate-dep (name "tokio-util") (req "^0.7.10") (default-features #t) (kind 0)) (crate-dep (name "toml") (req "^0.8.8") (default-features #t) (kind 0)) (crate-dep (name "tun") (req "^0.6.1") (features (quote ("async"))) (default-features #t) (kind 0)))) (hash "0f3d63f7cgj3ciqmqp0m0n6d7jlx3fll1qhp5b2j8qrcjrwk7720")))

(define-public crate-zika-3 (crate (name "zika") (vers "3.4.1") (deps (list (crate-dep (name "base64") (req "^0.21.5") (default-features #t) (kind 0)) (crate-dep (name "bytes") (req "^1.5.0") (default-features #t) (kind 0)) (crate-dep (name "env_logger") (req "^0.10.1") (default-features #t) (kind 0)) (crate-dep (name "etherparse") (req "^0.13.0") (default-features #t) (kind 0)) (crate-dep (name "futures") (req "^0.3.29") (default-features #t) (kind 0)) (crate-dep (name "ipnetwork") (req "^0.20.0") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.20") (default-features #t) (kind 0)) (crate-dep (name "lru") (req "^0.12.0") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)) (crate-dep (name "rumqttc") (req "^0.23.0") (features (quote ("use-native-tls"))) (default-features #t) (kind 0)) (crate-dep (name "rustls") (req "^0.21.10") (default-features #t) (kind 0)) (crate-dep (name "rustls-pemfile") (req "^2.0.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.193") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_default_utils") (req "^0.2.1") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.34.0") (features (quote ("full"))) (default-features #t) (kind 0)) (crate-dep (name "tokio-util") (req "^0.7.10") (default-features #t) (kind 0)) (crate-dep (name "toml") (req "^0.8.8") (default-features #t) (kind 0)) (crate-dep (name "tun") (req "^0.6.1") (features (quote ("async"))) (default-features #t) (kind 0)))) (hash "0a78qhsc4rpw17b3m2ivc3r6z2da91adlwj7bv191vdmgrjqzn3w")))

(define-public crate-zika-3 (crate (name "zika") (vers "3.4.2") (deps (list (crate-dep (name "base64") (req "^0.21.5") (default-features #t) (kind 0)) (crate-dep (name "bytes") (req "^1.5.0") (default-features #t) (kind 0)) (crate-dep (name "env_logger") (req "^0.10.1") (default-features #t) (kind 0)) (crate-dep (name "etherparse") (req "^0.13.0") (default-features #t) (kind 0)) (crate-dep (name "futures") (req "^0.3.29") (default-features #t) (kind 0)) (crate-dep (name "ipnetwork") (req "^0.20.0") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.20") (default-features #t) (kind 0)) (crate-dep (name "lru") (req "^0.12.0") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)) (crate-dep (name "rumqttc") (req "^0.23.0") (features (quote ("use-native-tls"))) (default-features #t) (kind 0)) (crate-dep (name "rustls") (req "^0.21.10") (default-features #t) (kind 0)) (crate-dep (name "rustls-pemfile") (req "^2.0.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.193") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_default_utils") (req "^0.2.1") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.34.0") (features (quote ("full"))) (default-features #t) (kind 0)) (crate-dep (name "tokio-util") (req "^0.7.10") (default-features #t) (kind 0)) (crate-dep (name "toml") (req "^0.8.8") (default-features #t) (kind 0)) (crate-dep (name "tun") (req "^0.6.1") (features (quote ("async"))) (default-features #t) (kind 0)))) (hash "14lvzhxh185gv09zlfdprm62r9s25vjhdhj9jm9az8fhji0kgvn6")))

(define-public crate-zika-3 (crate (name "zika") (vers "3.4.3") (deps (list (crate-dep (name "base64") (req "^0.21.5") (default-features #t) (kind 0)) (crate-dep (name "bytes") (req "^1.5.0") (default-features #t) (kind 0)) (crate-dep (name "env_logger") (req "^0.10.1") (default-features #t) (kind 0)) (crate-dep (name "etherparse") (req "^0.13.0") (default-features #t) (kind 0)) (crate-dep (name "futures") (req "^0.3.29") (default-features #t) (kind 0)) (crate-dep (name "ipnetwork") (req "^0.20.0") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.20") (default-features #t) (kind 0)) (crate-dep (name "lru") (req "^0.12.0") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)) (crate-dep (name "rumqttc") (req "^0.23.0") (features (quote ("use-rustls"))) (default-features #t) (kind 0)) (crate-dep (name "rustls") (req "^0.21.10") (default-features #t) (kind 0)) (crate-dep (name "rustls-pemfile") (req "^2.0.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.193") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_default_utils") (req "^0.2.1") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.34.0") (features (quote ("full"))) (default-features #t) (kind 0)) (crate-dep (name "tokio-util") (req "^0.7.10") (default-features #t) (kind 0)) (crate-dep (name "toml") (req "^0.8.8") (default-features #t) (kind 0)) (crate-dep (name "tun") (req "^0.6.1") (features (quote ("async"))) (default-features #t) (kind 0)))) (hash "0v2mi1gsmr5b2hd6ismyxgfw21v1126rlpy5574qr26g4b2b7b58")))

(define-public crate-zika-3 (crate (name "zika") (vers "3.4.4") (deps (list (crate-dep (name "base64") (req "^0.21.5") (default-features #t) (kind 0)) (crate-dep (name "bytes") (req "^1.5.0") (default-features #t) (kind 0)) (crate-dep (name "env_logger") (req "^0.10.1") (default-features #t) (kind 0)) (crate-dep (name "etherparse") (req "^0.13.0") (default-features #t) (kind 0)) (crate-dep (name "futures") (req "^0.3.29") (default-features #t) (kind 0)) (crate-dep (name "ipnetwork") (req "^0.20.0") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.20") (default-features #t) (kind 0)) (crate-dep (name "lru") (req "^0.12.0") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)) (crate-dep (name "rumqttc") (req "^0.23.0") (features (quote ("use-rustls"))) (default-features #t) (kind 0)) (crate-dep (name "rustls") (req "^0.21.10") (default-features #t) (kind 0)) (crate-dep (name "rustls-pemfile") (req "^2.0.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.193") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_default_utils") (req "^0.2.1") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.34.0") (features (quote ("full"))) (default-features #t) (kind 0)) (crate-dep (name "tokio-util") (req "^0.7.10") (default-features #t) (kind 0)) (crate-dep (name "toml") (req "^0.8.8") (default-features #t) (kind 0)) (crate-dep (name "tun") (req "^0.6.1") (features (quote ("async"))) (default-features #t) (kind 0)))) (hash "15jnq3xm9zpl12rvqcvaivzl1nczmn9dil2kqsbf2hflyklqfqva")))

