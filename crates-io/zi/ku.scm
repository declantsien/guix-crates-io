(define-module (crates-io zi ku) #:use-module (crates-io))

(define-public crate-ziku-algorithms-0.1 (crate (name "ziku-algorithms") (vers "0.1.0") (deps (list (crate-dep (name "crdts") (req "^0.1.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "pds") (req "^0.1.0") (optional #t) (default-features #t) (kind 0)))) (hash "0jljzkjrp029kixm474qz2w905rqp3py3kg9gqijxr2simlijhpn") (features (quote (("default" "crdts")))) (yanked #t) (v 2) (features2 (quote (("pds" "dep:pds") ("crdts" "dep:crdts"))))))

(define-public crate-ziku-algorithms-0.1 (crate (name "ziku-algorithms") (vers "0.1.1") (deps (list (crate-dep (name "ziku-crdts") (req "^0.1.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "ziku-pds") (req "^0.1.0") (optional #t) (default-features #t) (kind 0)))) (hash "1asv4azyx59i7m0wwsblj3xzxnr7jx5j7cis1ah23hjkw431vgc1") (features (quote (("default" "ziku-crdts" "ziku-pds")))) (v 2) (features2 (quote (("ziku-pds" "dep:ziku-pds") ("ziku-crdts" "dep:ziku-crdts"))))))

(define-public crate-ziku-algorithms-0.1 (crate (name "ziku-algorithms") (vers "0.1.2") (deps (list (crate-dep (name "ziku-crdts") (req "^0.1.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "ziku-pds") (req "^0.1.0") (optional #t) (default-features #t) (kind 0)))) (hash "0rg13r43qnjyfb46lfln8w6dxahsxmzjfdlyvhpl751lvszl2wfh") (features (quote (("default" "ziku-crdts" "ziku-pds")))) (v 2) (features2 (quote (("ziku-pds" "dep:ziku-pds") ("ziku-crdts" "dep:ziku-crdts"))))))

(define-public crate-ziku-algorithms-0.1 (crate (name "ziku-algorithms") (vers "0.1.3") (deps (list (crate-dep (name "ziku-crdts") (req "^0.1.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "ziku-pds") (req "^0.1.0") (optional #t) (default-features #t) (kind 0)))) (hash "11i14f5lvs7bs36pkg8cd0j7mhnjc23sp0q8xi8y17a952safra3") (features (quote (("default" "ziku-crdts" "ziku-pds")))) (v 2) (features2 (quote (("ziku-pds" "dep:ziku-pds") ("ziku-crdts" "dep:ziku-crdts"))))))

(define-public crate-ziku-crdts-0.1 (crate (name "ziku-crdts") (vers "0.1.0") (hash "1cld9dzj5l2zsyhr7p816r6w7m0y7pz54hf17m63m3kdqc5w6xcf")))

(define-public crate-ziku-pds-0.1 (crate (name "ziku-pds") (vers "0.1.0") (hash "10kkvnh5khn3s4v8ykj88g1wgdc6zidpdscrsz0kcr5i9bnavy1d")))

