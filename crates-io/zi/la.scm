(define-module (crates-io zi la) #:use-module (crates-io))

(define-public crate-zila-0.1 (crate (name "zila") (vers "0.1.2") (deps (list (crate-dep (name "chrono") (req "^0.4.24") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.28.0") (features (quote ("time"))) (default-features #t) (kind 0)))) (hash "1qsxrx15rcj08wrqqz8hjqm7pzfmbvssmh70yb83r683rix9v9mi") (features (quote (("second") ("minute") ("hour") ("default" "day" "hour" "minute" "second") ("day"))))))

(define-public crate-zila-0.1 (crate (name "zila") (vers "0.1.3") (deps (list (crate-dep (name "chrono") (req "^0.4.24") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.28.0") (features (quote ("time"))) (default-features #t) (kind 0)))) (hash "0sv2ljc9882z96n9idzrjr7c9vfknvv6yah96ifrhgjpjhj62rvw") (features (quote (("second") ("minute") ("hour") ("default" "day" "hour" "minute" "second") ("day"))))))

(define-public crate-zila-0.1 (crate (name "zila") (vers "0.1.4") (deps (list (crate-dep (name "chrono") (req "^0.4.24") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.28.0") (features (quote ("time"))) (default-features #t) (kind 0)))) (hash "0pkbhz3mm9igpq399hwgiq5nh5kvyv1c39jblagw5981g8k4wa6m") (features (quote (("second") ("minute") ("hour") ("default" "day" "hour" "minute" "second") ("day"))))))

(define-public crate-zila-0.1 (crate (name "zila") (vers "0.1.5") (deps (list (crate-dep (name "chrono") (req "^0.4.24") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.28.0") (features (quote ("time"))) (default-features #t) (kind 0)))) (hash "1iyadgrpqxy42v53va95svzxv9wid0kw1fdmdkg44ll28rf4n0n7") (features (quote (("timeout") ("second") ("minute") ("interval") ("hour") ("day"))))))

(define-public crate-zila-0.1 (crate (name "zila") (vers "0.1.6") (deps (list (crate-dep (name "chrono") (req "^0.4.24") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.28.0") (features (quote ("time"))) (default-features #t) (kind 0)))) (hash "03jzm25c47plk4dfn398l0q1nd6qpg5jbyk3hylcr156d4mpy3gd") (features (quote (("timeout") ("second") ("minute") ("interval") ("hour") ("day"))))))

(define-public crate-zila-0.1 (crate (name "zila") (vers "0.1.7") (deps (list (crate-dep (name "chrono") (req "^0.4.24") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.28.0") (features (quote ("time"))) (default-features #t) (kind 0)))) (hash "0lx06daqdif25xkvprfd1hdjwiwksav7x1n64ldfi6glh8xdqzjy") (features (quote (("timeout") ("second") ("minute") ("interval") ("hour") ("day"))))))

(define-public crate-zila-0.1 (crate (name "zila") (vers "0.1.8") (deps (list (crate-dep (name "chrono") (req "^0.4.24") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.28.0") (features (quote ("time"))) (default-features #t) (kind 0)))) (hash "0bgkhqq40l3k9fxgnk1kxsfrmv0rg6ny2izhjqgl348lvkc3cji0") (features (quote (("timeout") ("second") ("minute") ("interval") ("hour") ("full" "day" "hour" "minute" "second" "timeout" "interval") ("default") ("day"))))))

