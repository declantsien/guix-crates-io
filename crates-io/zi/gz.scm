(define-module (crates-io zi gz) #:use-module (crates-io))

(define-public crate-zigzag-0.1 (crate (name "zigzag") (vers "0.1.0") (deps (list (crate-dep (name "num-traits") (req "^0.2.11") (default-features #t) (kind 0)))) (hash "1przksgh335b16mqf1clgi6yxnypdyw67f1hlcbcx1ldl80h9d3h")))

