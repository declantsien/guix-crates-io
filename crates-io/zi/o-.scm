(define-module (crates-io zi o-) #:use-module (crates-io))

(define-public crate-zio-sendfile-0.1 (crate (name "zio-sendfile") (vers "0.1.0") (deps (list (crate-dep (name "libc") (req "^0.2.46") (default-features #t) (kind 0)))) (hash "13jznn6lw8abk1li3bajp0mjm0jr3dnz7d0aapq1nbwl6qfr9mcy")))

(define-public crate-zio-sendfile-0.1 (crate (name "zio-sendfile") (vers "0.1.1") (deps (list (crate-dep (name "libc") (req "^0.2.46") (default-features #t) (kind 0)))) (hash "1cdsmdyd8f92hxj0iwgrbn1r3176g0zvmzsmf63yajish7kyc6a2")))

(define-public crate-zio-sendfile-0.2 (crate (name "zio-sendfile") (vers "0.2.0") (deps (list (crate-dep (name "libc") (req "^0.2.46") (default-features #t) (kind 0)))) (hash "093anzyvw7vpgm5wrb9banwyj0arb0xlywnckasa91dshfqj15xi")))

