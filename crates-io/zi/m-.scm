(define-module (crates-io zi m-) #:use-module (crates-io))

(define-public crate-zim-rs-0.1 (crate (name "zim-rs") (vers "0.1.0") (deps (list (crate-dep (name "cxx") (req "^1.0.78") (default-features #t) (kind 0)) (crate-dep (name "zim-sys") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "1iaf0mb7swdgjchh518bpnc5fiyjkg1wmcv6rgl0xk09bps7mrl2")))

(define-public crate-zim-rs-0.1 (crate (name "zim-rs") (vers "0.1.1") (deps (list (crate-dep (name "cxx") (req "^1.0.78") (default-features #t) (kind 0)) (crate-dep (name "zim-sys") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "0h8kcsq2didhjk8lhq36fg39bdgjb7qx8bqmcqn2ihnrpsrnfay5")))

(define-public crate-zim-sys-0.1 (crate (name "zim-sys") (vers "0.1.0") (deps (list (crate-dep (name "cxx") (req "^1.0.78") (default-features #t) (kind 0)) (crate-dep (name "cxx-build") (req "^1.0.109") (default-features #t) (kind 1)) (crate-dep (name "pkg-config") (req "^0.3.25") (default-features #t) (target "cfg(target_family = \"unix\")") (kind 1)))) (hash "0s29smk6kh0rj7gadlnpkph3qdrjy2x0nxrkjx294kpraa7wgn7a") (links "zim")))

