(define-module (crates-io zi lo) #:use-module (crates-io))

(define-public crate-zilog_z80-0.5 (crate (name "zilog_z80") (vers "0.5.0") (deps (list (crate-dep (name "crossbeam-channel") (req "^0.5.6") (default-features #t) (kind 0)) (crate-dep (name "spin_sleep") (req "^1.0.0") (default-features #t) (target "cfg(windows)") (kind 0)))) (hash "0cyqxm1jm199g9ksa4fzpg6ffhlzhracs5siqgws9aivl6xyhvqg")))

(define-public crate-zilog_z80-0.6 (crate (name "zilog_z80") (vers "0.6.0") (deps (list (crate-dep (name "crossbeam-channel") (req "^0.5.6") (default-features #t) (kind 0)) (crate-dep (name "spin_sleep") (req "^1.0.0") (default-features #t) (target "cfg(windows)") (kind 0)))) (hash "1dnbs9mbjrv3p2vbh8g01b54pd4asqgxcms6haayk3janij8k8f6")))

(define-public crate-zilog_z80-0.7 (crate (name "zilog_z80") (vers "0.7.0") (deps (list (crate-dep (name "crossbeam-channel") (req "^0.5.6") (default-features #t) (kind 0)) (crate-dep (name "spin_sleep") (req "^1.0.0") (default-features #t) (target "cfg(windows)") (kind 0)))) (hash "0qj5j38ns67cxmhpq180vhrn93qkvllh8jachh2wa1fblqj37iy7")))

(define-public crate-zilog_z80-0.8 (crate (name "zilog_z80") (vers "0.8.0") (deps (list (crate-dep (name "crossbeam-channel") (req "^0.5.6") (default-features #t) (kind 0)) (crate-dep (name "spin_sleep") (req "^1.0.0") (default-features #t) (target "cfg(windows)") (kind 0)))) (hash "19kwsrf7nzggrln7qq8pakwk7bq76mrzrdi7lplbvyp6p5bkfz7h")))

(define-public crate-zilog_z80-0.9 (crate (name "zilog_z80") (vers "0.9.0") (deps (list (crate-dep (name "crossbeam-channel") (req "^0.5.6") (default-features #t) (kind 0)) (crate-dep (name "spin_sleep") (req "^1.0.0") (default-features #t) (target "cfg(windows)") (kind 0)))) (hash "0ai3y2984czrhmjynwhkx8v5mil9jjv3xyv401s2ydiz5r7bqdrw")))

(define-public crate-zilog_z80-0.10 (crate (name "zilog_z80") (vers "0.10.0") (deps (list (crate-dep (name "crossbeam-channel") (req "^0.5.6") (default-features #t) (kind 0)) (crate-dep (name "spin_sleep") (req "^1.0.0") (default-features #t) (target "cfg(windows)") (kind 0)))) (hash "0ivvbqaaks8jfig9f8nyslrjlq70r45vi32kiwfxkj85ryv3rwj4")))

(define-public crate-zilog_z80-0.11 (crate (name "zilog_z80") (vers "0.11.0") (deps (list (crate-dep (name "crossbeam-channel") (req "^0.5.6") (default-features #t) (kind 0)))) (hash "1b7mfknx3apbklp2aak05v4psczdswcd28yzqjzrayam82lad3gn")))

(define-public crate-zilog_z80-0.13 (crate (name "zilog_z80") (vers "0.13.0") (hash "1ib9n8k167n6a9ll1fsn7050hgzjqjkf3lrcz2dlw8nv93kd0n4g")))

(define-public crate-zilog_z80-0.14 (crate (name "zilog_z80") (vers "0.14.0") (hash "16bf2l5rgnjpvx32wyg4rlknd7wv1d8bqmpsc99w3vld5znpf9y2")))

(define-public crate-zilog_z80-0.15 (crate (name "zilog_z80") (vers "0.15.0") (hash "0kxji9vywmmwn09vifblpxfgwyc0hbal9j1a7867ln1q5fxw4bnc")))

(define-public crate-zilog_z80-0.16 (crate (name "zilog_z80") (vers "0.16.0") (hash "0c5svnmbpsj12wl0qd2lscywfilnfp171hg9a4spbn54mk7mcm6l")))

(define-public crate-zilog_z80_cpu-0.1 (crate (name "zilog_z80_cpu") (vers "0.1.0") (hash "1j9wq312wvafxxxdyjq9261f1vsmnvxvw40n6xxl0q9cygqsmvcb") (yanked #t)))

(define-public crate-zilog_z80_cpu-0.1 (crate (name "zilog_z80_cpu") (vers "0.1.1") (hash "1h030m4csk99mdk1m22p5grg6pcxack369qg0ahzm00xs2cqnwfy")))

(define-public crate-zilog_z80_cpu-0.1 (crate (name "zilog_z80_cpu") (vers "0.1.2") (hash "0flmgrp4rwp38rsbkkvsqzhsyr0nr3nv5f1lisx7n1ww90ppgwyr")))

