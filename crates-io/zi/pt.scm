(define-module (crates-io zi pt) #:use-module (crates-io))

(define-public crate-ziptree-0.1 (crate (name "ziptree") (vers "0.1.0") (deps (list (crate-dep (name "rand") (req "^0.6") (default-features #t) (kind 0)))) (hash "1nyvfbg7c4r4bhh25xkdq9i1igs9zfyakn4d4bdjjqmpdd1wxns4")))

(define-public crate-ziptree-0.1 (crate (name "ziptree") (vers "0.1.1") (deps (list (crate-dep (name "rand") (req "^0.6") (default-features #t) (kind 0)))) (hash "1yvynmmpbnd9q8j1rh2kyghgqvdcqxrj1y3rq4cbnf1039j24q4m")))

