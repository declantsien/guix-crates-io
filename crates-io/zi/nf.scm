(define-module (crates-io zi nf) #:use-module (crates-io))

(define-public crate-zinfo-0.1 (crate (name "zinfo") (vers "0.1.0") (deps (list (crate-dep (name "crossterm") (req "^0.23") (default-features #t) (kind 0)))) (hash "1bi8r04833bdl9bfbgdz04ab30b328l73ph6lj431r29j06rniiw")))

(define-public crate-zinfo-0.1 (crate (name "zinfo") (vers "0.1.1") (deps (list (crate-dep (name "crossterm") (req "^0.23") (default-features #t) (kind 0)))) (hash "1qmhhx0ygffhzc01y3ai1lbfh06j9jdrdjlx86zvgyx9qdbhd8ws")))

(define-public crate-zinfo-0.1 (crate (name "zinfo") (vers "0.1.2") (deps (list (crate-dep (name "crossterm") (req "^0.23") (default-features #t) (kind 0)))) (hash "1c1bmjf79ifr9300ajjmj5mi0dxg2xfdv8d5xyi6x48wffbj20jm")))

(define-public crate-zinfo-0.1 (crate (name "zinfo") (vers "0.1.3") (deps (list (crate-dep (name "crossterm") (req "^0.23") (default-features #t) (kind 0)))) (hash "0sjz4hn8wmchqrhn2apkiqflh2s1rl5w06nf7f158m5yb6cbl429")))

(define-public crate-zinfo-0.1 (crate (name "zinfo") (vers "0.1.4") (deps (list (crate-dep (name "crossterm") (req "^0.23") (default-features #t) (kind 0)))) (hash "17cmyl8flj078l52dr47lvild874dv00sfmbyc2nzzi49b2rhkik")))

(define-public crate-zinfo-0.1 (crate (name "zinfo") (vers "0.1.5") (deps (list (crate-dep (name "crossterm") (req "^0.23") (default-features #t) (kind 0)))) (hash "1jkcvik6cxlxdvzc6xv8qi310whq3yqrgajz8dbxjgr2h2ah72dz")))

(define-public crate-zinfo-0.2 (crate (name "zinfo") (vers "0.2.0") (deps (list (crate-dep (name "crossterm") (req "^0.23") (default-features #t) (kind 0)))) (hash "09f403dj0f0gzskdzy095a91vszbrhg8awp2dpp44gvv65g3psj8")))

(define-public crate-zinfo-0.2 (crate (name "zinfo") (vers "0.2.1") (deps (list (crate-dep (name "crossterm") (req "^0.23") (default-features #t) (kind 0)))) (hash "02dsx6j1wjrfx1s88nqpn3yygmgkm7409qkjp6vwm19lchqnfagf")))

(define-public crate-zinfo-0.2 (crate (name "zinfo") (vers "0.2.2") (deps (list (crate-dep (name "crossterm") (req "^0.23") (default-features #t) (kind 0)))) (hash "1vzm7ccl85bs6l0mgpdq9dlhrb42cpiq8qyzwg2f24nggk2msqrw")))

(define-public crate-zinfo-0.2 (crate (name "zinfo") (vers "0.2.3") (deps (list (crate-dep (name "crossterm") (req "^0.23") (default-features #t) (kind 0)))) (hash "0xbgpk4xpqawlac4lz5gi3ai3vsr51ir7bfq4g2qgnr66r2mdfkw")))

(define-public crate-zinfo-0.2 (crate (name "zinfo") (vers "0.2.4") (deps (list (crate-dep (name "crossterm") (req "^0.23") (default-features #t) (kind 0)))) (hash "10cgn7vgsl04yr3xdq2ji88kr5gv251zshxfzkfxgcvcg87lzxlj")))

(define-public crate-zinfo-0.2 (crate (name "zinfo") (vers "0.2.5") (deps (list (crate-dep (name "crossterm") (req "^0.25") (default-features #t) (kind 0)))) (hash "0p20chjs10p081nk6lycj3xamihqkqararjr983csjfnx7dr3zmy")))

(define-public crate-zinfo-0.3 (crate (name "zinfo") (vers "0.3.0") (deps (list (crate-dep (name "crossterm") (req "^0.26") (default-features #t) (kind 0)) (crate-dep (name "windows") (req "^0.46.0") (features (quote ("Win32_System_SystemInformation"))) (default-features #t) (target "cfg(windows)") (kind 0)))) (hash "1adncznmkj6sfrpvax99z21vynw8ixvq4m0cbhy6n2r8l6hg6kih")))

(define-public crate-zinfo-0.3 (crate (name "zinfo") (vers "0.3.1") (deps (list (crate-dep (name "crossterm") (req "^0.26") (default-features #t) (kind 0)) (crate-dep (name "windows") (req "^0.46.0") (features (quote ("Win32_System_SystemInformation" "Win32_Foundation"))) (default-features #t) (target "cfg(windows)") (kind 0)))) (hash "1q3385psaashdcvxp66phx3jsikfi8q8q2cv5bvvv0pcll2k85s9")))

