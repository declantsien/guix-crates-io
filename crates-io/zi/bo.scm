(define-module (crates-io zi bo) #:use-module (crates-io))

(define-public crate-ziboh_guess_game-0.1 (crate (name "ziboh_guess_game") (vers "0.1.0") (deps (list (crate-dep (name "rand") (req "^0.8.3") (default-features #t) (kind 0)))) (hash "19cq6i9jk787mccjhy8vp49q9kqb03fs3qglkpn9lrzvk56sk2rj")))

