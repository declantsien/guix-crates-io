(define-module (crates-io zi on) #:use-module (crates-io))

(define-public crate-zion-0.1 (crate (name "zion") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^2") (default-features #t) (kind 0)))) (hash "1v8k04khsr2qchnfyivr4xhi2a0gppr2vpjwfmmw6n65rngcy8qx")))

(define-public crate-zion-0.1 (crate (name "zion") (vers "0.1.1") (deps (list (crate-dep (name "clap") (req "^3") (features (quote ("cargo"))) (default-features #t) (kind 0)))) (hash "0a8m3m23w0y7lgw0gg3hkq37gg14xg1nmwv3zvhq5vvzjg1kbl66")))

