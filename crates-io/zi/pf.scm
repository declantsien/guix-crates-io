(define-module (crates-io zi pf) #:use-module (crates-io))

(define-public crate-zipf-0.1 (crate (name "zipf") (vers "0.1.0") (deps (list (crate-dep (name "rand") (req "^0.3") (default-features #t) (kind 0)))) (hash "1bgx0lnamsn7mcrklmqcn60ql2mv0symid2zfn7lci7m09phjlx4")))

(define-public crate-zipf-0.1 (crate (name "zipf") (vers "0.1.1") (deps (list (crate-dep (name "rand") (req "^0.3") (default-features #t) (kind 0)))) (hash "1wg91d4qpb70zpyxrhv7a0vq41ac7cxa4npsmdylygp7hidh65h4")))

(define-public crate-zipf-0.2 (crate (name "zipf") (vers "0.2.0") (deps (list (crate-dep (name "rand") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "randomkit") (req "^0.1") (default-features #t) (kind 2)))) (hash "04jbx36vdvy8a7nwxpwk94s75lzjnaaw2vh9hw9jk1dm4dh93dm4")))

(define-public crate-zipf-1 (crate (name "zipf") (vers "1.0.0") (deps (list (crate-dep (name "rand") (req "^0.3.15") (default-features #t) (kind 0)) (crate-dep (name "randomkit") (req "^0.1") (default-features #t) (kind 2)))) (hash "1axsz2a00856f0l64ha7ac7n3x7z3wcl4k201s2h3vz2nq1lw9ma")))

(define-public crate-zipf-1 (crate (name "zipf") (vers "1.1.0") (deps (list (crate-dep (name "rand") (req "^0.4.0") (default-features #t) (kind 0)) (crate-dep (name "randomkit") (req "^0.1") (default-features #t) (kind 2)))) (hash "149z97diyw079dw6di953psf3whiiabh13js14b9bp2x2pszifxx")))

(define-public crate-zipf-2 (crate (name "zipf") (vers "2.0.0") (deps (list (crate-dep (name "rand") (req "^0.4.0") (default-features #t) (kind 0)) (crate-dep (name "randomkit") (req "^0.1") (default-features #t) (kind 2)))) (hash "0bxi8wj3wh2y51kypvw3mdsn1knm9b0nxrznigai5l7kfj5hr4sr")))

(define-public crate-zipf-3 (crate (name "zipf") (vers "3.0.0") (deps (list (crate-dep (name "rand") (req "^0.4.0") (default-features #t) (kind 0)) (crate-dep (name "randomkit") (req "^0.1") (default-features #t) (kind 2)))) (hash "1h6wj0karfays8bq23nwa4hr6s25mhb9bplf0c5q6r2mxa13r0gs")))

(define-public crate-zipf-3 (crate (name "zipf") (vers "3.1.0") (deps (list (crate-dep (name "rand") (req "^0.5.0") (default-features #t) (kind 0)) (crate-dep (name "randomkit") (req "^0.1") (default-features #t) (kind 2)))) (hash "08j1p57kybnml96dq7nkj76934cazmhxw826mp1c628nx3pifx72") (yanked #t)))

(define-public crate-zipf-4 (crate (name "zipf") (vers "4.0.0") (deps (list (crate-dep (name "rand") (req "^0.5.0") (default-features #t) (kind 0)) (crate-dep (name "randomkit") (req "^0.1") (default-features #t) (kind 2)))) (hash "0k0fabs83373s1k8v6gbwrfsh454np4vhxdfl0jpg0ddmll8nda4")))

(define-public crate-zipf-4 (crate (name "zipf") (vers "4.0.1") (deps (list (crate-dep (name "rand") (req "^0.5.0") (default-features #t) (kind 0)) (crate-dep (name "randomkit") (req "^0.1") (default-features #t) (kind 2)))) (hash "0gh5cl2vdn2wfnr5akv6yg5b55q9y3vs29dxnhxmgsz202rm9mm9")))

(define-public crate-zipf-4 (crate (name "zipf") (vers "4.1.1") (deps (list (crate-dep (name "rand") (req "^0.6.0") (default-features #t) (kind 0)) (crate-dep (name "randomkit") (req "^0.1") (default-features #t) (kind 2)))) (hash "1vb42dpjqch2v0lqnnrzxadlw33kk39vslqr08a51n9kwq2cikdw") (yanked #t)))

(define-public crate-zipf-5 (crate (name "zipf") (vers "5.0.0") (deps (list (crate-dep (name "rand") (req "^0.6.0") (default-features #t) (kind 0)) (crate-dep (name "randomkit") (req "^0.1") (default-features #t) (kind 2)))) (hash "1pv4lkalgayv2bi6gff952ihg1kclkxh5aqqfvrw0x0hfz8nig1a")))

(define-public crate-zipf-5 (crate (name "zipf") (vers "5.0.1") (deps (list (crate-dep (name "rand") (req "^0.6.0") (default-features #t) (kind 0)) (crate-dep (name "randomkit") (req "^0.1") (default-features #t) (kind 2)))) (hash "07x6a7072xvqxsl5yvam7hqhrz2j1w7fwhiqv7ps1pmyhwnpfmr0")))

(define-public crate-zipf-6 (crate (name "zipf") (vers "6.0.0") (deps (list (crate-dep (name "rand") (req "^0.7.0") (default-features #t) (kind 0)) (crate-dep (name "randomkit") (req "^0.1") (default-features #t) (kind 2)))) (hash "127lqpxx2hcywpgsp1326k8pg3p19kbadmrlb3192h0kls54h3pz")))

(define-public crate-zipf-6 (crate (name "zipf") (vers "6.0.1") (deps (list (crate-dep (name "rand") (req "^0.7.0") (default-features #t) (kind 0)))) (hash "0j381nhsv2nlacn5r8lr9z0a8kl826rr2spib0n30z33mv3jqzvh")))

(define-public crate-zipf-6 (crate (name "zipf") (vers "6.1.0") (deps (list (crate-dep (name "rand") (req "^0.7.0") (default-features #t) (kind 0)))) (hash "12dmk8qsjqg7hd5fcsv12fxkvycj8gjkndpq6v967zsgg9kbh4ly")))

(define-public crate-zipf-7 (crate (name "zipf") (vers "7.0.0") (deps (list (crate-dep (name "rand") (req "^0.8.0") (kind 0)) (crate-dep (name "rand") (req "^0.8.0") (default-features #t) (kind 2)))) (hash "0plr7vph8j9z13z7476df1d0krvrp77v9qdpnnpdzlmml6kqhml3")))

(define-public crate-zipf-7 (crate (name "zipf") (vers "7.0.1") (deps (list (crate-dep (name "rand") (req "^0.8.0") (kind 0)) (crate-dep (name "rand") (req "^0.8.0") (default-features #t) (kind 2)))) (hash "1pcl3wpnil86dgn6dd4ghnfbjrprlrdzl58x03g3mk6q1vd523ir")))

(define-public crate-zipfs-0.0.0 (crate (name "zipfs") (vers "0.0.0") (hash "15g021f43j5fxpknzzn1069jsw4xyg66ncaq75zmvvyn461ga3yv")))

(define-public crate-zipfs-0.0.1 (crate (name "zipfs") (vers "0.0.1") (deps (list (crate-dep (name "async-trait") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "async_zip") (req "^0.0.11") (features (quote ("chrono" "deflate" "zstd"))) (default-features #t) (kind 0)) (crate-dep (name "lunchbox") (req "^0.1") (kind 0)) (crate-dep (name "path-clean") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "pin-project") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1") (features (quote ("io-util"))) (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1") (features (quote ("io-util" "fs"))) (default-features #t) (target "cfg(not(target_family = \"wasm\"))") (kind 0)))) (hash "1s4av1r7gjdh2w48q77ylnjfix09r9frgjmzq984gsxl1sm79pd4")))

(define-public crate-zipfs-0.0.2 (crate (name "zipfs") (vers "0.0.2") (deps (list (crate-dep (name "async-trait") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "async_zip") (req "^0.0.1") (features (quote ("chrono" "deflate" "zstd"))) (default-features #t) (kind 0) (package "async_zip2")) (crate-dep (name "lunchbox") (req "^0.1") (kind 0)) (crate-dep (name "path-clean") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "pin-project") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1") (features (quote ("io-util"))) (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1") (features (quote ("io-util" "fs"))) (default-features #t) (target "cfg(not(target_family = \"wasm\"))") (kind 0)) (crate-dep (name "tokio") (req "^1") (features (quote ("full"))) (default-features #t) (kind 2)) (crate-dep (name "tokio-util") (req "^0.7") (features (quote ("compat"))) (default-features #t) (kind 0)))) (hash "1rxlpmiz552hn3al4mahsynhm71372a95s21iyqs5c0jvjg8pad5")))

