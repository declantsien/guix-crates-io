(define-module (crates-io zi ga) #:use-module (crates-io))

(define-public crate-zigarg-0.1 (crate (name "zigarg") (vers "0.1.0") (hash "06fkvqx3jn29331wqx0248mabbb8v7ag6xbn8gxfsla0cbkm52g1")))

(define-public crate-zigarg-0.2 (crate (name "zigarg") (vers "0.2.0") (hash "0hr7r0d24m9s4yx6mkgikn95mi30v5nldi1dz4id1vf38p4a1ffd")))

(define-public crate-zigarg-0.2 (crate (name "zigarg") (vers "0.2.1") (hash "103n7b65yzkjbs29vj9y52bxa4rcfqdyz14s71cqf3905yi13d8w")))

(define-public crate-zigarg-0.2 (crate (name "zigarg") (vers "0.2.2") (hash "0dn1n5c01kiqckwjw65i5b6jcr3vm8vid6b5vaa7yn5lghvnz4sp")))

(define-public crate-zigarg-0.2 (crate (name "zigarg") (vers "0.2.3") (hash "0lzqyhpncazll900hldb0wb9v24qd4ayhvnyhbig8zpmff1kd5is")))

(define-public crate-zigarg-0.2 (crate (name "zigarg") (vers "0.2.4") (hash "0qff5fscsrvysj3fz2zd2ckyia5w5l2nz3fgzgspfhgyv2x5si7r")))

(define-public crate-zigarg-0.2 (crate (name "zigarg") (vers "0.2.5") (hash "0qlffnrncppa0dd06znx76driv31ph8617p2km49ld42sq38r7mq")))

(define-public crate-zigarg-1 (crate (name "zigarg") (vers "1.0.0") (hash "12187mqi8xrn5m2wfpgg5xxk4h51h77ipc31fxmlhzb7yc4iazdi")))

(define-public crate-zigarg-1 (crate (name "zigarg") (vers "1.1.0") (hash "043w06xsgk7kmmbyf81kv4a71z81jl5h0zf3wmpzyi8g2am4gqw7")))

