(define-module (crates-io zi sv) #:use-module (crates-io))

(define-public crate-zisvalidator-0.1 (crate (name "zisvalidator") (vers "0.1.0") (hash "040n0y5g6m06114ga1xs1hxszyrfs44lnq5749cdfbccccs43b8z")))

(define-public crate-zisvalidator-0.1 (crate (name "zisvalidator") (vers "0.1.1") (deps (list (crate-dep (name "zisvalidator_derive") (req "^0.1") (optional #t) (default-features #t) (kind 0)))) (hash "1s4y4bxrb0dil3bdc8mag1s803w404qg42ibn5qw04jfj282zkhr") (features (quote (("derive" "zisvalidator_derive"))))))

(define-public crate-zisvalidator-0.1 (crate (name "zisvalidator") (vers "0.1.2") (deps (list (crate-dep (name "zisvalidator_derive") (req "^0.1") (optional #t) (default-features #t) (kind 0)))) (hash "109a90xf67vfjd2np38p2k0gyildrmqv999xr96632yrsbf3lj77") (features (quote (("derive" "zisvalidator_derive"))))))

(define-public crate-zisvalidator-0.1 (crate (name "zisvalidator") (vers "0.1.3") (deps (list (crate-dep (name "zisvalidator_derive") (req "^0.1") (optional #t) (default-features #t) (kind 0)))) (hash "0cs5bvkb1vb8bhscy7l4k0gwg7km2nwiya1fnyyndmbhi4bfsb28") (features (quote (("derive" "zisvalidator_derive"))))))

(define-public crate-zisvalidator-0.1 (crate (name "zisvalidator") (vers "0.1.4") (deps (list (crate-dep (name "zisvalidator_derive") (req "^0.1.4") (optional #t) (default-features #t) (kind 0)))) (hash "0jfx43k72r16bn4ijhh694njdq1hlfbzm7mwnz9rrs61binsrzf0") (features (quote (("derive" "zisvalidator_derive"))))))

(define-public crate-zisvalidator-0.1 (crate (name "zisvalidator") (vers "0.1.5") (deps (list (crate-dep (name "zisvalidator_derive") (req "^0.1.5") (optional #t) (default-features #t) (kind 0)))) (hash "1r2k0pxpvscnrqs6kqay086wjds3q6zq139ckiqqv5d6q5margn7") (features (quote (("derive" "zisvalidator_derive"))))))

(define-public crate-zisvalidator-0.1 (crate (name "zisvalidator") (vers "0.1.6") (deps (list (crate-dep (name "zisvalidator_derive") (req "^0.1.6") (optional #t) (default-features #t) (kind 0)))) (hash "12dwb7jjglpzh0zzdz20xy0xzjb1n81qxqhikl9cxnhppl95s0ik") (features (quote (("derive" "zisvalidator_derive"))))))

(define-public crate-zisvalidator_derive-0.1 (crate (name "zisvalidator_derive") (vers "0.1.0") (deps (list (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (features (quote ("extra-traits" "full" "printing"))) (default-features #t) (kind 0)))) (hash "0c656jcirqc5an355a3v18wsr8dvqq9pmm09g8r9byg7h6fr6cgg")))

(define-public crate-zisvalidator_derive-0.1 (crate (name "zisvalidator_derive") (vers "0.1.2") (deps (list (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (features (quote ("extra-traits" "full" "printing"))) (default-features #t) (kind 0)))) (hash "0qqb427vpyqghac5dl9b0m68ayrxyka1mcgs5rhrr4nx25g9l4p7")))

(define-public crate-zisvalidator_derive-0.1 (crate (name "zisvalidator_derive") (vers "0.1.3") (deps (list (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (features (quote ("extra-traits" "full" "printing"))) (default-features #t) (kind 0)))) (hash "0s75xspzz3z1nf54f8ysh4fqws382bpgd2rlghx9gkvhhcqgszcq")))

(define-public crate-zisvalidator_derive-0.1 (crate (name "zisvalidator_derive") (vers "0.1.4") (deps (list (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (features (quote ("extra-traits" "full" "printing"))) (default-features #t) (kind 0)))) (hash "1nn3smmmr7ybmsd2ipm8k566yqr3bki50i78adac6ln4sb1zpbl4")))

(define-public crate-zisvalidator_derive-0.1 (crate (name "zisvalidator_derive") (vers "0.1.5") (deps (list (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (features (quote ("extra-traits" "full" "printing"))) (default-features #t) (kind 0)))) (hash "1dskpg39djv9v1qgnkdlwpb4k9nfjp2cm314yf2mnffvl2b2bdz4")))

(define-public crate-zisvalidator_derive-0.1 (crate (name "zisvalidator_derive") (vers "0.1.6") (deps (list (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (features (quote ("extra-traits" "full" "printing"))) (default-features #t) (kind 0)))) (hash "16jnzh7fcl702xdrbpch7121wb1kshiy4w99vjrjwgqyzl9asa5p")))

