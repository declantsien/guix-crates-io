(define-module (crates-io zi f_) #:use-module (crates-io))

(define-public crate-zif_identity-0.0.1 (crate (name "zif_identity") (vers "0.0.1") (deps (list (crate-dep (name "blake2-rfc") (req "^0.2.18") (default-features #t) (kind 0)) (crate-dep (name "ring") (req "^0.12.1") (default-features #t) (kind 0)) (crate-dep (name "untrusted") (req "^0.5.0") (default-features #t) (kind 0)))) (hash "0509kh0i7zp4cx9fxs33i5d3znd7iw0yiyah1kp7qfw6lh3dnmdr")))

(define-public crate-zif_net-0.0.1 (crate (name "zif_net") (vers "0.0.1") (hash "0z7qwqi2sq61m06z2xsjdz5b0yzmrmz4f1svzr4gjnx19acjw6kx")))

(define-public crate-zif_net-0.1 (crate (name "zif_net") (vers "0.1.0") (deps (list (crate-dep (name "tiny_http") (req "^0.5") (default-features #t) (kind 0)) (crate-dep (name "zif_identity") (req "^0.0.1") (default-features #t) (kind 0)))) (hash "0wcwsd9sc71ddrhq6zvl1fbcpjaqbm721rkg2bzb8lnzwb14w468")))

