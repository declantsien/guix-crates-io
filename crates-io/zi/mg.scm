(define-module (crates-io zi mg) #:use-module (crates-io))

(define-public crate-zimg-sys-0.1 (crate (name "zimg-sys") (vers "0.1.0") (deps (list (crate-dep (name "bindgen") (req "^0.30") (default-features #t) (kind 1)) (crate-dep (name "metadeps") (req "^1.1") (default-features #t) (kind 1)))) (hash "0ada10khsq9kz5bnxp47f8idhr0mr7r4m1a8nn60g7c6rf58ybn2")))

