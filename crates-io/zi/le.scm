(define-module (crates-io zi le) #:use-module (crates-io))

(define-public crate-zilean-0.1 (crate (name "zilean") (vers "0.1.0") (deps (list (crate-dep (name "chrono") (req "^0.4.25") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^4.3.0") (default-features #t) (kind 0)) (crate-dep (name "crossterm") (req "^0.26.1") (default-features #t) (kind 0)) (crate-dep (name "indicatif") (req "^0.17.4") (default-features #t) (kind 0)) (crate-dep (name "inquire") (req "^0.6.2") (default-features #t) (kind 0)))) (hash "1xqfl46zb1kc4iinp4y044xxvdb644lqviijcnjkkg05irs4bjlx")))

