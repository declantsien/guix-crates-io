(define-module (crates-io zi pw) #:use-module (crates-io))

(define-public crate-zipwhip-rs-0.1 (crate (name "zipwhip-rs") (vers "0.1.0") (deps (list (crate-dep (name "reqwest") (req "^0.10") (features (quote ("json"))) (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^0.2") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "0jy6rkqz1n0g5y7b7d7nbs9br6i3dyxla7zxg4ngv3d4ylsk78yp")))

(define-public crate-zipwhip-rs-0.2 (crate (name "zipwhip-rs") (vers "0.2.0") (deps (list (crate-dep (name "reqwest") (req "^0.10") (features (quote ("json"))) (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^0.2") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "0mqsy2fghs2kaflh4kww9063i63976lxlxipq3xnkb5f6n62lpw7")))

(define-public crate-zipwhip-rs-0.2 (crate (name "zipwhip-rs") (vers "0.2.1") (deps (list (crate-dep (name "reqwest") (req "^0.10") (features (quote ("json"))) (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^0.2") (features (quote ("full"))) (default-features #t) (kind 2)))) (hash "1llnghfgfgnfs9g6cwqnxir69v3a3b0g1pjizyjh63gwbnjscgn2")))

(define-public crate-zipwhip-rs-0.3 (crate (name "zipwhip-rs") (vers "0.3.0") (deps (list (crate-dep (name "reqwest") (req "^0.10") (features (quote ("json"))) (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^0.2") (features (quote ("full"))) (default-features #t) (kind 2)))) (hash "1316avw9g2hrxbb76d192g3jj83b64pzjjihkbhi2348z9gh80zb")))

(define-public crate-zipwhip-rs-0.4 (crate (name "zipwhip-rs") (vers "0.4.0") (deps (list (crate-dep (name "reqwest") (req "^0.10") (features (quote ("json"))) (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^0.2") (features (quote ("full"))) (default-features #t) (kind 2)))) (hash "0psijcg46prq3lx750f7m8xgbvp94ih0wsq8cgh90kf9y9zky8q5")))

(define-public crate-zipwhip-rs-0.4 (crate (name "zipwhip-rs") (vers "0.4.1") (deps (list (crate-dep (name "reqwest") (req "^0.10") (features (quote ("json"))) (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^0.2") (features (quote ("full"))) (default-features #t) (kind 2)))) (hash "0iciksn8xp2bli10jb49gdgjwjfhzfhaqsj6dvc3f9k4p9m9vpzc")))

(define-public crate-zipwhip-rs-0.5 (crate (name "zipwhip-rs") (vers "0.5.0") (deps (list (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.10") (features (quote ("json"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^0.2") (features (quote ("full"))) (default-features #t) (kind 2)))) (hash "0sqn2yb9lsffqq8xriaqy7llj6m3kvlr3b7bv3x7j9ghgfyifk7m")))

(define-public crate-zipwhip-rs-0.5 (crate (name "zipwhip-rs") (vers "0.5.1") (deps (list (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.10") (features (quote ("json"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^0.2") (features (quote ("full"))) (default-features #t) (kind 2)))) (hash "1k5wn8ayrhwqx3vbxpq0z24cq3mv4bdbbgjq228cg901pznnwsw2")))

(define-public crate-zipwhip-rs-0.5 (crate (name "zipwhip-rs") (vers "0.5.2") (deps (list (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.10") (features (quote ("json"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^0.2") (features (quote ("full"))) (default-features #t) (kind 2)))) (hash "164cdc350iip6m8yc6bmsgx2p0zrcvyy9pgfrg6k7grhgfmqs253")))

(define-public crate-zipwhip-rs-0.5 (crate (name "zipwhip-rs") (vers "0.5.3") (deps (list (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.10") (features (quote ("json"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^0.2") (features (quote ("full"))) (default-features #t) (kind 2)))) (hash "1l7dkl7hx9y0yh17d66ylg06cq0ndqmaf8dm9mkj9qg3ml2ykhva")))

(define-public crate-zipwhip-rs-0.5 (crate (name "zipwhip-rs") (vers "0.5.4") (deps (list (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.10") (features (quote ("json"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^0.2") (features (quote ("full"))) (default-features #t) (kind 2)))) (hash "07q0xyn8wxm6sfdrdarmqv6r4jikjn3inqdqg8qz28fpbj812gri")))

(define-public crate-zipwhip-rs-0.6 (crate (name "zipwhip-rs") (vers "0.6.0") (deps (list (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.11.0") (features (quote ("json"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.2") (features (quote ("full"))) (default-features #t) (kind 2)))) (hash "1p8jc6iahlx0qjbb0jbqlxb0pwp08c7r24mysqp7va69fr8ka3dc")))

(define-public crate-zipWith-0.1 (crate (name "zipWith") (vers "0.1.0") (hash "00wsmyhx77051y3fgnbwwq6v597my4d5vxlrf6wgibdyiysddjzb")))

(define-public crate-zipWith-0.2 (crate (name "zipWith") (vers "0.2.0") (hash "01jnz0abz6d8yf80nwyr4mfawydph7as9ls97cnzacvd0d4f4b7v")))

