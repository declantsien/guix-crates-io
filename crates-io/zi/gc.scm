(define-module (crates-io zi gc) #:use-module (crates-io))

(define-public crate-zigc-0.0.0 (crate (name "zigc") (vers "0.0.0-alpha0.1") (deps (list (crate-dep (name "osstrtools") (req "^0.2.2") (default-features #t) (kind 0)))) (hash "020pvjy02pjlwf19jiq8ndw4r8kqcmlbbn4mx7imjh2xqb1msypi")))

(define-public crate-zigc-0.0.0 (crate (name "zigc") (vers "0.0.0-alpha0.2") (deps (list (crate-dep (name "osstrtools") (req "^0.2.2") (default-features #t) (kind 0)))) (hash "0r5yrsp9d61xl940nvfwr80d8nyrvxn61nl9cyf2cl85q5983800")))

(define-public crate-zigc-0.0.1 (crate (name "zigc") (vers "0.0.1") (deps (list (crate-dep (name "osstrtools") (req "^0.2.2") (default-features #t) (kind 0)))) (hash "0i0vx5hp80pcy2v7riphv20hbszvwx7vy5k81d8kr0ck0alcqpx1")))

(define-public crate-zigc-0.0.2 (crate (name "zigc") (vers "0.0.2") (deps (list (crate-dep (name "chrono") (req "^0.4.23") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "osstrtools") (req "^0.2.2") (default-features #t) (kind 0)))) (hash "1c57d5sqqld85dk8ydhhc7hgaiqdqn0k4znd13z48n8ii81jkmki") (features (quote (("log" "chrono"))))))

(define-public crate-zigc-0.0.3 (crate (name "zigc") (vers "0.0.3") (deps (list (crate-dep (name "chrono") (req "^0.4.23") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "osstrtools") (req "^0.2.2") (default-features #t) (kind 0)))) (hash "0gkhjy5mi2qdk0chbsw2p4hvp5a8vvxva9m2af9vvnpykkqfpmfr") (features (quote (("log" "chrono"))))))

