(define-module (crates-io zi pu) #:use-module (crates-io))

(define-public crate-ziputil-0.2 (crate (name "ziputil") (vers "0.2.0") (deps (list (crate-dep (name "anyhow") (req "^1.0.31") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "zip") (req "^0.5.5") (default-features #t) (kind 0)))) (hash "0rd9g6dn3k54fpsi7mlb5dzjlhqkxv0zrzk170v9i1fm00f463jw")))

(define-public crate-ziputil-0.3 (crate (name "ziputil") (vers "0.3.0") (deps (list (crate-dep (name "anyhow") (req "^1.0.31") (default-features #t) (kind 0)) (crate-dep (name "pico-args") (req "^0.4.2") (default-features #t) (kind 0)) (crate-dep (name "zip") (req "^0.5.5") (default-features #t) (kind 0)))) (hash "1m4dhn1l5ayc6rb4xzqgs1rvwjrlqwqrf6c0d2gxpdh5jm3ibsbr")))

