(define-module (crates-io zi p3) #:use-module (crates-io))

(define-public crate-zip32-0.0.0 (crate (name "zip32") (vers "0.0.0") (hash "0qsh971096r91kga2wdn07w965zpm77y9rqn1z4bg4q1da25lssi")))

(define-public crate-zip32-0.1 (crate (name "zip32") (vers "0.1.0") (deps (list (crate-dep (name "assert_matches") (req "^1.5") (default-features #t) (kind 2)) (crate-dep (name "blake2b_simd") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "memuse") (req "^0.2.1") (default-features #t) (kind 0)) (crate-dep (name "subtle") (req "^2.2.3") (default-features #t) (kind 0)))) (hash "08kbbs454cb1algyjpfap2jv9r129sc45r8p6rzhpdfzwhxsc96p") (features (quote (("std") ("default" "std")))) (rust-version "1.60")))

(define-public crate-zip32-0.1 (crate (name "zip32") (vers "0.1.1") (deps (list (crate-dep (name "assert_matches") (req "^1.5") (default-features #t) (kind 2)) (crate-dep (name "blake2b_simd") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "memuse") (req "^0.2.1") (default-features #t) (kind 0)) (crate-dep (name "subtle") (req "^2.2.3") (default-features #t) (kind 0)))) (hash "17p1kphdkyic5rd1wkipwdyr2718gffyrzjd0qkpqh69x6pd09j2") (features (quote (("std") ("default" "std")))) (rust-version "1.60")))

(define-public crate-zip321-0.0.0 (crate (name "zip321") (vers "0.0.0") (hash "1dafw0zkwsc76729ci48d85yzxpaih4rsb4ky32qs6xvly8ycrd3") (rust-version "1.65")))

