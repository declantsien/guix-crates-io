(define-module (crates-io hs ml) #:use-module (crates-io))

(define-public crate-hsml-0.1 (crate (name "hsml") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^4.3.0") (features (quote ("cargo"))) (default-features #t) (kind 0)) (crate-dep (name "nom") (req "^7.1.3") (default-features #t) (kind 0)) (crate-dep (name "wasm-bindgen") (req "^0.2.86") (default-features #t) (kind 0)))) (hash "11y66qzk3srlsxjhfshmn3a4k8h6qxg690bd4zjxvpagxpn7jjkr")))

