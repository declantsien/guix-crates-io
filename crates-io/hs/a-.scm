(define-module (crates-io hs a-) #:use-module (crates-io))

(define-public crate-hsa-rt-0.1 (crate (name "hsa-rt") (vers "0.1.0") (deps (list (crate-dep (name "hsa-rt-sys") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.7") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)))) (hash "0jgh56l9vqlrhrhsxdkw506lxpjmp6kdb2br0nayyzzid2wh9n0y")))

(define-public crate-hsa-rt-sys-0.1 (crate (name "hsa-rt-sys") (vers "0.1.0") (deps (list (crate-dep (name "bindgen") (req "^0.51.1") (default-features #t) (kind 1)))) (hash "16923m287rgvpwznh5cz98xlg0hb02jblxz4g42cdr4q2iz7p3bd")))

