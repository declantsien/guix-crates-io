(define-module (crates-io hs -s) #:use-module (crates-io))

(define-public crate-hs-scraper-0.1 (crate (name "hs-scraper") (vers "0.1.0") (deps (list (crate-dep (name "reqwest") (req "^0.8.6") (default-features #t) (kind 0)) (crate-dep (name "select") (req "^0.4.2") (default-features #t) (kind 0)))) (hash "10clznr5gqwswpbp6djxg0milhnj7gljf43cxgpf38p3ihk8kjsl")))

(define-public crate-hs-scraper-0.1 (crate (name "hs-scraper") (vers "0.1.1") (deps (list (crate-dep (name "reqwest") (req "^0.8.6") (default-features #t) (kind 0)) (crate-dep (name "select") (req "^0.4.2") (default-features #t) (kind 0)))) (hash "127vc8w2z9pg932q33jipz8qggbzvs3mnxbh75c2g60a9wnqj4pk")))

