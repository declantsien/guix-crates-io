(define-module (crates-io hs lu) #:use-module (crates-io))

(define-public crate-hsluv-0.1 (crate (name "hsluv") (vers "0.1.0") (deps (list (crate-dep (name "json") (req "^0.11.5") (default-features #t) (kind 2)) (crate-dep (name "lazy_static") (req "^0.2.2") (default-features #t) (kind 2)))) (hash "0g5p4x9np7292fxinqj34vlys5v20hg5yqqr8vvqbw8xcl5l3rax")))

(define-public crate-hsluv-0.3 (crate (name "hsluv") (vers "0.3.0") (deps (list (crate-dep (name "json") (req "^0.11.13") (default-features #t) (kind 1)) (crate-dep (name "num-traits") (req "^0.2.14") (kind 0)))) (hash "10icg8bh2vipr78rkxiskxcaadhjph2fhnx5cba4ar1lap1b9466") (features (quote (("std" "alloc" "num-traits/std") ("libm" "num-traits/libm") ("default" "std") ("alloc"))))))

(define-public crate-hsluv-0.3 (crate (name "hsluv") (vers "0.3.1") (deps (list (crate-dep (name "json") (req "^0.11.13") (default-features #t) (kind 1)) (crate-dep (name "num-traits") (req "^0.2.14") (kind 0)))) (hash "1cmn1hixc0gk0v6ljv7mnix2g1n8w6flr1m1dkx1a56rhgk3r1lf") (features (quote (("std" "alloc" "num-traits/std") ("libm" "num-traits/libm") ("default" "std") ("alloc"))))))

