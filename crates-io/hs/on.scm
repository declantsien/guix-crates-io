(define-module (crates-io hs on) #:use-module (crates-io))

(define-public crate-hson-0.1 (crate (name "hson") (vers "0.1.0") (deps (list (crate-dep (name "lazy_static") (req "^1.2.0") (default-features #t) (kind 2)) (crate-dep (name "uuid") (req "^0.7.1") (features (quote ("v4"))) (default-features #t) (kind 0)))) (hash "0bq01dj3m4azs0wl09bvx72cg4fg1yv2vyialb5dqzyq3cjdhr3l")))

(define-public crate-hson-0.1 (crate (name "hson") (vers "0.1.1") (deps (list (crate-dep (name "lazy_static") (req "^1.2.0") (default-features #t) (kind 2)) (crate-dep (name "uuid") (req "^0.7.1") (features (quote ("v4"))) (default-features #t) (kind 0)))) (hash "04392j5r8abcypvlg38xz00xpqmhx1gq4pk996zjwhnm8iknxqfr")))

(define-public crate-hson-0.1 (crate (name "hson") (vers "0.1.2") (deps (list (crate-dep (name "lazy_static") (req "^1.2.0") (default-features #t) (kind 2)) (crate-dep (name "uuid") (req "^0.7.1") (features (quote ("v4"))) (default-features #t) (kind 0)))) (hash "1afj1hpw69mmdva28nmk2za39ads36jmpzgr80h8sfrslz14ynd2")))

(define-public crate-hson-0.1 (crate (name "hson") (vers "0.1.3") (deps (list (crate-dep (name "lazy_static") (req "^1.2.0") (default-features #t) (kind 2)) (crate-dep (name "uuid") (req "^0.7.1") (features (quote ("v4"))) (default-features #t) (kind 0)))) (hash "07z3as185zqb69sszvk0k6a6plkx7rgrn7d0qdzf0slg3yjmz1mj")))

(define-public crate-hson-0.1 (crate (name "hson") (vers "0.1.4") (deps (list (crate-dep (name "lazy_static") (req "^1.2.0") (default-features #t) (kind 2)))) (hash "0a5m042kbj1q62a446qwbxpw0iwbyyikscnd17m2d4in3khl4rgi")))

(define-public crate-hson-0.1 (crate (name "hson") (vers "0.1.5") (deps (list (crate-dep (name "lazy_static") (req "^1.2.0") (default-features #t) (kind 2)))) (hash "0cfpzyfpf0ylp1519r7sfr1s9gszvnbggbbag8f7pxmzxzpn845k")))

(define-public crate-hson-0.1 (crate (name "hson") (vers "0.1.6") (deps (list (crate-dep (name "lazy_static") (req "^1.2.0") (default-features #t) (kind 2)))) (hash "0kdgcjfjd40pjwr16lzxbwbjd8a4cr4c9x2nnvgk6bvdi8xrk0ad")))

(define-public crate-hson-0.1 (crate (name "hson") (vers "0.1.7") (deps (list (crate-dep (name "lazy_static") (req "^1.2.0") (default-features #t) (kind 2)))) (hash "0w0qq9y22cs4c9pw8c0rl662f749d0s1acqil31vjrhl5c3x8i6w")))

(define-public crate-hson-0.1 (crate (name "hson") (vers "0.1.8") (deps (list (crate-dep (name "lazy_static") (req "^1.2.0") (default-features #t) (kind 2)))) (hash "1f4sz5i6rzad9cls9bdll9rpajgd19jdhs9hg28mdgywhb3gav5h")))

(define-public crate-hson-0.1 (crate (name "hson") (vers "0.1.9") (deps (list (crate-dep (name "lazy_static") (req "^1.2.0") (default-features #t) (kind 2)))) (hash "0nrpc7bpx206ldgndkj1b2fbw7r6jiv5sj0mf03qcvmy0mc755mz")))

(define-public crate-hson-0.1 (crate (name "hson") (vers "0.1.10") (deps (list (crate-dep (name "lazy_static") (req "^1.2.0") (default-features #t) (kind 2)))) (hash "0ic829dv3r55v3lcmy76yx90la5jl6bmxq5723zmzs36fn816yg0")))

(define-public crate-hson-0.1 (crate (name "hson") (vers "0.1.11") (deps (list (crate-dep (name "lazy_static") (req "^1.2.0") (default-features #t) (kind 2)))) (hash "067nallgsccibn7wav62r46gw6g65pjfv83zp9qvcjm70nhhlapr")))

(define-public crate-hson_gen-0.1 (crate (name "hson_gen") (vers "0.1.0") (deps (list (crate-dep (name "hson") (req "^0.1.4") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.2.0") (default-features #t) (kind 0)))) (hash "0bqahb8hhn8fkl6a1gkx23qiw6hqdyjz082x5k70pa59gjcyf0cz")))

