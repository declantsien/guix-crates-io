(define-module (crates-io hs h_) #:use-module (crates-io))

(define-public crate-hsh_game-0.1 (crate (name "hsh_game") (vers "0.1.0") (deps (list (crate-dep (name "rand") (req "^0.4.3") (default-features #t) (kind 0)) (crate-dep (name "uuid") (req "^0.7") (features (quote ("v4"))) (default-features #t) (kind 0)))) (hash "1ii5s771dscz21ljcrqb9p5709m7v4va4v9knwi86qlkkjnnv6f2")))

(define-public crate-hsh_game-0.1 (crate (name "hsh_game") (vers "0.1.1") (deps (list (crate-dep (name "rand") (req "^0.4.3") (default-features #t) (kind 0)) (crate-dep (name "uuid") (req "^0.7") (features (quote ("v4"))) (default-features #t) (kind 0)))) (hash "1r7a9q4p9xs09gr92xhl47q4qypn77gravlx759iqg8v1hpmjr97")))

