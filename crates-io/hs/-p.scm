(define-module (crates-io hs -p) #:use-module (crates-io))

(define-public crate-hs-pack-0.2 (crate (name "hs-pack") (vers "0.2.0") (deps (list (crate-dep (name "ansi_term") (req "^0.12") (default-features #t) (kind 0)) (crate-dep (name "displaydoc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "toml") (req "^0.5") (default-features #t) (kind 0)))) (hash "0lwvp0dxdldxpylhidb9r5qm43xwh159140d38s4d2l99m6mmrm2") (yanked #t)))

(define-public crate-hs-pack-0.2 (crate (name "hs-pack") (vers "0.2.1") (hash "185gm4dm26cv40ja6b5l2y8n5pvy5lywzsqklxiswm7w9hhn67ji") (yanked #t)))

(define-public crate-hs-pack-0.2 (crate (name "hs-pack") (vers "0.2.2") (hash "0rd74ckg0hib52yir1if181v65crsahyb8v4pg8igfb70z5jiwqj") (rust-version "1.62.1")))

