(define-module (crates-io hs #{25}#) #:use-module (crates-io))

(define-public crate-hs256-bin-0.1 (crate (name "hs256-bin") (vers "0.1.0") (deps (list (crate-dep (name "base64ct") (req "^1") (features (quote ("alloc"))) (default-features #t) (kind 0)) (crate-dep (name "ring") (req "^0.16") (default-features #t) (kind 0)))) (hash "0lq7h5ra3z6zsdcqfxljwszpgk48pp137ki6xs6gdln1nxfrqwsa")))

(define-public crate-hs256-token-0.1 (crate (name "hs256-token") (vers "0.1.0") (deps (list (crate-dep (name "base64ct") (req "^1") (features (quote ("alloc"))) (default-features #t) (kind 0)) (crate-dep (name "ring") (req "^0.16") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1") (default-features #t) (kind 0)))) (hash "0x6cr179h0gnlsa883yw32864in5irdsqswchkaxl4jawr56lbfd")))

