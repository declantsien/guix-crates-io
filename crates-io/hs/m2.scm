(define-module (crates-io hs m2) #:use-module (crates-io))

(define-public crate-hsm2descriptors-0.1 (crate (name "hsm2descriptors") (vers "0.1.0") (deps (list (crate-dep (name "bitcoin") (req "^0.25.2") (default-features #t) (kind 0)) (crate-dep (name "hex") (req "^0.4.2") (default-features #t) (kind 0)) (crate-dep (name "hkdf") (req "^0.10.0") (default-features #t) (kind 0)) (crate-dep (name "sha2") (req "^0.9") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.3.21") (default-features #t) (kind 0)))) (hash "0gy5jxi3amdjh4fk0br93qxc58ynf28k02ci9jcjmr57969j3ygp")))

