(define-module (crates-io hs -r) #:use-module (crates-io))

(define-public crate-hs-rust-learn-0.1 (crate (name "hs-rust-learn") (vers "0.1.0") (deps (list (crate-dep (name "rand") (req "^0.7.3") (default-features #t) (kind 0)))) (hash "1wzixv7yd517hwwycza7awh3iv1689nj3i1drvrv7lb3ybfpx6ps")))

