(define-module (crates-io hs #{10}#) #:use-module (crates-io))

(define-public crate-hs100api-0.1 (crate (name "hs100api") (vers "0.1.0") (deps (list (crate-dep (name "byteorder") (req "^1.1") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)))) (hash "1w427zcgffshjyab00xapb2ns8mpyxp9ca7mlhw4x5qbf1v6qzjf")))

(define-public crate-hs100api-0.1 (crate (name "hs100api") (vers "0.1.1") (deps (list (crate-dep (name "byteorder") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)))) (hash "1mkqbli9k1n21fzs92jassb1d2zavq48v1bf418fckqnqqcfin7c")))

