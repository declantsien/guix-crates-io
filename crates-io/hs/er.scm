(define-module (crates-io hs er) #:use-module (crates-io))

(define-public crate-hserde-0.1 (crate (name "hserde") (vers "0.1.0") (hash "0p1989a917y204d3cvdvx404iy4d2q6ja1smhq5j2gg7sk1kaja7")))

(define-public crate-hserde-0.1 (crate (name "hserde") (vers "0.1.1") (hash "18fs2pgkw1mj630sl2rwpw4kmyhx3klj8pfrkvzm0nzkixp6hxhq")))

(define-public crate-hserde-0.2 (crate (name "hserde") (vers "0.2.0") (hash "0k9gw6gfv1744gxvbxjphf8p8rpsa294bkncmyqm40fkjn3ng1dv")))

