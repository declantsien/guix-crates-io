(define-module (crates-io hs ta) #:use-module (crates-io))

(define-public crate-hstats-0.1 (crate (name "hstats") (vers "0.1.0") (deps (list (crate-dep (name "float-cmp") (req "^0.9.0") (default-features #t) (kind 2)) (crate-dep (name "num-traits") (req "^0.2.15") (features (quote ("libm"))) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 2)) (crate-dep (name "rand_distr") (req "^0.4.3") (default-features #t) (kind 2)) (crate-dep (name "rayon") (req "^1.7.0") (default-features #t) (kind 2)) (crate-dep (name "rolling-stats") (req "^0.7.0") (default-features #t) (kind 0)))) (hash "0zwx6dm8kn02f8iikkzgj46i6dl2yij8sw9c9ifilzc3kdb99wz2")))

