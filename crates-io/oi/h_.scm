(define-module (crates-io oi h_) #:use-module (crates-io))

(define-public crate-oih_grrs-0.1 (crate (name "oih_grrs") (vers "0.1.0") (deps (list (crate-dep (name "assert_cmd") (req "^0.10") (default-features #t) (kind 2)) (crate-dep (name "exitfailure") (req "^0.5.1") (default-features #t) (kind 0)) (crate-dep (name "failure") (req "^0.1.5") (default-features #t) (kind 0)) (crate-dep (name "predicates") (req "^1") (default-features #t) (kind 2)) (crate-dep (name "structopt") (req "^0.2.10") (default-features #t) (kind 0)) (crate-dep (name "tempfile") (req "^3") (default-features #t) (kind 2)))) (hash "0w0dyiihrrjvf5f7dgisrzz1m236pwss064wp2ykg8453345sz8h")))

