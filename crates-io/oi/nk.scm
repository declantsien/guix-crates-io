(define-module (crates-io oi nk) #:use-module (crates-io))

(define-public crate-oink-0.1 (crate (name "oink") (vers "0.1.0") (deps (list (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)))) (hash "1r46lkyxl05lnz3y1h9304valzz9vgm7i5xsrfn58gpxjmiffhyx")))

