(define-module (crates-io oi -u) #:use-module (crates-io))

(define-public crate-oi-unwrap-0.1 (crate (name "oi-unwrap") (vers "0.1.0") (hash "0g9sv0fpsnlapszr2l9n4ax05nmwbmjlk7xifnrbs9hk6ylc7zzz")))

(define-public crate-oi-unwrap-0.1 (crate (name "oi-unwrap") (vers "0.1.1") (hash "12vg37fhbs0y4b2jgci4x3pc8dgyi78lb9v7q5cgh52p5yd3iflk")))

