(define-module (crates-io oi su) #:use-module (crates-io))

(define-public crate-oisuite-1 (crate (name "oisuite") (vers "1.0.0") (deps (list (crate-dep (name "home") (req "^0.5.3") (default-features #t) (kind 0)) (crate-dep (name "termion") (req "^1.5.6") (default-features #t) (kind 0)) (crate-dep (name "yaml-rust") (req "^0.4") (default-features #t) (kind 0)))) (hash "1rv89xa6fs1b06qmsy9wk89w9a7d3i0yzyrjg0qbq0fqla8x25l4")))

