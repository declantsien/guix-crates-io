(define-module (crates-io oi l_) #:use-module (crates-io))

(define-public crate-oil_parsers-0.1 (crate (name "oil_parsers") (vers "0.1.0") (deps (list (crate-dep (name "num") (req "*") (default-features #t) (kind 0)) (crate-dep (name "oil_shared") (req "= 0.1.0") (default-features #t) (kind 0)) (crate-dep (name "phf") (req "*") (default-features #t) (kind 0)) (crate-dep (name "phf_macros") (req "*") (default-features #t) (kind 0)) (crate-dep (name "xml-rs") (req "*") (default-features #t) (kind 0)))) (hash "1pgw1rrr0if5i8cv5bypa9y8kz1m46cgflr9hcdbbsmbz39g6yk7")))

(define-public crate-oil_shared-0.1 (crate (name "oil_shared") (vers "0.1.0") (deps (list (crate-dep (name "phf") (req "*") (default-features #t) (kind 0)) (crate-dep (name "phf_macros") (req "*") (default-features #t) (kind 0)))) (hash "08z8hniz9dpb3x8cnszjmcgclbkcarv7pgp3lbzrwhxvfrlxvnms")))

