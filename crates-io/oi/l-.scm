(define-module (crates-io oi l-) #:use-module (crates-io))

(define-public crate-oil-lang-2 (crate (name "oil-lang") (vers "2.2.0") (deps (list (crate-dep (name "fnv") (req "^1.0.5") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.3.15") (default-features #t) (kind 0)))) (hash "0b2f2ady8k8z7l3rmywa06370vcjmcacfi88bjhi3cvkvxn08nab")))

(define-public crate-oil-lang-2 (crate (name "oil-lang") (vers "2.2.1") (deps (list (crate-dep (name "fnv") (req "^1.0.5") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.3.15") (default-features #t) (kind 0)))) (hash "11a1zmk92n2acyi92ysg6zn5c1qizxz1k4zjns338dabz8b2fhd5")))

(define-public crate-oil-lang-2 (crate (name "oil-lang") (vers "2.3.0") (deps (list (crate-dep (name "fnv") (req "^1.0.5") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.3") (default-features #t) (kind 0)))) (hash "1v01b7di33pjs6c536jgkm9m22i4h3byqy9xwsg1d53waxf76gzn")))

(define-public crate-oil-lang-2 (crate (name "oil-lang") (vers "2.4.0") (deps (list (crate-dep (name "fnv") (req "^1.0.7") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.7.3") (default-features #t) (kind 0)))) (hash "0rdzylwigy6kqf9fjv2rv41mmfxli6iav594w8mcvpcc70xl01pa")))

