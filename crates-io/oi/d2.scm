(define-module (crates-io oi d2) #:use-module (crates-io))

(define-public crate-oid2-0.1 (crate (name "oid2") (vers "0.1.0") (deps (list (crate-dep (name "chrono") (req "^0.4.24") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "sqlx") (req "^0.6.3") (features (quote ("runtime-tokio-rustls"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "time") (req "^0.3.20") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "uuid") (req "^1.3.1") (optional #t) (default-features #t) (kind 0)))) (hash "0pr4y4rvvs78iz9rpmg61z0bnzkrrxfifp66z0qxslviflbd428s") (features (quote (("default" "rand" "chrono"))))))

