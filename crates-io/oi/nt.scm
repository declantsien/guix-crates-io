(define-module (crates-io oi nt) #:use-module (crates-io))

(define-public crate-oint-0.1 (crate (name "oint") (vers "0.1.0") (hash "1mdl9964a5c3b1r31pixwgrz5iqw3wmkl3x8rwa8z9cdrvhwl1yn")))

(define-public crate-oint-0.1 (crate (name "oint") (vers "0.1.1") (hash "0qada6n4vvk5kn9nxyw3van70bsidy47yxjjbk76hhq9j86nv58h")))

(define-public crate-ointer-0.1 (crate (name "ointer") (vers "0.1.0") (hash "04z0pkxkys6mibyajcg40ybjrxf4916ra1f2dp5q9zznllali8fr") (yanked #t)))

(define-public crate-ointer-0.2 (crate (name "ointer") (vers "0.2.0") (hash "0gk07fzhn20sncjwi2mlcz2rp5f81c9c4ilz9j1cw2bj8wcbk6s2") (yanked #t)))

(define-public crate-ointer-1 (crate (name "ointer") (vers "1.0.0") (hash "0r98bp5n69xzikwss75mk0idhhbk5cxf139xavlady0514mk38cw") (yanked #t)))

(define-public crate-ointer-1 (crate (name "ointer") (vers "1.1.0") (hash "1w13wbg9mxqsf450cpazg9gslisrwq7nggsxnbk67ss77fbadl0q") (yanked #t)))

(define-public crate-ointer-1 (crate (name "ointer") (vers "1.1.1") (hash "1yyrkqqr1zm32vn09ngj6ab8i6yhnl8badq8d8ry0jrwhm0hawq8") (yanked #t)))

(define-public crate-ointer-2 (crate (name "ointer") (vers "2.0.0") (hash "17hqmmbrcv1mw3xz5d7ra2q61dnfwkfis69hlxmrshpx2h1v7ibf") (yanked #t)))

(define-public crate-ointer-2 (crate (name "ointer") (vers "2.1.0") (hash "1s3w58i2lhnji4j968wfyzirjar3h2p9kvx9mplsvxcqznwn96mb") (yanked #t)))

(define-public crate-ointer-2 (crate (name "ointer") (vers "2.1.1") (hash "0yl3q7gf2bp2pjngvqi7v0micg5aqrkxaamrz9f6zlh6xlf74q16") (yanked #t)))

(define-public crate-ointer-3 (crate (name "ointer") (vers "3.0.0") (deps (list (crate-dep (name "paste") (req "^1.0.14") (default-features #t) (kind 0)))) (hash "1717bj4wv1xyni16mys1c65amal8zqr2cn36mrzi4yyqyaqw8p9z") (yanked #t)))

(define-public crate-ointer-3 (crate (name "ointer") (vers "3.0.1") (deps (list (crate-dep (name "paste") (req "^1.0.14") (default-features #t) (kind 0)))) (hash "0wl86pi4l0zqf7gc6964qvvx6cdw9nq7mr317w1w3g6cy3hj9sg5") (yanked #t)))

(define-public crate-ointer-3 (crate (name "ointer") (vers "3.0.2") (deps (list (crate-dep (name "paste") (req "^1.0.14") (default-features #t) (kind 0)))) (hash "107nhlpfn77d9yf9p8v52c2hf206zb2y7p5qlh6jmdayxcll7n9s") (yanked #t)))

(define-public crate-ointer-3 (crate (name "ointer") (vers "3.0.3") (deps (list (crate-dep (name "paste") (req "^1.0.14") (default-features #t) (kind 0)))) (hash "1yv4zd9jxlrv02kh2dv2k85a27qs52hni2rk1p5ahyb4lsh67b5c") (yanked #t)))

(define-public crate-ointer-3 (crate (name "ointer") (vers "3.0.4") (deps (list (crate-dep (name "paste") (req "^1.0.14") (default-features #t) (kind 0)))) (hash "0z8xhj4ilw7i6yhji00jzgwgfjcf9acrxnqx2msz30sqabpx121f") (yanked #t)))

(define-public crate-ointer-3 (crate (name "ointer") (vers "3.0.5") (deps (list (crate-dep (name "paste") (req "^1.0.14") (default-features #t) (kind 0)))) (hash "0l4l86ll0gyjgdwi7pcb8pragzw5plsph01imap6g7l001ynv87g") (yanked #t)))

(define-public crate-ointer-3 (crate (name "ointer") (vers "3.0.6") (deps (list (crate-dep (name "paste") (req "^1.0.14") (default-features #t) (kind 0)))) (hash "1sfdgs7z798x9n0qz65qihmqlgdpyi9zq07w1w2kdb4vv4qyhd3w") (yanked #t)))

(define-public crate-ointer-3 (crate (name "ointer") (vers "3.0.7") (deps (list (crate-dep (name "paste") (req "^1.0.14") (default-features #t) (kind 0)))) (hash "1a0s5bswryhqi4q87fp70ymj2wn1kdi0l03k4b7lhcdk1g3dq0pj") (yanked #t)))

(define-public crate-ointer-3 (crate (name "ointer") (vers "3.0.8") (deps (list (crate-dep (name "paste") (req "^1.0.14") (default-features #t) (kind 0)))) (hash "1wk1wmvzyx4z63hzl0pjrcwpzncc03836rapvzva4kg97m7h9lw2") (yanked #t)))

(define-public crate-ointer-3 (crate (name "ointer") (vers "3.0.9") (deps (list (crate-dep (name "paste") (req "^1.0") (default-features #t) (kind 0)))) (hash "0rzkpqfqzsbh3wddsp7zz11iphd7kgz72mkbakhz73qiz30y0x1x") (yanked #t)))

(define-public crate-ointer-3 (crate (name "ointer") (vers "3.0.10") (deps (list (crate-dep (name "paste") (req "^1.0") (default-features #t) (kind 0)))) (hash "11zml9ka6123qmbw27blr1g1w075hiwl2zyfcfb8h3094g4yrczh") (yanked #t)))

(define-public crate-ointer-3 (crate (name "ointer") (vers "3.0.11") (deps (list (crate-dep (name "paste") (req "^1") (default-features #t) (kind 0)))) (hash "0p0d0vidy3l40nv6islwyq38nnr1wixnkkbviivhj65nrdqk1lmb") (yanked #t)))

(define-public crate-ointer-3 (crate (name "ointer") (vers "3.0.12") (deps (list (crate-dep (name "paste") (req "^1") (default-features #t) (kind 0)))) (hash "0q2sg2zqh509hi7i4fzpshvg5mnpk3p97ii7qyi0w52gcxzrajxy")))

(define-public crate-ointers-1 (crate (name "ointers") (vers "1.0.0") (deps (list (crate-dep (name "rand") (req "^0.8.4") (default-features #t) (kind 2)) (crate-dep (name "rayon") (req "^1.5.1") (default-features #t) (kind 2)))) (hash "0qwawfiisqs9zy4x9b6mdfnfyjbsgb31zcx0g8b9fagnxc0cip1y") (yanked #t)))

(define-public crate-ointers-2 (crate (name "ointers") (vers "2.0.0") (deps (list (crate-dep (name "rand") (req "^0.8.4") (default-features #t) (kind 2)) (crate-dep (name "rayon") (req "^1.5.1") (default-features #t) (kind 2)))) (hash "1zfrdq9rg69626gndkbwm52nr7r8qix8bm2gfzlg8czr67g7kwg3") (yanked #t)))

(define-public crate-ointers-3 (crate (name "ointers") (vers "3.0.0") (deps (list (crate-dep (name "rand") (req "^0.8.4") (default-features #t) (kind 2)) (crate-dep (name "rayon") (req "^1.5.1") (default-features #t) (kind 2)))) (hash "0645wpzvyi5fkl9nqjli5x0qm8rni00jsp6hqikwp01bar6r2n6m") (yanked #t)))

(define-public crate-ointers-3 (crate (name "ointers") (vers "3.0.1") (deps (list (crate-dep (name "rand") (req "^0.8.4") (default-features #t) (kind 2)) (crate-dep (name "rayon") (req "^1.5.1") (default-features #t) (kind 2)))) (hash "16nnj8cr636w4fs09amvdrb4rxi9vxrgm26zwxmrvrxbykggb9d8") (features (quote (("i_know_what_im_doing") ("default")))) (yanked #t)))

(define-public crate-ointers-4 (crate (name "ointers") (vers "4.0.0") (deps (list (crate-dep (name "rand") (req "^0.8.4") (default-features #t) (kind 2)) (crate-dep (name "rayon") (req "^1.5.1") (default-features #t) (kind 2)))) (hash "0s31gfs51j1gqzvamwf4lgz689z99f49lvkl65f8i246327d4bmg") (features (quote (("i_know_what_im_doing") ("default" "alloc") ("alloc"))))))

(define-public crate-ointers-4 (crate (name "ointers") (vers "4.0.1") (deps (list (crate-dep (name "rand") (req "^0.8.4") (default-features #t) (kind 2)) (crate-dep (name "rayon") (req "^1.5.1") (default-features #t) (kind 2)) (crate-dep (name "sptr") (req "^0.3") (optional #t) (default-features #t) (kind 0)))) (hash "1zjfqqk28r3zfbnhq2lww516zj0d8j5chysf8kxqvayv26y6rc12") (features (quote (("i_know_what_im_doing") ("default" "alloc") ("alloc"))))))

