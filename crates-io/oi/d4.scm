(define-module (crates-io oi d4) #:use-module (crates-io))

(define-public crate-oid4vc-0.1 (crate (name "oid4vc") (vers "0.1.0") (deps (list (crate-dep (name "dif-presentation-exchange") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "oid4vci") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "oid4vp") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "siopv2") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "008bf52bb5023s46g7lg2x5c46iy8jb5sl2p46x76h3pib2hmx70")))

(define-public crate-oid4vci-0.1 (crate (name "oid4vci") (vers "0.1.0") (hash "1rxagkzabl2dm2w1xpwvqa5c4x7ja1p12m0xy5m7bp8hhkbfvnz6")))

(define-public crate-oid4vp-0.1 (crate (name "oid4vp") (vers "0.1.0") (hash "0bxac3jp6bjhfv6pp4xg55i3lh4afh81ac3n9rsg4j76lzjz3dmi")))

