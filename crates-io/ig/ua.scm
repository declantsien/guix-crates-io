(define-module (crates-io ig ua) #:use-module (crates-io))

(define-public crate-iguana-interpreter-0.1 (crate (name "iguana-interpreter") (vers "0.1.0") (deps (list (crate-dep (name "bitcoin-cash") (req "^0.1.2") (default-features #t) (kind 0)))) (hash "1icq44dz8ikxbc8pnlcllvnjnaai0wr1nyim334h8mb8by6rybwl")))

(define-public crate-iguana-interpreter-0.1 (crate (name "iguana-interpreter") (vers "0.1.1") (deps (list (crate-dep (name "bitcoin-cash") (req "^0.1.3") (default-features #t) (kind 0)))) (hash "1vg1f7px42b6f0lca0adbsh3a8np3pfrk50nf6yqw2gkf1pwc7ci")))

(define-public crate-iguana-interpreter-0.2 (crate (name "iguana-interpreter") (vers "0.2.0") (deps (list (crate-dep (name "bitcoin-cash") (req "^0.2.0") (default-features #t) (kind 0)))) (hash "1rngm8wgg3wa53ljm1h8ysvabmqrbwaskbhdhvjn1hw0jd1zadf4")))

(define-public crate-iguana-interpreter-0.2 (crate (name "iguana-interpreter") (vers "0.2.3") (deps (list (crate-dep (name "bitcoin-cash") (req "^0.2.3") (default-features #t) (kind 0)))) (hash "1vs6cb3rz9xcykcnf2a65gh87x4yyjsma1j6fr3pd26myjww6j60")))

(define-public crate-iguana-ws-0.1 (crate (name "iguana-ws") (vers "0.1.0") (deps (list (crate-dep (name "bitcoin-cash") (req "^0.1.2") (default-features #t) (kind 0)) (crate-dep (name "futures") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.4") (default-features #t) (kind 0)) (crate-dep (name "pretty_env_logger") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "warp") (req "^0.2") (default-features #t) (kind 0)))) (hash "0318i2z43xlc9lf61wm182pxi0gq4p3rq125canwwbdnx409aiii")))

(define-public crate-iguana-ws-0.1 (crate (name "iguana-ws") (vers "0.1.1") (deps (list (crate-dep (name "bitcoin-cash") (req "^0.1.3") (default-features #t) (kind 0)) (crate-dep (name "futures") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.4") (default-features #t) (kind 0)) (crate-dep (name "pretty_env_logger") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "warp") (req "^0.2") (default-features #t) (kind 0)))) (hash "1x3scvr9f9byvnkbi806gjycdpzvgwwgkibg75ga4rm07rmy6yvg")))

(define-public crate-iguana-ws-0.2 (crate (name "iguana-ws") (vers "0.2.0") (deps (list (crate-dep (name "bitcoin-cash") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "futures") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.4") (default-features #t) (kind 0)) (crate-dep (name "pretty_env_logger") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "warp") (req "^0.2") (default-features #t) (kind 0)))) (hash "1aqr873khixl7qyzg0jsq6kwsimg2mwdgqagh0fvb8m0jnzms9aw")))

