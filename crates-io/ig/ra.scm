(define-module (crates-io ig ra) #:use-module (crates-io))

(define-public crate-igraph-0.1 (crate (name "igraph") (vers "0.1.0") (hash "0aj8zsbccw12slcf9pyygv3ndjnikh6d1r9cw1b043clzi82d1cf") (yanked #t)))

(define-public crate-igraph-0.1 (crate (name "igraph") (vers "0.1.1") (hash "0hcq6zb4r35hdsz6f1y3dpc991mk3s0s60mhiy6b23phyrq6qqk8")))

(define-public crate-igraph-sys-0.0.0 (crate (name "igraph-sys") (vers "0.0.0") (hash "1y2jhwyx7sl2sdqacr0522qplib0ss39g5lp0v3w4f9axwbv0k74")))

(define-public crate-igraph-sys-0.0.1 (crate (name "igraph-sys") (vers "0.0.1") (deps (list (crate-dep (name "bindgen") (req "^0.60") (default-features #t) (kind 1)) (crate-dep (name "cc") (req "^1") (default-features #t) (kind 1)) (crate-dep (name "cmake") (req "^0.1") (default-features #t) (kind 1)))) (hash "0l1gddj749ixvlfaqagi0dyihqy988gpgagmm6gz46gfc3zk92hg") (links "igraph")))

