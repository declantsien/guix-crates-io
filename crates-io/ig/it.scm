(define-module (crates-io ig it) #:use-module (crates-io))

(define-public crate-igit-0.1 (crate (name "igit") (vers "0.1.0") (deps (list (crate-dep (name "anyhow") (req "^1.0.75") (default-features #t) (kind 0)) (crate-dep (name "gix") (req "^0.52.0") (features (quote ("blocking-network-client" "blocking-http-transport-reqwest-native-tls"))) (default-features #t) (kind 0)) (crate-dep (name "tree-sitter") (req "^0.20.10") (default-features #t) (kind 0)) (crate-dep (name "tree-sitter-loader") (req "^0.20.0") (default-features #t) (kind 0)))) (hash "1r1r5845mr1bjy15zlnmf04b9861iybng8a7z313bnid5c86yyzy")))

