(define-module (crates-io ig pa) #:use-module (crates-io))

(define-public crate-igpay-atinlay-0.1 (crate (name "igpay-atinlay") (vers "0.1.0") (deps (list (crate-dep (name "is-vowel") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.4.5") (default-features #t) (kind 0)) (crate-dep (name "version-sync") (req "^0.9") (default-features #t) (kind 2)))) (hash "190xh0p49g2nq0ar5lz1l5z1bg5br39hzq7lcw0vkjqgad3h9qcw")))

