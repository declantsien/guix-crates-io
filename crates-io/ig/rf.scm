(define-module (crates-io ig rf) #:use-module (crates-io))

(define-public crate-igrf-0.1 (crate (name "igrf") (vers "0.1.0") (deps (list (crate-dep (name "thiserror") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "time") (req "^0.2") (default-features #t) (kind 0)))) (hash "1algwzd36w2lvzmn2y1bcvvd00l925f2crrg1bmvdzispsr1fa7v")))

(define-public crate-igrf-0.2 (crate (name "igrf") (vers "0.2.0") (deps (list (crate-dep (name "thiserror") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "time") (req "^0.3") (default-features #t) (kind 0)))) (hash "16n7byhj3b8sln1nmvimb1lfrc6s5k20y389n02irf5pcan51dxf")))

