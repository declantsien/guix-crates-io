(define-module (crates-io ig -b) #:use-module (crates-io))

(define-public crate-ig-brokers-0.0.1 (crate (name "ig-brokers") (vers "0.0.1") (deps (list (crate-dep (name "chrono") (req "^0.4.19") (features (quote ("serde"))) (default-features #t) (kind 0)) (crate-dep (name "dotenv") (req "^0.15.0") (default-features #t) (kind 2)) (crate-dep (name "reqwest") (req "^0.11.4") (features (quote ("blocking" "json"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.117") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "11y9i3whppzqmal381qh4jphqwjkmfl6liga5xg6a638hwv1cvnx")))

