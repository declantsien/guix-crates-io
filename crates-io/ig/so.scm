(define-module (crates-io ig so) #:use-module (crates-io))

(define-public crate-igsolve-0.1 (crate (name "igsolve") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^4.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "igs") (req "^0.1") (default-features #t) (kind 0)))) (hash "1h7y09kcr1i91pjy8wdcirx55savyx61vyarkd69ifbf4vcad4lf")))

(define-public crate-igsolve-0.1 (crate (name "igsolve") (vers "0.1.1") (deps (list (crate-dep (name "clap") (req "^4.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "igs") (req "^0.1") (default-features #t) (kind 0)))) (hash "1wxmi4dw5d5v2bl54iphm8dwvr5qi8bhiya1xv42k63sfpvj0i4c")))

(define-public crate-igsolve-0.1 (crate (name "igsolve") (vers "0.1.2") (deps (list (crate-dep (name "clap") (req "^4.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "igs") (req "^0.1") (default-features #t) (kind 0)))) (hash "1hbpv3402wly14vxq46a59yk6286qnmsc4v6p7xixcv0zbiian4n")))

