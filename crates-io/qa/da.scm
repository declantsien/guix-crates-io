(define-module (crates-io qa da) #:use-module (crates-io))

(define-public crate-qadapt-0.1 (crate (name "qadapt") (vers "0.1.0") (deps (list (crate-dep (name "libc") (req "^0.2") (kind 0)) (crate-dep (name "spin") (req "^0.4") (default-features #t) (kind 0)))) (hash "05x5aalyxvwv04vyfqwc0na581vc5f2dl13pv57y24fcwkx2h1zl")))

(define-public crate-qadapt-0.2 (crate (name "qadapt") (vers "0.2.0") (deps (list (crate-dep (name "libc") (req "^0.2") (kind 0)) (crate-dep (name "spin") (req "^0.4") (default-features #t) (kind 0)))) (hash "1w8wkbmw5ifl19dh5jqg99kifg7ig9d7j6vzq716alhrgx6f6ikf")))

(define-public crate-qadapt-0.2 (crate (name "qadapt") (vers "0.2.1") (deps (list (crate-dep (name "libc") (req "^0.2") (kind 0)) (crate-dep (name "spin") (req "^0.4") (default-features #t) (kind 0)))) (hash "158037mgryhs7jb13scdn7jjgsi773pcqpglzhk066511l318wav")))

(define-public crate-qadapt-0.3 (crate (name "qadapt") (vers "0.3.0") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "qadapt-macro") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "spin") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "thread-id") (req "^3.3") (default-features #t) (kind 0)))) (hash "0gv1wiy6yl0mq3dp0pyp04rxp2j94ckwp71915i4fjcqx9ikp1dm")))

(define-public crate-qadapt-0.4 (crate (name "qadapt") (vers "0.4.0") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "qadapt-macro") (req "^0.4.0") (default-features #t) (kind 0)) (crate-dep (name "spin") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "thread-id") (req "^3.3") (default-features #t) (kind 0)))) (hash "060rrj0i0j7sib0ksci8l19p9dmp7f01anyx109pn033l7d45kja")))

(define-public crate-qadapt-0.5 (crate (name "qadapt") (vers "0.5.0") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "qadapt-macro") (req "^0.5.0") (default-features #t) (kind 0)) (crate-dep (name "spin") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "thread-id") (req "^3.3") (default-features #t) (kind 0)))) (hash "0x353z1xpcd0c9aiv0b6zjfm9a748vc7r60zgkkigvphz0x0bk98")))

(define-public crate-qadapt-0.6 (crate (name "qadapt") (vers "0.6.0") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "qadapt-macro") (req "^0.6.0") (default-features #t) (kind 0)) (crate-dep (name "spin") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "thread-id") (req "^3.3") (default-features #t) (kind 0)))) (hash "1irzd9dp4xjyz37363bzs6gxdgkifaz7cii65d7n7rd9bp4l9lrv")))

(define-public crate-qadapt-0.7 (crate (name "qadapt") (vers "0.7.0") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "qadapt-macro") (req "^0.7.0") (default-features #t) (kind 0)) (crate-dep (name "spin") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "thread-id") (req "^3.3") (default-features #t) (kind 0)))) (hash "1h3fsxbfxasf3bhd4286xjbchfyyy0641gywppljnc2mga445zrw")))

(define-public crate-qadapt-0.7 (crate (name "qadapt") (vers "0.7.1") (deps (list (crate-dep (name "env_logger") (req "^0.6") (default-features #t) (kind 2)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "qadapt-macro") (req "^0.7.1") (default-features #t) (kind 0)) (crate-dep (name "spin") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "thread-id") (req "^3.3") (default-features #t) (kind 0)))) (hash "1pzqrdydcd5rdbvja08v3fv5lazkw79xcnal3qcl4vvsd75cspm2")))

(define-public crate-qadapt-1 (crate (name "qadapt") (vers "1.0.0") (deps (list (crate-dep (name "futures") (req "^0.1") (default-features #t) (kind 2)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "qadapt-macro") (req "^1.0.0") (default-features #t) (kind 0)) (crate-dep (name "qadapt-spin") (req "^1.0.0") (default-features #t) (kind 0)) (crate-dep (name "thread-id") (req "^3.3") (default-features #t) (kind 0)))) (hash "070zshdi00i1268hh9864z72hrp003g5f1kz1xpcj1yhxvrv122a")))

(define-public crate-qadapt-1 (crate (name "qadapt") (vers "1.0.1") (deps (list (crate-dep (name "futures") (req "^0.1") (default-features #t) (kind 2)) (crate-dep (name "qadapt-macro") (req "^1.0.0") (default-features #t) (kind 0)) (crate-dep (name "qadapt-spin") (req "^1.0.0") (default-features #t) (kind 0)) (crate-dep (name "thread-id") (req "^3.3") (default-features #t) (kind 0)))) (hash "02i8gpc1plxya2h2pqp7aqx1zcj94i5sjqbl6yc2c3pm4b4f6xij")))

(define-public crate-qadapt-1 (crate (name "qadapt") (vers "1.0.2") (deps (list (crate-dep (name "futures") (req "^0.1") (default-features #t) (kind 2)) (crate-dep (name "qadapt-macro") (req "^1.0.2") (default-features #t) (kind 0)) (crate-dep (name "spin") (req "^0.5") (default-features #t) (kind 0)) (crate-dep (name "thread-id") (req "^3.3") (default-features #t) (kind 0)))) (hash "1hd0y5c0x36psanhmll4f2lxahnr4s2k63bb9v4mrwaw24a0jg48")))

(define-public crate-qadapt-1 (crate (name "qadapt") (vers "1.0.3") (deps (list (crate-dep (name "futures") (req "^0.1") (default-features #t) (kind 2)) (crate-dep (name "qadapt-macro") (req "^1.0.2") (default-features #t) (kind 0)) (crate-dep (name "spin") (req "^0.5") (default-features #t) (kind 0)) (crate-dep (name "thread-id") (req "^3.3") (default-features #t) (kind 0)))) (hash "07gvqyr8c37kwr4lfvyqsfccdsypgsq1wddrg2c4j7x3yq7bi5y5")))

(define-public crate-qadapt-1 (crate (name "qadapt") (vers "1.0.4") (deps (list (crate-dep (name "futures") (req "^0.1") (default-features #t) (kind 2)) (crate-dep (name "qadapt-macro") (req "^1.0.2") (default-features #t) (kind 0)) (crate-dep (name "spin") (req "^0.5") (default-features #t) (kind 0)) (crate-dep (name "thread-id") (req "^3.3") (default-features #t) (kind 0)))) (hash "0z5lf3myvhc5zhwdpizwlak3s151cs4kpjpmri313f5rnhlmwp4p")))

(define-public crate-qadapt-macro-0.3 (crate (name "qadapt-macro") (vers "0.3.0") (hash "0dfbd3m4nsvznzwy11ah1r5pqarrq5jn7m984dyy4mxjmislvjv6")))

(define-public crate-qadapt-macro-0.4 (crate (name "qadapt-macro") (vers "0.4.0") (hash "0v2jvfy4hd0911hmh9xi3m8r6jnpilmqb0ch4zkvy7k1hhw3m0az")))

(define-public crate-qadapt-macro-0.5 (crate (name "qadapt-macro") (vers "0.5.0") (hash "183wffyf2ndmgac13c87pi4ij5ba0kihhd5rcrp464fdkvw6707h")))

(define-public crate-qadapt-macro-0.6 (crate (name "qadapt-macro") (vers "0.6.0") (hash "1z0q9r2jjpgprjr4n181jaim56b25crw7aq7qvi0wkjnssld1jln")))

(define-public crate-qadapt-macro-0.7 (crate (name "qadapt-macro") (vers "0.7.0") (hash "0widq5qhrjbfgizzs2aazlasrjq64z9hpdvi9zijm7s0i8wzd3c6")))

(define-public crate-qadapt-macro-0.7 (crate (name "qadapt-macro") (vers "0.7.1") (hash "0r0i2dzhr5qj453w4cv1lkmwlsp4z4pnw054sgk79xwq2jsy23mr")))

(define-public crate-qadapt-macro-1 (crate (name "qadapt-macro") (vers "1.0.0") (hash "07k6xd15kzj3ypmslqnfr07kgyvddj5fakyw6v1wksnz3fzahmxz")))

(define-public crate-qadapt-macro-1 (crate (name "qadapt-macro") (vers "1.0.1") (hash "1pahny42hw11lhph8k1s5adwnqsyl06f9r1a4h6hqpgk32b01n41")))

(define-public crate-qadapt-macro-1 (crate (name "qadapt-macro") (vers "1.0.2") (hash "02pg6cwsy48713ah4q0rjdnl054npysjyxwyb6sgy98sfkqx5xa4")))

(define-public crate-qadapt-macro-1 (crate (name "qadapt-macro") (vers "1.0.3") (hash "16ywwwll652335cln5462hc747mh6v4xmj59k3jxjibb2k38dq7v")))

(define-public crate-qadapt-spin-1 (crate (name "qadapt-spin") (vers "1.0.0") (hash "1axgxsakhi8l02vi6k6krhl0cas7rjgik3dak96d6d30kwz6ydn5")))

(define-public crate-qadapt-spin-1 (crate (name "qadapt-spin") (vers "1.0.1") (hash "0vxiav7fwyz9dxv63cw1f1bra72crkxz45baqa2y9yv9hn7iqqcz")))

(define-public crate-qadata-rs-0.1 (crate (name "qadata-rs") (vers "0.1.0") (deps (list (crate-dep (name "bson") (req "^1.1.0") (default-features #t) (kind 0)) (crate-dep (name "chrono") (req "^0.4.13") (default-features #t) (kind 0)) (crate-dep (name "clickhouse-rs") (req "^0.1.21") (default-features #t) (kind 0)) (crate-dep (name "influxdb") (req "^0.3.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "mifi-rs") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "mongodb") (req "^1.1.0") (features (quote ("sync"))) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.64") (default-features #t) (kind 0)) (crate-dep (name "stopwatch") (req "^0.0.7") (default-features #t) (kind 0)))) (hash "0b8ywpwjjwh0v3f3vdw0vsdnfyqh42dpjv4iypl32ry7r30dx9kb")))

