(define-module (crates-io qa rg) #:use-module (crates-io))

(define-public crate-qargparser-0.5 (crate (name "qargparser") (vers "0.5.0") (deps (list (crate-dep (name "qpprint") (req "^0.1.1") (default-features #t) (kind 0)))) (hash "0iss8rq8vq1rj15l1hm1njacssma7yh8gpm5bzjpfl0l3bls1yz8")))

(define-public crate-qargparser-0.5 (crate (name "qargparser") (vers "0.5.2") (deps (list (crate-dep (name "qpprint") (req "^0.1.1") (default-features #t) (kind 0)))) (hash "19cjkibi4gw38764wj1pzg5fc9xq9ncsxcjwsy06p1fwsxx8mi83")))

(define-public crate-qargparser-0.5 (crate (name "qargparser") (vers "0.5.3") (deps (list (crate-dep (name "qpprint") (req "^0.1.1") (default-features #t) (kind 0)))) (hash "1fnrmyfch3l41s0yh2586fkhh3fjkyzpisc68440ii19skrj10qx")))

(define-public crate-qargparser-0.5 (crate (name "qargparser") (vers "0.5.4") (deps (list (crate-dep (name "qpprint") (req "^0.1.1") (default-features #t) (kind 0)))) (hash "0npmjgcvr2zx6avhij1y7alxadpdl9baccgp6i8a75knybvchnsd")))

(define-public crate-qargparser-0.5 (crate (name "qargparser") (vers "0.5.5") (deps (list (crate-dep (name "qpprint") (req "^0.1.1") (default-features #t) (kind 0)))) (hash "143f8l44gfbvhrpyw544vjmhshcybsd04rb2bavqdhi6v5rrg8h8")))

(define-public crate-qargparser-0.5 (crate (name "qargparser") (vers "0.5.6") (deps (list (crate-dep (name "qpprint") (req "^0.1.1") (default-features #t) (kind 0)))) (hash "1wjl4q8hrr13faprcbsfp8kxg1kmimgly9qbqqifar1g7inz69yg")))

