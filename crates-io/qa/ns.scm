(define-module (crates-io qa ns) #:use-module (crates-io))

(define-public crate-qansdk-0.1 (crate (name "qansdk") (vers "0.1.0") (deps (list (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.114") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_cbor") (req "^0.11.1") (default-features #t) (kind 0)))) (hash "0l9xp2pgpqyl7f62pywsz7lfvdq0wynfqblxzsvbyz1zk3cir5r5")))

(define-public crate-qansdk-macros-0.1 (crate (name "qansdk-macros") (vers "0.1.0") (deps (list (crate-dep (name "quote") (req "^1.0.7") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.33") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "0wsfk7v36xmyfr8mv8q6xpayqcmczwha5xn0ifnnrkhsdn60czvr")))

