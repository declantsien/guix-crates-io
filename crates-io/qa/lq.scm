(define-module (crates-io qa lq) #:use-module (crates-io))

(define-public crate-qalqulator-0.1 (crate (name "qalqulator") (vers "0.1.0") (deps (list (crate-dep (name "anyhow") (req "^1.0.65") (default-features #t) (kind 0)) (crate-dep (name "directories") (req "^4.0.1") (default-features #t) (kind 0)) (crate-dep (name "gcd") (req "^2.1.0") (default-features #t) (kind 0)) (crate-dep (name "pest") (req "^2.4.0") (default-features #t) (kind 0)) (crate-dep (name "pest_derive") (req "^2.4.0") (default-features #t) (kind 0)) (crate-dep (name "rustyline") (req "^10.0.0") (default-features #t) (kind 0)))) (hash "09wkkgxanr3g5ipk7wj9qwfpx5pvfk4ppjqvb8d7g3hzj1ydh7jr")))

