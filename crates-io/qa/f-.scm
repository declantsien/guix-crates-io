(define-module (crates-io qa f-) #:use-module (crates-io))

(define-public crate-qaf-build-utils-0.1 (crate (name "qaf-build-utils") (vers "0.1.0") (deps (list (crate-dep (name "anyhow") (req "^1.0.70") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2.0.13") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "1wrxsw9wv6w918y10b0g4clgwa5dalf3q86gxf1fwdx7wfcq12ns")))

(define-public crate-qaf-macros-0.2 (crate (name "qaf-macros") (vers "0.2.2") (hash "0d5ydq4riksjag4ylk3lvjz6ada1k2kh071iv0kgj89m7w8aif0z") (features (quote (("cloudflare") ("axum"))))))

(define-public crate-qaf-router-0.1 (crate (name "qaf-router") (vers "0.1.0") (deps (list (crate-dep (name "anyhow") (req "^1.0.70") (default-features #t) (kind 0)) (crate-dep (name "matchit") (req "^0.7.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.160") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "12y83v1pw5x2g1cqc8b9h8d9hy4x3s5s471amiljdyinpj52b1c8")))

(define-public crate-qaf-router-0.1 (crate (name "qaf-router") (vers "0.1.1") (deps (list (crate-dep (name "anyhow") (req "^1.0.70") (default-features #t) (kind 0)) (crate-dep (name "matchit") (req "^0.7.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.160") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "17nimp74khjgx19a9nc3h80wl1gc4hf4lazw8mbng5jbfprnkj02")))

