(define-module (crates-io l_ gr) #:use-module (crates-io))

(define-public crate-l_group_cnf-0.1 (crate (name "l_group_cnf") (vers "0.1.0") (deps (list (crate-dep (name "l_group_formulas") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "051kk0yxgf6xfgf4zs8xa23nx414g65lhi2x7br1mn4r9sa9zkf8")))

(define-public crate-l_group_formulas-0.1 (crate (name "l_group_formulas") (vers "0.1.0") (deps (list (crate-dep (name "criterion") (req "^0.3.3") (default-features #t) (kind 2)))) (hash "1ag763sxv4zjzfzyzinplmqhc5fdlq180f7am3m52cbph8wh84by")))

(define-public crate-l_group_validity-0.1 (crate (name "l_group_validity") (vers "0.1.0") (deps (list (crate-dep (name "l_group_cnf") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "l_group_formulas") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "truncated_free_groups") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "1a39vz272jspyxh3vd0qdkdrzrzm9a0a1dkqrriz9azf0v9847r5")))

