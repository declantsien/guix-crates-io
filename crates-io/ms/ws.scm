(define-module (crates-io ms ws) #:use-module (crates-io))

(define-public crate-msws-0.1 (crate (name "msws") (vers "0.1.0") (hash "11dvqkj32jnmp9zhswr3a9dhnlh49kw1ly7aa21h4isziz18a61v")))

(define-public crate-msws-0.2 (crate (name "msws") (vers "0.2.0") (hash "1vfdknsx106pmddqzhyllsfzhj808q9j4wizndr4hkanrygcba3d")))

