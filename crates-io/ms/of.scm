(define-module (crates-io ms of) #:use-module (crates-io))

(define-public crate-msoffice_crypt-0.1 (crate (name "msoffice_crypt") (vers "0.1.0") (hash "0kcj2lf1qwpd4qpim9a8qw70m4v40qdlr2n38j3pglqmqczp698h")))

(define-public crate-msoffice_crypt-0.1 (crate (name "msoffice_crypt") (vers "0.1.1") (hash "0qh4v8ja4wfyiq8lmh5kk1k3iipl9gdb8m513q4w790scxspr797")))

(define-public crate-msoffice_crypt-0.1 (crate (name "msoffice_crypt") (vers "0.1.2") (hash "1470bk5whxbzrd898s9qhhljwm1y5va1zcbp437rjviz4icdk7fl")))

(define-public crate-msoffice_crypt-0.1 (crate (name "msoffice_crypt") (vers "0.1.3") (hash "1vqr6lr5q0fabii5p0yqjmkqram4y6bry2ijpryfkvwifialbb4x")))

(define-public crate-msoffice_crypt-0.1 (crate (name "msoffice_crypt") (vers "0.1.4") (hash "1p1y9wxwza7wyn8wf23nm07bawhdq570bk60l3a5zzb71asyc919")))

(define-public crate-msoffice_crypt-0.1 (crate (name "msoffice_crypt") (vers "0.1.5") (hash "0xbjkv1qs4aizn4qixl68qjzbnwnn5bxairivswpffpnlcmiangp")))

(define-public crate-msoffice_pptx-0.1 (crate (name "msoffice_pptx") (vers "0.1.0") (deps (list (crate-dep (name "quick-xml") (req "^0.13.1") (default-features #t) (kind 0)) (crate-dep (name "zip") (req "^0.4.2") (default-features #t) (kind 0)))) (hash "0621i6fvlb7q42waj0mprdaywpxib742v3nq8jag8psan5rzrhcd")))

(define-public crate-msoffice_pptx-0.1 (crate (name "msoffice_pptx") (vers "0.1.1") (deps (list (crate-dep (name "log") (req "^0.4.6") (default-features #t) (kind 0)) (crate-dep (name "quick-xml") (req "^0.13.1") (default-features #t) (kind 0)) (crate-dep (name "simple_logger") (req "^1.0.1") (default-features #t) (kind 2)) (crate-dep (name "zip") (req "^0.4.2") (default-features #t) (kind 0)))) (hash "0i0f28gsp3d00cjg6bqknpl80ynpb6m19ypvwzm2cqv3rmhrirlr")))

(define-public crate-msoffice_pptx-0.2 (crate (name "msoffice_pptx") (vers "0.2.0") (deps (list (crate-dep (name "log") (req "^0.4.6") (default-features #t) (kind 0)) (crate-dep (name "msoffice_shared") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "simple_logger") (req "^1.0.1") (default-features #t) (kind 2)) (crate-dep (name "zip") (req "^0.4.2") (default-features #t) (kind 0)))) (hash "09xisv2164y4npbxqxyklwadjcrxwglscqqx885x9nlq0ighv413")))

(define-public crate-msoffice_pptx-0.2 (crate (name "msoffice_pptx") (vers "0.2.1") (deps (list (crate-dep (name "enum_from_str") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "enum_from_str_derive") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.6") (default-features #t) (kind 0)) (crate-dep (name "msoffice_shared") (req "^0.1.1") (default-features #t) (kind 0)) (crate-dep (name "simple_logger") (req "^1.2.0") (default-features #t) (kind 2)) (crate-dep (name "zip") (req "^0.5.2") (default-features #t) (kind 0)))) (hash "1ii8x2ksrwv3vdzk8srl3n9b138sy2msyfdc6wrj18gcy33ih9hl")))

(define-public crate-msoffice_shared-0.1 (crate (name "msoffice_shared") (vers "0.1.0") (deps (list (crate-dep (name "log") (req "^0.4.6") (default-features #t) (kind 0)) (crate-dep (name "quick-xml") (req "^0.13.1") (default-features #t) (kind 0)) (crate-dep (name "simple_logger") (req "^1.0.1") (default-features #t) (kind 2)) (crate-dep (name "zip") (req "^0.4.2") (default-features #t) (kind 0)))) (hash "1fa2k5ak4c81mhkxhz2f7hfal90nd63m58w55h0061kh9fipy8p0")))

(define-public crate-msoffice_shared-0.1 (crate (name "msoffice_shared") (vers "0.1.1") (deps (list (crate-dep (name "enum_from_str") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "enum_from_str_derive") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.6") (default-features #t) (kind 0)) (crate-dep (name "quick-xml") (req "^0.14.0") (default-features #t) (kind 0)) (crate-dep (name "simple_logger") (req "^1.2.0") (default-features #t) (kind 2)) (crate-dep (name "zip") (req "^0.5.2") (default-features #t) (kind 0)))) (hash "0wsiwddnwlzsrhl78wh8k6xym75ch8y0nr75avkxznxgmr7n7agz")))

