(define-module (crates-io ms u1) #:use-module (crates-io))

(define-public crate-msu1-reverser-1 (crate (name "msu1-reverser") (vers "1.0.0") (deps (list (crate-dep (name "clap") (req "^4.1.6") (features (quote ("wrap_help" "derive"))) (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)))) (hash "0kickmbj47fgdal5d1q125k4kyrlzmjq3gkbl5i06n5w2wq970fv")))

