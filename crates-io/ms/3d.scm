(define-module (crates-io ms #{3d}#) #:use-module (crates-io))

(define-public crate-ms3d-0.1 (crate (name "ms3d") (vers "0.1.0") (deps (list (crate-dep (name "bitflags") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "failure") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "memchr") (req "^2") (default-features #t) (kind 0)))) (hash "1w0wclg9mh9rcnz1s3hv82i3w2rasl4ig7ysdfky64cm9k3qvbpk")))

(define-public crate-ms3d-0.1 (crate (name "ms3d") (vers "0.1.1") (deps (list (crate-dep (name "bitflags") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "failure") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "memchr") (req "^2") (default-features #t) (kind 0)))) (hash "03bbb5qmh5qh3zj1p177nq1rczbsax4kl05cjlyldglh2hjypad5")))

(define-public crate-ms3d-0.1 (crate (name "ms3d") (vers "0.1.2") (deps (list (crate-dep (name "bitflags") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "failure") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "memchr") (req "^2") (default-features #t) (kind 0)))) (hash "0y2fc1b3gxknjy4hw60264lc4nxasjmwh98w9pg47kdnh47jkng0")))

(define-public crate-ms3d-0.1 (crate (name "ms3d") (vers "0.1.3") (deps (list (crate-dep (name "bitflags") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "failure") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "memchr") (req "^2") (default-features #t) (kind 0)))) (hash "1bl4bqhi3y3zsbdab96mnfvdg9mzf4aqjxkv9cz41i18iggd1gpa")))

