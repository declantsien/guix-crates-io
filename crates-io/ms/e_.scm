(define-module (crates-io ms e_) #:use-module (crates-io))

(define-public crate-mse_fmp4-0.1 (crate (name "mse_fmp4") (vers "0.1.0") (deps (list (crate-dep (name "byteorder") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^2") (default-features #t) (kind 2)) (crate-dep (name "mpeg2ts") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "trackable") (req "^0.2") (default-features #t) (kind 0)))) (hash "04scasjc6wxnwxhapiw1msi9h6xwqf58rca0x55j5vaf9gmycqry")))

(define-public crate-mse_fmp4-0.1 (crate (name "mse_fmp4") (vers "0.1.1") (deps (list (crate-dep (name "byteorder") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^2") (default-features #t) (kind 2)) (crate-dep (name "mpeg2ts") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "trackable") (req "^0.2") (default-features #t) (kind 0)))) (hash "05xqkaiswak4xhq5b93gb20yfkcdy2mk97ci8ksigxk2wabkp5i7")))

(define-public crate-mse_fmp4-0.1 (crate (name "mse_fmp4") (vers "0.1.2") (deps (list (crate-dep (name "byteorder") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^2") (default-features #t) (kind 2)) (crate-dep (name "mpeg2ts") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "trackable") (req "^0.2") (default-features #t) (kind 0)))) (hash "03p6jgdyfgs7bxgyv0dm7zlyzxzdl8yv3v4a6i45ypmkjawbimr7")))

(define-public crate-mse_fmp4-0.1 (crate (name "mse_fmp4") (vers "0.1.3") (deps (list (crate-dep (name "byteorder") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^2") (default-features #t) (kind 2)) (crate-dep (name "mpeg2ts") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "trackable") (req "^0.2") (default-features #t) (kind 0)))) (hash "1d7gh8xks5qb872ny4gkzyxdlha6s8l5xq9f51bj9jc0dxw0dak0")))

(define-public crate-mse_fmp4-0.2 (crate (name "mse_fmp4") (vers "0.2.0") (deps (list (crate-dep (name "byteorder") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^2") (default-features #t) (kind 2)) (crate-dep (name "mpeg2ts") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "trackable") (req "^0.2") (default-features #t) (kind 0)))) (hash "0ma4q4z74c7r19f98xvz9m6vjvyc1cd3dj360mvisvcb14sgpril")))

