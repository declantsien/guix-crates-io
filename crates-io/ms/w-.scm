(define-module (crates-io ms w-) #:use-module (crates-io))

(define-public crate-msw-hotkey-0.1 (crate (name "msw-hotkey") (vers "0.1.0") (hash "1f6yhabsd13dvdwfvws7a64sic0h8fiz6bqkb6zmdpb6w93c9whc") (yanked #t)))

(define-public crate-msw-hotkey-0.1 (crate (name "msw-hotkey") (vers "0.1.1") (hash "0lrl7lwcdssndxgl0jk2gscwzgwvy2cl2b635sc9rcsr2f0bq9d3")))

(define-public crate-msw-hotkey-0.2 (crate (name "msw-hotkey") (vers "0.2.0") (hash "00y0r252lxdx588bhp93cqxai8jjx6s09xm1kdvlijkbnxbgs9xf")))

(define-public crate-msw-hotkey-0.2 (crate (name "msw-hotkey") (vers "0.2.1") (hash "1rzzk20ikz2wyimhkw67wdw5yxg9pq017psl5kndwm5w9azjq31h")))

