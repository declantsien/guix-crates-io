(define-module (crates-io ms dk) #:use-module (crates-io))

(define-public crate-msdk_adapter-0.1 (crate (name "msdk_adapter") (vers "0.1.0") (deps (list (crate-dep (name "anyhow") (req "^1.0.81") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2.153") (default-features #t) (kind 0)))) (hash "11qp8bfv8wmflhky8njx5xky8bwvnag3jfwq57bbw41fdrgsylhj") (yanked #t)))

(define-public crate-msdk_adapter-0.1 (crate (name "msdk_adapter") (vers "0.1.1") (deps (list (crate-dep (name "anyhow") (req "^1.0.81") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2.153") (default-features #t) (kind 0)) (crate-dep (name "libloading") (req "^0.8.3") (default-features #t) (kind 0)))) (hash "18gmdi9jyxik776k2qaxv2cfw9azl7sy84vp0xc7325ll7z4jjz3") (yanked #t)))

(define-public crate-msdk_adapter-0.1 (crate (name "msdk_adapter") (vers "0.1.2") (deps (list (crate-dep (name "anyhow") (req "^1.0.81") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2.153") (default-features #t) (kind 0)) (crate-dep (name "libloading") (req "^0.8.3") (default-features #t) (kind 0)))) (hash "17f481syivm3az9pn4hpfl6ar5wx7c6fnlnsc1hiybn8xvv0dgn8")))

