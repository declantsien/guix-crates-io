(define-module (crates-io ms _s) #:use-module (crates-io))

(define-public crate-ms_samr-0.0.1 (crate (name "ms_samr") (vers "0.0.1") (deps (list (crate-dep (name "ms_dtyp") (req "^0.0") (default-features #t) (kind 0)))) (hash "1kj1pyzip17zhg9ik24lqmw4iyw9wnms24xm4yszsa2y98lvrhvv")))

(define-public crate-ms_samr-0.0.2 (crate (name "ms_samr") (vers "0.0.2") (deps (list (crate-dep (name "ms_dtyp") (req "^0.0") (default-features #t) (kind 0)))) (hash "06d7gkcikap67319wjr9jbjkj4ckvk3a60r3d6b1dxxqqn08c54f")))

(define-public crate-ms_speech_sys-0.1 (crate (name "ms_speech_sys") (vers "0.1.0") (deps (list (crate-dep (name "bindgen") (req "^0.49.3") (default-features #t) (kind 1)))) (hash "1h5aga8rlax0sznhbpzbgfqn9y2633dhafb81x15sza9dsikn090")))

(define-public crate-ms_speech_sys-0.1 (crate (name "ms_speech_sys") (vers "0.1.1") (deps (list (crate-dep (name "bindgen") (req "^0.50.0") (default-features #t) (kind 1)))) (hash "0bvp4hr2s1pak6w5zpri2sf34gcrnrgmwni730km9pzzcxwnhcxk")))

(define-public crate-ms_speech_sys-0.1 (crate (name "ms_speech_sys") (vers "0.1.2") (deps (list (crate-dep (name "bindgen") (req "^0.50.0") (default-features #t) (kind 1)))) (hash "0xrlbkvhbsz84lpfzw9g0ij0dy8x2y4qp0ad56gnydlgid3xp3nm")))

(define-public crate-ms_speech_sys-0.1 (crate (name "ms_speech_sys") (vers "0.1.3") (deps (list (crate-dep (name "bindgen") (req "^0.50.0") (default-features #t) (kind 1)))) (hash "01s7mlwcm60cf8hpsg9hcqyw59mmkspv7aw1xfyp5bjjpmpwfnpc")))

(define-public crate-ms_speech_sys-0.2 (crate (name "ms_speech_sys") (vers "0.2.0") (deps (list (crate-dep (name "bindgen") (req "^0.50.0") (default-features #t) (kind 1)))) (hash "07x29shh68jx163jawpibr2vahqpm1y2dx2vmjj51z138m8rn77r")))

(define-public crate-ms_speech_sys-0.2 (crate (name "ms_speech_sys") (vers "0.2.1") (deps (list (crate-dep (name "bindgen") (req "^0.50.0") (default-features #t) (kind 1)))) (hash "1l3af5diirn9wzkd97qghz2xlb363m2bvwbnxpas60dzgvypxrj7")))

