(define-module (crates-io ms sh) #:use-module (crates-io))

(define-public crate-mssh-0.0.0 (crate (name "mssh") (vers "0.0.0") (deps (list (crate-dep (name "insta") (req "^1.28.0") (default-features #t) (kind 2)))) (hash "1snd7pl8b0nvm2zrbqfsa8260f6i7p5sgfs9cmx6ygpbcm9kclrq")))

