(define-module (crates-io ms #{58}#) #:use-module (crates-io))

(define-public crate-ms5837-0.1 (crate (name "ms5837") (vers "0.1.0") (deps (list (crate-dep (name "cc") (req "^1.0") (default-features #t) (kind 1)) (crate-dep (name "embedded-hal") (req "^0.2.7") (default-features #t) (kind 0)) (crate-dep (name "embedded-hal-mock") (req "^0.8.0") (default-features #t) (kind 2)))) (hash "1fhj2hnm4dl36y8rwapzi5pqrfsfsxp0vprqzqq04nxhjx8m98ha")))

(define-public crate-ms5837-0.1 (crate (name "ms5837") (vers "0.1.1") (deps (list (crate-dep (name "cc") (req "^1.0") (default-features #t) (kind 1)) (crate-dep (name "embedded-hal") (req "^0.2.7") (default-features #t) (kind 0)) (crate-dep (name "embedded-hal-mock") (req "^0.8.0") (default-features #t) (kind 2)))) (hash "0y4hf4105bfvqnpa8qr41yvsihxl18gj27b07mpskpkijfp9fsdi")))

(define-public crate-ms5837-0.1 (crate (name "ms5837") (vers "0.1.2") (deps (list (crate-dep (name "cc") (req "^1.0") (default-features #t) (kind 1)) (crate-dep (name "embedded-hal") (req "^0.2.7") (default-features #t) (kind 0)) (crate-dep (name "embedded-hal-mock") (req "^0.8.0") (default-features #t) (kind 2)))) (hash "1rz8rmd1byjjlq7pcjk1kq0cbqwv8zgicqv25wxlcybz9zb5mnfq")))

(define-public crate-ms5837-0.1 (crate (name "ms5837") (vers "0.1.3") (deps (list (crate-dep (name "cc") (req "^1.0") (default-features #t) (kind 1)) (crate-dep (name "embedded-hal") (req "^0.2.7") (default-features #t) (kind 0)) (crate-dep (name "embedded-hal-mock") (req "^0.8.0") (default-features #t) (kind 2)))) (hash "0cry1lsqj1j6f9n7ixp3jhf38xvzkbxk4sq89xcnmx6vaddiaq9i")))

(define-public crate-ms5837-0.2 (crate (name "ms5837") (vers "0.2.1") (deps (list (crate-dep (name "cc") (req "^1.0") (default-features #t) (kind 1)) (crate-dep (name "embedded-hal") (req "^0.2.7") (default-features #t) (kind 0)) (crate-dep (name "embedded-hal-mock") (req "^0.8.0") (default-features #t) (kind 2)))) (hash "029cillwilzzcc8n3zznjhc8q1wkfrcsf5c19h8558b1mccqdhn9")))

