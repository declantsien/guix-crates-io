(define-module (crates-io ms c-) #:use-module (crates-io))

(define-public crate-msc-weather-0.6 (crate (name "msc-weather") (vers "0.6.2") (hash "1xwibqn0nnk6d3hrg7lm88kh2xd3jh51c29sx8cam9f1hnk1dp3y")))

(define-public crate-msc-weather-0.6 (crate (name "msc-weather") (vers "0.6.3") (hash "0mffcr5cfq5j3crzlr4rw1sz1bpfzfgd7kdy0ykxpp5alsyq970x")))

