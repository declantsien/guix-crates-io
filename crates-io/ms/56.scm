(define-module (crates-io ms #{56}#) #:use-module (crates-io))

(define-public crate-ms5611-1 (crate (name "ms5611") (vers "1.0.0") (deps (list (crate-dep (name "byteorder") (req "^1.1.0") (default-features #t) (kind 0)) (crate-dep (name "i2cdev") (req "^0.3.1") (default-features #t) (kind 0)))) (hash "1l07clrmiwqnyr8h1a13qlhffk7ybnqczishhrxb8a6j940kjjww")))

(define-public crate-ms5611-spi-0.1 (crate (name "ms5611-spi") (vers "0.1.0") (deps (list (crate-dep (name "embedded-hal") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "embedded-hal-mock") (req "^0.5") (default-features #t) (kind 2)) (crate-dep (name "linux-embedded-hal") (req "^0.2.2") (default-features #t) (kind 2)))) (hash "1fhkzz1f62h2nh78wr1hxjk1zfvn702h8z1s02xxczh336aqzrfy")))

(define-public crate-ms5611-spi-0.2 (crate (name "ms5611-spi") (vers "0.2.0") (deps (list (crate-dep (name "embedded-hal") (req "^0.2.3") (default-features #t) (kind 0)) (crate-dep (name "embedded-hal-mock") (req "^0.5") (default-features #t) (kind 2)) (crate-dep (name "linux-embedded-hal") (req "^0.2.2") (default-features #t) (kind 2)))) (hash "15wxqf365iyspwjlbgbz1grb5sv4z53m4md2vnnryxz47pgpqfg4") (features (quote (("ms5611") ("ms5607") ("default" "ms5611"))))))

(define-public crate-ms5637-0.1 (crate (name "ms5637") (vers "0.1.0") (deps (list (crate-dep (name "embedded-hal") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "libm") (req "^0.2.0") (optional #t) (default-features #t) (kind 0)))) (hash "0ib73v7i2pfk8m9gry46961q9lwrpkzxzzvcznw73rznqhm03ciz") (features (quote (("second-order")))) (v 2) (features2 (quote (("altitude-adjust" "dep:libm")))) (rust-version "1.60")))

