(define-module (crates-io ms -l) #:use-module (crates-io))

(define-public crate-ms-learn-md-parser-0.0.1 (crate (name "ms-learn-md-parser") (vers "0.0.1") (deps (list (crate-dep (name "deno_core") (req "^0.175.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.7.1") (default-features #t) (kind 0)) (crate-dep (name "scraper") (req "^0.15.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.157") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_yaml") (req "^0.9.19") (default-features #t) (kind 0)))) (hash "0na5dfr237r9wkbn0j01113iyqjzj12wqmhadf5sdrpnpy5izmrr")))

