(define-module (crates-io ms f6) #:use-module (crates-io))

(define-public crate-msf60_utils-0.3 (crate (name "msf60_utils") (vers "0.3.1") (deps (list (crate-dep (name "radio_datetime_utils") (req "^0.5.0") (default-features #t) (kind 0)))) (hash "0aw67gl2l04mbyaf0jq15qkdc875pp0aarm8a4qcsr65v0hqc7rr")))

(define-public crate-msf60_utils-0.3 (crate (name "msf60_utils") (vers "0.3.2") (deps (list (crate-dep (name "radio_datetime_utils") (req "^0.5.0") (default-features #t) (kind 0)))) (hash "0vza8pzk5nys1vbmq0pqn89dv9rv1zrqb4wscb8g45gmxlpcgnaj")))

(define-public crate-msf60_utils-0.4 (crate (name "msf60_utils") (vers "0.4.0") (deps (list (crate-dep (name "radio_datetime_utils") (req "^0.5") (default-features #t) (kind 0)))) (hash "0l1wgvq2bnkl5w8b68wvm5ni9cr7jnpmy5a6wxf4vvzfjrnj75zb")))

