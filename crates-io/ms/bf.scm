(define-module (crates-io ms bf) #:use-module (crates-io))

(define-public crate-msbfinal_db-0.1 (crate (name "msbfinal_db") (vers "0.1.0") (deps (list (crate-dep (name "assert_approx_eq") (req "^1.1.0") (default-features #t) (kind 2)) (crate-dep (name "bincode") (req "^1.3.3") (default-features #t) (kind 0)) (crate-dep (name "float-cmp") (req "^0.8.0") (default-features #t) (kind 2)) (crate-dep (name "pretty_assertions") (req "^0.7") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.8.3") (default-features #t) (kind 2)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "1qfg3i6h6fww28582pc6p3p7884iqndp4lf23i03g8p70i0lrnbn")))

