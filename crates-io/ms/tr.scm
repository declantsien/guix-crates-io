(define-module (crates-io ms tr) #:use-module (crates-io))

(define-public crate-mstr-0.1 (crate (name "mstr") (vers "0.1.0") (deps (list (crate-dep (name "serde") (req "^1") (features (quote ("alloc"))) (optional #t) (kind 0)))) (hash "1fzkpap0b1vw3a31619pkh95w7i7p69wg9sa1lmj19macfyk0gy6") (features (quote (("default")))) (yanked #t) (v 2) (features2 (quote (("serde" "dep:serde")))) (rust-version "1.65")))

(define-public crate-mstr-0.1 (crate (name "mstr") (vers "0.1.1") (deps (list (crate-dep (name "serde") (req "^1") (features (quote ("alloc"))) (optional #t) (kind 0)) (crate-dep (name "serde_json") (req "^1") (default-features #t) (kind 2)) (crate-dep (name "serde_test") (req "^1") (default-features #t) (kind 2)))) (hash "16vazm7pdz68qsx0gavqwgljsh2hz6kg2sqh7gg14pnz8ayqcg1p") (features (quote (("default")))) (v 2) (features2 (quote (("serde" "dep:serde")))) (rust-version "1.68")))

(define-public crate-mstr-0.1 (crate (name "mstr") (vers "0.1.2") (deps (list (crate-dep (name "serde") (req "^1") (features (quote ("alloc"))) (optional #t) (kind 0)) (crate-dep (name "serde_json") (req "^1") (default-features #t) (kind 2)) (crate-dep (name "serde_test") (req "^1") (default-features #t) (kind 2)))) (hash "02730mgh1v4302ajfb27d38ffj47aiymgz646zfgjw3hmh9nwkqd") (features (quote (("default")))) (v 2) (features2 (quote (("serde" "dep:serde")))) (rust-version "1.68")))

(define-public crate-mstr-0.1 (crate (name "mstr") (vers "0.1.3") (deps (list (crate-dep (name "serde") (req "^1") (features (quote ("alloc"))) (optional #t) (kind 0)) (crate-dep (name "serde_json") (req "^1") (default-features #t) (kind 2)) (crate-dep (name "serde_test") (req "^1") (default-features #t) (kind 2)))) (hash "0511739f2psyml2mkk4xv9i4a6sl6kq0lm18vrhxzp5jgvf20wmy") (features (quote (("default")))) (v 2) (features2 (quote (("serde" "dep:serde")))) (rust-version "1.68")))

(define-public crate-mstr-0.1 (crate (name "mstr") (vers "0.1.4") (deps (list (crate-dep (name "serde") (req "^1") (features (quote ("alloc"))) (optional #t) (kind 0)) (crate-dep (name "serde_json") (req "^1") (default-features #t) (kind 2)) (crate-dep (name "serde_test") (req "^1") (default-features #t) (kind 2)))) (hash "0hwwa4yr0gqshrq5pbvkfy3wxg84d758dnbkr7zr2xhhjairrh9y") (features (quote (("default")))) (v 2) (features2 (quote (("serde" "dep:serde")))) (rust-version "1.68")))

