(define-module (crates-io ms _p) #:use-module (crates-io))

(define-public crate-ms_pac-0.0.1 (crate (name "ms_pac") (vers "0.0.1") (deps (list (crate-dep (name "ms_adts") (req "^0.0") (default-features #t) (kind 0)) (crate-dep (name "ms_dtyp") (req "^0.0") (default-features #t) (kind 0)) (crate-dep (name "ms_samr") (req "^0.0") (default-features #t) (kind 2)))) (hash "1flzsq016dychzysl4wq4s7bvvpwz03a0z0n977hsxh51087k82g")))

(define-public crate-ms_pac-0.0.2 (crate (name "ms_pac") (vers "0.0.2") (deps (list (crate-dep (name "ms_adts") (req "^0.0") (default-features #t) (kind 0)) (crate-dep (name "ms_dtyp") (req "^0.0") (default-features #t) (kind 0)) (crate-dep (name "ms_samr") (req "^0.0") (default-features #t) (kind 2)))) (hash "0vadcvfvv53jgix0jsqk2cmhcyxyzwna3k78pk41kqh89y2aan3f")))

(define-public crate-ms_pac-0.0.3 (crate (name "ms_pac") (vers "0.0.3") (deps (list (crate-dep (name "ms_adts") (req "^0.0") (default-features #t) (kind 0)) (crate-dep (name "ms_dtyp") (req "^0.0") (default-features #t) (kind 0)) (crate-dep (name "ms_samr") (req "^0.0") (default-features #t) (kind 2)))) (hash "1q4ix157q9902vpikswf7rgbs9yn48ng0vvmcw29adznmfsc2l8x")))

(define-public crate-ms_pac-0.0.4 (crate (name "ms_pac") (vers "0.0.4") (deps (list (crate-dep (name "ms_adts") (req "^0.0") (default-features #t) (kind 0)) (crate-dep (name "ms_dtyp") (req "^0.0") (default-features #t) (kind 0)) (crate-dep (name "ms_samr") (req "^0.0") (default-features #t) (kind 2)))) (hash "15yj66i4hkf5cs7y7ics2blj9fby7ry4drl1icy8yphj3n72rnkl")))

