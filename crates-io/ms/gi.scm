(define-module (crates-io ms gi) #:use-module (crates-io))

(define-public crate-msgio-0.1 (crate (name "msgio") (vers "0.1.0") (deps (list (crate-dep (name "bytes") (req "^0.4.4") (default-features #t) (kind 0)) (crate-dep (name "futures") (req "^0.1.14") (default-features #t) (kind 0)) (crate-dep (name "tokio-io") (req "^0.1.2") (default-features #t) (kind 0)) (crate-dep (name "varmint") (req "^0.1.2") (default-features #t) (kind 0)))) (hash "0p2dya6mxaqb2ffk1b8wn3f4l2r234slnm2q4734l03kspwdgfqp")))

(define-public crate-msgio-0.1 (crate (name "msgio") (vers "0.1.1") (deps (list (crate-dep (name "bytes") (req "^0.4.4") (default-features #t) (kind 0)) (crate-dep (name "futures") (req "^0.1.14") (default-features #t) (kind 0)) (crate-dep (name "tokio-io") (req "^0.1.2") (default-features #t) (kind 0)) (crate-dep (name "varmint") (req "^0.1.2") (default-features #t) (kind 0)))) (hash "1rij885409hw4qjlcxmsfhnasr5x8m7a7kzxv0sxryqccw5qgd77")))

(define-public crate-msgio-0.1 (crate (name "msgio") (vers "0.1.2") (deps (list (crate-dep (name "bytes") (req "^0.4.4") (default-features #t) (kind 0)) (crate-dep (name "futures") (req "^0.1.14") (default-features #t) (kind 0)) (crate-dep (name "tokio-io") (req "^0.1.2") (default-features #t) (kind 0)) (crate-dep (name "varmint") (req "^0.1.2") (default-features #t) (kind 0)))) (hash "0nmv8kkb7l15ykxndzldg1zf5js4xf67z1a09m2x9xr9mpcxaici")))

