(define-module (crates-io ms hp) #:use-module (crates-io))

(define-public crate-mshp-0.1 (crate (name "mshp") (vers "0.1.0") (deps (list (crate-dep (name "envy") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "git2") (req "^0.13") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "yansi") (req "^0.5") (default-features #t) (kind 0)))) (hash "1476088rs6g04wzmapgwl2gzgbm1qy0292253k38vh2hcl3s99r8")))

(define-public crate-mshp-0.2 (crate (name "mshp") (vers "0.2.0") (deps (list (crate-dep (name "envy") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "git2") (req "^0.13") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "yansi") (req "^0.5") (default-features #t) (kind 0)))) (hash "0p24gxm69jznb6vv20vrf4sqhh0ri7pip97vx80hm7kwdcyz79d3")))

