(define-module (crates-io ms -s) #:use-module (crates-io))

(define-public crate-ms-sh-0.0.0 (crate (name "ms-sh") (vers "0.0.0") (deps (list (crate-dep (name "insta") (req "^1.28.0") (default-features #t) (kind 2)))) (hash "1w92mq2bdba0yrlxf0l6nb4vvwk00j6322ya4ggjv6hv2ir47zlj")))

(define-public crate-ms-symfetch-0.1 (crate (name "ms-symfetch") (vers "0.1.0") (deps (list (crate-dep (name "cargo-deny") (req "^0.14.3") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^4.4.7") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "ureq") (req "^2.8.0") (default-features #t) (kind 0)))) (hash "0273js146a2wbld0xghzaql76rnxhmslp4mfvfgh7ss168k9q3h7")))

(define-public crate-ms-symfetch-0.1 (crate (name "ms-symfetch") (vers "0.1.1") (deps (list (crate-dep (name "clap") (req "^4.4.7") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "ureq") (req "^2.8.0") (default-features #t) (kind 0)))) (hash "0v7hb85pwdf2y27ij7qbf2xins9pacs219p0pdpqll47ddwy12kb")))

(define-public crate-ms-symfetch-0.1 (crate (name "ms-symfetch") (vers "0.1.2") (deps (list (crate-dep (name "clap") (req "^4.4.7") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "ureq") (req "^2.8.0") (default-features #t) (kind 0)))) (hash "12k8pprksvk8p98zxx6lsffsf764n6ripfxaik5n2aknqm647c5n")))

(define-public crate-ms-symfetch-0.1 (crate (name "ms-symfetch") (vers "0.1.3") (deps (list (crate-dep (name "clap") (req "^4.4.7") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "ureq") (req "^2.8.0") (default-features #t) (kind 0)))) (hash "0vklfl6zqafbwxb44bv03wvs83jf1a74n3wlz1b2zrw9lr3991g9")))

(define-public crate-ms-symfetch-0.1 (crate (name "ms-symfetch") (vers "0.1.4") (deps (list (crate-dep (name "clap") (req "^4.4.7") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "ureq") (req "^2.8.0") (default-features #t) (kind 0)))) (hash "0g7aagyhjhpj90dman3x2yxl8w8wzsj096zd60c3bib4w37b9jx3")))

(define-public crate-ms-symfetch-0.1 (crate (name "ms-symfetch") (vers "0.1.5") (deps (list (crate-dep (name "clap") (req "^4.4.7") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "ureq") (req "^2.8.0") (default-features #t) (kind 0)))) (hash "06hgmcwb0yyi7sfh6ns6wdn45nja8z6cv4swhb1ihghbx57r2a87")))

