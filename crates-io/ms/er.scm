(define-module (crates-io ms er) #:use-module (crates-io))

(define-public crate-mser-1 (crate (name "mser") (vers "1.0.0") (deps (list (crate-dep (name "itoa") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "ryu") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "uuid") (req "^1") (kind 0)))) (hash "0jc2026f256yyscp1frbc977scv6c09mvf4ij1ax23i1msw64mq7") (yanked #t)))

(define-public crate-mser-1 (crate (name "mser") (vers "1.0.2") (deps (list (crate-dep (name "itoa") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "ryu") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "uuid") (req "^1") (kind 0)))) (hash "1hwii89jqqrgdwa66l8hizzb4537rzpgvk50izwk3yaf125zl7j5") (yanked #t)))

(define-public crate-mser-1 (crate (name "mser") (vers "1.0.3") (deps (list (crate-dep (name "itoa") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "ryu") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "uuid") (req "^1") (kind 0)))) (hash "0zjc6c4ldpq02cy8lm1b98zfj121f9hzqbxqg6yxzn6if5s4fmak") (yanked #t)))

(define-public crate-mser-1 (crate (name "mser") (vers "1.0.4") (deps (list (crate-dep (name "itoa") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "ryu") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "uuid") (req "^1") (kind 0)))) (hash "0bjxgld8ya0iq2limdwibg8vnsys01b3j54q3mb8zkyvdl7m7bfa") (yanked #t)))

(define-public crate-mser-1 (crate (name "mser") (vers "1.0.5") (deps (list (crate-dep (name "itoa") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "ryu") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "uuid") (req "^1") (kind 0)))) (hash "1hhbhvkvnakllv6s7vdvd0bv757ishm3a7v8j68phm5qwwlja1k8") (yanked #t)))

(define-public crate-mser-1 (crate (name "mser") (vers "1.1.0") (deps (list (crate-dep (name "itoa") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "ryu") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "uuid") (req "^1") (kind 0)))) (hash "0kjmk6x2g0152f09iaibngxyv66myd2zdr1yn8m2plhrmn564ryq") (yanked #t)))

(define-public crate-mser-1 (crate (name "mser") (vers "1.1.1") (deps (list (crate-dep (name "itoa") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "ryu") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "uuid") (req "^1") (kind 0)))) (hash "0ky79p0vsrl6xjl04sz0q96zh0f4xwiv6ssaqc7xsv2bcp4253sw")))

(define-public crate-mser-1 (crate (name "mser") (vers "1.1.2") (deps (list (crate-dep (name "itoa") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "ryu") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "uuid") (req "^1") (kind 0)))) (hash "11hlmyrjq5v54fwf495wmbdslqjr40hvs0frvf2fbzf5mgz4n1y2")))

(define-public crate-mser-1 (crate (name "mser") (vers "1.1.3") (deps (list (crate-dep (name "itoa") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "ryu") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "uuid") (req "^1") (kind 0)))) (hash "17gi9399xdv41d0b6lpcy7402xryys3k5vfxzynqpim1b4zgn9ay")))

(define-public crate-mser_macro-1 (crate (name "mser_macro") (vers "1.0.0") (deps (list (crate-dep (name "proc-macro2") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2") (features (quote ("parsing"))) (default-features #t) (kind 0)))) (hash "002svh7d82jafc6niccddnvv9njdx0v69m3yjdqsc5cmrvajadjs") (yanked #t)))

(define-public crate-mser_macro-1 (crate (name "mser_macro") (vers "1.0.1") (deps (list (crate-dep (name "proc-macro2") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2") (features (quote ("parsing"))) (default-features #t) (kind 0)))) (hash "0r4xd5kx4zgxj451n6yj4ic0450y0cjmxrn0agwrx6mqh3ikfv2j") (yanked #t)))

(define-public crate-mser_macro-1 (crate (name "mser_macro") (vers "1.0.2") (deps (list (crate-dep (name "proc-macro2") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2") (features (quote ("parsing"))) (default-features #t) (kind 0)))) (hash "08zassya54jqb41p9s5vll0m166p8r1xlwc7wqzz30axvfx2dz6j")))

(define-public crate-mser_macro-1 (crate (name "mser_macro") (vers "1.0.3") (deps (list (crate-dep (name "proc-macro2") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2") (features (quote ("parsing"))) (default-features #t) (kind 0)))) (hash "0whx8k4dhzbv58a320fix1hxs6fpf9f0qzlzgr707k6xkki9h9wf")))

(define-public crate-mser_macro-1 (crate (name "mser_macro") (vers "1.0.4") (deps (list (crate-dep (name "proc-macro2") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2") (features (quote ("parsing"))) (default-features #t) (kind 0)))) (hash "039qzkwi7p2ki41hcfpkpdnbbdbssiv7p52wscsilscq1zqc2sj4") (yanked #t)))

(define-public crate-mser_macro-1 (crate (name "mser_macro") (vers "1.0.5") (deps (list (crate-dep (name "proc-macro2") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2") (features (quote ("parsing"))) (default-features #t) (kind 0)))) (hash "1nimrbdnzcarpn76i9gli0z2x74dh9y73dmck8ilz6yrqbxk277n") (yanked #t)))

(define-public crate-mser_macro-1 (crate (name "mser_macro") (vers "1.0.6") (deps (list (crate-dep (name "proc-macro2") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2") (features (quote ("parsing"))) (default-features #t) (kind 0)))) (hash "0scrb44zzd8bpbp7pzl79izn2bsql82g8k61cn4slni9f2jn8fjp") (yanked #t)))

(define-public crate-mser_macro-1 (crate (name "mser_macro") (vers "1.0.7") (deps (list (crate-dep (name "proc-macro2") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2") (features (quote ("parsing"))) (default-features #t) (kind 0)))) (hash "1p756gbgrj7iywjpcnkdgx9yzc528mbjr53mshnfx1n0c8yk9729")))

(define-public crate-mser_macro-1 (crate (name "mser_macro") (vers "1.0.8") (deps (list (crate-dep (name "proc-macro2") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "0h9lvmh93c78gc69fwgmarprx29pnak2l8kivbbpvzjqkbgzl236")))

