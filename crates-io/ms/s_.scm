(define-module (crates-io ms s_) #:use-module (crates-io))

(define-public crate-mss_saliency-1 (crate (name "mss_saliency") (vers "1.0.0") (deps (list (crate-dep (name "imgref") (req "^1.0.0") (default-features #t) (kind 0)))) (hash "0q2a0c2i6xh1qh94dcb61kb383lv5wy3vndr158y99vpckmfbfr5") (yanked #t)))

(define-public crate-mss_saliency-1 (crate (name "mss_saliency") (vers "1.0.1") (deps (list (crate-dep (name "imgref") (req "^1.0.0") (default-features #t) (kind 0)) (crate-dep (name "lodepng") (req "^1.1.1") (default-features #t) (kind 2)))) (hash "12jh8n79iazirmgkb75zf7x4g7d1yxbwgmxax0387g50qvbb2kkc") (yanked #t)))

(define-public crate-mss_saliency-1 (crate (name "mss_saliency") (vers "1.0.2") (deps (list (crate-dep (name "imgref") (req "^1.2.0") (default-features #t) (kind 0)) (crate-dep (name "lodepng") (req "^1.1.3") (default-features #t) (kind 2)))) (hash "1gzzf4d78s20n56n2dbk20ybsfz61iqqcd9rn0wq0bwjiidm0d1v") (yanked #t)))

(define-public crate-mss_saliency-1 (crate (name "mss_saliency") (vers "1.0.3") (deps (list (crate-dep (name "imgref") (req "^1.2.0") (default-features #t) (kind 0)) (crate-dep (name "lodepng") (req "^2.0.0") (default-features #t) (kind 2)))) (hash "08k3i1zdfw9a2b82wl5c1az8zan2y7967w9f4hb43xh8zz8v5jva") (yanked #t)))

(define-public crate-mss_saliency-1 (crate (name "mss_saliency") (vers "1.0.4") (deps (list (crate-dep (name "imgref") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "lodepng") (req "^2.5.0") (default-features #t) (kind 2)))) (hash "0vdkc1363b8g27cg098bn9ymxybxwsmqr85s0jpq1d4fzx6292i0") (yanked #t)))

(define-public crate-mss_saliency-1 (crate (name "mss_saliency") (vers "1.0.5") (deps (list (crate-dep (name "imgref") (req "^1.6.1") (default-features #t) (kind 0)) (crate-dep (name "lodepng") (req "^3.0.0") (default-features #t) (kind 2)))) (hash "1050naygc2cv2zd0b5fym6lanvmi65nq8b04xxfpbfk0xl0ywz6z")))

(define-public crate-mss_saliency-1 (crate (name "mss_saliency") (vers "1.0.6") (deps (list (crate-dep (name "imgref") (req "^1.7.0") (default-features #t) (kind 0)) (crate-dep (name "lodepng") (req "^3.2.1") (default-features #t) (kind 2)))) (hash "1snfrclhrmhww2kn6fvk98a06wm7zxv7m6l76hhyg3pq6pzhx3wn")))

(define-public crate-mss_saliency-1 (crate (name "mss_saliency") (vers "1.1.0") (deps (list (crate-dep (name "imgref") (req "^1.9") (default-features #t) (kind 0)) (crate-dep (name "lodepng") (req "^3.7") (default-features #t) (kind 2)) (crate-dep (name "rgb") (req "^0.8.34") (kind 0)) (crate-dep (name "summed-area") (req "^1.0") (default-features #t) (kind 0)))) (hash "1vhwx7028rjra0fqaznpbnr3ar57a7g7kh054088d9a5rv5a8cdv")))

