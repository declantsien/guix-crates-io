(define-module (crates-io ms x-) #:use-module (crates-io))

(define-public crate-msx-mcp-0.4 (crate (name "msx-mcp") (vers "0.4.1") (deps (list (crate-dep (name "byteorder") (req "^1.3.1") (default-features #t) (kind 0)) (crate-dep (name "docopt") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quickcheck") (req "^0.8") (default-features #t) (kind 2)) (crate-dep (name "tempdir") (req "^0.3") (default-features #t) (kind 2)))) (hash "0s3r9lk1c3qip4f2k1vraakncjhd9f634jy1vd8fhdhfbd82j0qj") (yanked #t)))

