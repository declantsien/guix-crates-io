(define-module (crates-io ms i_) #:use-module (crates-io))

(define-public crate-msi_ffi-0.7 (crate (name "msi_ffi") (vers "0.7.0") (deps (list (crate-dep (name "chrono") (req "^0.4.37") (default-features #t) (kind 0)) (crate-dep (name "msi") (req "^0.7.0") (default-features #t) (kind 0)) (crate-dep (name "safer-ffi") (req "^0.1.6") (features (quote ("headers" "python-headers"))) (default-features #t) (kind 0)))) (hash "1lf2jy0qdvmji3njja1gdm1ybd97lv9m6rynlmphsp0zxcn97lvi")))

(define-public crate-msi_keyboard-0.0.1 (crate (name "msi_keyboard") (vers "0.0.1") (deps (list (crate-dep (name "hidapi_rust") (req "^0.1.3") (default-features #t) (kind 0)))) (hash "1cybyd6yyijn4c14p1pjcxffibranfd642gxwbl6x43ldvbb94lm") (yanked #t)))

(define-public crate-msi_klm-0.1 (crate (name "msi_klm") (vers "0.1.1") (deps (list (crate-dep (name "getopts") (req "^0.2.12") (default-features #t) (kind 0)) (crate-dep (name "hidapi_rust") (req "^0.1.1") (default-features #t) (kind 0)))) (hash "0lyhx435m5i1r84x84my8g24yzg95x9s5g2v8qjagsy3bvl35hwn")))

(define-public crate-msi_klm-0.1 (crate (name "msi_klm") (vers "0.1.2") (deps (list (crate-dep (name "getopts") (req "^0.2.12") (default-features #t) (kind 0)) (crate-dep (name "hidapi_rust") (req "^0.1.1") (default-features #t) (kind 0)))) (hash "12dnm9h9731lzf8xs309a88p2kl0vgc0zvqscga3wh19b5all4b5")))

(define-public crate-msi_klm-0.1 (crate (name "msi_klm") (vers "0.1.3") (deps (list (crate-dep (name "getopts") (req "^0.2.12") (default-features #t) (kind 0)) (crate-dep (name "hidapi_rust") (req "^0.1.2") (default-features #t) (kind 0)))) (hash "122yndaalg0cjazxjpgdl0x7qhrnc2mivwwsnwhaj1p99k0fn5la")))

(define-public crate-msi_klm-0.1 (crate (name "msi_klm") (vers "0.1.4") (deps (list (crate-dep (name "getopts") (req "^0.2.12") (default-features #t) (kind 0)) (crate-dep (name "hidapi_rust") (req "^0.1.3") (default-features #t) (kind 0)))) (hash "1wrh7zg3nsg6pd3cxzdbmsnyy8sg68ibs8695s4a1bhwbx1320zz")))

(define-public crate-msi_klm-0.1 (crate (name "msi_klm") (vers "0.1.5") (deps (list (crate-dep (name "getopts") (req "^0.2.12") (default-features #t) (kind 0)) (crate-dep (name "hidapi_rust") (req "^0.1.3") (default-features #t) (kind 0)))) (hash "0bkdqfk5xl04fbxv4sksc0dnx8grwmpw4yllm4bwza7cvy0amw2v")))

(define-public crate-msi_klm-0.1 (crate (name "msi_klm") (vers "0.1.6") (deps (list (crate-dep (name "getopts") (req "^0.2.12") (default-features #t) (kind 0)) (crate-dep (name "hidapi_rust") (req "^0.1.3") (default-features #t) (kind 0)))) (hash "0i2zs5rfih8bb0di95v9n3shsiv5mi7zwyfip65qdwa68x139gr8")))

(define-public crate-msi_klm-0.2 (crate (name "msi_klm") (vers "0.2.0") (deps (list (crate-dep (name "getopts") (req "^0.2.12") (default-features #t) (kind 0)) (crate-dep (name "hidapi") (req "^0.2.0") (default-features #t) (kind 0)))) (hash "0pyplivac7albhn992lvw1zkwrxrk341lzh220zkppz7kgjsiic8")))

(define-public crate-msi_klm-0.2 (crate (name "msi_klm") (vers "0.2.1") (deps (list (crate-dep (name "getopts") (req "^0.2.12") (default-features #t) (kind 0)) (crate-dep (name "hidapi") (req "^0.2.0") (default-features #t) (kind 0)))) (hash "04cmb7xix9d8lv7d5kf1ns4srjnrp5b0pqsmcxjfcmi4s71iiqnc")))

(define-public crate-msi_klm-0.2 (crate (name "msi_klm") (vers "0.2.2") (deps (list (crate-dep (name "getopts") (req "^0.2.12") (default-features #t) (kind 0)) (crate-dep (name "hidapi") (req "^0.2.0") (default-features #t) (kind 0)))) (hash "0nw4iqnxpn3151rlz0as054zikd9mpvaky9wima6k9r1ph838x4i")))

(define-public crate-msi_klm-0.3 (crate (name "msi_klm") (vers "0.3.0") (deps (list (crate-dep (name "getopts") (req "^0.2.12") (default-features #t) (kind 0)) (crate-dep (name "hidapi") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2.15") (default-features #t) (kind 0)))) (hash "0lygl71jv1b1kg9mlm56idg1w3bj52f7qgz2dal5xff57j5mcdb8")))

