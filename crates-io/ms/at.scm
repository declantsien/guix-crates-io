(define-module (crates-io ms at) #:use-module (crates-io))

(define-public crate-msat-0.1 (crate (name "msat") (vers "0.1.0") (deps (list (crate-dep (name "rsat") (req "=0.1.11") (default-features #t) (kind 0)))) (hash "0f6ljdzvgm5hj9axbxjraw62k44mmzpqac215c4xfi0r810cavyj")))

(define-public crate-msat-0.1 (crate (name "msat") (vers "0.1.1") (deps (list (crate-dep (name "rsat") (req "=0.1.12") (default-features #t) (kind 0)) (crate-dep (name "solhop-types") (req "=0.1.0") (default-features #t) (kind 0)))) (hash "1vnwsmrhj8xs4qv8fh6xkj9y3jzv2mzmfalfdik1m53dz48iwplq")))

