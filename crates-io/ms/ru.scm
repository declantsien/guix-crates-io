(define-module (crates-io ms ru) #:use-module (crates-io))

(define-public crate-msru-0.1 (crate (name "msru") (vers "0.1.0") (hash "1habkzxw7ig0s6iaw281j03a7wd6f9y4fbbkhnl1n08yv0kd3xw7") (rust-version "1.59")))

(define-public crate-msru-0.2 (crate (name "msru") (vers "0.2.0") (hash "1aiy58yzy0rppfsgir1djbcm38fb7q3fpv02xnggss7hiqh1980m") (rust-version "1.59")))

