(define-module (crates-io ms o5) #:use-module (crates-io))

(define-public crate-mso5k_dumpfb-0.1 (crate (name "mso5k_dumpfb") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^3.0.0-beta.2") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2.88") (default-features #t) (kind 0)) (crate-dep (name "memmap") (req "^0.7.0") (default-features #t) (kind 0)) (crate-dep (name "png") (req "^0.16.8") (default-features #t) (kind 0)) (crate-dep (name "simple-error") (req "^0.2") (default-features #t) (kind 0)))) (hash "1fikggiigj5l74hd2fycwd9cfpv7g4walcz2lqqdidxxw4psmkyv")))

