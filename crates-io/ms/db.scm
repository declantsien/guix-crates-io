(define-module (crates-io ms db) #:use-module (crates-io))

(define-public crate-msdb-0.4 (crate (name "msdb") (vers "0.4.0") (deps (list (crate-dep (name "clap") (req "^2.33.0") (default-features #t) (kind 0)) (crate-dep (name "hex") (req "^0.4.1") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.7.3") (default-features #t) (kind 0)) (crate-dep (name "rpassword") (req "^4.0.3") (default-features #t) (kind 0)) (crate-dep (name "rustyline") (req "^6.0.0") (default-features #t) (kind 0)) (crate-dep (name "sodiumoxide") (req "^0.2.5") (default-features #t) (kind 0)))) (hash "0175b26mkglqdymvpf5rj9pm9il62yfymfayhki1xj0k3h4vphk5")))

