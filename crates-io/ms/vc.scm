(define-module (crates-io ms vc) #:use-module (crates-io))

(define-public crate-msvc-alloca-0.1 (crate (name "msvc-alloca") (vers "0.1.0") (deps (list (crate-dep (name "cc") (req "^1.0") (default-features #t) (kind 1)))) (hash "1197g8vw5ix9wn5qbq3fdvz7sc95y7v60lxrr5zzw88ndm52j49s")))

(define-public crate-msvc-alloca-0.2 (crate (name "msvc-alloca") (vers "0.2.0") (deps (list (crate-dep (name "cc") (req "^1.0") (default-features #t) (kind 1)))) (hash "0af4xjafsa5168v8vhmp2mjskffngjgyp653s78y05z9bssd1lkz")))

(define-public crate-msvc-alloca-0.3 (crate (name "msvc-alloca") (vers "0.3.0") (deps (list (crate-dep (name "cc") (req "^1.0") (default-features #t) (kind 1)) (crate-dep (name "cfg-if") (req "^1.0") (default-features #t) (kind 1)))) (hash "16qs1wqkazhx0vr3bzdc31xp2j41xilmwj8hdmx6nj1izxha8dzh")))

(define-public crate-msvc-demangler-0.1 (crate (name "msvc-demangler") (vers "0.1.0") (deps (list (crate-dep (name "bitflags") (req "^1.0.1") (default-features #t) (kind 0)))) (hash "1vlg8x4yxi3y40s9pmbhiyh2y3i7ldyh6q00lwd5vwgn9yk6l0dv")))

(define-public crate-msvc-demangler-0.2 (crate (name "msvc-demangler") (vers "0.2.0") (deps (list (crate-dep (name "bitflags") (req "^1.0.1") (default-features #t) (kind 0)))) (hash "199q44v0x503hkv35in54kilh17knnb1rm4wy4pvwbd6kc3w096c")))

(define-public crate-msvc-demangler-0.3 (crate (name "msvc-demangler") (vers "0.3.0") (deps (list (crate-dep (name "bitflags") (req "^1.0.1") (default-features #t) (kind 0)))) (hash "1b5v6jabfvsz2a4ax22yshnd9rh12csp9fdvig88vvl4pazn6lxx")))

(define-public crate-msvc-demangler-0.3 (crate (name "msvc-demangler") (vers "0.3.1") (deps (list (crate-dep (name "bitflags") (req "^1.0.1") (default-features #t) (kind 0)))) (hash "1n8ji0dc30g8fxg5icn830rwi8wiv6azj37mw87j3a40j0xgqx08")))

(define-public crate-msvc-demangler-0.3 (crate (name "msvc-demangler") (vers "0.3.2") (deps (list (crate-dep (name "bitflags") (req "^1.0.1") (default-features #t) (kind 0)))) (hash "1qk6vfb6hi1hj5x5ajvgqf5zwk5lna0ck1wdw8s4b4gf86rq2rzg")))

(define-public crate-msvc-demangler-0.4 (crate (name "msvc-demangler") (vers "0.4.0") (deps (list (crate-dep (name "bitflags") (req "^1.0.1") (default-features #t) (kind 0)))) (hash "07da296jngcv16b9hqyd3w4qyd28c78c0fw0inhsky2q8zr3r4sr")))

(define-public crate-msvc-demangler-0.5 (crate (name "msvc-demangler") (vers "0.5.0") (deps (list (crate-dep (name "bitflags") (req "^1.0.1") (default-features #t) (kind 0)))) (hash "1avam6df0gsysyjg4ww7spfy95l67wj26rwv0w180yjdm5wwqf78")))

(define-public crate-msvc-demangler-0.6 (crate (name "msvc-demangler") (vers "0.6.0") (deps (list (crate-dep (name "bitflags") (req "^1.0.1") (default-features #t) (kind 0)))) (hash "0r21gyb16abbpjfpay9pps5mmxk7mjzrbhr2xlnpzqq1nh00zfn7")))

(define-public crate-msvc-demangler-0.7 (crate (name "msvc-demangler") (vers "0.7.0") (deps (list (crate-dep (name "bitflags") (req "^1.0.1") (default-features #t) (kind 0)))) (hash "14wgakm2rywx1vnrfyrilbbsvpgmg9gyj6nsfzb9gc7jhc9428wz")))

(define-public crate-msvc-demangler-0.8 (crate (name "msvc-demangler") (vers "0.8.0") (deps (list (crate-dep (name "bitflags") (req "^1.0.1") (default-features #t) (kind 0)))) (hash "0q602z4zrhqra4fgxps78gn7d9x9jp9nq48a8ab07z025w9cz135")))

(define-public crate-msvc-demangler-0.9 (crate (name "msvc-demangler") (vers "0.9.0") (deps (list (crate-dep (name "bitflags") (req "^1.0.1") (default-features #t) (kind 0)))) (hash "1j7kkmbd9yvhk7dmvd29pqcg9mcjdw5p1ia1kihh16zss1nprdmz")))

(define-public crate-msvc-demangler-0.10 (crate (name "msvc-demangler") (vers "0.10.0") (deps (list (crate-dep (name "bitflags") (req "^2") (default-features #t) (kind 0)))) (hash "1czx6fzw00b0zngp0g0hl6967z23rk529y117gbzpyx7wf1ck215")))

(define-public crate-msvc-demangler-0.10 (crate (name "msvc-demangler") (vers "0.10.1") (deps (list (crate-dep (name "bitflags") (req "^2") (default-features #t) (kind 0)))) (hash "1z55341d1lrvy30r2n821z9001xd84qjz0mlxbnfi06qnwxmmhn4")))

(define-public crate-msvc_def-0.1 (crate (name "msvc_def") (vers "0.1.0") (hash "0qj80xwiil5y2i29dkdjxfhfgs54dkgcv7p4gmj3x89a2vdlbvff") (features (quote (("std" "alloc") ("default" "std") ("alloc")))) (rust-version "1.65.0")))

(define-public crate-msvc_spectre_libs-0.1 (crate (name "msvc_spectre_libs") (vers "0.1.0") (deps (list (crate-dep (name "cc") (req "^1.0") (default-features #t) (kind 1)))) (hash "1xn9pg6f39d39fi92k6ffs30q1zh8mbv1hgjdq8x4jzj69yzx6fb") (features (quote (("error"))))))

(define-public crate-msvc_spectre_libs-0.1 (crate (name "msvc_spectre_libs") (vers "0.1.1") (deps (list (crate-dep (name "cc") (req "^1.0") (default-features #t) (target "cfg(all(target_os = \"windows\", target_env = \"msvc\"))") (kind 1)))) (hash "1cdvpwlc1q8rlqsma5jxhjlilg8cfs3hcalbzw2rhs4b88ldjcb4") (features (quote (("error"))))))

