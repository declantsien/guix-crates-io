(define-module (crates-io ms -d) #:use-module (crates-io))

(define-public crate-ms-detours-4 (crate (name "ms-detours") (vers "4.0.1") (deps (list (crate-dep (name "bindgen") (req "^0.60.1") (default-features #t) (kind 1)) (crate-dep (name "cc") (req "^1.0.73") (default-features #t) (kind 1)) (crate-dep (name "winapi") (req "^0.3") (features (quote ("minwindef" "libloaderapi"))) (default-features #t) (kind 2)))) (hash "14mbkcw7zdnl6c08va7p3prp6l8zcfy1s40s18wliabk4x3yq2g8")))

(define-public crate-ms-detours-4 (crate (name "ms-detours") (vers "4.0.2") (deps (list (crate-dep (name "bindgen") (req "^0.60.1") (default-features #t) (kind 1)) (crate-dep (name "cc") (req "^1.0.73") (default-features #t) (kind 1)) (crate-dep (name "winapi") (req "^0.3") (features (quote ("minwindef" "libloaderapi"))) (default-features #t) (kind 2)))) (hash "00al2j6wlpsfc42n6s1nq1lsg9dnh7cggcw4b0n1mg4l11diy1s7") (yanked #t)))

(define-public crate-ms-detours-4 (crate (name "ms-detours") (vers "4.0.3") (deps (list (crate-dep (name "bindgen") (req "^0.60.1") (default-features #t) (kind 1)) (crate-dep (name "cc") (req "^1.0.73") (default-features #t) (kind 1)) (crate-dep (name "winapi") (req "^0.3") (features (quote ("minwindef" "libloaderapi" "processthreadsapi" "windef"))) (default-features #t) (kind 0)))) (hash "18ycd8hypd84svg4wdj8vysqcb94z1xyzy14phcjf485rd8w3w8h")))

