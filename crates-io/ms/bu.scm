(define-module (crates-io ms bu) #:use-module (crates-io))

(define-public crate-msbuild-0.1 (crate (name "msbuild") (vers "0.1.0") (deps (list (crate-dep (name "serde_json") (req "^1.0.115") (default-features #t) (kind 0)))) (hash "18ywbz7a606akc45aa7mkjyan6kg3lvm5qfwn328k16x4qm0vwbm")))

