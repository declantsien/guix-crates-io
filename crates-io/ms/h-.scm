(define-module (crates-io ms h-) #:use-module (crates-io))

(define-public crate-msh-rw-0.1 (crate (name "msh-rw") (vers "0.1.0") (deps (list (crate-dep (name "bstr") (req "^0.2.13") (default-features #t) (kind 0)) (crate-dep (name "insta") (req "^0.16.0") (default-features #t) (kind 2)) (crate-dep (name "nom") (req "^5.1.1") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.52") (default-features #t) (kind 2)) (crate-dep (name "thiserror") (req "^1.0.15") (default-features #t) (kind 0)))) (hash "0ys16jwvl4pjanfip54ki47w872qvj7w6a0ji75awfcx8q7q6d0w")))

