(define-module (crates-io ms et) #:use-module (crates-io))

(define-public crate-mset-0.0.1 (crate (name "mset") (vers "0.0.1") (deps (list (crate-dep (name "version-sync") (req "^0.8") (default-features #t) (kind 2)))) (hash "0a6c9q660l3cwp40hlmxdclrvxnrg96js5ki3d8ym7gxb4vq3a8b")))

(define-public crate-mset-0.0.2 (crate (name "mset") (vers "0.0.2") (deps (list (crate-dep (name "version-sync") (req "^0.8") (default-features #t) (kind 2)))) (hash "0ynjpj12n4q6f4xqmv6jy2g65n8fzj05zjxjqm79b05wmhyh6cna")))

(define-public crate-mset-0.0.3 (crate (name "mset") (vers "0.0.3") (deps (list (crate-dep (name "version-sync") (req "^0.8") (default-features #t) (kind 2)))) (hash "1y4sbys6frn3pnfgzyy2wdhvyv66424gyjvh26p46555vp9g6cpm")))

(define-public crate-mset-0.0.4 (crate (name "mset") (vers "0.0.4") (deps (list (crate-dep (name "version-sync") (req "^0.8") (default-features #t) (kind 2)))) (hash "0hsxw2w28x67rqzv35d4bv7p0b6b57f8zjqg0fd43nc3427n5vgw")))

(define-public crate-mset-0.1 (crate (name "mset") (vers "0.1.0") (deps (list (crate-dep (name "version-sync") (req "^0.9.4") (default-features #t) (kind 2)))) (hash "1p0qyrk4i93jqsi39p6ymykz5p2q09qh1sm9ascqw528girwzl85")))

(define-public crate-mset-0.1 (crate (name "mset") (vers "0.1.1") (deps (list (crate-dep (name "version-sync") (req "^0.9.4") (default-features #t) (kind 2)))) (hash "0h4ivbpjsjbax6r2zz68rd4cnpslj7vrql3xdvn8j3ib7mmd3i16")))

