(define-module (crates-io ms do) #:use-module (crates-io))

(define-public crate-msdos_time-0.1 (crate (name "msdos_time") (vers "0.1.0") (deps (list (crate-dep (name "kernel32-sys") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "time") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "winapi") (req "^0.2") (default-features #t) (kind 0)))) (hash "06n6yrd4msmsb272fykmgrkm15k09lbka0z56lflj30wxarq3pwl")))

(define-public crate-msdos_time-0.1 (crate (name "msdos_time") (vers "0.1.1") (deps (list (crate-dep (name "kernel32-sys") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "time") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "winapi") (req "^0.2") (default-features #t) (kind 0)))) (hash "0x4sllm72s6r5qba1fw5icxzg47rvvv74l5x5d2h80ga0ch88blc")))

(define-public crate-msdos_time-0.1 (crate (name "msdos_time") (vers "0.1.2") (deps (list (crate-dep (name "kernel32-sys") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "time") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "winapi") (req "^0.2") (default-features #t) (kind 0)))) (hash "1p2yx15xlxmfggjlylbxxha8km44qhk4bxrydj6cc4inaw86zv5w")))

(define-public crate-msdos_time-0.1 (crate (name "msdos_time") (vers "0.1.3") (deps (list (crate-dep (name "kernel32-sys") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "time") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "winapi") (req "^0.2") (default-features #t) (kind 0)))) (hash "0ajlyydbdq26ssm7blclhr0pl31mkl0wpjnyxb9231qwyrs9i33l")))

(define-public crate-msdos_time-0.1 (crate (name "msdos_time") (vers "0.1.4") (deps (list (crate-dep (name "kernel32-sys") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "time") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "winapi") (req "^0.2") (default-features #t) (kind 0)))) (hash "1n67868fdhv1h58d9aakr7ggybp7gdgnjhq3anr0yj58cg66hjy0")))

(define-public crate-msdos_time-0.1 (crate (name "msdos_time") (vers "0.1.5") (deps (list (crate-dep (name "kernel32-sys") (req "^0.2") (default-features #t) (target "cfg(windows)") (kind 0)) (crate-dep (name "time") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "winapi") (req "^0.2") (default-features #t) (target "cfg(windows)") (kind 0)))) (hash "0n59jnb76sbzr9mss37ayb17cix6hkrfv3v129wf117apisrvfk5")))

(define-public crate-msdos_time-0.1 (crate (name "msdos_time") (vers "0.1.6") (deps (list (crate-dep (name "time") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "winapi") (req "^0.3") (features (quote ("winbase" "timezoneapi"))) (default-features #t) (target "cfg(windows)") (kind 0)))) (hash "0abpxgppbmq6pvms5asv5bpni11sb18smwn1x6zv2my0a3lxznda")))

