(define-module (crates-io ms fs) #:use-module (crates-io))

(define-public crate-msfs-0.0.0 (crate (name "msfs") (vers "0.0.0-pre.1") (deps (list (crate-dep (name "bindgen") (req "^0.53") (default-features #t) (kind 1)))) (hash "0wqxa0y0qr9glh9dv8vv2v39cv41s7s9nks6mghbh2qhzba519id")))

(define-public crate-msfs-0.0.1 (crate (name "msfs") (vers "0.0.1-alpha.0") (deps (list (crate-dep (name "bindgen") (req "^0.55") (default-features #t) (kind 1)) (crate-dep (name "futures") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "msfs_derive") (req "^0.0.1-alpha.0") (default-features #t) (kind 0)) (crate-dep (name "msfs_sdk") (req "^0.0.1-alpha.0") (default-features #t) (kind 1)))) (hash "0zmsq23l6hfcx2v9qn6ma5kbl4k5cwv7476sh5fysrz42ii9h8na")))

(define-public crate-msfs-0.0.1 (crate (name "msfs") (vers "0.0.1-alpha.2") (deps (list (crate-dep (name "bindgen") (req "^0.55") (default-features #t) (kind 1)) (crate-dep (name "cc") (req "^1.0") (default-features #t) (kind 1)) (crate-dep (name "futures") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "msfs_derive") (req "^0.0.1-alpha.2") (default-features #t) (kind 0)) (crate-dep (name "msfs_sdk") (req "^0.0.1-alpha.2") (default-features #t) (kind 1)))) (hash "0vlv07zqk0bj5w9illprlq4p5n32i623px0jfw9pv99shq9ish8m")))

(define-public crate-msfs-0.1 (crate (name "msfs") (vers "0.1.0") (deps (list (crate-dep (name "bindgen") (req "^0.59") (default-features #t) (kind 1)) (crate-dep (name "cc") (req "^1.0") (default-features #t) (kind 1)) (crate-dep (name "futures") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "msfs_derive") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "msfs_sdk") (req "^0.1.0") (default-features #t) (kind 1)))) (hash "1ydw4mwv80lva5af20kaip7jv8203wl191p7gv97jq2xn8jkf7bx")))

(define-public crate-msfs_derive-0.0.1 (crate (name "msfs_derive") (vers "0.0.1-alpha.0") (deps (list (crate-dep (name "msfs_sdk") (req "^0.0.1-alpha.0") (default-features #t) (kind 1)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "04ndkh14jycc8s33sns3z1hyapg4wiy57mfgc8hfybhlag7a7v84")))

(define-public crate-msfs_derive-0.0.1 (crate (name "msfs_derive") (vers "0.0.1-alpha.2") (deps (list (crate-dep (name "msfs_sdk") (req "^0.0.1-alpha.2") (default-features #t) (kind 1)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "0xzg5h6s53p4za2n0lb23vaqgfdsldfj8awjyrlzkk4c242rqdj2")))

(define-public crate-msfs_derive-0.1 (crate (name "msfs_derive") (vers "0.1.0") (deps (list (crate-dep (name "msfs_sdk") (req "^0.1.0") (default-features #t) (kind 1)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "1f4k0hlk40pa0jgfwpmqr0r0kw27dd6acg83h8hgs01pjs5a2w90")))

(define-public crate-msfs_sdk-0.0.0 (crate (name "msfs_sdk") (vers "0.0.0") (hash "0zyzmpsii5pdv69w5s1pb2fs6kg4y120myh2y47yab9wjpvvspg8")))

(define-public crate-msfs_sdk-0.0.1 (crate (name "msfs_sdk") (vers "0.0.1-alpha.1") (hash "0h7vyr5nc4qy7mmszlvy5q1863kz4yq91xgg2ayn6dg942mkk13w")))

(define-public crate-msfs_sdk-0.0.1 (crate (name "msfs_sdk") (vers "0.0.1-alpha.2") (hash "0v94qqp3n2r64yfs5ymlj5rh111mpd5nhwlad2mi2kx1kr23zbg0")))

(define-public crate-msfs_sdk-0.1 (crate (name "msfs_sdk") (vers "0.1.0") (hash "0fj6407lkmziwbsm64r7ky35f1k00wy1r86470mqmssq30nqfgcg")))

