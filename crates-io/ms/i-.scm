(define-module (crates-io ms i-) #:use-module (crates-io))

(define-public crate-msi-klc-1 (crate (name "msi-klc") (vers "1.0.0") (deps (list (crate-dep (name "clap") (req "^3.2.22") (default-features #t) (kind 0)) (crate-dep (name "hidapi") (req "^1.4.2") (default-features #t) (kind 0)))) (hash "13pyw4nskks3v394a960ng2rbah8yd26j783fs8vpml73kmkz6aw")))

(define-public crate-msi-klc-1 (crate (name "msi-klc") (vers "1.0.1") (deps (list (crate-dep (name "clap") (req "^3.2.22") (default-features #t) (kind 0)) (crate-dep (name "hidapi") (req "^1.4.2") (default-features #t) (kind 0)))) (hash "1mkpcl98669vvppdwd2fa6fcnz5fy16cvbdgxffmaadqn293n1im")))

(define-public crate-msi-klc-1 (crate (name "msi-klc") (vers "1.0.2") (deps (list (crate-dep (name "clap") (req "^3.2.22") (default-features #t) (kind 0)) (crate-dep (name "hidapi") (req "^1.4.2") (default-features #t) (kind 0)))) (hash "0cx3v61hmjks0gbsgk6dkym0gf072vjn8a0zh9qna62xzkpqimlz")))

(define-public crate-msi-klc-1 (crate (name "msi-klc") (vers "1.0.3") (deps (list (crate-dep (name "clap") (req "^3.2.22") (default-features #t) (kind 0)) (crate-dep (name "hidapi") (req "^2.4.1") (default-features #t) (kind 0)))) (hash "06lbfvzyy8xfn5aa2r2w3a8w096sn9jhib348hvjhxgx38d0fi4r")))

(define-public crate-msi-klc-1 (crate (name "msi-klc") (vers "1.0.4") (deps (list (crate-dep (name "clap") (req "^3.2.22") (default-features #t) (kind 0)) (crate-dep (name "hidapi") (req "^1.4.2") (default-features #t) (kind 0)))) (hash "1piqisb6w0pvv105kqwqmfqrk7j74j7isyf1viflbly9q433jy27")))

(define-public crate-msi-klc-1 (crate (name "msi-klc") (vers "1.1.0") (deps (list (crate-dep (name "clap") (req "^3.2.22") (default-features #t) (kind 0)) (crate-dep (name "hidapi") (req "^2.4.1") (features (quote ("linux-static-libusb"))) (kind 0)))) (hash "0088vq2jkq74s6iviqx7dw5ldzjdw976myp9zfycai3vdgs6kzq2")))

