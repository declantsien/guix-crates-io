(define-module (crates-io cc an) #:use-module (crates-io))

(define-public crate-ccanvas-config-derive-0.1 (crate (name "ccanvas-config-derive") (vers "0.1.0") (deps (list (crate-dep (name "quote") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2") (features (quote ("derive" "proc-macro" "parsing"))) (kind 0)))) (hash "1r1ypvc6rdb54jailn51zrl15bpxgxwjx8lwqxrq86j9la4457nh")))

