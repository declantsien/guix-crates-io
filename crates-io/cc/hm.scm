(define-module (crates-io cc hm) #:use-module (crates-io))

(define-public crate-cchmod-0.1 (crate (name "cchmod") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^3.0.0-beta.2") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0") (default-features #t) (kind 0)))) (hash "06v2kgcg3z2xj6l37wjnfpb7hnvhnvq7ii0adb3iny8y50hr67ja")))

(define-public crate-cchmod-0.1 (crate (name "cchmod") (vers "0.1.1") (deps (list (crate-dep (name "clap") (req "^3.0.0-beta.2") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0") (default-features #t) (kind 0)))) (hash "0ald1rwdlc98af8shrjd75wxny4xj2xcib9769fsxm7qv6py6hm8")))

(define-public crate-cchmod-0.1 (crate (name "cchmod") (vers "0.1.2") (deps (list (crate-dep (name "clap") (req "^3.0.0-beta.2") (default-features #t) (kind 0)) (crate-dep (name "doc-comment") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0") (default-features #t) (kind 0)))) (hash "122isc7f6yww07qhnx0jh43gly5jygz5ayrv1id3j9hbgvx58qk6")))

(define-public crate-cchmod-0.1 (crate (name "cchmod") (vers "0.1.3") (deps (list (crate-dep (name "clap") (req "^3.0") (features (quote ("cargo" "derive"))) (default-features #t) (kind 0)) (crate-dep (name "doc-comment") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0") (default-features #t) (kind 0)))) (hash "1c6wxd9whk5b6bz3v9ba8qfa3843cg4x9iqmgm1nsnkm2vb1mi30")))

