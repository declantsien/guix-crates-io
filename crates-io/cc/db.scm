(define-module (crates-io cc db) #:use-module (crates-io))

(define-public crate-ccdb-0.1 (crate (name "ccdb") (vers "0.1.0") (deps (list (crate-dep (name "cc") (req "^1.0") (default-features #t) (kind 1)) (crate-dep (name "openfile") (req "^0.1.1") (default-features #t) (kind 0)))) (hash "04k7xbmhkvv2pnmjhqgzn51kf0avm1xhqbj37m6j39adw9avx9iq") (links "./src/c/")))

(define-public crate-ccdb-0.2 (crate (name "ccdb") (vers "0.2.0") (deps (list (crate-dep (name "cc") (req "^1.0") (default-features #t) (kind 1)) (crate-dep (name "openfile") (req "^0.1.1") (default-features #t) (kind 0)))) (hash "0imvdiwpn0qnywx85kal4nv6h3771qfvdc0gjhhl024315c0b3zc") (links "./src/c/")))

(define-public crate-ccdb-0.3 (crate (name "ccdb") (vers "0.3.0") (deps (list (crate-dep (name "cc") (req "^1.0") (default-features #t) (kind 1)) (crate-dep (name "openfile") (req "^0.1.1") (default-features #t) (kind 0)))) (hash "0x2kmgx52wx4xpbj1z779fr885h2mg2ikl4z09j6h8kjg81f6z9n") (links "./src/c/")))

(define-public crate-ccdb-0.4 (crate (name "ccdb") (vers "0.4.0") (deps (list (crate-dep (name "cc") (req "^1.0") (default-features #t) (kind 1)) (crate-dep (name "openfile") (req "^0.1.1") (default-features #t) (kind 0)))) (hash "02kyf3f44d8h2jqa7wvh8i42j823pzvj2mvxdglfrk85qxfrsbn2") (links "./src/c/")))

(define-public crate-ccdb-0.5 (crate (name "ccdb") (vers "0.5.0") (deps (list (crate-dep (name "cc") (req "^1.0") (default-features #t) (kind 1)) (crate-dep (name "openfile") (req "^1.1.0") (default-features #t) (kind 0)))) (hash "1d55jpq165kgg9lz09kgyj6dh2franmvgl6bg34c4zv5yj2j78q1") (links "./src/c/")))

(define-public crate-ccdb-0.6 (crate (name "ccdb") (vers "0.6.0") (deps (list (crate-dep (name "cc") (req "^1.0") (default-features #t) (kind 1)) (crate-dep (name "openfile") (req "^1.1.0") (default-features #t) (kind 0)))) (hash "0ymzjixx82rfya2bb34cs5nvgqhg9l0jd9kbpmyhzki9cl096wpy") (links "./src/c/")))

(define-public crate-ccdb-0.7 (crate (name "ccdb") (vers "0.7.0") (deps (list (crate-dep (name "cc") (req "^1.0") (default-features #t) (kind 1)) (crate-dep (name "openfile") (req "^1.1.0") (default-features #t) (kind 0)))) (hash "1px91ya7q51pcf82m2sihns622y5zbcckb2rsp5imcfgr66pbg3n") (links "./src/c/")))

(define-public crate-ccdb-0.8 (crate (name "ccdb") (vers "0.8.0") (deps (list (crate-dep (name "cc") (req "^1.0") (default-features #t) (kind 1)) (crate-dep (name "openfile") (req "^1.1.0") (default-features #t) (kind 0)))) (hash "0y20icpzdmb1wnyizggxmhajrl9m7fgxsj56fhhjbwvyzwcd5rpz") (links "./src/c/")))

(define-public crate-ccdb-0.9 (crate (name "ccdb") (vers "0.9.0") (deps (list (crate-dep (name "cc") (req "^1.0") (default-features #t) (kind 1)) (crate-dep (name "openfile") (req "^1.1.0") (default-features #t) (kind 0)))) (hash "07s2n5kaam6wkfw0x1l0nw299gqsp9zizs5sz6ga1lrzyi5ar8lm") (links "./src/c/")))

(define-public crate-ccdb-1 (crate (name "ccdb") (vers "1.0.0") (deps (list (crate-dep (name "cc") (req "^1.0") (default-features #t) (kind 1)) (crate-dep (name "openfile") (req "^1.1.0") (default-features #t) (kind 0)))) (hash "1m1l22smil23jwndfaixmjmfnipg8q9is2cxd55as7izhkhcz3m8") (links "./src/c/")))

(define-public crate-ccdb-1 (crate (name "ccdb") (vers "1.1.0") (deps (list (crate-dep (name "cc") (req "^1.0") (default-features #t) (kind 1)) (crate-dep (name "openfile") (req "^1.1.0") (default-features #t) (kind 0)))) (hash "0hnjv6jkc7byspn1i4draz5j4zmmjqlvy5c3y8kacgxd7c6szvq6") (links "./src/c/")))

(define-public crate-ccdb-1 (crate (name "ccdb") (vers "1.1.1") (deps (list (crate-dep (name "cc") (req "^1.0") (default-features #t) (kind 1)) (crate-dep (name "openfile") (req "^1.1.0") (default-features #t) (kind 0)))) (hash "0zs90in7hqfpssj25r6z9v3s2n4ax7l9zi61p0pjra5dmb7cr2dc") (links "./src/c/")))

(define-public crate-ccdb-1 (crate (name "ccdb") (vers "1.2.0") (deps (list (crate-dep (name "cc") (req "^1.0") (default-features #t) (kind 1)) (crate-dep (name "openfile") (req "^1.1.0") (default-features #t) (kind 0)))) (hash "12rbi0hjnbg7hadkx38i2grhkvhdxsvz50813i7pk4vijpcjqlrh") (links "./src/c/")))

(define-public crate-ccdb-1 (crate (name "ccdb") (vers "1.3.0") (deps (list (crate-dep (name "cc") (req "^1.0") (default-features #t) (kind 1)) (crate-dep (name "openfile") (req "^1.1.0") (default-features #t) (kind 0)))) (hash "1yw2wd1ydz2yd4qfv64fhrmasis02zkpgjvw3djss43wh6zw12kh") (links "./src/c/")))

(define-public crate-ccdb-1 (crate (name "ccdb") (vers "1.4.0") (deps (list (crate-dep (name "cc") (req "^1.0") (default-features #t) (kind 1)) (crate-dep (name "openfile") (req "^1.1.0") (default-features #t) (kind 0)) (crate-dep (name "winapi-util") (req "^0.1.5") (default-features #t) (kind 0)))) (hash "1havks3c3f14fz9vk97p0wjgfwkcc8kyi81xs2i0qrs1yb6bch9j") (links "./src/c/")))

(define-public crate-ccdb-1 (crate (name "ccdb") (vers "1.5.0") (deps (list (crate-dep (name "cc") (req "^1.0") (default-features #t) (kind 1)) (crate-dep (name "openfile") (req "^1.1.0") (default-features #t) (kind 0)) (crate-dep (name "winapi-util") (req "^0.1.5") (default-features #t) (kind 0)))) (hash "0i1lq4hdnf42iji6z52rxw284nf50zy8r7hfwdwkmk7yd0hg08cc") (links "./src/c/")))

(define-public crate-ccdb-1 (crate (name "ccdb") (vers "1.6.0") (deps (list (crate-dep (name "cc") (req "^1.0") (default-features #t) (kind 1)) (crate-dep (name "openfile") (req "^1.1.0") (default-features #t) (kind 0)) (crate-dep (name "winapi-util") (req "^0.1.5") (default-features #t) (kind 0)))) (hash "1h4k1f7m5y91cndcxlcgc0qppbmlm081nfyxljg6r0bn3sz2fxvy") (links "./src/c/")))

(define-public crate-ccdb-1 (crate (name "ccdb") (vers "1.6.1") (deps (list (crate-dep (name "cc") (req "^1.0") (default-features #t) (kind 1)) (crate-dep (name "openfile") (req "^1.1.0") (default-features #t) (kind 0)) (crate-dep (name "winapi-util") (req "^0.1.5") (default-features #t) (kind 0)))) (hash "0i8frbdij8grxim18k5gb7h42ivp0zzi68hag8cjw107z524lf56") (links "./src/c/")))

(define-public crate-ccdb_script-0.1 (crate (name "ccdb_script") (vers "0.1.0") (hash "0yvl0978qwz9iixi215yr5zfrwj4f08kmv7hznk9j85cs3y8y9ml")))

(define-public crate-ccdb_script-1 (crate (name "ccdb_script") (vers "1.0.0") (hash "0viyi2c9pvbsfrf3588aqbq16sqc03b3p7szii7y1r2m4r1vcn9s")))

(define-public crate-ccdb_script-1 (crate (name "ccdb_script") (vers "1.1.0") (hash "0mmzqk4npvfiy4hzdzdn2fpj2y0h9m6l6dy543m3xh8k1k2qzn67")))

(define-public crate-ccdb_script-1 (crate (name "ccdb_script") (vers "1.2.0") (hash "0s89zr7gnrl1671jbv9lwr1fz55jhdv6g2hzvra0k8n4n1dcy6kw")))

(define-public crate-ccdb_script-1 (crate (name "ccdb_script") (vers "1.3.0") (hash "06wf1kap8qfb0s9kwb583z42wisdzsr6gnb0iwqhj9zjv4hfbyqm")))

(define-public crate-ccdb_script-1 (crate (name "ccdb_script") (vers "1.3.1") (hash "0xpbqjmakwis3zbjbdkg1wph9wxv3v65jvf3im3m2803pjxhklgg") (yanked #t)))

(define-public crate-ccdb_script-1 (crate (name "ccdb_script") (vers "1.3.2") (hash "1afychv5b7cd1hrzp4jbc0inzy48yv64r2df8fg35j4wjvn2cpvn")))

(define-public crate-ccdb_script-1 (crate (name "ccdb_script") (vers "1.4.0") (hash "1y8czi93m37nj7wg52s4vd5v5nqaafg7mv4hm7znwkwd5j7d7bw9")))

