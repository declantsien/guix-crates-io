(define-module (crates-io cc ge) #:use-module (crates-io))

(define-public crate-ccgeom-0.1 (crate (name "ccgeom") (vers "0.1.0") (deps (list (crate-dep (name "approx") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "rand_xorshift") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "vecmat") (req "^0.7") (default-features #t) (kind 0)))) (hash "0qdniljpvyfr3nsg2g0d0ab2rjf7g37psxssqhga7x7zcjlc71fj")))

