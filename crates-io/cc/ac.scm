(define-module (crates-io cc ac) #:use-module (crates-io))

(define-public crate-ccache-0.1 (crate (name "ccache") (vers "0.1.0") (deps (list (crate-dep (name "bitfield") (req "^0.13") (default-features #t) (kind 0)) (crate-dep (name "hashbrown") (req "^0.11") (features (quote ("raw"))) (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0") (default-features #t) (kind 0)))) (hash "0fgqfyzf48lfnyhj1k0irxcrpvc2y05r281j7wv5bqg01hv2qxi9")))

(define-public crate-ccache_stats_reader-0.1 (crate (name "ccache_stats_reader") (vers "0.1.0") (deps (list (crate-dep (name "chrono") (req "> 0.2.23") (default-features #t) (kind 0)))) (hash "1v3z347w0f7bncd8971xrbi25mzvax06h9hb2z4gy3fsjk56faqz") (features (quote (("non_exhaustive") ("nightly" "external_doc" "non_exhaustive") ("external_doc") ("default"))))))

(define-public crate-ccache_stats_reader-0.1 (crate (name "ccache_stats_reader") (vers "0.1.1") (deps (list (crate-dep (name "chrono") (req ">= 0.4.0") (default-features #t) (kind 0)))) (hash "087r4djx86qjsg74562mcrb7yf35d4yv2km07gfnqqf26vqf2qx8") (features (quote (("non_exhaustive") ("nightly" "external_doc" "non_exhaustive") ("external_doc") ("default"))))))

(define-public crate-ccache_stats_reader-0.1 (crate (name "ccache_stats_reader") (vers "0.1.2") (deps (list (crate-dep (name "chrono") (req "^0.4") (features (quote ("clock"))) (kind 0)))) (hash "1qsk5dg98ck9s6kydd802d0j50sk7i4xp81jzx9n0acwj3rafjhi") (features (quote (("non_exhaustive") ("nightly" "external_doc" "non_exhaustive") ("external_doc") ("default"))))))

(define-public crate-ccache_stats_reader_extras-0.1 (crate (name "ccache_stats_reader_extras") (vers "0.1.0") (deps (list (crate-dep (name "ccache_stats_reader") (req "^0.1") (default-features #t) (kind 0)))) (hash "0dzkyxnjc56m13c26bb31gxbiyy0fmf5aqn2g1j86vd7hcq90g7l") (features (quote (("nightly" "ccache_stats_reader/nightly") ("default"))))))

