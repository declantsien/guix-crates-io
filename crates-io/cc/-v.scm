(define-module (crates-io cc -v) #:use-module (crates-io))

(define-public crate-cc-version-0.1 (crate (name "cc-version") (vers "0.1.0") (deps (list (crate-dep (name "cc") (req "^1.0.58") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.20") (default-features #t) (kind 0)))) (hash "0p70i5nfr3azpqsjayb5rc8vq1jvwhkpicfcdhphc6hyaa52n51f")))

