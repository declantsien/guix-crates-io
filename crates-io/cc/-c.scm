(define-module (crates-io cc -c) #:use-module (crates-io))

(define-public crate-cc-cli-0.1 (crate (name "cc-cli") (vers "0.1.0") (deps (list (crate-dep (name "anyhow") (req "^1.0.43") (default-features #t) (kind 0)) (crate-dep (name "argh") (req "^0.1.5") (default-features #t) (kind 0)) (crate-dep (name "inquire") (req "^0.0.6") (default-features #t) (kind 0)))) (hash "12lpdg2fl1rmbh7i8y1c32q93nvxs73jap01mshsmd3x4vkpvfir")))

(define-public crate-cc-cli-0.1 (crate (name "cc-cli") (vers "0.1.1") (deps (list (crate-dep (name "anyhow") (req "^1.0.43") (default-features #t) (kind 0)) (crate-dep (name "argh") (req "^0.1.5") (default-features #t) (kind 0)) (crate-dep (name "inquire") (req "^0.0.6") (default-features #t) (kind 0)))) (hash "1lvpg1myj86s4axnp85rbcq6002z1p21y1p5lrgb38j83j0ndygg")))

(define-public crate-cc-cli-0.1 (crate (name "cc-cli") (vers "0.1.2") (deps (list (crate-dep (name "anyhow") (req "^1.0.50") (default-features #t) (kind 0)) (crate-dep (name "argh") (req "^0.1.6") (default-features #t) (kind 0)) (crate-dep (name "inquire") (req "^0.2.1") (default-features #t) (kind 0)))) (hash "0qf2grilj6wxlwrsbmfzk5rzmhcm3j5wa8lfz02qxdgkg25v3j8q")))

(define-public crate-cc-cli-0.1 (crate (name "cc-cli") (vers "0.1.3") (deps (list (crate-dep (name "anyhow") (req "^1.0.65") (default-features #t) (kind 0)) (crate-dep (name "argh") (req "^0.1.8") (default-features #t) (kind 0)) (crate-dep (name "inquire") (req "^0.3.0") (default-features #t) (kind 0)))) (hash "1y7m4l0g0a2iygn7rxmcibqk25rs61slhbkhf2d2qcv2xv3wnrrr")))

(define-public crate-cc-cli-0.1 (crate (name "cc-cli") (vers "0.1.5") (deps (list (crate-dep (name "anyhow") (req "^1.0.70") (default-features #t) (kind 0)) (crate-dep (name "argh") (req "^0.1.10") (default-features #t) (kind 0)) (crate-dep (name "inquire") (req "^0.6.0") (default-features #t) (kind 0)))) (hash "0wmrvv82802svrddyij3jfbwvl1hs0x2ci28ldg5nbsghff7mbnh")))

