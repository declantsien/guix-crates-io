(define-module (crates-io cc ta) #:use-module (crates-io))

(define-public crate-cctalk-0.1 (crate (name "cctalk") (vers "0.1.0") (deps (list (crate-dep (name "log") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "serial") (req "^0.4.0") (default-features #t) (kind 0)))) (hash "0jv80j4bsmhfwjsm22l6aypdimzbcyz5mrispj3i72kz42bs2kki")))

(define-public crate-cctalk-0.2 (crate (name "cctalk") (vers "0.2.0") (deps (list (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "serial") (req "^0.4.0") (default-features #t) (kind 0)))) (hash "00n4l5130j8a1v1qaxn0avf4c7km84aljrxyl75hg0h2h3mhi6s6")))

