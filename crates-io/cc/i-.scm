(define-module (crates-io cc i-) #:use-module (crates-io))

(define-public crate-cci-rs-0.1 (crate (name "cci-rs") (vers "0.1.0") (deps (list (crate-dep (name "md-rs") (req ">=0.1.1, <0.2.0") (default-features #t) (kind 0)) (crate-dep (name "ta-common") (req ">=0.1.0, <0.2.0") (default-features #t) (kind 0)) (crate-dep (name "typprice-rs") (req ">=0.1.0, <0.2.0") (default-features #t) (kind 0)))) (hash "10wfp661h8hl9ig44jpsynsllgdzd1m6yvvamfckwn5y2vaws69r")))

