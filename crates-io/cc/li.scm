(define-module (crates-io cc li) #:use-module (crates-io))

(define-public crate-ccli-0.0.1 (crate (name "ccli") (vers "0.0.1") (deps (list (crate-dep (name "anyhow") (req "^1.0.75") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^4.4.11") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "color-eyre") (req "^0.6.2") (default-features #t) (kind 0)) (crate-dep (name "tracing-subscriber") (req "^0.3.18") (features (quote ("env-filter"))) (default-features #t) (kind 0)))) (hash "1db9qkhxdp8fvn5l4xsj67gfr2bpsl17gbcvx3nizmvc1gwkxnbm")))

(define-public crate-cclip-0.1 (crate (name "cclip") (vers "0.1.0") (deps (list (crate-dep (name "atty") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "copypasta") (req "^0.7") (default-features #t) (kind 0)) (crate-dep (name "copypasta-ext") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "which") (req "^4.0") (default-features #t) (kind 0)))) (hash "1hj047vyascj6l3cij8p8dks0bimn68l3gdgqm2dcp5inrfggl0w")))

