(define-module (crates-io cc s8) #:use-module (crates-io))

(define-public crate-ccs811-0.1 (crate (name "ccs811") (vers "0.1.0") (deps (list (crate-dep (name "rppal") (req "^0.11.3") (default-features #t) (kind 0)))) (hash "10dm9q20gwgvpmy0v5sxpmbrs2rckldjzqnd97w51kayxlaa3d4y")))

(define-public crate-ccs811-0.1 (crate (name "ccs811") (vers "0.1.1") (deps (list (crate-dep (name "rppal") (req "^0.11.3") (default-features #t) (kind 0)))) (hash "1qyaxkiks53ngap0j051czdwnn2dj87jiaxy81mmfb761s7cibav")))

(define-public crate-ccs811-0.1 (crate (name "ccs811") (vers "0.1.2") (deps (list (crate-dep (name "rppal") (req "^0.11.3") (default-features #t) (kind 0)))) (hash "19ak6ffp9k86q8m8m140zq3ifgv9mfrgml2k7mav69ghagbxnrkg")))

(define-public crate-ccs811-0.1 (crate (name "ccs811") (vers "0.1.3") (deps (list (crate-dep (name "rppal") (req "^0.11.3") (default-features #t) (kind 0)))) (hash "1n7w2ys8ivnzd804jhvy9piljgmm1imv36hhmf4i14fk08jh8cz7")))

(define-public crate-ccs811-rs-0.1 (crate (name "ccs811-rs") (vers "0.1.0") (deps (list (crate-dep (name "embedded-hal") (req "^0.2.3") (default-features #t) (kind 0)) (crate-dep (name "embedded-hal") (req "^0.2.3") (features (quote ("unproven"))) (default-features #t) (kind 2)) (crate-dep (name "linux-embedded-hal") (req "^0.3.0") (default-features #t) (kind 2)) (crate-dep (name "nb") (req "^0.1.1") (default-features #t) (kind 0)) (crate-dep (name "rppal") (req "^0.11.3") (features (quote ("hal"))) (default-features #t) (kind 2)))) (hash "0pw1df36jai7ff1mja2vbshdmqd05zl7mqwxdnwzkskccxfs5qa3")))

