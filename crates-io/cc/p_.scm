(define-module (crates-io cc p_) #:use-module (crates-io))

(define-public crate-ccp_core_affinity-0.8 (crate (name "ccp_core_affinity") (vers "0.8.1") (deps (list (crate-dep (name "libc") (req "^0.2.30") (default-features #t) (target "cfg(any(target_os = \"android\", target_os = \"linux\", target_os = \"macos\", target_os = \"freebsd\"))") (kind 0)) (crate-dep (name "num_cpus") (req "^1.14.0") (default-features #t) (kind 0)) (crate-dep (name "winapi") (req "^0.3.9") (features (quote ("processthreadsapi" "winbase"))) (default-features #t) (target "cfg(target_os = \"windows\")") (kind 0)))) (hash "0pjxhfn9dspjdi2d7csimy8vxnsx5qlkhmiaz44l0hz9czqng8rj")))

