(define-module (crates-io cc it) #:use-module (crates-io))

(define-public crate-ccitt-t4-t6-0.1 (crate (name "ccitt-t4-t6") (vers "0.1.0") (deps (list (crate-dep (name "color-eyre") (req ">=0.5.0, <0.6.0") (kind 2)) (crate-dep (name "structopt") (req ">=0.3.0, <0.4.0") (default-features #t) (kind 2)) (crate-dep (name "thiserror") (req ">=1.0.0, <2.0.0") (default-features #t) (kind 0)))) (hash "1jp8l8p0k2hj6a61jvazwxps9wiaicvyqff2bah7vzjhac273y6p")))

