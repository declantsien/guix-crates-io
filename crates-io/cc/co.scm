(define-module (crates-io cc co) #:use-module (crates-io))

(define-public crate-cccolutils-0.0.1 (crate (name "cccolutils") (vers "0.0.1") (deps (list (crate-dep (name "cc") (req "^1.0.30") (default-features #t) (kind 1)))) (hash "190f0f0f799f7haqixs7zkmz1j09w3iqnwc5y37r8igwzq1fbr9c")))

(define-public crate-cccolutils-0.0.2 (crate (name "cccolutils") (vers "0.0.2") (deps (list (crate-dep (name "cc") (req "^1.0.30") (default-features #t) (kind 1)))) (hash "0c7a2g45481qbyhzi6v0rkvk6x15j09z0ii099znlc0aimwc6k7d")))

(define-public crate-cccolutils-0.0.3 (crate (name "cccolutils") (vers "0.0.3") (deps (list (crate-dep (name "cc") (req "^1.0.30") (default-features #t) (kind 1)))) (hash "0hhicymlr1n5yifsv6y2zkxja5nwgx0s97777kffm0slp4mkfxv1") (links "krb5")))

(define-public crate-cccolutils-0.1 (crate (name "cccolutils") (vers "0.1.0") (deps (list (crate-dep (name "cc") (req "^1.0.30") (default-features #t) (kind 1)))) (hash "14gbwvkc109ix9mp3vpp9gfss2mndn1995a6k4g2v5zazqh8dim6") (links "krb5")))

(define-public crate-cccolutils-0.2 (crate (name "cccolutils") (vers "0.2.0") (deps (list (crate-dep (name "cc") (req "^1.0.30") (default-features #t) (kind 1)))) (hash "0hilaxnl7faibcj3cg19c5l8k5n56qxjahncxbkncy5rrs7yj2bg") (links "krb5")))

(define-public crate-cccolutils-0.2 (crate (name "cccolutils") (vers "0.2.1") (deps (list (crate-dep (name "cc") (req "^1.0.48") (default-features #t) (kind 1)))) (hash "1qy103lihb6x19c6dld47r3jpsgpb62ckw433vkk2fil1dgp0pf7") (links "krb5")))

(define-public crate-cccolutils-0.2 (crate (name "cccolutils") (vers "0.2.2") (deps (list (crate-dep (name "cc") (req "^1.0.48") (default-features #t) (kind 1)))) (hash "1j2py7n8m8adq4vl4h85wdws9nmby869mgfgpmz8lxx4palda33q") (links "krb5")))

