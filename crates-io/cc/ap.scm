(define-module (crates-io cc ap) #:use-module (crates-io))

(define-public crate-ccap-0.1 (crate (name "ccap") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^2.33") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.5.4") (default-features #t) (kind 0)))) (hash "1p8vgq264003dy1kbsjq8bym9jsbgrcl5j8svrv63rs87794824l")))

(define-public crate-ccapi-0.1 (crate (name "ccapi") (vers "0.1.0") (deps (list (crate-dep (name "anyhow") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "getopts") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "ureq") (req "^2.2.0") (default-features #t) (kind 0)))) (hash "1k3lbpbmwm0p2qcrv3bbinvasnqa90ngpxizsjqnssgzpdnl2jdy") (rust-version "1.43.1")))

(define-public crate-ccapi-0.2 (crate (name "ccapi") (vers "0.2.0") (deps (list (crate-dep (name "anyhow") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "getopts") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "ureq") (req "^2.4.0") (default-features #t) (kind 0)))) (hash "0c7ppk92dffg7dg4b43dyzr9rqnj5cfxh8zlfa74fjfwf5nd931p") (rust-version "1.43.1")))

(define-public crate-ccapi-0.3 (crate (name "ccapi") (vers "0.3.0") (deps (list (crate-dep (name "anyhow") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "getopts") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "ureq") (req "^2.4.0") (default-features #t) (kind 0)))) (hash "1dh1zlf4rmsshy49zdv9rr7ww8n0x5jaddmm1ifzx6x0kk03b7hy") (rust-version "1.43.1")))

