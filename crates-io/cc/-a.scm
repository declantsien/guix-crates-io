(define-module (crates-io cc -a) #:use-module (crates-io))

(define-public crate-cc-args-0.1 (crate (name "cc-args") (vers "0.1.0") (deps (list (crate-dep (name "bindgen") (req "^0.60.1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "cc") (req "^1.0.73") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "pkg-config") (req "^0.3.25") (optional #t) (default-features #t) (kind 0)))) (hash "1q31qg8gjaypzjwarg0gknandfh5dqmdvaps95q7xpaajcgrx8y3") (features (quote (("default")))) (v 2) (features2 (quote (("pkg-config" "dep:pkg-config") ("cc" "dep:cc") ("bindgen" "dep:bindgen"))))))

(define-public crate-cc-args-0.2 (crate (name "cc-args") (vers "0.2.0") (deps (list (crate-dep (name "bindgen") (req "^0.69.2") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "cc") (req "^1.0.83") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "pkg-config") (req "^0.3.29") (optional #t) (default-features #t) (kind 0)))) (hash "1hzdhsfwmq98dhpl4c0rd6wb1h1gzc54v2z69wrdcalk0pybm6wv") (features (quote (("default")))) (v 2) (features2 (quote (("pkg-config" "dep:pkg-config") ("cc" "dep:cc") ("bindgen" "dep:bindgen"))))))

