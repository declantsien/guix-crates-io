(define-module (crates-io cc wd) #:use-module (crates-io))

(define-public crate-ccwd-0.1 (crate (name "ccwd") (vers "0.1.0") (hash "117rawhblsmgp4hrl13rh0xd399isqsiicmrnl5qb23cp113dfs3")))

(define-public crate-ccwd-0.1 (crate (name "ccwd") (vers "0.1.1") (hash "0h5wjy9lwdv987466bl67zils64b9y20vzcrhrx95r5bjf61lr76")))

(define-public crate-ccwd-0.1 (crate (name "ccwd") (vers "0.1.2") (hash "1pgzv0axdjcmcbfpmlgzd4lblkfgvy2sh23il02pvsbd470sf8a2")))

