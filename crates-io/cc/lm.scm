(define-module (crates-io cc lm) #:use-module (crates-io))

(define-public crate-cclm-0.0.1 (crate (name "cclm") (vers "0.0.1") (deps (list (crate-dep (name "criterion") (req "^0.4.0") (default-features #t) (kind 2)) (crate-dep (name "serde") (req "^1.0.152") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "1jq2n5q5v4qjb7dqyn5ijnm3nfiaxpwcv5xb8xd7jka7ak1hdicr") (features (quote (("default")))) (rust-version "1.67")))

