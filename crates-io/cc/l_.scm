(define-module (crates-io cc l_) #:use-module (crates-io))

(define-public crate-ccl_owning_ref-0.4 (crate (name "ccl_owning_ref") (vers "0.4.0") (deps (list (crate-dep (name "ccl_stable_deref_trait") (req "^1.0.0") (default-features #t) (kind 0)))) (hash "1qyfqmay9ary54bwi5mqxmpl6cq8j753qfa58hjizbng7kddkwh0")))

(define-public crate-ccl_owning_ref-0.4 (crate (name "ccl_owning_ref") (vers "0.4.1") (deps (list (crate-dep (name "ccl_stable_deref_trait") (req "^1.0.0") (default-features #t) (kind 0)))) (hash "0y4v4ff87alblyqxvy0hpjyqz7305cvmiw53sdxxrj9x816h6phm")))

(define-public crate-ccl_owning_ref-0.4 (crate (name "ccl_owning_ref") (vers "0.4.2") (deps (list (crate-dep (name "ccl_stable_deref_trait") (req "^1.0.2") (default-features #t) (kind 0)))) (hash "13zfmxbp3bc18a0bwlvnqba553dsvkjc4x86w37szb6839m1gbrb")))

(define-public crate-ccl_stable_deref_trait-1 (crate (name "ccl_stable_deref_trait") (vers "1.0.0") (deps (list (crate-dep (name "parking_lot") (req "^0.8.0") (default-features #t) (kind 0)))) (hash "11cryafyqv0anvd9j1xm38q5xfvz7da1h7mz9zl9ll2gddqj41q2") (features (quote (("std") ("default" "std") ("alloc"))))))

(define-public crate-ccl_stable_deref_trait-1 (crate (name "ccl_stable_deref_trait") (vers "1.0.1") (deps (list (crate-dep (name "parking_lot") (req "^0.8.0") (default-features #t) (kind 0)))) (hash "15mf597502khjjp3178v6jz6ciy7bf27yq13y70276028i6201r3") (features (quote (("std") ("default" "std") ("alloc"))))))

(define-public crate-ccl_stable_deref_trait-1 (crate (name "ccl_stable_deref_trait") (vers "1.0.2") (deps (list (crate-dep (name "parking_lot") (req "^0.9.0") (default-features #t) (kind 0)))) (hash "0z1716mb10hcy2z6yvbzr8m9wfm5qzgw8dakqwfwba5fvjk0cx13") (features (quote (("std") ("default" "std") ("alloc"))))))

