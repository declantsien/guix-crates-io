(define-module (crates-io cc #{11}#) #:use-module (crates-io))

(define-public crate-cc1101-0.0.1 (crate (name "cc1101") (vers "0.0.1") (deps (list (crate-dep (name "embedded-hal") (req "^0.1.2") (default-features #t) (kind 0)))) (hash "01wqpc3hcqhmkx9bhaf7a4gvhq82a6m0s9dgnzx1i99m0gdg3hpd")))

(define-public crate-cc1101-0.0.2 (crate (name "cc1101") (vers "0.0.2") (deps (list (crate-dep (name "embedded-hal") (req "^0.2.0") (default-features #t) (kind 0)))) (hash "10n2cydff4yjn6nvrndaawclill8aj57pz0c4lvq98ll7sz9mwam")))

(define-public crate-cc1101-0.0.3 (crate (name "cc1101") (vers "0.0.3") (deps (list (crate-dep (name "embedded-hal") (req "^0.2.0") (default-features #t) (kind 0)))) (hash "1q6vsii1m5jg2786948yvdpbbcvmxy0p18v4ln6hhqc7bcmzb2ha")))

(define-public crate-cc1101-0.1 (crate (name "cc1101") (vers "0.1.3") (deps (list (crate-dep (name "embedded-hal") (req "^1.0.0") (default-features #t) (kind 0)))) (hash "0bcfh635jbsxyp6jzjpdkhb4hdhah4n3fffjabcgji3j2yk377va") (features (quote (("std"))))))

(define-public crate-cc1101-rust-0.1 (crate (name "cc1101-rust") (vers "0.1.0") (deps (list (crate-dep (name "ioctl-sys") (req "^0.7.1") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2.101") (default-features #t) (kind 0)))) (hash "1nn88qzqvyqm1d9mw6abamwgn5gcrpnm5gracg1cv6gdjnkhq735")))

(define-public crate-cc1101-rust-0.2 (crate (name "cc1101-rust") (vers "0.2.0") (deps (list (crate-dep (name "ioctl-sys") (req "^0.8.0") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2.133") (default-features #t) (kind 0)))) (hash "0i27rp3065h6qn6q5y00xazv17l4vpqyn38vgfc168fpa8fx0lic")))

(define-public crate-cc1101-rust-0.2 (crate (name "cc1101-rust") (vers "0.2.1") (deps (list (crate-dep (name "ioctl-sys") (req "^0.8.0") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2.133") (default-features #t) (kind 0)))) (hash "1linf8jpzlk0sxhssg0v1zv83ik95w35885lra8jlxg0mw0i0cwi")))

