(define-module (crates-io cc es) #:use-module (crates-io))

(define-public crate-cces-0.1 (crate (name "cces") (vers "0.1.0") (deps (list (crate-dep (name "reqwest") (req "^0.11.20") (features (quote ("blocking" "json"))) (default-features #t) (kind 0)) (crate-dep (name "scraper") (req "^0.17.1") (default-features #t) (kind 0)))) (hash "1mcn13w9qixfwj0iw6jjh3gvd7j9wx27dnp93ckkldpl18mgk0i3")))

(define-public crate-cces-0.1 (crate (name "cces") (vers "0.1.1") (deps (list (crate-dep (name "reqwest") (req "^0.11.20") (features (quote ("blocking" "json"))) (default-features #t) (kind 0)) (crate-dep (name "scraper") (req "^0.17.1") (default-features #t) (kind 0)))) (hash "1ilrcl13lz9c9mf0gl3wlxcv0k6zlawli15667lvxzkhiql8w4v6")))

(define-public crate-cces-0.1 (crate (name "cces") (vers "0.1.2") (deps (list (crate-dep (name "reqwest") (req "^0.11.20") (features (quote ("blocking" "json"))) (default-features #t) (kind 0)) (crate-dep (name "scraper") (req "^0.17.1") (default-features #t) (kind 0)))) (hash "08xjxr4wz1qwrmqz1zjlld6wb8y6a093r0wa7hfkj66sjaqqhjgz")))

