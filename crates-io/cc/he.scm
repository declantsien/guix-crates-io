(define-module (crates-io cc he) #:use-module (crates-io))

(define-public crate-cchess-0.1 (crate (name "cchess") (vers "0.1.0") (deps (list (crate-dep (name "iced") (req "^0.4.2") (default-features #t) (kind 0)) (crate-dep (name "log4rs") (req "^1.1.1") (default-features #t) (kind 0)))) (hash "17rsykw73f4968sibf9qsk1amb6jqgk3zf29w1a48b72h7y2fd1m")))

