(define-module (crates-io cc wc) #:use-module (crates-io))

(define-public crate-ccwc-0.1 (crate (name "ccwc") (vers "0.1.0") (deps (list (crate-dep (name "anyhow") (req "^1.0.79") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^4.4.18") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "1zb76d3i9k5i4pxgjkbsv637ny7mj7fw44i4ry0hyrn6n5fpkc9s")))

