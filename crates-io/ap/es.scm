(define-module (crates-io ap es) #:use-module (crates-io))

(define-public crate-apes-0.1 (crate (name "apes") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^3.2.21") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "fuzzy_finder") (req "^0.1.2") (default-features #t) (kind 0)) (crate-dep (name "shellexpand") (req "^2.1.2") (default-features #t) (kind 0)))) (hash "0ppmnr7lmgsqx3mngrb01smcqd9jbjk00b5nr5ax2vpswfbjncb9")))

(define-public crate-apes-0.1 (crate (name "apes") (vers "0.1.1") (deps (list (crate-dep (name "clap") (req "^3.2.21") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "fuzzy_finder") (req "^0.1.2") (default-features #t) (kind 0)) (crate-dep (name "shellexpand") (req "^2.1.2") (default-features #t) (kind 0)))) (hash "124pwvqg3y76z6p8cbmiaq1jywiaa0msmcvwp7gjp83a228iz8bj")))

(define-public crate-apes-0.1 (crate (name "apes") (vers "0.1.2") (deps (list (crate-dep (name "clap") (req "^3.2.21") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "fuzzy_finder") (req "^0.1.2") (default-features #t) (kind 0)) (crate-dep (name "shellexpand") (req "^2.1.2") (default-features #t) (kind 0)))) (hash "1h2q8w0l1d8h8fp1lvydlxixgpbwb3hw1vgssm3pbbvwh0ga4kva")))

(define-public crate-apes-0.1 (crate (name "apes") (vers "0.1.3") (deps (list (crate-dep (name "clap") (req "^3.2.21") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "fuzzy_finder") (req "^0.1.2") (default-features #t) (kind 0)) (crate-dep (name "shellexpand") (req "^2.1.2") (default-features #t) (kind 0)))) (hash "06six3yphlmk9mwxnhjlhn38zjg4lv1lwnnwf43r0wh8i40qqym5")))

(define-public crate-apes-0.1 (crate (name "apes") (vers "0.1.4") (deps (list (crate-dep (name "clap") (req "^3.2.21") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "fuzzy_finder") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "shellexpand") (req "^2.1.2") (default-features #t) (kind 0)))) (hash "1a7m0zxx9gkhl5sc0w6y4skagqbmmijx61sz36a2ygfb9rqbi0zi")))

(define-public crate-apes-0.1 (crate (name "apes") (vers "0.1.5") (deps (list (crate-dep (name "clap") (req "^3.2.21") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "fuzzy_finder") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "shellexpand") (req "^2.1.2") (default-features #t) (kind 0)))) (hash "1r8v18ina7yzpc80raxx4nl4gapkqis3vq3grzdwzh01rjrlimsw")))

(define-public crate-apes-0.1 (crate (name "apes") (vers "0.1.6") (deps (list (crate-dep (name "clap") (req "^3.2.21") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "fuzzy_finder") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "shellexpand") (req "^2.1.2") (default-features #t) (kind 0)))) (hash "0a33gvhwssx0wpzl7rvsaxmzj6x0a622jaxc6j3k3vax07lx2bnx")))

(define-public crate-apes-0.1 (crate (name "apes") (vers "0.1.7") (deps (list (crate-dep (name "clap") (req "^4.0.12") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "fuzzy_finder") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "shellexpand") (req "^2.1.2") (default-features #t) (kind 0)))) (hash "0lyzj8s9ipr0rrk55brirm8kzb9y4p3jq9sczix6m4kc4kab6q8q")))

