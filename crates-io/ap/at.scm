(define-module (crates-io ap at) #:use-module (crates-io))

(define-public crate-apath-0.1 (crate (name "apath") (vers "0.1.0") (deps (list (crate-dep (name "nalgebra") (req "^0.8.2") (default-features #t) (kind 0)))) (hash "09yhnkib6nz4fpcvm677ph7729kq596hx4gbkx32dr8pm4h7gsdz")))

