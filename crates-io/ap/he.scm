(define-module (crates-io ap he) #:use-module (crates-io))

(define-public crate-aphelion-util-0.1 (crate (name "aphelion-util") (vers "0.1.0") (deps (list (crate-dep (name "half") (req "^2.4.0") (features (quote ("num-traits"))) (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2.18") (default-features #t) (kind 0)))) (hash "01d1b15j8pmzi0vfccgg0fnqc95x2lsv893v8a89nhj9pz58crw0")))

(define-public crate-aphelion-util-0.1 (crate (name "aphelion-util") (vers "0.1.1") (deps (list (crate-dep (name "half") (req "^2.4.0") (features (quote ("num-traits"))) (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2.18") (default-features #t) (kind 0)))) (hash "1p7qw7y1p1i690mqwzcra1i7kfdwn2l5rfaydwf2v7j4n3pzy0ya")))

(define-public crate-aphelion-util-0.1 (crate (name "aphelion-util") (vers "0.1.2") (deps (list (crate-dep (name "half") (req "^2.4.0") (features (quote ("num-traits"))) (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2.18") (default-features #t) (kind 0)))) (hash "0si2idgzbpkrf3cd0xncqfghggixngx5gv81sz9553ciip7kjq3v")))

