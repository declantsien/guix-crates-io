(define-module (crates-io ap rs) #:use-module (crates-io))

(define-public crate-aprs-0.1 (crate (name "aprs") (vers "0.1.0") (hash "0l01ffad7jagxjxw92glq9pn1bz2srykjiniyj66r8hyqn9nvws1")))

(define-public crate-aprs-0.2 (crate (name "aprs") (vers "0.2.0") (deps (list (crate-dep (name "approx") (req "^0.2.0") (default-features #t) (kind 2)))) (hash "13gc4jpa2jpfz7wi49s578yl6wchqnl05i0p38xc6jlqlvq0v6hv") (yanked #t)))

(define-public crate-aprs-0.2 (crate (name "aprs") (vers "0.2.1") (deps (list (crate-dep (name "approx") (req "^0.2.0") (default-features #t) (kind 2)))) (hash "06z0yhmdc61g1qxjnrvbhmy6zyaazm5l90lf9z1x1r8d5220cls6")))

(define-public crate-aprs-0.3 (crate (name "aprs") (vers "0.3.1") (deps (list (crate-dep (name "approx") (req "^0.2.0") (default-features #t) (kind 2)))) (hash "1165cdnrljsxf4vl6h1sy7318rld9yzah2qyxdcp7cxpx2vflj8h")))

(define-public crate-aprs-encode-0.1 (crate (name "aprs-encode") (vers "0.1.0") (deps (list (crate-dep (name "arrayvec") (req "^0.7.0") (kind 0)))) (hash "0ni5rmzca1d57g48bz034jkhs72a4dp0pxqlswyfc00h1s4wxsk5")))

(define-public crate-aprs-encode-0.1 (crate (name "aprs-encode") (vers "0.1.1") (deps (list (crate-dep (name "arrayvec") (req "^0.7.0") (kind 0)))) (hash "1qd609ar7vnwsbqlwxjx7rrp275zp7hsb0h5pm479h8x0ldpc80m")))

(define-public crate-aprs-encode-0.1 (crate (name "aprs-encode") (vers "0.1.2") (deps (list (crate-dep (name "arrayvec") (req "^0.7.0") (kind 0)))) (hash "0672119ys5jnass3k4z0fs42v1mbmvcqxg7kc6hjgnsv3nav5nmj")))

(define-public crate-aprs-parser-0.1 (crate (name "aprs-parser") (vers "0.1.0") (deps (list (crate-dep (name "approx") (req "^0.2.0") (default-features #t) (kind 2)) (crate-dep (name "failure") (req "^0.1.1") (default-features #t) (kind 0)) (crate-dep (name "failure_derive") (req "^0.1.1") (default-features #t) (kind 0)))) (hash "1njjlsyg2myk42ww37pacd8r4mppdj84nmma7fwfa5af8r5brbqa")))

(define-public crate-aprs-parser-0.1 (crate (name "aprs-parser") (vers "0.1.1") (deps (list (crate-dep (name "approx") (req "^0.2.0") (default-features #t) (kind 2)) (crate-dep (name "failure") (req "^0.1.1") (default-features #t) (kind 0)) (crate-dep (name "failure_derive") (req "^0.1.1") (default-features #t) (kind 0)))) (hash "028aqx4gda86rcvxa9dplqn6idnhvqggb7m55p2qckrkpvq6gyvn")))

(define-public crate-aprs-parser-0.2 (crate (name "aprs-parser") (vers "0.2.0") (deps (list (crate-dep (name "approx") (req "^0.5.1") (default-features #t) (kind 2)) (crate-dep (name "thiserror") (req "^1.0.32") (default-features #t) (kind 0)))) (hash "04x8bab2309arl2r5cavz65fp2y8nbmcqbvyp2mdv4h45nxzzqr7") (rust-version "1.52.0")))

(define-public crate-aprs-parser-0.3 (crate (name "aprs-parser") (vers "0.3.0") (deps (list (crate-dep (name "approx") (req "^0.5.1") (default-features #t) (kind 2)) (crate-dep (name "thiserror") (req "^1.0.37") (default-features #t) (kind 0)))) (hash "1kq8g6dr831zp0bxadzqzmaq8ilyvc5p3qq619gbdhh7anni59yk") (rust-version "1.52.0")))

(define-public crate-aprs-parser-0.4 (crate (name "aprs-parser") (vers "0.4.0") (deps (list (crate-dep (name "approx") (req "^0.5.1") (default-features #t) (kind 2)) (crate-dep (name "thiserror") (req "^1.0.38") (default-features #t) (kind 0)))) (hash "12rm0qisgj3ha53zfz8l70zlf3cdkhvbww144s2qqgxp4y0lav3p") (rust-version "1.60.0")))

(define-public crate-aprs-parser-0.4 (crate (name "aprs-parser") (vers "0.4.1") (deps (list (crate-dep (name "approx") (req "^0.5.1") (default-features #t) (kind 2)) (crate-dep (name "thiserror") (req "^1.0.56") (default-features #t) (kind 0)))) (hash "0187bjfpysngc2g5bawn1740gxhlq3ppdln0bcc056zsabpv3xm5") (rust-version "1.60.0")))

(define-public crate-aprs-parser-0.4 (crate (name "aprs-parser") (vers "0.4.2") (deps (list (crate-dep (name "approx") (req "^0.5.1") (default-features #t) (kind 2)) (crate-dep (name "thiserror") (req "^1.0.57") (default-features #t) (kind 0)))) (hash "01v27qj6ai8l7bwl58rh1h3zfyfq04x3p2ybhi81nv8if8mq2jyk") (rust-version "1.60.0")))

(define-public crate-aprshttp-0.1 (crate (name "aprshttp") (vers "0.1.0") (deps (list (crate-dep (name "aprs-encode") (req "^0.1.2") (default-features #t) (kind 0)) (crate-dep (name "arrayvec") (req "^0.7") (default-features #t) (kind 0)) (crate-dep (name "callpass") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^2.33") (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.11") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.0") (features (quote ("macros" "rt-multi-thread"))) (default-features #t) (kind 0)))) (hash "03jcwhvkvzrns39lhqd9xrg3p8cmzvhpa09gi8zspwsvx91s9jr7")))

(define-public crate-aprsproxy-0.3 (crate (name "aprsproxy") (vers "0.3.5") (deps (list (crate-dep (name "chrono") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "env_logger") (req "^0.9") (default-features #t) (kind 0)) (crate-dep (name "futures") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.4") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.11") (kind 0)) (crate-dep (name "structopt") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1") (features (quote ("net" "macros"))) (default-features #t) (kind 0)) (crate-dep (name "tokio-test") (req "^0.4") (default-features #t) (kind 2)) (crate-dep (name "trust-dns-resolver") (req "^0.21") (default-features #t) (kind 0)))) (hash "1hw6cwhs8cdmgrzj7g83wdczddgcqz912idqy8hzxzcww5rgpdqa")))

