(define-module (crates-io ap a1) #:use-module (crates-io))

(define-public crate-apa102-spi-0.1 (crate (name "apa102-spi") (vers "0.1.0") (deps (list (crate-dep (name "embedded-hal") (req "^0.2.1") (default-features #t) (kind 0)) (crate-dep (name "smart-leds-trait") (req "^0.1") (default-features #t) (kind 0)))) (hash "1fxilyk1mgdv25y11g8apazjddc2446bpcczhciwnn2sqc2kl05g")))

(define-public crate-apa102-spi-0.2 (crate (name "apa102-spi") (vers "0.2.0") (deps (list (crate-dep (name "embedded-hal") (req "^0.2.1") (default-features #t) (kind 0)) (crate-dep (name "smart-leds-trait") (req "^0.2") (default-features #t) (kind 0)))) (hash "1mcg2l1669dbqkv1cgxacq0s5nnfpwjgw29lwbyavgnaadg30k14")))

(define-public crate-apa102-spi-0.2 (crate (name "apa102-spi") (vers "0.2.1") (deps (list (crate-dep (name "embedded-hal") (req "^0.2.1") (default-features #t) (kind 0)) (crate-dep (name "smart-leds-trait") (req "^0.2") (default-features #t) (kind 0)))) (hash "12viz5hp6yp75qlwdmdy8xrfvb355fqksic6cwkf9cci3z84mdkx")))

(define-public crate-apa102-spi-0.3 (crate (name "apa102-spi") (vers "0.3.0") (deps (list (crate-dep (name "embedded-hal") (req "^0.2.1") (default-features #t) (kind 0)) (crate-dep (name "smart-leds-trait") (req "^0.2") (default-features #t) (kind 0)))) (hash "04h1i11aypl2xrcv4y13cr4qrj92175p6kf8h7va4s0rdg7zyz5j")))

(define-public crate-apa102-spi-0.3 (crate (name "apa102-spi") (vers "0.3.1") (deps (list (crate-dep (name "embedded-hal") (req "^0.2.1") (default-features #t) (kind 0)) (crate-dep (name "smart-leds-trait") (req "^0.2") (default-features #t) (kind 0)))) (hash "17dznj4plj66wmf9klfwn17jwd62wzk8bnq0a6k7jffkbzql7la9")))

(define-public crate-apa102-spi-0.3 (crate (name "apa102-spi") (vers "0.3.2") (deps (list (crate-dep (name "embedded-hal") (req "^0.2.1") (default-features #t) (kind 0)) (crate-dep (name "smart-leds-trait") (req "^0.2") (default-features #t) (kind 0)))) (hash "18sp9i7r7cl4mfq3kajiz36b7al2j0nkkp72972mpbiazn8dcb6r")))

(define-public crate-apa102-spi-0.4 (crate (name "apa102-spi") (vers "0.4.0") (deps (list (crate-dep (name "embedded-hal") (req "^1.0.0") (default-features #t) (kind 0)) (crate-dep (name "smart-leds-trait") (req "^0.3") (default-features #t) (kind 0)))) (hash "04lbcd1pcqalib9vj8gaap7zln9v7p2dp27qyzmkyh9v5sqvsyvl")))

