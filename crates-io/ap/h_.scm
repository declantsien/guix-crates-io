(define-module (crates-io ap h_) #:use-module (crates-io))

(define-public crate-aph_disjoint_set-0.1 (crate (name "aph_disjoint_set") (vers "0.1.0") (deps (list (crate-dep (name "itertools") (req "^0.10.5") (default-features #t) (kind 2)) (crate-dep (name "rstest") (req "^0.17.0") (kind 2)))) (hash "198ff9ghdryv53hjhrw19bizsbi1c2899qyrml5x0h4wk6gy2fx4") (rust-version "1.70")))

(define-public crate-aph_disjoint_set-0.1 (crate (name "aph_disjoint_set") (vers "0.1.1") (deps (list (crate-dep (name "itertools") (req "^0.10.5") (default-features #t) (kind 2)) (crate-dep (name "rstest") (req "^0.17.0") (kind 2)))) (hash "023wjdk07c4chsb4hlkjcil1ira4q5vkn6xdmr25prl3b1y6x4gc") (rust-version "1.70")))

