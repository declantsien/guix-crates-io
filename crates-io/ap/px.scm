(define-module (crates-io ap px) #:use-module (crates-io))

(define-public crate-appx-0.1 (crate (name "appx") (vers "0.1.0") (deps (list (crate-dep (name "wchar") (req "^0.6.1") (default-features #t) (target "cfg(windows)") (kind 0)) (crate-dep (name "wchar") (req "^0.6.1") (default-features #t) (kind 2)) (crate-dep (name "winapi") (req "^0.3.9") (features (quote ("minwindef" "ntdef" "winerror" "combaseapi" "handleapi" "processthreadsapi" "synchapi" "winbase" "winnt" "winreg"))) (default-features #t) (target "cfg(windows)") (kind 0)))) (hash "0rk86xg0w0m5yl635787lpvh836pzb8xw1rrajkll4la3l7czhaz")))

