(define-module (crates-io ap de) #:use-module (crates-io))

(define-public crate-apdex-0.1 (crate (name "apdex") (vers "0.1.0") (deps (list (crate-dep (name "yansi") (req "^0.4.0") (optional #t) (default-features #t) (kind 0)))) (hash "1h8ipib75knrxwcxxdrpfaxvaz2m4is6b9xjykzigkbs07r9bifn") (features (quote (("default" "yansi"))))))

(define-public crate-apdex-0.1 (crate (name "apdex") (vers "0.1.1") (deps (list (crate-dep (name "yansi") (req "^0.4.0") (optional #t) (default-features #t) (kind 0)))) (hash "03mp3h30ap5xixcw88xlmpsn2lqfjji1xgbjhyd1c1za0ric2msz") (features (quote (("default" "yansi"))))))

