(define-module (crates-io ap ro) #:use-module (crates-io))

(define-public crate-aprountzos_art-0.1 (crate (name "aprountzos_art") (vers "0.1.0") (hash "0yc20yrbjpkigb6mvhypa2k69vqbil734vx51gqg6j05ra0imv39")))

(define-public crate-aprox-0.0.0 (crate (name "aprox") (vers "0.0.0") (hash "1g8kx04f8kj4qq5pqnl4x4i35wn064779bb65g84b4fzl6winsbj")))

(define-public crate-aprox_derive-0.1 (crate (name "aprox_derive") (vers "0.1.0") (deps (list (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2.0") (default-features #t) (kind 0)))) (hash "0i1hh9k4dcdv2r8jyfrcbkpf7j06rifja2jn3amvi2f38l12560n")))

(define-public crate-aprox_derive-0.2 (crate (name "aprox_derive") (vers "0.2.0") (deps (list (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2.0") (default-features #t) (kind 0)))) (hash "08f5qlxwqihx6nvq6rf946pgx55jqv8j6b3nnqw5p2idp19wmgpy") (yanked #t)))

(define-public crate-aprox_derive-0.2 (crate (name "aprox_derive") (vers "0.2.1") (deps (list (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2.0") (default-features #t) (kind 0)))) (hash "14b0iz5yxgn0ys4fvz26pzqwxgh7m9km4qm7hmwm4i3n179waafy")))

(define-public crate-aprox_derive-0.3 (crate (name "aprox_derive") (vers "0.3.0") (deps (list (crate-dep (name "proc-macro2") (req "^1.0.66") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2.0") (default-features #t) (kind 0)))) (hash "0sp2s1p4vs1j423mkq4nx07wdx65fxazzka76wqya3xjjdff953q")))

(define-public crate-aprox_derive-0.3 (crate (name "aprox_derive") (vers "0.3.1") (deps (list (crate-dep (name "proc-macro2") (req "^1.0.66") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2.0") (default-features #t) (kind 0)))) (hash "0ygd42n17sdbff3bh0c9nlimq7rdmqf157vbrlh9vvnxivszikx2")))

(define-public crate-aprox_derive-0.3 (crate (name "aprox_derive") (vers "0.3.2") (deps (list (crate-dep (name "proc-macro2") (req "^1.0.66") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2.0") (default-features #t) (kind 0)))) (hash "1xip05jkq2sx6p2iijl811mgz7jw64rhfvab08px0d7bmnj013c4")))

(define-public crate-aprox_eq-0.1 (crate (name "aprox_eq") (vers "0.1.0") (deps (list (crate-dep (name "aprox_derive") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "1n46pixyai78dg2r1b2ir88l349c833pacqivzpqb93xihdaizh7")))

(define-public crate-aprox_eq-1 (crate (name "aprox_eq") (vers "1.1.0") (deps (list (crate-dep (name "aprox_derive") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "0b77221qpxx8wqv2xvdv375kna0i1k78pmr7ml0zvb670wp4s4q7")))

(define-public crate-aprox_eq-1 (crate (name "aprox_eq") (vers "1.2.0") (deps (list (crate-dep (name "aprox_derive") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "0bg8c7sqdvs51wwv6d9d4yhqr2pcgnlx0xa9x2pg30rbrc9jh86i") (yanked #t)))

(define-public crate-aprox_eq-1 (crate (name "aprox_eq") (vers "1.2.1") (deps (list (crate-dep (name "aprox_derive") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "0j872laqbvr17cnxapvkn4r0hnlfizw2bmw481pd4kf4m1w0ingh")))

(define-public crate-aprox_eq-1 (crate (name "aprox_eq") (vers "1.3.0") (deps (list (crate-dep (name "aprox_derive") (req "^0.2.1") (default-features #t) (kind 0)))) (hash "0wh4py9a5i1icl6qzvaxzlsp7xpvvid9n9wzhp98sv9lpiv09dik")))

(define-public crate-aprox_eq-1 (crate (name "aprox_eq") (vers "1.4.0") (deps (list (crate-dep (name "aprox_derive") (req "^0.2.1") (default-features #t) (kind 0)))) (hash "1n32lcbnxqdk6c0w6byldhwislldqd6j55pkvhd4597y8kx0wfck")))

(define-public crate-aprox_eq-1 (crate (name "aprox_eq") (vers "1.5.0") (deps (list (crate-dep (name "aprox_derive") (req "^0.2.1") (default-features #t) (kind 0)))) (hash "1ixl7vz3qr994x4r2wi02wifd1pmmv9qs2vqvqx7xc7ca1mgk1lc")))

(define-public crate-aprox_eq-1 (crate (name "aprox_eq") (vers "1.6.0") (deps (list (crate-dep (name "aprox_derive") (req "^0.3.0") (default-features #t) (kind 0)))) (hash "1ygd9b7lzm793dg1k7jby9xhfh7622b66xzrwsnvjzq8di8mnw9d")))

(define-public crate-aprox_eq-1 (crate (name "aprox_eq") (vers "1.6.1") (deps (list (crate-dep (name "aprox_derive") (req "^0.3.0") (default-features #t) (kind 0)))) (hash "0ph8ylmlac4h5lpm8f0zcxwhs9nl61c01fk1wgj93j043qypz4c7")))

(define-public crate-aprox_eq-1 (crate (name "aprox_eq") (vers "1.6.2") (deps (list (crate-dep (name "aprox_derive") (req "^0.3.2") (default-features #t) (kind 0)))) (hash "1y2f66nx9mv9x5g1k3p4058lww029brk0553yan0ylnmqzi6w0fp")))

(define-public crate-aprox_eq-2 (crate (name "aprox_eq") (vers "2.0.0") (deps (list (crate-dep (name "aprox_derive") (req "^0.3.2") (default-features #t) (kind 0)))) (hash "0nsid6n2bd310mjnljnmxpm8bnrarw62wjfkc00jdyxzk4jrlxmk")))

(define-public crate-aprox_eq-2 (crate (name "aprox_eq") (vers "2.0.1") (deps (list (crate-dep (name "aprox_derive") (req "^0.3.2") (default-features #t) (kind 0)))) (hash "0vr0q70m63ali0808nwc27f3rcav4h5m5xsyzbfavygdjrh9jy4i")))

