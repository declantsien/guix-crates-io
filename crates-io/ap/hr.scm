(define-module (crates-io ap hr) #:use-module (crates-io))

(define-public crate-aphreco-0.1 (crate (name "aphreco") (vers "0.1.0") (deps (list (crate-dep (name "chrono") (req "^0.4.19") (default-features #t) (kind 0)) (crate-dep (name "ndarray") (req "^0.14.0") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2") (features (quote ("i128"))) (kind 0)) (crate-dep (name "rand") (req "^0.8.4") (default-features #t) (kind 0)) (crate-dep (name "rust_decimal") (req "^1.19.0") (default-features #t) (kind 0)))) (hash "1vzfcn89qwwgw2jcyxk5wl9p0pkbb8ql8wchxrx8y5mybyqn5mr2")))

(define-public crate-aphreco-0.1 (crate (name "aphreco") (vers "0.1.1") (deps (list (crate-dep (name "chrono") (req "^0.4.19") (default-features #t) (kind 0)) (crate-dep (name "ndarray") (req "^0.14.0") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2") (features (quote ("i128"))) (kind 0)) (crate-dep (name "rand") (req "^0.8.4") (default-features #t) (kind 0)) (crate-dep (name "rust_decimal") (req "^1.19.0") (default-features #t) (kind 0)))) (hash "1ns9jxhnzxdlkj0qh2d2sljz30bn4zsh41plcg6snf50yhg6bwqs")))

(define-public crate-aphreco-0.1 (crate (name "aphreco") (vers "0.1.2") (deps (list (crate-dep (name "chrono") (req "^0.4.19") (default-features #t) (kind 0)) (crate-dep (name "ndarray") (req "^0.14.0") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2") (features (quote ("i128"))) (kind 0)) (crate-dep (name "rand") (req "^0.8.4") (default-features #t) (kind 0)) (crate-dep (name "rust_decimal") (req "^1.19.0") (default-features #t) (kind 0)))) (hash "0n34w5v3ymqd06rxv7jy1bm9lnmz06x44g9caxd3f3gfavnwrj10")))

(define-public crate-aphreco-0.1 (crate (name "aphreco") (vers "0.1.3") (deps (list (crate-dep (name "chrono") (req "^0.4.19") (default-features #t) (kind 0)) (crate-dep (name "ndarray") (req "^0.14.0") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2") (features (quote ("i128"))) (kind 0)) (crate-dep (name "rand") (req "^0.8.4") (default-features #t) (kind 0)) (crate-dep (name "rust_decimal") (req "^1.19.0") (default-features #t) (kind 0)))) (hash "084223dd2giv2dby9fd4gx7qqijrb8w34rz5qd73gln2bnq2qfjy")))

(define-public crate-aphreco-0.1 (crate (name "aphreco") (vers "0.1.4") (deps (list (crate-dep (name "chrono") (req "^0.4.19") (default-features #t) (kind 0)) (crate-dep (name "ndarray") (req "^0.14.0") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2") (features (quote ("i128"))) (kind 0)) (crate-dep (name "rand") (req "^0.8.4") (default-features #t) (kind 0)) (crate-dep (name "rust_decimal") (req "^1.19.0") (default-features #t) (kind 0)))) (hash "05x887abm1fjz2sccxlxdswgfl2j0yy5f6k6p6c8pwgr4pksbp2i")))

(define-public crate-aphreco-0.1 (crate (name "aphreco") (vers "0.1.5") (deps (list (crate-dep (name "ndarray") (req "^0.14.0") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2") (features (quote ("i128"))) (kind 0)) (crate-dep (name "rand") (req "^0.8.4") (default-features #t) (kind 0)) (crate-dep (name "rust_decimal") (req "^1.19.0") (default-features #t) (kind 0)))) (hash "1w6nh7fwh9dd3dvr5d5h4p11y6219w3rqfql3xvsiwifqm2wjhzh")))

(define-public crate-aphreco-0.1 (crate (name "aphreco") (vers "0.1.6") (deps (list (crate-dep (name "ndarray") (req "^0.14.0") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2") (features (quote ("i128"))) (kind 0)) (crate-dep (name "rand") (req "^0.8.4") (default-features #t) (kind 0)) (crate-dep (name "rust_decimal") (req "^1.19.0") (default-features #t) (kind 0)))) (hash "0naylngp53lp076pykyvll2ccd5bf4z2lfn1zp8snwcx09jm32h6")))

(define-public crate-aphreco-0.1 (crate (name "aphreco") (vers "0.1.7") (deps (list (crate-dep (name "ndarray") (req "^0.14.0") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2") (features (quote ("i128"))) (kind 0)) (crate-dep (name "rand") (req "^0.8.4") (default-features #t) (kind 0)) (crate-dep (name "rust_decimal") (req "^1.19.0") (default-features #t) (kind 0)))) (hash "186kakv5dinl8i8kyxxlgiwz9axkhfdiqfqywsq7hdwhwvmiw8m9")))

(define-public crate-aphrora-0.1 (crate (name "aphrora") (vers "0.1.0") (hash "181p4nchwwq6k1c1qzrkg56kz4hxn20lwbxwp59rldjq6yxw7fhk")))

(define-public crate-aphrora-0.1 (crate (name "aphrora") (vers "0.1.1") (hash "1zldwhrw01ky4glj1mqshdvs79slfc6sdyhh4x9aqiz85sl6pr8c")))

(define-public crate-aphrora-0.2 (crate (name "aphrora") (vers "0.2.0") (hash "1bj6x55cq5zmv38warq689j73spclp040ic95d4wbxghzfpdw472")))

