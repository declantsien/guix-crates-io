(define-module (crates-io ap if) #:use-module (crates-io))

(define-public crate-apiflash-0.1 (crate (name "apiflash") (vers "0.1.0") (hash "154wwrj7jz3ggy9myqgq2vhl7ba1a0kgjmm3m4l46hb8znvck072")))

(define-public crate-apiflash-0.1 (crate (name "apiflash") (vers "0.1.1") (hash "1rzr4r2289hwdh0ic6rp29vir5fq3qy70zija2f3nbagvk6xnlrb")))

(define-public crate-apiflash-0.1 (crate (name "apiflash") (vers "0.1.4") (hash "0xfl5pq3wq60y15vx853m8v62nra6xnx0bb222vh5c5vx6my0jqc")))

(define-public crate-apify-0.1 (crate (name "apify") (vers "0.1.0") (deps (list (crate-dep (name "apify-client") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.7.3") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.116") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.57") (default-features #t) (kind 0)) (crate-dep (name "tokio-test") (req "^0.2.0") (default-features #t) (kind 2)))) (hash "00ih5qj4ii69h1f1vmhd7nbkni5v2wnxq6yfps4nc0m2fyw7vmj0")))

(define-public crate-apify-0.1 (crate (name "apify") (vers "0.1.1") (deps (list (crate-dep (name "apify-client") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.7.3") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.116") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.57") (default-features #t) (kind 0)) (crate-dep (name "tokio-test") (req "^0.2.0") (default-features #t) (kind 2)))) (hash "1gpgx1l5yqgr260yx6qsaj5f8gbjgjqj1a42n7gn05s11jak19bj")))

(define-public crate-apify-client-0.1 (crate (name "apify-client") (vers "0.1.0") (deps (list (crate-dep (name "query_params") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.10.4") (features (quote ("json"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.110") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.55") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^0.2") (features (quote ("full"))) (default-features #t) (kind 0)) (crate-dep (name "tokio-test") (req "^0.2.1") (default-features #t) (kind 2)))) (hash "1g8jh5bwyhdafqbcvg65znzy6ss2bairfirnr3ajr3259mdpjyss")))

(define-public crate-apify-client-0.2 (crate (name "apify-client") (vers "0.2.0") (deps (list (crate-dep (name "query_params") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.11") (features (quote ("json"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.110") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.55") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1") (features (quote ("full"))) (default-features #t) (kind 0)) (crate-dep (name "tokio-test") (req "^0.4") (default-features #t) (kind 2)))) (hash "06ncarkai6ll8va48xyijpg3i9pb40q7mp9jv6yrqkkjqqsmsjbh")))

