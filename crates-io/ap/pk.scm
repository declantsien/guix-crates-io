(define-module (crates-io ap pk) #:use-module (crates-io))

(define-public crate-appkit-0.1 (crate (name "appkit") (vers "0.1.0") (hash "12dc8s597g9rc5sqd3zjk2p417l2dig43va3137qfmc0anxcrmjn")))

(define-public crate-appkit-derive-0.1 (crate (name "appkit-derive") (vers "0.1.0") (deps (list (crate-dep (name "quote") (req "^1.0.2") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.14") (default-features #t) (kind 0)))) (hash "0l3z1cfgq56vad73p304mx2l9skdndfgz3q6avypgiwbibs24qkx")))

(define-public crate-appkit-nsworkspace-bindings-0.1 (crate (name "appkit-nsworkspace-bindings") (vers "0.1.0") (deps (list (crate-dep (name "bindgen") (req "^0.59") (default-features #t) (target "cfg(target_os = \"macos\")") (kind 1)) (crate-dep (name "objc") (req "^0.2") (default-features #t) (target "cfg(target_os = \"macos\")") (kind 0)))) (hash "0jcqdx0hbhplichmbmzxm7whxa5d3wdfh3n7jqqry0y5jqv4czsf")))

(define-public crate-appkit-nsworkspace-bindings-0.1 (crate (name "appkit-nsworkspace-bindings") (vers "0.1.2") (deps (list (crate-dep (name "bindgen") (req "^0.68.1") (default-features #t) (target "cfg(target_os = \"macos\")") (kind 1)) (crate-dep (name "objc") (req "^0.2") (default-features #t) (target "cfg(target_os = \"macos\")") (kind 0)))) (hash "1mrdf28533yizayb5l50bcbpksz7y1dbg6k80cna1kq4hs9q48q6")))

