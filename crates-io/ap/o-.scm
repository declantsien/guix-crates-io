(define-module (crates-io ap o-) #:use-module (crates-io))

(define-public crate-apo-rs-0.1 (crate (name "apo-rs") (vers "0.1.0") (deps (list (crate-dep (name "ema-rs") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "ta-common") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "1fcn8y9nsbcfdjqik8w3vyrfwsywa2vqq8iddp0k83q87372a34s")))

