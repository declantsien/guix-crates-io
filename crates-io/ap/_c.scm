(define-module (crates-io ap _c) #:use-module (crates-io))

(define-public crate-ap_calc-0.1 (crate (name "ap_calc") (vers "0.1.0") (deps (list (crate-dep (name "fraction") (req "^0.12.1") (default-features #t) (kind 0)) (crate-dep (name "num") (req "^0.4.0") (default-features #t) (kind 0)))) (hash "09cla8hafj1xak9w4wxxfj2d1i3alzjvhw0h79l6zl3nm5sv4929")))

(define-public crate-ap_calc-0.1 (crate (name "ap_calc") (vers "0.1.1") (deps (list (crate-dep (name "fraction") (req "^0.12.1") (default-features #t) (kind 0)) (crate-dep (name "num") (req "^0.4.0") (default-features #t) (kind 0)))) (hash "12rpa6dlxr31w70z48lbbya0kappkvg3nw42vh795wlwhlsh1w7d")))

