(define-module (crates-io ap ir) #:use-module (crates-io))

(define-public crate-apir-0.1 (crate (name "apir") (vers "0.1.0") (deps (list (crate-dep (name "async-trait") (req "^0.1.42") (default-features #t) (kind 0)) (crate-dep (name "futures") (req "^0.3.5") (default-features #t) (kind 0)))) (hash "1qpm06688ann8mg9saxd300z3q5byjz4kgqbyvxppakyziqr8f2m")))

