(define-module (crates-io ap ei) #:use-module (crates-io))

(define-public crate-apeiron-0.0.0 (crate (name "apeiron") (vers "0.0.0") (hash "0y39v4pqislyixzi8ygmwpkmdiqbbzqhad82cmm6iqw3yxbzljsm")))

(define-public crate-apeiron-cli-0.0.0 (crate (name "apeiron-cli") (vers "0.0.0") (hash "0l4f14qkhlc8r4nxzrz6dzfy31z6x7d3yjrrjwksnp1k5kk8g7is")))

