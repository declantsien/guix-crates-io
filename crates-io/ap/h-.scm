(define-module (crates-io ap h-) #:use-module (crates-io))

(define-public crate-aph-cli-0.1 (crate (name "aph-cli") (vers "0.1.0") (deps (list (crate-dep (name "aph") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "15jm3vmwd7117w7qgp4c22sskkjsdbari1a9p9adi33z55mlbycy")))

(define-public crate-aph-cli-0.2 (crate (name "aph-cli") (vers "0.2.0") (deps (list (crate-dep (name "aph") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^3.1.6") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "11v25bdvh4qyj79xcxgvwjzrjzkv6k28ww7w6d1p9f1d9jlja8qd")))

