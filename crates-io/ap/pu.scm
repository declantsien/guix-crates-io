(define-module (crates-io ap pu) #:use-module (crates-io))

(define-public crate-apputils-0.1 (crate (name "apputils") (vers "0.1.0") (hash "1d499xdl23wv4rk2alykfd8bwxbqgl40zkpn9pc3a64bzgn6b79m")))

(define-public crate-apputils-0.1 (crate (name "apputils") (vers "0.1.1") (hash "0bnzr5fhmhcr89914d32b8bgc36q8lwk4mwcx0v1pjisxdkhx24d")))

(define-public crate-apputils-0.1 (crate (name "apputils") (vers "0.1.2") (hash "0nwk13fw1729fc6rgz2hdlx5viywbmblr43myk01a04d468blwr2")))

(define-public crate-apputils-0.1 (crate (name "apputils") (vers "0.1.3") (hash "0pyh68jrl7rbwvmwlffb7rgz4yi6ysl8gn6hgvssaff0mfq2v0yy")))

(define-public crate-apputils-0.1 (crate (name "apputils") (vers "0.1.4") (hash "1bj8vf2ammvw7n84szchwbywn70h3sivy4q1q0wsm9dii38inkdi") (yanked #t)))

(define-public crate-apputils-0.1 (crate (name "apputils") (vers "0.1.5") (hash "06nbvnmi0xbg29xx8hxlff1ixvl92dg0g9x18bjw6f4k7850q6hb")))

(define-public crate-apputils-0.1 (crate (name "apputils") (vers "0.1.6") (hash "1k5mm41ngkmm5x6xwp7qnvddzbpq2ixq0szf2d7c7l4g1scr8kp1")))

