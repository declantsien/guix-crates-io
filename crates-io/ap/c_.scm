(define-module (crates-io ap c_) #:use-module (crates-io))

(define-public crate-apc_guessing_game-0.1 (crate (name "apc_guessing_game") (vers "0.1.0") (deps (list (crate-dep (name "rand") (req "^0.8.3") (default-features #t) (kind 0)))) (hash "0ph3vlh641lcrzpiz0kxaa16vqxwm6lxlj9zp2xx194s1ppa9068")))

