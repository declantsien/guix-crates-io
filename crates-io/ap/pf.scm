(define-module (crates-io ap pf) #:use-module (crates-io))

(define-public crate-appfinder-0.1 (crate (name "appfinder") (vers "0.1.0") (deps (list (crate-dep (name "cfg-if") (req "^1.0.0") (default-features #t) (kind 0)) (crate-dep (name "freedesktop-desktop-entry") (req "^0.5.0") (default-features #t) (target "cfg(target_os = \"linux\")") (kind 0)))) (hash "0ar508yjklnl9mh9yl0az860b3bl77ja9qk7qpgcb2n5wzyrb1zi")))

(define-public crate-appfinder-0.1 (crate (name "appfinder") (vers "0.1.1") (deps (list (crate-dep (name "cfg-if") (req "^1.0.0") (default-features #t) (kind 0)) (crate-dep (name "freedesktop-desktop-entry") (req "^0.5.0") (default-features #t) (target "cfg(target_os = \"linux\")") (kind 0)))) (hash "02gq72a06cz0afgp1plknz461xbsh6zqqsc0qgs0650qypxahyqb")))

(define-public crate-appfinder-0.1 (crate (name "appfinder") (vers "0.1.2") (deps (list (crate-dep (name "cfg-if") (req "^1.0.0") (default-features #t) (kind 0)) (crate-dep (name "freedesktop-desktop-entry") (req "^0.5.0") (default-features #t) (target "cfg(target_os = \"linux\")") (kind 0)))) (hash "1dv03cd0giv7kw3qhmzams5akvhba6kpc070jkvxssmyyrm5v161")))

