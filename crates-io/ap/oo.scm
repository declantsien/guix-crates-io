(define-module (crates-io ap oo) #:use-module (crates-io))

(define-public crate-apool-0.1 (crate (name "apool") (vers "0.1.0") (deps (list (crate-dep (name "smallvec") (req "^1.6.1") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.10.0") (features (quote ("rt" "sync"))) (optional #t) (default-features #t) (kind 0)))) (hash "1wskk5xhyszndfiqyc7x1x2v2r0y8b85pxr943s47s1rmnicbgd2") (features (quote (("default" "tokio"))))))

(define-public crate-apool-0.1 (crate (name "apool") (vers "0.1.1") (deps (list (crate-dep (name "smallvec") (req "^1.6.1") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.10.0") (features (quote ("rt" "sync"))) (optional #t) (default-features #t) (kind 0)))) (hash "0x1200vn94fwjg8qipny2xnv34p7ha0iymxkvham4vyh8c20my4i") (features (quote (("default" "tokio"))))))

(define-public crate-apool-0.1 (crate (name "apool") (vers "0.1.2") (deps (list (crate-dep (name "smallvec") (req "^1.6.1") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.10.0") (features (quote ("rt" "sync"))) (optional #t) (default-features #t) (kind 0)))) (hash "1i9fi0f57m2h8ynra12akdmdfpcij66fks8sq7sr9q6410xxl25d") (features (quote (("default" "tokio"))))))

(define-public crate-apool-0.1 (crate (name "apool") (vers "0.1.3") (deps (list (crate-dep (name "smallvec") (req "^1.6.1") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.10.0") (features (quote ("rt" "sync"))) (optional #t) (default-features #t) (kind 0)))) (hash "18wbkivzg107qhv43z1f8jdqls0l5gh8x2gpvjcmwlc90051xplc") (features (quote (("default" "tokio"))))))

