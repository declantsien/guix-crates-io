(define-module (crates-io ap ds) #:use-module (crates-io))

(define-public crate-apds9151-0.1 (crate (name "apds9151") (vers "0.1.0") (deps (list (crate-dep (name "embedded-hal") (req "^0.2.7") (default-features #t) (kind 0)))) (hash "12qbx0dk196790b2l046skmysj1cccmzpg9kam9adgpc6pxs75a0")))

(define-public crate-apds9151-0.1 (crate (name "apds9151") (vers "0.1.1") (deps (list (crate-dep (name "embedded-hal") (req "^0.2.7") (default-features #t) (kind 0)))) (hash "1c4yl75q8pax6h1jws9ki6z7chcfxy3g5fsl41qxq3vbnqrdxad8")))

(define-public crate-apds9253-1 (crate (name "apds9253") (vers "1.0.0") (deps (list (crate-dep (name "embedded-hal") (req "^0.2.7") (default-features #t) (kind 0)) (crate-dep (name "embedded-hal-mock") (req "^0.9.0") (default-features #t) (kind 2)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)))) (hash "0yiggq7pi78szkmp9g4dv4ywv2nciagw5xc6p5cmh3j9s44gzwd2")))

(define-public crate-apds9960-0.1 (crate (name "apds9960") (vers "0.1.0") (deps (list (crate-dep (name "embedded-hal") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "embedded-hal-mock") (req "^0.4") (default-features #t) (kind 2)) (crate-dep (name "linux-embedded-hal") (req "^0.2") (default-features #t) (kind 2)) (crate-dep (name "nb") (req "^0.1") (default-features #t) (kind 0)))) (hash "11dz8dsba0wmj3i7d1069yqykfl96lflsiin55dppdrh5bzs7043")))

