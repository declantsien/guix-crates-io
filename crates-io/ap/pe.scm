(define-module (crates-io ap pe) #:use-module (crates-io))

(define-public crate-appear-1 (crate (name "appear") (vers "1.2.3") (hash "0d3w22zf1dz427dynhf5afqarwjlys3yhvask8jvd9klyfk9arql")))

(define-public crate-appearust-0.0.1 (crate (name "appearust") (vers "0.0.1") (hash "0gd1disfmikx1fkh15qxmfir7062biq18qs021a7h9w7wvgfgqxz")))

(define-public crate-appel-0.0.0 (crate (name "appel") (vers "0.0.0") (hash "0mg6ns6rgpdvvcdsrjrqdrjnai4bj2fygj1f2mnfbw9rlxjs2h3i") (yanked #t)))

(define-public crate-append-0.1 (crate (name "append") (vers "0.1.0") (hash "05lngyax8q62r0qnv5080pzvh7dj34jl46v8spgjld1d2prdqrzr")))

(define-public crate-append-0.2 (crate (name "append") (vers "0.2.0") (hash "0njqklx2v4xxara1r11bhrn8bfhm8hh68rb2i84mw3k5w7670h9z")))

(define-public crate-append-if-0.1 (crate (name "append-if") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^3.2.20") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.6.0") (default-features #t) (kind 0)))) (hash "0i3ls71nsy9r5iv4v2ffq69kdzbx3dfrxjbjbwhbawmyl7slndcl")))

(define-public crate-append-log-0.1 (crate (name "append-log") (vers "0.1.0-dev") (deps (list (crate-dep (name "crc32fast") (req "^1.1.2") (default-features #t) (kind 0)) (crate-dep (name "tempfile") (req "^3.0.7") (default-features #t) (kind 2)))) (hash "0ysscccssgxlhb15pm8yk1hm68nk1zmd8brznan9q7h93psdl6mi")))

(define-public crate-append-log-0.1 (crate (name "append-log") (vers "0.1.1-dev") (deps (list (crate-dep (name "crc32fast") (req "^1.1.2") (default-features #t) (kind 0)) (crate-dep (name "tempfile") (req "^3.0.7") (default-features #t) (kind 2)))) (hash "1wx7n4kcfhf3smcmylsav5l9af2q7lkwcg83bqr7shvf8p2fjhq4")))

(define-public crate-append-log-0.1 (crate (name "append-log") (vers "0.1.2-dev") (deps (list (crate-dep (name "crc32fast") (req "^1.1.2") (default-features #t) (kind 0)) (crate-dep (name "tempfile") (req "^3.0.7") (default-features #t) (kind 2)))) (hash "1w44mqb752i81jrr1wcib0h55hwbzvpx2488dpm3g1aia13ghpxj")))

(define-public crate-append-log-0.1 (crate (name "append-log") (vers "0.1.3-dev") (deps (list (crate-dep (name "crc32fast") (req "^1.1.2") (default-features #t) (kind 0)) (crate-dep (name "tempfile") (req "^3.0.7") (default-features #t) (kind 2)))) (hash "1ppgkqyyj42sfdhjbcsm62b2qhqzvpqjnf9wj4v4yxm8pl7766cs")))

(define-public crate-append-log-0.1 (crate (name "append-log") (vers "0.1.4-dev") (deps (list (crate-dep (name "crc32fast") (req "^1.1.2") (default-features #t) (kind 0)) (crate-dep (name "tempfile") (req "^3.0.7") (default-features #t) (kind 2)))) (hash "1qpg1jw88i97ib3lfpbr5br6ywm11bxz9l20j9wa9c2nxan5lja4")))

(define-public crate-append-only-0.0.1 (crate (name "append-only") (vers "0.0.1") (hash "0am9xx5m7837rg3i8qamkxrg00gnh2ybg11g86gvgc7pb47shd6x")))

(define-public crate-append-only-bytes-0.1 (crate (name "append-only-bytes") (vers "0.1.0") (hash "0j2s630jm5zcfadv55465wjki1i63igdx2l8kbqw3mjcpnkwwd01")))

(define-public crate-append-only-bytes-0.1 (crate (name "append-only-bytes") (vers "0.1.1") (hash "0npmsbch9lgfp6hvzv767r2vj9y49y82pcwk5wl649qjhk2b4ahm")))

(define-public crate-append-only-bytes-0.1 (crate (name "append-only-bytes") (vers "0.1.2") (hash "100991v0jrp6jrs6343w1p0wdf5413jizfyjq6icn7irbhdnsgf3") (features (quote (("u32_range"))))))

(define-public crate-append-only-bytes-0.1 (crate (name "append-only-bytes") (vers "0.1.3") (hash "1gynh5jqvyq2w7mylyx07lzvjp6fv57gvxmwq4ka3r3l7mh9p1na") (features (quote (("u32_range"))))))

(define-public crate-append-only-bytes-0.1 (crate (name "append-only-bytes") (vers "0.1.4") (hash "0yl3r132wbw6m1vywaym5hjk0c303i8bmj1fdd4qvnkdr1av4nzn") (features (quote (("u32_range"))))))

(define-public crate-append-only-bytes-0.1 (crate (name "append-only-bytes") (vers "0.1.5") (hash "0hhkyq3rp63pd1b5igfvrknspcrhsg39d6w8qdsb576lp91b9n25") (features (quote (("u32_range"))))))

(define-public crate-append-only-bytes-0.1 (crate (name "append-only-bytes") (vers "0.1.6") (deps (list (crate-dep (name "postcard") (req "^1.0.4") (features (quote ("alloc"))) (default-features #t) (kind 2)) (crate-dep (name "serde") (req "^1.0.171") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.100") (default-features #t) (kind 2)))) (hash "1h402aw78fjipl8ii9hnbly3ki19zng8w3si8125b0cyacmz724h") (features (quote (("u32_range") ("default" "serde"))))))

(define-public crate-append-only-bytes-0.1 (crate (name "append-only-bytes") (vers "0.1.7") (deps (list (crate-dep (name "postcard") (req "^1.0.4") (features (quote ("alloc"))) (default-features #t) (kind 2)) (crate-dep (name "serde") (req "^1.0.171") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.100") (default-features #t) (kind 2)))) (hash "0zbs0s15j7m07r268sa1vja1zksvrmqppyyf71823a422ndjbdr5") (features (quote (("u32_range") ("default"))))))

(define-public crate-append-only-bytes-0.1 (crate (name "append-only-bytes") (vers "0.1.8") (deps (list (crate-dep (name "postcard") (req "^1.0.4") (features (quote ("alloc"))) (default-features #t) (kind 2)) (crate-dep (name "serde") (req "^1.0.171") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.100") (default-features #t) (kind 2)))) (hash "1nc4wlksjk5fkzb0mbj9dzvr6b9q14saq2fk5ssgyli8l5bncwyx") (features (quote (("u32_range") ("default"))))))

(define-public crate-append-only-bytes-0.1 (crate (name "append-only-bytes") (vers "0.1.9") (deps (list (crate-dep (name "postcard") (req "^1.0.4") (features (quote ("alloc"))) (default-features #t) (kind 2)) (crate-dep (name "serde") (req "^1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.100") (default-features #t) (kind 2)))) (hash "14lppd9xnapvgm2pqy3csrls6ild6sxdva40kpvqg4kcf3m79162") (features (quote (("u32_range") ("default"))))))

(define-public crate-append-only-bytes-0.1 (crate (name "append-only-bytes") (vers "0.1.10") (deps (list (crate-dep (name "postcard") (req "^1.0.4") (features (quote ("alloc"))) (default-features #t) (kind 2)) (crate-dep (name "serde") (req "^1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.100") (default-features #t) (kind 2)))) (hash "1bw14iydmj197pvg6xhmyvgn52m7mcxa3cdkqf07yjjk0mj88pky") (features (quote (("u32_range") ("default"))))))

(define-public crate-append-only-bytes-0.1 (crate (name "append-only-bytes") (vers "0.1.11") (deps (list (crate-dep (name "postcard") (req "^1.0.4") (features (quote ("alloc"))) (default-features #t) (kind 2)) (crate-dep (name "serde") (req "^1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.100") (default-features #t) (kind 2)))) (hash "103pc6cmrkxx3dj492i1v7q9vb1r7vdvjs0bf7fj312p2jaqd3rw") (features (quote (("u32_range") ("default"))))))

(define-public crate-append-only-bytes-0.1 (crate (name "append-only-bytes") (vers "0.1.12") (deps (list (crate-dep (name "postcard") (req "^1.0.4") (features (quote ("alloc"))) (default-features #t) (kind 2)) (crate-dep (name "serde") (req "^1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.100") (default-features #t) (kind 2)))) (hash "1r9ypzsl0b8fs9nka6rmghwb7rzz57l97dbz1m56gpmxsq0nchxc") (features (quote (("u32_range") ("default"))))))

(define-public crate-append-only-vec-0.1 (crate (name "append-only-vec") (vers "0.1.0") (deps (list (crate-dep (name "parking_lot") (req "^0.12") (default-features #t) (kind 2)) (crate-dep (name "scaling") (req "^0.1.3") (default-features #t) (kind 2)))) (hash "1r2cn5gil7a4n643b0fja02kyh82n7xkwb4r4alyw7hr25crfp76")))

(define-public crate-append-only-vec-0.1 (crate (name "append-only-vec") (vers "0.1.1") (deps (list (crate-dep (name "parking_lot") (req "^0.12") (default-features #t) (kind 2)) (crate-dep (name "scaling") (req "^0.1.3") (default-features #t) (kind 2)))) (hash "1h9idnr0jmgq218jw0s3rknhi75gq66mk5mr8lqghrfav83ysa5y")))

(define-public crate-append-only-vec-0.1 (crate (name "append-only-vec") (vers "0.1.2") (deps (list (crate-dep (name "parking_lot") (req "^0.12") (default-features #t) (kind 2)) (crate-dep (name "scaling") (req "^0.1.3") (default-features #t) (kind 2)))) (hash "1vjzbp1rncny41g9hda740yzwmgb3dmjzf3v9kgr3203jiypc22n")))

(define-public crate-append-only-vec-0.1 (crate (name "append-only-vec") (vers "0.1.3") (deps (list (crate-dep (name "parking_lot") (req "^0.12") (default-features #t) (kind 2)) (crate-dep (name "scaling") (req "^0.1.3") (default-features #t) (kind 2)))) (hash "0al137anbgg38n404vb3cs4wnrp9dms7j0jx2vc9shfg9s3qzjzk")))

(define-public crate-append_db-0.2 (crate (name "append_db") (vers "0.2.0") (deps (list (crate-dep (name "async-trait") (req "^0.1.56") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.14") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.31") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "089r70ds7ny5ahzkl6h88whdwizf40m37bsr69scy42dym3ihksj")))

(define-public crate-append_db-0.3 (crate (name "append_db") (vers "0.3.1") (deps (list (crate-dep (name "async-trait") (req "^0.1.56") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.14") (default-features #t) (kind 0)) (crate-dep (name "stm") (req "^0.4.0") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.31") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "1ji0j2l2zq5nzsbl28h6carnyml8rrgi2zhi29n2gyh8az6gc6p6")))

(define-public crate-append_db-0.3 (crate (name "append_db") (vers "0.3.2") (deps (list (crate-dep (name "async-trait") (req "^0.1.56") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.14") (default-features #t) (kind 0)) (crate-dep (name "stm") (req "^0.4.0") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.31") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "0dw9jsnjsd9wv055qrq9xcgwm49bhvznwcqillv2rs2grg6kpj69")))

(define-public crate-append_db_postgres-0.2 (crate (name "append_db_postgres") (vers "0.2.0") (deps (list (crate-dep (name "append_db") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "append_db_postgres_derive") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "async-trait") (req "^0.1.56") (default-features #t) (kind 0)) (crate-dep (name "chrono") (req "^0.4.19") (features (quote ("serde"))) (default-features #t) (kind 0)) (crate-dep (name "futures") (req "^0.3.19") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.14") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "sqlx") (req "^0.5") (features (quote ("runtime-tokio-rustls" "migrate" "macros" "postgres" "json" "chrono"))) (default-features #t) (kind 0)) (crate-dep (name "sqlx-database-tester") (req "^0.2.0") (features (quote ("runtime-tokio"))) (default-features #t) (kind 2)) (crate-dep (name "thiserror") (req "^1.0.31") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "18pgbyykkbgkxnvqck7rm0b558sx529ryhbs3ijbq8kdjzyg7j5w") (features (quote (("derive"))))))

(define-public crate-append_db_postgres_derive-0.2 (crate (name "append_db_postgres_derive") (vers "0.2.0") (deps (list (crate-dep (name "convert_case") (req "^0.5.0") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1.0.39") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (default-features #t) (kind 0)))) (hash "0ah5vfs4345qv6qsf98nhb6svwh4h1hxs48cyyfj28irnz35y6np")))

(define-public crate-append_to_string-0.1 (crate (name "append_to_string") (vers "0.1.0") (deps (list (crate-dep (name "proc-macro2") (req "^1.0.47") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.8") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.57") (features (quote ("full" "fold"))) (default-features #t) (kind 0)))) (hash "1rjsivqhsbp0fals0j4n2yr7850xd52nb8fd7hw2xkk0gbs1ik8y")))

(define-public crate-appendbuf-0.0.1 (crate (name "appendbuf") (vers "0.0.1") (deps (list (crate-dep (name "memalloc") (req "^0") (default-features #t) (kind 0)))) (hash "1frv9jvbphvk4sc21k5jpapnyki61baca4azh7qhg9hjk4g4vr38")))

(define-public crate-appendbuf-0.0.2 (crate (name "appendbuf") (vers "0.0.2") (deps (list (crate-dep (name "memalloc") (req "^0") (default-features #t) (kind 0)))) (hash "10ljvd3dxr98nndl5j049hp86b300fm25a1mkx5iq6ii32y2m1va")))

(define-public crate-appendbuf-0.0.3 (crate (name "appendbuf") (vers "0.0.3") (deps (list (crate-dep (name "memalloc") (req "^0") (default-features #t) (kind 0)))) (hash "1iyhvzc7lyghvm7m8ygb46af34lqm41d1qyadk1q7h0bhlrry3f9")))

(define-public crate-appendbuf-0.1 (crate (name "appendbuf") (vers "0.1.0") (deps (list (crate-dep (name "memalloc") (req "^0") (default-features #t) (kind 0)))) (hash "1yad1h11cbb4cjp0xbdcxpxgnkiyh5za5wlpwm7pvfgn6rfw1k08")))

(define-public crate-appendbuf-0.1 (crate (name "appendbuf") (vers "0.1.1") (deps (list (crate-dep (name "memalloc") (req "^0") (default-features #t) (kind 0)))) (hash "06mxa31v0qkdgbfllm8c83hp4y5j0mca15phvv7v6c5cfc1q5il1")))

(define-public crate-appendbuf-0.1 (crate (name "appendbuf") (vers "0.1.2") (deps (list (crate-dep (name "memalloc") (req "^0") (default-features #t) (kind 0)))) (hash "04dfkx7jnvnazxjxvm9kfcbfp14hqsxnfs0vzzg7y8xcyq2sly3d")))

(define-public crate-appendbuf-0.1 (crate (name "appendbuf") (vers "0.1.3") (deps (list (crate-dep (name "memalloc") (req "^0") (default-features #t) (kind 0)))) (hash "0fb3f81v8vs8r4fd1b48idqm0vb9biam5wd9bd1ckjgl1mzln9c6")))

(define-public crate-appendbuf-0.1 (crate (name "appendbuf") (vers "0.1.4") (deps (list (crate-dep (name "memalloc") (req "^0") (default-features #t) (kind 0)))) (hash "14i0b5janmqqj8inbix9z8qdn564giwmxbh587j630pgg31bq9ws")))

(define-public crate-appendbuf-0.1 (crate (name "appendbuf") (vers "0.1.5") (deps (list (crate-dep (name "memalloc") (req "^0") (default-features #t) (kind 0)))) (hash "061b10zw2dlvaml1b102ci11qb6pdr3dfn1sfyarip4xb3bw2fsx")))

(define-public crate-appendbuf-0.1 (crate (name "appendbuf") (vers "0.1.6") (deps (list (crate-dep (name "memalloc") (req "^0") (default-features #t) (kind 0)))) (hash "1wm5nvzh9s1yq3dql6vqiw5frj462dankx68y15mfk4rp0n54yvq")))

(define-public crate-appendix-0.1 (crate (name "appendix") (vers "0.1.0") (deps (list (crate-dep (name "memmap") (req "^0.6.2") (default-features #t) (kind 0)) (crate-dep (name "parking_lot") (req "^0.5.3") (default-features #t) (kind 0)) (crate-dep (name "seahash") (req "^3.0") (default-features #t) (kind 0)) (crate-dep (name "tempdir") (req "^0.3.5") (default-features #t) (kind 2)))) (hash "103lgda0369niq07xmrddvaszi3514kc9qlvbnsnr988lfn0z34n")))

(define-public crate-appendix-2 (crate (name "appendix") (vers "2.0.0") (deps (list (crate-dep (name "arrayvec") (req "^0.4.10") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.2.0") (default-features #t) (kind 0)) (crate-dep (name "memmap") (req "^0.7.0") (default-features #t) (kind 0)) (crate-dep (name "parking_lot") (req "^0.7.1") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.6.4") (default-features #t) (kind 2)) (crate-dep (name "seahash") (req "^3.0.5") (default-features #t) (kind 0)) (crate-dep (name "tempfile") (req "^3.0.5") (default-features #t) (kind 2)))) (hash "06k44ng1yzzfwa42a6dy63w8f17i34afpkzaic1yfr8ip5s9vci2") (yanked #t)))

(define-public crate-appendix-0.1 (crate (name "appendix") (vers "0.1.1") (deps (list (crate-dep (name "arrayvec") (req "^0.4.10") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.2.0") (default-features #t) (kind 0)) (crate-dep (name "memmap") (req "^0.7.0") (default-features #t) (kind 0)) (crate-dep (name "parking_lot") (req "^0.7.1") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.6.4") (default-features #t) (kind 2)) (crate-dep (name "seahash") (req "^3.0.5") (default-features #t) (kind 0)) (crate-dep (name "tempfile") (req "^3.0.5") (default-features #t) (kind 2)))) (hash "17bpnqxci54h2c184n20307y57jvh0gh7kq9hdswmq3xnncj5fms")))

(define-public crate-appendix-0.1 (crate (name "appendix") (vers "0.1.2") (deps (list (crate-dep (name "arrayvec") (req "^0.4.10") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.2.0") (default-features #t) (kind 0)) (crate-dep (name "memmap") (req "^0.7.0") (default-features #t) (kind 0)) (crate-dep (name "parking_lot") (req "^0.7.1") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.6.4") (default-features #t) (kind 2)) (crate-dep (name "seahash") (req "^3.0.5") (default-features #t) (kind 0)) (crate-dep (name "tempfile") (req "^3.0.5") (default-features #t) (kind 2)))) (hash "0hb15pf6fv9ag318ala0rffs6kxpp6pxyc2xkm33a9kiggxjag0m")))

(define-public crate-appendix-0.1 (crate (name "appendix") (vers "0.1.3") (deps (list (crate-dep (name "arrayvec") (req "^0.4.10") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.2.0") (default-features #t) (kind 0)) (crate-dep (name "memmap") (req "^0.7.0") (default-features #t) (kind 0)) (crate-dep (name "parking_lot") (req "^0.7.1") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.6.4") (default-features #t) (kind 2)) (crate-dep (name "seahash") (req "^3.0.5") (default-features #t) (kind 0)) (crate-dep (name "tempfile") (req "^3.0.5") (default-features #t) (kind 2)))) (hash "1ryzd6v4xpvb1w79rgg77zjk2c33cqazw1gpi0w6igjvrm1b9m36")))

(define-public crate-appendix-0.2 (crate (name "appendix") (vers "0.2.0") (deps (list (crate-dep (name "arrayvec") (req "^0.4.10") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.2.0") (default-features #t) (kind 0)) (crate-dep (name "memmap") (req "^0.7.0") (default-features #t) (kind 0)) (crate-dep (name "parking_lot") (req "^0.7.1") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.6.4") (default-features #t) (kind 2)) (crate-dep (name "seahash") (req "^3.0.5") (default-features #t) (kind 0)) (crate-dep (name "tempfile") (req "^3.0.5") (default-features #t) (kind 2)))) (hash "14dx86cak8a8avgfb5vinyhmnimwl4ziggblbiqx3d8i26c5yiak")))

(define-public crate-appendix-0.2 (crate (name "appendix") (vers "0.2.1") (deps (list (crate-dep (name "arrayvec") (req "^0.4.10") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.2.0") (default-features #t) (kind 0)) (crate-dep (name "memmap") (req "^0.7.0") (default-features #t) (kind 0)) (crate-dep (name "parking_lot") (req "^0.7.1") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.6.4") (default-features #t) (kind 2)) (crate-dep (name "seahash") (req "^3.0.5") (default-features #t) (kind 0)) (crate-dep (name "tempfile") (req "^3.0.5") (default-features #t) (kind 2)))) (hash "11ih94ws4qvjd0i267zr7pdr3mpkc5nl839knn5fcga4d66fbymd")))

(define-public crate-appendix-0.2 (crate (name "appendix") (vers "0.2.2") (deps (list (crate-dep (name "arrayvec") (req "^0.4.10") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.2.0") (default-features #t) (kind 0)) (crate-dep (name "memmap") (req "^0.7.0") (default-features #t) (kind 0)) (crate-dep (name "parking_lot") (req "^0.7.1") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.6.4") (default-features #t) (kind 2)) (crate-dep (name "seahash") (req "^3.0.5") (default-features #t) (kind 0)) (crate-dep (name "tempfile") (req "^3.0.5") (default-features #t) (kind 2)))) (hash "1a482yly871b9gpp9xdd7sh6hgkkdda58d2kmci0r2qh1f7mfgbg")))

(define-public crate-appendlist-0.1 (crate (name "appendlist") (vers "0.1.0") (hash "1iwsjmddmw6bzx10i1ajbvxiynp7i0zag1q3fa92730chna9gcic")))

(define-public crate-appendlist-1 (crate (name "appendlist") (vers "1.0.0") (hash "0l5wfrmwk390369pcpg01ky2m499x3992c0qis75qapnbq8yvvkn")))

(define-public crate-appendlist-1 (crate (name "appendlist") (vers "1.1.0") (hash "0ca60jhimvdpmj5mqv2yx46s88vw1niig3h2998sxwykjqh29pck")))

(define-public crate-appendlist-1 (crate (name "appendlist") (vers "1.2.1") (hash "0dh5qwjddswrl4r1cinfg08yff4v98ph8bsmyb08d49cqsjzz68q")))

(define-public crate-appendlist-1 (crate (name "appendlist") (vers "1.3.0") (hash "0qd0akaiwkvmpzb4yiafwmsdncqm79mdwl21zgab8mr9wpc094ha")))

(define-public crate-appendlist-1 (crate (name "appendlist") (vers "1.4.0") (hash "1lnbl7mc7capcqj1z1ylxvm4h492sb9sr8pzww3q6lrhrmrxqjg1")))

(define-public crate-apperr-0.1 (crate (name "apperr") (vers "0.1.0") (hash "0fkrm6gj11d40a7m1wv4pq2bn7a8bhg34rdksv89pd8mizkm3rc8") (rust-version "1.0")))

(define-public crate-apperr-0.2 (crate (name "apperr") (vers "0.2.0") (hash "1hblkz8wc5qfxgsp0y3zk80l62010vcijdc9vla9p0bs1b7r96gx") (rust-version "1.0")))

