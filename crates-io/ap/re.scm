(define-module (crates-io ap re) #:use-module (crates-io))

(define-public crate-apread-0.1 (crate (name "apread") (vers "0.1.0") (hash "1as42rrj9di16x0d5ymjhzym38lbq2fn9gfmminl79a0i4kq6dw9")))

(define-public crate-apread-0.2 (crate (name "apread") (vers "0.2.0") (deps (list (crate-dep (name "anyhow") (req "^1.0.69") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^4.1.4") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "config") (req "^0.13.3") (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.11.14") (features (quote ("rustls" "json" "cookies"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.152") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.93") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.38") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.25.0") (features (quote ("macros" "rt" "rt-multi-thread" "tracing"))) (default-features #t) (kind 0)))) (hash "1abz2rhnws59j2j0312lgvcqd8xd5f9ajbfg8g0n2cxx2vz3pbs8")))

(define-public crate-apread-0.2 (crate (name "apread") (vers "0.2.1") (deps (list (crate-dep (name "anyhow") (req "^1.0.69") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^4.1.4") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "config") (req "^0.13.3") (default-features #t) (kind 0)) (crate-dep (name "html2md") (req "^0.2.14") (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.11.14") (features (quote ("rustls" "json" "cookies"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.152") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.93") (default-features #t) (kind 0)) (crate-dep (name "textwrap") (req "^0.16.0") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.38") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.25.0") (features (quote ("macros" "rt" "rt-multi-thread" "tracing"))) (default-features #t) (kind 0)))) (hash "0l3psg29y1drd8rzv3ivd4x92lz4z7r6vn0hyk5ksz8lp9ydjzxn")))

(define-public crate-apres-0.1 (crate (name "apres") (vers "0.1.0") (hash "02d41ibc16kykb1r33w5wi1nijl49ak19h94kg3jdby4pbw6901q")))

(define-public crate-apres-0.2 (crate (name "apres") (vers "0.2.1") (hash "0s4jdcpalv8p0lwrc9pjv0mqkvvb7nd2dgmh2gmqzwq5lpfgz8sv")))

(define-public crate-apres-0.2 (crate (name "apres") (vers "0.2.2") (hash "1fandrlhgq809pnssqp0jpwm0inps26rygmrx1c8x10la8plm69j")))

(define-public crate-apres-0.2 (crate (name "apres") (vers "0.2.3") (hash "0a442d7sncswsabv3gmwl5sfb058421fb99hzzjpjzsnai81s16j")))

(define-public crate-apres-0.2 (crate (name "apres") (vers "0.2.4") (hash "1mmjsnc84qh3wnyb13j906qy7qcv7mxqy2i5ps84h5c45pcapsay")))

(define-public crate-apres-0.2 (crate (name "apres") (vers "0.2.5") (hash "18a5s2xfrk1k4a98di90kb5vz1ksjwsy08rznizxv3amgdxna6jl")))

(define-public crate-apres-0.2 (crate (name "apres") (vers "0.2.8") (hash "07z47lrhsnm9cmrs43078sn45nxrd8f88s3nwxn5a7a9ll81l59m")))

(define-public crate-apres-0.2 (crate (name "apres") (vers "0.2.9") (hash "0zpd9wwbn8dw46sib75zlinkqwqxqs6c04rm0s9fijmaykdvgpcw")))

(define-public crate-apres-0.2 (crate (name "apres") (vers "0.2.10") (hash "139f81an468k3qaxnxga7mfpvbnjw7d9l33c4p6yn5hkjvwhkhkd")))

(define-public crate-apres-0.2 (crate (name "apres") (vers "0.2.11") (hash "19g2wa5h9yr63xw7d2h2incg928cqb9xbjlmazf8mkqfkcrvignz")))

(define-public crate-apres-0.3 (crate (name "apres") (vers "0.3.0") (hash "1nrhpmipvba9zqwnlj9k6gri41qll03j8n62x7xx90hcn6xczy14")))

(define-public crate-apres-0.3 (crate (name "apres") (vers "0.3.1") (hash "1yps1qaisb4sggwiq1ls8jc6cnb6wp00q2y9wyl86qw1wd9r0fgg")))

(define-public crate-apres-0.3 (crate (name "apres") (vers "0.3.2") (hash "0hycgzk9ykr1npns9vz55zfx715n77zs00rp4zlmbyrmf94ipss5")))

(define-public crate-apres-0.3 (crate (name "apres") (vers "0.3.3") (hash "151z189lwmh42vy8g6ypq7ns8d685v6sjfnj6sja0xnbhpanvshv")))

(define-public crate-apres-0.3 (crate (name "apres") (vers "0.3.4") (hash "0x8wa7fd0vjkcyimnk1ff9f03iy2y60gim0jmgwn0f0rdj8hpgjn")))

