(define-module (crates-io ap ip) #:use-module (crates-io))

(define-public crate-apipe-0.1 (crate (name "apipe") (vers "0.1.0") (deps (list (crate-dep (name "anyhow") (req "^1.0.62") (features (quote ("backtrace"))) (default-features #t) (kind 0)))) (hash "0ijdza3k3gd2z8wav27d9bcbkj0374lnp677mrm4hv9nzyd7jvm6")))

(define-public crate-apipe-0.1 (crate (name "apipe") (vers "0.1.1") (deps (list (crate-dep (name "anyhow") (req "^1.0.62") (features (quote ("backtrace"))) (default-features #t) (kind 0)))) (hash "04pn084p99s08ljq823zn8f9r0da0b0mijrkcn8a63lp2yj9b6kg")))

(define-public crate-apipe-0.2 (crate (name "apipe") (vers "0.2.0") (deps (list (crate-dep (name "lazy_static") (req "^1.4.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.6.0") (optional #t) (default-features #t) (kind 0)))) (hash "1bba9fsrwc72rdah6zkcyf247ywshmn7vkar0zkkz83bsnik0025") (features (quote (("nodeps") ("default" "parser")))) (v 2) (features2 (quote (("parser" "dep:lazy_static" "dep:regex"))))))

