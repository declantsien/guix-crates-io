(define-module (crates-io ap du) #:use-module (crates-io))

(define-public crate-apdu-0.1 (crate (name "apdu") (vers "0.1.0") (deps (list (crate-dep (name "apdu-core") (req "=0.1.0") (default-features #t) (kind 0)) (crate-dep (name "apdu-derive") (req "=0.1.0") (default-features #t) (kind 0)))) (hash "124lilxl514qd59g4cmkk5mpv3cy33hn82r7dy1vilkpcz4k1zmn")))

(define-public crate-apdu-0.1 (crate (name "apdu") (vers "0.1.1") (deps (list (crate-dep (name "apdu-core") (req "=0.1.1") (default-features #t) (kind 0)) (crate-dep (name "apdu-derive") (req "=0.1.1") (default-features #t) (kind 0)))) (hash "0qmyfbz8drdkxsn6b8sm23fi1jbnypam3mln5bxmv413pajh2z0w")))

(define-public crate-apdu-0.2 (crate (name "apdu") (vers "0.2.0") (deps (list (crate-dep (name "apdu-core") (req "=0.2.0") (default-features #t) (kind 0)) (crate-dep (name "apdu-derive") (req "=0.2.0") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0") (default-features #t) (kind 0)))) (hash "1rnxh6qgf8v33fcndkdr2qh1nn66zdcqqfr5v2j231c62cnqix0l")))

(define-public crate-apdu-0.2 (crate (name "apdu") (vers "0.2.1") (deps (list (crate-dep (name "apdu-core") (req "=0.2.1") (default-features #t) (kind 0)) (crate-dep (name "apdu-derive") (req "=0.2.1") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0") (default-features #t) (kind 0)))) (hash "1z2jj6lz6p7aj3vqdrf74054h7zy41gx890111007blr8hivhlhn")))

(define-public crate-apdu-0.3 (crate (name "apdu") (vers "0.3.0") (deps (list (crate-dep (name "apdu-core") (req "=0.3.0") (default-features #t) (kind 0)) (crate-dep (name "apdu-derive") (req "=0.3.0") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0") (default-features #t) (kind 0)))) (hash "0kfi1nixhavp18f8ic4jh94mkqgxsj7w9zs8r7zbbfqgmcfldc4n")))

(define-public crate-apdu-0.4 (crate (name "apdu") (vers "0.4.0") (deps (list (crate-dep (name "apdu-core") (req "=0.4.0") (default-features #t) (kind 0)) (crate-dep (name "apdu-derive") (req "=0.4.0") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0") (default-features #t) (kind 0)))) (hash "1zcgn3zywvbf044hwi22x6rh6lj3xd5p3hnnkd0m9cz9lq6a58bs")))

(define-public crate-apdu-core-0.1 (crate (name "apdu-core") (vers "0.1.0") (hash "1z5z8ifais40065mf1n36d1d8cvpfci9yvybmn64qbwpzw9lhmax")))

(define-public crate-apdu-core-0.1 (crate (name "apdu-core") (vers "0.1.1") (hash "1wnrfz2m0k5mkvz6h5pzi6lndm7n782hb61c4zhhxy8hm4s2mzsm")))

(define-public crate-apdu-core-0.2 (crate (name "apdu-core") (vers "0.2.0") (hash "0bk41a2yj17a70ii7ac9ym6wy46hjch6ydyyz9z6mzr26jz7xygw")))

(define-public crate-apdu-core-0.2 (crate (name "apdu-core") (vers "0.2.1") (hash "0z0f81a4jpv3ycl5zhr4zf1g0kns350cfx8j1c15sz102nglm8qv")))

(define-public crate-apdu-core-0.3 (crate (name "apdu-core") (vers "0.3.0") (hash "0mrm4wrkhrrwhxk0kmhi5kwgbi1qxw5xjxr9p756rfzy94fss8r8") (features (quote (("std") ("longer_payloads") ("default" "std"))))))

(define-public crate-apdu-core-0.4 (crate (name "apdu-core") (vers "0.4.0") (hash "11kzwa96kzn2l739bhn9wjgj70f6w8g73lx6bcr6igkbllhvjniw") (features (quote (("std") ("longer_payloads") ("default" "std"))))))

(define-public crate-apdu-derive-0.1 (crate (name "apdu-derive") (vers "0.1.0") (deps (list (crate-dep (name "apdu-core") (req "=0.1.0") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0") (default-features #t) (kind 2)))) (hash "0sik0mgmbchlfbsdv9mlgn12xxa3s30l7pq63smniqx5cxmf1giv")))

(define-public crate-apdu-derive-0.1 (crate (name "apdu-derive") (vers "0.1.1") (deps (list (crate-dep (name "apdu-core") (req "=0.1.1") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0") (default-features #t) (kind 2)))) (hash "08c1sjb32fkiw2lyqfiv6pvyaj9qqir09zhwp0gnj8np6bn6xcac")))

(define-public crate-apdu-derive-0.2 (crate (name "apdu-derive") (vers "0.2.0") (deps (list (crate-dep (name "apdu-core") (req "=0.2.0") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0") (default-features #t) (kind 2)))) (hash "1c968v4h3rrkqqxl2jm089r9g01hn3kgyfcy70a8f2d7bzslwgv1")))

(define-public crate-apdu-derive-0.2 (crate (name "apdu-derive") (vers "0.2.1") (deps (list (crate-dep (name "apdu-core") (req "=0.2.1") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0") (default-features #t) (kind 2)))) (hash "0cl88i58g0fkws5x5dcgc6zkf48zr5m24kz32xfd362r2ilbzw0f")))

(define-public crate-apdu-derive-0.3 (crate (name "apdu-derive") (vers "0.3.0") (deps (list (crate-dep (name "apdu-core") (req "=0.3.0") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0") (default-features #t) (kind 2)))) (hash "1fcndn5h26f23j2qbzky363fxagpip2pc9alb0yscy38sfg5dam2")))

(define-public crate-apdu-derive-0.4 (crate (name "apdu-derive") (vers "0.4.0") (deps (list (crate-dep (name "apdu-core") (req "=0.4.0") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0") (default-features #t) (kind 2)))) (hash "0z7yyw7xsv3jb42iffbgrrq17rcgc67fxf9rmh2h098hrvvpbmig")))

(define-public crate-apdu-dispatch-0.0.0 (crate (name "apdu-dispatch") (vers "0.0.0-unreleased") (hash "0pm3f30v85k1z1wrfarq0zy5dfmcq3cck93dsr5vv6xikassm93d")))

(define-public crate-apdu-dispatch-0.1 (crate (name "apdu-dispatch") (vers "0.1.0") (deps (list (crate-dep (name "delog") (req "^0.1.4") (default-features #t) (kind 0)) (crate-dep (name "heapless") (req "^0.7") (default-features #t) (kind 0)) (crate-dep (name "interchange") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "iso7816") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "serial_test") (req "^0.6") (default-features #t) (kind 2)))) (hash "05zz9psn9b6zpsz6fhk5i3bj16w5mf5s8vwdi035b98g1jgdm129") (features (quote (("std" "delog/std") ("log-warn") ("log-trace") ("log-none") ("log-info") ("log-error") ("log-debug") ("log-all") ("default"))))))

(define-public crate-apdu-dispatch-0.1 (crate (name "apdu-dispatch") (vers "0.1.1") (deps (list (crate-dep (name "delog") (req "^0.1.4") (default-features #t) (kind 0)) (crate-dep (name "heapless") (req "^0.7") (default-features #t) (kind 0)) (crate-dep (name "hex-literal") (req "^0.3.4") (default-features #t) (kind 2)) (crate-dep (name "interchange") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "iso7816") (req "^0.1.1") (default-features #t) (kind 0)) (crate-dep (name "serial_test") (req "^0.6") (default-features #t) (kind 2)))) (hash "0n4250zwbh7p8gb204lhry9lr117zmdz4nwyh2sb0ylrjlrf3ghp") (features (quote (("std" "delog/std") ("log-warn") ("log-trace") ("log-none") ("log-info") ("log-error") ("log-debug") ("log-all") ("default"))))))

(define-public crate-apdu-dispatch-0.1 (crate (name "apdu-dispatch") (vers "0.1.2") (deps (list (crate-dep (name "delog") (req "^0.1.4") (default-features #t) (kind 0)) (crate-dep (name "heapless") (req "^0.7") (default-features #t) (kind 0)) (crate-dep (name "hex-literal") (req "^0.3.4") (default-features #t) (kind 2)) (crate-dep (name "interchange") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "iso7816") (req "^0.1.1") (default-features #t) (kind 0)) (crate-dep (name "serial_test") (req "^0.6") (default-features #t) (kind 2)))) (hash "1yx58748z4082cy36fdp3i6yxnrg65xwyxjmklh7mcga1vilm349") (features (quote (("std" "delog/std") ("log-warn") ("log-trace") ("log-none") ("log-info") ("log-error") ("log-debug") ("log-all") ("default"))))))

(define-public crate-apdu-dispatch-0.2 (crate (name "apdu-dispatch") (vers "0.2.0") (deps (list (crate-dep (name "delog") (req "^0.1.4") (default-features #t) (kind 0)) (crate-dep (name "heapless") (req "^0.7") (default-features #t) (kind 0)) (crate-dep (name "hex-literal") (req "^0.3.4") (default-features #t) (kind 2)) (crate-dep (name "interchange") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "iso7816") (req "^0.1.1") (default-features #t) (kind 0)) (crate-dep (name "serial_test") (req "^0.6") (default-features #t) (kind 2)))) (hash "05rw57kd9bmavhinnkww86n6r3vamlb6ma99qzq5h5fylk3m98x0") (features (quote (("std" "delog/std") ("log-warn") ("log-trace") ("log-none") ("log-info") ("log-error") ("log-debug") ("log-all") ("default"))))))

