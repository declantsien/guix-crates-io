(define-module (crates-io ap ie) #:use-module (crates-io))

(define-public crate-apiety-0.0.1 (crate (name "apiety") (vers "0.0.1") (deps (list (crate-dep (name "error-chain") (req "^0.11.0") (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.7") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)))) (hash "09yamwr77810ikl289iscc8rgbjkyf7gi7f1s9kikxyn148qfajy") (yanked #t)))

(define-public crate-apiety-0.0.2 (crate (name "apiety") (vers "0.0.2") (deps (list (crate-dep (name "error-chain") (req "^0.11.0") (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.7") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)))) (hash "1ghah78kjxsq3zwmh0bi80rh1qsng4jz1ya1qqaw0x2zh6ad6x9s") (yanked #t)))

