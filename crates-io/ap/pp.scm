(define-module (crates-io ap pp) #:use-module (crates-io))

(define-public crate-apppass-0.1 (crate (name "apppass") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^2.33.3") (default-features #t) (kind 0)) (crate-dep (name "once_cell") (req "^1.8") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)))) (hash "1hnbzrcq6h5224gg2cvjizf2761825h5pg6s8c9v6qdx8jg2jx3f") (yanked #t)))

(define-public crate-apppass-0.1 (crate (name "apppass") (vers "0.1.1") (deps (list (crate-dep (name "clap") (req "^2.33.3") (default-features #t) (kind 0)) (crate-dep (name "once_cell") (req "^1.8") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)))) (hash "0ycb6aw61wi9p3vyzzgac0nf6cl3vz9h1cl8ssj8dgigpw3q27wy") (yanked #t)))

(define-public crate-apppass-0.1 (crate (name "apppass") (vers "0.1.2") (deps (list (crate-dep (name "clap") (req "^2.33.3") (default-features #t) (kind 0)) (crate-dep (name "once_cell") (req "^1.8") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)))) (hash "1br9zh8hgxiw9l8dgn6qcs545r2lhrrywj4zdavykz6n189x558p")))

