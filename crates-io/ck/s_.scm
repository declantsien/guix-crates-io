(define-module (crates-io ck s_) #:use-module (crates-io))

(define-public crate-cks_accumulator-0.1 (crate (name "cks_accumulator") (vers "0.1.0") (deps (list (crate-dep (name "amcl_wrapper") (req "^0.1.7") (features (quote ("bls381"))) (kind 0)) (crate-dep (name "failure") (req "^0.1.5") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.7") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0") (default-features #t) (kind 0)))) (hash "1f9h2w9gb06q11gvfqgbzzdwlh2bgsc5n2n7p102b21b5k22byvh")))

(define-public crate-cks_accumulator-0.1 (crate (name "cks_accumulator") (vers "0.1.2") (deps (list (crate-dep (name "amcl_wrapper") (req "^0.3.1") (features (quote ("bls381"))) (kind 0)) (crate-dep (name "failure") (req "^0.1.5") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.7") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0") (default-features #t) (kind 0)))) (hash "0xbsf4whns6f23l4r6j516mlvs8bndzw8fqrnx6ymbc22l1vi3ya")))

