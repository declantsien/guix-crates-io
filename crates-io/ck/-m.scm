(define-module (crates-io ck -m) #:use-module (crates-io))

(define-public crate-ck-meow-0.1 (crate (name "ck-meow") (vers "0.1.0") (deps (list (crate-dep (name "keccak") (req "^0.1.2") (default-features #t) (kind 0)) (crate-dep (name "proptest") (req "^1.0.0") (default-features #t) (kind 2)) (crate-dep (name "subtle") (req "^2.4.1") (default-features #t) (kind 0)) (crate-dep (name "zeroize") (req "^1.5.7") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "08qkv1acv7s2x949r2sj9srw6arhy12fiz8g4schc4f4iwqild90")))

