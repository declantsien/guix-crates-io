(define-module (crates-io ck _c) #:use-module (crates-io))

(define-public crate-ck_commands-0.0.4 (crate (name "ck_commands") (vers "0.0.4") (deps (list (crate-dep (name "rstest") (req "^0.18.2") (default-features #t) (kind 2)))) (hash "0ad4y5fsa5rqzmv71xz7hpbwzz0pbf305rlsd2pjvmb3clz0bbps")))

(define-public crate-ck_commands-0.0.5 (crate (name "ck_commands") (vers "0.0.5") (deps (list (crate-dep (name "rstest") (req "^0.18.2") (default-features #t) (kind 2)))) (hash "1n7d054hkp3prbhhncy3gml6q5axgjpr1n690ar2qxn1l534i0br")))

