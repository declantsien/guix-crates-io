(define-module (crates-io ck ia) #:use-module (crates-io))

(define-public crate-ckia_sys-121 (crate (name "ckia_sys") (vers "121.0.0") (hash "0pgkqkzkblyg1gnvbrhmw1c382j1745p0qmb1wyjq57vkhw015c8") (features (quote (("use_system_libs" "build_from_src") ("is_component_build") ("default") ("build_from_src")))) (links "skia")))

