(define-module (crates-io ck yd) #:use-module (crates-io))

(define-public crate-ckydb-0.0.5 (crate (name "ckydb") (vers "0.0.5") (deps (list (crate-dep (name "serial_test") (req "^0.8.0") (default-features #t) (kind 2)))) (hash "1k6nn1x6wnqf75ix5knkw5swf7sqg9sj8ypx7nmcyykxaqav2pkv")))

