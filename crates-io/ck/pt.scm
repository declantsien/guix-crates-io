(define-module (crates-io ck pt) #:use-module (crates-io))

(define-public crate-ckpt-analyzer-1 (crate (name "ckpt-analyzer") (vers "1.0.0") (deps (list (crate-dep (name "sha256") (req "^1.1.1") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.38") (default-features #t) (kind 0)))) (hash "18253w1c4k25p1mjf50z2d5j6qw9jc3n4lcf13xvzmkiyl84l824")))

(define-public crate-ckpt-analyzer-1 (crate (name "ckpt-analyzer") (vers "1.0.1") (deps (list (crate-dep (name "eyre") (req "^0.6.8") (default-features #t) (kind 0)) (crate-dep (name "sha256") (req "^1.1.1") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.38") (default-features #t) (kind 0)))) (hash "0bqwqx22d935rg32phhbdwnq5lwws2ah3f8bscis2lj0d7mlqbgv")))

(define-public crate-ckpttn-rs-0.1 (crate (name "ckpttn-rs") (vers "0.1.0") (deps (list (crate-dep (name "petgraph") (req "^0.6.3") (features (quote ("serde-1"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.104") (default-features #t) (kind 0)))) (hash "0qrkfrdgqrjr1d0a1lkh9283wcdzc8javhj24gv0lq5nm872dbq4")))

