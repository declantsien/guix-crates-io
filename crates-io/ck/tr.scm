(define-module (crates-io ck tr) #:use-module (crates-io))

(define-public crate-cktrs-0.1 (crate (name "cktrs") (vers "0.1.0") (deps (list (crate-dep (name "regex") (req "^1.6.0") (default-features #t) (kind 0)))) (hash "06ja2x26rbgvr4pl2k3m5zdb5zbw0fmcqagb1gdgvhqm9hkn4y1n")))

