(define-module (crates-io ck #{2j}#) #:use-module (crates-io))

(define-public crate-ck2json-0.1 (crate (name "ck2json") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^2.33.0") (default-features #t) (kind 0)) (crate-dep (name "encoding_rs") (req "^0.8.17") (default-features #t) (kind 0)) (crate-dep (name "encoding_rs_io") (req "^0.1.6") (default-features #t) (kind 0)) (crate-dep (name "pest") (req "^2.1.1") (default-features #t) (kind 0)) (crate-dep (name "pest_derive") (req "^2.1.0") (default-features #t) (kind 0)))) (hash "1nm064fwlrzi8yg77b7p3b7kbdr1k74q04mgjic4y3g6x3i6r5bk")))

