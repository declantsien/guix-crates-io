(define-module (crates-io qs v2) #:use-module (crates-io))

(define-public crate-qsv2flv-0.1 (crate (name "qsv2flv") (vers "0.1.2") (deps (list (crate-dep (name "chrono") (req "~0.4") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "~2.27.0") (default-features #t) (kind 0)))) (hash "0y7d5q8xn0pgi6hgl7vavs47d8w8gim32yv0kwb8jf1r7srfwadw")))

