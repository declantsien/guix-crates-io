(define-module (crates-io qs tr) #:use-module (crates-io))

(define-public crate-qstring-0.1 (crate (name "qstring") (vers "0.1.0") (deps (list (crate-dep (name "urlencoding") (req "^1.0.0") (default-features #t) (kind 0)))) (hash "00bh8hcff5jkqvkxbi6wj4m1d8nmik8ph6mlvczwpwrxdmnl6lzc")))

(define-public crate-qstring-0.1 (crate (name "qstring") (vers "0.1.1") (deps (list (crate-dep (name "urlencoding") (req "^1.0.0") (default-features #t) (kind 0)))) (hash "1br5dv91lnarf2rvmcf85ppfifndmkwzb1lfprk178j28b3jxynh")))

(define-public crate-qstring-0.1 (crate (name "qstring") (vers "0.1.2") (deps (list (crate-dep (name "urlencoding") (req "^1.0.0") (default-features #t) (kind 0)))) (hash "0n1flbz61hwl73avp5g1l0jawkh3hbzjzrphpykacbaj9x81vx4z")))

(define-public crate-qstring-0.2 (crate (name "qstring") (vers "0.2.0") (deps (list (crate-dep (name "urlencoding") (req "^1.0.0") (default-features #t) (kind 0)))) (hash "1y4gi8j0kfb0x60jz37y9wn2q2y6l647nb473m7w0lv6mk8bia78")))

(define-public crate-qstring-0.3 (crate (name "qstring") (vers "0.3.0") (deps (list (crate-dep (name "percent-encoding") (req "^1.0") (default-features #t) (kind 0)))) (hash "05nlynd3mqr2wmwrn402vx19xsb606gaa433b68ga1ziz65f745r")))

(define-public crate-qstring-0.4 (crate (name "qstring") (vers "0.4.0") (deps (list (crate-dep (name "percent-encoding") (req "^1.0") (default-features #t) (kind 0)))) (hash "1ccr5rxd0cq9672gpaidq3l4vvzypxs0r1d4ak8hg80x0kz9gzig")))

(define-public crate-qstring-0.5 (crate (name "qstring") (vers "0.5.0") (deps (list (crate-dep (name "percent-encoding") (req "^1.0") (default-features #t) (kind 0)))) (hash "1b1aaw4qa53sh89lggj67rnam4l7d1mjx5pgji85h1a2jqs9nvin")))

(define-public crate-qstring-0.5 (crate (name "qstring") (vers "0.5.1") (deps (list (crate-dep (name "percent-encoding") (req "^1.0") (default-features #t) (kind 0)))) (hash "0wz1ldjdq8r1qq86qgx8qva3idysi18mvrmzxbrqyk62k7rpgsvg")))

(define-public crate-qstring-0.5 (crate (name "qstring") (vers "0.5.2") (deps (list (crate-dep (name "percent-encoding") (req "^1.0") (default-features #t) (kind 0)))) (hash "1m4mqjrq255gp3rsg32402bvyy6mdmg0fijbxywhvkamngczblxl")))

(define-public crate-qstring-0.5 (crate (name "qstring") (vers "0.5.3") (deps (list (crate-dep (name "percent-encoding") (req "^1.0") (default-features #t) (kind 0)))) (hash "00qch2kiay6376bmsamwawbkd573k4kqasgi1a1m2dfwfll6frdh")))

(define-public crate-qstring-0.6 (crate (name "qstring") (vers "0.6.0") (deps (list (crate-dep (name "percent-encoding") (req "^1.0") (default-features #t) (kind 0)))) (hash "0sc483h65nb36h7byfslwbs4m6742bcswfw8nmgy54valdbw0pjl")))

(define-public crate-qstring-0.7 (crate (name "qstring") (vers "0.7.0") (deps (list (crate-dep (name "percent-encoding") (req "^2.0") (default-features #t) (kind 0)))) (hash "0njnmd96x4wb7hcva9g2k3mih59w6rn6hsxzrdkh76cjvz94fw16")))

(define-public crate-qstring-0.7 (crate (name "qstring") (vers "0.7.1") (deps (list (crate-dep (name "percent-encoding") (req "^2.0") (default-features #t) (kind 0)))) (hash "0n0lrph40w9q5x1lvjgw1jwd4jd2h20ysdxjax1551377jqpfvri")))

(define-public crate-qstring-0.7 (crate (name "qstring") (vers "0.7.2") (deps (list (crate-dep (name "percent-encoding") (req "^2.0") (default-features #t) (kind 0)))) (hash "0khhcpwifis87r5chr4jiv3w1bkswcf226c0yjx809pzbzkglr6l")))

