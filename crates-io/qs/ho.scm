(define-module (crates-io qs ho) #:use-module (crates-io))

(define-public crate-qshot-0.1 (crate (name "qshot") (vers "0.1.0") (deps (list (crate-dep (name "windows") (req "^0.43.0") (features (quote ("Win32_Foundation" "Win32_Graphics_Gdi"))) (default-features #t) (kind 0)))) (hash "171lmlpdk979vk9qazql3vhmxm043nnswxcqhda17m85hz7gp5gd")))

(define-public crate-qshot-0.1 (crate (name "qshot") (vers "0.1.1") (deps (list (crate-dep (name "windows") (req "^0.43.0") (features (quote ("Win32_Foundation" "Win32_Graphics_Gdi"))) (default-features #t) (kind 0)))) (hash "128dhq6mnpmyglnyl6gy3avj18z556228sdp4xdswnsz5fwa7vac")))

(define-public crate-qshot-0.1 (crate (name "qshot") (vers "0.1.2") (deps (list (crate-dep (name "windows") (req "^0.43.0") (features (quote ("Win32_Foundation" "Win32_Graphics_Gdi"))) (default-features #t) (kind 0)))) (hash "1kn4l95j3ig1glrxyvz9x95mbf1zp3mdjm4wc8mx34fnbzvb6g4k")))

(define-public crate-qshot-0.1 (crate (name "qshot") (vers "0.1.3") (deps (list (crate-dep (name "windows") (req "^0.51.1") (features (quote ("Win32_Foundation" "Win32_Graphics_Gdi"))) (default-features #t) (kind 0)))) (hash "1qsdvywaxgh8va8bb0liqjs3lnzdy1hynp1vz912hg5722bccznc")))

