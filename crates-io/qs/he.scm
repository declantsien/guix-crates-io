(define-module (crates-io qs he) #:use-module (crates-io))

(define-public crate-qshell-0.1 (crate (name "qshell") (vers "0.1.0") (deps (list (crate-dep (name "sh-macro") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "15g67nddjbmdq4ldip37fgl5m3g27n0rfxfkpvnlnf1dqg3gqsha") (yanked #t)))

(define-public crate-qshell-0.1 (crate (name "qshell") (vers "0.1.1") (deps (list (crate-dep (name "sh-macro") (req "^0.1.1") (default-features #t) (kind 0)))) (hash "1050488n3h9503anrdij6d6hvmxf5jxlmra7mdx0cfiqnncfvrx1") (yanked #t)))

