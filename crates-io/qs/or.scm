(define-module (crates-io qs or) #:use-module (crates-io))

(define-public crate-qsort-rs-0.1 (crate (name "qsort-rs") (vers "0.1.0") (hash "18xvr0cp48x3c1p09z2cv57fjn5v5fpdqlfyqfx1cc8bcx74hkq9")))

(define-public crate-qsort-rs-0.1 (crate (name "qsort-rs") (vers "0.1.1") (deps (list (crate-dep (name "rand") (req "^0.8") (default-features #t) (kind 2)))) (hash "1ds9igjhxcr7rr5nnhz176dmnhjvc2zrs7iyjs9lvfmpzdgzvlib")))

