(define-module (crates-io qs -r) #:use-module (crates-io))

(define-public crate-qs-rs-0.1 (crate (name "qs-rs") (vers "0.1.0") (hash "1m9hkl4sh83kknrfighpknbi1gzz430m8vz0rhsilwdg9g79bsw0")))

(define-public crate-qs-rs-0.1 (crate (name "qs-rs") (vers "0.1.1") (hash "0k5yxn46n7ws2h9410ckzb9s4gsjwygx7dvhjd8jjcgaxcrki8y6")))

(define-public crate-qs-rs-0.1 (crate (name "qs-rs") (vers "0.1.2") (deps (list (crate-dep (name "urlencoding") (req "^2.1.0") (default-features #t) (kind 0)))) (hash "0753cjc6zfkamrin2ckl3gzp99pmdmn2pysakydi7y94d66fvakh")))

(define-public crate-qs-rs-0.1 (crate (name "qs-rs") (vers "0.1.3") (deps (list (crate-dep (name "urlencoding") (req "^2.1.0") (default-features #t) (kind 0)))) (hash "1xflgi4imghd0adsk4mlz27hlmvs6kq6hsbs1n43aydy8h8cvynb") (yanked #t)))

(define-public crate-qs-rs-0.1 (crate (name "qs-rs") (vers "0.1.4") (deps (list (crate-dep (name "urlencoding") (req "^2.1.0") (default-features #t) (kind 0)))) (hash "03p1hrhbkqpsycjiy5hj8rccxwscjv3fd481bsrwkql7xvrgqc04")))

