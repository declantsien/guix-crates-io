(define-module (crates-io yo gc) #:use-module (crates-io))

(define-public crate-yogcrypt-0.0.0 (crate (name "yogcrypt") (vers "0.0.0") (deps (list (crate-dep (name "criterion") (req "^0.2") (default-features #t) (kind 2)) (crate-dep (name "lazy_static") (req "^1.0.2") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.5.4") (default-features #t) (kind 0)))) (hash "16c9ipl61ygpbaxdhddcrpwwbm94rwq17wnywfbxxk3yrsz9sw5q")))

