(define-module (crates-io yo ur) #:use-module (crates-io))

(define-public crate-your_game_of_life-0.1 (crate (name "your_game_of_life") (vers "0.1.0") (hash "1a0npy8a7p7qari8iz5llw8x1q8h0mg13sc6gpky224gvpvpny7c")))

(define-public crate-youran-0.1 (crate (name "youran") (vers "0.1.0") (hash "0h25r8nr4bdd4sfh5kf11nq0xpwj6aalx2xbrlkzvvki82d803hq")))

(define-public crate-youran-0.2 (crate (name "youran") (vers "0.2.0") (deps (list (crate-dep (name "anyhow") (req "^1.0.65") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^3.2.22") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "dirs") (req "^4.0.0") (default-features #t) (kind 0)) (crate-dep (name "qr2term") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "rusqlite") (req "^0.28.0") (default-features #t) (kind 0)))) (hash "17k0ds6wximr4c4qvskd19iy9v6fq7zci565vz6w901s5hg6gjm3")))

(define-public crate-youran-0.2 (crate (name "youran") (vers "0.2.1") (deps (list (crate-dep (name "anyhow") (req "^1.0.65") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^3.2.22") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "dirs") (req "^4.0.0") (default-features #t) (kind 0)) (crate-dep (name "qr2term") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "rusqlite") (req "^0.28.0") (default-features #t) (kind 0)))) (hash "0h7jjdy54mb46q8dhykg2q072cvpzkhl3cvlsjlhnizs1n2mxz3m")))

(define-public crate-youran-0.2 (crate (name "youran") (vers "0.2.2") (deps (list (crate-dep (name "anyhow") (req "^1.0.65") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^3.2.22") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "dirs") (req "^4.0.0") (default-features #t) (kind 0)) (crate-dep (name "qr2term") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "rusqlite") (req "^0.28.0") (default-features #t) (kind 0)))) (hash "1zsg10vjbfwqkrn9vxhmwxy5gsw3mvmfnb7l0vn6bwxnr1n21vgi")))

(define-public crate-youran-0.2 (crate (name "youran") (vers "0.2.3") (deps (list (crate-dep (name "anyhow") (req "^1.0.65") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^3.2.22") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "dirs") (req "^4.0.0") (default-features #t) (kind 0)) (crate-dep (name "qr2term") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "rusqlite") (req "^0.28.0") (default-features #t) (kind 0)))) (hash "0ld5x0s1068cgmb2rvzqz6fmp6djnf9c8d3bax5cmxq65k4slwwz")))

(define-public crate-youran-0.2 (crate (name "youran") (vers "0.2.4") (deps (list (crate-dep (name "anyhow") (req "^1.0.65") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^4.3.4") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "dirs") (req "^5.0.1") (default-features #t) (kind 0)) (crate-dep (name "qr2term") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "sqlx") (req "^0.6.3") (features (quote ("runtime-tokio-rustls" "sqlite" "migrate"))) (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.28.2") (features (quote ("macros"))) (default-features #t) (kind 0)) (crate-dep (name "unicode-width") (req "^0.1.10") (default-features #t) (kind 0)))) (hash "0ibvrp8mm3pb6ymrxg5mqfblyjv7fniv52gwg0ph9kvsska4z23p")))

(define-public crate-yourl-0.0.1 (crate (name "yourl") (vers "0.0.1") (hash "16bkj9v3i91m6pkbmymn8a30y7pvi8m3y4k6vy57vfb7p428zr1b") (yanked #t)))

