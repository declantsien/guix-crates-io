(define-module (crates-io yo im) #:use-module (crates-io))

(define-public crate-yoimiya-0.1 (crate (name "yoimiya") (vers "0.1.0") (deps (list (crate-dep (name "anyhow") (req "^1.0.79") (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.11.23") (features (quote ("json"))) (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.0.0") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "084sl7blj8x1gyqjy4zc7qq6ba84hd7an2cz09q6y0wny04mi7d2")))

