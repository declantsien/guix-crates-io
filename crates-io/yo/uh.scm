(define-module (crates-io yo uh) #:use-module (crates-io))

(define-public crate-youhtmod-0.1 (crate (name "youhtmod") (vers "0.1.0") (hash "06mvql0zqnld77g1jg35h411kfb28d5b8m5k0ihq3zmhlmifx56d")))

(define-public crate-youhtmod-0.1 (crate (name "youhtmod") (vers "0.1.1") (hash "1av2i5xqcc8k5gm634fdl3ha2pxr199cpn6qd64df7fmpqvj9d83")))

(define-public crate-youhtmod-0.1 (crate (name "youhtmod") (vers "0.1.2") (hash "040apq6x0p5q8l3hhp0rzxm9dwypcygnkycrxhfmam01xid7sr91")))

(define-public crate-youhtmod-0.1 (crate (name "youhtmod") (vers "0.1.3") (hash "0g248qw3ydyya2bsmp07l43rhgbqb8y1c659dn5pg6anb7y8nnj5")))

