(define-module (crates-io yo us) #:use-module (crates-io))

(define-public crate-youstat-0.1 (crate (name "youstat") (vers "0.1.0") (deps (list (crate-dep (name "chrono") (req "^0.4.19") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^2.33.3") (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.11.3") (features (quote ("json"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.126") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.64") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.11.0") (features (quote ("rt"))) (default-features #t) (kind 0)))) (hash "007c4lh26gpsbzy4mz9a2yxjsj780j0gj9g5xi5lzl2izi4vlpn9")))

