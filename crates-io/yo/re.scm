(define-module (crates-io yo re) #:use-module (crates-io))

(define-public crate-yore-0.1 (crate (name "yore") (vers "0.1.0") (deps (list (crate-dep (name "thiserror") (req "^1.0.25") (default-features #t) (kind 0)))) (hash "0f0x9k5xc5l34clf2psf0brjsdg8cbzi3vrk7b6kfgv331ja16b9")))

(define-public crate-yore-0.2 (crate (name "yore") (vers "0.2.0") (deps (list (crate-dep (name "thiserror") (req "^1.0.25") (default-features #t) (kind 0)))) (hash "0620xy00la178ci96xdgnm1h7p482l4q1y0shrbm633vypf54sn2")))

(define-public crate-yore-0.3 (crate (name "yore") (vers "0.3.0") (deps (list (crate-dep (name "thiserror") (req "^1.0.25") (default-features #t) (kind 0)))) (hash "1ydlzjkgwk6d5pq6dbnmwbqh1lr4hbnkr06anr09b30qg5hxhd34")))

(define-public crate-yore-0.3 (crate (name "yore") (vers "0.3.1") (deps (list (crate-dep (name "thiserror") (req "^1.0.25") (default-features #t) (kind 0)))) (hash "05d6mnvf7r129c436y9qji89qycq7l5hcfi2s7di2p2zkg7aqv4i")))

(define-public crate-yore-0.3 (crate (name "yore") (vers "0.3.2") (deps (list (crate-dep (name "thiserror") (req "^1.0.25") (default-features #t) (kind 0)))) (hash "0vrybz491q4fbn5q5kmmwzx00qjyawg9x7lf321r3q3l2i2xz1db")))

(define-public crate-yore-0.3 (crate (name "yore") (vers "0.3.3") (deps (list (crate-dep (name "thiserror") (req "^1.0.25") (default-features #t) (kind 0)))) (hash "0qbyvfqqhnimi9dkfl7m607jz4mvbibdhm2x572nki1vflw8rmc1")))

(define-public crate-yore-1 (crate (name "yore") (vers "1.0.0") (deps (list (crate-dep (name "criterion") (req "^0.3.5") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.8.4") (default-features #t) (kind 2)) (crate-dep (name "rand_pcg") (req "^0.3.1") (default-features #t) (kind 2)) (crate-dep (name "thiserror") (req "^1.0.25") (default-features #t) (kind 0)))) (hash "0fzk6iy34y4v58s5y6pn0v2kfhi4l5f3mnpz3vrni3gx24hz14aq")))

(define-public crate-yore-1 (crate (name "yore") (vers "1.0.1") (deps (list (crate-dep (name "criterion") (req "^0.3.5") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.8.4") (default-features #t) (kind 2)) (crate-dep (name "rand_pcg") (req "^0.3.1") (default-features #t) (kind 2)) (crate-dep (name "thiserror") (req "^1.0.25") (default-features #t) (kind 0)))) (hash "1dxfs0bndj967b99sd79kzac36pyckwbcwrkpqgk4mfhb3qjk511")))

(define-public crate-yore-1 (crate (name "yore") (vers "1.0.2") (deps (list (crate-dep (name "criterion") (req "^0.3.5") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.8.4") (default-features #t) (kind 2)) (crate-dep (name "rand_pcg") (req "^0.3.1") (default-features #t) (kind 2)) (crate-dep (name "thiserror") (req "^1.0.25") (default-features #t) (kind 0)))) (hash "09caq5ldsrfp2dil4h59dvj8y17axmldng3a19c7y0safx9rh5pz")))

(define-public crate-yore-1 (crate (name "yore") (vers "1.1.0") (deps (list (crate-dep (name "criterion") (req "^0.3.5") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.8.4") (default-features #t) (kind 2)) (crate-dep (name "rand_pcg") (req "^0.3.1") (default-features #t) (kind 2)) (crate-dep (name "thiserror") (req "^1.0.25") (default-features #t) (kind 0)))) (hash "02x2m28v4dw0csvp5d3v176vvjl9ii15pd58qnjqbakz3g7cfn4g")))

(define-public crate-yorex_data-0.1 (crate (name "yorex_data") (vers "0.1.0") (deps (list (crate-dep (name "chrono") (req "^0.4") (features (quote ("serde"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "ureq") (req "^2.1.0") (features (quote ("json" "charset"))) (default-features #t) (kind 0)))) (hash "1220h9if8nz8qh89my2m97g1a3vj8vsz0wl26dh6mn7asfm8vhpw")))

(define-public crate-yorex_data-0.1 (crate (name "yorex_data") (vers "0.1.1") (deps (list (crate-dep (name "chrono") (req "^0.4") (features (quote ("serde"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "ureq") (req "^2.1.0") (features (quote ("json" "charset"))) (default-features #t) (kind 0)))) (hash "08m7is79wcbfqj52yqw5gchrr11qhq88xghahg2pkya94bbx8j9v")))

