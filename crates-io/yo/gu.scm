(define-module (crates-io yo gu) #:use-module (crates-io))

(define-public crate-yogurt-0.1 (crate (name "yogurt") (vers "0.1.0") (deps (list (crate-dep (name "fnv") (req "^1.0.7") (default-features #t) (kind 0)) (crate-dep (name "nom") (req "^7.1.1") (default-features #t) (kind 0)))) (hash "0hgra32334zykx5fkjsa6lpwx9xv8qq5vv95px37b82amy34qalh")))

(define-public crate-yogurt-0.1 (crate (name "yogurt") (vers "0.1.1") (deps (list (crate-dep (name "fnv") (req "^1.0.7") (default-features #t) (kind 0)) (crate-dep (name "nom") (req "^7.1.1") (default-features #t) (kind 0)))) (hash "1l9if90qwvg9lqbdn64jm4livck04cs4g96akxnslxh7sank8x8s")))

(define-public crate-yogurt-0.2 (crate (name "yogurt") (vers "0.2.0") (deps (list (crate-dep (name "fnv") (req "^1.0.7") (default-features #t) (kind 0)) (crate-dep (name "nom") (req "^7.1.1") (default-features #t) (kind 0)))) (hash "00p2nlg2ipc94wyhj2akcf8xargnwb5hf3fg0d5ky73izc7k8l96")))

(define-public crate-yogurt-0.3 (crate (name "yogurt") (vers "0.3.0") (deps (list (crate-dep (name "fnv") (req "^1.0.7") (default-features #t) (kind 0)) (crate-dep (name "nom") (req "^7.1.1") (default-features #t) (kind 0)))) (hash "1prf3fj20qjh9hvm1q71q98w3aqspwzl0fadsgarba34lid9cj7r")))

(define-public crate-yogurt-0.3 (crate (name "yogurt") (vers "0.3.1") (deps (list (crate-dep (name "fnv") (req "^1.0.7") (default-features #t) (kind 0)) (crate-dep (name "nom") (req "^7.1.1") (default-features #t) (kind 0)))) (hash "0z21c70wj4nzlpjc8k5wph6j7z6i5di5w2q4jjk78hp6bbshgjhg")))

(define-public crate-yogurt-parse-0.0.0 (crate (name "yogurt-parse") (vers "0.0.0") (deps (list (crate-dep (name "regex") (req "^1.3.1") (default-features #t) (kind 0)) (crate-dep (name "yaml-rust") (req "^0.4.3") (default-features #t) (kind 0)))) (hash "06ab6mr5xmadvddkijc1s4q9ywnm58rz7617559qxmwaqkl2z1zy")))

(define-public crate-yogurt-yaml-0.0.0 (crate (name "yogurt-yaml") (vers "0.0.0") (deps (list (crate-dep (name "argparse") (req "^0.2.2") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.3.1") (default-features #t) (kind 0)) (crate-dep (name "yaml-rust") (req "^0.4.3") (default-features #t) (kind 0)))) (hash "0zy147r2i7v513kb0k7anpvaxcpv8lg12cc21s3rn9d4iwlqn3w1")))

(define-public crate-yogurt-yaml-0.0.1 (crate (name "yogurt-yaml") (vers "0.0.1") (deps (list (crate-dep (name "argparse") (req "^0.2.2") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.3.1") (default-features #t) (kind 0)) (crate-dep (name "yaml-rust") (req "^0.4.3") (default-features #t) (kind 0)))) (hash "1gdy1abrq4x3m2rm5jy48p3402dawlx755vw5zmsabgbmpsbxfvc")))

(define-public crate-yogurt-yaml-0.0.2 (crate (name "yogurt-yaml") (vers "0.0.2") (deps (list (crate-dep (name "argparse") (req "^0.2.2") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.3.1") (default-features #t) (kind 0)) (crate-dep (name "yaml-rust") (req "^0.4.3") (default-features #t) (kind 0)))) (hash "06kraf2v7sr1ax372k42nmqfh2gazihfym6p4gzfr3axcyqqkvyb")))

(define-public crate-yogurt-yaml-0.0.3 (crate (name "yogurt-yaml") (vers "0.0.3") (deps (list (crate-dep (name "argparse") (req "^0.2.2") (default-features #t) (kind 0)) (crate-dep (name "yaml-rust") (req "^0.4.3") (default-features #t) (kind 0)))) (hash "0qd378gdy8bfm1kldh07f0554z2syaxb822754b9awjj5wabbjfp")))

(define-public crate-yogurt-yaml-0.0.4 (crate (name "yogurt-yaml") (vers "0.0.4") (deps (list (crate-dep (name "argparse") (req "^0.2.2") (default-features #t) (kind 0)) (crate-dep (name "yaml-rust") (req "^0.4.3") (default-features #t) (kind 0)))) (hash "1h3hkv73pgdb9008d9ln88pp8agrk4ihppyfxa2dn64510bbizw5")))

(define-public crate-yogurt-yaml-0.1 (crate (name "yogurt-yaml") (vers "0.1.0") (deps (list (crate-dep (name "argparse") (req "^0.2.2") (default-features #t) (kind 0)) (crate-dep (name "criterion") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "yaml-rust") (req "^0.4.3") (default-features #t) (kind 0)))) (hash "1hdywzzyc4jq9pic1nx0jpbf0qmfspfia1v31r4glskzvc01vklp")))

(define-public crate-yogurt-yaml-0.1 (crate (name "yogurt-yaml") (vers "0.1.1") (deps (list (crate-dep (name "argparse") (req "^0.2.2") (default-features #t) (kind 0)) (crate-dep (name "criterion") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "yaml-rust") (req "^0.4.3") (default-features #t) (kind 0)))) (hash "0j4v7xpsyapdv5nmbij40q614d8hi2bm2gqxdcq095fl6npr4j8h")))

(define-public crate-yogurt-yaml-0.2 (crate (name "yogurt-yaml") (vers "0.2.0") (deps (list (crate-dep (name "argparse") (req "^0.2.2") (default-features #t) (kind 0)) (crate-dep (name "criterion") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "yaml-rust") (req "^0.4.3") (default-features #t) (kind 0)))) (hash "04whfi0gqkrivxld6cayads7nfhqviyih6ckyxp9rayzh0b1ci79")))

