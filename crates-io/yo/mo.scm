(define-module (crates-io yo mo) #:use-module (crates-io))

(define-public crate-yomo-0.1 (crate (name "yomo") (vers "0.1.0") (deps (list (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "1bndprsxjl9vj5941n8z45yqdwxzrzbgp8l7i3149wwjq5zqibnc") (yanked #t)))

(define-public crate-yomo-0.1 (crate (name "yomo") (vers "0.1.1") (deps (list (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "02nhvkf6lrfp1arn5v978ndkkfl5qx1bkc5vswhbnfaivvz3ia32") (yanked #t)))

(define-public crate-yomo-0.1 (crate (name "yomo") (vers "0.1.2") (deps (list (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "04dis2ynrig6wwpa8d0f2fbgxn7aa62p54zrgryp6agygj6xscwn") (yanked #t)))

(define-public crate-yomo-0.1 (crate (name "yomo") (vers "0.1.3") (deps (list (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "13xh54w0dqw60vpbv6095rsnb8qxw0619g4iyfvjl9hl9g1pzkib")))

(define-public crate-yomo-0.2 (crate (name "yomo") (vers "0.2.0") (deps (list (crate-dep (name "yomo_derive") (req "^0.2.0") (default-features #t) (kind 0)))) (hash "1ay4lmgj67azb663n9wbrw45jhn5x0frc4pcibyhpimb6k76dmdy")))

(define-public crate-yomo-0.3 (crate (name "yomo") (vers "0.3.0") (deps (list (crate-dep (name "yomo_derive") (req "^0.3.0") (default-features #t) (kind 0)))) (hash "0z737gxyrnbcl651mq9ggz6n95a9yax8ggkpxbxhzv4sy6z2ynhs")))

(define-public crate-yomo_derive-0.2 (crate (name "yomo_derive") (vers "0.2.0") (deps (list (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "1kkskg331m1nxlljraizgbsbr0f2dx21y7pa1xgwn7lnc4x62rjh") (features (quote (("default"))))))

(define-public crate-yomo_derive-0.3 (crate (name "yomo_derive") (vers "0.3.0") (deps (list (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "1gmkkmiqp05p910v6mmb7zcvild17z594z9fclydncb9ajrl3336") (features (quote (("default"))))))

