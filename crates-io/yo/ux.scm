(define-module (crates-io yo ux) #:use-module (crates-io))

(define-public crate-youx-0.1 (crate (name "youx") (vers "0.1.0") (hash "10xnprdjgz1p1cvsqh9pjscvr50b7b3qy5ac815svl1z62r6d0nx")))

(define-public crate-youxi-0.1 (crate (name "youxi") (vers "0.1.0") (deps (list (crate-dep (name "cgmath") (req "^0.17.0") (default-features #t) (kind 0)) (crate-dep (name "mini_gl_fb") (req "^0.6.0") (default-features #t) (kind 0)) (crate-dep (name "rusttype") (req "^0.7.3") (default-features #t) (kind 0)) (crate-dep (name "youxi-codegen") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "1p6b3h75zgbzdrfnlsjhbp31g9yh61gq130shsika0b4sbjd5px9")))

(define-public crate-youxi-codegen-0.1 (crate (name "youxi-codegen") (vers "0.1.0") (deps (list (crate-dep (name "proc-macro2") (req "^0.4.27") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^0.6.11") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^0.15.29") (features (quote ("parsing" "full" "extra-traits"))) (default-features #t) (kind 0)))) (hash "1khrvw0f6ygik6yh3i039f82z1ayryfh1x4vd1cyi7vpi81m07ds")))

