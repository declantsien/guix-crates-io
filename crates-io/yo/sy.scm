(define-module (crates-io yo sy) #:use-module (crates-io))

(define-public crate-yosys-netlist-json-0.0.1 (crate (name "yosys-netlist-json") (vers "0.0.1") (deps (list (crate-dep (name "serde") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)))) (hash "1cag7pj35kwixy60wmd2k5bnr2q9jg1b28z5z4nwxcqy7kvmg11k")))

(define-public crate-yosys-netlist-json-0.0.2 (crate (name "yosys-netlist-json") (vers "0.0.2") (deps (list (crate-dep (name "serde") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "slog") (req "^2") (default-features #t) (kind 0)))) (hash "1g2daknl70q8m50phz084cfvc1z8n8kj7iqmqvy42gdyxlidc5f5")))

(define-public crate-yosys-netlist-json-0.0.3 (crate (name "yosys-netlist-json") (vers "0.0.3") (deps (list (crate-dep (name "serde") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "slog") (req "^2") (default-features #t) (kind 0)))) (hash "0qg82bww8cvbwlmrnj0diyvvsf1gz1k9fkg5h0gb8ax80z9i48qh")))

(define-public crate-yosys-netlist-json-0.1 (crate (name "yosys-netlist-json") (vers "0.1.0") (deps (list (crate-dep (name "serde") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "slog") (req "^2") (optional #t) (default-features #t) (kind 0)))) (hash "0gyv79zabdyjlcgx80dac762zylnayspb4ycng9nl9y1zcshwpww") (features (quote (("default" "slog"))))))

