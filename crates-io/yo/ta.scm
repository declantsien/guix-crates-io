(define-module (crates-io yo ta) #:use-module (crates-io))

(define-public crate-yotasm-0.1 (crate (name "yotasm") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^4.5.4") (features (quote ("cargo"))) (default-features #t) (kind 0)))) (hash "0kplq60jdg024mpv0i194r58kq43wkv4rbj527403m18md61bz5s")))

