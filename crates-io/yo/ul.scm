(define-module (crates-io yo ul) #:use-module (crates-io))

(define-public crate-youlog-0.1 (crate (name "youlog") (vers "0.1.0") (deps (list (crate-dep (name "log") (req "^0.4.20") (features (quote ("std"))) (default-features #t) (kind 0)))) (hash "1hnqcvci1prc281511sij28kj9w7hdwncfwwqbk61nwxyi2j7249") (features (quote (("default"))))))

(define-public crate-youlog-0.1 (crate (name "youlog") (vers "0.1.1") (deps (list (crate-dep (name "log") (req "^0.4.20") (features (quote ("std"))) (default-features #t) (kind 0)))) (hash "1y48dip3l4vl1rxl977q7q4cbib4fz00lf6271l5vc6jrm608vxg") (features (quote (("default"))))))

(define-public crate-youlog-0.1 (crate (name "youlog") (vers "0.1.2") (deps (list (crate-dep (name "log") (req "^0.4.20") (features (quote ("std"))) (default-features #t) (kind 0)))) (hash "0mzg4ajlwfs3nka6ixng96h0casmvy6204q5bi0bd2qd49pwya5j") (features (quote (("default"))))))

