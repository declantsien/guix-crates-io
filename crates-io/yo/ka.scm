(define-module (crates-io yo ka) #:use-module (crates-io))

(define-public crate-yokai-0.0.0 (crate (name "yokai") (vers "0.0.0") (deps (list (crate-dep (name "crossterm") (req "^0.25.0") (default-features #t) (kind 0)) (crate-dep (name "kimono") (req "^0.1.3") (default-features #t) (kind 0)))) (hash "0c8g2ky1rqbvdq3vxzg3q2nf7s8scx949jg35p56zg7872wp7dfc")))

