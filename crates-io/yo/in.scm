(define-module (crates-io yo in) #:use-module (crates-io))

(define-public crate-yoin-0.0.1 (crate (name "yoin") (vers "0.0.1") (deps (list (crate-dep (name "clap") (req "^2.20") (default-features #t) (kind 0)) (crate-dep (name "yoin-core") (req "^0.0.1") (default-features #t) (kind 0)) (crate-dep (name "yoin-ipadic") (req "^0.0.1") (default-features #t) (kind 0)))) (hash "07vv9kyjp84qxaw63cpij3wc6ykynzv72qa4ncsyzv86rzy71zgb")))

(define-public crate-yoin-core-0.0.1 (crate (name "yoin-core") (vers "0.0.1") (deps (list (crate-dep (name "byteorder") (req "^1.0.0") (default-features #t) (kind 0)))) (hash "0h6v30a152hpza0g3bmf5jm9ixz6pxy92n80swpwv7i3ard4i02n")))

(define-public crate-yoin-ipadic-0.0.1 (crate (name "yoin-ipadic") (vers "0.0.1") (deps (list (crate-dep (name "bzip2") (req "^0.3") (default-features #t) (kind 1)) (crate-dep (name "tar") (req "^0.4") (default-features #t) (kind 1)) (crate-dep (name "yoin-core") (req "^0.0.1") (default-features #t) (kind 0)))) (hash "0nmkg6ycmpj7b8nz2q1s3fyvqhdv7nl92jfzipgsnw9pzk74g8j1")))

(define-public crate-yoink-0.0.0 (crate (name "yoink") (vers "0.0.0") (hash "0lxh3imliwfqy6bn3xmj75a8mh5ywjd936576ms3jgh5hqd9r022")))

