(define-module (crates-io yo uc) #:use-module (crates-io))

(define-public crate-youchoose-0.1 (crate (name "youchoose") (vers "0.1.0") (deps (list (crate-dep (name "ncurses") (req "^5.101.0") (default-features #t) (kind 0)))) (hash "0wzbpm4yrpiw7gvw04cfy34k90rkjyxqndsz3ggaggn5nwrwz1wd")))

(define-public crate-youchoose-0.1 (crate (name "youchoose") (vers "0.1.1") (deps (list (crate-dep (name "ncurses") (req "^5.101.0") (default-features #t) (kind 0)))) (hash "10srnvk7wv7gklqkcrrfi225m2vkf0cby60ldiz25vrqhzp3lq1q")))

