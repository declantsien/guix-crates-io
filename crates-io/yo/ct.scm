(define-module (crates-io yo ct) #:use-module (crates-io))

(define-public crate-yocto-0.1 (crate (name "yocto") (vers "0.1.0") (deps (list (crate-dep (name "ansi_term") (req "^0.10.0") (default-features #t) (kind 0)) (crate-dep (name "chashmap") (req "^2.2.2") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^2.32.0") (default-features #t) (kind 0)) (crate-dep (name "isatty") (req "^0.1.3") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "unicode-segmentation") (req "^1.2.0") (default-features #t) (kind 0)))) (hash "1iclm93ps8nvz9acwipbqydcwkl5xa1gvv0qji6ll4rd0c8pv8ha")))

(define-public crate-yocto-0.1 (crate (name "yocto") (vers "0.1.1") (deps (list (crate-dep (name "ansi_term") (req "^0.10.0") (default-features #t) (kind 0)) (crate-dep (name "chashmap") (req "^2.2.2") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^2.32.0") (default-features #t) (kind 0)) (crate-dep (name "isatty") (req "^0.1.3") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "unicode-segmentation") (req "^1.2.0") (default-features #t) (kind 0)))) (hash "148qzd5y21wq3dqnchdr8a9f9yq62d5a2y97w971kyyqxlm6r0jp")))

(define-public crate-yocto-0.1 (crate (name "yocto") (vers "0.1.2") (deps (list (crate-dep (name "ansi_term") (req "^0.10.0") (default-features #t) (kind 0)) (crate-dep (name "chashmap") (req "^2.2.2") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^2.32.0") (default-features #t) (kind 0)) (crate-dep (name "isatty") (req "^0.1.3") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "unicode-segmentation") (req "^1.2.0") (default-features #t) (kind 0)))) (hash "1bbgbb1fpfs27vkrrkv8f0ynw96bcdggig92nl2jxd8j6c5iqxfx")))

(define-public crate-yocto-0.1 (crate (name "yocto") (vers "0.1.3") (deps (list (crate-dep (name "ansi_term") (req "^0.10.0") (default-features #t) (kind 0)) (crate-dep (name "chashmap") (req "^2.2.2") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^2.32.0") (default-features #t) (kind 0)) (crate-dep (name "isatty") (req "^0.1.3") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "unicode-segmentation") (req "^1.2.0") (default-features #t) (kind 0)))) (hash "00d3bzxv3zmnzq5mpcwg7z25d9b82r2a4wf97vh2vlm3ajnc1p6f")))

(define-public crate-yocto-0.1 (crate (name "yocto") (vers "0.1.4") (deps (list (crate-dep (name "ansi_term") (req "^0.10.0") (default-features #t) (kind 0)) (crate-dep (name "chashmap") (req "^2.2.2") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^2.32.0") (default-features #t) (kind 0)) (crate-dep (name "isatty") (req "^0.1.3") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "unicode-segmentation") (req "^1.2.0") (default-features #t) (kind 0)))) (hash "0mjz62zws3z2g48avj4j0qc1lrxvyjdiyn9115vfcxwa5569fyvy")))

(define-public crate-yocto-0.1 (crate (name "yocto") (vers "0.1.5") (deps (list (crate-dep (name "ansi_term") (req "^0.10.0") (default-features #t) (kind 0)) (crate-dep (name "chashmap") (req "^2.2.2") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^2.32.0") (default-features #t) (kind 0)) (crate-dep (name "isatty") (req "^0.1.3") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "unicode-segmentation") (req "^1.2.0") (default-features #t) (kind 0)))) (hash "1l2yf8vhbmicnz51s9hvwv82a1rlz0s88bdsi944b39qqc0l9sji")))

(define-public crate-yocto-0.2 (crate (name "yocto") (vers "0.2.0") (deps (list (crate-dep (name "ansi_term") (req "^0.10.0") (default-features #t) (kind 0)) (crate-dep (name "chashmap") (req "^2.2.2") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^2.32.0") (default-features #t) (kind 0)) (crate-dep (name "isatty") (req "^0.1.3") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "unicode-segmentation") (req "^1.2.0") (default-features #t) (kind 0)))) (hash "0nkm26ppsbi0wh6qnas1x9w13si06d35kw0ybh0mjjzrfqwvb5qk")))

(define-public crate-yocto-0.3 (crate (name "yocto") (vers "0.3.0") (deps (list (crate-dep (name "ansi_term") (req "^0.10.0") (default-features #t) (kind 0)) (crate-dep (name "chashmap") (req "^2.2.2") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^2.32.0") (default-features #t) (kind 0)) (crate-dep (name "isatty") (req "^0.1.3") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "unicode-segmentation") (req "^1.2.0") (default-features #t) (kind 0)))) (hash "1jis47vgm52kb5wlvpsn12gqicha1g52jr1hmbp2vpbadbahh6q4")))

(define-public crate-yocto_client-0.0.1 (crate (name "yocto_client") (vers "0.0.1") (deps (list (crate-dep (name "log") (req "^0.3.0") (default-features #t) (kind 2)) (crate-dep (name "yocto") (req "^0.1.1") (default-features #t) (kind 2)))) (hash "0sph7r8sbadcqlpgs83q68cys77i6nmswxvw8vwbax2bpz8hs5gv")))

(define-public crate-yocto_client-0.0.2 (crate (name "yocto_client") (vers "0.0.2") (deps (list (crate-dep (name "log") (req "^0.3.0") (default-features #t) (kind 2)) (crate-dep (name "yocto") (req "^0.2.0") (default-features #t) (kind 2)))) (hash "0l8r5lwp29friwg5z7wj9j7lsm13bp2lp8af3jv5i4pz4h5y9zm9")))

(define-public crate-yocto_client-0.1 (crate (name "yocto_client") (vers "0.1.0") (deps (list (crate-dep (name "log") (req "^0.3.0") (default-features #t) (kind 2)) (crate-dep (name "yocto") (req "^0.3.0") (default-features #t) (kind 2)))) (hash "1107jcbqhalxjz18jh3vig64zc8zy5hnp4252mzbhrzq2fjg2184")))

(define-public crate-yoctolio-0.1 (crate (name "yoctolio") (vers "0.1.0") (deps (list (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)))) (hash "1m5140qybjk1wjasc3k2wc84bzhy7baw07blakh2ahw353npxhmf")))

(define-public crate-yoctolio-0.1 (crate (name "yoctolio") (vers "0.1.1") (deps (list (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)))) (hash "15lq7jimr7r9q5lkvvw0b8rwkjrl0h23785686wvljysw909i0jr")))

(define-public crate-yoctolio-0.1 (crate (name "yoctolio") (vers "0.1.2") (deps (list (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)))) (hash "0japqk7pdjb7rqijng39zpx9p5ga44arssfqrynhp54c2wxrrp4w")))

(define-public crate-yoctolio-0.1 (crate (name "yoctolio") (vers "0.1.3") (deps (list (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)))) (hash "085yh6p5vjrgdn5pp5lcs8vn39izs0v0l2cwdc2z1xnbbnkz5dj4")))

(define-public crate-yoctolio-0.2 (crate (name "yoctolio") (vers "0.2.0") (deps (list (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)))) (hash "1hscx3y5yjd03k3i11kvbb1151i7r73b03fg7669yphyljyq0j9y")))

(define-public crate-yoctolio-0.3 (crate (name "yoctolio") (vers "0.3.0") (deps (list (crate-dep (name "clap") (req "^3.0.14") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)))) (hash "04rz29d7ijgs6fh6gn0755rnw507pib2vg16ijk4v02z6y18b56d")))

(define-public crate-yoctolio-0.4 (crate (name "yoctolio") (vers "0.4.0") (deps (list (crate-dep (name "clap") (req "^4.2.2") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)))) (hash "17dz499yk282ighvwmh5kc3hg3sr7y7ggr53pykmbd4bf8pfdw6l")))

