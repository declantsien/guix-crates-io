(define-module (crates-io yo ns) #:use-module (crates-io))

(define-public crate-yonsei-flexible-0.1 (crate (name "yonsei-flexible") (vers "0.1.0") (hash "0d0adq19dfm2agkrqqfdw26s68djwmxbpzn4dx3n2c9nm1hbgypv")))

(define-public crate-yonsei-flexible-0.1 (crate (name "yonsei-flexible") (vers "0.1.1") (hash "1mb4z7qb59bvb13w1bbpq9vbd8z473l11czfw3i4a0aycq2yh9sr")))

(define-public crate-yonsei-flexible-0.2 (crate (name "yonsei-flexible") (vers "0.2.0") (deps (list (crate-dep (name "dialoguer") (req "^0.10.2") (default-features #t) (kind 0)))) (hash "1m2043jb7sbpgk1irk9gw1mkgljj5x3s14n7jxg15s13cyqf3ny8")))

(define-public crate-yonsei-flexible-0.2 (crate (name "yonsei-flexible") (vers "0.2.1") (deps (list (crate-dep (name "dialoguer") (req "^0.10.2") (default-features #t) (kind 0)) (crate-dep (name "dont_disappear") (req "^3.0.1") (default-features #t) (kind 0)))) (hash "0fmk0h3qblxrpxwyww2x3c3ss9014116sn79yvaflsz8link2l0q")))

