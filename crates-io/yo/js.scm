(define-module (crates-io yo js) #:use-module (crates-io))

(define-public crate-yojson-rs-0.1 (crate (name "yojson-rs") (vers "0.1.0") (deps (list (crate-dep (name "pest") (req "^2.1.3") (default-features #t) (kind 0)) (crate-dep (name "pest_derive") (req "^2.1.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.117") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0.117") (default-features #t) (kind 0)))) (hash "1qd6bjdbrnykn5162wh779hygmfcaj1jk11sqlyvzgpvdvaasw5p") (yanked #t)))

(define-public crate-yojson-rs-0.1 (crate (name "yojson-rs") (vers "0.1.1") (deps (list (crate-dep (name "pest") (req "^2.1.3") (default-features #t) (kind 0)) (crate-dep (name "pest_derive") (req "^2.1.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.117") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0.117") (default-features #t) (kind 0)))) (hash "0x11c01pndi03x3h7ljjp9vxzhknsrqkxlyl00im84w6q9axx41i")))

(define-public crate-yojson-rs-0.1 (crate (name "yojson-rs") (vers "0.1.2") (deps (list (crate-dep (name "pest") (req "^2.1.3") (default-features #t) (kind 0)) (crate-dep (name "pest_derive") (req "^2.1.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.117") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0.117") (default-features #t) (kind 0)))) (hash "1xxc4csgw0yz287ah68dxy3hd7a3ybij2al7cszb8pvcmsg0hn43")))

