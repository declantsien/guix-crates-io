(define-module (crates-io yo ts) #:use-module (crates-io))

(define-public crate-yotsuba-0.0.1 (crate (name "yotsuba") (vers "0.0.1") (hash "0q6y2dw1z22frjwcx371iarhgk0wj0lsngwnmjbvwpy300wngsi1") (yanked #t)))

(define-public crate-yotsuba-0.0.2 (crate (name "yotsuba") (vers "0.0.2") (hash "0wfcy71c38pf76qnpsygy4bhqlr15yqdmf5cb08vnydlrnjav4d5") (yanked #t)))

