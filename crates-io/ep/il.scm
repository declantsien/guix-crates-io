(define-module (crates-io ep il) #:use-module (crates-io))

(define-public crate-epilog-0.1 (crate (name "epilog") (vers "0.1.0") (deps (list (crate-dep (name "proptest") (req "^0.9.5") (default-features #t) (kind 2)))) (hash "0rx5s5gvc7yvd2i9qis2l0k6lbq18i3m45arl7wgs8qqlp42b7hz")))

