(define-module (crates-io ep an) #:use-module (crates-io))

(define-public crate-epan-0.1 (crate (name "epan") (vers "0.1.0") (deps (list (crate-dep (name "bindgen") (req "^0.30.0") (default-features #t) (kind 1)))) (hash "0rva5c5v29i15pv4v3h8c4gh0sprdsvyy4jmzn6nh5fly7d1z0mm")))

(define-public crate-epan-sys-0.1 (crate (name "epan-sys") (vers "0.1.0") (deps (list (crate-dep (name "bindgen") (req "^0.66") (optional #t) (default-features #t) (kind 1)) (crate-dep (name "cmake") (req "^0.1") (default-features #t) (kind 1)) (crate-dep (name "pkg-config") (req "^0.3") (default-features #t) (kind 1)))) (hash "1jpr6kxnq4l6h4vsdx3d8scavlw5y4qy1vcphhg7vwsk55nvwfiz") (links "wireshark") (v 2) (features2 (quote (("bindgen" "dep:bindgen"))))))

