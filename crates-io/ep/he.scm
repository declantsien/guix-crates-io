(define-module (crates-io ep he) #:use-module (crates-io))

(define-public crate-ephem-0.1 (crate (name "ephem") (vers "0.1.0-alpha") (deps (list (crate-dep (name "approx") (req "^0.3.2") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2.14") (default-features #t) (kind 0)))) (hash "1jlfgfbrsqih4gkmzbsjwn20x7bds74ya88b1dg8kpkjk2xkq71l")))

(define-public crate-ephem-0.1 (crate (name "ephem") (vers "0.1.0-alpha1") (deps (list (crate-dep (name "approx") (req "^0.3.2") (default-features #t) (kind 2)) (crate-dep (name "chrono") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "ephem_derive") (req "^0.1.0-alpha") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2.14") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.0") (default-features #t) (kind 2)))) (hash "10667vp7kdgi3r07k2hbarckihb8dnqyip600nr35vpkd0pvzgdj")))

(define-public crate-ephem_derive-0.1 (crate (name "ephem_derive") (vers "0.1.0-alpha") (deps (list (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "05vpg3r7gpjrw84hg7lrb607825xn2lkk3aivyhsx6ixb6qmdhdf")))

(define-public crate-ephemeral-0.1 (crate (name "ephemeral") (vers "0.1.0") (hash "0aqm4yw9y9aq6yz5ddv1hhx62v7yhglk4g1gpkrkb3abw2wkzxb1")))

(define-public crate-ephemeral-0.2 (crate (name "ephemeral") (vers "0.2.0") (hash "158z7ykq9ylxm4acipkhfwngfwc330k7ypa8wc1a871a2ck3kzdx")))

(define-public crate-ephemeral-0.2 (crate (name "ephemeral") (vers "0.2.1") (hash "11rmf7127hb9b7pf0zy7xlbsq9qvb13znwi0q3mbwhz29zyadkzy")))

(define-public crate-ephemerides-0.1 (crate (name "ephemerides") (vers "0.1.0") (hash "0js92qxk89ah5sj37z6870q7iaph14f2fvjc7kn5ys32cz620zz6")))

(define-public crate-ephemeris-0.1 (crate (name "ephemeris") (vers "0.1.0-alpha.0") (deps (list (crate-dep (name "proptest") (req "0.*") (default-features #t) (kind 2)))) (hash "0a6kbdlkhlmwdlyrc3yra8i9j1ppzpi9li9dbyf8921nlrd2rfk4")))

(define-public crate-ephemeris-0.1 (crate (name "ephemeris") (vers "0.1.0-alpha.2") (deps (list (crate-dep (name "proptest") (req "0.*") (default-features #t) (kind 2)))) (hash "0cyy6h3n28m6amfw2cd9fgadl7sb18d7d2kf4ywgarfp461ck3yj")))

(define-public crate-ephemeropt-0.1 (crate (name "ephemeropt") (vers "0.1.0") (deps (list (crate-dep (name "mock_instant") (req "^0.3.1") (default-features #t) (kind 2)) (crate-dep (name "sysinfo") (req "^0.29.7") (kind 2)) (crate-dep (name "tokio") (req "^1.30.0") (features (quote ("rt-multi-thread" "macros" "sync" "time"))) (default-features #t) (kind 2)))) (hash "0lbqaa5fiwdxx37fi6lvm0wq36jq4diq7fr36ik1b8r8wksxrw2r")))

(define-public crate-ephemeropt-0.2 (crate (name "ephemeropt") (vers "0.2.0") (deps (list (crate-dep (name "mock_instant") (req "^0.3.1") (default-features #t) (kind 2)) (crate-dep (name "sysinfo") (req "^0.29.7") (kind 2)) (crate-dep (name "tokio") (req "^1.30.0") (features (quote ("rt-multi-thread" "macros" "sync" "time"))) (default-features #t) (kind 2)))) (hash "0bx1xifvspi80qqal1za3b30cd189n7lckfg8ji36hzw9ixijrli")))

(define-public crate-ephemeropt-0.2 (crate (name "ephemeropt") (vers "0.2.1") (deps (list (crate-dep (name "mock_instant") (req "^0.3.1") (default-features #t) (kind 2)) (crate-dep (name "sysinfo") (req "^0.29.7") (kind 2)) (crate-dep (name "tokio") (req "^1.30.0") (features (quote ("rt-multi-thread" "macros" "sync" "time"))) (default-features #t) (kind 2)))) (hash "0h07bqh3469v1y3lskd3cff40pbrgasx5f9x2kdq5vdi3hl4qrx8")))

(define-public crate-ephemeropt-0.2 (crate (name "ephemeropt") (vers "0.2.2") (deps (list (crate-dep (name "mock_instant") (req "^0.3.1") (default-features #t) (kind 2)) (crate-dep (name "sysinfo") (req "^0.29.7") (kind 2)) (crate-dep (name "tokio") (req "^1.30.0") (features (quote ("rt-multi-thread" "macros" "sync" "time"))) (default-features #t) (kind 2)))) (hash "1znv2217p2j9pvcd2d91fkhiv0fg34i7szjhc43mfqc7fcbaxcxl")))

(define-public crate-ephemeropt-0.3 (crate (name "ephemeropt") (vers "0.3.0") (deps (list (crate-dep (name "mock_instant") (req "^0.3.1") (default-features #t) (kind 2)) (crate-dep (name "sysinfo") (req "^0.29.7") (kind 2)) (crate-dep (name "tokio") (req "^1.30.0") (features (quote ("rt-multi-thread" "macros" "sync" "time"))) (default-features #t) (kind 2)))) (hash "1naqb0hyaxk5g1nmzsq3fpvnparsmnqwiyirvha2vj90cykxq7ac")))

