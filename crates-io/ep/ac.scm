(define-module (crates-io ep ac) #:use-module (crates-io))

(define-public crate-epac-utils-0.1 (crate (name "epac-utils") (vers "0.1.0") (deps (list (crate-dep (name "anyhow") (req "^1.0.63") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "find_folder") (req "^0.3.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "piston_window") (req "^0.124.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "tracing") (req "^0.1.36") (optional #t) (default-features #t) (kind 0)))) (hash "1839j5qrdxrpnwc9fn1rh3mh4g6isgkd2z9bpwxwr59y1xcna5c1") (features (quote (("default" "anyhow" "tracing")))) (yanked #t) (v 2) (features2 (quote (("tracing" "dep:tracing") ("piston_cacher" "dep:piston_window" "dep:find_folder") ("anyhow" "dep:anyhow"))))))

