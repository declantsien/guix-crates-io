(define-module (crates-io ep oc) #:use-module (crates-io))

(define-public crate-epoch-0.0.0 (crate (name "epoch") (vers "0.0.0") (deps (list (crate-dep (name "chrono") (req "^0.4.6") (default-features #t) (kind 0)))) (hash "05sg97cdm4n1vi2fhaxsbzbi1jb2v68fpsmcblppgf11x6f3nbgb")))

(define-public crate-epoch-0.0.1 (crate (name "epoch") (vers "0.0.1") (deps (list (crate-dep (name "chrono") (req "^0.4.6") (default-features #t) (kind 0)))) (hash "1ipjzkv8mqlharkvi04wfv3h86m0sxg3nrh9zsf29z06k9ky6wz0")))

(define-public crate-epoch-0.0.2 (crate (name "epoch") (vers "0.0.2") (deps (list (crate-dep (name "chrono") (req "^0.4.6") (default-features #t) (kind 0)))) (hash "1xrr0rf36bcx595bjvvx83mqpqw25vwzqkdhv9jdgiz911p1ysza")))

(define-public crate-epoch-calc-0.1 (crate (name "epoch-calc") (vers "0.1.0") (deps (list (crate-dep (name "chrono") (req "^0.4.27") (default-features #t) (kind 0)))) (hash "01bs59qc1gm1d82in435yklgj9yni3iz3myvlbi4wmvva24cy5nc")))

(define-public crate-epoch-calc-0.1 (crate (name "epoch-calc") (vers "0.1.1") (deps (list (crate-dep (name "chrono") (req "^0.4.27") (default-features #t) (kind 0)))) (hash "1i0pik65ndrl6wg0v63nxmyd747i2sq2vp7vdc2s7nlcp80md1v5")))

(define-public crate-epoch-calc-0.1 (crate (name "epoch-calc") (vers "0.1.2") (deps (list (crate-dep (name "chrono") (req "^0.4.27") (default-features #t) (kind 0)))) (hash "11i16gqg8mwxpjrdf9dp28b9g52bfqr5fq3506w8h4k3bhi1jlii")))

(define-public crate-epoch-cli-0.1 (crate (name "epoch-cli") (vers "0.1.0") (deps (list (crate-dep (name "chrono") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^2.33") (features (quote ("yaml"))) (default-features #t) (kind 0)))) (hash "1ix2hrziqki8rzaydhrwzsj2jxsziqlqy0jd7l76kbcpwbyh4cpw")))

(define-public crate-epoch-cli-0.1 (crate (name "epoch-cli") (vers "0.1.1") (deps (list (crate-dep (name "chrono") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^2.33") (features (quote ("yaml"))) (default-features #t) (kind 0)))) (hash "1ncjzw9nlvxv0wgaa7sf0f866fd4j7n79sc7in4mxzn0kji98pv9")))

(define-public crate-epoch-cli-0.1 (crate (name "epoch-cli") (vers "0.1.2") (deps (list (crate-dep (name "chrono") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^2.33") (features (quote ("yaml"))) (default-features #t) (kind 0)))) (hash "05lz1lpwm09in00sz1gyyvy0r75wz56i8dv0pxx9vmgxw5fvfghm")))

(define-public crate-epoch-cli-0.1 (crate (name "epoch-cli") (vers "0.1.3") (deps (list (crate-dep (name "chrono") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^2.33") (features (quote ("yaml"))) (default-features #t) (kind 0)))) (hash "0g4yg95w3m94ga3diygi15zpza27cznjkvap8gxcphp2zw4yfyp8")))

(define-public crate-epoch-cli-0.1 (crate (name "epoch-cli") (vers "0.1.4") (deps (list (crate-dep (name "chrono") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^2.33") (features (quote ("yaml"))) (default-features #t) (kind 0)))) (hash "0cxh4jzbd4q9x7yg5nnfa2sz1d98qvrq6vbdxf0hgilqjyi5sxz4")))

(define-public crate-epoch-cli-0.1 (crate (name "epoch-cli") (vers "0.1.5") (deps (list (crate-dep (name "chrono") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^2.33") (features (quote ("yaml"))) (default-features #t) (kind 0)))) (hash "0chi8dlynw39fiiacyy9k48n6k8xlzmc18nkqfma147i8s9qck81")))

(define-public crate-epoch-cli-0.1 (crate (name "epoch-cli") (vers "0.1.6") (deps (list (crate-dep (name "chrono") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^2.33") (features (quote ("yaml"))) (default-features #t) (kind 0)))) (hash "1qy8b7nzapbgjabwj9b0phr9md48i9fpnj287mngmqa2c4cwdsqh")))

(define-public crate-epoch-cli-0.1 (crate (name "epoch-cli") (vers "0.1.7") (deps (list (crate-dep (name "chrono") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^2.33") (features (quote ("yaml"))) (default-features #t) (kind 0)))) (hash "023cw9hjv9xidsi2cw8zicsnk1cfln458pibqnzvpr51cqhd6nb6")))

(define-public crate-epoch-cli-0.2 (crate (name "epoch-cli") (vers "0.2.0") (deps (list (crate-dep (name "chrono") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^2.33") (features (quote ("yaml"))) (default-features #t) (kind 0)))) (hash "0fh0cpc0j26dv59h3qvf5pirpbk3wd6fv9vwaxsavn8ssgyw3gpm")))

(define-public crate-epoch-cli-0.2 (crate (name "epoch-cli") (vers "0.2.1") (deps (list (crate-dep (name "chrono") (req "^0.4") (features (quote ("clock"))) (kind 0)) (crate-dep (name "clap") (req "^2.33") (features (quote ("yaml"))) (default-features #t) (kind 0)))) (hash "04c80vh20fqy1a7aj3l63zsms0lfw4qdlv9c1q9w1b38gs9ma25l")))

(define-public crate-epoch-cli-1 (crate (name "epoch-cli") (vers "1.0.0") (deps (list (crate-dep (name "clap") (req "^3.1") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "time") (req "^0.3") (features (quote ("large-dates"))) (default-features #t) (kind 0)))) (hash "1fs3dayrwz6962f43pjpvzdnhigbyx50s9wnzc75d2f2fwmplbdz")))

(define-public crate-epoch-cli-1 (crate (name "epoch-cli") (vers "1.0.1") (deps (list (crate-dep (name "clap") (req "^4.1") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "time") (req "^0.3") (features (quote ("large-dates"))) (default-features #t) (kind 0)))) (hash "0vv41388rw9ggjn1cjyj8in6f8jq26zmi74mkgkszk8gns9zn7ib")))

(define-public crate-epoch-converter-0.1 (crate (name "epoch-converter") (vers "0.1.0") (hash "0khf8qkk74w0r00gv4kb7iddrsb65ra6vgall6jb6sh2dpyxjxj5")))

(define-public crate-epoch-converter-0.1 (crate (name "epoch-converter") (vers "0.1.1") (hash "00ckbqqzycn01hzrxsa6iqwxw3629jsj90kyvhpb09wk0h1nxww2")))

(define-public crate-epoch-converter-0.1 (crate (name "epoch-converter") (vers "0.1.2") (deps (list (crate-dep (name "log") (req "^0.4.14") (default-features #t) (kind 0)))) (hash "1k1pcdn8lc42kxzcmhhhpznb0jcck11ip6g43qa70pamsvnh7zbb") (yanked #t)))

(define-public crate-epoch-converter-0.1 (crate (name "epoch-converter") (vers "0.1.3") (deps (list (crate-dep (name "log") (req "^0.4.14") (default-features #t) (kind 0)))) (hash "0rglmxks0qxswpr7hfrl9imcaizf5aiz6c95w4k80zja973zhd86")))

(define-public crate-epoch-get-0.1 (crate (name "epoch-get") (vers "0.1.0") (deps (list (crate-dep (name "colored") (req "^2.0.0") (default-features #t) (kind 0)))) (hash "11hmy7kp0xblxwyhj56z5khyppibnw1ps1ynhmsgnkkqhg8rf2fs")))

(define-public crate-epoch-get-0.1 (crate (name "epoch-get") (vers "0.1.1") (deps (list (crate-dep (name "colored") (req "^2.0.0") (default-features #t) (kind 0)))) (hash "0bnp0a0pb8v8w1x3340nq5dm0x52w8kzfph416rk7lrgzv78j755")))

(define-public crate-epoch-get-0.2 (crate (name "epoch-get") (vers "0.2.0") (deps (list (crate-dep (name "colored") (req "^2.0.0") (default-features #t) (kind 0)))) (hash "1wmr91gz3vr9hc11lnnc7jzi6w82a83z4xb1xmxgd6yi5y7g9fv0")))

(define-public crate-epoch-get-0.3 (crate (name "epoch-get") (vers "0.3.0") (deps (list (crate-dep (name "colored") (req "^2.0.0") (default-features #t) (kind 0)))) (hash "085f694l887mw3gfpr2xdkb9gibfrd4zy3a7mmy64ghaj34rw4x7")))

(define-public crate-epoch-get-0.4 (crate (name "epoch-get") (vers "0.4.0") (deps (list (crate-dep (name "colored") (req "^2.0.0") (default-features #t) (kind 0)))) (hash "1ylmnp4kcsrrw5wi8xwxz6lax1ydbl9jhm5q3q8247r5q0pqah0d")))

(define-public crate-epoch-get-0.4 (crate (name "epoch-get") (vers "0.4.1") (deps (list (crate-dep (name "colored") (req "^2.0.0") (default-features #t) (kind 0)))) (hash "1b4a55kds5kilzgwvskcld176p76zn448807wm8gnss7i7csg1av")))

(define-public crate-epoch-timestamp-1 (crate (name "epoch-timestamp") (vers "1.0.0") (hash "1p35x20nnamr82g1zz2bxqgsz0qfg1lx5wqfnjzhya9fxck0d1gq")))

(define-public crate-epoch_jd-0.1 (crate (name "epoch_jd") (vers "0.1.0") (hash "1sa64ilm2dd600z152plsg9inqnnd79lz37m9aw83vhmvkh06j0c")))

(define-public crate-epochs-0.2 (crate (name "epochs") (vers "0.2.0") (deps (list (crate-dep (name "chrono") (req "^0.4") (default-features #t) (kind 0)))) (hash "0d66x9qs8s9ihm31fgmy3kyb843wp6hhp1gy8n13smkmiaza3x8b")))

(define-public crate-epochs-0.2 (crate (name "epochs") (vers "0.2.1") (deps (list (crate-dep (name "chrono") (req "^0.4") (default-features #t) (kind 0)))) (hash "04dwscymksm7gl0c7870zbf2hbp7nghxyxh23d1yq0sfmxvsymrs")))

(define-public crate-epochs-0.2 (crate (name "epochs") (vers "0.2.2") (deps (list (crate-dep (name "chrono") (req "^0.4") (default-features #t) (kind 0)))) (hash "0av8bga0b853q9yf85dvbjpca7bg7pdmcd2nnc748c7pj9v81lyi")))

(define-public crate-epochs-0.2 (crate (name "epochs") (vers "0.2.3") (deps (list (crate-dep (name "chrono") (req "^0.4") (default-features #t) (kind 0)))) (hash "12z992x8nxdxrq8sjvlgynfikhkk1b3264nxndh8p6d6j0wlj2w4")))

(define-public crate-epochs-0.2 (crate (name "epochs") (vers "0.2.4") (deps (list (crate-dep (name "chrono") (req "^0.4") (default-features #t) (kind 0)))) (hash "0r071qigfcrx1i6his5wbmczbc500hhv215m40qn0nw6a5wy746h")))

(define-public crate-epochs-cli-0.2 (crate (name "epochs-cli") (vers "0.2.0") (deps (list (crate-dep (name "chrono") (req "^0.4") (features (quote ("serde"))) (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^2.33") (default-features #t) (kind 0)) (crate-dep (name "epochs") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "itertools") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.4") (default-features #t) (kind 0)) (crate-dep (name "maplit") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "structopt-derive") (req "^0.4") (default-features #t) (kind 0)))) (hash "1l9y9cfpwp334x3a0chjb4pdybb71v7q31via3rq5lgxz8nhvhc1")))

(define-public crate-epochs-cli-0.3 (crate (name "epochs-cli") (vers "0.3.0") (deps (list (crate-dep (name "chrono") (req "^0.4") (features (quote ("serde"))) (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^3.2") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "epochs") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "itertools") (req "^0.10") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.4") (default-features #t) (kind 0)) (crate-dep (name "maplit") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)))) (hash "10ajyqk40jiwx7zxlj8kajvb48x7kqy852i9ig9ynhrspzlv54f3")))

(define-public crate-epochs-cli-0.4 (crate (name "epochs-cli") (vers "0.4.0") (deps (list (crate-dep (name "chrono") (req "^0.4") (features (quote ("serde"))) (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^3.2") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "crockford") (req "^1.1") (default-features #t) (kind 0)) (crate-dep (name "epochs") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "itertools") (req "^0.10") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.4") (default-features #t) (kind 0)) (crate-dep (name "maplit") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)))) (hash "1k0cxf4zd95yn434znphaklkfk2d7xvf1032ivfn7wjzka4nfrch")))

(define-public crate-epochs-cli-0.4 (crate (name "epochs-cli") (vers "0.4.1") (deps (list (crate-dep (name "chrono") (req "^0.4") (features (quote ("serde"))) (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^3.2") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "crockford") (req "^1.1") (default-features #t) (kind 0)) (crate-dep (name "epochs") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "itertools") (req "^0.10") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.4") (default-features #t) (kind 0)) (crate-dep (name "maplit") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)))) (hash "019gwl59r6h8hvnvgsq3chn0gh4pa88jqvk8rg9j2g17fy3cabj5")))

(define-public crate-epochs-cli-0.5 (crate (name "epochs-cli") (vers "0.5.0") (deps (list (crate-dep (name "chrono") (req "^0.4") (features (quote ("serde"))) (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^3.2") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "crockford") (req "^1.1") (default-features #t) (kind 0)) (crate-dep (name "epochs") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "itertools") (req "^0.10") (default-features #t) (kind 0)) (crate-dep (name "maplit") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "once_cell") (req "^1.14") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)))) (hash "060xhjcpsm1p25lgmjnqp4m7lpgp80r33k0azrlrdqaaf10x4283")))

