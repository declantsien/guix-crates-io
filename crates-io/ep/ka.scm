(define-module (crates-io ep ka) #:use-module (crates-io))

(define-public crate-epkard-0.1 (crate (name "epkard") (vers "0.1.1") (deps (list (crate-dep (name "colored") (req "^1.7.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.89") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0.89") (default-features #t) (kind 0)) (crate-dep (name "serde_yaml") (req "^0.8.8") (default-features #t) (kind 0)) (crate-dep (name "terminal_size") (req "^0.1.8") (default-features #t) (kind 0)))) (hash "18dmx47qgzy3vbzkgpmqx74cjjnqzq1hkx22k2qiixrg0rczsil4")))

