(define-module (crates-io ep so) #:use-module (crates-io))

(define-public crate-epson-0.1 (crate (name "epson") (vers "0.1.0") (deps (list (crate-dep (name "image") (req "^0") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1") (features (quote ("io-util"))) (optional #t) (kind 0)))) (hash "0709h5px8ags73yxn7v5f7hp1x8kdsd57vhsbb84grng52n2rp3i") (v 2) (features2 (quote (("tokio" "dep:tokio"))))))

(define-public crate-epson-0.1 (crate (name "epson") (vers "0.1.1") (deps (list (crate-dep (name "image") (req "^0") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1") (features (quote ("io-util"))) (optional #t) (kind 0)))) (hash "0g05p0671188sc9yjgm9hzz2ym502b06wsjkdawzyixh2yxlwsca") (v 2) (features2 (quote (("tokio" "dep:tokio"))))))

(define-public crate-epson-0.1 (crate (name "epson") (vers "0.1.2") (deps (list (crate-dep (name "image") (req "^0") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1") (features (quote ("io-util"))) (optional #t) (kind 0)))) (hash "1y2xal3ycp0l9s1nvrfcsqwd42q9jzghsj4d7ibj63s7q6piqwhf") (v 2) (features2 (quote (("tokio" "dep:tokio"))))))

(define-public crate-epson-0.1 (crate (name "epson") (vers "0.1.3") (deps (list (crate-dep (name "image") (req "^0") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1") (features (quote ("io-util"))) (optional #t) (kind 0)))) (hash "0zkdgb93sr0hd1l59qqid90yl6qvhr7gg66qds7yxizyzn1wv7ca") (v 2) (features2 (quote (("tokio" "dep:tokio"))))))

(define-public crate-epson-0.1 (crate (name "epson") (vers "0.1.4") (deps (list (crate-dep (name "image") (req "^0") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1") (features (quote ("io-util"))) (optional #t) (kind 0)))) (hash "0cdcwrihnqgn1qblyqcmd9mr1fnlaxzz8p1pqi3pkiwwwczaidz0") (v 2) (features2 (quote (("tokio" "dep:tokio"))))))

(define-public crate-epson-0.1 (crate (name "epson") (vers "0.1.5") (deps (list (crate-dep (name "image") (req "^0") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1") (features (quote ("io-util"))) (optional #t) (kind 0)))) (hash "01ywd5sznjssdqx0wvx463pzq6ilmds7fqfz2a3nnaff0q6pqf80") (v 2) (features2 (quote (("tokio" "dep:tokio"))))))

(define-public crate-epson-0.1 (crate (name "epson") (vers "0.1.6") (deps (list (crate-dep (name "image") (req "^0") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1") (features (quote ("io-util"))) (optional #t) (kind 0)))) (hash "17b8zda190gbjv1bandf0bf4rddqpkji88a86ksm739cd8fhixly") (v 2) (features2 (quote (("tokio" "dep:tokio"))))))

(define-public crate-epson-0.1 (crate (name "epson") (vers "0.1.7") (deps (list (crate-dep (name "image") (req "^0") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1") (features (quote ("io-util"))) (optional #t) (kind 0)))) (hash "0gbpfny2pnclvzwhffk7paqc871aqm74jknmy0080sns3xlsmicd") (v 2) (features2 (quote (("tokio" "dep:tokio"))))))

