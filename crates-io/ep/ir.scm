(define-module (crates-io ep ir) #:use-module (crates-io))

(define-public crate-epir-0.0.1 (crate (name "epir") (vers "0.0.1") (deps (list (crate-dep (name "criterion") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "curve25519-dalek") (req "^3") (default-features #t) (kind 0)) (crate-dep (name "num_cpus") (req "^1.13") (default-features #t) (kind 0)) (crate-dep (name "rand_core") (req "^0.5") (features (quote ("getrandom"))) (default-features #t) (kind 0)) (crate-dep (name "rayon") (req "^1.5") (default-features #t) (kind 0)) (crate-dep (name "sha2") (req "^0.9") (default-features #t) (kind 2)))) (hash "0s7ly3sq001xgva5xk3pn0zaig1nvd2c3nwndvz3wychv2cbjbfh")))

(define-public crate-epirust-0.0.1 (crate (name "epirust") (vers "0.0.1") (hash "1b1abwihsaip4pkb75pgjnqgmzcc4vl4fbprvp9ccbi17gyg5lik")))

