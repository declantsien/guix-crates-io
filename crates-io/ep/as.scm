(define-module (crates-io ep as) #:use-module (crates-io))

(define-public crate-epaste-1 (crate (name "epaste") (vers "1.0.0") (deps (list (crate-dep (name "rustc-serialize") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "sodiumoxide") (req "^0.0.10") (default-features #t) (kind 0)))) (hash "0cl2plinjk01kdzch9wwl23y738xmmwwrqy9lcr98hh8w45m8mnv")))

(define-public crate-epaste-1 (crate (name "epaste") (vers "1.0.1") (deps (list (crate-dep (name "rustc-serialize") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "sodiumoxide") (req "^0.0.10") (default-features #t) (kind 0)))) (hash "00nk966nrdgjdkcqqnhlky6225kmxxjrp62hvz9nimjdfpa4rnsa")))

(define-public crate-epaste-1 (crate (name "epaste") (vers "1.0.2") (deps (list (crate-dep (name "rustc-serialize") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "sodiumoxide") (req "^0.0.10") (default-features #t) (kind 0)))) (hash "0rm08ip5gylz6nwjnvvgp3s7vblgc0qddcjrar2n3ml3d9p56za8")))

(define-public crate-epaste-1 (crate (name "epaste") (vers "1.1.0") (deps (list (crate-dep (name "rustc-serialize") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "sodiumoxide") (req "^0.0.15") (default-features #t) (kind 0)))) (hash "0m7gf34yyj55llks54lwi07csgl4p8wlx8yarxm30ks0fjv9kg09")))

(define-public crate-epaste-2 (crate (name "epaste") (vers "2.0.0") (deps (list (crate-dep (name "assert_cmd") (req "^0.6") (default-features #t) (kind 2)) (crate-dep (name "base64") (req "^0.9") (default-features #t) (kind 0)) (crate-dep (name "failure") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "failure_derive") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "quickcheck") (req "^0.7") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.5") (default-features #t) (kind 2)) (crate-dep (name "size") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "sodiumoxide") (req "^0.0.16") (default-features #t) (kind 0)))) (hash "0gffbmrv4bx8fvl69l45agzmwvg8idykl2rqwn7x7jxd78d0hf2c")))

(define-public crate-epaste_-0.1 (crate (name "epaste_") (vers "0.1.0") (deps (list (crate-dep (name "clipboard") (req "^0.5") (default-features #t) (kind 0)))) (hash "096507k4iwc67kcsc8x32admmnzkfjgidjd03c7mcjdyadga097m")))

