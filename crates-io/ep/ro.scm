(define-module (crates-io ep ro) #:use-module (crates-io))

(define-public crate-eprompt-0.1 (crate (name "eprompt") (vers "0.1.0") (deps (list (crate-dep (name "crossterm") (req "^0.25.0") (default-features #t) (kind 0)))) (hash "1ys2bmp0x8xlmm33ir9fgxcvxgfsxq7pmrdfpgz7hswr1q8brwgr")))

(define-public crate-eprompt-0.1 (crate (name "eprompt") (vers "0.1.1") (deps (list (crate-dep (name "crossterm") (req "^0.25.0") (default-features #t) (kind 0)))) (hash "10zfjbggzbq5ss5hrpzlcl9x98j7amsfc6ms159k2z7dn9fldfs1")))

(define-public crate-eprompt-0.1 (crate (name "eprompt") (vers "0.1.2") (deps (list (crate-dep (name "crossterm") (req "^0.25.0") (default-features #t) (kind 0)))) (hash "1rb331c875whqrd9pkg1cl4jwn4gsynmmsc2x80nc31dypq848r3")))

(define-public crate-eprompt-0.1 (crate (name "eprompt") (vers "0.1.3") (deps (list (crate-dep (name "crossterm") (req "^0.25.0") (default-features #t) (kind 0)))) (hash "0l60lrbwjxp9x63a5mz2cn6b7f2dcw9b03qh69wi8sv9nm369a68")))

(define-public crate-eprompt-0.2 (crate (name "eprompt") (vers "0.2.0") (deps (list (crate-dep (name "crossterm") (req "^0.25.0") (default-features #t) (kind 0)))) (hash "04945kx7xvnjvwcd487292ajwjdr9flsrbx4x3bj168dd5rn7acw")))

(define-public crate-eprompt-0.3 (crate (name "eprompt") (vers "0.3.0") (deps (list (crate-dep (name "crossterm") (req "^0.25.0") (default-features #t) (kind 0)))) (hash "003hcap8w71i2x55hhn7bzxmlvls16cqcr715ydkvfg1q0bj50qd")))

(define-public crate-eproxy-0.0.0 (crate (name "eproxy") (vers "0.0.0") (hash "1phiyc1lld5fmggz98x1x1df5fsz6awq0imigk0arcm85ycibpv9")))

