(define-module (crates-io ep ol) #:use-module (crates-io))

(define-public crate-epoll-0.1 (crate (name "epoll") (vers "0.1.0") (deps (list (crate-dep (name "errno") (req "*") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "*") (default-features #t) (kind 0)))) (hash "0r6dj15z0n1qsz7xmxw7pbn9m8vwg2nxrk3fmvdajb89q6j2dpq6")))

(define-public crate-epoll-0.2 (crate (name "epoll") (vers "0.2.0") (deps (list (crate-dep (name "errno") (req "*") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "*") (default-features #t) (kind 0)))) (hash "1x9767ijfl4vdl3m85al351l44xz38b81jbm6iz8drv8ligb3vdx")))

(define-public crate-epoll-0.2 (crate (name "epoll") (vers "0.2.1") (deps (list (crate-dep (name "errno") (req "*") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "*") (default-features #t) (kind 0)))) (hash "0cyi374jwl7asba04z1g92lynxnnalgdrav6nwqg7dsxc0y5hin6")))

(define-public crate-epoll-0.2 (crate (name "epoll") (vers "0.2.2") (deps (list (crate-dep (name "errno") (req "*") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "*") (default-features #t) (kind 0)))) (hash "02qmnvhn552g7plg9gcpv2j0disj628l0syg5k9vzmmav5cl7pfd")))

(define-public crate-epoll-0.2 (crate (name "epoll") (vers "0.2.3") (deps (list (crate-dep (name "errno") (req "*") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "*") (default-features #t) (kind 0)))) (hash "13dpd9p720ixjjgglyl4gyg04znzi13wp0mk02wg5433kclmwdqg")))

(define-public crate-epoll-0.2 (crate (name "epoll") (vers "0.2.4") (deps (list (crate-dep (name "errno") (req "*") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "*") (default-features #t) (kind 0)))) (hash "1dcxv6sabysa9nkdg20j0hxfxdiwn4yf3wy78i5dwk5cci5ixpbn")))

(define-public crate-epoll-0.2 (crate (name "epoll") (vers "0.2.5") (deps (list (crate-dep (name "errno") (req "*") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "*") (default-features #t) (kind 0)))) (hash "0dqqwg7gw7hawnxh8s19ijv88hq7riwrsc69ksc9nf2iy0bw9v37")))

(define-public crate-epoll-0.3 (crate (name "epoll") (vers "0.3.0") (deps (list (crate-dep (name "errno") (req "^0.1.6") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2.9") (default-features #t) (kind 0)))) (hash "0amya475mjphp71xlwp95cn62gyn4rwmxi4mz81wh5y5fd8z4318")))

(define-public crate-epoll-0.4 (crate (name "epoll") (vers "0.4.0") (deps (list (crate-dep (name "bitflags") (req "^0.7") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "simple-slab") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "time") (req "^0.1") (default-features #t) (kind 0)))) (hash "1s71d3j4klbamynqfsv1sgz6l7gjkbw06031q2cngv90jfzrwjra")))

(define-public crate-epoll-0.4 (crate (name "epoll") (vers "0.4.1") (deps (list (crate-dep (name "bitflags") (req "^0.7") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "simple-slab") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "time") (req "^0.1") (default-features #t) (kind 0)))) (hash "1paxl392a12zmix77wm0nkd3lm5h6vhwx67kncz88q6vb9vsa6lr")))

(define-public crate-epoll-0.4 (crate (name "epoll") (vers "0.4.2") (deps (list (crate-dep (name "bitflags") (req "^0.7") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "simple-slab") (req "^0.2") (default-features #t) (kind 0)))) (hash "0ai5qy6in50272aav951k2rpigvdgkpbg28z30fwp3lvx8cp8pvc")))

(define-public crate-epoll-0.5 (crate (name "epoll") (vers "0.5.0") (deps (list (crate-dep (name "bitflags") (req "^0.7") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "simple-slab") (req "^0.2") (default-features #t) (kind 0)))) (hash "0kjcrzch86pyf53zhapqw4k7pc5pjzh36arwdlfv8kv5kyvl8wdv")))

(define-public crate-epoll-0.6 (crate (name "epoll") (vers "0.6.0") (deps (list (crate-dep (name "bitflags") (req "^0.7") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "simple-slab") (req "^0.2") (default-features #t) (kind 0)))) (hash "0fyy396rg30nga9wm738kjhlnj5i6gk1wsk9h7waaykx0jan5s7g")))

(define-public crate-epoll-1 (crate (name "epoll") (vers "1.0.0") (deps (list (crate-dep (name "bitflags") (req "^0.7") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "0chgh4x6wdzfvqpd01zwmk4vr1xs5927f3whf5fdd7wmh7m541xz")))

(define-public crate-epoll-2 (crate (name "epoll") (vers "2.0.0") (deps (list (crate-dep (name "bitflags") (req "^0.7") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "0rj0csz1850l80qvwqmd31s4mcxwx5pxcl7nkycqcfqq6bg2wvkb")))

(define-public crate-epoll-2 (crate (name "epoll") (vers "2.1.0") (deps (list (crate-dep (name "bitflags") (req "^0.7") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "1al3bcaqwgdq9kfk9xhkdg4nhjbhjz64r51y4v89qc02bbgz8f7l")))

(define-public crate-epoll-3 (crate (name "epoll") (vers "3.0.0") (deps (list (crate-dep (name "bitflags") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "1grx2qxmw49arbngigyalh53qaf2ahf87lf3kq09k56a8znrp1nv")))

(define-public crate-epoll-3 (crate (name "epoll") (vers "3.1.0") (deps (list (crate-dep (name "bitflags") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "1cqk20z0wwlrfbnadygfffj2bjbyp10sjx17cpdnx9z78fwqgslx")))

(define-public crate-epoll-3 (crate (name "epoll") (vers "3.1.1") (deps (list (crate-dep (name "bitflags") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "1msrn0df1fj8q5wbgckk883vr344cl6qrf0s1y8lijqb5akcah7w")))

(define-public crate-epoll-4 (crate (name "epoll") (vers "4.0.0") (deps (list (crate-dep (name "bitflags") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "010yc719inslnxizlivd75jibjb7aazmcf38bsig2kzbvfgra6bj")))

(define-public crate-epoll-4 (crate (name "epoll") (vers "4.0.1") (deps (list (crate-dep (name "bitflags") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "12my7d79x6y1lxb2yqwarz8ilpp4aiya4s46gbx1fakg587niw7k")))

(define-public crate-epoll-4 (crate (name "epoll") (vers "4.1.0") (deps (list (crate-dep (name "bitflags") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "1186f57fl1fqk1y32q46pj6mc0hxyrqgkhv8vsg6d2gadgicy2wr")))

(define-public crate-epoll-4 (crate (name "epoll") (vers "4.2.0") (deps (list (crate-dep (name "bitflags") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "0q08mirx2dd8rwjsvfr0a0cd1fx7m7bcib2qblg9p3gz4sh0nnvy")))

(define-public crate-epoll-4 (crate (name "epoll") (vers "4.3.0") (deps (list (crate-dep (name "bitflags") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "05fr1grpnlfax8glqyx9zll1qi35psb2brk9j92grdxv8dq7dz7j")))

(define-public crate-epoll-4 (crate (name "epoll") (vers "4.3.1") (deps (list (crate-dep (name "bitflags") (req "^1.2") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "1l0wa4vzbh7jsswx4mbrq89jjp912mmswvsdkphzf104f0y6kpr0")))

(define-public crate-epoll-4 (crate (name "epoll") (vers "4.3.2") (deps (list (crate-dep (name "bitflags") (req "^2") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "1xff2jbimwvc91cwy1vmiai64laxmc713xjvz9plx5ifpq0vsdz8") (yanked #t)))

(define-public crate-epoll-4 (crate (name "epoll") (vers "4.3.3") (deps (list (crate-dep (name "bitflags") (req "^2") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "0ygs486sbpxnhgrjy98knqf3ghla4qnh9q184v6zc7zaj8riqdbl")))

(define-public crate-epoll-rs-0.2 (crate (name "epoll-rs") (vers "0.2.0") (deps (list (crate-dep (name "bitflags") (req "^1.2.1") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2") (features (quote ("extra_traits"))) (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.4") (default-features #t) (kind 2)))) (hash "1dji285m7v4ck3i39vs56r1mwldds25njpj8v7f4kbhbni9g0yj1")))

(define-public crate-epoll-rs-0.2 (crate (name "epoll-rs") (vers "0.2.1") (deps (list (crate-dep (name "bitflags") (req "^1.2.1") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.4") (default-features #t) (kind 2)))) (hash "0rp7397qy408iimns49hk3walnlmd0qn1gxndm8ghcsg0ylq5iyz")))

(define-public crate-epoll-rs-0.2 (crate (name "epoll-rs") (vers "0.2.2") (deps (list (crate-dep (name "bitflags") (req "^1.2.1") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.4") (default-features #t) (kind 2)))) (hash "1jpkpccy1dqzjq96g1m6rzbxra2fiaiplpg4al4qxca24dr96c8s")))

(define-public crate-epoll_test-0.1 (crate (name "epoll_test") (vers "0.1.0") (hash "030zc9x1wy6jyw713vb8f9p49g877x9x01bl5v06yr8z6ik2ppsy")))

(define-public crate-epoll_test-0.1 (crate (name "epoll_test") (vers "0.1.1") (deps (list (crate-dep (name "rayon") (req "^1.9.0") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.36.0") (default-features #t) (kind 0)))) (hash "1zwa68psx191f6fbbbg6vnrjhz3pjm5p6wd308dpn32mjqj1sypw")))

