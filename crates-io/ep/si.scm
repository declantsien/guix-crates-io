(define-module (crates-io ep si) #:use-module (crates-io))

(define-public crate-epsi-0.1 (crate (name "epsi") (vers "0.1.0") (hash "0rzzp4f9nrqp5vqgsailzkic2xkkn04cyfsd5fv96xn7h77hvrpq") (rust-version "1.56.1")))

(define-public crate-epsi-0.1 (crate (name "epsi") (vers "0.1.1") (hash "0nwjvbkxnd9hxa7g415zhy0dj0mbkkpm1x5zxvybnqa7rzjpqyli") (rust-version "1.56.1")))

(define-public crate-epsilla-0.0.1 (crate (name "epsilla") (vers "0.0.1") (hash "14ykvn0h143sid5zv7lpygr0ayw3b5vl7zsx0ar154sbhhm8f9lw")))

(define-public crate-epsilla-client-0.0.1 (crate (name "epsilla-client") (vers "0.0.1") (hash "0ndm6xa2frs36g1fy4qsjvx53bv30zwfc9asd81d500ksdrxrj4z")))

(define-public crate-epsilon-0.0.1 (crate (name "epsilon") (vers "0.0.1") (hash "1c1qam29mrzcg05jcbqypv7z480ck1i7r17g8d2sagj09iph0afd")))

(define-public crate-epsilon-0.3 (crate (name "epsilon") (vers "0.3.13") (deps (list (crate-dep (name "paste") (req "^1.0") (default-features #t) (kind 0)))) (hash "1yppgxw8rl3h6162lpq32yscl4jg0frjsaahsq30idzjhpmn8vfb")))

(define-public crate-epsilon-0.313 (crate (name "epsilon") (vers "0.313.0") (deps (list (crate-dep (name "paste") (req "^1.0") (default-features #t) (kind 0)))) (hash "1bhwx0pwpz7ynqlf01c4jdwjgsnfgnxgwksrlsniwxm4picph73v")))

(define-public crate-epsilon-0.313 (crate (name "epsilon") (vers "0.313.1") (deps (list (crate-dep (name "paste") (req "^1.0") (default-features #t) (kind 0)))) (hash "0p0j2j1xccdmpi36xfy2biwpkyyw4bvhx7xyj3w94i2ia3w1i6ih")))

(define-public crate-epsilon-0.313 (crate (name "epsilon") (vers "0.313.2") (deps (list (crate-dep (name "paste") (req "^1.0") (default-features #t) (kind 0)))) (hash "1qqbihyrsmqq55dw396ssjvx10a3p83sj4xji5ili6f1x5zx75fj")))

(define-public crate-epsilon-0.313 (crate (name "epsilon") (vers "0.313.3") (deps (list (crate-dep (name "paste") (req "^1.0") (default-features #t) (kind 0)))) (hash "1fgpk8sbriqpgm6qpnx8khab9sqi7bapvwbfl8fkvm3bbz98cg95")))

(define-public crate-epsilon-0.313 (crate (name "epsilon") (vers "0.313.4") (deps (list (crate-dep (name "paste") (req "^1.0") (default-features #t) (kind 0)))) (hash "1csra5njbn0c9ramv6dzngdfmqpyms05dyhz58c7d2hrmv6nz5yh")))

(define-public crate-epsilon-ast-0.0.1 (crate (name "epsilon-ast") (vers "0.0.1") (hash "1xr0vayxkq85g1rm8d9lq9cizfi7w9ywvfkp1wiq6drk3b0bxy51")))

(define-public crate-epsilon-i10n-0.0.1 (crate (name "epsilon-i10n") (vers "0.0.1") (hash "10z0fs3iawrrwgd449idc32k39236yaw47fzjgmb3r6gg6m08fwn")))

(define-public crate-epsilon-ident-0.0.1 (crate (name "epsilon-ident") (vers "0.0.1") (hash "0ynhm5fn803x7kk2d7wz477ssn6lhw99mmxvfyd23cfdb6n7ycrk")))

(define-public crate-epsilon-identifier-0.0.1 (crate (name "epsilon-identifier") (vers "0.0.1") (hash "07c4wnblakn81yvs2zzpvvhxaydm4wvxyrvcchpk15hvnjbf1fz0")))

(define-public crate-epsilon-identifiers-0.0.1 (crate (name "epsilon-identifiers") (vers "0.0.1") (hash "11bn4pgh020shld740vv0lwc8jyl25dh2md4r8fjsl65m5xcrddc")))

(define-public crate-epsilon-llvm-0.0.1 (crate (name "epsilon-llvm") (vers "0.0.1") (hash "0g6w1j9gsng1gs1axvy83ndhch3fxqwvrivrgfy5hqprpka1bmh5")))

(define-public crate-epsilon-mir-0.0.1 (crate (name "epsilon-mir") (vers "0.0.1") (hash "0cm9s4kyy3chq27kvvrdppz9in7c2a8r6x4zp36q7lj2hac4x2cz")))

(define-public crate-epsilon-parse-0.0.1 (crate (name "epsilon-parse") (vers "0.0.1") (hash "1acw4qr4ilpw1vr4x1n80psqrzl8dfy0z8px82hrlkbdc78v9872")))

(define-public crate-epsilon-string-0.1 (crate (name "epsilon-string") (vers "0.1.0") (hash "19ncpx42kjl88qnr7k7phbjn2zd84gikmq0v6fsv654g9v0kh9g5")))

(define-public crate-epsilon-strings-0.1 (crate (name "epsilon-strings") (vers "0.1.0") (hash "0lqiq3zzcazwdjiw8hfrrklb3bnrg0k57s02rkn0vqp8w146h7y7")))

(define-public crate-epsilon-types-0.0.1 (crate (name "epsilon-types") (vers "0.0.1") (hash "1dgkp7fwpdjyn7k16f44qj40zdswpjjkras074mhmnk334zazq99")))

(define-public crate-epsilonz-0.0.1 (crate (name "epsilonz") (vers "0.0.1") (deps (list (crate-dep (name "epsilonz_algebra") (req "*") (default-features #t) (kind 0)) (crate-dep (name "fingertree") (req "*") (default-features #t) (kind 0)) (crate-dep (name "free") (req "*") (default-features #t) (kind 0)) (crate-dep (name "monad") (req "*") (default-features #t) (kind 0)) (crate-dep (name "pipes") (req "*") (default-features #t) (kind 0)) (crate-dep (name "pretty") (req "*") (default-features #t) (kind 0)) (crate-dep (name "rope") (req "*") (default-features #t) (kind 0)) (crate-dep (name "tailrec") (req "*") (default-features #t) (kind 0)))) (hash "1cvnivsd90ki76bnq253by68dl6da6km2svdzpba735sk780qmix")))

(define-public crate-epsilonz-0.0.2 (crate (name "epsilonz") (vers "0.0.2") (deps (list (crate-dep (name "epsilonz_algebra") (req "*") (default-features #t) (kind 0)) (crate-dep (name "fingertree") (req "*") (default-features #t) (kind 0)) (crate-dep (name "free") (req "*") (default-features #t) (kind 0)) (crate-dep (name "free_macros") (req "*") (default-features #t) (kind 0)) (crate-dep (name "monad") (req "*") (default-features #t) (kind 0)) (crate-dep (name "monad_macros") (req "*") (default-features #t) (kind 0)) (crate-dep (name "morphism") (req "*") (default-features #t) (kind 0)) (crate-dep (name "pipes") (req "*") (default-features #t) (kind 0)) (crate-dep (name "pretty") (req "*") (default-features #t) (kind 0)) (crate-dep (name "rope") (req "*") (default-features #t) (kind 0)) (crate-dep (name "tailrec") (req "*") (default-features #t) (kind 0)))) (hash "12wgdsaa0fiim6zc7kh29pq94pxnwa4y1cs5prlcnjchivb2wb98")))

(define-public crate-epsilonz_algebra-0.0.1 (crate (name "epsilonz_algebra") (vers "0.0.1") (hash "02jw7kfyaa7n53i7w9w8c7dfl4fm22f1fz5w57h2ylgzsz43hrh9")))

