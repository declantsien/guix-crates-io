(define-module (crates-io ep rn) #:use-module (crates-io))

(define-public crate-eprng-0.1 (crate (name "eprng") (vers "0.1.0") (hash "0japb66kn3cx72pl8haxzw74ch71js28v1bmf1glj35ykk5zzlng") (features (quote (("distribution") ("default" "distribution")))) (yanked #t)))

(define-public crate-eprng-0.1 (crate (name "eprng") (vers "0.1.1") (hash "09vz01jrrqssxnfzwznl62d69igva9jv7apvqi461s597h11jnjz") (features (quote (("distribution") ("default" "distribution"))))))

(define-public crate-eprng-0.1 (crate (name "eprng") (vers "0.1.2") (hash "1gmlcz17lrcz105cky7vv61kji973bsj0piaikr3vj3p3vhw2q8g") (features (quote (("distribution") ("default" "distribution"))))))

