(define-module (crates-io ep -p) #:use-module (crates-io))

(define-public crate-ep-pin-toggle-0.1 (crate (name "ep-pin-toggle") (vers "0.1.0") (deps (list (crate-dep (name "embedded-hal") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "embedded-profiling") (req "^0.2") (default-features #t) (kind 0)))) (hash "1w6bav50mvlr95kqdj82csys2yss3i1myzjrp8c6b6hxdv537l5c")))

(define-public crate-ep-pin-toggle-0.1 (crate (name "ep-pin-toggle") (vers "0.1.1") (deps (list (crate-dep (name "embedded-hal") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "embedded-profiling") (req "^0.2") (default-features #t) (kind 0)))) (hash "0dm4r2v1rxgyyrg4h1i02rghlvxw34fqgaz1456cs1c5i8lk730v")))

(define-public crate-ep-pin-toggle-0.1 (crate (name "ep-pin-toggle") (vers "0.1.2") (deps (list (crate-dep (name "cortex-m") (req "^0.7") (default-features #t) (kind 2)) (crate-dep (name "embedded-hal") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "embedded-profiling") (req "^0.2") (default-features #t) (kind 0)))) (hash "18b2jamgh6jwhfcrpmy41dsbqznaml4rrpmwn4c3hllw3p5ciwnr") (features (quote (("proc-macros" "embedded-profiling/proc-macros"))))))

(define-public crate-ep-pin-toggle-0.1 (crate (name "ep-pin-toggle") (vers "0.1.3") (deps (list (crate-dep (name "cortex-m") (req "^0.7") (default-features #t) (kind 2)) (crate-dep (name "embedded-hal") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "embedded-profiling") (req "^0.2") (default-features #t) (kind 0)))) (hash "0i98xb7sjakpki0wl15cbp1ypqdcickqzd0jncq5jpxfmwk41zlv") (features (quote (("proc-macros" "embedded-profiling/proc-macros"))))))

(define-public crate-ep-pin-toggle-0.1 (crate (name "ep-pin-toggle") (vers "0.1.4") (deps (list (crate-dep (name "cortex-m") (req "^0.7") (default-features #t) (kind 2)) (crate-dep (name "embedded-hal") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "embedded-profiling") (req "^0.2") (default-features #t) (kind 0)))) (hash "0wc8dr6c82j9wh5kxahapwlck6ci50g1hhsydsal1qfnj0fqg1jp") (features (quote (("proc-macros" "embedded-profiling/proc-macros"))))))

(define-public crate-ep-pin-toggle-0.1 (crate (name "ep-pin-toggle") (vers "0.1.5") (deps (list (crate-dep (name "cortex-m") (req "^0.7") (default-features #t) (kind 2)) (crate-dep (name "embedded-hal") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "embedded-profiling") (req "^0.2") (default-features #t) (kind 0)))) (hash "1zmkwx8g029fp8ligd0b1ni9cxjrzfw4xhxwfrp8n69ql38irnvf") (features (quote (("proc-macros" "embedded-profiling/proc-macros"))))))

(define-public crate-ep-pin-toggle-0.2 (crate (name "ep-pin-toggle") (vers "0.2.0") (deps (list (crate-dep (name "cortex-m") (req "^0.7") (default-features #t) (kind 2)) (crate-dep (name "embedded-hal") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "embedded-profiling") (req "^0.3") (default-features #t) (kind 0)))) (hash "108ykwy5sxlfb7sz0d0137q5vf4kbzx3s6majfkyq23sl1izb1q2") (features (quote (("proc-macros" "embedded-profiling/proc-macros")))) (rust-version "1.57")))

(define-public crate-ep-pin-toggle-0.3 (crate (name "ep-pin-toggle") (vers "0.3.0") (deps (list (crate-dep (name "cortex-m") (req "^0.7") (default-features #t) (kind 2)) (crate-dep (name "embedded-hal") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "embedded-profiling") (req "^0.3") (default-features #t) (kind 0)))) (hash "0hwz7cwz1gip6h100wld3vnsa2vbz9if28fgvs0j2qxkcr5y5g0v") (features (quote (("proc-macros" "embedded-profiling/proc-macros")))) (rust-version "1.61")))

