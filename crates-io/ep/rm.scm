(define-module (crates-io ep rm) #:use-module (crates-io))

(define-public crate-eprmutate-0.1 (crate (name "eprmutate") (vers "0.1.0") (hash "1bjfyx5hl7p35lmzbbr12h0gcw20r2d0wqm0ixa8wi2fjcd753dy")))

(define-public crate-eprmutate-0.1 (crate (name "eprmutate") (vers "0.1.1") (hash "14pypmkk2acscbf708hadwshr059l742l0x46dxs5yh6r0n163q4")))

(define-public crate-eprmutate-0.1 (crate (name "eprmutate") (vers "0.1.2") (hash "0fhn5bjzdlzkfj5cq2z6ac8bqf1zg03848lq6y9c309lw7p0wgph")))

