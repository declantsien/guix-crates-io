(define-module (crates-io ep ui) #:use-module (crates-io))

(define-public crate-epui-0.1 (crate (name "epui") (vers "0.1.0") (hash "072cqcv6h5s1a674mphlpcxkxshdwaisah9gpfyw9wql3ldxl470") (rust-version "1.56.1")))

(define-public crate-epui-0.1 (crate (name "epui") (vers "0.1.1") (hash "0a9w95n30ry541bh9jfj355kyvs91bz3fxca4ln6j1aa8li5s37l") (rust-version "1.56.1")))

