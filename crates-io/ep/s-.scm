(define-module (crates-io ep s-) #:use-module (crates-io))

(define-public crate-eps-ast-0.0.1 (crate (name "eps-ast") (vers "0.0.1") (hash "06q8ki21z7d58191ybx0bzvdafvkwjy889sj1bfk2hw92f65y43p")))

(define-public crate-eps-i10n-0.0.1 (crate (name "eps-i10n") (vers "0.0.1") (hash "1wslk6vndav31rfizj20liq764s51kakai23v8d4c51qcnm9ndw5")))

(define-public crate-eps-ident-0.0.1 (crate (name "eps-ident") (vers "0.0.1") (hash "05vbr8lsfxn4n4ki8i6wp9jfzdhmkib378imzq9pyiiln70f78m6")))

(define-public crate-eps-identifier-0.0.1 (crate (name "eps-identifier") (vers "0.0.1") (hash "1mmy6kw4mmbp3xxxcj4ppr4r5905mxgwvf9kjl40srxx3dajf8rz")))

(define-public crate-eps-identifiers-0.0.1 (crate (name "eps-identifiers") (vers "0.0.1") (hash "1kljxkvamsj676d8vibgkkcmnjmcbw1x2sma4pl9vxqzx69nx8fp")))

(define-public crate-eps-llvm-0.0.1 (crate (name "eps-llvm") (vers "0.0.1") (hash "0rb6lj0abgcm4v4xlhygwmz0sy0i8jil1m9z2wv0hpq92b47am74")))

(define-public crate-eps-mir-0.0.1 (crate (name "eps-mir") (vers "0.0.1") (hash "172z6f5fyb5vd1vp71zkvskspzcq7ig0kdd86zgcz0pc7xadgyqi")))

(define-public crate-eps-parse-0.0.1 (crate (name "eps-parse") (vers "0.0.1") (hash "0br54b3iriz748yp7v5kjpvljpjhrj7cn2j8xgab0nsr605ka4xh")))

(define-public crate-eps-string-0.0.1 (crate (name "eps-string") (vers "0.0.1") (hash "1f5hga722kl2r5dg1zzdl8bz88mfjjbq638cbc78i8sl0mhbx62f")))

(define-public crate-eps-strings-0.1 (crate (name "eps-strings") (vers "0.1.0") (hash "1d0si5cvg1k6xlih8qf778r2j0w3cacv2842ffw1872gjfckrpi5")))

(define-public crate-eps-types-0.0.1 (crate (name "eps-types") (vers "0.0.1") (hash "1x8qdyid4j9955aranqyc2gnpzq6x8d8g9m7s0ib3ddzbqpb9fxk")))

