(define-module (crates-io ep ss) #:use-module (crates-io))

(define-public crate-epss-0.1 (crate (name "epss") (vers "0.1.0") (hash "05ni1i7l4s41mr5j2lh929hs94yrkwl3jhsbaq4bvzv5f592g8x9")))

(define-public crate-epss-api-0.1 (crate (name "epss-api") (vers "0.1.0") (hash "1f0wik9v2yc8h88x4i8bx9kmnp775j9acy0vf87cvg4c7y1kx5bz")))

