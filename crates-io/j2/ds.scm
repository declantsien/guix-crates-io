(define-module (crates-io j2 ds) #:use-module (crates-io))

(define-public crate-j2ds-0.1 (crate (name "j2ds") (vers "0.1.0") (hash "14cn0m3jmp6mi5kcggyxvahlhhrdbl3zxwxbhhnwf98jnyq022jv")))

(define-public crate-j2ds-0.1 (crate (name "j2ds") (vers "0.1.1") (hash "0hra2q8xpmafyk7rlfgc41xsb75gwqz7gjik8mlm84msxpl7dcgp")))

(define-public crate-j2ds-0.2 (crate (name "j2ds") (vers "0.2.0") (hash "0pqxsrlk2xyy30lv5y4vpxpac26fw59i46v2ykvi7r1s7kf19ayd")))

(define-public crate-j2ds-0.3 (crate (name "j2ds") (vers "0.3.0") (hash "1ibwkcvaqj5mr2g4fv22x9499663smlf21rfai8sfp89dr06pj0v")))

