(define-module (crates-io j2 -g) #:use-module (crates-io))

(define-public crate-j2-gba-tool-0.1 (crate (name "j2-gba-tool") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^2.33.0") (default-features #t) (kind 0)) (crate-dep (name "colored") (req "^1.8.0") (default-features #t) (kind 0)) (crate-dep (name "image") (req "^0.21.1") (default-features #t) (kind 0)))) (hash "02ln3r3pllyci7fmzwl0pz0k4h42qjyb8rgm071xa0ysyx4bncbi")))

