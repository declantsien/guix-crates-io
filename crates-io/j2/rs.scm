(define-module (crates-io j2 rs) #:use-module (crates-io))

(define-public crate-j2rs_find_up_simple-0.0.1 (crate (name "j2rs_find_up_simple") (vers "0.0.1") (deps (list (crate-dep (name "criterion") (req "^0.5") (default-features #t) (kind 2)) (crate-dep (name "lets_find_up") (req "^0.0.3") (default-features #t) (kind 2)) (crate-dep (name "sugar_path") (req "^0.0.12") (default-features #t) (kind 2)))) (hash "1kvq9ccyllp2zm8ci38myhy75lw0zrwrcgdrd1mcygkxlmh4gl02")))

(define-public crate-j2rs_find_up_simple-0.0.2 (crate (name "j2rs_find_up_simple") (vers "0.0.2") (deps (list (crate-dep (name "codspeed-criterion-compat") (req "^2.3.3") (default-features #t) (kind 2)) (crate-dep (name "lets_find_up") (req "^0.0.3") (default-features #t) (kind 2)) (crate-dep (name "sugar_path") (req "^0.0.12") (default-features #t) (kind 2)))) (hash "1yiwaw25z4yrbwjx7mga827lbc3dj086d8amcw76dfca43w7y6mf")))

(define-public crate-j2rs_package_up-0.0.1 (crate (name "j2rs_package_up") (vers "0.0.1") (deps (list (crate-dep (name "j2rs_find_up_simple") (req "^0.0.2") (default-features #t) (kind 0)) (crate-dep (name "sugar_path") (req "^0.0.12") (default-features #t) (kind 2)))) (hash "02i2rkdgwv1fmkys2xhqwc27a0zb5m7s6z3qrcsrjiqwd6f23206")))

(define-public crate-j2rs_strip_ansi-0.0.1 (crate (name "j2rs_strip_ansi") (vers "0.0.1") (deps (list (crate-dep (name "ansi-regex") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "picocolors") (req "^0.1.0") (default-features #t) (kind 2)) (crate-dep (name "regex") (req "^1.10.2") (default-features #t) (kind 0)))) (hash "0mvjjsi3pgjad68bi66axmajpazrvzvnv6db4wzyvh82lnsi13pl")))

