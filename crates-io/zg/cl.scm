(define-module (crates-io zg cl) #:use-module (crates-io))

(define-public crate-zgclp-0.1 (crate (name "zgclp") (vers "0.1.0") (hash "0rqchk77vxv7lz9mx8946q3cbbs712dxm7zlq9rwhracqbl7scr1")))

(define-public crate-zgclp-0.1 (crate (name "zgclp") (vers "0.1.1") (hash "0nsjkppk2smg6jf63cbdiyp6nvakx47h93w8j4gsv4yqrh86wp9j")))

(define-public crate-zgclp-0.2 (crate (name "zgclp") (vers "0.2.0") (hash "1a08v4b4v7i4w4j7jh9vcad5451v946icamqq5a1p3g2afz9dl73")))

(define-public crate-zgclp-0.3 (crate (name "zgclp") (vers "0.3.0") (hash "1lkjmdh4lxia9hh0wcmvrva5hyrhas07gp05xrxhrp8q9cbfrib0")))

(define-public crate-zgclp-0.3 (crate (name "zgclp") (vers "0.3.1") (hash "09d30wngks2pb14yrkkvgnpc0g7k8s2ywlcm71cq8814f2am9j3v")))

