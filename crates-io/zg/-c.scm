(define-module (crates-io zg -c) #:use-module (crates-io))

(define-public crate-zg-co2-0.1 (crate (name "zg-co2") (vers "0.1.0-alpha.1") (deps (list (crate-dep (name "assert_float_eq") (req "^1.1") (kind 2)))) (hash "1f3jn8iv13m22an5gxhylfihwbyql0m3wspmmwinmc8xdnk3s4h2") (features (quote (("use_std") ("default" "use_std"))))))

(define-public crate-zg-co2-0.1 (crate (name "zg-co2") (vers "0.1.0-alpha.2") (deps (list (crate-dep (name "assert_float_eq") (req "^1.1") (kind 2)))) (hash "0p2z9zwj1fb5gghs6px0wqwn0jdz5v29hndbk2d1pinn0x40nlwk") (features (quote (("std") ("default" "std"))))))

(define-public crate-zg-co2-1 (crate (name "zg-co2") (vers "1.0.0-rc.1") (deps (list (crate-dep (name "assert_float_eq") (req "^1.1") (kind 2)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (optional #t) (kind 0)))) (hash "117wmh7qr1sgnbm4ncscv9dmwxmkqsjlm9s709w4xrs5s49d9j2k") (features (quote (("std") ("default" "std"))))))

(define-public crate-zg-co2-1 (crate (name "zg-co2") (vers "1.0.0-rc.2") (deps (list (crate-dep (name "assert_float_eq") (req "^1.1") (kind 2)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (optional #t) (kind 0)))) (hash "19y30fdyblksaas0nmgwj1mbj20qnn2kfhwl3bczrl1viy2f4k5h") (features (quote (("std") ("default" "std"))))))

(define-public crate-zg-co2-1 (crate (name "zg-co2") (vers "1.0.0") (deps (list (crate-dep (name "assert_float_eq") (req "^1.1") (kind 2)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (optional #t) (kind 0)))) (hash "0f2yxn3idixrb2jqy00njxgw2py7jz3a15ssjzr7bavfa1a3zffg") (features (quote (("std") ("default" "std"))))))

(define-public crate-zg-co2-2 (crate (name "zg-co2") (vers "2.0.0") (hash "1wvqkmhw50k5mn8zbahqlp2r24b9m8mhq0n9n0qhj7ymhx5wis4b") (features (quote (("std") ("default" "std"))))))

(define-public crate-zg-co2-2 (crate (name "zg-co2") (vers "2.0.1") (hash "1wl0bm50irq0yhkzaps2qm6mj50k2bmrfkzbzfqn2j5gwcpmvvmj") (features (quote (("std") ("default" "std"))))))

(define-public crate-zg-co2-2 (crate (name "zg-co2") (vers "2.1.0") (hash "0anczj35zg2h8fsf8kjw772k33yabayphgbjv80fmh4ykm8dzafk") (features (quote (("std") ("default" "std"))))))

