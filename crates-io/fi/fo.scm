(define-module (crates-io fi fo) #:use-module (crates-io))

(define-public crate-fifo-0.1 (crate (name "fifo") (vers "0.1.2") (hash "007p5w0k7mg0iivagrhlfpc6vjymmgkp3d75b4zgmqi4hg5qgyg9")))

(define-public crate-fifo-0.1 (crate (name "fifo") (vers "0.1.3") (hash "1rcmrpvf3j71x69vcqylk0grdagm8rcadds44g7xixkqvg21qp7v")))

(define-public crate-fifo-0.1 (crate (name "fifo") (vers "0.1.4") (hash "0lm3lyyiqwvji6dcy1cznc7qiq4za9fcryyr8d2p6iv1aikll156")))

(define-public crate-fifo-0.1 (crate (name "fifo") (vers "0.1.5") (hash "0dfz64xbwq2kk50yy4fa2saxqlp0djz1pxdlnjyzf497n3isihb4")))

(define-public crate-fifo-0.2 (crate (name "fifo") (vers "0.2.0") (hash "1birrm30izmacdd9m3pl6fdxacpd332bwwzk5zipg8k3a04i1r5f")))

(define-public crate-fifo-set-1 (crate (name "fifo-set") (vers "1.0.0") (hash "038prvhxqxv7xpwpkml507fwakydd8z3wd94d1nc2ismmk6fcjkk")))

