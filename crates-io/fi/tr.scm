(define-module (crates-io fi tr) #:use-module (crates-io))

(define-public crate-fitrs-0.1 (crate (name "fitrs") (vers "0.1.0") (deps (list (crate-dep (name "byteorder") (req "^1") (default-features #t) (kind 0)))) (hash "1r7rjp5s3wd1jmdlgvnp8916kjhhnsz8aqldw2apxfgb6rhxd4rs")))

(define-public crate-fitrs-0.1 (crate (name "fitrs") (vers "0.1.1") (deps (list (crate-dep (name "byteorder") (req "^1") (default-features #t) (kind 0)))) (hash "10kf3kchvfshb6l7wm7kcg1b7zfbl747d3abzvfdrm784iix4h8z")))

(define-public crate-fitrs-0.1 (crate (name "fitrs") (vers "0.1.2") (deps (list (crate-dep (name "byteorder") (req "^1") (default-features #t) (kind 0)))) (hash "08yfqsqwsr2mlviwany2554rqcmh660hpcg354r8wvmfkk8a42gq")))

(define-public crate-fitrs-0.2 (crate (name "fitrs") (vers "0.2.0") (deps (list (crate-dep (name "byteorder") (req "^1") (default-features #t) (kind 0)))) (hash "0v63p68f2f95v4pcmpdj256jb1zsqalkw6n5ssbs4mh8b71rpc17")))

(define-public crate-fitrs-0.2 (crate (name "fitrs") (vers "0.2.1") (deps (list (crate-dep (name "byteorder") (req "^1") (default-features #t) (kind 0)))) (hash "1ciwwwlk0jcj983z2as1wy7scc1z31f60a388rsw8nwjrlsfvdic")))

(define-public crate-fitrs-0.2 (crate (name "fitrs") (vers "0.2.2") (deps (list (crate-dep (name "byteorder") (req "^1") (default-features #t) (kind 0)))) (hash "081vank7vxga9392na90km1phjqcmlhyz0bgr1anfsw9qwrcdafm")))

(define-public crate-fitrs-0.2 (crate (name "fitrs") (vers "0.2.3") (deps (list (crate-dep (name "byteorder") (req "^1") (default-features #t) (kind 0)))) (hash "107f3q4y3r3k04kyjgnijy7b9dlbaalz7ikcp0jzn7830zrr7j63")))

(define-public crate-fitrs-0.2 (crate (name "fitrs") (vers "0.2.4") (deps (list (crate-dep (name "byteorder") (req "^1") (default-features #t) (kind 0)))) (hash "0n8lpk8z39ykh33ynql0bfrq54ayalracr5vbacl65bk22fnc3ag")))

(define-public crate-fitrs-0.2 (crate (name "fitrs") (vers "0.2.5") (deps (list (crate-dep (name "byteorder") (req "^1") (default-features #t) (kind 0)))) (hash "0r1hb3wsnqkv70b8qhi4hiyb7fp5496f8b0038y5hyzrfg8jf64i")))

(define-public crate-fitrs-0.2 (crate (name "fitrs") (vers "0.2.6") (deps (list (crate-dep (name "byteorder") (req "^1") (default-features #t) (kind 0)))) (hash "1d1nrjg80cf9hyma0bgfnsvhmy23x5118css3w3p6hv260ph4rvv")))

(define-public crate-fitrs-0.3 (crate (name "fitrs") (vers "0.3.0") (deps (list (crate-dep (name "byteorder") (req "^1") (default-features #t) (kind 0)))) (hash "0q3h1g8kwiwks080c2xza2n29awdwkj68zs35w00lx6z6lvsfbmq")))

(define-public crate-fitrs-0.3 (crate (name "fitrs") (vers "0.3.1") (deps (list (crate-dep (name "byteorder") (req "^1") (default-features #t) (kind 0)))) (hash "0vysib14wc9splwnf9s7nrmckczanm4vhahyfdm69klaq2kl20pz")))

(define-public crate-fitrs-0.3 (crate (name "fitrs") (vers "0.3.2") (deps (list (crate-dep (name "byteorder") (req "^1") (default-features #t) (kind 0)))) (hash "1wwmv7lh9id0ag5lj39x918bb0c8d4jn2vk47cb0iicx8hqkmi6v")))

(define-public crate-fitrs-0.4 (crate (name "fitrs") (vers "0.4.0") (deps (list (crate-dep (name "byteorder") (req "^1") (default-features #t) (kind 0)))) (hash "0mymnkjf4ayws5j952wq8006sk8j7scrdjg0c81lbyg7jc6qxnva")))

(define-public crate-fitrs-0.4 (crate (name "fitrs") (vers "0.4.1") (deps (list (crate-dep (name "byteorder") (req "^1") (default-features #t) (kind 0)))) (hash "1jhfyxi2ys6vyj35kcbwf4p4yw0j62wi3dxmnqj7ndr0w452pqpm")))

(define-public crate-fitrs-0.4 (crate (name "fitrs") (vers "0.4.2") (deps (list (crate-dep (name "byteorder") (req "^1") (default-features #t) (kind 0)))) (hash "1bv0c2biicn6pjhmf14vi56xv8h2ghzwskwv4ss8s6yvg5ng0ig0")))

(define-public crate-fitrs-0.5 (crate (name "fitrs") (vers "0.5.0") (deps (list (crate-dep (name "byteorder") (req "^1") (default-features #t) (kind 0)))) (hash "0d3i6iain1lsy6ppr4z8g5g98hqzsszp4z2sacp7pgg2sk4xhsjz")))

