(define-module (crates-io fi si) #:use-module (crates-io))

(define-public crate-fisica-0.1 (crate (name "fisica") (vers "0.1.0") (deps (list (crate-dep (name "float_eq") (req "^0.7") (default-features #t) (kind 2)) (crate-dep (name "glam") (req "^0.20") (default-features #t) (kind 0)) (crate-dep (name "paste") (req "^1.0") (default-features #t) (kind 0)))) (hash "03v0djw8sdcbnjkzdva7j499a0fzdga28f98l5c9g36a1bmpbk1w") (rust-version "1.57")))

(define-public crate-fisica-0.1 (crate (name "fisica") (vers "0.1.1") (deps (list (crate-dep (name "float_eq") (req "^0.7") (default-features #t) (kind 2)) (crate-dep (name "glam") (req "^0.20") (default-features #t) (kind 0)) (crate-dep (name "paste") (req "^1.0") (default-features #t) (kind 0)))) (hash "0p1857vzpwsvjarac3589rs1yz21ahv4zzq5zaw8d8xkvy895bwa") (features (quote (("std") ("safe") ("nightly") ("default" "std" "safe")))) (rust-version "1.57")))

(define-public crate-fisica-0.2 (crate (name "fisica") (vers "0.2.0") (deps (list (crate-dep (name "devela") (req "^0.7.1") (default-features #t) (kind 0)) (crate-dep (name "float_eq") (req "^0.7") (default-features #t) (kind 2)) (crate-dep (name "glam") (req "^0.24.1") (default-features #t) (kind 0)))) (hash "02dq7p1dijj7i2w7l98nkv6bcp2h2mqx77n51r8s2yvq89bbz0bg") (features (quote (("unsafest" "unsafe") ("unsafe") ("std" "alloc") ("safest" "safe") ("safe") ("no-std") ("nightly_docs" "nightly" "std" "unsafe") ("nightly") ("default" "std" "safe") ("alloc")))) (rust-version "1.71.1")))

