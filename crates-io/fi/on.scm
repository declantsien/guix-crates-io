(define-module (crates-io fi on) #:use-module (crates-io))

(define-public crate-fiona-0.1 (crate (name "fiona") (vers "0.1.0") (deps (list (crate-dep (name "cc") (req "^1.0") (default-features #t) (kind 1)))) (hash "0dayhsgrmy257vfplhw8klakvysmpjyqf95gbn95d6d2vx05h5kw")))

(define-public crate-fionread-0.1 (crate (name "fionread") (vers "0.1.0") (deps (list (crate-dep (name "nix") (req "^0.23") (default-features #t) (target "cfg(unix)") (kind 0)) (crate-dep (name "windows-sys") (req "^0.32") (features (quote ("Win32_Networking_WinSock"))) (default-features #t) (target "cfg(windows)") (kind 0)))) (hash "1kllrq15dl3dl87p2pvm8hqr379nafqwnl9ahkvfq8nmc3rqrq35")))

(define-public crate-fionread-0.1 (crate (name "fionread") (vers "0.1.1") (deps (list (crate-dep (name "nix") (req "^0.23") (default-features #t) (target "cfg(unix)") (kind 0)) (crate-dep (name "windows-sys") (req "^0.32") (features (quote ("Win32_Networking_WinSock"))) (default-features #t) (target "cfg(windows)") (kind 0)))) (hash "0chxd2k7ha3vznfll59dwv3qs330n6z855bvcr08vs67zhchnczz")))

