(define-module (crates-io fi es) #:use-module (crates-io))

(define-public crate-fiesta-0.1 (crate (name "fiesta") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^4.4.3") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "color-eyre") (req "^0.6.2") (default-features #t) (kind 0)) (crate-dep (name "dirs") (req "^5.0.1") (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.11.20") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.32.0") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "11drs3ggl9fvy895l8awf7c70v2n8jxqgp2vqd98a7hliif220xf")))

