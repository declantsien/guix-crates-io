(define-module (crates-io fi z-) #:use-module (crates-io))

(define-public crate-fiz-math-0.0.1 (crate (name "fiz-math") (vers "0.0.1") (deps (list (crate-dep (name "num") (req "*") (default-features #t) (kind 0)))) (hash "1y3spbc72yzlpp5yb20ihvqpk5izlzgclqbllq9n0zk1d8v7vgvp")))

(define-public crate-fiz-math-0.0.11 (crate (name "fiz-math") (vers "0.0.11") (deps (list (crate-dep (name "num") (req "*") (default-features #t) (kind 0)))) (hash "1pckhc5gp1nj52x04pbnrp6966a8vm4lnxg6m15aiy91mp521ci4") (yanked #t)))

(define-public crate-fiz-math-0.0.12 (crate (name "fiz-math") (vers "0.0.12") (deps (list (crate-dep (name "num") (req "*") (default-features #t) (kind 0)))) (hash "08ya1m8q2j1vxng6k6hfxg4irg0aqjrkaqyyqa8grhkv1v2g7y67")))

(define-public crate-fiz-math-0.0.13 (crate (name "fiz-math") (vers "0.0.13") (deps (list (crate-dep (name "num") (req "^0.1.27") (default-features #t) (kind 0)))) (hash "0mkxfhzmb7yfi8q7s267f10p92igfhcn4hjx1mp3qz0nhirxzl6y")))

(define-public crate-fiz-math-0.0.14 (crate (name "fiz-math") (vers "0.0.14") (deps (list (crate-dep (name "num") (req "^0.1.27") (default-features #t) (kind 0)))) (hash "00pv81na9zhlhqbrf10vkrz4cl2im12ly9ngkahp88yd6c0x83m1")))

