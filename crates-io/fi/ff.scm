(define-module (crates-io fi ff) #:use-module (crates-io))

(define-public crate-fiffy-0.1 (crate (name "fiffy") (vers "0.1.1") (deps (list (crate-dep (name "rust-crypto") (req "^0.2") (default-features #t) (kind 0)))) (hash "147ym59k8ig7nw7iwla9d1ln3h0ql6rcj8291lxlp6v7pvbqzivf") (features (quote (("default"))))))

(define-public crate-fiffy-0.1 (crate (name "fiffy") (vers "0.1.2") (deps (list (crate-dep (name "clippy") (req "^0.0.175") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "rust-crypto") (req "^0.2") (default-features #t) (kind 0)))) (hash "0xbzrizwmank1qdm4x3h3w2jkb11jpn1q1j1mid22sixkh2r032z") (features (quote (("default"))))))

(define-public crate-fiffy-0.1 (crate (name "fiffy") (vers "0.1.3") (deps (list (crate-dep (name "clippy") (req "^0.0.175") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "rust-crypto") (req "^0.2") (default-features #t) (kind 0)))) (hash "09x89a36405mr2bqrc5iscs2j1ibdp1s9z3vyq6i87l6myzclh3q") (features (quote (("default"))))))

(define-public crate-fiffy-0.1 (crate (name "fiffy") (vers "0.1.4") (deps (list (crate-dep (name "clippy") (req "^0.0.175") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "rust-crypto") (req "^0.2") (default-features #t) (kind 0)))) (hash "0m96y2yx48rkapdl1rs0z2v0bclvmdwdw79h8hwwbwa7mqclwdb8") (features (quote (("default"))))))

(define-public crate-fiffy-0.1 (crate (name "fiffy") (vers "0.1.5") (deps (list (crate-dep (name "clippy") (req "^0.0.175") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "rust-crypto") (req "^0.2") (default-features #t) (kind 0)))) (hash "0dc49a11929nz12n46asw7y63jiywallx0ahs8fc8i4l8z6fs982") (features (quote (("default"))))))

(define-public crate-fiffy-0.1 (crate (name "fiffy") (vers "0.1.6") (deps (list (crate-dep (name "clippy") (req "^0.0.175") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "rust-crypto") (req "^0.2") (default-features #t) (kind 0)))) (hash "08vjzkbz7gykh0ll4km6xqahw290z9fksp5845gs76i81wiyynq9") (features (quote (("default"))))))

(define-public crate-fiffy-0.1 (crate (name "fiffy") (vers "0.1.7") (deps (list (crate-dep (name "clippy") (req "^0.0.175") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "rust-crypto") (req "^0.2") (default-features #t) (kind 0)))) (hash "1vk1byri3wlw0qrn6hgl2qyaz4hw28782dfkkc5c7i7hzry06rrr") (features (quote (("default"))))))

(define-public crate-fiffy-0.1 (crate (name "fiffy") (vers "0.1.8") (deps (list (crate-dep (name "clippy") (req "^0.0.175") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "rust-crypto") (req "^0.2") (default-features #t) (kind 0)))) (hash "0kn9y96rpl4vv900ccw92wss2gs84m0gc7wr5nlsrzv2a95x4mi0") (features (quote (("default"))))))

(define-public crate-fiffy-0.1 (crate (name "fiffy") (vers "0.1.9") (deps (list (crate-dep (name "clippy") (req "^0.0.175") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "rust-crypto") (req "^0.2") (default-features #t) (kind 0)))) (hash "0ap2l55j7qiqk6ga4iwpqsghla96ksnhhinmn1a1pn52p7cbc4y8") (features (quote (("default"))))))

(define-public crate-fiffy-0.1 (crate (name "fiffy") (vers "0.1.10") (deps (list (crate-dep (name "clippy") (req "^0.0.175") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "rust-crypto") (req "^0.2") (default-features #t) (kind 0)))) (hash "1rlixn6ymiv484b4j70311gc2sa4f98vnb604hb71gcby9dgmqx1") (features (quote (("default"))))))

