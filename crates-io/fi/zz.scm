(define-module (crates-io fi zz) #:use-module (crates-io))

(define-public crate-fizz-0.0.0 (crate (name "fizz") (vers "0.0.0") (hash "0rgdr8bdbjg7v9y2qz9vxmvk2m4nxbz6wms62ccmb3k8p35sp1js")))

(define-public crate-fizzbuzz-0.1 (crate (name "fizzbuzz") (vers "0.1.0") (hash "1i3pxi61y5p0j7b9jm91sx695b89s6zfpmavp3mvql074albflij")))

(define-public crate-fizzy-0.6 (crate (name "fizzy") (vers "0.6.0-dev") (deps (list (crate-dep (name "bindgen") (req "^0.54.0") (default-features #t) (kind 1)) (crate-dep (name "cmake") (req "^0.1") (default-features #t) (kind 1)))) (hash "1wwscd2z22zk7l3y3cgpfvf1rdvy5is94xjadg2z341z8v18zasw")))

(define-public crate-fizzy-0.6 (crate (name "fizzy") (vers "0.6.0") (deps (list (crate-dep (name "bindgen") (req "^0.54.0") (default-features #t) (kind 1)) (crate-dep (name "cmake") (req "^0.1") (default-features #t) (kind 1)) (crate-dep (name "hex") (req "^0.4.2") (default-features #t) (kind 2)))) (hash "0g8cdpdfa291i7azzdfxci8zzmlrzs623kbv1swd3ipi9a9gar38")))

(define-public crate-fizzy-0.7 (crate (name "fizzy") (vers "0.7.0") (deps (list (crate-dep (name "bindgen") (req "^0.54.0") (default-features #t) (kind 1)) (crate-dep (name "cmake") (req "^0.1") (default-features #t) (kind 1)) (crate-dep (name "hex") (req "^0.4.2") (default-features #t) (kind 2)))) (hash "1ahg21k99ykp14x3mwknn03fx9bba2fb52z99kpwaj98r7ay9b1w")))

(define-public crate-fizzy-0.8 (crate (name "fizzy") (vers "0.8.0") (deps (list (crate-dep (name "bindgen") (req "^0.54.0") (default-features #t) (kind 1)) (crate-dep (name "cmake") (req "^0.1") (default-features #t) (kind 1)) (crate-dep (name "hex") (req "^0.4.2") (default-features #t) (kind 2)))) (hash "08yyfvzhcmcwr3z2xmnkrvn24mhp1w6kdixpa4swggrzbjm808ds")))

(define-public crate-fizzy-rs-0.0.7 (crate (name "fizzy-rs") (vers "0.0.7") (deps (list (crate-dep (name "copypasta-ext") (req "^0.4.4") (default-features #t) (kind 0)) (crate-dep (name "crossterm") (req "^0.26.1") (default-features #t) (kind 0)) (crate-dep (name "defer-lite") (req "^1.0.0") (default-features #t) (kind 0)))) (hash "16cv2i5lbd7p7mqzynv2w5ys7y0wgjn11nry6z1d9mjrv0drsnsx") (yanked #t)))

(define-public crate-fizzy-rs-0.0.8 (crate (name "fizzy-rs") (vers "0.0.8") (deps (list (crate-dep (name "copypasta-ext") (req "^0.4.4") (default-features #t) (kind 0)) (crate-dep (name "crossterm") (req "^0.26.1") (default-features #t) (kind 0)) (crate-dep (name "defer-lite") (req "^1.0.0") (default-features #t) (kind 0)) (crate-dep (name "xdg-home") (req "^1.0.0") (default-features #t) (kind 0)))) (hash "0msgrzlxk76zj3yj0b6xr64mb7g593jwp08ch11lh5mc6as1zxkn")))

(define-public crate-fizzy-rs-0.0.9 (crate (name "fizzy-rs") (vers "0.0.9") (deps (list (crate-dep (name "copypasta-ext") (req "^0.4.4") (default-features #t) (kind 0)) (crate-dep (name "crossterm") (req "^0.26.1") (default-features #t) (kind 0)) (crate-dep (name "defer-lite") (req "^1.0.0") (default-features #t) (kind 0)) (crate-dep (name "indicatif") (req "^0.17.5") (default-features #t) (kind 0)) (crate-dep (name "xdg-home") (req "^1.0.0") (default-features #t) (kind 0)))) (hash "1gspq7vslbdpk9fw87lbrgsqcazb0vys7mqay3zdwikp5rrj6fma")))

