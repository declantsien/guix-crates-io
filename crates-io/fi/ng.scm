(define-module (crates-io fi ng) #:use-module (crates-io))

(define-public crate-finger-0.1 (crate (name "finger") (vers "0.1.0") (hash "1wknqi18vlv4l3hadisr44b6lplxj5qsg3lh116l59f92l6arj7r")))

(define-public crate-fingerprint-0.0.1 (crate (name "fingerprint") (vers "0.0.1") (deps (list (crate-dep (name "clap") (req "^3.2.16") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "ffmpeg-next") (req "^5.0.3") (default-features #t) (kind 0)))) (hash "0hwzk44xlk4pj3x34a5jhywpmj2i3cz895h14v1nhwycz39f8n39") (features (quote (("video") ("text") ("image") ("default" "image" "video" "audio" "text") ("audio"))))))

(define-public crate-fingerprint-0.0.2 (crate (name "fingerprint") (vers "0.0.2") (deps (list (crate-dep (name "bitvec") (req "^1.0.1") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^3.2.16") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "divrem") (req "^1.0.0") (default-features #t) (kind 0)) (crate-dep (name "hex") (req "^0.4.3") (default-features #t) (kind 0)) (crate-dep (name "infer") (req "^0.9.0") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (features (quote ("small_rng"))) (default-features #t) (kind 0)))) (hash "094b03rhlg2ljqnncw8pnhclkd194rr9lc07652l8vy9hg4rp9mp") (features (quote (("video") ("text") ("image") ("default" "image" "video" "audio" "text") ("audio"))))))

(define-public crate-fingerprint-0.0.3 (crate (name "fingerprint") (vers "0.0.3") (deps (list (crate-dep (name "bitvec") (req "^1.0.1") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^3.2.16") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "divrem") (req "^1.0.0") (default-features #t) (kind 0)) (crate-dep (name "hex") (req "^0.4.3") (default-features #t) (kind 0)) (crate-dep (name "infer") (req "^0.9.0") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (features (quote ("small_rng"))) (default-features #t) (kind 0)) (crate-dep (name "rand_chacha") (req "^0.3.1") (default-features #t) (kind 0)))) (hash "19qimsw00xra4md5jdsfd0jfcdpasgx8srqxlg2kail8jspsw1ds") (features (quote (("video") ("text") ("image") ("default" "image" "video" "audio" "text") ("audio"))))))

(define-public crate-fingerprint-lib-0.1 (crate (name "fingerprint-lib") (vers "0.1.0") (deps (list (crate-dep (name "mac_address") (req "^1.1.3") (default-features #t) (kind 0)) (crate-dep (name "rust-crypto") (req "^0.2.36") (default-features #t) (kind 0)) (crate-dep (name "sys-info") (req "^0.9.1") (default-features #t) (kind 0)))) (hash "0qsaf618l9jsrzamx9jclm7i94pdm67nxjm7a4wws3vgc406xhfq")))

(define-public crate-fingerprint-lib-0.1 (crate (name "fingerprint-lib") (vers "0.1.1") (deps (list (crate-dep (name "mac_address") (req "^1.1.3") (default-features #t) (kind 0)) (crate-dep (name "rust-crypto") (req "^0.2.36") (default-features #t) (kind 0)) (crate-dep (name "sys-info") (req "^0.9.1") (default-features #t) (kind 0)))) (hash "0hbzig1ysfhglw75nwqwxjzlhyi88bavmw8gp57fgw2d4pgnmq2b")))

(define-public crate-fingerprint-lib-0.1 (crate (name "fingerprint-lib") (vers "0.1.2") (deps (list (crate-dep (name "mac_address") (req "^1.1.3") (default-features #t) (kind 0)) (crate-dep (name "rust-crypto") (req "^0.2.36") (default-features #t) (kind 0)) (crate-dep (name "sys-info") (req "^0.9.1") (default-features #t) (kind 0)))) (hash "13mqhwfw9r8i2asxb558xjvawasak2yx9ryplxp0ajln6pn3gmc2")))

(define-public crate-fingerprint-lib-0.1 (crate (name "fingerprint-lib") (vers "0.1.3") (deps (list (crate-dep (name "mac_address") (req "^1.1.3") (default-features #t) (kind 0)) (crate-dep (name "rust-crypto") (req "^0.2.36") (default-features #t) (kind 0)) (crate-dep (name "sys-info") (req "^0.9.1") (default-features #t) (kind 0)))) (hash "1ff2lvg54d938l3wjyl6mg3hklnc7b0c0820gzm8v5q9favslz1p")))

(define-public crate-fingerprint-lib-0.1 (crate (name "fingerprint-lib") (vers "0.1.4") (deps (list (crate-dep (name "mac_address") (req "^1.1.3") (default-features #t) (kind 0)) (crate-dep (name "rust-crypto") (req "^0.2.36") (default-features #t) (kind 0)) (crate-dep (name "sys-info") (req "^0.9.1") (default-features #t) (kind 0)))) (hash "0wnx6ynsxxqs2ikv4vakghm4ljai3lb9y1v5d0sb0x7iaq96208v")))

(define-public crate-fingerprint-lib-0.1 (crate (name "fingerprint-lib") (vers "0.1.5") (deps (list (crate-dep (name "mac_address") (req "^1.1.3") (default-features #t) (kind 0)) (crate-dep (name "rust-crypto") (req "^0.2.36") (default-features #t) (kind 0)) (crate-dep (name "sys-info") (req "^0.9.1") (default-features #t) (kind 0)))) (hash "1v61ffp2hq645xldzklh8l43m1qlwgrlwa3rlqjbrs28zafrjj0n")))

(define-public crate-fingerprint-struct-0.1 (crate (name "fingerprint-struct") (vers "0.1.0") (deps (list (crate-dep (name "blake2") (req "^0.10.4") (default-features #t) (kind 2)) (crate-dep (name "digest") (req "^0.10.5") (kind 0)) (crate-dep (name "fingerprint-struct-derive") (req "^0.1.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "hex") (req "^0.4.3") (default-features #t) (kind 2)) (crate-dep (name "mock-digest") (req "^0.1.0") (default-features #t) (kind 2)) (crate-dep (name "sha2") (req "^0.10.6") (default-features #t) (kind 2)))) (hash "0lgx2lb8f0p4cass35w1ah0kk504zng256v2cdgfwr6skpv0k5pi") (features (quote (("std" "alloc") ("os") ("derive" "fingerprint-struct-derive") ("default" "std" "derive") ("alloc")))) (rust-version "1.63")))

(define-public crate-fingerprint-struct-derive-0.1 (crate (name "fingerprint-struct-derive") (vers "0.1.0") (deps (list (crate-dep (name "proc-macro2") (req "^1.0.43") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.21") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.100") (default-features #t) (kind 0)))) (hash "06bagygmlxl9n6b4nnalcbjpx5vqlcf8y6q5avs35njpv6sbvj3y") (rust-version "1.63")))

(define-public crate-fingers-0.1 (crate (name "fingers") (vers "0.1.0") (hash "1hifqrcyr1lv945yybsbm7dl8ms6js4lc9m8wqslpidjnmfc5wxn")))

(define-public crate-fingers-0.1 (crate (name "fingers") (vers "0.1.1") (hash "0j62grcahiz5rkqfwghjhnw2jvbvjx86p5lii91bq7ihlbgnb1kw")))

(define-public crate-fingers-0.1 (crate (name "fingers") (vers "0.1.2") (hash "1qd11qpkzgnn6s94h04l9s6dlmp4v7wnv4slv9xxam2885i7fkhh")))

(define-public crate-fingertree-0.0.1 (crate (name "fingertree") (vers "0.0.1") (deps (list (crate-dep (name "epsilonz_algebra") (req "*") (default-features #t) (kind 0)))) (hash "1px9zw4cg1lr936089vsyhy71h39gcmn8wnwv60w0asi5yhgshck")))

(define-public crate-fingertrees-0.1 (crate (name "fingertrees") (vers "0.1.0") (deps (list (crate-dep (name "criterion") (req "^0.2") (default-features #t) (kind 2)) (crate-dep (name "quickcheck") (req "^0.6.2") (default-features #t) (kind 2)))) (hash "1n7qd6ypa7ycna7rq2aifvlq2l12si4xkw6vaywjsmzzwmrs6759")))

(define-public crate-fingertrees-0.1 (crate (name "fingertrees") (vers "0.1.1") (deps (list (crate-dep (name "criterion") (req "^0.2") (default-features #t) (kind 2)) (crate-dep (name "quickcheck") (req "^0.6.2") (default-features #t) (kind 2)))) (hash "0av20gai11srjfpd904djr9x2bvhycjizirr0ji4hnvnfl3rkx2c")))

(define-public crate-fingertrees-0.1 (crate (name "fingertrees") (vers "0.1.2") (deps (list (crate-dep (name "criterion") (req "^0.2") (default-features #t) (kind 2)) (crate-dep (name "quickcheck") (req "^0.6.2") (default-features #t) (kind 2)))) (hash "1jknjrh65annbffvvwycf4s9x18xwnadsh64f1zlndycj09c4zxm")))

(define-public crate-fingertrees-0.2 (crate (name "fingertrees") (vers "0.2.0") (deps (list (crate-dep (name "criterion") (req "^0.2") (default-features #t) (kind 2)) (crate-dep (name "quickcheck") (req "^0.6.2") (default-features #t) (kind 2)))) (hash "0ykgf42jmjimmkasi0agml46v80sin2g5c88f4j0mbvnbfhyhn5q")))

(define-public crate-fingertrees-0.2 (crate (name "fingertrees") (vers "0.2.1") (deps (list (crate-dep (name "criterion") (req "^0.2.5") (default-features #t) (kind 2)) (crate-dep (name "quickcheck") (req "^0.7.1") (default-features #t) (kind 2)))) (hash "1k0sz5ghpq0hz2drvfh7073bj67fd38jnwb5xsni23k40y361jc6")))

(define-public crate-fingertrees-0.2 (crate (name "fingertrees") (vers "0.2.2") (deps (list (crate-dep (name "criterion") (req "^0.2.5") (default-features #t) (kind 2)) (crate-dep (name "quickcheck") (req "^0.7.2") (default-features #t) (kind 2)))) (hash "1amngnr972anp2r981w2kgp3jrcn6gmjnpfl28yk59028wymcxqm")))

(define-public crate-fingertrees-0.2 (crate (name "fingertrees") (vers "0.2.3") (deps (list (crate-dep (name "criterion") (req "^0.3.0") (default-features #t) (kind 2)) (crate-dep (name "quickcheck") (req "^0.9.0") (default-features #t) (kind 2)))) (hash "07inrf5dzx5b14jbz9kk9mc7b4by8bcbgnj4hi8mj1jmzcx4yv05")))

(define-public crate-fingertrees-0.2 (crate (name "fingertrees") (vers "0.2.4") (deps (list (crate-dep (name "criterion") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "quickcheck") (req "^0.9") (default-features #t) (kind 2)))) (hash "1845lkh72daax10pwf4k77y6zid2fmpb0fz297qpm34w8avvxp6g")))

(define-public crate-fingertrees-0.2 (crate (name "fingertrees") (vers "0.2.5") (deps (list (crate-dep (name "criterion") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "quickcheck") (req "^0.9") (default-features #t) (kind 2)))) (hash "08dz19y32njxfd14rznxhjrz8naw7sygf2m72554vic090yvrlbp")))

(define-public crate-fingertrees-0.2 (crate (name "fingertrees") (vers "0.2.6") (deps (list (crate-dep (name "criterion") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "quickcheck") (req "^0.9") (default-features #t) (kind 2)))) (hash "1l3f1xml87ikjnhf7v1lv56ar80zdkwfk1x87cvi2s25967pvdjk")))

(define-public crate-fingertrees-0.2 (crate (name "fingertrees") (vers "0.2.7") (deps (list (crate-dep (name "criterion") (req "^0.3") (features (quote ("html_reports"))) (default-features #t) (kind 2)) (crate-dep (name "quickcheck") (req "^0.9") (default-features #t) (kind 2)))) (hash "0pdd8f2jciqqg0l7z1wimdngmy2dc4kwbkrhaj1zw5w6vg02bajk")))

(define-public crate-fingertrees-0.2 (crate (name "fingertrees") (vers "0.2.8") (deps (list (crate-dep (name "criterion") (req "^0.3") (features (quote ("html_reports"))) (default-features #t) (kind 2)) (crate-dep (name "quickcheck") (req "^0.9") (default-features #t) (kind 2)))) (hash "0wzpqrx86m07cbpmqxxn8pny7fbv2fc47cxqpxqlgwgvfqp7ymgm")))

(define-public crate-fingertrees-0.2 (crate (name "fingertrees") (vers "0.2.9") (deps (list (crate-dep (name "criterion") (req "^0.3") (features (quote ("html_reports"))) (default-features #t) (kind 2)) (crate-dep (name "quickcheck") (req "^0.9") (default-features #t) (kind 2)))) (hash "12safqbqi4qq0ymx2riq826kjqqcggs1lrgg6hnsrh9315syiiwz")))

(define-public crate-fingertrees-0.2 (crate (name "fingertrees") (vers "0.2.10") (deps (list (crate-dep (name "criterion") (req "^0.3") (features (quote ("html_reports"))) (default-features #t) (kind 2)) (crate-dep (name "quickcheck") (req "^0.9") (default-features #t) (kind 2)))) (hash "0zyfjljm1hl2nxbvmgx3508fk4s4sqwhks1cwidrgx9klbcrcpwc")))

(define-public crate-fingertrees-0.2 (crate (name "fingertrees") (vers "0.2.11") (deps (list (crate-dep (name "criterion") (req "^0.5") (features (quote ("html_reports"))) (default-features #t) (kind 2)) (crate-dep (name "quickcheck") (req "^1.0") (default-features #t) (kind 2)))) (hash "1xsw2mpfg9js81njb9jhw9fcxjp03m8jnj204dvjg7fckrz9w102")))

(define-public crate-fingles-0.1 (crate (name "fingles") (vers "0.1.0") (deps (list (crate-dep (name "murmurhash3") (req "^0.0.5") (default-features #t) (kind 0)))) (hash "04rppq8cd8ykhm6rvz9h8ij7aay30w570grrgz27dxi0bqdlhvgk")))

