(define-module (crates-io fi lt) #:use-module (crates-io))

(define-public crate-filter-0.1 (crate (name "filter") (vers "0.1.0") (deps (list (crate-dep (name "assert_approx_eq") (req "^1.1.0") (default-features #t) (kind 0)) (crate-dep (name "nalgebra") (req "^0.18") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2") (default-features #t) (kind 0)))) (hash "045nc8kkp7cqalby7wwr7sgpaz2pyysm35ssimic73nwymhq2xy5")))

(define-public crate-filter-0.2 (crate (name "filter") (vers "0.2.0") (deps (list (crate-dep (name "assert_approx_eq") (req "^1.1.0") (default-features #t) (kind 2)) (crate-dep (name "nalgebra") (req "^0.21.0") (kind 0)) (crate-dep (name "num-traits") (req "^0.2.11") (kind 0)))) (hash "0clcrymlb66ag07qbghcm2fx8wqnmm95iagwdaz7aayd4rnglh5i") (features (quote (("default" "alloc") ("alloc"))))))

(define-public crate-filter-city-0.1 (crate (name "filter-city") (vers "0.1.0") (deps (list (crate-dep (name "serde") (req "^1.0.102") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0.102") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.41") (default-features #t) (kind 0)) (crate-dep (name "unidecode") (req "^0.3.0") (default-features #t) (kind 0)))) (hash "0z42sr1swq2kv42xm2kljm3pgpf43gkwf12d9wfm3yjkj5b23jzx") (yanked #t)))

(define-public crate-filter-city-0.1 (crate (name "filter-city") (vers "0.1.1") (deps (list (crate-dep (name "serde") (req "^1.0.102") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0.102") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.41") (default-features #t) (kind 0)) (crate-dep (name "unidecode") (req "^0.3.0") (default-features #t) (kind 0)))) (hash "1z6hb3kfh1x8zwq0r2y0dyw9gzb0lkvbvsvvbbmh7bar5abcmw55") (yanked #t)))

(define-public crate-filter-city-0.1 (crate (name "filter-city") (vers "0.1.2") (deps (list (crate-dep (name "serde") (req "^1.0.102") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0.102") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.41") (default-features #t) (kind 0)) (crate-dep (name "unidecode") (req "^0.3.0") (default-features #t) (kind 0)))) (hash "1xis3plnj9daynr507wphdnbvs8r117wzdqhrgb7rabwk1nzbr2j") (yanked #t)))

(define-public crate-filter-city-0.1 (crate (name "filter-city") (vers "0.1.3") (deps (list (crate-dep (name "serde") (req "^1.0.102") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0.102") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.41") (default-features #t) (kind 0)) (crate-dep (name "unidecode") (req "^0.3.0") (default-features #t) (kind 0)))) (hash "0anz7268zh9wagdghhm3d2z2z520snhsawrlqzn6y0vx2qgf1rf0") (yanked #t)))

(define-public crate-filter-city-0.1 (crate (name "filter-city") (vers "0.1.4") (deps (list (crate-dep (name "serde") (req "^1.0.102") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0.102") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.41") (default-features #t) (kind 0)) (crate-dep (name "unidecode") (req "^0.3.0") (default-features #t) (kind 0)))) (hash "10pq44k2vd03x2dw6yg9ickpmj9gg079pkzfznqz3yp5aaiknzgr") (yanked #t)))

(define-public crate-filter-city-0.1 (crate (name "filter-city") (vers "0.1.5") (deps (list (crate-dep (name "serde") (req "^1.0.102") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0.102") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.41") (default-features #t) (kind 0)) (crate-dep (name "unidecode") (req "^0.3.0") (default-features #t) (kind 0)))) (hash "06nds59shi4pfpk1mvlj908q2d6c42vlfnb93aln3jlxw9pjzapb")))

(define-public crate-filter-city-0.1 (crate (name "filter-city") (vers "0.1.6") (deps (list (crate-dep (name "serde") (req "^1.0.102") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0.102") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.41") (default-features #t) (kind 0)) (crate-dep (name "unidecode") (req "^0.3.0") (default-features #t) (kind 0)))) (hash "1cw5w34269hk9ibv4phq7dx266xbkvb0yd98x97lxx1pi49cna3i")))

(define-public crate-filter-city-0.1 (crate (name "filter-city") (vers "0.1.7") (deps (list (crate-dep (name "serde") (req "^1.0.102") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0.102") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.41") (default-features #t) (kind 0)) (crate-dep (name "unidecode") (req "^0.3.0") (default-features #t) (kind 0)))) (hash "0zb9jsamsn5ncpa7isfb5g0r4kkwk0hpr614s185ylnfyiccp78z")))

(define-public crate-filter-city-0.1 (crate (name "filter-city") (vers "0.1.8") (deps (list (crate-dep (name "serde") (req "^1.0.102") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0.102") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.41") (default-features #t) (kind 0)) (crate-dep (name "unidecode") (req "^0.3.0") (default-features #t) (kind 0)))) (hash "0jg96k6vq7z082wvr5s8dzzfp8jm3nxpj2cy9rdszdd1h2vr0219")))

(define-public crate-filter-clipped-0.1 (crate (name "filter-clipped") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^3.2.15") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "env_logger") (req "^0.9.0") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.17") (default-features #t) (kind 0)) (crate-dep (name "rstest") (req "^0.15.0") (default-features #t) (kind 0)) (crate-dep (name "rust-htslib") (req "^0.39.5") (default-features #t) (kind 0)))) (hash "1xzichdmb10kvpldndzmgwl3rjr8jrdfllz1j8xy9bzv60y00hzx")))

(define-public crate-filter-clipped-0.2 (crate (name "filter-clipped") (vers "0.2.0") (deps (list (crate-dep (name "clap") (req "^3.2.15") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "env_logger") (req "^0.9.0") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.17") (default-features #t) (kind 0)) (crate-dep (name "rstest") (req "^0.15.0") (default-features #t) (kind 0)) (crate-dep (name "rust-htslib") (req "^0.39.5") (default-features #t) (kind 0)))) (hash "1b2x9q89h2r73xy1r8gq8gm55d1qm88rzxqz3yp7j2zz5ly2bafg")))

(define-public crate-filter-clipped-0.3 (crate (name "filter-clipped") (vers "0.3.0") (deps (list (crate-dep (name "clap") (req "^3.2.15") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "env_logger") (req "^0.9.0") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.17") (default-features #t) (kind 0)) (crate-dep (name "rstest") (req "^0.15.0") (default-features #t) (kind 0)) (crate-dep (name "rust-htslib") (req "^0.39.5") (default-features #t) (kind 0)))) (hash "0fjppwypsn2kswkzhimljqfhhkdrkrkr6bcvjj846zd2r8sl88c7")))

(define-public crate-filter-logger-0.1 (crate (name "filter-logger") (vers "0.1.0") (deps (list (crate-dep (name "chrono") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.2") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4") (features (quote ("std"))) (default-features #t) (kind 0)) (crate-dep (name "runtime-fmt") (req "^0.3") (default-features #t) (kind 0)))) (hash "1857aaz70qrbjf516gknyqwpldxwnjk409xzmhwgc58rbgs8v95d")))

(define-public crate-filter-logger-0.2 (crate (name "filter-logger") (vers "0.2.0") (deps (list (crate-dep (name "chrono") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.2") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4") (features (quote ("std"))) (default-features #t) (kind 0)) (crate-dep (name "runtime-fmt") (req "^0.3") (default-features #t) (kind 0)))) (hash "0cf3lahp83xdrngrviiym024r7xm63b1716i7qnfdm4vslfcg7y0")))

(define-public crate-filter-logger-0.3 (crate (name "filter-logger") (vers "0.3.0") (deps (list (crate-dep (name "chrono") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.2") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4") (features (quote ("std"))) (default-features #t) (kind 0)))) (hash "054qflxh7593js9vjqk0167aialsmnblrqiyi8qnh6xybnjxwfyb")))

(define-public crate-filter_ast-0.1 (crate (name "filter_ast") (vers "0.1.0") (deps (list (crate-dep (name "from_variants") (req "^0.5.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.104") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)))) (hash "0ib3zlqf4jibsk0ciiwif96lmx36da4lvp7sb2gzxm74xzqmqk3b")))

(define-public crate-filter_ast-0.1 (crate (name "filter_ast") (vers "0.1.1") (deps (list (crate-dep (name "serde") (req "^1.0.104") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)))) (hash "0hwbc5cyr92rba99bp6xyd65m1wn2w2z3p0dzxl6zx6qk86fg39r")))

(define-public crate-filter_ast-0.1 (crate (name "filter_ast") (vers "0.1.2") (deps (list (crate-dep (name "serde") (req "^1.0.104") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)))) (hash "17jl4f73mzyjs2w9k6rl1cr8gjk6a4r398xa2w6ra1dpa6wbq2jx")))

(define-public crate-filter_ast-0.2 (crate (name "filter_ast") (vers "0.2.0") (deps (list (crate-dep (name "serde") (req "^1.0.104") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)))) (hash "0dpl4hf5gflipp5pap4wac45114sxn371cbgcp9y2p74w7psrvbs")))

(define-public crate-filter_ast-0.2 (crate (name "filter_ast") (vers "0.2.1") (deps (list (crate-dep (name "serde") (req "^1.0.104") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)))) (hash "0dy43dqwwpy0mvbyfww5rd7sxlmy3hyq48vv66vq39gq5xmazjp8")))

(define-public crate-filter_cigars-0.0.1 (crate (name "filter_cigars") (vers "0.0.1") (deps (list (crate-dep (name "bio") (req "^0.31") (default-features #t) (kind 0)) (crate-dep (name "rust-htslib") (req "^0.31") (default-features #t) (kind 0)))) (hash "1r90mij2w69012ag9da0m73llm8cjx2p99h3k244iwjimnwdv9zw")))

(define-public crate-filterable-enum-0.1 (crate (name "filterable-enum") (vers "0.1.0") (deps (list (crate-dep (name "enumflags2") (req "^0.7.9") (default-features #t) (kind 0)) (crate-dep (name "filterable-enum-derive") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "1ix49l9c46rxm0rn6ykmrwqynvql7f4v7w1ma6hdd8i3hb73rh9q")))

(define-public crate-filterable-enum-derive-0.1 (crate (name "filterable-enum-derive") (vers "0.1.0") (deps (list (crate-dep (name "darling") (req "^0.20.8") (default-features #t) (kind 0)) (crate-dep (name "proc-macro-crate") (req "^3.1.0") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1.0.81") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.36") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2.0.60") (default-features #t) (kind 0)))) (hash "0ysrv0gq5yl9cc7avlhnssy7i5xqi5qhzh38jixpfyxqh5pa2np2")))

(define-public crate-filterfrom-0.1 (crate (name "filterfrom") (vers "0.1.0") (deps (list (crate-dep (name "paw") (req "^1.0.0") (default-features #t) (kind 0)) (crate-dep (name "stdinix") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "0jb2k6xwdxl1csqpxd1acprhslabbxdj1yz6q8c04znvskiy0kg3")))

(define-public crate-filterfrom-0.2 (crate (name "filterfrom") (vers "0.2.0") (deps (list (crate-dep (name "oops") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "paw") (req "^1.0.0") (default-features #t) (kind 0)) (crate-dep (name "stdinix") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "1vklpmcavpq6ghyarxadhyf4f8fs5hrg3nyyslp3avl29sixa540")))

(define-public crate-filterfrom-0.3 (crate (name "filterfrom") (vers "0.3.0") (deps (list (crate-dep (name "oops") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "paw") (req "^1.0.0") (default-features #t) (kind 0)) (crate-dep (name "stdinix") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "0l2k6pcnd71ljhqvryqxqz1msz9n4rdcl152d5mg376v7ifv2ayf")))

(define-public crate-filterfrom-0.4 (crate (name "filterfrom") (vers "0.4.0") (deps (list (crate-dep (name "oops") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "stdinix") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.3.20") (default-features #t) (kind 0)))) (hash "0ldrii9n213j3whzadsg87mnpd4jrdwsq02m2z51kqswg5ad4vwg")))

(define-public crate-filterfrom-0.4 (crate (name "filterfrom") (vers "0.4.1") (deps (list (crate-dep (name "oops") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "stdinix") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.3.20") (default-features #t) (kind 0)))) (hash "15dszj2mldx9pipn0air1smxa91dq9h233icx6fykdhcn5rp6v0l")))

(define-public crate-filterm-0.1 (crate (name "filterm") (vers "0.1.0") (deps (list (crate-dep (name "nix") (req "^0.23") (default-features #t) (kind 0)))) (hash "0kcgp2rkchp7g6p2hc7158p684asnljdvc866ipxhfinsg25b0ki") (yanked #t)))

(define-public crate-filterm-0.2 (crate (name "filterm") (vers "0.2.0") (deps (list (crate-dep (name "nix") (req "^0.24") (features (quote ("fs" "ioctl" "poll" "process" "signal" "term"))) (kind 0)))) (hash "06n6l8i71l32qiyzq72ai216l42fbgxzc816mzw408lgr24n5jbr") (yanked #t)))

(define-public crate-filterm-0.2 (crate (name "filterm") (vers "0.2.1") (deps (list (crate-dep (name "nix") (req "^0.24") (features (quote ("fs" "ioctl" "poll" "process" "signal" "term"))) (kind 0)))) (hash "0j7czl3frbkin9p8g3nghwmyb7bn3k82gja03v7bzz71svjhlr3q") (yanked #t)))

(define-public crate-filterm-0.1 (crate (name "filterm") (vers "0.1.1") (deps (list (crate-dep (name "nix") (req "^0.23") (default-features #t) (kind 0)))) (hash "1siqrpk4lzhia6bkmqzz9bd5d87lc85dsaaadlvbgxalrrz952yd")))

(define-public crate-filterm-0.2 (crate (name "filterm") (vers "0.2.2") (deps (list (crate-dep (name "nix") (req "^0.24") (features (quote ("fs" "ioctl" "poll" "process" "signal" "term"))) (kind 0)))) (hash "12j7pffb4wkvyczkd51z5nwrx2mr2k45aikbhpc89afspkp7m6sr")))

(define-public crate-filterm-0.1 (crate (name "filterm") (vers "0.1.2") (deps (list (crate-dep (name "nix") (req "^0.23") (default-features #t) (kind 0)))) (hash "1frv4y3sknl4dsz8jvb67gjf94bzfi6dr84r9dr4al7mwlyxs4iv")))

(define-public crate-filterm-0.2 (crate (name "filterm") (vers "0.2.3") (deps (list (crate-dep (name "nix") (req "^0.24") (features (quote ("fs" "ioctl" "poll" "process" "signal" "term"))) (kind 0)))) (hash "1kjpiy2l139ypim2chrh5dq9b3p2lvpzblih5wvzjg6cal4bcmjc")))

(define-public crate-filterm-0.2 (crate (name "filterm") (vers "0.2.4") (deps (list (crate-dep (name "nix") (req "^0.24") (features (quote ("fs" "ioctl" "poll" "process" "signal" "term"))) (kind 0)))) (hash "0cs7gjfxw5vmj787xyp0242cfxv743d81sdcifc7qmzfcymnmy52")))

(define-public crate-filterm-0.1 (crate (name "filterm") (vers "0.1.3") (deps (list (crate-dep (name "nix") (req "^0.23") (default-features #t) (kind 0)))) (hash "1j1ifzrqdk500z605wsy7iwl0f4fxyjb6y65i2qvpsbprpf1idn3")))

(define-public crate-filterm-0.2 (crate (name "filterm") (vers "0.2.5") (deps (list (crate-dep (name "nix") (req "^0.24") (features (quote ("fs" "ioctl" "poll" "process" "signal" "term"))) (kind 0)))) (hash "0zkjj2kly95fjpnqrwyy4455a1d5isdnjysmyh1cz76737mws2vb")))

(define-public crate-filterm-0.3 (crate (name "filterm") (vers "0.3.0") (deps (list (crate-dep (name "nix") (req "^0.26") (features (quote ("fs" "ioctl" "poll" "process" "signal" "term"))) (kind 0)))) (hash "0dqvysm721qs916av2dpal1y55yhwb2l0s5aa53lq05a2sbavg8g")))

(define-public crate-filterm-0.4 (crate (name "filterm") (vers "0.4.0") (deps (list (crate-dep (name "nix") (req "^0.26") (features (quote ("fs" "ioctl" "poll" "process" "signal" "term"))) (kind 0)))) (hash "1q6bx0xhqy44gzas4l9br12aiqq2ggbgs8rckcpyg2ahjjqywy37") (rust-version "1.63")))

(define-public crate-filterm-0.4 (crate (name "filterm") (vers "0.4.1") (deps (list (crate-dep (name "atomic-int") (req "^0.1") (features (quote ("c_int" "signal"))) (kind 0)) (crate-dep (name "nix") (req "^0.26") (features (quote ("fs" "ioctl" "poll" "process" "signal" "term"))) (kind 0)))) (hash "1654rrazz6vwblxx30phzn0nf40awdnw6vxf3l4hxifz85xig5c6") (rust-version "1.63")))

(define-public crate-filterm-0.4 (crate (name "filterm") (vers "0.4.2") (deps (list (crate-dep (name "atomic-int") (req "^0.1") (features (quote ("c_int" "signal"))) (kind 0)) (crate-dep (name "nix") (req "^0.26") (features (quote ("fs" "ioctl" "poll" "process" "signal" "term"))) (kind 0)))) (hash "1fphsnxygxyz52wnfzzm1lcjl07j74bj3xanw1fqbrsf340xy32g") (rust-version "1.63")))

(define-public crate-filters-0.1 (crate (name "filters") (vers "0.1.0") (hash "14y3qjlliwldm2x453d1kwdxrs23iql78nb7hbq3mhgfyl7dn2rp")))

(define-public crate-filters-0.1 (crate (name "filters") (vers "0.1.1") (hash "0hkyj6kr0qknx3cahjk38h04hh41hp8hajslx0nbs79hbnihhlbd") (features (quote (("unstable-filter-as-fn"))))))

(define-public crate-filters-0.2 (crate (name "filters") (vers "0.2.0") (hash "04yc7by54i0ksgaq4ad06ybh795vmhklz5kwbxbmw8pnq8afca2x") (features (quote (("unstable-filter-as-fn"))))))

(define-public crate-filters-0.3 (crate (name "filters") (vers "0.3.0") (hash "0jr77s8nsr02hlsqcr49rizik95ag8bdhcxpncq7qwq626r2x3k1") (features (quote (("unstable-filter-as-fn"))))))

(define-public crate-filters-0.4 (crate (name "filters") (vers "0.4.0") (hash "1qxk235404f473pvsk9mv37a3qpkidhbq9j1alwl3f1v59ca78hf") (features (quote (("unstable-filter-as-fn"))))))

(define-public crate-filterway-0.1 (crate (name "filterway") (vers "0.1.0") (deps (list (crate-dep (name "aargvark") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "defer") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "rustix") (req "^0.38") (features (quote ("fs"))) (default-features #t) (kind 0)))) (hash "1477ay6l2nhmwhl50lcy5lz4m1wwa1ygw2wh2i869v0k2vfbyvrk")))

(define-public crate-filtration-domination-0.0.1 (crate (name "filtration-domination") (vers "0.0.1") (deps (list (crate-dep (name "anyhow") (req "^1.0.56") (default-features #t) (kind 2)) (crate-dep (name "clap") (req "^3.1.9") (features (quote ("derive"))) (default-features #t) (kind 2)) (crate-dep (name "litemap") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "num") (req "^0.4.0") (default-features #t) (kind 0)) (crate-dep (name "ordered-float") (req "^2.8.0") (features (quote ("rand"))) (default-features #t) (kind 0)) (crate-dep (name "paste") (req "^1.0.6") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)) (crate-dep (name "rustc-hash") (req "^1.1.0") (default-features #t) (kind 0)) (crate-dep (name "sorted-iter") (req "^0.1.8") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0") (default-features #t) (kind 0)))) (hash "1dl65mmihr3wasi8nwq6x8yizmhw073kv3wzrzz7pi53bpr8ficp")))

