(define-module (crates-io fi lo) #:use-module (crates-io))

(define-public crate-filo-0.1 (crate (name "filo") (vers "0.1.0") (hash "1h3jjyhw8xjp4181m7iklpj3y9kxpxsaada6p20px4bpm2bxdddl")))

(define-public crate-filon-1 (crate (name "filon") (vers "1.0.0") (hash "0nwigf6gax306vwj84pzvjyfyh0yf6zxsa4n20a96ma20lr3l104")))

(define-public crate-filon-1 (crate (name "filon") (vers "1.1.0") (deps (list (crate-dep (name "num-complex") (req "^0.4.4") (default-features #t) (kind 0)))) (hash "0k8bbvd4j9sr8dsxcim0hr5fmh215nj0vja2xiyvd4pp0cs16qjj")))

(define-public crate-filon-1 (crate (name "filon") (vers "1.2.0") (deps (list (crate-dep (name "num-complex") (req "^0.4.4") (default-features #t) (kind 0)))) (hash "1wlx77hbm4n2d3dp2sbvq3ma97d5d85cfr13g2k58yc9wrwb6zbc")))

