(define-module (crates-io fi x_) #:use-module (crates-io))

(define-public crate-fix_checksum-0.0.1 (crate (name "fix_checksum") (vers "0.0.1") (hash "1r38nf2bh6l3ripyjlbia9iw50fzz8cbd8kxlr84h3xh6qyvg4cd")))

(define-public crate-fix_float-0.1 (crate (name "fix_float") (vers "0.1.0") (deps (list (crate-dep (name "criterion") (req "^0.3") (features (quote ("html_reports"))) (default-features #t) (kind 2)) (crate-dep (name "doc-comment") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 2)) (crate-dep (name "rand_chacha") (req "^0.3") (default-features #t) (kind 2)))) (hash "0ccba03xcpw861726hni0absz0q8n437c7yka2aqlyaij1qcfirc")))

(define-public crate-fix_float-0.1 (crate (name "fix_float") (vers "0.1.1") (deps (list (crate-dep (name "criterion") (req "^0.3") (features (quote ("html_reports"))) (default-features #t) (kind 2)) (crate-dep (name "doc-comment") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 2)) (crate-dep (name "rand_chacha") (req "^0.3") (default-features #t) (kind 2)))) (hash "14091jzd264xl2qbdsf01imjx4px91gf00jbjap89z9w3l7y6h3b")))

(define-public crate-fix_float-0.1 (crate (name "fix_float") (vers "0.1.2") (deps (list (crate-dep (name "criterion") (req "^0.3") (features (quote ("html_reports"))) (default-features #t) (kind 2)) (crate-dep (name "doc-comment") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 2)) (crate-dep (name "rand_chacha") (req "^0.3") (default-features #t) (kind 2)))) (hash "1sf2nii91klxwdanxng8wz8b08jkmr36g9q4k9bw1rhw4wlspnax")))

(define-public crate-fix_float-0.1 (crate (name "fix_float") (vers "0.1.3") (deps (list (crate-dep (name "criterion") (req "^0.3") (features (quote ("html_reports"))) (default-features #t) (kind 2)) (crate-dep (name "doc-comment") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 2)) (crate-dep (name "rand_chacha") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 2)))) (hash "0k7an2ldbbj9sxs0x80k3jrbp18p255w4awv100j7kq5rca21nb1")))

(define-public crate-fix_float-0.1 (crate (name "fix_float") (vers "0.1.4") (deps (list (crate-dep (name "criterion") (req "^0.3") (features (quote ("html_reports"))) (default-features #t) (kind 2)) (crate-dep (name "doc-comment") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 2)) (crate-dep (name "rand_chacha") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 2)))) (hash "1afanf0za2bg0z3p6f0hkqdywi4b88x6j20ma5pqg72rp3w04zir")))

(define-public crate-fix_fn-0.1 (crate (name "fix_fn") (vers "0.1.0") (hash "1m4dcd1dgijk1x1rgyji71g2g2q4y2f0rf13dh004w843c3bcsaw")))

(define-public crate-fix_fn-1 (crate (name "fix_fn") (vers "1.0.0") (hash "0fvqjsxfymma66qbls83n33whr2kc8andhn237m7ls0s2qc6dj3w")))

(define-public crate-fix_fn-1 (crate (name "fix_fn") (vers "1.0.1") (hash "0ksgqbr2ssha8ipw2lzq34n18hfcflr3ckkdbafdzp99p3rg3z4y")))

(define-public crate-fix_fn-1 (crate (name "fix_fn") (vers "1.0.2") (hash "1wgphq05iglhcvyqajc7fsjgm6s4pshm1xznw375hwj12cz1j3rq")))

(define-public crate-fix_me-0.1 (crate (name "fix_me") (vers "0.1.0") (hash "0xy6g3c4w4gfh1rwjrggxzv6fssry6mrc5v0hz0f8pvq1y736m43") (features (quote (("unfixed_code") ("default") ("build_tests"))))))

(define-public crate-fix_me-0.1 (crate (name "fix_me") (vers "0.1.1") (hash "19h45klac3yhp94s1dbbpq4ff2bwm3aaw4gk350pcx8i2bdzh8mg") (features (quote (("unfixed_code") ("build_tests"))))))

(define-public crate-fix_me-0.1 (crate (name "fix_me") (vers "0.1.2") (hash "05wwafimriir2jain3jwjrfslzbdgar7xawllk09q6gajybck12g") (features (quote (("unfixed_code"))))))

