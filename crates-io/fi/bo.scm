(define-module (crates-io fi bo) #:use-module (crates-io))

(define-public crate-fibo-0.0.1 (crate (name "fibo") (vers "0.0.1") (deps (list (crate-dep (name "num-bigint") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2") (default-features #t) (kind 0)))) (hash "0ipsn403gk05zvkarm5hrgi8sqvmp0dbjfz4dxk9r7wymg90bl22")))

(define-public crate-fibo-0.0.2 (crate (name "fibo") (vers "0.0.2") (deps (list (crate-dep (name "num-bigint") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2") (default-features #t) (kind 0)))) (hash "0lnbibmsfbc972vk0q34chri0b5x369azc2dsl76pfcz8hf7zvqj")))

(define-public crate-fibonacci-0.1 (crate (name "fibonacci") (vers "0.1.0") (deps (list (crate-dep (name "num") (req "^0.1") (default-features #t) (kind 0)))) (hash "09zlf3awljc7xh11rs3izddcxrksgizi5kajz1lj649i81ca01p2")))

(define-public crate-fibonacci-0.1 (crate (name "fibonacci") (vers "0.1.1") (deps (list (crate-dep (name "num") (req "^0.1") (default-features #t) (kind 0)))) (hash "12in929rj6ka87qn2qhr63g1a130fbivvhb4hxk2mpp3xza0mls2")))

(define-public crate-fibonacci-like-0.1 (crate (name "fibonacci-like") (vers "0.1.0") (deps (list (crate-dep (name "cfg-if") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "num-bigint") (req "^0.4") (optional #t) (default-features #t) (kind 0)))) (hash "017algir6g3s7dmksxsgk9klhk83ai1lqqgz06wpy10kmrjrmcwn") (features (quote (("std") ("default" "std" "big-int") ("big-int" "num-bigint" "std")))) (yanked #t)))

(define-public crate-fibonacci-like-0.1 (crate (name "fibonacci-like") (vers "0.1.1") (deps (list (crate-dep (name "cfg-if") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "num-bigint") (req "^0.4") (optional #t) (default-features #t) (kind 0)))) (hash "1sv572c8sdqp8bp2fsmk32js2wkcvhwlrpbbzh3winrj2k7csnbn") (features (quote (("std") ("default" "std" "big-int") ("big-int" "num-bigint" "std"))))))

(define-public crate-fibonacci-like-0.1 (crate (name "fibonacci-like") (vers "0.1.2") (deps (list (crate-dep (name "cfg-if") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "num-bigint") (req "^0.4") (optional #t) (default-features #t) (kind 0)))) (hash "00ayzg82agrgf46max1y9s3lggws73pyigkz50nlfnn3d51i0dwb") (features (quote (("std") ("default" "std" "big-int") ("big-int" "num-bigint" "std"))))))

(define-public crate-fibonacci-like-0.1 (crate (name "fibonacci-like") (vers "0.1.3") (deps (list (crate-dep (name "cfg-if") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "num-bigint") (req "^0.2") (optional #t) (default-features #t) (kind 0)))) (hash "1nv4yjdw8vnw1mgf97rwpfbdnwl4f1vix29y5q15ngv2cgacyi7j") (features (quote (("std") ("default" "std" "big-int") ("big-int" "num-bigint" "std"))))))

(define-public crate-fibonacci-numbers-0.0.0 (crate (name "fibonacci-numbers") (vers "0.0.0") (hash "18np7x1j31j6j7r262y0x6f1y9xbz3139blm4ldr6a48ykjb6s8q")))

(define-public crate-fibonacci-numbers-1 (crate (name "fibonacci-numbers") (vers "1.0.0") (hash "07c6nf9xdm2jkn1lyxip6x17xrf00dka6ws5ysi33w37chdpin89")))

(define-public crate-fibonacci-numbers-2 (crate (name "fibonacci-numbers") (vers "2.0.0") (deps (list (crate-dep (name "fib0") (req "^0") (default-features #t) (kind 0) (package "fibonacci-numbers")) (crate-dep (name "fib1") (req "^1") (default-features #t) (kind 0) (package "fibonacci-numbers")))) (hash "17w06dg0f6ii9hps1s02jbwyc67whm7ylnhqzydzrp159b6ia06p")))

(define-public crate-fibonacci-numbers-3 (crate (name "fibonacci-numbers") (vers "3.0.0") (deps (list (crate-dep (name "fib1") (req "^1") (default-features #t) (kind 0) (package "fibonacci-numbers")) (crate-dep (name "fib2") (req "^2") (default-features #t) (kind 0) (package "fibonacci-numbers")))) (hash "0bsk5a2bpxhkg4nfxkvn1bi1ic98m1w355pdv4k975j0f3qpi4n3")))

(define-public crate-fibonacci-numbers-4 (crate (name "fibonacci-numbers") (vers "4.0.0") (deps (list (crate-dep (name "fib2") (req "^2") (default-features #t) (kind 0) (package "fibonacci-numbers")) (crate-dep (name "fib3") (req "^3") (default-features #t) (kind 0) (package "fibonacci-numbers")))) (hash "0ygayzcqpz9k0bkw642q5sznfcf956hzrzznn40r1nfcfnxwz1cp")))

(define-public crate-fibonacci-numbers-5 (crate (name "fibonacci-numbers") (vers "5.0.0") (deps (list (crate-dep (name "fib3") (req "^3") (default-features #t) (kind 0) (package "fibonacci-numbers")) (crate-dep (name "fib4") (req "^4") (default-features #t) (kind 0) (package "fibonacci-numbers")))) (hash "1fyazwjgkssfn7bkzpxpjpsbkngydz50qwl7vm1s90ldkpjb8rk2")))

(define-public crate-fibonacci-numbers-6 (crate (name "fibonacci-numbers") (vers "6.0.0") (deps (list (crate-dep (name "fib4") (req "^4") (default-features #t) (kind 0) (package "fibonacci-numbers")) (crate-dep (name "fib5") (req "^5") (default-features #t) (kind 0) (package "fibonacci-numbers")))) (hash "1hz3ij1y7049067ix19280ymmxqm56l3q4si5szbdyv448ixdycx")))

(define-public crate-fibonacci-numbers-7 (crate (name "fibonacci-numbers") (vers "7.0.0") (deps (list (crate-dep (name "fib5") (req "^5") (default-features #t) (kind 0) (package "fibonacci-numbers")) (crate-dep (name "fib6") (req "^6") (default-features #t) (kind 0) (package "fibonacci-numbers")))) (hash "08gzmg17fqr487l6njfsr0dzg67j6hlvfl8xbzb9n6yhla9d06mc")))

(define-public crate-fibonacci-numbers-8 (crate (name "fibonacci-numbers") (vers "8.0.0") (deps (list (crate-dep (name "fib6") (req "^6") (default-features #t) (kind 0) (package "fibonacci-numbers")) (crate-dep (name "fib7") (req "^7") (default-features #t) (kind 0) (package "fibonacci-numbers")))) (hash "0c9ny49s4h39vxfk0yjqibkx272snp051yxll145gnxr0sh0bnmg")))

(define-public crate-fibonacci-numbers-9 (crate (name "fibonacci-numbers") (vers "9.0.0") (deps (list (crate-dep (name "fib7") (req "^7") (default-features #t) (kind 0) (package "fibonacci-numbers")) (crate-dep (name "fib8") (req "^8") (default-features #t) (kind 0) (package "fibonacci-numbers")))) (hash "0pic0cg2sazbq6ad557d6z40qwfmdk9yq1rf1n7z2nvvrszj2xs0")))

(define-public crate-fibonacci-numbers-10 (crate (name "fibonacci-numbers") (vers "10.0.0") (deps (list (crate-dep (name "fib8") (req "^8") (default-features #t) (kind 0) (package "fibonacci-numbers")) (crate-dep (name "fib9") (req "^9") (default-features #t) (kind 0) (package "fibonacci-numbers")))) (hash "1zklx4i4bcs1135xr9qwwf46yadpfi6q2j8c24r5c6x12pgv960v")))

(define-public crate-fibonacci-numbers-11 (crate (name "fibonacci-numbers") (vers "11.0.0") (deps (list (crate-dep (name "fib10") (req "^10") (default-features #t) (kind 0) (package "fibonacci-numbers")) (crate-dep (name "fib9") (req "^9") (default-features #t) (kind 0) (package "fibonacci-numbers")))) (hash "0x0gwwn67qa2q4aysv99cszxg8a86ls64m6z61vbshh711cfw3vm")))

(define-public crate-fibonacci-numbers-12 (crate (name "fibonacci-numbers") (vers "12.0.0") (deps (list (crate-dep (name "fib10") (req "^10") (default-features #t) (kind 0) (package "fibonacci-numbers")) (crate-dep (name "fib11") (req "^11") (default-features #t) (kind 0) (package "fibonacci-numbers")))) (hash "0zjdaz9wp8kzmqyawwvb5qgzd8wv546l561yrijq2gm9n0488apv")))

(define-public crate-fibonacci-numbers-13 (crate (name "fibonacci-numbers") (vers "13.0.0") (deps (list (crate-dep (name "fib11") (req "^11") (default-features #t) (kind 0) (package "fibonacci-numbers")) (crate-dep (name "fib12") (req "^12") (default-features #t) (kind 0) (package "fibonacci-numbers")))) (hash "0c4mrg9kv6v379afrkmn6v8xb54dfndpvl7mlf35mr7sv2xnbr5a")))

(define-public crate-fibonacci-numbers-14 (crate (name "fibonacci-numbers") (vers "14.0.0") (deps (list (crate-dep (name "fib12") (req "^12") (default-features #t) (kind 0) (package "fibonacci-numbers")) (crate-dep (name "fib13") (req "^13") (default-features #t) (kind 0) (package "fibonacci-numbers")))) (hash "0kbvwrn5wfix8lqr5414agfdmkmax95afsg06dynkb352x8bgy3j")))

(define-public crate-fibonacci-numbers-15 (crate (name "fibonacci-numbers") (vers "15.0.0") (deps (list (crate-dep (name "fib13") (req "^13") (default-features #t) (kind 0) (package "fibonacci-numbers")) (crate-dep (name "fib14") (req "^14") (default-features #t) (kind 0) (package "fibonacci-numbers")))) (hash "0lkqib96l5xapbz7z5wzka5746n41w864w9y905fpmf229r4w9rv")))

(define-public crate-fibonacci-numbers-16 (crate (name "fibonacci-numbers") (vers "16.0.0") (deps (list (crate-dep (name "fib14") (req "^14") (default-features #t) (kind 0) (package "fibonacci-numbers")) (crate-dep (name "fib15") (req "^15") (default-features #t) (kind 0) (package "fibonacci-numbers")))) (hash "131d2p77kik9ds48yy3b85iw9j18nlg0gardcfj5dhjdla1ir2js")))

(define-public crate-fibonacci-numbers-17 (crate (name "fibonacci-numbers") (vers "17.0.0") (deps (list (crate-dep (name "fib15") (req "^15") (default-features #t) (kind 0) (package "fibonacci-numbers")) (crate-dep (name "fib16") (req "^16") (default-features #t) (kind 0) (package "fibonacci-numbers")))) (hash "1fmy09hr5dsnkwpfspl73jwlnc25h64lgdchma8pibwx78kn03gm")))

(define-public crate-fibonacci-numbers-18 (crate (name "fibonacci-numbers") (vers "18.0.0") (deps (list (crate-dep (name "fib16") (req "^16") (default-features #t) (kind 0) (package "fibonacci-numbers")) (crate-dep (name "fib17") (req "^17") (default-features #t) (kind 0) (package "fibonacci-numbers")))) (hash "1l01k758q1f9wi5d9kg81bnxfz10w54xyy00diami76140xgw3j9")))

(define-public crate-fibonacci-numbers-19 (crate (name "fibonacci-numbers") (vers "19.0.0") (deps (list (crate-dep (name "fib17") (req "^17") (default-features #t) (kind 0) (package "fibonacci-numbers")) (crate-dep (name "fib18") (req "^18") (default-features #t) (kind 0) (package "fibonacci-numbers")))) (hash "17fv5pc06qbfh9lb1zhamksxjxnj0h6n3arkgijvg5gdrc8iq7jv")))

(define-public crate-fibonacci_codec-0.1 (crate (name "fibonacci_codec") (vers "0.1.0") (deps (list (crate-dep (name "bit-vec") (req "^0.4.4") (default-features #t) (kind 0)) (crate-dep (name "failure") (req "^0.1.1") (default-features #t) (kind 0)) (crate-dep (name "failure_derive") (req "^0.1.1") (default-features #t) (kind 0)) (crate-dep (name "num") (req "^0.1.42") (default-features #t) (kind 0)))) (hash "1mpgqn5b1a3rlq2wg6v8l5p9p2hwbfvjwxckaz40gn7b8l4iq5c5")))

(define-public crate-fibonacci_codec-0.1 (crate (name "fibonacci_codec") (vers "0.1.1") (deps (list (crate-dep (name "bit-vec") (req "^0.4.4") (default-features #t) (kind 0)) (crate-dep (name "failure") (req "^0.1.1") (default-features #t) (kind 0)) (crate-dep (name "failure_derive") (req "^0.1.1") (default-features #t) (kind 0)) (crate-dep (name "num") (req "^0.1.42") (default-features #t) (kind 0)))) (hash "0r13yb23rd5n9ijgwaf2csk6i29xkpwphg427wf2vxghzvns3af4")))

(define-public crate-fibonacci_codec-0.2 (crate (name "fibonacci_codec") (vers "0.2.0") (deps (list (crate-dep (name "bit-vec") (req "^0.6.0") (default-features #t) (kind 0)) (crate-dep (name "criterion") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "failure") (req "^0.1.1") (default-features #t) (kind 0)) (crate-dep (name "failure_derive") (req "^0.1.1") (default-features #t) (kind 0)) (crate-dep (name "num") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "num") (req "^0.4") (default-features #t) (kind 1)) (crate-dep (name "proptest") (req "^1.0.0") (default-features #t) (kind 2)))) (hash "1aiisgw116i9vs69xnydw55ipmxrhqqmrsnjl9mdg8q65b38r3l3")))

(define-public crate-fibonacci_guru-1 (crate (name "fibonacci_guru") (vers "1.0.0") (deps (list (crate-dep (name "chrono") (req "^0.4.19") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^2.34.0") (default-features #t) (kind 0)) (crate-dep (name "num-bigint") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0") (default-features #t) (kind 0)))) (hash "0b7p4i9f4hjfgy6pqz9y4xbcws07xp4j98bx2ym2ddjgszh2i0mp")))

(define-public crate-fibonacci_guru-1 (crate (name "fibonacci_guru") (vers "1.0.1") (deps (list (crate-dep (name "chrono") (req "^0.4.19") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^2.34.0") (default-features #t) (kind 0)) (crate-dep (name "num-bigint") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0") (default-features #t) (kind 0)))) (hash "0ciy3lg0dwplif0274k0j417vk5x8835a4qwwcij9c4sgzapamm5")))

(define-public crate-fibonacci_retracement-0.1 (crate (name "fibonacci_retracement") (vers "0.1.0") (hash "1m12962mf9sij56k5rx3b1k0g35r3d0i4l42kr1qyp6cnphsrall")))

(define-public crate-fibonacci_series-0.1 (crate (name "fibonacci_series") (vers "0.1.0") (hash "1wmk6r5niwg3sy0z9hiz18sd8ahb63m159w7606zfsdr8b52sjyx")))

(define-public crate-fibonacci_series-0.2 (crate (name "fibonacci_series") (vers "0.2.0") (hash "1hrlpgmj8q92g3nxg14zfks4rpsw9qia5jbhzszwb24fhgj0bls2")))

(define-public crate-fibonacii-heap-0.1 (crate (name "fibonacii-heap") (vers "0.1.0") (deps (list (crate-dep (name "criterion") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.8") (default-features #t) (kind 2)))) (hash "19wrwwcqrx04jrph62hmyimikwmvcd116bp6v14k8drhxkk6kiqv")))

(define-public crate-fibonacii-heap-0.1 (crate (name "fibonacii-heap") (vers "0.1.1") (deps (list (crate-dep (name "criterion") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.8") (default-features #t) (kind 2)))) (hash "1svgvnbrxzqynkwawdk9s8i90gp7n2z7i3qd8ghb6ya13631d086")))

(define-public crate-fibonacii-heap-0.1 (crate (name "fibonacii-heap") (vers "0.1.2") (deps (list (crate-dep (name "criterion") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.8") (default-features #t) (kind 2)))) (hash "1p9vh3yvmg15i2kakfzpbw9yw9h3vjf18f29j1ciinns11pzpa5g")))

(define-public crate-fibonnaci-stream-0.1 (crate (name "fibonnaci-stream") (vers "0.1.0") (deps (list (crate-dep (name "failure") (req "^0.1.1") (default-features #t) (kind 0)) (crate-dep (name "failure_derive") (req "^0.1.1") (default-features #t) (kind 0)) (crate-dep (name "futures") (req "^0.1.18") (default-features #t) (kind 0)))) (hash "1820b0yshwm3grk5psi0jszm1r3kv001q83bs6qz7pa4c2flw5a2")))

(define-public crate-fibora-0.1 (crate (name "fibora") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^3.0.0-beta.4") (default-features #t) (kind 0)))) (hash "1vkqkzq878p3s4m7003ssf2nwlmnkphk9p6vbn2zyqnfsf7889w6")))

(define-public crate-fibora-1 (crate (name "fibora") (vers "1.0.0") (deps (list (crate-dep (name "clap") (req "^3.0.0-beta.4") (default-features #t) (kind 0)))) (hash "1cwagfayzf6acd8bwpkd3w052mvp5fcgv46ygg408jfyxhg6pap5")))

(define-public crate-fibora-1 (crate (name "fibora") (vers "1.1.0") (deps (list (crate-dep (name "clap") (req "^3.0.0-beta.4") (optional #t) (default-features #t) (kind 0)))) (hash "1fhkvm7bgpsawphd9viz8pyp2nfki5810zrf4ycs2dfh92dl9lvz") (features (quote (("binaries" "clap"))))))

(define-public crate-fibora-1 (crate (name "fibora") (vers "1.2.0") (deps (list (crate-dep (name "clap") (req "^3.1.8") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "0m65s1yvc5sm9if5vvx95wyh48afqzvpmhqlimkrx52xgjwiz57y")))

