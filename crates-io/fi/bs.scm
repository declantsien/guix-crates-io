(define-module (crates-io fi bs) #:use-module (crates-io))

(define-public crate-fibs-0.1 (crate (name "fibs") (vers "0.1.0") (deps (list (crate-dep (name "num") (req "^0") (default-features #t) (kind 0)))) (hash "0b19r59yn9849l5068m2d3pbbqlfb005kfjc3ndym9g7pcpmmah2") (features (quote (("std") ("default" "std"))))))

(define-public crate-fibs-0.2 (crate (name "fibs") (vers "0.2.0") (deps (list (crate-dep (name "num") (req "^0") (kind 0)))) (hash "1z9rygkylg2k55fv9xs7phxs7nhf7i1nh7qb758p9zxv51prs3r7") (features (quote (("std" "num/std") ("default" "std"))))))

(define-public crate-fibs-0.2 (crate (name "fibs") (vers "0.2.1") (deps (list (crate-dep (name "num") (req "^0") (kind 0)))) (hash "1jwnmcyhbiskzzaww6122adhdf6n5fdb9nybc4pgcjfj18k02h85") (features (quote (("std" "num/std") ("default" "std"))))))

(define-public crate-fibs-0.2 (crate (name "fibs") (vers "0.2.2") (deps (list (crate-dep (name "num") (req "^0") (kind 0)) (crate-dep (name "paste") (req "^1") (default-features #t) (kind 2)))) (hash "00c8w5q3wa2vy9lks9ib3hvvld36rm4c3hzx31r5p1xw44fpxp6p") (features (quote (("std" "num/std") ("default" "std") ("bench" "std"))))))

(define-public crate-fibs-0.2 (crate (name "fibs") (vers "0.2.3") (deps (list (crate-dep (name "num") (req "^0") (kind 0)) (crate-dep (name "paste") (req "^1") (default-features #t) (kind 2)))) (hash "0lbpml9xpgalrawaijqps3dz0j3cfp7840jr6a4l6b7yiz3fw9c5") (features (quote (("std" "num/std") ("default" "std") ("bench" "std"))))))

(define-public crate-fibs-0.2 (crate (name "fibs") (vers "0.2.4") (deps (list (crate-dep (name "num") (req "^0") (kind 0)) (crate-dep (name "paste") (req "^1") (default-features #t) (kind 2)))) (hash "0nzsp3npnmckgnwfl0kldlkavh6bbvl84a510g7sp5qffd96xpm3") (features (quote (("std" "num/std") ("default" "std") ("bench" "std"))))))

(define-public crate-fibs-0.2 (crate (name "fibs") (vers "0.2.5") (deps (list (crate-dep (name "num") (req "^0") (kind 0)) (crate-dep (name "paste") (req "^1") (default-features #t) (kind 2)))) (hash "0dj1wps6cwz2aqzin3p256d0j3mnyzr47k4i81424fldl8lixv21") (features (quote (("std" "num/std") ("default" "std") ("bench" "std"))))))

(define-public crate-fibs-0.2 (crate (name "fibs") (vers "0.2.6") (deps (list (crate-dep (name "num") (req "^0") (kind 0)) (crate-dep (name "paste") (req "^1") (default-features #t) (kind 2)))) (hash "0n9g6wr4y4wlv8l5mvac8w4l9lz8fvimdfwk99a08ci30473s2fq") (features (quote (("std" "num/std") ("default" "std") ("bench" "std"))))))

(define-public crate-fibs-0.2 (crate (name "fibs") (vers "0.2.7") (deps (list (crate-dep (name "num") (req "^0") (kind 0)) (crate-dep (name "paste") (req "^1") (default-features #t) (kind 2)))) (hash "1zla2y2rihq8lckn53xg68xnpn0a8vpwzdb7nic918m40y35mlzq") (features (quote (("std" "num/std") ("default" "std") ("bench" "std"))))))

(define-public crate-fibs-0.2 (crate (name "fibs") (vers "0.2.8") (deps (list (crate-dep (name "num") (req "^0") (kind 0)) (crate-dep (name "paste") (req "^1") (default-features #t) (kind 2)))) (hash "07rf9h4adjwngfhs2f0j6m77gnm7mkxwpmr686p6sl4zsap0vmi0") (features (quote (("std" "num/std") ("default" "std") ("bench" "std"))))))

