(define-module (crates-io fi d-) #:use-module (crates-io))

(define-public crate-fid-rs-0.1 (crate (name "fid-rs") (vers "0.1.0") (deps (list (crate-dep (name "criterion") (req "^0.2") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.6") (default-features #t) (kind 2)) (crate-dep (name "rayon") (req "^1.0") (default-features #t) (kind 0)))) (hash "035aif0lw28pn4iy273il6qxaxbjz74c7kylaapxm79zzz128b95")))

(define-public crate-fid-rs-0.1 (crate (name "fid-rs") (vers "0.1.1") (deps (list (crate-dep (name "criterion") (req "^0.2") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.6") (default-features #t) (kind 2)) (crate-dep (name "rayon") (req "^1.0") (default-features #t) (kind 0)))) (hash "1kg25bxm6xbh4mgkl0bxa03l4q9d1lx87pmd0mbk081l1j66aa3c")))

(define-public crate-fid-rs-0.2 (crate (name "fid-rs") (vers "0.2.0") (deps (list (crate-dep (name "criterion") (req "^0.5") (default-features #t) (kind 2)) (crate-dep (name "mem_dbg") (req "^0.1.4") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8") (default-features #t) (kind 2)) (crate-dep (name "rayon") (req "^1.5") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)))) (hash "0iw4wh2w24n9j00f3lw3hn7m3bixl129s5ml8hml3lg2c0g6m5d6") (features (quote (("default" "rayon"))))))

