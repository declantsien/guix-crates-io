(define-module (crates-io fi gg) #:use-module (crates-io))

(define-public crate-figgy-0.1 (crate (name "figgy") (vers "0.1.0") (deps (list (crate-dep (name "serde") (req "^1.0.163") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.96") (default-features #t) (kind 0)))) (hash "14z141mfi0jiwsgm7avcad0fgvkz16qmvxlyn9nzhs0spmcr9zx4")))

(define-public crate-figgy-0.1 (crate (name "figgy") (vers "0.1.1") (deps (list (crate-dep (name "serde") (req "^1.0.163") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.96") (default-features #t) (kind 0)))) (hash "1jhjn7iyqnqn5r4hffvphmwpp0dlf3h7fb52gax5fm9rwdislffx")))

(define-public crate-figgy-0.1 (crate (name "figgy") (vers "0.1.2") (deps (list (crate-dep (name "serde") (req "^1.0.163") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.96") (default-features #t) (kind 0)))) (hash "1bndvygrl6m622qkxrz98q69p7k52hif58kwwzcirap03yfd9gz2")))

