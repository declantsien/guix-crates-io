(define-module (crates-io fi aa) #:use-module (crates-io))

(define-public crate-fiaas-logger-0.1 (crate (name "fiaas-logger") (vers "0.1.0") (deps (list (crate-dep (name "humantime") (req "^2.0") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4") (features (quote ("std"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)))) (hash "1ilh55irjc4ybvjy802rdggyaf7d2jb87zgjh6imh22gv59kz2s3") (yanked #t)))

(define-public crate-fiaas-logger-0.1 (crate (name "fiaas-logger") (vers "0.1.1") (deps (list (crate-dep (name "humantime") (req "^2.0") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4") (features (quote ("std"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)))) (hash "0j73f6r61nwdx9qwrjcysnyy0xw1a70b32kmhbrbgqrcisvsjdl5")))

