(define-module (crates-io fi ll) #:use-module (crates-io))

(define-public crate-fill-0.1 (crate (name "fill") (vers "0.1.0") (hash "1aq82lxn6shcapnm2j3l2sagrj4frdp2r5q40fxmfm9fwq2r4gl5") (features (quote (("default") ("alloc"))))))

(define-public crate-fill-0.1 (crate (name "fill") (vers "0.1.1") (hash "1jgsngwv1xgm4yq8kdxrrrdka6sg31zjwknl2xqbj15i5ydkiz0w") (features (quote (("default") ("alloc"))))))

(define-public crate-fill-array-0.1 (crate (name "fill-array") (vers "0.1.0") (deps (list (crate-dep (name "macrotest") (req "~1.0") (default-features #t) (kind 2)) (crate-dep (name "proc-macro2") (req "~1") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "~1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "~2.0") (default-features #t) (kind 0)))) (hash "0f7wsq37xwgxiwalh5pimcy8dzcnx0xswa16wsfp0rfccypc2d92")))

(define-public crate-fill-array-0.2 (crate (name "fill-array") (vers "0.2.0") (deps (list (crate-dep (name "macrotest") (req "~1.0") (default-features #t) (kind 2)) (crate-dep (name "proc-macro2") (req "~1") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "~1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "~2.0") (default-features #t) (kind 0)))) (hash "036iqyyrbpsc4qkzkk2qms31fqj0alpswg3ckz2xyhbbqh3pz6xm")))

(define-public crate-fill-array-0.2 (crate (name "fill-array") (vers "0.2.1") (deps (list (crate-dep (name "macrotest") (req "~1.0") (default-features #t) (kind 2)) (crate-dep (name "proc-macro2") (req "~1") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "~1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "~2.0") (default-features #t) (kind 0)))) (hash "1g7fp3qvlrls3nawkx2l99j75zdqj5gz7yjmmwi77iwk6iiwynqq")))

(define-public crate-fill_canvas-0.1 (crate (name "fill_canvas") (vers "0.1.0") (hash "0z4cyhq065p82agp2rwci2cyrl0p6l544hbm5jx479mpqk9ycq7z")))

(define-public crate-fill_canvas-0.1 (crate (name "fill_canvas") (vers "0.1.1") (hash "1dkyzr6mmlz71v6zvk6fdf6yyrwbfd37rdbi4dh8dpn66p3dr8sz")))

(define-public crate-fill_iter-0.1 (crate (name "fill_iter") (vers "0.1.0") (hash "1cgpjgpzpz09jiqjnjnqnyrw3sfafzxlrmx9mb4yckm71c4j5px4")))

(define-public crate-filler-0.0.0 (crate (name "filler") (vers "0.0.0") (hash "0cmz7s7k0gs89226w7c73qcbv5znpqqkvfmqv2lw3hd4lc4r2jqg") (yanked #t)))

(define-public crate-fillout-0.1 (crate (name "fillout") (vers "0.1.0") (deps (list (crate-dep (name "anyhow") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "csv") (req "^1.1") (default-features #t) (kind 0)) (crate-dep (name "exitcode") (req "^1.1.2") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.3") (kind 0)) (crate-dep (name "terminal_size") (req "^0.1.16") (default-features #t) (kind 0)))) (hash "1nbb61h8kpavn7a3c97lj51s78nb7g0mkvlki8mdx13kfwpqk6cs")))

(define-public crate-fillout-0.1 (crate (name "fillout") (vers "0.1.1") (deps (list (crate-dep (name "anyhow") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "csv") (req "^1.1") (default-features #t) (kind 0)) (crate-dep (name "exitcode") (req "^1.1.2") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.3") (kind 0)) (crate-dep (name "terminal_size") (req "^0.1.16") (default-features #t) (kind 0)))) (hash "1s4g25bfzd5kxky8ggvy306ycv9nd67lw6nhn49icbyj14v30nz1")))

