(define-module (crates-io fi rs) #:use-module (crates-io))

(define-public crate-first-create-1 (crate (name "first-create") (vers "1.0.0") (hash "1icspan1ivllnlqkd7gmp7kj8qws90z9wywjdr7iphmlkynhykp1")))

(define-public crate-first-err-0.1 (crate (name "first-err") (vers "0.1.0") (deps (list (crate-dep (name "criterion") (req "^0.5.1") (default-features #t) (kind 2)))) (hash "1ai6brfmvn2p1l23admcrsb5k9kqarchdsfr2hljhwfq8h64c6z5")))

(define-public crate-first-err-0.1 (crate (name "first-err") (vers "0.1.1") (deps (list (crate-dep (name "criterion") (req "^0.5.1") (default-features #t) (kind 2)))) (hash "0isfl01m8hjqpzwf0mj8bm9w4f1r729pnigxyk11b7injbqy97mh")))

(define-public crate-first-err-0.2 (crate (name "first-err") (vers "0.2.0") (deps (list (crate-dep (name "criterion") (req "^0.5.1") (default-features #t) (kind 2)))) (hash "1jawdycakmwhpl296nxdmwdsxi20ql4xkhpsbk0jkha6fg1hbk9f")))

(define-public crate-first-err-0.2 (crate (name "first-err") (vers "0.2.1") (deps (list (crate-dep (name "criterion") (req "^0.5.1") (default-features #t) (kind 2)))) (hash "1y4xlw02qnkpq4wwdjmh10zlyrhp1p1drq2qcva9dqd27d2b49pj")))

(define-public crate-first-err-0.2 (crate (name "first-err") (vers "0.2.2") (deps (list (crate-dep (name "criterion") (req "^0.5.1") (default-features #t) (kind 2)))) (hash "1n6w751f7kvn3k01r9s2dk0r2n519y4hlbssszxza2fsrn9s1cqp")))

(define-public crate-first-function-0.1 (crate (name "first-function") (vers "0.1.0") (hash "0gba3lz3a1bhcdsg56zwiap49vyb7wm30k55gkzlnmdy91z6p8r5")))

(define-public crate-first-lib-fakkar-0.1 (crate (name "first-lib-fakkar") (vers "0.1.0") (hash "0j6x5jg21d67c9g3f8axxqi7c2pmnlbr1rnclsniddqllbhrmrhb")))

(define-public crate-first-ok-0.1 (crate (name "first-ok") (vers "0.1.0") (deps (list (crate-dep (name "anyhow") (req "^1.0.71") (default-features #t) (kind 2)) (crate-dep (name "async-channel") (req "^1.8.0") (default-features #t) (kind 0)) (crate-dep (name "futures") (req "^0.3.28") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 2)) (crate-dep (name "reqwest") (req "^0.11.17") (features (quote ("trust-dns"))) (default-features #t) (kind 2)) (crate-dep (name "tokio") (req "^1.28.0") (features (quote ("rt-multi-thread" "macros" "sync"))) (default-features #t) (kind 0)))) (hash "081i4nnwby816ml6ziff5bbm2d4b1f00c1iwfinfpphsacdq81pg")))

(define-public crate-first-ok-0.1 (crate (name "first-ok") (vers "0.1.1") (deps (list (crate-dep (name "anyhow") (req "^1.0.71") (default-features #t) (kind 2)) (crate-dep (name "async-channel") (req "^1.8.0") (default-features #t) (kind 0)) (crate-dep (name "futures") (req "^0.3.28") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 2)) (crate-dep (name "reqwest") (req "^0.11.17") (features (quote ("trust-dns"))) (default-features #t) (kind 2)) (crate-dep (name "tokio") (req "^1.28.0") (features (quote ("rt-multi-thread" "macros" "sync"))) (default-features #t) (kind 0)))) (hash "1fa0znwvcpc019diajqz6g8dhg31mm4pjlj1c9rnm0ij0nijxx4q")))

(define-public crate-first-ok-0.1 (crate (name "first-ok") (vers "0.1.2") (deps (list (crate-dep (name "anyhow") (req "^1.0.71") (default-features #t) (kind 2)) (crate-dep (name "async-channel") (req "^1.8.0") (default-features #t) (kind 0)) (crate-dep (name "futures") (req "^0.3.28") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 2)) (crate-dep (name "reqwest") (req "^0.11.18") (features (quote ("trust-dns" "rustls-tls"))) (default-features #t) (kind 2)) (crate-dep (name "tokio") (req "^1.28.2") (features (quote ("rt-multi-thread" "macros" "sync"))) (default-features #t) (kind 0)))) (hash "0s6vh1gdvwb0xx587kd6r7n226akxyxlmiwvjjfb4409ijv1wp4s")))

(define-public crate-first-unique-0.1 (crate (name "first-unique") (vers "0.1.0") (deps (list (crate-dep (name "env_logger") (req "^0.6") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.7.0") (default-features #t) (kind 0)))) (hash "0d4p0gysjpq263c6db0s1zmrz1i8k8m49hd4pc0p5pp13cnr7w7s")))

(define-public crate-first_cargo_package-0.1 (crate (name "first_cargo_package") (vers "0.1.0") (hash "00a17jix2hiccn9hz3l7s7rl9mng6blrw6w6k75fd72hcjx8mmj1")))

(define-public crate-first_cargo_package-0.1 (crate (name "first_cargo_package") (vers "0.1.1") (hash "0glyjz1varaswywzn8ys1km0y9hbm0aph8mp8gwwd4fqczp1d9g6")))

(define-public crate-first_cli_tool-0.1 (crate (name "first_cli_tool") (vers "0.1.0") (deps (list (crate-dep (name "anyhow") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^4.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "src") (req "^0.0.6") (default-features #t) (kind 0)))) (hash "02fsj0i7bdivvyixc5n58hl0j28k90hzhdzzzcrjd4yq7gsk0v9q")))

(define-public crate-first_cli_tool-0.1 (crate (name "first_cli_tool") (vers "0.1.1") (deps (list (crate-dep (name "anyhow") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^4.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "src") (req "^0.0.6") (default-features #t) (kind 0)))) (hash "1dkxgqq2d07ybcs6b0wf8p54wfnqszszjsdpladwl7zn4qbrpn88")))

(define-public crate-first_crate-0.1 (crate (name "first_crate") (vers "0.1.0") (hash "1dl869vf9dqad1h7bg7wnh34wbfldi7pjm5a996czcy73q7khc5r") (yanked #t)))

(define-public crate-first_crate-0.1 (crate (name "first_crate") (vers "0.1.1") (hash "0njsbzb9yvrwwzwpx0vkvwyvwg4sgvxa5myy5q96bjn1f6q6b46k") (yanked #t)))

(define-public crate-first_crate-0.1 (crate (name "first_crate") (vers "0.1.2") (hash "101nliagv993m8k4madxb40v1v0rfhwj1jxmmn9srmlpg8qm8ig9") (yanked #t)))

(define-public crate-first_crate-0.1 (crate (name "first_crate") (vers "0.1.3") (hash "1rbbp3kwspc1frwqkk4gmya1vqbqi9qzlmrrnlms31z0rc34wd4n") (yanked #t)))

(define-public crate-first_crate-0.1 (crate (name "first_crate") (vers "0.1.4") (hash "18862ks31zf7nqld2l4jjpb2bn187jg6d76jdhnkhvgl0i039x3f") (yanked #t)))

(define-public crate-first_crate-0.1 (crate (name "first_crate") (vers "0.1.5") (hash "13fvnqv3n7gx08530s8kldgjpgygqv8y5z8m6arw27abha79b47x") (yanked #t)))

(define-public crate-first_crate-0.1 (crate (name "first_crate") (vers "0.1.6") (hash "16gnpjz8g267l1cnrf021jzvhyjh03i2k3pqyisc19qq9scnhm6n") (yanked #t)))

(define-public crate-first_crate-0.1 (crate (name "first_crate") (vers "0.1.7") (hash "1555albrwrv5y4l0rwqxw1i94k406lyv0dyalh7d9zjahks3y2sb") (yanked #t)))

(define-public crate-first_crate-0.1 (crate (name "first_crate") (vers "0.1.8") (hash "1blrnqs8g2836jh0c5agaccppip36c40wfndh3b96l1jhrzbnz9c") (yanked #t)))

(define-public crate-first_crate-0.1 (crate (name "first_crate") (vers "0.1.9") (hash "1bhfk7d6yk1k7lh32lx1k44c6p0sdib5nd7p4l71rmp6zs1c51vv")))

(define-public crate-first_crate-0.2 (crate (name "first_crate") (vers "0.2.0") (hash "1ykpvqyak6qn105jyki8ra4mpph5jq6cpyblcn2d30b6qjp6yfpa")))

(define-public crate-first_crate_test_12345-0.1 (crate (name "first_crate_test_12345") (vers "0.1.0") (hash "0m0r98js7hr039k6n4sgilr9h2aymlscn6m866ahrlhs0lgp2651")))

(define-public crate-first_crate_test_12345-0.1 (crate (name "first_crate_test_12345") (vers "0.1.2") (hash "1c0shkyv7w6qh6arg9q5dws6pjadr0sbz7gaaxfcgjlwkc1pdglm")))

(define-public crate-first_crate_yinuo-0.1 (crate (name "first_crate_yinuo") (vers "0.1.0") (hash "08gzana1iqjkbpncfp27imlmm190yz4hz4zfj9h6lwjfwyw46fwz")))

(define-public crate-first_crate_yinuo-0.2 (crate (name "first_crate_yinuo") (vers "0.2.0") (hash "1zn9r4nddwmhkyr276j3zsi5craj0rh60v9qx2vr0dp19hrkm56b")))

(define-public crate-first_crates-0.1 (crate (name "first_crates") (vers "0.1.0") (hash "1pisakrysilc904qki0pnz8sarkxpiyl3x32grbzlybiriwls53j")))

(define-public crate-first_lib-0.1 (crate (name "first_lib") (vers "0.1.7") (hash "0v9cv5vyh2igx24p16d5xgpzzmz8bkjk65ijx9hr1h0almkf6bdf")))

(define-public crate-first_lib-0.1 (crate (name "first_lib") (vers "0.1.10") (hash "063z8lc8vmcpmf3jak8wrkhll9q8xd344a4dg3k9j8psviw3zhjh")))

(define-public crate-first_lib_yulian-0.1 (crate (name "first_lib_yulian") (vers "0.1.0") (hash "17jz1rnd06bvksfn3cqldpbvs1hdjr9qw5ys6ib9k3fbwgrnd59a")))

(define-public crate-first_order_logic-0.1 (crate (name "first_order_logic") (vers "0.1.0") (deps (list (crate-dep (name "bimap") (req "^0.6.2") (default-features #t) (kind 0)) (crate-dep (name "enum_dispatch") (req "^0.3.11") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)))) (hash "03dxfgmr6jmdx8sgrlnplqakqrjraiibn5inl7a7mcy4bc3hyr3f") (features (quote (("syntax") ("semantics") ("default" "semantics" "syntax"))))))

(define-public crate-first_package-0.1 (crate (name "first_package") (vers "0.1.0") (hash "1mc54wb1z1r4dv0dr8c3h3wradh5gwasckrcmwnk9kabkkr57gja")))

(define-public crate-first_package_by_hannan-0.1 (crate (name "first_package_by_hannan") (vers "0.1.0") (hash "164qssmccmz0qsmq07d6gm9qfrihj4zl9mlp97xd716asrnbgzxc") (yanked #t)))

(define-public crate-first_rs_001-0.1 (crate (name "first_rs_001") (vers "0.1.0") (hash "1bd0ls0nbzijjd912vy5lmviy6ng84axcrr9ivg28lrbsy559ypq")))

(define-public crate-first_rs_001-0.1 (crate (name "first_rs_001") (vers "0.1.1") (hash "1df19gy0axzsz698kgnfzx2kcaim36ym1jn1762viz2w1v8x30s8") (yanked #t)))

(define-public crate-first_test-0.1 (crate (name "first_test") (vers "0.1.0") (hash "17rp4svzz17wn8r4i6jnrx9b5cas44r2ghfd7pzdkxkdgma90vmb")))

(define-public crate-first_test_minigrep-0.1 (crate (name "first_test_minigrep") (vers "0.1.0") (hash "0d47ic4q7g5jjp8nvjh7bi95ql3cq0dvzfjcc0jjwcgxnx39xfid")))

(define-public crate-firstwelcome-0.1 (crate (name "firstwelcome") (vers "0.1.0") (hash "0dh10i5gzp54rq63n9c5x57qihpfy6lfqahgx4vpkn98qy0kjlhk")))

(define-public crate-firstwelcome-0.1 (crate (name "firstwelcome") (vers "0.1.1") (hash "1i3h0sbxxn6frsa2bfmqc19fpjans8656iq2ffvfzzn3w369cckh")))

