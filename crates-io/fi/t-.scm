(define-module (crates-io fi t-) #:use-module (crates-io))

(define-public crate-fit-rust-0.1 (crate (name "fit-rust") (vers "0.1.0") (deps (list (crate-dep (name "binrw") (req "^0.13.0") (default-features #t) (kind 0)) (crate-dep (name "copyless") (req "^0.1.5") (default-features #t) (kind 0)) (crate-dep (name "paste") (req "^1.0") (default-features #t) (kind 0)))) (hash "0zm435mk5ivlqc5fagxq9c8qk4ml23m1jv7z8d6ddwz2p58ybbyn")))

(define-public crate-fit-rust-0.1 (crate (name "fit-rust") (vers "0.1.1") (deps (list (crate-dep (name "binrw") (req "^0.13.0") (default-features #t) (kind 0)) (crate-dep (name "copyless") (req "^0.1.5") (default-features #t) (kind 0)) (crate-dep (name "paste") (req "^1.0") (default-features #t) (kind 0)))) (hash "19kfkbdigq6w852cszly24v2q5k4rxsc4q65wqf4axysi9pv3l6q")))

(define-public crate-fit-rust-0.1 (crate (name "fit-rust") (vers "0.1.2") (deps (list (crate-dep (name "binrw") (req "^0.13.0") (default-features #t) (kind 0)) (crate-dep (name "copyless") (req "^0.1.5") (default-features #t) (kind 0)) (crate-dep (name "paste") (req "^1.0") (default-features #t) (kind 0)))) (hash "1i6bchw6qli3svdbjhdsmrwapg6638r3kp5gcgia5wqqqfmb5bd1")))

(define-public crate-fit-rust-0.1 (crate (name "fit-rust") (vers "0.1.3") (deps (list (crate-dep (name "binrw") (req "^0.13.0") (default-features #t) (kind 0)) (crate-dep (name "copyless") (req "^0.1.5") (default-features #t) (kind 0)) (crate-dep (name "paste") (req "^1.0") (default-features #t) (kind 0)))) (hash "04x55kksmaqkcx39dcypja0afr211f77956fplzbax3d3gh2p88k")))

(define-public crate-fit-rust-0.1 (crate (name "fit-rust") (vers "0.1.4") (deps (list (crate-dep (name "binrw") (req "^0.13.0") (default-features #t) (kind 0)) (crate-dep (name "copyless") (req "^0.1.5") (default-features #t) (kind 0)) (crate-dep (name "paste") (req "^1.0") (default-features #t) (kind 0)))) (hash "0jv6mh37ssqjgcbr129hc8w1hn0wvps7hw11364b046dl56k55c9")))

(define-public crate-fit-rust-0.1 (crate (name "fit-rust") (vers "0.1.5") (deps (list (crate-dep (name "binrw") (req "^0.13.0") (default-features #t) (kind 0)) (crate-dep (name "copyless") (req "^0.1.5") (default-features #t) (kind 0)) (crate-dep (name "paste") (req "^1.0") (default-features #t) (kind 0)))) (hash "1gh3ql42nhl4m6af6fh47w1244kmk1nsqppwnkf6m8pn8dgqi0ch")))

(define-public crate-fit-rust-0.1 (crate (name "fit-rust") (vers "0.1.6") (deps (list (crate-dep (name "binrw") (req "^0.13.0") (default-features #t) (kind 0)) (crate-dep (name "copyless") (req "^0.1.5") (default-features #t) (kind 0)) (crate-dep (name "paste") (req "^1.0") (default-features #t) (kind 0)))) (hash "0534w5vqwigxzf2xsf5vxmcvjzz1xws51lr54mr12wmq0cc8x3am")))

(define-public crate-fit-rust-0.1 (crate (name "fit-rust") (vers "0.1.7") (deps (list (crate-dep (name "binrw") (req "^0.13.0") (default-features #t) (kind 0)) (crate-dep (name "copyless") (req "^0.1.5") (default-features #t) (kind 0)) (crate-dep (name "paste") (req "^1.0") (default-features #t) (kind 0)))) (hash "1g9lfs5ixgi3f6f0fn7j6v47qc19sk8fgibpryh6n1dcqddpj23m")))

(define-public crate-fit-rust-0.1 (crate (name "fit-rust") (vers "0.1.8") (deps (list (crate-dep (name "binrw") (req "^0.13.0") (default-features #t) (kind 0)) (crate-dep (name "copyless") (req "^0.1.5") (default-features #t) (kind 0)) (crate-dep (name "paste") (req "^1.0") (default-features #t) (kind 0)))) (hash "022b454ipiv44xgigk7ksqkdjz0ljp75p0yzh7xxfd0k538vshn5")))

(define-public crate-fit-sdk-0.1 (crate (name "fit-sdk") (vers "0.1.0") (deps (list (crate-dep (name "csv") (req "^1.0") (default-features #t) (kind 1)) (crate-dep (name "heck") (req "^0.3") (default-features #t) (kind 1)))) (hash "19bvrwqk5hl0a2d30ffhvdr8q2z4jvrjrhpczg7mkkifz7knx77j")))

