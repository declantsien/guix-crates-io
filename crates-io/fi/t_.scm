(define-module (crates-io fi t_) #:use-module (crates-io))

(define-public crate-fit_file-0.1 (crate (name "fit_file") (vers "0.1.0") (hash "08axr5cqycvx25hg2j87awrvkkxcz6pifxrj40y2hxmz7gfaz28z")))

(define-public crate-fit_file-0.1 (crate (name "fit_file") (vers "0.1.1") (hash "1a08h1ij8a0rr4hyvrl9wpkvf8292w7zq9rqnrm9qmx5r3w976gp")))

(define-public crate-fit_file-0.1 (crate (name "fit_file") (vers "0.1.2") (hash "1fzcbkvwmj3c066rf213bwk6m0cwd8zj62341qp0n8rdb5j95dw5")))

(define-public crate-fit_file-0.1 (crate (name "fit_file") (vers "0.1.3") (hash "0m7zhnvqq59k7vrrvpnw337x92v7l99qjkhcgpyny7ymdymcjl3v")))

(define-public crate-fit_file-0.1 (crate (name "fit_file") (vers "0.1.4") (hash "07yiilp4v2m0zf3qm5digs1bssmirldphjrqzhzw2kvgjn6rcp48")))

(define-public crate-fit_file-0.2 (crate (name "fit_file") (vers "0.2.0") (deps (list (crate-dep (name "csv") (req "^1") (default-features #t) (kind 0)))) (hash "03j3rgzmdrvgvdgr3csjiqv1i852fiz38mwjl1ml7ms2px48l0rh")))

(define-public crate-fit_file-0.2 (crate (name "fit_file") (vers "0.2.1") (deps (list (crate-dep (name "csv") (req "^1") (default-features #t) (kind 0)))) (hash "07d847cs6iwj46bbay8bng973ry24ngm3kag4w43zh9ls4y356bl")))

(define-public crate-fit_file-0.3 (crate (name "fit_file") (vers "0.3.0") (deps (list (crate-dep (name "csv") (req "^1") (default-features #t) (kind 0)))) (hash "1vng8w8x8f7qssq0clhyb8ygvp17ma8kg51ji1dc8vhkypngfkvk")))

(define-public crate-fit_file-0.3 (crate (name "fit_file") (vers "0.3.1") (deps (list (crate-dep (name "csv") (req "^1") (default-features #t) (kind 0)))) (hash "015rplr4q4m2svmqp2hcihyja9dfivvfzpidyd7n1yzqgvilks4s")))

(define-public crate-fit_file-0.3 (crate (name "fit_file") (vers "0.3.2") (deps (list (crate-dep (name "csv") (req "^1") (default-features #t) (kind 0)))) (hash "16pzzvjch2fv7l1a6vr6haryhlqlfp65aq4adi75zy93km6ybcqy")))

(define-public crate-fit_file-0.3 (crate (name "fit_file") (vers "0.3.3") (deps (list (crate-dep (name "csv") (req "^1") (default-features #t) (kind 0)))) (hash "03mzg13syca2x3gjiplrn0zczrkrkw6kv32dzdl6a91nblaixdr1")))

(define-public crate-fit_file-0.3 (crate (name "fit_file") (vers "0.3.4") (deps (list (crate-dep (name "csv") (req "^1") (default-features #t) (kind 0)))) (hash "1d9y7i4l7ifwik3b0ylg2c7dbyjs0qhfm4dw9fwgw6h2knnc13sb")))

(define-public crate-fit_file-0.3 (crate (name "fit_file") (vers "0.3.5") (deps (list (crate-dep (name "csv") (req "^1") (default-features #t) (kind 0)))) (hash "0brx0lv39x0a7axmxskfdsryyy2cp7fx5y45vpk1sd9zl09fp5r2")))

(define-public crate-fit_file-0.4 (crate (name "fit_file") (vers "0.4.0") (deps (list (crate-dep (name "csv") (req "^1") (default-features #t) (kind 0)))) (hash "1x22j7wlbzgxh5s2r5gf084lajk1hc3s8kgcdhmjc5iv8v9c7hq5")))

(define-public crate-fit_file-0.5 (crate (name "fit_file") (vers "0.5.0") (deps (list (crate-dep (name "csv") (req "^1") (default-features #t) (kind 0)))) (hash "0gblvs4m5ffdi9af42nd594sqm3sjn4l9zr48z467nivp8jlbi7z")))

(define-public crate-fit_file-0.6 (crate (name "fit_file") (vers "0.6.0") (deps (list (crate-dep (name "csv") (req "^1") (default-features #t) (kind 0)))) (hash "0jp3mddznx2vraqyx8bkvypkw9yq09s06pn11wcp2mh53hlr5hrn")))

(define-public crate-fit_no_io-0.1 (crate (name "fit_no_io") (vers "0.1.0") (deps (list (crate-dep (name "copyless") (req "^0.1.4") (default-features #t) (kind 0)) (crate-dep (name "criterion") (req "^0.2.11") (default-features #t) (kind 2)) (crate-dep (name "csv") (req "^1.0") (default-features #t) (kind 1)) (crate-dep (name "heck") (req "^0.3") (default-features #t) (kind 1)) (crate-dep (name "mmap") (req "^0.1.0") (default-features #t) (kind 2)))) (hash "1m3s47j012scvar27y2c4j2g7rqzylyyxlf5j7k2ziw6m3cy25fj")))

(define-public crate-fit_text-0.1 (crate (name "fit_text") (vers "0.1.0") (deps (list (crate-dep (name "graphics_buffer") (req "^0.7.2") (default-features #t) (kind 2)) (crate-dep (name "once_cell") (req "^1.1.0") (default-features #t) (kind 0)) (crate-dep (name "piston2d-graphics") (req "^0.35.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "rusttype") (req "^0.8.0") (default-features #t) (kind 0)) (crate-dep (name "vector2math") (req "^0.6.0") (default-features #t) (kind 0)))) (hash "124qyh0c8cq6dca2bsj87vb4x9p0q87sa9nlay5fhxz00xibshkl") (features (quote (("graphics" "piston2d-graphics") ("default" "graphics"))))))

(define-public crate-fit_text-0.2 (crate (name "fit_text") (vers "0.2.0") (deps (list (crate-dep (name "graphics_buffer") (req "^0.7.2") (default-features #t) (kind 2)) (crate-dep (name "once_cell") (req "^1.3.1") (default-features #t) (kind 0)) (crate-dep (name "piston2d-graphics") (req "^0.36.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "rusttype") (req "^0.8.2") (default-features #t) (kind 0)) (crate-dep (name "vector2math") (req "^0.6.0") (default-features #t) (kind 0)))) (hash "1plnls40g9ajj2y7lxkagrpm6d9c7yrx2n6wlpjsw0vq5fx1m7jq") (features (quote (("graphics" "piston2d-graphics") ("default" "graphics"))))))

(define-public crate-fit_text-0.2 (crate (name "fit_text") (vers "0.2.1") (deps (list (crate-dep (name "graphics_buffer") (req "^0.7.3") (default-features #t) (kind 2)) (crate-dep (name "once_cell") (req "^1.3.1") (default-features #t) (kind 0)) (crate-dep (name "piston2d-graphics") (req "^0.36.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "rusttype") (req "^0.8.2") (default-features #t) (kind 0)) (crate-dep (name "vector2math") (req "^0.6.0") (default-features #t) (kind 0)))) (hash "1dfarw4ang8kz0gza4r311nkcmjwk49pff4vawk2wii3qc5ljw54") (features (quote (("graphics" "piston2d-graphics") ("default" "graphics"))))))

(define-public crate-fit_text-0.3 (crate (name "fit_text") (vers "0.3.0") (deps (list (crate-dep (name "graphics_buffer") (req "^0.7.4") (default-features #t) (kind 2)) (crate-dep (name "once_cell") (req "^1.4.1") (default-features #t) (kind 0)) (crate-dep (name "piston2d-graphics") (req "^0.36.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "rusttype") (req "^0.9.2") (default-features #t) (kind 0)) (crate-dep (name "vector2math") (req "^0.8.0") (default-features #t) (kind 0)))) (hash "0w64p1v703693chc5biy26dmn1zksvnyn97ph6y85zw5q0k2q56g") (features (quote (("graphics" "piston2d-graphics") ("default" "graphics"))))))

(define-public crate-fit_text-0.3 (crate (name "fit_text") (vers "0.3.1") (deps (list (crate-dep (name "graphics_buffer") (req "^0.7.7") (default-features #t) (kind 2)) (crate-dep (name "once_cell") (req "^1.4.1") (default-features #t) (kind 0)) (crate-dep (name "piston2d-graphics") (req "^0.40.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "rusttype") (req "^0.9.2") (default-features #t) (kind 0)) (crate-dep (name "vector2math") (req "^0.8.0") (default-features #t) (kind 0)))) (hash "0qn8lvsvdjnfzk6a9xr7x8z7zjfmm907w0z6c54vj5717g19wpw8") (features (quote (("graphics" "piston2d-graphics") ("default" "graphics"))))))

(define-public crate-fit_text-0.3 (crate (name "fit_text") (vers "0.3.2") (deps (list (crate-dep (name "graphics_buffer") (req "^0.7.7") (default-features #t) (kind 2)) (crate-dep (name "once_cell") (req "^1.4.1") (default-features #t) (kind 0)) (crate-dep (name "piston2d-graphics") (req "^0.40.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "rusttype") (req "^0.9.2") (default-features #t) (kind 0)) (crate-dep (name "vector2math") (req "^0.13.0") (default-features #t) (kind 0)))) (hash "1dvzjy8343pm44w0jyp0qb5jxcrczas49w2vbdlrlrh6gv1v3dxc") (features (quote (("graphics" "piston2d-graphics") ("default" "graphics"))))))

(define-public crate-fit_text-0.3 (crate (name "fit_text") (vers "0.3.3") (deps (list (crate-dep (name "graphics_buffer") (req "^0.7.7") (default-features #t) (kind 2)) (crate-dep (name "once_cell") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "piston2d-graphics") (req "^0.41.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "rusttype") (req "^0.9.2") (default-features #t) (kind 0)) (crate-dep (name "vector2math") (req "^0.13.0") (default-features #t) (kind 0)))) (hash "1gh4mp5n1fqs4mprj5jsn79wgx6xwc1w6v1qrqbf590k4fn16gi4") (features (quote (("graphics" "piston2d-graphics") ("default" "graphics"))))))

