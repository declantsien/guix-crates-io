(define-module (crates-io fi e-) #:use-module (crates-io))

(define-public crate-fie-ffi-0.1 (crate (name "fie-ffi") (vers "0.1.0") (deps (list (crate-dep (name "bincode") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "cbindgen") (req "^0.8") (optional #t) (default-features #t) (kind 1)) (crate-dep (name "fie") (req "^0.12") (default-features #t) (kind 0)))) (hash "1gvgpsiwx2nqnywgnzwpcad3y4dl48jr3cr239s2kvf8yq17xh62") (features (quote (("header" "cbindgen") ("default" "c") ("c"))))))

(define-public crate-fie-ffi-0.2 (crate (name "fie-ffi") (vers "0.2.0") (deps (list (crate-dep (name "bincode") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "cbindgen") (req "^0.8") (optional #t) (default-features #t) (kind 1)) (crate-dep (name "fie") (req "^0.13") (default-features #t) (kind 0)))) (hash "1avzw1c4h95mgclp1h6nd7vy3zhmq9y1yvzk7am8rs6wp49mn3f0") (features (quote (("header" "cbindgen") ("default" "c") ("c"))))))

(define-public crate-fie-ffi-0.3 (crate (name "fie-ffi") (vers "0.3.0") (deps (list (crate-dep (name "bincode") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "cbindgen") (req "^0.8") (optional #t) (default-features #t) (kind 1)) (crate-dep (name "fie") (req "^0.16") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^0.2") (features (quote ("rt-core" "net" "io-util"))) (default-features #t) (kind 0)) (crate-dep (name "tokio-global") (req "^0.4") (default-features #t) (kind 0)))) (hash "00mqc4gc03xjx6i4rjlqybqjqk5aff7gw3f033fv7wdp99wj93gs") (features (quote (("header" "cbindgen") ("default" "c") ("c"))))))

