(define-module (crates-io fi em) #:use-module (crates-io))

(define-public crate-fiemap-0.1 (crate (name "fiemap") (vers "0.1.0") (deps (list (crate-dep (name "bitflags") (req "^1") (default-features #t) (kind 0)))) (hash "036gd3gjnbinv7ag6nj89p209hbpq095v8ymxwmnydwc9vrrd19p")))

(define-public crate-fiemap-0.1 (crate (name "fiemap") (vers "0.1.1") (deps (list (crate-dep (name "bitflags") (req "^1") (default-features #t) (kind 0)))) (hash "1l95pyzx8hd4w0xynp20b7bb7536b9a3d14gw8rmc5jr59134ih8")))

(define-public crate-fiemap-0.1 (crate (name "fiemap") (vers "0.1.2") (deps (list (crate-dep (name "bitflags") (req "^2") (default-features #t) (kind 0)))) (hash "08ki52zzrcnccsi100c5wnr156h10pz7cykq11xnb0zk39xmbjl9")))

