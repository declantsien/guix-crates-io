(define-module (crates-io fi tz) #:use-module (crates-io))

(define-public crate-fitz-0.0.1 (crate (name "fitz") (vers "0.0.1") (hash "0inm6npacfhw0d1gibjm2c2q6p54llaxi8pjipadyami2k846hwm") (yanked #t)))

(define-public crate-fitz_example-0.1 (crate (name "fitz_example") (vers "0.1.0") (hash "1n34199jhp5i2n1yz0facb2m5azd41s1y8k7rdx7fllgmvnpfy0v") (yanked #t)))

(define-public crate-fitz_example-0.2 (crate (name "fitz_example") (vers "0.2.0") (hash "13wz968fsnb4sxy2vg804n1ygr9wnq53wnxqlf285mx4a9dbssxi") (yanked #t)))

