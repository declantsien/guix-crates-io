(define-module (crates-io fi lx) #:use-module (crates-io))

(define-public crate-filx-0.1 (crate (name "filx") (vers "0.1.0") (deps (list (crate-dep (name "chrono") (req "^0.4.19") (default-features #t) (kind 0)) (crate-dep (name "colored") (req "^2.0.0") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.3") (default-features #t) (kind 0)))) (hash "1lzdnmaqa8hymxr6hjjqb141r5gf60mmj1lm6zf74ji3wmx1hd6r")))

(define-public crate-filx-0.1 (crate (name "filx") (vers "0.1.1") (deps (list (crate-dep (name "chrono") (req "^0.4.19") (default-features #t) (kind 0)) (crate-dep (name "colored") (req "^2.0.0") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.3") (default-features #t) (kind 0)))) (hash "1s994bfb63kdq8jvha26jn2w5j43zic2n8pd7xjfjdfn37mrnjg3")))

(define-public crate-filx-0.1 (crate (name "filx") (vers "0.1.2") (deps (list (crate-dep (name "chrono") (req "^0.4.19") (default-features #t) (kind 0)) (crate-dep (name "colored") (req "^2.0.0") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.3") (default-features #t) (kind 0)))) (hash "07kn1q79c42bx1g2h9nkgl30zn73c8qgas8rgflh0mhsgylv88mi")))

