(define-module (crates-io fi ro) #:use-module (crates-io))

(define-public crate-firo-parser-0.1 (crate (name "firo-parser") (vers "0.1.0") (deps (list (crate-dep (name "anyhow") (req "^1.0.79") (default-features #t) (kind 0)) (crate-dep (name "pest") (req "^2.7.6") (features (quote ("pretty-print"))) (default-features #t) (kind 0)) (crate-dep (name "pest_derive") (req "^2.7.6") (default-features #t) (kind 0)))) (hash "0lmng5m0i39mapmfw2ixclkg70zyhjkw0pnng4aprmxwsz1vbadx")))

(define-public crate-firo-serai-0.1 (crate (name "firo-serai") (vers "0.1.0") (hash "0pg4m12v3ns5a0svi599cjw7r4wifjmq6zws2hr4cxgx9x34r2a3")))

