(define-module (crates-io fi gy) #:use-module (crates-io))

(define-public crate-figyel_grrs_0001-0.1 (crate (name "figyel_grrs_0001") (vers "0.1.0") (deps (list (crate-dep (name "assert_cmd") (req "^0.10") (default-features #t) (kind 2)) (crate-dep (name "predicates") (req "^1") (default-features #t) (kind 2)) (crate-dep (name "structopt") (req "^0.3.13") (default-features #t) (kind 0)) (crate-dep (name "tempfile") (req "^3") (default-features #t) (kind 2)))) (hash "05wxbvv9z5g40xi3wq7arfc47gckvylpzy4x0sgimmgkfscbxyc5")))

