(define-module (crates-io fi o-) #:use-module (crates-io))

(define-public crate-fio-ioengine-0.0.0 (crate (name "fio-ioengine") (vers "0.0.0") (deps (list (crate-dep (name "fio-ioengine-sys") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "1fmnah37hl4qb55qgp6jj8cfjhfab6zh8sapw0gaym6wd8xcb3q1")))

(define-public crate-fio-ioengine-sys-0.1 (crate (name "fio-ioengine-sys") (vers "0.1.0") (deps (list (crate-dep (name "bindgen") (req "^0.58.1") (default-features #t) (kind 1)) (crate-dep (name "cty") (req "^0.2.1") (default-features #t) (kind 0)))) (hash "1g945vsvii7vdjs5mp2azqam84gvhmg14f3l624akp4qvy4kx47z") (features (quote (("vendor") ("default" "vendor"))))))

