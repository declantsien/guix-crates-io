(define-module (crates-io fi bn) #:use-module (crates-io))

(define-public crate-fibnacci-0.2 (crate (name "fibnacci") (vers "0.2.0") (deps (list (crate-dep (name "clap") (req "^3.2.20") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "rayon") (req "^1.5.3") (default-features #t) (kind 0)) (crate-dep (name "rust-gmp") (req "^0.5.0") (default-features #t) (kind 0)))) (hash "109s459mqx7c7cvgaq3j6z3sg9rv49yc2mxxf8qp4n8qhmxy2j7a")))

(define-public crate-fibnacci-0.3 (crate (name "fibnacci") (vers "0.3.0") (deps (list (crate-dep (name "clap") (req "^3.2.20") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "rayon") (req "^1.5.3") (default-features #t) (kind 0)) (crate-dep (name "rust-gmp") (req "^0.5.0") (default-features #t) (kind 0)))) (hash "0ysry3p7idqfqfyciwzrmr8hwxlqfyvw8pkwcrm1qw45iapnr45p")))

(define-public crate-fibnacci-0.3 (crate (name "fibnacci") (vers "0.3.1") (deps (list (crate-dep (name "cache-macro") (req "^0.4.1") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^3.2.20") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "lru-cache") (req "^0.1.2") (default-features #t) (kind 0)) (crate-dep (name "rayon") (req "^1.5.3") (default-features #t) (kind 0)) (crate-dep (name "rust-gmp") (req "^0.5.0") (default-features #t) (kind 0)))) (hash "09gvpdm9vr4mikwjb7l3wbfx2y4l8p736wp4gjb8j283hrmyr94n")))

(define-public crate-fibnacci-0.4 (crate (name "fibnacci") (vers "0.4.0") (deps (list (crate-dep (name "clap") (req "^3.2.20") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "rayon") (req "^1.5.3") (default-features #t) (kind 0)) (crate-dep (name "rust-gmp") (req "^0.5.0") (default-features #t) (kind 0)))) (hash "0mi8iddp148akilfdxcw33zbsw53xviz0ndkzf8914jckwzx3xza")))

