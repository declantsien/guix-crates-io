(define-module (crates-io fi ni) #:use-module (crates-io))

(define-public crate-finish-it-0.1 (crate (name "finish-it") (vers "0.1.0") (deps (list (crate-dep (name "chrono") (req "^0.4") (features (quote ("serde"))) (default-features #t) (kind 0)) (crate-dep (name "crossterm") (req "^0.25") (features (quote ("serde"))) (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.7.3") (features (quote ("std"))) (kind 0)) (crate-dep (name "rusqlite") (req "^0.28.0") (features (quote ("bundled" "chrono"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "tui") (req "^0.19.0") (features (quote ("crossterm" "serde"))) (kind 0)) (crate-dep (name "tui-textarea") (req "^0.2.0") (default-features #t) (kind 0)))) (hash "1vhr4fbbfvi76yrk3aibisni34wsvzcgd1s64wj41z8l1xchmib0")))

(define-public crate-finite-0.0.0 (crate (name "finite") (vers "0.0.0") (hash "0hlysklnnznxf8wkpzsyj9qjrnswqi5q1rs8f7awgafgzccwfiam")))

(define-public crate-finite-0.0.1 (crate (name "finite") (vers "0.0.1") (hash "191gx97hb674qbi45babkv3fjnkxx6lx81vpd2yvf0xrqc7xvn6r")))

(define-public crate-finite-automata-0.1 (crate (name "finite-automata") (vers "0.1.0") (deps (list (crate-dep (name "segment-map") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "01dqz2xfn35y5mg9fdrjcj3p7wj108dn7fdj7wy15xgjipli1amr")))

(define-public crate-finite-automata-0.1 (crate (name "finite-automata") (vers "0.1.1") (deps (list (crate-dep (name "segment-map") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "0h6dld3zk4b6jc1412vy5cabkb1h0ghwy01853q4d92kgqhp42s2")))

(define-public crate-finite-fields-0.1 (crate (name "finite-fields") (vers "0.1.0") (hash "10kgg3ai4y0q1bjkhqhgc3kh04zizmn4hmz9zkg8q8iwy99n1ikv")))

(define-public crate-finite-fields-0.2 (crate (name "finite-fields") (vers "0.2.0") (deps (list (crate-dep (name "error_def") (req "^0.3.12") (default-features #t) (kind 0)) (crate-dep (name "newtype_derive") (req "^0.1.6") (default-features #t) (kind 0)))) (hash "1k9y5499ffbzy8ygzy50im6c8rklk2775mm7m5w7qcw11sx6wvm5")))

(define-public crate-finite-fields-0.3 (crate (name "finite-fields") (vers "0.3.0") (deps (list (crate-dep (name "error_def") (req "^0.3.12") (default-features #t) (kind 0)) (crate-dep (name "newtype_derive") (req "^0.1.6") (default-features #t) (kind 0)))) (hash "0xxjmk7q7gvli5sf5gclf2b3gmm7jc7yqqrms05rqmibq0di34j6")))

(define-public crate-finite-fields-0.4 (crate (name "finite-fields") (vers "0.4.0") (deps (list (crate-dep (name "error_def") (req "^0.3.12") (default-features #t) (kind 0)) (crate-dep (name "newtype_derive") (req "^0.1.6") (default-features #t) (kind 0)))) (hash "03zkiq14v982niv25ksp57kg90yc87msgzj4gg1ixflq6v0qhk86")))

(define-public crate-finite-fields-0.4 (crate (name "finite-fields") (vers "0.4.1") (deps (list (crate-dep (name "error_def") (req "^0.3.12") (default-features #t) (kind 0)) (crate-dep (name "newtype_derive") (req "^0.1.6") (default-features #t) (kind 0)))) (hash "13fpss0ryxbkp37pbyfcnn7vxwm1nhkm30x3q1ac1q6wh2l5spkq")))

(define-public crate-finite-fields-0.5 (crate (name "finite-fields") (vers "0.5.0") (deps (list (crate-dep (name "error_def") (req "^0.3.12") (default-features #t) (kind 0)) (crate-dep (name "newtype_derive") (req "^0.1.6") (default-features #t) (kind 0)))) (hash "1chc8iw81crql7ryjl7dvrxwsfl0mj7nard0lgqf8bkgx6pl47fx")))

(define-public crate-finite-fields-0.5 (crate (name "finite-fields") (vers "0.5.1") (deps (list (crate-dep (name "error_def") (req "^0.3.12") (default-features #t) (kind 0)) (crate-dep (name "newtype_derive") (req "^0.1.6") (default-features #t) (kind 0)))) (hash "03601qg9lgc5p4dqjs6klh1hhm0gi2pz5w8lqmbvlys76mahfp5d")))

(define-public crate-finite-fields-0.5 (crate (name "finite-fields") (vers "0.5.2") (deps (list (crate-dep (name "error_def") (req "^0.3.12") (default-features #t) (kind 0)) (crate-dep (name "newtype_derive") (req "^0.1.6") (default-features #t) (kind 0)))) (hash "08c1s98qcygyplmlgbdnl9glf4mszpkbcwzkd4lpdzpxbsiqkh0n")))

(define-public crate-finite-fields-0.6 (crate (name "finite-fields") (vers "0.6.0") (deps (list (crate-dep (name "error_def") (req "^0.3.12") (default-features #t) (kind 0)) (crate-dep (name "newtype_derive") (req "^0.1.6") (default-features #t) (kind 0)))) (hash "0pxha1giii25khbbnnnf7sf3zf9lsyg11z5sywifzg72r7p9aaj8")))

(define-public crate-finite-fields-0.6 (crate (name "finite-fields") (vers "0.6.1") (deps (list (crate-dep (name "error_def") (req "^0.3.12") (default-features #t) (kind 0)) (crate-dep (name "newtype_derive") (req "^0.1.6") (default-features #t) (kind 0)))) (hash "12wwhsimil2229jgarf6k11jvbc84nl4an0x74p57jjg5azpmn1x")))

(define-public crate-finite-fields-0.6 (crate (name "finite-fields") (vers "0.6.2") (deps (list (crate-dep (name "error_def") (req "^0.3.12") (default-features #t) (kind 0)) (crate-dep (name "newtype_derive") (req "^0.1.6") (default-features #t) (kind 0)))) (hash "1cfwcy4dqqdr9dsghx44gl87gf2dr5778s654c6dh0qgh11f5khy")))

(define-public crate-finite-fields-0.7 (crate (name "finite-fields") (vers "0.7.0") (deps (list (crate-dep (name "error_def") (req "^0.3.12") (default-features #t) (kind 0)) (crate-dep (name "newtype_derive") (req "^0.1.6") (default-features #t) (kind 0)))) (hash "0nha5671nrxnzjb5ar3pqvyp1jybd23ddjk7scrzf6xy59ikfnmr")))

(define-public crate-finite-fields-0.7 (crate (name "finite-fields") (vers "0.7.1") (deps (list (crate-dep (name "error_def") (req "^0.3.12") (default-features #t) (kind 0)) (crate-dep (name "newtype_derive") (req "^0.1.6") (default-features #t) (kind 0)))) (hash "0wbk53mca63nhqkzddw1caaqjjp1k8vcmnzjnm8q4zib2bgx8gxm")))

(define-public crate-finite-fields-0.7 (crate (name "finite-fields") (vers "0.7.2") (deps (list (crate-dep (name "error_def") (req "^0.3.12") (default-features #t) (kind 0)) (crate-dep (name "newtype_derive") (req "^0.1.6") (default-features #t) (kind 0)))) (hash "0285q1j7c214g2cmqmk4qjqi64cgxigjpkhr339rj8qbnrnw7xxf")))

(define-public crate-finite-fields-0.7 (crate (name "finite-fields") (vers "0.7.3") (deps (list (crate-dep (name "error_def") (req "^0.3.12") (default-features #t) (kind 0)) (crate-dep (name "newtype_derive") (req "^0.1.6") (default-features #t) (kind 0)))) (hash "0na50hd55ba3vn58ms127zk8g3rqaf6ihha8db75dbf83b9q57lr")))

(define-public crate-finite-fields-0.7 (crate (name "finite-fields") (vers "0.7.4") (deps (list (crate-dep (name "error_def") (req "^0.3.12") (default-features #t) (kind 0)) (crate-dep (name "newtype_derive") (req "^0.1.6") (default-features #t) (kind 0)))) (hash "04c34mmn2f7l6zhyb4jwnn00flmj7icfzfmmp5xjixl1alwfhw7b")))

(define-public crate-finite-fields-0.7 (crate (name "finite-fields") (vers "0.7.5") (deps (list (crate-dep (name "error_def") (req "^0.3.12") (default-features #t) (kind 0)) (crate-dep (name "newtype_derive") (req "^0.1.6") (default-features #t) (kind 0)))) (hash "1345achbip0kkd17vd3k8gmpxmqmnn0928jk2655mi11vw41dm3y")))

(define-public crate-finite-fields-0.7 (crate (name "finite-fields") (vers "0.7.6") (deps (list (crate-dep (name "error_def") (req "^0.3.12") (default-features #t) (kind 0)) (crate-dep (name "newtype_derive") (req "^0.1.6") (default-features #t) (kind 0)))) (hash "0qwd1xg3mwk1fdrr21jp55jj73sw6slb4vaya1avbkw70k6kxjrv")))

(define-public crate-finite-fields-0.7 (crate (name "finite-fields") (vers "0.7.7") (deps (list (crate-dep (name "error_def") (req "^0.3.12") (default-features #t) (kind 0)) (crate-dep (name "newtype_derive") (req "^0.1.6") (default-features #t) (kind 0)))) (hash "0bl6iab46gz43f96hfn2bbxvpxr18wd7jah4h971m1il74irjkps")))

(define-public crate-finite-fields-0.7 (crate (name "finite-fields") (vers "0.7.8") (deps (list (crate-dep (name "error_def") (req "^0.3.12") (default-features #t) (kind 0)) (crate-dep (name "newtype_derive") (req "^0.1.6") (default-features #t) (kind 0)))) (hash "19gwiwhwlp5mfdy2yf6qy3r1lf1831b0ldhvgakqkiarjq96hikv")))

(define-public crate-finite-fields-0.8 (crate (name "finite-fields") (vers "0.8.0") (deps (list (crate-dep (name "error_def") (req "^0.3.12") (default-features #t) (kind 0)) (crate-dep (name "newtype_derive") (req "^0.1.6") (default-features #t) (kind 0)))) (hash "0j8x5mbkr19kjqpj41vmjr0hlvvpf9my0kv8r7a4hs60b7fbmr99")))

(define-public crate-finite-fields-0.9 (crate (name "finite-fields") (vers "0.9.0") (deps (list (crate-dep (name "error_def") (req "^0.3.12") (default-features #t) (kind 0)) (crate-dep (name "newtype_derive") (req "^0.1.6") (default-features #t) (kind 0)))) (hash "18fph57qnc4wrrb67nh15ay7yvrrzjkv3v65azpilhna43abj68g")))

(define-public crate-finite-fields-0.9 (crate (name "finite-fields") (vers "0.9.1") (deps (list (crate-dep (name "error_def") (req "^0.3.12") (default-features #t) (kind 0)) (crate-dep (name "newtype_derive") (req "^0.1.6") (default-features #t) (kind 0)))) (hash "0vzvna144ipch8b61pk97yj2m8x8jhhfvl1kwh0j4qmfs755yh78") (features (quote (("nightly"))))))

(define-public crate-finite-fields-0.9 (crate (name "finite-fields") (vers "0.9.2") (deps (list (crate-dep (name "error_def") (req "^0.3.12") (default-features #t) (kind 0)) (crate-dep (name "newtype_derive") (req "^0.1.6") (default-features #t) (kind 0)))) (hash "166l0hbn0gq4pcsj15rscnr2n3n7qz5qmqc99ly7ccvdkgibj4rc") (features (quote (("nightly"))))))

(define-public crate-finite-fields-0.9 (crate (name "finite-fields") (vers "0.9.3") (deps (list (crate-dep (name "error_def") (req "^0.3.12") (default-features #t) (kind 0)) (crate-dep (name "newtype_derive") (req "^0.1.6") (default-features #t) (kind 0)))) (hash "1lwqyji1lalv071fq1q2s6imrf54rdv6d5yj05qff74qcmq7gf7z") (features (quote (("nightly"))))))

(define-public crate-finite-fields-0.9 (crate (name "finite-fields") (vers "0.9.4") (deps (list (crate-dep (name "error_def") (req "^0.3.12") (default-features #t) (kind 0)) (crate-dep (name "newtype_derive") (req "^0.1.6") (default-features #t) (kind 0)))) (hash "1ck9mb6n6kxgghw1sgzjpss5x4gkqcz2rw7yqw0sp9gx3qavp6r9") (features (quote (("nightly"))))))

(define-public crate-finite-fields-0.9 (crate (name "finite-fields") (vers "0.9.5") (deps (list (crate-dep (name "error_def") (req "^0.3.12") (default-features #t) (kind 0)) (crate-dep (name "newtype_derive") (req "^0.1.6") (default-features #t) (kind 0)))) (hash "020njd99nxpn5sp5mkk2wj2zcl58ip0sy7jhrlf8kysd9gahndg6") (features (quote (("nightly"))))))

(define-public crate-finite-fields-0.10 (crate (name "finite-fields") (vers "0.10.0") (deps (list (crate-dep (name "error_def") (req "^0.3.12") (default-features #t) (kind 0)) (crate-dep (name "newtype_derive") (req "^0.1.6") (default-features #t) (kind 0)))) (hash "1sxj2lzdbjvxfysklw0qj1i5q2kqdyr0s9i03vs9yd4dvw811jrq") (features (quote (("nightly"))))))

(define-public crate-finite-fields-0.10 (crate (name "finite-fields") (vers "0.10.1") (deps (list (crate-dep (name "error_def") (req "^0.3.12") (default-features #t) (kind 0)) (crate-dep (name "newtype_derive") (req "^0.1.6") (default-features #t) (kind 0)))) (hash "1nfl3kqkiyskc9d4w4g72ak9j41b69q358lq7msk41ynnk58msqr") (features (quote (("nightly"))))))

(define-public crate-finite-fields-0.10 (crate (name "finite-fields") (vers "0.10.2") (deps (list (crate-dep (name "error_def") (req "^0.3.12") (default-features #t) (kind 0)) (crate-dep (name "newtype_derive") (req "^0.1.6") (default-features #t) (kind 0)))) (hash "13f320vjjm4588npvmamcaxlw9sfvkd8zni3xp7fmy03m3dq2lw3") (features (quote (("nightly"))))))

(define-public crate-finite-fields-0.10 (crate (name "finite-fields") (vers "0.10.3") (deps (list (crate-dep (name "error_def") (req "^0.3.12") (default-features #t) (kind 0)) (crate-dep (name "newtype_derive") (req "^0.1.6") (default-features #t) (kind 0)))) (hash "0lrlqkh4wfjzxpwzac41gnbv4fs1bjd6ssrl0gc63adjq9b5nnid") (features (quote (("nightly"))))))

(define-public crate-finite-float-0.0.1 (crate (name "finite-float") (vers "0.0.1") (hash "04say2abb264qq9bx5ym0j5hihj94hy296086bs7dvfirmym34m0") (rust-version "1.49")))

(define-public crate-finite-state-automaton-0.1 (crate (name "finite-state-automaton") (vers "0.1.0") (hash "0v6bqmff2c8pfsvcffk6jlz10yayngaq4wy6imbj7dprkw6gm530")))

(define-public crate-finite-state-automaton-0.1 (crate (name "finite-state-automaton") (vers "0.1.1") (hash "00dm6azbsb8hqa7hydhdcnrq1zk1asfp8grdsws3riaq4n4w6a7i")))

(define-public crate-finite-state-automaton-0.1 (crate (name "finite-state-automaton") (vers "0.1.2") (hash "0vh5yhkb42r791s5s54b63mnqgi87za0brnmh5j36hwyi7qj6lj3")))

(define-public crate-finite-state-machine-0.1 (crate (name "finite-state-machine") (vers "0.1.0") (deps (list (crate-dep (name "paste") (req "^1.0.12") (default-features #t) (kind 0)))) (hash "15fqsa621nb0s0f1nh7hlngn0hvdn3wkmd0ajrpz6d3k3b2ni36n") (features (quote (("verbose"))))))

(define-public crate-finite-state-machine-0.1 (crate (name "finite-state-machine") (vers "0.1.1") (deps (list (crate-dep (name "paste") (req "^1.0.12") (default-features #t) (kind 0)))) (hash "0mz2n98z71z4pwlgk255hfg96ys68y42ignwsgg3hjp53iy4i4c5") (features (quote (("verbose"))))))

(define-public crate-finite-state-machine-0.2 (crate (name "finite-state-machine") (vers "0.2.0") (deps (list (crate-dep (name "paste") (req "^1.0.12") (default-features #t) (kind 0)))) (hash "0k4pqyz0h1s83kxvjf1yrqrxf2p9ij1qvk76hh86jzf0k09k4na5") (features (quote (("verbose") ("derive_default") ("default" "derive_default"))))))

(define-public crate-finite-wasm-0.1 (crate (name "finite-wasm") (vers "0.1.0") (deps (list (crate-dep (name "atoi") (req "^2.0.0") (default-features #t) (kind 2)) (crate-dep (name "bitvec") (req "^1.0.1") (default-features #t) (kind 0)) (crate-dep (name "dissimilar") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2.15") (default-features #t) (kind 0)) (crate-dep (name "prefix-sum-vec") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "rayon") (req "^1.6.1") (default-features #t) (kind 2)) (crate-dep (name "thiserror") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.0") (default-features #t) (kind 2)) (crate-dep (name "walkdir") (req "^2.3.0") (default-features #t) (kind 2)) (crate-dep (name "wasm-encoder") (req "^0.20") (default-features #t) (kind 2)) (crate-dep (name "wasmparser") (req "^0.96") (default-features #t) (kind 0)) (crate-dep (name "wasmprinter") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "wast") (req "^50") (default-features #t) (kind 2)))) (hash "17lrkjsq2vg6c2h8czj15vf7x1hrj83l2yk1d11b2jwl3m4h5y41")))

(define-public crate-finite-wasm-0.2 (crate (name "finite-wasm") (vers "0.2.0") (deps (list (crate-dep (name "atoi") (req "^2.0.0") (default-features #t) (kind 2)) (crate-dep (name "bitvec") (req "^1.0.1") (default-features #t) (kind 0)) (crate-dep (name "criterion") (req "^0.4.0") (default-features #t) (kind 2)) (crate-dep (name "dissimilar") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2.15") (default-features #t) (kind 0)) (crate-dep (name "prefix-sum-vec") (req "^0.1.2") (default-features #t) (kind 0)) (crate-dep (name "rayon") (req "^1.6.1") (default-features #t) (kind 2)) (crate-dep (name "thiserror") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "walkdir") (req "^2.3.0") (default-features #t) (kind 2)) (crate-dep (name "wasm-encoder") (req "^0.20") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "wasm-instrument") (req "^0.4") (default-features #t) (kind 2)) (crate-dep (name "wasmparser") (req "^0.98.1") (default-features #t) (kind 0)) (crate-dep (name "wasmprinter") (req "^0.2.49") (default-features #t) (kind 0)) (crate-dep (name "wast") (req "^52") (default-features #t) (kind 2)))) (hash "1rqcxkw8w11l6b3lnklarigmqdjfrp08dgl2mwz7fffdcqhm6wfx") (features (quote (("instrument" "wasm-encoder"))))))

(define-public crate-finite-wasm-0.3 (crate (name "finite-wasm") (vers "0.3.0") (deps (list (crate-dep (name "atoi") (req "^2.0.0") (default-features #t) (kind 2)) (crate-dep (name "bitvec") (req "^1.0.1") (default-features #t) (kind 0)) (crate-dep (name "criterion") (req "^0.4.0") (default-features #t) (kind 2)) (crate-dep (name "dissimilar") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2.15") (default-features #t) (kind 0)) (crate-dep (name "prefix-sum-vec") (req "^0.1.2") (default-features #t) (kind 0)) (crate-dep (name "rayon") (req "^1.6.1") (default-features #t) (kind 2)) (crate-dep (name "thiserror") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "walkdir") (req "^2.3.0") (default-features #t) (kind 2)) (crate-dep (name "wasm-encoder") (req "^0.22") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "wasm-instrument") (req "^0.4") (default-features #t) (kind 2)) (crate-dep (name "wasmparser") (req "^0.99") (default-features #t) (kind 0)) (crate-dep (name "wasmprinter") (req "^0.2.49") (default-features #t) (kind 0)) (crate-dep (name "wast") (req "^52") (default-features #t) (kind 2)))) (hash "1myixywhl8z8r7dalm7iwxw4l219sq31hficbvvn51cv9zh23f66") (features (quote (("instrument" "wasm-encoder"))))))

(define-public crate-finite-wasm-0.3 (crate (name "finite-wasm") (vers "0.3.1") (deps (list (crate-dep (name "atoi") (req "^2.0.0") (default-features #t) (kind 2)) (crate-dep (name "bitvec") (req "^1.0.1") (default-features #t) (kind 0)) (crate-dep (name "criterion") (req "^0.4.0") (default-features #t) (kind 2)) (crate-dep (name "dissimilar") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2.15") (default-features #t) (kind 0)) (crate-dep (name "prefix-sum-vec") (req "^0.1.2") (default-features #t) (kind 0)) (crate-dep (name "rayon") (req "^1.6.1") (default-features #t) (kind 2)) (crate-dep (name "thiserror") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "walkdir") (req "^2.3.0") (default-features #t) (kind 2)) (crate-dep (name "wasm-encoder") (req "^0.22") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "wasm-instrument") (req "^0.4") (default-features #t) (kind 2)) (crate-dep (name "wasmparser") (req "^0.99") (default-features #t) (kind 0)) (crate-dep (name "wasmprinter") (req "^0.2.49") (default-features #t) (kind 0)) (crate-dep (name "wast") (req "^52") (default-features #t) (kind 2)))) (hash "1s5hknipfihclc723559j3i7nsmij78wpka4x20dmkhqg3hgx4yk") (features (quote (("instrument" "wasm-encoder"))))))

(define-public crate-finite-wasm-0.4 (crate (name "finite-wasm") (vers "0.4.0") (deps (list (crate-dep (name "atoi") (req "^2.0.0") (default-features #t) (kind 2)) (crate-dep (name "bitvec") (req "^1.0.1") (default-features #t) (kind 0)) (crate-dep (name "criterion") (req "^0.4.0") (default-features #t) (kind 2)) (crate-dep (name "dissimilar") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2.15") (default-features #t) (kind 0)) (crate-dep (name "prefix-sum-vec") (req "^0.1.2") (default-features #t) (kind 0)) (crate-dep (name "rayon") (req "^1.6.1") (default-features #t) (kind 2)) (crate-dep (name "thiserror") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "walkdir") (req "^2.3.0") (default-features #t) (kind 2)) (crate-dep (name "wasm-encoder") (req "^0.26") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "wasm-instrument") (req "^0.4") (default-features #t) (kind 2)) (crate-dep (name "wasmparser") (req "^0.104.0") (default-features #t) (kind 0)) (crate-dep (name "wasmprinter") (req "=0.2.56") (default-features #t) (kind 0)) (crate-dep (name "wast") (req "^52") (default-features #t) (kind 2)))) (hash "017yx7wdjv287jnks6fwhsz0sxn80kq3g2spg3iw4gd54qj7rwpm") (features (quote (("instrument" "wasm-encoder"))))))

(define-public crate-finite-wasm-0.5 (crate (name "finite-wasm") (vers "0.5.0") (deps (list (crate-dep (name "atoi") (req "^2.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "bitvec") (req "^1.0.1") (default-features #t) (kind 0)) (crate-dep (name "criterion") (req "^0.4.0") (default-features #t) (kind 2)) (crate-dep (name "dissimilar") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.4") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2.144") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2.15") (default-features #t) (kind 0)) (crate-dep (name "prefix-sum-vec") (req "^0.1.2") (default-features #t) (kind 0)) (crate-dep (name "rayon") (req "^1.6.1") (default-features #t) (kind 2)) (crate-dep (name "tempfile") (req "^3.5") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "walkdir") (req "^2.3.0") (default-features #t) (kind 2)) (crate-dep (name "wasm-encoder") (req "^0.27.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "wasm-instrument") (req "^0.4") (default-features #t) (kind 2)) (crate-dep (name "wasmparser") (req "^0.105.0") (default-features #t) (kind 0)) (crate-dep (name "wasmprinter") (req "=0.2.57") (default-features #t) (kind 0)) (crate-dep (name "wast") (req "^52") (optional #t) (default-features #t) (kind 0)))) (hash "0m3qbrkqv3k0kvafkbb27q9hh9bwy8f4fdjfyvm6j9lwj88vb0bd") (features (quote (("wast-tests" "atoi" "instrument" "lazy_static" "libc" "tempfile" "wast") ("instrument" "wasm-encoder"))))))

(define-public crate-finite_element_method-0.2 (crate (name "finite_element_method") (vers "0.2.0") (deps (list (crate-dep (name "extended_matrix") (req "^0.2.11") (default-features #t) (kind 0)))) (hash "165aisddrln93maa7w0bxnwjspkhpl3j7c20d8mirbwa1nhafpdc") (yanked #t)))

(define-public crate-finite_element_method-0.2 (crate (name "finite_element_method") (vers "0.2.1") (deps (list (crate-dep (name "extended_matrix") (req "^0.2.11") (default-features #t) (kind 0)))) (hash "1bhpwn08j34v579xmmymcrybj4pvzasik7kyhk33xnsdhafjw5jj") (yanked #t)))

(define-public crate-finite_element_method-0.2 (crate (name "finite_element_method") (vers "0.2.2") (deps (list (crate-dep (name "extended_matrix") (req "^0.2.11") (default-features #t) (kind 0)))) (hash "1gb9hq4yf71a82iis5pi0rmwy8cwa0az4mhy2mz12bp0r0kawrzf") (yanked #t)))

(define-public crate-finite_element_method-0.2 (crate (name "finite_element_method") (vers "0.2.3") (deps (list (crate-dep (name "extended_matrix") (req "^0.2.11") (default-features #t) (kind 0)))) (hash "1g377f8ri0aqi513gfqh8icnga0j6k0dgqjcwj9zdamy0k2c5j60") (yanked #t)))

(define-public crate-finite_element_method-0.2 (crate (name "finite_element_method") (vers "0.2.4") (deps (list (crate-dep (name "extended_matrix") (req "^0.2.11") (default-features #t) (kind 0)))) (hash "0vkfqpv278x2hamn28ix6miz0cay5rqw860gc2jjip096bcfl3ja") (yanked #t)))

(define-public crate-finite_element_method-0.2 (crate (name "finite_element_method") (vers "0.2.5") (deps (list (crate-dep (name "extended_matrix") (req "^0.2.11") (default-features #t) (kind 0)))) (hash "1ksk7s0fmqbm3zm09ljxg8l3rcjq4ld6jr1bcpphs6wgca141iwf") (yanked #t)))

(define-public crate-finite_element_method-0.2 (crate (name "finite_element_method") (vers "0.2.6") (deps (list (crate-dep (name "extended_matrix") (req "^0.2.12") (default-features #t) (kind 0)))) (hash "04fizxn180mdw9sqngm793vih9m339ghyvx1vn2cfahjgv9mlmyv") (yanked #t)))

(define-public crate-finite_element_method-0.2 (crate (name "finite_element_method") (vers "0.2.7") (deps (list (crate-dep (name "extended_matrix") (req "^0.2.12") (default-features #t) (kind 0)))) (hash "1bn3w2zxwlb00i456d9bsljgy40hkrxaqw53rcgaffxl02xk1csd") (yanked #t)))

(define-public crate-finite_element_method-0.2 (crate (name "finite_element_method") (vers "0.2.8") (deps (list (crate-dep (name "extended_matrix") (req "^0.2.12") (default-features #t) (kind 0)))) (hash "0mvscdbk0w0bvkamscf4bhfrkn6klx9sl4i00xsss0b6ib384a4n") (yanked #t)))

(define-public crate-finite_element_method-0.2 (crate (name "finite_element_method") (vers "0.2.9") (deps (list (crate-dep (name "extended_matrix") (req "^0.2.12") (default-features #t) (kind 0)))) (hash "02561qb42l9dq4lr61lyw77j931ppzw99ifzqg8s5iy7rygq2gqg") (yanked #t)))

(define-public crate-finite_element_method-0.2 (crate (name "finite_element_method") (vers "0.2.10") (deps (list (crate-dep (name "extended_matrix") (req "^0.2.12") (default-features #t) (kind 0)))) (hash "0s2fwqzf15jvgyq1haipjsl6fg3dawajija2hvaxzpcccnifx8vl")))

(define-public crate-finite_element_method-0.3 (crate (name "finite_element_method") (vers "0.3.0") (deps (list (crate-dep (name "extended_matrix") (req "^0.3.2") (default-features #t) (kind 0)))) (hash "1nm9iicvk3ffi8v9clc12390nvpirx7ykxbyv1i66pdqi71pmlz0") (yanked #t)))

(define-public crate-finite_element_method-0.3 (crate (name "finite_element_method") (vers "0.3.1") (deps (list (crate-dep (name "extended_matrix") (req "^0.3.6") (default-features #t) (kind 0)))) (hash "0py7fffpjr3js6wq4xz0ab5zbn7y1cgjnvwkgwlw0l8ldbl5187x")))

(define-public crate-finite_element_method-0.4 (crate (name "finite_element_method") (vers "0.4.0") (deps (list (crate-dep (name "extended_matrix") (req "^0.3.8") (default-features #t) (kind 0)))) (hash "06r53bzz343s2h358ndgfiiafmx6081dssc45dcd9gqwbppcsabg") (yanked #t)))

(define-public crate-finite_element_method-0.4 (crate (name "finite_element_method") (vers "0.4.1") (deps (list (crate-dep (name "extended_matrix") (req "^0.3.9") (default-features #t) (kind 0)))) (hash "148p5fglayiavka1m056zgphqisnfiynyrw4yakh1g2f8khba8zw")))

(define-public crate-finite_element_method-0.4 (crate (name "finite_element_method") (vers "0.4.2") (deps (list (crate-dep (name "extended_matrix") (req "^0.3.12") (default-features #t) (kind 0)) (crate-dep (name "extended_matrix_float") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "06bbqgjjm1ypsrb3fqy21175mgz503v29a48rm71z1iisn0mspia")))

(define-public crate-finite_element_method-0.4 (crate (name "finite_element_method") (vers "0.4.3") (deps (list (crate-dep (name "extended_matrix") (req "^0.3.14") (default-features #t) (kind 0)) (crate-dep (name "extended_matrix_float") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "1fmyvzf143scdkpaqih2z2majvnlzfi60qyyky0r7rbd2kak6g3s") (yanked #t)))

(define-public crate-finite_element_method-0.4 (crate (name "finite_element_method") (vers "0.4.4") (deps (list (crate-dep (name "extended_matrix") (req "^0.3.14") (default-features #t) (kind 0)) (crate-dep (name "extended_matrix_float") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "0bvv3pz4q7n7380yxv09b4wjqfpkm8badbn57dxylg4x340aqwdr")))

(define-public crate-finite_element_method-0.4 (crate (name "finite_element_method") (vers "0.4.5") (deps (list (crate-dep (name "extended_matrix") (req "^0.3.14") (default-features #t) (kind 0)) (crate-dep (name "extended_matrix_float") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "17rfh12qk1kgvk4kxc99fmyfzzg194dpsgqds2wz1rzcrm5caywv")))

(define-public crate-finite_element_method-0.4 (crate (name "finite_element_method") (vers "0.4.6") (deps (list (crate-dep (name "extended_matrix") (req "^0.3.14") (default-features #t) (kind 0)) (crate-dep (name "extended_matrix_float") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "0dk24q1f5xyabz0llkkl444b38bl60838gab99lh992wgb3rqfcs")))

(define-public crate-finite_element_method-0.4 (crate (name "finite_element_method") (vers "0.4.7") (deps (list (crate-dep (name "extended_matrix") (req "^0.3.14") (default-features #t) (kind 0)) (crate-dep (name "extended_matrix_float") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "061j6wr3rs2rg0k89kljq8crbpxq87iqmx4xh4y6yhnil643h9gk")))

(define-public crate-finite_element_method-0.4 (crate (name "finite_element_method") (vers "0.4.8") (deps (list (crate-dep (name "extended_matrix") (req "^0.3.14") (default-features #t) (kind 0)) (crate-dep (name "extended_matrix_float") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "0n8pjm9iq2lg7qwdd0rrvmbaybjix0c6lfbqf263yw36rg6iwwn5")))

(define-public crate-finite_element_method-0.4 (crate (name "finite_element_method") (vers "0.4.9") (deps (list (crate-dep (name "extended_matrix") (req "^0.3.14") (default-features #t) (kind 0)) (crate-dep (name "extended_matrix_float") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "0z8905bslilcqf7rm3dl6yhs139rfvfmhfmy1ric0hz236n8ynf5")))

(define-public crate-finite_element_method-0.4 (crate (name "finite_element_method") (vers "0.4.10") (deps (list (crate-dep (name "extended_matrix") (req "^0.3.17") (default-features #t) (kind 0)) (crate-dep (name "extended_matrix_float") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "0kfm3zxwn2r7cnzw1vxh0365xfddf4inpfb9gpm4qa4xgy98snia")))

(define-public crate-finite_element_method-0.4 (crate (name "finite_element_method") (vers "0.4.11") (deps (list (crate-dep (name "extended_matrix") (req "^0.3.17") (default-features #t) (kind 0)) (crate-dep (name "extended_matrix_float") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "1sixdrszb6s270iy745f0d8g4wx7nv5vv778fd448yj7kjmf95qj") (yanked #t)))

(define-public crate-finite_element_method-0.4 (crate (name "finite_element_method") (vers "0.4.12") (deps (list (crate-dep (name "colsol") (req "^1.0.0") (default-features #t) (kind 0)) (crate-dep (name "extended_matrix") (req "^0.9.1") (default-features #t) (kind 0)))) (hash "0x4q5qha7p4427yqyvnclq8igjkdfwnh0hv2m8li04hazkr3l08x")))

(define-public crate-finite_element_method-0.9 (crate (name "finite_element_method") (vers "0.9.0") (deps (list (crate-dep (name "colsol") (req "^1.0.0") (default-features #t) (kind 0)) (crate-dep (name "extended_matrix") (req "^0.9.1") (default-features #t) (kind 0)))) (hash "00zrf3gvx1z7qz4fpp5f6sf0jvr8dnr0ns8ffm9d4n2gkhx8ab8l")))

(define-public crate-finite_element_method-0.9 (crate (name "finite_element_method") (vers "0.9.1") (deps (list (crate-dep (name "colsol") (req "^1.0.0") (default-features #t) (kind 0)) (crate-dep (name "extended_matrix") (req "^0.9.1") (default-features #t) (kind 0)))) (hash "1ad785r0a0w7wy0v384sqwb6p5mpbdwprksmbhm3qg4zkvxkrd2i")))

(define-public crate-finite_element_method-0.9 (crate (name "finite_element_method") (vers "0.9.2") (deps (list (crate-dep (name "colsol") (req "^1.0.0") (default-features #t) (kind 0)) (crate-dep (name "extended_matrix") (req "^0.9.1") (default-features #t) (kind 0)))) (hash "091c25r7nvdv4f50j2ii1r843mgmz1clmjaqspam03bxxzwca4nh")))

(define-public crate-finite_element_method-0.9 (crate (name "finite_element_method") (vers "0.9.3") (deps (list (crate-dep (name "colsol") (req "^1.0.0") (default-features #t) (kind 0)) (crate-dep (name "extended_matrix") (req "^0.9.2") (default-features #t) (kind 0)))) (hash "0m61a7x4pywbrvcshmqrahiahb1m4dkjbbmxm4nycmy3wj5qly7q")))

(define-public crate-finite_repr-0.1 (crate (name "finite_repr") (vers "0.1.0") (deps (list (crate-dep (name "finite_repr_derive") (req "^0.1.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "finite_repr_derive") (req "^0.1.0") (default-features #t) (kind 2)))) (hash "0adrisirc5snf9jgnmsxcgx5mnxrai5rvy01wyjcq2r2if0d0pn7") (features (quote (("strict") ("derive" "finite_repr_derive"))))))

(define-public crate-finite_repr-0.1 (crate (name "finite_repr") (vers "0.1.1") (deps (list (crate-dep (name "finite_repr_derive") (req "^0.1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "finite_repr_derive") (req "^0.1") (default-features #t) (kind 2)))) (hash "059dksmsmavxi9jbnhrzb57j7161j73a6id1kvh5hpl4cg199hp5") (features (quote (("strict") ("derive" "finite_repr_derive"))))))

(define-public crate-finite_repr-0.1 (crate (name "finite_repr") (vers "0.1.3") (deps (list (crate-dep (name "finite_repr_derive") (req "^0.1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "finite_repr_derive") (req "^0.1") (default-features #t) (kind 2)))) (hash "0mqbxj6dggawd0ng2by4hb45wd7ip0xgwmddgdbpwm4n51v0kifv") (features (quote (("strict") ("derive" "finite_repr_derive"))))))

(define-public crate-finite_repr-0.1 (crate (name "finite_repr") (vers "0.1.4") (deps (list (crate-dep (name "finite_repr_derive") (req "^0.1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "finite_repr_derive") (req "^0.1") (default-features #t) (kind 2)))) (hash "0mnirlka7ghj13r2502z6vycfbnj2prvr8yaz5fymrw8kq0gkvcj") (features (quote (("strict") ("derive" "finite_repr_derive"))))))

(define-public crate-finite_repr_derive-0.1 (crate (name "finite_repr_derive") (vers "0.1.0") (deps (list (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "trybuild") (req "^1.0.30") (default-features #t) (kind 2)))) (hash "0x8hdbk24xhp4ml77adq1zl7ckdlkpnipdzac7la1hsr311pnljq") (features (quote (("strict"))))))

(define-public crate-finite_repr_derive-0.1 (crate (name "finite_repr_derive") (vers "0.1.1") (deps (list (crate-dep (name "finite_repr") (req "^0.1") (features (quote ("derive"))) (default-features #t) (kind 2)) (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "trybuild") (req "^1.0.30") (default-features #t) (kind 2)))) (hash "0z3vvyczsjw1bd2ribbbf62b3p3ia002j5prbw9q6qhcfq1hb66p") (features (quote (("strict"))))))

(define-public crate-finite_repr_derive-0.1 (crate (name "finite_repr_derive") (vers "0.1.3") (deps (list (crate-dep (name "finite_repr") (req "^0.1") (features (quote ("derive"))) (default-features #t) (kind 2)) (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "trybuild") (req "^1.0.30") (default-features #t) (kind 2)))) (hash "1an1hihzpz00lay8xzzkkkx4nr9jsi2ks007hwd1phsxqwz45a95") (features (quote (("strict"))))))

(define-public crate-finite_repr_derive-0.1 (crate (name "finite_repr_derive") (vers "0.1.4") (deps (list (crate-dep (name "finite_repr") (req "^0.1") (features (quote ("derive"))) (default-features #t) (kind 2)) (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "trybuild") (req "^1.0.30") (default-features #t) (kind 2)))) (hash "108lsfhfmdc2rrxz1f48hiflsc0zhid5mrksmnwnzkax1gc9ymmf") (features (quote (("strict"))))))

(define-public crate-finitediff-0.1 (crate (name "finitediff") (vers "0.1.0") (deps (list (crate-dep (name "ndarray") (req "^0.12.1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "ndarray-linalg") (req "^0.10.0") (optional #t) (default-features #t) (kind 0)))) (hash "0aw3z2f1kwirrkr4g2x562llp0ps2239p1kshdp9lx6xx8ybx065")))

(define-public crate-finitediff-0.1 (crate (name "finitediff") (vers "0.1.1") (deps (list (crate-dep (name "ndarray") (req "^0.12.1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "ndarray-linalg") (req "^0.10.0") (optional #t) (default-features #t) (kind 0)))) (hash "09zz64wcpbwpr3apf4skqqw0iljf3n8qgdk9i8px15xxppm4adwp")))

(define-public crate-finitediff-0.1 (crate (name "finitediff") (vers "0.1.2") (deps (list (crate-dep (name "ndarray") (req "^0.13.0") (optional #t) (default-features #t) (kind 0)))) (hash "0haqz2wv511bns8mqll96py1a7ifmmrq89cdw46masn9khhykgs3")))

(define-public crate-finitediff-0.1 (crate (name "finitediff") (vers "0.1.3") (deps (list (crate-dep (name "ndarray") (req "^0.14.0") (optional #t) (default-features #t) (kind 0)))) (hash "1mssk15fm1bd63ris4pvmasxj67g1yf66jx72fxajvapxw7dzmhd")))

(define-public crate-finitediff-0.1 (crate (name "finitediff") (vers "0.1.4") (deps (list (crate-dep (name "ndarray") (req "^0.15.0") (optional #t) (default-features #t) (kind 0)))) (hash "0nknp8n40q1b0kbiypn24bh0bl8z19p4crjs95gri65k0wmkz6z2")))

(define-public crate-finiteelement-0.1 (crate (name "finiteelement") (vers "0.1.0") (deps (list (crate-dep (name "finiteelement_macros") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2.8") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.7.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "1s4qinsq6rbdccnbnpgrck633clkpz0fgrix4i2bq1wf6bhygbbs")))

(define-public crate-finiteelement-0.3 (crate (name "finiteelement") (vers "0.3.0") (deps (list (crate-dep (name "codenano") (req "^0.3.0") (kind 0)) (crate-dep (name "finiteelement_macros") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2.8") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.6") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "1bb35w0iihcxgn0r6xqwigs0613067q2k4w21zfw2cgc0x6960zr")))

(define-public crate-finiteelement_macros-0.1 (crate (name "finiteelement_macros") (vers "0.1.0") (deps (list (crate-dep (name "num-traits") (req "^0.2.8") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.7.0") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.39") (default-features #t) (kind 0)))) (hash "11m05h3s86cbr3k2rnckh9adjysxpl9ibx6rz1fgydapsgc484rr")))

(define-public crate-finitefields-0.1 (crate (name "finitefields") (vers "0.1.0") (deps (list (crate-dep (name "cc") (req "^1.0") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2.91") (default-features #t) (kind 0)))) (hash "1gcyxzngz5zkhf1zrmcfwgspsip7wzxw4hrp4x0f9ycq9pp0pc4q")))

(define-public crate-finitefields-0.1 (crate (name "finitefields") (vers "0.1.1") (deps (list (crate-dep (name "cc") (req "^1.0") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2.91") (default-features #t) (kind 0)))) (hash "1scgcb656c2lx6szs2rkr0b8ympsrb4fkp4am1l7iwbz7w3vynsx")))

(define-public crate-finitelib-0.1 (crate (name "finitelib") (vers "0.1.0") (deps (list (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)))) (hash "1g51b4kfcw143m1fxdlm1f3lggrbmzb7q2f8i6wh1w6cjf3qhi1d")))

(define-public crate-finitelib-0.1 (crate (name "finitelib") (vers "0.1.1") (deps (list (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)))) (hash "0sl16yc1z6jjzcgv23sgh80icv1skm4cbjcn71xjab52lrvzh0hn")))

(define-public crate-finitio-0.1 (crate (name "finitio") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^4.0.12") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "nom") (req "^7.0") (default-features #t) (kind 0)) (crate-dep (name "nom_locate") (req "^4.0") (default-features #t) (kind 0)) (crate-dep (name "parse-hyperlinks") (req "^0.23.4") (default-features #t) (kind 0)) (crate-dep (name "resolver") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde-hashkey") (req "^0.4.3") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "snafu") (req "^0.7.3") (default-features #t) (kind 0)))) (hash "1d1487hsyllxq4cqpwqrw067srxzg3z1wjbph7z9hf5gcvrj256i")))

(define-public crate-finito-0.1 (crate (name "finito") (vers "0.1.0") (deps (list (crate-dep (name "futures-timer") (req "^3") (default-features #t) (kind 0)) (crate-dep (name "pin-project") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1") (features (quote ("macros" "rt"))) (default-features #t) (kind 2)))) (hash "15dz1l7pijyjsfsiyn518vkmm3srwfg7lmj39fhmh8hnhmfj9113") (features (quote (("wasm-bindgen" "futures-timer/wasm-bindgen") ("default")))) (rust-version "1.70.0")))

(define-public crate-finity-0.1 (crate (name "finity") (vers "0.1.0") (deps (list (crate-dep (name "typenum") (req "^1.10") (default-features #t) (kind 0)) (crate-dep (name "void") (req "^1") (kind 0)))) (hash "11fdlvfxiixilgs6r8pnrl99hxy9nrgcbf3mh65gbafzhymzxf73")))

