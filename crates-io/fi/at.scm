(define-module (crates-io fi at) #:use-module (crates-io))

(define-public crate-fiat-constify-0.0.0 (crate (name "fiat-constify") (vers "0.0.0") (hash "087w5xllf23xssqd82qp2rm46h91xa1ni7b9xyn44ha5kpijxrbj")))

(define-public crate-fiat-crypto-0.1 (crate (name "fiat-crypto") (vers "0.1.0") (hash "18pg8hg545d920a4f5chgsmfph8lrlc48i9wb8pli5pfgzjd79qi")))

(define-public crate-fiat-crypto-0.1 (crate (name "fiat-crypto") (vers "0.1.1") (hash "0h3mmd6a1qbm89p7077nz9g49r43r58v2032g589qyc3n6w9zmvz")))

(define-public crate-fiat-crypto-0.1 (crate (name "fiat-crypto") (vers "0.1.2") (hash "1viy2gnxlpldlzxxn5vw7g4ih7cgb01j8zqbn1b4cxa661fz1fhb")))

(define-public crate-fiat-crypto-0.1 (crate (name "fiat-crypto") (vers "0.1.3") (hash "0kv3fn73n16p8iljzaz460dqa298n06z1j5ixi92nmbv2wm3f1qk")))

(define-public crate-fiat-crypto-0.1 (crate (name "fiat-crypto") (vers "0.1.4") (hash "0jcjy5pkcgb44yiwpxj0b50b0an88i0ls75jm1ild75502v8dlkv")))

(define-public crate-fiat-crypto-0.1 (crate (name "fiat-crypto") (vers "0.1.5") (hash "0sv1gdks031dz375glwzxwfkyi0fzw7k7a00mnv5fn31jmqbjshg")))

(define-public crate-fiat-crypto-0.1 (crate (name "fiat-crypto") (vers "0.1.6") (hash "11gg0hag5ak2g52qv5vvgky591fzgvvz2s2ilmlsk6adf0r2ncbj")))

(define-public crate-fiat-crypto-0.1 (crate (name "fiat-crypto") (vers "0.1.7") (hash "0z1bdjiz7dxrjl19n1f6yydq8psrr8p1w3a1jjf09zwnyw0a7i96")))

(define-public crate-fiat-crypto-0.1 (crate (name "fiat-crypto") (vers "0.1.8") (hash "0l3q9pc9y67m66npgix0drvb7n772s438bfza77zjf63j536srxh")))

(define-public crate-fiat-crypto-0.1 (crate (name "fiat-crypto") (vers "0.1.9") (hash "1gg1avz5kaf5sy6z5kzig94yy1994ky2z748qyyjbd8lbjn1y6i3")))

(define-public crate-fiat-crypto-0.1 (crate (name "fiat-crypto") (vers "0.1.10") (hash "0jck765r0wdlyxi6jg5xzwvvmjm5rcdhrh4z5k9za6fdkbb96giz") (features (quote (("std") ("default" "std"))))))

(define-public crate-fiat-crypto-0.1 (crate (name "fiat-crypto") (vers "0.1.11") (hash "12gyr01z3bkahbxcrj2nsdq42qbqgq12w32dvpr6a7930i0qfxi1") (features (quote (("std") ("default" "std"))))))

(define-public crate-fiat-crypto-0.1 (crate (name "fiat-crypto") (vers "0.1.12") (hash "0h0igif7z3cdkpfcqm28n8a6li7hvd1jcsr47iylnarzdpjd31y1") (features (quote (("std") ("default" "std"))))))

(define-public crate-fiat-crypto-0.1 (crate (name "fiat-crypto") (vers "0.1.13") (hash "1z0pw0pm8mxm7p3lvr4is84b42yxqwjrlhbgcis969cxpzv4qd9m") (features (quote (("std") ("default" "std"))))))

(define-public crate-fiat-crypto-0.1 (crate (name "fiat-crypto") (vers "0.1.14") (hash "1kismm295372lybd00akqhkzx687gy0vxn8a536ss2ydx0h9zv3h") (features (quote (("std") ("default" "std"))))))

(define-public crate-fiat-crypto-0.1 (crate (name "fiat-crypto") (vers "0.1.16") (hash "1m3ka0mydbqvli47rmbgs6crz5n3sq6nw1rgcqaqvb1lbh4k4qc2") (features (quote (("std") ("default" "std"))))))

(define-public crate-fiat-crypto-0.1 (crate (name "fiat-crypto") (vers "0.1.17") (hash "141wyw711ywk9xzvd7xsi5014qkv4y51zbpkg1j467bki2xza552") (features (quote (("std") ("default" "std"))))))

(define-public crate-fiat-crypto-0.1 (crate (name "fiat-crypto") (vers "0.1.18") (hash "1zjxv8sg6qci3zngkx599kzbhxc7lh85vnj5129rknad3v2z7cjl") (features (quote (("std") ("default" "std"))))))

(define-public crate-fiat-crypto-0.1 (crate (name "fiat-crypto") (vers "0.1.19") (hash "0m9rpk28wribkrmkbd86a2hawzrdd7maksij7b9qx761gknfdb4k") (features (quote (("std") ("default" "std"))))))

(define-public crate-fiat-crypto-0.1 (crate (name "fiat-crypto") (vers "0.1.20") (hash "0xvbcg6wh42q3n7294mzq5xxw8fpqsgc0d69dvm5srh1f6cgc9g8") (features (quote (("std") ("default" "std"))))))

(define-public crate-fiat-crypto-0.2 (crate (name "fiat-crypto") (vers "0.2.0") (hash "1y6hcsphbczph34mvlvwzhxly0w1wyz5njpcssr0bvpkik8f4bb9") (features (quote (("std") ("default" "std"))))))

(define-public crate-fiat-crypto-0.2 (crate (name "fiat-crypto") (vers "0.2.1") (hash "0b8xvq0gn2231zlg06763a81xqv59z127wy9wldlhjvd0620r1yh") (features (quote (("std") ("default" "std"))))))

(define-public crate-fiat-crypto-0.2 (crate (name "fiat-crypto") (vers "0.2.2") (hash "1ixkvhrc97dfyvwc6kszza7h8jqjf4zk8k22ala1p3vprxm5i0d4") (features (quote (("std") ("default" "std"))))))

(define-public crate-fiat-crypto-0.2 (crate (name "fiat-crypto") (vers "0.2.3") (hash "1pg1swnq3mqmyzp5rlki0sh7c61q8zvbqb2gdfc88pkq3gz3g47n") (features (quote (("std") ("default" "std"))))))

(define-public crate-fiat-crypto-0.2 (crate (name "fiat-crypto") (vers "0.2.4") (hash "106zg5n1m4xbzagxfg4f74szpzf5s0zhc9y1s6x7909ih03nz9ak") (features (quote (("std") ("default" "std"))))))

(define-public crate-fiat-crypto-0.2 (crate (name "fiat-crypto") (vers "0.2.5") (hash "1dxn0g50pv0ppal779vi7k40fr55pbhkyv4in7i13pgl4sn3wmr7") (features (quote (("std") ("default" "std"))))))

(define-public crate-fiat-crypto-0.2 (crate (name "fiat-crypto") (vers "0.2.6") (hash "10hkkkjynhibvchznkxx81gwxqarn9i5sgz40d6xxb8xzhsz8xhn") (features (quote (("std") ("default" "std"))))))

(define-public crate-fiat-crypto-0.2 (crate (name "fiat-crypto") (vers "0.2.7") (hash "03w3ic88yvdpwbz36dlm7csacz4b876mlc0nbbwbc75y7apb21y0") (features (quote (("std") ("default" "std"))))))

(define-public crate-fiat-crypto-0.2 (crate (name "fiat-crypto") (vers "0.2.8") (hash "07p0gaynz13i9vr1133gix1nzapzh6bjq37478p42crvb5akqy9q") (features (quote (("std") ("default" "std"))))))

(define-public crate-fiat-crypto-0.2 (crate (name "fiat-crypto") (vers "0.2.9") (hash "07c1vknddv3ak7w89n85ik0g34nzzpms6yb845vrjnv9m4csbpi8") (features (quote (("std") ("default" "std"))))))

(define-public crate-fiat-lux-0.3 (crate (name "fiat-lux") (vers "0.3.3") (deps (list (crate-dep (name "clap") (req "^3.2.6") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "comfy-table") (req "^6.0.0") (default-features #t) (kind 0)) (crate-dep (name "directories") (req "^4.0.1") (default-features #t) (kind 0)) (crate-dep (name "indexmap") (req "^1.9.1") (default-features #t) (kind 0)) (crate-dep (name "pager") (req "^0.16.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.5.6") (default-features #t) (kind 0)) (crate-dep (name "tantivy") (req "^0.18.0") (default-features #t) (kind 0)) (crate-dep (name "terminal_size") (req "^0.1.17") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.31") (default-features #t) (kind 0)))) (hash "1v7qg93rf7fg9r464k5hjzs3j7hadhd5vxgpj89qsyksi7n54xf5")))

(define-public crate-fiat-lux-0.3 (crate (name "fiat-lux") (vers "0.3.4") (deps (list (crate-dep (name "clap") (req "^3.2.6") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "comfy-table") (req "^6.0.0") (default-features #t) (kind 0)) (crate-dep (name "directories") (req "^4.0.1") (default-features #t) (kind 0)) (crate-dep (name "indexmap") (req "^1.9.1") (default-features #t) (kind 0)) (crate-dep (name "pager") (req "^0.16.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.5.6") (default-features #t) (kind 0)) (crate-dep (name "tantivy") (req "^0.18.0") (default-features #t) (kind 0)) (crate-dep (name "terminal_size") (req "^0.1.17") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.31") (default-features #t) (kind 0)))) (hash "0s9rw9qlhkgq4wdgcdszmxkhlvj36ic753lp85l9q3nz8p2qzqzi")))

(define-public crate-fiat-lux-0.3 (crate (name "fiat-lux") (vers "0.3.5") (deps (list (crate-dep (name "clap") (req "^3.2.7") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "comfy-table") (req "^6.0.0") (default-features #t) (kind 0)) (crate-dep (name "directories") (req "^4.0.1") (default-features #t) (kind 0)) (crate-dep (name "pager") (req "^0.16.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.5.6") (default-features #t) (kind 0)) (crate-dep (name "tantivy") (req "^0.18.0") (default-features #t) (kind 0)) (crate-dep (name "terminal_size") (req "^0.1.17") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.31") (default-features #t) (kind 0)))) (hash "026j3l3hxxp6ziygh433sy6yr7kgszwrwiajnmyp1g5i63ffb5i4") (features (quote (("default" "pager")))) (v 2) (features2 (quote (("pager" "dep:pager"))))))

(define-public crate-fiat-lux-0.3 (crate (name "fiat-lux") (vers "0.3.6") (deps (list (crate-dep (name "clap") (req "^3.2.8") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "comfy-table") (req "^6.0.0") (default-features #t) (kind 0)) (crate-dep (name "directories") (req "^4.0.1") (default-features #t) (kind 0)) (crate-dep (name "pager") (req "^0.16.0") (default-features #t) (target "cfg(not(windows))") (kind 0)) (crate-dep (name "regex") (req "^1.5.6") (default-features #t) (kind 0)) (crate-dep (name "tantivy") (req "^0.18.0") (default-features #t) (kind 0)) (crate-dep (name "terminal_size") (req "^0.1.17") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.31") (default-features #t) (kind 0)))) (hash "1997rzvfq7sc540r0md4lkcgaj5ygc3ayz996xkzm828sqfz6g4m")))

(define-public crate-fiat-lux-0.3 (crate (name "fiat-lux") (vers "0.3.7") (deps (list (crate-dep (name "clap") (req "^4.1.8") (features (quote ("derive" "wrap_help"))) (default-features #t) (kind 0)) (crate-dep (name "comfy-table") (req "^6.1.4") (default-features #t) (kind 0)) (crate-dep (name "directories") (req "^4.0.1") (default-features #t) (kind 0)) (crate-dep (name "pager") (req "^0.16.1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.7.1") (default-features #t) (kind 0)) (crate-dep (name "tantivy") (req "^0.19.2") (default-features #t) (kind 0)) (crate-dep (name "terminal_size") (req "^0.2.5") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.39") (default-features #t) (kind 0)))) (hash "01znwa27q7zzq8bnrryca01z2hjj08naksix93nmqpbvqxhz2b5i") (features (quote (("default" "pager")))) (v 2) (features2 (quote (("pager" "dep:pager"))))))

