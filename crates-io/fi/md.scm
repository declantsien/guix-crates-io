(define-module (crates-io fi md) #:use-module (crates-io))

(define-public crate-fimd-0.0.0 (crate (name "fimd") (vers "0.0.0") (deps (list (crate-dep (name "ahash") (req "^0.8.6") (default-features #t) (kind 0)) (crate-dep (name "hashbrown") (req "^0.14.3") (default-features #t) (kind 0)) (crate-dep (name "markdown") (req "^1.0.0-alpha.16") (default-features #t) (kind 0)))) (hash "12q85m4qrfbcfp69w326z55kcfx0vc021bm3n3xypr0h1k4lwjp6")))

(define-public crate-fimdoc-0.1 (crate (name "fimdoc") (vers "0.1.0") (deps (list (crate-dep (name "camino") (req "^1.1.6") (default-features #t) (kind 0)) (crate-dep (name "markdown") (req "^0.3.0") (default-features #t) (kind 0)))) (hash "19mxkvi8v1lpqgnf53dgq3dbjabskjqjacw9vp53kd8zwsap78yr")))

(define-public crate-fimdoc-0.1 (crate (name "fimdoc") (vers "0.1.1") (deps (list (crate-dep (name "camino") (req "^1.1.6") (default-features #t) (kind 0)) (crate-dep (name "markdown") (req "^0.3.0") (default-features #t) (kind 0)))) (hash "0waim4izgflwvp4488zkyhgypkznl9dpjqc9c7prnivawj5gxw52")))

(define-public crate-fimdoc-0.1 (crate (name "fimdoc") (vers "0.1.2") (deps (list (crate-dep (name "camino") (req "^1.1.6") (default-features #t) (kind 0)) (crate-dep (name "markdown") (req "^1.0.0-alpha.16") (default-features #t) (kind 0)))) (hash "0djqvy062dbz73xwjxwnmhmjf0i7k13lfax9r3n3vipfc43hwlw1")))

(define-public crate-fimdoc-0.1 (crate (name "fimdoc") (vers "0.1.3") (deps (list (crate-dep (name "camino") (req "^1.1.6") (default-features #t) (kind 0)) (crate-dep (name "markdown") (req "^1.0.0-alpha.16") (default-features #t) (kind 0)))) (hash "0w0v7w98x1zzqrsym4kgg4bmmxrzmipcg6ph72drpjiskfskp25b")))

(define-public crate-fimdoc-0.1 (crate (name "fimdoc") (vers "0.1.4") (deps (list (crate-dep (name "camino") (req "^1.1.6") (default-features #t) (kind 0)) (crate-dep (name "markdown") (req "^1.0.0-alpha.16") (default-features #t) (kind 0)))) (hash "0a12bjnx5319zv1ni0m2sp5qg22x9kgl5raq8rapq7alxp94ljr7")))

(define-public crate-fimdoc-0.1 (crate (name "fimdoc") (vers "0.1.5") (deps (list (crate-dep (name "camino") (req "^1.1.6") (default-features #t) (kind 0)) (crate-dep (name "markdown") (req "^1.0.0-alpha.16") (default-features #t) (kind 0)))) (hash "14npgpd5brf8nwpacwy49rxgwzxhdbd6cx74kzqikn4pknaz1y49")))

(define-public crate-fimdoc-0.1 (crate (name "fimdoc") (vers "0.1.6") (deps (list (crate-dep (name "camino") (req "^1.1.6") (default-features #t) (kind 0)) (crate-dep (name "markdown") (req "^1.0.0-alpha.16") (default-features #t) (kind 0)))) (hash "11rj4a5cvwki56mv3fnr03qk1l16k2k8bsvks52d7pcyf5i8ngsr")))

(define-public crate-fimdoc-0.1 (crate (name "fimdoc") (vers "0.1.7") (deps (list (crate-dep (name "camino") (req "^1.1.6") (default-features #t) (kind 0)) (crate-dep (name "markdown") (req "^1.0.0-alpha.16") (default-features #t) (kind 0)))) (hash "1wsl86v6km4wjyahzdzd4ci3d71sanzyx415g3bdrl5anvngvz1a")))

(define-public crate-fimdoc-0.1 (crate (name "fimdoc") (vers "0.1.8") (deps (list (crate-dep (name "camino") (req "^1.1.6") (default-features #t) (kind 0)) (crate-dep (name "markdown") (req "^1.0.0-alpha.16") (default-features #t) (kind 0)))) (hash "0fsqbrya0wdw6kq6spxp6vnlr0iqjhkw1jzk9kgvwrh64i61dvfl")))

(define-public crate-fimdoc-0.1 (crate (name "fimdoc") (vers "0.1.9") (deps (list (crate-dep (name "camino") (req "^1.1.6") (default-features #t) (kind 0)) (crate-dep (name "markdown") (req "^1.0.0-alpha.16") (default-features #t) (kind 0)))) (hash "0fyxnk7k390yhg5rwyxbh6y7q9zxnm2zf6dl692ajvraanbk23ki")))

(define-public crate-fimdoc-0.2 (crate (name "fimdoc") (vers "0.2.0") (deps (list (crate-dep (name "atty") (req "^0.2.14") (default-features #t) (kind 0)) (crate-dep (name "camino") (req "^1.1.6") (default-features #t) (kind 0)) (crate-dep (name "markdown") (req "^1.0.0-alpha.16") (default-features #t) (kind 0)))) (hash "0f01qgcrlhhlb4xm638lh76q2c4qmd56zaakcai6kfdw5wbb4s71")))

(define-public crate-fimdoc-0.2 (crate (name "fimdoc") (vers "0.2.1") (deps (list (crate-dep (name "camino") (req "^1.1.6") (default-features #t) (kind 0)) (crate-dep (name "markdown") (req "^1.0.0-alpha.16") (default-features #t) (kind 0)) (crate-dep (name "rarity") (req "^0.1.1") (default-features #t) (kind 0)))) (hash "12k2pdzc5scrcbrp29phczv6h095k2wbcmdp0bbsabcqj8nlj4ph")))

(define-public crate-fimdoc-0.2 (crate (name "fimdoc") (vers "0.2.2") (deps (list (crate-dep (name "camino") (req "^1.1.6") (default-features #t) (kind 0)) (crate-dep (name "markdown") (req "^1.0.0-alpha.16") (default-features #t) (kind 0)) (crate-dep (name "rarity") (req "^0.1.4") (default-features #t) (kind 0)))) (hash "19vgrppiahpgym2046xjw369yg4wfzqy2gqickcpga5krsx538fq")))

(define-public crate-fimdoc-0.3 (crate (name "fimdoc") (vers "0.3.0") (deps (list (crate-dep (name "camino") (req "^1.1.6") (default-features #t) (kind 0)) (crate-dep (name "markdown") (req "^1.0.0-alpha.16") (default-features #t) (kind 0)) (crate-dep (name "rarity") (req "^0.1.4") (default-features #t) (kind 0)))) (hash "0i75sm0jkdpraxaad0rvih5jfgn99bp6pxbc635m533kdgasy0x1")))

(define-public crate-fimdoc-0.3 (crate (name "fimdoc") (vers "0.3.1") (deps (list (crate-dep (name "camino") (req "^1.1.6") (default-features #t) (kind 0)) (crate-dep (name "markdown") (req "^1.0.0-alpha.16") (default-features #t) (kind 0)) (crate-dep (name "rarity") (req "^0.1.5") (default-features #t) (kind 0)))) (hash "1ji871y6531j8l3vddcc19ngybby9ncbaj6fvg2fq338xcx8xkn8")))

(define-public crate-fimdoc-0.3 (crate (name "fimdoc") (vers "0.3.2") (deps (list (crate-dep (name "camino") (req "^1.1.6") (default-features #t) (kind 0)) (crate-dep (name "markdown") (req "^1.0.0-alpha.16") (default-features #t) (kind 0)) (crate-dep (name "rarity") (req "^0.1.5") (default-features #t) (kind 0)))) (hash "1ldc2iwv346b62x2h39r34la3g80q5ddw1vq0wwlsixy32szjq8y")))

(define-public crate-fimdoc-0.3 (crate (name "fimdoc") (vers "0.3.3") (deps (list (crate-dep (name "camino") (req "^1.1.6") (default-features #t) (kind 0)) (crate-dep (name "markdown") (req "^1.0.0-alpha.16") (default-features #t) (kind 0)) (crate-dep (name "rarity") (req "^0.1.5") (default-features #t) (kind 0)))) (hash "0dmdqcqf18fw0d06kyrn7jh2jv7ai29h4l73alp1vnhpivm4n5kp")))

(define-public crate-fimdoc-0.3 (crate (name "fimdoc") (vers "0.3.4") (deps (list (crate-dep (name "camino") (req "^1.1.6") (default-features #t) (kind 0)) (crate-dep (name "golden-oak-library") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "1r0h6y9f5b7pswrf61j4c9zj84sscbprwrs6qw8rdhxj3bl2pz81")))

(define-public crate-fimdoc-0.4 (crate (name "fimdoc") (vers "0.4.0") (deps (list (crate-dep (name "camino") (req "^1.1.6") (default-features #t) (kind 0)) (crate-dep (name "golden-oak-library") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "indoc") (req "^2.0.5") (default-features #t) (kind 0)))) (hash "1hcscan8vmd4jcd72zxag1c3g6fnk14s8z6isg5w5nhd3m4m0yq1")))

(define-public crate-fimdoc-0.5 (crate (name "fimdoc") (vers "0.5.0") (deps (list (crate-dep (name "camino") (req "^1.1.6") (default-features #t) (kind 0)) (crate-dep (name "golden-oak-library") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "indoc") (req "^2.0.5") (default-features #t) (kind 0)))) (hash "0f8y5z6bx6s55lna0hbbrn8cs4g9ajizgqgbdyhbx92ld2kffx2n")))

(define-public crate-fimdoc-0.6 (crate (name "fimdoc") (vers "0.6.0") (deps (list (crate-dep (name "camino") (req "^1.1.6") (default-features #t) (kind 0)) (crate-dep (name "indoc") (req "^2.0.5") (default-features #t) (kind 0)) (crate-dep (name "markdown") (req "^1.0.0-alpha.16") (default-features #t) (kind 0)) (crate-dep (name "pinkie-pie") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "0qdcay1lzqcc6m62paqay4b23fi46dlcpn4xbsbs8y7vhxndhzi7")))

(define-public crate-fimdoc-0.6 (crate (name "fimdoc") (vers "0.6.1") (deps (list (crate-dep (name "camino") (req "^1.1.6") (default-features #t) (kind 0)) (crate-dep (name "golden-oak-library") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "indoc") (req "^2.0.5") (default-features #t) (kind 0)) (crate-dep (name "markdown") (req "^1.0.0-alpha.16") (default-features #t) (kind 0)))) (hash "0bm10i5lqvr6bx8s6qv37nbwzn1b09prgymgq50qx7kqs2bb0ivl")))

