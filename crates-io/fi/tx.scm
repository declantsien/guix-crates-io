(define-module (crates-io fi tx) #:use-module (crates-io))

(define-public crate-fitx-0.1 (crate (name "fitx") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^2") (default-features #t) (kind 0)))) (hash "1fizs80gbg7j2h1494smb094z0mr4qzhsxvm2yx4qyp1zb03rcxy")))

(define-public crate-fitx-0.1 (crate (name "fitx") (vers "0.1.1") (deps (list (crate-dep (name "clap") (req "^3") (features (quote ("cargo"))) (default-features #t) (kind 0)))) (hash "06hlxcvdzvff814n21n9708831kmr017w00abhyzi2qjv7cl9162")))

