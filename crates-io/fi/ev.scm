(define-module (crates-io fi ev) #:use-module (crates-io))

(define-public crate-fievar-0.1 (crate (name "fievar") (vers "0.1.0") (deps (list (crate-dep (name "proc-macro-error") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (features (quote ("derive" "parsing"))) (default-features #t) (kind 0)))) (hash "0vb10p6cmwa13qxgjaj609jxwz4kkdr5ab5nsvwpv9chd4h04rb0")))

(define-public crate-fievar-0.1 (crate (name "fievar") (vers "0.1.1") (deps (list (crate-dep (name "proc-macro-error") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (features (quote ("derive" "parsing"))) (default-features #t) (kind 0)))) (hash "1v59q675pv21bcdmn03mdr5br68bsp1jv76djxb50206wh0xf11s")))

(define-public crate-fievar-0.1 (crate (name "fievar") (vers "0.1.2") (deps (list (crate-dep (name "proc-macro-error") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (features (quote ("derive" "parsing"))) (default-features #t) (kind 0)))) (hash "11lwdw7ivk9f8c1b4d3xxa960ay9zzkxdlfl37s2jpkzp37k678j")))

(define-public crate-fievar-0.2 (crate (name "fievar") (vers "0.2.0") (deps (list (crate-dep (name "proc-macro-error") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (features (quote ("derive" "parsing"))) (default-features #t) (kind 0)))) (hash "1d8ln1xxm4anx5k92pvv7fawpr42xj2bb10nkywy3hh8pfbmkh18")))

