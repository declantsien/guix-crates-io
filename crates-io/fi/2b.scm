(define-module (crates-io fi #{2b}#) #:use-module (crates-io))

(define-public crate-fi2b-1 (crate (name "fi2b") (vers "1.0.0") (deps (list (crate-dep (name "proc-macro2") (req "^1.0.76") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.35") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2.0.48") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "07dqkh6iv58mlybd982bnk2shsdwjii4kkklz8qs5wf7ziq8r5cg")))

