(define-module (crates-io fi i-) #:use-module (crates-io))

(define-public crate-fii-id-0.1 (crate (name "fii-id") (vers "0.1.0") (deps (list (crate-dep (name "rand") (req "^0.7.3") (default-features #t) (kind 0)))) (hash "0qz4nkxcpf6g2bz5jwc2kpk4n3xigfmc49a16iaprn17glzf3zc9")))

(define-public crate-fii-id-1 (crate (name "fii-id") (vers "1.0.0") (deps (list (crate-dep (name "rand") (req "^0.7.3") (default-features #t) (kind 0)))) (hash "0mv1iwjkrzblrs5rypzjdl5dj8mvw5gqf5w9f6n4qh0x10fmm2pa")))

(define-public crate-fii-id-1 (crate (name "fii-id") (vers "1.3.0") (deps (list (crate-dep (name "rand") (req "^0.8.0") (default-features #t) (kind 0)))) (hash "0vc63ckxyg3m4fr0i6fdd2myi0wwxaqf7dx9r394lbyvvwlnrmsa")))

(define-public crate-fii-id-1 (crate (name "fii-id") (vers "1.3.1") (deps (list (crate-dep (name "rand") (req "^0.8.0") (default-features #t) (kind 0)))) (hash "0mqyf7cb5zk732zybswblyxkcclzdwx3ybn6c88v2fhizazn2h3q")))

(define-public crate-fii-id-1 (crate (name "fii-id") (vers "1.4.0") (deps (list (crate-dep (name "rand") (req "^0.8.3") (default-features #t) (kind 0)))) (hash "0j2g51vlk9fplkzbknkp5a36lxf2l1db4css6pqp03g451zib2s3")))

