(define-module (crates-io fi ve) #:use-module (crates-io))

(define-public crate-five32-0.1 (crate (name "five32") (vers "0.1.0") (deps (list (crate-dep (name "five32-instruction-set") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "1xi4bdx7r4fhfv36kdp289wlj0bm885b710j26wp8c4aq46zr71s")))

(define-public crate-five32-instruction-set-0.1 (crate (name "five32-instruction-set") (vers "0.1.0") (deps (list (crate-dep (name "bitvec") (req "^1.0.1") (default-features #t) (kind 0)) (crate-dep (name "funty") (req "^2.0.0") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "paste") (req "^1.0.9") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "strum") (req "^0.24") (default-features #t) (kind 0)) (crate-dep (name "strum_macros") (req "^0.24.3") (default-features #t) (kind 0)))) (hash "0jja9z3mcf7xlc9m2w8m653a9zhqkb66vcg09synv13jqf2hiysz")))

(define-public crate-fivedd-0.1 (crate (name "fivedd") (vers "0.1.0") (hash "0ciwbhr5sn7a7vdfvj7v2crab9ipp4cgjh1rdclmwr9m3ya5i5cz")))

(define-public crate-fivedd-client-0.1 (crate (name "fivedd-client") (vers "0.1.0") (hash "1rfl2cdrdf6rw4nx1hbba2pfpzd5dxg8045hvdw0v6ml0q305kzz")))

(define-public crate-fivedd-gui-0.1 (crate (name "fivedd-gui") (vers "0.1.0") (hash "0x5308wm976cpf7njz0x6qv6j4c4d551yxkwhlnagfs7igkk6gq5")))

(define-public crate-fivedd-server-0.1 (crate (name "fivedd-server") (vers "0.1.0") (hash "1hfgp4l4mbkfd7hdmd1z8h1ij8bdg4bhnmvycavvvay6h32w3772")))

(define-public crate-fiveo-0.1 (crate (name "fiveo") (vers "0.1.0") (deps (list (crate-dep (name "strsim") (req "^0.7.0") (default-features #t) (kind 0)))) (hash "0nk5y3zcig3ma2cf7417kiiwrf2gqq4i559m7n0xck1f0lq3w72d")))

(define-public crate-fiveo-0.2 (crate (name "fiveo") (vers "0.2.0") (deps (list (crate-dep (name "strsim") (req "^0.7.0") (default-features #t) (kind 0)))) (hash "0268zbsvh0rahaswd1j5n59qr4bqwbp38r2klaqqli68m516pk55")))

(define-public crate-fiveo-0.3 (crate (name "fiveo") (vers "0.3.0") (hash "1xmnbg9a967a3sjkii0d4c0p4vi5l2y1lihpq6b63njjwdrd5ada") (features (quote (("webassembly"))))))

(define-public crate-fiveo-0.3 (crate (name "fiveo") (vers "0.3.1") (hash "0wkagjqwy3n61l0f23r32ncr6lbjqb9mapi8iqfrdsg831awp1x6") (features (quote (("webassembly"))))))

(define-public crate-fiveo-0.3 (crate (name "fiveo") (vers "0.3.2") (hash "0j1gm5sg84gl8kb75p1241jppbl65cxq9sjq281l4gyxjbf0d5q7") (features (quote (("webassembly"))))))

