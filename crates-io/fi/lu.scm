(define-module (crates-io fi lu) #:use-module (crates-io))

(define-public crate-filum-0.1 (crate (name "filum") (vers "0.1.0") (deps (list (crate-dep (name "libc") (req "^0.2.66") (default-features #t) (kind 0)))) (hash "0s3hldrvl5n56nf3k30hkxln04hb7libfah9crxfrzhh9y405pqn") (features (quote (("verbose"))))))

(define-public crate-filum-0.1 (crate (name "filum") (vers "0.1.1") (deps (list (crate-dep (name "libc") (req "^0.2.66") (default-features #t) (kind 0)))) (hash "1xwwakf0a599phnvx8pnkjmwyqv4f9rqpid4r5g9wqn1z6i8sbl7") (features (quote (("verbose"))))))

(define-public crate-filum-0.1 (crate (name "filum") (vers "0.1.2") (deps (list (crate-dep (name "libc") (req "^0.2.66") (default-features #t) (kind 0)))) (hash "0w4vzg1bwwbahinyph843ba3g44pvr6syl2lz2gvscc797lay2xf") (features (quote (("verbose"))))))

