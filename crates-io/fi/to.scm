(define-module (crates-io fi to) #:use-module (crates-io))

(define-public crate-fito-0.1 (crate (name "fito") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^2") (default-features #t) (kind 0)))) (hash "0d1ldx1l3jndprcx7kkcfgh0hmzkwvck9nchnkk9nfxbs37qsfsg")))

(define-public crate-fito-0.1 (crate (name "fito") (vers "0.1.1") (deps (list (crate-dep (name "clap") (req "^2") (default-features #t) (kind 0)))) (hash "1kwadw0l12xa4kgjh4a0aynx77zy77gilbdwfgds3c72dqhgssp8")))

