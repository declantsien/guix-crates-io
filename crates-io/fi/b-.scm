(define-module (crates-io fi b-) #:use-module (crates-io))

(define-public crate-fib-sequence-0.1 (crate (name "fib-sequence") (vers "0.1.0") (deps (list (crate-dep (name "rand") (req "^0.8.4") (default-features #t) (kind 2)))) (hash "0lbiz4p1kzhrh7anbv51jcb9ksgk46bdginzvs5a7w9jkrallrj1")))

