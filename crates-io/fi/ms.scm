(define-module (crates-io fi ms) #:use-module (crates-io))

(define-public crate-fimsh-type-0.1 (crate (name "fimsh-type") (vers "0.1.0") (deps (list (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)) (crate-dep (name "termion") (req "^2.0.1") (default-features #t) (kind 0)))) (hash "0mlgwj0h2fd5v9grx4p8vavd70xrmqz7860fr48f92n5ap62617p")))

