(define-module (crates-io fi gl) #:use-module (crates-io))

(define-public crate-figlet-0.1 (crate (name "figlet") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "~2.32") (default-features #t) (kind 0)))) (hash "110jacsskrqz1031mzlss1x2zbgvscwxpvdp736im6fslj1fzh1y") (yanked #t)))

(define-public crate-figlet-rs-0.1 (crate (name "figlet-rs") (vers "0.1.0") (hash "080yfcn76v2m3w2l8c2p8q597p1cv55sh4zv9q6lw8pdmnax4mcy")))

(define-public crate-figlet-rs-0.1 (crate (name "figlet-rs") (vers "0.1.1") (hash "0cjszjmb8ihq5jl81fwp37ki3pwv1kfhb89pc6qn3jr87ppyxhfg")))

(define-public crate-figlet-rs-0.1 (crate (name "figlet-rs") (vers "0.1.2") (hash "0q2gbnbdmwn7gvd8xfhzaskilxfyyvhp0csk4lsdnd3cagzhs1fq")))

(define-public crate-figlet-rs-0.1 (crate (name "figlet-rs") (vers "0.1.3") (hash "1ani0adih1v153zv0ly5vpgcjfsppv90q4d36mmp5s93li1ki2ar")))

(define-public crate-figlet-rs-0.1 (crate (name "figlet-rs") (vers "0.1.4") (hash "1vb3mqhhvwxfkg05l63wb17fah8vgpayj28553908qjhr17mrbq1")))

(define-public crate-figlet-rs-0.1 (crate (name "figlet-rs") (vers "0.1.5") (hash "1igv75b0hbfsjli57vnpk740rm2k7vx0h6psz63gr54nrmqs0hj7")))

