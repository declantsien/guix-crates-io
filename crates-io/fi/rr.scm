(define-module (crates-io fi rr) #:use-module (crates-io))

(define-public crate-firrtl-0.1 (crate (name "firrtl") (vers "0.1.0") (hash "08kfm9ky9ya4hd37b8k2isji1iasg7s7g3k3az76ipcqmlxql4zk")))

(define-public crate-firrtl-parser-0.1 (crate (name "firrtl-parser") (vers "0.1.0") (deps (list (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "nom") (req "^7.0.0-alpha1") (features (quote ("regexp"))) (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.5") (default-features #t) (kind 0)))) (hash "0jjwnrgr8y5z6vky7p9n590wizdac6ip2y5mqym7w1dlhv2v1nsh")))

