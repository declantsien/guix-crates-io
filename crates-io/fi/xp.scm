(define-module (crates-io fi xp) #:use-module (crates-io))

(define-public crate-fixparser-0.1 (crate (name "fixparser") (vers "0.1.0") (deps (list (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (features (quote ("preserve_order"))) (default-features #t) (kind 0)))) (hash "1c5n4xpqy1lc8h3b7y10l7kbyiryh20kwxl3b4mj3kr2h2w4igmy")))

(define-public crate-fixparser-0.1 (crate (name "fixparser") (vers "0.1.1") (deps (list (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (features (quote ("preserve_order"))) (default-features #t) (kind 0)))) (hash "04imcksdppry1hxa7vn8iji399rcbdc6g3fijva8d8k400rj511w")))

(define-public crate-fixparser-0.1 (crate (name "fixparser") (vers "0.1.2") (deps (list (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (features (quote ("preserve_order"))) (default-features #t) (kind 0)))) (hash "0g5rhgzcdvngia55bq7g0giyqqlvnbf5mdl2q34mwbc6bxmxphk4") (features (quote (("debugging"))))))

(define-public crate-fixparser-0.1 (crate (name "fixparser") (vers "0.1.3") (deps (list (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (features (quote ("preserve_order"))) (default-features #t) (kind 0)))) (hash "195i5xnl8rjr6dynkpgjnaw12g9viy7f2vffp2yj1y7sshfr5jg3") (features (quote (("debugging"))))))

(define-public crate-fixparser-0.1 (crate (name "fixparser") (vers "0.1.4") (deps (list (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (features (quote ("preserve_order"))) (default-features #t) (kind 0)))) (hash "1kmp9wkdg241afsv5q1z07frw74aybwrlkll9q4m3z0bwizngma4") (features (quote (("debugging"))))))

(define-public crate-fixparser-0.1 (crate (name "fixparser") (vers "0.1.5") (deps (list (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (features (quote ("preserve_order"))) (default-features #t) (kind 0)) (crate-dep (name "wasm-bindgen") (req "^0.2") (default-features #t) (kind 0)))) (hash "0bf9hdh08j4ks9zvmv796n0w7dy91mikyk6ygc60ymsdbc24c5na") (features (quote (("debugging"))))))

(define-public crate-fixpoint-0.1 (crate (name "fixpoint") (vers "0.1.0") (hash "1kspbs7x593aqd5ksld06gygzzc9rz7fd9vyl0kqkvqafk5iq1nr")))

(define-public crate-fixpoint-0.2 (crate (name "fixpoint") (vers "0.2.0") (hash "0hlqv4wjrk9ncap91nfzfx4nrhn1s2ffdq9ry98c0vgnn6rrci4a")))

