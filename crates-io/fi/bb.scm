(define-module (crates-io fi bb) #:use-module (crates-io))

(define-public crate-fibbox-0.1 (crate (name "fibbox") (vers "0.1.0") (deps (list (crate-dep (name "gmp-mpfr-sys") (req "=1.1.13") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.7.3") (default-features #t) (kind 2)) (crate-dep (name "rug") (req "=1.6.0") (default-features #t) (kind 0)))) (hash "1ky41wjdbiq3vk0dx4jn1g95nxaid73gkih966f28f8d507izavj")))

