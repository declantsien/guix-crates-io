(define-module (crates-io pn eu) #:use-module (crates-io))

(define-public crate-pneuma-0.0.1 (crate (name "pneuma") (vers "0.0.1") (hash "1cfq3dp5lvwqjfggmmr407hh4d0sx0zcg6lhv6v5vc0pvqgzjqr4")))

(define-public crate-pneumatic-0.1 (crate (name "pneumatic") (vers "0.1.0") (hash "153wnb63ma795wyvgfrpzgwvs8zn0jcn82xs2r9jyw76dldgg0j2")))

