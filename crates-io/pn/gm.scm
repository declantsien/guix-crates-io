(define-module (crates-io pn gm) #:use-module (crates-io))

(define-public crate-pngme-0.5 (crate (name "pngme") (vers "0.5.0") (deps (list (crate-dep (name "crc") (req "^1.8.1") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.3.15") (default-features #t) (kind 0)))) (hash "05ys6qahirmsmwkwy1a88riwhn41ydahrmnrx4632gmdzcjqgwbb")))

(define-public crate-pngme-0.5 (crate (name "pngme") (vers "0.5.1") (deps (list (crate-dep (name "crc") (req "^1.8.1") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.3.15") (default-features #t) (kind 0)))) (hash "12y0bnmll22myj0al60lqs7hbdsfr6y9ns91ak4019y1fi2pn45s")))

(define-public crate-pngme-0.5 (crate (name "pngme") (vers "0.5.2") (deps (list (crate-dep (name "crc") (req "^1.8.1") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.3.15") (default-features #t) (kind 0)))) (hash "0s1zza7wapbqbf10rfrihxganqz12bibg5dx8ssb4qnrzm8365b6")))

(define-public crate-pngmini-0.0.0 (crate (name "pngmini") (vers "0.0.0") (hash "1im4dmpdg5gnhn6p4mqwzb3z019asbca6g4rxv4sj3gzki9n6ms6")))

