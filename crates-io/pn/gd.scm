(define-module (crates-io pn gd) #:use-module (crates-io))

(define-public crate-pngdefry-0.0.1 (crate (name "pngdefry") (vers "0.0.1") (deps (list (crate-dep (name "bindgen") (req "^0.69.4") (default-features #t) (kind 1)) (crate-dep (name "cc") (req "^1.0.83") (features (quote ("parallel"))) (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2.152") (default-features #t) (kind 0)))) (hash "1j4v1wwnwynkcwlfw83g4nj3njijkxzkyiiqcdvf3wad9vb9j2v9")))

(define-public crate-pngdefry-0.0.2 (crate (name "pngdefry") (vers "0.0.2") (deps (list (crate-dep (name "bindgen") (req "^0.69.4") (default-features #t) (kind 1)) (crate-dep (name "cc") (req "^1.0.83") (features (quote ("parallel"))) (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2.152") (default-features #t) (kind 0)))) (hash "0wqzhk0p04rfd9qqq0wwd5msf5g0qhsxq89y33zwhb7sbzzddimb")))

(define-public crate-pngdefry-0.0.3 (crate (name "pngdefry") (vers "0.0.3") (deps (list (crate-dep (name "bindgen") (req "^0.69.4") (default-features #t) (kind 1)) (crate-dep (name "cc") (req "^1.0.83") (features (quote ("parallel"))) (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2.152") (default-features #t) (kind 0)))) (hash "00rcf5s4jmfgysbzrhc908qy77a9ka4n2qmrqrvhg58yhh74daiv")))

(define-public crate-pngdefry-0.0.4 (crate (name "pngdefry") (vers "0.0.4") (deps (list (crate-dep (name "bindgen") (req "^0.69.4") (default-features #t) (kind 1)) (crate-dep (name "cc") (req "^1.0.83") (features (quote ("parallel"))) (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2.152") (default-features #t) (kind 0)))) (hash "1b67x5bdisqjgfdwnn4qf3gxfkb2cj07p5m1jwg06gdy2y3b01b3")))

(define-public crate-pngdefry-0.0.5 (crate (name "pngdefry") (vers "0.0.5") (deps (list (crate-dep (name "bindgen") (req "^0.69.4") (default-features #t) (kind 1)) (crate-dep (name "cc") (req "^1.0.83") (features (quote ("parallel"))) (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2.152") (default-features #t) (kind 0)))) (hash "1pzzk58b19sl4d91fhrzhagvxhyvhs9zszrd8ck5zjlwvl9nzgbb")))

(define-public crate-pngdefry-0.0.6 (crate (name "pngdefry") (vers "0.0.6") (deps (list (crate-dep (name "bindgen") (req "^0.69.4") (default-features #t) (kind 1)) (crate-dep (name "cc") (req "^1.0.83") (features (quote ("parallel"))) (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2.152") (default-features #t) (kind 0)))) (hash "14vzc391lzklbj79vvnmx9q44qi0cfww8qy5cfazw6hpwmkgdp6z")))

