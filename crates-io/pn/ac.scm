(define-module (crates-io pn ac) #:use-module (crates-io))

(define-public crate-pnacl-build-helper-1 (crate (name "pnacl-build-helper") (vers "1.0.0") (hash "1ny2vfcacwfzkqj3as2c5nn1vm0bi6jbjfywj0bbfn4bjwb41dqf")))

(define-public crate-pnacl-build-helper-1 (crate (name "pnacl-build-helper") (vers "1.1.0") (hash "0f39ni052yf1i3kydl0bfjdjni6vb5pxim7v7vxsl6rg2lg7wipn")))

(define-public crate-pnacl-build-helper-1 (crate (name "pnacl-build-helper") (vers "1.2.0") (hash "1m5m3spmgkmc7advr91g4sh82qqyjbf7j3yjyh27y5hrrlhqy8zn")))

(define-public crate-pnacl-build-helper-1 (crate (name "pnacl-build-helper") (vers "1.3.0") (hash "0yjlsmgpyna8yh08g03ikw3hr11gsfipbdpvs9bhn7lz2q8pg3wd")))

(define-public crate-pnacl-build-helper-1 (crate (name "pnacl-build-helper") (vers "1.3.1") (hash "1sjg3vaywak9qm1gavg5fc8iaqja89pcv6yx4v0b0vcy823whhm3")))

(define-public crate-pnacl-build-helper-1 (crate (name "pnacl-build-helper") (vers "1.3.2") (hash "15mvxmicxzscg3zhpjzh8cvwd7j3mz5l30hcwhjqzvl4ynca48vw")))

(define-public crate-pnacl-build-helper-1 (crate (name "pnacl-build-helper") (vers "1.4.0") (deps (list (crate-dep (name "tempdir") (req "*") (default-features #t) (kind 0)))) (hash "1np1akkjd47d1djlfsc9csk0i4p51n09z2qpdq1rqbrgadd134ah")))

(define-public crate-pnacl-build-helper-1 (crate (name "pnacl-build-helper") (vers "1.4.1") (deps (list (crate-dep (name "tempdir") (req "*") (default-features #t) (kind 0)))) (hash "0hb1fv4kjvg2x4x2f26r5scfwx2lg80h674z5jkzh1f48b303k2d")))

(define-public crate-pnacl-build-helper-1 (crate (name "pnacl-build-helper") (vers "1.4.2") (deps (list (crate-dep (name "tempdir") (req "*") (default-features #t) (kind 0)))) (hash "1dx39qqy67350pj328h321xp3npicgwa3shbyjlhhmrx81601mgj")))

(define-public crate-pnacl-build-helper-1 (crate (name "pnacl-build-helper") (vers "1.4.3") (deps (list (crate-dep (name "tempdir") (req "*") (default-features #t) (kind 0)))) (hash "0rsyafvbpg2bs5c4r4whwrjiq55qr7fa92azxcpkva8af63blw6z")))

(define-public crate-pnacl-build-helper-1 (crate (name "pnacl-build-helper") (vers "1.4.4") (deps (list (crate-dep (name "tempdir") (req "*") (default-features #t) (kind 0)))) (hash "14j1lii1n3g9gk3d1df19p88y3z1fzhk3v9gk6p2v2cavy37aqmv")))

(define-public crate-pnacl-build-helper-1 (crate (name "pnacl-build-helper") (vers "1.4.5") (deps (list (crate-dep (name "tempdir") (req "*") (default-features #t) (kind 0)))) (hash "166fs4x6wg668c4nj8mzhm0c3iny2jp1bcxvam9grsf8w0f0kmb5")))

(define-public crate-pnacl-build-helper-1 (crate (name "pnacl-build-helper") (vers "1.4.6") (deps (list (crate-dep (name "tempdir") (req "*") (default-features #t) (kind 0)))) (hash "08jc7c3zbaqfl13ly00c9hn1wc9w24hav4afs2nx1dp2sc24qr95")))

(define-public crate-pnacl-build-helper-1 (crate (name "pnacl-build-helper") (vers "1.4.7") (deps (list (crate-dep (name "tempdir") (req "*") (default-features #t) (kind 0)))) (hash "0l83d0ij37rv4vwk2lnl2g248rjbaslgvmjw1piqxkhwz1z1l514")))

(define-public crate-pnacl-build-helper-1 (crate (name "pnacl-build-helper") (vers "1.4.8") (deps (list (crate-dep (name "tempdir") (req "*") (default-features #t) (kind 0)))) (hash "13ms355s93j72gsr18m493qy2k415nacicvdrxc6lvfg81jznvs5")))

(define-public crate-pnacl-build-helper-1 (crate (name "pnacl-build-helper") (vers "1.4.9") (deps (list (crate-dep (name "tempdir") (req "*") (default-features #t) (kind 0)))) (hash "1py87ab9v5j20rvbsb82p9kidl7vis2g93a16lray2vk2g2wjwhg")))

(define-public crate-pnacl-build-helper-1 (crate (name "pnacl-build-helper") (vers "1.4.10") (deps (list (crate-dep (name "tempdir") (req "*") (default-features #t) (kind 0)))) (hash "12qiy460x4h93vp83061p6d99dlbnp5jzmj3fh04ba5f64fj7jb1")))

(define-public crate-pnacl-build-helper-1 (crate (name "pnacl-build-helper") (vers "1.4.11") (deps (list (crate-dep (name "tempdir") (req "^0.3.5") (default-features #t) (kind 0)) (crate-dep (name "walkdir") (req "^1") (default-features #t) (kind 0)))) (hash "145hxz3m3sg8mm9sfqqqaarnna43v65l6whwswrvcvy0fzp17gnz")))

