(define-module (crates-io pn ge) #:use-module (crates-io))

(define-public crate-pngenc-0.1 (crate (name "pngenc") (vers "0.1.0") (deps (list (crate-dep (name "atools") (req "^0.1.1") (default-features #t) (kind 0)) (crate-dep (name "crc32c") (req "^0.6.5") (default-features #t) (kind 0)) (crate-dep (name "crc32fast") (req "^1.4") (features (quote ("nightly"))) (default-features #t) (kind 0)) (crate-dep (name "png") (req "^0.17") (default-features #t) (kind 2)) (crate-dep (name "simd-adler32") (req "^0.3.7") (default-features #t) (kind 0)))) (hash "08ps4a9q3ghx7rnpdv3vb982c5450jjd95xz1l5f31mqscxnpxx6")))

(define-public crate-pngeq-0.1 (crate (name "pngeq") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^2.11.0") (default-features #t) (kind 0)) (crate-dep (name "exoquant") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "lodepng") (req "^0.8.1") (default-features #t) (kind 0)))) (hash "1mivv567xywjvpwx5k68qf0fvpfvlpx8lzil4i0wyh4l4han80rq")))

