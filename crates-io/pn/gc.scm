(define-module (crates-io pn gc) #:use-module (crates-io))

(define-public crate-pngchat-1 (crate (name "pngchat") (vers "1.0.0") (deps (list (crate-dep (name "clap") (req "^3.1") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "crc") (req "^3.0") (default-features #t) (kind 0)))) (hash "1sa1vcp7lfzs2f5syb8w412lxf3wacwpb9sh3snq23vkdz950pvj")))

(define-public crate-pngchat-1 (crate (name "pngchat") (vers "1.0.1") (deps (list (crate-dep (name "clap") (req "^3.1") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "crc") (req "^3.0") (default-features #t) (kind 0)))) (hash "0435zzprqv0pi27hls1ih1qbsvjsnd841219jyqpxicmdky3ccy5")))

(define-public crate-pngcrush-0.1 (crate (name "pngcrush") (vers "0.1.0") (deps (list (crate-dep (name "reqwest") (req "^0.9.20") (default-features #t) (kind 0)))) (hash "0fgw64pmak17640zan6i35dfmi2kvbqjpr373x0rpigmk0w6020k")))

(define-public crate-pngcrush-0.1 (crate (name "pngcrush") (vers "0.1.1") (deps (list (crate-dep (name "reqwest") (req "^0.9.20") (default-features #t) (kind 0)))) (hash "1bjn75l2ijfz0d2h91x7nfih0s2vfqbilq7rp79l6pr46408bdma")))

