(define-module (crates-io pn g_) #:use-module (crates-io))

(define-public crate-png_color_converter-0.1 (crate (name "png_color_converter") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^4.5.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "png") (req "^0.17.11") (default-features #t) (kind 0)))) (hash "0rlag4cfha0l152fhn27nhsxfwag1fl7y235gn6vcf3jdfsj54ha")))

(define-public crate-png_ect-1 (crate (name "png_ect") (vers "1.0.0") (deps (list (crate-dep (name "rayon") (req "^1.8.0") (default-features #t) (kind 0)) (crate-dep (name "walkdir") (req "^2.4.0") (default-features #t) (kind 0)))) (hash "016g1h1hl3g0ikdf47arl1rzlb2ih33yc67gl5ca4bsah79mgx3z")))

(define-public crate-png_encode_mini-0.1 (crate (name "png_encode_mini") (vers "0.1.0") (hash "13xrc3rfzqsbyncz85rnb75gkzk93cqpi5wzwh250ppb8az1i7s0")))

(define-public crate-png_encode_mini-0.1 (crate (name "png_encode_mini") (vers "0.1.1") (hash "0nwxvxywibflpl1pmp4khbm533kc24yn47dbqp2yskicdygiz85v")))

(define-public crate-png_encode_mini-0.1 (crate (name "png_encode_mini") (vers "0.1.2") (hash "0rpn91i8rkkgs607aq1fnkvali8b8zf56b2wj6cryrqsx9ifjshi")))

(define-public crate-png_filters-0.1 (crate (name "png_filters") (vers "0.1.0") (deps (list (crate-dep (name "getrandom") (req "^0.2") (default-features #t) (kind 2)))) (hash "0bm6n1f2c3gp5cjan8r85ycj7h2whcg7bbhryq59xwc1dig5rgkx")))

(define-public crate-png_filters-0.1 (crate (name "png_filters") (vers "0.1.1") (deps (list (crate-dep (name "getrandom") (req "^0.2") (default-features #t) (kind 2)))) (hash "19gsw6yha79b4dsm88lm1w7bddcgcz5lwqc62ck3cnjp2y15arij")))

(define-public crate-png_filters-0.1 (crate (name "png_filters") (vers "0.1.2") (deps (list (crate-dep (name "getrandom") (req "^0.2") (default-features #t) (kind 2)))) (hash "1l6xqlj1csmhklicm928izq26am335jn5izxsmllx8mdkfw8dbdx")))

(define-public crate-png_pong-0.0.1 (crate (name "png_pong") (vers "0.0.1") (deps (list (crate-dep (name "deflate") (req "^0.7") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "inflate") (req "^0.4") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "pix") (req "^0.6") (default-features #t) (kind 0)))) (hash "163d258fb4cisxx1gp5k4fdc57cv6npm27n4wkxwfsib34f5s28f") (features (quote (("flate" "deflate" "inflate") ("default" "flate"))))))

(define-public crate-png_pong-0.0.2 (crate (name "png_pong") (vers "0.0.2") (deps (list (crate-dep (name "miniz_oxide") (req "^0.3") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "pix") (req "^0.6") (default-features #t) (kind 0)))) (hash "13jrrilx8yr5pxqswah4wdsx459wkr78f6q3a43kqvjqbkvy209k") (features (quote (("flate" "miniz_oxide") ("default" "flate"))))))

(define-public crate-png_pong-0.1 (crate (name "png_pong") (vers "0.1.0") (deps (list (crate-dep (name "miniz_oxide") (req "^0.3") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "pix") (req "^0.7") (default-features #t) (kind 0)))) (hash "0hyy2mnn6mbpkqx8kabxwskjhiah7gqnpyj3gkr07acziyqpl0ll") (features (quote (("flate" "miniz_oxide") ("default" "flate"))))))

(define-public crate-png_pong-0.2 (crate (name "png_pong") (vers "0.2.0") (deps (list (crate-dep (name "miniz_oxide") (req "^0.3") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "pix") (req "^0.10") (default-features #t) (kind 0)))) (hash "1fxnidpnr452cfl0nvs7dnhy7dxs96z90d1bazrq2cmbj56dswfn") (features (quote (("flate" "miniz_oxide") ("default" "flate"))))))

(define-public crate-png_pong-0.2 (crate (name "png_pong") (vers "0.2.1") (deps (list (crate-dep (name "miniz_oxide") (req "^0.3") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "pix") (req "^0.10") (default-features #t) (kind 0)))) (hash "01nn6fysg0z71xhspp2bwndxip3n4xja75nkjwiaddzh3bhx7fdl") (features (quote (("flate" "miniz_oxide") ("docs-rs") ("default" "flate"))))))

(define-public crate-png_pong-0.2 (crate (name "png_pong") (vers "0.2.2") (deps (list (crate-dep (name "miniz_oxide") (req "^0.3") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "pix") (req "^0.10") (default-features #t) (kind 0)))) (hash "028vngx7sapdri2vyha5a6hj9izvagmv8yaasxn0krn41xnsm7kl") (features (quote (("flate" "miniz_oxide") ("docs-rs") ("default" "flate"))))))

(define-public crate-png_pong-0.3 (crate (name "png_pong") (vers "0.3.0") (deps (list (crate-dep (name "miniz_oxide") (req "^0.3") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "pix") (req "^0.11") (default-features #t) (kind 0)))) (hash "1w1kd9rx9rvk3p1jhc4i887v536inv24g7fsvsa4l1ab4x50n558") (features (quote (("flate" "miniz_oxide") ("docs-rs") ("default" "flate"))))))

(define-public crate-png_pong-0.4 (crate (name "png_pong") (vers "0.4.0") (deps (list (crate-dep (name "miniz_oxide") (req "^0.3") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "pix") (req "^0.12") (default-features #t) (kind 0)))) (hash "1plp9z2b03rmzjyigklnyb902jy9cpxd1sbbkba29w69rx0qn022") (features (quote (("flate" "miniz_oxide") ("docs-rs") ("default" "flate"))))))

(define-public crate-png_pong-0.5 (crate (name "png_pong") (vers "0.5.0") (deps (list (crate-dep (name "miniz_oxide") (req "^0.3") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "pix") (req "^0.13") (default-features #t) (kind 0)))) (hash "11b586jf43fhmqml8zakzzzrjw95jdxl03x9ildn3bgzp5kaji55") (features (quote (("flate" "miniz_oxide") ("docs-rs") ("default" "flate"))))))

(define-public crate-png_pong-0.6 (crate (name "png_pong") (vers "0.6.0") (deps (list (crate-dep (name "criterion") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "miniz_oxide") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "pix") (req "^0.13") (default-features #t) (kind 0)))) (hash "07b7s76lg5r3pd0r22hh9w5l32y6n991yfrp8s4wji7315swrkj8") (features (quote (("default"))))))

(define-public crate-png_pong-0.7 (crate (name "png_pong") (vers "0.7.0") (deps (list (crate-dep (name "criterion") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "miniz_oxide") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "pix") (req "^0.13") (default-features #t) (kind 0)))) (hash "1qymhfm1bvq54nfykhhizqc2li70vmdxbspv68z6qhdgajj6pxyl") (features (quote (("default"))))))

(define-public crate-png_pong-0.8 (crate (name "png_pong") (vers "0.8.0") (deps (list (crate-dep (name "criterion") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "miniz_oxide") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "pix") (req "^0.13") (default-features #t) (kind 0)))) (hash "1zvh5cwwwwfajnq05r86jyqvqmsmw80gmqh3kq3p7m0v118131bm") (features (quote (("default"))))))

(define-public crate-png_pong-0.8 (crate (name "png_pong") (vers "0.8.1") (deps (list (crate-dep (name "criterion") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "miniz_oxide") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "pix") (req "^0.13") (default-features #t) (kind 0)))) (hash "14nk2sfs6gyqgpz4hsq274sjx2rsww0b2gbihnglh7f9d9c9vajs") (features (quote (("default"))))))

(define-public crate-png_pong-0.8 (crate (name "png_pong") (vers "0.8.2") (deps (list (crate-dep (name "criterion") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "miniz_oxide") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "pix") (req "^0.13") (default-features #t) (kind 0)))) (hash "13vpcxnr2dcijjl08yv469sl8xd72n82p5w1iv8zw1lll4fnrdrj") (features (quote (("default"))))))

(define-public crate-png_pong-0.9 (crate (name "png_pong") (vers "0.9.0") (deps (list (crate-dep (name "criterion") (req "^0.5") (default-features #t) (kind 2)) (crate-dep (name "miniz_oxide") (req "^0.7") (features (quote ("simd"))) (default-features #t) (kind 0)) (crate-dep (name "pix") (req "^0.13") (default-features #t) (kind 0)) (crate-dep (name "simd-adler32") (req "^0.3") (default-features #t) (kind 0)))) (hash "01ksn6graaas3p2q546fdhdh41x8fg5ha48wify2f2mw1k4k35na") (features (quote (("default")))) (rust-version "1.77")))

(define-public crate-png_pong-0.9 (crate (name "png_pong") (vers "0.9.1") (deps (list (crate-dep (name "criterion") (req "^0.5") (default-features #t) (kind 2)) (crate-dep (name "miniz_oxide") (req "^0.7") (features (quote ("simd"))) (default-features #t) (kind 0)) (crate-dep (name "pix") (req "^0.13") (default-features #t) (kind 0)) (crate-dep (name "simd-adler32") (req "^0.3") (default-features #t) (kind 0)))) (hash "1zgzkbz11plxk8gqyix5q4lll8bqbak8b6i3syj36q6k61rmmaix") (features (quote (("default")))) (rust-version "1.70")))

