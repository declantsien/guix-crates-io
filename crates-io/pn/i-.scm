(define-module (crates-io pn i-) #:use-module (crates-io))

(define-public crate-pni-sdk-0.1 (crate (name "pni-sdk") (vers "0.1.0") (deps (list (crate-dep (name "crc16") (req "^0.4.0") (default-features #t) (kind 0)) (crate-dep (name "derive_more") (req "^0.99.17") (default-features #t) (kind 0)) (crate-dep (name "serialport") (req "^4.3.0") (default-features #t) (kind 0)))) (hash "0521szycafni9ys1nwx8lwpm8yw6vgpf16wv4m0in1z4574d9zgi") (features (quote (("reserved"))))))

