(define-module (crates-io at #{4_}#) #:use-module (crates-io))

(define-public crate-at4_protocol-0.1 (crate (name "at4_protocol") (vers "0.1.0") (deps (list (crate-dep (name "crc16") (req "^0.4.0") (default-features #t) (kind 0)))) (hash "0iy0rkpky36qzb2hmmkxldlswvzqmhc6pc9sxclr73qp7y0y9v6s") (yanked #t)))

(define-public crate-at4_protocol-0.1 (crate (name "at4_protocol") (vers "0.1.1") (deps (list (crate-dep (name "crc16") (req "^0.4.0") (default-features #t) (kind 0)))) (hash "0l07rzskbsgwhnbqcfwwxg7rg988k7ziicsnagxi9bgm2l4jhgfp") (yanked #t)))

(define-public crate-at4_protocol-0.1 (crate (name "at4_protocol") (vers "0.1.2") (deps (list (crate-dep (name "crc16") (req "^0.4.0") (default-features #t) (kind 0)))) (hash "1iji8d4q5szjc4sl936mv4jn4ypvry7n7rjkv5xx2472x9h8dmrq") (yanked #t)))

(define-public crate-at4_protocol-0.1 (crate (name "at4_protocol") (vers "0.1.3") (deps (list (crate-dep (name "crc16") (req "^0.4.0") (default-features #t) (kind 0)))) (hash "00wm0cbj7qch9b72lanqvrz99s1rkdhhczgnvc3d2vfsq7rv3w52") (yanked #t)))

(define-public crate-at4_protocol-1 (crate (name "at4_protocol") (vers "1.0.0") (deps (list (crate-dep (name "crc16") (req "^0.4.0") (default-features #t) (kind 0)))) (hash "1azj3m86hgjb2xl9rvx4j6nbv2vxwwc9fic4c5vh7cbv6vb5dw09") (yanked #t)))

(define-public crate-at4_protocol-1 (crate (name "at4_protocol") (vers "1.0.1") (deps (list (crate-dep (name "crc16") (req "^0.4.0") (default-features #t) (kind 0)))) (hash "19yaj2vw0rlg12xig78phdr295afjgl2ya5nci0z842zxy44hc9q") (yanked #t)))

(define-public crate-at4_protocol-2 (crate (name "at4_protocol") (vers "2.0.0") (deps (list (crate-dep (name "crc16") (req "^0.4.0") (default-features #t) (kind 0)))) (hash "02rfkfnwl5ng6kbyjc021s8ipkmlk7xnd4yx84ffhfzly7dg06iy") (yanked #t)))

(define-public crate-at4_protocol-2 (crate (name "at4_protocol") (vers "2.0.1") (deps (list (crate-dep (name "crc16") (req "^0.4.0") (default-features #t) (kind 0)))) (hash "0d0mqr0jgq4nsr5ykv5dwzalknwlj2nxsj06kaslrqg41a9qyc1n") (yanked #t)))

(define-public crate-at4_protocol-2 (crate (name "at4_protocol") (vers "2.1.0") (deps (list (crate-dep (name "crc16") (req "^0.4.0") (default-features #t) (kind 0)))) (hash "0prqsn6xndf6pyzrf0j2ky6lz0y118c96i1qgpzigajy7ia0rd79") (yanked #t)))

(define-public crate-at4_protocol-2 (crate (name "at4_protocol") (vers "2.1.1") (deps (list (crate-dep (name "crc16") (req "^0.4.0") (default-features #t) (kind 0)))) (hash "0brbk6kgjav1xiify76nszcci6lz5q9hf9sscpkb9md6igmg66v2")))

