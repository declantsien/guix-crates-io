(define-module (crates-io at ro) #:use-module (crates-io))

(define-public crate-atrocious_sort-0.1 (crate (name "atrocious_sort") (vers "0.1.0") (hash "1i2d8w3p8b5c2nnx4bnbcgzzbfn8rj6lkqnxc4mnmpqps7ima4ag")))

(define-public crate-atrocious_sort-0.1 (crate (name "atrocious_sort") (vers "0.1.1") (hash "174n3ysv5vvv9gp1zaf0sj7v9w7hin4kb3f8gkajifi44xlmskks")))

(define-public crate-atrocious_sort-0.1 (crate (name "atrocious_sort") (vers "0.1.2") (hash "0qylxrgrn9hcsg98mwvssn4rhw73b2x9nrvkwzl8fbs8abzpfxfx")))

