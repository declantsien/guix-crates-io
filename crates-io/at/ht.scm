(define-module (crates-io at ht) #:use-module (crates-io))

(define-public crate-athtool-0.1 (crate (name "athtool") (vers "0.1.0") (deps (list (crate-dep (name "base32") (req "^0.3.1") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^2.1.2") (default-features #t) (kind 0)) (crate-dep (name "oath") (req "^0.1.4") (default-features #t) (kind 0)) (crate-dep (name "toml") (req "^0.1.27") (kind 0)))) (hash "0b03p7xsvaqdz4snnhkhbs56yp3bgp7m7hwidsmzmnvdnszpxpi8")))

