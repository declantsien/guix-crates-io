(define-module (crates-io at p_) #:use-module (crates-io))

(define-public crate-atp_fire-0.1 (crate (name "atp_fire") (vers "0.1.0") (hash "1i6f3mniwjcv7xazhdf5qqs0la1h2xw09nigk9f5y52i41lvg00k")))

(define-public crate-atp_id-0.1 (crate (name "atp_id") (vers "0.1.0") (hash "00zd1fnf0v8jnq66hymyj63fjsyf2c77lm2f2kscw1fq57c870sr")))

(define-public crate-atp_repo-0.1 (crate (name "atp_repo") (vers "0.1.0") (hash "0ipf7sy6kba8f03ag7339lp5z6888wis8acxzr009nwrf4rfm4xg")))

