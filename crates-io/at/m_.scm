(define-module (crates-io at m_) #:use-module (crates-io))

(define-public crate-atm_parser_helper-1 (crate (name "atm_parser_helper") (vers "1.0.0") (deps (list (crate-dep (name "serde") (req "^1.0.126") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.30") (default-features #t) (kind 0)))) (hash "0i4awgqfvlmwa83iqig7ap5ngchdgi10215v1kn09r3c9zqh0pwh")))

(define-public crate-atm_parser_helper_common_syntax-1 (crate (name "atm_parser_helper_common_syntax") (vers "1.0.0") (deps (list (crate-dep (name "arbitrary") (req "^1.0.3") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "atm_parser_helper") (req "^1.0.0") (default-features #t) (kind 0)) (crate-dep (name "pretty_dtoa") (req "^0.3.0") (default-features #t) (kind 0)))) (hash "1wizrmsxg3ax7n71d05ajb7qk8sqsn9cwwi139fflbp9fk6fsnqi") (features (quote (("default" "arbitrary"))))))

(define-public crate-atm_parser_helper_common_syntax-2 (crate (name "atm_parser_helper_common_syntax") (vers "2.0.0") (deps (list (crate-dep (name "arbitrary") (req "^1.0.3") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "atm_parser_helper") (req "^1.0.0") (default-features #t) (kind 0)) (crate-dep (name "pretty_dtoa") (req "^0.3.0") (default-features #t) (kind 0)))) (hash "1y9j21kgpzqvngys5df5b3w0l91qhxbx9ph9v2h204g16awqq626")))

