(define-module (crates-io at ag) #:use-module (crates-io))

(define-public crate-atags-0.0.1 (crate (name "atags") (vers "0.0.1") (deps (list (crate-dep (name "custom_debug_derive") (req "^0.5.0") (default-features #t) (kind 0)))) (hash "1fywm9y543yfrc1ag80f9qyalch5d1lyq3bdsw7l0cidpr5g6s7r") (features (quote (("nightly"))))))

(define-public crate-atags-0.0.2 (crate (name "atags") (vers "0.0.2") (deps (list (crate-dep (name "custom_debug_derive") (req "^0.5.0") (default-features #t) (kind 0)))) (hash "07i3bhm0nsxz3m9a6glpav8y15y0wmspzp2mp4jfjbsm6lxdya3a") (features (quote (("nightly"))))))

