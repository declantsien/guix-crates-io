(define-module (crates-io at -a) #:use-module (crates-io))

(define-public crate-at-api-rs-0.1 (crate (name "at-api-rs") (vers "0.1.0") (deps (list (crate-dep (name "reqwest") (req "^0.11") (features (quote ("json"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_repr") (req "^0.1") (default-features #t) (kind 0)))) (hash "0dz529ww9cizl9vy4z6j21743jr8p8hiy4n5p0gmhqim0ccl1q81")))

(define-public crate-at-api-rs-0.1 (crate (name "at-api-rs") (vers "0.1.1") (deps (list (crate-dep (name "reqwest") (req "^0.11") (features (quote ("json"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_repr") (req "^0.1") (default-features #t) (kind 0)))) (hash "0c9rdn3srixlg2sgcp4qk9frnqiq5qvw96sb1l4ir7fdj3p3v6fa")))

(define-public crate-at-api-rs-0.1 (crate (name "at-api-rs") (vers "0.1.2") (deps (list (crate-dep (name "reqwest") (req "^0.11") (features (quote ("json"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_repr") (req "^0.1") (default-features #t) (kind 0)))) (hash "1j5jis8hvxz2qvl2d716wzkzf6fjyb7lhv52f1808fg44q1gkr5m")))

