(define-module (crates-io at me) #:use-module (crates-io))

(define-public crate-atmega32u4-0.1 (crate (name "atmega32u4") (vers "0.1.0") (deps (list (crate-dep (name "bare-metal") (req "^0.2.3") (default-features #t) (kind 0)) (crate-dep (name "vcell") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "0bczwzi1rhc9k7sa12ykgacfaz5f9l2lkxl88d037gswpkqpx0cj")))

(define-public crate-atmega32u4-0.1 (crate (name "atmega32u4") (vers "0.1.1") (deps (list (crate-dep (name "bare-metal") (req "^0.2.3") (default-features #t) (kind 0)) (crate-dep (name "vcell") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "1aabswx4551vm994r0kayhcczqmcny924zdli5169qj6dgx4fivy")))

(define-public crate-atmega32u4-0.1 (crate (name "atmega32u4") (vers "0.1.2") (deps (list (crate-dep (name "bare-metal") (req "^0.2.3") (default-features #t) (kind 0)) (crate-dep (name "vcell") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "0xzwlqpx5adfnq3d7g45wvzmfbs8x7lyxmj9rj7q561fmyiyhjg0")))

(define-public crate-atmega32u4-0.1 (crate (name "atmega32u4") (vers "0.1.3") (deps (list (crate-dep (name "bare-metal") (req "^0.2.3") (default-features #t) (kind 0)) (crate-dep (name "vcell") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "11f5cxsqwxbk9c45wli0dylqbq2xr3krd9hcsag731dn52ks9yxb")))

(define-public crate-atmega32u4-hal-0.1 (crate (name "atmega32u4-hal") (vers "0.1.0") (deps (list (crate-dep (name "atmega32u4") (req "^0.1.3") (default-features #t) (kind 0)) (crate-dep (name "embedded-hal") (req "^0.2.1") (features (quote ("unproven"))) (default-features #t) (kind 0)))) (hash "1isq4dlxa9rms88i29l9acdnqxmnchx7pbzkjxw6264gljg4avi8") (features (quote (("docs"))))))

(define-public crate-atmega32u4-hal-0.1 (crate (name "atmega32u4-hal") (vers "0.1.1") (deps (list (crate-dep (name "atmega32u4") (req "^0.1.3") (default-features #t) (kind 0)) (crate-dep (name "embedded-hal") (req "^0.2.1") (features (quote ("unproven"))) (default-features #t) (kind 0)))) (hash "1jvfxadcc06zgf6d1rqs3a05yd5i30d0vpfw020diw5sw4brhcw8") (features (quote (("docs"))))))

(define-public crate-atmega32u4-hal-0.1 (crate (name "atmega32u4-hal") (vers "0.1.2") (deps (list (crate-dep (name "atmega32u4") (req "^0.1.3") (default-features #t) (kind 0)) (crate-dep (name "embedded-hal") (req "^0.2.1") (features (quote ("unproven"))) (default-features #t) (kind 0)))) (hash "0zc7y8hq836zv613vy8zd9fz1x12slcysdqr68ffxampihxk79nq") (features (quote (("docs"))))))

(define-public crate-atmega32u4-hal-0.1 (crate (name "atmega32u4-hal") (vers "0.1.3") (deps (list (crate-dep (name "atmega32u4") (req "^0.1.3") (default-features #t) (kind 0)) (crate-dep (name "embedded-hal") (req "^0.2.1") (features (quote ("unproven"))) (default-features #t) (kind 0)))) (hash "13zvxgin40fqf3xnr6hg0q4j9x2cifpxkhn91bq4av01glhl5mi6") (features (quote (("docs"))))))

(define-public crate-atmega32u4-hal-0.1 (crate (name "atmega32u4-hal") (vers "0.1.4") (deps (list (crate-dep (name "atmega32u4") (req "^0.1.3") (default-features #t) (kind 0)) (crate-dep (name "embedded-hal") (req "^0.2.1") (features (quote ("unproven"))) (default-features #t) (kind 0)))) (hash "1r4byhyhlv3bvkvjcs8q5cijvyfn4yhnpgynmhli23v34dnr1jib") (features (quote (("docs"))))))

