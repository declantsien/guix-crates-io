(define-module (crates-io at li) #:use-module (crates-io))

(define-public crate-atlist-rs-0.1 (crate (name "atlist-rs") (vers "0.1.0") (deps (list (crate-dep (name "rand") (req "^0.7") (default-features #t) (kind 2)) (crate-dep (name "rand_xorshift") (req "^0.2") (default-features #t) (kind 2)))) (hash "1ppww7d23d9rqxm0rkm04jjs9jyw5ns7dhqs0fvxkpii3z3v9hqy")))

(define-public crate-atlist-rs-0.2 (crate (name "atlist-rs") (vers "0.2.0") (deps (list (crate-dep (name "rand") (req "^0.8") (default-features #t) (kind 2)) (crate-dep (name "rand_xorshift") (req "^0.3") (default-features #t) (kind 2)))) (hash "11xfkvl2l9s26nwiq74jnqnnp1phmzcg1rj535f40fgcm15grn5v")))

(define-public crate-atlist-rs-0.2 (crate (name "atlist-rs") (vers "0.2.1") (deps (list (crate-dep (name "rand") (req "^0.8") (default-features #t) (kind 2)) (crate-dep (name "rand_xorshift") (req "^0.3") (default-features #t) (kind 2)))) (hash "0xajdm3d7hs2vy0drk6b24zq052035fbrvdz240q2qgbs8q2gq6b")))

