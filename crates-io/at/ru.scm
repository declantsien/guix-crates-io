(define-module (crates-io at ru) #:use-module (crates-io))

(define-public crate-atruct-0.1 (crate (name "atruct") (vers "0.1.1") (deps (list (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (default-features #t) (kind 0)))) (hash "1fmsdw7q61d3a9xc8gm1slvzm8scr3dxlybxp8qspfhgm3zp1xgl")))

(define-public crate-atruct-0.1 (crate (name "atruct") (vers "0.1.2") (deps (list (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (default-features #t) (kind 0)))) (hash "1hx2fkv7kna6csjmi4d2lcqfjm7r7q4djbzdv31ps96dwsqcd8yi")))

(define-public crate-atruct-0.1 (crate (name "atruct") (vers "0.1.3") (deps (list (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (default-features #t) (kind 0)))) (hash "1cd9hz85ivc0iv19fshyl2aj7x89avkfghncw8h2gn5j0rx82a91")))

(define-public crate-atruct-0.2 (crate (name "atruct") (vers "0.2.0") (deps (list (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "06zpxls5xb3prqh2604jxm6ai2lf9rd7k2jhngcwccpm6bfh7ryi")))

(define-public crate-atruct-0.2 (crate (name "atruct") (vers "0.2.1") (deps (list (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "1ic0rp6pm06b2fgxyrgwxsjxygd8h1saw2xw3q7mxqzz9ncyqd2i")))

(define-public crate-atruct-0.2 (crate (name "atruct") (vers "0.2.2") (deps (list (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "0af9m27pfcn4qkcgmwxyri9minii3c22ykjw9g14b4ckaxigr5kd")))

(define-public crate-atruct-0.2 (crate (name "atruct") (vers "0.2.3") (deps (list (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "1hpz7difwg2s2ddpyz5qkizj0jiq3s8cnvp5p7ydlgkv1llrgzxy")))

(define-public crate-atruct-0.2 (crate (name "atruct") (vers "0.2.4") (deps (list (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "0l87a6n7x524mkwfbyg393dd5savbyj6ka06bb32way516sdqvsj")))

(define-public crate-atruct-0.3 (crate (name "atruct") (vers "0.3.0") (deps (list (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "0bn79lfjdz8fj5xsfz3w72fhh99x1pq0qvn34dmw0agcfw91lfn9")))

(define-public crate-atruct-0.3 (crate (name "atruct") (vers "0.3.1") (deps (list (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "011hqn2vipqrxplslxmb6c49ipms73075wh9yaqdnjw78d1dfqca")))

(define-public crate-atruct-0.3 (crate (name "atruct") (vers "0.3.2") (deps (list (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "057b6ah78mn3j9aq5zpg30mny3xx3lp063iial0gz3hva7r63djd")))

