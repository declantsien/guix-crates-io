(define-module (crates-io at th) #:use-module (crates-io))

(define-public crate-attheme-0.1 (crate (name "attheme") (vers "0.1.0") (deps (list (crate-dep (name "indexmap") (req "^1.0.1") (default-features #t) (kind 0)))) (hash "1izr9h7ryr5q489pl9yzcy4q9nya0ibf8dxblaby3bqk749c865v")))

(define-public crate-attheme-0.1 (crate (name "attheme") (vers "0.1.1") (deps (list (crate-dep (name "indexmap") (req "^1.0.1") (default-features #t) (kind 0)))) (hash "0wpzq5k2iqglslxfn5a1sk6zavg2j33fv2mn2m900k6zrrxxxh12")))

(define-public crate-attheme-0.2 (crate (name "attheme") (vers "0.2.0") (deps (list (crate-dep (name "indexmap") (req "^1.0.1") (default-features #t) (kind 0)))) (hash "0kiyv12rrhlfmzrp73ncg1yds70azhrasjldkj9zr2h1hxq47x1x")))

(define-public crate-attheme-0.2 (crate (name "attheme") (vers "0.2.1") (deps (list (crate-dep (name "indexmap") (req "^1.0.1") (default-features #t) (kind 0)))) (hash "0n977wvngiqx5448i3czfyr6ml189475pllszpjs7qr9mkx2m1h6")))

(define-public crate-attheme-0.2 (crate (name "attheme") (vers "0.2.2") (deps (list (crate-dep (name "indexmap") (req "^1.0.1") (default-features #t) (kind 0)))) (hash "18kr41dz29md626016i4f024g3hnfxpmnk6k67cvzr4bkmwhhd1a")))

(define-public crate-attheme-0.3 (crate (name "attheme") (vers "0.3.0") (deps (list (crate-dep (name "indexmap") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "palette") (req "^0.4") (default-features #t) (kind 0)))) (hash "079hkyckd3n9f06y5q4mdmziagfwk9cc8shmlkvbbzx5g9w848gz") (features (quote (("graphite-theme"))))))

