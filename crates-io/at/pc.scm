(define-module (crates-io at pc) #:use-module (crates-io))

(define-public crate-atpco-types-0.1 (crate (name "atpco-types") (vers "0.1.0") (deps (list (crate-dep (name "csv") (req "^0.15.0") (default-features #t) (kind 1)) (crate-dep (name "serde") (req "^1.0") (optional #t) (default-features #t) (kind 0)))) (hash "1jqcxvcxfmvi3b6r5dg1557ggl78wrqb011wsrzh97wlr6rid5ji")))

