(define-module (crates-io at wi) #:use-module (crates-io))

(define-public crate-atwinc1500-0.1 (crate (name "atwinc1500") (vers "0.1.0") (deps (list (crate-dep (name "defmt") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "embedded-hal") (req "^0.2") (features (quote ("unproven"))) (default-features #t) (kind 0)) (crate-dep (name "embedded-hal-mock") (req "^0.8.0") (default-features #t) (kind 2)) (crate-dep (name "embedded-nal") (req "^0.6") (default-features #t) (kind 0)))) (hash "08ghy4pf1a413wj38zafac4zq78rahzf1v1mqqlcn2nad36wrmhd")))

