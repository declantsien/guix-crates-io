(define-module (crates-io at ax) #:use-module (crates-io))

(define-public crate-ataxx-0.1 (crate (name "ataxx") (vers "0.1.0") (deps (list (crate-dep (name "num-derive") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "strum") (req "^0.26") (default-features #t) (kind 0)) (crate-dep (name "strum_macros") (req "^0.26") (default-features #t) (kind 0)))) (hash "0b67m8v75qxlbs6bcqszlv7f8i279rvkzz3wxblrj22d0ibnwsbj")))

(define-public crate-ataxx-0.2 (crate (name "ataxx") (vers "0.2.0") (deps (list (crate-dep (name "num-derive") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "strum") (req "^0.26") (default-features #t) (kind 0)) (crate-dep (name "strum_macros") (req "^0.26") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0") (default-features #t) (kind 0)))) (hash "08jh7aypcb2l4scp2727l8xbcdmd7j52a1z67g0rcgsgfm1xib6h")))

