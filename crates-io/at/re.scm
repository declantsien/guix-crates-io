(define-module (crates-io at re) #:use-module (crates-io))

(define-public crate-atree-0.5 (crate (name "atree") (vers "0.5.0") (deps (list (crate-dep (name "serde") (req "^1.0") (optional #t) (default-features #t) (kind 0)))) (hash "0agmvqph4d2bb4ps0p52yl76wi2yags38ff1l448r2s7f54n3zgm")))

(define-public crate-atree-0.5 (crate (name "atree") (vers "0.5.1") (deps (list (crate-dep (name "serde") (req "^1.0") (optional #t) (default-features #t) (kind 0)))) (hash "1pmyv35if1aaa59qav1kxr9qwlbd530dq8wbmpfgc56xhydc2ajv")))

(define-public crate-atree-0.5 (crate (name "atree") (vers "0.5.2") (deps (list (crate-dep (name "serde") (req "^1.0") (optional #t) (default-features #t) (kind 0)))) (hash "161sr3n46rzl8lrlh1k54rysfbf1b91fvl3mdwy9gzxrir3p698k")))

