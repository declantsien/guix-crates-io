(define-module (crates-io at m9) #:use-module (crates-io))

(define-public crate-atm90e32-0.1 (crate (name "atm90e32") (vers "0.1.0") (deps (list (crate-dep (name "embedded-hal") (req "^1.0.0-alpha.9") (default-features #t) (kind 0)))) (hash "1439xqbg04rcxyzyp3l2n0m7j2ky6ax2ry2mkmmsc2dvdhbdqbyj") (features (quote (("std"))))))

