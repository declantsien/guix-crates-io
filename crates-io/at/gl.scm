(define-module (crates-io at gl) #:use-module (crates-io))

(define-public crate-atglib-0.1 (crate (name "atglib") (vers "0.1.0") (deps (list (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "1j937inwndqsxirg724hm5qbgpnslsf3y53ij5m4lw28q94lkvjg")))

(define-public crate-atglib-0.1 (crate (name "atglib") (vers "0.1.1") (deps (list (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "167ymys8c6pd8by31ar3nnfg51gwl7wk6mk0ly51nxir7j9pz5iq")))

(define-public crate-atglib-0.1 (crate (name "atglib") (vers "0.1.2") (deps (list (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "0m5ar9j7n1yg82xqs55h624gh98h0v80l6wb3yziwy3xiw8hhhv5")))

(define-public crate-atglib-0.1 (crate (name "atglib") (vers "0.1.3") (deps (list (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "123jhgcahdpgpmm8j072kpb0m2vcn0kf33hy58x0hmsgpmr3afag")))

(define-public crate-atglib-0.1 (crate (name "atglib") (vers "0.1.4") (deps (list (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "15wlanq0k23dwwxkw2bk8w1h4pjw1zs8f6zf8ah4zxshyy814r23") (features (quote (("with-bench"))))))

(define-public crate-atglib-0.2 (crate (name "atglib") (vers "0.2.0") (deps (list (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "0qylg6ri4nf481apca4ns9id6vz1zfk94qkc9zghjah9hwl651hc") (features (quote (("with-bench"))))))

(define-public crate-atglib-0.2 (crate (name "atglib") (vers "0.2.1") (deps (list (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.156, <1.0.172") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "001spnzrlbdw5bcc41my3rqp8xkx968qwnq41i3aj5sby7kdfjwn") (features (quote (("with-bench"))))))

(define-public crate-atglib-0.2 (crate (name "atglib") (vers "0.2.2") (deps (list (crate-dep (name "criterion") (req "^0.4") (default-features #t) (kind 2)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "0rh1al6hsmnqcdhi2qcrbcyg7j5w839pjhvjkrl2dn7yfck6j59b")))

(define-public crate-atglib-0.2 (crate (name "atglib") (vers "0.2.3") (deps (list (crate-dep (name "criterion") (req "^0.4") (default-features #t) (kind 2)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "1dfr786lzm1pfczgki3kw328jwdnidk0mffpr91qhaxqvzrz3faq")))

