(define-module (crates-io at di) #:use-module (crates-io))

(define-public crate-atdir-0.0.0 (crate (name "atdir") (vers "0.0.0") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "0vra4jxplar9amnblx49sxhrhwh2rhw8jnbh9505n6n9r8s3sfai")))

(define-public crate-atdir-0.0.1 (crate (name "atdir") (vers "0.0.1-wip") (deps (list (crate-dep (name "cbuffer") (req "^0.2.0-pre") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)))) (hash "1fbd16g9z0kxh6nkkgr7p1cywffn743jal22c6hvaz2gz0bylb45")))

