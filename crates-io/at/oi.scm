(define-module (crates-io at oi) #:use-module (crates-io))

(define-public crate-atoi-0.1 (crate (name "atoi") (vers "0.1.0") (deps (list (crate-dep (name "num-traits") (req "^0.1.39") (default-features #t) (kind 0)))) (hash "0h1nanhhh49ylp2w63i74c16i9121k9xsppxi8p5p3fk3cybdm85")))

(define-public crate-atoi-0.2 (crate (name "atoi") (vers "0.2.0") (deps (list (crate-dep (name "num-traits") (req "^0.1.39") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.3.15") (default-features #t) (kind 2)))) (hash "14ji2vgapih3a84kmspy3as047jsqkkzl9wclb7s6kijg5pv895b")))

(define-public crate-atoi-0.2 (crate (name "atoi") (vers "0.2.1") (deps (list (crate-dep (name "num-traits") (req "^0.1.39") (default-features #t) (kind 0)))) (hash "1gkzq1dsjb121s56wdbzl89mm0aiq9ib6i8gdaxjnhir88g1qvjl")))

(define-public crate-atoi-0.2 (crate (name "atoi") (vers "0.2.2") (deps (list (crate-dep (name "num-traits") (req "^0.1.39") (default-features #t) (kind 0)))) (hash "0z96cvg4sayyyhz14m35rfraqrabsgwwink274wn33bsgb0b8ln6")))

(define-public crate-atoi-0.2 (crate (name "atoi") (vers "0.2.3") (deps (list (crate-dep (name "checked") (req "^0.5.0") (default-features #t) (kind 2)) (crate-dep (name "num-traits") (req "^0.2.1") (default-features #t) (kind 0)))) (hash "146iblyx8i5csyrvp9v169g4lp89gjva0517gv4ch2ff540xr9ds")))

(define-public crate-atoi-0.2 (crate (name "atoi") (vers "0.2.4") (deps (list (crate-dep (name "checked") (req "^0.5.0") (default-features #t) (kind 2)) (crate-dep (name "num-traits") (req "^0.2.1") (default-features #t) (kind 0)))) (hash "0hqawdf05sbm54mh3jnmhsis3k5k1jxj4mqmwds4bkrvn1h3hb8h")))

(define-public crate-atoi-0.3 (crate (name "atoi") (vers "0.3.0") (deps (list (crate-dep (name "num-traits") (req "^0.2") (default-features #t) (kind 0)))) (hash "08kpzw2yb97xvc0vifcgfj86nmh29dcmk1zixy2v35dimk8pvfmi")))

(define-public crate-atoi-0.3 (crate (name "atoi") (vers "0.3.1") (deps (list (crate-dep (name "num-traits") (req "^0.2") (default-features #t) (kind 0)))) (hash "1jqs6kag1pw25d7550sfspw95ys9ks1sqj19l82v9j4z65ymliq4")))

(define-public crate-atoi-0.3 (crate (name "atoi") (vers "0.3.2") (deps (list (crate-dep (name "num-traits") (req "^0.2") (default-features #t) (kind 0)))) (hash "0izgx9vf20x6z931yy15sdcvxm6bj40y9iz5l1f5ymv8gclbgbz0")))

(define-public crate-atoi-0.3 (crate (name "atoi") (vers "0.3.3") (deps (list (crate-dep (name "criterion") (req "^0.3.3") (default-features #t) (kind 2)) (crate-dep (name "num-traits") (req "^0.2.12") (default-features #t) (kind 0)))) (hash "11aqg55yrj3rmi8469njxglyxgyxjcpzmn7rgnrjaz6mjzqpv2aw")))

(define-public crate-atoi-0.4 (crate (name "atoi") (vers "0.4.0") (deps (list (crate-dep (name "criterion") (req "^0.3.3") (default-features #t) (kind 2)) (crate-dep (name "num-traits") (req "^0.2.12") (default-features #t) (kind 0)))) (hash "19fdm7gxwp98y67ghyis841vy5ka7hc1afm9cfa69qn0bzh9cs31")))

(define-public crate-atoi-1 (crate (name "atoi") (vers "1.0.0") (deps (list (crate-dep (name "criterion") (req "^0.3.5") (default-features #t) (kind 2)) (crate-dep (name "num-traits") (req "^0.2.14") (default-features #t) (kind 0)))) (hash "13mycnr954w17lcvvbpzr4rmhl1h13cg8hq63j0rrx9g6497vifp")))

(define-public crate-atoi-2 (crate (name "atoi") (vers "2.0.0") (deps (list (crate-dep (name "criterion") (req "^0.4.0") (default-features #t) (kind 2)) (crate-dep (name "num-traits") (req "^0.2.14") (kind 0)))) (hash "0a05h42fggmy7h0ajjv6m7z72l924i7igbx13hk9d8pyign9k3gj") (features (quote (("std" "num-traits/std") ("default" "std")))) (rust-version "1.57.0")))

(define-public crate-atoi_radix10-0.0.1 (crate (name "atoi_radix10") (vers "0.0.1") (deps (list (crate-dep (name "criterion") (req "^0.3") (features (quote ("html_reports"))) (default-features #t) (kind 2)) (crate-dep (name "getrandom") (req "^0.2") (features (quote ("js"))) (default-features #t) (kind 2)) (crate-dep (name "heapless") (req "^0.7") (default-features #t) (kind 2)) (crate-dep (name "numtoa") (req "^0.2.4") (default-features #t) (kind 2)) (crate-dep (name "paste") (req "^1.0") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.8") (default-features #t) (kind 2)) (crate-dep (name "wasm-bindgen-test") (req "^0.3") (default-features #t) (kind 2)))) (hash "08bidsna8vxqqxlb5v1hlbx1nnjh92n1ndxc5a38ysixlci0m5xn") (features (quote (("simd") ("nightly") ("default"))))))

(define-public crate-atoi_simd-0.0.1 (crate (name "atoi_simd") (vers "0.0.1") (hash "0ahpcs7smm36vfx1kavxlvwvgi3dqmvnj6dzlgp0rhk1szvixzg7") (yanked #t)))

(define-public crate-atoi_simd-0.0.2 (crate (name "atoi_simd") (vers "0.0.2") (hash "1vdx8cjp3var98sh0cp8a42mwzp81sgsxdj2rs2dfsgvcwib5fhq")))

(define-public crate-atoi_simd-0.0.3 (crate (name "atoi_simd") (vers "0.0.3") (hash "0ki835qlcgp2f8vw6h14cwl9267xkx89fgz9b6kmlxv7bnb9iwhw")))

(define-public crate-atoi_simd-0.1 (crate (name "atoi_simd") (vers "0.1.0") (deps (list (crate-dep (name "criterion") (req "^0.3") (default-features #t) (kind 2)))) (hash "0xwdb7papdcx0hn6zsg3dshhwz5sn38ndi5v6w6yjs987mhrnycz")))

(define-public crate-atoi_simd-0.1 (crate (name "atoi_simd") (vers "0.1.1") (deps (list (crate-dep (name "criterion") (req "^0.3") (default-features #t) (kind 2)))) (hash "158xrgbmlyid6wwksnmcycpl147vq60989kbzlbk4gq4frfc29z4")))

(define-public crate-atoi_simd-0.2 (crate (name "atoi_simd") (vers "0.2.0") (deps (list (crate-dep (name "criterion") (req "^0.3") (default-features #t) (kind 2)))) (hash "1wadrhyrzz9k7psgj6rfl0nqbd8kpfghnmijki7bs1w48j29argy")))

(define-public crate-atoi_simd-0.2 (crate (name "atoi_simd") (vers "0.2.1") (deps (list (crate-dep (name "criterion") (req "^0.3") (default-features #t) (kind 2)))) (hash "041kq9q7bviax2gl3lh42vr4pqzqghcar431nnc2riskg2skanc4")))

(define-public crate-atoi_simd-0.3 (crate (name "atoi_simd") (vers "0.3.0") (deps (list (crate-dep (name "criterion") (req "^0.3") (default-features #t) (kind 2)))) (hash "0xq4y7y83jam5hnqvvknv55kc42rqcgic9533hkal0xjwqdyg1rg")))

(define-public crate-atoi_simd-0.4 (crate (name "atoi_simd") (vers "0.4.0") (deps (list (crate-dep (name "criterion") (req "^0.4") (features (quote ("html_reports"))) (default-features #t) (kind 2)))) (hash "1bkn99cm10ak6wblpkbc2p512vpbmqq3701z6q1qbjpx8gafrjj5")))

(define-public crate-atoi_simd-0.5 (crate (name "atoi_simd") (vers "0.5.0") (deps (list (crate-dep (name "criterion") (req "^0.4") (features (quote ("html_reports"))) (default-features #t) (kind 2)))) (hash "0213i78m4vby3br23jb05mcygm159dcfpb6jv34spqir8an90v8c") (yanked #t)))

(define-public crate-atoi_simd-0.5 (crate (name "atoi_simd") (vers "0.5.1") (deps (list (crate-dep (name "criterion") (req "^0.4") (features (quote ("html_reports"))) (default-features #t) (kind 2)))) (hash "1m02xkqyjypbqpiln2azy6l5v3p6fb9q9y29w68pcb5szdp2rk91")))

(define-public crate-atoi_simd-0.6 (crate (name "atoi_simd") (vers "0.6.0") (deps (list (crate-dep (name "criterion") (req "^0.4") (features (quote ("html_reports"))) (default-features #t) (kind 2)))) (hash "0wh80qm2w1wyf002hscvbb41g8xydm1w9pibqv7nhv8z88bpq3b0")))

(define-public crate-atoi_simd-0.6 (crate (name "atoi_simd") (vers "0.6.1") (deps (list (crate-dep (name "criterion") (req "^0.4") (features (quote ("html_reports"))) (default-features #t) (kind 2)))) (hash "00is8yb97gpm416grfgj155xhsizn0dpi68jvyap1w9xf8kbz24b")))

(define-public crate-atoi_simd-0.7 (crate (name "atoi_simd") (vers "0.7.0") (deps (list (crate-dep (name "criterion") (req "^0.4") (features (quote ("html_reports"))) (default-features #t) (kind 2)))) (hash "1dpikvrgfsz109fiyapa3zn52q6dasqwa9ddsaqi42431cpybpab")))

(define-public crate-atoi_simd-0.8 (crate (name "atoi_simd") (vers "0.8.0") (deps (list (crate-dep (name "criterion") (req "^0.4") (features (quote ("html_reports"))) (default-features #t) (kind 2)))) (hash "1m7baas0g3ll9b4n3jxwj6ygpiph7dhxc29nyxn927m9y7szhkq4")))

(define-public crate-atoi_simd-0.9 (crate (name "atoi_simd") (vers "0.9.0") (deps (list (crate-dep (name "criterion") (req "^0.4") (features (quote ("html_reports"))) (default-features #t) (kind 2)))) (hash "0snax8ivhzyb5pf3iy343jzw52b18q4nk5pa4h9kd9v8v111a6j5")))

(define-public crate-atoi_simd-0.9 (crate (name "atoi_simd") (vers "0.9.1") (deps (list (crate-dep (name "criterion") (req "^0.4") (features (quote ("html_reports"))) (default-features #t) (kind 2)))) (hash "0c538daaii95i9rdjcw1mml7vqc9zbjxx0s5wrll8mh2sd2bxc24")))

(define-public crate-atoi_simd-0.9 (crate (name "atoi_simd") (vers "0.9.2") (deps (list (crate-dep (name "criterion") (req "^0.4") (features (quote ("html_reports"))) (default-features #t) (kind 2)))) (hash "0kln5dapfvd03vw343gp1mz7yr92shh9v8rx0qdxsrrfnnfsd0c8")))

(define-public crate-atoi_simd-0.10 (crate (name "atoi_simd") (vers "0.10.0") (deps (list (crate-dep (name "criterion") (req "^0.4") (features (quote ("html_reports"))) (default-features #t) (kind 2)))) (hash "0rz5jja9ibqy8vl0a7qsln48jybij4z4sdg3n9232mb9w4qv238k")))

(define-public crate-atoi_simd-0.10 (crate (name "atoi_simd") (vers "0.10.1") (deps (list (crate-dep (name "criterion") (req "^0.4") (features (quote ("html_reports"))) (default-features #t) (kind 2)))) (hash "03rrrk2wily0nyrnl535n314pxhm2p68hzk4l0y74j23xb4zgizw")))

(define-public crate-atoi_simd-0.10 (crate (name "atoi_simd") (vers "0.10.2") (deps (list (crate-dep (name "criterion") (req "^0.4") (features (quote ("html_reports"))) (default-features #t) (kind 2)))) (hash "1nb39fpyy1lg13j33i2q6zjj6lw281n2kd6778v5gzif6y1vn6sv") (features (quote (("std") ("default" "std"))))))

(define-public crate-atoi_simd-0.10 (crate (name "atoi_simd") (vers "0.10.3") (deps (list (crate-dep (name "arrayvec") (req "^0.7") (default-features #t) (kind 2)) (crate-dep (name "criterion") (req "^0.4") (features (quote ("html_reports"))) (default-features #t) (kind 2)))) (hash "1l2l4hcqzp1izx0fry5md9hgq13pbnxcrjzj4m50mwbk92aa7qpn") (features (quote (("std" "arrayvec/std") ("default" "std"))))))

(define-public crate-atoi_simd-0.11 (crate (name "atoi_simd") (vers "0.11.0") (deps (list (crate-dep (name "arrayvec") (req "^0.7") (default-features #t) (kind 2)) (crate-dep (name "criterion") (req "^0.4") (features (quote ("html_reports"))) (default-features #t) (kind 2)))) (hash "1wp90149ic0yfib1k6lgjnzij1syxp4fvrz8h76qpdgl8dn0gjg0") (features (quote (("std" "arrayvec/std") ("default" "std")))) (yanked #t)))

(define-public crate-atoi_simd-0.12 (crate (name "atoi_simd") (vers "0.12.0") (deps (list (crate-dep (name "arrayvec") (req "^0.7") (default-features #t) (kind 2)) (crate-dep (name "criterion") (req "^0.4") (features (quote ("html_reports"))) (default-features #t) (kind 2)))) (hash "0bxkldf242z67bdf927lngn0rih7qks9v9izwnkkggyzpj072x78") (features (quote (("std" "arrayvec/std") ("default" "std"))))))

(define-public crate-atoi_simd-0.12 (crate (name "atoi_simd") (vers "0.12.1") (deps (list (crate-dep (name "arrayvec") (req "^0.7") (default-features #t) (kind 2)) (crate-dep (name "criterion") (req "^0.4") (features (quote ("html_reports"))) (default-features #t) (kind 2)))) (hash "1323ii3xgp4ivqk487nmsfb2f6qclw1nn2hl0ah9n1ixnwb73gz8") (features (quote (("std" "arrayvec/std") ("default" "std"))))))

(define-public crate-atoi_simd-0.12 (crate (name "atoi_simd") (vers "0.12.2") (deps (list (crate-dep (name "arrayvec") (req "^0.7") (default-features #t) (kind 2)) (crate-dep (name "criterion") (req "^0.4") (features (quote ("html_reports"))) (default-features #t) (kind 2)))) (hash "1cazks428wk49hp3ay39yyazq5n0q1wvb6fbpqmfrii17h7211k5") (features (quote (("std" "arrayvec/std") ("default" "std"))))))

(define-public crate-atoi_simd-0.14 (crate (name "atoi_simd") (vers "0.14.0") (deps (list (crate-dep (name "arrayvec") (req "^0.7") (default-features #t) (kind 2)) (crate-dep (name "criterion") (req "^0.5") (features (quote ("html_reports"))) (default-features #t) (kind 2)))) (hash "07y0j5l18ixjnhkas70ka9j1fkn54k73184cfsn1f1kw645dsm7s") (features (quote (("std" "arrayvec/std") ("default" "std"))))))

(define-public crate-atoi_simd-0.14 (crate (name "atoi_simd") (vers "0.14.1") (deps (list (crate-dep (name "arrayvec") (req "^0.7") (default-features #t) (kind 2)) (crate-dep (name "criterion") (req "^0.5") (features (quote ("html_reports"))) (default-features #t) (kind 2)))) (hash "12mvnxbzz7rh1ksmdhjyw6njxmxwy8zspxp7m5nf2qld96s3b9xf") (features (quote (("std" "arrayvec/std") ("default" "std"))))))

(define-public crate-atoi_simd-0.14 (crate (name "atoi_simd") (vers "0.14.2") (deps (list (crate-dep (name "arrayvec") (req "^0.7") (default-features #t) (kind 2)) (crate-dep (name "criterion") (req "^0.5") (features (quote ("html_reports"))) (default-features #t) (kind 2)))) (hash "11ggln4s504s5wd7znk2xbwzib1kgx1a4wp7b03m4gnnk71040b3") (features (quote (("std" "arrayvec/std") ("default" "std"))))))

(define-public crate-atoi_simd-0.14 (crate (name "atoi_simd") (vers "0.14.3") (deps (list (crate-dep (name "arrayvec") (req "^0.7") (default-features #t) (kind 2)) (crate-dep (name "criterion") (req "^0.5") (features (quote ("html_reports"))) (default-features #t) (kind 2)) (crate-dep (name "numtoa") (req "^0.2.4") (default-features #t) (kind 2)))) (hash "1h1pyv1viwhsqfcx2347k8lll52r7x7ybw5gisli1b0a67rkqvr3") (features (quote (("std" "arrayvec/std") ("default" "std"))))))

(define-public crate-atoi_simd-0.14 (crate (name "atoi_simd") (vers "0.14.4") (deps (list (crate-dep (name "arrayvec") (req "^0.7") (default-features #t) (kind 2)) (crate-dep (name "criterion") (req "^0.5") (features (quote ("html_reports"))) (default-features #t) (kind 2)) (crate-dep (name "numtoa") (req "^0.2.4") (default-features #t) (kind 2)))) (hash "1793lwpy30vsh8rgfp8zzmq4hhcqqwim46ndl0w561313iww94a5") (features (quote (("std" "arrayvec/std") ("default" "std"))))))

(define-public crate-atoi_simd-0.14 (crate (name "atoi_simd") (vers "0.14.5") (deps (list (crate-dep (name "arrayvec") (req "^0.7") (default-features #t) (kind 2)) (crate-dep (name "criterion") (req "^0.5") (features (quote ("html_reports"))) (default-features #t) (kind 2)) (crate-dep (name "numtoa") (req "^0.2.4") (default-features #t) (kind 2)))) (hash "0ra7x11sixfqrkzq01ikriyk7iy8kmyccjyrq6dzjp9rs38pr766") (features (quote (("std" "arrayvec/std") ("default" "std"))))))

(define-public crate-atoi_simd-0.15 (crate (name "atoi_simd") (vers "0.15.0") (deps (list (crate-dep (name "arrayvec") (req "^0.7") (default-features #t) (kind 2)) (crate-dep (name "numtoa") (req "^0.2") (default-features #t) (kind 2)))) (hash "0y53xsbnq9d1lgv16raqrm4rx5mrkx8cckr7wwbiyb030y6323hr") (features (quote (("std" "arrayvec/std") ("default" "std"))))))

(define-public crate-atoi_simd-0.15 (crate (name "atoi_simd") (vers "0.15.1") (deps (list (crate-dep (name "arrayvec") (req "^0.7") (default-features #t) (kind 2)) (crate-dep (name "numtoa") (req "^0.2") (default-features #t) (kind 2)))) (hash "1lcgg789kvlcah50f0x6h1zm0jwy2h8kml7g4fkwrjcn6jaj38f2") (features (quote (("std" "arrayvec/std") ("default" "std"))))))

(define-public crate-atoi_simd-0.15 (crate (name "atoi_simd") (vers "0.15.2") (deps (list (crate-dep (name "arrayvec") (req "^0.7") (default-features #t) (kind 2)) (crate-dep (name "numtoa") (req "^0.2") (default-features #t) (kind 2)))) (hash "0rqpb6dbn62k0h1s0ivy73bsswbxyql5zf78228as72gk7qwzncl") (features (quote (("std" "arrayvec/std") ("default" "std"))))))

(define-public crate-atoi_simd-0.15 (crate (name "atoi_simd") (vers "0.15.3") (deps (list (crate-dep (name "arrayvec") (req "^0.7") (default-features #t) (kind 2)) (crate-dep (name "numtoa") (req "^0.2") (default-features #t) (kind 2)))) (hash "09jiwr7074j73viiafdzjq9mf39idd784hfpsbf1p1dn05gbchgw") (features (quote (("std" "arrayvec/std") ("default" "std"))))))

(define-public crate-atoi_simd-0.15 (crate (name "atoi_simd") (vers "0.15.4") (deps (list (crate-dep (name "arrayvec") (req "^0.7") (default-features #t) (kind 2)) (crate-dep (name "numtoa") (req "^0.2") (default-features #t) (kind 2)))) (hash "0dhzmg66j651wpnsy1l6z2kicdfa5sjj8f9gzyycv91bjcja7yck") (features (quote (("std" "arrayvec/std") ("default" "std"))))))

(define-public crate-atoi_simd-0.15 (crate (name "atoi_simd") (vers "0.15.5") (deps (list (crate-dep (name "arrayvec") (req "^0.7") (default-features #t) (kind 2)) (crate-dep (name "numtoa") (req "^0.2") (default-features #t) (kind 2)))) (hash "1l3z63dqliiyvahjw8wdwjyrngfyxhc9rfm775syakg3qgsi9z6c") (features (quote (("std" "arrayvec/std") ("default" "std"))))))

(define-public crate-atoi_simd-0.15 (crate (name "atoi_simd") (vers "0.15.6") (deps (list (crate-dep (name "arrayvec") (req "^0.7") (default-features #t) (kind 2)) (crate-dep (name "numtoa") (req "^0.2") (default-features #t) (kind 2)))) (hash "1a98kvaqyhb1shi2c6qhvklahc7ckvpmibcy319i6g1i9xqkgq4s") (features (quote (("std" "arrayvec/std") ("default" "std"))))))

(define-public crate-atoi_simd-0.16 (crate (name "atoi_simd") (vers "0.16.0") (deps (list (crate-dep (name "arrayvec") (req "^0.7") (default-features #t) (kind 2)) (crate-dep (name "numtoa") (req "^0.2") (default-features #t) (kind 2)))) (hash "1sfvqhx7845j9629qhba9b7p71jhkd28agbqxcmi228jjvlgk427") (features (quote (("std" "arrayvec/std") ("default" "std"))))))

