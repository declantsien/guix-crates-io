(define-module (crates-io at pp) #:use-module (crates-io))

(define-public crate-atpp-0.1 (crate (name "atpp") (vers "0.1.0") (deps (list (crate-dep (name "byteorder") (req "*") (default-features #t) (kind 0)))) (hash "0m7q426p3nffdzphnnl3n0mqjg0yrs4a5240skqb01cm6060dfvl")))

(define-public crate-atpp-1 (crate (name "atpp") (vers "1.6.6") (deps (list (crate-dep (name "byteorder") (req "*") (default-features #t) (kind 0)))) (hash "088mlpfvkbvbnhdra8mxl7x0y6dq9ykv9y9579dssdx535gqmiis")))

(define-public crate-atpp-1 (crate (name "atpp") (vers "1.6.7") (deps (list (crate-dep (name "byteorder") (req "*") (default-features #t) (kind 0)))) (hash "1zm07brj4d8d7jx7zawaw4jx1bbp2hd025l1r964fx64rcy8kfn5")))

