(define-module (crates-io at a_) #:use-module (crates-io))

(define-public crate-ata_x86-0.1 (crate (name "ata_x86") (vers "0.1.0") (deps (list (crate-dep (name "bit_field") (req "^0.10.0") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.4.0") (features (quote ("spin_no_std"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.126") (kind 0)) (crate-dep (name "spin") (req "^0.9.0") (default-features #t) (kind 0)))) (hash "103krvkl50p0j2j7yl1391k10i5v3xz9gadfql60m5g1j1rcl359")))

(define-public crate-ata_x86-0.1 (crate (name "ata_x86") (vers "0.1.1") (deps (list (crate-dep (name "bit_field") (req "^0.10.0") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.4.0") (features (quote ("spin_no_std"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.126") (kind 0)) (crate-dep (name "spin") (req "^0.9.0") (default-features #t) (kind 0)))) (hash "1fgd1c0h6jlymp3r6isa4ars58xcdm74844508axv1v9jjk4g5nc")))

