(define-module (crates-io at oo) #:use-module (crates-io))

(define-public crate-atools-0.1 (crate (name "atools") (vers "0.1.0") (hash "1123k3hi5nhmlhlwdnw4gwy14j7ndlghpb9kn1yh4rrhc9iij07y")))

(define-public crate-atools-0.1 (crate (name "atools") (vers "0.1.1") (hash "1fc2a8dn6vbvbbk02gblrw2m6jmm3mzir57mzmcxf7vg0v3vqjnh")))

(define-public crate-atools-0.1 (crate (name "atools") (vers "0.1.2") (hash "0mczvgrzvmqcxmqsl6811qx6pms4yz3l9zw18v4d2jpz4avfpp5n")))

(define-public crate-atools-0.1 (crate (name "atools") (vers "0.1.3") (hash "1qps9zwyx4n065qcg0wsj4rn97ad20dfcm78ppds4djvkcj9kswq") (yanked #t)))

(define-public crate-atools-0.1 (crate (name "atools") (vers "0.1.4") (hash "0rlgwqfyws2r085jik22il8yrfxjvkxmsm90nqn49cbmyb7nihyq")))

