(define-module (crates-io at er) #:use-module (crates-io))

(define-public crate-aterm-0.1 (crate (name "aterm") (vers "0.1.0") (deps (list (crate-dep (name "lazy_static") (req "^0.2.8") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^0.2.1") (default-features #t) (kind 0)))) (hash "0cwpdnrdaiwc97yvkg74hd6z1yc074p9sljcpsvmc4ipm4mh2wr4")))

(define-public crate-aterm-0.2 (crate (name "aterm") (vers "0.2.0") (deps (list (crate-dep (name "lazy_static") (req "^0.2.8") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^0.2.1") (default-features #t) (kind 0)))) (hash "0f45xia3hkrnxgps2garcn23h1aqzh758c82mz9cja5v3xwy15ks")))

(define-public crate-aterm-0.3 (crate (name "aterm") (vers "0.3.0") (deps (list (crate-dep (name "lazy_static") (req "^0.2.8") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^0.2.1") (default-features #t) (kind 0)))) (hash "1xiidnffxcg6jiw9zwadz1mlsdh6w4mbzva1b3v6q42ix9p2v472")))

(define-public crate-aterm-0.4 (crate (name "aterm") (vers "0.4.0") (deps (list (crate-dep (name "lazy_static") (req "^0.2.8") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^0.2.1") (default-features #t) (kind 0)))) (hash "13pll3vcs7swgwfsjmhrrybw6510fj8vw2hmj25pi45ac2gvcynq")))

(define-public crate-aterm-0.5 (crate (name "aterm") (vers "0.5.0") (deps (list (crate-dep (name "lazy_static") (req "^0.2.8") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^0.2.1") (default-features #t) (kind 0)))) (hash "03f8wkamx8d7yj45zq0s2sxk3ghb1si3pcz6k3jrp0alrkj50fcj")))

(define-public crate-aterm-0.6 (crate (name "aterm") (vers "0.6.0") (deps (list (crate-dep (name "lazy_static") (req "^0.2.8") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^0.2.1") (default-features #t) (kind 0)))) (hash "1h3z56k8n43x0bhmsq1lb5lqcgzc586ax73620iigh3v20nfj9vl")))

(define-public crate-aterm-0.7 (crate (name "aterm") (vers "0.7.0") (deps (list (crate-dep (name "lazy_static") (req "^0.2.8") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^0.2.1") (default-features #t) (kind 0)))) (hash "0mhyla5xf4pakr094b1pmcxh5ji32jspvs6js8ihqi30q5cd0l9w")))

(define-public crate-aterm-0.7 (crate (name "aterm") (vers "0.7.1") (deps (list (crate-dep (name "lazy_static") (req "^0.2.8") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^0.2.1") (default-features #t) (kind 0)))) (hash "0cw8nmfld8gxi1f4pvps89lvxh57rwyii8g1yvcwapl3pk255scx")))

(define-public crate-aterm-0.8 (crate (name "aterm") (vers "0.8.0") (deps (list (crate-dep (name "lazy_static") (req "^0.2.8") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^0.2.1") (default-features #t) (kind 0)))) (hash "1flwjvhlg7a12jx5nf3w8hjm3ds5kg0xiq89h2hngyypvsg9q381")))

(define-public crate-aterm-0.9 (crate (name "aterm") (vers "0.9.0") (deps (list (crate-dep (name "lazy_static") (req "^0.2.8") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^0.2.1") (default-features #t) (kind 0)))) (hash "1dgc3zpwkk9lv6a7mwk78ya125zz1dx4awlwjr3iws2ipvr699i3")))

(define-public crate-aterm-0.10 (crate (name "aterm") (vers "0.10.0") (deps (list (crate-dep (name "lazy_static") (req "^0.2.8") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^0.2.1") (default-features #t) (kind 0)))) (hash "0z08yw35x5n3pjx9hcxvrchf80gq10sc0wgkbhvr0p4irgrm9cz5")))

(define-public crate-aterm-0.10 (crate (name "aterm") (vers "0.10.1") (deps (list (crate-dep (name "lazy_static") (req "^0.2.8") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^0.2.1") (default-features #t) (kind 0)))) (hash "1834jlw5vz5w6qsivj5npcjdmhp6nhgs2qf7jdimm3p40r9a0i6k")))

(define-public crate-aterm-0.11 (crate (name "aterm") (vers "0.11.0") (deps (list (crate-dep (name "lazy_static") (req "^0.2.8") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^0.2.1") (default-features #t) (kind 0)))) (hash "1ihqlsys5maskx5655dwhymqj1fgq6kb3gqmdz7367080j1nbrjb") (yanked #t)))

(define-public crate-aterm-0.11 (crate (name "aterm") (vers "0.11.1") (deps (list (crate-dep (name "lazy_static") (req "^0.2.8") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^0.2.1") (default-features #t) (kind 0)))) (hash "0i5xxalsiwcfdijkfv70q0ssvk3vrl2gbs1a8zxd9zrhqnj4al6b") (yanked #t)))

(define-public crate-aterm-0.12 (crate (name "aterm") (vers "0.12.0") (deps (list (crate-dep (name "lazy_static") (req "^0.2.8") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^0.2.1") (default-features #t) (kind 0)))) (hash "10jgszrsnfqwakhcidnf4hyiqd3lgj8szprr6xfy5llvqjd6cnpk")))

(define-public crate-aterm-0.12 (crate (name "aterm") (vers "0.12.1") (deps (list (crate-dep (name "lazy_static") (req "^0.2.8") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^0.2.1") (default-features #t) (kind 0)))) (hash "1kp69v87yd9vg82srarqicacf6yvdiyabw1p4x5d83aas5d0d5ac")))

(define-public crate-aterm-0.12 (crate (name "aterm") (vers "0.12.2") (deps (list (crate-dep (name "lazy_static") (req "^0.2.8") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^0.2.1") (default-features #t) (kind 0)))) (hash "176rdjlnizzpjk5h6459905bvakwkgnhn34vx025plf8km1vivwm")))

(define-public crate-aterm-0.12 (crate (name "aterm") (vers "0.12.3") (deps (list (crate-dep (name "lazy_static") (req "^0.2.8") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^0.2.1") (default-features #t) (kind 0)))) (hash "134ymy2cmp4dxivhz97vyr520lzivqw5g97d7ishpcmchl0g4f70")))

(define-public crate-aterm-0.12 (crate (name "aterm") (vers "0.12.4") (deps (list (crate-dep (name "lazy_static") (req "^0.2.8") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^0.2.1") (default-features #t) (kind 0)))) (hash "1vclq2gmzw8072nk4fbnz7fcpzkjjb83k5f0n3pkngixgs30j0y5")))

(define-public crate-aterm-0.13 (crate (name "aterm") (vers "0.13.0") (deps (list (crate-dep (name "lazy_static") (req "^0.2.8") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^0.2.1") (default-features #t) (kind 0)))) (hash "0cy019nn18d27r514dwqihwj8gl9kby5rm16qvn6lkkv6shfwfd2")))

(define-public crate-aterm-0.14 (crate (name "aterm") (vers "0.14.0") (deps (list (crate-dep (name "lazy_static") (req "^0.2.8") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^0.2.1") (default-features #t) (kind 0)))) (hash "1n4fk379xclplwxq89d3c202cqd5pnjl57qfar2vrqcpd7lr2pwf") (yanked #t)))

(define-public crate-aterm-0.15 (crate (name "aterm") (vers "0.15.0") (deps (list (crate-dep (name "lazy_static") (req "^0.2.8") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^0.2.1") (default-features #t) (kind 0)))) (hash "0mbxxhf14124gvw7557g4v4y772pnryga9xf1fvy1hn0g3jk4vis") (yanked #t)))

(define-public crate-aterm-0.16 (crate (name "aterm") (vers "0.16.0") (deps (list (crate-dep (name "lazy_static") (req "^0.2.8") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^0.2.1") (default-features #t) (kind 0)))) (hash "19ahagqd0vj386l05bprd88jvy6gfnx5zjvl7kyrrscsz9qn3qnz") (yanked #t)))

(define-public crate-aterm-0.17 (crate (name "aterm") (vers "0.17.0") (deps (list (crate-dep (name "fnv") (req "^1.0.5") (default-features #t) (kind 2)) (crate-dep (name "lazy_static") (req "^0.2.8") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^0.2.1") (default-features #t) (kind 0)) (crate-dep (name "typed-arena") (req "^1.3.0") (default-features #t) (kind 0)))) (hash "1hvw45cvqwmlkb76kdm0k6dfi4yr3k115k31hyqd0nsb0fqkrl3b")))

(define-public crate-aterm-0.17 (crate (name "aterm") (vers "0.17.1") (deps (list (crate-dep (name "fnv") (req "^1.0.5") (default-features #t) (kind 2)) (crate-dep (name "lazy_static") (req "^0.2.8") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^0.2.1") (default-features #t) (kind 0)) (crate-dep (name "typed-arena") (req "^1.3.0") (default-features #t) (kind 0)))) (hash "0f9hblvng374095k08g1p13d175pvxv4agsny2sjwx8yvphs6q98")))

(define-public crate-aterm-0.18 (crate (name "aterm") (vers "0.18.0") (deps (list (crate-dep (name "fnv") (req "^1.0.5") (default-features #t) (kind 2)) (crate-dep (name "lazy_static") (req "^0.2.8") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^0.2.1") (default-features #t) (kind 0)) (crate-dep (name "typed-arena") (req "^1.3.0") (default-features #t) (kind 0)))) (hash "1ks4np6qnbv9ga45fd6w8fq7qggg5vn04zykblz1wbjb34s2ykiz") (yanked #t)))

(define-public crate-aterm-0.18 (crate (name "aterm") (vers "0.18.1") (deps (list (crate-dep (name "fnv") (req "^1.0.5") (default-features #t) (kind 2)) (crate-dep (name "lazy_static") (req "^0.2.8") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^0.2.1") (default-features #t) (kind 0)) (crate-dep (name "typed-arena") (req "^1.3.0") (default-features #t) (kind 0)))) (hash "1lxzmp2a98fqs380g70dbjwz88flw2rcyhhjwxvsyr2dd7r2s3qa")))

(define-public crate-aterm-0.19 (crate (name "aterm") (vers "0.19.0") (deps (list (crate-dep (name "fnv") (req "^1.0.5") (default-features #t) (kind 2)) (crate-dep (name "lazy_static") (req "^0.2.8") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^0.2.1") (default-features #t) (kind 0)) (crate-dep (name "typed-arena") (req "^1.3.0") (default-features #t) (kind 0)))) (hash "15ihqwlb8ds46hby3cr5zf5yw5hycnldl1fv0c5p82scbm9w6b26")))

(define-public crate-aterm-0.19 (crate (name "aterm") (vers "0.19.1") (deps (list (crate-dep (name "fnv") (req "^1.0.5") (default-features #t) (kind 2)) (crate-dep (name "lazy_static") (req "^0.2.8") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^0.2.1") (default-features #t) (kind 0)) (crate-dep (name "typed-arena") (req "^1.3.0") (default-features #t) (kind 0)))) (hash "1j3fdb4pjbp9865x2fa7qbvm9d67kf5mslib2z6vjmnxnlsyrbqw")))

(define-public crate-aterm-0.20 (crate (name "aterm") (vers "0.20.0") (deps (list (crate-dep (name "fnv") (req "^1.0.5") (default-features #t) (kind 2)) (crate-dep (name "lazy_static") (req "^0.2.8") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^0.2.1") (default-features #t) (kind 0)) (crate-dep (name "typed-arena") (req "^1.3.0") (default-features #t) (kind 0)))) (hash "0zpfba749qq14sc66l9aga394pakzk5mk5jgl7321gy94j2r58ka")))

