(define-module (crates-io at ad) #:use-module (crates-io))

(define-public crate-atadb-0.1 (crate (name "atadb") (vers "0.1.0") (deps (list (crate-dep (name "app_dirs") (req "^1.2") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.5") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0") (default-features #t) (kind 0)))) (hash "06sf3fqljk6iz51zi1lsyaal07j8fhfgmn3mw3wrr1mfhvgjk34z")))

