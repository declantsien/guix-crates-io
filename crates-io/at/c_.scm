(define-module (crates-io at c_) #:use-module (crates-io))

(define-public crate-atc_mi_thermometer-0.1 (crate (name "atc_mi_thermometer") (vers "0.1.0") (deps (list (crate-dep (name "aes") (req "^0.8.4") (default-features #t) (kind 0)) (crate-dep (name "ccm") (req "^0.5.0") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.21") (features (quote ("kv" "kv_std"))) (default-features #t) (kind 0)) (crate-dep (name "uuid") (req "^1.7.0") (default-features #t) (kind 0)))) (hash "1zv6s47w9ga7676cfbsa678i6gj33a01dw0bcdr493j441by1f3m")))

