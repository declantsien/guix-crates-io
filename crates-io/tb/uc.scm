(define-module (crates-io tb uc) #:use-module (crates-io))

(define-public crate-tbuck-1 (crate (name "tbuck") (vers "1.0.0") (deps (list (crate-dep (name "chrono") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^2") (default-features #t) (kind 0)) (crate-dep (name "hashbrown") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)))) (hash "1p8sxskshsfspjswa2vfskpkbx9kz7laii76xsn2z8lym6d8j6a7")))

(define-public crate-tbuck-1 (crate (name "tbuck") (vers "1.0.1") (deps (list (crate-dep (name "chrono") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^2") (default-features #t) (kind 0)) (crate-dep (name "hashbrown") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)))) (hash "0v719hylhgag3psyw9a7qfkf9brm48h72zvrnd5zr3p3g5f9cyc7")))

(define-public crate-tbuck-1 (crate (name "tbuck") (vers "1.1.0") (deps (list (crate-dep (name "chrono") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^2") (default-features #t) (kind 0)) (crate-dep (name "hashbrown") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)))) (hash "12dkr9vv9a2rlwm53bbd3hn91avgyp2qg7lk5v8i6m160rrzyi1j")))

