(define-module (crates-io tb #{66}#) #:use-module (crates-io))

(define-public crate-tb6612fng-0.1 (crate (name "tb6612fng") (vers "0.1.0") (deps (list (crate-dep (name "defmt") (req "^0.3") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "embedded-hal") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "embedded-hal-mock") (req "^0.9") (default-features #t) (kind 2)))) (hash "0nyfkmg9pgkg86rsd5niryvhbgz17bbxz2wl1lphjww8d34d3rn6") (rust-version "1.60")))

(define-public crate-tb6612fng-0.2 (crate (name "tb6612fng") (vers "0.2.0") (deps (list (crate-dep (name "defmt") (req "^0.3") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "embedded-hal") (req "^1.0.0-rc.1") (default-features #t) (kind 0)) (crate-dep (name "embedded-hal-mock") (req "^0.10.0-rc.2") (features (quote ("eh1"))) (default-features #t) (kind 2)))) (hash "1nv0x9a8b8y03315zsznrvm9b6g56nzqn4g7fv0c7raamh5xc0xk") (rust-version "1.63")))

