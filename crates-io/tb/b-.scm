(define-module (crates-io tb b-) #:use-module (crates-io))

(define-public crate-tbb-sys-1 (crate (name "tbb-sys") (vers "1.0.0+2021.5.0") (deps (list (crate-dep (name "cc") (req "^1.0") (features (quote ("parallel"))) (default-features #t) (kind 1)) (crate-dep (name "cxx") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "cxx-build") (req "^1.0") (default-features #t) (kind 1)) (crate-dep (name "glob") (req "^0.3") (default-features #t) (kind 1)))) (hash "1cqh5xdg7234w1ikh6rlv6zglxgbbvbxsra1cp0d082k00gv5smy") (links "tbb")))

(define-public crate-tbb-sys-1 (crate (name "tbb-sys") (vers "1.0.1+2021.5.0") (deps (list (crate-dep (name "cxx") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "cxx-build") (req "^1.0") (default-features #t) (kind 1)) (crate-dep (name "glob") (req "^0.3") (default-features #t) (kind 1)))) (hash "1ndcaavx1ip0vpc2mnwyp1mk71wiq7qlcpk736288vb1wv8h5i54") (links "tbb")))

(define-public crate-tbb-sys-1 (crate (name "tbb-sys") (vers "1.0.2+2021.5.0") (deps (list (crate-dep (name "cxx") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "cxx-build") (req "^1.0") (default-features #t) (kind 1)) (crate-dep (name "glob") (req "^0.3") (default-features #t) (kind 1)))) (hash "17qdninazgy7lz48z9a6kwn5y1jz06ljcs0yczvyx1l4lh41vzi8") (links "tbb")))

(define-public crate-tbb-sys-1 (crate (name "tbb-sys") (vers "1.1.0+2021.5.0") (deps (list (crate-dep (name "cc") (req "^1.0") (features (quote ("parallel"))) (default-features #t) (kind 1)) (crate-dep (name "glob") (req "^0.3") (default-features #t) (kind 1)))) (hash "1vnqlbqawn20v4dlmsra6jfw7fc9brhz70zl5g64zwa01mly2gxy") (links "tbb")))

