(define-module (crates-io tb a-) #:use-module (crates-io))

(define-public crate-tba-openapi-rust-3 (crate (name "tba-openapi-rust") (vers "3.8.2") (deps (list (crate-dep (name "reqwest") (req "^0.11") (features (quote ("json" "multipart"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "url") (req "^2.2") (default-features #t) (kind 0)) (crate-dep (name "uuid") (req "^1.0") (features (quote ("serde"))) (default-features #t) (kind 0)))) (hash "1adkpmlf7p8jfkw2m2h2p26bjll2kd7zf5k4kq6qwpsvcvg2k275")))

