(define-module (crates-io tb yt) #:use-module (crates-io))

(define-public crate-tbytes-0.1 (crate (name "tbytes") (vers "0.1.0-alpha1") (deps (list (crate-dep (name "env_logger") (req "^0.10.1") (default-features #t) (kind 2)) (crate-dep (name "log") (req "^0.4.20") (default-features #t) (kind 2)))) (hash "0qjhylwyxn9ngsav7qvh3znydqqswikgxfhcx7amv6ybyi7r5ik5") (features (quote (("std" "alloc") ("alloc"))))))

(define-public crate-tbytes-0.1 (crate (name "tbytes") (vers "0.1.0-alpha2") (deps (list (crate-dep (name "env_logger") (req "^0.10.1") (default-features #t) (kind 2)) (crate-dep (name "log") (req "^0.4.20") (default-features #t) (kind 2)))) (hash "1flqkv7rc0mnkxy5l54mi9758cjl7bkmvgv2bkd7887v8sxydlg1") (features (quote (("std" "alloc") ("alloc"))))))

(define-public crate-tbytes-0.1 (crate (name "tbytes") (vers "0.1.0") (deps (list (crate-dep (name "env_logger") (req "^0.11.3") (default-features #t) (kind 2)) (crate-dep (name "log") (req "^0.4.21") (default-features #t) (kind 2)) (crate-dep (name "serde") (req "^1.0.197") (features (quote ("derive"))) (optional #t) (kind 0)))) (hash "0x6hzx0j5mkdz4s09hzif82vrwcfbgv8h5a36arxiwiww14dkfbd") (features (quote (("std" "alloc" "serde/std") ("alloc" "serde/alloc")))) (v 2) (features2 (quote (("serde" "dep:serde"))))))

