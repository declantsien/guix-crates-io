(define-module (crates-io tb _r) #:use-module (crates-io))

(define-public crate-tb_row-1 (crate (name "tb_row") (vers "1.0.0") (deps (list (crate-dep (name "tb_row_derive") (req "^1.0") (default-features #t) (kind 0) (package "tb_row_derive")))) (hash "02rdihxm9w0n80w1w1q88ql578368pgz7fk0qp8akay1r5y232nz")))

(define-public crate-tb_row_derive-1 (crate (name "tb_row_derive") (vers "1.0.0") (deps (list (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.152") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.91") (features (quote ("raw_value"))) (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2.0") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "1l0j8vgij0gq5p2bx9cc3riklfq61rghlj9shcrmb16wz2axksrm")))

(define-public crate-tb_row_derive-1 (crate (name "tb_row_derive") (vers "1.0.1") (deps (list (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.152") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.91") (features (quote ("raw_value"))) (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2.0") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "0v4xaax8gc1q3blhd60i5qajwsyap1zxrp0qsrr16qx9i9y38fx2")))

