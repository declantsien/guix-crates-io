(define-module (crates-io tb lt) #:use-module (crates-io))

(define-public crate-tbltyp-0.1 (crate (name "tbltyp") (vers "0.1.0") (deps (list (crate-dep (name "ansistr") (req "^0.1.1") (default-features #t) (kind 0)))) (hash "1gikinxqwnvskzjii26csmfs76i8i4yd2sdc817vk24j1vga00zd")))

(define-public crate-tbltyp-0.1 (crate (name "tbltyp") (vers "0.1.1") (deps (list (crate-dep (name "ansistr") (req "^0.1.1") (default-features #t) (kind 0)))) (hash "0kxlr0527jxww74rb3d2r67j57yqxa6wpl665aq4sks1spzd7ix4")))

