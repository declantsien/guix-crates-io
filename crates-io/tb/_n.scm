(define-module (crates-io tb _n) #:use-module (crates-io))

(define-public crate-tb_normalization-0.9 (crate (name "tb_normalization") (vers "0.9.9") (deps (list (crate-dep (name "regex") (req "^1.3.1") (default-features #t) (kind 0)) (crate-dep (name "unicode-normalization") (req "^0.1.8") (default-features #t) (kind 0)))) (hash "1kwsjwxrgdv06yy0xv9f2vxc18yv7hjvm93fhbbb63rq7znxw0gv")))

(define-public crate-tb_normalization-1 (crate (name "tb_normalization") (vers "1.0.0") (deps (list (crate-dep (name "regex") (req "^1.3.1") (default-features #t) (kind 0)) (crate-dep (name "unicode-normalization") (req "^0.1.8") (default-features #t) (kind 0)))) (hash "18kqdds3vd34pcji84ws43g1h18fwww5i2a159k9rc52xh257r1x")))

