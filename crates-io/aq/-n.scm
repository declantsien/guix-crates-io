(define-module (crates-io aq -n) #:use-module (crates-io))

(define-public crate-aq-ng-0.1 (crate (name "aq-ng") (vers "0.1.4") (deps (list (crate-dep (name "accessibility-ng") (req "^0.1.6") (default-features #t) (kind 0)) (crate-dep (name "core-foundation") (req "^0.9") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.3") (default-features #t) (kind 0)))) (hash "1zx03dvy8zipsnczp6z0h4w5qrlkc6a1l99kzzgixrji9wzr7pl1")))

