(define-module (crates-io aq t_) #:use-module (crates-io))

(define-public crate-aqt_sim-0.1 (crate (name "aqt_sim") (vers "0.1.0") (deps (list (crate-dep (name "hashbrown") (req "^0.12.3") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)))) (hash "174i8869mplja8s4ajrj6bzwk9m4v41ksyimb1zknm1vmklyhbba")))

