(define-module (crates-io n_ ar) #:use-module (crates-io))

(define-public crate-n_array-0.1 (crate (name "n_array") (vers "0.1.0") (hash "1bjzsj223l4kajfckrmwla17iwlnxgx5j5safib0w53ichbpybz1")))

(define-public crate-n_array-0.1 (crate (name "n_array") (vers "0.1.1") (hash "1834af48lamycmwwkci8d5qmy0xb76bfp1wxyz0x0yq9c95vi7jw")))

(define-public crate-n_array-0.1 (crate (name "n_array") (vers "0.1.2") (hash "0qzwrv3k519xkwbxgyqmqbwx7zqlr0dgy81slv4lc4fjqjcf8izg")))

(define-public crate-n_array-0.1 (crate (name "n_array") (vers "0.1.3") (hash "0ij334npzq0vlgcam7qciyx20b2mbxj0gf54iap8i6h1p10hpwm0")))

