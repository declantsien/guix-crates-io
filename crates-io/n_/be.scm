(define-module (crates-io n_ be) #:use-module (crates-io))

(define-public crate-n_best-0.1 (crate (name "n_best") (vers "0.1.0") (deps (list (crate-dep (name "ord_by") (req "^0.1.1") (default-features #t) (kind 0)))) (hash "1a3sxp2qijk4p4i9vzfbq8cnjfzygdzi4d37kf431yk6j5l7cg1h")))

