(define-module (crates-io i1 #{8e}#) #:use-module (crates-io))

(define-public crate-i18e-0.1 (crate (name "i18e") (vers "0.1.1") (deps (list (crate-dep (name "aok") (req "^0.1.3") (default-features #t) (kind 2)) (crate-dep (name "loginit") (req "^0.1.10") (default-features #t) (kind 2)) (crate-dep (name "static_init") (req "^1.0.3") (default-features #t) (kind 2)) (crate-dep (name "tokio") (req "^1.35.1") (features (quote ("macros" "rt" "rt-multi-thread" "time" "sync"))) (default-features #t) (kind 2)) (crate-dep (name "tracing") (req "^0.1.40") (default-features #t) (kind 0)))) (hash "04g4h3sdja4brnw1rfxxncrkrkmm3j387m996ygpsmnf9x71x68y")))

