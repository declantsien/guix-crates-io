(define-module (crates-io uf wp) #:use-module (crates-io))

(define-public crate-ufwprofile-0.1 (crate (name "ufwprofile") (vers "0.1.0") (hash "1acp4p49ws93p0n4aag2j1q8hfv13d2a93pp6wzgdkrqfdpy3kzv")))

(define-public crate-ufwprofile-0.1 (crate (name "ufwprofile") (vers "0.1.1") (hash "1im1cr5nfg1sm4nlnpfiwwmcza1ribb0w3886aciag0wphq1x4vs")))

(define-public crate-ufwprofile-0.1 (crate (name "ufwprofile") (vers "0.1.2") (hash "1f9a29g3yswhqfn822aad1ds903zlsnpzrzybbpy9lahysxh1ljb")))

(define-public crate-ufwprofile-0.2 (crate (name "ufwprofile") (vers "0.2.2") (hash "1ln9h5x8a7160g5sd0a73anwkdfr2rc11nc1068rb820pzfvlqbc")))

(define-public crate-ufwprofile-0.2 (crate (name "ufwprofile") (vers "0.2.3") (hash "058nlr1flwbybisyixbiq3m03xb7q7kgw0csfiaydbnxhk3ya5gb")))

(define-public crate-ufwprofile-0.2 (crate (name "ufwprofile") (vers "0.2.4") (hash "03jdiabfmhy2lhcz7r4kj90jmh3257b97vabyz8lz19icxx7r9ac")))

(define-public crate-ufwprofile-1 (crate (name "ufwprofile") (vers "1.3.4") (deps (list (crate-dep (name "libc") (req "^0.2.139") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.17") (default-features #t) (kind 0)) (crate-dep (name "simple_logger") (req "^4.0.0") (default-features #t) (kind 2)))) (hash "18za6apvv37kihvvjhqyym3bd6f4pdir4k91zadfadvxr1miz4df")))

(define-public crate-ufwprofile-1 (crate (name "ufwprofile") (vers "1.4.5") (deps (list (crate-dep (name "libc") (req "^0.2.139") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.17") (default-features #t) (kind 0)) (crate-dep (name "simple_logger") (req "^4.0.0") (default-features #t) (kind 2)))) (hash "0k0608g0qsf12ijgpjyn3fnqprc054fk6lplnl6d2mx5dignlm4d")))

(define-public crate-ufwprofile-1 (crate (name "ufwprofile") (vers "1.4.6") (deps (list (crate-dep (name "anyhow") (req "^1.0.77") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2.151") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.20") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.10.2") (default-features #t) (kind 0)))) (hash "1zwbmh63g9lqcvmhv8vvla78h0bjsnkjrzn421k3flhnf2k360qn")))

(define-public crate-ufwprofile-1 (crate (name "ufwprofile") (vers "1.4.7") (deps (list (crate-dep (name "anyhow") (req "^1.0.77") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2.151") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.20") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.10.2") (default-features #t) (kind 0)))) (hash "1zj6s2qlnplwjrkcjmfqzwkv0fda9b6xpva8j1kvnb2wl2f402gc")))

