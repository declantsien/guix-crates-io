(define-module (crates-io uf o_) #:use-module (crates-io))

(define-public crate-ufo_rs-0.1 (crate (name "ufo_rs") (vers "0.1.0") (deps (list (crate-dep (name "hex") (req "^0.3.1") (default-features #t) (kind 0)))) (hash "1cac532042f35gswkjrzyb0ycrvr9gg4xhrjq5788x097fgvf75a")))

(define-public crate-ufo_rs-0.1 (crate (name "ufo_rs") (vers "0.1.1") (deps (list (crate-dep (name "hex") (req "^0.3.1") (default-features #t) (kind 0)))) (hash "0lhdx7rz1pl0nfc62cx9clls8vpblnlxzw95aq2hwl53ncszv970")))

