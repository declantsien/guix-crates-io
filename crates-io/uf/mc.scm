(define-module (crates-io uf mc) #:use-module (crates-io))

(define-public crate-ufmclient-0.1 (crate (name "ufmclient") (vers "0.1.0") (hash "0swnvrp4lywzvn32c6anq278gxn067j2na1glfcqpbh0j3c25ypa")))

(define-public crate-ufmclient-0.1 (crate (name "ufmclient") (vers "0.1.1") (deps (list (crate-dep (name "base64") (req "^0.21.0") (default-features #t) (kind 0)) (crate-dep (name "hyper") (req "^0.14") (features (quote ("full"))) (default-features #t) (kind 0)) (crate-dep (name "hyper-tls") (req "^0.5") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4") (features (quote ("std" "serde"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)))) (hash "0r41kdyg73bimfzh7rb7lc6s9828wr7vp2sjlwmhg03jsz7jh0j9")))

(define-public crate-ufmclient-0.1 (crate (name "ufmclient") (vers "0.1.2") (deps (list (crate-dep (name "base64") (req "^0.21.0") (default-features #t) (kind 0)) (crate-dep (name "hyper") (req "^0.14") (features (quote ("full"))) (default-features #t) (kind 0)) (crate-dep (name "hyper-tls") (req "^0.5") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4") (features (quote ("std" "serde"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)))) (hash "0cylxxg9vglfmsjkw6si5lqxmi9ps9lajc9nac1rnklspk7j2zi0")))

