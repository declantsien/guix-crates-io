(define-module (crates-io uf #{2-}#) #:use-module (crates-io))

(define-public crate-uf2-decode-0.1 (crate (name "uf2-decode") (vers "0.1.0") (hash "1ib3aw9lv16v11isvjkq3q59vgvvm7zijdcx4qvg0w3x784km4ld")))

(define-public crate-uf2-decode-0.2 (crate (name "uf2-decode") (vers "0.2.0") (hash "08q6n1xd9n418sshdngf6a36s35qg5ngjhr0yifs8gvyn8dd8xya")))

(define-public crate-uf2-util-0.1 (crate (name "uf2-util") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^4.4.11") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "109j1072gqq5v40gjva0ih0j8sir4frl39f0aq1vvqn180s67i6x")))

