(define-module (crates-io uf #{2_}#) #:use-module (crates-io))

(define-public crate-uf2_block-0.1 (crate (name "uf2_block") (vers "0.1.0") (deps (list (crate-dep (name "bitmask") (req "^0.5.0") (kind 0)) (crate-dep (name "packing") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "0jrsbyc5n91rspvgb7b6d4gbzwydirwq99gilx4yzkbblly7kpv8")))

