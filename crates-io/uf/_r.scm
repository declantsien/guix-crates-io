(define-module (crates-io uf _r) #:use-module (crates-io))

(define-public crate-uf_rush-0.1 (crate (name "uf_rush") (vers "0.1.0") (hash "0hy7jdqmwwfpxc5fznhg3jqpvzwikasxv2qlkwkpjbly6ch1lk43")))

(define-public crate-uf_rush-0.1 (crate (name "uf_rush") (vers "0.1.1") (hash "18sx8lvkhnx5gx131y4pqyl4xmdvcqwv8vb2vspfczifnrgxydw3")))

(define-public crate-uf_rush-0.2 (crate (name "uf_rush") (vers "0.2.0") (deps (list (crate-dep (name "rand") (req "^0.8") (default-features #t) (kind 2)))) (hash "00mf46501935jggw51yrkixq7wk8z91y4bb0mwwynhsdmgfafggk")))

(define-public crate-uf_rush-0.2 (crate (name "uf_rush") (vers "0.2.1") (deps (list (crate-dep (name "rand") (req "^0.8") (default-features #t) (kind 2)))) (hash "081d1mkik0qk9ihsdflxfifgpm0a4jqbnpbgbzida0ra04ygrqxx")))

