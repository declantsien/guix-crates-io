(define-module (crates-io uf cs) #:use-module (crates-io))

(define-public crate-ufcs-0.1 (crate (name "ufcs") (vers "0.1.0") (deps (list (crate-dep (name "futures") (req "^0.3.1") (default-features #t) (kind 2)))) (hash "0j5nqvxigjl7k2chfkszhki0yw82gka51p9vp6vxsyks4xn84ydq")))

