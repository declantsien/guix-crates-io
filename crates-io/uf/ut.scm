(define-module (crates-io uf ut) #:use-module (crates-io))

(define-public crate-ufut-0.0.0 (crate (name "ufut") (vers "0.0.0") (hash "0pzp9wf7llcgc4z1slrh7rv5h0s3h1rk8q7b22m0pbh4p7ajsiy7")))

(define-public crate-ufut-0.2 (crate (name "ufut") (vers "0.2.0") (deps (list (crate-dep (name "futures-micro") (req "=0.2.0") (default-features #t) (kind 0)))) (hash "0y9dc8j4gfmbvfkqqjz6mmkapl3iw84gingvpglzgagmpa4x4w6i")))

(define-public crate-ufut-0.3 (crate (name "ufut") (vers "0.3.0") (deps (list (crate-dep (name "futures-micro") (req "=0.3.0") (default-features #t) (kind 0)))) (hash "0jqpy50jgf5kjvc0qgj9xisqnhxgz7139qg89yzm3i7b63d1ng03")))

(define-public crate-ufut-0.3 (crate (name "ufut") (vers "0.3.1") (deps (list (crate-dep (name "futures-micro") (req "=0.3.1") (default-features #t) (kind 0)))) (hash "1pprr0mn2aj69r4v2yra9zpsmc0vdq5q42hf9h9vp38x2izxn8f3")))

