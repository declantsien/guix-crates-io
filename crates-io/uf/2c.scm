(define-module (crates-io uf #{2c}#) #:use-module (crates-io))

(define-public crate-uf2conv-0.1 (crate (name "uf2conv") (vers "0.1.0") (deps (list (crate-dep (name "byteorder") (req "^1.2") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^2.32") (default-features #t) (kind 0)) (crate-dep (name "uf2") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "1qbcr3y7g8dfrxv7jmws3574gqcqp4m9c1cjyv6xbg82774d410a")))

