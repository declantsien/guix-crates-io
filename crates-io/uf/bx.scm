(define-module (crates-io uf bx) #:use-module (crates-io))

(define-public crate-ufbx-0.1 (crate (name "ufbx") (vers "0.1.0") (deps (list (crate-dep (name "cc") (req "^1.0") (default-features #t) (kind 1)))) (hash "0v4179zvv1vwqxgksv94yqh3krm4zyp7y7mzr5bjbbl3w14ywwgm")))

(define-public crate-ufbx-0.1 (crate (name "ufbx") (vers "0.1.1") (deps (list (crate-dep (name "cc") (req "^1.0") (default-features #t) (kind 1)))) (hash "0hvp5h7mlpkjwc5dgjddf6n67r7dxdf9nwsnbba4bdlhzarcshxf")))

(define-public crate-ufbx-0.1 (crate (name "ufbx") (vers "0.1.2") (deps (list (crate-dep (name "cc") (req "^1.0") (default-features #t) (kind 1)))) (hash "1y4claj4darr2ypyyyv36iywvl1cfgi5sxd9f8sv6fygvh8phpdp")))

(define-public crate-ufbx-0.1 (crate (name "ufbx") (vers "0.1.3") (deps (list (crate-dep (name "cc") (req "^1.0") (default-features #t) (kind 1)))) (hash "0a3l1nc39avj49cag8kkxhr556205561widhjd6fkvvbiik1pmsm")))

(define-public crate-ufbx-0.2 (crate (name "ufbx") (vers "0.2.0") (deps (list (crate-dep (name "cc") (req "^1.0") (default-features #t) (kind 1)))) (hash "147fs4pkh4hh287hkdswayyjf3cfnzzv2jlf1acjs39vq41xyjjj")))

(define-public crate-ufbx-0.3 (crate (name "ufbx") (vers "0.3.0") (deps (list (crate-dep (name "cc") (req "^1.0") (default-features #t) (kind 1)))) (hash "0ly4f2y7yy11hg9x42vf54m1x7xq6cylxwpjg94lvncjjwav6ahk")))

(define-public crate-ufbx-0.4 (crate (name "ufbx") (vers "0.4.0") (deps (list (crate-dep (name "cc") (req "^1.0") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 2)) (crate-dep (name "mint") (req "^0.5.8") (optional #t) (kind 0)) (crate-dep (name "panic-message") (req "^0.3.0") (default-features #t) (kind 2)))) (hash "0744fhshsf39q4v1ay3k11hxy7cfv556h73031nq1biwjqw1kgn5") (features (quote (("nightly"))))))

(define-public crate-ufbx-0.5 (crate (name "ufbx") (vers "0.5.0-integration") (deps (list (crate-dep (name "cc") (req "^1.0") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 2)) (crate-dep (name "mint") (req "^0.5.8") (optional #t) (kind 0)) (crate-dep (name "panic-message") (req "^0.3.0") (default-features #t) (kind 2)))) (hash "01m66l3r5ybjcsf7y78ai2rlzcxfqsxcwdzwhkjp9wvccja9bbas") (features (quote (("nightly"))))))

(define-public crate-ufbx-0.5 (crate (name "ufbx") (vers "0.5.0") (deps (list (crate-dep (name "cc") (req "^1.0") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 2)) (crate-dep (name "mint") (req "^0.5.8") (optional #t) (kind 0)) (crate-dep (name "panic-message") (req "^0.3.0") (default-features #t) (kind 2)))) (hash "1bajcx9rhwhs4ws4jiigh2d85spl6vl391fgxlvg3hj613cf757q") (features (quote (("nightly"))))))

(define-public crate-ufbx-0.6 (crate (name "ufbx") (vers "0.6.0-integration") (deps (list (crate-dep (name "cc") (req "^1.0") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 2)) (crate-dep (name "mint") (req "^0.5.8") (optional #t) (kind 0)) (crate-dep (name "panic-message") (req "^0.3.0") (default-features #t) (kind 2)))) (hash "0mxk2913nlri6xb879qsngnzp9bb9klmz10b3s69rryq6h54fm68") (features (quote (("nightly"))))))

(define-public crate-ufbx-0.6 (crate (name "ufbx") (vers "0.6.0") (deps (list (crate-dep (name "cc") (req "^1.0") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 2)) (crate-dep (name "mint") (req "^0.5.8") (optional #t) (kind 0)) (crate-dep (name "panic-message") (req "^0.3.0") (default-features #t) (kind 2)))) (hash "05388478qp382hlf8lciqiv4q6mxw0sihz383k9vmxb6886kz6ki") (features (quote (("nightly"))))))

(define-public crate-ufbx-0.6 (crate (name "ufbx") (vers "0.6.1") (deps (list (crate-dep (name "cc") (req "^1.0") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 2)) (crate-dep (name "mint") (req "^0.5.8") (optional #t) (kind 0)) (crate-dep (name "panic-message") (req "^0.3.0") (default-features #t) (kind 2)))) (hash "17miw59m5s14714n6ibs9a8kfmlghzazj95pi4lgp8z3awpd827v") (features (quote (("nightly"))))))

