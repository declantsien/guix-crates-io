(define-module (crates-io uf ir) #:use-module (crates-io))

(define-public crate-ufire_ec-0.9 (crate (name "ufire_ec") (vers "0.9.0") (deps (list (crate-dep (name "byteorder") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "i2cdev") (req "^0.3.2") (default-features #t) (kind 0)))) (hash "1p03rlglqqnm5a4yqka3ygi8apag892na8pp4j68px20hgrn0hq5")))

(define-public crate-ufire_ec-0.9 (crate (name "ufire_ec") (vers "0.9.1") (deps (list (crate-dep (name "byteorder") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "i2cdev") (req "^0.3.2") (default-features #t) (kind 0)))) (hash "0339ij052kypa867h8qsx6lx5zfc0jbd5i4k07ya8qii6lcjbrgs")))

(define-public crate-ufire_ec-0.9 (crate (name "ufire_ec") (vers "0.9.2") (deps (list (crate-dep (name "byteorder") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "i2cdev") (req "^0.3.2") (default-features #t) (kind 0)))) (hash "01mb0psxc9knlw4dvdyrzr0zckxg4s4n4naamz0j20p0nvxylmac")))

(define-public crate-ufire_ec-2 (crate (name "ufire_ec") (vers "2.0.0") (deps (list (crate-dep (name "byteorder") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "i2cdev") (req "^0.3.2") (default-features #t) (kind 0)))) (hash "09ry6pn0zhps29jhcgiig1s8swjg7i956mndb9zv14slm3sllcs0")))

(define-public crate-ufire_ise-0.9 (crate (name "ufire_ise") (vers "0.9.0") (deps (list (crate-dep (name "byteorder") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "i2cdev") (req "^0.3.2") (default-features #t) (kind 0)))) (hash "1vzjywrw1ccns5j331nqfx95fyrx9b5k3j2vp4f8hppvypyi41jg")))

(define-public crate-ufire_ise-0.9 (crate (name "ufire_ise") (vers "0.9.1") (deps (list (crate-dep (name "byteorder") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "i2cdev") (req "^0.3.2") (default-features #t) (kind 0)))) (hash "16zjp0n2fqr5prm3ngy5smk5j945yh0abmnddch358c3x8bd2dcd")))

(define-public crate-ufire_ise-0.9 (crate (name "ufire_ise") (vers "0.9.2") (deps (list (crate-dep (name "byteorder") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "i2cdev") (req "^0.3.2") (default-features #t) (kind 0)))) (hash "1pdqn5wlk1rikqh1zjanh0vskr2raif54bw1r113vwiclhy6vazd")))

(define-public crate-ufire_ise-2 (crate (name "ufire_ise") (vers "2.0.0") (deps (list (crate-dep (name "byteorder") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "i2cdev") (req "^0.3.2") (default-features #t) (kind 0)))) (hash "1g53gj8ib0cm6ymjgwdj2drv6m6rkrhksjz625ma8qcv3nkl81kl")))

(define-public crate-ufire_iso_ec-0.9 (crate (name "ufire_iso_ec") (vers "0.9.0") (deps (list (crate-dep (name "byteorder") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "i2cdev") (req "^0.3.2") (default-features #t) (kind 0)) (crate-dep (name "shrust") (req "^0.0.6") (default-features #t) (kind 0)))) (hash "1aj79g176qqnshcv01mg1b6qs512s3jm3bz8qyy8xjsa1lx1138y")))

(define-public crate-ufire_iso_ec-0.9 (crate (name "ufire_iso_ec") (vers "0.9.1") (deps (list (crate-dep (name "byteorder") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "i2cdev") (req "^0.3.2") (default-features #t) (kind 0)) (crate-dep (name "shrust") (req "^0.0.6") (default-features #t) (kind 0)))) (hash "1zhiz313m1qxnfyw9i1y8lcqrhgwppbxad4s4hm3lvwjkjldwhzq")))

(define-public crate-ufire_iso_ec-0.9 (crate (name "ufire_iso_ec") (vers "0.9.2") (deps (list (crate-dep (name "byteorder") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "i2cdev") (req "^0.3.2") (default-features #t) (kind 0)) (crate-dep (name "shrust") (req "^0.0.6") (default-features #t) (kind 0)))) (hash "1gala4477bmd0kah6mhf0nc3vm9mn8299pwm64jm2f9bak25jwg0")))

(define-public crate-ufire_iso_ise-0.9 (crate (name "ufire_iso_ise") (vers "0.9.0") (deps (list (crate-dep (name "byteorder") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "i2cdev") (req "^0.3.2") (default-features #t) (kind 0)) (crate-dep (name "shrust") (req "^0.0.6") (default-features #t) (kind 0)))) (hash "0v9amrg2gnl8py2l967f4rp70678zy1sqhrjklkx3hwzjn9b8yv6")))

(define-public crate-ufire_sht20-0.9 (crate (name "ufire_sht20") (vers "0.9.0") (deps (list (crate-dep (name "byteorder") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "i2cdev") (req "^0.3.2") (default-features #t) (kind 0)) (crate-dep (name "shrust") (req "^0.0.6") (default-features #t) (kind 0)))) (hash "1z8ax8pc9nw6gyd262g7i61i68bd5vz676sql4z92mbzr8z5swqq")))

(define-public crate-ufire_sht20-0.9 (crate (name "ufire_sht20") (vers "0.9.1") (deps (list (crate-dep (name "byteorder") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "i2cdev") (req "^0.3.2") (default-features #t) (kind 0)) (crate-dep (name "shrust") (req "^0.0.6") (default-features #t) (kind 0)))) (hash "1zhdyrlbi2kcrpza5yb2l3619why3gg9l6w4xyays1nyh5vqz3sh")))

