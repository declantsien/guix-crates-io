(define-module (crates-io uf lo) #:use-module (crates-io))

(define-public crate-ufloat8-0.1 (crate (name "ufloat8") (vers "0.1.0") (hash "0x6lzhs1sl2j35ild56pm85g37czrhxmlzngxp4wq4maap75bfyg")))

(define-public crate-uflow-0.3 (crate (name "uflow") (vers "0.3.0") (deps (list (crate-dep (name "md5") (req "^0.7.0") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.8.4") (default-features #t) (kind 0)))) (hash "0z153ginnygw71l3k1ddxn4dkcvad705i9yfy2rbykp02rv001b3") (yanked #t)))

(define-public crate-uflow-0.3 (crate (name "uflow") (vers "0.3.1") (deps (list (crate-dep (name "md5") (req "^0.7.0") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.8.4") (default-features #t) (kind 0)))) (hash "0cj2mjflrhqyccjpb72id1r1647a1apv7dy7bhajlnzjm4dh08hk") (yanked #t)))

(define-public crate-uflow-0.3 (crate (name "uflow") (vers "0.3.2") (deps (list (crate-dep (name "md5") (req "^0.7.0") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.8.4") (default-features #t) (kind 0)))) (hash "1jn6z7g9q8hp4n05hdhz5ib4bd2hkc7bl1gaaxclyc2mvmlygcqi")))

(define-public crate-uflow-0.4 (crate (name "uflow") (vers "0.4.0") (deps (list (crate-dep (name "md5") (req "^0.7.0") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.8.4") (default-features #t) (kind 0)))) (hash "13xzhm52s1r23y2bdgxpgz8dsc7k3rcd9r940jjn3774bwx06ajs")))

(define-public crate-uflow-0.5 (crate (name "uflow") (vers "0.5.0") (deps (list (crate-dep (name "md5") (req "^0.7.0") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.8.4") (default-features #t) (kind 0)))) (hash "0ki8k3jk844g33q2zy3hj2b5x23c64l1dikbiw9m71nzvfvf5gqn") (yanked #t)))

(define-public crate-uflow-0.5 (crate (name "uflow") (vers "0.5.1") (deps (list (crate-dep (name "md5") (req "^0.7.0") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.8.4") (default-features #t) (kind 0)))) (hash "10ai8wf0d27hz7x8fcv56fchz8zs0a1iyhdz3x8jk2avpmqczs2j")))

(define-public crate-uflow-0.6 (crate (name "uflow") (vers "0.6.0") (deps (list (crate-dep (name "md5") (req "^0.7.0") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.8.4") (default-features #t) (kind 0)))) (hash "1syliflcndg49zxxqldjs9rb4snjv9vdfj6cp034smbayqylc3ys")))

(define-public crate-uflow-0.6 (crate (name "uflow") (vers "0.6.1") (deps (list (crate-dep (name "md5") (req "^0.7.0") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.8.4") (default-features #t) (kind 0)))) (hash "0d1fag3yyg0jigddj5mavw39pkjxxyjnhl77xsw00fcx9sflf98w")))

(define-public crate-uflow-0.7 (crate (name "uflow") (vers "0.7.0") (deps (list (crate-dep (name "md5") (req "^0.7.0") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.8.4") (default-features #t) (kind 0)))) (hash "02isvnzk6dwyp8bv36vjs7n1959z2sdab4mzninf6cm671lmsyg8")))

(define-public crate-uflow-0.7 (crate (name "uflow") (vers "0.7.1") (deps (list (crate-dep (name "md5") (req "^0.7.0") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.8.4") (default-features #t) (kind 0)))) (hash "1dj6pr2gbxyd0m2fi3xrl8b0n7jjsj5w5his6fq5fshh3hfdgr1v")))

