(define-module (crates-io uf ix) #:use-module (crates-io))

(define-public crate-ufix-0.1 (crate (name "ufix") (vers "0.1.0") (deps (list (crate-dep (name "hash32") (req "^0.1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "typenum") (req "^1") (default-features #t) (kind 0)))) (hash "184x46dnzknffcij6yx568vlv6vf4k6kwav9p6zcr3rq4g4lln9v") (features (quote (("word8") ("word16") ("i128" "typenum/i128") ("default"))))))

