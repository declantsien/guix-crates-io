(define-module (crates-io uf in) #:use-module (crates-io))

(define-public crate-ufind-0.1 (crate (name "ufind") (vers "0.1.0") (deps (list (crate-dep (name "atty") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^2.20") (default-features #t) (kind 0)))) (hash "066wkfik2k2idyxwvix26wh5v3wjp5m8xsnj39g6168rzb9nfn72")))

(define-public crate-ufind-0.2 (crate (name "ufind") (vers "0.2.0") (deps (list (crate-dep (name "atty") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^2.20") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^2.20") (default-features #t) (kind 1)))) (hash "1vp3l0gy5sqzp2iwv19wdrhypjb5zs31vlwzz4zr7p3xbs5znqcn")))

(define-public crate-ufind-0.3 (crate (name "ufind") (vers "0.3.0") (deps (list (crate-dep (name "atty") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^2.20") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^2.20") (default-features #t) (kind 1)))) (hash "0qjvvh606945cv616vdyvylaxlgc0h3xi70k1r27qxj1xyrpg2d5")))

(define-public crate-ufind-0.3 (crate (name "ufind") (vers "0.3.1") (deps (list (crate-dep (name "atty") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^2.20") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^2.20") (default-features #t) (kind 1)))) (hash "12z995h6lgdxlf74jshw5gm6c9308ph2738gq3vlzwhry14sxmnl")))

