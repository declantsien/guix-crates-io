(define-module (crates-io jy d2) #:use-module (crates-io))

(define-public crate-jyd2d-0.1 (crate (name "jyd2d") (vers "0.1.0") (deps (list (crate-dep (name "assert_approx_eq") (req "^1.1.0") (default-features #t) (kind 0)))) (hash "08d03pvz2pvd93rjrkda93hsi4vf85ay51x1hyp9hvl88782b0ik")))

