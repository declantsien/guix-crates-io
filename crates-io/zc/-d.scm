(define-module (crates-io zc -d) #:use-module (crates-io))

(define-public crate-zc-derive-0.1 (crate (name "zc-derive") (vers "0.1.0") (deps (list (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (default-features #t) (kind 0)))) (hash "14fjl9lyyvda5jgbpgjsi582j3klxh27d4rgds76rcdrqqp3j363")))

(define-public crate-zc-derive-0.2 (crate (name "zc-derive") (vers "0.2.0") (deps (list (crate-dep (name "proc-macro2") (req "^1.0") (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (default-features #t) (kind 0)))) (hash "0hindy45p5nmcbvyilh0iwn19zbwjyak0lf02gq5v17dqdad51yc")))

(define-public crate-zc-derive-0.3 (crate (name "zc-derive") (vers "0.3.0") (deps (list (crate-dep (name "proc-macro2") (req "^1.0") (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (default-features #t) (kind 0)))) (hash "1znwi617pkxxcjgmy1qs1v80fwdfd6hvi3fzadj3pzqrhlxpnjdp")))

(define-public crate-zc-derive-0.4 (crate (name "zc-derive") (vers "0.4.0") (deps (list (crate-dep (name "proc-macro2") (req "^1.0") (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (default-features #t) (kind 0)))) (hash "14l4ibiw3kgxc652ah6l91671q2ji5d5ffkxzmiwrfb7y4jwplax")))

