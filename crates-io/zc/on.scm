(define-module (crates-io zc on) #:use-module (crates-io))

(define-public crate-zconvertenumn-1 (crate (name "zconvertenumn") (vers "1.0.0") (deps (list (crate-dep (name "proc-macro2") (req "^1.0.74") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.35") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2.0.46") (default-features #t) (kind 0)))) (hash "02adjympk28dg2j7ggr36n4i2n8masdr1wni4bakn57pyhb63ra2") (rust-version "1.56")))

(define-public crate-zconvertenumn-1 (crate (name "zconvertenumn") (vers "1.0.1") (deps (list (crate-dep (name "proc-macro2") (req "^1.0.74") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.35") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2.0.46") (default-features #t) (kind 0)))) (hash "04hlz9x8v9wg36fwj13gb50vilspz9f1j6bmiq0yid0aa7cnlx32") (rust-version "1.56")))

(define-public crate-zconvertenumn-1 (crate (name "zconvertenumn") (vers "1.0.2") (deps (list (crate-dep (name "enumnvictorhowisers") (req "^0.1.13") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1.0.74") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.35") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2.0.46") (default-features #t) (kind 0)))) (hash "1mp4af659sy9n0kx2y72zjr6k9g754r21d46zb62nswf8amf7hvh") (rust-version "1.56")))

