(define-module (crates-io zc _g) #:use-module (crates-io))

(define-public crate-zc_geo-0.1 (crate (name "zc_geo") (vers "0.1.0") (deps (list (crate-dep (name "num-traits") (req "^0.1.37") (default-features #t) (kind 0)))) (hash "0admrsjzg2250l8z9fm34dzy02fvvpcq32fgpbm5i25ilih0g41s")))

