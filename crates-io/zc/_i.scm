(define-module (crates-io zc _i) #:use-module (crates-io))

(define-public crate-zc_io-0.1 (crate (name "zc_io") (vers "0.1.0") (hash "0q0qmf37nf6lzbq0am3xlz1yy31wkf9c3fqxdv83h3dd05mvmmvf") (features (quote (("std") ("default" "std"))))))

(define-public crate-zc_io-0.1 (crate (name "zc_io") (vers "0.1.1") (hash "15b37lrjl0smmynm3cshj715rgnv7lprzn10f233ib9yqacjvqsd") (features (quote (("std") ("default" "std"))))))

(define-public crate-zc_io-0.2 (crate (name "zc_io") (vers "0.2.0") (hash "0g6l70j90cs4zmbx2kji17z9ijcvwsrfr33g8g12xhly0svf54qf") (features (quote (("std") ("default" "std"))))))

