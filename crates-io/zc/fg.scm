(define-module (crates-io zc fg) #:use-module (crates-io))

(define-public crate-zcfg-0.1 (crate (name "zcfg") (vers "0.1.0") (deps (list (crate-dep (name "lazy_static") (req "^0.2") (default-features #t) (kind 0)))) (hash "09pghy51vk1hfhzcy4zdz26kx4ff3a95m6w9skhdg399sjm5rrsa")))

(define-public crate-zcfg-0.2 (crate (name "zcfg") (vers "0.2.0") (deps (list (crate-dep (name "lazy_static") (req "^0.2") (default-features #t) (kind 0)))) (hash "0pbndcrbaivg8gznvx3rl9dqwby8am5mk3p7idqj1xl2bi7iv94s")))

(define-public crate-zcfg_flag_parser-0.1 (crate (name "zcfg_flag_parser") (vers "0.1.0") (deps (list (crate-dep (name "itertools") (req "^0.6.0") (default-features #t) (kind 0)) (crate-dep (name "zcfg") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "019846n215dahrd2q6v7zd0c1vhk3j5krz0v3bdlycwb032yyglv")))

(define-public crate-zcfg_flag_parser-0.2 (crate (name "zcfg_flag_parser") (vers "0.2.0") (deps (list (crate-dep (name "itertools") (req "^0.6.0") (default-features #t) (kind 0)) (crate-dep (name "zcfg") (req "^0.2.0") (default-features #t) (kind 0)))) (hash "0cjx74g9bkg039sfd9mlzffwld5j6w0a3zyq2zwm8yqmik2w91bi")))

