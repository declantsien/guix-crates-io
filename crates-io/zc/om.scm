(define-module (crates-io zc om) #:use-module (crates-io))

(define-public crate-zcomponents-0.1 (crate (name "zcomponents") (vers "0.1.0") (hash "198rz0d336cvf52m1wqvhr0z0adgq7i1p0zr7n2ax33r71mxl5cy")))

(define-public crate-zcomponents-0.1 (crate (name "zcomponents") (vers "0.1.1") (hash "1866nvsxfyzggc3fgsz7s1x1wbydkzj5mahry4ym9r0p00jcpi7c")))

(define-public crate-zcomponents-0.2 (crate (name "zcomponents") (vers "0.2.0") (hash "1h6v0fp8hc154mncacv2z40yxwrqik8816fqcn1yrxmndyh3n1xf")))

