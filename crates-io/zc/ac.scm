(define-module (crates-io zc ac) #:use-module (crates-io))

(define-public crate-zcache-0.0.1 (crate (name "zcache") (vers "0.0.1") (deps (list (crate-dep (name "once_cell") (req "^1.19.0") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1") (features (quote ("full"))) (default-features #t) (kind 2)))) (hash "006wzf4sw73977wqdql4nny7xi5jwz0kq265gk8bbdgnjcgksydb")))

(define-public crate-zcache-0.0.2 (crate (name "zcache") (vers "0.0.2") (deps (list (crate-dep (name "once_cell") (req "^1.19.0") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1") (features (quote ("full"))) (default-features #t) (kind 2)))) (hash "0r45cpv948mxlr6m4llv9bki3m0cb994kcsa60rd16wi2r9ah0k0")))

(define-public crate-zcache-0.0.3 (crate (name "zcache") (vers "0.0.3") (deps (list (crate-dep (name "once_cell") (req "^1.19.0") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1") (features (quote ("full"))) (default-features #t) (kind 2)))) (hash "131l57ci313k31ixz0ncnx7kgyz3mig6xr4ar6f3x6yg0y2v4vrd")))

(define-public crate-zcache-0.0.4 (crate (name "zcache") (vers "0.0.4") (deps (list (crate-dep (name "once_cell") (req "^1.19.0") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1") (features (quote ("full"))) (default-features #t) (kind 2)))) (hash "0l6b5py3ds8cv7i5qjl571hg9m363c0a2z5zhkc4mdragj0r2raz") (yanked #t)))

(define-public crate-zcache-0.0.5 (crate (name "zcache") (vers "0.0.5") (deps (list (crate-dep (name "once_cell") (req "^1.19.0") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1") (features (quote ("full"))) (default-features #t) (kind 2)))) (hash "1wckns426jwfa8036h9z9bhp6w4rdb7hm878wrylp1idadrs69lf")))

(define-public crate-zcache-0.0.6 (crate (name "zcache") (vers "0.0.6") (deps (list (crate-dep (name "once_cell") (req "^1.19.0") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1") (features (quote ("full"))) (default-features #t) (kind 2)))) (hash "1m1n5l12p410zr60jiqkkc0v4c9c5j71rk6clqx3hiix5cyqgcdw")))

(define-public crate-zcache-0.0.7 (crate (name "zcache") (vers "0.0.7") (deps (list (crate-dep (name "once_cell") (req "^1.19.0") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1") (features (quote ("full"))) (default-features #t) (kind 2)))) (hash "1wz8cn5393gjy9hmmfdrvxqz94c8g6b62lpwb6bj179q5175d98l")))

(define-public crate-zcache-0.0.8 (crate (name "zcache") (vers "0.0.8") (deps (list (crate-dep (name "once_cell") (req "^1.19.0") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1") (features (quote ("full"))) (default-features #t) (kind 2)))) (hash "0dkw0afv3q19fvs73rm6bradwb2j394irzhanc323pk7m0zxdvxb")))

