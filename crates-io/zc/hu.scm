(define-module (crates-io zc hu) #:use-module (crates-io))

(define-public crate-zchunk-0.1 (crate (name "zchunk") (vers "0.1.0") (deps (list (crate-dep (name "hex") (req "^0.4.3") (default-features #t) (kind 2)) (crate-dep (name "sha2") (req "^0.10.8") (default-features #t) (kind 0)) (crate-dep (name "tempfile") (req "^3.8.1") (default-features #t) (kind 2)) (crate-dep (name "thiserror") (req "^1.0.51") (default-features #t) (kind 0)) (crate-dep (name "zstd") (req "^0.13.0") (default-features #t) (kind 0)))) (hash "0g7hq3dvk5hy1fkyn1cg9l5xah1dmhh4yd3w3gwhap1g5n8xaigz")))

(define-public crate-zchunk-0.2 (crate (name "zchunk") (vers "0.2.0") (deps (list (crate-dep (name "hex") (req "^0.4.3") (default-features #t) (kind 2)) (crate-dep (name "sha2") (req "^0.10.8") (default-features #t) (kind 0)) (crate-dep (name "tempfile") (req "^3.8.1") (default-features #t) (kind 2)) (crate-dep (name "thiserror") (req "^1.0.51") (default-features #t) (kind 0)) (crate-dep (name "zstd") (req "^0.13.0") (default-features #t) (kind 0)))) (hash "0nzi0z10hakcnackmwn37w3wqzznx0i9s15r0kx045dha6zv0x1v")))

