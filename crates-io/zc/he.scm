(define-module (crates-io zc he) #:use-module (crates-io))

(define-public crate-zchess-0.1 (crate (name "zchess") (vers "0.1.0") (deps (list (crate-dep (name "bitflags") (req "^2.3.3") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.43") (default-features #t) (kind 0)))) (hash "0rqk70s5c9ds2drf6swswsykcnrd1pfcyl2hhsql8j49mzfrgbg9")))

