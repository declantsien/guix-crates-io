(define-module (crates-io sl sr) #:use-module (crates-io))

(define-public crate-SLSR-0.0.1 (crate (name "SLSR") (vers "0.0.1") (deps (list (crate-dep (name "docopt") (req "*") (default-features #t) (kind 0)) (crate-dep (name "docopt_macros") (req "*") (default-features #t) (kind 0)) (crate-dep (name "rustc-serialize") (req "*") (default-features #t) (kind 0)) (crate-dep (name "term") (req "*") (default-features #t) (kind 0)) (crate-dep (name "union-find") (req "*") (default-features #t) (kind 0)))) (hash "13gsi2qwm97jx6ayl4p9g17j1ca8s9iw3lxrm3jz92n5cx70md89")))

