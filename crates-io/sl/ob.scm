(define-module (crates-io sl ob) #:use-module (crates-io))

(define-public crate-slobberchops-test1-1 (crate (name "slobberchops-test1") (vers "1.4.0") (deps (list (crate-dep (name "grcov") (req "^0.8") (default-features #t) (kind 2)))) (hash "1axlifpfwcq5h3nkg637dyladvl5v56gvpjk0kqm3rjlq5ibsnc2") (yanked #t)))

(define-public crate-slobberchops-test2-0.1 (crate (name "slobberchops-test2") (vers "0.1.0") (deps (list (crate-dep (name "slobberchops-test1") (req "^1") (default-features #t) (kind 0)))) (hash "0nslvxi31c6bxvc64kf47lfvms28pmc8pmypgzdl6b49cykir7rx") (yanked #t)))

