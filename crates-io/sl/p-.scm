(define-module (crates-io sl p-) #:use-module (crates-io))

(define-public crate-slp-cli-0.1 (crate (name "slp-cli") (vers "0.1.0") (deps (list (crate-dep (name "slp") (req "= 0.1.9") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.3") (default-features #t) (kind 0)))) (hash "0l5ak2j0nw2iglbaw3sq25d79mf3xnm7nd11gblprwpa6qj1a9mm") (yanked #t)))

