(define-module (crates-io sl -r) #:use-module (crates-io))

(define-public crate-sl-rs-0.1 (crate (name "sl-rs") (vers "0.1.0") (deps (list (crate-dep (name "getopts") (req "^0") (default-features #t) (kind 0)) (crate-dep (name "ncurses") (req "^5.85") (default-features #t) (kind 0)) (crate-dep (name "nix") (req "^0") (default-features #t) (kind 0)))) (hash "0wqp8zaqzpw8c2hjahwzzmmq0cc9g5g7n6bmqznh4nxxy3ywq3k8")))

(define-public crate-sl-rs-1 (crate (name "sl-rs") (vers "1.0.0") (deps (list (crate-dep (name "getopts") (req "^0") (default-features #t) (kind 0)) (crate-dep (name "ncurses") (req "^5.85") (default-features #t) (kind 0)) (crate-dep (name "nix") (req "^0") (default-features #t) (kind 0)))) (hash "1lz71qkw1534p0zff49vzp2yvb1wm125axl1d75v1v2wsigd1zss")))

(define-public crate-sl-rs-1 (crate (name "sl-rs") (vers "1.0.1") (deps (list (crate-dep (name "getopts") (req "^0") (default-features #t) (kind 0)) (crate-dep (name "ncurses") (req "^5.91") (default-features #t) (kind 0)) (crate-dep (name "nix") (req "^0") (default-features #t) (kind 0)))) (hash "0fx2iprbxpgm8w0xg6nnrw859l0lrg2i1nj7bf93906l082kz13p")))

(define-public crate-sl-rs-2 (crate (name "sl-rs") (vers "2.0.0") (deps (list (crate-dep (name "crossterm") (req "^0.9.6") (features (quote ("terminal" "cursor" "input" "screen"))) (kind 0)) (crate-dep (name "getopts") (req "^0") (default-features #t) (kind 0)))) (hash "0kkm3xba1ghlpd2nx8yjdmqnfn1kyg8f82fzxznmx3hc1b7i90k9")))

