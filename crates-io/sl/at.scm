(define-module (crates-io sl at) #:use-module (crates-io))

(define-public crate-slate-1 (crate (name "slate") (vers "1.0.2") (deps (list (crate-dep (name "docopt") (req "^0.6") (default-features #t) (kind 0)) (crate-dep (name "exec") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "regex") (req "^0.1.69") (default-features #t) (kind 0)) (crate-dep (name "rustc-serialize") (req "^0.3") (default-features #t) (kind 0)))) (hash "1xc70b2mr7an5l3rfagvxpzspczja30v7sj3r4fp9y5zbpxa26zc")))

(define-public crate-slate-1 (crate (name "slate") (vers "1.1.0") (deps (list (crate-dep (name "docopt") (req "^0.6") (default-features #t) (kind 0)) (crate-dep (name "exec") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "regex") (req "^0.1.69") (default-features #t) (kind 0)) (crate-dep (name "rustc-serialize") (req "^0.3") (default-features #t) (kind 0)))) (hash "0swirwydrzxl15hw2czafn3xn84kk9lqw6pqakj3938y4g2mhr9y")))

(define-public crate-slate-1 (crate (name "slate") (vers "1.1.1") (deps (list (crate-dep (name "docopt") (req "^0.6") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "regex") (req "^0.1.69") (default-features #t) (kind 0)) (crate-dep (name "rustc-serialize") (req "^0.3") (default-features #t) (kind 0)))) (hash "1p7dv7nn3b1c96xcl71200i4dhlvyhb9ygnibrwc048w251d1ayk")))

(define-public crate-slate-1 (crate (name "slate") (vers "1.2.0") (deps (list (crate-dep (name "docopt") (req "^0.6") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "regex") (req "^0.1.69") (default-features #t) (kind 0)) (crate-dep (name "rustc-serialize") (req "^0.3") (default-features #t) (kind 0)))) (hash "0ysivgmfh0l9w3y1kjl80i90q07rv2qrj0wrfghfphmkqd554dgc")))

(define-public crate-slate-1 (crate (name "slate") (vers "1.3.0") (deps (list (crate-dep (name "docopt") (req "^0.6") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "regex") (req "^0.1.69") (default-features #t) (kind 0)) (crate-dep (name "rustc-serialize") (req "^0.3") (default-features #t) (kind 0)))) (hash "1y4lffrv0kmyc4r8iklla7j650rl3zhscg5rzbwbnxk08nikxd18")))

(define-public crate-slate-1 (crate (name "slate") (vers "1.4.0") (deps (list (crate-dep (name "docopt") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "serde") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)))) (hash "1f3f90m9ifr1madg4xdg9m72ijjjnd9k8vg6maylynzi5sq8fm9p")))

(define-public crate-slater-0.1 (crate (name "slater") (vers "0.1.0") (deps (list (crate-dep (name "serde") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 2)))) (hash "0qvg2n0mai89gcvzlnvk1xjhh3xhmjq6y4b13sfi1d6qsnjb3qwd")))

(define-public crate-slater-0.2 (crate (name "slater") (vers "0.2.0") (deps (list (crate-dep (name "serde") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 2)))) (hash "1wqxdd55v2vhsdrmdk9vd1rin25nnx9an4vhkr9zpxp6fbw52d6x")))

(define-public crate-slater-0.2 (crate (name "slater") (vers "0.2.1") (deps (list (crate-dep (name "serde") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 2)))) (hash "16dxz5la3vmzp936zigizrv9550dm9i5vz89lfvgalqxc5qny8b0")))

(define-public crate-slater-0.2 (crate (name "slater") (vers "0.2.2") (deps (list (crate-dep (name "serde") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 2)))) (hash "1rgg8p3m1a2xvsvdy8zy587bq30nba846839nap1vg9akx39ymcm")))

(define-public crate-slater-0.2 (crate (name "slater") (vers "0.2.3") (deps (list (crate-dep (name "serde") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)))) (hash "1mn4zwb4p6yxm1v2f4kdpj8dnz5src5i2g53jd9s15nc8gf4lfv2")))

(define-public crate-slater-0.2 (crate (name "slater") (vers "0.2.4") (deps (list (crate-dep (name "serde") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)))) (hash "1wyr6whhb9vrwns5h0gcwd8m25m7fhkyzm4r8dm2sxcy98vjkvvf")))

