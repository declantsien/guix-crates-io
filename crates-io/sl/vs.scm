(define-module (crates-io sl vs) #:use-module (crates-io))

(define-public crate-slvs-0.0.0 (crate (name "slvs") (vers "0.0.0") (deps (list (crate-dep (name "bindgen") (req "^0.64.0") (default-features #t) (kind 1)) (crate-dep (name "cc") (req "^1.0.79") (default-features #t) (kind 1)) (crate-dep (name "dunce") (req "^1.0.3") (default-features #t) (kind 1)))) (hash "09kaf9qsplcryxjdr3z02qcm9cgkjz3kyy2p7r0gi75nggklb1sq")))

(define-public crate-slvs-0.5 (crate (name "slvs") (vers "0.5.0") (deps (list (crate-dep (name "bindgen") (req "^0.64.0") (default-features #t) (kind 1)) (crate-dep (name "cc") (req "^1.0.79") (default-features #t) (kind 1)) (crate-dep (name "dunce") (req "^1.0.3") (default-features #t) (kind 1)))) (hash "08zzxgi0hp8amgm1vmgic5pvw4h6v96hz1q2v47zs4nak3841gws")))

(define-public crate-slvs-0.5 (crate (name "slvs") (vers "0.5.1") (deps (list (crate-dep (name "bindgen") (req "^0.64.0") (default-features #t) (kind 1)) (crate-dep (name "cc") (req "^1.0.79") (default-features #t) (kind 1)) (crate-dep (name "dunce") (req "^1.0.3") (default-features #t) (kind 1)))) (hash "079h56i7s3lpv2avglax0y9a4maxjiym5xi3l4khy5xkr14ld7dp")))

(define-public crate-slvs-0.6 (crate (name "slvs") (vers "0.6.0") (deps (list (crate-dep (name "bindgen") (req "^0.64.0") (default-features #t) (kind 1)) (crate-dep (name "cc") (req "^1.0.79") (default-features #t) (kind 1)) (crate-dep (name "dunce") (req "^1.0.3") (default-features #t) (kind 1)) (crate-dep (name "euclid") (req "^0.22.9") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "1hrs5ap3y34h44i3p0h8rsrq1wycxk5ng5a5s2955m99b5z8q3mn")))

