(define-module (crates-io sl tu) #:use-module (crates-io))

(define-public crate-sltunnel-0.1 (crate (name "sltunnel") (vers "0.1.0") (deps (list (crate-dep (name "tokio") (req "^0.2") (features (quote ("io-util" "macros" "tcp" "rt-core"))) (default-features #t) (kind 0)) (crate-dep (name "tokio-rustls") (req "^0.14") (default-features #t) (kind 0)))) (hash "0d8k6zp0qgsxgr1lqkvwdra965nb82wgx90y2qrqjcbw6fxji8yf") (yanked #t)))

