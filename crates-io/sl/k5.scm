(define-module (crates-io sl k5) #:use-module (crates-io))

(define-public crate-slk581-0.0.1 (crate (name "slk581") (vers "0.0.1") (deps (list (crate-dep (name "chrono") (req "^0.2") (default-features #t) (kind 0)))) (hash "1vbqmqq1z9y53r3p3r1ddkmz3yv1nv2650pxl9w8dhn7i448n1a6")))

(define-public crate-slk581-0.0.2 (crate (name "slk581") (vers "0.0.2") (deps (list (crate-dep (name "chrono") (req "^0.2") (default-features #t) (kind 0)))) (hash "1l0cg1kfyk87m7a0r9cnclw80sxl46wpyjynbbcqzj1xxlcfhyyq")))

