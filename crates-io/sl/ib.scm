(define-module (crates-io sl ib) #:use-module (crates-io))

(define-public crate-slib-0.0.0 (crate (name "slib") (vers "0.0.0") (hash "08a7slg6zyrk5a0ibhb671rxv8zr33aw8wg1lrkygz7f9iqxc48n")))

(define-public crate-slib-0.0.1 (crate (name "slib") (vers "0.0.1") (hash "1p9q2wqw7pd6fj4k3lcyl740ffccb2w7w1gafy5jhl3jlj0ynn8r")))

(define-public crate-slib-0.0.2 (crate (name "slib") (vers "0.0.2") (hash "040n4s9kihv5bmcpmqnn70hj4mcsqzl2f8zk2x1qspckvbjbm549")))

(define-public crate-slib-0.0.3 (crate (name "slib") (vers "0.0.3") (hash "1v0hd5rh4c8bc83zddiajkbgaivvdc10qk97z516agv62h51lph2")))

