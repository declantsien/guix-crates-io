(define-module (crates-io sl ca) #:use-module (crates-io))

(define-public crate-slcan-0.1 (crate (name "slcan") (vers "0.1.2") (deps (list (crate-dep (name "serial") (req "^0.4") (default-features #t) (kind 2)) (crate-dep (name "serial-core") (req "^0.4") (default-features #t) (kind 0)))) (hash "091480c3cvcs939v044xdm2y6bjjk8iy93hnnyvp1vd3azn05l56")))

(define-public crate-slcan-0.1 (crate (name "slcan") (vers "0.1.3") (deps (list (crate-dep (name "serial") (req "^0.4") (default-features #t) (kind 2)) (crate-dep (name "serial-core") (req "^0.4") (default-features #t) (kind 0)))) (hash "0cbpmyk4d5b8yw4fvvn0189piw35fjp5q8swipddsjrwdk5nryxq")))

