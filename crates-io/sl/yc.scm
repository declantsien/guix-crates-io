(define-module (crates-io sl yc) #:use-module (crates-io))

(define-public crate-slyce-0.1 (crate (name "slyce") (vers "0.1.0") (hash "030qc82b9disfagwc4wx133pcd1xkrwax3ifdn4pxw3zy7djwpfv")))

(define-public crate-slyce-0.1 (crate (name "slyce") (vers "0.1.1") (hash "11ha3c9dpds67c2nhkd67xg6yvfiazdgpsvyb4hwfv04ji02h8xw")))

(define-public crate-slyce-0.1 (crate (name "slyce") (vers "0.1.2") (hash "0aqadlcrhjqzfvcf44fv5yd98grh58lnapk6cwaxf7ahnwzqqaqw")))

(define-public crate-slyce-0.1 (crate (name "slyce") (vers "0.1.3") (hash "12k51a49asr5n91kfn0rrvvr93k7v2zdkzhckg8kmyv1f5gw1vcc")))

(define-public crate-slyce-0.1 (crate (name "slyce") (vers "0.1.4") (hash "0wbfckz63s6hff7nhb37gng7wx6l8h3sfmja6h9rl9gzjq3jp2i8")))

(define-public crate-slyce-0.1 (crate (name "slyce") (vers "0.1.5") (hash "0f01xnc0ciywh520dmj0mr49310s7pkfpjpwv3zrnvxnx7pan2s2")))

(define-public crate-slyce-0.1 (crate (name "slyce") (vers "0.1.6") (hash "08fgkiw0904f410rbn575y69ci9yf6sb3nzlb49bsi7234vyww85")))

(define-public crate-slyce-0.2 (crate (name "slyce") (vers "0.2.0") (hash "1kasm9m0l5n9gdd0icrgx5jijzbxx30jy4195dixfqf5sqikf2g3")))

(define-public crate-slyce-0.2 (crate (name "slyce") (vers "0.2.1") (hash "054s300k317s1b7asf6r4vlk77dajrc306abmrfj2wzzg6i9jznm")))

(define-public crate-slyce-0.3 (crate (name "slyce") (vers "0.3.0") (deps (list (crate-dep (name "arbitrary") (req "^0.4.7") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)))) (hash "0qqf4rlv9qb6lbj0prxl1sars41wpb8hgvywsx8nm3maczbhh35d")))

(define-public crate-slyce-0.3 (crate (name "slyce") (vers "0.3.1") (deps (list (crate-dep (name "arbitrary") (req "^0.4.7") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)))) (hash "0aa792p5k0h37mcsmkzrdk4rcwxxvljaj10x7zciif3xndkiqv84")))

