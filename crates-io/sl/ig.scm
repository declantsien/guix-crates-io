(define-module (crates-io sl ig) #:use-module (crates-io))

(define-public crate-slight-0.1 (crate (name "slight") (vers "0.1.0") (deps (list (crate-dep (name "argh") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "derive_more") (req "^0.99.17") (default-features #t) (kind 0)) (crate-dep (name "once_cell") (req "^1.17.0") (default-features #t) (kind 0)) (crate-dep (name "strum") (req "^0.24.1") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.38") (default-features #t) (kind 0)))) (hash "1ji8pnsqwaz90qcscw2hqdb461ava4m312xhvrkcbdxl10xkxyrn")))

