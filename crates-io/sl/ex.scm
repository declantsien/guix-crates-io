(define-module (crates-io sl ex) #:use-module (crates-io))

(define-public crate-slex-0.1 (crate (name "slex") (vers "0.1.0") (hash "092ci2wyw1nbcd20l0swlxrr0bnr341fa28i14yxacfnd5wgy5hy")))

(define-public crate-slex-0.2 (crate (name "slex") (vers "0.2.0") (hash "1vy2bg27haijqb86mbv1khc112ksf9nvibb4yd1z3cpn22g47fs2")))

(define-public crate-slex-0.2 (crate (name "slex") (vers "0.2.1") (hash "1v9ba2lnnasq1v53rw9x6x4h8w82g6s5km4bh0p2fcw2pla9p1is")))

(define-public crate-slex-0.2 (crate (name "slex") (vers "0.2.2") (hash "14wvlw3a860cqq7zmvfdy6v5c3mp2sdcxgcg4mz5grkp77c8g2a0")))

(define-public crate-slex-0.2 (crate (name "slex") (vers "0.2.3") (hash "0czziv5jkj4cl5z4gz0571ra29j18gknc6w4mq80h7cn5jdjsggr")))

(define-public crate-slex-0.2 (crate (name "slex") (vers "0.2.4") (hash "1mb17iizqrcdpjwz3wznr03qb40l5gjr7asfg4y41nfn4rvxyq6s")))

