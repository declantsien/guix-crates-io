(define-module (crates-io sl ud) #:use-module (crates-io))

(define-public crate-sludge-0.0.0 (crate (name "sludge") (vers "0.0.0") (deps (list (crate-dep (name "extreme") (req "^666") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2.66") (default-features #t) (kind 0)) (crate-dep (name "rio") (req "^0.9") (default-features #t) (kind 0)) (crate-dep (name "sled") (req "^0.31") (default-features #t) (kind 0)))) (hash "1ra47iq6byf1xlc5js84759p31i431nc91q3ppanx8r0gamd6sk2") (features (quote (("no_metrics"))))))

(define-public crate-sludge-ar-with-ranlib-0.8 (crate (name "sludge-ar-with-ranlib") (vers "0.8.0") (hash "0881di3pbll9p5plvk8d5nwymsr4pvlk9ialdcrdv4nsp5qbwr43")))

(define-public crate-sludge-cicd-0.1 (crate (name "sludge-cicd") (vers "0.1.0") (hash "1lh41z2fdidmf21ab0sbyc21j604fiz20m1z9qd0ymh1mqvag72n")))

(define-public crate-sludge-cicd-0.1 (crate (name "sludge-cicd") (vers "0.1.1") (hash "0rij0y9i1gwgppcmfwpzjdqp2i897wkxdzkw4qq7r7746yrrb9b2")))

(define-public crate-sludge-cicd-0.1 (crate (name "sludge-cicd") (vers "0.1.2") (hash "10di6mgr1ahlhja8sdfcid36ps7bbzhqrav6c89ax4q376j7imc3")))

(define-public crate-sludge-cicd-0.1 (crate (name "sludge-cicd") (vers "0.1.3") (hash "13zr421amq00qnkfgj9cgrvp0jkf2r5is0dx89w16jzajcbyd6nx")))

(define-public crate-sludge-cicd-0.1 (crate (name "sludge-cicd") (vers "0.1.4") (hash "18h1dj6w1v8m1jjiilg5f33cqcwl0p8zhn2lciyks6nmmxwzb3dr")))

(define-public crate-sludge-cicd-0.1 (crate (name "sludge-cicd") (vers "0.1.5") (hash "1va80ybajh2mpchsayz7pk9fklss2wsr5nja3ql93qshlp2hlw5q")))

(define-public crate-sludge-cicd-0.1 (crate (name "sludge-cicd") (vers "0.1.6") (hash "1nh5s7bacjf0pn2w2vka1r4hlzc2zf9nyric3br2d84q9ndpkd63")))

(define-public crate-sludge-cicd-0.1 (crate (name "sludge-cicd") (vers "0.1.7") (hash "18bxny7gik2zf8yrdfq3vrz5hw4pwz1m5kfd6pnpfl07vrldf9gf")))

(define-public crate-sludge-cicd-0.1 (crate (name "sludge-cicd") (vers "0.1.8") (hash "1pxnh22k27fcx9zr4kra0822f8i2a2c1ckdllxkcprccvqv2m45l")))

(define-public crate-sludge-cicd-0.1 (crate (name "sludge-cicd") (vers "0.1.9") (hash "0cwb0lxnmm5vk935hqcgjpw1bjv071ki635ciq7cyjnaf6h61rpn")))

(define-public crate-sludge-cicd-0.1 (crate (name "sludge-cicd") (vers "0.1.10") (hash "121x0a34i1zv0wf9vjgjsi7w2cg6lr3nkqqk0yapmm76j3pnwfcm")))

(define-public crate-sludge-cicd-0.1 (crate (name "sludge-cicd") (vers "0.1.11") (hash "07bf9i5kxlhnyd69ykwlf4nwq4v66ibfg8fhh5618pj5xgzmbl8c")))

(define-public crate-sludge-cicd-0.1 (crate (name "sludge-cicd") (vers "0.1.12") (hash "1f56shzgnjmi2r0aq03blmn26kgg8sdcq9ay0z8pi7q80wc7p3n5")))

(define-public crate-sludge-cicd-0.1 (crate (name "sludge-cicd") (vers "0.1.13") (hash "1mdkyqgxlpmdjyyn6g4alhk687r2i0a4rr0x8j3d9z93zxb69a2m")))

(define-public crate-sludge-cicd-0.1 (crate (name "sludge-cicd") (vers "0.1.14") (deps (list (crate-dep (name "expect-test") (req "^1.5.0") (default-features #t) (kind 2)))) (hash "1d4fvanxvl70la5kz54xdarpadg60q75rywgq9ckx9x1yniif61s")))

