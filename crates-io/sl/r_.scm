(define-module (crates-io sl r_) #:use-module (crates-io))

(define-public crate-slr_config-0.0.1 (crate (name "slr_config") (vers "0.0.1") (deps (list (crate-dep (name "slr_lexer") (req "= 0.0.1") (default-features #t) (kind 0)))) (hash "1xc8fbriw14p8bsvm8c83i04azpjxb8sdpb20cfcs2bi0826jpjw")))

(define-public crate-slr_config-0.0.2 (crate (name "slr_config") (vers "0.0.2") (deps (list (crate-dep (name "slr_lexer") (req "= 0.0.2") (default-features #t) (kind 0)))) (hash "0179f27hq0g18sw4rgy6yzf1nrf14ay3hj207g27bdvxalffz1cx")))

(define-public crate-slr_config-0.0.3 (crate (name "slr_config") (vers "0.0.3") (deps (list (crate-dep (name "slr_lexer") (req "= 0.0.3") (default-features #t) (kind 0)))) (hash "0sr687id7asl6f0ikjlcr8v9n5n77vp78gn387fhs2ijhf7dnkhp")))

(define-public crate-slr_config-0.0.4 (crate (name "slr_config") (vers "0.0.4") (deps (list (crate-dep (name "slr_lexer") (req "= 0.0.4") (default-features #t) (kind 0)))) (hash "1xz6wdvsaqpc12w4c8wny5mn3f970s9pdmzaf7k1aqsb1074i8fy")))

(define-public crate-slr_config-0.0.5 (crate (name "slr_config") (vers "0.0.5") (deps (list (crate-dep (name "slr_lexer") (req "= 0.0.5") (default-features #t) (kind 0)))) (hash "1w68djf39s22y4j71cl95rpmd0dm913g89f0fhxjn45h3n02r0qq")))

(define-public crate-slr_config-0.0.6 (crate (name "slr_config") (vers "0.0.6") (deps (list (crate-dep (name "slr_lexer") (req "= 0.0.6") (default-features #t) (kind 0)))) (hash "09pihzbiyalh63v1blwp61vgmr83jfjd4v86j2dhd4hqnh31vi5v")))

(define-public crate-slr_config-0.0.7 (crate (name "slr_config") (vers "0.0.7") (deps (list (crate-dep (name "slr_lexer") (req "= 0.0.7") (default-features #t) (kind 0)))) (hash "1f1haapahm0yb0ljhca17jmah1yz72zmz5iwhsa5l27zhqf8642n")))

(define-public crate-slr_config-0.0.8 (crate (name "slr_config") (vers "0.0.8") (deps (list (crate-dep (name "slr_lexer") (req "= 0.0.8") (default-features #t) (kind 0)))) (hash "0kyy2zjv7zf9i70ll92b5h7ivjg3bryvqmh35a1wnjb4izivc2vb")))

(define-public crate-slr_config-0.0.9 (crate (name "slr_config") (vers "0.0.9") (deps (list (crate-dep (name "slr_lexer") (req "= 0.0.9") (default-features #t) (kind 0)))) (hash "0i5kbmz6rkq1ldmnfw2jy8kgvsydspvv5kq4qsm3sanmlahlcr19")))

(define-public crate-slr_config-0.0.10 (crate (name "slr_config") (vers "0.0.10") (deps (list (crate-dep (name "slr_lexer") (req "= 0.0.10") (default-features #t) (kind 0)))) (hash "0dwrbmm5wisznrg6wyxihwvh6kahzpxp84r0x89h2zhjh5lw6q3v")))

(define-public crate-slr_config-0.0.11 (crate (name "slr_config") (vers "0.0.11") (deps (list (crate-dep (name "slr_lexer") (req "= 0.0.11") (default-features #t) (kind 0)))) (hash "0a3whbj3x2z9ffskk3cj4cjy0l6cfvqyrhb79j0nzgxga4yqzwsz")))

(define-public crate-slr_config-0.0.12 (crate (name "slr_config") (vers "0.0.12") (deps (list (crate-dep (name "slr_lexer") (req "= 0.0.12") (default-features #t) (kind 0)))) (hash "0qj9b32wci63237j3qpx67dnzqj41llzr5i992d76bnf80yb4bbx")))

(define-public crate-slr_config-0.0.13 (crate (name "slr_config") (vers "0.0.13") (deps (list (crate-dep (name "slr_lexer") (req "= 0.0.13") (default-features #t) (kind 0)))) (hash "0qlhghf0g61g8h1p608jprc2fbgsv9lvxyr25r0ppxr99yncmbr9")))

(define-public crate-slr_config-0.0.14 (crate (name "slr_config") (vers "0.0.14") (deps (list (crate-dep (name "slr_parser") (req "= 0.0.14") (default-features #t) (kind 0)))) (hash "1s6sr69n4zwb4ygdybjkm64kxs7jz9gskfmpl5hwnvdbcrl11m14")))

(define-public crate-slr_config-0.0.15 (crate (name "slr_config") (vers "0.0.15") (deps (list (crate-dep (name "slr_parser") (req "= 0.0.15") (default-features #t) (kind 0)))) (hash "1y0yqn5y9gfs6sqfc8ymq2akjk17vkv4xnzkyqcswnxny0s3454p")))

(define-public crate-slr_config-0.0.16 (crate (name "slr_config") (vers "0.0.16") (deps (list (crate-dep (name "slr_parser") (req "= 0.0.16") (default-features #t) (kind 0)))) (hash "110qqj90g9d69wrhms2wx8naicnnj097i01sm42yqr6sm61vrs94")))

(define-public crate-slr_config-0.0.17 (crate (name "slr_config") (vers "0.0.17") (deps (list (crate-dep (name "slr_parser") (req "= 0.0.17") (default-features #t) (kind 0)))) (hash "0s2f9c4g773ka6kjcfb53zfl6pzyzmp2faw00mfkrk4dvp8md6y3")))

(define-public crate-slr_config-0.0.18 (crate (name "slr_config") (vers "0.0.18") (deps (list (crate-dep (name "serde") (req "= 1.0.82") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "= 1.0.82") (default-features #t) (kind 0)) (crate-dep (name "slr_parser") (req "= 0.0.18") (default-features #t) (kind 0)))) (hash "0vvcvznv0l0wwpixqhkb284m4n7k9ammpd9snsl4rbknxzpqa1aj")))

(define-public crate-slr_config-0.0.19 (crate (name "slr_config") (vers "0.0.19") (deps (list (crate-dep (name "serde") (req "^1.0.104") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0.104") (default-features #t) (kind 0)) (crate-dep (name "slr_parser") (req "= 0.0.19") (default-features #t) (kind 0)))) (hash "07y3qzm0qdfcgv5j8npmggfgka2zq7ryap1bcl9k0halajp6bsr2")))

(define-public crate-slr_config-0.0.20 (crate (name "slr_config") (vers "0.0.20") (deps (list (crate-dep (name "serde") (req "^1.0.104") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0.104") (default-features #t) (kind 0)) (crate-dep (name "slr_parser") (req "= 0.0.20") (default-features #t) (kind 0)))) (hash "15j1ss8p6ci7jgw7vq9473xyl5658qn4qh30vb5r8vrzhg6jg3kx")))

(define-public crate-slr_config-0.0.21 (crate (name "slr_config") (vers "0.0.21") (deps (list (crate-dep (name "indexmap") (req "^1.9.1") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.104") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0.104") (default-features #t) (kind 0)) (crate-dep (name "slr_parser") (req "=0.0.21") (default-features #t) (kind 0)))) (hash "0agq54md67x6zqk26xix0fi08d551xih6ri5akflxyx3b711zyyg")))

(define-public crate-slr_config-0.0.22 (crate (name "slr_config") (vers "0.0.22") (deps (list (crate-dep (name "indexmap") (req "^1.9.1") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.104") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0.104") (default-features #t) (kind 0)) (crate-dep (name "slr_parser") (req "=0.0.22") (default-features #t) (kind 0)))) (hash "0nknyb72kw70xwbp01fcydmf9zg6087lyqslk6dcn6ir4lz142j5")))

(define-public crate-slr_lexer-0.0.1 (crate (name "slr_lexer") (vers "0.0.1") (hash "0vyq9g509fcrgdwc8zxivbby18zn41k5m0zg5ih9kq962qb3iai6")))

(define-public crate-slr_lexer-0.0.2 (crate (name "slr_lexer") (vers "0.0.2") (hash "0x2264rm5s50sgsx94zhqpfy59137gp4888l9m6w9fh6ixjlh44i")))

(define-public crate-slr_lexer-0.0.3 (crate (name "slr_lexer") (vers "0.0.3") (hash "0khxijh4fv21z6kjr70jqlf9mbx2pzlgn8j7m9r8m65ys6gi3891")))

(define-public crate-slr_lexer-0.0.4 (crate (name "slr_lexer") (vers "0.0.4") (hash "0z9xy2xlcx9z20nmf71wp0ij5p2573xlifdx2xdpcwwd4wpv76qs")))

(define-public crate-slr_lexer-0.0.5 (crate (name "slr_lexer") (vers "0.0.5") (hash "0s9xmg8hw36pmmwfvvf2i7hzv02d8n5w716v47mm04s2ybk0jaq9")))

(define-public crate-slr_lexer-0.0.6 (crate (name "slr_lexer") (vers "0.0.6") (hash "13rndp2md3rr6062harvk859w0dsdirxq3flvz6c0w5mr0byqw3v")))

(define-public crate-slr_lexer-0.0.7 (crate (name "slr_lexer") (vers "0.0.7") (hash "0bn00yzhnirbg6dni3vschf16y7860n3x1p9n88j90r8qh7gsphj")))

(define-public crate-slr_lexer-0.0.8 (crate (name "slr_lexer") (vers "0.0.8") (hash "1aziic1in2ilyb6x7jlcjhl8j19zi77m3a83knjbazlmwlql3bzh")))

(define-public crate-slr_lexer-0.0.9 (crate (name "slr_lexer") (vers "0.0.9") (hash "1ykvbxxk79q3z9rf2sikdkkhjrvcgz93zys98ff5ql2k61yz9g0x")))

(define-public crate-slr_lexer-0.0.10 (crate (name "slr_lexer") (vers "0.0.10") (hash "0rvi0dxqbalkfd5xlkr004njk7p45hfmjjl8xv6hnb7c8zcsqjbi")))

(define-public crate-slr_lexer-0.0.11 (crate (name "slr_lexer") (vers "0.0.11") (hash "0akdcd72xlhgvgr04idrvnsfq04vp4r9sd6sbv5hx9xgs2dn3xg1")))

(define-public crate-slr_lexer-0.0.12 (crate (name "slr_lexer") (vers "0.0.12") (hash "1by289sa9ili3gqkj9zlw4iqkx6fvqin67d8aws1f9sylgpnlixr")))

(define-public crate-slr_lexer-0.0.13 (crate (name "slr_lexer") (vers "0.0.13") (hash "1xkiaps0qcvw4xvhjq34jb14wqk2pd0qkbmnwx6q5h8a6w1s8qrh")))

(define-public crate-slr_parser-0.0.14 (crate (name "slr_parser") (vers "0.0.14") (hash "0z664z46nnrz73hfrg632byrgvdmr8g66805j9ysnb16axrkv6b8")))

(define-public crate-slr_parser-0.0.15 (crate (name "slr_parser") (vers "0.0.15") (hash "1kblzj9j4i0cacrj724r4sld8wxfcxnkf02k5xvvawcfilj0dnhh")))

(define-public crate-slr_parser-0.0.16 (crate (name "slr_parser") (vers "0.0.16") (hash "0xh5g76nzrmfs6z8m354d36p88b5xd1xnl1bbp05jwv3f1g6r3zh")))

(define-public crate-slr_parser-0.0.17 (crate (name "slr_parser") (vers "0.0.17") (hash "0akzxlbfr2s3knkv7agm6wcn7qxr66f1j3bsnn012ig65fn5n8xk")))

(define-public crate-slr_parser-0.0.18 (crate (name "slr_parser") (vers "0.0.18") (deps (list (crate-dep (name "serde") (req "= 1.0.82") (default-features #t) (kind 0)))) (hash "1h6imywy6nwfny0fkf7gd1mvbzk6ybk2lvx350kfki0mcpqj4jhh")))

(define-public crate-slr_parser-0.0.19 (crate (name "slr_parser") (vers "0.0.19") (deps (list (crate-dep (name "serde") (req "^1.0.104") (default-features #t) (kind 0)))) (hash "02im8c1q6fwq9nfvwfsq9r0slyk8vywyh0xv2k4bmhdm4abfv9xn")))

(define-public crate-slr_parser-0.0.20 (crate (name "slr_parser") (vers "0.0.20") (deps (list (crate-dep (name "serde") (req "^1.0.104") (default-features #t) (kind 0)))) (hash "0lxlcg0afwpdacxnn2k87mn3j26crjjh2xqnq1vgfdnf7z0cmn55")))

(define-public crate-slr_parser-0.0.21 (crate (name "slr_parser") (vers "0.0.21") (deps (list (crate-dep (name "serde") (req "^1.0.104") (default-features #t) (kind 0)))) (hash "1ck35xjdycfz0wrzvvifpiy3id31kkcv6hshxnl47xx51k3cig2v")))

(define-public crate-slr_parser-0.0.22 (crate (name "slr_parser") (vers "0.0.22") (deps (list (crate-dep (name "serde") (req "^1.0.104") (default-features #t) (kind 0)))) (hash "1fsd2aqid45kfdrm8166i0khl3i7rn9c3xhgncwssrbkbcjflj63")))

