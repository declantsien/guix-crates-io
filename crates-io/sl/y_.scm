(define-module (crates-io sl y_) #:use-module (crates-io))

(define-public crate-sly_static-1 (crate (name "sly_static") (vers "1.0.0") (deps (list (crate-dep (name "linkme") (req "^0.3.23") (default-features #t) (kind 0)) (crate-dep (name "sly_static_macros") (req "^1.0.0") (default-features #t) (kind 0)))) (hash "06d0g5i9cqjrm9y1cxvzhx0b56hcbal5qghrdha6jgqxrv3868kw")))

(define-public crate-sly_static-1 (crate (name "sly_static") (vers "1.0.1") (deps (list (crate-dep (name "linkme") (req "^0.3.23") (default-features #t) (kind 0)) (crate-dep (name "sly_static_macros") (req "^1.0.0") (default-features #t) (kind 0)))) (hash "1ih922ma0c15rz06v28b0ksxvb6rnnf9spdnw8fdkaayz3fm691v")))

(define-public crate-sly_static-1 (crate (name "sly_static") (vers "1.0.2") (deps (list (crate-dep (name "linkme") (req "^0.3.23") (default-features #t) (kind 0)) (crate-dep (name "sly_static_macros") (req "^1.0.0") (default-features #t) (kind 0)))) (hash "17jyfh94kgycjd9lz8z830h500z75hnirf88ixq4frn5dx3z99r8")))

(define-public crate-sly_static-1 (crate (name "sly_static") (vers "1.0.3") (deps (list (crate-dep (name "linkme") (req "^0.3.23") (default-features #t) (kind 0)) (crate-dep (name "sly_static_macros") (req "^1.0.0") (default-features #t) (kind 0)))) (hash "0a48yx4nz8yn0qb285wnlrf09vhl4yqyk5s6ypfvhcrq10l843bn")))

(define-public crate-sly_static-1 (crate (name "sly_static") (vers "1.0.4") (deps (list (crate-dep (name "linkme") (req "^0.3.23") (default-features #t) (kind 0)) (crate-dep (name "sly_static_macros") (req "^1.0.4") (default-features #t) (kind 0)))) (hash "0cf2vlgd4jmzhkssrhj7sw5k2kd8gx6y2qhmj7dl8xsykqs7d2lr")))

(define-public crate-sly_static-1 (crate (name "sly_static") (vers "1.0.5") (deps (list (crate-dep (name "linkme") (req "^0.3.23") (default-features #t) (kind 0)) (crate-dep (name "sly_static_macros") (req "^1.0.5") (default-features #t) (kind 0)))) (hash "177z91n1mgbcm2rc87y1j052mabvwa2vxx55dm935jykz0w19xs1")))

(define-public crate-sly_static_macros-1 (crate (name "sly_static_macros") (vers "1.0.0") (deps (list (crate-dep (name "quote") (req "^1.0.35") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2.0.50") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "0si6wcmis5ayc40x3cknipayb114fskqq7f9905jljlrknn842fr")))

(define-public crate-sly_static_macros-1 (crate (name "sly_static_macros") (vers "1.0.1") (deps (list (crate-dep (name "quote") (req "^1.0.35") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2.0.50") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "084jhh9sfq8211vcna4pgwshrdvsf4fla1v7kbcb3zhfvibad293")))

(define-public crate-sly_static_macros-1 (crate (name "sly_static_macros") (vers "1.0.2") (deps (list (crate-dep (name "quote") (req "^1.0.35") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2.0.50") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "0y3isf2bnnlqd1vz6k4wwhscxnhwx3wp1n76nh78b2xmm3n2z3vz")))

(define-public crate-sly_static_macros-1 (crate (name "sly_static_macros") (vers "1.0.3") (deps (list (crate-dep (name "quote") (req "^1.0.35") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2.0.50") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "19hk12i7rf0jnxp4gd8qyclz2sb857fwlm5iaya4wyw7v8na555n")))

(define-public crate-sly_static_macros-1 (crate (name "sly_static_macros") (vers "1.0.4") (deps (list (crate-dep (name "quote") (req "^1.0.35") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2.0.50") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "1swmmllyb0109svlvhplgp8d96li4p2l5f5gqgj95qcf82bd8196")))

(define-public crate-sly_static_macros-1 (crate (name "sly_static_macros") (vers "1.0.5") (deps (list (crate-dep (name "quote") (req "^1.0.35") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2.0.50") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "0ngxhkxn06labw09szzj142kz3wsnc9swpmix86nr4qpwwcf7g0g")))

