(define-module (crates-io sl ug) #:use-module (crates-io))

(define-public crate-slug-0.1 (crate (name "slug") (vers "0.1.0") (deps (list (crate-dep (name "unidecode") (req "*") (default-features #t) (kind 0)))) (hash "1jkr8b4rkk0y3wb21c7q4230j172rdbqvx08z8iawb2z9s8mc5fz")))

(define-public crate-slug-0.1 (crate (name "slug") (vers "0.1.1") (deps (list (crate-dep (name "unidecode") (req "^0.2") (default-features #t) (kind 0)))) (hash "1wz9ldljfb968fkj0dh1gi4gnl3nalk6d8ygky5kq9d1i3l1rbrr")))

(define-public crate-slug-0.1 (crate (name "slug") (vers "0.1.2") (deps (list (crate-dep (name "unidecode") (req "^0.2") (default-features #t) (kind 0)))) (hash "1f4c681r8b4f75gdi9r7bagdyb522hkwjdljbxnbh1yb8d5zzxgn")))

(define-public crate-slug-0.1 (crate (name "slug") (vers "0.1.3") (deps (list (crate-dep (name "unidecode") (req "^0.3") (default-features #t) (kind 0)))) (hash "04cyd2ksa0zqfcydwwn402j8qbgv997q25alc6l3j4p94i6wnyvr")))

(define-public crate-slug-0.1 (crate (name "slug") (vers "0.1.4") (deps (list (crate-dep (name "deunicode") (req "^0.4.0") (default-features #t) (kind 0)))) (hash "0wrk0w7mcmnvpmc27fw8dxkip6f6xgwpfgp7mp56yv2bd8p7dg5k")))

(define-public crate-slug-0.1 (crate (name "slug") (vers "0.1.5") (deps (list (crate-dep (name "deunicode") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "wasm-bindgen") (req "^0.2") (default-features #t) (target "cfg(target_family = \"wasm\")") (kind 0)) (crate-dep (name "wasm-bindgen-test") (req "^0.3") (default-features #t) (target "cfg(target_family = \"wasm\")") (kind 2)))) (hash "1i68hkvpbf04ga5kcssyads2wdy0kyikbqgq0l069nn8r774mn9v")))

(define-public crate-sluggify-0.1 (crate (name "sluggify") (vers "0.1.0") (deps (list (crate-dep (name "regex") (req "^1.7.3") (default-features #t) (kind 0)))) (hash "17cq7yza6wqvwcwjxs9z8rgil7zap16nnziv74gxj9jx9r1saz7q")))

(define-public crate-slugid-1 (crate (name "slugid") (vers "1.0.0") (deps (list (crate-dep (name "base64") (req "^0.12.3") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "ring") (req "^0.16.15") (default-features #t) (kind 0)))) (hash "1zcjq4fx0w523py69fs5jm7bs0ln5j84ha6p5mp5kwm019awxzvx")))

(define-public crate-slugid-1 (crate (name "slugid") (vers "1.0.1") (deps (list (crate-dep (name "base64") (req "^0.13") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "ring") (req "^0.16.15") (default-features #t) (kind 0)))) (hash "1ssv5kwfjl6imznwhsyk2pmz5s7gpljvjv4lf0xjnjnlrspak5x7")))

(define-public crate-slugify-0.1 (crate (name "slugify") (vers "0.1.0") (deps (list (crate-dep (name "unidecode") (req "^0.3.0") (default-features #t) (kind 0)))) (hash "12xcybjhm7g2rql0n91lgb2j195za58qx3smswqvi2107lhczf6n")))

(define-public crate-slugify-rs-0.0.1 (crate (name "slugify-rs") (vers "0.0.1") (deps (list (crate-dep (name "deunicode") (req "^1.3.1") (default-features #t) (kind 0)) (crate-dep (name "nanoid") (req "^0.4.0") (default-features #t) (kind 0)))) (hash "098ayjgcxpf43kvmkr822ap13nwbnzddy3smg8r0idk7yaf7w6l4") (rust-version "1.56")))

(define-public crate-slugify-rs-0.0.2 (crate (name "slugify-rs") (vers "0.0.2") (deps (list (crate-dep (name "deunicode") (req "^1.3.1") (default-features #t) (kind 0)) (crate-dep (name "nanoid") (req "^0.4.0") (default-features #t) (kind 0)))) (hash "1fihfyyp3sw4hfwwf6y91gq61x1qvjqqv2w4gd6xnabpq0z41anh") (yanked #t) (rust-version "1.56")))

(define-public crate-slugify-rs-0.0.3 (crate (name "slugify-rs") (vers "0.0.3") (deps (list (crate-dep (name "deunicode") (req "^1.3.1") (default-features #t) (kind 0)) (crate-dep (name "nanoid") (req "^0.4.0") (default-features #t) (kind 0)))) (hash "1wxzpmzaj0k4sql6h4vb70jcwwr37cv3lz16dzg1vnlllxpdp368") (rust-version "1.56")))

(define-public crate-slugmin-1 (crate (name "slugmin") (vers "1.0.0") (deps (list (crate-dep (name "deunicode") (req "^1") (default-features #t) (kind 0)))) (hash "19c4kihhczhkpvw256zw7y5bwmqlpx7wd40pf8p4c5b2cljqaw9c")))

(define-public crate-slugmin-1 (crate (name "slugmin") (vers "1.0.1") (deps (list (crate-dep (name "deunicode") (req "^1") (default-features #t) (kind 0)))) (hash "01ifk40hv9mqvgk6w7lamgcbipcz42k790xibg2v881ism5larw5")))

(define-public crate-slugmin-1 (crate (name "slugmin") (vers "1.0.2") (deps (list (crate-dep (name "deunicode") (req "^1") (default-features #t) (kind 0)))) (hash "0vsvx0x4xspbhnmqc4wg38v32l83kmscw6rkqz3x2rz1avmbgkyq")))

