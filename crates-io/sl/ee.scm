(define-module (crates-io sl ee) #:use-module (crates-io))

(define-public crate-sleef-0.1 (crate (name "sleef") (vers "0.1.0") (deps (list (crate-dep (name "doubled") (req "^0.2.3") (features (quote ("packed_simd"))) (default-features #t) (kind 0)) (crate-dep (name "packed_simd") (req "^0.3.8") (features (quote ("into_bits"))) (default-features #t) (kind 0) (package "packed_simd_2")) (crate-dep (name "rand") (req "^0.8.5") (features (quote ("simd_support"))) (default-features #t) (kind 2)) (crate-dep (name "rug") (req "^1.16") (default-features #t) (kind 2)))) (hash "0mviy7gawqijkbz0gfc9jb1p7m12n5wrxkv12zppyw0h8jgbi6d5") (features (quote (("default"))))))

(define-public crate-sleef-0.2 (crate (name "sleef") (vers "0.2.0") (deps (list (crate-dep (name "doubled") (req "^0.3.0") (features (quote ("simd"))) (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 2)) (crate-dep (name "rug") (req "^1.16") (default-features #t) (kind 2)))) (hash "0c9bj7ap7299k5p18xsqpbg830xxnszkvp7qwiwjv7hnjgid1x1j") (features (quote (("default"))))))

(define-public crate-sleef-0.3 (crate (name "sleef") (vers "0.3.0") (deps (list (crate-dep (name "doubled") (req "^0.3.1") (features (quote ("simd"))) (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 2)) (crate-dep (name "rug") (req "^1.16") (default-features #t) (kind 2)))) (hash "1xzvrb22d6z6lr03b9lw3zvnwc3cxccli2xm4q1fya9hy756sa0w") (features (quote (("default"))))))

(define-public crate-sleef-0.3 (crate (name "sleef") (vers "0.3.1") (deps (list (crate-dep (name "doubled") (req "^0.3.2") (features (quote ("simd"))) (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 2)) (crate-dep (name "rug") (req "^1.16") (default-features #t) (kind 2)))) (hash "0cr2g2mr5d9sm4dnd6ldpl8qhsdqrvqch27x4s8gqd61p58qx57v") (features (quote (("default"))))))

(define-public crate-sleef-0.3 (crate (name "sleef") (vers "0.3.2") (deps (list (crate-dep (name "doubled") (req "^0.3.2") (features (quote ("simd"))) (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 2)) (crate-dep (name "rug") (req "^1.16") (default-features #t) (kind 2)))) (hash "0zgl9smpnsir199ys76xg2xiv3mfdmk0lrinawyh26cs0j461qcz") (features (quote (("default"))))))

(define-public crate-sleef-sys-0.1 (crate (name "sleef-sys") (vers "0.1.0") (deps (list (crate-dep (name "bindgen") (req "^0.38") (default-features #t) (kind 1)) (crate-dep (name "cfg-if") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "cmake") (req "^0.1") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2") (kind 0)))) (hash "1k7mxzaa915vzrqj6ngv8284yv5drighzb34r2rqm7acs0p859gi") (features (quote (("dft") ("default")))) (links "sleef")))

(define-public crate-sleef-sys-0.1 (crate (name "sleef-sys") (vers "0.1.1") (deps (list (crate-dep (name "bindgen") (req "^0.38") (default-features #t) (kind 1)) (crate-dep (name "cfg-if") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "cmake") (req "^0.1") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2") (kind 0)))) (hash "1nwkan44sj495vn8wbipp9aq2k0kbx96wr6xagi7hl1ic8v7c78h") (features (quote (("dft") ("default")))) (links "sleef")))

(define-public crate-sleef-sys-0.1 (crate (name "sleef-sys") (vers "0.1.2") (deps (list (crate-dep (name "bindgen") (req "^0.46") (default-features #t) (kind 1)) (crate-dep (name "cfg-if") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "cmake") (req "^0.1") (default-features #t) (kind 1)) (crate-dep (name "env_logger") (req "^0.6") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2") (kind 0)))) (hash "1881q2yc17j2m1yvh01447c93ws1mspnrj3k2nbvwbvcm8z81kkv") (features (quote (("dft") ("default")))) (links "sleef")))

(define-public crate-sleef-trig-0.1 (crate (name "sleef-trig") (vers "0.1.0") (deps (list (crate-dep (name "criterion") (req "^0.4") (default-features #t) (kind 2)) (crate-dep (name "quickcheck") (req "^1") (default-features #t) (kind 2)) (crate-dep (name "sleef-trig-sys") (req "^0.1.0") (default-features #t) (kind 2)))) (hash "09850kg50ilk5kbawn68rarm3wp7p06r5ic4ag6xy1daxilakihs")))

(define-public crate-sleef-trig-sys-0.1 (crate (name "sleef-trig-sys") (vers "0.1.0") (deps (list (crate-dep (name "cc") (req "^1.0") (default-features #t) (kind 1)))) (hash "07dp4mg365w2id7srclfk6wbrn0gi8n68daz8f9zdf4grc6v1b96")))

(define-public crate-sleek-0.1 (crate (name "sleek") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^4.2.1") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "glob") (req "^0.3.1") (default-features #t) (kind 0)) (crate-dep (name "sqlformat") (req "^0.2.1") (default-features #t) (kind 0)))) (hash "0v7gllj4g0r253smnjydsqn7j4bnynbbj49znckil2wcszl0c62n") (yanked #t)))

(define-public crate-sleek-0.1 (crate (name "sleek") (vers "0.1.1") (deps (list (crate-dep (name "clap") (req "^4.2.1") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "glob") (req "^0.3.1") (default-features #t) (kind 0)) (crate-dep (name "sqlformat") (req "^0.2.1") (default-features #t) (kind 0)))) (hash "00k8njgx71jcncsn7hcvkhg4nns57z0f6wqq328bbqg4cg8zil39")))

(define-public crate-sleek-0.1 (crate (name "sleek") (vers "0.1.2") (deps (list (crate-dep (name "clap") (req "^4.2.1") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "glob") (req "^0.3.1") (default-features #t) (kind 0)) (crate-dep (name "sqlformat") (req "^0.2.1") (default-features #t) (kind 0)))) (hash "0mz2kgfjhqid0kgzraad86vby1gy04b81fq5ar2kfcrxrgw35gil")))

(define-public crate-sleek-0.2 (crate (name "sleek") (vers "0.2.0") (deps (list (crate-dep (name "clap") (req "^4.2.1") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "glob") (req "^0.3.1") (default-features #t) (kind 0)) (crate-dep (name "sqlformat") (req "^0.2.1") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.40") (default-features #t) (kind 0)))) (hash "1r5i5lfzhr6wa7zs7mg5c2pgkfbv44gqjbw0hwc29yvi81jzf119")))

(define-public crate-sleek-0.2 (crate (name "sleek") (vers "0.2.1") (deps (list (crate-dep (name "clap") (req "^4.2.1") (features (quote ("derive" "cargo"))) (default-features #t) (kind 0)) (crate-dep (name "glob") (req "^0.3.1") (default-features #t) (kind 0)) (crate-dep (name "sqlformat") (req "^0.2.1") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.40") (default-features #t) (kind 0)))) (hash "0vkaddfra7vky7mmkhpqxn44lzam4r4mdqd81x4xcb66vjsa3qi6")))

(define-public crate-sleek-0.2 (crate (name "sleek") (vers "0.2.2") (deps (list (crate-dep (name "clap") (req "^4.2.1") (features (quote ("derive" "cargo"))) (default-features #t) (kind 0)) (crate-dep (name "glob") (req "^0.3.1") (default-features #t) (kind 0)) (crate-dep (name "sqlformat") (req "^0.2.1") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.40") (default-features #t) (kind 0)))) (hash "0c7lrhqry3rjivfhh2izf87hxnqzhq17f0zj5n54p04vmywa2clz")))

(define-public crate-sleek-0.2 (crate (name "sleek") (vers "0.2.3") (deps (list (crate-dep (name "clap") (req "^4.2.1") (features (quote ("derive" "cargo"))) (default-features #t) (kind 0)) (crate-dep (name "glob") (req "^0.3.1") (default-features #t) (kind 0)) (crate-dep (name "sqlformat") (req "^0.2.1") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.40") (default-features #t) (kind 0)))) (hash "0h92pkwdxfrv1a1r0amcns82fpimdlcjlkyp34d1ndkrfp2ajk38")))

(define-public crate-sleek-0.3 (crate (name "sleek") (vers "0.3.0") (deps (list (crate-dep (name "clap") (req "^4.2.4") (features (quote ("derive" "cargo"))) (default-features #t) (kind 0)) (crate-dep (name "glob") (req "^0.3.1") (default-features #t) (kind 0)) (crate-dep (name "sqlformat") (req "^0.2.1") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.40") (default-features #t) (kind 0)))) (hash "1ncaaj4ni9nr7j9z8nmc2s7fiz0dz4kphbxnbbx8frqkqgm2ahs5")))

(define-public crate-sleek_csv-0.1 (crate (name "sleek_csv") (vers "0.1.0") (deps (list (crate-dep (name "arrayvec") (req "^0.5.1") (default-features #t) (kind 2)) (crate-dep (name "csv-core") (req "^0.1.6") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.103") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.103") (features (quote ("derive"))) (default-features #t) (kind 2)))) (hash "0711wrd73mnc90ldqjpjiyf2zx541cc98a8jr9a7rmwq7laq39gp")))

(define-public crate-sleep-1 (crate (name "sleep") (vers "1.0.0") (deps (list (crate-dep (name "clap") (req "^2.33") (default-features #t) (kind 0)))) (hash "17d5w9jzis5qnpif0gnbbsqa5ny4hga96pb2wc71jvpkybz17qw9")))

(define-public crate-sleep-1 (crate (name "sleep") (vers "1.0.1") (deps (list (crate-dep (name "clap") (req "^2.33") (default-features #t) (kind 0)))) (hash "0n67ymvjn41xafq8vdvj7ri7lhcbbyf5kk7hzv42qk25bczrgxr8")))

(define-public crate-sleep-1 (crate (name "sleep") (vers "1.0.2") (deps (list (crate-dep (name "clap") (req "^4.4.11") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "11lyc3cagw3kk241qp76xcbs0rmqaczysm93qf1gn1d90fwh8h95")))

(define-public crate-sleep-file-parser-0.0.1 (crate (name "sleep-file-parser") (vers "0.0.1") (deps (list (crate-dep (name "nom") (req "^3.2.1") (default-features #t) (kind 0)))) (hash "12qh57sp8v1wj1f3yadgy4f7yyx46f67cjfl4rq4qn673vwsnh6r")))

(define-public crate-sleep-interactive-0.1 (crate (name "sleep-interactive") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^4.2.1") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "indicatif") (req "^0.17.3") (default-features #t) (kind 0)))) (hash "0v8ngmaxh0x3fj3v1c7hhd1vkbih1d5xcgpvw93y0dbs9bksc7mh")))

(define-public crate-sleep-interactive-1 (crate (name "sleep-interactive") (vers "1.0.0") (deps (list (crate-dep (name "clap") (req "^4.2.1") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "indicatif") (req "^0.17.3") (default-features #t) (kind 0)))) (hash "1020xfmjdkcv25nl7bmmph4wfdajcv2mbdxlx331ccdhbw3dspvf")))

(define-public crate-sleep-interactive-1 (crate (name "sleep-interactive") (vers "1.0.1") (deps (list (crate-dep (name "clap") (req "^4.2.1") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "indicatif") (req "^0.17.3") (default-features #t) (kind 0)))) (hash "0h6pp5hfrk2cpj0sj1nida1j5mn5lf294kxzxzbkgc8v01qn39qz")))

(define-public crate-sleep-parser-0.0.1 (crate (name "sleep-parser") (vers "0.0.1") (deps (list (crate-dep (name "nom") (req "^3.2.1") (default-features #t) (kind 0)))) (hash "0hmrk84mq0jqxgn7sdjz82z6f27w6fc1y4s949vv74vf17i3m0cs")))

(define-public crate-sleep-parser-0.0.5 (crate (name "sleep-parser") (vers "0.0.5") (deps (list (crate-dep (name "byteorder") (req "^1.2.1") (default-features #t) (kind 0)) (crate-dep (name "failure") (req "^0.1.1") (default-features #t) (kind 0)))) (hash "17i96cdfx9ypcv605d6lyfrspdl0iasnbg9rzr6v4j8lg7ww8mxn")))

(define-public crate-sleep-parser-0.1 (crate (name "sleep-parser") (vers "0.1.0") (deps (list (crate-dep (name "byteorder") (req "^1.2.1") (default-features #t) (kind 0)) (crate-dep (name "failure") (req "^0.1.1") (default-features #t) (kind 0)))) (hash "1a1s3yix486171vjzg0gkz33nzpfdjwszycp42sipdkwb6h6vm5l")))

(define-public crate-sleep-parser-0.1 (crate (name "sleep-parser") (vers "0.1.1") (deps (list (crate-dep (name "byteorder") (req "^1.2.1") (default-features #t) (kind 0)) (crate-dep (name "failure") (req "^0.1.1") (default-features #t) (kind 0)))) (hash "0cbmwmb0yqw3fm9yayfii0pfwzdr4jp4dj46gfddkq5w3z1nh7r0")))

(define-public crate-sleep-parser-0.2 (crate (name "sleep-parser") (vers "0.2.0") (deps (list (crate-dep (name "byteorder") (req "^1.2.1") (default-features #t) (kind 0)) (crate-dep (name "failure") (req "^0.1.1") (default-features #t) (kind 0)))) (hash "1w0lsadamr7a363bqc10by0pi1mgbgzssz8ysqz1nwli7gba3s1h")))

(define-public crate-sleep-parser-0.3 (crate (name "sleep-parser") (vers "0.3.1") (deps (list (crate-dep (name "byteorder") (req "^1.2.1") (default-features #t) (kind 0)) (crate-dep (name "failure") (req "^0.1.1") (default-features #t) (kind 0)))) (hash "08phsvfk8svspdswdzzaljq0smv4wgy1kzqp7spxjmp9488kbvrr")))

(define-public crate-sleep-parser-0.4 (crate (name "sleep-parser") (vers "0.4.0") (deps (list (crate-dep (name "byteorder") (req "^1.2.1") (default-features #t) (kind 0)) (crate-dep (name "failure") (req "^0.1.1") (default-features #t) (kind 0)))) (hash "07nyyiil5ikmyi5baa0jyhwlv68n9715mg1j21i28vj8lf5arw09")))

(define-public crate-sleep-parser-0.5 (crate (name "sleep-parser") (vers "0.5.0") (deps (list (crate-dep (name "byteorder") (req "^1.2.1") (default-features #t) (kind 0)) (crate-dep (name "failure") (req "^0.1.1") (default-features #t) (kind 0)))) (hash "1p8b35ajcyalqlp0j3svm8j9m9c9lmslzngfw7ngx5bdk6acz69z")))

(define-public crate-sleep-parser-0.6 (crate (name "sleep-parser") (vers "0.6.0") (deps (list (crate-dep (name "byteorder") (req "^1.2.1") (default-features #t) (kind 0)) (crate-dep (name "failure") (req "^0.1.1") (default-features #t) (kind 0)))) (hash "154l9xg9g2hgixix529sxmxypzyd18p609ghx4f10mq40lrzfdgp")))

(define-public crate-sleep-parser-0.7 (crate (name "sleep-parser") (vers "0.7.0") (deps (list (crate-dep (name "byteorder") (req "^1.2.1") (default-features #t) (kind 0)) (crate-dep (name "failure") (req "^0.1.1") (default-features #t) (kind 0)))) (hash "1gzk17wyh1mj0g08pjm2jlkhy8hdb4bs4csmd50j30vg2wx1wa6j")))

(define-public crate-sleep-parser-0.8 (crate (name "sleep-parser") (vers "0.8.0") (deps (list (crate-dep (name "byteorder") (req "^1.2.1") (default-features #t) (kind 0)) (crate-dep (name "failure") (req "^0.1.1") (default-features #t) (kind 0)))) (hash "0x57dw0jmraz6xrpsir6v49f8q5ri4rc9fncbqjk9vic7gvl8xxp")))

(define-public crate-sleep-progress-0.1 (crate (name "sleep-progress") (vers "0.1.0") (deps (list (crate-dep (name "arbitrary") (req "^1") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^4.0.22") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "indicatif") (req "^0.17.2") (default-features #t) (kind 0)) (crate-dep (name "miette") (req "^5.4.1") (features (quote ("fancy-no-backtrace"))) (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.37") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.21.2") (features (quote ("rt" "rt-multi-thread" "sync" "time" "macros"))) (default-features #t) (kind 0)))) (hash "06yc1grrw1qj2wz3l70bi7l3dplbhn4f1rhkqkb5jklwxikpkck2")))

(define-public crate-sleep-progress-0.1 (crate (name "sleep-progress") (vers "0.1.2") (deps (list (crate-dep (name "arbitrary") (req "^1") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^4.0.22") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "indicatif") (req "^0.17.2") (default-features #t) (kind 0)) (crate-dep (name "miette") (req "^5.4.1") (features (quote ("fancy-no-backtrace"))) (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.37") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.21.2") (features (quote ("rt" "rt-multi-thread" "sync" "time" "macros"))) (default-features #t) (kind 0)))) (hash "04knhi6saybhrb05hp78qyw229g0x5bqdp8fvgv55mh1hdqf02zz")))

(define-public crate-sleep-progress-0.1 (crate (name "sleep-progress") (vers "0.1.3") (deps (list (crate-dep (name "arbitrary") (req "^1") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^4.0.22") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "indicatif") (req "^0.17.2") (default-features #t) (kind 0)) (crate-dep (name "miette") (req "^5.4.1") (features (quote ("fancy-no-backtrace"))) (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.37") (default-features #t) (kind 0)))) (hash "1ykkc28ny3q1arlv2kbx7w4y4mkgh0i8i1p3xz3bxjjvfx3ggvky")))

(define-public crate-sleep-progress-0.2 (crate (name "sleep-progress") (vers "0.2.0") (deps (list (crate-dep (name "arbitrary") (req "^1") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "chrono") (req "^0.4.26") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^4.0.22") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "dateparser") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "indicatif") (req "^0.17.2") (default-features #t) (kind 0)) (crate-dep (name "miette") (req "^5.4.1") (features (quote ("fancy-no-backtrace"))) (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.37") (default-features #t) (kind 0)))) (hash "1zsdsx1wrnf8ycl9qxb2784i1kms5fhglazndh46w9ki7cmc5x3j")))

(define-public crate-sleep-protocol-0.1 (crate (name "sleep-protocol") (vers "0.1.0") (deps (list (crate-dep (name "protobuf") (req "^1.4.3") (default-features #t) (kind 0)) (crate-dep (name "protoc-rust") (req "^1.4") (default-features #t) (kind 1)))) (hash "1pdhkn53l5di1vaaywgkqkdk8zsg3shh46n00ypydlkapxjixqwh")))

(define-public crate-sleep-protocol-0.1 (crate (name "sleep-protocol") (vers "0.1.1") (deps (list (crate-dep (name "protobuf") (req "^1.4.3") (default-features #t) (kind 0)) (crate-dep (name "protoc-rust") (req "^1.4") (default-features #t) (kind 1)))) (hash "0fhw8z68l19j5wq0hkid46km8z198m5b8cmj5gf0h6ryxnv01sa6")))

(define-public crate-sleepfast-1 (crate (name "sleepfast") (vers "1.0.0") (hash "0gvmjd89gn56jy0w14fbmxli5mvjg03q6ly3r53cg6v0qsz4kqvp") (features (quote (("nightly"))))))

(define-public crate-sleepfast-1 (crate (name "sleepfast") (vers "1.1.0") (hash "1d74xgz2ny3fk130fjzgx5pcasmm6lv41mlhz1d2k0sl49hkhxqq") (features (quote (("nightly"))))))

(define-public crate-sleepfast-1 (crate (name "sleepfast") (vers "1.2.0") (hash "0qzk08696pvq37226v7q907z2pgkyqy52x4hplq2scx8kaxc4x59") (features (quote (("nightly"))))))

(define-public crate-sleepfast-1 (crate (name "sleepfast") (vers "1.3.0") (hash "1dbx7gfh91sdv0xjv3smyashbzk4b70hqq7lfbxpg3hfk8lcawrz")))

(define-public crate-sleepy-1 (crate (name "sleepy") (vers "1.0.0") (hash "1lsg6pmvk24p7vay2bags3spxw1v7wg0izxycjrmk0079ig7c5vq") (yanked #t)))

(define-public crate-sleepy-1 (crate (name "sleepy") (vers "1.0.1") (hash "0lic1r2ggqw13jsg6dxmwb7sgk2kdf9sapykbbnva9w6vcjw117n")))

(define-public crate-sleepycat-0.1 (crate (name "sleepycat") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "~2.33") (default-features #t) (kind 0)))) (hash "0sr38bb8cq3fs4fh0sbah9x9w1ravalni80a7iakfmigcbvqwfws")))

(define-public crate-sleepycat-0.1 (crate (name "sleepycat") (vers "0.1.1") (deps (list (crate-dep (name "clap") (req "~2.33") (default-features #t) (kind 0)))) (hash "1fsc44435fmy2v24vbdzlqsbq5zas7myfbmxicxgybidl7dybmk4")))

(define-public crate-sleepycat-0.1 (crate (name "sleepycat") (vers "0.1.2") (deps (list (crate-dep (name "clap") (req "~2.33") (default-features #t) (kind 0)))) (hash "1wk4n592v3d1gs0373kf96vphssjzl7mn4m89vxyc7r646xqlr2l")))

