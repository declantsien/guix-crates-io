(define-module (crates-io sl ou) #:use-module (crates-io))

(define-public crate-sloughi-0.1 (crate (name "sloughi") (vers "0.1.0") (hash "0r8mgd21fgrixfpgsyr4m4kpr95pdp2pzh0knzk4q9x0m80r4npm") (yanked #t) (rust-version "1.59")))

(define-public crate-sloughi-0.2 (crate (name "sloughi") (vers "0.2.0") (hash "0hshirjvmzpdak04c4sh5d8dg339dmwyfxmd00vawn4sfrmpvcyh") (rust-version "1.59")))

(define-public crate-sloughi-0.3 (crate (name "sloughi") (vers "0.3.0") (hash "1l5z6c8a2vjbxvrzywjvljx4jsskcpw6vcbprfyhvn546bjgws85")))

