(define-module (crates-io sl is) #:use-module (crates-io))

(define-public crate-slist-0.1 (crate (name "slist") (vers "0.1.0") (deps (list (crate-dep (name "slist-derive") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "12w6bf5gd23iissx2rizhdvm7yigbpw2cp9a5lq9kpy64l0a0vcg")))

(define-public crate-slist-0.1 (crate (name "slist") (vers "0.1.1") (deps (list (crate-dep (name "slist-derive") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "0r6rmrnbdvgnvys819l5bx4b4s0fg6f9ankzllgyp9l6bw81f5wm")))

(define-public crate-slist-0.1 (crate (name "slist") (vers "0.1.2") (deps (list (crate-dep (name "slist-derive") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "1j5iwy81q5qbfmj15q9k5nnn8if19cpncsja6xvy6qy1wavx930h")))

(define-public crate-slist-derive-0.1 (crate (name "slist-derive") (vers "0.1.0") (hash "1ibc84ab6k5r295iim11vpy2akp21g6asa6014bmifq5i430dx2i")))

