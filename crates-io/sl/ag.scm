(define-module (crates-io sl ag) #:use-module (crates-io))

(define-public crate-slag-0.1 (crate (name "slag") (vers "0.1.0") (deps (list (crate-dep (name "docopt") (req "^0.6") (default-features #t) (kind 0)) (crate-dep (name "syntex_syntax") (req "^0.7.0") (default-features #t) (kind 0)))) (hash "12prxxjah9mzl21aap47752lglqccwnsy75n1p3df9vzj6vp4cn9")))

(define-public crate-slag-0.1 (crate (name "slag") (vers "0.1.1") (deps (list (crate-dep (name "docopt") (req "^0.6") (default-features #t) (kind 0)) (crate-dep (name "syntex_syntax") (req "^0.7.0") (default-features #t) (kind 0)))) (hash "0zgjhln3ymcnq88xmb6k9fibcl1cac2ldjfb3lmc7l2hdai277gm")))

