(define-module (crates-io sl _t) #:use-module (crates-io))

(define-public crate-sl_time_convert-0.1 (crate (name "sl_time_convert") (vers "0.1.0") (deps (list (crate-dep (name "chrono") (req "^0.4.6") (default-features #t) (kind 0)) (crate-dep (name "time") (req "^0.1.42") (default-features #t) (kind 0)))) (hash "1ngcidwfsm0y1xwdlc81fzin443yhlh6js69wia00y5w9dkd7zhw")))

(define-public crate-sl_time_convert-0.3 (crate (name "sl_time_convert") (vers "0.3.2") (deps (list (crate-dep (name "chrono") (req "^0.4.6") (default-features #t) (kind 0)) (crate-dep (name "time") (req "^0.1.42") (default-features #t) (kind 0)))) (hash "1m7dnsvjyqj4cklxwhpnwciz7x3f5smqyqgyvcsr1jwgk8rpc0lj")))

(define-public crate-sl_time_convert-0.4 (crate (name "sl_time_convert") (vers "0.4.0") (deps (list (crate-dep (name "chrono") (req "^0.4.6") (default-features #t) (kind 0)) (crate-dep (name "time") (req "^0.1.42") (default-features #t) (kind 0)))) (hash "09xvgd2j6c677sfzph9y12zgiaknn7086mzsklp48s5krvx56ajq")))

