(define-module (crates-io sl oc) #:use-module (crates-io))

(define-public crate-sloc-0.0.1 (crate (name "sloc") (vers "0.0.1") (deps (list (crate-dep (name "clap") (req "^2.20.5") (default-features #t) (kind 0)))) (hash "1fv5jwkvlynlfz8cm4n1vga3zfxsvvfyiij90kcamssvkb72b1i4")))

(define-public crate-slock-0.1 (crate (name "slock") (vers "0.1.0") (deps (list (crate-dep (name "futures") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.4") (default-features #t) (kind 2)))) (hash "1ww6nq9rx1x5fqvjj7d1p92c30phpfpvmk8ql54n65zh9fqph0s2")))

(define-public crate-slock-0.1 (crate (name "slock") (vers "0.1.1") (deps (list (crate-dep (name "futures") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.4") (default-features #t) (kind 2)))) (hash "0mw0n14jzmmaa5di1dqghq0p533rn1dj74rzdlb71jz4qgg2p67m")))

(define-public crate-slock-0.1 (crate (name "slock") (vers "0.1.2") (deps (list (crate-dep (name "async-std") (req "^1.6") (default-features #t) (kind 0)) (crate-dep (name "futures") (req "^0.3") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "futures") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "lazy_static") (req "^1.4") (default-features #t) (kind 2)))) (hash "0c2d6yb0nnzmv2jfr3xh327hncc8acyx5afqpk3ac38r1bbhn3g6") (features (quote (("default" "futures" "blocking") ("blocking" "futures"))))))

(define-public crate-slock-0.2 (crate (name "slock") (vers "0.2.0") (deps (list (crate-dep (name "lazy_static") (req "^1.4") (default-features #t) (kind 2)) (crate-dep (name "tokio") (req "^1.22") (features (quote ("sync" "time" "rt" "macros"))) (default-features #t) (kind 0)))) (hash "0jhqqqq191imjk4sidr4nzpbscmbd8lqjfklih25m3igx4fa8riv")))

(define-public crate-slock-0.2 (crate (name "slock") (vers "0.2.1") (deps (list (crate-dep (name "lazy_static") (req "^1.4") (default-features #t) (kind 2)) (crate-dep (name "tokio") (req "^1.22") (features (quote ("sync" "time" "rt" "macros"))) (default-features #t) (kind 0)))) (hash "1shj7a0ynlqfzsjdp2k7w1gvfhyjj2nzz3wpjx7x7992ccbzv6xp")))

