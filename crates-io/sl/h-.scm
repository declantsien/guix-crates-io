(define-module (crates-io sl h-) #:use-module (crates-io))

(define-public crate-slh-dsa-0.0.1 (crate (name "slh-dsa") (vers "0.0.1") (hash "17y1w0kb210l8pyf6nqxk0wdzy166545cwhrvxqh88gcnp1ja51f")))

(define-public crate-slh-dsa-rs-0.1 (crate (name "slh-dsa-rs") (vers "0.1.0") (hash "168sdzzbch2x6lhj1flzqbnzzf0z9kxn636q56df58cmk4hwab7b") (yanked #t) (rust-version "1.73")))

(define-public crate-slh-dsa-rs-0.1 (crate (name "slh-dsa-rs") (vers "0.1.1") (hash "06vy6sfbh2k2zf1l7lkvp3dbs2375yz349wfckmr8hc663jknpxy") (rust-version "1.73")))

