(define-module (crates-io sl ic) #:use-module (crates-io))

(define-public crate-slice-0.0.1 (crate (name "slice") (vers "0.0.1") (hash "1vhirqxg34qjmjd9pxjqcmshgfhcz7qsvbb1msmld7kcjrlrn0z5")))

(define-public crate-slice-0.0.2 (crate (name "slice") (vers "0.0.2") (hash "1sgadmwjqmswiwv9bmgx8mhssl0zs9rhxlqjq57n82k81hcr4zm0")))

(define-public crate-slice-0.0.3 (crate (name "slice") (vers "0.0.3") (hash "0ii3z451h9swdnbwjsvxfdqlirwx7a5ddplvrs29dx5bnc2q6p4q")))

(define-public crate-slice-0.0.4 (crate (name "slice") (vers "0.0.4") (hash "0dzrck37gk6bzb7ah4crdcnrzhyy20zzc2bybd8mnxd02561g3d4")))

(define-public crate-slice-arena-1 (crate (name "slice-arena") (vers "1.0.0") (hash "0icjmb15lw08x090fy0xagd6nm04zpgwa87hf1xv5s3zvkz15vnl")))

(define-public crate-slice-by-8-0.1 (crate (name "slice-by-8") (vers "0.1.0") (deps (list (crate-dep (name "cc") (req "^1.0") (default-features #t) (kind 1)))) (hash "100ij8fj3yps83n956lf11icnpw4v17kg1m1c28q4ga09spy53y8") (yanked #t)))

(define-public crate-slice-by-8-1 (crate (name "slice-by-8") (vers "1.0.0") (deps (list (crate-dep (name "cc") (req "^1.0") (default-features #t) (kind 1)))) (hash "1q2z7zzf6a9v7x32lqm8pwvpf16v9yd1bllh4ak4km82xzvmhksl") (yanked #t)))

(define-public crate-slice-by-8-1 (crate (name "slice-by-8") (vers "1.0.1") (deps (list (crate-dep (name "cc") (req "^1.0") (default-features #t) (kind 1)))) (hash "1jadf4rchr1gc3c8s7sixx42r53hsfyx2r7j9qg8nc1kibi1lyy5") (yanked #t)))

(define-public crate-slice-by-8-1 (crate (name "slice-by-8") (vers "1.0.2") (deps (list (crate-dep (name "cc") (req "^1.0") (default-features #t) (kind 1)))) (hash "0v0giky3ajvw8pfp7pb7x7xnr9ajn3n78c4a1mfklzl0jsy2lv7a") (yanked #t)))

(define-public crate-slice-by-8-1 (crate (name "slice-by-8") (vers "1.0.3") (deps (list (crate-dep (name "cc") (req "^1.0") (default-features #t) (kind 1)))) (hash "1dmsiq6knvk9q8mq1iy1a4yvvzzayg0q4pqr8idih0kndb67kmp2") (yanked #t)))

(define-public crate-slice-by-8-1 (crate (name "slice-by-8") (vers "1.0.4") (deps (list (crate-dep (name "cc") (req "^1.0") (default-features #t) (kind 1)))) (hash "1k39kfd9v8px08rln11amh3f5z4kssgjw58ldsm6c5qfdskzjk3m") (yanked #t)))

(define-public crate-slice-by-8-1 (crate (name "slice-by-8") (vers "1.0.5") (deps (list (crate-dep (name "cc") (req "^1.0") (default-features #t) (kind 1)))) (hash "1max9ax79cwgqpd5x9rhwy2bf06v0m2qn5yvqxhnn38a1glzb1lm") (yanked #t)))

(define-public crate-slice-by-8-1 (crate (name "slice-by-8") (vers "1.0.6") (deps (list (crate-dep (name "cc") (req "^1.0") (default-features #t) (kind 1)))) (hash "102dgjjpl87fx41llj21jr5cqxpjw661iic9jssp8vfkwg7nlkdg") (yanked #t)))

(define-public crate-slice-cast-0.1 (crate (name "slice-cast") (vers "0.1.0") (deps (list (crate-dep (name "clippy") (req "^0.0") (optional #t) (default-features #t) (kind 0)))) (hash "0dwp273p2znqg7wn9xs9i0sw1df9c23mpxfpnqw9p87nrhqkxkx2") (yanked #t)))

(define-public crate-slice-cast-0.1 (crate (name "slice-cast") (vers "0.1.1") (deps (list (crate-dep (name "clippy") (req "^0.0") (optional #t) (default-features #t) (kind 0)))) (hash "15mmnf5ygrypg0nd1nd26wwqvg1h1vmpkjijcpnkhwaphkqp6hfx") (yanked #t)))

(define-public crate-slice-cast-0.1 (crate (name "slice-cast") (vers "0.1.2") (deps (list (crate-dep (name "clippy") (req "^0.0") (optional #t) (default-features #t) (kind 0)))) (hash "12793chfrzr9n16kna28jjp86z331qd78ysizy026g6adxbr27yf") (yanked #t)))

(define-public crate-slice-cell-0.0.1 (crate (name "slice-cell") (vers "0.0.1") (hash "1pmqjnsh7rj5y97ds5xq3lkzdm5rc42x4fv0f7vxplqax3y0kw54") (features (quote (("std" "alloc") ("rc" "alloc") ("default" "std" "assume_cell_layout" "rc") ("assume_cell_layout") ("alloc"))))))

(define-public crate-slice-cell-0.0.2 (crate (name "slice-cell") (vers "0.0.2") (hash "0fyzn2pgvjdcj5jv9a55bfj1vvwm62kricv0by393s0vvf2g665l") (features (quote (("std" "alloc") ("rc" "alloc") ("default" "std" "assume_cell_layout" "rc") ("assume_cell_layout") ("alloc"))))))

(define-public crate-slice-cell-0.0.3 (crate (name "slice-cell") (vers "0.0.3") (hash "0a0xqywkncs1iqcwfd4g5pmvk3dd1pgzzq2rw2baca9ksk55k9xk") (features (quote (("std" "alloc") ("rc" "alloc") ("default" "std" "assume_cell_layout" "rc") ("assume_cell_layout") ("alloc"))))))

(define-public crate-slice-cell-0.0.4 (crate (name "slice-cell") (vers "0.0.4") (deps (list (crate-dep (name "tokio") (req "^1.24.1") (optional #t) (kind 0)))) (hash "0z5vjfj31x13khqd92xsslm3yjcimgrrknwyvfhgva8gx8cgfzhz") (features (quote (("tokio_assumptions" "tokio") ("std" "alloc") ("rc" "alloc") ("default" "std" "assume_cell_layout" "rc") ("assume_cell_layout") ("alloc")))) (v 2) (features2 (quote (("tokio" "dep:tokio" "std"))))))

(define-public crate-slice-cell-0.0.5 (crate (name "slice-cell") (vers "0.0.5") (deps (list (crate-dep (name "tokio") (req "^1.24.1") (optional #t) (kind 0)))) (hash "1156ryaskrhjp721051mz4g2vwhx2bs41jqjizg2gx7mxi2p1km5") (features (quote (("tokio_assumptions" "tokio") ("std" "alloc") ("rc" "alloc") ("full" "std" "rc" "tokio") ("default" "std" "rc") ("alloc")))) (v 2) (features2 (quote (("tokio" "dep:tokio" "std"))))))

(define-public crate-slice-command-0.0.0 (crate (name "slice-command") (vers "0.0.0") (deps (list (crate-dep (name "clap") (req "^4.2.3") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "078jgad52q9s91yhkcan0wbligi6hvb2rkr60xcabqmcmjspxcbj")))

(define-public crate-slice-command-0.1 (crate (name "slice-command") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^4.2.4") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "1x8k90q3pk9z1fl3z68y51pdgf93666lsf8z25x3g9y8am04s0db")))

(define-public crate-slice-command-0.2 (crate (name "slice-command") (vers "0.2.0") (deps (list (crate-dep (name "clap") (req "^4.2.7") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "0hvmd4w88wvwgsnkd7nhi7j6lf7g65jhwh739jqnwj50c812av8m")))

(define-public crate-slice-command-0.2 (crate (name "slice-command") (vers "0.2.1") (deps (list (crate-dep (name "clap") (req "^4.3.0") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "14zpw1a02fhlh8ds3p6x74n6wp611jaz69jb3xxl6svagx1xr8hb")))

(define-public crate-slice-command-0.2 (crate (name "slice-command") (vers "0.2.2") (deps (list (crate-dep (name "clap") (req "^4.3.4") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "0j5y7p7h4c9znl4g1vf7ihlibjdhk9vq2rcx06kzih4rmq32psgl")))

(define-public crate-slice-command-0.3 (crate (name "slice-command") (vers "0.3.0") (deps (list (crate-dep (name "bytesize") (req "^1.2.0") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^4.3.8") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "0xs7jdfw1v2lr7qyr6nihqdlbmyq0jxpkpnwbi4xnh21x4l0qzg2")))

(define-public crate-slice-command-0.3 (crate (name "slice-command") (vers "0.3.1") (deps (list (crate-dep (name "bytesize") (req "^1.2.0") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^4.3.23") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "0bb72p0qi3qndza4ga8naygs33kz51gbyyjx468vm42xcn0q0n4f")))

(define-public crate-slice-command-0.4 (crate (name "slice-command") (vers "0.4.0") (deps (list (crate-dep (name "bytesize") (req "^1.3.0") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^4.4.6") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "1rcgvsnhahkq7qjjh7ibq08zsp4579llswx28vxsbib2q845pzl9")))

(define-public crate-slice-command-0.4 (crate (name "slice-command") (vers "0.4.1") (deps (list (crate-dep (name "bytesize") (req "^1.3.0") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^4.5.0") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "0lga0hb7zia7il28g8isms7gi8spmcai6lbmaawx0203p7xji30w")))

(define-public crate-slice-copy-0.1 (crate (name "slice-copy") (vers "0.1.0") (hash "0bcsnbqvkk8a8rshnz82jy151w0nk37r80hbdf8k6prrzdrrky4q")))

(define-public crate-slice-copy-0.2 (crate (name "slice-copy") (vers "0.2.0") (hash "0lb4hg2bdgkbmgs6y0z0yz2jbkr122mq7srmvybv564479w892cm") (features (quote (("nightly"))))))

(define-public crate-slice-copy-0.3 (crate (name "slice-copy") (vers "0.3.0") (hash "0ynh20n6d1fddn7v1lri220dx4bi51fg5i2frwi2vjndc930zq7m")))

(define-public crate-slice-deque-0.1 (crate (name "slice-deque") (vers "0.1.0") (deps (list (crate-dep (name "bytes") (req "0.4.*") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (target "cfg(target_os = \"linux\")") (kind 0)) (crate-dep (name "mach") (req "0.1.*") (default-features #t) (target "cfg(target_os = \"macos\")") (kind 0)) (crate-dep (name "winapi") (req "0.3.*") (features (quote ("memoryapi" "handleapi" "sysinfoapi" "winbase"))) (default-features #t) (target "cfg(target_os = \"windows\")") (kind 0)))) (hash "1ikrcdrs4k7qkhf2k25ccldwmisqwyw0xrchq00izardf1j7s9g5") (features (quote (("bytes_buf" "bytes"))))))

(define-public crate-slice-deque-0.1 (crate (name "slice-deque") (vers "0.1.1") (deps (list (crate-dep (name "bytes") (req "0.4.*") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (target "cfg(target_os = \"linux\")") (kind 0)) (crate-dep (name "mach") (req "0.1.*") (default-features #t) (target "cfg(target_os = \"macos\")") (kind 0)) (crate-dep (name "winapi") (req "0.3.*") (features (quote ("memoryapi" "handleapi" "sysinfoapi" "winbase"))) (default-features #t) (target "cfg(target_os = \"windows\")") (kind 0)))) (hash "03s8f7l3rwj13hvvy5ai5na9ywvwi8jx5xlahrgv76cg2sf9ziz0") (features (quote (("bytes_buf" "bytes"))))))

(define-public crate-slice-deque-0.1 (crate (name "slice-deque") (vers "0.1.2") (deps (list (crate-dep (name "bytes") (req "0.4.*") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (target "cfg(target_os = \"android\")") (kind 0)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (target "cfg(target_os = \"linux\")") (kind 0)) (crate-dep (name "mach") (req "0.1.*") (default-features #t) (target "cfg(target_os = \"macos\")") (kind 0)) (crate-dep (name "winapi") (req "0.3.*") (features (quote ("memoryapi" "handleapi" "sysinfoapi" "winbase"))) (default-features #t) (target "cfg(target_os = \"windows\")") (kind 0)))) (hash "0cy6v093a8vi8hif34qbs8ymf4glj5fdkc0597dwd5fjqv8cihwg") (features (quote (("unstable") ("bytes_buf" "bytes"))))))

(define-public crate-slice-deque-0.1 (crate (name "slice-deque") (vers "0.1.3") (deps (list (crate-dep (name "bytes") (req "0.4.*") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (target "cfg(target_os = \"android\")") (kind 0)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (target "cfg(target_os = \"linux\")") (kind 0)) (crate-dep (name "mach") (req "0.1.*") (default-features #t) (target "cfg(target_os = \"macos\")") (kind 0)) (crate-dep (name "winapi") (req "0.3.*") (features (quote ("memoryapi" "handleapi" "sysinfoapi" "winbase"))) (default-features #t) (target "cfg(target_os = \"windows\")") (kind 0)))) (hash "1h5xzz3i95wz6ychkfargh2yzk3dbw06mkwp49wrc2ky8z5w16vn") (features (quote (("unstable") ("bytes_buf" "bytes"))))))

(define-public crate-slice-deque-0.1 (crate (name "slice-deque") (vers "0.1.4") (deps (list (crate-dep (name "bytes") (req "0.4.*") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (target "cfg(target_os = \"android\")") (kind 0)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (target "cfg(target_os = \"linux\")") (kind 0)) (crate-dep (name "mach") (req "0.1.*") (default-features #t) (target "cfg(target_os = \"macos\")") (kind 0)) (crate-dep (name "winapi") (req "0.3.*") (features (quote ("memoryapi" "handleapi" "sysinfoapi" "winbase"))) (default-features #t) (target "cfg(target_os = \"windows\")") (kind 0)))) (hash "1ndvln0f7dsslfykm698i281m4mj86s223nxh2lwid3qnl9k254r") (features (quote (("unstable") ("bytes_buf" "bytes"))))))

(define-public crate-slice-deque-0.1 (crate (name "slice-deque") (vers "0.1.5") (deps (list (crate-dep (name "bytes") (req "0.4.*") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (target "cfg(target_os = \"android\")") (kind 0)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (target "cfg(target_os = \"linux\")") (kind 0)) (crate-dep (name "mach") (req "0.1.*") (default-features #t) (target "cfg(target_os = \"macos\")") (kind 0)) (crate-dep (name "winapi") (req "0.3.*") (features (quote ("memoryapi" "handleapi" "sysinfoapi" "winbase"))) (default-features #t) (target "cfg(target_os = \"windows\")") (kind 0)))) (hash "1fq499hxlg7xphiclzg60dl8gz95qpm4lzcd19z0rlkpxl1xsr2g") (features (quote (("unstable") ("bytes_buf" "bytes"))))))

(define-public crate-slice-deque-0.1 (crate (name "slice-deque") (vers "0.1.6") (deps (list (crate-dep (name "libc") (req "^0.2") (target "cfg(any(unix, target_os = \"dragonfly\"))") (kind 0)) (crate-dep (name "mach") (req "^0.2") (target "cfg(all(any(target_os = \"macos\", target_os = \"ios\"), not(feature = \"unix_sysv\")))") (kind 0)) (crate-dep (name "winapi") (req "0.3.*") (features (quote ("memoryapi" "handleapi" "sysinfoapi" "winbase"))) (target "cfg(target_os = \"windows\")") (kind 0)))) (hash "164b69jpxms6lvnm7aykw8rs4jkv406qibx69rgivnjc8has0lfh") (features (quote (("use_std" "libc/use_std" "mach/use_std") ("unstable" "mach/unstable") ("unix_sysv") ("default" "use_std"))))))

(define-public crate-slice-deque-0.1 (crate (name "slice-deque") (vers "0.1.8") (deps (list (crate-dep (name "libc") (req "^0.2") (target "cfg(any(unix, target_os = \"dragonfly\"))") (kind 0)) (crate-dep (name "mach") (req "^0.2") (target "cfg(all(any(target_os = \"macos\", target_os = \"ios\"), not(feature = \"unix_sysv\")))") (kind 0)) (crate-dep (name "winapi") (req "0.3.*") (features (quote ("memoryapi" "handleapi" "sysinfoapi" "winbase"))) (target "cfg(target_os = \"windows\")") (kind 0)))) (hash "0r3ikazkhzs4m1zdzi45wqnw17a6i8fwipfxcl07x9gbasz4v125") (features (quote (("use_std" "libc/use_std" "mach/use_std") ("unstable" "mach/unstable") ("unix_sysv") ("default" "use_std"))))))

(define-public crate-slice-deque-0.1 (crate (name "slice-deque") (vers "0.1.9") (deps (list (crate-dep (name "libc") (req "^0.2") (target "cfg(any(unix, target_os = \"dragonfly\"))") (kind 0)) (crate-dep (name "mach") (req "^0.2") (target "cfg(all(any(target_os = \"macos\", target_os = \"ios\"), not(feature = \"unix_sysv\")))") (kind 0)) (crate-dep (name "winapi") (req "0.3.*") (features (quote ("memoryapi" "handleapi" "sysinfoapi" "winbase"))) (target "cfg(target_os = \"windows\")") (kind 0)))) (hash "1zl0mz840g8gfr1i9bv8mkmivkky3cr6a4lprpjrs99pjlxnnr1q") (features (quote (("use_std" "libc/use_std" "mach/use_std") ("unstable" "mach/unstable") ("unix_sysv") ("default" "use_std"))))))

(define-public crate-slice-deque-0.1 (crate (name "slice-deque") (vers "0.1.10") (deps (list (crate-dep (name "libc") (req "^0.2") (target "cfg(any(unix, target_os = \"dragonfly\"))") (kind 0)) (crate-dep (name "mach") (req "^0.2") (target "cfg(all(any(target_os = \"macos\", target_os = \"ios\"), not(feature = \"unix_sysv\")))") (kind 0)) (crate-dep (name "winapi") (req "0.3.*") (features (quote ("memoryapi" "handleapi" "sysinfoapi" "winbase"))) (target "cfg(target_os = \"windows\")") (kind 0)))) (hash "0ians4a92f7428rpksdyz1nb2k4i2pxfwh2gvl7jjlx0mwdvry44") (features (quote (("use_std" "libc/use_std" "mach/use_std") ("unstable" "mach/unstable") ("unix_sysv") ("default" "use_std"))))))

(define-public crate-slice-deque-0.1 (crate (name "slice-deque") (vers "0.1.11") (deps (list (crate-dep (name "libc") (req "^0.2") (target "cfg(any(unix, target_os = \"dragonfly\"))") (kind 0)) (crate-dep (name "mach") (req "^0.2") (target "cfg(all(any(target_os = \"macos\", target_os = \"ios\"), not(feature = \"unix_sysv\")))") (kind 0)) (crate-dep (name "winapi") (req "0.3.*") (features (quote ("memoryapi" "handleapi" "sysinfoapi" "winbase"))) (target "cfg(target_os = \"windows\")") (kind 0)))) (hash "1jpjl608bvx0882izm96mhm83lvnas37nb8ff9cw0kspvsaz99fr") (features (quote (("use_std" "libc/use_std" "mach/use_std") ("unstable" "mach/unstable") ("unix_sysv") ("default" "use_std"))))))

(define-public crate-slice-deque-0.1 (crate (name "slice-deque") (vers "0.1.12") (deps (list (crate-dep (name "libc") (req "^0.2") (target "cfg(any(unix, target_os = \"dragonfly\"))") (kind 0)) (crate-dep (name "mach") (req "^0.2") (target "cfg(all(any(target_os = \"macos\", target_os = \"ios\"), not(feature = \"unix_sysv\")))") (kind 0)) (crate-dep (name "winapi") (req "0.3.*") (features (quote ("memoryapi" "handleapi" "sysinfoapi" "winbase"))) (target "cfg(target_os = \"windows\")") (kind 0)))) (hash "0cbrhpb02rrnqdadnw5igyjnhzqfpq9phv7qdhf09ia3w7d6rziq") (features (quote (("use_std" "libc/use_std" "mach/use_std") ("unstable" "mach/unstable") ("unix_sysv") ("default" "use_std"))))))

(define-public crate-slice-deque-0.1 (crate (name "slice-deque") (vers "0.1.13") (deps (list (crate-dep (name "libc") (req "^0.2") (target "cfg(any(unix, target_os = \"dragonfly\"))") (kind 0)) (crate-dep (name "mach") (req "^0.2") (target "cfg(all(any(target_os = \"macos\", target_os = \"ios\"), not(feature = \"unix_sysv\")))") (kind 0)) (crate-dep (name "winapi") (req "0.3.*") (features (quote ("memoryapi" "handleapi" "sysinfoapi" "winbase"))) (target "cfg(target_os = \"windows\")") (kind 0)))) (hash "11z42a4iz5nridap0gq30c8aqwgcc8lpjznvvv7v03ks4jcbprbr") (features (quote (("use_std" "libc/use_std" "mach/use_std") ("unstable" "mach/unstable") ("unix_sysv") ("default" "use_std"))))))

(define-public crate-slice-deque-0.1 (crate (name "slice-deque") (vers "0.1.14") (deps (list (crate-dep (name "libc") (req "^0.2") (target "cfg(any(unix, target_os = \"dragonfly\"))") (kind 0)) (crate-dep (name "mach") (req "^0.2") (target "cfg(all(any(target_os = \"macos\", target_os = \"ios\"), not(feature = \"unix_sysv\")))") (kind 0)) (crate-dep (name "winapi") (req "0.3.*") (features (quote ("memoryapi" "handleapi" "sysinfoapi" "winbase"))) (target "cfg(target_os = \"windows\")") (kind 0)))) (hash "0a9mckw2z2l7b8cdsamvqy1zdgm07xams5sy89awxh57hym194dz") (features (quote (("use_std" "libc/use_std" "mach/use_std") ("unstable" "mach/unstable") ("unix_sysv") ("default" "use_std"))))))

(define-public crate-slice-deque-0.1 (crate (name "slice-deque") (vers "0.1.15") (deps (list (crate-dep (name "libc") (req "^0.2") (target "cfg(any(unix, target_os = \"dragonfly\"))") (kind 0)) (crate-dep (name "mach") (req "^0.2") (target "cfg(all(any(target_os = \"macos\", target_os = \"ios\"), not(feature = \"unix_sysv\")))") (kind 0)) (crate-dep (name "winapi") (req "0.3.*") (features (quote ("memoryapi" "handleapi" "sysinfoapi" "winbase"))) (target "cfg(target_os = \"windows\")") (kind 0)))) (hash "076786n6zqllb9w8n05p77sl5cradfn8xd6g5w5b609cxccdwrfq") (features (quote (("use_std" "libc/use_std" "mach/use_std") ("unstable" "mach/unstable") ("unix_sysv") ("default" "use_std"))))))

(define-public crate-slice-deque-0.1 (crate (name "slice-deque") (vers "0.1.16") (deps (list (crate-dep (name "libc") (req "^0.2") (target "cfg(any(unix, target_os = \"dragonfly\"))") (kind 0)) (crate-dep (name "mach") (req "^0.2") (target "cfg(all(any(target_os = \"macos\", target_os = \"ios\"), not(feature = \"unix_sysv\")))") (kind 0)) (crate-dep (name "winapi") (req "0.3.*") (features (quote ("memoryapi" "handleapi" "sysinfoapi" "winbase"))) (target "cfg(target_os = \"windows\")") (kind 0)))) (hash "157lpgi7y2ad1q2m588n7gx3izkb7lwz88cf9aa03qhhim3wm7yk") (features (quote (("use_std" "libc/use_std" "mach/use_std") ("unstable" "mach/unstable") ("unix_sysv") ("default" "use_std"))))))

(define-public crate-slice-deque-0.2 (crate (name "slice-deque") (vers "0.2.0") (deps (list (crate-dep (name "libc") (req "^0.2") (target "cfg(any(unix, target_os = \"dragonfly\"))") (kind 0)) (crate-dep (name "mach") (req "^0.2") (target "cfg(all(any(target_os = \"macos\", target_os = \"ios\"), not(feature = \"unix_sysv\")))") (kind 0)) (crate-dep (name "winapi") (req "0.3.*") (features (quote ("memoryapi" "handleapi" "sysinfoapi" "winbase"))) (target "cfg(target_os = \"windows\")") (kind 0)))) (hash "0vdw692ff3rkcraigl7c927xsc95j879ppfc5v1rc181gxkmanxg") (features (quote (("use_std" "libc/use_std" "mach/use_std") ("unstable" "mach/unstable") ("unix_sysv") ("default" "use_std"))))))

(define-public crate-slice-deque-0.2 (crate (name "slice-deque") (vers "0.2.2") (deps (list (crate-dep (name "libc") (req "^0.2") (target "cfg(any(unix, target_os = \"dragonfly\"))") (kind 0)) (crate-dep (name "mach") (req "^0.2") (target "cfg(all(any(target_os = \"macos\", target_os = \"ios\"), not(feature = \"unix_sysv\")))") (kind 0)) (crate-dep (name "winapi") (req "0.3.*") (features (quote ("memoryapi" "handleapi" "sysinfoapi" "winbase"))) (target "cfg(target_os = \"windows\")") (kind 0)))) (hash "0g3kbnk0qz06jggv350nycbn6grydl1n35lam591pnj8cffgpx2r") (features (quote (("use_std" "libc/use_std" "mach/use_std") ("unstable" "mach/unstable") ("unix_sysv") ("default" "use_std"))))))

(define-public crate-slice-deque-0.2 (crate (name "slice-deque") (vers "0.2.3") (deps (list (crate-dep (name "libc") (req "^0.2") (target "cfg(any(unix, target_os = \"dragonfly\"))") (kind 0)) (crate-dep (name "mach") (req "^0.2") (target "cfg(all(any(target_os = \"macos\", target_os = \"ios\"), not(feature = \"unix_sysv\")))") (kind 0)) (crate-dep (name "winapi") (req "0.3.*") (features (quote ("memoryapi" "handleapi" "sysinfoapi" "winbase"))) (target "cfg(target_os = \"windows\")") (kind 0)))) (hash "1cqp4r29rdbr01cpnyb8zmjbbcx3v85j47fywf98wp7r11kl6b63") (features (quote (("use_std" "libc/use_std" "mach/use_std") ("unstable" "mach/unstable") ("unix_sysv") ("default" "use_std"))))))

(define-public crate-slice-deque-0.2 (crate (name "slice-deque") (vers "0.2.4") (deps (list (crate-dep (name "libc") (req "^0.2") (target "cfg(any(unix, target_os = \"dragonfly\"))") (kind 0)) (crate-dep (name "mach") (req "^0.2") (target "cfg(all(any(target_os = \"macos\", target_os = \"ios\"), not(feature = \"unix_sysv\")))") (kind 0)) (crate-dep (name "winapi") (req "0.3.*") (features (quote ("memoryapi" "handleapi" "sysinfoapi" "winbase"))) (target "cfg(target_os = \"windows\")") (kind 0)))) (hash "1mq78l0vfwabnyanb85amgzakfhdaxx455yq6cszd5zmynagbpgz") (features (quote (("use_std" "libc/use_std" "mach/use_std") ("unstable" "mach/unstable") ("unix_sysv") ("default" "use_std"))))))

(define-public crate-slice-deque-0.3 (crate (name "slice-deque") (vers "0.3.0") (deps (list (crate-dep (name "libc") (req "^0.2") (target "cfg(any(unix, target_os = \"dragonfly\"))") (kind 0)) (crate-dep (name "mach") (req "^0.3") (target "cfg(all(any(target_os = \"macos\", target_os = \"ios\"), not(feature = \"unix_sysv\")))") (kind 0)) (crate-dep (name "winapi") (req "0.3.*") (features (quote ("memoryapi" "handleapi" "sysinfoapi" "winbase"))) (target "cfg(target_os = \"windows\")") (kind 0)))) (hash "098gvqjw52qw4gac567c9hx3y6hw9al7hjqb5mnvmvydh3i6xvri") (features (quote (("use_std" "libc/use_std") ("unix_sysv") ("default" "use_std"))))))

(define-public crate-slice-diff-patch-0.1 (crate (name "slice-diff-patch") (vers "0.1.0") (deps (list (crate-dep (name "lcs-diff") (req "^0.1.1") (default-features #t) (kind 0)) (crate-dep (name "wu-diff") (req "^0.1.2") (default-features #t) (kind 0)))) (hash "15qcl842qi6dixnhbw5mrvi8k99vap4l5b0ravi9il2y2pjsgq9h")))

(define-public crate-slice-diff-patch-0.2 (crate (name "slice-diff-patch") (vers "0.2.0") (deps (list (crate-dep (name "diff") (req "^0.1.12") (default-features #t) (kind 0)) (crate-dep (name "lcs-diff") (req "^0.1.1") (default-features #t) (kind 0)) (crate-dep (name "wu-diff") (req "^0.1.2") (default-features #t) (kind 0)))) (hash "0b6l8gy6bw9zr9iy7mvhqb4byv1zjs4qqbjvsy9smr40wh549qrn")))

(define-public crate-slice-diff-patch-0.2 (crate (name "slice-diff-patch") (vers "0.2.1") (deps (list (crate-dep (name "diff") (req "^0.1.12") (default-features #t) (kind 0)) (crate-dep (name "lcs-diff") (req "^0.1.1") (default-features #t) (kind 0)) (crate-dep (name "wu-diff") (req "^0.1.2") (default-features #t) (kind 0)))) (hash "1fbz81z4jjkp8h25fk6acwyr9vrz4a32lfl0fmy3x5nhhd7x4ndl")))

(define-public crate-slice-diff-patch-0.2 (crate (name "slice-diff-patch") (vers "0.2.2") (deps (list (crate-dep (name "diff") (req "^0.1.12") (default-features #t) (kind 0)) (crate-dep (name "lcs-diff") (req "^0.1.1") (default-features #t) (kind 0)) (crate-dep (name "wu-diff") (req "^0.1.2") (default-features #t) (kind 0)))) (hash "066c2sqxwm6fc8ygkirfidw652ymb3hyzx6wy4ibp49200nv1gqz")))

(define-public crate-slice-diff-patch-0.3 (crate (name "slice-diff-patch") (vers "0.3.0") (deps (list (crate-dep (name "diff") (req "^0.1.12") (default-features #t) (kind 0)) (crate-dep (name "lcs-diff") (req "^0.1.1") (default-features #t) (kind 0)) (crate-dep (name "wu-diff") (req "^0.1.2") (default-features #t) (kind 0)))) (hash "19f01jhjdanavxqld0ny0j28n0ng91hja88hiigbfy7navhsaijg")))

(define-public crate-slice-diff-patch-1 (crate (name "slice-diff-patch") (vers "1.0.0") (deps (list (crate-dep (name "diff") (req "^0.1.12") (default-features #t) (kind 0)) (crate-dep (name "lcs-diff") (req "^0.1.1") (default-features #t) (kind 0)) (crate-dep (name "wu-diff") (req "^0.1.2") (default-features #t) (kind 0)))) (hash "0jryaf8fqisp6kwa92n0gj21g4d9xgj3rwvsf9nc0qdyq9dzbcsq")))

(define-public crate-slice-diff-patch-1 (crate (name "slice-diff-patch") (vers "1.1.0") (deps (list (crate-dep (name "diff") (req "^0.1.12") (default-features #t) (kind 0)) (crate-dep (name "lcs-diff") (req "^0.1.1") (default-features #t) (kind 0)) (crate-dep (name "wu-diff") (req "^0.1.2") (default-features #t) (kind 0)))) (hash "1a3mblgxmk6ngqbcm8fib4capww71si01pdd1chb5d15gm955yp7")))

(define-public crate-slice-diff-patch-1 (crate (name "slice-diff-patch") (vers "1.1.1") (deps (list (crate-dep (name "diff") (req "^0.1.12") (default-features #t) (kind 0)) (crate-dep (name "lcs-diff") (req "^0.1.1") (default-features #t) (kind 0)) (crate-dep (name "wu-diff") (req "^0.1.2") (default-features #t) (kind 0)))) (hash "1002gks633ncjs9a4lcfmniady2rxpghy6sy9xj6y1ivvahn5f68")))

(define-public crate-slice-diff-patch-1 (crate (name "slice-diff-patch") (vers "1.2.1") (deps (list (crate-dep (name "diff") (req "^0.1.12") (default-features #t) (kind 0)) (crate-dep (name "lcs-diff") (req "^0.1.1") (default-features #t) (kind 0)) (crate-dep (name "wu-diff") (req "^0.1.2") (default-features #t) (kind 0)))) (hash "04glp7ac20m2pb8kycm46cv6vmsajpcyj1zzaa8hp0slfcvvajfw")))

(define-public crate-slice-dst-1 (crate (name "slice-dst") (vers "1.0.0") (deps (list (crate-dep (name "autocfg") (req "^1.0.0") (default-features #t) (kind 1)) (crate-dep (name "erasable") (req "^1.0.0") (optional #t) (default-features #t) (kind 0)))) (hash "1gdy3bnhxq08cklmp2kiy38xphj9v6llgpfcfk0fy11n8ndrp6a2") (features (quote (("default" "erasable")))) (yanked #t)))

(define-public crate-slice-dst-1 (crate (name "slice-dst") (vers "1.1.0") (deps (list (crate-dep (name "autocfg") (req "^1.0.0") (default-features #t) (kind 1)) (crate-dep (name "erasable") (req "^1.0.0") (optional #t) (default-features #t) (kind 0)))) (hash "15mhclvq01bgbvqldg9qvb1g2qjkh03kn9c9mpvnz9liflaivih5") (features (quote (("default" "erasable")))) (yanked #t)))

(define-public crate-slice-dst-1 (crate (name "slice-dst") (vers "1.1.1") (deps (list (crate-dep (name "autocfg") (req "^1.0.0") (default-features #t) (kind 1)) (crate-dep (name "erasable") (req "^1.1.0") (optional #t) (default-features #t) (kind 0)))) (hash "0wihk6gj7qppfgdq780fi2l6dp2q5wgjlq1f2538g6s05yajzlhq") (features (quote (("default" "erasable")))) (yanked #t)))

(define-public crate-slice-dst-1 (crate (name "slice-dst") (vers "1.1.2") (deps (list (crate-dep (name "autocfg") (req "^1.0.0") (default-features #t) (kind 1)) (crate-dep (name "erasable") (req "^1.1.0") (optional #t) (default-features #t) (kind 0)))) (hash "1b2id0r1x622a3hfvwnldi3cn72bg5m3qaagirf7xlcjcgx03wv9") (features (quote (("default" "erasable")))) (yanked #t)))

(define-public crate-slice-dst-1 (crate (name "slice-dst") (vers "1.2.0") (deps (list (crate-dep (name "autocfg") (req "^1.0.0") (default-features #t) (kind 1)) (crate-dep (name "erasable") (req "^1.1.0") (optional #t) (default-features #t) (kind 0)))) (hash "1v97kc0lmq7wncy54icl2gab9lj4g152fnvb7c701wmall1y4d2r") (features (quote (("default" "erasable"))))))

(define-public crate-slice-dst-1 (crate (name "slice-dst") (vers "1.2.1") (deps (list (crate-dep (name "autocfg") (req "^1.0.0") (default-features #t) (kind 1)) (crate-dep (name "erasable") (req "^1.1.0") (optional #t) (default-features #t) (kind 0)))) (hash "0yrl62yyxpmcy2md77dlhzxaaad386irgrmx8i9cf8vv69n9nv34") (features (quote (("default" "erasable"))))))

(define-public crate-slice-dst-1 (crate (name "slice-dst") (vers "1.3.0") (deps (list (crate-dep (name "autocfg") (req "^1.0.0") (default-features #t) (kind 1)) (crate-dep (name "erasable") (req "^1.1.0") (optional #t) (default-features #t) (kind 0)))) (hash "1narz4xnzzqqjmbfx2gbvma5hnl8m0x9da0qd0fq73i4nscq9jba") (features (quote (("default" "erasable"))))))

(define-public crate-slice-dst-1 (crate (name "slice-dst") (vers "1.4.0") (deps (list (crate-dep (name "autocfg") (req "^1.0.0") (default-features #t) (kind 1)) (crate-dep (name "erasable") (req "^1.1.0") (optional #t) (default-features #t) (kind 0)))) (hash "1my1j5zd1q4a84bq1l13nwwd9k2yjf8s6f5mdnd1syc44rl070dz") (features (quote (("default" "erasable"))))))

(define-public crate-slice-dst-1 (crate (name "slice-dst") (vers "1.4.1") (deps (list (crate-dep (name "autocfg") (req "^1.0.0") (default-features #t) (kind 1)) (crate-dep (name "erasable") (req "^1.1.0") (optional #t) (default-features #t) (kind 0)))) (hash "0dkx3yqrmqf9sba50zs2pwclbxbix0dh4ap2kqha78q4pl159bcz") (features (quote (("default" "erasable"))))))

(define-public crate-slice-dst-1 (crate (name "slice-dst") (vers "1.5.0") (deps (list (crate-dep (name "autocfg") (req "^1.0.0") (default-features #t) (kind 1)) (crate-dep (name "erasable") (req "^1.1.0") (optional #t) (default-features #t) (kind 0)))) (hash "1i5an4a2mz7dqb321dz46hkx19f98jwkvqrmfiym5smzhwlxr1m1") (features (quote (("default" "erasable"))))))

(define-public crate-slice-dst-1 (crate (name "slice-dst") (vers "1.5.1") (deps (list (crate-dep (name "autocfg") (req "^1.0.0") (default-features #t) (kind 1)) (crate-dep (name "erasable") (req "^1.1.0") (optional #t) (default-features #t) (kind 0)))) (hash "04il428xdcfrvlixj3havp2k4x42dbd3wkk5x9y9khnplqhnf6pc") (features (quote (("default" "erasable"))))))

(define-public crate-slice-ext-0.1 (crate (name "slice-ext") (vers "0.1.0") (hash "0h22vy2akqbxwhamq4lj95nx8h9z8s59f2yarchwkdhhkkmilqsv")))

(define-public crate-slice-ext-0.1 (crate (name "slice-ext") (vers "0.1.1") (hash "11ch6xrlrjn2nmqhsj0jmsfdjxrvgqpzyrphf65sbsjdrd3ya48a")))

(define-public crate-slice-ext-0.1 (crate (name "slice-ext") (vers "0.1.2") (hash "0bf66ah2gn8pgzjmz4wlakfdzcw8i8ngqqzq8dq18vpcrwz99krh")))

(define-public crate-slice-ext-0.1 (crate (name "slice-ext") (vers "0.1.3") (hash "0pwwi7qxk3p2nj211m7129d7kcm2zcq00yn2iydrjvsq5f2whi91")))

(define-public crate-slice-ext-0.1 (crate (name "slice-ext") (vers "0.1.4") (hash "1aifd5fgn5hhgv6qdjrdzrp97wnw4wfa3h3g4nmxga77kihzhhja")))

(define-public crate-slice-fill-1 (crate (name "slice-fill") (vers "1.0.0") (deps (list (crate-dep (name "libc") (req "^0.2.68") (default-features #t) (kind 0)))) (hash "1nwjx3dycm35vfj43ziz8qq3c2v8vi2cl4fxi6a0drgai9l17c7h")))

(define-public crate-slice-fill-1 (crate (name "slice-fill") (vers "1.0.1") (deps (list (crate-dep (name "libc") (req "^0.2.68") (default-features #t) (kind 0)))) (hash "0n1b5sf1rl3z08yz1f6zzph0pz7aa95s6c98yqrjax6ad58ca0dp")))

(define-public crate-slice-group-by-0.1 (crate (name "slice-group-by") (vers "0.1.0") (deps (list (crate-dep (name "rand") (req "^0.5") (default-features #t) (kind 2)) (crate-dep (name "sdset") (req "^0.3") (default-features #t) (kind 0)))) (hash "0m1xgbfnrlc42s8lb6sx1ghchg7l5688dfhnfzszdq0q4am3xlwi") (features (quote (("nightly"))))))

(define-public crate-slice-group-by-0.2 (crate (name "slice-group-by") (vers "0.2.0") (deps (list (crate-dep (name "rand") (req "^0.5") (default-features #t) (kind 2)) (crate-dep (name "sdset") (req "^0.3") (default-features #t) (kind 0)))) (hash "0dika9mgdzin81vrv2l828bkxdzc1kq712kfkj6pfs7mlp7k2ksd") (features (quote (("nightly"))))))

(define-public crate-slice-group-by-0.2 (crate (name "slice-group-by") (vers "0.2.1") (deps (list (crate-dep (name "rand") (req "^0.5") (default-features #t) (kind 2)) (crate-dep (name "sdset") (req "^0.3") (default-features #t) (kind 0)))) (hash "1rxq3xl92qr6041pa027a8lgk4a4x2x0jfwdz5v6c5jqjzmd8q75") (features (quote (("nightly"))))))

(define-public crate-slice-group-by-0.2 (crate (name "slice-group-by") (vers "0.2.2") (deps (list (crate-dep (name "rand") (req "^0.5") (default-features #t) (kind 2)) (crate-dep (name "sdset") (req "^0.3") (default-features #t) (kind 0)))) (hash "14rzpdinrspbj3yidj1g19gx89qzhwdvz4b72m0z1fak52fqwlrm") (features (quote (("nightly"))))))

(define-public crate-slice-group-by-0.2 (crate (name "slice-group-by") (vers "0.2.3-alpha") (deps (list (crate-dep (name "rand") (req "^0.5") (default-features #t) (kind 2)))) (hash "0f70n6846q7pdkfvzr9fmrkzh5jbj4qc0drq2k27lzvrb82hlff4") (features (quote (("std") ("nightly") ("default" "std"))))))

(define-public crate-slice-group-by-0.2 (crate (name "slice-group-by") (vers "0.2.3") (deps (list (crate-dep (name "rand") (req "^0.5") (default-features #t) (kind 2)))) (hash "08arc72rgcj7d8wm7vyx7iav6znrgv77an7figp31d268rmwn5f1") (features (quote (("std") ("nightly") ("default" "std"))))))

(define-public crate-slice-group-by-0.2 (crate (name "slice-group-by") (vers "0.2.4") (deps (list (crate-dep (name "rand") (req "^0.5") (default-features #t) (kind 2)))) (hash "1c13fz29z3j09bm8dq8n273hz74rd0h4hr9jp5w9nz6j9rkrk584") (features (quote (("std") ("nightly") ("default" "std"))))))

(define-public crate-slice-group-by-0.2 (crate (name "slice-group-by") (vers "0.2.5") (deps (list (crate-dep (name "rand") (req "^0.6.5") (default-features #t) (kind 2)))) (hash "17vd2qx581hw4511is656mwwq8xils64il73pdcjmd9y5cxf23xb") (features (quote (("std") ("nightly") ("default" "std"))))))

(define-public crate-slice-group-by-0.2 (crate (name "slice-group-by") (vers "0.2.6") (deps (list (crate-dep (name "rand") (req "^0.6.5") (default-features #t) (kind 2)))) (hash "1yx765cnd8xz0f2ar7lmkn3bq5s6fisdjbmn18v2ilj6nvq78x0z") (features (quote (("std") ("nightly") ("default" "std"))))))

(define-public crate-slice-group-by-0.2 (crate (name "slice-group-by") (vers "0.2.7") (deps (list (crate-dep (name "rand") (req "^0.6.5") (default-features #t) (kind 2)))) (hash "16q20q5p95198r0cs5aywjxdyg667c7dk6bffxhqv9ld80ll6v9k") (features (quote (("std") ("nightly") ("default" "std")))) (yanked #t)))

(define-public crate-slice-group-by-0.3 (crate (name "slice-group-by") (vers "0.3.0") (deps (list (crate-dep (name "rand") (req "^0.6.5") (default-features #t) (kind 2)))) (hash "1v52vyzmwr0phb8zm2wpgig0fvsp12sl7qagijiv22lnggc39dh3") (features (quote (("std") ("nightly") ("default" "std"))))))

(define-public crate-slice-group-by-0.3 (crate (name "slice-group-by") (vers "0.3.1") (deps (list (crate-dep (name "rand") (req "^0.6.5") (default-features #t) (kind 2)))) (hash "19vbyyxqvc25fv2dmhlxijlk5sa9j34yb6hyydb9vf89kh36fqc2") (features (quote (("std") ("nightly") ("default" "std"))))))

(define-public crate-slice-of-array-0.1 (crate (name "slice-of-array") (vers "0.1.0") (deps (list (crate-dep (name "version-sync") (req "^0.3") (default-features #t) (kind 2)))) (hash "0ikcnjsjkqgh7vhmjr63j6q3a7cgg17rhd9ms5zrygs6cvkk1yk7")))

(define-public crate-slice-of-array-0.1 (crate (name "slice-of-array") (vers "0.1.1") (deps (list (crate-dep (name "version-sync") (req "^0.3") (default-features #t) (kind 2)))) (hash "09arbg8701pxfas4wv50b8ymajbaj9w42lylbncnjs31vzr3qp2h")))

(define-public crate-slice-of-array-0.2 (crate (name "slice-of-array") (vers "0.2.0") (deps (list (crate-dep (name "version-sync") (req "^0.3") (default-features #t) (kind 2)))) (hash "1rcy149jl634iv893wbr9fx9ahl0q9l978m1gvwd433rnlanmr7x")))

(define-public crate-slice-of-array-0.2 (crate (name "slice-of-array") (vers "0.2.1") (deps (list (crate-dep (name "version-sync") (req "^0.3") (default-features #t) (kind 2)))) (hash "0rv3j2hzqkyyfq6yai95b2c680d1w95m16sji13p46h5n4yfkzwg")))

(define-public crate-slice-of-array-0.3 (crate (name "slice-of-array") (vers "0.3.0") (deps (list (crate-dep (name "version-sync") (req "^0.9") (default-features #t) (kind 2)))) (hash "1m9gpk35snmlzvdzsf304s1qmxjjcsrb7rigqdhpb75q0pqdhnna")))

(define-public crate-slice-of-array-0.3 (crate (name "slice-of-array") (vers "0.3.1") (deps (list (crate-dep (name "version-sync") (req "^0.9") (default-features #t) (kind 2)))) (hash "0qqqnyihm8gi9a2dr9xralhyrmrbdf8hiq97czrljj3b4sskaxmg") (features (quote (("std"))))))

(define-public crate-slice-of-array-0.3 (crate (name "slice-of-array") (vers "0.3.2") (deps (list (crate-dep (name "version-sync") (req "^0.9") (default-features #t) (kind 2)))) (hash "116f8m4h89awryqhfvxl4gfpkzybqdl2k20wnbdb0k6bk2xj1wd4") (features (quote (("std"))))))

(define-public crate-slice-pool-0.1 (crate (name "slice-pool") (vers "0.1.0") (hash "1apfag72kx7sd45yjjkpj4mk7zb3jlqj3nfg27snnc5av8c2irpg")))

(define-public crate-slice-pool-0.2 (crate (name "slice-pool") (vers "0.2.0") (hash "0ajp9h9b3lmk2mqx59v453p5ayifal6cw4d87348mkp9k980aa4d")))

(define-public crate-slice-pool-0.3 (crate (name "slice-pool") (vers "0.3.0") (hash "0arxq4ssilwbd0mhzmkmh1y1frlyvvpgbqjmfd962fy9hrk1s5p3")))

(define-public crate-slice-pool-0.3 (crate (name "slice-pool") (vers "0.3.1") (hash "1hq5xvragihjpx9z7hrlim12vz6qsib55n77d854rkr01rjilpm9")))

(define-public crate-slice-pool-0.3 (crate (name "slice-pool") (vers "0.3.2") (hash "18awq9accvbcn7n5kq8ac2dynzdan2r3x3c3xs6az6gybngxgb6i")))

(define-public crate-slice-pool-0.3 (crate (name "slice-pool") (vers "0.3.3") (hash "05lm36x8v9ky2a1920zb91x1f4k02cz17pf7mai5wqkc27sr79dr")))

(define-public crate-slice-pool-0.3 (crate (name "slice-pool") (vers "0.3.4") (hash "1ajfp0nx6mygsfz5q7a0637l003pnics3hsc7w00k3skjlx9il90")))

(define-public crate-slice-pool-0.4 (crate (name "slice-pool") (vers "0.4.0") (hash "0nfc3grabc9ckl2x3irmqn5zy1pzlim504m4n1z982n2ds0ab58x")))

(define-public crate-slice-pool-0.4 (crate (name "slice-pool") (vers "0.4.1") (hash "0yifbvph8xmaqf88rjgwq88cj5sz9vmbvja2z0v82fmxy7jwcgvk")))

(define-public crate-slice-pool2-0.4 (crate (name "slice-pool2") (vers "0.4.2") (hash "059a518fv40hs6lpimz0xyi03n9bl4zwlch7lvjrcl15ihcvpx11")))

(define-public crate-slice-pool2-0.4 (crate (name "slice-pool2") (vers "0.4.3") (hash "1y1150phbpjd8pvn9dxk5qw6s8y0dami9a99pbzbv2dgajb6hgbs")))

(define-public crate-slice-rbtree-0.1 (crate (name "slice-rbtree") (vers "0.1.0-alpha") (deps (list (crate-dep (name "borsh") (req "^0.9.1") (default-features #t) (kind 0)) (crate-dep (name "bytemuck") (req "^1.7.2") (features (quote ("derive" "min_const_generics"))) (default-features #t) (kind 0)) (crate-dep (name "criterion") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "paste") (req "^1.0") (default-features #t) (kind 2)) (crate-dep (name "pretty_assertions") (req "^1.3.0") (default-features #t) (kind 2)))) (hash "0hp80qav5sn52ch7lj2c7kqf6fzr0bf0dzwnakad2wd7px9sdydj")))

(define-public crate-slice-rbtree-0.1 (crate (name "slice-rbtree") (vers "0.1.0-alpha.1") (deps (list (crate-dep (name "borsh") (req "^0.9.1") (features (quote ("const-generics"))) (kind 0)) (crate-dep (name "bytemuck") (req "^1.7.2") (features (quote ("derive" "min_const_generics"))) (default-features #t) (kind 0)) (crate-dep (name "criterion") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "paste") (req "^1.0") (default-features #t) (kind 2)) (crate-dep (name "pretty_assertions") (req "^1.3.0") (default-features #t) (kind 2)))) (hash "0fy4bqb9672xmn8j6hh51kvpw1x16bx5n7qf3a2dncvmpcs51klc")))

(define-public crate-slice-rbtree-0.1 (crate (name "slice-rbtree") (vers "0.1.0") (deps (list (crate-dep (name "arbitrary") (req "^1") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "borsh") (req "^0.9.1") (features (quote ("const-generics"))) (kind 0)) (crate-dep (name "bytemuck") (req "^1.7.2") (features (quote ("derive" "min_const_generics"))) (default-features #t) (kind 0)) (crate-dep (name "criterion") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "paste") (req "^1.0") (default-features #t) (kind 2)) (crate-dep (name "pretty_assertions") (req "^1.3.0") (default-features #t) (kind 2)))) (hash "0i2jv4amqrx5pbzvsb01c9rmg9m7jpk3i63fp7zfwbw861q5nn39")))

(define-public crate-slice-ring-buffer-0.3 (crate (name "slice-ring-buffer") (vers "0.3.1") (deps (list (crate-dep (name "libc") (req "^0.2") (target "cfg(any(unix, target_os = \"dragonfly\"))") (kind 0)) (crate-dep (name "mach") (req "^0.3") (target "cfg(all(any(target_os = \"macos\", target_os = \"ios\"), not(feature = \"unix_sysv\")))") (kind 0)) (crate-dep (name "winapi") (req "0.3.*") (features (quote ("memoryapi" "handleapi" "sysinfoapi" "winbase"))) (target "cfg(target_os = \"windows\")") (kind 0)))) (hash "0ciwk9qrxk6spfha58377y0q8gj9xcb0vzgdx8m3mqga02nhgrl8") (features (quote (("use_std" "libc/use_std") ("unix_sysv") ("default" "use_std")))) (yanked #t)))

(define-public crate-slice-ring-buffer-0.3 (crate (name "slice-ring-buffer") (vers "0.3.2") (deps (list (crate-dep (name "libc") (req "^0.2") (target "cfg(any(unix, target_os = \"dragonfly\"))") (kind 0)) (crate-dep (name "mach") (req "^0.3") (target "cfg(all(any(target_os = \"macos\", target_os = \"ios\"), not(feature = \"unix_sysv\")))") (kind 0)) (crate-dep (name "winapi") (req "0.3.*") (features (quote ("memoryapi" "handleapi" "sysinfoapi" "winbase"))) (target "cfg(target_os = \"windows\")") (kind 0)))) (hash "14yq42d844dr90whhm7cm9jki90icf2cd6d9f9k7mz82jd2fpj7w") (features (quote (("use_std" "libc/use_std") ("unix_sysv") ("default" "use_std"))))))

(define-public crate-slice-ring-buffer-0.3 (crate (name "slice-ring-buffer") (vers "0.3.3") (deps (list (crate-dep (name "libc") (req "^0.2") (target "cfg(any(unix, target_os = \"dragonfly\"))") (kind 0)) (crate-dep (name "mach2") (req "^0.4.1") (target "cfg(all(any(target_os = \"macos\", target_os = \"ios\"), not(feature = \"unix_sysv\")))") (kind 0)) (crate-dep (name "winapi") (req "0.3.*") (features (quote ("memoryapi" "handleapi" "sysinfoapi" "winbase"))) (target "cfg(target_os = \"windows\")") (kind 0)))) (hash "0xvb2ssx68wap25xy189x4ivpgciack4xp9fqgqgaqnbabjcg0bh") (features (quote (("use_std" "libc/use_std") ("unix_sysv") ("default" "use_std"))))))

(define-public crate-slice-string-0.1 (crate (name "slice-string") (vers "0.1.0") (deps (list (crate-dep (name "tinyvec") (req "^1.6.0") (default-features #t) (kind 0)))) (hash "0v5h0lnhn13xv34wy38jsxmq5pvrdjynbmnc7ysllkr6xfll4rz8")))

(define-public crate-slice-string-0.2 (crate (name "slice-string") (vers "0.2.0") (deps (list (crate-dep (name "tinyvec") (req "^1.6.0") (default-features #t) (kind 0)))) (hash "1afpv9gaqk14rlwq0qxbgvyy72l7qqms3072af0yprqmw5mqaxky")))

(define-public crate-slice-string-0.3 (crate (name "slice-string") (vers "0.3.0") (deps (list (crate-dep (name "tinyvec") (req "^1.6.0") (default-features #t) (kind 0)))) (hash "0wlh2xciqi8iwghghxwj37g7i7v3a1rrn46bn1n9z1c0r1frx7zx")))

(define-public crate-slice-string-0.4 (crate (name "slice-string") (vers "0.4.0") (deps (list (crate-dep (name "tinyvec") (req "^1.6.0") (default-features #t) (kind 0)))) (hash "0qld6h4hfxg248f9x2kca3rpgksild850kk0nqy0yx18nz57icq5")))

(define-public crate-slice-string-0.5 (crate (name "slice-string") (vers "0.5.0") (deps (list (crate-dep (name "tinyvec") (req "^1.6.0") (default-features #t) (kind 0)))) (hash "1vf562y9iyaaf1c1zvb8njyv21win3ls8kjz086i9cw72v160hcx")))

(define-public crate-slice-string-0.6 (crate (name "slice-string") (vers "0.6.0") (deps (list (crate-dep (name "tinyvec") (req "^1.6.0") (default-features #t) (kind 0)))) (hash "12q32516c1yj1gnjnbi6zpsvxbcs8pr00jx2rz797mgp6f8cmhq6")))

(define-public crate-slice-string-0.7 (crate (name "slice-string") (vers "0.7.0") (deps (list (crate-dep (name "tinyvec") (req "^1.6.0") (default-features #t) (kind 0)) (crate-dep (name "ufmt") (req "^0.2.0") (default-features #t) (kind 2)) (crate-dep (name "ufmt-write") (req "^0.1.0") (optional #t) (default-features #t) (kind 0)))) (hash "13qwlmrpf9ri67if26wxfp4hnxinyyrjpgxk7a8la648kpcs3kch") (features (quote (("default")))) (v 2) (features2 (quote (("ufmt-impl" "dep:ufmt-write"))))))

(define-public crate-slice-utils-1 (crate (name "slice-utils") (vers "1.0.0") (hash "0cf1i9mn4d8vw5299r3v4jmdhj7w1g1ax93bfand3152g22n7rqi") (features (quote (("std") ("default"))))))

(define-public crate-slice-utils-1 (crate (name "slice-utils") (vers "1.0.1") (hash "0q5gx8dn4x9zglb0fasfg5j8g3hinwij7c1p5rv6w2f7ffcq5nxr") (features (quote (("std") ("default"))))))

(define-public crate-slice-utils-1 (crate (name "slice-utils") (vers "1.0.2") (hash "1a9diyxryfaxw7zl8z0if80nxgx7ifdry7sczws2hwgah6wmx163") (features (quote (("std") ("default"))))))

(define-public crate-slice-utils-2 (crate (name "slice-utils") (vers "2.0.0") (deps (list (crate-dep (name "paste") (req "^1.0.14") (default-features #t) (kind 0)))) (hash "0y2i384smrh9h0v56r8n3mxdlj10yhf38fd7bgba95c8zwp91y8m") (features (quote (("std") ("default"))))))

(define-public crate-slice-utils-2 (crate (name "slice-utils") (vers "2.0.1") (deps (list (crate-dep (name "paste") (req "^1.0.14") (default-features #t) (kind 0)))) (hash "1gwldrfspl8ss96xg1mlygx6080j41ldjb5dk2v5nziliz3lhw4c") (features (quote (("std") ("default"))))))

(define-public crate-slice-utils-2 (crate (name "slice-utils") (vers "2.0.2") (deps (list (crate-dep (name "paste") (req "^1.0.14") (default-features #t) (kind 0)))) (hash "1mi2b384cxcbmyrf4c6f1mza1w0ldqbrayq6c530fmraa69yf25z") (features (quote (("std") ("default"))))))

(define-public crate-slice-utils-2 (crate (name "slice-utils") (vers "2.0.3") (deps (list (crate-dep (name "paste") (req "^1.0.14") (default-features #t) (kind 0)))) (hash "0p82ia4g60d69ijpjbv4jy5867hvi0r5g01vjff5dihkqwfp9m88") (features (quote (("std") ("default"))))))

(define-public crate-slice-utils-2 (crate (name "slice-utils") (vers "2.1.0") (deps (list (crate-dep (name "paste") (req "^1.0.14") (default-features #t) (kind 0)))) (hash "1dr8q3lia6zs2nzc77slkjfk47fmkblm9s626v7zn0zdi2894n8n") (features (quote (("std") ("default"))))))

(define-public crate-slice2d-0.1 (crate (name "slice2d") (vers "0.1.0") (hash "12s7p5w4bzngsf5hajs6qidh0akgk0hdim4dws64v11a2z4l77vl")))

(define-public crate-slice2d-0.1 (crate (name "slice2d") (vers "0.1.1") (hash "0dyak1cqjd7av51pqy287r7sdvg8dmvgdxsbps6w2fiph7rcwcvp")))

(define-public crate-slice_adapter-0.1 (crate (name "slice_adapter") (vers "0.1.0") (hash "0n336fcgvhwxxqczqxip8zlzjld38wbciswyjbjr4pbwbkpgdcql") (yanked #t)))

(define-public crate-slice_adapter-0.1 (crate (name "slice_adapter") (vers "0.1.1") (hash "1iyg8kwdpd5lq45148fi519p3jammm3q94xl3bmyrwripqymkb88")))

(define-public crate-slice_as_array-1 (crate (name "slice_as_array") (vers "1.0.0") (deps (list (crate-dep (name "compiletest_rs") (req "^0.0.11") (optional #t) (default-features #t) (kind 0)))) (hash "0p5pc5zyy255q4vkhxxhvx1p698cr5rrmywjcf7nicf947jrdqvp") (features (quote (("use_std") ("default" "use_std") ("compiletest" "compiletest_rs"))))))

(define-public crate-slice_as_array-1 (crate (name "slice_as_array") (vers "1.1.0") (deps (list (crate-dep (name "compiletest_rs") (req "^0.0.11") (optional #t) (default-features #t) (kind 0)))) (hash "1hz4py2rknxbpdpn7iaj59br58ml91ydjb6wjnmvbvfxb7p67jb4") (features (quote (("use_std") ("default" "use_std") ("compiletest" "compiletest_rs"))))))

(define-public crate-slice_math-0.1 (crate (name "slice_math") (vers "0.1.0") (deps (list (crate-dep (name "array__ops") (req "^0.1.8") (default-features #t) (kind 2)) (crate-dep (name "linspace") (req "^0.1.1") (default-features #t) (kind 2)) (crate-dep (name "moddef") (req "^0.2.2") (default-features #t) (kind 0)) (crate-dep (name "num") (req "^0.4.1") (default-features #t) (kind 0)) (crate-dep (name "plotters") (req "^0.3.5") (default-features #t) (kind 2)) (crate-dep (name "rustfft") (req "^6.2.0") (default-features #t) (kind 2)) (crate-dep (name "slice_ops") (req "^0.1.5") (features (quote ("std"))) (default-features #t) (kind 0)))) (hash "1dgawk69alxhcy2w7wj1rw0wh84d5akb18gm3nh59ic3b6gkv6kh")))

(define-public crate-slice_math-0.1 (crate (name "slice_math") (vers "0.1.1") (deps (list (crate-dep (name "array__ops") (req "^0.1.8") (default-features #t) (kind 2)) (crate-dep (name "linspace") (req "^0.1.1") (default-features #t) (kind 2)) (crate-dep (name "moddef") (req "^0.2.2") (default-features #t) (kind 0)) (crate-dep (name "num") (req "^0.4.1") (default-features #t) (kind 0)) (crate-dep (name "plotters") (req "^0.3.5") (default-features #t) (kind 2)) (crate-dep (name "rustfft") (req "^6.2.0") (default-features #t) (kind 2)) (crate-dep (name "slice_ops") (req "^0.1.5") (features (quote ("std"))) (default-features #t) (kind 0)))) (hash "0n93d411gr8hkg16wr3vddhvjl1a90jff45sw8hi11372rmmyrzh")))

(define-public crate-slice_math-0.1 (crate (name "slice_math") (vers "0.1.2") (deps (list (crate-dep (name "moddef") (req "^0.2.2") (default-features #t) (kind 0)) (crate-dep (name "num") (req "^0.4.1") (default-features #t) (kind 0)) (crate-dep (name "slice_ops") (req "^0.1.5") (features (quote ("std"))) (default-features #t) (kind 0)))) (hash "0rmm3ik4kka9pzhp8x41flm6h1jxndm233w04jj7jn3cmgad0f9i")))

(define-public crate-slice_math-0.1 (crate (name "slice_math") (vers "0.1.3") (deps (list (crate-dep (name "moddef") (req "^0.2.2") (default-features #t) (kind 0)) (crate-dep (name "num") (req "^0.4.1") (default-features #t) (kind 0)) (crate-dep (name "slice_ops") (req "^0.1.5") (features (quote ("std"))) (default-features #t) (kind 0)))) (hash "00m22z08ws5zm47xj1cxy54n9zlnlkiq3jbxnawyr0xs9w19aw6j")))

(define-public crate-slice_math-0.1 (crate (name "slice_math") (vers "0.1.4") (deps (list (crate-dep (name "moddef") (req "^0.2.2") (default-features #t) (kind 0)) (crate-dep (name "num") (req "^0.4.1") (default-features #t) (kind 0)) (crate-dep (name "plotters") (req "^0.3.5") (default-features #t) (kind 2)) (crate-dep (name "rustfft") (req "^6.2.0") (default-features #t) (kind 2)) (crate-dep (name "slice_ops") (req "^0.1.5") (features (quote ("std"))) (default-features #t) (kind 0)))) (hash "194m9i5498w69hps4j6s48x97p6xbx22h2bqag4cpqkx09cfgwn0")))

(define-public crate-slice_math-0.1 (crate (name "slice_math") (vers "0.1.5") (deps (list (crate-dep (name "moddef") (req "^0.2.2") (default-features #t) (kind 0)) (crate-dep (name "num") (req "^0.4.1") (default-features #t) (kind 0)) (crate-dep (name "plotters") (req "^0.3.5") (default-features #t) (kind 2)) (crate-dep (name "rustfft") (req "^6.2.0") (default-features #t) (kind 2)) (crate-dep (name "slice_ops") (req "^0.1.5") (features (quote ("std"))) (default-features #t) (kind 0)))) (hash "0j5daig23xnikr3cv08i2sdj80lv325f9imv530m7s2zg5g6sr97")))

(define-public crate-slice_math-0.1 (crate (name "slice_math") (vers "0.1.6") (deps (list (crate-dep (name "moddef") (req "^0.2.2") (default-features #t) (kind 0)) (crate-dep (name "num") (req "^0.4.1") (default-features #t) (kind 0)) (crate-dep (name "plotters") (req "^0.3.5") (default-features #t) (kind 2)) (crate-dep (name "rustfft") (req "^6.2.0") (default-features #t) (kind 2)) (crate-dep (name "slice_ops") (req "^0.1.5") (features (quote ("std"))) (default-features #t) (kind 0)))) (hash "19aqc8pnsglxywl608j4p69xykxxsnrjnqr7s45aznavwrqfy3r6")))

(define-public crate-slice_math-0.1 (crate (name "slice_math") (vers "0.1.7") (deps (list (crate-dep (name "moddef") (req "^0.2.2") (default-features #t) (kind 0)) (crate-dep (name "num") (req "^0.4.1") (default-features #t) (kind 0)) (crate-dep (name "plotters") (req "^0.3.5") (default-features #t) (kind 2)) (crate-dep (name "rustfft") (req "^6.2.0") (default-features #t) (kind 2)) (crate-dep (name "slice_ops") (req "^0.1.6") (features (quote ("std"))) (default-features #t) (kind 0)))) (hash "1myx895l9y0039dbqwkc1prwx5396pi5vjp3b5as7mk50g3bbs9q")))

(define-public crate-slice_math-0.1 (crate (name "slice_math") (vers "0.1.8") (deps (list (crate-dep (name "moddef") (req "^0.2.2") (default-features #t) (kind 0)) (crate-dep (name "num") (req "^0.4.1") (default-features #t) (kind 0)) (crate-dep (name "plotters") (req "^0.3.5") (default-features #t) (kind 2)) (crate-dep (name "rustfft") (req "^6.2.0") (default-features #t) (kind 2)) (crate-dep (name "slice_ops") (req "^0.1.7") (features (quote ("std"))) (default-features #t) (kind 0)))) (hash "0v5fjal26s0ddimlw17fxzmm6595zrd50v7g7cnjwkwxsd66v3a9")))

(define-public crate-slice_math-0.1 (crate (name "slice_math") (vers "0.1.9") (deps (list (crate-dep (name "moddef") (req "^0.2.2") (default-features #t) (kind 0)) (crate-dep (name "ndarray") (req "^0.15.6") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "ndarray-linalg") (req "^0.16.0") (features (quote ("intel-mkl"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "num") (req "^0.4.1") (default-features #t) (kind 0)) (crate-dep (name "plotters") (req "^0.3.5") (default-features #t) (kind 2)) (crate-dep (name "rustfft") (req "^6.2.0") (default-features #t) (kind 2)) (crate-dep (name "slice_ops") (req "^0.1.7") (features (quote ("std"))) (default-features #t) (kind 0)))) (hash "1j8ailb7xr8l7rlzl2ks2d48kwi3zmjr894yxfxgqr8i25qc4c8d") (features (quote (("default")))) (v 2) (features2 (quote (("ndarray" "dep:ndarray" "dep:ndarray-linalg"))))))

(define-public crate-slice_math-0.1 (crate (name "slice_math") (vers "0.1.10") (deps (list (crate-dep (name "moddef") (req "^0.2.2") (default-features #t) (kind 0)) (crate-dep (name "ndarray") (req "^0.15.6") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "ndarray-linalg") (req "^0.16.0") (features (quote ("intel-mkl"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "num") (req "^0.4.1") (default-features #t) (kind 0)) (crate-dep (name "plotters") (req "^0.3.5") (default-features #t) (kind 2)) (crate-dep (name "rustfft") (req "^6.2.0") (default-features #t) (kind 2)) (crate-dep (name "slice_ops") (req "^0.1.7") (features (quote ("std"))) (default-features #t) (kind 0)))) (hash "1qlv1mgj497vxiw8c8zz2x2szr667gisdsw3b3ayfgqisq4bdspk") (features (quote (("default")))) (v 2) (features2 (quote (("ndarray" "dep:ndarray" "dep:ndarray-linalg"))))))

(define-public crate-slice_math-0.1 (crate (name "slice_math") (vers "0.1.11") (deps (list (crate-dep (name "moddef") (req "^0.2.2") (default-features #t) (kind 0)) (crate-dep (name "ndarray") (req "^0.15.6") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "ndarray-linalg") (req "^0.16.0") (features (quote ("intel-mkl"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "num") (req "^0.4.1") (default-features #t) (kind 0)) (crate-dep (name "plotters") (req "^0.3.5") (default-features #t) (kind 2)) (crate-dep (name "rustfft") (req "^6.2.0") (default-features #t) (kind 2)) (crate-dep (name "slice_ops") (req "^0.1.7") (features (quote ("std"))) (default-features #t) (kind 0)))) (hash "0xdc2771ig59iakmnyav5mi38s83wcxs4l887izm1cldx5yc6byd") (features (quote (("default")))) (v 2) (features2 (quote (("ndarray" "dep:ndarray" "dep:ndarray-linalg"))))))

(define-public crate-slice_math-0.1 (crate (name "slice_math") (vers "0.1.12") (deps (list (crate-dep (name "moddef") (req "^0.2.2") (default-features #t) (kind 0)) (crate-dep (name "ndarray") (req "^0.15.6") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "ndarray-linalg") (req "^0.16.0") (features (quote ("intel-mkl"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "num") (req "^0.4.1") (default-features #t) (kind 0)) (crate-dep (name "plotters") (req "^0.3.5") (default-features #t) (kind 2)) (crate-dep (name "rustfft") (req "^6.2.0") (default-features #t) (kind 2)) (crate-dep (name "slice_ops") (req "^0.1.7") (features (quote ("std"))) (default-features #t) (kind 0)))) (hash "0r8i99zvn65wz53yncxzibrvsbhglidxp0f43ax6py5rar3ndzcd") (features (quote (("default")))) (v 2) (features2 (quote (("ndarray" "dep:ndarray" "dep:ndarray-linalg"))))))

(define-public crate-slice_math-0.1 (crate (name "slice_math") (vers "0.1.13") (deps (list (crate-dep (name "moddef") (req "^0.2.2") (default-features #t) (kind 0)) (crate-dep (name "ndarray") (req "^0.15.6") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "ndarray-linalg") (req "^0.16.0") (features (quote ("intel-mkl"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "num") (req "^0.4.1") (default-features #t) (kind 0)) (crate-dep (name "plotters") (req "^0.3.5") (default-features #t) (kind 2)) (crate-dep (name "rustfft") (req "^6.2.0") (default-features #t) (kind 2)) (crate-dep (name "slice_ops") (req "^0.1.7") (features (quote ("std"))) (default-features #t) (kind 0)))) (hash "0av0ps467fj2xagxfgzdvnfxhxql9a7bfr5jw93x4vxkdndyi52r") (features (quote (("default")))) (v 2) (features2 (quote (("ndarray" "dep:ndarray" "dep:ndarray-linalg"))))))

(define-public crate-slice_math-0.1 (crate (name "slice_math") (vers "0.1.14") (deps (list (crate-dep (name "moddef") (req "^0.2.2") (default-features #t) (kind 0)) (crate-dep (name "ndarray") (req "^0.15.6") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "ndarray-linalg") (req "^0.16.0") (features (quote ("intel-mkl"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "num") (req "^0.4.1") (default-features #t) (kind 0)) (crate-dep (name "plotters") (req "^0.3.5") (default-features #t) (kind 2)) (crate-dep (name "rustfft") (req "^6.2.0") (default-features #t) (kind 2)) (crate-dep (name "slice_ops") (req "^0.1.7") (features (quote ("std"))) (default-features #t) (kind 0)))) (hash "0kac8addj906gshvbkfipkj309ma167m0r8g5yclzq3dm3jij95y") (features (quote (("default")))) (v 2) (features2 (quote (("ndarray" "dep:ndarray" "dep:ndarray-linalg"))))))

(define-public crate-slice_math-0.1 (crate (name "slice_math") (vers "0.1.15") (deps (list (crate-dep (name "moddef") (req "^0.2.2") (default-features #t) (kind 0)) (crate-dep (name "ndarray") (req "^0.15.6") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "ndarray-linalg") (req "^0.16.0") (features (quote ("intel-mkl"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "num") (req "^0.4.1") (default-features #t) (kind 0)) (crate-dep (name "plotters") (req "^0.3.5") (default-features #t) (kind 2)) (crate-dep (name "rustfft") (req "^6.2.0") (default-features #t) (kind 2)) (crate-dep (name "slice_ops") (req "^0.1.7") (features (quote ("std"))) (default-features #t) (kind 0)))) (hash "0wmqfzvvqk40fygxszz0la8yvvmvfkvg1z2wrj004hvl9ryha99f") (features (quote (("default")))) (v 2) (features2 (quote (("ndarray" "dep:ndarray" "dep:ndarray-linalg"))))))

(define-public crate-slice_math-0.1 (crate (name "slice_math") (vers "0.1.16") (deps (list (crate-dep (name "moddef") (req "^0.2.2") (default-features #t) (kind 0)) (crate-dep (name "ndarray") (req "^0.15.6") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "ndarray-linalg") (req "^0.16.0") (features (quote ("intel-mkl"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "num") (req "^0.4.1") (default-features #t) (kind 0)) (crate-dep (name "plotters") (req "^0.3.5") (default-features #t) (kind 2)) (crate-dep (name "rustfft") (req "^6.2.0") (default-features #t) (kind 2)) (crate-dep (name "slice_ops") (req "^0.1.8") (features (quote ("std"))) (default-features #t) (kind 0)))) (hash "1k6zh2ckh3yjmi88mddrdl7dhadv216fn3vbj00863gynalk8ajy") (features (quote (("default")))) (v 2) (features2 (quote (("ndarray" "dep:ndarray" "dep:ndarray-linalg"))))))

(define-public crate-slice_math-0.1 (crate (name "slice_math") (vers "0.1.17") (deps (list (crate-dep (name "moddef") (req "^0.2.2") (default-features #t) (kind 0)) (crate-dep (name "ndarray") (req "^0.15.6") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "ndarray-linalg") (req "^0.16.0") (features (quote ("intel-mkl"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "num") (req "^0.4.1") (default-features #t) (kind 0)) (crate-dep (name "plotters") (req "^0.3.5") (default-features #t) (kind 2)) (crate-dep (name "rustfft") (req "^6.2.0") (default-features #t) (kind 2)) (crate-dep (name "slice_ops") (req "^0.1.8") (features (quote ("std"))) (default-features #t) (kind 0)))) (hash "0nl7q5nsg9hhm3k4zyf1d1mm0h5yq434p75mgvqmcjzn0yjp1lyi") (features (quote (("default")))) (v 2) (features2 (quote (("ndarray" "dep:ndarray" "dep:ndarray-linalg"))))))

(define-public crate-slice_math-0.1 (crate (name "slice_math") (vers "0.1.18") (deps (list (crate-dep (name "moddef") (req "^0.2.2") (default-features #t) (kind 0)) (crate-dep (name "ndarray") (req "^0.15.6") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "ndarray-linalg") (req "^0.16.0") (features (quote ("intel-mkl"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "num") (req "^0.4.1") (default-features #t) (kind 0)) (crate-dep (name "plotters") (req "^0.3.5") (default-features #t) (kind 2)) (crate-dep (name "rustfft") (req "^6.2.0") (default-features #t) (kind 2)) (crate-dep (name "slice_ops") (req "^0.1.8") (features (quote ("std"))) (default-features #t) (kind 0)))) (hash "1hhwf88pa00m4ky5s4ivjr7xlgn7jqj61j5kzxi70w4f9dgs71q2") (features (quote (("default")))) (v 2) (features2 (quote (("ndarray" "dep:ndarray" "dep:ndarray-linalg"))))))

(define-public crate-slice_math-0.1 (crate (name "slice_math") (vers "0.1.19") (deps (list (crate-dep (name "moddef") (req "^0.2.2") (default-features #t) (kind 0)) (crate-dep (name "ndarray") (req "^0.15.6") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "ndarray-linalg") (req "^0.16.0") (features (quote ("intel-mkl"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "num") (req "^0.4.1") (default-features #t) (kind 0)) (crate-dep (name "plotters") (req "^0.3.5") (default-features #t) (kind 2)) (crate-dep (name "rustfft") (req "^6.2.0") (default-features #t) (kind 2)) (crate-dep (name "slice_ops") (req "^0.1.8") (features (quote ("std"))) (default-features #t) (kind 0)))) (hash "154l3i7ajilhbwcvp98x0r2p232a2w0c67ldqkk3lai1mxgqw91g") (features (quote (("default")))) (v 2) (features2 (quote (("ndarray" "dep:ndarray" "dep:ndarray-linalg"))))))

(define-public crate-slice_math-0.1 (crate (name "slice_math") (vers "0.1.20") (deps (list (crate-dep (name "moddef") (req "^0.2.2") (default-features #t) (kind 0)) (crate-dep (name "ndarray") (req "^0.15.6") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "ndarray-linalg") (req "^0.16.0") (features (quote ("intel-mkl"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "num") (req "^0.4.1") (default-features #t) (kind 0)) (crate-dep (name "plotters") (req "^0.3.5") (default-features #t) (kind 2)) (crate-dep (name "rustfft") (req "^6.2.0") (default-features #t) (kind 2)) (crate-dep (name "slice_ops") (req "^0.1.10") (features (quote ("std"))) (default-features #t) (kind 0)))) (hash "1w2bnz0wrk58kkambg44csrmclgn0q18b27hwk75a8ppnnixc0ms") (features (quote (("default")))) (v 2) (features2 (quote (("ndarray" "dep:ndarray" "dep:ndarray-linalg"))))))

(define-public crate-slice_math-0.1 (crate (name "slice_math") (vers "0.1.21") (deps (list (crate-dep (name "moddef") (req "^0.2.2") (default-features #t) (kind 0)) (crate-dep (name "ndarray") (req "^0.15.6") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "ndarray-linalg") (req "^0.16.0") (features (quote ("intel-mkl"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "num") (req "^0.4.1") (default-features #t) (kind 0)) (crate-dep (name "plotters") (req "^0.3.5") (default-features #t) (kind 2)) (crate-dep (name "rustfft") (req "^6.2.0") (default-features #t) (kind 2)) (crate-dep (name "slice_ops") (req "^0.1.10") (features (quote ("std"))) (default-features #t) (kind 0)))) (hash "0k2glj00lxym220fk80sz19gvk7sz24kgp13ilnssv95xq5a03ss") (features (quote (("default")))) (v 2) (features2 (quote (("ndarray" "dep:ndarray" "dep:ndarray-linalg"))))))

(define-public crate-slice_math-0.1 (crate (name "slice_math") (vers "0.1.22") (deps (list (crate-dep (name "moddef") (req "^0.2.2") (default-features #t) (kind 0)) (crate-dep (name "ndarray") (req "^0.15.6") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "ndarray-linalg") (req "^0.16.0") (features (quote ("intel-mkl"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "num") (req "^0.4.1") (default-features #t) (kind 0)) (crate-dep (name "plotters") (req "^0.3.5") (default-features #t) (kind 2)) (crate-dep (name "rustfft") (req "^6.2.0") (default-features #t) (kind 2)) (crate-dep (name "slice_ops") (req "^0.1.10") (features (quote ("std"))) (default-features #t) (kind 0)))) (hash "1v7hmfdp1n1pz811glrjvw24rfkggms7128pxrqbz197fypfyk4y") (features (quote (("default")))) (v 2) (features2 (quote (("ndarray" "dep:ndarray" "dep:ndarray-linalg"))))))

(define-public crate-slice_math-0.1 (crate (name "slice_math") (vers "0.1.23") (deps (list (crate-dep (name "moddef") (req "^0.2.2") (default-features #t) (kind 0)) (crate-dep (name "ndarray") (req "^0.15.6") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "ndarray-linalg") (req "^0.16.0") (features (quote ("intel-mkl"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "num") (req "^0.4.1") (default-features #t) (kind 0)) (crate-dep (name "plotters") (req "^0.3.5") (default-features #t) (kind 2)) (crate-dep (name "rustfft") (req "^6.2.0") (default-features #t) (kind 2)) (crate-dep (name "slice_ops") (req "^0.1.10") (features (quote ("std"))) (default-features #t) (kind 0)))) (hash "1kf17hwvlfd180jjcy2dn7cdp1i5y04m3ycpa5qfhyxnrqxdcrfa") (features (quote (("default")))) (v 2) (features2 (quote (("ndarray" "dep:ndarray" "dep:ndarray-linalg"))))))

(define-public crate-slice_math-0.1 (crate (name "slice_math") (vers "0.1.24") (deps (list (crate-dep (name "moddef") (req "^0.2.2") (default-features #t) (kind 0)) (crate-dep (name "ndarray") (req "^0.15.6") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "ndarray-linalg") (req "^0.16.0") (features (quote ("intel-mkl"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "num") (req "^0.4.1") (default-features #t) (kind 0)) (crate-dep (name "plotters") (req "^0.3.5") (default-features #t) (kind 2)) (crate-dep (name "rustfft") (req "^6.2.0") (default-features #t) (kind 2)) (crate-dep (name "slice_ops") (req "^0.1.10") (features (quote ("std"))) (default-features #t) (kind 0)))) (hash "1ydw8rk3df37yvqrqk08hm441nb1r18v1klqp6wh92c4v5agpv95") (features (quote (("default")))) (v 2) (features2 (quote (("ndarray" "dep:ndarray" "dep:ndarray-linalg"))))))

(define-public crate-slice_math-0.1 (crate (name "slice_math") (vers "0.1.25") (deps (list (crate-dep (name "moddef") (req "^0.2.2") (default-features #t) (kind 0)) (crate-dep (name "ndarray") (req "^0.15.6") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "ndarray-linalg") (req "^0.16.0") (features (quote ("intel-mkl"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "num") (req "^0.4.1") (default-features #t) (kind 0)) (crate-dep (name "plotters") (req "^0.3.5") (default-features #t) (kind 2)) (crate-dep (name "rustfft") (req "^6.2.0") (default-features #t) (kind 2)) (crate-dep (name "slice_ops") (req "^0.1.10") (features (quote ("std"))) (default-features #t) (kind 0)))) (hash "1n2l7f2cq9lix2bwhxnhyzbin7va5z189w727ass1airr5xhw4b0") (features (quote (("default")))) (v 2) (features2 (quote (("ndarray" "dep:ndarray" "dep:ndarray-linalg"))))))

(define-public crate-slice_math-0.1 (crate (name "slice_math") (vers "0.1.26") (deps (list (crate-dep (name "moddef") (req "^0.2.2") (default-features #t) (kind 0)) (crate-dep (name "ndarray") (req "^0.15.6") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "ndarray-linalg") (req "^0.16.0") (features (quote ("intel-mkl"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "num") (req "^0.4.1") (default-features #t) (kind 0)) (crate-dep (name "plotters") (req "^0.3.5") (default-features #t) (kind 2)) (crate-dep (name "rustfft") (req "^6.2.0") (default-features #t) (kind 2)) (crate-dep (name "slice_ops") (req "^0.1.10") (features (quote ("std"))) (default-features #t) (kind 0)))) (hash "0rm8d3iv92kmakszrpdmc8kq9ahq8h7l1jdp3w4wppndrpidlw6c") (features (quote (("default")))) (v 2) (features2 (quote (("ndarray" "dep:ndarray" "dep:ndarray-linalg"))))))

(define-public crate-slice_math-0.1 (crate (name "slice_math") (vers "0.1.27") (deps (list (crate-dep (name "moddef") (req "^0.2.2") (default-features #t) (kind 0)) (crate-dep (name "ndarray") (req "^0.15.6") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "ndarray-linalg") (req "^0.16.0") (features (quote ("intel-mkl"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "num") (req "^0.4.1") (default-features #t) (kind 0)) (crate-dep (name "plotters") (req "^0.3.5") (default-features #t) (kind 2)) (crate-dep (name "rustfft") (req "^6.2.0") (default-features #t) (kind 2)) (crate-dep (name "slice_ops") (req "^0.1.10") (features (quote ("std"))) (default-features #t) (kind 0)))) (hash "16f9ci6z7c3vg2jmb4gkhwa69z7nkz8b4ibhig7w770pcpwdwmrg") (features (quote (("default")))) (v 2) (features2 (quote (("ndarray" "dep:ndarray" "dep:ndarray-linalg"))))))

(define-public crate-slice_math-0.1 (crate (name "slice_math") (vers "0.1.28") (deps (list (crate-dep (name "moddef") (req "^0.2.2") (default-features #t) (kind 0)) (crate-dep (name "ndarray") (req "^0.15.6") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "ndarray-linalg") (req "^0.16.0") (features (quote ("intel-mkl"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "num") (req "^0.4.1") (default-features #t) (kind 0)) (crate-dep (name "plotters") (req "^0.3.5") (default-features #t) (kind 2)) (crate-dep (name "rustfft") (req "^6.2.0") (default-features #t) (kind 2)) (crate-dep (name "slice_ops") (req "^0.1.10") (features (quote ("std"))) (default-features #t) (kind 0)))) (hash "0q8b057vh6mdglfbs26yhqg2y3ggvv6l3nazsj7sfl4ls5r9wx8g") (features (quote (("default")))) (v 2) (features2 (quote (("ndarray" "dep:ndarray" "dep:ndarray-linalg"))))))

(define-public crate-slice_math-0.1 (crate (name "slice_math") (vers "0.1.29") (deps (list (crate-dep (name "moddef") (req "^0.2.2") (default-features #t) (kind 0)) (crate-dep (name "ndarray") (req "^0.15.6") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "ndarray-linalg") (req "^0.16.0") (features (quote ("intel-mkl"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "num") (req "^0.4.1") (default-features #t) (kind 0)) (crate-dep (name "plotters") (req "^0.3.5") (default-features #t) (kind 2)) (crate-dep (name "rustfft") (req "^6.2.0") (default-features #t) (kind 2)) (crate-dep (name "slice_ops") (req "^0.1.10") (features (quote ("std"))) (default-features #t) (kind 0)))) (hash "0a3f4p1blmgpknh89wafqyfcc51ca1n7dfkap0pghvsw2jh2f8mg") (features (quote (("default")))) (v 2) (features2 (quote (("ndarray" "dep:ndarray" "dep:ndarray-linalg"))))))

(define-public crate-slice_math-0.1 (crate (name "slice_math") (vers "0.1.30") (deps (list (crate-dep (name "moddef") (req "^0.2.2") (default-features #t) (kind 0)) (crate-dep (name "ndarray") (req "^0.15.6") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "ndarray-linalg") (req "^0.16.0") (features (quote ("intel-mkl"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "num") (req "^0.4.1") (default-features #t) (kind 0)) (crate-dep (name "plotters") (req "^0.3.5") (default-features #t) (kind 2)) (crate-dep (name "rustfft") (req "^6.2.0") (default-features #t) (kind 2)) (crate-dep (name "slice_ops") (req "^0.1.10") (features (quote ("std"))) (default-features #t) (kind 0)))) (hash "0mbyp0qawfrqpps8af16p3l59s3v3a8kw8zmbazprs2gnzl433q4") (features (quote (("default")))) (v 2) (features2 (quote (("ndarray" "dep:ndarray" "dep:ndarray-linalg"))))))

(define-public crate-slice_mip-1 (crate (name "slice_mip") (vers "1.0.0") (hash "14jmra5qknmhbsx3zk19abbsf93f8qvdikg35ns01dlwqqb62ahz")))

(define-public crate-slice_n-0.0.1 (crate (name "slice_n") (vers "0.0.1") (deps (list (crate-dep (name "maybe-std") (req "^0.1.2") (default-features #t) (kind 0)))) (hash "1135xvsmcm0swsv1fk8lp0m7r2bjq5zs7pdmxxap91kzy1qpcd29") (features (quote (("unstable" "maybe-std/unstable") ("std" "maybe-std/std") ("default") ("alloc" "maybe-std/alloc"))))))

(define-public crate-slice_n-0.0.2 (crate (name "slice_n") (vers "0.0.2") (deps (list (crate-dep (name "maybe-std") (req "^0.1.2") (default-features #t) (kind 0)))) (hash "0dzzqqb85v0qimqcxvdrrdi5y6hf8044r4kysxcjqifp71nh5sm6") (features (quote (("unstable" "maybe-std/unstable") ("std" "maybe-std/std") ("default") ("alloc" "maybe-std/alloc"))))))

(define-public crate-slice_ops-0.1 (crate (name "slice_ops") (vers "0.1.0") (deps (list (crate-dep (name "moddef") (req "^0.2.2") (default-features #t) (kind 0)) (crate-dep (name "slice_trait") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "116aakdjynpp2nf8b4dvgk2lp7g3q8c0h9m3zpc4vzl9czsx7x55")))

(define-public crate-slice_ops-0.1 (crate (name "slice_ops") (vers "0.1.1") (deps (list (crate-dep (name "moddef") (req "^0.2.2") (default-features #t) (kind 0)) (crate-dep (name "slice_trait") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "0znsj0shprqfkghsgqiva1cqd34pqndm065glczp5k8dh62bz61m")))

(define-public crate-slice_ops-0.1 (crate (name "slice_ops") (vers "0.1.2") (deps (list (crate-dep (name "moddef") (req "^0.2.2") (default-features #t) (kind 0)) (crate-dep (name "slice_trait") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "1llb9r6hy2b88236m8mn6svj7f7m6b3v0qj16jid6z54fd5g02j3")))

(define-public crate-slice_ops-0.1 (crate (name "slice_ops") (vers "0.1.3") (deps (list (crate-dep (name "moddef") (req "^0.2.2") (default-features #t) (kind 0)) (crate-dep (name "slice_trait") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "0vz1q1p1jgx3qdpqxw2wylfg754skv2bvm08s88i2mvy0gvi1nif") (features (quote (("std") ("default" "std"))))))

(define-public crate-slice_ops-0.1 (crate (name "slice_ops") (vers "0.1.4") (deps (list (crate-dep (name "moddef") (req "^0.2.2") (default-features #t) (kind 0)) (crate-dep (name "slice_trait") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "1yg8fh94br1v696mwvga5w1870b8hmnpl09zrrj9qdx5c0x7s15l") (features (quote (("std") ("default" "std"))))))

(define-public crate-slice_ops-0.1 (crate (name "slice_ops") (vers "0.1.5") (deps (list (crate-dep (name "moddef") (req "^0.2.2") (default-features #t) (kind 0)) (crate-dep (name "slice_trait") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "1s8p5wy2xv57kn8p5xxn3mbrzdwh0h9alw0p4w56cbcdab31ffs3") (features (quote (("std") ("default" "std"))))))

(define-public crate-slice_ops-0.1 (crate (name "slice_ops") (vers "0.1.6") (deps (list (crate-dep (name "moddef") (req "^0.2.2") (default-features #t) (kind 0)) (crate-dep (name "slice_trait") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "1xm72d7jlxj42gqylg81zwv479h3m13jnsgv2zhwya17apfy8cls") (features (quote (("std") ("default" "std"))))))

(define-public crate-slice_ops-0.1 (crate (name "slice_ops") (vers "0.1.7") (deps (list (crate-dep (name "moddef") (req "^0.2.2") (default-features #t) (kind 0)) (crate-dep (name "slice_trait") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "072hfyq3paqdq43n0yz2xkg5sw91v5aaq0mgcap57w3rrwrycqfb") (features (quote (("std") ("default" "std"))))))

(define-public crate-slice_ops-0.1 (crate (name "slice_ops") (vers "0.1.8") (deps (list (crate-dep (name "moddef") (req "^0.2.2") (default-features #t) (kind 0)) (crate-dep (name "slice_trait") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "0rikl70c1kvxk1gmycp0mfhpz1n4r1b3kbdgvn0xpg4kcl502f41") (features (quote (("std") ("default" "std"))))))

(define-public crate-slice_ops-0.1 (crate (name "slice_ops") (vers "0.1.9") (deps (list (crate-dep (name "moddef") (req "^0.2.2") (default-features #t) (kind 0)) (crate-dep (name "slice_trait") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "1dvw8b24qb0q32kggljvc90pgyqi6p8q2anjxbkl14735laf0xmg") (features (quote (("std") ("default" "std"))))))

(define-public crate-slice_ops-0.1 (crate (name "slice_ops") (vers "0.1.10") (deps (list (crate-dep (name "moddef") (req "^0.2.2") (default-features #t) (kind 0)) (crate-dep (name "slice_trait") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "0q0z8iy245bgf23mvggpw14d89zvawdzm8znijx5rsf4xbzmx4x4") (features (quote (("std") ("default" "std"))))))

(define-public crate-slice_queue-0.1 (crate (name "slice_queue") (vers "0.1.0") (hash "0ih6ifc273cmhfw2sq87p3j8k96qb55n6g7h1pds0iz4aq3ylkfn")))

(define-public crate-slice_queue-0.1 (crate (name "slice_queue") (vers "0.1.1") (hash "1aia7mhly5z6hv5h7217qmaynjrajsvg6vvzxqpifakrwhjbsp0a")))

(define-public crate-slice_queue-0.1 (crate (name "slice_queue") (vers "0.1.2") (hash "1ckqdijsmpc5yrbh4lrjas3khzw7j3cyz2xqyak0jysyz6mrknxp")))

(define-public crate-slice_queue-0.1 (crate (name "slice_queue") (vers "0.1.3") (hash "00m0m4dabwawcmxk8pmpi107nipp8blq5l4kn2sp83fdw2192159")))

(define-public crate-slice_queue-0.1 (crate (name "slice_queue") (vers "0.1.4") (hash "06vf57zv74raxfz0l58113g0v4k4fy3n9crix2xff5v59g37kr31") (features (quote (("fast_unsafe_code") ("default"))))))

(define-public crate-slice_queue-0.1 (crate (name "slice_queue") (vers "0.1.5") (hash "01p84n56m6rxf73zich8m3rjn2a4l59vig0l40g7jd7ai6wnb8aj") (features (quote (("fast_unsafe_code") ("default"))))))

(define-public crate-slice_queue-0.1 (crate (name "slice_queue") (vers "0.1.6") (hash "1wjrycrbiaxp4yr8b7ilynmzk3amnks8acrk1azigccwwr2rq6pa") (features (quote (("fast_unsafe_code") ("deref") ("default" "deref"))))))

(define-public crate-slice_queue-0.1 (crate (name "slice_queue") (vers "0.1.7") (hash "1s1wz79wpn60405xiz2n8zhif3wn2g35jl0ync80h65gg5aqi411") (features (quote (("fast_unsafe_code") ("deref") ("default" "deref"))))))

(define-public crate-slice_queue-0.1 (crate (name "slice_queue") (vers "0.1.8") (hash "1hi6f73svx7xyy6xpbighclfnr75i655dvq9rkld1ziwdzjzd8b9") (features (quote (("unsafe_fast_code") ("deref") ("default" "deref" "unsafe_fast_code"))))))

(define-public crate-slice_queue-0.1 (crate (name "slice_queue") (vers "0.1.9") (hash "02w8lw74j8nfrh3f3c7bwjkmi1lx59am4lshf9v4lpy9k4502zns") (features (quote (("unsafe_fast_code") ("deref") ("default" "deref" "unsafe_fast_code"))))))

(define-public crate-slice_queue-0.1 (crate (name "slice_queue") (vers "0.1.10") (hash "10d6b0hmiw0biqhpvryn01ry1mw53949rvaqc30lxj5yscd65pwl") (features (quote (("unsafe_fast_code") ("deref") ("default" "deref" "unsafe_fast_code"))))))

(define-public crate-slice_queue-0.2 (crate (name "slice_queue") (vers "0.2.0") (hash "1sggsvb9zzjgabjakzsaiaw8jx1jq3ch088b463lh5bgcfwp0172") (features (quote (("unsafe_fast_code") ("deref") ("default" "deref" "unsafe_fast_code"))))))

(define-public crate-slice_queue-0.2 (crate (name "slice_queue") (vers "0.2.1") (hash "0b9v6yyxkgd55sc527jxxpiq0gyn9x19iazqm14xv9zbhnlrj8qr") (features (quote (("unsafe_fast_code") ("deref") ("default" "deref" "unsafe_fast_code"))))))

(define-public crate-slice_queue-0.2 (crate (name "slice_queue") (vers "0.2.2") (hash "1mzw882n6zbb87anvr7gnxi6kcdwjfmha5v0n4cv24kmsxf4w408") (features (quote (("unsafe_fast_code") ("deref") ("default" "deref" "unsafe_fast_code"))))))

(define-public crate-slice_queue-0.3 (crate (name "slice_queue") (vers "0.3.0") (hash "1jfvi0w28j0xqmi0vwp6rwlrf4rpw8pg9pqidib80lrs1gvbdxgl") (features (quote (("unsafe_fast_code") ("deref") ("default" "deref" "unsafe_fast_code"))))))

(define-public crate-slice_queue-0.3 (crate (name "slice_queue") (vers "0.3.1") (hash "1qp8jl494n8rkrqpzr9034jpdqvfp2pv0bdcr8x17nq7sqqa7870") (features (quote (("unsafe_fast_code") ("deref") ("default" "deref" "unsafe_fast_code"))))))

(define-public crate-slice_queue-0.3 (crate (name "slice_queue") (vers "0.3.2") (hash "1alh8xqzjs6jm94xx8wr7kab96jxq3yzwki7ip8d421gp2yj3gc6") (features (quote (("unsafe_fast_code") ("deref") ("default" "deref" "unsafe_fast_code"))))))

(define-public crate-slice_ring_buf-0.1 (crate (name "slice_ring_buf") (vers "0.1.0") (hash "0kyasy3yj10118p5v5j5ah7075gdbrils90lfykn65hjim8yy30g")))

(define-public crate-slice_ring_buf-0.1 (crate (name "slice_ring_buf") (vers "0.1.1") (hash "10g287wydhkqnwqxv3cgl7g9qmhz936zzss7bh3jiajyzxpi7qwz")))

(define-public crate-slice_ring_buf-0.1 (crate (name "slice_ring_buf") (vers "0.1.2") (hash "0l1xk5pcl1nwylf3p57l8grzxkw0g2x2abkvrj61qlx20sjlrnhg")))

(define-public crate-slice_ring_buf-0.1 (crate (name "slice_ring_buf") (vers "0.1.3") (hash "0dhkvybg5j70pkf4k1kg1lbrxbdccg4qwrgk09hz87wc038x5nj4")))

(define-public crate-slice_ring_buf-0.2 (crate (name "slice_ring_buf") (vers "0.2.0") (hash "1zb6daj2fk49qcz9igazhzzqnny3rv4fvmnjxc4a7d6mi5f1zh2y")))

(define-public crate-slice_ring_buf-0.2 (crate (name "slice_ring_buf") (vers "0.2.1") (hash "089fzfm9pqliv55w1i8kqa2arx9vfipqs55419kif0ngryn0n7vv")))

(define-public crate-slice_ring_buf-0.2 (crate (name "slice_ring_buf") (vers "0.2.2") (hash "02s6jn5zrkldhd6crsc3x0dgbr5h2flj9chmklf9p5p4zznxzahy")))

(define-public crate-slice_ring_buf-0.2 (crate (name "slice_ring_buf") (vers "0.2.3") (hash "1az797j2n5b3k59mwqvqxck4bb9s55x4zm5srzs54a4n092rxfv4")))

(define-public crate-slice_ring_buf-0.2 (crate (name "slice_ring_buf") (vers "0.2.4") (hash "1q95cddi99d37qhhqmphw11vryrcpvqgw23yw1d77vlgc3ddji7w")))

(define-public crate-slice_ring_buf-0.2 (crate (name "slice_ring_buf") (vers "0.2.5") (hash "05v7yr0lp3cjg5qbvqawn7y2z9n7670h89yiyla79fiqpii9dmf5")))

(define-public crate-slice_ring_buf-0.2 (crate (name "slice_ring_buf") (vers "0.2.6") (hash "0nalflxpifrl36gxnhmryd5nx58w6si9yq3ibpdz0slz4w7fmhf8")))

(define-public crate-slice_ring_buf-0.2 (crate (name "slice_ring_buf") (vers "0.2.7") (hash "0sg4isp226i0szpxdcdkmq78yn0831rcmx49ka401l0g4lf3di5q")))

(define-public crate-slice_search-0.1 (crate (name "slice_search") (vers "0.1.0") (hash "1g9wmdqb9ivn1zcwmbpqrvw1k110xl6n3i1fn94hhjfr8rwsr91v")))

(define-public crate-slice_search-0.1 (crate (name "slice_search") (vers "0.1.1") (hash "0dh4sfqrp4z9zg3is0hhahprzx3df167qlwdjkdxywqfx0gdkclx")))

(define-public crate-slice_search-0.1 (crate (name "slice_search") (vers "0.1.2") (hash "0dbx1fkyjyw8a0rdm730a4230frd6yr8kzz2g0bxp3q6acv49p82")))

(define-public crate-slice_trait-0.1 (crate (name "slice_trait") (vers "0.1.0") (hash "0ja9wyyiq3h81vq6p336sdchqmqrcdvkgm771vwr5fpxl15vvay2")))

(define-public crate-slicec-0.0.0 (crate (name "slicec") (vers "0.0.0") (hash "1bkwgw1x25240rzgg6sfx09jaw2qn42ssxwx50ai3ikdla2laf5m")))

(define-public crate-slicec-0.1 (crate (name "slicec") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^4.3.15") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "console") (req "^0.15.7") (default-features #t) (kind 0)) (crate-dep (name "convert_case") (req "^0.6.0") (default-features #t) (kind 0)) (crate-dep (name "in_definite") (req "^0.2.5") (default-features #t) (kind 0)) (crate-dep (name "lalrpop") (req "^0.20.0") (kind 1)) (crate-dep (name "lalrpop-util") (req "^0.20.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.171") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.103") (default-features #t) (kind 0)) (crate-dep (name "test-case") (req "^3.1.0") (default-features #t) (kind 2)))) (hash "1chsk4gksjq8r2j8fnp6rfmlibr24x6kr9xwgy29mx5q4s6pjfwp")))

(define-public crate-slicec-0.1 (crate (name "slicec") (vers "0.1.1") (deps (list (crate-dep (name "clap") (req "^4.3.15") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "console") (req "^0.15.7") (default-features #t) (kind 0)) (crate-dep (name "convert_case") (req "^0.6.0") (default-features #t) (kind 0)) (crate-dep (name "in_definite") (req "^0.2.5") (default-features #t) (kind 0)) (crate-dep (name "lalrpop") (req "^0.20.0") (kind 1)) (crate-dep (name "lalrpop-util") (req "^0.20.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.171") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.103") (default-features #t) (kind 0)) (crate-dep (name "test-case") (req "^3.1.0") (default-features #t) (kind 2)))) (hash "0y3qkk0kvbz773y8n8bxnzv3q22vm4cyrlhiwfwspk4z6zlxhc77") (rust-version "1.70")))

(define-public crate-slicec-0.2 (crate (name "slicec") (vers "0.2.0") (deps (list (crate-dep (name "clap") (req "^4.3.15") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "console") (req "^0.15.7") (default-features #t) (kind 0)) (crate-dep (name "convert_case") (req "^0.6.0") (default-features #t) (kind 0)) (crate-dep (name "in_definite") (req "^1.0.0") (default-features #t) (kind 0)) (crate-dep (name "lalrpop") (req "^0.20.0") (kind 1)) (crate-dep (name "lalrpop-util") (req "^0.20.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.193") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.108") (default-features #t) (kind 0)) (crate-dep (name "test-case") (req "^3.3.1") (default-features #t) (kind 2)))) (hash "15f295z7a1w8jdla42qsnvmkhdhq75yp6613cv4yqzb4sv1xf5k3") (rust-version "1.70")))

(define-public crate-slicec-0.2 (crate (name "slicec") (vers "0.2.1") (deps (list (crate-dep (name "clap") (req "^4.3.15") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "console") (req "^0.15.7") (default-features #t) (kind 0)) (crate-dep (name "convert_case") (req "^0.6.0") (default-features #t) (kind 0)) (crate-dep (name "in_definite") (req "^1.0.0") (default-features #t) (kind 0)) (crate-dep (name "lalrpop") (req "^0.20.0") (kind 1)) (crate-dep (name "lalrpop-util") (req "^0.20.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.193") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.108") (default-features #t) (kind 0)) (crate-dep (name "test-case") (req "^3.3.1") (default-features #t) (kind 2)))) (hash "1xic4xll89hxp9cm5azyv9306fk75qpsr104j9i0hphaj0kvqmh2") (rust-version "1.70")))

(define-public crate-slicec-0.3 (crate (name "slicec") (vers "0.3.0") (deps (list (crate-dep (name "clap") (req "^4.3.15") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "console") (req "^0.15.8") (default-features #t) (kind 0)) (crate-dep (name "convert_case") (req "^0.6.0") (default-features #t) (kind 0)) (crate-dep (name "in_definite") (req "^1.0.0") (default-features #t) (kind 0)) (crate-dep (name "lalrpop") (req "^0.20.0") (kind 1)) (crate-dep (name "lalrpop-util") (req "^0.20.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.196") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.113") (default-features #t) (kind 0)) (crate-dep (name "test-case") (req "^3.3.1") (default-features #t) (kind 2)))) (hash "1nhvzn0ah6xl9dgxfs7cjm4ggr0hknvvzdmnd67qy7y2qaljfcbb") (rust-version "1.70")))

(define-public crate-slicec-0.3 (crate (name "slicec") (vers "0.3.1") (deps (list (crate-dep (name "clap") (req "=4.3.20") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "console") (req "^0.15.8") (default-features #t) (kind 0)) (crate-dep (name "convert_case") (req "^0.6.0") (default-features #t) (kind 0)) (crate-dep (name "in_definite") (req "^1.0.0") (default-features #t) (kind 0)) (crate-dep (name "lalrpop") (req "^0.20.2") (kind 1)) (crate-dep (name "lalrpop-util") (req "^0.20.2") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.197") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.115") (default-features #t) (kind 0)) (crate-dep (name "test-case") (req "^3.3.1") (default-features #t) (kind 2)))) (hash "1zdcmnw59c34rjqyjaaplcfkh11hzp7cvf2sylia8559h4h0s657") (rust-version "1.70")))

(define-public crate-slicec-cs-0.0.0 (crate (name "slicec-cs") (vers "0.0.0") (hash "02d1wabb84af83i3pqmaxdxrxp95hwydk2mhmnswx3gbvvj3bs0r")))

(define-public crate-slicec-rs-0.0.0 (crate (name "slicec-rs") (vers "0.0.0") (hash "1p3qvwmpx940hffyn90vrzxwp11cznxh0mgdgfm3vipxmgv26r6b")))

(define-public crate-sliced-0.2 (crate (name "sliced") (vers "0.2.5") (deps (list (crate-dep (name "criterion") (req "^0.4.0") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.8.5") (features (quote ("small_rng"))) (default-features #t) (kind 2)))) (hash "10s8nmd2yi3ajvplmmmkmvd55amx78klvjijr7gr5fns8cahldbm")))

(define-public crate-sliced-0.2 (crate (name "sliced") (vers "0.2.6") (deps (list (crate-dep (name "criterion") (req "^0.4.0") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.8.5") (features (quote ("small_rng"))) (default-features #t) (kind 2)))) (hash "1am8709b3a14nfkhf2gzkxhkzc4w72m50x3hgzigk2mk4z2yijb8")))

(define-public crate-sliced-0.2 (crate (name "sliced") (vers "0.2.7") (deps (list (crate-dep (name "criterion") (req "^0.4.0") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.8.5") (features (quote ("small_rng"))) (default-features #t) (kind 2)) (crate-dep (name "rand_distr") (req "^0.4.3") (default-features #t) (kind 2)))) (hash "1bbq0ficix4avdhkkh4kw0cqv8iarha1h4f12adnqvihp8sd5fh5")))

(define-public crate-sliced-0.3 (crate (name "sliced") (vers "0.3.0") (deps (list (crate-dep (name "criterion") (req "^0.4.0") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.8.5") (features (quote ("small_rng"))) (default-features #t) (kind 2)) (crate-dep (name "rand_distr") (req "^0.4.3") (default-features #t) (kind 2)))) (hash "029sjckyajvkrf216mqyikf6gvcvbmrgv5dlvfj4kqvs9avv10y6")))

(define-public crate-sliced-0.3 (crate (name "sliced") (vers "0.3.2") (deps (list (crate-dep (name "criterion") (req "^0.4.0") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.8.5") (features (quote ("small_rng"))) (default-features #t) (kind 2)) (crate-dep (name "rand_distr") (req "^0.4.3") (default-features #t) (kind 2)))) (hash "0z3cm0b82qhzcvyc19nii6g2vfz87crq68bpk1qswwnnr9jkab10")))

(define-public crate-sliced-0.3 (crate (name "sliced") (vers "0.3.3") (deps (list (crate-dep (name "criterion") (req "^0.4.0") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.8.5") (features (quote ("small_rng"))) (default-features #t) (kind 2)) (crate-dep (name "rand_distr") (req "^0.4.3") (default-features #t) (kind 2)))) (hash "061hrf9mydicmn0my5wp59lfa0qgw1yi39ws9vzz2mdv6gj435wg")))

(define-public crate-slicedisplay-0.1 (crate (name "slicedisplay") (vers "0.1.0") (hash "1zig1q0x733qdys0ihhxbpzczjm2vh0f4p5g4dikrfjmbnv982z6")))

(define-public crate-slicedisplay-0.2 (crate (name "slicedisplay") (vers "0.2.0") (hash "1rlf7lm32s8xyz3gz12xjrksnnb8b7w5biz4rwpd0azz86iv2c31")))

(define-public crate-slicedisplay-0.2 (crate (name "slicedisplay") (vers "0.2.1") (hash "17gb9dmyfa5yag202r31s754kqmk6irqj95ql83gpp5zxinwj52x")))

(define-public crate-slicedisplay-0.2 (crate (name "slicedisplay") (vers "0.2.2") (hash "1rshlszv3ba1n0q829qly0vlb28bqraa1i0k0861nh5psgrhic78")))

(define-public crate-slicedvec-0.1 (crate (name "slicedvec") (vers "0.1.0") (deps (list (crate-dep (name "criterion") (req "^0.4.0") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.8.5") (features (quote ("small_rng"))) (default-features #t) (kind 2)))) (hash "1ilk9pzsma3434s2f95vdsj04660q063i6hdgda41l2i0bpvqlc3")))

(define-public crate-slicedvec-0.1 (crate (name "slicedvec") (vers "0.1.1") (deps (list (crate-dep (name "criterion") (req "^0.4.0") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.8.5") (features (quote ("small_rng"))) (default-features #t) (kind 2)))) (hash "15l1czcjv2x6mj5bb80ifgbw20h49fjqy6k0lwzlrfc4wg5csi3h")))

(define-public crate-slicedvec-0.2 (crate (name "slicedvec") (vers "0.2.0") (deps (list (crate-dep (name "criterion") (req "^0.4.0") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.8.5") (features (quote ("small_rng"))) (default-features #t) (kind 2)))) (hash "0fcw4lypw8537v96nzp4ddisg1lyy5dwhz05bnwg9ryc9jjr18dq")))

(define-public crate-slicedvec-0.2 (crate (name "slicedvec") (vers "0.2.1") (deps (list (crate-dep (name "criterion") (req "^0.4.0") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.8.5") (features (quote ("small_rng"))) (default-features #t) (kind 2)))) (hash "0wzfal9r72r6flc6vk37axdvsmh55sc9hkwgyv4zmd2gyj4bpjqv")))

(define-public crate-slicedvec-0.2 (crate (name "slicedvec") (vers "0.2.2") (deps (list (crate-dep (name "criterion") (req "^0.4.0") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.8.5") (features (quote ("small_rng"))) (default-features #t) (kind 2)))) (hash "041pa5s3ygmh42c5yc0fxgrvgw78wkbc25ka1420ac87s98iwav2")))

(define-public crate-slicedvec-0.2 (crate (name "slicedvec") (vers "0.2.3") (deps (list (crate-dep (name "criterion") (req "^0.4.0") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.8.5") (features (quote ("small_rng"))) (default-features #t) (kind 2)))) (hash "0pc6p3w222hkg4205zmrjq3hiynqbxr7djdpwwkzcv4xn3vj3ydq")))

(define-public crate-slicefields-1 (crate (name "slicefields") (vers "1.0.0") (deps (list (crate-dep (name "prettyplease") (req "^0.2") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2.0") (features (quote ("full"))) (optional #t) (default-features #t) (kind 0)))) (hash "18aiskjrzrnj3dj6w2bkjdifhp3a9ly9cxjx1pavdjb6fhvv0gdf") (v 2) (features2 (quote (("debug" "dep:syn" "dep:prettyplease"))))))

(define-public crate-slicefields-1 (crate (name "slicefields") (vers "1.0.1") (deps (list (crate-dep (name "prettyplease") (req "^0.2") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2.0") (features (quote ("full"))) (optional #t) (default-features #t) (kind 0)))) (hash "1dr7r3qvgyin6fhdp2glvmbws50vjy1d410sxqn5bxf7cdrs3rj0") (v 2) (features2 (quote (("debug" "dep:syn" "dep:prettyplease"))))))

(define-public crate-slicefields-1 (crate (name "slicefields") (vers "1.0.2") (deps (list (crate-dep (name "prettyplease") (req "^0.2") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2.0") (features (quote ("full"))) (optional #t) (default-features #t) (kind 0)))) (hash "0kzqhasvxpa1fddac9cbs3lv18j63dc5nsb97kgssa9rhbr45dlx") (v 2) (features2 (quote (("debug" "dep:syn" "dep:prettyplease"))))))

(define-public crate-slicefields-1 (crate (name "slicefields") (vers "1.0.3") (deps (list (crate-dep (name "prettyplease") (req "^0.2") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2.0") (features (quote ("full"))) (optional #t) (default-features #t) (kind 0)))) (hash "13n5lhplz77b7zwg7aakfjxvpqbiv9dzr0pgcrhs9v6ahyyrbvw6") (features (quote (("unstable")))) (v 2) (features2 (quote (("debug" "dep:syn" "dep:prettyplease"))))))

(define-public crate-slicefields-1 (crate (name "slicefields") (vers "1.0.4") (deps (list (crate-dep (name "prettyplease") (req "^0.2") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2.0") (features (quote ("full"))) (optional #t) (default-features #t) (kind 0)))) (hash "1lcmb8kymy47w2cl4izf0r3mnypp1rxpwnr9xbrcbalm2xxvzsj5") (features (quote (("unstable")))) (v 2) (features2 (quote (("debug" "dep:syn" "dep:prettyplease"))))))

(define-public crate-slicejson-0.1 (crate (name "slicejson") (vers "0.1.0") (deps (list (crate-dep (name "anyhow") (req "^1.0.58") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^4") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "regex") (req "=1.10.2") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.139") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.82") (default-features #t) (kind 0)) (crate-dep (name "simd-json") (req "^0.13.6") (default-features #t) (kind 0)))) (hash "0hc53b0kkg2iknp333cwz9w3vr2k8fx7rivrydx2kd5ybc5xr48a")))

(define-public crate-slicejson-0.1 (crate (name "slicejson") (vers "0.1.1") (deps (list (crate-dep (name "anyhow") (req "^1.0.58") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^4") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "regex") (req "=1.10.2") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.139") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.82") (default-features #t) (kind 0)) (crate-dep (name "simd-json") (req "^0.13.6") (default-features #t) (kind 0)))) (hash "1n2d6306afr1v4ywg8xscbb2j99spj107mzf574j5m3d1apbsrb4")))

(define-public crate-slicejson-0.1 (crate (name "slicejson") (vers "0.1.2") (deps (list (crate-dep (name "anyhow") (req "^1.0.58") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^4") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "regex") (req "=1.10.2") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.139") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.82") (default-features #t) (kind 0)) (crate-dep (name "simd-json") (req "^0.13.6") (default-features #t) (kind 0)))) (hash "04md9cngg36ccaqcy3zy15381w5j4sbcg92z6v31walnqbinvxn3")))

(define-public crate-slicemath-0.1 (crate (name "slicemath") (vers "0.1.0") (hash "04yr3a8q63qr3prz9brq2hbxlqfvik16bs09rbjjf9jh7ln5ffzg")))

(define-public crate-slicer-0.1 (crate (name "slicer") (vers "0.1.0") (hash "1cl7gfizzd8cdp1sk72di808g4cf6q7q8d5z0abm8qpjnhpani2j")))

(define-public crate-slicer-0.1 (crate (name "slicer") (vers "0.1.1") (hash "0280gb22iaivkrvp0rdqyp3dij1km4i4aylfpmlwj7sk05plzxwr")))

(define-public crate-sliceread-0.1 (crate (name "sliceread") (vers "0.1.0") (deps (list (crate-dep (name "criterion") (req "^0.3.3") (default-features #t) (kind 2)))) (hash "1arwxgwfpr6mj6n9qzvc0q35hcl4mf68l8p4gizzf19a5cqc2nnj") (yanked #t)))

(define-public crate-sliceread-0.1 (crate (name "sliceread") (vers "0.1.1") (deps (list (crate-dep (name "criterion") (req "^0.3.3") (default-features #t) (kind 2)))) (hash "1rdlhsn389db770djpbc34nh4xdq6vwj8qh0pa45a081sy3c84s6") (yanked #t)))

(define-public crate-sliceread-0.1 (crate (name "sliceread") (vers "0.1.2") (deps (list (crate-dep (name "criterion") (req "^0.3.3") (default-features #t) (kind 2)))) (hash "1zn55nb0lc6587yak52k9gckyq4q8apck5nlhsv7nx5nabhnwvhq") (yanked #t)))

(define-public crate-sliceread-0.1 (crate (name "sliceread") (vers "0.1.3") (deps (list (crate-dep (name "criterion") (req "^0.3.3") (default-features #t) (kind 2)))) (hash "1yvyd4ajb0bpbbywymr6a46dl4v94h8svkjig51g3k63hj0vfx8w") (yanked #t)))

(define-public crate-slices-0.1 (crate (name "slices") (vers "0.1.0") (deps (list (crate-dep (name "hack") (req "= 0.1.0") (default-features #t) (kind 0) (package "slices-hack")) (crate-dep (name "proc-macro-hack") (req "~0.5") (default-features #t) (kind 0)))) (hash "00g5iylg4hwky474mrkcmzb6y28fx6zjpsjf57nql8jg3y9kkw9d")))

(define-public crate-slices-0.1 (crate (name "slices") (vers "0.1.1") (deps (list (crate-dep (name "hack") (req "= 0.1.1") (default-features #t) (kind 0) (package "slices-hack")) (crate-dep (name "proc-macro-hack") (req "~0.5") (default-features #t) (kind 0)))) (hash "1brxndlk6l72f92zifh48l6xz2705ypw05lfkgkdag73b1znydkk")))

(define-public crate-slices-0.2 (crate (name "slices") (vers "0.2.0") (deps (list (crate-dep (name "faster-hex") (req "^0.6.0") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1.0.27") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.9") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.73") (default-features #t) (kind 0)))) (hash "1ks9fw8dmsp2r60bp6nr6vk3qbncnh2fvxp972ldr71ni92nw27j")))

(define-public crate-slices-hack-0.1 (crate (name "slices-hack") (vers "0.1.0") (deps (list (crate-dep (name "faster-hex") (req "~0.3") (default-features #t) (kind 0)) (crate-dep (name "proc-macro-hack") (req "~0.5") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "~0.4") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "~0.6") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "~0.15") (default-features #t) (kind 0)))) (hash "0x01z3p5qns2r7i3x05bqnqw51zgl4zna1b1iyd3idq5j72lq3g4")))

(define-public crate-slices-hack-0.1 (crate (name "slices-hack") (vers "0.1.1") (deps (list (crate-dep (name "faster-hex") (req "~0.4") (default-features #t) (kind 0)) (crate-dep (name "proc-macro-hack") (req "~0.5") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "~1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "~1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "~1.0") (default-features #t) (kind 0)))) (hash "07crddhx38qjwcdn7imwl3xs82cxk8hccx6mzx7wiwxas8xwy2ab")))

(define-public crate-slices_dispatch_wide-0.1 (crate (name "slices_dispatch_wide") (vers "0.1.0") (deps (list (crate-dep (name "itertools") (req "^0.10") (default-features #t) (kind 0)) (crate-dep (name "paste") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "wide") (req "^0.7.4") (default-features #t) (kind 0)))) (hash "0rm5dfzw6nphm4ya83xmvkix96zps3b4jzlkix9gkzz5nfqh8mpw")))

(define-public crate-slices_dispatch_wide-0.1 (crate (name "slices_dispatch_wide") (vers "0.1.1") (deps (list (crate-dep (name "itertools") (req "^0.10") (default-features #t) (kind 0)) (crate-dep (name "paste") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "wide") (req "^0.7.4") (default-features #t) (kind 0)))) (hash "11kgfffrypnsdkhc8jb8wnl89zv36ds96lqdm6mrf56ml0rqa6fs")))

(define-public crate-sliceslice-0.1 (crate (name "sliceslice") (vers "0.1.0") (deps (list (crate-dep (name "criterion") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "memchr") (req "^2.3") (default-features #t) (kind 0)) (crate-dep (name "memmem") (req "^0.1") (default-features #t) (kind 2)) (crate-dep (name "twoway") (req "^0.2") (default-features #t) (kind 2)))) (hash "0bxwpwyzbrc93j93xn0v5s5q9bdf3722yppl3aqsyj8g8gkcrmb7") (yanked #t)))

(define-public crate-sliceslice-0.2 (crate (name "sliceslice") (vers "0.2.0") (deps (list (crate-dep (name "criterion") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "memchr") (req "^2.3") (default-features #t) (kind 0)) (crate-dep (name "memmem") (req "^0.1") (default-features #t) (kind 2)) (crate-dep (name "twoway") (req "^0.2") (default-features #t) (kind 2)))) (hash "0bxl449xyx5sz597c2bdangl6q3spj32r3v16v2z6jpaz3g71dv1")))

(define-public crate-sliceslice-0.2 (crate (name "sliceslice") (vers "0.2.1") (deps (list (crate-dep (name "criterion") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "memchr") (req "^2.3") (default-features #t) (kind 0)) (crate-dep (name "memmem") (req "^0.1") (default-features #t) (kind 2)) (crate-dep (name "twoway") (req "^0.2") (default-features #t) (kind 2)))) (hash "1nm6ywl8pa2dp5yjkql9harm8rfzw6vmn8dzqw8z132c5kpj8hqi")))

(define-public crate-sliceslice-0.3 (crate (name "sliceslice") (vers "0.3.0") (deps (list (crate-dep (name "memchr") (req "^2.3") (default-features #t) (kind 0)) (crate-dep (name "seq-macro") (req "^0.2") (default-features #t) (kind 0)))) (hash "19zsafb7gfm4nr8fkx8m71piw798231lfx7c0rn22lmjmqn2jbr4")))

(define-public crate-sliceslice-0.3 (crate (name "sliceslice") (vers "0.3.1") (deps (list (crate-dep (name "memchr") (req "^2.3") (default-features #t) (kind 0)) (crate-dep (name "seq-macro") (req "^0.2") (default-features #t) (kind 0)))) (hash "173fmw79bzznw3wh6xzbv4yrkfg58psw9xw9ilv5pw8x879552nb")))

(define-public crate-sliceslice-0.4 (crate (name "sliceslice") (vers "0.4.0") (deps (list (crate-dep (name "cfg-if") (req "^1") (default-features #t) (kind 2)) (crate-dep (name "memchr") (req "^2.3") (default-features #t) (kind 0)) (crate-dep (name "memmap2") (req "^0.5") (default-features #t) (kind 2)) (crate-dep (name "multiversion") (req "^0.6") (kind 0)) (crate-dep (name "seq-macro") (req "^0.2") (default-features #t) (kind 0)))) (hash "02amqzbh8pai42aiwf34j7cdz5ilj7g23cjngb1gs8xan6ifqkn8") (features (quote (("stdsimd"))))))

(define-public crate-sliceslice-0.4 (crate (name "sliceslice") (vers "0.4.1") (deps (list (crate-dep (name "cfg-if") (req "^1") (default-features #t) (kind 2)) (crate-dep (name "memchr") (req "^2.3") (default-features #t) (kind 0)) (crate-dep (name "memmap2") (req "^0.5") (default-features #t) (kind 2)) (crate-dep (name "multiversion") (req "^0.6") (kind 0)) (crate-dep (name "seq-macro") (req "^0.2") (default-features #t) (kind 0)))) (hash "0h525zfjj3jpc08x3qkm8wawx3annghappxm0xdk12a60dxvddn7") (features (quote (("stdsimd") ("aarch64"))))))

(define-public crate-sliceslice-0.4 (crate (name "sliceslice") (vers "0.4.2") (deps (list (crate-dep (name "cfg-if") (req "^1") (default-features #t) (kind 2)) (crate-dep (name "memchr") (req "^2.3") (default-features #t) (kind 0)) (crate-dep (name "memmap2") (req "^0.5") (default-features #t) (kind 2)) (crate-dep (name "multiversion") (req "^0.6") (kind 0)) (crate-dep (name "seq-macro") (req "^0.3") (default-features #t) (kind 0)))) (hash "1il76pi1jkvv3bdkq945w86nfsidcp0nwa897f83k19ar5y43ch4") (features (quote (("stdsimd"))))))

(define-public crate-slicestar-fake-0.1 (crate (name "slicestar-fake") (vers "0.1.0") (hash "13nqhfyyqyas8lxl0j18f97hwfsqnb28z82h6x80nnf8b98vqsk8")))

(define-public crate-slicestring-0.1 (crate (name "slicestring") (vers "0.1.0") (hash "0i5nb53d7lfm6vsc87g5pg28idvlj7dcnig0ysyvqxlqjpqrsqd6")))

(define-public crate-slicestring-0.1 (crate (name "slicestring") (vers "0.1.1") (hash "1qrcca5dz2bg36hj3n5v3qj42z52865gg84110907rls9xb82kls") (yanked #t)))

(define-public crate-slicestring-0.1 (crate (name "slicestring") (vers "0.1.2") (hash "19f07ff3758pyjfraxggfypciqmczahsc11bqp4s054f8v8b305s") (yanked #t)))

(define-public crate-slicestring-0.1 (crate (name "slicestring") (vers "0.1.3") (hash "106i4c9fmfm5z3v2mi7yml6hkkblc2d0mji024m7rb3jasf9fsv3")))

(define-public crate-slicestring-0.2 (crate (name "slicestring") (vers "0.2.0") (hash "1p1s79k72d67qlw97sfkr6fq9mxcjdfnpg7y1b10b7chnh8mvm7g")))

(define-public crate-slicestring-0.2 (crate (name "slicestring") (vers "0.2.1") (hash "1x32p79382pbdprrc7k9npp7dv7waqin2w1sjyql023pqvplix8f")))

(define-public crate-slicestring-0.3 (crate (name "slicestring") (vers "0.3.0") (hash "1nrd82c6wjbhj3kcsanfwvalaab2cnbkl61a3kxzk5x7a2pf3j8v") (yanked #t)))

(define-public crate-slicestring-0.3 (crate (name "slicestring") (vers "0.3.1") (hash "0v62xdkilw3gh27qgh5whxax8swjfv8viqqzi1bf1alv6sa85gdc") (yanked #t)))

(define-public crate-slicestring-0.3 (crate (name "slicestring") (vers "0.3.2") (hash "0xfzv796m8i98psccxzmp85ix2xz6fk5mg40zyrjbnklxva0lyys")))

(define-public crate-slicestring-0.3 (crate (name "slicestring") (vers "0.3.3") (hash "1d27vlccl0b82hj83mqi67ig13vjfdmpw92f176n49mbdgzls8c4")))

(define-public crate-slicetools-0.1 (crate (name "slicetools") (vers "0.1.0") (hash "1d66m89ah9wpwsaqnkmqnxdgn79l3rcjjzxb2dvliii57x6rdl3k")))

(define-public crate-slicetools-0.1 (crate (name "slicetools") (vers "0.1.1") (hash "1bgp7chzc291s165mhn8cgm5c238580gprzh5xnr8zs6v7b9vfb2")))

(define-public crate-slicetools-0.2 (crate (name "slicetools") (vers "0.2.0") (hash "1509gx63adikshwxbr99f9b4l87kkgy3ps5c6h826ili0pyfx59p")))

(define-public crate-slicetools-0.3 (crate (name "slicetools") (vers "0.3.0") (hash "0l63mj3k5ghnzh7i2illk3c0q61swhf4mppnymxr6g6jbdyr013d")))

(define-public crate-slicevec-0.1 (crate (name "slicevec") (vers "0.1.0") (hash "1vx5wdj0sqr5xz53gbqgaxjz1lc6yga16bcf0yrlqvrn8wv7cwqg")))

(define-public crate-slicevec-0.1 (crate (name "slicevec") (vers "0.1.1") (hash "1y1dji654k0z7wb1phv14x7qlwjffjvyz9h1yr0h7nh6zb1yqf4q")))

(define-public crate-slick-0.1 (crate (name "slick") (vers "0.1.0") (hash "1jcnls0l6ch6zfq56cm0irr2mxs83bhkffp1vbr1f8x1gpvwcdc0")))

(define-public crate-slick-0.1 (crate (name "slick") (vers "0.1.1") (hash "0ln6pa6xxhn969xqik70658nvvjnqvl29kqwlbxric7sjr3h63cg")))

(define-public crate-slick-0.1 (crate (name "slick") (vers "0.1.2") (deps (list (crate-dep (name "clap") (req "^2") (default-features #t) (kind 0)) (crate-dep (name "git2") (req "^0.9") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)))) (hash "0q0i9wszqj7jfvdgbszk8silvlwr4k07vrclp6g6pap95xshdlcz")))

(define-public crate-slick-0.1 (crate (name "slick") (vers "0.1.3") (deps (list (crate-dep (name "clap") (req "^2") (default-features #t) (kind 0)) (crate-dep (name "git2") (req "^0.9") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)))) (hash "0mxb6nwgq3z7xprnzcssaam17776c8705w5z6s7kpq3g2gfg7jxh")))

(define-public crate-slick-0.1 (crate (name "slick") (vers "0.1.4") (deps (list (crate-dep (name "clap") (req "^2") (default-features #t) (kind 0)) (crate-dep (name "git2") (req "^0.9") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)))) (hash "1rkm6786gnkaf12gaz12lvjnsa8x4vn15xxydbmff7hk8fmw9kx8")))

(define-public crate-slick-0.1 (crate (name "slick") (vers "0.1.5") (deps (list (crate-dep (name "clap") (req "^2") (default-features #t) (kind 0)) (crate-dep (name "git2") (req "^0.9") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)))) (hash "1psr56miw3w8c3pscfifanzzsd396pgj8qzvk16yhbm17qrmlhhm")))

(define-public crate-slick-0.1 (crate (name "slick") (vers "0.1.6") (deps (list (crate-dep (name "clap") (req "^2") (default-features #t) (kind 0)) (crate-dep (name "git2") (req "^0.9") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)))) (hash "0s6mjdb9312nx2jwar12sxn5mc4balx1m4b1z4d6ywmnsgic2n4w")))

(define-public crate-slick-0.1 (crate (name "slick") (vers "0.1.7") (deps (list (crate-dep (name "clap") (req "^2") (default-features #t) (kind 0)) (crate-dep (name "compound_duration") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "git2") (req "^0.9") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)))) (hash "0jj7qycabin6plqn41437dh0fma1djq9p0y41xl9w4b2xr1kayhh")))

(define-public crate-slick-0.1 (crate (name "slick") (vers "0.1.8") (deps (list (crate-dep (name "clap") (req "^2") (default-features #t) (kind 0)) (crate-dep (name "compound_duration") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "git2") (req "^0.9") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)))) (hash "143zfii6y47cslpjg2axcd2p6d5ig76pzllw86l56b02xswsvvkz")))

(define-public crate-slick-0.1 (crate (name "slick") (vers "0.1.9") (deps (list (crate-dep (name "clap") (req "^2") (default-features #t) (kind 0)) (crate-dep (name "compound_duration") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "git2") (req "^0.9") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)))) (hash "17adzacqmx20g4filayz7b42y1r83s0pd9nblsl8hs7vn3kk0frk")))

(define-public crate-slick-0.1 (crate (name "slick") (vers "0.1.10") (deps (list (crate-dep (name "clap") (req "^2") (default-features #t) (kind 0)) (crate-dep (name "compound_duration") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "git2") (req "^0.9") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)))) (hash "1pm2gva6msdzj622iwhl6nw0qp7xqsgvswj6njp5i3kn0xzfzrbx")))

(define-public crate-slick-0.1 (crate (name "slick") (vers "0.1.11") (deps (list (crate-dep (name "clap") (req "^2") (default-features #t) (kind 0)) (crate-dep (name "compound_duration") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "git2") (req "^0.9") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)))) (hash "0m5r6hdj4fqlfgny1bnp2sqaz04dnpj0dzyyxzb9x5jszcsapzcv")))

(define-public crate-slick-0.1 (crate (name "slick") (vers "0.1.12") (deps (list (crate-dep (name "clap") (req "^2") (default-features #t) (kind 0)) (crate-dep (name "compound_duration") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "git2") (req "^0.9") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)))) (hash "1l6zp6avjf925ab8ss75bs1kqpad3sll7l19if0z9g8rf1ldndz1")))

(define-public crate-slick-0.2 (crate (name "slick") (vers "0.2.0") (deps (list (crate-dep (name "clap") (req "^2") (default-features #t) (kind 0)) (crate-dep (name "compound_duration") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "git2") (req "^0.9") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "users") (req "^0.9") (default-features #t) (kind 0)))) (hash "00l2agwaa5szc5b7i9j9lms4314x4wlzkbs6zb17xqn3lfpj2hr5")))

(define-public crate-slick-0.2 (crate (name "slick") (vers "0.2.1") (deps (list (crate-dep (name "clap") (req "^2") (default-features #t) (kind 0)) (crate-dep (name "compound_duration") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "git2") (req "^0.9") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "users") (req "^0.9") (default-features #t) (kind 0)))) (hash "10vn4j32a8d6881zs49kmh4hyfk26r3dq01wi863cw634c8z5v3x")))

(define-public crate-slick-0.3 (crate (name "slick") (vers "0.3.0") (deps (list (crate-dep (name "clap") (req "^2") (default-features #t) (kind 0)) (crate-dep (name "compound_duration") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "git2") (req "^0.9") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "users") (req "^0.9") (default-features #t) (kind 0)))) (hash "0aj0kn5a09wy94psjkwyff81bhw9cmckmdv91qlxj0d031p97sk2")))

(define-public crate-slick-0.4 (crate (name "slick") (vers "0.4.0") (deps (list (crate-dep (name "clap") (req "^2") (default-features #t) (kind 0)) (crate-dep (name "compound_duration") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "git2") (req "^0.9") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "users") (req "^0.9") (default-features #t) (kind 0)))) (hash "1qksriicb8j6v0l743qgvq38y4xjpp9lx2iihwh19fbwkwqvw2hj")))

(define-public crate-slick-0.4 (crate (name "slick") (vers "0.4.1") (deps (list (crate-dep (name "clap") (req "^2") (default-features #t) (kind 0)) (crate-dep (name "compound_duration") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "git2") (req "^0.9") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "users") (req "^0.9") (default-features #t) (kind 0)))) (hash "1r80d5xk2awkh8c5lnc5pg0ccksc26ylkgbpfdlynd77349ji272")))

(define-public crate-slick-0.5 (crate (name "slick") (vers "0.5.0") (deps (list (crate-dep (name "clap") (req "^2") (default-features #t) (kind 0)) (crate-dep (name "compound_duration") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "git2") (req "^0.9") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "users") (req "^0.9") (default-features #t) (kind 0)))) (hash "0lyrrgk2bdaw6fxgfq93n984rhgbq305axrdnmwyjsgvj1dr0r8f")))

(define-public crate-slick-0.6 (crate (name "slick") (vers "0.6.0") (deps (list (crate-dep (name "clap") (req "^2") (default-features #t) (kind 0)) (crate-dep (name "compound_duration") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "git2") (req "^0.9") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "users") (req "^0.9") (default-features #t) (kind 0)))) (hash "021nnm34102agdxq06faknfyfmwskssng2dpyy8blxj2np9mw6pq")))

(define-public crate-slick-0.6 (crate (name "slick") (vers "0.6.1") (deps (list (crate-dep (name "clap") (req "^2") (default-features #t) (kind 0)) (crate-dep (name "compound_duration") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "git2") (req "^0.9") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "users") (req "^0.9") (default-features #t) (kind 0)))) (hash "091gzji8ik6gpn9zgrkv28i2rvgfpwdhcg8gbm11a83b57kp50jp")))

(define-public crate-slick-0.6 (crate (name "slick") (vers "0.6.2") (deps (list (crate-dep (name "clap") (req "^2") (default-features #t) (kind 0)) (crate-dep (name "compound_duration") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "git2") (req "^0.9") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "users") (req "^0.9") (default-features #t) (kind 0)))) (hash "1y8k8hz0b1rd4cr9m0amskr925ykjc7izy2ndyybcdadq376b75d")))

(define-public crate-slick-0.7 (crate (name "slick") (vers "0.7.0") (deps (list (crate-dep (name "clap") (req "^2") (default-features #t) (kind 0)) (crate-dep (name "compound_duration") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "git2") (req "^0.9") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "users") (req "^0.9") (default-features #t) (kind 0)))) (hash "1g30aa0c4jh8jzv45swf99pzdgambv5kjws546lqfb6nrkchdxgz")))

(define-public crate-slick-0.7 (crate (name "slick") (vers "0.7.1") (deps (list (crate-dep (name "clap") (req "^2") (default-features #t) (kind 0)) (crate-dep (name "compound_duration") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "git2") (req "^0.9") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "users") (req "^0.9") (default-features #t) (kind 0)))) (hash "0crxbw2qy3zacmzrm67rdcx5lfz8a7gkzjy179jzqsff1nvkz6x8")))

(define-public crate-slick-0.7 (crate (name "slick") (vers "0.7.2") (deps (list (crate-dep (name "clap") (req "^2") (default-features #t) (kind 0)) (crate-dep (name "compound_duration") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "git2") (req "^0.10") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "users") (req "^0.9") (default-features #t) (kind 0)))) (hash "0g4lyif38ilpwcmfplpmx018bg9s1cb49rrad1gnas5lwn56a3qz")))

(define-public crate-slick-0.8 (crate (name "slick") (vers "0.8.0") (deps (list (crate-dep (name "clap") (req "^2") (default-features #t) (kind 0)) (crate-dep (name "compound_duration") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "git2") (req "^0.10") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "users") (req "^0.9") (default-features #t) (kind 0)))) (hash "0vv4s51q9072vfgmhxvx5v8yc88s0s27dqg8hggc7w83nb1cf619")))

(define-public crate-slick-0.9 (crate (name "slick") (vers "0.9.0") (deps (list (crate-dep (name "clap") (req "^2") (default-features #t) (kind 0)) (crate-dep (name "compound_duration") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "git2") (req "^0.10") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "users") (req "^0.9") (default-features #t) (kind 0)))) (hash "036xjn76qhicfjxhvk6zigic95nx6rh1flrai2yzim7nzhwvp2m2")))

(define-public crate-slick-0.9 (crate (name "slick") (vers "0.9.1") (deps (list (crate-dep (name "clap") (req "^2") (default-features #t) (kind 0)) (crate-dep (name "compound_duration") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "git2") (req "^0.11") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "users") (req "^0.9") (default-features #t) (kind 0)))) (hash "1nymv1by5v3qiwmrg261nrkl06paq2kf8a7ssglidh7dg4b7p9ag")))

(define-public crate-slick-0.9 (crate (name "slick") (vers "0.9.2") (deps (list (crate-dep (name "clap") (req "^2.33") (default-features #t) (kind 0)) (crate-dep (name "compound_duration") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "git2") (req "^0.13") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "users") (req "^0.9") (default-features #t) (kind 0)))) (hash "18jabf5r4fsz42h2dwp9lh5p3gh4sv61d1461qfnqf0f0bix1zhl")))

(define-public crate-slick-0.9 (crate (name "slick") (vers "0.9.3") (deps (list (crate-dep (name "clap") (req "^2.33") (default-features #t) (kind 0)) (crate-dep (name "compound_duration") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "git2") (req "^0.13") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "users") (req "^0.11") (default-features #t) (kind 0)))) (hash "14kaha0vd7li1jh41hn5j5gdbh5yla6ajb4i06vzj52ns2q75mcx")))

(define-public crate-slick-0.9 (crate (name "slick") (vers "0.9.4") (deps (list (crate-dep (name "clap") (req "^2.33") (default-features #t) (kind 0)) (crate-dep (name "compound_duration") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "git2") (req "^0.13") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "users") (req "^0.11") (default-features #t) (kind 0)))) (hash "09272y6ya5qqfk5q8b34na9rij9d2xd0gb3zvbmmk9kb1k7zxmds")))

(define-public crate-slick-0.9 (crate (name "slick") (vers "0.9.5") (deps (list (crate-dep (name "clap") (req "^2.33") (default-features #t) (kind 0)) (crate-dep (name "compound_duration") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "git2") (req "^0.13") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "users") (req "^0.11") (default-features #t) (kind 0)))) (hash "052kn809dhk3wlp9sh5njj6gvsy1xhhyqqcca67xbba4xikjcqp4")))

(define-public crate-slick-0.10 (crate (name "slick") (vers "0.10.0") (deps (list (crate-dep (name "clap") (req "^3.1") (default-features #t) (kind 0)) (crate-dep (name "compound_duration") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "git2") (req "^0.14") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "users") (req "^0.11") (default-features #t) (kind 0)))) (hash "0f4ka5vy0ip1fn4n2h0kzrpf861s1fxvssrnb99kzp7y876zs5bz")))

(define-public crate-slick-0.11 (crate (name "slick") (vers "0.11.0") (deps (list (crate-dep (name "clap") (req "^3.1") (default-features #t) (kind 0)) (crate-dep (name "compound_duration") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "git2") (req "^0.17") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "users") (req "^0.11") (default-features #t) (kind 0)))) (hash "0kfkp1zf1hq1d9ha8iqw5j8p9s62mipavjlxrc79ikjxnma8d10m")))

(define-public crate-slick-0.11 (crate (name "slick") (vers "0.11.1") (deps (list (crate-dep (name "clap") (req "^3.1") (default-features #t) (kind 0)) (crate-dep (name "compound_duration") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "git2") (req "^0.18") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "users") (req "^0.11") (default-features #t) (kind 0)))) (hash "1j4c2alp5d0f8z3mgmlp4g8jwd6swsa1ka2rsibrcr3fbimgfqrb")))

(define-public crate-slick-0.11 (crate (name "slick") (vers "0.11.2") (deps (list (crate-dep (name "clap") (req "^4") (default-features #t) (kind 0)) (crate-dep (name "compound_duration") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "git2") (req "^0.18") (default-features #t) (kind 0)) (crate-dep (name "openssl") (req "^0.10") (features (quote ("vendored"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "users") (req "^0.11") (default-features #t) (kind 0)))) (hash "0rhq8gr0aaq9g1c69xahkn2pr7hc7z7917sv4gsrl70v8b7r312l")))

(define-public crate-slick-0.12 (crate (name "slick") (vers "0.12.0") (deps (list (crate-dep (name "clap") (req "^4") (default-features #t) (kind 0)) (crate-dep (name "compound_duration") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "git2") (req "^0.18") (default-features #t) (kind 0)) (crate-dep (name "openssl") (req "^0.10") (features (quote ("vendored"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "users") (req "^0.11") (default-features #t) (kind 0)))) (hash "0nm3r6c36dsrbvg5svpni97ykis2rhnsgpzd43ibc5zbnsfdjw11")))

(define-public crate-slick2-0.2 (crate (name "slick2") (vers "0.2.0") (hash "10hvrcd0g40zjxpls24mb40ip35iffygqcvfk7cvibx60lcc1vpr")))

(define-public crate-slick2-0.1 (crate (name "slick2") (vers "0.1.0") (deps (list (crate-dep (name "blake3") (req "^0.3.6") (default-features #t) (kind 0)) (crate-dep (name "numext-fixed-uint") (req "^0.1.4") (default-features #t) (kind 0)))) (hash "056hilri7b07fgddzw35qsdjzpzpzfjdj08z9xp5vaj9hmwpbk63")))

