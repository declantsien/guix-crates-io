(define-module (crates-io sl mp) #:use-module (crates-io))

(define-public crate-slmp_client-0.1 (crate (name "slmp_client") (vers "0.1.0") (deps (list (crate-dep (name "num") (req "^0.4.0") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.3") (default-features #t) (kind 0)))) (hash "1s35ax81simq6v58xwbr1xbhc4034fi4zbmihb4yp1f0rygm1mrb")))

(define-public crate-slmp_client-0.1 (crate (name "slmp_client") (vers "0.1.1") (deps (list (crate-dep (name "num") (req "^0.4.0") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.3") (default-features #t) (kind 0)))) (hash "01wxh7vwhn83rnr138ayw95k7lh7wb8jpyhig7yh184z4v1s9fdk")))

(define-public crate-slmp_client-0.2 (crate (name "slmp_client") (vers "0.2.0") (deps (list (crate-dep (name "num") (req "^0.4.0") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.3") (default-features #t) (kind 0)))) (hash "1n7g45ig1y5zl5a80jcknzxdnn9rhsvvczkb5887j9wq745pf0yc")))

(define-public crate-slmp_client-0.2 (crate (name "slmp_client") (vers "0.2.1") (deps (list (crate-dep (name "num") (req "^0.4.0") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.3") (default-features #t) (kind 0)))) (hash "1qb66kqi84qlsimwqs2bl78pidkih892gxd13wd760ivid2ld1fj")))

