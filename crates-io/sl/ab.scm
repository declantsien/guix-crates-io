(define-module (crates-io sl ab) #:use-module (crates-io))

(define-public crate-slab-0.1 (crate (name "slab") (vers "0.1.0") (hash "0g7v862p48d5xywv965mf508pg0anzz7y9s93cksbv7wnla5x52a")))

(define-public crate-slab-0.1 (crate (name "slab") (vers "0.1.1") (hash "0zbj5pps6f46ac47hp813gjzvfjg0gx578cmjbni9cw3y6k0bklr")))

(define-public crate-slab-0.1 (crate (name "slab") (vers "0.1.2") (hash "1zqm0g51slgrr21bn0nlljcnqh4j3k436x39wqsdnc4i8cghzinn")))

(define-public crate-slab-0.1 (crate (name "slab") (vers "0.1.3") (hash "0vplakybxbpsk3k5pk1bald28j4ppa8vicvwsyzbl6qqqicgs1yq")))

(define-public crate-slab-0.2 (crate (name "slab") (vers "0.2.0") (hash "1i69vq1q0v7rbs4k718di3c86y2icak0w6y4s7d2ilr8plsd7gbd")))

(define-public crate-slab-0.3 (crate (name "slab") (vers "0.3.0") (hash "08xw8w61zdfn1094qkq1d554vh5wmm9bqdys8gqqxc4sv2pgrd0p")))

(define-public crate-slab-0.4 (crate (name "slab") (vers "0.4.0") (hash "179fj4c5lw8wiaxczcpd59fc6npy7nkvqk3lwg3rxxfgkv6z9vzx")))

(define-public crate-slab-0.4 (crate (name "slab") (vers "0.4.1") (hash "0b8hsgl5adwfcain6aqbzqlg2vwqml8nr16gqqsppxw6p7b7d5sz")))

(define-public crate-slab-0.4 (crate (name "slab") (vers "0.4.2") (hash "1y59xsa27jk84sxzswjk60xcjf8b4fm5960jwpznrrcmasyva4f1")))

(define-public crate-slab-0.4 (crate (name "slab") (vers "0.4.3") (deps (list (crate-dep (name "serde") (req "^1.0.95") (features (quote ("alloc"))) (optional #t) (kind 0)) (crate-dep (name "serde") (req "^1") (features (quote ("derive"))) (default-features #t) (kind 2)) (crate-dep (name "serde_test") (req "^1") (default-features #t) (kind 2)))) (hash "09v57dmy9gnfcj3c6gywp7wi09zywxf0ppj07w02hfvy38ysqwzi") (features (quote (("std") ("default" "std"))))))

(define-public crate-slab-0.4 (crate (name "slab") (vers "0.4.4") (deps (list (crate-dep (name "serde") (req "^1.0.95") (features (quote ("alloc"))) (optional #t) (kind 0)) (crate-dep (name "serde") (req "^1") (features (quote ("derive"))) (default-features #t) (kind 2)) (crate-dep (name "serde_test") (req "^1") (default-features #t) (kind 2)))) (hash "1465rj4y59qjvzxik0v2m0mp71q50m9xfigxqww7yhsw3hna61y3") (features (quote (("std") ("default" "std"))))))

(define-public crate-slab-0.4 (crate (name "slab") (vers "0.4.5") (deps (list (crate-dep (name "serde") (req "^1.0.95") (features (quote ("alloc"))) (optional #t) (kind 0)) (crate-dep (name "serde") (req "^1") (features (quote ("derive"))) (default-features #t) (kind 2)) (crate-dep (name "serde_test") (req "^1") (default-features #t) (kind 2)))) (hash "1ddg01hf8h4bpfm027h0snhb7jfcs1jzi497083y13q13vyr3vwx") (features (quote (("std") ("default" "std"))))))

(define-public crate-slab-0.4 (crate (name "slab") (vers "0.4.6") (deps (list (crate-dep (name "serde") (req "^1.0.95") (features (quote ("alloc"))) (optional #t) (kind 0)) (crate-dep (name "serde") (req "^1") (features (quote ("derive"))) (default-features #t) (kind 2)) (crate-dep (name "serde_test") (req "^1") (default-features #t) (kind 2)))) (hash "0cmvcy9ppsh3dz8mi6jljx7bxyknvgpas4aid2ayxk1vjpz3qw7b") (features (quote (("std") ("default" "std")))) (rust-version "1.31")))

(define-public crate-slab-0.4 (crate (name "slab") (vers "0.4.7") (deps (list (crate-dep (name "autocfg") (req "^1") (default-features #t) (kind 1)) (crate-dep (name "rustversion") (req "^1") (default-features #t) (kind 2)) (crate-dep (name "serde") (req "^1.0.95") (features (quote ("alloc"))) (optional #t) (kind 0)) (crate-dep (name "serde") (req "^1") (features (quote ("derive"))) (default-features #t) (kind 2)) (crate-dep (name "serde_test") (req "^1") (default-features #t) (kind 2)))) (hash "1vyw3rkdfdfkzfa1mh83s237sll8v5kazfwxma60bq4b59msf526") (features (quote (("std") ("default" "std")))) (rust-version "1.31")))

(define-public crate-slab-0.4 (crate (name "slab") (vers "0.4.8") (deps (list (crate-dep (name "autocfg") (req "^1") (default-features #t) (kind 1)) (crate-dep (name "rustversion") (req "^1") (default-features #t) (kind 2)) (crate-dep (name "serde") (req "^1.0.95") (features (quote ("alloc"))) (optional #t) (kind 0)) (crate-dep (name "serde") (req "^1") (features (quote ("derive"))) (default-features #t) (kind 2)) (crate-dep (name "serde_test") (req "^1") (default-features #t) (kind 2)))) (hash "0bgwxig8gkqp6gz8rvrpdj6qwa10karnsxwx7wsj5ay8kcf3aa35") (features (quote (("std") ("default" "std")))) (rust-version "1.31")))

(define-public crate-slab-0.4 (crate (name "slab") (vers "0.4.9") (deps (list (crate-dep (name "autocfg") (req "^1") (default-features #t) (kind 1)) (crate-dep (name "rustversion") (req "^1") (default-features #t) (kind 2)) (crate-dep (name "serde") (req "^1.0.95") (features (quote ("alloc"))) (optional #t) (kind 0)) (crate-dep (name "serde") (req "^1") (features (quote ("derive"))) (default-features #t) (kind 2)) (crate-dep (name "serde_test") (req "^1") (default-features #t) (kind 2)))) (hash "0rxvsgir0qw5lkycrqgb1cxsvxzjv9bmx73bk5y42svnzfba94lg") (features (quote (("std") ("default" "std")))) (rust-version "1.31")))

(define-public crate-slab-alloc-0.1 (crate (name "slab-alloc") (vers "0.1.0") (deps (list (crate-dep (name "interpolate_idents") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^0.2") (features (quote ("spin_no_std"))) (default-features #t) (kind 0)) (crate-dep (name "mmap-alloc") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "object-alloc") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "object-alloc-test") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "sysconf") (req "^0.3.0") (default-features #t) (kind 0)))) (hash "16l1w26wnqp541xxyiql0b0szhpkxzd5gcxaan844kch3f0v84x5") (features (quote (("use-stdlib-hashmap") ("std" "os") ("os") ("no-coloring") ("hashmap-no-resize") ("hashmap-no-coalesce") ("default" "std" "os") ("build-ignored-tests"))))))

(define-public crate-slab-alloc-0.1 (crate (name "slab-alloc") (vers "0.1.1") (deps (list (crate-dep (name "interpolate_idents") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^0.2") (features (quote ("spin_no_std"))) (default-features #t) (kind 0)) (crate-dep (name "mmap-alloc") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "object-alloc") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "object-alloc-test") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "sysconf") (req "^0.3.0") (default-features #t) (kind 0)))) (hash "1p46irv65sgb7hdpb4cflb89l2cm522f8qls6kpq1575qbb130jc") (features (quote (("use-stdlib-hashmap") ("std" "os") ("os") ("no-coloring") ("hashmap-no-resize") ("hashmap-no-coalesce") ("default" "std" "os") ("build-ignored-tests"))))))

(define-public crate-slab_32-0.1 (crate (name "slab_32") (vers "0.1.0") (hash "1y50ivky544b8h38k3aqvy43f20sm4s6y766y3wqbz80yd4q818w")))

(define-public crate-slab_32-0.1 (crate (name "slab_32") (vers "0.1.1") (hash "1vdlfhagpf0dcaz7vakr71ajgvqr9h56lcwqjq7m313kjr1bbpby")))

(define-public crate-slab_allocator-0.1 (crate (name "slab_allocator") (vers "0.1.0") (deps (list (crate-dep (name "spin") (req "^0.4.4") (default-features #t) (kind 0)))) (hash "0s0sz8n5skr8iirsc62a91w5n575d83b7v4b2vcpri2kr0slz2ik")))

(define-public crate-slab_allocator-0.2 (crate (name "slab_allocator") (vers "0.2.0") (deps (list (crate-dep (name "spin") (req "^0.4.6") (default-features #t) (kind 0)))) (hash "0l00cgrcv4kls0ngjfcag2hrd6qwqbw1v2jm3akkkbmmpk6g0005")))

(define-public crate-slab_allocator-0.3 (crate (name "slab_allocator") (vers "0.3.0") (deps (list (crate-dep (name "linked_list_allocator") (req "^0.4.3") (default-features #t) (kind 0)) (crate-dep (name "spin") (req "^0.4.6") (default-features #t) (kind 0)))) (hash "0l9kv4bhx99pxhsv2nf1q2nm4f8ngyp3rzcbycq09wbr8sqryazh")))

(define-public crate-slab_allocator-0.3 (crate (name "slab_allocator") (vers "0.3.1") (deps (list (crate-dep (name "linked_list_allocator") (req "^0.5") (default-features #t) (kind 0)) (crate-dep (name "spin") (req "^0.4") (default-features #t) (kind 0)))) (hash "15qrb3yxx766s5i2hhp6a7bm7pa2ayshwky2pnzq37aqk3mzi8nb")))

(define-public crate-slab_allocator-0.3 (crate (name "slab_allocator") (vers "0.3.2") (deps (list (crate-dep (name "linked_list_allocator") (req "^0.6.2") (default-features #t) (kind 0)) (crate-dep (name "spin") (req "^0.4.8") (default-features #t) (kind 0)))) (hash "11147ci4qccmkwc1gyl7lipykzbj7xfyqyx5aa8srlans1dswcx5")))

(define-public crate-slab_allocator-0.3 (crate (name "slab_allocator") (vers "0.3.3") (deps (list (crate-dep (name "linked_list_allocator") (req "^0.6.3") (default-features #t) (kind 0)) (crate-dep (name "spin") (req "^0.4.8") (default-features #t) (kind 0)))) (hash "0ka1q0wjnlbbqjkz1w12i6d5cmy63vs5pqa7bam1rv2w693r16nr")))

(define-public crate-slab_allocator-0.3 (crate (name "slab_allocator") (vers "0.3.4") (deps (list (crate-dep (name "linked_list_allocator") (req "^0.6.3") (default-features #t) (kind 0)) (crate-dep (name "spin") (req "^0.4.8") (default-features #t) (kind 0)))) (hash "0figgrqyf4j63k617yfyi9blzy6ls2rwmn8kzv59w9xsdbs75fmw") (yanked #t)))

(define-public crate-slab_allocator-0.3 (crate (name "slab_allocator") (vers "0.3.5") (deps (list (crate-dep (name "linked_list_allocator") (req "^0.6.3") (default-features #t) (kind 0)) (crate-dep (name "spin") (req "^0.4.9") (default-features #t) (kind 0)))) (hash "0yrmxpzw2yai4b3d2k890gv4r57a16hsipd0sybvaba995x3mcl4")))

(define-public crate-slab_allocator_rs-1 (crate (name "slab_allocator_rs") (vers "1.0.0") (deps (list (crate-dep (name "buddy_system_allocator") (req "^0.8.0") (default-features #t) (kind 0)) (crate-dep (name "spin") (req "^0.9.0") (default-features #t) (kind 0)))) (hash "1w7a812f2i6sxhk2vy055zvzlkb0cg0lndgq9w5pjy9irs690dap")))

(define-public crate-slab_allocator_rs-1 (crate (name "slab_allocator_rs") (vers "1.0.1") (deps (list (crate-dep (name "buddy_system_allocator") (req "^0.8.0") (default-features #t) (kind 0)) (crate-dep (name "spin") (req "^0.9.2") (default-features #t) (kind 0)))) (hash "07n2g4mkl4bwix97f0am4l3nb5nlmbicqs79iddv2r5smhhg1vkz")))

(define-public crate-slab_allocator_rs-1 (crate (name "slab_allocator_rs") (vers "1.0.2") (deps (list (crate-dep (name "buddy_system_allocator") (req "^0.9.0") (default-features #t) (kind 0)) (crate-dep (name "spin") (req "^0.9.4") (default-features #t) (kind 0)))) (hash "0s9nd54w75cqyi8y3xk1alxr514saddbsyqdzfbklimyigpfia1i")))

(define-public crate-slab_tree-0.1 (crate (name "slab_tree") (vers "0.1.0") (deps (list (crate-dep (name "snowflake") (req "^1.3.0") (default-features #t) (kind 0)))) (hash "0gh66wgywq6n820cilzg24ngjdqkgyjjiw19cj7mqvn96jagczbd")))

(define-public crate-slab_tree-0.1 (crate (name "slab_tree") (vers "0.1.1") (deps (list (crate-dep (name "snowflake") (req "^1.3.0") (default-features #t) (kind 0)))) (hash "03s1abv2vb9q63mvkfby7qg45k4fn4h49bzq1c6s5ap8fls5g1nq")))

(define-public crate-slab_tree-0.1 (crate (name "slab_tree") (vers "0.1.2") (deps (list (crate-dep (name "snowflake") (req "^1.3.0") (default-features #t) (kind 0)))) (hash "07hwmxxgfs9a9glmxq91bizmb1jzy5mq8407l0b12hwhk49yv6vz")))

(define-public crate-slab_tree-0.1 (crate (name "slab_tree") (vers "0.1.3") (deps (list (crate-dep (name "snowflake") (req "^1.3.0") (default-features #t) (kind 0)))) (hash "0pkgkjd04sk551j2lrhvf3wvz3mn3vp2b9x4xsc72gw2fs3pm2ib")))

(define-public crate-slab_tree-0.1 (crate (name "slab_tree") (vers "0.1.4") (deps (list (crate-dep (name "snowflake") (req "^1.3.0") (default-features #t) (kind 0)))) (hash "0sr1kffbrfyw6b357dzsrhbm8rimpmcynl30z3380hcahl3pzq6y")))

(define-public crate-slab_tree-0.2 (crate (name "slab_tree") (vers "0.2.0") (deps (list (crate-dep (name "snowflake") (req "^1.3.0") (default-features #t) (kind 0)))) (hash "1pykrdh3fhy78mvf7l07gbqiyy8rn7sqb0xwcq9zdkajjjx8ajw6")))

(define-public crate-slab_tree-0.3 (crate (name "slab_tree") (vers "0.3.0") (deps (list (crate-dep (name "snowflake") (req "^1.3.0") (default-features #t) (kind 0)))) (hash "049km2f7n5h73c31dghq82k542vlkawxjcdpkkbx9ghdr2d0kxld")))

(define-public crate-slab_tree-0.3 (crate (name "slab_tree") (vers "0.3.1") (deps (list (crate-dep (name "snowflake") (req "^1.3.0") (default-features #t) (kind 0)))) (hash "1zycdib53j2vks42ywvjx806m0mlls5grmj4cikh066981jhr4ra")))

(define-public crate-slab_tree-0.3 (crate (name "slab_tree") (vers "0.3.2") (deps (list (crate-dep (name "snowflake") (req "^1.3.0") (default-features #t) (kind 0)))) (hash "17z389gaywssmq8y5xaz5k469qgz7jbi2jh50fvwmdvpprdb9s7r")))

(define-public crate-slab_typesafe-0.1 (crate (name "slab_typesafe") (vers "0.1.0") (deps (list (crate-dep (name "slab") (req "^0.4") (default-features #t) (kind 0)))) (hash "0dnzyw7i2llf4nwsa9z7419dbklkhv7cczpy0ca0brdywyig106f")))

(define-public crate-slab_typesafe-0.1 (crate (name "slab_typesafe") (vers "0.1.1") (deps (list (crate-dep (name "slab") (req "^0.4") (default-features #t) (kind 0)))) (hash "1lidivzivjh9pskscjwmisq4crfk2pgipg9jhkv27x9d4krnxn5j")))

(define-public crate-slab_typesafe-0.1 (crate (name "slab_typesafe") (vers "0.1.2") (deps (list (crate-dep (name "slab") (req "^0.4") (default-features #t) (kind 0)))) (hash "14ya96qn66ybv69p0klksfldf6yvhhn7y8009vlf412p098v0vav")))

(define-public crate-slab_typesafe-0.1 (crate (name "slab_typesafe") (vers "0.1.3") (deps (list (crate-dep (name "slab") (req "^0.4") (default-features #t) (kind 0)))) (hash "1hf0l3xzf2g2i3fp4rlc1ivcp2f5klaa6iih510xmar64l3a5qan")))

(define-public crate-slaballoc-0.1 (crate (name "slaballoc") (vers "0.1.0") (hash "0iq68s4amaxb863j3fn0rjv7yh30i0g74d2fjq74rmqai494ff91")))

(define-public crate-slabbin-1 (crate (name "slabbin") (vers "1.0.0") (hash "15kddj33f1682v7z4nm39dm6yl1mw0gbnskhjhh8wrdl78szqh0h")))

(define-public crate-slabbin-1 (crate (name "slabbin") (vers "1.0.1") (hash "0wzj400wzrhxl4va1sh4f82grybwp0kjshw5xlklcqa4c040b0yx")))

(define-public crate-slabby-0.1 (crate (name "slabby") (vers "0.1.0") (hash "1ijvv8x7aqhkyvwzlhvr424j2a1ws0lw98z64xaaysf3d476lpcr") (yanked #t) (rust-version "1.56.1")))

(define-public crate-slabby-0.1 (crate (name "slabby") (vers "0.1.1") (hash "0wsfri8h8k1j3bqi79xd6lvgyd8y191izs2c0l5ipcr31fp76633") (yanked #t) (rust-version "1.56.1")))

(define-public crate-slabby-0.1 (crate (name "slabby") (vers "0.1.2") (hash "1l51ja88c3fdw5yppz8kxkbbi8x5l96rszf5jwyr203afix8ryp3") (yanked #t) (rust-version "1.56.1")))

(define-public crate-slabby-0.2 (crate (name "slabby") (vers "0.2.0") (deps (list (crate-dep (name "criterion") (req "^0.5.1") (features (quote ("html_reports"))) (default-features #t) (kind 2)))) (hash "1xd23i3hjsl6hd6wyj05yq17p7jj1qk83sy4b47b03ajm3pkwif3") (yanked #t) (rust-version "1.66")))

(define-public crate-slabby-0.3 (crate (name "slabby") (vers "0.3.0") (deps (list (crate-dep (name "criterion") (req "^0.5.1") (features (quote ("html_reports"))) (default-features #t) (kind 2)))) (hash "13f9bknxrxi8d0lrvm8wnhzzz9b2d7mish1rdbfbm8r3fg5nl10s") (rust-version "1.66")))

(define-public crate-slabby-0.3 (crate (name "slabby") (vers "0.3.1") (deps (list (crate-dep (name "criterion") (req "^0.5.1") (features (quote ("html_reports"))) (default-features #t) (kind 2)))) (hash "19jlk1grd60s8ap85ga8ckxhapz1p286abp5fp9vn5szj48w9h3g") (rust-version "1.66")))

(define-public crate-slabify-0.0.0 (crate (name "slabify") (vers "0.0.0") (deps (list (crate-dep (name "generational-arena") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)))) (hash "01z1234axrf7x6blxlimqyphsx9adx56qgmjk05x6h5n3cxhp8q0")))

(define-public crate-slabigator-0.1 (crate (name "slabigator") (vers "0.1.0") (hash "0dybzvilz35ij8s41qr7s3vc0pfhvm44kf2vfzxw5fc897sylxa1")))

(define-public crate-slabigator-0.1 (crate (name "slabigator") (vers "0.1.1") (hash "0jdgqzriv8j8vzdwr77p3p4z04bz3q4idy9isqwahvq4w1m23gvk")))

(define-public crate-slabigator-0.1 (crate (name "slabigator") (vers "0.1.3") (hash "1rvysy7n8ph622cyab3cdk5kjblb9nf02k0c4ri6g9p1lhabmmy7")))

(define-public crate-slabigator-0.1 (crate (name "slabigator") (vers "0.1.4") (hash "0pflasv59iwgf30pdab52hbnhbqzcfgs1bk1sp24b474935yzifv")))

(define-public crate-slabigator-0.1 (crate (name "slabigator") (vers "0.1.6") (deps (list (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 2)))) (hash "1yzf4jff4n6l09dgd4zb6ivdr77wdai03l0wab52zz8l0ghzb5zh") (features (quote (("unsafe"))))))

(define-public crate-slabigator-0.2 (crate (name "slabigator") (vers "0.2.0") (deps (list (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 2)))) (hash "0cf4sf4fprq95qn9dcws2sz1mfky8k6h3iklbll11lkivscv0sd4") (features (quote (("unsafe"))))))

(define-public crate-slabigator-0.1 (crate (name "slabigator") (vers "0.1.5") (deps (list (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 2)))) (hash "0vfgyk82wlbwplxw4j8h46ning8p7sfbv001l9rkgcr4kg4r4akh") (features (quote (("unsafe"))))))

(define-public crate-slabigator-0.9 (crate (name "slabigator") (vers "0.9.0") (deps (list (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 2)))) (hash "1mpdpb1sjhfvay9ygcayjam27xv46c9gzgr5h4fd8iayns6iinva") (features (quote (("unsafe") ("slot_usize") ("slot_u64") ("slot_u32"))))))

(define-public crate-slabigator-0.9 (crate (name "slabigator") (vers "0.9.1") (deps (list (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 2)))) (hash "09hn8rsh96avj1la9a2yrv0iw1qigfzblhbfpqg9czbynmli4l74") (features (quote (("slot_usize") ("slot_u64") ("slot_u32") ("releasefast"))))))

(define-public crate-slabigator-0.9 (crate (name "slabigator") (vers "0.9.2") (deps (list (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 2)))) (hash "1060n2l2qfnrlwwxqcj27fwc91rfq8fwamjk2q75gg5qns9lj4v5") (features (quote (("slot_usize") ("slot_u64") ("slot_u32") ("releasefast"))))))

(define-public crate-slablit-0.2 (crate (name "slablit") (vers "0.2.0") (deps (list (crate-dep (name "slab") (req "^0.4") (default-features #t) (kind 0)))) (hash "0icfs74w91rf21z32r5fwqd5dcvzb08dflppjw9c62drzrvkq41g")))

(define-public crate-slablit-0.2 (crate (name "slablit") (vers "0.2.1") (deps (list (crate-dep (name "slab") (req "^0.4") (default-features #t) (kind 0)))) (hash "07ngjqgh7xxga246ijll6vd7bgqnjbl2pib3i8sm003mbwi0470c")))

(define-public crate-slablit-0.3 (crate (name "slablit") (vers "0.3.0") (deps (list (crate-dep (name "slab") (req "^0.4") (default-features #t) (kind 0)))) (hash "0w803hx32jbvpwqfl7vs06qi9mxs6xhavxmjnanh3lpbx63fkr8b")))

(define-public crate-slabmalloc-0.0.1 (crate (name "slabmalloc") (vers "0.0.1") (deps (list (crate-dep (name "x86") (req "*") (default-features #t) (kind 0)))) (hash "0nprh0annwg22cb5frs0q94xryp6z521bqfbfmvn3bvri9shir7s")))

(define-public crate-slabmalloc-0.1 (crate (name "slabmalloc") (vers "0.1.0") (deps (list (crate-dep (name "libc") (req "*") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "x86") (req "*") (default-features #t) (kind 0)))) (hash "0irscm2jqawykydxmdb3ssj1hzid73xgkkyhl10jq4pfym5d5p8v")))

(define-public crate-slabmalloc-0.1 (crate (name "slabmalloc") (vers "0.1.1") (deps (list (crate-dep (name "libc") (req "*") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "x86") (req "*") (default-features #t) (kind 0)))) (hash "04fk23pma3bja6hk77394azn54dfb8dm1vzx9saclg78421w129v")))

(define-public crate-slabmalloc-0.2 (crate (name "slabmalloc") (vers "0.2.0") (deps (list (crate-dep (name "libc") (req "*") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "x86") (req "*") (default-features #t) (kind 0)))) (hash "1rizsk1scl3rpvpa94qg54i1hczbznffn91mhlsjlqiv1lqyy7bv")))

(define-public crate-slabmalloc-0.2 (crate (name "slabmalloc") (vers "0.2.1") (deps (list (crate-dep (name "libc") (req "*") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.3") (default-features #t) (kind 2)))) (hash "06dr0j25g3khhpac2impwfql94xzlcw5nvidaykfsjr4w7n6dlsl")))

(define-public crate-slabmalloc-0.3 (crate (name "slabmalloc") (vers "0.3.0") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "spin") (req "^0.4.8") (default-features #t) (kind 0)))) (hash "02mg906bm6wrk2r0fysvg9b5xjkbvyj85pmfgqgrrbkx8zyb5kpv")))

(define-public crate-slabmalloc-0.3 (crate (name "slabmalloc") (vers "0.3.1") (deps (list (crate-dep (name "env_logger") (req "^0.5.10") (default-features #t) (target "cfg(unix)") (kind 2)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (target "cfg(unix)") (kind 2)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.3") (default-features #t) (target "cfg(unix)") (kind 2)) (crate-dep (name "spin") (req "^0.4.8") (kind 0)))) (hash "1ix0xyr4cy6j4xq6ysfzi66ym7cckvz8f7ym3sk6my3sfhr8k4jh") (features (quote (("unstable" "spin/unstable") ("default" "unstable"))))))

(define-public crate-slabmalloc-0.3 (crate (name "slabmalloc") (vers "0.3.2") (deps (list (crate-dep (name "env_logger") (req "^0.5.10") (default-features #t) (target "cfg(unix)") (kind 2)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (target "cfg(unix)") (kind 2)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.3") (default-features #t) (target "cfg(unix)") (kind 2)) (crate-dep (name "spin") (req "^0.4.8") (kind 0)))) (hash "112wn2giza888cvh3m6xb5kbsr8pfdhn05j4dx6yhpqagavpl30c") (features (quote (("unstable" "spin/unstable") ("default" "unstable"))))))

(define-public crate-slabmalloc-0.3 (crate (name "slabmalloc") (vers "0.3.3") (deps (list (crate-dep (name "env_logger") (req "^0.5.10") (default-features #t) (target "cfg(unix)") (kind 2)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (target "cfg(unix)") (kind 2)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.3") (default-features #t) (target "cfg(unix)") (kind 2)) (crate-dep (name "spin") (req "^0.4.8") (kind 0)))) (hash "1w30pz57nqjz6s662v4apaypg5a78fxprxinvljhjqki5c5qrdbz") (features (quote (("unstable" "spin/unstable") ("default" "unstable"))))))

(define-public crate-slabmalloc-0.3 (crate (name "slabmalloc") (vers "0.3.4") (deps (list (crate-dep (name "env_logger") (req "^0.5.10") (default-features #t) (target "cfg(unix)") (kind 2)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (target "cfg(unix)") (kind 2)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.3") (default-features #t) (target "cfg(unix)") (kind 2)) (crate-dep (name "spin") (req "^0.5.2") (default-features #t) (kind 0)))) (hash "0b2krsi4v0rhlx739d9s19kqvq5aj0mfjdkrxh645ps5wkvwyr85") (features (quote (("unstable") ("default" "unstable"))))))

(define-public crate-slabmalloc-0.4 (crate (name "slabmalloc") (vers "0.4.0") (deps (list (crate-dep (name "env_logger") (req "^0.7") (default-features #t) (target "cfg(unix)") (kind 2)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (target "cfg(unix)") (kind 2)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.7") (default-features #t) (target "cfg(unix)") (kind 2)) (crate-dep (name "spin") (req "^0.5.2") (default-features #t) (kind 0)))) (hash "0jsmr22202qvd9vysvib3i8ds7drl59q33xan2lxligx2jghj194") (features (quote (("unstable") ("default" "unstable"))))))

(define-public crate-slabmalloc-0.5 (crate (name "slabmalloc") (vers "0.5.0") (deps (list (crate-dep (name "env_logger") (req "^0.7") (default-features #t) (target "cfg(unix)") (kind 2)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (target "cfg(unix)") (kind 2)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.7") (default-features #t) (target "cfg(unix)") (kind 2)) (crate-dep (name "spin") (req "^0.5.2") (default-features #t) (kind 0)))) (hash "0g52hhvpqlmv5fwvkk00ifz93a9d9bhcjqhzh4k21y0ih49grg5i") (features (quote (("unstable") ("default" "unstable")))) (yanked #t)))

(define-public crate-slabmalloc-0.6 (crate (name "slabmalloc") (vers "0.6.0") (deps (list (crate-dep (name "env_logger") (req "^0.7") (default-features #t) (target "cfg(unix)") (kind 2)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.7") (default-features #t) (target "cfg(unix)") (kind 2)) (crate-dep (name "spin") (req "^0.5.2") (default-features #t) (kind 0)))) (hash "0fvm80j0cymv94grxw2dv4g6798abn70jzigg76794hfydbq4lc4") (features (quote (("unstable") ("default" "unstable"))))))

(define-public crate-slabmalloc-0.7 (crate (name "slabmalloc") (vers "0.7.0") (deps (list (crate-dep (name "env_logger") (req "^0.7") (default-features #t) (target "cfg(unix)") (kind 2)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.7") (default-features #t) (target "cfg(unix)") (kind 2)) (crate-dep (name "spin") (req "^0.5.2") (default-features #t) (target "cfg(unix)") (kind 2)))) (hash "05a1q82icybc7vd5c0cx1jj2pacnhsh04q0a5r18jmblz4bdq7d5") (features (quote (("unstable") ("default" "unstable"))))))

(define-public crate-slabmalloc-0.8 (crate (name "slabmalloc") (vers "0.8.0") (deps (list (crate-dep (name "env_logger") (req "^0.7") (default-features #t) (target "cfg(unix)") (kind 2)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.7") (default-features #t) (target "cfg(unix)") (kind 2)) (crate-dep (name "spin") (req "^0.5.2") (default-features #t) (target "cfg(unix)") (kind 2)))) (hash "0969lmkyngx45mi346yhjvxnl3m1zixl247p23dl19d1rfbdikb2") (features (quote (("unstable") ("default" "unstable"))))))

(define-public crate-slabmalloc-0.9 (crate (name "slabmalloc") (vers "0.9.0") (deps (list (crate-dep (name "env_logger") (req "^0.7") (default-features #t) (target "cfg(unix)") (kind 2)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.7") (default-features #t) (target "cfg(unix)") (kind 2)) (crate-dep (name "spin") (req "^0.5.2") (default-features #t) (target "cfg(unix)") (kind 2)))) (hash "1pcgvxvswfyxp4h5rsx9lwbyp13q1saj4wqclp1rav0d5x6gjn9a") (features (quote (("unstable") ("default" "unstable"))))))

(define-public crate-slabmalloc-0.10 (crate (name "slabmalloc") (vers "0.10.0") (deps (list (crate-dep (name "env_logger") (req "^0.8") (default-features #t) (target "cfg(unix)") (kind 2)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8") (default-features #t) (target "cfg(unix)") (kind 2)) (crate-dep (name "spin") (req "^0.9.0") (default-features #t) (target "cfg(unix)") (kind 2)))) (hash "1w0h3r6x28zbkv5nsf1h96xzlyv7ayfrrwb7dcs8i0h5ql7wihs7") (features (quote (("unstable") ("default" "unstable"))))))

(define-public crate-slabmalloc-0.11 (crate (name "slabmalloc") (vers "0.11.0") (deps (list (crate-dep (name "env_logger") (req "^0.9") (default-features #t) (target "cfg(unix)") (kind 2)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8") (default-features #t) (target "cfg(unix)") (kind 2)) (crate-dep (name "spin") (req "^0.9.0") (default-features #t) (target "cfg(unix)") (kind 2)))) (hash "1dvkj2cac2yzwnh92yjh3zi1zigb6bqjnvy9jf9a2wwq3g379cjj") (features (quote (("unstable") ("default" "unstable"))))))

(define-public crate-slabmap-0.1 (crate (name "slabmap") (vers "0.1.0") (deps (list (crate-dep (name "criterion") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "proptest") (req "^0.10") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.7") (default-features #t) (kind 2)) (crate-dep (name "slab") (req "^0.4") (default-features #t) (kind 2)))) (hash "0yf2lhh3qgsh3mxb9174rqcbp66z0r85gswvplc9qim7rsbka5ds")))

(define-public crate-slabmap-0.1 (crate (name "slabmap") (vers "0.1.1") (deps (list (crate-dep (name "criterion") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "proptest") (req "^0.10") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.7") (default-features #t) (kind 2)) (crate-dep (name "slab") (req "^0.4") (default-features #t) (kind 2)))) (hash "1pajqx5w139nihv6l87ixd6jwqr03xlrs0hgh73hd2bywl7y69nm")))

(define-public crate-slabmap-0.2 (crate (name "slabmap") (vers "0.2.0") (deps (list (crate-dep (name "criterion") (req "^0.5.1") (default-features #t) (kind 2)) (crate-dep (name "proptest") (req "^1.2.0") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 2)) (crate-dep (name "slab") (req "^0.4.8") (default-features #t) (kind 2)) (crate-dep (name "test-strategy") (req "^0.3.1") (default-features #t) (kind 2)))) (hash "016ahrfvarhbwiql9lp9g5brvz9smgynngh7byzkpihj042idlnj")))

(define-public crate-slabmap-0.2 (crate (name "slabmap") (vers "0.2.1") (deps (list (crate-dep (name "criterion") (req "^0.5.1") (default-features #t) (kind 2)) (crate-dep (name "derive-ex") (req "^0.1.8") (default-features #t) (kind 0)) (crate-dep (name "proptest") (req "^1.4.0") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 2)) (crate-dep (name "slab") (req "^0.4.9") (default-features #t) (kind 2)) (crate-dep (name "test-strategy") (req "^0.3.1") (default-features #t) (kind 2)))) (hash "1196wcr1x3a7qqsk1x2w6ar4jzl0jkhhazp0lj0m7vlwf54klnsl")))

