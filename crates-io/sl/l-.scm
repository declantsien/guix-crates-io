(define-module (crates-io sl l-) #:use-module (crates-io))

(define-public crate-sll-rs-0.1 (crate (name "sll-rs") (vers "0.1.0") (deps (list (crate-dep (name "js-sys") (req "^0.3.63") (default-features #t) (target "cfg(target_arch = \"wasm32\")") (kind 0)) (crate-dep (name "rust-embed") (req "^6.7.0") (features (quote ("debug-embed"))) (default-features #t) (kind 0)) (crate-dep (name "toml") (req "^0.7.4") (default-features #t) (kind 0)))) (hash "1cklkbim7smj6xb7vjxza4hscj6nzw5p3xxf1fy3748n3cp4kh1p") (yanked #t)))

(define-public crate-sll-rs-0.1 (crate (name "sll-rs") (vers "0.1.1") (hash "0lkn44c59hy86aw1k93h2y12wrkrlfir0mad863r2j4050c8cnck") (yanked #t)))

