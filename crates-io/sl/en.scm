(define-module (crates-io sl en) #:use-module (crates-io))

(define-public crate-slen-0.1 (crate (name "slen") (vers "0.1.0") (deps (list (crate-dep (name "array-init") (req "^2.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "substring") (req "^1.4") (default-features #t) (kind 0)))) (hash "0m40jfc3l35z0jk03xk1j5730wgyvbmwdj0xp060ypdq14p7vcbr")))

(define-public crate-slen-0.1 (crate (name "slen") (vers "0.1.1") (deps (list (crate-dep (name "array-init") (req "^2.0") (default-features #t) (kind 0)) (crate-dep (name "substring") (req "^1.4") (default-features #t) (kind 0)))) (hash "1c4bg3y46f0g906xp943xi3514wcaiv7mi477a3r7fy0z3g89rkr")))

(define-public crate-slender-math-0.1 (crate (name "slender-math") (vers "0.1.0") (deps (list (crate-dep (name "bytemuck") (req "^1.13") (optional #t) (default-features #t) (kind 0)))) (hash "0pphz6x1wcpfmanr28769dr5pnnji4vz0iwvrgmh85d0r7w68xzj") (features (quote (("short_names") ("default" "bytemuck" "short_names" "color_fields") ("color_fields"))))))

(define-public crate-slender-math-0.1 (crate (name "slender-math") (vers "0.1.1") (deps (list (crate-dep (name "bytemuck") (req "^1.13") (optional #t) (default-features #t) (kind 0)))) (hash "1i3v9b1d7gb9az7fnyv3vb9yiam5r79a0dxn1x7rk7jyhyy1xw6s") (features (quote (("short_names") ("default" "bytemuck" "short_names" "color_fields") ("color_fields"))))))

