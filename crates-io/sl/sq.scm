(define-module (crates-io sl sq) #:use-module (crates-io))

(define-public crate-slsqp-0.1 (crate (name "slsqp") (vers "0.1.0-alpha.1") (deps (list (crate-dep (name "approx") (req "^0.5") (default-features #t) (kind 2)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "17m3kix13ngwd18ijkryxxdx5rhk4pfxy45njzyjfk4xhphjn33f")))

(define-public crate-slsqp-0.1 (crate (name "slsqp") (vers "0.1.0-alpha.2") (deps (list (crate-dep (name "approx") (req "^0.5") (default-features #t) (kind 2)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "1y89ra8xpls3dy4mr338wwaqqkh823ygv70j421dcc9vwc0gn0xa")))

(define-public crate-slsqp-0.1 (crate (name "slsqp") (vers "0.1.0-alpha.3") (deps (list (crate-dep (name "approx") (req "^0.5") (default-features #t) (kind 2)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "1yw4jg8l1w7ia02crbhlnrly0s4cqw9z1nfb7gahg83064ly8mhf")))

(define-public crate-slsqp-0.1 (crate (name "slsqp") (vers "0.1.0") (deps (list (crate-dep (name "approx") (req "^0.5") (default-features #t) (kind 2)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "17qjrg3lzc40j7zxz2rlylay8152klaa7ibkxk6z3v0wh8r2vgc8")))

(define-public crate-slsqp-0.1 (crate (name "slsqp") (vers "0.1.1") (deps (list (crate-dep (name "approx") (req "^0.5") (default-features #t) (kind 2)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "0n6ll93a4vzfz44swaw7cbkp50913rnwgsgy22ravx7qyi679l02")))

