(define-module (crates-io sl _c) #:use-module (crates-io))

(define-public crate-sl_cli-1 (crate (name "sl_cli") (vers "1.0.0") (deps (list (crate-dep (name "clap") (req "^3.0.0-beta.1") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2.71") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.19") (default-features #t) (kind 0)))) (hash "0lva9qrb2mjb4klnini4c4spmh390qi2piywzyjj83i8w6v9pkna")))

