(define-module (crates-io sl -u) #:use-module (crates-io))

(define-public crate-sl-up-0.1 (crate (name "sl-up") (vers "0.1.0") (deps (list (crate-dep (name "ansi-parser") (req "^0.8.0") (default-features #t) (kind 0)) (crate-dep (name "crossterm") (req "^0.27.0") (default-features #t) (kind 0)) (crate-dep (name "enum_dispatch") (req "^0.3.12") (default-features #t) (kind 0)))) (hash "1g3i6xd1llijj14hvdk7cr7smashalqdyz83hbjqyd0inrdcfakr")))

(define-public crate-sl-up-0.1 (crate (name "sl-up") (vers "0.1.1") (deps (list (crate-dep (name "ansi-parser") (req "^0.8.0") (default-features #t) (kind 0)) (crate-dep (name "crossterm") (req "^0.27.0") (default-features #t) (kind 0)) (crate-dep (name "enum_dispatch") (req "^0.3.12") (default-features #t) (kind 0)))) (hash "1ifz8dsh8vjpyavmw37b5x6p4hnq28lndipg8scccf6ngrh301by")))

