(define-module (crates-io sl av) #:use-module (crates-io))

(define-public crate-slav-0.0.0 (crate (name "slav") (vers "0.0.0") (hash "1xqkihnrn188k50jj3x62hwnb1jp63n0ih97hrlb46g4nyzca81f")))

(define-public crate-slave-pool-0.1 (crate (name "slave-pool") (vers "0.1.0") (deps (list (crate-dep (name "crossbeam-channel") (req "^0.4") (kind 0)) (crate-dep (name "parking_lot") (req "^0.10") (kind 0)))) (hash "1vdiiamx8nialqxbpzajl2254xgf0gwk197h50mq7ab6fmg57jnf") (yanked #t)))

(define-public crate-slave-pool-0.1 (crate (name "slave-pool") (vers "0.1.1") (deps (list (crate-dep (name "crossbeam-channel") (req "^0.4") (kind 0)) (crate-dep (name "parking_lot") (req "^0.10") (kind 0)))) (hash "0ilmd0l34vd77wr56wbccjscmij92la8kz05rpbqdqp2avrvjxhw") (yanked #t)))

(define-public crate-slave-pool-0.1 (crate (name "slave-pool") (vers "0.1.2") (deps (list (crate-dep (name "crossbeam-channel") (req "^0.4") (kind 0)))) (hash "1f371ishb8bzlkfh1b0pwaxndq1ydimj9c00k3mviabbjszji3is") (yanked #t)))

(define-public crate-slave-pool-0.2 (crate (name "slave-pool") (vers "0.2.0") (deps (list (crate-dep (name "crossbeam-channel") (req "^0.4") (kind 0)))) (hash "1fksbq8lp1dcb3g9ij2mydgfz0xwcnn3jybpdnd92z89n3s9v8ny") (yanked #t)))

(define-public crate-slave-pool-0.2 (crate (name "slave-pool") (vers "0.2.1") (deps (list (crate-dep (name "crossbeam-channel") (req "^0.4") (kind 0)))) (hash "18mcy150k3gvs0kfi13798wdl2j802cy7509lsfvz6np7zjm3plf")))

(define-public crate-slave-pool-0.2 (crate (name "slave-pool") (vers "0.2.2") (deps (list (crate-dep (name "crossbeam-channel") (req "^0.4") (kind 0)))) (hash "0jdvl44mc3bx2mrmnbhlz3x35jcfnsfqb0k80jr6p8k6xqd2an2c")))

(define-public crate-slave-pool-0.2 (crate (name "slave-pool") (vers "0.2.3") (deps (list (crate-dep (name "crossbeam-channel") (req "^0.5") (features (quote ("std"))) (kind 0)))) (hash "012g1k5jv18i4hl9lnvffkmq555krjyxx6lqyvnfsisdizi0f451")))

