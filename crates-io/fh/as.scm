(define-module (crates-io fh as) #:use-module (crates-io))

(define-public crate-fhash-0.1 (crate (name "fhash") (vers "0.1.0") (deps (list (crate-dep (name "ahash") (req "^0.8.3") (default-features #t) (kind 2)) (crate-dep (name "frand") (req "^0.7.0") (default-features #t) (kind 0)) (crate-dep (name "hashbrown") (req "^0.13.2") (kind 0)) (crate-dep (name "image") (req "^0.24.6") (features (quote ("png"))) (kind 2)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 2)))) (hash "155n5wmmvdqdj723vx3wwmjj3qaip4khw6czvzl915wc2vvw5xdy")))

(define-public crate-fhash-0.1 (crate (name "fhash") (vers "0.1.1") (deps (list (crate-dep (name "ahash") (req "^0.8.3") (default-features #t) (kind 2)) (crate-dep (name "frand") (req "^0.7.0") (default-features #t) (kind 0)) (crate-dep (name "hashbrown") (req "^0.13.2") (kind 0)) (crate-dep (name "image") (req "^0.24.6") (features (quote ("png"))) (kind 2)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 2)))) (hash "19wh3mj7l5bcpnb51l4gfmx0r6zbmg5cahsh5clf57nnv10ha1xs")))

(define-public crate-fhash-0.1 (crate (name "fhash") (vers "0.1.2") (deps (list (crate-dep (name "ahash") (req "^0.8.3") (default-features #t) (kind 2)) (crate-dep (name "frand") (req "^0.7.1") (default-features #t) (kind 0)) (crate-dep (name "hashbrown") (req "^0.13.2") (kind 0)) (crate-dep (name "image") (req "^0.24.6") (features (quote ("png"))) (kind 2)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 2)))) (hash "0fphz0flxznig9pxi96iif116l16q96d4ba6bhjxcsyd4qdd9iyd")))

(define-public crate-fhash-0.2 (crate (name "fhash") (vers "0.2.0") (deps (list (crate-dep (name "ahash") (req "^0.8.7") (default-features #t) (kind 2)) (crate-dep (name "frand") (req "^0.8.1") (default-features #t) (kind 0)) (crate-dep (name "hashbrown") (req "^0.14.3") (default-features #t) (kind 2)))) (hash "12c4s0x33c89qyw07jfpd73jl31dxikp24mjy3khy427dhd20pfp")))

(define-public crate-fhash-0.2 (crate (name "fhash") (vers "0.2.1") (deps (list (crate-dep (name "ahash") (req "^0.8.7") (default-features #t) (kind 2)) (crate-dep (name "frand") (req "^0.8.1") (default-features #t) (kind 0)) (crate-dep (name "hashbrown") (req "^0.14.3") (default-features #t) (kind 2)))) (hash "1q9libkrn4jld79awm9bvgwyildiiyibadi9ah0rd9w3hcf8hz21")))

(define-public crate-fhash-0.7 (crate (name "fhash") (vers "0.7.0") (deps (list (crate-dep (name "ahash") (req "^0.8.7") (default-features #t) (kind 2)) (crate-dep (name "frand") (req "^0.8.1") (default-features #t) (kind 2)) (crate-dep (name "hashbrown") (req "^0.14.3") (default-features #t) (kind 2)))) (hash "1jnb6li40mm0lhyj4i36r3ql4f2jp5kzqgzrklnvv5dxa31ibahl") (features (quote (("std") ("default" "std"))))))

(define-public crate-fhash-0.7 (crate (name "fhash") (vers "0.7.1") (deps (list (crate-dep (name "ahash") (req "^0.8.7") (default-features #t) (kind 2)) (crate-dep (name "frand") (req "^0.8.1") (default-features #t) (kind 2)) (crate-dep (name "hashbrown") (req "^0.14.3") (default-features #t) (kind 2)))) (hash "1xz33w61bwq67qay693k3s9irfhybbinh37hv430gh1r014jn4qz") (features (quote (("std") ("default" "std"))))))

