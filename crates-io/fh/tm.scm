(define-module (crates-io fh tm) #:use-module (crates-io))

(define-public crate-fhtml-0.1 (crate (name "fhtml") (vers "0.1.0") (deps (list (crate-dep (name "const_format") (req "^0.2") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "fhtml-macros") (req "^0.1") (default-features #t) (kind 0)))) (hash "01j8nachvc978b65qpnx3x28bbmbsfll9x2jx7lykhrpl463h69h") (v 2) (features2 (quote (("const_format" "dep:const_format"))))))

(define-public crate-fhtml-0.2 (crate (name "fhtml") (vers "0.2.0") (deps (list (crate-dep (name "const_format") (req "^0.2") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "fhtml-macros") (req "^0.2") (default-features #t) (kind 0)))) (hash "01pkjrgcjcyg2qi8is7fc1kjydhzldias85mqs5h5kg64ac6mkqk") (v 2) (features2 (quote (("const_format" "dep:const_format"))))))

(define-public crate-fhtml-0.2 (crate (name "fhtml") (vers "0.2.1") (deps (list (crate-dep (name "const_format") (req "^0.2") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "fhtml-macros") (req "^0.2") (default-features #t) (kind 0)))) (hash "0wrifss34xiiba7aggdim5b8ggm6id3nbvx50p54xsjfnpav1m8b") (features (quote (("default")))) (v 2) (features2 (quote (("const_format" "dep:const_format"))))))

(define-public crate-fhtml-0.3 (crate (name "fhtml") (vers "0.3.0") (deps (list (crate-dep (name "const_format") (req "^0.2") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "fhtml-macros") (req "^0.3") (default-features #t) (kind 0)))) (hash "002my9v2wr0jn4jbnvxfymqvgmkk99lciqwhnvcr4ib9vwkz6ww7") (features (quote (("default")))) (v 2) (features2 (quote (("const-format" "dep:const_format"))))))

(define-public crate-fhtml-0.4 (crate (name "fhtml") (vers "0.4.0") (deps (list (crate-dep (name "const_format") (req "^0.2") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "fhtml-macros") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "lazy_format") (req "^2.0.3") (optional #t) (default-features #t) (kind 0)))) (hash "0h2py6my0q4ksb4c6fz0ndw8jx33asl6bfc741shxnbysdafqkzz") (features (quote (("default")))) (v 2) (features2 (quote (("const" "dep:const_format"))))))

(define-public crate-fhtml-0.4 (crate (name "fhtml") (vers "0.4.1") (deps (list (crate-dep (name "const_format") (req "^0.2") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "fhtml-macros") (req "^0.4") (default-features #t) (kind 0)))) (hash "00y10v4spw940bddf4f28b6cdxiaag603g3zh8hbcgs0p8iibj9q") (features (quote (("default")))) (v 2) (features2 (quote (("const" "dep:const_format"))))))

(define-public crate-fhtml-macros-0.1 (crate (name "fhtml-macros") (vers "0.1.0") (deps (list (crate-dep (name "proc-macro2") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "1na0xvlpxp5167mz14gifvm6jrargwzyh35m75qhcyb651j5vlmv")))

(define-public crate-fhtml-macros-0.2 (crate (name "fhtml-macros") (vers "0.2.0") (deps (list (crate-dep (name "proc-macro2") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "1sl646m20sxsfsw2lblmd98nix0lhv9j7jz1g5fb8a0g38p0dmgx")))

(define-public crate-fhtml-macros-0.2 (crate (name "fhtml-macros") (vers "0.2.1") (deps (list (crate-dep (name "proc-macro2") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "1556nrfi509agj5wysb6qxwafxzk9bwxnyryy4r6pv0g4gq7s5l6")))

(define-public crate-fhtml-macros-0.3 (crate (name "fhtml-macros") (vers "0.3.0") (deps (list (crate-dep (name "proc-macro2") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "0scjfaf0lm8511aad04bi8j55nh1p6sjkhxiavxdiymb1dca2yav")))

(define-public crate-fhtml-macros-0.4 (crate (name "fhtml-macros") (vers "0.4.0") (deps (list (crate-dep (name "proc-macro2") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "0ab6p214a2wfrph4hl1wn2j26vmb41sddsy7m3icgpk3d0m9w6vi")))

(define-public crate-fhtml-macros-0.4 (crate (name "fhtml-macros") (vers "0.4.1") (deps (list (crate-dep (name "proc-macro2") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "001yafnj1j4pqrlq96ycbi6p5fyadlxk5rr91zpbj0nq8wnw8g6z")))

