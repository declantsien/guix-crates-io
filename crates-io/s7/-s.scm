(define-module (crates-io s7 -s) #:use-module (crates-io))

(define-public crate-s7-sys-0.1 (crate (name "s7-sys") (vers "0.1.0") (deps (list (crate-dep (name "bindgen") (req "^0.65.1") (default-features #t) (kind 1)) (crate-dep (name "cc") (req "^1.0.79") (default-features #t) (kind 1)))) (hash "0c6sy4g7gpxmnnpnqdl00k1mllff7b0czz3hrwzpp0050kjrynd1") (yanked #t)))

(define-public crate-s7-sys-0.1 (crate (name "s7-sys") (vers "0.1.1") (deps (list (crate-dep (name "bindgen") (req "^0.65.1") (default-features #t) (kind 1)) (crate-dep (name "cc") (req "^1.0.79") (default-features #t) (kind 1)))) (hash "0wl4fmdxb8z4xmb9kq7hfg34dppryi7ca1847wr5rag6s8ihdhcw")))

(define-public crate-s7-sys-0.1 (crate (name "s7-sys") (vers "0.1.2") (deps (list (crate-dep (name "bindgen") (req "^0.65.1") (default-features #t) (kind 1)) (crate-dep (name "cc") (req "^1.0.79") (default-features #t) (kind 1)))) (hash "0swza9armnn0wai0jhjksv8n3w60ky8q7p9z2m6kb1lg1a2a30ih")))

