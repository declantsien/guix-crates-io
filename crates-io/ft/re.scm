(define-module (crates-io ft re) #:use-module (crates-io))

(define-public crate-ftree-0.1 (crate (name "ftree") (vers "0.1.0") (deps (list (crate-dep (name "serde") (req "^1.0.151") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)))) (hash "10hb0ncbwhpkkwch5rcs3jvpyv17cw5h8dwbn4l3b8izix569gma") (v 2) (features2 (quote (("serde" "dep:serde"))))))

(define-public crate-ftree-0.1 (crate (name "ftree") (vers "0.1.1") (deps (list (crate-dep (name "serde") (req "^1.0.151") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)))) (hash "1mjg4jrlprpidlv63h0afiv7ah5904azgwkahs6v27yfhw8s5hva") (v 2) (features2 (quote (("serde" "dep:serde"))))))

(define-public crate-ftree-1 (crate (name "ftree") (vers "1.0.0") (deps (list (crate-dep (name "serde") (req "^1.0.151") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)))) (hash "04yw3m8v8yvbqbxablkwryi1k4pfcvc9wxsy1awbgcnww2symm6a") (v 2) (features2 (quote (("serde" "dep:serde"))))))

(define-public crate-ftree-1 (crate (name "ftree") (vers "1.0.1") (deps (list (crate-dep (name "ntest") (req "^0.9") (default-features #t) (kind 2)) (crate-dep (name "serde") (req "^1.0.151") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)))) (hash "07ky21mi6jvnkc99mfzj6awj0ykw7lvp4clgcjmiq8sq2x7xdqvc") (v 2) (features2 (quote (("serde" "dep:serde"))))))

(define-public crate-ftree-1 (crate (name "ftree") (vers "1.1.0") (deps (list (crate-dep (name "ntest") (req "^0.9") (default-features #t) (kind 2)) (crate-dep (name "serde") (req "^1.0.151") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)))) (hash "1xxbw5jf33mgwr5qmsh7zil1z9hj9f49z7lqd6rc0rwy13crs7r4") (v 2) (features2 (quote (("serde" "dep:serde"))))))

