(define-module (crates-io ft ld) #:use-module (crates-io))

(define-public crate-ftldat-0.1 (crate (name "ftldat") (vers "0.1.0") (deps (list (crate-dep (name "byteorder") (req "^1.4.3") (default-features #t) (kind 0)) (crate-dep (name "memmap2") (req "^0.5.10") (default-features #t) (kind 0)) (crate-dep (name "tempfile") (req "^3.3.0") (default-features #t) (kind 2)) (crate-dep (name "thiserror") (req "^1.0.32") (default-features #t) (kind 0)))) (hash "1dbcn5cbwg0ydsjymb1fmgz1k2sdl2wz0y8xz9lc7s4hfpssvqji")))

(define-public crate-ftldat-0.1 (crate (name "ftldat") (vers "0.1.1") (deps (list (crate-dep (name "byteorder") (req "^1.4.3") (default-features #t) (kind 0)) (crate-dep (name "memmap2") (req "^0.5.10") (default-features #t) (kind 0)) (crate-dep (name "tempfile") (req "^3.3.0") (default-features #t) (kind 2)) (crate-dep (name "thiserror") (req "^1.0.32") (default-features #t) (kind 0)))) (hash "0i11cm6121d8n6nh6c7jggbw0j7p3phnw1lpqcr8205h57h3g14l")))

