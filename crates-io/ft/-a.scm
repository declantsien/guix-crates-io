(define-module (crates-io ft -a) #:use-module (crates-io))

(define-public crate-ft-api-0.1 (crate (name "ft-api") (vers "0.1.0") (deps (list (crate-dep (name "chrono") (req "^0.4") (features (quote ("serde"))) (default-features #t) (kind 0)) (crate-dep (name "maplit") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "realm-client") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)))) (hash "10smicz59wb37k4av8j38mfkjdk9hqvj2r14jsym7752r6jc4pvw")))

