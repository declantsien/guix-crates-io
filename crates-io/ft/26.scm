(define-module (crates-io ft #{26}#) #:use-module (crates-io))

(define-public crate-ft260-0.1 (crate (name "ft260") (vers "0.1.0") (deps (list (crate-dep (name "hidapi") (req "^2.6.0") (default-features #t) (kind 0)) (crate-dep (name "rusb") (req "^0.9.1") (default-features #t) (kind 0)))) (hash "063h9np5b7cnynq4nj3ph6w7ma46y00hv4fg0iislwhmhqhli7yd")))

(define-public crate-ft260hid-0.1 (crate (name "ft260hid") (vers "0.1.0") (deps (list (crate-dep (name "bitflags") (req "^2.5.0") (default-features #t) (kind 0)) (crate-dep (name "hidapi") (req "^2.6.1") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)) (crate-dep (name "serial_test") (req "^3.1.1") (default-features #t) (kind 0)))) (hash "0pzi0ddi584a5rrcxpj84y404447k0z8pc8v6lqil7xik6jzvsdb")))

