(define-module (crates-io ft -d) #:use-module (crates-io))

(define-public crate-ft-derive-0.1 (crate (name "ft-derive") (vers "0.1.0") (deps (list (crate-dep (name "quote") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1") (features (quote ("full" "extra-traits"))) (default-features #t) (kind 0)))) (hash "1r03w3mlbg8g4wb2f31zh4xm8fini7scf3vhlgiyh7slihfs9k1r")))

