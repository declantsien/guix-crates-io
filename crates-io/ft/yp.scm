(define-module (crates-io ft yp) #:use-module (crates-io))

(define-public crate-ftype-0.0.0 (crate (name "ftype") (vers "0.0.0") (deps (list (crate-dep (name "clap") (req "^3.0.0-beta.2") (default-features #t) (kind 0)) (crate-dep (name "criterion") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "proptest") (req "^1.0") (default-features #t) (kind 2)) (crate-dep (name "tempfile") (req "^3.2") (default-features #t) (kind 2)))) (hash "0qsmixhn2sqi1ghv7cz08xq30l82g33paj0x5538wjj347zl7z0m")))

