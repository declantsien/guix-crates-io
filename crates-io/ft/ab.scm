(define-module (crates-io ft ab) #:use-module (crates-io))

(define-public crate-ftab-dump-1 (crate (name "ftab-dump") (vers "1.0.0") (deps (list (crate-dep (name "byteorder") (req "^1.3.2") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^2.33.0") (default-features #t) (kind 0)))) (hash "0iqw24ym5ip4gbfkv1scyckcmvv18zyy6ybcq7sbg0c8xykg7gbb")))

(define-public crate-ftab-dump-1 (crate (name "ftab-dump") (vers "1.0.1") (deps (list (crate-dep (name "byteorder") (req "^1.4.3") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^2.33.3") (default-features #t) (kind 0)))) (hash "1yir3cwgi2884wzbh1aag467yapdjki4ds6qnvv9aawkzidy79mb")))

(define-public crate-ftab-dump-1 (crate (name "ftab-dump") (vers "1.0.2") (deps (list (crate-dep (name "byteorder") (req "^1.4.3") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^2.33.3") (default-features #t) (kind 0)))) (hash "1kamad3h9ang1v53s7fd8pcsjs6s75v5z3v4ni93v449hyv7a67j")))

(define-public crate-ftab-dump-1 (crate (name "ftab-dump") (vers "1.0.3") (deps (list (crate-dep (name "byteorder") (req "^1.4.3") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^2.33.3") (default-features #t) (kind 0)))) (hash "1did2zm5344w8qgn41x7sgvqvyhsg05gln64bmi380bn55m94ng0")))

(define-public crate-ftabutil-0.1 (crate (name "ftabutil") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^4.0") (features (quote ("cargo"))) (default-features #t) (kind 0)) (crate-dep (name "hex") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "simple_logger") (req "^4.0") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "toml") (req "^0.5") (default-features #t) (kind 0)))) (hash "08hjf6q1b749whyb5108h06fmy9j0d63vrz8y5704fp6wl0rq4lp")))

(define-public crate-ftabutil-0.2 (crate (name "ftabutil") (vers "0.2.0") (deps (list (crate-dep (name "clap") (req "^4.0") (features (quote ("cargo"))) (default-features #t) (kind 0)) (crate-dep (name "dialoguer") (req "^0.10") (kind 0)) (crate-dep (name "hex") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "simple_logger") (req "^4.0") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "toml") (req "^0.5") (default-features #t) (kind 0)))) (hash "0h1vlyf2zxs1yz75lx629k5rfcb4b4mh62av3mxncck7348pp4ad")))

