(define-module (crates-io ft ki) #:use-module (crates-io))

(define-public crate-ftkit-0.1 (crate (name "ftkit") (vers "0.1.4") (deps (list (crate-dep (name "getrandom") (req "^0.2") (default-features #t) (kind 0)))) (hash "1qskdiidki7yrvy9mwsj4rc0brkiyy8cddbdcpgja2xwwysvmyv8")))

(define-public crate-ftkit-0.1 (crate (name "ftkit") (vers "0.1.5") (deps (list (crate-dep (name "getrandom") (req "^0.2") (default-features #t) (kind 0)))) (hash "1rj2cqr1r1cy0yqd4kvc1qb8vj7f58ggai524qdl49j33y99dql5")))

(define-public crate-ftkit-0.1 (crate (name "ftkit") (vers "0.1.7") (deps (list (crate-dep (name "getrandom") (req "^0.2") (default-features #t) (kind 0)))) (hash "03l0l678pmi92m93w2dajhxzqpqnip18q42277czcb859hm7j95y")))

(define-public crate-ftkit-0.1 (crate (name "ftkit") (vers "0.1.8") (deps (list (crate-dep (name "getrandom") (req "^0.2") (default-features #t) (kind 0)))) (hash "0nn1qvn71sd286x6aif1bz9dc6axmc9942h88gp2jawmzhrzpqkq")))

(define-public crate-ftkit-0.1 (crate (name "ftkit") (vers "0.1.9") (deps (list (crate-dep (name "getrandom") (req "^0.2") (default-features #t) (kind 0)))) (hash "1pqd585jccfxjs2nkzcp8zi9703x3vg0m7ai6i8brzykwdz9mz09")))

(define-public crate-ftkit-0.1 (crate (name "ftkit") (vers "0.1.10") (hash "1ybzsb81b7mv5mx4abzmw1x0zh6r5ag0sf1qzmmqqi489pg0vp3q")))

(define-public crate-ftkit-0.1 (crate (name "ftkit") (vers "0.1.11") (hash "0dprg6g9wrqzjvzk39gkly8yp08dcn1q58iv7hfk8achx7f1r6yi")))

(define-public crate-ftkit-0.1 (crate (name "ftkit") (vers "0.1.12") (hash "0647w19bgik8c52dk5d9s4y4lgwhysx7b0gm5klyllwcp0rpyaql")))

