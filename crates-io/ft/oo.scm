(define-module (crates-io ft oo) #:use-module (crates-io))

(define-public crate-ftools-0.1 (crate (name "ftools") (vers "0.1.0") (hash "13lx6ah5qz9r2askwg7f94lxm6zd5qw033ihlqp2ki4dlkp5hzvr")))

(define-public crate-ftools-0.1 (crate (name "ftools") (vers "0.1.1") (deps (list (crate-dep (name "futures") (req "0.3.*") (default-features #t) (kind 2)) (crate-dep (name "tokio") (req "1.4.*") (features (quote ("full"))) (default-features #t) (kind 2)))) (hash "1mx1pzlngh3j74r69pddjbiqpv17wwd6f4c5dj6amp8f64bpajby")))

