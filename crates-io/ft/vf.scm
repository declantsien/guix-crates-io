(define-module (crates-io ft vf) #:use-module (crates-io))

(define-public crate-ftvf-0.5 (crate (name "ftvf") (vers "0.5.0") (hash "1mvifnqbjzzmf9ql85dxsxdfk6mrlqdsvbp11lvpmfn4ja3b0813")))

(define-public crate-ftvf-0.6 (crate (name "ftvf") (vers "0.6.0") (hash "1kmz615a315hzaj3bb35wjm9vlmqlxa883sggbzbchp3g9i735bf") (features (quote (("no_std") ("default"))))))

