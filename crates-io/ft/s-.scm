(define-module (crates-io ft s-) #:use-module (crates-io))

(define-public crate-fts-sys-0.1 (crate (name "fts-sys") (vers "0.1.0") (deps (list (crate-dep (name "bindgen") (req "^0.57") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "12igc53wqvkzf4nwifyhysafd3qbjgc8rakajlggdmjq9vacjham") (links "c")))

(define-public crate-fts-sys-0.2 (crate (name "fts-sys") (vers "0.2.0") (deps (list (crate-dep (name "bindgen") (req "^0.58") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (target "cfg(unix)") (kind 0)))) (hash "09d5lf0scwsg4w4bq67vqfhskq1mgiiihqdwlrv4klmc9digpdzr") (links "c")))

(define-public crate-fts-sys-0.2 (crate (name "fts-sys") (vers "0.2.1") (deps (list (crate-dep (name "bindgen") (req "^0.59") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (target "cfg(unix)") (kind 0)))) (hash "1jfaclykcqf3cfillbdyv5142lal5w87pbhzkbj0w9qfb3qwj7nk") (links "c")))

(define-public crate-fts-sys-0.2 (crate (name "fts-sys") (vers "0.2.2") (deps (list (crate-dep (name "bindgen") (req "^0.60") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (target "cfg(unix)") (kind 0)))) (hash "19azvzq0pmsb1d337k68rrzpg5y32dm5h1vh1qadrbnzsp6ssnfx") (links "c")))

(define-public crate-fts-sys-0.2 (crate (name "fts-sys") (vers "0.2.3") (deps (list (crate-dep (name "bindgen") (req "^0.62") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (target "cfg(unix)") (kind 0)))) (hash "00vajscb7k4j3jb57f13ai0nscq5rgsfnwl20h5jqx0h7lrrig9j") (links "c")))

(define-public crate-fts-sys-0.2 (crate (name "fts-sys") (vers "0.2.4") (deps (list (crate-dep (name "bindgen") (req "^0.63") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (target "cfg(unix)") (kind 0)))) (hash "1lcxyicvk2g98k9rhb0kiw0sfh2grx1jd8acgg420krl3sic0rls") (links "c")))

(define-public crate-fts-sys-0.2 (crate (name "fts-sys") (vers "0.2.5") (deps (list (crate-dep (name "bindgen") (req "^0.65") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (target "cfg(unix)") (kind 0)))) (hash "0wig9a0hm4ib9588rvrlm2iq1p83g69b34b0d2f324121jjf5r73") (links "c")))

(define-public crate-fts-sys-0.2 (crate (name "fts-sys") (vers "0.2.6") (deps (list (crate-dep (name "bindgen") (req "^0.65") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (target "cfg(unix)") (kind 0)))) (hash "0nbkqmrn9p94l21v80b135zdkcb7zm70vwp66vi6da3ivdb4gxay") (links "c")))

(define-public crate-fts-sys-0.2 (crate (name "fts-sys") (vers "0.2.7") (deps (list (crate-dep (name "bindgen") (req "^0.68") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (target "cfg(unix)") (kind 0)))) (hash "1zdizq3ccb6jrihn19p4y4ygbl7zhdsjqhfb79zwxx2fb9prn43s") (links "c")))

(define-public crate-fts-sys-0.2 (crate (name "fts-sys") (vers "0.2.8") (deps (list (crate-dep (name "bindgen") (req "^0.69") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (target "cfg(unix)") (kind 0)))) (hash "18np47bdqrjj9qskg9bvcbwd84cidvfdqcsc7p8k49jgcympkx5r") (links "c")))

(define-public crate-fts-sys-0.2 (crate (name "fts-sys") (vers "0.2.9") (deps (list (crate-dep (name "bindgen") (req "^0.69") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (target "cfg(unix)") (kind 0)))) (hash "1fry2lbb8vdszh7qjj2fbisvrw15injzkdmg4qzpj69xb5gls62f") (links "c")))

