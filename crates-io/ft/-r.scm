(define-module (crates-io ft -r) #:use-module (crates-io))

(define-public crate-ft-rs-0.0.1 (crate (name "ft-rs") (vers "0.0.1") (hash "0fjjxgr8ib9llf33n7bjkq611cxrn1fpd2l4lxrfliyy9qr2a4q2") (rust-version "1.56")))

(define-public crate-ft-rs-0.0.2 (crate (name "ft-rs") (vers "0.0.2") (deps (list (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)))) (hash "0i5qmpjcbc3slww43zsr9y5yxqln07gi2fwlz9zn0b5xgw7w9l63") (features (quote (("default")))) (rust-version "1.56")))

(define-public crate-ft-rs-0.0.3 (crate (name "ft-rs") (vers "0.0.3") (deps (list (crate-dep (name "chrono") (req "^0.4.33") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.11") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.13") (features (quote ("full"))) (default-features #t) (kind 2)))) (hash "1m7hyv6hlkl8111xas8vvm7flqm7vbqxvgjd6digafz5w8i1y6qm") (rust-version "1.56")))

