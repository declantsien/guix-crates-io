(define-module (crates-io ft #{62}#) #:use-module (crates-io))

(define-public crate-ft6236-0.0.1 (crate (name "ft6236") (vers "0.0.1") (deps (list (crate-dep (name "defmt") (req "^0.3.4") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "embedded-hal-02") (req "^0.2") (features (quote ("unproven"))) (default-features #t) (kind 0) (package "embedded-hal")) (crate-dep (name "embedded-hal-1") (req "^1.0.0-alpha.10") (default-features #t) (kind 0) (package "embedded-hal")))) (hash "1i29xzipnzbqngx39dr8y3h2jf7iv3bw5knlmv6wwa4x2wnimf5c")))

(define-public crate-ft6236-0.0.2 (crate (name "ft6236") (vers "0.0.2") (deps (list (crate-dep (name "defmt") (req "^0.3.4") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "embedded-hal-02") (req "^0.2") (features (quote ("unproven"))) (default-features #t) (kind 0) (package "embedded-hal")) (crate-dep (name "embedded-hal-1") (req "^1.0.0-alpha.10") (default-features #t) (kind 0) (package "embedded-hal")))) (hash "0jvxgxlrw0knvs83j9lsss19rqxvrzjz7rqsmigl6mxw55h0sn3x")))

(define-public crate-ft6236-0.0.3 (crate (name "ft6236") (vers "0.0.3") (deps (list (crate-dep (name "defmt") (req "^0.3.4") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "embedded-hal-02") (req "^0.2") (features (quote ("unproven"))) (default-features #t) (kind 0) (package "embedded-hal")) (crate-dep (name "embedded-hal-1") (req "^1.0.0-alpha.10") (default-features #t) (kind 0) (package "embedded-hal")))) (hash "1r9wb0cjnvz5zgdf48dc3yxi21xr3w7i6fxkpdfpgjhvyccpgr2k")))

