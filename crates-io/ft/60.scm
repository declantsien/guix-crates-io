(define-module (crates-io ft #{60}#) #:use-module (crates-io))

(define-public crate-ft60x_rs-0.1 (crate (name "ft60x_rs") (vers "0.1.0") (deps (list (crate-dep (name "dirs") (req "^5.0.0") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2.139") (default-features #t) (kind 0)) (crate-dep (name "libloading") (req "^0.7.4") (default-features #t) (kind 0)) (crate-dep (name "once_cell") (req "^1.17.1") (default-features #t) (kind 0)) (crate-dep (name "rust-embed") (req "^6.8.1") (default-features #t) (kind 0)) (crate-dep (name "tempfile") (req "^3.7.1") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.38") (default-features #t) (kind 0)))) (hash "179yhf71lv40a5gxism7va73flivlk6gs0gcc10c9g14gxgb23rm")))

