(define-module (crates-io hz rd) #:use-module (crates-io))

(define-public crate-hzrd-0.0.1 (crate (name "hzrd") (vers "0.0.1") (hash "1v18fj15ikzijbq8kp604sp7x9jbkynv6w539aw7vksa9aw3b7n2") (rust-version "1.70")))

(define-public crate-hzrd-0.0.2 (crate (name "hzrd") (vers "0.0.2") (hash "0si6zhhxzg0pj4rdfq0wfc31m5y9phwym0iba52pxjd47n2gzlij") (rust-version "1.65")))

(define-public crate-hzrd-0.0.3 (crate (name "hzrd") (vers "0.0.3") (hash "0nzbxlykgkbmwvhfj28mkykamflkx83vizygws0bssl7jv2bs9g8") (rust-version "1.65")))

(define-public crate-hzrd-0.0.4 (crate (name "hzrd") (vers "0.0.4") (hash "1wqzm4in9sr4ar7jgv0ankq5s0jc13p41jlk0zg63cwm3vpilss2") (rust-version "1.65")))

(define-public crate-hzrd-0.0.5 (crate (name "hzrd") (vers "0.0.5") (hash "1g2w13pk618qfw3b0kprxi2q3a7a3gj5nj31rhh409nsf5i6pvhw") (rust-version "1.65")))

(define-public crate-hzrd-0.0.6 (crate (name "hzrd") (vers "0.0.6") (hash "1p6ci5fap6zcw8whr1qqwbfj3cqc577rxfcxih98hcafyaq8zbxz") (rust-version "1.70")))

