(define-module (crates-io ei ff) #:use-module (crates-io))

(define-public crate-eiffel-0.0.1 (crate (name "eiffel") (vers "0.0.1") (hash "0b9hafz6wrpws41fjvmkv8l5jsrcynnpqrhb53i4m741y2qqzjf2")))

(define-public crate-eiffel-0.0.2 (crate (name "eiffel") (vers "0.0.2") (deps (list (crate-dep (name "eiffel-macros") (req "^0.0.2") (default-features #t) (kind 0)) (crate-dep (name "eiffel-macros-gen") (req "^0.0.2") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "release-plz") (req "^0.3.49") (default-features #t) (kind 2)) (crate-dep (name "syn") (req "^2.0.50") (features (quote ("full"))) (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.57") (default-features #t) (kind 0)))) (hash "1a2g49id7wdpiwm04ysk1nlln2g1rcqig96y8s8r5wsryaw791pk")))

(define-public crate-eiffel-0.0.3 (crate (name "eiffel") (vers "0.0.3") (deps (list (crate-dep (name "eiffel-macros") (req ">=0.0.3") (default-features #t) (kind 0)) (crate-dep (name "eiffel-macros-gen") (req ">=0.0.3") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "release-plz") (req "^0.3.49") (default-features #t) (kind 2)) (crate-dep (name "syn") (req "^2.0.50") (features (quote ("full"))) (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.57") (default-features #t) (kind 0)))) (hash "01ywq4kvygdfa8fpyshds38vpf9s0xd5qmhl6wxb0zr0h0y454kh")))

(define-public crate-eiffel-0.0.4 (crate (name "eiffel") (vers "0.0.4") (deps (list (crate-dep (name "eiffel-macros") (req ">=0.0.3") (default-features #t) (kind 0)) (crate-dep (name "eiffel-macros-gen") (req ">=0.0.3") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2.0.50") (features (quote ("full"))) (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.57") (default-features #t) (kind 0)))) (hash "109p4i7x5davfhlazvi7hcalzpg29iahlb7d8l38if815sv3vlsd")))

(define-public crate-eiffel-macros-0.0.1 (crate (name "eiffel-macros") (vers "0.0.1") (deps (list (crate-dep (name "thiserror") (req "^1.0.57") (default-features #t) (kind 0)))) (hash "0n9rxczj52zzvxm2c44svizbwgmpjjazjq2vzc7gdsfxfsi444bw")))

(define-public crate-eiffel-macros-0.0.2 (crate (name "eiffel-macros") (vers "0.0.2") (deps (list (crate-dep (name "thiserror") (req "^1.0.57") (default-features #t) (kind 0)))) (hash "0a7wvzkqfadqab4v437pkgjva3mzlfn3p0pf07ki9xs6w5b45bcx")))

(define-public crate-eiffel-macros-0.0.3 (crate (name "eiffel-macros") (vers "0.0.3") (deps (list (crate-dep (name "thiserror") (req "^1.0.57") (default-features #t) (kind 0)))) (hash "1g4v2bv134bzb00cbihm7v49frxajp3ci38klrzihpdc1nq2ivr6")))

(define-public crate-eiffel-macros-0.0.4 (crate (name "eiffel-macros") (vers "0.0.4") (deps (list (crate-dep (name "thiserror") (req "^1.0.57") (default-features #t) (kind 0)))) (hash "0dakx7xba7zqh5bnbfvmw0ybfc0sq6pv120h7cg6nnjb3bhqdsx5")))

(define-public crate-eiffel-macros-gen-0.0.1 (crate (name "eiffel-macros-gen") (vers "0.0.1") (deps (list (crate-dep (name "proc-macro2") (req "^1.0.78") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2.0.50") (features (quote ("full"))) (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.57") (default-features #t) (kind 0)))) (hash "13mnibh693cww71nafv049sv6dxv2a1bqv8nc2973lyfwdaa90c5")))

(define-public crate-eiffel-macros-gen-0.0.2 (crate (name "eiffel-macros-gen") (vers "0.0.2") (deps (list (crate-dep (name "proc-macro2") (req "^1.0.78") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2.0.50") (features (quote ("full"))) (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.57") (default-features #t) (kind 0)))) (hash "1pfs57jvl37552vdk13adj2xlx1wln12by2p5f707vkb76zflmwp")))

(define-public crate-eiffel-macros-gen-0.0.3 (crate (name "eiffel-macros-gen") (vers "0.0.3") (deps (list (crate-dep (name "proc-macro2") (req "^1.0.78") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2.0.50") (features (quote ("full"))) (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.57") (default-features #t) (kind 0)))) (hash "181rv3j7lkjn76x3vm3mzc627ghf2z1gx02dph4l1j4jmbrgyhwv")))

(define-public crate-eiffel-macros-gen-0.0.4 (crate (name "eiffel-macros-gen") (vers "0.0.4") (deps (list (crate-dep (name "proc-macro2") (req "^1.0.78") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2.0.50") (features (quote ("full"))) (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.57") (default-features #t) (kind 0)))) (hash "0m8j2qac725i4p24mm21bnsxz0d73ij68jbx7ddsmrbzcpkgkkar")))

