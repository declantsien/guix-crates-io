(define-module (crates-io ei p5) #:use-module (crates-io))

(define-public crate-eip55-0.1 (crate (name "eip55") (vers "0.1.0") (deps (list (crate-dep (name "rust-crypto") (req "^0.2.36") (default-features #t) (kind 0)) (crate-dep (name "sha3") (req "^0.9.1") (default-features #t) (kind 0)))) (hash "1ifhdjv6pxwx6ycp529bays3bx64yx5kpnhcgyk1ccs8al6qzxgm")))

(define-public crate-eip55-0.1 (crate (name "eip55") (vers "0.1.1") (deps (list (crate-dep (name "rust-crypto") (req "^0.2.36") (default-features #t) (kind 0)))) (hash "0bzkg2i3gds4pm3jbaxkbqwz4a833mx2mv9mbnjfdr6p4fr95ylj")))

(define-public crate-eip55-0.2 (crate (name "eip55") (vers "0.2.0") (deps (list (crate-dep (name "hex") (req "^0.4.3") (default-features #t) (kind 0)) (crate-dep (name "sha3") (req "^0.10.5") (default-features #t) (kind 0)))) (hash "0wqmh15ihgkx5d30vm33yvb99myv78aj7yr418nqnpsl7pmh2klp")))

(define-public crate-eip55-0.3 (crate (name "eip55") (vers "0.3.0") (deps (list (crate-dep (name "hex") (req "^0.4.3") (default-features #t) (kind 0)) (crate-dep (name "sha3") (req "^0.10.5") (default-features #t) (kind 0)))) (hash "1kxa52ha0qgy87xxsgx25x6hxn8l3l1fm3g6b22v37i7wzwb08a9")))

(define-public crate-eip55-0.4 (crate (name "eip55") (vers "0.4.0") (deps (list (crate-dep (name "hex") (req "^0.4.3") (default-features #t) (kind 0)) (crate-dep (name "sha3") (req "^0.10.5") (default-features #t) (kind 0)))) (hash "08g4mjgdrip9vx3jrvn37cacs73msyhsggcsk99bhbh339p6hizl")))

