(define-module (crates-io ei rc) #:use-module (crates-io))

(define-public crate-eirc-0.0.0 (crate (name "eirc") (vers "0.0.0") (deps (list (crate-dep (name "eventbus") (req "^0.1.5") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^0.2.0") (default-features #t) (kind 0)))) (hash "1a72ykq9zjmqajcgb7ciydb3rpvxxljjvn6f7h1v66a3qp058fw4")))

