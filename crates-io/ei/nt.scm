(define-module (crates-io ei nt) #:use-module (crates-io))

(define-public crate-eint-0.1 (crate (name "eint") (vers "0.1.0") (hash "1sp49ndv0wsbcd87lnv7h5najxha2liljyvkq433z8d1d06ifm9m")))

(define-public crate-eint-0.1 (crate (name "eint") (vers "0.1.1") (deps (list (crate-dep (name "proptest") (req "^1.0.0") (default-features #t) (kind 2)))) (hash "151spqjk92d5q60s9ld415fmkgx0rdzq85kg8k8lh6ciahrryd4b")))

(define-public crate-eint-0.1 (crate (name "eint") (vers "0.1.2") (deps (list (crate-dep (name "proptest") (req "^1.0.0") (default-features #t) (kind 2)))) (hash "0qdjmv4npbbidbswgw0vds3c2k8l3cybjibhb0knlzkpyy24gf9c")))

(define-public crate-eint-0.1 (crate (name "eint") (vers "0.1.3") (deps (list (crate-dep (name "criterion") (req "^0.3.5") (default-features #t) (kind 2)) (crate-dep (name "proptest") (req "^1.0.0") (default-features #t) (kind 2)))) (hash "036bjxwzdahyc4jpqw4shkmhkmwqmmp4d6za5v7mzm07pq1zjl1d")))

(define-public crate-eint-0.1 (crate (name "eint") (vers "0.1.4") (deps (list (crate-dep (name "criterion") (req "^0.3.5") (default-features #t) (kind 2)) (crate-dep (name "proptest") (req "^1.0.0") (default-features #t) (kind 2)))) (hash "1pg6qiznbx0qprcpvrh00174w80rv9ll4ly3yqx35fsqsiz89w4l")))

(define-public crate-eint-0.1 (crate (name "eint") (vers "0.1.5") (deps (list (crate-dep (name "criterion") (req "^0.3.5") (default-features #t) (kind 2)) (crate-dep (name "proptest") (req "^1.0.0") (default-features #t) (kind 2)))) (hash "0xln27kq3r42z945cjicgqlxkw2cyc9hg7sn0jmac7didx5njgvx")))

(define-public crate-eint-0.1 (crate (name "eint") (vers "0.1.6") (deps (list (crate-dep (name "criterion") (req "^0.3.5") (default-features #t) (kind 2)) (crate-dep (name "proptest") (req "^1.0.0") (default-features #t) (kind 2)))) (hash "04303s6nx4am6magkrm1h6cldnzkjsdpf236352cl2zbvqs3v2k6")))

(define-public crate-eint-0.1 (crate (name "eint") (vers "0.1.7") (deps (list (crate-dep (name "criterion") (req "^0.3.5") (default-features #t) (kind 2)) (crate-dep (name "proptest") (req "^1.0.0") (default-features #t) (kind 2)))) (hash "1wviyr4prr278dvw110fw0rhcbz0abzqbk77j4k0l3s90q77i1v6")))

(define-public crate-eint-0.1 (crate (name "eint") (vers "0.1.8") (deps (list (crate-dep (name "criterion") (req "^0.3.5") (default-features #t) (kind 2)) (crate-dep (name "proptest") (req "^1.0.0") (default-features #t) (kind 2)))) (hash "118yi8w5saynan0awijmgxwwp5vk239zjyxbpbixbqxd1hnh0l40")))

(define-public crate-eint-0.1 (crate (name "eint") (vers "0.1.9") (deps (list (crate-dep (name "cc") (req "^1.0") (default-features #t) (kind 1)) (crate-dep (name "criterion") (req "^0.3.5") (default-features #t) (kind 2)) (crate-dep (name "proptest") (req "^1.0.0") (default-features #t) (kind 2)))) (hash "1z7hwfq6yy89dx67q0r637g6hi0qhpayp4gjg3rrdwya34wjlwgf")))

(define-public crate-eint-0.1 (crate (name "eint") (vers "0.1.10") (deps (list (crate-dep (name "cc") (req "^1.0") (default-features #t) (kind 1)) (crate-dep (name "criterion") (req "^0.3.5") (default-features #t) (kind 2)) (crate-dep (name "proptest") (req "^1.0.0") (default-features #t) (kind 2)))) (hash "0r6hx1lnqmbnfrvlx4v571wxfcmq5ykz8506rvgw63wm6dqs2cyg")))

(define-public crate-eint-1 (crate (name "eint") (vers "1.0.0") (deps (list (crate-dep (name "criterion") (req "^0.3.5") (default-features #t) (kind 2)) (crate-dep (name "uint") (req "^0.9.3") (default-features #t) (kind 0)))) (hash "13ivrcsk5mx77vbak6y27r1pkfhdl8wzxdyi38hx3zqjr7hgnplr")))

