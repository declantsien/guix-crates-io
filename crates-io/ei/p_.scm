(define-module (crates-io ei p_) #:use-module (crates-io))

(define-public crate-eip_1024-0.1 (crate (name "eip_1024") (vers "0.1.0") (deps (list (crate-dep (name "arrayref") (req "^0.3.5") (default-features #t) (kind 0)) (crate-dep (name "hex") (req "^0.4.0") (default-features #t) (kind 0)) (crate-dep (name "saltbabe") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "1g3piiykxhvbdgwfaprvzdmqr6szkplk4fggnk141jryqdi91nss") (yanked #t)))

(define-public crate-eip_1024-0.1 (crate (name "eip_1024") (vers "0.1.1") (deps (list (crate-dep (name "arrayref") (req "^0.3.5") (default-features #t) (kind 0)) (crate-dep (name "hex") (req "^0.4.0") (default-features #t) (kind 0)) (crate-dep (name "saltbabe") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "0ija5yddcfnajhcbn069yc8h6n87ln8i5j78sx8nm3l2xyifis0v")))

