(define-module (crates-io ei gs) #:use-module (crates-io))

(define-public crate-eigs-0.0.1 (crate (name "eigs") (vers "0.0.1") (deps (list (crate-dep (name "arpack-ng-sys") (req "^0.2.1") (features (quote ("static"))) (default-features #t) (kind 0)) (crate-dep (name "num-complex") (req "^0.4.4") (default-features #t) (kind 0)) (crate-dep (name "umfpack-rs") (req "^0.0.1") (default-features #t) (kind 0)))) (hash "0was0pn4w0pk36hry7b81gqzkr69z4hpciibbs91nixgcayp57m5")))

(define-public crate-eigs-0.0.2 (crate (name "eigs") (vers "0.0.2") (deps (list (crate-dep (name "arpack-ng-sys") (req "^0.2.1") (features (quote ("static"))) (default-features #t) (kind 0)) (crate-dep (name "num-complex") (req "^0.4.4") (default-features #t) (kind 0)) (crate-dep (name "umfpack-rs") (req "^0.0.1") (default-features #t) (kind 0)))) (hash "1wbwp3521ww03pj9d9701l5abfxi2z5b5h69ia3yxphjq9g4pgwc")))

(define-public crate-eigs-0.0.3 (crate (name "eigs") (vers "0.0.3") (deps (list (crate-dep (name "arpack-ng-sys") (req "^0.2.1") (features (quote ("static"))) (default-features #t) (kind 0)) (crate-dep (name "num-complex") (req "^0.4.4") (default-features #t) (kind 0)) (crate-dep (name "umfpack-rs") (req "^0.0.1") (default-features #t) (kind 0)))) (hash "05ald4z92zlpwzgx4wdzzcn887849d579498y342a6mk8c19rp73")))

(define-public crate-eigs-rs-0.0.1 (crate (name "eigs-rs") (vers "0.0.1") (deps (list (crate-dep (name "arpack-ng-sys") (req "^0.2.1") (features (quote ("static"))) (default-features #t) (kind 0)) (crate-dep (name "num-complex") (req "^0.4.4") (default-features #t) (kind 0)) (crate-dep (name "umfpack-rs") (req "^0.0.1") (default-features #t) (kind 0)))) (hash "1dfag8s6nkrvykpkz8xj5rmyynnvi9nyzvgdrhpfw3s12sfcd1km") (yanked #t)))

(define-public crate-eigs-rs-0.0.2 (crate (name "eigs-rs") (vers "0.0.2") (deps (list (crate-dep (name "arpack-ng-sys") (req "^0.2.1") (features (quote ("static"))) (default-features #t) (kind 0)) (crate-dep (name "num-complex") (req "^0.4.4") (default-features #t) (kind 0)) (crate-dep (name "umfpack-rs") (req "^0.0.1") (default-features #t) (kind 0)))) (hash "15ad9362jwhgbda13z400ma6q92k2c5157i83hwmz5mdgbvixjvj") (yanked #t)))

