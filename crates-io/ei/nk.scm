(define-module (crates-io ei nk) #:use-module (crates-io))

(define-public crate-eink_waveshare_rs-0.1 (crate (name "eink_waveshare_rs") (vers "0.1.0") (deps (list (crate-dep (name "embedded-hal") (req "^0.2.1") (features (quote ("unproven"))) (default-features #t) (kind 0)))) (hash "1hzxv22j38pizyhvf5zrf4g1ffgqwqhshahqfnpaqca8p53mqxch") (features (quote (("epd4in2_fast_update") ("epd4in2") ("epd2in9") ("epd1in54") ("default" "epd1in54" "epd2in9" "epd4in2")))) (yanked #t)))

(define-public crate-eink_waveshare_rs-0.1 (crate (name "eink_waveshare_rs") (vers "0.1.1") (deps (list (crate-dep (name "embedded-hal") (req "^0.2.1") (features (quote ("unproven"))) (default-features #t) (kind 0)))) (hash "158srr42y1rd0b0ays3kgzwpzs9jd54gmjkhvqy0hdziq4z48l2y") (features (quote (("epd4in2_fast_update") ("epd4in2") ("epd2in9") ("epd1in54") ("default" "epd1in54" "epd2in9" "epd4in2")))) (yanked #t)))

(define-public crate-eink_waveshare_rs-0.1 (crate (name "eink_waveshare_rs") (vers "0.1.2") (deps (list (crate-dep (name "embedded-hal") (req "^0.2.1") (features (quote ("unproven"))) (default-features #t) (kind 0)))) (hash "1vql7jfnvbpy1fxsd1na8g0gqqp9syb1472sn3jpsxw566x0hbh0") (features (quote (("epd4in2_fast_update") ("epd4in2") ("epd2in9") ("epd1in54") ("default" "epd1in54" "epd2in9" "epd4in2")))) (yanked #t)))

(define-public crate-eink_waveshare_rs-0.1 (crate (name "eink_waveshare_rs") (vers "0.1.3") (hash "1s45c7x13garb08la6g6cxz69m7ngh7bwv7scxvfa8s123n70ryx") (yanked #t)))

(define-public crate-eink_waveshare_rs-0.1 (crate (name "eink_waveshare_rs") (vers "0.1.4") (hash "164vvhwg9kjcvbh9vqwa0fx03m6i88c94adkaha8by04awyby6i9") (yanked #t)))

(define-public crate-eink_waveshare_rs-0.1 (crate (name "eink_waveshare_rs") (vers "0.1.5") (hash "1kby1f959ygma29hh4y0pdbjll474lqw54h7qs4r6m7mmrri9wx0") (yanked #t)))

(define-public crate-eink_waveshare_rs-0.1 (crate (name "eink_waveshare_rs") (vers "0.1.6") (hash "0lzzhd1wqbv2p507z9m63gb23q1s225x6i3gvsb3kp6nkiyj772x")))

