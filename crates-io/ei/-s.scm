(define-module (crates-io ei -s) #:use-module (crates-io))

(define-public crate-ei-sys-0.1 (crate (name "ei-sys") (vers "0.1.0") (deps (list (crate-dep (name "libc") (req "^0.2") (kind 0)))) (hash "1dd1nrawd2fgxyhn49b2ad9dq7s45c93kkny4x13h0rwm7ailq3j") (links "ei")))

(define-public crate-ei-sys-0.1 (crate (name "ei-sys") (vers "0.1.1") (deps (list (crate-dep (name "libc") (req "^0.2") (kind 0)))) (hash "1dcl612wh5sjy6xz9hbzs9w26l8x8qcwmmsvdqs1lxggn6rldcwg") (links "ei")))

(define-public crate-ei-sys-0.2 (crate (name "ei-sys") (vers "0.2.0") (deps (list (crate-dep (name "libc") (req "^0.2") (kind 0)) (crate-dep (name "winapi") (req "^0.3") (features (quote ("inaddr"))) (default-features #t) (target "cfg(windows)") (kind 0)))) (hash "1wykkqkjqd7nw33pds1qqw2g6krcn9w7k1ah88f96hh1ld376lm0") (links "ei")))

(define-public crate-ei-sys-0.3 (crate (name "ei-sys") (vers "0.3.0") (deps (list (crate-dep (name "libc") (req "^0.2") (kind 0)) (crate-dep (name "winapi") (req "^0.3") (features (quote ("inaddr"))) (default-features #t) (target "cfg(windows)") (kind 0)))) (hash "17aynn1llkpignarsmq2al9gj4rav271fgki9mc5k9c7fgs926mr") (links "ei")))

(define-public crate-ei-sys-0.4 (crate (name "ei-sys") (vers "0.4.0") (deps (list (crate-dep (name "libc") (req "^0.2") (kind 0)) (crate-dep (name "winapi") (req "^0.3") (features (quote ("inaddr"))) (default-features #t) (target "cfg(windows)") (kind 0)))) (hash "04kabdvs2k3rdmb78rr2pbs9djji60s2r07r0jy675bj3d1askz4") (links "ei")))

(define-public crate-ei-sys-0.5 (crate (name "ei-sys") (vers "0.5.0") (deps (list (crate-dep (name "in_addr") (req "^0.1.2") (features (quote ("no-std"))) (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2") (kind 0)))) (hash "1ydrbd5p6z9inwkq675q6wi4lnl9317s2qjq8h2xfnyl2q02fvg2") (links "ei")))

(define-public crate-ei-sys-0.6 (crate (name "ei-sys") (vers "0.6.0") (deps (list (crate-dep (name "in_addr") (req "^0.2") (kind 0)) (crate-dep (name "libc") (req "^0.2") (kind 0)))) (hash "1dlrcf5fkgy5wxj3x6yscn82hq4cyjikjaz6f7rnqp8g2h93pg3v") (links "ei")))

(define-public crate-ei-sys-0.7 (crate (name "ei-sys") (vers "0.7.0") (deps (list (crate-dep (name "in_addr") (req "^1.0") (kind 0)) (crate-dep (name "libc") (req "^0.2") (kind 0)))) (hash "17dkk9x5nrzcxxi7k7s7nrznfciwyy8wcmrpxwrbldns6ia986rc") (links "ei")))

(define-public crate-ei-sys-0.8 (crate (name "ei-sys") (vers "0.8.0") (deps (list (crate-dep (name "in_addr") (req "^1.0") (kind 0)) (crate-dep (name "libc") (req "^0.2") (kind 0)))) (hash "1l6nyb9nx4b1pscrb0z5hkjdq1iv0xpxk6dc7g17r81j06vvp8sb") (links "ei")))

(define-public crate-ei-sys-0.8 (crate (name "ei-sys") (vers "0.8.1") (deps (list (crate-dep (name "in_addr") (req "^1.0") (kind 0)) (crate-dep (name "libc") (req "^0.2") (kind 0)))) (hash "1y7bh1sp1xfdlnq2qnwa4b4iiargfaj3yixnxi5gb8vjsdyz8vla") (links "ei")))

