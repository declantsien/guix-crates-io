(define-module (crates-io ei do) #:use-module (crates-io))

(define-public crate-eidolon-1 (crate (name "eidolon") (vers "1.1.0") (deps (list (crate-dep (name "regex") (req "^0.2") (default-features #t) (kind 0)))) (hash "1vlhmchvx934r0x1x00hmyli5wqdvp6fb0z0rs8nqcjj8kvc51ff")))

(define-public crate-eidolon-1 (crate (name "eidolon") (vers "1.1.1") (deps (list (crate-dep (name "regex") (req "^0.2") (default-features #t) (kind 0)))) (hash "183rf1wglc9m2h11hnk35q85zww4abhf44nfgm1yyafal6r9rl4v")))

(define-public crate-eidolon-1 (crate (name "eidolon") (vers "1.1.2") (deps (list (crate-dep (name "regex") (req "^0.2") (default-features #t) (kind 0)))) (hash "15ws55x47bzfnrqxdhv8hdqq1xvxamy9813ivhk6mx10qxk7qf8n")))

(define-public crate-eidolon-1 (crate (name "eidolon") (vers "1.1.3") (deps (list (crate-dep (name "regex") (req "^0.2") (default-features #t) (kind 0)))) (hash "1kc9f6ydyz9vxhbq89hx5hnirzmvrhs99mz61bvincp58jbhgkjp")))

(define-public crate-eidolon-1 (crate (name "eidolon") (vers "1.1.4") (deps (list (crate-dep (name "regex") (req "^0.2") (default-features #t) (kind 0)))) (hash "18738pqpfzpzwsbr9qg1ahr9s6qrspzwa9wg9hdgy8q5f1w0xz08")))

(define-public crate-eidolon-1 (crate (name "eidolon") (vers "1.1.5") (deps (list (crate-dep (name "regex") (req "^0.2") (default-features #t) (kind 0)))) (hash "0hfaiiinnm679zxyaii6x7kwidid5ykjms98m8hdcp243vjqq7fn")))

(define-public crate-eidolon-1 (crate (name "eidolon") (vers "1.2.5") (deps (list (crate-dep (name "regex") (req "^0.2") (default-features #t) (kind 0)))) (hash "13zfwjb4pp0bfq2vhjqsbgv183jhw9dwp8y2zf5hsf78a71kggyk")))

(define-public crate-eidolon-1 (crate (name "eidolon") (vers "1.2.6") (deps (list (crate-dep (name "regex") (req "^0.2") (default-features #t) (kind 0)))) (hash "0al2g6g30snlh7lzw282dq1wwhxxvqbfdcgc27kw6b9r40607chl")))

(define-public crate-eidolon-1 (crate (name "eidolon") (vers "1.2.7") (deps (list (crate-dep (name "regex") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.70") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)))) (hash "1gvry6v8lpmlazgk1spylmm4nmfs3f5hljrshc8a6hmg3l7f1y86")))

(define-public crate-eidolon-1 (crate (name "eidolon") (vers "1.3.0") (deps (list (crate-dep (name "regex") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.70") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)))) (hash "00b0ak8mfv4j2vkimp5n2aaxzg7ldb22gmkwx1w27arwhyz2lgj0")))

(define-public crate-eidolon-1 (crate (name "eidolon") (vers "1.4.0") (deps (list (crate-dep (name "regex") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.70") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)))) (hash "0d0wf7cchcjx8jypjjm493ajhk1hxphw9bdyjy7pm7b8aqbfnnnj")))

(define-public crate-eidolon-1 (crate (name "eidolon") (vers "1.4.1") (deps (list (crate-dep (name "regex") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.70") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.2.10") (default-features #t) (kind 0)))) (hash "0bnf2b8lv4mzdk9bg967kld41yjx8d1bjwfdnccbg2ywvc4w5a95")))

(define-public crate-eidolon-1 (crate (name "eidolon") (vers "1.4.2") (deps (list (crate-dep (name "butlerd") (req "^0.1.1") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.70") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.2.10") (default-features #t) (kind 0)))) (hash "0dnxb69agdgqjh0g63r1yk7hpmdngmp99ydizw15rp3lzv2v6x6p")))

(define-public crate-eidolon-1 (crate (name "eidolon") (vers "1.4.3") (deps (list (crate-dep (name "butlerd") (req "^0.1.2") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.70") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.2.10") (default-features #t) (kind 0)))) (hash "1gvigj3855f56myb2ngvi5qch3b44mmk9n57vi3815j4h4gwcayb")))

(define-public crate-eidolon-1 (crate (name "eidolon") (vers "1.4.4") (deps (list (crate-dep (name "butlerd") (req "^0.1.2") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.70") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.2.10") (default-features #t) (kind 0)))) (hash "172gq7l9987m3c4aqczbmd97gacr8c0af77sw7yi16w2h7b5ggq0")))

(define-public crate-eidolon-1 (crate (name "eidolon") (vers "1.4.5") (deps (list (crate-dep (name "butlerd") (req "^0.1.2") (default-features #t) (kind 0)) (crate-dep (name "dirs") (req "^1.0.4") (default-features #t) (kind 0)) (crate-dep (name "human-panic") (req "^1.0.1") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.70") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.2.10") (default-features #t) (kind 0)))) (hash "0h97hzz6b69iq1lj47z4jdmcvk9gjzqh90dlmkmr7v2w9d2ziqbr")))

(define-public crate-eidolon-1 (crate (name "eidolon") (vers "1.4.6") (deps (list (crate-dep (name "butlerd") (req "^0.1.2") (default-features #t) (kind 0)) (crate-dep (name "dirs") (req "^1.0.4") (default-features #t) (kind 0)) (crate-dep (name "human-panic") (req "^1.0.1") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.70") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.2.10") (default-features #t) (kind 0)))) (hash "0w8n6n8xah0gmlbi5bn4dp76n8csg7508qab87hjinvg4vpiwl4a")))

(define-public crate-eidolon-auth-0.1 (crate (name "eidolon-auth") (vers "0.1.0") (deps (list (crate-dep (name "amplify") (req "^4.0.0-beta.4") (default-features #t) (kind 0)) (crate-dep (name "cyphergraphy") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "11yp89mhpkksq478ahbm6g2c1c14py6vvyrrvz96412f0c2qszdw") (rust-version "1.65")))

(define-public crate-eidolon-auth-0.2 (crate (name "eidolon-auth") (vers "0.2.0") (deps (list (crate-dep (name "amplify") (req "^4.0.0-beta.17") (default-features #t) (kind 0)) (crate-dep (name "cyphergraphy") (req "^0.2.0") (default-features #t) (kind 0)))) (hash "1z1z7w82wip3s0b1rsi35gk2n69d1fglxlh3ab5wm2zfn48l8xja") (rust-version "1.65")))

(define-public crate-eidolon-auth-0.3 (crate (name "eidolon-auth") (vers "0.3.0") (deps (list (crate-dep (name "amplify") (req "^4.6.0") (default-features #t) (kind 0)) (crate-dep (name "cyphergraphy") (req "^0.3.0") (default-features #t) (kind 0)))) (hash "17krb5r4c19v6irjna9gscrkb99h66cy8vw928lpssaqghya2ms0") (rust-version "1.69")))

