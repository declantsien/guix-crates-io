(define-module (crates-io ei mg) #:use-module (crates-io))

(define-public crate-eimg-0.1 (crate (name "eimg") (vers "0.1.0") (deps (list (crate-dep (name "image") (req "^0.21") (default-features #t) (kind 0)))) (hash "1kg4n86nrry64dz3a26s2k2gw4m4h2dvnfla4hv1zkqbmfvzxvrm")))

(define-public crate-eimg-0.2 (crate (name "eimg") (vers "0.2.0") (deps (list (crate-dep (name "image") (req "^0.21") (default-features #t) (kind 0)))) (hash "0ilwlvim8d17fc4q5f5syxir3mjiksbc5i97yw0s89dcmfdqlglz")))

(define-public crate-eimg-0.2 (crate (name "eimg") (vers "0.2.1") (deps (list (crate-dep (name "image") (req "^0.21.1") (default-features #t) (kind 0)))) (hash "0k89a4p09mvg6alrv96wrpjwsll1rwhmjynm0ksfpjbzlw4xdpp8")))

