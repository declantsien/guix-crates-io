(define-module (crates-io ei no) #:use-module (crates-io))

(define-public crate-einops-0.1 (crate (name "einops") (vers "0.1.0") (deps (list (crate-dep (name "tch") (req "^0.3") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0") (default-features #t) (kind 0)))) (hash "12qj7dixfqg2px55dlc58yfwrzx236agiw3i69va0k0nq6hskbbq") (features (quote (("tch-bindings" "tch"))))))

(define-public crate-einops-0.2 (crate (name "einops") (vers "0.2.0") (deps (list (crate-dep (name "tch") (req "^0.5") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0") (default-features #t) (kind 0)))) (hash "1fjckp86879w5qgrs8wb0ilvczg7k17ak19yjihfjcva55p1p5vr") (features (quote (("tch-bindings" "tch") ("default" "tch-bindings"))))))

(define-public crate-einops-0.2 (crate (name "einops") (vers "0.2.1") (deps (list (crate-dep (name "tch") (req "^0.5") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0") (default-features #t) (kind 0)))) (hash "0rqcacld5gxy2dzplwcpwzg7wyzjq1q72a2qafka60xanjjzrkzy") (features (quote (("tch-bindings" "tch") ("default" "tch-bindings"))))))

(define-public crate-einops-0.3 (crate (name "einops") (vers "0.3.0-alpha.1") (deps (list (crate-dep (name "einops-macros") (req "^0.1.0-alpha.1") (default-features #t) (kind 0)) (crate-dep (name "tch") (req "^0.6") (optional #t) (default-features #t) (kind 0)))) (hash "03gak5m1fy1rmkv45cjgrm5wmwrn844bzb7k1dvczh8qb0iilgih") (features (quote (("tch-bindings" "tch") ("default" "tch-bindings"))))))

(define-public crate-einops-0.3 (crate (name "einops") (vers "0.3.0-alpha.2") (deps (list (crate-dep (name "einops-macros") (req "^0.1.0-alpha.2") (default-features #t) (kind 0)) (crate-dep (name "tch") (req "^0.7") (optional #t) (default-features #t) (kind 0)))) (hash "0mqqq802vph5anqkqhggl6gycbq8qjgkb54pg0rkhhks7wjqr60y") (features (quote (("tch-bindings" "tch") ("default" "tch-bindings"))))))

(define-public crate-einops-macros-0.1 (crate (name "einops-macros") (vers "0.1.0-alpha.1") (deps (list (crate-dep (name "proc-macro2") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1") (features (quote ("full" "extra-traits"))) (default-features #t) (kind 0)))) (hash "0b0zxg367n2s8xlmznl5kqx6s3x2ff95aghw32kiqwsd82aznlx4")))

(define-public crate-einops-macros-0.1 (crate (name "einops-macros") (vers "0.1.0-alpha.2") (deps (list (crate-dep (name "proc-macro2") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1") (features (quote ("full" "extra-traits"))) (default-features #t) (kind 0)))) (hash "1bymm0br2raf2k36drzizzcvb3ngd8m0szgg0m7fivaw04ws3c17")))

