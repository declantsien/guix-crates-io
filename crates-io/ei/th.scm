(define-module (crates-io ei th) #:use-module (crates-io))

(define-public crate-either-0.1 (crate (name "either") (vers "0.1.0") (hash "06ljmgv5w26rh7s32746wks5vph81v1av5azpp3zq298b970zprl")))

(define-public crate-either-0.1 (crate (name "either") (vers "0.1.1") (hash "1ddfv93njs54gzldbsfbjcf4rp97zl7y952f8xhg1ds6xhfh74gl")))

(define-public crate-either-0.1 (crate (name "either") (vers "0.1.2") (hash "09p6jm7gb03lq6h5xii3z4l1aypqv41mpqjrx5lwn1zpr62kqza2")))

(define-public crate-either-0.1 (crate (name "either") (vers "0.1.3") (hash "0696xqfapyc8xzmp9r0zc05gs05704zs8l23cp3zzfry68s4iyhi")))

(define-public crate-either-0.1 (crate (name "either") (vers "0.1.4") (hash "0zd9q77c92ak5bp21cwwsf2ajsz5hfy2p4h0xcn0vsibhvnakl4k")))

(define-public crate-either-0.1 (crate (name "either") (vers "0.1.5") (hash "1jgm3smnxa4c275c7w761xvclxf73w3y41igy1npm8bb5bv0m0n9")))

(define-public crate-either-0.1 (crate (name "either") (vers "0.1.6") (hash "16c6bbrf6xq05l67vawbhdc446x8glw1c8qf7gxms86m659sj9xn")))

(define-public crate-either-0.1 (crate (name "either") (vers "0.1.7") (hash "19wy97sp344n6i6653rr8b45imyh937v0g3plvcca5903vngz6x3")))

(define-public crate-either-1 (crate (name "either") (vers "1.0.0") (hash "19mrvdv98w33xr46h920ckfp32sadpr2siirvwb99xwpsivvyiaq") (features (quote (("use_std") ("default" "use_std"))))))

(define-public crate-either-1 (crate (name "either") (vers "1.0.1") (hash "13gdvxpd8kcl51dmvpsfi5mafvjpa559rn4zlnl8kg8sgqmwi8la") (features (quote (("use_std") ("default" "use_std"))))))

(define-public crate-either-1 (crate (name "either") (vers "1.0.2") (hash "149aygbby4hbgx9i5n7qngnsnfblaa62pksf86pjmmnshqy50arx") (features (quote (("use_std") ("default" "use_std"))))))

(define-public crate-either-1 (crate (name "either") (vers "1.0.3") (hash "0707v2rrk2nng0vrkrh3sv2mbsic7wvm00jzx1w423fam4slmyb3") (features (quote (("use_std") ("default" "use_std"))))))

(define-public crate-either-1 (crate (name "either") (vers "1.1.0") (hash "0ajng2fhcbyd89shfwiwgsinjzpfm6nl8zlkgh9mihh6m0dmqy0q") (features (quote (("use_std") ("default" "use_std"))))))

(define-public crate-either-1 (crate (name "either") (vers "1.2.0") (hash "0lm208pjfx5qi7h881sia4k4iqjrkg66rgajpxlnhha5j9g17vnb") (features (quote (("use_std") ("default" "use_std"))))))

(define-public crate-either-1 (crate (name "either") (vers "1.3.0") (deps (list (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)))) (hash "1kkksacv24msdgs5jdhiq1dzifgkb7n92kdmiy2xzyqjjm3sf4g3") (features (quote (("use_std") ("default" "use_std"))))))

(define-public crate-either-1 (crate (name "either") (vers "1.4.0") (deps (list (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)))) (hash "1cw136q6z8zqlvkyiai857yl4b9a8jhhjrbdis3rw6lbykfph0bl") (features (quote (("use_std") ("default" "use_std"))))))

(define-public crate-either-1 (crate (name "either") (vers "1.5.0") (deps (list (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)))) (hash "1l610pr0b00ya5pmdii9r583c0z3z521ikygwxcpyzambk56br9v") (features (quote (("use_std") ("default" "use_std"))))))

(define-public crate-either-1 (crate (name "either") (vers "1.5.1") (deps (list (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)))) (hash "1b0z3nbnry7cc8vamygg7fxykpjksydxc0hrx8j7316w87356wy6") (features (quote (("use_std") ("default" "use_std"))))))

(define-public crate-either-1 (crate (name "either") (vers "1.5.2") (deps (list (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)))) (hash "0yyggfd5yq9hyyp0bd5jj0fgz3rwws42d19ri0znxwwqs3hcy9sm") (features (quote (("use_std") ("default" "use_std"))))))

(define-public crate-either-1 (crate (name "either") (vers "1.5.3") (deps (list (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)))) (hash "1qyz1b1acad6w0k5928jw5zaq900zhsk7p8dlcp4hh61w4f6n7xv") (features (quote (("use_std") ("default" "use_std"))))))

(define-public crate-either-1 (crate (name "either") (vers "1.6.0") (deps (list (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.0") (default-features #t) (kind 2)))) (hash "0krcvv612bd14iz2nxncjya3nv4ga3x9qxsqb6n4gsdwcncbamnd") (features (quote (("use_std") ("default" "use_std"))))))

(define-public crate-either-1 (crate (name "either") (vers "1.6.1") (deps (list (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.0") (default-features #t) (kind 2)))) (hash "0mwl9vngqf5jvrhmhn9x60kr5hivxyjxbmby2pybncxfqhf4z3g7") (features (quote (("use_std") ("default" "use_std"))))))

(define-public crate-either-1 (crate (name "either") (vers "1.7.0") (deps (list (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.0") (default-features #t) (kind 2)))) (hash "1gl1cybx0rrcvny4rcfl2agqbj6n0vz5bb1ws57sdhmgns3pn41z") (features (quote (("use_std") ("default" "use_std")))) (rust-version "1.31")))

(define-public crate-either-1 (crate (name "either") (vers "1.8.0") (deps (list (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.0") (default-features #t) (kind 2)))) (hash "15z70yaivlkpx27vzv99ibf8d2x5jp24yn69y0xi20w86v4c3rch") (features (quote (("use_std") ("default" "use_std")))) (rust-version "1.36")))

(define-public crate-either-1 (crate (name "either") (vers "1.8.1") (deps (list (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.0") (default-features #t) (kind 2)))) (hash "14bdy4qsxlfnm4626z4shwaiffi8l5krzkn7ykki1jgqzsrapjkz") (features (quote (("use_std") ("default" "use_std")))) (rust-version "1.36")))

(define-public crate-either-1 (crate (name "either") (vers "1.9.0") (deps (list (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.0") (default-features #t) (kind 2)))) (hash "01qy3anr7jal5lpc20791vxrw0nl6vksb5j7x56q2fycgcyy8sm2") (features (quote (("use_std") ("default" "use_std"))))))

(define-public crate-either-1 (crate (name "either") (vers "1.10.0") (deps (list (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.0") (default-features #t) (kind 2)))) (hash "0jiyq2mc1aa5b8whwl1bhm11i06xxcbk9ck7macxxggzjk07l58i") (features (quote (("use_std") ("default" "use_std")))) (rust-version "1.36")))

(define-public crate-either-1 (crate (name "either") (vers "1.11.0") (deps (list (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.0") (default-features #t) (kind 2)))) (hash "18l0cwyw18syl8b52syv6balql8mnwfyhihjqqllx5pms93iqz54") (features (quote (("use_std") ("default" "use_std")))) (rust-version "1.36")))

(define-public crate-either-1 (crate (name "either") (vers "1.12.0") (deps (list (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.0") (default-features #t) (kind 2)))) (hash "12xmhlrv5gfsraimh6xaxcmb0qh6cc7w7ap4sw40ky9wfm095jix") (features (quote (("use_std") ("default" "use_std")))) (rust-version "1.37")))

(define-public crate-either-future-0.1 (crate (name "either-future") (vers "0.1.0") (deps (list (crate-dep (name "either") (req "^1.5") (default-features #t) (kind 0)) (crate-dep (name "futures") (req "^0.1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "tokio01") (req "^0.1") (default-features #t) (kind 2) (package "tokio")) (crate-dep (name "tokio02") (req "^0.2.0-alpha") (default-features #t) (kind 2) (package "tokio")))) (hash "1300q8d321nql8566vda9k9sbx71s2n6h3q0s3dw85qaf69vzpg8") (features (quote (("std_future") ("futures_future" "futures") ("default" "std_future" "futures_future"))))))

(define-public crate-either-future-1 (crate (name "either-future") (vers "1.0.0") (deps (list (crate-dep (name "either") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "futures") (req "^0.1") (optional #t) (kind 0)) (crate-dep (name "futures") (req "^0.1") (features (quote ("use_std"))) (kind 2)))) (hash "0r02j8zdg94qmzbawaminghlw4myzhg1y7cqy104kfmhy3rrk4pg") (features (quote (("std_future") ("futures01" "futures") ("default" "std_future"))))))

(define-public crate-either-future-1 (crate (name "either-future") (vers "1.1.0") (deps (list (crate-dep (name "either") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "futures") (req "^0.1") (optional #t) (kind 0)) (crate-dep (name "futures") (req "^0.1") (features (quote ("use_std"))) (kind 2)) (crate-dep (name "futures-util") (req "^0.3") (optional #t) (kind 0)))) (hash "08drrdnysr1hvnbb896rgczv6bk5r5gvxn15yyvm114zv3xlj7m1") (features (quote (("std_future") ("futures03" "futures-util") ("futures01" "futures") ("default" "std_future"))))))

(define-public crate-either-slot-0.1 (crate (name "either-slot") (vers "0.1.0") (deps (list (crate-dep (name "loom") (req "^0.7") (default-features #t) (target "cfg(loom)") (kind 2)))) (hash "1hl005yam6z5zdj1b1sb7xgj50fly9b6qbm6w3nazkk6bwkkx7pq")))

(define-public crate-either-slot-1 (crate (name "either-slot") (vers "1.0.0") (deps (list (crate-dep (name "loom") (req "^0.7") (default-features #t) (target "cfg(loom)") (kind 0)) (crate-dep (name "tuple_list") (req "^0.1") (kind 0)))) (hash "1ylpqhff1b7gv2idmp3mpnjgf3q6qnh20nny74wj37gfypvplsxf")))

(define-public crate-either-slot-1 (crate (name "either-slot") (vers "1.1.0") (deps (list (crate-dep (name "loom") (req "^0.7") (default-features #t) (target "cfg(loom)") (kind 0)) (crate-dep (name "tuple_list") (req "^0.1") (kind 0)))) (hash "0ayq6l32j33cbhyarg3y3bssg58q22i50p921rrrqsm6s4z50vdl")))

(define-public crate-either-slot-1 (crate (name "either-slot") (vers "1.2.0") (deps (list (crate-dep (name "loom") (req "^0.7") (default-features #t) (target "cfg(loom)") (kind 0)) (crate-dep (name "tuple_list") (req "^0.1") (kind 0)))) (hash "1qdmaayy45hk1pzmn92l2v5i75npdgkvzw5mczi3gcysnvlhx2y7")))

(define-public crate-either_n-0.1 (crate (name "either_n") (vers "0.1.0") (hash "1yr3289llm7p280xd73bq5ws00mg0wd0jvlfsr9q4z9p0r4ad6sf") (features (quote (("use_std") ("default" "use_std"))))))

(define-public crate-either_n-0.1 (crate (name "either_n") (vers "0.1.1") (hash "0zvwbyg5grfphza5zj7bqaksmc158hkp906g1qk2ljvli7r8m4z4") (features (quote (("use_std") ("default" "use_std"))))))

(define-public crate-either_n-0.2 (crate (name "either_n") (vers "0.2.0") (hash "1rcsri9nx4zfdiy753yacyvl87g0nvklmsqrzdf0s5i9118sx4ac") (features (quote (("use_std") ("default" "use_std"))))))

(define-public crate-either_of-0.1 (crate (name "either_of") (vers "0.1.0") (deps (list (crate-dep (name "pin-project-lite") (req "^0.2") (default-features #t) (kind 0)))) (hash "1jv9wsy2p6ckiflidcrdwikdgwphbv1s20j9qvrgkb2w9pmjzqnn") (rust-version "1.75")))

(define-public crate-either_trait_macro-0.1 (crate (name "either_trait_macro") (vers "0.1.0") (deps (list (crate-dep (name "either") (req "^1.5.3") (default-features #t) (kind 2)) (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (features (quote ("fold" "full"))) (default-features #t) (kind 0)))) (hash "017jf10m7ykxsqyfwpk2ghpnz2arqn107pq4g0ax9qgi9360fb92")))

(define-public crate-either_trait_macro-0.1 (crate (name "either_trait_macro") (vers "0.1.1") (deps (list (crate-dep (name "either") (req "^1.5.3") (default-features #t) (kind 2)) (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (features (quote ("fold" "full"))) (default-features #t) (kind 0)))) (hash "1k4mkvg633vxa1bcb1cazrpywnmhd4p3k7sqfw6nlh31qk3lxibn")))

(define-public crate-eitherable-0.1 (crate (name "eitherable") (vers "0.1.0") (deps (list (crate-dep (name "either") (req "^1") (default-features #t) (kind 0)))) (hash "09pzgw2psz3ms09jfg2f7ycqpy7g9yaifv3186zxikhmmnncmx7d")))

(define-public crate-eitherq-0.1 (crate (name "eitherq") (vers "0.1.0") (hash "0nzns5w7lqgrnqdmj982vnrd064zi3dyxl0mn5fcavdpxj2akmbz")))

(define-public crate-eitherq-0.1 (crate (name "eitherq") (vers "0.1.1") (hash "1blz6xj78vx0qb2z1w29jw2ksqkdvjzrcrmspl7v9bhgwnv1lag6")))

(define-public crate-eitherq-0.1 (crate (name "eitherq") (vers "0.1.2") (hash "02iifs03g8xywwb85k115l4rq07kgkqwadb6ckrypf085n384hqi")))

