(define-module (crates-io ei ch) #:use-module (crates-io))

(define-public crate-eiche-0.1 (crate (name "eiche") (vers "0.1.0") (deps (list (crate-dep (name "lazy_static") (req "^1.4") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8") (default-features #t) (kind 0)))) (hash "0f3jin71lw33bi6i0bf3isl7cy6x20fcpx5aam6p08d0dn8xb4c7")))

(define-public crate-eiche-0.1 (crate (name "eiche") (vers "0.1.1") (deps (list (crate-dep (name "lazy_static") (req "^1.4") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8") (default-features #t) (kind 0)))) (hash "1h0hq86ysg0s7qqs8b4pkgr3yihgp4f7dzaic99sbpdpfz783xmb")))

