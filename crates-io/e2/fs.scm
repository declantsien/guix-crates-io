(define-module (crates-io e2 fs) #:use-module (crates-io))

(define-public crate-e2fslibs-sys-0.1 (crate (name "e2fslibs-sys") (vers "0.1.0") (deps (list (crate-dep (name "bindgen") (req "^0.30.0") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "pkg-config") (req "^0.3") (default-features #t) (kind 1)))) (hash "1wlzyna1hay04x4hw1cqklzhr72hgaa09fbampzm77ncz5fvki5m")))

(define-public crate-e2fslibs-sys-0.2 (crate (name "e2fslibs-sys") (vers "0.2.0") (deps (list (crate-dep (name "bindgen") (req "^0.30.0") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "pkg-config") (req "^0.3") (default-features #t) (kind 1)))) (hash "0j9hvrc6rgd3wl7nsw9d5ki8pkrgpz3i8ij3jsh4a54px48izs2h")))

