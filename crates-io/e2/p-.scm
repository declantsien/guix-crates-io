(define-module (crates-io e2 p-) #:use-module (crates-io))

(define-public crate-e2p-fileflags-0.1 (crate (name "e2p-fileflags") (vers "0.1.0") (deps (list (crate-dep (name "bitflags") (req "^2.0.0") (default-features #t) (kind 0)) (crate-dep (name "e2p-sys") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.115") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "e2p-sys") (req "^0.1.0") (default-features #t) (kind 1)))) (hash "11ih11dymq7rks4v9m5bbz0fsz6x4ia9yki8w77fqfhqxpzdmgiy") (v 2) (features2 (quote (("serde" "dep:serde" "bitflags/serde"))))))

(define-public crate-e2p-sys-0.1 (crate (name "e2p-sys") (vers "0.1.0") (deps (list (crate-dep (name "bindgen") (req "^0.64") (features (quote ("runtime"))) (kind 1)) (crate-dep (name "metadeps") (req "^1.1.2") (default-features #t) (kind 1)) (crate-dep (name "regex") (req "^1.3.9") (default-features #t) (kind 1)))) (hash "1kg1whkkis4hbw20kcn7d1f889vwv24qcf5blgv1x6dcl1wmh3r0") (links "e2p")))

