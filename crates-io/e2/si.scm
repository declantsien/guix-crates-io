(define-module (crates-io e2 si) #:use-module (crates-io))

(define-public crate-e2size-0.1 (crate (name "e2size") (vers "0.1.0") (deps (list (crate-dep (name "byteorder") (req "^1.4.3") (default-features #t) (kind 0)) (crate-dep (name "positioned-io") (req "^0.2.2") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.25") (default-features #t) (kind 0)))) (hash "1ax0g748s4x7p27f1drsraa58sfkjz8iw1g1v1lzc989g4yzlhjf")))

(define-public crate-e2size-0.1 (crate (name "e2size") (vers "0.1.1") (deps (list (crate-dep (name "byteorder") (req "^1.4.3") (default-features #t) (kind 0)) (crate-dep (name "positioned-io") (req "^0.2.2") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.25") (default-features #t) (kind 0)))) (hash "11fa3zkmkgr4hsbffzarrr2lflqhn2542qm5xxbbdbk04rfjqqlf")))

(define-public crate-e2size-0.1 (crate (name "e2size") (vers "0.1.2") (deps (list (crate-dep (name "byteorder") (req "^1.4.3") (default-features #t) (kind 0)) (crate-dep (name "positioned-io") (req "^0.2.2") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.25") (default-features #t) (kind 0)))) (hash "1z01v4zqza8am0m49rlzl17636nsplym8wk9m9dxa74jzkh57qcw")))

(define-public crate-e2size-1 (crate (name "e2size") (vers "1.0.0") (deps (list (crate-dep (name "byteorder") (req "^1.4.3") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.25") (default-features #t) (kind 0)))) (hash "1gkl2lp9mcgfg3a48klmlg2z466yjz75ah6ml7zgvgc2hx6acmak")))

(define-public crate-e2size-1 (crate (name "e2size") (vers "1.0.1") (deps (list (crate-dep (name "byteorder") (req "^1.4.3") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.25") (default-features #t) (kind 0)))) (hash "174x2i7zilrvinwnghwpizlkb48amx62lqp7hnjz4bslvfin8k3b")))

