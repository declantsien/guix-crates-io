(define-module (crates-io lt xc) #:use-module (crates-io))

(define-public crate-ltxcut-0.1 (crate (name "ltxcut") (vers "0.1.0") (deps (list (crate-dep (name "argparse") (req "^0.2.2") (default-features #t) (kind 0)) (crate-dep (name "string-builder") (req "^0.2.0") (default-features #t) (kind 0)))) (hash "1d2nx9s2b1w8nqbihvamjc95acbpjkrx52lg2ivz4a39c2wfb1ml")))

(define-public crate-ltxcut-0.1 (crate (name "ltxcut") (vers "0.1.1") (deps (list (crate-dep (name "argparse") (req "^0.2.2") (default-features #t) (kind 0)) (crate-dep (name "string-builder") (req "^0.2.0") (default-features #t) (kind 0)))) (hash "04kcrw8ww9771f5j21i8xw4gx471b76v8ry2jhhf3r7x9prayddz")))

