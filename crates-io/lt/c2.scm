(define-module (crates-io lt c2) #:use-module (crates-io))

(define-public crate-ltc2497-0.1 (crate (name "ltc2497") (vers "0.1.0") (deps (list (crate-dep (name "embedded-hal") (req "^0.2.7") (default-features #t) (kind 0)) (crate-dep (name "linux-embedded-hal") (req "^0.3.2") (default-features #t) (kind 2)) (crate-dep (name "rppal") (req "^0.14.1") (features (quote ("hal"))) (default-features #t) (kind 2)))) (hash "1xq4afzhp3fs5qa636z5nncam34jdy2qhh371nllrmhhkf29j5yv")))

(define-public crate-ltc2983-0.1 (crate (name "ltc2983") (vers "0.1.0") (deps (list (crate-dep (name "embedded-hal") (req "=1.0.0-alpha.9") (default-features #t) (kind 0)))) (hash "1r7rpvpchy8w1xr65p4rx4y3agn1023jmbxyma2bjp9fqvrlfjsn")))

(define-public crate-ltc2983-0.1 (crate (name "ltc2983") (vers "0.1.1") (deps (list (crate-dep (name "bytebuffer") (req "^2.1.0") (default-features #t) (kind 0)) (crate-dep (name "embedded-hal") (req "=1.0.0-alpha.9") (default-features #t) (kind 0)) (crate-dep (name "fixed") (req "^1.21.0") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.38") (default-features #t) (kind 0)))) (hash "15jw0lxa4q8ii4jqha603fxx2dykhv05x4bms2kcx0z6bsixps80")))

(define-public crate-ltc2983-0.1 (crate (name "ltc2983") (vers "0.1.2") (deps (list (crate-dep (name "bytebuffer") (req "^2.1.0") (default-features #t) (kind 0)) (crate-dep (name "embedded-hal") (req "=1.0.0-alpha.9") (default-features #t) (kind 0)) (crate-dep (name "fixed") (req "^1.21.0") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.38") (default-features #t) (kind 0)))) (hash "17vczxd2amrhg55bjr11gznpf6nj92qazhphphpq0h7r8aq22rxa")))

(define-public crate-ltc2983-0.2 (crate (name "ltc2983") (vers "0.2.0") (deps (list (crate-dep (name "bytebuffer") (req "^2.1.1") (default-features #t) (kind 0)) (crate-dep (name "embedded-hal") (req "=1.0.0-alpha.9") (default-features #t) (kind 0)) (crate-dep (name "fixed") (req "^1.21.0") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.38") (default-features #t) (kind 0)))) (hash "0b94f0icn26sivwg28inlm8dadzj0bpagv8zlbgfypn45yydfwfb")))

