(define-module (crates-io lt sv) #:use-module (crates-io))

(define-public crate-ltsv-0.1 (crate (name "ltsv") (vers "0.1.0") (hash "15ci5s6clqzqhrsv51kj1p72zs5amzxsff2s1zsqf8pzpzfwkgvf")))

(define-public crate-ltsv-0.1 (crate (name "ltsv") (vers "0.1.1") (hash "19a97wy4swjpvyssygaqdgmwrj9565s5216rjnyi80g4pfsvca6p")))

(define-public crate-ltsv-0.1 (crate (name "ltsv") (vers "0.1.2") (hash "1yj9f8ghxndnv3npgixqdig80fhhwdm7y10v5h2jv0yl248d9scv")))

(define-public crate-ltsv-0.1 (crate (name "ltsv") (vers "0.1.3") (hash "0b6731j6ylv16sh8ql9mdps81ysw19fh7jkrk8dl5fwfamjrzdii")))

(define-public crate-ltsv-0.1 (crate (name "ltsv") (vers "0.1.4") (hash "1hmjsg94lrjvyxs9gavy0xmcvkwc9bzz7zgpaghxpmv5rbaw164f")))

(define-public crate-ltsv-0.1 (crate (name "ltsv") (vers "0.1.5") (hash "1azdkji9na9bkb0k636dnqsr4852xnsxa2dw3xibzakiwq9vp6ds")))

