(define-module (crates-io lt pt) #:use-module (crates-io))

(define-public crate-ltptr-0.1 (crate (name "ltptr") (vers "0.1.0") (hash "0qqlx1jd05hgbh1vg9ald0rp3p6mcpzjggjx2n5hvvlj4rxhasky") (features (quote (("std") ("default" "std"))))))

(define-public crate-ltptr-0.1 (crate (name "ltptr") (vers "0.1.1") (hash "1lwxyg6r6r6l8czk1gdhichmrbwwab41k3r4vmgvhqbcrdbgdqfp") (features (quote (("std") ("default" "std"))))))

(define-public crate-ltptr-0.1 (crate (name "ltptr") (vers "0.1.2") (hash "1ra91gknk75z9kdj0y50fj2czis2mks5mgz5i6ml7amicmhjcc2k") (features (quote (("std") ("default" "std"))))))

(define-public crate-ltptr-0.1 (crate (name "ltptr") (vers "0.1.3") (hash "04w3mzjb2va2bgwj221zgf1a0vy4qxw0dim9b0agx7g2iljkrkck") (features (quote (("std") ("default" "std"))))))

(define-public crate-ltptr-0.1 (crate (name "ltptr") (vers "0.1.4") (hash "0b84xyzcrwc87ygq54xj1b2x2fx81by09kr218i7nkqbnkbazssn") (features (quote (("std") ("default" "std"))))))

