(define-module (crates-io lt tn) #:use-module (crates-io))

(define-public crate-lttng-ust-0.1 (crate (name "lttng-ust") (vers "0.1.0") (hash "14x54s0fxwb9iikb65j6rn5a8wcxqn7q3j7wdy2q6n86hc39l0ix")))

(define-public crate-lttng-ust-generate-0.1 (crate (name "lttng-ust-generate") (vers "0.1.0") (deps (list (crate-dep (name "bindgen") (req "^0.32.3") (default-features #t) (kind 0)) (crate-dep (name "cc") (req "^1.0") (default-features #t) (kind 0)))) (hash "0pdhgrgps3j5sd54760z565fww6bmyyw6zb6z2b3rbn0wdznmxf7")))

(define-public crate-lttng-ust-generate-0.1 (crate (name "lttng-ust-generate") (vers "0.1.1") (deps (list (crate-dep (name "bindgen") (req "^0.32.3") (default-features #t) (kind 0)) (crate-dep (name "cc") (req "^1.0") (default-features #t) (kind 0)))) (hash "15a4jv6zadnn12cw77ayigifncbsdv98ljf1i0zj7dd03pzhy8gh")))

(define-public crate-lttng-ust-logging-0.1 (crate (name "lttng-ust-logging") (vers "0.1.0") (deps (list (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "lttng-ust") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "lttng-ust-generate") (req "^0.1.1") (default-features #t) (kind 1)))) (hash "159paz11zjq1q9ixwd9dk22vbkgc3yg87lsjqdw53xnv0q6g7kf7")))

