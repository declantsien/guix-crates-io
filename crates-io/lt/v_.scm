(define-module (crates-io lt v_) #:use-module (crates-io))

(define-public crate-ltv_derive-0.1 (crate (name "ltv_derive") (vers "0.1.0") (deps (list (crate-dep (name "ltv") (req "^0.1") (default-features #t) (kind 2)) (crate-dep (name "proc-macro2") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1") (default-features #t) (kind 0)))) (hash "1qj2rx6y4dbyr6hladi5316c7ndxfyraapbigv89bg53m2ns14dz")))

(define-public crate-ltv_derive-0.1 (crate (name "ltv_derive") (vers "0.1.2") (deps (list (crate-dep (name "ltv") (req "^0.1") (default-features #t) (kind 2)) (crate-dep (name "ltv_derive_impl") (req "^0.1") (default-features #t) (kind 0)))) (hash "0axmb6mckwb8va6z4rl84xg6dzbhh9h0x8iqnbyh68ml8m5cmxad")))

(define-public crate-ltv_derive-0.1 (crate (name "ltv_derive") (vers "0.1.3") (deps (list (crate-dep (name "ltv") (req "^0.1") (default-features #t) (kind 2)) (crate-dep (name "ltv_derive_impl") (req "^0.1") (default-features #t) (kind 0)))) (hash "059fx405a7b8v5w8h4zwx6wcd5km353wk59qpim6sxp9wwr0r1s6")))

(define-public crate-ltv_derive-0.1 (crate (name "ltv_derive") (vers "0.1.4") (deps (list (crate-dep (name "ltv") (req "^0.1") (default-features #t) (kind 2)) (crate-dep (name "ltv_derive_impl") (req "^0.1") (default-features #t) (kind 0)))) (hash "05qijd9x8hn1ns2s6cpbr74rd355y76am92lal25gf6ykla41bbn")))

(define-public crate-ltv_derive-0.2 (crate (name "ltv_derive") (vers "0.2.1") (deps (list (crate-dep (name "ltv") (req "^0.2") (default-features #t) (kind 2)) (crate-dep (name "ltv_derive_impl") (req "^0.2") (default-features #t) (kind 0)))) (hash "18zy2qn446rzn950p1r11m28fhmxyfw1hxykq2lpz420b5qm4d5a")))

(define-public crate-ltv_derive-0.2 (crate (name "ltv_derive") (vers "0.2.2") (deps (list (crate-dep (name "ltv") (req "^0.2") (default-features #t) (kind 2)) (crate-dep (name "ltv_derive_impl") (req "^0.2.3") (default-features #t) (kind 0)))) (hash "0amqf4k6mnfil3dly09558ifsz0zbah82xrm9j8wn7bf1mj6kyxk")))

(define-public crate-ltv_derive-0.2 (crate (name "ltv_derive") (vers "0.2.3") (deps (list (crate-dep (name "ltv") (req "^0.2.9") (default-features #t) (kind 2)) (crate-dep (name "ltv_derive_impl") (req "^0.2.7") (default-features #t) (kind 0)))) (hash "0s6krwp4lacd942c3ij2pzd8cx6sfwh1nkj803hncr2v4v7gd4pq")))

(define-public crate-ltv_derive-0.2 (crate (name "ltv_derive") (vers "0.2.4") (deps (list (crate-dep (name "ltv") (req "^0.2.11") (default-features #t) (kind 2)) (crate-dep (name "ltv_derive_impl") (req "^0.2.8") (default-features #t) (kind 0)))) (hash "0aw44p5zmzn04z7n1ygy82qk7cl0hgi62y2bv6ilgz0llf1npqz2")))

(define-public crate-ltv_derive-0.2 (crate (name "ltv_derive") (vers "0.2.5") (deps (list (crate-dep (name "ltv") (req "^0.2.15") (default-features #t) (kind 2)) (crate-dep (name "ltv_derive_impl") (req "^0.2.9") (default-features #t) (kind 0)))) (hash "09hva5i8hlir078dlzsjnvcxi2zmbss26gihpy278mh2v5ngsd5i")))

(define-public crate-ltv_derive-0.2 (crate (name "ltv_derive") (vers "0.2.6") (deps (list (crate-dep (name "ltv") (req "^0.2.15") (default-features #t) (kind 2)) (crate-dep (name "ltv_derive_impl") (req "^0.2.10") (default-features #t) (kind 0)))) (hash "0xxb5fdd3w5im3b5khaa46vhx6aab3jdgkawajhcfalmv9b8qazv")))

(define-public crate-ltv_derive-0.2 (crate (name "ltv_derive") (vers "0.2.7") (deps (list (crate-dep (name "ltv") (req "^0.2.15") (default-features #t) (kind 2)) (crate-dep (name "ltv_derive_impl") (req "^0.2.11") (default-features #t) (kind 0)))) (hash "13biv08r4cwrc6sj1avxscxgcqy2wqk5d0zf82cq2c43gkn2glis")))

(define-public crate-ltv_derive-0.2 (crate (name "ltv_derive") (vers "0.2.8") (deps (list (crate-dep (name "ltv") (req "^0.2.15") (default-features #t) (kind 2)) (crate-dep (name "ltv_derive_impl") (req "^0.2.11") (default-features #t) (kind 0)))) (hash "0ig6rm6nwmw7vq4q2xh3jhzlwjwxy6ij0bmx8pmbyh9r7fv5i64j")))

(define-public crate-ltv_derive_impl-0.1 (crate (name "ltv_derive_impl") (vers "0.1.0") (deps (list (crate-dep (name "proc-macro2") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1") (default-features #t) (kind 0)))) (hash "1zxfjys0f4kh4mj3fkrzvrbbkwzw9d48pkplawglwfcdvb8fxm7i")))

(define-public crate-ltv_derive_impl-0.1 (crate (name "ltv_derive_impl") (vers "0.1.1") (deps (list (crate-dep (name "proc-macro2") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1") (default-features #t) (kind 0)))) (hash "0mqjgskqxzrmx4h3alyajhvk0xnixdf2qmq3ixr3a6fyhnbivp36")))

(define-public crate-ltv_derive_impl-0.1 (crate (name "ltv_derive_impl") (vers "0.1.2") (deps (list (crate-dep (name "proc-macro2") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1") (default-features #t) (kind 0)))) (hash "0lmy9dxmgnpvjfki4p36zk7aza1im42cnywxj2qac65swxgjkb64")))

(define-public crate-ltv_derive_impl-0.2 (crate (name "ltv_derive_impl") (vers "0.2.0") (deps (list (crate-dep (name "proc-macro2") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1") (default-features #t) (kind 0)))) (hash "1dbjh0b3l0p29v71qwfzrl2bnh4gi7nj5qjgb37pg4mm1xrpc81y")))

(define-public crate-ltv_derive_impl-0.2 (crate (name "ltv_derive_impl") (vers "0.2.1") (deps (list (crate-dep (name "proc-macro2") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1") (default-features #t) (kind 0)))) (hash "16y997sidyn2fnni4chxvmd3xvyrd23kx0gq4x4db2sfwpivifkl")))

(define-public crate-ltv_derive_impl-0.2 (crate (name "ltv_derive_impl") (vers "0.2.2") (deps (list (crate-dep (name "proc-macro2") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1") (default-features #t) (kind 0)))) (hash "15vr94kf0facim72qmyq8qxn0snkk37ybi8vqlmy15vyizjq6y1v")))

(define-public crate-ltv_derive_impl-0.2 (crate (name "ltv_derive_impl") (vers "0.2.3") (deps (list (crate-dep (name "proc-macro2") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1") (default-features #t) (kind 0)))) (hash "1mj88g4nvg7r8gb5lvy9zjw8lq3wpr76zngp4waa6vdjfydmwlxx")))

(define-public crate-ltv_derive_impl-0.2 (crate (name "ltv_derive_impl") (vers "0.2.7") (deps (list (crate-dep (name "proc-macro2") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1") (default-features #t) (kind 0)))) (hash "056gsj9pilzgcvv3cxgw0d2dl5csbp1128cbz57la4g9l330nn4c")))

(define-public crate-ltv_derive_impl-0.2 (crate (name "ltv_derive_impl") (vers "0.2.8") (deps (list (crate-dep (name "proc-macro2") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1") (default-features #t) (kind 0)))) (hash "170v28a76y9i1qyvgljn1givzd68h62iwygba2rbzf9k3fjrs417")))

(define-public crate-ltv_derive_impl-0.2 (crate (name "ltv_derive_impl") (vers "0.2.9") (deps (list (crate-dep (name "proc-macro2") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1") (default-features #t) (kind 0)))) (hash "113h30lpjv0pn7snmm9bzdqwmkxgl6553xc93zr9wgrabcvxvkyn")))

(define-public crate-ltv_derive_impl-0.2 (crate (name "ltv_derive_impl") (vers "0.2.10") (deps (list (crate-dep (name "proc-macro2") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1") (default-features #t) (kind 0)))) (hash "0i4086gr5d2xva9hzbql426fnz561nhvkxz77svb5v8mv9qfj712")))

(define-public crate-ltv_derive_impl-0.2 (crate (name "ltv_derive_impl") (vers "0.2.11") (deps (list (crate-dep (name "proc-macro2") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1") (default-features #t) (kind 0)))) (hash "0w5vnyb2f3l81r7gk4f809xwwh9iigl0zf6fw680b9xhz5vmiqw1")))

