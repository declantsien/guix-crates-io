(define-module (crates-io lt tb) #:use-module (crates-io))

(define-public crate-lttb-0.1 (crate (name "lttb") (vers "0.1.0") (hash "1ljhvly71x4j2kwsj9iv9z77n0l8l7y6z67hdnxvj41wh760jbff")))

(define-public crate-lttb-0.1 (crate (name "lttb") (vers "0.1.1") (hash "19pwi7pg1n6f00awr96n4mjb0ws5d7z2smz0z9i9xy23w26ah93v")))

(define-public crate-lttb-0.2 (crate (name "lttb") (vers "0.2.0") (hash "0j141c1z5gbv87wb1qi7jr0z7cys7ppcgwcj215pn5h4686pbz5b")))

