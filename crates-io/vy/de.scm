(define-module (crates-io vy de) #:use-module (crates-io))

(define-public crate-vyder-0.1 (crate (name "vyder") (vers "0.1.0") (deps (list (crate-dep (name "object-safe") (req "^0.3.1") (default-features #t) (kind 0)) (crate-dep (name "vyder_macros") (req "^0.1.1") (default-features #t) (kind 0)))) (hash "00ikwbhrwfw8j096h1wlxsii4fnfy810a7kbqb5f2jq9l565kgdi")))

(define-public crate-vyder_macros-0.1 (crate (name "vyder_macros") (vers "0.1.0") (deps (list (crate-dep (name "object-safe") (req "^0.3.1") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1.0.78") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.35") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2.0.48") (default-features #t) (kind 0)))) (hash "0b477shkq0y86g692rx0hk8jpz3h1imrvzh5170yn8difnrdcdar")))

(define-public crate-vyder_macros-0.1 (crate (name "vyder_macros") (vers "0.1.1") (deps (list (crate-dep (name "object-safe") (req "^0.3.1") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1.0.78") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.35") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2.0.48") (default-features #t) (kind 0)))) (hash "0pq66n8pv1hzr7ibrl1cm6yxnwz75jpqi3vd3rckp4kmc523d0n2")))

