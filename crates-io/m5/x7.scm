(define-module (crates-io m5 x7) #:use-module (crates-io))

(define-public crate-m5x7-1 (crate (name "m5x7") (vers "1.0.0") (deps (list (crate-dep (name "lazy_static") (req "^1.4.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "piston2d-opengl_graphics") (req "^0.70.0") (default-features #t) (kind 2)) (crate-dep (name "rusttype") (req "^0.8.1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "rusttype") (req "^0.8.1") (default-features #t) (kind 2)))) (hash "02mhd52n81f9xghxapplzirjnnww1a51f4i3v06b70mp308s4ky4") (features (quote (("parsed" "rusttype" "lazy_static") ("default"))))))

