(define-module (crates-io na sm) #:use-module (crates-io))

(define-public crate-nasm-0.0.0 (crate (name "nasm") (vers "0.0.0") (hash "011wchl78h9zy0idhngf5d22h75qf8sprp7n3cqbdixa9042hk07")))

(define-public crate-nasm-0.0.1 (crate (name "nasm") (vers "0.0.1") (hash "0dl5g0ss54nnx4xvyyvjk8bjxixjmpganiy44znvcfvsyi6hlk79")))

(define-public crate-nasm-rs-0.0.2 (crate (name "nasm-rs") (vers "0.0.2") (hash "17k3q3zviwjzl3nq6qw465iwi7kw9ds07amgryhbz7986b889p82") (yanked #t)))

(define-public crate-nasm-rs-0.0.3 (crate (name "nasm-rs") (vers "0.0.3") (hash "0mypd6105lk818nxfn3db1qmr09bk310b6j6gl2v5swciyjlk10l")))

(define-public crate-nasm-rs-0.0.4 (crate (name "nasm-rs") (vers "0.0.4") (hash "1mi6g307di7fz5py5rjmlizg5asmcfwgh4ggwhli86ppj2v7jwar")))

(define-public crate-nasm-rs-0.0.5 (crate (name "nasm-rs") (vers "0.0.5") (hash "1imc3q0cswjlmvh1mzj1rm2mqxalvl53h07wbrfjvkzdq19s6i03")))

(define-public crate-nasm-rs-0.0.6 (crate (name "nasm-rs") (vers "0.0.6") (hash "0rdqh64xivj0jfdw5hfxcpjzfjkk8kfp6phxvdvpy2yybkhm866s")))

(define-public crate-nasm-rs-0.0.7 (crate (name "nasm-rs") (vers "0.0.7") (hash "0xmkz70s3b776fy1w99fqx6fdpzcak451ldwxfmkyy0k7p8kibw9")))

(define-public crate-nasm-rs-0.1 (crate (name "nasm-rs") (vers "0.1.0") (deps (list (crate-dep (name "rayon") (req "^0.8.2") (optional #t) (default-features #t) (kind 0)))) (hash "0db8vvq3s5wksfld1831c6hkhh1bl93lynb6m5cbhqq2d3sf7ssk") (features (quote (("parallel" "rayon"))))))

(define-public crate-nasm-rs-0.1 (crate (name "nasm-rs") (vers "0.1.1") (deps (list (crate-dep (name "rayon") (req "^0.8.2") (optional #t) (default-features #t) (kind 0)))) (hash "1yxlakw88ip52h14zy6758m56n0dp9i6518c7c747ccahw6x4zd3") (features (quote (("parallel" "rayon"))))))

(define-public crate-nasm-rs-0.1 (crate (name "nasm-rs") (vers "0.1.2") (deps (list (crate-dep (name "rayon") (req "^0.9") (optional #t) (default-features #t) (kind 0)))) (hash "1n4nz397nzf2fxd03yk2ywzpdly1da91lgx4irrf89j7wnwkda0m") (features (quote (("parallel" "rayon"))))))

(define-public crate-nasm-rs-0.1 (crate (name "nasm-rs") (vers "0.1.3") (deps (list (crate-dep (name "rayon") (req "^1.0") (optional #t) (default-features #t) (kind 0)))) (hash "1266k2bwhh3z2c27g06n4inywf6b4iwp87hvz7dl06rsykijq2q5") (features (quote (("parallel" "rayon"))))))

(define-public crate-nasm-rs-0.1 (crate (name "nasm-rs") (vers "0.1.4") (deps (list (crate-dep (name "rayon") (req "^1.0") (optional #t) (default-features #t) (kind 0)))) (hash "044yc3z8j4vls2spf7msph0vxh4vlh0l9854k058kh9zjw0gficn") (features (quote (("parallel" "rayon"))))))

(define-public crate-nasm-rs-0.1 (crate (name "nasm-rs") (vers "0.1.5") (deps (list (crate-dep (name "rayon") (req "^1.0") (optional #t) (default-features #t) (kind 0)))) (hash "0mvyvkr9m5ablbmr43rc9qh8jk7vp2gimq1igvjr05zlkn66saw8") (features (quote (("parallel" "rayon"))))))

(define-public crate-nasm-rs-0.1 (crate (name "nasm-rs") (vers "0.1.6") (deps (list (crate-dep (name "rayon") (req "^1.0") (optional #t) (default-features #t) (kind 0)))) (hash "0vsvj3l6v1bb6d6p1mn9llpr3byqs6r2p6308jlsmj459i4s9k5j") (features (quote (("parallel" "rayon"))))))

(define-public crate-nasm-rs-0.1 (crate (name "nasm-rs") (vers "0.1.7") (deps (list (crate-dep (name "rayon") (req "^1.0") (optional #t) (default-features #t) (kind 0)))) (hash "0r34hiy1pc0aksrfc02zsl0zyw33i9yi7kyx8l214l7nm0mzm97y") (features (quote (("parallel" "rayon"))))))

(define-public crate-nasm-rs-0.1 (crate (name "nasm-rs") (vers "0.1.8") (deps (list (crate-dep (name "rayon") (req "^1.0") (optional #t) (default-features #t) (kind 0)))) (hash "0d5mr6fjhs59gfxb5hyhah3sgp6jlii1q35g8dlxgkrj58bpfbnk") (features (quote (("parallel" "rayon"))))))

(define-public crate-nasm-rs-0.2 (crate (name "nasm-rs") (vers "0.2.0") (deps (list (crate-dep (name "arrayvec") (req "^0.5") (default-features #t) (kind 0)) (crate-dep (name "rayon") (req "^1.0") (optional #t) (default-features #t) (kind 0)))) (hash "1lgc3gg32hj4pcbfp07vzwy013smdm27469fyy4rqgyil3x46vx7") (features (quote (("parallel" "rayon"))))))

(define-public crate-nasm-rs-0.2 (crate (name "nasm-rs") (vers "0.2.1") (deps (list (crate-dep (name "rayon") (req "^1.4") (optional #t) (default-features #t) (kind 0)))) (hash "1aghasvrs6vwrzjzfvi80ry9caj10hsjr3k0x03v937fs9mzigwx") (features (quote (("parallel" "rayon"))))))

(define-public crate-nasm-rs-0.2 (crate (name "nasm-rs") (vers "0.2.2") (deps (list (crate-dep (name "rayon") (req "^1.4") (optional #t) (default-features #t) (kind 0)))) (hash "056nrnwjxhl5bxfjflb8szki0cdd1jaw6dpsjbwdmp2q7g980qx0") (features (quote (("parallel" "rayon"))))))

(define-public crate-nasm-rs-0.2 (crate (name "nasm-rs") (vers "0.2.3") (deps (list (crate-dep (name "rayon") (req "^1.4") (optional #t) (default-features #t) (kind 0)))) (hash "0whf6wwyc3zgh45rwxj9b0yccsd9hd9077bz3pzvhidhhi894cgh") (features (quote (("parallel" "rayon"))))))

(define-public crate-nasm-rs-0.2 (crate (name "nasm-rs") (vers "0.2.4") (deps (list (crate-dep (name "rayon") (req "^1.4") (optional #t) (default-features #t) (kind 0)))) (hash "10zl67i9gr7qarmnnnd8538mydw0yr6jlpbsvb5kxap9mr15h2ff") (features (quote (("parallel" "rayon"))))))

(define-public crate-nasm-rs-0.2 (crate (name "nasm-rs") (vers "0.2.5") (deps (list (crate-dep (name "rayon") (req "^1.4") (optional #t) (default-features #t) (kind 0)))) (hash "0lfs2xfbpl1j7zq6qfg2wmi4djbl36qsygjb2spisjsz0v89hkgy") (features (quote (("parallel" "rayon"))))))

(define-public crate-nasm-rs-0.3 (crate (name "nasm-rs") (vers "0.3.0") (deps (list (crate-dep (name "jobserver") (req "^0.1") (optional #t) (default-features #t) (kind 0)))) (hash "0nfrmnfvc1rcpghi14zbrdx3x5jr7gl2pv873pn440wyshdzmz0j") (features (quote (("parallel" "jobserver"))))))

(define-public crate-nasmi-0.0.0 (crate (name "nasmi") (vers "0.0.0") (hash "0ipyqazfl3pqh5jqhzmm7l0n7zimjx1k4bhmlkzwwlm66nmlg4yh")))

