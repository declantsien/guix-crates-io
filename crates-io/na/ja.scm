(define-module (crates-io na ja) #:use-module (crates-io))

(define-public crate-naja_async_runtime-0.1 (crate (name "naja_async_runtime") (vers "0.1.0") (deps (list (crate-dep (name "failure") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "futures-preview") (req "^0.3.0-alpha.16") (features (quote ("std" "compat" "nightly"))) (kind 0)) (crate-dep (name "juliex") (req "^0.3.0-alpha.6") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "once_cell") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "pretty_assertions") (req "^0.6") (default-features #t) (kind 2)) (crate-dep (name "wasm-bindgen-futures") (req "^0.3.22") (features (quote ("futures_0_3"))) (default-features #t) (target "cfg(target_arch = \"wasm32\")") (kind 0)) (crate-dep (name "wasm-bindgen-test") (req "^0.2") (default-features #t) (target "cfg(target_arch = \"wasm32\")") (kind 2)))) (hash "04inwxf52anllrs769j0qzsc2v6ygvlh5x1i39j1vyxlfvx1ja8s") (features (quote (("default" "juliex"))))))

(define-public crate-naja_async_runtime-0.1 (crate (name "naja_async_runtime") (vers "0.1.1") (deps (list (crate-dep (name "failure") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "futures-preview") (req "^0.3.0-alpha.16") (features (quote ("std" "compat" "nightly"))) (kind 0)) (crate-dep (name "juliex") (req "^0.3.0-alpha.6") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "once_cell") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "pretty_assertions") (req "^0.6") (default-features #t) (kind 2)) (crate-dep (name "wasm-bindgen-futures") (req "^0.3.22") (features (quote ("futures_0_3"))) (default-features #t) (target "cfg(target_arch = \"wasm32\")") (kind 0)) (crate-dep (name "wasm-bindgen-test") (req "^0.2") (default-features #t) (target "cfg(target_arch = \"wasm32\")") (kind 2)))) (hash "0lkmzz2gxw49qb98j2k3lil9j4nf4dizlyy9c3xq7ccfyw3k56dg") (features (quote (("default" "juliex"))))))

(define-public crate-naja_async_runtime-0.1 (crate (name "naja_async_runtime") (vers "0.1.2") (deps (list (crate-dep (name "async_runtime") (req "^0.1") (default-features #t) (kind 0) (package "naja_async_runtime")) (crate-dep (name "failure") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "futures-preview") (req "^0.3.0-alpha.16") (features (quote ("std" "compat" "nightly"))) (kind 0)) (crate-dep (name "juliex") (req "^0.3.0-alpha.7") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "once_cell") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "pretty_assertions") (req "^0.6") (default-features #t) (kind 2)) (crate-dep (name "wasm-bindgen-futures") (req "^0.3.22") (features (quote ("futures_0_3"))) (default-features #t) (target "cfg(target_arch = \"wasm32\")") (kind 0)) (crate-dep (name "wasm-bindgen-test") (req "^0.2") (default-features #t) (target "cfg(target_arch = \"wasm32\")") (kind 2)))) (hash "1x620q1ch7hvcpjygjjcqla76ichqccazavhldawcj7fkpjy701k") (features (quote (("default" "juliex"))))))

(define-public crate-naja_async_runtime-0.1 (crate (name "naja_async_runtime") (vers "0.1.3") (deps (list (crate-dep (name "failure") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "futures-preview") (req "^0.3.0-alpha.16") (features (quote ("std" "compat" "nightly"))) (kind 0)) (crate-dep (name "juliex") (req "^0.3.0-alpha.7") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "once_cell") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "pretty_assertions") (req "^0.6") (default-features #t) (kind 2)) (crate-dep (name "wasm-bindgen-futures") (req "^0.3.22") (features (quote ("futures_0_3"))) (default-features #t) (target "cfg(target_arch = \"wasm32\")") (kind 0)) (crate-dep (name "wasm-bindgen-test") (req "^0.2") (default-features #t) (target "cfg(target_arch = \"wasm32\")") (kind 2)))) (hash "0vci11flamy6104d5kj8anqrzcf4f5nxkwbli6knm4p262l35g1i") (features (quote (("default" "juliex"))))))

(define-public crate-naja_async_runtime-0.1 (crate (name "naja_async_runtime") (vers "0.1.4") (deps (list (crate-dep (name "failure") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "futures-preview") (req "^0.3.0-alpha.16") (features (quote ("std" "compat" "nightly"))) (kind 0)) (crate-dep (name "juliex") (req "^0.3.0-alpha.7") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "once_cell") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "pretty_assertions") (req "^0.6") (default-features #t) (kind 2)) (crate-dep (name "wasm-bindgen-futures") (req "^0.3.22") (features (quote ("futures_0_3"))) (default-features #t) (target "cfg(target_arch = \"wasm32\")") (kind 0)) (crate-dep (name "wasm-bindgen-test") (req "^0.2") (default-features #t) (target "cfg(target_arch = \"wasm32\")") (kind 2)))) (hash "1jv4iw14n8sbb4f3fs76bvh846aci25hz51lv7xa1dljlcrx7shi") (features (quote (("default" "juliex"))))))

(define-public crate-naja_async_runtime-0.1 (crate (name "naja_async_runtime") (vers "0.1.5") (deps (list (crate-dep (name "failure") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "futures-preview") (req "^0.3.0-alpha.16") (features (quote ("std" "compat" "nightly"))) (kind 0)) (crate-dep (name "juliex") (req "^0.3.0-alpha.7") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "once_cell") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "pretty_assertions") (req "^0.6") (default-features #t) (kind 2)) (crate-dep (name "wasm-bindgen-futures") (req "^0.3.22") (features (quote ("futures_0_3"))) (default-features #t) (target "cfg(target_arch = \"wasm32\")") (kind 0)) (crate-dep (name "wasm-bindgen-test") (req "^0.2") (default-features #t) (target "cfg(target_arch = \"wasm32\")") (kind 2)))) (hash "0jvaszrj7bxzzisdhbglvl1pnc4zgryd383zg7knvr02qhhvwzbx") (features (quote (("default" "juliex"))))))

(define-public crate-naja_async_runtime-0.1 (crate (name "naja_async_runtime") (vers "0.1.6") (deps (list (crate-dep (name "failure") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "futures-preview") (req "= 0.3.0-alpha.16") (features (quote ("std" "compat" "nightly"))) (kind 0)) (crate-dep (name "juliex") (req "^0.3.0-alpha.7") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "once_cell") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "pretty_assertions") (req "^0.6") (default-features #t) (kind 2)) (crate-dep (name "wasm-bindgen-futures") (req "^0.3.24") (features (quote ("futures_0_3"))) (default-features #t) (target "cfg(target_arch = \"wasm32\")") (kind 0)) (crate-dep (name "wasm-bindgen-test") (req "^0.2") (default-features #t) (target "cfg(target_arch = \"wasm32\")") (kind 2)))) (hash "1yqc302l566hb5gyf0i4q5ivrgjbjc8bq48sikdhxkmfjwy93wsf") (features (quote (("default" "juliex"))))))

(define-public crate-naja_async_runtime-0.1 (crate (name "naja_async_runtime") (vers "0.1.7") (deps (list (crate-dep (name "failure") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "futures-preview") (req "^0.3.0-alpha.17") (features (quote ("std" "compat" "nightly"))) (kind 0)) (crate-dep (name "juliex") (req "^0.3.0-alpha.7") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "once_cell") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "pretty_assertions") (req "^0.6") (default-features #t) (kind 2)) (crate-dep (name "wasm-bindgen-futures") (req "^0.3.24") (features (quote ("futures_0_3"))) (default-features #t) (target "cfg(target_arch = \"wasm32\")") (kind 0)) (crate-dep (name "wasm-bindgen-test") (req "^0.2") (default-features #t) (target "cfg(target_arch = \"wasm32\")") (kind 2)))) (hash "04cd8qbfgsk1izz4j2r68g99ihmr30d6i2pp8qadm1v3jk619ajv") (features (quote (("default" "juliex"))))))

(define-public crate-naja_async_runtime-0.2 (crate (name "naja_async_runtime") (vers "0.2.0") (deps (list (crate-dep (name "failure") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "futures-preview") (req "^0.3.0-alpha.17") (features (quote ("std" "compat" "nightly"))) (kind 0)) (crate-dep (name "juliex") (req "^0.3.0-alpha.7") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "once_cell") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "pretty_assertions") (req "^0.6") (default-features #t) (kind 2)) (crate-dep (name "wasm-bindgen-futures") (req "^0.3.24") (features (quote ("futures_0_3"))) (default-features #t) (target "cfg(target_arch = \"wasm32\")") (kind 0)) (crate-dep (name "wasm-bindgen-test") (req "^0.2") (default-features #t) (target "cfg(target_arch = \"wasm32\")") (kind 2)))) (hash "01np6r97b827yxgkjx245b31560fmp9bz78khh167m319dni6jrz") (features (quote (("default" "juliex"))))))

(define-public crate-naja_async_runtime-0.2 (crate (name "naja_async_runtime") (vers "0.2.1") (deps (list (crate-dep (name "failure") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "futures-preview") (req "^0.3.0-alpha.17") (features (quote ("std" "compat" "nightly"))) (kind 0)) (crate-dep (name "juliex") (req "^0.3.0-alpha.7") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "once_cell") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "pretty_assertions") (req "^0.6") (default-features #t) (kind 2)) (crate-dep (name "wasm-bindgen-futures") (req "^0.3.24") (features (quote ("futures_0_3"))) (default-features #t) (target "cfg(target_arch = \"wasm32\")") (kind 0)) (crate-dep (name "wasm-bindgen-test") (req "^0.2") (default-features #t) (target "cfg(target_arch = \"wasm32\")") (kind 2)))) (hash "1ymj94gm0lfj7j5svfq4j7viqqlyxjcawi3bs038bqnr23vvypj5") (features (quote (("default" "juliex"))))))

(define-public crate-naja_async_runtime-0.3 (crate (name "naja_async_runtime") (vers "0.3.0") (deps (list (crate-dep (name "failure") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "futures-preview") (req "^0.3.0-alpha.17") (features (quote ("std" "compat" "nightly"))) (kind 0)) (crate-dep (name "juliex") (req "^0.3.0-alpha.7") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "once_cell") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "pretty_assertions") (req "^0.6") (default-features #t) (kind 2)) (crate-dep (name "wasm-bindgen-futures") (req "^0.3.24") (features (quote ("futures_0_3"))) (default-features #t) (target "cfg(target_arch = \"wasm32\")") (kind 0)) (crate-dep (name "wasm-bindgen-test") (req "^0.2") (default-features #t) (target "cfg(target_arch = \"wasm32\")") (kind 2)))) (hash "13rqcjn0pvgk7wdbkxa7r77qfpcc7pr5ciih8pii6d6vzl4svl89") (features (quote (("default" "juliex"))))))

(define-public crate-naja_async_runtime-0.3 (crate (name "naja_async_runtime") (vers "0.3.1") (deps (list (crate-dep (name "failure") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "futures-preview") (req "^0.3.0-alpha") (features (quote ("std" "compat"))) (kind 0)) (crate-dep (name "juliex") (req "^0.3.0-alpha") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "once_cell") (req "^0.2") (kind 0)) (crate-dep (name "pretty_assertions") (req "^0.6") (default-features #t) (kind 2)) (crate-dep (name "wasm-bindgen-futures") (req "^0.4") (default-features #t) (target "cfg(target_arch = \"wasm32\")") (kind 0)) (crate-dep (name "wasm-bindgen-test") (req "^0.3") (default-features #t) (target "cfg(target_arch = \"wasm32\")") (kind 2)))) (hash "13ng40nbw3ss802300bpvbwz0h97sr4gvpklkrl6zf2p31h47p22") (features (quote (("default" "juliex"))))))

(define-public crate-naja_async_runtime-0.3 (crate (name "naja_async_runtime") (vers "0.3.2") (deps (list (crate-dep (name "failure") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "futures-preview") (req "^0.3.0-alpha") (features (quote ("std" "compat"))) (kind 0)) (crate-dep (name "juliex") (req "^0.3.0-alpha") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "once_cell") (req "^0.2") (kind 0)) (crate-dep (name "pretty_assertions") (req "^0.6") (default-features #t) (kind 2)) (crate-dep (name "wasm-bindgen-futures") (req "^0.4") (default-features #t) (target "cfg(target_arch = \"wasm32\")") (kind 0)) (crate-dep (name "wasm-bindgen-test") (req "^0.3") (default-features #t) (target "cfg(target_arch = \"wasm32\")") (kind 2)))) (hash "13zph7bgc8vbm3r46mq8dr99i0w6fdpanmgqf5kfdjjny89b4s3p") (features (quote (("default" "juliex"))))))

(define-public crate-naja_async_runtime-0.3 (crate (name "naja_async_runtime") (vers "0.3.3") (deps (list (crate-dep (name "failure") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "futures-preview") (req "^0.3.0-alpha") (features (quote ("std" "compat"))) (kind 0)) (crate-dep (name "juliex") (req "^0.3.0-alpha") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "once_cell") (req "^0.2") (kind 0)) (crate-dep (name "pretty_assertions") (req "^0.6") (default-features #t) (kind 2)) (crate-dep (name "wasm-bindgen-futures") (req "^0.4") (default-features #t) (target "cfg(target_arch = \"wasm32\")") (kind 0)) (crate-dep (name "wasm-bindgen-test") (req "^0.3") (default-features #t) (target "cfg(target_arch = \"wasm32\")") (kind 2)))) (hash "00gkrpg9xq5hmsxi0bj9jfasaz9dl87pdx3zj8qfzal27qpih75l") (features (quote (("external_doc") ("default" "juliex"))))))

(define-public crate-naja_async_runtime-0.3 (crate (name "naja_async_runtime") (vers "0.3.4") (deps (list (crate-dep (name "failure") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "futures-preview") (req "^0.3.0-alpha") (features (quote ("std" "compat"))) (kind 0)) (crate-dep (name "juliex") (req "^0.3.0-alpha") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "once_cell") (req "^0.2") (kind 0)) (crate-dep (name "pretty_assertions") (req "^0.6") (default-features #t) (kind 2)) (crate-dep (name "wasm-bindgen-futures") (req "^0.4") (default-features #t) (target "cfg(target_arch = \"wasm32\")") (kind 0)) (crate-dep (name "wasm-bindgen-test") (req "^0.3") (default-features #t) (target "cfg(target_arch = \"wasm32\")") (kind 2)))) (hash "0yap32c8a1cwr0g8cqq6kly7sr2nfn0b3l5mznfx3282kksqcdyr") (features (quote (("external_doc") ("default" "juliex"))))))

(define-public crate-naja_async_runtime-0.5 (crate (name "naja_async_runtime") (vers "0.5.0-deprecated.1") (deps (list (crate-dep (name "async_std_crate") (req "^1") (optional #t) (default-features #t) (kind 0) (package "async-std")) (crate-dep (name "async_std_crate") (req "^1") (default-features #t) (target "cfg(not(target_arch = \"wasm32\"))") (kind 2) (package "async-std")) (crate-dep (name "futures") (req "^0.3") (features (quote ("std" "executor"))) (kind 0)) (crate-dep (name "juliex_crate") (req "^0.3.0-alpha") (optional #t) (default-features #t) (kind 0) (package "juliex")) (crate-dep (name "once_cell") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "pretty_assertions") (req "^0.6") (default-features #t) (kind 2)) (crate-dep (name "tokio") (req "^0.2.0-alpha") (features (quote ("rt-full"))) (optional #t) (kind 0)) (crate-dep (name "wasm-bindgen-futures") (req "^0.4") (default-features #t) (target "cfg(target_arch = \"wasm32\")") (kind 0)) (crate-dep (name "wasm-bindgen-futures") (req "^0.4") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "wasm-bindgen-test") (req "^0.3") (default-features #t) (target "cfg(target_arch = \"wasm32\")") (kind 2)))) (hash "17xdxk1malmmzwpzwgszxwj8lgddv428zx85dqqaj0r5xy3n0xmx") (features (quote (("tokio_ct" "tokio") ("threadpool" "futures/thread-pool") ("notwasm") ("localpool") ("juliex" "juliex_crate") ("external_doc") ("default" "notwasm") ("bindgen" "wasm-bindgen-futures") ("async_std" "async_std_crate"))))))

