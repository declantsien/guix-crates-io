(define-module (crates-io na ca) #:use-module (crates-io))

(define-public crate-nacafoil-0.0.1 (crate (name "nacafoil") (vers "0.0.1") (hash "09bkp0x07l8algjgw6m6gjp87zrd3g4x7gydz0215p4zl7g2sv6v")))

(define-public crate-nacafoil-0.0.2 (crate (name "nacafoil") (vers "0.0.2") (hash "012dwbagfcsyxh3ibd9pjx6bp7ymlx0s11v0n1afdc3jgbndkv8z")))

(define-public crate-nacafoil-0.0.3 (crate (name "nacafoil") (vers "0.0.3") (hash "1qzxlf00jxhb370ms0nh9xygrj43n0gnw9nxj3hksqhpihz7m4nb")))

(define-public crate-nacafoil-0.0.4 (crate (name "nacafoil") (vers "0.0.4") (hash "05005c82rm6asg26i44k9q8z9f5x4mfjy10fj7fxjiy59awrs50f")))

(define-public crate-nacafoil-0.0.5 (crate (name "nacafoil") (vers "0.0.5") (deps (list (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 2)) (crate-dep (name "serde_json") (req "^1.0.96") (default-features #t) (kind 2)))) (hash "0rvka0xrykba40kr9hf5bw86fza41n6xnavkdyrbdmb5x08vjh0p")))

