(define-module (crates-io na nv) #:use-module (crates-io))

(define-public crate-nanval-0.1 (crate (name "nanval") (vers "0.1.0") (hash "1bnpp4fm1xpxdf6ym76lgnnbdx6b69kyfbwnccrjlral2gzywzlj")))

(define-public crate-nanval-0.1 (crate (name "nanval") (vers "0.1.1") (hash "1jib52krn36my57skrdchpd6anh554iiapw1wmaidn2zv89k3gcx")))

(define-public crate-nanval-0.2 (crate (name "nanval") (vers "0.2.0") (hash "1vpl6z4j13zgkk3rxy5h9c65c9d993d402p9rds418sgai9lh78w") (features (quote (("std") ("default" "std" "cell") ("cell"))))))

(define-public crate-nanval-0.2 (crate (name "nanval") (vers "0.2.1") (hash "1hxsnp7dclh65irpdnnb15fmn9xchkabn3lab9rwv9cq8yv3q2il") (features (quote (("std") ("default" "std" "cell") ("cell"))))))

(define-public crate-nanvm-0.0.0 (crate (name "nanvm") (vers "0.0.0") (hash "1x9jikvl0609iv57gjkyig5sif90k7jv0xriqdry5pla2fa0pz5v")))

(define-public crate-nanvm-0.0.1 (crate (name "nanvm") (vers "0.0.1") (deps (list (crate-dep (name "io-impl") (req "^0.8.0") (default-features #t) (kind 0)) (crate-dep (name "nanvm-lib") (req "^0.0.1") (default-features #t) (kind 0)))) (hash "07mpbgxj10b9nwc0acpp31asfr671mbrj429cwwcz68xc464w5q8")))

(define-public crate-nanvm-0.0.2 (crate (name "nanvm") (vers "0.0.2") (deps (list (crate-dep (name "io-impl") (req "^0.9.0") (default-features #t) (kind 0)) (crate-dep (name "nanvm-lib") (req "^0.0.2") (default-features #t) (kind 0)))) (hash "0xqndprqkllmjvzlfxvp71ghh391zy07d5abvgvqr69qjid8hvrx")))

(define-public crate-nanvm-0.0.3 (crate (name "nanvm") (vers "0.0.3") (deps (list (crate-dep (name "io-impl") (req "^0.10.0") (default-features #t) (kind 0)) (crate-dep (name "nanvm-lib") (req "^0.0.3") (default-features #t) (kind 0)))) (hash "0an5iw50dria1wsswxwjzl3czhng9z4ilz8bnr7gslnwnmybiv3f")))

(define-public crate-nanvm-0.0.4 (crate (name "nanvm") (vers "0.0.4") (deps (list (crate-dep (name "io-impl") (req "^0.10.0") (default-features #t) (kind 0)) (crate-dep (name "nanvm-lib") (req "^0.0.4") (default-features #t) (kind 0)))) (hash "002fwq4mvdsz4vqrn793ck8wl5czd3hmg1p849fb80vbw9iabxzs")))

(define-public crate-nanvm-0.0.5 (crate (name "nanvm") (vers "0.0.5") (deps (list (crate-dep (name "io-impl") (req "^0.11.0") (default-features #t) (kind 0)) (crate-dep (name "io-trait") (req "^0.11.0") (default-features #t) (kind 0)) (crate-dep (name "nanvm-lib") (req "^0.0.5") (default-features #t) (kind 0)))) (hash "1y9jchpiwyvamv0gksayhvilabayjqxn9ydiipfmqgxbq8wiflqm")))

(define-public crate-nanvm-lib-0.0.0 (crate (name "nanvm-lib") (vers "0.0.0") (deps (list (crate-dep (name "wasm-bindgen-test") (req "^0.3.39") (default-features #t) (kind 2)))) (hash "1fdimcmchyqp38by2j9iymzbv28g3m61s5dnzx7h40x6dd1qpkas")))

(define-public crate-nanvm-lib-0.0.1 (crate (name "nanvm-lib") (vers "0.0.1") (deps (list (crate-dep (name "io-test") (req "^0.8.0") (default-features #t) (kind 2)) (crate-dep (name "io-trait") (req "^0.8.0") (default-features #t) (kind 0)) (crate-dep (name "wasm-bindgen-test") (req "^0.3.39") (default-features #t) (kind 2)))) (hash "0qxzaj6z6g5q5wzz6wsk5bqfg2fah4z3zs4cc8vixqgirg59dkip")))

(define-public crate-nanvm-lib-0.0.2 (crate (name "nanvm-lib") (vers "0.0.2") (deps (list (crate-dep (name "io-test") (req "^0.9.0") (default-features #t) (kind 2)) (crate-dep (name "io-trait") (req "^0.9.0") (default-features #t) (kind 0)) (crate-dep (name "wasm-bindgen-test") (req "^0.3.41") (default-features #t) (kind 2)))) (hash "0xvj2qvxybp83ddiq54d61sjl526ywxms30yalr5w7cw9fy656fy")))

(define-public crate-nanvm-lib-0.0.3 (crate (name "nanvm-lib") (vers "0.0.3") (deps (list (crate-dep (name "io-test") (req "^0.10.0") (default-features #t) (kind 2)) (crate-dep (name "io-trait") (req "^0.10.0") (default-features #t) (kind 0)) (crate-dep (name "wasm-bindgen-test") (req "^0.3.42") (default-features #t) (kind 2)))) (hash "0503r79hqj3i3rjbw27b9rachf68addsw7icyw4k9ina0w4r978r")))

(define-public crate-nanvm-lib-0.0.4 (crate (name "nanvm-lib") (vers "0.0.4") (deps (list (crate-dep (name "io-test") (req "^0.10.0") (default-features #t) (kind 2)) (crate-dep (name "io-trait") (req "^0.10.0") (default-features #t) (kind 0)) (crate-dep (name "wasm-bindgen-test") (req "^0.3.42") (default-features #t) (kind 2)))) (hash "0dnp3dk0sz4c33dasak8g3x4lapfriq36gv42qzc41p7f8hb90ms")))

(define-public crate-nanvm-lib-0.0.5 (crate (name "nanvm-lib") (vers "0.0.5") (deps (list (crate-dep (name "io-test") (req "^0.11.0") (default-features #t) (kind 2)) (crate-dep (name "io-trait") (req "^0.11.0") (default-features #t) (kind 0)) (crate-dep (name "wasm-bindgen-test") (req "^0.3.42") (default-features #t) (kind 2)))) (hash "1ps04md5z6mcszna1nl4gi9izmcgamsqyc3pb71rx8abgjxm0x4q")))

