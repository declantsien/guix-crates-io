(define-module (crates-io na nb) #:use-module (crates-io))

(define-public crate-nanbox-0.1 (crate (name "nanbox") (vers "0.1.0") (deps (list (crate-dep (name "quickcheck") (req "^0.4.1") (default-features #t) (kind 2)) (crate-dep (name "unreachable") (req "^0.1.1") (default-features #t) (kind 0)))) (hash "0dayavngpmz4y6cwkphfgiqxjxlln1zcpnb4b9w0pmzmknmlq4mx")))

(define-public crate-nanbox-0.2 (crate (name "nanbox") (vers "0.2.0") (deps (list (crate-dep (name "quickcheck") (req "^0.4.1") (default-features #t) (kind 2)) (crate-dep (name "unreachable") (req "^0.1.1") (default-features #t) (kind 0)))) (hash "0kc6vzcb55v2bdhb9maxs8r9wngfrxvw3s8a74chlbb1v660xwm3")))

