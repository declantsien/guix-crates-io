(define-module (crates-io na to) #:use-module (crates-io))

(define-public crate-nato-0.1 (crate (name "nato") (vers "0.1.0") (hash "0nwwnn0f807kadka0ls135127gkw3fiy1j91r7b1wdxidlv200x8")))

(define-public crate-nato-0.2 (crate (name "nato") (vers "0.2.0") (hash "1ax891i74bhrcaga4mx52y04ajy5pvxjkavb3cffyigq36626jlx")))

(define-public crate-natom-0.1 (crate (name "natom") (vers "0.1.0") (deps (list (crate-dep (name "ctor") (req "^0.2.2") (default-features #t) (kind 0)) (crate-dep (name "natom-macros") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "18wjmj4xp9wax9sxbyx5px9ahsrin494adyaljkv4nh3vl8jd894") (rust-version "1.69")))

(define-public crate-natom-0.1 (crate (name "natom") (vers "0.1.1-rc.1") (deps (list (crate-dep (name "ctor") (req "^0.2.2") (default-features #t) (kind 0)) (crate-dep (name "natom-macros") (req "^0.1.1-rc.1") (default-features #t) (kind 0)))) (hash "1bxqmvk788d1vvz58183xiz53wx4yv6qa6qh6b7hw9vw933xbll9") (rust-version "1.69")))

(define-public crate-natom-0.1 (crate (name "natom") (vers "0.1.1-rc.2") (deps (list (crate-dep (name "ctor") (req "^0.2.2") (default-features #t) (kind 0)) (crate-dep (name "natom-macros") (req "^0.1.1-rc.2") (default-features #t) (kind 0)))) (hash "0d1z69hvma351bx4spnb4ji3m2rf1kaxyasd32l1s1d3mkwikqds") (rust-version "1.69")))

(define-public crate-natom-macros-0.1 (crate (name "natom-macros") (vers "0.1.0") (deps (list (crate-dep (name "proc-macro2") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2") (features (quote ("full" "extra-traits"))) (default-features #t) (kind 0)))) (hash "072ni5v12bvy8i43s1fsxxfjrnp5iqhk17d6zpfyhm8f305vr995") (rust-version "1.69")))

(define-public crate-natom-macros-0.1 (crate (name "natom-macros") (vers "0.1.1-rc.1") (deps (list (crate-dep (name "proc-macro2") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2") (features (quote ("full" "extra-traits"))) (default-features #t) (kind 0)))) (hash "17pfs19p9b9gh2mwcn1w9fnssl2lkk7k52bvk234g8pmi1dzckri") (rust-version "1.69")))

(define-public crate-natom-macros-0.1 (crate (name "natom-macros") (vers "0.1.1-rc.2") (deps (list (crate-dep (name "proc-macro2") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2") (features (quote ("full" "extra-traits"))) (default-features #t) (kind 0)))) (hash "15pqyqrpl57y8rlvdd4sinh3pyigd4257svxcj3gjk5f4syac424") (rust-version "1.69")))

(define-public crate-natord-1 (crate (name "natord") (vers "1.0.0") (hash "1qg6rqkxfcy7d2r7l5slk7jh0l581vhnvwpjs8wbnawsx7825gf4")))

(define-public crate-natord-1 (crate (name "natord") (vers "1.0.1") (hash "117srinzjf0p9k96zyh84v8axf19c6zc16v6xs156pyrv0nssd9m")))

(define-public crate-natord-1 (crate (name "natord") (vers "1.0.2") (hash "0qc17827dn4psnqjczz26ls990fv1p2y01sxjlyvs8nddj8mcrpm")))

(define-public crate-natord-1 (crate (name "natord") (vers "1.0.3") (hash "0yhs3lkh5ziqpz296f1ay5bm8525yhlkbrw8jh1pd7fi9hx7sdj6")))

(define-public crate-natord-1 (crate (name "natord") (vers "1.0.4") (hash "1lm2pmdapy2ijhhicr26pykwsisnp9gnximik56mfdbrai5z3gq4")))

(define-public crate-natord-1 (crate (name "natord") (vers "1.0.5") (hash "0m69g9gdfi72zssn37pxj6v4n8r0pplbfl7l5qp4f7lsb6wzxavc")))

(define-public crate-natord-1 (crate (name "natord") (vers "1.0.6") (hash "0mssbl9ynscqv9fvi9s0wvs2kas2k9igx7ixy4qb46r6i9qj90yk")))

(define-public crate-natord-1 (crate (name "natord") (vers "1.0.7") (hash "1wl8mfv1hfjh4sdsng5s35ih7q74avwk28hwk325iym79z2dkzz0")))

(define-public crate-natord-1 (crate (name "natord") (vers "1.0.8") (hash "0vp5cdax4qb3x7jlx430rkjmw7gm4y3dlzm9hsk5yz3fb1hyfixz")))

(define-public crate-natord-1 (crate (name "natord") (vers "1.0.9") (hash "0z75spwag3ch20841pvfwhh3892i2z2sli4pzp1jgizbipdrd39h")))

