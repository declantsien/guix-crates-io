(define-module (crates-io na rc) #:use-module (crates-io))

(define-public crate-narc-0.1 (crate (name "narc") (vers "0.1.0") (deps (list (crate-dep (name "byteorder") (req "^1.2") (default-features #t) (kind 0)) (crate-dep (name "failure") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "memmap") (req "^0.6") (default-features #t) (kind 0)) (crate-dep (name "nitro_fs") (req "^0.1") (default-features #t) (kind 0)))) (hash "0390i5q9b76w2f9xdncpav429mgz4i98v1r1h4wfbw87qs78xdwh")))

(define-public crate-narc-0.1 (crate (name "narc") (vers "0.1.1") (deps (list (crate-dep (name "byteorder") (req "^1.3") (default-features #t) (kind 0)) (crate-dep (name "failure") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "memmap") (req "^0.7") (default-features #t) (kind 0)) (crate-dep (name "nitro_fs") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "num") (req "^0.2") (kind 0)) (crate-dep (name "rayon") (req "^1.0") (default-features #t) (kind 0)))) (hash "1vd5gnf1fnmj6jq5nz8bqrxynl1g5jxargjny7lbd4hvk37j96dx")))

(define-public crate-narc-0.2 (crate (name "narc") (vers "0.2.0") (deps (list (crate-dep (name "anyhow") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "byteorder") (req "^1.3") (default-features #t) (kind 0)) (crate-dep (name "memmap") (req "^0.7") (default-features #t) (kind 0)) (crate-dep (name "nitro_fs") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "num") (req "^0.2") (kind 0)) (crate-dep (name "rayon") (req "^1.5") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.26") (default-features #t) (kind 0)))) (hash "0ch6hhvbz4ggfhv8fa45arl25zhkl9hmv0x8s3sf4dbya7wx1ych")))

(define-public crate-narc_hal-0.1 (crate (name "narc_hal") (vers "0.1.0") (deps (list (crate-dep (name "cast") (req "^0.2.2") (kind 0)) (crate-dep (name "cortex-m") (req "^0.5.6") (default-features #t) (kind 0)) (crate-dep (name "cortex-m-rt") (req "^0.5.3") (default-features #t) (kind 0)) (crate-dep (name "embedded-hal") (req "^0.2.1") (features (quote ("unproven"))) (default-features #t) (kind 0)) (crate-dep (name "panic-semihosting") (req "^0.4.0") (default-features #t) (kind 0)) (crate-dep (name "stm32l0") (req "^0.2.3") (features (quote ("stm32l0x1"))) (default-features #t) (kind 0)))) (hash "047fc2xfzxygwmnb6g10w2dvsa3x3r6898hk177lgmciz12ks9lp")))

(define-public crate-narc_hal-0.1 (crate (name "narc_hal") (vers "0.1.1") (deps (list (crate-dep (name "cast") (req "^0.2.2") (kind 0)) (crate-dep (name "cortex-m") (req "^0.5.6") (default-features #t) (kind 0)) (crate-dep (name "cortex-m-rt") (req "^0.5.3") (default-features #t) (kind 0)) (crate-dep (name "embedded-hal") (req "^0.2.1") (features (quote ("unproven"))) (default-features #t) (kind 0)) (crate-dep (name "panic-semihosting") (req "^0.4.0") (default-features #t) (kind 0)) (crate-dep (name "stm32l0") (req "^0.2.3") (features (quote ("stm32l0x1"))) (default-features #t) (kind 0)))) (hash "10dmy7jdmwczywv4gfxk2sw83pzwiw2zmq7p8b29crn7630wg8fn")))

(define-public crate-narc_hal-0.1 (crate (name "narc_hal") (vers "0.1.2") (deps (list (crate-dep (name "cast") (req "^0.2.2") (kind 0)) (crate-dep (name "cortex-m") (req "^0.5.6") (default-features #t) (kind 0)) (crate-dep (name "cortex-m-rt") (req "^0.5.3") (default-features #t) (kind 0)) (crate-dep (name "embedded-hal") (req "^0.2.1") (features (quote ("unproven"))) (default-features #t) (kind 0)) (crate-dep (name "panic-semihosting") (req "^0.4.0") (default-features #t) (kind 0)) (crate-dep (name "stm32l0") (req "^0.2.3") (features (quote ("stm32l0x1"))) (default-features #t) (kind 0)))) (hash "11pjz0z73m1igp3fkag8lqxw12hny3ylc8x98g74asnbfi03lsb6")))

(define-public crate-narc_hal-0.1 (crate (name "narc_hal") (vers "0.1.3") (deps (list (crate-dep (name "cast") (req "^0.2.2") (kind 0)) (crate-dep (name "cortex-m") (req "^0.5.6") (default-features #t) (kind 0)) (crate-dep (name "cortex-m-rt") (req "^0.5.3") (default-features #t) (kind 0)) (crate-dep (name "embedded-hal") (req "^0.2.1") (features (quote ("unproven"))) (default-features #t) (kind 0)) (crate-dep (name "nb") (req "^0.1.1") (default-features #t) (kind 0)) (crate-dep (name "panic-semihosting") (req "^0.4.0") (default-features #t) (kind 0)) (crate-dep (name "stm32l0") (req "^0.2.3") (features (quote ("stm32l0x1" "rt"))) (default-features #t) (kind 0)))) (hash "1np7hjz6w54d6iprpyl1qig7rs1ifkhdanpn4m1p1xiiqmqmc3xa")))

(define-public crate-narcissistic-0.1 (crate (name "narcissistic") (vers "0.1.0") (deps (list (crate-dep (name "num") (req "^0.4.0") (default-features #t) (kind 0)))) (hash "0dv7q5xwakvk4h2bmfg41c86cb09qknlj1yk09p1kxnsmmv936d1")))

(define-public crate-narcissistic-0.2 (crate (name "narcissistic") (vers "0.2.0") (deps (list (crate-dep (name "convert-base") (req "^1.1.2") (default-features #t) (kind 0)) (crate-dep (name "num") (req "^0.4.0") (default-features #t) (kind 0)))) (hash "05vbvw63l174522wli180dib8qxx6k57c3qcrpgb0pri8p9crzcb")))

(define-public crate-narcissistic-0.2 (crate (name "narcissistic") (vers "0.2.1") (deps (list (crate-dep (name "num") (req "^0.4.0") (default-features #t) (kind 0)))) (hash "1bc3ykc0iv06mskpqxlhvhk27c0nm8rwpa29xqphn6rmsl8mq96a")))

(define-public crate-narcissistic-0.3 (crate (name "narcissistic") (vers "0.3.0") (deps (list (crate-dep (name "num") (req "^0.4.0") (default-features #t) (kind 0)))) (hash "1x6k5rgi19k3mv8qlvl1dvm4f0im5yb7nzxgnayvmajbd8y440c4")))

(define-public crate-narcissus-0.1 (crate (name "narcissus") (vers "0.1.0") (hash "1y6s63slp0r4p0v7l8wmqcyli5iyhh5lnvnasmpvxwa5dizh0f4f")))

(define-public crate-narcissus-0.1 (crate (name "narcissus") (vers "0.1.1") (deps (list (crate-dep (name "num") (req "*") (default-features #t) (kind 0)))) (hash "1a3mkrv2l7flgdpx37k909hi2f74d48a0f40mqdg043ryf3sz7qz")))

(define-public crate-narcissus-0.1 (crate (name "narcissus") (vers "0.1.2") (deps (list (crate-dep (name "num") (req "*") (default-features #t) (kind 0)))) (hash "1fbgj8miqaak7fgvzk3hs0s88s7icfgxcyc3k4ldxsbrlk45rmdv")))

(define-public crate-narcissus-0.1 (crate (name "narcissus") (vers "0.1.3") (deps (list (crate-dep (name "num") (req "*") (default-features #t) (kind 0)))) (hash "03zxyc06mrwc08wz3lmawqvrjrj5kmb9cxss4gv3q6gm6cjipql5")))

(define-public crate-narcissus-0.2 (crate (name "narcissus") (vers "0.2.0") (deps (list (crate-dep (name "num") (req "^0.1") (default-features #t) (kind 0)))) (hash "0lrw0a6d9883nzzki3a13yj7xbfnxf7wiccskna0n2sbc2mn2086")))

(define-public crate-narcissus-0.3 (crate (name "narcissus") (vers "0.3.0") (deps (list (crate-dep (name "num") (req "^0.1") (default-features #t) (kind 0)))) (hash "0pkgnk7xzv1ijzaka6ib84kv0sss6hjwnh5yf526xwbdckl6rjmd")))

