(define-module (crates-io na g-) #:use-module (crates-io))

(define-public crate-nag-driver-0.1 (crate (name "nag-driver") (vers "0.1.0") (deps (list (crate-dep (name "serde") (req "^1.0.152") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.91") (default-features #t) (kind 0)))) (hash "039z6an1dasv3xkl1kx8clmzxlg87gvg05b0k1w49i5ywsgc6dn9")))

(define-public crate-nag-toolkit-0.1 (crate (name "nag-toolkit") (vers "0.1.0") (deps (list (crate-dep (name "paste") (req "^1.0.11") (default-features #t) (kind 0)))) (hash "10p5x6gg91nnyq767qpqq42pcj5vyxihk2f7nrq5gzvbm4a29lbi")))

