(define-module (crates-io na cl) #:use-module (crates-io))

(define-public crate-nacl-0.1 (crate (name "nacl") (vers "0.1.0") (hash "081i076i6vj74m7kkz45gbrk86nymfm7izhr2lf6yxx3lc49jrsq")))

(define-public crate-nacl-0.2 (crate (name "nacl") (vers "0.2.0") (hash "1q3c42nww01am0jjq1fwkai4zn8104dab7m0z6b1l9m308c66qb1")))

(define-public crate-nacl-0.3 (crate (name "nacl") (vers "0.3.0") (hash "12axbhlrwmdsp0sw9rvsj18isx9vm9sypm9bmqpr0p4k7yf8a40n")))

(define-public crate-nacl-0.4 (crate (name "nacl") (vers "0.4.0") (hash "1f2la5ic370dhsbg9jakfr62d5bw5lvfayb6xqpv3wxp3z1w1hrv")))

(define-public crate-nacl-0.5 (crate (name "nacl") (vers "0.5.0") (hash "1b0gfcr6pi3lyp6yb826sahcaicf3vf801mych20gh9ipz3877n9")))

(define-public crate-nacl-0.5 (crate (name "nacl") (vers "0.5.1") (hash "1av0z35i00gvppwnlwgz4cgr1m9z9yd7vz58d7fl32kyjilgs51d")))

(define-public crate-nacl-0.5 (crate (name "nacl") (vers "0.5.2") (hash "0v5d4hdsbx4rzsyq5pkahb29hqmsv0zp0l4wbq4jmfkydzsgf341")))

(define-public crate-nacl-0.5 (crate (name "nacl") (vers "0.5.3") (hash "0j6m4d9y00d7h613rmkl68d0wbkzq63hx5ajg5g1pi8kv12grbih")))

(define-public crate-nacl-compat-0.0.0 (crate (name "nacl-compat") (vers "0.0.0") (hash "18a6zhh7c996jdvvip5dyispxsckxjvwmiy4yayfzdl3zghc5q03")))

