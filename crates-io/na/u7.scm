(define-module (crates-io na u7) #:use-module (crates-io))

(define-public crate-nau7802-0.1 (crate (name "nau7802") (vers "0.1.0-alpha") (deps (list (crate-dep (name "byteorder") (req "^1.3.4") (kind 0)) (crate-dep (name "embedded-hal") (req "^0.2.4") (default-features #t) (kind 0)) (crate-dep (name "nb") (req "^1") (default-features #t) (kind 0)))) (hash "0s3aww5fqj2lfipkwp5i75x246arvkgjrdzbnfcr746npyr33nb0") (features (quote (("embedded-hal-adc" "embedded-hal/unproven") ("default"))))))

