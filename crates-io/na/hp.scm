(define-module (crates-io na hp) #:use-module (crates-io))

(define-public crate-nahpack-0.2 (crate (name "nahpack") (vers "0.2.0") (deps (list (crate-dep (name "rustc-serialize") (req "*") (default-features #t) (kind 2)))) (hash "0rl380q50b647kpaxs3xm0k015sxm30admin8i2x6z9l0z23fynq")))

