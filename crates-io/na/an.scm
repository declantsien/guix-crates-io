(define-module (crates-io na an) #:use-module (crates-io))

(define-public crate-naan-0.1 (crate (name "naan") (vers "0.1.0") (hash "196d0z8iygnx7bawvmz7dn6glwsyn3gx9hb76fqiw4jmg6w6i952")))

(define-public crate-naan-0.1 (crate (name "naan") (vers "0.1.1") (hash "0pc6li9ql000lp5lwi2d8pcl17dkc9akmhyzc1cnpv0a47v981mw") (features (quote (("test") ("std" "alloc") ("docs") ("default" "std") ("alloc"))))))

(define-public crate-naan-0.1 (crate (name "naan") (vers "0.1.2") (hash "1nf1bxjm494k3cfc7y6xz5n1vd47czvi37ag210g89v24afscgyd") (features (quote (("test") ("std" "alloc") ("docs") ("default" "std") ("alloc"))))))

(define-public crate-naan-0.1 (crate (name "naan") (vers "0.1.3") (hash "0b4sxbqampi309vaavz0z13z9xj2pandiiblf6655bv9a0qqfkfa") (features (quote (("test") ("std" "alloc") ("docs") ("default" "std") ("alloc"))))))

(define-public crate-naan-0.1 (crate (name "naan") (vers "0.1.4") (hash "0wck3ikk96inr2p870gkd3x9hhzhqibbvyp34wlw095c7y9ijm9a") (features (quote (("test") ("std" "alloc") ("docs") ("default" "std") ("alloc"))))))

(define-public crate-naan-0.1 (crate (name "naan") (vers "0.1.5") (hash "1lh6jwmncg9i8gjph9ry5kkdynwy224v93s2kfcf0syj4msgg3dk") (features (quote (("test") ("std" "alloc") ("docs") ("default" "std") ("alloc"))))))

(define-public crate-naan-0.1 (crate (name "naan") (vers "0.1.6") (hash "087spn7y0qwxp83kq4f91618lhsbf99xd83kryjz8xvdd6767lzj") (features (quote (("test") ("std" "alloc") ("docs") ("default" "std") ("alloc"))))))

(define-public crate-naan-0.1 (crate (name "naan") (vers "0.1.7") (hash "1dyk1kv30pnnv4vimd89qvwdbsb9kw0g2sll3r6qd6yklfkynvln") (features (quote (("test") ("std" "alloc") ("docs") ("default" "std") ("alloc"))))))

(define-public crate-naan-0.1 (crate (name "naan") (vers "0.1.8") (hash "0zigwp731z7lpq4di9z3w8vsmxjzn12751g8i6skrpblb1bykbj3") (features (quote (("test") ("std" "alloc") ("docs") ("default" "std") ("alloc"))))))

(define-public crate-naan-0.1 (crate (name "naan") (vers "0.1.9") (hash "1vcicyh0cnrkcwjg343yjv35w05kqizywrpvb1kbmr214scvc0m4") (features (quote (("test") ("std" "alloc") ("docs") ("default" "std") ("alloc"))))))

(define-public crate-naan-0.1 (crate (name "naan") (vers "0.1.10") (hash "03vh89dwn1fkcngg8yyhc8qsv5k795v2gpz2y45pp62pdjf2d3g8") (features (quote (("test") ("std" "alloc") ("docs") ("default" "std") ("alloc"))))))

(define-public crate-naan-0.1 (crate (name "naan") (vers "0.1.11") (hash "1klpc77faa0lr18jrgmjkazsii82z9r6qw5zl8dy79c5y3xxkwb1") (features (quote (("test") ("std" "alloc") ("docs") ("default" "std") ("alloc"))))))

(define-public crate-naan-0.1 (crate (name "naan") (vers "0.1.12") (hash "0911lssgai7kbqgp9mqbl0p482ni808g3w9qdxp1y344dh9bah8i") (features (quote (("test") ("std" "alloc") ("docs") ("default" "std") ("alloc"))))))

(define-public crate-naan-0.1 (crate (name "naan") (vers "0.1.13") (hash "0lxg9il4813546y0zdr2nmxzx7gdvr0vdfngg8bk56k6kgg1js5s") (features (quote (("test") ("std" "alloc") ("docs") ("default" "std") ("alloc"))))))

(define-public crate-naan-0.1 (crate (name "naan") (vers "0.1.14") (hash "16frwmnsbmwrqpkp7hwxl9bxga7xrgls4hi0v0d1si21fhghxr65") (features (quote (("test") ("std" "alloc") ("docs") ("default" "std") ("alloc"))))))

(define-public crate-naan-0.1 (crate (name "naan") (vers "0.1.15") (hash "07qz68chx9mmlscwm1sgwzvj02d2l72yd5adihlcc5y1pbvb7lnh") (features (quote (("test") ("std" "alloc") ("docs") ("default" "std") ("alloc"))))))

(define-public crate-naan-0.1 (crate (name "naan") (vers "0.1.16") (hash "01ddqyx4ayccmb45k1fcvy4161h9lkychrpdm4ign68rvgyhlczy") (features (quote (("test") ("std" "alloc") ("docs") ("default" "std") ("alloc"))))))

(define-public crate-naan-0.1 (crate (name "naan") (vers "0.1.17") (hash "14fg36h1rwjldlb42qn4sggjkhvc0jw50jjn2c8v7nrsvk4h48gr") (features (quote (("test") ("std" "alloc") ("docs") ("default" "std") ("alloc"))))))

(define-public crate-naan-0.1 (crate (name "naan") (vers "0.1.18") (hash "1b58mg4qg56fjdf40xgw1dfzyqld11ia7c2idxmbr8j0x5h8xpih") (features (quote (("test") ("std" "alloc") ("docs") ("default" "std") ("alloc"))))))

(define-public crate-naan-0.1 (crate (name "naan") (vers "0.1.19") (hash "1y7mw0y9cyav2kzmskyz8hnyk2ccfajcplhsdqxd04a2d2sj525l") (features (quote (("test") ("std" "alloc") ("docs") ("default" "std") ("alloc"))))))

(define-public crate-naan-0.1 (crate (name "naan") (vers "0.1.20") (hash "1s425zj5kg5rs0y20p6dvzf9kgvyl5zgmgmc0r6jj25pmir64fy2") (features (quote (("test") ("std" "alloc") ("docs") ("default" "std") ("alloc"))))))

(define-public crate-naan-0.1 (crate (name "naan") (vers "0.1.21") (hash "1cazk69q39igna5c9rzbz3k2hysh2s0g7knwns2kn925acizkj7i") (features (quote (("test") ("std" "alloc") ("docs") ("default" "std") ("alloc"))))))

(define-public crate-naan-0.1 (crate (name "naan") (vers "0.1.22") (hash "1ca061nz1z07wfhvp3x2nb0pwx5paklbdi1j9ll6vvklxvb2fdib") (features (quote (("test") ("std" "alloc") ("docs") ("default" "std") ("alloc"))))))

(define-public crate-naan-0.1 (crate (name "naan") (vers "0.1.23") (deps (list (crate-dep (name "tinyvec") (req "^1.6.0") (features (quote ("rustc_1_57"))) (optional #t) (default-features #t) (kind 0)))) (hash "1h806wr64b6m9vjabmld99cv0vgwf6b09qpxkq8gjz4r73n9g3z4") (features (quote (("test") ("std" "alloc") ("docs") ("default" "std") ("alloc")))) (v 2) (features2 (quote (("tinyvec" "dep:tinyvec"))))))

(define-public crate-naan-0.1 (crate (name "naan") (vers "0.1.24") (deps (list (crate-dep (name "tinyvec") (req "^1.6.0") (features (quote ("rustc_1_57"))) (optional #t) (default-features #t) (kind 0)))) (hash "193wgaaq2fqfj6cvc7bb5gda3scz14ink6rld4qh0m9x6iyzqwx3") (features (quote (("test") ("std" "alloc") ("docs" "tinyvec") ("default" "std") ("alloc")))) (v 2) (features2 (quote (("tinyvec" "dep:tinyvec"))))))

(define-public crate-naan-0.1 (crate (name "naan") (vers "0.1.25") (deps (list (crate-dep (name "tinyvec") (req "^1.6.0") (features (quote ("rustc_1_57"))) (optional #t) (default-features #t) (kind 0)))) (hash "11w14y3fx32r5k17mwzyjn3bhzklahspjj4g3k48pai9jfawxlf1") (features (quote (("test") ("std" "alloc") ("docs" "tinyvec") ("default" "std") ("alloc")))) (v 2) (features2 (quote (("tinyvec" "dep:tinyvec"))))))

(define-public crate-naan-0.1 (crate (name "naan") (vers "0.1.26") (deps (list (crate-dep (name "tinyvec") (req "^1.6.0") (features (quote ("rustc_1_57"))) (optional #t) (default-features #t) (kind 0)))) (hash "1jr0csrl9yzajyirzx7rf9pmfkxx161bs3c6b4m5zrcvm5rs03l0") (features (quote (("test") ("std" "alloc") ("docs" "tinyvec") ("default" "std") ("alloc")))) (v 2) (features2 (quote (("tinyvec" "dep:tinyvec"))))))

(define-public crate-naan-0.1 (crate (name "naan") (vers "0.1.27") (deps (list (crate-dep (name "tinyvec") (req "^1.6.0") (features (quote ("rustc_1_57"))) (optional #t) (default-features #t) (kind 0)))) (hash "05qq1vp23gwdqfpabpi91ii2msshwn6jqdm8vbnbddil43wz7dh4") (features (quote (("test") ("std" "alloc") ("docs" "tinyvec") ("default" "std" "alloc") ("alloc")))) (v 2) (features2 (quote (("tinyvec" "dep:tinyvec"))))))

(define-public crate-naan-0.1 (crate (name "naan") (vers "0.1.28") (deps (list (crate-dep (name "tinyvec") (req "^1.6.0") (features (quote ("rustc_1_57"))) (optional #t) (default-features #t) (kind 0)))) (hash "1iqk4f963jigv8jdcjj94qha2s10c8wcklvvwqp955q5gcc71lxn") (features (quote (("test") ("std" "alloc") ("docs" "tinyvec") ("default" "std" "alloc") ("alloc")))) (v 2) (features2 (quote (("tinyvec" "dep:tinyvec"))))))

(define-public crate-naan-0.1 (crate (name "naan") (vers "0.1.29") (deps (list (crate-dep (name "tinyvec") (req "^1.6.0") (features (quote ("rustc_1_57"))) (optional #t) (default-features #t) (kind 0)))) (hash "0zl06qbn5c4036zrcjgj170gy1dwm9lh4im2z6x5k2z4x3zi6ghx") (features (quote (("test") ("std" "alloc") ("docs" "tinyvec") ("default" "std" "alloc") ("alloc")))) (v 2) (features2 (quote (("tinyvec" "dep:tinyvec"))))))

(define-public crate-naan-0.1 (crate (name "naan") (vers "0.1.30") (deps (list (crate-dep (name "tinyvec") (req "^1.6.0") (features (quote ("rustc_1_57"))) (optional #t) (default-features #t) (kind 0)))) (hash "0122acgxkk16cm0pnvkgqrh6qiczkmd7xlf06yglh8ylvp3gcvhg") (features (quote (("test") ("std" "alloc") ("docs" "tinyvec") ("default" "std" "alloc") ("alloc")))) (v 2) (features2 (quote (("tinyvec" "dep:tinyvec"))))))

(define-public crate-naan-0.1 (crate (name "naan") (vers "0.1.31") (deps (list (crate-dep (name "tinyvec") (req "^1.6.0") (features (quote ("rustc_1_57"))) (optional #t) (default-features #t) (kind 0)))) (hash "07gqsihafdxc0g03ihaw9vllnp8g83n98gdvmw917fczrwf1qqf7") (features (quote (("test") ("std" "alloc") ("docs" "tinyvec") ("default" "std" "alloc") ("alloc")))) (v 2) (features2 (quote (("tinyvec" "dep:tinyvec"))))))

(define-public crate-naan-0.1 (crate (name "naan") (vers "0.1.32") (deps (list (crate-dep (name "tinyvec") (req "^1.6.0") (features (quote ("rustc_1_57"))) (optional #t) (default-features #t) (kind 0)))) (hash "04n6xg0n2w712k0n301zv3k1sdavkmhmv70nmyprhi5z42cv2m31") (features (quote (("test") ("std" "alloc") ("docs" "tinyvec") ("default" "std" "alloc") ("alloc")))) (v 2) (features2 (quote (("tinyvec" "dep:tinyvec"))))))

