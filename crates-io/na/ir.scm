(define-module (crates-io na ir) #:use-module (crates-io))

(define-public crate-nairud-0.1 (crate (name "nairud") (vers "0.1.1") (hash "03vq4nlff5gxl7s97r9c3kmzbd55qwmr4ih8bn7d6prndk014z6d") (features (quote (("std"))))))

(define-public crate-nairud-0.1 (crate (name "nairud") (vers "0.1.2") (hash "1ag3wrrknnjw0sqknf9hja429gg7if0r9zr9qvilfywr6x6shi33") (features (quote (("std"))))))

(define-public crate-nairud-0.1 (crate (name "nairud") (vers "0.1.3") (hash "1s50mxacqr3c675j60fb0kyyfpc1zm6103rl3xkl31nbbvml7m00") (features (quote (("std"))))))

(define-public crate-nairud-0.1 (crate (name "nairud") (vers "0.1.4") (hash "0j6mvmiz8wkgsxq8xdx6lwrbs644sm0rr2mqfq1q5kz98jw672v5") (features (quote (("std"))))))

(define-public crate-nairud-0.1 (crate (name "nairud") (vers "0.1.5") (hash "1qhi8i937iy1ngkdj3y7pzq96x2k6xgyl87bvf4ir6fg4mj539c2") (features (quote (("std"))))))

(define-public crate-nairud-0.2 (crate (name "nairud") (vers "0.2.0") (hash "096dwwbv2jnqpbyfy7slva3q4rj686kv8xicai2br0kx2i0g1dnf") (features (quote (("std"))))))

(define-public crate-nairud-0.3 (crate (name "nairud") (vers "0.3.0") (hash "1xqr3vsdag637zqqp82rvacpvgfjrf9mzqk4lgcyx8a4263qffk4") (features (quote (("std"))))))

(define-public crate-nairud-0.3 (crate (name "nairud") (vers "0.3.1") (hash "1gzbc3mwr47f8x7r84bj8bvb171gqpfj3q66283g2cq8cygsw99i") (features (quote (("std"))))))

(define-public crate-nairud-0.3 (crate (name "nairud") (vers "0.3.2") (hash "04kd1rqllaj3cjf1kawqkg5sic8dlybxq9hliqy8bhzdwqdzw7hv") (features (quote (("std"))))))

(define-public crate-nairud-0.3 (crate (name "nairud") (vers "0.3.3") (hash "1dj6ga8a8zmj6bzb2lnhrnks8w0rpq1gfq1sasw18b84195f72wa") (features (quote (("std"))))))

(define-public crate-nairud-0.3 (crate (name "nairud") (vers "0.3.4") (hash "1yp6mpq61k2l2g1lk1l9agq5g6qvd4lrggy1xaxkbl70yvyha582") (features (quote (("std"))))))

(define-public crate-nairud-0.4 (crate (name "nairud") (vers "0.4.0") (hash "0ncssymczhlmpgwwyid5gz74g0i9gn0hwm68g3mbx23fvqbawsas") (features (quote (("std"))))))

(define-public crate-nairud-0.4 (crate (name "nairud") (vers "0.4.1") (hash "1za31sq42y9di9hk3ib9cfvcm6f5bc04cmbl3zhkrvg7q9jijzwr") (features (quote (("std"))))))

(define-public crate-nairud-0.5 (crate (name "nairud") (vers "0.5.0") (hash "0hgsif8ls3ill00wvdzhl4a16scki700mqbpk62anwq9ff8av1d2") (features (quote (("std"))))))

(define-public crate-nairud-0.5 (crate (name "nairud") (vers "0.5.1") (hash "0580776pdg2sbkkwf2sjmyswd9f5m97w15z8bn4vwq427xljvmsv") (features (quote (("std"))))))

(define-public crate-nairud-0.5 (crate (name "nairud") (vers "0.5.2") (hash "0cg7gjcrj9mdffm39crlan9w92hx9hfankyl31rj6h5vk70g3b16") (features (quote (("std"))))))

(define-public crate-nairud-0.5 (crate (name "nairud") (vers "0.5.3") (hash "13jp2hym6rpa9fz7rqgzv86cwn7ng49cm678b5w26qp11h0k3vxc") (features (quote (("std"))))))

(define-public crate-nairud-0.5 (crate (name "nairud") (vers "0.5.4") (hash "1czccrsdg4gjhfajrqp9vlx20bk3sd069002y0pm2ldlyjwijv0b") (features (quote (("std"))))))

(define-public crate-nairud-0.5 (crate (name "nairud") (vers "0.5.5") (hash "19qckj0jq2qa8lndwaz2jda4i2zh10l81as9rwr5jp44ffla2hif") (features (quote (("std"))))))

(define-public crate-nairud-0.5 (crate (name "nairud") (vers "0.5.6") (hash "0xlfwkf7bch8y2vr5av9ympr96cmkd522z3adpv0qsmd0c35gd6k") (features (quote (("std"))))))

(define-public crate-nairud-0.5 (crate (name "nairud") (vers "0.5.7") (hash "1zpqh99pmqi5nivcw60f2mgc7l9ndnzxpjhzikipc9cywn769pkm") (features (quote (("std"))))))

(define-public crate-nairud-0.5 (crate (name "nairud") (vers "0.5.8") (hash "0gxm4yljlnn9bwzcj80lhs75xcdadx7jja3mzs5aspjndmnykwxf") (features (quote (("std"))))))

(define-public crate-nairud-0.5 (crate (name "nairud") (vers "0.5.9") (hash "0y3q52id2acm8z0087bb4w0ksqvlqipgf36y6g1hcgck1dq4rsjg") (features (quote (("std"))))))

(define-public crate-nairud-0.5 (crate (name "nairud") (vers "0.5.10") (hash "0bd87kic0krxqfnrp5sclspv6q0ljc1ly6p7zxps1iq7qysciyd2") (features (quote (("std"))))))

(define-public crate-nairud-0.5 (crate (name "nairud") (vers "0.5.11") (hash "0jjkla9rc43nxsvxdgy45grw5dp56ajfax5ibbamif65kiyjd6i2") (features (quote (("std"))))))

(define-public crate-nairud-0.5 (crate (name "nairud") (vers "0.5.12") (hash "178ch5nz55h8cq7l8b6nwl8kmxmyax3cpqv8myrmr2yknxbh2mf7") (features (quote (("std"))))))

(define-public crate-nairud-0.5 (crate (name "nairud") (vers "0.5.13") (hash "0gyxsil410iykvjs7ibl2xp96qhi6zydpal8ka06dc1smdsvc85s") (features (quote (("std"))))))

(define-public crate-nairud-0.5 (crate (name "nairud") (vers "0.5.14") (hash "0392lqw0s0x2h6g8l40nzybpydp6qnqkplwnrrg92dfaal966q37") (features (quote (("std"))))))

(define-public crate-nairud-0.5 (crate (name "nairud") (vers "0.5.15") (hash "0bpsbg98nq6m1nd4vq064p17qm8f6i1lndfh3c4s64lyhbnc7vnr") (features (quote (("std"))))))

