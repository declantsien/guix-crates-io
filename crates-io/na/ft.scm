(define-module (crates-io na ft) #:use-module (crates-io))

(define-public crate-nafta-0.1 (crate (name "nafta") (vers "0.1.0") (deps (list (crate-dep (name "diesel") (req "^1.4") (features (quote ("r2d2" "sqlite"))) (default-features #t) (kind 0)) (crate-dep (name "diesel_migrations") (req "^1.4") (default-features #t) (kind 2)) (crate-dep (name "tempdir") (req "^0.3") (default-features #t) (kind 0)))) (hash "08nai48v6dr1jvkbhfiz317wj51lp6h8jym1p8nd99mnzzcpggdf")))

(define-public crate-nafta-0.1 (crate (name "nafta") (vers "0.1.1") (deps (list (crate-dep (name "diesel") (req "^1.4") (features (quote ("r2d2" "sqlite"))) (default-features #t) (kind 0)) (crate-dep (name "diesel_migrations") (req "^1.4") (default-features #t) (kind 2)) (crate-dep (name "tempfile") (req "^3.1") (default-features #t) (kind 0)))) (hash "07mvh4dsqq4d4561rk6602iwy84f267sc64r59zbraaprymxkjbp")))

(define-public crate-nafta-0.1 (crate (name "nafta") (vers "0.1.2") (deps (list (crate-dep (name "diesel") (req "^1.4") (features (quote ("r2d2" "sqlite"))) (default-features #t) (kind 0)) (crate-dep (name "diesel_migrations") (req "^1.4") (default-features #t) (kind 2)) (crate-dep (name "tempfile") (req "^3.1") (default-features #t) (kind 0)))) (hash "0gmzr7wkz0cqcqskgbxwlcdjfdm2j41dxhkfwr6nlx9fa9ygz690")))

(define-public crate-nafta-0.1 (crate (name "nafta") (vers "0.1.3") (deps (list (crate-dep (name "diesel") (req "^1.4") (features (quote ("r2d2" "sqlite"))) (default-features #t) (kind 0)) (crate-dep (name "diesel_migrations") (req "^1.4") (default-features #t) (kind 2)) (crate-dep (name "tempfile") (req "^3.1") (default-features #t) (kind 0)))) (hash "02qhdhx0xi2qvnzfgxz6pcjcgninpzlmay5jxjd2mbl8lbxi0cp1")))

