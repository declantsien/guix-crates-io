(define-module (crates-io na tn) #:use-module (crates-io))

(define-public crate-natnet-decode-0.1 (crate (name "natnet-decode") (vers "0.1.0") (deps (list (crate-dep (name "byteorder") (req "^0.5") (default-features #t) (kind 0)) (crate-dep (name "clippy") (req "^0.0.95") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "env_logger") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "log") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "nalgebra") (req "^0.10") (default-features #t) (kind 0)) (crate-dep (name "semver") (req "^0.4") (default-features #t) (kind 0)))) (hash "0jfpsbz2s4rgfi7dv0zhfszkiy7si59j9yi3pivn7vwql5v31psf") (features (quote (("default"))))))

