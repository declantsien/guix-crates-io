(define-module (crates-io na u8) #:use-module (crates-io))

(define-public crate-nau88c22-0.9 (crate (name "nau88c22") (vers "0.9.0") (deps (list (crate-dep (name "bitfield") (req "^0.14") (default-features #t) (kind 0)) (crate-dep (name "defmt") (req "^0.3.5") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "embedded-hal") (req "^1.0.0") (default-features #t) (kind 0)))) (hash "1lf6y9arcddbzlmibbg3k2rvb673m7y424qn6f6rj3sygw9mkp1v") (v 2) (features2 (quote (("defmt" "dep:defmt"))))))

