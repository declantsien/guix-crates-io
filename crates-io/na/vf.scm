(define-module (crates-io na vf) #:use-module (crates-io))

(define-public crate-navfs-0.1 (crate (name "navfs") (vers "0.1.0") (hash "0rca3ik4v32lwkxlgxas1v5rpidlsry7n9rg3i0aigyknw80544g")))

(define-public crate-navfs-0.1 (crate (name "navfs") (vers "0.1.1") (hash "0vxkd0a9gb1fq83r653rs04mhb6csj8c5kplppbdk98j6rnppdlp")))

(define-public crate-navfs-0.1 (crate (name "navfs") (vers "0.1.2") (deps (list (crate-dep (name "text-colorizer") (req "^1.0.0") (default-features #t) (kind 0)))) (hash "121rbjj07nagqymdr7vmwq9h9r2hrg0zq5rmc8bh8pgn539dz5s5")))

(define-public crate-navfs-0.1 (crate (name "navfs") (vers "0.1.3") (hash "1ybypwxrp6zydb9f192b6nf5vwyhx1pkfk5ay927ssm9ywh46a5d")))

