(define-module (crates-io na _p) #:use-module (crates-io))

(define-public crate-na_print-0.1 (crate (name "na_print") (vers "0.1.0") (deps (list (crate-dep (name "libc") (req "^0.2.132") (default-features #t) (kind 0)))) (hash "0mmryg3kai4axgp7zicl8yjyj28lsj72x8lkxdzryigi3fnnvjv5")))

(define-public crate-na_print-1 (crate (name "na_print") (vers "1.0.0") (deps (list (crate-dep (name "libc") (req "^0.2.132") (default-features #t) (kind 0)))) (hash "0vag2f0nhm72xgj2zjzwb7mi2921k7smrb0pg9bl2ibvgp2vd38c")))

