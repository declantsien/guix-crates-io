(define-module (crates-io na rq) #:use-module (crates-io))

(define-public crate-narqyez_shared-0.1 (crate (name "narqyez_shared") (vers "0.1.0") (deps (list (crate-dep (name "serde") (req "^1.0.193") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serenity") (req "^0.12.0") (default-features #t) (kind 0)))) (hash "0cs29384cpqhrpi5ap0b5d2vj57553sl5w7f9bq7dgj3814galx0")))

