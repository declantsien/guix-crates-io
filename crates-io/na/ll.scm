(define-module (crates-io na ll) #:use-module (crates-io))

(define-public crate-nalloc-0.1 (crate (name "nalloc") (vers "0.1.0") (hash "06kq0kkcsk7skkw5z2zhcgy68hxnyp3nf4yhiihpzkv0zq1jslx8")))

(define-public crate-nalloc-0.1 (crate (name "nalloc") (vers "0.1.1") (hash "00b5pgqiqyaibg6x717fbilzl53pcnsfxbg2j2pvidvbx7km10rk")))

(define-public crate-nalloc-0.1 (crate (name "nalloc") (vers "0.1.2") (hash "1w4z8z8g25kdff0xmnkrs342vg5yvqv7p2jk0p6bvsnn4ngrx5w3")))

