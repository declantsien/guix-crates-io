(define-module (crates-io na mu) #:use-module (crates-io))

(define-public crate-namui-panic-hook-0.1 (crate (name "namui-panic-hook") (vers "0.1.0") (deps (list (crate-dep (name "wasm-bindgen") (req "^0.2.87") (default-features #t) (kind 0)))) (hash "099mqlixr1b3hpvrlsawarnr1mfcqsarpwpvsvbn7c8qsnbxiwxh")))

(define-public crate-namui-panic-hook-0.1 (crate (name "namui-panic-hook") (vers "0.1.1") (deps (list (crate-dep (name "wasm-bindgen") (req "^0.2.87") (default-features #t) (kind 0)))) (hash "0n5xcdijf1537kcw0smvz4n0gawl1yh6m7n2gyi0l5c0apfab994")))

(define-public crate-namui-panic-hook-0.1 (crate (name "namui-panic-hook") (vers "0.1.2") (deps (list (crate-dep (name "wasm-bindgen") (req "^0.2.87") (default-features #t) (kind 0)))) (hash "094n414rzdm8pc4403k0z7vndm4y4bxpzsf13rc4alsw97g5p9ar")))

(define-public crate-namumark-0.1 (crate (name "namumark") (vers "0.1.0") (deps (list (crate-dep (name "namumark_parser") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "1xzxfx812kipm8lhcyjwxw4in103dn3raj0r8v7spfimwp1w9qgf")))

(define-public crate-namumark_parser-0.1 (crate (name "namumark_parser") (vers "0.1.0") (deps (list (crate-dep (name "nom") (req "^5.1.1") (default-features #t) (kind 0)))) (hash "1r37qwvz2c7nwkmzcp6y73yxclvlzyqsxlh978a1lchyfskjzasi")))

