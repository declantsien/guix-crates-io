(define-module (crates-io na r_) #:use-module (crates-io))

(define-public crate-nar_dev_utils-0.26 (crate (name "nar_dev_utils") (vers "0.26.2") (hash "0nw3h6cinqy7gbxynaancp7zyksrqc2r9kr44rld8w0cpfq5ahys") (features (quote (("vec_tools") ("str_processing") ("opt_res_boost") ("iterators") ("into_chars") ("floats") ("enum_union") ("default") ("bundled" "floats" "iterators" "str_processing" "vec_tools" "into_chars" "opt_res_boost" "enum_union"))))))

(define-public crate-nar_dev_utils-0.26 (crate (name "nar_dev_utils") (vers "0.26.3") (hash "037gawgbfpdd9g25wrsis90y0wwmhdb3ykpfyd6sayb9vanhqc7l") (features (quote (("vec_tools") ("str_processing") ("opt_res_boost") ("iterators") ("into_chars") ("floats") ("enum_union") ("default") ("bundled" "floats" "iterators" "str_processing" "vec_tools" "into_chars" "opt_res_boost" "enum_union"))))))

(define-public crate-nar_dev_utils-0.26 (crate (name "nar_dev_utils") (vers "0.26.4") (hash "0m93ck4j9x2wqbi3fxhb3dfk3i1x6zqfvfy15lsf2lr8m97c6qhn") (features (quote (("vec_tools") ("str_processing") ("opt_res_boost") ("iterators") ("into_chars") ("floats") ("enum_union") ("default") ("bundled" "floats" "iterators" "str_processing" "vec_tools" "into_chars" "opt_res_boost" "enum_union"))))))

(define-public crate-nar_dev_utils-0.27 (crate (name "nar_dev_utils") (vers "0.27.0") (hash "13b5sj58la4qwigr41yvr9fl2nrd0wplngcwg7fc0wszg67spzvj") (features (quote (("vec_tools") ("str_processing") ("opt_res_boost") ("iterators") ("into_chars") ("floats") ("enum_union") ("default") ("bundled" "floats" "iterators" "str_processing" "vec_tools" "into_chars" "opt_res_boost" "enum_union"))))))

(define-public crate-nar_dev_utils-0.28 (crate (name "nar_dev_utils") (vers "0.28.0") (hash "075f8l6xf5rvm8vksfzfr4ag808yy6mlk8mnvmawg74bnacigqvh") (features (quote (("vec_tools") ("str_processing") ("opt_res_boost") ("iterators") ("into_chars") ("floats") ("enum_union") ("default") ("bundled" "floats" "iterators" "str_processing" "vec_tools" "into_chars" "opt_res_boost" "enum_union"))))))

(define-public crate-nar_dev_utils-0.29 (crate (name "nar_dev_utils") (vers "0.29.0") (hash "086jwpy8i23qnzyaynqwq94cp77ic28kq89chf8p6r816kin9gn0") (features (quote (("vec_tools") ("str_processing") ("opt_res_boost") ("iterators") ("into_chars") ("floats") ("enum_union") ("default") ("bundled" "floats" "iterators" "str_processing" "vec_tools" "into_chars" "opt_res_boost" "enum_union"))))))

(define-public crate-nar_dev_utils-0.30 (crate (name "nar_dev_utils") (vers "0.30.0") (hash "0psyy8ids82f19fs90gxflja50gsai5h4wl2qjviinry4ihw6l8p") (features (quote (("void") ("vec_tools") ("str_processing") ("opt_res_boost") ("iterators") ("into_chars") ("floats") ("enum_union") ("default") ("bundled" "floats" "iterators" "str_processing" "vec_tools" "into_chars" "opt_res_boost" "enum_union" "void"))))))

(define-public crate-nar_dev_utils-0.31 (crate (name "nar_dev_utils") (vers "0.31.0") (hash "1siby3ji9s9h4dli54087grk3g0ls7bp43sdcdr9gwx0yl3asbyy") (features (quote (("void") ("vec_tools") ("str_processing") ("opt_res_boost") ("iterators") ("into_chars") ("floats") ("enum_union") ("default") ("bundled" "floats" "iterators" "str_processing" "vec_tools" "into_chars" "opt_res_boost" "enum_union" "void"))))))

