(define-module (crates-io na fc) #:use-module (crates-io))

(define-public crate-nafcodec-0.1 (crate (name "nafcodec") (vers "0.1.0") (deps (list (crate-dep (name "nom") (req "^7.1.3") (default-features #t) (kind 0)) (crate-dep (name "zstd") (req "^0.12.4") (features (quote ("experimental"))) (default-features #t) (kind 0)))) (hash "1izz7xlrvjyavk5q4ldaxap6hb4i4h03yqiw9n8kidq9nwa567pw") (features (quote (("default") ("arc"))))))

(define-public crate-nafcodec-0.1 (crate (name "nafcodec") (vers "0.1.1") (deps (list (crate-dep (name "nom") (req "^7.1.3") (default-features #t) (kind 0)) (crate-dep (name "zstd") (req "^0.12.4") (features (quote ("experimental"))) (default-features #t) (kind 0)))) (hash "0xm35f96zfi917bj6s1bpgp6c7dyqd55vbk785rj9zgagdfaafc2") (features (quote (("default") ("arc"))))))

(define-public crate-nafcodec-0.2 (crate (name "nafcodec") (vers "0.2.0") (deps (list (crate-dep (name "nom") (req "^7.1.3") (default-features #t) (kind 0)) (crate-dep (name "tempfile") (req "^3.8.1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "zstd") (req "^0.13.1") (features (quote ("experimental"))) (default-features #t) (kind 0)))) (hash "09d2lakipny74jlcck4swqzzdgyw8y08dvj5xw5qcgw5kigpsazl") (features (quote (("nightly") ("default" "tempfile") ("arc"))))))

(define-public crate-nafcodec-py-0.1 (crate (name "nafcodec-py") (vers "0.1.1") (deps (list (crate-dep (name "nafcodec") (req "^0.1.1") (features (quote ("arc"))) (default-features #t) (kind 0)) (crate-dep (name "pyo3") (req "^0.18.3") (default-features #t) (kind 0)))) (hash "09s7mkkrkfc35rb4wsiag5978ph4az9mb0gvb3kdj4midr5hrfbb") (features (quote (("nightly" "pyo3/nightly") ("extension-module" "pyo3/extension-module") ("default"))))))

(define-public crate-nafcodec-py-0.2 (crate (name "nafcodec-py") (vers "0.2.0") (deps (list (crate-dep (name "nafcodec") (req "^0.2.0") (features (quote ("arc"))) (default-features #t) (kind 0)) (crate-dep (name "pyo3") (req "^0.21.1") (default-features #t) (kind 0)))) (hash "03axzrdk06vvbxzv7j8q98yqjz5mnxgsjxyidcaib2wh7gm280dj") (features (quote (("nightly" "pyo3/nightly") ("extension-module" "pyo3/extension-module") ("default"))))))

