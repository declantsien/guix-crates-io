(define-module (crates-io na ro) #:use-module (crates-io))

(define-public crate-naro-0.0.1 (crate (name "naro") (vers "0.0.1") (deps (list (crate-dep (name "naro-derive") (req "^0.0.1") (default-features #t) (kind 0)))) (hash "0038n6xvs449q81mw0b7xi2d1idlllasklinxbz8713bmkwxym7m")))

(define-public crate-naro-derive-0.0.1 (crate (name "naro-derive") (vers "0.0.1") (deps (list (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.2") (default-features #t) (kind 0)) (crate-dep (name "rocksdb") (req "^0.13.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.8") (default-features #t) (kind 0)))) (hash "1gfq8snhsi6f2pdrk8mn9jif5awghrwf1a6sc7s00ahhq1wavhnq")))

(define-public crate-naromat-0.0.1 (crate (name "naromat") (vers "0.0.1") (deps (list (crate-dep (name "regex") (req "^1.3") (default-features #t) (kind 0)))) (hash "0k452bav2lrvy0igqmdhy9pgfii3x9m0am3kggxbl0480n0as7zc")))

(define-public crate-naromat-0.2 (crate (name "naromat") (vers "0.2.0") (deps (list (crate-dep (name "clap") (req "^2") (default-features #t) (kind 0)) (crate-dep (name "file_diff") (req "^1.0.0") (default-features #t) (kind 2)) (crate-dep (name "regex") (req "^1.3") (default-features #t) (kind 0)))) (hash "01bswzpwxcr8im23cr5g4qn4hbdvghmcpa71c8820k44mhmxclql")))

(define-public crate-naromat-0.2 (crate (name "naromat") (vers "0.2.1") (deps (list (crate-dep (name "clap") (req "^2") (default-features #t) (kind 0)) (crate-dep (name "file_diff") (req "^1.0.0") (default-features #t) (kind 2)) (crate-dep (name "regex") (req "^1.3") (default-features #t) (kind 0)))) (hash "0ii45z18ma7q6x6lzpx18gqa8di3gbxy3ghw047903sc34z4bzqm")))

(define-public crate-naromat-0.3 (crate (name "naromat") (vers "0.3.0") (deps (list (crate-dep (name "clap") (req "^2") (default-features #t) (kind 0)) (crate-dep (name "file_diff") (req "^1.0.0") (default-features #t) (kind 2)) (crate-dep (name "regex") (req "^1.3") (default-features #t) (kind 0)))) (hash "0sp6hx4ixvls577ny41ls712q9pmq4n86rq77vzs56pccyckmy9r")))

(define-public crate-naromat-0.3 (crate (name "naromat") (vers "0.3.1") (deps (list (crate-dep (name "clap") (req "^2") (default-features #t) (kind 0)) (crate-dep (name "file_diff") (req "^1.0.0") (default-features #t) (kind 2)) (crate-dep (name "regex") (req "^1.3") (default-features #t) (kind 0)))) (hash "1c62mb7iaps7knh6mimmhzi6qxrn0yyykmjjrbhqjifp8v5vnd3l")))

