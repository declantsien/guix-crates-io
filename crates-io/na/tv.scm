(define-module (crates-io na tv) #:use-module (crates-io))

(define-public crate-natvis-pdbs-0.0.0 (crate (name "natvis-pdbs") (vers "0.0.0") (deps (list (crate-dep (name "cargo_metadata") (req "^0.8.2") (default-features #t) (kind 0)))) (hash "1p64y037iyf77yp4qiw9gb476wifvj3y7azipbd2p8qw8hbdy98b") (yanked #t)))

(define-public crate-natvis-pdbs-0.0.1 (crate (name "natvis-pdbs") (vers "0.0.1") (deps (list (crate-dep (name "cargo_metadata") (req "^0.8.2") (default-features #t) (kind 0)))) (hash "05qag2vf1hjxqj5jf4ci8mvgal2a5c6mafixwqk7hzmh9ry949hy") (yanked #t)))

(define-public crate-natvis-pdbs-1 (crate (name "natvis-pdbs") (vers "1.0.0") (deps (list (crate-dep (name "cargo_metadata") (req "^0.8.2") (default-features #t) (kind 0)))) (hash "0cki4v7sql2997178349mvh7xvda00rz6mbxwnj9xg2s73ln280j")))

(define-public crate-natvis-pdbs-1 (crate (name "natvis-pdbs") (vers "1.0.1") (deps (list (crate-dep (name "cargo_metadata") (req "^0.8.2") (default-features #t) (kind 0)))) (hash "1pk97wsvc9h8bxm09a5xhiza2mrss8dn60lfk26vph9h9ixkay1v")))

(define-public crate-natvis-pdbs-1 (crate (name "natvis-pdbs") (vers "1.0.2") (deps (list (crate-dep (name "cargo_metadata") (req "^0.9") (default-features #t) (kind 0)))) (hash "132k78vqqjidkqr6iviip1d63zb68rsr7xdzda7nmxicfblaqf1p")))

(define-public crate-natvis-pdbs-1 (crate (name "natvis-pdbs") (vers "1.0.3") (deps (list (crate-dep (name "serde") (req "^1") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1") (default-features #t) (kind 0)))) (hash "1vll8lrvxxbxj9ih2iwxl1n3ynypadpjy3z98jmj1aaiays3ajjz")))

