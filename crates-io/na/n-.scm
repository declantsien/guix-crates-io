(define-module (crates-io na n-) #:use-module (crates-io))

(define-public crate-nan-preserving-float-0.1 (crate (name "nan-preserving-float") (vers "0.1.0") (deps (list (crate-dep (name "rand") (req "^0.4.2") (default-features #t) (kind 2)))) (hash "07zk1zqiwsa8brc4gbwpivi910p2m46xnwf9ikx9wk1grh7z1m1l")))

(define-public crate-nan-tag-0.1 (crate (name "nan-tag") (vers "0.1.0") (hash "05k43wjhk8138j5n48l4blnmv9laf4wc45wlnkvaij7zp4sqaa2b") (features (quote (("default" "boxed_ptr") ("boxed_ptr"))))))

