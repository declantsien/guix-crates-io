(define-module (crates-io na sl) #:use-module (crates-io))

(define-public crate-naslint-0.0.1 (crate (name "naslint") (vers "0.0.1") (deps (list (crate-dep (name "clap") (req "^2.33.3") (default-features #t) (kind 0)))) (hash "1vxy66chvhhgc2rslhmqkhlj2isqiia2d6xi1j3kzj1s202c1b0l")))

(define-public crate-naslint-0.0.11 (crate (name "naslint") (vers "0.0.11") (deps (list (crate-dep (name "clap") (req "^2.33.3") (default-features #t) (kind 0)))) (hash "0k4h47jrvzqkyn1skkg0jn2iiylh8lxvqq5v49s9dpdhm9xnbnwg")))

(define-public crate-naslint-0.0.12 (crate (name "naslint") (vers "0.0.12") (deps (list (crate-dep (name "clap") (req "^2.33.3") (default-features #t) (kind 0)))) (hash "1cs7ykxmnlv9wacvf3r0cgdqjkl64p9z86laa7spy7wnqdirkr93")))

(define-public crate-naslint-0.0.13 (crate (name "naslint") (vers "0.0.13") (deps (list (crate-dep (name "clap") (req "^2.33.3") (default-features #t) (kind 0)))) (hash "0k5bcrvzswgqzd6vrlzqc4lg9x4dapkzis675ghhw5rzrcs1zr46")))

(define-public crate-naslint-0.0.2 (crate (name "naslint") (vers "0.0.2") (deps (list (crate-dep (name "atty") (req "^0.2.14") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^2.33.3") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.4.3") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.123") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0.123") (default-features #t) (kind 0)) (crate-dep (name "toml") (req "^0.5.8") (default-features #t) (kind 0)))) (hash "0wqrarrnwhh991z1rqzspv9pxs8844fam4h31m30bxhk3adassfc")))

(define-public crate-naslint-0.0.21 (crate (name "naslint") (vers "0.0.21") (deps (list (crate-dep (name "atty") (req "^0.2.14") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^2.33.3") (default-features #t) (kind 0)) (crate-dep (name "promptly") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.4.3") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.123") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0.123") (default-features #t) (kind 0)) (crate-dep (name "toml") (req "^0.5.8") (default-features #t) (kind 0)))) (hash "0gz080y0jlfr98aygxam05p0p9fw6hsnz5zn5kv8xx7shf50kylb")))

(define-public crate-naslint-0.0.22 (crate (name "naslint") (vers "0.0.22") (deps (list (crate-dep (name "atty") (req "^0.2.14") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^2.33.3") (default-features #t) (kind 0)) (crate-dep (name "promptly") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.4.3") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.123") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0.123") (default-features #t) (kind 0)) (crate-dep (name "toml") (req "^0.5.8") (default-features #t) (kind 0)))) (hash "1yw4r1sqdr210g0a5kjxwjgbniks1ifjs321jcp0ckawx8609v6n")))

