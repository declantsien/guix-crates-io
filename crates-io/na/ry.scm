(define-module (crates-io na ry) #:use-module (crates-io))

(define-public crate-nary_tree-0.4 (crate (name "nary_tree") (vers "0.4.1") (deps (list (crate-dep (name "slab") (req "~0.4") (default-features #t) (kind 0)) (crate-dep (name "snowflake") (req "~1.3") (default-features #t) (kind 0)))) (hash "1n8gzwbm6h9qwixswgvc0vdms0yfw1g58gvv17fhqsb02hkpjyxw") (features (quote (("experimental")))) (yanked #t)))

(define-public crate-nary_tree-0.4 (crate (name "nary_tree") (vers "0.4.2") (deps (list (crate-dep (name "slab") (req "~0.4") (default-features #t) (kind 0)) (crate-dep (name "snowflake") (req "~1.3") (default-features #t) (kind 0)))) (hash "1xm9hz8mizfhp5xnyh647jk96ysyi56lxzm82nh7p4adww93jr33") (features (quote (("experimental"))))))

(define-public crate-nary_tree-0.4 (crate (name "nary_tree") (vers "0.4.3") (deps (list (crate-dep (name "slab") (req "~0.4") (default-features #t) (kind 0)) (crate-dep (name "snowflake") (req "~1.3") (default-features #t) (kind 0)))) (hash "1iqray1a716995l9mmvz5sfqrwg9a235bvrkpcn8bcqwjnwfv1pv") (features (quote (("experimental"))))))

