(define-module (crates-io na za) #:use-module (crates-io))

(define-public crate-nazar-0.1 (crate (name "nazar") (vers "0.1.0") (deps (list (crate-dep (name "redis") (req "^0.5.3") (default-features #t) (kind 0)))) (hash "10mf8lxiihypf279nadgfhbwsny06p5pjjsl6nq1jkwps749sr8h")))

(define-public crate-nazar-1 (crate (name "nazar") (vers "1.0.0") (deps (list (crate-dep (name "redis") (req "^0.5.3") (default-features #t) (kind 0)) (crate-dep (name "ws") (req "^0.7.3") (default-features #t) (kind 0)))) (hash "00qszhbn66ryshcx5vnkx37lryrwvjf6f5by1p18qcv3nddzhp67")))

(define-public crate-nazar-1 (crate (name "nazar") (vers "1.0.1") (deps (list (crate-dep (name "redis") (req "^0.5.3") (default-features #t) (kind 0)) (crate-dep (name "ws") (req "^0.7.3") (default-features #t) (kind 0)))) (hash "0nycnc5g1sm6f579lrwq3z4fwgp7li8xzxbg753h788674axalm9")))

(define-public crate-nazar-1 (crate (name "nazar") (vers "1.0.2") (deps (list (crate-dep (name "redis") (req "^0.5.3") (default-features #t) (kind 0)) (crate-dep (name "ws") (req "^0.7.3") (default-features #t) (kind 0)))) (hash "12d9cvkw8cd8xwsl03b7mfwjbxsx693nf9swpb9ivsmqxdzc9csm")))

(define-public crate-nazar-1 (crate (name "nazar") (vers "1.0.3") (deps (list (crate-dep (name "geojson") (req "^0.8.1") (default-features #t) (kind 0)) (crate-dep (name "redis") (req "^0.5.3") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "ws") (req "^0.7.3") (default-features #t) (kind 0)))) (hash "0b183z4x60dpyrlpdr41fa6czjwra4j11a7lkqvidk84s9axb6ka")))

(define-public crate-nazar-1 (crate (name "nazar") (vers "1.0.4") (deps (list (crate-dep (name "geojson") (req "^0.8.1") (default-features #t) (kind 0)) (crate-dep (name "redis") (req "^0.5.3") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "ws") (req "^0.7.3") (default-features #t) (kind 0)))) (hash "06i1xg62gh3s0zafajz1y21ijsymk3mg4d9jpzibzzpjr11n9sp2")))

(define-public crate-nazar-1 (crate (name "nazar") (vers "1.0.5") (deps (list (crate-dep (name "geojson") (req "^0.8.1") (default-features #t) (kind 0)) (crate-dep (name "redis") (req "^0.5.3") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "ws") (req "^0.7.3") (default-features #t) (kind 0)))) (hash "08w53hhn0pqwwp24p5x0vlkms3n71syrc08w147azz3kzyb4f7si")))

(define-public crate-nazar-1 (crate (name "nazar") (vers "1.0.6") (deps (list (crate-dep (name "geojson") (req "^0.8.1") (default-features #t) (kind 0)) (crate-dep (name "redis") (req "^0.5.3") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "ws") (req "^0.7.3") (default-features #t) (kind 0)))) (hash "0ygi1qv74i9ilslpmqgvyia2sbaalw99zfdybxpr3pr8j3wyw02y")))

(define-public crate-nazar-1 (crate (name "nazar") (vers "1.0.7") (deps (list (crate-dep (name "geojson") (req "^0.8.1") (default-features #t) (kind 0)) (crate-dep (name "redis") (req "^0.5.3") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "ws") (req "^0.7.3") (default-features #t) (kind 0)))) (hash "148ml62dc4ak52l1arpscjymznvaw6r2s879rig9g5lq099dqfar")))

(define-public crate-Nazarust-0.1 (crate (name "Nazarust") (vers "0.1.0") (hash "0xw2i1qdm0shsk437vxyj4p873rip1ayy4dm18drrb9m1fgqc0fx")))

