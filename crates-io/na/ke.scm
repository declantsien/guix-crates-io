(define-module (crates-io na ke) #:use-module (crates-io))

(define-public crate-naked-function-0.1 (crate (name "naked-function") (vers "0.1.0") (deps (list (crate-dep (name "cfg-if") (req "^1.0.0") (default-features #t) (kind 0)) (crate-dep (name "naked-function-macro") (req "=0.1.0") (default-features #t) (kind 0)))) (hash "0a3bs0mbk8w13f7pglp9z8y2c3p1k37p78byx06dhbwlk7zbd2h8") (rust-version "1.59.0")))

(define-public crate-naked-function-0.1 (crate (name "naked-function") (vers "0.1.1") (deps (list (crate-dep (name "cfg-if") (req "^1.0.0") (default-features #t) (kind 0)) (crate-dep (name "naked-function-macro") (req "=0.1.1") (default-features #t) (kind 0)))) (hash "17gbs4d0wag7hsyr8sn9336scyw1k24c8fhmwvl1cjghcsibb6r9") (rust-version "1.59.0")))

(define-public crate-naked-function-0.1 (crate (name "naked-function") (vers "0.1.2") (deps (list (crate-dep (name "cfg-if") (req "^1.0.0") (default-features #t) (kind 0)) (crate-dep (name "naked-function-macro") (req "=0.1.2") (default-features #t) (kind 0)))) (hash "0vaa4kpg99l1zr6jvjsa46rj23f57p5m47fhmg83s4nnfp4d0pmm") (rust-version "1.59.0")))

(define-public crate-naked-function-0.1 (crate (name "naked-function") (vers "0.1.3") (deps (list (crate-dep (name "cfg-if") (req "^1.0.0") (default-features #t) (kind 0)) (crate-dep (name "naked-function-macro") (req "=0.1.3") (default-features #t) (kind 0)))) (hash "0bklwig893k0nqzsqaaz606ykj9nh7890g5k7cxdbrjmi7m3z956") (rust-version "1.59.0")))

(define-public crate-naked-function-0.1 (crate (name "naked-function") (vers "0.1.4") (deps (list (crate-dep (name "cfg-if") (req "^1.0.0") (default-features #t) (kind 0)) (crate-dep (name "naked-function-macro") (req "=0.1.4") (default-features #t) (kind 0)))) (hash "1q42m4k495lbj15fijrbakjq3ikmg66rg03w18kwfvfq89w6r2jv") (rust-version "1.59.0")))

(define-public crate-naked-function-0.1 (crate (name "naked-function") (vers "0.1.5") (deps (list (crate-dep (name "cfg-if") (req "^1.0.0") (default-features #t) (kind 0)) (crate-dep (name "naked-function-macro") (req "=0.1.5") (default-features #t) (kind 0)))) (hash "1b0ryz4nqaagyfks7bhgmsfkdbjsmjwx7vqa05dj3rmidb55z39v") (rust-version "1.59.0")))

(define-public crate-naked-function-macro-0.1 (crate (name "naked-function-macro") (vers "0.1.0") (deps (list (crate-dep (name "proc-macro2") (req "^1.0.42") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.20") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.98") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "0sr6ph9affjxcav7hpwl6366vr3cmd0aapy7nz866cyixv5wz1gw") (rust-version "1.59.0")))

(define-public crate-naked-function-macro-0.1 (crate (name "naked-function-macro") (vers "0.1.1") (deps (list (crate-dep (name "proc-macro2") (req "^1.0.42") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.20") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.98") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "13g5is4nq0dcc2q98yzvxk1pj9k5kngchrs5wgv0i52d0a6i2b32") (rust-version "1.59.0")))

(define-public crate-naked-function-macro-0.1 (crate (name "naked-function-macro") (vers "0.1.2") (deps (list (crate-dep (name "proc-macro2") (req "^1.0.42") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.20") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.98") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "1vl31v8gdxb643csygh928s2r9mlks4dhlspn8xin0hjdh9kfqgf") (rust-version "1.59.0")))

(define-public crate-naked-function-macro-0.1 (crate (name "naked-function-macro") (vers "0.1.3") (deps (list (crate-dep (name "proc-macro2") (req "^1.0.56") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.26") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2.0.13") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "1wwzkhic3lc6i1hiwycrnkrwsj1rzyc83i9bxkc9gc3icz1l12b3") (rust-version "1.59.0")))

(define-public crate-naked-function-macro-0.1 (crate (name "naked-function-macro") (vers "0.1.4") (deps (list (crate-dep (name "proc-macro2") (req "^1.0.56") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.26") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2.0.13") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "1dw67m5jm2vjjwp6y1m4nrvqmdkbi8d0pvvzb29fimm8a7sid3cx") (rust-version "1.59.0")))

(define-public crate-naked-function-macro-0.1 (crate (name "naked-function-macro") (vers "0.1.5") (deps (list (crate-dep (name "proc-macro2") (req "^1.0.56") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.26") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2.0.13") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "0x84g8a9hm75yz035jfgl8j5771v8np6dwfgf2rhpzpm1pkj6hav") (rust-version "1.59.0")))

