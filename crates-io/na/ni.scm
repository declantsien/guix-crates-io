(define-module (crates-io na ni) #:use-module (crates-io))

(define-public crate-nani-the-facc-0.1 (crate (name "nani-the-facc") (vers "0.1.0") (hash "0m0bz1cjckkml5z3pzngwa610407caf4853lp85z9y36554vy1bn")))

(define-public crate-nanite-0.0.1 (crate (name "nanite") (vers "0.0.1") (hash "0qxc807rg7ny4y0d4i2pcis6a9zvrw54aph7krpc5hqxg2llhfgm")))

(define-public crate-nanite-cli-0.0.1 (crate (name "nanite-cli") (vers "0.0.1") (deps (list (crate-dep (name "nanite") (req "^0.0.1") (default-features #t) (kind 0)))) (hash "00z2n5r691apq4wfadwbcaxqqjjlgxpzybx48filf1aixybgam4s")))

