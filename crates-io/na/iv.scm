(define-module (crates-io na iv) #:use-module (crates-io))

(define-public crate-naive-0.1 (crate (name "naive") (vers "0.1.0") (deps (list (crate-dep (name "failure") (req "^0.1.5") (default-features #t) (kind 0)) (crate-dep (name "futures") (req "^0.1.28") (default-features #t) (kind 0)) (crate-dep (name "futures3") (req "^0.3.0-alpha.17") (features (quote ("async-await" "nightly" "compat"))) (default-features #t) (kind 0) (package "futures-preview")) (crate-dep (name "log") (req "^0.4.6") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.94") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.40") (default-features #t) (kind 0)) (crate-dep (name "tokio-tungstenite") (req "^0.8.0") (default-features #t) (kind 0)) (crate-dep (name "tungstenite") (req "^0.8.1") (default-features #t) (kind 0)) (crate-dep (name "url") (req "^1.7.2") (default-features #t) (kind 0)) (crate-dep (name "url_serde") (req "^0.2.0") (default-features #t) (kind 0)))) (hash "0p63hinjhk07acqsvmqm9xqcyagsjq0qihjl3wz7vv85xrx8j2r3")))

(define-public crate-naive-0.1 (crate (name "naive") (vers "0.1.1") (deps (list (crate-dep (name "failure") (req "^0.1.5") (default-features #t) (kind 0)) (crate-dep (name "futures") (req "^0.1.28") (default-features #t) (kind 0)) (crate-dep (name "futures3") (req "^0.3.0-alpha.17") (features (quote ("async-await" "nightly" "compat"))) (default-features #t) (kind 0) (package "futures-preview")) (crate-dep (name "log") (req "^0.4.6") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.94") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.40") (default-features #t) (kind 0)) (crate-dep (name "tokio-tungstenite") (req "^0.8.0") (default-features #t) (kind 0)) (crate-dep (name "tungstenite") (req "^0.8.1") (default-features #t) (kind 0)) (crate-dep (name "url") (req "^1.7.2") (default-features #t) (kind 0)) (crate-dep (name "url_serde") (req "^0.2.0") (default-features #t) (kind 0)))) (hash "0k7f6c9p8y2r1ywh5iczcyqbjyd0c6zi7xp9535k7awxyc8yvhm6")))

(define-public crate-naive-cityhash-0.1 (crate (name "naive-cityhash") (vers "0.1.0") (hash "1llcsfha7ik04lifjx6zxbs2k5vlvvkavlq8l1bprkv4ynh42mhy")))

(define-public crate-naive-cityhash-0.2 (crate (name "naive-cityhash") (vers "0.2.0") (deps (list (crate-dep (name "clickhouse-rs-cityhash-sys") (req "^0.1.2") (default-features #t) (kind 2)) (crate-dep (name "criterion") (req "^0.3.5") (default-features #t) (kind 2)) (crate-dep (name "fake") (req "^2.4.3") (default-features #t) (kind 2)) (crate-dep (name "quickcheck") (req "^1.0.3") (default-features #t) (kind 2)) (crate-dep (name "quickcheck_macros") (req "^1.0.0") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.8.4") (default-features #t) (kind 2)))) (hash "0hkzbq1kkjxs81fgddidxdb8gx8adj9lq77g69rad2kd3sgb9rzw")))

(define-public crate-naive-timer-0.1 (crate (name "naive-timer") (vers "0.1.0") (hash "1yhq1x339cv2wvafqhcg5ysmv8bdsrxhpi8gib70wkzd94llwgsj")))

(define-public crate-naive-timer-0.2 (crate (name "naive-timer") (vers "0.2.0") (hash "1v9zhizqmylfyk0d1ynqm5gc3hv6cq59ad94rymw5w7bvvbhljh3")))

(define-public crate-naive_json_io-0.1 (crate (name "naive_json_io") (vers "0.1.0") (deps (list (crate-dep (name "serde") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1") (default-features #t) (kind 0)))) (hash "0fb0z6i0grmkaz8fjlbb9n2w57nh41l5ggkn3vxvph47kvzanyd4")))

(define-public crate-naive_opt-0.1 (crate (name "naive_opt") (vers "0.1.1") (deps (list (crate-dep (name "criterion") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "memchr") (req "^2.3") (default-features #t) (kind 0)))) (hash "195609zq8vn0cpc9y61644wk6c8x78jc6m0i0cgb3qv6h3l1d8pq")))

(define-public crate-naive_opt-0.1 (crate (name "naive_opt") (vers "0.1.2") (deps (list (crate-dep (name "criterion") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "memchr") (req "^2.3") (default-features #t) (kind 0)))) (hash "18nw7h7jkfkvjas3054nhi6gvpm7bjjyc9hq9dslp16lb5cpywr0")))

(define-public crate-naive_opt-0.1 (crate (name "naive_opt") (vers "0.1.3") (deps (list (crate-dep (name "criterion") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "memchr") (req "^2.3") (default-features #t) (kind 0)))) (hash "0nwm6fma93f9svr5p7i3fz76qhaq5mcrgf9gz0nxfamky7c4wbgp")))

(define-public crate-naive_opt-0.1 (crate (name "naive_opt") (vers "0.1.4") (deps (list (crate-dep (name "criterion") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "memchr") (req "^2.3") (default-features #t) (kind 0)))) (hash "08803xryskagn59yffmf8xri7pw8g0p88r91gbcgq7z7xjwgfylm")))

(define-public crate-naive_opt-0.1 (crate (name "naive_opt") (vers "0.1.5") (deps (list (crate-dep (name "criterion") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "memchr") (req "^2.3") (default-features #t) (kind 0)))) (hash "09l6zrdjnrsmanrm07v2zdj7g8jaghpz8rymv0dgrwrykz5is138")))

(define-public crate-naive_opt-0.1 (crate (name "naive_opt") (vers "0.1.6") (deps (list (crate-dep (name "criterion") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "memchr") (req "^2.3") (default-features #t) (kind 0)))) (hash "1wbfb5gcc931awv339gk35liyfs6fhg89d6b5r27afi0ynwcmywz")))

(define-public crate-naive_opt-0.1 (crate (name "naive_opt") (vers "0.1.7") (deps (list (crate-dep (name "criterion") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (target "cfg(not(target_arch = \"x86_64\"))") (kind 0)) (crate-dep (name "memchr") (req "^2.3") (default-features #t) (target "cfg(target_arch = \"x86_64\")") (kind 0)))) (hash "0laxiv92msv4s7jpa6x4820dqp66rhlfwkdii0dzw17yf6a09n8j") (features (quote (("default"))))))

(define-public crate-naive_opt-0.1 (crate (name "naive_opt") (vers "0.1.8") (deps (list (crate-dep (name "criterion") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (target "cfg(not(target_arch = \"x86_64\"))") (kind 0)) (crate-dep (name "memchr") (req "^2.3") (default-features #t) (target "cfg(target_arch = \"x86_64\")") (kind 0)))) (hash "04br9f2744dhpqabd1zr9d2dpiqzm9d4q6952cgczrhlkwnlkdxf") (features (quote (("default"))))))

(define-public crate-naive_opt-0.1 (crate (name "naive_opt") (vers "0.1.9") (deps (list (crate-dep (name "criterion") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "libc") (req "=0.2") (default-features #t) (target "cfg(not(any(target_arch = \"x86_64\", target_arch = \"x86\")))") (kind 0)) (crate-dep (name "memchr") (req "=2.4") (default-features #t) (target "cfg(any(target_arch = \"x86_64\", target_arch = \"x86\"))") (kind 0)))) (hash "0yx4swahrx1a59wyzb6hr250956dabkn865k1s2rsj6s6lzyccq4") (features (quote (("default"))))))

(define-public crate-naive_opt-0.1 (crate (name "naive_opt") (vers "0.1.10") (deps (list (crate-dep (name "criterion") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "memx") (req "^0.1") (kind 0)))) (hash "03bypx81wpxm2nij6r560hq71hibc1xbmdl2r41071c4fcmdyd4w") (features (quote (("std" "memx/std") ("default" "std"))))))

(define-public crate-naive_opt-0.1 (crate (name "naive_opt") (vers "0.1.11") (deps (list (crate-dep (name "criterion") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "memx") (req "^0.1") (kind 0)))) (hash "05m55d4ikk22cdl9n07m088ar4mhskpyryvnyyvk7ma6l2nkx4ch") (features (quote (("std" "memx/std") ("only_mc_last") ("only_mc_1st") ("default" "std"))))))

(define-public crate-naive_opt-0.1 (crate (name "naive_opt") (vers "0.1.12") (deps (list (crate-dep (name "criterion") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "memx") (req "^0.1") (kind 0)))) (hash "17an37dfvyv75rmwyv0lm9g9ggddnrrawq1q6m3x5ac82lmys3lf") (features (quote (("std" "memx/std") ("only_mc_last") ("only_mc_1st") ("default" "std"))))))

(define-public crate-naive_opt-0.1 (crate (name "naive_opt") (vers "0.1.13") (deps (list (crate-dep (name "criterion") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "memx") (req "^0.1") (kind 0)))) (hash "0rvwba54r4gy4s82zyhpsi7qcdj8vijxxracdq0i1sg64p8k51df") (features (quote (("std" "memx/std") ("only_mc_last") ("only_mc_1st") ("default" "std"))))))

(define-public crate-naive_opt-0.1 (crate (name "naive_opt") (vers "0.1.14") (deps (list (crate-dep (name "criterion") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "memx") (req "^0.1") (kind 0)))) (hash "0ww8iq8knlnq7l2n0b2k1vgz7cydwv07k64v0bkdd8z1482d0rcd") (features (quote (("std" "memx/std") ("only_mc_last") ("only_mc_1st") ("default" "std")))) (yanked #t)))

(define-public crate-naive_opt-0.1 (crate (name "naive_opt") (vers "0.1.15") (deps (list (crate-dep (name "criterion") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "memx") (req "^0.1") (kind 0)))) (hash "04mzyh7dr0f36c4pag4cxp5pwk1rzjsd6087rmybhnfqxrvz13lz") (features (quote (("std" "memx/std") ("only_mc_last") ("only_mc_1st") ("default" "std"))))))

(define-public crate-naive_opt-0.1 (crate (name "naive_opt") (vers "0.1.16") (deps (list (crate-dep (name "criterion") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "memx") (req "^0.1") (kind 0)))) (hash "0z0fcmmwp5zcjimwn8fpkqv67jcji3c4fd0v8r0lhdl627k985yl") (features (quote (("std" "memx/std") ("only_mc_last") ("only_mc_1st") ("default" "std"))))))

(define-public crate-naive_opt-0.1 (crate (name "naive_opt") (vers "0.1.17") (deps (list (crate-dep (name "criterion") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "memx") (req "^0.1") (kind 0)))) (hash "0xqcgmbimbm1952x6fmmig71kfn6jwva9h11v7fam7jr1sv6d4a8") (features (quote (("std" "memx/std") ("only_mc_last") ("only_mc_1st") ("default" "std"))))))

(define-public crate-naive_opt-0.1 (crate (name "naive_opt") (vers "0.1.18") (deps (list (crate-dep (name "criterion") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "memx") (req "^0.1") (kind 0)))) (hash "10qz0fawpda7bmaqgy7s7r5690qbxis8v147dbdbx462r0rmyymq") (features (quote (("std" "memx/std") ("only_mc_last") ("only_mc_1st") ("default" "std"))))))

(define-public crate-naive_opt-0.1 (crate (name "naive_opt") (vers "0.1.19") (deps (list (crate-dep (name "criterion") (req "^0.4") (default-features #t) (kind 2)) (crate-dep (name "memx") (req "^0.1") (kind 0)))) (hash "0pqy1vlavifxylbmid7x42adxfwspbfw3hzvn8w0s02rn2rv1xxc") (features (quote (("std" "memx/std") ("only_mc_last") ("only_mc_1st") ("default" "std")))) (yanked #t)))

(define-public crate-naive_opt-0.1 (crate (name "naive_opt") (vers "0.1.20") (deps (list (crate-dep (name "criterion") (req "^0.4") (default-features #t) (kind 2)) (crate-dep (name "memx") (req "^0.1") (kind 0)))) (hash "1zwlrmzl2ms5bd3k1h7jrsv2gcd14yh68b9znw5bdpfbm6nk02yy") (features (quote (("only_mc_last") ("only_mc_1st") ("default"))))))

(define-public crate-naive_opt-0.1 (crate (name "naive_opt") (vers "0.1.21") (deps (list (crate-dep (name "memx") (req "^0.1") (kind 0)))) (hash "1z7s5d17333w8ybbrikslpx0s1hfiz97mvkrisy3dpb7w0ilcf1m") (features (quote (("only_mc_last") ("only_mc_1st") ("default"))))))

(define-public crate-naive_opt-0.1 (crate (name "naive_opt") (vers "0.1.22") (deps (list (crate-dep (name "memx") (req "^0.1") (kind 0)))) (hash "13vgj4c7k86v5ipkaa8cbimrrjbxvnaazaaadm13dn5cgmmfbcb5") (features (quote (("only_mc_last") ("only_mc_1st") ("default")))) (rust-version "1.56.0")))

(define-public crate-naive_opt-0.1 (crate (name "naive_opt") (vers "0.1.23") (deps (list (crate-dep (name "memx") (req "^0.1") (kind 0)))) (hash "0p5spv59bs4ddx1sdfciqilmpmy17vjp94y1a9g9s0vwx412r83v") (features (quote (("only_mc_last") ("only_mc_1st") ("default")))) (rust-version "1.56.0")))

(define-public crate-naive_opt-0.1 (crate (name "naive_opt") (vers "0.1.24") (deps (list (crate-dep (name "memx") (req "^0.1") (kind 0)))) (hash "03c9z11f3829j9zm819swhqiy0l5hdry45jmaczah5rlk77zd7n0") (features (quote (("only_mc_last") ("only_mc_1st") ("default")))) (rust-version "1.56.0")))

(define-public crate-naivebayes-0.1 (crate (name "naivebayes") (vers "0.1.0") (hash "1nab1frib341qvr46yajfp4yaivv77lyw6y8pqh2hhr24lbgi6xm")))

(define-public crate-naivebayes-0.1 (crate (name "naivebayes") (vers "0.1.1") (hash "1qdd8g4482fl3pkrgnfpw94i6h1chnr04p1d8j78mpjpsz9j2p7b")))

(define-public crate-naivebayes-0.1 (crate (name "naivebayes") (vers "0.1.2") (hash "1r8nf2yfqfjycbbc63rw97diq469cv657pxb87nx74mgnvig5fd4")))

(define-public crate-naiveuring-0.1 (crate (name "naiveuring") (vers "0.1.0") (deps (list (crate-dep (name "bindgen") (req "^0.66") (default-features #t) (kind 1)))) (hash "1r6hiaq6nz18xi4l955jn5kar8kg4ck1b4r8sd4hkaprr7h3msh5") (yanked #t)))

(define-public crate-naiveuring-0.1 (crate (name "naiveuring") (vers "0.1.1") (deps (list (crate-dep (name "bindgen") (req "^0.66") (default-features #t) (kind 1)))) (hash "03fybjj6i0jq26bzlpjxpzxnsb1mbx2ag8721pzvpr0isfgk8k1h") (yanked #t)))

(define-public crate-naiveuring-0.2 (crate (name "naiveuring") (vers "0.2.0") (deps (list (crate-dep (name "bindgen") (req "^0.66") (default-features #t) (kind 1)))) (hash "1wv1bmvkpb91qq5zv0skqjrdzlvmwc6m416dncq286f6krv2cc4x") (yanked #t)))

(define-public crate-naiveuring-0.3 (crate (name "naiveuring") (vers "0.3.0") (deps (list (crate-dep (name "bindgen") (req "^0.66") (default-features #t) (kind 1)))) (hash "1f4baih96hklbql1qa3yjb468jlwdsb7i9wwrgxkj40bgb9l4frf") (yanked #t)))

(define-public crate-naiveuring-0.4 (crate (name "naiveuring") (vers "0.4.0") (deps (list (crate-dep (name "bindgen") (req "^0.66") (default-features #t) (kind 1)))) (hash "0sdbxrhdzbajavqlv7k3vhyz1b40hcqk8hgq38mk0qbq2a4xgzw8") (yanked #t)))

