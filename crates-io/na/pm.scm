(define-module (crates-io na pm) #:use-module (crates-io))

(define-public crate-napmap-0.1 (crate (name "napmap") (vers "0.1.1") (deps (list (crate-dep (name "indexmap") (req "^2.2.6") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.35.1") (features (quote ("sync" "time" "rt"))) (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.35.1") (features (quote ("time" "macros" "rt-multi-thread"))) (default-features #t) (kind 2)) (crate-dep (name "tracing") (req "^0.1.40") (default-features #t) (kind 0)) (crate-dep (name "tracing-subscriber") (req "^0.3.18") (features (quote ("env-filter"))) (default-features #t) (kind 2)))) (hash "0qas4jnw8c34dzd3snh9gihlrlxvr898p8055yvk9lsqp62yaf6j")))

