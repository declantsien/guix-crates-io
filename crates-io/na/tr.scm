(define-module (crates-io na tr) #:use-module (crates-io))

(define-public crate-natrium-0.1 (crate (name "natrium") (vers "0.1.0") (deps (list (crate-dep (name "byteorder") (req "^1.2.3") (default-features #t) (kind 0)))) (hash "0r5cyjcvw5bv0imigijbdwl3705sb3p09k3ckpjldi9ra654zmkj")))

(define-public crate-natrium-0.2 (crate (name "natrium") (vers "0.2.0") (deps (list (crate-dep (name "byteorder") (req "^1.2.3") (default-features #t) (kind 0)) (crate-dep (name "failure") (req "^0.1.1") (default-features #t) (kind 0)))) (hash "0ns14xpdj815sx4v5kf3z0m0xnix7cfgzggd60qfyflapwfs0gqq")))

