(define-module (crates-io na tp) #:use-module (crates-io))

(define-public crate-natpmp-0.1 (crate (name "natpmp") (vers "0.1.5") (deps (list (crate-dep (name "cc") (req "^1.0") (default-features #t) (kind 1)))) (hash "15s38bhliycc1xsf2za5c997864g084nn457nnik6ns8yvqy83dy")))

(define-public crate-natpmp-0.1 (crate (name "natpmp") (vers "0.1.10") (deps (list (crate-dep (name "cc") (req "^1.0") (default-features #t) (kind 1)))) (hash "12hglx4zs859spciyzpqpipkdakc2z6w7mp6r5k3a5zrnkz71xlg")))

(define-public crate-natpmp-0.1 (crate (name "natpmp") (vers "0.1.11") (deps (list (crate-dep (name "cc") (req "^1.0") (default-features #t) (kind 1)))) (hash "128gbsdxzgpwdd7xpqgl2q40n8sdb845m3yfhs2pnfn7aa1f3m8l")))

(define-public crate-natpmp-0.1 (crate (name "natpmp") (vers "0.1.12") (deps (list (crate-dep (name "cc") (req "^1.0") (default-features #t) (kind 1)))) (hash "087pni8lnvf7shprzfkppfwg6j5sqvvzbndmyirnl22rhqxy9ili")))

(define-public crate-natpmp-0.1 (crate (name "natpmp") (vers "0.1.13") (deps (list (crate-dep (name "cc") (req "^1.0") (default-features #t) (kind 1)))) (hash "0vcc2xv7cis2zjw0jyvl0g9bivqbvs3r9qfmy4556c89lkj3vklj")))

(define-public crate-natpmp-0.1 (crate (name "natpmp") (vers "0.1.14") (deps (list (crate-dep (name "cc") (req "^1.0") (default-features #t) (kind 1)))) (hash "11yn49r7iylciaaapgrlb6nyzv222182fr5ari3d31n7mfp0rxcj")))

(define-public crate-natpmp-0.1 (crate (name "natpmp") (vers "0.1.15") (deps (list (crate-dep (name "cc") (req "^1.0") (default-features #t) (kind 1)))) (hash "059ycq1b0bj548zqdzkgzabv0brld9rx7jlxfr3drz40nydmms90")))

(define-public crate-natpmp-0.2 (crate (name "natpmp") (vers "0.2.0") (deps (list (crate-dep (name "cc") (req "^1.0") (default-features #t) (kind 1)))) (hash "04kaz1dw0r8jn39s87212jkf7v9z3pi2izmndarbislmgn8p8nyq")))

(define-public crate-natpmp-0.3 (crate (name "natpmp") (vers "0.3.0") (deps (list (crate-dep (name "async-std") (req "^1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "async-trait") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "cc") (req "^1") (default-features #t) (kind 1)) (crate-dep (name "tokio") (req "^1") (features (quote ("net"))) (optional #t) (default-features #t) (kind 0)))) (hash "1d7i4fip6346lx7lr9zq80rc5wq1zq0lfyswzcf24badjnb9mg0l") (features (quote (("default" "tokio") ("all" "tokio" "async-std"))))))

(define-public crate-natpmp-0.3 (crate (name "natpmp") (vers "0.3.1") (deps (list (crate-dep (name "async-std") (req "^1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "async-trait") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "cc") (req "^1") (default-features #t) (kind 1)) (crate-dep (name "tokio") (req "^1") (features (quote ("net"))) (optional #t) (default-features #t) (kind 0)))) (hash "175nmwlcmbjccrs6aa9bmdd5k198jpl4bvvk4frgw2m0f6l3avcz") (features (quote (("default" "tokio") ("all" "tokio" "async-std"))))))

(define-public crate-natpmp-0.3 (crate (name "natpmp") (vers "0.3.2") (deps (list (crate-dep (name "async-std") (req "^1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "async-trait") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "cc") (req "^1") (default-features #t) (kind 1)) (crate-dep (name "tokio") (req "^1") (features (quote ("net"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1") (features (quote ("full"))) (default-features #t) (kind 2)))) (hash "1bbn56k57x2lc0scjxy2dca48m5lim5pcpp8qn23k87r6fxyzshr") (features (quote (("default" "tokio") ("all" "tokio" "async-std"))))))

(define-public crate-natpmp-0.3 (crate (name "natpmp") (vers "0.3.3") (deps (list (crate-dep (name "async-std") (req "^1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "async-trait") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "cc") (req "^1") (default-features #t) (kind 1)) (crate-dep (name "tokio") (req "^1") (features (quote ("net"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1") (features (quote ("full"))) (default-features #t) (kind 2)))) (hash "0rd3x5aj2vvbg9sak1ghrr633q6iap8kh19z8w45f6hqjw8qsdz1") (features (quote (("default" "tokio") ("all" "tokio" "async-std"))))))

(define-public crate-natpmp-0.4 (crate (name "natpmp") (vers "0.4.0") (deps (list (crate-dep (name "async-std") (req "^1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "async-trait") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "cc") (req "^1") (default-features #t) (kind 1)) (crate-dep (name "tokio") (req "^1") (features (quote ("net"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1") (features (quote ("full"))) (default-features #t) (kind 2)))) (hash "11x7bx6j61zf8dzkag5gnrlm4g28klwlvik0h20grvg1qmfb6r5g") (features (quote (("default" "tokio") ("all" "tokio" "async-std"))))))

