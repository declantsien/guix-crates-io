(define-module (crates-io na gm) #:use-module (crates-io))

(define-public crate-nagme-0.1 (crate (name "nagme") (vers "0.1.0") (hash "1k9ghcdk458x3s0ld6r6z7w1ry92idsjdcp0ar8zv167rwnnzp8n")))

(define-public crate-nagme-0.2 (crate (name "nagme") (vers "0.2.0") (hash "12frydn9am8zsm9jbnz7hncn5g2l2nf22lmnmap21pd08a4n8073")))

