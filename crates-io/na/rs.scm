(define-module (crates-io na rs) #:use-module (crates-io))

(define-public crate-narsese-0.13 (crate (name "narsese") (vers "0.13.0") (deps (list (crate-dep (name "lazy_static") (req "^1.4.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "nar_dev_utils") (req "^0") (features (quote ("bundled"))) (default-features #t) (kind 0)))) (hash "054bwslmnv5dysqzd4953vriwfdpsmzhk7mcy9v9snn6gzilifrc") (features (quote (("lexical_narsese" "lazy_static") ("enum_narsese") ("default" "bundled") ("bundled" "enum_narsese" "lexical_narsese"))))))

(define-public crate-narsese-0.14 (crate (name "narsese") (vers "0.14.0") (deps (list (crate-dep (name "lazy_static") (req "^1.4.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "nar_dev_utils") (req "^0") (features (quote ("bundled"))) (default-features #t) (kind 0)))) (hash "0rb8wmj6bx1b4whkifc0p7lxn8gwdjcn5z3xyx5jg9mihiw8a2a5") (features (quote (("lexical_narsese" "lazy_static") ("enum_narsese") ("default" "bundled") ("bundled" "enum_narsese" "lexical_narsese"))))))

(define-public crate-narsese-0.15 (crate (name "narsese") (vers "0.15.0") (deps (list (crate-dep (name "lazy_static") (req "^1.4.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "nar_dev_utils") (req "^0") (features (quote ("bundled"))) (default-features #t) (kind 0)))) (hash "0axc846c18q11l5lk1vcvrgidq5yb1daiack92wx2iswihr2jayx") (features (quote (("lexical_narsese" "lazy_static") ("enum_narsese") ("default" "bundled") ("bundled" "enum_narsese" "lexical_narsese"))))))

(define-public crate-narsese-0.16 (crate (name "narsese") (vers "0.16.0") (deps (list (crate-dep (name "lazy_static") (req "^1.4.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "nar_dev_utils") (req "^0") (features (quote ("bundled"))) (default-features #t) (kind 0)))) (hash "1zzh6msdifb82ji6q2r8zl4bzzr7dg8raflqdnmmpih7rp3izrh7") (features (quote (("lexical_narsese" "lazy_static") ("enum_narsese") ("default" "bundled") ("bundled" "enum_narsese" "lexical_narsese"))))))

(define-public crate-narsese-0.17 (crate (name "narsese") (vers "0.17.0") (deps (list (crate-dep (name "lazy_static") (req "^1.4.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "nar_dev_utils") (req "^0") (features (quote ("bundled"))) (default-features #t) (kind 0)))) (hash "13svg96y8dqfbznxzdn6qigpcy9jnh4l7jj3c3n31p3nic9f7q7s") (features (quote (("lexical_narsese" "lazy_static") ("enum_narsese") ("default" "bundled") ("bundled" "enum_narsese" "lexical_narsese"))))))

(define-public crate-narsese-0.18 (crate (name "narsese") (vers "0.18.0") (deps (list (crate-dep (name "lazy_static") (req "^1.4.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "nar_dev_utils") (req "^0") (features (quote ("bundled"))) (default-features #t) (kind 0)))) (hash "0mznvq0awx369kv8cd3gb9b0vpf3wm4zjdrxvm7351lmf0sawyys") (features (quote (("lexical_narsese" "lazy_static") ("enum_narsese") ("default" "bundled") ("bundled" "enum_narsese" "lexical_narsese"))))))

(define-public crate-narsil-0.1 (crate (name "narsil") (vers "0.1.0") (hash "0w27c820hifranilfalin0lgfz0wscq9v5na40hpx5zh336izmmw")))

