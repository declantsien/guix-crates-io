(define-module (crates-io na ri) #:use-module (crates-io))

(define-public crate-nari-0.2 (crate (name "nari") (vers "0.2.1") (deps (list (crate-dep (name "file-lock") (req "^2.1") (default-features #t) (kind 0)) (crate-dep (name "notify") (req "^5.1") (kind 0)) (crate-dep (name "ron") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.25") (features (quote ("rt" "time" "sync"))) (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.25") (features (quote ("macros" "rt-multi-thread"))) (default-features #t) (kind 2)))) (hash "0cqds8xz5zz4dyq1z2q2w0l708kq0zdz0qj392nr5czrw2hd74pv")))

(define-public crate-narinfo-1 (crate (name "narinfo") (vers "1.0.0") (deps (list (crate-dep (name "derive_builder") (req "^0.11.2") (kind 0)))) (hash "19jj2fq0inziyawrb45n4g9k24993ns3ir6zqzg272qmlwdkc2f4")))

(define-public crate-narinfo-1 (crate (name "narinfo") (vers "1.0.1") (deps (list (crate-dep (name "derive_builder") (req "^0.11.2") (kind 0)))) (hash "1qm1nk8dzn8adsidhhg6v50v9b6mkmzjp8nr0y9mlgvf6w7jr75p")))

