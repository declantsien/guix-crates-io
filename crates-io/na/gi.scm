(define-module (crates-io na gi) #:use-module (crates-io))

(define-public crate-nagi-0.0.1 (crate (name "nagi") (vers "0.0.1") (hash "0vcjkkg30hns5vsjrlmylkdxkrcka6vn6nd3m0yg62a8p0n778f1")))

(define-public crate-nagios-range-0.1 (crate (name "nagios-range") (vers "0.1.0") (hash "1q2z6cxnw00p727d7wnsdbbv9ql50k5xr1clqjgvglqi99w0amig")))

(define-public crate-nagios-range-0.2 (crate (name "nagios-range") (vers "0.2.0") (hash "1b5vjipbj428bgnr6c3rajhk3rqh3bzzcz4zk7p850rwk85ypvxx")))

(define-public crate-nagios-range-0.2 (crate (name "nagios-range") (vers "0.2.1") (hash "1bjyv3c962jsd4l5gh8m96acic03lqi8jc0nvca1srjsrmw8mnf5")))

(define-public crate-nagios-range-0.2 (crate (name "nagios-range") (vers "0.2.2") (hash "1jx8spdgkjdyhry5w4lg0vrnhqgi8mdjq11dx02fpc8yz6pqzamm")))

(define-public crate-nagios-range-0.2 (crate (name "nagios-range") (vers "0.2.3") (hash "0zjxr9s897x8bby55imr89xrkgp970jf8ay6aksqii8hg72l2n8i")))

(define-public crate-nagios-range-0.2 (crate (name "nagios-range") (vers "0.2.4") (hash "086zssd4gl74wjdvck0qy2ai4dghh87b7qw8k83mj8dzka40ka47")))

(define-public crate-nagios-range-0.2 (crate (name "nagios-range") (vers "0.2.5") (hash "0g44046mny1g7dxw3d1vbsic1j835asisxwl4k010qjisdf9lwqf")))

(define-public crate-nagiosplugin-0.1 (crate (name "nagiosplugin") (vers "0.1.0") (hash "189si4ha1inb1rr2nqbb8vymswzwkqhcxnw20sc53sgdclsbdgfr")))

(define-public crate-nagiosplugin-0.1 (crate (name "nagiosplugin") (vers "0.1.1") (hash "1yl5vagfm6n79yjy4x0bfylm8p0dkcj446ijqsq8rc5zhqclzss6")))

(define-public crate-nagiosplugin-0.1 (crate (name "nagiosplugin") (vers "0.1.2") (hash "06z2xx7zyba8pp42mhn45r7g2a0fxdvcqvk3372kxgy9dc2krmcs")))

(define-public crate-nagiosplugin-0.1 (crate (name "nagiosplugin") (vers "0.1.3") (hash "174a638lmhjmijc3xrc5d9bh4ywc2qxgaccm9nbhn6v30xj9d5xf")))

(define-public crate-nagiosplugin-0.1 (crate (name "nagiosplugin") (vers "0.1.4") (hash "135w3bxsn5mabzbqfpcg2r0qqf9ag65aiyam1chgp3imbrm1v08h")))

(define-public crate-nagiosplugin-0.1 (crate (name "nagiosplugin") (vers "0.1.5") (hash "0is5v9lyz73g6ldfg3j3s52glimzfdpsqm8i3hlndlh8vjvwjm21")))

(define-public crate-nagiosplugin-0.1 (crate (name "nagiosplugin") (vers "0.1.6") (hash "1xrbb30drg4nkzs572dqxn7mrxngfkdnq5nhc1b31j9gxrkym6v1")))

(define-public crate-nagiosplugin-0.1 (crate (name "nagiosplugin") (vers "0.1.7") (hash "0l49fx3w5kw0l3n49iwrsf0cabyav5z5mdchm8dqlh4zgn317lqv")))

(define-public crate-nagiosplugin-0.1 (crate (name "nagiosplugin") (vers "0.1.8") (hash "1kic6infmldka4p62dp951yzkg8zxj1q68pr4slf7q19fvhrkwzh")))

(define-public crate-nagiosplugin-0.1 (crate (name "nagiosplugin") (vers "0.1.9") (hash "0dzzpfla439xci2mbnisvcwpjf55d0w49k16k7q6kl19wz5411px")))

(define-public crate-nagiosplugin-0.2 (crate (name "nagiosplugin") (vers "0.2.0") (hash "1adpb1mgbxabqhiiafdk96y3kzdk402y7bbm909743nb95r2qq39") (yanked #t)))

(define-public crate-nagiosplugin-0.2 (crate (name "nagiosplugin") (vers "0.2.1") (hash "0vzvwbdykr23n6lvdcv0a9mxp11s00yig25a439m918s8ismrs2c") (yanked #t)))

(define-public crate-nagiosplugin-0.2 (crate (name "nagiosplugin") (vers "0.2.2") (hash "1qql269c3h40zj5mbrzfmjm8lr2524m2q6rrn8v7ddmjhi5amjfy")))

(define-public crate-nagiosplugin-0.2 (crate (name "nagiosplugin") (vers "0.2.3") (hash "0ffv99k8qsc0r8rgij71s8ayizjb5ww88805559w1yah0llalr51")))

(define-public crate-nagiosplugin-0.2 (crate (name "nagiosplugin") (vers "0.2.4") (hash "09037pivsdz44bmji65ry03jqj0c0xihq54g673wha0k9ji8msrz")))

(define-public crate-nagiosplugin-0.2 (crate (name "nagiosplugin") (vers "0.2.5") (hash "0cg0ags68f6i1f0a2yvg4q09q7q9ak31fkgynhn63za8j12qnjw7")))

(define-public crate-nagiosplugin-0.2 (crate (name "nagiosplugin") (vers "0.2.6") (hash "0wsjia2v3ddjd2fa98l96mbrshshim7msdfjxzfca3394bpzjlx2")))

(define-public crate-nagiosplugin-0.2 (crate (name "nagiosplugin") (vers "0.2.7") (hash "0b8pi9inly0467w1515mr7n72xl47kf84ycd5m3r1mcfs2iw0rmk") (yanked #t)))

(define-public crate-nagiosplugin-0.2 (crate (name "nagiosplugin") (vers "0.2.8") (hash "0qraf74h1xpvlkqgls63n965s26l5b67pikhqn4ghx7h4lcrwldl")))

(define-public crate-nagiosplugin-0.2 (crate (name "nagiosplugin") (vers "0.2.9") (deps (list (crate-dep (name "anyhow") (req "^1.0.39") (default-features #t) (kind 0)))) (hash "0fdj6n5h5xi5xpgb2k1wlykhmpj2xwnycr2zw25jfnbg3k4y838i")))

(define-public crate-nagiosplugin-0.2 (crate (name "nagiosplugin") (vers "0.2.10") (deps (list (crate-dep (name "anyhow") (req "^1.0.39") (default-features #t) (kind 0)))) (hash "11nib15lssbwbwmwmx0b25f2flvsfisxhj7r88idx956n6a4zz93")))

(define-public crate-nagiosplugin-0.4 (crate (name "nagiosplugin") (vers "0.4.0") (deps (list (crate-dep (name "anyhow") (req "^1.0.40") (default-features #t) (kind 2)) (crate-dep (name "thiserror") (req "^1.0") (default-features #t) (kind 2)))) (hash "1n3q3im0j3gs6vpdql48gfymsxxyxl20vhdw9z4r1f0c4avgznav")))

(define-public crate-nagiosplugin-0.4 (crate (name "nagiosplugin") (vers "0.4.1") (deps (list (crate-dep (name "anyhow") (req "^1.0.40") (default-features #t) (kind 2)) (crate-dep (name "thiserror") (req "^1.0.24") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0") (default-features #t) (kind 2)))) (hash "0sacmq8jsjxxkjcfnz3z10fib48kgd9wnfflg3wqbqzymqfhkrd8")))

(define-public crate-nagiosplugin-0.5 (crate (name "nagiosplugin") (vers "0.5.0") (deps (list (crate-dep (name "anyhow") (req "^1.0.40") (default-features #t) (kind 2)) (crate-dep (name "thiserror") (req "^1.0.24") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0") (default-features #t) (kind 2)))) (hash "1b8rqrxcxkvdq47b5dj69ig7cndlc1jxyj0rzp9rfhf6yfv4yzk7")))

(define-public crate-nagiosplugin-0.5 (crate (name "nagiosplugin") (vers "0.5.1") (deps (list (crate-dep (name "anyhow") (req "^1.0.40") (default-features #t) (kind 2)) (crate-dep (name "thiserror") (req "^1.0.24") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0") (default-features #t) (kind 2)))) (hash "14min6c8g03grcklv1hh5jdha0ga95g2nrpanzz172qxn3nqsgri")))

(define-public crate-nagiosplugin-0.5 (crate (name "nagiosplugin") (vers "0.5.2") (deps (list (crate-dep (name "anyhow") (req "^1.0.40") (default-features #t) (kind 2)) (crate-dep (name "thiserror") (req "^1.0.24") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0") (default-features #t) (kind 2)))) (hash "006zs07fvfa5mmmv8pf1d7chajwca9vb7l6532a0barcxjv32zpp")))

(define-public crate-nagiosplugin-0.5 (crate (name "nagiosplugin") (vers "0.5.3") (deps (list (crate-dep (name "anyhow") (req "^1.0") (default-features #t) (kind 2)) (crate-dep (name "thiserror") (req "^1.0") (default-features #t) (kind 0)))) (hash "00l5rgx9y9y7359n6xbq6sqa9dc3d91540vlrvva6g3pqyq4sxwl")))

(define-public crate-nagiosplugin-0.5 (crate (name "nagiosplugin") (vers "0.5.4") (deps (list (crate-dep (name "anyhow") (req "^1.0") (default-features #t) (kind 2)) (crate-dep (name "thiserror") (req "^1.0") (default-features #t) (kind 0)))) (hash "1i6qwsjhxd58qkazcyqq9grrz2890ds50mzjz3wkyrjiqzzakkws")))

(define-public crate-nagiosplugin-0.6 (crate (name "nagiosplugin") (vers "0.6.0") (deps (list (crate-dep (name "anyhow") (req "^1.0") (default-features #t) (kind 2)) (crate-dep (name "thiserror") (req "^1.0") (default-features #t) (kind 0)))) (hash "1njcvvgribz186pkg6gslcvxcy4bp223wivwxp20dna4fk641d3m")))

(define-public crate-nagiosplugin-0.7 (crate (name "nagiosplugin") (vers "0.7.0") (deps (list (crate-dep (name "anyhow") (req "^1.0") (default-features #t) (kind 2)) (crate-dep (name "thiserror") (req "^1.0") (default-features #t) (kind 0)))) (hash "0vm18llzcrmyglq3xfby36d8nlirgl9chx2lx2kn9qxk4wana827")))

