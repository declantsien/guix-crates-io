(define-module (crates-io na ug) #:use-module (crates-io))

(define-public crate-naught-0.1 (crate (name "naught") (vers "0.1.0-dev") (deps (list (crate-dep (name "futures") (req "^0.1.25") (default-features #t) (kind 0)) (crate-dep (name "hyper") (req "^0.12.24") (default-features #t) (kind 0)) (crate-dep (name "jch") (req "^1.0.0") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.6.5") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.87") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.38") (default-features #t) (kind 0)) (crate-dep (name "tokio-sync") (req "^0.1.1") (default-features #t) (kind 0)) (crate-dep (name "twox-hash") (req "^1.1.2") (default-features #t) (kind 0)))) (hash "0jk155lw67nz1d8y12r4w235kr97rm2c99q7aqj54n6965kx5968")))

(define-public crate-naughty-strings-0.1 (crate (name "naughty-strings") (vers "0.1.0") (deps (list (crate-dep (name "rustc-serialize") (req "^0.3") (default-features #t) (kind 0)))) (hash "17qfsrwr3ga8l0s9x3xcsya1vimlq9610x11jl9bvjkdxkqll00l") (yanked #t)))

(define-public crate-naughty-strings-0.2 (crate (name "naughty-strings") (vers "0.2.0") (deps (list (crate-dep (name "rustc-serialize") (req "^0.3") (default-features #t) (kind 0)))) (hash "0wh0kwl4jwpzycx6sjkmrfqddanandxin6x0nabbaxj2cr568kwh")))

(define-public crate-naughty-strings-0.2 (crate (name "naughty-strings") (vers "0.2.1") (deps (list (crate-dep (name "rustc-serialize") (req "^0.3") (default-features #t) (kind 0)))) (hash "12cxljf0gwjd9j3n99i6iycz3djxmi62ikvayslv2zwqdm84plpg")))

(define-public crate-naughty-strings-0.2 (crate (name "naughty-strings") (vers "0.2.2") (deps (list (crate-dep (name "rustc-serialize") (req "^0.3") (default-features #t) (kind 0)))) (hash "079i5h43236j61qgiwmfiw6ml9z7zm9l6azxfac6wd3ihrcmviqb")))

(define-public crate-naughty-strings-0.2 (crate (name "naughty-strings") (vers "0.2.3") (deps (list (crate-dep (name "serde_json") (req "^1") (default-features #t) (kind 0)))) (hash "131w22gnzigq06pc2saf5mpgffdphpxapf9xfldqh22qvwg1kb3d")))

(define-public crate-naughty-strings-0.2 (crate (name "naughty-strings") (vers "0.2.4") (deps (list (crate-dep (name "serde_json") (req "^1") (default-features #t) (kind 0)))) (hash "03srmajmasqzcg2cz0n1bbvvsjdiv776lab9lm9l8d4i3w10pxkp")))

(define-public crate-naughty_lib-0.1 (crate (name "naughty_lib") (vers "0.1.0") (deps (list (crate-dep (name "lib_with_cfgs") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "0d1q146pwfiik9a6xk9adnhxgkisxxgxabpl4mx3kg25asws9f18")))

(define-public crate-naughtyfy-0.1 (crate (name "naughtyfy") (vers "0.1.0") (hash "1idhfdq5bxzyy64fhab3i03jcipwzimwq7jv3xviaphjyal6xjp9") (yanked #t)))

(define-public crate-naughtyfy-0.0.1 (crate (name "naughtyfy") (vers "0.0.1") (deps (list (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2.139") (default-features #t) (kind 0)))) (hash "0nspsaflyn3icccwqd4qsf1a53afallyy0fhj5w42dqc1cfiw14p")))

(define-public crate-naughtyfy-0.0.2 (crate (name "naughtyfy") (vers "0.0.2") (deps (list (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2.139") (default-features #t) (kind 0)))) (hash "03ipshi2khh6qp523n4lx1j5qfywh9rz1z0i59hq8dd7j8c4f412")))

(define-public crate-naughtyfy-0.0.3 (crate (name "naughtyfy") (vers "0.0.3") (deps (list (crate-dep (name "libc") (req "^0.2.139") (default-features #t) (kind 0)))) (hash "1hrkbxf51byvxnz04c6v6d36i1k9ykwpwlbbr259wxwkwzkq248v")))

(define-public crate-naughtyfy-0.0.4 (crate (name "naughtyfy") (vers "0.0.4") (deps (list (crate-dep (name "libc") (req "^0.2.139") (default-features #t) (kind 0)))) (hash "1gi27lqi1bh4ci370y13kmj3syvk6zk5frmjsq3hdpzwg9sw7fz5")))

(define-public crate-naughtyfy-0.0.5 (crate (name "naughtyfy") (vers "0.0.5") (deps (list (crate-dep (name "libc") (req "^0.2.139") (default-features #t) (kind 0)))) (hash "0yj9r0n3iqs9rpca8s3ijsssk13v3958x6ny53wqx8c2b3z55hgc")))

(define-public crate-naughtyfy-0.0.6 (crate (name "naughtyfy") (vers "0.0.6") (deps (list (crate-dep (name "libc") (req "^0.2.139") (default-features #t) (kind 0)))) (hash "0kyjgrr0988y517s7j75dc9875x25vcmvwpjz34x74di6grf27n8")))

(define-public crate-naughtyfy-0.0.7 (crate (name "naughtyfy") (vers "0.0.7") (deps (list (crate-dep (name "libc") (req "^0.2.139") (default-features #t) (kind 0)))) (hash "1ya09mr3cb04zycssnjsc0ra1zjzbsmbdc8rzl6yzdxpa3y9sqk5")))

(define-public crate-naughtyfy-0.0.8 (crate (name "naughtyfy") (vers "0.0.8") (deps (list (crate-dep (name "libc") (req "^0.2.139") (default-features #t) (kind 0)))) (hash "11361nvmpwwr1qgnfaxks82jm58g91wi57m2a1mf1pv1x6avqbyv")))

(define-public crate-naughtyfy-0.0.9 (crate (name "naughtyfy") (vers "0.0.9") (deps (list (crate-dep (name "libc") (req "^0.2.139") (default-features #t) (kind 0)))) (hash "071dd9awxglzn27dmg84flks735mp2ih9ga0ww9vzcgrvsa96fvl")))

(define-public crate-naughtyfy-0.0.10 (crate (name "naughtyfy") (vers "0.0.10") (deps (list (crate-dep (name "libc") (req "^0.2.139") (default-features #t) (kind 0)))) (hash "12wjg2gcv6nbz7p6qj4mdwa3cjll73j8x7q45xik9pwq20xhdap0")))

(define-public crate-naughtyfy-0.1 (crate (name "naughtyfy") (vers "0.1.1") (deps (list (crate-dep (name "libc") (req "^0.2.139") (default-features #t) (kind 0)))) (hash "1izjwf8fhzxrqizc42sgqf0paph7qvfn16x8s8b942fmy4q5gcsd")))

(define-public crate-naughtyfy-0.2 (crate (name "naughtyfy") (vers "0.2.0") (deps (list (crate-dep (name "libc") (req "^0.2.139") (default-features #t) (kind 0)))) (hash "1b3rcga8xky4vlbh92h76q9sa10cng5dcj54jqx41skfz5yn99iz")))

(define-public crate-naughtyfy-0.2 (crate (name "naughtyfy") (vers "0.2.1") (deps (list (crate-dep (name "libc") (req "^0.2.150") (default-features #t) (kind 0)))) (hash "0w91dvsy9mwnwwhb032v0rajs5kiprgjj9fggcmdlkkxdqbhll6k")))

(define-public crate-naughtyspy-0.0.0 (crate (name "naughtyspy") (vers "0.0.0") (hash "0389cj2jf2k3m69zmvsw7v027g3z7cawyz71s0ymzwpa27dygzhh")))

