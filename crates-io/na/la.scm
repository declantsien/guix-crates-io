(define-module (crates-io na la) #:use-module (crates-io))

(define-public crate-nalar-0.0.1 (crate (name "nalar") (vers "0.0.1") (deps (list (crate-dep (name "regex") (req "^1.8.3") (default-features #t) (kind 0)))) (hash "1ng5a25dlzrlngf9aw134fdpmajv2ff18hrmb3lya3widvnggrca") (features (quote (("utils" "errors") ("errors") ("default" "utils")))) (yanked #t)))

(define-public crate-nalar-0.0.0 (crate (name "nalar") (vers "0.0.0") (hash "1vy5l98sdv7qp8ad7fa2l3crxr83ablff38qjsqwcc7jdk66cg6a") (yanked #t)))

