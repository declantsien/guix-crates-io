(define-module (crates-io hx ca) #:use-module (crates-io))

(define-public crate-hxcadaptor-0.1 (crate (name "hxcadaptor") (vers "0.1.0") (deps (list (crate-dep (name "hxcadaptor-sys") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "1mxdh120z21ssbz1cgpfpdfzx362lmrl5fbr2jf8lcsg8yirxvgi")))

(define-public crate-hxcadaptor-0.1 (crate (name "hxcadaptor") (vers "0.1.1") (deps (list (crate-dep (name "hxcadaptor-sys") (req "^0.1.1") (default-features #t) (kind 0)))) (hash "07qm7fvz5xypclqv86sbmxiw9i9klf5fbxkz6iy48ckwhf17fzcv")))

(define-public crate-hxcadaptor-0.1 (crate (name "hxcadaptor") (vers "0.1.2") (deps (list (crate-dep (name "hxcadaptor-sys") (req "^0.1.2") (default-features #t) (kind 0)))) (hash "0w4zj1c7dih8ma31h6qsc82arcs1y0xl55rs5z51jcsg3bqdmm37")))

(define-public crate-hxcadaptor-0.1 (crate (name "hxcadaptor") (vers "0.1.3") (deps (list (crate-dep (name "hxcadaptor-sys") (req "^0.1.3") (default-features #t) (kind 0)))) (hash "0dplc772c63zicd9zgj4aggsyp6aiziq6aigzaszlxd2v1hbq71b")))

(define-public crate-hxcadaptor-0.1 (crate (name "hxcadaptor") (vers "0.1.4") (deps (list (crate-dep (name "hxcadaptor-sys") (req "^0.1.4") (default-features #t) (kind 0)))) (hash "0xhzprkpxvfz0qk3p8pmzh8p1iw21can0x74wcb9asqy4w9w8xc5")))

(define-public crate-hxcadaptor-sys-0.1 (crate (name "hxcadaptor-sys") (vers "0.1.0") (deps (list (crate-dep (name "bindgen") (req "^0.65.1") (default-features #t) (kind 1)) (crate-dep (name "copy_dir") (req "^0.1.3") (default-features #t) (kind 1)) (crate-dep (name "dunce") (req "^1.0.4") (default-features #t) (kind 1)) (crate-dep (name "make-cmd") (req "^0.1.0") (default-features #t) (kind 1)))) (hash "04i3kqlhalihk6451vlizba4f1ybg99j66lx905nj6kwkm8ijs91") (links "hxcadaptor")))

(define-public crate-hxcadaptor-sys-0.1 (crate (name "hxcadaptor-sys") (vers "0.1.1") (deps (list (crate-dep (name "bindgen") (req "^0.65.1") (default-features #t) (kind 1)) (crate-dep (name "copy_dir") (req "^0.1.3") (default-features #t) (kind 1)) (crate-dep (name "dunce") (req "^1.0.4") (default-features #t) (kind 1)) (crate-dep (name "make-cmd") (req "^0.1.0") (default-features #t) (kind 1)))) (hash "1giazb8z3zj4g2h1wbz86wa3kjhx9i71qiy7xnxsnhcknmd1xa37") (links "hxcadaptor")))

(define-public crate-hxcadaptor-sys-0.1 (crate (name "hxcadaptor-sys") (vers "0.1.2") (deps (list (crate-dep (name "bindgen") (req "^0.65.1") (default-features #t) (kind 1)) (crate-dep (name "copy_dir") (req "^0.1.3") (default-features #t) (kind 1)) (crate-dep (name "dunce") (req "^1.0.4") (default-features #t) (kind 1)) (crate-dep (name "make-cmd") (req "^0.1.0") (default-features #t) (kind 1)))) (hash "0b1jd2zkzc9x7y8q9h5sb5b53jhbrszxiw8hdhg9hmc21yd166j8") (links "hxcadaptor")))

(define-public crate-hxcadaptor-sys-0.1 (crate (name "hxcadaptor-sys") (vers "0.1.3") (deps (list (crate-dep (name "bindgen") (req "^0.65.1") (default-features #t) (kind 1)) (crate-dep (name "copy_dir") (req "^0.1.3") (default-features #t) (kind 1)) (crate-dep (name "dunce") (req "^1.0.4") (default-features #t) (kind 1)) (crate-dep (name "make-cmd") (req "^0.1.0") (default-features #t) (kind 1)))) (hash "0cl50q6c3irlanj2chka3mw6rb720ncnfvpk6g09dsqmzf5hpslk") (links "hxcadaptor")))

(define-public crate-hxcadaptor-sys-0.1 (crate (name "hxcadaptor-sys") (vers "0.1.4") (deps (list (crate-dep (name "bindgen") (req "^0.65.1") (default-features #t) (kind 1)) (crate-dep (name "copy_dir") (req "^0.1.3") (default-features #t) (kind 1)) (crate-dep (name "dunce") (req "^1.0.4") (default-features #t) (kind 1)) (crate-dep (name "make-cmd") (req "^0.1.0") (default-features #t) (kind 1)))) (hash "1cn10jx5lcw3hiab5z474ivvw70kw1glynk7hr0j7rxrp7ip5mnd") (links "hxcadaptor")))

(define-public crate-hxcadaptor-sys-0.1 (crate (name "hxcadaptor-sys") (vers "0.1.5") (deps (list (crate-dep (name "bindgen") (req "^0.68.1") (default-features #t) (kind 1)) (crate-dep (name "copy_dir") (req "^0.1.3") (default-features #t) (kind 1)) (crate-dep (name "dunce") (req "^1.0.4") (default-features #t) (kind 1)) (crate-dep (name "make-cmd") (req "^0.1.0") (default-features #t) (kind 1)))) (hash "1j03w724rya9v1vzgr4j5sw2cr2zy9ljizv1zk0q6dzq0izixij6") (links "hxcadaptor")))

