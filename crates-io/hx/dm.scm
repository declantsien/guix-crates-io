(define-module (crates-io hx dm) #:use-module (crates-io))

(define-public crate-hxdmp-0.1 (crate (name "hxdmp") (vers "0.1.0") (deps (list (crate-dep (name "lazy_static") (req "^1") (default-features #t) (kind 2)))) (hash "1m3fdzflhp44ianl1bl7h63pikl0085rhjv3fvvyiqjxrq1pgvis")))

(define-public crate-hxdmp-0.2 (crate (name "hxdmp") (vers "0.2.0") (deps (list (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 2)))) (hash "09njjriy0wgkm2sigj7bdlr7xgdms45dzw3m1mwsgcc3kqnl9zqx") (yanked #t)))

(define-public crate-hxdmp-0.2 (crate (name "hxdmp") (vers "0.2.1") (deps (list (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 2)))) (hash "1c66j4z423w2lc3iqzzbg10y8ip58i90lpx7mimq8rklibr2fyx1")))

(define-public crate-hxdmp-rs-0.1 (crate (name "hxdmp-rs") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^3.0.0-beta.2") (default-features #t) (kind 0)))) (hash "0cdqgp5ncm0hcbsc2iax48cw6jjn4311gg01bp68snj5mcjfgiyv") (yanked #t)))

(define-public crate-hxdmp-rs-0.1 (crate (name "hxdmp-rs") (vers "0.1.1") (deps (list (crate-dep (name "clap") (req "^3.0.0-beta.2") (default-features #t) (kind 0)))) (hash "18qcvvb4xgksf0kxrq9mw4q2yvz42wyljd8g47k8hwn2amlz1n3z") (yanked #t)))

