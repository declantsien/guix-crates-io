(define-module (crates-io hx #{71}#) #:use-module (crates-io))

(define-public crate-hx711-0.1 (crate (name "hx711") (vers "0.1.0") (deps (list (crate-dep (name "embedded-hal") (req "^0.2.2") (features (quote ("unproven"))) (default-features #t) (kind 0)) (crate-dep (name "nb") (req "^0.1.1") (default-features #t) (kind 0)))) (hash "1x7rabr8kwg1ry9bn2qcdib82rc0vd0c3fhj0lzb97r3fpv66zri")))

(define-public crate-hx711-0.2 (crate (name "hx711") (vers "0.2.0") (deps (list (crate-dep (name "embedded-hal") (req "^0.2.3") (features (quote ("unproven"))) (default-features #t) (kind 0)) (crate-dep (name "nb") (req "^0.1.1") (default-features #t) (kind 0)))) (hash "1jrr8i25cd53y67bwg6nclsz1d0jixdvawx039jr53a3lfq6ljif")))

(define-public crate-hx711-0.2 (crate (name "hx711") (vers "0.2.1") (deps (list (crate-dep (name "embedded-hal") (req "^0.2.3") (features (quote ("unproven"))) (default-features #t) (kind 0)) (crate-dep (name "nb") (req "^0.1.1") (default-features #t) (kind 0)))) (hash "1zkcdda70iyiyjirwak6r58jlmsmnn6ysz5jjib2q6f67320cavd")))

(define-public crate-hx711-0.3 (crate (name "hx711") (vers "0.3.0") (deps (list (crate-dep (name "embedded-hal") (req "^0.2.3") (features (quote ("unproven"))) (default-features #t) (kind 0)) (crate-dep (name "nb") (req "^0.1.2") (default-features #t) (kind 0)))) (hash "1rahhb792afcz2lsvp01mi3rqdq15si1xsn7dzabgx40xqvbhs4r")))

(define-public crate-hx711-0.4 (crate (name "hx711") (vers "0.4.0") (deps (list (crate-dep (name "embedded-hal") (req "^0.2.3") (features (quote ("unproven"))) (default-features #t) (kind 0)) (crate-dep (name "nb") (req "^0.1.2") (default-features #t) (kind 0)))) (hash "151v5jq5hanl0h5xhan7w4iv60fclk5ya792srh0a32kyxd9jkff")))

(define-public crate-hx711-0.5 (crate (name "hx711") (vers "0.5.0") (deps (list (crate-dep (name "embedded-hal") (req "^0.2.4") (features (quote ("unproven"))) (default-features #t) (kind 0)) (crate-dep (name "nb") (req "^0.1.2") (default-features #t) (kind 0)))) (hash "14a6qvnbwph8lypc6vykfn59rnvpilhvjn26xds8sdrnl3ii9dcd") (features (quote (("never_type"))))))

(define-public crate-hx711-0.6 (crate (name "hx711") (vers "0.6.0") (deps (list (crate-dep (name "embedded-hal") (req "^0.2.4") (features (quote ("unproven"))) (default-features #t) (kind 0)) (crate-dep (name "nb") (req "^1.0.0") (default-features #t) (kind 0)))) (hash "187z76yps70r923zzmrq3aag65aiyl8j8p2pma6zmg84db106s3v") (features (quote (("never_type"))))))

(define-public crate-hx711_spi-0.2 (crate (name "hx711_spi") (vers "0.2.1") (deps (list (crate-dep (name "bitmatch") (req "^0.1.1") (default-features #t) (kind 0)) (crate-dep (name "embedded-hal") (req "^0.2.4") (default-features #t) (kind 0)))) (hash "1d03hf9cci17cl8b8f3p3q9ryccnw5ap9cx6h33xsprfijf5xmxk")))

(define-public crate-hx711_spi-0.3 (crate (name "hx711_spi") (vers "0.3.1") (deps (list (crate-dep (name "bitmatch") (req "^0.1.1") (default-features #t) (kind 0)) (crate-dep (name "embedded-hal") (req "^0.2.4") (default-features #t) (kind 0)) (crate-dep (name "nb") (req "^1.0.0") (default-features #t) (kind 0)))) (hash "1kir5j3p6gcapvzppbxfdpsv468mghbr2h28fy3ca6v8wwpsar2p")))

(define-public crate-hx711_spi-0.3 (crate (name "hx711_spi") (vers "0.3.2") (deps (list (crate-dep (name "bitmatch") (req "^0.1.1") (default-features #t) (kind 0)) (crate-dep (name "embedded-hal") (req "^0.2.4") (default-features #t) (kind 0)) (crate-dep (name "nb") (req "^1.0.0") (default-features #t) (kind 0)) (crate-dep (name "test-case") (req "^2.0.2") (default-features #t) (kind 2)))) (hash "055spl6zbayxvl6h5mmxzzdy07vn1shnlmvq85fnq6b75pmxf9c1")))

(define-public crate-hx711_spi-0.4 (crate (name "hx711_spi") (vers "0.4.0") (deps (list (crate-dep (name "bitmatch") (req "^0.1.1") (default-features #t) (kind 0)) (crate-dep (name "embedded-hal") (req "^0.2.4") (default-features #t) (kind 0)) (crate-dep (name "embedded-hal-mock") (req "^0.8.0") (default-features #t) (kind 2)) (crate-dep (name "nb") (req "^1.0.0") (default-features #t) (kind 0)) (crate-dep (name "rppal") (req "^0.12.0") (features (quote ("hal"))) (default-features #t) (kind 2)) (crate-dep (name "test-case") (req "^2.0.2") (default-features #t) (kind 2)))) (hash "1hjj2krzid9z9wxdcr9p47z1z4a36hy1jck21n1rl6gm7sc6jp4w")))

(define-public crate-hx711_spi-0.4 (crate (name "hx711_spi") (vers "0.4.2") (deps (list (crate-dep (name "bitmatch") (req "^0.1.1") (default-features #t) (kind 0)) (crate-dep (name "embedded-hal") (req "^0.2.4") (default-features #t) (kind 0)) (crate-dep (name "nb") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "embedded-hal-mock") (req "^0.9.0") (default-features #t) (kind 2)) (crate-dep (name "rppal") (req "^0.14.1") (features (quote ("hal"))) (default-features #t) (kind 2)) (crate-dep (name "test-case") (req "^3") (default-features #t) (kind 2)))) (hash "1y8kmgmv21yyhq68yhlxi5zlnyq35lhgk94c9p9hpiwzrchrqy66")))

(define-public crate-hx711_spi-0.5 (crate (name "hx711_spi") (vers "0.5.0") (deps (list (crate-dep (name "bitmatch") (req "^0.1.1") (default-features #t) (kind 0)) (crate-dep (name "embedded-hal") (req "^0.2.4") (default-features #t) (kind 0)) (crate-dep (name "nb") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "embedded-hal-mock") (req "^0.9.0") (default-features #t) (kind 2)) (crate-dep (name "rppal") (req "^0.14.1") (features (quote ("hal"))) (default-features #t) (kind 2)) (crate-dep (name "test-case") (req "^3.0") (default-features #t) (kind 2)))) (hash "0ibc79fnc1g3qnx6i73yr17wjbbpyzclfr4j8jyr549f3a6ymqna") (features (quote (("invert-sdo"))))))

