(define-module (crates-io hx ml) #:use-module (crates-io))

(define-public crate-hxml-0.1 (crate (name "hxml") (vers "0.1.0") (hash "1k1rw21bwjacaj94r089837gxh5fjlb9dv475wf27az5622zls4j")))

(define-public crate-hxml-0.2 (crate (name "hxml") (vers "0.2.0") (hash "1lczcmpbbk6jaqw8falq9lzrf2s6z6vpvdk0x25vbifc1ww2dkx3")))

(define-public crate-hxml-0.2 (crate (name "hxml") (vers "0.2.1") (hash "1m2hr3h8biq65s5y6lx2zfhzc7slflf7xpycp0x5k7hbdyv1p234")))

(define-public crate-hxml-0.2 (crate (name "hxml") (vers "0.2.2") (hash "1f1jllp7w8apg8p8kqpkg221b0zgjngbqz2lmjwmn4qlycixh3q0")))

(define-public crate-hxml-0.2 (crate (name "hxml") (vers "0.2.3") (hash "13lh9a8mr9jw2gw5jc8bn88phg0a05z9ywmbjsilj5zdmkpldz8p")))

(define-public crate-hxml-0.3 (crate (name "hxml") (vers "0.3.0") (hash "1lhmkgydpfdpivd5bq9xa0dfbxdpbykwim0fk71k0y4q91ywjqx0")))

(define-public crate-hxml-0.3 (crate (name "hxml") (vers "0.3.1") (hash "07zdff65nqw4y2rdj9xwq8ixqj9dgyhwx2qflzbhbrvydr99b50g")))

(define-public crate-hxml-0.3 (crate (name "hxml") (vers "0.3.2") (hash "1ag0nvb5k8p0psgyp7xaarg0bsv7abanz9ifdyf792yannwpypcd")))

(define-public crate-hxml-0.3 (crate (name "hxml") (vers "0.3.3") (hash "0263l7gw6i4qy02pcz2acmb91ad45p3gqybh247aq2x7q1l31wjz")))

