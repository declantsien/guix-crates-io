(define-module (crates-io vu #{64}#) #:use-module (crates-io))

(define-public crate-vu64-0.1 (crate (name "vu64") (vers "0.1.0") (hash "0whwjvziswqwng498blp72m5kg46a8h3hgipygiph03xxrvdf1md") (features (quote (("io") ("default" "io"))))))

(define-public crate-vu64-0.1 (crate (name "vu64") (vers "0.1.1") (hash "13nxaxhrbabni3a48150nd2hd6a9zwh92r4vag5x3r389b58b33i") (features (quote (("io") ("default" "io"))))))

(define-public crate-vu64-0.1 (crate (name "vu64") (vers "0.1.2") (hash "14rkcmr4lj9hbw10fj7j30lrwd5qslmk19h70fjhpjcxxaa5qfsn") (features (quote (("io") ("default" "io"))))))

(define-public crate-vu64-0.1 (crate (name "vu64") (vers "0.1.3") (hash "16j36g9jyljgdzqbfkqrf6w5y48diwrb8zmzsw5800vkamrjalx5") (features (quote (("io") ("default" "io"))))))

(define-public crate-vu64-0.1 (crate (name "vu64") (vers "0.1.4") (hash "1x95b4n40y26zpf7dpngx6pg1sb8dji4n31i83wn2i39c6i6iysk") (features (quote (("vu64_debug") ("io") ("default" "io"))))))

(define-public crate-vu64-0.1 (crate (name "vu64") (vers "0.1.5") (hash "1vkbdvimzw6vv9kssh5188ckl99k3mf8557yh7234ljwwjq5vq7l") (features (quote (("vu64_debug") ("io") ("default" "io"))))))

(define-public crate-vu64-0.1 (crate (name "vu64") (vers "0.1.6") (hash "1zbhncsnwng7zfhf2fwi7qiaxfibk281kl72336p381mxcrqsmb2") (features (quote (("vu64_debug") ("io") ("default" "io"))))))

(define-public crate-vu64-0.1 (crate (name "vu64") (vers "0.1.7") (hash "1p4z34zqvqxm5mxagklwfz6qvg7b68q8zx8ad92r8w8zilkramsq") (features (quote (("vu64_debug") ("io") ("default" "io")))) (rust-version "1.56.0")))

(define-public crate-vu64-0.1 (crate (name "vu64") (vers "0.1.8") (hash "1d4qalqxjc5ycjafqj5w45if4p4lp706x85afkk9xlv42909rkqy") (features (quote (("vu64_debug") ("io") ("default" "io")))) (rust-version "1.56.0")))

(define-public crate-vu64-0.1 (crate (name "vu64") (vers "0.1.9") (hash "1xgqxl8fgk9vyr8zafxfg05xgihgy1rs84lzix3w2n1fjn2am8jn") (features (quote (("vu64_debug") ("io") ("default" "io")))) (rust-version "1.58.0")))

(define-public crate-vu64-0.1 (crate (name "vu64") (vers "0.1.10") (hash "0hsnvxd9ginfgpdw7s6y0rxari371xp7fkrn0mqjjzh9z9rvy675") (features (quote (("vu64_debug") ("io") ("default" "io")))) (rust-version "1.58.0")))

