(define-module (crates-io vu e_) #:use-module (crates-io))

(define-public crate-vue_convert-0.1 (crate (name "vue_convert") (vers "0.1.1") (deps (list (crate-dep (name "askama") (req "^0.10.1") (default-features #t) (kind 0)) (crate-dep (name "heck") (req "^0.3.1") (default-features #t) (kind 0)) (crate-dep (name "select") (req "^0.4.3") (default-features #t) (kind 0)))) (hash "0v8524c33ipqj46ivjngfhmz4fp83qp4mzykq2hv5nhwqv217lng")))

