(define-module (crates-io vu e-) #:use-module (crates-io))

(define-public crate-vue-compiler-core-0.1 (crate (name "vue-compiler-core") (vers "0.1.0") (deps (list (crate-dep (name "bitflags") (req "^1.3") (default-features #t) (kind 0)) (crate-dep (name "insta") (req "^1.8.0") (default-features #t) (kind 2)) (crate-dep (name "json") (req "^0.12.4") (default-features #t) (kind 0)) (crate-dep (name "rslint_parser") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "rustc-hash") (req "^1.1.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "smallvec") (req "^1.7.0") (default-features #t) (kind 0)))) (hash "19mhjyyr337rpdz1rmajf5nvf9bs2wby6xdbdr64zq0hcj8x6417") (features (quote (("default" "serde" "smallvec/serde"))))))

(define-public crate-vue-sfc-0.1 (crate (name "vue-sfc") (vers "0.1.0") (deps (list (crate-dep (name "nom") (req "^7.1") (default-features #t) (kind 0)))) (hash "1wx0rbvs6yi3k390rnr2hb2jc8z0k1p0zszf62a7n7klqxdk8481") (yanked #t)))

(define-public crate-vue-sfc-0.2 (crate (name "vue-sfc") (vers "0.2.0") (deps (list (crate-dep (name "nom") (req "^7.1") (default-features #t) (kind 0)))) (hash "1kqnry8g1kijqswshb6laxmajhdfmarrak7kyn76nixqvf5z2385")))

(define-public crate-vue-sfc-0.3 (crate (name "vue-sfc") (vers "0.3.0") (deps (list (crate-dep (name "nom") (req "^7.1") (default-features #t) (kind 0)))) (hash "07xgrxxvww6ah7qd2m4n13rg9akjl53pay7ra1hgraihgdzvngrz")))

(define-public crate-vue-sfc-0.3 (crate (name "vue-sfc") (vers "0.3.1") (deps (list (crate-dep (name "criterion") (req "^0.3.5") (default-features #t) (kind 2)) (crate-dep (name "memchr") (req "^2.4.1") (default-features #t) (kind 0)) (crate-dep (name "nom") (req "^7.1") (default-features #t) (kind 0)) (crate-dep (name "pprof") (req "^0.6.2") (features (quote ("flamegraph" "criterion"))) (default-features #t) (kind 2)))) (hash "0b64xipl02mhpinm7dnxinhkxmm3qlk993vdr2gkjnv3kaxzrs0q")))

(define-public crate-vue-sfc-0.3 (crate (name "vue-sfc") (vers "0.3.2") (deps (list (crate-dep (name "criterion") (req "^0.3.5") (default-features #t) (kind 2)) (crate-dep (name "memchr") (req "^2.4.1") (default-features #t) (kind 0)) (crate-dep (name "nom") (req "^7.1") (default-features #t) (kind 0)) (crate-dep (name "pprof") (req "^0.6.2") (features (quote ("flamegraph" "criterion"))) (default-features #t) (kind 2)))) (hash "0jkm5xv8dvlrw1rzwsrqa1gqnz7xj4d3mz2nvv0xd8brylk6z0n8")))

