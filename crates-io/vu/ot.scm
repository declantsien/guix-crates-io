(define-module (crates-io vu ot) #:use-module (crates-io))

(define-public crate-vuot-0.0.1 (crate (name "vuot") (vers "0.0.1") (deps (list (crate-dep (name "bumpalo") (req "^3") (features (quote ("allocator_api"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "pollster") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "smol") (req "^2") (default-features #t) (kind 2)) (crate-dep (name "smol-macros") (req "^0.1") (default-features #t) (kind 2)))) (hash "1892km2gvznlq8smqvs9mawgajkvi7pga5g6vwkmk1c7bx6p6kgr") (v 2) (features2 (quote (("bumpalo" "dep:bumpalo"))))))

