(define-module (crates-io vu et) #:use-module (crates-io))

(define-public crate-vuetom-0.0.1 (crate (name "vuetom") (vers "0.0.1") (hash "1i5y1b8pa8d15mg77lf20vk2d29krk2f5ahiwzg3l96gy9s4y1z3") (rust-version "1.60")))

(define-public crate-vuetom_dc-0.0.1 (crate (name "vuetom_dc") (vers "0.0.1") (deps (list (crate-dep (name "jni") (req "^0.21") (default-features #t) (kind 0)) (crate-dep (name "vuetom") (req "^0.0.1") (default-features #t) (kind 2)))) (hash "1vcvpwi3c9jqiva9ipj7rji599dp7812qn8lav0q1h0az9hlcqvi") (rust-version "1.60")))

