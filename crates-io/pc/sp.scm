(define-module (crates-io pc sp) #:use-module (crates-io))

(define-public crate-pcspecs-0.1 (crate (name "pcspecs") (vers "0.1.0") (deps (list (crate-dep (name "os_info") (req "^3") (default-features #t) (kind 0)) (crate-dep (name "sysinfo") (req "^0.26.7") (default-features #t) (kind 0)))) (hash "1aww8insyb8cch9zvxadvbz6pclnmy1fpkmhy7g5dqign99r8jff") (yanked #t)))

(define-public crate-pcspecs-0.1 (crate (name "pcspecs") (vers "0.1.1") (deps (list (crate-dep (name "os_info") (req "^3") (default-features #t) (kind 0)) (crate-dep (name "sysinfo") (req "^0.26.7") (default-features #t) (kind 0)))) (hash "0l854g7qj7ch9hd9hqnsl4n3799rmcswf92gawrnwf0n031iwr9x") (yanked #t)))

(define-public crate-pcspecs-0.2 (crate (name "pcspecs") (vers "0.2.0") (deps (list (crate-dep (name "os_info") (req "^3") (default-features #t) (kind 0)) (crate-dep (name "sysinfo") (req "^0.26.7") (default-features #t) (kind 0)))) (hash "17dyp51cnrb9rv4z0x94kmmrn6gxjs8imhrbl8rlrkh1shfaivw1") (yanked #t)))

(define-public crate-pcspecs-0.2 (crate (name "pcspecs") (vers "0.2.1") (deps (list (crate-dep (name "os_info") (req "^3") (default-features #t) (kind 0)) (crate-dep (name "sysinfo") (req "^0.26.7") (default-features #t) (kind 0)))) (hash "0hiqyyy024m1by73wih082a85xzdprhrkspas2wn3cqaglzs530g")))

