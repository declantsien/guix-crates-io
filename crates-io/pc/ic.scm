(define-module (crates-io pc ic) #:use-module (crates-io))

(define-public crate-pcics-0.1 (crate (name "pcics") (vers "0.1.0") (deps (list (crate-dep (name "byte") (req "^0.2.6") (default-features #t) (kind 0)) (crate-dep (name "displaydoc") (req "^0.2.3") (default-features #t) (kind 0)) (crate-dep (name "modular-bitfield") (req "^0.11.2") (default-features #t) (kind 0)) (crate-dep (name "pretty_assertions") (req "^0.7") (default-features #t) (kind 2)))) (hash "1r0wrl6xcha2l5s9rl0v6ch2mk0nl0cp2wg6gagp8s01fivkb25m")))

(define-public crate-pcics-0.1 (crate (name "pcics") (vers "0.1.1") (deps (list (crate-dep (name "byte") (req "^0.2.6") (default-features #t) (kind 0)) (crate-dep (name "displaydoc") (req "^0.2.3") (default-features #t) (kind 0)) (crate-dep (name "modular-bitfield") (req "^0.11.2") (default-features #t) (kind 0)) (crate-dep (name "pretty_assertions") (req "^0.7") (default-features #t) (kind 2)))) (hash "1nzhs5zz85qqxrwnsspd79d4rfyn788pgl73w9xfrn70x241434a")))

(define-public crate-pcics-0.2 (crate (name "pcics") (vers "0.2.0") (deps (list (crate-dep (name "heterob") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "pretty_assertions") (req "^0.7") (default-features #t) (kind 2)) (crate-dep (name "snafu") (req "^0.7.1") (features (quote ("rust_1_46"))) (kind 0)))) (hash "0i0835r653xcrbvb6pgs05p0rf49ialhh9khshbwwqdcgl5b256x") (features (quote (("caps_ea_real_entry_size"))))))

(define-public crate-pcics-0.3 (crate (name "pcics") (vers "0.3.0") (deps (list (crate-dep (name "heterob") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "pretty_assertions") (req "^0.7") (default-features #t) (kind 2)) (crate-dep (name "snafu") (req "^0.7.1") (features (quote ("rust_1_46"))) (kind 0)))) (hash "0isn3cgmf08s3pl6ispcm1sfc0x461vzqv7xs44095wa9b640yvq") (features (quote (("caps_ea_real_entry_size"))))))

(define-public crate-pcics-0.3 (crate (name "pcics") (vers "0.3.1") (deps (list (crate-dep (name "heterob") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "pretty_assertions") (req "^0.7") (default-features #t) (kind 2)) (crate-dep (name "snafu") (req "^0.7.1") (features (quote ("rust_1_46"))) (kind 0)))) (hash "1746k4kjg8v38idw4ynpvlxw60p2pblagf14avddqz868258i8qr") (features (quote (("caps_ea_real_entry_size"))))))

(define-public crate-pcics-0.3 (crate (name "pcics") (vers "0.3.2") (deps (list (crate-dep (name "heterob") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "pretty_assertions") (req "^0.7") (default-features #t) (kind 2)) (crate-dep (name "snafu") (req "^0.7.1") (features (quote ("rust_1_46"))) (kind 0)))) (hash "15nk5mxx528pprc7klj8ffjzkf939yln54fqsx49pdpyffg27zrd") (features (quote (("caps_ea_real_entry_size"))))))

