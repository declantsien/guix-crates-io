(define-module (crates-io pc li) #:use-module (crates-io))

(define-public crate-pcli-0.1 (crate (name "pcli") (vers "0.1.0") (hash "08xw6ygjvjjksmx0z0n906wdzfsq4m11il9v65qz58xx9k3rbxfx")))

(define-public crate-pclientd-0.1 (crate (name "pclientd") (vers "0.1.0") (hash "08wnfrgrdb5lk8a0f50lzs8y4zzgi42asmr8fax9kq1lvczd5hvy")))

