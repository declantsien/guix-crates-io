(define-module (crates-io pc d_) #:use-module (crates-io))

(define-public crate-pcd_reader-0.1 (crate (name "pcd_reader") (vers "0.1.0") (deps (list (crate-dep (name "byteorder") (req "^1.4.3") (default-features #t) (kind 0)) (crate-dep (name "lzf") (req "^0.3.1") (default-features #t) (kind 0)))) (hash "016zfdqgipm8j9pigk25chzamg10sz355hniyc81wrc066y5lqqz")))

