(define-module (crates-io pc ma) #:use-module (crates-io))

(define-public crate-pcman-0.0.0 (crate (name "pcman") (vers "0.0.0") (deps (list (crate-dep (name "async-std") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "futures") (req "^0.3") (default-features #t) (kind 0)))) (hash "14j8pdqsmc777246wib6cbshfbb4qbamvhzg5brqbsafi8bcvvg2")))

