(define-module (crates-io pc i_) #:use-module (crates-io))

(define-public crate-pci_fetch-0.1 (crate (name "pci_fetch") (vers "0.1.0") (hash "1ini778jgycg4m4n5hdz8w971q35whwqq06cvssc262g9jxdxd1m") (yanked #t)))

(define-public crate-pci_fetch-1 (crate (name "pci_fetch") (vers "1.0.0") (hash "1a7pxnilk1vm9ia6nz2vmrgqrb40c5xq049g2wsjk61sm73kscay") (yanked #t)))

(define-public crate-pci_fetch-2 (crate (name "pci_fetch") (vers "2.0.0") (hash "17757aav8fgrg9v4bsdi9qh5xhzydnmj7rklksjhqj3van9iqmd7") (yanked #t)))

(define-public crate-pci_fetch-2 (crate (name "pci_fetch") (vers "2.1.0") (hash "19yj2345cz5sg8qf0c3wr2vny67s7p6sgqjr45cm758ybp56xs4w") (yanked #t)))

(define-public crate-pci_fetch-2 (crate (name "pci_fetch") (vers "2.2.0") (hash "1pwr48vbsxa217djy614ldjsgbva9pwaza8g2mrymz7pnf539yss") (yanked #t)))

(define-public crate-pci_fetch-2 (crate (name "pci_fetch") (vers "2.2.1") (hash "0x8hjd8c1cmz003af4zv12c52l97prdzc72cx8alsrjahh49yiqm") (yanked #t)))

(define-public crate-pci_types-0.1 (crate (name "pci_types") (vers "0.1.0") (deps (list (crate-dep (name "bit_field") (req "^0.10") (default-features #t) (kind 0)))) (hash "0j5731nw8kwx1mqhjp495hlxh40vf8rflncwgw4kyjxma0wvf877")))

(define-public crate-pci_types-0.2 (crate (name "pci_types") (vers "0.2.0") (deps (list (crate-dep (name "bit_field") (req "^0.10") (default-features #t) (kind 0)))) (hash "01zvws6jwjdlpkadp2625sdyclq2nhn0ilyr3ama9cscmmhkfm5h")))

(define-public crate-pci_types-0.3 (crate (name "pci_types") (vers "0.3.0") (deps (list (crate-dep (name "bit_field") (req "^0.10") (default-features #t) (kind 0)))) (hash "0h3q8qy2gdbpv8mcrhavxn0hlchgnmvfspnyl2r7sig3wlkrmqdj")))

(define-public crate-pci_types-0.4 (crate (name "pci_types") (vers "0.4.0") (deps (list (crate-dep (name "bit_field") (req "^0.10") (default-features #t) (kind 0)))) (hash "0x10ap93hvzyj6rdf0f8xfnkmymi515mdk27b7028r9bcd4siyxj")))

(define-public crate-pci_types-0.5 (crate (name "pci_types") (vers "0.5.0") (deps (list (crate-dep (name "bit_field") (req "^0.10") (default-features #t) (kind 0)))) (hash "190gp52q3rs71khc8fgg5bc5xpb0mcr4pmfqqp6phvm2d1kp6x7c")))

(define-public crate-pci_types-0.6 (crate (name "pci_types") (vers "0.6.0") (deps (list (crate-dep (name "bit_field") (req "^0.10") (default-features #t) (kind 0)) (crate-dep (name "bitflags") (req "^2.4.1") (default-features #t) (kind 0)))) (hash "0qyi1qhswk0hj3lzs9di33cxs5sbkiv9ddlxk3mygzw2qys7hcpw")))

(define-public crate-pci_types-0.6 (crate (name "pci_types") (vers "0.6.1") (deps (list (crate-dep (name "bit_field") (req "^0.10") (default-features #t) (kind 0)) (crate-dep (name "bitflags") (req "^2.4.1") (default-features #t) (kind 0)))) (hash "1ghdc8v5dspzw63p7wvi7i2lq38j90lqj2q7x314nrqhy3mmn7l3")))

(define-public crate-pci_types-0.6 (crate (name "pci_types") (vers "0.6.2") (deps (list (crate-dep (name "bit_field") (req "^0.10") (default-features #t) (kind 0)) (crate-dep (name "bitflags") (req "^2.4.1") (default-features #t) (kind 0)))) (hash "0ycwybmg7zfwgx73gcp75r7h9cqnm4rbd10illhzg48pw4p2pb7b")))

