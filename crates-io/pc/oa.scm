(define-module (crates-io pc oa) #:use-module (crates-io))

(define-public crate-pcoa-0.1 (crate (name "pcoa") (vers "0.1.0") (deps (list (crate-dep (name "nalgebra") (req "^0.32.3") (default-features #t) (kind 0)) (crate-dep (name "simba") (req "^0.8.1") (default-features #t) (kind 0)))) (hash "0qp57gmyn4adg6ydw00y7fg77vb76wmqb3xwx4832pzzx9p595pk")))

(define-public crate-pcoa-0.1 (crate (name "pcoa") (vers "0.1.1") (deps (list (crate-dep (name "nalgebra") (req "^0.32.3") (default-features #t) (kind 0)) (crate-dep (name "simba") (req "^0.8.1") (default-features #t) (kind 0)))) (hash "1mynvsvyja50pyh9vg4q16m003xn0dan5il8nkr95jyaw36ilgqk")))

(define-public crate-pcoa-0.1 (crate (name "pcoa") (vers "0.1.2") (deps (list (crate-dep (name "nalgebra") (req "^0.32.3") (default-features #t) (kind 0)) (crate-dep (name "simba") (req "^0.8.1") (default-features #t) (kind 0)))) (hash "0yx0dsg5bjxqwshxnkgwv72ij0jlw4zzr0s0gp9daf9sdd9xipyi")))

