(define-module (crates-io pc om) #:use-module (crates-io))

(define-public crate-pcomb-0.1 (crate (name "pcomb") (vers "0.1.0") (hash "1c76v7q3whscw7f6n2cknkkc264zf8c6ph75ii8iyg5m3y491fsh")))

(define-public crate-pcomb-0.2 (crate (name "pcomb") (vers "0.2.0") (hash "04ia5pgpzb8s6y2hldkd7svpqxpm3bqzy23l61hmm21sf7gbxr31") (features (quote (("default" "builtin_parsers") ("builtin_parsers"))))))

(define-public crate-pcomb-0.3 (crate (name "pcomb") (vers "0.3.0") (hash "1i06wcdx916hdwdajn6krfqkhsr150ppdwc67m6wh4ffmhsqrv11") (features (quote (("std") ("default" "builtin_parsers" "std") ("builtin_parsers"))))))

(define-public crate-pcompress-0.1 (crate (name "pcompress") (vers "0.1.0") (deps (list (crate-dep (name "mimalloc") (req "^0.1.17") (kind 0)) (crate-dep (name "serde") (req "^1.0.127") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.64") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.3.21") (default-features #t) (kind 0)))) (hash "1r8j02nha6y95viwxw70fii0b86yd2mga1i59hayz8h7zknxmzd6") (yanked #t)))

(define-public crate-pcompress-0.1 (crate (name "pcompress") (vers "0.1.1") (deps (list (crate-dep (name "mimalloc") (req "^0.1.17") (kind 0)) (crate-dep (name "serde") (req "^1.0.127") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.64") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.3.21") (default-features #t) (kind 0)))) (hash "0bz6ih2gmcg4035534lqxqqvax573fchjd1d8d44mslzs55jhssh") (yanked #t)))

(define-public crate-pcompress-0.2 (crate (name "pcompress") (vers "0.2.1") (deps (list (crate-dep (name "mimalloc") (req "^0.1.17") (kind 0)) (crate-dep (name "serde") (req "^1.0.127") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.64") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.3.21") (default-features #t) (kind 0)))) (hash "1skqlqynf13mq67z4717xss8l8yaghhfxc8irn4j6zm4rap9mc9i") (yanked #t)))

(define-public crate-pcompress-0.2 (crate (name "pcompress") (vers "0.2.2") (deps (list (crate-dep (name "mimalloc") (req "^0.1.17") (kind 0)) (crate-dep (name "serde") (req "^1.0.127") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.64") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.3.21") (default-features #t) (kind 0)))) (hash "1v5qkvc97cf8yd33sj80j78yf33szxr3n16jb0rhm8jppq7zgclb") (yanked #t)))

(define-public crate-pcompress-1 (crate (name "pcompress") (vers "1.0.0") (deps (list (crate-dep (name "serde") (req "^1.0.127") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.64") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.3.21") (default-features #t) (kind 0)))) (hash "13n53ni4slxlc0c8rzsn3x6ns3zkl9g2da2l74w64jxdl8h000hb") (yanked #t)))

(define-public crate-pcompress-1 (crate (name "pcompress") (vers "1.0.2") (deps (list (crate-dep (name "serde") (req "^1.0.127") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.64") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.3.21") (default-features #t) (kind 0)))) (hash "1g5xnhdndgxrwjd6rm4b6hz05i29yc8397hjv7rbaggdmw9y1b9b") (yanked #t)))

(define-public crate-pcompress-1 (crate (name "pcompress") (vers "1.0.3") (deps (list (crate-dep (name "serde") (req "^1.0.127") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.64") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.3.21") (default-features #t) (kind 0)))) (hash "1m1vd83ipga6dm6kx9vz0idaza5pr97az9h8pq0mywa44r4ziank") (yanked #t)))

(define-public crate-pcompress-1 (crate (name "pcompress") (vers "1.0.4") (deps (list (crate-dep (name "serde") (req "^1.0.127") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.64") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.3.21") (default-features #t) (kind 0)))) (hash "0lvdvc9rg6wybnhy2klcbywy40dikfvrxvzqzsayqajil1qi4a8h") (yanked #t)))

(define-public crate-pcompress-1 (crate (name "pcompress") (vers "1.0.5") (deps (list (crate-dep (name "serde") (req "^1.0.127") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.64") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.3.21") (default-features #t) (kind 0)))) (hash "0qlgyx9j6h61r1gj70xhg9c4fm48cxj8k9cyzrjnc842vxr83kf4") (yanked #t)))

(define-public crate-pcompress-1 (crate (name "pcompress") (vers "1.0.6") (deps (list (crate-dep (name "serde") (req "^1.0.127") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.64") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.3.21") (default-features #t) (kind 0)))) (hash "1dk30pzn33zhfg5mwpphqcg204kclzm3axd47dqzf81cfikrbv16")))

(define-public crate-pcompress-1 (crate (name "pcompress") (vers "1.0.7") (deps (list (crate-dep (name "serde") (req "^1.0.127") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.64") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.3.21") (default-features #t) (kind 0)))) (hash "019ivckbsd62j59bybqb1hwnr5dmj0c9hzizbm8n0zm3ribkl0jr")))

