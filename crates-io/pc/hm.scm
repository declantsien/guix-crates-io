(define-module (crates-io pc hm) #:use-module (crates-io))

(define-public crate-pchmod-0.1 (crate (name "pchmod") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^2.32.0") (default-features #t) (kind 0)) (crate-dep (name "env_logger") (req "^0.6.1") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.6") (default-features #t) (kind 0)) (crate-dep (name "rayon") (req "^1.0.3") (default-features #t) (kind 0)))) (hash "0wqw3gnvv3d7nk3091x46nbljcxrbln78g261d1xh6jpchhnxhpc")))

