(define-module (crates-io pc sc) #:use-module (crates-io))

(define-public crate-pcsc-0.1 (crate (name "pcsc") (vers "0.1.0-alpha1") (deps (list (crate-dep (name "bitflags") (req "^0.7") (default-features #t) (kind 0)))) (hash "10z7hhdbsnj4a2zs1c41cg52b3cxg1ikm9q4xyp80m9l48hhf05c")))

(define-public crate-pcsc-0.1 (crate (name "pcsc") (vers "0.1.0-alpha2") (deps (list (crate-dep (name "bitflags") (req "^0.7") (default-features #t) (kind 0)))) (hash "1fw2l8mwb8ap8j2cyivmljckr9zm4fm8fgc9fa13kqchsjkgfn1a")))

(define-public crate-pcsc-0.1 (crate (name "pcsc") (vers "0.1.0-alpha3") (deps (list (crate-dep (name "bitflags") (req "^0.7") (default-features #t) (kind 0)))) (hash "0y9qan99yjvm62kj00b8hj96mnkmr4nmw7vwc4hwldanlrd3v6q8")))

(define-public crate-pcsc-0.1 (crate (name "pcsc") (vers "0.1.0-alpha4") (deps (list (crate-dep (name "bitflags") (req "^0.7") (default-features #t) (kind 0)) (crate-dep (name "pcsc-sys") (req "^0.1.0-alpha4") (default-features #t) (kind 0)))) (hash "14m1v0gxb0g5ip3n1bv2gvjgh1h4l1iw6gacdwladmrrlzks12k9")))

(define-public crate-pcsc-0.1 (crate (name "pcsc") (vers "0.1.0") (deps (list (crate-dep (name "bitflags") (req "^0.7") (default-features #t) (kind 0)) (crate-dep (name "pcsc-sys") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "1a6a15kq156n5x582jlrkhzyifvc34sg5p6a0ik7ik7mm8v569pa")))

(define-public crate-pcsc-0.1 (crate (name "pcsc") (vers "0.1.1") (deps (list (crate-dep (name "bitflags") (req "^0.7") (default-features #t) (kind 0)) (crate-dep (name "pcsc-sys") (req "^0.1.1") (default-features #t) (kind 0)))) (hash "13nkfz1bydmwws2vx2j0i8rmnxvc080gipykjxj927rs11bpd7hi")))

(define-public crate-pcsc-0.1 (crate (name "pcsc") (vers "0.1.2") (deps (list (crate-dep (name "bitflags") (req "^0.7") (default-features #t) (kind 0)) (crate-dep (name "pcsc-sys") (req "^0.1.2") (default-features #t) (kind 0)))) (hash "1m0m303ffhxxlfsk32gq76lcfhky6999gly3h5gr4761zpq340wh")))

(define-public crate-pcsc-1 (crate (name "pcsc") (vers "1.0.0") (deps (list (crate-dep (name "bitflags") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "pcsc-sys") (req "^1.0.0") (default-features #t) (kind 0)))) (hash "1lbdgbbal0lghxdxgzqv692haakls3sjap0yla9mm9sb5ymx47gr")))

(define-public crate-pcsc-1 (crate (name "pcsc") (vers "1.0.1") (deps (list (crate-dep (name "bitflags") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "pcsc-sys") (req "^1.0.1") (default-features #t) (kind 0)))) (hash "0mw6lv5sbd8cf387wc5c1y96p3h8azin0q8n9ylnsrqps0p67prl")))

(define-public crate-pcsc-2 (crate (name "pcsc") (vers "2.0.0") (deps (list (crate-dep (name "bitflags") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "pcsc-sys") (req "^1.0.1") (default-features #t) (kind 0)))) (hash "03bbr6c4vs0phyk89904537221111frdrlvvqy6jnsqx9yi7z9j3")))

(define-public crate-pcsc-2 (crate (name "pcsc") (vers "2.1.0") (deps (list (crate-dep (name "bitflags") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "pcsc-sys") (req "^1.0.1") (default-features #t) (kind 0)))) (hash "193h8dr4jwx26wvccz55rdim8zsf2rk814bvd7sh30rz0j6fcx07")))

(define-public crate-pcsc-2 (crate (name "pcsc") (vers "2.1.1") (deps (list (crate-dep (name "bitflags") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "pcsc-sys") (req "^1.0.1") (default-features #t) (kind 0)))) (hash "10xbag0gvgx1lcd13yzm2zi6acraddg302aw6ahxbfmhkix44753")))

(define-public crate-pcsc-2 (crate (name "pcsc") (vers "2.2.0") (deps (list (crate-dep (name "bitflags") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "pcsc-sys") (req "^1.1.0") (default-features #t) (kind 0)))) (hash "0ach9ih4j09alip859332wassxdr8ks0923r3ip1ragafnls06ai")))

(define-public crate-pcsc-2 (crate (name "pcsc") (vers "2.3.0") (deps (list (crate-dep (name "bitflags") (req "^1.2.1") (default-features #t) (kind 0)) (crate-dep (name "pcsc-sys") (req "^1.2.0") (default-features #t) (kind 0)))) (hash "1nfanhkaj832x3l0jj3dmq6alfpk19jni94caamnb5g33rabaln9")))

(define-public crate-pcsc-2 (crate (name "pcsc") (vers "2.3.1") (deps (list (crate-dep (name "bitflags") (req "^1.2.1") (default-features #t) (kind 0)) (crate-dep (name "pcsc-sys") (req "^1.2.0") (default-features #t) (kind 0)))) (hash "0k42j09kij0fbqhvmrzmba4m14md4lb9zcsa0k6fdbq674inw4w7")))

(define-public crate-pcsc-2 (crate (name "pcsc") (vers "2.4.0") (deps (list (crate-dep (name "bitflags") (req "^1.2.1") (default-features #t) (kind 0)) (crate-dep (name "pcsc-sys") (req "^1.2.0") (default-features #t) (kind 0)))) (hash "10vcnmxrpscg0fcrzz0r1klgiqzgh2axvyg1zyqwk8h5hy6rmq48")))

(define-public crate-pcsc-2 (crate (name "pcsc") (vers "2.5.0") (deps (list (crate-dep (name "bitflags") (req "^1.2.1") (default-features #t) (kind 0)) (crate-dep (name "pcsc-sys") (req "^1.2.0") (default-features #t) (kind 0)))) (hash "066v2234m24z6smk3a4vv034n6sc23mh3bahm154fw14x2y2vj6l")))

(define-public crate-pcsc-2 (crate (name "pcsc") (vers "2.6.0") (deps (list (crate-dep (name "bitflags") (req "^1.2.1") (default-features #t) (kind 0)) (crate-dep (name "pcsc-sys") (req "^1.2.0") (default-features #t) (kind 0)))) (hash "0nznls3mn5dkw6xark63rl5g82l7ccjk75sc0884b45r15i3ayqg")))

(define-public crate-pcsc-2 (crate (name "pcsc") (vers "2.7.0") (deps (list (crate-dep (name "bitflags") (req "^1.2.1") (default-features #t) (kind 0)) (crate-dep (name "pcsc-sys") (req "^1.2.0") (default-features #t) (kind 0)))) (hash "1i7xqknwfwi990nw452sr0fy3zgrl1avvdbgs3nawcx4g3gf8aby")))

(define-public crate-pcsc-2 (crate (name "pcsc") (vers "2.8.0") (deps (list (crate-dep (name "bitflags") (req "^1.2.1") (default-features #t) (kind 0)) (crate-dep (name "pcsc-sys") (req "^1.2.0") (default-features #t) (kind 0)))) (hash "1d5smcr9irdppl20r7wg2s5xqwfdxx5sb7q5v2l0is04knzb1jip") (rust-version "1.38")))

(define-public crate-pcsc-2 (crate (name "pcsc") (vers "2.8.1") (deps (list (crate-dep (name "bitflags") (req "^1.2.1") (default-features #t) (kind 0)) (crate-dep (name "pcsc-sys") (req "^1.2.0") (default-features #t) (kind 0)))) (hash "0nnsljri06w5c9lj99h08rprfkn0dxb4f900wx39zcri4gsyw4xv") (rust-version "1.38")))

(define-public crate-pcsc-2 (crate (name "pcsc") (vers "2.8.2") (deps (list (crate-dep (name "bitflags") (req "^1.2.1") (default-features #t) (kind 0)) (crate-dep (name "pcsc-sys") (req "^1.2.0") (default-features #t) (kind 0)))) (hash "027a2s8lp6w025aa758s84qszcwkyg92s1mhvplrqzbbh5zrvva5") (rust-version "1.38")))

(define-public crate-pcsc-sys-0.1 (crate (name "pcsc-sys") (vers "0.1.0-alpha4") (deps (list (crate-dep (name "pkg-config") (req "^0.3.9") (default-features #t) (kind 1)))) (hash "01lwgvnl6xiqpwrr163g03p0q3bpfvfcxwmm7qlq1ail8d43y90j")))

(define-public crate-pcsc-sys-0.1 (crate (name "pcsc-sys") (vers "0.1.0") (deps (list (crate-dep (name "pkg-config") (req "^0.3.9") (default-features #t) (kind 1)))) (hash "0r4phf46fzrlx4293c5qcwwh1rpayngjmj9lcv2rznm6zixmb2ji")))

(define-public crate-pcsc-sys-0.1 (crate (name "pcsc-sys") (vers "0.1.1") (deps (list (crate-dep (name "pkg-config") (req "^0.3.9") (default-features #t) (kind 1)))) (hash "17wkvfi5jfa1hai2hnsgam46i4gl5qhaxskv4m9g31lmrjnigz88")))

(define-public crate-pcsc-sys-0.1 (crate (name "pcsc-sys") (vers "0.1.2") (deps (list (crate-dep (name "pkg-config") (req "^0.3.9") (default-features #t) (kind 1)))) (hash "1ldr7dnrrx1hdkrml5asi71ljkapfjy3g3c7zpgvmnm27hbnw7fv")))

(define-public crate-pcsc-sys-1 (crate (name "pcsc-sys") (vers "1.0.0") (deps (list (crate-dep (name "pkg-config") (req "^0.3.9") (default-features #t) (kind 1)))) (hash "0xcbnlm5fccpzx8smfqrwjka5fc1mlmgmpzhqw2dqyv2pmr39fhr")))

(define-public crate-pcsc-sys-1 (crate (name "pcsc-sys") (vers "1.0.1") (deps (list (crate-dep (name "pkg-config") (req "^0.3.9") (default-features #t) (kind 1)))) (hash "0l0z9iyxq2a9j1bpfaaq0pacz4305snyga5kc69xcrcfrpari8dy")))

(define-public crate-pcsc-sys-1 (crate (name "pcsc-sys") (vers "1.1.0") (deps (list (crate-dep (name "pkg-config") (req "^0.3.9") (default-features #t) (kind 1)))) (hash "1k7vwi0h2nmsgkl04p5mjjxgv0mh4vz8xsx0v490mzxw413gqy19") (links "pcsc")))

(define-public crate-pcsc-sys-1 (crate (name "pcsc-sys") (vers "1.2.0") (deps (list (crate-dep (name "pkg-config") (req "^0.3.9") (default-features #t) (kind 1)))) (hash "1si37v9n07r3csqcnnqn4i82j75b6dssyz0fzdg1n3rcpbnbzdz1") (links "pcsc")))

(define-public crate-pcsc-sys-1 (crate (name "pcsc-sys") (vers "1.2.1") (deps (list (crate-dep (name "pkg-config") (req "^0.3.9") (default-features #t) (kind 1)))) (hash "00vlrfv3kcr49ajbzzr1b4ls7g28f97mj9vdjdzick9c1yl9p7mh") (links "pcsc") (rust-version "1.38")))

(define-public crate-pcsclite-sys-0.1 (crate (name "pcsclite-sys") (vers "0.1.0") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "pkg-config") (req "^0.3") (default-features #t) (kind 1)))) (hash "06xk9brqnaxh4nsvqcyp1gf92ws6skwz14hvwqf7pppyscy1hmvg")))

(define-public crate-pcsclite-sys-0.1 (crate (name "pcsclite-sys") (vers "0.1.1") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "pkg-config") (req "^0.3") (default-features #t) (kind 1)))) (hash "143zkzg27a48f8z32cma8kw6cfzm5w4p6h5r725y9i5y20l2qa9h")))

(define-public crate-pcsclite-sys-0.1 (crate (name "pcsclite-sys") (vers "0.1.2") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "pkg-config") (req "^0.3") (default-features #t) (kind 1)))) (hash "17i4k5rgz3qq29a1f052kckwc1cdq5ir5irjhdvrx5a56z2l6527")))

