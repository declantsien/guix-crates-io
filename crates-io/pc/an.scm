(define-module (crates-io pc an) #:use-module (crates-io))

(define-public crate-pcan-basic-bindings-0.1 (crate (name "pcan-basic-bindings") (vers "0.1.0") (deps (list (crate-dep (name "bindgen") (req "^0.24.0") (default-features #t) (kind 1)))) (hash "127wz65gd3hmi390p1qjxgh4v55jpsd8j64rf7hmz2sgji2ar2vb") (yanked #t)))

