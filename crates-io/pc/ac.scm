(define-module (crates-io pc ac) #:use-module (crates-io))

(define-public crate-pcacsv-0.1 (crate (name "pcacsv") (vers "0.1.0") (deps (list (crate-dep (name "anyhow") (req "^1.0.75") (default-features #t) (kind 0)) (crate-dep (name "csv") (req "^1.2.2") (default-features #t) (kind 0)) (crate-dep (name "gumdrop") (req "^0.8.1") (default-features #t) (kind 0)) (crate-dep (name "ndarray") (req "^0.15.6") (default-features #t) (kind 0)) (crate-dep (name "number_range") (req "^0.3.2") (default-features #t) (kind 0)) (crate-dep (name "pca") (req "^0.1.4") (default-features #t) (kind 0)) (crate-dep (name "trimothy") (req "^0.1.8") (default-features #t) (kind 0)))) (hash "18k16awv6ynzwlgf9p734ggsfb6xrn3j36kjm95508v4gbbd00mp")))

