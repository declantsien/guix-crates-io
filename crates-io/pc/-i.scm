(define-module (crates-io pc #{-i}#) #:use-module (crates-io))

(define-public crate-pc-ints-0.0.1 (crate (name "pc-ints") (vers "0.0.1") (hash "0d3ndimawawddq3dci6flhziw5bvpixzj7bysi3w408aam03whx1") (rust-version "1.61")))

(define-public crate-pc-ints-0.0.2 (crate (name "pc-ints") (vers "0.0.2") (hash "1xngy1xmjs4hwfj5dd2avcjw89cvxh51i6bw0v635r848k955pqn") (rust-version "1.61")))

(define-public crate-pc-ints-0.1 (crate (name "pc-ints") (vers "0.1.0") (hash "0cv0ngsxcla0n23s6rcz86287irlcm0r40b2q1n43msi4m3iqsv1")))

(define-public crate-pc-ints-0.1 (crate (name "pc-ints") (vers "0.1.1") (hash "047bn6bn2jqwnscvz00fzqymf897z7wb8fk2az7ny9b8ixzirw9n") (yanked #t)))

(define-public crate-pc-ints-0.1 (crate (name "pc-ints") (vers "0.1.2") (hash "1svph294qk9gl8asgjdqdm0ks82n55vk799ghfhw5abd0vln0xrj") (yanked #t)))

(define-public crate-pc-ints-0.1 (crate (name "pc-ints") (vers "0.1.3") (hash "0630jl7369nhdy2mirqgnfl70ap20854c269r4wf6hcx3x1mgpms")))

(define-public crate-pc-ints-0.1 (crate (name "pc-ints") (vers "0.1.4") (hash "11f0w9nssmhcwrq19pqn3xx93gc58liqr51ggncf1xk6m71hqas2")))

(define-public crate-pc-ints-0.2 (crate (name "pc-ints") (vers "0.2.0") (hash "0s27gkjbgdqwf5is8zwmsfqn7z6k69qzz5lqvhny67il0wn0iqp7")))

(define-public crate-pc-ints-0.2 (crate (name "pc-ints") (vers "0.2.1") (hash "1d61in12qcw1iszim49k5ckil79dpgfdw2pin2zcvgycdswdsb5z")))

(define-public crate-pc-ints-0.3 (crate (name "pc-ints") (vers "0.3.0") (deps (list (crate-dep (name "memoffset") (req "^0.8.0") (features (quote ("unstable_const"))) (default-features #t) (kind 0)))) (hash "05xdcxhzd1wldgicrq8q7wyi7d79grg1lpaprrqcwm1fasc203l0")))

(define-public crate-pc-ints-0.3 (crate (name "pc-ints") (vers "0.3.1") (deps (list (crate-dep (name "memoffset") (req "^0.8.0") (features (quote ("unstable_const"))) (default-features #t) (kind 0)))) (hash "1l9dk7c115x549gyc5wl0w59gxvfvgnw5w0d48rfq62fqxahffha")))

(define-public crate-pc-ints-0.3 (crate (name "pc-ints") (vers "0.3.2") (deps (list (crate-dep (name "memoffset") (req "^0.8.0") (features (quote ("unstable_const"))) (default-features #t) (kind 0)))) (hash "18j4097n742k4i4ymbld5qarqla6zn6xrlis7wddbbs230lh8918")))

(define-public crate-pc-ints-0.3 (crate (name "pc-ints") (vers "0.3.3") (deps (list (crate-dep (name "memoffset") (req "^0.9.0") (features (quote ("unstable_const"))) (default-features #t) (kind 0)))) (hash "08ywc6hvmx90ziml4akk3777fsya35v57id9d92nh0wchfjwg182")))

(define-public crate-pc-ints-0.3 (crate (name "pc-ints") (vers "0.3.4") (deps (list (crate-dep (name "memoffset") (req "^0.9.0") (features (quote ("unstable_const"))) (default-features #t) (kind 0)))) (hash "1dzgny9jlqnjhypx0yhd6viwkaxb0lmimmfvd0cp1bzwkw4wb826")))

