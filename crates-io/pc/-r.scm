(define-module (crates-io pc -r) #:use-module (crates-io))

(define-public crate-pc-rs-0.1 (crate (name "pc-rs") (vers "0.1.0") (deps (list (crate-dep (name "anyhow") (req "^1.0.75") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^4.4.8") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.10.2") (default-features #t) (kind 0)))) (hash "13rk0zfc8nqdwblmws6qvr4lxqy1nl6arhsfcixfccjfa6rxirk3")))

(define-public crate-pc-rs-0.2 (crate (name "pc-rs") (vers "0.2.0") (deps (list (crate-dep (name "anyhow") (req "^1.0.75") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^4.4.8") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.10.2") (default-features #t) (kind 0)))) (hash "1mhvr6z19108zvllh0hjg2j3h3yml9sq7zz3a5h438rqiwwvjm63")))

(define-public crate-pc-rs-0.2 (crate (name "pc-rs") (vers "0.2.1") (deps (list (crate-dep (name "anyhow") (req "^1.0.75") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^4.4.8") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "clap-stdin") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.10.2") (default-features #t) (kind 0)))) (hash "0rl2jzqccz8dyyf2c4ggcs6ign8bgi0fgz0nf4875xsfl0x03kdw")))

(define-public crate-pc-rs-0.2 (crate (name "pc-rs") (vers "0.2.2") (deps (list (crate-dep (name "anyhow") (req "^1.0.75") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^4.4.8") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "clap-stdin") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.10.2") (default-features #t) (kind 0)))) (hash "07yv0kqnsi5hisc6ygdi7a4k1dv91brzl5spabmg84i5pqgfsx16")))

