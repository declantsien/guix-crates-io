(define-module (crates-io pc f8) #:use-module (crates-io))

(define-public crate-pcf85063a-0.1 (crate (name "pcf85063a") (vers "0.1.0") (deps (list (crate-dep (name "defmt") (req "^0.3") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "embedded-hal-async") (req "^1.0.0-rc.1") (default-features #t) (kind 0)) (crate-dep (name "time") (req "^0.3.17") (kind 0)))) (hash "189jkk5929h1hipf4zk8w76sj32q7nbm04x5wh9zf4pvxwwxdxi2") (v 2) (features2 (quote (("defmt" "dep:defmt"))))))

(define-public crate-pcf8523-0.1 (crate (name "pcf8523") (vers "0.1.0") (deps (list (crate-dep (name "chrono") (req "^0.4.7") (default-features #t) (kind 0)) (crate-dep (name "i2cdev") (req "^0.4.2") (default-features #t) (kind 0)))) (hash "0smb5xj0n8zyajq0akiysgy1rxrmfyq9acbaxf2al3wcivg4i2js")))

(define-public crate-pcf8563-0.1 (crate (name "pcf8563") (vers "0.1.0") (deps (list (crate-dep (name "embedded-hal") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "embedded-hal-mock") (req "^0.2") (default-features #t) (kind 2)))) (hash "1an7k5pr83sy28bxhrp12gcsk774mhncx7w6m3jjnf2pr6l0v5vf")))

(define-public crate-pcf8563-0.1 (crate (name "pcf8563") (vers "0.1.1") (deps (list (crate-dep (name "embedded-hal") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "embedded-hal-mock") (req "^0.2") (default-features #t) (kind 2)))) (hash "04m7zmbqbpm4wihpfpyzapqf8fp34v07lj3v2mwf4ydr03la2mb1") (yanked #t)))

(define-public crate-pcf8563-0.1 (crate (name "pcf8563") (vers "0.1.2") (deps (list (crate-dep (name "embedded-hal") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "embedded-hal-mock") (req "^0.2") (default-features #t) (kind 2)))) (hash "1xip0krcq35nlz4pfrz71van4n6knkgn9palvapsp2mqlwhvlkj1")))

(define-public crate-pcf857x-0.1 (crate (name "pcf857x") (vers "0.1.0") (deps (list (crate-dep (name "embedded-hal") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "embedded-hal-mock") (req "^0.2") (default-features #t) (kind 2)) (crate-dep (name "linux-embedded-hal") (req "^0.2") (default-features #t) (kind 2)))) (hash "0y6037a4algp8lpyx52qb6ivr5wajdwm9c5h5xwf265ryqww24fj") (features (quote (("default"))))))

(define-public crate-pcf857x-0.2 (crate (name "pcf857x") (vers "0.2.0") (deps (list (crate-dep (name "embedded-hal") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "embedded-hal-mock") (req "^0.2") (default-features #t) (kind 2)) (crate-dep (name "linux-embedded-hal") (req "^0.2") (default-features #t) (kind 2)))) (hash "1jfl6r2vc2q5f014kzsa49my3z9fdmjh3i0jm5fqnapzfdb4lp9w") (features (quote (("unproven" "embedded-hal/unproven") ("std") ("default" "unproven"))))))

(define-public crate-pcf857x-0.3 (crate (name "pcf857x") (vers "0.3.0") (deps (list (crate-dep (name "embedded-hal") (req "^0.2.3") (default-features #t) (kind 0)) (crate-dep (name "embedded-hal-mock") (req "^0.4") (default-features #t) (kind 2)) (crate-dep (name "linux-embedded-hal") (req "^0.2") (default-features #t) (kind 2)))) (hash "1wd0m69hwwy4cf2na7l9j9ds11wq9bx6gkkkjgvdwl69b0qklc7y") (features (quote (("unproven" "embedded-hal/unproven") ("default" "unproven"))))))

(define-public crate-pcf857x-0.4 (crate (name "pcf857x") (vers "0.4.0") (deps (list (crate-dep (name "embedded-hal") (req "^0.2.5") (default-features #t) (kind 0)) (crate-dep (name "embedded-hal-mock") (req "^0.7") (default-features #t) (kind 2)) (crate-dep (name "linux-embedded-hal") (req "^0.3") (default-features #t) (kind 2)))) (hash "0hi02w463378j3viqaylkch9698fd2qafk4j8pkl9s2ma4lnd57b") (features (quote (("unproven" "embedded-hal/unproven") ("default" "unproven"))))))

(define-public crate-pcf857x-simple-0.1 (crate (name "pcf857x-simple") (vers "0.1.0") (deps (list (crate-dep (name "embedded-hal") (req "^0.2") (default-features #t) (kind 0)))) (hash "1syd2ydzhkmr4rvd1g8s834yil85901wm7yz4cr64fgwjaamsksx")))

(define-public crate-pcf857x-simple-0.1 (crate (name "pcf857x-simple") (vers "0.1.1") (deps (list (crate-dep (name "embedded-hal") (req "^0.2") (default-features #t) (kind 0)))) (hash "1zn3hmrwfc0j3sxkjll9ihn8pgix9g3jvbrncd1awm4xlwwk1ss3")))

(define-public crate-pcf857x-simple-0.2 (crate (name "pcf857x-simple") (vers "0.2.0") (deps (list (crate-dep (name "embedded-hal") (req "^0.2") (default-features #t) (kind 0)))) (hash "0s9dk4ym8m6pn2wdn4hfn1j7p8fgszjxqjfq5k79bcybqqr09ypg")))

(define-public crate-pcf8591-0.1 (crate (name "pcf8591") (vers "0.1.0") (deps (list (crate-dep (name "i2cdev") (req "^0.3.1") (default-features #t) (kind 0)))) (hash "1b12n978h3y3pvv057c6wpp7pvmq63npvd95qd938ypzsf0g23l3")))

(define-public crate-pcf8591-0.1 (crate (name "pcf8591") (vers "0.1.1") (deps (list (crate-dep (name "i2cdev") (req "^0.3.1") (default-features #t) (kind 0)))) (hash "0910a3hhas642n4njn8f3375cyqzgxndm0hs3rg1yv9cqw5k3d98")))

(define-public crate-pcf8591-hal-0.1 (crate (name "pcf8591-hal") (vers "0.1.0") (deps (list (crate-dep (name "embedded-hal") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "linux-embedded-hal") (req "^0.3") (default-features #t) (kind 2)))) (hash "19l6hk06xws5v7j6c5qn4rsidwyxxs5pb0awa0xaw284s0fcfkf0")))

