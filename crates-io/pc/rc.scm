(define-module (crates-io pc rc) #:use-module (crates-io))

(define-public crate-pcrc-0.1 (crate (name "pcrc") (vers "0.1.0") (deps (list (crate-dep (name "reqwest") (req "^0.10") (features (quote ("json"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.3.13") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^0.2") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "0dvvpx4zkzlqkyjawj6d6ca0pk9ll861wjv0c81g7w6mjp9b5jpi")))

