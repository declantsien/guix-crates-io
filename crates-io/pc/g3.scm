(define-module (crates-io pc g3) #:use-module (crates-io))

(define-public crate-pcg32-0.1 (crate (name "pcg32") (vers "0.1.0") (deps (list (crate-dep (name "rand_core") (req "^0.6.4") (kind 2)) (crate-dep (name "rand_pcg") (req "^0.3.1") (kind 2)))) (hash "007bkshr6v28hjf5cbsq0jgs3plcxi2pbxffgq3l0flmx9bgai1v")))

