(define-module (crates-io pc -a) #:use-module (crates-io))

(define-public crate-pc-atomics-0.0.1 (crate (name "pc-atomics") (vers "0.0.1") (hash "11ysc15d7zfw0s3kfdv3nw97lggals9h92ivphcr6p13iwj75ix3") (rust-version "1.66")))

(define-public crate-pc-atomics-0.0.2 (crate (name "pc-atomics") (vers "0.0.2") (hash "1fxsg8g8yxrb4r807scv49ll0g3jkfnh98b0n27bnc4s47qiqaj6") (rust-version "1.66")))

