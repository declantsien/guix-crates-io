(define-module (crates-io pc k-) #:use-module (crates-io))

(define-public crate-pck-tweaker-0.1 (crate (name "pck-tweaker") (vers "0.1.0") (deps (list (crate-dep (name "binrw") (req "^0.4") (features (quote ("std"))) (default-features #t) (kind 0)) (crate-dep (name "color-eyre") (req "^0.5") (default-features #t) (kind 0)) (crate-dep (name "linked-hash-map") (req "^0.5") (default-features #t) (kind 0)) (crate-dep (name "md-5") (req "^0.9") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.3") (default-features #t) (kind 0)))) (hash "1jfdp2ylrpvgh3l8qgsdmya1809qp308r03m3r2755jzkpv8apdn")))

