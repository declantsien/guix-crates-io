(define-module (crates-io pc la) #:use-module (crates-io))

(define-public crate-pclass-parser-0.1 (crate (name "pclass-parser") (vers "0.1.0") (deps (list (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.0") (default-features #t) (kind 0)) (crate-dep (name "nom") (req "^5.1.1") (default-features #t) (kind 0)))) (hash "10s4xsqjzswby3j5pvvq5gjj1pd8h8i2kiz49hyd1bb9h2i1z3i8")))

