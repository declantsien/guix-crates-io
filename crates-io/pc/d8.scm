(define-module (crates-io pc d8) #:use-module (crates-io))

(define-public crate-pcd8544-0.1 (crate (name "pcd8544") (vers "0.1.0") (deps (list (crate-dep (name "embedded-hal") (req "^0.1.0") (features (quote ("unproven"))) (default-features #t) (kind 0)))) (hash "04zvr84wjqcqvqhjm3jmsg2pmizpr6y94ys7890mi82a8iiz8j65")))

(define-public crate-pcd8544-0.1 (crate (name "pcd8544") (vers "0.1.1") (deps (list (crate-dep (name "embedded-hal") (req "^0.1.0") (features (quote ("unproven"))) (default-features #t) (kind 0)))) (hash "0i8h4qws2gs78nqrcyvfw2djy83g5fiwfdy3zcx2snnla3bbjgc3")))

(define-public crate-pcd8544-0.1 (crate (name "pcd8544") (vers "0.1.2") (deps (list (crate-dep (name "embedded-hal") (req "^0.1.0") (features (quote ("unproven"))) (default-features #t) (kind 0)))) (hash "0iqrj7cz4xcgpq3i1fl4awkb6xq4bh9999571glcl3vz56afbvqf")))

(define-public crate-pcd8544-0.1 (crate (name "pcd8544") (vers "0.1.3") (deps (list (crate-dep (name "embedded-hal") (req "^0.1.0") (features (quote ("unproven"))) (default-features #t) (kind 0)))) (hash "01m1fbjailzpyswx95gla6v77pp2xpdkqqbzbk5j4rwx3q88ag5j")))

(define-public crate-pcd8544-0.1 (crate (name "pcd8544") (vers "0.1.4") (deps (list (crate-dep (name "embedded-hal") (req "^0.2.1") (features (quote ("unproven"))) (default-features #t) (kind 0)))) (hash "16x59m140qg4q91yhklwb2rb28sgdf58alhvrnbh2alj5sl5wcdm")))

(define-public crate-pcd8544-0.1 (crate (name "pcd8544") (vers "0.1.5") (deps (list (crate-dep (name "embedded-hal") (req "^0.2.1") (features (quote ("unproven"))) (default-features #t) (kind 0)))) (hash "012sfl6xd9xld0gig5yhchgfxaipn2ksn9pf2xgm82z2qhdqmqsf")))

(define-public crate-pcd8544-0.1 (crate (name "pcd8544") (vers "0.1.6") (deps (list (crate-dep (name "embedded-hal") (req "^0.2.1") (features (quote ("unproven"))) (default-features #t) (kind 0)))) (hash "18wx74l9q5mjy33mfxabcmi2g7lhyjl957c8ab1qsh19shskrz6q")))

(define-public crate-pcd8544-0.1 (crate (name "pcd8544") (vers "0.1.7") (deps (list (crate-dep (name "embedded-hal") (req "^0.2.1") (features (quote ("unproven"))) (default-features #t) (kind 0)))) (hash "0ddnbgmk29i3nkanvpfqkll0jxw48xi8z1q3y6s3lkyfrsvs2qnq")))

(define-public crate-pcd8544-0.1 (crate (name "pcd8544") (vers "0.1.8") (deps (list (crate-dep (name "embedded-hal") (req "^0.2.1") (features (quote ("unproven"))) (default-features #t) (kind 0)))) (hash "1sazi7c1acfrb88hv0aw2fds7mf457nm2jvxxzdpf121pj1yn0lv")))

(define-public crate-pcd8544-0.2 (crate (name "pcd8544") (vers "0.2.0") (deps (list (crate-dep (name "embedded-hal") (req "^0.2.3") (features (quote ("unproven"))) (default-features #t) (kind 0)))) (hash "1q5y2akgj5fghpr80aphq8lv7rz9vvyim6wvh9x2qgjf87y9f52p")))

