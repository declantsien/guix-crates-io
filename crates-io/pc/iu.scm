(define-module (crates-io pc iu) #:use-module (crates-io))

(define-public crate-pciutils-0.1 (crate (name "pciutils") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^4.2.1") (default-features #t) (kind 0)) (crate-dep (name "env_logger") (req "^0.10.0") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.17") (default-features #t) (kind 0)) (crate-dep (name "pci-ids") (req "^0.2.5") (default-features #t) (kind 0)) (crate-dep (name "pretty-hex") (req "^0.3.0") (default-features #t) (kind 0)))) (hash "1dvfwxq20kd2kwdr3nw3xai82c9vvfnfkrlb9xalksnkyf8j4x7k")))

(define-public crate-pciutils-0.1 (crate (name "pciutils") (vers "0.1.1") (deps (list (crate-dep (name "clap") (req "^4.2.1") (default-features #t) (kind 0)) (crate-dep (name "env_logger") (req "^0.10.0") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.17") (default-features #t) (kind 0)) (crate-dep (name "pci-ids") (req "^0.2.5") (default-features #t) (kind 0)) (crate-dep (name "pretty-hex") (req "^0.3.0") (default-features #t) (kind 0)))) (hash "0z30h95x64lhqansakzjvs9mzahigypdglh315ijy6w9k2rl7vd7")))

(define-public crate-pciutils-sys-0.1 (crate (name "pciutils-sys") (vers "0.1.0") (deps (list (crate-dep (name "bindgen") (req "^0.66.1") (default-features #t) (kind 1)))) (hash "1zxgwii0gl1vg483yh7a4lqmy61hvvn7a6dy2m77fx1nrsa00v2s")))

(define-public crate-pciutils-sys-0.1 (crate (name "pciutils-sys") (vers "0.1.1") (deps (list (crate-dep (name "bindgen") (req "^0.66.1") (default-features #t) (kind 1)))) (hash "01n1n92mh673fil4n6hdv51zxw365n5l4487pn1rckj0l3dfnn4v")))

(define-public crate-pciutils-sys-0.1 (crate (name "pciutils-sys") (vers "0.1.2") (deps (list (crate-dep (name "bindgen") (req "^0.66.1") (default-features #t) (kind 1)))) (hash "0igvf9pzad7w4y09drvxpsj44g9hrz75ja94mnnxf68jl3iz73c1")))

(define-public crate-pciutils-sys-0.1 (crate (name "pciutils-sys") (vers "0.1.3") (deps (list (crate-dep (name "bindgen") (req "^0.66.1") (default-features #t) (kind 1)))) (hash "03hdvh08pxdyf0m91lzgiificsbx6ry7nda6999gngd6i7670d1k")))

