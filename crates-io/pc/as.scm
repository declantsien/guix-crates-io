(define-module (crates-io pc as) #:use-module (crates-io))

(define-public crate-pcast-0.3 (crate (name "pcast") (vers "0.3.0") (deps (list (crate-dep (name "try_from") (req "^0.2.0") (default-features #t) (kind 0)))) (hash "0r7i4n02y5716hx9hkj1j6vw52msdka6fd0mislxw7yy7c7cfv52")))

