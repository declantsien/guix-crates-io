(define-module (crates-io pc b-) #:use-module (crates-io))

(define-public crate-pcb-c-0.1 (crate (name "pcb-c") (vers "0.1.3") (deps (list (crate-dep (name "pcb") (req "^0.1.2") (default-features #t) (kind 0)))) (hash "09y3ly4fh5pc8mkyc72vq51dj0l2xqsks6nm1wspqg8zwi98zxrj")))

(define-public crate-pcb-c-0.2 (crate (name "pcb-c") (vers "0.2.0") (deps (list (crate-dep (name "libc") (req "^0.2.7") (default-features #t) (kind 0)) (crate-dep (name "pcb") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "pcb-llvm") (req "^0.2.0") (default-features #t) (kind 0)))) (hash "0dvjway1aspwkqjsny8h50xkw94s8wyv1wadzbqjfd1mg3kjx75w")))

(define-public crate-pcb-core-0.1 (crate (name "pcb-core") (vers "0.1.2") (deps (list (crate-dep (name "libc") (req "^0.2.7") (default-features #t) (kind 0)) (crate-dep (name "llvm-sys") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "typed-arena") (req "^1.1.0") (default-features #t) (kind 0)))) (hash "1wbsw8iikkxxpq9p5ghlkm4w7iz09k7qpykdqi9589fz6knkksz0")))

(define-public crate-pcb-core-0.1 (crate (name "pcb-core") (vers "0.1.3") (deps (list (crate-dep (name "libc") (req "^0.2.7") (default-features #t) (kind 0)) (crate-dep (name "llvm-sys") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "typed-arena") (req "^1.1.0") (default-features #t) (kind 0)))) (hash "0ysyp0l85w9dn7b7gif121i5jy8q8vb0pbfz54abwagc7l90nixm")))

(define-public crate-pcb-core-0.2 (crate (name "pcb-core") (vers "0.2.0") (deps (list (crate-dep (name "typed-arena") (req "^1.1.0") (default-features #t) (kind 0)))) (hash "02k0gls1aw315wnyhpkhsg7kfzv9kbspziq952nx21dxb4r15d15")))

(define-public crate-pcb-llvm-0.2 (crate (name "pcb-llvm") (vers "0.2.0") (deps (list (crate-dep (name "libc") (req "^0.2.7") (default-features #t) (kind 0)) (crate-dep (name "llvm-sys") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "pcb-core") (req "^0.2.0") (default-features #t) (kind 0)))) (hash "006xslw10dqjcplgd22zpj1y48m8pfkmxwzhx6bp64q4c731vydh")))

(define-public crate-pcb-rs-0.1 (crate (name "pcb-rs") (vers "0.1.0") (deps (list (crate-dep (name "pcb-rs-macros") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "pcb-rs-traits") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "12vpvnzv124qh4iwhrq3ldc8zigp41s9bsj4ffhyh158ayrqm5wc")))

(define-public crate-pcb-rs-macros-0.1 (crate (name "pcb-rs-macros") (vers "0.1.0") (deps (list (crate-dep (name "pcb-rs-traits") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1.0.33") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.10") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.82") (features (quote ("extra-traits"))) (default-features #t) (kind 0)))) (hash "1ca6np5nx6gi281hgz0ybix03birxcqmaa5x620zq51asf3sv80d")))

(define-public crate-pcb-rs-traits-0.1 (crate (name "pcb-rs-traits") (vers "0.1.0") (deps (list (crate-dep (name "downcast-rs") (req "^1.2.0") (default-features #t) (kind 0)))) (hash "0l5vww29czc5614xlgaddbx0jb6vmf0vfzrvh0c109zs58llwb0s")))

