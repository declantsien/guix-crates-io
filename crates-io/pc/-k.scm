(define-module (crates-io pc -k) #:use-module (crates-io))

(define-public crate-pc-keyboard-0.1 (crate (name "pc-keyboard") (vers "0.1.1") (hash "1q2jikf3rf9z75rv7jxif4a0w23y9n75chdwv2g4ni7933ibfcd6")))

(define-public crate-pc-keyboard-0.2 (crate (name "pc-keyboard") (vers "0.2.0") (hash "00c36ly1n3a2d7x6acdvnbhcprxbamc8bjbibdd8ygk6bm7y1awb")))

(define-public crate-pc-keyboard-0.3 (crate (name "pc-keyboard") (vers "0.3.0") (hash "0r87dv10ngxkdvzmczijqdld9lwsciggdi64v5c978srnj0izchn")))

(define-public crate-pc-keyboard-0.3 (crate (name "pc-keyboard") (vers "0.3.1") (hash "1b08712fir38grcf1rm9vjnf3yjj156fdx390sycw6x3kfq0mxgz")))

(define-public crate-pc-keyboard-0.4 (crate (name "pc-keyboard") (vers "0.4.0") (hash "138x469snk1a8x4y8yzb3jfdvgr7ig14zz5j7xdjw499w6bmwm4m")))

(define-public crate-pc-keyboard-0.4 (crate (name "pc-keyboard") (vers "0.4.1") (hash "05d1rp3is969pkkp8dbmxhvcln6dr2xbpz27wclmi1cl92ig1cky")))

(define-public crate-pc-keyboard-0.5 (crate (name "pc-keyboard") (vers "0.5.0") (hash "1kigdvwdnma7mz84ypiw84a7pfvyz72mdqrv1fgadsf4fvdr50y4")))

(define-public crate-pc-keyboard-0.5 (crate (name "pc-keyboard") (vers "0.5.1") (hash "1b1mcv7hg5131lz2x70js6fvqha0mqmiwh01kd26739vgs9jsvsw")))

(define-public crate-pc-keyboard-0.6 (crate (name "pc-keyboard") (vers "0.6.0") (hash "1j1hd70n1jwxbfn0z3ym35xra9xn0fbrxwdvrc86m63hkbad7lij")))

(define-public crate-pc-keyboard-0.6 (crate (name "pc-keyboard") (vers "0.6.1") (hash "0f2k02bvazfacf0f00lfv42qn3c7ab4wm0m6xyip41jx2q2ribg0")))

(define-public crate-pc-keyboard-0.7 (crate (name "pc-keyboard") (vers "0.7.0") (hash "0x1i7jai4gj3abl40nnyd5zf9c8y3yc1ql25lfhkfczypwgrl27d")))

