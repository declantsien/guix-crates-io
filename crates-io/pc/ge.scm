(define-module (crates-io pc ge) #:use-module (crates-io))

(define-public crate-pcgen-0.1 (crate (name "pcgen") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^4.4.5") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)))) (hash "0w1la564iva6gwsy26xd7k3zmpn0mzj8fm86lqjf997m0h7qah39")))

(define-public crate-pcgen-0.1 (crate (name "pcgen") (vers "0.1.1") (deps (list (crate-dep (name "clap") (req "^4.4.5") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)))) (hash "1c2il669anhn6h7afx93sbx3byl733rziqhra8n75wzm1n1h2snn")))

