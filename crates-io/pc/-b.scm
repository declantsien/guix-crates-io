(define-module (crates-io pc -b) #:use-module (crates-io))

(define-public crate-pc-beeper-0.1 (crate (name "pc-beeper") (vers "0.1.0") (deps (list (crate-dep (name "x86_64") (req "^0.11.0") (default-features #t) (kind 0)))) (hash "11y7w76729vjnhr2315dz7644nagyazn1dbkpyxm1s6pxfyqwmhi")))

(define-public crate-pc-beeper-0.1 (crate (name "pc-beeper") (vers "0.1.1") (deps (list (crate-dep (name "x86_64") (req "^0.14.10") (default-features #t) (kind 0)))) (hash "06yd889xlp6d8whvips4lawnxi4x8gzy6byyqcv5qy9p5z4marhn")))

