(define-module (crates-io pc g-) #:use-module (crates-io))

(define-public crate-pcg-mwc-0.1 (crate (name "pcg-mwc") (vers "0.1.0") (deps (list (crate-dep (name "rand_core") (req "^0.6.2") (default-features #t) (kind 0)))) (hash "0gczv74qpxx2ivlsbxvyvqm17ppch17m0sc5ynqc1gnk8bd3dsic")))

(define-public crate-pcg-mwc-0.2 (crate (name "pcg-mwc") (vers "0.2.0") (deps (list (crate-dep (name "rand_core") (req "^0.6.2") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)))) (hash "1jd08wb9zxqyvn6br2vm22rd0gxsn0anha09m408jlcxk66pfbcz") (features (quote (("serde1" "serde"))))))

(define-public crate-pcg-mwc-0.2 (crate (name "pcg-mwc") (vers "0.2.1") (deps (list (crate-dep (name "rand_core") (req "^0.6.2") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)))) (hash "0z6cpz8x387iqcx8kjnqfihgggi0yngqx73zwjz132y56f38a5i2") (features (quote (("serde1" "serde"))))))

