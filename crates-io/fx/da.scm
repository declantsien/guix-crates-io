(define-module (crates-io fx da) #:use-module (crates-io))

(define-public crate-fxdatapi-0.1 (crate (name "fxdatapi") (vers "0.1.5") (deps (list (crate-dep (name "dotenv") (req "^0.15.0") (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.11") (features (quote ("json"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "1g93yg1nafg8si8dvsdlfv7avnmmq9d3l1asaybcmfhpg08rbrcl")))

(define-public crate-fxdatapi-0.1 (crate (name "fxdatapi") (vers "0.1.6") (deps (list (crate-dep (name "dotenv") (req "^0.15.0") (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.11") (features (quote ("json"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "0gk6zchlcffmx3z18mgh97sqrm1l6camdpag14iq0qrh4ssrki9i")))

(define-public crate-fxdatapi-0.1 (crate (name "fxdatapi") (vers "0.1.7") (deps (list (crate-dep (name "dotenv") (req "^0.15.0") (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.11") (features (quote ("json"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "1cn8kk4avk7606b3lpml4gq2ng1yafs8kiydip15a66i1v5cvaz7")))

