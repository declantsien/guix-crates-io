(define-module (crates-io fx ty) #:use-module (crates-io))

(define-public crate-fxtypemap-0.1 (crate (name "fxtypemap") (vers "0.1.0") (deps (list (crate-dep (name "fxhash") (req "^0.2.1") (default-features #t) (kind 0)))) (hash "04nnjv89pw3paxsiv8a506hhwcry62fhyvb2w737lqs3ifrybrvj")))

(define-public crate-fxtypemap-0.1 (crate (name "fxtypemap") (vers "0.1.1") (deps (list (crate-dep (name "fxhash") (req "^0.2.1") (default-features #t) (kind 0)))) (hash "0hg0z2y6a5dfb4mzf4ak5bq12fimwda6qv683rsfqajf7jpilnnf")))

(define-public crate-fxtypemap-0.2 (crate (name "fxtypemap") (vers "0.2.0") (deps (list (crate-dep (name "fxhash") (req "^0.2.1") (default-features #t) (kind 0)))) (hash "1nv9qp2slpm6lwac4zsprhr6z20d0b7lk5qpi4rvyp6x6v4qf761")))

