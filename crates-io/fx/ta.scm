(define-module (crates-io fx ta) #:use-module (crates-io))

(define-public crate-fxtabs-0.1 (crate (name "fxtabs") (vers "0.1.0") (deps (list (crate-dep (name "anyhow") (req "^1.0.0") (default-features #t) (kind 0)) (crate-dep (name "lz4") (req "^1.23.2") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.0") (default-features #t) (kind 0)))) (hash "1s1j5zzmdpzihpfqkdrr32xccp39wq6626zrscamdcg4xpm32x2l")))

(define-public crate-fxtabs-0.2 (crate (name "fxtabs") (vers "0.2.0") (deps (list (crate-dep (name "anyhow") (req "^1.0.0") (default-features #t) (kind 0)) (crate-dep (name "lz4") (req "^1.23.2") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.0") (default-features #t) (kind 0)))) (hash "051xjai6lghb28cca1dkraa3yhlk1a3kyrfv1hkbr8ibj9vzy1x6")))

