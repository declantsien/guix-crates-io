(define-module (crates-io fx re) #:use-module (crates-io))

(define-public crate-fxread-0.1 (crate (name "fxread") (vers "0.1.0") (deps (list (crate-dep (name "anyhow") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "flate2") (req "^1") (default-features #t) (kind 0)))) (hash "07i9fzmhhkilg8anlb4pahhyvxhp01h46n156f3kldd0bcn9778s")))

(define-public crate-fxread-0.1 (crate (name "fxread") (vers "0.1.1") (deps (list (crate-dep (name "anyhow") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "flate2") (req "^1") (default-features #t) (kind 0)))) (hash "0axvqahp4xw70w05492y2rshpsrg2lbr6h2mlx56c6w86phx2iw8")))

(define-public crate-fxread-0.1 (crate (name "fxread") (vers "0.1.2") (deps (list (crate-dep (name "anyhow") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "flate2") (req "^1") (default-features #t) (kind 0)))) (hash "01q9hjr0r5rasq3bp9byzhwhkdx3h58bxb2v2jmlljsc3a9xjwhg")))

(define-public crate-fxread-0.1 (crate (name "fxread") (vers "0.1.3") (deps (list (crate-dep (name "anyhow") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "flate2") (req "^1") (default-features #t) (kind 0)))) (hash "170i9b7airjy2n44x9x3qqvg57f2xyh15zacg6x6ci7gikcyvgdd")))

(define-public crate-fxread-0.1 (crate (name "fxread") (vers "0.1.4") (deps (list (crate-dep (name "anyhow") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "flate2") (req "^1") (default-features #t) (kind 0)))) (hash "18xg1jgnyvfn2wl9lpg7a157p8qmzmy0yhdj7d8w6kp67av1i8j8")))

(define-public crate-fxread-0.1 (crate (name "fxread") (vers "0.1.5") (deps (list (crate-dep (name "anyhow") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "flate2") (req "^1") (default-features #t) (kind 0)))) (hash "0d28s516k8f75r0a1vb9q0sy85dw74yv57p3nw4w3qsbqbjf1zf7")))

(define-public crate-fxread-0.1 (crate (name "fxread") (vers "0.1.6") (deps (list (crate-dep (name "anyhow") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "flate2") (req "^1") (default-features #t) (kind 0)))) (hash "0bp1x65ypxcbdvfpz9bnfazc10mdwpdlzdy636yvgpq8g213vf95")))

(define-public crate-fxread-0.1 (crate (name "fxread") (vers "0.1.7") (deps (list (crate-dep (name "anyhow") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "flate2") (req "^1") (default-features #t) (kind 0)))) (hash "1hvc1xdi9z6ngpv2dmal5byfwp76f7x74vryvpczgp840jnz7ksy")))

(define-public crate-fxread-0.1 (crate (name "fxread") (vers "0.1.8") (deps (list (crate-dep (name "anyhow") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "flate2") (req "^1") (default-features #t) (kind 0)))) (hash "11jz9z97s8g04psjadljxhb464fgcsk3nd49i872d6syqwjmdpjd")))

(define-public crate-fxread-0.2 (crate (name "fxread") (vers "0.2.2") (deps (list (crate-dep (name "anyhow") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "flate2") (req "^1") (default-features #t) (kind 0)))) (hash "05x7y8dm34474iq4kjjzdjps35yj4ncjc6fzvyp9d4ww6za9lsfg")))

(define-public crate-fxread-0.2 (crate (name "fxread") (vers "0.2.4") (deps (list (crate-dep (name "anyhow") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "flate2") (req "^1") (default-features #t) (kind 0)))) (hash "0kwl6dr7jrnzs1565k6gx937b267cvzlvjgclml38x4vvblkid43")))

(define-public crate-fxread-0.2 (crate (name "fxread") (vers "0.2.5") (deps (list (crate-dep (name "anyhow") (req "^1.0.65") (default-features #t) (kind 0)) (crate-dep (name "flate2") (req "^1.0.24") (default-features #t) (kind 0)))) (hash "1lj4hal8n7k93g4m3ygny4wd7kg4pf7nfzb4dkivbakmi15aqc18")))

(define-public crate-fxread-0.2 (crate (name "fxread") (vers "0.2.6") (deps (list (crate-dep (name "anyhow") (req "^1.0.65") (default-features #t) (kind 0)) (crate-dep (name "flate2") (req "^1.0.24") (default-features #t) (kind 0)))) (hash "1ni3234ls96a034lx5hinpqmy145cfdfk73cns5wflqp4k2qgy6w")))

(define-public crate-fxread-0.2 (crate (name "fxread") (vers "0.2.7") (deps (list (crate-dep (name "anyhow") (req "^1.0.65") (default-features #t) (kind 0)) (crate-dep (name "flate2") (req "^1.0.24") (default-features #t) (kind 0)))) (hash "0pd3xj75svzfvi2nihwkag4l7pzmnnl1f5if1qrzw17x6hnsiixc")))

(define-public crate-fxread-0.2 (crate (name "fxread") (vers "0.2.8") (deps (list (crate-dep (name "anyhow") (req "^1.0.65") (default-features #t) (kind 0)) (crate-dep (name "flate2") (req "^1.0.24") (default-features #t) (kind 0)))) (hash "01r6g22dk4bqsi48jgnqkms1xp1zqypxv2sihgrabzjbx5hlv919")))

(define-public crate-fxread-0.2 (crate (name "fxread") (vers "0.2.9") (deps (list (crate-dep (name "anyhow") (req "^1.0.65") (default-features #t) (kind 0)) (crate-dep (name "flate2") (req "^1.0.24") (default-features #t) (kind 0)))) (hash "0d7apx4w6j05qr5q5jwjw0g451iicghx0ih4ihpkqkk772fkzn5c")))

(define-public crate-fxread-0.2 (crate (name "fxread") (vers "0.2.10") (deps (list (crate-dep (name "anyhow") (req "^1.0.65") (default-features #t) (kind 0)) (crate-dep (name "flate2") (req "^1.0.24") (default-features #t) (kind 0)))) (hash "00scrj8079nzrllr3l9k1i4xqpbri1k9kcm1snsf6ppivvr9336y")))

(define-public crate-fxread-0.2 (crate (name "fxread") (vers "0.2.11") (deps (list (crate-dep (name "anyhow") (req "^1.0.65") (default-features #t) (kind 0)) (crate-dep (name "flate2") (req "^1.0.24") (default-features #t) (kind 0)))) (hash "15hck0ci0x4ivcbpglkyvqbyw5dr9mznaapllnlcrwsp36si4593")))

(define-public crate-fxread-0.2 (crate (name "fxread") (vers "0.2.12") (deps (list (crate-dep (name "anyhow") (req "^1.0.65") (default-features #t) (kind 0)) (crate-dep (name "flate2") (req "^1.0.24") (default-features #t) (kind 0)))) (hash "1b0isf89w5rsslrqi4mj3dmmh8vmyc2i40n9zj6jqsmwb7sdxi26")))

