(define-module (crates-io fx ap) #:use-module (crates-io))

(define-public crate-fxapi-rs-0.1 (crate (name "fxapi-rs") (vers "0.1.0") (deps (list (crate-dep (name "reqwest") (req "^0.11.7") (features (quote ("rustls-tls"))) (kind 0)) (crate-dep (name "serde") (req "^1.0.131") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.72") (default-features #t) (kind 0)) (crate-dep (name "strum") (req "^0.24") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.30") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.19.2") (features (quote ("rt" "macros"))) (default-features #t) (kind 2)))) (hash "0hjcp4kvh2ls13zwxq8ghdscsqrb9sqz0vmj4wgzpgy19afhirh4")))

