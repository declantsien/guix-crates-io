(define-module (crates-io fx ha) #:use-module (crates-io))

(define-public crate-fxhash-0.1 (crate (name "fxhash") (vers "0.1.0") (deps (list (crate-dep (name "byteorder") (req "^1.0.0") (default-features #t) (kind 0)) (crate-dep (name "fnv") (req "^1.0.5") (default-features #t) (kind 2)) (crate-dep (name "seahash") (req "^3.0.5") (default-features #t) (kind 2)))) (hash "04xxzwbgkyrj1ac2h78bdqcrdq5vjkh90nr4a38j3zkjzsyv67n2")))

(define-public crate-fxhash-0.1 (crate (name "fxhash") (vers "0.1.1") (deps (list (crate-dep (name "byteorder") (req "^1.0.0") (default-features #t) (kind 0)) (crate-dep (name "fnv") (req "^1.0.5") (default-features #t) (kind 2)) (crate-dep (name "seahash") (req "^3.0.5") (default-features #t) (kind 2)))) (hash "1xq7d7jgdr8qwkasgzvc7qhgx36v7fqvrpg6ykbjcam7wcjn2qxs")))

(define-public crate-fxhash-0.1 (crate (name "fxhash") (vers "0.1.2") (deps (list (crate-dep (name "byteorder") (req "^1.0.0") (default-features #t) (kind 0)) (crate-dep (name "fnv") (req "^1.0.5") (default-features #t) (kind 2)) (crate-dep (name "seahash") (req "^3.0.5") (default-features #t) (kind 2)))) (hash "0xga2acbjcfhnm53f47bdbn56c9i6yxlc0ljxr7b0p8b5l48bapv")))

(define-public crate-fxhash-0.2 (crate (name "fxhash") (vers "0.2.0") (deps (list (crate-dep (name "byteorder") (req "^1.0.0") (default-features #t) (kind 0)) (crate-dep (name "fnv") (req "^1.0.5") (default-features #t) (kind 2)) (crate-dep (name "seahash") (req "^3.0.5") (default-features #t) (kind 2)))) (hash "1yvwfagz2ssq9j5qj9mrls88nkzhs40abw5fa5zwy34rid87af43") (yanked #t)))

(define-public crate-fxhash-0.2 (crate (name "fxhash") (vers "0.2.1") (deps (list (crate-dep (name "byteorder") (req "^1.0.0") (default-features #t) (kind 0)) (crate-dep (name "fnv") (req "^1.0.5") (default-features #t) (kind 2)) (crate-dep (name "seahash") (req "^3.0.5") (default-features #t) (kind 2)))) (hash "037mb9ichariqi45xm6mz0b11pa92gj38ba0409z3iz239sns6y3")))

