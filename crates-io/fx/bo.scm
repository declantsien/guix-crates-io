(define-module (crates-io fx bo) #:use-module (crates-io))

(define-public crate-fxbot-0.1 (crate (name "fxbot") (vers "0.1.0") (deps (list (crate-dep (name "regex") (req "^1.10") (default-features #t) (kind 0)) (crate-dep (name "serenity") (req "^0.11") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "0c8gmns08ii27rg0pyp6j0hvvvv8snfqrnfn2g5kww677gcmffqg")))

(define-public crate-fxbot-0.1 (crate (name "fxbot") (vers "0.1.1") (deps (list (crate-dep (name "regex") (req "^1.10") (default-features #t) (kind 0)) (crate-dep (name "serenity") (req "^0.11") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "11669k00bvpkcxpf3wzzajbf4v46wa3l6mdq893hm9j9l9ik238x")))

