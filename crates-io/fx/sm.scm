(define-module (crates-io fx sm) #:use-module (crates-io))

(define-public crate-fxsm-0.1 (crate (name "fxsm") (vers "0.1.0") (hash "0q5ybvdmz9wxmyd7v0gk254nfbngwywa75ybm4wrr0glhgnl868y")))

(define-public crate-fxsm-0.2 (crate (name "fxsm") (vers "0.2.0") (hash "0gfwxw0iq1c8qxrm1civh963a70lrwcj8jrmb1jwxjpfd4am2i5m")))

(define-public crate-fxsm-derive-0.1 (crate (name "fxsm-derive") (vers "0.1.0") (deps (list (crate-dep (name "quote") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^0.11") (default-features #t) (kind 0)))) (hash "0ccr8g1c87j07ppwhsxchjvrq91kcpnxapi76nqaimqm8ynggggm")))

(define-public crate-fxsm-derive-0.2 (crate (name "fxsm-derive") (vers "0.2.0") (deps (list (crate-dep (name "quote") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^0.11") (default-features #t) (kind 0)))) (hash "14gdmxzfdq2qb5fbchwdnfc1wjkgg6ifcfl3qwllf4ya4qb6ikan")))

(define-public crate-fxsm-derive-0.3 (crate (name "fxsm-derive") (vers "0.3.0") (deps (list (crate-dep (name "quote") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^0.11") (default-features #t) (kind 0)))) (hash "01wgpl0h1p3dllny7cvwzgq1zms33yp0dyp55z3lzx5sz4y2kqxq")))

