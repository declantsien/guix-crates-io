(define-module (crates-io cx c_) #:use-module (crates-io))

(define-public crate-cxc_derive-0.1 (crate (name "cxc_derive") (vers "0.1.0") (deps (list (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "14bvcb8njf39ivpwhxnqgycn64wanq2z9rbb3iai1bk5k7f38a3j")))

(define-public crate-cxc_derive-0.2 (crate (name "cxc_derive") (vers "0.2.0") (deps (list (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "1yxy9xkhf0jyv60zfif027iaf8avh96glaiyb2gb5673bdz93z1a")))

