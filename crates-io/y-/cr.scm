(define-module (crates-io y- cr) #:use-module (crates-io))

(define-public crate-y-craft-0.2 (crate (name "y-craft") (vers "0.2.0") (deps (list (crate-dep (name "image") (req "^0.24") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "sdl2") (req "^0.36") (features (quote ("ttf" "image" "mixer"))) (default-features #t) (kind 0)))) (hash "02kf3nxxhx5zascc2j0ybpbhc37y1f1apsaan26wzv9zwlba7v4b")))

(define-public crate-y-craft-0.2 (crate (name "y-craft") (vers "0.2.1") (deps (list (crate-dep (name "image") (req "^0.24") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "sdl2") (req "^0.36") (features (quote ("ttf" "image" "mixer"))) (default-features #t) (kind 0)))) (hash "15zdd7mzyzga259a3dd1rj5v63chwww953p36jz0c8yz1zlgdkas")))

(define-public crate-y-craft-0.2 (crate (name "y-craft") (vers "0.2.2") (deps (list (crate-dep (name "image") (req "^0.24") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "sdl2") (req "^0.36") (features (quote ("ttf" "image" "mixer"))) (default-features #t) (kind 0)))) (hash "0i8jkk2bjdm8sddqi259hcwfq444m1g9v8jd0554d80rqx3vdh8q")))

(define-public crate-y-craft-0.3 (crate (name "y-craft") (vers "0.3.0") (deps (list (crate-dep (name "image") (req "^0.24") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "sdl2") (req "^0.36") (features (quote ("ttf" "image" "mixer"))) (default-features #t) (kind 0)))) (hash "1b3k305p5d2adwmdq76hy6j5xnc6448r5fx34ypq4ch4lrxmcyxx")))

