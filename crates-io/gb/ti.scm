(define-module (crates-io gb ti) #:use-module (crates-io))

(define-public crate-gbtile-0.1 (crate (name "gbtile") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^2.33") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "png") (req "^0.16") (default-features #t) (kind 0)) (crate-dep (name "simple_logger") (req "^1.6") (default-features #t) (kind 0)))) (hash "1s4900cjvlcy3b1a9n0zaxpnj3hhjvhv3lhhzy6fz7vz4yca5ysl")))

(define-public crate-gbtile-0.2 (crate (name "gbtile") (vers "0.2.0") (deps (list (crate-dep (name "clap") (req "^2.33") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "png") (req "^0.16") (default-features #t) (kind 0)) (crate-dep (name "simple_logger") (req "^1.6") (default-features #t) (kind 0)))) (hash "12lfzpvj8fldbxlf34mkvjlah1nqyj3q1dyzrn1qg4mjxn81xc6d")))

