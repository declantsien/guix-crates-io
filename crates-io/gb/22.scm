(define-module (crates-io gb #{22}#) #:use-module (crates-io))

(define-public crate-gb2260-0.0.1 (crate (name "gb2260") (vers "0.0.1") (deps (list (crate-dep (name "phf") (req "^0.7") (default-features #t) (kind 0)))) (hash "1wl994yrsffrp82r08hcnfy45ym25kdc2ll43icfn63bhfihv021")))

(define-public crate-gb2260-0.0.2 (crate (name "gb2260") (vers "0.0.2") (deps (list (crate-dep (name "phf") (req "^0.7") (default-features #t) (kind 0)))) (hash "0758by662zpxx6x4vs09f5jsl27sv85vgg22vq0j502rg4317fvj")))

(define-public crate-gb2260-0.1 (crate (name "gb2260") (vers "0.1.0") (deps (list (crate-dep (name "phf") (req "^0.7") (default-features #t) (kind 0)))) (hash "0qf0k5zlvmp1v1dzv4cmk93rjrhgxx7mcprja87y21jgwlndf4h4")))

(define-public crate-gb2260-0.1 (crate (name "gb2260") (vers "0.1.1") (deps (list (crate-dep (name "phf") (req "^0.7") (default-features #t) (kind 0)))) (hash "0dvap25mc132izr9cah7ka4jzw58bpqig4vah29qp65pi6pbw2dn")))

(define-public crate-gb2260-0.1 (crate (name "gb2260") (vers "0.1.2") (deps (list (crate-dep (name "phf") (req "^0.8") (default-features #t) (kind 0)))) (hash "1r6s3iqffkpccvpwka92xaaqm2znx17wa4s6dr1b9pyjrhq4lbp0")))

