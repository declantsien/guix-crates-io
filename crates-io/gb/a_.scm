(define-module (crates-io gb a_) #:use-module (crates-io))

(define-public crate-gba_clock-0.1 (crate (name "gba_clock") (vers "0.1.0") (deps (list (crate-dep (name "claims") (req "^0.7.1") (default-features #t) (kind 2)) (crate-dep (name "deranged") (req "^0.3.8") (kind 0)) (crate-dep (name "serde") (req "^1.0.188") (optional #t) (kind 0)) (crate-dep (name "time") (req "^0.3.27") (kind 0)))) (hash "1l1b7hjzpslrnf2s14gbbvz0zh3g98hrvrv6z869w5418bcjyhzs") (v 2) (features2 (quote (("serde" "dep:serde" "deranged/serde" "time/serde"))))))

(define-public crate-gba_clock-0.2 (crate (name "gba_clock") (vers "0.2.0") (deps (list (crate-dep (name "claims") (req "^0.7.1") (default-features #t) (kind 2)) (crate-dep (name "deranged") (req "^0.3.8") (kind 0)) (crate-dep (name "serde") (req "^1.0.188") (optional #t) (kind 0)) (crate-dep (name "time") (req "^0.3.27") (kind 0)))) (hash "002ksfbvjri59gxildcggnmn9wcpdzgzmihsqm5r102xlc6s59di") (v 2) (features2 (quote (("serde" "dep:serde" "deranged/serde" "time/serde"))))))

(define-public crate-gba_clock-0.3 (crate (name "gba_clock") (vers "0.3.0") (deps (list (crate-dep (name "claims") (req "^0.7.1") (default-features #t) (kind 2)) (crate-dep (name "deranged") (req "^0.3.8") (kind 0)) (crate-dep (name "serde") (req "^1.0.188") (optional #t) (kind 0)) (crate-dep (name "time") (req "^0.3.27") (kind 0)))) (hash "1y260qa2rjcvq5y1j1f27ibfasah9smj0qmhf6h1ra3fmys8iq6n") (v 2) (features2 (quote (("serde" "dep:serde" "deranged/serde" "time/serde"))))))

(define-public crate-gba_clock-0.3 (crate (name "gba_clock") (vers "0.3.1") (deps (list (crate-dep (name "claims") (req "^0.7.1") (default-features #t) (kind 2)) (crate-dep (name "deranged") (req "^0.3.8") (kind 0)) (crate-dep (name "serde") (req "^1.0.188") (optional #t) (kind 0)) (crate-dep (name "time") (req "^0.3.27") (kind 0)))) (hash "113mdhmxf49mdxw3dfbm97li09is9zy9lqi40nnmpmigrnlzy0pk") (v 2) (features2 (quote (("serde" "dep:serde" "deranged/serde" "time/serde"))))))

(define-public crate-gba_env-1 (crate (name "gba_env") (vers "1.0.0") (hash "16hlilv3dmvdws1f3i4mw6fp8rh2wskxgqzb13dhwnj97iy4fha3")))

(define-public crate-gba_env-1 (crate (name "gba_env") (vers "1.1.0") (hash "1vi5sbmzvrhwz946h0vjlywrnfvrpp3gd7xyady443i3iii9mry4")))

