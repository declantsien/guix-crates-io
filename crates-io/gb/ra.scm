(define-module (crates-io gb ra) #:use-module (crates-io))

(define-public crate-gbranch-0.1 (crate (name "gbranch") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^2.32") (default-features #t) (kind 0)) (crate-dep (name "dialoguer") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "git2") (req "^0.7") (default-features #t) (kind 0)))) (hash "05sdx3hn6m23gy1kafwlgpjijgbh74x98dd6wy6np22mi8zl6m8z")))

