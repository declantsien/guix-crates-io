(define-module (crates-io gb af) #:use-module (crates-io))

(define-public crate-gbafix-0.0.0 (crate (name "gbafix") (vers "0.0.0") (deps (list (crate-dep (name "bytemuck") (req "^0.1") (default-features #t) (kind 0)))) (hash "072nhh7agnd5x7dpzkmy7b1kg6pi3gm83c9y3fm0n26vl6xcdq1d")))

(define-public crate-gbafix-1 (crate (name "gbafix") (vers "1.0.0") (deps (list (crate-dep (name "bytemuck") (req "^0.1") (default-features #t) (kind 0)))) (hash "1pi4gfh5jxhm310xvkcay4i6mfb676adh9p80d3w6mngyzb4xrrm")))

(define-public crate-gbafix-1 (crate (name "gbafix") (vers "1.0.1") (deps (list (crate-dep (name "bytemuck") (req "^0.1") (default-features #t) (kind 0)))) (hash "11n1rjs7giqikkizjfcyxzs0j6svaa32k4jywc5ikxjfyw4x5v87")))

(define-public crate-gbafix-1 (crate (name "gbafix") (vers "1.0.2") (deps (list (crate-dep (name "bytemuck") (req "^0.1") (default-features #t) (kind 0)))) (hash "1ahi51vphkwyns6a152w25l5qwi25p7mgv9gp9q7fcp71ikki070")))

(define-public crate-gbafix-1 (crate (name "gbafix") (vers "1.0.3") (deps (list (crate-dep (name "bytemuck") (req "^1") (default-features #t) (kind 0)))) (hash "1h33lp5mk9znyzhycw57vxvd118yqannhb24f4laqfgqn1wx8lhl")))

(define-public crate-gbafix-1 (crate (name "gbafix") (vers "1.0.4") (deps (list (crate-dep (name "bytemuck") (req "^1") (default-features #t) (kind 0)))) (hash "1dxldb11fx3r6z0zxicpbcp1yli8dipr3nakxw6qnyrpspwpmr7p")))

