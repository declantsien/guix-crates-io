(define-module (crates-io gb -c) #:use-module (crates-io))

(define-public crate-gb-cpu-sim-1 (crate (name "gb-cpu-sim") (vers "1.0.0") (deps (list (crate-dep (name "try-from-discrim") (req "^1.0.0") (default-features #t) (kind 0)))) (hash "1gkxjh1wm30p7l7p2vkwyxg9kwkkyazdd66vx9l0a9gdxdgp9l8l")))

(define-public crate-gb-cpu-sim-1 (crate (name "gb-cpu-sim") (vers "1.1.0") (deps (list (crate-dep (name "try-from-discrim") (req "^1.0.0") (default-features #t) (kind 0)))) (hash "0dg18qgl00c5qkbvarg3xpwrw1hah20pdsp4vp2684bbq5vs7ihq")))

