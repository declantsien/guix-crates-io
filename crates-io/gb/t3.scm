(define-module (crates-io gb t3) #:use-module (crates-io))

(define-public crate-gbt32960_parser-0.1 (crate (name "gbt32960_parser") (vers "0.1.0") (deps (list (crate-dep (name "chrono") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "encoding") (req "^0.2.33") (default-features #t) (kind 0)) (crate-dep (name "nom") (req "^7") (default-features #t) (kind 0)) (crate-dep (name "num_enum") (req "^0.5.1") (default-features #t) (kind 0)))) (hash "108q01pnvgpwaazd037m9rx4nkwx4rakg8ib8c7rrdp8k54ridxh")))

