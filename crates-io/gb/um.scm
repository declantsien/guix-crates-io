(define-module (crates-io gb um) #:use-module (crates-io))

(define-public crate-gbump-0.1 (crate (name "gbump") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^2.33.0") (default-features #t) (kind 0)) (crate-dep (name "git2") (req "^0.10") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.3.1") (default-features #t) (kind 0)))) (hash "1d55l09q4qsrkrp2jkgzvdxzz2if047ji5lrsbw2vag5v9l4ffxq")))

(define-public crate-gbump-0.2 (crate (name "gbump") (vers "0.2.0") (deps (list (crate-dep (name "clap") (req "^2.33.0") (default-features #t) (kind 0)) (crate-dep (name "git2") (req "^0.10") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.3.1") (default-features #t) (kind 0)))) (hash "19piv1hb81z1qw8fvnddjxdg5c7irdb8qwkbimksi231nwk37g3n")))

(define-public crate-gbump-0.3 (crate (name "gbump") (vers "0.3.0") (deps (list (crate-dep (name "clap") (req "^2.33.0") (default-features #t) (kind 0)) (crate-dep (name "git2") (req "^0.10") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.3.1") (default-features #t) (kind 0)))) (hash "1c3zrb1sk76l31dph4xaj0yxahqg1d6hgrcnmid13lmnyf7qhdjs")))

(define-public crate-gbump-0.3 (crate (name "gbump") (vers "0.3.1") (deps (list (crate-dep (name "clap") (req "^2.33.0") (default-features #t) (kind 0)) (crate-dep (name "git2") (req "^0.10") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.3.1") (default-features #t) (kind 0)))) (hash "03avmy3www16f9rm8w91bf4w6b3pnaa8fzcf79bcr3d1c1a0fv3l")))

(define-public crate-gbump-0.4 (crate (name "gbump") (vers "0.4.0") (deps (list (crate-dep (name "clap") (req "^2.33.0") (default-features #t) (kind 0)) (crate-dep (name "git2") (req "^0.10") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.3.1") (default-features #t) (kind 0)))) (hash "1i00a56mv3l0kcyv4gyid2ilfvx37anr80jvy824piwidl6pl3ki")))

(define-public crate-gbump-1 (crate (name "gbump") (vers "1.0.0") (deps (list (crate-dep (name "clap") (req "^2.33.0") (default-features #t) (kind 0)) (crate-dep (name "git2") (req "^0.10") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.3.1") (default-features #t) (kind 0)))) (hash "1n7b0z5py163kgfnrfixf2fxln091wj49xw2847065hbsxfhcqxk")))

(define-public crate-gbump-1 (crate (name "gbump") (vers "1.0.1") (deps (list (crate-dep (name "clap") (req "^2.33.0") (default-features #t) (kind 0)) (crate-dep (name "git2") (req "^0.10") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.3.1") (default-features #t) (kind 0)))) (hash "07mwgxfxgrj6r33kyk4dkbnzj5czibgp5l3mvk9d6j7k50cdgvgi")))

(define-public crate-gbump-1 (crate (name "gbump") (vers "1.0.2") (deps (list (crate-dep (name "clap") (req "^2.33") (default-features #t) (kind 0)) (crate-dep (name "git2") (req "^0.11") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.3") (default-features #t) (kind 0)))) (hash "1jhh3gas3qd0lq8wdgd24s0jzgsabq2g0gmga0dn2c8x20kywjjv")))

(define-public crate-gbump-1 (crate (name "gbump") (vers "1.0.3") (deps (list (crate-dep (name "clap") (req "^2.33") (default-features #t) (kind 0)) (crate-dep (name "git2") (req "^0.13") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.3") (default-features #t) (kind 0)))) (hash "0vhwq5a2nw8br1gcmrdp5q0p5dzr4dvg2q0fj4hm34pwdggj69rx")))

(define-public crate-gbump-1 (crate (name "gbump") (vers "1.0.4") (deps (list (crate-dep (name "clap") (req "^2.33") (default-features #t) (kind 0)) (crate-dep (name "git2") (req "^0.13") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.3") (default-features #t) (kind 0)))) (hash "0fjlb52swfspl6jq2fjfzn6bza2h666grmlbf57j54awwxc3rbmw")))

(define-public crate-gbump-1 (crate (name "gbump") (vers "1.0.5") (deps (list (crate-dep (name "clap") (req "^2.33") (default-features #t) (kind 0)) (crate-dep (name "git2") (req "^0.13") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.4") (default-features #t) (kind 0)))) (hash "0z84cg0pgx6pgv9hgvk5djjnzxsjlmwr9x9p9v2q7mj4ia1klckj")))

(define-public crate-gbump-1 (crate (name "gbump") (vers "1.0.6") (deps (list (crate-dep (name "clap") (req "^3.2") (default-features #t) (kind 0)) (crate-dep (name "git2") (req "^0.15") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.6") (default-features #t) (kind 0)))) (hash "0zfvp0dyq0k45nwb1c1idbkc9qwl49xyr7cahnsp6pmpacgcx161")))

(define-public crate-gbump-1 (crate (name "gbump") (vers "1.0.7") (deps (list (crate-dep (name "clap") (req "^3.2") (default-features #t) (kind 0)) (crate-dep (name "git2") (req "^0.15") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.6") (default-features #t) (kind 0)))) (hash "0dl82wlcabgxizvkangmi7g09ycldmxxbbx4z2wsm844x54f7c6x")))

(define-public crate-gbump-1 (crate (name "gbump") (vers "1.0.8") (deps (list (crate-dep (name "clap") (req "^3") (default-features #t) (kind 0)) (crate-dep (name "git2") (req "^0.16") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.7") (default-features #t) (kind 0)))) (hash "0w16afxhnwalqigx1fs214y1mfzq9v7qdfx89z0i75fa9z7jffdr")))

(define-public crate-gbump-1 (crate (name "gbump") (vers "1.0.9") (deps (list (crate-dep (name "clap") (req "^3") (default-features #t) (kind 0)) (crate-dep (name "git2") (req "^0.17") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.8") (default-features #t) (kind 0)))) (hash "00zwkk53gzn97gq8rvy2g3i90x3422fyq51i05s0yk7rk113ffh9")))

(define-public crate-gbump-1 (crate (name "gbump") (vers "1.0.10") (deps (list (crate-dep (name "clap") (req "^4") (default-features #t) (kind 0)) (crate-dep (name "git2") (req "^0.18") (default-features #t) (kind 0)) (crate-dep (name "openssl") (req "^0.10") (features (quote ("vendored"))) (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.8") (default-features #t) (kind 0)))) (hash "066iswr5zk381vxvz322wi7im8gzg2krhd238xfcp4as7s37ac4b")))

(define-public crate-gbump-1 (crate (name "gbump") (vers "1.1.0") (deps (list (crate-dep (name "clap") (req "^4") (default-features #t) (kind 0)) (crate-dep (name "git2") (req "^0.18") (default-features #t) (kind 0)) (crate-dep (name "openssl") (req "^0.10") (features (quote ("vendored"))) (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.8") (default-features #t) (kind 0)))) (hash "0q9y26n2xs321jvlli2a26iid7xsfnagdzacw9ffja8r1rx86clg")))

(define-public crate-gbump-1 (crate (name "gbump") (vers "1.1.1") (deps (list (crate-dep (name "clap") (req "^4") (default-features #t) (kind 0)) (crate-dep (name "git2") (req "^0.18") (default-features #t) (kind 0)) (crate-dep (name "openssl") (req "^0.10") (features (quote ("vendored"))) (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.8") (default-features #t) (kind 0)))) (hash "0ffbicjwbj8c2px6bixhlz12gjy67q0wwvxh4pgw8ac9pd0fshg7")))

(define-public crate-gbump-1 (crate (name "gbump") (vers "1.1.2") (deps (list (crate-dep (name "clap") (req "^4") (default-features #t) (kind 0)) (crate-dep (name "git2") (req "^0.18") (default-features #t) (kind 0)) (crate-dep (name "openssl") (req "^0.10") (features (quote ("vendored"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.8") (default-features #t) (kind 0)))) (hash "08k3d9pn1zm85dc0bhz86frhs4r0wqbbg96bv4p63rf4hm4v1x3v")))

