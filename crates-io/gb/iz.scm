(define-module (crates-io gb iz) #:use-module (crates-io))

(define-public crate-gbiz-info-api-0.1 (crate (name "gbiz-info-api") (vers "0.1.0") (deps (list (crate-dep (name "dotenv") (req "^0.15.0") (default-features #t) (kind 2)) (crate-dep (name "log") (req "^0.4.21") (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "~0") (features (quote ("json"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "~1") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "~1") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "~1") (features (quote ("full"))) (default-features #t) (kind 2)))) (hash "0h4crgjascqdhab972fyylgsh8hf12hcqa1lbr28b8ib5vh2vs8h")))

