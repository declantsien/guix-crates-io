(define-module (crates-io gb a-) #:use-module (crates-io))

(define-public crate-gba-addresses-0.0.1 (crate (name "gba-addresses") (vers "0.0.1") (hash "0mms2sgbcz70gqqbwr77vs3k7clcw5vwyvx6m8i9zlmfkrz0d3yf")))

(define-public crate-gba-addresses-0.1 (crate (name "gba-addresses") (vers "0.1.0") (hash "0chi64h176fw9py79da2rixd9gc4fq031vyzy8fvrhcvbx2zb6d2")))

(define-public crate-gba-compression-0.1 (crate (name "gba-compression") (vers "0.1.0") (hash "0xi2rfz5gsr8qyl6ps3jla8fz8bpb22x7fdjb9hx362hcil1s4xg")))

(define-public crate-gba-hal-0.0.1 (crate (name "gba-hal") (vers "0.0.1") (deps (list (crate-dep (name "gba-proc-macro") (req "^0.6") (default-features #t) (kind 0)) (crate-dep (name "typenum") (req "^1.10") (default-features #t) (kind 0)) (crate-dep (name "voladdress") (req "^0.2") (default-features #t) (kind 0)))) (hash "12nmcz8sy1vr4lyp2igy256n6frdiydsbx132sqjcd581a2y7p84") (features (quote (("unsafe_docs_rs_mmio_listing_override") ("default"))))))

(define-public crate-gba-img-convert-0.1 (crate (name "gba-img-convert") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^4.1.4") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "imagine") (req "^0.3.0") (features (quote ("miniz_oxide"))) (default-features #t) (kind 0)))) (hash "0asd0gzla12pxkjl9nf7d1izasgdi8qj9xq9kw4b2z8gdalz2b8w")))

(define-public crate-gba-make-cartridge-1 (crate (name "gba-make-cartridge") (vers "1.0.0") (deps (list (crate-dep (name "clap") (req "~2.23.2") (default-features #t) (kind 0)) (crate-dep (name "error-chain") (req "~0.10.0") (default-features #t) (kind 0)) (crate-dep (name "tempfile") (req "^2.1.5") (default-features #t) (kind 0)))) (hash "0vjgawwyd1amw1i95x3mkfhmvfpn9nr6m7mnh8b0l304lafhpgxq")))

(define-public crate-gba-make-cartridge-1 (crate (name "gba-make-cartridge") (vers "1.0.1") (deps (list (crate-dep (name "clap") (req "~2.23.2") (default-features #t) (kind 0)) (crate-dep (name "error-chain") (req "~0.10.0") (default-features #t) (kind 0)) (crate-dep (name "tempfile") (req "^2.1.5") (default-features #t) (kind 0)))) (hash "1xl7wagzpihhlg0srxc1qyibfkmyvgazzs7vyvfrwjc69vs5hlj0")))

(define-public crate-gba-net-boot-0.0.0 (crate (name "gba-net-boot") (vers "0.0.0") (hash "0fjqhlaq5v8x6dr2lgia66n5zpvbb86f9w24qmp1m24knpmakfkb")))

(define-public crate-gba-net-send-0.0.1 (crate (name "gba-net-send") (vers "0.0.1") (hash "1mkirn18jjq3bi1p11fd8n5a4fprvkvfkylckd81cx8qxwnrhlgq")))

(define-public crate-gba-proc-macro-0.1 (crate (name "gba-proc-macro") (vers "0.1.0") (deps (list (crate-dep (name "proc-macro2") (req "^0.4.20") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^0.6.9") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^0.15.18") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "0n2hgcji6k4f1rvc7y1vwqbyr12p78iybwf4lk1a40vi2nhlb394")))

(define-public crate-gba-proc-macro-0.1 (crate (name "gba-proc-macro") (vers "0.1.1") (deps (list (crate-dep (name "proc-macro2") (req "^0.4.20") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^0.6.9") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^0.15.18") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "1d35csgzz49dy4sdzdlwi2vvyrmgjai1ybjdqp6w0m8hdmdsdz6w")))

(define-public crate-gba-proc-macro-0.2 (crate (name "gba-proc-macro") (vers "0.2.0") (deps (list (crate-dep (name "proc-macro2") (req "^0.4.20") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^0.6.9") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^0.15.18") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "1ixxv4r71lhria3dvwzji0axq5g1pnrpp9h5bxrwl04mf6wn0w31")))

(define-public crate-gba-proc-macro-0.2 (crate (name "gba-proc-macro") (vers "0.2.1") (deps (list (crate-dep (name "proc-macro2") (req "^0.4.20") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^0.6.9") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^0.15.18") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "1jnndmpwk9z06ax2131212i3rdw3v4xx92nx4bjy3qj82az77ai0")))

(define-public crate-gba-proc-macro-0.3 (crate (name "gba-proc-macro") (vers "0.3.0") (deps (list (crate-dep (name "proc-macro2") (req "^0.4.20") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^0.6.9") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^0.15.18") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "0hymf29wgz40isnp9q1zhzpsas3hsjxvd0vcjpclggfq8a9ccxwz")))

(define-public crate-gba-proc-macro-0.4 (crate (name "gba-proc-macro") (vers "0.4.0") (deps (list (crate-dep (name "proc-macro2") (req "^0.4.24") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^0.6.9") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^0.15.23") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "0qja56si81flfdppghgv8sdhx7qx61ab65aynq6x7pdj9x130phc")))

(define-public crate-gba-proc-macro-0.4 (crate (name "gba-proc-macro") (vers "0.4.1") (deps (list (crate-dep (name "proc-macro2") (req "^0.4.24") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^0.6.9") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^0.15.23") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "1myh6b7q643fkdv9a7yz21lvbkysg1xxlrhqmcibi01irn4n8zw4")))

(define-public crate-gba-proc-macro-0.5 (crate (name "gba-proc-macro") (vers "0.5.0") (deps (list (crate-dep (name "proc-macro2") (req "^0.4.24") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^0.6.9") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^0.15.23") (features (quote ("full" "extra-traits"))) (default-features #t) (kind 0)))) (hash "1b153n0wp2pq1s2lbrimx34hcisvrg895k14blbc3r8fk62p5y1c")))

(define-public crate-gba-proc-macro-0.6 (crate (name "gba-proc-macro") (vers "0.6.0") (deps (list (crate-dep (name "proc-macro2") (req "^0.4.24") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^0.6.9") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.5.5") (kind 0)) (crate-dep (name "syn") (req "^0.15.23") (features (quote ("full" "extra-traits"))) (default-features #t) (kind 0)))) (hash "0sjcl3n5yq58vcyls5wmrljws0fgz71gr9xj34yym3rl0vpl6igj")))

(define-public crate-gba-types-0.0.1 (crate (name "gba-types") (vers "0.0.1") (hash "1f3rqic33x6ka684zrvypfw8pf99w16942v2k6fnjh6k50bkfpvv")))

(define-public crate-gba-types-0.0.2 (crate (name "gba-types") (vers "0.0.2") (hash "1n7x80f215nwma4fgr8qxfrm30sah88p1yvfjd777nvhfa3zj0np")))

(define-public crate-gba-types-0.1 (crate (name "gba-types") (vers "0.1.0") (hash "1rwn67vxp2jyym7p7iii91braqxjbl4f33ypczc2kmkh8pnw2sby")))

