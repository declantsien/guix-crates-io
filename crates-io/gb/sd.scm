(define-module (crates-io gb sd) #:use-module (crates-io))

(define-public crate-gbsdiff-1 (crate (name "gbsdiff") (vers "1.0.0") (deps (list (crate-dep (name "argh") (req "^0.1.9") (default-features #t) (kind 0)) (crate-dep (name "gb-cpu-sim") (req "^1.1.0") (default-features #t) (kind 0)) (crate-dep (name "owo-colors") (req "^3.5.0") (features (quote ("supports-colors"))) (default-features #t) (kind 0)) (crate-dep (name "parse-display") (req "^0.6.0") (kind 0)) (crate-dep (name "slicedisplay") (req "^0.2.2") (default-features #t) (kind 0)))) (hash "04c7z646kagxmpqzkgagvfhv9k3szhc5z7izb15ajzyp56p1y9n3")))

