(define-module (crates-io gb en) #:use-module (crates-io))

(define-public crate-gbench-0.1 (crate (name "gbench") (vers "0.1.0") (hash "1il2k4s23b6x17k1fa8g6sps8gnc90d029805xvshz916i4js3r3")))

(define-public crate-gbench-0.2 (crate (name "gbench") (vers "0.2.0") (hash "17vfc47id9ycmmhkbhz4lyw0y9fci59p51ln7lz3r9c2ggi33gqq")))

(define-public crate-gbench-0.2 (crate (name "gbench") (vers "0.2.1") (hash "0winzqrs0g08g4153dgn070rhpdahhw8k31jg6ijxhbya3nn0nwp")))

(define-public crate-gbench-0.2 (crate (name "gbench") (vers "0.2.2") (hash "0cpbzj31vrgzh06kidvalanc4q1cbvm3qs7ankn1641pgvr5hcz0")))

(define-public crate-gbench-0.3 (crate (name "gbench") (vers "0.3.0") (hash "0s72qwibpymrps3fd7v3q9261jfryc96alksisd4hxfxcl7fa4y5")))

(define-public crate-gbench-0.3 (crate (name "gbench") (vers "0.3.1") (hash "10hvksb5va2qyylh2wqikazm4w3vr030kxqyrsbh2ij9lp53lyws")))

(define-public crate-gbench-1 (crate (name "gbench") (vers "1.0.0") (hash "15fb6h4m9c73qrl1np927acca42srni1rikif7bqa0bbvy0gn36n")))

(define-public crate-gbench-1 (crate (name "gbench") (vers "1.0.1") (hash "1mwvjniy29g4waarqa39610ybpgq53q8h84r2h8fml8ybg2f5iqw")))

(define-public crate-gbenchmark-0.1 (crate (name "gbenchmark") (vers "0.1.0") (hash "0dgy40b58f5kkbrpislmb06qg2kj1ws0hi1gls0md5zs5nqhdkzn")))

