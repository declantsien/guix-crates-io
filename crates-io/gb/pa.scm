(define-module (crates-io gb pa) #:use-module (crates-io))

(define-public crate-gbparser-0.1 (crate (name "gbparser") (vers "0.1.0") (deps (list (crate-dep (name "pest") (req "^2.1") (default-features #t) (kind 0)) (crate-dep (name "pest_derive") (req "^2.1") (default-features #t) (kind 0)))) (hash "11wk5hdbjzsysi0gvgkxavzd09ddp8r9njdcq45jz5cvf4mvic7m")))

