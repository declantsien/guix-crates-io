(define-module (crates-io gb -s) #:use-module (crates-io))

(define-public crate-gb-sym-file-1 (crate (name "gb-sym-file") (vers "1.0.0") (deps (list (crate-dep (name "thiserror") (req "^1.0.37") (default-features #t) (kind 0)))) (hash "1qp521bw4vdqx0xxs0nbwwrlxpj66k762rpq7id7fihcddfsmigv")))

(define-public crate-gb-sym-file-1 (crate (name "gb-sym-file") (vers "1.0.1") (deps (list (crate-dep (name "thiserror") (req "^1.0.37") (default-features #t) (kind 0)))) (hash "14bqmc3vniw8grq1wbkym54pmafyd18g5x77x2zysshrd9336g0k")))

