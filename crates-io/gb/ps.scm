(define-module (crates-io gb ps) #:use-module (crates-io))

(define-public crate-gbps-0.1 (crate (name "gbps") (vers "0.1.0") (deps (list (crate-dep (name "log") (req "^0.4.11") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.7.3") (default-features #t) (kind 0)))) (hash "053nhaj70p5fnmw62a6q3fl04l9i1s3z2w4zp1jn6r3ibd8dnpqq")))

(define-public crate-gbps-0.2 (crate (name "gbps") (vers "0.2.0") (deps (list (crate-dep (name "log") (req "^0.4.11") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.7.3") (default-features #t) (kind 0)))) (hash "1zpzglvkcmyikqpil9qra4gdw8g40x6py31rjxn2ghgnyr5vah7a")))

