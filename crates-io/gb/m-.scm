(define-module (crates-io gb m-) #:use-module (crates-io))

(define-public crate-gbm-rs-0.1 (crate (name "gbm-rs") (vers "0.1.0") (hash "1r39jn2cc04pi08i9rfip8djzmxhpw6p8x1wa31miah56dwh3b1y")))

(define-public crate-gbm-rs-0.2 (crate (name "gbm-rs") (vers "0.2.0") (deps (list (crate-dep (name "libc") (req "^0.2.16") (default-features #t) (kind 0)))) (hash "0vd1qz7ywzyk5222sa75pf5p7nbw16shn8q9ki3bbr8wvcxq64s7")))

(define-public crate-gbm-sys-0.1 (crate (name "gbm-sys") (vers "0.1.0") (deps (list (crate-dep (name "bindgen") (req "^0.22") (optional #t) (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "1xl3h4dyrj44w0d1qjylnjhsjsssgnimpjbbdb1dc01alnd1gdp0") (features (quote (("gen" "bindgen"))))))

(define-public crate-gbm-sys-0.2 (crate (name "gbm-sys") (vers "0.2.0") (deps (list (crate-dep (name "bindgen") (req "^0.58") (optional #t) (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "1ph2658l0xy5y0wxmx08034029x0akqhm0bf4jgpvfgcmriv7qy1") (features (quote (("update_bindings" "gen") ("gen" "bindgen"))))))

(define-public crate-gbm-sys-0.2 (crate (name "gbm-sys") (vers "0.2.1") (deps (list (crate-dep (name "bindgen") (req "^0.58") (optional #t) (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "05by5hihchbshpkpfkl4756q3pyx4vq68zm1y5ssyz3f5s5mfrn3") (features (quote (("update_bindings" "gen") ("gen" "bindgen"))))))

(define-public crate-gbm-sys-0.2 (crate (name "gbm-sys") (vers "0.2.2") (deps (list (crate-dep (name "bindgen") (req "^0.58") (optional #t) (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "0rxgzvmk861q7x06js5c97qgkj81jdshisrd90a1a8vskfdvlgmn") (features (quote (("update_bindings" "gen") ("gen" "bindgen"))))))

(define-public crate-gbm-sys-0.3 (crate (name "gbm-sys") (vers "0.3.0") (deps (list (crate-dep (name "bindgen") (req "^0.69") (optional #t) (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1.0.69") (optional #t) (default-features #t) (kind 1)) (crate-dep (name "regex") (req "^1.10") (optional #t) (default-features #t) (kind 1)))) (hash "0n6w1lmsk3gjizsjpxilhjda8lf8qjjzj1ffxs5v6hq1gjzxdlkg") (features (quote (("update_bindings" "use_bindgen")))) (v 2) (features2 (quote (("use_bindgen" "bindgen" "dep:proc-macro2" "dep:regex"))))))

