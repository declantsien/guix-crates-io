(define-module (crates-io gb -p) #:use-module (crates-io))

(define-public crate-gb-parser-0.0.1 (crate (name "gb-parser") (vers "0.0.1") (hash "049pqjc8pldr1673lvyb2l8rv3mr4sfiha6rxp3369cx6lv23rnn")))

(define-public crate-gb-parser-0.0.2 (crate (name "gb-parser") (vers "0.0.2") (hash "17a5dlfy3hh9gbwxnknx4s04b0xhxvaw6yb1x34rii4mkrfa52z7")))

(define-public crate-gb-parser-0.0.3 (crate (name "gb-parser") (vers "0.0.3") (hash "0d96zw3gmzknfcl89j2xj8m9m3x78c1cnnp6rdqgyhrqdprnnqcr")))

(define-public crate-gb-parser-0.0.4 (crate (name "gb-parser") (vers "0.0.4") (hash "1j2xw2ivqfx5zwim5n09pz24vy0p2ygihvr62v7gs45682bw3xk1")))

(define-public crate-gb-parser-0.0.5 (crate (name "gb-parser") (vers "0.0.5") (hash "1gp88651xsczal8birlf4z7kfrvwy6ydvi8gyd5plnwzlfv8f13x")))

(define-public crate-gb-parser-0.0.6 (crate (name "gb-parser") (vers "0.0.6") (hash "1hlgpmslxijgm2i1a5avi8is7jk9yqdc01fkgh6x2kffj5x0aniy")))

(define-public crate-gb-parser-0.0.7 (crate (name "gb-parser") (vers "0.0.7") (hash "0yhnirn97255p9g24jinnxgxw23246wkwkd4qap2mnfks35plpc1")))

