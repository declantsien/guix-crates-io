(define-module (crates-io ef l-) #:use-module (crates-io))

(define-public crate-efl-sys-0.1 (crate (name "efl-sys") (vers "0.1.0") (deps (list (crate-dep (name "bindgen") (req "^0.69.4") (default-features #t) (kind 1)) (crate-dep (name "pkg-config") (req "^0.3.30") (default-features #t) (kind 1)))) (hash "1443h31lkk8nq96nhlp5ajz8fbfswpvs50mn2xah9gkgd3icbva4") (links "efl")))

(define-public crate-efl-sys-0.2 (crate (name "efl-sys") (vers "0.2.0") (deps (list (crate-dep (name "bindgen") (req "^0.69.4") (default-features #t) (kind 1)) (crate-dep (name "pkg-config") (req "^0.3.30") (default-features #t) (kind 1)))) (hash "17hpargr0ys5d7p3qddx8k6hxlxamrbwkfl0ai1cxd10l9ymjizj") (links "efl")))

