(define-module (crates-io ef f_) #:use-module (crates-io))

(define-public crate-eff_data-0.1 (crate (name "eff_data") (vers "0.1.0") (deps (list (crate-dep (name "binrw") (req "^0.12.0") (default-features #t) (kind 0)) (crate-dep (name "eff_lib") (req "^0.1.0") (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)))) (hash "1xc9narfrm3qpxkv0n9nk4mjkhrr05svirqqygk0spyv6nydr8kk") (v 2) (features2 (quote (("serde" "dep:serde" "eff_lib/serde"))))))

(define-public crate-eff_lib-0.1 (crate (name "eff_lib") (vers "0.1.0") (deps (list (crate-dep (name "binrw") (req "^0.12.0") (default-features #t) (kind 0)) (crate-dep (name "modular-bitfield") (req "^0.11.2") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)))) (hash "1a6mzji50nynm23w5icmalwh4ap3lin4a4xmphbpm7v2xr6apbd6") (v 2) (features2 (quote (("serde" "dep:serde"))))))

