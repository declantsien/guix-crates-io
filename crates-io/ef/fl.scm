(define-module (crates-io ef fl) #:use-module (crates-io))

(define-public crate-efflux-1 (crate (name "efflux") (vers "1.0.0") (hash "0h1z9l6la3lqxylg2cbjh5vqkvcazb6vhjwzqskpjlsxqbysf6v0")))

(define-public crate-efflux-1 (crate (name "efflux") (vers "1.0.1") (hash "0mivp6s9d6xawv4qlwz6izfs935w7bgmfz4ib32gf6gwj9bgibzp")))

(define-public crate-efflux-1 (crate (name "efflux") (vers "1.0.2") (hash "0p4b561x8nixjcv8dq1f5115m897024ipdsakkc7lfrq7harxkma")))

(define-public crate-efflux-1 (crate (name "efflux") (vers "1.1.0") (hash "1cqki6cck3m0zwimskrcsraq8vikjanfaqqpb5i06340qyn8qzqv")))

(define-public crate-efflux-1 (crate (name "efflux") (vers "1.2.0") (deps (list (crate-dep (name "bytelines") (req "^2.2") (default-features #t) (kind 0)))) (hash "1pfbm0ris7sh38kyn42jfx1kxw6h424jms1ps979bwfn77jlhd6a")))

(define-public crate-efflux-2 (crate (name "efflux") (vers "2.0.0") (deps (list (crate-dep (name "bytelines") (req "^2.2") (default-features #t) (kind 0)) (crate-dep (name "twoway") (req "^0.2") (default-features #t) (kind 0)))) (hash "0cdg00nc0cna3920iq3540rf4whsqfd88hzdw1708jh1z67h8zv1")))

(define-public crate-efflux-2 (crate (name "efflux") (vers "2.0.1") (deps (list (crate-dep (name "bytelines") (req "^2.2") (default-features #t) (kind 0)) (crate-dep (name "twoway") (req "^0.2") (default-features #t) (kind 0)))) (hash "11n9nk9svcs26wzmnj230yc720mng9s2ypzmys1k1yxd0d9y7jri")))

