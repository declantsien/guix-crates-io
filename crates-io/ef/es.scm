(define-module (crates-io ef es) #:use-module (crates-io))

(define-public crate-efes-1 (crate (name "efes") (vers "1.0.0") (deps (list (crate-dep (name "tempfile") (req "^3.2") (default-features #t) (kind 2)))) (hash "0zvhp2c6zj6psa5dlldwlcl8ll3ks4pw1yyb34l8pc123qnwv3gg")))

(define-public crate-efes-1 (crate (name "efes") (vers "1.0.1") (deps (list (crate-dep (name "tempfile") (req "^3.2") (default-features #t) (kind 2)))) (hash "057nl1pdb8d0ij13gci67i5vzqnmih3h5g9rk03j0y37v3fqn4hb")))

