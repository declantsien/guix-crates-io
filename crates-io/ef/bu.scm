(define-module (crates-io ef bu) #:use-module (crates-io))

(define-public crate-efbuilder-0.0.1 (crate (name "efbuilder") (vers "0.0.1") (deps (list (crate-dep (name "proc-macro-error") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1") (default-features #t) (kind 0)))) (hash "1q34sbskd49jv4kd7p9brhg18bkajaa24lhi6yvw9k3znlv4wlkg") (yanked #t)))

(define-public crate-efbuilder-0.0.2 (crate (name "efbuilder") (vers "0.0.2") (deps (list (crate-dep (name "proc-macro-error") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1") (default-features #t) (kind 0)))) (hash "1il1hcj8g36r8lv5qqjwx6sjfqyl3h3v6qvh2p4hz9jb0c8rzkf0") (yanked #t)))

(define-public crate-efbuilder-0.0.3 (crate (name "efbuilder") (vers "0.0.3") (deps (list (crate-dep (name "proc-macro-error") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1") (default-features #t) (kind 0)))) (hash "0k0vkqzbr01vmkjl8nx3g45ppqbsskmrvdkldzc63d9cp8jsm0ii")))

(define-public crate-efbuilder-0.0.4 (crate (name "efbuilder") (vers "0.0.4") (deps (list (crate-dep (name "proc-macro-error") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1") (default-features #t) (kind 0)))) (hash "18hc91za1h5b4b79wyaj2lwi3z6ybnvxd418sbf6bkfr2mp7naig")))

