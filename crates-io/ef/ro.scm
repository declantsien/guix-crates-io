(define-module (crates-io ef ro) #:use-module (crates-io))

(define-public crate-efron_numbertheory-0.3 (crate (name "efron_numbertheory") (vers "0.3.1") (deps (list (crate-dep (name "maplit") (req "^1.0.1") (default-features #t) (kind 0)))) (hash "0ryrywzqklcdrkx4xv5qinb6zhhsmnmhi73499j19qavqckggv8h")))

