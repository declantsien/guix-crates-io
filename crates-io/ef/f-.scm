(define-module (crates-io ef f-) #:use-module (crates-io))

(define-public crate-eff-attr-0.0.2 (crate (name "eff-attr") (vers "0.0.2") (deps (list (crate-dep (name "proc-macro-hack") (req "^0.5") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^0.6") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^0.15") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "0dgb6vlyx38v5czzbgf2zpck2vbh3wjwf0qs0vky1r7psdg4wxzh")))

(define-public crate-eff-attr-0.0.3 (crate (name "eff-attr") (vers "0.0.3") (deps (list (crate-dep (name "proc-macro-hack") (req "^0.5") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^0.6") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^0.15") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "13fb4crvs21pkpan8lnsl1p9kwkry0mn5f6nvb5h3ljv0559pd78")))

(define-public crate-eff-attr-0.0.4 (crate (name "eff-attr") (vers "0.0.4") (deps (list (crate-dep (name "proc-macro-hack") (req "^0.5") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^0.6") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^0.15") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "0bpzj9bf0dsxsj87x5i14lhxcvx7byvjw3nxxa30hrxxlaik8znh")))

(define-public crate-eff-attr-0.0.5 (crate (name "eff-attr") (vers "0.0.5") (deps (list (crate-dep (name "proc-macro-hack") (req "^0.5") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^0.6") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^0.15") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "195vdf6cbbqfxrqzvziqr0wg0fyq5nbv1c74wbpfsaa8nx5sa5pz")))

(define-public crate-eff-attr-0.0.6 (crate (name "eff-attr") (vers "0.0.6") (deps (list (crate-dep (name "proc-macro-hack") (req "^0.5") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^0.6") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^0.15") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "1mj4dyjq7ylf68pn6nd88smjmria2f5s060m87l6a2n1f98j6g7s")))

(define-public crate-eff-attr-0.0.7 (crate (name "eff-attr") (vers "0.0.7") (deps (list (crate-dep (name "proc-macro-hack") (req "^0.5") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^0.6") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^0.15") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "0mkmgpi9cyg75xzp9r7q3qamasch5ylp7i5g0svmbm12rwf5rxnp")))

(define-public crate-eff-attr-0.0.8 (crate (name "eff-attr") (vers "0.0.8") (deps (list (crate-dep (name "proc-macro-hack") (req "^0.5") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^0.6") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^0.15") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "0hz9b6185dnqv3cgxwd7rr4mm2wi8gwkzz6sbyx8cf83q3746mf8")))

(define-public crate-eff-attr-0.1 (crate (name "eff-attr") (vers "0.1.0") (deps (list (crate-dep (name "proc-macro-hack") (req "^0.5.11") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1.0.6") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.2") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.8") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "0igg3zp371g7rpapr0hwyx48ll5ajcvam3kqg8ha6wi3x91fmvyj")))

(define-public crate-eff-lexical-data-1 (crate (name "eff-lexical-data") (vers "1.0.0-rc.1") (hash "1z1949p4namv2v71jklg3ap99jzlfg8i02w9r2c69sw22zhkch66")))

(define-public crate-eff-lexical-data-1 (crate (name "eff-lexical-data") (vers "1.0.0") (hash "0vk7caxvr8kbj08adbv57zpkwylqdzzfcixccg50gpjxqqaqgva9")))

(define-public crate-eff-wordlist-0.1 (crate (name "eff-wordlist") (vers "0.1.0") (hash "09h54w4hm60drwaz4mis5shxza1f5k7359aiwvq0pl2w0rdi4qfg")))

(define-public crate-eff-wordlist-0.1 (crate (name "eff-wordlist") (vers "0.1.1") (deps (list (crate-dep (name "arrayvec") (req "^0.4.10") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.6.5") (default-features #t) (kind 0)))) (hash "1hgvc848wvg7wyl27rahzrj3jfjmb85k5naf31d082qx2k3mid9r")))

(define-public crate-eff-wordlist-0.1 (crate (name "eff-wordlist") (vers "0.1.2") (deps (list (crate-dep (name "arrayvec") (req "^0.4.10") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.6.5") (default-features #t) (kind 0)))) (hash "04rzknnr3hnpvmayp6xs02an3x7kmik49ckqkxl61kdsis3saib2")))

(define-public crate-eff-wordlist-1 (crate (name "eff-wordlist") (vers "1.0.0") (deps (list (crate-dep (name "arrayvec") (req "^0.4.10") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.6.5") (default-features #t) (kind 0)))) (hash "0qlwf3wg0rxikhqkdhib8zpvxz1dac2vskvr8macn60higxk764j")))

(define-public crate-eff-wordlist-1 (crate (name "eff-wordlist") (vers "1.0.1") (deps (list (crate-dep (name "rand") (req "^0.6.5") (default-features #t) (kind 0)))) (hash "0mm8mzk4hvh5dkgz2jspadmv17pny13fs9rhkr8j88izy5482y31")))

(define-public crate-eff-wordlist-1 (crate (name "eff-wordlist") (vers "1.0.2") (deps (list (crate-dep (name "rand") (req "^0.6") (default-features #t) (kind 0)))) (hash "092cngjpdg9zhzvwx8fykr395sk6lclxvph69m29yxkbi9cc4v12")))

(define-public crate-eff-wordlist-1 (crate (name "eff-wordlist") (vers "1.0.3") (deps (list (crate-dep (name "rand") (req "^0.8") (default-features #t) (kind 0)))) (hash "0av1n35vpwyvlhaci8yfvwk529ygbq3lnybhw5q5yg7mj9pwi2yd")))

