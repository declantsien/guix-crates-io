(define-module (crates-io ef i-) #:use-module (crates-io))

(define-public crate-efi-loadopt-0.1 (crate (name "efi-loadopt") (vers "0.1.0") (deps (list (crate-dep (name "byteorder") (req "^1.3") (default-features #t) (kind 0)) (crate-dep (name "failure") (req "^0.1") (default-features #t) (kind 0)))) (hash "0xp2q1w9z25c5ch0vaxikq7g1mipdfdigkryflrsnfhax1a2lim2")))

(define-public crate-efi-loadopt-0.2 (crate (name "efi-loadopt") (vers "0.2.0") (deps (list (crate-dep (name "byteorder") (req "^1.3") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1") (default-features #t) (kind 0)))) (hash "03kmcgm78vggrcmbwg4dbh7gpv6maz6m81sw1s2zysyphfn5zcgw")))

(define-public crate-efi-loadopt-0.2 (crate (name "efi-loadopt") (vers "0.2.1") (deps (list (crate-dep (name "byteorder") (req "^1.4") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1") (default-features #t) (kind 0)))) (hash "18zyj31xaq4bg6lh7vlirb062wwi3lgh85cfl08x0skz3pafdbnl")))

(define-public crate-efi-runner-0.1 (crate (name "efi-runner") (vers "0.1.0") (deps (list (crate-dep (name "tempfile") (req "^3.8.0") (default-features #t) (kind 0)) (crate-dep (name "uefi-run") (req "^0.6.1") (default-features #t) (kind 0)))) (hash "1km4qgc7m7kgxg3qvkniyry6g2lkv4xx2230vr7c4f5ilxzkqhpd")))

(define-public crate-efi-runner-0.1 (crate (name "efi-runner") (vers "0.1.1") (deps (list (crate-dep (name "tempfile") (req "^3.8.0") (default-features #t) (kind 0)) (crate-dep (name "uefi-run") (req "^0.6.1") (default-features #t) (kind 0)))) (hash "06d1qx8z5sjcc60g63nzzr5b0ajf3qvgrb0andsj9lp767hll5ch")))

