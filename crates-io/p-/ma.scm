(define-module (crates-io p- ma) #:use-module (crates-io))

(define-public crate-p-macro-0.1 (crate (name "p-macro") (vers "0.1.0") (hash "1hgj0psw0jvnnpkvr5r89qi2i4554aqf6gym7dfwgfwvhc0hbi79")))

(define-public crate-p-macro-0.2 (crate (name "p-macro") (vers "0.2.0") (hash "1jc1hlb3gb9jl6cg5s1xyl036iyf03cv4l8icj8f397w1wkfrrm2")))

