(define-module (crates-io p- ki) #:use-module (crates-io))

(define-public crate-p-kill-0.1 (crate (name "p-kill") (vers "0.1.0") (hash "0s61xzzpnxhira6pf3kxp109xgck0g05vbw6vky6pn5sj5dyngzb")))

(define-public crate-p-kill-0.1 (crate (name "p-kill") (vers "0.1.1") (hash "1aray4b2smzsmmdkfnw9fclhy9vfbv0al55wn48rsyk42il2n9mj")))

(define-public crate-p-kill-0.1 (crate (name "p-kill") (vers "0.1.2") (hash "0aahp5281amal7bdajyppy1yr3ifxkpqdyg05alsr8q2h5i8rmhj")))

(define-public crate-p-kill-0.1 (crate (name "p-kill") (vers "0.1.4") (hash "06wpywa2n11clvm7xwpnx701xz1mvsgc9p2j26ix0hh0dzsz589r")))

(define-public crate-p-kill-0.1 (crate (name "p-kill") (vers "0.1.5") (hash "1xbxygxh75cqbh1hm4lgq4d38rfpnqnlih4hw30zx2kdlhbhh759")))

(define-public crate-p-kill-0.2 (crate (name "p-kill") (vers "0.2.0") (hash "1d9f2s7ci07x91ls2hh6xppzx481anlkxp058fpkg5yyrqi5apad")))

