(define-module (crates-io kx cj) #:use-module (crates-io))

(define-public crate-kxcj9-0.1 (crate (name "kxcj9") (vers "0.1.0") (deps (list (crate-dep (name "embedded-hal") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "embedded-hal-mock") (req "^0.4") (default-features #t) (kind 2)) (crate-dep (name "linux-embedded-hal") (req "^0.2") (default-features #t) (kind 2)) (crate-dep (name "nb") (req "^0.1") (default-features #t) (kind 0)))) (hash "1yxkzl2vjnkk9352jw95ic964c7j05nmajwrz3yb9qrn18kf56gn")))

(define-public crate-kxcj9-0.2 (crate (name "kxcj9") (vers "0.2.0") (deps (list (crate-dep (name "embedded-hal") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "embedded-hal-mock") (req "^0.4") (default-features #t) (kind 2)) (crate-dep (name "linux-embedded-hal") (req "^0.2") (default-features #t) (kind 2)) (crate-dep (name "nb") (req "^0.1") (default-features #t) (kind 0)))) (hash "11dr6l7n09yidl1aa6zzvidv8797038wzr23jgriarn5d8ywka2p")))

