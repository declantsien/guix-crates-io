(define-module (crates-io kx tj) #:use-module (crates-io))

(define-public crate-kxtj3-1057-0.1 (crate (name "kxtj3-1057") (vers "0.1.0") (hash "00b74zqvi3vch9fmiyjbs9kqyvkr2rqpvk1c9vd4yaa912kqahia")))

(define-public crate-kxtj3-1057-0.2 (crate (name "kxtj3-1057") (vers "0.2.0") (deps (list (crate-dep (name "accelerometer") (req "~0.12") (default-features #t) (kind 0)) (crate-dep (name "embedded-hal") (req "~0.2") (default-features #t) (kind 0)) (crate-dep (name "esp-idf-hal") (req "^0.39.3") (default-features #t) (kind 2)) (crate-dep (name "esp-idf-sys") (req "^0.31.11") (default-features #t) (kind 2)) (crate-dep (name "num_enum") (req "~0.5") (kind 0)))) (hash "08ii6q5mlm99b4329rxpn8air52n1598rx4vjmcqng08lawj89yg")))

(define-public crate-kxtj3-1057-0.3 (crate (name "kxtj3-1057") (vers "0.3.0") (deps (list (crate-dep (name "accelerometer") (req "~0.12") (default-features #t) (kind 0)) (crate-dep (name "embedded-hal") (req "^1.0.0") (default-features #t) (kind 0)) (crate-dep (name "esp-idf-svc") (req "^0.48.0") (kind 2)) (crate-dep (name "num_enum") (req "~0.5") (kind 0)))) (hash "17psv3gh0a62b1z4a3x34b4jwa8an4f2sz91956wf2lp7bqjy3yv")))

(define-public crate-kxtj3-1057-0.3 (crate (name "kxtj3-1057") (vers "0.3.1") (deps (list (crate-dep (name "accelerometer") (req "~0.12") (default-features #t) (kind 0)) (crate-dep (name "embedded-hal") (req "^1.0.0") (default-features #t) (kind 0)) (crate-dep (name "esp-idf-svc") (req "^0.48.0") (kind 2)) (crate-dep (name "num_enum") (req "~0.5") (kind 0)))) (hash "1g8x7wdcsvd1kp2hb5ibqz9fsfyi5srfh9nx7cq925k60xrqayl1")))

