(define-module (crates-io s- ma) #:use-module (crates-io))

(define-public crate-s-macro-0.1 (crate (name "s-macro") (vers "0.1.0") (deps (list (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (default-features #t) (kind 0)))) (hash "0jrdzyldpx1aiss4d8az6q0mi34f39y89vgqgiypxsk5y8l5y58s")))

