(define-module (crates-io s- se) #:use-module (crates-io))

(define-public crate-s-secp256k1-0.5 (crate (name "s-secp256k1") (vers "0.5.7") (deps (list (crate-dep (name "arrayvec") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "cc") (req "^1.0") (default-features #t) (kind 1)) (crate-dep (name "cfg-if") (req "^0.1") (default-features #t) (kind 1)) (crate-dep (name "clippy") (req "^0.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "hex") (req "^0.3.1") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.4") (default-features #t) (kind 0)))) (hash "1gz8j3lmnqvzkrb9jqy9y6gd6ii42xikd2d9xzc8vsjzqhl0dbps") (features (quote (("unstable") ("dev" "clippy") ("default"))))))

