(define-module (crates-io s- ro) #:use-module (crates-io))

(define-public crate-s-rocksdb-0.5 (crate (name "s-rocksdb") (vers "0.5.0") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "local-encoding") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "s-rocksdb-sys") (req "^0.5") (default-features #t) (kind 0)))) (hash "00nr77n2y2dbxbz8zaaif12ahj855gzvw4p1j3d5m0qar0brm9m0") (features (quote (("valgrind") ("default"))))))

(define-public crate-s-rocksdb-sys-0.5 (crate (name "s-rocksdb-sys") (vers "0.5.5") (deps (list (crate-dep (name "cmake") (req "^0.1") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "local-encoding") (req "^0.2.0") (default-features #t) (kind 0)))) (hash "1vzwbv3ahm9qxllzqglcz1wccncjvybbj6k8xnfw9z7k4vsb8yvf") (features (quote (("default")))) (links "rocksdb")))

