(define-module (crates-io s- ex) #:use-module (crates-io))

(define-public crate-s-exp-0.1 (crate (name "s-exp") (vers "0.1.0") (deps (list (crate-dep (name "alt-std") (req "^0.2.9") (default-features #t) (kind 0)))) (hash "0f2y8rmyyq8p0wzgcaxrclz2gyjpmxh747vprb06dcmp6qjlyw37")))

(define-public crate-s-expr-0.1 (crate (name "s-expr") (vers "0.1.0") (deps (list (crate-dep (name "unicode-xid") (req "^0.2") (optional #t) (default-features #t) (kind 0)))) (hash "0al90akdh4i3likcj73a8hbsd4vmicbqq9gxa45rahaf5xk3078r") (features (quote (("unicode" "unicode-xid") ("default" "unicode"))))))

(define-public crate-s-expr-0.1 (crate (name "s-expr") (vers "0.1.1") (deps (list (crate-dep (name "unicode-xid") (req "^0.2") (optional #t) (default-features #t) (kind 0)))) (hash "1x1rs3nfjy31qc3aaj5n2r40ak2phh8v2qgfb8xjj34i3g9j98kk") (features (quote (("unicode" "unicode-xid") ("default" "unicode"))))))

(define-public crate-s-expression-0.1 (crate (name "s-expression") (vers "0.1.0") (hash "01qlbsxf7b7cdjxy84c4b6rq1a52xrzyqv3nxfgqfkc4f51dkcp0")))

