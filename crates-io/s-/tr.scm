(define-module (crates-io s- tr) #:use-module (crates-io))

(define-public crate-s-tree-0.1 (crate (name "s-tree") (vers "0.1.0") (deps (list (crate-dep (name "criterion") (req "^0.3.1") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.7.3") (default-features #t) (kind 2)))) (hash "1wam2kr3512npj24g2b6g0ym844y3h7bhpfnz0znfy8znwpz1kww")))

(define-public crate-s-tree-0.3 (crate (name "s-tree") (vers "0.3.0") (deps (list (crate-dep (name "criterion") (req "^0.3.1") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.7.3") (default-features #t) (kind 2)))) (hash "1cw18cx81ahqz6vi2wnj7lscq7y6mq85iazlxxiqddhp4n1zyaj7")))

(define-public crate-s-trie-standardmap-0.1 (crate (name "s-trie-standardmap") (vers "0.1.1") (deps (list (crate-dep (name "s-types") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "srlp") (req "^0.2.4") (default-features #t) (kind 0)))) (hash "0pn6dxqhki279c59394pqcwzmh7y22w37wr833z0fqd43sj7lvs3")))

