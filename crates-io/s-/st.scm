(define-module (crates-io s- st) #:use-module (crates-io))

(define-public crate-s-structured-log-0.1 (crate (name "s-structured-log") (vers "0.1.0") (deps (list (crate-dep (name "log") (req "^0.3.6") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^0.7.11") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^0.7.3") (default-features #t) (kind 0)))) (hash "1694ycd5wd9plc533ydsa4k6y0c730mg0ybkkak8rqk4q49g6672") (features (quote (("default"))))))

(define-public crate-s-structured-log-0.2 (crate (name "s-structured-log") (vers "0.2.0") (deps (list (crate-dep (name "log") (req "^0.3.6") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^0.8.0") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^0.8.0") (default-features #t) (kind 0)))) (hash "1qvxsgwghivqz7rad3z8vdajyxf05vm5j6yrspmskya33zz74gg0") (features (quote (("default"))))))

