(define-module (crates-io kc on) #:use-module (crates-io))

(define-public crate-kconfig-0.1 (crate (name "kconfig") (vers "0.1.0") (deps (list (crate-dep (name "bitflags") (req "^1.3") (default-features #t) (kind 0)) (crate-dep (name "cpp") (req "^0.5") (default-features #t) (kind 0)) (crate-dep (name "cpp_build") (req "^0.5") (default-features #t) (kind 1)) (crate-dep (name "env_logger") (req "^0.9") (default-features #t) (kind 2)) (crate-dep (name "kconfig_impl") (req "^0.0.1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "kde_frameworks") (req "^0.2") (default-features #t) (kind 1)) (crate-dep (name "lazy_static") (req "^1.4") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 2)) (crate-dep (name "qmetaobject") (req "^0.2.7") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "qmetaobject") (req "^0.2.7") (default-features #t) (kind 2)) (crate-dep (name "qttypes") (req "^0.2.7") (default-features #t) (kind 0)) (crate-dep (name "qttypes") (req "^0.2.7") (features (quote ("qtquick"))) (default-features #t) (kind 2)) (crate-dep (name "semver") (req "^1.0") (default-features #t) (kind 1)))) (hash "15gyp0l13pm2myywhqmp9ahamk7a5mdr51iwcki5jb5dxfwcpg7z") (features (quote (("kconfigxt" "kconfig_impl") ("all" "kconfig_impl" "qmetaobject")))) (links "KF5Config")))

(define-public crate-kconfig-linux-0.1 (crate (name "kconfig-linux") (vers "0.1.0") (deps (list (crate-dep (name "anyhow") (req "^1.0.56") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.5.5") (features (quote ("pattern"))) (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.30") (default-features #t) (kind 0)) (crate-dep (name "uuid") (req "^0.8.2") (features (quote ("v4"))) (default-features #t) (kind 0)))) (hash "0liprj1nfsvfln5lf109lycbmixs0j7z727627ldqxhh09vwwdf4")))

(define-public crate-kconfig-linux-0.1 (crate (name "kconfig-linux") (vers "0.1.1") (deps (list (crate-dep (name "anyhow") (req "^1.0.56") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.5.5") (features (quote ("pattern"))) (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.30") (default-features #t) (kind 0)) (crate-dep (name "uuid") (req "^0.8.2") (features (quote ("v4"))) (default-features #t) (kind 0)))) (hash "0l1zjhqzpbzfsclrnmy28s49djfazqafga3hzjdz2hdn17i87wr8")))

(define-public crate-kconfig-parser-0.0.1 (crate (name "kconfig-parser") (vers "0.0.1") (hash "0cld5hy2mvdpwkr8vwgnzxij6l4djw0acwb3vs8li1ix8jjdqyf9")))

(define-public crate-kconfig-parser-0.0.2 (crate (name "kconfig-parser") (vers "0.0.2") (hash "04zl824hac0gd7y0mv9ld92fjg20b241ni66d25swiljlq6073r2")))

(define-public crate-kconfig-parser-0.0.3 (crate (name "kconfig-parser") (vers "0.0.3") (hash "05bjh3cqxw8v7r1yv2zcv7dkqrdd00i1rrmk4zfy7fflrfaq8rll")))

(define-public crate-kconfig-parser-0.0.4 (crate (name "kconfig-parser") (vers "0.0.4") (hash "1wc1m8qr66cm3nrn7nnmzxlh7i1wv1gy51mxkd7q9zwq9wkyqjjw")))

(define-public crate-kconfig-parser-0.0.5 (crate (name "kconfig-parser") (vers "0.0.5") (hash "12x76wmz3y7ca08215n2jj8h94a0z13cspk11h84kpbs42v0hwm0")))

(define-public crate-kconfig-parser-0.1 (crate (name "kconfig-parser") (vers "0.1.0") (hash "1r6psccdqv2flcb4n0wfnadx2kzzdamxqbwdjj5cswd834mfchan")))

(define-public crate-kconfig-parser-0.1 (crate (name "kconfig-parser") (vers "0.1.1") (hash "1amp7p8cw0cb9wvvw1bzavsw1anjf9w54bi99cs30zs7vi759mcf")))

(define-public crate-kconfig-represent-0.0.1 (crate (name "kconfig-represent") (vers "0.0.1") (deps (list (crate-dep (name "kconfig-parser") (req ">=0.0.1") (default-features #t) (kind 0)))) (hash "16ljvp3hr3k3nqfbvsr6m0ld3qb49prdpaafv9dgmv8cwf1qkpw6")))

(define-public crate-kconfig-represent-0.0.2 (crate (name "kconfig-represent") (vers "0.0.2") (deps (list (crate-dep (name "kconfig-parser") (req ">=0.0.2") (default-features #t) (kind 0)))) (hash "17z55cdq6pk6vcavdj2cwc5k9wlnpcpig5ck9na450497c0yg6fc")))

(define-public crate-kconfig-represent-0.0.3 (crate (name "kconfig-represent") (vers "0.0.3") (deps (list (crate-dep (name "kconfig-parser") (req ">=0.0.2") (default-features #t) (kind 0)))) (hash "1if6fw6znmdvjblx6yan3322kl3xw6fmz7sziih59cipzlv1fgxy")))

(define-public crate-kconfig-represent-0.0.4 (crate (name "kconfig-represent") (vers "0.0.4") (deps (list (crate-dep (name "kconfig-parser") (req ">=0.0.3") (default-features #t) (kind 0)))) (hash "1vjbzggywjihqc7358v3pwv4809kff89k2y6mqyx42z8n0hq0hq9")))

(define-public crate-kconfig-represent-0.0.5 (crate (name "kconfig-represent") (vers "0.0.5") (deps (list (crate-dep (name "kconfig-parser") (req ">=0.0.3") (default-features #t) (kind 0)))) (hash "1jpdmc35f4m2bzvbisjhxjb9cxrcly2a60w8qy75b29z4z5gj9ww")))

(define-public crate-kconfig-represent-0.0.6 (crate (name "kconfig-represent") (vers "0.0.6") (deps (list (crate-dep (name "kconfig-parser") (req ">=0.0.3") (default-features #t) (kind 0)))) (hash "1l9j0wn7fdvj4n37fxxbijq3vqqr49h1mkzqj6c9bwzbn22j3z89")))

(define-public crate-kconfig-represent-0.0.7 (crate (name "kconfig-represent") (vers "0.0.7") (deps (list (crate-dep (name "kconfig-parser") (req ">=0.0.3") (default-features #t) (kind 0)))) (hash "0fravw8xi3s665x6x3q5xp9d1rppfy5mczvra8f11ddlwd3x44vm")))

(define-public crate-kconfig-represent-0.0.8 (crate (name "kconfig-represent") (vers "0.0.8") (deps (list (crate-dep (name "kconfig-parser") (req ">=0.0.5") (default-features #t) (kind 0)))) (hash "0s6c235fhk6byya0a2fwnrbcybapkw7mayizp4263qhn5wfmivs6")))

(define-public crate-kconfig-represent-0.1 (crate (name "kconfig-represent") (vers "0.1.0") (deps (list (crate-dep (name "kconfig-parser") (req ">=0.1.0") (default-features #t) (kind 0)))) (hash "01w19adi9ml0b69zxf7i487n279mq5x00kz1nk020njv4zmrrfbj")))

(define-public crate-kconfig-represent-0.1 (crate (name "kconfig-represent") (vers "0.1.1") (deps (list (crate-dep (name "kconfig-parser") (req ">=0.1.1") (default-features #t) (kind 0)))) (hash "003vdf5179wy6hklcbbsv85id4ng9i0wwagjbqr9hlq3d5dsfpjp")))

(define-public crate-kconfig_compiler-0.0.1 (crate (name "kconfig_compiler") (vers "0.0.1") (deps (list (crate-dep (name "heck") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "roxmltree") (req "^0.14") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "0s2f1gyh1wh9kand2x54scf7dg3fn0ps61yql37rlf151zmnxdbg")))

(define-public crate-kconfig_impl-0.0.1 (crate (name "kconfig_impl") (vers "0.0.1") (deps (list (crate-dep (name "kconfig_compiler") (req "^0.0.1") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (default-features #t) (kind 0)))) (hash "1v30mvzykw9kzvsn5gyx2blh2vh9dssbqap86xaak0f7xky0p5y9")))

