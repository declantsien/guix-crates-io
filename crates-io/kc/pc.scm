(define-module (crates-io kc pc) #:use-module (crates-io))

(define-public crate-kcpclient-0.1 (crate (name "kcpclient") (vers "0.1.0") (deps (list (crate-dep (name "bytes") (req "^0.5") (default-features #t) (kind 0)) (crate-dep (name "chrono") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)))) (hash "02hdky2nn81zwg7176497jcw7qbisl88p210kl6bwpd8i3zmaw2m")))

(define-public crate-kcpclient-0.1 (crate (name "kcpclient") (vers "0.1.1") (deps (list (crate-dep (name "bytes") (req "^0.5") (default-features #t) (kind 0)) (crate-dep (name "chrono") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)))) (hash "1h7pwfvf8ydx45qm6jgabjmq53w9q7263d2mxhzcwpn93746x35d")))

