(define-module (crates-io kc at) #:use-module (crates-io))

(define-public crate-kcat-0.1 (crate (name "kcat") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^2.3") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syntect") (req "^2.0") (default-features #t) (kind 0)))) (hash "1vxl531kyp8xicfhl8mwv0dblkycc0jvq6l97padnrwr5hl8aa3x")))

(define-public crate-kcat-0.1 (crate (name "kcat") (vers "0.1.1") (deps (list (crate-dep (name "clap") (req "^2.3") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syntect") (req "^2.0") (default-features #t) (kind 0)))) (hash "1k2hdyxi3x77z484l6z1kwkylam645ny800mvcrf8x7jhg86hwga")))

(define-public crate-kcat-0.1 (crate (name "kcat") (vers "0.1.2") (deps (list (crate-dep (name "clap") (req "^2.3") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syntect") (req "^2.0") (default-features #t) (kind 0)))) (hash "1k20sndbm0ih6v8w0pi77z2md1blhl6arwy5diwn9sgn4cmkdrqp")))

(define-public crate-kcat-0.1 (crate (name "kcat") (vers "0.1.3") (deps (list (crate-dep (name "clap") (req "^2.3") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syntect") (req "^2.0") (default-features #t) (kind 0)))) (hash "13hh3xy69biasdwhwlmlmrs8xrk7akgf0z7m04k0ikb6dgnn0dq6")))

(define-public crate-kcat-0.1 (crate (name "kcat") (vers "0.1.4") (deps (list (crate-dep (name "clap") (req "^2.3") (default-features #t) (kind 0)) (crate-dep (name "failure") (req "^0.1.1") (default-features #t) (kind 0)) (crate-dep (name "failure_derive") (req "^0.1.1") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syntect") (req "^2.0") (default-features #t) (kind 0)))) (hash "11xc48wij4w5z71s70ibpsxkqy58mwm5ydyqcwfhmm81iv1hn21y")))

(define-public crate-kcat-0.1 (crate (name "kcat") (vers "0.1.5") (deps (list (crate-dep (name "clap") (req "^2.3") (default-features #t) (kind 0)) (crate-dep (name "failure") (req "^0.1.1") (default-features #t) (kind 0)) (crate-dep (name "failure_derive") (req "^0.1.1") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syntect") (req "^2.0") (default-features #t) (kind 0)))) (hash "0vsmr56fwx98ndhyf5i91dvwz4yx7d8cx4xnrb7bakvliqjb2g3k")))

(define-public crate-kcat-0.1 (crate (name "kcat") (vers "0.1.6") (deps (list (crate-dep (name "clap") (req "^2.3") (default-features #t) (kind 0)) (crate-dep (name "failure") (req "^0.1.1") (default-features #t) (kind 0)) (crate-dep (name "failure_derive") (req "^0.1.1") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syntect") (req "^2.0") (default-features #t) (kind 0)))) (hash "1gllainahrn0iz1x5jmclm4wv75x9wab62b3j8dmmcnrvmxmfrfn")))

