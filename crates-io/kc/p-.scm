(define-module (crates-io kc p-) #:use-module (crates-io))

(define-public crate-kcp-rust-native-0.1 (crate (name "kcp-rust-native") (vers "0.1.0") (deps (list (crate-dep (name "bindgen") (req "^0.53.1") (default-features #t) (kind 1)))) (hash "1sj0wz883n02rsl65ld1bnd96myfzjv7k8p2n8nhm449yr6diap9")))

