(define-module (crates-io kc ov) #:use-module (crates-io))

(define-public crate-kcov-coverage-fix-0.1 (crate (name "kcov-coverage-fix") (vers "0.1.0") (deps (list (crate-dep (name "quick-xml") (req "^0.17.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.3.1") (features (quote ("std" "unicode-perl"))) (kind 0)))) (hash "0x52ssaradcmdsqzyiffzxx7vrpkpp9xmmhcfxn7jaapkyhyw415") (yanked #t)))

