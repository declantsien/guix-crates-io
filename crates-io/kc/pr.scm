(define-module (crates-io kc pr) #:use-module (crates-io))

(define-public crate-kcprs-0.4 (crate (name "kcprs") (vers "0.4.12") (deps (list (crate-dep (name "bytes") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "env_logger") (req "^0.8") (default-features #t) (kind 2)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8") (default-features #t) (kind 2)) (crate-dep (name "time") (req "^0.2") (default-features #t) (kind 2)))) (hash "0q14zl9ypki8f9wpqk5nvrz0z3fi2z488f7bww6acw7kbwny0w0a")))

(define-public crate-kcprs-0.4 (crate (name "kcprs") (vers "0.4.13") (deps (list (crate-dep (name "bytes") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "env_logger") (req "^0.8") (default-features #t) (kind 2)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8") (default-features #t) (kind 2)) (crate-dep (name "time") (req "^0.2") (default-features #t) (kind 2)))) (hash "03pjvpfxhp5yjdfgr915khnwj9w0xxzr8kvksids9q7qhpsmv2wc")))

(define-public crate-kcprs-0.5 (crate (name "kcprs") (vers "0.5.0") (deps (list (crate-dep (name "bytes") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "env_logger") (req "^0.8") (default-features #t) (kind 2)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8") (default-features #t) (kind 2)) (crate-dep (name "time") (req "^0.2") (default-features #t) (kind 2)))) (hash "1fg6fzb83znln22fvri48ws2rrzksb069c60vbivycmyc4vwrj11")))

