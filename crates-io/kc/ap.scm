(define-module (crates-io kc ap) #:use-module (crates-io))

(define-public crate-kcapi-0.1 (crate (name "kcapi") (vers "0.1.0") (deps (list (crate-dep (name "kcapi-sys") (req "^1.1.0") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2.107") (default-features #t) (kind 0)))) (hash "06nkxij5p2snk4lnf3476q93dx1klik5vqx9vhgwv30prf90wqy5")))

(define-public crate-kcapi-0.1 (crate (name "kcapi") (vers "0.1.1") (deps (list (crate-dep (name "kcapi-sys") (req "^1.1.0") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2.107") (default-features #t) (kind 0)))) (hash "0kpp54w13acf33kpj20ms820i7z03234vbs50f3b9prbmj0p3bzc")))

(define-public crate-kcapi-0.1 (crate (name "kcapi") (vers "0.1.2") (deps (list (crate-dep (name "kcapi-sys") (req "^1.1.0") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2.107") (default-features #t) (kind 0)))) (hash "0c0ipqc868absid4ivqw6pqn3pavx221ajy5ch1rqbcz2fkhi8ns")))

(define-public crate-kcapi-0.1 (crate (name "kcapi") (vers "0.1.3") (deps (list (crate-dep (name "kcapi-sys") (req "^1.1.0") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2.107") (default-features #t) (kind 0)))) (hash "09wlj7m4gxsmxn53iy66wg9ajir3ia03rwqx62wcamqjasgg5ah3")))

(define-public crate-kcapi-0.1 (crate (name "kcapi") (vers "0.1.4") (deps (list (crate-dep (name "kcapi-sys") (req "^1.1.0") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2.107") (default-features #t) (kind 0)))) (hash "07jfm6qj2d607y9w4cipmmy9mn4jhszlg4vfdjrsczr5mwwr24zq")))

(define-public crate-kcapi-0.1 (crate (name "kcapi") (vers "0.1.5") (deps (list (crate-dep (name "kcapi-sys") (req "^1.3.0") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2.107") (default-features #t) (kind 0)))) (hash "06ahwwyasd7iskwhnag89f9f8kad79bmyh9i6glc70ic5c0x7xc1")))

(define-public crate-kcapi-0.1 (crate (name "kcapi") (vers "0.1.6") (deps (list (crate-dep (name "kcapi-sys") (req "^1.4.1") (kind 0)) (crate-dep (name "libc") (req "^0.2.107") (default-features #t) (kind 0)))) (hash "15pkhb80pajsmpj9f1rmp2whdns92rsr07gsizqsp3zs40xr4455") (features (quote (("vendored-kcapi" "kcapi-sys/vendored-kcapi") ("local-kcapi" "kcapi-sys/local-kcapi") ("default" "vendored-kcapi" "asym") ("asym"))))))

(define-public crate-kcapi-sys-0.0.1 (crate (name "kcapi-sys") (vers "0.0.1") (hash "0qijb8aqsah01lq2grbpln9vr0ar1h8cmbvpm00q9fqy32s41gh7")))

(define-public crate-kcapi-sys-0.0.2 (crate (name "kcapi-sys") (vers "0.0.2") (hash "1rsri08y3cs1zva8aw9mlam7jlrfg3wh7xpkl2bnx2ip4qm2jscm")))

(define-public crate-kcapi-sys-0.0.3 (crate (name "kcapi-sys") (vers "0.0.3") (hash "0idiah13h7s2cd4f2n4fqh37hacg4hr2icz2n3jxsd9m64cir171")))

(define-public crate-kcapi-sys-1 (crate (name "kcapi-sys") (vers "1.1.0") (deps (list (crate-dep (name "autotools") (req "^0.2") (default-features #t) (kind 1)) (crate-dep (name "bindgen") (req "^0.53") (default-features #t) (kind 1)) (crate-dep (name "fs_extra") (req "^1.2.0") (default-features #t) (kind 1)))) (hash "0gyzz827wfj8gkmjmwk612agmpx85gh0p4lgmbj9qmncycv0g3xb")))

(define-public crate-kcapi-sys-1 (crate (name "kcapi-sys") (vers "1.2.0") (deps (list (crate-dep (name "autotools") (req "^0.2") (default-features #t) (kind 1)) (crate-dep (name "bindgen") (req "^0.53") (default-features #t) (kind 1)) (crate-dep (name "fs_extra") (req "^1.2.0") (default-features #t) (kind 1)))) (hash "01lyffi20f1f447s43gl8rr5rh16gbcgcvpai316c0j98rwx42zy")))

(define-public crate-kcapi-sys-1 (crate (name "kcapi-sys") (vers "1.3.0") (deps (list (crate-dep (name "autotools") (req "^0.2") (default-features #t) (kind 1)) (crate-dep (name "bindgen") (req "^0.53") (default-features #t) (kind 1)) (crate-dep (name "fs_extra") (req "^1.2.0") (default-features #t) (kind 1)))) (hash "06qs2n22jr1fm18kq8cjg43cwsgyrnd22s4fp4m89xhrg0sz67kk")))

(define-public crate-kcapi-sys-1 (crate (name "kcapi-sys") (vers "1.4.0") (deps (list (crate-dep (name "autotools") (req "^0.2") (default-features #t) (kind 1)) (crate-dep (name "bindgen") (req "^0.53") (default-features #t) (kind 1)) (crate-dep (name "fs_extra") (req "^1.2.0") (default-features #t) (kind 1)))) (hash "0qa97jmqzagpc5rdvq4pvp4rvvjmzz2rlz8ax2w21fjvj2aa1zwy") (features (quote (("vendored-kcapi") ("local-kcapi") ("default" "vendored-kcapi"))))))

(define-public crate-kcapi-sys-1 (crate (name "kcapi-sys") (vers "1.4.1") (deps (list (crate-dep (name "autotools") (req "^0.2") (default-features #t) (kind 1)) (crate-dep (name "bindgen") (req "^0.53") (default-features #t) (kind 1)) (crate-dep (name "fs_extra") (req "^1.2.0") (default-features #t) (kind 1)))) (hash "0xxszvdd1kjmfa9j18cgscq7v71zxr6fyj1d77hw81jf997faicg") (features (quote (("vendored-kcapi") ("local-kcapi") ("default" "vendored-kcapi"))))))

