(define-module (crates-io kc tf) #:use-module (crates-io))

(define-public crate-kctf-pow-1 (crate (name "kctf-pow") (vers "1.0.0") (deps (list (crate-dep (name "base64") (req "^0.13.0") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.4") (default-features #t) (kind 0)) (crate-dep (name "rug") (req "^1.12.0") (default-features #t) (kind 0)))) (hash "1pf2qkf0xm3x6s7b3dp3s1smnx07ss38blahcmn6ac2sy12zjn7c")))

(define-public crate-kctf-pow-1 (crate (name "kctf-pow") (vers "1.1.0") (deps (list (crate-dep (name "base64") (req "^0.13.0") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.4") (default-features #t) (kind 0)) (crate-dep (name "rug") (req "^1.12.0") (default-features #t) (kind 0)))) (hash "12akr3zvkspqzqgbyjrdjh2vjbfsblqjqi9c3k5kr8zj7jcgzzlk")))

(define-public crate-kctf-pow-1 (crate (name "kctf-pow") (vers "1.1.1") (deps (list (crate-dep (name "base64") (req "^0.13.0") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.4") (default-features #t) (kind 0)) (crate-dep (name "rug") (req "^1.12.0") (default-features #t) (kind 0)))) (hash "10cmzkabv18949lmv0y0r027wxq324xy7f50dzsgynxl31mfg22l")))

(define-public crate-kctf-pow-1 (crate (name "kctf-pow") (vers "1.1.2") (deps (list (crate-dep (name "base64") (req "^0.13.0") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.4") (default-features #t) (kind 0)) (crate-dep (name "rug") (req "^1.12.0") (default-features #t) (kind 0)))) (hash "11slqcpy6da779h3jd4jpadnrsr6k9nacfn6fb2wfkwmlcghachq")))

(define-public crate-kctf-pow-1 (crate (name "kctf-pow") (vers "1.2.0") (deps (list (crate-dep (name "base64") (req "^0.13.0") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.4") (default-features #t) (kind 0)) (crate-dep (name "rug") (req "^1.12.0") (default-features #t) (kind 0)))) (hash "15596ndhkk3dmmj3rf68cswhwpsxscbj0kxy93jq4frsprbdk9qi")))

