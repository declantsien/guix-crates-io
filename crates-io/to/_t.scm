(define-module (crates-io to _t) #:use-module (crates-io))

(define-public crate-to_table-0.1 (crate (name "to_table") (vers "0.1.0") (deps (list (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2.0.52") (default-features #t) (kind 0)))) (hash "02gryn7a87gky3xl8ly05m6gfrz7cbwfz6sshckd2af4c071avc0")))

(define-public crate-to_that-1 (crate (name "to_that") (vers "1.0.0") (hash "0sb1swk5qk946mrqna540l0kqi75phf2gzxxd7j8ayl015pqr6r8")))

(define-public crate-to_that-1 (crate (name "to_that") (vers "1.0.1") (hash "1h43m1d80snxg0mag5wirvkafkwkapci33iyjv72rjk164xvaa0b")))

(define-public crate-to_tokenstream-0.1 (crate (name "to_tokenstream") (vers "0.1.0") (deps (list (crate-dep (name "proc-macro2") (req "^1.0.69") (default-features #t) (kind 0)))) (hash "02sklyzaf6kp8pf4l60ga5sq3lwc0ryhg1f1n1an1ix0y5bh0vbr")))

(define-public crate-to_tokenstream-0.1 (crate (name "to_tokenstream") (vers "0.1.1") (deps (list (crate-dep (name "proc-macro2") (req "^1.0.69") (default-features #t) (kind 0)))) (hash "0pwag2nk7bkbf19icgbbj4b0v72rg3q9l8ihldrpxfk3yckbj7jf")))

(define-public crate-to_tokenstream-0.1 (crate (name "to_tokenstream") (vers "0.1.2") (deps (list (crate-dep (name "proc-macro2") (req "^1.0.69") (default-features #t) (kind 0)))) (hash "0s7hywv8j3lr05cg32qijv1iibbr8xsglgzcvwsxi9kkgdflb1ng")))

(define-public crate-to_tokenstream-0.1 (crate (name "to_tokenstream") (vers "0.1.3") (deps (list (crate-dep (name "proc-macro2") (req "^1.0.69") (default-features #t) (kind 0)))) (hash "1jppcssxgqbd697i136s7k7jibxysd89gk9lqgz70v4a89hidb95")))

(define-public crate-to_trait-0.1 (crate (name "to_trait") (vers "0.1.0") (hash "0xkcnxb9rqn2inf4xwalbbkn1id4z27ax2fx11291bz3jyp003gr")))

(define-public crate-to_trait-0.1 (crate (name "to_trait") (vers "0.1.1") (hash "05jv5m22m5176mvcrfc0g40gb5lyxcq80syh7d0qv2y6icwhn9l1")))

