(define-module (crates-io to _s) #:use-module (crates-io))

(define-public crate-to_shmem-0.0.0 (crate (name "to_shmem") (vers "0.0.0") (hash "12k69c7zknzmza1mkqpmd86cialbsvazz7zr4hscm0dnfzy2bvmi")))

(define-public crate-to_shmem_derive-0.0.0 (crate (name "to_shmem_derive") (vers "0.0.0") (hash "0z4zc81p0i5fpkil2v2jq54nmilasvr0v25q7jbv6w0nrpl5qw2b")))

(define-public crate-to_snake_case-0.1 (crate (name "to_snake_case") (vers "0.1.0") (hash "1wdqf2n567x13hn1hdv6gs3scv6i5br67a3345lw8vbv2rah29ij")))

(define-public crate-to_snake_case-0.1 (crate (name "to_snake_case") (vers "0.1.1") (hash "1bc7x87v8v7sd2c8rbaqnj3g2hpdk0236wpjq0g6ywb7ybc360l9")))

(define-public crate-to_sql_condition-0.1 (crate (name "to_sql_condition") (vers "0.1.0") (deps (list (crate-dep (name "proc-macro2") (req "^1.0.51") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.23") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.109") (features (quote ("extra-traits"))) (default-features #t) (kind 0)))) (hash "18hmpd2i5sqfzqvbinl1ps3h1y98miayv9f19nix65pvh4y94pi7") (yanked #t)))

(define-public crate-to_sql_condition-0.1 (crate (name "to_sql_condition") (vers "0.1.1") (deps (list (crate-dep (name "proc-macro2") (req "^1.0.51") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.23") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.109") (features (quote ("extra-traits"))) (default-features #t) (kind 0)))) (hash "0zbwnd75nqnxy0l20l5awk82z64mlsxqdwvj2dzy95cl28sk91xs")))

(define-public crate-to_sql_condition-0.1 (crate (name "to_sql_condition") (vers "0.1.2") (deps (list (crate-dep (name "proc-macro2") (req "^1.0.51") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.23") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.109") (features (quote ("extra-traits"))) (default-features #t) (kind 0)))) (hash "1h3w4l9cas3k9bsw5hvl5kd0rkwhrzgbz6906sn02ppqgj9maakf")))

(define-public crate-to_sql_condition-0.1 (crate (name "to_sql_condition") (vers "0.1.3") (deps (list (crate-dep (name "proc-macro2") (req "^1.0.51") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.23") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.109") (features (quote ("extra-traits"))) (default-features #t) (kind 0)))) (hash "12picgi7g315s03pjnfm40rl8gyh2gbxpyl7sd895d19r15j25r3")))

(define-public crate-to_sql_condition-0.1 (crate (name "to_sql_condition") (vers "0.1.4") (deps (list (crate-dep (name "proc-macro2") (req "^1.0.51") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.23") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.109") (features (quote ("extra-traits"))) (default-features #t) (kind 0)))) (hash "1mwv5bs9rg4z3l9rh1cbng74y1sxjf4as5pl3hbbvzz4n5xlq7d7")))

(define-public crate-to_sql_condition-0.2 (crate (name "to_sql_condition") (vers "0.2.1") (deps (list (crate-dep (name "proc-macro2") (req "^1.0.51") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.23") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.109") (features (quote ("extra-traits"))) (default-features #t) (kind 0)))) (hash "1m4mq07vflxi3dn6r9231ral5p4fgq3sfccvj3jsl3v20myiiqdr")))

(define-public crate-to_string-0.1 (crate (name "to_string") (vers "0.1.0") (hash "0q8rnamzbyli4g0a3ih55fxsd2qsjwmq8i3rdyl5877m6d2bmh8m")))

