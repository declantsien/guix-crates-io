(define-module (crates-io to tu) #:use-module (crates-io))

(define-public crate-totum-revolutum-0.2 (crate (name "totum-revolutum") (vers "0.2.2") (hash "0ggc6fizly4bcqy0d4rwp79vrfgl636r9ca8563j6idhlcdwbjba")))

(define-public crate-totum-revolutum-0.2 (crate (name "totum-revolutum") (vers "0.2.3") (hash "1bhl0061cjqhkn20pqsf7fmphxwch8jhkwzhx4y94vk3xi3liq5a")))

(define-public crate-totum-revolutum-0.2 (crate (name "totum-revolutum") (vers "0.2.4") (hash "1ywkydyra8j509ampljsmg0bh9m9h1in9ygwqh8cqmx79jc5x16g") (rust-version "1.56.1")))

(define-public crate-totum-revolutum-0.2 (crate (name "totum-revolutum") (vers "0.2.5") (hash "190nnwnbq86fvnpmx3l6z3f90lqf4z4pbd69b21pk2pnrx3fnkw4") (rust-version "1.56.1")))

