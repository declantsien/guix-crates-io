(define-module (crates-io to ff) #:use-module (crates-io))

(define-public crate-toffee-0.1 (crate (name "toffee") (vers "0.1.0") (hash "1l4szy32fk6q6w0gh4m578636x86iy15ff2inyh5kcb40acdixmb")))

(define-public crate-toffee-0.2 (crate (name "toffee") (vers "0.2.0") (deps (list (crate-dep (name "anyhow") (req "^1.0.34") (default-features #t) (kind 0)) (crate-dep (name "argh") (req "^0.1.4") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.4.2") (default-features #t) (kind 0)))) (hash "0i68g7vlh6zcmmq5n9qpnf1asiy5wk1vgbmb38ji6cl1h8jpm9s0")))

(define-public crate-toffee-0.3 (crate (name "toffee") (vers "0.3.0") (deps (list (crate-dep (name "anyhow") (req "^1.0.34") (default-features #t) (kind 0)) (crate-dep (name "argh") (req "^0.1.4") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.4.2") (default-features #t) (kind 0)))) (hash "1h502yb4z9fp351c13mal7s4g0z5ny9v4wi0w2fa354lq0cwk5kc")))

(define-public crate-toffee-0.4 (crate (name "toffee") (vers "0.4.0") (deps (list (crate-dep (name "anyhow") (req "^1.0.34") (default-features #t) (kind 0)) (crate-dep (name "argh") (req "^0.1.4") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.4.2") (default-features #t) (kind 0)))) (hash "1sykaw3bqpgf0zij4m9d5jnxk9szkyqrww988083gx5ixx1xsmwl")))

(define-public crate-toffee-0.4 (crate (name "toffee") (vers "0.4.2") (deps (list (crate-dep (name "anyhow") (req "^1.0.34") (default-features #t) (kind 0)) (crate-dep (name "argh") (req "^0.1.4") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.4.2") (default-features #t) (kind 0)))) (hash "0blsa03crd8bkxhgac97fqig2kbl44vb73dsyrs5f89www74sq0d")))

(define-public crate-toffee-0.5 (crate (name "toffee") (vers "0.5.1") (deps (list (crate-dep (name "anyhow") (req "^1.0.34") (default-features #t) (kind 0)) (crate-dep (name "argh") (req "^0.1.4") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.4.2") (default-features #t) (kind 0)))) (hash "0h0lf0b4ms8xdqxpnvrfrcvnjljx82vrikjdsi816c6szmcdfphj")))

(define-public crate-toffee-0.5 (crate (name "toffee") (vers "0.5.2") (deps (list (crate-dep (name "anyhow") (req "^1.0.34") (default-features #t) (kind 0)) (crate-dep (name "argh") (req "^0.1.4") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.5.5") (default-features #t) (kind 0)))) (hash "06ajihbs1w13603pf2q5jwnkrid0q3mvvhba8c8c7p8dysdsrvkf")))

