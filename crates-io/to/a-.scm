(define-module (crates-io to a-) #:use-module (crates-io))

(define-public crate-toa-find-0.1 (crate (name "toa-find") (vers "0.1.0") (deps (list (crate-dep (name "regex") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "walkdir") (req "^1") (default-features #t) (kind 0)))) (hash "1v1zd8p7a45yddsryaxfcwmg6cqcs658b5hf0kf6mmsha4nm65g7")))

(define-public crate-toa-find-0.1 (crate (name "toa-find") (vers "0.1.1") (deps (list (crate-dep (name "regex") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "walkdir") (req "^1") (default-features #t) (kind 0)))) (hash "0p05j79a9i8j3ncizpgds92wvd818rx8205spmr341d7xf1clpwp")))

(define-public crate-toa-ping-0.0.1 (crate (name "toa-ping") (vers "0.0.1") (deps (list (crate-dep (name "lazy-socket") (req "^0.1") (default-features #t) (kind 0)))) (hash "1fkdc84fs6am5pi2w92qlzyg5sgga42xnbiac81hyigcb0ijdc6g")))

(define-public crate-toa-ping-0.0.2 (crate (name "toa-ping") (vers "0.0.2") (deps (list (crate-dep (name "lazy-socket") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2.20") (default-features #t) (target "cfg(unix)") (kind 0)))) (hash "03vbj7i5sxr477i1chfanqp1p9nk5w03n2zs4wqhsjm82giizgm1")))

(define-public crate-toa-ping-0.1 (crate (name "toa-ping") (vers "0.1.0") (deps (list (crate-dep (name "lazy-socket") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2.20") (default-features #t) (target "cfg(unix)") (kind 0)))) (hash "17r2gjb48r155diirdxvbb56w79y4z5b5v9h5myj6z2jqk51d0g3")))

(define-public crate-toa-ping-0.1 (crate (name "toa-ping") (vers "0.1.1") (deps (list (crate-dep (name "lazy-socket") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2.20") (default-features #t) (target "cfg(unix)") (kind 0)))) (hash "13wjli83341kky9ri3x3pm3fi9837bs53mvg2h93g527cl0rc6cb")))

