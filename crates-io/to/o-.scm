(define-module (crates-io to o-) #:use-module (crates-io))

(define-public crate-too-many-open-files-0.1 (crate (name "too-many-open-files") (vers "0.1.0") (hash "15b6px7d5bq05m5sr8j4lc6mqh13hap0ylfsj9pkbm8cjqspyxj4")))

(define-public crate-too-many-timestamps-0.1 (crate (name "too-many-timestamps") (vers "0.1.0") (deps (list (crate-dep (name "anyhow") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "chrono") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^3.0.0-beta.4") (default-features #t) (kind 0)) (crate-dep (name "crossterm") (req "^0.22") (default-features #t) (kind 0)))) (hash "1gc6nmdd5hp8blasdwypxj0dbxdy4b8d3a1nchfjd5yqgaqysbfj")))

