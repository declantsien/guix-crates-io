(define-module (crates-io to ks) #:use-module (crates-io))

(define-public crate-toks-0.1 (crate (name "toks") (vers "0.1.0") (deps (list (crate-dep (name "html5ever") (req "^0.18.0") (default-features #t) (kind 0)))) (hash "1rh03c99r8qc36hvak8q0p7i2imlyp6dvg2rvibnwzxvir6sir2g")))

(define-public crate-toks-0.1 (crate (name "toks") (vers "0.1.1") (deps (list (crate-dep (name "html5ever") (req "^0.18.0") (default-features #t) (kind 0)))) (hash "15caqpdq6qwf5silzs8k7n8i1wpli18cqycyvqmd4r6m0xk5yj32")))

(define-public crate-toks-0.2 (crate (name "toks") (vers "0.2.0") (deps (list (crate-dep (name "html5ever") (req "^0.19.0") (default-features #t) (kind 0)))) (hash "1990r0pwm9dbhh49gpag3k8pnypagx7hs2b16rrs4fawcsxla098")))

(define-public crate-toks-0.3 (crate (name "toks") (vers "0.3.0") (deps (list (crate-dep (name "html5ever") (req "^0.20.0") (default-features #t) (kind 0)))) (hash "0f2lppm03ql8f9lp5paz4q9al7vlgksckygxn58vkjhnzvyrc923")))

(define-public crate-toks-0.4 (crate (name "toks") (vers "0.4.0") (deps (list (crate-dep (name "html5ever") (req "^0.21.0") (default-features #t) (kind 0)))) (hash "0dh895s3f6dw03khrg0d66954l4rqjdk50pw7fprbggkf5ij6xv5")))

(define-public crate-toks-0.5 (crate (name "toks") (vers "0.5.0") (deps (list (crate-dep (name "html5ever") (req "^0.22.0") (default-features #t) (kind 0)))) (hash "0sjn899dvaad238ca4dd2v0dzgwk7j66crbf9xqk0k5gpz5df1gd")))

(define-public crate-toks-0.6 (crate (name "toks") (vers "0.6.0") (deps (list (crate-dep (name "html5ever") (req "^0.22.2") (default-features #t) (kind 0)))) (hash "02dr40nwklr6w8rhgkrylzrgaiv5i8kq7pjrfc0cnxm3zfawqz8p")))

(define-public crate-toks-0.7 (crate (name "toks") (vers "0.7.0") (deps (list (crate-dep (name "html5ever") (req "^0.22.3") (default-features #t) (kind 0)))) (hash "15x3w3qcs8l4xj3v74i3rq4h7skx61swimsfq6zk5r5zssqh23w2")))

(define-public crate-toks-0.8 (crate (name "toks") (vers "0.8.0") (deps (list (crate-dep (name "html5ever") (req "^0.22.5") (default-features #t) (kind 0)))) (hash "1c9y5f76fz08ls8b7r6zpywh8mwwln1x4gwarpc2nld9hkbwdly3")))

(define-public crate-toks-0.9 (crate (name "toks") (vers "0.9.0") (deps (list (crate-dep (name "html5ever") (req "^0.25") (default-features #t) (kind 0)) (crate-dep (name "markup5ever_rcdom") (req "^0.1") (default-features #t) (kind 0)))) (hash "1ph8nhb6zwlag3an62vnv979sd570621kd6ka9pz02khgrvykm8d")))

(define-public crate-toks-1 (crate (name "toks") (vers "1.0.0") (deps (list (crate-dep (name "html5ever") (req "^0.25") (default-features #t) (kind 0)) (crate-dep (name "markup5ever_rcdom") (req "^0.1") (default-features #t) (kind 0)))) (hash "032bpmfcw23hv80pgdys8xx936zw4drbd88wzfxz38gi111nrag9")))

(define-public crate-toks-1 (crate (name "toks") (vers "1.1.0") (deps (list (crate-dep (name "html5ever") (req "^0.26.0") (default-features #t) (kind 0)) (crate-dep (name "markup5ever_rcdom") (req "^0.2.0") (default-features #t) (kind 0)))) (hash "1wm0yb4frnvl13s4zs3rra82f7ya6ffi50drk18rnqqwngrm29ga")))

(define-public crate-toks-1 (crate (name "toks") (vers "1.2.0") (deps (list (crate-dep (name "html5ever") (req "^0.26.0") (default-features #t) (kind 0)) (crate-dep (name "markup5ever_rcdom") (req "^0.2.0") (default-features #t) (kind 0)))) (hash "1f39wzgclzjalad14f032dmdjhd9gj6smwrfav1s2i9nhzgjnn4b")))

