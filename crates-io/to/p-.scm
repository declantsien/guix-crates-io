(define-module (crates-io to p-) #:use-module (crates-io))

(define-public crate-top-domain-list-0.1 (crate (name "top-domain-list") (vers "0.1.0") (deps (list (crate-dep (name "anyhow") (req "^1.0.75") (default-features #t) (kind 0)) (crate-dep (name "assert_cmd") (req "^2.0.12") (default-features #t) (kind 2)) (crate-dep (name "assert_fs") (req "^1.0.13") (default-features #t) (kind 2)) (crate-dep (name "clap") (req "^4.3.23") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "predicates") (req "^3.0.3") (default-features #t) (kind 2)))) (hash "1mrra6rzkz3f6mrki6pvzi9gw1b5bykfrjr9289xiclcihdrpadm")))

(define-public crate-top-domain-list-0.2 (crate (name "top-domain-list") (vers "0.2.0") (deps (list (crate-dep (name "anyhow") (req "^1.0.75") (default-features #t) (kind 0)) (crate-dep (name "assert_cmd") (req "^2.0.12") (default-features #t) (kind 2)) (crate-dep (name "assert_fs") (req "^1.0.13") (default-features #t) (kind 2)) (crate-dep (name "clap") (req "^4.3.23") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "predicates") (req "^3.0.3") (default-features #t) (kind 2)))) (hash "189z6rpjq78azik11a0k6yrja8isaj9dbs5mkvy21g56rlf3vlkd")))

(define-public crate-top-english-words-1 (crate (name "top-english-words") (vers "1.0.0") (hash "1aafsqx29qggcx8vq6ncn0pkwxrqdv6jg93yanp3h8h88f28bxz5")))

(define-public crate-top-english-words-1 (crate (name "top-english-words") (vers "1.0.1") (hash "1drhqvv8if89dsr5lgss7h366k77xkb658w4j99jqf3a3iyv9bsc")))

(define-public crate-top-english-words-1 (crate (name "top-english-words") (vers "1.0.2") (deps (list (crate-dep (name "include-lines") (req "^1.0.0") (default-features #t) (kind 0)))) (hash "0r6qkfvvj040b9lv2g8kyifkpwpb8gawsib6j853ggpppvj16xxa")))

(define-public crate-top-english-words-1 (crate (name "top-english-words") (vers "1.1.0") (deps (list (crate-dep (name "include-lines") (req "^1.0.0") (default-features #t) (kind 0)))) (hash "0dcfrvigz5svbr96hv1833lfmqnrj9dcra12jw12hq37n9qsxayi")))

(define-public crate-top-english-words-1 (crate (name "top-english-words") (vers "1.1.1") (deps (list (crate-dep (name "include-lines") (req "^1.1.2") (default-features #t) (kind 0)))) (hash "0b57spkjdy38l108fls1gi0jla6r9a7lz76hj2l8ywf2ng9zhvbq")))

(define-public crate-top-gg-0.0.0 (crate (name "top-gg") (vers "0.0.0") (hash "14fzysfv8ykjkwi0zc5pnz24kxi30fhca9apjkrqiya65d1niwsk") (yanked #t)))

(define-public crate-top-gg-0.1 (crate (name "top-gg") (vers "0.1.0-alpha.0") (deps (list (crate-dep (name "chrono") (req "^0.4") (features (quote ("serde"))) (default-features #t) (kind 0)) (crate-dep (name "futures-preview") (req "^0.3.0-alpha.19") (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.10.0-alpha.1") (features (quote ("json"))) (kind 0)) (crate-dep (name "serde") (req "^1") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "snafu") (req "^0.5") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^0.2.0-alpha.6") (default-features #t) (kind 2)) (crate-dep (name "url") (req "^2") (default-features #t) (kind 0)))) (hash "0fyjvgkh8zkzzhh7jvkwy7c5xwr51mlpgvmgs1wrpq7fciyxs1ml")))

(define-public crate-top-repos-0.1 (crate (name "top-repos") (vers "0.1.0") (hash "06vavl8aprcgrq4smkgnyv1qw9byavlvlv6m8m52ncily42wrva5")))

(define-public crate-top-repos-0.1 (crate (name "top-repos") (vers "0.1.1") (deps (list (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.5.4") (default-features #t) (kind 0)) (crate-dep (name "threadpool") (req "^1.8.1") (default-features #t) (kind 0)))) (hash "1wz7jw7jdmvqdn22dwim38sjjkyb0vjd2975zldhd0r3mpfqmh22")))

(define-public crate-top-type-sizes-0.1 (crate (name "top-type-sizes") (vers "0.1.0") (deps (list (crate-dep (name "eyre") (req "^0.6.8") (default-features #t) (kind 0)) (crate-dep (name "nom") (req "^7.1.3") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.3.26") (default-features #t) (kind 0)) (crate-dep (name "insta") (req "^1.28.0") (default-features #t) (kind 2)))) (hash "1zmj1y3bzspiqidq2djqy7ld8phhva6g02q6snz1x52nywrpy4md")))

(define-public crate-top-type-sizes-0.1 (crate (name "top-type-sizes") (vers "0.1.1") (deps (list (crate-dep (name "eyre") (req "^0.6.8") (default-features #t) (kind 0)) (crate-dep (name "nom") (req "^7.1.3") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.3.26") (default-features #t) (kind 0)) (crate-dep (name "insta") (req "^1.28.0") (default-features #t) (kind 2)))) (hash "0wcbl16w3ird91jzmxqfnjpcfaa0mhn6dp89s3ckhxy05n336rb6")))

(define-public crate-top-type-sizes-0.1 (crate (name "top-type-sizes") (vers "0.1.2") (deps (list (crate-dep (name "eyre") (req "^0.6.8") (default-features #t) (kind 0)) (crate-dep (name "nom") (req "^7.1.3") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.7.1") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.3.26") (default-features #t) (kind 0)) (crate-dep (name "insta") (req "^1.28.0") (default-features #t) (kind 2)))) (hash "14dc6jcdc0d9i7bx42fr7bi7xcw9pxhgvg79av9ijpfdcy3hva1j")))

(define-public crate-top-type-sizes-0.1 (crate (name "top-type-sizes") (vers "0.1.3") (deps (list (crate-dep (name "eyre") (req "^0.6.8") (default-features #t) (kind 0)) (crate-dep (name "nom") (req "^7.1.3") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.7.1") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.3.26") (default-features #t) (kind 0)) (crate-dep (name "insta") (req "^1.28.0") (default-features #t) (kind 2)))) (hash "0cmw2k9f3ckwl0ycw336gl7s875y25j96wkxl4hbsisypdm9yc5z")))

(define-public crate-top-type-sizes-0.1 (crate (name "top-type-sizes") (vers "0.1.4") (deps (list (crate-dep (name "eyre") (req "^0.6.8") (default-features #t) (kind 0)) (crate-dep (name "nom") (req "^7.1.3") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.7.1") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.3.26") (default-features #t) (kind 0)) (crate-dep (name "insta") (req "^1.28.0") (default-features #t) (kind 2)))) (hash "1djnca71gy1md20wnlr50y9zwh68swbw29j7s1hdcmnwhrksb3x0")))

(define-public crate-top-type-sizes-0.1 (crate (name "top-type-sizes") (vers "0.1.5") (deps (list (crate-dep (name "eyre") (req "^0.6.8") (default-features #t) (kind 0)) (crate-dep (name "nom") (req "^7.1.3") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.7.1") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.3.26") (default-features #t) (kind 0)) (crate-dep (name "insta") (req "^1.28.0") (default-features #t) (kind 2)))) (hash "17ijc7p34inz2w53yvgzjz9zamwnjjsi1a9wxxfqyk9199ffqxbv")))

(define-public crate-top-words-0.1 (crate (name "top-words") (vers "0.1.0") (hash "1vh2gcmzb9338rkqn1xvf9d973nvivvqliw03zva9g589c7y3ibr")))

