(define-module (crates-io to bi) #:use-module (crates-io))

(define-public crate-tobii-sys-0.1 (crate (name "tobii-sys") (vers "0.1.0") (hash "07kr2j3pdsfygv7d5zxmiy8qjfn16sphbyfif1rilgdj8mmyvf6c")))

(define-public crate-tobii-sys-0.2 (crate (name "tobii-sys") (vers "0.2.0") (hash "0y8imaj917qqr90av79wg0nyiij4iy9598g81g9qwjqyn617a830")))

(define-public crate-tobikris-foo-0.1 (crate (name "tobikris-foo") (vers "0.1.0") (hash "17qqfg7s3q8vw2d2hp6q1hiwlf402bwqnywnhv47kdqh9mb2aisp") (yanked #t)))

