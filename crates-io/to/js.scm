(define-module (crates-io to js) #:use-module (crates-io))

(define-public crate-tojson-0.1 (crate (name "tojson") (vers "0.1.0") (deps (list (crate-dep (name "serde") (req "^1.0.110") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.53") (default-features #t) (kind 0)) (crate-dep (name "serde_yaml") (req "^0.8.12") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.3.14") (default-features #t) (kind 0)) (crate-dep (name "toml") (req "^0.5.6") (default-features #t) (kind 0)))) (hash "0z2rkqcpvsnvl76ryx28bwmpvlzpibfz8dw6g7l0lkz63q6181an")))

(define-public crate-tojson-0.1 (crate (name "tojson") (vers "0.1.5") (deps (list (crate-dep (name "serde") (req "^1.0.110") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.53") (default-features #t) (kind 0)) (crate-dep (name "serde_yaml") (req "^0.8.12") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.3.14") (default-features #t) (kind 0)) (crate-dep (name "toml") (req "^0.5.6") (default-features #t) (kind 0)))) (hash "0hcx5acxvq4rlhhpvnvhwd58lk876hcn3l4c7fjjz40fnpgvfwlb")))

(define-public crate-tojson-0.1 (crate (name "tojson") (vers "0.1.6") (deps (list (crate-dep (name "serde") (req "^1.0.110") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.53") (default-features #t) (kind 0)) (crate-dep (name "serde_yaml") (req "^0.8.12") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.3.14") (default-features #t) (kind 0)) (crate-dep (name "toml") (req "^0.5.6") (default-features #t) (kind 0)))) (hash "09w0132gmnbw04v9rfa8rzlwin26bd1vs1p8ymkrjzna64xnv19j")))

(define-public crate-tojson-0.2 (crate (name "tojson") (vers "0.2.6") (deps (list (crate-dep (name "serde") (req "^1.0.110") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.53") (default-features #t) (kind 0)) (crate-dep (name "serde_yaml") (req "^0.8.12") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.3.14") (default-features #t) (kind 0)) (crate-dep (name "toml") (req "^0.5.6") (default-features #t) (kind 0)))) (hash "1djagb782920l0g3sgpmcp3nbnzldd4iblsddv1a9vb4jriwdwaj")))

(define-public crate-tojson-0.2 (crate (name "tojson") (vers "0.2.7") (deps (list (crate-dep (name "serde") (req "^1.0.110") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.53") (default-features #t) (kind 0)) (crate-dep (name "serde_yaml") (req "^0.8.12") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.3.14") (default-features #t) (kind 0)) (crate-dep (name "toml") (req "^0.5.6") (default-features #t) (kind 0)))) (hash "0mizv022vpqzs1a9q05fqiz6nc2iw131aq2a0rv22iy8qskcfrry")))

(define-public crate-tojson-0.3 (crate (name "tojson") (vers "0.3.0") (deps (list (crate-dep (name "serde") (req "^1.0.110") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.53") (features (quote ("preserve_order"))) (default-features #t) (kind 0)) (crate-dep (name "serde_yaml") (req "^0.8.12") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.3.14") (default-features #t) (kind 0)) (crate-dep (name "toml") (req "^0.5.6") (default-features #t) (kind 0)))) (hash "1q88k5iiilvgwadk01qch50a595dlajb0b83k4h1r2d8yhdi1qln")))

(define-public crate-tojson-0.3 (crate (name "tojson") (vers "0.3.1") (deps (list (crate-dep (name "serde") (req "^1.0.110") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.53") (features (quote ("preserve_order"))) (default-features #t) (kind 0)) (crate-dep (name "serde_yaml") (req "^0.8.12") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.3.14") (default-features #t) (kind 0)) (crate-dep (name "toml") (req "^0.5.6") (default-features #t) (kind 0)))) (hash "1z3pv8hnq4q766r7f8hdl7v9ipw4927s9nj8wdlp6fp2gf2zk3bd")))

(define-public crate-tojson-0.3 (crate (name "tojson") (vers "0.3.2") (deps (list (crate-dep (name "serde") (req "^1.0.110") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.53") (features (quote ("preserve_order"))) (default-features #t) (kind 0)) (crate-dep (name "serde_yaml") (req "^0.8.12") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.3.14") (default-features #t) (kind 0)) (crate-dep (name "toml") (req "^0.5.6") (default-features #t) (kind 0)))) (hash "1xzaxfwc6czfimdnn7p6hn4jhdzbphnwc4qc3lrz75l1g19gvpr3")))

(define-public crate-tojson_macros-0.1 (crate (name "tojson_macros") (vers "0.1.0") (deps (list (crate-dep (name "rustc-serialize") (req "*") (default-features #t) (kind 2)))) (hash "07pcv3sqhck948y717fa2cblp3cq5ybh2xhhpr2m17028r8yw7g6")))

(define-public crate-tojson_macros-0.1 (crate (name "tojson_macros") (vers "0.1.1") (deps (list (crate-dep (name "rustc-serialize") (req "*") (default-features #t) (kind 2)))) (hash "13ijjgz8xn5iw64mal35l4ij016r7x0jk7981wjqw9wb498fjanm")))

(define-public crate-tojson_macros-0.1 (crate (name "tojson_macros") (vers "0.1.2") (deps (list (crate-dep (name "rustc-serialize") (req "*") (default-features #t) (kind 2)))) (hash "0wmjb43adks4x53xr2dza6ha4492bfn9gi372j74nxl0lyr1dpmq")))

(define-public crate-tojson_macros-0.1 (crate (name "tojson_macros") (vers "0.1.3") (deps (list (crate-dep (name "rustc-serialize") (req "*") (default-features #t) (kind 2)))) (hash "0msan45db2pv2lsffbcb3clgwvp02ih41a3yvccrigzg81x1xgg0")))

(define-public crate-tojson_macros-0.1 (crate (name "tojson_macros") (vers "0.1.4") (deps (list (crate-dep (name "rustc-serialize") (req "*") (default-features #t) (kind 2)))) (hash "18ifvmns59jy450i0cxqxh4rm2czs4y2v01a6vksbwr40fh1vfjf")))

(define-public crate-tojson_macros-0.1 (crate (name "tojson_macros") (vers "0.1.5") (deps (list (crate-dep (name "rustc-serialize") (req "*") (default-features #t) (kind 2)))) (hash "0rsp4bndwfvl5mgv326iikmxl9pwhjziyb2gy6l42xcrp43kf1d7")))

(define-public crate-tojson_macros-0.1 (crate (name "tojson_macros") (vers "0.1.6") (deps (list (crate-dep (name "rustc-serialize") (req "*") (default-features #t) (kind 2)))) (hash "14d2rmg7kbz2sisg1nd3rx8nhxmsigjmllmszbmg2f8wr9qnh0d2")))

(define-public crate-tojson_macros-0.2 (crate (name "tojson_macros") (vers "0.2.0") (deps (list (crate-dep (name "rustc-serialize") (req "*") (default-features #t) (kind 2)))) (hash "0cqcfn8xhvam28ar96f7v2s92iacg02n3344dglak1raw95ryp2n")))

(define-public crate-tojson_macros-0.2 (crate (name "tojson_macros") (vers "0.2.1") (deps (list (crate-dep (name "rustc-serialize") (req "*") (default-features #t) (kind 2)))) (hash "02jpc7jfdpkq5whmr8gyfdz581ap0jwrzgsxnja45spxx2ja3w7q")))

(define-public crate-tojson_macros-0.2 (crate (name "tojson_macros") (vers "0.2.2") (deps (list (crate-dep (name "rustc-serialize") (req "*") (default-features #t) (kind 2)))) (hash "0ll0b5bgdnvxpfzdzq9p3f1rf173dqdw8pyyvc07g92ar1iiwhi8")))

(define-public crate-tojson_macros-0.2 (crate (name "tojson_macros") (vers "0.2.3") (deps (list (crate-dep (name "rustc-serialize") (req "^0.3.18") (default-features #t) (kind 2)))) (hash "1f6pdplr8d9fyqwa9wsm9hnidk13c9crk10kyj3v86pkd2n5ksc1")))

(define-public crate-tojson_macros-0.2 (crate (name "tojson_macros") (vers "0.2.4") (deps (list (crate-dep (name "rustc-serialize") (req "^0.3.18") (default-features #t) (kind 2)))) (hash "12252hqm8j61pk5w18lynsxv8ls25l7wa2faagshwi73hnzk4hp3")))

(define-public crate-tojson_macros-0.3 (crate (name "tojson_macros") (vers "0.3.0") (deps (list (crate-dep (name "quote") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "rustc-serialize") (req "^0.3.18") (default-features #t) (kind 2)) (crate-dep (name "syn") (req "^0.8") (default-features #t) (kind 0)))) (hash "0nqm6nj6b9hp3qdldmvxa78mayjg65bpdhx0rhr0d4mi625qc7y3")))

(define-public crate-tojson_macros-0.3 (crate (name "tojson_macros") (vers "0.3.1") (deps (list (crate-dep (name "quote") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "rustc-serialize") (req "^0.3.18") (default-features #t) (kind 2)) (crate-dep (name "syn") (req "^0.8") (default-features #t) (kind 0)))) (hash "08v3nznxrmx41lgmk7vqdywni1xjykpih9paybxw4j75j7gldlfg")))

(define-public crate-tojson_macros-0.3 (crate (name "tojson_macros") (vers "0.3.2") (deps (list (crate-dep (name "quote") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "rustc-serialize") (req "^0.3.18") (default-features #t) (kind 2)) (crate-dep (name "syn") (req "^0.10") (default-features #t) (kind 0)))) (hash "18bwjgrymjx8n5h73zi1ydhxkvchwxg1zf9ghpji10kdjdr92r2g")))

(define-public crate-tojson_macros-0.3 (crate (name "tojson_macros") (vers "0.3.3") (deps (list (crate-dep (name "quote") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "rustc-serialize") (req "^0.3.18") (default-features #t) (kind 2)) (crate-dep (name "syn") (req "^0.10") (default-features #t) (kind 0)))) (hash "02arnsgrcfcpz6vk9sbnjjskq21nrmgpd7ydnrjgswxb4hi05ppk")))

(define-public crate-tojson_macros-0.3 (crate (name "tojson_macros") (vers "0.3.4") (deps (list (crate-dep (name "quote") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "rustc-serialize") (req "^0.3.18") (default-features #t) (kind 2)) (crate-dep (name "syn") (req "^0.11") (default-features #t) (kind 0)))) (hash "0bdkhyrgbsj5m9gsk77ahiacrlnimdrzzyak14mv4056s2k1c5km")))

