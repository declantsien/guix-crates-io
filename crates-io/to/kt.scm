(define-module (crates-io to kt) #:use-module (crates-io))

(define-public crate-toktok-0.1 (crate (name "toktok") (vers "0.1.0") (hash "1q2xmz8sd8fp1vq9hnp6jprn1bcj76pi1053fw3nr9sj21403q2b")))

(define-public crate-toktok_generator-0.1 (crate (name "toktok_generator") (vers "0.1.0") (hash "1z2x41pahfmq05fv3z24qj5dhy58fcvfd95i9ilzfr7wdparawj3")))

(define-public crate-toktor-0.1 (crate (name "toktor") (vers "0.1.0") (deps (list (crate-dep (name "thiserror") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.37") (features (quote ("sync" "rt"))) (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.37") (features (quote ("time" "macros" "test-util"))) (default-features #t) (kind 2)) (crate-dep (name "tokio-test") (req "^0.4.4") (default-features #t) (kind 2)))) (hash "0ndihngzcrmpv8yz14zgqj2iasxkbjhiazljwmh7hpksicd2zksm")))

(define-public crate-toktor-0.1 (crate (name "toktor") (vers "0.1.1") (deps (list (crate-dep (name "thiserror") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.37") (features (quote ("sync" "rt"))) (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.37") (features (quote ("time" "macros" "test-util"))) (default-features #t) (kind 2)) (crate-dep (name "tokio-test") (req "^0.4.4") (default-features #t) (kind 2)))) (hash "1gpbszbh2da7wd04rn1n3xzg79lkj1nm2r7ac4zpjmb0xhq4462y") (rust-version "1.75")))

(define-public crate-toktor-0.1 (crate (name "toktor") (vers "0.1.2") (deps (list (crate-dep (name "thiserror") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.37") (features (quote ("sync" "rt"))) (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.37") (features (quote ("time" "macros" "test-util"))) (default-features #t) (kind 2)) (crate-dep (name "tokio-test") (req "^0.4.4") (default-features #t) (kind 2)))) (hash "04v653i0rnm0xcz82dkhsdpgzl1rmyz5bmvvn0mw7766z937y84g") (rust-version "1.75")))

(define-public crate-toktor-0.1 (crate (name "toktor") (vers "0.1.3") (deps (list (crate-dep (name "thiserror") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.37") (features (quote ("sync" "rt"))) (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.37") (features (quote ("time" "macros" "test-util"))) (default-features #t) (kind 2)) (crate-dep (name "tokio-test") (req "^0.4.4") (default-features #t) (kind 2)))) (hash "1sayh2zc6i0jaw8hxbqnbhkiy3cvf2kkg22rg8vgv1101dbnq5ym") (rust-version "1.75")))

(define-public crate-toktor-0.2 (crate (name "toktor") (vers "0.2.1") (deps (list (crate-dep (name "thiserror") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.37") (features (quote ("sync" "rt"))) (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.37") (features (quote ("time" "macros" "test-util"))) (default-features #t) (kind 2)) (crate-dep (name "tokio-test") (req "^0.4.4") (default-features #t) (kind 2)))) (hash "19js9m7w1k40b78wc23gnb2k570r76nj258zf9sasm4091ww6hg8") (rust-version "1.75")))

