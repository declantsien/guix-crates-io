(define-module (crates-io to su) #:use-module (crates-io))

(define-public crate-tosu-0.1 (crate (name "tosu") (vers "0.1.0") (deps (list (crate-dep (name "crossterm") (req "^0.27.0") (default-features #t) (kind 0)) (crate-dep (name "ratatui") (req "^0.25.0") (default-features #t) (kind 0)) (crate-dep (name "rodio") (req "^0.17.3") (default-features #t) (kind 0)))) (hash "0wjw4hrbqakrhwsi0k3jh5d5yrdp1408gfb4zn6am2nppi60wjbf")))

