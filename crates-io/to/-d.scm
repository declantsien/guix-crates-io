(define-module (crates-io to -d) #:use-module (crates-io))

(define-public crate-to-dir-0.2 (crate (name "to-dir") (vers "0.2.0") (deps (list (crate-dep (name "anyhow") (req "^1.0.44") (default-features #t) (kind 0)) (crate-dep (name "dirs") (req "^4.0.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.130") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "toml") (req "^0.5.8") (default-features #t) (kind 0)))) (hash "0ndrd50jyzi2b48shpjy574lnya7g2hdm379gkm4fz7lfvmx04b9")))

(define-public crate-to-dir-0.2 (crate (name "to-dir") (vers "0.2.1") (deps (list (crate-dep (name "anyhow") (req "^1.0.44") (default-features #t) (kind 0)) (crate-dep (name "dirs") (req "^4.0.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.130") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "toml") (req "^0.5.8") (default-features #t) (kind 0)))) (hash "06apc63i21x73zh2jdp1cr9i11b7ng6n30rfnbxx3wn194vrdmck")))

(define-public crate-to-directory-0.1 (crate (name "to-directory") (vers "0.1.0") (deps (list (crate-dep (name "bincode") (req "^0.5.1") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^2.1.2") (default-features #t) (kind 0)) (crate-dep (name "rustc-serialize") (req "^0.3.18") (default-features #t) (kind 0)))) (hash "143r4bkx5662bbyr10h5ak395w3rn1z76nj6ihm1dq3ifyszfflp")))

(define-public crate-to-do-0.1 (crate (name "to-do") (vers "0.1.0") (hash "0rb2ci5ggv6nlvvnppxz4yl4c3ysmncvbj8swhnc3mj0igzhpbcj")))

