(define-module (crates-io to rp) #:use-module (crates-io))

(define-public crate-torpid-0.1 (crate (name "torpid") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^2.33.3") (default-features #t) (kind 0)))) (hash "1fsr7y1ym61asxivnxnlf5nm633lc335yqxycml2a0gdsgb4v93n")))

(define-public crate-torpid-0.1 (crate (name "torpid") (vers "0.1.1") (deps (list (crate-dep (name "clap") (req "^2.33.3") (default-features #t) (kind 0)))) (hash "1bw13l4viff9d2vxjzhs2fkxlj6z47cfmhmfx7lcaa15hgvm2vv1")))

(define-public crate-torpid-0.1 (crate (name "torpid") (vers "0.1.2") (deps (list (crate-dep (name "clap") (req "^2.33.3") (default-features #t) (kind 0)))) (hash "1qdz4klj5bmlgvj9yic0nzimikyhyclpypbw65cpp3zj61qj6sfa")))

(define-public crate-torpid-0.1 (crate (name "torpid") (vers "0.1.3") (deps (list (crate-dep (name "clap") (req "^2.33.3") (default-features #t) (kind 0)))) (hash "1x8yiilz7g89zklw1j2disi05f7bpmahy25lyfih7vmbjlb9gdmq")))

