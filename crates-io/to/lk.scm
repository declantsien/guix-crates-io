(define-module (crates-io to lk) #:use-module (crates-io))

(define-public crate-tolk-0.1 (crate (name "tolk") (vers "0.1.0") (deps (list (crate-dep (name "tolk-sys") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "1ynyqg9105rmg68jna1jys238dxfib9vgi4xs0ddg70xj17xdfpi")))

(define-public crate-tolk-0.1 (crate (name "tolk") (vers "0.1.1") (deps (list (crate-dep (name "tolk-sys") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "1k4b9lbvmclliqwn0y22xb9rpb52wip2ln3dc353dskicmxmvapq")))

(define-public crate-tolk-0.2 (crate (name "tolk") (vers "0.2.0") (deps (list (crate-dep (name "tolk-sys") (req "^0.2") (default-features #t) (kind 0)))) (hash "17nilalx4h1hdrc4zlnqfr0jsxjpjwkrxs37j8z8fp7piqycdf1p")))

(define-public crate-tolk-0.2 (crate (name "tolk") (vers "0.2.1") (deps (list (crate-dep (name "tolk-sys") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "widestring") (req "^0.4") (default-features #t) (kind 0)))) (hash "1fzbdf0cyhii7lvnm88ny72kx7h6shg4hjk9h9rx1p90agbi87ci")))

(define-public crate-tolk-0.3 (crate (name "tolk") (vers "0.3.0") (deps (list (crate-dep (name "tolk-sys") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "widestring") (req "^0.4") (default-features #t) (kind 0)))) (hash "0fbyp1w5cjzscbrzpj7sa63g6si9wpr7vj0y2762k3bacclgx45n")))

(define-public crate-tolk-0.4 (crate (name "tolk") (vers "0.4.0") (deps (list (crate-dep (name "tolk-sys") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "widestring") (req "^0.4") (default-features #t) (kind 0)))) (hash "1jxp568a3y8qrr9l8z3mjfqr9f8skyz5g73z67z0rbzapfz66qgg")))

(define-public crate-tolk-0.5 (crate (name "tolk") (vers "0.5.0") (deps (list (crate-dep (name "lazy_static") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "tolk-sys") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "widestring") (req "^0.4") (default-features #t) (kind 0)))) (hash "1ih09v2jc15k6vlikz20b4sc6nzfs1d56fg7rs87irim8x632ndk")))

(define-public crate-tolk-sys-0.1 (crate (name "tolk-sys") (vers "0.1.0") (deps (list (crate-dep (name "libc") (req "^0.2.20") (default-features #t) (kind 0)))) (hash "0skvhs8haiswxj8s46fcdvpa1qwlcri09qfsmq6qa37122bksvis")))

(define-public crate-tolk-sys-0.2 (crate (name "tolk-sys") (vers "0.2.0") (deps (list (crate-dep (name "cc") (req "^1") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2.20") (default-features #t) (kind 0)))) (hash "12j7afivl73kn573aphf6rvyjw4dmc7sw8xw2b3apz3fm5sajxkm")))

(define-public crate-tolk-sys-0.2 (crate (name "tolk-sys") (vers "0.2.1") (deps (list (crate-dep (name "cc") (req "^1") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2.20") (default-features #t) (kind 0)))) (hash "1nj0cp267zik5xb3afx7zn7r4677ll3p2736790rzmp6rsrn5lig")))

(define-public crate-tolk-sys-0.2 (crate (name "tolk-sys") (vers "0.2.2") (deps (list (crate-dep (name "cc") (req "^1") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2.20") (default-features #t) (kind 0)))) (hash "0xjc9r0588jbjl1raxp8f4mp18b5898f7yrgv621zfibwxdd6285")))

(define-public crate-tolkien-0.1 (crate (name "tolkien") (vers "0.1.0") (hash "0lwwbpswp5az94xyrxk8n5kzy56sjnsq9dmfshnad6j5v7fdz94q")))

