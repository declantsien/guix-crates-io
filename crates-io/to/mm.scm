(define-module (crates-io to mm) #:use-module (crates-io))

(define-public crate-tommilligan_hello_world-0.1 (crate (name "tommilligan_hello_world") (vers "0.1.0") (hash "1di989w0s6nd877n193ij4d8zwi8f1918x25fgkcbk357s474mjy")))

(define-public crate-tommilligan_rmatrix-0.0.1 (crate (name "tommilligan_rmatrix") (vers "0.0.1") (deps (list (crate-dep (name "crossbeam-channel") (req "^0.3.3") (default-features #t) (kind 0)) (crate-dep (name "pancurses") (req "^0.16.0") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.6.1") (default-features #t) (kind 0)) (crate-dep (name "rand_xorshift") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "signal-hook") (req "^0.1.6") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.2.14") (default-features #t) (kind 0)) (crate-dep (name "term_size") (req "^0.3.1") (default-features #t) (kind 0)))) (hash "16v4w7bwi1i7lalzhkyyni2jak20d7b6s7knr5g5149hs4lixvm7")))

