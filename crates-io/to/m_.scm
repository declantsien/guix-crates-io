(define-module (crates-io to m_) #:use-module (crates-io))

(define-public crate-tom_jim-guess_number_game-0.1 (crate (name "tom_jim-guess_number_game") (vers "0.1.0") (deps (list (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)))) (hash "0371r851cyv09b6c868i9wy0gl9v70b5028mv7q1zy4l2d3jxl7j")))

(define-public crate-tom_tan-0.1 (crate (name "tom_tan") (vers "0.1.0") (hash "0f9fs14wigrpl5lpj212ynrw6pzwjz3mkxxk6c59a22yk94d1dqy") (yanked #t)))

(define-public crate-tom_thread_pool-0.1 (crate (name "tom_thread_pool") (vers "0.1.0") (hash "1m01k8h908crv17qy87ajgsymh4l3gyffw32yzhdb3n8fyh3qpqb")))

