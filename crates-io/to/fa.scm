(define-module (crates-io to fa) #:use-module (crates-io))

(define-public crate-tofas-0.1 (crate (name "tofas") (vers "0.1.0") (deps (list (crate-dep (name "custom_error") (req "^1.9.2") (default-features #t) (kind 0)) (crate-dep (name "fastrand") (req "^1.7.0") (default-features #t) (kind 2)))) (hash "0iaj037v2mihr6fy74ihn0jl0cxkfacad7zfnvyjq9h5im3jn6i5")))

(define-public crate-tofas-0.2 (crate (name "tofas") (vers "0.2.0") (deps (list (crate-dep (name "custom_error") (req "^1.9.2") (default-features #t) (kind 0)) (crate-dep (name "fastrand") (req "^1.7.0") (default-features #t) (kind 2)))) (hash "18ym6cpw59fbjjq79wcn8bbpmk28dzc5a7j841j7xmbzjb5pisr2")))

(define-public crate-tofas-0.2 (crate (name "tofas") (vers "0.2.1") (deps (list (crate-dep (name "custom_error") (req "^1.9.2") (default-features #t) (kind 0)) (crate-dep (name "fastrand") (req "^1.7.0") (default-features #t) (kind 2)))) (hash "0bl5gvhd6jl35zvynniymq5xghnd73q3ivx7cbr402qp0bfinr0k")))

(define-public crate-tofas-0.2 (crate (name "tofas") (vers "0.2.2") (deps (list (crate-dep (name "custom_error") (req "^1.9.2") (default-features #t) (kind 0)) (crate-dep (name "fastrand") (req "^1.7.0") (default-features #t) (kind 2)))) (hash "150nkwnq20g3rgxbn2ycab4snmgmkmsc1qb9p1zg8krsg4ghr6w7")))

(define-public crate-tofas-0.2 (crate (name "tofas") (vers "0.2.3") (deps (list (crate-dep (name "custom_error") (req "^1.9.2") (default-features #t) (kind 0)) (crate-dep (name "fastrand") (req "^1.7.0") (default-features #t) (kind 2)))) (hash "09px8n8dx2znzy4qzsqwdgkz5llfm1hvgxddnv6i533vghpsc0dx")))

(define-public crate-tofas-0.2 (crate (name "tofas") (vers "0.2.4") (deps (list (crate-dep (name "custom_error") (req "^1.9.2") (default-features #t) (kind 0)) (crate-dep (name "fastrand") (req "^1.7.0") (default-features #t) (kind 2)))) (hash "0hqrp87cm17njlhjd7lfcrb5xlrxl6fyjgsgpy8f4iwgnjsmc2i4")))

(define-public crate-tofas-0.2 (crate (name "tofas") (vers "0.2.5") (deps (list (crate-dep (name "custom_error") (req "^1.9.2") (default-features #t) (kind 0)) (crate-dep (name "fastrand") (req "^1.7.0") (default-features #t) (kind 2)))) (hash "0kzyjqd36g2iqkgkzxwggw401q49hs3v6sqqmmq92bjjnsp9197s")))

(define-public crate-tofas-0.2 (crate (name "tofas") (vers "0.2.6") (deps (list (crate-dep (name "custom_error") (req "^1.9.2") (default-features #t) (kind 0)) (crate-dep (name "fastrand") (req "^1.7.0") (default-features #t) (kind 2)))) (hash "018685spf7hd25g7r1jjhhwijqq7i40iphrzg2rlqrqinl3w9b2r")))

(define-public crate-tofas-0.2 (crate (name "tofas") (vers "0.2.7") (deps (list (crate-dep (name "custom_error") (req "^1.9.2") (default-features #t) (kind 0)) (crate-dep (name "fastrand") (req "^1.7.0") (default-features #t) (kind 2)))) (hash "17gndh8kiwc4na3aczf75887vyqsbn19lpl07dzvm7f7p7hjckab")))

(define-public crate-tofas_extras-0.2 (crate (name "tofas_extras") (vers "0.2.4") (deps (list (crate-dep (name "custom_error") (req "^1.9.2") (default-features #t) (kind 0)) (crate-dep (name "tofas") (req "^0.2.4") (default-features #t) (kind 0)))) (hash "0rhq90w5czw4xwjzf9c3a4jdcmcbxzvpflhiidbs6xiksg2s2is6")))

(define-public crate-tofas_extras-0.2 (crate (name "tofas_extras") (vers "0.2.5") (deps (list (crate-dep (name "custom_error") (req "^1.9.2") (default-features #t) (kind 0)) (crate-dep (name "tofas") (req "^0.2.5") (default-features #t) (kind 0)))) (hash "079d1xp564xz49risg2z9jx3yqnhwpgyqk4kbqd29ya2fw3lfnnj")))

(define-public crate-tofas_extras-0.2 (crate (name "tofas_extras") (vers "0.2.6") (deps (list (crate-dep (name "anyhow") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "custom_error") (req "^1.9.2") (default-features #t) (kind 0)) (crate-dep (name "pico-args") (req "^0.5") (default-features #t) (kind 0)) (crate-dep (name "tofas") (req "^0.2.6") (default-features #t) (kind 0)))) (hash "0131r0w1r8dwjyfjh6m08ijf0nxiha4ipj6xj250kg16aka0sf1c")))

(define-public crate-tofas_extras-0.2 (crate (name "tofas_extras") (vers "0.2.7") (deps (list (crate-dep (name "anyhow") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "custom_error") (req "^1.9.2") (default-features #t) (kind 0)) (crate-dep (name "pico-args") (req "^0.5") (default-features #t) (kind 0)) (crate-dep (name "tofas") (req "^0.2.7") (default-features #t) (kind 0)))) (hash "0paq1cbdvd5sycngx3inycdkxavnn1ny9f5y6mhcpa3wgcb8z1my")))

