(define-module (crates-io to nu) #:use-module (crates-io))

(define-public crate-tonujet_crate-0.1 (crate (name "tonujet_crate") (vers "0.1.0") (hash "06ikdzrxppix1z1r7ssm782gqikg61bvq13y7dzlj4aixkgnmw39")))

(define-public crate-tonujet_crate-0.1 (crate (name "tonujet_crate") (vers "0.1.1") (hash "08j05aqndaa6l4bfhx949rb2rqi4nf2pw421zq4kfp7qc0184pi3") (yanked #t)))

(define-public crate-tonujet_crate-1 (crate (name "tonujet_crate") (vers "1.1.1") (hash "1kad46q39rkyh1nljd4jx146zdd7cajm18m14g4ivrrr5g9zl5ck")))

