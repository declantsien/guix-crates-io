(define-module (crates-io to ms) #:use-module (crates-io))

(define-public crate-tomsg-rs-0.1 (crate (name "tomsg-rs") (vers "0.1.0") (deps (list (crate-dep (name "futures") (req "^0.3.4") (default-features #t) (kind 0)) (crate-dep (name "futures-util") (req "^0.3.4") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^0.2.22") (features (quote ("io-util" "sync" "net"))) (default-features #t) (kind 0)))) (hash "1v9k5x9ww2r4z1m8g6v1qwjaq8kcmz1qsi3xhlqnin9m3lfd3r6w")))

(define-public crate-tomsg-rs-0.2 (crate (name "tomsg-rs") (vers "0.2.0") (deps (list (crate-dep (name "futures") (req "^0.3.4") (default-features #t) (kind 0)) (crate-dep (name "futures-util") (req "^0.3.4") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^0.2.22") (features (quote ("io-util" "sync" "net"))) (default-features #t) (kind 0)))) (hash "16w7wfbx3n91g1w6rh3bh19dsgawfc2gyrhq94xxj2gb0w5kn4gq")))

(define-public crate-tomsg-rs-0.3 (crate (name "tomsg-rs") (vers "0.3.0") (deps (list (crate-dep (name "futures") (req "^0.3.4") (default-features #t) (kind 0)) (crate-dep (name "futures-util") (req "^0.3.4") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.5") (features (quote ("io-util" "sync" "net" "rt"))) (default-features #t) (kind 0)))) (hash "17q7ci94xjmnxzj2bpa9bl18fhi3hd0lgssjli8liw3n78qmiysl")))

(define-public crate-tomsg-rs-0.4 (crate (name "tomsg-rs") (vers "0.4.0") (deps (list (crate-dep (name "futures") (req "^0.3.4") (default-features #t) (kind 0)) (crate-dep (name "futures-util") (req "^0.3.4") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.5") (features (quote ("io-util" "sync" "net" "rt"))) (default-features #t) (kind 0)))) (hash "1jkc8sfz2xnrrj0brpzri4xvx9g1raqbpmnk82xj9fdlydhmpyfy")))

(define-public crate-tomsg-rs-0.5 (crate (name "tomsg-rs") (vers "0.5.0") (deps (list (crate-dep (name "futures") (req "^0.3.4") (default-features #t) (kind 0)) (crate-dep (name "futures-util") (req "^0.3.4") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.5") (features (quote ("io-util" "sync" "net" "rt"))) (default-features #t) (kind 0)))) (hash "19z6irvzzccyh3fndf4w73vgcfhvzazqaf1hmw3hq648czj12x6b")))

(define-public crate-tomsg-rs-0.6 (crate (name "tomsg-rs") (vers "0.6.0") (deps (list (crate-dep (name "tokio") (req "^1.5") (features (quote ("io-util" "sync" "net" "rt"))) (default-features #t) (kind 0)))) (hash "0ry9whphhlfzzk09mmwyd8qa0mxr9fmh5nqhbyg4d1bxxfb5xq6s")))

(define-public crate-tomson-0.1 (crate (name "tomson") (vers "0.1.0") (deps (list (crate-dep (name "rustc-serialize") (req "^0.3.6") (default-features #t) (kind 0)) (crate-dep (name "toml") (req "^0.1.20") (default-features #t) (kind 0)))) (hash "0zyvh0456msrv0ig1j6prj3hhb9xf0dfdqipm1g41yf7vmg5hk34")))

(define-public crate-tomson-0.1 (crate (name "tomson") (vers "0.1.1") (deps (list (crate-dep (name "rustc-serialize") (req "^0.3.14") (default-features #t) (kind 0)) (crate-dep (name "toml") (req "^0.1.20") (default-features #t) (kind 0)))) (hash "0m0aplw825pwqvg3lv5qj7lnrwc0bi3sviysv19fdlbr48wywn30")))

