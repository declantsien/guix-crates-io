(define-module (crates-io to mi) #:use-module (crates-io))

(define-public crate-tomi_minigrep-0.1 (crate (name "tomi_minigrep") (vers "0.1.0") (hash "05d90gapbi3n7n24gqkdm3rviprp9w3ibqrs23sf0jdqyq0nyyrz")))

(define-public crate-tomiko-0.1 (crate (name "tomiko") (vers "0.1.0") (hash "0nm7v9h3dnilw5ssjh3kbby7r3f12znxw1m21ffb91wlb62wls3w")))

(define-public crate-tomiko-auth-0.1 (crate (name "tomiko-auth") (vers "0.1.0") (deps (list (crate-dep (name "dotenv") (req "^0.15") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.7") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "sqlx") (req "^0.3") (features (quote ("runtime-tokio" "sqlite" "macros"))) (kind 0)) (crate-dep (name "tokio") (req "^0.2") (features (quote ("macros"))) (default-features #t) (kind 0)) (crate-dep (name "warp") (req "^0.2") (default-features #t) (kind 0)))) (hash "1hgknvgslr0jrj3d76942dp5plbgqwpn81zcpa7r4nb0qcfvscgn")))

