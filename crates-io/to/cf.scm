(define-module (crates-io to cf) #:use-module (crates-io))

(define-public crate-tocfl-0.1 (crate (name "tocfl") (vers "0.1.0") (deps (list (crate-dep (name "calamine") (req "^0.19.1") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.156") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.94") (default-features #t) (kind 0)))) (hash "052r91a0lpvhvh1sc34rc1fi4k9lcf0qvrhqzmh4xkrm3nh0102i")))

(define-public crate-tocfl-0.2 (crate (name "tocfl") (vers "0.2.0") (deps (list (crate-dep (name "calamine") (req "^0.19.1") (default-features #t) (kind 0)) (crate-dep (name "prettify_pinyin") (req "^1.1.2") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.156") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.94") (default-features #t) (kind 0)))) (hash "124lvx3mbgw9sjq8dwlf6kj6wkr5vsqcw337pg2551qjn7nj9jwm")))

(define-public crate-tocfl-0.3 (crate (name "tocfl") (vers "0.3.0") (deps (list (crate-dep (name "calamine") (req "^0.19.1") (default-features #t) (kind 0)) (crate-dep (name "prettify_pinyin") (req "^1.1.2") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.156") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.94") (default-features #t) (kind 0)))) (hash "0dv7na59dsclr59mahb918025nrikvjyf4xnp40738pxccr5h5x4")))

