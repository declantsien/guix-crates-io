(define-module (crates-io to mp) #:use-module (crates-io))

(define-public crate-tompl-0.0.1 (crate (name "tompl") (vers "0.0.1") (deps (list (crate-dep (name "clap") (req "^4.1.11") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "handlebars") (req "^4.3.6") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.158") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.94") (default-features #t) (kind 0)) (crate-dep (name "toml") (req "^0.7.3") (default-features #t) (kind 0)))) (hash "0qay8mfd6fwni6ziqryaydyyqr3fplwpavzkpydkb6sgrzx507j4") (yanked #t)))

