(define-module (crates-io to to) #:use-module (crates-io))

(define-public crate-toto-0.1 (crate (name "toto") (vers "0.1.0") (hash "0vb5409givxd44868zifnvfippg8d85hni8hl4k2f5wx8vna9h9g")))

(define-public crate-toto-1 (crate (name "toto") (vers "1.0.0") (hash "0xdd8553a0f0w1qc36h3yb7b0bd5b0yxzwlgjpyn9f0cx9ikdz7c")))

(define-public crate-totocalli-0.1 (crate (name "totocalli") (vers "0.1.1") (hash "08p5dccj5x82zrricwnm55x00kiigxr4r59bj9irgrnxnhb8igai")))

