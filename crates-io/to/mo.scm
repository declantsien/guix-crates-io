(define-module (crates-io to mo) #:use-module (crates-io))

(define-public crate-tomoko_cargo_more-0.1 (crate (name "tomoko_cargo_more") (vers "0.1.0") (hash "02q08yzk3z11s8kwk0svfndwd5gldb1i398p5hss6f9idf631a4m")))

(define-public crate-tomoko_cargo_more-0.3 (crate (name "tomoko_cargo_more") (vers "0.3.0") (hash "1wrawyy7dzsz60qklzby5rlyschjvns7d4navh15944axjqwh6ap")))

(define-public crate-tomorrow-0.0.0 (crate (name "tomorrow") (vers "0.0.0") (hash "1r95vw3z3jmi9ndbm4aaik4hbgk5a4qvraav7ys73lkshyh5nlgy")))

