(define-module (crates-io to pq) #:use-module (crates-io))

(define-public crate-topq-0.1 (crate (name "topq") (vers "0.1.0") (deps (list (crate-dep (name "heapless") (req "^0.5.5") (default-features #t) (kind 0)))) (hash "013vilnxkf3f9rnlx1ffib6nmi05avjknz1kjy5spw7b6zzjghjr")))

(define-public crate-topq-0.2 (crate (name "topq") (vers "0.2.0") (deps (list (crate-dep (name "generic-array") (req "^0.14.2") (default-features #t) (kind 0)))) (hash "1z739m2dwbj8nzfkin5wniwwz1nwmha4llgra5vvx1ha38w8b1dz")))

