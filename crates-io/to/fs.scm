(define-module (crates-io to fs) #:use-module (crates-io))

(define-public crate-tofs_guessing_game-0.1 (crate (name "tofs_guessing_game") (vers "0.1.0") (deps (list (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)))) (hash "00mb0hs5ig9wjzvw11b42f259fadcyjxgs10cxlip4rabmnfv6aa")))

