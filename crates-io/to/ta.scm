(define-module (crates-io to ta) #:use-module (crates-io))

(define-public crate-total-maps-0.1 (crate (name "total-maps") (vers "0.1.0") (deps (list (crate-dep (name "itertools") (req "^0.12.1") (default-features #t) (kind 2)) (crate-dep (name "num-traits") (req "^0.2.18") (optional #t) (default-features #t) (kind 0)))) (hash "0s6wy3459c209md2s5p2rl5wmsycy66p4z4ysxsc2ifv2rlchg33")))

(define-public crate-total-order-multi-map-0.3 (crate (name "total-order-multi-map") (vers "0.3.0") (deps (list (crate-dep (name "stable_deref_trait") (req "^1.0") (default-features #t) (kind 0)))) (hash "067zgwyz9hpz5bpv3i2zwhs9q1af84dcxcg99daq0xx5shcs86zz")))

(define-public crate-total-order-multi-map-0.4 (crate (name "total-order-multi-map") (vers "0.4.1") (deps (list (crate-dep (name "stable_deref_trait") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "vec-drain-where") (req "^1") (default-features #t) (kind 0)))) (hash "1dc8skjhajw8q5vnwjkijhhqa29lykbhsfyi2p6qwx3qmav5f0im")))

(define-public crate-total-order-multi-map-0.4 (crate (name "total-order-multi-map") (vers "0.4.2") (deps (list (crate-dep (name "stable_deref_trait") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "vec-drain-where") (req "^1") (default-features #t) (kind 0)))) (hash "04xykaxzqkiw9wpg5mq351bb4h1j1hb48pmqdk59njbalzswan4k")))

(define-public crate-total-order-multi-map-0.4 (crate (name "total-order-multi-map") (vers "0.4.3") (deps (list (crate-dep (name "stable_deref_trait") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "vec-drain-where") (req "^1") (default-features #t) (kind 0)))) (hash "0giy7grj4xaagb1cqdrd18g25bkvcvyia8c0vz1r5230wra5dqh4")))

(define-public crate-total-order-multi-map-0.4 (crate (name "total-order-multi-map") (vers "0.4.4") (deps (list (crate-dep (name "stable_deref_trait") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "vec-drain-where") (req "^1") (default-features #t) (kind 0)))) (hash "1wkjq9f2frs6745a7gr52dnyh5isd9anvrsmz0mix7ps99rsvqib")))

(define-public crate-total-order-multi-map-0.4 (crate (name "total-order-multi-map") (vers "0.4.5") (deps (list (crate-dep (name "stable_deref_trait") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "vec-drain-where") (req "^1") (default-features #t) (kind 0)))) (hash "1fjfrpgzrrfxjd1xvgxa8y7b28pirblpci1rn45v91r88nlbvrrd")))

(define-public crate-total-order-multi-map-0.4 (crate (name "total-order-multi-map") (vers "0.4.6") (deps (list (crate-dep (name "stable_deref_trait") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "vec-drain-where") (req "^1") (default-features #t) (kind 0)))) (hash "0gavil52xm1kdly1br8rk74669wh0kpwdvwlw2lghmb0jccmc79v")))

(define-public crate-total-recall-0.3 (crate (name "total-recall") (vers "0.3.0") (deps (list (crate-dep (name "eframe") (req "^0.16.0") (default-features #t) (kind 0)) (crate-dep (name "egui") (req "^0.16.1") (default-features #t) (kind 0)) (crate-dep (name "postgres") (req "^0.19.2") (default-features #t) (kind 0)))) (hash "10bxffgmq66qq8qfm83k5g636jq6kwjn52fcbf8690rg1n2g9a4p")))

(define-public crate-total-space-0.1 (crate (name "total-space") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^2.33.0") (default-features #t) (kind 0)) (crate-dep (name "escargot") (req "^0.5.2") (default-features #t) (kind 2)) (crate-dep (name "hashbrown") (req "^0.11.2") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2.8") (default-features #t) (kind 0)) (crate-dep (name "num_cpus") (req "^1.0.0") (default-features #t) (kind 0)) (crate-dep (name "string_cache") (req "^0.8.1") (default-features #t) (kind 0)) (crate-dep (name "strum") (req "^0.20.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "strum_macros") (req "^0.20.0") (default-features #t) (kind 0)))) (hash "0y8hz9r5ircfkmqqpnfvcha9fb3cc7yh1hq8xwd1vlmynnvhdcka") (yanked #t)))

(define-public crate-total-space-0.1 (crate (name "total-space") (vers "0.1.1") (deps (list (crate-dep (name "clap") (req "^2.33.0") (default-features #t) (kind 0)) (crate-dep (name "escargot") (req "^0.5.2") (default-features #t) (kind 2)) (crate-dep (name "hashbrown") (req "^0.11.2") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2.8") (default-features #t) (kind 0)) (crate-dep (name "num_cpus") (req "^1.0.0") (default-features #t) (kind 0)) (crate-dep (name "string_cache") (req "^0.8.1") (default-features #t) (kind 0)) (crate-dep (name "strum") (req "^0.20.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "strum_macros") (req "^0.20.0") (default-features #t) (kind 0)))) (hash "0q3rmr7wmzbgjg2dn42wmiffp6757a73843danzjk4flq6zvwp92")))

(define-public crate-total_float_wrap-0.1 (crate (name "total_float_wrap") (vers "0.1.0") (hash "13y2lcqr6wagqzc3bz4zqnj7amz3ikibyl7z9qaav0g4g1dfp6ky")))

(define-public crate-total_float_wrap-0.1 (crate (name "total_float_wrap") (vers "0.1.1") (hash "1cajsgi5pn6w1fwk67hs7nbqvpf1dfw9q9ckyddpp8vcra4vwnr4")))

(define-public crate-totall-0.1 (crate (name "totall") (vers "0.1.0") (deps (list (crate-dep (name "linecount") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "0vyjrh9fqpfxfh846gcld7q5j88vnm0p79xpnm43rfbk0znrh5f7")))

(define-public crate-totall-0.1 (crate (name "totall") (vers "0.1.1") (deps (list (crate-dep (name "linecount") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "path-clean") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "1hajh1hf9x3rpzddi5gcwd50x2h2smqp5qg13sfz9wda5zihvyxh")))

(define-public crate-totall-0.2 (crate (name "totall") (vers "0.2.0") (deps (list (crate-dep (name "linecount") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "path-clean") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "0r82qhw7hd2jph5ilyj8ld4py51jk7gg9fz7b0bg1cq7x38xhs16")))

(define-public crate-totally-0.1 (crate (name "totally") (vers "0.1.0") (hash "1i3j7rdj78i98f0qv14lm0lrc21z01xfsq7c6dc8077nh15jgi72")))

(define-public crate-totally-ordered-0.1 (crate (name "totally-ordered") (vers "0.1.0") (hash "1lhd893ql8ivgq8ksgw6y8zwypcb6d8bhkx1a59mqarffva8391r")))

(define-public crate-totally-ordered-0.1 (crate (name "totally-ordered") (vers "0.1.1") (hash "0cvzvdqaf578k9iawidpkf02mq21dkpbhcmhwdx7n7sb5v7fsnkb")))

(define-public crate-totally-ordered-0.2 (crate (name "totally-ordered") (vers "0.2.0") (deps (list (crate-dep (name "criterion") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.8") (default-features #t) (kind 2)))) (hash "08dfg3mz0kv0ws3dl3apvijg5xd5k2ik4p9czlgmlymv93dp3bkg") (features (quote (("std") ("default" "std"))))))

(define-public crate-totally-safe-transmute-0.0.1 (crate (name "totally-safe-transmute") (vers "0.0.1") (hash "1n8fim6myha23gcq0vxwp4nxhakcak340ki5ynlyjm7l0niqy37a")))

(define-public crate-totally-safe-transmute-0.0.2 (crate (name "totally-safe-transmute") (vers "0.0.2") (hash "0yyinvf4r6zc8pv6k57sjjlw2070av8v9vi9pv0yxxqcnxfg7v3s")))

(define-public crate-totally-safe-transmute-0.0.3 (crate (name "totally-safe-transmute") (vers "0.0.3") (hash "0m479s5gwqcxb0dfvbsryx6wqpmfgavgjd50dmrp5w6a72nyic1c")))

(define-public crate-totally-safe-transmute-0.0.4 (crate (name "totally-safe-transmute") (vers "0.0.4") (hash "1a33fi2kv0baqdvihm7a1j0j2n34ncr5yx9ws9n3xsym5pzhpfpr")))

(define-public crate-totally-sound-ptr-int-cast-0.1 (crate (name "totally-sound-ptr-int-cast") (vers "0.1.0") (deps (list (crate-dep (name "libc") (req "^0.2.121") (default-features #t) (kind 0)))) (hash "0q0l6xyih02n1l0xkm7p8jq0nlxiwziaxa335xl4l7hy7wc6sd6v")))

(define-public crate-totally-sound-ptr-int-cast-0.1 (crate (name "totally-sound-ptr-int-cast") (vers "0.1.1") (deps (list (crate-dep (name "libc") (req "^0.2.121") (default-features #t) (kind 0)))) (hash "1wc7v0pkwk4jbsql51d8m4bpkbhyq7g6gb2i1g5vd6xawfgniw6s")))

(define-public crate-totally-speedy-transmute-1 (crate (name "totally-speedy-transmute") (vers "1.69.420") (deps (list (crate-dep (name "safe") (req "^0.1.0") (default-features #t) (kind 0) (package "friendly_safety_buddy")))) (hash "0a3qpk76z5qzxpw57psmzm5x4ds6sk7q73j335hkczr4n1yhlzyl")))

(define-public crate-totally-sync-variable-0.1 (crate (name "totally-sync-variable") (vers "0.1.0") (deps (list (crate-dep (name "paste") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)))) (hash "0k5jaanb09dmc9rw8hdzl6ws8cn2llhk8cnzd39jgd01v9vfpxsa")))

(define-public crate-totally-sync-variable-0.1 (crate (name "totally-sync-variable") (vers "0.1.1") (deps (list (crate-dep (name "paste") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)))) (hash "0mld7vc0wldk0a2fd5rksjilixy50hpxzwdax9j79111cq7b1848")))

(define-public crate-totally_not_malicious-1 (crate (name "totally_not_malicious") (vers "1.0.0") (hash "117sa38i8pcamijyffphh37md0d3dypihr804padchrylwxcq6ay")))

