(define-module (crates-io to bu) #:use-module (crates-io))

(define-public crate-tobu-0.1 (crate (name "tobu") (vers "0.1.0") (deps (list (crate-dep (name "bytes") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1") (default-features #t) (kind 2)) (crate-dep (name "thiserror") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "tobu-format") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "05bsrscc068lqsp99caaznw4kbh3nz3vawv4rvx4m66d7rbvyc4w") (yanked #t)))

(define-public crate-tobu-format-0.1 (crate (name "tobu-format") (vers "0.1.0") (deps (list (crate-dep (name "bytes") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1") (default-features #t) (kind 0)))) (hash "014ymifzp70cnw286qi1vrvf3sgzxwqjxyc9cjnl1fhvwb0xkmw4") (yanked #t)))

(define-public crate-tobu-gen-0.1 (crate (name "tobu-gen") (vers "0.1.0") (deps (list (crate-dep (name "anyhow") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "bytes") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^2") (default-features #t) (kind 0)) (crate-dep (name "heck") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "tobu-format") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "1hqhmnr0km3f6ph4j7458b7xlnm9bjhfd5grxp7l8y24r04514z7") (yanked #t)))

