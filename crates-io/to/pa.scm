(define-module (crates-io to pa) #:use-module (crates-io))

(define-public crate-topaco_tasks-0.1 (crate (name "topaco_tasks") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^4.4.6") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "rusqlite") (req "^0.29.0") (features (quote ("bundled"))) (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.49") (default-features #t) (kind 0)) (crate-dep (name "uuid") (req "^1.5.0") (features (quote ("v4" "fast-rng" "macro-diagnostics"))) (default-features #t) (kind 0)))) (hash "1cls0n99jr342rmz72d67bm8gwd0y2w94lki0h1mx3bj3ljd18nc") (yanked #t)))

(define-public crate-topaz-0.1 (crate (name "topaz") (vers "0.1.0") (deps (list (crate-dep (name "time") (req "^0.1.38") (default-features #t) (kind 0)))) (hash "147il2v4yl2r5f4mcp73fwjnfzbkmkc47spx0zj51ifwaj661d9k")))

(define-public crate-topaz-0.1 (crate (name "topaz") (vers "0.1.1") (deps (list (crate-dep (name "time") (req "^0.1.38") (default-features #t) (kind 0)))) (hash "1lcx3im8ngr8xciajmj0mzg15zj1pbyb1fxvyvhy5a0zhgyybnji")))

(define-public crate-topaz-0.2 (crate (name "topaz") (vers "0.2.0") (deps (list (crate-dep (name "time") (req "^0.1.38") (default-features #t) (kind 0)))) (hash "07ngc4ia05jmwa65fgwnpnzksiikpifm1az3gdgk51shqnd0dgrm")))

(define-public crate-topaz-0.3 (crate (name "topaz") (vers "0.3.0") (deps (list (crate-dep (name "specs") (req "^0.10.0") (default-features #t) (kind 0)))) (hash "07a97mm9127kmnz37bakw7h3ns00arijxp7pis124gagnwg3mddh")))

(define-public crate-topaz-0.4 (crate (name "topaz") (vers "0.4.0") (deps (list (crate-dep (name "specs") (req "^0.10.0") (default-features #t) (kind 0)))) (hash "040v17yzk1qnai5l24j2pzqs4mscgdc483kzds05hg20jqnzbxqf")))

