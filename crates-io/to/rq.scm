(define-module (crates-io to rq) #:use-module (crates-io))

(define-public crate-torq-0.1 (crate (name "torq") (vers "0.1.0") (deps (list (crate-dep (name "benko") (req "^1.0") (default-features #t) (kind 0)))) (hash "188znrw4hij7d92d07m2296b47drrwqac5ss8h0v1li8ys962fry")))

