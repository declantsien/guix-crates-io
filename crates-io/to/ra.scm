(define-module (crates-io to ra) #:use-module (crates-io))

(define-public crate-tora-0.1 (crate (name "tora") (vers "0.1.0") (deps (list (crate-dep (name "tora_derive") (req "^0.1.0") (optional #t) (default-features #t) (kind 0)))) (hash "0bmgykfaimdxgcpsmfhqvnxdg66yz8xs152s229iciv5jbxmyp0m") (features (quote (("read_impl") ("derive" "tora_derive") ("default" "read_impl" "tora_derive"))))))

(define-public crate-tora-0.1 (crate (name "tora") (vers "0.1.1") (deps (list (crate-dep (name "tora_derive") (req "^0.1.0") (optional #t) (default-features #t) (kind 0)))) (hash "0mwkg8m14bj74lvdp5a4qapwd45fa8f646mi3f0416nwpa99pqby") (features (quote (("read_impl") ("derive" "tora_derive") ("default" "read_impl" "tora_derive"))))))

(define-public crate-tora-0.1 (crate (name "tora") (vers "0.1.2") (deps (list (crate-dep (name "tora_derive") (req "^0.1.1") (optional #t) (default-features #t) (kind 0)))) (hash "1s96vdfgcdmcpygz626dlgf12f62r629wn4zs6iqddjjs01ahnw1") (features (quote (("read_impl") ("derive" "tora_derive") ("default" "read_impl" "tora_derive"))))))

(define-public crate-tora-0.1 (crate (name "tora") (vers "0.1.3") (deps (list (crate-dep (name "tora_derive") (req "^0.1.2") (optional #t) (default-features #t) (kind 0)))) (hash "181l2d2fgmbvn1r9px969kkrl99spw995jni9p5ri4x61qaigc9p") (features (quote (("read_impl") ("derive" "tora_derive") ("default" "read_impl" "tora_derive"))))))

(define-public crate-tora-0.1 (crate (name "tora") (vers "0.1.4") (deps (list (crate-dep (name "tora_derive") (req "^0.1.3") (optional #t) (default-features #t) (kind 0)))) (hash "1xxpmfh5rr4gh77nz70narymn786b7pkhr5iz0ysa4apb0lrawww") (features (quote (("read_impl") ("derive" "tora_derive") ("default" "read_impl" "tora_derive"))))))

(define-public crate-tora-0.1 (crate (name "tora") (vers "0.1.5") (deps (list (crate-dep (name "tora_derive") (req "^0.1.3") (optional #t) (default-features #t) (kind 0)))) (hash "01kmr5r4kp17lr92zmzww7w5mbgxx3h9fsdba6gfj13j8scws6ml") (features (quote (("read_impl") ("dyn_impl") ("derive" "tora_derive") ("default" "tora_derive" "read_impl" "dyn_impl"))))))

(define-public crate-tora_derive-0.1 (crate (name "tora_derive") (vers "0.1.0") (deps (list (crate-dep (name "proc-macro2") (req "^1.0.52") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.23") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2.0.4") (default-features #t) (kind 0)))) (hash "0zns73wsidy6nfxma5az10cssmapn27xz9m7fbzpayyrlf5cwvpk")))

(define-public crate-tora_derive-0.1 (crate (name "tora_derive") (vers "0.1.1") (deps (list (crate-dep (name "proc-macro2") (req "^1.0.52") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.23") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2.0.4") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "0m6fzx94271r8n0mikaagnz4b69g56lwpz1424qg7m7dvlj7xk8j")))

(define-public crate-tora_derive-0.1 (crate (name "tora_derive") (vers "0.1.2") (deps (list (crate-dep (name "proc-macro2") (req "^1.0.63") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.23") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2.0.4") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "14pyzxkirz5ccczsgf7jikwyd1m6r3asnbwrdgzqpvmfyc8rkxxj")))

(define-public crate-tora_derive-0.1 (crate (name "tora_derive") (vers "0.1.3") (deps (list (crate-dep (name "proc-macro2") (req "^1.0.63") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.23") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2.0.4") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "0z25nlc389p9al3h238bqgc72blr6l8lf44rh28bnqbwnxhnfbnk")))

