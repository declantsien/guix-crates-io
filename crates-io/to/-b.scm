(define-module (crates-io to -b) #:use-module (crates-io))

(define-public crate-to-binary-0.1 (crate (name "to-binary") (vers "0.1.0") (deps (list (crate-dep (name "hex") (req "^0.4.2") (default-features #t) (kind 0)))) (hash "19hcp1hlf7s005i2xpbi1jkm29rg062hikc9p8fq6jz5b0vx8cd3")))

(define-public crate-to-binary-0.2 (crate (name "to-binary") (vers "0.2.0") (deps (list (crate-dep (name "hex") (req "^0.4.2") (default-features #t) (kind 0)))) (hash "1691mj9wahp387j09x7624r84f6qwkl96yk02fiybdh243mp4gnw")))

(define-public crate-to-binary-0.2 (crate (name "to-binary") (vers "0.2.1") (deps (list (crate-dep (name "hex") (req "^0.4.2") (default-features #t) (kind 0)))) (hash "03f6yixvw595rpamhpwbw56n2dy3xfrsndabhz7chbrrz2aw7gqh")))

(define-public crate-to-binary-0.3 (crate (name "to-binary") (vers "0.3.0") (deps (list (crate-dep (name "hex") (req "^0.4.2") (default-features #t) (kind 0)))) (hash "0a02fr2l5c3943995wj5z7qaj6dqb8dn1yfp6ssbvv2b5x41q6c8")))

(define-public crate-to-binary-0.4 (crate (name "to-binary") (vers "0.4.0") (deps (list (crate-dep (name "hex") (req "^0.4.2") (default-features #t) (kind 0)))) (hash "1qxv4c46vp6nb9jiid0vv07vj0bl9fjyiw41rpximza8r0mma924")))

