(define-module (crates-io to ro) #:use-module (crates-io))

(define-public crate-toronja-0.1 (crate (name "toronja") (vers "0.1.0") (hash "0rbp6p6ncyqsx2c3dagm3v9g29y28bcn5zwpc4d61ck4h7s3zxxn")))

(define-public crate-toronto-0.1 (crate (name "toronto") (vers "0.1.0") (hash "0lcl4w6y6a4k3k7ffk2c4rha5f27myrvqxp0wpn69bn62n6w12fc")))

(define-public crate-torophy-0.1 (crate (name "torophy") (vers "0.1.0") (deps (list (crate-dep (name "glium") (req "^0.26") (default-features #t) (kind 2)) (crate-dep (name "imgui") (req "^0.3.0") (default-features #t) (kind 2)) (crate-dep (name "imgui-glium-renderer") (req "^0.3.0") (default-features #t) (kind 2)) (crate-dep (name "imgui-winit-support") (req "^0.3.0") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.7") (default-features #t) (kind 2)))) (hash "04zvwp1mjzqfxqdq4q6s17l08l2q45nl5yy06fdivfy1ncarxmn4")))

(define-public crate-torophy-0.1 (crate (name "torophy") (vers "0.1.1") (deps (list (crate-dep (name "glium") (req "^0.26") (default-features #t) (kind 2)) (crate-dep (name "imgui") (req "^0.3.0") (default-features #t) (kind 2)) (crate-dep (name "imgui-glium-renderer") (req "^0.3.0") (default-features #t) (kind 2)) (crate-dep (name "imgui-winit-support") (req "^0.3.0") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.7") (default-features #t) (kind 2)))) (hash "1y80psygq7bfmx420rxj9y3s0jjrx6fb1qmrbmq3zl98n9lv1xrr")))

(define-public crate-toros-0.1 (crate (name "toros") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^3") (features (quote ("std"))) (kind 0)) (crate-dep (name "log") (req "^0") (features (quote ("std"))) (kind 0)) (crate-dep (name "nixel") (req "^2") (kind 0)) (crate-dep (name "walkdir") (req "^2") (kind 2)))) (hash "1yc126j4wyaz7r8k1g9wjm61dsf1anpz0ppa6f2n5nxcfgap1yza")))

