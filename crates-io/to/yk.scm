(define-module (crates-io to yk) #:use-module (crates-io))

(define-public crate-toykio-0.2 (crate (name "toykio") (vers "0.2.0") (deps (list (crate-dep (name "futures-preview") (req "^0.3.0-alpha.2") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "pretty_env_logger") (req "^0.2") (default-features #t) (kind 0)))) (hash "15fml22629y0kz6i3jdcs62gkmmifcg84yg6as33jb722hcdivi7") (yanked #t)))

(define-public crate-toykio-0.2 (crate (name "toykio") (vers "0.2.1") (deps (list (crate-dep (name "futures-preview") (req "^0.3.0-alpha.2") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "pretty_env_logger") (req "^0.2") (default-features #t) (kind 0)))) (hash "00an44x9nkd7bp3ry5y0qk694jfbs1ldrjjslyffdici8fz3s8l3") (yanked #t)))

(define-public crate-toykio-0.2 (crate (name "toykio") (vers "0.2.2") (deps (list (crate-dep (name "futures-preview") (req "^0.3.0-alpha.3") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "pretty_env_logger") (req "^0.2") (default-features #t) (kind 0)))) (hash "1ywx58ar6vjvjhhc8phblqz8c4ki4waa178karj2aba53djdcwp6") (yanked #t)))

