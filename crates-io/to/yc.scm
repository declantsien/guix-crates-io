(define-module (crates-io to yc) #:use-module (crates-io))

(define-public crate-toyc-0.1 (crate (name "toyc") (vers "0.1.0") (hash "0522i32xzkkkdh3d14s7x0l9g8aq4cx3zqk6i1q81aw1zy1jf42w")))

(define-public crate-toyc-0.1 (crate (name "toyc") (vers "0.1.1") (hash "199m646gndv0q6agqvims6r79g0249qqfw0b08hgn4x6z9ykdhmg")))

(define-public crate-toyc-0.1 (crate (name "toyc") (vers "0.1.2") (hash "1zwbh735vch23caf56h3jfw1fwqkwmnzwndkqnhfagwksza7z6ns")))

(define-public crate-toyc-0.1 (crate (name "toyc") (vers "0.1.3") (hash "1g3wg6zc98d59jwvb75i7wmzmqgv5n33556sm0f1184mrhavqyd1")))

(define-public crate-toyc-0.1 (crate (name "toyc") (vers "0.1.4") (hash "0s8iz7g78vlb1ip6r3w0rjnnq2wpy42xbmhdrgwmc2i9wjz5ppsv")))

