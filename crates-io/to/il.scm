(define-module (crates-io to il) #:use-module (crates-io))

(define-public crate-toil-0.1 (crate (name "toil") (vers "0.1.0") (hash "1f2v0jqll79g9nrwcs560wrgfpzmvb2as73m3lm4wz3ldhwjwzg0") (yanked #t)))

(define-public crate-toiletcli-0.0.1 (crate (name "toiletcli") (vers "0.0.1") (hash "0c03jvfhm86kdabl0gqkw26qg9y62f108lw05472wc1cd2sg7hv9") (yanked #t)))

(define-public crate-toiletcli-0.0.2 (crate (name "toiletcli") (vers "0.0.2") (hash "1prcmaqfyb0sxp38mzm1bss34dj4lh449rsmqgj6yrd2yqxan32s") (yanked #t)))

(define-public crate-toiletcli-0.0.3 (crate (name "toiletcli") (vers "0.0.3") (hash "1az1rayacsq13mrzqp04ljrsc8c8xv774q1ylnxmi6zzp1dy7kv1") (yanked #t)))

(define-public crate-toiletcli-0.0.4 (crate (name "toiletcli") (vers "0.0.4") (hash "1f70a387z0ksyswvrb62npjmrqxhqxzgdfzvimp4s1701mhk9nkq") (yanked #t)))

(define-public crate-toiletcli-0.0.5 (crate (name "toiletcli") (vers "0.0.5") (hash "1sk72mxx1k1lqpl9i531f7g55z4qsrxkfigrlccw6q10wwj31g9x")))

(define-public crate-toiletcli-0.0.6 (crate (name "toiletcli") (vers "0.0.6") (hash "1r0g55h98snqbxgna6xi6za62lhqf39ihbm2i2khm8fqr1a8121g") (yanked #t)))

(define-public crate-toiletcli-0.0.7 (crate (name "toiletcli") (vers "0.0.7") (hash "0k8hv2llnifzm26vncfiahny1mxklh0zcxf3bzj2hfp96rgnjzrs") (yanked #t)))

(define-public crate-toiletcli-0.1 (crate (name "toiletcli") (vers "0.1.0") (hash "1dvf48lg17w6fd83xnnsfxqj7s86w5sb6bsp0sca29k2rkbx10g2")))

(define-public crate-toiletcli-0.2 (crate (name "toiletcli") (vers "0.2.0") (hash "1yh3n3cckf9is2y05gb016y8fjzyfd17v6yf2330v3py27b80g9j") (features (quote (("mock_colors")))) (yanked #t)))

(define-public crate-toiletcli-0.2 (crate (name "toiletcli") (vers "0.2.1") (hash "0gk5ipr6dijjw3n0aazw06yk7csizvk1w0xcblr7s0hnv5cql32k") (features (quote (("mock_colors")))) (yanked #t)))

(define-public crate-toiletcli-0.2 (crate (name "toiletcli") (vers "0.2.3") (hash "0xparvsj160a6c8l7r3hqi2yh8zn7k66c9zk8vqracgv87iihlbq") (features (quote (("mock_colors") ("flags") ("default" "colors" "flags") ("colors")))) (yanked #t)))

(define-public crate-toiletcli-0.4 (crate (name "toiletcli") (vers "0.4.0") (hash "1p2c29f00szqk9wvysynrscvl6jmch6kib85cvpaq9b3fhp6k118") (features (quote (("default")))) (yanked #t)))

(define-public crate-toiletcli-0.4 (crate (name "toiletcli") (vers "0.4.1") (hash "13mar4spmmylh1abnq2zm23rbds194x56hlqbrhn1qmi1aa07wqx") (features (quote (("default")))) (yanked #t)))

(define-public crate-toiletcli-0.5 (crate (name "toiletcli") (vers "0.5.0") (hash "1ya1ybgcrvmgz5xx29621m5dhlgv6cwx117hxqy5mmssar4b2ci0") (features (quote (("default")))) (yanked #t)))

(define-public crate-toiletcli-0.5 (crate (name "toiletcli") (vers "0.5.1") (hash "0fkc6jw99nbq50caaldki456rzjalywqai4i5fh29jpfyqhfmwin") (features (quote (("default"))))))

(define-public crate-toiletcli-0.6 (crate (name "toiletcli") (vers "0.6.0") (hash "0khid51rr1ghr2fv2683ix9zz1irwrc6r1v6zcvdfh7y91hbb7vm") (features (quote (("flags") ("escapes") ("default" "flags" "colors" "escapes") ("colors")))) (yanked #t)))

(define-public crate-toiletcli-0.6 (crate (name "toiletcli") (vers "0.6.1") (hash "00k02pnd5314cybq5n2fbkdfly9iml9nms3jcrplcqw198ny20wg") (features (quote (("flags") ("escapes") ("default" "flags" "colors" "escapes") ("colors"))))))

(define-public crate-toiletcli-0.7 (crate (name "toiletcli") (vers "0.7.0") (hash "0kr8vih2dkw22iar7mp8gk41whncfz42h6vfgxipvm7dcb0l6j6w") (features (quote (("flags") ("escapes") ("default" "flags" "colors" "escapes") ("colors")))) (yanked #t)))

(define-public crate-toiletcli-0.7 (crate (name "toiletcli") (vers "0.7.1") (hash "1b600z2cq816yhqgrxp53xlqcpnkc2ap7airqjdba600s4i71x5v") (features (quote (("flags") ("escapes") ("default" "flags" "colors" "escapes") ("colors")))) (yanked #t)))

(define-public crate-toiletcli-0.7 (crate (name "toiletcli") (vers "0.7.3") (hash "0v31fai3i460zmkvqqrbm0bmz1ppn6wxqdhqg7gpw8wkisfwr9xi") (features (quote (("mock_codes" "colors") ("flags") ("escapes") ("default" "flags" "colors" "escapes") ("colors"))))))

(define-public crate-toiletcli-0.8 (crate (name "toiletcli") (vers "0.8.1") (hash "03ck29p6lv197sdmn4xf2mws5fm2jc19qar1rlzgmzkpidkq0rkx") (features (quote (("mock_codes" "colors") ("flags") ("escapes") ("default" "flags" "colors" "escapes") ("colors"))))))

(define-public crate-toiletcli-0.9 (crate (name "toiletcli") (vers "0.9.0") (deps (list (crate-dep (name "atty") (req "^0.2.14") (default-features #t) (kind 0)))) (hash "1596pj333f7i88s4lbfdvr0857hc9fzq7fnjwpdzqvabd5zy1krz") (features (quote (("mock_codes" "colors") ("flags") ("escapes") ("default" "flags" "colors" "escapes") ("colors")))) (yanked #t)))

(define-public crate-toiletcli-0.9 (crate (name "toiletcli") (vers "0.9.1") (deps (list (crate-dep (name "atty") (req "^0.2.14") (default-features #t) (kind 0)))) (hash "12063xr1y2g35625f0arah5l6lc1lqdf185rchnicqmc3fzfx1wi") (features (quote (("mock_codes" "colors") ("flags") ("escapes") ("default" "flags" "colors" "escapes") ("colors")))) (yanked #t)))

(define-public crate-toiletcli-0.9 (crate (name "toiletcli") (vers "0.9.2") (deps (list (crate-dep (name "atty") (req "^0.2.14") (default-features #t) (kind 0)))) (hash "1vrxly27732sy6825xsiqfpri4lbxhic39sbvvcjd4vhg50kqrmj") (features (quote (("mock_codes" "colors") ("flags") ("escapes") ("default" "flags" "colors" "escapes") ("colors")))) (yanked #t)))

(define-public crate-toiletcli-0.9 (crate (name "toiletcli") (vers "0.9.3") (deps (list (crate-dep (name "atty") (req "^0.2.14") (default-features #t) (kind 0)))) (hash "17k3kzmfpbn2ksmkjjl5mqpkann6yk0261pdzynri2g1pf5f10wf") (features (quote (("mock_codes" "colors") ("flags") ("escapes") ("default" "flags" "colors" "escapes") ("colors"))))))

(define-public crate-toiletcli-0.9 (crate (name "toiletcli") (vers "0.9.4") (deps (list (crate-dep (name "atty") (req "^0.2.14") (default-features #t) (kind 0)))) (hash "12nljzw1mrmaxqy2rxhxkslr4kdf4diwchw7sxcz9h3csv1lbv8j") (features (quote (("mock_codes" "colors") ("flags") ("escapes") ("default" "flags" "colors" "escapes") ("colors"))))))

(define-public crate-toiletcli-0.10 (crate (name "toiletcli") (vers "0.10.0") (deps (list (crate-dep (name "atty") (req "^0.2.14") (default-features #t) (kind 0)))) (hash "1m4nyhgs9bb7w78k3vnkj1n2kyjrqrb17h993g49yb8gvmjgk9aw") (features (quote (("mock_codes" "colors") ("flags") ("escapes") ("default" "flags" "colors" "escapes") ("colors"))))))

(define-public crate-toiletcli-0.11 (crate (name "toiletcli") (vers "0.11.0") (deps (list (crate-dep (name "atty") (req "^0.2.14") (default-features #t) (kind 0)))) (hash "07v6la6a4rvqdgn328ifazmwnim5vrkf3ws0b4xzpz4cpz6p3l1b") (features (quote (("mock_codes" "colors") ("flags") ("escapes") ("default" "flags" "colors" "escapes") ("colors")))) (yanked #t)))

(define-public crate-toiletcli-0.12 (crate (name "toiletcli") (vers "0.12.0") (deps (list (crate-dep (name "atty") (req "^0.2.14") (default-features #t) (kind 0)))) (hash "0vww7cfx8hbqcz7ywjz0q52w1hlpvnqsgcy0czy09syrmqk10psz") (features (quote (("mock_codes" "colors") ("flags") ("escapes") ("default" "flags" "colors" "escapes") ("colors"))))))

(define-public crate-toiletdb-0.1 (crate (name "toiletdb") (vers "0.1.0") (deps (list (crate-dep (name "serde") (req "^1.0.104") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.45") (default-features #t) (kind 0)) (crate-dep (name "tempfile") (req "^3.1.0") (default-features #t) (kind 0)))) (hash "1rnfzzzw30r6picd578ffr2k2wqa2zivl6hqpy6r3gwvnc2axq04")))

(define-public crate-toiletdb-0.1 (crate (name "toiletdb") (vers "0.1.1") (deps (list (crate-dep (name "serde") (req "^1.0.104") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.45") (default-features #t) (kind 0)) (crate-dep (name "tempfile") (req "^3.1.0") (default-features #t) (kind 0)))) (hash "1dr8f46czhhxnfjp5hx86pxppwvx91p5j3ngyflb7p90avg8m7jw")))

(define-public crate-toiletdb-0.1 (crate (name "toiletdb") (vers "0.1.2") (deps (list (crate-dep (name "serde") (req "^1.0.104") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.45") (default-features #t) (kind 0)) (crate-dep (name "tempfile") (req "^3.1.0") (default-features #t) (kind 0)))) (hash "1d0qfrwam3zbcw6cf8wkiiyhkznjf0dgx0gyl32d1czcd07mr2gq")))

