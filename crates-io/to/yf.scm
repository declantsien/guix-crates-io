(define-module (crates-io to yf) #:use-module (crates-io))

(define-public crate-toyfoc-0.0.1 (crate (name "toyfoc") (vers "0.0.1") (deps (list (crate-dep (name "embedded-hal") (req "^0.2") (features (quote ("unproven"))) (default-features #t) (kind 0)) (crate-dep (name "fugit") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "mt6701") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "nb") (req "^1.1") (default-features #t) (kind 0)))) (hash "08ylnr8mp3ac7kl64fzqrhayz9mwvxk26dz524vx0zllhxd4ky5l")))

