(define-module (crates-io to _h) #:use-module (crates-io))

(define-public crate-to_hash_map-0.1 (crate (name "to_hash_map") (vers "0.1.0") (deps (list (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (default-features #t) (kind 0)))) (hash "0hr57j7d8icg2hnbharcin17ghvz18p4m771xc7sqdm8ggqn9amr")))

