(define-module (crates-io to _u) #:use-module (crates-io))

(define-public crate-to_unit-1 (crate (name "to_unit") (vers "1.0.0") (hash "19pmacx5wiv0r6i38cl20jxf5cxpykky5z0fsh243i000i4p02z4")))

(define-public crate-to_unit-1 (crate (name "to_unit") (vers "1.0.1") (hash "0wsjk0rch7ma24344bsyqjmkmykgw5w9k8r2bgpr7vl3v3pij1mg")))

(define-public crate-to_unit-1 (crate (name "to_unit") (vers "1.0.2") (hash "1vrlsgindhk8yq7vcz9na6d0d63ka4ff3fg0b9hw5233x1m9ydxm")))

(define-public crate-to_url-0.1 (crate (name "to_url") (vers "0.1.0") (deps (list (crate-dep (name "serde_json") (req "^1.0.93") (default-features #t) (kind 0)) (crate-dep (name "to_url_derive") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "0plmprw38pd218v4bczkqjc5vcf1w8qscry2waa6l2cp41s4gkwa") (features (quote (("derive") ("default" "derive"))))))

(define-public crate-to_url_derive-0.1 (crate (name "to_url_derive") (vers "0.1.0") (deps (list (crate-dep (name "proc-macro2") (req "^1.0.51") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.23") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.93") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.107") (default-features #t) (kind 0)))) (hash "05fkkfmyfjhsp67q0i6n1y1m6ngv3m76kj2v195gp5r567hrgli4")))

