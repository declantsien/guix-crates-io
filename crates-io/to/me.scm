(define-module (crates-io to me) #:use-module (crates-io))

(define-public crate-tome-0.1 (crate (name "tome") (vers "0.1.0") (hash "0qayvan5cgb2g3pllhk4ifpqimw57w96vhadqd4cmlsbf7nng71p") (yanked #t)))

(define-public crate-tome-0.0.1 (crate (name "tome") (vers "0.0.1") (hash "0k0v2c7rr8cz68rcbns2g4rrq5a3khvnizf91g6lhbsnb7v8j4f6") (yanked #t)))

(define-public crate-tometrics-0.1 (crate (name "tometrics") (vers "0.1.0") (deps (list (crate-dep (name "chrono") (req "^0.4.22") (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.11.11") (features (quote ("json"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.85") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "0xh6pzglvb1anwc5qyr7k6fhhbn2kyl66js664m4x63h1l1vq4k6")))

(define-public crate-tometrics-0.1 (crate (name "tometrics") (vers "0.1.1") (deps (list (crate-dep (name "chrono") (req "^0.4.22") (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.11.11") (features (quote ("json"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.85") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "1dwgnpz05ccl0dp0r85xl7yy9a9wq4m2c9cr2dhi94650xx9ng8q")))

(define-public crate-tometrics-0.1 (crate (name "tometrics") (vers "0.1.2") (deps (list (crate-dep (name "chrono") (req "^0.4.22") (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.11.11") (features (quote ("json"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.85") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "1wn6fdfxgb3cwmci2bracy70cq1l70v9mdjlg8qpnhn91n1ixhax")))

