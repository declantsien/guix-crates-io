(define-module (crates-io to y_) #:use-module (crates-io))

(define-public crate-toy_arena-0.1 (crate (name "toy_arena") (vers "0.1.0") (deps (list (crate-dep (name "derivative") (req "^2.2.0") (default-features #t) (kind 0)) (crate-dep (name "smallvec") (req "^1.6.1") (default-features #t) (kind 0)))) (hash "0yqn9wvaiarqp6xmpsrxfx6gi0pl7xz3vn9wjkxmlm8hrnzlsid9")))

(define-public crate-toy_arena-0.1 (crate (name "toy_arena") (vers "0.1.1") (deps (list (crate-dep (name "derivative") (req "^2.2.0") (default-features #t) (kind 0)) (crate-dep (name "smallvec") (req "^1.6.1") (default-features #t) (kind 0)))) (hash "1fq79l2x5n3n61cvcapv57aw4pxc321vxyz5lzvbd9kdqa5d7ni1")))

(define-public crate-toy_arena-0.1 (crate (name "toy_arena") (vers "0.1.2") (deps (list (crate-dep (name "derivative") (req "^2.2.0") (default-features #t) (kind 0)) (crate-dep (name "smallvec") (req "^1.6.1") (default-features #t) (kind 0)))) (hash "16jfr904v3wvmm6i9zqfh760q7qya0ab4wsdaxl5mppc2vi2hpnc")))

(define-public crate-toy_arena-0.1 (crate (name "toy_arena") (vers "0.1.3") (deps (list (crate-dep (name "derivative") (req "^2.2.0") (default-features #t) (kind 0)) (crate-dep (name "smallvec") (req "^1.6.1") (default-features #t) (kind 0)))) (hash "1s5wv2fysjq2naagj84yych4z9n0xlwiwbwrfgrvnhmay36q3r86")))

(define-public crate-toy_arena-0.2 (crate (name "toy_arena") (vers "0.2.0") (deps (list (crate-dep (name "derivative") (req "^2.2.0") (default-features #t) (kind 0)))) (hash "1fmxapm57ylw8mcghnip9mrpf2yicvlcnap0pssv5q2i9089zspj")))

(define-public crate-toy_ast-0.1 (crate (name "toy_ast") (vers "0.1.0") (hash "0rh8r755py7yivxq491yf4xi89zh15xra623mgs94k0dhpgxh7x5")))

(define-public crate-toy_codegen-0.1 (crate (name "toy_codegen") (vers "0.1.0") (deps (list (crate-dep (name "toy_ast") (req "0.1.*") (default-features #t) (kind 0)))) (hash "1rczv4ahc44f9z9vm1r3ww4sjgbrqlaawnjdzk8nrbzhafrvp0w1")))

(define-public crate-toy_ml-0.1 (crate (name "toy_ml") (vers "0.1.0") (hash "0aja4qbqd7p05rxcz6vrlw34ajl8jjs2yyw5vc069vnsk3sznla7")))

(define-public crate-toy_pool-0.1 (crate (name "toy_pool") (vers "0.1.0") (deps (list (crate-dep (name "derivative") (req "^2.2.0") (default-features #t) (kind 0)))) (hash "00ggv9i11s0xsjpa0sy8j0qpzl1wcgbwghwjs3c12jgyzr62npkn")))

(define-public crate-toy_pool-0.1 (crate (name "toy_pool") (vers "0.1.1") (deps (list (crate-dep (name "derivative") (req "^2.2.0") (default-features #t) (kind 0)))) (hash "05hixqjcsafngikr7rc2zy5q4wxyldql6fsmjh5yxza4yv4sddnv")))

(define-public crate-toy_runtime-0.1 (crate (name "toy_runtime") (vers "0.1.0") (hash "0ijmykgz49kph7r15vwmrylr5lv4m81kfajy4gzrcw70dnifkx5a")))

(define-public crate-toy_share-0.1 (crate (name "toy_share") (vers "0.1.4") (deps (list (crate-dep (name "bincode") (req "^1.3.3") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.162") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "05xnc633vv37c5yqjskhbxvqc65lgmalhjvrznmm3sd4lq4vrswy")))

(define-public crate-toy_xcb-0.2 (crate (name "toy_xcb") (vers "0.2.0-beta.0") (deps (list (crate-dep (name "bitflags") (req "^1.3.2") (default-features #t) (kind 0)) (crate-dep (name "xcb") (req "^1.0.0-beta") (features (quote ("xlib_xcb" "xkb"))) (default-features #t) (kind 0)) (crate-dep (name "xkbcommon") (req "^0.5.0-beta") (features (quote ("x11"))) (default-features #t) (kind 0)))) (hash "1sjcq1ws7hyzkgi606sxs486q358k8f8ivm80zh90v324yxycrf1")))

