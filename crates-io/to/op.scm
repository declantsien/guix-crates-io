(define-module (crates-io to op) #:use-module (crates-io))

(define-public crate-toopkarcher_minigrep_23984729834792384-0.1 (crate (name "toopkarcher_minigrep_23984729834792384") (vers "0.1.1") (hash "1kc4c4w76apxwzwzs8byz136k0jxj1g97ridp36f2wmgspk6cv6c")))

(define-public crate-tooples-0.1 (crate (name "tooples") (vers "0.1.0") (hash "0mr0hflb4bvydknigk7b2v700baiabmki1fkiir9pj8mcj8vs4lr")))

(define-public crate-tooples-0.2 (crate (name "tooples") (vers "0.2.0") (hash "0avdr37zycs2sran52sk5qhgi1lbg18ci8r4pn9l9dbl5rglhyrp")))

