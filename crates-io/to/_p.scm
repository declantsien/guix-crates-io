(define-module (crates-io to _p) #:use-module (crates-io))

(define-public crate-to_phantom-0.1 (crate (name "to_phantom") (vers "0.1.0") (deps (list (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2.0") (features (quote ("derive" "parsing" "printing"))) (kind 0)))) (hash "1pp54wckhng96vvrs3fgbv8jr3la6pfy8ffvbidmj6qxmcsmy6jd")))

(define-public crate-to_precision-0.1 (crate (name "to_precision") (vers "0.1.0") (hash "00rzykh5yb847cv0y27k03lc8a2g2sw89mbr570vpbf8khmz708i")))

(define-public crate-to_precision-0.1 (crate (name "to_precision") (vers "0.1.1") (hash "0nxil6fjyjbanvnjikvdy42x77gdcgrlglad4kr2r0hc67h7rj4g")))

(define-public crate-to_precision-0.1 (crate (name "to_precision") (vers "0.1.2") (hash "19jsmks4bm079v2ijmaf4kyv6680kxl12pn9ai4vlvxm960igv26")))

