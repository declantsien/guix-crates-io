(define-module (crates-io to -a) #:use-module (crates-io))

(define-public crate-to-arraystring-0.1 (crate (name "to-arraystring") (vers "0.1.0") (deps (list (crate-dep (name "arrayvec") (req "^0.7") (kind 0)) (crate-dep (name "itoa") (req "^1.0.10") (default-features #t) (kind 0)) (crate-dep (name "paste") (req "^1") (default-features #t) (kind 2)) (crate-dep (name "ryu") (req "^1.0.17") (default-features #t) (kind 0)))) (hash "1fvhj50ssgdq735v9hpr6wvmff9xxk89x47mb0lz4fxb96lgp4ib")))

(define-public crate-to-arraystring-0.1 (crate (name "to-arraystring") (vers "0.1.1") (deps (list (crate-dep (name "arrayvec") (req "^0.7") (kind 0)) (crate-dep (name "itoa") (req "^1.0.10") (default-features #t) (kind 0)) (crate-dep (name "paste") (req "^1") (default-features #t) (kind 2)) (crate-dep (name "ryu") (req "^1.0.17") (default-features #t) (kind 0)))) (hash "0qd6mzs8rhlsq5pmx4jivh54918qlg7jlfvbg56r0ipvnw1s9dpb") (rust-version "1.56")))

(define-public crate-to-arraystring-0.1 (crate (name "to-arraystring") (vers "0.1.2") (deps (list (crate-dep (name "arrayvec") (req "^0.7") (kind 0)) (crate-dep (name "itoa") (req "^1.0.10") (default-features #t) (kind 0)) (crate-dep (name "paste") (req "^1") (default-features #t) (kind 2)) (crate-dep (name "ryu") (req "^1.0.17") (default-features #t) (kind 0)))) (hash "10n2akcfndlhyki6wp988cabb77jarfi6pxa2x02flwny64ps6l1") (rust-version "1.56")))

(define-public crate-to-arraystring-0.1 (crate (name "to-arraystring") (vers "0.1.3") (deps (list (crate-dep (name "arrayvec") (req "^0.7") (kind 0)) (crate-dep (name "itoa") (req "^1.0.10") (default-features #t) (kind 0)) (crate-dep (name "paste") (req "^1") (default-features #t) (kind 2)) (crate-dep (name "ryu") (req "^1.0.17") (default-features #t) (kind 0)))) (hash "1d300yb6p0l31y3yh14dv2g47czha4f919ccfqj5q2yb5cj4c2rl") (rust-version "1.56")))

