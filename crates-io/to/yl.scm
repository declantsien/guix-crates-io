(define-module (crates-io to yl) #:use-module (crates-io))

(define-public crate-toylang-0.1 (crate (name "toylang") (vers "0.1.0") (hash "0j1gidlvchdrjh9qj7q7n392zdibnklbr4zm0cii97b9pxb0xdmc")))

(define-public crate-toylang-0.1 (crate (name "toylang") (vers "0.1.1") (hash "0i1bgngx4afz3s4hzln566c62cywa05kca9x7j0jgrndr3dffbfl")))

(define-public crate-toylang-0.1 (crate (name "toylang") (vers "0.1.2") (hash "0x9211lqci96wya64d83ckg5av5fx4z7k901wq8cxmhd808p2w4p")))

(define-public crate-toylang-0.1 (crate (name "toylang") (vers "0.1.3") (deps (list (crate-dep (name "toy_runtime") (req "0.1.*") (default-features #t) (kind 0)) (crate-dep (name "toyc") (req "0.1.*") (default-features #t) (kind 0)))) (hash "0im7z7ca81m6vhjcg3xp0a4agmkm7wy3ilmi22sf2x3l3njsly3a")))

(define-public crate-toylang-0.1 (crate (name "toylang") (vers "0.1.4") (deps (list (crate-dep (name "toy_runtime") (req "0.1.*") (default-features #t) (kind 0)) (crate-dep (name "toyc") (req "0.1.*") (default-features #t) (kind 0)))) (hash "0kfw8x0jxf4nkcdjr9lyn7zw87lkvi4fn2k6d9nksiiz59krzxm7")))

