(define-module (crates-io to p_) #:use-module (crates-io))

(define-public crate-top_n_tail-0.1 (crate (name "top_n_tail") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^3.0.0-beta.2") (default-features #t) (kind 0)))) (hash "0kirwcazxsymk94pl68v5bvv8d65h1bav1f6r01l5s0z3mr32n6i")))

(define-public crate-top_n_tail-0.1 (crate (name "top_n_tail") (vers "0.1.1") (deps (list (crate-dep (name "clap") (req "^3.2.8") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "0lrb9xvsh501a49rmgz2mpccfjjncdqnbr5jw677vmvkqgsvb76b")))

