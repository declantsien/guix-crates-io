(define-module (crates-io to oe) #:use-module (crates-io))

(define-public crate-tooey-0.1 (crate (name "tooey") (vers "0.1.0") (deps (list (crate-dep (name "keyboard-types") (req "^0.6.2") (default-features #t) (kind 0)))) (hash "1vx1i6qbkh1gi833cgiicf237pxans87v313q1cig9nzkbfvsi5g") (features (quote (("std") ("no_std")))) (yanked #t)))

(define-public crate-tooey-0.1 (crate (name "tooey") (vers "0.1.1") (deps (list (crate-dep (name "keyboard-types") (req "^0.6.2") (default-features #t) (kind 0)))) (hash "1dj7h087jdhimjqinnb1l4h63qz50facfnq3jza94kwxp4icy9rj") (features (quote (("std") ("no_std"))))))

(define-public crate-tooey-0.1 (crate (name "tooey") (vers "0.1.2") (deps (list (crate-dep (name "keyboard-types") (req "^0.6.2") (default-features #t) (kind 0)))) (hash "0kc9ahd47k42zqqxgc1pzhr7673d29bi3fm7mvzyxvxbv68dkr2w") (features (quote (("std") ("no_std"))))))

(define-public crate-tooey-0.1 (crate (name "tooey") (vers "0.1.3") (deps (list (crate-dep (name "keyboard-types") (req "^0.6.2") (kind 0)))) (hash "15n6yikaf85pp5csyz3yy34v1r1gizxclh2w5b39152q9hq948ib") (features (quote (("std") ("no_std"))))))

(define-public crate-tooey-0.1 (crate (name "tooey") (vers "0.1.4") (hash "0haxqza360bq12rgxibw3mb544y04b0v044ignpwbran722gr6x8") (features (quote (("std") ("no_std")))) (yanked #t)))

(define-public crate-tooey-0.2 (crate (name "tooey") (vers "0.2.0") (hash "0xa92gckv2zjdna7x61nw79p9yq4h47329lx4jc8v2dlykh17msq") (features (quote (("std") ("no_std"))))))

(define-public crate-tooey-0.3 (crate (name "tooey") (vers "0.3.0") (hash "15lq9y304i2s6yy0z23s1bisha9l8knw1q0sdvc46xg0ijl6n54f") (features (quote (("std") ("no_std"))))))

(define-public crate-tooey-0.4 (crate (name "tooey") (vers "0.4.0") (deps (list (crate-dep (name "ndarray") (req "^0.15.6") (default-features #t) (kind 0)))) (hash "0mihbcyn1zmg1105ba1d1w8sx15k24yzp1ijvdbf2x9yjwmxmlx2") (features (quote (("std") ("no_std")))) (yanked #t)))

(define-public crate-tooey-0.4 (crate (name "tooey") (vers "0.4.1") (deps (list (crate-dep (name "ndarray") (req "^0.15.6") (default-features #t) (kind 0)))) (hash "13p7vgkfaxrxzdrwm0lc73ax5rnj1yl0jnirhz60683pczhzyzpl") (features (quote (("std") ("no_std")))) (yanked #t)))

(define-public crate-tooey-0.4 (crate (name "tooey") (vers "0.4.2") (deps (list (crate-dep (name "ndarray") (req "^0.15.6") (kind 0)))) (hash "0mr8gnwvk5jixbngr7vdrglbyl53a6vj5wjyix54wis7xp0xxb33") (features (quote (("std") ("no_std"))))))

(define-public crate-tooey-0.5 (crate (name "tooey") (vers "0.5.0") (deps (list (crate-dep (name "ndarray") (req "^0.15.6") (kind 0)))) (hash "00a4iv1hz3j16gzgw0is4saq2h830p8vpdcapqi9i52ai82m6gbz") (features (quote (("std") ("no_std"))))))

(define-public crate-tooey-0.6 (crate (name "tooey") (vers "0.6.0") (deps (list (crate-dep (name "log") (req "^0.4.17") (default-features #t) (kind 0)) (crate-dep (name "ndarray") (req "^0.15.6") (kind 0)))) (hash "19q9hzjc388bb7wxzfsjgq8h5z6pz42x9inidmn82z6kgy8mfxdr") (features (quote (("std") ("no_std"))))))

(define-public crate-tooey-0.7 (crate (name "tooey") (vers "0.7.0") (deps (list (crate-dep (name "log") (req "^0.4.17") (default-features #t) (kind 0)) (crate-dep (name "ndarray") (req "^0.15.6") (kind 0)))) (hash "02mw65qlylza9n8vwmfxswggxvf4n8nakkama263ia0bfn235hsz") (features (quote (("std") ("no_std"))))))

(define-public crate-tooey-0.7 (crate (name "tooey") (vers "0.7.1") (deps (list (crate-dep (name "log") (req "^0.4.17") (default-features #t) (kind 0)) (crate-dep (name "ndarray") (req "^0.15.6") (kind 0)))) (hash "0z0cwqk9ddzhblvlasg2hhjc6wfmzny1dp54q1cdrcaqbn4w32qa") (features (quote (("std") ("objects") ("no_std") ("default" "objects"))))))

