(define-module (crates-io to rd) #:use-module (crates-io))

(define-public crate-tord-0.1 (crate (name "tord") (vers "0.1.0") (hash "0cagl8qrcadg8p6hxkvv8bg90ncvw8ca8nbaijn2k9hh7zxbpymg")))

(define-public crate-tord-0.1 (crate (name "tord") (vers "0.1.1") (hash "0d6vp3l7jhbkry824nrz0y9s8bkkdsk5p3p5b6s48n3dfn8nsbg5")))

(define-public crate-tord-0.1 (crate (name "tord") (vers "0.1.2") (hash "1x1nng9fn1hvl0jzh95x20xjv1yz9s7achn79xvx14pajnkk4lm5")))

(define-public crate-tord-0.1 (crate (name "tord") (vers "0.1.3") (hash "1m3iqfml54p4shsbvjzq198bx7pr1mqzqi5wx7qix02id6cgnc8k")))

(define-public crate-tord-0.1 (crate (name "tord") (vers "0.1.4") (hash "0sr6js5l5bhj01w7vv2ph8gwcc5mf7n7rf1l6qi146cxw4g1qr40")))

(define-public crate-tord-0.1 (crate (name "tord") (vers "0.1.5") (hash "0q1bzbmp3c4g99qvgm52y461ncb3ygndx2dj9br8ll6vhabhh5vs")))

(define-public crate-tord-0.1 (crate (name "tord") (vers "0.1.6") (hash "0xm3p73nar9ips7r68jmqhvz7m4lgspifpf82vlwzlava2zdg9q0")))

(define-public crate-tord-0.1 (crate (name "tord") (vers "0.1.7") (hash "1shd1d9bisqn6m4bxbcb0kgc5q6r95hvblv6c7x2vfhcnp7znjy4")))

(define-public crate-tordir-0.1 (crate (name "tordir") (vers "0.1.0") (hash "05f7c8rg3qv0b9a2m1z17fd0qhdm1wxyvn60rmricskk1gcxs4rh")))

(define-public crate-tordir-0.1 (crate (name "tordir") (vers "0.1.1") (hash "0hkgw7ahr4cgz6cv67wxz6ff8jkdd6ijhkvp9mwds63wqga85a76")))

(define-public crate-tordir-0.1 (crate (name "tordir") (vers "0.1.2") (hash "1z3hmp2ijawls9rfg9hsm44x6ywkxc7y5qxdr6zlgfmq2w8pvdnh")))

(define-public crate-tordir-0.1 (crate (name "tordir") (vers "0.1.3") (hash "128cbl0m0wqlwysa0ydhyqs88f75bcivl2djrwhw61jxmwrcyb62")))

