(define-module (crates-io to rs) #:use-module (crates-io))

(define-public crate-tors-0.1 (crate (name "tors") (vers "0.1.0") (hash "190sllwwc6dn2gi1412w44zd7cas50pxb21l5gvdzmi3667hmkbc")))

(define-public crate-tors_to_json-0.1 (crate (name "tors_to_json") (vers "0.1.0") (deps (list (crate-dep (name "regex") (req "^1.9.4") (default-features #t) (kind 0)))) (hash "1pdilz8dzqvjnx3wp9sgzqmr0i8d806dbf4h95hmyj7mffs8m4yc")))

(define-public crate-tors_to_json-0.2 (crate (name "tors_to_json") (vers "0.2.0") (deps (list (crate-dep (name "regex") (req "^1.9.4") (default-features #t) (kind 0)))) (hash "0s9im8aqb9cppinhx1xngljybzxarf0czjck1r8ari7hbibi5kjz")))

(define-public crate-tors_to_json-0.2 (crate (name "tors_to_json") (vers "0.2.1") (deps (list (crate-dep (name "regex") (req "^1.9.4") (default-features #t) (kind 0)))) (hash "0m4gxh0j84bhs8x8v68bcs2sdnx9dzk9zki8sh8ndh5gvyk72px1")))

(define-public crate-tors_to_json-0.2 (crate (name "tors_to_json") (vers "0.2.2") (deps (list (crate-dep (name "regex") (req "^1.9.4") (default-features #t) (kind 0)))) (hash "0h08128lmb6zcmxmsw08sdx03szs41l6bz2d15pq86shb5a93qpa")))

