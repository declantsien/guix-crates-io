(define-module (crates-io to zn) #:use-module (crates-io))

(define-public crate-tozny_auth-1 (crate (name "tozny_auth") (vers "1.1.0") (deps (list (crate-dep (name "chrono") (req "~0.2.1") (default-features #t) (kind 0)) (crate-dep (name "hyper") (req "~0.3.0") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "~0.2.1") (default-features #t) (kind 0)) (crate-dep (name "rust-crypto") (req "~0.2.15") (default-features #t) (kind 0)) (crate-dep (name "rustc-serialize") (req "~0.3.1") (default-features #t) (kind 0)) (crate-dep (name "url") (req "~0.2.18") (default-features #t) (kind 0)))) (hash "1q7xmw4h4hlihz8qcwfgphyn9qi9k7w9g9yfwh5prjzw7ipay2kp")))

