(define-module (crates-io to k-) #:use-module (crates-io))

(define-public crate-tok-grammar-0.1 (crate (name "tok-grammar") (vers "0.1.0") (deps (list (crate-dep (name "lalrpop") (req "^0.19") (features (quote ("lexer"))) (default-features #t) (kind 1)) (crate-dep (name "lalrpop-util") (req "^0.19") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.5") (default-features #t) (kind 0)))) (hash "0h1smmh5l4sx5bvgccqn0cn48m07fy2g1vyrhqadbvw8m66q71q4")))

(define-public crate-tok-grammar-0.1 (crate (name "tok-grammar") (vers "0.1.1") (deps (list (crate-dep (name "lalrpop") (req "^0.19") (features (quote ("lexer"))) (default-features #t) (kind 1)) (crate-dep (name "lalrpop-util") (req "^0.19") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.5") (default-features #t) (kind 0)))) (hash "1raa52nvwnzhpvhk7mq9hyxacnrnakqbyq1ssy1046qbdf66rkjs")))

(define-public crate-tok-grammar-0.1 (crate (name "tok-grammar") (vers "0.1.2") (deps (list (crate-dep (name "lalrpop") (req "^0.19") (features (quote ("lexer"))) (default-features #t) (kind 1)) (crate-dep (name "lalrpop-util") (req "^0.19") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.5") (default-features #t) (kind 0)))) (hash "0hf7dpwmr6v2ffq859klicbf834g686mvcwbmjy1df3ichb9639l")))

