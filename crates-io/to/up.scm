(define-module (crates-io to up) #:use-module (crates-io))

(define-public crate-touptek-1 (crate (name "touptek") (vers "1.0.0") (deps (list (crate-dep (name "bitflags") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "*") (default-features #t) (kind 0)))) (hash "0a0qy4dnjfmj76w05hf00x4hyzkcy885908gw5fjfzjjr7b0rjw1")))

(define-public crate-touptek-1 (crate (name "touptek") (vers "1.1.0") (deps (list (crate-dep (name "bitflags") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "*") (default-features #t) (kind 0)))) (hash "0ig4pgd0w6b2l65nfcd5bfqp30kqk1svfzsx2i3zxq8mfg2b0wv4")))

