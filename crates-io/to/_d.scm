(define-module (crates-io to _d) #:use-module (crates-io))

(define-public crate-to_debug-0.1 (crate (name "to_debug") (vers "0.1.0") (hash "11l44yfi3dgd2y64yar8yn8bnhncbhs8avli5n5c7c3369rp6ybh")))

(define-public crate-to_default-0.1 (crate (name "to_default") (vers "0.1.0") (hash "1x2sqb5c5mrawqzlwh5s5gpsgjcx08jdgrqmpdl23w24gpyj1cmm")))

(define-public crate-to_default-0.1 (crate (name "to_default") (vers "0.1.1") (hash "1x656m8bd5nwh89c1hr8rg36q6sj17b5ghpv6dx9y682ipjmq5jk")))

(define-public crate-to_default-0.1 (crate (name "to_default") (vers "0.1.2") (hash "0z2samkd3mb7h901x7d2snpc0qq7c3x2qv6m168f0vpxghznbhbx")))

(define-public crate-to_done-0.1 (crate (name "to_done") (vers "0.1.0") (deps (list (crate-dep (name "chrono") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "chrono-english") (req "^0.1.4") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^2.33.0") (default-features #t) (kind 0)) (crate-dep (name "dirs") (req "^2.0") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "prettytable-rs") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "termion") (req "^1.5.5") (default-features #t) (kind 0)))) (hash "1ain6fdgvc75azhww93qc9vcp9s6a56xvm9q0lcvwh8c8glsmkyg")))

(define-public crate-to_done-1 (crate (name "to_done") (vers "1.0.0") (deps (list (crate-dep (name "chrono") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "chrono-english") (req "^0.1.4") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^2.33.0") (default-features #t) (kind 0)) (crate-dep (name "dirs") (req "^2.0") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "prettytable-rs") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "termion") (req "^1.5.5") (default-features #t) (kind 0)))) (hash "057iaxzywz1g8vndgcq2asnzi8hx77c3if848y0mpj6j2m1halw5")))

