(define-module (crates-io to _m) #:use-module (crates-io))

(define-public crate-to_markdown_table-0.1 (crate (name "to_markdown_table") (vers "0.1.1") (deps (list (crate-dep (name "thiserror") (req "^1.0.58") (default-features #t) (kind 0)))) (hash "0m5giaawvlnib16zjax9kwblp8nc8lajimys43ljp4szl2v9n5s1")))

(define-public crate-to_method-1 (crate (name "to_method") (vers "1.0.0") (hash "1mwyknl4bwdbd0lgjhswzahh060z6cy43jb0npf0i6rw8yrr0p91")))

(define-public crate-to_method-1 (crate (name "to_method") (vers "1.1.0") (hash "1s72l06fnb5kv6vm5ds0lilg1dyciyyis09ypi5kij0mrbpcxi67")))

