(define-module (crates-io to xe) #:use-module (crates-io))

(define-public crate-toxearley-0.0.3 (crate (name "toxearley") (vers "0.0.3") (deps (list (crate-dep (name "lexers") (req "^0.0.4") (default-features #t) (kind 0)) (crate-dep (name "linenoise-rust") (req "^0.2") (default-features #t) (kind 2)) (crate-dep (name "regex") (req "^0.1") (default-features #t) (kind 2)))) (hash "0ipa3nbx79nkyx7c0q3whc9rpmkyrqnpaxx4przgvij2qpky0qs5")))

