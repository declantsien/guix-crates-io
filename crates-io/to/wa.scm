(define-module (crates-io to wa) #:use-module (crates-io))

(define-public crate-towa-core-0.1 (crate (name "towa-core") (vers "0.1.0") (deps (list (crate-dep (name "base64") (req "^0.21.5") (default-features #t) (kind 0)) (crate-dep (name "byteorder") (req "^1.5.0") (default-features #t) (kind 0)) (crate-dep (name "sha2") (req "^0.10.8") (default-features #t) (kind 0)) (crate-dep (name "threefish") (req "^0.5.2") (default-features #t) (kind 0)))) (hash "1zxqd3mz1h2vcw2q1iqh14xgy4fcnnfnkg7v00xwswawd9l5if9j")))

