(define-module (crates-io to ul) #:use-module (crates-io))

(define-public crate-touls-0.1 (crate (name "touls") (vers "0.1.0") (deps (list (crate-dep (name "borgflux") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^4.3.23") (features (quote ("derive" "cargo"))) (default-features #t) (kind 0)) (crate-dep (name "wakey-wakey") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "0jpan2fmf08w185il0ggbch79j9bwx7rspqz18kyvvc70zi2p5rv")))

