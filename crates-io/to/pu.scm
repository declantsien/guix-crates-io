(define-module (crates-io to pu) #:use-module (crates-io))

(define-public crate-topus-0.1 (crate (name "topus") (vers "0.1.0") (hash "1lww712rgw1j859ypy35m12pl758rni1m6p67p9prkcfskf2kjc7")))

(define-public crate-topus-0.1 (crate (name "topus") (vers "0.1.1") (hash "0ann2flzxqmyrkww8qaqivbzli8sr7i31w0iasa7fzi7yk8a19js")))

(define-public crate-topus-0.1 (crate (name "topus") (vers "0.1.2") (hash "1hj355j68ippdgmhqd3jg7mqrywsd7d47g802bs0wl0scaqy4qxw")))

(define-public crate-topus-0.1 (crate (name "topus") (vers "0.1.3") (hash "1v9206hc50m8wfsf003jqkjiszpns9i0w7wajjx6npl5l9sl7d92")))

(define-public crate-topus-0.1 (crate (name "topus") (vers "0.1.4") (hash "1bxc1mxm204ccawnysw3aj6qxznw6imnfmr7knbr55n4xwi2x2s3")))

(define-public crate-topus-0.1 (crate (name "topus") (vers "0.1.5") (hash "1ygzsgybh05p3g8gjbmyk19gp9vgv2lq1p9j2blj8kaw700llkn5")))

(define-public crate-topus-0.1 (crate (name "topus") (vers "0.1.6") (hash "1g92p1pvkwbrlmpgv2xw0wjkpxphhfs92ci2dvc3ldkh9gsapnhg")))

