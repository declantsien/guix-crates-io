(define-module (crates-io to ck) #:use-module (crates-io))

(define-public crate-tock-0.1 (crate (name "tock") (vers "0.1.0") (deps (list (crate-dep (name "chrono") (req "^0.4.6") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2.58") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.2.16") (default-features #t) (kind 0)))) (hash "0y01g0n2kifylf9v06jf2cp4m4xa7c24b7w6impghg3js99bcpw4") (features (quote (("interactive") ("default" "interactive"))))))

(define-public crate-tock-0.1 (crate (name "tock") (vers "0.1.1") (deps (list (crate-dep (name "chrono") (req "^0.4.6") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2.58") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.2.16") (default-features #t) (kind 0)))) (hash "0q9zppignr40c30620kcwylkmslpycw0hj9l7gz4jxwagj5dmga9") (features (quote (("interactive") ("default" "interactive"))))))

(define-public crate-tock-0.1 (crate (name "tock") (vers "0.1.2") (deps (list (crate-dep (name "chrono") (req "^0.4.6") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2.58") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.2.16") (default-features #t) (kind 0)))) (hash "0jp050a2zxhdvsxllq1acigd7cdn60wlr4ap98v0h8fi653h34na") (features (quote (("interactive") ("default" "interactive"))))))

(define-public crate-tock-0.1 (crate (name "tock") (vers "0.1.3") (deps (list (crate-dep (name "chrono") (req "^0.4.6") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2.58") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.2.16") (default-features #t) (kind 0)))) (hash "1ibh4fy7s3hw2niyvhxxcdfz11yfx3nbcjbymrdffiavm0lcqzbl") (features (quote (("interactive") ("default" "interactive"))))))

(define-public crate-tock-0.1 (crate (name "tock") (vers "0.1.4") (deps (list (crate-dep (name "chrono") (req "^0.4.6") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2.58") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.2.16") (default-features #t) (kind 0)))) (hash "0lki1h64ssgwllgdi210f6zrhwxkn3mqaadlk9l0v7d30p4yw6nd") (features (quote (("interactive") ("default" "interactive"))))))

(define-public crate-tock-1 (crate (name "tock") (vers "1.0.0") (deps (list (crate-dep (name "chrono") (req "^0.4") (features (quote ("clock" "std"))) (kind 0)) (crate-dep (name "clap") (req "^4.4") (features (quote ("color" "derive" "help" "std" "usage"))) (kind 0)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "1hlpbl0nwzhssh47s3p3m54vhvx37ay7giriiadp1dcjpirs94rr") (features (quote (("interactive") ("default" "interactive"))))))

(define-public crate-tock-1 (crate (name "tock") (vers "1.0.1") (deps (list (crate-dep (name "chrono") (req "^0.4") (features (quote ("clock" "std"))) (kind 0)) (crate-dep (name "clap") (req "^4.4") (features (quote ("color" "derive" "help" "std" "usage"))) (kind 0)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "1phlibd786afz9p3grjh1yxijczr6mig6hv2alcdh78n5ml9dgk1") (features (quote (("interactive") ("default" "interactive"))))))

(define-public crate-tock-registers-0.1 (crate (name "tock-registers") (vers "0.1.0") (hash "1adpl6gqjyw5z8941c1sw5aymhgv6w32v8rls8cfwg72h3wk7k1a")))

(define-public crate-tock-registers-0.2 (crate (name "tock-registers") (vers "0.2.0") (hash "05p3i27wk3l41x60slrl48ksl7v2v7w9znqab9260bpnyfa5sf1s")))

(define-public crate-tock-registers-0.3 (crate (name "tock-registers") (vers "0.3.0") (hash "1qn3lmhjfzn8jplv2kgn9kvnyxmj0rjm0vygzvczj39fb8czan67")))

(define-public crate-tock-registers-0.4 (crate (name "tock-registers") (vers "0.4.0") (hash "0g6qdd0nmkz1gpvps0ncnb03fb11v9ns8pkwzzi665ybs0gpalpf")))

(define-public crate-tock-registers-0.4 (crate (name "tock-registers") (vers "0.4.1") (hash "0w3ac01kaa3ih7dl39myw75qil757gdgmhbqvlvl9yswqm4rrbsh")))

(define-public crate-tock-registers-0.5 (crate (name "tock-registers") (vers "0.5.0") (hash "03y2v839i3g9jw2w08f9h7b07lafdq7y383dk308c888p3yklckh") (features (quote (("no_std_unit_tests"))))))

(define-public crate-tock-registers-0.6 (crate (name "tock-registers") (vers "0.6.0") (hash "1shcxvzvpdg9wbvk46fs4rhn4sq510hy4z67r4bw8s6frjdaf8gm") (features (quote (("no_std_unit_tests"))))))

(define-public crate-tock-registers-0.7 (crate (name "tock-registers") (vers "0.7.0") (hash "0bi8wrhq3955qn5vqf0wqq6qzcmn1d9m86pndqwhnk8zdjhgps2f") (features (quote (("std_unit_tests") ("register_types") ("default" "register_types" "std_unit_tests"))))))

(define-public crate-tock-registers-0.8 (crate (name "tock-registers") (vers "0.8.0") (hash "1dc3fmvdrhic6mikhij2nshdcss4z1zpwdgnqgxz3d91pxn9hnm1") (features (quote (("register_types") ("default" "register_types"))))))

(define-public crate-tock-registers-0.8 (crate (name "tock-registers") (vers "0.8.1") (hash "077jq2lhq1qkg0cxlsrxbk2j4pgx31wv6y59cnhpdqp7msh42sb9") (features (quote (("register_types") ("default" "register_types"))))))

(define-public crate-tock-registers-0.9 (crate (name "tock-registers") (vers "0.9.0") (hash "1i4kil3lg6z0xzxwp2ksjkqiv0chag97x2vnc432r1hy7bdjz7ib") (features (quote (("register_types") ("default" "register_types"))))))

(define-public crate-tocket-0.1 (crate (name "tocket") (vers "0.1.0") (deps (list (crate-dep (name "criterion") (req "^0.3.5") (default-features #t) (kind 2)) (crate-dep (name "crossbeam") (req "^0.8.1") (default-features #t) (kind 2)) (crate-dep (name "parking_lot") (req "^0.12") (default-features #t) (kind 0)) (crate-dep (name "redis") (req "^0.21") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0") (default-features #t) (kind 0)))) (hash "13p4vvciyfqijjf36zn0l4pgvp0fgnm48dnqrdq5dadkx4zizfq5") (features (quote (("redis-impl") ("dev-redis-impl") ("default" "redis-impl"))))))

(define-public crate-tocket-0.1 (crate (name "tocket") (vers "0.1.1") (deps (list (crate-dep (name "criterion") (req "^0.3.5") (default-features #t) (kind 2)) (crate-dep (name "crossbeam") (req "^0.8.1") (default-features #t) (kind 2)) (crate-dep (name "parking_lot") (req "^0.12") (default-features #t) (kind 0)) (crate-dep (name "redis") (req "^0.21") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0") (default-features #t) (kind 0)))) (hash "08rqx29xm92g3lfvkzp9l97jvpwpsana5a8hv0g6a1w489fsb5xb") (features (quote (("redis-impl") ("dev-redis-impl") ("default" "redis-impl"))))))

(define-public crate-tocket-0.2 (crate (name "tocket") (vers "0.2.0") (deps (list (crate-dep (name "async-trait") (req "^0.1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "borsh") (req "^0.9") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "bytes") (req "^1.1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "crc32fast") (req "^1.3") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "criterion") (req "^0.3.5") (default-features #t) (kind 2)) (crate-dep (name "crossbeam") (req "^0.8.1") (default-features #t) (kind 2)) (crate-dep (name "futures") (req "^0.3") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "parking_lot") (req "^0.12") (default-features #t) (kind 0)) (crate-dep (name "redis") (req "^0.21") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "time") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.17") (features (quote ("net" "rt" "macros" "sync"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.17") (features (quote ("full"))) (default-features #t) (kind 2)) (crate-dep (name "tokio-util") (req "^0.7") (features (quote ("codec" "net"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "tracing") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "uuid") (req "^1.0") (features (quote ("v4"))) (default-features #t) (kind 2)))) (hash "17fhfx2xn56hsj52f9nj1jvlvb82sjnvz4dw4syl2byqh9gqdfcx") (features (quote (("redis-impl" "redis") ("distributed-impl" "async-trait" "borsh" "bytes" "crc32fast" "futures" "tokio" "tokio-util") ("default")))) (yanked #t)))

(define-public crate-tocket-0.2 (crate (name "tocket") (vers "0.2.1") (deps (list (crate-dep (name "async-trait") (req "^0.1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "borsh") (req "^0.9") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "bytes") (req "^1.1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "crc32fast") (req "^1.3") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "criterion") (req "^0.3.5") (default-features #t) (kind 2)) (crate-dep (name "crossbeam") (req "^0.8.1") (default-features #t) (kind 2)) (crate-dep (name "futures") (req "^0.3") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "parking_lot") (req "^0.12") (default-features #t) (kind 0)) (crate-dep (name "redis") (req "^0.21") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "time") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.17") (features (quote ("net" "rt" "macros" "sync"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.17") (features (quote ("full"))) (default-features #t) (kind 2)) (crate-dep (name "tokio-util") (req "^0.7") (features (quote ("codec" "net"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "tracing") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "uuid") (req "^1.0") (features (quote ("v4"))) (default-features #t) (kind 2)))) (hash "1caps5nfhb0a475k7x49kch6vd7jdn0fzi6yrlkc12ldc2557572") (features (quote (("redis-impl" "redis") ("distributed-impl" "async-trait" "borsh" "bytes" "crc32fast" "futures" "tokio" "tokio-util") ("default"))))))

(define-public crate-tockloader-proto-0.1 (crate (name "tockloader-proto") (vers "0.1.0") (deps (list (crate-dep (name "byteorder") (req "^1") (kind 0)))) (hash "1yb2w6gcsfj92wm7j73cmcbvkhr32gym5ra34nl4yac9204abkbk")))

(define-public crate-tockloader-proto-0.1 (crate (name "tockloader-proto") (vers "0.1.1") (deps (list (crate-dep (name "byteorder") (req "^1") (kind 0)))) (hash "11hrm2sgkb87qbl67qhdq4sf7yza3100973126r1cwxpl6bxzm76")))

(define-public crate-tockloader-proto-0.2 (crate (name "tockloader-proto") (vers "0.2.1") (deps (list (crate-dep (name "byteorder") (req "^1") (kind 0)))) (hash "1n02q4y0radrb93pmwcq650z4ysyg1r5x7xv4a1mz8s1w2fwm5w9")))

(define-public crate-tocks-0.1 (crate (name "tocks") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^2") (default-features #t) (kind 0)))) (hash "0apm52v1dgjj9b1c50grjmk7gkyxkcl8w111rg756s4x57kqimdj") (yanked #t)))

