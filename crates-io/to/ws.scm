(define-module (crates-io to ws) #:use-module (crates-io))

(define-public crate-tows-1 (crate (name "tows") (vers "1.0.0") (deps (list (crate-dep (name "clap") (req "^2.33.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.104") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.48") (default-features #t) (kind 0)) (crate-dep (name "termion") (req "^1.5.5") (default-features #t) (kind 0)))) (hash "0rs0zfn1y12mjbrmjcb0rry7rxn5d5fanl8n4l4zys3szy7jx5gr")))

(define-public crate-tows-1 (crate (name "tows") (vers "1.0.1") (deps (list (crate-dep (name "clap") (req "^2.33.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.104") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.48") (default-features #t) (kind 0)) (crate-dep (name "termion") (req "^1.5.5") (default-features #t) (kind 0)))) (hash "0427mhbw8si0znz2nnbwfapvh7q8bslxivpj4z1mmnycinxixz69")))

(define-public crate-tows-1 (crate (name "tows") (vers "1.0.2") (deps (list (crate-dep (name "clap") (req "^2.33.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.104") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.48") (default-features #t) (kind 0)) (crate-dep (name "termion") (req "^1.5.5") (default-features #t) (kind 0)))) (hash "097d1vcwnvlc5a0mi5yixcdj4hpw73sar1dxib17sfdm59pi8lv0")))

