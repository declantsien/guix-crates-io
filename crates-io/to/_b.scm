(define-module (crates-io to _b) #:use-module (crates-io))

(define-public crate-to_bopomofo-0.1 (crate (name "to_bopomofo") (vers "0.1.0") (deps (list (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)))) (hash "0ya8ds4lx7y96kp3gyn6ry6b8jn2mqcgcx2ly8bwhn7i7nlm50m7")))

(define-public crate-to_bopomofo-0.1 (crate (name "to_bopomofo") (vers "0.1.1") (deps (list (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)))) (hash "0hfp5fzwb7dxckxl50ljzh8m82g53wy9nkm8x05iim461nd169nn")))

(define-public crate-to_bopomofo-0.1 (crate (name "to_bopomofo") (vers "0.1.2") (deps (list (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)))) (hash "0q5j0wsn3q210zvs76ww6594kk4glg9bw6ppgbg5czlx0kj6x87v")))

(define-public crate-to_bytes-0.1 (crate (name "to_bytes") (vers "0.1.0") (deps (list (crate-dep (name "to_bytes_derive") (req "^0.1") (optional #t) (default-features #t) (kind 0)))) (hash "09p85jng20agi38z0mmlfxjlll542bxy314d8qv6skzm94il4g88") (features (quote (("derive" "to_bytes_derive") ("default" "derive"))))))

(define-public crate-to_bytes_derive-0.1 (crate (name "to_bytes_derive") (vers "0.1.0") (deps (list (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (default-features #t) (kind 0)))) (hash "1nrm8vgi79d9i2v664f5jf4vyqh73ngn9vjaidh9va5xmpcgcv9x")))

