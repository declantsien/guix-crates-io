(define-module (crates-io to yv) #:use-module (crates-io))

(define-public crate-toyvm-0.1 (crate (name "toyvm") (vers "0.1.0") (hash "19574nmdv5b487l1hqhd5dlc4bvdiqz3nzjyyw7i2gq2wvd9cbg3")))

(define-public crate-toyvm-0.1 (crate (name "toyvm") (vers "0.1.1") (hash "12yidw7x38fr7xs8p229y80f0xc6qd9g8wwbswh8wxbca38x09qq")))

(define-public crate-toyvm-0.1 (crate (name "toyvm") (vers "0.1.2") (hash "1nckq58i1hv14di1jyi9p9sldcl3k39nwjys6phqscnspah2yyd4")))

(define-public crate-toyvm-0.1 (crate (name "toyvm") (vers "0.1.3") (deps (list (crate-dep (name "toy_runtime") (req "0.1.*") (default-features #t) (kind 0)))) (hash "00dfha0djqyzrbx9bi9r68mjrhsvma8graax509j339mhnlg5wqm")))

(define-public crate-toyvm-0.1 (crate (name "toyvm") (vers "0.1.4") (deps (list (crate-dep (name "toy_runtime") (req "0.1.*") (default-features #t) (kind 0)))) (hash "0rpwgbcffhm88g88vs4i2xi92ffrmw3grnwyckjc3v25nkdjslzk")))

