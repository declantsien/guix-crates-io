(define-module (crates-io to ca) #:use-module (crates-io))

(define-public crate-toca-0.1 (crate (name "toca") (vers "0.1.0") (deps (list (crate-dep (name "device_query") (req "^1.1.1") (default-features #t) (kind 0)) (crate-dep (name "enigo") (req "^0.0.14") (default-features #t) (kind 0)))) (hash "1c3pz8360z3msb1qzq26vyrihy2zx25y3ycnynv6amdnr3yb9zwg")))

(define-public crate-toca-0.1 (crate (name "toca") (vers "0.1.1") (deps (list (crate-dep (name "device_query") (req "^1.1.1") (default-features #t) (kind 0)) (crate-dep (name "enigo") (req "^0.0.14") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)))) (hash "07p25s39qh1crbj0zha51blx3ifwa3h2rk05jv3fgxqjc3z8fyl0")))

(define-public crate-tocall-0.2 (crate (name "tocall") (vers "0.2.0") (hash "1pkanv4b5zyfz56b50dnxm1y237qlc0r8h0mprayqfrnfs3sbp8c")))

