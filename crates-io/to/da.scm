(define-module (crates-io to da) #:use-module (crates-io))

(define-public crate-todate-0.1 (crate (name "todate") (vers "0.1.0") (deps (list (crate-dep (name "chrono") (req "^0.4.31") (default-features #t) (kind 0)))) (hash "17f4j51bi6hkz7k89qqy2qiy29f405y0srzhxkybfxpabvx802ss")))

(define-public crate-todate-0.1 (crate (name "todate") (vers "0.1.1") (deps (list (crate-dep (name "chrono") (req "^0.4.31") (default-features #t) (kind 0)))) (hash "0g54s7f2kawjs641kyvrwdk6rr6nfzm2hzc0x6wcrlf5b52c8v8m")))

(define-public crate-today-0.0.0 (crate (name "today") (vers "0.0.0") (hash "1vcld4grrjwfq2l5ca8cgzy89k9clkb3jq37fs5jw8wy4zj1lg45")))

(define-public crate-today_utils-0.1 (crate (name "today_utils") (vers "0.1.0") (hash "05vb8rcjsy1kb9mqlzpv93bj1v2bazd2f8hvf3gyva14wp0348zq")))

