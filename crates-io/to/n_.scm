(define-module (crates-io to n_) #:use-module (crates-io))

(define-public crate-ton_smart_contract_address-0.0.0 (crate (name "ton_smart_contract_address") (vers "0.0.0") (deps (list (crate-dep (name "base64") (req "^0.20.0") (default-features #t) (kind 0)))) (hash "1va32fddp0cr7m0xhnm45gqy9yrzdqdy08mdk6s0j5lnrp5i2hzp")))

(define-public crate-ton_smart_contract_address-0.0.1 (crate (name "ton_smart_contract_address") (vers "0.0.1") (deps (list (crate-dep (name "base64") (req "^0.20.0") (default-features #t) (kind 0)) (crate-dep (name "crc16") (req "^0.4.0") (default-features #t) (kind 0)))) (hash "1rl05vyf7n0m7qcpq4172cl15k3svsxp6mm6l8hrnfm2l2q7ijh7")))

(define-public crate-ton_smart_contract_address-0.0.2 (crate (name "ton_smart_contract_address") (vers "0.0.2") (deps (list (crate-dep (name "base64") (req "^0.20.0") (default-features #t) (kind 0)) (crate-dep (name "crc16") (req "^0.4.0") (default-features #t) (kind 0)))) (hash "0vninlk5i6v2bwf2a38q19llkbr2n4m3d80ijkb2dphc29j48n3j")))

(define-public crate-ton_smart_contract_address-0.0.3 (crate (name "ton_smart_contract_address") (vers "0.0.3") (deps (list (crate-dep (name "base64") (req "^0.20.0") (default-features #t) (kind 0)) (crate-dep (name "crc16") (req "^0.4.0") (default-features #t) (kind 0)))) (hash "1l4h7j2kfgnixa3hw7b9i4lfxcg63zamccy3fn81zw2z548r6vrp")))

(define-public crate-ton_smart_contract_address-0.0.4 (crate (name "ton_smart_contract_address") (vers "0.0.4") (deps (list (crate-dep (name "base64") (req "^0.20.0") (default-features #t) (kind 0)) (crate-dep (name "crc16") (req "^0.4.0") (default-features #t) (kind 0)))) (hash "19vllrgdvmnzy6x1wa3hr712vq0ynkqsif6kaxr70fxw9vscmy61")))

