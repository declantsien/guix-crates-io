(define-module (crates-io to ns) #:use-module (crates-io))

(define-public crate-tonsoe-0.0.1 (crate (name "tonsoe") (vers "0.0.1-alpha") (hash "1jd7ln8cgrqsl2wh0v9lkk6r9m90rwywmd9xvjihcm3g5cwbg3vw") (yanked #t)))

(define-public crate-tonsoe-0.0.1 (crate (name "tonsoe") (vers "0.0.1-pre-dev-1") (hash "04zbdxbh3v87qmx6j0bvr6x831pa57d5mkiyib9fd51y83drypx8") (yanked #t)))

(define-public crate-tonsoe-0.0.1 (crate (name "tonsoe") (vers "0.0.1-pre-dev-2") (deps (list (crate-dep (name "anyhow") (req "^1.0.62") (default-features #t) (kind 0)) (crate-dep (name "async-trait") (req "^0.1.57") (default-features #t) (kind 0)) (crate-dep (name "bitflags") (req "^1.3.2") (default-features #t) (kind 0)) (crate-dep (name "futures-util") (req "^0.3.23") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.11") (features (quote ("json"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.144") (features (quote ("derive" "rc"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.20.1") (features (quote ("full"))) (default-features #t) (kind 0)) (crate-dep (name "tokio-tungstenite") (req "^0.17.2") (features (quote ("native-tls"))) (default-features #t) (kind 0)))) (hash "1lf46s0qxlmq590nkdv6i8wwylyl2iayxp57nkw6sxl2iry0qabc") (yanked #t)))

