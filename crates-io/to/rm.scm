(define-module (crates-io to rm) #:use-module (crates-io))

(define-public crate-torment-nexus-0.0.0 (crate (name "torment-nexus") (vers "0.0.0") (hash "0shgvbjfd8c9m1h57k39rdj7k2wz6540mxn4w0gd5nl15pzl2nnh") (yanked #t)))

(define-public crate-tormov-0.1 (crate (name "tormov") (vers "0.1.1") (deps (list (crate-dep (name "regex") (req "^0.2.2") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.14") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0.14") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.3") (default-features #t) (kind 0)))) (hash "0hzpy2iclfhii93xkqnxyjs6xv8xr8kkal7gmwn3d77y4lw38pkh")))

(define-public crate-tormov-0.2 (crate (name "tormov") (vers "0.2.0") (deps (list (crate-dep (name "regex") (req "^0.2.2") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.15") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0.15") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.4") (default-features #t) (kind 0)))) (hash "04is2pbipiykxd8rmhgzja4kkza4q7y2nnj3lg6n8wrzdqaid403")))

(define-public crate-tormov-0.2 (crate (name "tormov") (vers "0.2.1") (deps (list (crate-dep (name "regex") (req "^0.2.2") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.15") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0.15") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.4") (default-features #t) (kind 0)))) (hash "0ar70k8hb3bd3l8r2l0gh4z893iwa4vdikrd44b7h43m11iihp8v")))

(define-public crate-tormov-0.2 (crate (name "tormov") (vers "0.2.2") (deps (list (crate-dep (name "regex") (req "^0.2.2") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.15") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0.15") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.4") (default-features #t) (kind 0)))) (hash "0ji0cwkcsl6yjz3m1yh4f213n81yv28c2k5fxd38lycakvmzsmxx")))

(define-public crate-tormov-0.3 (crate (name "tormov") (vers "0.3.0") (deps (list (crate-dep (name "regex") (req "^1.3.4") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.104") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0.104") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.48") (default-features #t) (kind 0)))) (hash "0r0a8v0bd5jxvw9a9xf8zar27r0qrm2aplysp2bbp94rdsds2hav")))

(define-public crate-tormov-0.3 (crate (name "tormov") (vers "0.3.1") (deps (list (crate-dep (name "regex") (req "^1.3.4") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.104") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0.104") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.48") (default-features #t) (kind 0)))) (hash "1bss7mk5vkwvfmmgigrrvhycrgvz2kf0mqzh21l25z84wsx8gkr1")))

