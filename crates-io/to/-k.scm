(define-module (crates-io to -k) #:use-module (crates-io))

(define-public crate-to-kana-0.1 (crate (name "to-kana") (vers "0.1.0") (hash "0shxwcgir6mnfzai5kl5wvq3inkfwmh0bkgbwj4880vf3l38s5da")))

(define-public crate-to-kana-0.2 (crate (name "to-kana") (vers "0.2.0") (hash "0jw6afvwh9xyhdgiqchp84dlj7zw5bz7m1nqwjklg6q8399yjlwj")))

(define-public crate-to-kana-0.3 (crate (name "to-kana") (vers "0.3.0") (deps (list (crate-dep (name "clap") (req "^2.33") (default-features #t) (kind 0)))) (hash "1cdnr1fymxg3pd5cvajgbyibfsyw44466gx8nwckk45h556k8cbk")))

(define-public crate-to-kana-0.4 (crate (name "to-kana") (vers "0.4.0") (deps (list (crate-dep (name "clap") (req "^2.33") (default-features #t) (kind 0)))) (hash "1hl3qcy5mbaf044wqji28ph761x9z5ja8nj57s7gyz00dxgddk93")))

(define-public crate-to-kana-0.5 (crate (name "to-kana") (vers "0.5.0") (deps (list (crate-dep (name "clap") (req "^2.33") (default-features #t) (kind 0)) (crate-dep (name "criterion") (req "^0.3") (default-features #t) (kind 2)))) (hash "0wgjbbmz0p2kkra82iqb138cka9cdyh071qsvm0gg2r5js4l9lwd") (yanked #t)))

(define-public crate-to-kana-0.5 (crate (name "to-kana") (vers "0.5.1") (deps (list (crate-dep (name "clap") (req "^2.33") (default-features #t) (kind 0)) (crate-dep (name "criterion") (req "^0.3") (default-features #t) (kind 2)))) (hash "0kawinx6n58qf1x5dic66rr0wp2z5f3gmyxcfq8yh8x35fmk3p5z")))

(define-public crate-to-kana-0.6 (crate (name "to-kana") (vers "0.6.0") (deps (list (crate-dep (name "clap") (req "^2.33") (default-features #t) (kind 0)) (crate-dep (name "criterion") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)))) (hash "0law264ig4cwhyc451igb88gns2mli8dlbyy3h4gy8gw070fhn5p")))

