(define-module (crates-io to te) #:use-module (crates-io))

(define-public crate-tote-0.1 (crate (name "tote") (vers "0.1.0") (deps (list (crate-dep (name "async-trait") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "dirs") (req "^3.0") (default-features #t) (kind 2)) (crate-dep (name "serde") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "tempfile") (req "^3.0") (default-features #t) (kind 2)) (crate-dep (name "thiserror") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^0.2") (features (quote ("macros" "rt-threaded"))) (default-features #t) (kind 2)))) (hash "0sxgmwiha5h1kx2blikcr7kkqpk24vakv5q9f5qw9pfb8ifaqxq2")))

(define-public crate-tote-0.2 (crate (name "tote") (vers "0.2.0") (deps (list (crate-dep (name "async-trait") (req "^0.1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "dirs") (req "^3.0") (default-features #t) (kind 2)) (crate-dep (name "serde") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "tempfile") (req "^3.0") (default-features #t) (kind 2)) (crate-dep (name "thiserror") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^0.2") (features (quote ("macros" "rt-threaded"))) (default-features #t) (kind 2)))) (hash "0g6v1f7x3i054gq2r4hgwhy6cxhm8c42h4s2c0n0b35723i3nvj0") (features (quote (("default") ("async" "async-trait"))))))

(define-public crate-tote-0.3 (crate (name "tote") (vers "0.3.0") (deps (list (crate-dep (name "async-trait") (req "^0.1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "dirs") (req "^3.0") (default-features #t) (kind 2)) (crate-dep (name "reqwest") (req "^0.11") (features (quote ("json"))) (default-features #t) (kind 2)) (crate-dep (name "serde") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "tempfile") (req "^3.2") (default-features #t) (kind 2)) (crate-dep (name "thiserror") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.12") (features (quote ("full"))) (default-features #t) (kind 2)))) (hash "0pwp468nmlqgy7avcjlys9bw6p111bfmy5flhhwc6d60l5dn4kia") (features (quote (("default") ("async" "async-trait"))))))

(define-public crate-tote-0.3 (crate (name "tote") (vers "0.3.1") (deps (list (crate-dep (name "async-trait") (req "^0.1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "dirs") (req "^3.0") (default-features #t) (kind 2)) (crate-dep (name "reqwest") (req "^0.11") (features (quote ("json"))) (default-features #t) (kind 2)) (crate-dep (name "serde") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "tempfile") (req "^3.2") (default-features #t) (kind 2)) (crate-dep (name "thiserror") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.12") (features (quote ("full"))) (default-features #t) (kind 2)))) (hash "120lk9bqyq4zvdfkgxdvkl7arf40wz8599l63i63lpk2q60k77b5") (features (quote (("default") ("async" "async-trait"))))))

(define-public crate-tote-0.3 (crate (name "tote") (vers "0.3.2") (deps (list (crate-dep (name "async-trait") (req "^0.1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "dirs") (req "^3.0") (default-features #t) (kind 2)) (crate-dep (name "reqwest") (req "^0.11") (features (quote ("json"))) (default-features #t) (kind 2)) (crate-dep (name "serde") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "tempfile") (req "^3.2") (default-features #t) (kind 2)) (crate-dep (name "thiserror") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.12") (features (quote ("full"))) (default-features #t) (kind 2)))) (hash "0jb642h1bvznalgbzr4b360pd73m388mx37h8ha7l3g5zi1dvj1s") (features (quote (("default") ("async" "async-trait"))))))

(define-public crate-tote-0.4 (crate (name "tote") (vers "0.4.1") (deps (list (crate-dep (name "async-trait") (req "^0.1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "curl") (req "^0.4") (default-features #t) (kind 2)) (crate-dep (name "dirs") (req "^3.0") (default-features #t) (kind 2)) (crate-dep (name "reqwest") (req "^0.11") (features (quote ("blocking" "json"))) (default-features #t) (kind 2)) (crate-dep (name "serde") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0") (default-features #t) (kind 2)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "tempfile") (req "^3.2") (default-features #t) (kind 2)) (crate-dep (name "thiserror") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.12") (features (quote ("full"))) (default-features #t) (kind 2)))) (hash "17gr8fbgwq0qvswi66r5rm8zqwiykj957z81andshbh5n1n9m7fm") (features (quote (("default") ("async" "async-trait"))))))

(define-public crate-tote-0.4 (crate (name "tote") (vers "0.4.2") (deps (list (crate-dep (name "async-trait") (req "^0.1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "dirs") (req "^3.0") (default-features #t) (kind 2)) (crate-dep (name "reqwest") (req "^0.11") (features (quote ("blocking" "json"))) (default-features #t) (kind 2)) (crate-dep (name "serde") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0") (default-features #t) (kind 2)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "tempfile") (req "^3.2") (default-features #t) (kind 2)) (crate-dep (name "thiserror") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.12") (features (quote ("full"))) (default-features #t) (kind 2)))) (hash "0qfyxwwzwm9z2axi2vgix4add1mahglszwfmk92pfsdcygjkd74n") (features (quote (("default") ("async" "async-trait"))))))

(define-public crate-tote-0.5 (crate (name "tote") (vers "0.5.0") (deps (list (crate-dep (name "async-trait") (req "^0.1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "dirs") (req "^3.0") (default-features #t) (kind 2)) (crate-dep (name "reqwest") (req "^0.11") (features (quote ("blocking" "json"))) (default-features #t) (kind 2)) (crate-dep (name "serde") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0") (default-features #t) (kind 2)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "tempfile") (req "^3.2") (default-features #t) (kind 2)) (crate-dep (name "thiserror") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.12") (features (quote ("full"))) (default-features #t) (kind 2)))) (hash "1c13nfhzgwh8d2dq6nhvd5xh7fcjm813182qfmdp3da047ibbxjb") (features (quote (("default") ("async" "async-trait"))))))

(define-public crate-tote-0.5 (crate (name "tote") (vers "0.5.1") (deps (list (crate-dep (name "async-trait") (req "^0.1.53") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "dirs") (req "^4.0.0") (default-features #t) (kind 2)) (crate-dep (name "reqwest") (req "^0.11.10") (features (quote ("blocking" "json"))) (default-features #t) (kind 2)) (crate-dep (name "serde") (req "^1.0.137") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0.137") (default-features #t) (kind 2)) (crate-dep (name "serde_json") (req "^1.0.81") (default-features #t) (kind 0)) (crate-dep (name "tempfile") (req "^3.3.0") (default-features #t) (kind 2)) (crate-dep (name "thiserror") (req "^1.0.31") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.18.2") (features (quote ("full"))) (default-features #t) (kind 2)))) (hash "1nxrsasla23hgwybjhx7wz782gizjzniy0hchfmika36108jbfqh") (features (quote (("default") ("async" "async-trait"))))))

(define-public crate-totem-0.0.1 (crate (name "totem") (vers "0.0.1") (hash "07xgwdy3l1xwnhv8hnf4fhng0qm04k8rxxw3cr601llcsh178cbp")))

(define-public crate-totems-0.1 (crate (name "totems") (vers "0.1.0") (hash "1x6r515swclck3nqm9s17w2pi6ikws0jl7r7gw0q8hgglk81xyj4")))

(define-public crate-totems-0.1 (crate (name "totems") (vers "0.1.1") (hash "1xzjm3zmh99bxr3vxvk9n0r45b3rphjvcrdb3vd8chym6vqsnfk6")))

(define-public crate-totems-0.1 (crate (name "totems") (vers "0.1.2") (hash "1p4z279i7gva2387dgdbfd4wxcqq40c5r32i1i9gr3j1zs7idz5k")))

(define-public crate-totems-0.1 (crate (name "totems") (vers "0.1.3") (hash "10jyv3h226vw3h8sfwn2srhr1a794379zcck2anqchblq4ws1hsg")))

(define-public crate-totems-0.1 (crate (name "totems") (vers "0.1.4") (hash "19w7xi50bwhrj38fm01d3s6g1wh53g0jb5dzkhxp1jm0d0zd649b")))

(define-public crate-totems-0.1 (crate (name "totems") (vers "0.1.5") (hash "1a5lgzm608cqd19h1wgy4a9gk0vh1wwfpgcyjspw3qs4jwlldla4")))

(define-public crate-totems-0.1 (crate (name "totems") (vers "0.1.6") (hash "1lppa47wrwraskd0k4v8w8483aralyjd8vljqbjx9ys84465vm87")))

(define-public crate-totems-0.1 (crate (name "totems") (vers "0.1.7") (hash "08vwliy6n4qq1jijf5qbykssw3ic158r1m0wiwazkrgsa4f5hcg4")))

(define-public crate-totems-0.1 (crate (name "totems") (vers "0.1.8") (hash "1041p07hnsfjckdapvmh9ly6v7q6wry4gak1rbr8i1vr196xhnss")))

(define-public crate-totems-0.1 (crate (name "totems") (vers "0.1.9") (hash "0z6dfwpmvih93jyf0aslhrabr6xcpk70wdhhwnda2ibri39skkjs")))

(define-public crate-totems-0.2 (crate (name "totems") (vers "0.2.0") (hash "1xwaicxyyfmjcyhw7pmb541gzmq58wgkmqp3dll7ndab2sbz6512")))

(define-public crate-totems-0.2 (crate (name "totems") (vers "0.2.1") (hash "1nhxn6f881pakm7ms7vga7mlk2pndmh2pdfq3811cq549vy5b0yg")))

(define-public crate-totems-0.2 (crate (name "totems") (vers "0.2.2") (hash "013nx2r7kdj08nh12jjsc1qv46prhaqd1pm4sv20b9b8xrvz47f2")))

(define-public crate-totems-0.2 (crate (name "totems") (vers "0.2.3") (hash "1cmamqqn01wa1nrahiz7z3wvm001sr3pz4fkbb3v5rqikqg479wc")))

(define-public crate-totems-0.2 (crate (name "totems") (vers "0.2.4") (hash "187sy6xpzmqz5kdqppk21kvpsnvcqrzicvink9k25w19yixa6z6g")))

(define-public crate-totems-0.2 (crate (name "totems") (vers "0.2.5") (hash "01h15kpak44cibjdj05z1s2xbrc7x7jc38vi73l6vrzjv0bjxqa0")))

(define-public crate-totems-0.2 (crate (name "totems") (vers "0.2.6") (hash "1mfhlg0fl58prpibz4h5kgifhcqsjifwszd63hcwrp210lpv9x7a")))

(define-public crate-totems-0.2 (crate (name "totems") (vers "0.2.7") (hash "0sp1ay6ql1clflq8haffk1mld85xw81lay51ps8rcwxlni4xxlff")))

