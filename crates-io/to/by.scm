(define-module (crates-io to by) #:use-module (crates-io))

(define-public crate-tobytcp-0.8 (crate (name "tobytcp") (vers "0.8.0") (hash "1lg23xri1wh130w4355qcm1y5zd7mmg4nhv9nj2rlpk2xdwba4dk") (yanked #t)))

(define-public crate-tobytcp-0.8 (crate (name "tobytcp") (vers "0.8.1") (hash "145kfnk7qf27mzj81sv5flnarnlfd6d9ymmlv7h4f0jcnxc25ka5") (yanked #t)))

(define-public crate-tobytcp-0.9 (crate (name "tobytcp") (vers "0.9.0") (deps (list (crate-dep (name "log") (req "^0.4.0") (default-features #t) (kind 0)) (crate-dep (name "uuid") (req "^0.6") (features (quote ("v4"))) (default-features #t) (kind 0)))) (hash "1p5nd9yz7j45305qjp4qp3x0nmmjfz1ldcn5xhq68yb6w4vfm0zl") (yanked #t)))

(define-public crate-tobytcp-0.9 (crate (name "tobytcp") (vers "0.9.1") (deps (list (crate-dep (name "log") (req "^0.4.0") (default-features #t) (kind 0)) (crate-dep (name "uuid") (req "^0.6") (features (quote ("v4"))) (default-features #t) (kind 0)))) (hash "13xsy9v9frk4zh6rvj653g01m8q1sk09rvs829j79cklih88hix7") (yanked #t)))

(define-public crate-tobytcp-0.9 (crate (name "tobytcp") (vers "0.9.2") (deps (list (crate-dep (name "log") (req "^0.4.0") (default-features #t) (kind 0)) (crate-dep (name "uuid") (req "^0.6") (features (quote ("v4"))) (default-features #t) (kind 0)))) (hash "08kkhm8mml0zispc8arkr88gazah6kwxn81zwxcavaj075qainyf") (yanked #t)))

(define-public crate-tobytcp-0.9 (crate (name "tobytcp") (vers "0.9.3") (deps (list (crate-dep (name "log") (req "^0.4.0") (default-features #t) (kind 0)) (crate-dep (name "uuid") (req "^0.6") (features (quote ("v4"))) (default-features #t) (kind 0)))) (hash "0v84k7d9wxsnajh3nx10if85z86173cizg9hj82rx0l7r4dd8sqr")))

(define-public crate-tobytcp-0.10 (crate (name "tobytcp") (vers "0.10.0") (deps (list (crate-dep (name "log") (req "^0.4.0") (default-features #t) (kind 0)) (crate-dep (name "uuid") (req "^0.6") (features (quote ("v4"))) (default-features #t) (kind 0)))) (hash "0a3zxjks178hhydaw44i4z9ncwshx438ka0l8jzy1yigxaav2q95")))

(define-public crate-tobytcp-0.10 (crate (name "tobytcp") (vers "0.10.1") (deps (list (crate-dep (name "log") (req "^0.4.0") (default-features #t) (kind 0)) (crate-dep (name "uuid") (req "^0.6") (features (quote ("v4"))) (default-features #t) (kind 0)))) (hash "04zxri3xq42i1ah2szgmm7vpgv89fx6c1gadsqbxpzsbrxx11rcz")))

(define-public crate-tobytcp-0.11 (crate (name "tobytcp") (vers "0.11.0") (deps (list (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "uuid") (req "^0.6") (features (quote ("v4"))) (default-features #t) (kind 0)))) (hash "0bljwk7jpc3g6mv800xw292jdm460hm2775xx55crzyr10ld2qkm")))

(define-public crate-tobytcp-0.12 (crate (name "tobytcp") (vers "0.12.0") (hash "1baam0d50wya24k9p6xcaqmgrnyqfcpzp4ld2pv7v17cls3bv3pw")))

(define-public crate-tobytcp-0.13 (crate (name "tobytcp") (vers "0.13.0") (deps (list (crate-dep (name "futures-preview") (req "^0.3.0-alpha.16") (features (quote ("compat" "io-compat"))) (default-features #t) (kind 0)) (crate-dep (name "romio") (req "^0.3.0-alpha.8") (default-features #t) (kind 2)) (crate-dep (name "runtime") (req "^0.3.0-alpha.5") (default-features #t) (kind 2)))) (hash "09466plpw3lndh0gad9frprfqrcjjxry4xysm82rd14nr90kmsbc")))

(define-public crate-tobytes-0.1 (crate (name "tobytes") (vers "0.1.0") (hash "0929xfzjpfyr5nqn7vw6rhd6lddjap78ag6rgn2yh29qg5731x1i")))

