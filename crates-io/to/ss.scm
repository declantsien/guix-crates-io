(define-module (crates-io to ss) #:use-module (crates-io))

(define-public crate-tosserror-0.1 (crate (name "tosserror") (vers "0.1.0") (deps (list (crate-dep (name "proc-macro2") (req "^1.0.63") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.29") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2.0.23") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0") (default-features #t) (kind 2)))) (hash "0542z9prqk2vs9b385dzbvh7znb24y48ws308m6cahqp07qsbvdi") (rust-version "1.56")))

(define-public crate-tosserror-0.1 (crate (name "tosserror") (vers "0.1.1") (deps (list (crate-dep (name "thiserror") (req "^1.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0") (default-features #t) (kind 2)) (crate-dep (name "tosserror-derive") (req "=0.1.1") (default-features #t) (kind 0)))) (hash "1cba134h1c4fa933mwvy7nfd2pb43m5i7lbfms80gmbb91xjqbkz") (features (quote (("default")))) (v 2) (features2 (quote (("thiserror" "dep:thiserror" "tosserror-derive/thiserror")))) (rust-version "1.56")))

(define-public crate-tosserror-0.1 (crate (name "tosserror") (vers "0.1.2") (deps (list (crate-dep (name "thiserror") (req "^1.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0") (default-features #t) (kind 2)) (crate-dep (name "tosserror-derive") (req "=0.1.2") (default-features #t) (kind 0)))) (hash "00lbaammsqzz7zr82fayqz2q29i4c1inw8ib3lf05siw5mwnbrwj") (features (quote (("default")))) (v 2) (features2 (quote (("thiserror" "dep:thiserror" "tosserror-derive/thiserror")))) (rust-version "1.56")))

(define-public crate-tosserror-derive-0.1 (crate (name "tosserror-derive") (vers "0.1.1") (deps (list (crate-dep (name "proc-macro2") (req "^1.0.63") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.29") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2.0.23") (default-features #t) (kind 0)))) (hash "091bcsc2xwrkq5mnnprbfi7lfqizdmf896k2nqjl6fjbv0sp67c2") (features (quote (("thiserror") ("default")))) (rust-version "1.56")))

(define-public crate-tosserror-derive-0.1 (crate (name "tosserror-derive") (vers "0.1.2") (deps (list (crate-dep (name "proc-macro2") (req "^1.0.63") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.29") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2.0.23") (default-features #t) (kind 0)))) (hash "1clm38z6p6cd16c9wf56sphm2l5gcq7x0plpqc6w0yrp37nzfm6s") (features (quote (("thiserror") ("default")))) (rust-version "1.56")))

(define-public crate-tossicat-0.1 (crate (name "tossicat") (vers "0.1.0") (hash "0f55bjnrs8s61bkprpzcsdvlg1v977rd8hfavb9cajj2657kfv7g")))

(define-public crate-tossicat-0.2 (crate (name "tossicat") (vers "0.2.0") (hash "1s2ffqf09airdvc3yh7p4z1xzy79x7ysm1ki0n5r9di0rclvqfxc")))

(define-public crate-tossicat-0.3 (crate (name "tossicat") (vers "0.3.0") (hash "0mqpj3nswx0614l7in7jzwcbhjswcbxpyvpna5zvw1wgfxdw7408")))

(define-public crate-tossicat-0.3 (crate (name "tossicat") (vers "0.3.1") (hash "0xg4caabd5axav04q309zhhdvjdxzdi478mn2nlbrkih7qhlyjiz")))

(define-public crate-tossicat-0.3 (crate (name "tossicat") (vers "0.3.2") (hash "1xpk8fl376j9igkhmn441bwm5s022pjvl3r2nzmalrw4spwc06di")))

(define-public crate-tossicat-0.4 (crate (name "tossicat") (vers "0.4.1") (hash "1afw7r48bzxq0fddn6zmhqmj7p0y0ihns8hp75jnx0v40lfwn9f7")))

(define-public crate-tossicat-0.4 (crate (name "tossicat") (vers "0.4.2") (hash "05jgl9apx14b8a5p11swxnyhyysc284az72xqjiqi18vks5v6pj9")))

(define-public crate-tossicat-0.4 (crate (name "tossicat") (vers "0.4.3") (hash "083m0kwzyfxkxakf9krgsrqkpsnvljhvdbg2zjzg1yqb11ma5xiz")))

(define-public crate-tossicat-0.4 (crate (name "tossicat") (vers "0.4.4") (hash "0bb6mq4wzj7s7bz027s0ybw6qrhls2nax7myghv5rayvgkn5yg1w")))

(define-public crate-tossicat-0.4 (crate (name "tossicat") (vers "0.4.5") (hash "13rdsgcjcdapr8pjf9h2drax2arlskf85s10h0lywk6gpd3m9920")))

(define-public crate-tossicat-0.4 (crate (name "tossicat") (vers "0.4.6") (hash "0mjar9d5b46pdq0r21gfz54vg4fkry54yp6lrji110hcf3j5kixa")))

(define-public crate-tossicat-0.4 (crate (name "tossicat") (vers "0.4.7") (hash "12yqj2373cbs5qzbvx3ffdk4khwr60ybq4dd7d3yhg7yqkv85r7z")))

(define-public crate-tossicat-0.5 (crate (name "tossicat") (vers "0.5.0") (hash "0zhixg896c7nzh1wsmfm4sss1g7q0h2dpwz4rzr17na4ygsmx7if")))

(define-public crate-tossicat-0.5 (crate (name "tossicat") (vers "0.5.1") (hash "0msic98ly7rsgbgl2xy09pq26y5fpphyd0iaimhal4mg6vn1l3h6")))

(define-public crate-tossicat-0.6 (crate (name "tossicat") (vers "0.6.0") (hash "14hi5gar0sldw51gjac2j68603m6pdabdvz5khv769m0yi0clbwh")))

(define-public crate-tosspay-0.1 (crate (name "tosspay") (vers "0.1.0") (deps (list (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.12.2") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.197") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.115") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.37.0") (features (quote ("macros" "rt-multi-thread"))) (default-features #t) (kind 0)))) (hash "1b0j9fy3sdkq3z5r9x7rdnzmzccsz1bcnyn8wr6hqp2mcw3dahh3")))

(define-public crate-tosspayments-rs-0.1 (crate (name "tosspayments-rs") (vers "0.1.0") (deps (list (crate-dep (name "base64") (req "^0.21") (default-features #t) (kind 0)) (crate-dep (name "httpmock") (req "^0.6") (default-features #t) (kind 2)) (crate-dep (name "reqwest") (req "^0.11") (features (quote ("json" "rustls-tls"))) (kind 0)) (crate-dep (name "serde") (req ">=1.0.79") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_qs") (req "^0.12.0") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.50") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.34") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.34") (features (quote ("full"))) (default-features #t) (kind 2)))) (hash "0gq4lyw2yk7hphxczczsbif2angwd54qf74rs1q9dqcslipnqnd4") (rust-version "1.68.0")))

(define-public crate-tosspayments-rs-0.1 (crate (name "tosspayments-rs") (vers "0.1.1") (deps (list (crate-dep (name "base64") (req "^0.21") (default-features #t) (kind 0)) (crate-dep (name "httpmock") (req "^0.6") (default-features #t) (kind 2)) (crate-dep (name "reqwest") (req "^0.11") (features (quote ("json" "rustls-tls"))) (kind 0)) (crate-dep (name "serde") (req ">=1.0.79") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_qs") (req "^0.12.0") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.50") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.34") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.34") (features (quote ("full"))) (default-features #t) (kind 2)))) (hash "0dwgfq8d5plw3iz094m4vpnc0sxnq98zy56mv30n2prbn3ydavgg") (rust-version "1.68.0")))

(define-public crate-tosspayments-rs-0.2 (crate (name "tosspayments-rs") (vers "0.2.0") (deps (list (crate-dep (name "base64") (req "^0.21") (default-features #t) (kind 0)) (crate-dep (name "convert_case") (req "^0.6") (default-features #t) (kind 0)) (crate-dep (name "httpmock") (req "^0.6") (default-features #t) (kind 2)) (crate-dep (name "reqwest") (req "^0.11") (features (quote ("json" "rustls-tls"))) (kind 0)) (crate-dep (name "serde") (req ">=1.0.79") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_qs") (req "^0.12.0") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.50") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.34") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.34") (features (quote ("full"))) (default-features #t) (kind 2)) (crate-dep (name "typed-builder") (req "^0.18") (default-features #t) (kind 0)))) (hash "199k6nwqa8965rd1gdi71150sjj6qa0miqfws7q7drxwxc5l83fq") (rust-version "1.68.0")))

