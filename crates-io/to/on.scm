(define-module (crates-io to on) #:use-module (crates-io))

(define-public crate-toon-0.0.0 (crate (name "toon") (vers "0.0.0") (hash "150qsyli1x2hy5jxwci6d56kln6gpjg16csniy7ddzb3k7kpk19j")))

(define-public crate-toonify-1 (crate (name "toonify") (vers "1.0.0") (deps (list (crate-dep (name "json") (req "^0.12.4") (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.11") (features (quote ("blocking" "multipart"))) (default-features #t) (kind 0)))) (hash "1k9ijr3qj8q596jmqwb0ff310pn3nq51238mrshi3hrhazgphn9r")))

(define-public crate-toonify-cli-1 (crate (name "toonify-cli") (vers "1.0.0") (deps (list (crate-dep (name "reqwest") (req "^0.11") (features (quote ("blocking"))) (default-features #t) (kind 0)) (crate-dep (name "toonify") (req "^1.0.0") (default-features #t) (kind 0)))) (hash "1n22mlrfsqx4dd48snbvil8gz0fghnn7m3d1h2zyh3i4qnbw4vfp")))

