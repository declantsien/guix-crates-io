(define-module (crates-io to _q) #:use-module (crates-io))

(define-public crate-to_query-0.5 (crate (name "to_query") (vers "0.5.0") (deps (list (crate-dep (name "proc-macro2") (req "^1.0.78") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2.0") (features (quote ("extra-traits"))) (default-features #t) (kind 0)))) (hash "0bdv62wb8h29zv02sslc984qk2ivgd59f4vvb9n3gi6ncw2cmw18") (yanked #t)))

(define-public crate-to_query-0.5 (crate (name "to_query") (vers "0.5.1") (deps (list (crate-dep (name "proc-macro2") (req "^1.0.78") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2.0") (features (quote ("extra-traits"))) (default-features #t) (kind 0)))) (hash "1yp0dhhl6r1g700jsvmb46javiily3wbmdqnjf6y5jyj1i1j6rcl") (yanked #t)))

(define-public crate-to_query-0.5 (crate (name "to_query") (vers "0.5.2") (deps (list (crate-dep (name "proc-macro2") (req "^1.0.78") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2.0") (features (quote ("extra-traits"))) (default-features #t) (kind 0)))) (hash "0c1y5qdl09q71v8f1yxgmycr5fh0amnjf3kccpyf3ckmwvnq0qd3") (yanked #t)))

(define-public crate-to_query-0.5 (crate (name "to_query") (vers "0.5.3") (deps (list (crate-dep (name "proc-macro2") (req "^1.0.78") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2.0") (features (quote ("extra-traits"))) (default-features #t) (kind 0)))) (hash "0bzanq4d24brd82q3w0xac9g6hbzqj8amj3cf2p14jwvd5g1w2gi") (yanked #t)))

(define-public crate-to_query-0.5 (crate (name "to_query") (vers "0.5.4") (deps (list (crate-dep (name "proc-macro2") (req "^1.0.78") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2.0") (features (quote ("extra-traits"))) (default-features #t) (kind 0)))) (hash "08r28xv447r1jkgv6ari679rlxmnjmlhx490ammx0q4nzp4lnz8d")))

