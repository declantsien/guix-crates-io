(define-module (crates-io to -m) #:use-module (crates-io))

(define-public crate-to-mut-0.1 (crate (name "to-mut") (vers "0.1.0") (deps (list (crate-dep (name "to-mut-proc-macro") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "17hbkcp85ic4597s1nbmy6z77wfz91awh3nn3zd27ir2x06ai0yr")))

(define-public crate-to-mut-proc-macro-0.1 (crate (name "to-mut-proc-macro") (vers "0.1.0") (deps (list (crate-dep (name "quote") (req "^1.0.23") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.107") (default-features #t) (kind 0)))) (hash "0zl5z6z3yzz7py8g3zy73vswa8d1hgsjllhrpbhgsfdgrqy4wwjv")))

