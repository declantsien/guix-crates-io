(define-module (crates-io xb it) #:use-module (crates-io))

(define-public crate-xbitwise-0.1 (crate (name "xbitwise") (vers "0.1.0") (hash "0hwipbqsp6pzrgjmcq57v2zmbf8d47izs4fcfph7lyxk86ai0940") (features (quote (("unsigned" "u8" "u16" "u32" "u64" "u128") ("u8") ("u64") ("u32") ("u16") ("u128") ("signed" "i8" "i16" "i32" "i64" "i128") ("i8") ("i64") ("i32") ("i16") ("i128") ("default" "signed" "unsigned")))) (rust-version "1.31.0")))

