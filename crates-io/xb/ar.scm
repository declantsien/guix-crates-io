(define-module (crates-io xb ar) #:use-module (crates-io))

(define-public crate-xbar-1 (crate (name "xbar") (vers "1.0.0") (deps (list (crate-dep (name "clap") (req "^2.33.0") (default-features #t) (kind 2)) (crate-dep (name "svg") (req "^0.5.12") (default-features #t) (kind 2)))) (hash "07wz93gq5z7pljg77cx6s6l0aca3j91d6xvry87v587jb9q7q39y") (yanked #t)))

(define-public crate-xbar-1 (crate (name "xbar") (vers "1.0.1") (deps (list (crate-dep (name "clap") (req "^2.33.0") (default-features #t) (kind 2)) (crate-dep (name "svg") (req "^0.5.12") (default-features #t) (kind 2)))) (hash "1afh2jsqzfmdzfgzyycj0snq5jhrsy8c1p03rs8x8r5kzhz4x13v")))

