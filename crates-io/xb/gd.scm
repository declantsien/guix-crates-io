(define-module (crates-io xb gd) #:use-module (crates-io))

(define-public crate-xbgdump-0.1 (crate (name "xbgdump") (vers "0.1.0") (deps (list (crate-dep (name "anyhow") (req "^1.0.40") (default-features #t) (kind 0)) (crate-dep (name "image") (req "^0.23.14") (features (quote ("png" "pnm"))) (kind 0)) (crate-dep (name "imageproc") (req "^0.22.0") (kind 0)) (crate-dep (name "x11rb") (req "^0.8.1") (default-features #t) (kind 0)))) (hash "09niwr085nnzi3pryjg32iw4wh4l28jw66nh534sr6g5sagmai21")))

(define-public crate-xbgdump-0.1 (crate (name "xbgdump") (vers "0.1.3") (deps (list (crate-dep (name "anyhow") (req "^1.0.40") (default-features #t) (kind 0)) (crate-dep (name "image") (req "^0.23.14") (features (quote ("png" "pnm"))) (kind 0)) (crate-dep (name "imageproc") (req "^0.22.0") (kind 0)) (crate-dep (name "x11rb") (req "^0.8.1") (default-features #t) (kind 0)))) (hash "0n5c77bj7nzwvjlc11r92685xmlyr22dyjlnx5v9b7ahlf9lf3pd")))

(define-public crate-xbgdump-0.1 (crate (name "xbgdump") (vers "0.1.4") (deps (list (crate-dep (name "anyhow") (req "^1.0.40") (default-features #t) (kind 0)) (crate-dep (name "image") (req "^0.23.14") (features (quote ("png" "pnm"))) (kind 0)) (crate-dep (name "imageproc") (req "^0.22.0") (kind 0)) (crate-dep (name "x11rb") (req "^0.8.1") (default-features #t) (kind 0)))) (hash "121shsa2db164vid6qapk28mddi2ybdw11r68zb8xkq84hb1a8hk")))

(define-public crate-xbgdump-0.1 (crate (name "xbgdump") (vers "0.1.5") (deps (list (crate-dep (name "anyhow") (req "^1.0.40") (default-features #t) (kind 0)) (crate-dep (name "getopts") (req "^0.2.21") (default-features #t) (kind 0)) (crate-dep (name "image") (req "^0.23.14") (features (quote ("png" "pnm"))) (kind 0)) (crate-dep (name "x11rb") (req "^0.8.1") (features (quote ("randr"))) (default-features #t) (kind 0)))) (hash "1hd453z0hk7y0jabd0l5swsd8vyii85bw4wr8bpyksm5hy0w5kf3")))

(define-public crate-xbgdump-0.1 (crate (name "xbgdump") (vers "0.1.7") (deps (list (crate-dep (name "anyhow") (req "^1.0.40") (default-features #t) (kind 0)) (crate-dep (name "getopts") (req "^0.2.21") (default-features #t) (kind 0)) (crate-dep (name "image") (req "^0.23.14") (features (quote ("png" "pnm"))) (kind 0)) (crate-dep (name "x11rb") (req "^0.8.1") (features (quote ("randr"))) (default-features #t) (kind 0)))) (hash "1mjxzxrk3aqixsdk155zcndhxnff28vmqarzyrffpswp5hdcvyfc")))

