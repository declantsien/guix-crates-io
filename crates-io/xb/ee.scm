(define-module (crates-io xb ee) #:use-module (crates-io))

(define-public crate-xbee_s2c-0.1 (crate (name "xbee_s2c") (vers "0.1.0") (deps (list (crate-dep (name "arraydeque") (req "^0.4") (kind 0)) (crate-dep (name "arrayvec") (req "^0.4") (features (quote ("use_union"))) (kind 0)) (crate-dep (name "bitflags") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "embedded-hal") (req "^0.2.1") (features (quote ("unproven"))) (default-features #t) (kind 0)) (crate-dep (name "nb") (req "^0.1.1") (default-features #t) (kind 0)))) (hash "1zmkka0149rrrbgzir18n97589q5lmzakwwrfr96q01zzlk1krcp")))

