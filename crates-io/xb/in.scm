(define-module (crates-io xb in) #:use-module (crates-io))

(define-public crate-xbin-0.1 (crate (name "xbin") (vers "0.1.1") (deps (list (crate-dep (name "aok") (req "^0.1.3") (default-features #t) (kind 2)) (crate-dep (name "loginit") (req "^0.1.10") (default-features #t) (kind 2)) (crate-dep (name "static_init") (req "^1.0.3") (default-features #t) (kind 2)) (crate-dep (name "tokio") (req "^1.36.0") (features (quote ("macros" "rt" "rt-multi-thread" "time" "sync"))) (default-features #t) (kind 2)) (crate-dep (name "tracing") (req "^0.1.40") (default-features #t) (kind 2)))) (hash "16zd3kmz1c1pfhkmkz1m37jpn60ma9cggh673rij14dy4psrcf5g")))

(define-public crate-xbin-0.1 (crate (name "xbin") (vers "0.1.3") (deps (list (crate-dep (name "aok") (req "^0.1.3") (default-features #t) (kind 2)) (crate-dep (name "loginit") (req "^0.1.10") (default-features #t) (kind 2)) (crate-dep (name "static_init") (req "^1.0.3") (default-features #t) (kind 2)) (crate-dep (name "tokio") (req "^1.36.0") (features (quote ("macros" "rt" "rt-multi-thread" "time" "sync"))) (default-features #t) (kind 2)) (crate-dep (name "tracing") (req "^0.1.40") (default-features #t) (kind 2)))) (hash "118534a9rybz1jckxg3jfl04zdk169p2rxh0mdpgp109hng0fbr1")))

(define-public crate-xbinary-0.1 (crate (name "xbinary") (vers "0.1.0") (deps (list (crate-dep (name "bytes") (req "^0.5") (default-features #t) (kind 0)))) (hash "1h36kidrbmdbpgna18yi1zfzsx3nrpfgcfqmhx6syxbh47s2dxnc")))

(define-public crate-xbinary-0.1 (crate (name "xbinary") (vers "0.1.1") (deps (list (crate-dep (name "bytes") (req "^0.5") (default-features #t) (kind 0)))) (hash "0as5890jb6qdfjvfi5rc0b90r84b3j53m4zgj94c9wy3w1gdhyw2")))

(define-public crate-xbinary-0.1 (crate (name "xbinary") (vers "0.1.2") (deps (list (crate-dep (name "bytes") (req "^0.5") (default-features #t) (kind 0)))) (hash "0zynlrzk8xhpw2dkafwbigzff4ckpaf4ysbrydmgj8pc32bp2brr")))

(define-public crate-xbinary-0.1 (crate (name "xbinary") (vers "0.1.3") (deps (list (crate-dep (name "bytes") (req "^0.5") (default-features #t) (kind 0)))) (hash "1z75xlalyfb5lnsjhb7hzi39qx8gj4clgpni4g9bvryfha8viaxp")))

(define-public crate-xbinary-0.1 (crate (name "xbinary") (vers "0.1.4") (deps (list (crate-dep (name "bytes") (req "^0.5") (default-features #t) (kind 0)))) (hash "0bhf00g1bqa9agh0qbdv48bdch7ivkp59628dbp0xjr4czaaxfdy")))

(define-public crate-xbinary-0.1 (crate (name "xbinary") (vers "0.1.5") (deps (list (crate-dep (name "bytes") (req "^0.5") (default-features #t) (kind 0)))) (hash "14az3rifq2aaknsbyg6qhc6m7cwky14rbb94czr9qlhx50bkkfm1")))

