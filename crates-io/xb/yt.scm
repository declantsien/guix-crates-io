(define-module (crates-io xb yt) #:use-module (crates-io))

(define-public crate-xbytes-0.1 (crate (name "xbytes") (vers "0.1.0") (deps (list (crate-dep (name "bitflags") (req "^2.4.1") (default-features #t) (kind 0)) (crate-dep (name "fraction") (req "^0.14.0") (optional #t) (default-features #t) (kind 0)))) (hash "1y1lmf0smvblb9dv1m6m71ark1p74lpv70r9qayjip5s34pjf856") (features (quote (("u128") ("no-panic" "lossless") ("lossless" "fraction") ("default" "u128" "lossless" "no-panic") ("case-insensitive") ("bits"))))))

(define-public crate-xbytes-0.1 (crate (name "xbytes") (vers "0.1.1") (deps (list (crate-dep (name "bitflags") (req "^2.4.1") (default-features #t) (kind 0)) (crate-dep (name "fraction") (req "^0.14.0") (optional #t) (default-features #t) (kind 0)))) (hash "1q9cks3qlnjyv7hkf65x2mqyw1gbg2pwaswhwswiczi9d1b9w7x3") (features (quote (("u128") ("no-panic" "lossless") ("lossless" "fraction") ("default" "u128" "lossless" "no-panic") ("case-insensitive") ("bits"))))))

