(define-module (crates-io mw ca) #:use-module (crates-io))

(define-public crate-mwcas-0.1 (crate (name "mwcas") (vers "0.1.0") (deps (list (crate-dep (name "crossbeam-epoch") (req "^0.9.4") (default-features #t) (kind 0)) (crate-dep (name "crossbeam-utils") (req "^0.8.4") (default-features #t) (kind 2)) (crate-dep (name "num_cpus") (req "^1.11.1") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.8.3") (default-features #t) (kind 2)))) (hash "0kvwcvafcksdn34qnvnd9ykxbdbs4cqxvm3fxw2y94chbk09ax2f")))

(define-public crate-mwcas-0.2 (crate (name "mwcas") (vers "0.2.0") (deps (list (crate-dep (name "criterion") (req "^0.3.4") (default-features #t) (kind 2)) (crate-dep (name "crossbeam-epoch") (req "^0.9.4") (default-features #t) (kind 0)) (crate-dep (name "crossbeam-utils") (req "^0.8.4") (default-features #t) (kind 2)) (crate-dep (name "num_cpus") (req "^1.11.1") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.8.3") (default-features #t) (kind 2)))) (hash "14jvji6gsi1wdl9mn8jxx04rgakp730laislrp5g00bxybq916dn")))

