(define-module (crates-io mw ps) #:use-module (crates-io))

(define-public crate-mwps-0.0.0 (crate (name "mwps") (vers "0.0.0") (deps (list (crate-dep (name "pyo3") (req "^0.17.2") (features (quote ("extension-module" "multiple-pymethods" "abi3-py37"))) (optional #t) (default-features #t) (kind 0)))) (hash "1l2h74lxrw47938xn4wja1f9qaj78lygvg2ik1pmx8pclar80vp3") (features (quote (("python_binding" "pyo3") ("default"))))))

