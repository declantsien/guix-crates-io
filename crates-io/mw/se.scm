(define-module (crates-io mw se) #:use-module (crates-io))

(define-public crate-mwseaql-0.1 (crate (name "mwseaql") (vers "0.1.0") (deps (list (crate-dep (name "sea-query") (req "^0.26.3") (features (quote ("derive"))) (kind 0)))) (hash "1zzyzvwykdv3rjsg8lg778sapxm2ffm1jyy1s9xvqwbvr0svbqaw") (features (quote (("toolforge") ("proofread_page") ("page_assessments") ("linter")))) (rust-version "1.60")))

(define-public crate-mwseaql-0.1 (crate (name "mwseaql") (vers "0.1.1") (deps (list (crate-dep (name "sea-query") (req "^0.26.3") (features (quote ("derive"))) (kind 0)))) (hash "00xavhm1mwb8a37cr472c26n15lhgrf1j6z1wdnm9xqk8rynyn06") (features (quote (("toolforge") ("proofread_page") ("page_assessments") ("linter")))) (rust-version "1.60")))

(define-public crate-mwseaql-0.1 (crate (name "mwseaql") (vers "0.1.2") (deps (list (crate-dep (name "sea-query") (req "^0.26.3") (features (quote ("derive" "backend-mysql"))) (kind 0)))) (hash "06msb53zqp7jvbc3bm6a41xkvxr7lr2mrlrabqmicpk8vnxmrqiq") (features (quote (("toolforge") ("proofread_page") ("page_assessments") ("linter")))) (rust-version "1.60")))

(define-public crate-mwseaql-0.2 (crate (name "mwseaql") (vers "0.2.0") (deps (list (crate-dep (name "sea-query") (req "^0.27.1") (features (quote ("derive" "backend-mysql"))) (kind 0)))) (hash "18c9sws2as3456r14ikvjqhh51696rals4w9ig3qyahf9fh8jzl3") (features (quote (("toolforge") ("proofread_page") ("page_assessments") ("linter")))) (rust-version "1.60")))

(define-public crate-mwseaql-0.3 (crate (name "mwseaql") (vers "0.3.0") (deps (list (crate-dep (name "sea-query") (req "^0.28.2") (features (quote ("derive" "backend-mysql"))) (kind 0)))) (hash "1fbnd8fm5hzlc5msqknw60bc6v7nwaqqxbksn86j9ccmb7w7by3x") (features (quote (("toolforge") ("proofread_page") ("page_assessments") ("linter")))) (rust-version "1.62")))

