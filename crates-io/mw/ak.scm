(define-module (crates-io mw ak) #:use-module (crates-io))

(define-public crate-mwaka-0.0.1 (crate (name "mwaka") (vers "0.0.1") (deps (list (crate-dep (name "mwaka-aria") (req "^0.0.1") (default-features #t) (kind 0)))) (hash "02km59h1gy5d7sblni0w7kwznh1r1dvj1sz1s4i27by61gljxvbb")))

(define-public crate-mwaka-0.0.2 (crate (name "mwaka") (vers "0.0.2") (deps (list (crate-dep (name "mwaka-aria") (req "^0.0.2") (default-features #t) (kind 0)))) (hash "1b8jylc90a3ldxk005i67h683n8rqcwm5a9jl8ng9ld72k250i6i")))

(define-public crate-mwaka-aria-0.0.1 (crate (name "mwaka-aria") (vers "0.0.1") (hash "1m2qragxd0kcg54ax1l464g94i02b0jhm5rxkb1mbqii1r7xbii2")))

(define-public crate-mwaka-aria-0.0.2 (crate (name "mwaka-aria") (vers "0.0.2") (hash "0asw8hhpgi251a1pl3z84dcby3yvknn8fhjbindnlzpf2dpsblrz")))

