(define-module (crates-io mw s-) #:use-module (crates-io))

(define-public crate-mws-derive-0.1 (crate (name "mws-derive") (vers "0.1.0") (deps (list (crate-dep (name "quote") (req "^0.6") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^0.14") (default-features #t) (kind 0)))) (hash "09cr6av4g0dzdbswdqr4vkh1d3ghylls0b785r4k2mdzi2amg58n")))

(define-public crate-mws-derive-0.9 (crate (name "mws-derive") (vers "0.9.0") (deps (list (crate-dep (name "quote") (req "^0.6") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^0.14") (default-features #t) (kind 0)))) (hash "1c56rlyx6dvr79r2sjij56k4mb283p99wa78jbzyy8nsfbyv31s2")))

(define-public crate-mws-derive-0.12 (crate (name "mws-derive") (vers "0.12.0") (deps (list (crate-dep (name "quote") (req ">=1.0.0, <2.0.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req ">=1.0.0, <2.0.0") (features (quote ("extra-traits"))) (default-features #t) (kind 0)))) (hash "0cfdyvdlrx7x5ax9d0w1gngzyym6pvxbgb9nc7jmycmn7k52s8xv")))

