(define-module (crates-io ea rs) #:use-module (crates-io))

(define-public crate-ears-0.3 (crate (name "ears") (vers "0.3.1") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "0ax8zs9amm55cla210jb37wfrpblv3j41bi39c989np5d9j1f6dx")))

(define-public crate-ears-0.3 (crate (name "ears") (vers "0.3.2") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "1zshp9k9x53n9l6pdvc8358w2qwkjx2saail6r46h7xc56ldfix3")))

(define-public crate-ears-0.3 (crate (name "ears") (vers "0.3.3") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "1bmw658al4g9wzchjkz1janrmqb1wyj5mr12adhlvr73ig6b88bp")))

(define-public crate-ears-0.3 (crate (name "ears") (vers "0.3.4") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "15d7id5fxrql38c547l8mh4yvj0mrxi0jnfawbm31370aq2gd05b")))

(define-public crate-ears-0.3 (crate (name "ears") (vers "0.3.5") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "1qmhgxa534x6zpdkwglg3pwb9z87yvmmf3cg399vs36p047aqs6r")))

(define-public crate-ears-0.4 (crate (name "ears") (vers "0.4.0") (deps (list (crate-dep (name "lazy_static") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "08nwxfdl2izf2x0bpz9ipm76m23ihjjmy0pxw0dj5i5c5mbbrj6p")))

(define-public crate-ears-0.5 (crate (name "ears") (vers "0.5.0") (deps (list (crate-dep (name "lazy_static") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "0ysck1afd7lycl00v2di431j8b9l0mkyclcg45j0qn89qjrs1klc")))

(define-public crate-ears-0.5 (crate (name "ears") (vers "0.5.1") (deps (list (crate-dep (name "lazy_static") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "11k9xiiwjxbb5fy65lgf9a2a0m1q5lmldq42b5fb2s9v7vh973j0")))

(define-public crate-ears-0.6 (crate (name "ears") (vers "0.6.0") (deps (list (crate-dep (name "lazy_static") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "0fkhs4b716jvb75h120brsaqgr2d79i08qpga231zdzbph6w4vm0")))

(define-public crate-ears-0.7 (crate (name "ears") (vers "0.7.0") (deps (list (crate-dep (name "lazy_static") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "00cwn1s2939pr34qbgxvcnwd4h3b51v4wy091k94pmhzdgi4hncd")))

(define-public crate-ears-0.8 (crate (name "ears") (vers "0.8.0") (deps (list (crate-dep (name "lazy_static") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "pkg-config") (req "^0.3") (default-features #t) (kind 1)))) (hash "04i1kf73pbbp0vp6h8n5828rakv2s9na5z1pr03qy87cg0qcahg7")))

(define-public crate-earst-0.1 (crate (name "earst") (vers "0.1.0") (deps (list (crate-dep (name "nix") (req "^0.27.1") (features (quote ("fs"))) (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.34.0") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "1jykw6ha6706d4gw786mprnn0cgjccvg1g4cx7djwhw90kh70qr8")))

(define-public crate-earst-0.1 (crate (name "earst") (vers "0.1.1") (deps (list (crate-dep (name "nix") (req "^0.27.1") (features (quote ("fs"))) (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.34.0") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "0ksyp4qk81phyggsxyw32nal4bqb7bypxw7zvydglvjkcvb6zlda")))

