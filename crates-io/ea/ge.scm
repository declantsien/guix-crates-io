(define-module (crates-io ea ge) #:use-module (crates-io))

(define-public crate-eager-0.1 (crate (name "eager") (vers "0.1.0") (hash "1lx3kdjbjs3awfi0y44vfbv5psqx4rb61yfzcc0hc4hqkmbivrxb")))

(define-public crate-eager-futures-0.0.1 (crate (name "eager-futures") (vers "0.0.1") (hash "14wi4bzgpgj743wfk4gairi0zlfkspmgf2i8pbqw6pshwccw0xzj")))

(define-public crate-eager_json-0.1 (crate (name "eager_json") (vers "0.1.0") (deps (list (crate-dep (name "serde") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)))) (hash "0g0cwfr6j40x505xx270k8zcwdx2mjf93r50kbnj033p265afbdz")))

(define-public crate-eager_log-0.1 (crate (name "eager_log") (vers "0.1.0") (deps (list (crate-dep (name "chrono") (req "^0.4.16") (features (quote ("serde" "clock"))) (default-features #t) (kind 0)))) (hash "0nchwpw8yxd3xhfpc63iccf4ajhkizyk80c8xfsgmy83mh36825f") (yanked #t)))

(define-public crate-eager_log-0.1 (crate (name "eager_log") (vers "0.1.1") (deps (list (crate-dep (name "chrono") (req "^0.4.16") (features (quote ("serde" "clock"))) (default-features #t) (kind 0)))) (hash "06cirbhn0vjq9jkdfxxmy0k3shwbsbvzzqhp1jz3fh82f6azjs6c")))

