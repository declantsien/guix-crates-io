(define-module (crates-io ea rw) #:use-module (crates-io))

(define-public crate-earwax-0.1 (crate (name "earwax") (vers "0.1.0") (deps (list (crate-dep (name "ao_rs") (req "^0.1.0") (default-features #t) (kind 2)) (crate-dep (name "gcc") (req "^0.3") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "num") (req "^0.1") (default-features #t) (kind 0)))) (hash "0irfv67wzw58z73y4imvp6kl927h3n5rw2i1svk7vdrzzcgsbqr1")))

(define-public crate-earwax-0.1 (crate (name "earwax") (vers "0.1.1") (deps (list (crate-dep (name "ao_rs") (req "^0.1.0") (default-features #t) (kind 2)) (crate-dep (name "gcc") (req "^0.3") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "num") (req "^0.1") (default-features #t) (kind 0)))) (hash "13j4ppfgnj3xf6c3d6jmqzzklby8gxvyaz1vdvlf69zsdhmvkbn1")))

(define-public crate-earwax-0.1 (crate (name "earwax") (vers "0.1.2") (deps (list (crate-dep (name "ao_rs") (req "^0.1.0") (default-features #t) (kind 2)) (crate-dep (name "gcc") (req "^0.3") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "num") (req "^0.1") (default-features #t) (kind 0)))) (hash "0qvv9wmcibf5ca2gf2l8pr9m0wrf0ap1bhwxx1p6vv7bpcq1izf4")))

(define-public crate-earwax-0.1 (crate (name "earwax") (vers "0.1.3") (deps (list (crate-dep (name "ao_rs") (req "^0.1.0") (default-features #t) (kind 2)) (crate-dep (name "gcc") (req "^0.3") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "num") (req "^0.1") (default-features #t) (kind 0)))) (hash "13v991v0v1r1a47z77pd8y3hj14j7pmzkdfv9c5dncag9y1p98z5")))

(define-public crate-earwax-0.1 (crate (name "earwax") (vers "0.1.4") (deps (list (crate-dep (name "ao_rs") (req "^0.1.0") (default-features #t) (kind 2)) (crate-dep (name "gcc") (req "^0.3") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "num") (req "^0.1") (default-features #t) (kind 0)))) (hash "1jj6rikrfm220w5flg887mqvv9skxfw2sinqxv21bw3m186qq0ki")))

(define-public crate-earwax-0.1 (crate (name "earwax") (vers "0.1.6") (deps (list (crate-dep (name "ao_rs") (req "^0.1.0") (default-features #t) (kind 2)) (crate-dep (name "gcc") (req "^0.3") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "num") (req "^0.1") (default-features #t) (kind 0)))) (hash "0gka4b04kbf45vbwpgv0jpn7bqbxmv5vc0qdmma2chximr104ay1")))

(define-public crate-earwax-0.1 (crate (name "earwax") (vers "0.1.7") (deps (list (crate-dep (name "ao_rs") (req "^0.1.0") (default-features #t) (kind 2)) (crate-dep (name "gcc") (req "^0.3") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "num") (req "^0.1") (default-features #t) (kind 0)))) (hash "1vszshq3ihg329lhxybsvhspj35h1yrjb8cmfhhgx522sy80a380")))

