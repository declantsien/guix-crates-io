(define-module (crates-io ea si) #:use-module (crates-io))

(define-public crate-easier-0.1 (crate (name "easier") (vers "0.1.0") (hash "1zha1x28xdl1l70yif6snnjk5rl798a14zbqjcbsfqzhqi6dczfq")))

(define-public crate-easier-0.2 (crate (name "easier") (vers "0.2.0") (hash "19hxsndl962805j0njskzasq76fg62jkb2c7vgq2li7s79pba4im")))

(define-public crate-easify-0.1 (crate (name "easify") (vers "0.1.0") (hash "1k6yi2x2bn8q1jqsy8ia94y6fz5kxy475kw2bs4lsp163zy63rwl")))

(define-public crate-easify-0.2 (crate (name "easify") (vers "0.2.0") (deps (list (crate-dep (name "easify-macros") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "0gi4rm0hg019gm62gljrrs4nvcd5hqnlxigm77xyrh4s34pw2cmk")))

(define-public crate-easify-0.2 (crate (name "easify") (vers "0.2.1") (deps (list (crate-dep (name "easify-macros") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "1q0lnwqd7ry62yq55kz1vb0n3c55psqbbm9j9gcbxjmynakyxw58")))

(define-public crate-easify-0.2 (crate (name "easify") (vers "0.2.2") (deps (list (crate-dep (name "easify-macros") (req "^0.1.1") (default-features #t) (kind 0)))) (hash "1w77m707vjfzbz3zml3755rfkdwd07ipd69i827z8ignm3b5qq1r")))

(define-public crate-easify-macros-0.1 (crate (name "easify-macros") (vers "0.1.0") (deps (list (crate-dep (name "proc-macro2") (req "^1.0.76") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.35") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2.0.48") (features (quote ("extra-traits" "full"))) (default-features #t) (kind 0)))) (hash "0g1551sapsm0s95rlf2xhxd8z3hxb9yl35qsvrkmzy1vv0n0fcly")))

(define-public crate-easify-macros-0.1 (crate (name "easify-macros") (vers "0.1.1") (deps (list (crate-dep (name "proc-macro2") (req "^1.0.76") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.35") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2.0.48") (features (quote ("extra-traits" "full"))) (default-features #t) (kind 0)))) (hash "1j6k7nm2dj97k5kva3zjs0lk291cx7j6wr0bgjwk2w27752cwyww")))

(define-public crate-easing-0.0.1 (crate (name "easing") (vers "0.0.1") (hash "1yd9anhp0px09wjjglillxvmhkgp6k9938kx4yh5s6a86z2sk3cv")))

(define-public crate-easing-0.0.2 (crate (name "easing") (vers "0.0.2") (hash "06npafnxxpl0w9cq4m3ji3kkzzg1jbx9nyldz0m5s96j2r4j4px4")))

(define-public crate-easing-0.0.3 (crate (name "easing") (vers "0.0.3") (hash "1jnnq3nj9ibhz6bix4sgm0hxmkqvaz7qwy5002mx5mw2bwcjdf2c")))

(define-public crate-easing-0.0.5 (crate (name "easing") (vers "0.0.5") (hash "0gszvzchknjl7jdavyq72avamxij4nnjnzpkwlx6mycx5s6wzcch")))

(define-public crate-easings-0.1 (crate (name "easings") (vers "0.1.0") (hash "1c39ykl72qrmrc8lbjhwjm1d5cf80lkj5g8px0isqb4mdsp73dm6")))

(define-public crate-easings-rs-0.1 (crate (name "easings-rs") (vers "0.1.0") (hash "0rrynz5hgv27c3zawifd9xcyppnx6cxdjcpamzx3jzfphix2y42n")))

(define-public crate-easings-rs-0.1 (crate (name "easings-rs") (vers "0.1.1") (hash "1lpa0a1xbpx2ns2zv1aj334gl4pqn7fpyx7yqb6v760zvkkmr7fk")))

