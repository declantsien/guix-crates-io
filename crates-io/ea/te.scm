(define-module (crates-io ea te) #:use-module (crates-io))

(define-public crate-eater-arc-illegal-0.1 (crate (name "eater-arc-illegal") (vers "0.1.0") (hash "11wbfzp0mzjhhr5qwhjw3h8zpmvdzz0ajz9i0p7q22q8kpgaxbnm")))

(define-public crate-eater_domainmatcher-0.1 (crate (name "eater_domainmatcher") (vers "0.1.0") (deps (list (crate-dep (name "smallvec") (req "^1") (optional #t) (default-features #t) (kind 0)))) (hash "0x29lq655baf4s6ndb5r3pg4kxnn68rhindgnn2drz1mrzjprv3x") (v 2) (features2 (quote (("smallvec" "dep:smallvec"))))))

(define-public crate-eater_domainmatcher-0.1 (crate (name "eater_domainmatcher") (vers "0.1.1") (deps (list (crate-dep (name "smallvec") (req "^1") (optional #t) (default-features #t) (kind 0)))) (hash "1cilkq5qvkk91bnk5dyxkh02lzjirwsjz7w5j8jdydsdq60d41i2") (v 2) (features2 (quote (("smallvec" "dep:smallvec"))))))

(define-public crate-eater_domainmatcher-0.1 (crate (name "eater_domainmatcher") (vers "0.1.2") (deps (list (crate-dep (name "smallvec") (req "^1") (optional #t) (default-features #t) (kind 0)))) (hash "0ngcbmwhbfbvrrdb8rgzv41zhmz13xfpjmn8495nb795xx9afsl6") (v 2) (features2 (quote (("smallvec" "dep:smallvec"))))))

(define-public crate-eater_rangeset-0.1 (crate (name "eater_rangeset") (vers "0.1.0") (deps (list (crate-dep (name "serde") (req "^1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "smallvec") (req "^1") (features (quote ("const_new"))) (optional #t) (default-features #t) (kind 0)))) (hash "1jig89r28gxsvpqih9qwszwsfwx50cv2x87mac2zy5m50kldfayv") (features (quote (("default" "smallvec")))) (v 2) (features2 (quote (("smallvec" "dep:smallvec") ("serde" "dep:serde" "smallvec/serde"))))))

(define-public crate-eater_rangeset-0.1 (crate (name "eater_rangeset") (vers "0.1.1") (deps (list (crate-dep (name "serde") (req "^1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "smallvec") (req "^1") (features (quote ("const_new"))) (optional #t) (default-features #t) (kind 0)))) (hash "18129lprlv33mfas90xyjkapry2llsns1m7bgjyrydn4dc3w2cr9") (features (quote (("default" "smallvec")))) (v 2) (features2 (quote (("smallvec" "dep:smallvec") ("serde" "dep:serde" "smallvec/serde"))))))

