(define-module (crates-io ea se) #:use-module (crates-io))

(define-public crate-ease-0.1 (crate (name "ease") (vers "0.1.0") (deps (list (crate-dep (name "hyper") (req "*") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "*") (default-features #t) (kind 0)) (crate-dep (name "serde_macros") (req "*") (default-features #t) (kind 0)) (crate-dep (name "url") (req "*") (default-features #t) (kind 0)))) (hash "1c4cs0yfzcp95ilpjsbmlkdyynk269q8n9pkk27wsypiqnv5rkcg")))

(define-public crate-ease-0.2 (crate (name "ease") (vers "0.2.0") (deps (list (crate-dep (name "hyper") (req "*") (default-features #t) (kind 0)) (crate-dep (name "rustc-serialize") (req "*") (default-features #t) (kind 0)) (crate-dep (name "url") (req "*") (default-features #t) (kind 0)))) (hash "0q4kxbx1aaqiavsp0yyp0a5ajkqh36lxgnysyjalx6k6cfbprp0r")))

(define-public crate-ease-0.2 (crate (name "ease") (vers "0.2.1") (deps (list (crate-dep (name "hyper") (req "*") (default-features #t) (kind 0)) (crate-dep (name "rustc-serialize") (req "*") (default-features #t) (kind 0)) (crate-dep (name "url") (req "*") (default-features #t) (kind 0)))) (hash "1ssi2hz9brzf6jarvfiaw29kx7n7vnz7n4jj0im88wryylbwwckm")))

(define-public crate-ease-0.3 (crate (name "ease") (vers "0.3.0") (deps (list (crate-dep (name "hyper") (req "*") (default-features #t) (kind 0)) (crate-dep (name "rustc-serialize") (req "*") (default-features #t) (kind 0)) (crate-dep (name "url") (req "*") (default-features #t) (kind 0)))) (hash "0g01dcmbg3inxirhwc95jhw24zrxx0j31rxmp9vl1xrywlkyfmsg")))

(define-public crate-ease-0.4 (crate (name "ease") (vers "0.4.1") (deps (list (crate-dep (name "hyper") (req "*") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "*") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "*") (default-features #t) (kind 0)) (crate-dep (name "serde_macros") (req "*") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "url") (req "*") (default-features #t) (kind 0)))) (hash "0i48c3cfraxfvwdh2syk2mag72fma7f872kyg2nsyrc208x2ngfv") (features (quote (("nightly" "serde_macros"))))))

(define-public crate-ease-0.4 (crate (name "ease") (vers "0.4.2") (deps (list (crate-dep (name "hyper") (req "^0.7.2") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^0.6.11") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^0.6.0") (default-features #t) (kind 0)) (crate-dep (name "serde_macros") (req "^0.6.11") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "url") (req "^0.5.3") (default-features #t) (kind 0)))) (hash "0wygcjarygjqpv6l8gyvld29hqqgf956r23njvw2341h3pwx5b4d") (features (quote (("nightly" "serde_macros"))))))

(define-public crate-ease-0.5 (crate (name "ease") (vers "0.5.0") (deps (list (crate-dep (name "hyper") (req "^0.9.4") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^0.7.10") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^0.7.1") (default-features #t) (kind 0)) (crate-dep (name "serde_macros") (req "^0.7.10") (default-features #t) (kind 2)) (crate-dep (name "url") (req "^1.1.1") (default-features #t) (kind 0)))) (hash "1chrzjxhr6r9bbvy4arc8bf2r4j6sjkwsasvnqlgjjdddw5xq365")))

(define-public crate-ease-0.6 (crate (name "ease") (vers "0.6.0") (deps (list (crate-dep (name "hyper") (req "^0.9.4") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "serde_macros") (req "^0.8") (default-features #t) (kind 2)) (crate-dep (name "url") (req "^1.1.1") (default-features #t) (kind 0)))) (hash "1ssgbgd2ilwr3wv8yq1j991lgm8p8dh169kh3j41sc3866pjph3l")))

(define-public crate-ease-0.6 (crate (name "ease") (vers "0.6.1") (deps (list (crate-dep (name "hyper") (req "^0.9.4") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "serde_macros") (req "^0.8") (default-features #t) (kind 2)) (crate-dep (name "url") (req "^1.1.1") (default-features #t) (kind 0)))) (hash "1wkc6jzq8sr4qyz4brgz1m0603n44jdyi6hlbpn0zkslrlsnzx7k")))

(define-public crate-easegress-macros-0.1 (crate (name "easegress-macros") (vers "0.1.0") (deps (list (crate-dep (name "proc-macro2") (req "^1.0.47") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.21") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.103") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "1hmqhshvy8vahz1wmk57i5wdgj753ifn6pc1nzpv47z7sbzx28kg")))

(define-public crate-easegress-macros-0.1 (crate (name "easegress-macros") (vers "0.1.1") (deps (list (crate-dep (name "proc-macro2") (req "^1.0.47") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.21") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.103") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "0km2y54p42fmkykj0mrkm65y5jg5vmd7swalyn333syillh4ppqr")))

(define-public crate-easegress-sdk-0.1 (crate (name "easegress-sdk") (vers "0.1.0") (deps (list (crate-dep (name "wee_alloc") (req "^0.4.5") (default-features #t) (kind 0)))) (hash "0yk08nn0sj5rm1iichfqjlrdmabbf2fdd24wyrp9jbawhsds8xz3")))

(define-public crate-easel-0.1 (crate (name "easel") (vers "0.1.0") (hash "0f07kz7v14rvphbp42l7clywr0mvb784dc3zjpj92f9iamcdm3wk")))

(define-public crate-easel-rs-1 (crate (name "easel-rs") (vers "1.0.4") (deps (list (crate-dep (name "bytemuck") (req "^1.4.1") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "byteorder") (req "^1.3.4") (default-features #t) (kind 0)) (crate-dep (name "chrono") (req "^0.4.19") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^3.0.0-beta.2") (default-features #t) (kind 0)) (crate-dep (name "env_logger") (req "^0.8.2") (default-features #t) (kind 0)) (crate-dep (name "futures") (req "^0.3.8") (default-features #t) (kind 0)) (crate-dep (name "half") (req "^1.6.0") (default-features #t) (kind 0)) (crate-dep (name "image") (req "^0.23.12") (default-features #t) (kind 0)) (crate-dep (name "imgui") (req "^0.6.1") (default-features #t) (kind 0)) (crate-dep (name "imgui-wgpu") (req "^0.12.0") (default-features #t) (kind 0)) (crate-dep (name "imgui-winit-support") (req "^0.6.1") (default-features #t) (kind 0)) (crate-dep (name "json") (req "^0.12.4") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.11") (default-features #t) (kind 0)) (crate-dep (name "notify") (req "^4.0.15") (default-features #t) (kind 0)) (crate-dep (name "raw-window-handle") (req "^0.3.3") (default-features #t) (kind 0)) (crate-dep (name "shaderc") (req "^0.7.0") (default-features #t) (kind 0)) (crate-dep (name "stopwatch") (req "^0.0.7") (default-features #t) (kind 0)) (crate-dep (name "wgpu") (req "^0.6.2") (default-features #t) (kind 0)) (crate-dep (name "winit") (req "^0.24.0") (default-features #t) (kind 0)))) (hash "1gadrw5b9bbdrdm4d14s7vjga3naphgk250gycqgxwgg4lgwxyib")))

(define-public crate-easer-0.1 (crate (name "easer") (vers "0.1.0") (deps (list (crate-dep (name "approx") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "1rnalxj3vssnx4miwrklbnipjdm6394nmm62h16k3mvamkiqz3n8")))

(define-public crate-easer-0.1 (crate (name "easer") (vers "0.1.1") (deps (list (crate-dep (name "approx") (req "^0.1.0") (default-features #t) (kind 2)) (crate-dep (name "gnuplot") (req "^0.0.20") (default-features #t) (kind 2)))) (hash "1ilngmfa0zqzn18wvqzsv4554qz7w6d0jp55qqqq9ncqy39av8nx")))

(define-public crate-easer-0.2 (crate (name "easer") (vers "0.2.0") (deps (list (crate-dep (name "approx") (req "^0.1") (default-features #t) (kind 2)) (crate-dep (name "gnuplot") (req "^0.0") (default-features #t) (kind 2)) (crate-dep (name "num-traits") (req "^0.1") (default-features #t) (kind 0)))) (hash "09rhjrh4l3fiz8c0008d1dwn02gxccnr5z9b4wmsgxpjzq7a2kb0")))

(define-public crate-easer-0.2 (crate (name "easer") (vers "0.2.1") (deps (list (crate-dep (name "approx") (req "^0.2") (default-features #t) (kind 2)) (crate-dep (name "gnuplot") (req "^0.0.25") (default-features #t) (kind 2)) (crate-dep (name "num-traits") (req "^0.2") (default-features #t) (kind 0)))) (hash "06hr0sjavhsnvk092dqdc9m6hq85jv7pmjjk9m9dcxmpxh64dpld")))

(define-public crate-easer-0.3 (crate (name "easer") (vers "0.3.0") (deps (list (crate-dep (name "approx") (req "^0.2") (default-features #t) (kind 2)) (crate-dep (name "gnuplot") (req "^0.0.25") (default-features #t) (kind 2)) (crate-dep (name "num-traits") (req "^0.2") (kind 0)))) (hash "05zrb310izfdd10xb9l90jc827wxvqkidfy20bg5p71wp3w299gv") (features (quote (("std" "num-traits/std") ("libm" "num-traits/libm") ("default" "std"))))))

(define-public crate-easey-1 (crate (name "easey") (vers "1.0.0") (deps (list (crate-dep (name "assert_approx_eq") (req "^1.1.0") (default-features #t) (kind 2)))) (hash "07fqjllfy46y1j8fcyps7mhx2gpfvy3q3xs1vyklxbj5xg7wqg1c")))

(define-public crate-easey-1 (crate (name "easey") (vers "1.1.0") (deps (list (crate-dep (name "assert_approx_eq") (req "^1.1.0") (default-features #t) (kind 2)))) (hash "0gq4dj4syp2n5pdabyg9428a8cd0wj1vhb59s2vr46g9gfmsiskg")))

(define-public crate-easey-1 (crate (name "easey") (vers "1.1.1") (deps (list (crate-dep (name "assert_approx_eq") (req "^1.1.0") (default-features #t) (kind 2)))) (hash "18id6n9vj04ryg2vlhpmc63cqkd7a77h87z96lwzwnzynxpcm1v4")))

(define-public crate-easey-1 (crate (name "easey") (vers "1.1.2") (deps (list (crate-dep (name "assert_approx_eq") (req "^1.1.0") (default-features #t) (kind 2)))) (hash "1jyq0ggc2jjs70z19l29an835z04vviqi879cjmyd0i7qm64w923")))

(define-public crate-easey-1 (crate (name "easey") (vers "1.2.0") (deps (list (crate-dep (name "assert_approx_eq") (req "^1.1.0") (default-features #t) (kind 2)))) (hash "1727x7rrsjj1qrglab155c1vhx645xk7akmlhjdybh2r8zzm3gpj")))

(define-public crate-easey-2 (crate (name "easey") (vers "2.0.0") (deps (list (crate-dep (name "assert_approx_eq") (req "^1.1.0") (default-features #t) (kind 2)))) (hash "0qb8jl5sagz3i9fa0xfz2amzdlz6kz92v9d7h784pgx9h6m38ryh")))

(define-public crate-easey-2 (crate (name "easey") (vers "2.1.0") (deps (list (crate-dep (name "assert_approx_eq") (req "^1.1.0") (default-features #t) (kind 2)))) (hash "0w6lrc1h7c4vr45p3gp6fmwxsfl0wycb8ki1ipcslz6rizg44nyh")))

