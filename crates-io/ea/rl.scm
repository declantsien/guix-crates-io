(define-module (crates-io ea rl) #:use-module (crates-io))

(define-public crate-earl-0.0.0 (crate (name "earl") (vers "0.0.0") (hash "1ca72kh258i5zrcyxb3ck2gk8g3cdpfkyh9mfvl4jpdyk777f2xf")))

(define-public crate-earl-0.1 (crate (name "earl") (vers "0.1.0") (deps (list (crate-dep (name "bencher") (req "^0.1") (default-features #t) (kind 2)) (crate-dep (name "encoding") (req "^0.2") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "heapsize") (req "^0.4.2") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "idna") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "matches") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "percent-encoding") (req "^1.0.0") (default-features #t) (kind 0)) (crate-dep (name "rustc-test") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "serde") (req "^1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0.94") (default-features #t) (kind 2)) (crate-dep (name "serde_json") (req "^1") (default-features #t) (kind 2)))) (hash "0j4an7hrdc6mfg3yswbn1f350mb1skn9ps9p9ygfwryi0wdsaqbd") (features (quote (("query_encoding" "encoding") ("heap_size" "heapsize")))) (yanked #t)))

(define-public crate-earl-lang-syntax-0.1 (crate (name "earl-lang-syntax") (vers "0.1.0") (hash "19vcr4wz6wzr1dwky973ik16b16wy40b17v093yjd6yrp0wpkn51")))

(define-public crate-earl-lang-syntax-1 (crate (name "earl-lang-syntax") (vers "1.0.0") (hash "0nx5js6hiqsmpn47myqv4xfvascm2rfq96aw1pxv5anh2gx95xx6")))

(define-public crate-earley-0.1 (crate (name "earley") (vers "0.1.0") (deps (list (crate-dep (name "env_logger") (req "^0.3.2") (default-features #t) (kind 2)) (crate-dep (name "log") (req "^0.3.5") (default-features #t) (kind 0)) (crate-dep (name "unicode-segmentation") (req "^0.1.2") (default-features #t) (kind 0)))) (hash "14b9d85bp2z1lnmfcqlnc7z19sn23i80lz4yyvph01hiv8hs1mn1")))

(define-public crate-earlgrey-0.0.4 (crate (name "earlgrey") (vers "0.0.4") (deps (list (crate-dep (name "lexers") (req "^0.0.4") (default-features #t) (kind 0)) (crate-dep (name "linenoise-rust") (req "^0.2") (default-features #t) (kind 2)) (crate-dep (name "regex") (req "^0.1") (default-features #t) (kind 2)))) (hash "12d4wcbfcmmh3dfi4mzgd8q4bzbvzz554lprjq0gyxm0cxbwmnc1")))

(define-public crate-earlgrey-0.0.5 (crate (name "earlgrey") (vers "0.0.5") (deps (list (crate-dep (name "lexers") (req "^0.0.4") (default-features #t) (kind 0)) (crate-dep (name "linenoise-rust") (req "^0.2") (default-features #t) (kind 2)) (crate-dep (name "regex") (req "^0.1") (default-features #t) (kind 2)))) (hash "1mwy0vvaqhxx90gli21hylf1sv0hy4rd0yy82m49xswq8pqdz3c5")))

(define-public crate-earlgrey-0.0.6 (crate (name "earlgrey") (vers "0.0.6") (deps (list (crate-dep (name "lexers") (req "^0.0.5") (default-features #t) (kind 0)) (crate-dep (name "rustyline") (req "^1.0.0") (default-features #t) (kind 2)))) (hash "1k22v1fmw6s4mgl22k5f7k8spy71k2prk993kcpr5qn912zv2x1c")))

(define-public crate-earlgrey-0.0.7 (crate (name "earlgrey") (vers "0.0.7") (deps (list (crate-dep (name "lexers") (req "^0.0.5") (default-features #t) (kind 0)) (crate-dep (name "rustyline") (req "^1.0.0") (default-features #t) (kind 2)))) (hash "0a73hrgf503ndk72wkzfq6mqcdamqbw5xj3q60rpnzfy3n17p6nd")))

(define-public crate-earlgrey-0.1 (crate (name "earlgrey") (vers "0.1.0") (deps (list (crate-dep (name "lexers") (req "^0.0.5") (default-features #t) (kind 0)) (crate-dep (name "rustyline") (req "^1.0.0") (default-features #t) (kind 2)))) (hash "1adps29zllljrj00zwmnw1h03gg7i3n5cv9q9j4adml85fwr0njg")))

(define-public crate-earlgrey-0.2 (crate (name "earlgrey") (vers "0.2.0") (deps (list (crate-dep (name "lexers") (req "^0.0.6") (default-features #t) (kind 2)) (crate-dep (name "rustyline") (req "^1.0.0") (default-features #t) (kind 2)))) (hash "0390gj3mw3cygp9di3sfq2njkqi88x61sl1s2cdd34y2gffgc2fy")))

(define-public crate-earlgrey-0.2 (crate (name "earlgrey") (vers "0.2.1") (deps (list (crate-dep (name "lexers") (req "^0.0.7") (default-features #t) (kind 2)) (crate-dep (name "rustyline") (req "^1.0.0") (default-features #t) (kind 2)))) (hash "1hzq7b2gx025xrhjx0cr821bra4w318jl2n7aqs68r2m516xxbdd")))

(define-public crate-earlgrey-0.2 (crate (name "earlgrey") (vers "0.2.3") (deps (list (crate-dep (name "lexers") (req "^0.0.8") (default-features #t) (kind 2)) (crate-dep (name "rustyline") (req "^1.0.0") (default-features #t) (kind 2)))) (hash "1a2xz02ms4f1hnyk3is477x6s9bbxprlnxc0g9vl8bmvb72jfx79") (features (quote (("debug"))))))

(define-public crate-earlgrey-0.2 (crate (name "earlgrey") (vers "0.2.4") (deps (list (crate-dep (name "lexers") (req "^0.1.2") (default-features #t) (kind 2)) (crate-dep (name "rustyline") (req "^1.0.0") (default-features #t) (kind 2)))) (hash "1y98sq3x8s4rg3hc6i94nnay52c0g7crcr3j823faarzy3qksq9m") (features (quote (("debug"))))))

(define-public crate-earlgrey-0.3 (crate (name "earlgrey") (vers "0.3.0") (deps (list (crate-dep (name "lexers") (req "^0.1.2") (default-features #t) (kind 2)) (crate-dep (name "rustyline") (req "^5.0.0") (default-features #t) (kind 2)))) (hash "15n638mzxcbpd2caxndrgfqnzjal9brs61qivmjplhrxnb6y9jal") (features (quote (("debug"))))))

(define-public crate-earlgrey-0.3 (crate (name "earlgrey") (vers "0.3.1") (deps (list (crate-dep (name "lexers") (req "^0.1.4") (default-features #t) (kind 2)) (crate-dep (name "rustyline") (req "^9.1.0") (default-features #t) (kind 2)))) (hash "1fphnyh5sxwhp916dl8y2aqhiim8n5bhrj4hxf1jkv0kz4sqfca3") (features (quote (("debug"))))))

(define-public crate-earlgrey-0.3 (crate (name "earlgrey") (vers "0.3.2") (deps (list (crate-dep (name "lexers") (req "^0.1") (default-features #t) (kind 2)) (crate-dep (name "rustyline") (req "^9.1") (default-features #t) (kind 2)))) (hash "1ka5dcx04jp76hz8y55kk7x57174b5ywsamd1bny7bqdq3hw827k") (features (quote (("debug"))))))

(define-public crate-early-0.1 (crate (name "early") (vers "0.1.0") (deps (list (crate-dep (name "itertools") (req "^0.10") (default-features #t) (kind 0)) (crate-dep (name "urlencoding") (req "^2.1") (default-features #t) (kind 0)))) (hash "124wq6f2rlz97fdx7r485igyjv0s6hnh7ksjjfjxr75xvq84s19v")))

(define-public crate-early_returns-0.1 (crate (name "early_returns") (vers "0.1.0") (hash "0rbv3sd6qfzms6czx3qij3fhpb3w0zsh4jjm3kzdjv2b61z2cv7k")))

(define-public crate-early_returns-0.2 (crate (name "early_returns") (vers "0.2.1") (hash "068szg82wdn35m9yqjavb7igdq5liab80lkiklx4z62ss085qvr4")))

(define-public crate-early_returns-0.3 (crate (name "early_returns") (vers "0.3.0") (hash "0jsg8hkkl19q7fdm18dby8086nyaslxxlhag845zdawc7fydgkfm")))

(define-public crate-early_returns-0.3 (crate (name "early_returns") (vers "0.3.1") (hash "08fb4088vxzsr6fi8x4b6c0dg98fh84lwvl6b6cr18wwrg3dhi6a")))

(define-public crate-early_returns-0.4 (crate (name "early_returns") (vers "0.4.0") (hash "1fk5030zqkmpglznjs5b3qmsyw5i0iyasv2c4a3fy9k7av93jngk")))

