(define-module (crates-io ea r2) #:use-module (crates-io))

(define-public crate-ear2ctl-0.1 (crate (name "ear2ctl") (vers "0.1.0") (deps (list (crate-dep (name "bluer") (req "^0.16.1") (features (quote ("full"))) (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^4.4.12") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "futures") (req "^0.3.30") (default-features #t) (kind 0)) (crate-dep (name "strum") (req "^0.25.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.35.1") (default-features #t) (kind 0)))) (hash "15q158j5r5yglkdjnf75a38hz8q64b1km6lcfsk8fqsq8knf3kgp")))

