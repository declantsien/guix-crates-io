(define-module (crates-io ea tn) #:use-module (crates-io))

(define-public crate-eatnodemodules-0.1 (crate (name "eatnodemodules") (vers "0.1.0") (deps (list (crate-dep (name "glob") (req "^0.3.1") (default-features #t) (kind 0)))) (hash "161vhwkvqgjkgm6iqcbagza0zfc75ja6lbgj4p5bgiyxq2rmwfbb")))

(define-public crate-eatnodemodules-0.1 (crate (name "eatnodemodules") (vers "0.1.1") (deps (list (crate-dep (name "glob") (req "^0.3.1") (default-features #t) (kind 0)))) (hash "07n091c4kxh0zc8kq43flgr2civ5i86d7kfw2ccpbydyr4cvxhss")))

(define-public crate-eatnodemodules-0.1 (crate (name "eatnodemodules") (vers "0.1.2") (deps (list (crate-dep (name "glob") (req "^0.3.1") (default-features #t) (kind 0)))) (hash "0gjbrifr61x34ddm1szzsqyy3kyb8dppjms621kj3mbd36gsdrcl")))

(define-public crate-eatnodemodules-0.1 (crate (name "eatnodemodules") (vers "0.1.3") (deps (list (crate-dep (name "glob") (req "^0.3.1") (default-features #t) (kind 0)))) (hash "0sf0hp791wkjrjkzy98k2kd63xbvqd5bx23wwg5carkl296476ys")))

(define-public crate-eatnodemodules-0.1 (crate (name "eatnodemodules") (vers "0.1.4") (deps (list (crate-dep (name "fs_extra") (req "^1.2.0") (default-features #t) (kind 0)))) (hash "1idla2w20vw0m633dch69jli4wrnq1dk041dr2qwgzjs01p5mppp")))

(define-public crate-eatnp-0.1 (crate (name "eatnp") (vers "0.1.4") (deps (list (crate-dep (name "fs_extra") (req "^1.2.0") (default-features #t) (kind 0)))) (hash "0pw3yh8rm2z92smvpb2jrldcvyw15jfj11is8kxzjbsjgljag20g")))

(define-public crate-eatnp-0.1 (crate (name "eatnp") (vers "0.1.5") (deps (list (crate-dep (name "fs_extra") (req "^1.2.0") (default-features #t) (kind 0)))) (hash "1b7d0j0szgvbi5yy9qqfmfy0igcps7ff97wz6b990043nxv7rp7r")))

