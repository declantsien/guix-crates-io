(define-module (crates-io ea gl) #:use-module (crates-io))

(define-public crate-eaglesong-0.1 (crate (name "eaglesong") (vers "0.1.0") (deps (list (crate-dep (name "blake2b-rs") (req "^0.1.4") (default-features #t) (kind 2)) (crate-dep (name "criterion") (req "^0.2") (default-features #t) (kind 2)))) (hash "095q35a1aymc0nfgzzqncv4zs0n6z4zxkj0gbjdsps23sgaqp5wd")))

(define-public crate-eagleye-0.0.0 (crate (name "eagleye") (vers "0.0.0") (hash "0yb69ckly42hwnwjkvn7fzql397hm8nnsjnfidywr1z1k0gl4ymj")))

