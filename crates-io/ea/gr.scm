(define-module (crates-io ea gr) #:use-module (crates-io))

(define-public crate-eagre-asn1-0.1 (crate (name "eagre-asn1") (vers "0.1.0") (deps (list (crate-dep (name "byteorder") (req "^0.4.2") (default-features #t) (kind 0)))) (hash "1ln2zyfqry7s2fw3qv6v1pjaxm6k8223014h967zjbfgfhmj6i55")))

(define-public crate-eagre-asn1-0.2 (crate (name "eagre-asn1") (vers "0.2.0") (deps (list (crate-dep (name "byteorder") (req "^0.4.2") (default-features #t) (kind 0)))) (hash "1fk8hchsgf7mriz1miqhl8fkynyyjgrgh1173qq1bxxjzi3wfmx7")))

(define-public crate-eagre-asn1-0.3 (crate (name "eagre-asn1") (vers "0.3.0") (deps (list (crate-dep (name "byteorder") (req "^0.4.2") (default-features #t) (kind 0)))) (hash "0pqg7x36cb497c9lshmw0cj4p2r2rr2ywdbi774z70mazmqrc6d4")))

