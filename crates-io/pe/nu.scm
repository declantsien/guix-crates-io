(define-module (crates-io pe nu) #:use-module (crates-io))

(define-public crate-penum-0.1 (crate (name "penum") (vers "0.1.0") (deps (list (crate-dep (name "proc-macro2") (req "^1.0.51") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.23") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.109") (features (quote ("parsing"))) (default-features #t) (kind 0)))) (hash "10rj841s5b0da1ms0qxlkjlgin8rizv5bnhzb8slhfws1klhm20g")))

(define-public crate-penum-0.1 (crate (name "penum") (vers "0.1.1") (deps (list (crate-dep (name "proc-macro2") (req "^1.0.51") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.23") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.109") (features (quote ("parsing"))) (default-features #t) (kind 0)))) (hash "0y39gw0iyw8v901d0ffxnc0bbpbn2s1a2kq3dn055lsihvgbrlr1")))

(define-public crate-penum-0.1 (crate (name "penum") (vers "0.1.2") (deps (list (crate-dep (name "proc-macro2") (req "^1.0.51") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.23") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.109") (features (quote ("parsing"))) (default-features #t) (kind 0)))) (hash "1cwxlcfnf5gy1kds0vjk2vx0wym7bw9286f16qx3nb3lf5pdkhv2")))

(define-public crate-penum-0.1 (crate (name "penum") (vers "0.1.3") (deps (list (crate-dep (name "proc-macro2") (req "^1.0.51") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.23") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.109") (features (quote ("parsing"))) (default-features #t) (kind 0)))) (hash "062207cdzriknyahkfkqzqlc4mf59k1145pah4ynxb460i3mnixf")))

(define-public crate-penum-0.1 (crate (name "penum") (vers "0.1.4") (deps (list (crate-dep (name "proc-macro2") (req "^1.0.51") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.23") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.109") (features (quote ("parsing"))) (default-features #t) (kind 0)))) (hash "1fhnanhz901p5mx5js4yd5kcfgan3vg4x3jqb0jwhvs6wzq10hcx")))

(define-public crate-penum-0.1 (crate (name "penum") (vers "0.1.5") (deps (list (crate-dep (name "proc-macro2") (req "^1.0.51") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.23") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.109") (features (quote ("parsing"))) (default-features #t) (kind 0)))) (hash "0bn0nsl0q9gmjdwd7324m10cphpklav6n1p5zv7ijp0wd8mxcxnj")))

(define-public crate-penum-0.1 (crate (name "penum") (vers "0.1.6") (deps (list (crate-dep (name "proc-macro2") (req "^1.0.51") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.23") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.109") (features (quote ("parsing"))) (default-features #t) (kind 0)))) (hash "1q2msa96i62zgpi816bjl92hz8l3ww9xkx07va04ha0n14p2ar3x")))

(define-public crate-penum-0.1 (crate (name "penum") (vers "0.1.7") (deps (list (crate-dep (name "proc-macro2") (req "^1.0.51") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.23") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.109") (features (quote ("parsing"))) (default-features #t) (kind 0)))) (hash "0kfgqjv7az4zwisqcilqg4d7qchx3z7xqqrl1l8hpmiipgmpz0i9")))

(define-public crate-penum-0.1 (crate (name "penum") (vers "0.1.8") (deps (list (crate-dep (name "proc-macro2") (req "^1.0.51") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.23") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.109") (features (quote ("parsing"))) (default-features #t) (kind 0)))) (hash "1bhil8jr1jbhyfdzirvmwl9vl6k9dks6in9x6vbnrcxvy0pmv7mi")))

(define-public crate-penum-0.1 (crate (name "penum") (vers "0.1.9") (deps (list (crate-dep (name "proc-macro2") (req "^1.0.51") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.23") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.109") (features (quote ("parsing"))) (default-features #t) (kind 0)))) (hash "0m7d1mgn7i55f7fk1n8z0xj2ibknx0vcksbmcxdxijarbmy9ld8m")))

(define-public crate-penum-0.1 (crate (name "penum") (vers "0.1.10") (deps (list (crate-dep (name "proc-macro2") (req "^1.0.51") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.23") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.109") (features (quote ("parsing" "full"))) (default-features #t) (kind 0)))) (hash "0nvbdmp92ggn5l7p7idrq9pwjhayvzrmga2r2kml31bi5a5cr5w4")))

(define-public crate-penum-0.1 (crate (name "penum") (vers "0.1.11") (deps (list (crate-dep (name "proc-macro2") (req "^1.0.51") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.23") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.109") (features (quote ("parsing" "full"))) (default-features #t) (kind 0)) (crate-dep (name "trybuild") (req "^1.0.79") (default-features #t) (kind 0)))) (hash "0n8fk3ryfxbr8v17pm8dfv35dzd7bdbrmrcdb5lf2v4qhq7fjy25")))

(define-public crate-penum-0.1 (crate (name "penum") (vers "0.1.12") (deps (list (crate-dep (name "proc-macro2") (req "^1.0.51") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.23") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.109") (features (quote ("parsing" "full"))) (default-features #t) (kind 0)) (crate-dep (name "trybuild") (req "^1.0.79") (default-features #t) (kind 0)))) (hash "0z1nmcmbqk550pvz77ziwd3cn32cn4szm14c0darwbgb9xqvwbb5")))

(define-public crate-penum-0.1 (crate (name "penum") (vers "0.1.13") (deps (list (crate-dep (name "proc-macro2") (req "^1.0.51") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.23") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.109") (features (quote ("full" "visit" "visit-mut"))) (default-features #t) (kind 0)) (crate-dep (name "trybuild") (req "^1.0.79") (default-features #t) (kind 0)))) (hash "1si1z9pwn81daw428p9vllhi8pbx3pv9ghqvzpy4wkhp0j3klpjs")))

(define-public crate-penum-0.1 (crate (name "penum") (vers "0.1.14") (deps (list (crate-dep (name "proc-macro2") (req "^1.0.51") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.23") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.109") (features (quote ("full" "visit" "visit-mut"))) (default-features #t) (kind 0)) (crate-dep (name "trybuild") (req "^1.0.79") (default-features #t) (kind 0)))) (hash "0by1ajq06lqggz0cda1hgv6np0pws2cxn5bf1qxb3rr94r74pk3m")))

(define-public crate-penum-0.1 (crate (name "penum") (vers "0.1.15") (deps (list (crate-dep (name "proc-macro2") (req "^1.0.51") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.23") (default-features #t) (kind 0)) (crate-dep (name "rustfmt") (req "^0.10.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.109") (features (quote ("full" "visit" "visit-mut" "extra-traits"))) (default-features #t) (kind 0)) (crate-dep (name "trybuild") (req "^1.0.79") (default-features #t) (kind 0)))) (hash "1wy3900qj1hjl3yv3iayz70as7jhimklgzj2r8svjqszqxf1slhy")))

(define-public crate-penum-0.1 (crate (name "penum") (vers "0.1.16") (deps (list (crate-dep (name "proc-macro2") (req "^1.0.51") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.23") (default-features #t) (kind 0)) (crate-dep (name "rustfmt") (req "^0.10.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.109") (features (quote ("full" "visit" "visit-mut" "extra-traits"))) (default-features #t) (kind 0)) (crate-dep (name "trybuild") (req "^1.0.79") (default-features #t) (kind 0)))) (hash "12mqswj839dz1b7lf9sphbxsazh9m5n2bl8lsrw4kjqypfx2iab2")))

(define-public crate-penum-0.1 (crate (name "penum") (vers "0.1.17") (deps (list (crate-dep (name "proc-macro2") (req "^1.0.51") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.23") (default-features #t) (kind 0)) (crate-dep (name "rustfmt") (req "^0.10.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.109") (features (quote ("full" "visit" "visit-mut" "extra-traits"))) (default-features #t) (kind 0)) (crate-dep (name "trybuild") (req "^1.0.79") (default-features #t) (kind 0)))) (hash "1rphzbz655v50by3ijws16rnr9xsvw4x9xp4v7b6krgwl58kwsxa")))

(define-public crate-penum-0.1 (crate (name "penum") (vers "0.1.18") (deps (list (crate-dep (name "proc-macro2") (req "^1.0.51") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.23") (default-features #t) (kind 0)) (crate-dep (name "rustfmt") (req "^0.10.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.109") (features (quote ("full" "visit" "visit-mut" "extra-traits"))) (default-features #t) (kind 0)) (crate-dep (name "trybuild") (req "^1.0.79") (default-features #t) (kind 0)))) (hash "02i9dyz6vh3cxbf5asihmq62697yjfqkyvpw1r0hvbq37pcdyyjn")))

(define-public crate-penum-0.1 (crate (name "penum") (vers "0.1.19") (deps (list (crate-dep (name "proc-macro2") (req "^1.0.51") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.23") (default-features #t) (kind 0)) (crate-dep (name "rustfmt") (req "^0.10.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.109") (features (quote ("full" "visit" "visit-mut" "extra-traits"))) (default-features #t) (kind 0)) (crate-dep (name "trybuild") (req "^1.0.79") (default-features #t) (kind 0)))) (hash "18d8a5swdf3cs5ffni9c15qas7jy8xl58rpzxmyrzy4v55ba3knm") (features (quote (("dispatch-std") ("dispatch-custom") ("default" "dispatch-std"))))))

(define-public crate-penum-0.1 (crate (name "penum") (vers "0.1.20") (deps (list (crate-dep (name "proc-macro2") (req "^1.0.51") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.23") (default-features #t) (kind 0)) (crate-dep (name "rustfmt") (req "^0.10.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.109") (features (quote ("full" "visit" "visit-mut" "extra-traits"))) (default-features #t) (kind 0)) (crate-dep (name "trybuild") (req "^1.0.79") (default-features #t) (kind 0)))) (hash "1w4xqbb6v138iqifsnssmv5rxk7ipairmx76bcb2mm2b3s3x4vfv") (features (quote (("dispatch-std") ("dispatch-custom") ("default" "dispatch-std"))))))

(define-public crate-penum-0.1 (crate (name "penum") (vers "0.1.21") (deps (list (crate-dep (name "cargo-release") (req "^0.24.8") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1.0.51") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.23") (default-features #t) (kind 0)) (crate-dep (name "rustfmt") (req "^0.10.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.109") (features (quote ("full" "visit" "visit-mut" "extra-traits"))) (default-features #t) (kind 0)) (crate-dep (name "trybuild") (req "^1.0.79") (default-features #t) (kind 0)))) (hash "1vxvfy23zykhx7zbjbf01984hbgyq4vbmfi7dnlsfpv2k74x6a88") (features (quote (("dispatch-std") ("dispatch-custom") ("default" "dispatch-std"))))))

(define-public crate-penum-0.1 (crate (name "penum") (vers "0.1.22") (deps (list (crate-dep (name "cargo-release") (req "^0.24.8") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1.0.51") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.23") (default-features #t) (kind 0)) (crate-dep (name "rustfmt") (req "^0.10.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.109") (features (quote ("full" "visit" "visit-mut" "extra-traits"))) (default-features #t) (kind 0)) (crate-dep (name "trybuild") (req "^1.0.79") (default-features #t) (kind 0)))) (hash "0r6wfz4cz0jryb8a292vqndjj2b47dy7bbg3m0s9l9q3scvizba8") (features (quote (("dispatch-std") ("dispatch-custom") ("default" "dispatch-std"))))))

(define-public crate-penum-0.1 (crate (name "penum") (vers "0.1.23") (deps (list (crate-dep (name "cargo-release") (req "^0.24.8") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1.0.51") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.23") (default-features #t) (kind 0)) (crate-dep (name "rustfmt") (req "^0.10.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.109") (features (quote ("full" "visit" "visit-mut" "extra-traits"))) (default-features #t) (kind 0)) (crate-dep (name "trybuild") (req "^1.0.79") (default-features #t) (kind 0)))) (hash "0gfvg05i9k3cwxk5x0alghp5hbznygmarl60p9yvh0c7ggpdmcmg") (features (quote (("dispatch-std") ("dispatch-custom") ("default" "dispatch-std"))))))

(define-public crate-penum-0.1 (crate (name "penum") (vers "0.1.24") (deps (list (crate-dep (name "cargo-release") (req "^0.24.8") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1.0.51") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.23") (default-features #t) (kind 0)) (crate-dep (name "rustfmt") (req "^0.10.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.109") (features (quote ("full" "visit" "visit-mut" "extra-traits"))) (default-features #t) (kind 0)) (crate-dep (name "trybuild") (req "^1.0.79") (default-features #t) (kind 0)))) (hash "08wyxsk2yqyxz5sf0lk1r5yzxyxwi7bw31531dy53dh42vqxnwzi") (features (quote (("dispatch-std") ("dispatch-custom") ("default" "dispatch-std"))))))

(define-public crate-penum-0.1 (crate (name "penum") (vers "0.1.25") (deps (list (crate-dep (name "cargo-release") (req "^0.24.8") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1.0.51") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.23") (default-features #t) (kind 0)) (crate-dep (name "rustfmt") (req "^0.10.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.109") (features (quote ("full" "visit" "visit-mut" "extra-traits"))) (default-features #t) (kind 0)) (crate-dep (name "trybuild") (req "^1.0.79") (default-features #t) (kind 0)))) (hash "1a1ilzy6hv1zkfj3wxsc5qc5ils9pk9cc6dysm2wgq3axw2m06z6") (features (quote (("dispatch-std") ("dispatch-custom") ("default" "dispatch-std"))))))

(define-public crate-penum-0.1 (crate (name "penum") (vers "0.1.26") (deps (list (crate-dep (name "cargo-release") (req "^0.24.8") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1.0.51") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.23") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.109") (features (quote ("full" "visit" "visit-mut" "extra-traits"))) (default-features #t) (kind 0)) (crate-dep (name "trybuild") (req "^1.0.79") (default-features #t) (kind 0)))) (hash "1i2a9ljb8bq4adddj0gzi5q7y4i24ld248v74pbjizgajzap45hw") (features (quote (("dispatch-std") ("dispatch-custom") ("default" "dispatch-std"))))))

(define-public crate-penum-0.1 (crate (name "penum") (vers "0.1.27") (deps (list (crate-dep (name "cargo-release") (req "^0.24.8") (default-features #t) (kind 0)) (crate-dep (name "itertools") (req "^0.10.5") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1.0.51") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.23") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.109") (features (quote ("full" "visit" "visit-mut" "extra-traits"))) (default-features #t) (kind 0)) (crate-dep (name "trybuild") (req "^1.0.79") (default-features #t) (kind 0)))) (hash "1qabm5ryzcivw08chvcnff7lcgy6kczq4x4crz8hq31rkpfrhhfm") (features (quote (("dispatch-std") ("dispatch-custom") ("default" "dispatch-std"))))))

(define-public crate-penum-0.1 (crate (name "penum") (vers "0.1.28") (deps (list (crate-dep (name "cargo-release") (req "^0.24.8") (default-features #t) (kind 0)) (crate-dep (name "itertools") (req "^0.10.5") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1.0.51") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.23") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.109") (features (quote ("full" "visit" "visit-mut" "extra-traits"))) (default-features #t) (kind 0)) (crate-dep (name "trybuild") (req "^1.0.79") (default-features #t) (kind 0)))) (hash "1yy980w385bkzrm4n33ydh6lwrf8q4n7scijpbcckzl3kk7c6hs5") (features (quote (("dispatch-std") ("dispatch-custom") ("default" "dispatch-std"))))))

(define-public crate-penum-0.1 (crate (name "penum") (vers "0.1.29") (deps (list (crate-dep (name "cargo-release") (req "^0.24.8") (default-features #t) (kind 0)) (crate-dep (name "itertools") (req "^0.10.5") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1.0.51") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.23") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.109") (features (quote ("full" "visit" "visit-mut" "extra-traits"))) (default-features #t) (kind 0)) (crate-dep (name "trybuild") (req "^1.0.79") (default-features #t) (kind 0)))) (hash "1d5p1lfq3s9ylpgv8sv0yhyrrmp79c8bacpdm6kyxvksyxl8nd8z") (features (quote (("dispatch-std") ("dispatch-custom") ("default" "dispatch-std"))))))

(define-public crate-penumbra-0.1 (crate (name "penumbra") (vers "0.1.0") (deps (list (crate-dep (name "chrono") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^2.32") (features (quote ("suggestions"))) (kind 0)) (crate-dep (name "fern") (req "^0.5") (default-features #t) (kind 0)) (crate-dep (name "futures") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "http") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "hyper") (req "^0.13") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.9") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^0.2") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "1kq0g0s1krv8zqrffvirgkhickiym0zzy7rmwpdshf8c7dx6qcxi")))

(define-public crate-penumbra-app-0.1 (crate (name "penumbra-app") (vers "0.1.0") (hash "0hnciwwb2m5dabhpnjz1pl3ki50w1v7jr2hhz4b2i60j0gss8jz3")))

(define-public crate-penumbra-asset-0.1 (crate (name "penumbra-asset") (vers "0.1.0") (hash "1fb41lz33gnzz2mzq166y43sgs8xfsqdjsdybq8wvj55ap3cv7i8")))

(define-public crate-penumbra-chain-0.1 (crate (name "penumbra-chain") (vers "0.1.0") (hash "0s1kcqzblafd1x91s6r4khv5rnq4an6ihcirhq1ry6k07zqih7jf")))

(define-public crate-penumbra-compact-block-0.1 (crate (name "penumbra-compact-block") (vers "0.1.0") (hash "18cffcjvhy4kdhrs0x4xcmix15a07hpd5d0pic57y3pdxy883pi2")))

(define-public crate-penumbra-component-0.1 (crate (name "penumbra-component") (vers "0.1.0") (hash "1r7gaijn3vkpyy4f0dcyayick0kqg4v7l8pdaz21izqkcx54b0xa")))

(define-public crate-penumbra-crypto-0.1 (crate (name "penumbra-crypto") (vers "0.1.0") (hash "058z4as7m3si8w4rlyzs4ga4bmyk0l0bx2a27xqj5z0swl1q0z7j")))

(define-public crate-penumbra-custody-0.1 (crate (name "penumbra-custody") (vers "0.1.0") (hash "03dyldg6255gn5iqba5qgsanyix21fw149krcii6bsyc6nbvccw6")))

(define-public crate-penumbra-daemon-0.1 (crate (name "penumbra-daemon") (vers "0.1.0") (hash "1k2yy1pvsy531bsqb700lwma6lgk0l29piaqph57gipganrlh56i")))

(define-public crate-penumbra-dao-0.1 (crate (name "penumbra-dao") (vers "0.1.0") (hash "0lmmw4mdn61nnrwx8c9sdxaska6a3bj467fdms8v3z7ib20x1xbh")))

(define-public crate-penumbra-dex-0.1 (crate (name "penumbra-dex") (vers "0.1.0") (hash "0fvsrncryll5ym6xk4i1vayfpvy7pvd1qnrkgvz9g7xq1cr2qhc7")))

(define-public crate-penumbra-distributions-0.1 (crate (name "penumbra-distributions") (vers "0.1.0") (hash "0dy199184fpfwarh9hnaprjjwr3rhsv5k8mnahlh94bl1gyb4qak")))

(define-public crate-penumbra-eddy-0.1 (crate (name "penumbra-eddy") (vers "0.1.0") (hash "0js0gv05vaw0mqngm9a5xi89nggzf8vw2x9galg7a7qphqqck51r")))

(define-public crate-penumbra-fee-0.1 (crate (name "penumbra-fee") (vers "0.1.0") (hash "035c2l5fg171fgghczlzq4b2hvfi7ii2j9dshkdyfmq201gjgily")))

(define-public crate-penumbra-fmd-0.1 (crate (name "penumbra-fmd") (vers "0.1.0") (hash "1q67mn6ix9wv4j4ifyds7c159had16jmxsnzm7ph1q3zkm1nww0i")))

(define-public crate-penumbra-governance-0.1 (crate (name "penumbra-governance") (vers "0.1.0") (hash "0zn3b46gq6bkjv7b6c7di68lpd5afprhc3af5m19pckr9ifkvzp6")))

(define-public crate-penumbra-hub-0.1 (crate (name "penumbra-hub") (vers "0.1.0") (hash "19xj8vc1j1awxy3mnjqhb4sf243v1300qrda4vj7lmdfkf5m0ld7")))

(define-public crate-penumbra-ibc-0.1 (crate (name "penumbra-ibc") (vers "0.1.0") (hash "1c59w81i6178pf9s7hagh5dm53akakiw0hlbdilw2gnfcv298gpr")))

(define-public crate-penumbra-keys-0.1 (crate (name "penumbra-keys") (vers "0.1.0") (hash "0j5agjrcqjzxc7r29v53ljd7ndkxxjxa5s10hki1ycr4l09aswq9")))

(define-public crate-penumbra-measure-0.1 (crate (name "penumbra-measure") (vers "0.1.0") (hash "08rim2j9z9kmz0bpx97qch8nlsx6mjl3h95drnz7xjk8br8pab4h")))

(define-public crate-penumbra-num-0.1 (crate (name "penumbra-num") (vers "0.1.0") (hash "1dh00jp6mxmhrlnx07sgazfriznbbsn47bhircfs44k7wgsc7kif")))

(define-public crate-penumbra-parameter-setup-0.1 (crate (name "penumbra-parameter-setup") (vers "0.1.0") (hash "1f9ipdy78hy59pjm89pwqbi9fpzvrahiv7sj1j0glqsraq3hj0gb")))

(define-public crate-penumbra-pd-0.1 (crate (name "penumbra-pd") (vers "0.1.0") (hash "1k7r26wr9qpkf89pmj029yxdmqyglm8avbk0hiq3m7f9y8abyf0a")))

(define-public crate-penumbra-proof-params-0.1 (crate (name "penumbra-proof-params") (vers "0.1.0") (hash "0dl4pjrn84wylm82wafrb0baxlnl8rs31wf9h23pdn0avyhpn0zl")))

(define-public crate-penumbra-proto-0.1 (crate (name "penumbra-proto") (vers "0.1.0") (hash "1iv1pcdkqn7gq28d89p671rgh4fjy1dm3p2sv6f4gz3cl109302z")))

(define-public crate-penumbra-proto-compiler-0.1 (crate (name "penumbra-proto-compiler") (vers "0.1.0") (hash "10hpk69pxjj8zqga6mvihkijp41mz25l6m4sr383dbvvmyccvihl")))

(define-public crate-penumbra-sct-0.1 (crate (name "penumbra-sct") (vers "0.1.0") (hash "0nayxrvz3ymy9vdizb5j67c0zc9ccx1qa5a5c219g0cg8zypzw3s")))

(define-public crate-penumbra-sdk-0.1 (crate (name "penumbra-sdk") (vers "0.1.0") (hash "0h97hmypa6g2fwfrbg88dl743ci40pc40p8a25bys2qx188w6nng")))

(define-public crate-penumbra-shielded-pool-0.1 (crate (name "penumbra-shielded-pool") (vers "0.1.0") (hash "0xl49gwwka4klwjsxi07303phw61392592w3haap7hd9mqmndfpw")))

(define-public crate-penumbra-stake-0.1 (crate (name "penumbra-stake") (vers "0.1.0") (hash "1kxv20749hzqasszk6wgjj4chq59jmzqlmyf4ad2603jhnkd3jgb")))

(define-public crate-penumbra-storage-0.1 (crate (name "penumbra-storage") (vers "0.1.0") (hash "1j34g04hh1pnqs6wpppq69zc8pxrskr7q5i6zfy7fbfgxaply2r3")))

(define-public crate-penumbra-tct-0.1 (crate (name "penumbra-tct") (vers "0.1.0") (hash "14zxi608xywjz5wiahhgy4g4gpci1ksckq1wm7p93vlanl3mxsim")))

(define-public crate-penumbra-tct-property-test-0.1 (crate (name "penumbra-tct-property-test") (vers "0.1.0") (hash "0v9cx8agz5rbdy75bpr20h5wyqz3kb35kxgigmj04gx4kacrs4va")))

(define-public crate-penumbra-tct-visualize-0.1 (crate (name "penumbra-tct-visualize") (vers "0.1.0") (hash "0q33jvswnrja9szrvm2dx420z7vs73lp4l0900vvbvnqr60ykg94")))

(define-public crate-penumbra-tendermint-proxy-0.1 (crate (name "penumbra-tendermint-proxy") (vers "0.1.0") (hash "1gx6045pccmsn76r2ai7j3336cy99fqybqxn1nc2qf7f5pan3494")))

(define-public crate-penumbra-tower-trace-0.1 (crate (name "penumbra-tower-trace") (vers "0.1.0") (hash "08fi5qh0q2h7gff922nnicmhs8g4g6f5zpgxmac6ljdfdam489g8")))

(define-public crate-penumbra-transaction-0.1 (crate (name "penumbra-transaction") (vers "0.1.0") (hash "0gnnkdw0644d4vwghyl8cxws5fvbj3vna59q4d5yy4y3fbxhd40s")))

(define-public crate-penumbra-utils-0.1 (crate (name "penumbra-utils") (vers "0.1.0") (hash "0wkxilhr9hr7v3zs6xpr7gnqiwld0zyp7dj877dhi0bdfjzn2nhq")))

(define-public crate-penumbra-view-0.1 (crate (name "penumbra-view") (vers "0.1.0") (hash "1v35vc9x07175kbmni97qchjli4jnizhgqzpzlnjn4yrk11i54a1")))

(define-public crate-penumbra-wallet-0.1 (crate (name "penumbra-wallet") (vers "0.1.0") (hash "0ih6qa73sc192jqa0krw6kibwp8z4ij1rpbvs5r0fkkw751abhjr")))

(define-public crate-penumbra-wasm-0.1 (crate (name "penumbra-wasm") (vers "0.1.0") (hash "1sxzkl2fab7r9cdn4zp2117bhzxg5dqy2mfkv3i9y1whm4y3h6nl")))

(define-public crate-penut-rs-0.1 (crate (name "penut-rs") (vers "0.1.0") (hash "0x31y0wvad9kl3g8qzzb6f215yi08d8a84ji9mk0j5zssj2cgakg")))

