(define-module (crates-io pe m-) #:use-module (crates-io))

(define-public crate-pem-iterator-0.1 (crate (name "pem-iterator") (vers "0.1.0") (deps (list (crate-dep (name "pem") (req "^0.4") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.3") (default-features #t) (kind 2)))) (hash "124vh94631q98sl97kq6320g6w4axkqy14sjikpxzjqmwxvrsqqa") (features (quote (("store_label") ("std") ("default" "std" "store_label"))))))

(define-public crate-pem-iterator-0.2 (crate (name "pem-iterator") (vers "0.2.0") (deps (list (crate-dep (name "pem") (req "^0.4") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.3") (default-features #t) (kind 2)))) (hash "09kzqsh9zkyib9gavgy2gc7pjvfznmpk505dfyq0qa9yll0dmja5") (features (quote (("std") ("generators") ("default" "std"))))))

(define-public crate-pem-parser-0.1 (crate (name "pem-parser") (vers "0.1.0") (deps (list (crate-dep (name "openssl") (req "*") (default-features #t) (kind 2)) (crate-dep (name "regex") (req "*") (default-features #t) (kind 0)) (crate-dep (name "rustc-serialize") (req "*") (default-features #t) (kind 0)) (crate-dep (name "tempdir") (req "*") (default-features #t) (kind 2)))) (hash "1qf7w4jyzmj0hlgpymy04kj08m40vgzdwvq1af70hwx6sn3vvppv")))

(define-public crate-pem-parser-0.1 (crate (name "pem-parser") (vers "0.1.1") (deps (list (crate-dep (name "openssl") (req "*") (default-features #t) (kind 2)) (crate-dep (name "regex") (req "*") (default-features #t) (kind 0)) (crate-dep (name "rustc-serialize") (req "*") (default-features #t) (kind 0)))) (hash "1l3splkmb125vqrgj2agi6047afaa45id3mrs45drhn16aj9hda4")))

(define-public crate-pem-rfc7468-0.0.0 (crate (name "pem-rfc7468") (vers "0.0.0") (hash "0bqaxacrrldxy05nmfpx4g1vg23091rc77hkai9p15kqvqayh47g") (yanked #t)))

(define-public crate-pem-rfc7468-0.1 (crate (name "pem-rfc7468") (vers "0.1.0") (deps (list (crate-dep (name "base64ct") (req "^1") (default-features #t) (kind 0)))) (hash "0q90pz0vb3vcl9dfzy5j0j0s4gs106yrkxg2vfrppfp8dw58wdan") (features (quote (("std" "alloc") ("alloc")))) (yanked #t)))

(define-public crate-pem-rfc7468-0.1 (crate (name "pem-rfc7468") (vers "0.1.1") (deps (list (crate-dep (name "base64ct") (req "^1") (default-features #t) (kind 0)))) (hash "0ns0i0arlcpkh3ygsgx9xxrfy2jwkd85gr4xi9rxs7x2p0dgh7l0") (features (quote (("std" "alloc") ("alloc"))))))

(define-public crate-pem-rfc7468-0.2 (crate (name "pem-rfc7468") (vers "0.2.0") (deps (list (crate-dep (name "base64ct") (req "^1") (default-features #t) (kind 0)))) (hash "04vc2kxzy9wvq7nlhivrw6qbljyjvi2il6m4clk485wsik3r1zmq") (features (quote (("std" "alloc") ("alloc"))))))

(define-public crate-pem-rfc7468-0.2 (crate (name "pem-rfc7468") (vers "0.2.1") (deps (list (crate-dep (name "base64ct") (req "^1") (default-features #t) (kind 0)))) (hash "1mdgdknswfqkp8nh7006wilzm95whj9racianjs79q2kf9ay1a1c") (features (quote (("std" "alloc") ("alloc"))))))

(define-public crate-pem-rfc7468-0.2 (crate (name "pem-rfc7468") (vers "0.2.2") (deps (list (crate-dep (name "base64ct") (req "^1") (default-features #t) (kind 0)))) (hash "0r82g7gy2wp8j8zm19bdm01xdsml1x0xkrmd5bap2lm107ab47z7") (features (quote (("std" "alloc") ("alloc"))))))

(define-public crate-pem-rfc7468-0.2 (crate (name "pem-rfc7468") (vers "0.2.3") (deps (list (crate-dep (name "base64ct") (req "^1") (default-features #t) (kind 0)))) (hash "15yljsciaz07wmgl391s34zx8lmpyr6b5x4zkslr8cjr7h7fn8lg") (features (quote (("std" "alloc") ("alloc"))))))

(define-public crate-pem-rfc7468-0.2 (crate (name "pem-rfc7468") (vers "0.2.4") (deps (list (crate-dep (name "base64ct") (req ">=1, <1.2") (default-features #t) (kind 0)))) (hash "1m1c9jypydzabg4yscplmvff7pdcc8gg4cqg081hnlf03hxkmsc4") (features (quote (("std" "alloc") ("alloc"))))))

(define-public crate-pem-rfc7468-0.3 (crate (name "pem-rfc7468") (vers "0.3.0") (deps (list (crate-dep (name "base64ct") (req "^1.2") (default-features #t) (kind 0)))) (hash "1vijz4iyw842iwpw9vbbsk22sqksbwqnzwxr3nprmz12h19m84mg") (features (quote (("std" "alloc") ("alloc")))) (rust-version "1.56")))

(define-public crate-pem-rfc7468-0.3 (crate (name "pem-rfc7468") (vers "0.3.1") (deps (list (crate-dep (name "base64ct") (req "^1") (default-features #t) (kind 0)))) (hash "0c7vrrksg8fqzxb7q4clzl14f0qnqky7jqspjqi4pailiybmvph1") (features (quote (("std" "alloc") ("alloc")))) (rust-version "1.56")))

(define-public crate-pem-rfc7468-0.4 (crate (name "pem-rfc7468") (vers "0.4.0-pre.0") (deps (list (crate-dep (name "base64ct") (req "=1.4.0-pre.0") (default-features #t) (kind 0)))) (hash "0zvznxwzfal4zxwra8jwaghjxm0cvqrjdvignwnpjsb3g7vcxwpz") (features (quote (("std" "alloc") ("alloc")))) (rust-version "1.56")))

(define-public crate-pem-rfc7468-0.4 (crate (name "pem-rfc7468") (vers "0.4.0-pre.1") (deps (list (crate-dep (name "base64ct") (req "^1.4") (default-features #t) (kind 0)))) (hash "1mjfjqnp13y87xvjmq15d24c11f850l3k37q9826g5knsjb8yhh6") (features (quote (("std" "alloc" "base64ct/std") ("alloc" "base64ct/alloc")))) (rust-version "1.56")))

(define-public crate-pem-rfc7468-0.4 (crate (name "pem-rfc7468") (vers "0.4.0") (deps (list (crate-dep (name "base64ct") (req "^1.4") (default-features #t) (kind 0)))) (hash "1489q0axcw7i16f1rgi4k4p1sfz6sk5gbs4nxnfd6c330zk19xv2") (features (quote (("std" "alloc" "base64ct/std") ("alloc" "base64ct/alloc")))) (rust-version "1.56")))

(define-public crate-pem-rfc7468-0.5 (crate (name "pem-rfc7468") (vers "0.5.0") (deps (list (crate-dep (name "base64ct") (req "^1.4") (default-features #t) (kind 0)))) (hash "0hlmq7hsgcdg6k6blwrcmdiirwyzc858hbzpjzs8s8f0chiyn9kf") (features (quote (("std" "alloc" "base64ct/std") ("alloc" "base64ct/alloc")))) (yanked #t) (rust-version "1.56")))

(define-public crate-pem-rfc7468-0.5 (crate (name "pem-rfc7468") (vers "0.5.1") (deps (list (crate-dep (name "base64ct") (req "^1.4") (default-features #t) (kind 0)))) (hash "0cmrnp8q5x66ldgvgp7p6mv0nrvdyf3x3ypgwm4dmb5a7420fglp") (features (quote (("std" "alloc" "base64ct/std") ("alloc" "base64ct/alloc")))) (rust-version "1.56")))

(define-public crate-pem-rfc7468-0.6 (crate (name "pem-rfc7468") (vers "0.6.0") (deps (list (crate-dep (name "base64ct") (req "^1.4") (default-features #t) (kind 0)))) (hash "1b5d8rvc4lgwxhs72m99fnrg0wq7bqh4x4wq0c7501ci7a1mkl94") (features (quote (("std" "alloc" "base64ct/std") ("alloc" "base64ct/alloc")))) (rust-version "1.56")))

(define-public crate-pem-rfc7468-0.7 (crate (name "pem-rfc7468") (vers "0.7.0") (deps (list (crate-dep (name "base64ct") (req "^1.4") (default-features #t) (kind 0)))) (hash "04l4852scl4zdva31c1z6jafbak0ni5pi0j38ml108zwzjdrrcw8") (features (quote (("std" "alloc" "base64ct/std") ("alloc" "base64ct/alloc")))) (rust-version "1.60")))

(define-public crate-pem-rfc7468-1 (crate (name "pem-rfc7468") (vers "1.0.0-pre.0") (deps (list (crate-dep (name "base64ct") (req "^1.4") (default-features #t) (kind 0)))) (hash "0zf23n156jp6jsc8q25xdifsmmh8kyacky5k0n40ys6i4wf5x9kn") (features (quote (("std" "alloc" "base64ct/std") ("alloc" "base64ct/alloc")))) (rust-version "1.60")))

