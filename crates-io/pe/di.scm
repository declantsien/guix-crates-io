(define-module (crates-io pe di) #:use-module (crates-io))

(define-public crate-pedia-0.0.0 (crate (name "pedia") (vers "0.0.0") (hash "1hiw9mx04a4pxp83sxhl7lskjx0gjv45s3s8k52zc3csbcfs26vm")))

(define-public crate-pedigree-0.3 (crate (name "pedigree") (vers "0.3.0") (deps (list (crate-dep (name "methylome") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "ndarray") (req "^0.15") (features (quote ("blas"))) (default-features #t) (kind 0)) (crate-dep (name "petgraph") (req "^0.6.2") (default-features #t) (kind 0)) (crate-dep (name "progress_bars") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.40") (default-features #t) (kind 0)))) (hash "0mv04y5g5l9i4p5rlfy1pkv0falin28371714p2s1w0jf1lkmgxp")))

(define-public crate-pedis-core-0.0.1 (crate (name "pedis-core") (vers "0.0.1") (hash "1kcm4n4w7miyhvlxq3y5h5p51mmwwdgx8mjcq26g3az9wfz5s1fh") (rust-version "1.78")))

(define-public crate-pedis-core-0.0.2 (crate (name "pedis-core") (vers "0.0.2") (hash "0b1751hhdpa0mh5q40wp8v43qfhgvfdd501pghcp65fd1g9rw1xx") (rust-version "1.78")))

(define-public crate-pedis-core-0.0.3 (crate (name "pedis-core") (vers "0.0.3") (hash "168pazqkd7rl9cdrmq7i53x4b5wmqhz7y8i7dll8xv8h36ls32yd") (rust-version "1.77.2")))

(define-public crate-pedit-1 (crate (name "pedit") (vers "1.0.0") (deps (list (crate-dep (name "cotton") (req "^0.0.9") (default-features #t) (kind 0)) (crate-dep (name "diff") (req "^0.1.12") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.8") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.3.4") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.3.2") (default-features #t) (kind 0)))) (hash "1q7szngk5w1rsqndkppl9a3dhxjng48mj2f6c23yk2s7k8mvn82j")))

