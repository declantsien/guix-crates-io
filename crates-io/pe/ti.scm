(define-module (crates-io pe ti) #:use-module (crates-io))

(define-public crate-petit-0.0.1 (crate (name "petit") (vers "0.0.1") (deps (list (crate-dep (name "anyhow") (req "^1.0.38") (default-features #t) (kind 0)) (crate-dep (name "base64") (req "^0.13.0") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^3.0.0-beta.2") (default-features #t) (kind 0)) (crate-dep (name "kuon") (req "^0.0.18") (default-features #t) (kind 0)) (crate-dep (name "maplit") (req "^1.0.2") (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.11.1") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.123") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.62") (default-features #t) (kind 0)) (crate-dep (name "termion") (req "^1.5.6") (default-features #t) (kind 0)) (crate-dep (name "textwrap") (req "^0.13.3") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.2.0") (default-features #t) (kind 0)) (crate-dep (name "tui") (req "^0.14.0") (default-features #t) (kind 0)) (crate-dep (name "unicode-width") (req "^0.1.8") (default-features #t) (kind 0)))) (hash "1ixg0lsh6ip4v2ar95lav8h6lr72sgw7mviacwd94xv8y9j7fxpr")))

(define-public crate-petite-0.0.0 (crate (name "petite") (vers "0.0.0") (hash "0cpa8nqnikk0dvq48jc1pii6mp0w4qvd3yfp11q6bb6pl1ymiacf")))

(define-public crate-petitset-0.1 (crate (name "petitset") (vers "0.1.0") (deps (list (crate-dep (name "thiserror") (req "^1.0") (optional #t) (default-features #t) (kind 0)))) (hash "1wbnw84y1nlx2svnscmqggqzyfn9fh1v8rd2cya375sb009xm24i") (features (quote (("thiserror_trait" "thiserror") ("set_algebra"))))))

(define-public crate-petitset-0.1 (crate (name "petitset") (vers "0.1.1") (deps (list (crate-dep (name "thiserror") (req "^1.0") (optional #t) (default-features #t) (kind 0)))) (hash "0ca3jzfj9n3bzaqzpi9fmf0jfp0pc5mwbg5r65bgamb144zc9xlz") (features (quote (("thiserror_trait" "thiserror") ("set_algebra"))))))

(define-public crate-petitset-0.1 (crate (name "petitset") (vers "0.1.2") (deps (list (crate-dep (name "thiserror") (req "^1.0") (optional #t) (default-features #t) (kind 0)))) (hash "1mrnpl0lhckvaypman2nryd101li1lvm8dql88pf98x85f3isbxa") (features (quote (("thiserror_trait" "thiserror") ("set_algebra"))))))

(define-public crate-petitset-0.2 (crate (name "petitset") (vers "0.2.0") (deps (list (crate-dep (name "serde") (req "^1.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0") (optional #t) (default-features #t) (kind 0)))) (hash "18aj1lfn23r832fybd34wvjkchpg890dkyg6ncac6wd84f095kx3") (features (quote (("std") ("set_algebra")))) (v 2) (features2 (quote (("thiserror" "dep:thiserror" "std") ("serde" "dep:serde" "std"))))))

(define-public crate-petitset-0.2 (crate (name "petitset") (vers "0.2.1") (deps (list (crate-dep (name "serde") (req "^1.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0") (optional #t) (default-features #t) (kind 0)))) (hash "1b32v1hzxiww5khs6qg3hslk4da5iv326v3mspv6llm247c506kb") (features (quote (("thiserror_compat" "thiserror" "std") ("std") ("set_algebra") ("serde_compat" "serde" "std"))))))

