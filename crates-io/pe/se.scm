(define-module (crates-io pe se) #:use-module (crates-io))

(define-public crate-pesel-0.1 (crate (name "pesel") (vers "0.1.0") (deps (list (crate-dep (name "rand") (req "^0.7.0") (default-features #t) (kind 0)))) (hash "0aypvb911hphll8sr7v9s3kbb22dc44275lxcgb46lvpkml9w5yn")))

(define-public crate-pesel-0.1 (crate (name "pesel") (vers "0.1.1") (deps (list (crate-dep (name "rand") (req "^0.7.0") (default-features #t) (kind 0)))) (hash "0xkfzgfnvdjgkzgj5d9danl0ciwyn38622z435zfrn3bf8p862d1")))

(define-public crate-pesel-0.1 (crate (name "pesel") (vers "0.1.2") (deps (list (crate-dep (name "chrono") (req "^0.4.7") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.7.0") (default-features #t) (kind 0)))) (hash "1gp57k2qsbna5f2kdvc3pwhnnp95ixfb26hxc55r5n3q56rhsy2j")))

(define-public crate-pesel-0.1 (crate (name "pesel") (vers "0.1.3") (deps (list (crate-dep (name "chrono") (req "^0.4.7") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.7.0") (default-features #t) (kind 0)))) (hash "15wiiiysah23mnxhbgl7lijhj0687yxwmbm8kfc1s5vcl34d92w2")))

