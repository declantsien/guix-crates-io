(define-module (crates-io pe gt) #:use-module (crates-io))

(define-public crate-pegtastic-0.1 (crate (name "pegtastic") (vers "0.1.0") (deps (list (crate-dep (name "pegtastic-macros") (req "=0.1.0") (default-features #t) (kind 0)) (crate-dep (name "pegtastic-runtime") (req "=0.1.0") (default-features #t) (kind 0)) (crate-dep (name "trybuild") (req "^1.0") (default-features #t) (kind 2)))) (hash "1pharann35lzp9lp14mvkcc3xvzviszvlnj3rr4hm8kns3450n30") (features (quote (("trace" "pegtastic-macros/trace"))))))

(define-public crate-pegtastic-macros-0.1 (crate (name "pegtastic-macros") (vers "0.1.0") (deps (list (crate-dep (name "pegtastic-runtime") (req "=0.1.0") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)))) (hash "14h8x9m9gcrjgxn65a28757hhbvv5473p7rixpal2q3pmqlr4ml8") (features (quote (("trace"))))))

(define-public crate-pegtastic-runtime-0.1 (crate (name "pegtastic-runtime") (vers "0.1.0") (hash "0yhqm5gf1bsd7g31v7ssyn4rl3sa1h9160a1rbxxvlryyxvsy6yf")))

(define-public crate-pegtx-0.1 (crate (name "pegtx") (vers "0.1.1") (deps (list (crate-dep (name "csv") (req "^1.1.1") (default-features #t) (kind 0)) (crate-dep (name "indicatif") (req "^0.13.0") (default-features #t) (kind 0)) (crate-dep (name "pegnetd") (req "^0.1.2") (default-features #t) (kind 0)) (crate-dep (name "rust_decimal") (req "^1.0.3") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.102") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.3.5") (default-features #t) (kind 0)))) (hash "07a02xi5d9dmkkr5m4wwy3a6di87awlnhzg2lsvlpm16wvx707zh")))

(define-public crate-pegtx-0.1 (crate (name "pegtx") (vers "0.1.4") (deps (list (crate-dep (name "csv") (req "^1.1.1") (default-features #t) (kind 0)) (crate-dep (name "indicatif") (req "^0.13.0") (default-features #t) (kind 0)) (crate-dep (name "pegnetd") (req "^0.1.2") (default-features #t) (kind 0)) (crate-dep (name "rust_decimal") (req "^1.0.3") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.102") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.3.5") (default-features #t) (kind 0)))) (hash "1x319nm2cgs9w6hsl2bdyyl949jdrb3bmy8xklz0k3mrmpfw4c1i")))

