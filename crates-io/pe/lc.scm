(define-module (crates-io pe lc) #:use-module (crates-io))

(define-public crate-pelcodrs-0.2 (crate (name "pelcodrs") (vers "0.2.0") (deps (list (crate-dep (name "bitflags") (req "^1.1.0") (default-features #t) (kind 0)))) (hash "058rhnbvam2bq7lcw2kj2p9n0f0hlnxy51irys6plhh9bj6q55v6")))

(define-public crate-pelcodrs-0.2 (crate (name "pelcodrs") (vers "0.2.1") (deps (list (crate-dep (name "bitflags") (req "^1.1.0") (default-features #t) (kind 0)))) (hash "13hf7sih4zf1l9xjhgr8gqk9gws5cj25hcnmxyrwlmipbjkfrai8")))

