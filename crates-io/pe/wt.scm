(define-module (crates-io pe wt) #:use-module (crates-io))

(define-public crate-pewter-0.0.1 (crate (name "pewter") (vers "0.0.1-prerelease") (deps (list (crate-dep (name "bitflags") (req "^2.2.1") (default-features #t) (kind 0)) (crate-dep (name "snafu") (req "^0.7.4") (default-features #t) (kind 0)))) (hash "1qbv0snqwzplhbz54csg2xgkhbs0kxrim43jxx5qp98sr05pwz8k") (features (quote (("std") ("fast-rw") ("default" "std" "fast-rw"))))))

(define-public crate-pewter-0.0.2 (crate (name "pewter") (vers "0.0.2") (deps (list (crate-dep (name "bitflags") (req "^2.2.1") (default-features #t) (kind 0)))) (hash "07v3lfndkzs6y2xin82vkq45fb8ks2z4dyjhc9pgzz05ixj7nh9c") (features (quote (("std") ("fast-rw") ("default" "std" "fast-rw"))))))

(define-public crate-pewter-0.0.3 (crate (name "pewter") (vers "0.0.3") (deps (list (crate-dep (name "bitflags") (req "^2.2.1") (default-features #t) (kind 0)))) (hash "1sjnfzrjp08gs1xic7cmmqb92ssxffcmxvhbcb9abf72lrzdsspn") (features (quote (("std") ("fast-rw") ("default" "std" "fast-rw"))))))

