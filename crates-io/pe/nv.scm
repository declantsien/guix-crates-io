(define-module (crates-io pe nv) #:use-module (crates-io))

(define-public crate-penv-0.1 (crate (name "penv") (vers "0.1.0") (deps (list (crate-dep (name "console") (req "^0.15") (default-features #t) (kind 0)) (crate-dep (name "glob") (req "^0.3") (default-features #t) (kind 0)))) (hash "1cq7y3b7nyrs070csmmyh1dyxrs3hyfs95wmqx6z721brm6bmzdv")))

