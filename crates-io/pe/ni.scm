(define-module (crates-io pe ni) #:use-module (crates-io))

(define-public crate-peniko-0.1 (crate (name "peniko") (vers "0.1.0") (deps (list (crate-dep (name "kurbo") (req "^0.11") (kind 0)) (crate-dep (name "smallvec") (req "^1.8.0") (default-features #t) (kind 0)))) (hash "1iahbny2ck1vyqzrkjbib9x1q7ms7qsbgjm4v5ahar0xc3n7zbya") (features (quote (("std" "kurbo/std") ("libm" "kurbo/libm") ("default" "std")))) (rust-version "1.70")))

(define-public crate-peniko-0.1 (crate (name "peniko") (vers "0.1.1") (deps (list (crate-dep (name "kurbo") (req "^0.11.0") (kind 0)) (crate-dep (name "serde") (req "^1.0.203") (features (quote ("alloc" "derive"))) (optional #t) (kind 0)) (crate-dep (name "serde_bytes") (req "^0.11.14") (features (quote ("alloc"))) (optional #t) (kind 0)) (crate-dep (name "smallvec") (req "^1.13.2") (default-features #t) (kind 0)))) (hash "1a8pm7q2alr8n2ivfhqblg4cwbvaqhf1kbc0pdb7i0wk80lxfa1w") (features (quote (("std" "kurbo/std") ("libm" "kurbo/libm") ("default" "std")))) (v 2) (features2 (quote (("serde" "smallvec/serde" "kurbo/serde" "dep:serde_bytes" "dep:serde")))) (rust-version "1.70")))

(define-public crate-penis2-0.1 (crate (name "penis2") (vers "0.1.0") (hash "0fzvfy4v053wlhb9w68xaa82ymazmahx4gr8wx1m10bfqf23b48a")))

