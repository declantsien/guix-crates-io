(define-module (crates-io pe ll) #:use-module (crates-io))

(define-public crate-pella-0.0.1 (crate (name "pella") (vers "0.0.1") (hash "01413gyb8nwx08ip36a71rs7lpla3k5id4kmyckbfr6f3xjlvnl7")))

(define-public crate-pellet-0.0.0 (crate (name "pellet") (vers "0.0.0") (hash "0advb72mc8a4crrb8ap663d0i1mv4i0rx1b66ij56sv23wy4kc2j")))

