(define-module (crates-io pe gg) #:use-module (crates-io))

(define-public crate-peggle-0.1 (crate (name "peggle") (vers "0.1.0") (deps (list (crate-dep (name "num") (req "^0.4") (default-features #t) (kind 0)))) (hash "13158v655xx4k1fq7jgmh5g3k3l3ff0xclwprhg8m59b4gyfzggf")))

(define-public crate-peggle-derive-0.1 (crate (name "peggle-derive") (vers "0.1.0") (deps (list (crate-dep (name "peggle") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2.0") (features (quote ("full" "parsing"))) (default-features #t) (kind 0)))) (hash "13hn230j8vkj5spl957j4pi1hrrl593d2jxcgdyv8g9bpp9v7w3p")))

(define-public crate-peggler-0.1 (crate (name "peggler") (vers "0.1.0") (hash "0bxfsag3517rn8hbdp31zqjxkibjhzhap93h56hzv1mf69gimk9b") (yanked #t)))

(define-public crate-peggy-0.0.1 (crate (name "peggy") (vers "0.0.1") (hash "0nzbqj9kfv8dgkn1bsv68j87y5h6v9n3jyhljxci6x4ym7c8476i")))

