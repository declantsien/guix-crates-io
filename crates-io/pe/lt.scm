(define-module (crates-io pe lt) #:use-module (crates-io))

(define-public crate-pelt-0.1 (crate (name "pelt") (vers "0.1.0") (deps (list (crate-dep (name "fnv") (req "^1.0.3") (default-features #t) (kind 0)))) (hash "15337y7xw3636kgrl5ym7lyl7m1fb9s72ayr1znxli234vh8gjy9")))

