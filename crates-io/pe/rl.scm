(define-module (crates-io pe rl) #:use-module (crates-io))

(define-public crate-perl-0.0.0 (crate (name "perl") (vers "0.0.0") (hash "094ynzr3kdfad79wsshbwp8fk2nmfrf12vyxg5rmwc3ma0kaz5fq")))

(define-public crate-perl-0.1 (crate (name "perl") (vers "0.1.3") (hash "1v6b9gqxqf4p279c33m9sa740lbsx9kfb4221a3gim9y5yl336gg")))

(define-public crate-perl-critic-sarif-0.1 (crate (name "perl-critic-sarif") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^4.5.0") (features (quote ("derive" "string"))) (default-features #t) (kind 0)) (crate-dep (name "eyre") (req "^0.6.12") (default-features #t) (kind 0)) (crate-dep (name "git2") (req "^0.18.2") (features (quote ("vendored-libgit2"))) (kind 0)) (crate-dep (name "regex") (req "^1.10.3") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.196") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde-sarif") (req "^0.4.2") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.113") (default-features #t) (kind 0)))) (hash "1mlf6fcn7a80yb818wqlnn6cq5zh1xp8nwp33dp4q11dvz172s2z")))

(define-public crate-perlin-0.1 (crate (name "perlin") (vers "0.1.0") (deps (list (crate-dep (name "lazy_static") (req "^0.2.1") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "walkdir") (req "^0.1") (default-features #t) (kind 2)))) (hash "0bnfdc6w3hzl9wjckrd52yvs1ic24y093d1d38l2n4db2j7dh3hf")))

(define-public crate-perlin2-0.1 (crate (name "perlin2") (vers "0.1.0") (deps (list (crate-dep (name "rand") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "rand_xorshift") (req "^0.3") (default-features #t) (kind 2)))) (hash "1a32j8cmac0bk2a3414h5dbyh79x9rv6laxhsl7wlvh3yz2j9xjr")))

(define-public crate-perlin2-0.1 (crate (name "perlin2") (vers "0.1.1") (deps (list (crate-dep (name "rand") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "rand_xorshift") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "serde") (req "^1.0") (features (quote ("serde_derive"))) (optional #t) (default-features #t) (kind 0)))) (hash "1fgrk6xwbg042wkaidvzhk4frxkmbi5v0w5xr87nvcmpfmcmlnzy") (features (quote (("serialize" "serde"))))))

(define-public crate-perlin2d-0.1 (crate (name "perlin2d") (vers "0.1.0") (hash "0m49bv94vjqvfmf4nszw3jwdc7i0xbb8c9qa9lxjizxq6a2vs8gs") (yanked #t)))

(define-public crate-perlin2d-0.2 (crate (name "perlin2d") (vers "0.2.0") (hash "13rhliv7rcvmlm5zk9zhfqafcvzxxd4mpmlh896wsb3j0vjg9wnq") (yanked #t)))

(define-public crate-perlin2d-0.2 (crate (name "perlin2d") (vers "0.2.5") (hash "14f9396fwck9fy7n7hndy0kmaknacjyvm655axyrrh68pnnj6rgw") (yanked #t)))

(define-public crate-perlin2d-0.2 (crate (name "perlin2d") (vers "0.2.6") (hash "1q6df4828arnk283hqdb9p5xkjydhr3ljnghc8qc9690v77p2v5v")))

(define-public crate-perlin_noise-1 (crate (name "perlin_noise") (vers "1.0.0") (deps (list (crate-dep (name "rand") (req "^0.5.0") (default-features #t) (kind 0)))) (hash "1kjwb500w0x4xgj38pzcf38ppqhsqsklg3w68mvqvfxvhr0xr2ll")))

(define-public crate-perlin_noise-1 (crate (name "perlin_noise") (vers "1.0.1") (deps (list (crate-dep (name "rand") (req "^0.5.0") (default-features #t) (kind 0)))) (hash "1yk1pmnyz55w6jjsw1s3x26icnl5rh8kp9bj351dqpbjapd65ji3")))

(define-public crate-perlin_rust-0.1 (crate (name "perlin_rust") (vers "0.1.0") (hash "0qx9cw8rqgjxxrajq142fmvb51rzrz59ygqx3plvgccjqhsgv6cw")))

