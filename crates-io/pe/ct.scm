(define-module (crates-io pe ct) #:use-module (crates-io))

(define-public crate-pectin-0.0.2 (crate (name "pectin") (vers "0.0.2") (deps (list (crate-dep (name "git-testament") (req "^0.2.5") (default-features #t) (kind 1)) (crate-dep (name "quake-util") (req "^0.3.2") (default-features #t) (kind 0)) (crate-dep (name "tcl") (req "^0.1.9") (default-features #t) (kind 0)))) (hash "1j27cqwbz4bf7nknc2aqvppyhc3a97xcvsiaaakggxpjzxl3psz3")))

(define-public crate-pectin-0.0.3 (crate (name "pectin") (vers "0.0.3") (deps (list (crate-dep (name "git-testament") (req "^0.2.5") (default-features #t) (kind 1)) (crate-dep (name "quake-util") (req "^0.3.2") (default-features #t) (kind 0)) (crate-dep (name "tcl") (req "^0.1.9") (default-features #t) (kind 0)))) (hash "044pja5sifhdrxdfbclb9wygrskdsdksxf9r3qfgzrir8n24yx2r")))

