(define-module (crates-io pe lf) #:use-module (crates-io))

(define-public crate-pelf-0.1 (crate (name "pelf") (vers "0.1.1") (deps (list (crate-dep (name "log") (req "^0.4.17") (default-features #t) (kind 0)) (crate-dep (name "nom") (req "^7.1.3") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.40") (default-features #t) (kind 0)))) (hash "14i0220j0cppflnvqa282njrg4j3qspnzabka8wq8iqnk704lp9j")))

(define-public crate-pelf-0.1 (crate (name "pelf") (vers "0.1.2") (deps (list (crate-dep (name "log") (req "^0.4.17") (default-features #t) (kind 0)) (crate-dep (name "nom") (req "^7.1.3") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.40") (default-features #t) (kind 0)))) (hash "0zb8kk06kz1d996d7dfncddncqbxyjsp89kj9njx9wc79pdkci3s")))

(define-public crate-pelf-0.1 (crate (name "pelf") (vers "0.1.3") (deps (list (crate-dep (name "log") (req "^0.4.17") (default-features #t) (kind 0)) (crate-dep (name "nom") (req "^7.1.3") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.40") (default-features #t) (kind 0)))) (hash "0cv8axz13a4v5s82fvxm5f54fabwxg0g3igdjwb4xqjw987pwib8")))

(define-public crate-pelf-0.1 (crate (name "pelf") (vers "0.1.4") (deps (list (crate-dep (name "log") (req "^0.4.17") (default-features #t) (kind 0)) (crate-dep (name "nom") (req "^7.1.3") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.40") (default-features #t) (kind 0)))) (hash "0pc6v292fbvzsc7n54x7qk1nj0gy5jvi9saxlnb71vqk3kjdzld1")))

(define-public crate-pelf-0.1 (crate (name "pelf") (vers "0.1.5") (deps (list (crate-dep (name "log") (req "^0.4.17") (default-features #t) (kind 0)) (crate-dep (name "nom") (req "^7.1.3") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.40") (default-features #t) (kind 0)))) (hash "0igw41zhjzca5b03lisbkkcyf3vhbpkh0hjdg791xbvm7xa5ip0n")))

