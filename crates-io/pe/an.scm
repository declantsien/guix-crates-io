(define-module (crates-io pe an) #:use-module (crates-io))

(define-public crate-peano-1 (crate (name "peano") (vers "1.0.0") (hash "0rdy1aqkkbrnac1gf0sl2r3s1nk5xdk7y6d1n1i43ba3jbk3sbqy")))

(define-public crate-peano-1 (crate (name "peano") (vers "1.0.1") (hash "06idswphqn4qwbgxaixaj0cajlp0vc43yyn0bmjv8w0ig3lifbfp")))

(define-public crate-peano-1 (crate (name "peano") (vers "1.0.2") (hash "0h2jl9kddw1d2kr4lpmpr8fcniy2rlwp4mhsngjya97avj2c13ba")))

(define-public crate-peano-axioms-0.1 (crate (name "peano-axioms") (vers "0.1.0") (deps (list (crate-dep (name "local-type-alias") (req "^0.1.4") (default-features #t) (kind 0)))) (hash "06mjrp6pr592shv15snv65l4qi8689ixcagv7k2yayb5klmx35av")))

(define-public crate-peano-natural-0.1 (crate (name "peano-natural") (vers "0.1.0") (hash "0n7fd77vhb6kf7mmc98c5y8km9c8v6zw8fc9y855n8klkr7x4ddm")))

(define-public crate-peano-natural-0.2 (crate (name "peano-natural") (vers "0.2.0") (hash "1rqbhv9bxs708wf5d0vq0d4c610pcvw5d9di8izr77id8khkhhyn")))

(define-public crate-peano-natural-0.2 (crate (name "peano-natural") (vers "0.2.1") (hash "044v0higaw4khn9160ch3cmbbymzc2y3hn3jmvhii7k53h5x73if")))

(define-public crate-peanut-0.0.0 (crate (name "peanut") (vers "0.0.0") (hash "0dbpid7zvrwkr9jh17yslrg3k2x6vfyv89rnggwfvhsnwbk1jjkv")))

