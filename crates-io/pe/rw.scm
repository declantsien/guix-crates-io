(define-module (crates-io pe rw) #:use-module (crates-io))

(define-public crate-perw-0.0.1 (crate (name "perw") (vers "0.0.1-prerelease") (deps (list (crate-dep (name "snafu") (req "^0.7.4") (default-features #t) (kind 0)))) (hash "1vk0jgkmjlmhxmrkm3xgpm9b5wc0p27j1zr04wvdvgmi6bb4gfsx") (features (quote (("default" "alloc") ("alloc"))))))

