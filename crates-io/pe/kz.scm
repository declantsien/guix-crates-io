(define-module (crates-io pe kz) #:use-module (crates-io))

(define-public crate-pekzep_numeral-0.1 (crate (name "pekzep_numeral") (vers "0.1.0") (hash "0fbjws5nqhhvhf7amkifxjnia8j3q8cb63pk4zsb1b5s0fbs9i85")))

(define-public crate-pekzep_numeral-0.2 (crate (name "pekzep_numeral") (vers "0.2.0") (hash "0703681aw98nm79pa7h9bchrw2p34kd47glhh2y68f3n4fjxrda3")))

(define-public crate-pekzep_syllable-0.1 (crate (name "pekzep_syllable") (vers "0.1.0") (hash "1845gyj1h07wddpq8yc73h59p3mxjxm513cl5wh9x0zsy9hr1fik")))

(define-public crate-pekzep_syllable-0.1 (crate (name "pekzep_syllable") (vers "0.1.1") (hash "1q4bmq7mqxq4jq22hsmjnprp7d68qm82rppmlv9wjfss28czv650")))

(define-public crate-pekzep_syllable-0.1 (crate (name "pekzep_syllable") (vers "0.1.2") (hash "09f6ypl7pxgr8vrbc1kxxwjby5p7xasjvpqixfxwfkssgcmfprnc")))

