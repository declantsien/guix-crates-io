(define-module (crates-io pe dr) #:use-module (crates-io))

(define-public crate-pedro-0.0.1 (crate (name "pedro") (vers "0.0.1") (hash "1y5hx8zx38dfjql5akxf1f8f4x4c461i6cp87dymicgcss1vnd9q")))

(define-public crate-pedro_add_one-0.1 (crate (name "pedro_add_one") (vers "0.1.0") (hash "0fzc4gch67dgc479757glvhxm92cbhb5r63gsnpi4d4vpvvcszjx")))

(define-public crate-pedrov-0.1 (crate (name "pedrov") (vers "0.1.0") (hash "0c0bvx2gx6aqiiq6h059dhfdlncxag5nf4xk88fqx8fjjwr6r9r2")))

(define-public crate-pedrov-0.2 (crate (name "pedrov") (vers "0.2.0") (hash "1yiy7cryixzrp0h9nykiw2z2rnkvpiamqycjsdbidv8667bnqhja")))

