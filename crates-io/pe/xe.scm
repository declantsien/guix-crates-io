(define-module (crates-io pe xe) #:use-module (crates-io))

(define-public crate-pexels-0.1 (crate (name "pexels") (vers "0.1.0") (deps (list (crate-dep (name "reqwest") (req "^0.9.16") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.39") (default-features #t) (kind 0)))) (hash "06i89vsmz12vx58gxzw8hk0n8yafz8g5q4yb2zd5mflbpssfysbj")))

(define-public crate-pexels-0.1 (crate (name "pexels") (vers "0.1.1") (deps (list (crate-dep (name "reqwest") (req "^0.9.16") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.39") (default-features #t) (kind 0)))) (hash "003g9i3drpsq4czwi6f841qbhl66p0mf86imqwrilcypbindpizm")))

(define-public crate-pexels-0.1 (crate (name "pexels") (vers "0.1.2") (deps (list (crate-dep (name "reqwest") (req "^0.9.16") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.39") (default-features #t) (kind 0)))) (hash "04hx9q5wq5ha3idi6pr4mls9g280fgkdlh7f9dn12yap2xn7nd23")))

(define-public crate-pexels-0.2 (crate (name "pexels") (vers "0.2.1") (deps (list (crate-dep (name "reqwest") (req "^0.9.16") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.39") (default-features #t) (kind 0)))) (hash "0fqmmrqri3nb890g6bm02bzdjrv60gsfcklnbxn3l3q7npak4nwz")))

(define-public crate-pexels-uri-0.1 (crate (name "pexels-uri") (vers "0.1.0") (deps (list (crate-dep (name "url") (req "^2.3.1") (default-features #t) (kind 0)))) (hash "04vilp1y9y3hhz5a7k8yr0h2z758wq6vfsf8q7y345vvrnxp0ysm")))

(define-public crate-pexels_client-0.1 (crate (name "pexels_client") (vers "0.1.0") (deps (list (crate-dep (name "reqwest") (req "^0.11.16") (features (quote ("json"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.160") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.96") (default-features #t) (kind 0)))) (hash "0b8i59wswlhx1q89zh11p0gvxd0db8m5s1556bdk0vr7iaj771mc")))

(define-public crate-pexels_client-0.1 (crate (name "pexels_client") (vers "0.1.1") (deps (list (crate-dep (name "reqwest") (req "^0.11.16") (features (quote ("json"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.160") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.96") (default-features #t) (kind 0)))) (hash "1razdcqsfb0h5088h5pp13wpj8c1j3y7pn36ljfqgrbb78adxqqa")))

