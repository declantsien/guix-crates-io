(define-module (crates-io pe ss) #:use-module (crates-io))

(define-public crate-pessimize-1 (crate (name "pessimize") (vers "1.0.0") (deps (list (crate-dep (name "safe_arch") (req "^0.7") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "safe_arch") (req "^0.7") (default-features #t) (kind 2)) (crate-dep (name "tempfile") (req "^3.3") (default-features #t) (kind 2)))) (hash "0ybqy5m1bd14q8k97rs0975kfsz2k29ckd57gsa4km06i2c2gprw") (features (quote (("std" "alloc") ("nightly") ("default_impl" "nightly") ("default" "std") ("alloc")))) (rust-version "1.63.0")))

