(define-module (crates-io pe nm) #:use-module (crates-io))

(define-public crate-penman-0.1 (crate (name "penman") (vers "0.1.0") (deps (list (crate-dep (name "api") (req "^0.1.0") (default-features #t) (kind 0) (package "ledgers-api")) (crate-dep (name "futures") (req "^0.3.5") (default-features #t) (kind 0)) (crate-dep (name "prost") (req "^0.6") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^0.2") (features (quote ("full"))) (default-features #t) (kind 0)) (crate-dep (name "tonic") (req "^0.2.1") (default-features #t) (kind 0)))) (hash "0fm2a775mmla5v8lp0kwzkm59qvcn02d0x0c42jaja5vyf7c7wky")))

(define-public crate-penman-0.1 (crate (name "penman") (vers "0.1.1") (deps (list (crate-dep (name "futures") (req "^0.3.5") (default-features #t) (kind 0)) (crate-dep (name "ledgers") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "prost") (req "^0.6") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^0.2") (features (quote ("full"))) (default-features #t) (kind 0)) (crate-dep (name "tonic") (req "^0.2.1") (default-features #t) (kind 0)))) (hash "1c9ciz367x9d8x3rs8j3nrpn8if6i8b2jawzck3jywcyfm69f9a2")))

