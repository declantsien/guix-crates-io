(define-module (crates-io pe ru) #:use-module (crates-io))

(define-public crate-peruse-0.2 (crate (name "peruse") (vers "0.2.0") (deps (list (crate-dep (name "regex") (req "^0.1.10") (default-features #t) (kind 0)))) (hash "1lv0892ii7v7vnlp53zlc049j300vbsif8p5ki8wh3qfy9sfap9x")))

(define-public crate-peruse-0.3 (crate (name "peruse") (vers "0.3.0") (deps (list (crate-dep (name "regex") (req "^0.1.10") (default-features #t) (kind 0)))) (hash "19prbq0ix24j02jfdp6w5rfvmmcm1g3q91ggw4375drnzs98nx83")))

