(define-module (crates-io pe lp) #:use-module (crates-io))

(define-public crate-pelp-0.1 (crate (name "pelp") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^4.5.4") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "080dchw85lyrh3rm390pvgvvcb36iq6n021yqx6cdqn84lskd3bl")))

