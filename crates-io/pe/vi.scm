(define-module (crates-io pe vi) #:use-module (crates-io))

(define-public crate-peview-0.1 (crate (name "peview") (vers "0.1.0") (hash "01b182kp7ppcb648sjwvv75ilb3pshhgx0lx9fwsrfd6q607gqya")))

(define-public crate-peview-0.1 (crate (name "peview") (vers "0.1.1") (hash "1kn56cpck7ivg7ca55qxwj7p0sl3z5pys2w6d39dkdf7vg5k1q6b")))

(define-public crate-peview-0.2 (crate (name "peview") (vers "0.2.0") (hash "0khm0cq23xk5fh9k45n07dhd8i5ama5ab1nqa8h1x21zrjh4bdr0")))

(define-public crate-peview-0.2 (crate (name "peview") (vers "0.2.1") (hash "1pzxdfy3jsidgsz4jwmf82f4csgp5my0h05gc4lnxsi110f6bkya")))

(define-public crate-peview-0.2 (crate (name "peview") (vers "0.2.3") (hash "0kbbxnpywqy05l90zchaf9vmza544vrl32ryrml9ca4m8wb2pj2f")))

