(define-module (crates-io pe x-) #:use-module (crates-io))

(define-public crate-pex-trie-0.0.0 (crate (name "pex-trie") (vers "0.0.0") (deps (list (crate-dep (name "ucd-trie") (req "^0.1.5") (default-features #t) (kind 0)) (crate-dep (name "unicode-script") (req "^0.5.5") (default-features #t) (kind 0)))) (hash "1p8hr0d8f76cg7kpkzjxzmlq0jbpjhix0hqqzlhy5j0jvvxvs13h") (features (quote (("default"))))))

(define-public crate-pex-trie-0.0.1 (crate (name "pex-trie") (vers "0.0.1") (deps (list (crate-dep (name "ucd-trie") (req "^0.1.5") (default-features #t) (kind 0)) (crate-dep (name "unicode-script") (req "^0.5.5") (default-features #t) (kind 0)))) (hash "1a92lrpph5yn5d1my152ykl07vh6ym67rsmj1kha0ffxp65pnj35") (features (quote (("default"))))))

(define-public crate-pex-trie-0.0.3 (crate (name "pex-trie") (vers "0.0.3") (deps (list (crate-dep (name "pex") (req "^0.1.0") (features (quote ("ucd-trie" "regex-automata"))) (default-features #t) (kind 2)) (crate-dep (name "regex-automata") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "ucd-trie") (req "^0.1.5") (default-features #t) (kind 0)) (crate-dep (name "unicode-script") (req "^0.5.5") (default-features #t) (kind 0)))) (hash "0smqa83v6v51svvgqnivp72zp387ai572i49b8q80zxznw1s9jmg") (features (quote (("default"))))))

