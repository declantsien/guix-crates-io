(define-module (crates-io pe nl) #:use-module (crates-io))

(define-public crate-penlib-0.0.1 (crate (name "penlib") (vers "0.0.1") (deps (list (crate-dep (name "palette") (req "^0.4") (default-features #t) (kind 0)))) (hash "1q4v9cpcn06s9v470m3dsx0xp3252dr89q8mvsfh4bhsbfqs2917")))

(define-public crate-penlib-0.0.2 (crate (name "penlib") (vers "0.0.2") (deps (list (crate-dep (name "palette") (req "^0.4") (default-features #t) (kind 0)))) (hash "09wbzzhqn1sp2v9n0bhc9iglfn2kf571kg1nb1xjmf8mb9j4brl7")))

(define-public crate-penlib-0.0.3 (crate (name "penlib") (vers "0.0.3") (deps (list (crate-dep (name "palette") (req "^0.4") (default-features #t) (kind 0)))) (hash "0fbgrrbl4z1fcn5kpagrvfwnkzyw5r7g1qhd2qyfwbf4hvjxylar")))

(define-public crate-penlib-0.0.4 (crate (name "penlib") (vers "0.0.4") (deps (list (crate-dep (name "palette") (req "^0.4") (default-features #t) (kind 0)))) (hash "1g47p3r7s7ckh9f56ka9simampc2pkaxmb15z3zb6k7qqb60sxl8")))

(define-public crate-penlist-0.0.2 (crate (name "penlist") (vers "0.0.2") (deps (list (crate-dep (name "colored") (req "^2.1.0") (default-features #t) (kind 0)))) (hash "17in8sh2p3wgfl91xv0al2kg6rc0zr3ph83cbd8szy5y0mbzm0x2")))

(define-public crate-penlist-0.0.3 (crate (name "penlist") (vers "0.0.3") (deps (list (crate-dep (name "colored") (req "^2.1.0") (default-features #t) (kind 0)))) (hash "02llfpnq3szjk6a0vkdzmp55wbjpz3z7nssmdrs0ricd5jncmlvc")))

(define-public crate-penlist-0.0.5 (crate (name "penlist") (vers "0.0.5") (deps (list (crate-dep (name "colored") (req "^2.1.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)))) (hash "1clmmsyrgxz3h7cnjkqj73kkfhh3vajv64s2i88nyqc96f8655yk")))

(define-public crate-penlist-0.0.6 (crate (name "penlist") (vers "0.0.6") (deps (list (crate-dep (name "colored") (req "^2.1.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)))) (hash "0vfflvs7wfw5padl0qasggdmc0ny9a20m5fdzw454631zw7yv752")))

(define-public crate-penlist-0.0.7 (crate (name "penlist") (vers "0.0.7") (deps (list (crate-dep (name "colored") (req "^2.1.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)))) (hash "1rcn26pxijl1rn8cy9rlhzjgfd3vx7kqk7i8q0iyv960bs09fp9g")))

(define-public crate-penlist-0.0.9 (crate (name "penlist") (vers "0.0.9") (deps (list (crate-dep (name "colored") (req "^2.1.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)))) (hash "11l7mk8vfri0jm9dcvy71b32z0z8phwv33hxgid3x15llgh2w7cb")))

(define-public crate-penlist-0.1 (crate (name "penlist") (vers "0.1.1") (deps (list (crate-dep (name "colored") (req "^2.1.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.10.4") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "term_size") (req "^0.3.2") (default-features #t) (kind 0)))) (hash "1xj14mddndsiimarqzkm8dy42sdrxa81g07sf09kdki1gb627iif")))

