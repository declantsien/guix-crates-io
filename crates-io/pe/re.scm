(define-module (crates-io pe re) #:use-module (crates-io))

(define-public crate-peregrine-0.1 (crate (name "peregrine") (vers "0.1.0") (hash "077jfjwakxvgmqdi5cpd9zcsm5p11qv20gsqidm2k2lm4bxw0crh") (yanked #t)))

(define-public crate-peregrine_db-0.1 (crate (name "peregrine_db") (vers "0.1.0") (deps (list (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 2)) (crate-dep (name "snafu") (req "^0.6.8") (default-features #t) (kind 0)) (crate-dep (name "tokio-test") (req "^0.4.2") (default-features #t) (kind 2)))) (hash "11nkryzy25v3y3mng10z0jwr4x43xa7hk8zk7h8c1vipiqkkjlli")))

(define-public crate-peresil-0.1 (crate (name "peresil") (vers "0.1.0") (hash "05qjbqfqmz0466594k17xhxhbfsfvhyf5zikvdlypf2v8lzsv2kv")))

(define-public crate-peresil-0.2 (crate (name "peresil") (vers "0.2.0") (hash "0qwm6z23paffxkihlnqy16zjcwdr9dqmh6lhp9ql3fzsafy8bg37")))

(define-public crate-peresil-0.2 (crate (name "peresil") (vers "0.2.1") (hash "1gzcydcwhhxnxjkrkbs14ald3z12bc2q7fzlqj4alv8fbjcyza1d")))

(define-public crate-peresil-0.3 (crate (name "peresil") (vers "0.3.0") (hash "0mwyw03yqp0yqdjf4a89vn86szxaksmxvgzv1j2nw69fsmp8hn7n")))

(define-public crate-peresil-0.4 (crate (name "peresil") (vers "0.4.0-alpha.0") (hash "19gscdxf1alxp2nr9wk81q1haf7ww54vkfjv3i8z3ds7n7vbnvsr") (features (quote (("default" "combinators") ("combinators"))))))

(define-public crate-perestroika-0.1 (crate (name "perestroika") (vers "0.1.0") (deps (list (crate-dep (name "bevy") (req "^0.8.1") (default-features #t) (kind 0)))) (hash "1drw04nj8gb1jlyhfq56xh3dzw7a4fs166ssifgzv6fwfr0dbwrl")))

(define-public crate-perestroika-0.1 (crate (name "perestroika") (vers "0.1.1") (deps (list (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)) (crate-dep (name "rand_chacha") (req "^0.3.1") (default-features #t) (kind 0)))) (hash "0h0avj3ykbal05ym8aylxvawxgb4b3yr759mz1xsmrf1zz64md9j")))

(define-public crate-perestroika-0.1 (crate (name "perestroika") (vers "0.1.2") (deps (list (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)) (crate-dep (name "rand_chacha") (req "^0.3.1") (default-features #t) (kind 0)))) (hash "0viljgcarm3d3d0awplqs551nab79wjfbh6gkb92y739y2n9d8rp")))

(define-public crate-perestroika-0.1 (crate (name "perestroika") (vers "0.1.3") (deps (list (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)) (crate-dep (name "rand_chacha") (req "^0.3.1") (default-features #t) (kind 0)))) (hash "0a53bcglibxb8dlnp5r4m556d1gr2bkljw85ns2i9flr2i69y6rp")))

(define-public crate-perestroika-0.1 (crate (name "perestroika") (vers "0.1.4") (deps (list (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)) (crate-dep (name "rand_chacha") (req "^0.3.1") (default-features #t) (kind 0)) (crate-dep (name "rand_core") (req "^0.6") (default-features #t) (kind 0)))) (hash "1i46s7rfp770n4aqckj70xk8jpw1zvbp766ccpg1g5fhgljd69c7")))

(define-public crate-perestroika-0.1 (crate (name "perestroika") (vers "0.1.5") (deps (list (crate-dep (name "criterion") (req "^0.5.1") (features (quote ("html_reports"))) (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)) (crate-dep (name "rand_chacha") (req "^0.3.1") (default-features #t) (kind 0)) (crate-dep (name "rand_core") (req "^0.6") (default-features #t) (kind 0)))) (hash "1q2r30zmwg8aaw6k8lwbf81h278rc21kfl4n82dl1yy3da5gzdl9")))

