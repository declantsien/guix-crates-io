(define-module (crates-io pe ak) #:use-module (crates-io))

(define-public crate-peak-0.1 (crate (name "peak") (vers "0.1.0") (hash "1gm8317kdcp168ll8nbkggvajmcimw1vcrbqjjs44dzz3s89zbgk")))

(define-public crate-peak-result-0.0.1 (crate (name "peak-result") (vers "0.0.1") (hash "1rbpwfi6yrcq3r26hwpf498wi9vmz1m0bzgmka4y5mnqajixh9gy")))

(define-public crate-peak-result-1 (crate (name "peak-result") (vers "1.0.0") (hash "0hj8a5hgrmnhrn3msq78qdwaglarci8znhi76w69drav5ds7d9q8")))

(define-public crate-peak-result-1 (crate (name "peak-result") (vers "1.0.1") (deps (list (crate-dep (name "criterion") (req "^0.5") (features (quote ("html_reports"))) (default-features #t) (kind 2)))) (hash "0pxrw913608lzgl63g8naiw6fr2av77w2wpsxkiw71q8ygkw69li")))

(define-public crate-peak_alloc-0.1 (crate (name "peak_alloc") (vers "0.1.0") (hash "164xdrmzwxvqrby60dnamid1zlg5vm816wg6jfivps9n9wmcmr8a")))

(define-public crate-peak_alloc-0.2 (crate (name "peak_alloc") (vers "0.2.0") (hash "1rcdc2vf366mrxm5yqv75djrr1dccj1c51138ls6rm571a5rglw4")))

(define-public crate-peak_alloc-0.2 (crate (name "peak_alloc") (vers "0.2.1") (hash "0gpawzx2rwdb52hlb9as9sx303b09qp853s68rixfbw3vpifii19")))

(define-public crate-peak_finder-1 (crate (name "peak_finder") (vers "1.0.0") (deps (list (crate-dep (name "csv") (req "^1") (default-features #t) (kind 0)))) (hash "1siqr92ygamvijc9gy000vk847g4nf5kp5di4s9raxvyrqjq5gdx")))

(define-public crate-peak_finder-1 (crate (name "peak_finder") (vers "1.0.1") (deps (list (crate-dep (name "csv") (req "^1") (default-features #t) (kind 0)))) (hash "066dp6kc59ywncnsx112xjp24jqrgc2zh61lr12x6wwwy3lfzzfi")))

(define-public crate-peakbag-0.1 (crate (name "peakbag") (vers "0.1.0") (deps (list (crate-dep (name "log") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "num") (req "^0.1") (default-features #t) (kind 0)))) (hash "09yrv9i5rqywgs5rhqhwbqppr6h2gf7zw5m37nblishhg4ia9r4v")))

(define-public crate-peakmem-alloc-0.1 (crate (name "peakmem-alloc") (vers "0.1.0") (hash "15zy4kwwwdq6sq7n4yggfq8m1pcmgylsjp0kykn0s0fl2xpr9x9v") (features (quote (("nightly") ("default"))))))

(define-public crate-peakmem-alloc-0.2 (crate (name "peakmem-alloc") (vers "0.2.0") (deps (list (crate-dep (name "jemallocator") (req "^0.5.4") (default-features #t) (kind 2)))) (hash "0z73p03f443bm8ig0yaadc52jn07h1bf770lf8vsls6c2n92k39n") (features (quote (("nightly") ("default"))))))

(define-public crate-peakmem-alloc-0.2 (crate (name "peakmem-alloc") (vers "0.2.1") (deps (list (crate-dep (name "jemallocator") (req "^0.5.4") (default-features #t) (kind 2)))) (hash "0vbm0075njbppqyzwpw886xpmir6x3vry4prdp32a9iykq6qqwx0") (features (quote (("nightly") ("default"))))))

(define-public crate-peakmem-alloc-0.2 (crate (name "peakmem-alloc") (vers "0.2.2") (deps (list (crate-dep (name "jemallocator") (req "^0.5.4") (default-features #t) (kind 2)))) (hash "1m3kqd0gdyksh9fxgqp8r9l4g01wj5d7cii0p3bag7nwiplzxdan") (features (quote (("nightly") ("default"))))))

(define-public crate-peakmem-alloc-0.3 (crate (name "peakmem-alloc") (vers "0.3.0") (deps (list (crate-dep (name "jemallocator") (req "^0.5.4") (default-features #t) (kind 2)))) (hash "1xj8v7cd7bkgzn1w41cwrv06fqax6g9dhmzdmijj8ivsjy545dsy") (features (quote (("nightly") ("default"))))))

