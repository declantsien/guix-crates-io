(define-module (crates-io pe p-) #:use-module (crates-io))

(define-public crate-pep-508-0.1 (crate (name "pep-508") (vers "0.1.0") (deps (list (crate-dep (name "chumsky") (req "^0.9.0") (default-features #t) (kind 0)) (crate-dep (name "insta") (req "^1.28.0") (default-features #t) (kind 2)))) (hash "17z3rrdamhg75h6lyczj3zm8xmi7lrh70xwx1mz3zfwy223v6dq3")))

(define-public crate-pep-508-0.2 (crate (name "pep-508") (vers "0.2.0") (deps (list (crate-dep (name "chumsky") (req "^1.0.0-alpha.0") (default-features #t) (kind 0)) (crate-dep (name "insta") (req "^1.28.0") (default-features #t) (kind 2)))) (hash "1j1q250ncf6z3xwyxyqi70i4v3m59izf7s0kpmadd8xpicyhxls1")))

(define-public crate-pep-508-0.3 (crate (name "pep-508") (vers "0.3.0") (deps (list (crate-dep (name "chumsky") (req ">=1.0.0-alpha.3") (default-features #t) (kind 0)) (crate-dep (name "insta") (req "^1.28.0") (default-features #t) (kind 2)))) (hash "18qd06pm4fgkc10970fad5rcfjgardg704cszrjzvb9b0d07kv6s")))

(define-public crate-pep-508-0.4 (crate (name "pep-508") (vers "0.4.0") (deps (list (crate-dep (name "chumsky") (req "=1.0.0-alpha.6") (default-features #t) (kind 0)) (crate-dep (name "insta") (req "^1.34.0") (default-features #t) (kind 2)))) (hash "0kqybppyq5i9yxjvwdq8ndgjjp8x719jpbi73a0cap7pbnwkcrfm")))

