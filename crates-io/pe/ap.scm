(define-module (crates-io pe ap) #:use-module (crates-io))

(define-public crate-peapod-0.1 (crate (name "peapod") (vers "0.1.0") (deps (list (crate-dep (name "bitvec") (req "^1") (features (quote ("alloc"))) (kind 0)) (crate-dep (name "phenotype-internal") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "phenotype-macro") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "1043k8x8i7i3pbhvn6pia7p5pyp9xxdxxbpdxa4app8qm204cdal") (yanked #t)))

(define-public crate-peapod-0.1 (crate (name "peapod") (vers "0.1.1") (deps (list (crate-dep (name "bitvec") (req "^1") (features (quote ("alloc"))) (kind 0)) (crate-dep (name "phenotype-internal") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "phenotype-macro") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "0q4v2v06jzd6nwq8jaz592ysjm417i9m5mxljlqlr8qa2z55wiyv") (yanked #t)))

(define-public crate-peapod-0.1 (crate (name "peapod") (vers "0.1.2") (deps (list (crate-dep (name "bitvec") (req "^1") (features (quote ("alloc"))) (kind 0)) (crate-dep (name "phenotype-internal") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "phenotype-macro") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "1pb2cjw0xz6lpi6fzssnmwvqkl2za8xba9qc7nm9pkid4z8ayjzc") (yanked #t)))

(define-public crate-peapod-0.1 (crate (name "peapod") (vers "0.1.3") (deps (list (crate-dep (name "bitvec") (req "^1") (features (quote ("alloc"))) (kind 0)) (crate-dep (name "phenotype-internal") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "phenotype-macro") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "11va1jyiiwyaj1rki3xccfz7b42zmngqdhb12bkss02562qs8ppy") (yanked #t)))

(define-public crate-peapod-0.1 (crate (name "peapod") (vers "0.1.4") (deps (list (crate-dep (name "bitvec") (req "^1") (features (quote ("alloc"))) (kind 0)) (crate-dep (name "phenotype-internal") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "phenotype-macro") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "035yg92ncwx785gpyrw9svj6jz5rwbqgvqmlf5l9jd7ik8cjnlf2") (yanked #t)))

(define-public crate-peapod-0.1 (crate (name "peapod") (vers "0.1.5") (deps (list (crate-dep (name "bitvec") (req "^1") (features (quote ("alloc"))) (kind 0)) (crate-dep (name "phenotype-internal") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "phenotype-macro") (req "^0.1.1") (default-features #t) (kind 0)))) (hash "11bwpyf9v0bi71ivjcg974z9cqma6icxb3r5089vqkcaq5khfhca") (yanked #t)))

(define-public crate-peapod-0.1 (crate (name "peapod") (vers "0.1.6") (deps (list (crate-dep (name "bitvec") (req "^1") (features (quote ("alloc"))) (kind 0)) (crate-dep (name "phenotype-internal") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "phenotype-macro") (req "^0.1.2") (default-features #t) (kind 0)))) (hash "03gi7lji5m5h2k6rb9g9pf5831rgw1dn1yvyrf5dcz6lqkc5f1jf") (yanked #t)))

(define-public crate-peapod-0.1 (crate (name "peapod") (vers "0.1.7") (deps (list (crate-dep (name "bitvec") (req "^1") (features (quote ("alloc"))) (kind 0)) (crate-dep (name "phenotype-internal") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "phenotype-macro") (req "^0.1.3") (default-features #t) (kind 0)))) (hash "1l4jbbabg81dm5v370qpfnyniivc1mrgk4ixgk32hfnhl59fs9md") (yanked #t)))

(define-public crate-peapod-0.1 (crate (name "peapod") (vers "0.1.8") (deps (list (crate-dep (name "bitvec") (req "^1") (features (quote ("alloc"))) (kind 0)) (crate-dep (name "phenotype-internal") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "phenotype-macro") (req "^0.1.4") (default-features #t) (kind 0)))) (hash "1vg3smmbksw3dv6gzhg21br9vzayzj01hinkcaz2i14fkzkj9vlr")))

(define-public crate-peapod-0.1 (crate (name "peapod") (vers "0.1.9") (deps (list (crate-dep (name "bitvec") (req "^1") (features (quote ("alloc"))) (kind 0)) (crate-dep (name "phenotype-internal") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "phenotype-macro") (req "^0.1.4") (default-features #t) (kind 0)))) (hash "00h77g1m4njfarbd3j84r0a2nfri12pwsj4a1ykv9ncbbd1ii3v5")))

