(define-module (crates-io pe bb) #:use-module (crates-io))

(define-public crate-pebble-0.8 (crate (name "pebble") (vers "0.8.4") (deps (list (crate-dep (name "ansi_term") (req "^0.9.0") (default-features #t) (kind 0)) (crate-dep (name "recipe-reader") (req "^0.9.9") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.4") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0.4") (default-features #t) (kind 0)) (crate-dep (name "toml") (req "^0.4.1") (default-features #t) (kind 0)))) (hash "0zwr68vvi93ncvnqi4491zmgwlf66g7km6czfri51pzhm00g0pga")))

(define-public crate-pebble-0.8 (crate (name "pebble") (vers "0.8.5") (deps (list (crate-dep (name "ansi_term") (req "^0.9.0") (default-features #t) (kind 0)) (crate-dep (name "lzf") (req "^0.3.1") (default-features #t) (kind 0)) (crate-dep (name "recipe-reader") (req "^0.9.9") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.4") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0.4") (default-features #t) (kind 0)) (crate-dep (name "toml") (req "^0.4.1") (default-features #t) (kind 0)))) (hash "0wkn28kvxq89z4c1v0zyx0zdj86gj3ss2i9xb4c7cc7vbffziqrh")))

(define-public crate-pebble-rust-0.1 (crate (name "pebble-rust") (vers "0.1.0") (hash "1hzkddli9cf6b5v35mkq24i149wdzqlhy0g8kxnk3s4lsxz7v2gc")))

(define-public crate-pebble-skip-0.0.1 (crate (name "pebble-skip") (vers "0.0.1") (deps (list (crate-dep (name "debugless-unwrap") (req "^0.0.4") (default-features #t) (kind 0)) (crate-dep (name "pebble-sys") (req "^0.0.1") (default-features #t) (kind 0)) (crate-dep (name "unsafe_unwrap") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "0z4svbcdgqgb5zi3y4qqmahhy297pqcdldw728928rnp2cka3i83")))

(define-public crate-pebble-sys-0.0.1 (crate (name "pebble-sys") (vers "0.0.1") (hash "1j96whbgv87yy3jhb27mxs2fy9yvq4b5m0vjf7339r8lwy9jg1qh")))

(define-public crate-pebble_query-0.1 (crate (name "pebble_query") (vers "0.1.0") (deps (list (crate-dep (name "convert_case") (req "^0.6.0") (default-features #t) (kind 0)) (crate-dep (name "prost") (req "^0.12.1") (default-features #t) (kind 0)) (crate-dep (name "sea-orm") (req "^0.12.3") (features (quote ("sqlx-sqlite" "runtime-tokio-rustls" "mock" "macros" "debug-print" "uuid"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.188") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.37") (default-features #t) (kind 0)))) (hash "09lgfdca3wbfz027iwrk9i2gih0bw4viwqcfh1yqzznbjbkrbayl")))

(define-public crate-pebble_syscall_common-0.1 (crate (name "pebble_syscall_common") (vers "0.1.0") (hash "0q1l12hrl8vk8idxcay62nayvb7279p2ii0agcc6k38n7wcsi81q")))

(define-public crate-pebble_syscall_common-0.1 (crate (name "pebble_syscall_common") (vers "0.1.1") (hash "0sy9f4gn5shh75acavs79s3kl0mw4v2aqf5zfysl9ar2y9c95nng")))

(define-public crate-pebble_syscall_common-0.1 (crate (name "pebble_syscall_common") (vers "0.1.2") (hash "0118aw7nhf7n0a3i26mjh55mmprcwnjjyd1rn5v0fqczviralwr4")))

(define-public crate-pebble_syscall_common-0.1 (crate (name "pebble_syscall_common") (vers "0.1.3") (hash "1cks8dxxjjcqr99pj8rwqyayfd623ckvym00d37lkkx68h99pr59")))

(define-public crate-pebble_syscall_common-0.1 (crate (name "pebble_syscall_common") (vers "0.1.4") (hash "0ckczrspan47rvngm3wvwgcbp0c7vynnh46rf98hg8nrhwdg1q3w")))

(define-public crate-pebbles-0.0.1 (crate (name "pebbles") (vers "0.0.1") (deps (list (crate-dep (name "quickjs-rs") (req "^0.5.2") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)) (crate-dep (name "rand_distr") (req "^0.4.3") (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.11") (features (quote ("json" "blocking"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)))) (hash "17jpps5x4kx8hyw64241ng5x2p0nfrin4706j1lznsf10hrxvjlj")))

(define-public crate-pebbles-0.0.101 (crate (name "pebbles") (vers "0.0.101") (deps (list (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)) (crate-dep (name "rand_distr") (req "^0.4.3") (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.11") (features (quote ("json" "blocking"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.107") (default-features #t) (kind 0)))) (hash "1b3bm19qbsq10rv3bgjz4jnir8nx17p8q7ym0qbb32bc4hn6ca3l")))

(define-public crate-pebbles-0.0.102 (crate (name "pebbles") (vers "0.0.102") (deps (list (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)) (crate-dep (name "rand_distr") (req "^0.4.3") (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.11") (features (quote ("json" "blocking"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.107") (default-features #t) (kind 0)))) (hash "11d65yfdqz677f29ni87z1iy5dkiavp6caic60pbqijfk2ihf5lx")))

(define-public crate-pebbles-0.0.103 (crate (name "pebbles") (vers "0.0.103") (deps (list (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)) (crate-dep (name "rand_distr") (req "^0.4.3") (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.11") (features (quote ("json" "blocking"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.107") (default-features #t) (kind 0)))) (hash "1brmglmr3ngzkgqiiknpd8k7l25zpc64275msbg2acv0dabgp8m4")))

