(define-module (crates-io pe tp) #:use-module (crates-io))

(define-public crate-petpet-0.1 (crate (name "petpet") (vers "0.1.0") (deps (list (crate-dep (name "image") (req "^0.23.14") (default-features #t) (kind 0)) (crate-dep (name "imageproc") (req "^0.22.0") (default-features #t) (kind 0)))) (hash "10hr9f3aypf8cbv1wlagr2phvs0cy1yqpjh5khk620xn99zkdk55") (yanked #t)))

(define-public crate-petpet-0.2 (crate (name "petpet") (vers "0.2.0") (deps (list (crate-dep (name "image") (req "^0.23.14") (default-features #t) (kind 0)) (crate-dep (name "imageproc") (req "^0.22.0") (default-features #t) (kind 0)))) (hash "1dvzmm2klq29p7l7xrhksqxnszp7wfi7zr0il698glcgaldlhb7s") (yanked #t)))

(define-public crate-petpet-0.2 (crate (name "petpet") (vers "0.2.1") (deps (list (crate-dep (name "image") (req "^0.23.14") (default-features #t) (kind 0)) (crate-dep (name "imageproc") (req "^0.22.0") (default-features #t) (kind 0)))) (hash "0g3x4ylcr4ih1mnib39cxj7s0wy94a2ld6frqkwqvh4hx9ck35xf") (yanked #t)))

(define-public crate-petpet-0.2 (crate (name "petpet") (vers "0.2.2") (deps (list (crate-dep (name "image") (req "^0.23.14") (default-features #t) (kind 0)) (crate-dep (name "imageproc") (req "^0.22.0") (default-features #t) (kind 0)))) (hash "1rahbm6qpr44x00yijh049jv0546fvymdrhq6x3kwl1rc2sqmr53") (yanked #t)))

(define-public crate-petpet-0.2 (crate (name "petpet") (vers "0.2.3") (deps (list (crate-dep (name "image") (req "^0.23.14") (default-features #t) (kind 0)) (crate-dep (name "imageproc") (req "^0.22.0") (default-features #t) (kind 0)))) (hash "1arw3dvsc43ksk1m2zpjyk3615d9fc0n3m60p2c7ydfrwdz2l05k") (yanked #t)))

(define-public crate-petpet-0.3 (crate (name "petpet") (vers "0.3.0") (deps (list (crate-dep (name "image") (req "^0.23.14") (default-features #t) (kind 0)))) (hash "1q3gz93vli12avhsfi7ivximzakhq3i0c5qvgjmip0njj0xgk25c")))

(define-public crate-petpet-0.5 (crate (name "petpet") (vers "0.5.0") (deps (list (crate-dep (name "image") (req "^0.23.14") (default-features #t) (kind 0)))) (hash "14c18fkss08qcy0r54xzbhkvn6q9v3jmfr3xqkq31afp5i7xwssf")))

(define-public crate-petpet-0.6 (crate (name "petpet") (vers "0.6.0") (deps (list (crate-dep (name "image") (req "^0.23.14") (default-features #t) (kind 0)))) (hash "1b0yqhdyzm9na5r8zpjr5ri9wz5jxvcjrbpa9c5bm2vnk2nabjsr")))

(define-public crate-petpet-0.7 (crate (name "petpet") (vers "0.7.1") (deps (list (crate-dep (name "image") (req "^0.23.14") (default-features #t) (kind 0)))) (hash "14688wh7szhky3l4kv03bdvjzra71sk22nxmwplpif15z7y5h8nw")))

(define-public crate-petpet-2 (crate (name "petpet") (vers "2.1.1") (deps (list (crate-dep (name "image") (req "^0.24.0") (default-features #t) (kind 0)))) (hash "0imhzh8pzy2dgg7l7kg5g5z88950krd58ahgy2f2n3ncwd7h1am8")))

(define-public crate-petpet-2 (crate (name "petpet") (vers "2.2.0") (deps (list (crate-dep (name "image") (req "^0.24.0") (default-features #t) (kind 0)))) (hash "1ng0wh14855jqxbm6rqlr8rwwqam8y2pwp7zqha9brnyf3h5dgvg")))

(define-public crate-petpet-2 (crate (name "petpet") (vers "2.2.1") (deps (list (crate-dep (name "image") (req "^0.24.0") (default-features #t) (kind 0)))) (hash "004iv2s59bkwivfalwk9nbf1jszngnhq0r35p9yrkp0fyxqm677k")))

(define-public crate-petpet-2 (crate (name "petpet") (vers "2.3.1") (deps (list (crate-dep (name "image") (req "^0.24.0") (kind 0)) (crate-dep (name "image") (req "^0.24.0") (optional #t) (kind 1)))) (hash "0i6f251gfkw00lbxpx8k9ajjqi4dl2pzhlxjxbyidp826pjkbv2m") (features (quote (("encode_to_gif" "image/gif") ("default" "encode_to_gif" "bundle_raw_hands")))) (v 2) (features2 (quote (("bundle_raw_hands" "dep:image" "image/webp")))) (rust-version "1.70")))

(define-public crate-petpet-2 (crate (name "petpet") (vers "2.3.2") (deps (list (crate-dep (name "image") (req "^0.24.0") (kind 0)) (crate-dep (name "image") (req "^0.24.0") (optional #t) (kind 1)))) (hash "09p610pr28knsz36rv0h4q4623prl7k4jdl7aasycbsg4j3d9s6g") (features (quote (("more_format" "image/png" "image/jpeg" "image/webp") ("image") ("encode_to_gif" "image/gif") ("default" "encode_to_gif" "bundle_raw_hands")))) (v 2) (features2 (quote (("bundle_raw_hands" "dep:image" "image/webp")))) (rust-version "1.70")))

(define-public crate-petpet-2 (crate (name "petpet") (vers "2.4.0") (deps (list (crate-dep (name "apng") (req "^0.3.1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "image") (req "^0.24.0") (kind 0)) (crate-dep (name "image") (req "^0.24.0") (optional #t) (kind 1)) (crate-dep (name "png") (req "^0.17.7") (optional #t) (default-features #t) (kind 0)))) (hash "1p944ndmnlx5ln6kr3rq649b1n04ski5zxaflq6k7d1phxawkjsa") (features (quote (("more_format" "image/png" "image/jpeg" "image/webp") ("image") ("encode_to_gif" "image/gif") ("default" "encode_to_gif" "bundle_raw_hands" "encode_to_apng")))) (v 2) (features2 (quote (("encode_to_apng" "dep:apng" "dep:png") ("bundle_raw_hands" "dep:image" "image/webp")))) (rust-version "1.70")))

(define-public crate-petpet-2 (crate (name "petpet") (vers "2.4.1") (deps (list (crate-dep (name "apng") (req "^0.3.1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "image") (req "^0.24.0") (kind 0)) (crate-dep (name "image") (req "^0.24.0") (optional #t) (kind 1)) (crate-dep (name "png") (req "^0.17.7") (optional #t) (default-features #t) (kind 0)))) (hash "10smhscvka57jp2wrln8bcshjp9n3h315bkmldy6qp9rw918nsbw") (features (quote (("more_format" "image/png" "image/jpeg" "image/webp") ("image") ("encode_to_gif" "image/gif") ("default" "encode_to_gif" "bundle_raw_hands" "encode_to_apng")))) (v 2) (features2 (quote (("encode_to_apng" "dep:apng" "dep:png") ("bundle_raw_hands" "dep:image" "image/webp")))) (rust-version "1.70")))

(define-public crate-petpet-2 (crate (name "petpet") (vers "2.4.2") (deps (list (crate-dep (name "apng") (req "^0.3.1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "image") (req "^0.24.0") (kind 0)) (crate-dep (name "image") (req "^0.24.0") (optional #t) (kind 1)) (crate-dep (name "png") (req "^0.17.7") (optional #t) (default-features #t) (kind 0)))) (hash "01gsf7m5anpkmy6qcaq7fvw46aj1zs489v0mmr9a12jh7ws0dmbc") (features (quote (("more_format" "image/png" "image/jpeg" "image/webp") ("image") ("encode_to_gif" "image/gif") ("default" "encode_to_gif" "bundle_raw_hands" "encode_to_apng")))) (v 2) (features2 (quote (("encode_to_apng" "dep:apng" "dep:png") ("bundle_raw_hands" "dep:image" "image/webp")))) (rust-version "1.70")))

(define-public crate-petpet-2 (crate (name "petpet") (vers "2.4.3") (deps (list (crate-dep (name "apng") (req "^0.3.4") (features (quote ("png"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "image") (req "^0.24.0") (kind 0)) (crate-dep (name "image") (req "^0.24.0") (optional #t) (kind 1)))) (hash "097r8j4ivdaa9g7hl746331k0avn4dpa75hfg4m8m02h3871f8wp") (features (quote (("more_format" "image/png" "image/jpeg" "image/webp") ("image") ("encode_to_gif" "image/gif") ("default" "encode_to_gif" "bundle_raw_hands" "encode_to_apng")))) (v 2) (features2 (quote (("encode_to_apng" "dep:apng") ("bundle_raw_hands" "dep:image" "image/webp")))) (rust-version "1.70")))

(define-public crate-petpet-gui-1 (crate (name "petpet-gui") (vers "1.0.0") (deps (list (crate-dep (name "nfd2") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "petpet") (req "^2.1.1") (default-features #t) (kind 0)))) (hash "1rfwx9g2ryfxp5zg6bwp0v7fcr806pg7vbjcgwr8049sh6zppz7m")))

(define-public crate-petpet-gui-1 (crate (name "petpet-gui") (vers "1.0.1") (deps (list (crate-dep (name "petpet") (req "^2.3.2") (features (quote ("more_format" "image"))) (default-features #t) (kind 0)) (crate-dep (name "rfd") (req "^0.13") (default-features #t) (kind 0)))) (hash "1jd87bw5chv6pk0bn0q7q2cl228rvg4f9zyy39q4bzpp309pjspf")))

(define-public crate-petpet-gui-1 (crate (name "petpet-gui") (vers "1.1.0") (deps (list (crate-dep (name "petpet") (req "^2.4.1") (features (quote ("more_format" "image"))) (default-features #t) (kind 0)) (crate-dep (name "rfd") (req "^0.13") (default-features #t) (kind 0)))) (hash "0935yz3pc1wbw85v4gsglavsapk4ycy6alvbymh1gv7zpimhmlzm")))

(define-public crate-petpet-gui-1 (crate (name "petpet-gui") (vers "1.1.1") (deps (list (crate-dep (name "petpet") (req "^2.4.1") (features (quote ("more_format" "image"))) (default-features #t) (kind 0)) (crate-dep (name "rfd") (req "^0.13") (default-features #t) (kind 0)))) (hash "1ikdf8bxa10ip2kqppilac7zc7zi8diqw4j58qavbr434gv48c5c")))

