(define-module (crates-io pe g-) #:use-module (crates-io))

(define-public crate-peg-macros-0.6 (crate (name "peg-macros") (vers "0.6.0") (deps (list (crate-dep (name "peg-runtime") (req "= 0.6.0") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)))) (hash "0vp7w8r9p9cxxa0vcwikmwcb3sbz2zr6rlv97xml1mh7nm9li07j") (features (quote (("trace"))))))

(define-public crate-peg-macros-0.6 (crate (name "peg-macros") (vers "0.6.1") (deps (list (crate-dep (name "peg-runtime") (req "= 0.6.1") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)))) (hash "176d203k8hkzvi0nz6dpk46agn15008a5gd1pcd7kcb2cd7ss21x") (features (quote (("trace"))))))

(define-public crate-peg-macros-0.6 (crate (name "peg-macros") (vers "0.6.2") (deps (list (crate-dep (name "peg-runtime") (req "= 0.6.2") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)))) (hash "0li8qrb8hyqr7v5mhrkym0xp7ijnbksqviqc2i3556cysdgick62") (features (quote (("trace"))))))

(define-public crate-peg-macros-0.6 (crate (name "peg-macros") (vers "0.6.3") (deps (list (crate-dep (name "peg-runtime") (req "=0.6.3") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)))) (hash "0kdisa6di5gkgpw97897lg78jhsx6nliax3d4s6y8cvnz6n60vb3") (features (quote (("trace"))))))

(define-public crate-peg-macros-0.7 (crate (name "peg-macros") (vers "0.7.0") (deps (list (crate-dep (name "peg-runtime") (req "=0.7.0") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1.0.24") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)))) (hash "0k44f3xwhkpbk0mbl0v88l0v12il24kj145gjgvbxv4dkf155amm") (features (quote (("trace"))))))

(define-public crate-peg-macros-0.8 (crate (name "peg-macros") (vers "0.8.0") (deps (list (crate-dep (name "peg-runtime") (req "=0.8.0") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1.0.24") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)))) (hash "05nwzh94lvkalkm4cjac9gqlk3nfri7r7bfbjm4842vpgcabwdj5") (features (quote (("trace"))))))

(define-public crate-peg-macros-0.8 (crate (name "peg-macros") (vers "0.8.1") (deps (list (crate-dep (name "peg-runtime") (req "=0.8.1") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1.0.24") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)))) (hash "16ylkinxv7qc2ywlzxh916dx3alzyfcj7lg352245w2wq16hi42a") (features (quote (("trace"))))))

(define-public crate-peg-macros-0.8 (crate (name "peg-macros") (vers "0.8.2") (deps (list (crate-dep (name "peg-runtime") (req "=0.8.2") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1.0.24") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)))) (hash "141c76na4n9mfs1y22az59yanaz9kw5aabgnj28d2xlvhp71rrj6") (features (quote (("trace"))))))

(define-public crate-peg-macros-0.8 (crate (name "peg-macros") (vers "0.8.3") (deps (list (crate-dep (name "peg-runtime") (req "=0.8.3") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1.0.24") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)))) (hash "10imq1mmvy9ig6i8b1c6dprih54fhchbf7ffzsjbcfpdcwhd8hgj") (features (quote (("trace"))))))

(define-public crate-peg-pack-0.1 (crate (name "peg-pack") (vers "0.1.0") (deps (list (crate-dep (name "atty") (req "^0.2.14") (default-features #t) (kind 0)) (crate-dep (name "bimap") (req "^0.6.2") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^4.0") (features (quote ("cargo" "derive"))) (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.5") (default-features #t) (kind 0)) (crate-dep (name "seahash") (req "^4.1") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "termcolor") (req "^1.1") (default-features #t) (kind 0)))) (hash "0hd7k9a9ns3kb407r27kykcn3jirnjzsmwh27km32v2xvvgx29hl")))

(define-public crate-peg-runtime-0.6 (crate (name "peg-runtime") (vers "0.6.0") (hash "1isjp09n5kzpxqy9vspanxp6pklfpc5fv24b306hn8p1wmi6fh8k")))

(define-public crate-peg-runtime-0.6 (crate (name "peg-runtime") (vers "0.6.1") (hash "05841rqpv462r9n0apn8z7mb8hw2pp3yaqvii529qv3qllyp4zi0")))

(define-public crate-peg-runtime-0.6 (crate (name "peg-runtime") (vers "0.6.2") (hash "0r583cq923v0narrpq73qmp780yg4pablzklhrwnr64xwsbjh6hc")))

(define-public crate-peg-runtime-0.6 (crate (name "peg-runtime") (vers "0.6.3") (hash "1i99fq2xj1isx44d2b06m31f58spqga9kiyka20xg69d9m8v2mcm")))

(define-public crate-peg-runtime-0.7 (crate (name "peg-runtime") (vers "0.7.0") (hash "12201by4nr6z7jc6gi4r6mz1g2n1jjsrlr66ckksg8q9bzsxq6f7")))

(define-public crate-peg-runtime-0.8 (crate (name "peg-runtime") (vers "0.8.0") (hash "17znn3433nhhwks3zrmry2y45i4y4xgl4q2dsh4s9hq3pb9yzc7r")))

(define-public crate-peg-runtime-0.8 (crate (name "peg-runtime") (vers "0.8.1") (hash "0fcpjs0ay4y0cqcjvh3x8zh7hbb84s5md79cm08nvbbyndi0984z")))

(define-public crate-peg-runtime-0.8 (crate (name "peg-runtime") (vers "0.8.2") (hash "08l9sad4mh5f0niizjk1k44n2z2s9cn8pfbq8v79h8zsc0nfkfin") (features (quote (("unstable") ("std"))))))

(define-public crate-peg-runtime-0.8 (crate (name "peg-runtime") (vers "0.8.3") (hash "0fmasxbncm503dcakq8qwkcbjihz4jklkrjy0v1190q79ksvibp3") (features (quote (("unstable") ("std"))))))

(define-public crate-peg-syntax-ext-0.4 (crate (name "peg-syntax-ext") (vers "0.4.0") (deps (list (crate-dep (name "peg") (req "^0.4") (default-features #t) (kind 0)))) (hash "0gpc8fymw0vbwdawrm6flqjd61w57qr1s79im0yzc3ha1xv570pz")))

(define-public crate-peg-syntax-ext-0.4 (crate (name "peg-syntax-ext") (vers "0.4.1") (deps (list (crate-dep (name "peg") (req "^0.4") (default-features #t) (kind 0)))) (hash "03kahq74h963qxgv1sg43yx0giw33y80p7x3b0gfjx3bkkhfs0jd")))

(define-public crate-peg-syntax-ext-0.4 (crate (name "peg-syntax-ext") (vers "0.4.2") (deps (list (crate-dep (name "peg") (req "^0.4") (default-features #t) (kind 0)))) (hash "1p3bvpxr84b8vfzmdasj1bs256cy5jhvindcvdbihpbd756y5ya0")))

(define-public crate-peg-syntax-ext-0.5 (crate (name "peg-syntax-ext") (vers "0.5.0") (deps (list (crate-dep (name "peg") (req "^0.5") (default-features #t) (kind 0)))) (hash "0y5w35iqhrn9fwxf8lp0njnbjsq8cwv4z4csiw2n84nzx7rnikj1")))

(define-public crate-peg-syntax-ext-0.5 (crate (name "peg-syntax-ext") (vers "0.5.2") (deps (list (crate-dep (name "peg") (req "^0.5") (default-features #t) (kind 0)))) (hash "1a01alv8d7hhk2hnaajgwwym919wxrk8a80xbx2p7y5w84b59ikk")))

(define-public crate-peg-syntax-ext-0.5 (crate (name "peg-syntax-ext") (vers "0.5.5") (deps (list (crate-dep (name "peg") (req "^0.5") (default-features #t) (kind 0)))) (hash "0kfqvccz5ynq368yi0jficlqr2y3hr56719kr7k70yxa3b69pnvx")))

(define-public crate-peg-syntax-ext-0.5 (crate (name "peg-syntax-ext") (vers "0.5.6") (deps (list (crate-dep (name "peg") (req "^0.5") (default-features #t) (kind 0)))) (hash "14x3aqq1234pq2ad2m0isb0js22g9jdl86dqlrpz7v7d0n8nq779")))

(define-public crate-peg-syntax-ext-0.5 (crate (name "peg-syntax-ext") (vers "0.5.7") (deps (list (crate-dep (name "peg") (req "^0.5") (default-features #t) (kind 0)))) (hash "15jkn9d0vfvp8gvdpqnw01zmhfz5j8774h2p9r551c2kbx3mbzj2")))

