(define-module (crates-io pe m_) #:use-module (crates-io))

(define-public crate-pem_helper-0.1 (crate (name "pem_helper") (vers "0.1.0") (deps (list (crate-dep (name "anyhow") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "rustls-pemfile") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1") (features (quote ("fs"))) (default-features #t) (kind 0)))) (hash "0r1s08bxf2lzf4z0nswm05c1fzig77nvjzyrj76ncb3f8aia2zcq") (yanked #t)))

