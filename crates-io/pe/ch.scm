(define-module (crates-io pe ch) #:use-module (crates-io))

(define-public crate-pechakucha-gen-0.1 (crate (name "pechakucha-gen") (vers "0.1.0") (hash "00zxfjxqwg7cskcwafs4s1j3c4d16q5mifp86kzdl09m2h2nmbls")))

(define-public crate-pechakucha-gen-0.1 (crate (name "pechakucha-gen") (vers "0.1.1") (hash "0z5gvj136ycz8lb125a6n793ys1gkk09a76f4bcjgp8cr8xyvzk0")))

(define-public crate-pecho-1 (crate (name "pecho") (vers "1.0.0") (deps (list (crate-dep (name "clap") (req "^2.33.3") (default-features #t) (kind 0)) (crate-dep (name "colored") (req "^2") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)))) (hash "1v22g049rgnyn0n4xr5nm73zh45kramknl3fpva5ssqdnsp6cy34")))

(define-public crate-pecho-2 (crate (name "pecho") (vers "2.0.0") (deps (list (crate-dep (name "clap") (req "^2.33.3") (default-features #t) (kind 0)) (crate-dep (name "colored") (req "^2") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)))) (hash "11rpnh75pvmn93y6xknk4msk6mi5z3v9dh46h7v9yz8llv0adi7k")))

(define-public crate-pecho-2 (crate (name "pecho") (vers "2.0.1") (deps (list (crate-dep (name "clap") (req "^2.33.3") (default-features #t) (kind 0)) (crate-dep (name "colored") (req "^2") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)))) (hash "0rn1fp6h61z7a95fzzh1v06w04xfkysi83m8b0sg0x2514jz0s7j")))

