(define-module (crates-io pe nt) #:use-module (crates-io))

(define-public crate-pentacle-0.1 (crate (name "pentacle") (vers "0.1.0") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)))) (hash "0v4j7ij9f6fpnmdpdsqcs0kbmav85fgc42rsws3i59alj949jhym")))

(define-public crate-pentacle-0.1 (crate (name "pentacle") (vers "0.1.1") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)))) (hash "0f76vcr3w6jzp21ibppdrvddvqs6jismpa3nwb5azrc7dxgwv2x6")))

(define-public crate-pentacle-0.2 (crate (name "pentacle") (vers "0.2.0") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)))) (hash "11hg4fj04737677267kqb4gfryx1l1wr09wvvx6zj2nnmld5yg5y")))

(define-public crate-pentacle-1 (crate (name "pentacle") (vers "1.0.0") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)))) (hash "1dp77q2ddwwryxjgv69hf3rqhb8r9r1wxjk8ra83i5wawgxy8vp2")))

(define-public crate-pentagram-0.1 (crate (name "pentagram") (vers "0.1.0") (hash "0sqh0w4l6wjrg7fmc7sdq7kyya0fr5sz63ab3jmaqqzg3v7pyx3r")))

(define-public crate-pentest-0.1 (crate (name "pentest") (vers "0.1.0") (hash "0dpm05a3w2k47dwy04bjfpd9hlf7qylly86mh710dmvc54i51bfy")))

(define-public crate-pentest-toolbox-0.1 (crate (name "pentest-toolbox") (vers "0.1.0") (hash "1l3gz3vg3bdb1vpjq2dfd6lmmkjswpq010ml6cqq1j78k42gcmdc")))

(define-public crate-pentest-toolbox-0.1 (crate (name "pentest-toolbox") (vers "0.1.1") (hash "0gb1qvs79qkqw9n8p4nr8lz86j4swiijg9s0h65dq0rz7yszhais")))

(define-public crate-pentest-toolbox-0.1 (crate (name "pentest-toolbox") (vers "0.1.2") (hash "0mwbnz8vzy13gkx0ry0q0gvx1hdvhk489pvar7gm4rpc9hyhjpzi")))

(define-public crate-pentest-toolbox-0.1 (crate (name "pentest-toolbox") (vers "0.1.3") (hash "00q5cnd4yps5z2clmd3kdh12p47w4csnc74fcz6j0ih405yxhl7f")))

(define-public crate-pentest-toolbox-0.1 (crate (name "pentest-toolbox") (vers "0.1.4") (hash "1xp8r7lg464s3bq7asnmz4zw959k2w826i40c273c0mgr5x019bd")))

(define-public crate-pentest-toolbox-0.1 (crate (name "pentest-toolbox") (vers "0.1.5") (hash "1qrn7slyrfbna0a089w9dc3yphc4n1wagry899bzldb457xps457")))

(define-public crate-pentest-toolbox-0.1 (crate (name "pentest-toolbox") (vers "0.1.6") (hash "0dff2r1vx0g6b5yi47yr5l1dc44ls24p0q98qxcag710wywmdql0")))

(define-public crate-pentest-toolbox-0.1 (crate (name "pentest-toolbox") (vers "0.1.7") (hash "03s4ggrwsr49f48xs46l3mznbpfaj7c1gk92rmcwg20skn704rfn")))

(define-public crate-pentest-toolbox-0.1 (crate (name "pentest-toolbox") (vers "0.1.8") (hash "0k2pk8jknxvr7rpcmhxwqyrbjh2zp0y393klwgbrrb5fxmmnqgjj")))

(define-public crate-pentest-toolbox-0.1 (crate (name "pentest-toolbox") (vers "0.1.9") (hash "1yny3m90lq6s448jc76il21svq0y3h1896pswcnmwq11fxjkhadm")))

(define-public crate-pentest-toolbox-0.1 (crate (name "pentest-toolbox") (vers "0.1.10") (hash "1y547vnlld8my5332qyfb2fgy6jq62lrnc1qljzrhq73svajiy2c")))

(define-public crate-pentest-toolbox-0.1 (crate (name "pentest-toolbox") (vers "0.1.11") (hash "0gx6crx3cmn15s832k0rm0nlxlan21806fy398h8vxwg9zp53s9r")))

(define-public crate-pentest-toolbox-0.1 (crate (name "pentest-toolbox") (vers "0.1.12") (hash "1wsi00gc8wsqyg27g40c6agxz4f1vk88l4zzqyyylvqbry7xfvs4")))

(define-public crate-pentest-toolbox-0.1 (crate (name "pentest-toolbox") (vers "0.1.13") (hash "1vhvbp6v9qajasqkbvma3n5cifhid6b80v8gqcq8j26yh4iaqnjy")))

(define-public crate-pentest-toolbox-0.1 (crate (name "pentest-toolbox") (vers "0.1.14") (hash "0vpqz4hmqyh1f0yy2mmb1y7l2hav74vydhgiiwnrg92pgyrvbk6g")))

(define-public crate-pentest-toolbox-improved-0.1 (crate (name "pentest-toolbox-improved") (vers "0.1.0") (hash "1yfmqgl527c77l7dkdpqa9ca0xrdybxya3b05y82yh69sfzaybr8")))

(define-public crate-pentest-toolbox-improved-0.1 (crate (name "pentest-toolbox-improved") (vers "0.1.2") (hash "0fx7x40pm65lwxlmc5vrslxj6ydkr57f6pn68sm6lki7izy3d3px") (yanked #t)))

(define-public crate-pentest-toolbox-improved-0.1 (crate (name "pentest-toolbox-improved") (vers "0.1.3") (hash "02sabslnvp0wsb1jjdlpqjzgs0gcx5h86isx35nfhv0z1w5swyi5")))

(define-public crate-pentest-toolbox-improved-0.1 (crate (name "pentest-toolbox-improved") (vers "0.1.4") (hash "1ppdk7hskf98z6415b05f7xnryqmdhg5dq5k16gqfpw8pnfnc0hl")))

(define-public crate-pentest-toolbox-improved-0.1 (crate (name "pentest-toolbox-improved") (vers "0.1.5") (hash "0mfyaylz5mcpf2ipy76dnawsl8bsfcdqip0z35h6f8fsc0i0crj1")))

(define-public crate-pentest-toolbox-improved-0.1 (crate (name "pentest-toolbox-improved") (vers "0.1.6") (hash "03ibx799vhf0fk0271yryd3zfk9z597zlqwqpcpybaimckwb68a9")))

(define-public crate-pentest-toolbox-improved-0.1 (crate (name "pentest-toolbox-improved") (vers "0.1.7") (hash "16afhb1ai4bhc9l07sk30qxfq8iinz13csjd50c74yjp5m9c1w7p")))

(define-public crate-pentest-toolbox-improved-0.1 (crate (name "pentest-toolbox-improved") (vers "0.1.8") (hash "16mmv6hdikxapgkl9xhyai7ibk9pxbwy95yy08hv6w2h9pdcmp7b")))

(define-public crate-pentest-toolbox-improved-0.1 (crate (name "pentest-toolbox-improved") (vers "0.1.9") (hash "106inc3566qhh297dqb4pbb4xcsjb7zhb2ghb3pd3kx06r99kcj9")))

(define-public crate-pentest-toolbox-improved-0.1 (crate (name "pentest-toolbox-improved") (vers "0.1.10") (hash "1x9ccqa9agzh6aaqdlwnkcgw8p24gygalpm6qjhifcw8117wmn19")))

(define-public crate-pentest-toolbox-improved-0.1 (crate (name "pentest-toolbox-improved") (vers "0.1.11") (hash "0zznd6klnmf1nkh77vsy2cmcmpg4kphkgqg4ym1gqknxk4zki30f")))

(define-public crate-pentest-toolbox-improved-0.1 (crate (name "pentest-toolbox-improved") (vers "0.1.12") (hash "1v6ji3ismhxij87f5vskv4yglq85iid6nvnd25wmhzk74wqdn2fz")))

(define-public crate-pentest-toolbox-improved-0.1 (crate (name "pentest-toolbox-improved") (vers "0.1.13") (hash "07bma5z09wy19si2k18wn70k66g0kfyl9l4gqgpzp6kx61am2wmj")))

(define-public crate-pentest-toolbox-improved-0.1 (crate (name "pentest-toolbox-improved") (vers "0.1.14") (hash "1gz7q011244cwmd5ay7h01yairiai1g5hci17npxfcxhn1zbczkh")))

(define-public crate-pentest-toolbox-improved-0.1 (crate (name "pentest-toolbox-improved") (vers "0.1.15") (hash "1jjhzrsqnyk7qq2k0z3l4qy3n5i3yk8hypcfylzhk7j0jsb06bww")))

(define-public crate-pentest-toolbox-improved-0.1 (crate (name "pentest-toolbox-improved") (vers "0.1.16") (hash "17h7c8pbzjpkhfg9v99gxjpy1szwwvxihk0zmmmjs4vw0knr4ngw")))

(define-public crate-pentest-toolbox-improved-0.1 (crate (name "pentest-toolbox-improved") (vers "0.1.17") (hash "1nbnvqw9ja38bf10yr0qxpnz0jy85hlqpyl20zbhbvwwz3r6xrsy")))

(define-public crate-pentest-toolbox-improved-0.1 (crate (name "pentest-toolbox-improved") (vers "0.1.18") (hash "1x1cfc0yqsrxs6vhczbnr664yw3bd9qflznp2hblc30m45g1i2w2")))

(define-public crate-pentest-toolbox-improved-0.1 (crate (name "pentest-toolbox-improved") (vers "0.1.19") (hash "1jvcxc30vykcgx3d2avmlrn0ghvnn3k2zgxy4a97lkrpnmsy1vaa")))

(define-public crate-pentest-toolbox-improved-0.1 (crate (name "pentest-toolbox-improved") (vers "0.1.20") (hash "1iqy1v2ncbz9gydfqdcndyj6by496cvlhpadwr37l3a4h8smmvzi")))

(define-public crate-pentest-toolbox-improved-0.1 (crate (name "pentest-toolbox-improved") (vers "0.1.21") (hash "0vw8nsfgw07jifvcnzxv0kgi1nz4wbw862xc44aikfxidam4fs3j")))

(define-public crate-pentest-toolbox-improved-0.1 (crate (name "pentest-toolbox-improved") (vers "0.1.23") (hash "1d82b6dggkqmcv6z6kvpvxjmrcfw7dj8gfs101czriaabfn8y9c1")))

(define-public crate-pentest-toolbox-improved-0.1 (crate (name "pentest-toolbox-improved") (vers "0.1.22") (hash "0zgr73n66mkqngnqvhhqgaidpwp53z4m3wnx9m2wl4mhn5sznjy0")))

(define-public crate-pentest-toolbox-improved-0.1 (crate (name "pentest-toolbox-improved") (vers "0.1.24") (hash "19f4xp7ib03x1ad5nqf8pm2axa2hd43cvda06d8xg3vni3vcs27f")))

(define-public crate-pentest-toolbox-improved-0.1 (crate (name "pentest-toolbox-improved") (vers "0.1.25") (hash "1pvi8cb9wy4i53ivjfvmispkajqi3ny5hk0hxk032pdwz91ya299")))

(define-public crate-pentest-toolbox-improved-0.1 (crate (name "pentest-toolbox-improved") (vers "0.1.26") (hash "02bqqqw23xl1l1lszkb4lhkprarrdahv5c6lhqm15d0fq7k9hq6q")))

(define-public crate-pentest-toolbox-improved-0.1 (crate (name "pentest-toolbox-improved") (vers "0.1.27") (hash "0cckj8lcr91xql9wvjiyy6k5l2n1hqwfd0g3ylyvb1qgghw2spbv")))

(define-public crate-pentest-toolbox-improved-0.1 (crate (name "pentest-toolbox-improved") (vers "0.1.28") (hash "1hmvh49by1iy9yjcfxzlyzirca7mw5k8dnrxjrs828aygl3p1n83")))

(define-public crate-pentest-toolbox-improved-0.1 (crate (name "pentest-toolbox-improved") (vers "0.1.29") (hash "1i9g7ag88580axcvh6l13qhdzmbspsh5nr3vysrzbjs8wym0sggn")))

(define-public crate-pentest-toolbox-improved-0.1 (crate (name "pentest-toolbox-improved") (vers "0.1.30") (hash "03jxmhrq2nmcppg1vpkcsvh7d7zp548hlwhrhfw83j7185lxn6fz")))

(define-public crate-pentest-toolbox-improved-0.1 (crate (name "pentest-toolbox-improved") (vers "0.1.31") (hash "0x8gz1pgd7crkpavpj1d1ypgxgpp60w3is9y8bz05dfljhcl4ygm")))

(define-public crate-pentest-toolbox-improved-0.1 (crate (name "pentest-toolbox-improved") (vers "0.1.32") (hash "029ch38nyv87p6651sp28zjkggkh4xa2k64k6ls71iqmyhwxw1ki")))

(define-public crate-pentest-toolbox-improved-0.1 (crate (name "pentest-toolbox-improved") (vers "0.1.33") (hash "08sp7s7qn5f408gnph8jvp8hdvbhvwb2xj068dvqnrg94skmg5a5")))

(define-public crate-pentest-toolbox-improved-0.1 (crate (name "pentest-toolbox-improved") (vers "0.1.34") (hash "1v4r5cfbh1awnia6f4g3fbjh0zwqw2kgq96mh74dcjs42x0pdx7y")))

(define-public crate-pentest-toolbox-improved-0.1 (crate (name "pentest-toolbox-improved") (vers "0.1.35") (hash "1kndxkrchvpx5s810ybc14ryzvvl3g4n00q1z2ybjpkv5h9ws4sn")))

(define-public crate-pentest-toolbox-improved-0.1 (crate (name "pentest-toolbox-improved") (vers "0.1.36") (hash "0wz63yly77ck7id2887bjyivnxyz6jrs8l00sz01p6nh9a1njgmv")))

(define-public crate-pentest-toolbox-improved-0.1 (crate (name "pentest-toolbox-improved") (vers "0.1.37") (hash "1vk6vb782fj1dg58k3a8pf9hx011bc4hhm7gwkagf7raq288q9k4")))

(define-public crate-pentry-0.1 (crate (name "pentry") (vers "0.1.0") (deps (list (crate-dep (name "getopts") (req "0.2.*") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "0.2.*") (default-features #t) (kind 0)) (crate-dep (name "procinfo") (req "0.2.*") (default-features #t) (target "cfg(unix)") (kind 0)))) (hash "12wkr512rjhzalrgxa6p518b7c90jm3i5y4yla98pm9xvlmb5nh5")))

(define-public crate-pentry-0.1 (crate (name "pentry") (vers "0.1.1") (deps (list (crate-dep (name "getopts") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "procinfo") (req "^0.3") (default-features #t) (target "cfg(unix)") (kind 0)))) (hash "07j70snqdz8gx58pp4gxkil2qb3s0r7xvkkjs5s0w9qqx791bz52")))

