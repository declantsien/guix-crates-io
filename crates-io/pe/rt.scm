(define-module (crates-io pe rt) #:use-module (crates-io))

(define-public crate-pertable-0.1 (crate (name "pertable") (vers "0.1.0") (hash "1dxg2d0rh8wjkqpb47gqzr6zz1kpdzgikm6320w80b4andq4vdl0") (yanked #t)))

(define-public crate-pertable-0.1 (crate (name "pertable") (vers "0.1.2") (hash "0fs4m7fpfp0irpix82372kpncf80dx81wxc8bmmgnzj8qwxffn2p")))

(define-public crate-pertable-0.1 (crate (name "pertable") (vers "0.1.3") (hash "12hm2gc40dyw6h1q7l1xrv8la81jmp1dzj0kr55rw54ak5mdd518")))

(define-public crate-pertable-0.1 (crate (name "pertable") (vers "0.1.4") (hash "18865d9f6zlaswarylck5flb9rl7fscs4scbp62m48kbn8xfqkbv")))

(define-public crate-perthread-0.1 (crate (name "perthread") (vers "0.1.0") (deps (list (crate-dep (name "lazy_static") (req "^1.0") (default-features #t) (kind 2)))) (hash "1kf1r8kxkdhlbb3kcycnz9h07mj68vxm28cpk9vsniwsr318psra")))

(define-public crate-perthread-0.1 (crate (name "perthread") (vers "0.1.1") (deps (list (crate-dep (name "lazy_static") (req "^1.0") (default-features #t) (kind 2)))) (hash "03zjvaxk11zsz7c4rsxyzrvwr5frxi6gpr388ly5bvbwfpajibqy")))

(define-public crate-perthread-0.1 (crate (name "perthread") (vers "0.1.2") (deps (list (crate-dep (name "lazy_static") (req "^1.0") (default-features #t) (kind 2)))) (hash "03mw7lkzwn6nl1azh16ils02jjap4ym307i9f8gi3hdiywwwrc0k")))

(define-public crate-perthread-0.1 (crate (name "perthread") (vers "0.1.3") (deps (list (crate-dep (name "once_cell") (req "^1.12") (default-features #t) (kind 2)))) (hash "1g6jbn0r0yz31qa3q47lkal818jm0p02vp7hacgpvfiycrp0cpq6")))

