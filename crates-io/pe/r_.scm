(define-module (crates-io pe r_) #:use-module (crates-io))

(define-public crate-per_test_directory-0.1 (crate (name "per_test_directory") (vers "0.1.0") (deps (list (crate-dep (name "per_test_directory_macros") (req "^0.1") (default-features #t) (kind 0)))) (hash "1s99d4n3ba570i80pg9wrkgakbz3nsfz505hs9g48rzs17v9pcg2")))

(define-public crate-per_test_directory_macros-0.1 (crate (name "per_test_directory_macros") (vers "0.1.0") (deps (list (crate-dep (name "quote") (req "^1.0.2") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.5") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "0pcs14r6p6sy6v00n6kfikggqcsgyvmq9ljdfjxbn6z36l6jln6s")))

