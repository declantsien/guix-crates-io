(define-module (crates-io ph al) #:use-module (crates-io))

(define-public crate-phala-allocator-0.1 (crate (name "phala-allocator") (vers "0.1.0") (hash "1929m1gyj44mvhkj61fr43l1h25ci4hpqizxwjk1snvx63k2xbkp")))

(define-public crate-phala-rocket-middleware-0.1 (crate (name "phala-rocket-middleware") (vers "0.1.0") (deps (list (crate-dep (name "log") (req "^0.4.16") (default-features #t) (kind 0)) (crate-dep (name "rocket") (req "^0.5.0") (default-features #t) (kind 0)))) (hash "15n4zi3h27wx73wpjzsmk1faf904d9h2nsdqkxyg62nsiy922jn5")))

(define-public crate-phala-rocket-middleware-0.1 (crate (name "phala-rocket-middleware") (vers "0.1.1") (deps (list (crate-dep (name "log") (req "^0.4.16") (default-features #t) (kind 0)) (crate-dep (name "rocket") (req "^0.5.0") (default-features #t) (kind 0)))) (hash "11w7b2x7l5c7b207dvr2nkwll8hfnk60dcx00yx2mqky3r659k9b")))

(define-public crate-phala-scheduler-0.1 (crate (name "phala-scheduler") (vers "0.1.0") (deps (list (crate-dep (name "rbtree") (req "^0.1.5") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1") (features (quote ("sync"))) (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1") (features (quote ("full"))) (default-features #t) (kind 2)))) (hash "0474ila5v1m97dxcjw16zy4vzddadxwxn0w6v8b5ql4xramdssc1")))

(define-public crate-phala-tokio-proxy-0.1 (crate (name "phala-tokio-proxy") (vers "0.1.0") (deps (list (crate-dep (name "bytes") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1") (features (quote ("full"))) (default-features #t) (kind 0)) (crate-dep (name "url") (req "^2.1.0") (default-features #t) (kind 0)))) (hash "0z05sgmr1zm6w7ffgclcv2csvlmfd8325gq9m5wyj3pqn1430z00")))

(define-public crate-phala-wasmer-tunables-0.1 (crate (name "phala-wasmer-tunables") (vers "0.1.0") (deps (list (crate-dep (name "wasmer") (req "^3") (default-features #t) (kind 0)))) (hash "10xb4mj1vx24pd1sjpd4qrijbrnmjajf1mr240ifsy0xqf12qar4")))

(define-public crate-phalanx-0.0.1 (crate (name "phalanx") (vers "0.0.1") (hash "1hn0lkjr3mzyn310jdcszvxpdb87fzzzlj7bjydxv4bvrsdfp3rk")))

(define-public crate-phalanx-0.0.2 (crate (name "phalanx") (vers "0.0.2") (hash "1g5558gw9laqnja1kixm0avijzi1l258fy45n8v4br6i1qc9lfp8")))

