(define-module (crates-io ph ps) #:use-module (crates-io))

(define-public crate-phpser-0.0.0 (crate (name "phpser") (vers "0.0.0") (deps (list (crate-dep (name "derive-new") (req "^0.5.8") (default-features #t) (kind 0)) (crate-dep (name "derive_more") (req "^0.99.5") (default-features #t) (kind 0)) (crate-dep (name "getset") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "1zbwalgin9fcwb6yi3wy6n0b6s13n7f9dlxdllir9v1n05sl07a9")))

