(define-module (crates-io ph os) #:use-module (crates-io))

(define-public crate-phos-0.0.1 (crate (name "phos") (vers "0.0.1") (hash "0dlfb66fszjjvwzp0ivlh1q5yr0k25f7hjvwbdl13j6lmnbdi5jb")))

(define-public crate-phos-0.0.2 (crate (name "phos") (vers "0.0.2") (hash "0w3yb8sss4ja1f7bfhqymz24cz9dwx6fl1zzzcgzgl1zic8a2bav")))

(define-public crate-phosphophyllite-0.1 (crate (name "phosphophyllite") (vers "0.1.0") (hash "1wkylfa1qd3fyaark01bwzajmyf0g86inf9qsg29w3akn7dvj0x8")))

(define-public crate-phosphor-0.0.1 (crate (name "phosphor") (vers "0.0.1") (deps (list (crate-dep (name "cgmath") (req "^0.17.0") (features (quote ("swizzle"))) (default-features #t) (kind 0)) (crate-dep (name "fnv") (req "^1.0.6") (default-features #t) (kind 0)) (crate-dep (name "half") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "hashbrown") (req "^0.6.2") (default-features #t) (kind 0)) (crate-dep (name "hemlock") (req "^0.0.2") (default-features #t) (kind 0)) (crate-dep (name "image") (req "^0.22.3") (default-features #t) (kind 0)) (crate-dep (name "imgui") (req "^0.2.1") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "nametag") (req "^0.0.1") (default-features #t) (kind 0)) (crate-dep (name "noise") (req "^0.6.0") (default-features #t) (kind 0)) (crate-dep (name "parking_lot") (req "^0.9.0") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.7.2") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.3.1") (default-features #t) (kind 1)) (crate-dep (name "rerun_except") (req "^0.1.0") (default-features #t) (kind 1)) (crate-dep (name "rusttype") (req "^0.8.1") (features (quote ("gpu_cache"))) (default-features #t) (kind 0)) (crate-dep (name "smallvec") (req "^1.0.0") (default-features #t) (kind 0)) (crate-dep (name "time") (req "^0.1.42") (default-features #t) (kind 0)) (crate-dep (name "tobj") (req "^0.1.11") (default-features #t) (kind 0)) (crate-dep (name "toolbelt") (req "^0.0.2") (default-features #t) (kind 0)) (crate-dep (name "vulkano") (req "^0.16.0") (default-features #t) (kind 0)) (crate-dep (name "vulkano-shaders") (req "^0.16.0") (default-features #t) (kind 0)) (crate-dep (name "walkdir") (req "^2.2.9") (default-features #t) (kind 1)) (crate-dep (name "winit") (req "^0.19.4") (default-features #t) (kind 0)) (crate-dep (name "xalloc") (req "^0.2.6") (default-features #t) (kind 0)))) (hash "0k4wlbzm0arbl3hcj2wz66nkkdy3c58szwnvsd9n32lv6kfxc5c5")))

(define-public crate-phosphor-leptos-0.1 (crate (name "phosphor-leptos") (vers "0.1.0") (deps (list (crate-dep (name "convert_case") (req "^0.6.0") (default-features #t) (kind 1)) (crate-dep (name "leptos") (req "^0.5.1") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.10.2") (default-features #t) (kind 1)))) (hash "0pa5dm2zk2wmq2fgci8bchbv3iq8dx2ij557xrz7xnlzi1ynq8i3")))

(define-public crate-phosphor-leptos-0.1 (crate (name "phosphor-leptos") (vers "0.1.1") (deps (list (crate-dep (name "leptos") (req "^0.5.1") (default-features #t) (kind 0)))) (hash "0rn7mf9ji1kib8h948zr1nhvggvaavqiqrgwpz17vhi05dc8vf6l")))

(define-public crate-phosphor-leptos-0.2 (crate (name "phosphor-leptos") (vers "0.2.0") (deps (list (crate-dep (name "leptos") (req "^0.5") (default-features #t) (kind 0)))) (hash "0v54yx6ancl830axsmnhj5ib5vffkfnvi61rfdf57s0kw5kj8jbm")))

(define-public crate-phosphor-leptos-0.2 (crate (name "phosphor-leptos") (vers "0.2.1") (deps (list (crate-dep (name "leptos") (req "^0.5") (default-features #t) (kind 0)))) (hash "0fawqfcm7r51m2dipx6ca3n1hl2ibzjd8f38xh5621csayl9n5xa")))

(define-public crate-phosphor-leptos-0.3 (crate (name "phosphor-leptos") (vers "0.3.0") (deps (list (crate-dep (name "leptos") (req "^0.6") (default-features #t) (kind 0)))) (hash "0a1nvgkm9la6ja5iizi2bvivzni99m11748p14974jldldc3h3n3")))

(define-public crate-phosphor-leptos-0.3 (crate (name "phosphor-leptos") (vers "0.3.1") (deps (list (crate-dep (name "leptos") (req "^0.6") (default-features #t) (kind 0)))) (hash "03frcr4ic1fj95s8wxdahiva60xh9i4pczwn525l81bbar5rmvx3") (features (quote (("weather") ("uncategorized") ("system") ("people") ("office") ("objects") ("nature") ("media") ("map") ("health") ("games") ("finance") ("editor") ("development") ("design") ("default" "all") ("communication") ("commerce") ("brand") ("arrows") ("all" "arrows" "brand" "commerce" "communication" "design" "development" "editor" "finance" "games" "health" "map" "media" "nature" "objects" "office" "people" "system" "uncategorized" "weather"))))))

(define-public crate-phosphor-leptos-0.4 (crate (name "phosphor-leptos") (vers "0.4.0") (deps (list (crate-dep (name "leptos") (req "^0.6") (default-features #t) (kind 0)))) (hash "1pny8nhn8bzy7c5vsa996zqwqldjma7m0s5arp24cvhg3gy74sx7") (features (quote (("weather") ("uncategorized") ("system") ("people") ("office") ("objects") ("nature") ("media") ("map") ("health") ("games") ("finance") ("editor") ("development") ("design") ("default" "all") ("communication") ("commerce") ("brand") ("arrows") ("all" "arrows" "brand" "commerce" "communication" "design" "development" "editor" "finance" "games" "health" "map" "media" "nature" "objects" "office" "people" "system" "uncategorized" "weather"))))))

(define-public crate-phosphor-leptos-0.5 (crate (name "phosphor-leptos") (vers "0.5.0") (deps (list (crate-dep (name "leptos") (req "^0.6") (default-features #t) (kind 0)))) (hash "15ywx0zsixf0g1llwhrwb4j1cmj25ms4fb8a46ffvyq7zh4kq290") (features (quote (("weather") ("uncategorized") ("system") ("people") ("office") ("objects") ("nature") ("media") ("map") ("health") ("games") ("finance") ("editor") ("development") ("design") ("default" "all") ("communication") ("commerce") ("brand") ("arrows") ("all" "arrows" "brand" "commerce" "communication" "design" "development" "editor" "finance" "games" "health" "map" "media" "nature" "objects" "office" "people" "system" "uncategorized" "weather"))))))

(define-public crate-phosphor-svgs-0.1 (crate (name "phosphor-svgs") (vers "0.1.0") (hash "1phn27m68y83f71dlp0ckch3d8hc6gh2miqxk0ac9k5gv0vsdhv9")))

(define-public crate-phosphor-svgs-0.2 (crate (name "phosphor-svgs") (vers "0.2.0") (hash "02jh8r102w2rbi612w8i2a8b9fv8qh2kb7cmi3v1i9a8gy52njny")))

(define-public crate-phosphorus-0.0.0 (crate (name "phosphorus") (vers "0.0.0") (deps (list (crate-dep (name "clap") (req "^2") (default-features #t) (kind 0)) (crate-dep (name "khronos_api") (req "^3") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "xml-rs") (req "^0.8") (default-features #t) (kind 0)))) (hash "0ldkbszs2lbmlx0qzprw1jilg2k069qkmh7gy7j70v8gai5y5wc2") (yanked #t)))

(define-public crate-phosphorus-0.0.1 (crate (name "phosphorus") (vers "0.0.1") (deps (list (crate-dep (name "clap") (req "^2") (default-features #t) (kind 0)) (crate-dep (name "khronos_api") (req "^3") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "xml-rs") (req "^0.8") (default-features #t) (kind 0)))) (hash "1kz74h8wm62al67454hajs0nv2d60rnl2yw6l74gq1fyz5rgjjrg")))

(define-public crate-phosphorus-0.0.2 (crate (name "phosphorus") (vers "0.0.2") (deps (list (crate-dep (name "clap") (req "^2") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "khronos_api") (req "^3") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "xml-rs") (req "^0.8") (default-features #t) (kind 0)))) (hash "1n0alh7yzvhbdvb9szl9mig8rhx30fzzdcrma77bs84acr9v5dfm")))

(define-public crate-phosphorus-0.0.3 (crate (name "phosphorus") (vers "0.0.3") (deps (list (crate-dep (name "clap") (req "^2") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "khronos_api") (req "^3") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "xml-rs") (req "^0.8") (default-features #t) (kind 0)))) (hash "0kpf9ciwql2z0hqbm5711sq64y80d3aq1nj7kmb4dxd8lsm17pq0")))

(define-public crate-phosphorus-0.0.4 (crate (name "phosphorus") (vers "0.0.4") (deps (list (crate-dep (name "chlorine") (req "^1") (default-features #t) (kind 2)) (crate-dep (name "hashbrown") (req "^0.6") (default-features #t) (kind 0)))) (hash "0pzmxqw51a43r5265df6rsyx7l1xs31k1ww079d68pd8wc95p2s8") (features (quote (("trace_calls") ("struct_loader") ("global_loader") ("error_checks") ("default" "global_loader" "struct_loader" "trace_calls" "error_checks"))))))

(define-public crate-phosphorus-0.0.5 (crate (name "phosphorus") (vers "0.0.5") (deps (list (crate-dep (name "magnesium") (req "^0.0.4") (default-features #t) (kind 0)))) (hash "151fmh3b91dlbhzl9q7zjj7qmsi5vb8sa8jq0lni7wnm8szfpyqj")))

(define-public crate-phosphorus-0.0.6 (crate (name "phosphorus") (vers "0.0.6") (deps (list (crate-dep (name "magnesium") (req "^0.0.4") (default-features #t) (kind 0)))) (hash "059l0xdz7k6vgks75vymiwlzzjndafiqxcmisk4mb53382a80gbr")))

(define-public crate-phosphorus-0.0.7 (crate (name "phosphorus") (vers "0.0.7") (deps (list (crate-dep (name "magnesium") (req "^0.0.4") (default-features #t) (kind 0)))) (hash "0s9cbnrr0v18wi56dd968dspf8xilj0gppqhmyqk8pihvaxdqcbk")))

(define-public crate-phosphorus-0.0.8 (crate (name "phosphorus") (vers "0.0.8") (deps (list (crate-dep (name "magnesium") (req "^0.0.4") (default-features #t) (kind 0)))) (hash "0z392j2xx808sknsai22z2yk93kapp2fvacs7d7ll52xi670qw9p")))

(define-public crate-phosphorus-0.0.9 (crate (name "phosphorus") (vers "0.0.9") (deps (list (crate-dep (name "magnesium") (req "^0.0.4") (default-features #t) (kind 0)))) (hash "01z59z4m58v1yw0rpx33j3f026dqxljlmz1lkcgb2wz0kjdahzah")))

(define-public crate-phosphorus-0.0.10 (crate (name "phosphorus") (vers "0.0.10") (deps (list (crate-dep (name "magnesium") (req "^0.0.4") (default-features #t) (kind 0)))) (hash "11qzmmqyd7cnl5dsyd2rx2frk8vd60747d5xn5drx34zy54chmwb")))

(define-public crate-phosphorus-0.0.11 (crate (name "phosphorus") (vers "0.0.11") (deps (list (crate-dep (name "magnesium") (req "^1") (default-features #t) (kind 0)))) (hash "0kgs66wkc3amnmbsil7x354clgq1xiv8267bg8s2lgvjvqhs70qs")))

(define-public crate-phosphorus-0.0.12 (crate (name "phosphorus") (vers "0.0.12") (deps (list (crate-dep (name "magnesium") (req "^1") (default-features #t) (kind 0)))) (hash "0241g5vzfkcbrpahlpsrwj008zlh722m98hs92rzxal13xzdj420")))

(define-public crate-phosphorus-0.0.13 (crate (name "phosphorus") (vers "0.0.13") (deps (list (crate-dep (name "magnesium") (req "^1") (default-features #t) (kind 0)))) (hash "1aw8rzip18ba9qcqy2fr1jhv5n4f6c44by8nqz4mcnz3cjm62cd9")))

(define-public crate-phosphorus-0.0.14 (crate (name "phosphorus") (vers "0.0.14") (deps (list (crate-dep (name "magnesium") (req "^1") (default-features #t) (kind 0)))) (hash "0hb8rrzwpvrz2zzxrnypg153iwjiiavcqynq2mnqzhysxnpga5wh")))

(define-public crate-phosphorus-0.0.15 (crate (name "phosphorus") (vers "0.0.15") (deps (list (crate-dep (name "magnesium") (req "^1") (default-features #t) (kind 0)))) (hash "1lgy49dpk4qmzhws6kr03p4rj2zw3phvjbzwq9sap5mmay5kzl3b")))

(define-public crate-phosphorus-0.0.16 (crate (name "phosphorus") (vers "0.0.16") (deps (list (crate-dep (name "magnesium") (req "^1") (default-features #t) (kind 0)))) (hash "1n0j0sc49h0mjp0jdmiz0cck2b3z04f9gjhzm7mgkv58nrrc0r4g")))

(define-public crate-phosphorus-0.0.17 (crate (name "phosphorus") (vers "0.0.17") (deps (list (crate-dep (name "magnesium") (req "^1") (default-features #t) (kind 0)))) (hash "05w9m1flla3kvdd9yk7wwjp7h1izq3fj797fblf3haaryf4azmcy")))

(define-public crate-phosphorus-0.0.18 (crate (name "phosphorus") (vers "0.0.18") (deps (list (crate-dep (name "magnesium") (req "^1") (default-features #t) (kind 0)))) (hash "1wzjb69zl1jprrlls52bgg98zmjaw4f4ivrkpwa0lgdw706a84nb")))

(define-public crate-phosphorus-0.0.19 (crate (name "phosphorus") (vers "0.0.19") (deps (list (crate-dep (name "magnesium") (req "^1") (default-features #t) (kind 0)))) (hash "0ymva54pfh828lskpi6bbpxz5w8pvbdfmz8v4mk0dhvmwy907801")))

(define-public crate-phosphorus-0.0.20 (crate (name "phosphorus") (vers "0.0.20") (deps (list (crate-dep (name "magnesium") (req "^1") (default-features #t) (kind 0)))) (hash "1iz96issswxf8lpkd4br0c65fbv6lj05s1hpnvcr1gnzplvzf32n")))

(define-public crate-phosphorus-0.0.21 (crate (name "phosphorus") (vers "0.0.21") (deps (list (crate-dep (name "magnesium") (req "^1") (default-features #t) (kind 0)))) (hash "1n62m1x9vgxdbrsf7zmjgm3h1pyw72j649w2q60i2k638i2gflh4")))

(define-public crate-phosphorus-0.0.22 (crate (name "phosphorus") (vers "0.0.22") (deps (list (crate-dep (name "magnesium") (req "^1") (default-features #t) (kind 0)))) (hash "0qcfyi8k7p7pv1nmi9vgzfbiwjp2bm17qxkmhdr0sj8h2vkgnj8j")))

(define-public crate-phosphorus-0.0.23 (crate (name "phosphorus") (vers "0.0.23") (deps (list (crate-dep (name "magnesium") (req "^1") (default-features #t) (kind 0)))) (hash "06mnwrbirsvfys1plc5baf0svjgjxz84dw77ijknrwh6k18j9s69")))

(define-public crate-phosphorus-0.0.24 (crate (name "phosphorus") (vers "0.0.24") (deps (list (crate-dep (name "magnesium") (req "^1") (default-features #t) (kind 0)))) (hash "0lc2bak6k0jw8x5zn4dskcndhvfmcaxc2l8igf1wzbj39pjb0j7a")))

(define-public crate-phosphorus-1 (crate (name "phosphorus") (vers "1.0.0") (deps (list (crate-dep (name "clap") (req "^4") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "magnesium") (req "^1.2") (default-features #t) (kind 0)) (crate-dep (name "tinyvec") (req "^1") (features (quote ("alloc"))) (default-features #t) (kind 0)))) (hash "1xxll4v9qb0nmiz1f9pjb8c8z86apq78s3gs2iniw85zxwh9q68s")))

(define-public crate-phosphorus-2 (crate (name "phosphorus") (vers "2.0.0") (deps (list (crate-dep (name "clap") (req "^4") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "magnesium") (req "^1.2") (default-features #t) (kind 0)) (crate-dep (name "tinyvec") (req "^1") (features (quote ("alloc"))) (default-features #t) (kind 0)))) (hash "0iszbgmg7g5gz05hqbf21hpan0099xw2fqjizapgkiz4wv98xqyi")))

(define-public crate-phosphorus-2 (crate (name "phosphorus") (vers "2.0.1") (deps (list (crate-dep (name "clap") (req "^4") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "magnesium") (req "^1.2") (default-features #t) (kind 0)) (crate-dep (name "tinyvec") (req "^1") (features (quote ("alloc"))) (default-features #t) (kind 0)))) (hash "1mdfm223i8jz5cm1kkkmknl6fwx33qq3m891y9zsr5dccz0mac0d")))

(define-public crate-phosphorus-2 (crate (name "phosphorus") (vers "2.0.2") (deps (list (crate-dep (name "clap") (req "^4") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "magnesium") (req "^1.2") (default-features #t) (kind 0)) (crate-dep (name "tinyvec") (req "^1") (features (quote ("alloc"))) (default-features #t) (kind 0)))) (hash "0finxkmfmi5cngrxwfq5x7d6kp6fx0sbgla38i3036nvifwd0h2c")))

(define-public crate-phosphorus-2 (crate (name "phosphorus") (vers "2.0.3") (deps (list (crate-dep (name "clap") (req "^4") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "magnesium") (req "^1.2") (default-features #t) (kind 0)) (crate-dep (name "tinyvec") (req "^1") (features (quote ("alloc"))) (default-features #t) (kind 0)))) (hash "01b5f9fqpbl14r67qnzdfp4310lrr04d05jy374f518p0bkiv9md")))

