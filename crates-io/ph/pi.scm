(define-module (crates-io ph pi) #:use-module (crates-io))

(define-public crate-phpify-0.1 (crate (name "phpify") (vers "0.1.0-beta1") (hash "0bksrlwvics20hknvsvsmww84frnl7cwc0534x0gqjd4pww1j0s5")))

(define-public crate-phpify-0.1 (crate (name "phpify") (vers "0.1.0-beta2") (hash "0vy5jrsajm8dqapbsangp9wrnazcpz2zx1f13gkv3zqpl22pll8w")))

(define-public crate-phpify-0.1 (crate (name "phpify") (vers "0.1.0-beta3") (hash "1b5l39f6r4zcm6sgmr1yrrgby0fd5g769nnjbc27lcv6yy6mz07b")))

(define-public crate-phpify-0.1 (crate (name "phpify") (vers "0.1.0-beta4") (deps (list (crate-dep (name "rand") (req "^0.7.3") (default-features #t) (kind 0)))) (hash "1z9rwxfvp26wmr2lq3vklwz236sdfhsv7bx0i15kb1syzaw4yhh1")))

