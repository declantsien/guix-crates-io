(define-module (crates-io ph az) #:use-module (crates-io))

(define-public crate-phazer-0.1 (crate (name "phazer") (vers "0.1.0") (hash "0qpn0rb6d5acalp55bvaxgc54mlyx3izpi0sw61hn7308fyihchy")))

(define-public crate-phazer-0.1 (crate (name "phazer") (vers "0.1.1") (hash "09cv28135dri6vdsk5b4c8ilrq50wyxvq5kwndkqfyw0hc85frsg") (features (quote (("simple"))))))

(define-public crate-phazer-0.1 (crate (name "phazer") (vers "0.1.2") (deps (list (crate-dep (name "futures-util") (req "^0.3.29") (default-features #t) (kind 2)) (crate-dep (name "reqwest") (req "^0.11.22") (features (quote ("stream"))) (default-features #t) (kind 2)) (crate-dep (name "tokio") (req "^1") (features (quote ("fs"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1") (features (quote ("full"))) (default-features #t) (kind 2)) (crate-dep (name "url") (req "^2.5.0") (default-features #t) (kind 2)))) (hash "0n31j0l3cdxqb8366ldfyf3w8z2mqa55cvi36mn01pgc3rrc1g0y") (features (quote (("simple"))))))

