(define-module (crates-io ph -m) #:use-module (crates-io))

(define-public crate-ph-mobile-network-0.1 (crate (name "ph-mobile-network") (vers "0.1.0") (deps (list (crate-dep (name "regex") (req "^1.10.4") (default-features #t) (kind 0)))) (hash "07sasjgw4qj6wv0k5g5xg8faignqqxch8diacxq142v6d6795w7v")))

(define-public crate-ph-mobile-network-0.1 (crate (name "ph-mobile-network") (vers "0.1.1") (deps (list (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.10.4") (default-features #t) (kind 0)))) (hash "0qj2p5gaz3j4arlsn7kphhkhahg7pw9gl8fyzmlw5g4izykizy54")))

(define-public crate-ph-mobile-network-0.1 (crate (name "ph-mobile-network") (vers "0.1.2") (deps (list (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.10.4") (default-features #t) (kind 0)))) (hash "11rwi3ckzvcll113j3xai6zmy6r4vniaiffwgqnwv7jyr0x4d4y7")))

(define-public crate-ph-mobile-network-0.1 (crate (name "ph-mobile-network") (vers "0.1.3") (deps (list (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.10.4") (default-features #t) (kind 0)))) (hash "09mdkxn15msjc7cg5h293ay081wghrhh4vsbgv4mj7flkmng6b3f")))

(define-public crate-ph-mobile-network-0.1 (crate (name "ph-mobile-network") (vers "0.1.4") (deps (list (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.10.4") (default-features #t) (kind 0)))) (hash "0an5dclp5vb67arb4n9rgv7l5bw1az9vjjh6sb6vyyqrmv7pkpjn")))

