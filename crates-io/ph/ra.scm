(define-module (crates-io ph ra) #:use-module (crates-io))

(define-public crate-phrace-0.1 (crate (name "phrace") (vers "0.1.0") (deps (list (crate-dep (name "terminal_size") (req "^0.3.0") (default-features #t) (kind 0)))) (hash "0hmddmminlmarwfd8qxrnsb8sylgiqrmzadc3zv0yglrkgfa6r9n")))

(define-public crate-phrace-0.2 (crate (name "phrace") (vers "0.2.0") (deps (list (crate-dep (name "lexopt") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "terminal_size") (req "^0.3.0") (default-features #t) (kind 0)))) (hash "0dd62b21aib3ylb7hlhfsakzfbfl92g43q7hkl2gpb0jajgph0nx")))

(define-public crate-phrace-0.2 (crate (name "phrace") (vers "0.2.1") (deps (list (crate-dep (name "lexopt") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "terminal_size") (req "^0.3.0") (default-features #t) (kind 0)))) (hash "1qhxavipvb7y8rvxfc4aindnbsd784n5p71s7q42y0vf50j6rsv1")))

(define-public crate-phrace-0.2 (crate (name "phrace") (vers "0.2.2") (deps (list (crate-dep (name "lexopt") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "terminal_size") (req "^0.3.0") (default-features #t) (kind 0)))) (hash "1wgw1pxi36wjm50hpba7rw8d5c3gj6k032xzrgqa40yw1m0hxics")))

(define-public crate-phrace-0.2 (crate (name "phrace") (vers "0.2.3") (deps (list (crate-dep (name "lexopt") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "terminal_size") (req "^0.3.0") (default-features #t) (kind 0)))) (hash "1grjybvbacm2632awa0zsqkvdhxh6xb31hz9m0p7z5xk4bps2fn2")))

(define-public crate-phrace-0.2 (crate (name "phrace") (vers "0.2.4") (deps (list (crate-dep (name "lexopt") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "terminal_size") (req "^0.3.0") (default-features #t) (kind 0)))) (hash "15ik4mvna13mydgajwc87994fnjdsz30gc4k9r6ni01zqs50yjx6")))

(define-public crate-phrase-0.1 (crate (name "phrase") (vers "0.1.1") (deps (list (crate-dep (name "rand") (req ">=0.7.3, <0.8.0") (default-features #t) (kind 0)))) (hash "0bk9x0a7lq0img60gsc7fscf9g5njw6ip12pwc69dvj48pgzcvwl")))

(define-public crate-phrases-0.1 (crate (name "phrases") (vers "0.1.0") (hash "04pqybhcxzqjvq1l24hnkpbfm6kbs4gqc59vv3x4pwrzvc8qpsvk")))

(define-public crate-phrases-0.0.1 (crate (name "phrases") (vers "0.0.1") (hash "0lnf8j221lfh8bcrygrcmwjrfs3638fyy6i130rw6azg27j6ifk6")))

(define-public crate-phraze-0.3 (crate (name "phraze") (vers "0.3.3") (deps (list (crate-dep (name "clap") (req "^4.4.7") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "criterion") (req "^0.5.1") (default-features #t) (kind 2)) (crate-dep (name "include-lines") (req "^1.1.2") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)) (crate-dep (name "unicode-normalization") (req "^0.1.22") (default-features #t) (kind 0)))) (hash "0r6pdf3vi1jbkxp7kv2v4xc9d16sz9z0f9cnlvkfgvjrrzpff3aa")))

(define-public crate-phraze-0.3 (crate (name "phraze") (vers "0.3.4") (deps (list (crate-dep (name "clap") (req "^4.4.7") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "criterion") (req "^0.5.1") (default-features #t) (kind 2)) (crate-dep (name "include-lines") (req "^1.1.2") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)) (crate-dep (name "unicode-normalization") (req "^0.1.22") (default-features #t) (kind 0)))) (hash "0lp9nhd3hcyzgpail9dl0pbp18gi4kinp7j7rywm1xc88dcr1pw3")))

(define-public crate-phraze-0.3 (crate (name "phraze") (vers "0.3.5") (deps (list (crate-dep (name "clap") (req "^4.4.7") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "criterion") (req "^0.5.1") (default-features #t) (kind 2)) (crate-dep (name "include-lines") (req "^1.1.2") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)) (crate-dep (name "unicode-normalization") (req "^0.1.22") (default-features #t) (kind 0)))) (hash "1magbwqmpa2qlvj9aqa0rvp3g2kycvgg808rq7v7q1d43f2ik22q")))

(define-public crate-phraze-0.3 (crate (name "phraze") (vers "0.3.6") (deps (list (crate-dep (name "clap") (req "^4.4.7") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "criterion") (req "^0.5.1") (default-features #t) (kind 2)) (crate-dep (name "include-lines") (req "^1.1.2") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)) (crate-dep (name "unicode-normalization") (req "^0.1.22") (default-features #t) (kind 0)))) (hash "06jiymm8rp2kmggi676xls18imy2az7m4mn66g5piaqlh3gq0p35")))

