(define-module (crates-io ph pa) #:use-module (crates-io))

(define-public crate-phpass-0.1 (crate (name "phpass") (vers "0.1.0") (deps (list (crate-dep (name "base64") (req "^0.12") (default-features #t) (kind 0)) (crate-dep (name "md5") (req "^0.7") (default-features #t) (kind 0)))) (hash "1nkpzbp9vbx4lsdqwinb3awrkpkzsnqvw7gkvmrdv7nichll8wba")))

(define-public crate-phpass-0.1 (crate (name "phpass") (vers "0.1.1") (deps (list (crate-dep (name "base64") (req "^0.12") (default-features #t) (kind 0)) (crate-dep (name "md5") (req "^0.7") (default-features #t) (kind 0)))) (hash "1amnywx5s3pd84d2j0nmpzxdbbfldfpcw8hznrqqpyvpgq7ayzvp")))

(define-public crate-phpass-0.1 (crate (name "phpass") (vers "0.1.2") (deps (list (crate-dep (name "base64") (req "^0.12") (default-features #t) (kind 0)) (crate-dep (name "md5") (req "^0.7") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.7.3") (default-features #t) (kind 0)))) (hash "11hjrqrd4lwip6dmxz2gkk59j7skw5a08m359l06rvz7zkm0g7zl")))

(define-public crate-phpass-0.1 (crate (name "phpass") (vers "0.1.3") (deps (list (crate-dep (name "base64") (req "^0.12") (default-features #t) (kind 0)) (crate-dep (name "md5") (req "^0.7") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.7.3") (default-features #t) (kind 0)))) (hash "0bh939c95mz3r6kpi91id2mjka7l7km2zs148dsi5qiswkzvghv8")))

