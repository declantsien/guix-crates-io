(define-module (crates-io ph t-) #:use-module (crates-io))

(define-public crate-pht-crypto-0.1 (crate (name "pht-crypto") (vers "0.1.0-alpha") (deps (list (crate-dep (name "anyhow") (req "^1.0.43") (default-features #t) (kind 0)) (crate-dep (name "glass_pumpkin") (req "^1.1.0") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.4") (default-features #t) (kind 0)) (crate-dep (name "rug") (req "^1.13.0") (features (quote ("integer" "rand" "serde"))) (kind 0)) (crate-dep (name "serde") (req "^1.0.129") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "1fzhvph4h3n20qvbx5gm082mlzk0dm0ygqdh662l0w7208wmxypx")))

