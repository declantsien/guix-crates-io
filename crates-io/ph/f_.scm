(define-module (crates-io ph f_) #:use-module (crates-io))

(define-public crate-phf_builder-0.7 (crate (name "phf_builder") (vers "0.7.14") (deps (list (crate-dep (name "phf") (req "= 0.7.14") (default-features #t) (kind 0)) (crate-dep (name "phf_generator") (req "= 0.7.14") (default-features #t) (kind 0)))) (hash "15hfrf8bchh1q49z147mssaj78nngwgyk5yxkynf7v5z402062bs")))

(define-public crate-phf_builder-0.7 (crate (name "phf_builder") (vers "0.7.15") (deps (list (crate-dep (name "phf") (req "= 0.7.15") (default-features #t) (kind 0)) (crate-dep (name "phf_generator") (req "= 0.7.15") (default-features #t) (kind 0)))) (hash "1xch7ffvs6ygnamywfb8yyq5z7bqhmsz29g5qbp6ni2yf07n9x2w")))

(define-public crate-phf_builder-0.7 (crate (name "phf_builder") (vers "0.7.16") (deps (list (crate-dep (name "phf") (req "= 0.7.16") (default-features #t) (kind 0)) (crate-dep (name "phf_generator") (req "= 0.7.16") (default-features #t) (kind 0)))) (hash "0wq7ssm4bzlvbn0ph1qsicd6zl5q8dkqc3v8kvhx6q6syr9k90ka")))

(define-public crate-phf_builder-0.7 (crate (name "phf_builder") (vers "0.7.17") (deps (list (crate-dep (name "phf") (req "= 0.7.17") (default-features #t) (kind 0)) (crate-dep (name "phf_generator") (req "= 0.7.17") (default-features #t) (kind 0)))) (hash "0r93zb9vf1vrsdry5jn8n509sbhxzpl27im0gq6yxd1izkg1aajg")))

(define-public crate-phf_builder-0.7 (crate (name "phf_builder") (vers "0.7.18") (deps (list (crate-dep (name "phf") (req "= 0.7.18") (default-features #t) (kind 0)) (crate-dep (name "phf_generator") (req "= 0.7.18") (default-features #t) (kind 0)))) (hash "0jlmwq4x8nbvmgsjf5aig1y7z6yj9s0fshrwmgiv2gwvwvjf5xrp")))

(define-public crate-phf_builder-0.7 (crate (name "phf_builder") (vers "0.7.19") (deps (list (crate-dep (name "phf") (req "= 0.7.19") (default-features #t) (kind 0)) (crate-dep (name "phf_generator") (req "= 0.7.19") (default-features #t) (kind 0)))) (hash "0826xxzzg0zl4p8jimckf5xxss7i982an9y3p80pn8jh7r27xcg5")))

(define-public crate-phf_builder-0.7 (crate (name "phf_builder") (vers "0.7.20") (deps (list (crate-dep (name "phf") (req "= 0.7.20") (default-features #t) (kind 0)) (crate-dep (name "phf_generator") (req "= 0.7.20") (default-features #t) (kind 0)))) (hash "03wax6wimgxajj8ria6wb6hh8dylslahkyddgc0z440knrm88l5n")))

(define-public crate-phf_builder-0.7 (crate (name "phf_builder") (vers "0.7.21") (deps (list (crate-dep (name "phf") (req "= 0.7.21") (default-features #t) (kind 0)) (crate-dep (name "phf_generator") (req "= 0.7.21") (default-features #t) (kind 0)))) (hash "06sykzalcfs8ya7vnx5hxfx71dahj08bbz891pmcxs7j9knwgsmc")))

(define-public crate-phf_builder-0.7 (crate (name "phf_builder") (vers "0.7.22") (deps (list (crate-dep (name "phf") (req "= 0.7.22") (default-features #t) (kind 0)) (crate-dep (name "phf_generator") (req "= 0.7.22") (default-features #t) (kind 0)))) (hash "0m775x7h74gd9pw2ibr7d5m2r0f9cl3qgviiljc258idwn2xds8s")))

(define-public crate-phf_builder-0.7 (crate (name "phf_builder") (vers "0.7.23") (deps (list (crate-dep (name "phf") (req "= 0.7.23") (default-features #t) (kind 0)) (crate-dep (name "phf_generator") (req "= 0.7.23") (default-features #t) (kind 0)))) (hash "07zz8pkxx8mvivrdsjrcm8vkfq7ygmrlsiygg3ccry2dcbp6dmn9")))

(define-public crate-phf_builder-0.7 (crate (name "phf_builder") (vers "0.7.24") (deps (list (crate-dep (name "phf") (req "^0.7.24") (default-features #t) (kind 0)) (crate-dep (name "phf_generator") (req "^0.7.24") (default-features #t) (kind 0)))) (hash "09891gv9wvsp6d0xn22y5mkpng0yrb8d7r4bfq0whh545nq7zvrk")))

(define-public crate-phf_builder-0.7 (crate (name "phf_builder") (vers "0.7.25") (deps (list (crate-dep (name "phf") (req "^0.7.24") (default-features #t) (kind 0)) (crate-dep (name "phf_generator") (req "^0.7.24") (default-features #t) (kind 0)))) (hash "1awihp5kxddzda62pawr8cpd49yx35wzdp6nij58944fmiyj0yj4")))

(define-public crate-phf_codegen-0.6 (crate (name "phf_codegen") (vers "0.6.10") (deps (list (crate-dep (name "phf") (req "= 0.6.10") (default-features #t) (kind 2)) (crate-dep (name "phf_generator") (req "= 0.6.10") (default-features #t) (kind 0)) (crate-dep (name "phf_shared") (req "= 0.6.10") (default-features #t) (kind 0)))) (hash "18yn4r79vgm0ys4z99s4zhyq9d56358dipa83jvadw82k1g5x2mx")))

(define-public crate-phf_codegen-0.6 (crate (name "phf_codegen") (vers "0.6.11") (deps (list (crate-dep (name "phf") (req "= 0.6.11") (default-features #t) (kind 2)) (crate-dep (name "phf_generator") (req "= 0.6.11") (default-features #t) (kind 0)) (crate-dep (name "phf_shared") (req "= 0.6.11") (default-features #t) (kind 0)))) (hash "1pmd5h3x4hmmwlkwbb0rs2l03f9ip2mp784f691p87m3cyi9qzkl")))

(define-public crate-phf_codegen-0.6 (crate (name "phf_codegen") (vers "0.6.12") (deps (list (crate-dep (name "phf") (req "= 0.6.12") (default-features #t) (kind 2)) (crate-dep (name "phf_generator") (req "= 0.6.12") (default-features #t) (kind 0)) (crate-dep (name "phf_shared") (req "= 0.6.12") (default-features #t) (kind 0)))) (hash "06s6cnh39pxdrjrd0ghy2axknw25c05gmsmj9llfzwp0vp5sm2bk")))

(define-public crate-phf_codegen-0.6 (crate (name "phf_codegen") (vers "0.6.13") (deps (list (crate-dep (name "phf") (req "= 0.6.13") (default-features #t) (kind 2)) (crate-dep (name "phf_generator") (req "= 0.6.13") (default-features #t) (kind 0)) (crate-dep (name "phf_shared") (req "= 0.6.13") (default-features #t) (kind 0)))) (hash "0l817apx19vj3dr02limzbzic0527gkdz9vvkg3jw508fy99n14j")))

(define-public crate-phf_codegen-0.6 (crate (name "phf_codegen") (vers "0.6.14") (deps (list (crate-dep (name "phf") (req "= 0.6.14") (default-features #t) (kind 2)) (crate-dep (name "phf_generator") (req "= 0.6.14") (default-features #t) (kind 0)) (crate-dep (name "phf_shared") (req "= 0.6.14") (default-features #t) (kind 0)))) (hash "0g9n38vgjq2xc03qyjms9f2562qxcic3vmwvgyg9f4p11d9pzwww")))

(define-public crate-phf_codegen-0.6 (crate (name "phf_codegen") (vers "0.6.15") (deps (list (crate-dep (name "phf_generator") (req "= 0.6.15") (default-features #t) (kind 0)) (crate-dep (name "phf_shared") (req "= 0.6.15") (default-features #t) (kind 0)))) (hash "034w55azd7bc3kmp1f91b1dz63ii2ncv0jw81v5gvksm7l0aadwf")))

(define-public crate-phf_codegen-0.6 (crate (name "phf_codegen") (vers "0.6.17") (deps (list (crate-dep (name "phf_generator") (req "= 0.6.17") (default-features #t) (kind 0)) (crate-dep (name "phf_shared") (req "= 0.6.17") (default-features #t) (kind 0)))) (hash "0mck0bm31pkmw2zm0drnf1zwck0b7l9cqzrcf6b7lk7sv8az6jiq")))

(define-public crate-phf_codegen-0.6 (crate (name "phf_codegen") (vers "0.6.18") (deps (list (crate-dep (name "phf_generator") (req "= 0.6.18") (default-features #t) (kind 0)) (crate-dep (name "phf_shared") (req "= 0.6.18") (default-features #t) (kind 0)))) (hash "15274cjh69xiyv2x4536jh8lg083fcbh1yhy05yyb1d8s2939zs7")))

(define-public crate-phf_codegen-0.6 (crate (name "phf_codegen") (vers "0.6.19") (deps (list (crate-dep (name "phf_generator") (req "= 0.6.19") (default-features #t) (kind 0)) (crate-dep (name "phf_shared") (req "= 0.6.19") (default-features #t) (kind 0)))) (hash "0a88nqrp6qxa0qrq4ji1bskhf1aqn48jag0aqwinlw3dd18wmcky")))

(define-public crate-phf_codegen-0.7 (crate (name "phf_codegen") (vers "0.7.0") (deps (list (crate-dep (name "phf_generator") (req "= 0.7.0") (default-features #t) (kind 0)) (crate-dep (name "phf_shared") (req "= 0.7.0") (default-features #t) (kind 0)))) (hash "19b5k844vlhfc2cw3x0b9lm8p8xmnfhj6pnzydhacpnlqdkigici")))

(define-public crate-phf_codegen-0.7 (crate (name "phf_codegen") (vers "0.7.1") (deps (list (crate-dep (name "phf_generator") (req "= 0.7.1") (default-features #t) (kind 0)) (crate-dep (name "phf_shared") (req "= 0.7.1") (default-features #t) (kind 0)))) (hash "1zgdaw1f98l3vh2f69ysi4a2vh6dqwbz8wd09lndydlyhdzq8vxb")))

(define-public crate-phf_codegen-0.7 (crate (name "phf_codegen") (vers "0.7.2") (deps (list (crate-dep (name "phf_generator") (req "= 0.7.2") (default-features #t) (kind 0)) (crate-dep (name "phf_shared") (req "= 0.7.2") (default-features #t) (kind 0)))) (hash "1x9b5l336b2y5mkj0k1rhg10wm33rn600mv1b10265vx1l23y60k")))

(define-public crate-phf_codegen-0.7 (crate (name "phf_codegen") (vers "0.7.3") (deps (list (crate-dep (name "phf_generator") (req "= 0.7.3") (default-features #t) (kind 0)) (crate-dep (name "phf_shared") (req "= 0.7.3") (default-features #t) (kind 0)))) (hash "0srwpfv6p67c4pjdzp7a6132r21gq3n8gk6fr67qh1v5wxi5crpn")))

(define-public crate-phf_codegen-0.7 (crate (name "phf_codegen") (vers "0.7.4") (deps (list (crate-dep (name "phf_generator") (req "= 0.7.4") (default-features #t) (kind 0)) (crate-dep (name "phf_shared") (req "= 0.7.4") (default-features #t) (kind 0)))) (hash "0f89771ifn5km6waa6ysap8wj8v1cgwgc268zln7vlgk2hdbcvix")))

(define-public crate-phf_codegen-0.7 (crate (name "phf_codegen") (vers "0.7.5") (deps (list (crate-dep (name "phf_generator") (req "= 0.7.5") (default-features #t) (kind 0)) (crate-dep (name "phf_shared") (req "= 0.7.5") (default-features #t) (kind 0)))) (hash "014pvs43mnglhx5cqq54xxnacx5hspljrq0l93ps2nyjspm4jk0n")))

(define-public crate-phf_codegen-0.7 (crate (name "phf_codegen") (vers "0.7.6") (deps (list (crate-dep (name "phf_generator") (req "= 0.7.6") (default-features #t) (kind 0)) (crate-dep (name "phf_shared") (req "= 0.7.6") (default-features #t) (kind 0)))) (hash "10ywi1fd4vigz2i6bvsakz42r2wnnlgr27fk4vfd4ks3f92fv656")))

(define-public crate-phf_codegen-0.7 (crate (name "phf_codegen") (vers "0.7.7") (deps (list (crate-dep (name "phf_generator") (req "= 0.7.7") (default-features #t) (kind 0)) (crate-dep (name "phf_shared") (req "= 0.7.7") (default-features #t) (kind 0)))) (hash "190fsl7mm1lv461clllpzr07ak3yrgacgl4nnid6bx6z22klb4jv")))

(define-public crate-phf_codegen-0.7 (crate (name "phf_codegen") (vers "0.7.8") (deps (list (crate-dep (name "phf_generator") (req "= 0.7.8") (default-features #t) (kind 0)) (crate-dep (name "phf_shared") (req "= 0.7.8") (default-features #t) (kind 0)))) (hash "03chv6qjj7zrzc5dlf80hby8wjrh4jmid5b86mbx52lv184nhqyr")))

(define-public crate-phf_codegen-0.7 (crate (name "phf_codegen") (vers "0.7.9") (deps (list (crate-dep (name "phf_generator") (req "= 0.7.9") (default-features #t) (kind 0)) (crate-dep (name "phf_shared") (req "= 0.7.9") (default-features #t) (kind 0)))) (hash "1ip88vyfzafi7chqzwbaw4ad0m0fpzxgvv830y0djj6ca398x61j")))

(define-public crate-phf_codegen-0.7 (crate (name "phf_codegen") (vers "0.7.10") (deps (list (crate-dep (name "phf_generator") (req "= 0.7.10") (default-features #t) (kind 0)) (crate-dep (name "phf_shared") (req "= 0.7.10") (default-features #t) (kind 0)))) (hash "1hj22n4fijz5karr9zc383b1wb19c4fn3928pz1y8gip9fngpgsp")))

(define-public crate-phf_codegen-0.7 (crate (name "phf_codegen") (vers "0.7.11") (deps (list (crate-dep (name "phf_generator") (req "= 0.7.11") (default-features #t) (kind 0)) (crate-dep (name "phf_shared") (req "= 0.7.11") (default-features #t) (kind 0)))) (hash "0j5lrgg2mfvpg12lss6jv7gz9s0vgfs9s6518gs5i4ffwggswmwf")))

(define-public crate-phf_codegen-0.7 (crate (name "phf_codegen") (vers "0.7.12") (deps (list (crate-dep (name "phf_generator") (req "= 0.7.12") (default-features #t) (kind 0)) (crate-dep (name "phf_shared") (req "= 0.7.12") (default-features #t) (kind 0)))) (hash "08653qq0mkm5fkm366c7g8wkl308p4ccp6m1g5i7nsj9rr976ngs")))

(define-public crate-phf_codegen-0.7 (crate (name "phf_codegen") (vers "0.7.13") (deps (list (crate-dep (name "phf_generator") (req "= 0.7.13") (default-features #t) (kind 0)) (crate-dep (name "phf_shared") (req "= 0.7.13") (default-features #t) (kind 0)))) (hash "1bv12fc315ldiyiciv17b63v81iwgfkyhwfc2cpjmnhl4dx72rv9")))

(define-public crate-phf_codegen-0.7 (crate (name "phf_codegen") (vers "0.7.14") (deps (list (crate-dep (name "phf_generator") (req "= 0.7.14") (default-features #t) (kind 0)) (crate-dep (name "phf_shared") (req "= 0.7.14") (default-features #t) (kind 0)))) (hash "07ghhaxi5dx3w9bnxgjafh3k97pnl705q7j95clh59bm7xyaxxwa")))

(define-public crate-phf_codegen-0.7 (crate (name "phf_codegen") (vers "0.7.15") (deps (list (crate-dep (name "phf_generator") (req "= 0.7.15") (default-features #t) (kind 0)) (crate-dep (name "phf_shared") (req "= 0.7.15") (default-features #t) (kind 0)))) (hash "0dsladnfvxiw5vi5rp55izkfgj0m560690p7xkpvri68y6307im5")))

(define-public crate-phf_codegen-0.7 (crate (name "phf_codegen") (vers "0.7.16") (deps (list (crate-dep (name "phf_generator") (req "= 0.7.16") (default-features #t) (kind 0)) (crate-dep (name "phf_shared") (req "= 0.7.16") (default-features #t) (kind 0)))) (hash "02ymivdk8aww0lgwd71wgfrplpj4kapw13iqf2pfi8hfrk11528a")))

(define-public crate-phf_codegen-0.7 (crate (name "phf_codegen") (vers "0.7.17") (deps (list (crate-dep (name "phf_generator") (req "= 0.7.17") (default-features #t) (kind 0)) (crate-dep (name "phf_shared") (req "= 0.7.17") (default-features #t) (kind 0)))) (hash "0hm2avcxh8lq61n5ridlkxl9n3akhybvprs2y6zljbbr2446ffsn")))

(define-public crate-phf_codegen-0.7 (crate (name "phf_codegen") (vers "0.7.18") (deps (list (crate-dep (name "phf_generator") (req "= 0.7.18") (default-features #t) (kind 0)) (crate-dep (name "phf_shared") (req "= 0.7.18") (default-features #t) (kind 0)))) (hash "0wbzxrx04mxx7ck2jvszdry7cf5dh4i0asbxq4y4kpdi36hpzyi9")))

(define-public crate-phf_codegen-0.7 (crate (name "phf_codegen") (vers "0.7.19") (deps (list (crate-dep (name "phf_generator") (req "= 0.7.19") (default-features #t) (kind 0)) (crate-dep (name "phf_shared") (req "= 0.7.19") (default-features #t) (kind 0)))) (hash "0850l8gs6rqa75nikdr9gig3jbda9nb1wp41xzdnlpzal1p50x4b")))

(define-public crate-phf_codegen-0.7 (crate (name "phf_codegen") (vers "0.7.20") (deps (list (crate-dep (name "phf_generator") (req "= 0.7.20") (default-features #t) (kind 0)) (crate-dep (name "phf_shared") (req "= 0.7.20") (default-features #t) (kind 0)))) (hash "0z1gnskdvrl2gm5lpssfhxirvrz0m09q6pd6f8hqy4lspwhz2qvb")))

(define-public crate-phf_codegen-0.7 (crate (name "phf_codegen") (vers "0.7.21") (deps (list (crate-dep (name "phf_generator") (req "= 0.7.21") (default-features #t) (kind 0)) (crate-dep (name "phf_shared") (req "= 0.7.21") (default-features #t) (kind 0)))) (hash "13z87h15zva2prc02bmgklq90zhp70h50p8p6gv69i2lpg0989fn")))

(define-public crate-phf_codegen-0.7 (crate (name "phf_codegen") (vers "0.7.22") (deps (list (crate-dep (name "phf_generator") (req "= 0.7.22") (default-features #t) (kind 0)) (crate-dep (name "phf_shared") (req "= 0.7.22") (default-features #t) (kind 0)))) (hash "0asgfczsd8vdhiwqhmf76fdnw4j9660d7mpc4y0np86pgpz4hh2f")))

(define-public crate-phf_codegen-0.7 (crate (name "phf_codegen") (vers "0.7.23") (deps (list (crate-dep (name "phf_generator") (req "= 0.7.23") (default-features #t) (kind 0)) (crate-dep (name "phf_shared") (req "= 0.7.23") (default-features #t) (kind 0)))) (hash "1b85wq4r117ksbf2kcxfca8l8fklh7qnr3w9v2yazmcqrl07y63x")))

(define-public crate-phf_codegen-0.7 (crate (name "phf_codegen") (vers "0.7.24") (deps (list (crate-dep (name "phf_generator") (req "^0.7.24") (default-features #t) (kind 0)) (crate-dep (name "phf_shared") (req "^0.7.24") (default-features #t) (kind 0)))) (hash "0zjiblicfm0nrmr2xxrs6pnf6zz2394wgch6dcbd8jijkq98agmh")))

(define-public crate-phf_codegen-0.8 (crate (name "phf_codegen") (vers "0.8.0") (deps (list (crate-dep (name "phf_generator") (req "^0.8.0") (default-features #t) (kind 0)) (crate-dep (name "phf_shared") (req "^0.8.0") (default-features #t) (kind 0)))) (hash "05d8w7aqqjb6039pfm6404gk5dlwrrf97kiy1n21212vb1hyxzyb")))

(define-public crate-phf_codegen-0.9 (crate (name "phf_codegen") (vers "0.9.0") (deps (list (crate-dep (name "phf_generator") (req "^0.9.0") (default-features #t) (kind 0)) (crate-dep (name "phf_shared") (req "^0.9.0") (default-features #t) (kind 0)))) (hash "00q5chz1v9idglrzk5qbijjyr80sgibpbkq1skgnbvi2rw8xnfln")))

(define-public crate-phf_codegen-0.9 (crate (name "phf_codegen") (vers "0.9.1") (deps (list (crate-dep (name "phf_generator") (req "^0.9.1") (default-features #t) (kind 0)) (crate-dep (name "phf_shared") (req "^0.9.0") (default-features #t) (kind 0)))) (hash "1q4r7mqisvzjz5fzfnr16nb5bxrj6xv32qnq1ds75xql783md31z") (yanked #t)))

(define-public crate-phf_codegen-0.10 (crate (name "phf_codegen") (vers "0.10.0") (deps (list (crate-dep (name "phf_generator") (req "^0.10.0") (default-features #t) (kind 0)) (crate-dep (name "phf_shared") (req "^0.10.0") (default-features #t) (kind 0)))) (hash "1k8kdad9wk2d5972k6jmjki2xpdy2ky4zd19rv7ybm2dpjlc7cag")))

(define-public crate-phf_codegen-0.11 (crate (name "phf_codegen") (vers "0.11.0") (deps (list (crate-dep (name "phf_generator") (req "^0.11.0") (default-features #t) (kind 0)) (crate-dep (name "phf_shared") (req "^0.11.0") (default-features #t) (kind 0)))) (hash "0n7f584spriq7dsb0dzhia18fffqry8259i454mlkdm1sx1hrfij") (rust-version "1.60")))

(define-public crate-phf_codegen-0.11 (crate (name "phf_codegen") (vers "0.11.1") (deps (list (crate-dep (name "phf_generator") (req "^0.11.0") (default-features #t) (kind 0)) (crate-dep (name "phf_shared") (req "^0.11.0") (default-features #t) (kind 0)))) (hash "0w274bcqbz499vpvd7isb252bc5mxmj9kagapn5mkjp3qn8chsm5") (rust-version "1.60")))

(define-public crate-phf_codegen-0.11 (crate (name "phf_codegen") (vers "0.11.2") (deps (list (crate-dep (name "phf_generator") (req "^0.11.0") (default-features #t) (kind 0)) (crate-dep (name "phf_shared") (req "^0.11.0") (default-features #t) (kind 0)))) (hash "0nia6h4qfwaypvfch3pnq1nd2qj64dif4a6kai3b7rjrsf49dlz8") (rust-version "1.60")))

(define-public crate-phf_generator-0.6 (crate (name "phf_generator") (vers "0.6.10") (deps (list (crate-dep (name "phf_shared") (req "= 0.6.10") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.1") (default-features #t) (kind 0)))) (hash "1b9695wdbpq3jb8na6vj3z4pd0v07m2jmzc5dz8ydmqkzk0cq6zx")))

(define-public crate-phf_generator-0.6 (crate (name "phf_generator") (vers "0.6.11") (deps (list (crate-dep (name "phf_shared") (req "= 0.6.11") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.1") (default-features #t) (kind 0)))) (hash "08y5qs4wjkij5hxmnd0yfmlpspwil9bi58s3qw2ssvy9k5999h8g")))

(define-public crate-phf_generator-0.6 (crate (name "phf_generator") (vers "0.6.12") (deps (list (crate-dep (name "phf_shared") (req "= 0.6.12") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.1") (default-features #t) (kind 0)))) (hash "1q2m7mahbjh1rzjq0vp6gnd60lpgpvcdh46s2xigz12xvb63sl1y")))

(define-public crate-phf_generator-0.6 (crate (name "phf_generator") (vers "0.6.13") (deps (list (crate-dep (name "phf_shared") (req "= 0.6.13") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.1") (default-features #t) (kind 0)))) (hash "0sql7bpw50xcyzmqmrrqbn5hlfqzksr0mn40z3szjq8j5d3wswxv")))

(define-public crate-phf_generator-0.6 (crate (name "phf_generator") (vers "0.6.14") (deps (list (crate-dep (name "phf_shared") (req "= 0.6.14") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.1") (default-features #t) (kind 0)))) (hash "1givzyhdf7b53h7cw6vhv315rn3z7synwkh71ylxy8vg024q6070")))

(define-public crate-phf_generator-0.6 (crate (name "phf_generator") (vers "0.6.15") (deps (list (crate-dep (name "phf_shared") (req "= 0.6.15") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.2") (default-features #t) (kind 0)))) (hash "1qdf0vapnz11yg5ps5p84x1sik61m7gc9bb2galiw8q88gp2mhv7")))

(define-public crate-phf_generator-0.6 (crate (name "phf_generator") (vers "0.6.16") (deps (list (crate-dep (name "phf_shared") (req "= 0.6.15") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.3") (default-features #t) (kind 0)))) (hash "1ngc1pk4sw2s1am41rqyn13jyrpxm0ifcw7x7l0lwam1mc0j0h7h") (yanked #t)))

(define-public crate-phf_generator-0.6 (crate (name "phf_generator") (vers "0.6.17") (deps (list (crate-dep (name "phf_shared") (req "= 0.6.17") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.3") (default-features #t) (kind 0)))) (hash "0dvcnwb2li21hzxi9s7gzx7z6z93z3ifzj3w93whx64l0m491qkp")))

(define-public crate-phf_generator-0.6 (crate (name "phf_generator") (vers "0.6.18") (deps (list (crate-dep (name "phf_shared") (req "= 0.6.18") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.3") (default-features #t) (kind 0)))) (hash "08mhchrag737yw1janm1hwm43jgn3yzwqi4hw49a1bkfd7xqaf47")))

(define-public crate-phf_generator-0.6 (crate (name "phf_generator") (vers "0.6.19") (deps (list (crate-dep (name "phf_shared") (req "= 0.6.19") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.3") (default-features #t) (kind 0)))) (hash "178ij3gd8ifawqvhr2b8m2576isz6mwgd7gwihjd3v0inr4aqxkx")))

(define-public crate-phf_generator-0.7 (crate (name "phf_generator") (vers "0.7.0") (deps (list (crate-dep (name "phf_shared") (req "= 0.7.0") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.3") (default-features #t) (kind 0)))) (hash "1vglmkfyp1xw5x29s4nblqyqammqj7zgqx67x315wlw8rbq565jw")))

(define-public crate-phf_generator-0.7 (crate (name "phf_generator") (vers "0.7.1") (deps (list (crate-dep (name "phf_shared") (req "= 0.7.1") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.3") (default-features #t) (kind 0)))) (hash "17p693zxypdck2zp3m5m69bi2hnpjzrbb049gz6la4nr916m4gzv")))

(define-public crate-phf_generator-0.7 (crate (name "phf_generator") (vers "0.7.2") (deps (list (crate-dep (name "phf_shared") (req "= 0.7.2") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.3") (default-features #t) (kind 0)))) (hash "0gsgm7w981wvdzbxll0a4f3knyjwj1wx72b79kr0n4dxqifpr0kk")))

(define-public crate-phf_generator-0.7 (crate (name "phf_generator") (vers "0.7.3") (deps (list (crate-dep (name "phf_shared") (req "= 0.7.3") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.3") (default-features #t) (kind 0)))) (hash "1x8xk6vfmlzahyzhaq0mfz2d2k1ikdvnlyxk24k77lwmbwy3ir28")))

(define-public crate-phf_generator-0.7 (crate (name "phf_generator") (vers "0.7.4") (deps (list (crate-dep (name "phf_shared") (req "= 0.7.4") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.3") (default-features #t) (kind 0)))) (hash "1yz4nnx88ajipsh599v8qxgyy6nr8p8bp51lgx5aj09gqvkslp9n")))

(define-public crate-phf_generator-0.7 (crate (name "phf_generator") (vers "0.7.5") (deps (list (crate-dep (name "phf_shared") (req "= 0.7.5") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.3") (default-features #t) (kind 0)))) (hash "1i1q56cqrhsplkps5vw7l8hkmys5z1hsy8kssz546jv892q5rhy5")))

(define-public crate-phf_generator-0.7 (crate (name "phf_generator") (vers "0.7.6") (deps (list (crate-dep (name "phf_shared") (req "= 0.7.6") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.3") (default-features #t) (kind 0)))) (hash "0v42yqppxykh0gyaaqpafkpbch9vpsjkka975084r15jy9acmdbd")))

(define-public crate-phf_generator-0.7 (crate (name "phf_generator") (vers "0.7.7") (deps (list (crate-dep (name "phf_shared") (req "= 0.7.7") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.3") (default-features #t) (kind 0)))) (hash "1pidg0srlc131i3iziwkgl80smjgra75zcmfcq2a863iry77cl7c")))

(define-public crate-phf_generator-0.7 (crate (name "phf_generator") (vers "0.7.8") (deps (list (crate-dep (name "phf_shared") (req "= 0.7.8") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.3") (default-features #t) (kind 0)))) (hash "036hghhjq8i48rwj3gyrnp6znk81d16wrv85d32a5ns8bzgljpaa")))

(define-public crate-phf_generator-0.7 (crate (name "phf_generator") (vers "0.7.9") (deps (list (crate-dep (name "phf_shared") (req "= 0.7.9") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.3") (default-features #t) (kind 0)))) (hash "0hcmp7s0m6nvm1f4zpxwznh34fq5vri934w19njl442zm7gpcd0j")))

(define-public crate-phf_generator-0.7 (crate (name "phf_generator") (vers "0.7.10") (deps (list (crate-dep (name "phf_shared") (req "= 0.7.10") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.3") (default-features #t) (kind 0)))) (hash "14m1lk2vqkwphl6x7vqph84yv22kdg8nsxrr1k0alzfh9m54a1j9")))

(define-public crate-phf_generator-0.7 (crate (name "phf_generator") (vers "0.7.11") (deps (list (crate-dep (name "phf_shared") (req "= 0.7.11") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.3") (default-features #t) (kind 0)))) (hash "0g9nv1wj7z58z77w4d46n3w2g6c2akfpxh32i5d9qg434x9rars5")))

(define-public crate-phf_generator-0.7 (crate (name "phf_generator") (vers "0.7.12") (deps (list (crate-dep (name "phf_shared") (req "= 0.7.12") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.3") (default-features #t) (kind 0)))) (hash "1gwf0r7hsv6xhmg9l04rzj26rxj06x5divkq0i70pvyyx0210faz")))

(define-public crate-phf_generator-0.7 (crate (name "phf_generator") (vers "0.7.13") (deps (list (crate-dep (name "phf_shared") (req "= 0.7.13") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.3") (default-features #t) (kind 0)))) (hash "1hq9miimf5lrbzfjwk8vgygvyqly3kz4sbchfx808041mj9mi9by")))

(define-public crate-phf_generator-0.7 (crate (name "phf_generator") (vers "0.7.14") (deps (list (crate-dep (name "phf_shared") (req "= 0.7.14") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.3") (default-features #t) (kind 0)))) (hash "1hqwvjvxrvrc8rwlm8csd1dhb0pm9j4pqshhfj60r04rzl45c06v")))

(define-public crate-phf_generator-0.7 (crate (name "phf_generator") (vers "0.7.15") (deps (list (crate-dep (name "phf_shared") (req "= 0.7.15") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.3") (default-features #t) (kind 0)))) (hash "0jhjlppz9pcwm2r6migwhfxkpv7cmpxrxrnm645c9mqprjw2rg5i")))

(define-public crate-phf_generator-0.7 (crate (name "phf_generator") (vers "0.7.16") (deps (list (crate-dep (name "phf_shared") (req "= 0.7.16") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.3") (default-features #t) (kind 0)))) (hash "1i5pxh05n4lva0js1v8s49db80r0kc93n4yri7ynxjr8bs1fmd84")))

(define-public crate-phf_generator-0.7 (crate (name "phf_generator") (vers "0.7.17") (deps (list (crate-dep (name "phf_shared") (req "= 0.7.17") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.3") (default-features #t) (kind 0)))) (hash "0hlh0a9faazdmsch95wh9x74rxhqbqdx83wn7d22cdf8lqq6hh91")))

(define-public crate-phf_generator-0.7 (crate (name "phf_generator") (vers "0.7.18") (deps (list (crate-dep (name "phf_shared") (req "= 0.7.18") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.3") (default-features #t) (kind 0)))) (hash "13ggnmxxh5vzcyk8k72b60shdpblkg47m1yljn96yvmbj3fpnjg3")))

(define-public crate-phf_generator-0.7 (crate (name "phf_generator") (vers "0.7.19") (deps (list (crate-dep (name "phf_shared") (req "= 0.7.19") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.3") (default-features #t) (kind 0)))) (hash "123wsp1sqwaxszbq0a172icawl5qp5p9wf7a4nq0kinz4jrd9qfm")))

(define-public crate-phf_generator-0.7 (crate (name "phf_generator") (vers "0.7.20") (deps (list (crate-dep (name "phf_shared") (req "= 0.7.20") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.3") (default-features #t) (kind 0)))) (hash "1c3y1infxwjf2gc94ylway0jpdwp1j1vdmsx7h4glnppf1wvvzsh")))

(define-public crate-phf_generator-0.7 (crate (name "phf_generator") (vers "0.7.21") (deps (list (crate-dep (name "phf_shared") (req "= 0.7.21") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.3") (default-features #t) (kind 0)))) (hash "00zcfp9c507y7hp65ygwbgpy9ngmkd36an64mziqbk1cag6gy1vb")))

(define-public crate-phf_generator-0.7 (crate (name "phf_generator") (vers "0.7.22") (deps (list (crate-dep (name "phf_shared") (req "= 0.7.22") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.4") (default-features #t) (kind 0)))) (hash "166rxc3sbwg2j9vdn9r8rni5dzayq2vcncfb456nfyrf0pfpk805")))

(define-public crate-phf_generator-0.7 (crate (name "phf_generator") (vers "0.7.23") (deps (list (crate-dep (name "phf_shared") (req "= 0.7.23") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.5") (default-features #t) (kind 0)))) (hash "0jqcxv5d9j5f8m56pzlyq6p837avg6dm9mih2gfb024vxcgikp03")))

(define-public crate-phf_generator-0.7 (crate (name "phf_generator") (vers "0.7.24") (deps (list (crate-dep (name "phf_shared") (req "^0.7.24") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.6") (default-features #t) (kind 0)))) (hash "0qi62gxk3x3whrmw5c4i71406icqk11qmpgln438p6qm7k4lqdh9")))

(define-public crate-phf_generator-0.8 (crate (name "phf_generator") (vers "0.8.0") (deps (list (crate-dep (name "criterion") (req "^0.3") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "criterion") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "phf_shared") (req "^0.8.0") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.7") (features (quote ("small_rng"))) (default-features #t) (kind 0)))) (hash "09i5338d1kixq6a60fcayz6awgxjlxcfw9ic5f02abbgr067ydhp")))

(define-public crate-phf_generator-0.9 (crate (name "phf_generator") (vers "0.9.0") (deps (list (crate-dep (name "criterion") (req "^0.3") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "criterion") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "phf_shared") (req "^0.9.0") (kind 0)) (crate-dep (name "rand") (req "^0.8") (features (quote ("small_rng"))) (default-features #t) (kind 0)))) (hash "03igl7ly34wqr1sap302d4wwalxz6y0n1fzh73arffhgv9x47h8g")))

(define-public crate-phf_generator-0.9 (crate (name "phf_generator") (vers "0.9.1") (deps (list (crate-dep (name "criterion") (req "=0.3.4") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "criterion") (req "=0.3.4") (default-features #t) (kind 2)) (crate-dep (name "phf_shared") (req "^0.9.0") (kind 0)) (crate-dep (name "rand") (req "^0.8") (features (quote ("small_rng"))) (default-features #t) (kind 0)))) (hash "10ih96kaxnkn1yxk3ghpzgm09nc0rn69fd52kv68003fv4h34gyl")))

(define-public crate-phf_generator-0.10 (crate (name "phf_generator") (vers "0.10.0") (deps (list (crate-dep (name "criterion") (req "=0.3.4") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "criterion") (req "=0.3.4") (default-features #t) (kind 2)) (crate-dep (name "phf_shared") (req "^0.10.0") (kind 0)) (crate-dep (name "rand") (req "^0.8") (features (quote ("small_rng"))) (default-features #t) (kind 0)))) (hash "1mlq6hlajsvlsx6rhw49g9ricsm017lrxmgmmbk85sxm7f4qaljx")))

(define-public crate-phf_generator-0.11 (crate (name "phf_generator") (vers "0.11.0") (deps (list (crate-dep (name "criterion") (req "=0.3.4") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "criterion") (req "=0.3.4") (default-features #t) (kind 2)) (crate-dep (name "phf_shared") (req "^0.11.0") (kind 0)) (crate-dep (name "rand") (req "^0.8") (features (quote ("small_rng"))) (default-features #t) (kind 0)))) (hash "01b75z0wm5vjagw8ccd8z6j3gwv56yyi8n0rpkxgnp7pnqh0fiav") (rust-version "1.60")))

(define-public crate-phf_generator-0.11 (crate (name "phf_generator") (vers "0.11.1") (deps (list (crate-dep (name "criterion") (req "^0.3.6") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "criterion") (req "^0.3.6") (default-features #t) (kind 2)) (crate-dep (name "phf_shared") (req "^0.11.1") (kind 0)) (crate-dep (name "rand") (req "^0.8") (features (quote ("small_rng"))) (kind 0)))) (hash "1gsgy5k45y937qnwp58dc05d63lwlfm3imqr1zslb8qgb2a1q65i") (rust-version "1.60")))

(define-public crate-phf_generator-0.11 (crate (name "phf_generator") (vers "0.11.2") (deps (list (crate-dep (name "criterion") (req "^0.3.6") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "criterion") (req "^0.3.6") (default-features #t) (kind 2)) (crate-dep (name "phf_shared") (req "^0.11.2") (kind 0)) (crate-dep (name "rand") (req "^0.8") (features (quote ("small_rng"))) (kind 0)))) (hash "1c14pjyxbcpwkdgw109f7581cc5fa3fnkzdq1ikvx7mdq9jcrr28") (rust-version "1.60")))

(define-public crate-phf_mac-0.0.1 (crate (name "phf_mac") (vers "0.0.1") (deps (list (crate-dep (name "phf_shared") (req "^0.0.1") (default-features #t) (kind 0)) (crate-dep (name "time") (req "*") (default-features #t) (kind 0)))) (hash "1r6jvcqs2f5rkz7wm1c799h8q06vk8qc11z31q1srqyasd6p8fxx")))

(define-public crate-phf_mac-0.1 (crate (name "phf_mac") (vers "0.1.0") (deps (list (crate-dep (name "phf_shared") (req "= 0.1.0") (default-features #t) (kind 0)) (crate-dep (name "time") (req "*") (default-features #t) (kind 0)))) (hash "0ds67dk0bibwj60k57hqbbfd413qd7mx5l6w2x2fpyap2cknbffg")))

(define-public crate-phf_mac-0.2 (crate (name "phf_mac") (vers "0.2.0") (deps (list (crate-dep (name "phf_shared") (req "= 0.2.0") (default-features #t) (kind 0)) (crate-dep (name "time") (req "^0.1") (default-features #t) (kind 0)))) (hash "0lg3q9n51lv80qac65p4240jiph45vn3jcjjpk2rsi5ljjndv17y")))

(define-public crate-phf_mac-0.3 (crate (name "phf_mac") (vers "0.3.0") (deps (list (crate-dep (name "phf_shared") (req "= 0.3.0") (default-features #t) (kind 0)) (crate-dep (name "time") (req "^0.1") (default-features #t) (kind 0)))) (hash "1hawawk9fs7d8cdsk8nbafsrw9wjblw578jvrj0bf1vyglm7abc9")))

(define-public crate-phf_mac-0.4 (crate (name "phf_mac") (vers "0.4.0") (deps (list (crate-dep (name "phf_shared") (req "= 0.4.0") (default-features #t) (kind 0)) (crate-dep (name "time") (req "^0.1") (default-features #t) (kind 0)))) (hash "03iyw9fg92fyb9n4wf9jw33cfiwj0x3h4ibrcal080gbp94grszx")))

(define-public crate-phf_mac-0.4 (crate (name "phf_mac") (vers "0.4.1") (deps (list (crate-dep (name "phf_shared") (req "= 0.4.1") (default-features #t) (kind 0)) (crate-dep (name "time") (req "^0.1") (default-features #t) (kind 0)))) (hash "1i99l9bsqvzswk7bki75kyx7lnxfxb82vly9fsf3k2da69274n3w")))

(define-public crate-phf_mac-0.4 (crate (name "phf_mac") (vers "0.4.2") (deps (list (crate-dep (name "phf_shared") (req "= 0.4.2") (default-features #t) (kind 0)) (crate-dep (name "time") (req "^0.1") (default-features #t) (kind 0)))) (hash "00i7rjh7grixcacjk6wm0vay9a8r9a7n6phglqrvbf78nzvnmzjx")))

(define-public crate-phf_mac-0.4 (crate (name "phf_mac") (vers "0.4.3") (deps (list (crate-dep (name "phf_shared") (req "= 0.4.3") (default-features #t) (kind 0)) (crate-dep (name "time") (req "^0.1") (default-features #t) (kind 0)))) (hash "1y7dpmdlak94l78klvi2zz68xahc1czmfwa13c1rnaxx8g5z8g6x")))

(define-public crate-phf_mac-0.4 (crate (name "phf_mac") (vers "0.4.4") (deps (list (crate-dep (name "phf_shared") (req "= 0.4.4") (default-features #t) (kind 0)) (crate-dep (name "time") (req "^0.1") (default-features #t) (kind 0)))) (hash "1ga48zkpk7day0f4b6ygils6mlfn5v0lx6ykzpn07r716njn8dw9")))

(define-public crate-phf_mac-0.4 (crate (name "phf_mac") (vers "0.4.5") (deps (list (crate-dep (name "phf_shared") (req "= 0.4.5") (default-features #t) (kind 0)) (crate-dep (name "time") (req "^0.1") (default-features #t) (kind 0)))) (hash "01ma7li31rbn6a8ym8hzs50667f2r9356kxfiz7pxalp6n6k0s2a")))

(define-public crate-phf_mac-0.4 (crate (name "phf_mac") (vers "0.4.6") (deps (list (crate-dep (name "phf_shared") (req "= 0.4.6") (default-features #t) (kind 0)) (crate-dep (name "time") (req "^0.1") (default-features #t) (kind 0)))) (hash "1yjfgnzklcfwf6s615pw2c90jx8aaklhdczbkwk8y7cfaf182fyp")))

(define-public crate-phf_mac-0.4 (crate (name "phf_mac") (vers "0.4.8") (deps (list (crate-dep (name "phf_shared") (req "= 0.4.8") (default-features #t) (kind 0)) (crate-dep (name "time") (req "^0.1") (default-features #t) (kind 0)))) (hash "0ylsxni2i6wayxwka212im5afl834sswmxia286jm5l8ip8wfkc9")))

(define-public crate-phf_mac-0.4 (crate (name "phf_mac") (vers "0.4.9") (deps (list (crate-dep (name "phf_shared") (req "= 0.4.9") (default-features #t) (kind 0)) (crate-dep (name "time") (req "^0.1") (optional #t) (default-features #t) (kind 0)))) (hash "0aqsa0d7cgr2wgwx7xkbs5rfrknyhjb51ir3r79c2m7bwzpsscnl") (features (quote (("stats" "time"))))))

(define-public crate-phf_mac-0.5 (crate (name "phf_mac") (vers "0.5.0") (deps (list (crate-dep (name "phf_shared") (req "= 0.5.0") (default-features #t) (kind 0)) (crate-dep (name "time") (req "^0.1") (optional #t) (default-features #t) (kind 0)))) (hash "035j8q5l340d5rpqk0pspwbz5003fs86mf90ghh57r4f3zj5203w") (features (quote (("stats" "time"))))))

(define-public crate-phf_macros-0.6 (crate (name "phf_macros") (vers "0.6.0") (deps (list (crate-dep (name "phf_shared") (req "= 0.6.0") (default-features #t) (kind 0)) (crate-dep (name "time") (req "^0.1") (optional #t) (default-features #t) (kind 0)))) (hash "1cz6rnpxjb34dvqrvkwwb8dppx1pbw6mn1wzcvybkb1m2l6y6ikk") (features (quote (("stats" "time"))))))

(define-public crate-phf_macros-0.6 (crate (name "phf_macros") (vers "0.6.1") (deps (list (crate-dep (name "phf_shared") (req "= 0.6.1") (default-features #t) (kind 0)) (crate-dep (name "time") (req "^0.1") (optional #t) (default-features #t) (kind 0)))) (hash "1vriyval2imwkd04p0abf5vrrvvrlkra5h1g9v79bh7p3iyqyh6i") (features (quote (("stats" "time"))))))

(define-public crate-phf_macros-0.6 (crate (name "phf_macros") (vers "0.6.2") (deps (list (crate-dep (name "phf_shared") (req "= 0.6.2") (default-features #t) (kind 0)) (crate-dep (name "time") (req "^0.1") (optional #t) (default-features #t) (kind 0)))) (hash "0g6npjj6hz9a0galpxkz6kfh6i7b2pnw95nh7p5sh4j6rdgfpdk8") (features (quote (("stats" "time"))))))

(define-public crate-phf_macros-0.6 (crate (name "phf_macros") (vers "0.6.3") (deps (list (crate-dep (name "phf_shared") (req "= 0.6.3") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "time") (req "^0.1") (optional #t) (default-features #t) (kind 0)))) (hash "1kdkcnzc9qwrm33yvmgnf1yvrxaaxipg4hzjk69mgsmz6mcp2k6w") (features (quote (("stats" "time"))))))

(define-public crate-phf_macros-0.6 (crate (name "phf_macros") (vers "0.6.4") (deps (list (crate-dep (name "phf_shared") (req "= 0.6.4") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "time") (req "^0.1") (optional #t) (default-features #t) (kind 0)))) (hash "0z35k5l76y442wh5225a4sfmw1h7v4jcwivhkfkf00gi2csrwcpf") (features (quote (("stats" "time"))))))

(define-public crate-phf_macros-0.6 (crate (name "phf_macros") (vers "0.6.5") (deps (list (crate-dep (name "phf_shared") (req "= 0.6.5") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "time") (req "^0.1") (optional #t) (default-features #t) (kind 0)))) (hash "1c2fyrqn4sv90a4qpbryvx5yqs0w1bfxk81css1qyzfmnadfpdrn") (features (quote (("stats" "time"))))))

(define-public crate-phf_macros-0.6 (crate (name "phf_macros") (vers "0.6.6") (deps (list (crate-dep (name "phf_shared") (req "= 0.6.6") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "time") (req "^0.1") (optional #t) (default-features #t) (kind 0)))) (hash "0pln4ajjz4h4qgfc3fm706547fwqvga0ilp42ylfdychafy1p2kg") (features (quote (("stats" "time"))))))

(define-public crate-phf_macros-0.6 (crate (name "phf_macros") (vers "0.6.7") (deps (list (crate-dep (name "phf_shared") (req "= 0.6.7") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "time") (req "^0.1") (optional #t) (default-features #t) (kind 0)))) (hash "01ryx6bxyqygipy971lb0vyzydz0iih77x3851b59g7hljxrr20y") (features (quote (("stats" "time"))))))

(define-public crate-phf_macros-0.6 (crate (name "phf_macros") (vers "0.6.8") (deps (list (crate-dep (name "phf_shared") (req "= 0.6.8") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "time") (req "^0.1") (optional #t) (default-features #t) (kind 0)))) (hash "1rigpbx34arlh8z64ac47ywwjrldfpdw0g5mqi7asb8a2giknpx5") (features (quote (("stats" "time"))))))

(define-public crate-phf_macros-0.6 (crate (name "phf_macros") (vers "0.6.9") (deps (list (crate-dep (name "phf_shared") (req "= 0.6.9") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "time") (req "^0.1") (optional #t) (default-features #t) (kind 0)))) (hash "1zm5lcwyjcwh6cxfqcjmk8q2sdmx6p9v4i37dvbi2ah7y0x5pknx") (features (quote (("stats" "time"))))))

(define-public crate-phf_macros-0.6 (crate (name "phf_macros") (vers "0.6.10") (deps (list (crate-dep (name "phf") (req "= 0.6.10") (default-features #t) (kind 2)) (crate-dep (name "phf_generator") (req "= 0.6.10") (default-features #t) (kind 0)) (crate-dep (name "phf_shared") (req "= 0.6.10") (default-features #t) (kind 0)) (crate-dep (name "time") (req "^0.1") (optional #t) (default-features #t) (kind 0)))) (hash "02nzchrw9vpsqvjz780vg1sv5cxslcpkbd6hbs09bc3nnl16i6d1") (features (quote (("stats" "time"))))))

(define-public crate-phf_macros-0.6 (crate (name "phf_macros") (vers "0.6.11") (deps (list (crate-dep (name "phf") (req "= 0.6.11") (default-features #t) (kind 2)) (crate-dep (name "phf_generator") (req "= 0.6.11") (default-features #t) (kind 0)) (crate-dep (name "phf_shared") (req "= 0.6.11") (default-features #t) (kind 0)) (crate-dep (name "time") (req "^0.1") (optional #t) (default-features #t) (kind 0)))) (hash "1lqjidhmyl29yz6ra4ic266718ffhpkrradqfyydqahw9rfc1ng0") (features (quote (("stats" "time"))))))

(define-public crate-phf_macros-0.6 (crate (name "phf_macros") (vers "0.6.12") (deps (list (crate-dep (name "phf") (req "= 0.6.12") (default-features #t) (kind 2)) (crate-dep (name "phf_generator") (req "= 0.6.12") (default-features #t) (kind 0)) (crate-dep (name "phf_shared") (req "= 0.6.12") (default-features #t) (kind 0)) (crate-dep (name "time") (req "^0.1") (optional #t) (default-features #t) (kind 0)))) (hash "02pybnbqvzhbqxik71rljqqxqj7znak1sjqq776p22gcsh7963na") (features (quote (("stats" "time"))))))

(define-public crate-phf_macros-0.6 (crate (name "phf_macros") (vers "0.6.13") (deps (list (crate-dep (name "phf") (req "= 0.6.13") (default-features #t) (kind 2)) (crate-dep (name "phf_generator") (req "= 0.6.13") (default-features #t) (kind 0)) (crate-dep (name "phf_shared") (req "= 0.6.13") (default-features #t) (kind 0)) (crate-dep (name "time") (req "^0.1") (optional #t) (default-features #t) (kind 0)))) (hash "0dxchs0mik7jg30bpyd9dz78i4hzwxa23khkhwq1zlpqn8dc0gzr") (features (quote (("stats" "time"))))))

(define-public crate-phf_macros-0.6 (crate (name "phf_macros") (vers "0.6.14") (deps (list (crate-dep (name "phf") (req "= 0.6.14") (default-features #t) (kind 2)) (crate-dep (name "phf_generator") (req "= 0.6.14") (default-features #t) (kind 0)) (crate-dep (name "phf_shared") (req "= 0.6.14") (default-features #t) (kind 0)) (crate-dep (name "time") (req "^0.1") (optional #t) (default-features #t) (kind 0)))) (hash "1jhddwfmfrmmdqsnrw1091h2c0p4c89frxnq803lzvn90345sfjb") (features (quote (("stats" "time"))))))

(define-public crate-phf_macros-0.6 (crate (name "phf_macros") (vers "0.6.15") (deps (list (crate-dep (name "phf") (req "= 0.6.15") (default-features #t) (kind 2)) (crate-dep (name "phf_generator") (req "= 0.6.15") (default-features #t) (kind 0)) (crate-dep (name "phf_shared") (req "= 0.6.15") (default-features #t) (kind 0)) (crate-dep (name "time") (req "^0.1") (optional #t) (default-features #t) (kind 0)))) (hash "11k6hf6amzr5ffax3v86x3nk62jbhivv1l1kcrmyagvx7l6w7rrk") (features (quote (("stats" "time"))))))

(define-public crate-phf_macros-0.6 (crate (name "phf_macros") (vers "0.6.17") (deps (list (crate-dep (name "phf") (req "= 0.6.17") (default-features #t) (kind 2)) (crate-dep (name "phf_generator") (req "= 0.6.17") (default-features #t) (kind 0)) (crate-dep (name "phf_shared") (req "= 0.6.17") (default-features #t) (kind 0)) (crate-dep (name "time") (req "^0.1") (optional #t) (default-features #t) (kind 0)))) (hash "0vpn7zlm292pj9a7cly21q5b0nqlx54vlcalbbvagkzl8bxxbw67") (features (quote (("stats" "time"))))))

(define-public crate-phf_macros-0.6 (crate (name "phf_macros") (vers "0.6.18") (deps (list (crate-dep (name "phf") (req "= 0.6.18") (default-features #t) (kind 2)) (crate-dep (name "phf_generator") (req "= 0.6.18") (default-features #t) (kind 0)) (crate-dep (name "phf_shared") (req "= 0.6.18") (default-features #t) (kind 0)) (crate-dep (name "time") (req "^0.1") (optional #t) (default-features #t) (kind 0)))) (hash "1bg55v6zdxqqp5v9r25xyn7236mn990phk8j69q3khc0pjg5q8s7") (features (quote (("stats" "time"))))))

(define-public crate-phf_macros-0.6 (crate (name "phf_macros") (vers "0.6.19") (deps (list (crate-dep (name "phf") (req "= 0.6.19") (default-features #t) (kind 2)) (crate-dep (name "phf_generator") (req "= 0.6.19") (default-features #t) (kind 0)) (crate-dep (name "phf_shared") (req "= 0.6.19") (default-features #t) (kind 0)) (crate-dep (name "time") (req "^0.1") (optional #t) (default-features #t) (kind 0)))) (hash "1ch2djc3ld6zlgj16dzbm9w0bdl3pvp7bvf695nfz2j6fy6fr4n1") (features (quote (("stats" "time"))))))

(define-public crate-phf_macros-0.7 (crate (name "phf_macros") (vers "0.7.0") (deps (list (crate-dep (name "phf") (req "= 0.7.0") (default-features #t) (kind 2)) (crate-dep (name "phf_generator") (req "= 0.7.0") (default-features #t) (kind 0)) (crate-dep (name "phf_shared") (req "= 0.7.0") (default-features #t) (kind 0)) (crate-dep (name "time") (req "^0.1") (optional #t) (default-features #t) (kind 0)))) (hash "13rkwdldnq41vqm2slascika7kj53za01fxv0hi79lgiwzzm34k8") (features (quote (("stats" "time"))))))

(define-public crate-phf_macros-0.7 (crate (name "phf_macros") (vers "0.7.1") (deps (list (crate-dep (name "phf") (req "= 0.7.1") (default-features #t) (kind 2)) (crate-dep (name "phf_generator") (req "= 0.7.1") (default-features #t) (kind 0)) (crate-dep (name "phf_shared") (req "= 0.7.1") (default-features #t) (kind 0)) (crate-dep (name "time") (req "^0.1") (optional #t) (default-features #t) (kind 0)))) (hash "1r2rpnbg4sh151rflgxp360h7zxcikjmw26m4p8gp4xsv7r6bkmh") (features (quote (("stats" "time"))))))

(define-public crate-phf_macros-0.7 (crate (name "phf_macros") (vers "0.7.2") (deps (list (crate-dep (name "phf") (req "= 0.7.2") (default-features #t) (kind 2)) (crate-dep (name "phf_generator") (req "= 0.7.2") (default-features #t) (kind 0)) (crate-dep (name "phf_shared") (req "= 0.7.2") (default-features #t) (kind 0)) (crate-dep (name "time") (req "^0.1") (optional #t) (default-features #t) (kind 0)))) (hash "1b8q8flac2b7z5fhqcqvlg12ldzipsp5jp0r9prh78ha4v3bzr8q") (features (quote (("stats" "time"))))))

(define-public crate-phf_macros-0.7 (crate (name "phf_macros") (vers "0.7.3") (deps (list (crate-dep (name "phf") (req "= 0.7.3") (default-features #t) (kind 2)) (crate-dep (name "phf_generator") (req "= 0.7.3") (default-features #t) (kind 0)) (crate-dep (name "phf_shared") (req "= 0.7.3") (default-features #t) (kind 0)) (crate-dep (name "time") (req "^0.1") (optional #t) (default-features #t) (kind 0)))) (hash "0v6mrg3fmfby1x4vbih6ivb80q96z0ilywz7i2cpvx87f5dggazc") (features (quote (("stats" "time"))))))

(define-public crate-phf_macros-0.7 (crate (name "phf_macros") (vers "0.7.4") (deps (list (crate-dep (name "phf") (req "= 0.7.4") (default-features #t) (kind 2)) (crate-dep (name "phf_generator") (req "= 0.7.4") (default-features #t) (kind 0)) (crate-dep (name "phf_shared") (req "= 0.7.4") (default-features #t) (kind 0)) (crate-dep (name "time") (req "^0.1") (optional #t) (default-features #t) (kind 0)))) (hash "10d1kvr31sk98vj9l9xxzzbryhhfdci7i75b3dxj59xaw4j0ndh1") (features (quote (("stats" "time"))))))

(define-public crate-phf_macros-0.7 (crate (name "phf_macros") (vers "0.7.5") (deps (list (crate-dep (name "phf") (req "= 0.7.5") (default-features #t) (kind 2)) (crate-dep (name "phf_generator") (req "= 0.7.5") (default-features #t) (kind 0)) (crate-dep (name "phf_shared") (req "= 0.7.5") (default-features #t) (kind 0)) (crate-dep (name "time") (req "^0.1") (optional #t) (default-features #t) (kind 0)))) (hash "0dy3ix0fmsvny58hnpi4cs371p57yc25p6fk3inwrb6vjyqyp55d") (features (quote (("stats" "time"))))))

(define-public crate-phf_macros-0.7 (crate (name "phf_macros") (vers "0.7.6") (deps (list (crate-dep (name "phf") (req "= 0.7.6") (default-features #t) (kind 2)) (crate-dep (name "phf_generator") (req "= 0.7.6") (default-features #t) (kind 0)) (crate-dep (name "phf_shared") (req "= 0.7.6") (default-features #t) (kind 0)) (crate-dep (name "time") (req "^0.1") (optional #t) (default-features #t) (kind 0)))) (hash "1pp4gac2jpziqlsdn2cd21b1rl3yzcpjrvvb9nza3p5vi9fz55i6") (features (quote (("stats" "time"))))))

(define-public crate-phf_macros-0.7 (crate (name "phf_macros") (vers "0.7.7") (deps (list (crate-dep (name "phf") (req "= 0.7.7") (default-features #t) (kind 2)) (crate-dep (name "phf_generator") (req "= 0.7.7") (default-features #t) (kind 0)) (crate-dep (name "phf_shared") (req "= 0.7.7") (default-features #t) (kind 0)) (crate-dep (name "time") (req "^0.1") (optional #t) (default-features #t) (kind 0)))) (hash "1n0vr9sfiqzg091r6rlgycib4p3952vjhqsgqqfid2qgkbyw6v94") (features (quote (("stats" "time"))))))

(define-public crate-phf_macros-0.7 (crate (name "phf_macros") (vers "0.7.8") (deps (list (crate-dep (name "phf") (req "= 0.7.8") (default-features #t) (kind 2)) (crate-dep (name "phf_generator") (req "= 0.7.8") (default-features #t) (kind 0)) (crate-dep (name "phf_shared") (req "= 0.7.8") (default-features #t) (kind 0)) (crate-dep (name "time") (req "^0.1") (optional #t) (default-features #t) (kind 0)))) (hash "0cvr3jm14f80lnq6lvbqwgb6f6k7j84k57ad8paxwg2fr24kflfl") (features (quote (("stats" "time"))))))

(define-public crate-phf_macros-0.7 (crate (name "phf_macros") (vers "0.7.9") (deps (list (crate-dep (name "phf") (req "= 0.7.9") (default-features #t) (kind 2)) (crate-dep (name "phf_generator") (req "= 0.7.9") (default-features #t) (kind 0)) (crate-dep (name "phf_shared") (req "= 0.7.9") (default-features #t) (kind 0)) (crate-dep (name "time") (req "^0.1") (optional #t) (default-features #t) (kind 0)))) (hash "1n8087767kbv6sdcjw51qpxq5mgnnmrsa01xiih5a3grylgdy9cs") (features (quote (("stats" "time"))))))

(define-public crate-phf_macros-0.7 (crate (name "phf_macros") (vers "0.7.10") (deps (list (crate-dep (name "phf") (req "= 0.7.10") (default-features #t) (kind 2)) (crate-dep (name "phf_generator") (req "= 0.7.10") (default-features #t) (kind 0)) (crate-dep (name "phf_shared") (req "= 0.7.10") (default-features #t) (kind 0)) (crate-dep (name "time") (req "^0.1") (optional #t) (default-features #t) (kind 0)))) (hash "0p14sxz7z8jsjh5qsdqk0hznbp35q74i1llvgqxgwmvpi8r51hnb") (features (quote (("stats" "time"))))))

(define-public crate-phf_macros-0.7 (crate (name "phf_macros") (vers "0.7.11") (deps (list (crate-dep (name "phf") (req "= 0.7.11") (default-features #t) (kind 2)) (crate-dep (name "phf_generator") (req "= 0.7.11") (default-features #t) (kind 0)) (crate-dep (name "phf_shared") (req "= 0.7.11") (default-features #t) (kind 0)) (crate-dep (name "time") (req "^0.1") (optional #t) (default-features #t) (kind 0)))) (hash "1hkh624a73d2sfcl07rxx5vbjxrw16g5cr86pm3n21bafgj1fv4a") (features (quote (("stats" "time"))))))

(define-public crate-phf_macros-0.7 (crate (name "phf_macros") (vers "0.7.12") (deps (list (crate-dep (name "phf") (req "= 0.7.12") (default-features #t) (kind 2)) (crate-dep (name "phf_generator") (req "= 0.7.12") (default-features #t) (kind 0)) (crate-dep (name "phf_shared") (req "= 0.7.12") (default-features #t) (kind 0)) (crate-dep (name "time") (req "^0.1") (optional #t) (default-features #t) (kind 0)))) (hash "1ripj0cshjad7f9y3yxvgc3nw7yfzacd3d0n0mgwmaq8y1v7byvg") (features (quote (("stats" "time"))))))

(define-public crate-phf_macros-0.7 (crate (name "phf_macros") (vers "0.7.13") (deps (list (crate-dep (name "phf") (req "= 0.7.13") (default-features #t) (kind 2)) (crate-dep (name "phf_generator") (req "= 0.7.13") (default-features #t) (kind 0)) (crate-dep (name "phf_shared") (req "= 0.7.13") (default-features #t) (kind 0)) (crate-dep (name "time") (req "^0.1") (optional #t) (default-features #t) (kind 0)))) (hash "194i4p1jbr0kbhnfrdpgqrml152kidlamcxba2ngy32bal9ma9pg") (features (quote (("stats" "time"))))))

(define-public crate-phf_macros-0.7 (crate (name "phf_macros") (vers "0.7.14") (deps (list (crate-dep (name "phf") (req "= 0.7.14") (default-features #t) (kind 2)) (crate-dep (name "phf_generator") (req "= 0.7.14") (default-features #t) (kind 0)) (crate-dep (name "phf_shared") (req "= 0.7.14") (default-features #t) (kind 0)) (crate-dep (name "time") (req "^0.1") (optional #t) (default-features #t) (kind 0)))) (hash "0c5hfrgv164kndr3d43dpfzy85jd68k6wg5i5wfj1426qhpwrx1y") (features (quote (("stats" "time"))))))

(define-public crate-phf_macros-0.7 (crate (name "phf_macros") (vers "0.7.15") (deps (list (crate-dep (name "phf") (req "= 0.7.15") (default-features #t) (kind 2)) (crate-dep (name "phf_generator") (req "= 0.7.15") (default-features #t) (kind 0)) (crate-dep (name "phf_shared") (req "= 0.7.15") (default-features #t) (kind 0)) (crate-dep (name "time") (req "^0.1") (optional #t) (default-features #t) (kind 0)))) (hash "0g8grvnw7wn1c3d9jlw5k50n757indi5bb8lnv2z4xp7jn9i73qy") (features (quote (("stats" "time"))))))

(define-public crate-phf_macros-0.7 (crate (name "phf_macros") (vers "0.7.16") (deps (list (crate-dep (name "phf") (req "= 0.7.16") (default-features #t) (kind 2)) (crate-dep (name "phf_generator") (req "= 0.7.16") (default-features #t) (kind 0)) (crate-dep (name "phf_shared") (req "= 0.7.16") (default-features #t) (kind 0)) (crate-dep (name "time") (req "^0.1") (optional #t) (default-features #t) (kind 0)))) (hash "0zp3zkx841wdkj5xjw862mgvv51a55if4m0fipcj0ib5xl7afbhk") (features (quote (("stats" "time"))))))

(define-public crate-phf_macros-0.7 (crate (name "phf_macros") (vers "0.7.17") (deps (list (crate-dep (name "compiletest_rs") (req "^0.2") (default-features #t) (kind 2)) (crate-dep (name "phf") (req "= 0.7.17") (default-features #t) (kind 2)) (crate-dep (name "phf_generator") (req "= 0.7.17") (default-features #t) (kind 0)) (crate-dep (name "phf_shared") (req "= 0.7.17") (default-features #t) (kind 0)) (crate-dep (name "time") (req "^0.1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "unicase") (req "^1.4") (optional #t) (default-features #t) (kind 0)))) (hash "0csrwrvx91g3b5k3w8al64hlv1xivvclf7k1cxd0lk0jz2hjxqqw") (features (quote (("unicase_support" "unicase" "phf_shared/unicase") ("stats" "time"))))))

(define-public crate-phf_macros-0.7 (crate (name "phf_macros") (vers "0.7.18") (deps (list (crate-dep (name "compiletest_rs") (req "^0.2") (default-features #t) (kind 2)) (crate-dep (name "phf") (req "= 0.7.18") (default-features #t) (kind 2)) (crate-dep (name "phf_generator") (req "= 0.7.18") (default-features #t) (kind 0)) (crate-dep (name "phf_shared") (req "= 0.7.18") (default-features #t) (kind 0)) (crate-dep (name "time") (req "^0.1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "unicase") (req "^1.4") (optional #t) (default-features #t) (kind 0)))) (hash "1apc1ryasg3qin0b6rymv940xk0lv1br9r60y53pdckpgnv1dzca") (features (quote (("unicase_support" "unicase" "phf_shared/unicase") ("stats" "time"))))))

(define-public crate-phf_macros-0.7 (crate (name "phf_macros") (vers "0.7.19") (deps (list (crate-dep (name "compiletest_rs") (req "^0.2") (default-features #t) (kind 2)) (crate-dep (name "phf") (req "= 0.7.19") (default-features #t) (kind 2)) (crate-dep (name "phf_generator") (req "= 0.7.19") (default-features #t) (kind 0)) (crate-dep (name "phf_shared") (req "= 0.7.19") (default-features #t) (kind 0)) (crate-dep (name "time") (req "^0.1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "unicase") (req "^1.4") (optional #t) (default-features #t) (kind 0)))) (hash "0n3klbp267yciv88nv59y1c1fw0k5h9rmvaqivgjhkalvh95kg2b") (features (quote (("unicase_support" "unicase" "phf_shared/unicase") ("stats" "time"))))))

(define-public crate-phf_macros-0.7 (crate (name "phf_macros") (vers "0.7.20") (deps (list (crate-dep (name "compiletest_rs") (req "^0.2") (default-features #t) (kind 2)) (crate-dep (name "phf") (req "= 0.7.20") (default-features #t) (kind 2)) (crate-dep (name "phf_generator") (req "= 0.7.20") (default-features #t) (kind 0)) (crate-dep (name "phf_shared") (req "= 0.7.20") (default-features #t) (kind 0)) (crate-dep (name "time") (req "^0.1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "unicase") (req "^1.4") (optional #t) (default-features #t) (kind 0)))) (hash "15z4ca3wz5pmav0ipxc7fhjw0kwj1dh7f0vnvrv37ih3hw92r6v1") (features (quote (("unicase_support" "unicase" "phf_shared/unicase") ("stats" "time"))))))

(define-public crate-phf_macros-0.7 (crate (name "phf_macros") (vers "0.7.21") (deps (list (crate-dep (name "compiletest_rs") (req "^0.2") (default-features #t) (kind 2)) (crate-dep (name "phf") (req "^0.7.21") (default-features #t) (kind 2)) (crate-dep (name "phf_generator") (req "= 0.7.21") (default-features #t) (kind 0)) (crate-dep (name "phf_shared") (req "= 0.7.21") (default-features #t) (kind 0)) (crate-dep (name "unicase") (req "^1.4") (optional #t) (default-features #t) (kind 0)))) (hash "0125fcyhczq27gvqfif05jnxhi58ijg9q294zgwcp29x5qvdmv80") (features (quote (("unicase_support" "unicase" "phf_shared/unicase") ("stats"))))))

(define-public crate-phf_macros-0.7 (crate (name "phf_macros") (vers "0.7.22") (deps (list (crate-dep (name "compiletest_rs") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "phf") (req "^0.7.22") (default-features #t) (kind 2)) (crate-dep (name "phf_generator") (req "= 0.7.22") (default-features #t) (kind 0)) (crate-dep (name "phf_shared") (req "= 0.7.22") (default-features #t) (kind 0)) (crate-dep (name "unicase") (req "^1.4") (optional #t) (default-features #t) (kind 0)))) (hash "1d833kl45w2rr02sxja5481srmavzdmmck1jw598br85cs2gwv6f") (features (quote (("unicase_support" "unicase" "phf_shared/unicase") ("stats"))))))

(define-public crate-phf_macros-0.7 (crate (name "phf_macros") (vers "0.7.23") (deps (list (crate-dep (name "compiletest_rs") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "phf") (req "^0.7.23") (default-features #t) (kind 2)) (crate-dep (name "phf_generator") (req "= 0.7.23") (default-features #t) (kind 0)) (crate-dep (name "phf_shared") (req "= 0.7.23") (default-features #t) (kind 0)) (crate-dep (name "unicase") (req "^1.4") (optional #t) (default-features #t) (kind 0)))) (hash "1mm6glranym3m0617f7j01igxcsvmp3nrqdra3c7kxjhd4nzja8i") (features (quote (("unicase_support" "unicase" "phf_shared/unicase") ("stats"))))))

(define-public crate-phf_macros-0.7 (crate (name "phf_macros") (vers "0.7.24") (deps (list (crate-dep (name "compiletest_rs") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "phf_generator") (req "^0.7.24") (default-features #t) (kind 0)) (crate-dep (name "phf_shared") (req "^0.7.24") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^0.6") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^0.15") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "0dzylcy14ksy60h265l433j9ra8xhg8xlq3pd5qk658m6f1mxd5x")))

(define-public crate-phf_macros-0.8 (crate (name "phf_macros") (vers "0.8.0") (deps (list (crate-dep (name "phf") (req "^0.8") (features (quote ("macros" "unicase"))) (default-features #t) (kind 2)) (crate-dep (name "phf_generator") (req "^0.8.0") (default-features #t) (kind 0)) (crate-dep (name "phf_shared") (req "^0.8.0") (default-features #t) (kind 0)) (crate-dep (name "proc-macro-hack") (req "^0.5.4") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1") (features (quote ("full"))) (default-features #t) (kind 0)) (crate-dep (name "trybuild") (req "^1.0") (default-features #t) (kind 2)) (crate-dep (name "unicase") (req "^2.4.0") (default-features #t) (kind 2)))) (hash "170qm6yqn6b9mjlwb2xmm3iad9d5nzwgfawfwy7zr7s2zwcdwvvz")))

(define-public crate-phf_macros-0.9 (crate (name "phf_macros") (vers "0.9.0") (deps (list (crate-dep (name "phf") (req "^0.8") (features (quote ("macros" "unicase"))) (default-features #t) (kind 2)) (crate-dep (name "phf_generator") (req "^0.9.0") (default-features #t) (kind 0)) (crate-dep (name "phf_shared") (req "^0.9.0") (kind 0)) (crate-dep (name "proc-macro-hack") (req "^0.5.4") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1") (features (quote ("full"))) (default-features #t) (kind 0)) (crate-dep (name "trybuild") (req "^1.0") (default-features #t) (kind 2)) (crate-dep (name "unicase_") (req "^2.4.0") (optional #t) (default-features #t) (kind 0) (package "unicase")) (crate-dep (name "unicase_") (req "^2.4.0") (default-features #t) (kind 2) (package "unicase")))) (hash "11mbi4x03gz7jnf4bg9fzxdxn6gd8ddkj29hms0dh3mmds9za1mp") (features (quote (("unicase" "unicase_" "phf_shared/unicase"))))))

(define-public crate-phf_macros-0.9 (crate (name "phf_macros") (vers "0.9.1") (deps (list (crate-dep (name "phf") (req "^0.9") (features (quote ("macros" "unicase"))) (default-features #t) (kind 2)) (crate-dep (name "phf_generator") (req "^0.9.1") (default-features #t) (kind 0)) (crate-dep (name "phf_shared") (req "^0.9.0") (kind 0)) (crate-dep (name "proc-macro-hack") (req "^0.5.4") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1") (features (quote ("full"))) (default-features #t) (kind 0)) (crate-dep (name "trybuild") (req "^1.0") (default-features #t) (kind 2)) (crate-dep (name "unicase_") (req "^2.4.0") (optional #t) (default-features #t) (kind 0) (package "unicase")) (crate-dep (name "unicase_") (req "^2.4.0") (default-features #t) (kind 2) (package "unicase")))) (hash "1rpc0jy5sfrk3ir87k2q0kk44a45nsrbwc131jmsi6f8hi3fqi7g") (features (quote (("unicase" "unicase_" "phf_shared/unicase")))) (yanked #t)))

(define-public crate-phf_macros-0.10 (crate (name "phf_macros") (vers "0.10.0") (deps (list (crate-dep (name "phf") (req "^0.9") (features (quote ("macros" "unicase"))) (default-features #t) (kind 2)) (crate-dep (name "phf_generator") (req "^0.10.0") (default-features #t) (kind 0)) (crate-dep (name "phf_shared") (req "^0.10.0") (kind 0)) (crate-dep (name "proc-macro-hack") (req "^0.5.4") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1") (features (quote ("full"))) (default-features #t) (kind 0)) (crate-dep (name "trybuild") (req "^1.0") (default-features #t) (kind 2)) (crate-dep (name "unicase_") (req "^2.4.0") (optional #t) (default-features #t) (kind 0) (package "unicase")) (crate-dep (name "unicase_") (req "^2.4.0") (default-features #t) (kind 2) (package "unicase")))) (hash "1q5ljwvb10dx188i6jxzckqfimjw5pm2p4kkvmhg2q6m9lcg7zaq") (features (quote (("unicase" "unicase_" "phf_shared/unicase"))))))

(define-public crate-phf_macros-0.11 (crate (name "phf_macros") (vers "0.11.0") (deps (list (crate-dep (name "phf_generator") (req "^0.11.0") (default-features #t) (kind 0)) (crate-dep (name "phf_shared") (req "^0.11.0") (kind 0)) (crate-dep (name "proc-macro2") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1") (features (quote ("full"))) (default-features #t) (kind 0)) (crate-dep (name "unicase_") (req "^2.4.0") (optional #t) (default-features #t) (kind 0) (package "unicase")))) (hash "1ai6wlha906m32f0y0m3m01gjdaik811agl8b5myaw2fqhd3b56d") (features (quote (("unicase" "unicase_" "phf_shared/unicase")))) (rust-version "1.60")))

(define-public crate-phf_macros-0.11 (crate (name "phf_macros") (vers "0.11.1") (deps (list (crate-dep (name "phf_generator") (req "^0.11.0") (default-features #t) (kind 0)) (crate-dep (name "phf_shared") (req "^0.11.1") (kind 0)) (crate-dep (name "proc-macro2") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1") (features (quote ("full"))) (default-features #t) (kind 0)) (crate-dep (name "unicase_") (req "^2.4.0") (optional #t) (default-features #t) (kind 0) (package "unicase")))) (hash "0rncvjimjri2vancig85icbk8h03a5s3z4cyasd70s37y72wvalj") (features (quote (("unicase" "unicase_" "phf_shared/unicase")))) (rust-version "1.60")))

(define-public crate-phf_macros-0.11 (crate (name "phf_macros") (vers "0.11.2") (deps (list (crate-dep (name "phf_generator") (req "^0.11.1") (default-features #t) (kind 0)) (crate-dep (name "phf_shared") (req "^0.11.2") (kind 0)) (crate-dep (name "proc-macro2") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2") (features (quote ("full"))) (default-features #t) (kind 0)) (crate-dep (name "unicase_") (req "^2.4.0") (optional #t) (default-features #t) (kind 0) (package "unicase")))) (hash "0js61lc0bhzzrbd9vhpcqp11vvwckdkz3g7k95z5h1k651p68i1l") (features (quote (("unicase" "unicase_" "phf_shared/unicase")))) (rust-version "1.60")))

(define-public crate-phf_mut-0.3 (crate (name "phf_mut") (vers "0.3.0") (deps (list (crate-dep (name "bit-vec") (req "^0.4.3") (default-features #t) (kind 0)))) (hash "0cf9cdrqyxf1ai889axlyhn2ck0nq6wpjvl0grcf2ggmmf07745g")))

(define-public crate-phf_mut-0.3 (crate (name "phf_mut") (vers "0.3.1") (deps (list (crate-dep (name "bit-vec") (req "^0.4.3") (default-features #t) (kind 0)))) (hash "1f6c9m3iyam50k4q20cpp9sjlzmc30h695cljx57q8c15x500h97")))

(define-public crate-phf_mut-0.3 (crate (name "phf_mut") (vers "0.3.2") (deps (list (crate-dep (name "bit-vec") (req "^0.4.3") (default-features #t) (kind 0)))) (hash "0g6f1yvf7pqqxy87jnggcya97bqzw47rpqw48wrsc8v7hswwhrni")))

(define-public crate-phf_mut-0.3 (crate (name "phf_mut") (vers "0.3.3") (deps (list (crate-dep (name "bit-vec") (req "^0.4.3") (default-features #t) (kind 0)))) (hash "11z0z1143y0q2s9p2wras695jsqwr4dfgvdxa4604fchbhny5li1")))

(define-public crate-phf_mut-0.4 (crate (name "phf_mut") (vers "0.4.1") (deps (list (crate-dep (name "bit-vec") (req "^0.6.3") (default-features #t) (kind 0)))) (hash "00z0953brzncnz4a3j20mghylp6ssvdw8kdq0axhl65wwkbajgaf")))

(define-public crate-phf_shared-0.0.1 (crate (name "phf_shared") (vers "0.0.1") (deps (list (crate-dep (name "xxhash") (req "*") (default-features #t) (kind 0)))) (hash "027d9davy409phb4s604rd38bpxj8nvq2dp9lj2z4vr527xahq6v")))

(define-public crate-phf_shared-0.1 (crate (name "phf_shared") (vers "0.1.0") (deps (list (crate-dep (name "xxhash") (req "*") (default-features #t) (kind 0)))) (hash "0pl2bcd1v9rypqny3fnv3dsrssff6gdzmz68f5n05ac4whlg7m6w")))

(define-public crate-phf_shared-0.2 (crate (name "phf_shared") (vers "0.2.0") (deps (list (crate-dep (name "xxhash") (req "*") (default-features #t) (kind 0)))) (hash "0fn7251k0lddxb4rk25rc1h9xrmg5yvancwxvq9jzamckakiiqkr")))

(define-public crate-phf_shared-0.3 (crate (name "phf_shared") (vers "0.3.0") (deps (list (crate-dep (name "xxhash") (req "*") (default-features #t) (kind 0)))) (hash "1hnanp338ivqp6w81lcs9cflyg5nqjp28di8jicz9mz6a7wbx3ii")))

(define-public crate-phf_shared-0.4 (crate (name "phf_shared") (vers "0.4.0") (hash "04i8vfz9hvhamjl0x11d0znsnq2x42x40690mw70irbapcl4f6mz")))

(define-public crate-phf_shared-0.4 (crate (name "phf_shared") (vers "0.4.1") (hash "12hvyxz8mh27f48wshkfrlmws3cfxjryf6fa8yhn2v5hxry1p2kr")))

(define-public crate-phf_shared-0.4 (crate (name "phf_shared") (vers "0.4.2") (hash "05v51nhp475ljll8wcxmlplx1alyw4q8bvry8amdhlf3h8icrkr6")))

(define-public crate-phf_shared-0.4 (crate (name "phf_shared") (vers "0.4.3") (hash "0pklfrgjl16hhh249gy2cgbr94m025llm77w70qsjv2kb9phxvj7")))

(define-public crate-phf_shared-0.4 (crate (name "phf_shared") (vers "0.4.4") (hash "0x7a00iiqhi7awqls0yd06c6lsm6lix8sdsbcgmghkzy3rvwrp6p")))

(define-public crate-phf_shared-0.4 (crate (name "phf_shared") (vers "0.4.5") (hash "1dz9nbg2iwambjrvzglfx8g40zdyz0avj9xqwkb91scvgjjzj1z5")))

(define-public crate-phf_shared-0.4 (crate (name "phf_shared") (vers "0.4.6") (hash "085p8cbdziv864sqisrvml3d8v3bprllj2a4lm8wj7gs3mx5b13p")))

(define-public crate-phf_shared-0.4 (crate (name "phf_shared") (vers "0.4.7") (hash "0cwxjzmrhwn5890h2044kdyl3sinddamad98h845jq9hpc3xgkf5") (yanked #t)))

(define-public crate-phf_shared-0.4 (crate (name "phf_shared") (vers "0.4.8") (hash "1v5h452810srv5sml4lwnvx964hh8vfidsk35dn0xzbqslfz84hn")))

(define-public crate-phf_shared-0.4 (crate (name "phf_shared") (vers "0.4.9") (hash "0h2x3ikw1rzzpsqzn9fpnpmswx6nf0a5j39ag64scw36i3fsa28h")))

(define-public crate-phf_shared-0.5 (crate (name "phf_shared") (vers "0.5.0") (hash "1qvwqi7hfi5mk5sbr65x1wnjmypxccv0dzmpcz924j3df942vl92")))

(define-public crate-phf_shared-0.6 (crate (name "phf_shared") (vers "0.6.0") (hash "0wdz9a96p0b75mp4mf09l5f2g9r3lbbzdzkpi9kkblzymr18bpg1")))

(define-public crate-phf_shared-0.6 (crate (name "phf_shared") (vers "0.6.1") (hash "0qxcrwd9zn5azwjr0h5mcqpq3ii5wgqhjb0dwvld0s2s2hy1x0jc")))

(define-public crate-phf_shared-0.6 (crate (name "phf_shared") (vers "0.6.2") (hash "1navq41ysw6mmbdrkl42fk0as7z84710z2rynxz0wzs6qb5gc0d0") (features (quote (("core"))))))

(define-public crate-phf_shared-0.6 (crate (name "phf_shared") (vers "0.6.3") (hash "1d63xwjwjpwf0avbq49bkk2zqz3dzmd96ddr3lsv2psx5jl71nrh") (features (quote (("core"))))))

(define-public crate-phf_shared-0.6 (crate (name "phf_shared") (vers "0.6.4") (hash "0yghsdxvrcqbl7v6z6xn90m9xr8vn3fn7r5gbv261fp42iaq72yf") (features (quote (("core"))))))

(define-public crate-phf_shared-0.6 (crate (name "phf_shared") (vers "0.6.5") (hash "07xwm2fmp5vgi8g607nwv01mmp7whpm10s3qfd7k4gngrw1aichv") (features (quote (("core"))))))

(define-public crate-phf_shared-0.6 (crate (name "phf_shared") (vers "0.6.6") (hash "1aynmisj395m7w69rlxbvghlzp00z7q27rvg97jz185w2nm2ja6c") (features (quote (("core"))))))

(define-public crate-phf_shared-0.6 (crate (name "phf_shared") (vers "0.6.7") (hash "1cf2cdf4ll7rbqhw08bg2qy3jab77fh0l8gc8dszmc3mckab3bcc") (features (quote (("core"))))))

(define-public crate-phf_shared-0.6 (crate (name "phf_shared") (vers "0.6.8") (hash "1z816hfz5yc45l6rabxqbp2mjb5yar48xgf69q8qfljzz8cj2ii8") (features (quote (("core"))))))

(define-public crate-phf_shared-0.6 (crate (name "phf_shared") (vers "0.6.9") (hash "0ibm6z075b8hmr6idlmk95k0258sdq73xlddl6472kjnx72qf3d6") (features (quote (("core"))))))

(define-public crate-phf_shared-0.6 (crate (name "phf_shared") (vers "0.6.10") (hash "1248dd1chxvwmv5583w1r8bq8y31f9ggbyg7c09ain45phgny456")))

(define-public crate-phf_shared-0.6 (crate (name "phf_shared") (vers "0.6.11") (hash "07g2frk4pafnv7xhrj0r9w5ryy4fss61maqms06gg7jv3m42mss3")))

(define-public crate-phf_shared-0.6 (crate (name "phf_shared") (vers "0.6.12") (hash "1y8lvjyphbffyf93qrx1k87xdgs0v4ycy1wgws2ayyn8iglz7pnf")))

(define-public crate-phf_shared-0.6 (crate (name "phf_shared") (vers "0.6.13") (hash "0xxkdmi678rvsahziv0iidq2pw3a6aqjv4yybhfvfspria4i2785")))

(define-public crate-phf_shared-0.6 (crate (name "phf_shared") (vers "0.6.14") (hash "0i6wzj198957rl26d1xzi3vfvrg62g1s7icya0r9dnwgn403jiff")))

(define-public crate-phf_shared-0.6 (crate (name "phf_shared") (vers "0.6.15") (hash "0r83y0bbcg03wjnf2bik30f4chnxgzjlwpnklbrk6kfcrkayd45z")))

(define-public crate-phf_shared-0.6 (crate (name "phf_shared") (vers "0.6.16") (hash "14bicmjdksbggy3izyyaikyglpvfqccri4i2w322dkcgp2m3s5yd") (yanked #t)))

(define-public crate-phf_shared-0.6 (crate (name "phf_shared") (vers "0.6.17") (hash "0w9gw0zrhwx3fakplgdxlpqlbmik4sjyllw3r46l27rpn5c2q5g2")))

(define-public crate-phf_shared-0.6 (crate (name "phf_shared") (vers "0.6.18") (hash "04zhm5252m4k8pz8i7yizzknllks6pifjx1a28gd215q76g0xbcm")))

(define-public crate-phf_shared-0.6 (crate (name "phf_shared") (vers "0.6.19") (hash "1z22f8mavnh7fg16m4yhff52y9ccv9dy7s6c9i2h454w19ii09ps")))

(define-public crate-phf_shared-0.7 (crate (name "phf_shared") (vers "0.7.0") (hash "1a7bwgch6sh8i2frmmvx435p99mgaamdf8fx9xld1l6rk8lngca1")))

(define-public crate-phf_shared-0.7 (crate (name "phf_shared") (vers "0.7.1") (hash "19amrznlf17xcrzyzg2ckd8vf4dayga0v80qp46wbqn4ya1rvnik")))

(define-public crate-phf_shared-0.7 (crate (name "phf_shared") (vers "0.7.2") (hash "09q9hsfcdk0bf628nqffqjfwbsxpzzhc4722rmlximmrg1ycmapw")))

(define-public crate-phf_shared-0.7 (crate (name "phf_shared") (vers "0.7.3") (hash "06584y29ncb2r2pya3kkwpb4am5q97vw8xjjsni57n35pwgy0hj5")))

(define-public crate-phf_shared-0.7 (crate (name "phf_shared") (vers "0.7.4") (hash "0ry9xxf66ag0mlvllk5i3jslr997m46nf1drmrj32k7bhki6la8v")))

(define-public crate-phf_shared-0.7 (crate (name "phf_shared") (vers "0.7.5") (hash "1xdnd1rgp483miq9rk2n0i5571glx31cpd7qnnr3gyy7jbrdd7rd")))

(define-public crate-phf_shared-0.7 (crate (name "phf_shared") (vers "0.7.6") (hash "1x149424na80x6rp11p0fyn7681c6n04zywfid5ckyamkynsm5rp") (features (quote (("core"))))))

(define-public crate-phf_shared-0.7 (crate (name "phf_shared") (vers "0.7.7") (hash "1yk5khwm16jljajfqrx8wjqjd4hq2yn8z1525hk1j5qv8xs4vlw8") (features (quote (("core"))))))

(define-public crate-phf_shared-0.7 (crate (name "phf_shared") (vers "0.7.8") (hash "0lcz78y4sldglp7ysyajislfz8im2pachib2cif2k39c5fwnb7ml") (features (quote (("core"))))))

(define-public crate-phf_shared-0.7 (crate (name "phf_shared") (vers "0.7.9") (hash "0jq5vjb53l6g708znrqskhc9ib2pxpd8vwsp6q36sv4h94i40rfd") (features (quote (("core"))))))

(define-public crate-phf_shared-0.7 (crate (name "phf_shared") (vers "0.7.10") (hash "1z2n6mjb72xq8n9ida1dxyblkfgj3zxk3wghd03ijxf008hxgpmb") (features (quote (("core"))))))

(define-public crate-phf_shared-0.7 (crate (name "phf_shared") (vers "0.7.11") (hash "0fsxnby0p4w4j167lll7lbrqh4445mlhaif1pp2h2j0f627xbj7b") (features (quote (("core"))))))

(define-public crate-phf_shared-0.7 (crate (name "phf_shared") (vers "0.7.12") (hash "1lx0r3ipyibfnwp9m0ir3qczslgxxg3nvrqqs5hi81svhq36k3zl") (features (quote (("core"))))))

(define-public crate-phf_shared-0.7 (crate (name "phf_shared") (vers "0.7.13") (hash "0i9qfidsm448mg0vw52zm5a5l4gvq9b394yajdk2h52jcc4m2a36") (features (quote (("core"))))))

(define-public crate-phf_shared-0.7 (crate (name "phf_shared") (vers "0.7.14") (deps (list (crate-dep (name "unicase") (req "^1.1") (optional #t) (default-features #t) (kind 0)))) (hash "19ia1yac83pa8afc0ga8nr3rhj9sz5n9f5cv7h94akqfjcwx1r7y") (features (quote (("core"))))))

(define-public crate-phf_shared-0.7 (crate (name "phf_shared") (vers "0.7.15") (deps (list (crate-dep (name "unicase") (req "^1.1") (optional #t) (default-features #t) (kind 0)))) (hash "1fpzzpnmgiiai5m3hmixi5yb25ll4iphjxqlnh32n30lq6m18v5v") (features (quote (("core"))))))

(define-public crate-phf_shared-0.7 (crate (name "phf_shared") (vers "0.7.16") (deps (list (crate-dep (name "unicase") (req "^1.1") (optional #t) (default-features #t) (kind 0)))) (hash "1mhd2dz26p2vj452nhhm7zajzrvhdnq0pia5xpsg2cadx7dvahrc") (features (quote (("core"))))))

(define-public crate-phf_shared-0.7 (crate (name "phf_shared") (vers "0.7.17") (deps (list (crate-dep (name "unicase") (req "^1.4") (optional #t) (default-features #t) (kind 0)))) (hash "1g98i064dqc55xncqnix1a4vxr6ixhbm67nkk33k6a0pj68z0raa") (features (quote (("core"))))))

(define-public crate-phf_shared-0.7 (crate (name "phf_shared") (vers "0.7.18") (deps (list (crate-dep (name "unicase") (req "^1.4") (optional #t) (default-features #t) (kind 0)))) (hash "0awfgbr65x61y6qkylihaaagj0521gmarr04xi4f6qpg8gdqqrk9") (features (quote (("core"))))))

(define-public crate-phf_shared-0.7 (crate (name "phf_shared") (vers "0.7.19") (deps (list (crate-dep (name "unicase") (req "^1.4") (optional #t) (default-features #t) (kind 0)))) (hash "0f64samykz5938rk073bm4pdjinwl98vav32kr5ynh20ii2q8gcz") (features (quote (("core"))))))

(define-public crate-phf_shared-0.7 (crate (name "phf_shared") (vers "0.7.20") (deps (list (crate-dep (name "unicase") (req "^1.4") (optional #t) (default-features #t) (kind 0)))) (hash "1dygq9jgcywai50giak1w2q9bhw4rzq1jb2vw6y4fh8dwnh8aqr8") (features (quote (("core"))))))

(define-public crate-phf_shared-0.7 (crate (name "phf_shared") (vers "0.7.21") (deps (list (crate-dep (name "siphasher") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "unicase") (req "^1.4") (optional #t) (default-features #t) (kind 0)))) (hash "1wn2gcxb44qizhm9j2qf0fqbp28vdgdb7wij0v6xwfv4m464pqh7") (features (quote (("core"))))))

(define-public crate-phf_shared-0.7 (crate (name "phf_shared") (vers "0.7.22") (deps (list (crate-dep (name "siphasher") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "unicase") (req "^1.4") (optional #t) (default-features #t) (kind 0)))) (hash "0c2rggczc7jgbmpqrwiffr7wdixrf7ihnaq220xsmdib9ia1s9n2") (features (quote (("core"))))))

(define-public crate-phf_shared-0.7 (crate (name "phf_shared") (vers "0.7.23") (deps (list (crate-dep (name "siphasher") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "unicase") (req "^1.4") (optional #t) (default-features #t) (kind 0)))) (hash "14qb2lzqxiky173qgjwjbl4nkp29frrhajpn0znks9yl4a6qjfdm") (features (quote (("core"))))))

(define-public crate-phf_shared-0.7 (crate (name "phf_shared") (vers "0.7.24") (deps (list (crate-dep (name "siphasher") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "unicase") (req "^1.4") (optional #t) (default-features #t) (kind 0)))) (hash "18371fla0vsj7d6d5rlfb747xbr2in11ar9vgv5qna72bnhp2kr3") (features (quote (("core"))))))

(define-public crate-phf_shared-0.8 (crate (name "phf_shared") (vers "0.8.0") (deps (list (crate-dep (name "siphasher") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "unicase") (req "^2.4.0") (optional #t) (default-features #t) (kind 0)))) (hash "1xssnqrrcn0nr9ayqrnm8xm37ac4xvwcx8pax7jxss7yxawzh360") (features (quote (("std") ("default" "std"))))))

(define-public crate-phf_shared-0.9 (crate (name "phf_shared") (vers "0.9.0") (deps (list (crate-dep (name "siphasher") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "uncased") (req "^0.9.6") (optional #t) (kind 0)) (crate-dep (name "unicase") (req "^2.4.0") (optional #t) (default-features #t) (kind 0)))) (hash "1ng0hi2byifqah6bcdy3zcpbwq8jxgl4laz65gq40dp3dm11i0x6") (features (quote (("std") ("default" "std"))))))

(define-public crate-phf_shared-0.10 (crate (name "phf_shared") (vers "0.10.0") (deps (list (crate-dep (name "siphasher") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "uncased") (req "^0.9.6") (optional #t) (kind 0)) (crate-dep (name "unicase") (req "^2.4.0") (optional #t) (default-features #t) (kind 0)))) (hash "15n02nc8yqpd8hbxngblar2g53p3nllc93d8s8ih3p5cf7bnlydn") (features (quote (("std") ("default" "std"))))))

(define-public crate-phf_shared-0.11 (crate (name "phf_shared") (vers "0.11.0") (deps (list (crate-dep (name "siphasher") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "uncased") (req "^0.9.6") (optional #t) (kind 0)) (crate-dep (name "unicase") (req "^2.4.0") (optional #t) (default-features #t) (kind 0)))) (hash "1r28c1shni0zq5mzfpgkdk0hkqvc2hdjx8q8z5kp3y1d9ffn1mcx") (features (quote (("std") ("default" "std")))) (rust-version "1.60")))

(define-public crate-phf_shared-0.11 (crate (name "phf_shared") (vers "0.11.1") (deps (list (crate-dep (name "siphasher") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "uncased") (req "^0.9.6") (optional #t) (kind 0)) (crate-dep (name "unicase") (req "^2.4.0") (optional #t) (default-features #t) (kind 0)))) (hash "0xp6krf3cd411rz9rbk7p6xprlz786a215039j6jlxvbh9pmzyz1") (features (quote (("std") ("default" "std")))) (rust-version "1.60")))

(define-public crate-phf_shared-0.11 (crate (name "phf_shared") (vers "0.11.2") (deps (list (crate-dep (name "siphasher") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "uncased") (req "^0.9.6") (optional #t) (kind 0)) (crate-dep (name "unicase") (req "^2.4.0") (optional #t) (default-features #t) (kind 0)))) (hash "0azphb0a330ypqx3qvyffal5saqnks0xvl8rj73jlk3qxxgbkz4h") (features (quote (("std") ("default" "std")))) (rust-version "1.60")))

