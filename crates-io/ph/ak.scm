(define-module (crates-io ph ak) #:use-module (crates-io))

(define-public crate-phakebit-0.1 (crate (name "phakebit") (vers "0.1.0") (deps (list (crate-dep (name "cargo-readme") (req "^3.3.0") (default-features #t) (kind 2)) (crate-dep (name "circular-buffer") (req "^0.1.3") (default-features #t) (kind 2)))) (hash "0y3ak6anh705f6lpi9dnis6819wdka59i3c9vc4nzhxkxcz0ybx1")))

(define-public crate-phakebit-0.1 (crate (name "phakebit") (vers "0.1.1") (deps (list (crate-dep (name "cargo-readme") (req "^3.3.0") (default-features #t) (kind 2)) (crate-dep (name "circular-buffer") (req "^0.1.3") (default-features #t) (kind 2)))) (hash "01k3y1zyzq4vvcf171zj3ms4b21g63a6rb3ah0jc0vpjz9zvqqs6")))

(define-public crate-phakebit-0.1 (crate (name "phakebit") (vers "0.1.2") (deps (list (crate-dep (name "cargo-readme") (req "^3.3.0") (default-features #t) (kind 2)) (crate-dep (name "circular-buffer") (req "^0.1.3") (default-features #t) (kind 2)))) (hash "0fkvwqvgr3rn7qbjs4yjrl2znfi6x7prh9ywjr76qis7xxvlgp5s")))

(define-public crate-phakebit-0.1 (crate (name "phakebit") (vers "0.1.3") (deps (list (crate-dep (name "cargo-readme") (req "^3.3.0") (default-features #t) (kind 2)) (crate-dep (name "circular-buffer") (req "^0.1.3") (default-features #t) (kind 2)))) (hash "0j9vk4g4g4q1kldfpijw752smi89nsadpm91qi5rksfgyvv9yjna")))

(define-public crate-phakebit-0.1 (crate (name "phakebit") (vers "0.1.4") (deps (list (crate-dep (name "cargo-readme") (req "^3.3.0") (default-features #t) (kind 2)) (crate-dep (name "circular-buffer") (req "^0.1.3") (default-features #t) (kind 2)))) (hash "07i8rdww8w880s77bc1wz2yjgjj48pj389yp20hi8vx022f2ymn4")))

(define-public crate-phaktionz-0.4 (crate (name "phaktionz") (vers "0.4.0") (deps (list (crate-dep (name "mkproj-lib") (req "^0.1.5") (default-features #t) (kind 0)) (crate-dep (name "run_script") (req "^0.7.0") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.3.13") (default-features #t) (kind 0)))) (hash "0p0lywi3flpm6h0bw36sq5rs5nc2qxcijc0l7pfqa21ncnwb3fd0") (yanked #t)))

(define-public crate-phaktionz-0.4 (crate (name "phaktionz") (vers "0.4.1") (deps (list (crate-dep (name "mkproj-lib") (req "^0.1.5") (default-features #t) (kind 0)) (crate-dep (name "run_script") (req "^0.7.0") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.3.13") (default-features #t) (kind 0)))) (hash "1pyrf43r3yww310wbwjivdzwn2imfwk0m9r2hs3hz2lc4w0ar2di") (yanked #t)))

(define-public crate-phaktionz-0.4 (crate (name "phaktionz") (vers "0.4.2") (deps (list (crate-dep (name "mkproj-lib") (req "^0.1.5") (default-features #t) (kind 0)) (crate-dep (name "run_script") (req "^0.7.0") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.3.13") (default-features #t) (kind 0)))) (hash "0r0gh8qlics6a9ijkj949422bd9kj4mmvhj2b6w1yail98rxz82i") (yanked #t)))

(define-public crate-phaktionz-0.5 (crate (name "phaktionz") (vers "0.5.3") (deps (list (crate-dep (name "mkproj-lib") (req "^0.1.13") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.3.13") (default-features #t) (kind 0)))) (hash "00p5d6i8zrxcc3gv97h29nxa72ccsg2mf5fgmsi0pim2s6ff2viz") (yanked #t)))

(define-public crate-phaktionz-1 (crate (name "phaktionz") (vers "1.6.0") (deps (list (crate-dep (name "mkproj-lib") (req "^0.2.10") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.3.13") (default-features #t) (kind 0)))) (hash "0niqjhh12zd2c9k8i2dh3gp0gw8qvp10c9a125kklryrh0rh303i") (yanked #t)))

