(define-module (crates-io ph pb) #:use-module (crates-io))

(define-public crate-phpbb-pwhash-0.1 (crate (name "phpbb-pwhash") (vers "0.1.0") (deps (list (crate-dep (name "base64") (req "^0.12") (default-features #t) (kind 0)) (crate-dep (name "md5") (req "^0.7") (default-features #t) (kind 0)))) (hash "02zd2zk28c3x4cm4d9yy7xjfzkxaiqb7kg2jc00s2i021q3s6nvm")))

(define-public crate-phpbb-pwhash-0.1 (crate (name "phpbb-pwhash") (vers "0.1.1") (deps (list (crate-dep (name "base64") (req "^0.12") (default-features #t) (kind 0)) (crate-dep (name "md5") (req "^0.7") (default-features #t) (kind 0)))) (hash "09zivc3h0g4k4qyj1vrgks1gab41vwchls2fa39fmqikhq7rfsh5")))

