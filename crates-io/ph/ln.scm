(define-module (crates-io ph ln) #:use-module (crates-io))

(define-public crate-phln-0.1 (crate (name "phln") (vers "0.1.0") (deps (list (crate-dep (name "bitflags") (req "^1.3.2") (default-features #t) (kind 0)) (crate-dep (name "bxcan") (req "^0.7.0") (default-features #t) (kind 0)) (crate-dep (name "num-complex") (req "^0.4.3") (kind 0)))) (hash "1ikxl0ddb0bk91w1c8149wvw9xvkk60n11415a8vqwcbpzb8qlj2")))

