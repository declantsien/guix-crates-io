(define-module (crates-io ph pf) #:use-module (crates-io))

(define-public crate-phpfmt-0.0.0 (crate (name "phpfmt") (vers "0.0.0-b1") (deps (list (crate-dep (name "cmder") (req "^0.6.1") (default-features #t) (kind 0)) (crate-dep (name "colored") (req "^2.0.0") (default-features #t) (kind 0)) (crate-dep (name "phpfmt-lib") (req "^0.0.0-b1") (default-features #t) (kind 0)))) (hash "0x9m3waw3q8wp64s3cw4pxhbrgsgbx60cd0dj9982b2fymxlry6j")))

(define-public crate-phpfmt-lib-0.0.0 (crate (name "phpfmt-lib") (vers "0.0.0-b1") (deps (list (crate-dep (name "serde") (req "^1.0.151") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0.151") (default-features #t) (kind 0)) (crate-dep (name "toml") (req "^0.5.10") (default-features #t) (kind 0)))) (hash "1qhihwjiy4ccsqzamiq10qszwgi58ypx4ps2k0kx6mrh4xp782c1")))

