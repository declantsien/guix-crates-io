(define-module (crates-io ph p_) #:use-module (crates-io))

(define-public crate-php_codegen-0.1 (crate (name "php_codegen") (vers "0.1.0") (hash "1xna4bwvh81y4p699lda9149p1hf341z0kjdrf3pkkjns1c2kgpy")))

(define-public crate-php_serde-0.6 (crate (name "php_serde") (vers "0.6.0") (deps (list (crate-dep (name "bson") (req "^0.14.0") (default-features #t) (kind 2)) (crate-dep (name "proptest") (req "^1.0.0") (default-features #t) (kind 2)) (crate-dep (name "ryu") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.101") (features (quote ("derive"))) (default-features #t) (kind 2)) (crate-dep (name "serde_bytes") (req "^0.11.2") (default-features #t) (kind 2)) (crate-dep (name "smallvec") (req "^1.6") (default-features #t) (kind 0)) (crate-dep (name "tempfile") (req "^3.2") (default-features #t) (kind 2)))) (hash "0nrj8x6qv7vr6adg4pyfc6zdny032mjnp9j9vgbp8lx24ysw8a9d")))

