(define-module (crates-io ph or) #:use-module (crates-io))

(define-public crate-phoron_asm-0.1 (crate (name "phoron_asm") (vers "0.1.0") (hash "0k6929nzy7w8dx5blafjpj3is99ji8sdfcwcbjrnzlrm1k9wl4np")))

(define-public crate-phoron_asm-1 (crate (name "phoron_asm") (vers "1.0.0") (deps (list (crate-dep (name "phoron_core") (req "^0.5.3") (default-features #t) (kind 0)))) (hash "0f01y0a21rj0hmh6za92cp9l1fnnp85lknfs8hamqlds9n3xlknq")))

(define-public crate-phoron_asm-1 (crate (name "phoron_asm") (vers "1.0.1") (deps (list (crate-dep (name "phoron_core") (req "^0.5.4") (default-features #t) (kind 0)))) (hash "0nz7y6jv0awxmvnnpijfkkl6q68v753i3z8qk3c4p9v1f853nm5q")))

(define-public crate-phoron_asm-1 (crate (name "phoron_asm") (vers "1.0.2") (deps (list (crate-dep (name "phoron_core") (req "^0.5.4") (default-features #t) (kind 0)))) (hash "1mczfnascl54c7kvx1kab12n9gdqrrq6lwmnk25frjykqwyqhfwv")))

(define-public crate-phoron_core-0.1 (crate (name "phoron_core") (vers "0.1.0") (hash "1qg4r1k6z47g6lwdvfkwp7bxx5gg1jxlwjy7clc6bdwrb7j21yi1")))

(define-public crate-phoron_core-0.2 (crate (name "phoron_core") (vers "0.2.0") (hash "11mnlby5jxvskpdv6sajvpf3x6pw16ay341v7jx9zzhs3mam6hn2")))

(define-public crate-phoron_core-0.3 (crate (name "phoron_core") (vers "0.3.0") (hash "00ynkj01h0zd1n2dp53wr70msm9jpa5jm7km6rbf8abn9f80pcx4")))

(define-public crate-phoron_core-0.4 (crate (name "phoron_core") (vers "0.4.0") (hash "051xb5cnq8j73nab9g7wjs0gjn5nbjrrb2qjwykdb78y3raiqnlj")))

(define-public crate-phoron_core-0.4 (crate (name "phoron_core") (vers "0.4.1") (hash "1dg52vnk6s3yvpmy4v2l59b7h5gxc1kmmmisn2gfj97spxq4slnj")))

(define-public crate-phoron_core-0.5 (crate (name "phoron_core") (vers "0.5.1") (hash "0bgwcg4splxq3r9v76qvizascvfzanng008gn0xrhr89y64x0cak")))

(define-public crate-phoron_core-0.5 (crate (name "phoron_core") (vers "0.5.2") (hash "0f4prmdy8mi8m4cnnvawyy9gpf94zzqrnmw3xawns1y7k8macgzm")))

(define-public crate-phoron_core-0.5 (crate (name "phoron_core") (vers "0.5.3") (hash "052wkg8vxdlmgmgnbww4s0p9kz00kf3xwv7if3g5xfxcximfz4mp")))

(define-public crate-phoron_core-0.5 (crate (name "phoron_core") (vers "0.5.4") (hash "1ldxnp04ibm81nnaibpqyixavvrawxhkswg926hn6i93l7y1xm95")))

(define-public crate-phoronix-reader-0.2 (crate (name "phoronix-reader") (vers "0.2.3") (deps (list (crate-dep (name "gdk") (req "^0.3.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "gtk") (req "^0.0.7") (features (quote ("v3_16"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "hyper") (req "^0.9.2") (default-features #t) (kind 0)) (crate-dep (name "select") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "term") (req "^0.4.4") (optional #t) (default-features #t) (kind 0)))) (hash "04ykj4f4v2s9knx3jg8ihav27bmb1n491ksgx58algqhp97bffv3") (features (quote (("enable_gtk" "gtk" "gdk") ("enable_colors" "term"))))))

