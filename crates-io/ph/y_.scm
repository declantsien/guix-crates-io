(define-module (crates-io ph y_) #:use-module (crates-io))

(define-public crate-phy_numerical-0.1 (crate (name "phy_numerical") (vers "0.1.0") (hash "0f7xa6v8zhmfq5dgj3sp62ip9s9ml3j6mffxn3rziv5brdxn9a53")))

(define-public crate-phy_numerical-0.1 (crate (name "phy_numerical") (vers "0.1.1") (hash "0kl9663072wvhpwy2k16wxlmaav464i9l1hy3p7rcdr6xphvsa3c")))

(define-public crate-phy_numerical-0.1 (crate (name "phy_numerical") (vers "0.1.2") (hash "0kfzk9bkbvlspqqfvsapknivw656bdrga88n1lr9lyr5dw4qv8cg")))

(define-public crate-phy_numerical-0.1 (crate (name "phy_numerical") (vers "0.1.3") (hash "15fs4jcclqig2649rnpvclvxc2g096fdsaw9h3186hv19ahyli3v")))

(define-public crate-phy_numerical-0.1 (crate (name "phy_numerical") (vers "0.1.4") (hash "1a3yq04yqzfji6vlpr0a3s1zbkhxr6hpg1bcgarbhw4kfk85bd9z")))

