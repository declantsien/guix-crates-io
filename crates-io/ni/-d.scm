(define-module (crates-io ni -d) #:use-module (crates-io))

(define-public crate-ni-daqmx-sys-20 (crate (name "ni-daqmx-sys") (vers "20.7.0") (deps (list (crate-dep (name "bindgen") (req "^0.60") (default-features #t) (kind 1)))) (hash "1sc5bn3x2gxv850xkdcp7xif953gc44dnppx67snf234grgw2sdy") (links "nidaqmx")))

(define-public crate-ni-daqmx-sys-20 (crate (name "ni-daqmx-sys") (vers "20.7.1") (deps (list (crate-dep (name "bindgen") (req "^0.60") (default-features #t) (kind 1)))) (hash "1y7pkdvvj2r1kk3xg5kci69zd7znz1xr6vgpal1diaigbsx4px3g") (links "nidaqmx")))

