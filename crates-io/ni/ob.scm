(define-module (crates-io ni ob) #:use-module (crates-io))

(define-public crate-niobe-0.0.0 (crate (name "niobe") (vers "0.0.0") (hash "056lhy1jkw3bnw815yjsric4rsbp1g431fpszn4s42agaddsgwwj")))

(define-public crate-niobe-core-0.0.0 (crate (name "niobe-core") (vers "0.0.0") (hash "0x0idcvrzi2iqs1i2n6xw95bcdd65f9v8l4jwdln77qh4zbp34gl")))

(define-public crate-niobium-0.0.0 (crate (name "niobium") (vers "0.0.0") (hash "1s3llql38xzyvvqdl9mfn4z6yi9k8a27yi1c7m7pwglw2szpxlw9")))

