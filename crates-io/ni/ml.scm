(define-module (crates-io ni ml) #:use-module (crates-io))

(define-public crate-nimlib-0.0.0 (crate (name "nimlib") (vers "0.0.0") (hash "1nj5wda2k8j4yzwj2ds7b78vfxvglpi8aga7cwg6d0zd3v89x010")))

(define-public crate-nimlib-0.0.1 (crate (name "nimlib") (vers "0.0.1") (deps (list (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.152") (features (quote ("std" "serde_derive"))) (default-features #t) (kind 0)))) (hash "07vaqv23v5h1m7f14hbvp86fphqy6n7gd53m7rhl6hq52yrqb1ki")))

(define-public crate-nimlib-0.1 (crate (name "nimlib") (vers "0.1.0") (deps (list (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.152") (features (quote ("std" "serde_derive"))) (default-features #t) (kind 0)))) (hash "0qd8j9s63rgcmbx8lvs3f29lghbf7fagqz32fq7iiwh47lpzanj5")))

(define-public crate-nimlib-0.1 (crate (name "nimlib") (vers "0.1.1") (deps (list (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.152") (features (quote ("std" "serde_derive"))) (default-features #t) (kind 0)))) (hash "0lb06r0s34k01q24spzbf2n9afqq78lm4sna0izsi59xgagz6s6r")))

