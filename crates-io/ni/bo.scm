(define-module (crates-io ni bo) #:use-module (crates-io))

(define-public crate-nibonacci-0.1 (crate (name "nibonacci") (vers "0.1.0") (deps (list (crate-dep (name "num-traits") (req "^0.2.8") (default-features #t) (kind 0)))) (hash "1533awi6vc8s0ggicl3964z0s791gvayzz9wlq7zi53r6vdvvfd6")))

