(define-module (crates-io ni sa) #:use-module (crates-io))

(define-public crate-nisargthakkar_hello_world-0.1 (crate (name "nisargthakkar_hello_world") (vers "0.1.0") (hash "1163r765rbnv0zyy35075mm6prc04sakwy9aa5ycjmmf2f90kdnk")))

(define-public crate-nisargthakkar_hello_world-0.1 (crate (name "nisargthakkar_hello_world") (vers "0.1.1") (hash "0x0xz46mg5gpgddbcw5yc674qzp2yyqf46cg99mgjsf0ynnv5q4l")))

(define-public crate-nisargthakkar_hello_world-0.1 (crate (name "nisargthakkar_hello_world") (vers "0.1.2") (hash "081zg3x6pj1sk4wklfxxmh23rgm4fx46991f2sn9shz7blkwc735")))

