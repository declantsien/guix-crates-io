(define-module (crates-io ni da) #:use-module (crates-io))

(define-public crate-nida_crates-0.1 (crate (name "nida_crates") (vers "0.1.1") (hash "12q191p388srz5qmnmdf1lr9c6wzff3dlyw9dwkjbkyqvyd5ljds")))

(define-public crate-nida_function-0.1 (crate (name "nida_function") (vers "0.1.1") (hash "193dmvmfk04c5kwi4zsa57fg93drv5g9188bf1g0ml7fnaqgyqkw")))

(define-public crate-nida_khan_functions-0.1 (crate (name "nida_khan_functions") (vers "0.1.0") (hash "19zf7bdkick5k8rjkrqac2qp1qsb1gk772w64019v8sf7b4y65mv")))

(define-public crate-nida_professional-0.1 (crate (name "nida_professional") (vers "0.1.0") (hash "0nkv1jpfjv942dfg9agmib6pqgg7k09v6crpmn0l4w15ffn9fj9k")))

