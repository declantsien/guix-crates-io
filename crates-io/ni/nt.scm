(define-module (crates-io ni nt) #:use-module (crates-io))

(define-public crate-nintendo-lz-0.1 (crate (name "nintendo-lz") (vers "0.1.0") (deps (list (crate-dep (name "byteorder") (req "^1.2.3") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^2.32.0") (default-features #t) (kind 0)) (crate-dep (name "needle") (req "^0.1.1") (default-features #t) (kind 0)) (crate-dep (name "simple-error") (req "^0.1.1") (default-features #t) (kind 0)))) (hash "1lg7azjnhw56sqk4l7yam09sfbn8cq2wl84j10lmzz5n42mj2q4j")))

(define-public crate-nintendo-lz-0.1 (crate (name "nintendo-lz") (vers "0.1.1") (deps (list (crate-dep (name "byteorder") (req "^1.2.3") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^2.32.0") (default-features #t) (kind 0)) (crate-dep (name "needle") (req "^0.1.1") (default-features #t) (kind 0)) (crate-dep (name "simple-error") (req "^0.1.1") (default-features #t) (kind 0)))) (hash "19jq1in5c9h9l20ilb90rizryykhp1bnrlv38dr3dwch10z2sjxh")))

(define-public crate-nintendo-lz-0.1 (crate (name "nintendo-lz") (vers "0.1.2") (deps (list (crate-dep (name "byteorder") (req "^1.2.3") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^2.32.0") (default-features #t) (kind 0)) (crate-dep (name "needle") (req "^0.1.1") (default-features #t) (kind 0)))) (hash "0ncsgwh5abbh1i9fpzk9gcwvq2b8ddnm2bzk0kacy5sy1k7cn1pi")))

(define-public crate-nintendo-lz-0.1 (crate (name "nintendo-lz") (vers "0.1.3") (deps (list (crate-dep (name "byteorder") (req "^1.2.3") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^2.32.0") (default-features #t) (kind 0)))) (hash "0ip6b863bjsvmf3jkmkchxm9j80b12fsd43ydhnvia51cp6rb2vn")))

(define-public crate-nintendo64-0.0.0 (crate (name "nintendo64") (vers "0.0.0") (hash "1llamknwdcxgklh677i13smfw50b9s6g1n4iq78s83wxiq0fwz11")))

(define-public crate-nintendo64-hal-0.0.0 (crate (name "nintendo64-hal") (vers "0.0.0") (hash "1zsdxd1v9d0r822jdqjwpa9223r1w0zi8g82wcxa267h6biszj5q")))

(define-public crate-nintendo64-pac-0.0.0 (crate (name "nintendo64-pac") (vers "0.0.0") (hash "1j9h9fp72ca64j4v1xjxcgz4f9ddk9hz9xi51xxi9d4amx4i7h9x")))

(define-public crate-nintendo64-pac-0.1 (crate (name "nintendo64-pac") (vers "0.1.0") (deps (list (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "tock-registers") (req "^0.8.1") (default-features #t) (kind 0)))) (hash "0sc15rgsnl2mm9zjzx8z6459lipppgqqwb75jlymnkmn4nmpljx0")))

(define-public crate-nintendo64-pac-0.2 (crate (name "nintendo64-pac") (vers "0.2.0") (deps (list (crate-dep (name "proc-bitfield") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "ux") (req "^0.1.5") (default-features #t) (kind 0)))) (hash "1fq4dn2pz3nd7jv0pnmqn474b4q8v0q0cpqrilv47s4f7qhzgvqw")))

