(define-module (crates-io ni c-) #:use-module (crates-io))

(define-public crate-nic-port-info-0.0.1 (crate (name "nic-port-info") (vers "0.0.1") (deps (list (crate-dep (name "libc") (req "^0.2.140") (default-features #t) (kind 0)) (crate-dep (name "nix") (req "^0.26.2") (features (quote ("ioctl"))) (default-features #t) (kind 0)))) (hash "15f6dmywwsjq1qqcd2w56pp8il3yzphydpzzdxhjykb3jxxkwmjs")))

(define-public crate-nic-port-info-0.0.2 (crate (name "nic-port-info") (vers "0.0.2-alphav1") (deps (list (crate-dep (name "libc") (req "^0.2.140") (default-features #t) (kind 0)) (crate-dep (name "nix") (req "^0.26.2") (features (quote ("ioctl"))) (default-features #t) (kind 0)))) (hash "17fy2yfb20222sp25lpm6nb9x6iixia2b8vigw91pscsaglciqq2")))

