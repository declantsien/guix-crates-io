(define-module (crates-io ni na) #:use-module (crates-io))

(define-public crate-nina-0.0.0 (crate (name "nina") (vers "0.0.0") (hash "1nw3qkkm1wz0mqhxhjd8p23xxqk5f4krapg6jhcb9mi15qm1vz3m")))

(define-public crate-nina-0.0.1 (crate (name "nina") (vers "0.0.1") (deps (list (crate-dep (name "curl") (req "^0.4.38") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.100") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.66") (default-features #t) (kind 0)))) (hash "02s8bwlyq04kyacfv9xl533v91mycswlrn98bl4qk4j5534z16dk")))

(define-public crate-nina-1 (crate (name "nina") (vers "1.0.0") (deps (list (crate-dep (name "curl") (req "^0.4.38") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.100") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.66") (default-features #t) (kind 0)))) (hash "0vrck0m5j67v7mcxr4z4sd682rk3wr7778z3azysby3nwmba4647")))

(define-public crate-ninat-0.1 (crate (name "ninat") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^2.33.1") (default-features #t) (kind 0)) (crate-dep (name "dns-lookup") (req "^1.0.3") (default-features #t) (kind 0)) (crate-dep (name "socks") (req "^0.3.2") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.3.15") (default-features #t) (kind 0)))) (hash "1cr2m0xqnd3p6n1i7nf2aph9fd0vmij1hy11w9621h0rr9ch1a5b")))

