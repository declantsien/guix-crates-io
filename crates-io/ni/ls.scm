(define-module (crates-io ni ls) #:use-module (crates-io))

(define-public crate-nilsimsa-0.1 (crate (name "nilsimsa") (vers "0.1.0") (deps (list (crate-dep (name "hex") (req "^0.4.2") (default-features #t) (kind 0)))) (hash "137vzxmbrzlrppg7q92j556ylickjs30781rj1fh9bbc63amsdm9")))

(define-public crate-nilsimsa-0.1 (crate (name "nilsimsa") (vers "0.1.1") (deps (list (crate-dep (name "hex") (req "^0.4.2") (default-features #t) (kind 0)))) (hash "1v6bgg7j8r8inxalxg3fnkzlgvmfpjhc0zcz3piwg6ac2ihv97xk")))

(define-public crate-nilsimsa-0.2 (crate (name "nilsimsa") (vers "0.2.0") (deps (list (crate-dep (name "hex") (req "^0.4.2") (default-features #t) (kind 0)))) (hash "1ps0a7kwsdyg73ls2d5h6c9wwcmgcb9kl22wnla4al13p7x50i6v")))

