(define-module (crates-io ni te) #:use-module (crates-io))

(define-public crate-nite2-sys-0.0.1 (crate (name "nite2-sys") (vers "0.0.1") (deps (list (crate-dep (name "openni2-sys") (req "^1.0") (default-features #t) (kind 0)))) (hash "09yns0pkdwanwgwblr9iwffn60g9mwb8n1gv496sqwm1x4gl3v4j") (links "nite2")))

(define-public crate-nite2-sys-0.1 (crate (name "nite2-sys") (vers "0.1.0") (deps (list (crate-dep (name "openni2-sys") (req "^1.0") (default-features #t) (kind 0)))) (hash "0ycwq7sx834mrlam6miah98f34xcsfvq42iq9ipmn97iwzsl7myb") (links "nite2")))

(define-public crate-nite2-sys-0.1 (crate (name "nite2-sys") (vers "0.1.1") (deps (list (crate-dep (name "openni2-sys") (req "^1.0") (default-features #t) (kind 0)))) (hash "0ngsilyjwzpl9lc9w36gbvzlaxdplza2450c1x5h9v8m9cmy557k") (links "nite2")))

(define-public crate-nite2-sys-0.2 (crate (name "nite2-sys") (vers "0.2.0") (deps (list (crate-dep (name "openni2-sys") (req "^1.0") (default-features #t) (kind 0)))) (hash "1lcfdb2b6zf93g5lakbf94g9faajz7i70wl99sivrzirgdm2w5h1") (links "nite2")))

(define-public crate-niter-0.0.0 (crate (name "niter") (vers "0.0.0") (hash "0agak0g5v4vqh6k9b48kx3j4267255ggknwpikq1df7i8d3fnhfd")))

(define-public crate-niterpack-0.1 (crate (name "niterpack") (vers "0.1.0-rc.0") (deps (list (crate-dep (name "clap") (req "^4.1") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "eyre") (req "^0.6") (default-features #t) (kind 0)) (crate-dep (name "futures") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4") (features (quote ("std"))) (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.11") (features (quote ("blocking"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "termcolor") (req "^1.2") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "toml") (req "^0.7") (default-features #t) (kind 0)) (crate-dep (name "url") (req "^2.3") (default-features #t) (kind 0)))) (hash "1nyidh8q6yj98cmba0zva5a0vvv72lxlv6gkp7mcl3qjhir5jjvg")))

(define-public crate-niterpack-0.1 (crate (name "niterpack") (vers "0.1.0-rc.1") (deps (list (crate-dep (name "clap") (req "^4.1") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "eyre") (req "^0.6") (default-features #t) (kind 0)) (crate-dep (name "lazy-regex") (req "^2.5") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4") (features (quote ("std"))) (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.11") (features (quote ("blocking"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "termcolor") (req "^1.2") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "toml") (req "^0.7") (default-features #t) (kind 0)) (crate-dep (name "url") (req "^2.3") (default-features #t) (kind 0)))) (hash "0a9x8x9a60h0psa0aqlmykhzwpqygywy34jg0vgqsdrg5w9had7a")))

(define-public crate-nitesh_minigrep-0.1 (crate (name "nitesh_minigrep") (vers "0.1.0") (hash "1blqibalacp478za5sgwxnhmy8480yb85fz4khh916a48g9hphaj")))

