(define-module (crates-io ni fp) #:use-module (crates-io))

(define-public crate-nifpga-0.1 (crate (name "nifpga") (vers "0.1.0") (deps (list (crate-dep (name "nifpga-sys") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "1kijnfyw5hj4qj3zls7wvvzckkgdqjy3sl8s1icacp14r0d121pg")))

(define-public crate-nifpga-0.1 (crate (name "nifpga") (vers "0.1.2") (deps (list (crate-dep (name "fehler") (req "^1.0.0") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2.71") (default-features #t) (kind 0)) (crate-dep (name "nifpga-sys") (req "^0.1.1") (default-features #t) (kind 0)) (crate-dep (name "nifpga-type-macro") (req "^0.1.1") (default-features #t) (kind 0)))) (hash "0fiwk1nfd2dwyx85wwrqcsdaj98rv5hqq01vvdiln6x4sj6g24rh") (features (quote (("static" "nifpga-sys/static") ("default"))))))

(define-public crate-nifpga-apigen-0.1 (crate (name "nifpga-apigen") (vers "0.1.0") (hash "1qd06q08r9s892z3k9cci8806alrkdhw7g3z15k75v9vs0c004g8")))

(define-public crate-nifpga-apigen-0.1 (crate (name "nifpga-apigen") (vers "0.1.1") (deps (list (crate-dep (name "clap") (req "^2.33.1") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.3.9") (default-features #t) (kind 0)))) (hash "1bxjaq9rsbjw9qfizbjpyabm03aprmpmg2h8ql181c000bj20f2n")))

(define-public crate-nifpga-apigen-0.1 (crate (name "nifpga-apigen") (vers "0.1.2") (deps (list (crate-dep (name "clap") (req "^2.33.1") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.3.9") (default-features #t) (kind 0)))) (hash "13xw9bqcfxfcj8a8z7vglvyi3hrp5mc9gbxz8jxqznzqhfa4d699")))

(define-public crate-nifpga-apigen-0.1 (crate (name "nifpga-apigen") (vers "0.1.3") (deps (list (crate-dep (name "clap") (req "^2.33.1") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.3.9") (default-features #t) (kind 0)))) (hash "03ihhw7hqyby82lrrc5ps6ifqb9476fhciyssw1c6khs2vbf27a4")))

(define-public crate-nifpga-apigen-0.1 (crate (name "nifpga-apigen") (vers "0.1.4") (deps (list (crate-dep (name "clap") (req "^2.33.1") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.3.9") (default-features #t) (kind 0)))) (hash "0l6gj8kfkmhs17r6wv65nxrz0si8mml7wj17xzilbj58i738vgml")))

(define-public crate-nifpga-apigen-0.1 (crate (name "nifpga-apigen") (vers "0.1.5") (deps (list (crate-dep (name "clap") (req "^2.33.1") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.3.9") (default-features #t) (kind 0)))) (hash "01zqhkqjsvd317nx5zr1h0wmyignb9icfcndb4v43ihzni3x42fc")))

(define-public crate-nifpga-dll-0.1 (crate (name "nifpga-dll") (vers "0.1.6") (deps (list (crate-dep (name "fehler") (req "^1.0.0") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2.71") (default-features #t) (kind 0)) (crate-dep (name "nifpga-dll-sys") (req "^0.1.6") (default-features #t) (kind 0)) (crate-dep (name "nifpga-dll-type-macro") (req "^0.1.6") (default-features #t) (kind 0)))) (hash "19siln0k2r91d2gr2pby9wq7k0qrqn54j1ijf59kwzsjabwi044g") (yanked #t)))

(define-public crate-nifpga-dll-0.1 (crate (name "nifpga-dll") (vers "0.1.7") (deps (list (crate-dep (name "fehler") (req "^1.0.0") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2.71") (default-features #t) (kind 0)) (crate-dep (name "nifpga-dll-sys") (req "^0.1.7") (default-features #t) (kind 0)) (crate-dep (name "nifpga-dll-type-macro") (req "^0.1.7") (default-features #t) (kind 0)))) (hash "1r0hl8swnm1b90iv6ibs09jrk74hcbyw86lkrpvgivjn7mjagq4n") (yanked #t)))

(define-public crate-nifpga-dll-0.2 (crate (name "nifpga-dll") (vers "0.2.0") (deps (list (crate-dep (name "fehler") (req "^1.0.0") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2.71") (default-features #t) (kind 0)) (crate-dep (name "nifpga-dll-sys") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "nifpga-dll-type-macro") (req "^0.2.0") (default-features #t) (kind 0)))) (hash "1rz994857wsyld347s6xc6nglni8iyymhgrlqjf23fmpcwpikqfn") (yanked #t)))

(define-public crate-nifpga-dll-0.3 (crate (name "nifpga-dll") (vers "0.3.0") (deps (list (crate-dep (name "fehler") (req "^1.0.0") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2.71") (default-features #t) (kind 0)) (crate-dep (name "nifpga-dll-sys") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "nifpga-dll-type-macro") (req "^0.3.0") (default-features #t) (kind 0)))) (hash "1nfm6cgydg0fskfw6k99kis69s256myvsj6x1q74pshikbkn5xkb")))

(define-public crate-nifpga-dll-sys-0.1 (crate (name "nifpga-dll-sys") (vers "0.1.6") (deps (list (crate-dep (name "libc") (req "^0.2.71") (default-features #t) (kind 0)))) (hash "0dwvk797wzg5m4xcfz40p0ja2i2ygf54c06l2ksgfvv3bdc63af5") (yanked #t) (links "NiFpga")))

(define-public crate-nifpga-dll-sys-0.1 (crate (name "nifpga-dll-sys") (vers "0.1.7") (deps (list (crate-dep (name "libc") (req "^0.2.71") (default-features #t) (kind 0)))) (hash "04scwz673rd0pdf3ry55wyyrml3j4lssc5nkarmhzg3zzhz24086") (yanked #t) (links "NiFpga")))

(define-public crate-nifpga-dll-sys-0.2 (crate (name "nifpga-dll-sys") (vers "0.2.0") (deps (list (crate-dep (name "libc") (req "^0.2.71") (default-features #t) (kind 0)))) (hash "1z6ncdsxrfvyf1xzjl7abknfaiis494mvrv0zza0q0dacrv3azc8") (yanked #t) (links "NiFpga")))

(define-public crate-nifpga-dll-sys-0.3 (crate (name "nifpga-dll-sys") (vers "0.3.0") (deps (list (crate-dep (name "libc") (req "^0.2.71") (default-features #t) (kind 0)))) (hash "1njcy3w33622rjfklxyh4aypdqgk76jfbh8gk1lhvczpappigkyj") (links "NiFpga")))

(define-public crate-nifpga-dll-type-macro-0.1 (crate (name "nifpga-dll-type-macro") (vers "0.1.6") (deps (list (crate-dep (name "proc-macro2") (req "^1.0.17") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.6") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.27") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "0jqn8qsa7yn7lbi136w89rlzwz6r98sfqdia1yym7yk3xb0dwd7c") (yanked #t)))

(define-public crate-nifpga-dll-type-macro-0.1 (crate (name "nifpga-dll-type-macro") (vers "0.1.7") (deps (list (crate-dep (name "proc-macro2") (req "^1.0.17") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.6") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.27") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "0gm5l6xif2n8xz3h3xq9iffb1ra5bcci6i47yhkncwca68l1as28") (yanked #t)))

(define-public crate-nifpga-dll-type-macro-0.2 (crate (name "nifpga-dll-type-macro") (vers "0.2.0") (deps (list (crate-dep (name "proc-macro2") (req "^1.0.17") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.6") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.27") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "1ds19v1bm9rxjg0vgi93r80n1x1vl8brl7dmwwmbq5d5yl22gkk0") (yanked #t)))

(define-public crate-nifpga-dll-type-macro-0.3 (crate (name "nifpga-dll-type-macro") (vers "0.3.0") (deps (list (crate-dep (name "proc-macro2") (req "^1.0.17") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.6") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.27") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "10w0m5mm6qh6r36gqry2nvs28alv52svs3n2lmba0ic7l51ky1xh")))

(define-public crate-nifpga-sys-0.1 (crate (name "nifpga-sys") (vers "0.1.0") (deps (list (crate-dep (name "libc") (req "^0.2.71") (default-features #t) (kind 0)))) (hash "0vj1y2mgzydvb3vz1glx9sh5iivn0rqmcmans91d721frk7qw4w9")))

(define-public crate-nifpga-sys-0.1 (crate (name "nifpga-sys") (vers "0.1.1") (deps (list (crate-dep (name "libc") (req "^0.2.71") (default-features #t) (kind 0)))) (hash "0rj0ql30jr1n61m05c5qpdprxzhld7j76a8q8g7hakdn9i54qkla") (features (quote (("static") ("default"))))))

(define-public crate-nifpga-type-macro-0.1 (crate (name "nifpga-type-macro") (vers "0.1.0") (deps (list (crate-dep (name "proc-macro2") (req "^1.0.17") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.6") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.27") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "1h1783vzc2vd9gv9qfafjgg77nx1phr4x0hcb2fnflxwd2pi71hi")))

(define-public crate-nifpga-type-macro-0.1 (crate (name "nifpga-type-macro") (vers "0.1.1") (deps (list (crate-dep (name "proc-macro2") (req "^1.0.17") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.6") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.27") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "1g1nzsxyzvcrnn0pp451zz5p43f354j1camgw4qi36rw3ycp7k3n")))

