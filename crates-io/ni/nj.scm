(define-module (crates-io ni nj) #:use-module (crates-io))

(define-public crate-ninja-0.0.1 (crate (name "ninja") (vers "0.0.1") (hash "0krkidx9z0rqf040h2pkh1xcg7yykyh76bc5xn30x2h4fwi5zvkj")))

(define-public crate-ninja-build-0.0.1 (crate (name "ninja-build") (vers "0.0.1") (deps (list (crate-dep (name "cc") (req "^1") (default-features #t) (kind 1)) (crate-dep (name "cfg-if") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^2") (default-features #t) (kind 0)) (crate-dep (name "errno") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "kernel32-sys") (req "^0.2") (default-features #t) (target "cfg(windows)") (kind 0)) (crate-dep (name "lazy_static") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "libc-spawn") (req "^0.0.1") (default-features #t) (target "cfg(unix)") (kind 0)) (crate-dep (name "libc-stdhandle") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "murmurhash64") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "nom") (req "^3") (default-features #t) (kind 0)) (crate-dep (name "num_cpus") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "widestring") (req "^0.2") (default-features #t) (target "cfg(windows)") (kind 0)) (crate-dep (name "winapi") (req "^0.2") (default-features #t) (target "cfg(windows)") (kind 0)) (crate-dep (name "wstr") (req "^0.2") (features (quote ("widestring"))) (default-features #t) (target "cfg(windows)") (kind 0)))) (hash "0wn8rw691kra29d49pv8lx5v7b6bv91iqllifnjr4pf5sf75kiyi")))

(define-public crate-ninja-build-syntax-0.0.1 (crate (name "ninja-build-syntax") (vers "0.0.1") (deps (list (crate-dep (name "bstr") (req "^0.2") (default-features #t) (kind 2)) (crate-dep (name "criterion") (req "^0.2") (default-features #t) (kind 2)) (crate-dep (name "crossbeam") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "nom") (req "^5.0") (default-features #t) (kind 0)) (crate-dep (name "tar") (req "^0.4") (default-features #t) (kind 2)) (crate-dep (name "xz2") (req "^0.1") (default-features #t) (kind 2)))) (hash "0ll3lip7r227bb1mg6x9mj5rx9izl7z9ralrls7lqxzi1g738adp")))

(define-public crate-ninja-build-syntax-0.0.2 (crate (name "ninja-build-syntax") (vers "0.0.2") (deps (list (crate-dep (name "bstr") (req "^0.2") (default-features #t) (kind 2)) (crate-dep (name "criterion") (req "^0.2") (default-features #t) (kind 2)) (crate-dep (name "crossbeam") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "nom") (req "^5.0") (default-features #t) (kind 0)) (crate-dep (name "tar") (req "^0.4") (default-features #t) (kind 2)) (crate-dep (name "xz2") (req "^0.1") (default-features #t) (kind 2)))) (hash "1068igw9wm1q1vsvn02klif7vfkyfgv39p8mh0c2lv3isnqcirp5")))

(define-public crate-ninja-builder-0.1 (crate (name "ninja-builder") (vers "0.1.0") (deps (list (crate-dep (name "anyhow") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "async-trait") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "console") (req "^0.11") (kind 0)) (crate-dep (name "futures") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "insta") (req "^0.16.0") (default-features #t) (kind 2)) (crate-dep (name "ninja-metrics") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "ninja-parse") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "petgraph") (req "^0.5") (default-features #t) (kind 0)) (crate-dep (name "proptest") (req "^0.10.0") (default-features #t) (kind 2)) (crate-dep (name "thiserror") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^0.2") (features (quote ("sync" "rt-core" "process" "rt-util"))) (kind 0)))) (hash "10xrmp6g2bxnc54x9jnancbqhfbds7l0zppygaxiimjbg5cvyf9y")))

(define-public crate-ninja-files-0.1 (crate (name "ninja-files") (vers "0.1.0") (deps (list (crate-dep (name "ninja-files-cookie") (req "^0.1.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "ninja-files-data") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "19harv0xr4mbfkc7srwj6fy6ynkzz2q06bhnf7bwyp4s8vxx4mmc") (features (quote (("default" "format")))) (v 2) (features2 (quote (("format" "dep:ninja-files-cookie"))))))

(define-public crate-ninja-files-0.2 (crate (name "ninja-files") (vers "0.2.0") (deps (list (crate-dep (name "ninja-files-cookie") (req "^0.1.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "ninja-files-data") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "1bn9xbd47bkpjhdlmvd36n9lfwqq5sjqr53y50a0cwqqiv9sf12l") (features (quote (("default" "format")))) (v 2) (features2 (quote (("format" "dep:ninja-files-cookie"))))))

(define-public crate-ninja-files-cargo-0.1 (crate (name "ninja-files-cargo") (vers "0.1.0") (deps (list (crate-dep (name "ninja-files") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "1qmrwbpzvdry3wi5x291dzs4kv8ifmag1dh1v0pjijkxkh9a6jxs")))

(define-public crate-ninja-files-cargo-0.2 (crate (name "ninja-files-cargo") (vers "0.2.0") (deps (list (crate-dep (name "ninja-files-data") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "1jvysy64dfm9hmdjxgsh69nhxigln7ryr63vx70ds9qjk2pdq8qa")))

(define-public crate-ninja-files-cookie-0.1 (crate (name "ninja-files-cookie") (vers "0.1.0") (deps (list (crate-dep (name "cookie-factory") (req "^0.3.2") (default-features #t) (kind 0)) (crate-dep (name "indoc") (req "^2") (default-features #t) (kind 2)) (crate-dep (name "ninja-files-data") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "os_str_bytes") (req "^6.5") (default-features #t) (kind 0)))) (hash "15cv6d0zgccg681pvbgr7d47r9h8jf4hy29kjdhrhg7ari165325")))

(define-public crate-ninja-files-cookie2-0.1 (crate (name "ninja-files-cookie2") (vers "0.1.0") (deps (list (crate-dep (name "cookie-factory") (req "^0.3.2") (default-features #t) (kind 0)) (crate-dep (name "indoc") (req "^2") (default-features #t) (kind 2)) (crate-dep (name "ninja-files-data") (req "^0.1.0") (default-features #t) (kind 0) (package "ninja-files-data2")) (crate-dep (name "os_str_bytes") (req "^6.5") (default-features #t) (kind 0)))) (hash "07qcin7j1zrb64vc6hpwqcvk142h44irf0yznsphixn2wafl6pzf")))

(define-public crate-ninja-files-coreutils-0.1 (crate (name "ninja-files-coreutils") (vers "0.1.0") (deps (list (crate-dep (name "ninja-files-data") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "0344ivzn2bfqfs29j5nnzndziv1a6zv1bmfb8rrywnfjq1lgl0f3")))

(define-public crate-ninja-files-coreutils-0.2 (crate (name "ninja-files-coreutils") (vers "0.2.0") (deps (list (crate-dep (name "ninja-files-data") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "1ja1ccy0j4lrzl73m33qpwfbyb9hfs9vjv5xbxkqv31jx7spk3aq")))

(define-public crate-ninja-files-data-0.1 (crate (name "ninja-files-data") (vers "0.1.0") (deps (list (crate-dep (name "bumpalo") (req "^3") (features (quote ("collections"))) (default-features #t) (kind 0)) (crate-dep (name "camino") (req "^1.1.4") (default-features #t) (kind 0)))) (hash "15wdslnyjiajdcicz8i50mp70rx1wz0fygqs00gkbajdmmll930q")))

(define-public crate-ninja-files-data2-0.1 (crate (name "ninja-files-data2") (vers "0.1.0") (deps (list (crate-dep (name "bumpalo") (req "^3") (features (quote ("collections"))) (default-features #t) (kind 0)) (crate-dep (name "camino") (req "^1.1") (default-features #t) (kind 0)))) (hash "0h8rnnsj0my4qbmhy3lj3654sskq5flbwwrxydmqd0vipqzlw16b")))

(define-public crate-ninja-files-kubectl-0.1 (crate (name "ninja-files-kubectl") (vers "0.1.0") (deps (list (crate-dep (name "ninja-files") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "14zyhqbcdjkcyvy2x5gvdpnzq65pwxxziln4paw51y630lpk039x")))

(define-public crate-ninja-files-kubectl-0.2 (crate (name "ninja-files-kubectl") (vers "0.2.0") (deps (list (crate-dep (name "ninja-files-data") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "05vvhlzv18g2cvi9mc90j0vdvcd94als3wgr5g2138bb7skfhbbw")))

(define-public crate-ninja-files-kustomize-0.1 (crate (name "ninja-files-kustomize") (vers "0.1.0") (deps (list (crate-dep (name "ninja-files") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "1bfkz29xn3914gnyb5jy5509z2ym1klh17ffrpy23ffczap221pb")))

(define-public crate-ninja-files-kustomize-0.2 (crate (name "ninja-files-kustomize") (vers "0.2.0") (deps (list (crate-dep (name "ninja-files-data") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "1c99gbdry09336nq0zl3n4lhm82y7j80cqh1666dd5k2l07m1kmi")))

(define-public crate-ninja-files-mdbook-0.1 (crate (name "ninja-files-mdbook") (vers "0.1.0") (deps (list (crate-dep (name "ninja-files-data") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "1z6xmrbby7wi8pmkz5ffvsl3qm00hdsyjhx4g8n66b6d8gp3kqrz")))

(define-public crate-ninja-files-passwordstore-0.1 (crate (name "ninja-files-passwordstore") (vers "0.1.0") (deps (list (crate-dep (name "camino") (req "^1.1.4") (default-features #t) (kind 0)) (crate-dep (name "ninja-files") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "05ifyn8vgqr2z926vsgsi0asns2yk3h4z3mchjyn58mbzlkchi1y")))

(define-public crate-ninja-files-passwordstore-0.2 (crate (name "ninja-files-passwordstore") (vers "0.2.0") (deps (list (crate-dep (name "ninja-files-data") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "0wnpm8clsl1rxzbc75l9c38p5faajc0mg9gq5aglkbykkwcc5jq8")))

(define-public crate-ninja-files-talosctl-0.1 (crate (name "ninja-files-talosctl") (vers "0.1.0") (deps (list (crate-dep (name "ninja-files") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "1lckzdz673cppx7ps2q6gs4xhh5w3mhnr645qzgmmk57r0b90vh5")))

(define-public crate-ninja-files-talosctl-0.2 (crate (name "ninja-files-talosctl") (vers "0.2.0") (deps (list (crate-dep (name "ninja-files-data") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "0giq3rj3h31wg4mgqwvpzlfc5y9iv9l894wcfkz80a8vg4aqyfgs")))

(define-public crate-ninja-files-tar-0.1 (crate (name "ninja-files-tar") (vers "0.1.0") (deps (list (crate-dep (name "ninja-files-data") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "13y138087r2d27y12jagla1m19fl1058bashn5j7sqpxfl5hyjkn")))

(define-public crate-ninja-files2-0.2 (crate (name "ninja-files2") (vers "0.2.0") (deps (list (crate-dep (name "ninja-files-cookie") (req "^0.1.0") (optional #t) (default-features #t) (kind 0) (package "ninja-files-cookie2")) (crate-dep (name "ninja-files-data") (req "^0.1.0") (default-features #t) (kind 0) (package "ninja-files-data2")))) (hash "1nppnsl1hwf2cv7zyb5vwbxaw72pn55s4nsqd9ra0j567mpyqmxz") (features (quote (("default" "format")))) (v 2) (features2 (quote (("format" "dep:ninja-files-cookie"))))))

(define-public crate-ninja-metrics-0.1 (crate (name "ninja-metrics") (vers "0.1.0") (hash "1hp7prr3mv7ii2n3aaw5iwkxqhp7nc5i31i7mbbrzqdx9903my3n")))

(define-public crate-ninja-metrics-0.2 (crate (name "ninja-metrics") (vers "0.2.0") (hash "0f32l2k6jn3gks4bffhf3wsfy6ayr9b8d7b6n90sw949q17hwngp")))

(define-public crate-ninja-parse-0.1 (crate (name "ninja-parse") (vers "0.1.0") (deps (list (crate-dep (name "globwalk") (req "^0.8") (default-features #t) (kind 2)) (crate-dep (name "insta") (req "^0.16.0") (default-features #t) (kind 2)) (crate-dep (name "ninja-metrics") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0") (default-features #t) (kind 0)))) (hash "0spbm5dbkgayhrzyx9mr7lkn8sa9m9xz4wai1vvv5d0x2xx0d0cm")))

(define-public crate-ninja-rs-0.1 (crate (name "ninja-rs") (vers "0.1.0") (hash "05bighi687z7ch1p30wxhrxqdx64rri4zgbi7793rxmcrj9px6ng")))

(define-public crate-ninja-sys-0.0.0 (crate (name "ninja-sys") (vers "0.0.0") (hash "0mnxjddnmgjd6h25zp3gcplan4vdjdpwl62iwyp3121ac8r6xb0d")))

(define-public crate-ninja-writer-0.1 (crate (name "ninja-writer") (vers "0.1.0") (hash "0xm5xhfa6cd3s40pl9lvf5snvl609nls1jx8zpxc1h947ffcjp39") (features (quote (("std") ("default" "std")))) (yanked #t)))

(define-public crate-ninja-writer-0.1 (crate (name "ninja-writer") (vers "0.1.1") (hash "1ry8f5l33jkk63l5h31lkysbfjrpq4985ir1p3nz4v74maa2fk53") (features (quote (("std") ("default" "std")))) (yanked #t)))

(define-public crate-ninja-writer-0.1 (crate (name "ninja-writer") (vers "0.1.2") (hash "14chwnfx4176klcv7jbj4igygjgswjaia8qf3vfss4injvv376zs") (features (quote (("std") ("default" "std"))))))

(define-public crate-ninja-writer-0.2 (crate (name "ninja-writer") (vers "0.2.0") (hash "0r0slr6434wahdq8zaldpv0ylwpc4vd24qz0id9ykjhxr75pjiwf") (features (quote (("thread-safe" "std") ("std") ("default" "std"))))))

(define-public crate-ninja_syntax-0.1 (crate (name "ninja_syntax") (vers "0.1.0") (deps (list (crate-dep (name "assert_cmd") (req "^2.0") (default-features #t) (kind 2)) (crate-dep (name "predicates") (req "^2.1") (default-features #t) (kind 2)) (crate-dep (name "tempdir") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "textwrap") (req "^0.16.0") (default-features #t) (kind 0)))) (hash "099yjp6s6rssik8h5hgh956fkl2xshaf3r4294ac8pz7pzw47nf5")))

(define-public crate-ninja_syntax-0.1 (crate (name "ninja_syntax") (vers "0.1.1") (deps (list (crate-dep (name "assert_cmd") (req "^2.0") (default-features #t) (kind 2)) (crate-dep (name "predicates") (req "^2.1") (default-features #t) (kind 2)) (crate-dep (name "tempdir") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "textwrap") (req "^0.16.0") (default-features #t) (kind 0)))) (hash "139nmv38w5wwx85cxfgib2vb1sxkvk4bpkzvxl94d8026blnjrw6")))

(define-public crate-ninjabook-0.1 (crate (name "ninjabook") (vers "0.1.0") (deps (list (crate-dep (name "criterion") (req "^0.5.1") (features (quote ("html_reports"))) (default-features #t) (kind 0)) (crate-dep (name "csv") (req "^1.3.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.197") (default-features #t) (kind 0)))) (hash "0n1sr5a15kkq4gxkbh2xgjggfv7nnd8gy6iga67d2h2afh65fpsc")))

(define-public crate-ninjabook-0.1 (crate (name "ninjabook") (vers "0.1.1") (deps (list (crate-dep (name "criterion") (req "^0.5.1") (features (quote ("html_reports"))) (default-features #t) (kind 0)) (crate-dep (name "csv") (req "^1.3.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.197") (default-features #t) (kind 0)))) (hash "0mgbwn8647gwnvqf2cprnbr2392k2yw8mnvspsq7r3rn2izd45y3")))

(define-public crate-ninjabook-0.1 (crate (name "ninjabook") (vers "0.1.2") (deps (list (crate-dep (name "criterion") (req "^0.5.1") (features (quote ("html_reports"))) (default-features #t) (kind 0)) (crate-dep (name "csv") (req "^1.3.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.197") (default-features #t) (kind 0)))) (hash "0iivycbqgmr26sm7c5x45fsda8d9zxzmljicrpcd40jx2py5zr84")))

(define-public crate-ninjabook-0.1 (crate (name "ninjabook") (vers "0.1.3") (deps (list (crate-dep (name "criterion") (req "^0.5.1") (features (quote ("html_reports"))) (default-features #t) (kind 0)) (crate-dep (name "csv") (req "^1.3.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.197") (default-features #t) (kind 0)))) (hash "1mzs3xsimy3rds9byh32bfdz781bdimw6vc2gzy44hlc9l0bidz5")))

(define-public crate-ninjabook-0.1 (crate (name "ninjabook") (vers "0.1.5") (deps (list (crate-dep (name "criterion") (req "^0.5.1") (features (quote ("html_reports"))) (default-features #t) (kind 0)) (crate-dep (name "csv") (req "^1.3.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.197") (default-features #t) (kind 0)))) (hash "1qib0m9l108k4ar55dzrcvglkzldaxsy5hd32hpmss79vsig3pbz")))

(define-public crate-ninjabook-0.1 (crate (name "ninjabook") (vers "0.1.6") (deps (list (crate-dep (name "criterion") (req "^0.5.1") (features (quote ("html_reports"))) (default-features #t) (kind 0)) (crate-dep (name "csv") (req "^1.3.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.197") (default-features #t) (kind 0)))) (hash "01zkyf3pag99zkg3hicga4zlf7bsnk6c27qrnpiafpzxrw752n6j")))

(define-public crate-ninjars-0.1 (crate (name "ninjars") (vers "0.1.0") (deps (list (crate-dep (name "anyhow") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "ninja-builder") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "ninja-metrics") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "ninja-parse") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "num_cpus") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "once_cell") (req "^1.4") (default-features #t) (kind 0)) (crate-dep (name "petgraph") (req "^0.5") (default-features #t) (kind 0)) (crate-dep (name "pico-args") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0") (default-features #t) (kind 0)))) (hash "1w8vr2145zpq72rkk7big6k81hnhlp2z4j3zb7x2hfwarmf203h0")))

(define-public crate-ninjen-0.0.0 (crate (name "ninjen") (vers "0.0.0") (hash "17324j1vmxrh56bg1l2izgmb9lascq6yhn3609ccvkq1d2zvr6j9")))

(define-public crate-ninjen-0.1 (crate (name "ninjen") (vers "0.1.0") (deps (list (crate-dep (name "insta") (req "^1.5.3") (default-features #t) (kind 2)) (crate-dep (name "literally") (req "^0.1.3") (default-features #t) (kind 2)))) (hash "0blg1kkdx4l0dizxpc88x57r2xfs7zxr76bmns3327grbpkzr3qp")))

(define-public crate-ninjen-0.1 (crate (name "ninjen") (vers "0.1.1") (deps (list (crate-dep (name "insta") (req "^1.5.3") (default-features #t) (kind 2)) (crate-dep (name "literally") (req "^0.1.3") (default-features #t) (kind 2)))) (hash "1k1sf493f9145jaqlm5q2kds8nlsriy3v78d4zb75c6hp6r16pvy")))

(define-public crate-ninjify-0.1 (crate (name "ninjify") (vers "0.1.4") (deps (list (crate-dep (name "ron") (req "^0.5") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_any") (req "^0.5") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_yaml") (req "^0.8") (default-features #t) (kind 0)))) (hash "0lfzml04s8cjas7gwkn32mjr689jhky6cjklqrbxpk6ihikk6rz7")))

(define-public crate-ninjify-0.1 (crate (name "ninjify") (vers "0.1.5") (deps (list (crate-dep (name "clap") (req "^2.33") (default-features #t) (kind 0)) (crate-dep (name "ron") (req "^0.5") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_any") (req "^0.5") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_yaml") (req "^0.8") (default-features #t) (kind 0)))) (hash "0llcx5fgxf1c8ghp25h571qp3hzvpfqzhp0d6b38lra3kiwzkbzy")))

(define-public crate-ninjify-0.1 (crate (name "ninjify") (vers "0.1.6") (deps (list (crate-dep (name "clap") (req "^2.33") (default-features #t) (kind 0)) (crate-dep (name "ron") (req "^0.5") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_any") (req "^0.5") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_yaml") (req "^0.8") (default-features #t) (kind 0)))) (hash "0xakc6b8b1vrmc4wb2j72x6n7rc6r2wgn64pw2xq1554mnlbhm0h")))

(define-public crate-ninjify-0.1 (crate (name "ninjify") (vers "0.1.7") (deps (list (crate-dep (name "clap") (req "^2.33") (default-features #t) (kind 0)) (crate-dep (name "ron") (req "^0.5") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_any") (req "^0.5") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_yaml") (req "^0.8") (default-features #t) (kind 0)))) (hash "06ph6hckqag451dg47i4mw9mv2ixypchyrkrp2zp4fsv2gzjsx6f")))

