(define-module (crates-io ni sl) #:use-module (crates-io))

(define-public crate-nislib-0.1 (crate (name "nislib") (vers "0.1.0") (deps (list (crate-dep (name "log") (req "^0.4.20") (default-features #t) (kind 0)) (crate-dep (name "palserializer") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)) (crate-dep (name "rsa") (req "^0.9.2") (default-features #t) (kind 0)) (crate-dep (name "spki") (req "^0.7.2") (default-features #t) (kind 0)) (crate-dep (name "strum") (req "^0.25.0") (default-features #t) (kind 0)) (crate-dep (name "strum_macros") (req "^0.25.3") (default-features #t) (kind 0)))) (hash "10nizzzxjxpv4c2arw89yprffs797wgf1k7js104shp2m1lhiyy5")))

