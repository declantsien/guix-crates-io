(define-module (crates-io ni t-) #:use-module (crates-io))

(define-public crate-nit-rs-0.1 (crate (name "nit-rs") (vers "0.1.0") (deps (list (crate-dep (name "colored") (req "^2.0.0") (default-features #t) (kind 0)) (crate-dep (name "toml") (req "^0.5") (default-features #t) (kind 0)))) (hash "08n49zfh8qkjbwisrjay32vj92m5ffdfcd68g0x0iqvznlapwqxs")))

