(define-module (crates-io ni b-) #:use-module (crates-io))

(define-public crate-nib-cli-0.0.1 (crate (name "nib-cli") (vers "0.0.1") (deps (list (crate-dep (name "cargo-husky") (req "^1") (features (quote ("user-hooks"))) (kind 2)) (crate-dep (name "nib") (req "^0.0.6") (default-features #t) (kind 0)) (crate-dep (name "toml") (req "^0.5") (default-features #t) (kind 0)))) (hash "1gnxlm5mr7ib6lxq2giw3s13sag0mgmzlmp8x9jhlqgg18y7dy0q")))

(define-public crate-nib-cli-0.0.2 (crate (name "nib-cli") (vers "0.0.2") (deps (list (crate-dep (name "cargo-husky") (req "^1") (features (quote ("user-hooks"))) (kind 2)) (crate-dep (name "nib") (req "^0.0.7") (default-features #t) (kind 0)) (crate-dep (name "toml") (req "^0.5") (default-features #t) (kind 0)))) (hash "1dpcjyjm19yjhn1nrzcpfvgqmfbbks1hw0fyc8h4yhsdnjgqqnq1")))

(define-public crate-nib-cli-0.0.3 (crate (name "nib-cli") (vers "0.0.3") (deps (list (crate-dep (name "cargo-husky") (req "^1") (features (quote ("user-hooks"))) (kind 2)) (crate-dep (name "nib") (req "^0.0.8") (default-features #t) (kind 0)) (crate-dep (name "toml") (req "^0.5") (default-features #t) (kind 0)))) (hash "0nwkp37wlkjcn2nvb86kw050rw3fm8zxfzrlj8xxalln0l509bds")))

(define-public crate-nib-server-0.0.1 (crate (name "nib-server") (vers "0.0.1") (deps (list (crate-dep (name "futures") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "hyper") (req "0.13.*") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "0.2.*") (features (quote ("fs" "macros" "stream"))) (kind 0)) (crate-dep (name "tokio-util") (req "^0.3.1") (features (quote ("codec"))) (kind 0)))) (hash "0zg22a8sl17mg3jrxr3kahaz16icvw4r7cqxypw0wyz66p43bjvm")))

(define-public crate-nib-server-0.0.2 (crate (name "nib-server") (vers "0.0.2") (deps (list (crate-dep (name "futures") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "hyper") (req "0.14.*") (features (quote ("http1" "runtime" "server" "stream"))) (kind 0)) (crate-dep (name "tokio") (req "1.7.*") (features (quote ("fs" "macros" "rt" "rt-multi-thread"))) (kind 0)) (crate-dep (name "tokio-util") (req "^0.6.7") (features (quote ("codec"))) (kind 0)))) (hash "03s408d6nqr6qaqn1w4kkjjg8wpfy2h590hsvwgsnq9rinn10xh8")))

