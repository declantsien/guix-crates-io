(define-module (crates-io ni bb) #:use-module (crates-io))

(define-public crate-nibb-0.0.1 (crate (name "nibb") (vers "0.0.1") (hash "0hhfld4p0dbmgfkjw531m745mxgd0g4qdqxs3dd41fyr4klsh334")))

(define-public crate-nibble-0.1 (crate (name "nibble") (vers "0.1.0") (deps (list (crate-dep (name "arrayvec") (req "^0.4.2") (kind 0)) (crate-dep (name "cfg-if") (req "^0.1.2") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.1.40") (default-features #t) (kind 0)))) (hash "14ypdq06g8r7jb3h90qq7nlck134j26jvjh4bbdp44gqrzvzq2y1") (features (quote (("std" "arrayvec/std") ("default" "std"))))))

(define-public crate-nibble8-0.1 (crate (name "nibble8") (vers "0.1.0") (deps (list (crate-dep (name "rand") (req "^0.8.4") (default-features #t) (kind 0)) (crate-dep (name "sdl2") (req "^0.34.5") (default-features #t) (kind 0)))) (hash "0sc5brf4kfcxyc21w9gyqng1gvc4byxpchh5wc5af9f4947g1arh") (yanked #t)))

(define-public crate-nibble8-0.1 (crate (name "nibble8") (vers "0.1.1") (deps (list (crate-dep (name "rand") (req "^0.8.4") (default-features #t) (kind 0)) (crate-dep (name "sdl2") (req "^0.34.5") (default-features #t) (kind 0)))) (hash "0wwwb0d7fpym4zhjci36s31bfznp4cwra4mxjs5i9p8sfib0hsqk") (yanked #t)))

(define-public crate-nibble8-0.1 (crate (name "nibble8") (vers "0.1.2") (deps (list (crate-dep (name "rand") (req "^0.8.4") (default-features #t) (kind 0)) (crate-dep (name "sdl2") (req "^0.34.5") (default-features #t) (kind 0)))) (hash "1zjr6i6pkm4rnbdhjmmpkm9dckyciwizkpji3gfmg92gglqj68c8")))

(define-public crate-nibble8-0.1 (crate (name "nibble8") (vers "0.1.3") (deps (list (crate-dep (name "rand") (req "^0.8.4") (default-features #t) (kind 0)) (crate-dep (name "sdl2") (req "^0.34.5") (default-features #t) (kind 0)))) (hash "11mw6zcp5rl06rhj8k3ga29xf59zs57a8m00b9mjwslykivx3ir0")))

(define-public crate-nibble8-0.1 (crate (name "nibble8") (vers "0.1.4") (deps (list (crate-dep (name "rand") (req "^0.8.4") (default-features #t) (kind 0)) (crate-dep (name "sdl2") (req "^0.34.5") (default-features #t) (kind 0)))) (hash "0rlmx4h1q7bbxlhqqjhxmfzc35y27aq3lbdfxn07710kk7vbcvrq")))

(define-public crate-nibble8-0.2 (crate (name "nibble8") (vers "0.2.0") (deps (list (crate-dep (name "rand") (req "^0.8.4") (default-features #t) (kind 0)) (crate-dep (name "sdl2") (req "^0.34.5") (default-features #t) (kind 0)))) (hash "10bw9r4xypkkbbvgq8va1w11105h3304bzw1npd4fb1svlfflvxi")))

(define-public crate-nibble_vec-0.0.1 (crate (name "nibble_vec") (vers "0.0.1") (hash "0q826anwds6s5fpr8k0r50532wxkf97yrk51x984pr9i159lib90")))

(define-public crate-nibble_vec-0.0.2 (crate (name "nibble_vec") (vers "0.0.2") (hash "1dbk1b1fkpap248dyllz76grcdl4vfvj785pb1xn1vrmvb5w17f4")))

(define-public crate-nibble_vec-0.0.3 (crate (name "nibble_vec") (vers "0.0.3") (hash "17px618yya2fknjyh1hz880giyqd11yzvkhpp7rcaw2cg8ipirk2")))

(define-public crate-nibble_vec-0.0.4 (crate (name "nibble_vec") (vers "0.0.4") (hash "1ykgfnksyvw811fz51jfnh13s77gn9wq1c2ds3s37q5wnhypzmy8")))

(define-public crate-nibble_vec-0.0.5 (crate (name "nibble_vec") (vers "0.0.5") (deps (list (crate-dep (name "criterion") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "smallvec") (req "^1.0") (default-features #t) (kind 0)))) (hash "0k6y8a4i4ny5bwpgqymjhvnfw3fnxn5apd95zw5y6khqzw2i0m94") (yanked #t)))

(define-public crate-nibble_vec-0.1 (crate (name "nibble_vec") (vers "0.1.0") (deps (list (crate-dep (name "criterion") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "smallvec") (req "^1.0") (default-features #t) (kind 0)))) (hash "0hsdp3s724s30hkqz74ky6sqnadhp2xwcj1n1hzy4vzkz4yxi9bp")))

(define-public crate-nibbler-0.1 (crate (name "nibbler") (vers "0.1.1") (hash "16s2iipvs23nr4hqpy19p3q3645c8vvsv009rpfw7fpxky2nhd4q") (yanked #t)))

(define-public crate-nibbler-0.1 (crate (name "nibbler") (vers "0.1.2") (hash "11pl4b8p6gd2f6z0pqh0k7ipqs4k18j14j53s7wp5bb76g40cbsx")))

(define-public crate-nibbler-0.1 (crate (name "nibbler") (vers "0.1.3") (hash "1vvhdlk5hq28qvgvwsyr8gmds06c5aqw01n2mdqakm9wzhkl1q4d")))

(define-public crate-nibbler-0.1 (crate (name "nibbler") (vers "0.1.8") (hash "1ghhlk1i3fwa1wgp1gz001nwp8x1wqx4z2dwfsfxn93dcijcakkg")))

(define-public crate-nibbler-0.2 (crate (name "nibbler") (vers "0.2.1") (hash "1688vi8rid6350487b5h1icq3xwj084m0jy2gnm7y7plzkwvd053")))

(define-public crate-nibbler-0.2 (crate (name "nibbler") (vers "0.2.3") (hash "1hcvclq2a6fpwx2ya367wrvmfag099q49zpac0p9xzsj5g6m6jrs")))

(define-public crate-nibbletree-0.1 (crate (name "nibbletree") (vers "0.1.0") (deps (list (crate-dep (name "bitvec") (req "^1.0.1") (default-features #t) (kind 0)) (crate-dep (name "ip_network") (req "^0.4.1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "ipnet") (req "^2.7.1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 2)) (crate-dep (name "rand_xoshiro") (req "^0.6.0") (default-features #t) (kind 2)) (crate-dep (name "rstest") (req "^0.16.0") (kind 2)) (crate-dep (name "rstest_reuse") (req "^0.5.0") (kind 2)) (crate-dep (name "thin-vec") (req "^0.2.10") (default-features #t) (kind 0)))) (hash "0s564q4cinhxd46fw1np1z5rv1sq40wzw7yd8w4sv0v1f4g6ind2")))

(define-public crate-nibbletree-0.2 (crate (name "nibbletree") (vers "0.2.0") (deps (list (crate-dep (name "bitvec") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "ip_network") (req "^0.4") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "ipnet") (req "^2.9") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8") (default-features #t) (kind 2)) (crate-dep (name "rand_xoshiro") (req "^0.6") (default-features #t) (kind 2)) (crate-dep (name "rstest") (req "^0.18") (kind 2)) (crate-dep (name "rstest_reuse") (req "^0.6") (kind 2)) (crate-dep (name "thin-vec") (req "^0.2") (default-features #t) (kind 0)))) (hash "0xy0zxhkszpml3djc8ls2v9kcfl2513jw74s4rrmdcf3vny9z04y")))

(define-public crate-nibbstack-0.0.1 (crate (name "nibbstack") (vers "0.0.1") (hash "1pzvgvanh8yl4ksnpzdd5nrs9a7q5r0yar5n1a61r96fp4rwrgwi")))

