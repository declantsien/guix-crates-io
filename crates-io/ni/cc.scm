(define-module (crates-io ni cc) #:use-module (crates-io))

(define-public crate-niccalc-0.9 (crate (name "niccalc") (vers "0.9.0") (deps (list (crate-dep (name "fltk") (req "^0.13") (default-features #t) (kind 0)))) (hash "1akckcg3m3iq21jl1dlahvqva2spg87hjfqql31hc9w237lcsh53")))

(define-public crate-niccalc-0.9 (crate (name "niccalc") (vers "0.9.1") (deps (list (crate-dep (name "fltk") (req "^0.13") (default-features #t) (kind 0)))) (hash "02lsswkahym6ls6h6yw3d5kafmhf0ld13i8wpf0a6xjcfldqw8mh")))

(define-public crate-niccalc-0.9 (crate (name "niccalc") (vers "0.9.2") (deps (list (crate-dep (name "fltk") (req "^0.13") (default-features #t) (kind 0)))) (hash "1v1ymbmja95whrwic32qj34css2iayac9zjcfba2csrmbw1zd10g")))

(define-public crate-niccalc-0.9 (crate (name "niccalc") (vers "0.9.3") (deps (list (crate-dep (name "fltk") (req "^0.13") (default-features #t) (kind 0)))) (hash "1jmz4j9v9mibg2m5zz8ysqqh5rf46sci70cjc7qzllycfg6gpz17")))

(define-public crate-niccalc-0.9 (crate (name "niccalc") (vers "0.9.4") (deps (list (crate-dep (name "fltk") (req "^0.13") (default-features #t) (kind 0)))) (hash "1crv8kdfigcksjsj5h73x1nxil6ja0bsi2x21mx83iz7d0cl2qwv")))

(define-public crate-niccalc-0.9 (crate (name "niccalc") (vers "0.9.5") (deps (list (crate-dep (name "fltk") (req "^0.13") (default-features #t) (kind 0)))) (hash "0m184m3nz2mzdqw79mvgc5bbdc6hqwpjybl09grjf7i7yd66p8gp")))

(define-public crate-niccalc-0.9 (crate (name "niccalc") (vers "0.9.6") (deps (list (crate-dep (name "fltk") (req "^0.13") (default-features #t) (kind 0)))) (hash "041wqg7z6ixxq6996xwdh603hr4s3g43ds7hsarbhqx8xl5fzswm")))

(define-public crate-niccalc-0.9 (crate (name "niccalc") (vers "0.9.7") (deps (list (crate-dep (name "fltk") (req "^0.13") (default-features #t) (kind 0)))) (hash "0j37x1ysqw9ywbpya39q9fyr1ldc75qa7zvf0srdi06by4lwwaiy")))

(define-public crate-niccalc-0.9 (crate (name "niccalc") (vers "0.9.8") (deps (list (crate-dep (name "fltk") (req "^0.13") (default-features #t) (kind 0)))) (hash "1kx8m5za1pvvixmkfh492pzqsbhbs0fmjc93im3wybr1n3pcbq1s")))

(define-public crate-niccalc-0.9 (crate (name "niccalc") (vers "0.9.9") (deps (list (crate-dep (name "fltk") (req "^0.13") (default-features #t) (kind 0)))) (hash "1aqb6g294yniwfm22n4dp9igvh0aw0i4yz20wk0gi7m8mpdx8ds8")))

(define-public crate-niccalc-0.9 (crate (name "niccalc") (vers "0.9.10") (deps (list (crate-dep (name "fltk") (req "^0.13") (default-features #t) (kind 0)))) (hash "0v6fdf7x9mwvd1jb89db1i6ikcifrm5rz7vys034l8c0aygi0d25")))

(define-public crate-niccalc-0.9 (crate (name "niccalc") (vers "0.9.11") (deps (list (crate-dep (name "comfy-table") (req "^2.1.0") (default-features #t) (kind 0)) (crate-dep (name "fltk") (req "^0.13") (default-features #t) (kind 0)))) (hash "0g5cxg8zzg6r04d7jdqgvnf5pgiqf2mdi1isk8gwhw4bsxqa8apd")))

(define-public crate-niccalc-0.9 (crate (name "niccalc") (vers "0.9.12") (deps (list (crate-dep (name "comfy-table") (req "^2.1.0") (default-features #t) (kind 0)) (crate-dep (name "fltk") (req "^0.13") (default-features #t) (kind 0)))) (hash "1vrhxb34gmziaa39ml6sag843jdiqlb7jm11jmvmhhayb95xw7bm")))

(define-public crate-niccalc-0.9 (crate (name "niccalc") (vers "0.9.15") (deps (list (crate-dep (name "comfy-table") (req "^2.1.0") (default-features #t) (kind 0)) (crate-dep (name "fltk") (req "^0.13") (default-features #t) (kind 0)))) (hash "0icdplscmfzli413plsx0m99m0wcd731l4bwl3aly4bh5szgsh9y")))

