(define-module (crates-io ni co) #:use-module (crates-io))

(define-public crate-nico_nico_ni-0.1 (crate (name "nico_nico_ni") (vers "0.1.1") (deps (list (crate-dep (name "base64") (req "^0.9.3") (default-features #t) (kind 0)) (crate-dep (name "ears") (req "^0.5.1") (default-features #t) (kind 0)))) (hash "1kwqzxcw4aiab4n50xffqm69887kczs72rx6ii53d3qpgyayzfdn")))

(define-public crate-nico_nico_ni-0.1 (crate (name "nico_nico_ni") (vers "0.1.3") (deps (list (crate-dep (name "base64") (req "^0.9.3") (default-features #t) (kind 0)) (crate-dep (name "ears") (req "^0.5.1") (default-features #t) (kind 0)))) (hash "0jmxk91ljmwxgqbvq277yryrvwm3axz5591xwyx597djy9rlz78z")))

(define-public crate-nico_nico_ni-0.1 (crate (name "nico_nico_ni") (vers "0.1.4") (deps (list (crate-dep (name "base64") (req "^0.9.3") (default-features #t) (kind 0)) (crate-dep (name "ears") (req "^0.5.1") (default-features #t) (kind 0)))) (hash "0rxqnxkn5rvmpa2rn853856hqsmynkw35gr1gm7z90g1jgnp6rlj")))

(define-public crate-nicoise-0.1 (crate (name "nicoise") (vers "0.1.0") (hash "03jimlz2zvzhhwblg33mmmc4016jnw2d023g35n010ygcn5m60gm")))

(define-public crate-nicolas-0.1 (crate (name "nicolas") (vers "0.1.0") (deps (list (crate-dep (name "nicolas_macros") (req "^0.1") (default-features #t) (kind 0)))) (hash "0r92232vpnr8zplbyqzxidv2k7hzlf92qj7628qjymfavx9xrl7i")))

(define-public crate-nicolas-0.1 (crate (name "nicolas") (vers "0.1.1") (deps (list (crate-dep (name "nicolas_macros") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "sffs") (req "^0.1") (default-features #t) (kind 0)))) (hash "1mj8pm77xg48xjqf5yd9v0x81m01ldb9msdg2iv4d6bih56c3bmc")))

(define-public crate-nicolas_macros-0.1 (crate (name "nicolas_macros") (vers "0.1.0") (deps (list (crate-dep (name "quote") (req "^0.6") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^0.15") (default-features #t) (kind 0)))) (hash "1xxrf8aw1arl1209nfdkl1j761apc7rjyyp86kdxzhrpnvwsc0p6")))

(define-public crate-nicompiler_backend-0.1 (crate (name "nicompiler_backend") (vers "0.1.0") (deps (list (crate-dep (name "maplit") (req "^1.0.2") (default-features #t) (kind 0)) (crate-dep (name "ndarray") (req "^0.15.6") (default-features #t) (kind 0)) (crate-dep (name "numpy") (req "^0.19.0") (default-features #t) (kind 0)) (crate-dep (name "pyo3") (req "^0.19.2") (features (quote ("multiple-pymethods"))) (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.9.3") (default-features #t) (kind 0)))) (hash "182ly1i0wva9nb03x9sgq29wwkflfj9snxcsm1qg2gsjvxrcrcm0")))

(define-public crate-nicompiler_backend-0.2 (crate (name "nicompiler_backend") (vers "0.2.0") (deps (list (crate-dep (name "maplit") (req "^1.0.2") (default-features #t) (kind 0)) (crate-dep (name "ndarray") (req "^0.15.6") (default-features #t) (kind 0)) (crate-dep (name "numpy") (req "^0.19.0") (default-features #t) (kind 0)) (crate-dep (name "pyo3") (req "^0.19.2") (features (quote ("multiple-pymethods"))) (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.9.3") (default-features #t) (kind 0)))) (hash "01zvj9w0z7pvnfwqam9by802sm0a9miqv5s47a3wb4702fs3mbda")))

(define-public crate-nicompiler_backend-0.3 (crate (name "nicompiler_backend") (vers "0.3.0") (deps (list (crate-dep (name "maplit") (req "^1.0.2") (default-features #t) (kind 0)) (crate-dep (name "ndarray") (req "^0.15.6") (default-features #t) (kind 0)) (crate-dep (name "numpy") (req "^0.19.0") (default-features #t) (kind 0)) (crate-dep (name "pyo3") (req "^0.19.2") (features (quote ("multiple-pymethods"))) (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.9.3") (default-features #t) (kind 0)))) (hash "18dd0jkh44ynapdl0zd52lqi726y36ljzwygqk8p82j4fl8rbckm")))

(define-public crate-NicoNicoNi-0.1 (crate (name "NicoNicoNi") (vers "0.1.0") (deps (list (crate-dep (name "ears") (req "^0.5.1") (default-features #t) (kind 0)))) (hash "1cnih54k83cyqa32ij4dpp5w3dyl2p7l7lnfb8xnkk26n750lq2m")))

(define-public crate-NicoNicoNi-0.1 (crate (name "NicoNicoNi") (vers "0.1.5") (deps (list (crate-dep (name "base64") (req "^0.9.3") (default-features #t) (kind 0)) (crate-dep (name "ears") (req "^0.5.1") (default-features #t) (kind 0)))) (hash "0b130lzwqgcjljghp21lfvnmydm329lpp0fq4pfkbkajjgrsj8j4")))

