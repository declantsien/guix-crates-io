(define-module (crates-io ni -s) #:use-module (crates-io))

(define-public crate-ni-sync-rs-23 (crate (name "ni-sync-rs") (vers "23.8.0") (deps (list (crate-dep (name "bindgen") (req "^0.60") (default-features #t) (kind 1)))) (hash "15vh232q9fq4mld7pxj27pb9mgcvdzmcckihas957h945zyp65bg") (links "ni-sync")))

(define-public crate-ni-syscfg-0.1 (crate (name "ni-syscfg") (vers "0.1.0") (deps (list (crate-dep (name "ni-syscfg-sys") (req "^20.5") (default-features #t) (kind 0)) (crate-dep (name "num-derive") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1") (default-features #t) (kind 0)))) (hash "15zazzih475wvizdww9kwl9m2k9lymy933ryfqlfd5zfwcb172p8")))

(define-public crate-ni-syscfg-0.2 (crate (name "ni-syscfg") (vers "0.2.0") (deps (list (crate-dep (name "ni-syscfg-sys") (req "^20.5") (default-features #t) (kind 0)) (crate-dep (name "num-derive") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "paste") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1") (default-features #t) (kind 0)))) (hash "0c0hrf6j5lsh42glh5hcbbgpvlgsyxr0ynjb34q0vfnfn221wrrb")))

(define-public crate-ni-syscfg-sys-20 (crate (name "ni-syscfg-sys") (vers "20.5.0") (hash "1zgwdn5h78n54raqp5f51j8nb4qajk3wrh27hs2mlvpkxb5azpw5") (links "nisyscfg")))

