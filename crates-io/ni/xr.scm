(define-module (crates-io ni xr) #:use-module (crates-io))

(define-public crate-nixrs-0.1 (crate (name "nixrs") (vers "0.1.0") (deps (list (crate-dep (name "libc") (req "^0.2.155") (default-features #t) (kind 0)) (crate-dep (name "nixrs-sys") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.61") (default-features #t) (kind 0)))) (hash "0y3algwg3m6iajh8mkq7y2cr5gr7cb1dl1snq82rg6gah7ib6naj")))

(define-public crate-nixrs-sys-0.1 (crate (name "nixrs-sys") (vers "0.1.0") (deps (list (crate-dep (name "bindgen") (req "^0.69.4") (default-features #t) (kind 1)) (crate-dep (name "pkg-config") (req "^0.3.30") (default-features #t) (kind 1)))) (hash "1gzy8mlw3jndwvvg56m3r78006dy136phh6pk7bmnjj02l8fva1r")))

