(define-module (crates-io ni xm) #:use-module (crates-io))

(define-public crate-nixmodule-0.1 (crate (name "nixmodule") (vers "0.1.0") (deps (list (crate-dep (name "colored") (req "^2.0.0") (default-features #t) (kind 0)) (crate-dep (name "flate2") (req "^1.0.22") (default-features #t) (kind 0)) (crate-dep (name "home-dir") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "prettytable-rs") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.11.10") (features (quote ("rustls-tls-native-roots" "blocking"))) (kind 0)) (crate-dep (name "serde") (req "^1.0.136") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.3.26") (default-features #t) (kind 0)) (crate-dep (name "tar") (req "^0.4.38") (default-features #t) (kind 0)) (crate-dep (name "toml") (req "^0.5.8") (default-features #t) (kind 0)))) (hash "1yqm8l6nbcl78l0hbxyrbgj4is718rcnnp9ldn60yvy58xrssck9")))

(define-public crate-nixmodule-0.2 (crate (name "nixmodule") (vers "0.2.0") (deps (list (crate-dep (name "colored") (req "^2.0.0") (default-features #t) (kind 0)) (crate-dep (name "flate2") (req "^1.0.22") (default-features #t) (kind 0)) (crate-dep (name "home-dir") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "prettytable-rs") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.11.10") (features (quote ("rustls-tls-native-roots" "blocking"))) (kind 0)) (crate-dep (name "serde") (req "^1.0.136") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.3.26") (default-features #t) (kind 0)) (crate-dep (name "tar") (req "^0.4.38") (default-features #t) (kind 0)) (crate-dep (name "toml") (req "^0.5.8") (default-features #t) (kind 0)))) (hash "0i8la76ywzxdd7h4pq4pbplxih2r1bvgq9zva1268x4i0y2zmiz3")))

(define-public crate-nixmodule-0.3 (crate (name "nixmodule") (vers "0.3.0") (deps (list (crate-dep (name "colored") (req "^2.0.0") (default-features #t) (kind 0)) (crate-dep (name "flate2") (req "^1.0.22") (default-features #t) (kind 0)) (crate-dep (name "home-dir") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "prettytable-rs") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.11.10") (features (quote ("rustls-tls-native-roots" "blocking"))) (kind 0)) (crate-dep (name "serde") (req "^1.0.136") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.3.26") (default-features #t) (kind 0)) (crate-dep (name "tar") (req "^0.4.38") (default-features #t) (kind 0)) (crate-dep (name "toml") (req "^0.5.8") (default-features #t) (kind 0)))) (hash "0lqawr4m3fmj4bw8p4hz8vlpzmfrbpxnj8pv47vnbz5s2s0d77h2")))

(define-public crate-nixmodule-0.4 (crate (name "nixmodule") (vers "0.4.0") (deps (list (crate-dep (name "colored") (req "^2.0.0") (default-features #t) (kind 0)) (crate-dep (name "flate2") (req "^1.0.22") (default-features #t) (kind 0)) (crate-dep (name "home-dir") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "prettytable-rs") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.11.10") (features (quote ("rustls-tls-native-roots" "blocking"))) (kind 0)) (crate-dep (name "serde") (req "^1.0.136") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.3.26") (default-features #t) (kind 0)) (crate-dep (name "tar") (req "^0.4.38") (default-features #t) (kind 0)) (crate-dep (name "toml") (req "^0.5.8") (default-features #t) (kind 0)))) (hash "0idn8c8qnz97na9qns21qm36irzr3ph70n1cs3zvsmjk8p81xlbb")))

(define-public crate-nixmodule-0.4 (crate (name "nixmodule") (vers "0.4.1") (deps (list (crate-dep (name "clap") (req "^3.2.22") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "colored") (req "^2.0.0") (default-features #t) (kind 0)) (crate-dep (name "flate2") (req "^1.0.24") (default-features #t) (kind 0)) (crate-dep (name "prettytable-rs") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.11.11") (features (quote ("rustls-tls-native-roots" "blocking"))) (kind 0)) (crate-dep (name "serde") (req "^1.0.136") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "shellexpand") (req "^2.1.2") (default-features #t) (kind 0)) (crate-dep (name "tar") (req "^0.4.38") (default-features #t) (kind 0)) (crate-dep (name "toml") (req "^0.5.9") (default-features #t) (kind 0)))) (hash "0i0vb2z9wdfk0kljx8fi4j7z775gmlfgmc4yg7xiwv65lbsj5n64")))

(define-public crate-nixmodule-0.4 (crate (name "nixmodule") (vers "0.4.2") (deps (list (crate-dep (name "clap") (req "^3.2.22") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "colored") (req "^2.0.0") (default-features #t) (kind 0)) (crate-dep (name "flate2") (req "^1.0.24") (default-features #t) (kind 0)) (crate-dep (name "prettytable-rs") (req "^0.10") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.11") (features (quote ("rustls-tls-native-roots" "blocking"))) (kind 0)) (crate-dep (name "serde") (req "^1.0.136") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "shellexpand") (req "^2.1.2") (default-features #t) (kind 0)) (crate-dep (name "tar") (req "^0.4.38") (default-features #t) (kind 0)) (crate-dep (name "toml") (req "^0.5.9") (default-features #t) (kind 0)))) (hash "0kb3z3p92jprpqj1ys3kb5y4yp4vs19hag84mirwf1qq0chj1s23")))

