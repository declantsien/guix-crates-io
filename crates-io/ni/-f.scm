(define-module (crates-io ni -f) #:use-module (crates-io))

(define-public crate-ni-fpga-0.1 (crate (name "ni-fpga") (vers "0.1.0") (deps (list (crate-dep (name "bitvec") (req "^0.17.4") (default-features #t) (kind 0)) (crate-dep (name "ni-fpga-sys") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.19") (default-features #t) (kind 0)))) (hash "17r2vffxhlgwqwdzybs35zbybdqfpa6w25iyhmb4w85rljwapgm9")))

(define-public crate-ni-fpga-1 (crate (name "ni-fpga") (vers "1.0.0") (deps (list (crate-dep (name "bitvec") (req "^0.17.4") (default-features #t) (kind 0)) (crate-dep (name "ni-fpga-sys") (req "^1.0.0") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.19") (default-features #t) (kind 0)))) (hash "1r7ygdzxl7w0m5s0459c0hla8hwzr2gmirpgl7yhxpsxgi0csyky")))

(define-public crate-ni-fpga-1 (crate (name "ni-fpga") (vers "1.1.0") (deps (list (crate-dep (name "bitvec") (req "^0.17.4") (default-features #t) (kind 0)) (crate-dep (name "ni-fpga-sys") (req "^1.0.0") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.19") (default-features #t) (kind 0)))) (hash "13qxzaw0vgcq5y0mq4v4g722papkg84fbbxkxndplizxa5n912if")))

(define-public crate-ni-fpga-1 (crate (name "ni-fpga") (vers "1.2.0") (deps (list (crate-dep (name "bitvec") (req "^0.17.4") (default-features #t) (kind 0)) (crate-dep (name "ni-fpga-sys") (req "^1.0.0") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.19") (default-features #t) (kind 0)))) (hash "1xwwda4mgl1v8qcafpd77i64ccxn9xvc97dpm9c40h4n7hvg9zss")))

(define-public crate-ni-fpga-1 (crate (name "ni-fpga") (vers "1.3.0") (deps (list (crate-dep (name "bitvec") (req "^0.17.4") (default-features #t) (kind 0)) (crate-dep (name "ni-fpga-sys") (req "^1.0.0") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.19") (default-features #t) (kind 0)))) (hash "01dm8ac46nw1kkhkgkdwb9gc3zaww9r5ajdm11rkw2z7f3jk2qip")))

(define-public crate-ni-fpga-1 (crate (name "ni-fpga") (vers "1.4.0") (deps (list (crate-dep (name "bitvec") (req "^0.17.4") (default-features #t) (kind 0)) (crate-dep (name "ni-fpga-sys") (req "^1.0.1") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.19") (default-features #t) (kind 0)))) (hash "1y1rizwvbsmxs23nrl92ksnhjnqgjp5i2l25q4658fi51xa7hpfy")))

(define-public crate-ni-fpga-1 (crate (name "ni-fpga") (vers "1.4.1") (deps (list (crate-dep (name "bitvec") (req "^0.17.4") (default-features #t) (kind 0)) (crate-dep (name "ni-fpga-sys") (req "^1.0.1") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.19") (default-features #t) (kind 0)))) (hash "08bwry8g6mlfnwm5nyn01x8zz2pkx4mcwgyafgpggag0jj06pczb")))

(define-public crate-ni-fpga-interface-0.1 (crate (name "ni-fpga-interface") (vers "0.1.0") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "paste") (req "^1.0") (default-features #t) (kind 0)))) (hash "0mxmh472a2nhywhkcdiyxkyjqw08plzrca1y7xsk9l9bjw4yjgb4")))

(define-public crate-ni-fpga-interface-build-0.1 (crate (name "ni-fpga-interface-build") (vers "0.1.0") (deps (list (crate-dep (name "cc") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "lang-c") (req "^0.15") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "prettyplease") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2.0") (features (quote ("visit" "parsing"))) (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1") (default-features #t) (kind 0)))) (hash "1v0gg2qjszz6fnbhg6nyl7yqn9bmkindfj9kc1f8dkfb9c706awd")))

(define-public crate-ni-fpga-macros-0.1 (crate (name "ni-fpga-macros") (vers "0.1.0") (deps (list (crate-dep (name "quote") (req "^1.0.6") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.23") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "1gcarjl46i043d9vf27v1f56s5qb957q22v6b2kzg7djdr9jm6cf")))

(define-public crate-ni-fpga-macros-1 (crate (name "ni-fpga-macros") (vers "1.0.0") (deps (list (crate-dep (name "quote") (req "^1.0.6") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.23") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "1k9fwrdybmv16xkrz64qcdf4x2lsbcwfcy9342lnny8rii3lwzlc")))

(define-public crate-ni-fpga-macros-1 (crate (name "ni-fpga-macros") (vers "1.0.1") (deps (list (crate-dep (name "quote") (req "^1.0.6") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.23") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "1divm8gpj7m56jgg2jg0vsz77n58qs98m4bw55pkb1bwmdkbagdn")))

(define-public crate-ni-fpga-sys-0.1 (crate (name "ni-fpga-sys") (vers "0.1.0") (hash "1c2dv2z81s7d751xi3b1diqk5m8xdwxcxwlz0h3njhjgdiy93796") (links "NiFpga")))

(define-public crate-ni-fpga-sys-1 (crate (name "ni-fpga-sys") (vers "1.0.0") (hash "1vzkzq9j00hpkyf01y0sv5ibl27a6x5pjdh57hyim2waxfs60z13") (links "NiFpga")))

(define-public crate-ni-fpga-sys-1 (crate (name "ni-fpga-sys") (vers "1.0.1") (hash "0l80gzdrm9jwqx6gcgpd4wai3fgfqi8c3m5v5kmpaijq3f3fq8py") (links "NiFpga")))

