(define-module (crates-io ni as) #:use-module (crates-io))

(define-public crate-nias-0.1 (crate (name "nias") (vers "0.1.0") (hash "10r0nskd0i949sx53iycry07iqxqs0qdy4hc6zyvn1m57s69ia70")))

(define-public crate-nias-0.3 (crate (name "nias") (vers "0.3.0") (deps (list (crate-dep (name "rusty-hook") (req "^0.9.1") (default-features #t) (kind 2)))) (hash "1jr1iy52vg05ysih6xga8a64ks7xps3ndjp9cazh9d92zmfr3gpm")))

(define-public crate-nias-0.3 (crate (name "nias") (vers "0.3.1") (deps (list (crate-dep (name "rusty-hook") (req "^0.9.1") (default-features #t) (kind 2)))) (hash "14r4mxhcmn4686npnhian3qh7hrqzxrwjdgn8fk0ghg7dcl8jnnn")))

(define-public crate-nias-0.4 (crate (name "nias") (vers "0.4.0") (deps (list (crate-dep (name "rusty-hook") (req "^0.9.1") (default-features #t) (kind 2)))) (hash "0xk30isvyh6p3n0mim92pc8ypypb2c97dpb19l78ss9jq3sh2vzp")))

(define-public crate-nias-0.5 (crate (name "nias") (vers "0.5.0") (deps (list (crate-dep (name "rusty-hook") (req "^0.10.0") (default-features #t) (kind 2)))) (hash "1w0jrshjqr1dxd01kg12f1ic067hvwwqc9jxbf0m063gr11089db")))

(define-public crate-nias-0.7 (crate (name "nias") (vers "0.7.0") (deps (list (crate-dep (name "rusty-hook") (req "^0.11.0") (default-features #t) (kind 2)))) (hash "08milmdr9z3jxzccgjfmx7ldjf8vplb8d8qspp47lm35c8wzsd81")))

