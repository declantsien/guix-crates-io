(define-module (crates-io ni ss) #:use-module (crates-io))

(define-public crate-nisshoku-0.1 (crate (name "nisshoku") (vers "0.1.0") (deps (list (crate-dep (name "mlua") (req "^0.9.1") (features (quote ("luau-jit"))) (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)) (crate-dep (name "raylib") (req "^3.7.0") (default-features #t) (kind 0)) (crate-dep (name "raylib-ffi") (req "^4.5.3") (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.11.20") (features (quote ("blocking"))) (default-features #t) (kind 0)) (crate-dep (name "rouille") (req "^3.6.2") (default-features #t) (kind 0)) (crate-dep (name "three-d") (req "^0.16.0") (default-features #t) (kind 0)))) (hash "0jvafxi1j5han6xggw9vghxl1yrxl34jwpb5vmi1xi8q7yql8dfr")))

(define-public crate-nissy_a-0.0.1 (crate (name "nissy_a") (vers "0.0.1") (hash "1d2djripn7fvvhzwjasajwrf9gkam63n7zfdx440fvwgid7i74yf") (yanked #t)))

(define-public crate-nissy_a-0.0.2 (crate (name "nissy_a") (vers "0.0.2") (hash "0lmcfdhm788bkhmmka9x41n5li4z8k8dddljlmpgqx8jx28dlc6l") (yanked #t)))

(define-public crate-nissy_a-0.0.3 (crate (name "nissy_a") (vers "0.0.3") (hash "1bfqh2x1zxhnpwpdyjm07jihqz5f1w5sd6x7hhl1lfag25jlqkb0") (yanked #t)))

(define-public crate-nissy_b-0.0.1 (crate (name "nissy_b") (vers "0.0.1") (deps (list (crate-dep (name "nissy_a") (req "^0.0.1") (default-features #t) (kind 0)))) (hash "1g5g2p421r23m4pjr2mn4f06h5yyj6p89sl7rdry27finf4d508c") (yanked #t)))

(define-public crate-nissy_b-0.0.2 (crate (name "nissy_b") (vers "0.0.2") (deps (list (crate-dep (name "nissy_a") (req "^0.0.2") (default-features #t) (kind 0)))) (hash "110cn18isyx0w39184y6p0gcnpfk8a9w5f6g7c7nscksgwvfdp7g") (yanked #t)))

(define-public crate-nissy_b-0.0.3 (crate (name "nissy_b") (vers "0.0.3") (deps (list (crate-dep (name "nissy_a") (req "^0.0.3") (default-features #t) (kind 0)))) (hash "1db6lp7jgndn788ic72xillfhlqrfzy2i0v0s3zcd69ax821s9qk") (yanked #t)))

(define-public crate-nissy_c-0.0.1 (crate (name "nissy_c") (vers "0.0.1") (deps (list (crate-dep (name "nissy_a") (req "^0.0.1") (default-features #t) (kind 0)) (crate-dep (name "nissy_b") (req "^0.0.1") (default-features #t) (kind 0)))) (hash "1365izqwv1bqm2kihqzx7cgikfzlc3dfsj8sihj34b9isyjqi44g") (yanked #t)))

(define-public crate-nissy_c-0.0.2 (crate (name "nissy_c") (vers "0.0.2") (deps (list (crate-dep (name "nissy_a") (req "^0.0.2") (default-features #t) (kind 0)) (crate-dep (name "nissy_b") (req "^0.0.2") (default-features #t) (kind 0)))) (hash "07rcggf8bli9vp08jgi4ydplhg957d4mwnh3yxy4312w76i9n46m") (yanked #t)))

(define-public crate-nissy_c-0.0.3 (crate (name "nissy_c") (vers "0.0.3") (deps (list (crate-dep (name "nissy_a") (req "^0.0.3") (default-features #t) (kind 0)) (crate-dep (name "nissy_b") (req "^0.0.3") (default-features #t) (kind 0)))) (hash "03inim6h614plfx32dfjv3a5fmdlz81ydlrmkbgknvdq25kyix73") (yanked #t)))

(define-public crate-nissy_d-0.0.1 (crate (name "nissy_d") (vers "0.0.1") (deps (list (crate-dep (name "nissy_b") (req "^0.0.1") (default-features #t) (kind 0)) (crate-dep (name "nissy_c") (req "^0.0.1") (default-features #t) (kind 0)))) (hash "0lnxrvmmi1rx6brj7yk546lqz4dxkih1jrw5yzba1nq02sygdalb") (yanked #t)))

(define-public crate-nissy_d-0.0.2 (crate (name "nissy_d") (vers "0.0.2") (deps (list (crate-dep (name "nissy_b") (req "^0.0.2") (default-features #t) (kind 0)) (crate-dep (name "nissy_c") (req "^0.0.2") (default-features #t) (kind 0)))) (hash "0xlgjn32q0ccsclxdlj0kjfw5bi5wmippsv147afzkw91gsn8g19") (yanked #t)))

(define-public crate-nissy_d-0.0.3 (crate (name "nissy_d") (vers "0.0.3") (deps (list (crate-dep (name "nissy_b") (req "^0.0.3") (default-features #t) (kind 0)) (crate-dep (name "nissy_c") (req "^0.0.3") (default-features #t) (kind 0)))) (hash "1m14h1cmn9lif3g8wy12xz1pzpdnx3pb3i29b1zfcqm4q40qlcgw") (yanked #t)))

(define-public crate-nissy_e-0.0.1 (crate (name "nissy_e") (vers "0.0.1") (deps (list (crate-dep (name "nissy_d") (req "^0.0.1") (default-features #t) (kind 0)))) (hash "1sgwvkz2z186akyrisngp4mxv34qa2kj3d216vxbpqvpbykc0ayk") (yanked #t)))

(define-public crate-nissy_e-0.0.2 (crate (name "nissy_e") (vers "0.0.2") (deps (list (crate-dep (name "nissy_d") (req "^0.0.2") (default-features #t) (kind 0)))) (hash "0w9b62z8zw764xdhfhzdfb0szp363drlyrqhxk2nl10yyy0s31fs") (yanked #t)))

(define-public crate-nissy_e-0.0.3 (crate (name "nissy_e") (vers "0.0.3") (deps (list (crate-dep (name "nissy_d") (req "^0.0.3") (default-features #t) (kind 0)))) (hash "0dh2jfz605ql4m1spnazpldp3gyc0whxy618whvcx3ri78xxpwxb") (yanked #t)))

