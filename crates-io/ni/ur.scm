(define-module (crates-io ni ur) #:use-module (crates-io))

(define-public crate-niura-0.1 (crate (name "niura") (vers "0.1.0") (deps (list (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)))) (hash "102nrf4yfckqddg25widh1x5nj6jvwq11f0ls8jga7bv2a2ynvj7")))

