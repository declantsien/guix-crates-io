(define-module (crates-io ni xi) #:use-module (crates-io))

(define-public crate-nixify-0.1 (crate (name "nixify") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^4.4.0") (features (quote ("cargo" "derive"))) (default-features #t) (kind 0)) (crate-dep (name "nixpkgs-fmt") (req "^1.3.0") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.117") (default-features #t) (kind 0)) (crate-dep (name "toml") (req "^0.8.12") (default-features #t) (kind 0)) (crate-dep (name "yaml-rust2") (req "^0.8.0") (default-features #t) (kind 0)))) (hash "1gz9sqys3fggk10pqg78pdwcjy48q9bn3g61rfr01zbvkhw6f0ml")))

(define-public crate-nixify-0.1 (crate (name "nixify") (vers "0.1.1") (deps (list (crate-dep (name "clap") (req "^4.4.0") (features (quote ("cargo" "derive"))) (default-features #t) (kind 0)) (crate-dep (name "crossbeam-utils") (req "^0.8.19") (default-features #t) (kind 0)) (crate-dep (name "indexmap") (req "^2.2.6") (default-features #t) (kind 0)) (crate-dep (name "nixpkgs-fmt") (req "^1.3.0") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.117") (default-features #t) (kind 0)) (crate-dep (name "toml") (req "^0.8.12") (default-features #t) (kind 0)) (crate-dep (name "yaml-rust2") (req "^0.8.0") (default-features #t) (kind 0)))) (hash "0v5h3gsgz0442786qawwb5qlp79bhw4zs15b1wqyhvw3shd0s18p")))

(define-public crate-nixinfo-0.1 (crate (name "nixinfo") (vers "0.1.0") (deps (list (crate-dep (name "glob") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "mpd") (req "^0.0.12") (optional #t) (default-features #t) (kind 0)))) (hash "1ya978qznzaxcd8lz5pj92wf5icf1ixzmr80h4i6g9c8zl75ancd") (features (quote (("nomusic") ("music" "mpd"))))))

(define-public crate-nixinfo-0.1 (crate (name "nixinfo") (vers "0.1.1") (deps (list (crate-dep (name "glob") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "mpd") (req "^0.0.12") (optional #t) (default-features #t) (kind 0)))) (hash "19wa2w7hdbggr9kwbi7nv09ndbsbx02g9yvhdlw1n9paml91jx68") (features (quote (("nomusic") ("music" "mpd"))))))

(define-public crate-nixinfo-0.1 (crate (name "nixinfo") (vers "0.1.2") (deps (list (crate-dep (name "glob") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "mpd") (req "^0.0.12") (optional #t) (default-features #t) (kind 0)))) (hash "1agxkfd8ya3rq3slci87pwz18wfwj57a1bgidqav6hamvk1wb71l") (features (quote (("music" "mpd"))))))

(define-public crate-nixinfo-0.1 (crate (name "nixinfo") (vers "0.1.3") (deps (list (crate-dep (name "glob") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "mpd") (req "^0.0.12") (optional #t) (default-features #t) (kind 0)))) (hash "0mg6dg0im9dxbj4sf0byg2s1v0f5axawkv0l20ycg769wmppz9nw") (features (quote (("music" "mpd"))))))

(define-public crate-nixinfo-0.1 (crate (name "nixinfo") (vers "0.1.4") (deps (list (crate-dep (name "glob") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "mpd") (req "^0.0.12") (optional #t) (default-features #t) (kind 0)))) (hash "1hm3k2vvpl8qs2g3zvpivqk4k7c6vp67y82swrkcwfp1ly0qcka0") (features (quote (("music" "mpd"))))))

(define-public crate-nixinfo-0.1 (crate (name "nixinfo") (vers "0.1.5") (deps (list (crate-dep (name "glob") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "mpd") (req "^0.0.12") (optional #t) (default-features #t) (kind 0)))) (hash "0z89rhik0642ilp3bqy8snilf6brmnxvpg9x61arx26wnvy5f2g0") (features (quote (("music" "mpd"))))))

(define-public crate-nixinfo-0.1 (crate (name "nixinfo") (vers "0.1.6") (deps (list (crate-dep (name "glob") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "mpd") (req "^0.0.12") (optional #t) (default-features #t) (kind 0)))) (hash "15nffsr0x0k1djs09idcwvpqmcq3yj9y4izkqsdp8a45g4piall9") (features (quote (("music" "mpd"))))))

(define-public crate-nixinfo-0.1 (crate (name "nixinfo") (vers "0.1.7") (deps (list (crate-dep (name "glob") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "mpd") (req "^0.0.12") (optional #t) (default-features #t) (kind 0)))) (hash "13ngxa58sqmjgvma7nz79biy7ha7jy9885yh9nkjizs8x26yzss0") (features (quote (("music" "mpd"))))))

(define-public crate-nixinfo-0.1 (crate (name "nixinfo") (vers "0.1.8") (deps (list (crate-dep (name "glob") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "mpd") (req "^0.0.12") (optional #t) (default-features #t) (kind 0)))) (hash "1dy520dsndp5xv6i0fggzfj5qxij4larav7dyqvayrkbi05732xy") (features (quote (("music" "mpd"))))))

(define-public crate-nixinfo-0.1 (crate (name "nixinfo") (vers "0.1.9") (deps (list (crate-dep (name "glob") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "mpd") (req "^0.0.12") (optional #t) (default-features #t) (kind 0)))) (hash "1m9fzix7z1yw09h3wn51l85cgr9m5r3xcn8q050j0ma0laf3a2v4") (features (quote (("music" "mpd"))))))

(define-public crate-nixinfo-0.2 (crate (name "nixinfo") (vers "0.2.0") (deps (list (crate-dep (name "glob") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "mpd") (req "^0.0.12") (optional #t) (default-features #t) (kind 0)))) (hash "127ykpvdknx1lkszi1ivpc24hh01mj7fgwzyjakx69qdjaz9x3dp") (features (quote (("music" "mpd"))))))

(define-public crate-nixinfo-0.2 (crate (name "nixinfo") (vers "0.2.1") (deps (list (crate-dep (name "glob") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "mpd") (req "^0.0.12") (optional #t) (default-features #t) (kind 0)))) (hash "068y41d0rv0mq0gdns63xikzr6zgfdd43qfyg0xfh5qdxlca270d") (features (quote (("music" "mpd"))))))

(define-public crate-nixinfo-0.2 (crate (name "nixinfo") (vers "0.2.2") (deps (list (crate-dep (name "glob") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "mpd") (req "^0.0.12") (optional #t) (default-features #t) (kind 0)))) (hash "0r6yigzqmn198grhrkpp6qpfrbsi4invj0p3bnpry71q9d49cvqx") (features (quote (("music" "mpd"))))))

(define-public crate-nixinfo-0.2 (crate (name "nixinfo") (vers "0.2.3") (deps (list (crate-dep (name "glob") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "mpd") (req "^0.0.12") (optional #t) (default-features #t) (kind 0)))) (hash "040cr9g8hdiklnpqad53dg3vzx1bc0fyq7w11nja73is5a6szdra") (features (quote (("music" "mpd"))))))

(define-public crate-nixinfo-0.2 (crate (name "nixinfo") (vers "0.2.4") (deps (list (crate-dep (name "glob") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "mpd") (req "^0.0.12") (optional #t) (default-features #t) (kind 0)))) (hash "07z2br7vjayvwphli3qc6y49s80apc2px3il6f9gxn5864b6gkpv") (features (quote (("music" "mpd"))))))

(define-public crate-nixinfo-0.2 (crate (name "nixinfo") (vers "0.2.5") (deps (list (crate-dep (name "glob") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "mpd") (req "^0.0.12") (optional #t) (default-features #t) (kind 0)))) (hash "04r7r28dgxc1wzdnsi8ka68i5r8ijfbw3csb4pvgdpqlw75nns5b") (features (quote (("music" "mpd"))))))

(define-public crate-nixinfo-0.2 (crate (name "nixinfo") (vers "0.2.6") (deps (list (crate-dep (name "glob") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "mpd") (req "^0.0.12") (optional #t) (default-features #t) (kind 0)))) (hash "1k8a6isicmr9yjmzlp8z8dyd717v9lp606rvp7372578fvbz46y3") (features (quote (("music" "mpd"))))))

(define-public crate-nixinfo-0.2 (crate (name "nixinfo") (vers "0.2.7") (deps (list (crate-dep (name "glob") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "mpd") (req "^0.0.12") (optional #t) (default-features #t) (kind 0)))) (hash "0vcpwan6psrg1bmndp5c02wx8pwfdkfscrqp9k0s0rwln7i0fwpp") (features (quote (("music" "mpd"))))))

(define-public crate-nixinfo-0.2 (crate (name "nixinfo") (vers "0.2.8") (deps (list (crate-dep (name "glob") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "mpd") (req "^0.0.12") (optional #t) (default-features #t) (kind 0)))) (hash "0zxmr2y43ic9w5rn6aa3kkhbq5ax5c3aszczw6194gnbn4igbp9f") (features (quote (("music" "mpd"))))))

(define-public crate-nixinfo-0.2 (crate (name "nixinfo") (vers "0.2.9") (deps (list (crate-dep (name "glob") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "mpd") (req "^0.0.12") (optional #t) (default-features #t) (kind 0)))) (hash "0jzx703wrqbvrm59djw0iyzflpnyx58c2ly1dpp2ls50cxp14c0r") (features (quote (("music" "mpd"))))))

(define-public crate-nixinfo-0.3 (crate (name "nixinfo") (vers "0.3.0") (deps (list (crate-dep (name "glob") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "mpd") (req "^0.0.12") (optional #t) (default-features #t) (kind 0)))) (hash "0ng4yz8bs2jxbzzknvac4ir8l4wsgkznq4mnq6sqjar3gf0hpc3m") (features (quote (("music" "mpd"))))))

(define-public crate-nixinfo-0.3 (crate (name "nixinfo") (vers "0.3.1") (deps (list (crate-dep (name "glob") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "mpd") (req "^0.0.12") (optional #t) (default-features #t) (kind 0)))) (hash "1i7kf381r29qj5xh6a98p6krrh529bv7jgq6vqrk18s5grcdyrjf") (features (quote (("music" "mpd"))))))

(define-public crate-nixinfo-0.3 (crate (name "nixinfo") (vers "0.3.2") (deps (list (crate-dep (name "glob") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "mpd") (req "^0.0.12") (optional #t) (default-features #t) (kind 0)))) (hash "007xmp1wzb43yv93q7lvxc2v99qpw1jghvm2xh08xyzs0nqgyr2w") (features (quote (("music" "mpd"))))))

(define-public crate-nixinfo-0.3 (crate (name "nixinfo") (vers "0.3.3") (deps (list (crate-dep (name "glob") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "mpd") (req "^0.0.12") (optional #t) (default-features #t) (kind 0)))) (hash "11vrmk9ld0vg6famkzlr443z6y7z5lxxbyc68kwm8gjzxnjj1z3a") (features (quote (("music" "mpd"))))))

