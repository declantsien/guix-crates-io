(define-module (crates-io ni tp) #:use-module (crates-io))

(define-public crate-nitpickers-0.1 (crate (name "nitpickers") (vers "0.1.0") (hash "1yjnf6vir1v6vpkxnz9v0xxic05qk7wm6kv3jvpar65yll9x8ziw") (yanked #t)))

(define-public crate-nitpickers-0.1 (crate (name "nitpickers") (vers "0.1.1") (hash "1f434nzw20jlsq60j5i9r4rbj9s5pzcc46s22f3l2bl54ldvs863")))

