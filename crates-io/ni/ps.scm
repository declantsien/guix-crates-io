(define-module (crates-io ni ps) #:use-module (crates-io))

(define-public crate-nips-0.0.35 (crate (name "nips") (vers "0.0.35") (hash "080l8y5rqdrmzxzx8fvy4wvcc39c0h2rg9grf9k156fy2kzsrjbr") (rust-version "1.73")))

(define-public crate-nips-0.0.36 (crate (name "nips") (vers "0.0.36") (hash "0id7j88mdjh3jsi21b0fqjk09rg1ff4aj5pyvc7g8v8smjjfl32l") (rust-version "1.73")))

