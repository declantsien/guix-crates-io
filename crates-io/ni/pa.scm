(define-module (crates-io ni pa) #:use-module (crates-io))

(define-public crate-nipah_tokenizer-0.1 (crate (name "nipah_tokenizer") (vers "0.1.0") (deps (list (crate-dep (name "bencher") (req "^0.1.5") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)))) (hash "0x8l2mcs96sfcxc25hmaiagwv84m7iapsy7imm9hhm1yjgxq6zhx")))

