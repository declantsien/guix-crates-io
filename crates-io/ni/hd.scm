(define-module (crates-io ni hd) #:use-module (crates-io))

(define-public crate-nihdb-0.1 (crate (name "nihdb") (vers "0.1.0") (deps (list (crate-dep (name "crc") (req "^1.0.0") (default-features #t) (kind 0)) (crate-dep (name "fnv") (req "^1.0.6") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "owning_ref") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.4") (default-features #t) (kind 0)))) (hash "1n1fi3pgq9mhxjhbj6j4nqwajb72km5si4m2qrv9k0xrzbpq7dm7")))

(define-public crate-nihdb-0.2 (crate (name "nihdb") (vers "0.2.0") (deps (list (crate-dep (name "crc") (req "^1.0.0") (default-features #t) (kind 0)) (crate-dep (name "fnv") (req "^1.0.6") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "owning_ref") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.4") (default-features #t) (kind 0)))) (hash "0h4wnnspwdrrgqn8ppl8f4cs69sz59a5hx04jg3r4598grd8krgz")))

(define-public crate-nihdb-0.3 (crate (name "nihdb") (vers "0.3.0") (deps (list (crate-dep (name "crc") (req "^1.0.0") (default-features #t) (kind 0)) (crate-dep (name "fnv") (req "^1.0.6") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "owning_ref") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.4") (default-features #t) (kind 0)))) (hash "0pv6sc5mady24hy580y4vh8qps2l7l3ip45mlg0f9ycild309dnr")))

