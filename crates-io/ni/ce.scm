(define-module (crates-io ni ce) #:use-module (crates-io))

(define-public crate-nice-0.0.1 (crate (name "nice") (vers "0.0.1") (hash "17py8h8rw9b1vvass5ycg94n7g2b8g0n07k2jp43qqz5jf1bxyx3")))

(define-public crate-nice-0.0.2 (crate (name "nice") (vers "0.0.2") (hash "0chqlhpakhzw96br5z89lgqhjlk782ypxj20ym0vp6h29gsxgvgl")))

(define-public crate-nice-0.0.3 (crate (name "nice") (vers "0.0.3") (hash "023xza8rs6gf2msyh1hzycblg5pqjnw3gzip1mdplyjf41g50iqy")))

(define-public crate-nice-0.0.4 (crate (name "nice") (vers "0.0.4") (hash "0r21clvlhs76yj4zcbh06ilfnj79hmg6wsvc13lfvvqz2a2gal2c")))

(define-public crate-nice-0.0.5 (crate (name "nice") (vers "0.0.5") (hash "00kfq8sb295iqif3w46n20w3ykdfbxjncv0chcavgdk5z1v1pndm")))

(define-public crate-nice-0.0.6 (crate (name "nice") (vers "0.0.6") (hash "01k6l0zn77ws0vlxhkr5dkvf888g0rljk19mccppnlj9ra6al0w2")))

(define-public crate-nice-0.0.7 (crate (name "nice") (vers "0.0.7") (hash "1agggw1d7sqi8fzlh3i6b6l7208654x17dxm34haawn7cw8arf6d")))

(define-public crate-nice-0.0.8 (crate (name "nice") (vers "0.0.8") (hash "1qgmvndak0s10z4zp8zh5x1l0p80siaqhj993rj7lw83il4s4a57")))

(define-public crate-nice-0.0.9 (crate (name "nice") (vers "0.0.9") (hash "0m62ry2q468hmchi2ibp4x5awz7qsndzphb6ikfmy1jnjjq4jrym")))

(define-public crate-nice-0.0.10 (crate (name "nice") (vers "0.0.10") (hash "062snyf9lj0qir77649fj72rlj8jxwv05d7cnlb57p8lclh1483v")))

(define-public crate-nice-0.0.11 (crate (name "nice") (vers "0.0.11") (hash "10zj7g8n0x6g8l28xv2wzjbsi18kqwwjizdg89clnazk17gkw7mh")))

(define-public crate-nice-0.0.12 (crate (name "nice") (vers "0.0.12") (hash "003v8bb8lpv5zpfi5vb60naqcrgcj9lbxh8w4n22f8cwapxdvj9b")))

(define-public crate-nice-0.0.13 (crate (name "nice") (vers "0.0.13") (hash "17nbb01zfzaylwvn8bff0qxvp0f7j5hyvg40k79npkydmyg3axzr")))

(define-public crate-nice-0.0.14 (crate (name "nice") (vers "0.0.14") (hash "14kaqirds4p3y27va23i4nmi8aw3nggjy110z0sfiyjscr95hd6p")))

(define-public crate-nice-0.0.15 (crate (name "nice") (vers "0.0.15") (hash "1kdrqh0pq7fhikgshna2xxjm8xlpdd603bkggjdlikrgvfnq7rvz")))

(define-public crate-nice-0.0.16 (crate (name "nice") (vers "0.0.16") (hash "1s3zpwzpa447rvr9al3f6q38rq5d8hg227g7bky5bv5s5zasbi38")))

(define-public crate-nice-0.0.17 (crate (name "nice") (vers "0.0.17") (hash "0xhznc8ypghgjb99cnzdbfz4qyrrynnm5zn0rb18fdqr74nlxdkz")))

(define-public crate-nice-0.0.18 (crate (name "nice") (vers "0.0.18") (hash "0h27mp8158f7cxfrsf6kqj0cbypbi6grapzfcjp3qvin3gvapgsy")))

(define-public crate-nice-0.0.19 (crate (name "nice") (vers "0.0.19") (hash "10hbx2qv1vcnwlz5a0rd6jjvcqcyknh84pqriqpjv9k9pgb40siz")))

(define-public crate-nice-0.0.20 (crate (name "nice") (vers "0.0.20") (hash "0apywakrb6w7pfh0q12nraychxknazd7kc3ph2x1njfn5qkpnx46")))

(define-public crate-nice-0.0.21 (crate (name "nice") (vers "0.0.21") (deps (list (crate-dep (name "libc") (req "*") (default-features #t) (kind 0)) (crate-dep (name "log") (req "*") (default-features #t) (kind 0)))) (hash "16fc61x4c1n0nqbq8z90qpfy2iwiazg9l1nsx3y59ndirvya2y0b")))

(define-public crate-nice-0.0.22 (crate (name "nice") (vers "0.0.22") (deps (list (crate-dep (name "libc") (req "*") (default-features #t) (kind 0)) (crate-dep (name "log") (req "*") (default-features #t) (kind 0)))) (hash "198cwc7j5q1xd9vlb0pwm9jc6b6qg5lvqm6ifv0ywadvrm3hd2rv")))

(define-public crate-nice-0.1 (crate (name "nice") (vers "0.1.0") (deps (list (crate-dep (name "bitflags") (req "*") (default-features #t) (kind 0)) (crate-dep (name "condition_variable") (req "*") (default-features #t) (kind 0)) (crate-dep (name "env_logger") (req "*") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "*") (default-features #t) (kind 0)) (crate-dep (name "log") (req "*") (default-features #t) (kind 0)))) (hash "0sikmwmbw7jlv74kr854xagdgbpz30nhqb48iylq8040vwk139sv")))

(define-public crate-nice-0.1 (crate (name "nice") (vers "0.1.1") (deps (list (crate-dep (name "bitflags") (req "*") (default-features #t) (kind 0)) (crate-dep (name "condition_variable") (req "*") (default-features #t) (kind 0)) (crate-dep (name "env_logger") (req "*") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "*") (default-features #t) (kind 0)) (crate-dep (name "log") (req "*") (default-features #t) (kind 0)))) (hash "1ss7n3bcb1q5blbcj093lrbi5kvypsl4chxsa19kfwaxdyyrny4x")))

(define-public crate-nice-0.1 (crate (name "nice") (vers "0.1.3") (deps (list (crate-dep (name "bitflags") (req "*") (default-features #t) (kind 0)) (crate-dep (name "condition_variable") (req "*") (default-features #t) (kind 0)) (crate-dep (name "env_logger") (req "*") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "*") (default-features #t) (kind 0)) (crate-dep (name "log") (req "*") (default-features #t) (kind 0)))) (hash "0nxjdfcvbj8j53x5dhqnbvmzsp06yl5r9hpf5kx4s1cib5ibwxlr")))

(define-public crate-nice-0.1 (crate (name "nice") (vers "0.1.4") (deps (list (crate-dep (name "bitflags") (req "*") (default-features #t) (kind 0)) (crate-dep (name "condition_variable") (req "*") (default-features #t) (kind 0)) (crate-dep (name "env_logger") (req "*") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "*") (default-features #t) (kind 0)) (crate-dep (name "log") (req "*") (default-features #t) (kind 0)))) (hash "1rxaiczgbajaz5j7scvsh3a9qbcarn81adf73ddamv9f5wwg1cs3")))

(define-public crate-nice-0.1 (crate (name "nice") (vers "0.1.5") (deps (list (crate-dep (name "bitflags") (req "*") (default-features #t) (kind 0)) (crate-dep (name "condition_variable") (req "*") (default-features #t) (kind 0)) (crate-dep (name "env_logger") (req "*") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "*") (default-features #t) (kind 0)) (crate-dep (name "log") (req "*") (default-features #t) (kind 0)))) (hash "19liln6jk250k2qaf56cq1qnqx5idh2kf438fn03ikvinmw9r0pw")))

(define-public crate-nice-0.1 (crate (name "nice") (vers "0.1.6") (deps (list (crate-dep (name "bitflags") (req "*") (default-features #t) (kind 0)) (crate-dep (name "condition_variable") (req "*") (default-features #t) (kind 0)) (crate-dep (name "env_logger") (req "*") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "*") (default-features #t) (kind 0)) (crate-dep (name "log") (req "*") (default-features #t) (kind 0)))) (hash "12hqgzi6l884abjqy6ny0vrkah929yf9i5yg03vj1h420c5xzw06")))

(define-public crate-nice-0.1 (crate (name "nice") (vers "0.1.7") (deps (list (crate-dep (name "bitflags") (req "*") (default-features #t) (kind 0)) (crate-dep (name "condition_variable") (req "*") (default-features #t) (kind 0)) (crate-dep (name "env_logger") (req "*") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "*") (default-features #t) (kind 0)) (crate-dep (name "log") (req "*") (default-features #t) (kind 0)))) (hash "1bgl3v09mhy4z4kv8iyyskcdgldy41si5lhpz7xmswi0k1liig0n")))

(define-public crate-nice-0.1 (crate (name "nice") (vers "0.1.8") (deps (list (crate-dep (name "bitflags") (req "*") (default-features #t) (kind 0)) (crate-dep (name "condition_variable") (req "*") (default-features #t) (kind 0)) (crate-dep (name "env_logger") (req "*") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "*") (default-features #t) (kind 0)) (crate-dep (name "log") (req "*") (default-features #t) (kind 0)))) (hash "08s3k3nhs8znacks0fm0a11yxjxi5hmhg2mklrz0x7a2sapdyva7")))

(define-public crate-nice-0.1 (crate (name "nice") (vers "0.1.9") (deps (list (crate-dep (name "bitflags") (req "*") (default-features #t) (kind 0)) (crate-dep (name "condition_variable") (req "*") (default-features #t) (kind 0)) (crate-dep (name "env_logger") (req "*") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "*") (default-features #t) (kind 0)) (crate-dep (name "log") (req "*") (default-features #t) (kind 0)))) (hash "054srr4ynzqs1681abvdvinyz6wsd2ws8dp20cw6mpld5pp2pdzq")))

(define-public crate-nice-gst-meet-0.1 (crate (name "nice-gst-meet") (vers "0.1.0") (deps (list (crate-dep (name "bitflags") (req "^1") (optional #t) (kind 0)) (crate-dep (name "glib") (req "^0.14") (kind 0)) (crate-dep (name "libc") (req "^0.2") (kind 0)) (crate-dep (name "nice-gst-meet-sys") (req "^0.1") (kind 0)) (crate-dep (name "nix") (req "^0.22") (kind 0)))) (hash "0ibnlly9ws2kslzvji3s11c42flyk610w7kkg5sr7plc11b6f3q1") (features (quote (("v0_1_8" "nice-gst-meet-sys/v0_1_8" "v0_1_6") ("v0_1_6" "nice-gst-meet-sys/v0_1_6" "v0_1_5") ("v0_1_5" "nice-gst-meet-sys/v0_1_5" "v0_1_4") ("v0_1_4" "nice-gst-meet-sys/v0_1_4") ("v0_1_20" "nice-gst-meet-sys/v0_1_20" "v0_1_18") ("v0_1_18" "nice-gst-meet-sys/v0_1_18" "v0_1_17") ("v0_1_17" "nice-gst-meet-sys/v0_1_17" "v0_1_16") ("v0_1_16" "nice-gst-meet-sys/v0_1_16" "v0_1_15") ("v0_1_15" "nice-gst-meet-sys/v0_1_15" "v0_1_14" "bitflags") ("v0_1_14" "nice-gst-meet-sys/v0_1_14" "v0_1_8") ("dox"))))))

(define-public crate-nice-gst-meet-0.1 (crate (name "nice-gst-meet") (vers "0.1.1") (deps (list (crate-dep (name "bitflags") (req "^1") (optional #t) (kind 0)) (crate-dep (name "glib") (req "^0.14") (kind 0)) (crate-dep (name "libc") (req "^0.2") (kind 0)) (crate-dep (name "nice-gst-meet-sys") (req "^0.1") (kind 0)) (crate-dep (name "nix") (req "^0.22") (kind 0)))) (hash "02hss1x2haz9xljqvl5s5y5qjsivchmhrk3x3wlya06lm04cc406") (features (quote (("v0_1_8" "nice-gst-meet-sys/v0_1_8" "v0_1_6") ("v0_1_6" "nice-gst-meet-sys/v0_1_6" "v0_1_5") ("v0_1_5" "nice-gst-meet-sys/v0_1_5" "v0_1_4") ("v0_1_4" "nice-gst-meet-sys/v0_1_4") ("v0_1_20" "nice-gst-meet-sys/v0_1_20" "v0_1_18") ("v0_1_18" "nice-gst-meet-sys/v0_1_18" "v0_1_17") ("v0_1_17" "nice-gst-meet-sys/v0_1_17" "v0_1_16") ("v0_1_16" "nice-gst-meet-sys/v0_1_16" "v0_1_15") ("v0_1_15" "nice-gst-meet-sys/v0_1_15" "v0_1_14" "bitflags") ("v0_1_14" "nice-gst-meet-sys/v0_1_14" "v0_1_8") ("dox"))))))

(define-public crate-nice-gst-meet-0.2 (crate (name "nice-gst-meet") (vers "0.2.0") (deps (list (crate-dep (name "bitflags") (req "^1") (optional #t) (kind 0)) (crate-dep (name "glib") (req "^0.15") (kind 0)) (crate-dep (name "libc") (req "^0.2") (kind 0)) (crate-dep (name "nice-gst-meet-sys") (req "^0.2") (kind 0)) (crate-dep (name "nix") (req "^0.23") (kind 0)))) (hash "1rmchy27h61q2zqnqph5jds7dm24041gzz7m417yc4jd5sa3d5sg") (features (quote (("v0_1_8" "nice-gst-meet-sys/v0_1_8" "v0_1_6") ("v0_1_6" "nice-gst-meet-sys/v0_1_6" "v0_1_5") ("v0_1_5" "nice-gst-meet-sys/v0_1_5" "v0_1_4") ("v0_1_4" "nice-gst-meet-sys/v0_1_4") ("v0_1_20" "nice-gst-meet-sys/v0_1_20" "v0_1_18") ("v0_1_18" "nice-gst-meet-sys/v0_1_18" "v0_1_17") ("v0_1_17" "nice-gst-meet-sys/v0_1_17" "v0_1_16") ("v0_1_16" "nice-gst-meet-sys/v0_1_16" "v0_1_15") ("v0_1_15" "nice-gst-meet-sys/v0_1_15" "v0_1_14" "bitflags") ("v0_1_14" "nice-gst-meet-sys/v0_1_14" "v0_1_8") ("dox"))))))

(define-public crate-nice-gst-meet-0.3 (crate (name "nice-gst-meet") (vers "0.3.0") (deps (list (crate-dep (name "bitflags") (req "^2") (optional #t) (kind 0)) (crate-dep (name "glib") (req "^0.17") (kind 0)) (crate-dep (name "libc") (req "^0.2") (kind 0)) (crate-dep (name "nice-gst-meet-sys") (req "^0.3") (kind 0)) (crate-dep (name "nix") (req "^0.26") (features (quote ("socket" "net"))) (kind 0)))) (hash "0ixf13qghs946icw0m9char0l1r2gji1a1j3nii509alx59z6hih") (features (quote (("v0_1_8" "nice-gst-meet-sys/v0_1_8" "v0_1_6") ("v0_1_6" "nice-gst-meet-sys/v0_1_6" "v0_1_5") ("v0_1_5" "nice-gst-meet-sys/v0_1_5" "v0_1_4") ("v0_1_4" "nice-gst-meet-sys/v0_1_4") ("v0_1_20" "nice-gst-meet-sys/v0_1_20" "v0_1_18") ("v0_1_18" "nice-gst-meet-sys/v0_1_18" "v0_1_17") ("v0_1_17" "nice-gst-meet-sys/v0_1_17" "v0_1_16") ("v0_1_16" "nice-gst-meet-sys/v0_1_16" "v0_1_15") ("v0_1_15" "nice-gst-meet-sys/v0_1_15" "v0_1_14" "bitflags") ("v0_1_14" "nice-gst-meet-sys/v0_1_14" "v0_1_8") ("dox"))))))

(define-public crate-nice-gst-meet-sys-0.1 (crate (name "nice-gst-meet-sys") (vers "0.1.0") (deps (list (crate-dep (name "gio") (req "^0.14") (kind 0)) (crate-dep (name "glib") (req "^0.14") (kind 0)) (crate-dep (name "libc") (req "^0.2") (kind 0)) (crate-dep (name "shell-words") (req "^1") (kind 2)) (crate-dep (name "system-deps") (req "^3") (kind 1)) (crate-dep (name "tempfile") (req "^3") (kind 2)))) (hash "05v03mq93gmshm27alsjaj1jalpnix6y0m5v1kmsj8yw4a49mz1s") (features (quote (("v0_1_8" "v0_1_6") ("v0_1_6" "v0_1_5") ("v0_1_5" "v0_1_4") ("v0_1_4") ("v0_1_20" "v0_1_18") ("v0_1_18" "v0_1_17") ("v0_1_17" "v0_1_16") ("v0_1_16" "v0_1_15") ("v0_1_15" "v0_1_14") ("v0_1_14" "v0_1_8") ("dox")))) (links "nice")))

(define-public crate-nice-gst-meet-sys-0.2 (crate (name "nice-gst-meet-sys") (vers "0.2.0") (deps (list (crate-dep (name "gio") (req "^0.15") (kind 0)) (crate-dep (name "glib") (req "^0.15") (kind 0)) (crate-dep (name "libc") (req "^0.2") (kind 0)) (crate-dep (name "shell-words") (req "^1") (kind 2)) (crate-dep (name "system-deps") (req "^6") (kind 1)) (crate-dep (name "tempfile") (req "^3") (kind 2)))) (hash "0jmw0b9kd5zgx086l14l74bdnhi859fh2lvsk63wmv2g4y9gxnyz") (features (quote (("v0_1_8" "v0_1_6") ("v0_1_6" "v0_1_5") ("v0_1_5" "v0_1_4") ("v0_1_4") ("v0_1_20" "v0_1_18") ("v0_1_18" "v0_1_17") ("v0_1_17" "v0_1_16") ("v0_1_16" "v0_1_15") ("v0_1_15" "v0_1_14") ("v0_1_14" "v0_1_8") ("dox")))) (links "nice")))

(define-public crate-nice-gst-meet-sys-0.3 (crate (name "nice-gst-meet-sys") (vers "0.3.0") (deps (list (crate-dep (name "gio") (req "^0.17") (kind 0)) (crate-dep (name "glib") (req "^0.17") (kind 0)) (crate-dep (name "gobject-sys") (req "^0.17") (kind 0)) (crate-dep (name "libc") (req "^0.2") (kind 0)) (crate-dep (name "shell-words") (req "^1") (kind 2)) (crate-dep (name "system-deps") (req "^6") (kind 1)) (crate-dep (name "tempfile") (req "^3") (kind 2)))) (hash "05q2awqssjfc4d4fzyirjbwl2alsd7igfqfj7bwv6gzxhj5chqi7") (features (quote (("v0_1_8" "v0_1_6") ("v0_1_6" "v0_1_5") ("v0_1_5" "v0_1_4") ("v0_1_4") ("v0_1_20" "v0_1_18") ("v0_1_18" "v0_1_17") ("v0_1_17" "v0_1_16") ("v0_1_16" "v0_1_15") ("v0_1_15" "v0_1_14") ("v0_1_14" "v0_1_8") ("dox")))) (links "nice")))

(define-public crate-nice-numbers-1 (crate (name "nice-numbers") (vers "1.0.0") (hash "1m10rsbjhv9z5hz1r16nmkaah8zlqa4jzrw2gzaiglsmx36dm9vs")))

(define-public crate-nice-numbers-1 (crate (name "nice-numbers") (vers "1.0.1") (hash "0xs41l63g3lx8fhh13kzix2yah072lcg356a1zxz49wqrr7fxsr6")))

(define-public crate-nice-numbers-1 (crate (name "nice-numbers") (vers "1.0.2") (hash "0xv7q725vx4fglz0pf6f50q9ahd9zcxr4y73vmywcrcj1lb5r1sm")))

(define-public crate-nice-numbers-1 (crate (name "nice-numbers") (vers "1.0.3") (hash "1hrvn2pkinliwajdbdb36jq06nxpym8xm4i5af55fkvqzky9l8qk")))

(define-public crate-nice-sys-0.1 (crate (name "nice-sys") (vers "0.1.0") (deps (list (crate-dep (name "gio-sys") (req ">= 0.5") (default-features #t) (kind 0)) (crate-dep (name "glib-sys") (req ">= 0.5") (default-features #t) (kind 0)) (crate-dep (name "gobject-sys") (req ">= 0.5") (default-features #t) (kind 0)) (crate-dep (name "pkg-config") (req ">= 0.3") (default-features #t) (kind 1)))) (hash "0fj963kk4dlqcc8fw8792xk5wlk5lrbpvd5n36kjh54zlwcwq87d")))

(define-public crate-nice_glfw-1 (crate (name "nice_glfw") (vers "1.0.1") (deps (list (crate-dep (name "glfw") (req "*") (default-features #t) (kind 0)) (crate-dep (name "log") (req "*") (default-features #t) (kind 0)))) (hash "1icgg67k4rk08pzpsqf9anlc0s32hjs12s7mzzzj2dg5z7lnxh75")))

(define-public crate-nice_prompt-0.1 (crate (name "nice_prompt") (vers "0.1.0") (deps (list (crate-dep (name "ansi_term") (req "^0.12.1") (default-features #t) (kind 0)) (crate-dep (name "dirs") (req "^3.0.1") (default-features #t) (kind 0)) (crate-dep (name "git2") (req "^0.13.6") (default-features #t) (kind 0)) (crate-dep (name "itertools") (req "^0.9.0") (default-features #t) (kind 0)) (crate-dep (name "temp_testdir") (req "^0.2.3") (default-features #t) (kind 2)))) (hash "01rrs35mshsmkkwnh6j035zrj3f057rihsgnjlcwjvkzlvlvw6zh")))

(define-public crate-nice_prompt-0.1 (crate (name "nice_prompt") (vers "0.1.1") (deps (list (crate-dep (name "ansi_term") (req "^0.12.1") (default-features #t) (kind 0)) (crate-dep (name "anyhow") (req "^1.0.32") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^2.33.2") (default-features #t) (kind 0)) (crate-dep (name "dirs") (req "^3.0.1") (default-features #t) (kind 0)) (crate-dep (name "git2") (req "^0.13.6") (default-features #t) (kind 0)) (crate-dep (name "itertools") (req "^0.9.0") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.11") (default-features #t) (kind 0)) (crate-dep (name "simplelog") (req "^0.8.0") (default-features #t) (kind 0)) (crate-dep (name "temp_testdir") (req "^0.2.3") (default-features #t) (kind 2)))) (hash "1dv5ik7nn56d226a50nsgj20cym75arhxb4vv2zgfbkfjwp4gsdy")))

(define-public crate-niced-0.2 (crate (name "niced") (vers "0.2.0") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "procfs") (req "^0.8") (default-features #t) (kind 0)))) (hash "1skp4rvn8j7d9q7237iqvfd54s0k4v5nh8mzw4ncbcb6197h7lkm")))

(define-public crate-niced-0.2 (crate (name "niced") (vers "0.2.1") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "procfs") (req "^0.8") (default-features #t) (kind 0)))) (hash "0c6j9ih0nsjm898iy1hgs8zr33320jkzr5sm620rfnpfmiv6iydk")))

(define-public crate-nicegit-0.0.1 (crate (name "nicegit") (vers "0.0.1") (deps (list (crate-dep (name "git2") (req "^0.7") (default-features #t) (kind 0)))) (hash "0fr8xpi8h1dd2475x84dam59qna61yqj693apwvb8a6lahifv99s")))

(define-public crate-nicehash-0.1 (crate (name "nicehash") (vers "0.1.0") (deps (list (crate-dep (name "hyper") (req "^0.9") (features (quote ("serde-serialization"))) (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "semver") (req "^0.5") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^0.8") (default-features #t) (kind 0)))) (hash "1qpml2grz3lsv5djzzkvk6llj7s9m84nmynvaajczgc79v1qqh33")))

(define-public crate-nicehash-0.1 (crate (name "nicehash") (vers "0.1.1") (deps (list (crate-dep (name "hyper") (req "^0.9") (features (quote ("serde-serialization"))) (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "semver") (req "^0.5") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^0.8") (default-features #t) (kind 0)))) (hash "0yfz9pcxmrm4c578y53qx5837qv5m2dw0rzys8657qljw602l97g")))

(define-public crate-nicehash-0.1 (crate (name "nicehash") (vers "0.1.2") (deps (list (crate-dep (name "hyper") (req "^0.9") (features (quote ("serde-serialization"))) (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "semver") (req "^0.5") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^0.8") (default-features #t) (kind 0)))) (hash "01la9kfil25w7apxiqyg2gzi5r1jpdgrqc0g17ppy5cjdhhbkwfi")))

(define-public crate-nicehash-0.1 (crate (name "nicehash") (vers "0.1.4") (deps (list (crate-dep (name "hyper") (req "^0.9") (features (quote ("serde-serialization"))) (default-features #t) (kind 0)) (crate-dep (name "semver") (req "^0.5") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^0.8") (default-features #t) (kind 0)))) (hash "0wkjgl4v0dj1c4i3sssqj5bz41fl4njb5njjdg46r2fkagn326s7")))

(define-public crate-nicehash-0.2 (crate (name "nicehash") (vers "0.2.0") (deps (list (crate-dep (name "hyper") (req "^0.9") (features (quote ("serde-serialization"))) (default-features #t) (kind 0)) (crate-dep (name "semver") (req "^0.5") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^0.8") (default-features #t) (kind 0)))) (hash "0czxggg7ffj38nr4kp0108xpa75k9jl7kwbpkwcgb1605z950825")))

(define-public crate-nicehttp-0.1 (crate (name "nicehttp") (vers "0.1.0") (hash "050indwhbwmkdjkmxpdhn63h6b4337blsipsajncl7f4vsada5r3")))

(define-public crate-nicelocal-ext-php-rs-0.10 (crate (name "nicelocal-ext-php-rs") (vers "0.10.1") (deps (list (crate-dep (name "anyhow") (req "^1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "anyhow") (req "^1") (default-features #t) (kind 1)) (crate-dep (name "bindgen") (req "^0.65.1") (default-features #t) (kind 1)) (crate-dep (name "bitflags") (req "^2") (default-features #t) (kind 0)) (crate-dep (name "cc") (req "^1.0") (default-features #t) (kind 1)) (crate-dep (name "cfg-if") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "ext-php-rs-derive") (req "=0.10.0") (default-features #t) (kind 0)) (crate-dep (name "native-tls") (req "^0.2") (default-features #t) (target "cfg(windows)") (kind 1)) (crate-dep (name "once_cell") (req "^1.17") (default-features #t) (kind 0)) (crate-dep (name "parking_lot") (req "^0.12") (default-features #t) (kind 0)) (crate-dep (name "skeptic") (req "^0.13") (default-features #t) (kind 1)) (crate-dep (name "skeptic") (req "^0.13") (default-features #t) (kind 2)) (crate-dep (name "ureq") (req "^2.4") (features (quote ("native-tls" "gzip"))) (target "cfg(windows)") (kind 1)) (crate-dep (name "zip") (req "^0.6") (default-features #t) (target "cfg(windows)") (kind 1)))) (hash "1svngzj671gfbviz16xdmfz9xj0q04j4vcf88ff8xq3skcz8fak1") (features (quote (("closure")))) (yanked #t)))

(define-public crate-nicelocal-ext-php-rs-0.10 (crate (name "nicelocal-ext-php-rs") (vers "0.10.3") (deps (list (crate-dep (name "anyhow") (req "^1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "anyhow") (req "^1") (default-features #t) (kind 1)) (crate-dep (name "bindgen") (req "^0.65.1") (default-features #t) (kind 1)) (crate-dep (name "bitflags") (req "^2") (default-features #t) (kind 0)) (crate-dep (name "cc") (req "^1.0") (default-features #t) (kind 1)) (crate-dep (name "cfg-if") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "ext-php-rs-derive") (req "=0.10.0") (default-features #t) (kind 0)) (crate-dep (name "native-tls") (req "^0.2") (default-features #t) (target "cfg(windows)") (kind 1)) (crate-dep (name "once_cell") (req "^1.17") (default-features #t) (kind 0)) (crate-dep (name "parking_lot") (req "^0.12") (default-features #t) (kind 0)) (crate-dep (name "skeptic") (req "^0.13") (default-features #t) (kind 1)) (crate-dep (name "skeptic") (req "^0.13") (default-features #t) (kind 2)) (crate-dep (name "ureq") (req "^2.4") (features (quote ("native-tls" "gzip"))) (target "cfg(windows)") (kind 1)) (crate-dep (name "zip") (req "^0.6") (default-features #t) (target "cfg(windows)") (kind 1)))) (hash "0xw7q11i5vnidq05606m2cckf2ia4wydmam7ghlc5zqjgr0f5xfk") (features (quote (("closure")))) (yanked #t)))

(define-public crate-nicelocal-ext-php-rs-0.10 (crate (name "nicelocal-ext-php-rs") (vers "0.10.4") (deps (list (crate-dep (name "anyhow") (req "^1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "anyhow") (req "^1") (default-features #t) (kind 1)) (crate-dep (name "bindgen") (req "^0.65.1") (default-features #t) (kind 1)) (crate-dep (name "bitflags") (req "^2") (default-features #t) (kind 0)) (crate-dep (name "cc") (req "^1.0") (default-features #t) (kind 1)) (crate-dep (name "cfg-if") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "native-tls") (req "^0.2") (default-features #t) (target "cfg(windows)") (kind 1)) (crate-dep (name "nicelocal-ext-php-rs-derive") (req "=0.10.1") (default-features #t) (kind 0)) (crate-dep (name "once_cell") (req "^1.17") (default-features #t) (kind 0)) (crate-dep (name "parking_lot") (req "^0.12") (default-features #t) (kind 0)) (crate-dep (name "skeptic") (req "^0.13") (default-features #t) (kind 1)) (crate-dep (name "skeptic") (req "^0.13") (default-features #t) (kind 2)) (crate-dep (name "ureq") (req "^2.4") (features (quote ("native-tls" "gzip"))) (target "cfg(windows)") (kind 1)) (crate-dep (name "zip") (req "^0.6") (default-features #t) (target "cfg(windows)") (kind 1)))) (hash "0dz6v1m21070m3y5bqsl0csas30xkg12aq6n99qqa6r89n73bvi0") (features (quote (("closure")))) (yanked #t)))

(define-public crate-nicelocal-ext-php-rs-derive-0.10 (crate (name "nicelocal-ext-php-rs-derive") (vers "0.10.1") (deps (list (crate-dep (name "anyhow") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "darling") (req "^0.14") (default-features #t) (kind 0)) (crate-dep (name "ident_case") (req "^1.0.1") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1.0.26") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.9") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.68") (features (quote ("full" "extra-traits" "printing"))) (default-features #t) (kind 0)))) (hash "19x9jfbd2ych2gf6yi9x0w97clbm8hsnjvlfn285mixl94266q54") (yanked #t)))

(define-public crate-niceui-0.1 (crate (name "niceui") (vers "0.1.0") (hash "0sk75pjn5qdr1kg5dslv1xcv98rdyfndb0gzhbdhgazlfy26pkix")))

(define-public crate-niceware-0.4 (crate (name "niceware") (vers "0.4.0") (deps (list (crate-dep (name "rand") (req "^0.8.4") (default-features #t) (kind 0)))) (hash "0mpm97j63pnvmzmk165asijny26k4wi6rckh8s1fsr33zr1v1xcd")))

(define-public crate-niceware-0.5 (crate (name "niceware") (vers "0.5.0") (deps (list (crate-dep (name "rand") (req "^0.8.4") (default-features #t) (kind 0)))) (hash "1za2dmqn0dikkg6k3fz1kcwrf19mxlfz9kwmwn09739j0ahv55ar")))

(define-public crate-niceware-0.6 (crate (name "niceware") (vers "0.6.0") (deps (list (crate-dep (name "rand") (req "^0.8.4") (default-features #t) (kind 0)))) (hash "1ym1b6bq4mpnbypi84fpifqnp4dp4l6rdh385yfvzc9r0lpyzif6")))

(define-public crate-niceware-1 (crate (name "niceware") (vers "1.0.0") (deps (list (crate-dep (name "rand") (req "^0.8.4") (default-features #t) (kind 0)))) (hash "0ggw2xcss4mkk37wazyvhww7zavvgwf4s7pm22zn13s38byqwm5z")))

