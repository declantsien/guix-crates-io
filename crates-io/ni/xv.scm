(define-module (crates-io ni xv) #:use-module (crates-io))

(define-public crate-nixv-0.0.1 (crate (name "nixv") (vers "0.0.1") (deps (list (crate-dep (name "libc") (req "^0.2.64") (default-features #t) (kind 0)))) (hash "1dywvawj0clx7fkcc8bhhy035hal1xib168a1y56gp21535ymn57")))

