(define-module (crates-io ni _d) #:use-module (crates-io))

(define-public crate-ni_display-0.1 (crate (name "ni_display") (vers "0.1.0") (deps (list (crate-dep (name "embedded-graphics") (req "^0.7.1") (default-features #t) (kind 0)) (crate-dep (name "embedded-graphics-core") (req "^0.3.3") (default-features #t) (kind 0)) (crate-dep (name "rusb") (req "^0.9.1") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.30") (default-features #t) (kind 0)) (crate-dep (name "zerocopy") (req "^0.6.1") (default-features #t) (kind 0)))) (hash "1yi8y9h2rxs6m3l799pn2yfah42vaic1ajjrh1qx9igrl67521ij")))

