(define-module (crates-io ni xt) #:use-module (crates-io))

(define-public crate-nixterm-0.1 (crate (name "nixterm") (vers "0.1.0") (deps (list (crate-dep (name "failure") (req "^0.1.1") (default-features #t) (kind 0)) (crate-dep (name "failure_derive") (req "^0.1.1") (default-features #t) (kind 0)) (crate-dep (name "nix") (req "^0.11.0") (default-features #t) (kind 0)))) (hash "1w8ss5473vag80a4xb583snv7jpns4i9ai73qxq7ipf8cw1ihn7c")))

(define-public crate-nixterm-0.1 (crate (name "nixterm") (vers "0.1.1") (deps (list (crate-dep (name "failure") (req "^0.1.1") (default-features #t) (kind 0)) (crate-dep (name "failure_derive") (req "^0.1.1") (default-features #t) (kind 0)) (crate-dep (name "nix") (req "^0.11.0") (default-features #t) (kind 0)))) (hash "15pfq5gnrprrhmmd5nxpkiflq4l3314jpf3bi4x5587hqhy9mckf")))

(define-public crate-nixterm-0.1 (crate (name "nixterm") (vers "0.1.2") (deps (list (crate-dep (name "failure") (req "^0.1.1") (default-features #t) (kind 0)) (crate-dep (name "failure_derive") (req "^0.1.1") (default-features #t) (kind 0)) (crate-dep (name "memchr") (req "^2.1.0") (default-features #t) (kind 0)) (crate-dep (name "nix") (req "^0.11.0") (default-features #t) (kind 0)))) (hash "02ssmyjhcpqrrpznpr50iwbykpz1hsa29n64dfyvdc9pf7hsqq6d")))

