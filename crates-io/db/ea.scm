(define-module (crates-io db ea) #:use-module (crates-io))

(define-public crate-DBeaverPasswordViewer-0.1 (crate (name "DBeaverPasswordViewer") (vers "0.1.0") (deps (list (crate-dep (name "aes") (req "^0.7.5") (default-features #t) (kind 0)) (crate-dep (name "block-modes") (req "^0.8.1") (default-features #t) (kind 0)) (crate-dep (name "hex") (req "^0.4.3") (default-features #t) (kind 0)) (crate-dep (name "hex-literal") (req "^0.3.3") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)))) (hash "0xnngjv9fj18zpaqw5m4ciihd8anazs18fbhv49vhj3iz1b68pwk")))

