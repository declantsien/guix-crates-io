(define-module (crates-io db so) #:use-module (crates-io))

(define-public crate-dbson-0.0.1 (crate (name "dbson") (vers "0.0.1") (deps (list (crate-dep (name "bson") (req "^2.4.0") (default-features #t) (kind 0)) (crate-dep (name "rusqlite") (req "^0.29.0") (optional #t) (kind 0)) (crate-dep (name "serde") (req "^1") (kind 0)) (crate-dep (name "sqlx") (req "^0.7.2") (optional #t) (kind 0)))) (hash "1pyq2rmh4vlr721hm04wi8bhblhhvwwmq57z6jgqngb9xkzylwg0") (v 2) (features2 (quote (("sqlx" "dep:sqlx") ("rusqlite" "dep:rusqlite"))))))

