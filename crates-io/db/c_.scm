(define-module (crates-io db c_) #:use-module (crates-io))

(define-public crate-dbc_parser-0.1 (crate (name "dbc_parser") (vers "0.1.0") (deps (list (crate-dep (name "prost") (req "^0.11.9") (default-features #t) (kind 0)))) (hash "1m25iah5jycr57la9071459hqs9qf9mhpkb2li8l2ywgv2p55f2d") (rust-version "1.70.0")))

