(define-module (crates-io db -k) #:use-module (crates-io))

(define-public crate-db-key-0.0.1 (crate (name "db-key") (vers "0.0.1") (hash "0z57vm7nxrlg1xhg79v7pmhv8i4qh6i05qf8h39y249v05p4jc2v")))

(define-public crate-db-key-0.0.5 (crate (name "db-key") (vers "0.0.5") (hash "0kyi54wy9k70bbbc6ir7kimxv59s07rzf1ygv4an13sidps6a95p")))

(define-public crate-db-key-0.1 (crate (name "db-key") (vers "0.1.0") (hash "0yf494wkhm5w4205hqzk2hcw2iqj3qhy1a59k360lb5fk4cipn0k")))

