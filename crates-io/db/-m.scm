(define-module (crates-io db -m) #:use-module (crates-io))

(define-public crate-db-map-0.1 (crate (name "db-map") (vers "0.1.0") (deps (list (crate-dep (name "anyhow") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "rusqlite") (req "^0.31") (features (quote ("bundled"))) (default-features #t) (kind 0)))) (hash "124n62smsxnr29qmpl20cclkwfq1fdiaggwddkcfvw3bria65js2")))

