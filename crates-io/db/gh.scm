(define-module (crates-io db gh) #:use-module (crates-io))

(define-public crate-dbghelp-sys-0.0.1 (crate (name "dbghelp-sys") (vers "0.0.1") (deps (list (crate-dep (name "winapi") (req "*") (default-features #t) (kind 0)))) (hash "1igr0c46ml0lp6zj18f5j4ix1vdhgir8n5vjgdvi7ifhjc702f7g")))

(define-public crate-dbghelp-sys-0.1 (crate (name "dbghelp-sys") (vers "0.1.0") (deps (list (crate-dep (name "winapi") (req "*") (default-features #t) (kind 0)) (crate-dep (name "winapi-build") (req "*") (default-features #t) (kind 1)))) (hash "1w1khnjf72j0k4691d09wr8immw4w34schw3zjvj1rrhd4nwaacc")))

(define-public crate-dbghelp-sys-0.2 (crate (name "dbghelp-sys") (vers "0.2.0") (deps (list (crate-dep (name "winapi") (req "^0.2.4") (default-features #t) (kind 0)) (crate-dep (name "winapi-build") (req "^0.1.1") (default-features #t) (kind 1)))) (hash "0l5qqm7pqr4i4lq67ymkix5gvl94m51sj70ng61c52nb7fjhnncp")))

