(define-module (crates-io db gt) #:use-module (crates-io))

(define-public crate-dbgtools-0.1 (crate (name "dbgtools") (vers "0.1.0") (deps (list (crate-dep (name "dbgtools-win") (req "^0.1") (default-features #t) (target "cfg(windows)") (kind 0)))) (hash "0qdg9d7fidl0145n789cirzmdjxajbdqnv8f0rj6rf1grr5nrfcj")))

(define-public crate-dbgtools-0.2 (crate (name "dbgtools") (vers "0.2.0") (deps (list (crate-dep (name "dbgtools-win") (req "^0.2") (default-features #t) (target "cfg(windows)") (kind 0)))) (hash "1dip4r0yz103jdh4ydrdjqsnwi0ijd6riig53daw1hx6z1xspw02")))

(define-public crate-dbgtools-0.2 (crate (name "dbgtools") (vers "0.2.1") (deps (list (crate-dep (name "dbgtools-hexdump") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "dbgtools-win") (req "^0.2") (default-features #t) (target "cfg(windows)") (kind 0)))) (hash "1n85i2nvbdgic3dr00a5nab7ksq3ps3d625zcxw4k1fry8yj24nf")))

(define-public crate-dbgtools-hexdump-0.1 (crate (name "dbgtools-hexdump") (vers "0.1.0") (hash "0s20qkhk1fgarwnclmjgk57zwlnjy18297i6k6qq4x6znqk61zz1")))

(define-public crate-dbgtools-win-0.1 (crate (name "dbgtools-win") (vers "0.1.0") (deps (list (crate-dep (name "backtrace") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "widestring") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "windows") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "windows") (req "^0.3") (default-features #t) (kind 1)))) (hash "0s7z3cwnrk1s2lapnpwrds7zf3qjngl7mq84f13f7d5isfv93bfm")))

(define-public crate-dbgtools-win-0.2 (crate (name "dbgtools-win") (vers "0.2.0") (deps (list (crate-dep (name "backtrace") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "chrono") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "widestring") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "windows") (req "^0.9") (default-features #t) (kind 0)) (crate-dep (name "windows") (req "^0.9") (default-features #t) (kind 1)))) (hash "1ijpsd599qrsr6qnd22vv5dy1rcmxjjp2izczfvyryisz1771nzs")))

(define-public crate-dbgtools-win-0.2 (crate (name "dbgtools-win") (vers "0.2.1") (deps (list (crate-dep (name "backtrace") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "chrono") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "widestring") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "windows") (req "^0.9") (default-features #t) (kind 0)) (crate-dep (name "windows") (req "^0.9") (default-features #t) (kind 1)))) (hash "09cl7q9mwxm57zfqsrxj97xzka0p9d1impyy18d5fhgl02wr63f1")))

