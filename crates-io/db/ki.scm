(define-module (crates-io db ki) #:use-module (crates-io))

(define-public crate-dbkit-engine-0.0.1 (crate (name "dbkit-engine") (vers "0.0.1") (deps (list (crate-dep (name "itertools") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "toml") (req "^0.2") (default-features #t) (kind 0)))) (hash "1ihvrswszl739bgz41z48ddqb935hc2v3zsh5g5ykbds0i3vllbb")))

(define-public crate-dbkit-engine-0.0.2 (crate (name "dbkit-engine") (vers "0.0.2") (deps (list (crate-dep (name "itertools") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "toml") (req "^0.2") (default-features #t) (kind 0)))) (hash "1bg5v0j5y856b5ijs3wkk2ql432ygg8fhh7lm1j0sqb98w62jsxk")))

(define-public crate-dbkit-engine-0.0.3 (crate (name "dbkit-engine") (vers "0.0.3") (deps (list (crate-dep (name "itertools") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "toml") (req "^0.2") (default-features #t) (kind 0)))) (hash "1gc0fc66rdws71zpqdnkq118fkdjwjwvdida0x4a8rxk21axknqh")))

(define-public crate-dbkit-engine-0.0.4 (crate (name "dbkit-engine") (vers "0.0.4") (deps (list (crate-dep (name "itertools") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "toml") (req "^0.2") (default-features #t) (kind 0)))) (hash "09y1hhvmm2ijaafszs7nl5qp0i2g12q9liqhbvg5ig7abdgbgwhw")))

(define-public crate-dbkit-engine-0.0.5 (crate (name "dbkit-engine") (vers "0.0.5") (deps (list (crate-dep (name "itertools") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "toml") (req "^0.2") (default-features #t) (kind 0)))) (hash "17x07j8lkzmldjr2im4qwiwvvii310jhm3044zmcd3d40ly3id3a")))

(define-public crate-dbkit-engine-0.0.6 (crate (name "dbkit-engine") (vers "0.0.6") (deps (list (crate-dep (name "itertools") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "toml") (req "^0.2") (default-features #t) (kind 0)))) (hash "0ixix5j4prjw0l1rdwz11qkxykc3x1lwvqwkqr2w9nsizlbvsphg")))

(define-public crate-dbkit-engine-0.0.7 (crate (name "dbkit-engine") (vers "0.0.7") (deps (list (crate-dep (name "itertools") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "toml") (req "^0.2") (default-features #t) (kind 0)))) (hash "0jvzkry6n6nav88cxhy4xi4bx2myipdrzmbviw94rs582rf1ymb1")))

(define-public crate-dbkit-engine-0.0.8 (crate (name "dbkit-engine") (vers "0.0.8") (deps (list (crate-dep (name "itertools") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "toml") (req "^0.2") (default-features #t) (kind 0)))) (hash "1rff2kh7ijry8nd70x9cqaldszrzns6j42sc85chm9v3kg1xkj8i")))

(define-public crate-dbkit-engine-0.0.9 (crate (name "dbkit-engine") (vers "0.0.9") (deps (list (crate-dep (name "itertools") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "toml") (req "^0.2") (default-features #t) (kind 0)))) (hash "197whg2zn48336602lawdwbhkvnb2br1fiqk0z6z5xv5hg75rpx9")))

