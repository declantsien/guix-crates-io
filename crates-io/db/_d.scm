(define-module (crates-io db _d) #:use-module (crates-io))

(define-public crate-db_dep-0.1 (crate (name "db_dep") (vers "0.1.0") (deps (list (crate-dep (name "rocksdb") (req "^0.19.0") (features (quote ("lz4"))) (kind 0)) (crate-dep (name "surrealdb") (req "^1.0.0-beta.8") (features (quote ("kv-rocksdb" "parallel"))) (kind 0)) (crate-dep (name "tokio") (req "^1.21.1") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "1vdbxnkmxpn6875qfd9acy5lx31bi3pazga7dph4jrhwvbfqs04b")))

(define-public crate-db_dep-0.1 (crate (name "db_dep") (vers "0.1.1") (deps (list (crate-dep (name "rocksdb") (req "^0.19.0") (features (quote ("lz4"))) (kind 0)) (crate-dep (name "surrealdb") (req "^1.0.0-beta.8") (features (quote ("kv-rocksdb" "parallel"))) (kind 0)) (crate-dep (name "tokio") (req "^1.21.1") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "1xca4s7vvrxaxnmx714n8jy2vml0hkgaj8jwsk9a58r9x9abkmy0") (features (quote (("test" "surrealdb/kv-mem" "surrealdb/parallel") ("default" "surrealdb/kv-rocksdb" "surrealdb/parallel"))))))

