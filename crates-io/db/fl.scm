(define-module (crates-io db fl) #:use-module (crates-io))

(define-public crate-dbflow-0.0.1 (crate (name "dbflow") (vers "0.0.1") (deps (list (crate-dep (name "clap") (req "^2.33") (default-features #t) (kind 0)) (crate-dep (name "postgres") (req "^0.15") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (default-features #t) (kind 0)))) (hash "10iqpqr0622ajmf86xq92b51968l0p9g75197p511qylffid57qh")))

