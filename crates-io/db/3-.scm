(define-module (crates-io db #{3-}#) #:use-module (crates-io))

(define-public crate-db3-proto-0.4 (crate (name "db3-proto") (vers "0.4.2") (deps (list (crate-dep (name "prost") (req "^0.11") (default-features #t) (kind 0)) (crate-dep (name "prost-types") (req "^0.12") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.114") (default-features #t) (kind 0)) (crate-dep (name "tonic") (req "^0.9.2") (default-features #t) (kind 0)) (crate-dep (name "tonic-build") (req "^0.9.2") (default-features #t) (kind 1)))) (hash "1pqmb7vm464m68nl3m0q4mpr98b7fwwm5y0iy67lz4nd0wpxcapx")))

(define-public crate-db3-sqlparser-0.0.1 (crate (name "db3-sqlparser") (vers "0.0.1") (deps (list (crate-dep (name "cc") (req "^1.0") (default-features #t) (kind 1)) (crate-dep (name "tree-sitter") (req "~0.20") (default-features #t) (kind 0)))) (hash "1g92g5fgsfn9jqzxbrj1bmrlalm8r9bygncw54k7a9vm2mxvfbhk")))

