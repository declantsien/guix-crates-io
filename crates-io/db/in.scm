(define-module (crates-io db in) #:use-module (crates-io))

(define-public crate-dbin-0.1 (crate (name "dbin") (vers "0.1.0") (hash "0650h29xi8lnkvwl8az1ch7k16q2225jrj3gdcfr3mncx5y5m9gl")))

(define-public crate-dbin-0.1 (crate (name "dbin") (vers "0.1.1") (hash "1z93pwyr08za1kq9xmfgjjlwh9xnwkp6g53l5gfwbg0n2z33p2i5")))

(define-public crate-dbin-0.1 (crate (name "dbin") (vers "0.1.2") (hash "0kwadqx0ajkrrbq1lxg42fnzk7wv9670apc9npy5qpqf5pq5sisy")))

(define-public crate-dbin-0.1 (crate (name "dbin") (vers "0.1.3") (hash "1qgny8zj8rci53ykdsk1980cxjghvl6qizlgg2wyw3msbbyx6yw0")))

(define-public crate-dbin-0.1 (crate (name "dbin") (vers "0.1.4") (hash "1b0j05qa10vls28l37h7pz5qzvfq6xl8j9wmslcxpzvmbiww85fg")))

(define-public crate-dbin-0.1 (crate (name "dbin") (vers "0.1.5") (hash "0vmsc49r6l2calr1gjxj1ll24b0y6ghzw6qd42rl5d9rzhhm64yj")))

(define-public crate-dbin-0.1 (crate (name "dbin") (vers "0.1.6") (hash "107abvfj46wm554d8xggbqarp9hhjsij6z8xz20v6c6zcdba4avz")))

(define-public crate-dbin-0.1 (crate (name "dbin") (vers "0.1.7") (hash "07rm0zclwbq2d4sa5x6mgwqykqkxwbh4ffpv1vbyl29l7qsp1pw3")))

