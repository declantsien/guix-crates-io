(define-module (crates-io db _m) #:use-module (crates-io))

(define-public crate-db_meta_derive-0.1 (crate (name "db_meta_derive") (vers "0.1.0") (deps (list (crate-dep (name "anyhow") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "askama") (req "^0.12.1") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1") (default-features #t) (kind 0)))) (hash "1jbfngjirg7w3m3wwb4xbncf57ms64dqnfyn9zgj3cj1sjhac2dn")))

