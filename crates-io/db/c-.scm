(define-module (crates-io db c-) #:use-module (crates-io))

(define-public crate-dbc-codegen-0.1 (crate (name "dbc-codegen") (vers "0.1.0") (deps (list (crate-dep (name "anyhow") (req "^1.0.33") (default-features #t) (kind 0)) (crate-dep (name "can-dbc") (req "^3.0.2") (default-features #t) (kind 0)) (crate-dep (name "heck") (req "^0.3.1") (default-features #t) (kind 0)))) (hash "16pyjdbykcrdjcs2cx5iz1a0wcq3nrisj3v61bbk3qb9ma19z6i0")))

(define-public crate-dbc-codegen-0.2 (crate (name "dbc-codegen") (vers "0.2.0") (deps (list (crate-dep (name "anyhow") (req "^1.0.33") (default-features #t) (kind 0)) (crate-dep (name "can-dbc") (req "^3.0.2") (default-features #t) (kind 0)) (crate-dep (name "heck") (req "^0.3.1") (default-features #t) (kind 0)))) (hash "0mppiqmq9vb6wgkwbs9a7acl8h00ijh7xi8xqy8x71rp39qfr63g") (features (quote (("std"))))))

(define-public crate-dbc-codegen-0.3 (crate (name "dbc-codegen") (vers "0.3.0") (deps (list (crate-dep (name "anyhow") (req "^1.0.68") (default-features #t) (kind 0)) (crate-dep (name "can-dbc") (req "^5.0.0") (default-features #t) (kind 0)) (crate-dep (name "heck") (req "^0.4.0") (default-features #t) (kind 0)))) (hash "1z3b13la8zc453c262rblnr3cr8qdaxgf156aqmk2pw9b76fy1nb") (features (quote (("std")))) (rust-version "1.73.0")))

(define-public crate-dbc-codegen-cli-0.1 (crate (name "dbc-codegen-cli") (vers "0.1.0") (deps (list (crate-dep (name "dbc-codegen") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "exitcode") (req "^1.1.2") (default-features #t) (kind 0)) (crate-dep (name "heck") (req "^0.3.1") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.3.20") (default-features #t) (kind 0)))) (hash "1w4zk0v2p4p15ybnvnkpcmcc4g5j9z8ylad5gp0my5z0qnm8d8xv")))

(define-public crate-dbc-codegen-cli-0.2 (crate (name "dbc-codegen-cli") (vers "0.2.0") (deps (list (crate-dep (name "dbc-codegen") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "exitcode") (req "^1.1.2") (default-features #t) (kind 0)) (crate-dep (name "heck") (req "^0.3.1") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.3.20") (default-features #t) (kind 0)))) (hash "0d2ly5afijp16vj3wzjw7hbhxw4m2qjb5jny29qkafwk5hcnnb1l")))

(define-public crate-dbc-codegen-cli-0.3 (crate (name "dbc-codegen-cli") (vers "0.3.0") (deps (list (crate-dep (name "clap") (req "^4.1") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "dbc-codegen") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "exitcode") (req "^1.1.2") (default-features #t) (kind 0)) (crate-dep (name "heck") (req "^0.4.0") (default-features #t) (kind 0)))) (hash "13nzn8s18vd2lvffn82ad2wvha2a5r4m01vd0l9wj7bgaay8kmba")))

