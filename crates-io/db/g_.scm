(define-module (crates-io db g_) #:use-module (crates-io))

(define-public crate-dbg_as_curl-1 (crate (name "dbg_as_curl") (vers "1.0.0") (deps (list (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.9.19") (kind 0)))) (hash "075xhhr48b9cfad9k5w0bqj9iml67wwa194fawxby1d05z6j2mj9")))

(define-public crate-dbg_hex-0.1 (crate (name "dbg_hex") (vers "0.1.0") (hash "0b4b1bs02awl2dhz0zna0nixm1paj567il9cpbx74gay9869hgr1")))

(define-public crate-dbg_hex-0.1 (crate (name "dbg_hex") (vers "0.1.1") (hash "0q1xvbdczjp2rqa8q6h1hinnv73cqrlmhmlbak8c9nz48knsnx1g")))

(define-public crate-dbg_hex-0.2 (crate (name "dbg_hex") (vers "0.2.0") (hash "1vl4i1zblc0zly934aqn3r5q6adn5b8kfpkzxrjmsaw7sjd0dprm") (rust-version "1.39.0")))

(define-public crate-dbg_if-0.1 (crate (name "dbg_if") (vers "0.1.0") (deps (list (crate-dep (name "approx") (req "^0.5.1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "atomic_float") (req "^0.1.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "gag") (req "^1.0.0") (default-features #t) (kind 2)) (crate-dep (name "regex") (req "^1.10.4") (default-features #t) (kind 2)) (crate-dep (name "version-sync") (req "^0.9") (default-features #t) (kind 2)))) (hash "1zafph53m93l6cdrj570yv3rxjl12yg3dzglvclpf5miv4z0fha0") (v 2) (features2 (quote (("float" "dep:approx" "dep:atomic_float"))))))

(define-public crate-dbg_mac-0.1 (crate (name "dbg_mac") (vers "0.1.0") (hash "1wi3vvzffjqj2bs0m2yj7lhc43wz25fsr1284wfwzjv6m37lgzkm")))

(define-public crate-dbg_mac-0.1 (crate (name "dbg_mac") (vers "0.1.1") (hash "133ncpa5wp6f6fd0lbxd8w5igkjfr6jz0ckl3ywpx29p7bap1nlv")))

(define-public crate-dbg_mac-0.1 (crate (name "dbg_mac") (vers "0.1.2") (hash "0vdi7hc8s50h91z85vp0vi5myva8cx31jk8cwa7fis7i7lvhr1bv")))

