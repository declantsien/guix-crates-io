(define-module (crates-io db g6) #:use-module (crates-io))

(define-public crate-dbg64_plugins_sdk_sys-0.1 (crate (name "dbg64_plugins_sdk_sys") (vers "0.1.0") (deps (list (crate-dep (name "bindgen") (req "^0.59.2") (default-features #t) (kind 1)))) (hash "0z0rx9xynl5i7mn81g076npqhla5k0943354xfh7pb4ql3f1f1lk")))

(define-public crate-dbg64_plugins_sdk_sys-0.1 (crate (name "dbg64_plugins_sdk_sys") (vers "0.1.1") (deps (list (crate-dep (name "bindgen") (req "^0.59.2") (default-features #t) (kind 1)))) (hash "1bk481q732zssggqvgr40320hbkf6w3rpi5l5bf6n0b502z0pqn2")))

(define-public crate-dbg64_plugins_sdk_sys-0.1 (crate (name "dbg64_plugins_sdk_sys") (vers "0.1.2") (deps (list (crate-dep (name "bindgen") (req "^0.59") (default-features #t) (kind 1)))) (hash "1wwqcgzccy2r6053a5gm5k3nl1mf0rwfa7yp1g3xsgx10x5m2mz2") (yanked #t)))

(define-public crate-dbg64_plugins_sdk_sys-0.1 (crate (name "dbg64_plugins_sdk_sys") (vers "0.1.3") (deps (list (crate-dep (name "bindgen") (req "^0.59") (default-features #t) (kind 1)))) (hash "1dib2h778kc2d7n04l7hqg1rmd5f805d29jp9dbv4c8vd0yqrhaq") (yanked #t)))

(define-public crate-dbg64_plugins_sdk_sys-0.1 (crate (name "dbg64_plugins_sdk_sys") (vers "0.1.4") (deps (list (crate-dep (name "bindgen") (req "^0.59") (default-features #t) (kind 1)))) (hash "0jjv680mh83vaahlrcka7zc40x62dvjw937yxwzb3gbmkpnvrxsp") (yanked #t)))

(define-public crate-dbg64_plugins_sdk_sys-0.1 (crate (name "dbg64_plugins_sdk_sys") (vers "0.1.5") (deps (list (crate-dep (name "bindgen") (req "^0.59") (default-features #t) (kind 1)) (crate-dep (name "cfg-if") (req "^1") (default-features #t) (kind 1)))) (hash "0xr1d5hlsgmpc76afqbgqwlyz4qg1r3xk9781r4xjpqaqc2s37qp") (features (quote (("x86") ("x64") ("default" "x64")))) (yanked #t)))

(define-public crate-dbg64_plugins_sdk_sys-0.1 (crate (name "dbg64_plugins_sdk_sys") (vers "0.1.6") (deps (list (crate-dep (name "bindgen") (req "^0.59") (default-features #t) (kind 1)) (crate-dep (name "cfg-if") (req "^1") (default-features #t) (kind 1)))) (hash "17va8d369zlwk7s547s4k2ciikcc9w52xnmliardhs3wqx36ibdw") (features (quote (("x86") ("x64") ("default" "x64")))) (yanked #t)))

(define-public crate-dbg64_plugins_sdk_sys-0.1 (crate (name "dbg64_plugins_sdk_sys") (vers "0.1.7") (deps (list (crate-dep (name "bindgen") (req "^0.59") (default-features #t) (kind 1)) (crate-dep (name "cfg-if") (req "^1") (default-features #t) (kind 1)))) (hash "1pngf3qs8h69l76wz53wqjbycmg99g5h2xff7yxb2r97a5ncc0lw") (features (quote (("x86") ("x64") ("default" "x64"))))))

