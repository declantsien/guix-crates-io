(define-module (crates-io db sd) #:use-module (crates-io))

(define-public crate-dbsdk-cli-0.1 (crate (name "dbsdk-cli") (vers "0.1.0") (deps (list (crate-dep (name "cargo_toml") (req "^0.11.6") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^3.2.22") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "fs_extra") (req "^1.2.0") (default-features #t) (kind 0)))) (hash "0sdz8gns3fjcdnh6kgwfj817as1v10nd5wmhby38yxyf595s8v8w")))

(define-public crate-dbsdk-cli-0.1 (crate (name "dbsdk-cli") (vers "0.1.1") (deps (list (crate-dep (name "cargo_toml") (req "^0.11.6") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^3.2.22") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "fs_extra") (req "^1.2.0") (default-features #t) (kind 0)))) (hash "0sfd9z0wqfqpk9ssiayq6lgc5kl1ln4pz9ikl896dna4d6lf8xaq")))

(define-public crate-dbsdk-cli-0.1 (crate (name "dbsdk-cli") (vers "0.1.2") (deps (list (crate-dep (name "cargo_toml") (req "^0.11.6") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^3.2.22") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "fs_extra") (req "^1.2.0") (default-features #t) (kind 0)))) (hash "14jcdiyqw2f3yc77kg69n72ycyyjs0np7a7h5a7wykwlaghfkqxr")))

(define-public crate-dbsdk-rs-0.1 (crate (name "dbsdk-rs") (vers "0.1.0") (deps (list (crate-dep (name "bitmask") (req "^0.5.0") (default-features #t) (kind 0)) (crate-dep (name "field-offset") (req "^0.3.4") (default-features #t) (kind 0)))) (hash "0yqldaydc225g544g68xlyrnylv1vgrjv1adjvszirknqd1pajs9")))

(define-public crate-dbsdk-rs-0.1 (crate (name "dbsdk-rs") (vers "0.1.1") (deps (list (crate-dep (name "bitmask") (req "^0.5.0") (default-features #t) (kind 0)) (crate-dep (name "field-offset") (req "^0.3.4") (default-features #t) (kind 0)))) (hash "1zqvfhw5msn71z7h734vsgscxj95szwwxkd99qls9dvblrj18x8x")))

(define-public crate-dbsdk-rs-0.1 (crate (name "dbsdk-rs") (vers "0.1.2") (deps (list (crate-dep (name "bitmask") (req "^0.5.0") (default-features #t) (kind 0)) (crate-dep (name "field-offset") (req "^0.3.4") (default-features #t) (kind 0)))) (hash "1937qnd6527fb8ys6lhcmss0lh6ykr79p0kfca4mrbc1gg6b89vk")))

(define-public crate-dbsdk-rs-0.1 (crate (name "dbsdk-rs") (vers "0.1.3") (deps (list (crate-dep (name "bitmask") (req "^0.5.0") (default-features #t) (kind 0)) (crate-dep (name "field-offset") (req "^0.3.4") (default-features #t) (kind 0)))) (hash "01qrkd8r9cvp3jkija68lcg7m6j5vq4k86hazrld9wb9blbra807")))

(define-public crate-dbsdk-rs-0.1 (crate (name "dbsdk-rs") (vers "0.1.4") (deps (list (crate-dep (name "bitmask") (req "^0.5.0") (default-features #t) (kind 0)) (crate-dep (name "field-offset") (req "^0.3.4") (default-features #t) (kind 0)))) (hash "19qn75m15xl15ngj14xq4jhfyrjj383bc826i4a3cqpvjl1hm9wq")))

(define-public crate-dbsdk-rs-0.1 (crate (name "dbsdk-rs") (vers "0.1.6") (deps (list (crate-dep (name "bitmask") (req "^0.5.0") (default-features #t) (kind 0)) (crate-dep (name "byteorder") (req "^1.4.3") (default-features #t) (kind 0)) (crate-dep (name "field-offset") (req "^0.3.4") (default-features #t) (kind 0)))) (hash "0vnfm9fxd4w3y8rmcgzlv9ahqkv45fbyk08axv0ln1djaip2yam8")))

(define-public crate-dbsdk-rs-0.1 (crate (name "dbsdk-rs") (vers "0.1.7") (deps (list (crate-dep (name "bitmask") (req "^0.5.0") (default-features #t) (kind 0)) (crate-dep (name "byteorder") (req "^1.4.3") (default-features #t) (kind 0)) (crate-dep (name "field-offset") (req "^0.3.4") (default-features #t) (kind 0)))) (hash "17syp14j9iarxkpzdpiww6hxblrga43vjw7x50p8z335pyacbk2x")))

(define-public crate-dbsdk-rs-0.1 (crate (name "dbsdk-rs") (vers "0.1.8") (deps (list (crate-dep (name "bitmask") (req "^0.5.0") (default-features #t) (kind 0)) (crate-dep (name "byteorder") (req "^1.4.3") (default-features #t) (kind 0)) (crate-dep (name "field-offset") (req "^0.3.4") (default-features #t) (kind 0)))) (hash "0lrgszpkclhlvzyrxp3qw7q0h0jk3i2k3miadq0rzq5f5ghvbjk3")))

(define-public crate-dbsdk-rs-0.1 (crate (name "dbsdk-rs") (vers "0.1.9") (deps (list (crate-dep (name "bitmask") (req "^0.5.0") (default-features #t) (kind 0)) (crate-dep (name "byteorder") (req "^1.4.3") (default-features #t) (kind 0)) (crate-dep (name "field-offset") (req "^0.3.4") (default-features #t) (kind 0)))) (hash "1prxpalvxazzlnq7lb9n39xkf1lk50vsjg6gjqjbc4xs0qkyz09h")))

(define-public crate-dbsdk-rs-0.1 (crate (name "dbsdk-rs") (vers "0.1.10") (deps (list (crate-dep (name "bitmask") (req "^0.5.0") (default-features #t) (kind 0)) (crate-dep (name "byteorder") (req "^1.4.3") (default-features #t) (kind 0)) (crate-dep (name "field-offset") (req "^0.3.4") (default-features #t) (kind 0)))) (hash "1ilz5bpsqpnv50nl3kj29x6hd7wz07fspghlyb3n4b2frvd7wx3s")))

(define-public crate-dbsdk-rs-0.1 (crate (name "dbsdk-rs") (vers "0.1.11") (deps (list (crate-dep (name "bitmask") (req "^0.5.0") (default-features #t) (kind 0)) (crate-dep (name "byteorder") (req "^1.4.3") (default-features #t) (kind 0)) (crate-dep (name "field-offset") (req "^0.3.4") (default-features #t) (kind 0)))) (hash "0caf6bg66g1fvgy5jm7c8y7swgykr3vmamza4jzr8qidiz0k5cfm")))

(define-public crate-dbsdk-rs-0.1 (crate (name "dbsdk-rs") (vers "0.1.12") (deps (list (crate-dep (name "bitmask") (req "^0.5.0") (default-features #t) (kind 0)) (crate-dep (name "byteorder") (req "^1.4.3") (default-features #t) (kind 0)) (crate-dep (name "field-offset") (req "^0.3.4") (default-features #t) (kind 0)))) (hash "0l9pkcahxlimrz1addl48zvrvrp9x66blz7a343zhvi2hbwh36jl")))

(define-public crate-dbsdk-rs-0.1 (crate (name "dbsdk-rs") (vers "0.1.13") (deps (list (crate-dep (name "bitmask") (req "^0.5.0") (default-features #t) (kind 0)) (crate-dep (name "byteorder") (req "^1.4.3") (default-features #t) (kind 0)) (crate-dep (name "field-offset") (req "^0.3.4") (default-features #t) (kind 0)))) (hash "1bch6whdcqyyzibr1932hcrjms1sf1i0m8lg1f0v2dqc49d9lmj0")))

