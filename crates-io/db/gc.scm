(define-module (crates-io db gc) #:use-module (crates-io))

(define-public crate-dbgcmd-0.1 (crate (name "dbgcmd") (vers "0.1.0") (deps (list (crate-dep (name "itertools") (req "~0.8.0") (default-features #t) (kind 0)))) (hash "0w6650h0k2z7pahvyhm88j6n8ljyx88ih75vbpv0lk0vpif4yixi") (features (quote (("force-enabled"))))))

