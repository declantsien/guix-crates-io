(define-module (crates-io db -h) #:use-module (crates-io))

(define-public crate-db-helpers-0.1 (crate (name "db-helpers") (vers "0.1.0") (hash "0l8cfk10xci94iclds2r9fgzn642sv094dwpkp69d0sjf6h34g36")))

(define-public crate-db-helpers-0.2 (crate (name "db-helpers") (vers "0.2.0") (deps (list (crate-dep (name "tokio-postgres") (req "^0.7") (optional #t) (default-features #t) (kind 0)))) (hash "1pmmxyix6x1rdbi50i2b9h1cpdw74wjhijyxnc7l7g3w70hnlap0") (features (quote (("tokio_postgres" "tokio-postgres") ("default" "tokio_postgres"))))))

(define-public crate-db-helpers-0.3 (crate (name "db-helpers") (vers "0.3.0") (deps (list (crate-dep (name "tokio-postgres") (req "^0.7") (optional #t) (default-features #t) (kind 0)))) (hash "1p71kfgjf49nlqv436bvrgwmyhfg8dav4r64r4yd92p58a78ayg7") (features (quote (("tokio_postgres" "tokio-postgres") ("default" "tokio_postgres"))))))

(define-public crate-db-helpers-0.4 (crate (name "db-helpers") (vers "0.4.0") (deps (list (crate-dep (name "tokio-postgres") (req "^0.7") (optional #t) (default-features #t) (kind 0)))) (hash "1g5cjhkxhdbay94biy6l8paz48g4ykyh4y7wkahmgsfigpcfd1wc") (features (quote (("tokio_postgres" "tokio-postgres") ("default" "tokio_postgres"))))))

(define-public crate-db-helpers-0.5 (crate (name "db-helpers") (vers "0.5.0") (deps (list (crate-dep (name "tokio-postgres") (req "^0.7") (optional #t) (default-features #t) (kind 0)))) (hash "0abvxqvas3qcgfx59p54hi19q2f5vvng1b9b8kb15ijlp766c1ni") (features (quote (("tokio_postgres" "tokio-postgres") ("default" "tokio_postgres"))))))

(define-public crate-db-helpers-0.5 (crate (name "db-helpers") (vers "0.5.1") (deps (list (crate-dep (name "tokio-postgres") (req "^0.7") (optional #t) (default-features #t) (kind 0)))) (hash "1gx9zr8i586dbnldv74mg2vxvwmfzxwxffs1qk4fhsr5pwsf4y6b") (features (quote (("tokio_postgres" "tokio-postgres") ("default" "tokio_postgres"))))))

(define-public crate-db-helpers-0.6 (crate (name "db-helpers") (vers "0.6.0") (deps (list (crate-dep (name "tokio-postgres") (req "^0.7") (default-features #t) (kind 0)))) (hash "0gynacq9rcbnb64d8fvjgrsy6f9yw8c4f72h6cmgcj6s9d4mh85c")))

(define-public crate-db-helpers-0.7 (crate (name "db-helpers") (vers "0.7.0") (deps (list (crate-dep (name "tokio-postgres") (req "^0.7") (default-features #t) (kind 0)))) (hash "17l4sr2p3ba5w3nv3r8fsdnv61cvdxq3kb3rryak5x8sx5d7rzzp")))

(define-public crate-db-helpers-0.7 (crate (name "db-helpers") (vers "0.7.1") (deps (list (crate-dep (name "tokio-postgres") (req "^0.7") (default-features #t) (kind 0)))) (hash "13xsl81asilajcz869k8nqshf3z3504mshw5qxyvrxcpqw5l6j2g")))

(define-public crate-db-helpers-0.8 (crate (name "db-helpers") (vers "0.8.0") (deps (list (crate-dep (name "tokio-postgres") (req "^0.7") (default-features #t) (kind 0)))) (hash "0l4kg0f7136g3kjdfdbnihw75nvkrhjzaq3f1qg400i96xkn1mch")))

(define-public crate-db-helpers-0.9 (crate (name "db-helpers") (vers "0.9.0") (deps (list (crate-dep (name "tokio-postgres") (req "^0.7") (default-features #t) (kind 0)))) (hash "0pygpm8pw38k0v3xps3k3gbk1n7a3mkqy5nwhhlc48a9s65qzmns")))

(define-public crate-db-helpers-0.9 (crate (name "db-helpers") (vers "0.9.1") (deps (list (crate-dep (name "tokio-postgres") (req "^0.7") (default-features #t) (kind 0)))) (hash "1chyhian6m90f8m468gzd646cmyp2kbw923js8r4gfq99ana7h1z")))

(define-public crate-db-helpers-0.10 (crate (name "db-helpers") (vers "0.10.0") (deps (list (crate-dep (name "tokio-postgres") (req "^0.7") (default-features #t) (kind 0)))) (hash "0qargwj6lzvdw00w5w7z0gj4mqf47mdlqc8z3zga2zrhcyrv9wla")))

(define-public crate-db-helpers-0.10 (crate (name "db-helpers") (vers "0.10.1") (deps (list (crate-dep (name "tokio-postgres") (req "^0.7") (default-features #t) (kind 0)))) (hash "12qvp2nkkz4awi2y1vivnsjxkzj6fk4zl4i7b02c97bqwpr8hmvv")))

(define-public crate-db-helpers-0.11 (crate (name "db-helpers") (vers "0.11.0") (deps (list (crate-dep (name "tokio-postgres") (req "^0.7") (optional #t) (default-features #t) (kind 0)))) (hash "0iq4djwhcqp92zpmmncr7y96q0c7hn3gpp68s4amjpp0cz5lklck") (features (quote (("pg" "tokio-postgres") ("default" "pg"))))))

(define-public crate-db-helpers-0.11 (crate (name "db-helpers") (vers "0.11.1") (deps (list (crate-dep (name "tokio-postgres") (req "^0.7") (optional #t) (default-features #t) (kind 0)))) (hash "0czhil5q9i9y6l9raclmsjj3j38zsaflfxlhvbwx0dn7b0zjx5a6") (features (quote (("pg" "tokio-postgres") ("default" "pg"))))))

(define-public crate-db-helpers-1 (crate (name "db-helpers") (vers "1.0.0") (deps (list (crate-dep (name "db-helpers-derive") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "tokio-postgres") (req "^0.7") (optional #t) (default-features #t) (kind 0)))) (hash "05246s8hs6xn32rm38fivpfjvqsih9rn9ncgy657nmnm5v8l4v7w") (features (quote (("pg" "tokio-postgres" "db-helpers-derive/pg") ("default" "pg"))))))

(define-public crate-db-helpers-1 (crate (name "db-helpers") (vers "1.0.1") (deps (list (crate-dep (name "db-helpers-derive") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "tokio-postgres") (req "^0.7") (optional #t) (default-features #t) (kind 0)))) (hash "13fwipcbjpr01ij4vh46z3vgnmf98a8bzv09mnsr91g3m9vdx4pw") (features (quote (("pg" "tokio-postgres" "db-helpers-derive/pg") ("default" "pg"))))))

(define-public crate-db-helpers-1 (crate (name "db-helpers") (vers "1.1.0") (deps (list (crate-dep (name "db-helpers-derive") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "tokio-postgres") (req "^0.7") (optional #t) (default-features #t) (kind 0)))) (hash "0r59ymj9jm07kgbgmdrz068rw04km6f3s3cqp3jkpyjy9hgqqmm3") (features (quote (("pg" "tokio-postgres" "db-helpers-derive/pg") ("default" "pg"))))))

(define-public crate-db-helpers-1 (crate (name "db-helpers") (vers "1.3.0") (deps (list (crate-dep (name "db-helpers-derive") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "tokio-postgres") (req "^0.7") (optional #t) (default-features #t) (kind 0)))) (hash "01wa65z73qmhq9vp5qqahpdph2b3nzh4z29rx1kiw10hq1gg3zkh") (features (quote (("pg" "tokio-postgres" "db-helpers-derive/pg") ("default" "pg"))))))

(define-public crate-db-helpers-1 (crate (name "db-helpers") (vers "1.3.1") (deps (list (crate-dep (name "db-helpers-derive") (req "^0.3.1") (default-features #t) (kind 0)) (crate-dep (name "tokio-postgres") (req "^0.7") (optional #t) (default-features #t) (kind 0)))) (hash "1w6s68zwyf7sf6v45npys3mas230pfzjsccz77fxjwqcxkw8drs9") (features (quote (("pg" "tokio-postgres" "db-helpers-derive/pg") ("default" "pg"))))))

(define-public crate-db-helpers-1 (crate (name "db-helpers") (vers "1.4.0") (deps (list (crate-dep (name "db-helpers-derive") (req "^0.4.0") (default-features #t) (kind 0)) (crate-dep (name "tokio-postgres") (req "^0.7") (optional #t) (default-features #t) (kind 0)))) (hash "077npf9bs8b9927vgcmkvwf93z885ah0fvc1z1nn6g3dml0cf57n") (features (quote (("pg" "tokio-postgres" "db-helpers-derive/pg") ("default" "pg"))))))

(define-public crate-db-helpers-1 (crate (name "db-helpers") (vers "1.5.0") (deps (list (crate-dep (name "db-helpers-derive") (req "^0.5.0") (default-features #t) (kind 0)) (crate-dep (name "tokio-postgres") (req "^0.7") (optional #t) (default-features #t) (kind 0)))) (hash "096q50aqqmlp7svrbf8694kfy83wdsxr01242m266xzplw6rjwrm") (features (quote (("pg" "tokio-postgres" "db-helpers-derive/pg") ("default" "pg"))))))

(define-public crate-db-helpers-derive-0.1 (crate (name "db-helpers-derive") (vers "0.1.0") (deps (list (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1.0.36") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.15") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.86") (features (quote ("extra-traits"))) (default-features #t) (kind 0)) (crate-dep (name "trybuild") (req "^1.0.56") (default-features #t) (kind 2)))) (hash "06fv9jqrp54qc5bb72p32c092xki1njrbvq9avzdngnpjkc57v66") (features (quote (("pg") ("default" "pg"))))))

(define-public crate-db-helpers-derive-0.2 (crate (name "db-helpers-derive") (vers "0.2.0") (deps (list (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1.0.36") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.15") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.86") (features (quote ("extra-traits"))) (default-features #t) (kind 0)) (crate-dep (name "trybuild") (req "^1.0.56") (default-features #t) (kind 2)))) (hash "0nnl66q8cg9kn42lazwgx1lw7h063mdvv657396abm2rw4n532l1") (features (quote (("pg") ("default" "pg"))))))

(define-public crate-db-helpers-derive-0.3 (crate (name "db-helpers-derive") (vers "0.3.0") (deps (list (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1.0.36") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.15") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.86") (features (quote ("extra-traits"))) (default-features #t) (kind 0)) (crate-dep (name "trybuild") (req "^1.0.56") (default-features #t) (kind 2)))) (hash "0dhdza9cn8zmlkpvr9f1qnnicp0pqnj7q10mva4i1j4w6x9m4xf5") (features (quote (("pg") ("default" "pg"))))))

(define-public crate-db-helpers-derive-0.3 (crate (name "db-helpers-derive") (vers "0.3.1") (deps (list (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "parking_lot") (req "^0.12.0") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1.0.36") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.15") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.86") (features (quote ("extra-traits"))) (default-features #t) (kind 0)) (crate-dep (name "trybuild") (req "^1.0.56") (default-features #t) (kind 2)))) (hash "09yb8y281k985fsznpil9mm9hxl3qqwg7ccd8xjjh843fi0pkawg") (features (quote (("pg") ("default" "pg"))))))

(define-public crate-db-helpers-derive-0.4 (crate (name "db-helpers-derive") (vers "0.4.0") (deps (list (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "parking_lot") (req "^0.12.0") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1.0.36") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.15") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.86") (features (quote ("extra-traits"))) (default-features #t) (kind 0)) (crate-dep (name "trybuild") (req "^1.0.56") (default-features #t) (kind 2)))) (hash "1mbj36yqxlkgmg2mwb324g0v6fwgra8dqj2ra7k813dzx8jfzncv") (features (quote (("pg") ("default" "pg"))))))

(define-public crate-db-helpers-derive-0.5 (crate (name "db-helpers-derive") (vers "0.5.0") (deps (list (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "parking_lot") (req "^0.12.0") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1.0.36") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.15") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.86") (features (quote ("extra-traits"))) (default-features #t) (kind 0)) (crate-dep (name "trybuild") (req "^1.0.56") (default-features #t) (kind 2)))) (hash "1280586maq3l75b678bm0hzvbdzsf72hn19kn0ilk1biiyg7cpld") (features (quote (("pg") ("default" "pg"))))))

