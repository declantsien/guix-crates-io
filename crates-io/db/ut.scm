(define-module (crates-io db ut) #:use-module (crates-io))

(define-public crate-dbutils-0.2 (crate (name "dbutils") (vers "0.2.0") (deps (list (crate-dep (name "bytes") (req "^1.4") (kind 0)))) (hash "1qdq1chlgshd15jixm400p5cy3fy595mkm1jqj7yyq7agpfdkv37") (features (quote (("std") ("default" "std"))))))

(define-public crate-dbutils-0.2 (crate (name "dbutils") (vers "0.2.1") (deps (list (crate-dep (name "bytes") (req "^1.4") (kind 0)))) (hash "105ghk7rssiy3p0d6sn1441dzaj6i0kcnydq3rcv54c557fg7s4g") (features (quote (("std") ("default" "std"))))))

