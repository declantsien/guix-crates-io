(define-module (crates-io db qu) #:use-module (crates-io))

(define-public crate-dbquota-0.1 (crate (name "dbquota") (vers "0.1.0") (hash "0qzs08pmz1kdqcm4sqw05fcyglkbx8c139z5cjbrl47ljsyc55kc")))

(define-public crate-dbquota-0.2 (crate (name "dbquota") (vers "0.2.0") (deps (list (crate-dep (name "clap") (req "^2") (default-features #t) (kind 0)) (crate-dep (name "mysql") (req "^16.0") (default-features #t) (kind 0)))) (hash "08kgciz61p4i091w7nzxs66jzwq71f0sw06ys2zr24ggx0mqivra")))

(define-public crate-dbquota-0.3 (crate (name "dbquota") (vers "0.3.0") (deps (list (crate-dep (name "clap") (req "^2") (default-features #t) (kind 0)) (crate-dep (name "mysql") (req "^16.0") (default-features #t) (kind 0)))) (hash "0ryznwg4zg8r41cfkyvj3rk39w6kj9z77asm3rv1hvx0xhl5kkib")))

(define-public crate-dbquota-0.3 (crate (name "dbquota") (vers "0.3.1") (deps (list (crate-dep (name "clap") (req "^2") (default-features #t) (kind 0)) (crate-dep (name "mysql") (req "^16.0") (default-features #t) (kind 0)))) (hash "1771ywx2wwpnvbpgf89w01vkrv31fi9ijrw2abgf540myn8xfxvs")))

