(define-module (crates-io db f2) #:use-module (crates-io))

(define-public crate-dbf2csv-0.1 (crate (name "dbf2csv") (vers "0.1.0") (deps (list (crate-dep (name "csv") (req "^1.0.0-beta.5") (default-features #t) (kind 0)) (crate-dep (name "dbf") (req "^0.1.1") (default-features #t) (kind 0)))) (hash "168yc9na5pv3sqh1xi1fag67akdsdvgxgypjmmzdyc3hx3ys1d2g")))

(define-public crate-dbf2csv-0.1 (crate (name "dbf2csv") (vers "0.1.1") (deps (list (crate-dep (name "csv") (req "^1.0.0-beta.5") (default-features #t) (kind 0)) (crate-dep (name "dbf") (req "^0.1.1") (default-features #t) (kind 0)))) (hash "1rq12il89fjkf83b0hxbxrh4gf2s7zwcia97hvszp0306pk11yhh")))

