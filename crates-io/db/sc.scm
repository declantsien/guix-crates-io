(define-module (crates-io db sc) #:use-module (crates-io))

(define-public crate-dbscan-0.1 (crate (name "dbscan") (vers "0.1.0") (hash "102vis5v6p341b374pr0dy8r8nsza6s9386fqmyj1r34mfq4s1xh")))

(define-public crate-dbscan-0.1 (crate (name "dbscan") (vers "0.1.1") (hash "0ap3bcz86mnbcljkxk9wg693fvc1vjxrw7cv04r8i6dqyqpl9kcg")))

(define-public crate-dbscan-0.2 (crate (name "dbscan") (vers "0.2.0") (hash "1hkwngg9rygl7fry06x127pzbws2h75754abc46cjfla2lx9n7fk")))

(define-public crate-dbscan-0.3 (crate (name "dbscan") (vers "0.3.0") (hash "1ng5dc84g33pbl0xajmp8ad10lcqdpfksvkdavkc6m0lw106pxzc")))

(define-public crate-dbscan-0.3 (crate (name "dbscan") (vers "0.3.1") (hash "11cakn8mhb0jpqk42rg8m5j16vkcvqnyfhylp5qjmih7iaf10m4n")))

