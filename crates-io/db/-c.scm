(define-module (crates-io db -c) #:use-module (crates-io))

(define-public crate-db-core-0.1 (crate (name "db-core") (vers "0.1.0") (deps (list (crate-dep (name "fs2") (req "^0.4.3") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.8") (default-features #t) (kind 0)))) (hash "0ppyay1v1v77zilccyc8pm7frlzklw7ll5gvjcn48f7k3nj43pvg")))

(define-public crate-db-core-0.1 (crate (name "db-core") (vers "0.1.1") (deps (list (crate-dep (name "fs2") (req "^0.4.3") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.8") (default-features #t) (kind 0)))) (hash "16wfcqd10xn4595ixjyqg7pnsypqa3khxgwhmqshdxj800nf9c33")))

(define-public crate-db-core-0.2 (crate (name "db-core") (vers "0.2.0") (deps (list (crate-dep (name "fs2") (req "^0.4.3") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.8") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.4") (default-features #t) (kind 2)))) (hash "1ddv1sx4ff7y12v8qq18gizgdgiy9mylk1f5rz7ypk64932qbd1j")))

(define-public crate-db-core-0.2 (crate (name "db-core") (vers "0.2.1") (deps (list (crate-dep (name "fs2") (req "^0.4.3") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.8") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.4") (default-features #t) (kind 2)))) (hash "0pp2anzadnwymd5gmh6z69iblqnpacbaddqfnca79l9p1nax1fn3")))

