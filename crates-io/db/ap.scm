(define-module (crates-io db ap) #:use-module (crates-io))

(define-public crate-dbap-0.1 (crate (name "dbap") (vers "0.1.0") (deps (list (crate-dep (name "num-traits") (req "^0.2") (default-features #t) (kind 0)))) (hash "1mgglqq17gv91gaa8mqiwyqsg96gh1fmk75h2p414yqy5c8qd7d6")))

