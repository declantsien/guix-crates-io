(define-module (crates-io db gp) #:use-module (crates-io))

(define-public crate-dbgp-0.0.1 (crate (name "dbgp") (vers "0.0.1") (deps (list (crate-dep (name "base64") (req "^0.4.1") (default-features #t) (kind 0)) (crate-dep (name "bytes") (req "^0.4.1") (default-features #t) (kind 0)) (crate-dep (name "futures") (req "^0.1.12") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^0.2.6") (default-features #t) (kind 0)) (crate-dep (name "tokio-io") (req "^0.1.1") (default-features #t) (kind 0)) (crate-dep (name "tokio-proto") (req "^0.1.1") (default-features #t) (kind 0)) (crate-dep (name "tokio-service") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "url") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "xml-rs") (req "^0.4.1") (default-features #t) (kind 0)))) (hash "0bbdilnic77xj4fvp2b7cc6w7x7khc154y7xyn17hx776klfzspc")))

(define-public crate-dbgprint-0.1 (crate (name "dbgprint") (vers "0.1.0") (hash "0sb1iw1xi40y4ip030nqjn3xwz72w56p6kn7c2mnmvmbax0fjl2k")))

