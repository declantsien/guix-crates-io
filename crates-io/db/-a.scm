(define-module (crates-io db -a) #:use-module (crates-io))

(define-public crate-db-accelerate-2 (crate (name "db-accelerate") (vers "2.0.0") (deps (list (crate-dep (name "clap") (req "^2.5") (features (quote ("color" "suggestions"))) (default-features #t) (kind 0)) (crate-dep (name "colored") (req "^1.2") (default-features #t) (kind 0)) (crate-dep (name "postgres") (req "^0.11") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^0.1") (default-features #t) (kind 0)))) (hash "06lvjzipcizx7hqn18qavsswcwmmj7c2jz4ga5lnz12i78xw0fdw") (features (quote (("driver-test") ("driver-postgres" "postgres") ("default" "driver-test" "driver-postgres"))))))

