(define-module (crates-io db f_) #:use-module (crates-io))

(define-public crate-dbf_dextractor-0.1 (crate (name "dbf_dextractor") (vers "0.1.0") (deps (list (crate-dep (name "byteorder") (req "^1.3.4") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.106") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.51") (default-features #t) (kind 0)))) (hash "0i5kp16wiy3rnnhkcyhq767iplm99fmvr85jfpl76nzvnayfnd8c")))

(define-public crate-dbf_dextractor-0.1 (crate (name "dbf_dextractor") (vers "0.1.1") (deps (list (crate-dep (name "byteorder") (req "^1.3.4") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.106") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "11l7r9vvrip06lf9fa4250r1yc0flhanvlxb8crws35ik2ds27mn")))

(define-public crate-dbf_dextractor-0.1 (crate (name "dbf_dextractor") (vers "0.1.2") (deps (list (crate-dep (name "byteorder") (req "^1.3.4") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.106") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "103ssv02w51hwv1r3j9n93q8vj1hjs5v5f0d4hz50xjvzhpzncbw")))

