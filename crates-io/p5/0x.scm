(define-module (crates-io p5 #{0x}#) #:use-module (crates-io))

(define-public crate-p50x-0.1 (crate (name "p50x") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^2.33.1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "safe-transmute") (req "^0.10.1") (default-features #t) (kind 0)) (crate-dep (name "serial-unit-testing") (req "^0.2.3") (kind 0)))) (hash "0aqsrlv1lri28j0mppg99h9i4fg9kcz9qpn3snkc4nqwysqyfcmq") (features (quote (("default" "binary") ("binary" "clap"))))))

