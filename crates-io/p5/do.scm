(define-module (crates-io p5 do) #:use-module (crates-io))

(define-public crate-p5doc-0.1 (crate (name "p5doc") (vers "0.1.0") (deps (list (crate-dep (name "proc-macro-error") (req "^1.0.4") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1.0.51") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.23") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.7.1") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.109") (features (quote ("full" "extra-traits"))) (default-features #t) (kind 0)))) (hash "0mfpimb5wcfj8l7ssy2xnfky6y8d2qs9rvkh1wjf7yk2l8hdr48x")))

