(define-module (crates-io p5 -s) #:use-module (crates-io))

(define-public crate-p5-sys-0.1 (crate (name "p5-sys") (vers "0.1.0") (deps (list (crate-dep (name "js-sys") (req "^0.3.44") (default-features #t) (kind 0)) (crate-dep (name "wasm-bindgen") (req "^0.2.67") (default-features #t) (kind 0)))) (hash "11v1hha5s5sq52wa0vklcih1x0xvmwdql0rrhjfyf7my37cvsfmm")))

(define-public crate-p5-sys-0.1 (crate (name "p5-sys") (vers "0.1.1") (deps (list (crate-dep (name "js-sys") (req "^0.3.44") (default-features #t) (kind 0)) (crate-dep (name "wasm-bindgen") (req "^0.2.67") (default-features #t) (kind 0)))) (hash "17fil56y25x1zx0mgiac640v4h6bwcvf7276jq7k5gg3r4n72mcs")))

