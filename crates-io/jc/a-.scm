(define-module (crates-io jc a-) #:use-module (crates-io))

(define-public crate-jca-cli-0.0.0 (crate (name "jca-cli") (vers "0.0.0") (deps (list (crate-dep (name "jca") (req "^0.0") (default-features #t) (kind 0)) (crate-dep (name "jca-interp") (req "^0.0") (default-features #t) (kind 0)))) (hash "0920y8q2rqrgsjl03a87sgdkvjya792gcnd1g8c2mr0p6rygw9jc")))

(define-public crate-jca-interp-0.0.0 (crate (name "jca-interp") (vers "0.0.0") (deps (list (crate-dep (name "jca") (req "^0.0") (default-features #t) (kind 0)))) (hash "18438ag880xvsmx44xxcv3s3zaajfa9lrvm6ka08kgwssly1rqcg")))

