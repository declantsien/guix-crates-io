(define-module (crates-io jc al) #:use-module (crates-io))

(define-public crate-jcalendar-0.1 (crate (name "jcalendar") (vers "0.1.0") (deps (list (crate-dep (name "chrono") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "colored") (req "^2.0") (default-features #t) (kind 0)) (crate-dep (name "jput") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "koyomi") (req "^0.4") (default-features #t) (kind 0)))) (hash "1mcf1wl2cfm2iajc5cpn354wviyd9nncn11b0fri9157j63v2p1q") (yanked #t)))

(define-public crate-jcalendar-0.1 (crate (name "jcalendar") (vers "0.1.1") (deps (list (crate-dep (name "chrono") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "colored") (req "^2.0") (default-features #t) (kind 0)) (crate-dep (name "jput") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "koyomi") (req "^0.4") (default-features #t) (kind 0)))) (hash "0wqw7s1in2pzxjz3kq2xsw4kgw0sj6170n76z09ra46z886f4x3g") (yanked #t)))

(define-public crate-jcalendar-0.1 (crate (name "jcalendar") (vers "0.1.2") (deps (list (crate-dep (name "chrono") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "colored") (req "^2.0") (default-features #t) (kind 0)) (crate-dep (name "jput") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "koyomi") (req "^0.4") (default-features #t) (kind 0)))) (hash "1bi69akrssfv26dm6if302apinn94y480xz61mimkwwv1hqx726v")))

