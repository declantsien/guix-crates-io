(define-module (crates-io jc er) #:use-module (crates-io))

(define-public crate-jcers-0.1 (crate (name "jcers") (vers "0.1.0") (deps (list (crate-dep (name "bytes") (req "^1.1") (default-features #t) (kind 0)) (crate-dep (name "hex") (req "^0.4") (default-features #t) (kind 2)) (crate-dep (name "jcers_proc") (req "^0.1.0") (optional #t) (default-features #t) (kind 0)))) (hash "1yigmxldy4d087az650b2w1c8mkr6h97vrzm1xr2xymbm0442njl") (features (quote (("derive" "jcers_proc"))))))

(define-public crate-jcers-0.1 (crate (name "jcers") (vers "0.1.1") (deps (list (crate-dep (name "bytes") (req "^1.1") (default-features #t) (kind 0)) (crate-dep (name "hex") (req "^0.4") (default-features #t) (kind 2)) (crate-dep (name "jcers_proc") (req "^0.1.0") (optional #t) (default-features #t) (kind 0)))) (hash "06dhvzq3lsf4njfn4lzsxjxnsvnrx9b42agx4qb9aynn3wi0cxzi") (features (quote (("derive" "jcers_proc"))))))

(define-public crate-jcers-0.1 (crate (name "jcers") (vers "0.1.2") (deps (list (crate-dep (name "bytes") (req "^1.1") (default-features #t) (kind 0)) (crate-dep (name "hex") (req "^0.4") (default-features #t) (kind 2)) (crate-dep (name "jcers_proc") (req "^0.1.0") (optional #t) (default-features #t) (kind 0)))) (hash "1rkbl8f5yraya1il5f6k6481bj5vqh9fyn7q16yil3r0i01xilkn") (features (quote (("derive" "jcers_proc"))))))

(define-public crate-jcers_proc-0.1 (crate (name "jcers_proc") (vers "0.1.0") (deps (list (crate-dep (name "proc-macro2") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1") (default-features #t) (kind 0)))) (hash "0m541laz8i9cwmph5z2hca3jyl2h5sp4hsdndi1x93a56whd11n8")))

