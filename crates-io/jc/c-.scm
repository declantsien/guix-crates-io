(define-module (crates-io jc c-) #:use-module (crates-io))

(define-public crate-jcc-cli-0.1 (crate (name "jcc-cli") (vers "0.1.4") (deps (list (crate-dep (name "clap") (req "^4.4.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "env_logger") (req "^0.10.0") (default-features #t) (kind 0)) (crate-dep (name "jcc") (req "^0.1.4") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.20") (default-features #t) (kind 0)))) (hash "0sa99izxc8983499m0n31i5b6bihdpgm9m9bhp1d9wh0jhj6l7nw")))

(define-public crate-jcc-cli-0.1 (crate (name "jcc-cli") (vers "0.1.5") (deps (list (crate-dep (name "clap") (req "^4.4.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "env_logger") (req "^0.10.0") (default-features #t) (kind 0)) (crate-dep (name "jcc") (req "^0.1.5") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.20") (default-features #t) (kind 0)))) (hash "0k3zg5rngzbyf03b835vf5qrrnfyh2dwri2954w1zpafa8jzpahg")))

(define-public crate-jcc-cli-0.1 (crate (name "jcc-cli") (vers "0.1.6") (deps (list (crate-dep (name "clap") (req "^4.4.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "env_logger") (req "^0.10.0") (default-features #t) (kind 0)) (crate-dep (name "jcc") (req "^0.1.6") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.20") (default-features #t) (kind 0)))) (hash "0zivlmjfkdlmj7i295bq9sbd2qmlx538f04m22vznlpbijmyp18h")))

(define-public crate-jcc-cli-0.1 (crate (name "jcc-cli") (vers "0.1.7") (deps (list (crate-dep (name "clap") (req "^4.4.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "env_logger") (req "^0.10.0") (default-features #t) (kind 0)) (crate-dep (name "jcc") (req "^0.1.7") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.20") (default-features #t) (kind 0)))) (hash "0vij4zyvcb5kxik3g1knqfy3y2xb5ybq8s81cqmi0i2fdlzghfsf")))

