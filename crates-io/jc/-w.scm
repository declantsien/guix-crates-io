(define-module (crates-io jc -w) #:use-module (crates-io))

(define-public crate-jc-wrapper-0.1 (crate (name "jc-wrapper") (vers "0.1.0") (deps (list (crate-dep (name "serde") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0") (default-features #t) (kind 0)))) (hash "1lg2bk6n7v714cg5yddymz8cyq9mc1p1ba4xj5bygw0lv42v1zn4")))

(define-public crate-jc-wrapper-0.1 (crate (name "jc-wrapper") (vers "0.1.1") (deps (list (crate-dep (name "serde") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0") (default-features #t) (kind 0)))) (hash "0mbki30rrlr4ds2libyxl3mm8b3da4qwbp12zdqriny8aw1bk133")))

(define-public crate-jc-wrapper-0.2 (crate (name "jc-wrapper") (vers "0.2.0") (deps (list (crate-dep (name "serde") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0") (default-features #t) (kind 0)))) (hash "0x2l0dy4afw8vg1l1lkmzrv9l8fhhn7kmphd1p3hf7fcgim02yvz")))

(define-public crate-jc-wrapper-0.2 (crate (name "jc-wrapper") (vers "0.2.1") (deps (list (crate-dep (name "serde") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0") (default-features #t) (kind 0)))) (hash "1hw3p762lwaix2is99k01kmax1my0kqfvwrm7wx8c5riw10ivaz5")))

