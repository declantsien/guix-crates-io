(define-module (crates-io jc e-) #:use-module (crates-io))

(define-public crate-jce-derive-0.1 (crate (name "jce-derive") (vers "0.1.0") (deps (list (crate-dep (name "proc-macro2") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.3") (features (quote ("extra-traits"))) (default-features #t) (kind 0)))) (hash "1b4bpiwlk88krdk1z0zdmangrk2dsqf3mmhp5zk7wsvgvhd5xl2f")))

(define-public crate-jce-derive-0.1 (crate (name "jce-derive") (vers "0.1.1") (deps (list (crate-dep (name "proc-macro2") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.3") (features (quote ("extra-traits"))) (default-features #t) (kind 0)))) (hash "185vg72m2pjjk7hqxmbxkn75ayg6achjhrx55n0fw087pz9r222i")))

