(define-module (crates-io wu ti) #:use-module (crates-io))

(define-public crate-wutil-0.0.1 (crate (name "wutil") (vers "0.0.1") (hash "1jsjzbablm3kbss8b5lcn6pjxwgwj5jzfxw2106i135dal5h2q2n")))

(define-public crate-wutil-0.0.2 (crate (name "wutil") (vers "0.0.2") (hash "0l7zqhfjxbyfmbk4xm8w832f1nqb396w015jppzjd8xl00wwwxm1")))

(define-public crate-wutil-0.0.3 (crate (name "wutil") (vers "0.0.3") (hash "1af02nx5gc9faq8lym3afwcw237jiny4kbnwl4xm7nna1mhs8y3x")))

