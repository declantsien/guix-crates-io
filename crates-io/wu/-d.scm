(define-module (crates-io wu -d) #:use-module (crates-io))

(define-public crate-wu-diff-0.1 (crate (name "wu-diff") (vers "0.1.0") (deps (list (crate-dep (name "clippy") (req "^0.0.209") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "criterion") (req "^0.2") (default-features #t) (kind 2)))) (hash "1mc5y3i6ys6rpjzxcdq26876qp47sj93v97mb1gbbjnz5i3i5p20")))

(define-public crate-wu-diff-0.1 (crate (name "wu-diff") (vers "0.1.1") (deps (list (crate-dep (name "base64") (req "^0.9.0") (default-features #t) (kind 2)) (crate-dep (name "clippy") (req "^0.0.209") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "criterion") (req "^0.2") (default-features #t) (kind 2)) (crate-dep (name "image") (req "^0.18.0") (default-features #t) (kind 2)))) (hash "0r12nzm9xaq58gsknwvjh81adxpkmjfqw3p7vr8hag8bbv6vlcfz")))

(define-public crate-wu-diff-0.1 (crate (name "wu-diff") (vers "0.1.2") (deps (list (crate-dep (name "base64") (req "^0.9.0") (default-features #t) (kind 2)) (crate-dep (name "clippy") (req "^0.0.209") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "criterion") (req "^0.2") (default-features #t) (kind 2)) (crate-dep (name "image") (req "^0.18.0") (default-features #t) (kind 2)))) (hash "14w8yacn4hk9k4rfzshlgir966xbwgbwk3fvf0l461nyzhsnfglf")))

