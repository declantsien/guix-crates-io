(define-module (crates-io wu st) #:use-module (crates-io))

(define-public crate-wust-0.1 (crate (name "wust") (vers "0.1.0") (hash "1h7af594rs6pb6zykrh008qzlqcswis6yh97da3kw9vbl30px1zs")))

(define-public crate-wustc-0.1 (crate (name "wustc") (vers "0.1.0") (hash "17h670p5cn1bl62zjvf9pvx3hss4zp3xaa6qf7f9c9ay1ifk69fj")))

