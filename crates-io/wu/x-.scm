(define-module (crates-io wu x-) #:use-module (crates-io))

(define-public crate-wux-cli-0.1 (crate (name "wux-cli") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^4.4.7") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "indicatif") (req "^0.17.7") (default-features #t) (kind 0)) (crate-dep (name "wux") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "03kkd95m18yn6c1i029fvmb80c08agpag8jka7xikq38qw13whah")))

