(define-module (crates-io wu tf) #:use-module (crates-io))

(define-public crate-wutf-0.1 (crate (name "wutf") (vers "0.1.0") (deps (list (crate-dep (name "chardet") (req "^0.2") (default-features #t) (kind 0)))) (hash "0kgpizl2gshkgydh14jdpig9vxz1365mic628y1g1ccyi83dagj7")))

