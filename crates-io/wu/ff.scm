(define-module (crates-io wu ff) #:use-module (crates-io))

(define-public crate-wuffs-0.1 (crate (name "wuffs") (vers "0.1.0") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "wuffs-sys") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "0f0h2lqws3qp4df7jivxwp2fvzzl3hq1qzpikbxif221c14n3r1b")))

(define-public crate-wuffs-0.2 (crate (name "wuffs") (vers "0.2.0") (deps (list (crate-dep (name "wuffs-sys") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "0mvn00z8ipab8icm69ijz8lvabkcx2gxw8ccp9f06qavi09njxfj")))

(define-public crate-wuffs-sys-0.1 (crate (name "wuffs-sys") (vers "0.1.0") (deps (list (crate-dep (name "bindgen") (req "^0.58") (default-features #t) (kind 1)) (crate-dep (name "cc") (req "^1.0") (default-features #t) (kind 1)))) (hash "022wgyvaa6m0sxjcj12phynr5diyddcl6nx2l2fz7zxllgwjfr12")))

