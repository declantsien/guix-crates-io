(define-module (crates-io ob la) #:use-module (crates-io))

(define-public crate-oblast-demo-0.1 (crate (name "oblast-demo") (vers "0.1.0") (deps (list (crate-dep (name "blst") (req "^0.3.10") (default-features #t) (kind 0)) (crate-dep (name "hex") (req "^0.4.2") (default-features #t) (kind 0)) (crate-dep (name "num-bigint") (req "^0.3.1") (default-features #t) (kind 0)) (crate-dep (name "paste") (req "^1.0.4") (default-features #t) (kind 0)))) (hash "0hq1znl6k22zsacnv57236pvgi0c99yax8mw8ljx60ljnxqrwisn")))

