(define-module (crates-io ob j-) #:use-module (crates-io))

(define-public crate-obj-c-0.0.0 (crate (name "obj-c") (vers "0.0.0") (hash "09dc6ylqzqix4i7yr2yckkrlxslgi785bcrr7c2f5a3m6hqqgplp")))

(define-public crate-obj-exporter-0.1 (crate (name "obj-exporter") (vers "0.1.0") (deps (list (crate-dep (name "itertools") (req "^0.6.1") (default-features #t) (kind 0)) (crate-dep (name "wavefront_obj") (req "^5.1.0") (default-features #t) (kind 0)))) (hash "06wqv4b1pb0vic16plk0jqwwhx3fcrxlicdi0hr80rvm4psprxjs")))

(define-public crate-obj-exporter-0.2 (crate (name "obj-exporter") (vers "0.2.0") (deps (list (crate-dep (name "lazy_static") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "wavefront_obj") (req "^5.1.0") (default-features #t) (kind 0)))) (hash "0978pvrnrgzrkhx0z13hxk13r65cqk29hv7bak4k1dbvz86lccyi")))

(define-public crate-obj-pool-0.1 (crate (name "obj-pool") (vers "0.1.0") (deps (list (crate-dep (name "unreachable") (req "^0.1") (default-features #t) (kind 0)))) (hash "012yf48rfn0ac4mm1y5xwvfa1x9wn9qv8qa1jjmj1b0pzcsrqkp6")))

(define-public crate-obj-pool-0.2 (crate (name "obj-pool") (vers "0.2.0") (deps (list (crate-dep (name "rand") (req "^0.5") (default-features #t) (kind 0)) (crate-dep (name "unreachable") (req "^0.1") (default-features #t) (kind 0)))) (hash "0lv09xx0d5h58ngacs1c091qah0rbxpfpdl3v66pdij08vsbghm8")))

(define-public crate-obj-pool-0.3 (crate (name "obj-pool") (vers "0.3.0") (deps (list (crate-dep (name "optional") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.5") (default-features #t) (kind 0)) (crate-dep (name "unreachable") (req "^0.1") (default-features #t) (kind 0)))) (hash "167fnj39b0a3pv2cpi7jn5fraxqycahwl9qgqksprdq5lr2j6cbp")))

(define-public crate-obj-pool-0.3 (crate (name "obj-pool") (vers "0.3.1") (deps (list (crate-dep (name "optional") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.5") (default-features #t) (kind 0)) (crate-dep (name "unreachable") (req "^0.1") (default-features #t) (kind 0)))) (hash "1hd6j1fipznn8z6xdlyjilhdqm1dsf7zijwd8laq8pqi4107p6cf")))

(define-public crate-obj-pool-0.4 (crate (name "obj-pool") (vers "0.4.0") (deps (list (crate-dep (name "optional") (req "^0.5") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.6.5") (default-features #t) (kind 0)) (crate-dep (name "unreachable") (req "^1.0") (default-features #t) (kind 0)))) (hash "0md1al5nyk1693yxjsk87a5y5m442vim7f3il312l9fh60cx3crc")))

(define-public crate-obj-pool-0.4 (crate (name "obj-pool") (vers "0.4.1") (deps (list (crate-dep (name "optional") (req "^0.5") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.7.2") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "unreachable") (req "^1.0") (default-features #t) (kind 0)))) (hash "1q3g9r5mdqj7330a3ii65xdz2qyqlj5v8iyqj2s79960fmavdjwg") (features (quote (("serde_support" "serde") ("default"))))))

(define-public crate-obj-pool-0.4 (crate (name "obj-pool") (vers "0.4.2") (deps (list (crate-dep (name "optional") (req "^0.5") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.7.2") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "unreachable") (req "^1.0") (default-features #t) (kind 0)))) (hash "0xajgw3zgycfzs9xwll8nfh0x27iz8dbp410l1mc9pk26xnhlkfj") (features (quote (("serde_support" "serde") ("default"))))))

(define-public crate-obj-pool-0.4 (crate (name "obj-pool") (vers "0.4.3") (deps (list (crate-dep (name "optional") (req "^0.5") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.7.2") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "unreachable") (req "^1.0") (default-features #t) (kind 0)))) (hash "1h9pn9aid8wqikf5hzghbqwlamc322bvwgf8gpy6cpnvf5y3yynd") (features (quote (("serde_support" "serde" "optional/serde") ("default"))))))

(define-public crate-obj-pool-0.4 (crate (name "obj-pool") (vers "0.4.4") (deps (list (crate-dep (name "const-random") (req "^0.1.8") (default-features #t) (kind 0)) (crate-dep (name "optional") (req "^0.5") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.7.3") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "unreachable") (req "^1.0") (default-features #t) (kind 0)))) (hash "1lkgxp0vh52z1jrl7g76hhhhp85dhvzn78lknip7wq1x5vj1hg8j") (features (quote (("serde_support" "serde" "optional/serde") ("default"))))))

(define-public crate-obj-pool-0.5 (crate (name "obj-pool") (vers "0.5.0") (deps (list (crate-dep (name "parking_lot") (req "^0.12.1") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "unreachable") (req "^1.0.0") (default-features #t) (kind 0)))) (hash "0cr8ahahn1acagf6f0w7jp3hjd4ijkdhp795myxai5ryp3ll5rix") (features (quote (("serde_support" "serde") ("default"))))))

(define-public crate-obj-pool-0.5 (crate (name "obj-pool") (vers "0.5.1") (deps (list (crate-dep (name "parking_lot") (req "^0.12.1") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "unreachable") (req "^1.0.0") (default-features #t) (kind 0)))) (hash "04ifz0rymrak8j9wbw4hxl8z3hy8lrmi9k1bg0ayzvycgzvkqqxj") (features (quote (("serde_support" "serde") ("default"))))))

(define-public crate-obj-rs-0.0.1 (crate (name "obj-rs") (vers "0.0.1") (hash "1fwds7rss5x6dfb1ap17ra5l1bhay2wxv4p01n56hxsjnjyhq1s3")))

(define-public crate-obj-rs-0.0.2 (crate (name "obj-rs") (vers "0.0.2") (hash "0h6m4xya5sn351rw3njwi2dymmiwy0r8bwww31v203zn3295f4jd")))

(define-public crate-obj-rs-0.0.3 (crate (name "obj-rs") (vers "0.0.3") (deps (list (crate-dep (name "bear") (req "^0.1.1") (default-features #t) (kind 2)))) (hash "0xa4vkfqjj1v53ly8wz7ddqzjiszp05zgfgbfp7f47cg5qd55713")))

(define-public crate-obj-rs-0.0.4 (crate (name "obj-rs") (vers "0.0.4") (deps (list (crate-dep (name "bear") (req "^0.1.1") (default-features #t) (kind 2)))) (hash "17yan21jv9ab6ppgc3m7q7cnyvza39hjmk1qpf32amh80lj7zn8x")))

(define-public crate-obj-rs-0.0.5 (crate (name "obj-rs") (vers "0.0.5") (hash "1shz8bkkkvjcp6hl733jpy3vxvwszvklnk3jvjlcb1h4w47jms9q")))

(define-public crate-obj-rs-0.1 (crate (name "obj-rs") (vers "0.1.0") (hash "0b2cn2nj60658kgz07dzlawx3xnf8s1n5z0gl1lrs6f91dcmniid")))

(define-public crate-obj-rs-0.1 (crate (name "obj-rs") (vers "0.1.1") (hash "0k23wsaiayjb75b5rhv5qbb73a9b2h22c233f7d7mb0c2y1i2gvh")))

(define-public crate-obj-rs-0.2 (crate (name "obj-rs") (vers "0.2.0") (hash "1mblrixp234vlrpsvdvsbfw9p3sblgfbq6bs8vlw7n2yr7i02qa3")))

(define-public crate-obj-rs-0.2 (crate (name "obj-rs") (vers "0.2.1") (hash "066d5hwgpfys32nz0lmb3rvvji9dvn983p76vvg9c3sgziv37rgd")))

(define-public crate-obj-rs-0.2 (crate (name "obj-rs") (vers "0.2.2") (hash "0dris5jxsi4rwnyy536383hsf045j6lns650dk0kbwgnf5nrly41")))

(define-public crate-obj-rs-0.3 (crate (name "obj-rs") (vers "0.3.0") (hash "0iagwr4f9pqqr3f0q3ff4glp67i2nbvjc26sssnna587isar0bv3")))

(define-public crate-obj-rs-0.4 (crate (name "obj-rs") (vers "0.4.0") (hash "0g5lx5gdxmgxhgl1w2wlk2ng9fjlq74h4ghg36y3bnl0v3zlj29p")))

(define-public crate-obj-rs-0.4 (crate (name "obj-rs") (vers "0.4.1") (hash "118zwh9sjc1yw31s3q240j3afnydhz297dixcpcw11lahhjn5ikm")))

(define-public crate-obj-rs-0.4 (crate (name "obj-rs") (vers "0.4.2") (hash "1mskrd5zfld856nygbsgnifbdrlas68dw86n0iqfmmf70pvsm8zn")))

(define-public crate-obj-rs-0.4 (crate (name "obj-rs") (vers "0.4.3") (deps (list (crate-dep (name "glium") (req "*") (optional #t) (kind 0)))) (hash "1hb856nprgy9i69ppq5m63fkv3in0kqyqlmnv6pi61zi7cbvjnbg") (features (quote (("glium-support" "glium"))))))

(define-public crate-obj-rs-0.4 (crate (name "obj-rs") (vers "0.4.4") (deps (list (crate-dep (name "glium") (req "*") (optional #t) (kind 0)))) (hash "19qic3iv7s63vg7ay5gaar9qbvjvif46g8cjrh7w1xhcjkydqlh3") (features (quote (("glium-support" "glium"))))))

(define-public crate-obj-rs-0.4 (crate (name "obj-rs") (vers "0.4.5") (deps (list (crate-dep (name "glium") (req "*") (optional #t) (kind 0)))) (hash "0q9xyc4xh1gmr1zzr9rcq2yf5vg4ymixg4iwx7aci3xp2zdhaq3i") (features (quote (("glium-support" "glium"))))))

(define-public crate-obj-rs-0.4 (crate (name "obj-rs") (vers "0.4.6") (deps (list (crate-dep (name "glium") (req "*") (optional #t) (kind 0)))) (hash "1jijkb7haakrvc4qwfy05bgcixm1p094qck933565l8bbrp9bxrn") (features (quote (("glium-support" "glium"))))))

(define-public crate-obj-rs-0.4 (crate (name "obj-rs") (vers "0.4.7") (deps (list (crate-dep (name "glium") (req "*") (optional #t) (kind 0)))) (hash "1c3p8fpwqrqq123m6b160zqpr9ff5n38jkc8siyla0bxb240fply") (features (quote (("glium-support" "glium"))))))

(define-public crate-obj-rs-0.4 (crate (name "obj-rs") (vers "0.4.8") (deps (list (crate-dep (name "glium") (req "*") (optional #t) (kind 0)))) (hash "06rlg353ica1kw3axxrq0k54rd7islahnznm72qd7h07gdrhlsvv") (features (quote (("glium-support" "glium"))))))

(define-public crate-obj-rs-0.4 (crate (name "obj-rs") (vers "0.4.9") (deps (list (crate-dep (name "glium") (req "*") (optional #t) (kind 0)))) (hash "0gkxnfalz6qbbr6cj7y56d0wm9c9cis985hwbbjmi8320kr280hg") (features (quote (("glium-support" "glium"))))))

(define-public crate-obj-rs-0.4 (crate (name "obj-rs") (vers "0.4.10") (deps (list (crate-dep (name "glium") (req "^0.8.2") (optional #t) (kind 0)) (crate-dep (name "vec_map") (req "^0.3.0") (default-features #t) (kind 0)))) (hash "1b6v4mf4l6q54zmf1x317z78yfsz40rfwjcvpp91vdpym88cx384") (features (quote (("glium-support" "glium"))))))

(define-public crate-obj-rs-0.4 (crate (name "obj-rs") (vers "0.4.11") (deps (list (crate-dep (name "glium") (req "^0.8.2") (optional #t) (kind 0)) (crate-dep (name "vec_map") (req "^0.3.0") (default-features #t) (kind 0)))) (hash "0z15n1v3cd4lrvqaqqc37srv4c6iq05m5pjflsh2aykd4xc0j8v5") (features (quote (("glium-support" "glium"))))))

(define-public crate-obj-rs-0.4 (crate (name "obj-rs") (vers "0.4.12") (deps (list (crate-dep (name "glium") (req "^0.8.2") (optional #t) (kind 0)) (crate-dep (name "vec_map") (req "^0.3.0") (default-features #t) (kind 0)))) (hash "0pwc9ivs8hfpg3n0idzckvlqjqgq4a8vz1l7qw5php499viv1a02") (features (quote (("glium-support" "glium"))))))

(define-public crate-obj-rs-0.4 (crate (name "obj-rs") (vers "0.4.13") (deps (list (crate-dep (name "glium") (req "^0.8.2") (optional #t) (kind 0)) (crate-dep (name "rustc-serialize") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "vec_map") (req "^0.3.0") (default-features #t) (kind 0)))) (hash "1dldg6r7sxm9wd4c1sil3l3pg9696xcyr9f5azkiqdj6wv5pv7nn") (features (quote (("glium-support" "glium"))))))

(define-public crate-obj-rs-0.4 (crate (name "obj-rs") (vers "0.4.14") (deps (list (crate-dep (name "glium") (req "^0.8.2") (optional #t) (kind 0)) (crate-dep (name "rustc-serialize") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "vec_map") (req "^0.3.0") (default-features #t) (kind 0)))) (hash "1482ksn33wx40iv4056vqb35ksybq3gbacjwgzy870jpc75g4510") (features (quote (("glium-support" "glium"))))))

(define-public crate-obj-rs-0.4 (crate (name "obj-rs") (vers "0.4.15") (deps (list (crate-dep (name "glium") (req "^0.8.2") (optional #t) (kind 0)) (crate-dep (name "rustc-serialize") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "vec_map") (req "^0.3.0") (default-features #t) (kind 0)))) (hash "114wc2lviazcdxykx1wa5x91b1n0zzf5cwigckkvr0kdiy6vrqvz") (features (quote (("glium-support" "glium"))))))

(define-public crate-obj-rs-0.4 (crate (name "obj-rs") (vers "0.4.16") (deps (list (crate-dep (name "glium") (req "^0.14.0") (optional #t) (kind 0)) (crate-dep (name "glium") (req "^0.14.0") (default-features #t) (kind 2)) (crate-dep (name "rustc-serialize") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "vec_map") (req "^0.6.0") (default-features #t) (kind 0)))) (hash "01c8p6x98yf30pnkgm0gk5jgnbaf6fzrcy9461l71yzpypghpah6") (features (quote (("glium-support" "glium"))))))

(define-public crate-obj-rs-0.4 (crate (name "obj-rs") (vers "0.4.17") (deps (list (crate-dep (name "glium") (req "^0.14.0") (optional #t) (kind 0)) (crate-dep (name "glium") (req "^0.14.0") (default-features #t) (kind 2)) (crate-dep (name "rustc-serialize") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "vec_map") (req "^0.6.0") (default-features #t) (kind 0)))) (hash "1k12mjcs9dgcs8ijfvfsvkdlvyc3352v0sdyklcklbxwvqi7rkr8") (features (quote (("glium-support" "glium"))))))

(define-public crate-obj-rs-0.4 (crate (name "obj-rs") (vers "0.4.18") (deps (list (crate-dep (name "glium") (req "^0.14.0") (optional #t) (kind 0)) (crate-dep (name "glium") (req "^0.14.0") (default-features #t) (kind 2)) (crate-dep (name "rustc-serialize") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "vec_map") (req "^0.6.0") (default-features #t) (kind 0)))) (hash "0qnxpqbyb29xqlskrsrsi9ncrl6br7h6gi1iq8sii3n70bpylxq9") (features (quote (("glium-support" "glium"))))))

(define-public crate-obj-rs-0.4 (crate (name "obj-rs") (vers "0.4.19") (deps (list (crate-dep (name "glium") (req "^0.14.0") (optional #t) (kind 0)) (crate-dep (name "glium") (req "^0.14.0") (default-features #t) (kind 2)) (crate-dep (name "rustc-serialize") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "vec_map") (req "^0.6.0") (default-features #t) (kind 0)))) (hash "1jhmjlryx7xw8sxxsxid7ykn9mxw3calbpzs0y2c0rljbh9r7f79") (features (quote (("glium-support" "glium"))))))

(define-public crate-obj-rs-0.4 (crate (name "obj-rs") (vers "0.4.20") (deps (list (crate-dep (name "glium") (req "^0.14.0") (optional #t) (kind 0)) (crate-dep (name "glium") (req "^0.14.0") (default-features #t) (kind 2)) (crate-dep (name "serde") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "vec_map") (req "^0.6.0") (default-features #t) (kind 0)))) (hash "03f5r41n5bhbasvs70vrg9pmx8ji0k3y3diyy9p9djw1r9qrggm8") (features (quote (("glium-support" "glium")))) (yanked #t)))

(define-public crate-obj-rs-0.4 (crate (name "obj-rs") (vers "0.4.21") (deps (list (crate-dep (name "glium") (req "^0.22.0") (optional #t) (kind 0)) (crate-dep (name "glium") (req "^0.22.0") (default-features #t) (kind 2)) (crate-dep (name "num") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "vec_map") (req "^0.6.0") (default-features #t) (kind 0)))) (hash "1yxhi77j80lc7c694y50p92gbp7mx68zyppnhb7z2kvsipdk4285") (features (quote (("glium-support" "glium")))) (yanked #t)))

(define-public crate-obj-rs-0.5 (crate (name "obj-rs") (vers "0.5.0") (deps (list (crate-dep (name "glium") (req "^0.22.0") (optional #t) (kind 0)) (crate-dep (name "glium") (req "^0.22.0") (default-features #t) (kind 2)) (crate-dep (name "num") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "vec_map") (req "^0.6.0") (default-features #t) (kind 0)))) (hash "1cm3mywyhdwq8kddn0nm863zvgha9d8aj18y8652nwglyi5qpwry") (features (quote (("glium-support" "glium"))))))

(define-public crate-obj-rs-0.6 (crate (name "obj-rs") (vers "0.6.0") (deps (list (crate-dep (name "glium") (req "^0.26.0") (optional #t) (kind 0)) (crate-dep (name "glium") (req "^0.26.0") (default-features #t) (kind 2)) (crate-dep (name "num") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "vec_map") (req "^0.6.0") (default-features #t) (kind 0)))) (hash "0acnw94rk7b9v74c6wqc7ch3g56pjs1qx6fhbci1ylk7kw4cng6f") (features (quote (("glium-support" "glium"))))))

(define-public crate-obj-rs-0.6 (crate (name "obj-rs") (vers "0.6.1") (deps (list (crate-dep (name "glium") (req "^0.26.0") (optional #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2.11") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "vec_map") (req "^0.8.2") (default-features #t) (kind 0)))) (hash "1ppp9mkfcb5gy3fkh42b678bmqj94c70ynlw408kir88jhz0f95w") (features (quote (("glium-support" "glium") ("default" "serde"))))))

(define-public crate-obj-rs-0.6 (crate (name "obj-rs") (vers "0.6.2") (deps (list (crate-dep (name "glium") (req "^0.26.0") (optional #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2.11") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "vec_map") (req "^0.8.2") (default-features #t) (kind 0)) (crate-dep (name "vulkano") (req "^0.19.0") (optional #t) (default-features #t) (kind 0)))) (hash "0zl4lxk3i7af2pg8k2kmv6ddpc1h8gski5sk592bydh65cjdsdyg") (features (quote (("glium-support" "glium") ("default" "serde"))))))

(define-public crate-obj-rs-0.6 (crate (name "obj-rs") (vers "0.6.3") (deps (list (crate-dep (name "glium") (req ">=0.26.0, <0.30.0") (optional #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2.11") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "vec_map") (req "^0.8.2") (default-features #t) (kind 0)) (crate-dep (name "vulkano") (req "^0.19.0") (optional #t) (default-features #t) (kind 0)))) (hash "0k713aywfhr3m1ni7i528pvlxvjki1aaj507rgv1jcw3qmb5zyr1") (features (quote (("glium-support" "glium") ("default" "serde"))))))

(define-public crate-obj-rs-0.6 (crate (name "obj-rs") (vers "0.6.4") (deps (list (crate-dep (name "glium") (req ">=0.26.0, <0.30.0") (optional #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2.11") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "vec_map") (req "^0.8.2") (default-features #t) (kind 0)) (crate-dep (name "vulkano") (req "^0.19.0") (optional #t) (default-features #t) (kind 0)))) (hash "1qnp80q7cpjmiq6mdxz9ggwzlcllm5sh75f90xvrxgmbh3cfyv99") (features (quote (("glium-support" "glium") ("default" "serde"))))))

(define-public crate-obj-rs-0.7 (crate (name "obj-rs") (vers "0.7.0") (deps (list (crate-dep (name "glium") (req ">=0.26.0, <0.30.0") (optional #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2.11") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "vulkano") (req "^0.19.0") (optional #t) (default-features #t) (kind 0)))) (hash "1d0xrd9syim9j5aag3wdxqihyar88yl3y7a5173abnrkaij2y2n9") (features (quote (("glium-support" "glium") ("default" "serde"))))))

(define-public crate-obj-rs-0.7 (crate (name "obj-rs") (vers "0.7.1") (deps (list (crate-dep (name "bytemuck") (req "^1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "glium") (req ">=0.26.0, <0.33.0") (optional #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2.11") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "vulkano") (req ">=0.19.0, <0.33.0") (optional #t) (default-features #t) (kind 0)))) (hash "18qxxblh9m6a7ipbmq0pa0wzzlsbbbmj1y2371zdqnm670jih0m5") (features (quote (("glium-support" "glium") ("default" "serde")))) (v 2) (features2 (quote (("vulkano" "dep:vulkano" "dep:bytemuck") ("serde" "dep:serde") ("glium" "dep:glium"))))))

