(define-module (crates-io ob ex) #:use-module (crates-io))

(define-public crate-obex-0.1 (crate (name "obex") (vers "0.1.0") (deps (list (crate-dep (name "git2") (req "^0.13.11") (default-features #t) (kind 0)))) (hash "1i1q5vndixi9avhqqih76l9phhb80x0lgk0xbdab20ard1mbccm1")))

(define-public crate-obex-0.2 (crate (name "obex") (vers "0.2.0") (deps (list (crate-dep (name "git2") (req "^0.13.11") (default-features #t) (kind 0)))) (hash "1fsfh4y032qy1jwxlkg3c9j5kh8m5wk6xb3k1hxnpg9fpnvzh092")))

