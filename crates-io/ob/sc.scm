(define-module (crates-io ob sc) #:use-module (crates-io))

(define-public crate-obscura-0.0.1 (crate (name "obscura") (vers "0.0.1") (hash "09j6xm1m01g343b00phv7kw9d89nccpb66mjk5iwkibhnlxiw22v")))

(define-public crate-obscura-0.0.2 (crate (name "obscura") (vers "0.0.2") (hash "0j9yb7scbfb98drwl6gkp6fjq6waycsyy6givy8i64lqmgaaqq09")))

(define-public crate-obscurus-0.1 (crate (name "obscurus") (vers "0.1.0") (hash "1sqy1kc5wibcl3fycn6gnfk5616jhg5wzwnzy7qp9wg4qvvq1b98")))

