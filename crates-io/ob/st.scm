(define-module (crates-io ob st) #:use-module (crates-io))

(define-public crate-obstack-0.1 (crate (name "obstack") (vers "0.1.0") (deps (list (crate-dep (name "bencher") (req "^0.1.4") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.3.16") (default-features #t) (kind 2)))) (hash "1zrdf9iykk9jabdxmz2qsf3p74h0id3nmnw558s476h8wh7x2cx0")))

(define-public crate-obstack-0.1 (crate (name "obstack") (vers "0.1.1") (deps (list (crate-dep (name "bencher") (req "^0.1.4") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.3.16") (default-features #t) (kind 2)))) (hash "0ncb2dns7whyrwmyhssi63dcyk39lxq9nyf5inssq5i8h01b5m55")))

(define-public crate-obstack-0.1 (crate (name "obstack") (vers "0.1.2") (deps (list (crate-dep (name "bencher") (req "^0.1.4") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.3.16") (default-features #t) (kind 2)))) (hash "1az1zgp4am5kdivlz6kiyaiml65amfzdrwmi4wbnag4ynjra64xq")))

(define-public crate-obstack-0.1 (crate (name "obstack") (vers "0.1.3") (deps (list (crate-dep (name "bencher") (req "^0.1.4") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.3.16") (default-features #t) (kind 2)))) (hash "0c48jnnw7na9yphpm7kbychhmyb5di7276amccyavp3s4n63ww6l")))

(define-public crate-obstack-0.1 (crate (name "obstack") (vers "0.1.4") (deps (list (crate-dep (name "bencher") (req "^0.1.4") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.3.16") (default-features #t) (kind 2)))) (hash "1kabk9yh44b143d4ncj76ph0dij0kmyx22vbmvif2cf3sxn0l6pd")))

(define-public crate-obstacle-0.0.2 (crate (name "obstacle") (vers "0.0.2") (deps (list (crate-dep (name "aws-creds") (req "^0.35.0") (default-features #t) (kind 2)) (crate-dep (name "futures") (req "^0.3.28") (default-features #t) (kind 0)) (crate-dep (name "futures-util") (req "^0.3.28") (default-features #t) (kind 0)) (crate-dep (name "home") (req "^0.5.5") (default-features #t) (kind 0)) (crate-dep (name "memmap2") (req "^0.7.1") (default-features #t) (kind 0)) (crate-dep (name "object_store") (req "^0.6.1") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.9.1") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.29.1") (features (quote ("net" "rt-multi-thread"))) (default-features #t) (kind 0)) (crate-dep (name "url") (req "^2.4.0") (default-features #t) (kind 0)) (crate-dep (name "uuid") (req "^1.4.0") (features (quote ("v4"))) (default-features #t) (kind 0)))) (hash "0mbw1z0kl6mla7qnyv0qqgin2x92k16d2r85ql6fs7cl02c77ix3") (features (quote (("serde-lazy") ("http" "async" "object_store/http") ("gcp" "async" "object_store/gcp") ("azure" "async" "object_store/azure") ("aws" "async" "object_store/aws") ("async"))))))

(define-public crate-obstinate-0.1 (crate (name "obstinate") (vers "0.1.0") (hash "1ymnvjkyv7rkdkmdfsg5ig1kichcqm68wbgm5gn9lf65ya5zmisd") (yanked #t)))

(define-public crate-obstinate-0.1 (crate (name "obstinate") (vers "0.1.1") (deps (list (crate-dep (name "memmap2") (req "^0.7.1") (default-features #t) (kind 0)) (crate-dep (name "object_store") (req "^0.6.1") (default-features #t) (kind 0)))) (hash "1dwc8622m4sz0s0nl7k3yi6z8w2asbzig70nfvvc72cwxl2kr921") (features (quote (("http" "async" "object_store/http") ("gcp" "async" "object_store/gcp") ("azure" "async" "object_store/azure") ("aws" "async" "object_store/aws") ("async")))) (yanked #t)))

(define-public crate-obstinate-0.1 (crate (name "obstinate") (vers "0.1.2") (deps (list (crate-dep (name "memmap2") (req "^0.7.1") (default-features #t) (kind 0)) (crate-dep (name "object_store") (req "^0.6.1") (default-features #t) (kind 0)))) (hash "19455w4c9ak5chzrc56m6xzmnwxydwa3xg1lw2ibh6z7f82mixhp") (features (quote (("http" "async" "object_store/http") ("gcp" "async" "object_store/gcp") ("azure" "async" "object_store/azure") ("aws" "async" "object_store/aws") ("async")))) (yanked #t)))

(define-public crate-obstruct-0.1 (crate (name "obstruct") (vers "0.1.0") (deps (list (crate-dep (name "obstruct-macros") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "trybuild") (req "^1.0.85") (default-features #t) (kind 0)))) (hash "1flpy7a5y9w0apam1gdypkw0iicmhy5g0kx50jkqkji22sxjlvv1")))

(define-public crate-obstruct-macros-0.1 (crate (name "obstruct-macros") (vers "0.1.0") (deps (list (crate-dep (name "itertools") (req "^0.10") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2.0.2") (features (quote ("full" "parsing" "printing" "extra-traits"))) (default-features #t) (kind 0)))) (hash "00irir9c8q5hpvyqafhdj9by8m014rlh0ar3wlrhgssdlblq8q0h")))

