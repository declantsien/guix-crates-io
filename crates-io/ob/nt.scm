(define-module (crates-io ob nt) #:use-module (crates-io))

(define-public crate-obnth-0.1 (crate (name "obnth") (vers "0.1.0") (deps (list (crate-dep (name "bitflags") (req "^1.2") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2") (features (quote ("extra_traits"))) (default-features #t) (kind 0)) (crate-dep (name "tempfile") (req "^3.1") (default-features #t) (kind 2)))) (hash "0a8k66fvqkpgvvs5db6rqc5c6dr9j34n561x05qy92rpcy0ymh2j") (features (quote (("openat2") ("default" "openat2"))))))

(define-public crate-obnth-0.1 (crate (name "obnth") (vers "0.1.1") (deps (list (crate-dep (name "bitflags") (req "^1.2") (default-features #t) (kind 0)) (crate-dep (name "cfg-if") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2") (features (quote ("extra_traits"))) (default-features #t) (kind 0)) (crate-dep (name "tempfile") (req "^3.1") (default-features #t) (kind 2)))) (hash "16bqlr22kk9r8jk5667c32dxqhqgqky77i16hycfpm0lgzv90jkw") (features (quote (("openat2") ("default" "openat2"))))))

