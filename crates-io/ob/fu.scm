(define-module (crates-io ob fu) #:use-module (crates-io))

(define-public crate-obfuscate-0.0.0 (crate (name "obfuscate") (vers "0.0.0") (hash "0w4jfzyfr6hsp05ic45jncyh44v2fnpnsg1fi5fd7irwwk9lyr7f")))

(define-public crate-obfuscate-integer-0.1 (crate (name "obfuscate-integer") (vers "0.1.0") (hash "0ysx2cy9qq6hd6pr23wmjfchnp2ymghpif00iy59cc346bzhqgr7")))

(define-public crate-obfuscate-integer-0.1 (crate (name "obfuscate-integer") (vers "0.1.1") (hash "02pl2q5vk0kgmfvj3i0gh95lkjdw7s3n0zabmkkp8s2pd5mcr8qz") (features (quote (("forbid-unsafe") ("default")))) (yanked #t)))

(define-public crate-obfuscate-integer-0.1 (crate (name "obfuscate-integer") (vers "0.1.2") (hash "1k79dv59vjsm8935325rp6dajgg466fwkhaf2j8bc1w3kr9an3y2") (features (quote (("forbid-unsafe") ("default"))))))

(define-public crate-obfuscation-0.0.0 (crate (name "obfuscation") (vers "0.0.0") (hash "1vqvrfnbzj8r0cicfvq9n8xsrn538jplg6z5jcx7xflbk9ixc75p")))

(define-public crate-obfustring-0.5 (crate (name "obfustring") (vers "0.5.0") (deps (list (crate-dep (name "quote") (req "^1.0.23") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.107") (default-features #t) (kind 0)))) (hash "1j0sixxgyjkdsm0yqhd43hkfbxfppiph9cx2rgw9g5llqfy0vr6a")))

