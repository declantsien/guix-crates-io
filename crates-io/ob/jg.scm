(define-module (crates-io ob jg) #:use-module (crates-io))

(define-public crate-objgrep-1 (crate (name "objgrep") (vers "1.0.0") (deps (list (crate-dep (name "PrintLib") (req "^1.5.2") (default-features #t) (kind 0)) (crate-dep (name "capstone") (req "^0.12.0") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^4.5.4") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "object") (req "^0.34.0") (default-features #t) (kind 0)) (crate-dep (name "pad") (req "^0.1.6") (default-features #t) (kind 0)))) (hash "1nyrbwpp5fzjycw39pj785wkva704k21ygcx11rzccz2x9hkhsnd")))

