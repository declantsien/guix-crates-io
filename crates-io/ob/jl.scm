(define-module (crates-io ob jl) #:use-module (crates-io))

(define-public crate-objld-0.2 (crate (name "objld") (vers "0.2.0") (deps (list (crate-dep (name "nom") (req "7.1.*") (default-features #t) (kind 0)) (crate-dep (name "rayon") (req "1.5.*") (default-features #t) (kind 0)))) (hash "1r4c0z6zj6kk56snihwpnkkfyyxy3acr00rqc060dqbsskwpf5ip")))

(define-public crate-objld-0.3 (crate (name "objld") (vers "0.3.0") (deps (list (crate-dep (name "nom") (req "7.1.*") (default-features #t) (kind 0)) (crate-dep (name "rayon") (req "1.5.*") (default-features #t) (kind 0)))) (hash "1s9k4n7xx0fa0czzmvdm6m6bzafcjpjmwkah75b70b4f7hmjfga6")))

(define-public crate-objld-0.4 (crate (name "objld") (vers "0.4.0") (deps (list (crate-dep (name "nom") (req "7.1.*") (default-features #t) (kind 0)) (crate-dep (name "rayon") (req "1.5.*") (default-features #t) (kind 0)))) (hash "1589bq7dipinjdglk33kwy3la9m9q3li5hpfkvc55fc0nal79gbb")))

(define-public crate-objld-0.5 (crate (name "objld") (vers "0.5.0") (deps (list (crate-dep (name "nom") (req "7.1.*") (default-features #t) (kind 0)) (crate-dep (name "rayon") (req "1.5.*") (default-features #t) (kind 0)))) (hash "0xwv9p417xjwzb2200795x4hf0xalnmsk15rxkbyb2pvchylbwwg")))

