(define-module (crates-io ob fs) #:use-module (crates-io))

(define-public crate-obfs-0.0.0 (crate (name "obfs") (vers "0.0.0") (hash "19q3a7nijhlia3yja1faw0if74785y3b092n2jw6rafi59ljycpp")))

(define-public crate-obfstr-0.1 (crate (name "obfstr") (vers "0.1.0") (deps (list (crate-dep (name "obfstr-impl") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "06ybr7pg0r86whczq1kl4k6nggf1g8q9a4j3lc6sf2fdkzawv1c3") (features (quote (("unsafe_static_str") ("rand" "obfstr-impl/rand") ("default" "rand"))))))

(define-public crate-obfstr-0.1 (crate (name "obfstr") (vers "0.1.1") (deps (list (crate-dep (name "obfstr-impl") (req "^0.1.1") (default-features #t) (kind 0)))) (hash "17w9rgx31mf382y2izjhvksn953w1sw09bq1q7zqxgvbf9x6gpjg") (features (quote (("unsafe_static_str") ("rand" "obfstr-impl/rand") ("default" "rand"))))))

(define-public crate-obfstr-0.2 (crate (name "obfstr") (vers "0.2.0") (hash "0f6b1p8x552k5ph384vzpdns63f2zb0mhv1jqgwry0ik0apcsf88") (features (quote (("unsafe_static_str"))))))

(define-public crate-obfstr-0.2 (crate (name "obfstr") (vers "0.2.1") (hash "0ash27mz2l1fzlvdxyq22hg556m5nhjanj59lchjbajj57nv7fgn") (features (quote (("unsafe_static_str"))))))

(define-public crate-obfstr-0.2 (crate (name "obfstr") (vers "0.2.2") (hash "0jgiz6y9my6nnn9k4qhsi19wgmm7cd0avm3x0m4kssrj46pjlb4c") (features (quote (("unsafe_static_str"))))))

(define-public crate-obfstr-0.2 (crate (name "obfstr") (vers "0.2.3") (hash "10dhik130j2qpcia4hqq4dbgd128zmpabhm8mav5pfp84n0p6gph") (features (quote (("unsafe_static_str"))))))

(define-public crate-obfstr-0.2 (crate (name "obfstr") (vers "0.2.4") (hash "09yk87w6bl6mq6ybvjr15bzv4bqv059qqign4g721z69lf402cmp") (features (quote (("unsafe_static_str"))))))

(define-public crate-obfstr-0.2 (crate (name "obfstr") (vers "0.2.5") (hash "1h9bjp2g8vv5jfrqx8a4cz5z9r1lcy9k8m606cdgb7q18001nckh") (features (quote (("unsafe_static_str"))))))

(define-public crate-obfstr-0.2 (crate (name "obfstr") (vers "0.2.6") (hash "0l4ixd3adsa0pfkiawzgc1mnyg5485hn7i1b6wcgk3568271vl5b") (features (quote (("unsafe_static_str"))))))

(define-public crate-obfstr-0.3 (crate (name "obfstr") (vers "0.3.0") (hash "0y5azgk8ds43m1sd5pc4hhakxzxkfcfscl24y8gsbvwdznxjqavv")))

(define-public crate-obfstr-0.4 (crate (name "obfstr") (vers "0.4.0") (hash "0h527nz3563mlbljbbp42jkh4xf5gg4ziy583ql8xszh7dpbssdr")))

(define-public crate-obfstr-0.4 (crate (name "obfstr") (vers "0.4.1") (hash "0hn582zzf97hlsz5gfmlzf51inx2kphzv0s59y1yczgdzj12k8s3")))

(define-public crate-obfstr-0.4 (crate (name "obfstr") (vers "0.4.2") (hash "0ic8f9jrc5iylkyk9bjm138mkdap173mbpfvl3v1w94z1gnx1gbj") (yanked #t)))

(define-public crate-obfstr-0.4 (crate (name "obfstr") (vers "0.4.3") (hash "1p2ihfqrsi12sqaf1i87m27vdiphzsbyydrqs6k11jbcp1wjkfp3")))

(define-public crate-obfstr-impl-0.1 (crate (name "obfstr-impl") (vers "0.1.0") (deps (list (crate-dep (name "rand") (req "^0.6") (optional #t) (default-features #t) (kind 0)))) (hash "19krc8xwxp2895m2zvcs97l26mrk76b8i5xhjjaxc615bz5qx96q")))

(define-public crate-obfstr-impl-0.1 (crate (name "obfstr-impl") (vers "0.1.1") (deps (list (crate-dep (name "rand") (req "^0.6") (optional #t) (default-features #t) (kind 0)))) (hash "1r35gvbdv4zw3m82y7fh2szyjhc71z2hnx4zvfjsdphpra5gyxfr")))

