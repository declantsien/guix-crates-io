(define-module (crates-io ob jf) #:use-module (crates-io))

(define-public crate-objforces-0.1 (crate (name "objforces") (vers "0.1.0") (deps (list (crate-dep (name "libm") (req "^0.2.8") (default-features #t) (kind 0)))) (hash "1cjpmmc3h4ki9k4f70nw1lha5y2pwka9b190y28766fsfqla6wf6") (features (quote (("std")))) (rust-version "1.73")))

