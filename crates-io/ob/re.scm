(define-module (crates-io ob re) #:use-module (crates-io))

(define-public crate-obrewin-0.0.1 (crate (name "obrewin") (vers "0.0.1") (deps (list (crate-dep (name "pyo3") (req "^0.19.0") (default-features #t) (kind 0)))) (hash "06rm260l65rzd16cfxpd70ccwyxw08rzbdbk2z5wh4rzig82c98q")))

