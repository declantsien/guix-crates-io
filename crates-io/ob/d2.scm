(define-module (crates-io ob d2) #:use-module (crates-io))

(define-public crate-obd2-0.1 (crate (name "obd2") (vers "0.1.0") (deps (list (crate-dep (name "env_logger") (req "^0.10") (default-features #t) (kind 0)) (crate-dep (name "ftdi") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1") (default-features #t) (kind 0)))) (hash "0ns2i0n35dp9xf16m24bsixfsx831pqpq5nng10qc1vj6mixvdf2")))

(define-public crate-obd2-0.2 (crate (name "obd2") (vers "0.2.0-pre1") (deps (list (crate-dep (name "env_logger") (req "^0.10") (default-features #t) (kind 0)) (crate-dep (name "ftdi") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1") (default-features #t) (kind 0)))) (hash "1ldklca06rm0bjkvr6457mpljini0jnaw2ajs3q92qkgqj1pxmk4")))

(define-public crate-obd2-0.2 (crate (name "obd2") (vers "0.2.0-pre2") (deps (list (crate-dep (name "env_logger") (req "^0.10") (default-features #t) (kind 0)) (crate-dep (name "ftdi") (req "^0.1.3") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.8") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.15") (default-features #t) (kind 0)))) (hash "1ngja2yr4za8bqa1asihjhzkcs4jy9dpd7gmx8xfzb12sm1b8nr1")))

(define-public crate-obd2-0.2 (crate (name "obd2") (vers "0.2.0-pre3") (deps (list (crate-dep (name "env_logger") (req "^0.10") (default-features #t) (kind 0)) (crate-dep (name "ftdi") (req "^0.1.3") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.8") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.15") (default-features #t) (kind 0)))) (hash "13nz5bb0gqq88zql7faki007c7b1amkcimrhpjw3bnn3p8iij3bz")))

