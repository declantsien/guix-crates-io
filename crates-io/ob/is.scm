(define-module (crates-io ob is) #:use-module (crates-io))

(define-public crate-obis-1 (crate (name "obis") (vers "1.0.0") (deps (list (crate-dep (name "atoi") (req "^2.0.0") (kind 0)))) (hash "1pc4d4z1fb63ps04db8lp0cmpjm3f6fg8xddxjih14pnk8cgwqsg")))

