(define-module (crates-io ob f-) #:use-module (crates-io))

(define-public crate-obf-rs-0.1 (crate (name "obf-rs") (vers "0.1.0") (deps (list (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_with") (req "^1.9.4") (default-features #t) (kind 0)))) (hash "0jd7bmfhwlcwlw74syzrqc87l64mhz8icr9fvzadnw0d077887s1")))

(define-public crate-obf-rs-0.1 (crate (name "obf-rs") (vers "0.1.1") (deps (list (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_with") (req "^1.9.4") (default-features #t) (kind 0)))) (hash "14lbfz4qkql5y72w802pvh2airby13rjlmqm1cq8dcprryqmg5lk")))

