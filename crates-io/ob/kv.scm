(define-module (crates-io ob kv) #:use-module (crates-io))

(define-public crate-obkv-0.1 (crate (name "obkv") (vers "0.1.0") (deps (list (crate-dep (name "quickcheck") (req "^0.9.2") (default-features #t) (kind 2)))) (hash "071iv95mk4fq2abm5d7ilgiqgfbw3xwav3rf1wjyg0sx2caa4c6f")))

(define-public crate-obkv-0.1 (crate (name "obkv") (vers "0.1.1") (deps (list (crate-dep (name "quickcheck") (req "^0.9.2") (default-features #t) (kind 2)))) (hash "1s28zb0nj2nnc48lqhcjny4c668swarsancj6kzdlfigmahabn6x")))

(define-public crate-obkv-0.2 (crate (name "obkv") (vers "0.2.0") (deps (list (crate-dep (name "quickcheck") (req "^0.9.2") (default-features #t) (kind 2)))) (hash "11d3mc0v4qydckk3qn3n65kk4z47zpdpya513lmbanwfgk6li7pn")))

(define-public crate-obkv-0.2 (crate (name "obkv") (vers "0.2.1") (deps (list (crate-dep (name "quickcheck") (req "^0.9.2") (default-features #t) (kind 2)))) (hash "100hv36g93irj8l3d50w87hhxjmfmbmm7l48zwwwcmk089192ibc")))

