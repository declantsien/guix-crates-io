(define-module (crates-io ob i-) #:use-module (crates-io))

(define-public crate-obi-derive-0.0.1 (crate (name "obi-derive") (vers "0.0.1") (deps (list (crate-dep (name "obi-derive-internal") (req "^0.0.1") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^0.3.15") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1") (features (quote ("full" "fold"))) (default-features #t) (kind 0)))) (hash "19c4ky485j5gkpg3n0brxxnk1ax3gz7v32xn0qqsqdqyqfqik22g")))

(define-public crate-obi-derive-0.0.2 (crate (name "obi-derive") (vers "0.0.2") (deps (list (crate-dep (name "obi-derive-internal") (req "^0.0.2") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^0.3.15") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.62") (features (quote ("full" "fold"))) (default-features #t) (kind 0)))) (hash "0ppjkp2sgb3i52imf9xx507zc2ns2n7aznkxcl71zgx60glr7sjm")))

(define-public crate-obi-derive-internal-0.0.1 (crate (name "obi-derive-internal") (vers "0.0.1") (deps (list (crate-dep (name "proc-macro2") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1") (features (quote ("full" "fold"))) (default-features #t) (kind 0)))) (hash "1yx754drbr6ivdlmiyix0k6icw5ggniva0clgb2afnm77h0qnfn6")))

(define-public crate-obi-derive-internal-0.0.2 (crate (name "obi-derive-internal") (vers "0.0.2") (deps (list (crate-dep (name "proc-macro2") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.62") (features (quote ("full" "fold"))) (default-features #t) (kind 0)))) (hash "17fhn0g3f336hl479ms0ny4ygwsa213c81n1li3qlmfpzzyn81mh")))

