(define-module (crates-io ob oz) #:use-module (crates-io))

(define-public crate-obozrenie-core-0.1 (crate (name "obozrenie-core") (vers "0.1.0") (hash "02yg0p2zdwhf3s7zmpzqal9zsby5miw2vcvn55am1pvipfp436cf")))

(define-public crate-obozrenie-gtk-0.1 (crate (name "obozrenie-gtk") (vers "0.1.0") (deps (list (crate-dep (name "gio") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "gtk") (req "^0.1.0") (features (quote ("v3_10"))) (default-features #t) (kind 0)) (crate-dep (name "obozrenie-core") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "023z412288218jqgx3z7ca8b888kxyk7i5l0r5ma3vgjfpb1c2pr")))

