(define-module (crates-io ob jp) #:use-module (crates-io))

(define-public crate-objpoke-0.1 (crate (name "objpoke") (vers "0.1.0") (deps (list (crate-dep (name "eyre") (req "^0.6.3") (default-features #t) (kind 0)) (crate-dep (name "goblin") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.3.9") (default-features #t) (kind 0)) (crate-dep (name "scroll") (req "^0.10") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.3") (default-features #t) (kind 0)))) (hash "0qdmi53108hm4g0s7hjc1n0vl4slpncacywglz6f61bv0j2h1pgx")))

(define-public crate-objpoke-0.2 (crate (name "objpoke") (vers "0.2.0") (deps (list (crate-dep (name "eyre") (req "^0.6.3") (default-features #t) (kind 0)) (crate-dep (name "goblin") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.3.9") (default-features #t) (kind 0)) (crate-dep (name "scroll") (req "^0.10") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.3") (default-features #t) (kind 0)))) (hash "19p18z51ixf0v25yx3n342c144hs79naflglki36lra0g9zs82w0")))

(define-public crate-objpoke-0.2 (crate (name "objpoke") (vers "0.2.1") (deps (list (crate-dep (name "eyre") (req "^0.6.3") (default-features #t) (kind 0)) (crate-dep (name "goblin") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.3.9") (default-features #t) (kind 0)) (crate-dep (name "scroll") (req "^0.10") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.3") (default-features #t) (kind 0)))) (hash "19wy1q9mpp5g19hb26x4g0hc12k18v580cb1rrab12f9ly8zjf42")))

(define-public crate-objpoke-0.2 (crate (name "objpoke") (vers "0.2.3") (deps (list (crate-dep (name "eyre") (req "^0.6.3") (default-features #t) (kind 0)) (crate-dep (name "goblin") (req "^0.4.3") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.3.9") (default-features #t) (kind 0)) (crate-dep (name "scroll") (req "^0.10") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.3") (default-features #t) (kind 0)))) (hash "13xpcw34y8f4zckbiyml6z1yljbg8v5w1679i6fxz968dbpcpxpj")))

(define-public crate-objpoke-0.2 (crate (name "objpoke") (vers "0.2.4") (deps (list (crate-dep (name "anyhow") (req "^1.0.56") (features (quote ("backtrace"))) (default-features #t) (kind 0)) (crate-dep (name "goblin") (req "^0.5") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.3.9") (default-features #t) (kind 0)) (crate-dep (name "scroll") (req "^0.11") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.3") (default-features #t) (kind 0)))) (hash "15gp3h015l8xqwraiay273pblx2ckvi9lkjm45ic5a98iabgf0ss")))

(define-public crate-objpoke-0.3 (crate (name "objpoke") (vers "0.3.0") (deps (list (crate-dep (name "anyhow") (req "^1.0.56") (features (quote ("backtrace"))) (default-features #t) (kind 0)) (crate-dep (name "goblin") (req "=0.4.1") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.3.9") (default-features #t) (kind 0)) (crate-dep (name "scroll") (req "^0.10") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.3") (default-features #t) (kind 0)))) (hash "0c0ca2jj0ivjrlcwjgm8xp4g9r0z5875w1i7533qpqp43j8x49pj")))

(define-public crate-objpool-0.1 (crate (name "objpool") (vers "0.1.0") (hash "04rv8n57kksvbsw5c2rci6f81prbpljs8jccnj9va7vi0j7hhdlh")))

(define-public crate-objpool-0.2 (crate (name "objpool") (vers "0.2.0") (hash "16pk9hgljr82bybk0m7mcdvsqaqblc8pbxsbfy4nzrsg0hqnb74l")))

