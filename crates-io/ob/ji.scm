(define-module (crates-io ob ji) #:use-module (crates-io))

(define-public crate-objid-0.1 (crate (name "objid") (vers "0.1.0") (deps (list (crate-dep (name "rand") (req "^0.7") (default-features #t) (kind 0)))) (hash "0gq52plk4vynli7gqcgagmqm1glbgncrg11svmbwh0yyxgi6ylqd")))

(define-public crate-objid-0.1 (crate (name "objid") (vers "0.1.1") (deps (list (crate-dep (name "rand") (req "^0.7") (default-features #t) (kind 0)))) (hash "0sf5s7q7q8fkigldzaisqpdfmnlm8caisfs7ivssa7gpy496pyls")))

(define-public crate-objid-0.1 (crate (name "objid") (vers "0.1.2") (deps (list (crate-dep (name "getopts") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.7") (default-features #t) (kind 0)))) (hash "1w04mdl2w8x8b0bbh1jfzlsqnsm5x0rrskcsc4a8k050kaa6pggg")))

(define-public crate-objid-0.1 (crate (name "objid") (vers "0.1.3") (deps (list (crate-dep (name "getopts") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.7") (default-features #t) (kind 0)))) (hash "17wlw96rwz3hsx15zh3nc4d5d0s73ga3gh7r7rnwgly5l53n3hsz")))

(define-public crate-objid-0.1 (crate (name "objid") (vers "0.1.4") (deps (list (crate-dep (name "getopts") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.7") (default-features #t) (kind 0)))) (hash "1b013nhb6a7jjn3nld1wzglhdkrcwzjq1zc2237pd7rznqkw577q")))

(define-public crate-objid-0.1 (crate (name "objid") (vers "0.1.5") (deps (list (crate-dep (name "getopts") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.7") (default-features #t) (kind 0)))) (hash "0faygp7b260x98lc7mxwpzcrvyid4hrww3f8xs230b1zbc2ynklk")))

(define-public crate-objid-0.1 (crate (name "objid") (vers "0.1.9") (deps (list (crate-dep (name "getopts") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8") (default-features #t) (kind 0)))) (hash "0bzdbhpnr3z9r91ifaygw9k74r8v13d6ycrg13ig8v0yw1zd9lza")))

(define-public crate-objid-0.1 (crate (name "objid") (vers "0.1.10") (deps (list (crate-dep (name "getopts") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8") (default-features #t) (kind 0)))) (hash "1q7jdkm2dq1pcckwi7v8izkls7gfxcsc80dxh3myn7100dv4dprc")))

(define-public crate-objid-0.1 (crate (name "objid") (vers "0.1.11") (deps (list (crate-dep (name "getopts") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.4") (default-features #t) (kind 0)))) (hash "10wga8zp6w1mnfphqh9flj42jinchcyblpdfgb5yk2ixn7k0ih6v")))

