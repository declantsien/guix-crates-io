(define-module (crates-io ob ak) #:use-module (crates-io))

(define-public crate-obake-1 (crate (name "obake") (vers "1.0.0") (deps (list (crate-dep (name "obake_macros") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "trybuild") (req "^1.0") (default-features #t) (kind 2)))) (hash "0z3d4n3i3lmfm50sx2w1dh7bdq6rdxpyxdqy2z4h7bsb4xb9ff8x")))

(define-public crate-obake-1 (crate (name "obake") (vers "1.0.1") (deps (list (crate-dep (name "obake_macros") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "trybuild") (req "^1.0") (default-features #t) (kind 2)))) (hash "0rxlqqwccc5nnjpdiq2dlrws6bmz2qx00405lldzr5rf2xx9z33d")))

(define-public crate-obake-1 (crate (name "obake") (vers "1.0.2") (deps (list (crate-dep (name "obake_macros") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "trybuild") (req "^1.0") (default-features #t) (kind 2)))) (hash "0am0xhw005zxmq9l887nywvl616hgkif9km9rxbwbm9wnfs5vx1s")))

(define-public crate-obake-1 (crate (name "obake") (vers "1.0.3") (deps (list (crate-dep (name "obake_macros") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "trybuild") (req "^1.0") (default-features #t) (kind 2)))) (hash "0bm4f9bzswhrn7lnv8akagxnjrs8k1sjrrgvx0bv2k6634x9ivjq")))

(define-public crate-obake-1 (crate (name "obake") (vers "1.0.4") (deps (list (crate-dep (name "obake_macros") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "obake_macros") (req "^1.0") (features (quote ("serde"))) (default-features #t) (kind 2)) (crate-dep (name "trybuild") (req "^1.0") (default-features #t) (kind 2)))) (hash "1z99grj8vifapwpy56nz3687q02w59mj8ay7613alxbi8dj698pd") (features (quote (("serde" "obake_macros/serde") ("default"))))))

(define-public crate-obake-1 (crate (name "obake") (vers "1.0.5") (deps (list (crate-dep (name "obake_macros") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "obake_macros") (req "^1.0") (features (quote ("serde"))) (default-features #t) (kind 2)) (crate-dep (name "trybuild") (req "^1.0") (default-features #t) (kind 2)))) (hash "06q9vr65x0ghb3higzyxq831d4k5d1rqnri3mby3nwg5bd7x65n0") (features (quote (("serde" "obake_macros/serde") ("default"))))))

(define-public crate-obake_macros-1 (crate (name "obake_macros") (vers "1.0.0") (deps (list (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "semver") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "02qs23gcfsl8qj8i1sj72i7gvc0r884s77z7c7pz0ykbm9mh5q90")))

(define-public crate-obake_macros-1 (crate (name "obake_macros") (vers "1.0.1") (deps (list (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "semver") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "03y6affgrfr33sznha208a5ya0qavv1w1jd8c3ncrm4zbl5fq83f")))

(define-public crate-obake_macros-1 (crate (name "obake_macros") (vers "1.0.2") (deps (list (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "semver") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "1w6p88kazvnppwz3zklkpwrbsz5wx88wn1by7z17crk2gcf755q2")))

(define-public crate-obake_macros-1 (crate (name "obake_macros") (vers "1.0.3") (deps (list (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "semver") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "1nvm6492r1fc8vr8kadw2pj57mxc5zjlhdxgi59dbg0wlxc4016l")))

(define-public crate-obake_macros-1 (crate (name "obake_macros") (vers "1.0.4") (deps (list (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "semver") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "12wizpg6ywgdlv7p50gas516gbs6qxm8q1k3704dn20d34an36rl") (features (quote (("serde") ("default"))))))

(define-public crate-obake_macros-1 (crate (name "obake_macros") (vers "1.0.5") (deps (list (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "semver") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "1p67gki4jjb2zv541r3k1yssz7lvg42rrxi9vjgpm1hvn276n1a7") (features (quote (("serde") ("default"))))))

