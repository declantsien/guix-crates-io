(define-module (crates-io jk -t) #:use-module (crates-io))

(define-public crate-jk-tui-base-0.0.1 (crate (name "jk-tui-base") (vers "0.0.1") (deps (list (crate-dep (name "crossterm") (req "^0.26.1") (features (quote ("event-stream" "serde" "futures-core"))) (default-features #t) (kind 0)) (crate-dep (name "futures") (req "^0.3.28") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.29.1") (features (quote ("full"))) (default-features #t) (kind 0)) (crate-dep (name "tui") (req "^0.19.0") (features (quote ("serde"))) (default-features #t) (kind 0)))) (hash "0xxns7xqphb4dizc7fwmna4j018fzzfmrh77l2ld1xkd9vq1hihh")))

