(define-module (crates-io jk l-) #:use-module (crates-io))

(define-public crate-jkl-lang-0.1 (crate (name "jkl-lang") (vers "0.1.5") (deps (list (crate-dep (name "num") (req "^0.4.1") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)))) (hash "0fylyb2k8wcp1lh6f2d7f542xbfmhlp1a3ripjr9wa81l74fpfnf")))

(define-public crate-jkl-lang-0.1 (crate (name "jkl-lang") (vers "0.1.51") (deps (list (crate-dep (name "num") (req "^0.4.1") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)))) (hash "194vnz0vh9syggy9rxnas25dwclqhwzbmcx1frnizzg804rkn543")))

