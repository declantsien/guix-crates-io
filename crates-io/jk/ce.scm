(define-module (crates-io jk ce) #:use-module (crates-io))

(define-public crate-jkcenum-0.2 (crate (name "jkcenum") (vers "0.2.0") (deps (list (crate-dep (name "jkcenum_derive") (req "^0.2.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "jkcenum_derive") (req "^0.2.0") (default-features #t) (kind 2)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 2)) (crate-dep (name "thiserror") (req "^1.0") (default-features #t) (kind 0)))) (hash "0kahjpgx13l31n1s13f4ybcqh9693d023kx3agscb5mb5221ka2f") (features (quote (("derive" "jkcenum_derive")))) (rust-version "1.73")))

(define-public crate-jkcenum-0.2 (crate (name "jkcenum") (vers "0.2.1") (deps (list (crate-dep (name "jkcenum_derive") (req "^0.2.1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "jkcenum_derive") (req "^0.2.1") (default-features #t) (kind 2)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 2)) (crate-dep (name "thiserror") (req "^1.0") (default-features #t) (kind 0)))) (hash "1mzc8qb3xkkh431khg6ivh606dmfpznichbiqzi5xclmd67gm56a") (features (quote (("derive" "jkcenum_derive"))))))

(define-public crate-jkcenum_derive-0.2 (crate (name "jkcenum_derive") (vers "0.2.0") (deps (list (crate-dep (name "virtue") (req "^0.0") (default-features #t) (kind 0)))) (hash "03f1fhxvb5y68kqknd3i7abn18b8lsw7pbj0plwqjy05a1dcba1a") (rust-version "1.73")))

(define-public crate-jkcenum_derive-0.2 (crate (name "jkcenum_derive") (vers "0.2.1") (deps (list (crate-dep (name "virtue") (req "^0.0") (default-features #t) (kind 0)))) (hash "0hk4f2xwxw7r2lyq8db487r4s9d7zkm3sy72yvl2zgcyllgkbpi6")))

