(define-module (crates-io jd ef) #:use-module (crates-io))

(define-public crate-jdefault_derive-0.1 (crate (name "jdefault_derive") (vers "0.1.0") (deps (list (crate-dep (name "virtue") (req "^0.0") (default-features #t) (kind 0)))) (hash "17xmkzsfv633qxgmg0gld429w7ag08sc8da1mgjwz3rh4x5jwx9a")))

(define-public crate-jdefault_derive-0.2 (crate (name "jdefault_derive") (vers "0.2.0") (deps (list (crate-dep (name "virtue") (req "^0.0") (default-features #t) (kind 0)))) (hash "1w41m290q21dpz80ngz0jrhwjiv5pwkydzrdl6xvfkw11cixnhwg")))

(define-public crate-jdefault_derive-0.3 (crate (name "jdefault_derive") (vers "0.3.0") (deps (list (crate-dep (name "jppe") (req "^0") (features (quote ("derive"))) (default-features #t) (kind 2)) (crate-dep (name "virtue") (req "^0.0") (default-features #t) (kind 0)))) (hash "0806i81w4mzb4ibv83j0d0sr1477j5h20p5hi3jgkd45xhavcl2w")))

(define-public crate-jdefault_derive-0.3 (crate (name "jdefault_derive") (vers "0.3.1") (deps (list (crate-dep (name "jppe") (req "^0") (features (quote ("derive"))) (default-features #t) (kind 2)) (crate-dep (name "virtue") (req "^0.0") (default-features #t) (kind 0)))) (hash "123y65gx9l04qx4i27qrkys6sjbpcmbzh7cv53am7a4h7k508p5f")))

