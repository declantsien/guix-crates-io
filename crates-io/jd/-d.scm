(define-module (crates-io jd -d) #:use-module (crates-io))

(define-public crate-jd-decrypter-0.1 (crate (name "jd-decrypter") (vers "0.1.1") (deps (list (crate-dep (name "error-chain") (req "^0.11.0") (default-features #t) (kind 0)) (crate-dep (name "rust-crypto") (req "^0.2.36") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.76") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0.76") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.26") (default-features #t) (kind 0)))) (hash "0p858akdzl1pb0nk4n9pwc2ddf2wvs8c3jhxgj2h48ql4r8kywnp")))

