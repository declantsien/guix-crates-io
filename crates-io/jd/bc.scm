(define-module (crates-io jd bc) #:use-module (crates-io))

(define-public crate-jdbc-0.0.0 (crate (name "jdbc") (vers "0.0.0") (deps (list (crate-dep (name "jni") (req "^0.21.1") (features (quote ("invocation"))) (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.20") (default-features #t) (kind 0)))) (hash "009zhb95l05k03v7gfpy1i434b2dbb34wanxbsl35zwcgzi6vhzp")))

(define-public crate-jdbc-0.1 (crate (name "jdbc") (vers "0.1.0") (deps (list (crate-dep (name "chrono") (req "^0.4.26") (default-features #t) (kind 0)) (crate-dep (name "chrono") (req "^0.4.26") (default-features #t) (kind 2)) (crate-dep (name "jni") (req "^0.21.1") (features (quote ("invocation"))) (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 2)) (crate-dep (name "log") (req "^0.4.20") (default-features #t) (kind 0)))) (hash "12rw5ldkc3f6n1ikkdk6dapnl9x8b8kzmdys9ddvlidcvg4k6ry5") (features (quote (("default") ("chrono"))))))

