(define-module (crates-io bp ht) #:use-module (crates-io))

(define-public crate-bpht-1 (crate (name "bpht") (vers "1.0.0") (deps (list (crate-dep (name "bincode") (req "^1.2.1") (default-features #t) (kind 0)) (crate-dep (name "color-backtrace") (req "^0.2") (default-features #t) (kind 2)) (crate-dep (name "itertools") (req "^0.8.1") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.7.2") (default-features #t) (kind 2)) (crate-dep (name "rand_distr") (req "^0.2.2") (default-features #t) (kind 2)) (crate-dep (name "serde") (req "^1.0.104") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "1xmwm699xknpzw5rw9bynx07n4wsv2hyf5z06rabifshhj14wcwi")))

