(define-module (crates-io bp ci) #:use-module (crates-io))

(define-public crate-bpci-0.1 (crate (name "bpci") (vers "0.1.0-alpha.1") (deps (list (crate-dep (name "approx") (req "^0.5.1") (default-features #t) (kind 2)) (crate-dep (name "num-traits") (req "^0.2.15") (default-features #t) (kind 0)))) (hash "10bwdk75i1kml63ril0rm9ip49b4sdf5yg2m21vh6ahwrhd369km") (features (quote (("default"))))))

(define-public crate-bpci-0.1 (crate (name "bpci") (vers "0.1.0-alpha.2") (deps (list (crate-dep (name "approx") (req "^0.5.1") (default-features #t) (kind 2)) (crate-dep (name "num-traits") (req "^0.2.15") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.31") (default-features #t) (kind 0)))) (hash "0a42zxyczyvhh14fhcy9iin84axnnccnhpay6kvw0hyikjkkg0k5") (features (quote (("default"))))))

(define-public crate-bpci-0.1 (crate (name "bpci") (vers "0.1.0-beta.1") (deps (list (crate-dep (name "approx") (req "^0.5.1") (default-features #t) (kind 2)) (crate-dep (name "num-traits") (req "^0.2.15") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.31") (default-features #t) (kind 0)))) (hash "16xs5fq20sh1p6j3nqqslhqxwqpqar43wizwlcvnca450glldalz") (features (quote (("default"))))))

(define-public crate-bpci-0.1 (crate (name "bpci") (vers "0.1.0-beta.2") (deps (list (crate-dep (name "approx") (req "^0.5.1") (default-features #t) (kind 2)) (crate-dep (name "num-traits") (req "^0.2.15") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.31") (default-features #t) (kind 0)))) (hash "1fmhzkayqm70c7mxa4dfkiw1bdvbvr57z6ylfwxnnsigkqivs83h") (features (quote (("default"))))))

(define-public crate-bpci-0.1 (crate (name "bpci") (vers "0.1.0-beta.3") (deps (list (crate-dep (name "approx") (req "^0.5.1") (default-features #t) (kind 2)) (crate-dep (name "num-traits") (req "^0.2.15") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.31") (default-features #t) (kind 0)))) (hash "05szd0q66flgbji1mhc407s78bl93k1a7r3xckcpdh5bwfp72da5") (features (quote (("default"))))))

(define-public crate-bpci-0.1 (crate (name "bpci") (vers "0.1.0-beta.4") (deps (list (crate-dep (name "approx") (req "^0.5.1") (default-features #t) (kind 2)) (crate-dep (name "num-traits") (req "^0.2.15") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.31") (default-features #t) (kind 0)))) (hash "0dlc84hgx4yrrilzywqvdb6898kdc9wq4s2ji7jq2mjz35jql1xn") (features (quote (("default"))))))

(define-public crate-bpci-0.1 (crate (name "bpci") (vers "0.1.0-beta.5") (deps (list (crate-dep (name "approx") (req "^0.5.1") (default-features #t) (kind 2)) (crate-dep (name "num-traits") (req "^0.2.15") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.31") (default-features #t) (kind 0)))) (hash "0ls5vgvrkib272vx8r5i09dbd0d0j3xa2hki95xn861m886mrcmc") (features (quote (("default"))))))

(define-public crate-bpci-0.1 (crate (name "bpci") (vers "0.1.0-beta.6") (deps (list (crate-dep (name "approx") (req "^0.5.1") (default-features #t) (kind 2)) (crate-dep (name "num-traits") (req "^0.2.15") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.31") (default-features #t) (kind 0)))) (hash "12f194s7ycmjkkjk6jdziw2hxhsq44d74knjfjnx310wbicbc0ic") (features (quote (("default"))))))

(define-public crate-bpci-0.1 (crate (name "bpci") (vers "0.1.0-beta.7") (deps (list (crate-dep (name "approx") (req "^0.5.1") (default-features #t) (kind 2)) (crate-dep (name "num-traits") (req "^0.2.15") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.31") (default-features #t) (kind 0)))) (hash "1gjc66ic2788djzr9940kjh3jqr3b03jrg2ypc7818j0z56z02kz") (features (quote (("default"))))))

(define-public crate-bpci-0.1 (crate (name "bpci") (vers "0.1.0") (deps (list (crate-dep (name "approx") (req "^0.5.1") (default-features #t) (kind 2)) (crate-dep (name "num-traits") (req "^0.2.15") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.31") (default-features #t) (kind 0)))) (hash "19q6wymcbqwwma38glgapz4glbvkfq6wkfajy7mhlpzhpfp3yb2m") (features (quote (("default"))))))

