(define-module (crates-io bp g2) #:use-module (crates-io))

(define-public crate-bpg2hevc-0.1 (crate (name "bpg2hevc") (vers "0.1.0") (deps (list (crate-dep (name "anyhow") (req "^1.0.71") (default-features #t) (kind 0)) (crate-dep (name "binrw") (req "^0.11.1") (default-features #t) (kind 0)) (crate-dep (name "bitstream-io") (req "^1.6.0") (default-features #t) (kind 0)) (crate-dep (name "modular-bitfield") (req "^0.11.2") (default-features #t) (kind 0)) (crate-dep (name "xflags") (req "^0.3.1") (default-features #t) (kind 0)))) (hash "17hzdcm0i73qa0fbp9zp2swdy3r9agidiass4c1idgv55166z6y0")))

