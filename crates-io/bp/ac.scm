(define-module (crates-io bp ac) #:use-module (crates-io))

(define-public crate-bpack-0.1 (crate (name "bpack") (vers "0.1.0") (deps (list (crate-dep (name "rand") (req "^0.7.3") (default-features #t) (kind 2)) (crate-dep (name "serde") (req "^1.0.117") (features (quote ("derive"))) (default-features #t) (kind 2)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 2)))) (hash "06ky3qb6h2fifgff0r77p9m63bp6sqjgyxzw78q7wbxd1dk8kkn0")))

(define-public crate-bpack-0.1 (crate (name "bpack") (vers "0.1.1") (deps (list (crate-dep (name "rand") (req "^0.7.3") (default-features #t) (kind 2)) (crate-dep (name "serde") (req "^1.0.117") (features (quote ("derive"))) (default-features #t) (kind 2)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 2)))) (hash "0qg6ifxfv3zqngazwzzqklzwb35dp37in60752y9zbxq6a245889")))

