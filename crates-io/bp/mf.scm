(define-module (crates-io bp mf) #:use-module (crates-io))

(define-public crate-bpmf_py-0.1 (crate (name "bpmf_py") (vers "0.1.0") (deps (list (crate-dep (name "once_cell") (req "^1.19.0") (default-features #t) (kind 0)) (crate-dep (name "sugars") (req "^3.0.1") (default-features #t) (kind 0)))) (hash "11z3br0wqf89r0bnbhn4axqhf5ggyj1344jahbn3s4dsak919bna")))

