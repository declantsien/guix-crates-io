(define-module (crates-io bp ar) #:use-module (crates-io))

(define-public crate-bparse-0.1 (crate (name "bparse") (vers "0.1.0") (deps (list (crate-dep (name "bstr") (req "^1.8.0") (default-features #t) (kind 0)))) (hash "041710r4a7qqq4yaycny026kp895mr4i4zrkwz6y6rym2nxxv3d1")))

(define-public crate-bparse-0.2 (crate (name "bparse") (vers "0.2.0") (deps (list (crate-dep (name "bstr") (req "^1.8.0") (default-features #t) (kind 0)))) (hash "00zahvsnqd8gz6ybfp15c24a00bn4agxw1mhgb8vj77zff6szqgb")))

(define-public crate-bparse-0.3 (crate (name "bparse") (vers "0.3.0") (deps (list (crate-dep (name "bstr") (req "^1.8.0") (default-features #t) (kind 0)))) (hash "1pbxwir7lh9m92n0dj2c2nz97fjhg394yy4qy2y4hgkc8ycn9mzx")))

(define-public crate-bparse-0.4 (crate (name "bparse") (vers "0.4.0") (hash "05brlgj4x01z6cz11gl6ghr02xs257wyaa1n6d5wdksdxygjf4al")))

(define-public crate-bparse-0.5 (crate (name "bparse") (vers "0.5.0") (hash "1iakd82cqvqzqwb73835j3mq7w9xig270r04i4hy584falx0rwhw")))

(define-public crate-bparse-0.6 (crate (name "bparse") (vers "0.6.0") (hash "1dpwbrccdrn0g945v8qjqcf7ikgpqba8yvzx0nf4d2qyrwcblm22")))

(define-public crate-bparse-0.7 (crate (name "bparse") (vers "0.7.0") (hash "1g0j7bai540vzafh0m96xb3n6cwxcc46g66w9nw9k6xswjv73i3w")))

(define-public crate-bparse-0.8 (crate (name "bparse") (vers "0.8.0") (hash "1fdfvjryaw05r5jdid3k5qj2wap139pmcsz7p6z6sjbgmxg5qa09")))

(define-public crate-bparse-0.9 (crate (name "bparse") (vers "0.9.0") (hash "0sp598h9wd28caskbrq9827ikir45l6jbahxky1h2xd484q1zvl9")))

(define-public crate-bparse-0.10 (crate (name "bparse") (vers "0.10.0") (hash "1ay722jkrn9dxpznf13yfy5pcgl9rsqh6gir0m5k0cw965xz8rad")))

(define-public crate-bparse-0.11 (crate (name "bparse") (vers "0.11.0") (hash "03kqcvi8418484nk7vx1pm238yq6klgq0jbhrqlxy6svcflmc65v")))

(define-public crate-bparse-0.12 (crate (name "bparse") (vers "0.12.0") (hash "0xlzcakxh8bdnr5ah464r4xlxlp99dcyx0k0w8d45828fxbrh7n6")))

(define-public crate-bparse-0.13 (crate (name "bparse") (vers "0.13.0") (hash "13sh4gjik0c05dwns87v648b3m776w3ggns932v5wifbkq0cprhp")))

