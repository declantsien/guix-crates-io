(define-module (crates-io bp fj) #:use-module (crates-io))

(define-public crate-bpfjit-0.1 (crate (name "bpfjit") (vers "0.1.0") (deps (list (crate-dep (name "gcc") (req "^0.3") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "04kzl9vfabpkjgxsd9ynpbwjxdjyw4cik1sn2y0y6dl7ls1kr43j")))

(define-public crate-bpfjit-0.1 (crate (name "bpfjit") (vers "0.1.1") (deps (list (crate-dep (name "gcc") (req "^0.3") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "1w2achy0blgbfinq96y5r33547y1dc4hhr9lgbxwsz3r2g39w8cm")))

(define-public crate-bpfjit-sys-1 (crate (name "bpfjit-sys") (vers "1.0.0") (deps (list (crate-dep (name "cc") (req "^1.0") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "1g980w1yzxipzpfxgs3pgfdzc9x2hmr6na7irjsxal1wrrlqmr2a")))

(define-public crate-bpfjit-sys-1 (crate (name "bpfjit-sys") (vers "1.0.1") (deps (list (crate-dep (name "cc") (req "^1.0") (default-features #t) (kind 1)) (crate-dep (name "lazy_static") (req "^1.3") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "0ibivdlbj7qgiar2ycqb3dn47rk6zz0ih7yhzcf3a31nxyv975nm")))

(define-public crate-bpfjit-sys-1 (crate (name "bpfjit-sys") (vers "1.0.2") (deps (list (crate-dep (name "cc") (req "^1.0") (default-features #t) (kind 1)) (crate-dep (name "lazy_static") (req "^1.3") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "0xcinaw9xxdzm7sy266m3dzjc298sxy4fw1pf53rh5921gwz4lwd")))

(define-public crate-bpfjit-sys-1 (crate (name "bpfjit-sys") (vers "1.1.0") (deps (list (crate-dep (name "cc") (req "^1.0") (default-features #t) (kind 1)) (crate-dep (name "lazy_static") (req "^1.3") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "1vscgnw5kdmcivm3vyvgp8ffcminhgd70xxvn5a2p6bzgkby69cs")))

(define-public crate-bpfjit-sys-2 (crate (name "bpfjit-sys") (vers "2.0.0") (deps (list (crate-dep (name "cc") (req "^1.0") (default-features #t) (kind 1)) (crate-dep (name "lazy_static") (req "^1.4") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "04gpp6ic5r673l0ayqyqr6d0f4rnxd6xjh2v2f6fsjvc3rc4anz5")))

(define-public crate-bpfjit-sys-2 (crate (name "bpfjit-sys") (vers "2.1.0") (deps (list (crate-dep (name "cc") (req "^1.0") (default-features #t) (kind 1)) (crate-dep (name "lazy_static") (req "^1.4") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "0n97lxf7a4h2gj3wk4dzc3i4jsdid3las2v57b19rgps6qm1lwsb")))

