(define-module (crates-io bp fa) #:use-module (crates-io))

(define-public crate-bpfaas-0.0.1 (crate (name "bpfaas") (vers "0.0.1") (hash "1jilb27r0vgy8gajnih51q7b1a6h4558v3x115i2v581j1mygwdg")))

(define-public crate-bpfasm-1 (crate (name "bpfasm") (vers "1.0.0-beta1") (deps (list (crate-dep (name "pest") (req "~2") (default-features #t) (kind 0)) (crate-dep (name "pest_generator") (req "~2") (default-features #t) (kind 1)) (crate-dep (name "quote") (req "~1") (default-features #t) (kind 1)) (crate-dep (name "thiserror") (req "~1") (default-features #t) (kind 0)))) (hash "0r25ckwhgggq6frlcnzys313mswpvxsrzf6ynfgschp3w9mvdq27")))

(define-public crate-bpfasm-1 (crate (name "bpfasm") (vers "1.0.0") (deps (list (crate-dep (name "pest") (req "~2") (default-features #t) (kind 0)) (crate-dep (name "pest_generator") (req "~2") (default-features #t) (kind 1)) (crate-dep (name "quote") (req "~1") (default-features #t) (kind 1)) (crate-dep (name "thiserror") (req "~1") (default-features #t) (kind 0)))) (hash "0igfmw5hlrdlqm0ic72zx054sqxyv4kpnm1ykgp3f5nprafx610c")))

