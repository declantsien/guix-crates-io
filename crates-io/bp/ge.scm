(define-module (crates-io bp ge) #:use-module (crates-io))

(define-public crate-bpgen-0.1 (crate (name "bpgen") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^4.0.26") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "0lrhafzkjmqmw7qjf7z4048g93xddflgswxdsppisdr5gpmy6cdv")))

