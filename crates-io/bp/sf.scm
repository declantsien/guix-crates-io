(define-module (crates-io bp sf) #:use-module (crates-io))

(define-public crate-bpsfuck-0.1 (crate (name "bpsfuck") (vers "0.1.0") (deps (list (crate-dep (name "chumsky") (req "^0.6") (default-features #t) (kind 0)))) (hash "0hk5h7v0nvsgqyh59r709ww7knhpl57jm956zwvxa39jf4d7hkhx")))

(define-public crate-bpsfuck-0.1 (crate (name "bpsfuck") (vers "0.1.1") (deps (list (crate-dep (name "chumsky") (req "^0.6") (default-features #t) (kind 0)))) (hash "12yp80qi9gbfaxbcn2n6c59qcgj3r73vg68ka2fn8l9qcnvfwns6")))

(define-public crate-bpsfuck-0.1 (crate (name "bpsfuck") (vers "0.1.2") (deps (list (crate-dep (name "chumsky") (req "^0.6") (default-features #t) (kind 0)))) (hash "0ag1xgw5cw2vknciwsmy04hsij1pm1l0i0l9khlgi53cdik744gb")))

