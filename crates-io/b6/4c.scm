(define-module (crates-io b6 #{4c}#) #:use-module (crates-io))

(define-public crate-b64ct-0.1 (crate (name "b64ct") (vers "0.1.0") (hash "0cj0wgbbis9hnklmrsdsiiky82x3n2a5ajiw7bqd6pridp1sv4x2") (features (quote (("std" "alloc") ("alloc")))) (yanked #t)))

(define-public crate-b64ct-0.2 (crate (name "b64ct") (vers "0.2.0") (hash "0vknwvmqfm9cn4d36gwg30dnkb1dpipyrgnrq0vmz4p9z6rz8kaj") (features (quote (("std" "alloc") ("alloc")))) (yanked #t)))

