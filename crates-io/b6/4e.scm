(define-module (crates-io b6 #{4e}#) #:use-module (crates-io))

(define-public crate-b64ed-0.1 (crate (name "b64ed") (vers "0.1.0") (deps (list (crate-dep (name "base64") (req "^0.21.2") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^4.3.0") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "1fv4shgyk9cz9s9cpjhnx030g5k3bv00dcrzl2ra2blmypkw8b8k")))

