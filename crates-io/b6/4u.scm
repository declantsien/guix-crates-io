(define-module (crates-io b6 #{4u}#) #:use-module (crates-io))

(define-public crate-b64url-0.1 (crate (name "b64url") (vers "0.1.0") (deps (list (crate-dep (name "base64") (req "^0.13.0") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.3.13") (default-features #t) (kind 0)))) (hash "1jpissxw6nw1bzk6kfb83acy2y76jwsgc80gvrnalypqddh0ia3g")))

(define-public crate-b64url-0.1 (crate (name "b64url") (vers "0.1.1") (deps (list (crate-dep (name "base64") (req "^0.13.0") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.3.13") (default-features #t) (kind 0)))) (hash "08n6hkm9fvrncl6i4cvd283yj2i2wzjvxaf79cbb8hbricjvjwp2")))

