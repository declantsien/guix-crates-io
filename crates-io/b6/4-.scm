(define-module (crates-io b6 #{4-}#) #:use-module (crates-io))

(define-public crate-b64-ct-0.1 (crate (name "b64-ct") (vers "0.1.0") (deps (list (crate-dep (name "paste") (req "^0.1") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.6") (default-features #t) (kind 2)))) (hash "13pdz5qid5dm9makjnn8smlq19jxfcrk5z8ff291x28yx5rb31rv") (features (quote (("std") ("nightly") ("default" "std"))))))

(define-public crate-b64-ct-0.1 (crate (name "b64-ct") (vers "0.1.1") (deps (list (crate-dep (name "paste") (req "^0.1") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.6") (default-features #t) (kind 2)))) (hash "1hwd7ahnxbk3lbyy8p070gr870fgjby29q3gxzy8g9nwpzp8s7mc") (features (quote (("std") ("nightly") ("default" "std"))))))

(define-public crate-b64-ct-0.1 (crate (name "b64-ct") (vers "0.1.2") (deps (list (crate-dep (name "paste") (req "^0.1") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.7") (default-features #t) (kind 2)))) (hash "1286fgra0dg9scpn5qhg6wwl05nw0m1yyx0psdnl8d72jmd1w9ij") (features (quote (("std") ("nightly") ("default" "std"))))))

(define-public crate-b64-rs-1 (crate (name "b64-rs") (vers "1.0.0") (hash "0isi389ncgs9ipyf8djk6rqjqgv276nzwsldh6ycz5kdbqh42ggq") (yanked #t)))

(define-public crate-b64-rs-1 (crate (name "b64-rs") (vers "1.0.1") (hash "0vizf9fk4lwh9q824bvxzd85a5x00314lk3pfmv84avacbpay0m9") (yanked #t)))

(define-public crate-b64-rs-1 (crate (name "b64-rs") (vers "1.0.2") (hash "0775zbxh0drj5w257kdv0rxp1clrnm8ba5cmdc350bfl50gxd303")))

(define-public crate-b64-rs-1 (crate (name "b64-rs") (vers "1.0.3") (hash "19xr7k3i67a3lmk11snbsfj09wsnwybjh48h0qix5ln0psmjjyji")))

(define-public crate-b64-url-0.1 (crate (name "b64-url") (vers "0.1.0") (hash "0vli7d0q7k6zwa5cpikpkrh75bm2r396rmm56ni93gr343qxv750")))

(define-public crate-b64-url-0.2 (crate (name "b64-url") (vers "0.2.0") (hash "0lh21r8v3rklsk2sa97jv55k3cxz019ccwlj9z8yslwjlkyizwk5")))

(define-public crate-b64-url-0.3 (crate (name "b64-url") (vers "0.3.0") (deps (list (crate-dep (name "lazy_static") (req "^1.4") (default-features #t) (kind 0)))) (hash "1jwx4c6ck3595pn1khfld4rwmjd77j2wfnjadqmql3w1bkpb2ihw")))

(define-public crate-b64-url-0.4 (crate (name "b64-url") (vers "0.4.0") (deps (list (crate-dep (name "lazy_static") (req "^1.4") (default-features #t) (kind 0)))) (hash "143z0w07x3738fsmjrbjac586g9zwwkrcrqd5lnzw3p3pi03nplj")))

(define-public crate-b64-url-0.5 (crate (name "b64-url") (vers "0.5.0") (deps (list (crate-dep (name "lazy_static") (req "^1.4") (default-features #t) (kind 0)))) (hash "0v6c3ddf9mkpdmzd44rmrqvydn0f2bfx66d089dv12sb1xmjahis")))

