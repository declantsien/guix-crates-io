(define-module (crates-io we b2) #:use-module (crates-io))

(define-public crate-web2app-0.2 (crate (name "web2app") (vers "0.2.5") (deps (list (crate-dep (name "base64") (req "^0.20.0") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^4.1.1") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "home") (req "^0.5.4") (default-features #t) (kind 0)))) (hash "1lqxjih3id0np9n3ss24gybdig78vw4hnkd307g6wp5s58iizc66")))

(define-public crate-web2app-0.2 (crate (name "web2app") (vers "0.2.6") (deps (list (crate-dep (name "base64") (req "^0.20.0") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^4.1.1") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "home") (req "^0.5.4") (default-features #t) (kind 0)))) (hash "0qrrsxj76hcgi78gb1f769xz61n8xbwdaz9pj9w3wlhpjp4kppyn")))

(define-public crate-web2app-0.2 (crate (name "web2app") (vers "0.2.7") (deps (list (crate-dep (name "base64") (req "^0.20.0") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^4.1.1") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "home") (req "^0.5.4") (default-features #t) (kind 0)))) (hash "1m0zbpilyj5sf8c7bbba86gm303jf6xpmpsbyzn6qq2484faiwmc")))

(define-public crate-web2app-0.2 (crate (name "web2app") (vers "0.2.8") (deps (list (crate-dep (name "base64") (req "^0.20.0") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^4.1.1") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "home") (req "^0.5.4") (default-features #t) (kind 0)))) (hash "03qcmz9k5dxisc9qdb5wvsmyfc6z03plch84q6mwj89f3c4rxcy3")))

(define-public crate-web2app-0.2 (crate (name "web2app") (vers "0.2.9") (deps (list (crate-dep (name "base64") (req "^0.20.0") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^4.1.1") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "home") (req "^0.5.4") (default-features #t) (kind 0)))) (hash "1ab8s8fbbqsddwagmgvg0hy8zf9rzgi27491slm9p0lkcilfpmd4")))

(define-public crate-web2app-0.3 (crate (name "web2app") (vers "0.3.0") (deps (list (crate-dep (name "base64") (req "^0.20.0") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^4.1.1") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "home") (req "^0.5.4") (default-features #t) (kind 0)))) (hash "1zyfx0mlh6mzfs60s0z3vj2qmqqqj0p48alw4b7b0pd92zmddgbi") (yanked #t)))

(define-public crate-web2app-0.2 (crate (name "web2app") (vers "0.2.10") (deps (list (crate-dep (name "base64") (req "^0.20.0") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^4.1.1") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "home") (req "^0.5.4") (default-features #t) (kind 0)))) (hash "10sdbh1wpj12ky3710n6dsa4xbq5f6v9i5w6lm61r4b7429i4m1w")))

(define-public crate-web2app-0.2 (crate (name "web2app") (vers "0.2.11") (deps (list (crate-dep (name "base64") (req "^0.20.0") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^4.1.1") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "home") (req "^0.5.4") (default-features #t) (kind 0)))) (hash "0ww69gbawy0inzy7si9m0b6gx9msyd6r9v4860lf4dwh5sbbg8vb")))

(define-public crate-web2app-0.2 (crate (name "web2app") (vers "0.2.12") (deps (list (crate-dep (name "base64") (req "^0.20.0") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^4.1.1") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "home") (req "^0.5.4") (default-features #t) (kind 0)))) (hash "0hpif2fpvzy1yggsp13xnhjiqdxk95393djxznihk257by8d5jxr")))

