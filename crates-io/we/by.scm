(define-module (crates-io we by) #:use-module (crates-io))

(define-public crate-weby-0.1 (crate (name "weby") (vers "0.1.0") (hash "0c8c40iysz673a0mbzp8j2fffr2lqwlp42hims95fb12f972k08f")))

(define-public crate-weby-cli-0.1 (crate (name "weby-cli") (vers "0.1.0") (hash "0dzxyclzpgx01jzqa4qv3ayvhbdfk8d1598yjf05lsgj46a77i8f")))

