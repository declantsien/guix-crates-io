(define-module (crates-io we ir) #:use-module (crates-io))

(define-public crate-weird-0.1 (crate (name "weird") (vers "0.1.0") (hash "0wfvm2mp5mmiv3qqa1zn5mzd29gmh9da4l7pyczlaqpqdyai69qk")))

(define-public crate-weird-0.1 (crate (name "weird") (vers "0.1.1") (hash "0fg60mg8kx3f4ksq4anq1c64h4brwq2z269l2f5qr0d3rhr14m7i")))

(define-public crate-weird-0.1 (crate (name "weird") (vers "0.1.2") (hash "13aj2spr7ksqmw7xm3yw0vdskc3c5kgkdqbfv8v1iw6xyhkqyvp2")))

(define-public crate-weird-0.2 (crate (name "weird") (vers "0.2.0") (deps (list (crate-dep (name "criterion") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "crockford") (req "^1.1.0") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.8.3") (default-features #t) (kind 0)) (crate-dep (name "squirrel-rng") (req "^0.1.1") (default-features #t) (kind 0)))) (hash "0sr3wbcj6fjij07xffsd3x4vx37qvhs903zg06i3djxbj7l8ck0k")))

(define-public crate-weird-data-0.0.0 (crate (name "weird-data") (vers "0.0.0") (hash "15w12vkkjfg94chc3c6c7xgnw8imyfm6pw4kq7mn0n1ma0j6k54r")))

(define-public crate-weird-data-0.0.1 (crate (name "weird-data") (vers "0.0.1") (hash "0ylww1n8h1s85zvjddgrmzvkjmcnrmr2ls0q03mrlll1jvgfd0as")))

(define-public crate-weird-data-0.1 (crate (name "weird-data") (vers "0.1.0") (deps (list (crate-dep (name "fastrand") (req "^2.0.1") (kind 0)))) (hash "015cpvy4i0k2ck080d79w9phnnzr20w4npmn9l7g942x01nj3zlf") (features (quote (("std" "fastrand/std") ("default" "std")))) (rust-version "1.75")))

(define-public crate-weird-data-0.2 (crate (name "weird-data") (vers "0.2.0") (deps (list (crate-dep (name "fastrand") (req "^2.0.1") (kind 0)) (crate-dep (name "paste") (req "^1.0.14") (default-features #t) (kind 0)))) (hash "1wg59lfsq1016g9icnwdcqbr4q59wrjzsixnd7v2a8v56f51j9qj") (features (quote (("std" "fastrand/std") ("default" "std")))) (rust-version "1.75")))

(define-public crate-weirdgrep-1 (crate (name "weirdgrep") (vers "1.0.0") (deps (list (crate-dep (name "clap") (req "^3.2.17") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)))) (hash "1q85x1j3wnk183qgsw5znjzg8j82x2acfgp8aqwzksg3nqg8ar28") (yanked #t)))

(define-public crate-weirdgrep-1 (crate (name "weirdgrep") (vers "1.0.1") (deps (list (crate-dep (name "clap") (req "^3.2.17") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)))) (hash "0nrbq5jnxxpbj04r8nd2lalyzfcjmpq598hybhr16sn844p84d2m")))

(define-public crate-weirdgrep-1 (crate (name "weirdgrep") (vers "1.0.2") (deps (list (crate-dep (name "clap") (req "^3.2.17") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)))) (hash "16nr2vs98j429wyrnw3x6mppgphw8gnxfqa0aa6d97xv8c7fnczd")))

(define-public crate-weirdgrep-1 (crate (name "weirdgrep") (vers "1.0.3") (deps (list (crate-dep (name "clap") (req "^3.2.17") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "glob") (req "^0.3.1") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)))) (hash "182rz8wc515lcjw2dw5l2xz6lpnc76nh03z3s6fic2pc6zrwjyj5") (yanked #t)))

(define-public crate-weirdgrep-1 (crate (name "weirdgrep") (vers "1.0.4") (deps (list (crate-dep (name "clap") (req "^3.2.17") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "glob") (req "^0.3.1") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)))) (hash "1yvn9j99c1vgffh9d44jwrb2svs8nliyg7g0zffzynz8gxmnd73h")))

(define-public crate-weirdgrep-1 (crate (name "weirdgrep") (vers "1.0.5") (deps (list (crate-dep (name "clap") (req "^3.2.17") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "glob") (req "^0.3.1") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)))) (hash "1dbawsh9k5a6csbrmhzj2adir56rmgd2fws31f2cq340q8m74pzq")))

(define-public crate-weiroll-0.1 (crate (name "weiroll") (vers "0.1.0") (deps (list (crate-dep (name "bitflags") (req "^2") (default-features #t) (kind 0)) (crate-dep (name "bytes") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "ethers") (req "^2.0.3") (default-features #t) (kind 0)) (crate-dep (name "slotmap") (req "^1.0.6") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.40") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.21") (features (quote ("macros"))) (default-features #t) (kind 2)) (crate-dep (name "tracing") (req "^0.1.37") (default-features #t) (kind 0)))) (hash "0k2c9wbll8dm6igywn765j5vmdcgvly89mrmfjwx667624mwklq9")))

(define-public crate-weiroll-0.1 (crate (name "weiroll") (vers "0.1.1") (deps (list (crate-dep (name "bitflags") (req "^2") (default-features #t) (kind 0)) (crate-dep (name "bytes") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "ethers") (req "^2.0.3") (default-features #t) (kind 0)) (crate-dep (name "slotmap") (req "^1.0.6") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1.0.40") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.21") (features (quote ("macros"))) (default-features #t) (kind 2)) (crate-dep (name "tracing") (req "^0.1.37") (default-features #t) (kind 0)))) (hash "16gyf84w5vpcnyzbbq5gwpmbjj70qjnxsv4ygbwygb40q2v6h7rb")))

