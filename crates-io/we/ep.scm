(define-module (crates-io we ep) #:use-module (crates-io))

(define-public crate-weepingtown-0.1 (crate (name "weepingtown") (vers "0.1.0") (deps (list (crate-dep (name "function_name") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.17") (features (quote ("max_level_trace" "release_max_level_warn"))) (default-features #t) (kind 0)) (crate-dep (name "pretty_env_logger") (req "^0.4.0") (default-features #t) (kind 0)) (crate-dep (name "volmark") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "1awkf9705723nxdhys8llymx75vl2wch86g4x7lxy4bhw8mb2shj")))

