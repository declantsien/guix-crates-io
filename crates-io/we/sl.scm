(define-module (crates-io we sl) #:use-module (crates-io))

(define-public crate-wesley-0.1 (crate (name "wesley") (vers "0.1.0") (deps (list (crate-dep (name "getopts") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "glob") (req "^0.2.11") (default-features #t) (kind 0)) (crate-dep (name "notify") (req "^2.5.5") (default-features #t) (kind 0)))) (hash "159l7d6nv8ycl222i3wgkm1rgbzdkip0qhs5wk60pbqv0f0rzgki")))

