(define-module (crates-io we -c) #:use-module (crates-io))

(define-public crate-we-cdk-0.3 (crate (name "we-cdk") (vers "0.3.0") (deps (list (crate-dep (name "we-contract-proc-macro") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "wevm-core") (req "^0.3.2") (features (quote ("bindings"))) (kind 0)))) (hash "167dykjg8sjv687lnz5g8m4chwy7si8153sa804h40w2049g9x3s")))

(define-public crate-we-cdk-0.3 (crate (name "we-cdk") (vers "0.3.1") (deps (list (crate-dep (name "we-contract-proc-macro") (req "^0.3.1") (default-features #t) (kind 0)) (crate-dep (name "wevm-core") (req "^0.3.2") (features (quote ("bindings"))) (kind 0)))) (hash "0acbx62z0zc2i7rpncsg8vmg4fvyxl2qchj6qm7lls3831y27q5k")))

(define-public crate-we-cdk-0.3 (crate (name "we-cdk") (vers "0.3.2") (deps (list (crate-dep (name "we-contract-proc-macro") (req "^0.3.1") (default-features #t) (kind 0)) (crate-dep (name "wevm-core") (req "^0.3.2") (features (quote ("bindings"))) (kind 0)))) (hash "07j2ngak5h9dpqjffaij8m87svza3v8lwjzcdmsamc5hwza3fzl3")))

(define-public crate-we-contract-proc-macro-0.3 (crate (name "we-contract-proc-macro") (vers "0.3.0") (deps (list (crate-dep (name "proc-macro2") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "06if0slwb4pqsh91fd3s2qfsl4fizxkn3iv6l1pl7y0g19621c2r")))

(define-public crate-we-contract-proc-macro-0.3 (crate (name "we-contract-proc-macro") (vers "0.3.1") (deps (list (crate-dep (name "proc-macro2") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "043jfk2f0ylnr7n2kgriijnlapdlznqkni4yb1chgifpzgf2wmfj")))

(define-public crate-we-core-0.1 (crate (name "we-core") (vers "0.1.0") (hash "133y4az0f314vg85gicvc5g92w5av1k6ra4iygrxs1blbpqpsvn5") (yanked #t)))

(define-public crate-we-core-0.1 (crate (name "we-core") (vers "0.1.1") (hash "129ckrbs31fb48pxq6fi6n1j243b686d3lgy7i7pinqyfvrq51j3") (yanked #t)))

(define-public crate-we-core-0.1 (crate (name "we-core") (vers "0.1.2") (hash "11h2n77pddbf2svs7h8m0whan2fxig6mrxfj3cms8djddqhnx5yp") (yanked #t)))

