(define-module (crates-io we tr) #:use-module (crates-io))

(define-public crate-wetransfer-0.1 (crate (name "wetransfer") (vers "0.1.0") (deps (list (crate-dep (name "mockito") (req "^0.13.0") (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.9.8") (features (quote ("hyper-011"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)))) (hash "1jmx6pjsjr7z4fgz35dc5nq5pfijsrrrwigypajr7qrkf9wybd3k")))

(define-public crate-wetransfer-0.1 (crate (name "wetransfer") (vers "0.1.1") (deps (list (crate-dep (name "mockito") (req "^0.13.0") (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.9.8") (features (quote ("hyper-011"))) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)))) (hash "02xafzwvjyiiyms3spcix3v84h96xsgizn77iyzrbl7gyf30irgd")))

