(define-module (crates-io we ak) #:use-module (crates-io))

(define-public crate-weak-0.0.0 (crate (name "weak") (vers "0.0.0-0.0.0-0.0.0") (hash "1cj6fqi49p0iw6axldh30pzj12rdg6nv9v22h2shvx0rgc6d7kar") (yanked #t)))

(define-public crate-weak-0.0.0 (crate (name "weak") (vers "0.0.0-dev.0") (deps (list (crate-dep (name "num-derive") (req ">=0") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req ">=0") (default-features #t) (kind 0)) (crate-dep (name "weak-build-macros") (req "^0.0.0-dev.0") (default-features #t) (kind 0)))) (hash "17a0133m9mdvli5v0x1nfwfyrfn2nc7jipwqgfkhs42vv8qcaybq") (yanked #t)))

(define-public crate-weak-0.0.0 (crate (name "weak") (vers "0.0.0-empty-test") (hash "1gyig7gn6a21vy42n3mgdbawzg3ryns0j71crys0r2p6i73c6s63") (yanked #t)))

(define-public crate-weak-0.0.0 (crate (name "weak") (vers "0.0.0-empty-test2") (hash "1d5lwfv0jyrkhv7hca2dp46dcq1b1zbj0yqbkmiafas2950cnmgw") (yanked #t)))

(define-public crate-weak-0.0.0 (crate (name "weak") (vers "0.0.0-empty-test3") (hash "0372g2p22z7dpbwvhf1y3bqb3hvy331x7qpj991m0q9n58z3p4bj") (yanked #t)))

(define-public crate-weak-0.0.0 (crate (name "weak") (vers "0.0.0--") (hash "1chw0psymb6zfnpipshgljmvmgxh5nwk2pzd7kxa60hjp51xcbz6") (yanked #t)))

(define-public crate-weak-alloc-0.1 (crate (name "weak-alloc") (vers "0.1.0") (deps (list (crate-dep (name "once_cell") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "weak-list2") (req "^0.1") (default-features #t) (kind 0)))) (hash "0k8n50vdqr3i5dn06bja5z1j99bkhly93lijss84zrk4lqzqkf3v")))

(define-public crate-weak-build-macros-0.0.0 (crate (name "weak-build-macros") (vers "0.0.0-dev.0") (hash "1fndzdxwfig8za63c6sy9b1mqzq2vwkvjwddyr4kaxb9cqmaj5jl") (yanked #t)))

(define-public crate-weak-build-macros-0.0.0 (crate (name "weak-build-macros") (vers "0.0.0--") (hash "1g617z6rki0j7zx4f0ji9mk0q6n12nr49j8qkznblak1zz1lgqga") (yanked #t)))

(define-public crate-weak-list2-0.1 (crate (name "weak-list2") (vers "0.1.0") (deps (list (crate-dep (name "once_cell") (req "^1.13") (default-features #t) (kind 0)))) (hash "0d4yz1zp0ypr50gqf7ndjlzrqnwfn00h4rb8kx7rkxdmd236y09w")))

(define-public crate-weak-self-1 (crate (name "weak-self") (vers "1.0.0") (hash "0zjxq3y9m9zgvixrfsx74iyqcc6rfrvjfhmsdjb4v49w1m9b7x3l")))

(define-public crate-weak-self-1 (crate (name "weak-self") (vers "1.0.1") (hash "1qv3mbhwy3rqdi2y8sk2d8y6nn1i98carzkfy6qhkf57ssyzacfp")))

(define-public crate-weak-self-1 (crate (name "weak-self") (vers "1.0.2") (hash "1q0dhdxam30l3jvrkk6lgwrrihkg6bh8xhpp19a7vdj172l4zpqs")))

(define-public crate-weak-table-0.1 (crate (name "weak-table") (vers "0.1.0") (deps (list (crate-dep (name "quickcheck") (req "^0.6") (default-features #t) (kind 2)))) (hash "0v32xba8982gsdjnjz7f884ksbvza6awqs2vyv7vhfbda9im2spy")))

(define-public crate-weak-table-0.1 (crate (name "weak-table") (vers "0.1.1") (deps (list (crate-dep (name "quickcheck") (req "^0.6") (default-features #t) (kind 2)))) (hash "1yzng8vlzjlbnha99ln8c07ikzdvlg5w52y1wbb07cicsgpyq068")))

(define-public crate-weak-table-0.1 (crate (name "weak-table") (vers "0.1.2") (deps (list (crate-dep (name "quickcheck") (req "^0.6") (default-features #t) (kind 2)))) (hash "00312nwcr22g0zavr2z4jinms7nj09b5yk4rgwmzvryrq8hkwspm")))

(define-public crate-weak-table-0.1 (crate (name "weak-table") (vers "0.1.3") (deps (list (crate-dep (name "quickcheck") (req "^0.6") (default-features #t) (kind 2)))) (hash "05hk493yn2g2wv68kahnnvwc47mw5cl9qzmv48q49ln26yn8ai85")))

(define-public crate-weak-table-0.2 (crate (name "weak-table") (vers "0.2.0") (deps (list (crate-dep (name "quickcheck") (req "^0.6") (default-features #t) (kind 2)))) (hash "141s2hbxr68jqmkk9frhgddbcz0cyx9v2j6kb1wzzzk2zkgws2f1")))

(define-public crate-weak-table-0.2 (crate (name "weak-table") (vers "0.2.1") (deps (list (crate-dep (name "quickcheck") (req "^0.6") (default-features #t) (kind 2)))) (hash "01d59yxjxclcybf6kfs36jhhjdkfri2yifvb1l0kcz87zccapp1n")))

(define-public crate-weak-table-0.2 (crate (name "weak-table") (vers "0.2.2") (deps (list (crate-dep (name "quickcheck") (req "^0.6") (default-features #t) (kind 2)))) (hash "1vypyrm3adk6q6abr8jg0qhljmqa586xp3c209sfkkg6vsb7asq0")))

(define-public crate-weak-table-0.2 (crate (name "weak-table") (vers "0.2.3") (deps (list (crate-dep (name "quickcheck") (req "^0.6") (default-features #t) (kind 2)))) (hash "13c9v7nkwfh7s8qfy4s5ljyp24hqzxl9dhzkqrb2m1ac4jxn4n3a")))

(define-public crate-weak-table-0.3 (crate (name "weak-table") (vers "0.3.0") (deps (list (crate-dep (name "quickcheck") (req "^0.9") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.7.2") (default-features #t) (kind 2)))) (hash "00fx45xiprhps15vjdlrx9jk8gllmjv930kax86m0hrd9zvkp3ws")))

(define-public crate-weak-table-0.2 (crate (name "weak-table") (vers "0.2.4") (deps (list (crate-dep (name "quickcheck") (req "^0.9") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.7.2") (default-features #t) (kind 2)))) (hash "04w12y1px90b06lglhkckmvwn6gkpqjhp65nx4m7nm0hx0apgqvq")))

(define-public crate-weak-table-0.3 (crate (name "weak-table") (vers "0.3.1") (deps (list (crate-dep (name "ahash") (req "^0.7.6") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "quickcheck") (req "^1") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.8.4") (default-features #t) (kind 2)))) (hash "1s4bgni6fmv5i1gxcw41qx9iblzm6dfyhg1zwygmmgjsn3vlxdx2") (features (quote (("std") ("default" "std") ("alloc"))))))

(define-public crate-weak-table-0.3 (crate (name "weak-table") (vers "0.3.2") (deps (list (crate-dep (name "ahash") (req "^0.7.6") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "quickcheck") (req "^1") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.8.4") (default-features #t) (kind 2)))) (hash "0ja5zqr1bp5z8wv928y670frnxlj71v6x75g3sg6d6iyaallsgrj") (features (quote (("std") ("default" "std") ("alloc"))))))

(define-public crate-weak_list-0.1 (crate (name "weak_list") (vers "0.1.0") (hash "1rvi6s3wxzjcyksb663zfp6rqk4191ar3l9lm05mkpqms4fdn95v") (yanked #t)))

(define-public crate-weak_list-0.2 (crate (name "weak_list") (vers "0.2.0") (hash "0m1i6f45hjvx4afd4km4h3sd6yjnvzxx1q1q7xy987c7s7j2sgdr") (yanked #t)))

(define-public crate-weak_static-0.1 (crate (name "weak_static") (vers "0.1.0") (deps (list (crate-dep (name "lazy_static") (req "^0.2.8") (default-features #t) (kind 2)))) (hash "1abnwm1klgba7k924cckhzc9945i452qwiinjabl1h0i0qqwwp1s")))

(define-public crate-weak_static-0.1 (crate (name "weak_static") (vers "0.1.1") (deps (list (crate-dep (name "lazy_static") (req "^0.2.8") (default-features #t) (kind 2)))) (hash "0ngjb58p0nwx9kp4ncf6fslwpz9qs7a2bl8423l78wjj9zdfq689")))

(define-public crate-weak_true-0.1 (crate (name "weak_true") (vers "0.1.0") (hash "0sqrjaich2hj177cc6gh3li8h3zz2ipfmmdj8yfjihpq232hp60b")))

(define-public crate-weak_true-0.1 (crate (name "weak_true") (vers "0.1.1") (hash "1nkqkklklzvdzdqdvkjv0b0dmy9cpzpn3qha9wcyhsrmvgx11fg1")))

(define-public crate-weak_true-0.1 (crate (name "weak_true") (vers "0.1.2") (hash "001vfrvvkgfrwwy42lpk0f422c449xr8m4l6kv7xbm9gpprwc550")))

(define-public crate-weak_true-0.1 (crate (name "weak_true") (vers "0.1.3") (hash "0pdr6g080nkhj5c3rdq7qkja04sza863ds5hvgzja8s2iag113aw")))

(define-public crate-weak_true-0.1 (crate (name "weak_true") (vers "0.1.4") (hash "1kd18sgq2ffzsragzc4yg09v23jr9dp4gqlnv0mfkh2xrczj5bzz")))

(define-public crate-weakheap-0.1 (crate (name "weakheap") (vers "0.1.0") (deps (list (crate-dep (name "criterion") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.8.4") (default-features #t) (kind 2)))) (hash "1vzpylncnmk3jsw11662gbch1gfnyii91r9pzakj3v6rdhwv68xv")))

(define-public crate-weakjson-0.0.1 (crate (name "weakjson") (vers "0.0.1") (deps (list (crate-dep (name "rustc-serialize") (req "^0.3") (default-features #t) (kind 0)))) (hash "06lf3r1xr4zywgnv3m71y5s71fs8ydsf5kmq27shfmyxl4nvrabc")))

(define-public crate-weakjson-0.0.2 (crate (name "weakjson") (vers "0.0.2") (deps (list (crate-dep (name "rustc-serialize") (req "^0.3") (default-features #t) (kind 0)))) (hash "156fvx4047hn13k59k5xbfh9w6qghx38vc7lbcpy6bp4fql7n4fr")))

(define-public crate-weakjson-0.0.3 (crate (name "weakjson") (vers "0.0.3") (deps (list (crate-dep (name "rustc-serialize") (req "^0.3") (default-features #t) (kind 0)))) (hash "0qrbimfsvw0kpg994ggsii5syy2bjng185hpqkpvvyx9b77s6k88")))

(define-public crate-weakjson-0.0.4 (crate (name "weakjson") (vers "0.0.4") (deps (list (crate-dep (name "rustc-serialize") (req "^0.3") (default-features #t) (kind 0)))) (hash "0cr2l385qmyvz8s9biwvzzm73rz10x2irxd65aywpxl1z3xg0djk")))

(define-public crate-weakjson-0.0.5 (crate (name "weakjson") (vers "0.0.5") (deps (list (crate-dep (name "rustc-serialize") (req "^0.3") (default-features #t) (kind 0)))) (hash "0anbl82qd5ry0kg23dlimkmq0rdkdmpr1923aznxcrngqnccnhpy")))

(define-public crate-weakjson-0.0.6 (crate (name "weakjson") (vers "0.0.6") (deps (list (crate-dep (name "rustc-serialize") (req "^0.3.7") (default-features #t) (kind 0)))) (hash "005cyyx4hz723957ls49vx3mj8cv4ijp4n5q92m8vg96wimzq456")))

(define-public crate-weakjson-0.0.7 (crate (name "weakjson") (vers "0.0.7") (deps (list (crate-dep (name "rustc-serialize") (req "^0.3.16") (default-features #t) (kind 0)))) (hash "0xq3sdhx6d0p11bh76q550cvpzz2lhifi1k2f4qhx3miiph2xwlg")))

(define-public crate-weakrand-1 (crate (name "weakrand") (vers "1.0.0") (deps (list (crate-dep (name "rand") (req "^0.3") (default-features #t) (kind 0)))) (hash "03ppq2ngd21xsvlbwl3nszxdrpa3qhl4gqdrwk12v1kgi6zc04sq") (features (quote (("nightly"))))))

(define-public crate-weakrand-1 (crate (name "weakrand") (vers "1.0.1") (deps (list (crate-dep (name "rand") (req "^0.3") (default-features #t) (kind 0)))) (hash "1w6rhk7kc4fdx4d51adg79hcws5p923n2dl0n7qswnlsi9zzh9sn") (features (quote (("nightly"))))))

(define-public crate-weakrand-1 (crate (name "weakrand") (vers "1.1.0") (deps (list (crate-dep (name "rand") (req "^0.3") (default-features #t) (kind 0)))) (hash "0vnj883jqz7069wcjx63c36sax57dfxik63jsqpv28h8ai1dm67m") (features (quote (("nightly"))))))

(define-public crate-weakrand-1 (crate (name "weakrand") (vers "1.2.0") (deps (list (crate-dep (name "rand") (req "^0.3") (default-features #t) (kind 0)))) (hash "0c7z0yxindjnz5jrs9lcwv08a2gw5zr95hz79s66m2bykx52ns0m") (features (quote (("nightly"))))))

