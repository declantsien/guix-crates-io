(define-module (crates-io we xi) #:use-module (crates-io))

(define-public crate-wexit-0.1 (crate (name "wexit") (vers "0.1.0") (deps (list (crate-dep (name "winapi") (req "^0.3") (features (quote ("processthreadsapi" "wincon" "winuser"))) (default-features #t) (target "cfg(windows)") (kind 0)))) (hash "1hqm8l5xaalzzaavp581gp13a6pkz59l29973515nb5rnglj19d5")))

