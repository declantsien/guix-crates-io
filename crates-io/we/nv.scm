(define-module (crates-io we nv) #:use-module (crates-io))

(define-public crate-wenv-cli-0.1 (crate (name "wenv-cli") (vers "0.1.0") (deps (list (crate-dep (name "anyhow") (req "^1.0.75") (default-features #t) (kind 0)) (crate-dep (name "atty") (req "^0.2.14") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^4.4.6") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "owo-colors") (req "^3.5.0") (default-features #t) (kind 0)) (crate-dep (name "tabwriter") (req "^1.3.0") (default-features #t) (kind 0)) (crate-dep (name "term_size") (req "^0.3.2") (default-features #t) (kind 0)) (crate-dep (name "winreg") (req "^0.51.0") (default-features #t) (kind 0)))) (hash "1s7bi737y7rgb9ji5ckd37pgf5djlkvypfdcfyxa7s3r5flss0am")))

