(define-module (crates-io we va) #:use-module (crates-io))

(define-public crate-weval-0.0.0 (crate (name "weval") (vers "0.0.0") (hash "1s14502hgxqs7qvy9cgjzcglv1qmx0jdh5kygvlhixndqklhbqsl")))

(define-public crate-weval-0.1 (crate (name "weval") (vers "0.1.0") (deps (list (crate-dep (name "anyhow") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "env_logger") (req "^0.9") (default-features #t) (kind 0)) (crate-dep (name "fxhash") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "indicatif") (req "^0.17") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "rayon") (req "^1.5") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "waffle") (req "^0.0.22") (default-features #t) (kind 0)) (crate-dep (name "wasm-encoder") (req "^0.20") (default-features #t) (kind 0)) (crate-dep (name "wasmparser") (req "^0.95") (default-features #t) (kind 0)) (crate-dep (name "wizer") (req "^2.0") (default-features #t) (kind 0)))) (hash "1fxghy8sr0mf246x49zva0ljlgszlix9ykkf1xjszdfqpgdz800q")))

