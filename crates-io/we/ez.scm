(define-module (crates-io we ez) #:use-module (crates-io))

(define-public crate-weezl-0.0.1 (crate (name "weezl") (vers "0.0.1") (deps (list (crate-dep (name "criterion") (req "^0.3.1") (default-features #t) (kind 2)))) (hash "1qsx1cxdbcbarfg0xdc6yqiq5ws1zjs4ans67mbym6v2z4z3axxv") (features (quote (("raii_no_panic") ("default" "raii_no_panic"))))))

(define-public crate-weezl-0.0.2 (crate (name "weezl") (vers "0.0.2") (deps (list (crate-dep (name "criterion") (req "^0.3.1") (default-features #t) (kind 2)))) (hash "0jn3lajy8jlajz1n87g212395p9xwyszr6rxw67jkyv9y5nla5s4") (features (quote (("std" "alloc") ("default" "std") ("alloc"))))))

(define-public crate-weezl-0.1 (crate (name "weezl") (vers "0.1.0-alpha") (deps (list (crate-dep (name "criterion") (req "^0.3.1") (default-features #t) (kind 2)))) (hash "043k8aijgd8vwx7gz44p2b86sd9y68w5cimbg0h66wzl0zwcpgci") (features (quote (("std" "alloc") ("default" "std") ("alloc"))))))

(define-public crate-weezl-0.1 (crate (name "weezl") (vers "0.1.0-beta.0") (deps (list (crate-dep (name "criterion") (req "^0.3.1") (default-features #t) (kind 2)))) (hash "1x82qvf1vgqq73l8fr99b8skmglbnjyg53fcx093xjhykhh33ash") (features (quote (("std" "alloc") ("default" "std") ("alloc"))))))

(define-public crate-weezl-0.1 (crate (name "weezl") (vers "0.1.0-beta.1") (deps (list (crate-dep (name "criterion") (req "^0.3.1") (default-features #t) (kind 2)))) (hash "0z0sa940rl0r3dy1shdw632kl7k2zl0jniaapicgr19y5a138hq8") (features (quote (("std" "alloc") ("default" "std") ("alloc"))))))

(define-public crate-weezl-0.1 (crate (name "weezl") (vers "0.1.0") (deps (list (crate-dep (name "criterion") (req "^0.3.1") (default-features #t) (kind 2)))) (hash "1fgyz7919kzqkjk9mw3k7aq6dghwpyz6smkr6arjza9sdi5z5lm3") (features (quote (("std" "alloc") ("default" "std") ("alloc"))))))

(define-public crate-weezl-0.1 (crate (name "weezl") (vers "0.1.1") (deps (list (crate-dep (name "criterion") (req "^0.3.1") (default-features #t) (kind 2)))) (hash "1x424yarxj0jwqmailx01n0sqswinjw4956695wkv3lr9mx6xqp0") (features (quote (("std" "alloc") ("default" "std") ("alloc"))))))

(define-public crate-weezl-0.1 (crate (name "weezl") (vers "0.1.2") (deps (list (crate-dep (name "criterion") (req "^0.3.1") (default-features #t) (kind 2)))) (hash "02jqx386s8dkw37nd7913g3bfn8dijxjdw8fq4y811blw7hdd5c7") (features (quote (("std" "alloc") ("default" "std") ("alloc"))))))

(define-public crate-weezl-0.1 (crate (name "weezl") (vers "0.1.3") (deps (list (crate-dep (name "criterion") (req "^0.3.1") (default-features #t) (kind 2)))) (hash "0xx299f7zck0q4g0klpnirzx9ha471kkc4v5rpbls209hgybjary") (features (quote (("std" "alloc") ("default" "std") ("alloc"))))))

(define-public crate-weezl-0.1 (crate (name "weezl") (vers "0.1.4") (deps (list (crate-dep (name "criterion") (req "^0.3.1") (default-features #t) (kind 2)) (crate-dep (name "futures") (req "^0.3.12") (features (quote ("std"))) (optional #t) (kind 0)) (crate-dep (name "tokio") (req "^1") (features (quote ("macros" "io-util" "net" "rt" "rt-multi-thread"))) (kind 2)) (crate-dep (name "tokio-util") (req "^0.6.2") (features (quote ("compat"))) (kind 2)))) (hash "0v16mvdmsicinbhgsm1l7gq1jmcaqrvm22rgn9lrhkhg71wb6cja") (features (quote (("std" "alloc") ("default" "std") ("async" "futures" "std") ("alloc"))))))

(define-public crate-weezl-0.1 (crate (name "weezl") (vers "0.1.5") (deps (list (crate-dep (name "criterion") (req "^0.3.1") (default-features #t) (kind 2)) (crate-dep (name "futures") (req "^0.3.12") (features (quote ("std"))) (optional #t) (kind 0)) (crate-dep (name "tokio") (req "^1") (features (quote ("macros" "io-util" "net" "rt" "rt-multi-thread"))) (kind 2)) (crate-dep (name "tokio-util") (req "^0.6.2") (features (quote ("compat"))) (kind 2)))) (hash "13hy1zwnqrf021syjhz79mmi9bwwqjizzr0lnx5bwlx2spgpzdyq") (features (quote (("std" "alloc") ("default" "std") ("async" "futures" "std") ("alloc"))))))

(define-public crate-weezl-0.1 (crate (name "weezl") (vers "0.1.6") (deps (list (crate-dep (name "criterion") (req "^0.3.1") (default-features #t) (kind 2)) (crate-dep (name "futures") (req "^0.3.12") (features (quote ("std"))) (optional #t) (kind 0)) (crate-dep (name "tokio") (req "^1") (features (quote ("macros" "io-util" "net" "rt" "rt-multi-thread"))) (kind 2)) (crate-dep (name "tokio-util") (req "^0.6.2") (features (quote ("compat"))) (kind 2)))) (hash "1r6mbc322d93g5ayqafmhrs12sziiibdx4bh966q6dpqv24y95ww") (features (quote (("std" "alloc") ("default" "std") ("async" "futures" "std") ("alloc"))))))

(define-public crate-weezl-0.1 (crate (name "weezl") (vers "0.1.7") (deps (list (crate-dep (name "criterion") (req "^0.3.1") (default-features #t) (kind 2)) (crate-dep (name "futures") (req "^0.3.12") (features (quote ("std"))) (optional #t) (kind 0)) (crate-dep (name "tokio") (req "^1") (features (quote ("macros" "io-util" "net" "rt" "rt-multi-thread"))) (kind 2)) (crate-dep (name "tokio-util") (req "^0.6.2") (features (quote ("compat"))) (kind 2)))) (hash "1frdbq6y5jn2j93i20hc80swpkj30p1wffwxj1nr4fp09m6id4wi") (features (quote (("std" "alloc") ("default" "std") ("async" "futures" "std") ("alloc"))))))

(define-public crate-weezl-0.1 (crate (name "weezl") (vers "0.1.8") (deps (list (crate-dep (name "criterion") (req "^0.3.1") (default-features #t) (kind 2)) (crate-dep (name "futures") (req "^0.3.12") (features (quote ("std"))) (optional #t) (kind 0)) (crate-dep (name "tokio") (req "^1") (features (quote ("macros" "io-util" "net" "rt" "rt-multi-thread"))) (kind 2)) (crate-dep (name "tokio-util") (req "^0.6.2") (features (quote ("compat"))) (kind 2)))) (hash "10lhndjgs6y5djpg3b420xngcr6jkmv70q8rb1qcicbily35pa2k") (features (quote (("std" "alloc") ("default" "std") ("async" "futures" "std") ("alloc"))))))

