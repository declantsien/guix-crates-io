(define-module (crates-io we cs) #:use-module (crates-io))

(define-public crate-wecs-0.1 (crate (name "wecs") (vers "0.1.0") (deps (list (crate-dep (name "wecs-core") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "wecs-derive") (req "^0.1.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "wecs-events") (req "^0.1.0") (optional #t) (default-features #t) (kind 0)))) (hash "006was2mwgvgc2z11fv2hqxm7fz43wfibp4xi8c2q6rnda25if9c") (features (quote (("default" "derive" "events")))) (yanked #t) (v 2) (features2 (quote (("events" "dep:wecs-events") ("derive" "dep:wecs-derive"))))))

(define-public crate-wecs-0.1 (crate (name "wecs") (vers "0.1.2") (deps (list (crate-dep (name "wecs-core") (req "^0.1.2") (default-features #t) (kind 0)) (crate-dep (name "wecs-derive") (req "^0.1.2") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "wecs-events") (req "^0.1.2") (optional #t) (default-features #t) (kind 0)))) (hash "09d1mj7540nbma5ivjli0iffp9ycrmrf73xyp0hcf6wiv9r0nrq0") (features (quote (("default" "derive" "events")))) (v 2) (features2 (quote (("events" "dep:wecs-events") ("derive" "dep:wecs-derive"))))))

(define-public crate-wecs-core-0.1 (crate (name "wecs-core") (vers "0.1.0") (deps (list (crate-dep (name "wecs-derive") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "1k5z1akz0z0l5q52ml6gpdhbfj19k9ri4hcs15ghhbsg7zrpb3l4") (yanked #t)))

(define-public crate-wecs-core-0.1 (crate (name "wecs-core") (vers "0.1.1") (deps (list (crate-dep (name "wecs-derive") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "1blds1vi84rsp4prsa68wn1bz6k6f5070hs8vgcycfca4f9kyfnz") (yanked #t)))

(define-public crate-wecs-core-0.1 (crate (name "wecs-core") (vers "0.1.2") (hash "0dr8b92m4kmxpz6z9j4dwf37ajj9451ycliwcgig6qyqzlxj7hyk")))

(define-public crate-wecs-derive-0.1 (crate (name "wecs-derive") (vers "0.1.0") (deps (list (crate-dep (name "quote") (req "^1.0.28") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2.0.17") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "17qmsga39vvsjwsvgcrmw6bkq364ix88afda5fsdmw5q7gpybk82") (yanked #t)))

(define-public crate-wecs-derive-0.1 (crate (name "wecs-derive") (vers "0.1.1") (deps (list (crate-dep (name "quote") (req "^1.0.28") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2.0.17") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "1pzh34gx8ihfxd4d28hhymc0dfxcrs24ff6g9lv6rhhc2fbf9738") (yanked #t)))

(define-public crate-wecs-derive-0.1 (crate (name "wecs-derive") (vers "0.1.2") (deps (list (crate-dep (name "quote") (req "^1.0.28") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2.0.17") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "06kgf08n3mi60mhmp6bazyv1p39jnzbjfxmsgnndyia3pqqqhkgy")))

(define-public crate-wecs-events-0.1 (crate (name "wecs-events") (vers "0.1.0") (deps (list (crate-dep (name "wecs-core") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "wecs-derive") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "0ng53d9wj6zsdp9kwyrnqx8b4kf2zq9x83xrykgvd950p8szqmvk") (yanked #t)))

(define-public crate-wecs-events-0.1 (crate (name "wecs-events") (vers "0.1.2") (deps (list (crate-dep (name "wecs-core") (req "^0.1.2") (default-features #t) (kind 0)) (crate-dep (name "wecs-derive") (req "^0.1.2") (default-features #t) (kind 0)))) (hash "0v0jnbaq0qvdsh341lzdyrg8r57g7xrbjm5zsads5pz20vcn14ah")))

