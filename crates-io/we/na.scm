(define-module (crates-io we na) #:use-module (crates-io))

(define-public crate-wena-0.0.1 (crate (name "wena") (vers "0.0.1") (deps (list (crate-dep (name "clap") (req "^3.1.18") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "colored") (req "^2.0.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.5.6") (default-features #t) (kind 0)))) (hash "1rf6fz0h41dkj6546acdgvsxdpk4s4jsaqxni4m69snyczgpw8vr")))

(define-public crate-wena-0.0.2 (crate (name "wena") (vers "0.0.2") (deps (list (crate-dep (name "clap") (req "^3.1.18") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "colored") (req "^2.0.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.5.6") (default-features #t) (kind 0)))) (hash "12zi655zw5g6b42sgb4jihh9dmbbzarp29sblsadys2sgbk2nz7l")))

(define-public crate-wena-0.0.3 (crate (name "wena") (vers "0.0.3") (deps (list (crate-dep (name "clap") (req "^3.1.18") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "colored") (req "^2.0.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.5.6") (default-features #t) (kind 0)) (crate-dep (name "terminal_size") (req "^0.1.17") (default-features #t) (kind 0)))) (hash "104gag9jk2w9z8bcqw7mk9w6iwpfz89mq2r1xx8ivf9xs7k1kmhl")))

(define-public crate-wena-0.1 (crate (name "wena") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^3.1.18") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "colored") (req "^2.0.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.5.6") (default-features #t) (kind 0)) (crate-dep (name "terminal_size") (req "^0.1.17") (default-features #t) (kind 0)))) (hash "1psjhj08jgvplys8d70ncb4ikdaqrjn0sjarnrcnha1ns144lp90")))

(define-public crate-wena-0.1 (crate (name "wena") (vers "0.1.1") (deps (list (crate-dep (name "clap") (req "^3.1.18") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "colored") (req "^2.0.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.5.6") (default-features #t) (kind 0)) (crate-dep (name "terminal_size") (req "^0.1.17") (default-features #t) (kind 0)))) (hash "16yi3h5dqjsypvs5n5dc7xv21v5ck0i5k34d7dfvi10jmy0swjs4")))

(define-public crate-wena-0.2 (crate (name "wena") (vers "0.2.0") (deps (list (crate-dep (name "clap") (req "^3.1.18") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "colored") (req "^2.0.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.5.6") (default-features #t) (kind 0)) (crate-dep (name "terminal_size") (req "^0.1.17") (default-features #t) (kind 0)))) (hash "12waggndrb1fx3n9ippa216dzyxj9nyviy2mn2agyns71i9wglpj")))

