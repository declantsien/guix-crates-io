(define-module (crates-io we dk) #:use-module (crates-io))

(define-public crate-wedkarz_guessing_game-0.1 (crate (name "wedkarz_guessing_game") (vers "0.1.0") (deps (list (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)))) (hash "0s5nb70rlsyq47bagx6xs1240kwxn1j1795jdzmrcsdrf0llhzwy") (yanked #t)))

(define-public crate-wedkarz_guessing_game-0.1 (crate (name "wedkarz_guessing_game") (vers "0.1.1") (deps (list (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)))) (hash "0li2w1qp19z5p1hsjxs4ffcnbwwx1jf11q8v9aanvb12j4fk6s3d")))

(define-public crate-wedkarz_guessing_game-0.1 (crate (name "wedkarz_guessing_game") (vers "0.1.2") (deps (list (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)))) (hash "1v12bp98iffv8xw1g8c6k5pl53qdkqzfdpq51hcihg7ydjx6s738")))

