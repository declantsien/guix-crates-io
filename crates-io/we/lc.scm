(define-module (crates-io we lc) #:use-module (crates-io))

(define-public crate-welch-sde-0.1 (crate (name "welch-sde") (vers "0.1.0") (deps (list (crate-dep (name "complot") (req "^0.3.2") (default-features #t) (kind 2)) (crate-dep (name "num-complex") (req "^0.4.0") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2.14") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.4") (default-features #t) (kind 2)) (crate-dep (name "rand_distr") (req "^0.4.2") (default-features #t) (kind 2)) (crate-dep (name "rustfft") (req "^6.0.1") (default-features #t) (kind 0)))) (hash "0rv034ygy7jg2hsnwhd5bhcvrzwkxfd341gxzv2iy0sqvq7f1ycw")))

(define-public crate-WelcomeBack-0.1 (crate (name "WelcomeBack") (vers "0.1.0") (deps (list (crate-dep (name "futures") (req "^0.3") (default-features #t) (kind 0)))) (hash "1ch3byhws7svr4g9hpcfbskj1gn5mixbpmd8mjn5q2xkck1yadqm") (yanked #t)))

(define-public crate-WelcomeBack-0.1 (crate (name "WelcomeBack") (vers "0.1.1") (hash "19lisdil75805ldggbf49w26a4bwmswb02zddfkzvjj5mhggkn5f")))

