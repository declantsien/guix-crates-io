(define-module (crates-io we b_) #:use-module (crates-io))

(define-public crate-web_audit-0.0.0 (crate (name "web_audit") (vers "0.0.0") (deps (list (crate-dep (name "async-recursion") (req "^1.0.0") (default-features #t) (kind 0)) (crate-dep (name "async-trait") (req "^0.1.53") (default-features #t) (kind 2)) (crate-dep (name "clap") (req "^3.1.12") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "fantoccini") (req "^0.18.0") (default-features #t) (kind 0)) (crate-dep (name "futures") (req "^0.3.21") (default-features #t) (kind 0)) (crate-dep (name "futures") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "geckodriver") (req "^0.31.0") (default-features #t) (kind 0)) (crate-dep (name "no_deadlocks") (req "^1.3.0") (default-features #t) (kind 2)) (crate-dep (name "serde") (req "^1.0.136") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.79") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1") (features (quote ("full"))) (default-features #t) (kind 0)) (crate-dep (name "tokio-test") (req "^0.4.2") (default-features #t) (kind 2)))) (hash "179l2afxlic0nry5asi9r0r6ig61f6my43q23cfma7hl5rc012b3") (yanked #t)))

(define-public crate-web_audit-0.0.1 (crate (name "web_audit") (vers "0.0.1") (deps (list (crate-dep (name "async-recursion") (req "^1.0.0") (default-features #t) (kind 0)) (crate-dep (name "async-trait") (req "^0.1.53") (default-features #t) (kind 2)) (crate-dep (name "clap") (req "^3.1.12") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "fantoccini") (req "^0.18.0") (default-features #t) (kind 0)) (crate-dep (name "futures") (req "^0.3.21") (default-features #t) (kind 0)) (crate-dep (name "futures") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "geckodriver") (req "^0.31.0") (default-features #t) (kind 0)) (crate-dep (name "no_deadlocks") (req "^1.3.0") (default-features #t) (kind 2)) (crate-dep (name "serde") (req "^1.0.136") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.79") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1") (features (quote ("full"))) (default-features #t) (kind 0)) (crate-dep (name "tokio-test") (req "^0.4.2") (default-features #t) (kind 2)))) (hash "1zcb607fb6143x6h1ih400306msh3icg1ypp47iyzkvf8w02lkx0")))

(define-public crate-web_canvas-0.0.0 (crate (name "web_canvas") (vers "0.0.0") (hash "0hpagv3yab2ns5pb9rgix9zsjvzb4bglcz4nh6qmckciid817g43")))

(define-public crate-web_canvas-0.0.1 (crate (name "web_canvas") (vers "0.0.1") (deps (list (crate-dep (name "globals") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "js") (req "^0.2") (default-features #t) (kind 0)))) (hash "1xzv4f9nz3i9psmvg2mcykrcxlclzscp5df2768mcv8xr6x63k4d")))

(define-public crate-web_canvas-0.0.2 (crate (name "web_canvas") (vers "0.0.2") (deps (list (crate-dep (name "js") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "once_cell") (req "^1.5.2") (default-features #t) (kind 0)) (crate-dep (name "spin") (req "^0.7.0") (default-features #t) (kind 0)))) (hash "0zv5w9czv711f8jz1i7hhby199qbparqa2b7cf8ada70i32kpwjb")))

(define-public crate-web_canvas-0.0.3 (crate (name "web_canvas") (vers "0.0.3") (deps (list (crate-dep (name "js") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1") (features (quote ("spin_no_std"))) (kind 0)) (crate-dep (name "spin") (req "^0.7.0") (default-features #t) (kind 0)))) (hash "03hy0va46yj0303avaxx6j9zwq49vpi796wz806la0yz68yb3myp")))

(define-public crate-web_canvas-0.0.4 (crate (name "web_canvas") (vers "0.0.4") (deps (list (crate-dep (name "js") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1") (features (quote ("spin_no_std"))) (kind 0)) (crate-dep (name "spin") (req "^0.7.0") (default-features #t) (kind 0)))) (hash "1xp1ic78hxgypm5k5708xd124m4cdwzbbvd7fh9151rj64dx3b8r")))

(define-public crate-web_canvas-0.0.5 (crate (name "web_canvas") (vers "0.0.5") (deps (list (crate-dep (name "js") (req ">=0.2.0, <0.3.0") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req ">=1.0.0, <2.0.0") (features (quote ("spin_no_std"))) (kind 0)) (crate-dep (name "spin") (req ">=0.7.0, <0.8.0") (default-features #t) (kind 0)))) (hash "1j0gipy2qyfzbqar1i0dni76av9z58y42fprsmrnxwkyhkfn0r6v")))

(define-public crate-web_canvas-0.0.6 (crate (name "web_canvas") (vers "0.0.6") (deps (list (crate-dep (name "js") (req ">=0.2.0, <0.3.0") (default-features #t) (kind 0)))) (hash "06092dxvg3mlvbbqcdy5g661ad7baz9j1qgysyqq5h789jk5j81j")))

(define-public crate-web_canvas-0.0.7 (crate (name "web_canvas") (vers "0.0.7") (deps (list (crate-dep (name "js") (req "^0") (default-features #t) (kind 0)))) (hash "02c880d8jri0n1hf2rlhfmpq4b8l3cmzmyaackzb23fdvajlv1fp")))

(define-public crate-web_common-0.0.1 (crate (name "web_common") (vers "0.0.1") (hash "0asf41dfw7aysxj2h7jqqnngvisicq4bh22z45yi2jrm080n0c0f")))

(define-public crate-web_common-0.0.2 (crate (name "web_common") (vers "0.0.2") (hash "0985f9in02a8l6vjak0x9fs9pyq5ram1d3h10g3gdbdr6lvzsyci")))

(define-public crate-web_common-0.0.3 (crate (name "web_common") (vers "0.0.3") (hash "002rw46l41gwmh8kprcjxk3plabkkdhnymkvcryr1625fmhi4ycn")))

(define-public crate-web_common-0.0.4 (crate (name "web_common") (vers "0.0.4") (hash "1rbz8dl97jq67g66rgmy2ccrln96b9a73lxgmycbav3w8a0sk02k")))

(define-public crate-web_common-0.0.5 (crate (name "web_common") (vers "0.0.5") (hash "0j4n0v02h5ddryks8c4ga2an1grbwrdq9kpnih4dspwffvi86wp3")))

(define-public crate-web_common-0.0.6 (crate (name "web_common") (vers "0.0.6") (hash "0dlhvdvghndiayqv43sr16fzrx4iqch30hsil5jzy7r61vmv51kb")))

(define-public crate-web_common-0.0.7 (crate (name "web_common") (vers "0.0.7") (hash "1n47fw3m2grkrvrzkg8fif3amzg5wpvg89dxfd652azmvl0csjmy")))

(define-public crate-web_common-0.1 (crate (name "web_common") (vers "0.1.0") (hash "1rm02qns6ymw8bbc5mfcqwnqylnh6pks19imi5qf83g9mg5im50y")))

(define-public crate-web_common-0.2 (crate (name "web_common") (vers "0.2.0") (deps (list (crate-dep (name "web_console") (req "^0") (default-features #t) (kind 0)) (crate-dep (name "web_random") (req "^0") (default-features #t) (kind 0)) (crate-dep (name "web_timer") (req "^0") (default-features #t) (kind 0)))) (hash "0kx6ywy1vlgwc81b1b5p7zlqcs1kfp669szgp81sf1cx1mvhmxnq")))

(define-public crate-web_common-0.3 (crate (name "web_common") (vers "0.3.0") (deps (list (crate-dep (name "js") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "once_cell") (req "^1.5.2") (default-features #t) (kind 0)) (crate-dep (name "spin") (req "^0.7.0") (default-features #t) (kind 0)))) (hash "1fkhppv57nvnildcz0n7q925j8iys2rldf2n4rpvbina919w3gld")))

(define-public crate-web_common-0.3 (crate (name "web_common") (vers "0.3.1") (deps (list (crate-dep (name "js") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1") (features (quote ("spin_no_std"))) (kind 0)) (crate-dep (name "spin") (req "^0.7.0") (default-features #t) (kind 0)))) (hash "1wm22f99ixranb4c0hw5wm1y46w21vpq0i3q0sk4sjc4iahjrl75")))

(define-public crate-web_common-0.3 (crate (name "web_common") (vers "0.3.2") (deps (list (crate-dep (name "js") (req ">=0.2.0, <0.3.0") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req ">=1.0.0, <2.0.0") (features (quote ("spin_no_std"))) (kind 0)) (crate-dep (name "spin") (req ">=0.7.0, <0.8.0") (default-features #t) (kind 0)))) (hash "192dddandnr1fv7x0ylj4f8ck3xxw620m0h17agw2wgmi3iirj2s")))

(define-public crate-web_common-0.3 (crate (name "web_common") (vers "0.3.3") (deps (list (crate-dep (name "js") (req ">=0.2.0, <0.3.0") (default-features #t) (kind 0)))) (hash "1866b9jc6x2vanydpgx6a536vfyr0paf73sfpz7fpfyrl0h875kk")))

(define-public crate-web_common-0.4 (crate (name "web_common") (vers "0.4.0") (deps (list (crate-dep (name "js") (req "^0.2") (default-features #t) (kind 0)))) (hash "0s850vpaiynvfmd9kfv1nd7s15944raicxda1r060bjj2wlpiskj")))

(define-public crate-web_common-0.4 (crate (name "web_common") (vers "0.4.1") (deps (list (crate-dep (name "js") (req "^0.2") (default-features #t) (kind 0)))) (hash "0s5crpn9zb7af0qy66yndaa0jb4yibsyn9wkmphm5y7fz93pbvch")))

(define-public crate-web_common-0.4 (crate (name "web_common") (vers "0.4.2") (deps (list (crate-dep (name "js") (req "^0") (default-features #t) (kind 0)))) (hash "09nsbj5r1bpn4h09aqkfi7m9x4gbbqdax263n2dy13zpynq988nn")))

(define-public crate-web_common-0.4 (crate (name "web_common") (vers "0.4.3") (deps (list (crate-dep (name "js") (req "^0") (default-features #t) (kind 0)))) (hash "0saw3612czy0089am0wyrmkj13jxjnqwygb39kpnii4cqpssbb5r")))

(define-public crate-web_console-0.0.0 (crate (name "web_console") (vers "0.0.0") (deps (list (crate-dep (name "js_ffi") (req "^0.0.13") (default-features #t) (kind 0)))) (hash "11jixj5ygs2lqdx4kxdm4p321xa24g3n43376c9fw9j4zqwjwisl")))

(define-public crate-web_console-0.0.2 (crate (name "web_console") (vers "0.0.2") (deps (list (crate-dep (name "js_ffi") (req "^0.1") (default-features #t) (kind 0)))) (hash "1qk115paj04ijnsghgfalip634a60yniad38dcl2z2nkwxw17yrj")))

(define-public crate-web_console-0.0.3 (crate (name "web_console") (vers "0.0.3") (deps (list (crate-dep (name "js_ffi") (req "^0.5") (default-features #t) (kind 0)))) (hash "0bzspyrnmq1qj0fzzcjb2xrl16bfnpy6bag2a2cy5d6zwhb56flk")))

(define-public crate-web_console-0.0.4 (crate (name "web_console") (vers "0.0.4") (deps (list (crate-dep (name "js_ffi") (req "^0.6") (default-features #t) (kind 0)))) (hash "0kqbfxhyg98sx39m5in53jra8h30bs3wv1cj3z9wf9b9c2chh66w")))

(define-public crate-web_console-0.1 (crate (name "web_console") (vers "0.1.0") (deps (list (crate-dep (name "globals") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "js_ffi") (req "^0.6") (default-features #t) (kind 0)))) (hash "01k2640dbpjg0sbxsm6hy6dd67nm28597ax176r2vrvd8hji74hp")))

(define-public crate-web_console-0.2 (crate (name "web_console") (vers "0.2.0") (deps (list (crate-dep (name "globals") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "js_ffi") (req "^0.6") (default-features #t) (kind 0)))) (hash "1gak1i8g57gpvnkgramss67lhclcmk95rb4s29cqzralwa0b5pa5")))

(define-public crate-web_console-0.2 (crate (name "web_console") (vers "0.2.1") (deps (list (crate-dep (name "globals") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "js_ffi") (req "^0.6") (default-features #t) (kind 0)))) (hash "18r84529qn7pynmc50adhzc1yas50wmcwk8sq8aim42wz8y5kp7y")))

(define-public crate-web_console-0.3 (crate (name "web_console") (vers "0.3.1") (deps (list (crate-dep (name "globals") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "js") (req "^0.2") (default-features #t) (kind 0)))) (hash "0cgg4b94rvjhzy3qy1hr2xhb2xlxiw2h3kkj3ry896jf82wi7i7w")))

(define-public crate-web_console-0.3 (crate (name "web_console") (vers "0.3.2") (deps (list (crate-dep (name "js") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "once_cell") (req "^1.5.2") (default-features #t) (kind 0)) (crate-dep (name "spin") (req "^0.7.0") (default-features #t) (kind 0)))) (hash "06qn3fsn7ljdp5ghxd6xmqixfq0yn1yn8x3g88r6kh3gmcdj8dc6")))

(define-public crate-web_console-0.3 (crate (name "web_console") (vers "0.3.3") (deps (list (crate-dep (name "js") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1") (features (quote ("spin_no_std"))) (kind 0)) (crate-dep (name "spin") (req "^0.7.0") (default-features #t) (kind 0)))) (hash "0fjd3rdsc3l7lng9vk0qm6i6ic3pi6ngb1lmlr5kzn4x1ibl357g")))

(define-public crate-web_console-0.3 (crate (name "web_console") (vers "0.3.4") (deps (list (crate-dep (name "js") (req ">=0.2.0, <0.3.0") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req ">=1.4.0, <2.0.0") (features (quote ("spin_no_std"))) (kind 0)) (crate-dep (name "spin") (req ">=0.7.0, <0.8.0") (default-features #t) (kind 0)))) (hash "1hbm9hsll4n52rac1gz3wcir6mb4dka94395g47qlcivw823fb23")))

(define-public crate-web_console-0.3 (crate (name "web_console") (vers "0.3.5") (deps (list (crate-dep (name "js") (req ">=0.2.0, <0.3.0") (default-features #t) (kind 0)))) (hash "1116fazvsfg4x5dp5xzaapza3yjdz7gj57rlwkiy79kz7w2nlz8m")))

(define-public crate-web_console-0.3 (crate (name "web_console") (vers "0.3.6") (deps (list (crate-dep (name "js") (req "^0.2") (default-features #t) (kind 0)))) (hash "1p27aqgr2qk979z7bk94wvaj2vblqk2zdgl7cy5r907n9jsd5f5r")))

(define-public crate-web_console-0.3 (crate (name "web_console") (vers "0.3.7") (deps (list (crate-dep (name "js") (req "^0") (default-features #t) (kind 0)))) (hash "0barnlv08bk9i8sxkmhn1081zx6n1mvmn251zmh38lgxd81wr4p1")))

(define-public crate-web_dev-0.1 (crate (name "web_dev") (vers "0.1.0") (hash "064psak7vpm8x8wpf9p4wb1yrj3lbkqw9s9bq2sw0lfykdjg6k0h")))

(define-public crate-web_fetch-0.0.0 (crate (name "web_fetch") (vers "0.0.0") (hash "00q28fqxpjjahxmgmjvfg28q6ah4wqgaxa8mqwd4ayrsawshff84")))

(define-public crate-web_gen-0.0.0 (crate (name "web_gen") (vers "0.0.0") (deps (list (crate-dep (name "nom") (req "^7.1.2") (default-features #t) (kind 0)))) (hash "018pqhwj36p1jcfj03nmh1ykl3rk28kvmbxf6qwnp4y8nqga0gmx")))

(define-public crate-web_ical-0.1 (crate (name "web_ical") (vers "0.1.0") (deps (list (crate-dep (name "chrono") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.1.9") (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.9.18") (default-features #t) (kind 0)))) (hash "08ldvyrd92g0gym43njwkrxm5axdjjwx0i8iililh5h107p9wdh4") (yanked #t)))

(define-public crate-web_ical-0.1 (crate (name "web_ical") (vers "0.1.1") (deps (list (crate-dep (name "chrono") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.1.9") (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.9.18") (default-features #t) (kind 0)))) (hash "02091kik2755v0inv7piqipd1b49mm9fqvdhyciiva4iknigdj77") (yanked #t)))

(define-public crate-web_ical-0.1 (crate (name "web_ical") (vers "0.1.2") (deps (list (crate-dep (name "chrono") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.1.9") (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.9.18") (default-features #t) (kind 0)))) (hash "08mrq5i7a8fzl5j4s8j8zwh8ib31rdbxkhp67v42sjrq749hq93p")))

(define-public crate-web_instant-0.1 (crate (name "web_instant") (vers "0.1.0") (deps (list (crate-dep (name "js-sys") (req "^0.3.60") (default-features #t) (target "wasm32-unknown-unknown") (kind 0)))) (hash "0pwbmkvjd9gy68sdpg63g8566q5yrm1v56xnzy73b328n28w1jzm")))

(define-public crate-web_instant-0.2 (crate (name "web_instant") (vers "0.2.0") (deps (list (crate-dep (name "js-sys") (req "^0.3.60") (default-features #t) (target "wasm32-unknown-unknown") (kind 0)))) (hash "091srdrfg2qbbsad92l6231y9y9xck3rbq8c70is9h1hsz52h64w")))

(define-public crate-web_instant-0.2 (crate (name "web_instant") (vers "0.2.1") (deps (list (crate-dep (name "js-sys") (req "^0.3.60") (default-features #t) (target "wasm32-unknown-unknown") (kind 0)))) (hash "1plh6bqn00zk9h92993awkqcx33xa74jpih73402n18bmyayp7zv")))

(define-public crate-web_instant-0.3 (crate (name "web_instant") (vers "0.3.0") (deps (list (crate-dep (name "js-sys") (req "^0.3.60") (default-features #t) (target "wasm32-unknown-unknown") (kind 0)))) (hash "0pys816q4rsf5ir00mzj0h3mggg1sng4xy99g176qhpli66ip5sf")))

(define-public crate-web_local_storage-0.0.0 (crate (name "web_local_storage") (vers "0.0.0") (deps (list (crate-dep (name "js") (req "^0.2") (default-features #t) (kind 0)))) (hash "1bv64jhp6qzlyidjfg0jiw3ym8j6f4hc1hbk63yz3rw30hknz7sq")))

(define-public crate-web_local_storage-0.0.1 (crate (name "web_local_storage") (vers "0.0.1") (deps (list (crate-dep (name "js") (req "^0.2") (default-features #t) (kind 0)))) (hash "0lkzkagmny73rk5hrfiwq99m78cr47143clrf4n713ypdb0wnqgk")))

(define-public crate-web_local_storage-0.0.2 (crate (name "web_local_storage") (vers "0.0.2") (deps (list (crate-dep (name "js") (req "^0") (default-features #t) (kind 0)))) (hash "12cm3scynzfvcj9y8msngl4098lr057hqz53bzhvg6f0yc3b36w9")))

(define-public crate-web_local_storage-0.0.3 (crate (name "web_local_storage") (vers "0.0.3") (deps (list (crate-dep (name "js") (req "^0") (default-features #t) (kind 0)))) (hash "0drna8hgaj0zsv46h0bziwjwvfh9mrrmjs3n9j9zp1l5rgqzpwpz")))

(define-public crate-web_logger-0.1 (crate (name "web_logger") (vers "0.1.0") (deps (list (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "stdweb") (req "^0.4") (default-features #t) (kind 0)))) (hash "19b1s61lqc0x681qjmfzjw825s3bi18ihv294nl3p0vb2k1bk3d7")))

(define-public crate-web_logger-0.2 (crate (name "web_logger") (vers "0.2.0") (deps (list (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "stdweb") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "wasm-bindgen") (req "^0.2") (default-features #t) (target "cfg(all(target_arch = \"wasm32\", not(cargo_web)))") (kind 0)))) (hash "10s6qxdnsn7n6vm0z7p5sbqmk1cw0knx2xzv7yiqqwif1k1gw2pm")))

(define-public crate-web_macro-0.0.0 (crate (name "web_macro") (vers "0.0.0") (deps (list (crate-dep (name "quote") (req "^1.0.23") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.107") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "0syy92b2yh7kmlwk36hx5hl28c746n887d4p7aarydiqsqrwvh5b")))

(define-public crate-web_macro-0.0.1 (crate (name "web_macro") (vers "0.0.1") (deps (list (crate-dep (name "quote") (req "^1.0.23") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.107") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "1gw9p7y5fbr9fvli2k0ycrzdn842ahsvnicd3j034a8nk4za12m2")))

(define-public crate-web_macro-0.0.2 (crate (name "web_macro") (vers "0.0.2") (deps (list (crate-dep (name "quote") (req "^1.0.23") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.107") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "1rnllz1jny6b3ndnvw8dmnzzyj29xad5kyl4v3z2qrj0zk9bidyb")))

(define-public crate-web_macro-0.0.3 (crate (name "web_macro") (vers "0.0.3") (deps (list (crate-dep (name "quote") (req "^1.0.23") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.107") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "1q3a09a94wlskndmrzva9065wrndcar49gjlf2pqajg5cx99z492")))

(define-public crate-web_panic_hook-0.1 (crate (name "web_panic_hook") (vers "0.1.0") (deps (list (crate-dep (name "html") (req "^0.6") (default-features #t) (kind 0)) (crate-dep (name "web-sys") (req "^0.3") (features (quote ("Document" "DomException" "Element" "HtmlElement" "Window"))) (default-features #t) (kind 0)))) (hash "06parm2c9pwdgccsxib0xs1v2g77aw8cixl10n8v0di7kpvxqfmz")))

(define-public crate-web_panic_report-0.1 (crate (name "web_panic_report") (vers "0.1.0") (deps (list (crate-dep (name "wasm-bindgen") (req "^0.2.92") (default-features #t) (kind 0)) (crate-dep (name "wasm-bindgen-test") (req "^0.3.42") (default-features #t) (kind 2)) (crate-dep (name "web-sys") (req "^0.3.69") (features (quote ("Window" "Document" "CssStyleDeclaration" "HtmlElement" "HtmlTextAreaElement" "HtmlButtonElement"))) (default-features #t) (kind 0)))) (hash "0rcd7hl1s1a2y0cgnywlblirvy682393vrg9xvmllmpr7xsq3irf")))

(define-public crate-web_panic_report-0.1 (crate (name "web_panic_report") (vers "0.1.1") (deps (list (crate-dep (name "wasm-bindgen") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "wasm-bindgen-test") (req "^0.3.42") (default-features #t) (kind 2)) (crate-dep (name "web-sys") (req "^0.3") (features (quote ("Window" "Document" "CssStyleDeclaration" "HtmlElement" "HtmlTextAreaElement" "HtmlButtonElement"))) (default-features #t) (kind 0)))) (hash "06ag8igw1vhbc455sfpijqm5addmr6vs9f96zqycbzsv2ng6l69s")))

(define-public crate-web_panic_report-0.2 (crate (name "web_panic_report") (vers "0.2.0") (deps (list (crate-dep (name "wasm-bindgen") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "wasm-bindgen-test") (req "^0.3.42") (default-features #t) (kind 2)) (crate-dep (name "web-sys") (req "^0.3") (features (quote ("Window" "Document" "CssStyleDeclaration" "HtmlElement" "HtmlTextAreaElement" "HtmlButtonElement"))) (default-features #t) (kind 0)))) (hash "1kl17rxxxsn2gm92w5kca14hqn9811xx2jn46p1h34n0f9h5i28c") (features (quote (("default-form") ("default" "default-form"))))))

(define-public crate-web_proxy-0.1 (crate (name "web_proxy") (vers "0.1.0") (deps (list (crate-dep (name "bytes") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^4.2.4") (default-features #t) (kind 0)) (crate-dep (name "http") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "http-body-util") (req "^0.1.0-rc.2") (default-features #t) (kind 0)) (crate-dep (name "hyper") (req "^1.0.0-rc.3") (features (quote ("full"))) (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1") (features (quote ("full"))) (default-features #t) (kind 0)) (crate-dep (name "url") (req "^2.3.1") (default-features #t) (kind 0)))) (hash "0m7yjf6a45wy62xmd7cwr9zkl3100hss9hy1axw5jqhw6k9c1753") (rust-version "1.68.0")))

(define-public crate-web_random-0.0.0 (crate (name "web_random") (vers "0.0.0") (deps (list (crate-dep (name "js_ffi") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.7.2") (default-features #t) (kind 0)))) (hash "017av33g61cqdwik9bc6j5fw11fvrisgzzxwlcvmgbbdrcppsnq7")))

(define-public crate-web_random-0.0.1 (crate (name "web_random") (vers "0.0.1") (deps (list (crate-dep (name "js_ffi") (req "^0.5") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.7.2") (default-features #t) (kind 0)))) (hash "1wflzb60kv8hjy3h17w9kwj42iag39nnym37c7xa6ns5p2sayyz2")))

(define-public crate-web_random-0.0.2 (crate (name "web_random") (vers "0.0.2") (deps (list (crate-dep (name "js_ffi") (req "^0.6") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.7.2") (default-features #t) (kind 0)))) (hash "02jrqdjjxmmyqzsrn1wfcjphg0xkwcsmrfljlc8kp1f0qskzsgf1")))

(define-public crate-web_random-0.1 (crate (name "web_random") (vers "0.1.0") (deps (list (crate-dep (name "globals") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "js") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.7.3") (default-features #t) (kind 0)))) (hash "0wbrxya9kjdl8ypm0y5yb30v3aqigzinmlgh0m897rhsjcj1q273")))

(define-public crate-web_random-0.1 (crate (name "web_random") (vers "0.1.1") (deps (list (crate-dep (name "js") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "once_cell") (req "^1.5.2") (default-features #t) (kind 0)) (crate-dep (name "spin") (req "^0.7.0") (default-features #t) (kind 0)))) (hash "1mcc8s2sf69qf31zqpspzy9daqdb11740h5r4vrdq1sg8a8lz89s")))

(define-public crate-web_random-0.1 (crate (name "web_random") (vers "0.1.2") (deps (list (crate-dep (name "js") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1") (features (quote ("spin_no_std"))) (kind 0)) (crate-dep (name "spin") (req "^0.7.0") (default-features #t) (kind 0)))) (hash "04qb6q9al0h81b5v2dx2vd48qgm73r8mca9z6vvrjc6wvh9i0400")))

(define-public crate-web_random-0.1 (crate (name "web_random") (vers "0.1.4") (deps (list (crate-dep (name "js") (req ">=0.2.0, <0.3.0") (default-features #t) (kind 0)))) (hash "1jdhm6m1ajm62y5py5ly7qgd68x31fj0py9xnqjrh3f8hdr5vw8d")))

(define-public crate-web_random-0.1 (crate (name "web_random") (vers "0.1.5") (deps (list (crate-dep (name "js") (req "^0") (default-features #t) (kind 0)))) (hash "1b56nwkc8d2c7g8x3lqjzcxbdshi1h94gqdvhbx8sykznj27ds8z")))

(define-public crate-web_server-0.1 (crate (name "web_server") (vers "0.1.0") (hash "0c0rj7l9dzfqczyw42b71n99v2phfvx2hwic683vj86rb4zhqx9z")))

(define-public crate-web_server-0.1 (crate (name "web_server") (vers "0.1.1") (hash "1h4k604d3l6ka4g7jw0aab0d3lww5k53jxp3kyx14iyzix3107qp")))

(define-public crate-web_server-0.1 (crate (name "web_server") (vers "0.1.11") (hash "0cbg75qfnhqibfwlsc0y07h4k7a1za5cxqpvp7zaaqqlr0p57cwn")))

(define-public crate-web_server-0.1 (crate (name "web_server") (vers "0.1.2") (hash "0v7jajxqzd7bxi0mi6dspfkbkwm9m8wg5ad1vp99pnhbqhjrwhsz")))

(define-public crate-web_server-0.1 (crate (name "web_server") (vers "0.1.20") (hash "1k6bcxzlxaiblfz4dak38nmmx3bak3adcrhmlgjws2ypd760p9m5")))

(define-public crate-web_server-0.1 (crate (name "web_server") (vers "0.1.30") (hash "128x3jh7v7dl52dwys1wjj0qcgski5g15xi51iq1j5v0m8fnr9vq")))

(define-public crate-web_server-0.2 (crate (name "web_server") (vers "0.2.1") (deps (list (crate-dep (name "cargo-husky") (req "^1") (features (quote ("precommit-hook" "run-cargo-clippy"))) (kind 2)))) (hash "04aynpiv3451jn131aw5mcypkzgb9i21qdql1iq5rqj2gwf9q34s")))

(define-public crate-web_server-0.2 (crate (name "web_server") (vers "0.2.2") (deps (list (crate-dep (name "cargo-husky") (req "^1") (features (quote ("precommit-hook" "run-cargo-clippy"))) (kind 2)))) (hash "10fifkz31c94qj70dhjn84rrnp0wn6grivdl15m4i8laiyhvryz1")))

(define-public crate-web_server-0.3 (crate (name "web_server") (vers "0.3.0") (deps (list (crate-dep (name "cargo-husky") (req "^1") (features (quote ("precommit-hook" "run-cargo-clippy"))) (kind 2)))) (hash "0adkay3azmcr9grp682sky7n45f3piavgmnzdcv11jmljawx6wbg")))

(define-public crate-web_server-0.3 (crate (name "web_server") (vers "0.3.1") (deps (list (crate-dep (name "cargo-husky") (req "^1") (features (quote ("precommit-hook" "run-cargo-clippy"))) (kind 2)))) (hash "08jw6ybk9in3b5zb71a9pmzpar07djymh6ahf4w9sp1m3hd33bi5")))

(define-public crate-web_server-0.3 (crate (name "web_server") (vers "0.3.2") (deps (list (crate-dep (name "cargo-husky") (req "^1") (features (quote ("precommit-hook" "run-cargo-clippy"))) (kind 2)))) (hash "0w8v1xs1v926877j7dz2mfvs05717w9kg89vdj9ml71h8rdnw89s")))

(define-public crate-web_server-0.3 (crate (name "web_server") (vers "0.3.3") (deps (list (crate-dep (name "cargo-husky") (req "^1") (features (quote ("precommit-hook" "run-cargo-clippy"))) (kind 2)))) (hash "16a14hzmh8mmkkc9bp6rhw9wy3awjndj55x1pswj2llpkrcq34nb")))

(define-public crate-web_server-0.3 (crate (name "web_server") (vers "0.3.4") (deps (list (crate-dep (name "cargo-husky") (req "^1") (features (quote ("precommit-hook" "run-cargo-clippy"))) (kind 2)))) (hash "17fzwrwznc6yx0yyqlwq6y59867f3zacwh9kzznpqg904ygzrz06")))

(define-public crate-web_server-0.3 (crate (name "web_server") (vers "0.3.41") (deps (list (crate-dep (name "cargo-husky") (req "^1") (features (quote ("precommit-hook" "run-cargo-clippy"))) (kind 2)))) (hash "1acy2f86821253fccwmnld1wfw4iy4yg8p6j4gn17khdb5rk6j55")))

(define-public crate-web_server-0.4 (crate (name "web_server") (vers "0.4.0") (deps (list (crate-dep (name "cargo-husky") (req "^1") (features (quote ("precommit-hook" "run-cargo-clippy"))) (kind 2)))) (hash "1gk7w8l3ci9gxm7df85il4pfbskfqnmna6d25m5y74mibv1md4m4")))

(define-public crate-web_server-0.4 (crate (name "web_server") (vers "0.4.1") (deps (list (crate-dep (name "cargo-husky") (req "^1") (features (quote ("precommit-hook" "run-cargo-clippy"))) (kind 2)))) (hash "10x6i35mg3fjsv2v8nr3ymvwffj713ihm1h63pdqz7dafq546kb8")))

(define-public crate-web_server-0.4 (crate (name "web_server") (vers "0.4.2") (deps (list (crate-dep (name "cargo-husky") (req "^1") (features (quote ("precommit-hook" "run-cargo-clippy"))) (kind 2)))) (hash "10k6nqmawh508il44kd58y93k15hlkcfjp9761fy636a52mjc0vj")))

(define-public crate-web_stress_tools-0.5 (crate (name "web_stress_tools") (vers "0.5.8") (deps (list (crate-dep (name "addr") (req "^0.15.6") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^4.3.11") (default-features #t) (kind 0)) (crate-dep (name "confy") (req "^0.5.1") (default-features #t) (kind 0)) (crate-dep (name "console") (req "^0.15.7") (default-features #t) (kind 0)) (crate-dep (name "indicatif") (req "^0.17.5") (default-features #t) (kind 0)) (crate-dep (name "no_browser") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.164") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0.164") (default-features #t) (kind 0)) (crate-dep (name "url") (req "^2.4.0") (default-features #t) (kind 0)))) (hash "1dy2pykkb6zh0x8i6k18blvb79wdwgbp4b7b2zwcixg85spdn6zs")))

(define-public crate-web_stress_tools-0.5 (crate (name "web_stress_tools") (vers "0.5.9") (deps (list (crate-dep (name "addr") (req "^0.15.6") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^4.3.11") (default-features #t) (kind 0)) (crate-dep (name "confy") (req "^0.5.1") (default-features #t) (kind 0)) (crate-dep (name "console") (req "^0.15.7") (default-features #t) (kind 0)) (crate-dep (name "indicatif") (req "^0.17.5") (default-features #t) (kind 0)) (crate-dep (name "no_browser") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.164") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0.164") (default-features #t) (kind 0)) (crate-dep (name "url") (req "^2.4.0") (default-features #t) (kind 0)))) (hash "16jximp87sva8vmsr6jvfqqpyp8r7avds3y5kvyikhgr8sdxn6w1")))

(define-public crate-web_stress_tools-0.5 (crate (name "web_stress_tools") (vers "0.5.10") (deps (list (crate-dep (name "addr") (req "^0.15.6") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^4.3.11") (default-features #t) (kind 0)) (crate-dep (name "confy") (req "^0.5.1") (default-features #t) (kind 0)) (crate-dep (name "console") (req "^0.15.7") (default-features #t) (kind 0)) (crate-dep (name "indicatif") (req "^0.17.5") (default-features #t) (kind 0)) (crate-dep (name "no_browser") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.164") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0.164") (default-features #t) (kind 0)) (crate-dep (name "url") (req "^2.4.0") (default-features #t) (kind 0)))) (hash "1vkbhack75mrqh4ahh8jpamykihal3kvs7nbs9zrhqazk5cfsf7n")))

(define-public crate-web_stress_tools-0.6 (crate (name "web_stress_tools") (vers "0.6.0") (deps (list (crate-dep (name "addr") (req "^0.15.6") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^4.3.11") (default-features #t) (kind 0)) (crate-dep (name "confy") (req "^0.5.1") (default-features #t) (kind 0)) (crate-dep (name "console") (req "^0.15.7") (default-features #t) (kind 0)) (crate-dep (name "indicatif") (req "^0.17.5") (default-features #t) (kind 0)) (crate-dep (name "no_browser") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.164") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0.164") (default-features #t) (kind 0)) (crate-dep (name "url") (req "^2.4.0") (default-features #t) (kind 0)))) (hash "1w9zpiid04rqlmqfg765vikychazs160fi76yapvbsmlyf3kjj1s")))

(define-public crate-web_stress_tools-0.6 (crate (name "web_stress_tools") (vers "0.6.1") (deps (list (crate-dep (name "addr") (req "^0.15.6") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^4.3.11") (default-features #t) (kind 0)) (crate-dep (name "confy") (req "^0.5.1") (default-features #t) (kind 0)) (crate-dep (name "console") (req "^0.15.7") (default-features #t) (kind 0)) (crate-dep (name "indicatif") (req "^0.17.5") (default-features #t) (kind 0)) (crate-dep (name "no_browser") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.164") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0.164") (default-features #t) (kind 0)) (crate-dep (name "url") (req "^2.4.0") (default-features #t) (kind 0)))) (hash "01c1hjfbr2kh7d6v53lbm53r0axak31cawiisslvdwshgwgd9cck")))

(define-public crate-web_timer-0.0.0 (crate (name "web_timer") (vers "0.0.0") (deps (list (crate-dep (name "js_ffi") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "0n4fys07qsnrcpxbh38335pgxdga1gzpqd61x2ycv4d5lp6kjnkc")))

(define-public crate-web_timer-0.0.1 (crate (name "web_timer") (vers "0.0.1") (deps (list (crate-dep (name "js_ffi") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "1fpaq9sldyjdzxpm141f5n5nhqwjr0k4jsqnr65p9nmljwglw129")))

(define-public crate-web_timer-0.0.2 (crate (name "web_timer") (vers "0.0.2") (deps (list (crate-dep (name "js_ffi") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "1d8y6j7k9ri6phn1p3jbfzixas7rx58i5w26f7w9imcxzhzyq91s")))

(define-public crate-web_timer-0.1 (crate (name "web_timer") (vers "0.1.0") (deps (list (crate-dep (name "js_ffi") (req "^0.1") (default-features #t) (kind 0)))) (hash "0s8vyp28qfizdfd9qjcz4q4h03dqvfb30v7kq9ap2pr6x0gil73a")))

(define-public crate-web_timer-0.1 (crate (name "web_timer") (vers "0.1.1") (deps (list (crate-dep (name "js_ffi") (req "^0.1") (default-features #t) (kind 0)))) (hash "0hzbp03adj1vqiyhqcd9f51q639xzxbflykzabkaa5viglcpcsqb")))

(define-public crate-web_timer-0.1 (crate (name "web_timer") (vers "0.1.2") (deps (list (crate-dep (name "js_ffi") (req "^0.5") (default-features #t) (kind 0)))) (hash "07fric5a683v3znj2qw8qz0v5jxvhkp0p1wch772pkkb534vf738")))

(define-public crate-web_timer-0.1 (crate (name "web_timer") (vers "0.1.3") (deps (list (crate-dep (name "js_ffi") (req "^0.5") (default-features #t) (kind 0)))) (hash "1mj2fkmc4lrzjdxz40g6mc8djsazksb7c6i62hsf4id78wc5nkjm")))

(define-public crate-web_timer-0.1 (crate (name "web_timer") (vers "0.1.4") (deps (list (crate-dep (name "js_ffi") (req "^0.6") (default-features #t) (kind 0)))) (hash "0vjy6frvyqd6qjs9v3fb7wbz25j2c9rnfki78187ijgfsh1whin0")))

(define-public crate-web_timer-0.1 (crate (name "web_timer") (vers "0.1.5") (deps (list (crate-dep (name "js_ffi") (req "^0.8") (default-features #t) (kind 0)))) (hash "0q53r0jv3s9ixrhczqyi2s1rp482bsgzabdsz505dbf2z4n8xqyq")))

(define-public crate-web_timer-0.2 (crate (name "web_timer") (vers "0.2.0") (deps (list (crate-dep (name "callback") (req "^0.5") (default-features #t) (kind 0)) (crate-dep (name "globals") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "js") (req "^0.2") (default-features #t) (kind 0)))) (hash "010cl1qydg671i3wy6j64l8yjsv6mkr09q8pam4zkik9j02zbr5r")))

(define-public crate-web_timer-0.2 (crate (name "web_timer") (vers "0.2.1") (deps (list (crate-dep (name "callback") (req "^0.5.0") (default-features #t) (kind 0)) (crate-dep (name "js") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "once_cell") (req "^1.5.2") (default-features #t) (kind 0)) (crate-dep (name "spin") (req "^0.7.0") (default-features #t) (kind 0)))) (hash "0r717lpm7wiqv7vlbqvhq9xvska4d0cjh228ijafplpipiay0qgs")))

(define-public crate-web_timer-0.2 (crate (name "web_timer") (vers "0.2.2") (deps (list (crate-dep (name "callback") (req "^0.5.0") (default-features #t) (kind 0)) (crate-dep (name "js") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1") (features (quote ("spin_no_std"))) (kind 0)) (crate-dep (name "spin") (req "^0.7.0") (default-features #t) (kind 0)))) (hash "1wr2nj9jgvnnlm6majrsmwlhdxkjqk5ci2la2xl5zh0m1akndjmq")))

(define-public crate-web_timer-0.2 (crate (name "web_timer") (vers "0.2.3") (deps (list (crate-dep (name "callback") (req ">=0.5.0, <0.6.0") (default-features #t) (kind 0)) (crate-dep (name "js") (req ">=0.2.0, <0.3.0") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req ">=1.0.0, <2.0.0") (features (quote ("spin_no_std"))) (kind 0)) (crate-dep (name "spin") (req ">=0.7.0, <0.8.0") (default-features #t) (kind 0)))) (hash "170pl3rs14pb06x5h15r30q4bqasqcy9aj83k8y637dcli7h31ls")))

(define-public crate-web_timer-0.2 (crate (name "web_timer") (vers "0.2.4") (deps (list (crate-dep (name "js") (req ">=0.2.0, <0.3.0") (default-features #t) (kind 0)))) (hash "1a7xr1vkg4qn1vak8j6sin3l5rhf3f9pxa5r281m3mmfpwhasynl")))

(define-public crate-web_timer-0.2 (crate (name "web_timer") (vers "0.2.5") (deps (list (crate-dep (name "js") (req "^0") (default-features #t) (kind 0)))) (hash "13srbndvxg0x7bcgskw8dkldycmk32la8q1968j8x9v4q5yhkp8j")))

(define-public crate-web_token-0.1 (crate (name "web_token") (vers "0.1.0") (deps (list (crate-dep (name "rand") (req "^0.5.4") (default-features #t) (kind 0)))) (hash "1bxmpi73nj440x5hx34fhr3m3hi4s6njnkhma1nv4bp6mazjqkhk")))

(define-public crate-web_token-0.1 (crate (name "web_token") (vers "0.1.1") (deps (list (crate-dep (name "rand") (req "^0.5.4") (default-features #t) (kind 0)))) (hash "0mkdq4c0jq17ddsy5qd7qrrc0ij57qc7w66s0y98bbn1xrxbmc6v")))

(define-public crate-web_token-0.2 (crate (name "web_token") (vers "0.2.0") (deps (list (crate-dep (name "rand") (req "^0.5.4") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.70") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0.70") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.24") (default-features #t) (kind 2)))) (hash "1kj1xs8qj9618kb29x30h4r3g9qaix2kcvg4h2w3g5n4aqmxn7ga") (features (quote (("serde_support" "serde" "serde_derive"))))))

(define-public crate-web_token-0.3 (crate (name "web_token") (vers "0.3.0") (deps (list (crate-dep (name "diesel") (req "^1.3.2") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "diesel-derive-newtype") (req "^0.1.1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.5.4") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.70") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0.70") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.24") (default-features #t) (kind 2)))) (hash "12h7hwm30m374h41rfpgsydqf43bipqiyqdmpiycxmx6lvk70c54") (features (quote (("serde_support" "serde" "serde_derive") ("diesel_support" "diesel" "diesel-derive-newtype"))))))

(define-public crate-web_token-1 (crate (name "web_token") (vers "1.0.0") (deps (list (crate-dep (name "diesel") (req "^2.0.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "diesel-derive-newtype") (req "^2.0.0-rc.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.70") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0.70") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.24") (default-features #t) (kind 2)))) (hash "02af7wm63y7gk53sd63i9hjlrclbnm1y2y3g8s6gj661f2ns5pan") (features (quote (("serde_support" "serde" "serde_derive") ("diesel_support" "diesel" "diesel-derive-newtype"))))))

(define-public crate-web_wars_engine-0.1 (crate (name "web_wars_engine") (vers "0.1.0") (deps (list (crate-dep (name "arrayref") (req "^0.3.5") (default-features #t) (kind 0)) (crate-dep (name "basic-pathfinding") (req "^0.2.1") (default-features #t) (kind 0)) (crate-dep (name "console_error_panic_hook") (req "^0.1.1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "env_logger") (req "^0.6.1") (default-features #t) (kind 0)) (crate-dep (name "fake") (req "^1.3.0") (default-features #t) (kind 0)) (crate-dep (name "libmath") (req "^0.2.1") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.6") (default-features #t) (kind 0)) (crate-dep (name "num") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "num-derive") (req "^0.2.5") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2.8") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.6.5") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.59") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0.59") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_repr") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "sha2") (req "^0.8.0") (default-features #t) (kind 0)) (crate-dep (name "uuid") (req "^0.7") (features (quote ("serde" "v4" "wasm-bindgen"))) (default-features #t) (kind 0)) (crate-dep (name "wasm-bindgen") (req "^0.2") (features (quote ("serde-serialize"))) (default-features #t) (kind 0)) (crate-dep (name "wasm-bindgen-test") (req "^0.2") (default-features #t) (kind 2)) (crate-dep (name "wee_alloc") (req "^0.4.2") (optional #t) (default-features #t) (kind 0)))) (hash "19vpmlm1ijg1s1apvlm12zpk1ngwsirp992qbaxgv6s4zqx0f70z") (features (quote (("default" "console_error_panic_hook")))) (yanked #t)))

(define-public crate-web_worker-0.1 (crate (name "web_worker") (vers "0.1.0") (deps (list (crate-dep (name "console_error_panic_hook") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "futures") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "js-sys") (req "^0.3.24") (default-features #t) (kind 0)) (crate-dep (name "rayon") (req "^1.1.0") (default-features #t) (kind 0)) (crate-dep (name "rayon-core") (req "^1.5.0") (default-features #t) (kind 0)) (crate-dep (name "wasm-bindgen") (req "^0.2.47") (features (quote ("serde-serialize"))) (default-features #t) (kind 0)) (crate-dep (name "wasm-bindgen-futures") (req "^0.3.24") (default-features #t) (kind 0)) (crate-dep (name "web-sys") (req "^0.3.4") (features (quote ("CanvasRenderingContext2d" "ErrorEvent" "Event" "ImageData" "Navigator" "Window" "Worker" "DedicatedWorkerGlobalScope" "MessageEvent"))) (default-features #t) (kind 0)))) (hash "1lzffipb78rd74zad1cc4yrzx35p06wnzmvz3dpz3mpbha8083c5")))

(define-public crate-web_worker-0.2 (crate (name "web_worker") (vers "0.2.0") (deps (list (crate-dep (name "console_error_panic_hook") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "futures") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "js-sys") (req "^0.3.33") (default-features #t) (kind 0)) (crate-dep (name "rayon") (req "^1.1.0") (default-features #t) (kind 0)) (crate-dep (name "rayon-core") (req "^1.5.0") (default-features #t) (kind 0)) (crate-dep (name "wasm-bindgen") (req "^0.2.56") (features (quote ("serde-serialize"))) (default-features #t) (kind 0)) (crate-dep (name "wasm-bindgen-futures") (req "^0.4.6") (default-features #t) (kind 0)) (crate-dep (name "web-sys") (req "^0.3.4") (features (quote ("CanvasRenderingContext2d" "ErrorEvent" "Event" "ImageData" "Navigator" "Window" "Worker" "DedicatedWorkerGlobalScope" "MessageEvent"))) (default-features #t) (kind 0)))) (hash "0qp264va68v79p9v20h5jv830rx8hihgfcvlkfys633l9gvqf7xl")))

(define-public crate-web_worker-0.3 (crate (name "web_worker") (vers "0.3.0") (deps (list (crate-dep (name "console_error_panic_hook") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "futures") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "js-sys") (req "^0.3.33") (default-features #t) (kind 0)) (crate-dep (name "rayon") (req "^1.1.0") (default-features #t) (kind 0)) (crate-dep (name "rayon-core") (req "^1.5.0") (default-features #t) (kind 0)) (crate-dep (name "wasm-bindgen") (req "^0.2.56") (features (quote ("serde-serialize"))) (default-features #t) (kind 0)) (crate-dep (name "wasm-bindgen-futures") (req "^0.4.6") (default-features #t) (kind 0)) (crate-dep (name "web-sys") (req "^0.3.4") (features (quote ("CanvasRenderingContext2d" "ErrorEvent" "Event" "ImageData" "Navigator" "Window" "Worker" "DedicatedWorkerGlobalScope" "MessageEvent"))) (default-features #t) (kind 0)))) (hash "1rdvlspwnlpijkcarxj6ackwgdx26zg3yn0llm4ipd1gis9s6ygg")))

