(define-module (crates-io we pa) #:use-module (crates-io))

(define-public crate-wepack-0.0.0 (crate (name "wepack") (vers "0.0.0") (hash "1dmm6dbk9llf8avm63nr9awfqbhsxhbdnzgclalzwwm0z6chlp9y")))

(define-public crate-wepay-0.1 (crate (name "wepay") (vers "0.1.0") (hash "17azm0z3pd74ha0bicmwp1v5n9hip8f3jckc29k75723g6vr7pfl")))

