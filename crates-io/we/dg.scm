(define-module (crates-io we dg) #:use-module (crates-io))

(define-public crate-wedge-0.0.0 (crate (name "wedge") (vers "0.0.0") (hash "0ilpkswfqnig819r5bsan4cjvcwpksgfw8lw3wfqk9njmcn6fwpf")))

(define-public crate-wedge-rs-0.0.0 (crate (name "wedge-rs") (vers "0.0.0") (hash "0j1c80d41ipqmj2wyppimrp2wzbwkm00k88c5xv64rwhiyyzdaqc")))

(define-public crate-wedged-0.1 (crate (name "wedged") (vers "0.1.0") (deps (list (crate-dep (name "approx") (req "^0.5") (default-features #t) (kind 0)) (crate-dep (name "derivative") (req "^2.2") (default-features #t) (kind 0)) (crate-dep (name "nalgebra") (req "^0.30") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "rayon") (req "^1.5") (default-features #t) (kind 2)) (crate-dep (name "typenum") (req "^1.15") (default-features #t) (kind 0)))) (hash "1qivdmg9vw81fp8r29jrl81nckk3dl77slsfaqiajvmy4q7jqqaq") (features (quote (("fn_traits"))))))

