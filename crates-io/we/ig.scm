(define-module (crates-io we ig) #:use-module (crates-io))

(define-public crate-weight-cache-0.1 (crate (name "weight-cache") (vers "0.1.0") (deps (list (crate-dep (name "linked-hash-map") (req "^0.5.4") (default-features #t) (kind 0)) (crate-dep (name "quickcheck") (req "^1.0.3") (default-features #t) (kind 2)) (crate-dep (name "quickcheck_macros") (req "^1.0.0") (default-features #t) (kind 2)))) (hash "0mqlpvm8wgfwvizdm6hwfg4hbyajpb919b245vfzxllds460cw50")))

(define-public crate-weight-cache-0.1 (crate (name "weight-cache") (vers "0.1.1") (deps (list (crate-dep (name "linked-hash-map") (req "^0.5.4") (default-features #t) (kind 0)) (crate-dep (name "quickcheck") (req "^1.0.3") (default-features #t) (kind 2)) (crate-dep (name "quickcheck_macros") (req "^1.0.0") (default-features #t) (kind 2)))) (hash "11sqg32anvalydn7zfmbypw6ndp4ajjnxphzgx6r7lkypys2xj6f")))

(define-public crate-weight-cache-0.1 (crate (name "weight-cache") (vers "0.1.2") (deps (list (crate-dep (name "linked-hash-map") (req "^0.5.4") (default-features #t) (kind 0)) (crate-dep (name "quickcheck") (req "^1.0.3") (default-features #t) (kind 2)) (crate-dep (name "quickcheck_macros") (req "^1.0.0") (default-features #t) (kind 2)))) (hash "0vg87lkdnl053397wx6ckgivj7vfjp97ncild17h98iimy169b6y")))

(define-public crate-weight-cache-0.2 (crate (name "weight-cache") (vers "0.2.0") (deps (list (crate-dep (name "linked-hash-map") (req "^0.5.4") (default-features #t) (kind 0)) (crate-dep (name "quickcheck") (req "^1.0.3") (default-features #t) (kind 2)) (crate-dep (name "quickcheck_macros") (req "^1.0.0") (default-features #t) (kind 2)))) (hash "12bvnri0x47wzfwb3373j2a1h178br1q0ii4kpyagscph58718l0")))

(define-public crate-weight-cache-0.2 (crate (name "weight-cache") (vers "0.2.1") (deps (list (crate-dep (name "linked-hash-map") (req "^0.5.4") (default-features #t) (kind 0)) (crate-dep (name "quickcheck") (req "^1.0.3") (default-features #t) (kind 2)) (crate-dep (name "quickcheck_macros") (req "^1.0.0") (default-features #t) (kind 2)))) (hash "1il37gggarfqwhls7784r117lad6qs4hhsayyzck7m4kcmmxb4k7")))

(define-public crate-weight-cache-0.2 (crate (name "weight-cache") (vers "0.2.2") (deps (list (crate-dep (name "linked-hash-map") (req "^0.5.4") (default-features #t) (kind 0)) (crate-dep (name "prometheus") (req "^0.12.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "quickcheck") (req "^1.0.3") (default-features #t) (kind 2)) (crate-dep (name "quickcheck_macros") (req "^1.0.0") (default-features #t) (kind 2)))) (hash "0ksk2syh1lzqym4ylrwiqlik4bckn8wxlrczbcacki9x2ch5m7qq") (features (quote (("metrics" "prometheus") ("default"))))))

(define-public crate-weight-cache-0.2 (crate (name "weight-cache") (vers "0.2.3") (deps (list (crate-dep (name "linked-hash-map") (req "^0.5.4") (default-features #t) (kind 0)) (crate-dep (name "prometheus") (req "^0.12.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "quickcheck") (req "^1.0.3") (default-features #t) (kind 2)) (crate-dep (name "quickcheck_macros") (req "^1.0.0") (default-features #t) (kind 2)))) (hash "00bjzc31i07vlxrsd8i228b7lw6magccpbyqprr82abg78cm0vss") (features (quote (("metrics" "prometheus") ("default"))))))

(define-public crate-weight-gen-0.0.0 (crate (name "weight-gen") (vers "0.0.0") (hash "062m6hb78cd7lj3r3ss2wh8nr7awnva7kwx637kb3spbs26bshs6") (yanked #t)))

(define-public crate-weight-meter-procedural-0.0.0 (crate (name "weight-meter-procedural") (vers "0.0.0") (hash "00gpp2sfy4hjaniy18ks1izavh3xcd8dkpdmblay4a929jlfr9hw") (yanked #t)))

(define-public crate-weighted-median-0.1 (crate (name "weighted-median") (vers "0.1.0") (deps (list (crate-dep (name "is_sorted") (req "^0.1.1") (default-features #t) (kind 0)))) (hash "19kbbrxmrm3svh5088gp9rm7cp3f88jhzb9vqybvk1fpgjkd3wgq")))

(define-public crate-weighted-median-0.2 (crate (name "weighted-median") (vers "0.2.0") (deps (list (crate-dep (name "is_sorted") (req "^0.1.1") (default-features #t) (kind 0)))) (hash "10kf72l0hpilmwlp2879qzjy7l1vr1fsi2w6b0kqa3ixf8800a5n")))

(define-public crate-weighted-median-0.3 (crate (name "weighted-median") (vers "0.3.0") (deps (list (crate-dep (name "is_sorted") (req "^0.1.1") (default-features #t) (kind 0)))) (hash "0k3yc40b7308x9w30yrckfwc6mb6zn4zvvsn9q3378vd3kh10mrd")))

(define-public crate-weighted-median-0.4 (crate (name "weighted-median") (vers "0.4.0") (deps (list (crate-dep (name "criterion") (req "^0.3") (default-features #t) (kind 2)))) (hash "1gim7yw8q27v3yf100518qd86w463bp8wwap162b1nnm3jflbv5v")))

(define-public crate-weighted-median-0.5 (crate (name "weighted-median") (vers "0.5.0") (deps (list (crate-dep (name "criterion") (req "^0.3") (default-features #t) (kind 2)))) (hash "0w63wv5ykyaapkgxkwikywxkag3ajvip80a6fybnh16c3l6zvqv3")))

(define-public crate-weighted-median-0.6 (crate (name "weighted-median") (vers "0.6.0") (deps (list (crate-dep (name "criterion") (req "^0.3") (default-features #t) (kind 2)))) (hash "0rdfj2c337b3b1v42icv5jxssadbn5rwagv3vbf3y3v57gqyi93q")))

(define-public crate-weighted-median-0.7 (crate (name "weighted-median") (vers "0.7.0") (deps (list (crate-dep (name "criterion") (req "^0.3") (default-features #t) (kind 2)))) (hash "0mgp0491423wcsibxjij2ya95mc0pwin5sgria5bmb7q0dyy410j")))

(define-public crate-weighted-regexp-0.1 (crate (name "weighted-regexp") (vers "0.1.0") (deps (list (crate-dep (name "num-traits") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "quickcheck") (req "^0.2") (default-features #t) (kind 2)) (crate-dep (name "quickcheck_macros") (req "^0.2") (default-features #t) (kind 2)))) (hash "1dacbswzmi47v06j7g3dl23pmzvgq5bd9pj03h1mg2aarahgcy2j")))

(define-public crate-weighted-rs-0.1 (crate (name "weighted-rs") (vers "0.1.0") (hash "0g056q37gnbagzjdqqxl5rbcml3hff0d6186fim9hwnysxvzhqyn")))

(define-public crate-weighted-rs-0.1 (crate (name "weighted-rs") (vers "0.1.1") (deps (list (crate-dep (name "rand") (req "^0.7.0") (default-features #t) (kind 0)))) (hash "0f11xg3qm3vfrvvh6xg8nw19cf78vhs744hiap01h0jp7n2f61n6")))

(define-public crate-weighted-rs-0.1 (crate (name "weighted-rs") (vers "0.1.2") (deps (list (crate-dep (name "rand") (req "^0.7.0") (default-features #t) (kind 0)))) (hash "1920xpniyix8vnjm79m6rbq0zr614w1acah0h85yzqmbj9cfvvcr")))

(define-public crate-weighted-rs-0.1 (crate (name "weighted-rs") (vers "0.1.3") (deps (list (crate-dep (name "rand") (req "^0.8.4") (default-features #t) (kind 0)))) (hash "08qcqclxpmz1h7x28vcnxc23k14yxhjwqpwxzppxwv6qp1r2hnrn")))

(define-public crate-weighted-rs-1-0.1 (crate (name "weighted-rs-1") (vers "0.1.4") (hash "026qy31ww69ph44jm0yd4xbx15nqj4k4ppcsmdr8dwicn6mams0j")))

(define-public crate-weighted-rs-wasm-0.1 (crate (name "weighted-rs-wasm") (vers "0.1.3") (hash "16vc4j6k9in2chkcpdnq9rjclb7xca0mdgddsfw2czyzkqcy3p6q")))

(define-public crate-weighted-rs-wasm-0.1 (crate (name "weighted-rs-wasm") (vers "0.1.4") (hash "1w6md142i1lin1g0zv5qsrirwz83xdib64kfmvsllv91zphsdvbz")))

(define-public crate-weighted-select-0.1 (crate (name "weighted-select") (vers "0.1.0") (deps (list (crate-dep (name "futures") (req "^0.1.26") (default-features #t) (kind 0)) (crate-dep (name "quickcheck") (req "^0.8.2") (default-features #t) (kind 2)) (crate-dep (name "quickcheck_macros") (req "^0.8.0") (default-features #t) (kind 2)))) (hash "0i2alhwkyqf1pby5qjh04rx7fms0dnmwns86jwhp7vhmic763jsk")))

(define-public crate-weighted-select-0.1 (crate (name "weighted-select") (vers "0.1.1") (deps (list (crate-dep (name "futures") (req "^0.1.26") (default-features #t) (kind 0)) (crate-dep (name "quickcheck") (req "^0.8.2") (default-features #t) (kind 2)) (crate-dep (name "quickcheck_macros") (req "^0.8.0") (default-features #t) (kind 2)))) (hash "0rf2pf9g98xfjr0bnivr0lijwa8r77g5rcpk9ad2cj2mic1k6phl")))

(define-public crate-weighted_levenshtein-0.1 (crate (name "weighted_levenshtein") (vers "0.1.0") (deps (list (crate-dep (name "unicode-segmentation") (req "^1.3.0") (default-features #t) (kind 2)))) (hash "1ldf4330yym2hpmx8c4zggllz53ppdw1k30giii29bg6z2chkc0d") (features (quote (("default-weight"))))))

(define-public crate-weighted_levenshtein-0.2 (crate (name "weighted_levenshtein") (vers "0.2.0") (deps (list (crate-dep (name "unicode-segmentation") (req "^1.3.0") (default-features #t) (kind 2)))) (hash "16zdrc2cczfyhfwik0b7px6nd66ia61rnxdp0mjpv7cm86y06c46") (features (quote (("default-weight"))))))

(define-public crate-weighted_rand-0.1 (crate (name "weighted_rand") (vers "0.1.0") (deps (list (crate-dep (name "anyhow") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "0nbl51ydwbjyqhmb3qym37rwc6mfprgapqynfbi1j9j5x6nn2wk2")))

(define-public crate-weighted_rand-0.2 (crate (name "weighted_rand") (vers "0.2.0") (deps (list (crate-dep (name "rand") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "04bvakpn3zz8cka29fc8qzfag8k1mdfcw56gy695xp9axw2xrkvv")))

(define-public crate-weighted_rand-0.3 (crate (name "weighted_rand") (vers "0.3.0") (deps (list (crate-dep (name "rand") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "1dcd1gnyc4y02kq07l66ykhf2h1pb3596z5f3yim7zy2yqpxg8qg")))

(define-public crate-weighted_rand-0.3 (crate (name "weighted_rand") (vers "0.3.1") (deps (list (crate-dep (name "criterion") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "1srq9n3fb8jna2sj93ycix08md227d8v81lwslx2gy68r1cb1wfb")))

(define-public crate-weighted_rand-0.3 (crate (name "weighted_rand") (vers "0.3.2") (deps (list (crate-dep (name "criterion") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "1gpga6wqy1ra1s8y191lbm8wcjv3m85bql3l1q03f2wkyjxqz8qz")))

(define-public crate-weighted_rand-0.4 (crate (name "weighted_rand") (vers "0.4.0") (deps (list (crate-dep (name "criterion") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "1acpyz28ii65b20sf3vb3h0xr4slvr8zzy3f5i980bysrypdabhc") (yanked #t)))

(define-public crate-weighted_rand-0.4 (crate (name "weighted_rand") (vers "0.4.1") (deps (list (crate-dep (name "criterion") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "1dz73vgdxpshsa67l7cr7dpwwpdy8xmcinf1pj6vfd4gpdn4w6j3")))

(define-public crate-weighted_rand-0.4 (crate (name "weighted_rand") (vers "0.4.2") (deps (list (crate-dep (name "criterion") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "0vmiy0g5q2a1n834apr8xyrgp92ad8hn7i8yy19gf3j7m2rbdfca")))

(define-public crate-weighted_random_list-0.1 (crate (name "weighted_random_list") (vers "0.1.0") (deps (list (crate-dep (name "rand") (req "^0.6") (default-features #t) (kind 0)))) (hash "0l0dpkxgyf3xgki5raj25xni1y0npr01fymksdahz30pmf8grm89")))

(define-public crate-weighted_random_list-0.1 (crate (name "weighted_random_list") (vers "0.1.1") (deps (list (crate-dep (name "rand") (req "^0.6") (default-features #t) (kind 0)))) (hash "1wzg2h1hp2y41bcnc1zx3kfh9hcwvz7v1nmwvpcgbmi8dg9gsprr")))

(define-public crate-weighted_rate_limiter-0.1 (crate (name "weighted_rate_limiter") (vers "0.1.0") (deps (list (crate-dep (name "futures") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "queues") (req "^1.1") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8") (default-features #t) (kind 2)) (crate-dep (name "tokio") (req "^1.15") (features (quote ("time"))) (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.15") (features (quote ("full" "macros" "test-util"))) (default-features #t) (kind 2)))) (hash "0ln8blm8mbrpm3z9lcw2idlv69dylb1cw0r3l22a2s7vl6m6fijq")))

(define-public crate-weighted_rate_limiter-0.1 (crate (name "weighted_rate_limiter") (vers "0.1.1") (deps (list (crate-dep (name "futures") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "queues") (req "^1.1") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8") (default-features #t) (kind 2)) (crate-dep (name "tokio") (req "^1.15") (features (quote ("time"))) (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.15") (features (quote ("full" "macros" "test-util"))) (default-features #t) (kind 2)))) (hash "13qi9lgc0bxmbw9wpj46k455afvmy7snd0vmxqnsv9im6w9f28yn")))

(define-public crate-weighted_trie-0.1 (crate (name "weighted_trie") (vers "0.1.0") (hash "0y78m73sv2y4kyp5xgf9jppgglqkm13g7knr5hbd8920f35r02xb")))

(define-public crate-weighted_trie-0.1 (crate (name "weighted_trie") (vers "0.1.1") (hash "0cfjz9421l9rclgpix21j2z2ixjz55dnd5yxrn43nznb3l2q6iqb")))

(define-public crate-weighted_trie-0.1 (crate (name "weighted_trie") (vers "0.1.2") (deps (list (crate-dep (name "criterion") (req "^0.4.0") (default-features #t) (kind 2)) (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 2)))) (hash "0zn5pf0fy2sv6j4b9ifhvd9rl5fwgg544kkpw8li0175ls8hbv4k")))

(define-public crate-weighted_trie-0.1 (crate (name "weighted_trie") (vers "0.1.3") (deps (list (crate-dep (name "criterion") (req "^0.4.0") (default-features #t) (kind 2)) (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 2)))) (hash "09m75qxqmw7gdcqaqdp58caiahrb77f8csx6g4mj662rj2vlvs89")))

(define-public crate-weighted_trie-0.1 (crate (name "weighted_trie") (vers "0.1.4") (deps (list (crate-dep (name "criterion") (req "^0.4.0") (default-features #t) (kind 2)) (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 2)))) (hash "030w5fkpn3rj7l32knc0vdjj8fcngr95cpw7z3lbxqdm33c2brk6")))

(define-public crate-weighty-0.1 (crate (name "weighty") (vers "0.1.0") (deps (list (crate-dep (name "hidapi") (req "^1.2.3") (default-features #t) (kind 0)) (crate-dep (name "uom") (req "^0") (optional #t) (default-features #t) (kind 0)))) (hash "0xx8bbx1f3hsx531ngax7krrbris0n5k6sxj3ibn7a10p4c40al2") (features (quote (("units" "uom") ("default"))))))

(define-public crate-weighty-0.1 (crate (name "weighty") (vers "0.1.1") (deps (list (crate-dep (name "hidapi") (req "^1.2.3") (default-features #t) (kind 0)) (crate-dep (name "uom") (req "^0") (optional #t) (default-features #t) (kind 0)))) (hash "0pjkka9cb02lg53c78d30xm7z2y365fp8wb5wsmjlnblf77xnj1d") (features (quote (("units" "uom") ("default"))))))

