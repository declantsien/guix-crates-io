(define-module (crates-io we sc) #:use-module (crates-io))

(define-public crate-wesc-0.1 (crate (name "wesc") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^4.4.11") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "lol_html") (req "^1.2.1") (default-features #t) (kind 0)) (crate-dep (name "pretty_assertions") (req "^1.4.0") (default-features #t) (kind 2)))) (hash "0wq463zjx4p4rzgpfrnxl2092xmpyz6ryi9gyskpd8sv0yswvxss")))

(define-public crate-wesc-0.1 (crate (name "wesc") (vers "0.1.1") (deps (list (crate-dep (name "clap") (req "^4.4.11") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "lol_html") (req "^1.2.1") (default-features #t) (kind 0)) (crate-dep (name "pretty_assertions") (req "^1.4.0") (default-features #t) (kind 2)))) (hash "1cpp7ia62diwvc4adghj7jnjm6ymvv2fpd127l4rz3mwxfy32zc1")))

(define-public crate-wesc-0.2 (crate (name "wesc") (vers "0.2.0") (deps (list (crate-dep (name "clap") (req "^4.4.11") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "lol_html") (req "^1.2.1") (default-features #t) (kind 0)) (crate-dep (name "pretty_assertions") (req "^1.4.0") (default-features #t) (kind 2)))) (hash "030hjbc9jhxsia2xb3a1i4zacj1xb2viwqd33ldx8bcyip28bw1n")))

(define-public crate-wesc-0.2 (crate (name "wesc") (vers "0.2.1") (deps (list (crate-dep (name "clap") (req "^4.4.11") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "lol_html") (req "^1.2.1") (default-features #t) (kind 0)) (crate-dep (name "pretty_assertions") (req "^1.4.0") (default-features #t) (kind 2)))) (hash "1xgs8w14ffrdfk2gl2vr3lnhcyv29ig20xqxrpjjbabp8ai5xxhl")))

