(define-module (crates-io we rd) #:use-module (crates-io))

(define-public crate-werds-1 (crate (name "werds") (vers "1.0.0") (deps (list (crate-dep (name "assert_cmd") (req "^2.0") (default-features #t) (kind 2)) (crate-dep (name "clap") (req "^4.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "predicates") (req "^2.1") (default-features #t) (kind 2)))) (hash "0g9bs8jvkd2fpj0fp8d8ywjdsyqndfbmgmmq36185xhar61ilznc")))

