(define-module (crates-io we yl) #:use-module (crates-io))

(define-public crate-weyl-0.1 (crate (name "weyl") (vers "0.1.0") (deps (list (crate-dep (name "lazy_static") (req "^1.3") (default-features #t) (kind 0)))) (hash "1gpk894sx3bmhzh7kyi0b4lc6x9wlp452lmfw8b11zi9r03ng8v5")))

(define-public crate-weyl-0.1 (crate (name "weyl") (vers "0.1.1") (deps (list (crate-dep (name "lazy_static") (req "^1.3") (default-features #t) (kind 0)))) (hash "1rrnragggd0mq08pp5x507sc8smjq30ry6wxibif65z1qwk6np4p")))

(define-public crate-weyl-0.1 (crate (name "weyl") (vers "0.1.2") (deps (list (crate-dep (name "lazy_static") (req "^1.3") (default-features #t) (kind 0)))) (hash "1cggchcm4g9aly6ybc893mrpyhzcx2ndn9dvbj4x87lqq36spsga")))

(define-public crate-weyl-0.1 (crate (name "weyl") (vers "0.1.3") (deps (list (crate-dep (name "lazy_static") (req "^1.3") (default-features #t) (kind 0)))) (hash "0dsrbfapg3icagrmkl1jyvkczp27gid49scn7l0mni57858wfny5")))

