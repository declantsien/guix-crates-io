(define-module (crates-io we ng) #:use-module (crates-io))

(define-public crate-wenger-0.0.0 (crate (name "wenger") (vers "0.0.0") (hash "1hykdg2gx2wj9qqlwgmc2fd8xxrq1kiw3yf8ap0lvm0ncj3j3j12")))

(define-public crate-wenger-0.0.1 (crate (name "wenger") (vers "0.0.1") (hash "0rnwvfhkgbk24j6zf3ivb51xcy2f8m89chz4gmxn0178hrx2bnyi")))

(define-public crate-wenger-0.0.2 (crate (name "wenger") (vers "0.0.2") (hash "1njd56x4hnm6w04gddxc5am5wz5rczpr5xdz6gpm0q4077vpnk8n")))

(define-public crate-wenger-0.0.3 (crate (name "wenger") (vers "0.0.3") (hash "00xj9xalfrxn8ymlrli6nhsf92ggkz4b1bdpcjwwc7p0dvw2mk2k")))

