(define-module (crates-io we nd) #:use-module (crates-io))

(define-public crate-wender-0.0.1 (crate (name "wender") (vers "0.0.1") (hash "15zqz404aaa475w17wfijxxwk3iggxz3hx2lsggqvapci6y9c7r0")))

(define-public crate-wendy-k8-metadata-client-3 (crate (name "wendy-k8-metadata-client") (vers "3.3.1") (deps (list (crate-dep (name "async-trait") (req "^0.1.21") (default-features #t) (kind 0)) (crate-dep (name "futures-util") (req "^0.3.5") (default-features #t) (kind 0)) (crate-dep (name "k8-diff") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "k8-types") (req "^0.5.2") (default-features #t) (kind 0)) (crate-dep (name "pin-utils") (req "^0.1.0-alpha.4") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.103") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.40") (default-features #t) (kind 0)) (crate-dep (name "tracing") (req "^0.1.19") (default-features #t) (kind 0)))) (hash "02rk1kbim9jfcxb5c9zv7blq6cc4h95rrikjgi83bwras60hnfai")))

