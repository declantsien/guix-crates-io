(define-module (crates-io we po) #:use-module (crates-io))

(define-public crate-wepoll-0.0.0 (crate (name "wepoll") (vers "0.0.0") (hash "0hx28brfyhr2ma5y66prbv9c9rivxkmhjiaqis5j9x215yqsgqxc")))

(define-public crate-wepoll-binding-1 (crate (name "wepoll-binding") (vers "1.0.0") (deps (list (crate-dep (name "bitflags") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "wepoll-sys") (req "^1.0") (default-features #t) (kind 0)))) (hash "0dhayan3313f2796cjzzsxlrqp3rgq4n8my1r7d0rx7iylirsb3p")))

(define-public crate-wepoll-binding-1 (crate (name "wepoll-binding") (vers "1.0.1") (deps (list (crate-dep (name "bitflags") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "wepoll-sys") (req "^1.0") (default-features #t) (kind 0)))) (hash "14086sji8s09cva4blkb6lh900p8w2xzy1scgd7380jkvdjdzbvq")))

(define-public crate-wepoll-binding-1 (crate (name "wepoll-binding") (vers "1.0.2") (deps (list (crate-dep (name "bitflags") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "wepoll-sys") (req "^1.0") (default-features #t) (kind 0)))) (hash "0hk1pwkzm6skvhsci1sgzjyi3ipb4m8b9084yvw08kgv2xcxcmgr")))

(define-public crate-wepoll-binding-1 (crate (name "wepoll-binding") (vers "1.0.3") (deps (list (crate-dep (name "bitflags") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "wepoll-sys") (req "^1.0.3") (default-features #t) (kind 0)))) (hash "0q15ch3zvhkhrrnps5r1yrd29vic3hhfm6xjgw7ahw9psslcxrpf")))

(define-public crate-wepoll-binding-1 (crate (name "wepoll-binding") (vers "1.0.4") (deps (list (crate-dep (name "bitflags") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "wepoll-sys") (req "^1.0.3") (default-features #t) (kind 0)))) (hash "0a9x1jd37gk0ana7j5kas6kfri3glkk94m5l45shn6r9z7hps3c6")))

(define-public crate-wepoll-binding-1 (crate (name "wepoll-binding") (vers "1.0.5") (deps (list (crate-dep (name "bitflags") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "wepoll-sys") (req "^1.0.4") (default-features #t) (kind 0)))) (hash "0iy5ycqp2pr3fxqs87jj28833zmi2jn1xrd3ik0yvr7pdiadimrg")))

(define-public crate-wepoll-binding-1 (crate (name "wepoll-binding") (vers "1.1.0") (deps (list (crate-dep (name "bitflags") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "wepoll-sys") (req "^1.0.4") (default-features #t) (kind 0)))) (hash "16w8j28qxhxhl03zq8zzvhir4kfsqyh1jch1ql1c6g2jx9g20nbp")))

(define-public crate-wepoll-binding-2 (crate (name "wepoll-binding") (vers "2.0.0") (deps (list (crate-dep (name "bitflags") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "wepoll-sys") (req "^2.0") (default-features #t) (kind 0)))) (hash "1idgw46bg426xf20xisy9vkgdlz40rwmzhdvqybp03vlc8kca33j")))

(define-public crate-wepoll-binding-2 (crate (name "wepoll-binding") (vers "2.0.1") (deps (list (crate-dep (name "bitflags") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "wepoll-sys") (req "^2.0") (default-features #t) (kind 0)))) (hash "1356lfs41cjb3a8izy8n7sybw5a9jxidrmkzc2x2rapdj96h24bm")))

(define-public crate-wepoll-binding-2 (crate (name "wepoll-binding") (vers "2.0.2") (deps (list (crate-dep (name "bitflags") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "wepoll-sys") (req "^2.0") (default-features #t) (kind 0)))) (hash "19wri7a6fngzhrhi731dcdil1i2n676vl50dmnvgh7vhz57zykrp")))

(define-public crate-wepoll-binding-3 (crate (name "wepoll-binding") (vers "3.0.0") (deps (list (crate-dep (name "bitflags") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "wepoll-sys") (req "^3.0") (default-features #t) (kind 0)))) (hash "0p3s55nqa0ydmhg7nmdfyyyhw9m25gh6h36lx6v7hh7751z7j4qd")))

(define-public crate-wepoll-ffi-0.1 (crate (name "wepoll-ffi") (vers "0.1.0") (deps (list (crate-dep (name "cc") (req "^1.0") (default-features #t) (kind 1)))) (hash "0vsx2p4j6w9a9hwifx83p7grw2sv813q5l5cbqns56a3r6ibxhvv")))

(define-public crate-wepoll-ffi-0.1 (crate (name "wepoll-ffi") (vers "0.1.1") (deps (list (crate-dep (name "cc") (req "^1.0") (default-features #t) (kind 1)))) (hash "1ss49h1qm665l06pfnd4qm5qphpf49223d3f0a00zcp84qvd5v5h") (features (quote (("null-overlapped-wakeups-patch"))))))

(define-public crate-wepoll-ffi-0.1 (crate (name "wepoll-ffi") (vers "0.1.2") (deps (list (crate-dep (name "cc") (req "^1.0") (default-features #t) (kind 1)))) (hash "1yxpkva08d5f6ih3b5hdb8h45mkz3jq3dh1bzjspfhy6qpnzshyp") (features (quote (("null-overlapped-wakeups-patch"))))))

(define-public crate-wepoll-sys-1 (crate (name "wepoll-sys") (vers "1.0.0") (deps (list (crate-dep (name "bindgen") (req "^0.49") (default-features #t) (kind 1)) (crate-dep (name "cc") (req "^1.0") (default-features #t) (kind 1)))) (hash "1vqga28nz83035m4dws51xm7hr5lbadj839bdn9w38qbgpz32i8b") (links "wepoll")))

(define-public crate-wepoll-sys-1 (crate (name "wepoll-sys") (vers "1.0.1") (deps (list (crate-dep (name "bindgen") (req "^0.49") (default-features #t) (kind 1)) (crate-dep (name "cc") (req "^1.0") (default-features #t) (kind 1)))) (hash "07g5lgckcnralbkzq6cpa1yq19w7n8ax5ikmpzm5sv0nan70zyvm") (features (quote (("default" "compile-wepoll") ("compile-wepoll")))) (links "wepoll")))

(define-public crate-wepoll-sys-1 (crate (name "wepoll-sys") (vers "1.0.2") (deps (list (crate-dep (name "bindgen") (req "^0.49") (default-features #t) (kind 1)) (crate-dep (name "cc") (req "^1.0") (default-features #t) (kind 1)))) (hash "1fdxk1lc1753nhwv06lffmzzrfwrbdhdppj34ddcsqc0glhxxzy8") (features (quote (("default" "compile-wepoll") ("compile-wepoll")))) (links "wepoll")))

(define-public crate-wepoll-sys-1 (crate (name "wepoll-sys") (vers "1.0.3") (deps (list (crate-dep (name "bindgen") (req "^0.49") (default-features #t) (kind 1)) (crate-dep (name "cc") (req "^1.0") (default-features #t) (kind 1)))) (hash "0vsn6pp9bh5g06w01i8x5x51f9wx9qfbdia4s79prkz111a3ip8y") (links "wepoll")))

(define-public crate-wepoll-sys-1 (crate (name "wepoll-sys") (vers "1.0.4") (deps (list (crate-dep (name "bindgen") (req "^0.52") (default-features #t) (kind 1)) (crate-dep (name "cc") (req "^1.0") (default-features #t) (kind 1)))) (hash "10gn648ylyzpcm6wzka6ac716l6ylbsja6s6wrkqpm2vdqq4qad1") (links "wepoll")))

(define-public crate-wepoll-sys-1 (crate (name "wepoll-sys") (vers "1.1.0") (deps (list (crate-dep (name "bindgen") (req "^0.52") (default-features #t) (kind 1)) (crate-dep (name "cc") (req "^1.0") (default-features #t) (kind 1)))) (hash "0kjzccphsmxrps9081v30lhdqvvpp73cn5xka00if9hvgzn7v4h8") (links "wepoll")))

(define-public crate-wepoll-sys-2 (crate (name "wepoll-sys") (vers "2.0.0") (deps (list (crate-dep (name "bindgen") (req "^0.53") (optional #t) (default-features #t) (kind 1)) (crate-dep (name "cc") (req "^1.0") (default-features #t) (kind 1)))) (hash "093azxx6rdh7kf10kw6s2pbc7w99rfh4lr9bkrvgd4frmrvsg0lh") (features (quote (("default") ("buildtime-bindgen" "bindgen")))) (links "wepoll")))

(define-public crate-wepoll-sys-3 (crate (name "wepoll-sys") (vers "3.0.0") (deps (list (crate-dep (name "cc") (req "^1.0") (default-features #t) (kind 1)))) (hash "1q619gq2cbz2j8kp4f3l5p7v1macz8kwfmfbiwdbx27ylg5w4aql") (features (quote (("default")))) (links "wepoll")))

(define-public crate-wepoll-sys-3 (crate (name "wepoll-sys") (vers "3.0.1") (deps (list (crate-dep (name "cc") (req "^1.0") (default-features #t) (kind 1)))) (hash "1zvpkr4dz3ny0k20mg1wdlp8vawz5p4gnya7h8j24119m7g19jqg") (features (quote (("default")))) (links "wepoll")))

(define-public crate-wepoll-sys-stjepang-1 (crate (name "wepoll-sys-stjepang") (vers "1.0.0") (deps (list (crate-dep (name "bindgen") (req "^0.53") (optional #t) (default-features #t) (kind 1)) (crate-dep (name "cc") (req "^1.0") (default-features #t) (kind 1)))) (hash "15pllir026h56l9m7fvqp6r10iz9xnz405gsi636d7gwvr6l3q3l") (features (quote (("default") ("buildtime-bindgen" "bindgen")))) (links "wepoll")))

(define-public crate-wepoll-sys-stjepang-1 (crate (name "wepoll-sys-stjepang") (vers "1.0.1") (deps (list (crate-dep (name "bindgen") (req "^0.53") (optional #t) (default-features #t) (kind 1)) (crate-dep (name "cc") (req "^1.0") (default-features #t) (kind 1)))) (hash "0f2y1755q54hwcskqrbg11jyawvisavxv0smypmq4g1qf3fcbr5q") (features (quote (("default") ("buildtime-bindgen" "bindgen")))) (links "wepoll")))

(define-public crate-wepoll-sys-stjepang-1 (crate (name "wepoll-sys-stjepang") (vers "1.0.2") (deps (list (crate-dep (name "bindgen") (req "^0.53") (optional #t) (default-features #t) (kind 1)) (crate-dep (name "cc") (req "^1.0") (default-features #t) (kind 1)))) (hash "0yfw1lar68067545rhmhbqp1c12n6dyaj1jjzpw433grzjqqlpq3") (features (quote (("default") ("buildtime-bindgen" "bindgen")))) (links "wepoll")))

(define-public crate-wepoll-sys-stjepang-1 (crate (name "wepoll-sys-stjepang") (vers "1.0.3") (deps (list (crate-dep (name "bindgen") (req "^0.53") (optional #t) (default-features #t) (kind 1)) (crate-dep (name "cc") (req "^1.0") (default-features #t) (kind 1)))) (hash "1zqgj8vvr5lfasmkrpjvxc0ah7x67xg1g1fi5drwxcjsljggm1rr") (features (quote (("default") ("buildtime-bindgen" "bindgen")))) (links "wepoll")))

(define-public crate-wepoll-sys-stjepang-1 (crate (name "wepoll-sys-stjepang") (vers "1.0.4") (deps (list (crate-dep (name "bindgen") (req "^0.53") (optional #t) (default-features #t) (kind 1)) (crate-dep (name "cc") (req "^1.0") (default-features #t) (kind 1)))) (hash "0cgm6n3xxn0fjigixjf82zqjlfrrx69zzc52lg6l8m2msprdawmj") (features (quote (("default") ("buildtime-bindgen" "bindgen")))) (links "wepoll")))

(define-public crate-wepoll-sys-stjepang-1 (crate (name "wepoll-sys-stjepang") (vers "1.0.5") (deps (list (crate-dep (name "bindgen") (req "^0.53") (optional #t) (default-features #t) (kind 1)) (crate-dep (name "cc") (req "^1.0") (default-features #t) (kind 1)))) (hash "0m46s27awqm2jkb6ifvbpdp6fmgrbdm094zcsz23xsx7hz2cp52x") (features (quote (("default") ("buildtime-bindgen" "bindgen")))) (links "wepoll")))

(define-public crate-wepoll-sys-stjepang-1 (crate (name "wepoll-sys-stjepang") (vers "1.0.6") (deps (list (crate-dep (name "bindgen") (req "^0.53") (optional #t) (default-features #t) (kind 1)) (crate-dep (name "cc") (req "^1.0") (default-features #t) (kind 1)))) (hash "152651ajnd42n6g8ggk382swcrmd29l05c8p7ssnc0cqf7liklvg") (features (quote (("default") ("buildtime-bindgen" "bindgen")))) (links "wepoll")))

(define-public crate-wepoll-sys-stjepang-1 (crate (name "wepoll-sys-stjepang") (vers "1.0.7") (deps (list (crate-dep (name "bindgen") (req "^0.53") (optional #t) (default-features #t) (kind 1)) (crate-dep (name "cc") (req "^1.0") (default-features #t) (kind 1)))) (hash "09xag8iynb2635q7iwlv3a86fxqb4npsw8yfl13h21nq2vpy51f8") (features (quote (("default") ("buildtime-bindgen" "bindgen")))) (links "wepoll")))

(define-public crate-wepoll-sys-stjepang-1 (crate (name "wepoll-sys-stjepang") (vers "1.0.8") (deps (list (crate-dep (name "bindgen") (req "^0.53") (optional #t) (default-features #t) (kind 1)) (crate-dep (name "cc") (req "^1.0") (default-features #t) (kind 1)))) (hash "138pxc8k6wayyywnjcpk5nhywk3vk6h4i39fj8khpjlhy81vppqz") (features (quote (("default") ("buildtime-bindgen" "bindgen")))) (links "wepoll")))

