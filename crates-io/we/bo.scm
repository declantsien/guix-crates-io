(define-module (crates-io we bo) #:use-module (crates-io))

(define-public crate-webots-0.1 (crate (name "webots") (vers "0.1.0") (deps (list (crate-dep (name "webots-bindings") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "0z8xkiid5qx4s79m72jaax2mrwq3ah33w307ikqs1mbq1nxdgh9r") (yanked #t)))

(define-public crate-webots-0.1 (crate (name "webots") (vers "0.1.1") (deps (list (crate-dep (name "webots-bindings") (req "^0.1.0") (optional #t) (default-features #t) (kind 0)))) (hash "17mfrpvnkp7p1y5p60455rhl3ix93i2gp6kgfrpy7annps5q5d9v") (features (quote (("default" "webots-bindings")))) (yanked #t)))

(define-public crate-webots-0.1 (crate (name "webots") (vers "0.1.2") (deps (list (crate-dep (name "webots-bindings") (req "^0.1.1") (default-features #t) (kind 0)))) (hash "117zj2wi135yjshm2yd00w41s8s25dvc2cx7l4kfcwdmfmvs2g2y") (yanked #t)))

(define-public crate-webots-0.1 (crate (name "webots") (vers "0.1.3") (deps (list (crate-dep (name "webots-bindings") (req "^0.1.2") (default-features #t) (kind 0)))) (hash "0k2hz59qiraw75h8linvdnmdlgm231jm91a5jv3mh2qnk5f01485")))

(define-public crate-webots-0.2 (crate (name "webots") (vers "0.2.0") (deps (list (crate-dep (name "webots-bindings") (req "^0.2.0") (default-features #t) (kind 0)))) (hash "1igrp3gcqhkxqmw6iy4dcw5rbhd6qjsafvmgx5n9ky2gpwhy23ay")))

(define-public crate-webots-0.3 (crate (name "webots") (vers "0.3.0") (deps (list (crate-dep (name "anyhow") (req "^1.0.57") (default-features #t) (kind 0)) (crate-dep (name "webots-bindings") (req "^0.2.0") (default-features #t) (kind 0)))) (hash "1yj2dasvnhdkm33931dgxrpsb1113z7w5hj4z537vfj439glr2ri")))

(define-public crate-webots-0.4 (crate (name "webots") (vers "0.4.0") (deps (list (crate-dep (name "anyhow") (req "^1.0.57") (default-features #t) (kind 0)) (crate-dep (name "webots-bindings") (req "^0.3.0") (default-features #t) (kind 0)))) (hash "06aidrv0yv7p2vh2r09rfrbkayfyv8n2bhlb22j7gxkra7rp23yj")))

(define-public crate-webots-0.5 (crate (name "webots") (vers "0.5.0") (deps (list (crate-dep (name "anyhow") (req "^1.0.57") (default-features #t) (kind 0)) (crate-dep (name "webots-bindings") (req "^0.5.0") (default-features #t) (kind 0)))) (hash "0l6a2iszwhfghvw0mpjd2x5xlsf4z9svnsfrg2f2dmfrkmp90hs3")))

(define-public crate-webots-0.6 (crate (name "webots") (vers "0.6.0") (deps (list (crate-dep (name "thiserror") (req "^1.0.37") (default-features #t) (kind 0)) (crate-dep (name "webots-bindings") (req "^0.6.0") (default-features #t) (kind 0)))) (hash "0860sny2d42ng3r65wc9dxxqsbjx3caa0xck7jmsb22mc9z7gz99")))

(define-public crate-webots-0.7 (crate (name "webots") (vers "0.7.0") (deps (list (crate-dep (name "thiserror") (req "^1.0.37") (default-features #t) (kind 0)) (crate-dep (name "webots-bindings") (req "^0.7.0") (default-features #t) (kind 0)))) (hash "1a7c2r2gb6v6kdpd1npzk4qc4hmm9ansxcw74cmssf2jks6410rh")))

(define-public crate-webots-0.8 (crate (name "webots") (vers "0.8.0") (deps (list (crate-dep (name "thiserror") (req "^1.0.37") (default-features #t) (kind 0)) (crate-dep (name "webots-bindings") (req "^0.8.0") (default-features #t) (kind 0)))) (hash "1ix1p7km2v9c3ddyd72v2fiaxbzsln5lwdfnisxw3g83gf5a6yzn")))

(define-public crate-webots-bindings-0.1 (crate (name "webots-bindings") (vers "0.1.0") (deps (list (crate-dep (name "bindgen") (req "^0.59") (default-features #t) (kind 1)))) (hash "0avgaby48ih98dy51fvcfbnscpiqh0sbsyhyxxkqw87hyy5x936a") (yanked #t)))

(define-public crate-webots-bindings-0.1 (crate (name "webots-bindings") (vers "0.1.1") (deps (list (crate-dep (name "bindgen") (req "^0.59") (default-features #t) (kind 1)) (crate-dep (name "tempfile") (req "^3.2.0") (default-features #t) (kind 1)))) (hash "058wmasy7plagnjvwz2q1dkcq3zqlazjqxy1222q4jjnr3nj99zp") (yanked #t)))

(define-public crate-webots-bindings-0.1 (crate (name "webots-bindings") (vers "0.1.2") (deps (list (crate-dep (name "bindgen") (req "^0.59") (default-features #t) (kind 1)) (crate-dep (name "walkdir") (req "^2.3.2") (default-features #t) (kind 1)))) (hash "0wlx72qnz3pjyhl2sxcz8qvxrafgifg5jcvr0vlyjmh8a2rgiiw6")))

(define-public crate-webots-bindings-0.2 (crate (name "webots-bindings") (vers "0.2.0") (deps (list (crate-dep (name "bindgen") (req "^0.59") (default-features #t) (kind 1)) (crate-dep (name "walkdir") (req "^2.3.2") (default-features #t) (kind 1)))) (hash "1smpy7cqlx264v8d308pv894y99rd0733x0pqsziggzxyvsdwiyr")))

(define-public crate-webots-bindings-0.3 (crate (name "webots-bindings") (vers "0.3.0") (deps (list (crate-dep (name "bindgen") (req "^0.59") (default-features #t) (kind 1)) (crate-dep (name "walkdir") (req "^2.3.2") (default-features #t) (kind 1)))) (hash "172v10vcwg8zkxn0dwcrwjci1xq90wyxl96bsls44dwcw9zynyj1")))

(define-public crate-webots-bindings-0.5 (crate (name "webots-bindings") (vers "0.5.0") (deps (list (crate-dep (name "bindgen") (req "^0.60") (default-features #t) (kind 1)) (crate-dep (name "walkdir") (req "^2.3.2") (default-features #t) (kind 1)))) (hash "1ccajiawg1ff6savngylzlm2xdyzl7f3vr5ia19gc8p1v2dhaffi")))

(define-public crate-webots-bindings-0.6 (crate (name "webots-bindings") (vers "0.6.0") (deps (list (crate-dep (name "bindgen") (req "^0.63") (default-features #t) (kind 1)) (crate-dep (name "walkdir") (req "^2.3.2") (default-features #t) (kind 1)))) (hash "0plkz23ngsjiwgjhgrgba49nxyj2nzych9zn2g9h62yvgn0plhh9")))

(define-public crate-webots-bindings-0.7 (crate (name "webots-bindings") (vers "0.7.0") (deps (list (crate-dep (name "bindgen") (req "^0.63") (default-features #t) (kind 1)) (crate-dep (name "walkdir") (req "^2.3.2") (default-features #t) (kind 1)))) (hash "0czjp18r8z0y73b5z2brpjmg5zj3fzihgg8qzzss1fki21gwxfbv")))

(define-public crate-webots-bindings-0.8 (crate (name "webots-bindings") (vers "0.8.0") (deps (list (crate-dep (name "bindgen") (req "^0.63") (default-features #t) (kind 1)) (crate-dep (name "walkdir") (req "^2.3.2") (default-features #t) (kind 1)))) (hash "12swgmcpdr2crkfnacik9iki8d3k5ckiscvh91c991g1la5lp8cd")))

(define-public crate-webout-0.1 (crate (name "webout") (vers "0.1.0") (deps (list (crate-dep (name "actix") (req "^0.9.0") (default-features #t) (kind 0)) (crate-dep (name "actix-codec") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "actix-rt") (req "^1.1.1") (default-features #t) (kind 0)) (crate-dep (name "awc") (req "^1.0.1") (features (quote ("rustls"))) (default-features #t) (kind 0)) (crate-dep (name "bytes") (req "^0.5.6") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^2.33.3") (default-features #t) (kind 0)) (crate-dep (name "crossbeam-channel") (req "^0.5.0") (default-features #t) (kind 0)) (crate-dep (name "futures") (req "^0.3.5") (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.10.8") (features (quote ("blocking" "json"))) (default-features #t) (kind 0)) (crate-dep (name "rustls") (req "^0.16") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.116") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.57") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^0.2.22") (features (quote ("process"))) (default-features #t) (kind 0)) (crate-dep (name "tokio-util") (req "^0.3.1") (features (quote ("codec"))) (default-features #t) (kind 0)) (crate-dep (name "webpki-roots") (req "^0.19.0") (default-features #t) (kind 0)))) (hash "19jygqlxjj5z0vbmz1frhdzq7wsd0vy2vzpwkhsfadn6gs46idby")))

(define-public crate-weboxi-0.1 (crate (name "weboxi") (vers "0.1.0") (hash "18q2c0812kl4c9m9p510cngkyqgiqlq9kxbzfq6qq418ac5zy53r")))

