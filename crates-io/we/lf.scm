(define-module (crates-io we lf) #:use-module (crates-io))

(define-public crate-welford-0.1 (crate (name "welford") (vers "0.1.0") (deps (list (crate-dep (name "num-traits") (req "^0.2.14") (default-features #t) (kind 0)))) (hash "1cp3227ggndfgg7fpjk5y2cy8r3r0fn689lzscjf30n7809rcxs8")))

