(define-module (crates-io gl he) #:use-module (crates-io))

(define-public crate-glhelpe6-0.1 (crate (name "glhelpe6") (vers "0.1.6") (deps (list (crate-dep (name "gl") (req "^0.6.1") (default-features #t) (kind 0)))) (hash "18jdm56b32mqhizjs4pwxhh45xaj2flq65gsy60cfgirc3q8cs04")))

(define-public crate-glhelpe6-0.1 (crate (name "glhelpe6") (vers "0.1.7") (deps (list (crate-dep (name "gl") (req "^0.6.1") (default-features #t) (kind 0)))) (hash "0s92d3mf6j9wls8fb8qailwcdzzvpdwgldzl3phyqfkv8hxh9p4z")))

(define-public crate-glhelper-0.1 (crate (name "glhelper") (vers "0.1.0") (deps (list (crate-dep (name "gl") (req "^0.6.0") (default-features #t) (kind 0)) (crate-dep (name "nalgebra") (req "^0.8.2") (default-features #t) (kind 0)))) (hash "14s0wqab8v68v66njc2k8cyf5qr1v3j23kqbbcvic1yz5is7pkny")))

(define-public crate-glhelper-0.1 (crate (name "glhelper") (vers "0.1.1") (deps (list (crate-dep (name "gl") (req "^0.6.0") (default-features #t) (kind 0)) (crate-dep (name "nalgebra") (req "^0.8.2") (default-features #t) (kind 0)))) (hash "12ylp6zgplgn3cg91n6xkkzv0ia36d5i97ij5za3fv9v3h0a983g")))

(define-public crate-glhelper-0.1 (crate (name "glhelper") (vers "0.1.2") (deps (list (crate-dep (name "gl") (req "^0.6.0") (default-features #t) (kind 0)) (crate-dep (name "nalgebra") (req "^0.8.2") (default-features #t) (kind 0)))) (hash "1604argq9diwf0w40p9pys94d0akw7zld13dfjwxpffjlkv12rxf")))

(define-public crate-glhelper-0.1 (crate (name "glhelper") (vers "0.1.3") (deps (list (crate-dep (name "gl") (req "^0.6.0") (default-features #t) (kind 0)) (crate-dep (name "nalgebra") (req "^0.8.2") (default-features #t) (kind 0)))) (hash "1mzg86p8qs68kcbbzphbi9ff9943n2dqaay55sb7y4bpl9gxb6wv")))

(define-public crate-glhelper-0.1 (crate (name "glhelper") (vers "0.1.4") (deps (list (crate-dep (name "gl") (req "^0.6.0") (default-features #t) (kind 0)) (crate-dep (name "nalgebra") (req "^0.8.2") (default-features #t) (kind 0)))) (hash "09w2jf8rpf561p2cd08nc0l1ikblgxq4k19f0hbxv6qvyz5inx9v")))

(define-public crate-glhelper-0.1 (crate (name "glhelper") (vers "0.1.5") (deps (list (crate-dep (name "gl") (req "^0.6.0") (default-features #t) (kind 0)) (crate-dep (name "nalgebra") (req "^0.8.2") (default-features #t) (kind 0)))) (hash "1v008cb134qk9acfb93lv5ldqhi8byq9578zj5ghfz9j1qk5i182")))

(define-public crate-glhelper-0.1 (crate (name "glhelper") (vers "0.1.8") (deps (list (crate-dep (name "gl") (req "^0.6.1") (default-features #t) (kind 0)))) (hash "17xqd6pjnn402cblr5a4h8by2fbmvnja37b01piplyrabljw7mr3")))

(define-public crate-glhelper-0.1 (crate (name "glhelper") (vers "0.1.9") (deps (list (crate-dep (name "gl") (req "^0.6.1") (default-features #t) (kind 0)) (crate-dep (name "sdl2") (req "^0.29.1") (default-features #t) (kind 2)))) (hash "13zqcn3vs1ymv60v3n5mymws3hnhl0sxkicwbc5d5qpicdhn847b")))

(define-public crate-glhelper-0.1 (crate (name "glhelper") (vers "0.1.10") (deps (list (crate-dep (name "gl") (req "^0.6.1") (default-features #t) (kind 0)) (crate-dep (name "sdl2") (req "^0.29.1") (default-features #t) (kind 2)))) (hash "05ws70sc4xfj8xr4ivbx80vzwcxlbk026q21njfsh5sgq0mxnl9q")))

(define-public crate-glhelper-0.1 (crate (name "glhelper") (vers "0.1.11") (deps (list (crate-dep (name "gl") (req "^0.6.1") (default-features #t) (kind 0)) (crate-dep (name "sdl2") (req "^0.29.1") (default-features #t) (kind 2)))) (hash "0y14hi8vbby5k4h1grdwqrr29qwbg3nj3w6bp15n2ykimg0926i3")))

(define-public crate-glhelper-0.1 (crate (name "glhelper") (vers "0.1.12") (deps (list (crate-dep (name "gl") (req "^0.6.1") (default-features #t) (kind 0)) (crate-dep (name "sdl2") (req "^0.29.1") (default-features #t) (kind 2)))) (hash "0p5lrmkfzw1z8r0ncpv4qvns50bx5gdiny88a1x3286xpm8lrrxg")))

