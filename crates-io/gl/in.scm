(define-module (crates-io gl in) #:use-module (crates-io))

(define-public crate-glint-0.1 (crate (name "glint") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^2") (default-features #t) (kind 0)) (crate-dep (name "crossterm") (req "^0.10") (default-features #t) (kind 0)) (crate-dep (name "unic-segment") (req "^0.9") (default-features #t) (kind 0)))) (hash "0p6mylhcppy1zvv07ap2lslzq4apvkdrlgl3fv16j1r0dj8p200v")))

(define-public crate-glint-0.1 (crate (name "glint") (vers "0.1.2") (deps (list (crate-dep (name "clap") (req "^2") (default-features #t) (kind 0)) (crate-dep (name "crossterm") (req "^0.10") (default-features #t) (kind 0)) (crate-dep (name "unic-segment") (req "^0.9") (default-features #t) (kind 0)))) (hash "1s92s97yrvwx94fi721cp0cb50z8b1jhw9lv5qyjahcwvrxkq12c")))

(define-public crate-glint-0.1 (crate (name "glint") (vers "0.1.3") (deps (list (crate-dep (name "clap") (req "^2") (default-features #t) (kind 0)) (crate-dep (name "crossterm") (req "^0.10") (default-features #t) (kind 0)) (crate-dep (name "unic-segment") (req "^0.9") (default-features #t) (kind 0)))) (hash "1szn01nifj0ib743bjz6sm8fwswrx42m355fn05gbkmvrd3ii0x9")))

(define-public crate-glint-0.1 (crate (name "glint") (vers "0.1.4") (deps (list (crate-dep (name "clap") (req "^2") (default-features #t) (kind 0)) (crate-dep (name "crossterm") (req "^0.10") (default-features #t) (kind 0)) (crate-dep (name "unic-segment") (req "^0.9") (default-features #t) (kind 0)))) (hash "0prjmvqkmq6pdi7x2zjfqmnnkbkx2x2zw9x3v87az9bzxy8wxdca")))

(define-public crate-glint-0.1 (crate (name "glint") (vers "0.1.5") (deps (list (crate-dep (name "clap") (req "^2") (default-features #t) (kind 0)) (crate-dep (name "crossterm") (req "^0.10") (default-features #t) (kind 0)) (crate-dep (name "unic-segment") (req "^0.9") (default-features #t) (kind 0)))) (hash "0jccmpjly3vr0wb338f21r77nqrkfrhnhbibwp1az3fr8y509dsg")))

(define-public crate-glint-0.1 (crate (name "glint") (vers "0.1.6") (deps (list (crate-dep (name "clap") (req "^2") (default-features #t) (kind 0)) (crate-dep (name "crossterm") (req "^0.10") (default-features #t) (kind 0)) (crate-dep (name "unic-segment") (req "^0.9") (default-features #t) (kind 0)))) (hash "1zjqy4rlawxl984yvclycp6sy5j6xaqxqz05wp050ynggik0x6qm")))

(define-public crate-glint-0.1 (crate (name "glint") (vers "0.1.7") (deps (list (crate-dep (name "clap") (req "^2") (default-features #t) (kind 0)) (crate-dep (name "crossterm") (req "^0.10") (default-features #t) (kind 0)) (crate-dep (name "unic-segment") (req "^0.9") (default-features #t) (kind 0)))) (hash "0cbpjw7halh9skkcwnf0m52jf1nbr1ciij6sm3rddcvfjckq96fa")))

(define-public crate-glint-0.2 (crate (name "glint") (vers "0.2.0") (deps (list (crate-dep (name "clap") (req "^2") (default-features #t) (kind 0)) (crate-dep (name "crossterm") (req "^0.10") (default-features #t) (kind 0)) (crate-dep (name "unic-segment") (req "^0.9") (default-features #t) (kind 0)))) (hash "08jlm7i0gr9sl9i0qiv41a6rrhgd4s6j94z7vblwj7pc2qzdphaf")))

(define-public crate-glint-0.2 (crate (name "glint") (vers "0.2.1") (deps (list (crate-dep (name "clap") (req "^2") (default-features #t) (kind 0)) (crate-dep (name "crossterm") (req "^0.10") (default-features #t) (kind 0)) (crate-dep (name "unic-segment") (req "^0.9") (default-features #t) (kind 0)))) (hash "0j8s2xczbz0vp90yh948pi3ln3i5qlgv2dnb5dnpz3hpza4h616m")))

(define-public crate-glint-0.4 (crate (name "glint") (vers "0.4.0") (deps (list (crate-dep (name "crossterm") (req "^0.11") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "unic-segment") (req "^0.9") (default-features #t) (kind 0)))) (hash "197jfrhws5ffc42na94if306cny49qw08p863w03s1v1qx1013zg")))

(define-public crate-glint-0.5 (crate (name "glint") (vers "0.5.0") (deps (list (crate-dep (name "crossterm") (req "^0.17.7") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.3.15") (default-features #t) (kind 0)) (crate-dep (name "unic-segment") (req "^0.9.0") (default-features #t) (kind 0)))) (hash "05qd07d6x2c65ipdy5p86nzaliwvhf7k66cwh335ada06kd9s8z6")))

(define-public crate-glint-0.6 (crate (name "glint") (vers "0.6.0") (deps (list (crate-dep (name "crossterm") (req "^0.17.7") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.3.15") (default-features #t) (kind 0)) (crate-dep (name "unic-segment") (req "^0.9.0") (default-features #t) (kind 0)))) (hash "0k9x7x5xq0zxjyks3mmx4ss595nbqa0rnz4j7bgfryspq5h9mgir")))

(define-public crate-glint-0.6 (crate (name "glint") (vers "0.6.2") (deps (list (crate-dep (name "crossterm") (req "^0.17.7") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.3.15") (default-features #t) (kind 0)) (crate-dep (name "unic-segment") (req "^0.9.0") (default-features #t) (kind 0)))) (hash "0dlqwykcpvdyavc97393sz8a5s8020c5n0ksd98bifyrg50vsm05")))

(define-public crate-glint-6 (crate (name "glint") (vers "6.3.4") (deps (list (crate-dep (name "crossterm") (req "^0.17.7") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.3.23") (default-features #t) (kind 0)) (crate-dep (name "unic-segment") (req "^0.9.0") (default-features #t) (kind 0)))) (hash "1503gq1czqwy9q9sl07lhp622m6vnzhqljnzqphgvsb5pklb4vww")))

