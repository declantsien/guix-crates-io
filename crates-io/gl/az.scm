(define-module (crates-io gl az) #:use-module (crates-io))

(define-public crate-glaze-0.0.1 (crate (name "glaze") (vers "0.0.1") (deps (list (crate-dep (name "pollster") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "wgpu") (req "^0.15.1") (default-features #t) (kind 0)) (crate-dep (name "winit") (req "^0.28.3") (default-features #t) (kind 0)))) (hash "1z1bc7caygd86icbwpznh0knjh6gzmaglp1fhqslw6b41vd0qihp")))

