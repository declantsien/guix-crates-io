(define-module (crates-io gl cm) #:use-module (crates-io))

(define-public crate-glcmrs-0.1 (crate (name "glcmrs") (vers "0.1.0") (deps (list (crate-dep (name "blas-src") (req "^0.9.0") (features (quote ("openblas"))) (default-features #t) (kind 0)) (crate-dep (name "ndarray") (req "^0.15.6") (features (quote ("blas" "rayon"))) (default-features #t) (kind 0)) (crate-dep (name "ndarray-rand") (req "^0.14.0") (default-features #t) (kind 0)) (crate-dep (name "numpy") (req "^0.20.0") (default-features #t) (kind 0)) (crate-dep (name "openblas-src") (req "^0.10") (features (quote ("cblas" "system"))) (default-features #t) (kind 0)) (crate-dep (name "pyo3") (req "^0.20.0") (default-features #t) (kind 0)))) (hash "1wr020idxr77ahnf44cqrr2f8ig5bm1ip620ajsc44ynzzjgsn2r")))

