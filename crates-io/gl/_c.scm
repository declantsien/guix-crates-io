(define-module (crates-io gl _c) #:use-module (crates-io))

(define-public crate-gl_common-0.0.2 (crate (name "gl_common") (vers "0.0.2") (hash "05x6bbcxbznmqshxza65vrx1igqjimmv3ykjdvllncwl4h5id1qm")))

(define-public crate-gl_common-0.0.3 (crate (name "gl_common") (vers "0.0.3") (hash "09k02zff065f8spf8b9mrsbgk78ypdjz21y6j69sn3rk3sz3770h")))

(define-public crate-gl_common-0.0.4 (crate (name "gl_common") (vers "0.0.4") (deps (list (crate-dep (name "libc") (req "*") (default-features #t) (kind 0)))) (hash "1ha3vrclcna8j61r3m6zf5066bwag7zbxxzag0ssh2sm7qhn191v")))

(define-public crate-gl_common-0.1 (crate (name "gl_common") (vers "0.1.0") (deps (list (crate-dep (name "libc") (req "^0.1") (default-features #t) (kind 0)))) (hash "1jmi0qp8jx6699p39m3bqqw31mmxsj1gk07vpjmy87q56w2x6gy8")))

(define-public crate-gl_constants-0.1 (crate (name "gl_constants") (vers "0.1.0") (deps (list (crate-dep (name "gl_types") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "1xflaiigd1r1ldy0mb37c90apznif3qgjg2cjfbq9969psmxys9v")))

(define-public crate-gl_constants-0.1 (crate (name "gl_constants") (vers "0.1.1") (deps (list (crate-dep (name "gl_types") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "1l3cqsi96z44jxw8hl3kjk0rffyw7xi9j35jr2dmf5vdm66gmfg1")))

