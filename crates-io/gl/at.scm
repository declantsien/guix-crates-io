(define-module (crates-io gl at) #:use-module (crates-io))

(define-public crate-glatic-0.1 (crate (name "glatic") (vers "0.1.0") (deps (list (crate-dep (name "posh") (req "^0.1") (default-features #t) (kind 0)))) (hash "13cjbphixvzg0i2yvk2rbv75ji4w4b19gr2qc2lmjq3fljk5v1va") (yanked #t)))

(define-public crate-glatic-0.1 (crate (name "glatic") (vers "0.1.1") (hash "1rxjkj59h7bpi2jwxi1q494df0hmyi3ikdc2yiv82g31caqqpcl7")))

(define-public crate-glatic-0.1 (crate (name "glatic") (vers "0.1.2") (hash "1f0n0amh8vfvvc81y7y5b5f9bhrg8v7ihmj5q0wd0ym60y53sbgv")))

