(define-module (crates-io gl _m) #:use-module (crates-io))

(define-public crate-gl_matrix-0.0.1 (crate (name "gl_matrix") (vers "0.0.1") (deps (list (crate-dep (name "rand") (req "^0.7.3") (default-features #t) (kind 0)))) (hash "1fsxxw5izy7450hih6wgravd1gi20yl089lacvxxcf06kdvvwmmi") (yanked #t)))

(define-public crate-gl_matrix-0.0.2 (crate (name "gl_matrix") (vers "0.0.2") (deps (list (crate-dep (name "rand") (req "^0.7.3") (default-features #t) (kind 0)))) (hash "1bik9h5263vy87q1siyxkchgzbmjglzad1d3nnh336aqbhjd0r6z")))

