(define-module (crates-io gl _l) #:use-module (crates-io))

(define-public crate-gl_lib-0.0.1 (crate (name "gl_lib") (vers "0.0.1") (deps (list (crate-dep (name "failure") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "gl_generator") (req "^0.14.0") (default-features #t) (kind 1)) (crate-dep (name "image") (req "^0.23.14") (default-features #t) (kind 0)) (crate-dep (name "itertools") (req "^0.10.1") (default-features #t) (kind 0)) (crate-dep (name "nalgebra") (req "^0.25.4") (default-features #t) (kind 0)) (crate-dep (name "sdl2") (req "^0.34.3") (features (quote ("bundled" "static-link"))) (default-features #t) (kind 2)) (crate-dep (name "walkdir") (req "^2.1") (default-features #t) (kind 0)))) (hash "0zj8szh0bn9f9p31hxwsawr1xa4yy84s338wcnmpdj1z66wwl0r8") (features (quote (("debug"))))))

(define-public crate-gl_lib-0.1 (crate (name "gl_lib") (vers "0.1.0") (deps (list (crate-dep (name "failure") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "gl_generator") (req "^0.14.0") (default-features #t) (kind 1)) (crate-dep (name "image") (req "^0.23.14") (default-features #t) (kind 0)) (crate-dep (name "itertools") (req "^0.10.1") (default-features #t) (kind 0)) (crate-dep (name "nalgebra") (req "^0.25.4") (default-features #t) (kind 0)) (crate-dep (name "sdl2") (req "^0.34.3") (features (quote ("bundled" "static-link"))) (default-features #t) (kind 2)) (crate-dep (name "walkdir") (req "^2.1") (default-features #t) (kind 0)))) (hash "15jrrch4d43v287r7b1cvhkwdyqllhvv159p99pdr9w21rkm164p") (features (quote (("debug"))))))

(define-public crate-gl_loader-0.0.1 (crate (name "gl_loader") (vers "0.0.1") (deps (list (crate-dep (name "core-foundation") (req "^0.6") (default-features #t) (target "cfg(target_os = \"macos\")") (kind 0)))) (hash "0a7a1hid3akdwdc3y82qd8jpvsmjmpp83wcw42wgc227h1sahn2g")))

(define-public crate-gl_loader-0.0.2 (crate (name "gl_loader") (vers "0.0.2") (deps (list (crate-dep (name "core-foundation") (req "^0.6") (default-features #t) (target "cfg(target_os = \"macos\")") (kind 0)))) (hash "18vk8pb4i5g9z3vax2w5y3i6nnhrgm0kgbrgr0w4g5k8515w8v97")))

(define-public crate-gl_loader-0.0.4 (crate (name "gl_loader") (vers "0.0.4") (deps (list (crate-dep (name "cc") (req "^1.0") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "1jjzn03irv29jnr3cldygcs9yky0gabbvkcicxgmlswq96wl6clb")))

(define-public crate-gl_loader-0.1 (crate (name "gl_loader") (vers "0.1.0") (deps (list (crate-dep (name "cc") (req "^1.0") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "11nhskvw1p6i52s3pa4lkywacs992brxm80yxf0hv070il6vn7wc")))

(define-public crate-gl_loader-0.1 (crate (name "gl_loader") (vers "0.1.1") (deps (list (crate-dep (name "cc") (req "^1.0") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "1psa8l0sx86wiypai5sd5zpyjpd5wxpldrr6gkjbsmfpabca6k53")))

(define-public crate-gl_loader-0.1 (crate (name "gl_loader") (vers "0.1.2") (deps (list (crate-dep (name "cc") (req "^1.0") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "1lwr1gd7hrb2nk67zw4pc04vl4h868r5a7846zjr0548bzfrcbg3")))

