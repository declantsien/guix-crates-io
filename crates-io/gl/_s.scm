(define-module (crates-io gl _s) #:use-module (crates-io))

(define-public crate-gl_struct_loader-0.1 (crate (name "gl_struct_loader") (vers "0.1.0") (deps (list (crate-dep (name "gl_types") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "0lmpb4ryh4k6jsi8lchr1pxjvcd660wyapsrg9l63mnvfdcxax6f") (features (quote (("track_caller") ("std" "alloc") ("default" "std") ("alloc"))))))

(define-public crate-gl_struct_loader-0.1 (crate (name "gl_struct_loader") (vers "0.1.1") (deps (list (crate-dep (name "gl_types") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "0n67daxz13lgjzz1w7py1xrgr8x3v28mjrwzdvp3aw2w6wxhdnsi") (features (quote (("track_caller") ("std" "alloc") ("default" "std") ("alloc"))))))

