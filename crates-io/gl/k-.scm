(define-module (crates-io gl k-) #:use-module (crates-io))

(define-public crate-glk-sys-0.1 (crate (name "glk-sys") (vers "0.1.0") (deps (list (crate-dep (name "bindgen") (req "^0.51") (default-features #t) (kind 1)) (crate-dep (name "cc") (req "^1.0") (default-features #t) (kind 1)))) (hash "14rax7y1mdgb3gqs11k2b3f28s0gi79dm1bin75apglfyzvm5jc2")))

(define-public crate-glk-sys-0.2 (crate (name "glk-sys") (vers "0.2.0") (deps (list (crate-dep (name "bindgen") (req "^0.51") (default-features #t) (kind 1)) (crate-dep (name "cc") (req "^1.0") (default-features #t) (kind 1)))) (hash "05k05pz6zghj15mzaqgzpdg8qx4jqczm4qnjghc6kg2a0sz3rizm")))

