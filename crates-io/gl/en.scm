(define-module (crates-io gl en) #:use-module (crates-io))

(define-public crate-glen-0.0.0 (crate (name "glen") (vers "0.0.0") (hash "0hsyyvr2fgxilm9z4i1z5spl7ivj3i2d415g87nk5xxggps495mw")))

(define-public crate-glennib-thelib-0.1 (crate (name "glennib-thelib") (vers "0.1.0") (hash "1jnlfzw7gq24470fk63s34rvydir3mq5rr05q952i2p0920pkhnj")))

(define-public crate-glennib-thelib-0.1 (crate (name "glennib-thelib") (vers "0.1.1") (hash "0klvlxfm92ivixzzx5s9q9nzvmnfr264r9cl0x35sdbyfcga9wgk")))

(define-public crate-glennib-thelib-0.1 (crate (name "glennib-thelib") (vers "0.1.2") (hash "054grin8yx5qha8piv848rbwgfkkdyhwfanalhn6gmy0v9r5dwbl")))

(define-public crate-glennib-theserver-0.1 (crate (name "glennib-theserver") (vers "0.1.0") (deps (list (crate-dep (name "anyhow") (req "^1.0.82") (default-features #t) (kind 0)) (crate-dep (name "axum") (req "^0.7.5") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^4.5.4") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "thelib") (req "^0.1.1") (default-features #t) (kind 0) (package "glennib-thelib")) (crate-dep (name "tokio") (req "^1.37.0") (features (quote ("full"))) (default-features #t) (kind 0)) (crate-dep (name "tracing") (req "^0.1.40") (default-features #t) (kind 0)) (crate-dep (name "tracing-subscriber") (req "^0.3.18") (default-features #t) (kind 0)))) (hash "0xxk2z5rf37ylvfabg4x23qlqjz145di57sz5mxyvmzmv6gazy0s")))

(define-public crate-glennib-theserver-0.1 (crate (name "glennib-theserver") (vers "0.1.1") (deps (list (crate-dep (name "anyhow") (req "^1.0.82") (default-features #t) (kind 0)) (crate-dep (name "axum") (req "^0.7.5") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^4.5.4") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "thelib") (req "^0.1.1") (default-features #t) (kind 0) (package "glennib-thelib")) (crate-dep (name "tokio") (req "^1.37.0") (features (quote ("full"))) (default-features #t) (kind 0)) (crate-dep (name "tracing") (req "^0.1.40") (default-features #t) (kind 0)) (crate-dep (name "tracing-subscriber") (req "^0.3.18") (default-features #t) (kind 0)))) (hash "0km8yhh8h2s46xprmhjzij7qvf367gjypr9as536rj7gzbz6ryi2")))

(define-public crate-glennib-theserver-0.1 (crate (name "glennib-theserver") (vers "0.1.2") (deps (list (crate-dep (name "anyhow") (req "^1.0.82") (default-features #t) (kind 0)) (crate-dep (name "axum") (req "^0.7.5") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^4.5.4") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "thelib") (req "^0.1.2") (default-features #t) (kind 0) (package "glennib-thelib")) (crate-dep (name "tokio") (req "^1.37.0") (features (quote ("full"))) (default-features #t) (kind 0)) (crate-dep (name "tracing") (req "^0.1.40") (default-features #t) (kind 0)) (crate-dep (name "tracing-subscriber") (req "^0.3.18") (default-features #t) (kind 0)))) (hash "1xyarcqzkkmchbbcwg98m61535k7wv37ja4p207ikcwmlxzhd4ij")))

(define-public crate-glennib-theserver-0.1 (crate (name "glennib-theserver") (vers "0.1.3") (deps (list (crate-dep (name "anyhow") (req "^1.0.82") (default-features #t) (kind 0)) (crate-dep (name "axum") (req "^0.7.5") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^4.5.4") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "thelib") (req "^0.1.2") (default-features #t) (kind 0) (package "glennib-thelib")) (crate-dep (name "tokio") (req "^1.37.0") (features (quote ("full"))) (default-features #t) (kind 0)) (crate-dep (name "tracing") (req "^0.1.40") (default-features #t) (kind 0)) (crate-dep (name "tracing-subscriber") (req "^0.3.18") (default-features #t) (kind 0)))) (hash "10w33xifd7a2ffaiqi15rfcww7jvxv34344iw1jalyr9cnhr0r5s")))

(define-public crate-glenum-0.1 (crate (name "glenum") (vers "0.1.0") (hash "11f9ay0jhcnfk0laxymnn7afcrvvr4a8isflz8f0k2vvz1j3dah0")))

(define-public crate-glenum-0.1 (crate (name "glenum") (vers "0.1.1") (deps (list (crate-dep (name "serde") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^1.0") (default-features #t) (kind 0)))) (hash "0hfkg0hz83d4g0qpzgjd344j28m8bynxidw684m5ymh6lifkx3fy")))

