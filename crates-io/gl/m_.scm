(define-module (crates-io gl m_) #:use-module (crates-io))

(define-public crate-glm_color-0.1 (crate (name "glm_color") (vers "0.1.0") (deps (list (crate-dep (name "glm") (req "*") (default-features #t) (kind 0)) (crate-dep (name "quickcheck") (req "*") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "*") (default-features #t) (kind 0)))) (hash "1ivxzscsv64ill793c37vsrnxq8hh2hb9vy28qbvsplcbxdg5c0h")))

(define-public crate-glm_color-0.1 (crate (name "glm_color") (vers "0.1.1") (deps (list (crate-dep (name "glm") (req "*") (default-features #t) (kind 0)) (crate-dep (name "quickcheck") (req "*") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "*") (default-features #t) (kind 0)))) (hash "12pjc6lngm7mmzz73inmc4c5q46xfg7qmnx4cdk3dfr6kkqgddzd")))

(define-public crate-glm_color-0.1 (crate (name "glm_color") (vers "0.1.2") (deps (list (crate-dep (name "glm") (req "*") (default-features #t) (kind 0)) (crate-dep (name "quickcheck") (req "*") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "*") (default-features #t) (kind 0)))) (hash "1hqj7kv3pslw3bdr3d7rbdc1gz9f4mxy93qdx7xdcrja7bcgzls2")))

