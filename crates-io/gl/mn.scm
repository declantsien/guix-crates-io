(define-module (crates-io gl mn) #:use-module (crates-io))

(define-public crate-glmnet-0.1 (crate (name "glmnet") (vers "0.1.0") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "ndarray") (req "^0.11") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.5") (default-features #t) (kind 2)))) (hash "075cp14wif81j30i5a2gxag23awznhwjjmb8fizxxccrj2biywx3")))

(define-public crate-glmnet-0.1 (crate (name "glmnet") (vers "0.1.1") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "ndarray") (req "^0.11") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.5") (default-features #t) (kind 2)))) (hash "0aj6gndk154lqkp6z7arka89dlb3f9qff65rjpddqm586ag1py4n")))

