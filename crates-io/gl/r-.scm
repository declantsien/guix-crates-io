(define-module (crates-io gl r-) #:use-module (crates-io))

(define-public crate-glr-parser-0.0.1 (crate (name "glr-parser") (vers "0.0.1") (deps (list (crate-dep (name "regex") (req "^0.1.32") (default-features #t) (kind 0)))) (hash "04z6irlglvz9y81lbzidx5kv3s533g4av9hmab4fh2fcda00km45")))

