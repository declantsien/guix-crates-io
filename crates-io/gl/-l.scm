(define-module (crates-io gl -l) #:use-module (crates-io))

(define-public crate-gl-lite-0.1 (crate (name "gl-lite") (vers "0.1.0") (deps (list (crate-dep (name "gl") (req "^0.11.0") (default-features #t) (target "cfg(not(target_arch = \"wasm32\"))") (kind 0)) (crate-dep (name "glutin") (req "^0.20.0") (default-features #t) (target "cfg(not(target_arch = \"wasm32\"))") (kind 0)))) (hash "01fn03hf3br44nqdbrfi7h71b22ny7kf0kiajkqmk55fbsj97m9n")))

(define-public crate-gl-lite-0.1 (crate (name "gl-lite") (vers "0.1.1") (deps (list (crate-dep (name "gl") (req "^0.11.0") (default-features #t) (target "cfg(not(target_arch = \"wasm32\"))") (kind 0)) (crate-dep (name "glutin") (req "^0.20.0") (default-features #t) (target "cfg(not(target_arch = \"wasm32\"))") (kind 0)))) (hash "1x22k6w40fsp9n7cqaci20yln2sr9wrazifsrdg6s47hjxgyq618")))

(define-public crate-gl-lite-0.1 (crate (name "gl-lite") (vers "0.1.2") (deps (list (crate-dep (name "gl") (req "^0.11.0") (default-features #t) (target "cfg(not(target_arch = \"wasm32\"))") (kind 0)) (crate-dep (name "glutin") (req "^0.20.0") (default-features #t) (target "cfg(not(target_arch = \"wasm32\"))") (kind 0)))) (hash "1dlyj3ifpcxnll134xi1bcd3i434f73vs2awr74xnbvifn97cz2r")))

