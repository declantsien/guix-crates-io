(define-module (crates-io gl u-) #:use-module (crates-io))

(define-public crate-glu-sys-0.1 (crate (name "glu-sys") (vers "0.1.0") (hash "0nn6w3pmqah1dp483nll7zq292plqqmcvs6qlmh4jmla14zf11nz")))

(define-public crate-glu-sys-0.1 (crate (name "glu-sys") (vers "0.1.1") (hash "1h3h3dhr966iwlcal8hk006548yxk1rw680mvwqpl945rc24alhf")))

(define-public crate-glu-sys-0.1 (crate (name "glu-sys") (vers "0.1.2") (deps (list (crate-dep (name "libc") (req "^0.2.74") (default-features #t) (kind 0)))) (hash "06gsc2qb83n64i746h5mrgw9g282ahhzrjpachfyrh8ffvwzvd0f")))

(define-public crate-glu-sys-0.1 (crate (name "glu-sys") (vers "0.1.3") (deps (list (crate-dep (name "libc") (req "^0.2.74") (default-features #t) (kind 0)))) (hash "074979wgd71v074wkq065mk9nydqkyxjbp36nmh4bxfpymxq9vrs")))

(define-public crate-glu-sys-0.1 (crate (name "glu-sys") (vers "0.1.4") (deps (list (crate-dep (name "libc") (req "^0.2.74") (default-features #t) (kind 0)))) (hash "1wy3bj82gcqzb4k9mkvwl9hdc98q1wwgn3kcf26y4by2kjmnlvmr")))

