(define-module (crates-io gl oc) #:use-module (crates-io))

(define-public crate-glock-0.1 (crate (name "glock") (vers "0.1.0") (hash "1lfq8f336w8pscghn2k94xw2kg0zlsip2yiqvbd7dzh9jgyk9519")))

(define-public crate-glock-0.1 (crate (name "glock") (vers "0.1.1") (hash "141j1hkh75h8jhdvxsj7cc3cshhpl47rwqlrj6fb1dg39g5az9cz")))

(define-public crate-glock-0.1 (crate (name "glock") (vers "0.1.2") (hash "0c7l4k4ni0q0i49my2phwcqp7rlywsibsfkid23zl5v8h04z2bgi")))

