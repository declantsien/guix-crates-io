(define-module (crates-io gl is) #:use-module (crates-io))

(define-public crate-glisp-0.0.1 (crate (name "glisp") (vers "0.0.1") (hash "1q5893p8ir9ynn6ha5x451i1xgy79qrzmcb63smpkslb4x7v9x5l")))

(define-public crate-glisp-0.0.2 (crate (name "glisp") (vers "0.0.2") (hash "0g9fmxhqiqddpspjr7afqjgz1sb6pmpijdkxpffp2sqi000bcljd")))

(define-public crate-glisp-0.0.3 (crate (name "glisp") (vers "0.0.3") (deps (list (crate-dep (name "cc") (req "^1.0.29") (features (quote ("parallel"))) (default-features #t) (kind 1)) (crate-dep (name "cpp") (req "^0.5.1") (default-features #t) (kind 0)) (crate-dep (name "cpp_build") (req "^0.5.1") (default-features #t) (kind 1)) (crate-dep (name "lazy_static") (req "^1.2.0") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2.49") (default-features #t) (kind 0)) (crate-dep (name "maplit") (req "^1.0.1") (default-features #t) (kind 0)) (crate-dep (name "rayon") (req "^1.0.3") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.1.0") (default-features #t) (kind 0)))) (hash "0k3b88s9v0j82jzh6afi08z5gllfhw6hm99fqb11nnj1vc4gf5mr")))

(define-public crate-glisp-0.0.4 (crate (name "glisp") (vers "0.0.4") (deps (list (crate-dep (name "cc") (req "^1.0.30") (features (quote ("parallel"))) (default-features #t) (kind 1)) (crate-dep (name "cpp") (req "^0.5.1") (default-features #t) (kind 0)) (crate-dep (name "cpp_build") (req "^0.5.1") (default-features #t) (kind 1)) (crate-dep (name "lazy_static") (req "^1.3.0") (default-features #t) (kind 0)) (crate-dep (name "libc") (req "^0.2.49") (default-features #t) (kind 0)) (crate-dep (name "maplit") (req "^1.0.1") (default-features #t) (kind 0)) (crate-dep (name "rayon") (req "^1.0.3") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.1.2") (default-features #t) (kind 0)))) (hash "1nwg0jn374cxcg4n8xhwlwyb4j6v4nziiw5jjfpr5mm6l0h75m73")))

(define-public crate-glissile-0.0.1 (crate (name "glissile") (vers "0.0.1") (hash "0lsgxn40ghsknsbgd7b7mcy2x6wycll0jfxxnbd0bxpfja0cqcg1")))

(define-public crate-glissile-0.0.2 (crate (name "glissile") (vers "0.0.2") (hash "1rfx7r8m80zczmjwyfykyhpwg7vj756znj2w1jm7nmm148fj6my8")))

(define-public crate-glissile-0.0.3 (crate (name "glissile") (vers "0.0.3") (hash "1g6gavamn06an82bb2cwb9kc71y8ycrya1i391jh1gi4py6lqn7k")))

