(define-module (crates-io gl ot) #:use-module (crates-io))

(define-public crate-glot-0.1 (crate (name "glot") (vers "0.1.0") (deps (list (crate-dep (name "nom") (req "^7.1.3") (default-features #t) (kind 0)))) (hash "1m5zmvknabhqsqmxc692y23ck11xjgnpqs8ganmkai5z63zihjcd")))

(define-public crate-glotaran_converter_cli-0.1 (crate (name "glotaran_converter_cli") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^4.4.5") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "glotaran_converter_lib") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "1hmj2w6lyfcvwhj9pd18q80z5lzwfw1ng9si8z3xs4ma64h7yxr6")))

(define-public crate-glotaran_converter_cli-0.1 (crate (name "glotaran_converter_cli") (vers "0.1.1") (deps (list (crate-dep (name "clap") (req "^4.4.5") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "glotaran_converter_lib") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "16zz3xg5pibmf5d84v5y21lj301x45hgn1vxq3v56js0f09rbncp")))

(define-public crate-glotaran_converter_lib-0.1 (crate (name "glotaran_converter_lib") (vers "0.1.0") (deps (list (crate-dep (name "csv") (req "^1.2.2") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.9.5") (default-features #t) (kind 0)))) (hash "1biiv3pjsgfzd7xjdsbqrl505wyahqzid3kj10m437prm9ya6186")))

(define-public crate-glotaran_converter_lib-0.1 (crate (name "glotaran_converter_lib") (vers "0.1.1") (deps (list (crate-dep (name "csv") (req "^1.2.2") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.9.5") (default-features #t) (kind 0)))) (hash "1a1djvah5fn29d17qk43i2jm3cx0qs2998z9ypwrvi43zxx48jbr")))

(define-public crate-glotaran_converter_lib-0.1 (crate (name "glotaran_converter_lib") (vers "0.1.2") (deps (list (crate-dep (name "csv") (req "^1.2.2") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.9.5") (default-features #t) (kind 0)))) (hash "1z0lzblnv31vx1mszxl2jz0w7rq22djirxmcy3w6gaib0f1yw88f")))

(define-public crate-glotaran_converter_lib-0.1 (crate (name "glotaran_converter_lib") (vers "0.1.3") (deps (list (crate-dep (name "csv") (req "^1.2.2") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.9.5") (default-features #t) (kind 0)))) (hash "0jvjj5nb9nij21g48bmmgnlvpkpmjsnc0jg7hl8ls8v921z9zvv1")))

