(define-module (crates-io gl ma) #:use-module (crates-io))

(define-public crate-glmath-0.1 (crate (name "glmath") (vers "0.1.0") (hash "031kr2y58k84spihz0z97dqifdlyvp2fcx1nb0lhvn85qa9icn6z")))

(define-public crate-glmatrix-0.1 (crate (name "glmatrix") (vers "0.1.0") (hash "1lclf2gdrdij1f6dwx34m6r3wbiwag4y9rm1r06d6msddwqlpwwq")))

