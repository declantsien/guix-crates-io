(define-module (crates-io gl ru) #:use-module (crates-io))

(define-public crate-glrun-0.1 (crate (name "glrun") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^3.1.4") (default-features #t) (kind 0)) (crate-dep (name "colored") (req "^2.0.0") (default-features #t) (kind 0)) (crate-dep (name "isahc") (req "^1.7.2") (default-features #t) (kind 0)) (crate-dep (name "url") (req "^2.2.2") (default-features #t) (kind 0)))) (hash "1zihjh8s8ja2djjx88rk9psfs8cczffwb1r46f4n1clva5nl45i9")))

(define-public crate-glrun-0.1 (crate (name "glrun") (vers "0.1.1") (deps (list (crate-dep (name "clap") (req "^3.1.4") (default-features #t) (kind 0)) (crate-dep (name "colored") (req "^2.0.0") (default-features #t) (kind 0)) (crate-dep (name "isahc") (req "^1.7.2") (default-features #t) (kind 0)) (crate-dep (name "url") (req "^2.2.2") (default-features #t) (kind 0)))) (hash "02xc52laa43a1ja6hndnhkplr4l7dgvf5jq3gz447kxqajphxwm2")))

(define-public crate-glrun-0.1 (crate (name "glrun") (vers "0.1.2") (deps (list (crate-dep (name "clap") (req "^4.5.3") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "colored") (req "^2.1.0") (default-features #t) (kind 0)) (crate-dep (name "isahc") (req "^1.7.2") (default-features #t) (kind 0)) (crate-dep (name "url") (req "^2.2.2") (default-features #t) (kind 0)))) (hash "0m8jaabgsjfgqv4r3gv0ch41jfr4rngwis3rwryd9a8mcpbphyqf")))

