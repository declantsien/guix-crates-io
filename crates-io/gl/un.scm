(define-module (crates-io gl un) #:use-module (crates-io))

(define-public crate-gluni-0.1 (crate (name "gluni") (vers "0.1.0") (deps (list (crate-dep (name "posh") (req "^0.1") (default-features #t) (kind 0)))) (hash "1yvdi60l0yy05jiamamazdaq41qwcp988i7jamb6ydlyjinh9s1s") (yanked #t)))

(define-public crate-gluni-0.1 (crate (name "gluni") (vers "0.1.1") (hash "1acqb60r0sc8kr8y4l0wp01zz7pbhmyzq36zr5kz2w6my3ijzb61")))

(define-public crate-gluni-0.1 (crate (name "gluni") (vers "0.1.2") (hash "1vlqqqi46qd1x1nfii7763m3ahf13yi89bggh4zgaazgang03ky8")))

(define-public crate-gluni-0.1 (crate (name "gluni") (vers "0.1.3") (hash "1isk3hbsd0dxfccv3qjw0gm60bqlkgkkra8rgzkagzpmbpjal0qa")))

(define-public crate-gluni-0.1 (crate (name "gluni") (vers "0.1.4") (hash "076bb3wwxnhaf3aws3dav2593qs1rfhphbxj187gj2nil1y3y4v5")))

