(define-module (crates-io gl ip) #:use-module (crates-io))

(define-public crate-glip-0.3 (crate (name "glip") (vers "0.3.0") (deps (list (crate-dep (name "clap") (req "^2.32") (default-features #t) (kind 0)) (crate-dep (name "libflate") (req "^0.1.19") (default-features #t) (kind 0)) (crate-dep (name "maxminddb") (req "^0.12.0") (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.9.8") (default-features #t) (kind 0)))) (hash "0pc7b9y52dj2sjgmk1jrmj9n5g2gsngms3pbkzjpbf1x3da7jpiy")))

(define-public crate-glip-0.5 (crate (name "glip") (vers "0.5.0") (deps (list (crate-dep (name "clap") (req "^2.32") (default-features #t) (kind 0)) (crate-dep (name "failure") (req "^0.1.5") (default-features #t) (kind 0)) (crate-dep (name "libflate") (req "^0.1.19") (default-features #t) (kind 0)) (crate-dep (name "maxminddb") (req "^0.12.0") (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.9.8") (default-features #t) (kind 0)))) (hash "15048465xmkg7807i0sm069s5xy3fd1bkpjikdhwmr6dcblsy4xv")))

(define-public crate-glip-0.6 (crate (name "glip") (vers "0.6.0") (deps (list (crate-dep (name "clap") (req "^2.32") (default-features #t) (kind 0)) (crate-dep (name "libflate") (req "^0.1.19") (default-features #t) (kind 0)) (crate-dep (name "maxminddb") (req "^0.12.0") (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.9.8") (default-features #t) (kind 0)))) (hash "1w783n3gkd5vhij1axpr864i2gbwj2vbbz8kk3ivbl26169icly5")))

