(define-module (crates-io gl -a) #:use-module (crates-io))

(define-public crate-gl-abstractions-0.1 (crate (name "gl-abstractions") (vers "0.1.0") (deps (list (crate-dep (name "glad-sys") (req "^0.0.4") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "glfw-sys") (req "^3.2.2") (optional #t) (default-features #t) (kind 0)))) (hash "10w1kw2wqr9h4mfk1jl2f7lkdqfgcv0k1a7ph2y14l56g0whg2gf") (features (quote (("glfw" "glfw-sys") ("glad" "glad-sys") ("full" "glad-sys" "glfw-sys") ("default")))) (yanked #t)))

