(define-module (crates-io gl ul) #:use-module (crates-io))

(define-public crate-glulxe-0.1 (crate (name "glulxe") (vers "0.1.0") (deps (list (crate-dep (name "glk") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "glulxe-sys") (req "^0.1") (default-features #t) (kind 0)))) (hash "1nqs377flg3521k5ksmicrh0344rh13gr8fy6xpmd8767mpb1xsd")))

(define-public crate-glulxe-0.1 (crate (name "glulxe") (vers "0.1.1") (deps (list (crate-dep (name "glk-sys") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "glulxe-sys") (req "^0.1") (default-features #t) (kind 0)))) (hash "0pkhz8lnyvrnijw54wyr2njz6mr8sa2rac9p2v4v2chg3vanx1c6")))

(define-public crate-glulxe-0.2 (crate (name "glulxe") (vers "0.2.0") (deps (list (crate-dep (name "glk-sys") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "glulxe-sys") (req "^0.2") (default-features #t) (kind 0)))) (hash "1l83assqi9gfknzh1fgc5hlmjcfh7shnginfhl5qsv8jrcll6zyi")))

(define-public crate-glulxe-sys-0.1 (crate (name "glulxe-sys") (vers "0.1.0") (deps (list (crate-dep (name "bindgen") (req "^0.51") (default-features #t) (kind 1)) (crate-dep (name "cc") (req "^1.0") (default-features #t) (kind 1)) (crate-dep (name "glk") (req "^0.1") (default-features #t) (kind 0)))) (hash "1fv18sgsvpd5gvb54ljy4n7n44v5fw7px0z1yxsg2h50pr4yl49x") (yanked #t)))

(define-public crate-glulxe-sys-0.1 (crate (name "glulxe-sys") (vers "0.1.1") (deps (list (crate-dep (name "bindgen") (req "^0.51") (default-features #t) (kind 1)) (crate-dep (name "cc") (req "^1.0") (default-features #t) (kind 1)) (crate-dep (name "glk-sys") (req "^0.1") (default-features #t) (kind 0)))) (hash "0fglmjrccs0q3r06rnrpws3wyhi4hlacx4ymi4026lgfs7zm7n2s")))

(define-public crate-glulxe-sys-0.2 (crate (name "glulxe-sys") (vers "0.2.0") (deps (list (crate-dep (name "bindgen") (req "^0.51") (default-features #t) (kind 1)) (crate-dep (name "cc") (req "^1.0") (default-features #t) (kind 1)) (crate-dep (name "glk-sys") (req "^0.2") (default-features #t) (kind 0)))) (hash "0hf508hyrdmshbb9i9a9a2v5lk7qqy3489sg7sxgg2pz4z7akczc")))

(define-public crate-glulxtoc-0.1 (crate (name "glulxtoc") (vers "0.1.0") (deps (list (crate-dep (name "bytes") (req "^1.0.1") (default-features #t) (kind 0)) (crate-dep (name "dyn-fmt") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "fnv") (req "^1.0.7") (default-features #t) (kind 0)) (crate-dep (name "if-decompiler") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "quick-xml") (req "^0.22") (default-features #t) (kind 0)) (crate-dep (name "relooper") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.3.13") (default-features #t) (kind 0)))) (hash "0yay8314fj8sjb9qszzsqx1mkfbiwbkvaa76nygqqml2rzvwp9mz")))

