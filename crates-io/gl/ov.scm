(define-module (crates-io gl ov) #:use-module (crates-io))

(define-public crate-glovebox-0.0.1 (crate (name "glovebox") (vers "0.0.1") (hash "0isvxmvsrsasw8mdnkyz2s2m8s3qh9lwbxhkp8cm7jixhs053vaw")))

(define-public crate-glovebox-0.0.2 (crate (name "glovebox") (vers "0.0.2") (hash "0bq5zhdphy629im12b9bqs59cqg7a4nkclqa4cijg006b9a0nqsh")))

