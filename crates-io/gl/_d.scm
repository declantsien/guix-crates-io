(define-module (crates-io gl _d) #:use-module (crates-io))

(define-public crate-gl_dstruct-0.1 (crate (name "gl_dstruct") (vers "0.1.0") (deps (list (crate-dep (name "gl_generator") (req "^0.14.0") (default-features #t) (kind 1)))) (hash "1dinz6kn5w708k9cgq8yrji2vi5qv0b0hda38lkpas6cl0rs626y") (features (quote (("debug"))))))

(define-public crate-gl_dstruct-0.2 (crate (name "gl_dstruct") (vers "0.2.0") (deps (list (crate-dep (name "gl_generator") (req "^0.14.0") (default-features #t) (kind 1)))) (hash "1mddl8wh61w6a3yly2ql3qfzgz9iz30jn9hc787678b9q8bwh3i9") (features (quote (("debug"))))))

