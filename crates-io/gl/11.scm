(define-module (crates-io gl #{11}#) #:use-module (crates-io))

(define-public crate-gl11-0.1 (crate (name "gl11") (vers "0.1.0") (deps (list (crate-dep (name "gl_generator") (req "^0.14") (default-features #t) (kind 1)) (crate-dep (name "glfw") (req "^0.51") (default-features #t) (kind 2)))) (hash "1072gs9i1mcvq7rxprvzkrfb4js6idvn1469q9zvf7kmgm9h8whm")))

(define-public crate-gl11-0.1 (crate (name "gl11") (vers "0.1.1") (deps (list (crate-dep (name "gl_generator") (req "^0.14") (default-features #t) (kind 1)) (crate-dep (name "glfw") (req "^0.51") (default-features #t) (kind 2)))) (hash "0dni2innvkwrq13q5n8l9sd8im2g6s2c3ppkhvm05x0947xbmhsi")))

