(define-module (crates-io gl io) #:use-module (crates-io))

(define-public crate-glio-0.0.1 (crate (name "glio") (vers "0.0.1") (deps (list (crate-dep (name "gl") (req "^0.14.0") (default-features #t) (kind 0)))) (hash "11f876h02njcls2pbxjjrdpmfhfsi07rgh3n2sdhscz79grn681h") (yanked #t)))

(define-public crate-glio-0.0.2 (crate (name "glio") (vers "0.0.2") (deps (list (crate-dep (name "gl") (req "^0.14.0") (default-features #t) (kind 0)))) (hash "1kd9yxywvfrb1p9mj7rpnpj66jm62lxc908kngh7vhz6gyl6rqd6") (yanked #t)))

(define-public crate-glio-0.0.3 (crate (name "glio") (vers "0.0.3") (deps (list (crate-dep (name "gl") (req "^0.14.0") (default-features #t) (kind 0)))) (hash "0smqawr7s9a17vglgjl72lxjxvmk43nigbiimj8d2wbssdn8mpaz") (yanked #t)))

