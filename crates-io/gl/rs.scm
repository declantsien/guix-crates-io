(define-module (crates-io gl rs) #:use-module (crates-io))

(define-public crate-glrs-0.1 (crate (name "glrs") (vers "0.1.0") (deps (list (crate-dep (name "glam") (req "^0.25.0") (default-features #t) (kind 0)) (crate-dep (name "glsl") (req "^7.0.0") (default-features #t) (kind 0)) (crate-dep (name "proc-macro-error") (req "^1.0.4") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1.0.76") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.35") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2.0.48") (features (quote ("full"))) (default-features #t) (kind 0)) (crate-dep (name "syn-path") (req "^2.0.0") (default-features #t) (kind 0)))) (hash "0gm5v7jgk42b1dvims49arx0g9rvl93ifpyrh5yr4khl88h7w4nj") (rust-version "1.75")))

