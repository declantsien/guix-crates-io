(define-module (crates-io gl #{32}#) #:use-module (crates-io))

(define-public crate-gl32-0.1 (crate (name "gl32") (vers "0.1.0") (deps (list (crate-dep (name "gl_generator") (req "~0.10.0") (default-features #t) (kind 1)) (crate-dep (name "glutin") (req "~0.19.0") (default-features #t) (kind 2)))) (hash "1pmba8kkz4jm6a4mzn7j4b441afxb5h825srzkjdxmnp5hl2b7cf")))

(define-public crate-gl32-0.1 (crate (name "gl32") (vers "0.1.1") (deps (list (crate-dep (name "gl_generator") (req "~0.10.0") (default-features #t) (kind 1)) (crate-dep (name "glutin") (req "~0.19.0") (default-features #t) (kind 2)))) (hash "1vrp1vv9l5zvbvam7k4azkmbrw68g2gkhj7zp7kqh1zmjnh9l2hd")))

(define-public crate-gl32-0.2 (crate (name "gl32") (vers "0.2.0") (deps (list (crate-dep (name "gl_generator") (req "~0.10.0") (default-features #t) (kind 1)) (crate-dep (name "glutin") (req "~0.19.0") (default-features #t) (kind 2)))) (hash "12n83zihxxiva3k2fd8s4kfgfnlc50p7pphj02znpxkpr1rga0a5")))

