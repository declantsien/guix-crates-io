(define-module (crates-io gl pk) #:use-module (crates-io))

(define-public crate-glpk-sys-0.1 (crate (name "glpk-sys") (vers "0.1.0") (deps (list (crate-dep (name "libc") (req "^0.1") (default-features #t) (kind 0)))) (hash "0izh4cjahxcr33dm7272mxm2ixqyywjn00vmi5qbgcl45dl3dv7l")))

(define-public crate-glpk-sys-0.2 (crate (name "glpk-sys") (vers "0.2.0") (deps (list (crate-dep (name "libc") (req "^0.1") (default-features #t) (kind 0)))) (hash "02sb258p1ld855dc4il3n1gm7rckcdf80xrh992w7v8dixh8wd2d")))

