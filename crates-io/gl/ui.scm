(define-module (crates-io gl ui) #:use-module (crates-io))

(define-public crate-glui-0.0.0 (crate (name "glui") (vers "0.0.0") (hash "056ir7h5irp8sgwp6wisicp62n3vgrz05fakmnyds7jry9dq7cq7")))

(define-public crate-gluino-0.1 (crate (name "gluino") (vers "0.1.0") (deps (list (crate-dep (name "serde") (req "^1.0.143") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "0czgg6s44w9b00fiwvwhi1mkbnyf7mxxxbklhwxs4fh3csy7w082")))

