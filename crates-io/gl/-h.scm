(define-module (crates-io gl -h) #:use-module (crates-io))

(define-public crate-gl-headless-0.1 (crate (name "gl-headless") (vers "0.1.0") (deps (list (crate-dep (name "gl") (req "^0.14") (default-features #t) (kind 0)) (crate-dep (name "glfw") (req "^0.51") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "172v991a7k0anzp1vcrqw36gljjsw2k4lqfamkkrajq8la9q92m1")))

(define-public crate-gl-headless-0.2 (crate (name "gl-headless") (vers "0.2.0") (deps (list (crate-dep (name "gl") (req "^0.14") (default-features #t) (kind 0)) (crate-dep (name "gl-headless-macros") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "glfw") (req "^0.51") (default-features #t) (kind 0)))) (hash "1hr0midmprs7xcraffcbjb0s35i424ni6fpl9yprhy4xwn5585v8") (yanked #t)))

(define-public crate-gl-headless-0.2 (crate (name "gl-headless") (vers "0.2.1") (deps (list (crate-dep (name "gl") (req "^0.14") (default-features #t) (kind 0)) (crate-dep (name "gl-headless-macros") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "glfw") (req "^0.51") (default-features #t) (kind 0)))) (hash "1lc9ypfijmm15j2dxmyq31l94yyfjclrv8myymld6576zq7z1dz9")))

(define-public crate-gl-headless-0.2 (crate (name "gl-headless") (vers "0.2.2") (deps (list (crate-dep (name "gl") (req "^0.14") (default-features #t) (kind 0)) (crate-dep (name "gl-headless-macros") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "glfw") (req "^0.51") (default-features #t) (kind 0)))) (hash "0c592pgnp6z656ljyhn0cqzqq5i1rr2rj3c4p47cmqvms02md8g2")))

(define-public crate-gl-headless-macros-0.1 (crate (name "gl-headless-macros") (vers "0.1.0") (deps (list (crate-dep (name "proc-macro2") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "1cjfjnfniq0bnkw9w284149nfy41sdkzwr0b1v8v05i1mr2l3znc")))

(define-public crate-gl-headless-macros-0.1 (crate (name "gl-headless-macros") (vers "0.1.1") (deps (list (crate-dep (name "proc-macro2") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "03z62w9xxrmdppszdcrvn8s2a0675ngm1lxv8bxjn0626ky62bci")))

