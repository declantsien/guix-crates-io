(define-module (crates-io gl l-) #:use-module (crates-io))

(define-public crate-gll-macros-0.0.1 (crate (name "gll-macros") (vers "0.0.1") (deps (list (crate-dep (name "gll") (req "^0.0.1") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^0.6") (default-features #t) (kind 2)))) (hash "0581ihax7ha559dsi4xijly5nshys33rzji54vrmzkxiz4zdrq6a")))

(define-public crate-gll-macros-0.0.2 (crate (name "gll-macros") (vers "0.0.2") (deps (list (crate-dep (name "gll") (req "^0.0.2") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^0.6") (default-features #t) (kind 2)))) (hash "1xsny4zrvpj51i3bbkm9xvjyf7fqg7ic7wjzysazfvyhk3a0izds")))

(define-public crate-gll-pg-core-0.1 (crate (name "gll-pg-core") (vers "0.1.0") (deps (list (crate-dep (name "logos") (req "^0.9") (default-features #t) (kind 0)) (crate-dep (name "petgraph") (req "^0.4") (default-features #t) (kind 0)))) (hash "00pydwblxq0wh591bqvnmsq8kcnyj3i7j7i7p3awainard81jg0i")))

(define-public crate-gll-pg-core-0.2 (crate (name "gll-pg-core") (vers "0.2.0") (deps (list (crate-dep (name "logos") (req "^0.9") (default-features #t) (kind 0)) (crate-dep (name "petgraph") (req "^0.4") (default-features #t) (kind 0)))) (hash "08hm2yf29nkllqf2n4s7zsda86crkifcf8vzfpcbh4i0y9s5k4c1")))

(define-public crate-gll-pg-core-0.2 (crate (name "gll-pg-core") (vers "0.2.1") (deps (list (crate-dep (name "logos") (req "^0.9") (default-features #t) (kind 0)) (crate-dep (name "petgraph") (req "^0.4") (default-features #t) (kind 0)))) (hash "168zvi24yxmpakk81rb9g93cn83fr9b07dih297zljbgxn3b8wkw")))

(define-public crate-gll-pg-core-0.3 (crate (name "gll-pg-core") (vers "0.3.0") (deps (list (crate-dep (name "logos") (req "^0.9") (default-features #t) (kind 0)) (crate-dep (name "petgraph") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "streaming-iterator") (req "^0.1") (default-features #t) (kind 0)))) (hash "1v2cdi1xpf7l5xh6vakvpj6g97rh8kylmn3f5rkjmvnlnk3l2y9w")))

(define-public crate-gll-pg-core-0.4 (crate (name "gll-pg-core") (vers "0.4.0") (deps (list (crate-dep (name "logos") (req "^0.11.4") (default-features #t) (kind 0)) (crate-dep (name "petgraph") (req "^0.5.1") (default-features #t) (kind 0)) (crate-dep (name "streaming-iterator") (req "^0.1.5") (default-features #t) (kind 0)))) (hash "19173s08i22isnrgmsfnrs0d1kwsgr10yp2s1jaadz7mz5yhlkv0")))

(define-public crate-gll-pg-core-0.5 (crate (name "gll-pg-core") (vers "0.5.0") (deps (list (crate-dep (name "logos") (req "^0.11.4") (default-features #t) (kind 0)) (crate-dep (name "petgraph") (req "^0.6.4") (default-features #t) (kind 0)) (crate-dep (name "streaming-iterator") (req "^0.1.5") (default-features #t) (kind 0)))) (hash "0dr98lfg3fgnixh3j9ahv3ydbh87y6hdz40w6vl7fkk85590ivxx")))

(define-public crate-gll-pg-macros-0.1 (crate (name "gll-pg-macros") (vers "0.1.0") (deps (list (crate-dep (name "aho-corasick") (req "^0.7") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (features (quote ("extra-traits" "full"))) (default-features #t) (kind 0)))) (hash "03s1rqgvxzr9234cn2sdvhy5acn819xr54g3s9hl7iblsj87404n")))

(define-public crate-gll-pg-macros-0.2 (crate (name "gll-pg-macros") (vers "0.2.0") (deps (list (crate-dep (name "aho-corasick") (req "^0.7") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (features (quote ("extra-traits" "full"))) (default-features #t) (kind 0)))) (hash "0pds53jx5xkgdrx428y69jh333blpclxqxa32asl2wlyg1mbd9r5")))

(define-public crate-gll-pg-macros-0.2 (crate (name "gll-pg-macros") (vers "0.2.1") (deps (list (crate-dep (name "aho-corasick") (req "^0.7") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (features (quote ("extra-traits" "full"))) (default-features #t) (kind 0)))) (hash "16vfzf6cvfrajrybxv3hndsv09qlnrbirrgv4hg4r86zz31ygia6")))

(define-public crate-gll-pg-macros-0.3 (crate (name "gll-pg-macros") (vers "0.3.0") (deps (list (crate-dep (name "aho-corasick") (req "^0.7") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (features (quote ("extra-traits" "full"))) (default-features #t) (kind 0)))) (hash "0km82zcgxk9rblqs357jgpl3p7gg7nz4vy76xa5qiavxh4qn6ksa")))

(define-public crate-gll-pg-macros-0.4 (crate (name "gll-pg-macros") (vers "0.4.0") (deps (list (crate-dep (name "aho-corasick") (req "^0.7.15") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.8") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.58") (features (quote ("extra-traits" "full"))) (default-features #t) (kind 0)))) (hash "15r280kfw1kiivrrq63blq3g1am6p3isqp1pv3jbgprpbw3f23v1")))

(define-public crate-gll-pg-macros-0.5 (crate (name "gll-pg-macros") (vers "0.5.0") (deps (list (crate-dep (name "aho-corasick") (req "^1.1.2") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.8") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.109") (features (quote ("extra-traits" "full"))) (default-features #t) (kind 0)))) (hash "0fi9qk6yw3fqgzxnv0pwrfbnphdwb1lbdgfm534l0z7rk0l3wgw2")))

