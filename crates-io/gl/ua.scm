(define-module (crates-io gl ua) #:use-module (crates-io))

(define-public crate-glua-0.1 (crate (name "glua") (vers "0.1.0") (hash "11c1caxy1jlwmn83zxffqgdx8fw3akcbbppz4ikzzsq12pb7cx2i")))

(define-public crate-gluac-rs-0.1 (crate (name "gluac-rs") (vers "0.1.0") (deps (list (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "libloading") (req "^0.7.0") (default-features #t) (kind 0)) (crate-dep (name "parking_lot") (req "^0.11") (optional #t) (default-features #t) (kind 0)))) (hash "1p1bvmdlc0rzjjpl3r7qm7znkffv15drw7z0kf3b8abydrr07pva") (yanked #t)))

(define-public crate-gluac-rs-0.1 (crate (name "gluac-rs") (vers "0.1.1") (deps (list (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "libloading") (req "^0.7.0") (default-features #t) (kind 0)) (crate-dep (name "parking_lot") (req "^0.11") (optional #t) (default-features #t) (kind 0)))) (hash "1kirlc0mxlysgwnj7z632dyl5z3s1fjch57x1ryjfjm1qfgk10hy")))

(define-public crate-gluac-rs-0.1 (crate (name "gluac-rs") (vers "0.1.3") (deps (list (crate-dep (name "clap") (req "^2.33.3") (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "libloading") (req "^0.7.0") (default-features #t) (kind 0)) (crate-dep (name "parking_lot") (req "^0.11") (optional #t) (default-features #t) (kind 0)))) (hash "1r3aq8zlvy4cprywkzwks3gqjjpz37ykj5fz69ynpy0970wbk09a")))

