(define-module (crates-io gl cp) #:use-module (crates-io))

(define-public crate-glcp-0.1 (crate (name "glcp") (vers "0.1.0") (deps (list (crate-dep (name "anyhow") (req "^1.0.70") (default-features #t) (kind 0)))) (hash "0mn6w3z8vaxjg7wy2gxqha2v3qsim6xyjvv2pkn3w575jmy7jpga")))

(define-public crate-glcp-0.1 (crate (name "glcp") (vers "0.1.1") (deps (list (crate-dep (name "anyhow") (req "^1.0.70") (default-features #t) (kind 0)))) (hash "1zjnwsi162j4fvkywa4p6210w19nrqnfkqbxrjxg3xm92q8i1g1s")))

(define-public crate-glcp-0.1 (crate (name "glcp") (vers "0.1.2") (deps (list (crate-dep (name "anyhow") (req "^1.0.70") (default-features #t) (kind 0)))) (hash "1p0a7qzdkhmdbcjf6q40a7kbr24ajn5d9pqqm8i3yzf29q8wp1wh")))

