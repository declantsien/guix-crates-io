(define-module (crates-io ow ni) #:use-module (crates-io))

(define-public crate-owning_ref-0.1 (crate (name "owning_ref") (vers "0.1.0") (hash "0rflzsklgk2hmir1v0gy8xq087ipyhravwlxh0dzljpcrxma0lg9") (features (quote (("nightly"))))))

(define-public crate-owning_ref-0.1 (crate (name "owning_ref") (vers "0.1.1") (hash "067fm2vd9qjzz060vjbx05f7n2a4vs97p5cv7p8g4df5g5b81c1s") (features (quote (("nightly"))))))

(define-public crate-owning_ref-0.1 (crate (name "owning_ref") (vers "0.1.2") (hash "0z2i3nka288nrr5bvzk9pykypqnz7vb92ap2n6irhdxdrjflcimy") (features (quote (("nightly"))))))

(define-public crate-owning_ref-0.1 (crate (name "owning_ref") (vers "0.1.3") (hash "1pfm8wkpngr93s4crhgcg1jnns95zkgwa52rvfi0xyhdqabvnhf5")))

(define-public crate-owning_ref-0.1 (crate (name "owning_ref") (vers "0.1.4") (hash "0zczmpkny5j8s6rpcy8nb7w0mxaiz8vwj5cz3ind2h92fwc36z2d")))

(define-public crate-owning_ref-0.2 (crate (name "owning_ref") (vers "0.2.0") (hash "1wzdyar8xv89bwq70axjpnziqkwkjhh3nrj2qp0qddkggm6a39kv")))

(define-public crate-owning_ref-0.2 (crate (name "owning_ref") (vers "0.2.1") (hash "0g99hc9006dn001chhd7x623rnw5p79vafczdbncn1vmhc2la248")))

(define-public crate-owning_ref-0.2 (crate (name "owning_ref") (vers "0.2.2") (hash "1rvm7p721v6l07zl8s5affj1n4d0hilb2yi8g902d51mhmq3g4cd")))

(define-public crate-owning_ref-0.2 (crate (name "owning_ref") (vers "0.2.3") (hash "1vay8m3wivsbcdwd7s0il7b0r6q5id3zngagqicjdrkb4p0fkhji")))

(define-public crate-owning_ref-0.2 (crate (name "owning_ref") (vers "0.2.4") (hash "1k5nmk88a6jvkkaivmgw4gvksfbysvc1h6lh1j89qbmlvhfmfllx")))

(define-public crate-owning_ref-0.3 (crate (name "owning_ref") (vers "0.3.0") (hash "0zmqcp4c2dxi8gx50rwjlxb544d99hpnn3dapgyg1qrhf0pa9b1x")))

(define-public crate-owning_ref-0.3 (crate (name "owning_ref") (vers "0.3.1") (hash "0pnh79g4k9v9n9sr15w9ygffls3b3s6818jly283ixzl6sj3bn9p")))

(define-public crate-owning_ref-0.3 (crate (name "owning_ref") (vers "0.3.2") (hash "13v9gzyk7i59vh6wxzd16b7c0nhcxc4c5kral07hn17h7gav0fji")))

(define-public crate-owning_ref-0.3 (crate (name "owning_ref") (vers "0.3.3") (deps (list (crate-dep (name "stable_deref_trait") (req "^1.0.0") (default-features #t) (kind 0)))) (hash "0dqgf5hwbmvkf2ffbik5xmhvaqvqi6iklhwk9x47n0wycd0lzy6d")))

(define-public crate-owning_ref-0.4 (crate (name "owning_ref") (vers "0.4.0") (deps (list (crate-dep (name "stable_deref_trait") (req "^1.0.0") (default-features #t) (kind 0)))) (hash "04zgwy77lin8qz398s6g44467pd6kjhbrlqifkia5rkr47mbi929")))

(define-public crate-owning_ref-0.4 (crate (name "owning_ref") (vers "0.4.1") (deps (list (crate-dep (name "stable_deref_trait") (req "^1.0.0") (default-features #t) (kind 0)))) (hash "1kjj9m28wjv452jw49p1mp3d8ql058x78v4bz00avr7rvsnmpxbg")))

(define-public crate-owning_ref_async-0.4 (crate (name "owning_ref_async") (vers "0.4.3") (deps (list (crate-dep (name "stable_deref_trait") (req "^1.0.0") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^0.2.22") (features (quote ("macros" "rt-threaded" "time"))) (default-features #t) (kind 2)))) (hash "0if4rxi9if37cjhcnysh5j04r4j9lq3gwrz6w546vwgkv36wa08k") (features (quote (("async")))) (yanked #t)))

(define-public crate-owning_ref_async-0.4 (crate (name "owning_ref_async") (vers "0.4.4") (deps (list (crate-dep (name "stable_deref_trait") (req "^1.0.0") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^0.2.22") (features (quote ("macros" "rt-threaded" "time"))) (default-features #t) (kind 2)))) (hash "1n978h825iljpm18mc21r0v5w2kgfdccbljwqjqai3cqv5j5g1ri") (features (quote (("async"))))))

(define-public crate-owning_ref_async-0.4 (crate (name "owning_ref_async") (vers "0.4.5") (deps (list (crate-dep (name "stable_deref_trait") (req "^1.0.0") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^0.2.22") (features (quote ("macros" "rt-threaded" "time"))) (default-features #t) (kind 2)))) (hash "0821289lqyp04lf2gb0rvm5n5kzh2fccgjxklba92qcgkv3n9vgc") (features (quote (("async"))))))

(define-public crate-owning_ref_lockable-0.4 (crate (name "owning_ref_lockable") (vers "0.4.2") (deps (list (crate-dep (name "stable_deref_trait") (req "^1.0.0") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.17.0") (features (quote ("rt" "macros"))) (default-features #t) (kind 2)))) (hash "0yydz8zkvb8vk9nymdqcqnrmvzsas5f2l5j3k2wqhg7vba043rdn")))

