(define-module (crates-io ow ml) #:use-module (crates-io))

(define-public crate-owml-parser-0.1 (crate (name "owml-parser") (vers "0.1.0") (deps (list (crate-dep (name "nom") (req "^5.0.0") (default-features #t) (kind 0)))) (hash "1xq8g5snjnbg64znywcz23g3c5p67dcqf6dmp5a5rcygigprpwk3")))

(define-public crate-owml-parser-0.1 (crate (name "owml-parser") (vers "0.1.1") (deps (list (crate-dep (name "nom") (req "^5.0.0") (default-features #t) (kind 0)))) (hash "143csbrp8vj36l7wdqvmp7vr27z0sagh0iyxly6qsiri5dgz67pi")))

(define-public crate-owml-parser-0.1 (crate (name "owml-parser") (vers "0.1.2") (deps (list (crate-dep (name "nom") (req "^5.0.0") (default-features #t) (kind 0)))) (hash "1p75ahl22n9x7lrf205jvhg68jqmqydqyz8imnrs6lv5b7x3g9v9")))

