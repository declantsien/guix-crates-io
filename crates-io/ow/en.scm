(define-module (crates-io ow en) #:use-module (crates-io))

(define-public crate-owen-0.1 (crate (name "owen") (vers "0.1.0") (deps (list (crate-dep (name "crossterm") (req "^0.26.0") (default-features #t) (kind 0)))) (hash "18d1418r45p9bdz9fl6hqkpp8q0fgi3fisw5mjx7qdh6njzsr31g") (yanked #t)))

(define-public crate-owen-0.0.1 (crate (name "owen") (vers "0.0.1") (deps (list (crate-dep (name "crossterm") (req "^0.26.0") (default-features #t) (kind 0)))) (hash "0sm4mc2hj88q057hz1cp317ym1vx8rfnmdqcbqc3wm9k971zd8rl") (yanked #t)))

(define-public crate-owen-0.0.0 (crate (name "owen") (vers "0.0.0") (hash "1vhd42ykic118sp728jq6dmyz6bx67klrsb5m6233qi3xcdxl562")))

(define-public crate-owens-ml-parser-0.0.0 (crate (name "owens-ml-parser") (vers "0.0.0") (deps (list (crate-dep (name "nom") (req "^5.0.0") (default-features #t) (kind 0)))) (hash "1ip6mlrwb151hz3k13y625n2gpnqcwn6gq3sw0gmn1kbjrscda4f") (yanked #t)))

