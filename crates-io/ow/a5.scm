(define-module (crates-io ow a5) #:use-module (crates-io))

(define-public crate-owa5x-sys-0.1 (crate (name "owa5x-sys") (vers "0.1.0") (deps (list (crate-dep (name "bindgen") (req "^0.54.0") (default-features #t) (kind 1)))) (hash "1nhqz0fnsmcp623s6srlyv19k8lpwg2mplxvr3rybi43s414cgb2")))

