(define-module (crates-io ow ma) #:use-module (crates-io))

(define-public crate-owmath-0.0.1 (crate (name "owmath") (vers "0.0.1") (deps (list (crate-dep (name "num-traits") (req "^0.2.8") (default-features #t) (kind 0)))) (hash "066w03s7jrsjcn9caps4lms95gc70d0ncg10pbajqj9cin31qz2c")))

(define-public crate-owmath-0.0.2 (crate (name "owmath") (vers "0.0.2") (deps (list (crate-dep (name "num-traits") (req "^0.2.8") (default-features #t) (kind 0)))) (hash "0d9pyqi60amq07yq8mgac3qgm4rvhqnij2zgdkbf7mh8s2wsr3yq")))

(define-public crate-owmath-0.0.3 (crate (name "owmath") (vers "0.0.3") (deps (list (crate-dep (name "num-traits") (req "^0.2.8") (default-features #t) (kind 0)))) (hash "11xrrzyn6s0453lpki961i4ldgz0j5ka1pqi4z47lpq7dcd24fc0")))

(define-public crate-owmath-0.0.4 (crate (name "owmath") (vers "0.0.4") (deps (list (crate-dep (name "duplicate") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2.8") (default-features #t) (kind 0)))) (hash "1dqm17l9d0igj3acgr94f1hvlhiphc88aqi408d0vf0vfccfil8d")))

(define-public crate-owmath-0.0.5 (crate (name "owmath") (vers "0.0.5") (deps (list (crate-dep (name "duplicate") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2.8") (default-features #t) (kind 0)))) (hash "0idqmpyry5qzgi7h5dl3mga47c2r52bgwbfbqm4xwy2b9ayxxjzb")))

(define-public crate-owmath-0.0.7 (crate (name "owmath") (vers "0.0.7") (deps (list (crate-dep (name "duplicate") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2.8") (default-features #t) (kind 0)))) (hash "0qyg77zqpzj4k22xy98kav8vjvf5jlams3wf5vg5wfbh27ikl6n8")))

(define-public crate-owmath-0.0.8 (crate (name "owmath") (vers "0.0.8") (deps (list (crate-dep (name "duplicate") (req "^0.3.0") (default-features #t) (kind 0)))) (hash "0k5x6xjp7826ihhgyv74j2xp8v1hs8gxnr5bg0kcyfa4hhf8kymy")))

