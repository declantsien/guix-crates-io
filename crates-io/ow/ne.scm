(define-module (crates-io ow ne) #:use-module (crates-io))

(define-public crate-owned-0.1 (crate (name "owned") (vers "0.1.0") (deps (list (crate-dep (name "dropcheck") (req "^0.1.0") (default-features #t) (kind 2)))) (hash "06197crjd2gccxiyzdwff3fiy94km1mijmg1ahmikabg0lzyphm7")))

(define-public crate-owned-alloc-0.1 (crate (name "owned-alloc") (vers "0.1.0") (hash "1rbpcyrgc13dra1r7mgbcjsfqy3qwzhf1c8xliys0lv5lbvsrv2f")))

(define-public crate-owned-alloc-0.2 (crate (name "owned-alloc") (vers "0.2.0") (hash "1mh54983yz8chn6lc4rgvhazys73dc129y654a9gy4ls3x0ypz1h")))

(define-public crate-owned-buf-0.0.0 (crate (name "owned-buf") (vers "0.0.0") (hash "01gzdq60yx6siw5487018ijr8fmi3p32dq5f49mva5fd9qj6zfhn")))

(define-public crate-owned-buf-0.1 (crate (name "owned-buf") (vers "0.1.0") (hash "0dgvp2rrpg2ylaw8yjfw7rik2i3r1y79mgm9zmmqnyd18gsch752")))

(define-public crate-owned-buf-0.3 (crate (name "owned-buf") (vers "0.3.0") (hash "03d07vyv1hdnh227jgl63sdj15wmx3mfag87rjcbvdi2nvh13cr9")))

(define-public crate-owned-drop-0.1 (crate (name "owned-drop") (vers "0.1.0") (hash "1d0x511f5ymg4bkkai5z074m6g415cwx2p0g94r6jwdji2azdw7p")))

(define-public crate-owned-drop-0.1 (crate (name "owned-drop") (vers "0.1.1") (hash "0fc8i56vva410qc42zmza167f3y40ab3wcp3zp214z0xmcbj5sjh")))

(define-public crate-owned-fd-0.1 (crate (name "owned-fd") (vers "0.1.0") (deps (list (crate-dep (name "libc") (req "0.*") (default-features #t) (kind 0)) (crate-dep (name "tempfile") (req "2.*") (default-features #t) (kind 2)))) (hash "1lisqlv5cr2gmz7l7j9c0l12lvzkfdfws5hcsnv38dpsk4ja4369")))

(define-public crate-owned-pin-0.1 (crate (name "owned-pin") (vers "0.1.0") (hash "0cx0j517ac49d9fkqk3i9ds6vpcz02jnfzj6s01pklvcx9n5xh43") (features (quote (("default" "alloc") ("alloc"))))))

(define-public crate-owned-pin-0.1 (crate (name "owned-pin") (vers "0.1.1") (hash "151ab9pc5wqjpqx9h0a19i1rn6202003ni1hmr438r67f0i80lq5") (features (quote (("default" "alloc") ("alloc"))))))

(define-public crate-owned-pin-0.2 (crate (name "owned-pin") (vers "0.2.0") (hash "05472jpa4igv86nvp8yjb2v9ymygpjpml3gzi80knf7ihln9s389") (features (quote (("default" "alloc") ("alloc"))))))

(define-public crate-owned-pin-1 (crate (name "owned-pin") (vers "1.0.0") (hash "1i08r6xfjff6p1ydgly0nvbfvqlclpqhw2x9wgrnmd25afv41qpq") (features (quote (("default" "alloc") ("alloc"))))))

(define-public crate-owned-pin-1 (crate (name "owned-pin") (vers "1.0.1") (hash "1vwj0s7wnqdjxhjdhshhdiaf2xwig5l90zri940lq4cbwhcs6jyy") (features (quote (("default" "alloc") ("alloc"))))))

(define-public crate-owned-pin-1 (crate (name "owned-pin") (vers "1.1.0") (hash "0bnbrcslaabg71slqznhc4nggni9n4j8vi7cnrjvi613239pxr99") (features (quote (("default" "alloc") ("alloc"))))))

(define-public crate-owned-pin-1 (crate (name "owned-pin") (vers "1.2.0") (deps (list (crate-dep (name "pinned-init") (req "^0.0") (optional #t) (kind 0)))) (hash "07kj08qccxrhdfdf948ay06jca033ml7ia0sf7m98jh11sl42lyq") (features (quote (("default" "alloc" "pinned-init") ("alloc" "pinned-init/alloc")))) (v 2) (features2 (quote (("pinned-init" "dep:pinned-init"))))))

(define-public crate-owned-pin-1 (crate (name "owned-pin") (vers "1.3.0") (deps (list (crate-dep (name "owned-pin-macros") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "pinned-init") (req "^0.0") (optional #t) (kind 0)))) (hash "0v7sw1ahcjzkkxmc1yvb1rk4nh2w5fw7qpvlwhdldvq6579cwqvm") (features (quote (("default" "alloc" "pinned-init") ("alloc" "pinned-init/alloc")))) (yanked #t) (v 2) (features2 (quote (("pinned-init" "dep:pinned-init"))))))

(define-public crate-owned-pin-1 (crate (name "owned-pin") (vers "1.3.1") (deps (list (crate-dep (name "owned-pin-macros") (req "^0.1") (default-features #t) (kind 0)) (crate-dep (name "pinned-init") (req "^0.0") (optional #t) (kind 0)))) (hash "0rb7r9rrrg4l0mafnvmqlbrq8j3abdyj759y79p8x6jzxq4j43dc") (features (quote (("default" "alloc" "pinned-init") ("alloc" "pinned-init/alloc")))) (v 2) (features2 (quote (("pinned-init" "dep:pinned-init"))))))

(define-public crate-owned-pin-1 (crate (name "owned-pin") (vers "1.3.2") (deps (list (crate-dep (name "owned-pin-macros") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "pinned-init") (req "^0.0") (optional #t) (kind 0)))) (hash "0a7yzws3xa52lbnpnpj78yzzc3iq71ka5fb9dyninr6aw0vf9wkf") (features (quote (("default" "alloc" "pinned-init") ("alloc" "pinned-init/alloc")))) (v 2) (features2 (quote (("pinned-init" "dep:pinned-init"))))))

(define-public crate-owned-pin-1 (crate (name "owned-pin") (vers "1.3.3") (deps (list (crate-dep (name "owned-pin-macros") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "pinned-init") (req "^0.0") (optional #t) (kind 0)))) (hash "06hqfpxvmj05mk9a9qd71c8c64a5mrbv8kcf983ms098l291wfi0") (features (quote (("default" "alloc" "pinned-init") ("alloc" "pinned-init/alloc")))) (v 2) (features2 (quote (("pinned-init" "dep:pinned-init"))))))

(define-public crate-owned-pin-macros-0.1 (crate (name "owned-pin-macros") (vers "0.1.0") (deps (list (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2.0") (default-features #t) (kind 0)))) (hash "1nfxvif6l53ij9ks3579il89b84j5gk2scq6wwc35pr2xnkakfh4")))

(define-public crate-owned-pin-macros-0.2 (crate (name "owned-pin-macros") (vers "0.2.0") (deps (list (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^2.0") (default-features #t) (kind 0)))) (hash "0yrc2wjz9r2fhk0iwqj0i8hnd8xppd5zhgan3i58k9f3zdk07sc3")))

(define-public crate-owned-read-0.1 (crate (name "owned-read") (vers "0.1.0") (deps (list (crate-dep (name "rental") (req "^0.5") (default-features #t) (kind 0)) (crate-dep (name "stable_deref_trait") (req "^1") (default-features #t) (kind 0)))) (hash "07s2yksd6wfzyp6sr3a6bfnxr1017dzr2qpawnfgc2vajg28c6l9")))

(define-public crate-owned-read-0.2 (crate (name "owned-read") (vers "0.2.0") (deps (list (crate-dep (name "rental") (req "^0.5") (default-features #t) (kind 0)) (crate-dep (name "stable_deref_trait") (req "^1") (default-features #t) (kind 0)))) (hash "1wilxlkid7m9jmrxcj7wafx8ljp481h7nnbgf30nvfbffgr8nf5j")))

(define-public crate-owned-read-0.3 (crate (name "owned-read") (vers "0.3.0") (deps (list (crate-dep (name "rental") (req "^0.5") (default-features #t) (kind 0)) (crate-dep (name "stable_deref_trait") (req "^1") (default-features #t) (kind 0)))) (hash "0zas7jxj22arf7daf1lfv926b1h4sm7bvz9cj9mpj9v60vnq4zh8")))

(define-public crate-owned-read-0.4 (crate (name "owned-read") (vers "0.4.0") (deps (list (crate-dep (name "rental") (req "^0.5") (default-features #t) (kind 0)) (crate-dep (name "stable_deref_trait") (req "^1") (default-features #t) (kind 0)))) (hash "0frd3gplrvhv00gj6957zl8m4nsvbaid8y5xzx6zq9yn32mpzm85")))

(define-public crate-owned-read-0.4 (crate (name "owned-read") (vers "0.4.1") (deps (list (crate-dep (name "stable_deref_trait") (req "^1") (default-features #t) (kind 0)))) (hash "188vyywh8hfy7r1zpyln2h2qqijgny4hnmckrx2wisxwb8iiwvdn")))

(define-public crate-owned-singleton-0.1 (crate (name "owned-singleton") (vers "0.1.0") (deps (list (crate-dep (name "owned-singleton-macros") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "stable_deref_trait") (req "^1.1.1") (kind 0)))) (hash "10zd9vys3az5gsljbbg1w33qs9g81k7pdid40svpsz69d643gai1")))

(define-public crate-owned-singleton-macros-0.1 (crate (name "owned-singleton-macros") (vers "0.1.0") (deps (list (crate-dep (name "proc-macro2") (req "^0.4.20") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^0.6.8") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.5.5") (kind 0)) (crate-dep (name "syn") (req "^0.15.13") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "1qvffpwnmwzvr35gdzb3lszlmvxhgw1hgdm5f2zifl5rf702lp4d")))

(define-public crate-owned_chars-0.1 (crate (name "owned_chars") (vers "0.1.0") (hash "15w861h5hlanq27za62wclihgppxacqm06xsgnvmw94f00vgrck9")))

(define-public crate-owned_chars-0.1 (crate (name "owned_chars") (vers "0.1.1") (hash "00b7lmhd9gqhig2wfsyzx9i28yv8wv0a3khgjxsxcvl3pd4jlfcq")))

(define-public crate-owned_chars-0.2 (crate (name "owned_chars") (vers "0.2.0") (hash "0l6372bly1r010fmhc7a002rfm5qfr8a6ipl2xdhhhba6dnp2wsj")))

(define-public crate-owned_chars-0.2 (crate (name "owned_chars") (vers "0.2.1") (hash "1jcym7rwq268ilaqpnln16x50w3f9r8j6b0bd3lnpagn34r70lwz")))

(define-public crate-owned_chars-0.3 (crate (name "owned_chars") (vers "0.3.0") (deps (list (crate-dep (name "delegate") (req "^0.1.2") (default-features #t) (kind 0)))) (hash "1nvylv0hy8vykz09ri5cwdfb0bz47wdb5nhvx3q6wzl4vmmsxl3v")))

(define-public crate-owned_chars-0.3 (crate (name "owned_chars") (vers "0.3.1") (deps (list (crate-dep (name "delegate") (req "^0.4") (default-features #t) (kind 0)))) (hash "12q4dnrzipak2ai01ya8ylaiqx46qxx0gisdwkbn83fng76zm6id")))

(define-public crate-owned_chars-0.3 (crate (name "owned_chars") (vers "0.3.2") (deps (list (crate-dep (name "delegate-attr") (req "^0.2.6") (default-features #t) (kind 0)))) (hash "0fyw0v1amxgv64xl9b4hardzj5jpv0fbp1g8skb5fw5c00qsznq9")))

(define-public crate-owned_chunks-0.1 (crate (name "owned_chunks") (vers "0.1.0") (hash "1i9al7n18y2cwszgnwdcb49bnvl13f70z0klrsbww42vhpdsz3rs") (yanked #t)))

(define-public crate-owned_chunks-0.1 (crate (name "owned_chunks") (vers "0.1.1") (hash "0kjxan1vxbmflmk7kh6qmzag6phx8fmmkfix46g9z15smmpwswgd")))

(define-public crate-owned_json_deserializer-1 (crate (name "owned_json_deserializer") (vers "1.0.0") (deps (list (crate-dep (name "serde") (req "^1.0") (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 2)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)))) (hash "02fkb29yz259g7sm92r111as650272lwsaxw9sf0ff6ckr4f7r4m")))

(define-public crate-owned_ref_cell-0.1 (crate (name "owned_ref_cell") (vers "0.1.0") (hash "1s5n40lgr9icb18m0qvxl423xfi8gw2m2mn3vn20qn7pjqw54qfr")))

(define-public crate-owned_ttf_parser-0.5 (crate (name "owned_ttf_parser") (vers "0.5.0") (deps (list (crate-dep (name "ttf-parser") (req "^0.5") (kind 0)))) (hash "1jk447y7g56p67nmzri8qrz4471shny7z436axb0jn7sacdh1876") (features (quote (("std") ("default" "std" "ttf-parser/default"))))))

(define-public crate-owned_ttf_parser-0.5 (crate (name "owned_ttf_parser") (vers "0.5.1") (deps (list (crate-dep (name "ttf-parser") (req "^0.5") (kind 0)))) (hash "02v93101xmr526v9vkvr582gi2vi2w0immvq2r4bifvm4yfapkvj") (features (quote (("std") ("default" "std" "ttf-parser/default"))))))

(define-public crate-owned_ttf_parser-0.6 (crate (name "owned_ttf_parser") (vers "0.6.0") (deps (list (crate-dep (name "ttf-parser") (req "^0.6") (kind 0)))) (hash "1qydjksjcllf0pnm0jkjvbg4n52wfcwv59dl5b06cqn40sw3z4lz") (features (quote (("std") ("default" "std" "ttf-parser/default"))))))

(define-public crate-owned_ttf_parser-0.7 (crate (name "owned_ttf_parser") (vers "0.7.0") (deps (list (crate-dep (name "ttf-parser") (req "^0.7") (kind 0)))) (hash "1kpwca1fx60alcx9g7y57q3wib3j8g000wh8jcfmnsj3y33vrkwj") (features (quote (("std") ("default" "std" "ttf-parser/default"))))))

(define-public crate-owned_ttf_parser-0.8 (crate (name "owned_ttf_parser") (vers "0.8.0") (deps (list (crate-dep (name "ttf-parser") (req "^0.8") (kind 0)))) (hash "1xsrvj3way7h3ifmmp1141qdzwh4kd72xjn63m7f19m3s9zpqizv") (features (quote (("std") ("default" "std" "ttf-parser/default"))))))

(define-public crate-owned_ttf_parser-0.9 (crate (name "owned_ttf_parser") (vers "0.9.0") (deps (list (crate-dep (name "ttf-parser") (req "^0.9") (kind 0)))) (hash "129f5dw029qwzgh8djng7n98y9lbalnci7ki056iqh1p341v6d8h") (features (quote (("std") ("default" "std" "ttf-parser/default"))))))

(define-public crate-owned_ttf_parser-0.10 (crate (name "owned_ttf_parser") (vers "0.10.0") (deps (list (crate-dep (name "ttf-parser") (req "^0.10") (kind 0)))) (hash "0iy5mlb2qp715766vxd09pchg1p3p26r754kb5r81h880hhs5334") (features (quote (("variable-fonts" "ttf-parser/variable-fonts") ("std" "ttf-parser/std") ("default" "std" "variable-fonts"))))))

(define-public crate-owned_ttf_parser-0.11 (crate (name "owned_ttf_parser") (vers "0.11.0") (deps (list (crate-dep (name "ttf-parser") (req "^0.11") (kind 0)))) (hash "15zrifb2ldnrjyzbqzhkhfiv71l9y6g9459l5p348qd5gdijg2wl") (features (quote (("variable-fonts" "ttf-parser/variable-fonts") ("std" "ttf-parser/std") ("default" "std" "variable-fonts"))))))

(define-public crate-owned_ttf_parser-0.12 (crate (name "owned_ttf_parser") (vers "0.12.0") (deps (list (crate-dep (name "ttf-parser") (req "^0.12") (kind 0)))) (hash "0mg35sfiifn1qaj8nhc33n5fmhcvwdccmxpfd0z248piwch7lg1s") (features (quote (("variable-fonts" "ttf-parser/variable-fonts") ("std" "ttf-parser/std") ("default" "std" "variable-fonts"))))))

(define-public crate-owned_ttf_parser-0.12 (crate (name "owned_ttf_parser") (vers "0.12.1") (deps (list (crate-dep (name "ttf-parser") (req "^0.12.3") (kind 0)))) (hash "1c6yxx8rqhls6vljm4hcf58v4gbqz7pkyfqf93v9ph2w5vd8vb30") (features (quote (("variable-fonts" "ttf-parser/variable-fonts") ("std" "ttf-parser/std") ("default" "std" "variable-fonts"))))))

(define-public crate-owned_ttf_parser-0.13 (crate (name "owned_ttf_parser") (vers "0.13.0") (deps (list (crate-dep (name "ttf-parser") (req "^0.13") (kind 0)))) (hash "01kjrdf6na2k4pc28r1d02rhypdh2aq7bw50pcs88kccqq4ccq79") (features (quote (("variable-fonts" "ttf-parser/variable-fonts") ("std" "ttf-parser/std") ("opentype-layout" "ttf-parser/opentype-layout") ("glyph-names" "ttf-parser/glyph-names") ("default" "std" "opentype-layout" "variable-fonts" "glyph-names"))))))

(define-public crate-owned_ttf_parser-0.13 (crate (name "owned_ttf_parser") (vers "0.13.1") (deps (list (crate-dep (name "ttf-parser") (req "^0.13.1") (kind 0)))) (hash "1hq9yp41cg1fs9fbarw9r2f3nhfaah7qmkw8p41bib29f8p6v1h6") (features (quote (("variable-fonts" "ttf-parser/variable-fonts") ("std" "ttf-parser/std") ("opentype-layout" "ttf-parser/opentype-layout") ("glyph-names" "ttf-parser/glyph-names") ("default" "std" "opentype-layout" "variable-fonts" "glyph-names"))))))

(define-public crate-owned_ttf_parser-0.13 (crate (name "owned_ttf_parser") (vers "0.13.2") (deps (list (crate-dep (name "ttf-parser") (req "^0.13.2") (kind 0)))) (hash "1vlqh55vchjjbrzym6bna09lx375yibr17qwqi61cvvfcdr3zvk5") (features (quote (("variable-fonts" "ttf-parser/variable-fonts") ("std" "ttf-parser/std") ("opentype-layout" "ttf-parser/opentype-layout") ("gvar-alloc" "std" "ttf-parser/gvar-alloc") ("glyph-names" "ttf-parser/glyph-names") ("default" "std" "opentype-layout" "variable-fonts" "glyph-names"))))))

(define-public crate-owned_ttf_parser-0.14 (crate (name "owned_ttf_parser") (vers "0.14.0") (deps (list (crate-dep (name "ttf-parser") (req "^0.14") (kind 0)))) (hash "1hx5dspd5sglp0zvzlh0rv4bzxri4vg3l58cq6nfgcx8h8l5zw2f") (features (quote (("variable-fonts" "ttf-parser/variable-fonts") ("std" "ttf-parser/std") ("opentype-layout" "ttf-parser/opentype-layout") ("gvar-alloc" "std" "ttf-parser/gvar-alloc") ("glyph-names" "ttf-parser/glyph-names") ("default" "std" "opentype-layout" "variable-fonts" "glyph-names"))))))

(define-public crate-owned_ttf_parser-0.15 (crate (name "owned_ttf_parser") (vers "0.15.0") (deps (list (crate-dep (name "ttf-parser") (req "^0.15") (kind 0)))) (hash "17gcrycx0gdm0v2pg9jxgws5mifvziyhbyhbm6r2v8g7rw4ybcag") (features (quote (("variable-fonts" "ttf-parser/variable-fonts") ("std" "ttf-parser/std") ("opentype-layout" "ttf-parser/opentype-layout") ("gvar-alloc" "std" "ttf-parser/gvar-alloc") ("glyph-names" "ttf-parser/glyph-names") ("default" "std" "opentype-layout" "apple-layout" "variable-fonts" "glyph-names") ("apple-layout" "ttf-parser/apple-layout"))))))

(define-public crate-owned_ttf_parser-0.15 (crate (name "owned_ttf_parser") (vers "0.15.1") (deps (list (crate-dep (name "ttf-parser") (req "^0.15.2") (kind 0)))) (hash "0glsxqkpqfhqgjj45ssb53qjhh69nf42rypl0rlxsyg49901mvq7") (features (quote (("variable-fonts" "ttf-parser/variable-fonts") ("std" "ttf-parser/std") ("opentype-layout" "ttf-parser/opentype-layout") ("gvar-alloc" "std" "ttf-parser/gvar-alloc") ("glyph-names" "ttf-parser/glyph-names") ("default" "std" "opentype-layout" "apple-layout" "variable-fonts" "glyph-names") ("apple-layout" "ttf-parser/apple-layout"))))))

(define-public crate-owned_ttf_parser-0.15 (crate (name "owned_ttf_parser") (vers "0.15.2") (deps (list (crate-dep (name "ttf-parser") (req "^0.15.2") (kind 0)))) (hash "1frgpf2a5j17ylk1lmrj2lpyhf6izq7x8b1xlbv6ybb3n7zazrh5") (features (quote (("variable-fonts" "ttf-parser/variable-fonts") ("std" "ttf-parser/std") ("opentype-layout" "ttf-parser/opentype-layout") ("gvar-alloc" "std" "ttf-parser/gvar-alloc") ("glyph-names" "ttf-parser/glyph-names") ("default" "std" "opentype-layout" "apple-layout" "variable-fonts" "glyph-names") ("apple-layout" "ttf-parser/apple-layout"))))))

(define-public crate-owned_ttf_parser-0.16 (crate (name "owned_ttf_parser") (vers "0.16.0") (deps (list (crate-dep (name "ttf-parser") (req "^0.16") (kind 0)))) (hash "1xm4hw1d1pz3bcrpn7bjrpk0pivdj664zx9n19ldk3a0fi5v34pp") (features (quote (("variable-fonts" "ttf-parser/variable-fonts") ("std" "ttf-parser/std") ("opentype-layout" "ttf-parser/opentype-layout") ("gvar-alloc" "std" "ttf-parser/gvar-alloc") ("glyph-names" "ttf-parser/glyph-names") ("default" "std" "opentype-layout" "apple-layout" "variable-fonts" "glyph-names") ("apple-layout" "ttf-parser/apple-layout"))))))

(define-public crate-owned_ttf_parser-0.17 (crate (name "owned_ttf_parser") (vers "0.17.0") (deps (list (crate-dep (name "ttf-parser") (req "^0.17") (kind 0)))) (hash "11x19idvyhvfmk15m801k2lr7vwnq06y4104svl5j5qmfa2m0ra6") (features (quote (("variable-fonts" "ttf-parser/variable-fonts") ("std" "ttf-parser/std") ("opentype-layout" "ttf-parser/opentype-layout") ("gvar-alloc" "std" "ttf-parser/gvar-alloc") ("glyph-names" "ttf-parser/glyph-names") ("default" "std" "opentype-layout" "apple-layout" "variable-fonts" "glyph-names") ("apple-layout" "ttf-parser/apple-layout"))))))

(define-public crate-owned_ttf_parser-0.17 (crate (name "owned_ttf_parser") (vers "0.17.1") (deps (list (crate-dep (name "ttf-parser") (req "^0.17") (kind 0)))) (hash "1igdfw2qprf3dpsg97kbk8qdrgv9gwd3saa2fl6ryfj9cly4v40q") (features (quote (("variable-fonts" "ttf-parser/variable-fonts") ("std" "ttf-parser/std") ("opentype-layout" "ttf-parser/opentype-layout") ("gvar-alloc" "std" "ttf-parser/gvar-alloc") ("glyph-names" "ttf-parser/glyph-names") ("default" "std" "opentype-layout" "apple-layout" "variable-fonts" "glyph-names") ("apple-layout" "ttf-parser/apple-layout"))))))

(define-public crate-owned_ttf_parser-0.18 (crate (name "owned_ttf_parser") (vers "0.18.0") (deps (list (crate-dep (name "ttf-parser") (req "^0.18") (kind 0)))) (hash "14vyqlw8lxsl72dxk6izlwpb7vpms4j5xcjzjvkpjs4bl1y3qpra") (features (quote (("variable-fonts" "ttf-parser/variable-fonts") ("std" "ttf-parser/std") ("opentype-layout" "ttf-parser/opentype-layout") ("gvar-alloc" "std" "ttf-parser/gvar-alloc") ("glyph-names" "ttf-parser/glyph-names") ("default" "std" "opentype-layout" "apple-layout" "variable-fonts" "glyph-names") ("apple-layout" "ttf-parser/apple-layout"))))))

(define-public crate-owned_ttf_parser-0.18 (crate (name "owned_ttf_parser") (vers "0.18.1") (deps (list (crate-dep (name "ttf-parser") (req "^0.18.1") (kind 0)))) (hash "0a22qay3p6izi4jvdyw6678sl0s10ipc4mdbb2p4yy8payqrypp2") (features (quote (("variable-fonts" "ttf-parser/variable-fonts") ("std" "ttf-parser/std") ("opentype-layout" "ttf-parser/opentype-layout") ("gvar-alloc" "std" "ttf-parser/gvar-alloc") ("glyph-names" "ttf-parser/glyph-names") ("default" "std" "opentype-layout" "apple-layout" "variable-fonts" "glyph-names") ("apple-layout" "ttf-parser/apple-layout"))))))

(define-public crate-owned_ttf_parser-0.19 (crate (name "owned_ttf_parser") (vers "0.19.0") (deps (list (crate-dep (name "ttf-parser") (req "^0.19") (kind 0)))) (hash "192v6mjgy2fv7z8n7wm6548pikngcc293l9qh8xdc4s147iffvbh") (features (quote (("variable-fonts" "ttf-parser/variable-fonts") ("std" "ttf-parser/std") ("opentype-layout" "ttf-parser/opentype-layout") ("gvar-alloc" "std" "ttf-parser/gvar-alloc") ("glyph-names" "ttf-parser/glyph-names") ("default" "std" "opentype-layout" "apple-layout" "variable-fonts" "glyph-names") ("apple-layout" "ttf-parser/apple-layout"))))))

(define-public crate-owned_ttf_parser-0.20 (crate (name "owned_ttf_parser") (vers "0.20.0") (deps (list (crate-dep (name "ttf-parser") (req "^0.20") (kind 0)))) (hash "1rr38229kigjp4mzwpgz5qhjpd9jrfx88k57jwbwfj66wkgnwn6l") (features (quote (("variable-fonts" "ttf-parser/variable-fonts") ("std" "ttf-parser/std") ("opentype-layout" "ttf-parser/opentype-layout") ("gvar-alloc" "std" "ttf-parser/gvar-alloc") ("glyph-names" "ttf-parser/glyph-names") ("default" "std" "opentype-layout" "apple-layout" "variable-fonts" "glyph-names") ("apple-layout" "ttf-parser/apple-layout"))))))

(define-public crate-owned_ttf_parser-0.21 (crate (name "owned_ttf_parser") (vers "0.21.0") (deps (list (crate-dep (name "ttf-parser") (req "^0.21") (kind 0)))) (hash "1mb7f0b0n8sgfpszrcj78fh1pi42rmfby0r29b3lcg665y6l6hbb") (features (quote (("variable-fonts" "ttf-parser/variable-fonts") ("std" "ttf-parser/std") ("opentype-layout" "ttf-parser/opentype-layout") ("gvar-alloc" "std" "ttf-parser/gvar-alloc") ("glyph-names" "ttf-parser/glyph-names") ("default" "std" "opentype-layout" "apple-layout" "variable-fonts" "glyph-names") ("apple-layout" "ttf-parser/apple-layout"))))))

(define-public crate-ownedbytes-0.1 (crate (name "ownedbytes") (vers "0.1.0") (deps (list (crate-dep (name "stable_deref_trait") (req "^1.2.0") (default-features #t) (kind 0)))) (hash "1v8ra9bm3qix2an0b4w59pf9bxfikajaxmni7fhwp5bbdxwb0qyc")))

(define-public crate-ownedbytes-0.2 (crate (name "ownedbytes") (vers "0.2.0") (deps (list (crate-dep (name "stable_deref_trait") (req "^1.2.0") (default-features #t) (kind 0)))) (hash "17n9lm25yxkfhg9fzg5c102w86sc5rwjfm5qg0fl2fbs465j1yhb")))

(define-public crate-ownedbytes-0.3 (crate (name "ownedbytes") (vers "0.3.0") (deps (list (crate-dep (name "stable_deref_trait") (req "^1.2.0") (default-features #t) (kind 0)))) (hash "0h60bjqw2vhj7g2m3waqfky6k8km29b60g08a1n0x9xjrzbip672")))

(define-public crate-ownedbytes-0.4 (crate (name "ownedbytes") (vers "0.4.0") (deps (list (crate-dep (name "stable_deref_trait") (req "^1.2.0") (default-features #t) (kind 0)))) (hash "1nzq0wwwsb5df4vlbl4i7bbfj1f550qmnvj1anbz76d2cjm7x5cf")))

(define-public crate-ownedbytes-0.5 (crate (name "ownedbytes") (vers "0.5.0") (deps (list (crate-dep (name "stable_deref_trait") (req "^1.2.0") (default-features #t) (kind 0)))) (hash "173kc1ldfgxr2zhhh5sc4lcirxi2990pzl2ibpxxa107nacf8667")))

(define-public crate-ownedbytes-0.6 (crate (name "ownedbytes") (vers "0.6.0") (deps (list (crate-dep (name "stable_deref_trait") (req "^1.2.0") (default-features #t) (kind 0)))) (hash "1dxli131kz85jdq7v6kb5d21sghji011k351nfmri0df32wp52kf")))

(define-public crate-ownedbytes-0.7 (crate (name "ownedbytes") (vers "0.7.0") (deps (list (crate-dep (name "stable_deref_trait") (req "^1.2.0") (default-features #t) (kind 0)))) (hash "0n65zhnkxci7sw7664z9c3zfv1dxp7k45q28p4jz9f33n3pmk863")))

(define-public crate-owner-monad-0.1 (crate (name "owner-monad") (vers "0.1.0") (hash "11h77hb10j7hhddzs9psfrk09bg56az0ija76j804wkfvxq1v04f")))

(define-public crate-owners-0.1 (crate (name "owners") (vers "0.1.0") (deps (list (crate-dep (name "glob") (req "^0.2.11") (default-features #t) (kind 0)) (crate-dep (name "nom") (req "^2.2.1") (default-features #t) (kind 0)))) (hash "0c179wchgv5jpvwfnmv150dx6j92sv3clq3lhs7yy68627jxm4nb")))

