(define-module (crates-io ow o-) #:use-module (crates-io))

(define-public crate-owo-code-0.1 (crate (name "owo-code") (vers "0.1.0") (hash "15j5czdxzn78gscs58hs6h4gvw9xmfr1467p751ai4fiqh1q3ybl")))

(define-public crate-owo-code-0.1 (crate (name "owo-code") (vers "0.1.1") (hash "1x3s4wsllwkkayshqbghci0rpv4l5g82s42sv39lh48vlnlw21cc")))

(define-public crate-owo-colors-0.0.0 (crate (name "owo-colors") (vers "0.0.0") (hash "0pq0h9xyzzgiwcnddvp0yjd9l67326jib5mnd9k3m88lik952ib9")))

(define-public crate-owo-colors-1 (crate (name "owo-colors") (vers "1.0.0") (hash "1l05d21v6ql44sn1304hqyq4z3aq34k0x5w765f4irv84af299bs")))

(define-public crate-owo-colors-1 (crate (name "owo-colors") (vers "1.0.1") (hash "0308bxwipy6h8zs80gjsnirigl22g4kxg5rn1xjszyf0r3iiv5zi")))

(define-public crate-owo-colors-1 (crate (name "owo-colors") (vers "1.0.2") (hash "0k9n091pkbdqifikly2fdj7il6d2m433b7di4k1ahf026vyh2h2d")))

(define-public crate-owo-colors-1 (crate (name "owo-colors") (vers "1.0.3") (hash "0hxk065pkp1ik1cgandxg0khdxr7kdkp5l368n45fjbp36081dl4")))

(define-public crate-owo-colors-1 (crate (name "owo-colors") (vers "1.1.0") (hash "19188218dsm102pn0diri2z7n5hpvrcqcgnjk4zgma257sbrjqnx") (features (quote (("custom"))))))

(define-public crate-owo-colors-1 (crate (name "owo-colors") (vers "1.1.1") (hash "1x4qy4ajyy0r3y7h06yd9gqi13qln8adkkqci38wa1xxx5rb9zzi") (features (quote (("custom")))) (yanked #t)))

(define-public crate-owo-colors-1 (crate (name "owo-colors") (vers "1.1.2") (hash "1j8jr1zify92lbjqqlxnbh005ky4j6nci4xyl58f46qr5b53nxwl") (features (quote (("custom")))) (yanked #t)))

(define-public crate-owo-colors-1 (crate (name "owo-colors") (vers "1.1.3") (hash "1wqh8icl30ikfw1zc9qslh0gqcgri4lyhnibajyzdvh3s76m04ks") (features (quote (("custom"))))))

(define-public crate-owo-colors-1 (crate (name "owo-colors") (vers "1.2.0") (hash "1jzrarp0palx4rglb2ysh415r5qs5mvhj6dnq5n5z40mfhd8kb15") (features (quote (("custom"))))))

(define-public crate-owo-colors-1 (crate (name "owo-colors") (vers "1.2.1") (hash "0yxaqcfigw9msr82syimn16419nwyjs916xn3dq2jhj78jp0sdqk") (features (quote (("custom"))))))

(define-public crate-owo-colors-1 (crate (name "owo-colors") (vers "1.3.0") (deps (list (crate-dep (name "atty") (req "^0.2") (optional #t) (default-features #t) (kind 0)))) (hash "0mbs3z0c6p48wh96paa230xf6c6h8nhyyk1d118pybqwx7mv91i3") (features (quote (("tty" "atty") ("custom"))))))

(define-public crate-owo-colors-1 (crate (name "owo-colors") (vers "1.4.0") (deps (list (crate-dep (name "atty") (req "^0.2") (optional #t) (default-features #t) (kind 0)))) (hash "0jq7ppinx7hlf01nlwxj8448b2y5d80hgpj6kawz23g7kb03gsx9") (features (quote (("tty" "atty") ("custom")))) (yanked #t)))

(define-public crate-owo-colors-1 (crate (name "owo-colors") (vers "1.4.1") (deps (list (crate-dep (name "atty") (req "^0.2") (optional #t) (default-features #t) (kind 0)))) (hash "0yl4ps6nis7f9c9zg5189fs9yn31qw20rw1sa3cym15lkxwrj3nn") (features (quote (("tty" "atty") ("custom")))) (yanked #t)))

(define-public crate-owo-colors-1 (crate (name "owo-colors") (vers "1.4.2") (deps (list (crate-dep (name "atty") (req "^0.2") (optional #t) (default-features #t) (kind 0)))) (hash "0rybl2lvhaycpkpaq45099idp5ny7nv4sqsafz0cvfqw1wjfy9vz") (features (quote (("tty" "atty") ("custom")))) (yanked #t)))

(define-public crate-owo-colors-2 (crate (name "owo-colors") (vers "2.0.0") (deps (list (crate-dep (name "atty") (req "^0.2") (optional #t) (default-features #t) (kind 0)))) (hash "0zsdyvaw0k971p7y9fv806q9misrvcalf575rsfcq21b6yzl7zpj") (features (quote (("tty" "atty") ("custom"))))))

(define-public crate-owo-colors-2 (crate (name "owo-colors") (vers "2.1.0-beta.0") (deps (list (crate-dep (name "atty") (req "^0.2") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "supports-color") (req "^1.1") (optional #t) (default-features #t) (kind 0)))) (hash "163q9w2mqkjkl2xh46ky19rhcw57r9ww08zpqkalifdgnadj65yd") (features (quote (("tty" "atty") ("supports-colors" "supports-color") ("custom"))))))

(define-public crate-owo-colors-2 (crate (name "owo-colors") (vers "2.1.0") (deps (list (crate-dep (name "atty") (req "^0.2") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "supports-color") (req "^1.1") (optional #t) (default-features #t) (kind 0)))) (hash "0z2j9vlajrg65j5pc8nsp7zwdbqzl2hs64iqnayhmi5f4mcpcq9s") (features (quote (("tty" "atty") ("supports-colors" "supports-color") ("custom"))))))

(define-public crate-owo-colors-3 (crate (name "owo-colors") (vers "3.0.0-beta.0") (deps (list (crate-dep (name "atty") (req "^0.2") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "supports-color") (req "^1.3") (optional #t) (default-features #t) (kind 0)))) (hash "1wqnf6i7c7wimf3n03dcnsgxczrs0rvm9ch60krsx2bfbcyi0q9f") (features (quote (("tty" "atty") ("supports-colors" "supports-color") ("custom"))))))

(define-public crate-owo-colors-3 (crate (name "owo-colors") (vers "3.0.0-beta.1") (deps (list (crate-dep (name "supports-color") (req "^1.3") (optional #t) (default-features #t) (kind 0)))) (hash "0hy4in0s0mg09hpq4q3srn3njqnq23s98xg4lc7vr60l4q51fig6") (features (quote (("supports-colors" "supports-color"))))))

(define-public crate-owo-colors-3 (crate (name "owo-colors") (vers "3.0.0-beta.2") (deps (list (crate-dep (name "supports-color") (req "^1.3") (optional #t) (default-features #t) (kind 0)))) (hash "02ykfh2zbaipn2jrsbpazjvcnl4f1rh4i1036pq1x7qnkjr9ax7v") (features (quote (("supports-colors" "supports-color"))))))

(define-public crate-owo-colors-3 (crate (name "owo-colors") (vers "3.0.0") (deps (list (crate-dep (name "supports-color") (req "^1.3") (optional #t) (default-features #t) (kind 0)))) (hash "0629qds6qvl1vs4k558l9rcsdn2858n6lprfbz4pp2by9by11sng") (features (quote (("supports-colors" "supports-color"))))))

(define-public crate-owo-colors-3 (crate (name "owo-colors") (vers "3.0.1") (deps (list (crate-dep (name "supports-color") (req "^1.3") (optional #t) (default-features #t) (kind 0)))) (hash "04wyq17g0rb895ml3gxqilmwa5c2z4fpafn9mnwfyv0q6ik1lfjh") (features (quote (("supports-colors" "supports-color"))))))

(define-public crate-owo-colors-3 (crate (name "owo-colors") (vers "3.1.0") (deps (list (crate-dep (name "supports-color") (req "^1.3") (optional #t) (default-features #t) (kind 0)))) (hash "1zvg7wkjm7f81r0hvwf56cdn1kdfpxl4zbxpmg6528yw5hi6vbgr") (features (quote (("supports-colors" "supports-color"))))))

(define-public crate-owo-colors-3 (crate (name "owo-colors") (vers "3.1.1") (deps (list (crate-dep (name "supports-color") (req "^1.3") (optional #t) (default-features #t) (kind 0)))) (hash "121b75wgs500pbldq9frma728krl19wh7asxg9r72hbybsh1qfwd") (features (quote (("supports-colors" "supports-color"))))))

(define-public crate-owo-colors-3 (crate (name "owo-colors") (vers "3.2.0") (deps (list (crate-dep (name "supports-color") (req "^1.3") (optional #t) (default-features #t) (kind 0)))) (hash "1x7b8kf3854zlix6cpai065fjrdgfil4gq5v2pmfc17cg3b8yi10") (features (quote (("supports-colors" "supports-color"))))))

(define-public crate-owo-colors-3 (crate (name "owo-colors") (vers "3.3.0") (deps (list (crate-dep (name "supports-color") (req "^1.3") (optional #t) (default-features #t) (kind 0)))) (hash "1sq3i8g5haj6xy56ln1ggm8waazzkkfj6272i8y9kl70g02y6wjy") (features (quote (("supports-colors" "supports-color"))))))

(define-public crate-owo-colors-3 (crate (name "owo-colors") (vers "3.4.0") (deps (list (crate-dep (name "supports-color") (req "^1.3") (optional #t) (default-features #t) (kind 0)))) (hash "0yx8glp9vipa6gybbqmpg8lyrcsrv9z6dia94p5lvshzja0p7kyy") (features (quote (("supports-colors" "supports-color") ("alloc"))))))

(define-public crate-owo-colors-3 (crate (name "owo-colors") (vers "3.5.0") (deps (list (crate-dep (name "supports-color") (req "^1.3") (optional #t) (default-features #t) (kind 0)))) (hash "0vyvry6ba1xmpd45hpi6savd8mbx09jpmvnnwkf6z62pk6s4zc61") (features (quote (("supports-colors" "supports-color") ("alloc")))) (rust-version "1.51")))

(define-public crate-owo-colors-3 (crate (name "owo-colors") (vers "3.6.0") (deps (list (crate-dep (name "supports-color") (req "^2.0") (optional #t) (default-features #t) (kind 0)))) (hash "0943lynkwz1glq3w7m9anv73lnrhd8yabs09krbh49g1wz4lxp39") (features (quote (("supports-colors" "supports-color") ("alloc")))) (yanked #t) (rust-version "1.51")))

(define-public crate-owo-colors-4 (crate (name "owo-colors") (vers "4.0.0-rc.0") (deps (list (crate-dep (name "supports-color") (req "^2.0") (optional #t) (default-features #t) (kind 0)))) (hash "18jz0vwk60dzpg92j8862bd580wm56fdj89xs8zabyjkxrxszbkh") (features (quote (("supports-colors" "supports-color") ("alloc")))) (rust-version "1.56")))

(define-public crate-owo-colors-4 (crate (name "owo-colors") (vers "4.0.0-rc.1") (deps (list (crate-dep (name "supports-color") (req "^2.0") (optional #t) (default-features #t) (kind 0)))) (hash "0jhvab83rxp2m4i0s9p0b95wfwdy1gjvsaq5064s5b5qhaj2xvkh") (features (quote (("supports-colors" "supports-color") ("alloc")))) (rust-version "1.56")))

(define-public crate-owo-colors-4 (crate (name "owo-colors") (vers "4.0.0") (deps (list (crate-dep (name "supports-color") (req "^2.0") (optional #t) (default-features #t) (kind 0)))) (hash "0grsk47cllj0s4nc4qxvy4gdhj2lyiglbqx4lmw2m7grdmq59zya") (features (quote (("supports-colors" "supports-color") ("alloc")))) (rust-version "1.56")))

