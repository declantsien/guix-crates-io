(define-module (crates-io ow n-) #:use-module (crates-io))

(define-public crate-own-ref-0.0.0 (crate (name "own-ref") (vers "0.0.0") (hash "0dchv7b0h9cd025wbd90pgqj6v49hrr2yhdjy6bm81mh82qpkd2a")))

(define-public crate-own-ref-0.1 (crate (name "own-ref") (vers "0.1.0-alpha") (deps (list (crate-dep (name "extension-traits") (req "^1.0.1") (default-features #t) (kind 0)) (crate-dep (name "macro_rules_attribute") (req "^0.1.3") (default-features #t) (kind 2)) (crate-dep (name "paste") (req "^1.0.15") (default-features #t) (kind 0)) (crate-dep (name "scopeguard") (req "^1.1.0") (default-features #t) (kind 2)))) (hash "1hnkwx1v6fnhnl95q9j9cxmg1j1fxdw2brrkzlqyv3yj3l7amd6q") (features (quote (("offset_of") ("default" "offset_of")))) (rust-version "1.68.0")))

