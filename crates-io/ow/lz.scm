(define-module (crates-io ow lz) #:use-module (crates-io))

(define-public crate-owlz-0.1 (crate (name "owlz") (vers "0.1.0") (deps (list (crate-dep (name "enum-assoc") (req "^0.3.4") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)) (crate-dep (name "rand_derive2") (req "^0.1.17") (default-features #t) (kind 0)))) (hash "1m7vs8xcyg2v4004m8v49lrakdf5l5svigiv26gnh1vp57gf9n5i")))

(define-public crate-owlz-0.1 (crate (name "owlz") (vers "0.1.1") (deps (list (crate-dep (name "enum-assoc") (req "^0.3.4") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)) (crate-dep (name "rand_derive2") (req "^0.1.17") (default-features #t) (kind 0)))) (hash "1zrm6vqnmjgx33sbmfrzwaw9zawhvv3biwhs9szy4scycckxjzya")))

