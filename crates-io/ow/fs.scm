(define-module (crates-io ow fs) #:use-module (crates-io))

(define-public crate-owfs-0.0.1 (crate (name "owfs") (vers "0.0.1") (hash "1wxlzqpv1l6d3xiniv7i1bjslnx941188n4c6vhrrx9xjhahf2y3")))

(define-public crate-owfs-0.0.2 (crate (name "owfs") (vers "0.0.2") (hash "0p2yrfxiqdci08m5ksfpsaim7sy5mnais8rmgzv6s9mj9clhfhng")))

(define-public crate-owfs-0.0.3 (crate (name "owfs") (vers "0.0.3") (hash "05rlmzfx6an8k20i6sljg83as6q029i4cszpf03pq1a5c616f2ac")))

(define-public crate-owfs-0.0.4 (crate (name "owfs") (vers "0.0.4") (hash "02m1ab9gwqc0zkkf8gmwh5q38q40rf821jwkgyn1h0hahm52mhcz")))

(define-public crate-owfs-0.0.5 (crate (name "owfs") (vers "0.0.5") (hash "149sc0225k08fn2y1674wy6x8mbm3hy2gf9dk4xim3qzcps3cm8x")))

(define-public crate-owfs-0.0.6 (crate (name "owfs") (vers "0.0.6") (hash "18p20w6z2bp584n8pc64vvj8pk70yw0nqi69aw966h4ayf7pvv6g")))

(define-public crate-owfs-0.0.7 (crate (name "owfs") (vers "0.0.7") (hash "0nkazs6wiq5w0b0hjsg55r8c902jrg3nv81wkn6n45rnq9qhkd6g")))

(define-public crate-owfs-0.0.8 (crate (name "owfs") (vers "0.0.8") (deps (list (crate-dep (name "libc") (req "^0.2.126") (default-features #t) (kind 0)))) (hash "0q9vhy4750aqcj2155897r728h8zxhkdphzswiwnkz5jdhfqkil4")))

(define-public crate-owfs-0.0.9 (crate (name "owfs") (vers "0.0.9") (deps (list (crate-dep (name "libc") (req "^0.2.126") (default-features #t) (kind 0)))) (hash "14dpw4kf954jq749fcm7bqm8ggy9pg753xdm833b9q9lh80lnxvr")))

(define-public crate-owfs-0.1 (crate (name "owfs") (vers "0.1.0") (deps (list (crate-dep (name "libc") (req "^0.2.126") (default-features #t) (kind 0)) (crate-dep (name "num_cpus") (req "^1.16.0") (optional #t) (default-features #t) (kind 1)) (crate-dep (name "ureq") (req "^2.9.7") (optional #t) (default-features #t) (kind 1)))) (hash "03qmy38xai8b3z8q0vcxpibwrm272wxiqxrin7223ip1mcv0kwal") (features (quote (("vendored" "num_cpus" "ureq"))))))

