(define-module (crates-io ow nr) #:use-module (crates-io))

(define-public crate-ownref-0.1 (crate (name "ownref") (vers "0.1.0") (hash "0f616sm0cz17223r3d3426fhav4f1v513464xmr5812gky4fsraf")))

(define-public crate-ownref-0.2 (crate (name "ownref") (vers "0.2.0") (deps (list (crate-dep (name "indexmap") (req "^1.8.0") (default-features #t) (kind 2)))) (hash "11jwd4sz86wnm8k72wk6sygx0r0c07pvamg2rk1jw0zsi4v1vd1m")))

(define-public crate-ownref-0.3 (crate (name "ownref") (vers "0.3.0") (deps (list (crate-dep (name "indexmap") (req "^1.8.0") (default-features #t) (kind 2)))) (hash "1wznskh9ys7y526z05y8wfadlf8lnbbzbz4z7774zrz0qizj71m2")))

(define-public crate-ownref-0.3 (crate (name "ownref") (vers "0.3.1") (deps (list (crate-dep (name "indexmap") (req "^1.8.0") (default-features #t) (kind 2)))) (hash "054k01klg533h2cr0hnmp7n1di5shyqcy9zhgp9n43233gwxnpb9")))

(define-public crate-ownrs-0.1 (crate (name "ownrs") (vers "0.1.0") (hash "18a2l72145bg7i9dfxhhly9rgbl620ynblvkwl8wmikl66vf8n4h") (yanked #t)))

(define-public crate-ownrs-0.1 (crate (name "ownrs") (vers "0.1.1") (hash "00n85z219lvdbafslcfi1f71afb3d56n2g4vagwrbpkvnab47yrl")))

(define-public crate-ownrs-0.1 (crate (name "ownrs") (vers "0.1.2") (deps (list (crate-dep (name "globset") (req "^0.4.2") (default-features #t) (kind 0)) (crate-dep (name "nom") (req "^4.1.1") (default-features #t) (kind 0)))) (hash "1ha63sw2xk052nyqf9jalynl14747vnxxks0x6hmk328q2n527bx")))

(define-public crate-ownrs-0.2 (crate (name "ownrs") (vers "0.2.0") (deps (list (crate-dep (name "globset") (req "^0.4.2") (default-features #t) (kind 0)) (crate-dep (name "nom") (req "^4.1.1") (default-features #t) (kind 0)))) (hash "0p13fjcwaj0hprrxaa7l0plqfz9vpi44qpdxssflac8pdcn93x6b")))

(define-public crate-ownrs-0.3 (crate (name "ownrs") (vers "0.3.0") (deps (list (crate-dep (name "globset") (req "^0.4.2") (default-features #t) (kind 0)) (crate-dep (name "nom") (req "^4.2.3") (default-features #t) (kind 0)))) (hash "1g7wki1iw4jcybwy74jx7plcmnahb3a3100p0hxf7p5nwd0sny8p")))

(define-public crate-ownrs-0.3 (crate (name "ownrs") (vers "0.3.1") (deps (list (crate-dep (name "globset") (req "^0.4.2") (default-features #t) (kind 0)) (crate-dep (name "nom") (req "^4.2.3") (default-features #t) (kind 0)))) (hash "1iazq2brz65hwhlp4hj3ik9ml9c9anhd949pzvkvcif2zb2bj76m")))

(define-public crate-ownrs-0.3 (crate (name "ownrs") (vers "0.3.2") (deps (list (crate-dep (name "globset") (req "^0.4.2") (default-features #t) (kind 0)) (crate-dep (name "nom") (req "^4.2.3") (default-features #t) (kind 0)))) (hash "1z62s0imksvlmz5620b7irrqzy2vi4m4245w5102am5qb9ym7wym")))

(define-public crate-ownrs-0.3 (crate (name "ownrs") (vers "0.3.3") (deps (list (crate-dep (name "globset") (req "^0.4.5") (default-features #t) (kind 0)) (crate-dep (name "nom") (req "^6.0.0-alpha1") (default-features #t) (kind 0)))) (hash "0cpbd9va6j65prdslhz7jb9i0kbradw8f5ijy5awz703l97g73fy")))

(define-public crate-ownrs-0.3 (crate (name "ownrs") (vers "0.3.4") (deps (list (crate-dep (name "globset") (req "^0.4.6") (default-features #t) (kind 0)) (crate-dep (name "nom") (req "^6.0.1") (default-features #t) (kind 0)))) (hash "01kgyhgjxiddnn5d7222n89c8snhzmf6z318hcl9p0vassqdhpaf")))

(define-public crate-ownrs-0.3 (crate (name "ownrs") (vers "0.3.5") (deps (list (crate-dep (name "globset") (req "^0.4.6") (default-features #t) (kind 0)) (crate-dep (name "nom") (req "^6.0.1") (default-features #t) (kind 0)))) (hash "0sllw5jpc61dsc8ihnxippaqxg47j2ifhi8llwiq92b337vapppg")))

