(define-module (crates-io ow oi) #:use-module (crates-io))

(define-public crate-owoifier-0.1 (crate (name "owoifier") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^3.1.18") (features (quote ("cargo"))) (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)))) (hash "02m0i1rlvf2basxkmjr1xnsmkhqvyimcm8hwk6fmbb10fgiha5z9") (yanked #t)))

(define-public crate-owoifier-1 (crate (name "owoifier") (vers "1.0.0") (deps (list (crate-dep (name "clap") (req "^3.1.18") (features (quote ("cargo"))) (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)))) (hash "17xna7c51afx32f8k34mgl65zwk3nw87rmhd6wx11gfdygzarnfy")))

(define-public crate-owoifier-1 (crate (name "owoifier") (vers "1.1.0") (deps (list (crate-dep (name "clap") (req "^3.1.18") (features (quote ("cargo"))) (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)))) (hash "0i8qfahkrznp7ll9yi75j1y798571qvmflvcikyjmm31cmy05kw6")))

(define-public crate-owoifier-1 (crate (name "owoifier") (vers "1.2.0-beta") (deps (list (crate-dep (name "clap") (req "^3.1.18") (features (quote ("cargo"))) (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)))) (hash "0mizjan692bi72nsajy0hjwcddglhxz6sqlgwwys6dwk4ixlmlzv")))

(define-public crate-owoifier-1 (crate (name "owoifier") (vers "1.2.0") (deps (list (crate-dep (name "clap") (req "^3.1.18") (features (quote ("cargo"))) (default-features #t) (kind 0)) (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)))) (hash "000l6xvrsv28svas7gaa9nv4nb1zffvrpxh59v13qzg54zympsh4")))

(define-public crate-owoify-0.1 (crate (name "owoify") (vers "0.1.0") (deps (list (crate-dep (name "rand") (req "^0.7.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.2.0") (default-features #t) (kind 0)))) (hash "0477i8hkqnd0d0sy00gsb1ymq2w6ydzlljfz5dkg93m5pm069mrj")))

(define-public crate-owoify-0.1 (crate (name "owoify") (vers "0.1.1") (deps (list (crate-dep (name "rand") (req "^0.7.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.2.0") (default-features #t) (kind 0)))) (hash "1yriaw9y429f6yx387047xiqrv5chfwc0hfva4z9y2wry8ihazq7")))

(define-public crate-owoify-0.1 (crate (name "owoify") (vers "0.1.2") (deps (list (crate-dep (name "rand") (req "^0.7.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.2.0") (default-features #t) (kind 0)))) (hash "1jlvpwc5fn0y6f5g6r7fv6cviacb3idvasa87f1j0wk9h1kdz1nd")))

(define-public crate-owoify-0.1 (crate (name "owoify") (vers "0.1.3") (deps (list (crate-dep (name "rand") (req "^0.7.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.2.0") (default-features #t) (kind 0)))) (hash "042a7xp0b1l7lqnnf992c200dlm9c2xvksxlfbd5wli7a65r31yi")))

(define-public crate-owoify-0.1 (crate (name "owoify") (vers "0.1.4") (deps (list (crate-dep (name "rand") (req "^0.7.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.2.0") (default-features #t) (kind 0)))) (hash "09zpkam83p2mciws9fc0gyldfi06d416w3zm7538zikj121bzm74")))

(define-public crate-owoify-0.1 (crate (name "owoify") (vers "0.1.5") (deps (list (crate-dep (name "rand") (req "^0.7.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.2.0") (default-features #t) (kind 0)))) (hash "0fcklkzji47ajciypq99y5hp38qw9sp605r3h1h2ngyihlv3gmgm")))

(define-public crate-owoify_rs-0.1 (crate (name "owoify_rs") (vers "0.1.0") (deps (list (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.7.3") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)))) (hash "1ydqffyrshyb3dqd9fsd2g8z64xh1siqr5cl5dwwcikw3iwgskkh")))

(define-public crate-owoify_rs-0.1 (crate (name "owoify_rs") (vers "0.1.1") (deps (list (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.7.3") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)))) (hash "1syd6iim43dl8gg564p9m2dymmnf8wj9amdsb1slvq4yicd4hfdp")))

(define-public crate-owoify_rs-0.1 (crate (name "owoify_rs") (vers "0.1.2") (deps (list (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.7.3") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)))) (hash "0pc2iyj4hmf3ymy0mafy6kv9d2byhqnjqa8d310g1954af50q2cl")))

(define-public crate-owoify_rs-0.1 (crate (name "owoify_rs") (vers "0.1.3") (deps (list (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.7.3") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)))) (hash "0y078wsi2l7gaxqpdmwj0yhhw6war5j5r37s2w1vqkz00kvzwmic")))

(define-public crate-owoify_rs-0.1 (crate (name "owoify_rs") (vers "0.1.4") (deps (list (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.7.3") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)))) (hash "0jw2bs4q7n8zi5j787ykvs9igl9n15f6h3yaij5iasdyy9bfrz5p")))

(define-public crate-owoify_rs-0.1 (crate (name "owoify_rs") (vers "0.1.5") (deps (list (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.7.3") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)))) (hash "0gcfkb2m782zqpwzdqqrh41mn3zmmq3mcah5pp49krqrqv9nzhg5")))

(define-public crate-owoify_rs-0.2 (crate (name "owoify_rs") (vers "0.2.0") (deps (list (crate-dep (name "lazy_static") (req "~1.4.0") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "~0.8.3") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)))) (hash "1mgixj7scdwd8mkw2d2nvbip5nc6b3iy163gnc8ldn7bzi748i96")))

(define-public crate-owoify_rs-1 (crate (name "owoify_rs") (vers "1.0.0") (deps (list (crate-dep (name "lazy_static") (req "~1.4.0") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "~0.8.3") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)))) (hash "0yv7iv03jrp0l63yb5snm6qmrcxv6y5m9g2m0vpcrabm10sz5kv0")))

(define-public crate-owoify_rs-1 (crate (name "owoify_rs") (vers "1.0.1") (deps (list (crate-dep (name "lazy_static") (req "~1.4.0") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "~0.8.3") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)))) (hash "0gh613yix5bwb826k6zz1kdpwsh39mg0njfic4pdl5r3j41j9b6w")))

(define-public crate-owoify_rs-1 (crate (name "owoify_rs") (vers "1.1.0") (deps (list (crate-dep (name "lazy_static") (req "~1.4.0") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "~0.8.3") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)))) (hash "0xrz2sdm1vcg7pign9axafnchadz1vbhv32pknsalqq2whls009x")))

