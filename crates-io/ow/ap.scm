(define-module (crates-io ow ap) #:use-module (crates-io))

(define-public crate-owapi-0.1 (crate (name "owapi") (vers "0.1.0") (deps (list (crate-dep (name "reqwest") (req "^0.5") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^0.9") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^0.9") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^0.9") (default-features #t) (kind 0)))) (hash "05fwis7b65fg8kwbm5haihldip4zqip0mj27jfb7fvsh39ns7rc7")))

(define-public crate-owapi-1 (crate (name "owapi") (vers "1.0.0") (deps (list (crate-dep (name "reqwest") (req "^0.5") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^0.9") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^0.9") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^0.9") (default-features #t) (kind 0)))) (hash "1adbvfv96q4hil4ip85vbs1z6dyanpqnyj046zvbqph5m6kj4pjg")))

(define-public crate-owapi-2 (crate (name "owapi") (vers "2.0.0") (deps (list (crate-dep (name "reqwest") (req "^0.5") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^0.9") (default-features #t) (kind 0)) (crate-dep (name "serde_derive") (req "^0.9") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^0.9") (default-features #t) (kind 0)))) (hash "1g90j8ydzm8haqn6rmw9y41j42nyyvdgzjx62dybhgx3cg97g692")))

