(define-module (crates-io is op) #:use-module (crates-io))

(define-public crate-isoperm-0.1 (crate (name "isoperm") (vers "0.1.0") (deps (list (crate-dep (name "bimap") (req "^0.6.2") (default-features #t) (kind 0)) (crate-dep (name "itertools") (req "^0.10") (default-features #t) (kind 0)))) (hash "1s7vdf07xs2i218z3kd53nwqqaxs19hi31bjb32ba395a4v5vq1c")))

(define-public crate-isoperm-0.1 (crate (name "isoperm") (vers "0.1.1") (deps (list (crate-dep (name "bimap") (req "^0.6.2") (default-features #t) (kind 0)) (crate-dep (name "itertools") (req "^0.10") (default-features #t) (kind 0)))) (hash "1wwpiw1hb69pdvh3x30vw4i0clqsf09biwh5agwxda646ghg5hpm")))

(define-public crate-isoperm-0.1 (crate (name "isoperm") (vers "0.1.2") (deps (list (crate-dep (name "bimap") (req "^0.6.2") (default-features #t) (kind 0)) (crate-dep (name "itertools") (req "^0.10") (default-features #t) (kind 0)))) (hash "16gf739jghidb7bdcz44pzx6sn6ad90gw9sihaghcbs2anw6pa6g")))

(define-public crate-isopropanol-0.0.1 (crate (name "isopropanol") (vers "0.0.1") (hash "07kl0h21c3vj6bx2mszhg96h6pzzbhza966grgi9rygy4blqw04h")))

