(define-module (crates-io is re) #:use-module (crates-io))

(define-public crate-isrepr-0.1 (crate (name "isrepr") (vers "0.1.0") (deps (list (crate-dep (name "isrepr_macros") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "0zvdsqdnk868pxjz23yxcb5xyvna366zdw8nyxdf11w2rxkbp0zm")))

(define-public crate-isrepr_macros-0.1 (crate (name "isrepr_macros") (vers "0.1.0") (deps (list (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (features (quote ("full" "visit-mut"))) (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (features (quote ("extra-traits"))) (default-features #t) (kind 2)))) (hash "004b8sp9mrm7aids58ylh5sd6m5bivvh2g7ljnskdg5kvfjcii3v")))

