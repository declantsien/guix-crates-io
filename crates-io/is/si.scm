(define-module (crates-io is si) #:use-module (crates-io))

(define-public crate-issi-is31fl3731-0.1 (crate (name "issi-is31fl3731") (vers "0.1.1") (deps (list (crate-dep (name "embedded-hal") (req "^0.2.4") (default-features #t) (kind 0)))) (hash "02prznrmckvhhsb1y2yg9cg52dzlqci224x76r5ks0z7zpcn3dka")))

(define-public crate-issiz-0.1 (crate (name "issiz") (vers "0.1.0") (hash "1fn80kj8msnizglp929dknc651nyjxxdvr35r2f87ni7dkv42flk")))

(define-public crate-issiz-0.1 (crate (name "issiz") (vers "0.1.1") (hash "02qwn1pqa3cb291w8rq8rcxspdfn6f7zyqfl6x3n5am9a6c55yqr")))

(define-public crate-issiz-0.1 (crate (name "issiz") (vers "0.1.2") (hash "118fykk8jz1d4dvrc0m9c6nj63vwyyhc1yhw93nmabaxxjviq7h8")))

(define-public crate-issiz-0.1 (crate (name "issiz") (vers "0.1.3") (hash "08wsi88grvmffhrj3a92knblgb6hvyah0lnp2dcix9b1k71bxa17")))

