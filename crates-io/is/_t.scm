(define-module (crates-io is _t) #:use-module (crates-io))

(define-public crate-is_terminal_polyfill-1 (crate (name "is_terminal_polyfill") (vers "1.70.0") (hash "0018q5cf3rifbnzfc1w1z1xcx9c6i7xlywp2n0fw4limq1vqaizq") (features (quote (("default")))) (rust-version "1.70.0")))

(define-public crate-is_terminal_polyfill-1 (crate (name "is_terminal_polyfill") (vers "1.48.0") (deps (list (crate-dep (name "is-terminal") (req "^0.4.2") (default-features #t) (kind 0)))) (hash "09qrvj2dpiawhp33ik2ffq5flkm59zq1mjk1rs9ihd6h9vl2saxm") (features (quote (("default")))) (rust-version "1.48.0")))

(define-public crate-is_tested-0.1 (crate (name "is_tested") (vers "0.1.0") (deps (list (crate-dep (name "proc-macro-error") (req "^1.0.4") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.107") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "13ic5wkpqbf176clrmw2ciff2vypx0mg32vfa6zm7vkl558nbb6z")))

(define-public crate-is_tested-0.1 (crate (name "is_tested") (vers "0.1.1") (deps (list (crate-dep (name "proc-macro-error") (req "^1.0.4") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.107") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "0rd4qf5yp58nlzchyryyl96ya2yqpd931mczl1i0w5l36fx86ln9")))

(define-public crate-is_thirteen_rs-0.1 (crate (name "is_thirteen_rs") (vers "0.1.0") (deps (list (crate-dep (name "chrono") (req "^0.4") (default-features #t) (kind 0)))) (hash "188hz4l04jkn5sp8xmhly33gznld14jfzmil8a18r61fcih5yjxx")))

(define-public crate-is_trait-0.1 (crate (name "is_trait") (vers "0.1.0") (hash "0pry41cg83nr5gf9rgrq6b3sm7n45faqsxsjxadshdv2wgnad0if")))

(define-public crate-is_trait-0.1 (crate (name "is_trait") (vers "0.1.1") (hash "1phq8gvzk06bvv9fhw0xa0mmjp962r14q4l0fnfn2dbdc86y42yx")))

(define-public crate-is_trait-0.1 (crate (name "is_trait") (vers "0.1.2") (hash "18s57w4sbb9klmi6mc18d9xlpmall6ybaryaka75a0bbfj8d3xry")))

(define-public crate-is_trait-0.1 (crate (name "is_trait") (vers "0.1.3") (hash "1xbwsbgph8pbfz0qj317pyv2qamq2z3s29il6z5jvlvr6y860jza")))

(define-public crate-is_trait-0.1 (crate (name "is_trait") (vers "0.1.4") (hash "0ww1q7by4qd77zcg6rp0d83xz2882rzbq6694hfbk731z2mnnqx7")))

(define-public crate-is_true-0.1 (crate (name "is_true") (vers "0.1.0") (hash "0smnwf7qqwr7d1yzgzwcx288qz2w0bi1gmi48nfy5kixk3svnfm8")))

(define-public crate-is_true_rs-0.1 (crate (name "is_true_rs") (vers "0.1.0") (hash "0rzkzci1c0a6n6jqxp3pl6hgj5x8gfj0y2wfshvjs19jigy3hml9") (yanked #t)))

(define-public crate-is_true_rs-0.1 (crate (name "is_true_rs") (vers "0.1.1") (hash "085n053qbylc3i47jw4i7fyc5n3ml6qrvsxn2nr1b3378svlbfic") (yanked #t)))

(define-public crate-is_true_rs-0.1 (crate (name "is_true_rs") (vers "0.1.2") (hash "0kp74q8nb3qsy6wa317nls82yhbanbpyib07sdlkgs8sv8wgmcbk")))

(define-public crate-is_tuesday-0.1 (crate (name "is_tuesday") (vers "0.1.0") (deps (list (crate-dep (name "chrono") (req "^0.4.24") (default-features #t) (kind 0)))) (hash "1wfr2299i16c7ibrzk5dyg1ar03rqggqh8fs8dfhl6lckv0syiq3")))

(define-public crate-is_tuesday-0.1 (crate (name "is_tuesday") (vers "0.1.1") (deps (list (crate-dep (name "chrono") (req "^0.4.24") (default-features #t) (kind 0)))) (hash "00xvrh13ws7qaax4hppyw5kiabfmjj4p59y28zk41zz0b9q01qrs")))

(define-public crate-is_type-0.1 (crate (name "is_type") (vers "0.1.0") (hash "1r53b0b7b7wz58zgnx6lgmjdlbbk7qfsmgal7pvsmdqjarxv3pmn")))

(define-public crate-is_type-0.2 (crate (name "is_type") (vers "0.2.0") (hash "1cn08mi34fzyr9gr7z3vnclab6n6ylpzqpizn9whd4qv0pqwg8p6")))

(define-public crate-is_type-0.2 (crate (name "is_type") (vers "0.2.1") (hash "1v7knam6fgymp5nzmg58r5lk33q28pjnv4kh3kmydbxzcyhwvrzy")))

