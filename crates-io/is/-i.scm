(define-module (crates-io is #{-i}#) #:use-module (crates-io))

(define-public crate-is-impl-0.1 (crate (name "is-impl") (vers "0.1.0") (deps (list (crate-dep (name "futures-preview") (req "^0.3.0-alpha.19") (features (quote ("compat"))) (default-features #t) (kind 2)) (crate-dep (name "futures01") (req "^0.1") (default-features #t) (kind 2) (package "futures")))) (hash "1cmmwvfic99gbc59y29lyvvn38bm5ca012grw8lh7xvm4nr69njf")))

(define-public crate-is-impl-0.1 (crate (name "is-impl") (vers "0.1.1") (deps (list (crate-dep (name "futures-preview") (req "^0.3.0-alpha.19") (features (quote ("compat"))) (default-features #t) (kind 2)) (crate-dep (name "futures01") (req "^0.1") (default-features #t) (kind 2) (package "futures")))) (hash "1xi1mj94n2b7x42nnd6ynwafj2jjff6pfd44ql6vgb95x4iw1phd")))

(define-public crate-is-interactive-0.1 (crate (name "is-interactive") (vers "0.1.0") (deps (list (crate-dep (name "atty") (req "^0.2.14") (default-features #t) (kind 0)))) (hash "1ml3zxasfi83czawqgb2fv0rsk20n50pq1qw60l8da6a0s5i7hsv")))

