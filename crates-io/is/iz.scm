(define-module (crates-io is iz) #:use-module (crates-io))

(define-public crate-isize-vec-0.1 (crate (name "isize-vec") (vers "0.1.0") (deps (list (crate-dep (name "criterion") (req "^0.3.1") (default-features #t) (kind 2)) (crate-dep (name "quickcheck") (req "^0.9.2") (default-features #t) (kind 2)) (crate-dep (name "quickcheck_macros") (req "^0.9.1") (default-features #t) (kind 2)))) (hash "1a86i6x54m42lkcydj9wfk5rnwx35jqqnf545yl19414895ldbcg")))

(define-public crate-isize-vec-0.1 (crate (name "isize-vec") (vers "0.1.1") (deps (list (crate-dep (name "criterion") (req "^0.3.1") (default-features #t) (kind 2)) (crate-dep (name "quickcheck") (req "^0.9.2") (default-features #t) (kind 2)) (crate-dep (name "quickcheck_macros") (req "^0.9.1") (default-features #t) (kind 2)))) (hash "091y4z1llh2vhx40p9if3f295d5lk252fy2p5lx2rld5qdjsw11a")))

