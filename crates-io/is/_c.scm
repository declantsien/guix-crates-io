(define-module (crates-io is _c) #:use-module (crates-io))

(define-public crate-is_chinese-1 (crate (name "is_chinese") (vers "1.0.0") (hash "1qkkpak11wjb23q44mh6gg05ycc9vbpz8j1bck1rgrvm1pbjcpl9")))

(define-public crate-is_chinese-1 (crate (name "is_chinese") (vers "1.0.1") (hash "0br8xgziw6wjqz8dph1bcy65a7xys2xsypib3gggxcfb7fsv1d3f")))

(define-public crate-is_chinese-1 (crate (name "is_chinese") (vers "1.0.2") (hash "0mf0fkxsc711bbldhz5hd9dr885wxar0b8ackql9c2bmlcy74a64")))

(define-public crate-is_chinese-1 (crate (name "is_chinese") (vers "1.0.3") (deps (list (crate-dep (name "criterion") (req "^0.3") (default-features #t) (kind 2)))) (hash "0n1kibf733hcsr5k709z8525l9ab32mgjp341d3mp3qb4b545hzi")))

(define-public crate-is_ci-0.0.0 (crate (name "is_ci") (vers "0.0.0") (hash "1xa27pg02lgxrlg70n6lr0cvn93yixy60ww1mkm5nrlnsycxbr9h")))

(define-public crate-is_ci-1 (crate (name "is_ci") (vers "1.0.0") (hash "0949lhxdlvaplsfi980ncs7z8dcafbmdxaf74q02rpa5vqzbpdpn")))

(define-public crate-is_ci-1 (crate (name "is_ci") (vers "1.0.1") (hash "1i7v275g230f20gyrq3rkffgcmvv60bgl3zw2rbfzzp87k8b524r")))

(define-public crate-is_ci-1 (crate (name "is_ci") (vers "1.1.0") (hash "10f6mqfscfxgr2zq478ijgx94bqfnggzajzgvr3cgn0mj5yb201m")))

(define-public crate-is_ci-1 (crate (name "is_ci") (vers "1.1.1") (hash "1ywra2z56x6d4pc02zq24a4x7gvpixynh9524icbpchbf9ydwv31")))

(define-public crate-is_ci-1 (crate (name "is_ci") (vers "1.2.0") (hash "0ifwvxmrsj4r29agfzr71bjq6y1bihkx38fbzafq5vl0jn1wjmbn")))

(define-public crate-is_close-0.1 (crate (name "is_close") (vers "0.1.0") (deps (list (crate-dep (name "num-traits") (req "^0.2") (default-features #t) (kind 0)))) (hash "0q2x72zk06r2brvla080drbpq4g1z7d7gyn0p0l7swxn4ny4ia1r")))

(define-public crate-is_close-0.1 (crate (name "is_close") (vers "0.1.1") (deps (list (crate-dep (name "num-traits") (req "^0.2") (default-features #t) (kind 0)))) (hash "1cgdmx7cw283g0splif3l8994wl9s5y0pw624nkx6fdgbk9lcw9r")))

(define-public crate-is_close-0.1 (crate (name "is_close") (vers "0.1.2") (deps (list (crate-dep (name "num-traits") (req "^0.2") (default-features #t) (kind 0)))) (hash "0d5mgdy85kpdhcaawsfcxirp14hwzmkg64hfwhwaamad3bc5kig6")))

(define-public crate-is_close-0.1 (crate (name "is_close") (vers "0.1.3") (deps (list (crate-dep (name "num-traits") (req "^0.2") (default-features #t) (kind 0)))) (hash "18m8g5ly4kn3m2svmcddliscm2rhfbmhjvyf1ldffwa8y40rxm64")))

(define-public crate-is_computer_on-1 (crate (name "is_computer_on") (vers "1.0.0") (deps (list (crate-dep (name "wasm-bindgen") (req "^0.2") (default-features #t) (kind 0)))) (hash "17249k5n01lhijgpifpymcxm6yqs5hgzzgw9yxd1mscxl3xkb5xh")))

(define-public crate-is_copy-0.1 (crate (name "is_copy") (vers "0.1.0") (deps (list (crate-dep (name "chrono") (req "^0.4") (default-features #t) (kind 2)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "md5") (req "^0.7") (default-features #t) (kind 0)) (crate-dep (name "tracing") (req "^0.1") (default-features #t) (kind 2)) (crate-dep (name "tracing-subscriber") (req "^0.3") (default-features #t) (kind 2)))) (hash "0g0w8glih71f88158slf3hz4d632d6cw8yrxw3lbh1lsvvazkjcb")))

(define-public crate-is_copy-0.1 (crate (name "is_copy") (vers "0.1.1") (deps (list (crate-dep (name "chrono") (req "^0.4") (default-features #t) (kind 2)) (crate-dep (name "criterion") (req "^0.4") (features (quote ("html_reports"))) (default-features #t) (kind 2)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "md5") (req "^0.7") (default-features #t) (kind 0)) (crate-dep (name "tracing") (req "^0.1") (default-features #t) (kind 2)) (crate-dep (name "tracing-subscriber") (req "^0.3") (default-features #t) (kind 2)))) (hash "0sr0d819vn1ssphy5cghf6gq84ccw3mfj0iva7vys7r7i6a67304")))

(define-public crate-is_copy-0.1 (crate (name "is_copy") (vers "0.1.2") (deps (list (crate-dep (name "chrono") (req "^0.4") (default-features #t) (kind 2)) (crate-dep (name "criterion") (req "^0.4") (features (quote ("html_reports"))) (default-features #t) (kind 2)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "md5") (req "^0.7") (default-features #t) (kind 0)) (crate-dep (name "tracing") (req "^0.1") (default-features #t) (kind 2)) (crate-dep (name "tracing-subscriber") (req "^0.3") (default-features #t) (kind 2)))) (hash "1c75wdc0br0kc2p7j7c0g6symvdkgg1nbd79fgl22flk7k8660dw")))

(define-public crate-is_copy-0.1 (crate (name "is_copy") (vers "0.1.3") (deps (list (crate-dep (name "chrono") (req "^0.4") (default-features #t) (kind 2)) (crate-dep (name "criterion") (req "^0.4") (features (quote ("html_reports"))) (default-features #t) (kind 2)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "md5") (req "^0.7") (default-features #t) (kind 0)) (crate-dep (name "tracing") (req "^0.1") (default-features #t) (kind 2)) (crate-dep (name "tracing-subscriber") (req "^0.3") (default-features #t) (kind 2)))) (hash "19kl33l7wya1fhjbmzgnzdc6jplyz0brhiw7pzlv2jw9pxgkima0")))

(define-public crate-is_copy-0.1 (crate (name "is_copy") (vers "0.1.4") (deps (list (crate-dep (name "chrono") (req "^0.4") (default-features #t) (kind 2)) (crate-dep (name "criterion") (req "^0.4") (features (quote ("html_reports"))) (default-features #t) (kind 2)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "md-5") (req "^0.10") (default-features #t) (kind 0)) (crate-dep (name "tracing") (req "^0.1") (default-features #t) (kind 2)) (crate-dep (name "tracing-subscriber") (req "^0.3") (default-features #t) (kind 2)))) (hash "1l0l1m6lyp294nzfc8f9c9k7rgq6awxq0r9s33q9619rhffczr18")))

(define-public crate-is_copy-0.1 (crate (name "is_copy") (vers "0.1.5") (deps (list (crate-dep (name "chrono") (req "^0.4") (default-features #t) (kind 2)) (crate-dep (name "criterion") (req "^0.4") (features (quote ("html_reports"))) (default-features #t) (kind 2)) (crate-dep (name "image") (req "^0.24") (default-features #t) (kind 2)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "tracing") (req "^0.1") (default-features #t) (kind 2)) (crate-dep (name "tracing-subscriber") (req "^0.3") (default-features #t) (kind 2)))) (hash "0x26p0s3d8y4sgnj6qasvfri3zb3ffi8vf9m8dpcb989v12gs5n0")))

(define-public crate-is_copy-0.1 (crate (name "is_copy") (vers "0.1.6") (deps (list (crate-dep (name "chrono") (req "^0.4") (default-features #t) (kind 2)) (crate-dep (name "criterion") (req "^0.4") (features (quote ("html_reports"))) (default-features #t) (kind 2)) (crate-dep (name "dev_util") (req "^0.1") (default-features #t) (kind 2)) (crate-dep (name "image") (req "^0.24") (default-features #t) (kind 2)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "tracing") (req "^0.1") (default-features #t) (kind 2)) (crate-dep (name "tracing-subscriber") (req "^0.3") (default-features #t) (kind 2)))) (hash "1gcdx72di8rq860q5f4ahk3vl6yyr09lvw6by4zk80rgz4ggdrsb")))

