(define-module (crates-io is in) #:use-module (crates-io))

(define-public crate-isin-0.1 (crate (name "isin") (vers "0.1.0") (hash "05z7yj4q64l0li39asjssdqilv0n3by8iasc71w49w6hxx631vxn") (yanked #t)))

(define-public crate-isin-0.1 (crate (name "isin") (vers "0.1.1") (hash "0nf3hf5ni35xgdf65cqp3xiw7y4md58xvvd9mmbjl9qjra4j85xh") (yanked #t)))

(define-public crate-isin-0.1 (crate (name "isin") (vers "0.1.2") (hash "1fxfzfyb3bpwwr04lgc95yq7dln8p4877p1b9vzpn2ppbf4wc87g") (yanked #t)))

(define-public crate-isin-0.1 (crate (name "isin") (vers "0.1.3") (hash "0yqvqvn6sb8x0z8bcx7j0h5zk2hszgx2nyfy4v2ni83gx6svv1hk") (yanked #t)))

(define-public crate-isin-0.1 (crate (name "isin") (vers "0.1.4") (hash "10d0dkmmkr9r8lzrji61pwj0i654fa1mr2slklh26dmm6bwn96qn") (yanked #t)))

(define-public crate-isin-0.1 (crate (name "isin") (vers "0.1.5") (deps (list (crate-dep (name "criterion") (req "^0.3") (features (quote ("html_reports"))) (default-features #t) (kind 2)) (crate-dep (name "proptest") (req "^1.0") (default-features #t) (kind 2)))) (hash "0rrhacs8616mx3syncwz3axpx5z4fih1fc3amgdmmd60xkckvl8k") (yanked #t)))

(define-public crate-isin-0.1 (crate (name "isin") (vers "0.1.6") (deps (list (crate-dep (name "criterion") (req "^0.3") (features (quote ("html_reports"))) (default-features #t) (kind 2)) (crate-dep (name "proptest") (req "^1.0") (default-features #t) (kind 2)))) (hash "10zf4jp3z502kjj8ainli6sc9pdx5rrr9gaycxhp3yqdp7hic4a4") (yanked #t)))

(define-public crate-isin-0.1 (crate (name "isin") (vers "0.1.7") (deps (list (crate-dep (name "bstr") (req "^0.2") (features (quote ("std"))) (default-features #t) (kind 0)) (crate-dep (name "criterion") (req "^0.3") (features (quote ("html_reports"))) (default-features #t) (kind 2)) (crate-dep (name "proptest") (req "^1.0") (default-features #t) (kind 2)))) (hash "02wwhfcbgcrb2gdb0a389rhs4x2djwwp2wmcr04ac3vj8szlxxz2") (yanked #t)))

(define-public crate-isin-0.1 (crate (name "isin") (vers "0.1.8") (deps (list (crate-dep (name "bstr") (req "^0.2") (features (quote ("std"))) (default-features #t) (kind 0)) (crate-dep (name "criterion") (req "^0.3") (features (quote ("html_reports"))) (default-features #t) (kind 2)) (crate-dep (name "proptest") (req "^1.0") (default-features #t) (kind 2)))) (hash "06pnsfssc6r7h62yy0nmpxc70a26zhygg2h1r8azp3zcmgv99b9n") (yanked #t)))

(define-public crate-isin-0.1 (crate (name "isin") (vers "0.1.9") (deps (list (crate-dep (name "bstr") (req "^0.2") (features (quote ("std"))) (default-features #t) (kind 0)) (crate-dep (name "criterion") (req "^0.3") (features (quote ("html_reports"))) (default-features #t) (kind 2)) (crate-dep (name "proptest") (req "^1.0") (default-features #t) (kind 2)))) (hash "0arwn19v0wyl4pcjhsmsmf3iinwjp57nm31rbiy1gd2aq48kxild") (yanked #t)))

(define-public crate-isin-0.1 (crate (name "isin") (vers "0.1.10") (deps (list (crate-dep (name "bstr") (req "^0.2") (features (quote ("std"))) (default-features #t) (kind 0)) (crate-dep (name "criterion") (req "^0.3") (features (quote ("html_reports"))) (default-features #t) (kind 2)) (crate-dep (name "proptest") (req "^1.0") (default-features #t) (kind 2)))) (hash "0x3ms0i991pnq5m6wpg1f0c5is2qhc00837dp4apch64b2aqbgzc") (yanked #t)))

(define-public crate-isin-0.1 (crate (name "isin") (vers "0.1.11") (deps (list (crate-dep (name "bstr") (req "^0.2") (features (quote ("std"))) (default-features #t) (kind 0)) (crate-dep (name "criterion") (req "^0.3") (features (quote ("html_reports"))) (default-features #t) (kind 2)) (crate-dep (name "proptest") (req "^1.0") (default-features #t) (kind 2)))) (hash "0z3xs52jlqq3hq7rdcffk2xkxli9ml23isdfz9f5r8pmrykylh11") (yanked #t)))

(define-public crate-isin-0.1 (crate (name "isin") (vers "0.1.12") (deps (list (crate-dep (name "bstr") (req "^0.2") (features (quote ("std"))) (default-features #t) (kind 0)) (crate-dep (name "criterion") (req "^0.3") (features (quote ("html_reports"))) (default-features #t) (kind 2)) (crate-dep (name "proptest") (req "^1.0") (default-features #t) (kind 2)))) (hash "1wbsabaqdm1nxp4p54kqmmjqdjf09661vxa49vs2qilslbkgscpg") (yanked #t)))

(define-public crate-isin-0.1 (crate (name "isin") (vers "0.1.13") (deps (list (crate-dep (name "bstr") (req "^0.2") (features (quote ("std"))) (default-features #t) (kind 0)) (crate-dep (name "criterion") (req "^0.3") (features (quote ("html_reports"))) (default-features #t) (kind 2)) (crate-dep (name "proptest") (req "^1.0") (default-features #t) (kind 2)))) (hash "1ivlrl8h3vvqknhm9vnsp2skxnxqcaji982y25zflgrlll6dbl0a") (yanked #t)))

(define-public crate-isin-0.1 (crate (name "isin") (vers "0.1.14") (deps (list (crate-dep (name "bstr") (req "^1.2") (features (quote ("std"))) (default-features #t) (kind 0)) (crate-dep (name "criterion") (req "^0.3") (features (quote ("html_reports"))) (default-features #t) (kind 2)) (crate-dep (name "proptest") (req "^1.0") (default-features #t) (kind 2)))) (hash "0n0lcmnaa3xdsqsinn4c6sqh3ng1b7kwyvyqms7ihw4ryp7665b5") (yanked #t)))

(define-public crate-isin-0.1 (crate (name "isin") (vers "0.1.15") (deps (list (crate-dep (name "bstr") (req "^1.2") (features (quote ("std"))) (default-features #t) (kind 0)) (crate-dep (name "criterion") (req "^0.3") (features (quote ("html_reports"))) (default-features #t) (kind 2)) (crate-dep (name "proptest") (req "^1.0") (default-features #t) (kind 2)))) (hash "1n4vf741vgmskg5cigkrysqp0cf3a2rx191pkjhc3jkv15vsdf0s") (yanked #t)))

(define-public crate-isin-0.1 (crate (name "isin") (vers "0.1.16") (deps (list (crate-dep (name "criterion") (req "^0.5.1") (features (quote ("html_reports"))) (default-features #t) (kind 2)) (crate-dep (name "proptest") (req "^1.2.0") (default-features #t) (kind 2)))) (hash "19lh6rwh593djqhzf55acii4xxn7q3zs38s9yrnvjnbvvhgaj53r") (yanked #t)))

(define-public crate-isin-0.1 (crate (name "isin") (vers "0.1.17") (deps (list (crate-dep (name "criterion") (req "^0.5.1") (features (quote ("html_reports"))) (default-features #t) (kind 2)) (crate-dep (name "proptest") (req "^1.3.1") (default-features #t) (kind 2)) (crate-dep (name "serde") (req "^1.0.188") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.107") (default-features #t) (kind 2)))) (hash "0g9q7ii431rdv747gs0msww0y91nr4bfja0qpz9zxy30apixi4ld") (features (quote (("default")))) (v 2) (features2 (quote (("serde" "dep:serde"))))))

(define-public crate-isin-0.1 (crate (name "isin") (vers "0.1.18") (deps (list (crate-dep (name "criterion") (req "^0.5.1") (features (quote ("html_reports"))) (default-features #t) (kind 2)) (crate-dep (name "proptest") (req "^1.3.1") (default-features #t) (kind 2)) (crate-dep (name "serde") (req "^1.0.188") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.107") (default-features #t) (kind 2)))) (hash "0l6fk89c5b6ik6lq72w5h5q44zkkhjy5yg2rjbs269gwdj3ckqsy") (features (quote (("default")))) (v 2) (features2 (quote (("serde" "dep:serde"))))))

(define-public crate-ising-0.2 (crate (name "ising") (vers "0.2.1") (deps (list (crate-dep (name "rand") (req "^0.8.5") (features (quote ("std" "small_rng"))) (kind 0)))) (hash "11v5lm0572mhazipxv12iwwvqhhxlwxbv35xmc84dk3zw6gc7all") (yanked #t)))

(define-public crate-ising-0.2 (crate (name "ising") (vers "0.2.2") (deps (list (crate-dep (name "rand") (req "^0.8.5") (features (quote ("std" "small_rng"))) (kind 0)))) (hash "19v29hr70kfl7v0dvd0jj8sg9a1qkrxdjd0gzzzkcfvpadfv4qr7") (yanked #t)))

(define-public crate-ising-0.2 (crate (name "ising") (vers "0.2.3") (deps (list (crate-dep (name "rand") (req "^0.8.5") (features (quote ("std" "small_rng"))) (kind 0)))) (hash "0qgsryqcqzizpsixv9kdpgqvg8wpp4v3nfzhkinmi6a0xbzj7p23")))

(define-public crate-ising_gui-0.3 (crate (name "ising_gui") (vers "0.3.2") (deps (list (crate-dep (name "eframe") (req "^0.22.0") (features (quote ("glow" "default_fonts"))) (kind 0)) (crate-dep (name "ising") (req "=0.2.1") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (features (quote ("std" "small_rng"))) (kind 0)) (crate-dep (name "rfd") (req "^0.11.4") (features (quote ("xdg-portal"))) (kind 0)))) (hash "0d648kyibsl5gi6hwp8znff0nngqvkrvp6s1bin5mlgwwjhy365y") (yanked #t)))

(define-public crate-ising_gui-0.3 (crate (name "ising_gui") (vers "0.3.3") (deps (list (crate-dep (name "eframe") (req "^0.22.0") (features (quote ("glow" "default_fonts"))) (kind 0)) (crate-dep (name "ising") (req "=0.2.3") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (features (quote ("std" "small_rng"))) (kind 0)) (crate-dep (name "rfd") (req "^0.11.4") (features (quote ("xdg-portal"))) (kind 0)))) (hash "1fj9mhfhcdffxrsijhlk6csbhn37l871c19xg2aaybv8sphj1aji")))

(define-public crate-ising_lib-0.1 (crate (name "ising_lib") (vers "0.1.0") (deps (list (crate-dep (name "chrono") (req "^0.4") (default-features #t) (kind 2)) (crate-dep (name "ndarray") (req "^0.12") (default-features #t) (kind 0)) (crate-dep (name "pbr") (req "^1.0") (default-features #t) (kind 2)) (crate-dep (name "pretty_assertions") (req "^0.6") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.6") (default-features #t) (kind 0)) (crate-dep (name "rayon") (req "^1.0") (default-features #t) (kind 2)))) (hash "1nln95r7qf7zps24gi2s5i1ljg5yj4vqw58vklr8y4ai29x571y8")))

(define-public crate-ising_lib-0.2 (crate (name "ising_lib") (vers "0.2.0") (deps (list (crate-dep (name "chrono") (req "^0.4") (default-features #t) (kind 2)) (crate-dep (name "criterion") (req "^0.2") (default-features #t) (kind 2)) (crate-dep (name "ndarray") (req "^0.12") (default-features #t) (kind 0)) (crate-dep (name "pbr") (req "^1.0") (default-features #t) (kind 2)) (crate-dep (name "pretty_assertions") (req "^0.6") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.6") (default-features #t) (kind 0)) (crate-dep (name "rayon") (req "^1.0") (default-features #t) (kind 2)))) (hash "1kgldw62iml6z39ynjxs3gwxh8aidva21gbvfipscqsywqp1g16f")))

(define-public crate-ising_lib-0.3 (crate (name "ising_lib") (vers "0.3.0") (deps (list (crate-dep (name "chrono") (req "^0.4") (default-features #t) (kind 2)) (crate-dep (name "criterion") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "ndarray") (req "^0.12") (default-features #t) (kind 0)) (crate-dep (name "pbr") (req "^1.0") (default-features #t) (kind 2)) (crate-dep (name "pretty_assertions") (req "^0.6") (default-features #t) (kind 2)) (crate-dep (name "rand") (req "^0.7") (features (quote ("small_rng"))) (default-features #t) (kind 0)) (crate-dep (name "rayon") (req "^1.0") (default-features #t) (kind 2)))) (hash "07b84diq9ixi4z8xbfrz3cdp0dwalinsff60fzwxz8r8fy8l4ba4")))

(define-public crate-ising_lib-1 (crate (name "ising_lib") (vers "1.0.0") (deps (list (crate-dep (name "criterion") (req "^0.3.5") (default-features #t) (kind 2)) (crate-dep (name "ndarray") (req "^0.15.4") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)))) (hash "1vfak9azs9vdk081bl8p1xkl6bi0lb3ikprdm9jbf15j64zdm7wc")))

(define-public crate-ising_toy-0.2 (crate (name "ising_toy") (vers "0.2.0") (deps (list (crate-dep (name "criterion") (req "^0.5") (features (quote ("html_reports"))) (default-features #t) (kind 2)) (crate-dep (name "plotly") (req "^0.8.4") (features (quote ("kaleido"))) (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.5") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 0)))) (hash "1d5x8ziqjzmm123yjs2qwb6mv4qzaccwb6mykm5sfh7j3x155vid")))

