(define-module (crates-io is hm) #:use-module (crates-io))

(define-public crate-ishmael-0.1 (crate (name "ishmael") (vers "0.1.0") (deps (list (crate-dep (name "curl") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "rustc-serialize") (req "^0.3") (default-features #t) (kind 0)))) (hash "17wx936bik0fn83vkw4scsv7ynj2j0zfb8pf0469iksqpd6asay5") (yanked #t)))

(define-public crate-ishmael-0.1 (crate (name "ishmael") (vers "0.1.1") (deps (list (crate-dep (name "curl") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "rustc-serialize") (req "^0.3") (default-features #t) (kind 0)))) (hash "0lpim1iyf2bz61l3jpdmv8ik73aw4p2f8iaxilmrddkkvqvsmcdp") (yanked #t)))

(define-public crate-ishmael-0.1 (crate (name "ishmael") (vers "0.1.2") (deps (list (crate-dep (name "curl") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "rustc-serialize") (req "^0.3") (default-features #t) (kind 0)))) (hash "03mix2vmxpskqhg8sxazd41lnidmgksp8frnylqk6ijyasw5937y") (yanked #t)))

