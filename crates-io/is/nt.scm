(define-module (crates-io is nt) #:use-module (crates-io))

(define-public crate-isnt-0.1 (crate (name "isnt") (vers "0.1.0") (hash "002j63y9whlxjycjkvnl56xrja6f9ac4jwad2jsg1jnxjv65hx9a")))

(define-public crate-isnt-even-nor-odd-1 (crate (name "isnt-even-nor-odd") (vers "1.0.0") (deps (list (crate-dep (name "is-even-or-odd") (req "^1.0.0") (default-features #t) (kind 0)))) (hash "0cn8jihcziscb46m6y1sb34pfxmzf8149vcxah2rzh6z0hxgddgn") (yanked #t)))

(define-public crate-isnt-even-nor-odd-1 (crate (name "isnt-even-nor-odd") (vers "1.0.1") (deps (list (crate-dep (name "is-even-or-odd") (req "^1.0.0") (default-features #t) (kind 0)))) (hash "1rks47zs5zjxf40llb5ps70i6g3xxkqp6sp0xdpdpjkd0dwa1kq2")))

(define-public crate-isnt-even-or-odd-0.1 (crate (name "isnt-even-or-odd") (vers "0.1.0") (deps (list (crate-dep (name "is-even-or-odd") (req "^1.0.0") (default-features #t) (kind 0)))) (hash "0p6pfg8i2h9gdckxhfdx8jhgwxm4d1yaz3snd7pkdy6m5k76imlf") (yanked #t)))

(define-public crate-isnt-even-or-odd-1 (crate (name "isnt-even-or-odd") (vers "1.0.0") (deps (list (crate-dep (name "is-even-or-odd") (req "^1.0.0") (default-features #t) (kind 0)))) (hash "1hf9iwv4578ld0naffh66ky1qsj1q033v08l2bpf89m69z9wfjjv") (yanked #t)))

