(define-module (crates-io is o4) #:use-module (crates-io))

(define-public crate-iso4217-0.1 (crate (name "iso4217") (vers "0.1.0") (hash "1l6pnydxs6whmkdhkib8lzwbrdp6hh76jlfgjcsczpmrqrxfxrnz") (yanked #t)))

(define-public crate-iso4217-0.1 (crate (name "iso4217") (vers "0.1.1") (hash "117cci9xzh8x8lgh1501i7g6x502x6ns44fq0gz7fn2g57wrmgjc") (yanked #t)))

(define-public crate-iso4217-0.2 (crate (name "iso4217") (vers "0.2.0") (hash "0mrs6y4ysg5l4671ld09w9qv12flana2cl7xyg0mkacdg9b3r38s") (yanked #t)))

(define-public crate-iso4217-0.3 (crate (name "iso4217") (vers "0.3.0") (hash "1giir7xx4fwl3x6jhaij0idsfhsh5i0svsfydlz3j1jcczq3q9b9") (features (quote (("default" "const-fn") ("const-fn"))))))

(define-public crate-iso4217-0.3 (crate (name "iso4217") (vers "0.3.1") (hash "0p4cgjhzqh5mi9f97lnd04wd280jirwva8gqdhvxy48ikw6k8sqn")))

(define-public crate-iso4217-0.3 (crate (name "iso4217") (vers "0.3.2") (hash "0c12r05y2hrp9n6yd7bdqpbbw77hy8ld9jz9nmvh2kkhk2ls43hs")))

