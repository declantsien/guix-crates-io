(define-module (crates-io is tr) #:use-module (crates-io))

(define-public crate-istr-0.1 (crate (name "istr") (vers "0.1.0") (deps (list (crate-dep (name "ahash") (req "^0.8") (default-features #t) (kind 0)) (crate-dep (name "hashbrown") (req "^0.14") (features (quote ("raw"))) (default-features #t) (kind 0)) (crate-dep (name "nohash-hasher") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "simdutf8") (req "^0.1") (optional #t) (default-features #t) (kind 0)))) (hash "1hf6ax8vjm6xjvxdn6i2p12vy5bm0gvd0wgcral83z07xl58x3pn") (features (quote (("cache-utf8" "simdutf8"))))))

(define-public crate-istring-0.1 (crate (name "istring") (vers "0.1.0") (hash "0frb3nway9zx69avviflwj5i3ynmbw7svl7z8xpqljr7p0im4871")))

(define-public crate-istring-0.1 (crate (name "istring") (vers "0.1.1") (hash "0kllsaf2wn3jypdw91838qbs0dhanp23nf3biwm3iiwp75f1h9b7")))

(define-public crate-istring-0.1 (crate (name "istring") (vers "0.1.2") (hash "1lm9sbv5iil45wb51yxz7gwz6ij6mj7nqj184nld9hm3f5vs9cb6")))

(define-public crate-istring-0.1 (crate (name "istring") (vers "0.1.3") (hash "12grjsa7d7sqk4xmbvac3bcppgs73cn0f6nh7hkc5rx8cqnmmxn4")))

(define-public crate-istring-0.1 (crate (name "istring") (vers "0.1.4") (hash "0zv623krnv3s9ly6drkchxdj1xjjwr90l9m44iwaciv0z38dhqxg")))

(define-public crate-istring-0.1 (crate (name "istring") (vers "0.1.5") (hash "0l4d6z34h3ph7d4jhd0bjws3x54z60wz2r4r2lv3bn8vwx5hnn8f")))

(define-public crate-istring-0.1 (crate (name "istring") (vers "0.1.6") (hash "0vqhny98asqlafh60dp40bg069i6xkhvdxsmb2ls2jy2z8i50j59")))

(define-public crate-istring-0.1 (crate (name "istring") (vers "0.1.7") (hash "06gipjmgkkk4mh0id6iqahbqzvv8djrazgsv9r7r0k7xfbgn44kb")))

(define-public crate-istring-0.1 (crate (name "istring") (vers "0.1.9") (hash "142n1zk1bw94rs8m0fw1b2k5l572qvlpqymcxh49skv8iykbqbdh")))

(define-public crate-istring-0.1 (crate (name "istring") (vers "0.1.10") (hash "00kz2anq4nshml55a1jwybny80w8fdhymn983x539a9vz43r8pwc")))

(define-public crate-istring-0.1 (crate (name "istring") (vers "0.1.11") (hash "0m6m9qcvn485923bbb59qi4dr5bj022f8qixwiacyajcs336xddd")))

(define-public crate-istring-0.1 (crate (name "istring") (vers "0.1.12") (hash "1ckwrm93m3qnf314g4xqs83ig73wj7kd8zp7yqzr3m36a5qq2xqc")))

(define-public crate-istring-0.1 (crate (name "istring") (vers "0.1.13") (hash "0yz62vh8k0gx1xf6cyc7h3x681408hhw1qfsvn84iz86ad5qp5zc")))

(define-public crate-istring-0.2 (crate (name "istring") (vers "0.2.0") (hash "1c05vk82i6hki9y9la6wgbl9cc91kxhi9asdv2c2ahdb3d9fhvwr")))

(define-public crate-istring-0.2 (crate (name "istring") (vers "0.2.1") (hash "086ff52gqs7fbd9g0aadwj54zpq2g4ffcczddwan9h0wq61j7yyy")))

(define-public crate-istring-0.2 (crate (name "istring") (vers "0.2.2") (hash "06fgx59l3c3p280w8b9ppxl89i2b2hfg7z9yvs5x8la5vn0wlw2c")))

(define-public crate-istring-0.3 (crate (name "istring") (vers "0.3.0") (deps (list (crate-dep (name "datasize") (req "^0.2.10") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (optional #t) (default-features #t) (kind 0)))) (hash "15blwhs02a662a2ph6jd7jcy0xh1g2disyyy9z7d47b74awm2xw5") (features (quote (("size" "datasize") ("serialize" "serde"))))))

(define-public crate-istring-0.3 (crate (name "istring") (vers "0.3.2") (deps (list (crate-dep (name "datasize") (req "^0.2.10") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (optional #t) (default-features #t) (kind 0)))) (hash "1l2djyaaq6a1b015nfa6kl3vd272xd7w498pwvi4ykv3m9n3jgf0") (features (quote (("std") ("size" "datasize") ("serialize" "serde"))))))

(define-public crate-istring-0.3 (crate (name "istring") (vers "0.3.3") (deps (list (crate-dep (name "datasize") (req "^0.2.13") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (optional #t) (default-features #t) (kind 0)))) (hash "0r06a4ixn66zhm6jiilx136slgfs4xy4m0xcjigcjv0dpyl5ryl0") (features (quote (("std") ("size" "datasize") ("serialize" "serde"))))))

(define-public crate-istring-0.3 (crate (name "istring") (vers "0.3.4") (deps (list (crate-dep (name "datasize") (req "^0.2") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "rkyv") (req "^0.7") (optional #t) (kind 0)) (crate-dep (name "serde") (req "^1.0") (optional #t) (default-features #t) (kind 0)))) (hash "0fbs0z5snm114ql5xqy5h166nxg02a5x3winsydimg7ckbxwcp47") (features (quote (("std") ("size" "datasize") ("serialize" "serde")))) (v 2) (features2 (quote (("rkyv" "dep:rkyv"))))))

