(define-module (crates-io is pe) #:use-module (crates-io))

(define-public crate-ispell-0.1 (crate (name "ispell") (vers "0.1.0") (hash "1qhj409clwwwag6mqdgqv9wv4iidsnfa6l0q3hlff9w414bjb12d")))

(define-public crate-ispell-0.2 (crate (name "ispell") (vers "0.2.0") (hash "02b2055x6j1a5x4aqpj9am7vlir15nkfph5kggd03rc69nbz56bc")))

(define-public crate-ispell-0.3 (crate (name "ispell") (vers "0.3.0") (hash "0cakxyglk4wd1v2rflcxb8wp8lppcz9cpygmr7y3f0pikhjnnzj4")))

(define-public crate-ispell-0.3 (crate (name "ispell") (vers "0.3.1") (hash "1npj0pdmhn8rx9k1nykg4bk61j91w7921g0qs90jkqhdvd9wbfhw")))

