(define-module (crates-io is _u) #:use-module (crates-io))

(define-public crate-is_utf8-0.1 (crate (name "is_utf8") (vers "0.1.0-alpha") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "02c8ghp4gqkplvzh8wyyzcg47jcp1xjpzvng5f9dq5nrilmx23m4")))

(define-public crate-is_utf8-0.1 (crate (name "is_utf8") (vers "0.1.0") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)))) (hash "15jpvdr5i2vgiwn1pxw6ckz7cj8bgbp0mc3qf0c9rgjczch3707z")))

(define-public crate-is_utf8-0.1 (crate (name "is_utf8") (vers "0.1.1") (hash "0nf14jbgkpsdyf18p4cgbgwyy6s9ksgc8ahlj7v6ms2jiddalgr5")))

(define-public crate-is_utf8-0.1 (crate (name "is_utf8") (vers "0.1.2") (deps (list (crate-dep (name "criterion") (req "^0.3") (default-features #t) (kind 2)))) (hash "11nwg71kjnd0c1hkz4h41llcv8mhgxpzrawc0zqjfdp7304krd6f")))

(define-public crate-is_utf8-0.1 (crate (name "is_utf8") (vers "0.1.3") (deps (list (crate-dep (name "criterion") (req "^0.3") (default-features #t) (kind 2)))) (hash "1lfwmaxkl2q399xpmf0mr02k5kd8r0apdaxfrwyw29d2v7a1lh6n")))

(define-public crate-is_utf8-0.1 (crate (name "is_utf8") (vers "0.1.4") (deps (list (crate-dep (name "criterion") (req "^0.3") (default-features #t) (kind 2)))) (hash "12v32711ii47chhhx3w3ypnx3ri0kf0k3hqjdkiai26c6x62gd1h")))

