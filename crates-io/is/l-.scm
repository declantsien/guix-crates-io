(define-module (crates-io is l-) #:use-module (crates-io))

(define-public crate-isl-rs-0.1 (crate (name "isl-rs") (vers "0.1.0") (deps (list (crate-dep (name "libc") (req "^0.2.133") (default-features #t) (kind 0)))) (hash "0samvz4hh8v42k7m2r2n4ji5zb34jqr9v9lxdqs04nc4v9iddpva") (yanked #t)))

(define-public crate-isl-rs-0.1 (crate (name "isl-rs") (vers "0.1.1") (deps (list (crate-dep (name "fs_extra") (req "^1.2.0") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2.133") (default-features #t) (kind 0)))) (hash "1v8yysbvn5iw9v28908klq0gwfri011bc0xnsr6ri70b9d4x397h")))

(define-public crate-isl-rs-0.1 (crate (name "isl-rs") (vers "0.1.2") (deps (list (crate-dep (name "fs_extra") (req "^1.2.0") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2.133") (default-features #t) (kind 0)))) (hash "0wzm71nsd3x6hrhadqpj436fr8a6vrmmd2n28m3ap67xz80rv01j")))

(define-public crate-isl-rs-0.1 (crate (name "isl-rs") (vers "0.1.3") (deps (list (crate-dep (name "fs_extra") (req "^1.2.0") (default-features #t) (kind 1)) (crate-dep (name "libc") (req "^0.2.133") (default-features #t) (kind 0)))) (hash "09iws6037b333b64947qibpa5czis29zbnprwgzx2j7zapih355x")))

