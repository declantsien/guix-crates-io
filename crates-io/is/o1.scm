(define-module (crates-io is o1) #:use-module (crates-io))

(define-public crate-iso15924-0.1 (crate (name "iso15924") (vers "0.1.0") (deps (list (crate-dep (name "serde") (req "^1") (optional #t) (default-features #t) (kind 0)))) (hash "1c27cs20vzwvwysx861lhf2zwnfvi45z8dfknlznvhv2z3mzpsa5")))

