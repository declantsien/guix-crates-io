(define-module (crates-io is et) #:use-module (crates-io))

(define-public crate-iset-0.0.1 (crate (name "iset") (vers "0.0.1") (deps (list (crate-dep (name "bit-vec") (req "^0.6") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.7") (default-features #t) (kind 2)))) (hash "1pvg00j47a57i89h54jfbq3arg8x8d9qabgb4bhgzrri1y493jdn")))

(define-public crate-iset-0.0.2 (crate (name "iset") (vers "0.0.2") (deps (list (crate-dep (name "bit-vec") (req "^0.6") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.7") (default-features #t) (kind 2)))) (hash "1ncb4223a79bc3v2qrhhvbi7kkl2qpf2d9vr2yskbdbnfqy1sidd")))

(define-public crate-iset-0.0.3 (crate (name "iset") (vers "0.0.3") (deps (list (crate-dep (name "bit-vec") (req "^0.6") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.7") (default-features #t) (kind 2)))) (hash "0ji2vfz3ssn53ygim5w947jdzkwykrk5v9p3klmrq2xbby5p8jwj")))

(define-public crate-iset-0.0.4 (crate (name "iset") (vers "0.0.4") (deps (list (crate-dep (name "bit-vec") (req "^0.6") (kind 0)) (crate-dep (name "rand") (req "^0.8") (default-features #t) (kind 2)))) (hash "0jj4g85yqc1xyms6cx96qww5gbz2kv9h5xh9f7wng4z9hj529hzh") (features (quote (("dot") ("default" "dot"))))))

(define-public crate-iset-0.0.5 (crate (name "iset") (vers "0.0.5") (deps (list (crate-dep (name "bit-vec") (req "^0.6") (kind 0)) (crate-dep (name "rand") (req "^0.8") (default-features #t) (kind 2)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (optional #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 2)))) (hash "11z25fhblky63p3kn24yaii1npwjw22z7k77h97v04k0jg087j62") (features (quote (("dot") ("default" "dot"))))))

(define-public crate-iset-0.1 (crate (name "iset") (vers "0.1.0") (deps (list (crate-dep (name "rand") (req "^0.8") (default-features #t) (kind 2)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (optional #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 2)))) (hash "1ssp8xr6gn7y65i40bbi65993hmwrfmdi36apbq4lhspjy57hmxh") (features (quote (("dot") ("default" "dot"))))))

(define-public crate-iset-0.1 (crate (name "iset") (vers "0.1.1") (deps (list (crate-dep (name "rand") (req "^0.8") (default-features #t) (kind 2)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (optional #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 2)))) (hash "01jgrv766rp79hh7h1rmvbdhhl6nyzir0gh1x6bdazfbjm4sprl3") (features (quote (("dot") ("default" "dot"))))))

(define-public crate-iset-0.2 (crate (name "iset") (vers "0.2.0") (deps (list (crate-dep (name "serde") (req "^1.0") (features (quote ("derive" "std"))) (optional #t) (kind 0)) (crate-dep (name "rand") (req "^0.8") (default-features #t) (kind 2)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 2)))) (hash "0vhai2gby3vfvcirf63m47gaxy04bbalizhrhdi1p9yl2bpbssjr") (features (quote (("std") ("dot" "std") ("default")))) (yanked #t) (v 2) (features2 (quote (("serde" "std" "dep:serde"))))))

(define-public crate-iset-0.2 (crate (name "iset") (vers "0.2.1") (deps (list (crate-dep (name "serde") (req "^1.0") (features (quote ("derive" "std"))) (optional #t) (kind 0)) (crate-dep (name "rand") (req "^0.8") (default-features #t) (kind 2)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 2)))) (hash "0ykrm6a2nprqfh3ssrplwsac54jkykammlpv2i7ijglc52l4d0mj") (features (quote (("std") ("dot" "std") ("default")))) (v 2) (features2 (quote (("serde" "std" "dep:serde"))))))

(define-public crate-iset-0.2 (crate (name "iset") (vers "0.2.2") (deps (list (crate-dep (name "serde") (req "^1.0") (features (quote ("derive" "std"))) (optional #t) (kind 0)) (crate-dep (name "rand") (req "^0.8") (default-features #t) (kind 2)) (crate-dep (name "serde_json") (req "^1.0") (default-features #t) (kind 2)))) (hash "0xmcz43xw5kv853sy86rgd9yvxbgbqqpcqj2p4h7pjw0f06nlwfh") (features (quote (("std") ("dot" "std") ("default")))) (v 2) (features2 (quote (("serde" "std" "dep:serde"))))))

