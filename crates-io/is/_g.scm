(define-module (crates-io is _g) #:use-module (crates-io))

(define-public crate-is_generic-0.1 (crate (name "is_generic") (vers "0.1.0") (deps (list (crate-dep (name "syn") (req "^2.0") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "1snpw9371rm6kz0qrg5b4g13x5qpby9ygjib4a0crzn97dqzhi0a")))

