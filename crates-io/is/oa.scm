(define-module (crates-io is oa) #:use-module (crates-io))

(define-public crate-isoalloc-0.1 (crate (name "isoalloc") (vers "0.1.0") (deps (list (crate-dep (name "libisoalloc-sys") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "04zdq76nbdcy1p6ypqyk3a3wyarki0dn2q25ryd68ng0yrzs021s")))

(define-public crate-isoalloc-0.2 (crate (name "isoalloc") (vers "0.2.0") (deps (list (crate-dep (name "libisoalloc-sys") (req "^0.2.0") (default-features #t) (kind 0)))) (hash "0b878dczkig32ix8aqdsnxwsk67fxh08hzldc3yz5nskhcrlhhaz")))

(define-public crate-isoalloc-0.2 (crate (name "isoalloc") (vers "0.2.1") (deps (list (crate-dep (name "libisoalloc-sys") (req "^0.2.1") (default-features #t) (kind 0)))) (hash "0nmpi2lnyyszb0hx6sbaxrnfqff48wn5dlqkrdxcx3plg0cqksi3")))

(define-public crate-isoalloc-0.2 (crate (name "isoalloc") (vers "0.2.2") (deps (list (crate-dep (name "libisoalloc-sys") (req "^0.2.2") (default-features #t) (kind 0)))) (hash "07vfkz4v7xnvhll3z5m49zqfaqnr83mfic0w0dwi6xwyxi63yx3w")))

(define-public crate-isoalloc-0.2 (crate (name "isoalloc") (vers "0.2.3") (deps (list (crate-dep (name "libisoalloc-sys") (req "^0.2.3") (default-features #t) (kind 0)))) (hash "01ccbd5ndznpd3pw8fw9qzfvbrnwdcr4d4z31qbabab1a9bwmiqx")))

(define-public crate-isoalloc-0.2 (crate (name "isoalloc") (vers "0.2.4") (deps (list (crate-dep (name "libisoalloc-sys") (req "^0.2.4") (default-features #t) (kind 0)))) (hash "048xwh7ikph78vciqb1pwgxqabd1rqslbbpf49w7q5ca0nhv0mj4")))

(define-public crate-isoalloc-0.2 (crate (name "isoalloc") (vers "0.2.5") (deps (list (crate-dep (name "libisoalloc-sys") (req "^0.2.5") (default-features #t) (kind 0)))) (hash "16m8lmfdaiqn1l55kk9l3nzw4h4nmpc25xc0pcbjrply0hla2bz7") (features (quote (("userfaultfd") ("tagging") ("sanity") ("neon") ("mte") ("memory_tagging")))) (yanked #t)))

(define-public crate-isoalloc-0.2 (crate (name "isoalloc") (vers "0.2.6") (deps (list (crate-dep (name "libisoalloc-sys") (req "^0.2.5") (default-features #t) (kind 0)))) (hash "1zck0psvh6mgiqnzb2f2cgzs469mqnbp4ibpc3k46y0nc37zjvfa") (features (quote (("userfaultfd") ("tagging") ("sanity") ("neon") ("mte") ("memory_tagging"))))))

(define-public crate-isoalloc-0.2 (crate (name "isoalloc") (vers "0.2.7") (deps (list (crate-dep (name "libisoalloc-sys") (req "^0.2.7") (default-features #t) (kind 0)))) (hash "0gd050r02kkh4lxil9n8ffq3dly7yxcmv0r4zhy2q23bsj261807") (features (quote (("userfaultfd" "libisoalloc-sys/userfaultfd") ("tagging" "libisoalloc-sys/tagging") ("sanity" "libisoalloc-sys/sanity") ("nothread" "libisoalloc-sys/nothread") ("neon" "libisoalloc-sys/neon") ("mte" "libisoalloc-sys/mte") ("memory_tagging" "libisoalloc-sys/memory_tagging"))))))

(define-public crate-isoalloc-0.2 (crate (name "isoalloc") (vers "0.2.8") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "libisoalloc-sys") (req "^0.2.7") (default-features #t) (kind 0)) (crate-dep (name "spin") (req "^0.9") (default-features #t) (kind 0)))) (hash "14fd1ijlksyjspa5gl67zmd1ncpni9s31ibqx2rihx91r8078f34") (features (quote (("userfaultfd" "libisoalloc-sys/userfaultfd") ("tagging" "libisoalloc-sys/tagging") ("smallmem" "libisoalloc-sys/smallmem") ("sanity" "libisoalloc-sys/sanity") ("nothread" "libisoalloc-sys/nothread") ("neon" "libisoalloc-sys/neon") ("mte" "libisoalloc-sys/mte") ("memory_tagging" "libisoalloc-sys/memory_tagging"))))))

(define-public crate-isoalloc-0.2 (crate (name "isoalloc") (vers "0.2.9") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "libisoalloc-sys") (req "^0.2.9") (default-features #t) (kind 0)) (crate-dep (name "spin") (req "^0.9") (default-features #t) (kind 0)))) (hash "0160dcddbvp8ga81nirhphds9k8j4914fqdrsng267bd5rq76p5d") (features (quote (("userfaultfd" "libisoalloc-sys/userfaultfd") ("tagging" "libisoalloc-sys/tagging") ("smallmem" "libisoalloc-sys/smallmem") ("sanity" "libisoalloc-sys/sanity") ("nothread" "libisoalloc-sys/nothread") ("neon" "libisoalloc-sys/neon") ("mte" "libisoalloc-sys/mte") ("memory_tagging" "libisoalloc-sys/memory_tagging"))))))

