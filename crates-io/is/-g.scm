(define-module (crates-io is -g) #:use-module (crates-io))

(define-public crate-is-gd-0.1 (crate (name "is-gd") (vers "0.1.0") (deps (list (crate-dep (name "async-trait") (req "^0.1.64") (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.11") (features (quote ("blocking"))) (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "1q0wygrcrm66dsgy94vrzn66hqn5v1ls24kagz9aqhbasrh96d5p")))

(define-public crate-is-glob-0.1 (crate (name "is-glob") (vers "0.1.0") (deps (list (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.7.0") (default-features #t) (kind 0)))) (hash "16kzsc67aicys0ain31mxim1wiq25mxzavm22a7fz2c9vlryxfkv")))

