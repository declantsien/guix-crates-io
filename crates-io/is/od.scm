(define-module (crates-io is od) #:use-module (crates-io))

(define-public crate-isodt-0.1 (crate (name "isodt") (vers "0.1.0") (deps (list (crate-dep (name "chrono") (req "^0.4.26") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^4.3.5") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.8.4") (default-features #t) (kind 0)))) (hash "0zqzd96rig3flnbmja4y3x55n8avn0y6h5nyl0n7f6cpm6bwyhpi")))

(define-public crate-isodt-0.1 (crate (name "isodt") (vers "0.1.2") (deps (list (crate-dep (name "chrono") (req "^0.4.26") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^4.3.5") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.8.4") (default-features #t) (kind 0)))) (hash "1885m2f5c8iyzr07kaxzr905cnh6iw7a4s9aad3zky5vd2lhan65")))

(define-public crate-isodt-0.1 (crate (name "isodt") (vers "0.1.3") (deps (list (crate-dep (name "chrono") (req "^0.4.26") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^4.3.5") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.8.4") (default-features #t) (kind 0)))) (hash "1bqsx9b87ig9a3j75z846bhgjn31qf7anjb8idnhisw5wij1yc78")))

