(define-module (crates-io is _s) #:use-module (crates-io))

(define-public crate-is_send_sync-0.1 (crate (name "is_send_sync") (vers "0.1.0") (hash "1fgc25nbp01lnqb9v7168i5dl4ysdr0k10ksz2d11j7wxx1ki8yd")))

(define-public crate-is_signed_trait-0.1 (crate (name "is_signed_trait") (vers "0.1.0") (hash "1fh8mc2fkifx322cdm89vgka3fygrzkinngk026yha978bvr4q1v") (rust-version "1.56.1")))

(define-public crate-is_slice-0.0.0 (crate (name "is_slice") (vers "0.0.0") (deps (list (crate-dep (name "wtest") (req "~0") (default-features #t) (kind 2)))) (hash "0xgzyikdjxfynhfprhzc26pb04l5hl635wgf52vyrx3piz6bz0wd")))

(define-public crate-is_slice-0.0.1 (crate (name "is_slice") (vers "0.0.1") (deps (list (crate-dep (name "wtest") (req "~0") (default-features #t) (kind 2)))) (hash "13by1cs9bxlqas235hhmvwm5mb99z53bhq21rnj2y4j1d5zwbm7w")))

(define-public crate-is_slice-0.0.2 (crate (name "is_slice") (vers "0.0.2") (deps (list (crate-dep (name "wtest_basic") (req "~0") (default-features #t) (kind 2)))) (hash "0v5x6niz0h9jq8d84ifh11k6xph00inb7317jjij1124f2kkhivj")))

(define-public crate-is_slice-0.1 (crate (name "is_slice") (vers "0.1.0") (deps (list (crate-dep (name "wtest_basic") (req "~0.1") (default-features #t) (kind 2)))) (hash "0x0j6agxffaj7n215sm13f3wznj7dlvg1w5zx7jz1apj5lzq52ki")))

(define-public crate-is_slice-0.1 (crate (name "is_slice") (vers "0.1.1") (deps (list (crate-dep (name "test_tools") (req "~0.1") (default-features #t) (kind 2)))) (hash "0fjmlwzy8an44m8xwbwrhf1308c5jnp0k6pcr7lqf67ifijn0f8h")))

(define-public crate-is_slice-0.2 (crate (name "is_slice") (vers "0.2.0") (deps (list (crate-dep (name "test_tools") (req "^0.1.5") (default-features #t) (kind 2)))) (hash "0w0xmfdppgpq659b05hfc2r0k367jj3ip25rpp1navkh8c77g2v8") (features (quote (("use_alloc") ("no_std") ("full" "enabled") ("enabled") ("default" "enabled"))))))

(define-public crate-is_slice-0.3 (crate (name "is_slice") (vers "0.3.0") (deps (list (crate-dep (name "test_tools") (req "^0.1.5") (default-features #t) (kind 2)))) (hash "0acj3gl260hqcwddmpcg7vm0gsx746zx72872ij1bszxvwnvzg9b") (features (quote (("use_alloc") ("no_std") ("full" "enabled") ("enabled") ("default" "enabled"))))))

(define-public crate-is_slice-0.4 (crate (name "is_slice") (vers "0.4.0") (deps (list (crate-dep (name "test_tools") (req "~0.4.0") (default-features #t) (kind 2)))) (hash "0kjgyqzayg20l1nl1v9f7v4hlnszqkb4jh3azhn3gjd0x7n7n5wp") (features (quote (("use_alloc") ("no_std") ("full" "enabled") ("enabled") ("default" "enabled"))))))

(define-public crate-is_slice-0.5 (crate (name "is_slice") (vers "0.5.0") (deps (list (crate-dep (name "test_tools") (req "~0.5.0") (default-features #t) (kind 2)))) (hash "04gh1fll79wls5db71hbprizmwcrlinjsc48v4nl3anxzyi9n9nx") (features (quote (("use_alloc") ("no_std") ("full" "enabled") ("enabled") ("default" "enabled"))))))

(define-public crate-is_slice-0.6 (crate (name "is_slice") (vers "0.6.0") (deps (list (crate-dep (name "test_tools") (req "~0.6.0") (default-features #t) (kind 2)))) (hash "0130609l8627m7x3igj64waavhnpy0wk9f1g7r579x4pr28x2ljr") (features (quote (("use_alloc") ("no_std") ("full" "enabled") ("enabled") ("default" "enabled"))))))

(define-public crate-is_slice-0.7 (crate (name "is_slice") (vers "0.7.0") (deps (list (crate-dep (name "test_tools") (req "~0.7.0") (default-features #t) (kind 2)))) (hash "1japcjb8w5chijizzhalfrjn83gczc8jvhxrxrjvmgaaxsrmigjm") (features (quote (("use_alloc") ("no_std") ("full" "enabled") ("enabled") ("default" "enabled"))))))

(define-public crate-is_slice-0.8 (crate (name "is_slice") (vers "0.8.0") (deps (list (crate-dep (name "test_tools") (req "~0.7.0") (default-features #t) (kind 2)))) (hash "144cvhjgvvxbg4y6zv6fc152knxb7ykv6cvl9sn59lzcfww3j5r5") (features (quote (("use_alloc") ("no_std") ("full" "enabled") ("enabled") ("default" "enabled"))))))

(define-public crate-is_slice-0.9 (crate (name "is_slice") (vers "0.9.0") (deps (list (crate-dep (name "test_tools") (req "~0.8.0") (default-features #t) (kind 2)))) (hash "0pb1mzvhj4ax8qp933qd08zbbg6n4lgaz27rvjxlcw717ky65x8i") (features (quote (("use_alloc" "no_std") ("no_std") ("full" "enabled") ("enabled") ("default" "enabled"))))))

(define-public crate-is_sorted-0.1 (crate (name "is_sorted") (vers "0.1.0") (deps (list (crate-dep (name "rand") (req "0.4.*") (default-features #t) (kind 2)))) (hash "0ffl94lkr7nf5d4d02yif7292wwva9hhjhjyhill9vdynnarvz61") (features (quote (("use_std") ("unstable") ("default" "use_std"))))))

(define-public crate-is_sorted-0.1 (crate (name "is_sorted") (vers "0.1.1") (deps (list (crate-dep (name "rand") (req "0.4.*") (default-features #t) (kind 2)))) (hash "089b75jmic5wgvvywia33pqx1lvf6dfmh03axxr37nrpbi37cwrm") (features (quote (("use_std") ("unstable") ("default" "use_std"))))))

(define-public crate-is_sudo-0.0.1 (crate (name "is_sudo") (vers "0.0.1") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "winapi") (req "^0.3.8") (features (quote ("handleapi" "processthreadsapi" "winnt" "securitybaseapi" "impl-default"))) (default-features #t) (kind 0)))) (hash "1fg4km0sf404bv9acycgqq5is7h6lvqr31nqmfqvb3yrbjq5v4q5")))

(define-public crate-is_superuser-1 (crate (name "is_superuser") (vers "1.0.0") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "winapi") (req "^0.3.9") (features (quote ("securitybaseapi" "processthreadsapi"))) (default-features #t) (target "cfg(windows)") (kind 0)))) (hash "0szfv9h85n4nidrql2y42qhgiinwl6amd076fskgxszb08kv67qk") (yanked #t)))

(define-public crate-is_superuser-1 (crate (name "is_superuser") (vers "1.0.1") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "winapi") (req "^0.3.9") (features (quote ("securitybaseapi" "processthreadsapi"))) (default-features #t) (target "cfg(windows)") (kind 0)))) (hash "0r0g2ln3yv0ay38bj3f29rl5s94xjb8av6l08hzvgv8k1rnzbvbi")))

