(define-module (crates-io is o6) #:use-module (crates-io))

(define-public crate-iso639-0.1 (crate (name "iso639") (vers "0.1.0") (deps (list (crate-dep (name "csv") (req "^1.1.3") (default-features #t) (kind 1)) (crate-dep (name "phf") (req "^0.8.0") (default-features #t) (kind 0)) (crate-dep (name "phf_codegen") (req "^0.8.0") (default-features #t) (kind 1)) (crate-dep (name "structopt") (req "^0.3.15") (optional #t) (default-features #t) (kind 0)))) (hash "1rw0776v7akn1dg92kkaf9i7jm53s0f13jahwf36j5k2vg35cqbm")))

(define-public crate-iso639-1-0.1 (crate (name "iso639-1") (vers "0.1.0") (hash "0mpwwcya80rxz53f0mx1y4kykmlpxfz9rkjp17j0fm5hgrzjjpg5")))

(define-public crate-iso639-1-0.1 (crate (name "iso639-1") (vers "0.1.4") (hash "1gf90yxv9qdsxynmngcpp3qyp3hv85wsi0qxbilay6wzr89sr257")))

(define-public crate-iso639-1-0.1 (crate (name "iso639-1") (vers "0.1.5") (hash "1gkfs4ni0pxm2lf71rk9rplvjijcrnjg9kvrc2ghxw54p027p526")))

(define-public crate-iso639-1-0.1 (crate (name "iso639-1") (vers "0.1.6") (hash "0i2zdfldddz2sq6pvkq16jpd3d8f8964iipmkfl9bx1y3rjxcak1")))

(define-public crate-iso639-1-0.1 (crate (name "iso639-1") (vers "0.1.7") (hash "0ml9rvyf8p7h9aci8cjqvp2i299is1c9v3mj2s6a244n95aywyh9")))

(define-public crate-iso639-1-0.1 (crate (name "iso639-1") (vers "0.1.8") (hash "1iik884qx5l6k13j7vdp3xh5237zvm5kfa97f2hbwr1gwfa2pxrf")))

(define-public crate-iso639-1-0.1 (crate (name "iso639-1") (vers "0.1.9") (hash "0gnwj1xfggm66a43hb25r2ybp75jh4m3l3wrff52nknvcpvclvyr")))

(define-public crate-iso639-1-0.1 (crate (name "iso639-1") (vers "0.1.10") (hash "03v1jvg40ncd0pas7x91wqgrbikdasr6v82q4i0ngnrpnnsmlb5z")))

(define-public crate-iso639-1-0.1 (crate (name "iso639-1") (vers "0.1.11") (hash "1vg9fa2gbdrrvi03ywn3r3ww16s8xlc723xwmkvp3nn8hddcny7y")))

(define-public crate-iso639-1-0.1 (crate (name "iso639-1") (vers "0.1.12") (hash "19c7zq5g7rzwinkd4hj5l86g68sjhp84xg39195lgv72v4hf02lj")))

(define-public crate-iso639-1-0.2 (crate (name "iso639-1") (vers "0.2.0") (deps (list (crate-dep (name "failure") (req "^0.1.3") (default-features #t) (kind 0)))) (hash "1xs7hcy2phwgf36hzvglis1r5yfmsnlr8245cf4mi7nr4mwp58i9")))

(define-public crate-iso639-1-0.2 (crate (name "iso639-1") (vers "0.2.1") (deps (list (crate-dep (name "failure") (req "^0.1.3") (default-features #t) (kind 0)))) (hash "0rpa5yfzvv79vgwr4070n250r8zsx37fq7ky083i8frcjhscg43k")))

(define-public crate-iso639-1-0.3 (crate (name "iso639-1") (vers "0.3.0") (deps (list (crate-dep (name "failure") (req "^0.1.3") (default-features #t) (kind 0)))) (hash "17j8dxis9mdymgyjh59vi8jqziyg5nvczf05drz1967881c71pb8")))

(define-public crate-iso639-1-0.4 (crate (name "iso639-1") (vers "0.4.0") (deps (list (crate-dep (name "failure") (req "^0.1.3") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1") (features (quote ("derive"))) (default-features #t) (kind 2)) (crate-dep (name "serde_json") (req "^1") (default-features #t) (kind 2)) (crate-dep (name "strum") (req "^0.24") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "strum_macros") (req "^0.24") (optional #t) (default-features #t) (kind 0)))) (hash "1971ahpn9hhcdlmwls8167mx99dsbna08241wdw4dcdbb52qv2v1") (v 2) (features2 (quote (("strum" "dep:strum" "dep:strum_macros"))))))

(define-public crate-iso639-1-0.4 (crate (name "iso639-1") (vers "0.4.1") (deps (list (crate-dep (name "failure") (req "^0.1.3") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1") (features (quote ("derive"))) (default-features #t) (kind 2)) (crate-dep (name "serde_json") (req "^1") (default-features #t) (kind 2)) (crate-dep (name "strum") (req "^0.24") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "strum_macros") (req "^0.24") (optional #t) (default-features #t) (kind 0)))) (hash "1dn2h5z48myjplllddrc4q5pb5wy8m01x3h6vvb0lw5jj2zay2hh") (v 2) (features2 (quote (("strum" "dep:strum" "dep:strum_macros"))))))

(define-public crate-iso639_2-0.1 (crate (name "iso639_2") (vers "0.1.2") (hash "09x7nzrjn1ygp9j8hyqsl5h41rd33g5cs896bzb77nipxav3ds5z")))

(define-public crate-iso639_2-0.1 (crate (name "iso639_2") (vers "0.1.3") (hash "119qhy8p2i9d2llh3kzj4g190gyhsrgsf8ryamkcymbd7471ij13")))

(define-public crate-iso639_2-0.1 (crate (name "iso639_2") (vers "0.1.5") (hash "0c6gp2150r0n681wyi83psiaa7w5zq887azbhgv40wsfdgm8cg78")))

(define-public crate-iso639_2-0.1 (crate (name "iso639_2") (vers "0.1.6") (hash "0gnlj2c53lwbwi2vh47lrmv3h6j47hf0cnldr062vjdzmmdl6cpg")))

(define-public crate-iso639_2-0.1 (crate (name "iso639_2") (vers "0.1.7") (hash "0ri2wn7p1bpplrcy333zyp2a26hg2vfi1njmppyyfpplb6g8jrk0")))

(define-public crate-iso639_2-0.1 (crate (name "iso639_2") (vers "0.1.8") (hash "199l8f8hz08nmm7j6d7110cacamplf841srsvcn0gn8zw2xiidrl")))

(define-public crate-iso639_2-0.1 (crate (name "iso639_2") (vers "0.1.9") (hash "0sw41vh8ibnaii9ndsp0vazhdzh9z9qgpgygrsh87kk4qra0dwwx")))

(define-public crate-iso639_2-0.1 (crate (name "iso639_2") (vers "0.1.10") (hash "0s1h398qvk5nv9jbj631hm46vp264s4l2bl5ip0m6bnab8v4iayd")))

(define-public crate-iso639_2-0.1 (crate (name "iso639_2") (vers "0.1.11") (hash "0nr34y9ny18mk6yqhv5335yqggr2hd2c2na4kscad4lmiq3dh42j")))

(define-public crate-iso639_2-0.1 (crate (name "iso639_2") (vers "0.1.12") (hash "10px50gpb5vrs20785d17122jjsml3f7vs89hp2bm111nz5rcrlw")))

(define-public crate-iso639_2-0.1 (crate (name "iso639_2") (vers "0.1.13") (hash "1ww5yvpkq9bd2h87yjgxpl46y5bhpgdgc7jrdicj27vlcff9p60q")))

(define-public crate-iso639_enum-0.1 (crate (name "iso639_enum") (vers "0.1.0") (deps (list (crate-dep (name "Inflector") (req "^0.11.4") (default-features #t) (kind 1)) (crate-dep (name "anyhow") (req "^1.0.38") (default-features #t) (kind 0)) (crate-dep (name "csv") (req "^1.1") (default-features #t) (kind 1)) (crate-dep (name "phf") (req "^0.8.0") (default-features #t) (kind 0)))) (hash "079nrfn9khpdbhj847v0xbvchjaljvc9m33h1l6hcqfzprmdca0v")))

(define-public crate-iso639_enum-0.1 (crate (name "iso639_enum") (vers "0.1.1") (deps (list (crate-dep (name "Inflector") (req "^0.11.4") (default-features #t) (kind 1)) (crate-dep (name "anyhow") (req "^1.0.38") (default-features #t) (kind 0)) (crate-dep (name "csv") (req "^1.1") (default-features #t) (kind 1)) (crate-dep (name "phf") (req "^0.8.0") (default-features #t) (kind 0)))) (hash "19imbzhd06jhfk9xk0i1zpg69nm57lac3vj52j335myhcxrn9a5g")))

(define-public crate-iso639_enum-0.2 (crate (name "iso639_enum") (vers "0.2.2") (deps (list (crate-dep (name "Inflector") (req "^0.11.4") (default-features #t) (kind 1)) (crate-dep (name "anyhow") (req "^1.0.38") (default-features #t) (kind 0)) (crate-dep (name "csv") (req "^1.1") (default-features #t) (kind 1)) (crate-dep (name "phf") (req "^0.8.0") (default-features #t) (kind 0)))) (hash "1sy88w4sy6q3hkb7df4f56lh6n1dadgm9pgdsmcsdhcaa40ih2rr")))

(define-public crate-iso639_enum-0.3 (crate (name "iso639_enum") (vers "0.3.0") (deps (list (crate-dep (name "Inflector") (req "^0.11.4") (default-features #t) (kind 1)) (crate-dep (name "anyhow") (req "^1.0.38") (default-features #t) (kind 0)) (crate-dep (name "csv") (req "^1.1") (default-features #t) (kind 1)) (crate-dep (name "phf") (req "^0.10") (default-features #t) (kind 0)) (crate-dep (name "phf_codegen") (req "^0.10") (default-features #t) (kind 1)))) (hash "0gxg9kak29qnawrnrdx4mzw6nd87kf1q6wrp5imxlh0zs6igz6z2")))

(define-public crate-iso639_enum-0.5 (crate (name "iso639_enum") (vers "0.5.0") (deps (list (crate-dep (name "Inflector") (req "^0.11.4") (default-features #t) (kind 1)) (crate-dep (name "anyhow") (req "^1.0.38") (default-features #t) (kind 0)) (crate-dep (name "csv") (req "^1.1") (default-features #t) (kind 1)) (crate-dep (name "phf") (req "^0.10") (default-features #t) (kind 0)) (crate-dep (name "phf_codegen") (req "^0.10") (default-features #t) (kind 1)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "1md0w35fiq3i90ik3cl6pyiq0v09m1m6p656zzf9if3f3wx32zbj")))

(define-public crate-iso639_enum-0.5 (crate (name "iso639_enum") (vers "0.5.1") (deps (list (crate-dep (name "Inflector") (req "^0.11.4") (default-features #t) (kind 1)) (crate-dep (name "anyhow") (req "^1.0.38") (default-features #t) (kind 0)) (crate-dep (name "csv") (req "^1.1") (default-features #t) (kind 1)) (crate-dep (name "phf") (req "^0.10") (default-features #t) (kind 0)) (crate-dep (name "phf_codegen") (req "^0.10") (default-features #t) (kind 1)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "1sqmvgn4m6xkmg40q4zrwfpszzsh34k9x6c22y4llpw34bgcvn3z")))

(define-public crate-iso639_enum-0.6 (crate (name "iso639_enum") (vers "0.6.0") (deps (list (crate-dep (name "Inflector") (req "^0.11.4") (default-features #t) (kind 1)) (crate-dep (name "anyhow") (req "^1.0.38") (default-features #t) (kind 0)) (crate-dep (name "csv") (req "^1.1") (default-features #t) (kind 1)) (crate-dep (name "phf") (req "^0.10") (default-features #t) (kind 0)) (crate-dep (name "phf_codegen") (req "^0.10") (default-features #t) (kind 1)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (default-features #t) (kind 0)))) (hash "07xwpsj101ngpd9pgnsqfbz33dlwi1v4374vkinkniizyjkqbd48")))

(define-public crate-iso6709parse-0.1 (crate (name "iso6709parse") (vers "0.1.0") (deps (list (crate-dep (name "criterion") (req "^0.5") (default-features #t) (kind 2)) (crate-dep (name "geo-types") (req "^0.7") (default-features #t) (kind 0)) (crate-dep (name "latlon") (req "^0.1.3") (default-features #t) (kind 2)) (crate-dep (name "nom") (req "^7") (default-features #t) (kind 0)))) (hash "1irsy40di8sg9w18v9k5b1qs738340h3g3wh6qs0gvhi6r4k31ry")))

(define-public crate-iso6937-0.1 (crate (name "iso6937") (vers "0.1.0") (hash "0ahmr44sf5xqcl48rsff0vb6x6afiylbvs7q2nvg504h8gmaf2xi")))

