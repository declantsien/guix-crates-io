(define-module (crates-io is _f) #:use-module (crates-io))

(define-public crate-is_false-0.1 (crate (name "is_false") (vers "0.1.0") (deps (list (crate-dep (name "is_true") (req "^0.1") (default-features #t) (kind 0)))) (hash "0p8yz84bg3f6pzl1kc59zxi62ivc1s0cmwi1bd5nam2m0cm2finy")))

(define-public crate-is_false_rs-0.1 (crate (name "is_false_rs") (vers "0.1.2") (deps (list (crate-dep (name "is_true_rs") (req "^0.1.2") (default-features #t) (kind 0)))) (hash "1fgc9r0m4z8ir626i2d3dwcra2v9nvblc22jd6vabbx8ckyx5dfb")))

