(define-module (crates-io is _p) #:use-module (crates-io))

(define-public crate-is_prime-0.1 (crate (name "is_prime") (vers "0.1.0") (deps (list (crate-dep (name "ramp") (req "^0.3.3") (default-features #t) (kind 0)))) (hash "1ssklpsvr731b6a61dcvl8961zcmjnm9fkpmvnqzj5301xs8kb3d")))

(define-public crate-is_prime-0.1 (crate (name "is_prime") (vers "0.1.1") (deps (list (crate-dep (name "ramp") (req "^0.3.3") (default-features #t) (kind 0)))) (hash "0jpl5p5rilvjq5wxzw70sygg8w2dpci1iagp67lgvb1a6n94267s")))

(define-public crate-is_prime-0.1 (crate (name "is_prime") (vers "0.1.2") (deps (list (crate-dep (name "ramp") (req "^0.3.3") (default-features #t) (kind 0)))) (hash "0xlha1n09y9s1hj5mv01zjssqd3w271g5h0azacyx5a68aajsrjj")))

(define-public crate-is_prime-0.1 (crate (name "is_prime") (vers "0.1.3") (deps (list (crate-dep (name "ramp") (req "^0.3.3") (default-features #t) (kind 0)))) (hash "0i7wpjbq70gmacf7ah3adz4bshdvh2rj7sxb9ar000j1ricq7kq8")))

(define-public crate-is_prime-1 (crate (name "is_prime") (vers "1.0.2") (deps (list (crate-dep (name "ramp") (req "^0.3.3") (default-features #t) (kind 0)))) (hash "14hvdpk8har4sy0b3k2nxydzdvrpswrbjhjirh7rrqk9q9ma3jpn")))

(define-public crate-is_prime-1 (crate (name "is_prime") (vers "1.0.4") (deps (list (crate-dep (name "ramp") (req "^0.5.0") (default-features #t) (kind 0)))) (hash "18fxhyq4fzm7z7pkf7464zixhcf70lb50cdqmq3lqd0k8qf5icsh")))

(define-public crate-is_prime-1 (crate (name "is_prime") (vers "1.0.5") (deps (list (crate-dep (name "ramp") (req "^0.5.0") (default-features #t) (kind 0)))) (hash "04fnvajscssbc2k44agcbc9d03flmfl5nrbwjdj8pkycvmg8qdna")))

(define-public crate-is_prime-2 (crate (name "is_prime") (vers "2.0.7") (deps (list (crate-dep (name "num-bigint") (req "=0.4") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "=0.2") (default-features #t) (kind 0)))) (hash "0xs5b9jyn17b4qlxcjnmrk72g32vjbbn7y947p5y3p63bn9igdn0")))

(define-public crate-is_prime-2 (crate (name "is_prime") (vers "2.0.8") (deps (list (crate-dep (name "num-bigint") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2") (default-features #t) (kind 0)))) (hash "07ks0q9kfy75b0p242qr2qbp2939a19cn4mrd39s83j84b67qzcm")))

(define-public crate-is_prime-2 (crate (name "is_prime") (vers "2.0.9") (deps (list (crate-dep (name "num-bigint") (req "^0.4") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2") (default-features #t) (kind 0)))) (hash "13w0pinlvvbr2sk7zxh575b7cipb51jbran40s6xwlw6hambifad")))

(define-public crate-is_proc_translated-0.1 (crate (name "is_proc_translated") (vers "0.1.0") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (target "cfg(target_os = \"macos\")") (kind 0)))) (hash "0n6gw76a54pp496g7ia5i0qsa8gzgyf1r3bnzj6l0a0csbkj1x8k")))

(define-public crate-is_proc_translated-0.1 (crate (name "is_proc_translated") (vers "0.1.1") (deps (list (crate-dep (name "libc") (req "^0.2") (default-features #t) (target "cfg(all(target_os = \"macos\", target_arch = \"x86_64\"))") (kind 0)))) (hash "0jkzwpsqk9afl87i4azibb6w4zdzpl7pjwpxw120sb2y3qk1f6y9")))

