(define-module (crates-io is wo) #:use-module (crates-io))

(define-public crate-iswow64-0.1 (crate (name "iswow64") (vers "0.1.0") (deps (list (crate-dep (name "kernel32-sys") (req "^0.2.2") (default-features #t) (target "cfg(windows)") (kind 0)) (crate-dep (name "winapi") (req "^0.2.8") (default-features #t) (target "cfg(windows)") (kind 0)))) (hash "0mm0gn35zkj98gdj8xnp0hqn9cncp6pp297r80ld520m926zk6p2")))

