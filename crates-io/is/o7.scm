(define-module (crates-io is o7) #:use-module (crates-io))

(define-public crate-iso7816-0.0.0 (crate (name "iso7816") (vers "0.0.0-unreleased") (hash "07kmxk4r1mx4swi4q01vs758gfzwvwlqyfqm588m95sx1njw3dyq")))

(define-public crate-iso7816-0.1 (crate (name "iso7816") (vers "0.1.0-alpha.0") (deps (list (crate-dep (name "heapless") (req "^0.6") (default-features #t) (kind 0)) (crate-dep (name "heapless-bytes") (req "^0.2.0") (default-features #t) (kind 0)))) (hash "1vvni8xv4gb5vchqazfaw2zvp9i0b0w2yf5ki5vc31i7apk821rw")))

(define-public crate-iso7816-0.1 (crate (name "iso7816") (vers "0.1.0-alpha.1") (deps (list (crate-dep (name "delog") (req "^0.1.2") (default-features #t) (kind 0)) (crate-dep (name "heapless") (req "^0.7") (default-features #t) (kind 0)) (crate-dep (name "hex-literal") (req "^0.3.1") (default-features #t) (kind 2)))) (hash "05d3pzpirbl9zaci3ydwhl9bpc1bvah6nlgy32rwwfgmpb71mb6m")))

(define-public crate-iso7816-0.1 (crate (name "iso7816") (vers "0.1.0") (deps (list (crate-dep (name "delog") (req "^0.1.2") (default-features #t) (kind 0)) (crate-dep (name "heapless") (req "^0.7") (default-features #t) (kind 0)) (crate-dep (name "hex-literal") (req "^0.3.1") (default-features #t) (kind 2)))) (hash "07f7zz7w79r9mhkw7qvsynqls1lkh7nxx5k9pv45sfqnyqpr88d9")))

(define-public crate-iso7816-0.1 (crate (name "iso7816") (vers "0.1.1") (deps (list (crate-dep (name "delog") (req "^0.1.2") (default-features #t) (kind 0)) (crate-dep (name "heapless") (req "^0.7") (default-features #t) (kind 0)) (crate-dep (name "hex-literal") (req "^0.3.1") (default-features #t) (kind 2)))) (hash "0lpnwfn63hvfwywhvv5q8agybyzwpf8cx5amhsvlk4sh7msarrp7")))

(define-public crate-iso7816-0.1 (crate (name "iso7816") (vers "0.1.2") (deps (list (crate-dep (name "delog") (req "^0.1.2") (default-features #t) (kind 0)) (crate-dep (name "heapless") (req "^0.7") (default-features #t) (kind 0)) (crate-dep (name "heapless-bytes") (req "^0.3.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "hex-literal") (req "^0.3.1") (default-features #t) (kind 2)) (crate-dep (name "quickcheck") (req "^1.0.3") (default-features #t) (kind 2)) (crate-dep (name "quickcheck_macros") (req "^1.0.0") (default-features #t) (kind 2)))) (hash "036j4xdy56ii1szkr4jwnixrxpsww4c22lw06bm7l7l2kjn77bxk") (features (quote (("std"))))))

(define-public crate-iso7816-tlv-0.1 (crate (name "iso7816-tlv") (vers "0.1.0") (deps (list (crate-dep (name "untrusted") (req "^0.7.0") (default-features #t) (kind 0)))) (hash "0mpcvz06b1pqds8pk4cf671pzvnmj8c8d1kcrddaf067dgi2jsnx")))

(define-public crate-iso7816-tlv-0.1 (crate (name "iso7816-tlv") (vers "0.1.1") (deps (list (crate-dep (name "untrusted") (req "^0.7.0") (default-features #t) (kind 0)))) (hash "0vsbcsd7q2x9laprqg52a8853y4kc70a9vfdzfzra162m3ckhn6i")))

(define-public crate-iso7816-tlv-0.2 (crate (name "iso7816-tlv") (vers "0.2.0") (deps (list (crate-dep (name "untrusted") (req "^0.7.0") (default-features #t) (kind 0)))) (hash "1ap8byibp95r3ch5vd0gh6dsgznx8ragivglfhrf3phpsrp5vm5b")))

(define-public crate-iso7816-tlv-0.2 (crate (name "iso7816-tlv") (vers "0.2.1") (deps (list (crate-dep (name "untrusted") (req "^0.7.0") (default-features #t) (kind 0)))) (hash "0d0mfsydzkh1ywq2zv8dnaqkmdrwnhpdcqgz4134x4sfyz2wny5f")))

(define-public crate-iso7816-tlv-0.3 (crate (name "iso7816-tlv") (vers "0.3.0") (deps (list (crate-dep (name "rand") (req "^0.7") (default-features #t) (kind 2)) (crate-dep (name "untrusted") (req "^0.7.0") (default-features #t) (kind 0)))) (hash "183kb49jlnr6himh5dd5bw50zd3pwd1wjbkm4f19m5448bkha3bq")))

(define-public crate-iso7816-tlv-0.4 (crate (name "iso7816-tlv") (vers "0.4.0") (deps (list (crate-dep (name "rand_core") (req "^0.5.1") (default-features #t) (kind 2)) (crate-dep (name "rand_xorshift") (req "^0.2") (default-features #t) (kind 2)) (crate-dep (name "static-alloc") (req "^0.2.1") (default-features #t) (kind 2)) (crate-dep (name "untrusted") (req "^0.7.0") (default-features #t) (kind 0)))) (hash "034fyw5d77i54axyd418a3pk3jiwai0cbfr0ilqbrzz7qnpjzw9j")))

(define-public crate-iso7816-tlv-0.4 (crate (name "iso7816-tlv") (vers "0.4.1") (deps (list (crate-dep (name "rand_core") (req "^0.5.1") (default-features #t) (kind 2)) (crate-dep (name "rand_xorshift") (req "^0.2") (default-features #t) (kind 2)) (crate-dep (name "static-alloc") (req "^0.2.1") (default-features #t) (kind 2)) (crate-dep (name "untrusted") (req "^0.7.0") (default-features #t) (kind 0)))) (hash "0am547d5lym9n753lfnmn6p6k220wmjwcnc0yxaq72jw64ckcbw0")))

(define-public crate-iso7816-tlv-0.4 (crate (name "iso7816-tlv") (vers "0.4.2") (deps (list (crate-dep (name "hex-literal") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "rand_core") (req "^0.6") (default-features #t) (kind 2)) (crate-dep (name "rand_xorshift") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "static-alloc") (req "^0.2.1") (default-features #t) (kind 2)) (crate-dep (name "untrusted") (req "^0.9") (default-features #t) (kind 0)))) (hash "10ifspyqw8d7rxpj0n01rr5y702q2mj2i9zlpiph3d9ywq58wp9r")))

(define-public crate-iso7816-tlv-0.4 (crate (name "iso7816-tlv") (vers "0.4.3") (deps (list (crate-dep (name "hex-literal") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "rand_core") (req "^0.6") (default-features #t) (kind 2)) (crate-dep (name "rand_xorshift") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "static-alloc") (req "^0.2.1") (default-features #t) (kind 2)) (crate-dep (name "untrusted") (req "^0.9") (default-features #t) (kind 0)))) (hash "0sh18nxzj5nxsbnkpgr2bjpf1978yz04ai1qlgw55hmlqgpnawyl")))

(define-public crate-iso7816-tlv-0.4 (crate (name "iso7816-tlv") (vers "0.4.4") (deps (list (crate-dep (name "hex-literal") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "rand_core") (req "^0.6") (default-features #t) (kind 2)) (crate-dep (name "rand_xorshift") (req "^0.3") (default-features #t) (kind 2)) (crate-dep (name "static-alloc") (req "^0.2.1") (default-features #t) (kind 2)) (crate-dep (name "untrusted") (req "^0.9") (default-features #t) (kind 0)))) (hash "0immb18gxx8sycf4k3ks2qxhz8sl8ra5s9wa4a8dccd84j6x4q3n")))

(define-public crate-iso7816_tx-0.1 (crate (name "iso7816_tx") (vers "0.1.0") (deps (list (crate-dep (name "hex-literal") (req "^0.4") (default-features #t) (kind 2)))) (hash "0iv9h558md462m628dbz0gz7mgkwx8zy877chbb7yyi6ka0pf7ka")))

(define-public crate-iso7816_tx-0.1 (crate (name "iso7816_tx") (vers "0.1.1") (deps (list (crate-dep (name "hex-literal") (req "^0.4") (default-features #t) (kind 2)))) (hash "0hn64wydg82hiwilnw27izbyzbbzfxa7h4s3sd51qpbdi8b5hd4h")))

(define-public crate-iso7816_tx-0.1 (crate (name "iso7816_tx") (vers "0.1.2") (deps (list (crate-dep (name "hex-literal") (req "^0.4") (default-features #t) (kind 2)))) (hash "1acx9d9nskcq61hji2fmi3jjrdym4dybplx7fbvwnlb0yp6wrabw")))

