(define-module (crates-io is -c) #:use-module (crates-io))

(define-public crate-is-container-0.1 (crate (name "is-container") (vers "0.1.0") (hash "1d3n19zsjr5mpmdbjg7mjx8lkd6kiqcfb4n183xz4na942kl8ga3")))

(define-public crate-is-container-0.1 (crate (name "is-container") (vers "0.1.1") (deps (list (crate-dep (name "once_cell") (req "^1.17") (default-features #t) (kind 0)))) (hash "02js8cjvlh24dkccnaxysq77k4xgy4m3bnv3idgh0i4jvxxdm9w9") (yanked #t)))

(define-public crate-is-container-0.1 (crate (name "is-container") (vers "0.1.2") (deps (list (crate-dep (name "once_cell") (req "^1.17") (default-features #t) (kind 0)))) (hash "1wi0gbkmj93llv6gaynlgrnh4l06q9i404rdka342f4jhvvkl1vy")))

