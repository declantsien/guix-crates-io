(define-module (crates-io is _a) #:use-module (crates-io))

(define-public crate-is_affected-0.4 (crate (name "is_affected") (vers "0.4.1") (deps (list (crate-dep (name "git2") (req "^0.13.23") (kind 0)) (crate-dep (name "log") (req "^0.4.14") (default-features #t) (kind 0)) (crate-dep (name "pretty_env_logger") (req "^0.4.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.5.4") (default-features #t) (kind 0)) (crate-dep (name "rstest") (req "^0.11.0") (default-features #t) (kind 2)) (crate-dep (name "structopt") (req "^0.3.25") (default-features #t) (kind 0)))) (hash "0miybr0riddmak3rj4rgz9j75rwm4h9dkra2w2vhh6d8523bdl4h")))

(define-public crate-is_affected-0.4 (crate (name "is_affected") (vers "0.4.2") (deps (list (crate-dep (name "git2") (req "^0.13.23") (kind 0)) (crate-dep (name "is_affected_lib") (req "^0.1.1") (default-features #t) (kind 0)) (crate-dep (name "log") (req "^0.4.14") (default-features #t) (kind 0)) (crate-dep (name "pretty_env_logger") (req "^0.4.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.5.4") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.3.25") (default-features #t) (kind 0)))) (hash "00vw19vabp5v6dbsamy2v4gbzz6g5hd6f15z15w6rhz5lyhzg4sg")))

(define-public crate-is_affected_lib-0.1 (crate (name "is_affected_lib") (vers "0.1.1") (deps (list (crate-dep (name "git2") (req "^0.13.23") (kind 0)) (crate-dep (name "log") (req "^0.4.14") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.5.4") (default-features #t) (kind 0)) (crate-dep (name "rstest") (req "^0.11.0") (default-features #t) (kind 2)))) (hash "1a5sqnjg445irj7zviyfcyhv7vqdfj5w6i5gndmq8fgrwwjjbv3k")))

(define-public crate-is_anagram-0.1 (crate (name "is_anagram") (vers "0.1.0") (deps (list (crate-dep (name "ascii") (req "^0.8.4") (default-features #t) (kind 0)) (crate-dep (name "error-chain") (req "^0.10.0") (default-features #t) (kind 0)))) (hash "1g025bx7shbpca7dalf4731s8i6c0v2qcbmds50zj738lja3smz7")))

(define-public crate-is_available-0.1 (crate (name "is_available") (vers "0.1.0") (deps (list (crate-dep (name "reqwest") (req "^0.11") (features (quote ("json"))) (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.14.0") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "18nphdrr1l112v6y06dpkj6vmd8jw77zzy0f5isx68xqa0blm8yj")))

