(define-module (crates-io is ey) #:use-module (crates-io))

(define-public crate-iseyen-0.1 (crate (name "iseyen") (vers "0.1.0") (deps (list (crate-dep (name "futures-core") (req "^0.3") (features (quote ("alloc"))) (default-features #t) (kind 0)) (crate-dep (name "futures-util") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.29") (features (quote ("time" "rt"))) (default-features #t) (kind 0)))) (hash "148hbv6jsbplzd2vndjn01c3l7i2qwxpfrhp4dk66khwl0qsqm3v") (yanked #t) (rust-version "1.56.1")))

(define-public crate-iseyen-0.1 (crate (name "iseyen") (vers "0.1.1") (deps (list (crate-dep (name "futures-core") (req "^0.3") (features (quote ("alloc"))) (default-features #t) (kind 0)) (crate-dep (name "futures-util") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.29") (features (quote ("time" "rt"))) (default-features #t) (kind 0)))) (hash "13kq2l94fhn2vs1vn7k87d24vzddm01bg1rir2c3fsnaw1419s4w") (yanked #t) (rust-version "1.56.1")))

(define-public crate-iseyen-0.1 (crate (name "iseyen") (vers "0.1.2") (deps (list (crate-dep (name "futures-core") (req "^0.3") (features (quote ("alloc"))) (default-features #t) (kind 0)) (crate-dep (name "futures-util") (req "^0.3") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.29") (features (quote ("full"))) (default-features #t) (kind 0)))) (hash "1w7k64isy3cmwkx860hlm6468n2qanndkrmi4c079a0r65il132z") (yanked #t) (rust-version "1.56.1")))

