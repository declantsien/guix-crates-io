(define-module (crates-io is oc) #:use-module (crates-io))

(define-public crate-isocal-0.1 (crate (name "isocal") (vers "0.1.0") (deps (list (crate-dep (name "chrono") (req "^0.4") (default-features #t) (kind 0)))) (hash "012q6l3hyc8vz8hpvm8rrc45nfpx3aq3ksizqmk5rnhxf247haf4")))

(define-public crate-isocal-0.1 (crate (name "isocal") (vers "0.1.1") (deps (list (crate-dep (name "chrono") (req "^0.4") (default-features #t) (kind 0)))) (hash "11dxgv9w5h9w136g47rjxs5a5rnq0kbw2l6wz9f1j735sb6da580")))

(define-public crate-isochronous_finite_fields-1 (crate (name "isochronous_finite_fields") (vers "1.0.0") (hash "05yjhs60m5gixc2rzamplww1q9fcfy3y7vlp1l24zra57wshxlcn")))

(define-public crate-isochronous_finite_fields-1 (crate (name "isochronous_finite_fields") (vers "1.0.1") (deps (list (crate-dep (name "criterion") (req "^0.3.6") (default-features #t) (kind 2)))) (hash "1vml8a4bziwz1y2bqb0hkczkzj1arv3jw42p4b8fxd9zf550yhwq")))

(define-public crate-isocountry-0.1 (crate (name "isocountry") (vers "0.1.1") (deps (list (crate-dep (name "failure") (req "^0.1.1") (default-features #t) (kind 0)))) (hash "0mkfacszlhwhwgr0l5ycc1jj2d2j3kfw0lc0acnl11nsqp1bp6x4") (yanked #t)))

(define-public crate-isocountry-0.1 (crate (name "isocountry") (vers "0.1.2") (deps (list (crate-dep (name "failure") (req "^0.1.1") (default-features #t) (kind 0)))) (hash "0qwriv980bqjph2dxa09bvy7a1r5lrllxiiid1qlki9wkiqskwjm")))

(define-public crate-isocountry-0.1 (crate (name "isocountry") (vers "0.1.3") (deps (list (crate-dep (name "failure") (req "^0.1.1") (default-features #t) (kind 0)))) (hash "10bad0zn747li73qbxa00f9dm3g3xw3dlvx96l7cfg36900jkdyp")))

(define-public crate-isocountry-0.1 (crate (name "isocountry") (vers "0.1.5") (deps (list (crate-dep (name "failure") (req "^0.1.1") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1") (default-features #t) (kind 0)))) (hash "0lhalj1cqwi8ixdcps1syajsa4j2hz7yw9qwxrbzpl8f76bdzq2b")))

(define-public crate-isocountry-0.1 (crate (name "isocountry") (vers "0.1.6") (deps (list (crate-dep (name "failure") (req "^0.1.1") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1") (default-features #t) (kind 0)))) (hash "0093wafggpaih9lrsqml1ig8j661v1pdr5zhzz0ncimb9kwn93ck")))

(define-public crate-isocountry-0.2 (crate (name "isocountry") (vers "0.2.0") (deps (list (crate-dep (name "failure") (req "^0.1.1") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1") (default-features #t) (kind 0)))) (hash "0zfk56jhjy26675wl1vv2x3xv7zz8k7j946schgzi0123zbaks9n")))

(define-public crate-isocountry-0.3 (crate (name "isocountry") (vers "0.3.0") (deps (list (crate-dep (name "serde") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1") (default-features #t) (kind 0)))) (hash "1mc9igrxwm2iyvfqlp6k204x4jfnvrkbhs32wy3zk58avymq5ria")))

(define-public crate-isocountry-0.3 (crate (name "isocountry") (vers "0.3.1") (deps (list (crate-dep (name "serde") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1") (default-features #t) (kind 0)))) (hash "0qvfkjr5390fqpzw2pqwvmx4fqvs3ijlsqgarbv4d48mlgc8l29p")))

(define-public crate-isocountry-0.3 (crate (name "isocountry") (vers "0.3.2") (deps (list (crate-dep (name "serde") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "thiserror") (req "^1") (default-features #t) (kind 0)))) (hash "011y6sb6rs2i85g2jvifx5s54clw7nprinzzhfx08jgvy15xr88y")))

(define-public crate-isocountry-rs-0.1 (crate (name "isocountry-rs") (vers "0.1.0") (deps (list (crate-dep (name "failure") (req "^0.1.1") (default-features #t) (kind 0)))) (hash "0x8k9hrbb7z8cvdiqjnbdj3r0w1056nv31zz3cfbmyy5d6gdgdq8") (yanked #t)))

