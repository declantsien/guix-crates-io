(define-module (crates-io is a-) #:use-module (crates-io))

(define-public crate-isa-l-0.1 (crate (name "isa-l") (vers "0.1.0") (deps (list (crate-dep (name "libisal-sys") (req "^0.1") (default-features #t) (kind 0)))) (hash "19ggrsc7bq4y728qixmf1vmwrks0qv6wf013qzb96hkpw5v5jmyv")))

(define-public crate-isa-l-0.1 (crate (name "isa-l") (vers "0.1.1") (deps (list (crate-dep (name "libisal-sys") (req "^0.1") (default-features #t) (kind 0)))) (hash "0j3dhn52m15dbnf1vh6gwmk0zbq9rkplbm4lxr8kyy5hirx7i324")))

(define-public crate-isa-l-0.1 (crate (name "isa-l") (vers "0.1.2") (deps (list (crate-dep (name "libisal-sys") (req "^0.1") (default-features #t) (kind 0)))) (hash "0rqa1v0gbb909qb4f6riyzdcjdg8rb0c6jsph1yiqszib7885z0b")))

(define-public crate-isa-l-0.2 (crate (name "isa-l") (vers "0.2.0") (deps (list (crate-dep (name "libisal-sys") (req "^0.1") (default-features #t) (kind 0)))) (hash "1yb7jwc3l0la5p9gbzb9i511z6d8vcwk411l3i946bd8a5fjclhf")))

(define-public crate-isa-l-erasure-coder-0.1 (crate (name "isa-l-erasure-coder") (vers "0.1.0") (deps (list (crate-dep (name "isa-l") (req "^0.2") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.7") (kind 2)) (crate-dep (name "rand_pcg") (req "^0.2") (kind 2)))) (hash "0fwdz8jh6y3jcmjrhnpf23hkncd7qpaxb77cy1hkvihxah8ixgsy")))

