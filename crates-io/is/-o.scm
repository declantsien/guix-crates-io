(define-module (crates-io is -o) #:use-module (crates-io))

(define-public crate-is-odd-1 (crate (name "is-odd") (vers "1.0.0") (hash "0znihxhxljvgv3v3nvzcppjmbhjxd2srpl37n1mmy47nr55cqhap")))

(define-public crate-is-odd-1 (crate (name "is-odd") (vers "1.0.1") (hash "0wbzlrs82ll7w7m0831rs6srcmxdf1ia68xjmcrgllajzq1h0ygx")))

(define-public crate-is-odd-1 (crate (name "is-odd") (vers "1.1.0") (hash "0hwjxnl2nid2ga9zsww7f0wjrnixy2z313nhm33nq9qw28c9ckbp")))

(define-public crate-is-online-0.1 (crate (name "is-online") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^3.0.5") (features (quote ("cargo" "derive" "wrap_help"))) (default-features #t) (kind 0)) (crate-dep (name "ipnet") (req "^2.5.0") (default-features #t) (kind 0)) (crate-dep (name "num_cpus") (req "^1.13.1") (default-features #t) (kind 0)) (crate-dep (name "rayon") (req "^1.5.1") (default-features #t) (kind 0)))) (hash "1nf7hyjk2mjihia47gjhkv5bc3nkqkl29mw7qhs1dgmgx3pqrfxr")))

