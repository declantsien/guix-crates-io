(define-module (crates-io is -p) #:use-module (crates-io))

(define-public crate-is-prime-for-primitive-int-0.1 (crate (name "is-prime-for-primitive-int") (vers "0.1.0") (deps (list (crate-dep (name "fast-modulo") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.0") (default-features #t) (kind 2)))) (hash "0qgrs91aq6yqg6xy22i3j8rbymc2csabz1pipp89nsswip2a1cyh")))

(define-public crate-is-prime-for-primitive-int-0.2 (crate (name "is-prime-for-primitive-int") (vers "0.2.0") (deps (list (crate-dep (name "fast-modulo") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.0") (default-features #t) (kind 2)))) (hash "05hp9jlvj6rky8whrs9n49mlvbcwhlm5hl8l4q8zacwvsz12hwv9")))

(define-public crate-is-prime-for-primitive-int-0.3 (crate (name "is-prime-for-primitive-int") (vers "0.3.0") (deps (list (crate-dep (name "fast-modulo") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.0") (default-features #t) (kind 2)))) (hash "1frrv8fq2366g69svfnr7slg01zh2sxai1v90xp2ngf1kcaqcqqk")))

(define-public crate-is-prime-for-primitive-int-0.3 (crate (name "is-prime-for-primitive-int") (vers "0.3.1") (deps (list (crate-dep (name "fast-modulo") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.0") (default-features #t) (kind 2)))) (hash "11brb3vir99s28s4zjwrr2r3rv1w2cwnna97qkdlvfa07qashhli")))

(define-public crate-is-prime-for-primitive-int-0.4 (crate (name "is-prime-for-primitive-int") (vers "0.4.0") (deps (list (crate-dep (name "fast-modulo") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.0") (default-features #t) (kind 2)))) (hash "0afaqb5fz0j6sj6j6vmkmmrxfx6mwlbvv723ywixvp9qvm9597n6")))

(define-public crate-is-prime-for-primitive-int-0.5 (crate (name "is-prime-for-primitive-int") (vers "0.5.0") (deps (list (crate-dep (name "fast-modulo") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "rand") (req "^0.8.0") (default-features #t) (kind 2)))) (hash "13way7msbi1fkg7vndi0sf48ddjr3l3jh3yscy7kp1ivikjhn8l2")))

(define-public crate-is-prime-for-primitive-int-0.5 (crate (name "is-prime-for-primitive-int") (vers "0.5.1") (deps (list (crate-dep (name "rand") (req "^0.8.0") (default-features #t) (kind 2)))) (hash "0k2jaj711yd84crc5vwqqm3f767mk6cjrjcrhsblhmqmw4ng1j45")))

(define-public crate-is-prime2-0.1 (crate (name "is-prime2") (vers "0.1.0") (deps (list (crate-dep (name "criterion") (req "^0.5.1") (features (quote ("tokio" "smol"))) (default-features #t) (kind 0)) (crate-dep (name "itertools") (req "^0.10.5") (default-features #t) (kind 0)) (crate-dep (name "rayon") (req "^1.7.0") (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.11.18") (features (quote ("rustls-tls"))) (kind 0)) (crate-dep (name "smallvec") (req "^1.10.0") (default-features #t) (kind 0)) (crate-dep (name "smol") (req "^1.3.0") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.28.2") (default-features #t) (kind 0)))) (hash "0dzmlsc1gc932nc0v2pa9yskkkmnrrc9d2rqwhs2lvy0fpcx66ac")))

