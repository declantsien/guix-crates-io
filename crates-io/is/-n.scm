(define-module (crates-io is -n) #:use-module (crates-io))

(define-public crate-is-nice-0.0.0 (crate (name "is-nice") (vers "0.0.0") (hash "02xs0r5j9bqlyzc5af8js0mi9h4mv3vgl52iy149c2d4n71amns5")))

(define-public crate-is-nice-0.0.69 (crate (name "is-nice") (vers "0.0.69") (deps (list (crate-dep (name "once_cell") (req "^1.17.1") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.7.1") (default-features #t) (kind 0)) (crate-dep (name "rstest") (req "^0.16.0") (default-features #t) (kind 0)))) (hash "1cd50n8vzb1d7m539nb2bhx7fz9qv7qnjkdhahyicaf2m0m5nnaa")))

(define-public crate-is-not-capnp-0.0.1 (crate (name "is-not-capnp") (vers "0.0.1") (deps (list (crate-dep (name "async-std") (req "^1.12.0") (features (quote ("attributes" "unstable"))) (default-features #t) (kind 0)) (crate-dep (name "capnp") (req "^0.17.2") (default-features #t) (kind 0)) (crate-dep (name "capnp-rpc") (req "^0.17.0") (default-features #t) (kind 0)))) (hash "1s9hnbvpg92r65jmmlhsbb5miq47szg4gp0c9w4rkilqkk564myz")))

