(define-module (crates-io is is) #:use-module (crates-io))

(define-public crate-isis-0.0.0 (crate (name "isis") (vers "0.0.0") (hash "1hgvlffswg5sh5p2y4931zfn84gdj6sqh3lfj5gh880541x0xjfp")))

(define-public crate-isislovecruft-0.0.0 (crate (name "isislovecruft") (vers "0.0.0") (hash "04gghayyl1fbv0dvzdppbk809mc6gqscr5m25cpp8058fgihwqjs")))

