(define-module (crates-io is -d) #:use-module (crates-io))

(define-public crate-is-dark-theme-1 (crate (name "is-dark-theme") (vers "1.0.0") (deps (list (crate-dep (name "core-foundation-sys") (req "^0.8.6") (default-features #t) (target "cfg(target_vendor = \"apple\")") (kind 0)))) (hash "1q19v85rapvwn9m1nbq36im4xyx07mh8d8akc4mkiad0rs2nyq32")))

(define-public crate-is-dev-0.1 (crate (name "is-dev") (vers "0.1.0") (hash "0si48qp9b9w87cjfizf24yr21g63wc1m7z3xpz0wxrkc0h5q14ka")))

(define-public crate-is-digit-0.1 (crate (name "is-digit") (vers "0.1.0") (hash "157ajd3qb488vgr64d5r0xjvbp7ykgzprbw0k18z3r5awrv6ncpl") (yanked #t)))

(define-public crate-is-digit-0.1 (crate (name "is-digit") (vers "0.1.1") (hash "16v9iyc8rknh3nk6nvp5hka95z22kfp227nm0slpcx7wqyp8yhhn") (yanked #t)))

(define-public crate-is-digit-0.1 (crate (name "is-digit") (vers "0.1.2") (hash "10lb24c1z58xxfyasdzh21b84y4b7pyzr60mard3ipcaqd9aqfnl")))

(define-public crate-is-docker-0.1 (crate (name "is-docker") (vers "0.1.0") (hash "0k1rayybbh9shhwwy8spkpmlfksmi6gpjf370xc9066wsl4xykhp")))

(define-public crate-is-docker-0.2 (crate (name "is-docker") (vers "0.2.0") (deps (list (crate-dep (name "once_cell") (req "^1.17.0") (default-features #t) (kind 0)))) (hash "1cyibrv6817cqcpf391m327ss40xlbik8wxcv5h9pj9byhksx2wj")))

