(define-module (crates-io is o_) #:use-module (crates-io))

(define-public crate-iso_11649-0.1 (crate (name "iso_11649") (vers "0.1.1") (hash "14l2a679afh8hy3rpalj4fdapjb312g18kl62rhjafn20nr1rplq") (rust-version "1.56")))

(define-public crate-iso_11649-0.1 (crate (name "iso_11649") (vers "0.1.2") (hash "1gw0jl5a0xhm8zigsnnrw5fnipf37ln9nhw046afm81fvr77rlb7") (rust-version "1.56")))

(define-public crate-iso_14977-0.1 (crate (name "iso_14977") (vers "0.1.0") (deps (list (crate-dep (name "pest") (req "^2.1.3") (default-features #t) (kind 0)) (crate-dep (name "pest_derive") (req "^2.1.0") (default-features #t) (kind 0)))) (hash "04asiajncn7snjy7vc2rj0xamf6l9q8s30g81b89bmirsqh107xf")))

(define-public crate-iso_country-0.1 (crate (name "iso_country") (vers "0.1.0") (hash "1kk5hxw00smbvnv608ibpj53rrgwh5xd0zl733cnijpfcg2lgfsv")))

(define-public crate-iso_country-0.1 (crate (name "iso_country") (vers "0.1.1") (hash "1jdr0snd499zlqza6m6bm6w1fw4ilgbw4slz6zzjn1amqmflm8b3")))

(define-public crate-iso_country-0.1 (crate (name "iso_country") (vers "0.1.2") (hash "04iq0fmy5iqf3pv6pw299lxdpf9srrs3ba71kz2m6kx71xbf48wg")))

(define-public crate-iso_country-0.1 (crate (name "iso_country") (vers "0.1.3") (deps (list (crate-dep (name "serde") (req "^1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1") (default-features #t) (kind 2)))) (hash "14ypkgfynmmjhf3i0fq7lhgyf9cv4dyapzdry5jfz6bik8h3w4mn")))

(define-public crate-iso_country-0.1 (crate (name "iso_country") (vers "0.1.4") (deps (list (crate-dep (name "lazy_static") (req "^1.1.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1") (default-features #t) (kind 2)))) (hash "0kb251nf4zd97xsg1s378ap5szr4xh4xn7w66rrylj1rimw3wqr0")))

(define-public crate-iso_currency-0.1 (crate (name "iso_currency") (vers "0.1.0") (hash "15kh976bczh4n0y6h89yfv89cgbqzr62pk2135mvd9y1sz9w8nsx")))

(define-public crate-iso_currency-0.2 (crate (name "iso_currency") (vers "0.2.0") (hash "01sq07jv9c64d027pijlbn31phxaa1dvws3vbaz0834af04cv1vm")))

(define-public crate-iso_currency-0.2 (crate (name "iso_currency") (vers "0.2.1") (deps (list (crate-dep (name "serde") (req "^1.0.107") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.52") (default-features #t) (kind 2)))) (hash "1zys7n8xm80yva20kri6540rz9fcwjzbh7ykl0grm5dgwmxqyk3a") (features (quote (("with-serde" "serde") ("default"))))))

(define-public crate-iso_currency-0.3 (crate (name "iso_currency") (vers "0.3.0") (deps (list (crate-dep (name "iso_country") (req "^0.1.4") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.107") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.52") (default-features #t) (kind 2)))) (hash "03jpm4a08ssgvznp3afakpjdkh7clhf1wlg6xsxx7db0h0jcxpn9") (features (quote (("with-serde" "serde") ("default"))))))

(define-public crate-iso_currency-0.3 (crate (name "iso_currency") (vers "0.3.1") (deps (list (crate-dep (name "iso_country") (req "^0.1.4") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.107") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.52") (default-features #t) (kind 2)))) (hash "0dbnwra8lpmdk9vjn7cpyfz7sd2clv8v1zjvpivk1ljb0a6f179y") (features (quote (("with-serde" "serde") ("default"))))))

(define-public crate-iso_currency-0.3 (crate (name "iso_currency") (vers "0.3.2") (deps (list (crate-dep (name "iso_country") (req "^0.1.4") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.107") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.52") (default-features #t) (kind 2)))) (hash "0zly9cldwimk4h7m5dy79j145532y419f613hxqvifznm8i4hz13") (features (quote (("with-serde" "serde") ("default"))))))

(define-public crate-iso_currency-0.4 (crate (name "iso_currency") (vers "0.4.0") (deps (list (crate-dep (name "iso_country") (req "^0.1.4") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.107") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.52") (default-features #t) (kind 2)))) (hash "06x1f2ghcnqg638h08ip2d6gir7svhhnqvr0c65hpps8vfy1n0js") (features (quote (("with-serde" "serde") ("default"))))))

(define-public crate-iso_currency-0.4 (crate (name "iso_currency") (vers "0.4.1") (deps (list (crate-dep (name "iso_country") (req "^0.1.4") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.107") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.52") (default-features #t) (kind 2)))) (hash "0z7vrkm0njwg2vq64ik6jvi6vx6xp9wv3ca4062a5fxfagyy4597") (features (quote (("with-serde" "serde") ("default"))))))

(define-public crate-iso_currency-0.4 (crate (name "iso_currency") (vers "0.4.2") (deps (list (crate-dep (name "iso_country") (req "^0.1.4") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.107") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.52") (default-features #t) (kind 2)))) (hash "0sn9ll7mbkjd58pjjiyr5wxr8vagi8qa8xkjjfl2jdyyk9lr0jm8") (features (quote (("with-serde" "serde") ("default"))))))

(define-public crate-iso_currency-0.4 (crate (name "iso_currency") (vers "0.4.3") (deps (list (crate-dep (name "iso_country") (req "^0.1.4") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.127") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.66") (default-features #t) (kind 2)))) (hash "15p0yr7cky50djgcxkg8l49kvi9cpyagfhg1d1is9l1dvksqdkcq") (features (quote (("with-serde" "serde") ("default"))))))

(define-public crate-iso_currency-0.4 (crate (name "iso_currency") (vers "0.4.4") (deps (list (crate-dep (name "iso_country") (req "^0.1.4") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.127") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.66") (default-features #t) (kind 2)) (crate-dep (name "strum") (req "^0.25.0") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)))) (hash "06rwxjd5mv8hbsvivvq8imz8mk9a7pscmx3wl13j7j4mps0p3w1k") (features (quote (("with-serde" "serde") ("iterator" "strum") ("default"))))))

(define-public crate-iso_iec_7064-0.1 (crate (name "iso_iec_7064") (vers "0.1.0") (hash "19j3c13xrz0jykys959k5qjblbabgjndpq3pm0ygk2wp5h65ng88")))

(define-public crate-iso_iec_7064-0.1 (crate (name "iso_iec_7064") (vers "0.1.1") (hash "1xbgxm3l30v42qd034y4wjkw1wgnb2j6k1qvm4l6zcs3z8pfh35m")))

