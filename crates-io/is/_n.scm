(define-module (crates-io is _n) #:use-module (crates-io))

(define-public crate-is_none_or-0.1 (crate (name "is_none_or") (vers "0.1.0") (hash "0d1332p7dfsglhqjibj398vapxg3f8vh1fjwdfdg68maf358wd2i")))

(define-public crate-is_not-0.1 (crate (name "is_not") (vers "0.1.0") (hash "1ca2wh20py9v747y2mhp6v6hdqq9yj3szb7bj359v6dzld1c1wp7")))

(define-public crate-is_not_tested-0.1 (crate (name "is_not_tested") (vers "0.1.0") (deps (list (crate-dep (name "darling") (req "^0.14.2") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.107") (default-features #t) (kind 0)))) (hash "0j7qsdlmmg7va66cs2lbzafyq0v6sircmx75nwcsy5j0ipx31xhp")))

