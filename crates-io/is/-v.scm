(define-module (crates-io is -v) #:use-module (crates-io))

(define-public crate-is-vowel-0.1 (crate (name "is-vowel") (vers "0.1.0") (deps (list (crate-dep (name "once_cell") (req "^1.7.2") (default-features #t) (kind 0)) (crate-dep (name "unicode-normalization") (req "^0.1.17") (default-features #t) (kind 0)) (crate-dep (name "version-sync") (req "^0.9") (default-features #t) (kind 2)))) (hash "0jy1q3md4y26zckd4nk8r0xk28z6x6igbbyxvwrzdw7dhcyq8p4z")))

