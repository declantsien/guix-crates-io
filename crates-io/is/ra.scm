(define-module (crates-io is ra) #:use-module (crates-io))

(define-public crate-israeli-queue-0.1 (crate (name "israeli-queue") (vers "0.1.0") (hash "1a5wz8m4941x232z0xc0w2ksjwslnqminifmcqi774ag00w3pzj9")))

(define-public crate-israelid-0.1 (crate (name "israelid") (vers "0.1.0") (hash "1gvpkh0nck0p69jjca9s7q75k2l3ghrbllrdzsyj5vfa0ai1x8x9")))

(define-public crate-israelmuca-hello-world-0.1 (crate (name "israelmuca-hello-world") (vers "0.1.0") (hash "1l4pxngdx76i6y0lm62h60h1ka35qzzk9axxyg82g5262cq88g56")))

