(define-module (crates-io is l2) #:use-module (crates-io))

(define-public crate-isl29125-0.1 (crate (name "isl29125") (vers "0.1.0") (deps (list (crate-dep (name "embedded-hal") (req "^0.2.3") (default-features #t) (kind 0)) (crate-dep (name "embedded-hal-mock") (req "^0.7") (default-features #t) (kind 2)) (crate-dep (name "linux-embedded-hal") (req "^0.3") (default-features #t) (kind 2)))) (hash "1gbaw3613jrh7ajn19615ym9cbycvy2yknhlp1zhhf4zrvllnz16")))

