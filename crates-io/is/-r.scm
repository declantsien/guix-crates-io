(define-module (crates-io is -r) #:use-module (crates-io))

(define-public crate-is-railway-1 (crate (name "is-railway") (vers "1.0.0") (hash "0l82vab6nl3rip7my840wxbzqcywinqiwbwqlwp1b3fc2yc9j4cd")))

(define-public crate-is-railway-1 (crate (name "is-railway") (vers "1.0.1") (hash "1vnlx5l5pfpcw95bb87wxg6ig438q8kypmzy1jhijbgns749hrln")))

(define-public crate-is-real-0.1 (crate (name "is-real") (vers "0.1.1") (hash "0yid4w6ailm9i5brrzzpqnnnzfaswnvqhkai6c4cwy4g57qqnifj")))

(define-public crate-is-real-0.1 (crate (name "is-real") (vers "0.1.2") (hash "14821cpglxbh02nzb3fx51l22mdwhbl10dxhq4d90b0dhhrxa5gx")))

(define-public crate-is-real-0.1 (crate (name "is-real") (vers "0.1.0") (hash "00gg4v9zab5fm0f08j7ygf47zrjfbm35awrivgl2kqj909l5rz2k")))

(define-public crate-is-root-0.1 (crate (name "is-root") (vers "0.1.0") (deps (list (crate-dep (name "users") (req "^0.10") (default-features #t) (target "cfg(unix)") (kind 0)) (crate-dep (name "winapi") (req "^0.3") (features (quote ("minwindef" "handleapi" "processthreadsapi" "securitybaseapi"))) (target "cfg(windows)") (kind 0)))) (hash "1v7v5pg38lihcz13k6917vkqby1s4y7ikw7m6dmbqh2lw2cpb7ib")))

(define-public crate-is-root-0.1 (crate (name "is-root") (vers "0.1.1") (deps (list (crate-dep (name "users") (req "^0.10") (default-features #t) (target "cfg(unix)") (kind 0)) (crate-dep (name "winapi") (req "^0.3") (features (quote ("minwindef" "handleapi" "processthreadsapi" "securitybaseapi"))) (target "cfg(windows)") (kind 0)))) (hash "0zpisfqqp393w4sh9r3i4jsc45ngappsn6ghbxmbv5dnrhnzicxs")))

(define-public crate-is-root-0.1 (crate (name "is-root") (vers "0.1.2") (deps (list (crate-dep (name "users") (req "^0.10") (default-features #t) (target "cfg(unix)") (kind 0)) (crate-dep (name "winapi") (req "^0.3") (features (quote ("minwindef" "handleapi" "processthreadsapi" "securitybaseapi"))) (target "cfg(windows)") (kind 0)))) (hash "0wy8ydxg31iwarbh30dp5lzk7pfsf0ib8svh0abiqvz8c0m21904")))

(define-public crate-is-root-0.1 (crate (name "is-root") (vers "0.1.3") (deps (list (crate-dep (name "users") (req "^0.11") (default-features #t) (target "cfg(unix)") (kind 0)) (crate-dep (name "winapi") (req "^0.3") (features (quote ("minwindef" "handleapi" "processthreadsapi" "securitybaseapi"))) (target "cfg(windows)") (kind 0)))) (hash "17pffwxan8fbf9i96i6pf4dz66j1j0n1qgp5xlmz0m8qn1185ycx")))

(define-public crate-is-rtl-0.1 (crate (name "is-rtl") (vers "0.1.0") (hash "1h5r44vc1509y77dw1ch2qsfd42sf5qvzw4k9fhbssm0kz4f8ysp") (yanked #t)))

(define-public crate-is-rtl-0.1 (crate (name "is-rtl") (vers "0.1.1") (hash "12l95a1qwz17yvm5zhiyvglka26iikas9x8i4f9ckc57zpyfmdr1")))

(define-public crate-is-rust-0.1 (crate (name "is-rust") (vers "0.1.0") (hash "0hzlygckjbbawm9vs1s6kkf1ijhcg4n3h4cak09h6b516ybx5jz8")))

