(define-module (crates-io is ob) #:use-module (crates-io))

(define-public crate-isobus-plus-plus-0.0.1 (crate (name "isobus-plus-plus") (vers "0.0.1") (deps (list (crate-dep (name "isobus-plus-plus-sys") (req "^0.0.1") (default-features #t) (kind 0)))) (hash "1fd5gph61nc5ri14cspgzci3a4w9c8vm1g1r5a3xanm6yyps3mzw") (yanked #t)))

(define-public crate-isobus-plus-plus-sys-0.0.1 (crate (name "isobus-plus-plus-sys") (vers "0.0.1") (hash "0f5yyamgqqc2jas7a91rw04wncka0xskynajpf6csimf47w07jzi") (yanked #t)))

