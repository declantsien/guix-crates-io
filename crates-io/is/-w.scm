(define-module (crates-io is -w) #:use-module (crates-io))

(define-public crate-is-wrong-0.1 (crate (name "is-wrong") (vers "0.1.0") (hash "1jk88rim1pvf8hjcsxjj9i9ghwjq8gv197kvzi7r1bjsddwh94x2")))

(define-public crate-is-wrong-0.2 (crate (name "is-wrong") (vers "0.2.0") (hash "0333q4dw0kd1czvbc4ncy0y72jik36win5am1a69vghv3bhc9sck")))

(define-public crate-is-wsl-0.1 (crate (name "is-wsl") (vers "0.1.0") (deps (list (crate-dep (name "is-docker") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "sys-info") (req "^0.9.1") (default-features #t) (kind 0)))) (hash "1jxk0ivaf0nhy6425bc6fv002ql48gjgxd5di0vab66wjvq09gca")))

(define-public crate-is-wsl-0.2 (crate (name "is-wsl") (vers "0.2.0") (deps (list (crate-dep (name "is-docker") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "once_cell") (req "^1.17.0") (default-features #t) (kind 0)) (crate-dep (name "sys-info") (req "^0.9.1") (default-features #t) (kind 0)))) (hash "1h9wl2gw06m2jra9cjhmgaypi6mrxg8i7b432j8f1cq3j4hl71q9")))

(define-public crate-is-wsl-0.3 (crate (name "is-wsl") (vers "0.3.0") (deps (list (crate-dep (name "is-docker") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "once_cell") (req "^1.17.0") (default-features #t) (kind 0)) (crate-dep (name "sys-info") (req "^0.9.1") (default-features #t) (kind 0)))) (hash "08bgxdb17qcl7ca5hvcj4wmfrm4qcw8dl19x4avczn6qaay89qmq")))

(define-public crate-is-wsl-0.4 (crate (name "is-wsl") (vers "0.4.0") (deps (list (crate-dep (name "is-docker") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "once_cell") (req "^1.17.0") (default-features #t) (kind 0)))) (hash "19bs5pq221d4bknnwiqqkqrnsx2in0fsk8fylxm1747iim4hjdhp")))

