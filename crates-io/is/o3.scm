(define-module (crates-io is o3) #:use-module (crates-io))

(define-public crate-iso3166-1 (crate (name "iso3166") (vers "1.0.0") (deps (list (crate-dep (name "serde") (req "^1") (optional #t) (kind 0)))) (hash "1hcm8wgc3yzpz2kzqy1c9d0qgqjkqzr5sw4kfqy0287lhrjz2hp6")))

(define-public crate-iso3166-1 (crate (name "iso3166") (vers "1.0.1") (deps (list (crate-dep (name "serde") (req "^1") (optional #t) (kind 0)))) (hash "0wmzyl5pjzrag91x8vj6wawlw9nhfn4mz0z1qkk51nib7jsc8y2p")))

(define-public crate-iso3166-1-0.1 (crate (name "iso3166-1") (vers "0.1.0") (hash "0h0znbmzdhwj8nfi9qdww6kpqk3whiwimlrksxrshqqhhx8g0nzw")))

(define-public crate-iso3166-1-1 (crate (name "iso3166-1") (vers "1.0.0") (hash "06bybx7bdglwj0nh405svp9l28w7hk0cfmbcbrcy8pqx15il8nqf")))

(define-public crate-iso3166-1-1 (crate (name "iso3166-1") (vers "1.0.1") (hash "1b5r7sbypj188jcc6smcap1msf0b1qsxxmlqlp6iyim0nbf2mrpc")))

(define-public crate-iso3166-3-0.1 (crate (name "iso3166-3") (vers "0.1.0") (hash "0l1d77m3g0samdz4hcva9y7d3a9fsn0ylng6kdcpgdqa2dl7bbky")))

(define-public crate-iso3166-3-0.2 (crate (name "iso3166-3") (vers "0.2.0") (hash "1l4smn2x31zghail0a42b4z3w4a4l6aa4vb58acsnd6lqq9ch4yl")))

(define-public crate-iso3166-3-0.3 (crate (name "iso3166-3") (vers "0.3.0") (hash "0v48c6rv8lscrn7qjwbmkqyl7az09hfaa1m4wbafays2j76k11dy")))

