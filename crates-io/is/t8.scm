(define-module (crates-io is t8) #:use-module (crates-io))

(define-public crate-ist8310-0.1 (crate (name "ist8310") (vers "0.1.0") (deps (list (crate-dep (name "embedded-hal") (req "^0.2.3") (features (quote ("unproven"))) (default-features #t) (kind 0)) (crate-dep (name "embedded-hal-mock") (req "^0.7.1") (default-features #t) (kind 2)))) (hash "01lf2yrxm0qdvgxa93mh7jsazyspwfvrlkfql07pf9mvag9m2pzq")))

(define-public crate-ist8310-0.1 (crate (name "ist8310") (vers "0.1.1") (deps (list (crate-dep (name "embedded-hal") (req "^0.2.3") (features (quote ("unproven"))) (default-features #t) (kind 0)) (crate-dep (name "embedded-hal-mock") (req "^0.7.1") (default-features #t) (kind 2)))) (hash "0pqgr6slghldkrg542wk32swr8ivyp2cz74vaddipgzag0zmi39m")))

