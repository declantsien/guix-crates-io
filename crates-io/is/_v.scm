(define-module (crates-io is _v) #:use-module (crates-io))

(define-public crate-is_variant-0.1 (crate (name "is_variant") (vers "0.1.0") (deps (list (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (default-features #t) (kind 0)))) (hash "0fk6hdar5mc6xg5p5zwzxbi2yii2issj67d8v4i0sg3x049z946b")))

(define-public crate-is_variant-0.1 (crate (name "is_variant") (vers "0.1.1") (deps (list (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (default-features #t) (kind 0)))) (hash "1bs8q9mh440ap33nd9wfps5c31ix41dza1nna95wr1wavf0qgniy")))

(define-public crate-is_variant-1 (crate (name "is_variant") (vers "1.0.0") (deps (list (crate-dep (name "convert_case") (req "^0.4.0") (default-features #t) (kind 0)) (crate-dep (name "proc-macro2") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0") (default-features #t) (kind 0)))) (hash "0kkrsinslxhxrgbmp1x02q92xy9dwfvjbf2vnvq9jqsifchy4dyd")))

