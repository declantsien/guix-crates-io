(define-module (crates-io is bn) #:use-module (crates-io))

(define-public crate-isbn-0.1 (crate (name "isbn") (vers "0.1.0") (hash "1j35m9bihg7bslcwih6sll55clazfsmlkk11mpp4cd9rn8ydrh8b")))

(define-public crate-isbn-0.2 (crate (name "isbn") (vers "0.2.0") (deps (list (crate-dep (name "arrayvec") (req "^0.4") (kind 0)) (crate-dep (name "codegen") (req "^0.1") (default-features #t) (kind 1)) (crate-dep (name "roxmltree") (req "^0.4") (default-features #t) (kind 1)))) (hash "0nqdssvac3ij5j6y8q9xbfk2b8852ycd51g45ib1xig62dv1i7hj")))

(define-public crate-isbn2-0.4 (crate (name "isbn2") (vers "0.4.0") (deps (list (crate-dep (name "arrayvec") (req "^0.5.2") (kind 0)) (crate-dep (name "codegen") (req "^0.1") (default-features #t) (kind 1)) (crate-dep (name "indexmap") (req "^1.6.1") (optional #t) (kind 0)) (crate-dep (name "quick-xml") (req "^0.20.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "roxmltree") (req "^0.13.0") (default-features #t) (kind 1)) (crate-dep (name "serde") (req "^1.0") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)))) (hash "1n4w63sppbgpcql97ivs0rs23268w6gzzn10qfcfwaz1v6n05v1j") (features (quote (("serialize" "serde") ("runtime-ranges" "quick-xml" "indexmap"))))))

(define-public crate-isbnid-0.1 (crate (name "isbnid") (vers "0.1.0") (deps (list (crate-dep (name "regex") (req "*") (default-features #t) (kind 0)))) (hash "0y9b2fnfviyrj2d3339zmh7vlqylrzagzal1n88qaxhmrbpn1nx8")))

(define-public crate-isbnid-0.1 (crate (name "isbnid") (vers "0.1.2") (deps (list (crate-dep (name "regex") (req "0.1.*") (default-features #t) (kind 0)))) (hash "1ia9lmrk7xijc71v92i7nk8hkyi3wlp4h065x9f635sjjzdvss08")))

(define-public crate-isbnid-0.1 (crate (name "isbnid") (vers "0.1.3") (deps (list (crate-dep (name "regex") (req "0.2.*") (default-features #t) (kind 0)))) (hash "1ii37d3qihm1mdxic9b86bppwjxv8szfyx4riggvjkk1gq112mqq")))

