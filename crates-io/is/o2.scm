(define-module (crates-io is o2) #:use-module (crates-io))

(define-public crate-iso20022-0.1 (crate (name "iso20022") (vers "0.1.0") (deps (list (crate-dep (name "prost") (req "^0.6") (default-features #t) (kind 0)) (crate-dep (name "tonic-build") (req "^0.1") (default-features #t) (kind 1)))) (hash "1hfsslac6vzfnqj63kax2ajv2i2dskpcq5ha771dmk1pnnh44z5q")))

(define-public crate-iso20022-0.1 (crate (name "iso20022") (vers "0.1.1") (deps (list (crate-dep (name "prost") (req "^0.6") (default-features #t) (kind 0)) (crate-dep (name "tonic-build") (req "^0.1") (default-features #t) (kind 1)))) (hash "0lhx1jdv5dz03pqwk9a62n9p4zfdg2c1m75qzbga7b1g9r71syn5")))

(define-public crate-iso20022-0.1 (crate (name "iso20022") (vers "0.1.2") (deps (list (crate-dep (name "prost") (req "^0.6") (default-features #t) (kind 0)) (crate-dep (name "tonic-build") (req "^0.1") (default-features #t) (kind 1)))) (hash "0p6wsa4gc6ldsk0q05si1z924cgn4yfv1h56534i8klx15xwzi7f")))

(define-public crate-iso20022-0.1 (crate (name "iso20022") (vers "0.1.3") (deps (list (crate-dep (name "prost") (req "^0.6") (default-features #t) (kind 0)) (crate-dep (name "tonic-build") (req "^0.1") (default-features #t) (kind 1)))) (hash "0s61hinc6d4bclqdk2lv3s5bp285s7cbspn4spvdgp7ym1l266ni")))

(define-public crate-iso20022-0.1 (crate (name "iso20022") (vers "0.1.4") (deps (list (crate-dep (name "prost") (req "^0.6") (default-features #t) (kind 0)) (crate-dep (name "tonic-build") (req "^0.1") (default-features #t) (kind 1)))) (hash "0zixk8h82fmjcky6w56rarwcybczb67j1dwg831rz48cp1csqwn6")))

(define-public crate-iso20022-0.1 (crate (name "iso20022") (vers "0.1.5") (deps (list (crate-dep (name "prost") (req "^0.6") (default-features #t) (kind 0)) (crate-dep (name "tonic-build") (req "^0.1") (default-features #t) (kind 1)))) (hash "1ang3vwqbvyjax7kpj14baq7ddxc6pp55fzml0jyyqcm6x1v71q3")))

(define-public crate-iso20022-0.1 (crate (name "iso20022") (vers "0.1.6") (deps (list (crate-dep (name "prost") (req "^0.6") (default-features #t) (kind 0)) (crate-dep (name "tonic-build") (req "^0.1") (default-features #t) (kind 1)))) (hash "12k4x9pvjj7cpqha6vqsrl2vhl62ynq4iz78nrzf7aqfrsm13c3w")))

(define-public crate-iso20022-0.1 (crate (name "iso20022") (vers "0.1.7") (deps (list (crate-dep (name "prost") (req "^0.6") (default-features #t) (kind 0)) (crate-dep (name "tonic-build") (req "^0.1") (default-features #t) (kind 1)))) (hash "08m2q6q0hn4h585fcy8avb7iz4jmzqfws45vg6gdq2xfkq9jrhcc")))

(define-public crate-iso20022-0.1 (crate (name "iso20022") (vers "0.1.8") (deps (list (crate-dep (name "prost") (req "^0.6") (default-features #t) (kind 0)) (crate-dep (name "tonic-build") (req "^0.1") (default-features #t) (kind 1)))) (hash "1z97w1vd0fvwa7pkh128iw3w570di0v5jkvxy0lmi7nn3lvc8a6l")))

(define-public crate-iso20022-0.1 (crate (name "iso20022") (vers "0.1.9") (deps (list (crate-dep (name "prost") (req "^0.6") (default-features #t) (kind 0)) (crate-dep (name "tonic-build") (req "^0.1") (default-features #t) (kind 1)))) (hash "0ws212pv6cpl5dx03wg56ib58zb1nbphr9ds7nl9y6y8ahkxq1k2")))

