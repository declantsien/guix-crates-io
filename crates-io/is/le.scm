(define-module (crates-io is le) #:use-module (crates-io))

(define-public crate-isle-0.0.0 (crate (name "isle") (vers "0.0.0-reserve") (hash "077mgyzsks1j69v7d9zzhj7hz5scmcqa2yzx1jbgx02xpv037g80")))

(define-public crate-isleep-0.1 (crate (name "isleep") (vers "0.1.0") (deps (list (crate-dep (name "spin_sleep") (req "^1.1.1") (optional #t) (default-features #t) (kind 0)))) (hash "0ch98la3yg0sissqz6wapq66zd39vym053w7iyygd4bvifrsq79f") (v 2) (features2 (quote (("accuracy" "dep:spin_sleep"))))))

(define-public crate-isleep-0.2 (crate (name "isleep") (vers "0.2.0") (deps (list (crate-dep (name "spin_sleep") (req "^1.1.1") (optional #t) (default-features #t) (kind 0)))) (hash "0ihdmj296km8mcgaccxnava0jmah45rksmkjbg3yqsm252agn0p4") (v 2) (features2 (quote (("accuracy" "dep:spin_sleep"))))))

(define-public crate-isleep-0.2 (crate (name "isleep") (vers "0.2.1") (deps (list (crate-dep (name "spin_sleep") (req "^1.1.1") (optional #t) (default-features #t) (kind 0)))) (hash "1ixs0flzc200ahxblqj4333vggq9a461fbgcf9yhylkwfws1vh0n") (v 2) (features2 (quote (("accuracy" "dep:spin_sleep"))))))

(define-public crate-isleep-0.3 (crate (name "isleep") (vers "0.3.0") (deps (list (crate-dep (name "spin_sleep") (req "^1") (optional #t) (default-features #t) (kind 0)))) (hash "1ww2cc996x0y172ibdppv9m7dr9737s6vyy4r3z0r5as9srsp3a0") (v 2) (features2 (quote (("accuracy" "dep:spin_sleep"))))))

(define-public crate-isleep-0.3 (crate (name "isleep") (vers "0.3.1") (deps (list (crate-dep (name "spin_sleep") (req "^1") (optional #t) (default-features #t) (kind 0)))) (hash "1ifd8j5v5zapf12pmi6aqs5322zybz1gcd67xbid1nqq7nk5y9by") (v 2) (features2 (quote (("accuracy" "dep:spin_sleep"))))))

(define-public crate-isleep-0.3 (crate (name "isleep") (vers "0.3.2") (deps (list (crate-dep (name "spin_sleep") (req "^1") (optional #t) (default-features #t) (kind 0)))) (hash "0sf454r45zq0jkws36fyl2fxg4nxi492s2g5514ccrr1hfgkk178") (v 2) (features2 (quote (("accuracy" "dep:spin_sleep"))))))

(define-public crate-islennyfree-0.1 (crate (name "islennyfree") (vers "0.1.0") (deps (list (crate-dep (name "axum") (req "^0.6.11") (default-features #t) (kind 0)) (crate-dep (name "chrono") (req "^0.4.24") (features (quote ("serde"))) (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^4.1.9") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "dioxus") (req "^0.3.2") (default-features #t) (kind 0)) (crate-dep (name "dioxus-ssr") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.156") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "serde_yaml") (req "^0.9.19") (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.26.0") (features (quote ("full"))) (default-features #t) (kind 0)) (crate-dep (name "walkdir") (req "^2.3.2") (default-features #t) (kind 0)))) (hash "1d6y3g4rc6fdyphang1xpcmfmny1awwq43536xfdn7wjg0rilbz6") (yanked #t)))

(define-public crate-islet-0.0.0 (crate (name "islet") (vers "0.0.0") (hash "18m0icvz5j16svjihx9ri3c0lndsx636xngx7yfixr5zhyl1g4ww")))

