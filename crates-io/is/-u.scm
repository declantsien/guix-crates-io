(define-module (crates-io is -u) #:use-module (crates-io))

(define-public crate-is-unicode-supported-0.1 (crate (name "is-unicode-supported") (vers "0.1.0") (deps (list (crate-dep (name "once_cell") (req "^1.17.0") (default-features #t) (kind 0)))) (hash "0p29c1pm0cpspx8xj86g5h18smm7mk1i38yahsm9yw705ipf5vqw")))

(define-public crate-is-up-0.1 (crate (name "is-up") (vers "0.1.0") (hash "09p5mnsm1pwn5g3zc0fs66hzvn50kd8ns0bkgdkhvws1rpmklxlh")))

(define-public crate-is-up-0.1 (crate (name "is-up") (vers "0.1.1") (hash "0jx9dgy0mqy1499hr7g971k5d526z6l4h3bxhpp5jf8ldpd7b9l0")))

(define-public crate-is-up-0.1 (crate (name "is-up") (vers "0.1.2") (hash "18w8vba7gmayy6p7sdc9mapm5bsxg08gvsr083vmlcjdhcbh9dvv")))

(define-public crate-is-url-1 (crate (name "is-url") (vers "1.0.0") (deps (list (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)))) (hash "15x0cbal8rr6i0bjm5j69qg273cflqndxcclkbdvy4vc4m1i4h0r")))

(define-public crate-is-url-1 (crate (name "is-url") (vers "1.0.1") (deps (list (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)))) (hash "01j43kpmk76cdlnifgjr6qpyd9la569i7dkp600l8ans0qllynjy")))

(define-public crate-is-url-1 (crate (name "is-url") (vers "1.0.2") (deps (list (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)))) (hash "0cxbkl3fac122vv0vrp59fx5jkikxqj904aind3hngpjk0wj2xsw")))

(define-public crate-is-url-1 (crate (name "is-url") (vers "1.0.3") (deps (list (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)))) (hash "1fqhy7zx7r28asqz0wc2m0p0pvk9qvw7hjaang8hcs5ns4mxblwc")))

(define-public crate-is-url-1 (crate (name "is-url") (vers "1.0.4") (deps (list (crate-dep (name "lazy_static") (req "^1.4.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)))) (hash "0sqn9l2jdprd4d8mcs2zyl3qrv5wxykvf28l4z7nvm8lyqfw3hzc")))

