(define-module (crates-io is th) #:use-module (crates-io))

(define-public crate-istherepie-hello_world-0.1 (crate (name "istherepie-hello_world") (vers "0.1.0") (hash "1nkgndf19kpchdw8j1pda2ailr1xqjhd54hagql1sa25gy8z8r5v") (yanked #t)))

(define-public crate-istherepie-hello_world-0.0.1 (crate (name "istherepie-hello_world") (vers "0.0.1") (hash "0wlh87cm73ynpvcwj5xij8dm7frqk0rk3vhx96a4dhj61a4p9yma")))

