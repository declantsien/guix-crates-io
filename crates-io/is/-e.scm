(define-module (crates-io is -e) #:use-module (crates-io))

(define-public crate-is-even-1 (crate (name "is-even") (vers "1.0.0") (deps (list (crate-dep (name "is-odd") (req "^1.0.0") (default-features #t) (kind 0)))) (hash "1l42sf7g9bl3dpnpmpia144jxn0sh9hfm72d49n1xgwxrysmimc3")))

(define-public crate-is-even-or-odd-1 (crate (name "is-even-or-odd") (vers "1.0.0") (deps (list (crate-dep (name "is-even") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "is-odd") (req "^1") (default-features #t) (kind 0)))) (hash "1h8x4zd8xrklq25a67rplq6sid05px703v6dnk49yg81dd49dlx3")))

(define-public crate-is-even-u16-1 (crate (name "is-even-u16") (vers "1.0.0") (hash "1l9v6rxw9hlan5rbamm2wbs7aszfkg1qizds6zkhyig260fiinx3")))

(define-public crate-is-even-u16-1 (crate (name "is-even-u16") (vers "1.0.1") (hash "00y7xs98gml5x4gm9gv64rdqgr6l2rl43ww2v2jcypzaydrq3dnx")))

(define-public crate-is-even-u16-1 (crate (name "is-even-u16") (vers "1.0.2") (hash "1mdbwbsy51gmjlh46qb9zmygnn9qxr1asphvniqnn8l0h8mmq51v")))

