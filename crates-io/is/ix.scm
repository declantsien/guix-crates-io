(define-module (crates-io is ix) #:use-module (crates-io))

(define-public crate-isixhosa-0.1 (crate (name "isixhosa") (vers "0.1.0") (deps (list (crate-dep (name "num_enum") (req "^0.5.4") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "unicode-normalization") (req "^0.1.19") (default-features #t) (kind 0)))) (hash "1ba25r7kqpwny7073qlwb6iq5sw7sx0wz52m5q1x4kqwpp7qzs3y") (features (quote (("with-serde-1" "serde") ("with-num_enum-0_5" "num_enum"))))))

(define-public crate-isixhosa-0.1 (crate (name "isixhosa") (vers "0.1.1") (deps (list (crate-dep (name "num_enum") (req "^0.5.4") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "unicode-normalization") (req "^0.1.19") (default-features #t) (kind 0)))) (hash "1lgmxpzj612ap074q2pandsgqb9l3qadm8c8289xbc3zqwamcir0") (features (quote (("with-serde-1" "serde") ("with-num_enum-0_5" "num_enum"))))))

(define-public crate-isixhosa-0.2 (crate (name "isixhosa") (vers "0.2.0") (deps (list (crate-dep (name "num_enum") (req "^0.7.2") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "unicode-normalization") (req "^0.1.19") (default-features #t) (kind 0)))) (hash "0bzq49m3p1vq0nzsyp90xmzqp0y62nj1585m2ay36bdxkyh54hcx") (features (quote (("with-serde-1" "serde") ("with-num_enum-0_7" "num_enum"))))))

