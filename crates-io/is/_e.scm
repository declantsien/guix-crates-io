(define-module (crates-io is _e) #:use-module (crates-io))

(define-public crate-is_effected-0.0.1 (crate (name "is_effected") (vers "0.0.1") (hash "00mppm6h02dpcp1nij5dibmap85cs32zvriadpqkzlxjdnjz8kfn")))

(define-public crate-is_effected-0.1 (crate (name "is_effected") (vers "0.1.0") (deps (list (crate-dep (name "git2") (req "^0.13.20") (kind 0)) (crate-dep (name "log") (req "^0.4.14") (default-features #t) (kind 0)) (crate-dep (name "pretty_env_logger") (req "^0.4.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.5.4") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.3.21") (default-features #t) (kind 0)))) (hash "0w6qym3k96rqmq8qrqqqfdvwsp8m2x7gfi2x899bsvkk471vry4r")))

(define-public crate-is_effected-0.2 (crate (name "is_effected") (vers "0.2.0") (deps (list (crate-dep (name "git2") (req "^0.13.21") (kind 0)) (crate-dep (name "log") (req "^0.4.14") (default-features #t) (kind 0)) (crate-dep (name "pretty_env_logger") (req "^0.4.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.5.4") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.3.23") (default-features #t) (kind 0)))) (hash "0zxcj9fi6pjf6vaa302bldg13q3i58b42y5wbbn16pndxj8m0j4f")))

(define-public crate-is_effected-0.3 (crate (name "is_effected") (vers "0.3.0") (deps (list (crate-dep (name "git2") (req "^0.13.22") (kind 0)) (crate-dep (name "log") (req "^0.4.14") (default-features #t) (kind 0)) (crate-dep (name "pretty_env_logger") (req "^0.4.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.5.4") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0.3.23") (default-features #t) (kind 0)))) (hash "1hfd18ipdsfzd6vygrs398n2559f5lx5rxfr6l1gi3vjrxqk8m8z")))

(define-public crate-is_effected-0.4 (crate (name "is_effected") (vers "0.4.0") (deps (list (crate-dep (name "git2") (req "^0.13.22") (kind 0)) (crate-dep (name "log") (req "^0.4.14") (default-features #t) (kind 0)) (crate-dep (name "pretty_env_logger") (req "^0.4.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.5.4") (default-features #t) (kind 0)) (crate-dep (name "rstest") (req "^0.11.0") (default-features #t) (kind 2)) (crate-dep (name "structopt") (req "^0.3.23") (default-features #t) (kind 0)))) (hash "16pkj22g9nrcd8bikchf40xmk4blfqpa6hhfd7ygs5hgykkyn84g")))

(define-public crate-is_effected-0.4 (crate (name "is_effected") (vers "0.4.1") (deps (list (crate-dep (name "git2") (req "^0.13.22") (kind 0)) (crate-dep (name "log") (req "^0.4.14") (default-features #t) (kind 0)) (crate-dep (name "pretty_env_logger") (req "^0.4.0") (default-features #t) (kind 0)) (crate-dep (name "regex") (req "^1.5.4") (default-features #t) (kind 0)) (crate-dep (name "rstest") (req "^0.11.0") (default-features #t) (kind 2)) (crate-dep (name "structopt") (req "^0.3.23") (default-features #t) (kind 0)))) (hash "1avf1nmpv2xx4wdh66a1bl4wjvgx75vnsiyvflq7lf7q87h4ip7g")))

(define-public crate-is_elevated-0.1 (crate (name "is_elevated") (vers "0.1.0") (deps (list (crate-dep (name "winapi") (req "^0.3.6") (features (quote ("securitybaseapi" "processthreadsapi"))) (default-features #t) (kind 0)))) (hash "1chadmqzmgwancpz1hc2643dakgpxvyfpnh95h2x74sghmizlx78")))

(define-public crate-is_elevated-0.1 (crate (name "is_elevated") (vers "0.1.1") (deps (list (crate-dep (name "winapi") (req "^0.3.6") (features (quote ("securitybaseapi" "processthreadsapi"))) (default-features #t) (kind 0)))) (hash "1qn0vsvzi96r3i95ang83dmrk38cjbmpvwqk4bipi804qlvppffp")))

(define-public crate-is_elevated-0.1 (crate (name "is_elevated") (vers "0.1.2") (deps (list (crate-dep (name "winapi") (req "^0.3.6") (features (quote ("securitybaseapi" "processthreadsapi"))) (default-features #t) (kind 0)))) (hash "1dgr8azqq0d5xfyjrv8pjzyz914vmljrbjsx064ffqyvyl7hd6aj")))

(define-public crate-is_empty-0.1 (crate (name "is_empty") (vers "0.1.0") (deps (list (crate-dep (name "is_empty_derive") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.152") (features (quote ("derive"))) (default-features #t) (kind 2)) (crate-dep (name "serde_json") (req "^1.0.91") (default-features #t) (kind 2)))) (hash "0znpyr5cs965vnx4x839mxgysvjnglh6f2p7fz6pw3r550arjm6h")))

(define-public crate-is_empty-0.2 (crate (name "is_empty") (vers "0.2.0") (deps (list (crate-dep (name "is_empty_derive") (req "^0.1.0") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.152") (features (quote ("derive"))) (default-features #t) (kind 2)) (crate-dep (name "serde_json") (req "^1.0.91") (default-features #t) (kind 2)))) (hash "08a0vws47i7fvj0lb6chkihr0795nf9mapc7553qjlznnxa634qa")))

(define-public crate-is_empty_derive-0.1 (crate (name "is_empty_derive") (vers "0.1.0") (deps (list (crate-dep (name "proc-macro2") (req "^1.0.50") (default-features #t) (kind 0)) (crate-dep (name "quote") (req "^1.0.23") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.107") (default-features #t) (kind 0)) (crate-dep (name "syn") (req "^1.0.107") (features (quote ("extra-traits"))) (default-features #t) (kind 2)))) (hash "040n9xhaah88yi3n4qb7dzjg1x01j39avbdnghp8izpgxh4k146q")))

(define-public crate-is_even_odd-0.1 (crate (name "is_even_odd") (vers "0.1.0") (hash "0jsxqmm3hrcm6n6lz675rymlgi43igicby5bbg5p5kh9qiy1zx6k") (yanked #t)))

(define-public crate-is_even_odd-0.1 (crate (name "is_even_odd") (vers "0.1.1") (hash "0xmqnb5zsk294cpaq28qh05b0cg69yq859fjv20jjinqc824g15v")))

(define-public crate-is_executable-0.1 (crate (name "is_executable") (vers "0.1.0") (deps (list (crate-dep (name "diff") (req "^0.1.10") (default-features #t) (kind 2)))) (hash "1a754crfyc5987kv4fp2382zmvgdq1v2n4cwdv2a3fi7nksn0rf1")))

(define-public crate-is_executable-0.1 (crate (name "is_executable") (vers "0.1.2") (deps (list (crate-dep (name "diff") (req "^0.1.10") (default-features #t) (kind 2)) (crate-dep (name "winapi") (req "^0.3") (features (quote ("winbase"))) (default-features #t) (target "cfg(target_os = \"windows\")") (kind 0)))) (hash "0xy516afjh79a0d53j9v4w5mgi2s0r6f6qynnyz8g0dwi8xmab9h")))

(define-public crate-is_executable-1 (crate (name "is_executable") (vers "1.0.0") (deps (list (crate-dep (name "diff") (req "^0.1.10") (default-features #t) (kind 2)) (crate-dep (name "winapi") (req "^0.3") (features (quote ("winbase"))) (default-features #t) (target "cfg(target_os = \"windows\")") (kind 0)))) (hash "0vchfjjgq2913bnyddwk61vnv4zgissjqpp5jlcx1vg6si8s09hl")))

(define-public crate-is_executable-1 (crate (name "is_executable") (vers "1.0.1") (deps (list (crate-dep (name "diff") (req "^0.1.10") (default-features #t) (kind 2)) (crate-dep (name "winapi") (req "^0.3") (features (quote ("winbase"))) (default-features #t) (target "cfg(target_os = \"windows\")") (kind 0)))) (hash "1j59iqaxcgax0qll30rarpcr7y3dpkl38iv4mlkfcxbvsv3cv6ps")))

