(define-module (crates-io is om) #:use-module (crates-io))

(define-public crate-isomagic-0.1 (crate (name "isomagic") (vers "0.1.0") (deps (list (crate-dep (name "dot_vox") (req "^3") (default-features #t) (kind 0)) (crate-dep (name "error-chain") (req "^0") (default-features #t) (kind 0)) (crate-dep (name "image") (req "^0") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0") (default-features #t) (kind 0)) (crate-dep (name "structopt-derive") (req "^0") (default-features #t) (kind 0)))) (hash "0ryfgxwjv422c6ziqymmbjjmz07gp68am7j0rhsfdm3qpn8wjmiq")))

(define-public crate-isomagic-0.1 (crate (name "isomagic") (vers "0.1.1") (deps (list (crate-dep (name "dot_vox") (req "^3") (default-features #t) (kind 0)) (crate-dep (name "error-chain") (req "^0") (default-features #t) (kind 0)) (crate-dep (name "image") (req "^0") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0") (default-features #t) (kind 0)) (crate-dep (name "structopt-derive") (req "^0") (default-features #t) (kind 0)))) (hash "0c7gk25722qhsm0c9a7l385r7m4ncl5zb57b0d9xq10xazwydgvk")))

(define-public crate-isomagic-0.1 (crate (name "isomagic") (vers "0.1.2") (deps (list (crate-dep (name "dot_vox") (req "^3") (default-features #t) (kind 0)) (crate-dep (name "error-chain") (req "^0") (default-features #t) (kind 0)) (crate-dep (name "image") (req "^0") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0") (default-features #t) (kind 0)) (crate-dep (name "structopt-derive") (req "^0") (default-features #t) (kind 0)))) (hash "0jpb3mhbiay2dk299c0827a5x8xy26h16a8nwl3ab2i7z95yhg8r")))

(define-public crate-isomagic-0.1 (crate (name "isomagic") (vers "0.1.3") (deps (list (crate-dep (name "dot_vox") (req "^3") (default-features #t) (kind 0)) (crate-dep (name "error-chain") (req "^0") (default-features #t) (kind 0)) (crate-dep (name "image") (req "^0") (default-features #t) (kind 0)) (crate-dep (name "structopt") (req "^0") (default-features #t) (kind 0)) (crate-dep (name "structopt-derive") (req "^0") (default-features #t) (kind 0)))) (hash "01z4ifi133sfrw9kydf6b47glq9j418ds0s7s9a0n6fs97wh66rg")))

(define-public crate-isomdl-0.1 (crate (name "isomdl") (vers "0.1.0") (hash "0805axr34zv0pk4akih78pi1qvcsq88wvkirvrnw87w0dzyjrxd0")))

(define-public crate-isomdoc-0.0.0 (crate (name "isomdoc") (vers "0.0.0") (hash "1d80ijs7f9lhzzgnyc0zr3m3admyx4d3nnh8lgh3z4cxqp5gkrjb")))

(define-public crate-isomer-0.0.0 (crate (name "isomer") (vers "0.0.0") (deps (list (crate-dep (name "crossbeam-skiplist") (req "^0.1.1") (default-features #t) (kind 0)) (crate-dep (name "dashu") (req "^0.3.1") (default-features #t) (kind 0)) (crate-dep (name "num-traits") (req "^0.2.15") (default-features #t) (kind 0)))) (hash "1ix88096piybfja5mim580m7h77bgfi9y8lpq1n44rw4mlaimmba") (features (quote (("default"))))))

(define-public crate-isomorphism-0.1 (crate (name "isomorphism") (vers "0.1.0") (deps (list (crate-dep (name "quickcheck") (req "^0.6.0") (default-features #t) (kind 2)) (crate-dep (name "serde") (req "^1.0.42") (optional #t) (default-features #t) (kind 0)))) (hash "1dfz24071zm3hk435cp78mqxxfdjra62g8b1a1shrynanyzc6qyb")))

(define-public crate-isomorphism-0.1 (crate (name "isomorphism") (vers "0.1.1") (deps (list (crate-dep (name "quickcheck") (req "^0.6.0") (default-features #t) (kind 2)) (crate-dep (name "serde") (req "^1.0.42") (optional #t) (default-features #t) (kind 0)))) (hash "0gg08wa993n0vf2z2qfh2wzpfpv0g6v3361c4swfz2frf2gxpsnv")))

(define-public crate-isomorphism-0.1 (crate (name "isomorphism") (vers "0.1.2") (deps (list (crate-dep (name "quickcheck") (req "^0.6.0") (default-features #t) (kind 2)) (crate-dep (name "serde") (req "^1.0.42") (optional #t) (default-features #t) (kind 0)))) (hash "086zhr60n89h0163gxxd03pwbmzgq4jq8rq8035bj5fvmaf2bf2x")))

(define-public crate-isomorphism-0.1 (crate (name "isomorphism") (vers "0.1.3") (deps (list (crate-dep (name "quickcheck") (req "^0.6.0") (default-features #t) (kind 2)) (crate-dep (name "serde") (req "^1.0.42") (optional #t) (default-features #t) (kind 0)))) (hash "1sl8ap8rfr0fzyrf13m4yxk1hr9jkzf4y48lqia2qv5zm88mipkz")))

