(define-module (crates-io is to) #:use-module (crates-io))

(define-public crate-istor-0.1 (crate (name "istor") (vers "0.1.0") (deps (list (crate-dep (name "clap") (req "^3.0.9") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "colored") (req "^2.0.0") (default-features #t) (kind 0)) (crate-dep (name "ipaddress") (req "^0.1.1") (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.11") (features (quote ("blocking" "json"))) (default-features #t) (kind 0)))) (hash "0hppis179c8ll4yfanhq0g9lh468ci1cq5sbqp9a46bn06nyjf62")))

(define-public crate-istor-0.1 (crate (name "istor") (vers "0.1.1") (deps (list (crate-dep (name "clap") (req "^3.0.9") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "colored") (req "^2.0.0") (default-features #t) (kind 0)) (crate-dep (name "ipaddress") (req "^0.1.1") (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.11") (features (quote ("blocking" "json"))) (default-features #t) (kind 0)))) (hash "03lwxh4m84xp7z22gh7sh3x9dc75fbb39rs37x10m54ds339r3f2")))

(define-public crate-istor-0.1 (crate (name "istor") (vers "0.1.2") (deps (list (crate-dep (name "clap") (req "^3.0.9") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "colored") (req "^2.0.0") (default-features #t) (kind 0)) (crate-dep (name "ipaddress") (req "^0.1.1") (default-features #t) (kind 0)) (crate-dep (name "reqwest") (req "^0.11") (features (quote ("blocking" "json"))) (default-features #t) (kind 0)))) (hash "0yc44szv09c5ynbpwfvws0csfzvmi81w7dc2g2w6xvwz6wbz4grb")))

(define-public crate-istor-0.2 (crate (name "istor") (vers "0.2.0") (deps (list (crate-dep (name "clap") (req "^3.0.9") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "ureq") (req "^2.4.0") (default-features #t) (kind 0)))) (hash "16w2klm7pcmrl9kp3pg5ns3vyq87gw98zh8pk6f13vx4hgxdhmkk")))

(define-public crate-istor-0.2 (crate (name "istor") (vers "0.2.1") (deps (list (crate-dep (name "minparse") (req "^0.1.2") (default-features #t) (kind 0)) (crate-dep (name "ureq") (req "^2.4.0") (default-features #t) (kind 0)))) (hash "0452217wh6jzzws7zl4zk23jch4zcx09bdkgj21ghrpgkzid5ldp")))

(define-public crate-istos-0.0.1 (crate (name "istos") (vers "0.0.1") (hash "0rginb30dy7w4nkjy2hnsc84lm39gldm9nwh7mb4281j6aqw6k5h")))

(define-public crate-istos-0.0.2 (crate (name "istos") (vers "0.0.2") (hash "1ihspfvcxwi416zjy7s6zix0mgm7aapf5gfi4kf1xyrvk12yprxw")))

(define-public crate-istos-0.0.3 (crate (name "istos") (vers "0.0.3") (hash "011r7q1j435dp8ycc8x0f90jphg0ysyg1p87nd4861n7pkqab7zb")))

