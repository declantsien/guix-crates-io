(define-module (crates-io is _i) #:use-module (crates-io))

(define-public crate-is_i32-0.1 (crate (name "is_i32") (vers "0.1.0") (hash "1970iz53sw454fcp4141wgcqjp3szck7aqk0cakl3690r9gillhk")))

(define-public crate-is_i32-0.1 (crate (name "is_i32") (vers "0.1.1") (hash "017a5lgx8lhax8a6hds3hvj1qvbz3yfdrj30hcg3mgiyscvdv9qq")))

(define-public crate-is_in-0.1 (crate (name "is_in") (vers "0.1.0") (hash "0226wrlppav3pw249viyz4sx2z3v5iblwcmdz4kk0ilpnfnv55hg")))

(define-public crate-is_in-0.1 (crate (name "is_in") (vers "0.1.1") (hash "1l127vywgvflzfvnfyjyv52xs9wxmpbqa2ijxyi5p0jg2rd6qj0j")))

(define-public crate-is_in-1 (crate (name "is_in") (vers "1.0.0") (hash "17ih5v7747q29kv8z4crbhgbdipi2p48r9yfp6sbp3cc34jb33ba")))

