(define-module (crates-io k8 -d) #:use-module (crates-io))

(define-public crate-k8-diff-0.1 (crate (name "k8-diff") (vers "0.1.0") (deps (list (crate-dep (name "log") (req "^0.4.8") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.103") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.40") (default-features #t) (kind 0)))) (hash "0v9s4srdlbq36wcr3189lik4ym3zqjsjznbip91s886gc8p97g3w")))

(define-public crate-k8-diff-0.1 (crate (name "k8-diff") (vers "0.1.1") (deps (list (crate-dep (name "serde") (req "^1.0.103") (default-features #t) (kind 0)) (crate-dep (name "serde_json") (req "^1.0.40") (default-features #t) (kind 0)) (crate-dep (name "tracing") (req "^0.1.19") (default-features #t) (kind 0)))) (hash "09qxxmwp8j22kb1nv4n529cfdzvqdphs7wyfqla8j91x6xp5rfpl")))

(define-public crate-k8-diff-0.1 (crate (name "k8-diff") (vers "0.1.2") (deps (list (crate-dep (name "serde") (req "^1.0.118") (default-features #t) (kind 0)) (crate-dep (name "serde") (req "^1.0.118") (features (quote ("derive"))) (default-features #t) (kind 2)) (crate-dep (name "serde_json") (req "^1.0.60") (default-features #t) (kind 0)) (crate-dep (name "tracing") (req "^0.1.22") (default-features #t) (kind 0)))) (hash "0fsgpp3q2ninykgz2brd7lw1y7v9jz2d64dfswlq2i7b5s8mshzg")))

