(define-module (crates-io k8 #{05}#) #:use-module (crates-io))

(define-public crate-k8055-0.0.1 (crate (name "k8055") (vers "0.0.1") (deps (list (crate-dep (name "usb") (req "~0.1.0") (default-features #t) (kind 0)))) (hash "1c8mxc5frb1cllki9kmdmbwsmqp7p3jhn28k54ny8d1y4na7pmva")))

(define-public crate-k8055-0.0.2 (crate (name "k8055") (vers "0.0.2") (deps (list (crate-dep (name "usb") (req "~0.1.0") (default-features #t) (kind 0)))) (hash "144sb2pvzwn8y0bszmr2dzq9ddp9sxa2x1d4wmz80lp4q5zv44xc")))

(define-public crate-k8055-0.0.3 (crate (name "k8055") (vers "0.0.3") (deps (list (crate-dep (name "usb") (req "~0.1.0") (default-features #t) (kind 0)))) (hash "13aq48zab1b5rwlz0wahrizpgpv343d1sq9xbdbl0b3wk236py99")))

(define-public crate-k8055-0.0.4 (crate (name "k8055") (vers "0.0.4") (deps (list (crate-dep (name "usb") (req "~0.1.0") (default-features #t) (kind 0)))) (hash "18vm0svv5wjlrw3l8nd0mfjasm9mwagwfzw9nma3grdnvmpkdp3i")))

(define-public crate-k8055-0.0.5 (crate (name "k8055") (vers "0.0.5") (deps (list (crate-dep (name "rustc-serialize") (req "~0.2.7") (default-features #t) (kind 0)) (crate-dep (name "usb") (req "~0.1.0") (default-features #t) (kind 0)))) (hash "16xf3x00i9hadblx24mignk2spwaf3jm2dmi9sqf5brxxxav0wlw")))

(define-public crate-k8055-0.0.6 (crate (name "k8055") (vers "0.0.6") (deps (list (crate-dep (name "rustc-serialize") (req "~0.2.7") (default-features #t) (kind 0)) (crate-dep (name "usb") (req "~0.1.0") (default-features #t) (kind 0)))) (hash "1xplz6csk4dgg7w8vkdhfny74br83swashdfvwpb2x1gwqv6qihg")))

(define-public crate-k8055-0.1 (crate (name "k8055") (vers "0.1.0") (deps (list (crate-dep (name "bitflags") (req "~0.3") (default-features #t) (kind 0)) (crate-dep (name "error-chain") (req "~0.7") (default-features #t) (kind 0)) (crate-dep (name "libusb") (req "~0.3") (default-features #t) (kind 0)) (crate-dep (name "rustc-serialize") (req "~0.3") (default-features #t) (kind 0)))) (hash "0c5l1ap2ac4865m5kwmnfd4pfx4nvzyf98xsssxdld1y1i4q0shj")))

(define-public crate-k8055-0.2 (crate (name "k8055") (vers "0.2.0") (deps (list (crate-dep (name "bitflags") (req "^1.0.1") (default-features #t) (kind 0)) (crate-dep (name "error-chain") (req "^0.11.0") (default-features #t) (kind 0)) (crate-dep (name "libusb") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "rustc-serialize") (req "^0.3.24") (default-features #t) (kind 0)))) (hash "0mjwibi7kcg5qfvw26cjczgc55wr7024ydwshqm2zzc3ammsvb0h")))

(define-public crate-k8055-0.2 (crate (name "k8055") (vers "0.2.1") (deps (list (crate-dep (name "bitflags") (req "^1.1") (default-features #t) (kind 0)) (crate-dep (name "error-chain") (req "^0.12") (default-features #t) (kind 0)) (crate-dep (name "libusb") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "rustc-serialize") (req "^0.3.24") (default-features #t) (kind 0)))) (hash "10id7b494j801lb683lkj75jafckxjk1l7nnp8mj9bvp4b0c0x5i")))

(define-public crate-k8055-0.2 (crate (name "k8055") (vers "0.2.3") (deps (list (crate-dep (name "bitflags") (req "^1.1") (default-features #t) (kind 0)) (crate-dep (name "error-chain") (req "^0.12") (default-features #t) (kind 0)) (crate-dep (name "libusb") (req "^0.3.0") (default-features #t) (kind 0)) (crate-dep (name "rustc-serialize") (req "^0.3.24") (default-features #t) (kind 0)))) (hash "1v6ypsh0q7fznp60r2b9lsxwva4j3rxh24gsmzph43lmyr40zg5m")))

(define-public crate-k8055_rs-0.1 (crate (name "k8055_rs") (vers "0.1.0") (deps (list (crate-dep (name "serialport") (req "^4.0") (default-features #t) (kind 0)))) (hash "00kpxnj326bmcw0d1r4m14nrz6h1qmpwaqq5qmnvkhh5rcirsck1")))

(define-public crate-k8056-0.1 (crate (name "k8056") (vers "0.1.0") (hash "11z3fbydjg8jgwki6xfkymivq4hgi8pd3dvsqsfd0zg76qcqw5x0")))

