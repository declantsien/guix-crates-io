(define-module (crates-io k8 s_) #:use-module (crates-io))

(define-public crate-k8s_quantity_parser-0.0.1 (crate (name "k8s_quantity_parser") (vers "0.0.1") (deps (list (crate-dep (name "eyre") (req "^0.6") (default-features #t) (kind 0)) (crate-dep (name "k8s-openapi") (req "^0.14") (features (quote ("v1_22"))) (kind 0)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)))) (hash "1skxh2r88wlaippi4325injsgm66yhs9mbhqwvxfxiynvl1fgnql")))

(define-public crate-k8s_quantity_parser-0.0.2 (crate (name "k8s_quantity_parser") (vers "0.0.2") (deps (list (crate-dep (name "eyre") (req "^0.6") (default-features #t) (kind 0)) (crate-dep (name "k8s-openapi") (req "^0.14") (features (quote ("v1_22"))) (kind 0)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)))) (hash "1xr1n25zpa8p27bicalqw4ipyhwsyrjdnw5ajn7hb18r3dw6yyf8")))

(define-public crate-k8s_quantity_parser-0.1 (crate (name "k8s_quantity_parser") (vers "0.1.0") (deps (list (crate-dep (name "eyre") (req "^0.6") (default-features #t) (kind 0)) (crate-dep (name "k8s-openapi") (req "^0.14") (features (quote ("v1_22"))) (kind 0)) (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)))) (hash "0gp6amidvnkw0757v1ngbq89jldrjym3zazz0r5qs0qzhi4q3f0v")))

