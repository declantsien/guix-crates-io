(define-module (crates-io fa vy) #:use-module (crates-io))

(define-public crate-favy-0.1 (crate (name "favy") (vers "0.1.0-beta.0") (deps (list (crate-dep (name "ikon") (req "^0.1.0-beta.1") (default-features #t) (kind 0)))) (hash "1q7v6gsw19z92nbwl82rjwr05n5syi64mwshxhfhkiphd93f6jf4") (yanked #t)))

(define-public crate-favy-0.1 (crate (name "favy") (vers "0.1.0-beta.1") (deps (list (crate-dep (name "ikon") (req "^0.1.0-beta.2") (default-features #t) (kind 0)))) (hash "0ldf3z18690wymyy755nx4b3h4vs4ic00ld1gnq3idr5fab7x8w6") (yanked #t)))

(define-public crate-favy-0.1 (crate (name "favy") (vers "0.1.0-beta.2") (deps (list (crate-dep (name "ikon") (req "^0.1.0-beta.3") (default-features #t) (kind 0)))) (hash "1jm0r0vzcq7g72rdvdjflx87bapz8ah0rj3lpngj6wrb13ayyi81") (yanked #t)))

(define-public crate-favy-0.1 (crate (name "favy") (vers "0.1.0-beta.3") (deps (list (crate-dep (name "ikon") (req "^0.1.0-beta.4") (default-features #t) (kind 0)))) (hash "0418c0775q93a341ch6yycw0sb5fsznk2yp161q0048pd5sfv8ll") (yanked #t)))

