(define-module (crates-io fa is) #:use-module (crates-io))

(define-public crate-faiss-0.1 (crate (name "faiss") (vers "0.1.0") (deps (list (crate-dep (name "faiss-sys") (req "^0.1.0") (default-features #t) (kind 0)))) (hash "0hj2b1ixw2ww4xf5hqk5kkbn9q7nxhsmfhk23x3hgpi2r7j84cyd") (features (quote (("gpu" "faiss-sys/gpu"))))))

(define-public crate-faiss-0.2 (crate (name "faiss") (vers "0.2.0") (deps (list (crate-dep (name "faiss-sys") (req "^0.2.0") (default-features #t) (kind 0)))) (hash "0l9kj17jzkdd1d4cbadw7gaama8j0xg5nl7680h83x83sna3wn83") (features (quote (("gpu" "faiss-sys/gpu"))))))

(define-public crate-faiss-0.3 (crate (name "faiss") (vers "0.3.0") (deps (list (crate-dep (name "faiss-sys") (req "^0.2.0") (default-features #t) (kind 0)))) (hash "05qx6xj2cky031m84kxfybsy9adcja97z8jz1rdc8srz10yyd57s") (features (quote (("gpu" "faiss-sys/gpu"))))))

(define-public crate-faiss-0.4 (crate (name "faiss") (vers "0.4.0") (deps (list (crate-dep (name "faiss-sys") (req "^0.2.0") (default-features #t) (kind 0)))) (hash "0qq41rvm5hbza5kvzgx9d1arpp7mhygvh08a4sdcmxnx7mq65l5w") (features (quote (("gpu" "faiss-sys/gpu"))))))

(define-public crate-faiss-0.5 (crate (name "faiss") (vers "0.5.0") (deps (list (crate-dep (name "faiss-sys") (req "^0.3.0") (default-features #t) (kind 0)))) (hash "1pm0kd1aiy80mjc9g5y3didpabc5321pp7jl19jj387b7hrks1xr") (features (quote (("gpu" "faiss-sys/gpu")))) (yanked #t)))

(define-public crate-faiss-0.5 (crate (name "faiss") (vers "0.5.1") (deps (list (crate-dep (name "faiss-sys") (req "^0.3.0") (default-features #t) (kind 0)))) (hash "19gf342ma0icjkpryazb0hm6f91fdzijcxxk1z17m8qz7jgxnlh9") (features (quote (("gpu" "faiss-sys/gpu"))))))

(define-public crate-faiss-0.6 (crate (name "faiss") (vers "0.6.0") (deps (list (crate-dep (name "faiss-sys") (req "^0.3.0") (default-features #t) (kind 0)))) (hash "095xk22vmvjnfyanq9cjb8vim22x3f7g9cn486xc1ydnn7i41jxb") (features (quote (("gpu" "faiss-sys/gpu"))))))

(define-public crate-faiss-0.7 (crate (name "faiss") (vers "0.7.0") (deps (list (crate-dep (name "faiss-sys") (req "^0.3.0") (default-features #t) (kind 0)))) (hash "0jgmhqsn6mg5dabdmfk6ywhbgmszmjpan45rvk698l0m4gy84jdx") (features (quote (("gpu" "faiss-sys/gpu"))))))

(define-public crate-faiss-0.8 (crate (name "faiss") (vers "0.8.0") (deps (list (crate-dep (name "faiss-sys") (req "^0.3.0") (default-features #t) (kind 0)))) (hash "187aj98hw62za3vrxf8nj2x5ch3ik6hm5aliyly3pbhyfsyhhsi6") (features (quote (("gpu" "faiss-sys/gpu"))))))

(define-public crate-faiss-0.9 (crate (name "faiss") (vers "0.9.0") (deps (list (crate-dep (name "faiss-sys") (req "^0.4.0") (default-features #t) (kind 0)))) (hash "0fh7ss1r4w0zi34ky59ymzh9smcdy51ap1isqc3i6wxxn18jkly6") (features (quote (("gpu" "faiss-sys/gpu"))))))

(define-public crate-faiss-0.10 (crate (name "faiss") (vers "0.10.0") (deps (list (crate-dep (name "faiss-sys") (req "^0.5.0") (default-features #t) (kind 0)))) (hash "14ni2qqpxkq68w34f5y0gffd7f7qfsbbmw4j3sgnrcqx2xbilg30") (features (quote (("gpu" "faiss-sys/gpu"))))))

(define-public crate-faiss-0.11 (crate (name "faiss") (vers "0.11.0") (deps (list (crate-dep (name "faiss-sys") (req "^0.6.0") (default-features #t) (kind 0)))) (hash "02pnrlpqsj43pc6zky1w36w7r0llwbx9n4xakbqxc0p3892jr841") (features (quote (("gpu" "faiss-sys/gpu"))))))

(define-public crate-faiss-0.12 (crate (name "faiss") (vers "0.12.0") (deps (list (crate-dep (name "faiss-sys") (req "^0.6.1") (default-features #t) (kind 0)))) (hash "1hsib1x57qcyia30b4digy7l2kyk4hfrdahj4fvr8hy8p51kmn2y") (features (quote (("gpu" "faiss-sys/gpu"))))))

(define-public crate-faiss-0.12 (crate (name "faiss") (vers "0.12.1") (deps (list (crate-dep (name "faiss-sys") (req "^0.6.2") (default-features #t) (kind 0)))) (hash "0cwpvp92ghp57yla96s3h39ww20fw49ivaih1a5h51i78d4f1zxk") (features (quote (("static" "faiss-sys/static") ("gpu" "faiss-sys/gpu"))))))

(define-public crate-faiss-next-0.1 (crate (name "faiss-next") (vers "0.1.0") (deps (list (crate-dep (name "bindgen") (req "^0.69.2") (default-features #t) (kind 1)) (crate-dep (name "cfg-if") (req "^1.0.0") (default-features #t) (kind 1)) (crate-dep (name "ndarray") (req "^0.15.6") (default-features #t) (kind 2)) (crate-dep (name "ndarray-rand") (req "^0.14.0") (default-features #t) (kind 2)) (crate-dep (name "thiserror") (req "^1.0.56") (default-features #t) (kind 0)) (crate-dep (name "tracing") (req "^0.1.40") (default-features #t) (kind 0)) (crate-dep (name "tracing-subscriber") (req "^0.3.18") (default-features #t) (kind 2)))) (hash "130axyv6g9sma10z7iff7p9amcamqz7da32yx3mr4cflxlxf7pd1") (features (quote (("gpu") ("default"))))))

(define-public crate-faiss-next-0.1 (crate (name "faiss-next") (vers "0.1.1") (deps (list (crate-dep (name "faiss-next-sys") (req "^0.1.1") (default-features #t) (kind 0)) (crate-dep (name "ndarray") (req "^0.15.6") (default-features #t) (kind 2)) (crate-dep (name "ndarray-rand") (req "^0.14.0") (default-features #t) (kind 2)) (crate-dep (name "thiserror") (req "^1.0.56") (default-features #t) (kind 0)) (crate-dep (name "tracing") (req "^0.1.40") (default-features #t) (kind 0)) (crate-dep (name "tracing-subscriber") (req "^0.3.18") (default-features #t) (kind 2)))) (hash "0xri94nri31ksrp7dkxx00v3cyarn5d8k16jd9ky04jbzzzrk1j6") (features (quote (("gpu" "faiss-next-sys/gpu") ("default"))))))

(define-public crate-faiss-next-0.1 (crate (name "faiss-next") (vers "0.1.2") (deps (list (crate-dep (name "faiss-next-sys") (req "^0.1.1") (default-features #t) (kind 0)) (crate-dep (name "ndarray") (req "^0.15.6") (default-features #t) (kind 2)) (crate-dep (name "ndarray-rand") (req "^0.14.0") (default-features #t) (kind 2)) (crate-dep (name "thiserror") (req "^1.0.56") (default-features #t) (kind 0)) (crate-dep (name "tracing") (req "^0.1.40") (default-features #t) (kind 0)) (crate-dep (name "tracing-subscriber") (req "^0.3.18") (default-features #t) (kind 2)))) (hash "1i73w2p4pcymn701cdwk6xnrwmgfzmf7bpsb1wpls8v8xn4nbap6") (features (quote (("gpu" "faiss-next-sys/gpu") ("default"))))))

(define-public crate-faiss-next-0.1 (crate (name "faiss-next") (vers "0.1.3") (deps (list (crate-dep (name "faiss-next-sys") (req "^0.1.1") (default-features #t) (kind 0)) (crate-dep (name "ndarray") (req "^0.15.6") (default-features #t) (kind 2)) (crate-dep (name "ndarray-rand") (req "^0.14.0") (default-features #t) (kind 2)) (crate-dep (name "thiserror") (req "^1.0.56") (default-features #t) (kind 0)) (crate-dep (name "tracing") (req "^0.1.40") (default-features #t) (kind 0)) (crate-dep (name "tracing-subscriber") (req "^0.3.18") (default-features #t) (kind 2)))) (hash "0yfsghj35alp4cnhxfapk8g8kg1pk01b6ywf6xcz2iw20s0rv678") (features (quote (("gpu" "faiss-next-sys/gpu") ("default"))))))

(define-public crate-faiss-next-0.2 (crate (name "faiss-next") (vers "0.2.0") (deps (list (crate-dep (name "faiss-next-sys") (req "^0.2.0") (default-features #t) (kind 0)) (crate-dep (name "ndarray") (req "^0.15.6") (default-features #t) (kind 2)) (crate-dep (name "ndarray-rand") (req "^0.14.0") (default-features #t) (kind 2)) (crate-dep (name "thiserror") (req "^1.0.56") (default-features #t) (kind 0)) (crate-dep (name "tracing") (req "^0.1.40") (default-features #t) (kind 0)) (crate-dep (name "tracing-subscriber") (req "^0.3.18") (default-features #t) (kind 2)))) (hash "12argh1dbj0zfdbsas2skfj8scq01f4m6wzbq7shywv2grsj966b") (features (quote (("gpu" "faiss-next-sys/gpu") ("default") ("bindgen" "faiss-next-sys/bindgen"))))))

(define-public crate-faiss-next-sys-0.1 (crate (name "faiss-next-sys") (vers "0.1.1") (deps (list (crate-dep (name "bindgen") (req "^0.69.2") (default-features #t) (kind 1)))) (hash "13sxk1hf1lbhnmpcp1hgq4r9bjd1v4pjip3q8g0w8wl1g6vlgfb0") (features (quote (("gpu") ("default") ("bindgen"))))))

(define-public crate-faiss-next-sys-0.2 (crate (name "faiss-next-sys") (vers "0.2.0") (deps (list (crate-dep (name "bindgen") (req "^0.69.2") (default-features #t) (kind 1)))) (hash "0lld11czsw4arr5cb9aa2madl8hs9zyh60k354bq2y9wccpb4mp5") (features (quote (("gpu") ("default") ("bindgen"))))))

(define-public crate-faiss-sys-0.1 (crate (name "faiss-sys") (vers "0.1.0") (hash "1m38h39pkg7zwg2j9pcnsvy337v2ybbc4p6wj141sfs787yjwbac") (features (quote (("gpu"))))))

(define-public crate-faiss-sys-0.2 (crate (name "faiss-sys") (vers "0.2.0") (hash "1cm3k85a9r3fsdgrlp9yzmz1bbnjlq60r4m61vcnvz1k1syaivn3") (features (quote (("gpu"))))))

(define-public crate-faiss-sys-0.3 (crate (name "faiss-sys") (vers "0.3.0") (hash "1ykziagly0bsyajxm2dffnaxd7dcdk9nphgq5xma1bisayvf5bp7") (features (quote (("gpu")))) (links "faiss_c")))

(define-public crate-faiss-sys-0.4 (crate (name "faiss-sys") (vers "0.4.0") (hash "128bp9yhs3fvb3f2apd0dj9kw5lcsm3gh0jjwxj3pj13cqnspsjn") (features (quote (("gpu")))) (links "faiss_c")))

(define-public crate-faiss-sys-0.5 (crate (name "faiss-sys") (vers "0.5.0") (hash "1bjd65bpj4w9llkl7sq6vm0ndgpbm7zij06qzdjsfh2dmki4y5a2") (features (quote (("gpu")))) (links "faiss_c")))

(define-public crate-faiss-sys-0.6 (crate (name "faiss-sys") (vers "0.6.0") (hash "1jnpj7q9yqinyjp5dd0ahmanf4ylrljm121w5bwh4v6hj7fy90fc") (features (quote (("gpu")))) (links "faiss_c")))

(define-public crate-faiss-sys-0.6 (crate (name "faiss-sys") (vers "0.6.1") (hash "1q56lsx87snadq7vcz0qk6lkl94ybrr01zq7z08ss8x6jvqdvhsl") (features (quote (("gpu")))) (links "faiss_c")))

(define-public crate-faiss-sys-0.6 (crate (name "faiss-sys") (vers "0.6.2") (deps (list (crate-dep (name "cmake") (req "^0.1.50") (optional #t) (default-features #t) (kind 1)))) (hash "0pli8qp12rszkisp8isv4kv2wp2skinj45vz6lsby8k4qn7h172b") (features (quote (("static" "cmake") ("gpu")))) (links "faiss_c")))

(define-public crate-faiss4rs-1 (crate (name "faiss4rs") (vers "1.6.301") (deps (list (crate-dep (name "cpp") (req "^0.5.4") (default-features #t) (kind 0)) (crate-dep (name "cpp_build") (req "^0.5.4") (default-features #t) (kind 1)) (crate-dep (name "rand") (req "^0.7.3") (default-features #t) (kind 2)))) (hash "0a1j3by1mpcsrpsn0pvx2nwi0skmlin06iccqk8x54hv9n5w7gvy")))

(define-public crate-faiss4rs-1 (crate (name "faiss4rs") (vers "1.6.302") (deps (list (crate-dep (name "cpp") (req "^0.5.4") (default-features #t) (kind 0)) (crate-dep (name "cpp_build") (req "^0.5.4") (default-features #t) (kind 1)) (crate-dep (name "rand") (req "^0.7.3") (default-features #t) (kind 2)))) (hash "0rw713in3jzb0gqa6g1ikq7aqsga66p5f1ssg3lppaf7zqwmf82d")))

(define-public crate-faiss4rs-1 (crate (name "faiss4rs") (vers "1.6.303") (deps (list (crate-dep (name "cpp") (req "^0.5.4") (default-features #t) (kind 0)) (crate-dep (name "cpp_build") (req "^0.5.4") (default-features #t) (kind 1)) (crate-dep (name "rand") (req "^0.7.3") (default-features #t) (kind 2)))) (hash "0n7fgmw5qnw9ai0yaaqmqix29gkh2h4mw6473q347as5sgacd1va")))

(define-public crate-faiss4rs-1 (crate (name "faiss4rs") (vers "1.6.304") (deps (list (crate-dep (name "cpp") (req "^0.5.4") (default-features #t) (kind 0)) (crate-dep (name "cpp_build") (req "^0.5.4") (default-features #t) (kind 1)) (crate-dep (name "rand") (req "^0.7.3") (default-features #t) (kind 2)))) (hash "0x2li0cvbk9fmcr9nrb6iihjq80vysy88hdx8k6gyy8yrxljkb8m")))

(define-public crate-faiss4rs-1 (crate (name "faiss4rs") (vers "1.6.305") (deps (list (crate-dep (name "cpp") (req "^0.5.4") (default-features #t) (kind 0)) (crate-dep (name "cpp_build") (req "^0.5.4") (default-features #t) (kind 1)) (crate-dep (name "rand") (req "^0.7.3") (default-features #t) (kind 2)))) (hash "0a5fqiwnfadyf6nryz87dfywr2ypgasawkk2141lhp7v52w0qi0h") (links "faiss")))

(define-public crate-faiss4rs-1 (crate (name "faiss4rs") (vers "1.6.306") (deps (list (crate-dep (name "cpp") (req "^0.5.4") (default-features #t) (kind 0)) (crate-dep (name "cpp_build") (req "^0.5.4") (default-features #t) (kind 1)) (crate-dep (name "rand") (req "^0.7.3") (default-features #t) (kind 2)))) (hash "0aa1fyrp2bvysnxaji75bz02rz4ri6rzgbmsh1ljbpg7wvf2lnxb") (links "faiss")))

(define-public crate-faiss4rs-1 (crate (name "faiss4rs") (vers "1.6.307") (deps (list (crate-dep (name "cpp") (req "^0.5.4") (default-features #t) (kind 0)) (crate-dep (name "cpp_build") (req "^0.5.4") (default-features #t) (kind 1)) (crate-dep (name "rand") (req "^0.7.3") (default-features #t) (kind 2)))) (hash "1hfq8nch49ggfscpaam063ccb7ngmdx2z37hy518x6568l6vjn6q") (links "faiss")))

