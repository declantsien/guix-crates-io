(define-module (crates-io fa sb) #:use-module (crates-io))

(define-public crate-fasb-0.1 (crate (name "fasb") (vers "0.1.0") (deps (list (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "rustyline") (req "^11.0.0") (default-features #t) (kind 0)) (crate-dep (name "savan") (req "^0.2.0") (default-features #t) (kind 0)))) (hash "02ymiapa0hx10w7jgwd1p2ln44gmbj7xil9kfkg51ik1vchd08qr") (features (quote (("verbose") ("interpreter"))))))

(define-public crate-fasb-0.1 (crate (name "fasb") (vers "0.1.1") (deps (list (crate-dep (name "regex") (req "^1") (default-features #t) (kind 0)) (crate-dep (name "rustyline") (req "^11.0.0") (default-features #t) (kind 0)) (crate-dep (name "savan") (req "^0.2.0") (default-features #t) (kind 0)))) (hash "059vj6kkyz5z7hk1wabk3kryflq5w2173msnynj0n5f3fxfrzy02") (features (quote (("verbose") ("interpreter"))))))

