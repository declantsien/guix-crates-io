(define-module (crates-io fa ht) #:use-module (crates-io))

(define-public crate-FahtocelCONV_RS-3 (crate (name "FahtocelCONV_RS") (vers "3.1.0") (hash "0zz8p3khildw9cgvsgzlgpajjc0b50zrhm5ppa67fbpvpjw37x14") (yanked #t)))

(define-public crate-fahtsex-0.1 (crate (name "fahtsex") (vers "0.1.0") (deps (list (crate-dep (name "hex") (req "^0.4.3") (default-features #t) (kind 2)))) (hash "0k5zn703x2jwg5rfnrwjfzwj7bvc337s60r4pbimw8403j7p7kdz")))

(define-public crate-fahtsex-0.1 (crate (name "fahtsex") (vers "0.1.1") (deps (list (crate-dep (name "hex") (req "^0.4.3") (default-features #t) (kind 2)))) (hash "0fyi6a4hv3hv2hn55f8479msx43hvs2c6ppdcw2a34ak8ncqwmkp")))

(define-public crate-fahtsex-0.1 (crate (name "fahtsex") (vers "0.1.2") (deps (list (crate-dep (name "hex") (req "^0.4.3") (default-features #t) (kind 2)))) (hash "0g7amr2c29k333006a5i6kcwqqabqb3k3i2h4y4fdnjp754zh603")))

(define-public crate-fahtsex-0.2 (crate (name "fahtsex") (vers "0.2.0") (deps (list (crate-dep (name "hex") (req "^0.4.3") (default-features #t) (kind 2)))) (hash "1kxzb9wgsk3gj12blzw15gvsq5n7szr3qzdqjzphnza7ysjappnh")))

(define-public crate-fahtsex-0.2 (crate (name "fahtsex") (vers "0.2.1") (deps (list (crate-dep (name "hex") (req "^0.4.3") (default-features #t) (kind 2)))) (hash "1ss1z68cp1y53k3wwaj7fg0kc7jayd95xnsj8j1cigylbhhx3mgb")))

(define-public crate-fahtsex-0.2 (crate (name "fahtsex") (vers "0.2.2") (deps (list (crate-dep (name "hex") (req "^0.4.3") (default-features #t) (kind 2)))) (hash "07zgib3m1yqlsvzcv051sra96aq47banggkcjxrflqpdzy57a3s5")))

