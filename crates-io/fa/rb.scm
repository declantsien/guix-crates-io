(define-module (crates-io fa rb) #:use-module (crates-io))

(define-public crate-farba-0.1 (crate (name "farba") (vers "0.1.0") (deps (list (crate-dep (name "image") (req "^0.24.6") (optional #t) (default-features #t) (kind 0)))) (hash "0w3ksqcfg3qy9vg614rbaxhrg28nd6jzb7zkgrcwjgkkji428lig") (features (quote (("default" "image")))) (v 2) (features2 (quote (("image" "dep:image"))))))

(define-public crate-farba-0.1 (crate (name "farba") (vers "0.1.1") (deps (list (crate-dep (name "image") (req "^0.24.6") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "minifb") (req "^0.24.0") (optional #t) (default-features #t) (kind 0)))) (hash "1a6z1n4r0qb11p7132w7799lqhx8f01rhxnwg28nzj5kdddjjdml") (features (quote (("default")))) (v 2) (features2 (quote (("window" "dep:minifb") ("image" "dep:image"))))))

(define-public crate-farbe-0.1 (crate (name "farbe") (vers "0.1.0") (deps (list (crate-dep (name "anyhow") (req "^1.0.70") (default-features #t) (kind 0)) (crate-dep (name "byteorder") (req "^1.4.3") (default-features #t) (kind 0)) (crate-dep (name "png") (req "^0.17.7") (default-features #t) (kind 0)))) (hash "0zkcd7vsl71r9157sshmgvby2fpbsr8v3zhd4929crvv3k8g330n")))

(define-public crate-farbe-0.2 (crate (name "farbe") (vers "0.2.0") (deps (list (crate-dep (name "anyhow") (req "^1.0.70") (default-features #t) (kind 0)) (crate-dep (name "byteorder") (req "^1.4.3") (default-features #t) (kind 0)) (crate-dep (name "clap") (req "^4.1.13") (features (quote ("derive"))) (default-features #t) (kind 0)) (crate-dep (name "png") (req "^0.17.7") (default-features #t) (kind 0)))) (hash "0ggqk02wn62k9qhf0iy04kn4c5cxq9w3rj4rf0vs5cbq1m6ikqhh")))

(define-public crate-farbfeld-0.2 (crate (name "farbfeld") (vers "0.2.0") (deps (list (crate-dep (name "byteorder") (req "^0.4") (default-features #t) (kind 0)))) (hash "08kzx7a03vds3q5nxn59jgssfqqagb7mjszq84iaplj6alvfff4j")))

(define-public crate-farbfeld-0.3 (crate (name "farbfeld") (vers "0.3.1") (deps (list (crate-dep (name "byteorder") (req "^1.0.0") (default-features #t) (kind 0)))) (hash "0zxrkmpfr59rbfgnsj9395ib2a0575190awlzhcb90m0kz50kxhf")))

(define-public crate-farbraum-0.1 (crate (name "farbraum") (vers "0.1.0") (deps (list (crate-dep (name "serde") (req "^1") (features (quote ("derive"))) (optional #t) (default-features #t) (kind 0)))) (hash "12285xni4srh1146gb6y8a5izzi8izwsaam44hn1g4iafs0d2hfi") (features (quote (("double-precision") ("default" "double-precision"))))))

