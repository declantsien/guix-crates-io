(define-module (crates-io fa ri) #:use-module (crates-io))

(define-public crate-farid-0.1 (crate (name "farid") (vers "0.1.0") (hash "1miz9fhgjcksrswc11y2m5smm57xh6n7rg0izcsnfj5j3g3fd2il")))

(define-public crate-farid-0.1 (crate (name "farid") (vers "0.1.1") (hash "1j9i85na432ciwqp25hv3ms2b9i4l9988xxm1jjzfsr8nnp03f9n")))

(define-public crate-farid-0.1 (crate (name "farid") (vers "0.1.2") (hash "0sym76imqfh3z3gnnihzabxq92kiz6hkz35k7c21d8s2plz97v7p")))

