(define-module (crates-io fa bu) #:use-module (crates-io))

(define-public crate-fabula-0.1 (crate (name "fabula") (vers "0.1.0") (hash "05ak973n0s9h5xcw3mw86b8ysp5bdavmb0bk88m5sv3rivbj81nd")))

(define-public crate-fabulist-core-0.1 (crate (name "fabulist-core") (vers "0.1.0") (hash "05ppin5psns7yvhflr86snnpwaia4rvmbanzyrq6kjv2sl5y8z29")))

