(define-module (crates-io fa ye) #:use-module (crates-io))

(define-public crate-faye-0.0.1 (crate (name "faye") (vers "0.0.1") (hash "1x9jakgz9sm5xwj2kccdi7bmfak33n2bbjjmqda4c6gjwq59xj6b")))

(define-public crate-faye-0.1 (crate (name "faye") (vers "0.1.0") (deps (list (crate-dep (name "rustyline") (req "^12.0.0") (default-features #t) (kind 0)))) (hash "187ahyks157ryrx90s3i3ssiaazzbki734xzka36vl5rbnxk50ll")))

(define-public crate-faye-0.1 (crate (name "faye") (vers "0.1.1") (deps (list (crate-dep (name "rustyline") (req "^12.0.0") (default-features #t) (kind 0)))) (hash "0lrc0yy5pzzgm73bfbidfn18xi0yfk56wrj7ybq8m7xrzgs0pnfh")))

(define-public crate-faye-0.1 (crate (name "faye") (vers "0.1.2") (deps (list (crate-dep (name "clap") (req "^4.4.2") (features (quote ("derive" "cargo"))) (default-features #t) (kind 0)) (crate-dep (name "rustyline") (req "^12.0.0") (default-features #t) (kind 0)))) (hash "13vpqcy4a4k6nn2av4dhbfm0g8xxxap08zra18drc65sifikzpf3")))

(define-public crate-faye-0.1 (crate (name "faye") (vers "0.1.3") (deps (list (crate-dep (name "clap") (req "^4.4.2") (features (quote ("derive" "cargo"))) (default-features #t) (kind 0)) (crate-dep (name "rustyline") (req "^12.0.0") (default-features #t) (kind 0)))) (hash "1sbzszz5xv16hrq6yi92zrw97j1fmslcs1nmhbh5rqlablbvxrwj")))

(define-public crate-faye-0.2 (crate (name "faye") (vers "0.2.0") (deps (list (crate-dep (name "clap") (req "^4.4.2") (features (quote ("derive" "cargo"))) (default-features #t) (kind 0)) (crate-dep (name "rustyline") (req "^12.0.0") (default-features #t) (kind 0)))) (hash "0qy792c86g0y2vx7qvz3mifrj6n29bzasrgsqbkwc3p48d1in8f3")))

(define-public crate-faye-0.2 (crate (name "faye") (vers "0.2.2") (deps (list (crate-dep (name "clap") (req "^4.4.2") (features (quote ("derive" "cargo"))) (default-features #t) (kind 0)) (crate-dep (name "rustyline") (req "^12.0.0") (default-features #t) (kind 0)))) (hash "0ryr0z3mgr9z2dgki790zsy3ymbjhm9q6xq866pmgs9630adfhy0")))

(define-public crate-faye-0.3 (crate (name "faye") (vers "0.3.0") (deps (list (crate-dep (name "clap") (req "^4.4.2") (features (quote ("derive" "cargo"))) (default-features #t) (kind 0)) (crate-dep (name "rustyline") (req "^12.0.0") (default-features #t) (kind 0)))) (hash "1lcnr58b972hl4kzqx8raj687hmpfb0qfxdmvvl6628mkcb4jz7m")))

(define-public crate-faye-0.3 (crate (name "faye") (vers "0.3.1") (deps (list (crate-dep (name "clap") (req "^4.4.2") (features (quote ("derive" "cargo"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "rustyline") (req "^12.0.0") (optional #t) (default-features #t) (kind 0)))) (hash "1grafwcszbrkxnvxqxxx5wlblhzmackmzbnq3357spxavgsdza05") (features (quote (("repl" "rustyline") ("default" "cli") ("cli" "clap" "repl")))) (v 2) (features2 (quote (("rustyline" "dep:rustyline") ("clap" "dep:clap"))))))

(define-public crate-faye-0.3 (crate (name "faye") (vers "0.3.2") (deps (list (crate-dep (name "clap") (req "^4.4.2") (features (quote ("derive" "cargo"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "rustyline") (req "^12.0.0") (optional #t) (default-features #t) (kind 0)))) (hash "0yis3ndd11n8hn3kbbx41mn6b5qwqmxk3svxm3llnhkpcikhd1v6") (v 2) (features2 (quote (("default" "dep:clap" "dep:rustyline"))))))

(define-public crate-faye-0.3 (crate (name "faye") (vers "0.3.3") (deps (list (crate-dep (name "clap") (req "^4.4.2") (features (quote ("derive" "cargo"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "rustyline") (req "^12.0.0") (optional #t) (default-features #t) (kind 0)))) (hash "1agp26w2n7i5pawcr26jibcsia5fs63gzs2b1169viixwb5hkz3d") (v 2) (features2 (quote (("default" "dep:clap" "dep:rustyline"))))))

(define-public crate-faye-0.4 (crate (name "faye") (vers "0.4.0") (deps (list (crate-dep (name "clap") (req "^4.4.2") (features (quote ("derive" "cargo"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "rustyline") (req "^12.0.0") (optional #t) (default-features #t) (kind 0)))) (hash "0rpw2nvxhx0rw57qnah6ai2x8xrf5dv787208f2f67wxm8cr8zxs") (v 2) (features2 (quote (("default" "dep:clap" "dep:rustyline"))))))

(define-public crate-faye-0.4 (crate (name "faye") (vers "0.4.1") (deps (list (crate-dep (name "clap") (req "^4.4.2") (features (quote ("derive" "cargo"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "rustyline") (req "^12.0.0") (optional #t) (default-features #t) (kind 0)))) (hash "0dv2z85axx6whp4ijs01599lz0ids7pdn9b3dcpj80lad932i6g4") (v 2) (features2 (quote (("default" "dep:clap" "dep:rustyline"))))))

(define-public crate-faye-0.4 (crate (name "faye") (vers "0.4.2") (deps (list (crate-dep (name "clap") (req "^4.4.2") (features (quote ("derive" "cargo"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "rustyline") (req "^12.0.0") (optional #t) (default-features #t) (kind 0)))) (hash "0cgk1xxv330znl22v5wv1ppm40lpzskf1c0a5vypaaw15gn7rsqd") (v 2) (features2 (quote (("default" "dep:clap" "dep:rustyline"))))))

(define-public crate-faye-0.5 (crate (name "faye") (vers "0.5.0") (deps (list (crate-dep (name "clap") (req "^4.4.2") (features (quote ("derive" "cargo"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "rustyline") (req "^12.0.0") (optional #t) (default-features #t) (kind 0)))) (hash "01f244dpsnhamsrlxvm1a3q1n8i7nnagyhiw25kv0k70gcgvk68x") (v 2) (features2 (quote (("default" "dep:clap" "dep:rustyline"))))))

(define-public crate-faye-0.5 (crate (name "faye") (vers "0.5.1") (deps (list (crate-dep (name "clap") (req "^4.4.2") (features (quote ("derive" "cargo"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "rustyline") (req "^12.0.0") (optional #t) (default-features #t) (kind 0)))) (hash "12r4igpa8vn0ql8yb00vmhb6zy1m1y0nxrd1j7s8c35yqhj08hvm") (v 2) (features2 (quote (("default" "dep:clap" "dep:rustyline"))))))

(define-public crate-faye-0.5 (crate (name "faye") (vers "0.5.2") (deps (list (crate-dep (name "clap") (req "^4.4.2") (features (quote ("derive" "cargo"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "faye-lsp") (req "^0.2.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "pomprt") (req "^0.4.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.33.0") (features (quote ("full"))) (optional #t) (default-features #t) (kind 0)))) (hash "133pgggdcrncj5phx92drqp0x41dy642mj0bwvhs89cnifzwf4lv") (v 2) (features2 (quote (("lsp" "dep:faye-lsp" "dep:tokio") ("default" "dep:clap" "dep:pomprt"))))))

(define-public crate-faye-0.6 (crate (name "faye") (vers "0.6.0") (deps (list (crate-dep (name "clap") (req "^4.4.7") (features (quote ("derive" "cargo"))) (optional #t) (default-features #t) (kind 0)) (crate-dep (name "faye-lsp") (req "^0.2.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "pomprt") (req "^0.4.0") (optional #t) (default-features #t) (kind 0)) (crate-dep (name "tokio") (req "^1.33.0") (features (quote ("full"))) (optional #t) (default-features #t) (kind 0)))) (hash "0xrc54sfkm8ylfw1rl523kf784gb310dgaz9h74sf717q3881m7h") (v 2) (features2 (quote (("lsp" "dep:faye-lsp" "dep:tokio") ("default" "dep:clap" "dep:pomprt"))))))

(define-public crate-faye-lsp-0.1 (crate (name "faye-lsp") (vers "0.1.0") (deps (list (crate-dep (name "dashmap") (req "^5.5.3") (default-features #t) (kind 0)) (crate-dep (name "faye") (req "^0.3.2") (kind 0)) (crate-dep (name "tokio") (req "^1.32.0") (features (quote ("full"))) (default-features #t) (kind 0)) (crate-dep (name "tower-lsp") (req "^0.20.0") (default-features #t) (kind 0)))) (hash "1hw8y0zpbibalkdryr4cj7q0mqgb8l2mrg908m5cb46v9hzprjl2")))

(define-public crate-faye-lsp-0.2 (crate (name "faye-lsp") (vers "0.2.0") (deps (list (crate-dep (name "dashmap") (req "^5.5.3") (default-features #t) (kind 0)) (crate-dep (name "faye") (req "^0.5.1") (kind 0)) (crate-dep (name "tokio") (req "^1.32.0") (features (quote ("full"))) (default-features #t) (kind 0)) (crate-dep (name "tower-lsp") (req "^0.20.0") (default-features #t) (kind 0)))) (hash "1va7zx1pv8s9q4z0dq75k3dz1jd9fs8kvcczdfr7bj481l6y4nc8")))

