(define-module (crates-io fa a_) #:use-module (crates-io))

(define-public crate-faa_array_queue-0.1 (crate (name "faa_array_queue") (vers "0.1.0") (deps (list (crate-dep (name "array-macro") (req "^2.0") (default-features #t) (kind 0)) (crate-dep (name "peril") (req "^0.4") (default-features #t) (kind 0)))) (hash "04zzgjw0fknlwf2c1vzx31vd1rrwhsapkgr3gld0k3lysycv4pqn")))

(define-public crate-faa_array_queue-0.1 (crate (name "faa_array_queue") (vers "0.1.1") (deps (list (crate-dep (name "array-macro") (req "^2.0") (default-features #t) (kind 0)) (crate-dep (name "peril") (req "^0.4") (default-features #t) (kind 0)))) (hash "1ls8cpnqqicc4kxa3jivl2jhna1ph0ia4wyhwz50i2w687j8h094")))

(define-public crate-faa_array_queue-0.1 (crate (name "faa_array_queue") (vers "0.1.2") (deps (list (crate-dep (name "array-macro") (req "^2.0") (default-features #t) (kind 0)) (crate-dep (name "peril") (req "^0.4") (default-features #t) (kind 0)))) (hash "1qkk6gaknpnkv10k4bm7gxm0wa55r93xgvgzrkxf6m0di4pxbc9d")))

