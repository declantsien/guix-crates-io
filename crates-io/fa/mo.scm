(define-module (crates-io fa mo) #:use-module (crates-io))

(define-public crate-famous-0.1 (crate (name "famous") (vers "0.1.0") (hash "01lbllxgx9ah5zipsip6g21hh7qnfzvcvr7z3cn31g63njd6jrg8") (yanked #t)))

(define-public crate-famous-last-words-0.1 (crate (name "famous-last-words") (vers "0.1.0") (deps (list (crate-dep (name "thiserror") (req "^1.0.26") (default-features #t) (kind 0)))) (hash "1df319x9cbaxm5n7a5hsmzza8awkjbbb990arbw7hy21bqrzyhfx")))

(define-public crate-famous-last-words-0.1 (crate (name "famous-last-words") (vers "0.1.1") (deps (list (crate-dep (name "thiserror") (req "^1.0.26") (default-features #t) (kind 0)))) (hash "0s5pg3xca4cn6mfzbkk8a97lk53cykvhxfjwh8fkkv0p08d2m4wv")))

