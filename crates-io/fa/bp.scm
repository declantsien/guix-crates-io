(define-module (crates-io fa bp) #:use-module (crates-io))

(define-public crate-fabparse-0.1 (crate (name "fabparse") (vers "0.1.0") (deps (list (crate-dep (name "smallvec") (req "^1.11.2") (default-features #t) (kind 0)))) (hash "0yzma4lmk4asdp3f2aqph9bs8cvqd4p08gh24yg5jj6qpg53hj87")))

(define-public crate-fabparse-0.1 (crate (name "fabparse") (vers "0.1.1") (deps (list (crate-dep (name "smallvec") (req "^1.11.2") (default-features #t) (kind 0)))) (hash "1izfivhzc196x4p24lywa5jyd7ms1zxfyx3ldxrw85dvny1yydif")))

